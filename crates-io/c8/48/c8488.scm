(define-module (crates-io c8 #{48}# c8488) #:use-module (crates-io))

(define-public crate-c8488-0.1.0 (c (n "c8488") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "chrono") (r "^0.4.22") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.1") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (f (quote ("max_level_debug" "release_max_level_off"))) (d #t) (k 0)) (d (n "pico-args") (r "^0.5.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "0vh5sq541iwjaqs1168z8rjf1f97z01d9kmg6wy2078aba46l0n3")))

(define-public crate-c8488-0.1.1 (c (n "c8488") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "chrono") (r "^0.4.22") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.1") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (f (quote ("max_level_debug" "release_max_level_off"))) (d #t) (k 0)) (d (n "pico-args") (r "^0.5.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "052br3nza77i050rrfmm7ifwb0h83r3g8i7i2459f6n3ig6r4qy2")))

