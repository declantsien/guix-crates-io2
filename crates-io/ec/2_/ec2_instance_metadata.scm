(define-module (crates-io ec #{2_}# ec2_instance_metadata) #:use-module (crates-io))

(define-public crate-ec2_instance_metadata-0.1.0 (c (n "ec2_instance_metadata") (v "0.1.0") (d (list (d (n "json") (r "^0.12") (d #t) (k 0)) (d (n "ureq") (r "^0.12") (d #t) (k 0)))) (h "1f8dzi5mbi01im7jca6zc1mxahlzr4g74yablfr3344n213cs8iq")))

(define-public crate-ec2_instance_metadata-0.1.1 (c (n "ec2_instance_metadata") (v "0.1.1") (d (list (d (n "json") (r "^0.12") (d #t) (k 0)) (d (n "ureq") (r "^0.12") (d #t) (k 0)))) (h "0zca9jn7zy3awxc6q7h98la6s5mgpsnj64apbw3v8f4yp7xh564z")))

(define-public crate-ec2_instance_metadata-0.1.2 (c (n "ec2_instance_metadata") (v "0.1.2") (d (list (d (n "json") (r "^0.12") (d #t) (k 0)) (d (n "ureq") (r "^0.12") (d #t) (k 0)))) (h "1m0n9s4yl82vd3wsv64gw5y7dwsvmgywkj4yspga460pw4n9dwj5")))

(define-public crate-ec2_instance_metadata-0.2.0 (c (n "ec2_instance_metadata") (v "0.2.0") (d (list (d (n "json") (r "^0.12") (d #t) (k 0)) (d (n "ureq") (r "^1.2") (d #t) (k 0)))) (h "18635a0nm0q7fhwh15icfkv2spbr8kaq3ijpbfvphxz78fsxbm8d")))

(define-public crate-ec2_instance_metadata-0.2.1 (c (n "ec2_instance_metadata") (v "0.2.1") (d (list (d (n "json") (r "^0.12") (d #t) (k 0)) (d (n "ureq") (r "^1.3") (d #t) (k 0)))) (h "1hjvmnhyqr4vjg5shyr0dsj76hbvz73nj4002vzk8wmi6b27lc6n")))

(define-public crate-ec2_instance_metadata-0.2.2 (c (n "ec2_instance_metadata") (v "0.2.2") (d (list (d (n "json") (r "^0.12") (d #t) (k 0)) (d (n "ureq") (r "^1.3") (d #t) (k 0)))) (h "0fjb2h0930xj1vcca0h18bc83lg3m5dfvyzl7gq576maf6pgpli9")))

(define-public crate-ec2_instance_metadata-0.3.0 (c (n "ec2_instance_metadata") (v "0.3.0") (d (list (d (n "json") (r "^0.12") (d #t) (k 0)) (d (n "ureq") (r "^1.4") (d #t) (k 0)))) (h "0jr8gijnmk5wp8l0lcfwl8a9j7gx93kqnwbaqf421hdb2z6wgrh9")))

