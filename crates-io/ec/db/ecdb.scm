(define-module (crates-io ec db ecdb) #:use-module (crates-io))

(define-public crate-ecdb-0.0.1 (c (n "ecdb") (v "0.0.1") (h "0dx5d5smp673jx9jhc6nf3mramacs7pc9c32clrpzqnfr3iywdqj")))

(define-public crate-ecdb-0.0.2 (c (n "ecdb") (v "0.0.2") (d (list (d (n "protobuf") (r "^2.8.1") (f (quote ("with-serde"))) (d #t) (k 0)) (d (n "protobuf-codegen-pure") (r "^2.3") (d #t) (k 1)) (d (n "serde") (r "^1.0.101") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)))) (h "0hlsibvh2xfc49xs63b7b3phh36sifdn8afs48i1j09sn48nn9b4") (f (quote (("with-serde"))))))

