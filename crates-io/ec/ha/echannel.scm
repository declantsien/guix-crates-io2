(define-module (crates-io ec ha echannel) #:use-module (crates-io))

(define-public crate-echannel-0.0.1 (c (n "echannel") (v "0.0.1") (d (list (d (n "async-channel") (r "^1.8.0") (d #t) (k 0)))) (h "14ybswpbfzy60yrqc47vwmgsj6fzfp4g6r8sm97b7nx7s601rr4v")))

(define-public crate-echannel-0.0.2 (c (n "echannel") (v "0.0.2") (d (list (d (n "async-channel") (r "^1.8.0") (d #t) (k 0)))) (h "0dk3q9b1kx8vasgixpp70z93yp44n4zvmnlkia2rkdrm5dcc1k4y")))

(define-public crate-echannel-0.0.3 (c (n "echannel") (v "0.0.3") (d (list (d (n "async-channel") (r "^1.8.0") (d #t) (k 0)))) (h "0840nmjlx221lzidb971fwx5vh3zfca6ka2ygpshx00h3xrjnfj2")))

