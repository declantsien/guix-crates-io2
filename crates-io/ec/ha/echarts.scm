(define-module (crates-io ec ha echarts) #:use-module (crates-io))

(define-public crate-echarts-0.1.0 (c (n "echarts") (v "0.1.0") (d (list (d (n "js-sys") (r "^0.3.61") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.84") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.60") (f (quote ("Element" "HtmlCanvasElement"))) (d #t) (k 0)))) (h "0jqkphl62axk3zh69h5h09lyghg7q80441xa04vcwviyx7cqjigs")))

(define-public crate-echarts-0.1.1 (c (n "echarts") (v "0.1.1") (d (list (d (n "wasm-bindgen") (r "^0.2.84") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.60") (f (quote ("Element" "HtmlCanvasElement"))) (d #t) (k 0)))) (h "0lfjknwv6n7np04jq5vsjly32mm0j15kzbxgvblbjqvsv5x8pk51")))

(define-public crate-echarts-0.1.2 (c (n "echarts") (v "0.1.2") (d (list (d (n "wasm-bindgen") (r "^0.2.84") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.60") (f (quote ("Element" "HtmlCanvasElement"))) (d #t) (k 0)))) (h "06jzfmlkga2jki191j2i26h14qfd19vn71y3i9p3d7ixg0a7d0ns")))

