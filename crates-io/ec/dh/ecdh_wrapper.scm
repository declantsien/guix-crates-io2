(define-module (crates-io ec dh ecdh_wrapper) #:use-module (crates-io))

(define-public crate-ecdh_wrapper-0.0.0 (c (n "ecdh_wrapper") (v "0.0.0") (d (list (d (n "rand") (r "^0.4.2") (d #t) (k 0)) (d (n "rand") (r "^0.4.2") (d #t) (k 2)) (d (n "sodiumoxide") (r "^0.0.16") (d #t) (k 0)))) (h "0gi243fcvdmldk5x51cwya7zjx832k66lh5swz15ymqrqkgz1wmf")))

(define-public crate-ecdh_wrapper-0.0.1 (c (n "ecdh_wrapper") (v "0.0.1") (d (list (d (n "rand") (r "^0.4.2") (d #t) (k 0)) (d (n "rand") (r "^0.4.2") (d #t) (k 2)) (d (n "sodiumoxide") (r "^0.0.16") (d #t) (k 0)))) (h "128jinq84z87va2scapw1mjxaz9dm91s6wygrjyadssxvgmg0apk")))

(define-public crate-ecdh_wrapper-0.0.2 (c (n "ecdh_wrapper") (v "0.0.2") (d (list (d (n "rand") (r "^0.4.2") (d #t) (k 0)) (d (n "rand") (r "^0.4.2") (d #t) (k 2)) (d (n "sodiumoxide") (r "^0.0.16") (d #t) (k 0)))) (h "0l28vcr9nxjg4vdqzjkbraxrmln377fjjdjz6fs1cb66m8fcszw0")))

(define-public crate-ecdh_wrapper-0.0.3 (c (n "ecdh_wrapper") (v "0.0.3") (d (list (d (n "rand") (r "^0.4.2") (d #t) (k 0)) (d (n "rand") (r "^0.4.2") (d #t) (k 2)) (d (n "sodiumoxide") (r "^0.0.16") (d #t) (k 0)))) (h "0lr6ns2karkrjri9m340pfbhsk98rrnjx84c2q814rfcjzylc6nv")))

(define-public crate-ecdh_wrapper-0.0.4 (c (n "ecdh_wrapper") (v "0.0.4") (d (list (d (n "rand") (r "^0.4.2") (d #t) (k 0)) (d (n "rand") (r "^0.4.2") (d #t) (k 2)) (d (n "x25519-dalek") (r "= 0.2.1") (d #t) (k 0)))) (h "1wfi36abb35sw0byh5i4qghcsb9c9rf3l6xa7mrzqkrj119ljn6b")))

(define-public crate-ecdh_wrapper-0.0.5 (c (n "ecdh_wrapper") (v "0.0.5") (d (list (d (n "rand") (r "^0.4.2") (d #t) (k 0)) (d (n "rand") (r "^0.4.2") (d #t) (k 2)) (d (n "x25519-dalek") (r "= 0.2.1") (d #t) (k 0)))) (h "0nhhf3gzx8zmdgcvgjbzndi0r06di4apf1g3c2gjfv9chhyihpjl")))

(define-public crate-ecdh_wrapper-0.0.6 (c (n "ecdh_wrapper") (v "0.0.6") (d (list (d (n "base64") (r "^0.10.0") (d #t) (k 0)) (d (n "pem") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.4.2") (d #t) (k 0)) (d (n "rand") (r "^0.4.2") (d #t) (k 2)) (d (n "tempfile") (r "^3.0.4") (d #t) (k 2)) (d (n "x25519-dalek") (r "= 0.2.1") (d #t) (k 0)))) (h "1damzzq8sj24066sapy70yh8p81xfcvnlylmq0826s943303p6pn")))

(define-public crate-ecdh_wrapper-0.0.7 (c (n "ecdh_wrapper") (v "0.0.7") (d (list (d (n "base64") (r "^0.10.0") (d #t) (k 0)) (d (n "clear_on_drop") (r "^0.2.3") (d #t) (k 0)) (d (n "pem") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.4.2") (d #t) (k 0)) (d (n "rand") (r "^0.4.2") (d #t) (k 2)) (d (n "tempfile") (r "^3.0.4") (d #t) (k 2)) (d (n "x25519-dalek") (r "= 0.2.1") (d #t) (k 0)))) (h "1dxmcm813zdmrjnki2kw07dcrd4pgpjsxj0linyn4j7vgbfr2vr8")))

(define-public crate-ecdh_wrapper-0.0.8 (c (n "ecdh_wrapper") (v "0.0.8") (d (list (d (n "base64") (r "^0.10.0") (d #t) (k 0)) (d (n "clear_on_drop") (r "^0.2.3") (d #t) (k 0)) (d (n "pem") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.4.2") (d #t) (k 0)) (d (n "rand") (r "^0.4.2") (d #t) (k 2)) (d (n "tempfile") (r "^3.0.4") (d #t) (k 2)) (d (n "x25519-dalek") (r "^0.4.2") (d #t) (k 0)))) (h "0ab5mmpmc75g70ijxvs7v35mdwsqi2nx6i2gvnnmyp66g2kn6qq5")))

(define-public crate-ecdh_wrapper-0.0.9 (c (n "ecdh_wrapper") (v "0.0.9") (d (list (d (n "base64") (r "^0.10.0") (d #t) (k 0)) (d (n "clear_on_drop") (r "^0.2.3") (d #t) (k 0)) (d (n "pem") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.4.2") (d #t) (k 0)) (d (n "rand") (r "^0.4.2") (d #t) (k 2)) (d (n "tempfile") (r "^3.0.4") (d #t) (k 2)) (d (n "x25519-dalek") (r "^0.4.2") (d #t) (k 0)))) (h "044z3ka25a9harrwrmj3xa5c7nvlzqfjqks37p60nrcsfx67y93m")))

