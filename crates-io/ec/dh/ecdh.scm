(define-module (crates-io ec dh ecdh) #:use-module (crates-io))

(define-public crate-ecdh-0.0.1 (c (n "ecdh") (v "0.0.1") (h "0zban1dbxkxgfz9sh9bwick6wirv3y2cc6dnmfxv3l7pl1ppxvvj")))

(define-public crate-ecdh-0.0.2 (c (n "ecdh") (v "0.0.2") (h "1wl5rfp2xk2ngzd3838bamajs3r6qlxfmmvhh2p4r5vgdjv88nba")))

(define-public crate-ecdh-0.0.3 (c (n "ecdh") (v "0.0.3") (h "1yi03vrmjp5c9x6acm318glpsdy1pcvkqzxlmhj48q7qprwjl9y6")))

(define-public crate-ecdh-0.0.4 (c (n "ecdh") (v "0.0.4") (d (list (d (n "log") (r "^0.2.5") (d #t) (k 0)))) (h "1hij7yjjhs248kagywib8mkqi6rk5x4av32825j0mfyzn1cf2w4y")))

(define-public crate-ecdh-0.0.5 (c (n "ecdh") (v "0.0.5") (d (list (d (n "log") (r "^0.2.5") (d #t) (k 0)))) (h "1h9c047qhpm03pb34fvcqp4sd1vrxd8mn3lzq3nkmfsw6mkqy9ig")))

(define-public crate-ecdh-0.0.6 (c (n "ecdh") (v "0.0.6") (d (list (d (n "log") (r "^0.2.5") (d #t) (k 0)))) (h "02z6aa1yvwipa2pjqjlv3albs3m8jxb8n8y1is9y9kishzpn1ydc")))

(define-public crate-ecdh-0.0.7 (c (n "ecdh") (v "0.0.7") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "log") (r "^0.3.1") (d #t) (k 0)))) (h "0p0mqbl8vhi736a9y45wgwlgjllrkhmynfbdbvxvyz36ag7q9g27")))

(define-public crate-ecdh-0.0.8 (c (n "ecdh") (v "0.0.8") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "log") (r "^0.3.1") (d #t) (k 0)))) (h "01w99h0vzd6w9rkj7fvmkkf9drl84pd50q0rqcqjanylm5dg3bnm")))

(define-public crate-ecdh-0.0.9 (c (n "ecdh") (v "0.0.9") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "log") (r "^0.3.1") (d #t) (k 0)))) (h "0hf7yxk355ygd5i4hhgzwn69pa5yqxgxp9kzql1fzsd3ah1zhfvj")))

(define-public crate-ecdh-0.0.10 (c (n "ecdh") (v "0.0.10") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "log") (r "^0.3.1") (d #t) (k 0)))) (h "10sv5caxm6ixh7yjcjbxryb2blv6qnqj3r60f8l559lm9n3zqcjp")))

(define-public crate-ecdh-0.0.12 (c (n "ecdh") (v "0.0.12") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "log") (r "^0.3.1") (d #t) (k 0)))) (h "1rgbq879h6xhq2yp7npv69isscf2n9kxc6mhih7imm0jq23llxhp")))

