(define-module (crates-io ec ip ecip) #:use-module (crates-io))

(define-public crate-ecip-0.1.0 (c (n "ecip") (v "0.1.0") (d (list (d (n "ciphersuite") (r "^0.3") (k 0)) (d (n "multiexp") (r "^0.3") (k 0)) (d (n "pasta_curves") (r "^0.5") (f (quote ("bits" "alloc"))) (o #t) (k 0)) (d (n "pasta_curves") (r "^0.5") (f (quote ("bits" "alloc"))) (k 2)) (d (n "rand_core") (r "^0.6") (k 0)) (d (n "rand_core") (r "^0.6") (f (quote ("getrandom"))) (d #t) (k 2)) (d (n "subtle") (r "^2.4") (k 0)) (d (n "transcript") (r "^0.3") (d #t) (k 0) (p "flexible-transcript")) (d (n "zeroize") (r "^1.5") (f (quote ("zeroize_derive"))) (k 0)))) (h "11c1x39s470k1digp11rb87m97mxhfb0myxzqcfzvvj2s8qrz5jl") (f (quote (("pasta" "pasta_curves"))))))

