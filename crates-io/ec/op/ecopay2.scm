(define-module (crates-io ec op ecopay2) #:use-module (crates-io))

(define-public crate-ecopay2-0.1.0 (c (n "ecopay2") (v "0.1.0") (d (list (d (n "cosmwasm") (r "^0.7.2") (d #t) (k 0)) (d (n "cosmwasm-vm") (r "^0.7.2") (k 2)) (d (n "cw-storage") (r "^0.2.0") (d #t) (k 0)) (d (n "schemars") (r "= 0.5") (d #t) (k 0)) (d (n "serde") (r "= 1.0.103") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "snafu") (r "= 0.5.0") (f (quote ("rust_1_30"))) (k 0)))) (h "0crxrxszj62hm0jzd2bsgbmhyf252dgjq7hfmgccp0jgkx5vslz4") (f (quote (("singlepass" "cosmwasm-vm/default-singlepass") ("default" "cranelift") ("cranelift" "cosmwasm-vm/default-cranelift") ("backtraces" "cosmwasm/backtraces" "cosmwasm-vm/backtraces"))))))

