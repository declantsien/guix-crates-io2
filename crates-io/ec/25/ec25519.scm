(define-module (crates-io ec #{25}# ec25519) #:use-module (crates-io))

(define-public crate-ec25519-0.1.0 (c (n "ec25519") (v "0.1.0") (d (list (d (n "ct-codecs") (r "^1.1") (o #t) (d #t) (k 0)) (d (n "ed25519") (r "^1.5") (o #t) (d #t) (k 0)) (d (n "getrandom") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "ct-codecs") (r "^1.1") (d #t) (k 2)) (d (n "getrandom") (r "^0.2") (d #t) (k 2)))) (h "09f8f60n3m79hzylpqf26z5x46ky3yp15569733ph4f05wx57zdx") (f (quote (("x25519") ("traits" "ed25519") ("std") ("self-verify") ("random" "getrandom") ("pem" "ct-codecs") ("opt_size") ("disable-signatures") ("default" "random" "std" "x25519" "pem") ("blind-keys"))))))

