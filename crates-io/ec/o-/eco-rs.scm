(define-module (crates-io ec o- eco-rs) #:use-module (crates-io))

(define-public crate-eco-rs-0.1.0 (c (n "eco-rs") (v "0.1.0") (h "02k3a99p4wv0543z7l0c989z7djjg12168a2a15wdjdcjr3kmqv3")))

(define-public crate-eco-rs-0.1.1 (c (n "eco-rs") (v "0.1.1") (h "19nxacfzr8rkb9hc17sy0hym8b9yydml4rqxvvfqhgnfjaql01lj")))

(define-public crate-eco-rs-0.1.2 (c (n "eco-rs") (v "0.1.2") (h "1jk9fjgzlnl1gxc5j37npvk6nly7i4c8sxm0jgbsrs6mv62g5jyi")))

(define-public crate-eco-rs-0.1.3 (c (n "eco-rs") (v "0.1.3") (d (list (d (n "console") (r "^0.15.8") (f (quote ("windows-console-colors"))) (d #t) (k 0)))) (h "0dfllkmc861x7gk8mzsx7gd66cqlzzxm8jjs2z59m8b25mzg7lla")))

(define-public crate-eco-rs-0.1.4 (c (n "eco-rs") (v "0.1.4") (d (list (d (n "console") (r "^0.15.8") (f (quote ("windows-console-colors"))) (d #t) (k 0)))) (h "1bfdi45n9v50jp8k3flnbdarilcywbscibxvcfch32zc03qy27r3")))

(define-public crate-eco-rs-0.1.5 (c (n "eco-rs") (v "0.1.5") (d (list (d (n "console") (r "^0.15.8") (f (quote ("windows-console-colors"))) (d #t) (k 0)))) (h "08ygg4by64g06vlf9qc0fdh21z7pzm75ykpp2b6qk34ik4fwrqs9")))

(define-public crate-eco-rs-0.1.6 (c (n "eco-rs") (v "0.1.6") (d (list (d (n "console") (r "^0.15.8") (f (quote ("windows-console-colors"))) (d #t) (k 0)))) (h "17mqaryb7m3li97pnskz7hgc83rvd80w3w6qyich2j1amw4iplfp")))

