(define-module (crates-io ec -p ec-pairing) #:use-module (crates-io))

(define-public crate-ec-pairing-0.0.10 (c (n "ec-pairing") (v "0.0.10") (d (list (d (n "bls-12-381") (r "^0.0.10") (k 0)) (d (n "jub-jub") (r "^0.0.10") (k 0)) (d (n "parity-scale-codec") (r "^2.0.0") (f (quote ("derive"))) (k 0)) (d (n "rand_core") (r "^0.6.4") (f (quote ("getrandom"))) (d #t) (k 2)) (d (n "zkstd") (r "^0.0.10") (k 0)))) (h "1b1yc4d36qd4hwmpsicj7dhwp3wlmy1xsn9c9xc7gcfpq3vf1n5f")))

(define-public crate-ec-pairing-0.0.11 (c (n "ec-pairing") (v "0.0.11") (d (list (d (n "bls-12-381") (r "^0.0.12") (k 0)) (d (n "jub-jub") (r "^0.0.14") (k 0)) (d (n "parity-scale-codec") (r "^2.0.0") (f (quote ("derive"))) (k 0)) (d (n "rand_core") (r "^0.6.4") (f (quote ("getrandom"))) (d #t) (k 2)) (d (n "zkstd") (r "^0.0.12") (k 0)))) (h "1g2zs8gcbw8lm2qg37y2bz5y3a4f2shm33i06w03n9miv48qk4zh")))

(define-public crate-ec-pairing-0.0.12 (c (n "ec-pairing") (v "0.0.12") (d (list (d (n "bls-12-381") (r "^0.0.13") (k 0)) (d (n "jub-jub") (r "^0.0.15") (k 0)) (d (n "parity-scale-codec") (r "^2.0.0") (f (quote ("derive"))) (k 0)) (d (n "rand_core") (r "^0.6.4") (f (quote ("getrandom"))) (d #t) (k 2)) (d (n "zkstd") (r "^0.0.13") (k 0)))) (h "0r2ifvazblxyl57inv5ha5mkzsgf5mvwnsmhv0pillg6imjsr57r")))

(define-public crate-ec-pairing-0.0.13 (c (n "ec-pairing") (v "0.0.13") (d (list (d (n "bls-12-381") (r "^0.0.22") (k 0)) (d (n "jub-jub") (r "^0.0.19") (k 0)) (d (n "parity-scale-codec") (r "^2.0.0") (f (quote ("derive"))) (k 0)) (d (n "rand_core") (r "^0.6.4") (f (quote ("getrandom"))) (d #t) (k 2)) (d (n "zkstd") (r "^0.0.21") (k 0)))) (h "0vj9r1sn2r6gsdf4lawry28dd8aj6r4z2zdypwyg9xizisah481h")))

(define-public crate-ec-pairing-0.0.14 (c (n "ec-pairing") (v "0.0.14") (d (list (d (n "bls-12-381") (r "^0.0.23") (k 0)) (d (n "jub-jub") (r "^0.0.20") (k 0)) (d (n "parity-scale-codec") (r "^2.0.0") (f (quote ("derive"))) (k 0)) (d (n "rand_core") (r "^0.6.4") (f (quote ("getrandom"))) (d #t) (k 2)) (d (n "zkstd") (r "^0.0.22") (k 0)))) (h "0qhjf3h87i0r4jq89bi04mqfxswyq30lmy9k373mgg8pn99g7bp0")))

