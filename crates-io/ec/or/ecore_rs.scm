(define-module (crates-io ec or ecore_rs) #:use-module (crates-io))

(define-public crate-ecore_rs-0.1.0 (c (n "ecore_rs") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.6") (d #t) (k 0)) (d (n "safe_index") (r "^0.9") (d #t) (k 0)) (d (n "simple_logger") (r "^2.3") (d #t) (k 2)) (d (n "smallvec") (r "^1.10") (d #t) (k 0)))) (h "1jpmhhv470kq1zb2biba20lsqpjm1irab9ck3k35lypawzpzqwa5")))

