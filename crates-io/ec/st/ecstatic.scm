(define-module (crates-io ec st ecstatic) #:use-module (crates-io))

(define-public crate-ecstatic-0.0.1 (c (n "ecstatic") (v "0.0.1") (h "180cikckqnxyfjwzmdj3jcc0nr3n2b0c7m0xj6fckg0dk2jf7vmx")))

(define-public crate-ecstatic-0.0.2 (c (n "ecstatic") (v "0.0.2") (h "09was8ys7sg1rqabpca7k9aa2ajc03bpb84srbd8gfvnf2xz055z")))

