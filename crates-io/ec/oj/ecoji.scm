(define-module (crates-io ec oj ecoji) #:use-module (crates-io))

(define-public crate-ecoji-1.0.0 (c (n "ecoji") (v "1.0.0") (d (list (d (n "clap") (r "^2.31.1") (o #t) (d #t) (k 0)) (d (n "phf") (r "^0.7.21") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.7.21") (d #t) (k 1)) (d (n "quickcheck") (r "^0.6") (d #t) (k 2)))) (h "02yh2b9bapcmbclvsiisn7jd48khyvb2khg36bm9gk4993ld0idl") (f (quote (("build-binary" "clap"))))))

