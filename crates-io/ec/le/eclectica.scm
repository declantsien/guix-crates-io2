(define-module (crates-io ec le eclectica) #:use-module (crates-io))

(define-public crate-eclectica-0.0.1 (c (n "eclectica") (v "0.0.1") (h "18dd75as5f1s5hsk7c55psxjnb8h6yrncl828xkpkp2hzv74s5j8")))

(define-public crate-eclectica-0.0.2 (c (n "eclectica") (v "0.0.2") (h "04hy4assc6kf9ja4a2829nahl35ksbka2m3pi2hv9dvgl125g223")))

(define-public crate-eclectica-0.0.6 (c (n "eclectica") (v "0.0.6") (h "0sa5mc09v4f1r2s6l1q4yrsg2pk4sin72r3vlr4bss09rc8g5fmi")))

(define-public crate-eclectica-0.0.7 (c (n "eclectica") (v "0.0.7") (h "1xdwgw07byf0iclrsp9smx1nwrlcm0s7wxcqb4l4rn3jp4y6g0g6")))

(define-public crate-eclectica-0.0.8 (c (n "eclectica") (v "0.0.8") (h "1pm4x27jhvjav4n0f6v5nbzvc2xgz3jyc9cvpzgk395lxyq77363")))

(define-public crate-eclectica-0.0.9 (c (n "eclectica") (v "0.0.9") (h "1dijcy49ykls8npwq0p5i0z3p56f911vk2p36dz2g6kn4vd87ixb")))

(define-public crate-eclectica-0.0.12 (c (n "eclectica") (v "0.0.12") (h "0vb4d34sgnfcah7qj29bsdgl749jrlaq4dfgjlrrcrjgxfr29d7d")))

(define-public crate-eclectica-0.0.13 (c (n "eclectica") (v "0.0.13") (h "1k4lr0qi35jhm25lx3bw963wh7xil51zx3p4mcfaxj4x66f87m47")))

(define-public crate-eclectica-0.0.14 (c (n "eclectica") (v "0.0.14") (h "09bs4l1hgcm4y35fvdd80al3biy1d82bhb54k6wi3553080zk8gp")))

(define-public crate-eclectica-0.0.15 (c (n "eclectica") (v "0.0.15") (h "1sr7mb4campzd9n860v6q88vmpssqgdgrg267hl702g2fnjgkj8j")))

(define-public crate-eclectica-0.1.0 (c (n "eclectica") (v "0.1.0") (h "02jpa15rrvmv19rz2iwdrjm8f67sw2vhcjjrj99sjjh71rd8nb9a")))

(define-public crate-eclectica-0.1.1 (c (n "eclectica") (v "0.1.1") (h "0ikybh3yl0jvr8jza1fazgi3wzmrlq7fvi85vzak2x3j3iqzgd8l")))

(define-public crate-eclectica-0.2.0 (c (n "eclectica") (v "0.2.0") (h "1ks7ql83hv5b50d55xsbllmswf5znqxja7akxi4crd09wy29ij78")))

(define-public crate-eclectica-0.2.1 (c (n "eclectica") (v "0.2.1") (h "0xv2281fmas6i43m02fq7xkvfqykpnsh1ki63fa4vl3424dbg3q4")))

(define-public crate-eclectica-0.3.0 (c (n "eclectica") (v "0.3.0") (h "1gr240bc272brlwkgz7cbl0j33ivhb22xqxb4hcvs7kk0layrsq1")))

(define-public crate-eclectica-0.3.1 (c (n "eclectica") (v "0.3.1") (h "1afjv4nadb4xick7h8mlax0mnh24y1m8bq1a90v52rzd8i9igbrc")))

(define-public crate-eclectica-0.3.2 (c (n "eclectica") (v "0.3.2") (h "10bhy3jnkw85h6jwb9pl5yimwgfli8myvsaqhxj4cdkzhx5y1ikp")))

(define-public crate-eclectica-0.3.3 (c (n "eclectica") (v "0.3.3") (h "150ncaykc71rkqvslhw1wg8w6hdq3lacdavcsn245pfv53nzlil2")))

(define-public crate-eclectica-0.4.0 (c (n "eclectica") (v "0.4.0") (h "0ll4zn96qa833iq2kr51bbkm58ww5356jdq50b14840k6zlzipri")))

(define-public crate-eclectica-0.4.1 (c (n "eclectica") (v "0.4.1") (h "0ilxc1imgm2qr84sk1vcq3q6rf46c16h198yz6p56ldz7l8c7ay1")))

(define-public crate-eclectica-0.5.0 (c (n "eclectica") (v "0.5.0") (h "0dj876ggrpaqldk8giagbx4a1l2jxsjbq2p4qq1xsfx8nfcnb6wr")))

(define-public crate-eclectica-0.6.0 (c (n "eclectica") (v "0.6.0") (h "0v5cxc6xvyxdpvky0p1a91n0440kapr17ri6dp1rqwzrg84nq90c")))

(define-public crate-eclectica-0.7.0 (c (n "eclectica") (v "0.7.0") (h "0xlgm01h7jvf4xd495v0b5qrg01y9zbdvs03400miny2q8gf9hpw")))

(define-public crate-eclectica-0.7.1 (c (n "eclectica") (v "0.7.1") (h "18yvw9nasaqqgfr9scjffkgcd5v6739w0wdfff9qs5bmd6xkqb17")))

(define-public crate-eclectica-0.8.0 (c (n "eclectica") (v "0.8.0") (h "0c9y2ha68cwfj7dd1kdh1b87i7c22ppk4rikkg1bdbrb1hvr6icy")))

(define-public crate-eclectica-0.8.2 (c (n "eclectica") (v "0.8.2") (h "1wwvs48dsx95ww6pi9dwfk6q2hayr50f8y3flqj8xln8b9j3kp3v")))

(define-public crate-eclectica-0.8.3 (c (n "eclectica") (v "0.8.3") (h "1g39dninhhmlzwcng6nbyf6k0din6y1kvbwsdxzviqbm81n8i1i9")))

(define-public crate-eclectica-0.8.4 (c (n "eclectica") (v "0.8.4") (h "10jl4fr3zca6wc3y6zw1bdr04h6kmpqmms2lycgjkznn76l4rrjp")))

(define-public crate-eclectica-0.8.5 (c (n "eclectica") (v "0.8.5") (h "0gqpvk2v38gi8z74kfri9x7lvwv7p7j16rdaibilns7kigaiw8ha")))

(define-public crate-eclectica-0.8.6 (c (n "eclectica") (v "0.8.6") (h "0r032bzd8vb9jajfl829h966c8zy9wybzy3ys5mn8mmsw1mawd5h")))

