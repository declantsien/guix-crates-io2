(define-module (crates-io ec le eclectic) #:use-module (crates-io))

(define-public crate-eclectic-0.0.1 (c (n "eclectic") (v "0.0.1") (d (list (d (n "collect") (r "*") (o #t) (d #t) (k 0)) (d (n "compare") (r "*") (o #t) (d #t) (k 0)) (d (n "trie") (r "*") (o #t) (d #t) (k 0)))) (h "19k5zf5acxjh9w8zgpbjqs0y9k5m68al7lf9rkkm7xfn7997ipb1") (f (quote (("default" "collect_impls" "trie") ("collect_impls" "collect" "compare"))))))

(define-public crate-eclectic-0.0.2 (c (n "eclectic") (v "0.0.2") (h "0dd0ahl5lahbljqr2wkjlw1mk9d8a0xhhk8mknjdr15xcmr4r205")))

(define-public crate-eclectic-0.0.3 (c (n "eclectic") (v "0.0.3") (h "1yqsc8xbbd8c89d8rsbw974sfyjsxk871vqki7vjldf9pp9qwzy5") (f (quote (("nightly"))))))

(define-public crate-eclectic-0.1.0 (c (n "eclectic") (v "0.1.0") (d (list (d (n "linear-map") (r "*") (o #t) (d #t) (k 0)) (d (n "linked-hash-map") (r "*") (o #t) (d #t) (k 0)) (d (n "vec_map") (r "*") (o #t) (d #t) (k 0)))) (h "02zrjw4sm2yrgr1vxfzzbf74sk5vpavqx7hrk6zssbp07mmhc8q3")))

(define-public crate-eclectic-0.3.0 (c (n "eclectic") (v "0.3.0") (d (list (d (n "linear-map") (r "*") (o #t) (d #t) (k 0)) (d (n "linked-hash-map") (r "*") (o #t) (d #t) (k 0)) (d (n "trie") (r "*") (o #t) (d #t) (k 0)) (d (n "vec_map") (r "*") (o #t) (d #t) (k 0)))) (h "1l2qxxi21p7zzc4xmsmn59lan1dwsgjq465h3p72iiqnh86lrzxx") (f (quote (("std_impls") ("nightly") ("default" "std_impls"))))))

(define-public crate-eclectic-0.4.0 (c (n "eclectic") (v "0.4.0") (d (list (d (n "linear-map") (r "*") (o #t) (d #t) (k 0)) (d (n "linked-hash-map") (r "*") (o #t) (d #t) (k 0)) (d (n "trie") (r "*") (o #t) (d #t) (k 0)) (d (n "vec_map") (r "*") (o #t) (d #t) (k 0)))) (h "0b4czvyn68zd3fmgcp5jihqzcg32vwcav89m52jhzc59r9d43mac") (f (quote (("std_impls") ("nightly") ("default" "std_impls"))))))

(define-public crate-eclectic-0.5.0 (c (n "eclectic") (v "0.5.0") (h "0npb2365iga3zdkgq3xywgxnjpf0cwsld09yy2gs7np0k2adfax2")))

(define-public crate-eclectic-0.6.0 (c (n "eclectic") (v "0.6.0") (h "16pan5zzdh19icnhwqj64577npms7p6gxvqg1kgq5ihca17nygb0")))

(define-public crate-eclectic-0.7.0 (c (n "eclectic") (v "0.7.0") (h "09r70f4cbwzr9pkjdi87i6l1xv5w43nl3k726lcrg550fdwn1j76")))

(define-public crate-eclectic-0.8.0 (c (n "eclectic") (v "0.8.0") (h "1snawd7pkd1v5d9yjf657jvdy9b6i855skmg565mnva0qbn2jlqi")))

(define-public crate-eclectic-0.9.0 (c (n "eclectic") (v "0.9.0") (h "0kwk8bcl8r7vs3dw6y657f33nz94nrf71ygf7k0ipmyz994gj0zn")))

(define-public crate-eclectic-0.10.0 (c (n "eclectic") (v "0.10.0") (h "0p0s03wjz1gqbc1jc6lpbdv7adx46w5j0z57z255zn6hfh9x095m") (f (quote (("nightly"))))))

(define-public crate-eclectic-0.11.0 (c (n "eclectic") (v "0.11.0") (h "0zhxx587bx23il6qazh7rmrrvh3q58jmpw3bafjdjf82sh01jvm0") (f (quote (("nightly"))))))

(define-public crate-eclectic-0.12.0 (c (n "eclectic") (v "0.12.0") (h "0r2gw1nrysjnyrgy34h03nnzj8fpbysray0y4d7nwymhkcy1kafq") (f (quote (("nightly")))) (y #t)))

