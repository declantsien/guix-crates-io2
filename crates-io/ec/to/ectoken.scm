(define-module (crates-io ec to ectoken) #:use-module (crates-io))

(define-public crate-ectoken-0.1.0 (c (n "ectoken") (v "0.1.0") (d (list (d (n "base64") (r "^0.10") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)))) (h "1fp2lxrx5mmzakac1bq2kwq308bjnzh6lfmfwqbh1i564gac5kl5")))

(define-public crate-ectoken-0.1.1 (c (n "ectoken") (v "0.1.1") (d (list (d (n "base64") (r "^0.10") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)))) (h "006rbm5qv2p51z25gj3qxi67c1rc2adw5p1bs4kcg915wj1s24md")))

(define-public crate-ectoken-0.2.0 (c (n "ectoken") (v "0.2.0") (d (list (d (n "base64") (r "^0.11") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)))) (h "1nd12qrhf2zb3bnjnky1xd5rb27f4wsmqaxxnfclqk8q8j3bhj1c") (y #t)))

(define-public crate-ectoken-0.2.1 (c (n "ectoken") (v "0.2.1") (d (list (d (n "base64") (r "^0.11") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)))) (h "1cgfbjpsjwzly2zzv492wdm44736jf9f4znfi09n06xl1y3fl24p")))

(define-public crate-ectoken-0.2.2 (c (n "ectoken") (v "0.2.2") (d (list (d (n "base64") (r "^0.11") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)))) (h "1v6gssdasrdqgmsq7p2y8kcqxv3rfxpsav99wh8y4wh65kdmld6s")))

(define-public crate-ectoken-0.3.1 (c (n "ectoken") (v "0.3.1") (d (list (d (n "aes-gcm") (r "^0.8") (d #t) (k 0)) (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "sha2") (r "^0.9") (d #t) (k 0)))) (h "1n900kx6gxknp3nb2gz8rj5w2hdnqnvzj5s7zxjzj43p88gl9h2h")))

(define-public crate-ectoken-0.4.0 (c (n "ectoken") (v "0.4.0") (d (list (d (n "aes-gcm") (r "^0.9") (d #t) (k 0)) (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "sha2") (r "^0.9") (d #t) (k 0)))) (h "0rjk64lxc5wkdnh328mbqv19smzswmz7g9v6zyhscyy6fwp4x1zp") (y #t)))

(define-public crate-ectoken-0.4.1 (c (n "ectoken") (v "0.4.1") (d (list (d (n "aes-gcm") (r "^0.9") (d #t) (k 0)) (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "sha2") (r "^0.9") (d #t) (k 0)))) (h "1r228vyw6nw3hgypmfs2gry1ys122vrbbpdp99yc2idigxnjawgm")))

(define-public crate-ectoken-0.4.2 (c (n "ectoken") (v "0.4.2") (d (list (d (n "aes-gcm") (r "^0.9") (d #t) (k 0)) (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)))) (h "0zv571jg11qa2h2q2hjacaha0j0ykkag2lmx1fmqac92gh6xr1k5")))

(define-public crate-ectoken-0.4.3 (c (n "ectoken") (v "0.4.3") (d (list (d (n "aes-gcm") (r "^0.9") (d #t) (k 0)) (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)))) (h "0ldnd40sks4ghy7fhskxc6hyjvw4zy0988gy2gmh3g4qg18k10x0")))

(define-public crate-ectoken-0.4.4 (c (n "ectoken") (v "0.4.4") (d (list (d (n "aes-gcm") (r "^0.9") (d #t) (k 0)) (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)))) (h "19n16gdv4cjl43chl4n95rfvsaxq0hykjnvhycbyrpz1y28wzn8p")))

