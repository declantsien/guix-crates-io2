(define-module (crates-io ec to ectoplasm) #:use-module (crates-io))

(define-public crate-ectoplasm-0.1.0 (c (n "ectoplasm") (v "0.1.0") (d (list (d (n "futures") (r "^0.3.12") (d #t) (k 0)) (d (n "num-complex") (r "^0.3.1") (f (quote ("rand"))) (d #t) (k 2)) (d (n "rand") (r "^0.7.0") (d #t) (k 2)) (d (n "tokio") (r "^1.2.0") (f (quote ("macros" "rt-multi-thread" "sync" "time"))) (d #t) (k 0)) (d (n "tokio-native-tls") (r "^0.3.0") (d #t) (k 0)) (d (n "tokio-tungstenite") (r "^0.13.0") (f (quote ("tls"))) (d #t) (k 0)) (d (n "triggered") (r "^0.1.1") (d #t) (k 0)) (d (n "unifont") (r "^0.1.0") (d #t) (k 2)) (d (n "url") (r "^2.2.0") (d #t) (k 0)))) (h "1d55rnmgi9my2m3vgq8gwv5vxjsdvd95brj2fvkxsziml5hdd5yv")))

