(define-module (crates-io ec e_ ece_421_sam_cynthia_aditya_trees) #:use-module (crates-io))

(define-public crate-ece_421_sam_cynthia_aditya_trees-0.1.0 (c (n "ece_421_sam_cynthia_aditya_trees") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.7.0") (d #t) (k 0)))) (h "1xn5jl21dqg9pphplnbcck1xll8br6bn04i1yams8dk49rs5nsk0")))

(define-public crate-ece_421_sam_cynthia_aditya_trees-0.1.1 (c (n "ece_421_sam_cynthia_aditya_trees") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.7.0") (d #t) (k 0)))) (h "0gvacan53nli524fjl6qxjz8mczcxzc2hagmq9lncypqpmif8kqs")))

