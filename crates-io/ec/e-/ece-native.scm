(define-module (crates-io ec e- ece-native) #:use-module (crates-io))

(define-public crate-ece-native-0.1.0-pre (c (n "ece-native") (v "0.1.0-pre") (d (list (d (n "aes-gcm") (r "^0.10.1") (d #t) (k 0)) (d (n "base64") (r "^0.13.0") (d #t) (k 2)) (d (n "hkdf") (r "^0.12.3") (d #t) (k 0)) (d (n "once_cell") (r "^1.13.1") (d #t) (k 2)) (d (n "sha2") (r "^0.10.2") (d #t) (k 0)))) (h "07nddn8c8kcfzjgabmmfr2j4pv1z8ilkq813bqb15nxj8xjyqszm")))

(define-public crate-ece-native-0.1.0-pre2 (c (n "ece-native") (v "0.1.0-pre2") (d (list (d (n "aes-gcm") (r "^0.10.1") (d #t) (k 0)) (d (n "base64") (r "^0.13.0") (d #t) (k 2)) (d (n "hkdf") (r "^0.12.3") (d #t) (k 0)) (d (n "once_cell") (r "^1.13.1") (d #t) (k 2)) (d (n "sha2") (r "^0.10.2") (d #t) (k 0)))) (h "0f54rp2qij03x7frghf2lhg6b77mrbczh777mcxq7c6rv93zbpx0")))

(define-public crate-ece-native-0.1.0-pre3 (c (n "ece-native") (v "0.1.0-pre3") (d (list (d (n "aes-gcm") (r "^0.10.1") (d #t) (k 0)) (d (n "base64") (r "^0.13.0") (d #t) (k 2)) (d (n "hkdf") (r "^0.12.3") (d #t) (k 0)) (d (n "once_cell") (r "^1.13.1") (d #t) (k 2)) (d (n "sha2") (r "^0.10.2") (d #t) (k 0)))) (h "15axg2q3vzbj4qgqh4iff6d7zld4vz60h9k2qwxk8pcgp3c76hgf")))

(define-public crate-ece-native-0.1.0-pre4 (c (n "ece-native") (v "0.1.0-pre4") (d (list (d (n "aes-gcm") (r "^0.10.1") (d #t) (k 0)) (d (n "base64") (r "^0.13.1") (d #t) (k 2)) (d (n "hkdf") (r "^0.12.3") (d #t) (k 0)) (d (n "once_cell") (r "^1.15.0") (d #t) (k 2)) (d (n "sha2") (r "^0.10.6") (d #t) (k 0)))) (h "1lvaviq7cwck8vlfw56ghlcb2yigg6yzxjqybxkwrjgdyq85n781")))

(define-public crate-ece-native-0.1.0 (c (n "ece-native") (v "0.1.0") (d (list (d (n "aes-gcm") (r "^0.10.1") (d #t) (k 0)) (d (n "base64") (r "^0.13.1") (d #t) (k 2)) (d (n "hkdf") (r "^0.12.3") (d #t) (k 0)) (d (n "once_cell") (r "^1.15.0") (d #t) (k 2)) (d (n "sha2") (r "^0.10.6") (d #t) (k 0)))) (h "0dwnirc8i6a4vggz3ffdmsn0lx7qhqskmjvbv1bzrgq43wa34n6w")))

(define-public crate-ece-native-0.1.1 (c (n "ece-native") (v "0.1.1") (d (list (d (n "aes-gcm") (r "^0.10.1") (d #t) (k 0)) (d (n "base64ct") (r "^1.5.3") (f (quote ("alloc" "std"))) (d #t) (k 2)) (d (n "hkdf") (r "^0.12.3") (d #t) (k 0)) (d (n "once_cell") (r "^1.16.0") (d #t) (k 2)) (d (n "sha2") (r "^0.10.6") (d #t) (k 0)))) (h "0syhwl080gvv9ybrzs8bwgg6kljdjn9v4s0f3fpkw3641alg3xi8")))

(define-public crate-ece-native-0.1.2 (c (n "ece-native") (v "0.1.2") (d (list (d (n "aes-gcm") (r "^0.10.1") (d #t) (k 0)) (d (n "base64ct") (r "^1.5.3") (f (quote ("alloc" "std"))) (d #t) (k 2)) (d (n "hkdf") (r "^0.12.3") (d #t) (k 0)) (d (n "once_cell") (r "^1.16.0") (d #t) (k 2)) (d (n "sha2") (r "^0.10.6") (d #t) (k 0)))) (h "0bp852s1gm0az3k3lrvhr9njympmcm7q9bx26p6a5nlid5a99y7m")))

(define-public crate-ece-native-0.1.3 (c (n "ece-native") (v "0.1.3") (d (list (d (n "aes-gcm") (r "^0.10.1") (d #t) (k 0)) (d (n "base64ct") (r "^1.5.3") (f (quote ("alloc" "std"))) (d #t) (k 2)) (d (n "hkdf") (r "^0.12.3") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.0") (d #t) (k 2)) (d (n "sha2") (r "^0.10.6") (d #t) (k 0)))) (h "0raw6x9w7dd4xcbfycf74pcbg6ps9bckhs3rxd9pngv8w12hv432") (r "1.60.0")))

(define-public crate-ece-native-0.1.4 (c (n "ece-native") (v "0.1.4") (d (list (d (n "aes-gcm") (r "^0.10.1") (d #t) (k 0)) (d (n "base64ct") (r "^1.6.0") (f (quote ("alloc" "std"))) (d #t) (k 2)) (d (n "hkdf") (r "^0.12.3") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 2)) (d (n "sha2") (r "^0.10.6") (d #t) (k 0)))) (h "0i5yyn2nyvc11ypccc9q4nk7wbhwhm176zbm2jj3h9fhd6ncxiwz") (r "1.60.0")))

(define-public crate-ece-native-0.2.0 (c (n "ece-native") (v "0.2.0") (d (list (d (n "aes-gcm") (r "^0.10.2") (d #t) (k 0)) (d (n "base64ct") (r "^1.6.0") (f (quote ("alloc" "std"))) (d #t) (k 2)) (d (n "hkdf") (r "^0.12.3") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 2)) (d (n "sha2") (r "^0.10.6") (d #t) (k 0)))) (h "07db5m50p48w7d2lv5c9a9jgmqhqh69z7j7byajarrmz39l758pg") (r "1.60.0")))

(define-public crate-ece-native-0.2.1 (c (n "ece-native") (v "0.2.1") (d (list (d (n "aes-gcm") (r "^0.10.3") (d #t) (k 0)) (d (n "base64ct") (r "^1.6.0") (f (quote ("alloc" "std"))) (d #t) (k 2)) (d (n "hkdf") (r "^0.12.3") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 2)) (d (n "sha2") (r "^0.10.7") (d #t) (k 0)))) (h "16wy85i0qf9059x2kikv7yc691w8mzhzrskal6znwrajw7skrh77") (r "1.60.0")))

(define-public crate-ece-native-0.3.0 (c (n "ece-native") (v "0.3.0") (d (list (d (n "aes-gcm") (r "^0.10.3") (d #t) (k 0)) (d (n "base64ct") (r "^1.6.0") (f (quote ("alloc" "std"))) (d #t) (k 2)) (d (n "hkdf") (r "^0.12.3") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 2)) (d (n "sha2") (r "^0.10.7") (d #t) (k 0)))) (h "1vflvqmjk5mbdw1v0134ynrvfwll6nfhqviz5iixvqmlzs9d1g9c") (r "1.60.0")))

(define-public crate-ece-native-0.4.0 (c (n "ece-native") (v "0.4.0") (d (list (d (n "aes-gcm") (r "^0.10.3") (d #t) (k 0)) (d (n "base64ct") (r "^1.6.0") (f (quote ("alloc" "std"))) (d #t) (k 2)) (d (n "hkdf") (r "^0.12.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 2)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)))) (h "1mh5dimvixbczihc2yc8jj6hyamxk88iyg3669x41jk4ak0f5n1h") (r "1.60.0")))

