(define-module (crates-io ec la eclair_bindings) #:use-module (crates-io))

(define-public crate-eclair_bindings-0.0.1 (c (n "eclair_bindings") (v "0.0.1") (d (list (d (n "ffi-opaque") (r "^2.0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.139") (d #t) (k 0)))) (h "1pmmknff5dv1y74yrqlqlpqm810k6912byc3chydbbgawsapj80m")))

(define-public crate-eclair_bindings-0.1.0 (c (n "eclair_bindings") (v "0.1.0") (d (list (d (n "ffi-opaque") (r "^2.0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.139") (d #t) (k 0)))) (h "19lmakmafd17kyk5ip9g5fy33kmbqjcapblfk932s3f5hb4h06sd")))

