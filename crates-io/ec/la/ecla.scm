(define-module (crates-io ec la ecla) #:use-module (crates-io))

(define-public crate-ecla-0.1.0 (c (n "ecla") (v "0.1.0") (d (list (d (n "elog") (r "^0.1") (d #t) (k 0)))) (h "1cn43jgkialig7ahs8giz8sc4g2w9z64qix5hnc06x4fxzilqh78")))

(define-public crate-ecla-0.1.1 (c (n "ecla") (v "0.1.1") (d (list (d (n "elog") (r "^0.1") (d #t) (k 0)))) (h "0qypdhs9dm1136m1hl7hm0c87a4jb07cgzwqwr655f8b0qv0b4d2")))

(define-public crate-ecla-1.0.0 (c (n "ecla") (v "1.0.0") (d (list (d (n "elog") (r "^0.1") (d #t) (k 0)))) (h "04v5ia81xxhj0zq93a2nqvfklw3hdh50k2bqvnxh2q61b7irxd9p")))

