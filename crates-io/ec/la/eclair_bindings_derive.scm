(define-module (crates-io ec la eclair_bindings_derive) #:use-module (crates-io))

(define-public crate-eclair_bindings_derive-0.0.1 (c (n "eclair_bindings_derive") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1.0.50") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (d #t) (k 0)))) (h "1gi1sqq3f1myfs3lxlbh19lh0aiplzx06al9n038smdizvarsgq3")))

(define-public crate-eclair_bindings_derive-0.1.0 (c (n "eclair_bindings_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.50") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (d #t) (k 0)) (d (n "cc") (r "^1.0.79") (d #t) (k 1)))) (h "09aip3cill3kj7pq0xjxajknrihjcjinhv85x57wc3c849w8zy76")))

