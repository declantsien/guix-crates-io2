(define-module (crates-io ec la eclair-builder) #:use-module (crates-io))

(define-public crate-eclair-builder-0.1.0 (c (n "eclair-builder") (v "0.1.0") (d (list (d (n "cc") (r "^1.0.79") (d #t) (k 0)) (d (n "which") (r "^4.4.0") (d #t) (k 0)))) (h "0f1vqrb11sxcyjdqlsydiqvhla9c70zxdjywfad6vsj419yvz240")))

