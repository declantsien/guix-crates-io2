(define-module (crates-io ec cl eccles) #:use-module (crates-io))

(define-public crate-eccles-0.1.2 (c (n "eccles") (v "0.1.2") (d (list (d (n "time") (r "*") (d #t) (k 0)))) (h "0dbxpbk7v90zrc8jwjs7l2xhmwcf70xwdaa02smm0md19zibvv7x")))

(define-public crate-eccles-0.1.3 (c (n "eccles") (v "0.1.3") (d (list (d (n "time") (r "*") (d #t) (k 0)))) (h "06h67zfjhwxc616vydxskhsdvxdip44322zyhsfr5qwrzpdkxmnx")))

(define-public crate-eccles-0.2.0 (c (n "eccles") (v "0.2.0") (d (list (d (n "time") (r "*") (d #t) (k 0)))) (h "1v0kgawk16s464ds75dkwrzmz67qnq98r2x88b71x7mb7pf04qzl")))

(define-public crate-eccles-0.2.1 (c (n "eccles") (v "0.2.1") (d (list (d (n "time") (r "*") (o #t) (d #t) (k 0)))) (h "0zrd7msvbj2nzq4wi5ci5nqk0z5zyafjd674wk8ikkhh8sh6ns7s") (f (quote (("default" "time"))))))

(define-public crate-eccles-0.2.2 (c (n "eccles") (v "0.2.2") (d (list (d (n "time") (r "*") (o #t) (d #t) (k 0)))) (h "02w5mmzkm7lqazk0wrnb3qzfzkabjvk53xq46pmblfz7lv8h1p7j") (f (quote (("default" "time"))))))

(define-public crate-eccles-0.2.3 (c (n "eccles") (v "0.2.3") (d (list (d (n "clock_ticks") (r "*") (d #t) (k 0)))) (h "0ay8v8yn128k9g3yly2shn45hsjagn9z0q8jv638rdzhd8idhd83")))

