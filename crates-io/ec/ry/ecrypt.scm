(define-module (crates-io ec ry ecrypt) #:use-module (crates-io))

(define-public crate-ecrypt-0.1.2 (c (n "ecrypt") (v "0.1.2") (d (list (d (n "argon2") (r "^0.4.1") (d #t) (k 0)) (d (n "argon_hash_password") (r "^0.1.2") (d #t) (k 0)) (d (n "chacha20poly1305") (r "^0.10.1") (f (quote ("stream"))) (d #t) (k 0)) (d (n "clap") (r "^4.1.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0.23") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rpassword") (r "^7.2.0") (d #t) (k 0)) (d (n "tar") (r "^0.4.38") (d #t) (k 0)) (d (n "zeroize") (r "^1.5.7") (d #t) (k 0)) (d (n "ctor") (r "^0.1.22") (d #t) (k 2)))) (h "1k61sjalxfh47378k7lbhql7afnqa01mb7ayasqb5mc7zpgm24k0")))

