(define-module (crates-io ec vr ecvrf-rs) #:use-module (crates-io))

(define-public crate-ecvrf-rs-1.0.0 (c (n "ecvrf-rs") (v "1.0.0") (d (list (d (n "curve25519-entropic") (r "^3.2.1") (f (quote ("alloc" "u64_backend"))) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "schemars") (r "^0.8.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (k 0)) (d (n "sha2") (r "^0.10.2") (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0kffnrvgha9ipxx3xci2j0n4cl79jn549lgla1ydpvkmsmjpwksn")))

