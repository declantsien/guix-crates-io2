(define-module (crates-io ec vr ecvrf) #:use-module (crates-io))

(define-public crate-ecvrf-0.1.0 (c (n "ecvrf") (v "0.1.0") (d (list (d (n "curve25519-dalek") (r "^1") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)) (d (n "rand_os") (r "^0.1.3") (d #t) (k 0)) (d (n "sha3") (r "^0.8.2") (d #t) (k 0)))) (h "0xayfgjs7n7d1rqj5sgzvhv91c30vij23f5jm9ikps9qg3nn63cd")))

(define-public crate-ecvrf-0.1.1 (c (n "ecvrf") (v "0.1.1") (d (list (d (n "curve25519-dalek") (r "^1") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)) (d (n "rand_os") (r "^0.1.3") (d #t) (k 0)) (d (n "sha3") (r "^0.8.2") (d #t) (k 0)))) (h "10vzh01h3d9jssa9a3f587jhbw31p2801lwrrrl0bjyl942ds8x9")))

(define-public crate-ecvrf-0.2.1 (c (n "ecvrf") (v "0.2.1") (d (list (d (n "curve25519-dalek") (r "^1") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)) (d (n "rand_os") (r "^0.1.3") (d #t) (k 0)) (d (n "sha3") (r "^0.8.2") (d #t) (k 0)))) (h "1509l6vrxf8z2488r57czf6f7jc9c59bkw8csip5yl6dvbk4pib9")))

(define-public crate-ecvrf-0.2.2 (c (n "ecvrf") (v "0.2.2") (d (list (d (n "curve25519-dalek") (r "^1") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)) (d (n "rand_os") (r "^0.1.3") (d #t) (k 0)) (d (n "sha3") (r "^0.8.2") (d #t) (k 0)))) (h "0naj1pw668jam7kqrbb4qkxfbgl9yqy95pp1nqpxm7a5im3fayw9")))

(define-public crate-ecvrf-0.2.3 (c (n "ecvrf") (v "0.2.3") (d (list (d (n "curve25519-dalek") (r "^2.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "sha3") (r "^0.8.2") (d #t) (k 0)))) (h "02yb5ln8gbdhhv5kxls049iqijzh5qxgrrzf0r6rr5a26g9idqfl")))

(define-public crate-ecvrf-0.3.2 (c (n "ecvrf") (v "0.3.2") (d (list (d (n "curve25519-dalek") (r "^1") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)) (d (n "rand_os") (r "^0.1.3") (d #t) (k 0)) (d (n "sha3") (r "^0.8.2") (d #t) (k 0)))) (h "02zqxl3724xi9v9663dzj3zzkgkv0k9srvq2n8pb3kqlfzg8hlr5")))

(define-public crate-ecvrf-0.3.3 (c (n "ecvrf") (v "0.3.3") (d (list (d (n "curve25519-dalek") (r "^1") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)) (d (n "rand_os") (r "^0.1.3") (d #t) (k 0)) (d (n "sha3") (r "^0.8.2") (d #t) (k 0)))) (h "1wbw0ckkkilfcvhjmqzsri3a61gn0lyha2s76zp28g54nj14ir3h")))

(define-public crate-ecvrf-0.4.0 (c (n "ecvrf") (v "0.4.0") (d (list (d (n "curve25519-dalek") (r "^2.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "rand_core") (r "^0.5.1") (d #t) (k 0)) (d (n "sha3") (r "^0.8.2") (d #t) (k 0)))) (h "17k5v2r9hibgz77yg1b7z0dnpwvfhrayl4g06l8h4l991w6qrdhh")))

(define-public crate-ecvrf-0.4.2 (c (n "ecvrf") (v "0.4.2") (d (list (d (n "curve25519-dalek") (r "^2.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "rand_core") (r "^0.5.1") (d #t) (k 0)) (d (n "sha3") (r "^0.8.2") (d #t) (k 0)))) (h "0gqav763yxp5gkkzwvfc02ilgh4npbnvarsl13cd9h497hxdkvwm")))

(define-public crate-ecvrf-0.4.3 (c (n "ecvrf") (v "0.4.3") (d (list (d (n "curve25519-dalek") (r "^3.0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "rand_core") (r "^0.5.1") (d #t) (k 0)) (d (n "sha3") (r "^0.9.1") (d #t) (k 0)))) (h "1l7yp7z4zkj4chrcl986a9zj6lzgwds0cb7lzr5kcs9dkp9nxazi")))

(define-public crate-ecvrf-0.4.4 (c (n "ecvrf") (v "0.4.4") (d (list (d (n "curve25519-dalek") (r "^3.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "rand_core") (r "^0.5.1") (d #t) (k 0)) (d (n "sha3") (r "^0.9.1") (d #t) (k 0)))) (h "0ya66i088a9wmi8lzm7m8jpbca203qdgwsxammk700g5wsw3ca1x")))

