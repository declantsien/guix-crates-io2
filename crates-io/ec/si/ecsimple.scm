(define-module (crates-io ec si ecsimple) #:use-module (crates-io))

(define-public crate-ecsimple-0.1.0 (c (n "ecsimple") (v "0.1.0") (d (list (d (n "asn1obj") (r "^0.1.6") (d #t) (k 0)) (d (n "asn1obj_codegen") (r "^0.1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_core") (r "^0.6.4") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.42") (d #t) (k 0)) (d (n "sm3") (r "^0.4.2") (d #t) (k 0)))) (h "09wwsic9fpfxkdh7l3hb15zq5h5cxp39mxh91c6y15wjbkmbl7n4") (f (quote (("debug_mode")))) (r "1.59.0")))

