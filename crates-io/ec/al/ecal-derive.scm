(define-module (crates-io ec al ecal-derive) #:use-module (crates-io))

(define-public crate-ecal-derive-0.1.0 (c (n "ecal-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0p5pm3gwpg8hdmp73kq94blqj1whphf3xjaf7jbayms074dnmj7j")))

