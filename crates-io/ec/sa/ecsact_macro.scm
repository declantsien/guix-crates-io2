(define-module (crates-io ec sa ecsact_macro) #:use-module (crates-io))

(define-public crate-ecsact_macro-0.1.0 (c (n "ecsact_macro") (v "0.1.0") (d (list (d (n "ecsact") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.49") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.105") (f (quote ("full"))) (d #t) (k 0)))) (h "0i2fmmzn664bbqr2l8gnx3w0xk6s024hvl2a9r3sp11jj0f6d349")))

