(define-module (crates-io ec sa ecsact_dylib_runtime) #:use-module (crates-io))

(define-public crate-ecsact_dylib_runtime-0.1.0 (c (n "ecsact_dylib_runtime") (v "0.1.0") (d (list (d (n "ecsact") (r "^0.1.0") (d #t) (k 0)) (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.78") (d #t) (k 1)) (d (n "json") (r "^0.12.4") (d #t) (k 1)))) (h "1v1hrjr81iihav6yyk04gk5qljxx38cz0zis3xc55jxlph3qzwv9")))

