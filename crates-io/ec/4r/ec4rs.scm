(define-module (crates-io ec #{4r}# ec4rs) #:use-module (crates-io))

(define-public crate-ec4rs-1.0.0-rc.1 (c (n "ec4rs") (v "1.0.0-rc.1") (h "198xxlpqiw9qqrq0gy5wchp3kv0fdwpxvnsnlv482k3lc6qwsm82") (r "1.59")))

(define-public crate-ec4rs-1.0.0 (c (n "ec4rs") (v "1.0.0") (h "1iqiyszz8my3618sa9v8y03nl89marihyy104g0gspiinmczrzmk") (r "1.59")))

(define-public crate-ec4rs-1.0.1 (c (n "ec4rs") (v "1.0.1") (h "1srl1c8dl4rad07rrmx4a05bwnvi6835aavsc5h2jvjdb1n0ym6v") (r "1.56")))

(define-public crate-ec4rs-1.0.2 (c (n "ec4rs") (v "1.0.2") (h "0dwlw73v0lyschcfx94fj196jdgn8311rp6gb3y1zajqshcpiryb") (r "1.56")))

(define-public crate-ec4rs-1.1.0 (c (n "ec4rs") (v "1.1.0") (d (list (d (n "language-tags") (r "^0.3.2") (o #t) (d #t) (k 0)))) (h "0b4njh898zcnqvlhpar30s1iv5l5zysqjri064g2474spmmycdyc") (f (quote (("allow-empty-values")))) (r "1.56")))

