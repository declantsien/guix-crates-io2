(define-module (crates-io ec sr ecsr) #:use-module (crates-io))

(define-public crate-ecsr-0.1.0 (c (n "ecsr") (v "0.1.0") (d (list (d (n "aws-config") (r "^0.49.0") (d #t) (k 0)) (d (n "aws-sdk-ecs") (r "^0.19.0") (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dialoguer") (r "^0.10.2") (f (quote ("fuzzy-select"))) (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("full"))) (d #t) (k 0)))) (h "0gkn30mxbxjqa8gwca8az61kxa2n4zknf6f4y7ksy066jd2mgqsh")))

