(define-module (crates-io ec ge ecgen-rs) #:use-module (crates-io))

(define-public crate-ecgen-rs-0.1.0 (c (n "ecgen-rs") (v "0.1.0") (d (list (d (n "genawaiter") (r "^0.99.1") (f (quote ("futures03"))) (d #t) (k 0)))) (h "0y690hlpzc0abzd1jz9076pjcvdba89cvpfx1zk6hc9qlxlbq1fc")))

(define-public crate-ecgen-rs-0.1.1 (c (n "ecgen-rs") (v "0.1.1") (d (list (d (n "genawaiter") (r "^0.99.1") (f (quote ("futures03"))) (d #t) (k 0)))) (h "08fg10ma35iia013b93pm4a59r7qynhi8nfanyiqphgi233yf4bf")))

