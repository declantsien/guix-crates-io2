(define-module (crates-io ec ho echo-adb) #:use-module (crates-io))

(define-public crate-echo-adb-1.0.0 (c (n "echo-adb") (v "1.0.0") (d (list (d (n "regex") (r "^1.1.0") (d #t) (k 0)))) (h "1mfax7rjmhwb7pcl7gx9ycrgdpl3p25wx78azl20wbxkqlamkw9f")))

(define-public crate-echo-adb-1.0.1 (c (n "echo-adb") (v "1.0.1") (d (list (d (n "regex") (r "^1.1.0") (d #t) (k 0)))) (h "1z49my2f62z5pq3vf6m8j75bpfk8rb3bd3ia2vvd5q2sxn1l7pfn")))

