(define-module (crates-io ec ho echolot) #:use-module (crates-io))

(define-public crate-echolot-0.1.0 (c (n "echolot") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "cpal") (r "^0.13.5") (d #t) (k 0)) (d (n "fundsp") (r "^0.6.3") (d #t) (k 0)))) (h "09yw1jyh0wl8cnyc1swq2s1pf9hjfm0in57hk0i5xi3cyfksj83z")))

(define-public crate-echolot-0.1.1 (c (n "echolot") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "cpal") (r "^0.13.5") (d #t) (k 0)) (d (n "fundsp") (r "^0.6.3") (d #t) (k 0)))) (h "0x2zrc8ncclkc6mldgz1siwa8drvxph1jsygkqvyx6j3ds39m867")))

