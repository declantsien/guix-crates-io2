(define-module (crates-io ec ho echodb) #:use-module (crates-io))

(define-public crate-echodb-0.1.0 (c (n "echodb") (v "0.1.0") (d (list (d (n "arc-swap") (r "^1.5.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.19") (d #t) (k 0)) (d (n "im") (r "^15.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "tokio") (r "^1.15.0") (f (quote ("rt" "macros"))) (d #t) (k 2)))) (h "0w3qdg0b6zx7z0iz45m6p6zh16djbm58qhsasfjgw6cp26p2qahy")))

(define-public crate-echodb-0.1.1 (c (n "echodb") (v "0.1.1") (d (list (d (n "arc-swap") (r "^1.5.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.19") (d #t) (k 0)) (d (n "im") (r "^15.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "tokio") (r "^1.15.0") (f (quote ("rt" "macros"))) (d #t) (k 2)))) (h "1v6np2dch02smqf4z3x8dx1hjx7whmhi2dbmybw7nhywxhsak7jm")))

(define-public crate-echodb-0.2.0 (c (n "echodb") (v "0.2.0") (d (list (d (n "arc-swap") (r "^1.5.0") (d #t) (k 0)) (d (n "im") (r "^15.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "tokio") (r "^1.16.1") (f (quote ("sync"))) (d #t) (k 0)) (d (n "tokio") (r "^1.16.1") (f (quote ("rt" "macros"))) (d #t) (k 2)))) (h "0998i2xb1cdivnxwxiil4nnj9mbpnk4avgn83lyvx6yw6cdmrcf3")))

(define-public crate-echodb-0.2.1 (c (n "echodb") (v "0.2.1") (d (list (d (n "arc-swap") (r "^1.5.0") (d #t) (k 0)) (d (n "imbl") (r "^1.0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("sync"))) (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("rt" "macros"))) (d #t) (k 2)))) (h "08y57ppi5qw2yg9cd0s1vmdy1gwdj18yik2jcsg19bx5m2spswg7")))

(define-public crate-echodb-0.3.0 (c (n "echodb") (v "0.3.0") (d (list (d (n "arc-swap") (r "^1.5.0") (d #t) (k 0)) (d (n "imbl") (r "^1.0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("sync"))) (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("rt" "macros"))) (d #t) (k 2)))) (h "0hbdwmgh02di8b5yygckj1dl5mggg4ff9yw4q8yvp9md9mvjjkcv")))

(define-public crate-echodb-0.4.0 (c (n "echodb") (v "0.4.0") (d (list (d (n "arc-swap") (r "^1.6.0") (d #t) (k 0)) (d (n "imbl") (r "^2.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("sync"))) (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("rt" "macros"))) (d #t) (k 2)))) (h "0jn3xri7fmwv4k01aad6snj6krywz5740668a392rs26pg0228ii")))

(define-public crate-echodb-0.5.0 (c (n "echodb") (v "0.5.0") (d (list (d (n "arc-swap") (r "^1.7.1") (d #t) (k 0)) (d (n "imbl") (r "^2.0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("sync"))) (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("rt" "macros"))) (d #t) (k 2)))) (h "07w00y2889gkfi6lsqb18qz4jxi43nfy6a3f9n8anhqawrmmdlfc")))

(define-public crate-echodb-0.6.0 (c (n "echodb") (v "0.6.0") (d (list (d (n "arc-swap") (r "^1.7.1") (d #t) (k 0)) (d (n "imbl") (r "^2.0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("sync"))) (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("rt" "macros"))) (d #t) (k 2)))) (h "1qcfvfjp7q3drw8mj43iby0jbw6plsr9lv4x3g80sxxcmqw1xhqs")))

