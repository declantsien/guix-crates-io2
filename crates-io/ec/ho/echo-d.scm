(define-module (crates-io ec ho echo-d) #:use-module (crates-io))

(define-public crate-echo-d-0.0.0 (c (n "echo-d") (v "0.0.0") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.90") (d #t) (k 0)))) (h "1jr72qlixi2mnlkjk9i4lammp50cz8nfw93c1s95ifkkn7qaal9l")))

