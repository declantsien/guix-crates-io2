(define-module (crates-io ec ho echor) #:use-module (crates-io))

(define-public crate-echor-1.0.0 (c (n "echor") (v "1.0.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "iron") (r "^0.6.1") (d #t) (k 0)) (d (n "router") (r "^0.6.0") (d #t) (k 0)))) (h "1ps6x1vjk7m6k40kj0h0n0n1xlq33n9ig7g4l6pd6nsvajp4xp00")))

