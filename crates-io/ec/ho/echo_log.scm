(define-module (crates-io ec ho echo_log) #:use-module (crates-io))

(define-public crate-echo_log-0.1.0 (c (n "echo_log") (v "0.1.0") (h "1jkg8361hnzxpvb2crk1dv8870py3r10ynza20384aplkgmqci35") (y #t)))

(define-public crate-echo_log-0.1.1 (c (n "echo_log") (v "0.1.1") (h "0pv2lhffyfqjyjhq516fs4w9mjjlc4zb3d7navm057x23y5a48n9") (y #t)))

