(define-module (crates-io ec ho echoreq) #:use-module (crates-io))

(define-public crate-echoreq-1.0.0 (c (n "echoreq") (v "1.0.0") (d (list (d (n "axum") (r "^0.7.3") (d #t) (k 0)) (d (n "fastrand") (r "^2.0.1") (d #t) (k 2)) (d (n "http") (r "^1.0.0") (d #t) (k 2)) (d (n "reqwest") (r "^0.11.23") (f (quote ("multipart"))) (d #t) (k 2)) (d (n "tokio") (r "^1.35.1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)) (d (n "tower") (r "^0.4.13") (d #t) (k 2)))) (h "0mrvxgs04ak2fsz6n01byp57zjmw4m88x29d48rqqwif1606cpy5")))

