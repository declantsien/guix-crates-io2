(define-module (crates-io ec ho echoserver) #:use-module (crates-io))

(define-public crate-echoserver-0.0.1 (c (n "echoserver") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^4.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "hyper") (r "^0.14") (f (quote ("server" "tcp" "http1" "http2"))) (d #t) (k 0)) (d (n "tokio") (r "^1.24") (f (quote ("rt-multi-thread" "signal" "macros" "net"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 0)))) (h "0vqx22hfnd38nfy5crfqbqlj5128qcyc3xad3jgyj6f8pfq0q0yb")))

(define-public crate-echoserver-0.1.0 (c (n "echoserver") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "axum") (r "^0.7") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("rt-multi-thread" "signal" "macros" "net" "io-util"))) (d #t) (k 0)) (d (n "tower-http") (r "^0.5.2") (f (quote ("add-extension" "trace" "cors" "timeout"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.18") (d #t) (k 0)))) (h "0b3f7sdcl7fdzrywgh6n2rd057f5igkzaa26p24m7c6hm78hayad")))

