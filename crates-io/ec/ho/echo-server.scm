(define-module (crates-io ec ho echo-server) #:use-module (crates-io))

(define-public crate-echo-server-0.1.0 (c (n "echo-server") (v "0.1.0") (h "1yn1g0b0giwsb71p9w1n5x17xfi1ly9564qlphqy3zxsr52yrpr9")))

(define-public crate-echo-server-0.2.0 (c (n "echo-server") (v "0.2.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "hyper") (r "^0.14") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1s4f6hixlmv2wfvysvm3q7qf668fm9n12az7mx41klzfzwmf9ad2")))

(define-public crate-echo-server-1.0.0 (c (n "echo-server") (v "1.0.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "hyper") (r "^0.14") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1ackp5chd8vlqnkch48azr3nqkp3hdm6mmj9jzy7lfn74bjmf68r")))

(define-public crate-echo-server-1.1.0 (c (n "echo-server") (v "1.1.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "hyper") (r "^0.14") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0qfznaj2rwrxlv7p4fn16v4i51ryl6xi73z5c6ig1xm045k3i0vw")))

(define-public crate-echo-server-1.2.0 (c (n "echo-server") (v "1.2.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "hyper") (r "^0.14") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0hlhadgzyvspsf8vmhgmgr7ga9m5bni1bdhzacngmvg7g37s11zj")))

(define-public crate-echo-server-1.2.1 (c (n "echo-server") (v "1.2.1") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "hyper") (r "^0.14") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "12mykkxzvqr3afjykxj2gi2zfsz91y3m71h9cl0r7qghpm5pxl9x")))

(define-public crate-echo-server-2.0.0 (c (n "echo-server") (v "2.0.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "hyper") (r "^0.14") (f (quote ("full"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3.25") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-test") (r "^0.4.2") (d #t) (k 2)))) (h "1cnlrgkr32fm1mdcx28qz4bwnfwvgxjfn6n671j89cbf09g6bmyy")))

