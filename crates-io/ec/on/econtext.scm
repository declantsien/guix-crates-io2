(define-module (crates-io ec on econtext) #:use-module (crates-io))

(define-public crate-econtext-0.1.0 (c (n "econtext") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (k 2)))) (h "14hnrfpxwh58azh7vzlgdk7ds1ky3yiixcmax7a1jsihgqlmnr4w")))

(define-public crate-econtext-0.1.1 (c (n "econtext") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3") (k 2)))) (h "09h9j228x8652gvj3pl6jws0qhb4li6hslr5m56dmm6zv4ykljmw")))

(define-public crate-econtext-0.1.2 (c (n "econtext") (v "0.1.2") (d (list (d (n "criterion") (r "^0.3") (k 2)))) (h "0gir4i3nqmrrrnarid1p7s4izwn758vq2cviqf87whci5921ylp5")))

(define-public crate-econtext-0.2.0 (c (n "econtext") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3") (k 2)))) (h "0kr1sm6hlh8h4r8sq818lbbr52n0vm1i3kj30822k570sn0dxahq")))

