(define-module (crates-io ec on econf-derive) #:use-module (crates-io))

(define-public crate-econf-derive-0.1.0 (c (n "econf-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "0s32fmnkr2pzdhigdrdw6h2bzpp09z0q0x6kbmbn6jljbm5k6llx")))

(define-public crate-econf-derive-0.1.2 (c (n "econf-derive") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "1bran780qz34w3x4nlplh8lpb61nbfq0vzkdl9k3ca5d4if7lb2w")))

(define-public crate-econf-derive-0.1.3 (c (n "econf-derive") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "1ik0abhdpn2y3jr1mcnabfjd21d69snyiv54y58bi99572f0bjdq")))

(define-public crate-econf-derive-0.1.4 (c (n "econf-derive") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "1hjrslckgkjbwmh7yha3akh8dad2ni3j30kxl4bk6pkfd298xw7h")))

(define-public crate-econf-derive-0.2.0 (c (n "econf-derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "0injlhdc1kgj1w4fkxpzqsn9i457v9a4pz2xxad3250h8yn351bw")))

(define-public crate-econf-derive-0.2.1 (c (n "econf-derive") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "0lydvvr5c5q8d43h206qsi79gjsx5lzahx6mm61mm6qc78vc9ns7")))

