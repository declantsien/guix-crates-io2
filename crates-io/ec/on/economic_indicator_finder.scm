(define-module (crates-io ec on economic_indicator_finder) #:use-module (crates-io))

(define-public crate-economic_indicator_finder-0.1.0 (c (n "economic_indicator_finder") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.190") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "129pi73bbqf8dnysv22inqing5ryxykgribwnsbvdnlhb4x5xbd5") (y #t)))

(define-public crate-economic_indicator_finder-0.1.1 (c (n "economic_indicator_finder") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.190") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "07dpj23dhqmijgr16a7jwq063dxb56imb9ca5cbkpzzw4ggiz069") (f (quote (("slow_finder") ("default"))))))

