(define-module (crates-io ec on econf) #:use-module (crates-io))

(define-public crate-econf-0.1.0 (c (n "econf") (v "0.1.0") (d (list (d (n "econf-derive") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "simple_logger") (r "^0.5") (d #t) (k 2)))) (h "0x70khy7gsw3n8zh23mdagdccklmf582pnccjnmknvm96hlxbdma")))

(define-public crate-econf-0.1.1 (c (n "econf") (v "0.1.1") (d (list (d (n "econf-derive") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "parse_duration") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "simple_logger") (r "^0.5") (d #t) (k 2)))) (h "1gbi0pssy4h0zc1mr8ca428ls2bxyvzhahq9sqkrqk4h5wskpwkh")))

(define-public crate-econf-0.1.2 (c (n "econf") (v "0.1.2") (d (list (d (n "econf-derive") (r "^0.1.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "parse_duration") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "simple_logger") (r "^0.5") (d #t) (k 2)))) (h "08jcd1i2drnfix0kxph0k8h2r1329y0i2gkkkav8v3ws913yjpzr")))

(define-public crate-econf-0.1.3 (c (n "econf") (v "0.1.3") (d (list (d (n "econf-derive") (r "^0.1.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "parse_duration") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "simple_logger") (r "^0.5") (d #t) (k 2)))) (h "090c34jwvk89pyxi0cijnbsm0ajshs542ldz3na1046j3iz06v53")))

(define-public crate-econf-0.1.4 (c (n "econf") (v "0.1.4") (d (list (d (n "econf-derive") (r "^0.1.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "parse_duration") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "simple_logger") (r "^0.5") (d #t) (k 2)))) (h "0h3zh48yrwafzj7mfgzns0b6nqilgmhd9svmamymr9snb2wlqjxp")))

(define-public crate-econf-0.2.0 (c (n "econf") (v "0.2.0") (d (list (d (n "econf-derive") (r "^0.2.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "parse_duration") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "simple_logger") (r "^0.5") (d #t) (k 2)))) (h "0fd8kayhrccam8zfybfvd9va7slv21d7jc734fl0bbz7c28db7sr")))

(define-public crate-econf-0.2.1 (c (n "econf") (v "0.2.1") (d (list (d (n "econf-derive") (r "^0.2.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "parse_duration") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "simple_logger") (r "^0.5") (d #t) (k 2)))) (h "0niil1i5dx67z6wbqxm27f6rkzk03fqgjbas7xfj8yvkvbpkwvy5")))

