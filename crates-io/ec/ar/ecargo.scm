(define-module (crates-io ec ar ecargo) #:use-module (crates-io))

(define-public crate-ecargo-0.1.0 (c (n "ecargo") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "cargo-platform") (r "^0.1.5") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.18.1") (d #t) (k 0)) (d (n "eframe") (r "^0.27.2") (d #t) (k 0)) (d (n "egui-modal") (r "^0.3.6") (d #t) (k 0)) (d (n "slotmap") (r "^1.0.7") (d #t) (k 0)))) (h "1qlqs16r047awb8ghv4dxwz783p8lkifv9xkqfffk340j4av578j")))

