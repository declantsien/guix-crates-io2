(define-module (crates-io ec ds ecdsa_verify) #:use-module (crates-io))

(define-public crate-ecdsa_verify-1.0.0 (c (n "ecdsa_verify") (v "1.0.0") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "pgrx") (r "=0.11") (d #t) (k 0)) (d (n "pgrx-tests") (r "=0.11") (d #t) (k 2)) (d (n "sha2") (r "^0.10") (d #t) (k 0)))) (h "1fg8pjqhh50qy631dk9a744yprpryhdndc8h3fkknp1j02x9kfbf") (f (quote (("pg_test") ("pg16" "pgrx/pg16" "pgrx-tests/pg16") ("pg15" "pgrx/pg15" "pgrx-tests/pg15") ("pg14" "pgrx/pg14" "pgrx-tests/pg14") ("pg13" "pgrx/pg13" "pgrx-tests/pg13") ("pg12" "pgrx/pg12" "pgrx-tests/pg12") ("pg11" "pgrx/pg11" "pgrx-tests/pg11") ("default" "pg16"))))))

(define-public crate-ecdsa_verify-1.1.0 (c (n "ecdsa_verify") (v "1.1.0") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)))) (h "0mx07l3mc9py46m6f99hq29ymbcw1g3xid6qdhc97155kbbjzvdx")))

(define-public crate-ecdsa_verify-1.1.1 (c (n "ecdsa_verify") (v "1.1.1") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)))) (h "0p7f3fg0wc9pf1gxnacnn6kyf4brz3bj5bwva8c7zivn3gp1ibv6")))

