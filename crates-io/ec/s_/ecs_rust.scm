(define-module (crates-io ec s_ ecs_rust) #:use-module (crates-io))

(define-public crate-ecs_rust-0.0.1 (c (n "ecs_rust") (v "0.0.1") (h "03lpmw10m1188qyx1af77nfhwkkrg5bwchdibqvx5izsmb21h57m")))

(define-public crate-ecs_rust-0.0.2 (c (n "ecs_rust") (v "0.0.2") (h "1hp5q4fzd0b4sb0i5ly19l54vazr4sr0xqqb1grg4m26ba96kq88")))

(define-public crate-ecs_rust-0.0.3 (c (n "ecs_rust") (v "0.0.3") (h "1fk4k96dgaac6jdl09v63f4crrhbry1srnp5ac731z35kb5h2vjg")))

(define-public crate-ecs_rust-0.0.4 (c (n "ecs_rust") (v "0.0.4") (h "0mg8ic7yd2byb880qz3r11ihaq8cxcvj5gk8p85vmzq2q4wpmcym")))

