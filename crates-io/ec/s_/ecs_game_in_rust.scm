(define-module (crates-io ec s_ ecs_game_in_rust) #:use-module (crates-io))

(define-public crate-ecs_game_in_rust-0.1.0 (c (n "ecs_game_in_rust") (v "0.1.0") (d (list (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "0rzl02krlsq01vpw7qg377lb4lmxz4zhq3lfc1y1pivfa9hzpyc4")))

(define-public crate-ecs_game_in_rust-0.1.1 (c (n "ecs_game_in_rust") (v "0.1.1") (d (list (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "1wd24x6xwlba8ix64mszzpq23gp2nb9n5dlxd2dz9zdnnnsb8sv8")))

(define-public crate-ecs_game_in_rust-0.1.2 (c (n "ecs_game_in_rust") (v "0.1.2") (d (list (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "0df57impw41bakniwiklgz87k92kdplcadw78sxyqz17jf41b77i")))

