(define-module (crates-io ec od ecode) #:use-module (crates-io))

(define-public crate-ecode-0.1.0 (c (n "ecode") (v "0.1.0") (d (list (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.17") (d #t) (k 0)))) (h "1g0i9qyspzkfkgp4yr3rhsdmvz306xnlpbiw0nyayq1n7yrjc6c3")))

(define-public crate-ecode-0.1.1 (c (n "ecode") (v "0.1.1") (d (list (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.17") (d #t) (k 0)))) (h "1h2dynnfvd1r4bc0dwjs92ai5xf1hajam3jwd8b1l6v1j1kwn26g")))

(define-public crate-ecode-0.1.2 (c (n "ecode") (v "0.1.2") (d (list (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.17") (d #t) (k 0)))) (h "11b13c97nsiwyfzz9b1rmsyjmnbsl3isi4pw5xxqbskzkni0yc0j")))

(define-public crate-ecode-0.1.3 (c (n "ecode") (v "0.1.3") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.17") (d #t) (k 0)))) (h "1hwv5sb79arnn9ysrnq5q82sf94znf7q24jarrbg7jhdgkfapx7k")))

(define-public crate-ecode-0.1.4 (c (n "ecode") (v "0.1.4") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 0)) (d (n "scanln") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.17") (d #t) (k 0)))) (h "00dvp2yqfvvn492my18b426nzhvgkjx4nvlj3f4nghr2kjcp6pna")))

(define-public crate-ecode-0.1.6 (c (n "ecode") (v "0.1.6") (d (list (d (n "clap") (r "^3.0.0-beta.5") (d #t) (k 0)) (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 0)) (d (n "scanln") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.17") (d #t) (k 0)))) (h "00hby5pgjafw9sr9mf487r1pih8qjwi1ywafwbvxqn5vlqya28js")))

(define-public crate-ecode-0.1.7 (c (n "ecode") (v "0.1.7") (d (list (d (n "clap") (r "^3.0.0-rc.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 0)) (d (n "scanln") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.17") (d #t) (k 0)))) (h "1zqhs8w3vgmnr13f2zvxvas86xn7jx16z5fs9i40gq13nnxy9v35")))

(define-public crate-ecode-0.1.8 (c (n "ecode") (v "0.1.8") (d (list (d (n "clap") (r "^3.0.0-rc.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 0)) (d (n "scanln") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.17") (d #t) (k 0)))) (h "0c3985rdkri46sqf544q5i9y9vhzm5ccznbrgc2iipldrf2g9m1v")))

(define-public crate-ecode-0.1.9 (c (n "ecode") (v "0.1.9") (d (list (d (n "clap") (r "^3.0.0-rc.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 0)) (d (n "scanln") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.17") (d #t) (k 0)))) (h "0cl50b8s539m9j8250v657wdafwaqxkcp6hsilic35rjw3wqkqxf")))

(define-public crate-ecode-1.0.0 (c (n "ecode") (v "1.0.0") (d (list (d (n "clap") (r "^3.0.0-rc.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 0)) (d (n "iced") (r "^0.3.0") (d #t) (k 0)) (d (n "scanln") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.17") (d #t) (k 0)))) (h "1blnk4ylpmwkhzlmyqb5w4xjw6xzjklb80q40534f1pj585cxkb6")))

(define-public crate-ecode-1.0.1 (c (n "ecode") (v "1.0.1") (d (list (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 0)) (d (n "iced") (r "^0.4.2") (d #t) (k 0)) (d (n "scanln") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.24") (d #t) (k 0)))) (h "0bcxfdhjq1n8fp4qv3l1zkk506x49dsz97973z5pmv3p8gx3rizh")))

(define-public crate-ecode-1.0.2 (c (n "ecode") (v "1.0.2") (d (list (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 0)) (d (n "iced") (r "^0.4") (f (quote ("glow"))) (d #t) (k 0)) (d (n "scanln") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.24") (d #t) (k 0)))) (h "05vciy7aiib4z7hm567b1canidfk09ck46plaknpz1ax8l17z9g9")))

