(define-module (crates-io ec ma ecma402_traits) #:use-module (crates-io))

(define-public crate-ecma402_traits-0.1.0 (c (n "ecma402_traits") (v "0.1.0") (h "11a11h73qka0canayqb0wg4aspw0xdq64jg8csgr3jgbxapclkxs")))

(define-public crate-ecma402_traits-0.2.0 (c (n "ecma402_traits") (v "0.2.0") (h "1k7vfdqggai42nvkwh1qv1y88cr8qj31q3yr85ia8nm8ikd2vilq")))

(define-public crate-ecma402_traits-1.0.0 (c (n "ecma402_traits") (v "1.0.0") (h "102l6kcn8srjaqlp5khr0hb73bqfgn9ck99bng9mix7y6zjdg6p8")))

(define-public crate-ecma402_traits-1.0.1 (c (n "ecma402_traits") (v "1.0.1") (h "0rg6n66pp3r2h9xkg61rpcl9wv8qsw1sdlf01swy18bq4hqa1czj")))

(define-public crate-ecma402_traits-1.0.3 (c (n "ecma402_traits") (v "1.0.3") (h "0wc9jvi9r1d1kaks33x30n2pgxqdq4in9iwkkiyz0362zh6p9acx")))

(define-public crate-ecma402_traits-2.0.0 (c (n "ecma402_traits") (v "2.0.0") (h "1ldi2zcycbicn55fc7nam1z6j6dqdc8k3w0gd7sdwm9hbhadxmpl")))

(define-public crate-ecma402_traits-2.0.1 (c (n "ecma402_traits") (v "2.0.1") (h "021c3azcp0pdln5lx1nfxw6b8hv8d7q2q7rmg4fmkfmhlg8am2ax")))

(define-public crate-ecma402_traits-2.0.2 (c (n "ecma402_traits") (v "2.0.2") (h "1yw0qhin4zsr2dlvwpl7psmkl8ih6xmzmmnxibgh9n6d2d8wv0g0")))

(define-public crate-ecma402_traits-2.0.3 (c (n "ecma402_traits") (v "2.0.3") (h "0jz0zyzjgy0gaf6xms2ncjihsj8n14bfnnwvb4cl3gbr0hzaqimb")))

(define-public crate-ecma402_traits-3.0.0 (c (n "ecma402_traits") (v "3.0.0") (h "1jgbajxl6g2hciafpqfs9ywqwi31v3hx4njnwrwjvix66j9flvjk")))

(define-public crate-ecma402_traits-4.0.0 (c (n "ecma402_traits") (v "4.0.0") (h "1r04hfd8ff4753l4d826688hmnqb4fz67lnw2ldc8nb2l972l31b")))

(define-public crate-ecma402_traits-4.2.0 (c (n "ecma402_traits") (v "4.2.0") (h "081jwj359flfj1gqk0djhq5jgriy3jhgqwvxxjjcqv9jqkknsg0n")))

(define-public crate-ecma402_traits-4.2.1 (c (n "ecma402_traits") (v "4.2.1") (h "1amind48snkqf47rib27xiad2abfmcl107hjibfafqibbmwdc087")))

(define-public crate-ecma402_traits-4.2.2 (c (n "ecma402_traits") (v "4.2.2") (h "1y5zfhc2y1pcllmp6hwy0h3z5yxm2xclzqqn461rh00dlrfyvgf0")))

(define-public crate-ecma402_traits-4.2.3 (c (n "ecma402_traits") (v "4.2.3") (h "0pm1ccyvrn4cgfk383ff60mw9zlhynqb7wg2p3s91y50cy6z3y8w")))

(define-public crate-ecma402_traits-5.0.0 (c (n "ecma402_traits") (v "5.0.0") (h "18vkcvmxk92xaq7dnx8y3ylg3ihslnpxfl6jmjl3nj1jgb0d5dxx")))

