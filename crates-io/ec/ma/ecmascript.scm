(define-module (crates-io ec ma ecmascript) #:use-module (crates-io))

(define-public crate-ecmascript-0.0.1 (c (n "ecmascript") (v "0.0.1") (d (list (d (n "combine") (r "^3.3.2") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.1") (d #t) (k 0)) (d (n "unicode-xid") (r "^0.1.0") (d #t) (k 0)))) (h "0c5l5xmfkbc6dpip3kqyw3afznn6lb8g29gq3rrkgkf2s74243hz")))

(define-public crate-ecmascript-0.1.0 (c (n "ecmascript") (v "0.1.0") (d (list (d (n "combine") (r "^3.3.2") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.1") (d #t) (k 0)) (d (n "unicode-xid") (r "^0.1.0") (d #t) (k 0)))) (h "0557rnvxb9ay4gg61mqk68fd21kbcvlyjjzqj3gycd8bhsskinj2")))

(define-public crate-ecmascript-0.2.0 (c (n "ecmascript") (v "0.2.0") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "combine") (r "^3.3.2") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "glob") (r "^0.2.11") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.70") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.70") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.22") (d #t) (k 0)) (d (n "unicode-xid") (r "^0.1.0") (d #t) (k 0)))) (h "1zbdc9rq754ffhzrrb4hdmi484cvkiqg4qnfa3la6sb0xmnbbq8i")))

