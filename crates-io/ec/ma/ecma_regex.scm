(define-module (crates-io ec ma ecma_regex) #:use-module (crates-io))

(define-public crate-ecma_regex-0.0.1 (c (n "ecma_regex") (v "0.0.1") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "libregexp-sys") (r "^0.0.2") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "regex") (r "^1.7.1") (d #t) (k 2)))) (h "0fnxrhyfh2xzi1jwclijiis3nfcxnc67zqynjn022rlywjlrfyfp")))

(define-public crate-ecma_regex-0.0.2 (c (n "ecma_regex") (v "0.0.2") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "libregexp-sys") (r "^0.0.2") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "rs_regex") (r "^1.7.1") (d #t) (k 2) (p "regex")))) (h "0i577cs07sxba56wdy1dfj1yqmnqj7jaqn81ivwxd21n3qgi2nhh")))

