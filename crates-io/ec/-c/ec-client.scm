(define-module (crates-io ec -c ec-client) #:use-module (crates-io))

(define-public crate-ec-client-0.1.0 (c (n "ec-client") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.9.18") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)) (d (n "serde") (r "^1.0.90") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11.1") (d #t) (k 0)) (d (n "serde_cbor") (r "^0.9.0") (d #t) (k 0)))) (h "0sqkdxdn3xhggbch1c31q0g0ks8qd2n8brxqclfswy17yq7ivb3w")))

