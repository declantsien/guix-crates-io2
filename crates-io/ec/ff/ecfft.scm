(define-module (crates-io ec ff ecfft) #:use-module (crates-io))

(define-public crate-ecfft-0.1.0 (c (n "ecfft") (v "0.1.0") (d (list (d (n "ark-ff") (r "^0.4") (d #t) (k 0)) (d (n "ark-ff-optimized") (r "^0.4") (d #t) (k 0)) (d (n "ark-poly") (r "^0.4") (d #t) (k 0)) (d (n "ark-serialize") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "num-bigint") (r "^0.4") (d #t) (k 0)) (d (n "num-integer") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rayon") (r "^1.7") (d #t) (k 2)))) (h "0g0yxik3ny14i8zlv73vn15n9a5vzfmx6wdy17pcsd5ayh513gqg")))

