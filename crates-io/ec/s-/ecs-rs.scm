(define-module (crates-io ec s- ecs-rs) #:use-module (crates-io))

(define-public crate-ecs-rs-0.1.0 (c (n "ecs-rs") (v "0.1.0") (h "1dkbs29s6zgmnj67wygfgvpfipcdvsmxc10rsj0w8vfyzb9dw1x1")))

(define-public crate-ecs-rs-0.1.1 (c (n "ecs-rs") (v "0.1.1") (h "0qkwlg9zi650qh5jhx9brhkrcphs5cqwa0l1ih8yrwl4z92nyfpl")))

(define-public crate-ecs-rs-0.1.2 (c (n "ecs-rs") (v "0.1.2") (h "07nx341hlnph9g2p22f55hiv0jj071x1b173y5d7cbqh8pxgljln")))

