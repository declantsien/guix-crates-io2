(define-module (crates-io ec s- ecs-tiny) #:use-module (crates-io))

(define-public crate-ecs-tiny-0.1.0 (c (n "ecs-tiny") (v "0.1.0") (d (list (d (n "ahash") (r "^0.8") (d #t) (k 0)) (d (n "slab") (r "^0.4") (d #t) (k 0)))) (h "12rdfc18b47wcljcai4102kmpshwc4dbqz8gl3ayn3671sw9w3lp")))

(define-public crate-ecs-tiny-0.1.1 (c (n "ecs-tiny") (v "0.1.1") (d (list (d (n "ahash") (r "^0.8") (d #t) (k 0)) (d (n "slab") (r "^0.4") (d #t) (k 0)))) (h "0srmj884vxqry8bwmlhmrsdr4m37dcnan2012m1z9a499qasr4kr")))

