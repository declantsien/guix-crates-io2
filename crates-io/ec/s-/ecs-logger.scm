(define-module (crates-io ec s- ecs-logger) #:use-module (crates-io))

(define-public crate-ecs-logger-1.0.0-rc.1 (c (n "ecs-logger") (v "1.0.0-rc.1") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1dndwp5fn849bfkiirkvj9pizda2p8hgqpfm3292vc2g6ym95wjk")))

(define-public crate-ecs-logger-1.0.0-rc.2 (c (n "ecs-logger") (v "1.0.0-rc.2") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "01hbndnxl40bvw50kwcbdx40s5xhcf70xcxa9sj3q7fdlfdj41f7")))

(define-public crate-ecs-logger-1.0.0 (c (n "ecs-logger") (v "1.0.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "09skin3kw15ydq5ymp8y1qrww4l1axislx44398vfqbxr337wv5v")))

(define-public crate-ecs-logger-1.1.0 (c (n "ecs-logger") (v "1.1.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (f (quote ("preserve_order"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 2)) (d (n "regex") (r "^1") (d #t) (k 2)))) (h "04adasc5l8xga4cy8rqawzl3w4hajkig8lq1llmf7qr1npnazrb4")))

