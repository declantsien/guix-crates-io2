(define-module (crates-io ec s- ecs-lib-rs) #:use-module (crates-io))

(define-public crate-ecs-lib-rs-0.1.0 (c (n "ecs-lib-rs") (v "0.1.0") (d (list (d (n "eyre") (r "^0.6.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.28") (d #t) (k 0)))) (h "0sh6m3ffgh6fz3fnk0w96hm6zxw6klrjq3vhyz1fjx9khni5xasf")))

(define-public crate-ecs-lib-rs-0.1.1 (c (n "ecs-lib-rs") (v "0.1.1") (d (list (d (n "eyre") (r "^0.6.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.28") (d #t) (k 0)))) (h "11caq0jxf55qpzxn1ikp2qrc2anzwmby27m6jh6mrcg8j8wcb9z5")))

