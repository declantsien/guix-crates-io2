(define-module (crates-io ec -d ec-divisors) #:use-module (crates-io))

(define-public crate-ec-divisors-0.1.0 (c (n "ec-divisors") (v "0.1.0") (d (list (d (n "group") (r "^0.13") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 2)) (d (n "pasta_curves") (r "^0.5") (f (quote ("bits" "alloc"))) (k 2)) (d (n "rand_core") (r "^0.6") (k 0)) (d (n "rand_core") (r "^0.6") (f (quote ("getrandom"))) (d #t) (k 2)) (d (n "subtle") (r "^2.4") (k 0)) (d (n "transcript") (r "^0.3") (d #t) (k 0) (p "flexible-transcript")) (d (n "zeroize") (r "^1.5") (f (quote ("zeroize_derive"))) (k 0)))) (h "0v98zc6jw33gvias2aa6srzx446k7gink8facpw0kkdg24i5ygdr")))

