(define-module (crates-io ec li eclipto-rs) #:use-module (crates-io))

(define-public crate-eclipto-rs-1.0.0 (c (n "eclipto-rs") (v "1.0.0") (d (list (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)))) (h "1waw3zd6yq4yanqpijb8hf3in5pyc1ydkin3sqz22x2ig0xv074y")))

(define-public crate-eclipto-rs-1.1.0 (c (n "eclipto-rs") (v "1.1.0") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)))) (h "04i8mp8gv6k24lsym3b2ck8i7anp4qm8fczn4dk1skdclf94lyl3") (y #t)))

