(define-module (crates-io ec li ecli) #:use-module (crates-io))

(define-public crate-ecli-0.1.0 (c (n "ecli") (v "0.1.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "templar") (r "^0.1") (d #t) (k 0)) (d (n "termion") (r "^1.5") (d #t) (k 0)) (d (n "unstructured") (r "^0.3") (d #t) (k 0)))) (h "1693dgxzcs3l910r16gpkn89w0m8ypgnp5j6qrjdaz17ls69w43v")))

(define-public crate-ecli-0.1.1 (c (n "ecli") (v "0.1.1") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "templar") (r "^0.1") (d #t) (k 0)) (d (n "termion") (r "^1.5") (d #t) (k 0)) (d (n "unstructured") (r "^0.3") (d #t) (k 0)))) (h "1hbhwfwl82nq6crd8hy9plg5q39gsxx40bg2mps2bx7m3y0f38yr")))

(define-public crate-ecli-0.2.0 (c (n "ecli") (v "0.2.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "templar") (r "^0.1") (d #t) (k 0)) (d (n "termion") (r "^1.5") (d #t) (k 0)) (d (n "unstructured") (r "^0.3") (d #t) (k 0)))) (h "12i7gzp9093c59385gvkvz5dsb54nyg78d93vbhna2frn3ji8llx")))

