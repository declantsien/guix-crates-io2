(define-module (crates-io ec -g ec-generic) #:use-module (crates-io))

(define-public crate-ec-generic-0.1.0 (c (n "ec-generic") (v "0.1.0") (d (list (d (n "num-bigint") (r "^0.4.3") (d #t) (k 0)))) (h "1g03vr5z0vr19fra703c25bg61bxmkfgvmgvrv9fnd13yi591bfc") (y #t)))

(define-public crate-ec-generic-0.1.1 (c (n "ec-generic") (v "0.1.1") (d (list (d (n "num-bigint") (r "^0.4.3") (d #t) (k 0)))) (h "18x34wgp8qicw9fag5vz40l28cs647a0py6qrcr6iywxkb07q3fs") (y #t)))

(define-public crate-ec-generic-0.1.2 (c (n "ec-generic") (v "0.1.2") (d (list (d (n "num-bigint") (r "^0.4.3") (d #t) (k 0)))) (h "0m13x2p5jhz78frnajgcvnvkr5rl1f3p2v63mkpb66cn9g2x0zvd") (y #t)))

(define-public crate-ec-generic-0.1.3 (c (n "ec-generic") (v "0.1.3") (d (list (d (n "num-bigint") (r "^0.4.3") (d #t) (k 0)))) (h "1jv7545bmv6ihbxiy0s614fcz0dmpbfwfcgvkj9xl6pvriipwqfx") (y #t)))

(define-public crate-ec-generic-0.1.4 (c (n "ec-generic") (v "0.1.4") (d (list (d (n "num-bigint") (r "^0.4.3") (d #t) (k 0)))) (h "1944lv2ndddbski48inb3dj3d8wpdixaskqbj86z7jpvh1gxp5n3") (y #t)))

(define-public crate-ec-generic-0.1.5 (c (n "ec-generic") (v "0.1.5") (d (list (d (n "num-bigint") (r "^0.4.3") (d #t) (k 0)))) (h "1szs4an5n8024mhvg2dsfqfj0a7fv4nyyg6lflgfx8asw17jzi87") (y #t)))

(define-public crate-ec-generic-0.1.6 (c (n "ec-generic") (v "0.1.6") (d (list (d (n "num-bigint") (r "^0.4.3") (d #t) (k 0)))) (h "0awwpsnb8ndysmihni8g28bywbwfv6x3g0bxprmd9ar1xx2rn330") (y #t)))

(define-public crate-ec-generic-0.1.7 (c (n "ec-generic") (v "0.1.7") (d (list (d (n "num-bigint") (r "^0.4.3") (d #t) (k 0)))) (h "1dfas5cj8zw838s3jjvfsvzgq0kynryqbd6mpr0zbk6pp74z1x71") (y #t)))

(define-public crate-ec-generic-0.1.8 (c (n "ec-generic") (v "0.1.8") (d (list (d (n "num-bigint") (r "^0.4.3") (d #t) (k 0)))) (h "0dnw1x89rzav7i20gz0mjwv1frd88d3ir8r21f5dmf6wd43531if") (y #t)))

(define-public crate-ec-generic-0.1.9 (c (n "ec-generic") (v "0.1.9") (d (list (d (n "num-bigint") (r "^0.4.3") (d #t) (k 0)))) (h "0lnmh9yf3vdgkm6ryf8ywlq2jzkpz30s834fgdz4n89a2jdam5zb") (y #t)))

(define-public crate-ec-generic-0.1.10 (c (n "ec-generic") (v "0.1.10") (d (list (d (n "num-bigint") (r "^0.4.3") (d #t) (k 0)))) (h "1gxzyblf5z1r51wrfmnaggyim4qwykby6i4zwgj3bsp2nn9a0nfs")))

(define-public crate-ec-generic-0.1.11 (c (n "ec-generic") (v "0.1.11") (d (list (d (n "num-bigint") (r "^0.4.3") (d #t) (k 0)))) (h "0wc3qnbvlm9mpq7niw4fa703cca3nwvmspdfbwyxixvb3052sxwh") (y #t)))

(define-public crate-ec-generic-0.1.12 (c (n "ec-generic") (v "0.1.12") (d (list (d (n "num-bigint") (r "^0.4.3") (d #t) (k 0)))) (h "0ranpf4ryszc52d23dhw3qgwkxrnx4h56gz2jh12h1rklywdqwvb") (y #t)))

(define-public crate-ec-generic-0.1.13 (c (n "ec-generic") (v "0.1.13") (d (list (d (n "num-bigint") (r "^0.4.3") (d #t) (k 0)))) (h "1qm5h28dq2n7fr00nf0h0q1cilyhm861yrydq2d0azfd7pqf7w1w") (y #t)))

(define-public crate-ec-generic-0.1.14 (c (n "ec-generic") (v "0.1.14") (d (list (d (n "num-bigint") (r "^0.4.3") (d #t) (k 0)))) (h "0kpgfp5wb8jbp8akjigs378rwibzkdgpb724hv5nvm2y6vsb0lnx")))

