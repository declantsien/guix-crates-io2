(define-module (crates-io ec -g ec-gpu) #:use-module (crates-io))

(define-public crate-ec-gpu-0.1.0 (c (n "ec-gpu") (v "0.1.0") (h "0bis6klqmh72gsr4bvhcismfv0v5n3q0p7kkik6dm5gfyx6fdwbq")))

(define-public crate-ec-gpu-0.2.0 (c (n "ec-gpu") (v "0.2.0") (h "0iaykwg0xynd78dm55s392aqsqc7bldr9hfp92ma37mmw8nmhqxx")))

