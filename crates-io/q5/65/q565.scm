(define-module (crates-io q5 #{65}# q565) #:use-module (crates-io))

(define-public crate-q565-0.1.0 (c (n "q565") (v "0.1.0") (d (list (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "png") (r "^0.17.7") (d #t) (k 2)))) (h "1wnqxbcnp88kmbspifhky4b3qdzm0ny0jf5wb663lj21a1hjcd2r") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-q565-0.2.0 (c (n "q565") (v "0.2.0") (d (list (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "png") (r "^0.17.8") (d #t) (k 2)) (d (n "snafu") (r "^0.7.4") (f (quote ("rust_1_61"))) (k 0)))) (h "0fbw6wh57s56fwvklzd5ilrfdgrxrjlvvjvwk30sga5v5v5wq64f") (f (quote (("std" "alloc" "snafu/std") ("default" "std") ("alloc"))))))

(define-public crate-q565-0.3.0 (c (n "q565") (v "0.3.0") (d (list (d (n "byteorder") (r "^1.4.3") (k 0)) (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "png") (r "^0.17.8") (d #t) (k 2)) (d (n "snafu") (r "^0.7.4") (f (quote ("rust_1_61"))) (k 0)))) (h "0l3pb4mwb969q6sl05lnxiar0828k2d0d1a08xb07794zjlr6hrr") (f (quote (("std" "alloc" "snafu/std") ("default" "std") ("alloc"))))))

(define-public crate-q565-0.4.0 (c (n "q565") (v "0.4.0") (d (list (d (n "byteorder") (r "^1.4") (k 0)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "image") (r "^0.24.6") (f (quote ("png" "webp"))) (k 2)) (d (n "itertools") (r "^0.10") (k 0)) (d (n "snafu") (r "^0.7.4") (f (quote ("rust_1_61"))) (k 0)))) (h "0k6yjdcx6035ak90s1w91yw2ian25821lw8rvzxalb6sr2hvagmk") (f (quote (("std" "alloc" "snafu/std") ("default" "std") ("alloc"))))))

