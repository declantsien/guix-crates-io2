(define-module (crates-io i2 c- i2c-multiplexer) #:use-module (crates-io))

(define-public crate-i2c-multiplexer-0.1.0 (c (n "i2c-multiplexer") (v "0.1.0") (d (list (d (n "crc") (r "^3.0.0") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)) (d (n "embedded-svc") (r "^0.22.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.9.0") (d #t) (k 2)) (d (n "rstest") (r "^0.16.0") (d #t) (k 2)))) (h "0ixk4448v787k16n8ps9l54rf6h15jpf6f14zjvssvyvrz60aj4x")))

(define-public crate-i2c-multiplexer-0.1.1 (c (n "i2c-multiplexer") (v "0.1.1") (d (list (d (n "crc") (r "^3.0.0") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)) (d (n "embedded-svc") (r "^0.22.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.9.0") (d #t) (k 2)) (d (n "rstest") (r "^0.16.0") (d #t) (k 2)) (d (n "shared-bus") (r "^0.2.5") (f (quote ("std"))) (d #t) (k 2)))) (h "0nlr9kmm59c6g4faryawf0saiwhak1hqafixjlj94ixjp2cawdka") (f (quote (("default") ("bus"))))))

