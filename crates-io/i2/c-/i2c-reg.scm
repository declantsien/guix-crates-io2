(define-module (crates-io i2 c- i2c-reg) #:use-module (crates-io))

(define-public crate-i2c-reg-0.1.0 (c (n "i2c-reg") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2.2") (d #t) (k 0)) (d (n "i2c-reg-derive") (r "^0.1") (d #t) (k 0)))) (h "1j6yq3117fs84hp417sxj3n23bj0vvn9lc918gjc0ifac3xlcnn5")))

(define-public crate-i2c-reg-0.1.1 (c (n "i2c-reg") (v "0.1.1") (d (list (d (n "embedded-hal") (r "^0.2.2") (d #t) (k 0)) (d (n "i2c-reg-derive") (r "^0.1") (d #t) (k 0)))) (h "19nkhvj1g5qx2260jgnkcj87s8p0i78xcf446rcajxgky45qmvmq")))

(define-public crate-i2c-reg-0.1.2 (c (n "i2c-reg") (v "0.1.2") (d (list (d (n "embedded-hal") (r "^0.2.2") (d #t) (k 0)) (d (n "i2c-reg-derive") (r "^0.1") (d #t) (k 0)))) (h "17dpn2sk4bm6n9qg5m08ia28cvs0falk69ypxj6mgf0kl1jz0zmi")))

