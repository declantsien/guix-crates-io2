(define-module (crates-io i2 c- i2c-hal-tools) #:use-module (crates-io))

(define-public crate-i2c-hal-tools-0.0.1 (c (n "i2c-hal-tools") (v "0.0.1") (d (list (d (n "cast") (r "^0.2.2") (k 0)) (d (n "embedded-hal") (r "^0.2.2") (f (quote ("unproven"))) (d #t) (k 0)))) (h "0pz5z6d264zs6sip0n881kxgrk8wyxcgdaqayjbhwz8lylr5pync")))

(define-public crate-i2c-hal-tools-0.0.2 (c (n "i2c-hal-tools") (v "0.0.2") (d (list (d (n "cast") (r "^0.2.2") (k 0)) (d (n "embedded-hal") (r "^0.2.2") (f (quote ("unproven"))) (d #t) (k 0)))) (h "0xw3z65dvb5x9106hf7fz9721js41lrvr0ym6xnlik47sa5ickyw")))

