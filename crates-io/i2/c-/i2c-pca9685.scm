(define-module (crates-io i2 c- i2c-pca9685) #:use-module (crates-io))

(define-public crate-i2c-pca9685-0.1.0 (c (n "i2c-pca9685") (v "0.1.0") (d (list (d (n "i2cdev") (r "~0.3.1") (d #t) (k 0)))) (h "16zp9yxx1l4f2pvp9jxg81dv38wmi3hy60vws40s8z56qhva0zi7")))

(define-public crate-i2c-pca9685-0.1.2 (c (n "i2c-pca9685") (v "0.1.2") (d (list (d (n "i2cdev") (r "~0.3.1") (d #t) (k 0)))) (h "0bk1gmn46f2820ncvwqkqz9wpkfz5lsdrwss6shfdbkgp2lcz3rm")))

(define-public crate-i2c-pca9685-0.1.4 (c (n "i2c-pca9685") (v "0.1.4") (d (list (d (n "i2cdev") (r "~0.3.1") (d #t) (k 0)))) (h "0g5ahchkn7jfxy74hai849vr13pd5qq8bfmqcr0jhk99v4xrlfng")))

(define-public crate-i2c-pca9685-0.1.6 (c (n "i2c-pca9685") (v "0.1.6") (d (list (d (n "i2cdev") (r "~0.3.1") (d #t) (k 0)))) (h "09jg04r21bazkddy2hd1j2v3dcxlcnp9rb0yvm4v9izzqb5ci1wx")))

(define-public crate-i2c-pca9685-0.1.8 (c (n "i2c-pca9685") (v "0.1.8") (d (list (d (n "i2cdev") (r "~0.3.1") (d #t) (k 0)))) (h "0qvawc1gs2ysbhj2dwdjm2690gzm3c411dgd97p9v95qb65nqzy6")))

