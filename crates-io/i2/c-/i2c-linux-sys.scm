(define-module (crates-io i2 c- i2c-linux-sys) #:use-module (crates-io))

(define-public crate-i2c-linux-sys-0.0.1 (c (n "i2c-linux-sys") (v "0.0.1") (d (list (d (n "bitflags") (r "^1.0.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.2.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.36") (d #t) (k 0)) (d (n "nix") (r "^0.10.0") (d #t) (k 0)))) (h "0dd3ihr0ip5fqh9x339cqzpaq3lgp9hgfwf34kjgvqxn362s8ffv")))

(define-public crate-i2c-linux-sys-0.0.2 (c (n "i2c-linux-sys") (v "0.0.2") (d (list (d (n "bitflags") (r "^1.0.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.2.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.36") (d #t) (k 0)) (d (n "nix") (r "^0.10.0") (d #t) (k 0)))) (h "1804m7rga9ziwinbs902rv2rn00654mlkgwssrlc2pw2a19c5k6z")))

(define-public crate-i2c-linux-sys-0.0.3 (c (n "i2c-linux-sys") (v "0.0.3") (d (list (d (n "bitflags") (r "^1.0.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.2.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.36") (d #t) (k 0)) (d (n "nix") (r "^0.10.0") (d #t) (k 0)))) (h "1ylh70j0c213n5c669ma8lp2gg6vcs08812j9niq8plsa373bmy3")))

(define-public crate-i2c-linux-sys-0.0.4 (c (n "i2c-linux-sys") (v "0.0.4") (d (list (d (n "bitflags") (r "^1.0.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.2.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.36") (d #t) (k 0)) (d (n "nix") (r "^0.10.0") (d #t) (k 0)))) (h "1n5dbks8ayzva4hbdcchw7w61vqqnapqsjnc87bygkcn7xpa5d6g")))

(define-public crate-i2c-linux-sys-0.1.0 (c (n "i2c-linux-sys") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.0.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.2.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.36") (d #t) (k 0)) (d (n "nix") (r "^0.10.0") (d #t) (k 0)))) (h "1nhhp5hxzd51x60aqkbfnx1j2by5yanqxqhwpr0y1ns9r9j2cgrz")))

(define-public crate-i2c-linux-sys-0.2.0 (c (n "i2c-linux-sys") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.0.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.2.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.36") (d #t) (k 0)))) (h "1v3r0ip1568jxhivxg4nr7lc5nap12fc1izlhw68amq1sg9hzdvj")))

(define-public crate-i2c-linux-sys-0.2.1 (c (n "i2c-linux-sys") (v "0.2.1") (d (list (d (n "bitflags") (r "^1.0.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.2.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.36") (d #t) (k 0)))) (h "1y5w7g6sm4fkydcqwfhz1plls22q04xs5lsfvb9j2rh1s070dkam")))

