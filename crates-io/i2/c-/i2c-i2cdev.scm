(define-module (crates-io i2 c- i2c-i2cdev) #:use-module (crates-io))

(define-public crate-i2c-i2cdev-0.0.1 (c (n "i2c-i2cdev") (v "0.0.1") (d (list (d (n "i2c") (r "^0.1.0") (d #t) (k 0)) (d (n "i2cdev") (r "^0.4.0") (d #t) (k 0)))) (h "06bwbny4nhph2miwk8bzaqgnyjhqp6ka2f301f5z8vqahvy3azil")))

