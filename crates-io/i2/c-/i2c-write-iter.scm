(define-module (crates-io i2 c- i2c-write-iter) #:use-module (crates-io))

(define-public crate-i2c-write-iter-1.0.0-rc.1 (c (n "i2c-write-iter") (v "1.0.0-rc.1") (d (list (d (n "embedded-hal") (r "^1.0.0-rc.1") (d #t) (k 0)) (d (n "embedded-hal-async") (r "^1.0.0-rc.1") (o #t) (d #t) (k 0)))) (h "1xq1dl2sbcn02ydk505pc2g7ndyjir1kp7xh3h0qg77s6610i6m6") (f (quote (("async")))) (y #t)))

(define-public crate-i2c-write-iter-1.0.0-rc.1.1 (c (n "i2c-write-iter") (v "1.0.0-rc.1.1") (d (list (d (n "embedded-hal") (r "^1.0.0-rc.1") (d #t) (k 0)) (d (n "embedded-hal-async") (r "^1.0.0-rc.1") (o #t) (d #t) (k 0)))) (h "1bhis8fg0c0dhngi6wfjyzm6wa4fv6ilmcfy4bd9c2dw5v78dpas") (f (quote (("async" "embedded-hal-async")))) (y #t)))

(define-public crate-i2c-write-iter-1.0.0-rc.1.2 (c (n "i2c-write-iter") (v "1.0.0-rc.1.2") (d (list (d (n "embedded-hal") (r "^1.0.0-rc.1") (d #t) (k 0)) (d (n "embedded-hal-async") (r "^1.0.0-rc.1") (o #t) (d #t) (k 0)))) (h "12hpzd92sf07h3g68203lb1bzds3wc0yabxv238x784myrq9ja59") (f (quote (("async" "embedded-hal-async")))) (y #t)))

(define-public crate-i2c-write-iter-1.0.0-rc.1.3 (c (n "i2c-write-iter") (v "1.0.0-rc.1.3") (d (list (d (n "embedded-hal") (r "^1.0.0-rc.1") (d #t) (k 0)) (d (n "embedded-hal-async") (r "^1.0.0-rc.1") (o #t) (d #t) (k 0)))) (h "1cg3l1waqgncvram5s04w3ah0f5cajayrazs0gp1nys3sb0vjlxb") (f (quote (("async" "embedded-hal-async"))))))

(define-public crate-i2c-write-iter-1.0.0-rc.1.4 (c (n "i2c-write-iter") (v "1.0.0-rc.1.4") (d (list (d (n "embedded-hal") (r "^1.0.0-rc.3") (d #t) (k 0)) (d (n "embedded-hal-async") (r "^1.0.0-rc.3") (o #t) (d #t) (k 0)))) (h "1zy7n813jj25k0vlfs4hhqhpw8g6giq3c583w74hjvnbrnaj4r76") (f (quote (("async" "embedded-hal-async"))))))

(define-public crate-i2c-write-iter-1.0.0 (c (n "i2c-write-iter") (v "1.0.0") (d (list (d (n "embedded-hal") (r "^1.0.0") (d #t) (k 0)) (d (n "embedded-hal-async") (r "^1.0.0") (o #t) (d #t) (k 0)))) (h "02yd24bskf1ndwhd8n9av8kqg6gwlg9qcqxd5bf1bqjmggdazynr") (f (quote (("async" "embedded-hal-async"))))))

