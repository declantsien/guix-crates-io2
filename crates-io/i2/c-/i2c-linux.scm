(define-module (crates-io i2 c- i2c-linux) #:use-module (crates-io))

(define-public crate-i2c-linux-0.0.1 (c (n "i2c-linux") (v "0.0.1") (d (list (d (n "bitflags") (r "^1.0.1") (d #t) (k 0)) (d (n "i2c-linux-sys") (r "^0.0.4") (d #t) (k 0)) (d (n "resize-slice") (r "^0.1.2") (d #t) (k 0)))) (h "0zqqqjbaj4x988sm4frgjxr5k20l3sb43cmzayk58awwzsggjhy1")))

(define-public crate-i2c-linux-0.1.0 (c (n "i2c-linux") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.0.1") (d #t) (k 0)) (d (n "i2c") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "i2c-linux-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "resize-slice") (r "^0.1.2") (d #t) (k 0)) (d (n "udev") (r "^0.2.0") (o #t) (d #t) (k 0)))) (h "1wix9b8ly298mv3g7jv2pav44wy2xilk6w36g97y3xj3dg14kzkx")))

(define-public crate-i2c-linux-0.1.1 (c (n "i2c-linux") (v "0.1.1") (d (list (d (n "bitflags") (r "^1.0.1") (d #t) (k 0)) (d (n "i2c") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "i2c-linux-sys") (r "^0.2.0") (d #t) (k 0)) (d (n "resize-slice") (r "^0.1.2") (d #t) (k 0)) (d (n "udev") (r "^0.2.0") (o #t) (d #t) (k 0)))) (h "1aflwdx75214hxk293f6m7qqyza3dcbj25khwjngzgr9yy5qnpqq")))

(define-public crate-i2c-linux-0.1.2 (c (n "i2c-linux") (v "0.1.2") (d (list (d (n "bitflags") (r "^1.0.1") (d #t) (k 0)) (d (n "i2c") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "i2c-linux-sys") (r "^0.2.0") (d #t) (k 0)) (d (n "resize-slice") (r "^0.1.2") (d #t) (k 0)) (d (n "udev") (r "^0.2.0") (o #t) (d #t) (k 0)))) (h "196jgnw9hpmgiy6cd1qdkgji0iznvjz5x1y2sqhi41xa3a3ql9n0")))

