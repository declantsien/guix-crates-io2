(define-module (crates-io i2 c- i2c-tpa2016) #:use-module (crates-io))

(define-public crate-i2c-tpa2016-0.1.0 (c (n "i2c-tpa2016") (v "0.1.0") (d (list (d (n "i2cdev") (r "~0.3.1") (d #t) (k 0)) (d (n "log") (r "^0.4.2") (d #t) (k 0)) (d (n "skeptic") (r "^0.13") (d #t) (k 1)) (d (n "skeptic") (r "^0.13") (d #t) (k 2)))) (h "1xsn7pvinlivbpf4m7wy2agbpychhxsh8m4yy1x2b669zwk4qc4g")))

