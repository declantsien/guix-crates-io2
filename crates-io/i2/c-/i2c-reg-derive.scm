(define-module (crates-io i2 c- i2c-reg-derive) #:use-module (crates-io))

(define-public crate-i2c-reg-derive-0.1.0 (c (n "i2c-reg-derive") (v "0.1.0") (d (list (d (n "quote") (r "^0.6.11") (d #t) (k 0)) (d (n "syn") (r "^0.15.30") (d #t) (k 0)))) (h "1qrlgv28r2kcz40qbhd1maj6wv51abaqrdv3cg9jh5dbldcrz34c")))

(define-public crate-i2c-reg-derive-0.1.1 (c (n "i2c-reg-derive") (v "0.1.1") (d (list (d (n "quote") (r "^0.6.11") (d #t) (k 0)) (d (n "syn") (r "^0.15.30") (d #t) (k 0)))) (h "065g780p4lz46mgphp9qhfxfm398ir98fm29q1wi14fmnnas6bri")))

(define-public crate-i2c-reg-derive-0.1.2 (c (n "i2c-reg-derive") (v "0.1.2") (d (list (d (n "quote") (r "^0.6.11") (d #t) (k 0)) (d (n "syn") (r "^0.15.30") (d #t) (k 0)))) (h "0g87q8px3z6m41f601hhxr273jg7z5m1bh11xyznrb0lglvgvk8s")))

