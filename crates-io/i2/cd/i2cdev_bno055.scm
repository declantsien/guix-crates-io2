(define-module (crates-io i2 cd i2cdev_bno055) #:use-module (crates-io))

(define-public crate-i2cdev_bno055-0.1.1 (c (n "i2cdev_bno055") (v "0.1.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "i2cdev") (r "^0.3.1") (d #t) (k 0)) (d (n "i2csensors") (r "^0.1.2") (d #t) (k 0)))) (h "1jyahqalfzcd66xqi1fmmk6rk2ish2f8aj35bz2gdd783ngzf1cz")))

(define-public crate-i2cdev_bno055-0.1.2 (c (n "i2cdev_bno055") (v "0.1.2") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "i2cdev") (r "^0.3.1") (d #t) (k 0)) (d (n "i2csensors") (r "^0.1.2") (d #t) (k 0)))) (h "0f495rxz3xx28h243s7wf7bqd0j50p4aj87wbns55z3ckimzxnz8")))

(define-public crate-i2cdev_bno055-0.1.3 (c (n "i2cdev_bno055") (v "0.1.3") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "i2cdev") (r "^0.3.1") (d #t) (k 0)) (d (n "i2csensors") (r "^0.1.2") (d #t) (k 0)))) (h "0qxlw5dl01cpw70359qv4b1n33rn2fricmbhmgxkww04kfcwvwic")))

