(define-module (crates-io i2 cd i2cdev-lsm9ds0) #:use-module (crates-io))

(define-public crate-i2cdev-lsm9ds0-0.1.0 (c (n "i2cdev-lsm9ds0") (v "0.1.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "i2cdev") (r "^0.3.1") (d #t) (k 0)) (d (n "i2csensors") (r "^0.1.0") (d #t) (k 0)))) (h "1yrm4qfj4kf4ikikid81pybl3r1633ikkbvy624lnb9xm2j6cjv2")))

(define-public crate-i2cdev-lsm9ds0-0.1.1 (c (n "i2cdev-lsm9ds0") (v "0.1.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "i2cdev") (r "^0.3.1") (d #t) (k 0)) (d (n "i2csensors") (r "0.1.*") (d #t) (k 0)))) (h "0v4wpvzlv9pird84x67r0dm45zgb81b4vdvmz56ddgia2i8649v4")))

(define-public crate-i2cdev-lsm9ds0-0.1.2 (c (n "i2cdev-lsm9ds0") (v "0.1.2") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "i2cdev") (r "^0.3.1") (d #t) (k 0)) (d (n "i2csensors") (r "0.1.*") (d #t) (k 0)))) (h "1f8af8nxyn28vx2vvjvm4wfvpr4fp2nc2max056fmd3xk537mgv7")))

(define-public crate-i2cdev-lsm9ds0-0.1.3 (c (n "i2cdev-lsm9ds0") (v "0.1.3") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "i2cdev") (r "^0.3.1") (d #t) (k 0)) (d (n "i2csensors") (r "0.1.*") (d #t) (k 0)))) (h "0s7h5z5smvbw9plxal6hfjc4jdzzms9y8zcglcahpw819g9zzn75")))

