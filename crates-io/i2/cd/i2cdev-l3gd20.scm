(define-module (crates-io i2 cd i2cdev-l3gd20) #:use-module (crates-io))

(define-public crate-i2cdev-l3gd20-0.1.0 (c (n "i2cdev-l3gd20") (v "0.1.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "i2cdev") (r "^0.3.1") (d #t) (k 0)) (d (n "i2csensors") (r "^0.1.0") (d #t) (k 0)))) (h "01f2lxqwackmk0c9nb73mjwwsz2zap5angb16x1b3hqdfpmq00qk")))

(define-public crate-i2cdev-l3gd20-0.1.1 (c (n "i2cdev-l3gd20") (v "0.1.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "i2cdev") (r "^0.3.1") (d #t) (k 0)) (d (n "i2csensors") (r "0.1.*") (d #t) (k 0)))) (h "1yn0srzg088d7jkmq2qxy0mr5sbpwhhx3466vi12qf2pry9kgwgz")))

(define-public crate-i2cdev-l3gd20-0.1.2 (c (n "i2cdev-l3gd20") (v "0.1.2") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "i2cdev") (r "^0.3.1") (d #t) (k 0)) (d (n "i2csensors") (r "0.1.*") (d #t) (k 0)))) (h "153a81p203g58i31c8h3jc5zzy4qgalrs7qxr3aqsyxd7rii38sb")))

(define-public crate-i2cdev-l3gd20-0.1.3 (c (n "i2cdev-l3gd20") (v "0.1.3") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "i2cdev") (r "^0.3.1") (d #t) (k 0)) (d (n "i2csensors") (r "0.1.*") (d #t) (k 0)))) (h "1ky3qyv68ikr2girnagzrvcdbd1aml4l0gmb3l2lwxxyw9mwiqmm")))

