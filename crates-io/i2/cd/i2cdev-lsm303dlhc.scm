(define-module (crates-io i2 cd i2cdev-lsm303dlhc) #:use-module (crates-io))

(define-public crate-i2cdev-lsm303dlhc-1.0.0 (c (n "i2cdev-lsm303dlhc") (v "1.0.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "i2cdev") (r "^0.3.1") (d #t) (k 0)) (d (n "i2csensors") (r "0.1.*") (d #t) (k 0)))) (h "1xlhi2a5m42nzy5ppy8ap2ymvxv3c7bfp0dfli2gx3dg3z2x6cy7")))

(define-public crate-i2cdev-lsm303dlhc-1.0.1 (c (n "i2cdev-lsm303dlhc") (v "1.0.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "i2cdev") (r "^0.3.1") (d #t) (k 0)) (d (n "i2csensors") (r "0.1.*") (d #t) (k 0)))) (h "1x466y6wwvnz7b6bn4cz887hncwblzc2rxh9p0w6q4brzrvn0fg4")))

