(define-module (crates-io i2 cd i2cdev-bmp280) #:use-module (crates-io))

(define-public crate-i2cdev-bmp280-0.1.0 (c (n "i2cdev-bmp280") (v "0.1.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "i2cdev") (r "^0.3.1") (d #t) (k 0)) (d (n "i2csensors") (r "^0.1.0") (d #t) (k 0)))) (h "17838i86j3pr3i57arn6y85nphpsn3gc092xh1na2v0s0q8c1pkw")))

(define-public crate-i2cdev-bmp280-0.1.1 (c (n "i2cdev-bmp280") (v "0.1.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "i2cdev") (r "^0.3.1") (d #t) (k 0)) (d (n "i2csensors") (r "^0.1.0") (d #t) (k 0)))) (h "0djxjd3y48cmd1gkvvrk84rxa94hwr9h4icqaa8sfs21mz7svyad")))

(define-public crate-i2cdev-bmp280-0.1.2 (c (n "i2cdev-bmp280") (v "0.1.2") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "i2cdev") (r "^0.3.1") (d #t) (k 0)) (d (n "i2csensors") (r "0.1.*") (d #t) (k 0)))) (h "17zl2y5ic93qgxj5f0wji6j9wfzw77f52ycyp01y4c26m7fkd1rh")))

(define-public crate-i2cdev-bmp280-0.1.3 (c (n "i2cdev-bmp280") (v "0.1.3") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "i2cdev") (r "^0.3.1") (d #t) (k 0)) (d (n "i2csensors") (r "0.1.*") (d #t) (k 0)))) (h "0r8d43qa0bgzz7j7w96i16l061knmg96i8hhv7y3ihclld9nvg5q")))

(define-public crate-i2cdev-bmp280-0.1.4 (c (n "i2cdev-bmp280") (v "0.1.4") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "i2cdev") (r "^0.3.1") (d #t) (k 0)) (d (n "i2csensors") (r "0.1.*") (d #t) (k 0)))) (h "1gns3pklydm2qvpqky62csrd8rhiw6fvkphdg4akhy6a9rrwgjlr")))

