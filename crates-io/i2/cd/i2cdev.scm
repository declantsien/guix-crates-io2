(define-module (crates-io i2 cd i2cdev) #:use-module (crates-io))

(define-public crate-i2cdev-0.1.0 (c (n "i2cdev") (v "0.1.0") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)) (d (n "byteorder") (r "*") (d #t) (k 0)) (d (n "docopt") (r "*") (d #t) (k 2)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "nix") (r "^0.4") (d #t) (k 0)))) (h "1r215jncxk3v9pgsvp39ddzq4230iyhzk3m3ap9jzdvh3b8naf52")))

(define-public crate-i2cdev-0.2.0 (c (n "i2cdev") (v "0.2.0") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)) (d (n "byteorder") (r "*") (d #t) (k 0)) (d (n "docopt") (r "*") (d #t) (k 2)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "nix") (r "^0.4") (d #t) (k 0)))) (h "19d66z79i9960i7bdgr3fh4n6fjpzf22waj3fxpgigrw12rf08g0")))

(define-public crate-i2cdev-0.3.0 (c (n "i2cdev") (v "0.3.0") (d (list (d (n "bitflags") (r "^0.5.0") (d #t) (k 0)) (d (n "byteorder") (r "^0.4.2") (d #t) (k 0)) (d (n "docopt") (r "^0.6.78") (d #t) (k 2)) (d (n "libc") (r "^0.2.7") (d #t) (k 0)) (d (n "nix") (r "^0.5.0") (d #t) (k 0)))) (h "1a892f2y5ibl4s5mbv5chj7525g8432avrbl3jgysp59qlcaw68j")))

(define-public crate-i2cdev-0.3.1 (c (n "i2cdev") (v "0.3.1") (d (list (d (n "bitflags") (r "^0.5.0") (d #t) (k 0)) (d (n "byteorder") (r "^0.4.2") (d #t) (k 0)) (d (n "docopt") (r "^0.6.78") (d #t) (k 2)) (d (n "libc") (r "^0.2.7") (d #t) (k 0)) (d (n "nix") (r "^0.6.0") (d #t) (k 0)) (d (n "skeptic") (r "^0.5") (d #t) (k 1)) (d (n "skeptic") (r "^0.5") (d #t) (k 2)))) (h "1z78hvicpxd2x8pidsk9h1c5zzcjw1qz2445zqklmj9bf8p2s81d")))

(define-public crate-i2cdev-0.3.2 (c (n "i2cdev") (v "0.3.2") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "docopt") (r "^0.8") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.10") (d #t) (k 0)) (d (n "skeptic") (r "^0.13") (d #t) (k 1)) (d (n "skeptic") (r "^0.13") (d #t) (k 2)))) (h "13in4rzhs2a2j8qv8lvaf50vkcczxmv3yspb1fps4vh5fr61c41w")))

(define-public crate-i2cdev-0.4.0 (c (n "i2cdev") (v "0.4.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "docopt") (r "^0.8") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.10") (d #t) (k 0)) (d (n "skeptic") (r "^0.13") (d #t) (k 1)) (d (n "skeptic") (r "^0.13") (d #t) (k 2)))) (h "0add1cii2a270f9sm1g98anqlkqd6hp4bawnpg4gxps6ij61vx87")))

(define-public crate-i2cdev-0.4.1 (c (n "i2cdev") (v "0.4.1") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "docopt") (r "^0.8") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.11") (d #t) (k 0)))) (h "0p1jzsijknx5az617mk84w31z8834yd0chk3z8h3p5vc3kfviarx")))

(define-public crate-i2cdev-0.4.2 (c (n "i2cdev") (v "0.4.2") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "docopt") (r "^0.8") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.14") (d #t) (k 0)))) (h "1ng3wfbbdaz5ah0q58kapd1sinsqm8s04hs4rfx344ci00sz0z6v")))

(define-public crate-i2cdev-0.4.3 (c (n "i2cdev") (v "0.4.3") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "docopt") (r "^1") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.14") (d #t) (k 0)))) (h "17sqi7x3045svpi0xmqqf4chfxs1hbm27swvsxxc1sg9hbym31ww")))

(define-public crate-i2cdev-0.4.4 (c (n "i2cdev") (v "0.4.4") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "docopt") (r "^1") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.14") (d #t) (k 0)))) (h "00shbf44rxmi1r4kmmdrp9247r0400bkjd72ir8c4bdhnvcv63iw")))

(define-public crate-i2cdev-0.5.0 (c (n "i2cdev") (v "0.5.0") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "docopt") (r "^1") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.22") (d #t) (k 0)))) (h "00qgsridkdayk56w8p6rmd7zp8ndnfjr268ydd4ps72kswxdssma")))

(define-public crate-i2cdev-0.5.1 (c (n "i2cdev") (v "0.5.1") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "docopt") (r "^1") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.23") (d #t) (k 0)))) (h "0n4kydva33jl1vxcgz25dqyfsgpn1nzk3lagwpnqircw3qs63zmq")))

(define-public crate-i2cdev-0.6.0 (c (n "i2cdev") (v "0.6.0") (d (list (d (n "bitflags") (r "^2.3") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "docopt") (r "^1") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.26.2") (f (quote ("ioctl"))) (k 0)))) (h "0p5k5x4bja0ar45xc8bs56ncgqa99w94i7ylcwzgph7bik85czsr")))

