(define-module (crates-io i2 cd i2cdev-bmp180) #:use-module (crates-io))

(define-public crate-i2cdev-bmp180-0.1.0 (c (n "i2cdev-bmp180") (v "0.1.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "i2cdev") (r "^0.3.1") (d #t) (k 0)) (d (n "i2csensors") (r "^0.1.0") (d #t) (k 0)))) (h "1x1j6m7wlnzg2kv3rm1dwqs90kzx1qkqs62ixbc06qlhyxbb7qfz")))

