(define-module (crates-io i2 cd i2cdev-lsm303d) #:use-module (crates-io))

(define-public crate-i2cdev-lsm303d-0.1.0 (c (n "i2cdev-lsm303d") (v "0.1.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "i2cdev") (r "^0.3.1") (d #t) (k 0)) (d (n "i2csensors") (r "^0.1.0") (d #t) (k 0)))) (h "1kvsz6pwid6jqh7gqgjnjhym6rrf2avm8cnksjfkgivl7cssa7hz")))

(define-public crate-i2cdev-lsm303d-0.1.1 (c (n "i2cdev-lsm303d") (v "0.1.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "i2cdev") (r "^0.3.1") (d #t) (k 0)) (d (n "i2csensors") (r "0.1.*") (d #t) (k 0)))) (h "1ghddiwn6ldm6mw7qm7pb5mfnqv5s9b61hkmkvzghrsi1kd78ivb")))

(define-public crate-i2cdev-lsm303d-0.1.2 (c (n "i2cdev-lsm303d") (v "0.1.2") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "i2cdev") (r "^0.3.1") (d #t) (k 0)) (d (n "i2csensors") (r "0.1.*") (d #t) (k 0)))) (h "11pcm95x82xxhcvy2ay47dq687zjk21848dnhnr8xbx036px1s53")))

