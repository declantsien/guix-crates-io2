(define-module (crates-io i2 cd i2cdev2) #:use-module (crates-io))

(define-public crate-i2cdev2-0.4.0 (c (n "i2cdev2") (v "0.4.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "docopt") (r "^0.8") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.11") (d #t) (k 0)) (d (n "skeptic") (r "^0.13") (d #t) (k 1)) (d (n "skeptic") (r "^0.13") (d #t) (k 2)))) (h "0n6gdciz8vjpsgsfm24hhwxa0wp5yfdhmassn6d7ddfvs82ckpj8")))

