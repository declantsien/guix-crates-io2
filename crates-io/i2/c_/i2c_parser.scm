(define-module (crates-io i2 c_ i2c_parser) #:use-module (crates-io))

(define-public crate-i2c_parser-0.1.0 (c (n "i2c_parser") (v "0.1.0") (h "04bkj23qk3bwjai3w9n0jkb10702racxdb16gyinsq7b4sza8l3d")))

(define-public crate-i2c_parser-0.1.1 (c (n "i2c_parser") (v "0.1.1") (h "148lnw0fyqzvg2bf2rwcig9iqrhcnr96w13lzc18d7r67k3v2w48")))

(define-public crate-i2c_parser-0.1.2 (c (n "i2c_parser") (v "0.1.2") (h "1vm80fjnh5awc1f4bnybfyqn5pdls1rkgha6j4ghi38jgfxgrj45")))

(define-public crate-i2c_parser-0.1.3 (c (n "i2c_parser") (v "0.1.3") (h "0vdb1zfxf3krkzv13np58qg5sc2z6yibr4aqz4128g4lycbdbf35")))

(define-public crate-i2c_parser-0.1.4 (c (n "i2c_parser") (v "0.1.4") (h "15dizb5rrzz6f3cnjhhvdmylni7b37pagi2yfx1bs66v4zirs0bl")))

(define-public crate-i2c_parser-0.1.5 (c (n "i2c_parser") (v "0.1.5") (h "1q2kk7hdh53kpd0585d6gir440z94kw48hk1rp213n4xk7gm3jl2")))

(define-public crate-i2c_parser-0.1.6 (c (n "i2c_parser") (v "0.1.6") (h "0dpi16diy5mlj001rr5dwv2jpcbbfrjimx0yqk44bahdiwwdxsa3")))

