(define-module (crates-io i2 cs i2csensors) #:use-module (crates-io))

(define-public crate-i2csensors-0.1.0 (c (n "i2csensors") (v "0.1.0") (h "07bmcn9pc4f7n102ll3n9c192z12zfxdw5qw07arfwx3plvjlbyf")))

(define-public crate-i2csensors-0.1.1 (c (n "i2csensors") (v "0.1.1") (h "17jmh7zrkpw84zs6arp6hzj5as9dzlm3i5w36m2wx7agixsamn8v")))

(define-public crate-i2csensors-0.1.2 (c (n "i2csensors") (v "0.1.2") (h "0fiibj2xinm9h17cnaphd8hgj1hq0q0cvfacv119ii5mpcvhkh4p")))

(define-public crate-i2csensors-0.1.3 (c (n "i2csensors") (v "0.1.3") (h "1mb9hax7ahxj3qxg217nk1p0cmahggdi7yppa74fw8099y6pll55")))

