(define-module (crates-io i2 a- i2a-rs) #:use-module (crates-io))

(define-public crate-i2a-rs-0.1.0 (c (n "i2a-rs") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "crossterm") (r "^0.9.5") (d #t) (k 0)) (d (n "data-encoding") (r "^2.1.2") (d #t) (k 0)) (d (n "image") (r "^0.21.2") (d #t) (k 0)) (d (n "json") (r "^0.11.14") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.7") (d #t) (k 0)) (d (n "ring") (r "^0.14.6") (d #t) (k 0)) (d (n "term_size") (r "^0.3.1") (d #t) (k 0)))) (h "1zffca8rk5mnzxln2yvm7qjimd5qf871b11lvxnxf647gl4z28cg") (f (quote (("gif_codec"))))))

