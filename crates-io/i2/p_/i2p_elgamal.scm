(define-module (crates-io i2 p_ i2p_elgamal) #:use-module (crates-io))

(define-public crate-i2p_elgamal-0.0.0 (c (n "i2p_elgamal") (v "0.0.0") (h "1fasxjgyn1vlhnv9xbkf5zs500a35hwkan47kvim81r0yfyb6rp6")))

(define-public crate-i2p_elgamal-0.1.0 (c (n "i2p_elgamal") (v "0.1.0") (d (list (d (n "data-encoding") (r "^2.1") (d #t) (k 2)) (d (n "lazy_static") (r "^1.1") (d #t) (k 0)) (d (n "num-bigint") (r "^0.2") (f (quote ("rand"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "sha2") (r "^0.7") (d #t) (k 0)))) (h "1ywi0vyw1mm8sca7yp488479za0wlpc2p15i2xnfkqfnzl872syp")))

