(define-module (crates-io sc ut scuttle-proc) #:use-module (crates-io))

(define-public crate-scuttle-proc-0.1.0 (c (n "scuttle-proc") (v "0.1.0") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0ca7jaq6c3qfk1br53r83lkzvzmmqhw4sdfiy1p76fcs4id3pv03") (f (quote (("sol-tightening") ("phasing") ("oracle-term"))))))

(define-public crate-scuttle-proc-0.1.1 (c (n "scuttle-proc") (v "0.1.1") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "06yrd7q6xwida8cdjd86d7b01a3bspvrvsg1xxqfjqlq3nbhgzqr") (f (quote (("sol-tightening") ("phasing") ("limit-conflicts") ("interrupt-oracle"))))))

