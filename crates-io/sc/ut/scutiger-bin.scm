(define-module (crates-io sc ut scutiger-bin) #:use-module (crates-io))

(define-public crate-scutiger-bin-0.1.0 (c (n "scutiger-bin") (v "0.1.0") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "git2") (r "^0.13") (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pcre2") (r "^0.1") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 0)) (d (n "scutiger-core") (r "^0.1.0") (f (quote ("pcre"))) (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "18jj9i8k789wf250irn4q9wq6phnj6ar264q3wg63k9hmwcz7lg8")))

(define-public crate-scutiger-bin-0.2.0 (c (n "scutiger-bin") (v "0.2.0") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "git2") (r "^0.13") (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pcre2") (r "^0.1") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 0)) (d (n "scutiger-core") (r "^0.2.0") (f (quote ("pcre"))) (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "0krsgapgvwq7vpqgahibmgryhcvdw200d1vn230z8a7rcbmbrghp")))

(define-public crate-scutiger-bin-0.3.0 (c (n "scutiger-bin") (v "0.3.0") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "git2") (r "^0.13") (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pcre2") (r "^0.2.3") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 0)) (d (n "scutiger-core") (r "^0.3.0") (f (quote ("pcre"))) (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1q3n84l6ahc2ak26x0aj49prlip2wwcd0dgk7mc2fh5rgs51fism") (r "1.41.1")))

