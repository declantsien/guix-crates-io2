(define-module (crates-io sc ut scutiger-core) #:use-module (crates-io))

(define-public crate-scutiger-core-0.1.0 (c (n "scutiger-core") (v "0.1.0") (d (list (d (n "git2") (r "^0.13") (k 0)) (d (n "pcre2") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "05p5nk6hd0q87n1scyc259yg3w0yd3zh03a8w9vs44xz6k75ji4g") (f (quote (("pcre" "pcre2"))))))

(define-public crate-scutiger-core-0.2.0 (c (n "scutiger-core") (v "0.2.0") (d (list (d (n "git2") (r "^0.13") (k 0)) (d (n "pcre2") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "0vd8a07vv0nin2kz6vqppn9v3iw3ldr22x4sxjdiyz0jiyy2g3n0") (f (quote (("pcre" "pcre2"))))))

(define-public crate-scutiger-core-0.3.0 (c (n "scutiger-core") (v "0.3.0") (d (list (d (n "git2") (r "^0.13") (k 0)) (d (n "pcre2") (r "^0.2.3") (o #t) (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1raj7arbg42byi23fjljhh2b9zkr0kvihy8v2wlf8cyp49q5xx89") (f (quote (("pcre" "pcre2")))) (r "1.41.1")))

