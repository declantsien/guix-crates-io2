(define-module (crates-io sc ut scut) #:use-module (crates-io))

(define-public crate-scut-1.0.0 (c (n "scut") (v "1.0.0") (d (list (d (n "argh") (r "^0.1.4") (d #t) (k 0)) (d (n "dirs") (r "^3.0.2") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "open") (r "^1.7.0") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)) (d (n "urlencoding") (r "^1.3.3") (d #t) (k 0)))) (h "037gx542mhsljbwaqwsw345qxjv3i6ql67kgaxmcv965r0dhgzm4")))

