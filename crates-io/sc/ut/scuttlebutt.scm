(define-module (crates-io sc ut scuttlebutt) #:use-module (crates-io))

(define-public crate-scuttlebutt-0.1.0 (c (n "scuttlebutt") (v "0.1.0") (d (list (d (n "hyper") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_codegen") (r "^0.8") (d #t) (k 1)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)))) (h "1g78bp0syjw505yfr0ikpa2srac9fzd2p8vbg7idqq0z23y4c9sf")))

(define-public crate-scuttlebutt-0.1.1 (c (n "scuttlebutt") (v "0.1.1") (d (list (d (n "hyper") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_codegen") (r "^0.8") (d #t) (k 1)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)))) (h "1gp6iwvbrsb48zgr23ckyylqd694bnn84kg2lmfjq6xh4q9j5v7j")))

(define-public crate-scuttlebutt-0.2.0 (c (n "scuttlebutt") (v "0.2.0") (d (list (d (n "hyper") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_codegen") (r "^0.8") (d #t) (k 1)) (d (n "serde_derive") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (d #t) (k 0)))) (h "1z8zkk4l038i699aai60pzz9v6vfvjiakq8fq655250017q00rsj")))

