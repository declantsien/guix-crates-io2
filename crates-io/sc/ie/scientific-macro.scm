(define-module (crates-io sc ie scientific-macro) #:use-module (crates-io))

(define-public crate-scientific-macro-0.1.0 (c (n "scientific-macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "scientific") (r "<0.5") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (d #t) (k 0)))) (h "0cj9n14wx6lzil498dsnljkjm0yr8210ls18cmb9fy70fjxqdz2v")))

(define-public crate-scientific-macro-0.5.0 (c (n "scientific-macro") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0.60") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("parsing" "proc-macro"))) (k 0)))) (h "1s7pmsz9wy2wp4dq059gp5h04x4i1lzyr6bh6rb1pycb2n7llfdv")))

(define-public crate-scientific-macro-0.5.2 (c (n "scientific-macro") (v "0.5.2") (d (list (d (n "proc-macro2") (r "^1.0.60") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("parsing" "proc-macro"))) (k 0)))) (h "1lig1i8kf5isl86g1syxg6gzxf1y2vcrq0sxl2zmbdib962livnj") (r "1.65.0")))

