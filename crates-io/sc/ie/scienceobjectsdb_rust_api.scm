(define-module (crates-io sc ie scienceobjectsdb_rust_api) #:use-module (crates-io))

(define-public crate-scienceobjectsdb_rust_api-0.2.0-rc1 (c (n "scienceobjectsdb_rust_api") (v "0.2.0-rc1") (d (list (d (n "prost") (r "^0.9") (d #t) (k 0)) (d (n "prost-derive") (r "^0.9") (d #t) (k 0)) (d (n "prost-types") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tonic") (r "^0.6") (d #t) (k 0)) (d (n "tonic-build") (r "^0.6") (d #t) (k 1)))) (h "0izcgfa8hz6vw0vfnn026z6gws7yywzn6qfg9q4n7qc3rz12ybkp")))

(define-public crate-scienceobjectsdb_rust_api-0.3.0-alpha.1 (c (n "scienceobjectsdb_rust_api") (v "0.3.0-alpha.1") (d (list (d (n "prost") (r "^0") (d #t) (k 0)) (d (n "prost-derive") (r "^0") (d #t) (k 0)) (d (n "prost-types") (r "^0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tonic") (r "^0") (d #t) (k 0)) (d (n "tonic-build") (r "^0") (d #t) (k 1)))) (h "02vzb6l3d6jla9l34j2nqw7qy608gjm1y01fyzf4c805j8c8grm1")))

(define-public crate-scienceobjectsdb_rust_api-0.3.0-alpha.2 (c (n "scienceobjectsdb_rust_api") (v "0.3.0-alpha.2") (d (list (d (n "prost") (r "^0") (d #t) (k 0)) (d (n "prost-derive") (r "^0") (d #t) (k 0)) (d (n "prost-types") (r "^0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tonic") (r "^0") (d #t) (k 0)) (d (n "tonic-build") (r "^0") (d #t) (k 1)))) (h "1vx12pnx0d6h63lshyqir22w3rc8fr2pwqhda359fp79c0zb73r4")))

