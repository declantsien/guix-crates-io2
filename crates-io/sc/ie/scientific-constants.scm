(define-module (crates-io sc ie scientific-constants) #:use-module (crates-io))

(define-public crate-scientific-constants-0.1.0 (c (n "scientific-constants") (v "0.1.0") (h "1x9l1x9x0bfz1iig27m331h9yk5r24463qjdbybkjiji2q7yncad")))

(define-public crate-scientific-constants-0.1.1 (c (n "scientific-constants") (v "0.1.1") (h "1x9g2x290k4wmfc1xhswhdj0364qwxlmvs0xil570zj1d88c8liq")))

