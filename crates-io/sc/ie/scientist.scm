(define-module (crates-io sc ie scientist) #:use-module (crates-io))

(define-public crate-scientist-0.1.0 (c (n "scientist") (v "0.1.0") (h "0b57wdcnw3h8b1fk3pd3rbh4ca4w6wjn4y1sf6k7y3ydqywnmjz5")))

(define-public crate-scientist-0.1.1 (c (n "scientist") (v "0.1.1") (h "1ins8ks8bxfvx02bwj1lyqc05mkkk9qk64rygjwz7h05p9kajbxn")))

(define-public crate-scientist-0.1.2 (c (n "scientist") (v "0.1.2") (h "0232vsd4f81lac0llwvw15nx10ja013pfyr15i08wp0hz95n7kvd")))

