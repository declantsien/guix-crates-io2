(define-module (crates-io sc ie scie_model) #:use-module (crates-io))

(define-public crate-scie_model-0.1.0 (c (n "scie_model") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.115") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1099xzrab066z15y7zjp1391dllmy87h1ybixw00xafkr1c5wfjz")))

