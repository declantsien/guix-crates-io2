(define-module (crates-io sc ie scientisto) #:use-module (crates-io))

(define-public crate-scientisto-0.1.0 (c (n "scientisto") (v "0.1.0") (d (list (d (n "tracing") (r "^0.1.37") (d #t) (k 2)))) (h "0kb496d84k0gm2dmxqnwy9bvsb726kmd98g0vydypsglabz16ng7")))

(define-public crate-scientisto-0.1.1 (c (n "scientisto") (v "0.1.1") (d (list (d (n "tracing") (r "^0.1.37") (d #t) (k 2)))) (h "105bn0g630ffv3bx5diqsk9dsv14gadk5q8mfzdf6kijl4xbqdw2")))

(define-public crate-scientisto-0.1.2 (c (n "scientisto") (v "0.1.2") (d (list (d (n "tracing") (r "^0.1.37") (d #t) (k 2)))) (h "0582vdammrjmd8n518sxf2pl1g4y41f8a4zpwg7y0grn6yqk08ka")))

(define-public crate-scientisto-0.1.3 (c (n "scientisto") (v "0.1.3") (d (list (d (n "tracing") (r "^0.1.37") (d #t) (k 2)))) (h "08zgkhaxklaf9qqd7769q8d3p29nlzlb4lpqrim6h6ic0d08fnzh")))

(define-public crate-scientisto-0.1.4 (c (n "scientisto") (v "0.1.4") (d (list (d (n "tracing") (r "^0.1.37") (d #t) (k 2)))) (h "0wyqp793fcv1hn3fgm4m048qph20qyjdxlrkzhs1b8s797j8cc41")))

(define-public crate-scientisto-0.2.0 (c (n "scientisto") (v "0.2.0") (d (list (d (n "tracing") (r "^0.1.37") (d #t) (k 2)))) (h "03zxy0snbsvw1s1lpdgf11gchjlygrg5lsdzlivlffizcri8gx9p")))

(define-public crate-scientisto-0.3.0 (c (n "scientisto") (v "0.3.0") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 2)))) (h "1arwqb9cbm5v0lzvw2p1z0h9ckqjb4xk38q5fa7z8xacxj0k0wj1")))

