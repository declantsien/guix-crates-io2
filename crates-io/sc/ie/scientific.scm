(define-module (crates-io sc ie scientific) #:use-module (crates-io))

(define-public crate-scientific-0.1.0 (c (n "scientific") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.126") (o #t) (d #t) (k 0)))) (h "073imyj7sxcskkih5xflxx3w4ffmfgrp621kc2wdvxxyd108c22q") (f (quote (("std") ("default") ("debug") ("arc"))))))

(define-public crate-scientific-0.1.2 (c (n "scientific") (v "0.1.2") (d (list (d (n "scientific-macro") (r "<0.5") (d #t) (k 2)) (d (n "serde") (r "^1.0.126") (o #t) (d #t) (k 0)))) (h "197z71d22afgi4k5awbc2pm2qrr9w3g3chh13d2a82v46hhwvslk") (f (quote (("std") ("default") ("debug") ("arc"))))))

(define-public crate-scientific-0.1.3 (c (n "scientific") (v "0.1.3") (d (list (d (n "scientific-macro") (r "<0.5") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0dk1hx4wpirp31k4v4i3ibv76hswclla325cyk4s00ia0cr4fjhr") (f (quote (("std") ("default") ("debug") ("arc"))))))

(define-public crate-scientific-0.2.0 (c (n "scientific") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1v8jmdazxkwxa41ngap9kqqp18iz8y06s5nlyfx21cjx4xw6kwad") (f (quote (("std") ("default") ("debug") ("arc"))))))

(define-public crate-scientific-0.2.1 (c (n "scientific") (v "0.2.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "04bhc7xyq8279fkwqs1grzg34j9n0wsv5xgmv7qvil3w6naljzjd") (f (quote (("std") ("default") ("debug") ("arc"))))))

(define-public crate-scientific-0.2.2 (c (n "scientific") (v "0.2.2") (d (list (d (n "num-integer") (r "^0.1.45") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0im1vzi6qskkn5hqi9bggkszybvq5xbscgbzipv6dwx92pg4ilav") (f (quote (("std") ("default") ("debug") ("arc"))))))

(define-public crate-scientific-0.5.0 (c (n "scientific") (v "0.5.0") (d (list (d (n "num-integer") (r "^0.1.45") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "scientific-macro") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "16kg2f6k2xp5dhzdcgwqfw000280m3ij6wfn57mrxpidfv5z12w5") (f (quote (("std") ("macro" "scientific-macro") ("default" "macro") ("debug") ("arc"))))))

(define-public crate-scientific-0.5.1 (c (n "scientific") (v "0.5.1") (d (list (d (n "num-integer") (r "^0.1.39") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "scientific-macro") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.16") (o #t) (d #t) (k 0)))) (h "0lgnvj3bx5qwbppviqy8kqdghijqvxjbhw9gw4m5did5ms9fy38y") (f (quote (("std") ("macro" "scientific-macro") ("default" "macro") ("debug") ("arc"))))))

(define-public crate-scientific-0.5.2 (c (n "scientific") (v "0.5.2") (d (list (d (n "num-integer") (r "^0.1.39") (d #t) (k 2)) (d (n "postcard") (r "^1.0.8") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "scientific-macro") (r "^0.5.2") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.16") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 2)))) (h "0nczqfc6ngdfcx6z52bncn3jgncb3wd43a6vd0f4az13is5ijlyw") (f (quote (("std") ("macro" "scientific-macro") ("default" "macro") ("debug") ("arc")))) (r "1.65.0")))

