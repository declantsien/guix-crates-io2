(define-module (crates-io sc ie scient) #:use-module (crates-io))

(define-public crate-scient-0.1.0 (c (n "scient") (v "0.1.0") (h "0ximalzysnpnyx1w0j4i6d6qh2f14vskzcv4mkxr6b1gbjf7dmkv") (y #t)))

(define-public crate-scient-0.1.1 (c (n "scient") (v "0.1.1") (d (list (d (n "nom") (r "^7.1.1") (d #t) (k 0)))) (h "1zklbb9ga4jsa1f866pacv2nhvmwnd6kvrfbgvpx29hqcsbpgzwl") (y #t)))

(define-public crate-scient-0.0.10001 (c (n "scient") (v "0.0.10001") (h "06fw202ip4cfc1a6bwwy9sgdqxjqj3m5zgxcn9kkyc8fqmrb2xr8")))

