(define-module (crates-io sc ic scicrypt) #:use-module (crates-io))

(define-public crate-scicrypt-0.0.1 (c (n "scicrypt") (v "0.0.1") (h "14bpl5whir9566mpj9lzbznkxsh2jv5gappmaqgbng7r76bd19ar")))

(define-public crate-scicrypt-0.0.2 (c (n "scicrypt") (v "0.0.2") (d (list (d (n "curve25519-dalek") (r "^3") (d #t) (k 0)) (d (n "rand_core") (r "^0.5") (d #t) (k 0)))) (h "136x73mbiadvbkdyi6ckbpw9v8p05c2rx7xsp39lnpw2rha5g2w6")))

(define-public crate-scicrypt-0.0.3 (c (n "scicrypt") (v "0.0.3") (d (list (d (n "curve25519-dalek") (r "^3") (d #t) (k 0)) (d (n "primal") (r "^0.3") (d #t) (k 2)) (d (n "rand_core") (r "^0.5") (d #t) (k 0)) (d (n "rug") (r "^1.12") (d #t) (k 0)))) (h "1f617yhfll3xjix6vpy72fircx5zziyy2xax3hrl2568gj5xj8f0")))

(define-public crate-scicrypt-0.1.0 (c (n "scicrypt") (v "0.1.0") (d (list (d (n "curve25519-dalek") (r "^3") (d #t) (k 0)) (d (n "primal") (r "^0.3") (d #t) (k 2)) (d (n "rand_core") (r "^0.5") (d #t) (k 0)) (d (n "rug") (r "^1.12") (d #t) (k 0)))) (h "061yv38ckyg22jinrpsfq7xyacrjlsv63lml96mv21m6fvkfs7m1")))

(define-public crate-scicrypt-0.2.0 (c (n "scicrypt") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "curve25519-dalek") (r "^3") (d #t) (k 0)) (d (n "glass_pumpkin") (r "^1.0.0") (d #t) (k 2)) (d (n "primal") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_core") (r "^0.5") (d #t) (k 0)) (d (n "rug") (r "^1.13") (d #t) (k 0)))) (h "07cbwykddjb9620bkvwhba8pp3rhg5yajzdwdz7nayrs435440mm")))

(define-public crate-scicrypt-0.2.2 (c (n "scicrypt") (v "0.2.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "curve25519-dalek") (r "^3") (d #t) (k 0)) (d (n "glass_pumpkin") (r "^1.0.0") (d #t) (k 2)) (d (n "primal") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_core") (r "^0.5") (d #t) (k 0)) (d (n "rug") (r "^1.13") (d #t) (k 0)))) (h "0h1dyzirp91v55kmf0a2ml3fzxvsv1f73ljaajxzyh1v90z0x6cs")))

(define-public crate-scicrypt-0.3.1 (c (n "scicrypt") (v "0.3.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "curve25519-dalek") (r "^3") (d #t) (k 0)) (d (n "glass_pumpkin") (r "^1.0.0") (d #t) (k 2)) (d (n "primal") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_core") (r "^0.5") (d #t) (k 0)) (d (n "rug") (r "^1.13") (d #t) (k 0)))) (h "0wwp7g5fsmfjpslnkmzrdh0vwyxz65k39fl6jn4ycxgdqzjazba9")))

(define-public crate-scicrypt-0.4.0 (c (n "scicrypt") (v "0.4.0") (d (list (d (n "scicrypt-he") (r "^0.4.0") (d #t) (k 0)) (d (n "scicrypt-numbertheory") (r "^0.4.0") (d #t) (k 0)) (d (n "scicrypt-traits") (r "^0.4.0") (d #t) (k 0)))) (h "17zav0hmcmg8yw32arpxnv2fn2cwj5y8sppidhd3jmajbkkypbsv")))

(define-public crate-scicrypt-0.5.0 (c (n "scicrypt") (v "0.5.0") (d (list (d (n "scicrypt-he") (r "^0.5.0") (d #t) (k 0)) (d (n "scicrypt-numbertheory") (r "^0.5.0") (d #t) (k 0)) (d (n "scicrypt-traits") (r "^0.5.0") (d #t) (k 0)))) (h "1aa8kr9q7bkig2cdbhrb0dkc8wd0dpqyba9qb8wagryhvvjb01m9")))

(define-public crate-scicrypt-0.6.0 (c (n "scicrypt") (v "0.6.0") (d (list (d (n "scicrypt-he") (r "^0.6.0") (d #t) (k 0)) (d (n "scicrypt-numbertheory") (r "^0.6.0") (d #t) (k 0)) (d (n "scicrypt-traits") (r "^0.6.0") (d #t) (k 0)))) (h "0c785xs5w690nyqcibvfhjq5iicxf7sfbl7pimhd73lz2xw33bn6")))

(define-public crate-scicrypt-0.6.1 (c (n "scicrypt") (v "0.6.1") (d (list (d (n "scicrypt-he") (r "^0.6.1") (d #t) (k 0)) (d (n "scicrypt-numbertheory") (r "^0.6.1") (d #t) (k 0)) (d (n "scicrypt-traits") (r "^0.6.1") (d #t) (k 0)))) (h "0rw7963j90pr93xcp78xrd9hg26pby31pzrz4i7d201slg6jsyr6")))

(define-public crate-scicrypt-0.6.2 (c (n "scicrypt") (v "0.6.2") (d (list (d (n "scicrypt-he") (r "^0.6.2") (d #t) (k 0)) (d (n "scicrypt-numbertheory") (r "^0.6.2") (d #t) (k 0)) (d (n "scicrypt-traits") (r "^0.6.2") (d #t) (k 0)))) (h "0cgldhjs1wxghc1j8wnrcn77b5yxwmdjmfrfipv4j82n6jah6h1x")))

(define-public crate-scicrypt-0.7.0 (c (n "scicrypt") (v "0.7.0") (d (list (d (n "scicrypt-bigint") (r "^0.7.0") (d #t) (k 0)) (d (n "scicrypt-he") (r "^0.7.0") (d #t) (k 0)) (d (n "scicrypt-numbertheory") (r "^0.7.0") (d #t) (k 0)) (d (n "scicrypt-traits") (r "^0.7.0") (d #t) (k 0)))) (h "1rdfvwdnlwyryz5b15amg2ws2338xq8cbmdn11k2bisqn77jdjdy")))

(define-public crate-scicrypt-0.7.1 (c (n "scicrypt") (v "0.7.1") (d (list (d (n "scicrypt-bigint") (r "^0.7.1") (d #t) (k 0)) (d (n "scicrypt-he") (r "^0.7.1") (d #t) (k 0)) (d (n "scicrypt-numbertheory") (r "^0.7.1") (d #t) (k 0)) (d (n "scicrypt-traits") (r "^0.7.1") (d #t) (k 0)))) (h "0y8lp6mqx40zyfc7chcrkqc5p1frdy0rbif98g1p09fp74k6kmvz")))

