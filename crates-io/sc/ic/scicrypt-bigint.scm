(define-module (crates-io sc ic scicrypt-bigint) #:use-module (crates-io))

(define-public crate-scicrypt-bigint-0.7.0 (c (n "scicrypt-bigint") (v "0.7.0") (d (list (d (n "gmp-mpfr-sys") (r "^1.4") (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rug") (r "^1.13") (f (quote ("integer" "rand" "serde"))) (o #t) (k 0)) (d (n "scicrypt-traits") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0xzxv4cpp3criykp2qwlxwx6mqypwf2cqxq8gn3p8vxh82ijkx09") (f (quote (("default" "rug")))) (s 2) (e (quote (("rug" "dep:rug"))))))

(define-public crate-scicrypt-bigint-0.7.1 (c (n "scicrypt-bigint") (v "0.7.1") (d (list (d (n "gmp-mpfr-sys") (r "^1.4") (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rug") (r "^1.13") (f (quote ("integer" "rand" "serde"))) (o #t) (k 0)) (d (n "scicrypt-traits") (r "^0.7.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0y514161g0a9rjpm5nvfrvk9il7jjjsc7dg38qa2908hmvrq1hzw") (f (quote (("default" "rug")))) (s 2) (e (quote (("rug" "dep:rug"))))))

