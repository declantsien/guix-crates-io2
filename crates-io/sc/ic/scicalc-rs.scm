(define-module (crates-io sc ic scicalc-rs) #:use-module (crates-io))

(define-public crate-scicalc-rs-0.1.0 (c (n "scicalc-rs") (v "0.1.0") (d (list (d (n "float-cmp") (r "^0.8.0") (d #t) (k 0)))) (h "1bl503fnvlnx2w7jfj538c1w623wrngq1avzhlz5jngv715i1qi9")))

(define-public crate-scicalc-rs-0.2.0 (c (n "scicalc-rs") (v "0.2.0") (d (list (d (n "float-cmp") (r "^0.8.0") (d #t) (k 0)))) (h "09493c4zq99chpc81s7jfclriwc5qri3yczvqmjqvbhz0qr0dipd")))

