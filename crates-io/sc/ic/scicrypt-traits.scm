(define-module (crates-io sc ic scicrypt-traits) #:use-module (crates-io))

(define-public crate-scicrypt-traits-0.4.0 (c (n "scicrypt-traits") (v "0.4.0") (d (list (d (n "rand_core") (r "^0.5") (d #t) (k 0)) (d (n "rug") (r "^1.13") (d #t) (k 0)))) (h "1v91hn5lr21s6hywn0qvjw8vm2vnc406g9f64kn95jxqc38nw23q")))

(define-public crate-scicrypt-traits-0.5.0 (c (n "scicrypt-traits") (v "0.5.0") (d (list (d (n "rand_core") (r "^0.6") (d #t) (k 0)) (d (n "rug") (r "^1.13") (d #t) (k 0)))) (h "121yp4znfzxc2i4kxwm326a0pzhnqxf8l9l3xpgznbbqdxp5waqg")))

(define-public crate-scicrypt-traits-0.6.0 (c (n "scicrypt-traits") (v "0.6.0") (d (list (d (n "rand_core") (r "^0.6") (d #t) (k 0)) (d (n "rug") (r "^1.13") (d #t) (k 0)))) (h "0ba45iz4bq5g845mncx2q43px8iqhhyp4nj30yvlab955avjrbi5")))

(define-public crate-scicrypt-traits-0.6.1 (c (n "scicrypt-traits") (v "0.6.1") (d (list (d (n "rand_core") (r "^0.6") (d #t) (k 0)) (d (n "rug") (r "^1.13") (f (quote ("integer" "rand"))) (k 0)))) (h "1h1ki6f01gg2rmxxd99ib5sb021pqlhfddg6xkhb61r01gf7msdr")))

(define-public crate-scicrypt-traits-0.6.2 (c (n "scicrypt-traits") (v "0.6.2") (d (list (d (n "rand_core") (r "^0.6") (d #t) (k 0)) (d (n "rug") (r "^1.13") (f (quote ("integer" "rand"))) (k 0)))) (h "1hd5vwq2rn63w3nj7mafzp3g8gwpfc31my43wxcnwh5ywbwznqpj")))

(define-public crate-scicrypt-traits-0.7.0 (c (n "scicrypt-traits") (v "0.7.0") (d (list (d (n "rand_core") (r "^0.6") (d #t) (k 0)) (d (n "rug") (r "^1.13") (f (quote ("integer" "rand"))) (k 0)))) (h "0m8aqzj3cyzg44nx503zk5wds05lyccvxw6vwcv6l91ncpghxhy8")))

(define-public crate-scicrypt-traits-0.7.1 (c (n "scicrypt-traits") (v "0.7.1") (d (list (d (n "rand_core") (r "^0.6") (d #t) (k 0)) (d (n "rug") (r "^1.13") (f (quote ("integer" "rand"))) (k 0)))) (h "0vnfpd18zq1aayx5nwp0m9szdsnc7wbm0dldvdq2mk8yq2pq536w")))

