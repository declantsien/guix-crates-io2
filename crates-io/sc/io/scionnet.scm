(define-module (crates-io sc io scionnet) #:use-module (crates-io))

(define-public crate-scionnet-0.0.1 (c (n "scionnet") (v "0.0.1") (d (list (d (n "libc") (r "^0.2.151") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1qz558155kmxbaihf6axf6hqdvfynn0hnar8f8rglhb1clf03q06")))

(define-public crate-scionnet-0.0.2 (c (n "scionnet") (v "0.0.2") (d (list (d (n "libc") (r "^0.2.151") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0s382b1kh0r2y6l1j4435jr9qd75sw97k5lmnh776hxlzkkfl15n")))

(define-public crate-scionnet-0.0.3 (c (n "scionnet") (v "0.0.3") (d (list (d (n "libc") (r "^0.2.151") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1wbqsivbk9f84ywyk1bwvfzygkc1d0v1asbmbc8ky5zpblc8vgl6")))

(define-public crate-scionnet-0.0.4 (c (n "scionnet") (v "0.0.4") (d (list (d (n "libc") (r "^0.2.151") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0cl4i36hih4j94p9ksymjb0r90hkr3wl7cxxwm6g78zzpp0fr6f8")))

(define-public crate-scionnet-0.0.5 (c (n "scionnet") (v "0.0.5") (d (list (d (n "libc") (r "^0.2.151") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1lmz6rc78zdlhkhqq974mdpzz0yl0mhb6vgp4jfyxjfzaccpd6nn")))

(define-public crate-scionnet-0.0.6 (c (n "scionnet") (v "0.0.6") (d (list (d (n "libc") (r "^0.2.151") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "08crlrrwjx51r6v10sz2zxp7lzvlnfgl2f944x4bnbcmd6xdn6lj")))

(define-public crate-scionnet-0.0.7 (c (n "scionnet") (v "0.0.7") (d (list (d (n "libc") (r "^0.2.151") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1v1qayccnc4qhpqicscgsgm0k2dn28g4ppavja9w2cb9xs21iqsj")))

