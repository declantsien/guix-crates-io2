(define-module (crates-io sc oo scoot) #:use-module (crates-io))

(define-public crate-scoot-0.1.0 (c (n "scoot") (v "0.1.0") (h "0kkh5yjm7chxhjcqmwnsnnxmgkprvd4vrxaajnjqjkk0h7c605b9")))

(define-public crate-scoot-0.1.1 (c (n "scoot") (v "0.1.1") (h "0smzj8sfrkkvvzxc6h2mii57wvxj892xppp9k998bqwx4s8x9qay")))

