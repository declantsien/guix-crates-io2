(define-module (crates-io sc oo scoop-find) #:use-module (crates-io))

(define-public crate-scoop-find-0.1.0 (c (n "scoop-find") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0v9pdk4jsf18nhg69r9fvc7jp16x5xm45klmybhmadg66yl39gpn")))

(define-public crate-scoop-find-0.2.0 (c (n "scoop-find") (v "0.2.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "phf") (r "^0.10") (f (quote ("macros"))) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1zbfs5k52zb6a32g41mwj6xrwy1x49vbr9k8ndy3s94bpd27cpm5")))

