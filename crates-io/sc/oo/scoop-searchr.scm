(define-module (crates-io sc oo scoop-searchr) #:use-module (crates-io))

(define-public crate-scoop-searchr-0.1.0 (c (n "scoop-searchr") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "directories") (r "^5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0jrbj4r1330cbg768jvba4n7g914k515i60x3jvp4a3gl9qw3f0m")))

(define-public crate-scoop-searchr-0.2.0 (c (n "scoop-searchr") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "directories") (r "^5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1nymj4f8rp5zji9lmwvf1yjiy30f6m3i3x7yrj5la8z8a6x6qciq")))

(define-public crate-scoop-searchr-0.2.1 (c (n "scoop-searchr") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "directories") (r "^5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0wrj62q473ab6l4akv4sazblnlwnviarn70kkfir52050pr4qcdc")))

