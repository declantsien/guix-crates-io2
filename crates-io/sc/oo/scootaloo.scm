(define-module (crates-io sc oo scootaloo) #:use-module (crates-io))

(define-public crate-scootaloo-0.0.0 (c (n "scootaloo") (v "0.0.0") (h "1a1z34v6js7fs2vfvg6ng4qyx4jmckxfqcwk6rcishw9kv7aww10") (y #t)))

(define-public crate-scootaloo-0.0.1 (c (n "scootaloo") (v "0.0.1") (h "0nhn5aq0sbxfqv2i7qsg0x411ida6lahzzb1sw5chybaf0l7di08") (y #t)))

