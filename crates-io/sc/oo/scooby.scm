(define-module (crates-io sc oo scooby) #:use-module (crates-io))

(define-public crate-scooby-0.1.0 (c (n "scooby") (v "0.1.0") (d (list (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "postgres") (r "^0.19.1") (d #t) (k 2)))) (h "03n1g3j2swgxvsmv2nwzi6r72dbnarwla24mqdm5qf5idvg876b8") (f (quote (("validate-postgres-syntax"))))))

(define-public crate-scooby-0.1.1 (c (n "scooby") (v "0.1.1") (d (list (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "postgres") (r "^0.19.1") (d #t) (k 2)))) (h "0yq7d0lbbf3zqba342x864m0ibdmn1qxcaj948m6dkj7qqq16fkw") (f (quote (("validate-postgres-syntax"))))))

(define-public crate-scooby-0.1.2 (c (n "scooby") (v "0.1.2") (d (list (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "postgres") (r "^0.19.1") (d #t) (k 2)))) (h "0338zfsxi46wl61bmfhaq2x3sgvb1laxazjlzhnfhvhzbqk4mggf") (f (quote (("validate-postgres-syntax"))))))

(define-public crate-scooby-0.2.0 (c (n "scooby") (v "0.2.0") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "postgres") (r "^0.19.1") (d #t) (k 2)))) (h "1v0jx0kvzc50lxj6jls77jq7hwa92378mml407gcixhv4psmlp8a") (f (quote (("validate-postgres-syntax"))))))

(define-public crate-scooby-0.3.0 (c (n "scooby") (v "0.3.0") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "postgres") (r "^0.19.1") (d #t) (k 2)))) (h "1sf80vxp9ili499dg87x7n9w1gqz3bjl3rkspiwnah4vk3jznrmq") (f (quote (("validate-postgres-syntax"))))))

(define-public crate-scooby-0.4.0 (c (n "scooby") (v "0.4.0") (d (list (d (n "postgres") (r "^0.19.1") (d #t) (k 2)))) (h "1icyjiy8b3nb0jj1h90dvxzqs09jjs9gcyp9pyacgw2q6ri033l6") (f (quote (("validate-postgres-syntax"))))))

(define-public crate-scooby-0.5.0 (c (n "scooby") (v "0.5.0") (d (list (d (n "postgres") (r "^0.19.1") (d #t) (k 2)))) (h "014a1fsq2djm3bl1cs0a2kx0vpbbvwy7x5c17shlz1dh5hkdn8xl") (f (quote (("validate-postgres-syntax"))))))

