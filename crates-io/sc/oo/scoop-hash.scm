(define-module (crates-io sc oo scoop-hash) #:use-module (crates-io))

(define-public crate-scoop-hash-0.1.0-alpha.1 (c (n "scoop-hash") (v "0.1.0-alpha.1") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "md-5") (r "^0.10.4") (d #t) (k 2) (p "md-5")) (d (n "md5") (r "^0.7.0") (d #t) (k 2)) (d (n "sha1") (r "^0.10.4") (d #t) (k 2)) (d (n "sha1_smol") (r "^1.0.0") (d #t) (k 2)) (d (n "sha2") (r "^0.10.5") (d #t) (k 2)))) (h "0c3w08vip8nhzv4sszbl7c343g8vznmd3sinfrsrwbkq7646ap4n")))

(define-public crate-scoop-hash-0.1.0-beta.2 (c (n "scoop-hash") (v "0.1.0-beta.2") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "md-5") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "sha1") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "sha2") (r "^0.10") (o #t) (d #t) (k 0)))) (h "0ffggx3idva7fflwyaaam5pkgfsx5wf7n9wvllzbism825ib61gv") (f (quote (("rustcrypto" "md-5" "sha1" "sha2") ("default")))) (r "1.60.0")))

(define-public crate-scoop-hash-0.1.0-beta.3 (c (n "scoop-hash") (v "0.1.0-beta.3") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "md-5") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "sha1") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "sha2") (r "^0.10") (o #t) (d #t) (k 0)))) (h "14rhymicqa0mch6ymkhkph7lx37f0ax45xcnr1mmzmkaiqay6awf") (f (quote (("rustcrypto" "md-5" "sha1" "sha2") ("default")))) (r "1.60.0")))

