(define-module (crates-io sc oo scoop-fsearch) #:use-module (crates-io))

(define-public crate-scoop-fsearch-0.0.0 (c (n "scoop-fsearch") (v "0.0.0") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "threadpool") (r "^1.8.1") (d #t) (k 0)))) (h "1ah5gx2lq2257fw5ivlrnjq4cp754g1xx4kxj20nfmmsgp10qgvx") (r "1.59.0")))

