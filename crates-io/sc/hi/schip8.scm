(define-module (crates-io sc hi schip8) #:use-module (crates-io))

(define-public crate-schip8-0.1.0 (c (n "schip8") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)))) (h "1b4h6ip6r7fnk144vpkb1qhaxgdjp8n63jbbq8gawcq7j5c11c6z")))

