(define-module (crates-io sc ra scratch_file) #:use-module (crates-io))

(define-public crate-scratch_file-0.1.0 (c (n "scratch_file") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "zip") (r "^0.5") (f (quote ("deflate"))) (k 0)))) (h "145ipb6qikvzn1hqrch2hpxw2ar41wi33xd9i6wqif553rb2hp0m")))

