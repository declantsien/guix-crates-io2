(define-module (crates-io sc ra scram-2) #:use-module (crates-io))

(define-public crate-scram-2-0.7.0 (c (n "scram-2") (v "0.7.0") (d (list (d (n "base64") (r "^0.22") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "ring") (r "^0.17.7") (d #t) (k 0)))) (h "0nbkzzszn77w85ab0cjynh0m0svgx46y8135mskja62sfl7gwinp")))

