(define-module (crates-io sc ra scrappy-web-codegen) #:use-module (crates-io))

(define-public crate-scrappy-web-codegen-0.0.1 (c (n "scrappy-web-codegen") (v "0.0.1") (d (list (d (n "futures") (r "^0.3.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "scrappy-rt") (r "^0.0.1") (d #t) (k 2)) (d (n "syn") (r "^1") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "0x7ccpk1jxx30ng2qa45jlm9b9ss2bymzk5dsvzl9ndkrd8ylxqm")))

