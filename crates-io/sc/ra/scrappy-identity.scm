(define-module (crates-io sc ra scrappy-identity) #:use-module (crates-io))

(define-public crate-scrappy-identity-0.0.1 (c (n "scrappy-identity") (v "0.0.1") (d (list (d (n "bytes") (r "^0.5.3") (d #t) (k 2)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "scrappy") (r "^0.0.1") (f (quote ("secure-cookies"))) (k 0)) (d (n "scrappy-http") (r "^0.0.1") (d #t) (k 2)) (d (n "scrappy-rt") (r "^0.0.1") (d #t) (k 2)) (d (n "scrappy-service") (r "^0.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "time") (r "^0.1.42") (d #t) (k 0)))) (h "0vwgfj82bp9zfsd8my61a2zap9m82mb0irzp8l9jwj7p84pgd063")))

