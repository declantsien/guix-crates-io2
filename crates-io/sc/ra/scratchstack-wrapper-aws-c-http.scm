(define-module (crates-io sc ra scratchstack-wrapper-aws-c-http) #:use-module (crates-io))

(define-public crate-scratchstack-wrapper-aws-c-http-0.1.0-alpha.5 (c (n "scratchstack-wrapper-aws-c-http") (v "0.1.0-alpha.5") (d (list (d (n "bindgen") (r "^0.61") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "scratchstack-wrapper-aws-c-common") (r "=0.1.0-alpha.5") (d #t) (k 0)) (d (n "scratchstack-wrapper-aws-c-compression") (r "=0.1.0-alpha.6") (d #t) (k 0)) (d (n "scratchstack-wrapper-aws-c-io") (r "=0.1.0-alpha.5") (d #t) (k 0)))) (h "04g3cfzrzzpy10dfgzlsfipn9jyf597qm8vn5ahflvldlfblqzp9") (l "aws-c-http")))

(define-public crate-scratchstack-wrapper-aws-c-http-0.1.0-alpha.7 (c (n "scratchstack-wrapper-aws-c-http") (v "0.1.0-alpha.7") (d (list (d (n "bindgen") (r "^0.61") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "scratchstack-wrapper-aws-c-common") (r "=0.1.0-alpha.7") (d #t) (k 0)) (d (n "scratchstack-wrapper-aws-c-compression") (r "=0.1.0-alpha.7") (d #t) (k 0)) (d (n "scratchstack-wrapper-aws-c-io") (r "=0.1.0-alpha.7") (d #t) (k 0)))) (h "04y04n2crn0j67bmfaii8idi0spavgdsymx0lq30gazy3rziixl4") (l "aws-c-http")))

