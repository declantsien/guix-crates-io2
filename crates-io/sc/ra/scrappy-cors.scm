(define-module (crates-io sc ra scrappy-cors) #:use-module (crates-io))

(define-public crate-scrappy-cors-0.0.1 (c (n "scrappy-cors") (v "0.0.1") (d (list (d (n "derive_more") (r "^0.99.2") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "scrappy") (r "^0.0.1") (d #t) (k 0)) (d (n "scrappy-rt") (r "^0.0.1") (d #t) (k 2)) (d (n "scrappy-service") (r "^0.0.1") (d #t) (k 0)))) (h "0ijxr4aw9xphy7fy2l0gwz67rgpkcnn2pdgf3zx08s54b5pyyyi0")))

