(define-module (crates-io sc ra scrap) #:use-module (crates-io))

(define-public crate-scrap-0.0.1 (c (n "scrap") (v "0.0.1") (d (list (d (n "block") (r "^0.1") (d #t) (k 0)) (d (n "ioctl-gen") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1q44j7ma7bh8ldvhfkygbr1apnfmvp02iang72b8cr7dwmzwpqdh")))

(define-public crate-scrap-0.0.2 (c (n "scrap") (v "0.0.2") (d (list (d (n "block") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.2") (d #t) (k 0)))) (h "0xf4s2hfwkwk53h59za8apvwjjqvj498lgw2jg21kdrcx3pqskgk")))

(define-public crate-scrap-0.1.0 (c (n "scrap") (v "0.1.0") (d (list (d (n "block") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.2") (d #t) (k 0)))) (h "14p2c6l4bai4yf0nxvhvc7adff8z01s7jwh4lxfnjahdsrw9k3qq")))

(define-public crate-scrap-0.1.1 (c (n "scrap") (v "0.1.1") (d (list (d (n "block") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.2") (d #t) (k 0)))) (h "17avdvw3rli1j50wlwlw82x5v6jgr25mzsliygi8wbak5zsxv0q4")))

(define-public crate-scrap-0.1.2 (c (n "scrap") (v "0.1.2") (d (list (d (n "block") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.2") (d #t) (k 0)))) (h "10bklswdlky8xvs5dlpkadg0npg1fx1ia75nlyyram057dv1n5vr")))

(define-public crate-scrap-0.2.0 (c (n "scrap") (v "0.2.0") (d (list (d (n "block") (r "^0.1") (d #t) (k 0)) (d (n "image") (r "^0.14") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.2") (d #t) (k 0)))) (h "04a82ddmsknvdv6afqbfy5afg2wvhqq770jdaqgmgr5fmcg3qhca")))

(define-public crate-scrap-0.2.1 (c (n "scrap") (v "0.2.1") (d (list (d (n "block") (r "^0.1") (d #t) (k 0)) (d (n "image") (r "^0.14") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.2") (d #t) (k 0)))) (h "1i9is53l6crzsd0rx7k2hfzsc9x85kq6kwg9gw7pdjzllgcmk816")))

(define-public crate-scrap-0.3.0 (c (n "scrap") (v "0.3.0") (d (list (d (n "block") (r "^0.1") (d #t) (k 0)) (d (n "image") (r "^0.14") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.2") (d #t) (k 0)))) (h "1ny9m5zcqihp4wwcgbm5lyzn39fs2r3jky3n5z6zmrz1d7x4f859")))

(define-public crate-scrap-0.3.1 (c (n "scrap") (v "0.3.1") (d (list (d (n "block") (r "^0.1") (d #t) (k 0)) (d (n "image") (r "^0.14") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.2") (d #t) (k 0)))) (h "154z70xm1xsng8xfh85qwh1j6qyybnbv5yw1qbr0xgd3hgfhqn17")))

(define-public crate-scrap-0.3.2 (c (n "scrap") (v "0.3.2") (d (list (d (n "block") (r "^0.1") (d #t) (k 0)) (d (n "image") (r "^0.14") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.2") (d #t) (k 0)))) (h "1kppjhg4gra2i2vzi9w3hbpp27q7gv0a0chah5xp8xp2xmbbn1ym")))

(define-public crate-scrap-0.4.0 (c (n "scrap") (v "0.4.0") (d (list (d (n "block") (r "^0.1") (d #t) (k 0)) (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "image") (r "^0.14") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.2") (d #t) (k 0)))) (h "0cd3xz533wvrhg4sbsbi2qqaydhfzx77lyj92yk2c8svbk0lawqj")))

(define-public crate-scrap-0.5.0 (c (n "scrap") (v "0.5.0") (d (list (d (n "block") (r "^0.1") (d #t) (k 0)) (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "repng") (r "^0.2") (d #t) (k 2)) (d (n "winapi") (r "^0.2") (d #t) (k 0)))) (h "0v0jr62igmy13iz137xy644dmqnivc5zws779nq6b3dxngjybwq4")))

