(define-module (crates-io sc ra scrappy-tracing) #:use-module (crates-io))

(define-public crate-scrappy-tracing-0.0.1 (c (n "scrappy-tracing") (v "0.0.1") (d (list (d (n "futures-util") (r "^0.3.1") (d #t) (k 0)) (d (n "scrappy-rt") (r "^0.0.1") (d #t) (k 2)) (d (n "scrappy-service") (r "^0.0.1") (d #t) (k 0)) (d (n "slab") (r "^0.4") (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-futures") (r "^0.2") (d #t) (k 0)))) (h "0vscinb0qzji1gy3cg7gmwiqlfjzkfq617naw4cysi014zc2w6fi")))

