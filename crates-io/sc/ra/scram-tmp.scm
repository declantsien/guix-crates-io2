(define-module (crates-io sc ra scram-tmp) #:use-module (crates-io))

(define-public crate-scram-tmp-0.4.0 (c (n "scram-tmp") (v "0.4.0") (d (list (d (n "base64") (r "^0.9.0") (d #t) (k 0)) (d (n "rand") (r "^0.5.0") (d #t) (k 0)) (d (n "ring") (r "^0.13") (d #t) (k 0)))) (h "0fnrwrbnq0kiicdsisz8qdqncraqy7cfnwbv40hrkm4y73pps8wk") (y #t)))

