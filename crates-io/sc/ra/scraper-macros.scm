(define-module (crates-io sc ra scraper-macros) #:use-module (crates-io))

(define-public crate-scraper-macros-0.1.0 (c (n "scraper-macros") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1jgnvb0d5r3jsmyv70vd8bjyzw68widl4382sak39z3km6jz4z6l")))

(define-public crate-scraper-macros-0.1.1 (c (n "scraper-macros") (v "0.1.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1dpvvcv7pm5g983qfjjj9iia5qi2warv6sn4ll7lwdjnr1qiqm4l")))

(define-public crate-scraper-macros-0.2.0 (c (n "scraper-macros") (v "0.2.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0i69i4pz04j0b6wzfha7vk2q7pgs5abz8pdk528ppjkjjm2dmwxw")))

