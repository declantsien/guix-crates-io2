(define-module (crates-io sc ra scrawl) #:use-module (crates-io))

(define-public crate-scrawl-0.1.0 (c (n "scrawl") (v "0.1.0") (h "15d38xxbfba97mfyc69f005fp61m1329ljcivxwdz84hxq7v462q")))

(define-public crate-scrawl-0.1.1 (c (n "scrawl") (v "0.1.1") (h "0wz2yxjfvd7djvziv5l3wwa37nfj8sgkd49gp426ip2cglb1136y")))

(define-public crate-scrawl-0.1.2 (c (n "scrawl") (v "0.1.2") (h "06lhmsy2y6phm7kjdgr2ca4rmyy8mbgbrciz2zg7y7755w8arb3p")))

(define-public crate-scrawl-0.1.3 (c (n "scrawl") (v "0.1.3") (h "1dl4n445cc85pxwsk6d4wqiv6skhsyf4y5jzg2y3455b6xpqfabx")))

(define-public crate-scrawl-0.1.4 (c (n "scrawl") (v "0.1.4") (h "1j991vasrvgvxmra30scpbd404x1g1jqs715sw43vhvjwr0dhvf2")))

(define-public crate-scrawl-0.1.5 (c (n "scrawl") (v "0.1.5") (h "1r366kpxa75wgrdc9g28xlk4pfs0g17bjhl4g5c507ynm6b69lxd")))

(define-public crate-scrawl-0.9.0 (c (n "scrawl") (v "0.9.0") (h "0p9a8yv35a84h40mp3v47c9frw9nmfd57y0vsr5rb992fhh7rfv1")))

(define-public crate-scrawl-1.0.0 (c (n "scrawl") (v "1.0.0") (h "09840fjbg2gh639d853vdxbrl76g9vglv3j038dvmrzrxdbqm8cb")))

(define-public crate-scrawl-1.1.0 (c (n "scrawl") (v "1.1.0") (h "1lid625ykxhdms4x6sh8fv4sxk8vij5w9jcb0nlcihzrbh7xmgwg")))

(define-public crate-scrawl-2.0.0 (c (n "scrawl") (v "2.0.0") (h "1k7p0aihmv580w8kjymkbs4ny17yvffig0czkj44qg92x5a0q5n5")))

