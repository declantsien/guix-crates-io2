(define-module (crates-io sc ra scrappy-testing) #:use-module (crates-io))

(define-public crate-scrappy-testing-0.0.1 (c (n "scrappy-testing") (v "0.0.1") (d (list (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "net2") (r "^0.2") (d #t) (k 0)) (d (n "scrappy-macros") (r "^0.0.1") (d #t) (k 0)) (d (n "scrappy-rt") (r "^0.0.1") (d #t) (k 0)) (d (n "scrappy-server") (r "^0.0.1") (d #t) (k 0)) (d (n "scrappy-service") (r "^0.0.1") (d #t) (k 0)))) (h "0871ymrggfy5p6spv72jcwd26xzjwz64ipd6pvyy4bbklwpk9yhl")))

