(define-module (crates-io sc ra scratch-server) #:use-module (crates-io))

(define-public crate-scratch-server-0.1.0 (c (n "scratch-server") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.37") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (d #t) (k 0)) (d (n "mime_guess") (r "^2.0.4") (d #t) (k 0)) (d (n "native-tls") (r "^0.2.11") (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)) (d (n "termcolor") (r "^1.4.1") (d #t) (k 0)))) (h "1d3n24adhqyk9gi8rb0whsv3d0bqgfalzcjslywzc9sf1qc7qmdj")))

