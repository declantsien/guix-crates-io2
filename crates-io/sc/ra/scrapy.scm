(define-module (crates-io sc ra scrapy) #:use-module (crates-io))

(define-public crate-scrapy-0.0.1 (c (n "scrapy") (v "0.0.1") (h "059yh11qm4hg5s8jhma89chy1afw68ssb0fwv9vli0ww35nc26f7")))

(define-public crate-scrapy-0.0.2 (c (n "scrapy") (v "0.0.2") (h "1g2rmq1w8jhx286hna7l7zw2d64qij7fyd8a4cby7979h5q2l280")))

