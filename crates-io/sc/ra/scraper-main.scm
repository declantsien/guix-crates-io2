(define-module (crates-io sc ra scraper-main) #:use-module (crates-io))

(define-public crate-scraper-main-0.1.0 (c (n "scraper-main") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)) (d (n "xpather") (r "^0.1.4") (d #t) (k 0)))) (h "19l61kwgq9n0qn7bky38hg496shbq648mr9x8xlsgvrzh7r7awd0")))

(define-public crate-scraper-main-0.2.0 (c (n "scraper-main") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "xpather") (r "^0.2") (d #t) (k 0)))) (h "1lqcjx99fqknzcynjcblkq2d2zfb57pa5zr6wm6drymhmh0k4s3k")))

(define-public crate-scraper-main-0.3.0 (c (n "scraper-main") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "scraper-macros") (r "^0.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "xpather") (r "^0.3.0-beta.8") (d #t) (k 0)))) (h "0kgprf18cvgbv2v34cjqxhk1jkzkgwnama84rhrbjgmndks2rz1q")))

(define-public crate-scraper-main-0.3.1 (c (n "scraper-main") (v "0.3.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "scraper-macros") (r "^0.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "xpather") (r "^0.3.0-beta.9") (d #t) (k 0)))) (h "1g5619nnr031534iamd14f8bzaimgqgiyglinwfwbak3yld39kl8")))

