(define-module (crates-io sc ra scratchstack-wrapper-aws-c-cal) #:use-module (crates-io))

(define-public crate-scratchstack-wrapper-aws-c-cal-0.1.0-alpha.5 (c (n "scratchstack-wrapper-aws-c-cal") (v "0.1.0-alpha.5") (d (list (d (n "bindgen") (r "^0.61") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "scratchstack-wrapper-aws-c-common") (r "=0.1.0-alpha.5") (d #t) (k 0)) (d (n "scratchstack-wrapper-aws-lc") (r "=0.1.0-alpha.5") (d #t) (k 0)))) (h "1hclpsndrhpp82na8hljw26dnb912565ldw356wz4i627q05hjlr") (l "aws-c-cal")))

(define-public crate-scratchstack-wrapper-aws-c-cal-0.1.0-alpha.7 (c (n "scratchstack-wrapper-aws-c-cal") (v "0.1.0-alpha.7") (d (list (d (n "bindgen") (r "^0.61") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "scratchstack-wrapper-aws-c-common") (r "=0.1.0-alpha.7") (d #t) (k 0)) (d (n "scratchstack-wrapper-aws-lc") (r "=0.1.0-alpha.7") (d #t) (k 0)))) (h "07hwgx2az65dvjjp5a6vga30h5bkf03w4nd4dcgb8cnw5f2ab5kd") (l "aws-c-cal")))

