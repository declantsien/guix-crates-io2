(define-module (crates-io sc ra scrappy-threadpool) #:use-module (crates-io))

(define-public crate-scrappy-threadpool-0.0.1 (c (n "scrappy-threadpool") (v "0.0.1") (d (list (d (n "derive_more") (r "^0.99.2") (d #t) (k 0)) (d (n "futures-channel") (r "^0.3.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num_cpus") (r "^1.10") (d #t) (k 0)) (d (n "parking_lot") (r "^0.10") (d #t) (k 0)) (d (n "threadpool") (r "^1.7") (d #t) (k 0)))) (h "1byr4babwq0hxdm43sk7p4a9vknrdfd783df9ciyk9ginv6vlh6x")))

