(define-module (crates-io sc ra scrappy-service) #:use-module (crates-io))

(define-public crate-scrappy-service-0.0.1 (c (n "scrappy-service") (v "0.0.1") (d (list (d (n "futures-util") (r "^0.3.1") (d #t) (k 0)) (d (n "pin-project") (r "^0.4.6") (d #t) (k 0)) (d (n "scrappy-rt") (r "^0.0.1") (d #t) (k 2)))) (h "1hl3n8rfqa5sg8ir11s8ji7kjnacid6r5m89lj6m8dvh00kg03d0")))

