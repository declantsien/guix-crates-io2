(define-module (crates-io sc ra scrappy-codec) #:use-module (crates-io))

(define-public crate-scrappy-codec-0.0.1 (c (n "scrappy-codec") (v "0.0.1") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "bytes") (r "^0.5.2") (d #t) (k 0)) (d (n "futures-core") (r "^0.3.1") (d #t) (k 0)) (d (n "futures-sink") (r "^0.3.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^0.2.4") (k 0)) (d (n "tokio-util") (r "^0.2.0") (f (quote ("codec"))) (k 0)))) (h "1zramwbdlq188cag8zhd034hk94day4ibk25wkim4dkcnpx9ky2v")))

