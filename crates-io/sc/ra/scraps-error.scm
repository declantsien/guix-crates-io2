(define-module (crates-io sc ra scraps-error) #:use-module (crates-io))

(define-public crate-scraps-error-0.1.0 (c (n "scraps-error") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "00yfi8rqcpdv45462v1d0ybyrq915mnmn6rpa0lmksyssivyc56q")))

