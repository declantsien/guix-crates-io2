(define-module (crates-io sc ra scratchstack-wrapper-aws-c-sdkutils) #:use-module (crates-io))

(define-public crate-scratchstack-wrapper-aws-c-sdkutils-0.1.0-alpha.5 (c (n "scratchstack-wrapper-aws-c-sdkutils") (v "0.1.0-alpha.5") (d (list (d (n "bindgen") (r "^0.61") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "scratchstack-wrapper-aws-c-common") (r "=0.1.0-alpha.5") (d #t) (k 0)))) (h "18yaw4gz6wcr2f5haav9jxk6k49yz2860ln878p548gdq3bwqcpz") (l "aws-c-sdkutils")))

(define-public crate-scratchstack-wrapper-aws-c-sdkutils-0.1.0-alpha.7 (c (n "scratchstack-wrapper-aws-c-sdkutils") (v "0.1.0-alpha.7") (d (list (d (n "bindgen") (r "^0.61") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "scratchstack-wrapper-aws-c-common") (r "^0.1.0-alpha.7") (d #t) (k 0)))) (h "182fn4cwx9633g7kwx2jrd8pdv5w3sbwaba9bm33sn9i9kppfk2v") (l "aws-c-sdkutils")))

