(define-module (crates-io sc ra scrappy-router) #:use-module (crates-io))

(define-public crate-scrappy-router-0.0.1 (c (n "scrappy-router") (v "0.0.1") (d (list (d (n "bytestring") (r "^0.1.2") (d #t) (k 0)) (d (n "http") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "http") (r "^0.2.0") (d #t) (k 2)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "07xq2r24lp3hls56l3zb45x1yp38zg9vdw6lzinikg1ffcf71aq0") (f (quote (("default" "http"))))))

