(define-module (crates-io sc ra scratch_genetic) #:use-module (crates-io))

(define-public crate-scratch_genetic-22.5.16 (c (n "scratch_genetic") (v "22.5.16") (d (list (d (n "clap") (r "^3.1.5") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "csv") (r "^1.1.6") (d #t) (k 0)) (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1rwjpky7krw2fnp4yzzb0zd6q3p8b9xmzxbjrlafxa30x72j52kd") (y #t)))

(define-public crate-scratch_genetic-22.5.17 (c (n "scratch_genetic") (v "22.5.17") (d (list (d (n "clap") (r "^3.1.5") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "csv") (r "^1.1.6") (d #t) (k 0)) (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1xb1dfxgx0qq6j2gdv58yd67f4k43nf04mg8q87a60ggdw203nsk") (y #t)))

(define-public crate-scratch_genetic-22.5.23 (c (n "scratch_genetic") (v "22.5.23") (d (list (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0wwnjykg1hhi6zkqdp10zcp4c16s37pgq2zp0k1qqb1yqi7bxdc0")))

