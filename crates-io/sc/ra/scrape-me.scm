(define-module (crates-io sc ra scrape-me) #:use-module (crates-io))

(define-public crate-scrape-me-0.1.0 (c (n "scrape-me") (v "0.1.0") (h "08a26my2yybiq3987kw3lzrjfg0brxhh5s3b736lsvd4kvanb5z1")))

(define-public crate-scrape-me-0.1.1 (c (n "scrape-me") (v "0.1.1") (h "0a0h92yb266cp84zyrmk9sddxzrhnq13pz3fncn9b6gi6k8vm152")))

