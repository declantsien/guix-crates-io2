(define-module (crates-io sc ra scrapped-webs) #:use-module (crates-io))

(define-public crate-scrapped-webs-1.0.0 (c (n "scrapped-webs") (v "1.0.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "scraper") (r "^0.18.1") (d #t) (k 0)) (d (n "strsim") (r "^0.10.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1igxwld70xgq95iflwhyxcwfggmva2wll8xzgr2bm1hmd5vyrmg8")))

