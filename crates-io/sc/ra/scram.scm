(define-module (crates-io sc ra scram) #:use-module (crates-io))

(define-public crate-scram-0.0.1 (c (n "scram") (v "0.0.1") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)) (d (n "data-encoding") (r "^1.1.2") (d #t) (k 0)) (d (n "rand") (r "^0.3.14") (d #t) (k 0)) (d (n "ring") (r "^0.2.3") (d #t) (k 0)))) (h "1f77ymgc8rilwlwwlcp8sjdrff91f1q1yzqp3pkvmkcnzxh3fxk1")))

(define-public crate-scram-0.0.2 (c (n "scram") (v "0.0.2") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)) (d (n "data-encoding") (r "^1.1.2") (d #t) (k 0)) (d (n "rand") (r "^0.3.14") (d #t) (k 0)) (d (n "ring") (r "^0.3.0") (d #t) (k 0)))) (h "0kh65sggz2baiz8yjbfyy8pzznypg71wwc5z511q9lbl0q3hczk7")))

(define-public crate-scram-0.1.0 (c (n "scram") (v "0.1.0") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)) (d (n "data-encoding") (r "^1.1.2") (d #t) (k 0)) (d (n "rand") (r "^0.3.14") (d #t) (k 0)) (d (n "ring") (r "^0.3.0") (d #t) (k 0)))) (h "1arpasgka45l9c386p2b3x7h9h2a4zidr1wkcdcg30x6ydnknr2p")))

(define-public crate-scram-0.1.1 (c (n "scram") (v "0.1.1") (d (list (d (n "data-encoding") (r "^1.1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "ring") (r "^0.6") (d #t) (k 0)))) (h "16sm6kvm0w6lgiiz4pq11nj9cqzrqv91cgj8pmysagj611awnqc3")))

(define-public crate-scram-0.2.0 (c (n "scram") (v "0.2.0") (d (list (d (n "base64") (r "^0.5.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "ring") (r "^0.9.4") (d #t) (k 0)))) (h "1r302f5mnyw91xypk1654al4xapcvaknz6lhkx1m120ngs39bhnr")))

(define-public crate-scram-0.2.1 (c (n "scram") (v "0.2.1") (d (list (d (n "base64") (r "^0.6") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "ring") (r "^0.11") (d #t) (k 0)))) (h "0a956ypz0bibdjzlcf6jmxibvdbhbhlmxbxzkxsdw40npir5pfkl")))

(define-public crate-scram-0.2.2 (c (n "scram") (v "0.2.2") (d (list (d (n "base64") (r "^0.7.0") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "ring") (r "^0.12") (d #t) (k 0)))) (h "02qcf5363waa81paw1khyz7x5cvai61lna16i2jrgw9xiz9kxl09")))

(define-public crate-scram-0.2.3 (c (n "scram") (v "0.2.3") (d (list (d (n "base64") (r "^0.9.0") (d #t) (k 0)) (d (n "rand") (r "^0.4.2") (d #t) (k 0)) (d (n "ring") (r "^0.13.0-alpha") (d #t) (k 0)))) (h "0lbrmi7zjf6l8mnnmaiglf24gc58by2jwfmbkszzkbjh7qii69wc")))

(define-public crate-scram-0.2.4 (c (n "scram") (v "0.2.4") (d (list (d (n "base64") (r "^0.9.0") (d #t) (k 0)) (d (n "rand") (r "^0.4.2") (d #t) (k 0)) (d (n "ring") (r "^0.12.1") (d #t) (k 0)))) (h "14z1wmr7p3bdh8ldd6dldc68ig1xbw20pvxab5cl5r59ymlqg65d")))

(define-public crate-scram-0.3.0 (c (n "scram") (v "0.3.0") (d (list (d (n "base64") (r "^0.9.0") (d #t) (k 0)) (d (n "rand") (r "^0.5.0") (d #t) (k 0)) (d (n "ring") (r "^0.12.1") (d #t) (k 0)))) (h "08l691jiml1rxgnssj8yqpvs824gmbpbjinsccy104bbpnsgqi16")))

(define-public crate-scram-0.4.0 (c (n "scram") (v "0.4.0") (d (list (d (n "base64") (r "^0.9.0") (d #t) (k 0)) (d (n "rand") (r "^0.5.0") (d #t) (k 0)) (d (n "ring") (r "^0.13") (d #t) (k 0)))) (h "0lwa3bg7zbwwcr609azp7qmbnzcjwkxdhja68kbyyzm2xaf43whg")))

(define-public crate-scram-0.5.0 (c (n "scram") (v "0.5.0") (d (list (d (n "base64") (r "^0.11.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 0)) (d (n "ring") (r "^0.16.9") (d #t) (k 0)))) (h "1br33r8jxjhf9mj7rp5shbb6awdc3a9m7apsn619h7yfd3x7hiiy")))

(define-public crate-scram-0.5.1 (c (n "scram") (v "0.5.1") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "ring") (r "^0.16.9") (d #t) (k 0)))) (h "1d7mflyghzvblvirs4yyynjd0bmjb309f15z10dxxsd2463ssb71") (y #t)))

(define-public crate-scram-0.6.0 (c (n "scram") (v "0.6.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "ring") (r "^0.16.9") (d #t) (k 0)))) (h "125ydrzmy75zscf8qwhbgxjxj5sbsfh4p288qar9kb3vp7kaaybn")))

