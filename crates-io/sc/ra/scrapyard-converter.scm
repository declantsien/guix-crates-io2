(define-module (crates-io sc ra scrapyard-converter) #:use-module (crates-io))

(define-public crate-scrapyard-converter-0.1.0 (c (n "scrapyard-converter") (v "0.1.0") (d (list (d (n "clap") (r "^2.31.1") (d #t) (k 0)) (d (n "either") (r "^1.4.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "error-chain") (r "^0.11.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "scrapyard-core") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.33") (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.2.1") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.33") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.11") (d #t) (k 0)))) (h "11q79lxvjkza12mz4s4xjwrj262a9cp9f0az1jz2qsp3r9wbcqdn")))

