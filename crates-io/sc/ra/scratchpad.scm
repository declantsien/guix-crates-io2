(define-module (crates-io sc ra scratchpad) #:use-module (crates-io))

(define-public crate-scratchpad-0.1.0 (c (n "scratchpad") (v "0.1.0") (d (list (d (n "arrayvec") (r "^0.4") (k 2)))) (h "1kfhwi0qbs6dqliwj10s9v0y7azm8vg8507mf4gn5cxhiq2c39wl") (f (quote (("unstable") ("std") ("default" "std"))))))

(define-public crate-scratchpad-0.1.1 (c (n "scratchpad") (v "0.1.1") (d (list (d (n "arrayvec") (r "^0.4") (k 2)))) (h "1rv7iaz5lwy6i68p5pb9hgmvsy78alf05miqkj99h6s71rlzm35v") (f (quote (("unstable") ("std") ("default" "std")))) (y #t)))

(define-public crate-scratchpad-0.2.0 (c (n "scratchpad") (v "0.2.0") (d (list (d (n "arrayvec") (r "^0.4") (k 2)))) (h "1j3wqjk7ghylwvc85g42r8vwsdp13maf0rfcby8jlbx7h0s4rp7m") (f (quote (("unstable") ("std") ("default" "std"))))))

(define-public crate-scratchpad-0.3.0 (c (n "scratchpad") (v "0.3.0") (d (list (d (n "arrayvec") (r "^0.4") (k 2)))) (h "1jd8ka885bwncxlh69kg6p4978452qj9yr7wsifaism6id3bshw9") (f (quote (("unstable") ("std") ("default" "std"))))))

(define-public crate-scratchpad-1.0.0-beta.1 (c (n "scratchpad") (v "1.0.0-beta.1") (d (list (d (n "arrayvec") (r "^0.4") (k 2)))) (h "0ybwm8b5s54d38ilkvq2hhby43iw0w7bjgwmrhjp0r016cwdifi1") (f (quote (("unstable") ("std") ("default" "std")))) (y #t)))

(define-public crate-scratchpad-1.0.0-beta.2 (c (n "scratchpad") (v "1.0.0-beta.2") (d (list (d (n "arrayvec") (r "^0.4") (k 2)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)))) (h "0m1xwjinhl4mar1c5z4662y60py8ri9h7dfxzdsag92iczar2d3h") (f (quote (("unstable") ("std") ("default" "std")))) (y #t)))

(define-public crate-scratchpad-1.0.0 (c (n "scratchpad") (v "1.0.0") (d (list (d (n "arrayvec") (r "^0.4") (k 2)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)))) (h "10zyah66ksvizrrwjb3g5c4jzr21fw28rkhgsx7lkh3952qkifa0") (f (quote (("unstable") ("std") ("default" "std")))) (y #t)))

(define-public crate-scratchpad-1.0.1 (c (n "scratchpad") (v "1.0.1") (d (list (d (n "arrayvec") (r "^0.4") (k 2)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)))) (h "1s5rsszq55cwsq1lhl9lx02k2qy669mz03kj7s8aw4i0slnnwb36") (f (quote (("unstable") ("std") ("default" "std")))) (y #t)))

(define-public crate-scratchpad-1.1.0 (c (n "scratchpad") (v "1.1.0") (d (list (d (n "arrayvec") (r "^0.4") (k 2)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)))) (h "1m0300y0h2dw8na8digz7gapdk9z7qsjhia4imq8sxb5nykiv1rb") (f (quote (("unstable") ("std") ("default" "std")))) (y #t)))

(define-public crate-scratchpad-1.2.0 (c (n "scratchpad") (v "1.2.0") (d (list (d (n "arrayvec") (r "^0.4") (k 2)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "stable_deref_trait") (r "^1.0") (k 0)))) (h "1ajhm65b6zacsr744lip7mfbbz2549w5shlkjdsl86h6w26m6a4z") (f (quote (("unstable") ("std") ("default" "std")))) (y #t)))

(define-public crate-scratchpad-1.3.0 (c (n "scratchpad") (v "1.3.0") (d (list (d (n "arrayvec") (r "^0.4") (k 2)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "stable_deref_trait") (r "^1.0") (k 0)))) (h "01ybpkhxf731mxsw5x3hlwrr8g11njdks5yjr8z9z4x9pa983zdy") (f (quote (("unstable") ("std") ("default" "std") ("alloc")))) (y #t)))

(define-public crate-scratchpad-1.3.1 (c (n "scratchpad") (v "1.3.1") (d (list (d (n "arrayvec") (r "^0.4") (k 2)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "stable_deref_trait") (r "^1.0") (k 0)))) (h "07dwx52vbwh9208mymdi5sx7hkk070rzirzhj5af9bzad1s2vi05") (f (quote (("unstable") ("std") ("default" "std") ("alloc"))))))

