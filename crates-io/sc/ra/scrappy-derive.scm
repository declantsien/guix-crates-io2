(define-module (crates-io sc ra scrappy-derive) #:use-module (crates-io))

(define-public crate-scrappy-derive-0.0.1 (c (n "scrappy-derive") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0dv93bqnwqrmawsap8mwdkqsf12xhdfzpwrqgxym2bdw62p1rv39")))

