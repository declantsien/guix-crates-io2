(define-module (crates-io sc ra scrappy-session) #:use-module (crates-io))

(define-public crate-scrappy-session-0.0.1 (c (n "scrappy-session") (v "0.0.1") (d (list (d (n "bytes") (r "^0.5.3") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.2") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "scrappy") (r "^0.0.1") (d #t) (k 0)) (d (n "scrappy-rt") (r "^0.0.1") (d #t) (k 2)) (d (n "scrappy-service") (r "^0.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "time") (r "^0.1.42") (d #t) (k 0)))) (h "1iwqvgcgs08s9kdg2z5ig1digz8vgwswlf82jp95krgbpl5ll2q6") (f (quote (("default" "cookie-session") ("cookie-session" "scrappy/secure-cookies"))))))

