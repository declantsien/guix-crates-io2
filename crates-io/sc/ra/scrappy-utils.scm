(define-module (crates-io sc ra scrappy-utils) #:use-module (crates-io))

(define-public crate-scrappy-utils-0.0.1 (c (n "scrappy-utils") (v "0.0.1") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "bytes") (r "^0.5.3") (d #t) (k 0)) (d (n "either") (r "^1.5.3") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pin-project") (r "^0.4.6") (d #t) (k 0)) (d (n "scrappy-codec") (r "^0.0.1") (d #t) (k 0)) (d (n "scrappy-rt") (r "^0.0.1") (d #t) (k 0)) (d (n "scrappy-service") (r "^0.0.1") (d #t) (k 0)) (d (n "slab") (r "^0.4") (d #t) (k 0)))) (h "0jpa2xylm3ffb61f391pmw4n4mlnxpidd8ahp8i1shw9snbv45h7")))

