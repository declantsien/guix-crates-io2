(define-module (crates-io sc ra scrappy-ioframe) #:use-module (crates-io))

(define-public crate-scrappy-ioframe-0.0.1 (c (n "scrappy-ioframe") (v "0.0.1") (d (list (d (n "bytes") (r "^0.5.3") (d #t) (k 0)) (d (n "either") (r "^1.5.3") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pin-project") (r "^0.4.6") (d #t) (k 0)) (d (n "scrappy-codec") (r "^0.0.1") (d #t) (k 0)) (d (n "scrappy-connect") (r "^0.0.1") (d #t) (k 2)) (d (n "scrappy-rt") (r "^0.0.1") (d #t) (k 0)) (d (n "scrappy-service") (r "^0.0.1") (d #t) (k 0)) (d (n "scrappy-testing") (r "^0.0.1") (d #t) (k 2)) (d (n "scrappy-utils") (r "^0.0.1") (d #t) (k 0)))) (h "0rcr05qsqlr32k1m6gk5d7r1ddyk0yqimvf0x8amgiw7j7hh4n0v")))

