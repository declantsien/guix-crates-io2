(define-module (crates-io sc ra scrappy_do_codegen) #:use-module (crates-io))

(define-public crate-scrappy_do_codegen-0.1.0 (c (n "scrappy_do_codegen") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("visit-mut" "full"))) (d #t) (k 0)))) (h "0q9xy5dy6jnsykaww4vk4b69fkc8aylvabr86jf21vx6i42w9mgn")))

(define-public crate-scrappy_do_codegen-0.2.0 (c (n "scrappy_do_codegen") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("visit-mut" "full"))) (d #t) (k 0)))) (h "13qkd6ls8q469zfwl453rp7nsiji8acyaxy22bpmsq3wshrbln20")))

