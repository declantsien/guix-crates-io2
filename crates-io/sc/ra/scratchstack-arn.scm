(define-module (crates-io sc ra scratchstack-arn) #:use-module (crates-io))

(define-public crate-scratchstack-arn-0.4.0 (c (n "scratchstack-arn") (v "0.4.0") (d (list (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "regex") (r "^1.6") (d #t) (k 0)) (d (n "regex-syntax") (r "^0.6") (d #t) (k 0)))) (h "1wka2iwg0rpsz30x1vj75zkig7gkbpb2sbdv3xxm0hlgimvj1vqz")))

(define-public crate-scratchstack-arn-0.4.1 (c (n "scratchstack-arn") (v "0.4.1") (d (list (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.3") (d #t) (k 2)) (d (n "regex") (r "^1.6") (d #t) (k 0)) (d (n "regex-syntax") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1d4f471wkcaxryjz7brvbyq4kla1mxh0gyhzismq62q8zqqp2z9k")))

(define-public crate-scratchstack-arn-0.4.2 (c (n "scratchstack-arn") (v "0.4.2") (d (list (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.3") (d #t) (k 2)) (d (n "regex") (r "^1.6") (d #t) (k 0)) (d (n "regex-syntax") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1n5lzgg3pdzbgayz28izwyiyjrah77qxh168l4wmfxhswkisyqav")))

(define-public crate-scratchstack-arn-0.4.3 (c (n "scratchstack-arn") (v "0.4.3") (d (list (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.3") (d #t) (k 2)) (d (n "regex") (r "^1.6") (d #t) (k 0)) (d (n "regex-syntax") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1bcks68135fhd9m3qifbddbk1fgz4vz9l1jfqi50l4j9hzy8b1sh")))

(define-public crate-scratchstack-arn-0.4.4 (c (n "scratchstack-arn") (v "0.4.4") (d (list (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.3") (d #t) (k 2)) (d (n "regex") (r "^1.6") (d #t) (k 0)) (d (n "regex-syntax") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1jlz85hhyi2qr2psgh97mlwhz46mpq0h8axr9cv1vmx88vs8fznd")))

(define-public crate-scratchstack-arn-0.4.5 (c (n "scratchstack-arn") (v "0.4.5") (d (list (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.3") (d #t) (k 2)) (d (n "regex") (r "^1.6") (d #t) (k 0)) (d (n "regex-syntax") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "0mwi2zamxkgdfyn2vhbkjc0c43zrxinqpwzr51bcznskpdbg60km")))

(define-public crate-scratchstack-arn-0.4.6 (c (n "scratchstack-arn") (v "0.4.6") (d (list (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.3") (d #t) (k 2)) (d (n "regex") (r "^1.6") (d #t) (k 0)) (d (n "regex-syntax") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "15v8852ddv3iyhfb843h25js3lq0mzf23knjan16hrlzis9h0lgr")))

(define-public crate-scratchstack-arn-0.4.7 (c (n "scratchstack-arn") (v "0.4.7") (d (list (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.3") (d #t) (k 2)) (d (n "regex") (r "^1.6") (d #t) (k 0)) (d (n "regex-syntax") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "14jmy14rcsgzpn56km0b5sfa6whvarjmnib76f391wjwl5q8ccc8")))

(define-public crate-scratchstack-arn-0.4.8 (c (n "scratchstack-arn") (v "0.4.8") (d (list (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.3") (d #t) (k 2)) (d (n "regex") (r "^1.6") (d #t) (k 0)) (d (n "regex-syntax") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "12rdvx1z9xnmkrps15bjnb7wrxks5sg43hlhrsyx6mqpb3h3rniy")))

