(define-module (crates-io sc ra scratchstack-wrapper-aws-lc) #:use-module (crates-io))

(define-public crate-scratchstack-wrapper-aws-lc-0.1.0-alpha.0 (c (n "scratchstack-wrapper-aws-lc") (v "0.1.0-alpha.0") (d (list (d (n "bindgen") (r "^0.61") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0wc139q0syvm83dcpg4plyg3ck2j2cfwsb26cpaw1hxch6s4j5qg")))

(define-public crate-scratchstack-wrapper-aws-lc-0.1.0-alpha.2 (c (n "scratchstack-wrapper-aws-lc") (v "0.1.0-alpha.2") (d (list (d (n "bindgen") (r "^0.61") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1pd4g7hcichcbhig3gl6dzbrg705ncf9sm97cggvx23mj107vlsm")))

(define-public crate-scratchstack-wrapper-aws-lc-0.1.0-alpha.3 (c (n "scratchstack-wrapper-aws-lc") (v "0.1.0-alpha.3") (d (list (d (n "bindgen") (r "^0.61") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1yjn7kaijnfd0ga5yqfqzd9phijgahxi6nj4lird3ypn96xya0rs")))

(define-public crate-scratchstack-wrapper-aws-lc-0.1.0-alpha.5 (c (n "scratchstack-wrapper-aws-lc") (v "0.1.0-alpha.5") (d (list (d (n "bindgen") (r "^0.61") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "117bxdd4463jpx3dxvdy8w4gb4skiassnhxcnimdkdjxyq7nykpl") (l "aws-lc")))

(define-public crate-scratchstack-wrapper-aws-lc-0.1.0-alpha.7 (c (n "scratchstack-wrapper-aws-lc") (v "0.1.0-alpha.7") (d (list (d (n "bindgen") (r "^0.61") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "05mnjyq65kwwyisi2a5vvva8vgsvdpipajjr7jn8k24fvc4p1vl2") (l "aws-lc")))

