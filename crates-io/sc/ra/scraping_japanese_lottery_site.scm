(define-module (crates-io sc ra scraping_japanese_lottery_site) #:use-module (crates-io))

(define-public crate-scraping_japanese_lottery_site-0.1.0 (c (n "scraping_japanese_lottery_site") (v "0.1.0") (d (list (d (n "html-escape") (r "^0.2.13") (d #t) (k 0)) (d (n "html2text") (r "^0.6.0") (d #t) (k 0)) (d (n "html5ever") (r "^0.26.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "scraper") (r "^0.15.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1dsg8l3vch0n8sg82988nflz0hbvfbcpzycgvz0a95ywdqb3d6vd")))

(define-public crate-scraping_japanese_lottery_site-0.1.1 (c (n "scraping_japanese_lottery_site") (v "0.1.1") (d (list (d (n "html-escape") (r "^0.2.13") (d #t) (k 0)) (d (n "html2text") (r "^0.6.0") (d #t) (k 0)) (d (n "html5ever") (r "^0.26.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "scraper") (r "^0.15.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1gv2p4ih39ppdcy8k5pn7lrhzj7q5fghnvr4c2mhs2qsck5l53px")))

(define-public crate-scraping_japanese_lottery_site-0.1.2 (c (n "scraping_japanese_lottery_site") (v "0.1.2") (d (list (d (n "html-escape") (r "^0.2.13") (d #t) (k 0)) (d (n "html2text") (r "^0.6.0") (d #t) (k 0)) (d (n "html5ever") (r "^0.26.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "scraper") (r "^0.15.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1mg89bqgmllq3frhj768y6c4dgpzc2cl88wwm4i43dchhx4navgs")))

(define-public crate-scraping_japanese_lottery_site-0.1.3 (c (n "scraping_japanese_lottery_site") (v "0.1.3") (d (list (d (n "html-escape") (r "^0.2.13") (d #t) (k 0)) (d (n "html2text") (r "^0.6.0") (d #t) (k 0)) (d (n "html5ever") (r "^0.26.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "scraper") (r "^0.15.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0xj252qcpm895gzi7ryqxrrpk10i773zskym648i3xd76p5n58p4")))

