(define-module (crates-io sc ra scrappy-rt) #:use-module (crates-io))

(define-public crate-scrappy-rt-0.0.1 (c (n "scrappy-rt") (v "0.0.1") (d (list (d (n "copyless") (r "^0.1.4") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "scrappy-macros") (r "^0.0.1") (d #t) (k 0)) (d (n "scrappy-threadpool") (r "^0.0.1") (d #t) (k 0)) (d (n "tokio") (r "^0.2.6") (f (quote ("rt-core" "rt-util" "io-driver" "tcp" "uds" "udp" "time" "signal" "stream"))) (k 0)))) (h "09rbvjw54d00qzwp5lybnb6dbia5j0q04csflr98gk7ki49p6r9m")))

