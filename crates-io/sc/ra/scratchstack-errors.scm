(define-module (crates-io sc ra scratchstack-errors) #:use-module (crates-io))

(define-public crate-scratchstack-errors-0.4.0 (c (n "scratchstack-errors") (v "0.4.0") (d (list (d (n "http") (r "^0.2") (d #t) (k 0)))) (h "0x2157iiv3cymy4kayxg0fpb30iqwjzcn6xgmqb8m227s50cmmkd")))

(define-public crate-scratchstack-errors-0.4.1 (c (n "scratchstack-errors") (v "0.4.1") (d (list (d (n "http") (r "^0.2") (d #t) (k 0)))) (h "1gmqyli1falach388d146hz5kywcd45dzjlcd3r9aj5ss8gpskjp")))

(define-public crate-scratchstack-errors-0.4.2 (c (n "scratchstack-errors") (v "0.4.2") (d (list (d (n "http") (r "^0.2") (d #t) (k 0)))) (h "0cchpz5zr7n8j3c7dj5d9884sbp22cq9mjg9fmlf9j3vsphdx2l4")))

(define-public crate-scratchstack-errors-0.4.3 (c (n "scratchstack-errors") (v "0.4.3") (d (list (d (n "http") (r "^0.2") (d #t) (k 0)))) (h "19fvwxif8jmwff61725xmnim7ppl1d21k12cxzg12jpz65g5cfig")))

(define-public crate-scratchstack-errors-0.4.4 (c (n "scratchstack-errors") (v "0.4.4") (d (list (d (n "http") (r "^0.2") (d #t) (k 0)))) (h "0p4x14zyq3h4qa1psrfh8zfn38gmw1rh8szzqhvrzlhpyfvl3vyn")))

(define-public crate-scratchstack-errors-0.4.5 (c (n "scratchstack-errors") (v "0.4.5") (d (list (d (n "http") (r "^0.2") (d #t) (k 0)))) (h "0r05gngvbjj2r9z18nyrw0n4wg132l2sx9m5zs9yx0xnfavhcy1l")))

(define-public crate-scratchstack-errors-0.4.6 (c (n "scratchstack-errors") (v "0.4.6") (d (list (d (n "http") (r "^0.2") (d #t) (k 0)))) (h "1id3njpfzsraxfsvqqyx1gqgf4m2alay7f4zaqrqq713dwlyjm9c")))

(define-public crate-scratchstack-errors-0.4.7 (c (n "scratchstack-errors") (v "0.4.7") (d (list (d (n "http") (r "^0.2") (d #t) (k 0)))) (h "135b5wpr12r4n08syf2vn7f8p14264ixa3q17vb4p83cby9n8z14")))

(define-public crate-scratchstack-errors-0.4.8 (c (n "scratchstack-errors") (v "0.4.8") (d (list (d (n "http") (r "^0.2") (d #t) (k 0)))) (h "1h02l6bm08q77996gp4hfsrwdwckxm0n689m8ivda4b4g0qcrk3r")))

