(define-module (crates-io sc ra scrapyard-core) #:use-module (crates-io))

(define-public crate-scrapyard-core-0.1.0 (c (n "scrapyard-core") (v "0.1.0") (d (list (d (n "cbindgen") (r "^0.5.2") (o #t) (d #t) (k 1)) (d (n "error-chain") (r "^0.11.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.39") (o #t) (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.21") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.21") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.13") (d #t) (k 0)))) (h "1q34l7a9ajb88dafzb04a1055154cg75bmgmf1p40qh2d4yab4y9") (f (quote (("default") ("cc" "cbindgen" "libc"))))))

