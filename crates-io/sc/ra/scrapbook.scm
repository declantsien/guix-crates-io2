(define-module (crates-io sc ra scrapbook) #:use-module (crates-io))

(define-public crate-scrapbook-0.1.0 (c (n "scrapbook") (v "0.1.0") (d (list (d (n "handlebars") (r "^4.3.7") (f (quote ("dir_source"))) (o #t) (d #t) (k 0)) (d (n "paste") (r "^1.0.12") (d #t) (k 0)) (d (n "seq-macro") (r "^0.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.99") (d #t) (k 0)))) (h "1mr2kzym0xp6dcxvz5dv702abs44pnw1wbgjmc6df8bj8b4baa4x") (f (quote (("default" "handlebars")))) (s 2) (e (quote (("handlebars" "dep:handlebars")))) (r "1.71.0")))

(define-public crate-scrapbook-0.1.1 (c (n "scrapbook") (v "0.1.1") (d (list (d (n "handlebars") (r "^4.3.7") (f (quote ("dir_source"))) (o #t) (d #t) (k 0)) (d (n "paste") (r "^1.0.12") (d #t) (k 0)) (d (n "seq-macro") (r "^0.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.99") (d #t) (k 0)))) (h "1i3r4br4mikngmiaacvy6hfnwrrnbj22d5ri2hjdvdvl0cydmxcr") (f (quote (("default" "handlebars")))) (s 2) (e (quote (("handlebars" "dep:handlebars")))) (r "1.71.0")))

