(define-module (crates-io sc ra scrambler) #:use-module (crates-io))

(define-public crate-scrambler-0.1.0 (c (n "scrambler") (v "0.1.0") (d (list (d (n "clap") (r "^4.1.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)))) (h "1f7h3rlz479xpnr2g36ls0xlpkahrh1v0dm1zhssqfbvl499xb12")))

(define-public crate-scrambler-0.1.1 (c (n "scrambler") (v "0.1.1") (d (list (d (n "clap") (r "^4.1.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)))) (h "0r1cvfgq2yphxvlxxzaw5lnn4063pqdrkix2r0wqnkq96v5ki1h0")))

