(define-module (crates-io sc ra scratch) #:use-module (crates-io))

(define-public crate-scratch-1.0.0 (c (n "scratch") (v "1.0.0") (d (list (d (n "fs2") (r "^0.4") (d #t) (k 2)))) (h "0sff4rvfalp0ip98pl3xa32n7lhzcr4zqn8fgamaalbb64v4a4by")))

(define-public crate-scratch-1.0.1 (c (n "scratch") (v "1.0.1") (d (list (d (n "fs2") (r "^0.4") (d #t) (k 2)))) (h "1aybwqsbrqipp770z9024v6k33zm80n1afbapdbwfqk4l7s1wccn") (r "1.0")))

(define-public crate-scratch-1.0.2 (c (n "scratch") (v "1.0.2") (d (list (d (n "fs2") (r "^0.4") (d #t) (k 2)))) (h "161qac1v2pvxnrd8c0j0n3yg7cnyl22r57bqvc16xzfwb83350cw") (r "1.0")))

(define-public crate-scratch-1.0.3 (c (n "scratch") (v "1.0.3") (d (list (d (n "fs2") (r "^0.4") (d #t) (k 2)))) (h "1ck1ky9fbgh2c1mlypmlvyw3j1rai1pl7ngd7wxh4wz1ridv3k6x") (r "1.0")))

(define-public crate-scratch-1.0.4 (c (n "scratch") (v "1.0.4") (d (list (d (n "fs2") (r "^0.4") (d #t) (k 2)))) (h "137rk6r5vln94y4wxxia2pdwvm8g6rl4pl3d1spxx450dqphhpjx") (r "1.0")))

(define-public crate-scratch-1.0.5 (c (n "scratch") (v "1.0.5") (d (list (d (n "fs2") (r "^0.4") (d #t) (k 2)))) (h "1hchqnh9ggsrisyaaasfqa0r484r75xh2lw87w6fcnz9bh1xp4hp") (r "1.0")))

(define-public crate-scratch-1.0.6 (c (n "scratch") (v "1.0.6") (d (list (d (n "fs2") (r "^0.4") (d #t) (k 2)))) (h "028li6jxzmcc2rh24kwks9p4wcbav7srz1ajnlazx98wgsgask3n") (r "1.0")))

(define-public crate-scratch-1.0.7 (c (n "scratch") (v "1.0.7") (d (list (d (n "fs2") (r "^0.4") (d #t) (k 2)))) (h "0lk160986gamss2rxi746nj747xx7gg8r2hfskrr9fccqc8prkx3") (r "1.0")))

