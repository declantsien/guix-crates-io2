(define-module (crates-io sc ur scursor) #:use-module (crates-io))

(define-public crate-scursor-0.1.0 (c (n "scursor") (v "0.1.0") (h "1xzp0s83399rbg8fhw4hv1i4sy1dhbdcs81pqvgzqh16cyhza8jx")))

(define-public crate-scursor-0.2.0 (c (n "scursor") (v "0.2.0") (h "0n5xkhkdba18xbb313zd6lxb2cq8mpfwg54dm3cnvincsnby1sa9")))

