(define-module (crates-io sc f- scf-core) #:use-module (crates-io))

(define-public crate-scf-core-1.0.0 (c (n "scf-core") (v "1.0.0") (d (list (d (n "lang_extension") (r "^1.0.0") (d #t) (k 0)))) (h "1il7c021gxjgkhmhdy84rplnknggrfkqcig5wl6215w7ghq4wxxl")))

(define-public crate-scf-core-1.1.0 (c (n "scf-core") (v "1.1.0") (d (list (d (n "lang_extension") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "log4rs") (r "^0.8.3") (d #t) (k 2)))) (h "1ahyyj4asjvbqdhqxhbdbx9fra6ism1iy9bxwn79aax90vsp8hx1")))

(define-public crate-scf-core-1.1.1 (c (n "scf-core") (v "1.1.1") (d (list (d (n "lang_extension") (r "^1.0.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "log4rs") (r "^0.8.3") (d #t) (k 2)))) (h "0a4p180f693x8z8j67s8s889w8p3hcn8qin18577qhq4sr8fxwq7")))

(define-public crate-scf-core-1.2.0 (c (n "scf-core") (v "1.2.0") (d (list (d (n "lang_extension") (r "^1.0.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "log4rs") (r "^0.8.3") (d #t) (k 2)))) (h "0q20y24lhihp1a3iwcaij1kgrsi8jhrd7c8ncghjdvyn8didvc6i")))

(define-public crate-scf-core-1.2.1 (c (n "scf-core") (v "1.2.1") (d (list (d (n "lang_extension") (r "^1.0.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "log4rs") (r "^0.8.3") (d #t) (k 2)))) (h "0z3712jzvp72l865z0jq9jl9lxlfpbclgniahkvpidq5didczpvm")))

