(define-module (crates-io sc s- scs-rs-proc-macros) #:use-module (crates-io))

(define-public crate-scs-rs-proc-macros-0.1.0 (c (n "scs-rs-proc-macros") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "1f38nndlk1bcwvf9ffn1cvy9x1dnbaky187qi1v76g2d6dds8k7v")))

(define-public crate-scs-rs-proc-macros-0.1.1 (c (n "scs-rs-proc-macros") (v "0.1.1") (d (list (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "0hhiyl8swckfk4nxkls3igzsxywjlbsksgs81rq54asnm35wbc8k")))

