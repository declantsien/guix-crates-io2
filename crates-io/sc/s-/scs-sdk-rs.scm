(define-module (crates-io sc s- scs-sdk-rs) #:use-module (crates-io))

(define-public crate-scs-sdk-rs-0.2.0 (c (n "scs-sdk-rs") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "scs-rs-proc-macros") (r "^0.1") (d #t) (k 0)))) (h "153fad8049sms63d7gy2q4r4knh5y6p4wj7hlnfvm6cdcrjdb92a")))

(define-public crate-scs-sdk-rs-0.2.1 (c (n "scs-sdk-rs") (v "0.2.1") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "scs-rs-proc-macros") (r "^0.1") (d #t) (k 0)))) (h "15y6ha0qcm6jbw1p9rpw710bfh2vm6cs8xqpjcis1nk8vlf65d2l")))

(define-public crate-scs-sdk-rs-0.2.2 (c (n "scs-sdk-rs") (v "0.2.2") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "scs-rs-proc-macros") (r "^0.1") (d #t) (k 0)))) (h "0xkjsp3kbhi0szaw0kdcmm2c24vndmw7z4r0rlsvpn3fmb6x9p6b")))

