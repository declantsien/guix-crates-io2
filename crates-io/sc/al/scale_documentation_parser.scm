(define-module (crates-io sc al scale_documentation_parser) #:use-module (crates-io))

(define-public crate-scale_documentation_parser-0.0.1 (c (n "scale_documentation_parser") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0fg3vl0hhymva6n1vb21i5405akm1lz8g3hpbg5rzmnccjnns7cp")))

(define-public crate-scale_documentation_parser-0.1.0 (c (n "scale_documentation_parser") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1ndi569m46zfimibi919z7iq5v8mdjclcjinfmwdqba48rw44jcv")))

(define-public crate-scale_documentation_parser-0.1.1 (c (n "scale_documentation_parser") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "014vgx3c445359g2hg5yibmw8w87kkk3qbyl6jwy4ilsllh1wxlh")))

(define-public crate-scale_documentation_parser-0.1.2 (c (n "scale_documentation_parser") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "03wp2vbkxmz1vk4dmps44ha8ls9v73a9sp4d80sh91d1rhqlc2kv")))

