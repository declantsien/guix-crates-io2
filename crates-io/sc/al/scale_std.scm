(define-module (crates-io sc al scale_std) #:use-module (crates-io))

(define-public crate-scale_std-0.0.1 (c (n "scale_std") (v "0.0.1") (d (list (d (n "scale") (r "^0.0.1") (d #t) (k 0) (p "scale-core")))) (h "1bhf5dsp00nfb9bqqmkshzgncj7vi27zsndaxd72k2mg8m28jhym") (f (quote (("emulate" "scale/emulate"))))))

(define-public crate-scale_std-0.1.0 (c (n "scale_std") (v "0.1.0") (d (list (d (n "scale") (r "^0.1.0") (d #t) (k 0) (p "scale-core")))) (h "0p13lw74i1wbpwzff578028dhfvv9q5xa1ibx35qb3pnmlh2ya20") (f (quote (("emulate" "scale/emulate"))))))

(define-public crate-scale_std-0.1.2 (c (n "scale_std") (v "0.1.2") (d (list (d (n "scale") (r "^0.1.2") (d #t) (k 0) (p "scale-core")))) (h "0inzxjgdvdi81cqgxqsya1x19i2i0fzp975qmlnd5bdfyrmh37hy") (f (quote (("emulate" "scale/emulate"))))))

