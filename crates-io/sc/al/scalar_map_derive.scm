(define-module (crates-io sc al scalar_map_derive) #:use-module (crates-io))

(define-public crate-scalar_map_derive-0.1.0 (c (n "scalar_map_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.70") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (d #t) (k 0)))) (h "1l5y7vgv8ij3rah4pk79ar44kwcy8mpwbp8n2366gkcfx3r5frli")))

