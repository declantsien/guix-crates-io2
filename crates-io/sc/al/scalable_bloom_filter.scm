(define-module (crates-io sc al scalable_bloom_filter) #:use-module (crates-io))

(define-public crate-scalable_bloom_filter-0.1.0 (c (n "scalable_bloom_filter") (v "0.1.0") (d (list (d (n "siphasher") (r "^0.2") (d #t) (k 0)))) (h "16w4sypm1aaz6qsikk4lj3gp4xah13s67ldir33pvwyax4m4hd7q")))

(define-public crate-scalable_bloom_filter-0.1.1 (c (n "scalable_bloom_filter") (v "0.1.1") (d (list (d (n "siphasher") (r "^0.2") (d #t) (k 0)))) (h "1i2iwzsjs3vjpnfnz60k9xylwma1kbkw1nk0fpp9jhjyvmpl7z1a")))

