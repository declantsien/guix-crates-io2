(define-module (crates-io sc al scales) #:use-module (crates-io))

(define-public crate-scales-0.1.0 (c (n "scales") (v "0.1.0") (d (list (d (n "assert_approx_eq") (r "^1.1") (d #t) (k 2)))) (h "0xj4a9gpdkjang5mvppimr17wgp3dyvi2k735i77cxr48agpa9y3")))

(define-public crate-scales-0.1.1 (c (n "scales") (v "0.1.1") (d (list (d (n "assert_approx_eq") (r "^1.1") (d #t) (k 2)))) (h "15h3rq56wdczxmrdnm5sgcfyvnx0h8i8mkin3cqy4pid5a2d887k")))

(define-public crate-scales-0.1.2 (c (n "scales") (v "0.1.2") (d (list (d (n "assert_approx_eq") (r ">=1.1.0, <2.0.0") (d #t) (k 2)))) (h "07v9cpybyx1zax5s0ix1c5jwdfzkbbdi78chl3x106qqv8qqflck")))

(define-public crate-scales-0.1.3 (c (n "scales") (v "0.1.3") (d (list (d (n "assert_approx_eq") (r ">=1.1.0, <2.0.0") (d #t) (k 2)))) (h "0c88wabnc3bwzd1q4plcm1w9xfbwhm6lrv1v0ag4j94w0mmbfxbj")))

