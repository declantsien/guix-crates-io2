(define-module (crates-io sc al scale-type-resolver) #:use-module (crates-io))

(define-public crate-scale-type-resolver-0.1.0 (c (n "scale-type-resolver") (v "0.1.0") (d (list (d (n "bitvec") (r "^1") (d #t) (k 2)) (d (n "scale-info") (r "^2.7.0") (o #t) (k 0)) (d (n "scale-info") (r "^2.7.0") (f (quote ("std" "bit-vec" "derive"))) (k 2)))) (h "0zb56asxdw726zyq4jkkkddkb5vyfk37lxqf8sfli6gb2cqgfgbr") (f (quote (("default" "scale-info")))) (s 2) (e (quote (("scale-info" "dep:scale-info"))))))

(define-public crate-scale-type-resolver-0.1.1 (c (n "scale-type-resolver") (v "0.1.1") (d (list (d (n "bitvec") (r "^1") (d #t) (k 2)) (d (n "scale-info") (r "^2.7.0") (o #t) (k 0)) (d (n "scale-info") (r "^2.7.0") (f (quote ("std" "bit-vec" "derive"))) (k 2)) (d (n "smallvec") (r "^1.10.0") (o #t) (d #t) (k 0)))) (h "0hv8qwk0za5y8fn63nfn2v5s50k8skh57xln1x73fhzxkc301f0h") (f (quote (("default" "scale-info" "visitor")))) (s 2) (e (quote (("visitor" "dep:smallvec") ("scale-info" "dep:scale-info"))))))

(define-public crate-scale-type-resolver-0.2.0 (c (n "scale-type-resolver") (v "0.2.0") (d (list (d (n "bitvec") (r "^1") (d #t) (k 2)) (d (n "scale-info") (r "^2.7.0") (o #t) (k 0)) (d (n "scale-info") (r "^2.7.0") (f (quote ("std" "bit-vec" "derive"))) (k 2)) (d (n "smallvec") (r "^1.10.0") (o #t) (d #t) (k 0)))) (h "1jxb28sz8d5dsnmznjqm1wprji3hpy0sr21bpv0xc2xa31jyvkgh") (f (quote (("default" "scale-info" "visitor")))) (s 2) (e (quote (("visitor" "dep:smallvec") ("scale-info" "dep:scale-info"))))))

