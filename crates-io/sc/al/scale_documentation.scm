(define-module (crates-io sc al scale_documentation) #:use-module (crates-io))

(define-public crate-scale_documentation-0.0.1 (c (n "scale_documentation") (v "0.0.1") (d (list (d (n "documentation_parser") (r "^0.0.1") (d #t) (k 0) (p "scale_documentation_parser")) (d (n "eyre") (r "^0.6.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "01svirwb6j8y0ijfxvvbx4zkzzv5f0zil8ba90kxcwi416lz5wkb")))

(define-public crate-scale_documentation-0.1.0 (c (n "scale_documentation") (v "0.1.0") (d (list (d (n "documentation_parser") (r "^0.1.0") (d #t) (k 0) (p "scale_documentation_parser")) (d (n "eyre") (r "^0.6.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0da8ccbisfkcd59mc1236nvllmspnglcq0dfyh9gdby4qhvkiv9x")))

(define-public crate-scale_documentation-0.1.2 (c (n "scale_documentation") (v "0.1.2") (d (list (d (n "documentation_parser") (r "^0.1.2") (d #t) (k 0) (p "scale_documentation_parser")) (d (n "eyre") (r "^0.6.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0sdf55zcklk7vjq5680dxyw1dr2pjsd9drsshdy2bs87jhq6s11p")))

