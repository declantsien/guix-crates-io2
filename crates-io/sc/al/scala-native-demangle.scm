(define-module (crates-io sc al scala-native-demangle) #:use-module (crates-io))

(define-public crate-scala-native-demangle-0.0.1 (c (n "scala-native-demangle") (v "0.0.1") (d (list (d (n "clap") (r "^4.4.3") (f (quote ("derive"))) (d #t) (k 0)))) (h "1i1g98jkgffn8pm19fw118hqpmjcrlgb461x3xng4lbi7adcn27l")))

(define-public crate-scala-native-demangle-0.0.2 (c (n "scala-native-demangle") (v "0.0.2") (d (list (d (n "clap") (r "^4.4.3") (f (quote ("derive"))) (d #t) (k 0)))) (h "1zsmdk1zjw99q2mg5xksfhjzh0dgw3wq4pcfiv9ycc062alwragv")))

(define-public crate-scala-native-demangle-0.0.3 (c (n "scala-native-demangle") (v "0.0.3") (d (list (d (n "clap") (r "^4.4.3") (f (quote ("derive"))) (d #t) (k 0)))) (h "19apnvxrz5gqgvxxpz1yf0g7n8q6pcjc68x9lcr9jw2b8gjmrzp0")))

(define-public crate-scala-native-demangle-0.0.4 (c (n "scala-native-demangle") (v "0.0.4") (d (list (d (n "clap") (r "^4.4.3") (f (quote ("derive"))) (d #t) (k 0)))) (h "096wfik4b46qyykpk2hzkjn00cir4g8kj5jbmv6px83zayhwwyr9")))

(define-public crate-scala-native-demangle-0.0.5 (c (n "scala-native-demangle") (v "0.0.5") (d (list (d (n "clap") (r "^4.4.3") (f (quote ("derive"))) (d #t) (k 2)))) (h "1j26flaggw6dd0pmsv0jjddpf63szx6f8yilhalyhwpiln8gcijj")))

(define-public crate-scala-native-demangle-0.0.6 (c (n "scala-native-demangle") (v "0.0.6") (d (list (d (n "clap") (r "^4.4.3") (f (quote ("derive"))) (d #t) (k 2)))) (h "1qniqm4h0ccwry35v58klqdym222vg9rn0x49bh33bqfvknici0a")))

