(define-module (crates-io sc al scalar_map) #:use-module (crates-io))

(define-public crate-scalar_map-0.1.0 (c (n "scalar_map") (v "0.1.0") (h "1jx0bwji6li2id26wzfzcda7j1gnfmfjdm93x4cgfk6g6y2fhimx") (f (quote (("std") ("default" "std"))))))

(define-public crate-scalar_map-0.1.1 (c (n "scalar_map") (v "0.1.1") (h "0r7f2l7pqycchp1sakhj34mg5707cwhw9sndp73q7flyq8i5v5i8") (f (quote (("std"))))))

(define-public crate-scalar_map-0.1.2 (c (n "scalar_map") (v "0.1.2") (h "1b2xhx0m27h0xzkfrgvqhxiv39af3hs9z9as5cnshgk3a4kmhx1m") (f (quote (("std") ("default" "std"))))))

(define-public crate-scalar_map-0.1.3 (c (n "scalar_map") (v "0.1.3") (d (list (d (n "scalar_map_derive") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "0ax4clm8rc7l64c268cbmr6zlwnjsywxr33a0ffky6lg56ym714w") (f (quote (("std") ("default" "std")))) (s 2) (e (quote (("derive" "dep:scalar_map_derive"))))))

(define-public crate-scalar_map-0.1.4 (c (n "scalar_map") (v "0.1.4") (d (list (d (n "scalar_map_derive") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "0flk0p471wv10sf151w3b4cj9crgygy47nm3r8bg0hpjygnsvm2s") (f (quote (("std") ("default" "std")))) (s 2) (e (quote (("derive" "dep:scalar_map_derive"))))))

