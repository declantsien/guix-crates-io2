(define-module (crates-io sc al scale_extension_interfaces) #:use-module (crates-io))

(define-public crate-scale_extension_interfaces-0.1.0 (c (n "scale_extension_interfaces") (v "0.1.0") (h "0h1ficqfcnk6swlzg92x5vrgjl549c4cbmhzd43fr42abrqz99j1")))

(define-public crate-scale_extension_interfaces-0.1.2 (c (n "scale_extension_interfaces") (v "0.1.2") (h "1mqi4cm2bsq98b5kj5rih2h6m70l9x4i97sb24a89628lj0r876w")))

