(define-module (crates-io sc al scala) #:use-module (crates-io))

(define-public crate-scala-0.0.0 (c (n "scala") (v "0.0.0") (h "06m8a8n6zlcz0hqxwjf1yxqc04yincq2dk2pq071w46d1bkmcv5w")))

(define-public crate-scala-0.1.3 (c (n "scala") (v "0.1.3") (h "0hgq89kwjrwlyprxard56vq0f5kyzyblzq6qcrhy6pp2gar3k418")))

