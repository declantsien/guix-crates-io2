(define-module (crates-io sc al scall) #:use-module (crates-io))

(define-public crate-scall-0.1.0 (c (n "scall") (v "0.1.0") (h "0rzcqxn1x27i145sb2pa2ylzfh43w1i17ckk6vy3p9ljw303b3nq")))

(define-public crate-scall-0.2.0 (c (n "scall") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 2)))) (h "0jpai2h8v3jh9mxvsx7gfml31hc09932zy6qi0zbf2qd46mp10k2")))

(define-public crate-scall-0.2.1 (c (n "scall") (v "0.2.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 2)))) (h "0yk057iad8skwjg7spsqiv14pkfni6pwbqy08jjnw5zsjjmxrw3f")))

(define-public crate-scall-0.2.2 (c (n "scall") (v "0.2.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 2)))) (h "0cz54ylqh56vq67ws4hs2d6kyl1f66a23c7jcng1s69h8zxcicc3")))

(define-public crate-scall-0.2.3 (c (n "scall") (v "0.2.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 2)))) (h "1ddf6rq7rb7hxy0ah2hm4mfaa93p6bb12wpxkcq89nq352cr1alx")))

(define-public crate-scall-0.2.4 (c (n "scall") (v "0.2.4") (d (list (d (n "libc") (r "^0.2") (d #t) (k 2)))) (h "0395vdlasb1vdzymmmx51ldaqk6bg4njhhhfzjpkg6msjx8r60zw")))

(define-public crate-scall-0.2.5 (c (n "scall") (v "0.2.5") (d (list (d (n "libc") (r "^0.2") (d #t) (k 2)))) (h "055pd7qvmgj2vcznhq61x1x3c1ary2l9c1vlv5r6m0fmckngw6v7")))

