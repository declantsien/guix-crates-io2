(define-module (crates-io sc al scale_signature_interfaces) #:use-module (crates-io))

(define-public crate-scale_signature_interfaces-0.1.0 (c (n "scale_signature_interfaces") (v "0.1.0") (h "0mj3bn1d18rgyy6chs09innjaxys7hcxwgyvj13s3s7crfppjdh8") (y #t)))

(define-public crate-scale_signature_interfaces-0.1.1 (c (n "scale_signature_interfaces") (v "0.1.1") (h "13j5254hqfahwcqfnjzb17mzbc248m33f7nyfmjaahpjmlv2pmxm") (y #t)))

(define-public crate-scale_signature_interfaces-0.1.2 (c (n "scale_signature_interfaces") (v "0.1.2") (h "0afsnpr73i19a84qvcf9d18gpihb8yi6pcz4vgrrs5hl3cdrnaiv")))

(define-public crate-scale_signature_interfaces-0.1.3 (c (n "scale_signature_interfaces") (v "0.1.3") (h "0bmvgk1pc1413k3nvbnb5mm9g5scchzm96dj940im2qicshf5bq1")))

(define-public crate-scale_signature_interfaces-0.1.4 (c (n "scale_signature_interfaces") (v "0.1.4") (h "1fk2vk8hyask9j098gb2s13mzpqkhdglm0ab2ma2fx4372d0gz06")))

(define-public crate-scale_signature_interfaces-0.1.5 (c (n "scale_signature_interfaces") (v "0.1.5") (h "0ibddgjb8jskr4nyjrf8bj4vaqq0rw5mjsmvvpy76laivq1yj373")))

(define-public crate-scale_signature_interfaces-0.1.6 (c (n "scale_signature_interfaces") (v "0.1.6") (h "0a6j1yzima82y705fq7hmk2199zpfgxgl94d3y4azbqb5gq6g09i")))

(define-public crate-scale_signature_interfaces-0.1.7 (c (n "scale_signature_interfaces") (v "0.1.7") (h "1i84hjv80yk2h65a8g98vw861i7j60f8pjjkx57gq1w6xfd0njwc")))

