(define-module (crates-io sc al scalar_types) #:use-module (crates-io))

(define-public crate-scalar_types-0.1.0 (c (n "scalar_types") (v "0.1.0") (h "0dk8wsnq3xz3p21544kpd1kgym1lid2pxgjj76sqw5ahwaha8dyh")))

(define-public crate-scalar_types-0.1.1 (c (n "scalar_types") (v "0.1.1") (h "0ynmn1ffj0xwpimw4jy989jlw22bkljqxhajf2avynvvv2c7nkkl")))

