(define-module (crates-io sc al scaleway_sdk) #:use-module (crates-io))

(define-public crate-scaleway_sdk-0.1.0 (c (n "scaleway_sdk") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "failure") (r "^0.1.7") (d #t) (k 0)) (d (n "http") (r "^0.2.1") (d #t) (k 2)) (d (n "hyper") (r "^0.13.4") (f (quote ("tcp" "stream"))) (k 2)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.4") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "1yzr47cvmbyv0y635p9qkcsrl049k430jy0i24f7kgv26mflv7vc")))

