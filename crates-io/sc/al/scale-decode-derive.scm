(define-module (crates-io sc al scale-decode-derive) #:use-module (crates-io))

(define-public crate-scale-decode-derive-0.5.0 (c (n "scale-decode-derive") (v "0.5.0") (d (list (d (n "darling") (r "^0.14.2") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1nslg184y07lsxaxnnp6gw7jpspny0m10sxc9swr2hwfyyr431xk")))

(define-public crate-scale-decode-derive-0.6.0 (c (n "scale-decode-derive") (v "0.6.0") (d (list (d (n "darling") (r "^0.14.2") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "scale-decode") (r "^0.5.0") (d #t) (k 2)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "09j5xz4rjf3jdar4hfkjsbrr8a9n1yhnldf7zmxrpnzdp24365mx")))

(define-public crate-scale-decode-derive-0.7.0 (c (n "scale-decode-derive") (v "0.7.0") (d (list (d (n "darling") (r "^0.14.2") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1v3h12ls5zq7vdbc1g1nbhwwb37q29m5v8fjyqsz1436pggz14a3")))

(define-public crate-scale-decode-derive-0.8.0 (c (n "scale-decode-derive") (v "0.8.0") (d (list (d (n "darling") (r "^0.14.2") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0dzz5w4nb3y82xh3gh09769nrp731lql88bj2v8fk5ql6jhxgjb6")))

(define-public crate-scale-decode-derive-0.9.0 (c (n "scale-decode-derive") (v "0.9.0") (d (list (d (n "darling") (r "^0.14.2") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0iscv4l4rgi85yy8flyk4c9gak36msgi1znwfb6ghs2q02v3x1r7")))

(define-public crate-scale-decode-derive-0.10.0 (c (n "scale-decode-derive") (v "0.10.0") (d (list (d (n "darling") (r "^0.14.2") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1nv6d5lbkb1hw2d3rwj55y5mk6hh617rfraw3gypwb5nl4452iyk")))

(define-public crate-scale-decode-derive-0.11.0 (c (n "scale-decode-derive") (v "0.11.0") (d (list (d (n "darling") (r "^0.14.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0gvqwprg8h3k1hg3r8b206z4scdn1jgn2l7xrvx89k9mzj1g4lbv")))

(define-public crate-scale-decode-derive-0.11.1 (c (n "scale-decode-derive") (v "0.11.1") (d (list (d (n "darling") (r "^0.14.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "19fpm8hki36rxgs97xmmlzhkzsfavam86jdckd0wp8xyqyrzv62k")))

(define-public crate-scale-decode-derive-0.12.0 (c (n "scale-decode-derive") (v "0.12.0") (d (list (d (n "darling") (r "^0.14.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0356kw74m6ywm6yzfmbzmh8gzwnrxmxklzlmid6r8na5bybcddvs")))

(define-public crate-scale-decode-derive-0.13.0 (c (n "scale-decode-derive") (v "0.13.0") (d (list (d (n "darling") (r "^0.14.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1yxranimjig57sf5scvbq1id89hgh3yf3ydl5r9yrrib5ak8g8q6")))

