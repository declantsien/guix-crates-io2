(define-module (crates-io sc al scale-encode-derive) #:use-module (crates-io))

(define-public crate-scale-encode-derive-0.0.1 (c (n "scale-encode-derive") (v "0.0.1") (d (list (d (n "proc-macro-crate") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0p3gzga17y4bkyix3ii5s3g5nn3sgkzqr3i29zhab9ad3wpsx895")))

(define-public crate-scale-encode-derive-0.0.2 (c (n "scale-encode-derive") (v "0.0.2") (d (list (d (n "darling") (r "^0.14.2") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "044xd4i8vy0nblm622a5gqiz461mi1zx9l98s58wrd5fb2hgzhvi")))

(define-public crate-scale-encode-derive-0.0.3 (c (n "scale-encode-derive") (v "0.0.3") (d (list (d (n "darling") (r "^0.14.2") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "093ywgfzbjwhqv492xq828r0bh8p5q35fcm64xadfsrhv7piziij")))

(define-public crate-scale-encode-derive-0.0.4 (c (n "scale-encode-derive") (v "0.0.4") (d (list (d (n "darling") (r "^0.14.2") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1ibfwpkl1xzy0zg64qwlh9wwgk8a1cf59h29mk0nl9lfwdifmx3i")))

(define-public crate-scale-encode-derive-0.0.5 (c (n "scale-encode-derive") (v "0.0.5") (d (list (d (n "darling") (r "^0.14.2") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "17xjm8h6rgpk6gkwhr5jc9lcxf5i9ypd8l78582yvlhgpv7ad3cw")))

(define-public crate-scale-encode-derive-0.0.6 (c (n "scale-encode-derive") (v "0.0.6") (d (list (d (n "darling") (r "^0.14.2") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0xzbq7105v4azamhb01di2qjd1mx54yz3d6kyw4myi505pmna1dv")))

(define-public crate-scale-encode-derive-0.0.7 (c (n "scale-encode-derive") (v "0.0.7") (d (list (d (n "darling") (r "^0.14.2") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "17dzabcdvn7ycxrbk3mb85mvbvvvb90isjvf3r1z19bc6slzfmzj")))

(define-public crate-scale-encode-derive-0.0.8 (c (n "scale-encode-derive") (v "0.0.8") (d (list (d (n "darling") (r "^0.14.2") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "04whxc0s7vpmqr2ngdjxsr558lqdzmwfqrxdrdfmxg80r6fcjyqi")))

(define-public crate-scale-encode-derive-0.0.9 (c (n "scale-encode-derive") (v "0.0.9") (d (list (d (n "darling") (r "^0.14.2") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1zyfr7yrypnqj432rj0hsbf6wanx5p6v7f7zwy84ayvbx719vnfd")))

(define-public crate-scale-encode-derive-0.0.10 (c (n "scale-encode-derive") (v "0.0.10") (d (list (d (n "darling") (r "^0.14.2") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1h9g4mdsw9bfa14dbbxr6v8njf18lz0gs941c8qh5z8sdz9c8qdq")))

(define-public crate-scale-encode-derive-0.0.11 (c (n "scale-encode-derive") (v "0.0.11") (d (list (d (n "darling") (r "^0.14.2") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "15x78h78q16ylddilps4q46rja4zirwxrfsdxjz8pnmr8k6qmhd0")))

(define-public crate-scale-encode-derive-0.0.12 (c (n "scale-encode-derive") (v "0.0.12") (d (list (d (n "darling") (r "^0.14.2") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0lgc1r5by2hzbg6vhjg5vy9mgjrp98xswd3galn676h1s7miaqzd")))

(define-public crate-scale-encode-derive-0.0.13 (c (n "scale-encode-derive") (v "0.0.13") (d (list (d (n "darling") (r "^0.14.2") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "00iqchw3la875h3j2hg033bn2h1lf4gw82a389qf8wpn0rj858rj")))

(define-public crate-scale-encode-derive-0.0.14 (c (n "scale-encode-derive") (v "0.0.14") (d (list (d (n "darling") (r "^0.14.2") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1dp5c65m97kz8w5siwjbrd3zfj0h9rw093bs5s4iwvjbr8kjgz2x")))

(define-public crate-scale-encode-derive-0.0.15 (c (n "scale-encode-derive") (v "0.0.15") (d (list (d (n "darling") (r "^0.14.2") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1ikr47zp5q6l9d5ac53l0f4jyn5xvr8z6nknf5z6svyphvvad09v")))

(define-public crate-scale-encode-derive-0.0.16 (c (n "scale-encode-derive") (v "0.0.16") (d (list (d (n "darling") (r "^0.14.2") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0mcgjy1ib18wskkyksykpx3ynnh7k1lxd1dzy923n8kmh44d0504")))

(define-public crate-scale-encode-derive-0.0.17 (c (n "scale-encode-derive") (v "0.0.17") (d (list (d (n "darling") (r "^0.14.2") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0qj9k82nffm1gmfhbjvkyaja93sg9fhghz5frdyq75223szywgrp")))

(define-public crate-scale-encode-derive-0.0.18 (c (n "scale-encode-derive") (v "0.0.18") (d (list (d (n "darling") (r "^0.14.2") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0wdi12zr8805h9b3d4qili32fn832gm1mxp1i20j9fagg92z4fcb")))

(define-public crate-scale-encode-derive-0.1.0 (c (n "scale-encode-derive") (v "0.1.0") (d (list (d (n "darling") (r "^0.14.2") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0dxad0bwb0b4bv25rpl9bi16v3gvzcpszv8g0yw764qcbdw1za83")))

(define-public crate-scale-encode-derive-0.1.1 (c (n "scale-encode-derive") (v "0.1.1") (d (list (d (n "darling") (r "^0.14.2") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1fadcarqdwcpqw3g2f97c7l724pc4af7fa4a2gccj0abp7dhq93d")))

(define-public crate-scale-encode-derive-0.1.2 (c (n "scale-encode-derive") (v "0.1.2") (d (list (d (n "darling") (r "^0.14.2") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "10l04v0cadhqx7ajzhas2ypx2hp5dn5d3sjlhl9pdzggm7q3r66x")))

(define-public crate-scale-encode-derive-0.2.0 (c (n "scale-encode-derive") (v "0.2.0") (d (list (d (n "darling") (r "^0.14.2") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "scale-encode") (r "^0.1.2") (d #t) (k 2)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1j6l4fxk5bvc3nnrc24jbgqwdr3hk4fkz1iblfslpr129n840kn4")))

(define-public crate-scale-encode-derive-0.3.0 (c (n "scale-encode-derive") (v "0.3.0") (d (list (d (n "darling") (r "^0.14.2") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "19pmjjih2m2yydgmp37gh2ms8cp3nnx43mibh9kf5zn01sqhyvii")))

(define-public crate-scale-encode-derive-0.4.0 (c (n "scale-encode-derive") (v "0.4.0") (d (list (d (n "darling") (r "^0.14.2") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1ww8ihhzygrqdscx8j4pd9mzmprbp4lw1qqvy0nnl5c7fy3y32zj")))

(define-public crate-scale-encode-derive-0.5.0 (c (n "scale-encode-derive") (v "0.5.0") (d (list (d (n "darling") (r "^0.14.2") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "09bsf2j9awj6lm80fla7xascxgrj1qa4d9ynjvm6pp7g23qr2m4r")))

(define-public crate-scale-encode-derive-0.6.0 (c (n "scale-encode-derive") (v "0.6.0") (d (list (d (n "darling") (r "^0.14.2") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0g13cgdfl4q5s6ri9p6svvn8rk2n2ir15c089sifgyydywd4wc3s")))

(define-public crate-scale-encode-derive-0.7.0 (c (n "scale-encode-derive") (v "0.7.0") (d (list (d (n "darling") (r "^0.14.2") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1knxhkg573ly07xknnskba3fw5y1l40rjbwgl28l0vn1qxrd8jhs")))

(define-public crate-scale-encode-derive-0.7.1 (c (n "scale-encode-derive") (v "0.7.1") (d (list (d (n "darling") (r "^0.14.2") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1gy536xc11jadd7h7z2gc9g8wh84dyr2figl0mqx9j6rw9h7xaw2")))

