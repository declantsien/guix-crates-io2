(define-module (crates-io sc al scalc) #:use-module (crates-io))

(define-public crate-scalc-0.1.0 (c (n "scalc") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "paste") (r "^0.1") (d #t) (k 0)))) (h "01yhalawphfrixyn3yy273k8qm19zw3ziw2makv3wxprfb846czg")))

(define-public crate-scalc-0.2.0 (c (n "scalc") (v "0.2.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "paste") (r "^0.1") (d #t) (k 0)))) (h "1sdv67nk88f4wr0fiscwjiqbq10lfvjiypzgan093ak6mlzjx17x")))

(define-public crate-scalc-0.2.1 (c (n "scalc") (v "0.2.1") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "paste") (r "^0.1") (d #t) (k 0)))) (h "1w0dkpvvcmxj1m1z61llkf4jp9022s2vjw9rvr4sqbw56xmcbj5g")))

