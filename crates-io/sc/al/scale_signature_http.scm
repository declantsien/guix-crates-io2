(define-module (crates-io sc al scale_signature_http) #:use-module (crates-io))

(define-public crate-scale_signature_http-0.0.1 (c (n "scale_signature_http") (v "0.0.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "polyglot_rs") (r "^0.1.0") (d #t) (k 0)) (d (n "scale_signature") (r "^0.0.4") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4.5") (d #t) (k 0)))) (h "0zrnxj0yzq2xvf1zdkjjfigiw5g90n3iibwp0a44n7kc7zva52ri") (y #t)))

(define-public crate-scale_signature_http-0.0.2 (c (n "scale_signature_http") (v "0.0.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "polyglot_rs") (r "^0.1.0") (d #t) (k 0)) (d (n "scale_signature") (r "^0.0.4") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4.5") (d #t) (k 0)))) (h "0kbqadf295pnmy5k1jy21w67x1p3jxpvaq5jv8vh430n6mjl94dy") (y #t)))

(define-public crate-scale_signature_http-0.0.3 (c (n "scale_signature_http") (v "0.0.3") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "polyglot_rs") (r "^0.1.0") (d #t) (k 0)) (d (n "scale_signature") (r "^0.0.4") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4.5") (d #t) (k 0)))) (h "1k9rb6bapif507diy2phsihsx0596i7ifva23737qw4m1f80kiw0")))

(define-public crate-scale_signature_http-0.0.4 (c (n "scale_signature_http") (v "0.0.4") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "polyglot_rs") (r "^0.1.0") (d #t) (k 0)) (d (n "scale_signature") (r "^0.0.4") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4.5") (d #t) (k 0)))) (h "0424drl0nv27ynk6gk5q4wzihl7h5j4qj7arqlix7q7sg1kzgyhz")))

(define-public crate-scale_signature_http-0.1.6 (c (n "scale_signature_http") (v "0.1.6") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "polyglot_rs") (r "^0.6.0") (d #t) (k 0)) (d (n "scale_signature") (r "^0.2.0") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4.5") (d #t) (k 0)))) (h "05ir4iakbz5vvm8jzwm6bjx22a7r8bvw26m8xvblp7f9z3pw3q2y")))

(define-public crate-scale_signature_http-0.2.0 (c (n "scale_signature_http") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "polyglot_rs") (r "^0.6.0") (d #t) (k 0)) (d (n "scale_signature") (r "^0.2.0") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4.5") (d #t) (k 0)))) (h "1sh0w6aqz0crsz0lkq0774bxacq7f7bq1hs5kzj4aw672d62m3j1")))

(define-public crate-scale_signature_http-0.2.1 (c (n "scale_signature_http") (v "0.2.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "polyglot_rs") (r "^0.6.0") (d #t) (k 0)) (d (n "scale_signature") (r "^0.2.0") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4.5") (d #t) (k 0)))) (h "0xigkfjwabxmjac1gwy2p6wnnv0g9vs36p3cq8sp4kjimzzd4c54")))

(define-public crate-scale_signature_http-0.2.2 (c (n "scale_signature_http") (v "0.2.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "polyglot_rs") (r "^0.6.0") (d #t) (k 0)) (d (n "scale_signature") (r "^0.2.0") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4.5") (d #t) (k 0)))) (h "1ag01x3dn7g7575315k118zi0dikcnz36gqpwhrhbwba2if60qm2")))

(define-public crate-scale_signature_http-0.2.3 (c (n "scale_signature_http") (v "0.2.3") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "polyglot_rs") (r "^0.6.0") (d #t) (k 0)) (d (n "scale_signature") (r "^0.2.0") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4.5") (d #t) (k 0)))) (h "1hfj1ylfv0798dvfbbym60yb47kjb0v59w58gad7f3k09qf7myvc")))

(define-public crate-scale_signature_http-0.2.4 (c (n "scale_signature_http") (v "0.2.4") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "polyglot_rs") (r "^0.6.0") (d #t) (k 0)) (d (n "scale_signature") (r "^0.2.1") (d #t) (k 0)))) (h "1f8ih32c4fb4av99s0q8gzzd65cj4prwf6ays3ym5nry65msjsag")))

(define-public crate-scale_signature_http-0.2.5 (c (n "scale_signature_http") (v "0.2.5") (d (list (d (n "polyglot_rs") (r "^0.6.1") (d #t) (k 0)) (d (n "scale_signature") (r "^0.2.1") (d #t) (k 0)))) (h "0qa9x520plaal1945vdyfxfrbg9gxzn6z0mpsqi75mav4wlg4svh")))

(define-public crate-scale_signature_http-0.3.0 (c (n "scale_signature_http") (v "0.3.0") (d (list (d (n "polyglot_rs") (r "^0.6.1") (d #t) (k 0)) (d (n "scale_signature") (r "^0.2.2") (d #t) (k 0)))) (h "1lbalvg9a8jnys4nfcd1klsh9f2nkf38q6gj50hvahbh0vn3m1cj")))

(define-public crate-scale_signature_http-0.3.1 (c (n "scale_signature_http") (v "0.3.1") (d (list (d (n "polyglot_rs") (r "^0.6.1") (d #t) (k 0)) (d (n "scale_signature") (r "^0.2.3") (d #t) (k 0)))) (h "08c05czmw2biv9x81s2937d4aql70z7kh8hrrwmhkiww1s95jann")))

(define-public crate-scale_signature_http-0.3.2 (c (n "scale_signature_http") (v "0.3.2") (d (list (d (n "polyglot_rs") (r "^0.6.1") (d #t) (k 0)) (d (n "scale_signature") (r "^0.2.7") (d #t) (k 0)))) (h "19lr2m6jfwsv47f6g01mnhfcj40vch9qsm1lwhqhs3jmn1kyga84")))

(define-public crate-scale_signature_http-0.3.3 (c (n "scale_signature_http") (v "0.3.3") (d (list (d (n "polyglot_rs") (r "^0.6.1") (d #t) (k 0)) (d (n "scale_signature") (r "^0.2.8") (d #t) (k 0)))) (h "0ddfr3j2iy98cn9s6vsnv9ip4hq79mmpr2plyzd7cyjqsb9bk4qx")))

(define-public crate-scale_signature_http-0.3.4 (c (n "scale_signature_http") (v "0.3.4") (d (list (d (n "polyglot_rs") (r "^0.6.1") (d #t) (k 0)) (d (n "scale_signature") (r "^0.2.9") (d #t) (k 0)))) (h "1fk6b771slkxm0iiwfxpdnqgcr3ly73ifn463x7zr8q051fiqa0j")))

(define-public crate-scale_signature_http-0.3.6 (c (n "scale_signature_http") (v "0.3.6") (d (list (d (n "polyglot_rs") (r "^0.6.1") (d #t) (k 0)) (d (n "scale_signature") (r "^0.2.10") (d #t) (k 0)))) (h "0fw0xl1gmjr5krps4b90pdpvil2fryibkpksmxm15cvfn4yvmkhb")))

(define-public crate-scale_signature_http-0.3.7 (c (n "scale_signature_http") (v "0.3.7") (d (list (d (n "polyglot_rs") (r "^0.6.1") (d #t) (k 0)) (d (n "scale_signature") (r "^0.2.11") (d #t) (k 0)))) (h "00s8j692j9j14rc3da47cdr98mgi9l16r00mx6iawhv7j839yfsy")))

(define-public crate-scale_signature_http-0.3.8 (c (n "scale_signature_http") (v "0.3.8") (d (list (d (n "polyglot_rs") (r "^0.6.1") (d #t) (k 0)) (d (n "scale_signature") (r "^0.2.11") (d #t) (k 0)))) (h "0625s616i2zlw8pr753i19jnj89ypnj722gkajjv8glbl6rmq7np")))

