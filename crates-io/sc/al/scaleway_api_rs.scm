(define-module (crates-io sc al scaleway_api_rs) #:use-module (crates-io))

(define-public crate-scaleway_api_rs-0.1.0 (c (n "scaleway_api_rs") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "14cgzyl5ja288d4nr8qkwbl6djk2y5pl8fzdbpdmykam4hr422l5")))

(define-public crate-scaleway_api_rs-0.1.1 (c (n "scaleway_api_rs") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "158zi0y4hfzjhg9wj7ih91m0d62ws01p5zhrcgf20zsrg4l91vz5")))

(define-public crate-scaleway_api_rs-0.1.2 (c (n "scaleway_api_rs") (v "0.1.2") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "17dlwzqpm7ybfnail34zwrf125sjhifcfxv75g6hsi4b5bfj01nj")))

(define-public crate-scaleway_api_rs-0.1.4 (c (n "scaleway_api_rs") (v "0.1.4") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "0madcj8fg8d8fdbgqxn5dk9v47xrssgv3imz4sr492rrz5blg9qy")))

(define-public crate-scaleway_api_rs-0.1.5 (c (n "scaleway_api_rs") (v "0.1.5") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "1c7a11m9p73c28rb4mc914jcy859i1q1y5xlpjhfkrvs9bv5g5rj")))

(define-public crate-scaleway_api_rs-0.1.6 (c (n "scaleway_api_rs") (v "0.1.6") (d (list (d (n "reqwest") (r "^0.11.14") (f (quote ("json" "multipart"))) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.152") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.93") (d #t) (k 0)) (d (n "serde_with") (r "^2.2.0") (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "1c4ih3cc558z8hahdx5778c82ayx3dlsrg3l51znw2kmz937pfjl")))

(define-public crate-scaleway_api_rs-0.1.7 (c (n "scaleway_api_rs") (v "0.1.7") (d (list (d (n "reqwest") (r "^0.11.14") (f (quote ("json" "multipart"))) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.152") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.93") (d #t) (k 0)) (d (n "serde_with") (r "^2.2.0") (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "1063klcg3vw3ja4ibq39a8ay2hiy3ywc4sqcaxg50pp73a8qy74r")))

