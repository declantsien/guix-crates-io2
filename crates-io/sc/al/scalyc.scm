(define-module (crates-io sc al scalyc) #:use-module (crates-io))

(define-public crate-scalyc-0.0.1 (c (n "scalyc") (v "0.0.1") (h "1ck59k9id7izvmv6gd42r9j2gjmy86yfmx04inpbg3j97as67341")))

(define-public crate-scalyc-0.0.2 (c (n "scalyc") (v "0.0.2") (d (list (d (n "scaly") (r "^0.0.2") (d #t) (k 0)))) (h "1909af008qgrhf3d2nfqnan61fq7m0j34mngwgwr1qp789lm9ndv")))

(define-public crate-scalyc-0.0.3 (c (n "scalyc") (v "0.0.3") (d (list (d (n "scaly") (r "^0.0.3") (d #t) (k 0)))) (h "0jsh33fprjwl5xwmg32rpfzh4d1f1hsyagy3sl02bkfz4dffdd7k")))

(define-public crate-scalyc-0.0.4 (c (n "scalyc") (v "0.0.4") (d (list (d (n "scaly") (r "^0.0.4") (d #t) (k 0)))) (h "1x9p3hmqjwm7np8w0qfsa22rphsydsjqhqpvhkcdc0abzqz4jcmg")))

(define-public crate-scalyc-0.0.5 (c (n "scalyc") (v "0.0.5") (d (list (d (n "scaly") (r "^0.0.5") (d #t) (k 0)))) (h "1vrw3qb3qamssrlgdn72vk0cdsn5yz0l481vglh8pacb282r3bsx")))

(define-public crate-scalyc-0.0.6 (c (n "scalyc") (v "0.0.6") (h "0p4lb2b56scpfb4xcxp95w9dackp52y3cbgbg2mp94mx6ahrf2cl")))

