(define-module (crates-io sc al scale_signature) #:use-module (crates-io))

(define-public crate-scale_signature-0.0.1 (c (n "scale_signature") (v "0.0.1") (h "050ifg20pivk6781gy83q4x2h0msj7wjfd98s5hyzl181dgin3lp")))

(define-public crate-scale_signature-0.0.2 (c (n "scale_signature") (v "0.0.2") (h "0f11y0a59rs08dc0k4pgscxvrm297cg2m9fmkiy4qqnincaiy604")))

(define-public crate-scale_signature-0.0.3 (c (n "scale_signature") (v "0.0.3") (h "03r3m3jiq4cfx0p3kzakz9l3glwpylzizl2ixdvqnza5y6y9dkhp")))

(define-public crate-scale_signature-0.0.4 (c (n "scale_signature") (v "0.0.4") (h "0k8ym4g38iqqnwlpm27vvsb8dhzzx0vs13a1hsjm4lidkrlklp18")))

(define-public crate-scale_signature-0.0.5 (c (n "scale_signature") (v "0.0.5") (d (list (d (n "scale_signature_http") (r "^0.0.1") (d #t) (k 0)))) (h "10cypdkakqx70b93gdh896kcb736a6lyicds6nag75gai9kshd3a")))

(define-public crate-scale_signature-0.1.4 (c (n "scale_signature") (v "0.1.4") (h "09sgpqsvy00da5zhf6pv24vhwx546ys4l97pp26p7l02lj31vd0r")))

(define-public crate-scale_signature-0.1.5 (c (n "scale_signature") (v "0.1.5") (h "0kn1yf9hi5zl9i3arck1jgmrl6ialql36yxhhcsxck4wpd4s242k")))

(define-public crate-scale_signature-0.1.6 (c (n "scale_signature") (v "0.1.6") (h "06wp8f0ww5wbrx1i8niqdzkbhlpw09d15hqsrmyys99acjps2dqh")))

(define-public crate-scale_signature-0.1.7 (c (n "scale_signature") (v "0.1.7") (h "1bbkg7l0ky3kjdkyh0liv2fh3g8a9pzyggzd9dgwh0ai1kdsqdy6")))

(define-public crate-scale_signature-0.1.8 (c (n "scale_signature") (v "0.1.8") (h "1iyjbnrfcq5i12q51z63fjicwgs7fq4jgs9qqxxwn5ykx2y7b1hg")))

(define-public crate-scale_signature-0.2.0 (c (n "scale_signature") (v "0.2.0") (h "1kpx1pkfnl1bw5rbv2zn53gfy7wm50v74vp3d1c1snx7vkc85lim")))

(define-public crate-scale_signature-0.2.1 (c (n "scale_signature") (v "0.2.1") (h "18ghb3cnz0jv41hac0g574j1i90lqv4i861hmrfzia2mg3sqlf6h")))

(define-public crate-scale_signature-0.2.2 (c (n "scale_signature") (v "0.2.2") (h "0c9wg46y6k89fp1sdcqibhpk6jgcabkhc73zkvgrnw670ym3l6vy")))

(define-public crate-scale_signature-0.2.3 (c (n "scale_signature") (v "0.2.3") (h "1sh2vjm9jafnyhpcg1sdbj42pcf9ykxn15v2jgvca6jqwdfbq9md")))

(define-public crate-scale_signature-0.2.5 (c (n "scale_signature") (v "0.2.5") (h "0abddrnyv6qlnglj03cy013xmp3j11q1gd6xlgn7dw1q2ihxkbkh")))

(define-public crate-scale_signature-0.2.6 (c (n "scale_signature") (v "0.2.6") (h "1zh3jpfc2qjv4rrqw7w1s77pqf0xc35lsk7jh88fx7ssbz8zpqn9")))

(define-public crate-scale_signature-0.2.7 (c (n "scale_signature") (v "0.2.7") (h "10n7b8q36z08q7nqgvhqbkrvzyjcyb3yl190md2f6i2fkzfns9qb")))

(define-public crate-scale_signature-0.2.8 (c (n "scale_signature") (v "0.2.8") (h "07xzvmgrhmlvi2sja5gqb8zsihpciffg1wx7dja6bxmrji5vfy2i")))

(define-public crate-scale_signature-0.2.9 (c (n "scale_signature") (v "0.2.9") (h "1czfk4ndld92zgs55rk1vrbg8ibvx3aaawai2b0wj868ynb9kj8m")))

(define-public crate-scale_signature-0.2.10 (c (n "scale_signature") (v "0.2.10") (h "0igni8z7465aw2apfgkgg10i9ixjjgrfgs1jnf0f3azirpxh7vgm")))

(define-public crate-scale_signature-0.2.11 (c (n "scale_signature") (v "0.2.11") (h "1kzqis2nz3mvkh0kmz36zl7gbd8hwxihc3f92qsm8n981apdhrmc")))

