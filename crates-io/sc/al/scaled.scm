(define-module (crates-io sc al scaled) #:use-module (crates-io))

(define-public crate-scaled-0.0.1 (c (n "scaled") (v "0.0.1") (d (list (d (n "euclid") (r "0.20.*") (d #t) (k 0)) (d (n "fixed") (r "0.5.*") (d #t) (k 0)) (d (n "fixed-sqrt") (r "0.2.*") (d #t) (k 0)))) (h "0knx0m8as6p77xhclxgvwwrraah2yx5n844n13rfg0bc5vhyw5ca")))

(define-public crate-scaled-0.0.2 (c (n "scaled") (v "0.0.2") (d (list (d (n "euclid") (r "0.20.*") (d #t) (k 0)) (d (n "fixed") (r "1.*") (d #t) (k 0)) (d (n "fixed-sqrt") (r "0.2.*") (d #t) (k 0)))) (h "1ggbys7i6383kx6dld4wd7jcgj8lm3psrw6blwsjrc9j6jsqbj4w")))

(define-public crate-scaled-0.0.3 (c (n "scaled") (v "0.0.3") (d (list (d (n "euclid") (r "0.22.*") (d #t) (k 0)) (d (n "fixed") (r "1.*") (d #t) (k 0)) (d (n "fixed-sqrt") (r "0.2.*") (d #t) (k 0)))) (h "124sdvlnmn13mfnj2wbchcgfr285dkn8zr08yzlrcs369w0xp1jj")))

