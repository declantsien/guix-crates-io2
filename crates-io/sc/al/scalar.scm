(define-module (crates-io sc al scalar) #:use-module (crates-io))

(define-public crate-scalar-0.1.0 (c (n "scalar") (v "0.1.0") (h "0f67ff8p58s8143sha2rzs8i9h3yisrjlmww5gi52ain4ngpx4si")))

(define-public crate-scalar-0.1.1 (c (n "scalar") (v "0.1.1") (h "18v5pjlxyd0l8viw6h7y3ca1x1cy4mf7rav75s59g23kg9nz70lp")))

(define-public crate-scalar-0.1.2 (c (n "scalar") (v "0.1.2") (h "07b49049a5rynd015m736bm5b1wq0bcv99lhpg3b167w96p4z76q")))

(define-public crate-scalar-0.1.3 (c (n "scalar") (v "0.1.3") (h "14izcs9z77sg9cgcgv9i16w93ax2df9ma1czpkm49md1z7c36wsm")))

(define-public crate-scalar-0.1.4 (c (n "scalar") (v "0.1.4") (h "1wjgb55vpmqk526aghwhxdkzj6p539szsz68pmvq8i76qb64mr2c") (y #t)))

(define-public crate-scalar-0.1.5 (c (n "scalar") (v "0.1.5") (h "1wmxj246dwv5yks0c0s7ap2nrxw6fb11rqajr0mxq67xqpfny8km") (y #t)))

(define-public crate-scalar-0.1.6 (c (n "scalar") (v "0.1.6") (h "1d5azxdrlksp2wsz3ya4cpwbk1ywg7hwza7daq0072q9q55vy1pv")))

(define-public crate-scalar-0.1.7 (c (n "scalar") (v "0.1.7") (h "17zf9fqy1khbb8nwa9mn02sak569dkk0hgsgizr5g7w9d4kmgdfr")))

(define-public crate-scalar-0.1.8 (c (n "scalar") (v "0.1.8") (h "0adajrdsqy4w3aml93w8h8xq63pkbc5fhx4pwr1zb70ckvns9y4b")))

