(define-module (crates-io sc al scaling) #:use-module (crates-io))

(define-public crate-scaling-0.1.0 (c (n "scaling") (v "0.1.0") (h "07gk7m50ya8fw4izqh0b46hfkcg03lscqmvfr4md1v28bnzqldb0")))

(define-public crate-scaling-0.1.1 (c (n "scaling") (v "0.1.1") (h "0ij2dkn8j8nb16kf9irawkpjl6yqpg9dhd0hvyh3i2326ps0qx9h")))

(define-public crate-scaling-0.1.2 (c (n "scaling") (v "0.1.2") (h "1wg1h7zhfmvpbpdy741nxxjrxzgqp7lgbxj4gbjifdmc9h74jxgj")))

(define-public crate-scaling-0.1.3 (c (n "scaling") (v "0.1.3") (h "0bxyq4y69i89asg82749br3n7zsi55l1nrk737189s0whl03dbmx")))

