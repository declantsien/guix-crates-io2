(define-module (crates-io sc al scalar-field) #:use-module (crates-io))

(define-public crate-scalar-field-0.1.0 (c (n "scalar-field") (v "0.1.0") (h "0kasmnq7954nvx2rxmip91yxiadqgj55sqrzdq9yh6z2bwidhdg8")))

(define-public crate-scalar-field-0.1.1 (c (n "scalar-field") (v "0.1.1") (h "0mzny8jqq3qvi2d803m7076821rhkqiy4lcv8kjnvzivq7q70x1k")))

(define-public crate-scalar-field-0.1.2 (c (n "scalar-field") (v "0.1.2") (h "1ws494vix4xr9739n7lsiv90hz63hgpmk28jqadihhyp81xh3996")))

(define-public crate-scalar-field-0.1.3 (c (n "scalar-field") (v "0.1.3") (h "1h5ccl714wqhbs9lhacz6c016n4yfs403y6d8znh6vjzhljlpc5y")))

(define-public crate-scalar-field-0.1.4 (c (n "scalar-field") (v "0.1.4") (h "0m7zazh0d8sz06dqybkqyq6nj5cmfxw3lbq5jjkrhl8v3mrq7j8s")))

