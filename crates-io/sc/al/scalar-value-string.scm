(define-module (crates-io sc al scalar-value-string) #:use-module (crates-io))

(define-public crate-scalar-value-string-1.0.0 (c (n "scalar-value-string") (v "1.0.0") (d (list (d (n "enumflags2") (r "^0.7.5") (d #t) (k 0)) (d (n "once_cell") (r "^1.16.0") (d #t) (k 0)))) (h "0y8vl15hmzhaxigrll5123v362bp4nkknsm067vj9dn0z47jvarg")))

(define-public crate-scalar-value-string-1.0.1 (c (n "scalar-value-string") (v "1.0.1") (d (list (d (n "enumflags2") (r "^0.7.5") (d #t) (k 0)) (d (n "once_cell") (r "^1.16.0") (d #t) (k 0)))) (h "1gkrxs5i9hcwapnbcm4b4nrv9q2sz9byzf46kjlvaz3swvkyhs72")))

