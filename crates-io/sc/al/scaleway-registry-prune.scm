(define-module (crates-io sc al scaleway-registry-prune) #:use-module (crates-io))

(define-public crate-scaleway-registry-prune-0.0.0 (c (n "scaleway-registry-prune") (v "0.0.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "humantime") (r "^2.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.4") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "06hyryrwhsmj69grxg769500dx4niv12fzl5faqqbhhh70j9wm46")))

(define-public crate-scaleway-registry-prune-0.1.0 (c (n "scaleway-registry-prune") (v "0.1.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.7") (d #t) (k 0)) (d (n "humantime") (r "^2.0") (d #t) (k 0)) (d (n "indicatif") (r "^0.14") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "scaleway_sdk") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "08dscw9zclfijgbbs9qnnhqvkml1xg19qq8n1whjvprfax30d4cc")))

