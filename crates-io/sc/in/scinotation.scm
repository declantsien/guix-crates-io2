(define-module (crates-io sc in scinotation) #:use-module (crates-io))

(define-public crate-scinotation-0.0.1 (c (n "scinotation") (v "0.0.1") (h "1bi59g691sjmv18h345ai945s3rjis81svfnbh23kk993cqk1942")))

(define-public crate-scinotation-0.0.2 (c (n "scinotation") (v "0.0.2") (h "0zcbn06kklvnvz3bxrlrbf3brwaanzv9bws82r3d5cndgfc1mqv1")))

