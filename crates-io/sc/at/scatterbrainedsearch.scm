(define-module (crates-io sc at scatterbrainedsearch) #:use-module (crates-io))

(define-public crate-scatterbrainedsearch-0.2.0 (c (n "scatterbrainedsearch") (v "0.2.0") (h "0assihif7qaj4vbpix3a48mqvhjhpc4mj0729zq0r1zhg4c6hi5i") (y #t)))

(define-public crate-scatterbrainedsearch-0.2.1 (c (n "scatterbrainedsearch") (v "0.2.1") (h "14cbv8i9gz94srs66g3bwihmx9vqggypwbmjg1i3l843aj0gpwci")))

(define-public crate-scatterbrainedsearch-0.3.0 (c (n "scatterbrainedsearch") (v "0.3.0") (h "0n0h2pm5rfl68s8403lg5vhbn2nkqlr3gj2yhqsfkdis5jzmn7ms")))

(define-public crate-scatterbrainedsearch-1.0.0 (c (n "scatterbrainedsearch") (v "1.0.0") (h "0csr2rc6519nwv87gxnbc83718kl0ys27xsmy8dmx4zxc1vk47b5") (y #t)))

(define-public crate-scatterbrainedsearch-1.0.1 (c (n "scatterbrainedsearch") (v "1.0.1") (h "0cc6kyzsx21gllr1dyhs1wl5jx4f3a13r77laa13plzyy81z56wd")))

(define-public crate-scatterbrainedsearch-1.0.2 (c (n "scatterbrainedsearch") (v "1.0.2") (h "115biha62azcmfiv3gmbqgpybiv53m40ks4ha0rhpmi5w2cjxba9")))

(define-public crate-scatterbrainedsearch-1.0.3 (c (n "scatterbrainedsearch") (v "1.0.3") (h "1x61wgwpal995llr1pvb6s3ayglpjbjl4mpyrq84ymhz0n3c8jcx")))

(define-public crate-scatterbrainedsearch-2.0.0 (c (n "scatterbrainedsearch") (v "2.0.0") (h "14n9kkx3zjzyhbs337pj1x1980djf2rrc9y90vhlx7rindfhvagp")))

(define-public crate-scatterbrainedsearch-2.5.0 (c (n "scatterbrainedsearch") (v "2.5.0") (h "0v39mlfczbmi8sav5lw3flym817nvkjpx3hmqsnpama2qh6f8ckz")))

