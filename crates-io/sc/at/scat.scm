(define-module (crates-io sc at scat) #:use-module (crates-io))

(define-public crate-scat-0.1.0 (c (n "scat") (v "0.1.0") (h "00cnn8w6ra971w1yapqxjz2l77dmz69lfbljfvfnmvpl2hi8zfz1")))

(define-public crate-scat-0.1.1 (c (n "scat") (v "0.1.1") (h "1d7yarxdq1fljc0dw1l17n54ll897ahywjcr4hrd3f6y8affzqmb")))

