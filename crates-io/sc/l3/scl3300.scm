(define-module (crates-io sc l3 scl3300) #:use-module (crates-io))

(define-public crate-scl3300-0.1.0 (c (n "scl3300") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "libm") (r "^0.2") (o #t) (d #t) (k 0)))) (h "0pi7vn0jjrk5qyi027gznlbcwvmhqp01lv5zpkayspmxsisazlhz") (f (quote (("default" "libm"))))))

(define-public crate-scl3300-0.2.0 (c (n "scl3300") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.8") (d #t) (k 2)) (d (n "libm") (r "^0.2") (o #t) (d #t) (k 0)))) (h "0zg4lhcwpd36bv8apr56yyb4rgf8fgi0g02y5ld1mygj9gxicx71") (f (quote (("default" "libm"))))))

(define-public crate-scl3300-0.3.0 (c (n "scl3300") (v "0.3.0") (d (list (d (n "bitflags") (r "^2.3") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.9") (d #t) (k 2)) (d (n "libm") (r "^0.2") (o #t) (d #t) (k 0)))) (h "17y729a7dq8c7w06745hw0z8a70hlv718a38r54kbixz2j7yr3hq") (f (quote (("default" "libm"))))))

(define-public crate-scl3300-0.4.0 (c (n "scl3300") (v "0.4.0") (d (list (d (n "bitflags") (r "^2") (d #t) (k 0)) (d (n "embedded-hal") (r "^1") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.10") (d #t) (k 2)) (d (n "libm") (r "^0.2") (o #t) (d #t) (k 0)))) (h "1wj4x7h5vis02nckgpnwxjr0ji8k0wrx2gp19sxql6kfaqz06p3l") (f (quote (("default" "libm"))))))

(define-public crate-scl3300-0.5.0 (c (n "scl3300") (v "0.5.0") (d (list (d (n "bitflags") (r "^2") (d #t) (k 0)) (d (n "embedded-hal") (r "^1") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.10") (d #t) (k 2)) (d (n "libm") (r "^0.2") (o #t) (d #t) (k 0)))) (h "1fyh333m8ir1rmdpf60jhkziymjp0x68f2cs4aqifh7rhb73jiwp") (f (quote (("default" "libm"))))))

