(define-module (crates-io sc on scones_examples) #:use-module (crates-io))

(define-public crate-scones_examples-0.1.0 (c (n "scones_examples") (v "0.1.0") (d (list (d (n "scones") (r "^0.1") (d #t) (k 0)))) (h "1j5knfb2h79bmzd3kqygjc05b52m0swqniyq4a9qlgzrz2vy8y7w")))

(define-public crate-scones_examples-0.1.1 (c (n "scones_examples") (v "0.1.1") (d (list (d (n "scones") (r "^0.1") (d #t) (k 0)))) (h "0jfbi7vcj3s8b0jsbdcpxpv5zkgr5qxn42vhgq1clh24s5kvr7yv")))

(define-public crate-scones_examples-0.1.2 (c (n "scones_examples") (v "0.1.2") (d (list (d (n "scones") (r "^0.1") (d #t) (k 0)))) (h "0kmdgn6aaxmjhpi14gvy5sxyk08cahk0ixvny85ybaphy5r8l266")))

