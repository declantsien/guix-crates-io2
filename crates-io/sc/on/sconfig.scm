(define-module (crates-io sc on sconfig) #:use-module (crates-io))

(define-public crate-sconfig-0.1.0 (c (n "sconfig") (v "0.1.0") (d (list (d (n "sconfig-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.93") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "toml") (r "^0.7.2") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "12l7qz92ls2jcb6yh30sm5ba4ww5ag64q35vyv6aycvfbas244g8")))

(define-public crate-sconfig-0.1.1 (c (n "sconfig") (v "0.1.1") (d (list (d (n "sconfig-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.93") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "toml") (r "^0.7.2") (d #t) (k 0)) (d (n "toml_edit") (r "^0.19.9") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "0d37imn5v82mcqqlwwmzhf1f36isc36k0c55q6h4dkq3p0f2hdjh") (y #t)))

(define-public crate-sconfig-0.1.2 (c (n "sconfig") (v "0.1.2") (d (list (d (n "sconfig-macros") (r "^0.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.93") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "toml") (r "^0.7.2") (d #t) (k 0)) (d (n "toml_edit") (r "^0.19.9") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "01f6g4bkcxidbcb2p5idvf98s674lj6l1lhmwi7zhs202faka98a")))

