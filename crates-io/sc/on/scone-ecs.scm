(define-module (crates-io sc on scone-ecs) #:use-module (crates-io))

(define-public crate-scone-ecs-0.0.1 (c (n "scone-ecs") (v "0.0.1") (d (list (d (n "saunter") (r "^0.1.7") (d #t) (k 0)) (d (n "winit") (r "^0.27.5") (d #t) (k 0)))) (h "0b62fscq1kl4c40bifc3ma4q62fgb4r0l2ikhqbw5mcf469vqaaq")))

(define-public crate-scone-ecs-0.0.2 (c (n "scone-ecs") (v "0.0.2") (d (list (d (n "saunter") (r "^0.1.7") (d #t) (k 0)) (d (n "winit") (r "^0.27.5") (d #t) (k 0)))) (h "1gkg85j2m73l1l0qyl1ylz7sl1459ccl2r552kl5y68j8gpfrw4r")))

