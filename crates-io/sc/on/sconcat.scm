(define-module (crates-io sc on sconcat) #:use-module (crates-io))

(define-public crate-sconcat-0.1.0 (c (n "sconcat") (v "0.1.0") (h "1rw3min1sy9zfsnsk819lbbqq4lm2ys8rmrlxsd3knfgfjzjcs1h")))

(define-public crate-sconcat-0.1.1 (c (n "sconcat") (v "0.1.1") (h "1v23drqrbhp6jwlkasdhxacn43qcc3sj8app3ka4x3vji1n8zzgf")))

(define-public crate-sconcat-0.1.2 (c (n "sconcat") (v "0.1.2") (d (list (d (n "fast_fmt") (r "^0.1.2") (o #t) (d #t) (k 0)))) (h "170bfkxshzapsv31n2kzbjjmhbg1n63hkzkkdvh7ysfqw6zigmxc")))

(define-public crate-sconcat-0.2.0 (c (n "sconcat") (v "0.2.0") (d (list (d (n "fast_fmt") (r "^0.1.2") (o #t) (d #t) (k 0)))) (h "08vxnmcpmbv83hglksfd3ydw6594im57j2llfw5108bi29pm9zj7")))

(define-public crate-sconcat-0.2.1 (c (n "sconcat") (v "0.2.1") (d (list (d (n "fast_fmt") (r "^0.1.2") (o #t) (d #t) (k 0)))) (h "05sr0vmmwd3bwvvbbri4v8mazzs49l7l7rbks9hyz6drinr8zixi")))

