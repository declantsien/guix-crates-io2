(define-module (crates-io sc on scones) #:use-module (crates-io))

(define-public crate-scones-0.1.0 (c (n "scones") (v "0.1.0") (d (list (d (n "scones_macros") (r "^0.1") (d #t) (k 0)))) (h "0k1pv0i364mvkmrqagr5c5s5l89xw1vgp2l0w98ayjv5j2z4hdlh")))

(define-public crate-scones-0.1.1 (c (n "scones") (v "0.1.1") (d (list (d (n "scones_macros") (r "^0.1") (d #t) (k 0)))) (h "0f5hv3f7var37akx6483f8xk7whaq0irypasspa0cvqdqv1qwyfv")))

(define-public crate-scones-0.1.2 (c (n "scones") (v "0.1.2") (d (list (d (n "scones_macros") (r "^0.1") (d #t) (k 0)))) (h "002s14maiqw0qbgg2pg69q2gqjjwk7ddl28lpvsxmhbrxva8cy62")))

