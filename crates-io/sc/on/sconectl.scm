(define-module (crates-io sc on sconectl) #:use-module (crates-io))

(define-public crate-sconectl-0.1.0 (c (n "sconectl") (v "0.1.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "0axc8j7nbpmjdyhrim5l3hq6pn300kx218nb6bky4j71x5ccsm0n")))

(define-public crate-sconectl-0.2.0 (c (n "sconectl") (v "0.2.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "which") (r "^4.2.5") (d #t) (k 0)))) (h "1klcd5anps6m58z652ii7nmj5q99bkv4qi2naalj3v0dd1l9iqc0")))

(define-public crate-sconectl-0.2.1 (c (n "sconectl") (v "0.2.1") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "which") (r "^4.2.5") (d #t) (k 0)))) (h "0fgbmdp9yw4sm448zclswkrhm53bjb0x9nxyi9wymabp5wdk2qab")))

(define-public crate-sconectl-0.2.2 (c (n "sconectl") (v "0.2.2") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "which") (r "^4.2.5") (d #t) (k 0)))) (h "156ja5gnxx570pb2czay87vmjkpxvlwyjw3mvjrzpjc4mzwc56ai")))

(define-public crate-sconectl-0.2.3 (c (n "sconectl") (v "0.2.3") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "which") (r "^4.2.5") (d #t) (k 0)))) (h "0lc1vf71hsf5a49zqxhj6m4grir4dvs03pznnqr2l6axd6qm3wkm")))

(define-public crate-sconectl-0.2.4 (c (n "sconectl") (v "0.2.4") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "shells") (r "^0.2.0") (d #t) (k 0)) (d (n "which") (r "^4.2.5") (d #t) (k 0)))) (h "1j1cy4p4rwv3rwyw95bag9abpbngqgbln8xv1nx29dz5prdzpwdp")))

(define-public crate-sconectl-0.2.5 (c (n "sconectl") (v "0.2.5") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "shells") (r "^0.2.0") (d #t) (k 0)) (d (n "which") (r "^4.2.5") (d #t) (k 0)))) (h "0kz2464z5mvmmx412c984yjj21qclsj2jh1wqdyya255ygfl9zjp")))

(define-public crate-sconectl-0.2.6 (c (n "sconectl") (v "0.2.6") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "shells") (r "^0.2.0") (d #t) (k 0)) (d (n "which") (r "^4.2.5") (d #t) (k 0)))) (h "0ayqdmmgyl2v0sv6n683ss1kxl281s8a2fi497dh7grkkbnp6vim")))

(define-public crate-sconectl-0.2.7 (c (n "sconectl") (v "0.2.7") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "shells") (r "^0.2.0") (d #t) (k 0)) (d (n "which") (r "^4.2.5") (d #t) (k 0)))) (h "0i6lsiq8zi346jl8qqk2awhm910a595ws3kq44qbl0gflhmly9y6")))

(define-public crate-sconectl-0.2.8 (c (n "sconectl") (v "0.2.8") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "shells") (r "^0.2.0") (d #t) (k 0)) (d (n "which") (r "^4.2.5") (d #t) (k 0)))) (h "0j6469a8m08697yqqvy6w7rkbppb9bbk13kfi6y8wgscinazg98m")))

(define-public crate-sconectl-0.2.9 (c (n "sconectl") (v "0.2.9") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "shells") (r "^0.2.0") (d #t) (k 0)) (d (n "which") (r "^4.2.5") (d #t) (k 0)))) (h "13dqypqrygl61fz10wyg33q688pijqbqjm8413lh8lrd4diqfk94")))

(define-public crate-sconectl-0.2.10 (c (n "sconectl") (v "0.2.10") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "shells") (r "^0.2.0") (d #t) (k 0)) (d (n "which") (r "^4.2.5") (d #t) (k 0)))) (h "06jvcnf78bkydn0pydssnmwwrlibghyyvj7h7sdbkl08ham4y3mf")))

(define-public crate-sconectl-0.2.11 (c (n "sconectl") (v "0.2.11") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "shells") (r "^0.2.0") (d #t) (k 0)) (d (n "spinners") (r "^4.1.0") (d #t) (k 0)) (d (n "termimad") (r "^0.14") (d #t) (k 0)) (d (n "which") (r "^4.2.5") (d #t) (k 0)))) (h "0y0x59b6dw9hgcgsarnyazbq7kfdaai2p8mjxg7laq8yi28cjbx7")))

(define-public crate-sconectl-0.2.12 (c (n "sconectl") (v "0.2.12") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "shells") (r "^0.2.0") (d #t) (k 0)) (d (n "spinners") (r "^4.1.0") (d #t) (k 0)) (d (n "termimad") (r "^0.14") (d #t) (k 0)) (d (n "which") (r "^4.2.5") (d #t) (k 0)))) (h "1xkajgj8w48hmswdsdqihmqf92imqwhgvmdgz9yyml70aijyp4rz")))

(define-public crate-sconectl-0.2.13 (c (n "sconectl") (v "0.2.13") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "shells") (r "^0.2.0") (d #t) (k 0)) (d (n "spinners") (r "^4.1.0") (d #t) (k 0)) (d (n "termimad") (r "^0.14") (d #t) (k 0)) (d (n "which") (r "^4.2.5") (d #t) (k 0)))) (h "09w82fn4fcil4qczzmkpfy426hhbiwvv95zwn9qcphwkk5drf3qg")))

(define-public crate-sconectl-0.2.14 (c (n "sconectl") (v "0.2.14") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "shells") (r "^0.2.0") (d #t) (k 0)) (d (n "spinners") (r "^4.1.0") (d #t) (k 0)) (d (n "termimad") (r "^0.14") (d #t) (k 0)) (d (n "which") (r "^4.2.5") (d #t) (k 0)))) (h "1fpkf7b985l81cb13wrk292zymh8c28lbllach736p3akl9n0hi8")))

(define-public crate-sconectl-0.2.15 (c (n "sconectl") (v "0.2.15") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "shells") (r "^0.2.0") (d #t) (k 0)) (d (n "spinners") (r "^4.1.0") (d #t) (k 0)) (d (n "termimad") (r "^0.14") (d #t) (k 0)) (d (n "which") (r "^4.2.5") (d #t) (k 0)))) (h "11j0ncnliz7ypl4b57zjh6lgzc79y0djgp7agmrhgap3p1qdlgvs")))

(define-public crate-sconectl-0.2.16 (c (n "sconectl") (v "0.2.16") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "shells") (r "^0.2.0") (d #t) (k 0)) (d (n "spinners") (r "^4.1.0") (d #t) (k 0)) (d (n "termimad") (r "^0.14") (d #t) (k 0)) (d (n "which") (r "^4.2.5") (d #t) (k 0)))) (h "0y9kphjqbp3jcikcy5vvv35fnpz43mbf51pspw33fyjg6sspr9vq")))

(define-public crate-sconectl-0.2.17 (c (n "sconectl") (v "0.2.17") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "get_if_addrs") (r "^0.5.3") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "shells") (r "^0.2.0") (d #t) (k 0)) (d (n "spinners") (r "^4.1.0") (d #t) (k 0)) (d (n "termimad") (r "^0.14") (d #t) (k 0)) (d (n "which") (r "^4.2.5") (d #t) (k 0)))) (h "0qsn1sb5ihy5l005wjnnwdzmlhnf843nncpasykiy4i6p8y3fbq3")))

(define-public crate-sconectl-0.2.18 (c (n "sconectl") (v "0.2.18") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "get_if_addrs") (r "^0.5.3") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "shells") (r "^0.2.0") (d #t) (k 0)) (d (n "spinners") (r "^4.1.0") (d #t) (k 0)) (d (n "termimad") (r "^0.14") (d #t) (k 0)) (d (n "which") (r "^4.2.5") (d #t) (k 0)))) (h "0cxfmmxwhmspj2lmd0y1ia2j6kgncw2cb2l9rzr36yryf0a2p8ci")))

(define-public crate-sconectl-0.2.19 (c (n "sconectl") (v "0.2.19") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "get_if_addrs") (r "^0.5.3") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "shells") (r "^0.2.0") (d #t) (k 0)) (d (n "spinners") (r "^4.1.0") (d #t) (k 0)) (d (n "termimad") (r "^0.14") (d #t) (k 0)) (d (n "which") (r "^4.2.5") (d #t) (k 0)))) (h "0bf2yl0aapyg01xzpgik4x3crcb7w5j4lqhas72zgmh4p5gng4xq")))

(define-public crate-sconectl-5.8.0-rc.14 (c (n "sconectl") (v "5.8.0-rc.14") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "get_if_addrs") (r "^0.5.3") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "shells") (r "^0.2.0") (d #t) (k 0)) (d (n "spinners") (r "^4.1.0") (d #t) (k 0)) (d (n "termimad") (r "^0.14") (d #t) (k 0)) (d (n "which") (r "^4.2.5") (d #t) (k 0)))) (h "04icar7j1lz8jz2676f6anz34piv901hn19c69ccjn84b3c2ri8l")))

(define-public crate-sconectl-5.8.0 (c (n "sconectl") (v "5.8.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "get_if_addrs") (r "^0.5.3") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "shells") (r "^0.2.0") (d #t) (k 0)) (d (n "spinners") (r "^4.1.0") (d #t) (k 0)) (d (n "termimad") (r "^0.14") (d #t) (k 0)) (d (n "which") (r "^4.2.5") (d #t) (k 0)))) (h "10fpp3b4j6qid0bwra5fj2m4m9r4c8bcakf9ilq0wlv6svzlqkvg")))

(define-public crate-sconectl-5.8.1 (c (n "sconectl") (v "5.8.1") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "get_if_addrs") (r "^0.5.3") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "shells") (r "^0.2.0") (d #t) (k 0)) (d (n "spinners") (r "^4.1.0") (d #t) (k 0)) (d (n "termimad") (r "^0.14") (d #t) (k 0)) (d (n "which") (r "^4.2.5") (d #t) (k 0)))) (h "08k8lzc92krbhic4b726l6p9rl1pi84ry71sr4fci7vzvnlvidym")))

(define-public crate-sconectl-5.8.2 (c (n "sconectl") (v "5.8.2") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "get_if_addrs") (r "^0.5.3") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "shells") (r "^0.2.0") (d #t) (k 0)) (d (n "spinners") (r "^4.1.0") (d #t) (k 0)) (d (n "termimad") (r "^0.14") (d #t) (k 0)) (d (n "which") (r "^4.2.5") (d #t) (k 0)))) (h "0ij9604lc8d89nyihjcs3rwb7kdl5w1kd0rcy8drksdsdd5i6dc7")))

(define-public crate-sconectl-5.8.3 (c (n "sconectl") (v "5.8.3") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "get_if_addrs") (r "^0.5.3") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "shells") (r "^0.2.0") (d #t) (k 0)) (d (n "spinners") (r "^4.1.0") (d #t) (k 0)) (d (n "termimad") (r "^0.14") (d #t) (k 0)) (d (n "which") (r "^4.2.5") (d #t) (k 0)))) (h "052n92d9hcka7sibf32r4jhwl9iykg9q3aja3gs7lz75kh21ilxx")))

(define-public crate-sconectl-5.8.4 (c (n "sconectl") (v "5.8.4") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "shells") (r "^0.2.0") (d #t) (k 0)) (d (n "spinners") (r "^4.1.0") (d #t) (k 0)) (d (n "which") (r "^4.2.5") (d #t) (k 0)))) (h "1z0j2c0k8i77vbjj4za3xyqx1nph8f07lndr9ggvslb98wbsin9p")))

(define-public crate-sconectl-5.8.5 (c (n "sconectl") (v "5.8.5") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "shells") (r "^0.2.0") (d #t) (k 0)) (d (n "spinners") (r "^4.1.0") (d #t) (k 0)) (d (n "which") (r "^4.2.5") (d #t) (k 0)))) (h "1g6a1xs2jhin4b7584hbwdqva9crh904zfipn2rchyifd8dgyx40")))

