(define-module (crates-io sc on sconfig-macros) #:use-module (crates-io))

(define-public crate-sconfig-macros-0.1.0 (c (n "sconfig-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)) (d (n "syn") (r "^1.0.108") (f (quote ("extra-traits"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.2") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "1df38m0nlg1dvmpb490pdybgx586p8viywsz8mmziqqi64fzx5xb")))

(define-public crate-sconfig-macros-0.1.1 (c (n "sconfig-macros") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)) (d (n "syn") (r "^1.0.108") (f (quote ("extra-traits"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.2") (d #t) (k 0)) (d (n "toml_edit") (r "^0.19.9") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "1g2ys4p48325a1l37755q0bsq4cg9ylh948ad0synmy265spyr5w")))

(define-public crate-sconfig-macros-0.1.2 (c (n "sconfig-macros") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)) (d (n "syn") (r "^1.0.108") (f (quote ("extra-traits"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.2") (d #t) (k 0)) (d (n "toml_edit") (r "^0.19.9") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "1jsgzq1aj65zlkgaqz05rkcy5brxshvzdhlf1as3gncyd31xg6a7")))

