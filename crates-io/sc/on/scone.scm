(define-module (crates-io sc on scone) #:use-module (crates-io))

(define-public crate-scone-0.0.1 (c (n "scone") (v "0.0.1") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "saunter") (r "^0.1.7") (d #t) (k 0)) (d (n "scone-ecs") (r "^0.0.1") (d #t) (k 0)) (d (n "simplelog") (r "^0.12.0") (d #t) (k 0)) (d (n "winit") (r "^0.27.5") (d #t) (k 0)))) (h "1s6k5bl9zz9376xjk7qy5a0lwbdryynrk8k9n3iwr2y2nrg35w9d")))

(define-public crate-scone-0.0.2 (c (n "scone") (v "0.0.2") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "saunter") (r "^0.1.7") (d #t) (k 0)) (d (n "scone-ecs") (r "^0.0.2") (d #t) (k 0)) (d (n "simplelog") (r "^0.12.0") (d #t) (k 0)) (d (n "winit") (r "^0.27.5") (d #t) (k 0)))) (h "0jnjbppdvp2fhb6a7zxgz6y6qwdgi34x20hsipd24hykmzqvs8n3")))

