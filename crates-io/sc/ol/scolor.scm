(define-module (crates-io sc ol scolor) #:use-module (crates-io))

(define-public crate-scolor-0.1.0 (c (n "scolor") (v "0.1.0") (h "0ybkchyp3vyv3zn57fq0qh3girfwbhhsb7yayrbyyx71jjn92qlh")))

(define-public crate-scolor-1.0.0 (c (n "scolor") (v "1.0.0") (h "073736yysbc2i53qffg667g6mh1vacnw8pbzpfgyss86pgxp1487")))

(define-public crate-scolor-2.0.0 (c (n "scolor") (v "2.0.0") (h "0flfby2ppkv2bq2l81lyxghjg0y8bq8swcwmxxsmmljxq5fbs4km")))

(define-public crate-scolor-4.0.0 (c (n "scolor") (v "4.0.0") (h "1ly3w4rnplasz9mlpwgcgyyzjymqfl3nwad5c9kk2yd87m2fvalw")))

(define-public crate-scolor-4.0.1 (c (n "scolor") (v "4.0.1") (h "158lslg5k0s9yi4ih62cfpvgnchkpcksyr8l25ga03xcibsdk00l")))

(define-public crate-scolor-4.0.2 (c (n "scolor") (v "4.0.2") (h "0kdap113nvwsg28akz0a3xkc33dc9fhhx2w47ikdrjf02pyl416q")))

(define-public crate-scolor-4.1.0 (c (n "scolor") (v "4.1.0") (h "041n4c82ihis0v58pxwl00n7dapqmkw8pr0lmm85jswpy0l4h46z")))

(define-public crate-scolor-4.2.0 (c (n "scolor") (v "4.2.0") (h "10vxw0qiyp9mkrg8r93xlxp0fpbxa381l6zgsvix8s5ak3yrvddj")))

(define-public crate-scolor-4.2.1 (c (n "scolor") (v "4.2.1") (h "0xmgn2ivg643nhmjg537xkjxrdh36m89fwiyhpqfig7fnw9ln19a")))

(define-public crate-scolor-5.0.0 (c (n "scolor") (v "5.0.0") (h "1bvvk8xliczpahgcx8qhplvyan70lfbz8j8kb7ikf6w59fvkq2r6")))

(define-public crate-scolor-6.0.0 (c (n "scolor") (v "6.0.0") (h "1hsaysnnaigqbyy5nq93gc83ffhh28q1m03gjqfrxfdqcmd9jz27")))

(define-public crate-scolor-7.0.0 (c (n "scolor") (v "7.0.0") (h "1xjfmzjis83var0dpgx5i0fxjqmizrpbjnhksm8piflvpz9zrh2y") (f (quote (("zero-cost") ("default"))))))

(define-public crate-scolor-7.0.1 (c (n "scolor") (v "7.0.1") (h "1wk7sq2ns58hk94xg1wgi7k427lm1rhjga7ihfwxfqvlrq6x06i6") (f (quote (("zero-cost") ("default"))))))

(define-public crate-scolor-8.0.0 (c (n "scolor") (v "8.0.0") (h "1b5v2icinn6d6rz1gabs2i3pm63cinwxzx540x5a0kac7z25m3px") (f (quote (("zero-cost") ("default"))))))

