(define-module (crates-io sc fg scfg) #:use-module (crates-io))

(define-public crate-scfg-0.1.0 (c (n "scfg") (v "0.1.0") (d (list (d (n "indexmap") (r "^1.6.0") (o #t) (d #t) (k 0)) (d (n "peg") (r "^0.6.3") (d #t) (k 0)))) (h "0v5fr3nkj97i12fzrfx9hdgil0j87g76b4k8v6di9drxwbwfd3dj") (f (quote (("preserve_order" "indexmap") ("default"))))))

(define-public crate-scfg-0.2.0 (c (n "scfg") (v "0.2.0") (d (list (d (n "indexmap") (r "^1.6.0") (o #t) (d #t) (k 0)) (d (n "peg") (r "^0.6.3") (d #t) (k 0)) (d (n "shell-words") (r "^1.0.0") (d #t) (k 0)))) (h "0chla6j8jd1qbmnrn7s72y6s1nwb674591cxi3xm6ii8xs5m4ba9") (f (quote (("preserve_order" "indexmap") ("default"))))))

(define-public crate-scfg-0.3.0 (c (n "scfg") (v "0.3.0") (d (list (d (n "indexmap") (r "^1.6.0") (o #t) (d #t) (k 0)) (d (n "shell-words") (r "^1.0.0") (d #t) (k 0)))) (h "0m7rn0l1w4gvq5d85nijbl6a5fhzbbw8w00sm8mdzfm99cm3nm6c") (f (quote (("preserve_order" "indexmap") ("default"))))))

