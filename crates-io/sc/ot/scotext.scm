(define-module (crates-io sc ot scotext) #:use-module (crates-io))

(define-public crate-scotext-0.1.0 (c (n "scotext") (v "0.1.0") (d (list (d (n "clap") (r "^2.23.3") (d #t) (k 0)))) (h "0wxvdrdbnbza9jp9qmdkwsi8dkx2g8ry0nyhh3h4rwpjnjhgzxgp")))

(define-public crate-scotext-0.1.1 (c (n "scotext") (v "0.1.1") (d (list (d (n "clap") (r "^2.23.3") (d #t) (k 0)))) (h "1za8pm23sdic11x9cgcrrpa0ym56r587rmywpw6x20a9cdb5rcaz")))

(define-public crate-scotext-0.3.0 (c (n "scotext") (v "0.3.0") (d (list (d (n "clap") (r "^2.23.3") (d #t) (k 0)) (d (n "env_logger") (r "^0.4.2") (d #t) (k 0)) (d (n "log") (r "^0.3.7") (d #t) (k 0)))) (h "1162lyqdlm49xgnb7rmnpm2svsb7n6fzf3j5ms5zq2w4lf32pl3f")))

(define-public crate-scotext-0.4.0 (c (n "scotext") (v "0.4.0") (d (list (d (n "clap") (r "^2.23.3") (d #t) (k 0)) (d (n "env_logger") (r "^0.4.2") (d #t) (k 0)) (d (n "log") (r "^0.3.7") (d #t) (k 0)) (d (n "xor-utils") (r "^0.4.0") (d #t) (k 0)))) (h "0mv7cr0yhknfcfc7hi29178x2avzj2zjnkflkdq4mjcxyf9dizs0")))

(define-public crate-scotext-0.5.0 (c (n "scotext") (v "0.5.0") (d (list (d (n "clap") (r "^2.23.3") (d #t) (k 0)) (d (n "env_logger") (r "^0.4.2") (d #t) (k 0)) (d (n "log") (r "^0.3.7") (d #t) (k 0)) (d (n "xor-utils") (r "^0.5.0") (d #t) (k 0)))) (h "05jml9qkn7vyg1j4kldhzszf8wsaq1yv4xhzyj25vrsg2b4j8vdz")))

