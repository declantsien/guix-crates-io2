(define-module (crates-io sc ot scotch-sys) #:use-module (crates-io))

(define-public crate-scotch-sys-0.1.0 (c (n "scotch-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.58") (k 1)))) (h "1hqfghby2fqg2bfij5ycfsh791gvq1ih6wb9ir10i11pba1wycnc")))

(define-public crate-scotch-sys-0.1.1 (c (n "scotch-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.58") (k 1)))) (h "05qsx94mzxryhzy5rn7mrcvkmsg72801ycrgkvjvzmxclq4lxdbq")))

(define-public crate-scotch-sys-0.2.0 (c (n "scotch-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.65") (k 1)))) (h "04mlmpsfbqkrx3ip8fw4ffh9mmhagjl7j5s9hsgn1zy9h1zjxld9") (y #t)))

(define-public crate-scotch-sys-0.2.1 (c (n "scotch-sys") (v "0.2.1") (d (list (d (n "bindgen") (r "^0.65") (k 1)))) (h "10gm973f54k0xn46fr5yxspf3hm406ll770b222k88d10y6zvhgc")))

