(define-module (crates-io sc ot scottish_names) #:use-module (crates-io))

(define-public crate-scottish_names-0.1.0 (c (n "scottish_names") (v "0.1.0") (d (list (d (n "csv") (r "^1.0.6") (d #t) (k 1)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)))) (h "139mbj6vv2x5rf3ar1hqsdphlxk2di047va5362imbrmx9hv0mrl")))

(define-public crate-scottish_names-0.1.1 (c (n "scottish_names") (v "0.1.1") (d (list (d (n "criterion") (r "^0.2.11") (d #t) (k 2)) (d (n "csv") (r "^1.0.6") (d #t) (k 1)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)))) (h "0hk5pj7y84vm3xs756r7i7xwkh4i9663glvs50zvnvz0niv2yvc5")))

(define-public crate-scottish_names-0.2.0 (c (n "scottish_names") (v "0.2.0") (d (list (d (n "criterion") (r "^0.2.11") (d #t) (k 2)) (d (n "csv") (r "^1.0.6") (d #t) (k 1)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)))) (h "1r8sk0w45ac72wdjn4mvzni7k3x0mvxxwcxamkh13m7zfgjaazb6")))

(define-public crate-scottish_names-0.2.1 (c (n "scottish_names") (v "0.2.1") (d (list (d (n "criterion") (r "^0.2.11") (d #t) (k 2)) (d (n "csv") (r "^1.0.6") (d #t) (k 1)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)))) (h "052jm5mnhdph8d9jwhxy08bvbk3s3lixbz9ps4fmyx51iqhwhqk2")))

(define-public crate-scottish_names-0.2.2 (c (n "scottish_names") (v "0.2.2") (d (list (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "csv") (r "^1.0.6") (d #t) (k 1)) (d (n "rand") (r "^0.7.2") (d #t) (k 0)))) (h "0agv4mx01vfmxxpipvw5ji8w7xwdlrl70pdj25p8xfijkajfk17p")))

