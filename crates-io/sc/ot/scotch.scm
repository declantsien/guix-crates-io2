(define-module (crates-io sc ot scotch) #:use-module (crates-io))

(define-public crate-scotch-0.1.0 (c (n "scotch") (v "0.1.0") (d (list (d (n "scotch-sys") (r "^0.1") (d #t) (k 0)))) (h "0hv156ckpwklrb4idpipw8kgrdi3xldib2k90p7lqh8hhd7vlpgl")))

(define-public crate-scotch-0.2.0 (c (n "scotch") (v "0.2.0") (d (list (d (n "scotch-sys") (r "^0.2") (d #t) (k 0)))) (h "1zbhz3s73r41smfycj2jz8nqkwxp7xwajw9c8wvqhw8r4scd5s3w")))

(define-public crate-scotch-0.2.1 (c (n "scotch") (v "0.2.1") (d (list (d (n "scotch-sys") (r "^0.2") (d #t) (k 0)))) (h "1pp55i4gbih990lx640mnq7v0gdrpsajhc362nkd4264d3d0s5rj")))

