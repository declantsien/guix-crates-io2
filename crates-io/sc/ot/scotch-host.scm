(define-module (crates-io sc ot scotch-host) #:use-module (crates-io))

(define-public crate-scotch-host-0.1.0 (c (n "scotch-host") (v "0.1.0") (d (list (d (n "bincode") (r "^2.0.0-rc.2") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "scotch-host-macros") (r "^0.1") (d #t) (k 0)) (d (n "wasmer") (r "^3") (f (quote ("sys" "compiler"))) (k 0)) (d (n "criterion") (r "^0.4") (d #t) (k 2)))) (h "0py7vmq9mcsrkzvlzxky5s8wglj8d4srzw8fy8cv5ch7qllnxazl") (f (quote (("unstable-doc-cfg") ("singlepass" "wasmer/singlepass") ("llvm" "wasmer/llvm") ("default" "compiler" "cranelift") ("cranelift" "wasmer/cranelift") ("compiler"))))))

