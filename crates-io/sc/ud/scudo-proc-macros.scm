(define-module (crates-io sc ud scudo-proc-macros) #:use-module (crates-io))

(define-public crate-scudo-proc-macros-0.1.0 (c (n "scudo-proc-macros") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.104") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.46") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.102") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.71") (d #t) (k 2)))) (h "0qblivnxzmg5bcrbrshhj6qnpsfswb2p0nr3a62br3zf1a87q9mk")))

