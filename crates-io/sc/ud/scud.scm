(define-module (crates-io sc ud scud) #:use-module (crates-io))

(define-public crate-scud-0.1.3 (c (n "scud") (v "0.1.3") (d (list (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "dialoguer") (r "^0.10.1") (f (quote ("fuzzy-select"))) (d #t) (k 0)))) (h "12a4laqc458k7bsskxx0ymz8038dz7c1dfa7r84z9430k8vdg8b9")))

(define-public crate-scud-0.2.0 (c (n "scud") (v "0.2.0") (d (list (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "dialoguer") (r "^0.10.2") (f (quote ("fuzzy-select"))) (d #t) (k 0)))) (h "05zwwvg8b1q6j9kzmvv2imaphz1naf8738g1jjs69gcjx6ca5c6s")))

(define-public crate-scud-0.3.0 (c (n "scud") (v "0.3.0") (d (list (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "dialoguer") (r "^0.10.2") (f (quote ("fuzzy-select"))) (d #t) (k 0)))) (h "0z816d5cmny7jds5c5fzkg6h063xw26fi8ddjss736w9za0daakm")))

(define-public crate-scud-0.4.0 (c (n "scud") (v "0.4.0") (d (list (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "dialoguer") (r "^0.10.2") (f (quote ("fuzzy-select"))) (d #t) (k 0)))) (h "1q6smai8lz6xsvnhcgbl278xa30q0ycy4ag52wbnrpw0y89n1y7g")))

(define-public crate-scud-0.5.0 (c (n "scud") (v "0.5.0") (d (list (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "dialoguer") (r "^0.10.2") (f (quote ("fuzzy-select"))) (d #t) (k 0)))) (h "0xpsvh5gd681hhawm95wm6rb8q7w08dws7qnfam00hg8bf4wsxy0")))

(define-public crate-scud-0.6.0 (c (n "scud") (v "0.6.0") (d (list (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "dialoguer") (r "^0.10.2") (f (quote ("fuzzy-select"))) (d #t) (k 0)) (d (n "update-informer") (r "^0.5.0") (d #t) (k 0)))) (h "0rg0nvc2zkn0nlhr9ww4ln520sc3bqfvq4hnjcl1rqsakcc6bmiy")))

(define-public crate-scud-0.7.0 (c (n "scud") (v "0.7.0") (d (list (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "scud_core") (r "^0.7.0") (d #t) (k 0)))) (h "1zpkhakdpdac3rhx91in8ibbzwh7984gwlr8b75qmnfklzlx8h9n")))

(define-public crate-scud-0.8.0 (c (n "scud") (v "0.8.0") (d (list (d (n "clap") (r "^3.2.16") (f (quote ("derive"))) (d #t) (k 0)) (d (n "scud_core") (r "^0.8.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.32") (d #t) (k 0)))) (h "1gy9m62m473v8zj603ymsqbn7961147ssfqnzfdnzrvb8a32xcf0")))

(define-public crate-scud-0.9.0 (c (n "scud") (v "0.9.0") (d (list (d (n "clap") (r "^3.2.16") (f (quote ("derive"))) (d #t) (k 0)) (d (n "scud_core") (r "^0.9.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.32") (d #t) (k 0)))) (h "0hraibc412gd6rpv5rakilv6bnp617k36ylq9pvcibakx6i2aspi")))

(define-public crate-scud-0.10.0 (c (n "scud") (v "0.10.0") (d (list (d (n "clap") (r "^3.2.16") (f (quote ("derive"))) (d #t) (k 0)) (d (n "scud_core") (r "^0.10.0") (d #t) (k 0)))) (h "042h5a5kj7dg0lf4adfkssf318zwqd02y9lrlh1i4bb3p8071vha")))

(define-public crate-scud-0.10.1 (c (n "scud") (v "0.10.1") (d (list (d (n "clap") (r "^3.2.16") (f (quote ("derive"))) (d #t) (k 0)) (d (n "scud_core") (r "^0.10.1") (d #t) (k 0)))) (h "03zbsa617xp8njlcixqhi13nz01zymqkn8zqc933qlnmpmfj3yyv")))

(define-public crate-scud-0.11.0 (c (n "scud") (v "0.11.0") (d (list (d (n "clap") (r "^3.2.16") (f (quote ("derive"))) (d #t) (k 0)) (d (n "scud_core") (r "^0.11.0") (d #t) (k 0)))) (h "1z7avmi70qs9m0z2rfr2fndpz4vlfp87pidcq8nzk8b7xhs42zym")))

(define-public crate-scud-0.11.1 (c (n "scud") (v "0.11.1") (d (list (d (n "clap") (r "^3.2.16") (f (quote ("derive"))) (d #t) (k 0)) (d (n "scud_core") (r "^0.11.1") (d #t) (k 0)))) (h "0mybcrkpm5iksyxrnkysmz3vn554w9blq98fhcphqwp4ifyhwnzk")))

(define-public crate-scud-0.12.0 (c (n "scud") (v "0.12.0") (d (list (d (n "clap") (r "^3.2.16") (f (quote ("derive"))) (d #t) (k 0)) (d (n "scud_core") (r "^0.12.0") (d #t) (k 0)))) (h "0kbxrzvwcbaq42sywbykbcxifzkfahqky7s9qlx29bdsrd9ka0qy")))

(define-public crate-scud-0.13.0 (c (n "scud") (v "0.13.0") (d (list (d (n "clap") (r "^3.2.16") (f (quote ("derive"))) (d #t) (k 0)) (d (n "scud_core") (r "^0.13.0") (d #t) (k 0)))) (h "1s2srrk5lmsfrmhl2snxr6j64ak19cmwks11lkn2d6jq5zrf39jk")))

