(define-module (crates-io sc ud scudo) #:use-module (crates-io))

(define-public crate-scudo-0.1.0 (c (n "scudo") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.104") (d #t) (k 0)) (d (n "scudo-sys") (r "^0.1.0") (d #t) (k 0)))) (h "1wxycigrilbgh4794yv16sz6ax0rp009i71qnhasabwpldphc9jg")))

(define-public crate-scudo-0.1.1 (c (n "scudo") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.104") (d #t) (k 0)) (d (n "scudo-sys") (r "^0.2.0") (d #t) (k 0)))) (h "0czrajsadkdd27kbl51f2wbvvh8ly2nkd0sfkyn3r1hjdf90q4qz")))

(define-public crate-scudo-0.1.2 (c (n "scudo") (v "0.1.2") (d (list (d (n "libc") (r "^0.2.104") (d #t) (k 0)) (d (n "scudo-sys") (r "^0.2.0") (d #t) (k 0)))) (h "1qyqhrqd9lp7d5rpkn7gdpy0627lni379491f7jc0djnawp14hx0") (f (quote (("allocator_api"))))))

(define-public crate-scudo-0.1.3 (c (n "scudo") (v "0.1.3") (d (list (d (n "libc") (r "^0.2.104") (d #t) (k 0)) (d (n "scudo-proc-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "scudo-sys") (r "^0.2.0") (d #t) (k 0)))) (h "1266nl4ik071ckyhvhirggk8ikzklavvs4zamw378j3sl0fcpgqj") (f (quote (("allocator_api"))))))

