(define-module (crates-io sc ud scudo-sys) #:use-module (crates-io))

(define-public crate-scudo-sys-0.1.0 (c (n "scudo-sys") (v "0.1.0") (d (list (d (n "cc") (r "^1.0.71") (d #t) (k 1)) (d (n "libc") (r "^0.2.104") (d #t) (k 0)))) (h "0v9x08v95zrf42k3qxcb3rq2468ccypbnlnfslq5gnrhmzlcmpzl")))

(define-public crate-scudo-sys-0.2.0 (c (n "scudo-sys") (v "0.2.0") (d (list (d (n "cc") (r "^1.0.71") (d #t) (k 1)) (d (n "libc") (r "^0.2.104") (d #t) (k 0)))) (h "0nd94wknfrsa1hrq1f5npa0bnbns8vflargpcp1s5lvwikafnlck")))

(define-public crate-scudo-sys-0.2.1 (c (n "scudo-sys") (v "0.2.1") (d (list (d (n "cc") (r "^1.0.71") (d #t) (k 1)) (d (n "libc") (r "^0.2.104") (d #t) (k 0)))) (h "01pi9khwf8c1mawp9k75y083wjhjkdmb5xmb4cyygx9dl9rarvbs")))

(define-public crate-scudo-sys-0.2.2 (c (n "scudo-sys") (v "0.2.2") (d (list (d (n "cc") (r "^1.0.71") (d #t) (k 1)) (d (n "libc") (r "^0.2.104") (d #t) (k 0)))) (h "1nqdfqkyv91y08mhf4ahv0z01fw6wh3vpfnh8wxhigrnhardznxw")))

