(define-module (crates-io sc re screenruster-saver-laughing_man) #:use-module (crates-io))

(define-public crate-screenruster-saver-laughing_man-0.1.0 (c (n "screenruster-saver-laughing_man") (v "0.1.0") (d (list (d (n "nalgebra") (r "^0.8") (d #t) (k 0)) (d (n "screenruster-saver") (r "^0.1") (d #t) (k 0)))) (h "18qdj41z6qrblhrxc9z0l1l33pkyj1d5x33hn9qpdnxga77ii0a4")))

(define-public crate-screenruster-saver-laughing_man-0.1.1 (c (n "screenruster-saver-laughing_man") (v "0.1.1") (d (list (d (n "nalgebra") (r "^0.8") (d #t) (k 0)) (d (n "screenruster-saver") (r "^0.1") (d #t) (k 0)))) (h "19z1nlqnrqvjn7bn6qdadz8ama6y30sda1haz6m7gy2fn3742rzk")))

(define-public crate-screenruster-saver-laughing_man-0.1.2 (c (n "screenruster-saver-laughing_man") (v "0.1.2") (d (list (d (n "nalgebra") (r "^0.8") (d #t) (k 0)) (d (n "screenruster-saver") (r "^0.1") (d #t) (k 0)))) (h "11gnhg2q0mlkr3pqinjm0rjs17mdph1z9c54n71bbpjba5jqa33r")))

(define-public crate-screenruster-saver-laughing_man-0.1.3 (c (n "screenruster-saver-laughing_man") (v "0.1.3") (d (list (d (n "nalgebra") (r "^0.11") (d #t) (k 0)) (d (n "screenruster-saver") (r "^0.1") (d #t) (k 0)))) (h "0jk2vbdljph6sdmlr3dkgqnz3qkyc1m0naaz7nlcb77qxhrscb8l")))

(define-public crate-screenruster-saver-laughing_man-0.2.0 (c (n "screenruster-saver-laughing_man") (v "0.2.0") (d (list (d (n "nalgebra") (r "^0.18") (d #t) (k 0)) (d (n "screen") (r "^0.2") (d #t) (k 0) (p "screenruster-saver")))) (h "1kqzacpkfmd5p6pdixaz617nmmyp3r25jmhix11z8547iyqw7hzh")))

(define-public crate-screenruster-saver-laughing_man-0.2.1 (c (n "screenruster-saver-laughing_man") (v "0.2.1") (d (list (d (n "nalgebra") (r "^0.19") (d #t) (k 0)) (d (n "screen") (r "^0.2") (d #t) (k 0) (p "screenruster-saver")))) (h "07acbrmswkxrrcx5s9xw9gmyzgcb49sagw73yyp35cvynb019nlq")))

