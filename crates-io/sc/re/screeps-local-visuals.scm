(define-module (crates-io sc re screeps-local-visuals) #:use-module (crates-io))

(define-public crate-screeps-local-visuals-0.1.0 (c (n "screeps-local-visuals") (v "0.1.0") (d (list (d (n "image") (r "^0.24.9") (d #t) (k 0)) (d (n "imageproc") (r "^0.23.0") (d #t) (k 0)) (d (n "num-complex") (r "^0.4.5") (d #t) (k 0)) (d (n "rusttype") (r "^0.9.3") (d #t) (k 0)))) (h "1iaqiii3d5xv967c3hqvqk3mb2v0r2r64q4n3pd6lzvam7azzjf7")))

