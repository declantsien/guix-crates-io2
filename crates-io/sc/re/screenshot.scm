(define-module (crates-io sc re screenshot) #:use-module (crates-io))

(define-public crate-screenshot-0.0.1 (c (n "screenshot") (v "0.0.1") (h "10ids3jb2h397640rk2pg6h02n54h0xrzjh9i1447rx6qgdm3pbv")))

(define-public crate-screenshot-0.0.2 (c (n "screenshot") (v "0.0.2") (d (list (d (n "bmp") (r "*") (d #t) (k 2)))) (h "0ghgmq7z07c21cqsxw693pif8aiivmg0k6d6fba693dq4xi4zi7q")))

(define-public crate-screenshot-0.0.3 (c (n "screenshot") (v "0.0.3") (d (list (d (n "bmp") (r "*") (d #t) (k 2)))) (h "0kanr089xfvz109jgz7flirfm6dmxh3hv8awzsjn7r6prh5z6iqa")))

(define-public crate-screenshot-0.0.4 (c (n "screenshot") (v "0.0.4") (d (list (d (n "bmp") (r "*") (d #t) (k 2)) (d (n "image") (r "*") (d #t) (k 2)))) (h "1xbc3lghwhaqch3pd3klszr69afvvrcmbd499m29551qcljbqxww")))

(define-public crate-screenshot-0.0.5 (c (n "screenshot") (v "0.0.5") (d (list (d (n "bmp") (r "*") (d #t) (k 2)) (d (n "image") (r "*") (d #t) (k 2)))) (h "1d9rryqkb8bqigrw1syh05d68sdmmccdl2d7gkj3704c1m6lzqlg")))

(define-public crate-screenshot-0.0.6 (c (n "screenshot") (v "0.0.6") (d (list (d (n "bmp") (r "*") (d #t) (k 2)) (d (n "image") (r "*") (d #t) (k 2)))) (h "0haxd7y6j9avkprgx1ihcky6a4715g28mi771zlb6wyz962v2sj8")))

(define-public crate-screenshot-0.0.7 (c (n "screenshot") (v "0.0.7") (d (list (d (n "bmp") (r "*") (d #t) (k 2)) (d (n "image") (r "*") (d #t) (k 2)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "1xjj4mf2sayxp9pgq7v61mm6034fpdhjnnni056wp8f1aqkvkc14")))

