(define-module (crates-io sc re screeps-async-macros) #:use-module (crates-io))

(define-public crate-screeps-async-macros-0.1.0 (c (n "screeps-async-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0z2cb2c21v51nsdlff5jpzdh8fif6rl7jvchqd8sbjvcp2lzgnb9")))

(define-public crate-screeps-async-macros-0.1.1 (c (n "screeps-async-macros") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "098qwpzfhnvhla8ybk696w46b91y48sfi1n2z8qjqgsb6nhwzdxs")))

(define-public crate-screeps-async-macros-0.1.2 (c (n "screeps-async-macros") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1n692vij4xmjds2lsby6q25q5ii04k54cdsjwih46a5l5nii6sc4")))

(define-public crate-screeps-async-macros-0.1.3 (c (n "screeps-async-macros") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1p5l59539immp4axcllz069gp1h3rdwppwgz4q9sx89wh79gc4fj")))

