(define-module (crates-io sc re screen-info) #:use-module (crates-io))

(define-public crate-screen-info-0.0.1 (c (n "screen-info") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "core-graphics") (r "^0.23") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "fxhash") (r "^0.2") (d #t) (t "cfg(target_os = \"windows\")") (k 0)) (d (n "widestring") (r "^1.0") (d #t) (t "cfg(target_os = \"windows\")") (k 0)) (d (n "windows") (r "^0.52") (f (quote ("Win32_Foundation" "Win32_Graphics_Gdi"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)) (d (n "xcb") (r "^1.3") (f (quote ("randr"))) (d #t) (t "cfg(target_os = \"linux\")") (k 0)))) (h "0rbbvyxlqa9669idzvy9dy89447i4ipb1w1g2c6wd4q9jwp04di8")))

