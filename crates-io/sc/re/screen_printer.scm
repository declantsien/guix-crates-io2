(define-module (crates-io sc re screen_printer) #:use-module (crates-io))

(define-public crate-screen_printer-0.1.0 (c (n "screen_printer") (v "0.1.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "guard") (r "^0.5.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "termion") (r "^2.0.1") (d #t) (k 0)))) (h "1c07fdadszadfnbav33i7lj1fg45q38gl9ch6j0gnmkabwxk050b")))

(define-public crate-screen_printer-0.1.1 (c (n "screen_printer") (v "0.1.1") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "guard") (r "^0.5.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "termion") (r "^2.0.1") (d #t) (k 0)))) (h "1vykxhknx1j0gpvwkydd4n0i3q69jd493dzs5xvmdniby8vvml6k")))

(define-public crate-screen_printer-0.1.2 (c (n "screen_printer") (v "0.1.2") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "guard") (r "^0.5.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "termion") (r "^2.0.1") (d #t) (k 0)))) (h "19d1s3wnmypbb1mx5mpiggphl0rs0hpx6qnam7fbmlqrb0vhpwav")))

(define-public crate-screen_printer-0.1.3 (c (n "screen_printer") (v "0.1.3") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "guard") (r "^0.5.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "termion") (r "^2.0.1") (d #t) (k 0)))) (h "0ys3198xm9jf3w6gy5mmhs971vih10cq4hygnqm2f2d2dyqy2bja")))

(define-public crate-screen_printer-0.1.4 (c (n "screen_printer") (v "0.1.4") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "guard") (r "^0.5.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "termion") (r "^2.0.1") (d #t) (k 0)))) (h "0sfkfbw66b7k2saqdh3adp8012rzipln3bps9x9zbkmawqhqj66m")))

(define-public crate-screen_printer-0.1.5 (c (n "screen_printer") (v "0.1.5") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "guard") (r "^0.5.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "termion") (r "^2.0.1") (d #t) (k 0)))) (h "1dfjg5s68rl7rg5qikqfrw37ya2w9k8miz8fznzgng59xk2zv2vh")))

(define-public crate-screen_printer-0.1.6 (c (n "screen_printer") (v "0.1.6") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "termion") (r "^2.0.1") (d #t) (k 0)))) (h "0g000a3ak3170i7kfsvbmnijdq0srp5jnajjfhyi0nyzh6yy0ffh")))

(define-public crate-screen_printer-0.2.0 (c (n "screen_printer") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "log4rs") (r "^1.2.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "termion") (r "^2.0.1") (d #t) (k 0)))) (h "0dwlp24glhszs2k7dfwibkh2pjx2my66cb7v8l3c7dlpy5ks9hvy")))

(define-public crate-screen_printer-0.2.1 (c (n "screen_printer") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "log4rs") (r "^1.2.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "termion") (r "^2.0.1") (d #t) (k 0)))) (h "11kg8kf81rk7cslf93bqpbm9al3yvr647rf9y7sdkn4sr8abg1pw")))

(define-public crate-screen_printer-0.2.2 (c (n "screen_printer") (v "0.2.2") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "log4rs") (r "^1.2.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "termion") (r "^2.0.1") (d #t) (k 0)))) (h "1zsg0ksbc7pkq7z4zl7gwqf83h9251bj7ccx8czdawdxiqvcxd9d")))

(define-public crate-screen_printer-0.2.3 (c (n "screen_printer") (v "0.2.3") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "log4rs") (r "^1.2.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "termion") (r "^2.0.1") (d #t) (k 0)))) (h "0lpq7bn2qhqrccj0cs1jv1plvw7vjw4dqi1d2yhh5ryxji9bmv3z")))

(define-public crate-screen_printer-0.2.4 (c (n "screen_printer") (v "0.2.4") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "log4rs") (r "^1.2.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "termion") (r "^2.0.1") (d #t) (k 0)))) (h "0x437wwaciznf3r0zf5m9cfg2rr1p4jx570ks910rkxh77rpxrp0")))

(define-public crate-screen_printer-0.2.5 (c (n "screen_printer") (v "0.2.5") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "log4rs") (r "^1.2.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "termion") (r "^2.0.1") (d #t) (k 0)))) (h "01pw4xzwhdqs5ya3bdabz17ac9lyk7phgk1bjvv887vk48yh4r7a")))

(define-public crate-screen_printer-0.2.6 (c (n "screen_printer") (v "0.2.6") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "log4rs") (r "^1.2.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "termion") (r "^2.0.1") (d #t) (k 0)))) (h "0vwwgsw71f6d3y29f823bwgbpzkl36j45s63cp3jc88awjg32jbi")))

(define-public crate-screen_printer-0.2.7 (c (n "screen_printer") (v "0.2.7") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "log4rs") (r "^1.2.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "termion") (r "^2.0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "15335x8fh64nvgdx8pck1bi5g1i3h3hfrr48pph9l44vhv2z8sql")))

