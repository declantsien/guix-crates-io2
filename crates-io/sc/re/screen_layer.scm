(define-module (crates-io sc re screen_layer) #:use-module (crates-io))

(define-public crate-screen_layer-0.1.0 (c (n "screen_layer") (v "0.1.0") (d (list (d (n "rgb") (r "^0.8.25") (d #t) (k 0)) (d (n "vek") (r "^0.12.0") (f (quote ("libm"))) (k 0)))) (h "07m4z2vf9lhivw8kxlqa13nqi5magb9lz19z4faw87idgkqx1gph")))

(define-public crate-screen_layer-0.1.1 (c (n "screen_layer") (v "0.1.1") (d (list (d (n "rgb") (r "^0.8.25") (d #t) (k 0)) (d (n "vek") (r "^0.12.0") (f (quote ("libm"))) (k 0)))) (h "17386c4vqajq9dvanlz580x9bq5mw9h342bf9d2ivy249ghdn2ln")))

(define-public crate-screen_layer-0.1.2 (c (n "screen_layer") (v "0.1.2") (d (list (d (n "rgb") (r "^0.8.25") (d #t) (k 0)) (d (n "vek") (r "^0.12.0") (f (quote ("libm"))) (k 0)))) (h "1yl67n979b5kbclw2j72xzba57yil59lbim02132hvhypphdfs1p")))

(define-public crate-screen_layer-0.2.0 (c (n "screen_layer") (v "0.2.0") (d (list (d (n "rgb") (r "^0.8.25") (d #t) (k 0)) (d (n "vek") (r "^0.12.0") (f (quote ("libm"))) (k 0)))) (h "1kjvv2151qbvdxz7ksb0c3a4v4rjmfdd09zrl5w95rp4vn8pnwxq")))

(define-public crate-screen_layer-0.3.0 (c (n "screen_layer") (v "0.3.0") (d (list (d (n "rgb") (r "^0.8.25") (d #t) (k 0)) (d (n "vek") (r "^0.12.0") (f (quote ("libm"))) (k 0)))) (h "004ghx05mky5h6bry0w29idqxrrbmylgdjhxvsr6mkprq6i1injm")))

(define-public crate-screen_layer-0.3.1 (c (n "screen_layer") (v "0.3.1") (d (list (d (n "rgb") (r "^0.8.25") (d #t) (k 0)) (d (n "vek") (r "^0.13.0") (f (quote ("libm"))) (k 0)))) (h "1826vqss1xpwrhw8qc9fcffvk4navxi2gkkr7a9dnln01ry1pn6r")))

