(define-module (crates-io sc re scream-id) #:use-module (crates-io))

(define-public crate-scream-id-0.1.0 (c (n "scream-id") (v "0.1.0") (h "0zfn4za3w4yap6lgal0gpid7bs0dv5yd29sq51jnyq6ib013z3g7")))

(define-public crate-scream-id-0.1.1 (c (n "scream-id") (v "0.1.1") (h "03xf0j7bi4n0093gzdb6y1i7ymaf25q77cwcgnw04bs1qbpn3xpw")))

