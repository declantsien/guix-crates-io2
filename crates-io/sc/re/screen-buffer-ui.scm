(define-module (crates-io sc re screen-buffer-ui) #:use-module (crates-io))

(define-public crate-screen-buffer-ui-0.1.1 (c (n "screen-buffer-ui") (v "0.1.1") (d (list (d (n "crossterm") (r "^0.19.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.0") (d #t) (k 0)))) (h "1wbv892008qdgbh1py4gpyk269b09agqmq5zpqbdkbbh4lbqpsk2")))

(define-public crate-screen-buffer-ui-0.1.2 (c (n "screen-buffer-ui") (v "0.1.2") (d (list (d (n "crossterm") (r "^0.19.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.0") (d #t) (k 0)))) (h "09b86divg1vg4yli58mnvm5y9g73sdh4l18waqbvmr9766fp7w0h")))

(define-public crate-screen-buffer-ui-0.1.3 (c (n "screen-buffer-ui") (v "0.1.3") (d (list (d (n "crossterm") (r "^0.19.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.0") (d #t) (k 0)))) (h "0ha72pgj1wbq0400hzx2hcr9g5xhaw22r14rv834zjw1vdjm4szq")))

