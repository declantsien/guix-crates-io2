(define-module (crates-io sc re screenprints) #:use-module (crates-io))

(define-public crate-screenprints-0.1.0 (c (n "screenprints") (v "0.1.0") (h "1lsxg98xlpz5cmwggs2ihcw68ap1vdbhk9dv2mmd6f6hrr6w0fvx")))

(define-public crate-screenprints-0.1.1 (c (n "screenprints") (v "0.1.1") (d (list (d (n "kernel32-sys") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.2") (d #t) (k 0)))) (h "0fyjcz0wi6l0s0d338ajkjfy4n3n5dmxq78rmihw933nh5f60lgj")))

