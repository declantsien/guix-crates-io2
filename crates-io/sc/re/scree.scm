(define-module (crates-io sc re scree) #:use-module (crates-io))

(define-public crate-scree-0.0.1 (c (n "scree") (v "0.0.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.5") (d #t) (k 0)) (d (n "dhall") (r "^0.10") (d #t) (k 0)) (d (n "directories") (r "^4") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_dhall") (r "^0.10") (d #t) (k 0)))) (h "1x8adspsf8r058r55ij3d5pvfwfmc990082nn13dhk8y9x1jaij8")))

