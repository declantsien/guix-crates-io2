(define-module (crates-io sc re screenruster-saver-fractal) #:use-module (crates-io))

(define-public crate-screenruster-saver-fractal-0.1.1 (c (n "screenruster-saver-fractal") (v "0.1.1") (d (list (d (n "meval") (r "^0.0") (d #t) (k 0)) (d (n "palette") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "screenruster-saver") (r "^0.1") (d #t) (k 0)))) (h "0lca4hxl4pw5dysi4q7my5imlj3c1pnavlbv1w1nsjvmpqbjirvn")))

