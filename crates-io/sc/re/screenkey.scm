(define-module (crates-io sc re screenkey) #:use-module (crates-io))

(define-public crate-screenkey-0.1.0 (c (n "screenkey") (v "0.1.0") (d (list (d (n "iced") (r "^0.4.2") (d #t) (k 0)) (d (n "iced_native") (r "^0.5.1") (d #t) (k 0)) (d (n "image") (r "^0.24.3") (d #t) (k 0)) (d (n "rdev") (r "^0.5.1") (d #t) (k 0)))) (h "1rf3hq76m26gn4m7iw6iyg2hzan481v8km8crbs80aa7si04fdpv") (y #t)))

