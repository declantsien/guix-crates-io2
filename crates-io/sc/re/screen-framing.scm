(define-module (crates-io sc re screen-framing) #:use-module (crates-io))

(define-public crate-screen-framing-0.1.0 (c (n "screen-framing") (v "0.1.0") (d (list (d (n "framing") (r "^0.2") (d #t) (k 0)) (d (n "png-framing") (r "^0.1") (d #t) (k 2)) (d (n "scrap") (r "^0.3") (d #t) (k 0)))) (h "0lqpyq27byk9p5jj2jh1d2zxlc2r06lk6ij8bpblyz2ydpxgw5m0")))

(define-public crate-screen-framing-0.1.1 (c (n "screen-framing") (v "0.1.1") (d (list (d (n "framing") (r "^0.2") (d #t) (k 0)) (d (n "png-framing") (r "^0.2") (d #t) (k 2)) (d (n "scrap") (r "^0.3") (d #t) (k 0)))) (h "007cf8q5f46gcw8mlda6jwav1vr4bbmy1r7si63kai43cfdbfklz")))

(define-public crate-screen-framing-0.2.0 (c (n "screen-framing") (v "0.2.0") (d (list (d (n "framing") (r "^0.3") (d #t) (k 0)) (d (n "png-framing") (r "^0.3") (d #t) (k 2)) (d (n "scrap") (r "^0.3") (d #t) (k 0)))) (h "103cgqbw3zdvc8ka3d3xb61czcqhyz355rb8xd37insx3aj8qzbr")))

(define-public crate-screen-framing-0.2.1 (c (n "screen-framing") (v "0.2.1") (d (list (d (n "framing") (r "^0.3") (d #t) (k 0)) (d (n "png-framing") (r "^0.3") (d #t) (k 2)) (d (n "scrap") (r "^0.3") (d #t) (k 0)))) (h "0mamsys68dm89b2qx8s3khvhsis61w0wida4cj66l29dl78f2xj4")))

(define-public crate-screen-framing-0.3.0 (c (n "screen-framing") (v "0.3.0") (d (list (d (n "framing") (r "^0.6") (d #t) (k 0)) (d (n "png-framing") (r "^0.5") (d #t) (k 2)) (d (n "scrap") (r "^0.3") (d #t) (k 0)))) (h "1l45dac0jhaw8lrfz2c8ngs90kmbg3c5j3zz0mp729f0fhydf19k")))

