(define-module (crates-io sc re screwsat) #:use-module (crates-io))

(define-public crate-screwsat-0.1.0 (c (n "screwsat") (v "0.1.0") (h "17bx73v7c6p6xg5zdw3l16dglg15yyxi0x06pzvgaywiqqzgv0yi")))

(define-public crate-screwsat-0.1.1 (c (n "screwsat") (v "0.1.1") (h "06b6rjkia6m13yrhp4wxf2xmgiziwwcvrc663dlk4wbq8d8lbwiz")))

(define-public crate-screwsat-0.1.2 (c (n "screwsat") (v "0.1.2") (h "1dy61q9zk6ipc7yrb148c37kdgg1f0laap8x3h8i32asijcpidj2")))

(define-public crate-screwsat-0.1.3 (c (n "screwsat") (v "0.1.3") (d (list (d (n "walkdir") (r "^2") (d #t) (k 2)))) (h "0i03hd50im3kd93gxxl3vc76y55k0n71p1wfbnnzxi38h6cvnkf8")))

(define-public crate-screwsat-1.0.0 (c (n "screwsat") (v "1.0.0") (d (list (d (n "walkdir") (r "^2") (d #t) (k 2)))) (h "1r2vqh41vprd2pg8wpkq5f4in46ylzq2vs9jz2n4anfxg71j9n66")))

(define-public crate-screwsat-1.0.1 (c (n "screwsat") (v "1.0.1") (d (list (d (n "walkdir") (r "^2") (d #t) (k 2)))) (h "1yxcd60m1jh2f1rr3frhyl7ad3yxmidrdl0ccv1ql2kivnb27ygq")))

(define-public crate-screwsat-1.0.2 (c (n "screwsat") (v "1.0.2") (d (list (d (n "walkdir") (r "^2") (d #t) (k 2)))) (h "030hhcqaczjnjlyv3xbd4lzhywb7kz3vj7gxb2fark76183ygwak")))

(define-public crate-screwsat-1.1.0 (c (n "screwsat") (v "1.1.0") (d (list (d (n "walkdir") (r "^2") (d #t) (k 2)))) (h "0z17z7ih4fi4b6cy1bn1gq08jhjsphj5q564ai8ilh4nbb3ig818")))

(define-public crate-screwsat-1.2.0 (c (n "screwsat") (v "1.2.0") (d (list (d (n "walkdir") (r "^2") (d #t) (k 2)))) (h "0976wcvbf4p59x65fvj4v6p3iqcf28y3zhjazs9mb0ibv1zl6cg0")))

(define-public crate-screwsat-1.2.1 (c (n "screwsat") (v "1.2.1") (d (list (d (n "walkdir") (r "^2") (d #t) (k 2)))) (h "152lhaiz82z9wr8dxbbqvcwkknx0wi5yhcsmchb8xjc2njnslj0p")))

(define-public crate-screwsat-1.2.2 (c (n "screwsat") (v "1.2.2") (d (list (d (n "walkdir") (r "^2") (d #t) (k 2)))) (h "0dhc7sx7k2v4x3x1qdhlgv4fd4ifzydcdh1914jlxw0b91zasc1z")))

(define-public crate-screwsat-1.2.3 (c (n "screwsat") (v "1.2.3") (d (list (d (n "walkdir") (r "^2") (d #t) (k 2)))) (h "0i7iymkq0szsgs8i4mkjlpsxs6wjdvvzhsq23gdf23wkqsa4xg93")))

(define-public crate-screwsat-2.0.0 (c (n "screwsat") (v "2.0.0") (d (list (d (n "walkdir") (r "^2") (d #t) (k 2)))) (h "0f76x4mn0ghzwvnrx3mnmpbqs436hrv5bj4hwfb80nc16rzi9sik") (f (quote (("unsafe"))))))

(define-public crate-screwsat-2.1.0 (c (n "screwsat") (v "2.1.0") (d (list (d (n "walkdir") (r "^2") (d #t) (k 2)))) (h "1942apdk0r1s89pm45kyi31xgvf4ig5pyvc7sjdcmhmgfy51r1q2") (f (quote (("unsafe"))))))

(define-public crate-screwsat-2.1.1 (c (n "screwsat") (v "2.1.1") (d (list (d (n "walkdir") (r "^2") (d #t) (k 2)))) (h "0zxspmykk8fhpspx5pjbmgj4sbqkdqzb9fsjrylhzkilqwnlcc0d") (f (quote (("unsafe"))))))

(define-public crate-screwsat-2.1.2 (c (n "screwsat") (v "2.1.2") (d (list (d (n "walkdir") (r "^2") (d #t) (k 2)))) (h "1spbni20cc3cfdiz5d9idrshblypsp341af710carndvb35q2g0d") (f (quote (("unsafe"))))))

(define-public crate-screwsat-2.1.4 (c (n "screwsat") (v "2.1.4") (d (list (d (n "walkdir") (r "^2") (d #t) (k 2)))) (h "1fwrn6zhcpqrzb8pihi5r2vzi5v4sszs4y2clnfxc593wys9hz2s") (f (quote (("unsafe"))))))

(define-public crate-screwsat-2.1.5 (c (n "screwsat") (v "2.1.5") (d (list (d (n "walkdir") (r "^2") (d #t) (k 2)))) (h "0m4gsp9f51p0xv0gwfpf2in9gw7b1h1yihim68x33xfj2bms36d1") (f (quote (("unsafe"))))))

