(define-module (crates-io sc re screenruster-saver-hacks) #:use-module (crates-io))

(define-public crate-screenruster-saver-hacks-0.1.0 (c (n "screenruster-saver-hacks") (v "0.1.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "screenruster-saver") (r "^0.1") (d #t) (k 0)))) (h "1zx6cf2cjyr3lb1yrbz6rx5w5q9lmrqxxvjxcbqcinsy6jfg9zm4")))

(define-public crate-screenruster-saver-hacks-0.1.1 (c (n "screenruster-saver-hacks") (v "0.1.1") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "screenruster-saver") (r "^0.1") (k 0)))) (h "11vhch78zwq72xr425xbcgl8dyya46j56r3m8fwi00cmhmx36127")))

(define-public crate-screenruster-saver-hacks-0.2.0 (c (n "screenruster-saver-hacks") (v "0.2.0") (d (list (d (n "dirs") (r "^2") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "screen") (r "^0.2") (d #t) (k 0) (p "screenruster-saver")))) (h "1d0x7ijzk14d5n4pw9ahyk88vp5w9cp1kpnhq3v3i33dzr0gppgr")))

(define-public crate-screenruster-saver-hacks-0.2.1 (c (n "screenruster-saver-hacks") (v "0.2.1") (d (list (d (n "dirs") (r "^2") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "screen") (r "^0.2") (d #t) (k 0) (p "screenruster-saver")))) (h "1bzr9f7vxqnywz259g3q8qh064jmnplh7ssrnxyavhdilplyrk6x")))

