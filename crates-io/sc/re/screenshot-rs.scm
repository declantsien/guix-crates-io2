(define-module (crates-io sc re screenshot-rs) #:use-module (crates-io))

(define-public crate-screenshot-rs-0.1.0 (c (n "screenshot-rs") (v "0.1.0") (h "1isyrka44d2m8kkwhngv2s7idzlk8cd4pcs017cimk8fgcdg7cj0")))

(define-public crate-screenshot-rs-0.1.1 (c (n "screenshot-rs") (v "0.1.1") (h "0w3imphalyfhah60x78dql3wdi6sj00mmc25nbhfwd9w2z84ri4s")))

(define-public crate-screenshot-rs-0.1.2 (c (n "screenshot-rs") (v "0.1.2") (h "0lxzi545i2jgbrsxyhn3li7g84wvxrn569ki5a7qs61k6kcrnb42")))

(define-public crate-screenshot-rs-0.1.3 (c (n "screenshot-rs") (v "0.1.3") (h "04wb7qm632jz0c9dn725xdrsccnzmjhvdrvdjghd3k37yhcj0kfx")))

(define-public crate-screenshot-rs-0.1.4 (c (n "screenshot-rs") (v "0.1.4") (h "05sil936zka7bpkzy36zbrkfbvz7pdnd4mv4kvrr6dpjd9a8dn87")))

(define-public crate-screenshot-rs-0.1.5 (c (n "screenshot-rs") (v "0.1.5") (h "0r4s1462smwa4qz498zny6s4hxb1scdnw9ja2hq4c4xm3zcraamr")))

