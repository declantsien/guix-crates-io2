(define-module (crates-io sc re screeps-game-utils) #:use-module (crates-io))

(define-public crate-screeps-game-utils-0.17.0 (c (n "screeps-game-utils") (v "0.17.0") (d (list (d (n "assert_approx_eq") (r "^1.1") (d #t) (k 2)) (d (n "screeps-game-api") (r "^0.17") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0avcmidk5ig166fnd8ymqa3lv2610y7c35smhndcdx1ivj5ngbh9") (f (quote (("default"))))))

(define-public crate-screeps-game-utils-0.18.0 (c (n "screeps-game-utils") (v "0.18.0") (d (list (d (n "assert_approx_eq") (r "^1.1") (d #t) (k 2)) (d (n "screeps-game-api") (r "^0.18") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0xybzhnyixgy44z47a20kdgmiyd5rhgwnry256i5r4qhdqkfyw6p") (f (quote (("default"))))))

(define-public crate-screeps-game-utils-0.19.0 (c (n "screeps-game-utils") (v "0.19.0") (d (list (d (n "assert_approx_eq") (r "^1.1") (d #t) (k 2)) (d (n "screeps-game-api") (r "^0.19") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "05dqvkyq99fvnksfkakabcc0vcdd57n4ywjd0mr6qylil3pmzai7") (f (quote (("default"))))))

(define-public crate-screeps-game-utils-0.20.0 (c (n "screeps-game-utils") (v "0.20.0") (d (list (d (n "assert_approx_eq") (r "^1.1") (d #t) (k 2)) (d (n "screeps-game-api") (r "^0.20") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0bq81k9kg4zsnbhwkc6rqik7hwvvn1gsl4iq8n3g3njar8ilsvla") (f (quote (("default"))))))

(define-public crate-screeps-game-utils-0.21.0 (c (n "screeps-game-utils") (v "0.21.0") (d (list (d (n "assert_approx_eq") (r "^1.1") (d #t) (k 2)) (d (n "screeps-game-api") (r "^0.20") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0rjaz9x0alg9igbnd0px9v9bji8sir7mx3y4yy0zw9jlih2apfy2") (f (quote (("default")))) (y #t)))

(define-public crate-screeps-game-utils-0.21.1 (c (n "screeps-game-utils") (v "0.21.1") (d (list (d (n "assert_approx_eq") (r "^1.1") (d #t) (k 2)) (d (n "screeps-game-api") (r "^0.21") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "18rbsq244dfs6yfb461la89fj2rg012j3v3qsz2rhkc8n68f3840") (f (quote (("default"))))))

