(define-module (crates-io sc re screencapturekit) #:use-module (crates-io))

(define-public crate-screencapturekit-0.1.0 (c (n "screencapturekit") (v "0.1.0") (d (list (d (n "screencapturekit-sys") (r "^0.1.0") (d #t) (k 0)))) (h "0vnzks8d47bpr2wzyvychx5xhxj42r262nd3cx04d1bh7w2c6aqm") (y #t)))

(define-public crate-screencapturekit-0.2.1 (c (n "screencapturekit") (v "0.2.1") (d (list (d (n "objc-foundation") (r "^0.1") (d #t) (k 2)) (d (n "screencapturekit-sys") (r "^0.2.1") (d #t) (k 0)))) (h "181qdns48gxkz04gr4qcnnfcyf3qczlxs790h36v6x59jfr7dw2l") (f (quote (("ci"))))))

(define-public crate-screencapturekit-0.2.2 (c (n "screencapturekit") (v "0.2.2") (d (list (d (n "screencapturekit-sys") (r "^0.2.2") (d #t) (k 0)))) (h "0pb9ii575zaxmy0xrnjvgspd63zn9b0pd4w486q03fz58kvnlmb1") (f (quote (("ci")))) (r "1.75")))

(define-public crate-screencapturekit-0.2.3 (c (n "screencapturekit") (v "0.2.3") (d (list (d (n "screencapturekit-sys") (r "^0.2.3") (d #t) (k 0)))) (h "099ilzvccfmxvqicqdcq1zvddq7z70xbd1q3n083mfbry1k1n6vy") (f (quote (("ci")))) (r "1.75")))

(define-public crate-screencapturekit-0.2.4 (c (n "screencapturekit") (v "0.2.4") (d (list (d (n "screencapturekit-sys") (r "^0.2.4") (d #t) (k 0)))) (h "0qaclml35f4hbpx78si8n8xk5k3va8w1rv04zvsqlg1d9wwrfkfr") (f (quote (("ci")))) (r "1.75")))

(define-public crate-screencapturekit-0.2.5 (c (n "screencapturekit") (v "0.2.5") (d (list (d (n "screencapturekit-sys") (r "^0.2.5") (d #t) (k 0)))) (h "0yxvbz7q1pqf4p9glxhdam2vhjbl4zzknag7m9whi0c8ai2pqvk5") (f (quote (("ci")))) (r "1.75")))

(define-public crate-screencapturekit-0.2.6 (c (n "screencapturekit") (v "0.2.6") (d (list (d (n "screencapturekit-sys") (r "^0.2.6") (d #t) (k 0)))) (h "06h8gggv01ydr7nwhdhdcv7nxq4hm4jrlafw48q5a0i8325bakk9") (f (quote (("ci")))) (r "1.75")))

(define-public crate-screencapturekit-0.2.7 (c (n "screencapturekit") (v "0.2.7") (d (list (d (n "screencapturekit-sys") (r "^0.2.7") (d #t) (k 0)))) (h "1ws4rf3g9ghkph8rqpkrydkznbjhfmza34ns119i3vnvc5nn4pi3") (f (quote (("ci")))) (r "1.75")))

(define-public crate-screencapturekit-0.2.8 (c (n "screencapturekit") (v "0.2.8") (d (list (d (n "screencapturekit-sys") (r "^0.2.8") (d #t) (k 0)))) (h "0knn97lc4j304wdz8b4ssb4f8njqwqml0k7zwp7n0jf9gasywphs") (f (quote (("ci")))) (r "1.75")))

