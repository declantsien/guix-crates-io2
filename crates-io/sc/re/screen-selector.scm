(define-module (crates-io sc re screen-selector) #:use-module (crates-io))

(define-public crate-screen-selector-0.1.0 (c (n "screen-selector") (v "0.1.0") (d (list (d (n "gbuild") (r "^0.1.0") (o #t) (d #t) (k 1)) (d (n "gtk") (r "^0.2") (o #t) (d #t) (k 0) (p "gtk4")) (d (n "once_cell") (r "^1.8") (d #t) (k 0)))) (h "1gxxnb256zxxy3lcmaig988j79m56pi9wd2gahlk1fa0ib45j69d") (f (quote (("gui" "gtk" "gbuild") ("default" "gui"))))))

(define-public crate-screen-selector-0.1.1 (c (n "screen-selector") (v "0.1.1") (d (list (d (n "gbuild") (r "^0.1.0") (o #t) (d #t) (k 1)) (d (n "gtk") (r "^0.2") (o #t) (d #t) (k 0) (p "gtk4")) (d (n "once_cell") (r "^1.8") (d #t) (k 0)))) (h "1k4m8y5ysz0fph6xsyn2hphf6mdfajj7x87lgd5wg0r00nynf64l") (f (quote (("gui" "gtk" "gbuild") ("default" "gui"))))))

