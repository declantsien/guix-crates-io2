(define-module (crates-io sc re screeps-async) #:use-module (crates-io))

(define-public crate-screeps-async-0.1.0 (c (n "screeps-async") (v "0.1.0") (d (list (d (n "async-task") (r "^4.7") (d #t) (k 0)) (d (n "flume") (r "^0.11") (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)) (d (n "screeps-async-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "screeps-game-api") (r "^0.20.1") (d #t) (k 0)))) (h "05bs1rlmnndgcclc3xl5lp08sr78zfa095vsc92pdp9qaxadbir8")))

(define-public crate-screeps-async-0.1.1 (c (n "screeps-async") (v "0.1.1") (d (list (d (n "async-task") (r "^4.7") (d #t) (k 0)) (d (n "flume") (r "^0.11") (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)) (d (n "screeps-async-macros") (r "^0.1.1") (d #t) (k 0)) (d (n "screeps-game-api") (r "^0.20.1") (d #t) (k 0)))) (h "12xcx3rhv7cp8972dx2apzmdyf1ryfah6g1b3pi255q45wrgg0ir")))

(define-public crate-screeps-async-0.2.0 (c (n "screeps-async") (v "0.2.0") (d (list (d (n "async-task") (r "^4.7") (d #t) (k 0)) (d (n "flume") (r "^0.11") (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)) (d (n "screeps-async-macros") (r "^0.1.2") (d #t) (k 0)) (d (n "screeps-game-api") (r "^0.20.1") (d #t) (k 0)))) (h "19g05m4nivy7smx68jlmd7m97yfr265l8mbiyal46x60c99x74wg")))

(define-public crate-screeps-async-0.3.0 (c (n "screeps-async") (v "0.3.0") (d (list (d (n "async-task") (r "^4.7") (d #t) (k 0)) (d (n "flume") (r "^0.11") (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)) (d (n "screeps-async-macros") (r "^0.1.3") (d #t) (k 0)) (d (n "screeps-game-api") (r "^0.20.1") (d #t) (k 0)))) (h "0qalmfg3712hj285lmpwhqchfck7af65clc1s0gdzrprklpvd8y3")))

