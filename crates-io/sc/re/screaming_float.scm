(define-module (crates-io sc re screaming_float) #:use-module (crates-io))

(define-public crate-screaming_float-0.1.0 (c (n "screaming_float") (v "0.1.0") (d (list (d (n "noisy_float") (r "^0.1.13") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.12") (d #t) (k 0)) (d (n "rodio") (r "^0.11.0") (d #t) (k 0)))) (h "1g8fgbvnkyakydj9gdjls8nwdhyb8alpv2k5p0y92bf0pb6d4xwi")))

(define-public crate-screaming_float-0.1.1 (c (n "screaming_float") (v "0.1.1") (d (list (d (n "noisy_float") (r "^0.1.13") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.12") (d #t) (k 0)) (d (n "rodio") (r "^0.11.0") (d #t) (k 0)))) (h "1pz6pdic54h3diz37awy3yq6pz7vfy9wycrj3986ni6mvkmpln1w")))

