(define-module (crates-io sc re screentaker) #:use-module (crates-io))

(define-public crate-screentaker-0.1.0 (c (n "screentaker") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "screenshots") (r "^0.8.5") (d #t) (k 0)) (d (n "termion") (r "^2.0.1") (d #t) (k 0)) (d (n "whoami") (r "^1.4.1") (d #t) (k 0)))) (h "1xcha1bpky5cx4hm4nmq3q3r6wk7a4s8f647yvawlw8jn2i1nsyf")))

(define-public crate-screentaker-0.1.1 (c (n "screentaker") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "screenshots") (r "^0.8.5") (d #t) (k 0)) (d (n "termion") (r "^2.0.1") (d #t) (k 0)) (d (n "whoami") (r "^1.4.1") (d #t) (k 0)))) (h "04pbc5fzpdn6kv19rvh1r6ghdq0pgvklzgcrcffr7bnwbkx99077")))

(define-public crate-screentaker-0.1.2 (c (n "screentaker") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "screenshots") (r "^0.8.5") (d #t) (k 0)) (d (n "termion") (r "^2.0.1") (d #t) (k 0)) (d (n "whoami") (r "^1.4.1") (d #t) (k 0)))) (h "16ris1hqii79hzr9wz1hsqwc625r4f90b2003pgbxcgw6k4wlkap")))

(define-public crate-screentaker-0.1.3 (c (n "screentaker") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "screenshots") (r "^0.8.5") (d #t) (k 0)) (d (n "termion") (r "^2.0.1") (d #t) (k 0)) (d (n "whoami") (r "^1.4.1") (d #t) (k 0)))) (h "1vg9nwa72dl4scfxyvzdzayfppw9q4lpgvkw0yz2fw0fx30fh7hr")))

