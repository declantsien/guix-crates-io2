(define-module (crates-io sc re screenlocker) #:use-module (crates-io))

(define-public crate-screenlocker-0.1.0 (c (n "screenlocker") (v "0.1.0") (h "1dczn4z6l0hrbw8iar1lx0xll93y8c8zfh95gm397cgardfakip3")))

(define-public crate-screenlocker-0.2.0 (c (n "screenlocker") (v "0.2.0") (h "08g5g07n9br8gph47cqz8slrkbmpy165c50rdzjln7frr9l34vxq")))

(define-public crate-screenlocker-0.3.0 (c (n "screenlocker") (v "0.3.0") (h "1k36mxl69z3rddssmc2c16454gd4yl5s4cbgr6pv15dlyrwyh7v9")))

(define-public crate-screenlocker-0.3.1 (c (n "screenlocker") (v "0.3.1") (h "0qvggkj3i5iaw8b2lwxvk6dpyp52gllx34l80nncvy8hzzjf33pv")))

(define-public crate-screenlocker-0.3.2 (c (n "screenlocker") (v "0.3.2") (h "1ip3qq6kyfrijz8q5ydfk9y024ag5ijknzs2r541zh1gxvsmi7nb")))

(define-public crate-screenlocker-0.3.3 (c (n "screenlocker") (v "0.3.3") (h "0q3dk18d5wll04zp053q73i00kj6vz5y028dx2k9lkwr4wlvr86n")))

