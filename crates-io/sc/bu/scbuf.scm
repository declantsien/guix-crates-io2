(define-module (crates-io sc bu scbuf) #:use-module (crates-io))

(define-public crate-scbuf-0.1.0 (c (n "scbuf") (v "0.1.0") (h "17imvhmgj047msqx2qmyfj6macjz72yjrg4n2b4sk8x3i3scl8nd")))

(define-public crate-scbuf-0.1.1 (c (n "scbuf") (v "0.1.1") (h "0jrfc5gcdjgija8g64ni9p2n575m3vfvmll3bw8yp1w6k5pfhgcm")))

