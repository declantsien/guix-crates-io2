(define-module (crates-io sc -e sc-executor-polkavm) #:use-module (crates-io))

(define-public crate-sc-executor-polkavm-0.0.0 (c (n "sc-executor-polkavm") (v "0.0.0") (h "01knh68zci32m402jyrjaj4fxr47yjram35fhm84czbd1sp3gpnp")))

(define-public crate-sc-executor-polkavm-0.29.0 (c (n "sc-executor-polkavm") (v "0.29.0") (d (list (d (n "log") (r "^0.4.20") (k 0)) (d (n "polkavm") (r "^0.9.3") (d #t) (k 0)) (d (n "sc-executor-common") (r "^0.32.0") (d #t) (k 0)) (d (n "sp-wasm-interface") (r "^20.0.0") (d #t) (k 0)))) (h "1h6g70ldz389xicbi7lpqp2wi7iv3abaca6p4fizfkd9scacifb3")))

(define-public crate-sc-executor-polkavm-0.30.0 (c (n "sc-executor-polkavm") (v "0.30.0") (d (list (d (n "log") (r "^0.4.21") (k 0)) (d (n "polkavm") (r "^0.9.3") (d #t) (k 0)) (d (n "sc-executor-common") (r "^0.33.0") (d #t) (k 0)) (d (n "sp-wasm-interface") (r "^21.0.0") (d #t) (k 0)))) (h "096ya2ifcpzp56ncswzprpqrabqnzlzbc56i97s19fkrnwi6pfbg")))

(define-public crate-sc-executor-polkavm-0.31.0 (c (n "sc-executor-polkavm") (v "0.31.0") (d (list (d (n "log") (r "^0.4.21") (k 0)) (d (n "polkavm") (r "^0.9.3") (d #t) (k 0)) (d (n "sc-executor-common") (r "^0.34.0") (d #t) (k 0)) (d (n "sp-wasm-interface") (r "^21.0.0") (d #t) (k 0)))) (h "013k73g6iixg12swzlfrmnr5vhb4ws36bbd6facsidjkx846lcyv")))

