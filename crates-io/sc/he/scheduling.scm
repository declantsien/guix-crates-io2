(define-module (crates-io sc he scheduling) #:use-module (crates-io))

(define-public crate-scheduling-0.1.0 (c (n "scheduling") (v "0.1.0") (h "0bjwingaxrv35n49vqyqdv93iffjb3dc6wkx7kmlchl20x039dj1")))

(define-public crate-scheduling-0.1.1 (c (n "scheduling") (v "0.1.1") (h "0g50yrsndv0r63db9fcdspy15hniqqwqhs53j6i6swy5nc8g3yr0")))

(define-public crate-scheduling-0.1.2 (c (n "scheduling") (v "0.1.2") (h "0aq6ymv9lcxxdcn9s18w5r2mf27s9jjrsmn00d9hx3dq40izqal8")))

