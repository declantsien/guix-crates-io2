(define-module (crates-io sc he schemamama_postgres) #:use-module (crates-io))

(define-public crate-schemamama_postgres-0.0.1 (c (n "schemamama_postgres") (v "0.0.1") (d (list (d (n "postgres") (r "^0.8") (d #t) (k 0)) (d (n "schemamama") (r "*") (d #t) (k 0)))) (h "1kwcjj0481629g75hd5pb0qx76v01svayx3bzs4s2zp5d213ypvd")))

(define-public crate-schemamama_postgres-0.0.2 (c (n "schemamama_postgres") (v "0.0.2") (d (list (d (n "postgres") (r "^0.8") (d #t) (k 0)) (d (n "schemamama") (r "*") (d #t) (k 0)))) (h "1azfvdk6hcnxf0018h5q3yf5awhs6xm0swfhwsy4f6w5mbs6mjsy")))

(define-public crate-schemamama_postgres-0.0.3 (c (n "schemamama_postgres") (v "0.0.3") (d (list (d (n "postgres") (r "^0.9") (d #t) (k 0)) (d (n "schemamama") (r "*") (d #t) (k 0)))) (h "1k0szi9kg5fg6pd2np8lkas1w2xpia5fyzrczdq0gbqymir12qbc")))

(define-public crate-schemamama_postgres-0.0.4 (c (n "schemamama_postgres") (v "0.0.4") (d (list (d (n "postgres") (r "^0.10") (d #t) (k 0)) (d (n "schemamama") (r "*") (d #t) (k 0)))) (h "0bs17r4s61gxbg71nygspbp4cyrzx16xnx31lvv55damlcnd124i")))

(define-public crate-schemamama_postgres-0.0.5 (c (n "schemamama_postgres") (v "0.0.5") (d (list (d (n "postgres") (r "^0.11") (d #t) (k 0)) (d (n "schemamama") (r "*") (d #t) (k 0)))) (h "0va0b9cdx92d41abkld0mgjhyx04qjyrqaixr7nffbbdl6pp532s")))

(define-public crate-schemamama_postgres-0.0.6 (c (n "schemamama_postgres") (v "0.0.6") (d (list (d (n "postgres") (r "^0.11") (d #t) (k 0)) (d (n "schemamama") (r "*") (d #t) (k 0)))) (h "1q5d9i80jfgl8c2y804bq6mjlla5n51aw0s43ysvims2lx7xlp46")))

(define-public crate-schemamama_postgres-0.0.7 (c (n "schemamama_postgres") (v "0.0.7") (d (list (d (n "postgres") (r "^0.11") (d #t) (k 0)) (d (n "schemamama") (r "^0.0.11") (d #t) (k 0)))) (h "02gfjwa5hrw9i7qib9lplqznplzw4gx6j1nnnih7186rwxwzvzw9")))

(define-public crate-schemamama_postgres-0.0.8 (c (n "schemamama_postgres") (v "0.0.8") (d (list (d (n "postgres") (r "^0.11") (d #t) (k 0)) (d (n "schemamama") (r "^0.0.11") (d #t) (k 0)))) (h "0dv3yk38c5gpr5phddxkfs87kdw9pvhpm9dd0glp4li0hha8q94q")))

(define-public crate-schemamama_postgres-0.1.0 (c (n "schemamama_postgres") (v "0.1.0") (d (list (d (n "postgres") (r "^0.11") (d #t) (k 0)) (d (n "schemamama") (r "^0.1") (d #t) (k 0)))) (h "06dkbfkhn09107jmnqa8mf95y70g6c11884wrn4gm8hgn5j65gmy")))

(define-public crate-schemamama_postgres-0.1.1 (c (n "schemamama_postgres") (v "0.1.1") (d (list (d (n "postgres") (r "^0.11") (d #t) (k 0)) (d (n "schemamama") (r ">= 0.1, <= 0.2") (d #t) (k 0)))) (h "1j94x7r1bqnll3mm3wkcqqh16n9jni1sawz9mhpmsf50w52axxa0")))

(define-public crate-schemamama_postgres-0.2.0 (c (n "schemamama_postgres") (v "0.2.0") (d (list (d (n "postgres") (r "^0.13") (d #t) (k 0)) (d (n "schemamama") (r ">= 0.1, <= 0.2") (d #t) (k 0)))) (h "1vbrqz1cw5q6ijbbd5amzrsk90wppvxj4iadl6xxp898fxp2hvcy")))

(define-public crate-schemamama_postgres-0.2.1 (c (n "schemamama_postgres") (v "0.2.1") (d (list (d (n "postgres") (r ">= 0.13, <= 0.15") (d #t) (k 0)) (d (n "schemamama") (r ">= 0.1, <= 0.2") (d #t) (k 0)))) (h "18sk5gjivrxzmb1pw0rxjxm5dnj7mcaanslja0115hfjmqvqy9sp")))

(define-public crate-schemamama_postgres-0.2.2 (c (n "schemamama_postgres") (v "0.2.2") (d (list (d (n "postgres") (r ">= 0.13") (d #t) (k 0)) (d (n "schemamama") (r ">= 0.1") (d #t) (k 0)))) (h "18daqw7a46y8b3gsffah1gwvwhiz37j4px34qgs8z17mqspzcw3a")))

(define-public crate-schemamama_postgres-0.2.3 (c (n "schemamama_postgres") (v "0.2.3") (d (list (d (n "postgres") (r ">= 0.13") (d #t) (k 0)) (d (n "schemamama") (r ">= 0.1") (d #t) (k 0)))) (h "1xlp2r6hmgxpcia503k2izvax6vjr4jia2nspz2aapv2ggzdwscs")))

(define-public crate-schemamama_postgres-0.2.4 (c (n "schemamama_postgres") (v "0.2.4") (d (list (d (n "postgres") (r ">= 0.13, <= 0.15") (d #t) (k 0)) (d (n "schemamama") (r ">= 0.1") (d #t) (k 0)))) (h "0v5v5bnq6lrvai288x5mj134dfc252zy382dz4f2wjl510rdl5d0")))

(define-public crate-schemamama_postgres-0.3.0 (c (n "schemamama_postgres") (v "0.3.0") (d (list (d (n "postgres") (r ">= 0.17") (d #t) (k 0)) (d (n "schemamama") (r ">= 0.1") (d #t) (k 0)))) (h "1npdzzr4ja7mjzs0pm3696gyfhp28bq6nhd4i462v4pfyyca0p8a")))

