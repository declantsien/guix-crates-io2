(define-module (crates-io sc he schema_org_constants) #:use-module (crates-io))

(define-public crate-schema_org_constants-0.0.1 (c (n "schema_org_constants") (v "0.0.1") (h "14zqcl469grjdkmlnn9nb1lhhg4jvikn4s0f05scali841vwipwc")))

(define-public crate-schema_org_constants-0.0.2 (c (n "schema_org_constants") (v "0.0.2") (h "15bpy8ai2m8pwildgglj8z164ya3zfadsi93j3s2wdqi7xga8xjd")))

(define-public crate-schema_org_constants-0.0.3 (c (n "schema_org_constants") (v "0.0.3") (h "0vfba4vysgd85mkkj9k9yz01r1m5qywrafa7832dfy2nd0hwfnqr") (f (quote (("doc") ("default"))))))

(define-public crate-schema_org_constants-0.0.4 (c (n "schema_org_constants") (v "0.0.4") (h "0djbrwypyh3g37ifw5yj82kqjmjc6bzqfv77l2d5s35srg8xyrjj") (f (quote (("default"))))))

