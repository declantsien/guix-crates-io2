(define-module (crates-io sc he schedule_recv) #:use-module (crates-io))

(define-public crate-schedule_recv-0.0.1 (c (n "schedule_recv") (v "0.0.1") (d (list (d (n "lazy_static") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)))) (h "13rpqlm9g5pywr97czpn53fqhqd7d74kqq5aj9rikrsr2fb1hgsj")))

(define-public crate-schedule_recv-0.1.0 (c (n "schedule_recv") (v "0.1.0") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)))) (h "1ss07pn259fjl95bxh26bm7yk3xngsrsdfapxff350iikp7j05fa")))

