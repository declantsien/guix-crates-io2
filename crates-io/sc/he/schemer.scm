(define-module (crates-io sc he schemer) #:use-module (crates-io))

(define-public crate-schemer-0.1.0 (c (n "schemer") (v "0.1.0") (d (list (d (n "daggy") (r "~0.5") (d #t) (k 0)) (d (n "failure") (r "~0.1") (d #t) (k 0)) (d (n "failure_derive") (r "~0.1") (d #t) (k 0)) (d (n "uuid") (r "~0.5") (f (quote ("use_std"))) (d #t) (k 0)))) (h "0a86v9lywsryc7bq2mvnbaiz3bxh34yr6cmi770dkdm9j68gw0h0")))

(define-public crate-schemer-0.1.1 (c (n "schemer") (v "0.1.1") (d (list (d (n "daggy") (r "~0.5") (d #t) (k 0)) (d (n "failure") (r "~0.1") (d #t) (k 0)) (d (n "failure_derive") (r "~0.1") (d #t) (k 0)) (d (n "uuid") (r "~0.5") (f (quote ("use_std"))) (d #t) (k 0)))) (h "1zmxfdz568qy7pwbav1j9f0q3nr8m473ny9f3q2b870yiyzy5rm3")))

(define-public crate-schemer-0.1.2 (c (n "schemer") (v "0.1.2") (d (list (d (n "daggy") (r "~0.5") (d #t) (k 0)) (d (n "failure") (r "~0.1") (d #t) (k 0)) (d (n "failure_derive") (r "~0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "uuid") (r "~0.5") (f (quote ("use_std"))) (d #t) (k 0)))) (h "1br3ksa5mq3pbp4fjvqvsdqbalv12mlqcnq3pzaqb2i4bf91q0ql")))

(define-public crate-schemer-0.2.0 (c (n "schemer") (v "0.2.0") (d (list (d (n "daggy") (r "^0.6") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^1") (d #t) (k 0)))) (h "1cwqivhg2zaa2ahfjy8i9f6bz8zab0g9ik8aficin442nmwyvw28") (r "1.56")))

(define-public crate-schemer-0.2.1 (c (n "schemer") (v "0.2.1") (d (list (d (n "daggy") (r "^0.8") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^1") (d #t) (k 0)))) (h "0fgk66k8pkm8hjwcnc9npzar820dddkahhf9bwckcagxg2a8ypc3") (r "1.56")))

