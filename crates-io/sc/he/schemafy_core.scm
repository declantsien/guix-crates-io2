(define-module (crates-io sc he schemafy_core) #:use-module (crates-io))

(define-public crate-schemafy_core-0.5.0 (c (n "schemafy_core") (v "0.5.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1iw3nxg3r74ydafcw63scfl1yhkv9cq0ivsmz5rnk645bbh7iz1x")))

(define-public crate-schemafy_core-0.5.1 (c (n "schemafy_core") (v "0.5.1") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0q6xa4gg3kj53vk2ivhgbyia7i273nmq1br183ybx12c29xj4dc3")))

(define-public crate-schemafy_core-0.5.2 (c (n "schemafy_core") (v "0.5.2") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "06sin2ydn55xfb9clbid0f83n3gad92p9yvy534m5zgljbh1ly21")))

(define-public crate-schemafy_core-0.6.0 (c (n "schemafy_core") (v "0.6.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "096ziymx6vi3spw1sj7h7sc76r5mp03jfhhd7hpzjq7yvkfjkv1b")))

