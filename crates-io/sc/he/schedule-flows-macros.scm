(define-module (crates-io sc he schedule-flows-macros) #:use-module (crates-io))

(define-public crate-schedule-flows-macros-0.1.0 (c (n "schedule-flows-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0rw9k534fij3x18dgzbf0habhd867zxf8ll8bwa0crs9fq02nczm")))

