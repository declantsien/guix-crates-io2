(define-module (crates-io sc he schermz) #:use-module (crates-io))

(define-public crate-schermz-0.1.0 (c (n "schermz") (v "0.1.0") (d (list (d (n "clap") (r "^4.3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "indextree") (r "^4.6.0") (d #t) (k 0)) (d (n "insta") (r "^1.29.0") (f (quote ("yaml"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "11h1nd2mq9blw4q4c69pb8dw6giffsai2smn08zf3klp266vy2a8")))

(define-public crate-schermz-0.1.1 (c (n "schermz") (v "0.1.1") (d (list (d (n "clap") (r "^4.3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "indextree") (r "^4.6.0") (d #t) (k 0)) (d (n "insta") (r "^1.29.0") (f (quote ("yaml"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1fl4yrgbm3x7092krl3x2p0jn2syjlh5dv4fdmq2a0a2qxfa5s3r")))

(define-public crate-schermz-0.1.2 (c (n "schermz") (v "0.1.2") (d (list (d (n "clap") (r "^4.3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "indextree") (r "^4.6.0") (d #t) (k 0)) (d (n "insta") (r "^1.29.0") (f (quote ("yaml"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0q04asxl89lri4cy1nbv1rcxvbyrkcbdrbq24h4ffym3wnnsih13")))

(define-public crate-schermz-0.1.3 (c (n "schermz") (v "0.1.3") (d (list (d (n "clap") (r "^4.3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "indextree") (r "^4.6.0") (d #t) (k 0)) (d (n "insta") (r "^1.29.0") (f (quote ("yaml"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1ah0b6facslrhs0jvkgx0pv2q8k2l5aqsxrpw7r8xgq8vsx98pi2")))

(define-public crate-schermz-0.1.4 (c (n "schermz") (v "0.1.4") (d (list (d (n "clap") (r "^4.3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "indextree") (r "^4.6.0") (d #t) (k 0)) (d (n "insta") (r "^1.29.0") (f (quote ("yaml"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0wfl5dbg1fgk8hgn2cqfc6rlnbyz3g7q9x7dxcnrvrfgp6xfkjrr")))

(define-public crate-schermz-0.1.5 (c (n "schermz") (v "0.1.5") (d (list (d (n "clap") (r "^4.3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "indextree") (r "^4.6.0") (d #t) (k 0)) (d (n "insta") (r "^1.29.0") (f (quote ("yaml"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0dvysdx6jb74xs35hlxsi0aqcj4b26yqf8xmrbgl0vc3nqz02d0k")))

(define-public crate-schermz-0.1.6 (c (n "schermz") (v "0.1.6") (d (list (d (n "clap") (r "^4.3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "indextree") (r "^4.6.0") (d #t) (k 0)) (d (n "insta") (r "^1.29.0") (f (quote ("yaml"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1y6arsym7va60cpjwi8slqd735fahl9qhdig2cgmvaj696984p2s")))

(define-public crate-schermz-0.1.7 (c (n "schermz") (v "0.1.7") (d (list (d (n "clap") (r "^4.3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "indextree") (r "^4.6.0") (d #t) (k 0)) (d (n "insta") (r "^1.29.0") (f (quote ("yaml"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "16alj7gal6jjw7fw3p52b31i7xrq3qjibwxg4yaq9nmg4z9y2ljj")))

(define-public crate-schermz-0.1.8 (c (n "schermz") (v "0.1.8") (d (list (d (n "clap") (r "^4.3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "insta") (r "^1.29.0") (f (quote ("yaml"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "17i9azj9vqyfnp1l2fc30sjrqx7a0msaydrgccxcxn5c5581p99p")))

(define-public crate-schermz-0.1.9 (c (n "schermz") (v "0.1.9") (d (list (d (n "clap") (r "^4.3.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "insta") (r "^1.29.0") (f (quote ("yaml"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0k2wgly0hhd220xhz69xgj0zp2n1zr3fbxr6y47wndnjanysxm29")))

(define-public crate-schermz-0.1.10 (c (n "schermz") (v "0.1.10") (d (list (d (n "clap") (r "^4.3.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "insta") (r "^1.29.0") (f (quote ("yaml"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0hicm7rkrz4wibfdis52y9qkzyzg967pmky8dqcqd7qjjs0zgmr5")))

(define-public crate-schermz-0.1.11 (c (n "schermz") (v "0.1.11") (d (list (d (n "clap") (r "^4.3.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "insta") (r "^1.29.0") (f (quote ("json"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0hxbkcplkzzrhdxva84vhyih5mc2q7rwqn7sc43b54k0abfj7v59")))

(define-public crate-schermz-0.1.12 (c (n "schermz") (v "0.1.12") (d (list (d (n "clap") (r "^4.3.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "insta") (r "^1.30.0") (f (quote ("json"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0dswl22811212r2sg5ja6psq1k7jywb97g3cnj1rzw1gdg7md34c")))

(define-public crate-schermz-0.1.13 (c (n "schermz") (v "0.1.13") (d (list (d (n "clap") (r "^4.3.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "insta") (r "^1.31.0") (f (quote ("json"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "15w5m4pdw1n4nng96g29rlzgbrlx5rmcc0fhilaprxqmzfv9x4v6")))

(define-public crate-schermz-0.1.14 (c (n "schermz") (v "0.1.14") (d (list (d (n "clap") (r "^4.4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "insta") (r "^1.31.0") (f (quote ("json"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1babm4b1vkramc8laaa8g71wqhzhi04mdp6namjs94l4s6xmx78q")))

(define-public crate-schermz-0.1.15 (c (n "schermz") (v "0.1.15") (d (list (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "insta") (r "^1.33.0") (f (quote ("json"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0v82arhqa6dbjmbn325c1xvwchdyf2vr740izr9yqp0f2a699lrw")))

(define-public crate-schermz-0.2.0 (c (n "schermz") (v "0.2.0") (d (list (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "insta") (r "^1.33.0") (f (quote ("json"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "12nfqmjyn23m8d93p43ijbxgws74f9j0hi9gjc6m5lps97c7bvl4")))

(define-public crate-schermz-0.2.1 (c (n "schermz") (v "0.2.1") (d (list (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "insta") (r "^1.33.0") (f (quote ("json"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1r1shmdngr5zpqbp32igvxvfr7x7vaz6gfgsi05r3s2lkhhnfjrj")))

(define-public crate-schermz-0.3.0 (c (n "schermz") (v "0.3.0") (d (list (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "insta") (r "^1.33.0") (f (quote ("json"))) (d #t) (k 2)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1z5rgswk6sy99ghvlb62ng2mlbr67x38xlqfayrmh7nibg5j0vgp")))

(define-public crate-schermz-0.3.1 (c (n "schermz") (v "0.3.1") (d (list (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "insta") (r "^1.33.0") (f (quote ("json"))) (d #t) (k 2)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0wj47sb53y2i5i3p9kz8w2b5h7f8l16251abiyki8dph4hzg264v")))

(define-public crate-schermz-0.3.2 (c (n "schermz") (v "0.3.2") (d (list (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "insta") (r "^1.33.0") (f (quote ("json"))) (d #t) (k 2)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "11nxrzca0xj760d1sinz4cf3xrbxd17d8qvxp8h7pvbkqcbl9vr0")))

(define-public crate-schermz-0.3.3 (c (n "schermz") (v "0.3.3") (d (list (d (n "clap") (r "^4.4.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "insta") (r "^1.34.0") (f (quote ("json"))) (d #t) (k 2)) (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0gmyvg4wgvd4pihdwpgs9jvm9akp8ykhx645sq2pq4694rzxj48d")))

(define-public crate-schermz-0.3.4 (c (n "schermz") (v "0.3.4") (d (list (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "insta") (r "^1.35.1") (f (quote ("json"))) (d #t) (k 2)) (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0jycbpz7jpdqm2svbkxgl0lil1rj0ngfpkphblj9wzgykv83vn4f")))

