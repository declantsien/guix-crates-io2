(define-module (crates-io sc he schema-derive) #:use-module (crates-io))

(define-public crate-schema-derive-0.0.1 (c (n "schema-derive") (v "0.0.1") (d (list (d (n "paste") (r "^1.0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.64") (f (quote ("derive" "extra-traits"))) (d #t) (k 0)))) (h "0qp7r4zzimqj552fzrqv0xrzkhg8flxaxr1kql2nq5l457s82l0w")))

(define-public crate-schema-derive-0.1.0 (c (n "schema-derive") (v "0.1.0") (d (list (d (n "paste") (r "^1.0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^2.0.2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0vxs3h6ix1cv31ncq9qswj7mnw1dsb2ji39ajafv3f2w4gxmfpij")))

