(define-module (crates-io sc he schedule-rs) #:use-module (crates-io))

(define-public crate-schedule-rs-0.0.1 (c (n "schedule-rs") (v "0.0.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "1xgd046nax5iicg73nixm1ljhi1mfpn71994wdlv4wblxzxplyy0")))

(define-public crate-schedule-rs-0.0.2 (c (n "schedule-rs") (v "0.0.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "149qbz9d5jhz4246lrdb7b8xp9ljs1wqrxdgrqw08svfgqwghi2x")))

(define-public crate-schedule-rs-0.1.0 (c (n "schedule-rs") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "1ganwbpb356bpfa4fg3ihfn465dnmfcrpmkbzcj4nw1w7halna3f")))

(define-public crate-schedule-rs-0.1.1 (c (n "schedule-rs") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "11zrgpr3260f96qsshc82la2hyg02znnn75q5l18wca7qrk05p71")))

