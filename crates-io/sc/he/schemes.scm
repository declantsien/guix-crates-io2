(define-module (crates-io sc he schemes) #:use-module (crates-io))

(define-public crate-schemes-0.0.1-BETA (c (n "schemes") (v "0.0.1-BETA") (d (list (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 2)) (d (n "colored") (r "^2") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "proptest") (r "^1.0") (d #t) (k 2)) (d (n "regex") (r "^1") (d #t) (k 2)) (d (n "tokio") (r "^1.19") (f (quote ("rt" "rt-multi-thread" "fs" "macros" "io-util"))) (d #t) (k 2)))) (h "07b8zs9qyj6qc5ypg1021s4vbf0273ihhl4yrdkzc0dipsqxd601")))

