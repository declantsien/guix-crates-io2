(define-module (crates-io sc he schemato) #:use-module (crates-io))

(define-public crate-schemato-1.0.0 (c (n "schemato") (v "1.0.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "fern") (r "^0.5") (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "postgres") (r "^0.15") (d #t) (k 0)))) (h "1j818pj1z0jkqacinb7kpkvnsggyd5fxzmql8hzafwk1fb7jsi32")))

