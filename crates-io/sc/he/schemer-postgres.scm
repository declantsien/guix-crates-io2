(define-module (crates-io sc he schemer-postgres) #:use-module (crates-io))

(define-public crate-schemer-postgres-0.1.0 (c (n "schemer-postgres") (v "0.1.0") (d (list (d (n "postgres") (r "~0.15") (f (quote ("with-uuid"))) (d #t) (k 0)) (d (n "schemer") (r "^0.1.0") (d #t) (k 0)) (d (n "uuid") (r "~0.5") (f (quote ("use_std"))) (d #t) (k 0)))) (h "1xjpbz4h950mhi5vhhvylzkr1rwmmp1dq5iqkzvck4s2jd8n9qfw")))

(define-public crate-schemer-postgres-0.1.1 (c (n "schemer-postgres") (v "0.1.1") (d (list (d (n "postgres") (r "~0.15") (f (quote ("with-uuid"))) (d #t) (k 0)) (d (n "schemer") (r "^0.1") (d #t) (k 0)) (d (n "uuid") (r "~0.5") (f (quote ("use_std"))) (d #t) (k 0)))) (h "1wn1n1dfdz7065ckbk5djaxghrn2qcavlgrgx265sbfpkhcgh1mk")))

(define-public crate-schemer-postgres-0.2.0 (c (n "schemer-postgres") (v "0.2.0") (d (list (d (n "postgres") (r "^0.19") (f (quote ("with-uuid-1"))) (d #t) (k 0)) (d (n "schemer") (r "^0.2") (d #t) (k 0)) (d (n "uuid") (r "^1") (d #t) (k 0)))) (h "0az3q62c3hnzavk68lgsz492jjzbkfjqkdxahz9hrggk3h2vidzy") (r "1.56")))

