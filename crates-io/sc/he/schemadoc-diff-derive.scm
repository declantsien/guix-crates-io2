(define-module (crates-io sc he schemadoc-diff-derive) #:use-module (crates-io))

(define-public crate-schemadoc-diff-derive-0.1.0 (c (n "schemadoc-diff-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0ada1ma28mcbk9j8mq7qpdn6ms9l1jhbalhrsyhpny9m1x4067c4")))

