(define-module (crates-io sc he schemamama) #:use-module (crates-io))

(define-public crate-schemamama-0.0.1 (c (n "schemamama") (v "0.0.1") (d (list (d (n "postgres") (r "^0.8") (d #t) (k 0)))) (h "0d99d6diisl4axwfgrypgc0gfpxr11cwwrdyp4gzsydb6pn1xvcy")))

(define-public crate-schemamama-0.0.2 (c (n "schemamama") (v "0.0.2") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "1ggnk6ncmw1c6kf8p2miikxvaf9zdrkf22gwsr3f0k9m3vhiyiyb")))

(define-public crate-schemamama-0.0.3 (c (n "schemamama") (v "0.0.3") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "0x70mcd477bq0ljlr2m4dd34jckakg7alixyamshm71laxxq39cc")))

(define-public crate-schemamama-0.0.4 (c (n "schemamama") (v "0.0.4") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "1cv9srj3a7g196r4l8z9lqxvs94yy16vg7aidzz4q75cjzrgvq2n")))

(define-public crate-schemamama-0.0.5 (c (n "schemamama") (v "0.0.5") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "17354rwnhy9dvmmj4hqqkz01xdg6if0kkgjh9fc4icbkzl40s77d")))

(define-public crate-schemamama-0.0.6 (c (n "schemamama") (v "0.0.6") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "01vahjzc4d06b18vw2i273nqnia30q8f6s2fr64yhxakm5sximkp")))

(define-public crate-schemamama-0.0.8 (c (n "schemamama") (v "0.0.8") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "0sbq418ysgl5y4vvwv90fsmw2jki9r6dm3jbynavhy5wiqgp8zmf")))

(define-public crate-schemamama-0.0.9 (c (n "schemamama") (v "0.0.9") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "031kpc39i85azjfykw6h01zkghhxx6dbfphiysvm7f6zw41nn41k")))

(define-public crate-schemamama-0.0.10 (c (n "schemamama") (v "0.0.10") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "0z5gh7ir8135wr09jq8qlpfc7j8k1697x27d495pmhq003pjzv4c")))

(define-public crate-schemamama-0.0.11 (c (n "schemamama") (v "0.0.11") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "191mxq2n8nm643599gf4irrgfn8xxb2v533qb35wgg2c14shv55f")))

(define-public crate-schemamama-0.1.0 (c (n "schemamama") (v "0.1.0") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "0g5ii6szh79d2qd3ws8n8zgl9jvs9w4d7n44qvbrvkmd6cwmapvg")))

(define-public crate-schemamama-0.2.0 (c (n "schemamama") (v "0.2.0") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "1b7hya51zaxmg1d003n1f50a4fpvdlsk1abqcj1h5syqdr8rb2d3")))

(define-public crate-schemamama-0.2.1 (c (n "schemamama") (v "0.2.1") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "197ckj4zan4hna657gisq0y8i52whmz42pl7067wnxmc6qayvhc7")))

(define-public crate-schemamama-0.3.0 (c (n "schemamama") (v "0.3.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1yayhml0pl2yyynn5ajn21lbnjnzbmvmabn18nsr32hr20xnswhz")))

