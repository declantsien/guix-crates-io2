(define-module (crates-io sc he schedule-flows) #:use-module (crates-io))

(define-public crate-schedule-flows-0.1.1 (c (n "schedule-flows") (v "0.1.1") (d (list (d (n "http_req_wasi") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "urlencoding") (r "^2") (d #t) (k 0)))) (h "1vdchq2y4wwqx3li7pi771my17sxdb7wqil6y3bfnvk1wxinkfpm")))

(define-public crate-schedule-flows-0.1.2 (c (n "schedule-flows") (v "0.1.2") (d (list (d (n "http_req_wasi") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "urlencoding") (r "^2") (d #t) (k 0)))) (h "0mx0ciqmbnl69q4vv50ffkzaxmlakjahcylvzsycz1frb66za8jn")))

(define-public crate-schedule-flows-0.1.3 (c (n "schedule-flows") (v "0.1.3") (d (list (d (n "http_req_wasi") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "urlencoding") (r "^2") (d #t) (k 0)))) (h "14crgk5lff1yv9brmj57ifjal6nl3xi9yqjfg2fh3cgbdcpwxmqn")))

(define-public crate-schedule-flows-0.1.4 (c (n "schedule-flows") (v "0.1.4") (d (list (d (n "http_req_wasi") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "urlencoding") (r "^2") (d #t) (k 0)))) (h "1ivf9pkx6daf5bs0pylmj7wi1l5kgnjaqqfbg1raj4gzc73lfdfs")))

(define-public crate-schedule-flows-0.1.5 (c (n "schedule-flows") (v "0.1.5") (d (list (d (n "http_req_wasi") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "urlencoding") (r "^2") (d #t) (k 0)))) (h "185djjf78pzkcq8h2l3p0s12ycvq061qxbhwklxm800b3v17wq16")))

(define-public crate-schedule-flows-0.1.6 (c (n "schedule-flows") (v "0.1.6") (d (list (d (n "http_req_wasi") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "urlencoding") (r "^2") (d #t) (k 0)))) (h "10maabrzpwqbh8b0z3vrhcm75nldnkgqy08d0l4cbzzg06azm7m2")))

(define-public crate-schedule-flows-0.1.7 (c (n "schedule-flows") (v "0.1.7") (d (list (d (n "http_req_wasi") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "urlencoding") (r "^2") (d #t) (k 0)))) (h "0l7c2cw7hlgkchdxa4kg54rh57fdw47smhdnhb7rrm6bixdrgnnz")))

(define-public crate-schedule-flows-0.1.8 (c (n "schedule-flows") (v "0.1.8") (d (list (d (n "http_req_wasi") (r "^0.10") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "urlencoding") (r "^2") (d #t) (k 0)))) (h "1fwcpvm8sgjqlyxi5vhvz33wq3kf0j8vkyh438lpfzgii8l52iqr")))

(define-public crate-schedule-flows-0.1.9 (c (n "schedule-flows") (v "0.1.9") (d (list (d (n "http_req_wasi") (r "^0.10") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "urlencoding") (r "^2") (d #t) (k 0)))) (h "017sm5yv7a9w470ml5w0m5xrv3bmqzipk5nsaxf323sjczxn8hpn")))

(define-public crate-schedule-flows-0.1.10 (c (n "schedule-flows") (v "0.1.10") (d (list (d (n "http_req_wasi") (r "^0.10") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "urlencoding") (r "^2") (d #t) (k 0)))) (h "0gzpnjsn8z8rnrmv850mads6dzqqq2lbxzasp6fcfg1izkqsvvcp")))

(define-public crate-schedule-flows-0.2.0 (c (n "schedule-flows") (v "0.2.0") (d (list (d (n "http_req_wasi") (r "^0.10") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "urlencoding") (r "^2") (d #t) (k 0)))) (h "1xm9w3xsfchqj78gay3ysvad5r2c8wb1m6y4w553s9vjgqvraljr")))

(define-public crate-schedule-flows-0.2.1 (c (n "schedule-flows") (v "0.2.1") (d (list (d (n "http_req_wasi") (r "^0.10") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "urlencoding") (r "^2") (d #t) (k 0)))) (h "1mckcmprgcvb51iqqn9i2cj72nnv135gjhzl4bv5sp1x5c903bh2")))

(define-public crate-schedule-flows-0.2.2 (c (n "schedule-flows") (v "0.2.2") (d (list (d (n "http_req_wasi") (r "^0.10") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "urlencoding") (r "^2") (d #t) (k 0)))) (h "0x05xhwh935hrpihbcji5wsgb8rrzjn8q085sqzrh0k3gid37023")))

(define-public crate-schedule-flows-0.3.0 (c (n "schedule-flows") (v "0.3.0") (d (list (d (n "http_req_wasi") (r "^0.10") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "schedule-flows-macros") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "urlencoding") (r "^2") (d #t) (k 0)))) (h "1dnld8aaim8v4xr4m5pw8dzkk1wajwzha6y6ff1ys23i70n75dq5")))

