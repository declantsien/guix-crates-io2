(define-module (crates-io sc he schema-org) #:use-module (crates-io))

(define-public crate-schema-org-0.0.1 (c (n "schema-org") (v "0.0.1") (h "10vh528lp7cnbg7qw943sr8a1pk5i6zsv7ix9fqy24lv13kdd4f0") (y #t)))

(define-public crate-schema-org-0.0.1-1 (c (n "schema-org") (v "0.0.1-1") (h "10s8595swf860gq056f94g7gxgxk71kmwlpcy6q36i54y232va9v")))

