(define-module (crates-io sc he schemer-rusqlite) #:use-module (crates-io))

(define-public crate-schemer-rusqlite-0.1.0 (c (n "schemer-rusqlite") (v "0.1.0") (d (list (d (n "rusqlite") (r "^0.13") (d #t) (k 0)) (d (n "schemer") (r "^0.1.2") (d #t) (k 0)) (d (n "uuid") (r "~0.5") (f (quote ("use_std"))) (d #t) (k 0)))) (h "08wr1k6saadabq2n8xvkayyhm58hfii2rgr3887gisjfam1i82f6")))

(define-public crate-schemer-rusqlite-0.1.1 (c (n "schemer-rusqlite") (v "0.1.1") (d (list (d (n "rusqlite") (r "^0.14.0") (d #t) (k 0)) (d (n "schemer") (r "^0.1.2") (d #t) (k 0)) (d (n "uuid") (r "~0.5") (f (quote ("use_std"))) (d #t) (k 0)))) (h "0xpamv37fhzfz23qi9dk6hda8vjb3xvzj9ri9x2nmanqq4zqqhfa")))

(define-public crate-schemer-rusqlite-0.2.0 (c (n "schemer-rusqlite") (v "0.2.0") (d (list (d (n "rusqlite") (r "^0.24") (d #t) (k 0)) (d (n "schemer") (r "^0.2.0") (d #t) (k 0)) (d (n "uuid") (r "^1") (d #t) (k 0)))) (h "1msmpnnq9allkk9djbdq6biiah6k4ad57g82qq51yml0bnx86arz") (r "1.56")))

(define-public crate-schemer-rusqlite-0.2.1 (c (n "schemer-rusqlite") (v "0.2.1") (d (list (d (n "rusqlite") (r "^0.25") (d #t) (k 0)) (d (n "schemer") (r "^0.2.0") (d #t) (k 0)) (d (n "uuid") (r "^1") (d #t) (k 0)))) (h "09sz3mzs4as3dy5qvww9xk5fdxkcfmlbircga6w3crcpvnr4mwy1") (r "1.56")))

(define-public crate-schemer-rusqlite-0.2.2 (c (n "schemer-rusqlite") (v "0.2.2") (d (list (d (n "rusqlite") (r "^0.29.0") (d #t) (k 0)) (d (n "schemer") (r "^0.2.1") (d #t) (k 0)) (d (n "uuid") (r "^1") (d #t) (k 0)))) (h "1cdk70izxh99xa84k9zwxz8dhyjf8sfi9qqqlv3f4n1cllgsrd8g") (r "1.59")))

