(define-module (crates-io sc he scheduled) #:use-module (crates-io))

(define-public crate-scheduled-0.1.0 (c (n "scheduled") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.38") (d #t) (k 0)) (d (n "queued_rust") (r "^0.3.5") (d #t) (k 0)))) (h "0jchs2vk7vq8s4l40n7ldb17q7rk4ichynff49x15rfak7brxjj5") (y #t)))

(define-public crate-scheduled-0.2.0 (c (n "scheduled") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.38") (d #t) (k 0)) (d (n "queued_rust") (r "^0.3.5") (d #t) (k 0)))) (h "0afy8w861sqcqqfpp19rxh8300r6m2kbdl8b2n56j7x0rfms4zxj") (y #t)))

(define-public crate-scheduled-0.3.1 (c (n "scheduled") (v "0.3.1") (d (list (d (n "chrono") (r "^0.4.38") (d #t) (k 0)) (d (n "queued_rust") (r "^0.5.3") (d #t) (k 0)))) (h "07ww8ksc0afk1ldbpqs33p24n4w0lgmfy1ndbdlm13452nn13bqw") (y #t)))

(define-public crate-scheduled-1.0.0 (c (n "scheduled") (v "1.0.0") (d (list (d (n "chrono") (r "^0.4.38") (d #t) (k 0)) (d (n "queued_rust") (r "^0.7.4") (d #t) (k 0)))) (h "1836bq2v6hvj6ixxyl5bnfl42n54qdrv8hcvkk928psmnvn6nxv9")))

(define-public crate-scheduled-1.0.1 (c (n "scheduled") (v "1.0.1") (d (list (d (n "chrono") (r "^0.4.38") (d #t) (k 0)) (d (n "queued_rust") (r "^0.7.4") (d #t) (k 0)))) (h "1wld3lszqllmwgrkrvvb43k74mxplw5g0xjcr659ycjr6yz7fhm7")))

(define-public crate-scheduled-1.0.2 (c (n "scheduled") (v "1.0.2") (d (list (d (n "chrono") (r "^0.4.38") (d #t) (k 0)) (d (n "queued_rust") (r "^0.7.4") (d #t) (k 0)))) (h "1m6z3n93fn2s6v4ckp9lgfzbdn87nhz80c6nbhwgx246i3ih4rl7")))

