(define-module (crates-io sc he schema2000) #:use-module (crates-io))

(define-public crate-schema2000-0.1.0 (c (n "schema2000") (v "0.1.0") (d (list (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "0h2ybdgix58zmyc7rvnp24pf8m727r8b20y9gy4x3gpac8r0b7q0")))

