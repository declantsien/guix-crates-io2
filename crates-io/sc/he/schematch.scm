(define-module (crates-io sc he schematch) #:use-module (crates-io))

(define-public crate-schematch-1.0.0 (c (n "schematch") (v "1.0.0") (d (list (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0y9k9jhmmsqcssyhvqbsm0p70wnx89dzn7k6jpbrzxds83p3y8ha")))

