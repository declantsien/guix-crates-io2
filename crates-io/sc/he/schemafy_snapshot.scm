(define-module (crates-io sc he schemafy_snapshot) #:use-module (crates-io))

(define-public crate-schemafy_snapshot-0.1.0 (c (n "schemafy_snapshot") (v "0.1.0") (d (list (d (n "Inflector") (r "^0.6.0") (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_derive") (r "^0.8") (d #t) (k 0)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)))) (h "1vdpa4ms3n47ild3fpwghrksmn8my4gm7nnrq2x7h19202fzf9mf")))

(define-public crate-schemafy_snapshot-0.2.0 (c (n "schemafy_snapshot") (v "0.2.0") (d (list (d (n "Inflector") (r "^0.6.0") (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (d #t) (k 0)))) (h "1xsrqh35l9p3dxb67fpq2b572d662fh7g5achdgwj904rml9pc5q")))

(define-public crate-schemafy_snapshot-0.1.1 (c (n "schemafy_snapshot") (v "0.1.1") (d (list (d (n "Inflector") (r "^0.6.0") (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^0.8.0") (d #t) (k 0)) (d (n "serde_derive") (r "^0.8.0") (d #t) (k 0)) (d (n "serde_json") (r "^0.8.0") (d #t) (k 0)))) (h "08d6xffwhsga138ifmps5i45y8sq2iq36ydgpqy2bfzy89zbjhd3")))

(define-public crate-schemafy_snapshot-0.2.2 (c (n "schemafy_snapshot") (v "0.2.2") (d (list (d (n "Inflector") (r "^0.6.0") (d #t) (k 0)) (d (n "itertools") (r "^0.5.9") (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (d #t) (k 0)))) (h "17c4cqy9jqz98bgvjjzvm8vnkna7zzsz9xnq163knsmw5fk3m68m")))

(define-public crate-schemafy_snapshot-0.3.0 (c (n "schemafy_snapshot") (v "0.3.0") (d (list (d (n "Inflector") (r "^0.6.0") (d #t) (k 0)) (d (n "itertools") (r "^0.5.9") (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1k8ra29ggjvmh1hwzbaw0c6rmhn21acndmjslv2fwiswzj0ms09c")))

(define-public crate-schemafy_snapshot-0.4.0 (c (n "schemafy_snapshot") (v "0.4.0") (d (list (d (n "Inflector") (r "^0.6.0") (d #t) (k 0)) (d (n "itertools") (r "^0.5.9") (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "057s4bv1p1s7ws2hzq24xsf1zcaagsykjw367s7p24wjl1hyr3m8")))

(define-public crate-schemafy_snapshot-0.4.1 (c (n "schemafy_snapshot") (v "0.4.1") (d (list (d (n "Inflector") (r "^0.6.0") (d #t) (k 0)) (d (n "itertools") (r "^0.5.9") (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1a53gim6xf31rbqri71j7rvzbqca9j5bfm9ppy422iz8n1dawhcc")))

(define-public crate-schemafy_snapshot-0.4.2 (c (n "schemafy_snapshot") (v "0.4.2") (d (list (d (n "Inflector") (r "^0.11") (d #t) (k 0)) (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1nfq68lxjg3kvra6w7b02zgmn5syyg1y2yixh9gk22lmdsz5xwdf")))

