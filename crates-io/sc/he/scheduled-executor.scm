(define-module (crates-io sc he scheduled-executor) #:use-module (crates-io))

(define-public crate-scheduled-executor-0.2.0 (c (n "scheduled-executor") (v "0.2.0") (d (list (d (n "futures") (r "^0.1.14") (d #t) (k 0)) (d (n "futures-cpupool") (r "^0.1.5") (d #t) (k 0)) (d (n "log") (r "^0.3.0") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1.6") (d #t) (k 0)))) (h "01zp8bra5gk4rbfbdsbx6bzkb6cvyvahch74444zn3ra37brbmsw")))

(define-public crate-scheduled-executor-0.2.1 (c (n "scheduled-executor") (v "0.2.1") (d (list (d (n "futures") (r "^0.1.14") (d #t) (k 0)) (d (n "futures-cpupool") (r "^0.1.5") (d #t) (k 0)) (d (n "log") (r "^0.3.0") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1.6") (d #t) (k 0)))) (h "1x5pyjna8ylcpbkfkx0hmshyqmwad39h1b4d8ac8lwd6wrlg0pvx")))

(define-public crate-scheduled-executor-0.3.0 (c (n "scheduled-executor") (v "0.3.0") (d (list (d (n "futures") (r "^0.1.14") (d #t) (k 0)) (d (n "futures-cpupool") (r "^0.1.5") (d #t) (k 0)) (d (n "log") (r "^0.3.0") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "tokio-core") (r "^0.1.6") (d #t) (k 0)))) (h "00f9jbjgvyxps8j3xvdw78zmi9xl8x1rvvfjjzsi3d6hywzqal4y")))

(define-public crate-scheduled-executor-0.4.0 (c (n "scheduled-executor") (v "0.4.0") (d (list (d (n "futures") (r "^0.1.14") (d #t) (k 0)) (d (n "futures-cpupool") (r "^0.1.5") (d #t) (k 0)) (d (n "log") (r "^0.3.0") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "tokio-core") (r "^0.1.6") (d #t) (k 0)))) (h "16qv81alasq53my2kgf5l25kg2qlq99qv8x52gb2dspg8hh9rm4b")))

