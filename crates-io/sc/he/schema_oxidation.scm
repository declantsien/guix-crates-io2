(define-module (crates-io sc he schema_oxidation) #:use-module (crates-io))

(define-public crate-schema_oxidation-0.1.0 (c (n "schema_oxidation") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive" "proc-macro" "parsing"))) (d #t) (k 0)))) (h "1jpg2181lfyfdzn6fvhna6rhx11zb9x4l1rzl2pd0s7bkjyiy6fn")))

(define-public crate-schema_oxidation-0.1.1 (c (n "schema_oxidation") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive" "proc-macro" "parsing"))) (d #t) (k 0)))) (h "1y7nl7i67j4wbnn1qssqr2w0mld7k3krqpya33zlas0dnkb2hkqs")))

