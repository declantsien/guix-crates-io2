(define-module (crates-io sc he schemajen) #:use-module (crates-io))

(define-public crate-schemajen-0.1.0 (c (n "schemajen") (v "0.1.0") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.63") (d #t) (t "cfg(all(target_arch = \"wasm32\", target_vendor = \"unknown\", target_os = \"unknown\", target_env = \"\"))") (k 0)))) (h "1hn3q3l8zmcinq9941wzg605w5zjyzyf1vxb1gq2zg96z74b90d4")))

(define-public crate-schemajen-0.2.0 (c (n "schemajen") (v "0.2.0") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.63") (d #t) (t "cfg(all(target_arch = \"wasm32\", target_vendor = \"unknown\", target_os = \"unknown\", target_env = \"\"))") (k 0)))) (h "16acx99s4di0sq0xg5cvzm8ars3xmffwp2ncingkcbnm4mnj2sqy")))

