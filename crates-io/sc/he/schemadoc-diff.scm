(define-module (crates-io sc he schemadoc-diff) #:use-module (crates-io))

(define-public crate-schemadoc-diff-0.1.20 (c (n "schemadoc-diff") (v "0.1.20") (d (list (d (n "auto_impl") (r "^1.0.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "indexmap") (r "^1.8.1") (f (quote ("std" "serde"))) (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "schemadoc-diff-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_path_to_error") (r "^0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1lqh0rw5zrabaalqks0cy0bl66bh47pqbxm9c32ijr7xx3m3cpgq")))

