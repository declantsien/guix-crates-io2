(define-module (crates-io sc he schema) #:use-module (crates-io))

(define-public crate-schema-0.0.0 (c (n "schema") (v "0.0.0") (h "1r1lcg14ms2xw3lj8szwjrqbk75kl5ymikzfrgidrha3xpr0fwfp")))

(define-public crate-schema-0.0.1 (c (n "schema") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "schema-derive") (r "^0.0.1") (d #t) (k 0)) (d (n "syn") (r "^1.0.64") (f (quote ("derive"))) (d #t) (k 0)))) (h "0hyvl0rdmm3cq53n6cmiyb8a7cxwkmvi1bppc1rhnarzswzln9lw")))

(define-public crate-schema-0.1.0 (c (n "schema") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "schema-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.2") (d #t) (k 0)))) (h "0b4swckm58hg2a0qhz7awmck27qxw35lp8h5l0ccdy7vl8ij0hln")))

