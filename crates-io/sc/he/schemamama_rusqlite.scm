(define-module (crates-io sc he schemamama_rusqlite) #:use-module (crates-io))

(define-public crate-schemamama_rusqlite-0.1.0 (c (n "schemamama_rusqlite") (v "0.1.0") (d (list (d (n "env_logger") (r "*") (d #t) (k 2)) (d (n "log") (r "*") (d #t) (k 0)) (d (n "rusqlite") (r "^0.2.0") (d #t) (k 0)) (d (n "schemamama") (r "^0.0.8") (d #t) (k 0)))) (h "0lr0hzskj2gkji2v2rfxi5vfa8jllvs456nsp6lwk8b0bnbkqb6p") (f (quote (("unstable"))))))

(define-public crate-schemamama_rusqlite-0.1.1 (c (n "schemamama_rusqlite") (v "0.1.1") (d (list (d (n "env_logger") (r "*") (d #t) (k 2)) (d (n "log") (r "*") (d #t) (k 0)) (d (n "rusqlite") (r "^0.2.0") (d #t) (k 0)) (d (n "schemamama") (r "^0.0.8") (d #t) (k 0)))) (h "1hwxrsx9zxgagka1jdkbah8cf82zrkqqcvxg9786agd12vj4gx66") (f (quote (("unstable"))))))

(define-public crate-schemamama_rusqlite-0.1.2 (c (n "schemamama_rusqlite") (v "0.1.2") (d (list (d (n "env_logger") (r "*") (d #t) (k 2)) (d (n "log") (r "*") (d #t) (k 0)) (d (n "rusqlite") (r "^0.2.0") (d #t) (k 0)) (d (n "schemamama") (r "^0.0.8") (d #t) (k 0)))) (h "0073c0ckq3g4h2g3vy63m0ihpnh3sigbmh7phlmjxxn03wkbgh8k") (f (quote (("unstable"))))))

(define-public crate-schemamama_rusqlite-0.2.0 (c (n "schemamama_rusqlite") (v "0.2.0") (d (list (d (n "env_logger") (r "~0.3.0") (d #t) (k 2)) (d (n "log") (r "~0.3.0") (d #t) (k 0)) (d (n "rusqlite") (r "^0.6.0") (d #t) (k 0)) (d (n "schemamama") (r "^0.0.9") (d #t) (k 0)))) (h "1vp9hci0pq5iix9nnqxrrg5s7fx9fiaknwi9prlpmfwmf0mjwahl") (f (quote (("unstable"))))))

(define-public crate-schemamama_rusqlite-0.3.0 (c (n "schemamama_rusqlite") (v "0.3.0") (d (list (d (n "env_logger") (r "~0.3.0") (d #t) (k 2)) (d (n "log") (r "~0.3.0") (d #t) (k 0)) (d (n "rusqlite") (r "^0.6.0") (d #t) (k 0)) (d (n "schemamama") (r "^0.0.11") (d #t) (k 0)))) (h "0fwi5z2w6p7v8w8wk9z2vplnz632k6knzqmn9hvmngqflqzykab7") (f (quote (("unstable"))))))

(define-public crate-schemamama_rusqlite-0.4.0 (c (n "schemamama_rusqlite") (v "0.4.0") (d (list (d (n "env_logger") (r "~0.3.0") (d #t) (k 2)) (d (n "log") (r "~0.3.0") (d #t) (k 0)) (d (n "rusqlite") (r "^0.6.0") (d #t) (k 0)) (d (n "schemamama") (r "^0.1.0") (d #t) (k 0)))) (h "1y5sjshfzm6nakwpvfc8krdfn9g4v8krqix5pb7mlsdz64y1fq0a") (f (quote (("unstable"))))))

(define-public crate-schemamama_rusqlite-0.5.0 (c (n "schemamama_rusqlite") (v "0.5.0") (d (list (d (n "env_logger") (r "~0.3.0") (d #t) (k 2)) (d (n "log") (r "~0.3.0") (d #t) (k 0)) (d (n "rusqlite") (r "^0.6.0") (d #t) (k 0)) (d (n "schemamama") (r "^0.2.0") (d #t) (k 0)))) (h "1c518k166qi78fv94i037lpyfqc8rfd2296ngi3fq87y41a2wrjw") (f (quote (("unstable"))))))

(define-public crate-schemamama_rusqlite-0.7.0 (c (n "schemamama_rusqlite") (v "0.7.0") (d (list (d (n "env_logger") (r "~0.3.0") (d #t) (k 2)) (d (n "log") (r "~0.3.0") (d #t) (k 0)) (d (n "rusqlite") (r "^0.20.0") (d #t) (k 0)) (d (n "schemamama") (r "^0.3.0") (d #t) (k 0)))) (h "1ibryggr5ry065rx54h62ikyyxal5iqmp582bmfcdmab3nayw3x3") (f (quote (("unstable"))))))

(define-public crate-schemamama_rusqlite-0.8.0 (c (n "schemamama_rusqlite") (v "0.8.0") (d (list (d (n "env_logger") (r "~0.3.0") (d #t) (k 2)) (d (n "log") (r "^0.4.13") (d #t) (k 0)) (d (n "rusqlite") (r "^0.24.2") (d #t) (k 0)) (d (n "schemamama") (r "^0.3.0") (d #t) (k 0)))) (h "1l0dgvwzhn8ggdcpwphx9cni14k8knhl9ah51sv56ar0kmpzkxfi") (f (quote (("unstable"))))))

(define-public crate-schemamama_rusqlite-0.9.0 (c (n "schemamama_rusqlite") (v "0.9.0") (d (list (d (n "env_logger") (r "~0.3.0") (d #t) (k 2)) (d (n "log") (r "^0.4.13") (d #t) (k 0)) (d (n "rusqlite") (r "^0.25.3") (d #t) (k 0)) (d (n "schemamama") (r "^0.3.0") (d #t) (k 0)))) (h "08gk969g5ap5a0f6wjqpqgx5ypyb60xq3ngzw7hp3xlvclfn62pg") (f (quote (("unstable"))))))

(define-public crate-schemamama_rusqlite-0.9.1 (c (n "schemamama_rusqlite") (v "0.9.1") (d (list (d (n "env_logger") (r "~0.3.0") (d #t) (k 2)) (d (n "log") (r "^0.4.13") (d #t) (k 0)) (d (n "rusqlite") (r "^0.25.3") (d #t) (k 0)) (d (n "schemamama") (r "^0.3.0") (d #t) (k 0)))) (h "1lhvbnw29ynva9m9j8cpwd8591ds8picr6mw7mkrjhykjk0nmvpc") (f (quote (("unstable"))))))

(define-public crate-schemamama_rusqlite-0.10.0 (c (n "schemamama_rusqlite") (v "0.10.0") (d (list (d (n "env_logger") (r "~0.3.0") (d #t) (k 2)) (d (n "log") (r "^0.4.13") (d #t) (k 0)) (d (n "rusqlite") (r "^0.25.3") (d #t) (k 0)) (d (n "schemamama") (r "^0.3.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)))) (h "0x76bxwvwfkfkhiv0g114v889l8jlp8wgyxyvdsj3jgka662qlfl") (f (quote (("unstable"))))))

