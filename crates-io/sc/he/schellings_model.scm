(define-module (crates-io sc he schellings_model) #:use-module (crates-io))

(define-public crate-schellings_model-1.2.1 (c (n "schellings_model") (v "1.2.1") (d (list (d (n "open") (r "^4.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "sdl2") (r "^0.35.2") (f (quote ("ttf"))) (d #t) (k 0)))) (h "146idwvh3j3zz3hpysshxdnrl15az157d852lsx705vs53765av9")))

(define-public crate-schellings_model-1.3.0 (c (n "schellings_model") (v "1.3.0") (d (list (d (n "open") (r "^4.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "sdl2") (r "^0.35.2") (f (quote ("ttf"))) (d #t) (k 0)))) (h "1h1v28h60nd6gh6x55l9567a93j1h91jxrhas5zyjrwxbcxrjy11")))

(define-public crate-schellings_model-1.3.1 (c (n "schellings_model") (v "1.3.1") (d (list (d (n "open") (r "^4.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "sdl2") (r "^0.35.2") (f (quote ("ttf"))) (d #t) (k 0)))) (h "1d8gc4qgkvgrjwnq7m13mdk95kvd17igfzik6j8947rvsw9x98b4")))

(define-public crate-schellings_model-1.3.2 (c (n "schellings_model") (v "1.3.2") (d (list (d (n "open") (r "^4.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "sdl2") (r "^0.35.2") (f (quote ("ttf"))) (d #t) (k 0)))) (h "1ynjzp44s66w3hqk9aicr5ahgrg967xwryix0fmzz6al4xvbz22r")))

(define-public crate-schellings_model-1.4.0 (c (n "schellings_model") (v "1.4.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "sdl2") (r "^0.35.2") (f (quote ("ttf"))) (d #t) (k 0)))) (h "1x0rdahkgv4azrhra5klbqlnn856mpm2s2wjd2296nkapgrjlyx4")))

