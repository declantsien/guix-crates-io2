(define-module (crates-io sc he scheduled-thread-pool) #:use-module (crates-io))

(define-public crate-scheduled-thread-pool-0.1.0 (c (n "scheduled-thread-pool") (v "0.1.0") (d (list (d (n "antidote") (r "^1.0") (d #t) (k 0)))) (h "0kv4z406lpyq5jvbp2ws44wysq7j7faqak2ldyvl68ykx94bx7rd")))

(define-public crate-scheduled-thread-pool-0.2.0 (c (n "scheduled-thread-pool") (v "0.2.0") (d (list (d (n "antidote") (r "^1.0") (d #t) (k 0)))) (h "129qkp0crc03my7n1nn79i2nwrww4x0n8v402zl9p0i3abyg6bqs")))

(define-public crate-scheduled-thread-pool-0.2.1 (c (n "scheduled-thread-pool") (v "0.2.1") (d (list (d (n "parking_lot") (r "^0.8") (d #t) (k 0)))) (h "075fcqvqji0x2xqi433p4vihdahkbd3qs6gvmsjddq27dnrzrv5v")))

(define-public crate-scheduled-thread-pool-0.2.2 (c (n "scheduled-thread-pool") (v "0.2.2") (d (list (d (n "parking_lot") (r "^0.9") (d #t) (k 0)))) (h "1czv8r2vg2b2srh62cavfv6fgf9j5zqq7cpnymvw1xhz10p781xx")))

(define-public crate-scheduled-thread-pool-0.2.3 (c (n "scheduled-thread-pool") (v "0.2.3") (d (list (d (n "parking_lot") (r "^0.10") (d #t) (k 0)))) (h "0yfp5c5cf4imhmrg55fc2kf5y3v120yvzqgmip1fdy183z1ppppm")))

(define-public crate-scheduled-thread-pool-0.2.4 (c (n "scheduled-thread-pool") (v "0.2.4") (d (list (d (n "parking_lot") (r "^0.10") (d #t) (k 0)))) (h "1xpvb1s7zzyml92vrijbmfc3wprlmglg3813b77mypldz3yxg209")))

(define-public crate-scheduled-thread-pool-0.2.5 (c (n "scheduled-thread-pool") (v "0.2.5") (d (list (d (n "parking_lot") (r "^0.11") (d #t) (k 0)))) (h "1mz7s21q1d7xn9j15dlhhv1y86q2r2z6hpax5nh3y1q42byp8vyw")))

(define-public crate-scheduled-thread-pool-0.2.6 (c (n "scheduled-thread-pool") (v "0.2.6") (d (list (d (n "parking_lot") (r "^0.12") (d #t) (k 0)))) (h "1gzr9dpp75p431zlb1hnsxdjja8kmn07xl1ghi7s8hzipwcpaylp")))

(define-public crate-scheduled-thread-pool-0.2.7 (c (n "scheduled-thread-pool") (v "0.2.7") (d (list (d (n "parking_lot") (r "^0.12") (d #t) (k 0)))) (h "068s77f9xcpvzl70nsxk8750dzzc6f9pixajhd979815cj0ndg1w")))

