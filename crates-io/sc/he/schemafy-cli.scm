(define-module (crates-io sc he schemafy-cli) #:use-module (crates-io))

(define-public crate-schemafy-cli-0.1.0 (c (n "schemafy-cli") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.33") (d #t) (k 0)) (d (n "env_logger") (r "^0.8.1") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "schemafy_lib") (r "^0.5.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 0)))) (h "0bb552y74g0xn8wfx17drf7jimz6kyay7fswyp5kmjrnyn0372l1")))

