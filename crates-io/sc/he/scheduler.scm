(define-module (crates-io sc he scheduler) #:use-module (crates-io))

(define-public crate-scheduler-0.0.1 (c (n "scheduler") (v "0.0.1") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "0nsmhld0l88hlvv4f7768vfv0pybkbh2gbqpwyja6rnrbjx6li9x")))

(define-public crate-scheduler-0.0.2 (c (n "scheduler") (v "0.0.2") (d (list (d (n "errno") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "17gfb3hvc31ysg5zlqc7mc93x1k8y1xlss57x58pjl1w7fk355j4")))

(define-public crate-scheduler-0.0.3 (c (n "scheduler") (v "0.0.3") (d (list (d (n "errno") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "17m9wqs3n0fmf4qpr8g1pbkr4yda7n1lpl078sls2bsqdn5v473a")))

(define-public crate-scheduler-0.1.0 (c (n "scheduler") (v "0.1.0") (d (list (d (n "errno") (r "^0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1sj5hxd4cvx4m9dfx663p5bnwnrnwwrlzx9y36n152mfrp4yi5bh")))

(define-public crate-scheduler-0.1.1 (c (n "scheduler") (v "0.1.1") (d (list (d (n "errno") (r "^0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1hyns50j5j1j246brqfnkk792ivjr4l8zaq2zj56llq7vhkgp1cy")))

(define-public crate-scheduler-0.1.2 (c (n "scheduler") (v "0.1.2") (d (list (d (n "errno") (r "^0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "04i7fjyazdfzf534z6i02bc4lpkynd8rmfbniq8xvrfhvjsc10hq")))

(define-public crate-scheduler-0.1.3 (c (n "scheduler") (v "0.1.3") (d (list (d (n "errno") (r "^0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0f106yf7npla2k4x25wn26s1zy0z44mv1bxz1bm2shxh983kz93g")))

