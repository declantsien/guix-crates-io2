(define-module (crates-io sc he schema2000-cli) #:use-module (crates-io))

(define-public crate-schema2000-cli-0.1.0 (c (n "schema2000-cli") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "schema2000") (r "^0.1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "0930wsanp1j5y7nasv1cyg3aqyd6965y2kww643kx685iay9kk63")))

