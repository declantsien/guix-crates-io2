(define-module (crates-io sc he sched-clock) #:use-module (crates-io))

(define-public crate-sched-clock-0.1.0 (c (n "sched-clock") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "quickcheck") (r "^0.8") (k 2)) (d (n "quickcheck_macros") (r "^0.8") (d #t) (k 2)))) (h "0387apr7zvvcq5lbwcx1hj67n8w43fw1abqnda6zm6xavxfylwbz")))

(define-public crate-sched-clock-0.1.1 (c (n "sched-clock") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "quickcheck") (r "^0.8") (k 2)) (d (n "quickcheck_macros") (r "^0.8") (d #t) (k 2)))) (h "0ms4gcw9s1fc6kryvmj489kqijyfsicakr74vffbs8lh6sfvph7x")))

(define-public crate-sched-clock-0.1.2 (c (n "sched-clock") (v "0.1.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "quickcheck") (r "^0.8") (k 2)) (d (n "quickcheck_macros") (r "^0.8") (d #t) (k 2)))) (h "0ni6pw8jg9rmxbqxbr89p8a4l93wdjhfzw2wvxwswnyd7d502937")))

