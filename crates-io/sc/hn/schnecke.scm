(define-module (crates-io sc hn schnecke) #:use-module (crates-io))

(define-public crate-schnecke-0.1.0 (c (n "schnecke") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "hyper") (r "^0.13") (d #t) (k 0)) (d (n "hyper-tls") (r "^0.4.1") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)) (d (n "url") (r "^2.1.1") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "07x2mwc4wfpafgckrvq05k9nyhraxixb9qxwnxjj8iyz43jzz0wq")))

