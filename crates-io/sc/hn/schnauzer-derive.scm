(define-module (crates-io sc hn schnauzer-derive) #:use-module (crates-io))

(define-public crate-schnauzer-derive-0.1.0 (c (n "schnauzer-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "07rf3hrhasacgj9n6qs4phvf81ri7d3iggwin9wd753qfwhszd55")))

