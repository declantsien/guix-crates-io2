(define-module (crates-io sc hn schnauzer) #:use-module (crates-io))

(define-public crate-schnauzer-0.1.0 (c (n "schnauzer") (v "0.1.0") (d (list (d (n "scroll") (r "^0.11.0") (d #t) (k 0)))) (h "1dv2d47nrf4c25qzb2ddqvb9ambw8fjz96lnqzh1jn429ib6ivkk")))

(define-public crate-schnauzer-0.1.1 (c (n "schnauzer") (v "0.1.1") (d (list (d (n "scroll") (r "^0.11.0") (d #t) (k 0)))) (h "08wfnxn71w7vbsarqgz7phzwm8srpsn4br5zlsz0a10aqmzlqwix")))

(define-public crate-schnauzer-0.1.2 (c (n "schnauzer") (v "0.1.2") (d (list (d (n "scroll") (r "^0.11.0") (d #t) (k 0)))) (h "1j4v5xv1mmjir5palxyvwyk9r8qs30xzpaimxgag355ygnj0dm2n")))

(define-public crate-schnauzer-0.1.3 (c (n "schnauzer") (v "0.1.3") (d (list (d (n "scroll") (r "^0.11.0") (d #t) (k 0)))) (h "1n1kkybbnlfhzxdw499sdl3s6rqlcn3ag4sr2dll56d6iq5dc7ia")))

(define-public crate-schnauzer-0.1.4 (c (n "schnauzer") (v "0.1.4") (d (list (d (n "scroll") (r "^0.11.0") (d #t) (k 0)))) (h "1bkqzj4fkkrrn8i3x71dw04j7c3bc1xpsmcyq32j9s1kx5rl68ag")))

(define-public crate-schnauzer-0.1.5 (c (n "schnauzer") (v "0.1.5") (d (list (d (n "scroll") (r "^0.11.0") (d #t) (k 0)))) (h "0ikdpgfchjb7j373pcckgvaax3i724ys3skbppnjib4srk17v7bc")))

(define-public crate-schnauzer-0.1.6 (c (n "schnauzer") (v "0.1.6") (d (list (d (n "scroll") (r "^0.11.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uuid") (r "^1.3.1") (d #t) (k 0)))) (h "18nj7xncjw42haw27k95qcwl9hyxwz96kysbfk9hy6ybi1ssmdg2")))

(define-public crate-schnauzer-0.1.7 (c (n "schnauzer") (v "0.1.7") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "schnauzer-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "scroll") (r "^0.11.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uuid") (r "^1.3.1") (d #t) (k 0)))) (h "09qa9l35m4xyy3c9af485bxadr69ap8lhc1vs8y94q76ih53dakh")))

(define-public crate-schnauzer-0.1.8 (c (n "schnauzer") (v "0.1.8") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "schnauzer-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "scroll") (r "^0.11.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uuid") (r "^1.3.1") (d #t) (k 0)))) (h "1yn158i5vmvdhwfggrc4f28rkqs1hd8z6gdzcn5acmgzwy45g79q")))

(define-public crate-schnauzer-0.1.9 (c (n "schnauzer") (v "0.1.9") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "schnauzer-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "scroll") (r "^0.11.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uuid") (r "^1.3.1") (d #t) (k 0)))) (h "1r2q98biwqlbfpksmqwl4yx1054rrx2vlc3x4c1idclrv5hzsyzd")))

(define-public crate-schnauzer-0.2.0 (c (n "schnauzer") (v "0.2.0") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "schnauzer-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "scroll") (r "^0.11.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uuid") (r "^1.3.1") (d #t) (k 0)))) (h "0gv3aqlsx30a418nf5v10wlm1jfcbkwqc16bya1mbanp6cx0wj64")))

(define-public crate-schnauzer-0.2.1 (c (n "schnauzer") (v "0.2.1") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "schnauzer-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "scroll") (r "^0.11.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uuid") (r "^1.3.1") (d #t) (k 0)))) (h "0ydj9jdsgll2lhn29423bapmkkzfmr57y9d6kvg201y8zk7ihjw9")))

(define-public crate-schnauzer-0.2.2 (c (n "schnauzer") (v "0.2.2") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "schnauzer-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "scroll") (r "^0.11.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uuid") (r "^1.3.1") (d #t) (k 0)))) (h "082wr0g0naxn3yh0s46wpnr94vxf2y5n095wj5n5nnx29nkld2rl")))

(define-public crate-schnauzer-0.2.3 (c (n "schnauzer") (v "0.2.3") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "schnauzer-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "scroll") (r "^0.11.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uuid") (r "^1.3.1") (d #t) (k 0)))) (h "03bj4rj7zhkql772g2a5q226b2k25vcpx0hc1pla0hj62ay9skza")))

(define-public crate-schnauzer-0.2.4 (c (n "schnauzer") (v "0.2.4") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "schnauzer-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "scroll") (r "^0.11.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uuid") (r "^1.3.1") (d #t) (k 0)))) (h "1adzi9n1b3ff7p33klzjbb7m856ncai8i48m0agp1i66n5xmdm31") (y #t)))

(define-public crate-schnauzer-0.2.5 (c (n "schnauzer") (v "0.2.5") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "schnauzer-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "scroll") (r "^0.11.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uuid") (r "^1.3.1") (d #t) (k 0)))) (h "0pznfn2a4w9p0khhzyyf9vrcvvvfavgf9zmm0z3biczgp4picdv8")))

(define-public crate-schnauzer-0.2.6 (c (n "schnauzer") (v "0.2.6") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "schnauzer-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "scroll") (r "^0.11.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uuid") (r "^1.3.1") (d #t) (k 0)))) (h "124q1dldlmnj4v3hxkh8ay43hp9slfqkz5vc4vjdwg5r40ya8bqv")))

(define-public crate-schnauzer-0.2.7 (c (n "schnauzer") (v "0.2.7") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "schnauzer-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "scroll") (r "^0.11.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uuid") (r "^1.3.1") (d #t) (k 0)))) (h "0a1m60x45w8wc1m49zmcxp6447lgl719zai99rsijlf56af7xhla")))

(define-public crate-schnauzer-0.2.8 (c (n "schnauzer") (v "0.2.8") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "schnauzer-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "scroll") (r "^0.11.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uuid") (r "^1.3.1") (d #t) (k 0)))) (h "0w1wg8ib3pcw5nr59apymqs4m9fip3071pc715il524x76i4z1dr")))

(define-public crate-schnauzer-0.2.9 (c (n "schnauzer") (v "0.2.9") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "schnauzer-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "scroll") (r "^0.11.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uuid") (r "^1.3.1") (d #t) (k 0)))) (h "0v5mhqgipx1gp0ahnf3h7ckf7l6gpl400mg79ywv1604hnn8qldy")))

(define-public crate-schnauzer-0.3.0 (c (n "schnauzer") (v "0.3.0") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "schnauzer-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "scroll") (r "^0.11.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uuid") (r "^1.3.1") (d #t) (k 0)))) (h "1rdapj59ziwdg8idvg66lb83rv13y4nyv5wfq1fzv5nhjs58dvfi")))

(define-public crate-schnauzer-0.3.1 (c (n "schnauzer") (v "0.3.1") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "kex") (r "^0.1.5") (d #t) (k 0)) (d (n "schnauzer-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "scroll") (r "^0.11.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uuid") (r "^1.3.1") (d #t) (k 0)))) (h "0w6k488d00063qd4llsnqhsnrkcbf0syz0zfw0sdm0lh3hkyg9bf")))

(define-public crate-schnauzer-0.3.2 (c (n "schnauzer") (v "0.3.2") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "kex") (r "^0.1.6") (d #t) (k 0)) (d (n "schnauzer-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "scroll") (r "^0.11.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uuid") (r "^1.3.1") (d #t) (k 0)))) (h "08v48v6kj2ygyh86gg5k4anply5p7kzddi232grrv9gbjwylwmxy")))

(define-public crate-schnauzer-0.3.3 (c (n "schnauzer") (v "0.3.3") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "kex") (r "^0.1.8") (d #t) (k 0)) (d (n "schnauzer-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "scroll") (r "^0.11.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uuid") (r "^1.3.1") (d #t) (k 0)))) (h "0kzmjd10ijkxvg1186l2riwalc2nhx6r0abs6znavcdhjzl6qzvb")))

(define-public crate-schnauzer-0.3.4 (c (n "schnauzer") (v "0.3.4") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "kex") (r "^0.1.8") (d #t) (k 0)) (d (n "schnauzer-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "scroll") (r "^0.11.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uuid") (r "^1.3.1") (d #t) (k 0)))) (h "1nyapagkhlzbc2kx83vpgya2m6sn17hcgwcp5qzfvcjsdxazny02")))

(define-public crate-schnauzer-0.3.5 (c (n "schnauzer") (v "0.3.5") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "kex") (r "^0.2.6") (d #t) (k 0)) (d (n "schnauzer-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "scroll") (r "^0.11.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uuid") (r "^1.3.1") (d #t) (k 0)))) (h "1zvfcmndl7iyifrgqx42ssqcjqn6sal2m1wdqhzq661brj3bv81z")))

