(define-module (crates-io sc ai scailist) #:use-module (crates-io))

(define-public crate-scailist-0.1.0 (c (n "scailist") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "09fv5hmrdx0zjkg91j3lkwjqilka75vagh6nzpva4hpb1w3k7lhh")))

(define-public crate-scailist-0.1.1 (c (n "scailist") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "0iflbj6l8s3dqc6j7pral15zcagk1f1xvmq83vydk9r70n9mgzy7")))

(define-public crate-scailist-0.1.3 (c (n "scailist") (v "0.1.3") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "1974hxp1b2hvda6c614dyf808gn1fpddhhhxr01dvsh6b352gp0i")))

(define-public crate-scailist-0.1.4 (c (n "scailist") (v "0.1.4") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "1llyjgaz1msmiryj3555b2pndznbwlswpc3w686z8f29c1y7dvzi")))

(define-public crate-scailist-0.2.0 (c (n "scailist") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "06qma2xllhbj6k122wa6j1gi4jn75g4aqpahvhhplzdks35hzy16")))

