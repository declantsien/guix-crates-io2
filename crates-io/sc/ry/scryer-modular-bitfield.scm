(define-module (crates-io sc ry scryer-modular-bitfield) #:use-module (crates-io))

(define-public crate-scryer-modular-bitfield-0.11.3 (c (n "scryer-modular-bitfield") (v "0.11.3") (d (list (d (n "bitfield") (r "^0.13") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "modular-bitfield-impl") (r "^0.11.2") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0njqgwgrgdfa1xl84vr2d9c8qa3y0nxq9ia257x264ri6flgwqcj")))

(define-public crate-scryer-modular-bitfield-0.11.4 (c (n "scryer-modular-bitfield") (v "0.11.4") (d (list (d (n "bitfield") (r "^0.13") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "scryer-modular-bitfield-impl") (r "^0.11.2") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1b3n3wmkqq2bg1scja795cn5750rp6267n254nh98b3dqmq6rwni")))

