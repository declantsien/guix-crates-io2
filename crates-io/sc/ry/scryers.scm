(define-module (crates-io sc ry scryers) #:use-module (crates-io))

(define-public crate-scryers-0.1.0 (c (n "scryers") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.27") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.199") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.116") (d #t) (k 0)))) (h "1jx7fa7jdv3a4m7kjn3bbyrsjhgj5b87p37xpcgwva22yvw9hjpw")))

(define-public crate-scryers-0.1.1 (c (n "scryers") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.27") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.199") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.116") (d #t) (k 0)))) (h "10i5akqibclwxbias0kcvacf4zqpk3haq5khvkwrc4l1vwxlk6dk")))

(define-public crate-scryers-0.1.2 (c (n "scryers") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.27") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.199") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.116") (d #t) (k 0)))) (h "14s51cv4h62rp93dqz8lf8265k2xq4w2d8gqa90b59ihlgnlh7ly")))

(define-public crate-scryers-0.1.3 (c (n "scryers") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.27") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.199") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.116") (d #t) (k 0)))) (h "1vgakcnknv2p50mbazp1j5ya8iig1zs8ihnpq8ay8wrlmnl7g8f7")))

(define-public crate-scryers-0.1.4 (c (n "scryers") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.27") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.199") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.116") (d #t) (k 0)))) (h "18ib5y8l9pwf7m57ckfc7i0jf7dlm8r0sqx100gjgq4nl2nkcyw0")))

