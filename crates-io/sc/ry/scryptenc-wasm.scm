(define-module (crates-io sc ry scryptenc-wasm) #:use-module (crates-io))

(define-public crate-scryptenc-wasm-0.1.0 (c (n "scryptenc-wasm") (v "0.1.0") (d (list (d (n "getrandom") (r "^0.2.12") (f (quote ("js"))) (d #t) (k 0)) (d (n "scryptenc") (r "^0.9.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.90") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.40") (d #t) (k 2)))) (h "05qckk2gysidpapmbnpad8gcciap150dg28qdc577k4hl7vkqs5c") (r "1.70.0")))

(define-public crate-scryptenc-wasm-0.1.1 (c (n "scryptenc-wasm") (v "0.1.1") (d (list (d (n "getrandom") (r "^0.2.12") (f (quote ("js"))) (d #t) (k 0)) (d (n "scryptenc") (r "^0.9.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.90") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.40") (d #t) (k 2)))) (h "0j5lsn54n5nw7b1b0xl6yls9wlf7y5xs51aj4v1ddfiq61qk6mv9") (r "1.70.0")))

(define-public crate-scryptenc-wasm-0.1.2 (c (n "scryptenc-wasm") (v "0.1.2") (d (list (d (n "getrandom") (r "^0.2.12") (f (quote ("js"))) (d #t) (k 0)) (d (n "scryptenc") (r "^0.9.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.90") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.40") (d #t) (k 2)))) (h "0gxjbkzws0drvsfnsvkd2q6fcph8vn7h8aya4sxfhi842ljzni25") (r "1.70.0")))

(define-public crate-scryptenc-wasm-0.2.0 (c (n "scryptenc-wasm") (v "0.2.0") (d (list (d (n "getrandom") (r "^0.2.12") (f (quote ("js"))) (d #t) (k 0)) (d (n "scryptenc") (r "^0.9.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.90") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.40") (d #t) (k 2)))) (h "0dfvzy3303f21kv2a02jdjsalx85f0mvj07pr2ilzsc6x8pzkj6z") (r "1.70.0")))

(define-public crate-scryptenc-wasm-0.2.1 (c (n "scryptenc-wasm") (v "0.2.1") (d (list (d (n "getrandom") (r "^0.2.12") (f (quote ("js"))) (d #t) (k 0)) (d (n "scryptenc") (r "^0.9.3") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.91") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.41") (d #t) (k 2)))) (h "0m626wikspbz1w63mvsm2gkp7b7zq8yzaq5df54vni0bdxwdv5s6") (r "1.74.0")))

(define-public crate-scryptenc-wasm-0.2.2 (c (n "scryptenc-wasm") (v "0.2.2") (d (list (d (n "getrandom") (r "^0.2.12") (f (quote ("js"))) (d #t) (k 0)) (d (n "scryptenc") (r "^0.9.3") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.91") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.41") (d #t) (k 2)))) (h "1ini0y00dwqc6d787glfrfchp5mmxwwyla422j09259nhmqcmkjv") (r "1.74.0")))

(define-public crate-scryptenc-wasm-0.2.3 (c (n "scryptenc-wasm") (v "0.2.3") (d (list (d (n "getrandom") (r "^0.2.12") (f (quote ("js"))) (d #t) (k 0)) (d (n "scryptenc") (r "^0.9.3") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.91") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.41") (d #t) (k 2)))) (h "18aijg4g0mnad248xrsc3k6r3yfrpzfzghlvpwwqwh56z496z574") (r "1.74.0")))

