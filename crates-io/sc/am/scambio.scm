(define-module (crates-io sc am scambio) #:use-module (crates-io))

(define-public crate-scambio-0.1.0 (c (n "scambio") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("rt-threaded" "sync"))) (d #t) (k 2)))) (h "0ih2a89kdxqnmslwnxx0jz8fiwg5qp0sssamfkpcjmmsln213gyh")))

(define-public crate-scambio-0.2.0 (c (n "scambio") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("rt-threaded" "sync"))) (d #t) (k 2)))) (h "00jw5y2fjmwy8vanvr8sl1hb8n5gsa6zi9sr5yyvm865hvmpa2ly")))

(define-public crate-scambio-0.2.1 (c (n "scambio") (v "0.2.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("rt-threaded" "sync"))) (d #t) (k 2)))) (h "1rrz8x7bwjny8jzrgpd2rqiynhfgmblj00x4yz2yrcvq3j55iv9x")))

