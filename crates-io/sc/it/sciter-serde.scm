(define-module (crates-io sc it sciter-serde) #:use-module (crates-io))

(define-public crate-sciter-serde-0.3.0 (c (n "sciter-serde") (v "0.3.0") (d (list (d (n "sciter-rs") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.10") (d #t) (k 2)) (d (n "serde_derive") (r "^1") (d #t) (k 2)))) (h "1blrzjfc9nsws64c54m7y4a6x6l2q714ldm55j9glac51462zd65")))

(define-public crate-sciter-serde-0.3.1 (c (n "sciter-serde") (v "0.3.1") (d (list (d (n "sciter-rs") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.10") (d #t) (k 2)) (d (n "serde_derive") (r "^1") (d #t) (k 2)))) (h "1y70c17b7f9zcagdm8bh09p381v5pnwj172p7gpr61p98q7fjjcf")))

(define-public crate-sciter-serde-0.3.2 (c (n "sciter-serde") (v "0.3.2") (d (list (d (n "sciter-rs") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.10") (d #t) (k 2)) (d (n "serde_derive") (r "^1") (d #t) (k 2)))) (h "02qadjrk6srcwhggajbi0f0jw0827iv90xh72ac0x1kxsyvcixqw")))

