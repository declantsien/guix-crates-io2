(define-module (crates-io sc gs scgsm) #:use-module (crates-io))

(define-public crate-scgsm-0.1.0 (c (n "scgsm") (v "0.1.0") (h "1ka338ymdcx3q0p1fyps0axdgzzf3iizd50k1k8cvvby849fpp85")))

(define-public crate-scgsm-0.1.1 (c (n "scgsm") (v "0.1.1") (h "1iw16dhh0rlm5gh92ab0gzmyfc2snn2w4jrpg6q6r7ns17vzk0l3")))

(define-public crate-scgsm-0.1.2 (c (n "scgsm") (v "0.1.2") (h "0c1y9isvirig2v27fk9psj2zc354a3yrarqhmk2s6k5v7ym8rd6f")))

(define-public crate-scgsm-0.1.3 (c (n "scgsm") (v "0.1.3") (h "0yx91qm9lh9a1ayxbz4rm3rz9248ji568y922lid1266k01aggiz")))

(define-public crate-scgsm-0.2.0 (c (n "scgsm") (v "0.2.0") (h "0adg7ajs9vx9xnsp57pv0nhx554ifzfzv7z0mljr6iwmhn87lgfr")))

(define-public crate-scgsm-0.2.1 (c (n "scgsm") (v "0.2.1") (h "1s1lhwq05ipjncfj24dnv85snhrr87fjxc1j8zdjvy8yfg14cxkp")))

