(define-module (crates-io sc an scanflow) #:use-module (crates-io))

(define-public crate-scanflow-0.1.0 (c (n "scanflow") (v "0.1.0") (d (list (d (n "iced-x86") (r "^1.10.0") (d #t) (k 0)) (d (n "memflow") (r "^0.1.5") (d #t) (k 0)) (d (n "memflow-win32") (r "^0.1.5") (d #t) (k 0)) (d (n "pbr") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "pelite") (r "^0.9") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)) (d (n "rayon-tlsctx") (r "^0.2") (d #t) (k 0)))) (h "066svsiy4hylmryd2rrh673g3m8pxf9vp8f7cp109hkzb82425l5") (f (quote (("progress_bar" "pbr"))))))

(define-public crate-scanflow-0.1.1 (c (n "scanflow") (v "0.1.1") (d (list (d (n "iced-x86") (r "^1.10.0") (d #t) (k 0)) (d (n "memflow") (r "^0.1.5") (d #t) (k 0)) (d (n "memflow-win32") (r "^0.1.5") (d #t) (k 0)) (d (n "pbr") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "pelite") (r "^0.9") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)) (d (n "rayon-tlsctx") (r "^0.2") (d #t) (k 0)))) (h "0x8vpayxps3s5zgbrvaway01hji18s77dc6mczd3qiypcvxhb0vb") (f (quote (("progress_bar" "pbr"))))))

(define-public crate-scanflow-0.2.0-beta1 (c (n "scanflow") (v "0.2.0-beta1") (d (list (d (n "iced-x86") (r "^1.10.0") (d #t) (k 0)) (d (n "memflow") (r "^0.2.0-beta") (d #t) (k 0)) (d (n "pbr") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)) (d (n "rayon-tlsctx") (r "^0.2") (d #t) (k 0)))) (h "1c9f9xi8x1czkkgz01fdap4x1g8sznbhaw95r8sjrqc5lp4rnkwz") (f (quote (("progress_bar" "pbr"))))))

(define-public crate-scanflow-0.2.0-beta11 (c (n "scanflow") (v "0.2.0-beta11") (d (list (d (n "iced-x86") (r "^1.10.0") (d #t) (k 0)) (d (n "memflow") (r "=0.2.0-beta11") (d #t) (k 0)) (d (n "pbr") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)) (d (n "rayon-tlsctx") (r "^0.2") (d #t) (k 0)))) (h "1gak97vbhl9ki91fszi58appprnfb3ywlamaw4s4awgp16rf87y0") (f (quote (("progress_bar" "pbr"))))))

(define-public crate-scanflow-0.2.0 (c (n "scanflow") (v "0.2.0") (d (list (d (n "iced-x86") (r "^1.10.0") (d #t) (k 0)) (d (n "memflow") (r "^0.2") (d #t) (k 0)) (d (n "pbr") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)) (d (n "rayon-tlsctx") (r "^0.2") (d #t) (k 0)))) (h "0blhl2wxv6rlmws711jcnrwgs7la7r44psnz3z0qmhjkn102fp7b") (f (quote (("progress_bar" "pbr"))))))

