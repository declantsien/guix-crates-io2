(define-module (crates-io sc an scaner) #:use-module (crates-io))

(define-public crate-scaner-0.1.0 (c (n "scaner") (v "0.1.0") (d (list (d (n "homm5-types") (r "^0.1.0") (d #t) (k 0)) (d (n "quick-xml") (r "^0.30.0") (f (quote ("serialize" "serde-types"))) (d #t) (k 0)) (d (n "rc-zip") (r "^2.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum") (r "^0.25.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.25.2") (d #t) (k 0)) (d (n "utf16_reader") (r "^0.1.0") (d #t) (k 0)) (d (n "zip") (r "^0.6.6") (d #t) (k 0)))) (h "0zv0cjcram1qwpni09258dhj640xxsd4wwr69pj08izg78lkafq4")))

