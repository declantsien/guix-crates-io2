(define-module (crates-io sc an scandir_rs) #:use-module (crates-io))

(define-public crate-scandir_rs-2.4.2 (c (n "scandir_rs") (v "2.4.2") (d (list (d (n "pyo3") (r "^0.20") (f (quote ("extension-module"))) (d #t) (k 0)) (d (n "scandir") (r "^2.4.2") (d #t) (k 0)))) (h "1chl00qzm78phk6hpmaiir9r93dl8qbcii38bl403fbqjh9qhmda")))

(define-public crate-scandir_rs-2.5.0 (c (n "scandir_rs") (v "2.5.0") (d (list (d (n "pyo3") (r "^0.20") (f (quote ("extension-module"))) (d #t) (k 0)) (d (n "scandir") (r "^2.5.0") (d #t) (k 0)))) (h "1gjsji1gn3fw082d9jxzcr2vzh5gs7w1wflk5dvy587qk8g06kgg")))

(define-public crate-scandir_rs-2.5.1 (c (n "scandir_rs") (v "2.5.1") (d (list (d (n "pyo3") (r "^0.21") (f (quote ("extension-module"))) (d #t) (k 0)) (d (n "scandir") (r "^2.5.1") (d #t) (k 0)))) (h "03vx5wmb5j7fgb0rds5c5p03ifan8yfc8kqdgb18xk00w22l19jx")))

