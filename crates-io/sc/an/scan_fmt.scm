(define-module (crates-io sc an scan_fmt) #:use-module (crates-io))

(define-public crate-scan_fmt-0.1.0 (c (n "scan_fmt") (v "0.1.0") (h "1xk9d2skr1zhfy7p608vssv0m2whg2xg9d1913vfhdxzrqq5ksp7")))

(define-public crate-scan_fmt-0.1.1 (c (n "scan_fmt") (v "0.1.1") (h "1d6qcx7xmw5sq5107fw94hwsb09nvwv6hk1lhz0qvk2fjzm04s7s")))

(define-public crate-scan_fmt-0.1.2 (c (n "scan_fmt") (v "0.1.2") (d (list (d (n "regex") (r "^0.2.6") (d #t) (k 0)))) (h "0fxlpnbnr56fybgcynq2vhsw36s1lidq1g0xd8yl1cdpamzryjz9")))

(define-public crate-scan_fmt-0.1.3 (c (n "scan_fmt") (v "0.1.3") (d (list (d (n "regex") (r "^0.2.6") (d #t) (k 0)))) (h "0c6vd4llcyk39kqn478wvwgk76g8lpsjd5kbxqwybyzr4xs4k1wb")))

(define-public crate-scan_fmt-0.2.0 (c (n "scan_fmt") (v "0.2.0") (d (list (d (n "regex") (r "^0.2.6") (d #t) (k 0)))) (h "0ygx84lpj0m167spljzm53c6ajjjll769zq1vfpkax52rf5wrjda")))

(define-public crate-scan_fmt-0.2.1 (c (n "scan_fmt") (v "0.2.1") (d (list (d (n "regex") (r "~1.1.6") (o #t) (d #t) (k 0)))) (h "05zja5zf9yrw11v1pb0iyw6iiplsayagxwy7nycnsmx5xc6g9366") (f (quote (("default" "regex"))))))

(define-public crate-scan_fmt-0.2.2 (c (n "scan_fmt") (v "0.2.2") (d (list (d (n "regex") (r "~1.1.6") (o #t) (d #t) (k 0)))) (h "159i41dj1zm2rssgziyb9wpiv5hbfbpsqfv0jgrnbmgrwf1xbgls") (f (quote (("default" "regex"))))))

(define-public crate-scan_fmt-0.2.3 (c (n "scan_fmt") (v "0.2.3") (d (list (d (n "regex") (r "~1.1.6") (o #t) (d #t) (k 0)))) (h "18jg522y56ha94kmcvk80ayc9wjrmh9b4zvwxfaksg4i2s3d62rq") (f (quote (("default" "regex"))))))

(define-public crate-scan_fmt-0.2.4 (c (n "scan_fmt") (v "0.2.4") (d (list (d (n "regex") (r "^1") (o #t) (d #t) (k 0)))) (h "1nml76nav0vqw95x1wyblffkw8vzwk6qxx5mi4c20h53gkrkgxzs") (f (quote (("default" "regex"))))))

(define-public crate-scan_fmt-0.2.5 (c (n "scan_fmt") (v "0.2.5") (d (list (d (n "regex") (r "^1") (o #t) (d #t) (k 0)))) (h "1gmaa07z8bkkdv5xhq2lrgml6ri7fqyyrjpiks3phmpmq3p8d0i4") (f (quote (("default" "regex"))))))

(define-public crate-scan_fmt-0.2.6 (c (n "scan_fmt") (v "0.2.6") (d (list (d (n "regex") (r "^1") (o #t) (d #t) (k 0)))) (h "0j0jb1dsa8zjpnc875wy72190zlyngvl62mfv8pqwal8vfjv0lqb") (f (quote (("std") ("default" "regex" "std"))))))

