(define-module (crates-io sc an scan-fonts) #:use-module (crates-io))

(define-public crate-scan-fonts-1.0.0 (c (n "scan-fonts") (v "1.0.0") (d (list (d (n "failure") (r "^0.1.3") (d #t) (k 0)) (d (n "getset") (r "^0.0.6") (d #t) (k 0)))) (h "1d1dslbik6c9r92cb8vgyni0323si2sa3hvqard52w5iz17qpkaq")))

