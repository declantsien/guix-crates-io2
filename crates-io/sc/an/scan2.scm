(define-module (crates-io sc an scan2) #:use-module (crates-io))

(define-public crate-scan2-0.1.0 (c (n "scan2") (v "0.1.0") (h "05l3n9v93aqxr2fbwf8y8p9rw876pmr6bbhm133r4x6gv4vjs6aw")))

(define-public crate-scan2-0.1.1 (c (n "scan2") (v "0.1.1") (h "0gdzzzs164872g0iw5j0crfbljbcx8xrd9mmkzinmwqqadghq3by")))

