(define-module (crates-io sc an scansion) #:use-module (crates-io))

(define-public crate-scansion-0.0.1 (c (n "scansion") (v "0.0.1") (d (list (d (n "crossterm") (r "^0.27") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "modalkit") (r "^0.0.17") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "06h8l801gdxlzb1l67akkbs16nfkxcv2jac83v93hra04j12g8n9") (r "1.67")))

