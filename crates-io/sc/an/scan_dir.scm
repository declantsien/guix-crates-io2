(define-module (crates-io sc an scan_dir) #:use-module (crates-io))

(define-public crate-scan_dir-0.1.0 (c (n "scan_dir") (v "0.1.0") (d (list (d (n "quick-error") (r "^0.1.3") (d #t) (k 0)))) (h "0bkcvzmr834mdcgb119l65sjp5y48cwrssby2v1m9szmjzmijkfy")))

(define-public crate-scan_dir-0.2.0 (c (n "scan_dir") (v "0.2.0") (d (list (d (n "quick-error") (r "^0.1.3") (d #t) (k 0)))) (h "1k32v9wz3gnqyp89r2y65h6iw0fjzdkpjwfmgd0p2hldy91ix692")))

(define-public crate-scan_dir-0.3.0 (c (n "scan_dir") (v "0.3.0") (d (list (d (n "quick-error") (r "^0.1.3") (d #t) (k 0)))) (h "1q55zg9dc1pd1qzismbvs331flbvp27apws87b12agmxpznavzns") (y #t)))

(define-public crate-scan_dir-0.3.1 (c (n "scan_dir") (v "0.3.1") (d (list (d (n "quick-error") (r "^0.1.3") (d #t) (k 0)))) (h "095g51knrh8370i35p5qpcj03f123nd0g08as35i2mjasnlivhw0")))

(define-public crate-scan_dir-0.3.2 (c (n "scan_dir") (v "0.3.2") (d (list (d (n "quick-error") (r "^0.1.3") (d #t) (k 0)))) (h "07hdqrq18ssdq5jkdmpjkwcsszjglai19xkpkby15hm4kjyvbhwr")))

(define-public crate-scan_dir-0.3.3 (c (n "scan_dir") (v "0.3.3") (d (list (d (n "quick-error") (r "^1.0.0") (d #t) (k 0)))) (h "0mcidja2xx12r24cpqwpxxadj3mrzqkihg85ic95nxzj4qyqxjqw")))

