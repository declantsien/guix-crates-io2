(define-module (crates-io sc an scanner-syn) #:use-module (crates-io))

(define-public crate-scanner-syn-0.1.0 (c (n "scanner-syn") (v "0.1.0") (d (list (d (n "Inflector") (r "^0.11.4") (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "fold" "extra-traits" "visit"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "0i3zkxh0zqlzsb70js4y72wh2razlwayj9bqzmfwbphdsb6qyk8g")))

(define-public crate-scanner-syn-0.2.0 (c (n "scanner-syn") (v "0.2.0") (d (list (d (n "Inflector") (r "^0.11.4") (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "fold" "extra-traits" "visit"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "1ll16dw8fyw4rn1qdmw2zi25ah601nkhx98z7icb3r6h3rww3kxr")))

