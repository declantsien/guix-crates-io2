(define-module (crates-io sc an scanf) #:use-module (crates-io))

(define-public crate-scanf-0.0.0 (c (n "scanf") (v "0.0.0") (h "1xiraxagszp0qy0myfqbl2wdbrzl6rrz1r03imml6gl41792wr0y") (y #t)))

(define-public crate-scanf-0.1.0 (c (n "scanf") (v "0.1.0") (d (list (d (n "nom") (r "^7.1") (d #t) (k 0)))) (h "069r6pgzqgp9f164g78yynzlci4xn0ywn4bgfds7krc59lslix1c") (y #t)))

(define-public crate-scanf-0.1.1 (c (n "scanf") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "nom") (r "^7.1") (d #t) (k 0)))) (h "1w7dwjcr74fpg0iyz7qqqzfp900cr500cmqbziagnq4agcd98gia") (y #t)))

(define-public crate-scanf-0.1.2 (c (n "scanf") (v "0.1.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "nom") (r "^7.1") (d #t) (k 0)))) (h "0d854j3bfxdxfy3rpg15kzfp9nl7a9k6371fg2rnln6k31a4dxs2") (y #t)))

(define-public crate-scanf-0.1.3 (c (n "scanf") (v "0.1.3") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "nom") (r "^7.1") (d #t) (k 0)))) (h "17xns41h6y7nivd9l6y8vzqg45ygs8jv2p59ly4zmf6yga5amk0v") (y #t)))

(define-public crate-scanf-0.2.0 (c (n "scanf") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "nom") (r "^7.1") (d #t) (k 0)))) (h "1lf7b8pz1bmb1qkgar80cp6g8mkc0xvhy6kmsnynasqbfnv2hgr8") (y #t)))

(define-public crate-scanf-0.2.1 (c (n "scanf") (v "0.2.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "nom") (r "^7.1") (d #t) (k 0)))) (h "0s0azabzj3aldnmxcilg3vx6xayvbqjflq09xhzlnsp0palfgxdx") (y #t)))

(define-public crate-scanf-0.2.2 (c (n "scanf") (v "0.2.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "nom") (r "^7.1") (d #t) (k 0)))) (h "0x7jgdskq7vgnyg5rk0zkh50bl1xrs9a2hgm6di1q52jx2fnhhhg") (y #t)))

(define-public crate-scanf-0.2.3 (c (n "scanf") (v "0.2.3") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "nom") (r "^7.1") (d #t) (k 0)))) (h "197zap2i77prl91hiwdspj3936h65app0c8p8zkcmvvqqrpxymdq") (y #t)))

(define-public crate-scanf-1.0.0 (c (n "scanf") (v "1.0.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "nom") (r "^7.1") (d #t) (k 0)))) (h "0qr9gpb3s3d4chi46qw3mq8xc2qpg17fqgp6ldqskbkf6mbczxxi") (y #t)))

(define-public crate-scanf-1.0.1 (c (n "scanf") (v "1.0.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "nom") (r "^7.1") (d #t) (k 0)))) (h "0y6s4a9zwp6r9nm6jsd83s6b1bj64lgjzqynw8l91dxnn2irlpbx") (y #t)))

(define-public crate-scanf-1.0.2 (c (n "scanf") (v "1.0.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "nom") (r "^7") (d #t) (k 0)))) (h "1zq8d7jhvkg9khbxh88glvh9lblz7ary3x0wj6g2r3cp5rqpls8a") (y #t)))

(define-public crate-scanf-1.0.3 (c (n "scanf") (v "1.0.3") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "nom") (r "^7") (d #t) (k 0)))) (h "1qqhc6h6mfgi4vavwlmphldams0pni7rkh90k9xlbm91i3ysxfp3") (y #t)))

(define-public crate-scanf-1.0.4 (c (n "scanf") (v "1.0.4") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "nom") (r "^7") (d #t) (k 0)))) (h "18mxvqszcbpfigj1wvm408jn0rcc7pc968mrrjhnkcv6f6s60zpy") (y #t)))

(define-public crate-scanf-1.0.5 (c (n "scanf") (v "1.0.5") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "nom") (r "^7") (d #t) (k 0)))) (h "1czdns1ya32s3xgsfcqxrs7a3ahc1cdsr86dnzrl83wa1gk0cgkn") (y #t)))

(define-public crate-scanf-1.1.0 (c (n "scanf") (v "1.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "nom") (r "^7") (d #t) (k 0)))) (h "0b06il0jgci4afmd2b8x4qaaxw8ml0z9l64i3bhdfsjb7cq35xgr")))

(define-public crate-scanf-1.1.1 (c (n "scanf") (v "1.1.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "nom") (r "^7") (d #t) (k 0)))) (h "10rasdkrsswv31ssd18f4wkf72fkcc9d5vdlwkdxljpri4f07nxs")))

(define-public crate-scanf-1.2.0 (c (n "scanf") (v "1.2.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "nom") (r "^7") (d #t) (k 0)))) (h "0la51dg8brqqqicbw13hbg3lag0indssg4h45h6lmaq5fbr5jwsk")))

(define-public crate-scanf-1.1.2 (c (n "scanf") (v "1.1.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "nom") (r "^7") (d #t) (k 0)))) (h "1imfy9nb7ca9l3aawcwi84mnmhwgmnkrswlb9zdzja7df3jyi8my")))

(define-public crate-scanf-1.1.3 (c (n "scanf") (v "1.1.3") (d (list (d (n "nom") (r "^7") (d #t) (k 0)))) (h "0mfmwa33n4wqa1qx0c1b5i0wzqr9whpkf6r4sjszbkz6npfmjzq3")))

(define-public crate-scanf-1.2.1 (c (n "scanf") (v "1.2.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "nom") (r "^7") (d #t) (k 0)))) (h "1kp8ncspmf5hi259clq3yqca8mdp3596b1wqia1ly6v8ds03pwhs")))

