(define-module (crates-io sc an scanner-rust) #:use-module (crates-io))

(define-public crate-scanner-rust-1.0.0 (c (n "scanner-rust") (v "1.0.0") (h "0m2ncmk28siqdjmi0fd4dl6gdrlvh7v25kqxk6kvb7jf36pzfqaw") (f (quote (("nightly"))))))

(define-public crate-scanner-rust-1.0.1 (c (n "scanner-rust") (v "1.0.1") (h "1k1dnqx1hx5sn9m1ww1j4nvplrjrzz1w5pd39bwln651pfrhdps1") (f (quote (("nightly"))))))

(define-public crate-scanner-rust-1.0.2 (c (n "scanner-rust") (v "1.0.2") (h "1i2x75q9mnvzfy1hyi37yl0p7c6r2bybv11rp0jzx8ljb2rdm23v") (f (quote (("nightly"))))))

(define-public crate-scanner-rust-1.0.3 (c (n "scanner-rust") (v "1.0.3") (h "1nlgmsg18hxdw6nhrc9g999hfyisciaknbxyv9vp8vsf1pp834q6") (f (quote (("nightly"))))))

(define-public crate-scanner-rust-1.0.4 (c (n "scanner-rust") (v "1.0.4") (h "0q5gj4badkn3x6smv0fgvamai38s4jd5js089hhv4nw2x980vcpf") (f (quote (("nightly"))))))

(define-public crate-scanner-rust-1.0.5 (c (n "scanner-rust") (v "1.0.5") (h "0jlab4hrc19vxcqrg4vwjib2zm98k1aah90f84q7whi1kyvx4lrl") (f (quote (("nightly"))))))

(define-public crate-scanner-rust-1.0.6 (c (n "scanner-rust") (v "1.0.6") (h "1sm0lzxrg849j7qysvap9k7qf67dx5vqly3n1h720r3a4zz15ksj") (f (quote (("nightly"))))))

(define-public crate-scanner-rust-1.1.0 (c (n "scanner-rust") (v "1.1.0") (h "1k3vnc94qcahsr0asiyjmf49rv4kgqgf420h7m5n5d8dxwgj0f51") (f (quote (("nightly"))))))

(define-public crate-scanner-rust-1.2.0 (c (n "scanner-rust") (v "1.2.0") (h "12gmabdwx16za7048vap6bg5q1nv0ljc0yy5br2dby39gxlxz2fa") (f (quote (("nightly"))))))

(define-public crate-scanner-rust-1.2.1 (c (n "scanner-rust") (v "1.2.1") (d (list (d (n "derivative") (r "^1") (d #t) (k 0)))) (h "0ljsr0sgv9sh4hpdwb4bnzarvns3awvvkc3wds4sbqqifkychi7n") (f (quote (("nightly"))))))

(define-public crate-scanner-rust-1.2.2 (c (n "scanner-rust") (v "1.2.2") (d (list (d (n "educe") (r ">= 0.0.1") (d #t) (k 0)))) (h "17asdsa6h56g23cyx3dvd9xvcn1qav9q5g3r5z2q5yh39nrp5wkk") (f (quote (("nightly"))))))

(define-public crate-scanner-rust-1.2.3 (c (n "scanner-rust") (v "1.2.3") (d (list (d (n "educe") (r ">= 0.4.0") (f (quote ("Debug"))) (k 0)))) (h "0x7dnm2zz2qyr8axd0wy3w0vlldv4y53wgdiyhypj3gh2zmgmdna") (f (quote (("nightly"))))))

(define-public crate-scanner-rust-1.2.4 (c (n "scanner-rust") (v "1.2.4") (d (list (d (n "educe") (r ">= 0.4.0") (f (quote ("Debug"))) (k 0)))) (h "1cd0msvjrf0fyiaxdbvxg9xpf2hww81bvnrm40xx8li7dm3ba7sk") (f (quote (("nightly"))))))

(define-public crate-scanner-rust-1.2.5 (c (n "scanner-rust") (v "1.2.5") (d (list (d (n "educe") (r ">= 0.4.0") (f (quote ("Debug"))) (k 0)))) (h "04qxqfxfqd55dwhhkhm9980852p6wnhqpcvcmf5f8g2bin85jfsm") (f (quote (("nightly"))))))

(define-public crate-scanner-rust-1.3.0 (c (n "scanner-rust") (v "1.3.0") (d (list (d (n "educe") (r ">= 0.4.0") (f (quote ("Debug"))) (k 0)))) (h "0i8x6m8lbwvrc7m9xb1i1bl7c0qv6svq819qmdmmkrq89yqps69i") (f (quote (("nightly"))))))

(define-public crate-scanner-rust-1.4.0 (c (n "scanner-rust") (v "1.4.0") (d (list (d (n "educe") (r ">= 0.4.0") (f (quote ("Debug"))) (k 0)))) (h "1ig6gr46v41cmdw2kp4mfp5l53fy32yqkw2xsysy3qnk8f4ix4rc") (f (quote (("nightly")))) (y #t)))

(define-public crate-scanner-rust-1.4.1 (c (n "scanner-rust") (v "1.4.1") (d (list (d (n "educe") (r ">= 0.4.0") (f (quote ("Debug"))) (k 0)))) (h "11v33ma4ncarfqif2c4ma3gkb70g3d9f9cqfirhk16kyx4hjb28c") (f (quote (("nightly"))))))

(define-public crate-scanner-rust-2.0.0 (c (n "scanner-rust") (v "2.0.0") (d (list (d (n "educe") (r ">=0.4") (f (quote ("Debug"))) (k 0)))) (h "1yzv618lmi3d3bxaybhscjrcgyg32hygk08ryvnp8j5670gkx0k4") (f (quote (("nightly"))))))

(define-public crate-scanner-rust-2.0.1 (c (n "scanner-rust") (v "2.0.1") (d (list (d (n "educe") (r ">=0.4") (f (quote ("Debug"))) (k 0)))) (h "1a670y7iifpdbpzl14wq17xqyjxd472a5cyylpib11d93fxrwdrj") (f (quote (("nightly"))))))

(define-public crate-scanner-rust-2.0.2 (c (n "scanner-rust") (v "2.0.2") (d (list (d (n "educe") (r ">=0.4") (f (quote ("Debug"))) (k 0)))) (h "17ax30yqsz8pqzs6g3wvnda1w3qgnhr9gbnmcjzh0rzl92wr55jj") (f (quote (("nightly"))))))

(define-public crate-scanner-rust-2.0.3 (c (n "scanner-rust") (v "2.0.3") (d (list (d (n "educe") (r ">=0.4") (f (quote ("Debug"))) (k 0)))) (h "00id7nrbk18ih8a3nw1phhfsy7q7c4wh8275fcdm81w6pa2vc2l4") (f (quote (("nightly"))))))

(define-public crate-scanner-rust-2.0.4 (c (n "scanner-rust") (v "2.0.4") (d (list (d (n "educe") (r ">=0.4") (f (quote ("Debug"))) (k 0)))) (h "1pdcg84r75vyy5h8lb9j698jn8jvnh7kgp87yx0k2djk7q2ms7fz") (f (quote (("nightly"))))))

(define-public crate-scanner-rust-2.0.5 (c (n "scanner-rust") (v "2.0.5") (d (list (d (n "educe") (r ">=0.4") (f (quote ("Debug"))) (k 0)))) (h "0a3i4v3wky5rcaprx4bf6hnh9n4bsns1lgf5nlpg0yd9z5xplh98") (f (quote (("nightly")))) (y #t)))

(define-public crate-scanner-rust-2.0.6 (c (n "scanner-rust") (v "2.0.6") (d (list (d (n "educe") (r ">=0.4") (f (quote ("Debug"))) (k 0)))) (h "1i64kc2jyzsjl727xsfnxskw04v9gs1ggywbb47bzpkf5w5pdvwc") (f (quote (("nightly"))))))

(define-public crate-scanner-rust-2.0.7 (c (n "scanner-rust") (v "2.0.7") (d (list (d (n "educe") (r ">=0.4") (f (quote ("Debug"))) (k 0)))) (h "1ahfcry6zhm2pdsqlc47yb5zvr2cwdd8ycsa6xdj8ga0blxj3f4j") (f (quote (("nightly"))))))

(define-public crate-scanner-rust-2.0.8 (c (n "scanner-rust") (v "2.0.8") (d (list (d (n "educe") (r ">=0.4") (f (quote ("Debug"))) (k 0)) (d (n "generic-array") (r "^0.14") (d #t) (k 0)))) (h "17h448lwig5a1s7a473ir61613870azlzk70jijk7fz92mw988g5") (f (quote (("nightly"))))))

(define-public crate-scanner-rust-2.0.9 (c (n "scanner-rust") (v "2.0.9") (d (list (d (n "educe") (r ">=0.4") (f (quote ("Debug"))) (k 0)) (d (n "generic-array") (r "^0.14") (d #t) (k 0)))) (h "003b79n54333lkw4fp2gqb0lzp79kx491j4m5n4bijgsbk9vdv2z") (f (quote (("nightly"))))))

(define-public crate-scanner-rust-2.0.10 (c (n "scanner-rust") (v "2.0.10") (d (list (d (n "educe") (r ">=0.4") (f (quote ("Debug"))) (k 0)) (d (n "generic-array") (r "^0.14") (d #t) (k 0)))) (h "0z2fy526qdy51fzz4s4njbfnj8gkz0vja8nm8ami97sczifd6bw6") (f (quote (("nightly"))))))

(define-public crate-scanner-rust-2.0.11 (c (n "scanner-rust") (v "2.0.11") (d (list (d (n "educe") (r ">=0.4") (f (quote ("Debug"))) (k 0)) (d (n "generic-array") (r "^0.14") (d #t) (k 0)) (d (n "utf8-width") (r "^0.1") (d #t) (k 0)))) (h "08w8mwjpihf1ggg7ca0dgg6ff42hasa2fqgz3qc9j0b2dcwcvnrx") (f (quote (("nightly"))))))

(define-public crate-scanner-rust-2.0.12 (c (n "scanner-rust") (v "2.0.12") (d (list (d (n "educe") (r ">=0.4") (f (quote ("Debug"))) (k 0)) (d (n "generic-array") (r "^0.14") (d #t) (k 0)) (d (n "utf8-width") (r "^0.1") (d #t) (k 0)))) (h "0f0yrqavadm1g0ymvx19zrjpdr6jvpq83vy7ba32pynv9r9i8409")))

(define-public crate-scanner-rust-2.0.13 (c (n "scanner-rust") (v "2.0.13") (d (list (d (n "educe") (r ">=0.4") (f (quote ("Debug"))) (k 0)) (d (n "generic-array") (r "^0.14") (d #t) (k 0)) (d (n "utf8-width") (r "^0.1") (d #t) (k 0)))) (h "1lhczvvln43n2zx54i97g4yj6jysw67gksgirpxx6jkgg23qz4l1")))

(define-public crate-scanner-rust-2.0.14 (c (n "scanner-rust") (v "2.0.14") (d (list (d (n "educe") (r ">=0.4") (f (quote ("Debug"))) (k 0)) (d (n "generic-array") (r "^0.14") (d #t) (k 0)) (d (n "utf8-width") (r "^0.1") (d #t) (k 0)))) (h "1r35736wy5bmrssi6c585qh9ayhkd1yvjajgqya0jsin86wl1z1b")))

(define-public crate-scanner-rust-2.0.15 (c (n "scanner-rust") (v "2.0.15") (d (list (d (n "educe") (r ">=0.4") (f (quote ("Debug"))) (k 0)) (d (n "generic-array") (r "^0.14") (d #t) (k 0)) (d (n "utf8-width") (r "^0.1") (d #t) (k 0)))) (h "11n6dcmmii7vbqjkbx1946x736w08n44487qnsqgkgq8qgvggzv0")))

(define-public crate-scanner-rust-2.0.16 (c (n "scanner-rust") (v "2.0.16") (d (list (d (n "educe") (r ">=0.4") (f (quote ("Debug"))) (k 0)) (d (n "generic-array") (r "^0.14") (d #t) (k 0)) (d (n "utf8-width") (r "^0.1") (d #t) (k 0)))) (h "191v2ps48pa8jzcd9201811i3x1p0x7cn0m2q8dz3js6n7hcjvvj")))

(define-public crate-scanner-rust-2.0.17 (c (n "scanner-rust") (v "2.0.17") (d (list (d (n "educe") (r ">=0.4") (f (quote ("Debug"))) (k 0)) (d (n "generic-array") (r "^1") (d #t) (k 0)) (d (n "utf8-width") (r "^0.1") (d #t) (k 0)))) (h "16icqmjjqbi2kv8sd0xypwlzlx3zwvwkaxf07p0dj5rsnf3jj5q6") (r "1.65")))

