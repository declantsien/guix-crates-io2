(define-module (crates-io sc an scanrs) #:use-module (crates-io))

(define-public crate-scanrs-0.1.0 (c (n "scanrs") (v "0.1.0") (h "1qvd7id7hyiwp5g5fsi51jr3kwysjjals7mdyljc33085bpd4xbf")))

(define-public crate-scanrs-0.1.5 (c (n "scanrs") (v "0.1.5") (h "0h17sga4x96n07cybm1yq5w3crm70wqkdiwaggzl7qkixlqgaks0")))

(define-public crate-scanrs-0.1.6 (c (n "scanrs") (v "0.1.6") (h "1anpyb57i25jwldglh8j4sssvyb72ylrys048ha3di82yqdrfwww")))

(define-public crate-scanrs-0.1.7 (c (n "scanrs") (v "0.1.7") (h "0b60vdkndmzhfb0zi3ph8gsyl5z7w0i0wwycfzsnysxcrnzqn1dy")))

(define-public crate-scanrs-0.2.0 (c (n "scanrs") (v "0.2.0") (h "0xf7far73ydrj1cijrnckbvfbbf9vzabyj8z73s4l0k1m53mq6qa")))

(define-public crate-scanrs-0.2.1 (c (n "scanrs") (v "0.2.1") (h "1ajagls9z3hjsxkqvr4m7cianlyzmncp98hwajaaia4pzmiwm8wz")))

(define-public crate-scanrs-0.3.0 (c (n "scanrs") (v "0.3.0") (h "03lbx757vzjw0kaylvwarrq6djg914ql2522gmzfyprsnlgsm47z")))

