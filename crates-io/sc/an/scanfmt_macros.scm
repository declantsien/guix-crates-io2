(define-module (crates-io sc an scanfmt_macros) #:use-module (crates-io))

(define-public crate-scanfmt_macros-0.1.0 (c (n "scanfmt_macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.20") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.81") (f (quote ("full"))) (d #t) (k 0)))) (h "1dgyffpr44pzbljyfv6c5bn8n7hafdygwmlpfz1fxkpbw0zm14qr")))

(define-public crate-scanfmt_macros-0.1.1 (c (n "scanfmt_macros") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.20") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.81") (f (quote ("full"))) (d #t) (k 0)))) (h "13pf5n732gzjamqnzagvp3gkzx2g8cbawnksf760pb5cl3r968iw")))

