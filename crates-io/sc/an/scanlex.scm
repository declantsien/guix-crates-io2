(define-module (crates-io sc an scanlex) #:use-module (crates-io))

(define-public crate-scanlex-0.1.0 (c (n "scanlex") (v "0.1.0") (h "12r1yqy6w5qqfvda2m7a8nyi7nl38vcgvmn6vycz8snp22qw8pb6")))

(define-public crate-scanlex-0.1.1 (c (n "scanlex") (v "0.1.1") (h "0x5mv0x2xvvvsvs56aw92cyxz8rm6wl93r0f7bp1nykqzxyvhzf9")))

(define-public crate-scanlex-0.1.2 (c (n "scanlex") (v "0.1.2") (h "1p2mvmflvpl0lkl68jb7kkcba8ls0cbpnigpg1llkjbvd55wi8p6")))

(define-public crate-scanlex-0.1.3 (c (n "scanlex") (v "0.1.3") (h "01vydx5dy4xrybj3ny907n3nnd6xk6901s42k1wqd0fdzp6y997a")))

(define-public crate-scanlex-0.1.4 (c (n "scanlex") (v "0.1.4") (h "1nrkq1kjwf3v084pndiq18yx6vqsivlqr6jllyg94911axqmv308")))

