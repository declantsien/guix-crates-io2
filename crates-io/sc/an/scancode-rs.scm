(define-module (crates-io sc an scancode-rs) #:use-module (crates-io))

(define-public crate-scancode-rs-0.1.0 (c (n "scancode-rs") (v "0.1.0") (d (list (d (n "git2") (r "^0.13") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.7") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 2)))) (h "14l33s64192kgx3z328a38xxd2r163z0fn4mg8mf1h7amg0g6v1d")))

