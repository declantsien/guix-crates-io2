(define-module (crates-io sc an scanlan_sudoku) #:use-module (crates-io))

(define-public crate-scanlan_sudoku-0.1.0 (c (n "scanlan_sudoku") (v "0.1.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1.8.1") (d #t) (k 0)))) (h "0knhfcy6izmd5awg2c2h21njf2d8y73q21gy3zq67hiiqfw01bib")))

