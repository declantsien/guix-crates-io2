(define-module (crates-io sc an scannit-core-ffi) #:use-module (crates-io))

(define-public crate-scannit-core-ffi-0.1.0 (c (n "scannit-core-ffi") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "scannit-core") (r "^0.1.0") (d #t) (k 0)))) (h "15mnsskh78x4wn2vc1fp2fv0pk9hsal9fxpb4ds3y32c40kiyisc")))

(define-public crate-scannit-core-ffi-1.0.5 (c (n "scannit-core-ffi") (v "1.0.5") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "scannit-core") (r "1.0.*") (d #t) (k 0)))) (h "15pbxg3j6ibnv6bxwlgcmmqsa5zazzll5593gkrbkxvpi5a6k60j")))

(define-public crate-scannit-core-ffi-1.0.11 (c (n "scannit-core-ffi") (v "1.0.11") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "scannit-core") (r "1.0.*") (d #t) (k 0)))) (h "11z4g64bg5w8w8pm0vfl07kc330lchb6d0shdmbdk4m53kh6sqmg")))

