(define-module (crates-io sc an scanfmt) #:use-module (crates-io))

(define-public crate-scanfmt-0.1.0 (c (n "scanfmt") (v "0.1.0") (d (list (d (n "scanfmt_macros") (r "^0.1.0") (d #t) (k 0)))) (h "0a1nd146jw0zbn3nrk2w7lk503pnl879hg8mdyq4zwpw9rv50c7r")))

(define-public crate-scanfmt-0.2.1 (c (n "scanfmt") (v "0.2.1") (d (list (d (n "scanfmt_macros") (r "^0.1.1") (d #t) (k 0)))) (h "1srf304ls04cr497w02wjzhvqp6rx9r5awv8g5sd86fi6ym0a41a")))

