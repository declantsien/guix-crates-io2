(define-module (crates-io sc an scandium) #:use-module (crates-io))

(define-public crate-scandium-0.0.0 (c (n "scandium") (v "0.0.0") (d (list (d (n "beryllium") (r "^0.2.0-alpha.2") (d #t) (k 0)) (d (n "bytemuck") (r "^1") (d #t) (k 0)) (d (n "gles30") (r "^0.1") (d #t) (k 0)) (d (n "imagine") (r "^0.0.2") (d #t) (k 0)) (d (n "ultraviolet") (r "^0.3") (d #t) (k 0)))) (h "1448k23yr3bkmm3fsy1fw4p5iw0nh4fr31yzd5dk03j3dx30ym8x")))

