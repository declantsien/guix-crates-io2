(define-module (crates-io sc an scannerctl) #:use-module (crates-io))

(define-public crate-scannerctl-0.1.0 (c (n "scannerctl") (v "0.1.0") (d (list (d (n "clap") (r "^4.2.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dialoguer") (r "^0.10.4") (d #t) (k 0)))) (h "0ifsar5zm4vjjs5xfrni72wzzvryw5al1adi5yj0s9wy654a63wl")))

