(define-module (crates-io sc an scannit-core) #:use-module (crates-io))

(define-public crate-scannit-core-0.1.0 (c (n "scannit-core") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1prq5sq1pg9jrmxm5fnnz1kznb8628q42xyghbdp60b46ba68fdk")))

(define-public crate-scannit-core-1.0.4 (c (n "scannit-core") (v "1.0.4") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1rwxqcrm5y1dsi7bah6dc7kxv4v67kxigkbyjsqals7z180kqbw6")))

(define-public crate-scannit-core-1.0.5 (c (n "scannit-core") (v "1.0.5") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "15zbbhqf21dc3yqgfp3zk7i6jyqfsic53gd45vpc29bax1n6ar3x")))

(define-public crate-scannit-core-1.0.11 (c (n "scannit-core") (v "1.0.11") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1iamrpk1vi5bld83d48xjm50namx22p6py878mq44f4pwwlw7xbv")))

