(define-module (crates-io sc an scancode) #:use-module (crates-io))

(define-public crate-scancode-0.1.0 (c (n "scancode") (v "0.1.0") (d (list (d (n "glutin") (r "^0.6") (d #t) (k 2)))) (h "07k93qj6g014yi8frbyllrq936r4w75qnyaj3mxqr0gh38j3lkz6")))

(define-public crate-scancode-0.1.1 (c (n "scancode") (v "0.1.1") (d (list (d (n "enum_primitive") (r "^0.1") (d #t) (k 0)) (d (n "glutin") (r "^0.7") (d #t) (k 2)) (d (n "num") (r "^0.1") (d #t) (k 2)))) (h "08wlq2lqjaz97y03mdbbizxbdh3spvkir7zvb3fg0w5lmf7wkh6k")))

(define-public crate-scancode-0.1.2 (c (n "scancode") (v "0.1.2") (d (list (d (n "enum_primitive") (r "^0.1") (d #t) (k 0)) (d (n "glutin") (r "^0.8") (d #t) (k 2)) (d (n "num") (r "^0.1") (d #t) (k 2)))) (h "0rcfh733r8bdci69yn904z4kbzjipy2sh78lzp4ddnbwzfa9c6g3")))

