(define-module (crates-io sc an scanner-soundboard) #:use-module (crates-io))

(define-public crate-scanner-soundboard-0.1.0 (c (n "scanner-soundboard") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (k 0)) (d (n "evdev") (r "^0.11.1") (d #t) (k 0)) (d (n "rodio") (r "^0.14") (f (quote ("mp3" "vorbis"))) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "0rvwk9hgh712wpvb55gvgyhhcgnr9finyap3s88alchyfhaf67fk") (y #t)))

(define-public crate-scanner-soundboard-0.1.1 (c (n "scanner-soundboard") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (k 0)) (d (n "evdev") (r "^0.11.1") (d #t) (k 0)) (d (n "rodio") (r "^0.14") (f (quote ("mp3" "vorbis"))) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "14mn1sq56m132ryrvalmp78367iivvn1prf6466gv55rkiwf94kx")))

(define-public crate-scanner-soundboard-0.2.0 (c (n "scanner-soundboard") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^3.1.18") (f (quote ("derive" "std"))) (k 0)) (d (n "evdev") (r "^0.11.4") (d #t) (k 0)) (d (n "rodio") (r "^0.15") (f (quote ("mp3" "vorbis"))) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "1zikxac91skj45q5nhbm9grvr8ch802jhbrs5r16n43rmbsmabp5")))

