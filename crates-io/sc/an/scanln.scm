(define-module (crates-io sc an scanln) #:use-module (crates-io))

(define-public crate-scanln-0.1.0 (c (n "scanln") (v "0.1.0") (h "1ilb18n0032c9n16ir7g3xdj5imjjrjr28rw78b99qhrnac6hk5l")))

(define-public crate-scanln-0.1.1 (c (n "scanln") (v "0.1.1") (h "1zsj9c50g7nz445k74bwf3cspdsnbfxifpkbzs185bjndwprqz77")))

