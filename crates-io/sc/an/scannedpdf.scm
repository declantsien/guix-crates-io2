(define-module (crates-io sc an scannedpdf) #:use-module (crates-io))

(define-public crate-scannedpdf-0.1.0 (c (n "scannedpdf") (v "0.1.0") (d (list (d (n "flate2") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "printpdf") (r "^0.5") (f (quote ("embedded_images"))) (d #t) (k 2)))) (h "1pq13a4g507q2xipdhd2zakwd8wzwknj0vj21asj65qi6qg2bn7z") (f (quote (("default" "flate2")))) (s 2) (e (quote (("flate2" "dep:flate2"))))))

