(define-module (crates-io sc an scanmut) #:use-module (crates-io))

(define-public crate-scanmut-0.1.0 (c (n "scanmut") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "rand") (r "^0.7.2") (d #t) (k 2)) (d (n "rand_pcg") (r "^0.2.1") (d #t) (k 2)))) (h "1hcv39jj99k10yvf2ysa2x36wbgqw8s68pgfiqb3ildnxk3853mp")))

(define-public crate-scanmut-0.2.0 (c (n "scanmut") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "proptest") (r "^0.10.1") (d #t) (k 2)) (d (n "rand") (r "^0.7.2") (d #t) (k 2)) (d (n "rand_pcg") (r "^0.2.1") (d #t) (k 2)))) (h "0arbm5i21hcja424b79zarkjcqyy9zj7as51lij80vzwypp4pj7a")))

