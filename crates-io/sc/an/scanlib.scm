(define-module (crates-io sc an scanlib) #:use-module (crates-io))

(define-public crate-scanlib-0.1.0 (c (n "scanlib") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0f8za9m2679c0jcpbxmcy60isqzpiszalgnb0fli30xqp34jfl15")))

