(define-module (crates-io sc ou scout-audit-internal) #:use-module (crates-io))

(define-public crate-scout-audit-internal-0.2.0 (c (n "scout-audit-internal") (v "0.2.0") (d (list (d (n "scout-audit-clippy-utils") (r "=0.2.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "strum") (r "^0.25") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1agjc51q6n5gljggmffv1zz1mcgg0nax0kplza6hjlqmk5w3by7l") (s 2) (e (quote (("lint_helper" "dep:scout-audit-clippy-utils" "dep:serde_json") ("detector" "dep:strum"))))))

(define-public crate-scout-audit-internal-0.2.1 (c (n "scout-audit-internal") (v "0.2.1") (d (list (d (n "scout-audit-clippy-utils") (r "=0.2.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "strum") (r "^0.25") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "17ldizpnfm98x5gpq3nywp21bg8hbbajvvf82d026i6fjxrz42n0") (s 2) (e (quote (("lint_helper" "dep:scout-audit-clippy-utils" "dep:serde_json") ("detector" "dep:strum"))))))

(define-public crate-scout-audit-internal-0.2.2 (c (n "scout-audit-internal") (v "0.2.2") (d (list (d (n "scout-audit-clippy-utils") (r "=0.2.2") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "strum") (r "^0.25") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0nwlcvwcc2j1v5sxp9lxjlrhajc16fqwv66b3cz606ggslc3dhh2") (s 2) (e (quote (("lint_helper" "dep:scout-audit-clippy-utils" "dep:serde_json") ("detector" "dep:strum"))))))

(define-public crate-scout-audit-internal-0.2.3 (c (n "scout-audit-internal") (v "0.2.3") (d (list (d (n "scout-audit-clippy-utils") (r "=0.2.3") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "strum") (r "^0.25") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "00f0yh611h043gr7gyq3ca9m2alr4ikgzq2pvcwbgf5vl6aqkiqd") (s 2) (e (quote (("lint_helper" "dep:scout-audit-clippy-utils" "dep:serde_json") ("detector" "dep:strum"))))))

(define-public crate-scout-audit-internal-0.2.4 (c (n "scout-audit-internal") (v "0.2.4") (d (list (d (n "scout-audit-clippy-utils") (r "=0.2.3") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "strum") (r "^0.25") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0bzk34c11kpkh6581xaqw171rh7jb146l581c008fzc9z6jkjrg8") (s 2) (e (quote (("lint_helper" "dep:scout-audit-clippy-utils" "dep:serde_json") ("detector" "dep:strum"))))))

