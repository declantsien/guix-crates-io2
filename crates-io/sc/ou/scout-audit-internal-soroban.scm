(define-module (crates-io sc ou scout-audit-internal-soroban) #:use-module (crates-io))

(define-public crate-scout-audit-internal-soroban-0.2.1 (c (n "scout-audit-internal-soroban") (v "0.2.1") (d (list (d (n "scout-audit-clippy-utils") (r "=0.2.1") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "strum") (r "^0.25") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0hyymzfsmrak703ghbybg8yj6agf2ncvpmpnylzwiav4v8hlyx91") (s 2) (e (quote (("lint_helper" "dep:scout-audit-clippy-utils" "dep:serde_json") ("detector" "dep:strum"))))))

(define-public crate-scout-audit-internal-soroban-0.2.2 (c (n "scout-audit-internal-soroban") (v "0.2.2") (d (list (d (n "scout-audit-clippy-utils-soroban") (r "=0.2.2") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "strum") (r "^0.25") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "113l44rbjfigy1j81k2aqg86cq2b92ljyf7ica67rnqk5ais5h3b") (s 2) (e (quote (("lint_helper" "dep:scout-audit-clippy-utils-soroban" "dep:serde_json") ("detector" "dep:strum"))))))

