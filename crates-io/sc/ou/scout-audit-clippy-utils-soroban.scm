(define-module (crates-io sc ou scout-audit-clippy-utils-soroban) #:use-module (crates-io))

(define-public crate-scout-audit-clippy-utils-soroban-0.2.1 (c (n "scout-audit-clippy-utils-soroban") (v "0.2.1") (d (list (d (n "arrayvec") (r "^0.7") (k 0)) (d (n "if_chain") (r "^1.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "rustc-semver") (r "^1.1") (d #t) (k 0)))) (h "0bv7d6y8jhkhvxpfvlldc0dbsv4z5m8l6x93020sivjjrpfy4jxn") (f (quote (("internal") ("deny-warnings"))))))

(define-public crate-scout-audit-clippy-utils-soroban-0.2.2 (c (n "scout-audit-clippy-utils-soroban") (v "0.2.2") (d (list (d (n "arrayvec") (r "^0.7") (k 0)) (d (n "if_chain") (r "^1.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "rustc-semver") (r "^1.1") (d #t) (k 0)))) (h "0cb5k8lq49k85lis5lyvzzn6bjgn8vs4ir60q204xh2nmym3h3pz") (f (quote (("internal") ("deny-warnings"))))))

