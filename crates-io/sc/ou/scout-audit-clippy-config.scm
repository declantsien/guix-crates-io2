(define-module (crates-io sc ou scout-audit-clippy-config) #:use-module (crates-io))

(define-public crate-scout-audit-clippy-config-0.1.76 (c (n "scout-audit-clippy-config") (v "0.1.76") (d (list (d (n "rustc-semver") (r "^1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.3") (d #t) (k 0)) (d (n "walkdir") (r "^2.3") (d #t) (k 2)))) (h "01j4yw4f368niq78a76plmdbvri60c4m7jip52c0hqbnh0143056") (f (quote (("deny-warnings"))))))

