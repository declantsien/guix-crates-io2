(define-module (crates-io sc ou scout_game) #:use-module (crates-io))

(define-public crate-scout_game-0.1.0 (c (n "scout_game") (v "0.1.0") (d (list (d (n "rand") (r "^0.6.0") (d #t) (k 0)))) (h "1ki575dxx7ak3wmbmlszha9q424bf3dd688980ha3bcm7lmx93g6")))

(define-public crate-scout_game-0.1.1 (c (n "scout_game") (v "0.1.1") (d (list (d (n "rand") (r "^0.6.0") (d #t) (k 0)))) (h "18rvrlhs84flv1fcp4rpprgazvi3lkrz9y4cm4k0flg2rkg7lwjd")))

