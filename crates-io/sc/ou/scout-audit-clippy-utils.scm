(define-module (crates-io sc ou scout-audit-clippy-utils) #:use-module (crates-io))

(define-public crate-scout-audit-clippy-utils-0.2.0 (c (n "scout-audit-clippy-utils") (v "0.2.0") (d (list (d (n "arrayvec") (r "^0.7") (k 0)) (d (n "if_chain") (r "^1.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "rustc-semver") (r "^1.1") (d #t) (k 0)))) (h "06cra54p4kkmwp5wwm7dlxm70dvm67099pqsn3zyyvav41bl7881") (f (quote (("internal") ("deny-warnings"))))))

(define-public crate-scout-audit-clippy-utils-0.2.1 (c (n "scout-audit-clippy-utils") (v "0.2.1") (d (list (d (n "arrayvec") (r "^0.7") (k 0)) (d (n "if_chain") (r "^1.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "rustc-semver") (r "^1.1") (d #t) (k 0)))) (h "1x7zw2pj44s04iv1smii4hd2x8454m99bncx42l3r95281zawxm0") (f (quote (("internal") ("deny-warnings"))))))

(define-public crate-scout-audit-clippy-utils-0.2.2 (c (n "scout-audit-clippy-utils") (v "0.2.2") (d (list (d (n "arrayvec") (r "^0.7") (k 0)) (d (n "if_chain") (r "^1.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "rustc-semver") (r "^1.1") (d #t) (k 0)))) (h "0cf6lr5y194cv92anips9fmdhalgrjdihixxnnwv3nf2csfjz0ki") (f (quote (("internal") ("deny-warnings"))))))

(define-public crate-scout-audit-clippy-utils-0.2.3 (c (n "scout-audit-clippy-utils") (v "0.2.3") (d (list (d (n "arrayvec") (r "^0.7") (k 0)) (d (n "clippy_config") (r "^0.1.76") (d #t) (k 0) (p "scout-audit-clippy-config")) (d (n "itertools") (r "^0.11") (d #t) (k 0)) (d (n "rustc-semver") (r "^1.1") (d #t) (k 0)))) (h "12947qpwr1995azw8nh6m232vsj599am931ipr1hllzml8cd25f1")))

