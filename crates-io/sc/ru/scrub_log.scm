(define-module (crates-io sc ru scrub_log) #:use-module (crates-io))

(define-public crate-scrub_log-0.1.0 (c (n "scrub_log") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (f (quote ("std"))) (d #t) (k 0)))) (h "0hs6q5dnhbz3xsf8xh562p3xgd7pih7y9n0r19lv0a0j4hamqbwj")))

(define-public crate-scrub_log-0.1.1 (c (n "scrub_log") (v "0.1.1") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (f (quote ("std"))) (d #t) (k 0)))) (h "1s1favdgck4klr0kn720aa40i0hq8mr8czsk04nksdx0a62zc54x")))

(define-public crate-scrub_log-0.2.0 (c (n "scrub_log") (v "0.2.0") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "env_logger") (r "^0.7") (d #t) (k 0)) (d (n "gflags") (r "^0.3") (d #t) (k 2)) (d (n "log") (r "^0.4.8") (f (quote ("std"))) (d #t) (k 0)))) (h "0n5zy4xblmw2yj0mpb7f01ipm88vz4bgd7fbn3i441a7s3pfncsr")))

(define-public crate-scrub_log-0.2.1 (c (n "scrub_log") (v "0.2.1") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "env_logger") (r "^0.7") (d #t) (k 0)) (d (n "gflags") (r "^0.3") (d #t) (k 2)) (d (n "log") (r "^0.4.8") (f (quote ("std"))) (d #t) (k 0)))) (h "1v8awn6s7lryd4zhbppyaw34q74hagzc1px2wrnazyl653ngkw9q")))

