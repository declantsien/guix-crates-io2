(define-module (crates-io sc ru scrummage) #:use-module (crates-io))

(define-public crate-scrummage-0.1.0 (c (n "scrummage") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)))) (h "04i4xdh251vqrmcjv4k2kica9pdi8z5mbkvv6agdb3mhcykmd7l2") (f (quote (("std") ("default" "std"))))))

(define-public crate-scrummage-0.1.1 (c (n "scrummage") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)))) (h "1r35407m35pxrn9rcx3mw92r8d13876dgjhykw3bhd9c40xql4hl") (f (quote (("std") ("default" "std"))))))

