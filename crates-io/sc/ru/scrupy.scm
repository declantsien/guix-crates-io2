(define-module (crates-io sc ru scrupy) #:use-module (crates-io))

(define-public crate-scrupy-0.1.0 (c (n "scrupy") (v "0.1.0") (d (list (d (n "hyper") (r "^0.10.12") (d #t) (k 0)) (d (n "url") (r "^1.5.1") (d #t) (k 0)))) (h "0yx7bm5phkyykib6dn2n44fzr425fw29agpkqr54p1vfcsb6z8hl")))

(define-public crate-scrupy-0.1.5 (c (n "scrupy") (v "0.1.5") (d (list (d (n "hyper") (r "^0.10.12") (d #t) (k 0)) (d (n "url") (r "^1.5.1") (d #t) (k 0)))) (h "18prf4wci47gdd4pjs2vv9hjli87zdjsnsh8sd61v5abdcpdq7xv")))

(define-public crate-scrupy-0.1.6 (c (n "scrupy") (v "0.1.6") (d (list (d (n "hyper") (r "^0.10.12") (d #t) (k 0)) (d (n "url") (r "^1.5.1") (d #t) (k 0)))) (h "029lfbq6pg90b8hvqlak21a7zmwb4xsi1k75k93ah1390gfcbjk0")))

