(define-module (crates-io sc ru scrunch) #:use-module (crates-io))

(define-public crate-scrunch-0.1.1 (c (n "scrunch") (v "0.1.1") (d (list (d (n "buffertk") (r "^0.3") (d #t) (k 0)) (d (n "prototk") (r "^0.2") (d #t) (k 0)) (d (n "prototk_derive") (r "^0.2") (d #t) (k 0)) (d (n "zerror_core") (r "^0.2") (d #t) (k 0)))) (h "12700p8qx6iyd7mwmm419gb0la11gx43v96g7fqvflc6l994c9d1")))

(define-public crate-scrunch-0.2.0 (c (n "scrunch") (v "0.2.0") (d (list (d (n "arrrg") (r "^0.3") (d #t) (k 2)) (d (n "buffertk") (r "^0.6") (d #t) (k 0)) (d (n "guacamole") (r "^0.6") (d #t) (k 2)) (d (n "proptest") (r "^1.0") (d #t) (k 2)) (d (n "prototk") (r "^0.6") (d #t) (k 0)) (d (n "prototk_derive") (r "^0.6") (d #t) (k 0)) (d (n "statslicer") (r "^0.2") (d #t) (k 2)))) (h "0bb3v3252hd5cs0ib122yvhhip1yhb69qvns03iflgisl2jygrlc") (f (quote (("default" "binaries") ("binaries"))))))

