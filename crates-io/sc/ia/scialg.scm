(define-module (crates-io sc ia scialg) #:use-module (crates-io))

(define-public crate-scialg-0.1.0 (c (n "scialg") (v "0.1.0") (d (list (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "num") (r "^0.4.1") (d #t) (k 0)))) (h "0zxqcz2dkgs72rh4z31xiirq2g7vspfcljdd5dhbc587adx2yvj8")))

(define-public crate-scialg-0.2.0 (c (n "scialg") (v "0.2.0") (d (list (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "num") (r "^0.4.1") (d #t) (k 0)))) (h "06lcyypnh6m98cgqy9ajvi18cjd3qpgs0r33j1n161sfwrwr21hn")))

(define-public crate-scialg-0.3.0 (c (n "scialg") (v "0.3.0") (d (list (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "num") (r "^0.4.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0b6ih2qh1dnj3vqlg820la1rx9yw3rvjmzgwc97vmiabx85rx0r3")))

