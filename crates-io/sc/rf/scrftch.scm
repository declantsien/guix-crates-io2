(define-module (crates-io sc rf scrftch) #:use-module (crates-io))

(define-public crate-scrftch-0.1.0 (c (n "scrftch") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.6.7") (d #t) (k 0)) (d (n "psutil") (r "^1.1.1") (d #t) (k 0)) (d (n "rust-ini") (r "^0.11.0") (d #t) (k 0)) (d (n "sys-info") (r "^0.5.6") (d #t) (k 0)))) (h "1vj9970f8ch2vjz5fic8nbbvvqv1kb3xaq1kh77i1kgh5jjm3hm6") (y #t)))

(define-public crate-scrftch-0.1.1 (c (n "scrftch") (v "0.1.1") (d (list (d (n "log") (r "^0.4.1") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.6.7") (d #t) (k 0)) (d (n "rust-embed") (r "^1.1.1") (d #t) (k 0)) (d (n "rust-ini") (r "^0.11.0") (d #t) (k 0)) (d (n "sys-info") (r "^0.5.6") (d #t) (k 0)))) (h "0gyjl2b8dj7wws0njlgklcl36ik665csqxzzvaa891rilzg8hkmf") (y #t)))

