(define-module (crates-io sc pc scpcb_redux_mapper) #:use-module (crates-io))

(define-public crate-scpcb_redux_mapper-0.1.0 (c (n "scpcb_redux_mapper") (v "0.1.0") (d (list (d (n "clap") (r "^4.3.19") (f (quote ("derive"))) (d #t) (k 2)) (d (n "image") (r "^0.24.5") (d #t) (k 2)) (d (n "image_colored_text") (r "^0.1.2") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rusttype") (r "^0.9.3") (d #t) (k 2)))) (h "1l190rhkps7r9rmmc6p1fvnz86cyykvqjvlq233d1qghrhsy81gl")))

(define-public crate-scpcb_redux_mapper-0.2.0 (c (n "scpcb_redux_mapper") (v "0.2.0") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 2)) (d (n "image") (r "^0.24.7") (d #t) (k 2)) (d (n "image_colored_text") (r "^0.1.2") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rusttype") (r "^0.9.3") (d #t) (k 2)))) (h "1c20v83bzkr5j582q70rnk5ql1cpv46s4s1sfhm1labc8dhdfi3w")))

