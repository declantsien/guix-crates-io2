(define-module (crates-io sc is scispeak) #:use-module (crates-io))

(define-public crate-scispeak-0.1.0 (c (n "scispeak") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "disambiseq") (r "^0.1.10") (d #t) (k 0)) (d (n "fxread") (r "^0.2.10") (d #t) (k 0)) (d (n "gzp") (r "^0.11.3") (f (quote ("deflate_rust"))) (k 0)) (d (n "hashbrown") (r "^0.14.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "spinoff") (r "^0.8.0") (d #t) (k 0)))) (h "0lbwidlwb8p9sfc1bl79iwmllcymfygj8ygl0vkb68sml0ws76fw")))

