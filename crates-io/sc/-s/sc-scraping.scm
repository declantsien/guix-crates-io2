(define-module (crates-io sc -s sc-scraping) #:use-module (crates-io))

(define-public crate-sc-scraping-0.1.0 (c (n "sc-scraping") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.22") (f (quote ("serde"))) (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "scraper") (r "^0.13.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.142") (f (quote ("derive"))) (d #t) (k 0)) (d (n "url") (r "^2.2.2") (f (quote ("serde"))) (d #t) (k 0)))) (h "1arpnz8hnwnhlw9gxc5mrna4pfzxggwb9i8l0mjjihsqlr1k97kw")))

