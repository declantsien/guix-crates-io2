(define-module (crates-io sc #{2-}# sc2-proto) #:use-module (crates-io))

(define-public crate-sc2-proto-0.1.0 (c (n "sc2-proto") (v "0.1.0") (d (list (d (n "protobuf") (r "^1.4") (d #t) (k 0)))) (h "1dnxzk3r5d4vki1sbvn0flhwf364xb90j43nb9s8bghh81dm1na4")))

(define-public crate-sc2-proto-0.1.1 (c (n "sc2-proto") (v "0.1.1") (d (list (d (n "protobuf") (r "^1.4") (d #t) (k 0)))) (h "1qz7vml0rwxd8h7s0lwvscaq446mnqbf3a6964bk5q8vdvk1d2yw")))

(define-public crate-sc2-proto-0.1.2 (c (n "sc2-proto") (v "0.1.2") (d (list (d (n "protobuf") (r "^1.4") (d #t) (k 0)))) (h "1ak5kg2s3kcdbkb4zrhd474vk5xp41kny8d60wzwcig1vyi6dsb3")))

(define-public crate-sc2-proto-0.1.3 (c (n "sc2-proto") (v "0.1.3") (d (list (d (n "protobuf") (r "^1.4") (d #t) (k 0)))) (h "0vw5r1zhfmcwawr413zbb1y0jjljfq534wmwcbrn9lwbz1w0ka3g")))

(define-public crate-sc2-proto-0.1.4 (c (n "sc2-proto") (v "0.1.4") (d (list (d (n "protobuf") (r "^1.4") (d #t) (k 0)))) (h "09mn9hk46phi5niy1zw8klphgwzbah47jz65xj9ay6bggl8kbky4")))

(define-public crate-sc2-proto-0.1.5 (c (n "sc2-proto") (v "0.1.5") (d (list (d (n "protobuf") (r "^1.4") (d #t) (k 0)))) (h "1n4wwss8gjs1dh462si3qjqavbyad8vhlpc0p273dkanw95bf2dq")))

(define-public crate-sc2-proto-0.1.6 (c (n "sc2-proto") (v "0.1.6") (d (list (d (n "protobuf") (r "^1.4") (d #t) (k 0)))) (h "0md6zdgrx4hw5pf33ij46gws8rgfckxm01qrgc7mias43qq72viz")))

(define-public crate-sc2-proto-0.1.7 (c (n "sc2-proto") (v "0.1.7") (d (list (d (n "protobuf") (r "^1.4") (d #t) (k 0)))) (h "09dz096s37addmfw9hlzly8xxb13nbmr8j4hj8271qzjaaqlw1zn")))

(define-public crate-sc2-proto-0.1.8 (c (n "sc2-proto") (v "0.1.8") (d (list (d (n "protobuf") (r "^1.4") (d #t) (k 0)))) (h "16iia9cnzrrfpwnr5rnnnxh159r8wl2h8zni2v79708wknb5ly0a")))

(define-public crate-sc2-proto-0.2.0-alpha.0 (c (n "sc2-proto") (v "0.2.0-alpha.0") (d (list (d (n "protobuf") (r "^1.4") (d #t) (k 0)) (d (n "protoc-rust") (r "^1.5") (d #t) (k 1)))) (h "0g8l40x37kzax0jkgfha7xidvpjgdy6n9klsawwknhyh12srby8w")))

(define-public crate-sc2-proto-0.2.1 (c (n "sc2-proto") (v "0.2.1") (d (list (d (n "protobuf") (r "^2.3") (d #t) (k 0)) (d (n "protoc-rust") (r "^2.3") (d #t) (k 1)))) (h "1gysjqynbb9gcy189q1clgks9dx7bap05x0kd5zy5ly2in5lim94")))

(define-public crate-sc2-proto-0.2.2 (c (n "sc2-proto") (v "0.2.2") (d (list (d (n "protobuf") (r "^2.20") (d #t) (k 0)) (d (n "protoc-rust") (r "^2.20") (d #t) (k 1)))) (h "0s6d6i4ddkqf5ph6k50aqvycfi2n4qgmj0nq6kz7v30l6x7bvfy3")))

(define-public crate-sc2-proto-0.2.3 (c (n "sc2-proto") (v "0.2.3") (d (list (d (n "protobuf") (r "^2.20") (d #t) (k 0)) (d (n "protoc-rust") (r "^2.20") (o #t) (d #t) (k 1)))) (h "19amyqzc1a6q15m48q1r9byss8gygg7alz5zv9zxriqvsb28agki")))

