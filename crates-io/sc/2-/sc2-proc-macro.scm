(define-module (crates-io sc #{2-}# sc2-proc-macro) #:use-module (crates-io))

(define-public crate-sc2-proc-macro-1.0.0 (c (n "sc2-proc-macro") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1.0.18") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.33") (f (quote ("full"))) (d #t) (k 0)))) (h "0247y4wj8wziwv2jxqqnp2k0z8c011pxwjgvy414ys2cri4p8iiv")))

