(define-module (crates-io sc #{2-}# sc2-techtree) #:use-module (crates-io))

(define-public crate-sc2-techtree-0.1.0 (c (n "sc2-techtree") (v "0.1.0") (d (list (d (n "noisy_float") (r "^0.1.9") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1jbf25fpm0vf8dn87ac8rsrd4i2xl0pkzg4n5w33j53p7hmrl59b")))

