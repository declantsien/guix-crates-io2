(define-module (crates-io sc or score) #:use-module (crates-io))

(define-public crate-score-0.0.1 (c (n "score") (v "0.0.1") (d (list (d (n "clap") (r "^2.24.0") (d #t) (k 0)) (d (n "glob") (r "^0.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.3.0") (d #t) (k 0)) (d (n "rouille") (r "^1.0.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "time") (r "^0.1.0") (d #t) (k 0)))) (h "0djhgin5bdxj5nfiqfd3k3kgycrshphmfzrjbqsw6g4was1qg0ff")))

(define-public crate-score-0.1.0 (c (n "score") (v "0.1.0") (d (list (d (n "clap") (r "^2.24.0") (d #t) (k 0)) (d (n "glob") (r "^0.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.3.0") (d #t) (k 0)) (d (n "rouille") (r "^1.0.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "time") (r "^0.1.0") (d #t) (k 0)))) (h "1ma2s8bpa7g2fhpi9qi0nf5r4hqa9111pixadj3f4sm1w3mh1w5w")))

(define-public crate-score-0.2.0 (c (n "score") (v "0.2.0") (d (list (d (n "clap") (r "^2.24.0") (d #t) (k 0)) (d (n "glob") (r "^0.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.3.0") (d #t) (k 0)) (d (n "rouille") (r "^1.0.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "time") (r "^0.1.0") (d #t) (k 0)))) (h "170p1lsnk1fk58qi3hi06zv8fgshr6nfxvlpdmnr6lrvfhm26nqp")))

