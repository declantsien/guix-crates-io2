(define-module (crates-io sc or scorched) #:use-module (crates-io))

(define-public crate-scorched-0.2.1 (c (n "scorched") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "owo-colors") (r "^3.5.0") (d #t) (k 0)))) (h "1g7lg887zb7ar6b2jashxv1dxn7nx2gj94r4dyz2b2xwhbaiz5sk")))

(define-public crate-scorched-0.2.2 (c (n "scorched") (v "0.2.2") (d (list (d (n "owo-colors") (r "^3.5.0") (d #t) (k 0)))) (h "0wy7xz5nmszll6f5gip2cyp8lmfja846wqhmyiarzafn6fkma8l7")))

(define-public crate-scorched-0.2.3 (c (n "scorched") (v "0.2.3") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "owo-colors") (r "^3.5.0") (d #t) (k 0)))) (h "027ahhjpjy8i5swaxdmvpd4fw9l4pkpy4vxccql9v5yww3j792fn")))

(define-public crate-scorched-0.2.4 (c (n "scorched") (v "0.2.4") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "owo-colors") (r "^3.5.0") (d #t) (k 0)))) (h "0fcdgg16jmhd9w41p4v89bbpl2q75qz4q6grdr7zcn13a32lsmyz")))

(define-public crate-scorched-0.2.6 (c (n "scorched") (v "0.2.6") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "owo-colors") (r "^3.5.0") (d #t) (k 0)))) (h "0s94k1ndqvbqza2ixbjmchzrw16z09wfizq94jp7rk388nkj4z52") (y #t)))

(define-public crate-scorched-0.2.7 (c (n "scorched") (v "0.2.7") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "owo-colors") (r "^3.5.0") (d #t) (k 0)))) (h "1izgfzibmhxxgh175b43pxi1bb8b6f5d5p6pr2bc41493xfpjdz1")))

(define-public crate-scorched-0.2.8 (c (n "scorched") (v "0.2.8") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "owo-colors") (r "^3.5.0") (d #t) (k 0)))) (h "1jd6x3ck3dh2pm9gygns536qxi57g7qc7460kn5mh6jqfr0qwjzg")))

(define-public crate-scorched-0.2.9 (c (n "scorched") (v "0.2.9") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "owo-colors") (r "^3.5.0") (d #t) (k 0)))) (h "1bni521ad5qxp3vdcmfbxail6i51wrg2q4g5bh42r2s86qc0inmz")))

(define-public crate-scorched-0.3.0 (c (n "scorched") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "owo-colors") (r "^3.5.0") (d #t) (k 0)))) (h "1b1139jqvycbrnmqwp6xymq36qc188lg8kbb1n0fj8p70zj85fz5")))

(define-public crate-scorched-0.3.2 (c (n "scorched") (v "0.3.2") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "owo-colors") (r "^3.5.0") (d #t) (k 0)))) (h "1jrzvp7qf6fyay1kv3ggp47vnnd86bzcis9323j1l8wdi7f98cpc")))

(define-public crate-scorched-0.4.0 (c (n "scorched") (v "0.4.0") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "owo-colors") (r "^3.5.0") (d #t) (k 0)))) (h "1yis3vpahpgzwmk5qns2n4gw8zcqz5lqfr9m7r74zvgpizv7bmd2") (y #t)))

(define-public crate-scorched-0.4.1 (c (n "scorched") (v "0.4.1") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "owo-colors") (r "^3.5.0") (d #t) (k 0)))) (h "10kis902fy1vk7w8ibkc8faij6p8id98ks77l1yga34lw6r3h4vk")))

(define-public crate-scorched-0.4.2 (c (n "scorched") (v "0.4.2") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "owo-colors") (r "^3.5.0") (d #t) (k 0)))) (h "1c65i6y6plbgssbx3a6a21b00rl4ykcya3c7h5jk05g6blbrh2hy")))

(define-public crate-scorched-0.4.3 (c (n "scorched") (v "0.4.3") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "owo-colors") (r "^3.5.0") (d #t) (k 0)))) (h "0rsz33x4l13x61w8k5hszm017b2b9dwz63g3wbrj24wjm2f8v2bz")))

(define-public crate-scorched-0.4.4 (c (n "scorched") (v "0.4.4") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "owo-colors") (r "^3.5.0") (d #t) (k 0)))) (h "1w9skiwgwb3zp80dn7kcjjgpswqvssjywrpn0c8kri8p87ii2y57")))

(define-public crate-scorched-0.4.5 (c (n "scorched") (v "0.4.5") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "owo-colors") (r "^3.5.0") (d #t) (k 0)))) (h "0n8q6c71krr02wznics4krjnqk7cpi5a0vmqrbyz703xxcjp22h0")))

(define-public crate-scorched-0.5.0 (c (n "scorched") (v "0.5.0") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)))) (h "064m7a71nnf1f5m8ha6wh3syzacwc91ii6f5bfvpr1f40aipj2wr")))

(define-public crate-scorched-0.5.1 (c (n "scorched") (v "0.5.1") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)))) (h "0fhigpkid2517l0krzdk8wfy75jwymrhrhi5cmckxb2nwwvw2q8h")))

(define-public crate-scorched-0.5.2 (c (n "scorched") (v "0.5.2") (d (list (d (n "chrono") (r "^0.4.34") (d #t) (k 0)))) (h "14crzc576qv0va8k579p268i9v9hibfai68ibpy03sd5xnpni7pd")))

(define-public crate-scorched-0.5.3 (c (n "scorched") (v "0.5.3") (d (list (d (n "chrono") (r "^0.4.38") (d #t) (k 0)))) (h "1x1lqc85yw7p5p20xhkpjlllvvchz1hg2xmr7q9pggv939w8iijb")))

