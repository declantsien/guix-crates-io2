(define-module (crates-io sc or scorex_crypto_avltree) #:use-module (crates-io))

(define-public crate-scorex_crypto_avltree-0.1.0 (c (n "scorex_crypto_avltree") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "base16") (r "^0.2.1") (d #t) (k 0)) (d (n "blake2") (r "^0.9") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "bytes") (r "^1.0.1") (d #t) (k 0)) (d (n "debug-cell") (r "^0.1.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "sha2") (r "^0.9.8") (d #t) (k 0)))) (h "0jq1016awdjnjjxkw1h4a112842nlfh9z4h9qh2qwxq2hk94qa9w")))

