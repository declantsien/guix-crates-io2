(define-module (crates-io sc or scorpius) #:use-module (crates-io))

(define-public crate-scorpius-0.1.0 (c (n "scorpius") (v "0.1.0") (h "1gq51f71gsjn17d71b28p1vqvfr7yr3zhabalaw00jxqyr51gqy8")))

(define-public crate-scorpius-0.2.0 (c (n "scorpius") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3.5") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "0ps5zf6k5ndq8vlpvkhv0792ffyq9bwbgqnfinnb4rcb1pfls9jd")))

(define-public crate-scorpius-0.3.0 (c (n "scorpius") (v "0.3.0") (d (list (d (n "criterion") (r "^0.3.5") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "1pq9vb7gf42wyfdn4dybn8gzia29v51br33yc6cfr6q9lpk62scq")))

(define-public crate-scorpius-0.4.0 (c (n "scorpius") (v "0.4.0") (d (list (d (n "criterion") (r "^0.3.5") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "0l1vz6brqgb7m2414sbw7ih2hs8jqmn0n3szyx1r5kcgxq0vh3bb")))

