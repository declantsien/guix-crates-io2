(define-module (crates-io sc or scoreboard_db) #:use-module (crates-io))

(define-public crate-scoreboard_db-0.2.1 (c (n "scoreboard_db") (v "0.2.1") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "mysql") (r "^24.0.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.157") (f (quote ("derive"))) (d #t) (k 0)))) (h "0fp5f7kgysm0l5m9zqd4pnz5jsikg1fpqq5y4gsmbc22k260xy7j") (s 2) (e (quote (("database" "dep:mysql"))))))

