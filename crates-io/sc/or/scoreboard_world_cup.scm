(define-module (crates-io sc or scoreboard_world_cup) #:use-module (crates-io))

(define-public crate-scoreboard_world_cup-0.1.0 (c (n "scoreboard_world_cup") (v "0.1.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "099295gnig0dkgcjfvl5wplqp6abn22cbhi7qdf9g9vc9lkf5y00")))

(define-public crate-scoreboard_world_cup-0.1.1 (c (n "scoreboard_world_cup") (v "0.1.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "14n6xd9d6db5b8ypb81cbncbmf6rpy8dxizxrdmmdbf9jp9b6jim")))

