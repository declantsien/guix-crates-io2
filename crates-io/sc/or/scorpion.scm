(define-module (crates-io sc or scorpion) #:use-module (crates-io))

(define-public crate-scorpion-0.1.0 (c (n "scorpion") (v "0.1.0") (h "0ga90zyv2226fnxsmw3vfnhikwh9qkcvacmf8xlnrfndvsac7461")))

(define-public crate-scorpion-0.1.1 (c (n "scorpion") (v "0.1.1") (h "1p9dys2w60jax6c8d4wvkj7lxwad5mcp75qisi5qmw16i7kb60zr")))

(define-public crate-scorpion-0.1.2 (c (n "scorpion") (v "0.1.2") (h "1qh5v1j47ndizcbi70aydr6cpaapf3ymwhw0fn40bnj4srxjvilf")))

(define-public crate-scorpion-0.1.3 (c (n "scorpion") (v "0.1.3") (h "0swnmqr2igncs4zw7fgsdaf8yfw9nxss7yfrqinmm8fk7rcssy37")))

