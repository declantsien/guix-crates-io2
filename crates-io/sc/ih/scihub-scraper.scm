(define-module (crates-io sc ih scihub-scraper) #:use-module (crates-io))

(define-public crate-scihub-scraper-0.1.0 (c (n "scihub-scraper") (v "0.1.0") (d (list (d (n "quick-error") (r "^1.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (d #t) (k 0)) (d (n "scraper") (r "^0.11") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (d #t) (k 2)) (d (n "url") (r "^2.1") (d #t) (k 0)))) (h "0kh64rbvvsgj1fbapyi0w0icg0rrq707mvca4k95n8zs1lzc9z5x")))

(define-public crate-scihub-scraper-0.2.0 (c (n "scihub-scraper") (v "0.2.0") (d (list (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "quick-error") (r "^1.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (d #t) (k 0)) (d (n "scraper") (r "^0.11") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (d #t) (k 2)) (d (n "url") (r "^2.1") (d #t) (k 0)))) (h "0kq6ikrv4rigziyfbvg1dglp5xr6avhiazcp3zw0xxn1qivfmlsy")))

(define-public crate-scihub-scraper-0.3.0 (c (n "scihub-scraper") (v "0.3.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "quick-error") (r "^1.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (d #t) (k 0)) (d (n "scraper") (r "^0.11") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (d #t) (k 2)) (d (n "url") (r "^2.1") (d #t) (k 0)))) (h "0apddayrc0ihxrww38bpjlci1kr7lp66gp8ks7k1sifi3c1yrrll")))

(define-public crate-scihub-scraper-0.4.0 (c (n "scihub-scraper") (v "0.4.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "quick-error") (r "^1.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (d #t) (k 0)) (d (n "scraper") (r "^0.11") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (d #t) (k 2)) (d (n "url") (r "^2.1") (d #t) (k 0)))) (h "04qs9mdmhbbgyfyhlfjm67xv9k0xzmpmr823xbzmc3fd9p0d7ydd")))

(define-public crate-scihub-scraper-0.4.1 (c (n "scihub-scraper") (v "0.4.1") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "quick-error") (r "^1.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (d #t) (k 0)) (d (n "scraper") (r "^0.11") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (d #t) (k 2)) (d (n "url") (r "^2.1") (d #t) (k 0)))) (h "0h2y9xz5hjfmzk1kddp8icixyvwav8m0mag30450c56rfnf7pchl")))

(define-public crate-scihub-scraper-0.4.2 (c (n "scihub-scraper") (v "0.4.2") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "quick-error") (r "^1.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (d #t) (k 0)) (d (n "scraper") (r "^0.12") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (d #t) (k 2)) (d (n "url") (r "^2.1") (d #t) (k 0)))) (h "1fwxjfmybagxa7d9vsmwvgqj9l7aiwqlxx4q53rm0r3wxd64hipf")))

(define-public crate-scihub-scraper-0.5.0 (c (n "scihub-scraper") (v "0.5.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "quick-error") (r "^2.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "scraper") (r "^0.12") (d #t) (k 0)) (d (n "tokio") (r "^1.1") (f (quote ("rt" "rt-multi-thread"))) (d #t) (k 2)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "0lrayaj3m8zisrp435l95la8y2fy19xps8kk6yda9si1alpy1jfc")))

(define-public crate-scihub-scraper-0.5.1 (c (n "scihub-scraper") (v "0.5.1") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "quick-error") (r "^2.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "scraper") (r "^0.12") (d #t) (k 0)) (d (n "tokio") (r "^1.2") (f (quote ("rt" "rt-multi-thread"))) (d #t) (k 2)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "1mfpq3la8j7wwrbjykhgvn9x5wyci4k4nw6vp659samz966zgfsv")))

(define-public crate-scihub-scraper-0.5.2 (c (n "scihub-scraper") (v "0.5.2") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "quick-error") (r "^2.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "scraper") (r "^0.12") (d #t) (k 0)) (d (n "tokio") (r "^1.2") (f (quote ("rt" "rt-multi-thread"))) (d #t) (k 2)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "1lpn85lgjhlb16zzbkx6cfi7kl43y4p1b3lg9d0lj1h0wckk51p4")))

