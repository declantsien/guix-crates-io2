(define-module (crates-io sc ih scihub-scraper-cli) #:use-module (crates-io))

(define-public crate-scihub-scraper-cli-0.1.0 (c (n "scihub-scraper-cli") (v "0.1.0") (d (list (d (n "scihub-scraper") (r "^0.4.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros"))) (d #t) (k 0)))) (h "0rjis64ckvq88dmfqcvl857sy9y3cg7x0cgi07kv5f7x304sqh2n")))

(define-public crate-scihub-scraper-cli-0.1.1 (c (n "scihub-scraper-cli") (v "0.1.1") (d (list (d (n "scihub-scraper") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros"))) (d #t) (k 0)))) (h "1an5a9kjwam34dzi1m1x368gw347aj3p5vc0b47qvm7pcksjjgds")))

(define-public crate-scihub-scraper-cli-0.2.0 (c (n "scihub-scraper-cli") (v "0.2.0") (d (list (d (n "scihub-scraper") (r "^0.5") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1.1") (f (quote ("rt" "rt-multi-thread" "macros"))) (d #t) (k 0)))) (h "1h876zj7q8jpjp00ylica4inmp07b3n5fyb551c0fq9isa77337f")))

(define-public crate-scihub-scraper-cli-0.2.1 (c (n "scihub-scraper-cli") (v "0.2.1") (d (list (d (n "scihub-scraper") (r "^0.5") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1.2") (f (quote ("rt" "rt-multi-thread" "macros"))) (d #t) (k 0)))) (h "1mgy1kjgk4dmyw1z46gzq7xlkw36chzkzdfp917wl9kzs6pv9x61")))

(define-public crate-scihub-scraper-cli-0.2.2 (c (n "scihub-scraper-cli") (v "0.2.2") (d (list (d (n "scihub-scraper") (r "^0.5") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1.2") (f (quote ("rt" "rt-multi-thread" "macros"))) (d #t) (k 0)))) (h "0xfvm2cj6rhmbfbdxk2awp42h0lw6wh2gw8zla2i1yfnc7v1idgv")))

