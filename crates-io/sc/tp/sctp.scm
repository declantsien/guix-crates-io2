(define-module (crates-io sc tp sctp) #:use-module (crates-io))

(define-public crate-sctp-0.1.0 (c (n "sctp") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1dpz392r6zdxiyr7vzsq300xrl6lywhndb82bpbisj2pqsikwam8")))

(define-public crate-sctp-0.1.1 (c (n "sctp") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0mci28iylajj684hbsqpx7m1j5kp1r2hzw5nb4qqpmqpjn4vq86v")))

(define-public crate-sctp-0.1.2 (c (n "sctp") (v "0.1.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1gixr6vq9kd6kf8sgzid704kvpzw78244j0is340dzkn3v0q0qjh")))

(define-public crate-sctp-0.1.3 (c (n "sctp") (v "0.1.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1n8f89d5wwqby8269n82g1jd64gjlq8d79i0nidih1abn0xw87lq")))

(define-public crate-sctp-0.1.4 (c (n "sctp") (v "0.1.4") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0cl1190433v0lzh4xw4wzjmn83jdnkqvmi2c96jk5n1l64nw42dy")))

(define-public crate-sctp-0.1.5 (c (n "sctp") (v "0.1.5") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0c2j9krh8znavsvpg3mmrjmqvx2mink7s47yab8xjl5gz3b14kn1")))

