(define-module (crates-io sc tp sctpx) #:use-module (crates-io))

(define-public crate-sctpx-0.1.1 (c (n "sctpx") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0ag02n2z4zxqzcr7zh8m3gjym8b8vdngaclq1p3hpqn4y7yargnr") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.2 (c (n "sctpx") (v "0.1.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "05b4cd75swmixg7fvs4vyfc7ikls0a675qhs5mbwdh7lzwg3saf4") (f (quote (("server") ("default" "client" "server") ("client")))) (y #t)))

(define-public crate-sctpx-0.1.3 (c (n "sctpx") (v "0.1.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0p6kx0ndrybnm7ggq72ap97ppikrqisdp1j71vc5jbqjsdcrk9m1") (f (quote (("server") ("default" "client" "server") ("client")))) (y #t)))

(define-public crate-sctpx-0.1.4 (c (n "sctpx") (v "0.1.4") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0akcjvr2dmzip4qcjcxwd9n44a899x1r10mpvfqbcn69gw8fvc0k") (f (quote (("server") ("default" "client" "server") ("client")))) (y #t)))

(define-public crate-sctpx-0.1.5 (c (n "sctpx") (v "0.1.5") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "163j9ialg9li1fkglimigw9av57pq4ca70cy61paq3hly2zvj228") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.6 (c (n "sctpx") (v "0.1.6") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "02ylhisxh6d95hlmkg7vw5xi2hji44w20n10wzww3k8jxwx7qyx3") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.7 (c (n "sctpx") (v "0.1.7") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1hgppsb8wf91adp5c4fhikmagqjhxsz09839hx9hq5wva5k422my") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.8 (c (n "sctpx") (v "0.1.8") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0bgcywksvir1xhp6ynkvl24z9mjkia2bl1vx5lvy6dhkkr2y7wvi") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.9 (c (n "sctpx") (v "0.1.9") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1vv9bh4yjvisl3rnysilfjjk3mb020v8mcfyjijbrz2nhjc921m1") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.10 (c (n "sctpx") (v "0.1.10") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1zv9hql4yzprlnqw36a8ab0wqgxayqrxb80aay7kgkgvjma99883") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.11 (c (n "sctpx") (v "0.1.11") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1dfa8bvi6ppimx5kzgd1yfq7dpxkv6v2a6m1ziqsijbgkkg7v12c") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.12 (c (n "sctpx") (v "0.1.12") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1j8iz1xhv75yk99nk40c5bxfg3dmp8r1wy72v2a805wkqzmapgx8") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.13 (c (n "sctpx") (v "0.1.13") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1px18zrqvzi7vpk4c4ahynv7axihyvadb0rfasmsmsc4xk8fkgjl") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.14 (c (n "sctpx") (v "0.1.14") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "107bkrsmj7qsxs24l60y7zs1c0d4mic94lh9wc3pfrp7m4n101hn") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.15 (c (n "sctpx") (v "0.1.15") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0jck98f601lf2wwgh6m0y3zw09apf00qh2kmr26x4iibry8wc49x") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.16 (c (n "sctpx") (v "0.1.16") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "03sqxi7jxxj9r4sw16dd9r3k8qpgqv7qxsvza4b8ay9cp5im0djm") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.17 (c (n "sctpx") (v "0.1.17") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0h3qkc816wpyyvy95kyi1vh6p4sy1kmqrvyakkm751xrfz2xjg5h") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.18 (c (n "sctpx") (v "0.1.18") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1xnd8hjdj7nzj38jdxg29zhb8jkb4mn7grs4pngviw4qwy2gp8fb") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.19 (c (n "sctpx") (v "0.1.19") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1g2g27dgqbi1imvhgcgga5y2p97knlq19l847r4fbhsgdpgg94r4") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.20 (c (n "sctpx") (v "0.1.20") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1sb9v9743ryg1j0xbkw18s5bwl8d74krwi1m771vxh3kfzxac5dd") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.21 (c (n "sctpx") (v "0.1.21") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1fgrzfgz8sxy77xjz9nf7y38kmfcdjh695cxpd9dn77c0nzvnapy") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.22 (c (n "sctpx") (v "0.1.22") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "024vn9vmbbwn89qkpfmmsasmqdkdm0vcywyjrrgv6kfqlvfwglwf") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.23 (c (n "sctpx") (v "0.1.23") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "05w0ip5hdsmzj8xp0c93yl6g0vx826bb83wwzxlarkg5hnr9zk8m") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.25 (c (n "sctpx") (v "0.1.25") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0xijbp4x10phzj9wqviayqlx8r1yhjap5y561jrxyaklyg32w28f") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.26 (c (n "sctpx") (v "0.1.26") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0si0lq4b3hcnarihyf2snlkshscjqdg5jd6ma6cmnki6czb52q0d") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.27 (c (n "sctpx") (v "0.1.27") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0yzh2ivlg5lqg5l98sy920f2s8cpp1fp3gh93pln8asfmj3kz9wp") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.28 (c (n "sctpx") (v "0.1.28") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1ypq54aqy5fdkrrzb4yczwljdv672p0vhwg2rq4x8dk6c3ycgq7i") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.29 (c (n "sctpx") (v "0.1.29") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1yy72l4vbdz1z3ingcaw0qbnggl74cyry3b22w3g28axjdk2dbmq") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.30 (c (n "sctpx") (v "0.1.30") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1svqwsfr09rnipva2hdrdl48l0nrkwri1v5qqk2q6i222zd00nyq") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.31 (c (n "sctpx") (v "0.1.31") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0y8mbx26x3whnq3z7rf4rxbcid2xb5hs4arwdliqlhhnz7b0nqkx") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.32 (c (n "sctpx") (v "0.1.32") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1akmi9686v46mi6r0wffg1kggkf4al63zxjgic191misfa0s7miv") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.33 (c (n "sctpx") (v "0.1.33") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0va95sl5wnzx4id0hkadbggmjx3ya1hvc47pyb65q1d0p0fvy6v8") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.34 (c (n "sctpx") (v "0.1.34") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "07gfclqcm2947pszg2m3vl6jb3gn3g9fb1gawi01m00w9yhaf9dm") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.35 (c (n "sctpx") (v "0.1.35") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "19llyv2k3ds6k5vhfrx7pqadlrskn9jwrb306yidpbvdj6473qx8") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.36 (c (n "sctpx") (v "0.1.36") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1zzhxcl0zjy7yq7dakkr3c18385vvppwflzn79i2h1hznd39kq35") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.37 (c (n "sctpx") (v "0.1.37") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "147fvkqps5srhs0684q8034jqagrfa4xa2lzpjqcrzkbvi08621m") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.38 (c (n "sctpx") (v "0.1.38") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1gv625mdy5d48x86rrn4b9zgd51lx3g94p006hwkmviwssm9cpxz") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.39 (c (n "sctpx") (v "0.1.39") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0pyimj0lmsswcxjfmah5xxw8lh2nlyr8pr37zsc9pkf5h16pgq4f") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.40 (c (n "sctpx") (v "0.1.40") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "19wiann5ajxcrzqghnn1smg6f4ccrk7rk037jdcm2hznj8g6n03a") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.41 (c (n "sctpx") (v "0.1.41") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1snn63lmlfwq6mm9jna6ph5ic082n6k6m13cfyhfyfackspsma4c") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.42 (c (n "sctpx") (v "0.1.42") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "01cw9clnbwxcz0b7fbqjw8k2yniy67rdlk9f796id5ij9nqqgmfd") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.43 (c (n "sctpx") (v "0.1.43") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1xigxjhlr2cd7gm4szbh5agvymq60nl9lqnpqzjbvz78yypih3s1") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.44 (c (n "sctpx") (v "0.1.44") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "09im8mgk8qdqpg89bjvzz28ib1ck6wqlyjvmidrypmd6ix2wwqyv") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.45 (c (n "sctpx") (v "0.1.45") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1xih9dmqnkiqqwjgyk0jmbrfirnq93vgm49apc5lvdw3dj9lra01") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.46 (c (n "sctpx") (v "0.1.46") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0s0460vrbqr69wfzpnlgm6yc5b15pchdnwa3dgpw3qs3a4nlxc2m") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.47 (c (n "sctpx") (v "0.1.47") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0pxs3rwgsvavqj8frggbzjv6bda293liqldyns373pv5q8dzqs01") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.48 (c (n "sctpx") (v "0.1.48") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1byd28nr3xmcv2mn1jnxmvafhcnqiklz4yczqkqnk3sp37kn9h44") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.49 (c (n "sctpx") (v "0.1.49") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0h2r34ya67r1wvmhjgwyrwc85l935wq5khl493h3nz8fxrppfgcm") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.50 (c (n "sctpx") (v "0.1.50") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "17l0v77rpp2i61xssr044xrzlir2nhc0cdylm0bfjrgv5jnqpyba") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.51 (c (n "sctpx") (v "0.1.51") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "16ws7h9aaw76k7bs8gm4cz5q3dqr6yfmwcaki1937p96isq7k2hq") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.52 (c (n "sctpx") (v "0.1.52") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "00il0w1x3y8n73ks95np2k7304v0ghz8ailanxa9cp25jnpi2x6g") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.53 (c (n "sctpx") (v "0.1.53") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1jxij7sszjdj7v73yj81raf1mss8imqq6rhn8dl0v43rlqc48l72") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.54 (c (n "sctpx") (v "0.1.54") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0d4rhqnf9npdsrif1nr12af5vc8sgvv4r1yznkj9za7b8dfnkgcw") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.55 (c (n "sctpx") (v "0.1.55") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0mgaas5cs5gc4r01qg7gklvswndwbzsf4wzvgmr1gmy6gkkfh6p5") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.56 (c (n "sctpx") (v "0.1.56") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0igl52mbnnqnx3yh7d6msc9289wns4r1ncr8jqvsmjj9d58zyvjn") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.57 (c (n "sctpx") (v "0.1.57") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1065bgm5bfddzm9lppddxx4jk9jx2hblp8shisn7iwbfqv5vld33") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.58 (c (n "sctpx") (v "0.1.58") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1qdllxwyidllp3abdd8dcbr95fcdhhxqa03dl3xk86rpcc5r2vd8") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.59 (c (n "sctpx") (v "0.1.59") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1qb13mi5nm9xx8835236451wj6y119lqn54076bmk6bwsxnh7405") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.60 (c (n "sctpx") (v "0.1.60") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1bk3wl8ggby6iwa8mlbjnq4dxnirvsb2fa4yvqidvgiyqai2b2qm") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.61 (c (n "sctpx") (v "0.1.61") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0rrsgxbq9g6qsnkw4wvr3pyr51ldiv5n9zbk3pjx6wdqllgdclp0") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.62 (c (n "sctpx") (v "0.1.62") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0nw3w3xmshbw5rpj15nd8q0xixp7sx79l8hrrljah62gkxk7w8d3") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.63 (c (n "sctpx") (v "0.1.63") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "17z13ddfzj3i0mn7phpdnz88wai3ic5lp8a5x87k1sdvk3hzn7zm") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.64 (c (n "sctpx") (v "0.1.64") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0fwxkz2pgzyz9m4ac5azs3f9ry3xsalr6wkdr38gmi8xd45smyn1") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.65 (c (n "sctpx") (v "0.1.65") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0cc1fqr067bk16yfhfvgay3g1zv06ppyd5ynawkp96h5lracqjzk") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.66 (c (n "sctpx") (v "0.1.66") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "05v6rlsvgvfx9nqsai5khbdq1fciaqgsmpdxjfnw95pga553ifp7") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.67 (c (n "sctpx") (v "0.1.67") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0wr7iz44cl8rphrm2bck1hdq9dzzfivmxn9rj39k2sxpwaaq95jv") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.68 (c (n "sctpx") (v "0.1.68") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0v58fsqg0l0mbpicfrwhinz0d1xk8bbry3mm89m6mcwzls9chrwd") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.69 (c (n "sctpx") (v "0.1.69") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0349g29zfal4g2k9ws43hz0i2z5bh0c037vh6gn8n550jxid1ql2") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.70 (c (n "sctpx") (v "0.1.70") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0cqvs2fddb0qbw88g5x79nps0lia84kymvj3ny6i42q41a6hfq79") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.71 (c (n "sctpx") (v "0.1.71") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1caf3brbc73fmvkb4j9c2b0vl552ra0327wrcqslgl3lc6yfvys9") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.72 (c (n "sctpx") (v "0.1.72") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "14hb0k0syf1ddm17fl1xnvs655qfhsp8b3fh3wp13v1dc7ax6zyl") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.73 (c (n "sctpx") (v "0.1.73") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0v6ysrvpffxa6dzfabbh2zqqypxnsh12dhy097zixpkm9xhri0qx") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.74 (c (n "sctpx") (v "0.1.74") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1fmikxislbrffpyr65ccf0pfbk32lmk49d3g9vsnmw1x0dp0ihhj") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.75 (c (n "sctpx") (v "0.1.75") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1c098ba8rfhngnf2py9asdk8slj01kwfs7r0mg9g7yxja3yfci1z") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.76 (c (n "sctpx") (v "0.1.76") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0g9ymbsznqw91b7wqzfyxl2dlshbnaz5kangvkagqs06v7q2rfmk") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.77 (c (n "sctpx") (v "0.1.77") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0gk2cqz3z3938l2qpvlvj92ny6ijm1i18hhb5sn9lbhdjvmybclb") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.78 (c (n "sctpx") (v "0.1.78") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1lxbf6ajagsm09x1q0612hv2pr1ndgsgnijmnll8ic36jrj7zdyf") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.79 (c (n "sctpx") (v "0.1.79") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1kyk47ab6nqrawak60cqzpm3z1f6f1a339pa15ms7q78h2ahmzai") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.80 (c (n "sctpx") (v "0.1.80") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "19avvdzd2f5gl7yqvxykfnc2d127dql80hlbslp5f1f32n4g7jzy") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.81 (c (n "sctpx") (v "0.1.81") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0l39pwsmqmvfhlqkvk8gizj7pfv7vla0bd31z2w1r36gb6yxsj35") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.82 (c (n "sctpx") (v "0.1.82") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "14xqfsrny4hasf2wp68g9yx98h4c6qwz0arg8l93k44vhrfa5444") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.83 (c (n "sctpx") (v "0.1.83") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0xqy1mcdgz9rl385b3vmza231d0xbws3xcw7c5qsyj6q6l1n365v") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.84 (c (n "sctpx") (v "0.1.84") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "09da64yibkif85m4bbxvqv2500nr09ns8s5kbwf42yxm7jzwzl2k") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.85 (c (n "sctpx") (v "0.1.85") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1lwkcg9rzqz6vb51r9pq63b60cv9h1jzm22ajxfxyv6qlgqy7dn7") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.86 (c (n "sctpx") (v "0.1.86") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "003vpnifgkdpcprwgl9fc2z44bkmvkfm8f8r6lyz37vkslmn2qy4") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.87 (c (n "sctpx") (v "0.1.87") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1ycys2kp34n54832q0d8fprnidbczvj8xnd11cy95fvfl8s2ky59") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.88 (c (n "sctpx") (v "0.1.88") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1mf84z1ggagj1359616srx91v4dba7n27cb0m2ssc347raqy50hc") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.89 (c (n "sctpx") (v "0.1.89") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "06fimzdx7283zwx124rrxw57adcjd181wly3cd5k3fd9adbci6qh") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.90 (c (n "sctpx") (v "0.1.90") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "08kkrb9bvjmb6204ycrzh6fhf0nll7n7w80qx3rjki3jp6cwawsz") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.91 (c (n "sctpx") (v "0.1.91") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1cmsd2cbrc1sbdlrnxd3msc7g9ir1w52rl7l0z178lg8p2ypdvwv") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.92 (c (n "sctpx") (v "0.1.92") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "02i2hxpx2hip98fywyr5533223kx66vzx9ymbbpin8f18mqicaqf") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.93 (c (n "sctpx") (v "0.1.93") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1gvjpa328zhj43hg7p1940gn88y1p3vsp6bg7hs723q70xrv71yj") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.94 (c (n "sctpx") (v "0.1.94") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1wfk6ikshmwaf03rwnpbc90m1dz2g562dwhp9964l2sbwpi8r1lf") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.95 (c (n "sctpx") (v "0.1.95") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1a3qvkr4wlir3mak8sh3ci2cg3n9318cmfa5bignfc8f59v1nvli") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.96 (c (n "sctpx") (v "0.1.96") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0y1q9hy07dvmpdxwlc0ibcgjdibk4kkicyy69x73cywyc2rrni4s") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.97 (c (n "sctpx") (v "0.1.97") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1p30izfk9098sbjm7hhh3lzr1qh5jr16x3knsn8kpv0lz8knbhcr") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.98 (c (n "sctpx") (v "0.1.98") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1gd4w0rfbm1s3f6mc1385hwn63hs0v32bv0sshvvcf0h92jhis76") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.99 (c (n "sctpx") (v "0.1.99") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1hs5s0m58nnck5zmrg74fw9la1g6glpkhi5bkl9bmb744qx2ncdj") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.100 (c (n "sctpx") (v "0.1.100") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1wwc7xyggsn67davnhlkb5sc1bad117vf47ivz5cbip2qvv5idff") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.101 (c (n "sctpx") (v "0.1.101") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0cr676l937fj15vcg1s3apm5vs5slqqb1ww8kk7x5cam1n5lyy7r") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.102 (c (n "sctpx") (v "0.1.102") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "11k6h66r4r3q51jx8vbw5d9scvq5iaii8bbk958fwnli4snp8yhi") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.103 (c (n "sctpx") (v "0.1.103") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0rvdinv2x80br764b5gna81r9afx4vr60nz2j2qzki6dlc3q1a9g") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.104 (c (n "sctpx") (v "0.1.104") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1mls3cwdwnlfr8dsq2hjbvispaby89km1aij6bbhaq9g9kmjl14g") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.105 (c (n "sctpx") (v "0.1.105") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1vwmn3cfsfc00wprmizr5xv315m879m7s93xfgx3galgxcfsy7yh") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.106 (c (n "sctpx") (v "0.1.106") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1k696bl1rffmy9l92kkznncikinhj33mir7bnm01s8vxs30nxcdz") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.107 (c (n "sctpx") (v "0.1.107") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0r1cw23ygh8bnp5qmwmdw29389zm5i31ipvmds1i50cbxa4icy9v") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.108 (c (n "sctpx") (v "0.1.108") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "09saclihmaql1c7b1c78wld3911a36hckvpz5yx8lpfi7dqkbnp8") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.109 (c (n "sctpx") (v "0.1.109") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0i0080vbvfg8jw6by9i4gdr3a65m3ccclqqiiy3c3zkrb09b15i7") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.110 (c (n "sctpx") (v "0.1.110") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0xdhy486xqzva7d0i36a8lj6g554bqid90dy8pdd60xx2mg5gw2m") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.111 (c (n "sctpx") (v "0.1.111") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "15ylvbsb3ai5lxh998k25mxqkbg1r7dx8cxyhyydlila7vxwa5j9") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.113 (c (n "sctpx") (v "0.1.113") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1fsy8d1y02nhynj2jz66npvlpxkcd2zw7ai3dvdgianjfgiqjhk1") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.114 (c (n "sctpx") (v "0.1.114") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "115pcs5b6y9jw84i3c01b2yrgdbzjr2f65xgzfy1hy24vmi9bh8l") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.115 (c (n "sctpx") (v "0.1.115") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1l2h5p5c8rpnhq19ka048x3fm7lq2hdkdvvahscb5nkx8qjkiyg3") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.116 (c (n "sctpx") (v "0.1.116") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0zf477mfmz4cvh1l4y7km29rngpd4h7a5xsfgj434cah6f2g2131") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.117 (c (n "sctpx") (v "0.1.117") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0qvc9c0dxhyr7i6xjhjmcah14kcnb805zy0xddwq8q8livk09dly") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.118 (c (n "sctpx") (v "0.1.118") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "067sz7zm72q0n0p2zdp6k62lfzzkw6h61nad8zqip81j1dq6jnxd") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.119 (c (n "sctpx") (v "0.1.119") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0hxzhvx6skzjwxsqwlpn6fadybcng8vsnzi72f8sbx9x9rhfxc7b") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.120 (c (n "sctpx") (v "0.1.120") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0f3xk8dn7vbkn33nypsm61q1sp6phgnwk133w81rjl1yrn6w7jiq") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.121 (c (n "sctpx") (v "0.1.121") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "16c5jb2svmpby07hy8vrjscb63x5yxf847h6h5b4m0vq2cm309sv") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.122 (c (n "sctpx") (v "0.1.122") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1185gzpyfx55wvw5lngq2v8ivjd8hqwy1klsmrjs2r7kl1i8a8mr") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.123 (c (n "sctpx") (v "0.1.123") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0q05avlwkhc3r2vnf8i1l9sfqw74dnc0hd1yk37cfs067amfpkw0") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.124 (c (n "sctpx") (v "0.1.124") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "13pppisl2nbwvvmq31if1zx4l2qxamg869gh4rszj8v6rnszp8mg") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.125 (c (n "sctpx") (v "0.1.125") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1w7dmksb8wp5d68jg5qk0x4v0mid0vc2is77p9n556c520qw628g") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.126 (c (n "sctpx") (v "0.1.126") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1gls94nhgglkac94s6kmai5v1qpv0wc3afs8r0rbnmg9qbvsavp0") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.127 (c (n "sctpx") (v "0.1.127") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0galinlzhd84qg96514prwlncwnvhblbzmk11wr2fx2zzbc71sz0") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.128 (c (n "sctpx") (v "0.1.128") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "167l8fqyvph2qkq22mn3shqshgmami4kv2lmj2j9nk0hhl9619q8") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.129 (c (n "sctpx") (v "0.1.129") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0fgnb4jpq9hjgp7n839hlxg55d75pdf0vgcg3sjvp8lxpricciq1") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.130 (c (n "sctpx") (v "0.1.130") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0k54gnkgsv23wwfi3d9xg4ma6v6kbzx97dyy7576q2s9cxdyk6yz") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.131 (c (n "sctpx") (v "0.1.131") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1z3f66pj9rky31m0czamn9h0biki99p6r5178qakbb2jhshik1q9") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.132 (c (n "sctpx") (v "0.1.132") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0w5a3q1i7rg5h9vc2bs7yc9qjrkv6fsc7hjdn9n5p1nqgd2r32l5") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.133 (c (n "sctpx") (v "0.1.133") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "120syd0ks800qx5d454wnd3vi6jw8l2pri0irzp9nnqh3akqsqj7") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.134 (c (n "sctpx") (v "0.1.134") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1lmb52iak4x067j696g4ydm7hvpnxbh8f8hdlk3xpwrszazamr53") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.135 (c (n "sctpx") (v "0.1.135") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1kmi86yavkagjbqn091i44kkd8nqcwys770g8125vq439f3l0ljh") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.136 (c (n "sctpx") (v "0.1.136") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0z3xanw07mq1kv3y93bhnh653fszpnjpswcvdn9kzf1cdd83kf5f") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.137 (c (n "sctpx") (v "0.1.137") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0zd88p6zwii2whzhcizd02dr3m94hdcqqhg1l8srgqk82j981dfl") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.138 (c (n "sctpx") (v "0.1.138") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0z1b0ayp8pan51i4kjyh55hgfgd1yvc3r4cfzydnkrzby88vqijr") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.139 (c (n "sctpx") (v "0.1.139") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1ngydrb4wgwfwx9pd0y5va4xx3rzzpqqiflh2xb7ajyczsmy2d3q") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.140 (c (n "sctpx") (v "0.1.140") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0r45bj47nq0gif8b9f70500hpvjplsmpjs95z6n8z5mqczbkz5f1") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.141 (c (n "sctpx") (v "0.1.141") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "02ykrg8iqlya10bjb94ifz00whfvsa8hlwhcwig746lm41d9d1h2") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.142 (c (n "sctpx") (v "0.1.142") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "03z7dca8h0rrjvabwwswksnjyy1sh6fmzs6rmgczvv6vavbqb1if") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.143 (c (n "sctpx") (v "0.1.143") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1pshgjkhld353ffvqhb0i6ml53ihn6f5ljmalbf6pbfrjasyr7mr") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.144 (c (n "sctpx") (v "0.1.144") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0wjhjkl87fa6yh5sf4c1hkapfvm4byyxvzz1xy8k3sa01qbxqfda") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.145 (c (n "sctpx") (v "0.1.145") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0b0racc9dzj6jidry811p0vnblizywffqhihb1z1xzk6nxb9gk6a") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.146 (c (n "sctpx") (v "0.1.146") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "01j2rin4hc24g8qp1bzwbmk54vh439953ibk77np1y56vphizhqi") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.147 (c (n "sctpx") (v "0.1.147") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0bcbq6ggq7vhaxrp0jjldyhrr14gcmmprd9gzj05hxrgfxm4irq0") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.148 (c (n "sctpx") (v "0.1.148") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0yvs7r3ci76vd4dxkn2nazdzslq9jak8dd3x6b4f4pgg7zkh9ddl") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.149 (c (n "sctpx") (v "0.1.149") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1jigi3i48d9rklq9r3r893837fdi0gw1925swqy0pyq4isj3lfnv") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.150 (c (n "sctpx") (v "0.1.150") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0mz6lnlcbjjc5bi3h265z0dvvwkj28yvpil3dcipzyh534afz8x4") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.151 (c (n "sctpx") (v "0.1.151") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "12biva4w5c6bn9jrd6fn4sr3znrmwn7bqr3q4zzfxd3njg3kjf5w") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.152 (c (n "sctpx") (v "0.1.152") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1v01sz54s3pb62c422pp6kwsvy2wa7819h85c4w63rvfid0jjysz") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.153 (c (n "sctpx") (v "0.1.153") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1zvakvhkkgjraj0brnf3xva222gwadxblnlcrqvwl91995sgzpw8") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.154 (c (n "sctpx") (v "0.1.154") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0qblgp1v9v3g823g8zj8nyjwg04f38sjp40fyjhbhf4lzn06gmrf") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.155 (c (n "sctpx") (v "0.1.155") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0k2n5bs49f58wl2xanmzc7qsqxczf97nhypkzcs6gz8m954ikhic") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.156 (c (n "sctpx") (v "0.1.156") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "13rhvmbpaalzgmbxq5qfq8980812k694gk69h40nfqsw3xh85h52") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.157 (c (n "sctpx") (v "0.1.157") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1ygaaysdg2gm636ln4ax4cz0wql3j50mr0bln8gppf716cwpsdy3") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.158 (c (n "sctpx") (v "0.1.158") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1iccm7nchy113i9z4mfv9m0bs7yb8a6vj9ndk2i1hz0xzgj1aamz") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.159 (c (n "sctpx") (v "0.1.159") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0ddy3r23ajd8siivj377q1p8jdi37h5d0fvz23szj5sn1pmnwxpq") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.160 (c (n "sctpx") (v "0.1.160") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0cfvsn19j0wzsp29002zi2s39vlnp862amyfkmi7f1bc5a37znqz") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.161 (c (n "sctpx") (v "0.1.161") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1g9rsl2vy2f8l4mz0rwv513dbgwxpzf797i4i1s8xp4isry01z23") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.162 (c (n "sctpx") (v "0.1.162") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1j2babr99pwrf2cifprf1y2k0s9l821br35zraqg46n44hrs8w2s") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.163 (c (n "sctpx") (v "0.1.163") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "11qbwbi74rm38i89imvv7wgnfnh595iyg4d1879jz9xrrxqbzr8n") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.164 (c (n "sctpx") (v "0.1.164") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1fpa928f26cjrb4qa55vyxzvfr07csn9g43gr0bkdij8jpx4c1kn") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.165 (c (n "sctpx") (v "0.1.165") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0yh0gp789fkaw12sy0hpc1g0cypm9nvmmywr4l4dz8bmzsngyp77") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.166 (c (n "sctpx") (v "0.1.166") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "09iqp8hbgp6ggqrpdqh9gvyhbyg8lpy93hs13gbvrm9wbfyfl3da") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.167 (c (n "sctpx") (v "0.1.167") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "06p667vk47bw04rrq9ywihx297f4zf72lf813wl2z3062v3mgcfz") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.168 (c (n "sctpx") (v "0.1.168") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0kpbpd6005jwyjr6haqd661hrkxjnva8nxdx68kkg2kxia6nhn7n") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.169 (c (n "sctpx") (v "0.1.169") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0wrqkqbqsn5wca47v32a9y7njczy7pvgg5x0is4rxxcds19vdl7p") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.170 (c (n "sctpx") (v "0.1.170") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1ndfmmnairjr3vihwlivxbvr79zw8fkklfjilmnavjyqflwc3xfg") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.171 (c (n "sctpx") (v "0.1.171") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1x1rk3j1d44j7x8k2ajvzrrcl7bygg34ij4rnv6wr7scrhw6hpi2") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.172 (c (n "sctpx") (v "0.1.172") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1h1ia908xqvwsfqas6s2nz2v3288v3dx3ykd29659faqjhc4dd7j") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.173 (c (n "sctpx") (v "0.1.173") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0lg70kcfchzw2b33dhinpfig3hynvi7jfrhl1zkarm3k0d09ic7l") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.174 (c (n "sctpx") (v "0.1.174") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1j87i5a1vly8ifrnfx0wk3ij69y62836an3hafvdc276h22x7fib") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.175 (c (n "sctpx") (v "0.1.175") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "00pg7lpsym3d53150clah62cc4a5jb9jv3nln3acff6wj7yfsqam") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.176 (c (n "sctpx") (v "0.1.176") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "10hprsy9rdvkb6rq6ckkvn0c328syv26ns01zmf57x0s42yzhd0s") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.177 (c (n "sctpx") (v "0.1.177") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "05qi8r6139wfai6ys5fz6lhllyqlfx0ylmskcikzqhd2ad0mi5d1") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.179 (c (n "sctpx") (v "0.1.179") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0bj61wbg4qp627ada6gzzrbppkjx8rgblrbnphrkkddr06advzga") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.181 (c (n "sctpx") (v "0.1.181") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0v1wzq284jn39xaaqad85f61icm4kx7phw2kgx0r5j1j3y29kc4n") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.182 (c (n "sctpx") (v "0.1.182") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "142vx0rbi88yxz9qdxpm0d312dgk1q5k5iqfn7bccb9n1byn5vc6") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.183 (c (n "sctpx") (v "0.1.183") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "013vhvm483ihvpd8d825dp0jsbkdcyrkjqmdraaa5hafsi1w559v") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.185 (c (n "sctpx") (v "0.1.185") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0kp0ih8jzvishi53yb93gh9kqdsbwjb16ym0j6hhx0806kjvxvjm") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.186 (c (n "sctpx") (v "0.1.186") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1bv9dd1v5p4aypv4jvy1rjb0b7f4cvqpzxd0kf15adwl02rcyi45") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.187 (c (n "sctpx") (v "0.1.187") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "10ad3j5aw33sp82clp7cg05w0q9cjdpgmwlsll1pwr0b9jrb5h6r") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.188 (c (n "sctpx") (v "0.1.188") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0jprpsbp0p5khrdw45vmzbhbv7bpgv21wf2b0aiz5wnva796id81") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.189 (c (n "sctpx") (v "0.1.189") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1kinmhs1zx10daasqaivlgdxmp8vm34czr5mhz1l1k8gzfbhfh12") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.190 (c (n "sctpx") (v "0.1.190") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0injf3dl4y4f8d52y6305gsvzrvwy9rcs872psf3gl0vjhj4bxx8") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.191 (c (n "sctpx") (v "0.1.191") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0d7ri9hj0zz0v33diz99bs2hshdnpsdjq2p3r6idg2mabp183fah") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.192 (c (n "sctpx") (v "0.1.192") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0sx5gx6m1pzsvz83gk4csr2db8ip5blpdmmf2q4krbc6vmw801lx") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.193 (c (n "sctpx") (v "0.1.193") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "14gz2sl0xkd5zxc5zkh4ld764k6673dqxkyjhhy1149xfl009p61") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.194 (c (n "sctpx") (v "0.1.194") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "14ndb9kkj9lxb203b3k5q79jkmwb1sya053hc0dqqisx1p05kzz1") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.196 (c (n "sctpx") (v "0.1.196") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1i85yxx6z9ah3gqccbsiw3b15khqh06wvfi58qkliz4ifhm66b6h") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.197 (c (n "sctpx") (v "0.1.197") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "01ph5zylm2zgigjpbdj068xdhbcrk0v49a7ss7hbirilnxcb9nyi") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.198 (c (n "sctpx") (v "0.1.198") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "08rbk9dc49qzvcsvp57vq4i4vqvwmzq8j9mjxzcg517vsnapyhbm") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.199 (c (n "sctpx") (v "0.1.199") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0bgn5c32xwdahln7as4jdk67fkz4r42cmqfzhhmh6vq092v7yx5l") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.200 (c (n "sctpx") (v "0.1.200") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1xj131bjr76ah9xcc8h9fznwy9yhyvwr3ccx4yxm629fimn1dix7") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.201 (c (n "sctpx") (v "0.1.201") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "016rmgka97hswqqqb7f2h75xh9yix2bpqkany50f39r7cflqqjm5") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.202 (c (n "sctpx") (v "0.1.202") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "03vxh87h9jazs96df1ccfc0n5x3hf60sb1f4bhm8lbdkvdkavdgx") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.204 (c (n "sctpx") (v "0.1.204") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1bbd7yzm90vzj05l42z0y3mqvdwyw6qfcfsln6jjdhhim09w4z9v") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.205 (c (n "sctpx") (v "0.1.205") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1xlx530d4jnx846sq0kdjbvngvcf50cgyjd9a0ly04aaad456rsh") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.206 (c (n "sctpx") (v "0.1.206") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0h11drjvhw1wx98qkn2j5ybrskr2ly3x1mlmg2l5zgf7k0106nl5") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.207 (c (n "sctpx") (v "0.1.207") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "103hvwn8mnlcn44w9wrm83q0iz8hr6cdqwcgyzwmm0h0p16jnn5w") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.208 (c (n "sctpx") (v "0.1.208") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1xxwsr9brl5bi10icqk89nglrmq3gq3wbwqkmibgc3gk2k4la8ya") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.209 (c (n "sctpx") (v "0.1.209") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0awhdbkl5ik7gmj1zm8s20ngixizxyr2h9wzg61kkn32wl7jj0hy") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.210 (c (n "sctpx") (v "0.1.210") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1skjdlb9vxmm3s8wcxy8pkz9wv4cd159cvrqi4c8rn9yhcikp78z") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.211 (c (n "sctpx") (v "0.1.211") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1fjcl0kla01xr6c5r46ffgb0kdvhbqnbfv26x4z62sv00skgfp6s") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.212 (c (n "sctpx") (v "0.1.212") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0hbv8mwzd320sn613k47zgn0zlwpaxz8cwjsjk7fdcqs17j82chr") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.213 (c (n "sctpx") (v "0.1.213") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1qw0p7rfnpz6r06b60bndnl8b12jk885ivvcphqcy7g45cm3qh5r") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.214 (c (n "sctpx") (v "0.1.214") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1z2yp8bl842n6jr31pscl89rmnhkm3ny6gacrcgkwby76rm099px") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.215 (c (n "sctpx") (v "0.1.215") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1a9s0dykz7akzw91a8cnarjncqhh05sjcbhbmflsrg64hkipjxrv") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.216 (c (n "sctpx") (v "0.1.216") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1pxfy5n76yv123fkvfy9f679xf20w6a426wyq1i1r9m09h7x26hn") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.217 (c (n "sctpx") (v "0.1.217") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1w5piaw7z856q0qikffk49ma45zb7h14cv9vkf0f96236i30iywg") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.218 (c (n "sctpx") (v "0.1.218") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1i5rrqlip5s4jl922cm92h0h92jvp70vj9lvkfhm40bpscmh266n") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.219 (c (n "sctpx") (v "0.1.219") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1gj8lkdf5dbm9lpfwqnl9v4xv5fdjn269rj7sxdw573qwk8ya2sm") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.220 (c (n "sctpx") (v "0.1.220") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0yhzigzwf88142k2i9vm45jlwbvfmpjzfmjjvqaskyy488bcivb8") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.221 (c (n "sctpx") (v "0.1.221") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "09z4r9z9gnjyz1xf8sqxmi94z9zdzfy5hwysaxij0rr11fxq2rx9") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.222 (c (n "sctpx") (v "0.1.222") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0i946yqcg6pvznh1sp3lwxrj3ik3i9cyslri34zff71hivahg0bq") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.223 (c (n "sctpx") (v "0.1.223") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1yjrhv1shxq0di8djib6cpicq6dv1xflqa9slcji95rr10785sk6") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.224 (c (n "sctpx") (v "0.1.224") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0zx1gqjx00pc0slcmbjk4rawvhxp1knm0b02yiznn9gsf750gbcp") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.225 (c (n "sctpx") (v "0.1.225") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0fgy83hkigr8dq2rjwn5h4cqccjxc8j2vcngw16wdg91vk6kmmy1") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.226 (c (n "sctpx") (v "0.1.226") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "09z22xmlfw1p8nkh2rxaayf44cm0b2w1gbdlcarsk31hag52dpwv") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.227 (c (n "sctpx") (v "0.1.227") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0gyqiyw6d1p5l4rih887548s8j7xrgrwscqflgc94id8a638znar") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.228 (c (n "sctpx") (v "0.1.228") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1wy5r1nk6mjnldjv50fim8aihkvnjsr97fq6j9r9c6pgvwhg8l66") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.229 (c (n "sctpx") (v "0.1.229") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1c2jqrzmqcv1fai8iwm1i6nmvqzvr7bxbrjc73ck13ix3nr2w5sk") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.230 (c (n "sctpx") (v "0.1.230") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0iy3z5cnkvgipg6hz9izyy50j0wfc05bd9vsfj0lz80plfya9gp9") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.231 (c (n "sctpx") (v "0.1.231") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0z92623w8zgsn6jmvg2c324aqcsbswjldbhz44s9birrb7dlglz9") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.232 (c (n "sctpx") (v "0.1.232") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "10qh78qc3ycznm3n185p64dncq8rlghzwgqkajia7n5aar1hr01k") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.233 (c (n "sctpx") (v "0.1.233") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0gc54r79x4zlns7v5q811n53yrihi1aq6882vkanvpcyw93zrflg") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.234 (c (n "sctpx") (v "0.1.234") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0fdqihx7s8h9grzdz0msd1n4hrxvlpbkz65n3c9663m38rifpj6c") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.235 (c (n "sctpx") (v "0.1.235") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0zqkg1q3ph1ql6dc9y1gswr4jmgw9rdxism0asyl4qv8b2v22l75") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.236 (c (n "sctpx") (v "0.1.236") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1jr2ya8vksw5l4xs9l3f0k9cz300cc7zimvpilayh8jw71n99v8w") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.237 (c (n "sctpx") (v "0.1.237") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0i1wwjfmik5nzq4na0gjr073gr7lnniq93vkxbcqmrrlq3mqvwq6") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.238 (c (n "sctpx") (v "0.1.238") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1s7fjwmx4xxbb2rjw0k01c4iis7ngm4x8xcfqdsprwa4f7d2wl79") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.239 (c (n "sctpx") (v "0.1.239") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0givnmz6yzwnyf6ckdrkgij763ih6czwyba8956ywz2kq255x5n2") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.240 (c (n "sctpx") (v "0.1.240") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0926njnk6id2dnp17xzdz7hpn67lv6k7g4zcr859b7n1xzqi6hxb") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.241 (c (n "sctpx") (v "0.1.241") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0dgb14sr7j3sxmgn6xdymjkw0a7q0lvv2fyixwhd6q2yvw2swdyx") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.242 (c (n "sctpx") (v "0.1.242") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0j0rirhkna6zyim58mw6v9l3f59lfzgxxfp4r9xljli1519c1ym2") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.243 (c (n "sctpx") (v "0.1.243") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0bg78w5qk1zjdywfjyyfyk0bj1iipi6bh4xlsdid1sk95pq3vybx") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.244 (c (n "sctpx") (v "0.1.244") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0f10hpdybiaq1151ddw4qqfyvpwj8sb5lx1wgf8q3ar8rchwi7fb") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.245 (c (n "sctpx") (v "0.1.245") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1rl40xgw8586hqqmdrl8zljrgvyppcgbjrqjw45mv4l5i33ilmvg") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.246 (c (n "sctpx") (v "0.1.246") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0ji06zb7c88m5447qybhgyn6rxwp3kzn1ixm8s8pf869fnz1xyqk") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.247 (c (n "sctpx") (v "0.1.247") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0mb56rk8fmykdc3n3nmhyyh5w538vpcakjinfiv0fbh83ciq10zf") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.248 (c (n "sctpx") (v "0.1.248") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0x8srm3d0b8dcwxw4k1jqw7py0haag7j83ydafrwsl5nmw8l7kkk") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.249 (c (n "sctpx") (v "0.1.249") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0yyg1v5xxvmd34933cx17r5f15r35g8i5iac4wxhjkxf71hqc1ds") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.250 (c (n "sctpx") (v "0.1.250") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0rhsd2hjazs8w1xnzx50qlb1nhkiqqrc0f5hnlsrganvby4wgrjq") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.251 (c (n "sctpx") (v "0.1.251") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1b972hmm6vbwvbvhyhwc2zvqvgs6in75bvb9r8nl3v4qh67crhhw") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.252 (c (n "sctpx") (v "0.1.252") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1dy6j67mb9bjpym4bdfyvh6ccc0n2ily2jpim8qs32szph7z0jrq") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.253 (c (n "sctpx") (v "0.1.253") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "02a2cf63k78hlqz8b2b9lpcxfz69gxwmmwsxq70qkfdddjxvamvr") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.254 (c (n "sctpx") (v "0.1.254") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0lzh8zr7if8a6ndgaan9yl3wlc2mamcghp2w6rpw2mjs5vm1cyif") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.255 (c (n "sctpx") (v "0.1.255") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "02j3zbidfmad5m2qbs9gqs16pb84dsaqgjjfyy3sh7agqh8bhnjm") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.256 (c (n "sctpx") (v "0.1.256") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "12ybwj3y1ivpcn8m00jq2s8r5qm5g1cf6hmbpvqc9ibh16yj1q3z") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.257 (c (n "sctpx") (v "0.1.257") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0gid24ybrzy5wkl77c4lxz818x6azgha7zlj2lmifs297b1mzn5g") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.258 (c (n "sctpx") (v "0.1.258") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0r2c5fr8s1j0dwmas061dm1867rqhmwk19rm4pap196x82b7zjdv") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.259 (c (n "sctpx") (v "0.1.259") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1x9crrm7x95i3y1s4979xd8sdk42vf1a2p9rcqi4wlz1kvmz2db4") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.260 (c (n "sctpx") (v "0.1.260") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1jp7l2fj39d1wicqvsf8dfs3c4nwlkaz6qkyk9f7wnj2fc1rbzd3") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.261 (c (n "sctpx") (v "0.1.261") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1gvc240lpyv295nrm2h54kl7czx5ddajmj23zb14l0n67nlm6jl7") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.262 (c (n "sctpx") (v "0.1.262") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "16bbb8p4cqiqamgpch0ppamhfbqj5if18gm8aazzjff29asnryma") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.263 (c (n "sctpx") (v "0.1.263") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0p8nxcn7ipmmvldmbmi9f693dj67n8wc7ppzjh3q4r5s4p7zq8qp") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.264 (c (n "sctpx") (v "0.1.264") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1cmlb6qk879i8sasc4jp5i72yxdahvavjzarg6j1gsb8mzig69pn") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.265 (c (n "sctpx") (v "0.1.265") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0zz9268cjy5cfkzvd3j2b30vbh52gzb597jpcjsz04zl0dawnj5d") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.266 (c (n "sctpx") (v "0.1.266") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "16h0zkk6j1887bq909clswbg1n6fhljihmdmybl5phic8jn9kcl2") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.267 (c (n "sctpx") (v "0.1.267") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0gwfw3wfi94pp7i0mdl4b4jrki6y6kacv22a1km193zcgykk1z36") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.268 (c (n "sctpx") (v "0.1.268") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1hpk10843p7yy2sz4pgilf70vxvk95l4aylk0x6clz0xrpir93c9") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.269 (c (n "sctpx") (v "0.1.269") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0qkxvhd3qy8pfgzhgag9y1abp54ykm1szc5xpk4wwzw8324mw091") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.270 (c (n "sctpx") (v "0.1.270") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1wnkh82w460fj8z2icv7fdkj8p0rzirmpc9ww6aidlgh1hnxdx31") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.271 (c (n "sctpx") (v "0.1.271") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1wrc2kgc3gaxy9dfr6clnl61y50ckkh4rb77cwsj3nfd7nx6nq4x") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.272 (c (n "sctpx") (v "0.1.272") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "106qx6s3cz89a66qdsxfspbfl49afa4rirx71nkqrx6222kjzvbj") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.273 (c (n "sctpx") (v "0.1.273") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1wn5i8nv13xqfcj92hvi6gk0fi99shcrqrxzx7gl03ajw7vbygis") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.274 (c (n "sctpx") (v "0.1.274") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1rxz09c36a4fdj8ifpbq151f46v6n7gw2njg0qawrw2s5ydlqm7b") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.275 (c (n "sctpx") (v "0.1.275") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "05lm7bq5wpbzzzmcw37y9j3jx8m1bpdd2yl1vjmwn027y771j6nm") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.276 (c (n "sctpx") (v "0.1.276") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0ingnw18n6pyqmrxdipfbxkp62h854i0g2hnhml1bgyhrp5h8i14") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.277 (c (n "sctpx") (v "0.1.277") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0rb9bdkx4m5avsxg31zmhzah27fwg1yjplp7wzpr44fxsisyw03g") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.278 (c (n "sctpx") (v "0.1.278") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1nps4r5i89pjmxci3fcdn5xsmqkr8j1rivf5ijyyl4wrq7jdq7ys") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.279 (c (n "sctpx") (v "0.1.279") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0p6zfw9k6ildqbmndpipjk9dxf1qpnld986iqvqvgn07sc2idn4a") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.280 (c (n "sctpx") (v "0.1.280") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0ar3sgfmpfn9nl9a5vpddbn6rz3yqa4miwarpsxyx2fbqp224mzd") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.281 (c (n "sctpx") (v "0.1.281") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1wywdiz57dly4z6hfagfw8bjrjdav6hj2a2aq2lmyrazsr1gmzll") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.282 (c (n "sctpx") (v "0.1.282") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0fm84jmq9hbm129gcmn2ndclgvh1g7j5scc7fd5a8izzgmmjg16c") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.283 (c (n "sctpx") (v "0.1.283") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1dja52rk2fzdz23s1qsbhilbkcpgcv47zr4g26zpgpszwkfpwfqn") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.284 (c (n "sctpx") (v "0.1.284") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "01ri43fxdjh3qihv3xb93w9f49wz888jxdkw08zbqifix9gkxkjm") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.285 (c (n "sctpx") (v "0.1.285") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0lrxb9c7b2rjansa9ci89q8vdhmq3r3p8p9m0i8kf2h8c1kwglk1") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.286 (c (n "sctpx") (v "0.1.286") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "106arj278i2rmq8njb1pv4dhs6qnsxlin5iqlfcn854gs0nm5asv") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.287 (c (n "sctpx") (v "0.1.287") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0kyp32lb8g3xh7alc8jb3yw4y3vgaigq9h6ciab9hqj9pnngdbdy") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.288 (c (n "sctpx") (v "0.1.288") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1ck8svdx2rzwqw1ipazgclql95a2zvakybrcjvgxdxqawwv9m3hc") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.289 (c (n "sctpx") (v "0.1.289") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1d6hriwlchc7kg9b5nzsdx5dmbww9diw1lgbahh33755ais6jdwm") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.290 (c (n "sctpx") (v "0.1.290") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "19zdbw5vwdyb3ip2bv2jxgm3r3cfdihqn1v72p6vra9q7ijbk3m0") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.291 (c (n "sctpx") (v "0.1.291") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1zdbvkkms439rxw9kmcf4lqdq5b2n3dbgsynf44jdq7ksj9y9ibv") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.292 (c (n "sctpx") (v "0.1.292") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1qwd68z0syi9a6jjgv863gvgb24hsph1nzl10g7pm7sjgc9k50fq") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.293 (c (n "sctpx") (v "0.1.293") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0srvaw8p3h5anr49ambd2hpm9hdgwn403i7506yky98zhm7pvkh0") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.294 (c (n "sctpx") (v "0.1.294") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0g9ym6mmkwjkk96h0cqmcazgrs3xvdxa93cfm0xxihy24xpc9aj5") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.295 (c (n "sctpx") (v "0.1.295") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1frmck6zazp16r5kk55lq1fw8srmp3n09ns7sk0i7bsd4pfl5z7a") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.296 (c (n "sctpx") (v "0.1.296") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0gss8dsq78j6jdnnw3la64gnsrsj3s6nyzcasdgr3sx54pgvgblq") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.297 (c (n "sctpx") (v "0.1.297") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1b7pzgv3bfxhrvlak9hl24a15ngj3k4y11yklv2175fldf6w4d1a") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.298 (c (n "sctpx") (v "0.1.298") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1g0lz8s45q86086plfi9qf3jjaf3zs1vnlwx46gnj6nsvbpd36vm") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.299 (c (n "sctpx") (v "0.1.299") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1dnvv7wwrg55c3dv64gjl0h8wpvzd7y8wf3myr0jzcfi1sv271rh") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.300 (c (n "sctpx") (v "0.1.300") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0kfiv43g0196rz9z40807jf0y68jnx7g5b0g0ymw71bp2y4d9x52") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.301 (c (n "sctpx") (v "0.1.301") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1vyjkzjq9wwlwqlkbdr5ygyfasc6vbwij5hisi9n97yhp7lsma1n") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.302 (c (n "sctpx") (v "0.1.302") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1ql3sl6ghh8b2faf4c25z1xdl6sycqj2f1iw9zi2fr8fv4hk32ip") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.303 (c (n "sctpx") (v "0.1.303") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "01jqnk0d5bhfiwrg6vrmf0fwmj1f2bjnjd5sykj3z3fjmz1ascfw") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.304 (c (n "sctpx") (v "0.1.304") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1aa33sj3z78d34vhxilidaadsf0z7yhapah1vwqp3r3y7dhwgikh") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.305 (c (n "sctpx") (v "0.1.305") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "03mppv01y9wd251kl1ccdxfwvqq9vsnkywsw6n9113nzwq2w0xi8") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.306 (c (n "sctpx") (v "0.1.306") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1h3lsrn0yn4qlic53jjf73md4yhifrk2q5cg9jj2zkmcva39dkfz") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.308 (c (n "sctpx") (v "0.1.308") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1m1lnzqnhn0mk0jy1j2ildhxhqrh5wxnxfkx3mbv4w66r6f6wx1l") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.309 (c (n "sctpx") (v "0.1.309") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0g1q6lv1biwdd9mwrk5hjax54h2gcp7gggznx6gmkx52djy00685") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.310 (c (n "sctpx") (v "0.1.310") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0522500z8aidpd3l9s4k4ac397jmp2fi5p75fgfzq48w8n6l1z9v") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.311 (c (n "sctpx") (v "0.1.311") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1y1dd4mav371aqnii1v60fwiq1cwh88iynh7rpx621ln9lj45k8l") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.312 (c (n "sctpx") (v "0.1.312") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1i4q83cwxfnydxwcwpzziy24zaav10qafwxp8yq457jkv7zzc65d") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.313 (c (n "sctpx") (v "0.1.313") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1pz3sw7qcrz5z3pvxzrparh0icm8g7aahpfbycqmwh6y8aranbn9") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.314 (c (n "sctpx") (v "0.1.314") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0jzdr61ip5jag62v9iym3qqi39z2vli0f65yaylyacsjcymi313c") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.315 (c (n "sctpx") (v "0.1.315") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "04dd9sws88jskhcj9s9n876iwkvc2bmp78qcgjcazsr4l7jd9jpp") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.316 (c (n "sctpx") (v "0.1.316") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0zs6f9qrnvpmk539kj5b1q26ivimrk8m0bss9glfsy8dwm090pqw") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.317 (c (n "sctpx") (v "0.1.317") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0s0v3gn75bykg6hsvpq49z6cw238gva4j3qd9kyjbc733zm6dklv") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.318 (c (n "sctpx") (v "0.1.318") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "18hi237ffv62a97b09mw7jpzbkm8c3phcj2sfww0dqi1x7iqi8v9") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.319 (c (n "sctpx") (v "0.1.319") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0js2hr9ng42bxaxhcsmvpggl7ylkarrrgw8v32zd0lq9d5ar26hs") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.320 (c (n "sctpx") (v "0.1.320") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1jpkfg2nr9s0iayhfzrp3iv94ckfd5fzmp7b9wyi1raxdq826p1i") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.321 (c (n "sctpx") (v "0.1.321") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0lz0b6gx0fghsirb1da74ymzyzzmifigqs3qf6vsm92mf3f4az22") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.322 (c (n "sctpx") (v "0.1.322") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "01lq2jfcx5w1myc8l60kj54jw5vfcbjh20b11f0yl3wkmgknnbvh") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.323 (c (n "sctpx") (v "0.1.323") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0v1f5w3c3nil8zl3c4hdl69bjkcy0v54zwv4yj11n6i6c21y7vrb") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.324 (c (n "sctpx") (v "0.1.324") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "06m1cfraywq9d6l1jrd98k6pfdk9fkkfx03qlvqaa0n094ldvj66") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.325 (c (n "sctpx") (v "0.1.325") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0131cj4zh75k3q5bl0p5pvvirdrwr0sf793swb0krjdmxm2957zx") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.326 (c (n "sctpx") (v "0.1.326") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0gkq12bplhi35x9219ngv6d212shdc34s9myxvx3vxsyan4sh5yw") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.327 (c (n "sctpx") (v "0.1.327") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "03ymvripjg8kbv4fjw763nzm9j9882742d0s41gjxdzzhnsmx91p") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.328 (c (n "sctpx") (v "0.1.328") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1623rk6ph6ss4crfxrbffnr8qv57f0sq861jqqk5693frvzck1q9") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.329 (c (n "sctpx") (v "0.1.329") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "11ms83abdlpszpbzrpjk6yhfjpi17kpzknc91s8hh7d30n9w2h53") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.330 (c (n "sctpx") (v "0.1.330") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1vc3b6jgx4y0g1aabx4bir7dbkrkfrk0g3x6ln4s5964jgndq971") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.331 (c (n "sctpx") (v "0.1.331") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1gy2lixy72z8jvb8p59srqvpk4zg93aqq2p5fjh78zkv5sw9x98y") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.332 (c (n "sctpx") (v "0.1.332") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "15jwkghs09d7qai3dwgyyj6rk0qac0a635zzkakrrpb0hyw6avb1") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.333 (c (n "sctpx") (v "0.1.333") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "00m33yr6mz6r8hhf2sanf30g8vvfw196v47bn6qcqkc0i9njn4w0") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.334 (c (n "sctpx") (v "0.1.334") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0a9v9ch2ahayr437jllhba7q0dd893wxxas8xmlzkcvpn88xvigf") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.335 (c (n "sctpx") (v "0.1.335") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0f74fwldly0pjnpf3z8xblg0b318wjir8j5adjcbvykn1n0g7m2l") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.336 (c (n "sctpx") (v "0.1.336") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "19ihw82li08kvq32wp122b3mssc74k247z45d3vs8g04ix0ycmcp") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.337 (c (n "sctpx") (v "0.1.337") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1j9ixnpkkkjnl7lzb7cibhj1a4mfwwn4m0cv6nav05xkjbgmg1xg") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.338 (c (n "sctpx") (v "0.1.338") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1garwrn7silm0w9cs56761c659gk83qzg7f27camsknbw3k9gvp1") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.339 (c (n "sctpx") (v "0.1.339") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0db96kqrhbmb6pi510n7l6sf04200ns5q382x7pw8zjlgd4gbflj") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.340 (c (n "sctpx") (v "0.1.340") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1612dhlghvvjpvzcsv4rg4gpf2x2s2kilv0dji5rnk2bc5q0l4s6") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.341 (c (n "sctpx") (v "0.1.341") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "105xwj7wlahqwa8d96wckx1cmy0sdjmvwc2kg44jfhdks736qwhb") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.342 (c (n "sctpx") (v "0.1.342") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1l4ja10p5hbd1rw3wqpx935skmqskb5fgcgr2fm23qbbr9bbm2qm") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.343 (c (n "sctpx") (v "0.1.343") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "13ibw4y6s9d7py165ddfm5g47jp08xa34sqfgf0c5q265yg4x7p0") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.344 (c (n "sctpx") (v "0.1.344") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "18i9w2hccnxqsi05qk1gla1qj568vdf1mndqkydi6864nzbpy75m") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.345 (c (n "sctpx") (v "0.1.345") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "06njzz5sxxy3ppk3gs0ai26x8abmr1vjy6pn8zcvkkrcy7h8aas0") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.346 (c (n "sctpx") (v "0.1.346") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1si6a0ahsy0zan0x2j1krrj8x1nyy0dwciw032jb7fsz5alqjaxc") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.347 (c (n "sctpx") (v "0.1.347") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0l46v811f7pjjs91v2nba3l2xilbwkwqnh5m1pw0ziczjzj3lhnj") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.348 (c (n "sctpx") (v "0.1.348") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0axf67m17634j350sdhj11cylai8fyzfn7l4xx2nv8bnn48h6i35") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.349 (c (n "sctpx") (v "0.1.349") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0zvp7byd0hphz3m2gny0qfj2g1kh11rcfczxnd3pwda0czpkk1zv") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.350 (c (n "sctpx") (v "0.1.350") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "16gm2ifaksaipzkyd2l594lhad861590xnvs6cwiyrfyn5miqw3g") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.351 (c (n "sctpx") (v "0.1.351") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1skxsxxxywpscy8k7588g3j4g0bb95zp07f635gri2j5hfyb67ap") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.352 (c (n "sctpx") (v "0.1.352") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0lfcxgm310qbv70zn0a4grbgl9b8vvp30j8hf6vlvqrp47pkzklm") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.353 (c (n "sctpx") (v "0.1.353") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0fp25k37jlgdx5wagnvcbm8i4qmgwr1q5d45z7cfxizw2l7bg0rj") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.354 (c (n "sctpx") (v "0.1.354") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "07zk472ygpfb7cqv0sx3l21jwm8kqnvhy42x1dng1r7jjc9d9hl8") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.355 (c (n "sctpx") (v "0.1.355") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0kgpmx9xcymwhc8rvyzs85y2271r1ilvki41169cvqrvfz09qpji") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.356 (c (n "sctpx") (v "0.1.356") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "00kiisgnbqdwmws15481ifq9j7wyai3as8wiah0g9vz40gkvq3pn") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.357 (c (n "sctpx") (v "0.1.357") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0k411j04rqf11vlz94w83q56gjza9bfa9ar7a6kgcjmqhjxn0x3p") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.358 (c (n "sctpx") (v "0.1.358") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1x016mxr6l8h2cxld29xhdxmfzi169s3zj9mc1s6dl4ghigmjkq6") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.359 (c (n "sctpx") (v "0.1.359") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1mb2ds906csmlmr0rf6ixw5d1b1krnr494n4lxkjqh058c785gcx") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.360 (c (n "sctpx") (v "0.1.360") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0wh7vzq0y7413q8r48cl2m5g6aml1gnhcz1ryk016hgz9zq95wvj") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.361 (c (n "sctpx") (v "0.1.361") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0lx1cikd1fq8hy386r6fg21v84hwyll6y2h2mfxjyy7qy50x3bab") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.362 (c (n "sctpx") (v "0.1.362") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0x0mp59znax9qp5wag744clzcilpar273gsg0gxy2vsdd6qhvw9b") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.363 (c (n "sctpx") (v "0.1.363") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1rvkylj5xx4hrv6qjcs1w66inzrj8rlj46wcrwaf0xjk1fl3yw1z") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.364 (c (n "sctpx") (v "0.1.364") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "01wdgx8afz2xqjmca38a1bpv81x28xki2ilnaq687kq391czrdrs") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.365 (c (n "sctpx") (v "0.1.365") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1cds9ga0nnnq0d746gqhxc5j59gbj06ixbsfqsxz2gginzfg9a85") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.366 (c (n "sctpx") (v "0.1.366") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1ldnjgrjq5fxc0m6nydk015kw9kd7ri1spbg63q22bqdi2b05fc3") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.367 (c (n "sctpx") (v "0.1.367") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0halpq3najnqj1i8i0v5n85hyx77ci6b1sdanmimg33c2mxnv0bf") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.368 (c (n "sctpx") (v "0.1.368") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "12w56lpfsvsznkq884fl0wr88w15awjypwqhk77w4pvx4nvry2zz") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.369 (c (n "sctpx") (v "0.1.369") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1kp0d7ak11fmpsrn5pxf5q878wbs5drh9nsyfgaini2ba5xpqfci") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.370 (c (n "sctpx") (v "0.1.370") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0l0xdln4z0x9a749zbxapdal2fp50q0ffmy6ir5a598p220ad24i") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.371 (c (n "sctpx") (v "0.1.371") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1s9fjxf4gzw8j2xlhjl9qixyiim9bnnmrc1jm6hnad1zxsyqzn40") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.372 (c (n "sctpx") (v "0.1.372") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1jf2gklsax3jajc3mrzcpr2hbj1a0mq6wn420s1pls91nf8xqsgx") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.373 (c (n "sctpx") (v "0.1.373") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0ric86j8yj271mmsqx2fpk7a1nvlsf3xpsf5m93kr105xmxjw9w1") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.374 (c (n "sctpx") (v "0.1.374") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1hj92gih7xs0qq70qwaa68hljn77z6rnwbwcs2mg8g5nc7s1qsal") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.375 (c (n "sctpx") (v "0.1.375") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0g1pqiimipwlwy5bziniclikiy2binspngqyz7n34fdxza138dsi") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.376 (c (n "sctpx") (v "0.1.376") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0pm7lsvh7n0krhc3flg1kgr1g0c121hw44h72xkvjymcbllmlzb4") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.377 (c (n "sctpx") (v "0.1.377") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "12hb01aww34slwxr1zrzh462l53p5f7lmw3w4ix74bjbpzrbrwv9") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.378 (c (n "sctpx") (v "0.1.378") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0zwib96blh6pgcnhl4zhgy7bnlw2ss9mcfxcmqi7azp2swcgvkir") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.379 (c (n "sctpx") (v "0.1.379") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0zqp0jh917lbpiqgag382xi1kljs1156hmyr807rqlxnvibm35pk") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.380 (c (n "sctpx") (v "0.1.380") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "15v8yclkh3pksa48kpgvgxv634izd1bhykxlwvk39zcc40h0xg0k") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.381 (c (n "sctpx") (v "0.1.381") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "14r1hz5v3j312ad8d8b0cy5nrnqlhi1jlzgsadq0j8r30kjk5xnv") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.382 (c (n "sctpx") (v "0.1.382") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0azya9j618gdgfmf0pkcxm16av3bgdrmx3ycqdk9xzz3zbm60cli") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.383 (c (n "sctpx") (v "0.1.383") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "02bcamwa20n3iqkchzxpdrs184rqkydxy2s3h7a9yaa7ymdb73nq") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.384 (c (n "sctpx") (v "0.1.384") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1jljvn4mm1ndjbnh316mscx4k6cb69y5cq86djpr05fbnzjfp41r") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.385 (c (n "sctpx") (v "0.1.385") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0zv797wjgfvvhmb5payjdhlph6dhlqk692iw0198g84wfryczgiz") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.386 (c (n "sctpx") (v "0.1.386") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "14mx7gngs81phvicjj9c9db66nb9lr3yjh08jdgi24pla90gpqmn") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.387 (c (n "sctpx") (v "0.1.387") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "127ar85dswg9fi3wb45dhf86f43nnnid4bvhngvna9i8d2swmaq2") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.388 (c (n "sctpx") (v "0.1.388") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0bjbpj6rhxmnapi9dlvfs5cpnblapjypyqrr3xi30k9ik22z8m6j") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.389 (c (n "sctpx") (v "0.1.389") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "17rajs9zp638mfkx33fdqlsr4q20xp8jygkp5zcc3784b0ngck5f") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.390 (c (n "sctpx") (v "0.1.390") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "14mhzih9aynl6cpjj8ff4x5wbhag9di7c9kvmcz1b4hp23s95232") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.391 (c (n "sctpx") (v "0.1.391") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "19hf8qnxkbnnwadli7rl7yn6kjgkcl8y2ajswwk0r7ykwxdvhajh") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.392 (c (n "sctpx") (v "0.1.392") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0nncyan1n373aivnqx7rvyrr2qanfrwv5y9mzlka95x9sm1wnixs") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.393 (c (n "sctpx") (v "0.1.393") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "061i69figkkbl5c3a5w061pm4d18x373s2d4bvx7wax3ypjcnmz3") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.394 (c (n "sctpx") (v "0.1.394") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0ifsqd0bh0y3v33046y4lk590f144nlfrb0b0kmw0nh3bxisx2pi") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.395 (c (n "sctpx") (v "0.1.395") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1i1pd0iwsycmk16pfa2fy723pj2c47qnqzi0zgc0fs6whfa21mwx") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.396 (c (n "sctpx") (v "0.1.396") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "172pdqpaam75ncpy91zrpxzwfdyaf65q9n4y8s3x200rv4yjgz9z") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.397 (c (n "sctpx") (v "0.1.397") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0knfygj4yhifsjqydz4pqxid2pfck75g9kyq3vx1rr0yn1qdc865") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.398 (c (n "sctpx") (v "0.1.398") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0qyi0k3x9cc002vp2hla7hbh3gmglg2acwv6xjz2z7qn5hzbwxld") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.399 (c (n "sctpx") (v "0.1.399") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0mh3ij0i5njfz3clgr16l45isafvs9m9brwfbz8f00iwva2h1mzw") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.400 (c (n "sctpx") (v "0.1.400") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "17z0y8wcgaz1lxi9q5q4kxqmbq57mdzga3la0djpd4bdn50g5nv2") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.401 (c (n "sctpx") (v "0.1.401") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "00d8j82srn28ab902cd0b710s444kmzr5m43xhlzpp5mc7m6f350") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.402 (c (n "sctpx") (v "0.1.402") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0jj52zj0wsd77crcfj3378jmfw4rb9dgd8c4sxlbnah87qn7qhyz") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.403 (c (n "sctpx") (v "0.1.403") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1g0wb0ncq6c7w9qkwfg3ykhvarwk83gan8accxhl7f0rh48ncvag") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.404 (c (n "sctpx") (v "0.1.404") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "19015z5biqdinc497ghbh5nvq0pycli7d3hfipxy59cjdc9lzqq1") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.405 (c (n "sctpx") (v "0.1.405") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "18dkdgxyb8w7a2yirzpj4i9mq5w6adq3ixf1zjjncl4z4sr9b23z") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.406 (c (n "sctpx") (v "0.1.406") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1nh4n464fkjrcrgdf5dkl5d6in5vwwlmckp382w52avkddxgxvas") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.407 (c (n "sctpx") (v "0.1.407") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "07yxs55bl509n07lsv4773svdrv1hkzm2pldw65cljccr9y219xr") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.408 (c (n "sctpx") (v "0.1.408") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0qdx1rp9xw8pnqy8xbn1p962ix65wbdczs8h1ixmgjzr98hv25a9") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.409 (c (n "sctpx") (v "0.1.409") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "12mhkrx7rr9k2bpsj0m7a2ddvc8zpsa2qbv0l8bwl1vl09jwc732") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.410 (c (n "sctpx") (v "0.1.410") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1nfyygd6a6md13m4km201l861441fsrww90rv5fxmqmba0zdcv35") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.411 (c (n "sctpx") (v "0.1.411") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "02nd66wrxql632za64vmhwg17fnp840mzkvfcbardchhf68fcniq") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.412 (c (n "sctpx") (v "0.1.412") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0qjgzr54c9i3x09lwyj8f8vd4w2jr468wc2c2s7r64isw43jvpjy") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.413 (c (n "sctpx") (v "0.1.413") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0h3zbjv4sn90q8i086lfd74s8l2p34lfr4qz81xxf8fj9likdhbv") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.414 (c (n "sctpx") (v "0.1.414") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1slzvbnskbdidc7gyvw92qg103bnpfgkn9hw6mm8qjshs4kn02bi") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.415 (c (n "sctpx") (v "0.1.415") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1rsz4bqs2i923ashkg1nx4s14k8b9b7p726fsmf3mqv3w3ph7fkv") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.416 (c (n "sctpx") (v "0.1.416") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "13r2pmnhd5c36h089kp3pxryzywdbnig1bvshmq9r3icadgmhd93") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.417 (c (n "sctpx") (v "0.1.417") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0zb9lhnv37gkyq9j7xgfbqg6zzd5h0k7nx26sbvn11q0gnixplh4") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.418 (c (n "sctpx") (v "0.1.418") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1iacyhlz768hnckymnwjxzd1s5sw941ki4whpa7d88l5rmj6n19q") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.419 (c (n "sctpx") (v "0.1.419") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1qdin82aik4jyh1shyfx8mlw543ad38frqw5r3xb68kni0iljq5k") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.420 (c (n "sctpx") (v "0.1.420") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0g2qkmyp2cclmdl87h8v5y9divncghgqgbcx20004816b73shq8g") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.421 (c (n "sctpx") (v "0.1.421") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0daya21xcs4w4pbjkv5xyirvi35bwb3ml94fpkkaj0qbfq3jsrri") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.422 (c (n "sctpx") (v "0.1.422") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0kkn0f7pwk1sanndcci6z829hh5mwbyvdylhmwnmi9dj7qcj677i") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.423 (c (n "sctpx") (v "0.1.423") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0dmimsw1xjmw37kikm5sb8w7n001v861y0wbcqzfgac2p7dzdzlb") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.424 (c (n "sctpx") (v "0.1.424") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0vy0xaavz1521zai8mvh6jl02mz9gc7s6fndr8p4ay56y6dcdpxx") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.425 (c (n "sctpx") (v "0.1.425") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0flpwawwj1in82i762894ps50sza3pf634s72p1mj6i1zvskvm4r") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.426 (c (n "sctpx") (v "0.1.426") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0mzc6fk0jg11bg09r5fgyy0x89h4apsp1bnv38n0syvfdkfy367f") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.427 (c (n "sctpx") (v "0.1.427") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0kaas4rjx6jf1c8r52x80vabjzpi79iyhy68s34dcs8n2rkqbsha") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.428 (c (n "sctpx") (v "0.1.428") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0bn0jsz6q2xs99q6zqp2a5yv21655il5nw53jv54b7z1j1r6ag5f") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.429 (c (n "sctpx") (v "0.1.429") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0p16irvi6p3qwgys7m0gmlas705g3zphqfygph74wgxzm3yb7qa3") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.430 (c (n "sctpx") (v "0.1.430") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "14ds23183x6hjfs8kcj282qachkldazqmh9wqfxaq86q0kkwmj0h") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.431 (c (n "sctpx") (v "0.1.431") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0a2ak22pahn8agd8dfrpfrggb03330f585g1aw9693962v06jl8d") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.432 (c (n "sctpx") (v "0.1.432") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "09rxjjhqvg8h9m5fi0qrqcdinngchdyjk660c7p554hckribgzzh") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.433 (c (n "sctpx") (v "0.1.433") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "12vk87isx0zdcj5fbppzhys0jrak42zww6q9zmbivhmv74y6bfjn") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.434 (c (n "sctpx") (v "0.1.434") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0bh4v8h7d30rjz5np0vgk79wafxs63nd2arw5451slxxwm7qwf41") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.435 (c (n "sctpx") (v "0.1.435") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0aba9cfn636cph4mysbw325xshzf3083r68kvglwfbid04jsm3mk") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.436 (c (n "sctpx") (v "0.1.436") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1vlkldcb9hwwqa48cjcd2r6wd2305n5crgz5b94cvj98di693gss") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.437 (c (n "sctpx") (v "0.1.437") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "03fyn2rkkr0lbc7r5f37k03mq2r0bkscalkrwlvci7rbvdyaa5ww") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.438 (c (n "sctpx") (v "0.1.438") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1p412b8y6hbyac7f5zxf6sy6z2i7b19kkpcsnf9v09kancv0hgzd") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.439 (c (n "sctpx") (v "0.1.439") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "009yqr78swndcrjzk0llyjvm3y8wx4lmbqc3ssa1fwakb57iwdjv") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.440 (c (n "sctpx") (v "0.1.440") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0q75gfwd3m933iv5qwk55kgfj4kwcymcs4y4zzlbnw3l8gm5286k") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.441 (c (n "sctpx") (v "0.1.441") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1jl847fwbpdyaf9jis090n8hm5s2sjanf0llc1cigczssd8n8b15") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.442 (c (n "sctpx") (v "0.1.442") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0ska711rn4zqvnn6hq9gzwvppyfq8kd0b1n4ds8d6fl6gkskqi5h") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.443 (c (n "sctpx") (v "0.1.443") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "08fz1cmd1c1pb7cks1zhpjkmah1zqf8i66xsndq691sha89x5mxk") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.444 (c (n "sctpx") (v "0.1.444") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0gyjnpf24qcgvgdzxn89i3n8ybk9wjahqsam72pc8ng8rzwff9lz") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.445 (c (n "sctpx") (v "0.1.445") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "144nh6gsyzzk6fjzi78l78i47ssvkx6nhii5g2mghggsdfvy0854") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.446 (c (n "sctpx") (v "0.1.446") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0ajw8b9lfv6hhn0zz0baz1fvp66mhz01af3d1fdinwd1gm2ccm6w") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.447 (c (n "sctpx") (v "0.1.447") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0018qn2ririh3lgvqxvb3wnprnw1zzi9lzpwqwly5dr474fiqg5s") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.448 (c (n "sctpx") (v "0.1.448") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "166imd0ki4p48m58mq5nrlzjicrpjr5li7r6sy4wmkxkwnfri3mg") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.449 (c (n "sctpx") (v "0.1.449") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "14cda5vdgv4409aml71kmxkbpsmp0plbw41q8bwkyzqm5vjapmc2") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.450 (c (n "sctpx") (v "0.1.450") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0wjv3g31vwwrqnkcr30idxz9ja2makyixp3rhp6sfaarfnc5x4vs") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.451 (c (n "sctpx") (v "0.1.451") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1ri0zxpd6c3czkhj7bsxm30q4hl1pvbm6hjmpni33iw25f5wkqkm") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.452 (c (n "sctpx") (v "0.1.452") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0lb1l08gai8ypzgahn122ff8vnz6znpx13cpcx6a92rllnvdn3p8") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.453 (c (n "sctpx") (v "0.1.453") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1v02x5nwpd1a2wkj6x36i8y4nnabmk80wqn6aaf1v0l8x9vazmv0") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.454 (c (n "sctpx") (v "0.1.454") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1dh3bl7s8daysm3sybn0mn3i305kn20jb0ccdyjkrnlwxjljl59q") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.455 (c (n "sctpx") (v "0.1.455") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1x32bqci0hd9lh3nkbm2lx6jw5r4ypqfkd4j0anw3acabq3l3lx9") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.456 (c (n "sctpx") (v "0.1.456") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0rma0c0hmqqkhy4yzg8grd63w7yyhlz9z05m3dxnz186nxhvhdqx") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.457 (c (n "sctpx") (v "0.1.457") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1gnqk59q4xd3djjkw47xrvhvaq0wd7pb32fx1ss7m2il79az7h76") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.458 (c (n "sctpx") (v "0.1.458") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1sh6m086zy1mmwd4nziqpy5rmrivifa2ay469dzv1sjy46f26ymr") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.459 (c (n "sctpx") (v "0.1.459") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0785sxrfc1ikalvzgx8iq4r08jif8kfaa5px5v1a5nhci61hic9w") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.460 (c (n "sctpx") (v "0.1.460") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1cs0b45g627nbdr288hff2l9pha261g67qjlx4351dbrpxsg2aqh") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.461 (c (n "sctpx") (v "0.1.461") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0vjyca289gaflax8s85f7yx1s9rn2fr2c4zs86h4jwy215g8sbfd") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.462 (c (n "sctpx") (v "0.1.462") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1jlgvrr6kfbzm06vdgcf48af19d26mn67h601q8q3wz69bygjlq7") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.463 (c (n "sctpx") (v "0.1.463") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1fwshz06b4xad6931fnsa5dasfrl3f6prxr3lqzfavh3fjpb3468") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.464 (c (n "sctpx") (v "0.1.464") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0ljajvknr6bd3a9fzabg4j0h012c2srg0vgci94pvb0l03m20ag0") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.465 (c (n "sctpx") (v "0.1.465") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0w6q15ja3p8rskshyx5hsxa3hm40sz85ivg3gcrf4j2rswnh2byd") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.466 (c (n "sctpx") (v "0.1.466") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1vbz1qi57zm6q7x8y7rdixawc2lcw6pg638g0pc1wwpk6pr5f05v") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.467 (c (n "sctpx") (v "0.1.467") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0pvvvnb92b00krvwpd01j6wfn4k9f4wm3kaqi0j2y5179dpygl1r") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.468 (c (n "sctpx") (v "0.1.468") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "14igx6kph9dn8crcjba743q9sh192qa1jrz8l6jlwhib9syzqwcf") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.469 (c (n "sctpx") (v "0.1.469") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "17r31hba7gk35mqd7f287snhaw2v2pqwqrl3p860zddzq0rcgirb") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.470 (c (n "sctpx") (v "0.1.470") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0jrav2m08xw3pw5yi9cax6avdbc6wdc9ci7h8aj5jdlfz71nlrhv") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.471 (c (n "sctpx") (v "0.1.471") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1n5fn3hb1aippa3k9yda469m720dphhwsj88kh2zbpf8nj2nszy5") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.472 (c (n "sctpx") (v "0.1.472") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0pv2ziry5c7678f9cr5m9lgwp3z6bxdihfirgqiwi28k9yjgdrip") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.473 (c (n "sctpx") (v "0.1.473") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0in8gaj905qsq498y185317qi8hp895sx7ggk0ixvnvmyx24bmdc") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.474 (c (n "sctpx") (v "0.1.474") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1j124bh9f8iif1vlv9anzk9fa7hspaan6p15v4b4mv2jjsy8vbws") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.475 (c (n "sctpx") (v "0.1.475") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0dvgndcw0a6fpyil86bdhxc3zsrzy2753in6dv6hky7w2kpifryj") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.476 (c (n "sctpx") (v "0.1.476") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1r99cl8gm8920a5njz4q4xv0w52lmcw63rbxmn7164bn0awlpn0r") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.477 (c (n "sctpx") (v "0.1.477") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0aidig4srwdr50a87f4ykk44h49l79wjwc4k7bp6avwvq39ybx0m") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.478 (c (n "sctpx") (v "0.1.478") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0p5qwaxdql3sh4sz3bcs19rcdq6lrm2yv12i0qbwh0hryhgwqwmd") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.479 (c (n "sctpx") (v "0.1.479") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1x8zk09liqr2wibm8jnkkmgwh3jhma8rz3g7zzqsd6idbbxcgs5z") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.480 (c (n "sctpx") (v "0.1.480") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1jc1y8gvs207zl3yz3jwa6140npwq4q4wdy10hki2n5kr27r1l5x") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.481 (c (n "sctpx") (v "0.1.481") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0xkvx1g6syp95yap6kcx8rccygfm5jk7rlmcmyzn0d51a1byh4q3") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.482 (c (n "sctpx") (v "0.1.482") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0da058wamxydk47iniiss0sk7p7qbz6rwcmr5j4mkf7mq2aayry6") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.483 (c (n "sctpx") (v "0.1.483") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0idf13li0xkrhbwb7p3gr03qhqzs7yhz4ppsqapcbhd4qkrm4kwz") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.484 (c (n "sctpx") (v "0.1.484") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "19jdr19g642sxpcbwkfgk023bjd3ns2xq5v129zdkcgmy0iq6399") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.485 (c (n "sctpx") (v "0.1.485") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0ky9qrhissica1zbf8pxghv7f7y2p6clvk5n2v3yr9870iz0zj4d") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.486 (c (n "sctpx") (v "0.1.486") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0a28ng8yjw4ahxn6xa76888dgsy7w125fzdvgha6dy56ra0jycar") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.487 (c (n "sctpx") (v "0.1.487") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1cp3qfhgf329gd6q9csf6znfpyivzkhmc3fgrqm7xz9683ifsjc1") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.488 (c (n "sctpx") (v "0.1.488") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0s7irah5jk47p20y72aj93vxkplaf3zsk76ywciv81123ndp6jff") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.489 (c (n "sctpx") (v "0.1.489") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1q6x28x4n4iahk5p1calhqkv6w5m3v1dlb1kffzxbijklgylm2hb") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.490 (c (n "sctpx") (v "0.1.490") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "02bj4fhsib2ni60nb6zlm8a70q2f10m3sxzf7f9g8vsrhdgng1b9") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.491 (c (n "sctpx") (v "0.1.491") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "10kgx5s0jnx8pqv948fpk8chk5dc6nxl8nigrgfvamxag15m5avq") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.492 (c (n "sctpx") (v "0.1.492") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0sasgf3a1zky3labsdflwgr2y846p9yd07skb7k3150d5hr8fbv0") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.493 (c (n "sctpx") (v "0.1.493") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1fb0r23igw5a8gdm3rnx49ch89c9xknx666ipmdv60xgkmhbjiqw") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.494 (c (n "sctpx") (v "0.1.494") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0lj0hfa2pa8l4rnwx8w7kz58yf2j6phzpr6rsx9ivracb00slsa0") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.495 (c (n "sctpx") (v "0.1.495") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "11slh9k07zpkgcqgm39v1x6s42r36p3j79cjazc0wmrd159gc75s") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.496 (c (n "sctpx") (v "0.1.496") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0r7pmj1fka8kdjzq3m7r93a3ljngq96lw5hphmpknjck9vnq5pic") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.497 (c (n "sctpx") (v "0.1.497") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "03zb65r0inia89j6m39ci0nzmmzvr7p0baiap0qx9wfsqclzws38") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.498 (c (n "sctpx") (v "0.1.498") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0b745qvinvnzi485shdlgpc8s4laq7hk4g8lrvv6xxwnbnrx43dm") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.499 (c (n "sctpx") (v "0.1.499") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1n6fd78ks1yr2lj62hd56b98pssb2hzz0ri014jgpc2ip7d7fx1g") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.500 (c (n "sctpx") (v "0.1.500") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0gf3yf7h71g9ramxwqpfb17w6qi187bckrqpy5396m81g3zkpmq2") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.501 (c (n "sctpx") (v "0.1.501") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1r34dqk98ls9jpmjyhzz8dfz0lgwqd9f61i4l7c46g5d93imq4m9") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.502 (c (n "sctpx") (v "0.1.502") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1cn84bb8acxhzp6aqvs9780rq2rfqxkqwsy6gqnmw75ykab4gqw2") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.503 (c (n "sctpx") (v "0.1.503") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0s27lxdada0iipvhfwlpglrhgmdrfcq87mibdwfv7ywrapg141sh") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.504 (c (n "sctpx") (v "0.1.504") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0m70n0nsc41kmxs7z0mfj1s67r4hlmsd7hd3w28xcz1znyfjhkbf") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.505 (c (n "sctpx") (v "0.1.505") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1b0nz3fzavnwfd6jhf43vi2qwr6qlzwg9hl7kgy7hlcqn5yympjy") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.506 (c (n "sctpx") (v "0.1.506") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0kqh81rrj4jia8kscvydhivxlimmpq7kkyq19ir8chdc12mn073s") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.507 (c (n "sctpx") (v "0.1.507") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0pw8rsyxnjx5nj6dhrcs27yalrch72fsss825lb4z0dkkawq9mnk") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.508 (c (n "sctpx") (v "0.1.508") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1gzskxhqq3c1kvh0f6wp8y4m7jvz2x1biz6jrp126sj6rd0zhq2d") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.509 (c (n "sctpx") (v "0.1.509") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1wxwg6mpxsj00yyzwhsb23kaq4bwm8i4hqkav3m5w885lnz8pnsz") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.510 (c (n "sctpx") (v "0.1.510") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "07qgvqpaz494jzbh3clspl0fn4260bq8a27c23n3sainh9jr2vvq") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.511 (c (n "sctpx") (v "0.1.511") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "055662nmcfhy8zy6l6mzya42iib8bjd9gsysl7i2dcivxwb7i5rc") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.512 (c (n "sctpx") (v "0.1.512") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0s816pv408jk9pdh58f3pm25ckwa13bcfjdvmxj6nyyb6brzgjmc") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.513 (c (n "sctpx") (v "0.1.513") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0dh2vm1vad0p35pfm7p36d9xn7ha37djqcr1nm7qi7lhy243pdx1") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.514 (c (n "sctpx") (v "0.1.514") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0a7wiby7l8kmz8k065gs8psw93jvz2clwcam5126blsr7y7ay1i7") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.515 (c (n "sctpx") (v "0.1.515") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "04di7dbp657ra6mis0yjnvs19fnw6ib0bx0grvc1h8ny326h12f8") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.516 (c (n "sctpx") (v "0.1.516") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0gmja1h4pjzcxllypr7nq8s1wd778r1jk4s0gm1b4b4b9ddq4ns5") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.517 (c (n "sctpx") (v "0.1.517") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1dfdcxvj5p10z56bn2wxzrcx4d4phv2w6a3bmsk68qnb61fg3cp1") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.518 (c (n "sctpx") (v "0.1.518") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "14wvfayybq588nfa7w79aqdy3arfzcbvckv58n4cfr7p1dgr3yib") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.519 (c (n "sctpx") (v "0.1.519") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1mhnikjj1bkx3hnr0izc06wvmsanq6wl648srapnzr32mlqab274") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.520 (c (n "sctpx") (v "0.1.520") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "16s0hbppz996l033q17c1xzghrz4b89y2r161s1ri46c318sk985") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.521 (c (n "sctpx") (v "0.1.521") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "13n11v5dliaqbdf0nycy0f95vcbad93pvrasvhlfrahf6243smws") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.522 (c (n "sctpx") (v "0.1.522") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0l1v6wn3gkr9gczpzqjd2wrpxxh66922y3235hbqii8k352ys29q") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.523 (c (n "sctpx") (v "0.1.523") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0n2y16rmm08dk0cnrkakihhcypnasmw91dl98hlklp0lsrafqjqx") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.524 (c (n "sctpx") (v "0.1.524") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1hxs44xsfls675chld4dp5fq4kc3w4qbs0a0154dgvmvlqcqydsb") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.525 (c (n "sctpx") (v "0.1.525") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1474rm7x2mphsqfsnicz86v7nh1yfnii5ywn4ga9kl2y4vhi2f7l") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.526 (c (n "sctpx") (v "0.1.526") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "17rflzb293m09c3k2kw6hx9a539fz1mp8gwx1j87610nsn5ry87y") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.527 (c (n "sctpx") (v "0.1.527") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0d6ndlvifhq2pnbzlq9zxv6vsvrpp5mm92yz94blbb76kan0lyh6") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.528 (c (n "sctpx") (v "0.1.528") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0dwrj8i2dwr7i89xf72hcj6pibzc74xrbfls0gv6s44fvz7k3s7b") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.529 (c (n "sctpx") (v "0.1.529") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1xyxgl2kywqnmfd0vmzqwi7v2md8ryx86jmvls6zwd1mg5hg7dvl") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.530 (c (n "sctpx") (v "0.1.530") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0gy8x9x9f55yg1alwjjvsbk4py83rz194r7g7r9fid97qnn4zynx") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.531 (c (n "sctpx") (v "0.1.531") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "05x9wnn62n71a54905zww9495kkrn52g19jf6fdpay31662js1s4") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.532 (c (n "sctpx") (v "0.1.532") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0ia2impimd8rfsqjixnkkl3rk45jq6gwq045qdwcxi8sh8wl0kd7") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.533 (c (n "sctpx") (v "0.1.533") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0zyhcn4x44y6h4b5qsq4jyar0lh3scx9xlpqf67b79rqbnyn5acd") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.534 (c (n "sctpx") (v "0.1.534") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1vrk14qky9s9003f1sal3pqh2bn7rifkqyi4v44w22ys3xr6hbv8") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.535 (c (n "sctpx") (v "0.1.535") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1wbyxl3c6z1mn9gnqnq1zw83dd9pa1y25ib86j2zymdgfqnmqf3i") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.536 (c (n "sctpx") (v "0.1.536") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0dq39p2lndcy18mf4i3za6slrwq9zwwrybaaa9nvyfmwwsi32nwx") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.537 (c (n "sctpx") (v "0.1.537") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "01xb0x3c23pjz4jp74r4sxfdk2k9mq6avyk8gk2fhf5v5h28fqrb") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.538 (c (n "sctpx") (v "0.1.538") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0h0kmwqmygp43gl71kkal7ffkps2f04sv4vs594wavshki3hl9zv") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.539 (c (n "sctpx") (v "0.1.539") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "12l0krwngrfg0j8ngn8f5v92qn2838d9jqsbww3w9w6d3rxli866") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.540 (c (n "sctpx") (v "0.1.540") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0wayzid7n5j34rhh8fb7hdd04qadhd72xs3l8j2g4cf1nwj153n0") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.541 (c (n "sctpx") (v "0.1.541") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0cwmynjdvim54ic5p82xkl4w5n8kpbm2nm8a49pk7b20k2v6c45l") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.542 (c (n "sctpx") (v "0.1.542") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "06apnlshgl7p7k402s2ja77ln43gxfwdrhn5qv5pdckzpawiwvpr") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.543 (c (n "sctpx") (v "0.1.543") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1snybqg6sk6pwjmdxmjxnp5asf16cxymvyhxdrzhd3gs12p94aac") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.544 (c (n "sctpx") (v "0.1.544") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1nwdrr5fygaf2vv5sfs5d6am01yaxkg91bhdb1fbwy7gqx49hygv") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.545 (c (n "sctpx") (v "0.1.545") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0q35ims050p30lprk7agmhgqkwvcwr21hy8h98xvw6y332fjxglk") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.546 (c (n "sctpx") (v "0.1.546") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0h5l12blw4f18f50z35iwfw6li3qx18ivjyn7198f5r8ykanygik") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.547 (c (n "sctpx") (v "0.1.547") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "01v6rm1p0yvf5qld098i39qai3wr3f4fmkybx5csvg3fvx0yw9ma") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.548 (c (n "sctpx") (v "0.1.548") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0zjcfjgh6kdk7b2f65g7b136v3jq8i77hf7j4zypw8f9rvhq55cm") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.549 (c (n "sctpx") (v "0.1.549") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0l9m10p20zzmsclrv7vwr3p6r17ddcvnkcnh46ilw0xgyyi2b5d7") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.550 (c (n "sctpx") (v "0.1.550") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "09arabps8xd9ispn8vfsq2w2y1yc5apkw8d6afs65zv3gaybm5ma") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.551 (c (n "sctpx") (v "0.1.551") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "16pqfl959ql0cg1w5md5k4ywdnmhf4lyd7babmxnaph9l175spsf") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.552 (c (n "sctpx") (v "0.1.552") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1f0fgwf2vdqki1qndimfadjrxqy46mxbvivsm0kzs3qqzarjfavj") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.553 (c (n "sctpx") (v "0.1.553") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0kja5s788f73mw6mxy2dz3m3gcrxy0jykyqxryb2sv6xgirhj8im") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.554 (c (n "sctpx") (v "0.1.554") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "01zz8kzqfmzspfd149svfiw15j7ahk0m5vfjcsql15ib23gy88nb") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.555 (c (n "sctpx") (v "0.1.555") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1vjr83i20d4ya7wq1y047z8hrakklfqapvqzd6b1m2vr45gjsvqp") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.556 (c (n "sctpx") (v "0.1.556") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1s932ipkgqsvnfw3msjvxwirg89mbqb8xla362x89ncgkqh46pb4") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.557 (c (n "sctpx") (v "0.1.557") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0mv5k72crqhb9a3hwrkw7ghv88pp0qhsvcy8b2r58iwlmh6f2vca") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.558 (c (n "sctpx") (v "0.1.558") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0m5ybsmd5cx1ygq524xippgz8vf22f0r2p7nkwsfiwisdf3jmwmj") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.559 (c (n "sctpx") (v "0.1.559") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "03ndiz4szbn92d8989dshbprcm6r28h44yrhp2cx64nr26a3allg") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.560 (c (n "sctpx") (v "0.1.560") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0gm403j8b4a4x7y8diyvj6lvy8mzr9cf8vnfr6fq773mv8hdfzh5") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.561 (c (n "sctpx") (v "0.1.561") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1c7rblc7blbk95pqg4j39cshvkfra165zqdrpl5as3nv61fzk8lg") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.562 (c (n "sctpx") (v "0.1.562") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0zghqq5443rs525bvbb99gqap0dzqsvsd590c714m27lz1s8hd0j") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.563 (c (n "sctpx") (v "0.1.563") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "061062fbzvry0na0qcr7s444vzry80zb1kh7pq3kl2c62rbv35q5") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.564 (c (n "sctpx") (v "0.1.564") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1k3mk39b5ngnw35mvdr3w05lrgq5hschl1pqjkswpw6i2f0c3cpv") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.565 (c (n "sctpx") (v "0.1.565") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1fc8pfzxmz6sv8fm98pnah04g7cfi88jp64dkk0638qqzpvjannp") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.566 (c (n "sctpx") (v "0.1.566") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1wsh3gpdi4xw6cadg74mg2307ibj2bw8nxmx1qq2mppwy37rfkc2") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.567 (c (n "sctpx") (v "0.1.567") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1hcjmw5mbr9lm0ydbhz896w2y1brd18apv4s5c12z3915fr7wfda") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.568 (c (n "sctpx") (v "0.1.568") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0b6gsp9shvbiv9gxx3g45mzdqwibn325v5k3ffn280w89ckvz7wl") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.569 (c (n "sctpx") (v "0.1.569") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0g5abc2mnxpi3byi9j6dhbzifkrkx0bv4adnll2i32lq0q5jxdam") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.570 (c (n "sctpx") (v "0.1.570") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "14xgj6vx1r05qqbghm6gs42fvb87hd5wk4wkzfgx48mkdxpx0n5v") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.571 (c (n "sctpx") (v "0.1.571") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1zz6pv2dn9j14qm5jplnv3hm850q4qacly9v36ivzg07rdjnglrp") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.572 (c (n "sctpx") (v "0.1.572") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0w4nhy0l1an2nr5jgpsx46y3n5asqa4yqdmj479lnj1f1a6k7ihw") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.573 (c (n "sctpx") (v "0.1.573") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0avkcs97b4c0lsxj8m3mpb479glsq08s503xix29l9kd7ia135l4") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.574 (c (n "sctpx") (v "0.1.574") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1nzn1z4rb9lxa0ldrvd3wc1ji6w9rzvali6i4r835awyxc2c86p7") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.575 (c (n "sctpx") (v "0.1.575") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1prp215cihffiyg9j2avmi2323fb1aijgk852x9rm8xajdzi64y7") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.576 (c (n "sctpx") (v "0.1.576") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "13h9c9b128b4l9bqckq3wfh4j5hppnq72lyh7x07ng1l67nms3v8") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.577 (c (n "sctpx") (v "0.1.577") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "073wh6qyssg9mqr9dii65vb6j7pssqd4vbwkmp3f6w6254vvnmkh") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.578 (c (n "sctpx") (v "0.1.578") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "05ppw3yahazyw7xlaxbi7xsqcgapvs371f7pz77f554xz0lp2lyq") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.579 (c (n "sctpx") (v "0.1.579") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0g8sp62g3zyc5ljq1278y7s5bx48zx2mw54m6fd6mm5grkdjsyd2") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.580 (c (n "sctpx") (v "0.1.580") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1km0sm7vj05db1fi1m7cvh6mmydp3qlh1drflg7gkcypv2mn0ld4") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.581 (c (n "sctpx") (v "0.1.581") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1nxkm2h3kd9sjbap8pq60x4drb7ai13zksy288qr34dpfzk6q785") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.582 (c (n "sctpx") (v "0.1.582") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1mcqiqbkknc1mlwymzbcn5i3l604i011rk6za82q4ybmyhrdpxl4") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.583 (c (n "sctpx") (v "0.1.583") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0i1j4g8di5kzkkdqhc9vssc3f8233zjkiqsj5crg8x4w6p1vxr9l") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.584 (c (n "sctpx") (v "0.1.584") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "133902vd8xa0m77nwn3klnnj68cxslf33a0nbq7435gw7i9gfqpz") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.585 (c (n "sctpx") (v "0.1.585") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0pjszr3wy1794lyc0sgfy40rn0hg60ccdc3zg0r3bkxcl8x1x90y") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.586 (c (n "sctpx") (v "0.1.586") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1z259zsjid40a4ybdc5jss9q7b9jfwxr3cnajn569p08d6pbj0vi") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.587 (c (n "sctpx") (v "0.1.587") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0z976gdgman9raak2mmc1jvmqrwkaiwjbmwv7n6i3m0vw8jkprx1") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.588 (c (n "sctpx") (v "0.1.588") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1gg7h2m8i6vlvn7j79k3sbfgvg28g5r5jwz261v2l23wn7slqrvz") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.589 (c (n "sctpx") (v "0.1.589") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0931nyakxdld8jij2wgjhwjvn8y0a4fdnmbd1q9gsm9s9gw68n3p") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.590 (c (n "sctpx") (v "0.1.590") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "18hxc7z006q3ac8mdf5kpd9pzfhrysmz81wb6gagvs2923w1vl6w") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.591 (c (n "sctpx") (v "0.1.591") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1dnknrk6g6q747jc2715m411ncld564cz0jbaggb1zdjws44hwxz") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.592 (c (n "sctpx") (v "0.1.592") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1w767z4xqvcg1l3s96zd1apbxp4r4ps8990pvqdqk0fax3kp32kx") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.593 (c (n "sctpx") (v "0.1.593") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "02kfykvdw5ff0wsdsngj82v48w9fjzw8vw8nibnbjcri6k95iyv4") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.594 (c (n "sctpx") (v "0.1.594") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1j70ddkbjlcry5faabdvcf53gly03q86h7s549lsx5laqz81ic2p") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.595 (c (n "sctpx") (v "0.1.595") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0jwala24kz0jmsxdbxaqwfcd0kpwbdxcvn2r1m8h0gyh9n64vh8h") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.596 (c (n "sctpx") (v "0.1.596") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0rpsv3amhwp5hqxx8rkk3dcrhgm0yy2sx5dxxcv6l9nwzkil7s37") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.597 (c (n "sctpx") (v "0.1.597") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0vmndm4im7pzcmyh3an19sczy6r12bawnr672c7lrcmk6bcqxb21") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.598 (c (n "sctpx") (v "0.1.598") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1mdygrvr2n0w18w68qjdqc91cqifxk9dms1rw2ac9yvgih6ygah8") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.599 (c (n "sctpx") (v "0.1.599") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1v7pq1rh2x04blmda55cw71csi51mf1mf77xay87kgpawws1lwca") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.600 (c (n "sctpx") (v "0.1.600") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1dqadi5fyqidxrw2c3skbayafqxfcnvgb8jlsgfz784hap01n5h1") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.601 (c (n "sctpx") (v "0.1.601") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0223q5i3rjy01j72abpzfz55pmmrprlgdbn4nnij0svvgnpdyimd") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.602 (c (n "sctpx") (v "0.1.602") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1a5cnj8wa0cfm4q4mcchjmca23vi7n9c3fhasp0012h5mrfr52fw") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.603 (c (n "sctpx") (v "0.1.603") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "001s60filflwp2z6lfqldhm99h9hw948cc23m47yspgy0vmbzq0f") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.604 (c (n "sctpx") (v "0.1.604") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0shwf61sfzkawqsv499rdx626zipl65hz82vnwi0p71kw61ll9lf") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.605 (c (n "sctpx") (v "0.1.605") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "01m19j3czw1570f29v0q5h6kwbjq1b1b87bk3whl5isvc65ab4fi") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.606 (c (n "sctpx") (v "0.1.606") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "15v787jp5lxakpmbsjfs8lxp812ixrmrbbb0pqbawn8fsxrq0hfn") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.607 (c (n "sctpx") (v "0.1.607") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "08wivjq2y0bzi9hxlq6lz485pmrdld32dg6nkvy1hyx1n06cdcag") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.608 (c (n "sctpx") (v "0.1.608") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0vmbv9y46j5sz0s3mbgi8nniyz35lwgbb7lzmpas24cjxsw3l49y") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.609 (c (n "sctpx") (v "0.1.609") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1qix3373wb43f7zzb3bl5izdafx1jvwdkbjhsxks2bb0372i7lwc") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.610 (c (n "sctpx") (v "0.1.610") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1547djs89ih1g3s5bxh16awc083jv059jp89858d5l67b7mbn9wk") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.611 (c (n "sctpx") (v "0.1.611") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "05nwnb7qqh0i5nn4kpc8fw8iq9n4ymxxkrkn0kaacblmll0ns7sf") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.612 (c (n "sctpx") (v "0.1.612") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1i07g85ci511a0xddppdamiv52250wf2c16d1p1s41blbs4f42c2") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.613 (c (n "sctpx") (v "0.1.613") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1xcgs55ys59crfphf8xrp3llnz7l0x9xlaybzkdkiv1dq8mj8lwc") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.614 (c (n "sctpx") (v "0.1.614") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1y52sf19nxgqsaxv4y73blpnz3c05w83f5ags6fdb67pm5ax3l95") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.615 (c (n "sctpx") (v "0.1.615") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0gznz31xh0n2qbxcdyaxb6pwg91fy9d30jqrj6q2dchax3cd3zxm") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.616 (c (n "sctpx") (v "0.1.616") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0jyirj9jmg5dwsdpm7q47xkj3mq4kg7n9ykj8jykvdcb6smnp74n") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.617 (c (n "sctpx") (v "0.1.617") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0d1m1rdvfg35qj05m3mnp9axxmhwp5la8dbczzkq0kwlsl8rha7d") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.618 (c (n "sctpx") (v "0.1.618") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0xwb8y1blzpfxmp1vgs1l4pmdkkngi0chb6w1snpgwgc57csanzl") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.619 (c (n "sctpx") (v "0.1.619") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1jwjfyzn3pfw0vkwm9w0g3y79p81v1fgrmpws9qp6zr2mn3d505k") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.620 (c (n "sctpx") (v "0.1.620") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0yhr3511yf46v7r5n73a405581vn1b2irwg22h3ad6vj6y40nhn9") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.621 (c (n "sctpx") (v "0.1.621") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1yz5wrz0flc0ivdmpz4jb4ysh6zsrddjy75z84my35r075bf6zzy") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.1.622 (c (n "sctpx") (v "0.1.622") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0pwgzpkbnn7xwqi7dax535p17702drgpkhb8hj9ravzk1wl5jzwh") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-sctpx-0.2.0 (c (n "sctpx") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.26.1") (d #t) (k 0)) (d (n "ruc") (r "^3.0.1") (d #t) (k 0)))) (h "1ksgyfwh2n59mzxi2w7hwawfci5zwdb301s7szbbn9jbz66w57xs") (f (quote (("server") ("default" "client" "server") ("client"))))))

