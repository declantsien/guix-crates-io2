(define-module (crates-io sc tp sctp-rs) #:use-module (crates-io))

(define-public crate-sctp-rs-0.0.1 (c (n "sctp-rs") (v "0.0.1") (h "1slq64364hi2cj92d2i89710m88brp95w2p7298iw2y9rabl33ai")))

(define-public crate-sctp-rs-0.1.0 (c (n "sctp-rs") (v "0.1.0") (d (list (d (n "clap") (r "^3") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "os_socketaddr") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("net" "macros" "rt"))) (d #t) (k 0)))) (h "0lp9cricd9p5v34ghz1r7ns61prfkfh28sil95zlaacnwg5ck6yh")))

(define-public crate-sctp-rs-0.1.1 (c (n "sctp-rs") (v "0.1.1") (d (list (d (n "clap") (r "^3") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "os_socketaddr") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("net" "macros" "rt"))) (d #t) (k 0)))) (h "0j5an4i73j63scv6gpx18pwl9hkxd0b0ahifcalhyw4iv69f7aca")))

(define-public crate-sctp-rs-0.1.2 (c (n "sctp-rs") (v "0.1.2") (d (list (d (n "clap") (r "^3") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "os_socketaddr") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("net" "macros" "rt"))) (d #t) (k 0)))) (h "03gl07yig6l3nvgnhv3f29vblb67z0psi7hj60pfzdvs7fdkc1xy")))

(define-public crate-sctp-rs-0.1.3 (c (n "sctp-rs") (v "0.1.3") (d (list (d (n "clap") (r "^3") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "os_socketaddr") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("net" "macros" "rt"))) (d #t) (k 0)))) (h "16kdwafnh73lmaaq82qckhavaavnixqpm12bgmlf43zlvjfl7v7c")))

(define-public crate-sctp-rs-0.2.0 (c (n "sctp-rs") (v "0.2.0") (d (list (d (n "clap") (r "^3") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "os_socketaddr") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("net" "macros" "rt"))) (d #t) (k 0)))) (h "17islqzy4z8iwczw17863wgv6n648nrz5zdf0sf3a6raz50cglgv") (y #t)))

(define-public crate-sctp-rs-0.2.1 (c (n "sctp-rs") (v "0.2.1") (d (list (d (n "clap") (r "^3") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "os_socketaddr") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("net" "macros" "rt"))) (d #t) (k 0)))) (h "11qb6nscayy2ax1ak1ryjf7dpmjg2azn1gmia6rjjva5lzm0c2xw")))

(define-public crate-sctp-rs-0.2.2 (c (n "sctp-rs") (v "0.2.2") (d (list (d (n "clap") (r "^3") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "os_socketaddr") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("net" "macros" "rt"))) (d #t) (k 0)))) (h "0sqiz9radns3yd5s7blk4f43q6vm2pr6v9s7ann1vkqphg3dzv2a")))

(define-public crate-sctp-rs-0.2.3 (c (n "sctp-rs") (v "0.2.3") (d (list (d (n "clap") (r "^3") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "os_socketaddr") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("net" "macros" "rt"))) (d #t) (k 0)))) (h "0znsv05w7pv61xv4757kmgk1jynj20f53fj1vv5q9cnblva2w2hv")))

(define-public crate-sctp-rs-0.2.4 (c (n "sctp-rs") (v "0.2.4") (d (list (d (n "clap") (r "^3") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "os_socketaddr") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("net" "macros" "rt"))) (d #t) (k 0)))) (h "1l02ssiypbqsms6n46szwwnixpakargyqgn802xpfaybnfphhr9k")))

(define-public crate-sctp-rs-0.2.5 (c (n "sctp-rs") (v "0.2.5") (d (list (d (n "clap") (r "^3") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "os_socketaddr") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("net" "macros" "rt"))) (d #t) (k 0)))) (h "13gq8qkkmv6dfw0fai7c7yagcsjhwg0c6bwanzkyb082lklhg6dm")))

(define-public crate-sctp-rs-0.3.0 (c (n "sctp-rs") (v "0.3.0") (d (list (d (n "clap") (r "^3") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "os_socketaddr") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("net" "macros" "rt"))) (d #t) (k 0)))) (h "09rdw77xmpyl5y6y678ld5zrc43nlizvndqd9xvx22cja0p3cjk1")))

(define-public crate-sctp-rs-0.3.1 (c (n "sctp-rs") (v "0.3.1") (d (list (d (n "clap") (r "^4") (d #t) (k 2)) (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "os_socketaddr") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("net" "macros" "rt"))) (d #t) (k 0)))) (h "1if1z4h37hsg220yrv92p1wlj2x0kld4253hs9ghgn3vcwyff9by")))

