(define-module (crates-io sc tp sctp-udp) #:use-module (crates-io))

(define-public crate-sctp-udp-0.0.0 (c (n "sctp-udp") (v "0.0.0") (d (list (d (n "libc") (r "^0.2.69") (d #t) (k 0)) (d (n "proto") (r "^0.1.2") (k 0) (p "sctp-proto")) (d (n "socket2") (r "^0.4") (d #t) (k 0)) (d (n "tracing") (r "^0.1.10") (d #t) (k 0)) (d (n "windows-sys") (r "^0.36.1") (f (quote ("Win32_Networking_WinSock"))) (d #t) (t "cfg(windows)") (k 0)))) (h "069sv74ha2jhdlxw9b394vxacg9qz4n9y5r3yknlalicc3zjw15y")))

