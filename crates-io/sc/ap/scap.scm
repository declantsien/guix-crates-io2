(define-module (crates-io sc ap scap) #:use-module (crates-io))

(define-public crate-scap-0.0.3 (c (n "scap") (v "0.0.3") (d (list (d (n "apple-sys") (r "^0.2.0") (f (quote ("CoreMedia" "ScreenCaptureKit"))) (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "bytes") (r "^1.5.0") (d #t) (k 0)) (d (n "core-graphics") (r "^0.23.1") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "cpal") (r "^0.15.2") (d #t) (k 0)) (d (n "dbus") (r "^0.9.7") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "hound") (r "^3.5.1") (d #t) (k 0)) (d (n "image") (r "^0.24.8") (d #t) (k 0)) (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "pipewire") (r "^0.8.0") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "screencapturekit") (r "^0.2.3") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "screencapturekit-sys") (r "^0.2.3") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "sysinfo") (r "^0.30.0") (d #t) (k 0)) (d (n "tao-core-video-sys") (r "^0.2.0") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "windows") (r "^0.52") (f (quote ("Win32_Foundation" "Win32_Graphics_Gdi"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)) (d (n "windows-capture") (r "^1.0.59") (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "026p9fq4hj63a1mlilx7l24laf8kq1vlsq7l6ns0v7bx6fdmcv6z") (r "1.71")))

(define-public crate-scap-0.0.4 (c (n "scap") (v "0.0.4") (d (list (d (n "apple-sys-helmer-fork") (r "^0.2.0") (f (quote ("CoreMedia" "ScreenCaptureKit"))) (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "bytes") (r "^1.5.0") (d #t) (k 0)) (d (n "core-graphics-helmer-fork") (r "^0.24.0") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "cpal") (r "^0.15.2") (d #t) (k 0)) (d (n "dbus") (r "^0.9.7") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "hound") (r "^3.5.1") (d #t) (k 0)) (d (n "image") (r "^0.24.8") (d #t) (k 0)) (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "pipewire") (r "^0.8.0") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "screencapturekit") (r "^0.2.8") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "screencapturekit-sys") (r "^0.2.8") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "sysinfo") (r "^0.30.0") (d #t) (k 0)) (d (n "tao-core-video-sys") (r "^0.2.0") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "windows") (r "^0.52") (f (quote ("Graphics" "Graphics_Capture" "Win32_Foundation" "Win32_Graphics_Gdi"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)) (d (n "windows-capture") (r "^1.2.0") (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "10a87b4xv2phf4qirh33nx3qi6sivzqhzvglpfz7mlhimb6bzp19") (r "1.71")))

(define-public crate-scap-0.0.5 (c (n "scap") (v "0.0.5") (d (list (d (n "apple-sys-helmer-fork") (r "^0.2.0") (f (quote ("CoreMedia" "ScreenCaptureKit"))) (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "bytes") (r "^1.5.0") (d #t) (k 0)) (d (n "core-graphics-helmer-fork") (r "^0.24.0") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "cpal") (r "^0.15.2") (d #t) (k 0)) (d (n "dbus") (r "^0.9.7") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "hound") (r "^3.5.1") (d #t) (k 0)) (d (n "image") (r "^0.24.8") (d #t) (k 0)) (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "pipewire") (r "^0.8.0") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "screencapturekit") (r "^0.2.8") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "screencapturekit-sys") (r "^0.2.8") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "sysinfo") (r "^0.30.0") (d #t) (k 0)) (d (n "tao-core-video-sys") (r "^0.2.0") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "windows") (r "^0.52") (f (quote ("Graphics" "Graphics_Capture" "Win32_Foundation" "Win32_Graphics_Gdi"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)) (d (n "windows-capture") (r "^1.2.0") (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "1ykhmgh1zzgwkcr96jyw3s8vqd0qnih4x843yc6sqda854ia2nc7") (r "1.71")))

