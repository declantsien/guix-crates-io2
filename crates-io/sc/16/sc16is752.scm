(define-module (crates-io sc #{16}# sc16is752) #:use-module (crates-io))

(define-public crate-sc16is752-0.1.0 (c (n "sc16is752") (v "0.1.0") (d (list (d (n "embedded-hal") (r "=1.0.0-alpha.8") (d #t) (k 0)))) (h "0psz7rsvl5xdps34zgf1vva13v2y0c5yrzj0ldmg53sjw0rbkgi5")))

(define-public crate-sc16is752-0.1.1 (c (n "sc16is752") (v "0.1.1") (d (list (d (n "embedded-hal") (r "=1.0.0-alpha.8") (d #t) (k 0)))) (h "0kb6ipls929vf7jlgllax1xxhz431xxh3ldgf581xxdwmp9c3dfg")))

