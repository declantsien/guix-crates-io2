(define-module (crates-io sc p_ scp_username_module) #:use-module (crates-io))

(define-public crate-scp_username_module-0.1.0 (c (n "scp_username_module") (v "0.1.0") (d (list (d (n "askama") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.55") (d #t) (k 0)))) (h "1jp5rrqjvbcdz28bar4b31i1wrcmcqs0wnkcixa0lpjbkck1nazm")))

