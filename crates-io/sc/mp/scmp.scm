(define-module (crates-io sc mp scmp) #:use-module (crates-io))

(define-public crate-scmp-0.1.0 (c (n "scmp") (v "0.1.0") (d (list (d (n "bindgen") (r "^0") (d #t) (k 1)))) (h "1d9qz7nrm7nfi4ry1czjdjs6xcpn45w5rj6lhdb8ii6cn5qnpc1m")))

(define-public crate-scmp-0.1.1 (c (n "scmp") (v "0.1.1") (d (list (d (n "bindgen") (r ">= 0.26") (d #t) (k 1)))) (h "1nbrkghn4zgy2llsliryihds6h90igf7iv5nlkf386xpg0aj8bxx")))

(define-public crate-scmp-0.1.2 (c (n "scmp") (v "0.1.2") (d (list (d (n "bindgen") (r ">=0.69.4") (d #t) (k 1)))) (h "0cpl0wsj83mzmpgzr8gd7cc7cf7yrwisr8q38qlgha49w70zpnv7")))

