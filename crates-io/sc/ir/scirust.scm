(define-module (crates-io sc ir scirust) #:use-module (crates-io))

(define-public crate-scirust-0.0.2 (c (n "scirust") (v "0.0.2") (h "0fw36n3w8mckhhps9059annik8121xf6faif818whczszv17n2r0")))

(define-public crate-scirust-0.0.4 (c (n "scirust") (v "0.0.4") (d (list (d (n "log") (r "^0.3.1") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)) (d (n "rand") (r "^0.3.7") (d #t) (k 0)))) (h "1r9ndrgy2w3pv01477ml824sq2w9hjays3i6nvbcw9rxv8ydm11s")))

(define-public crate-scirust-0.0.5 (c (n "scirust") (v "0.0.5") (d (list (d (n "log") (r "^0.3.1") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)) (d (n "rand") (r "^0.3.7") (d #t) (k 0)))) (h "1sj1p091da0mhi85x7k2ssprm49cqbxv1597v1kg0jg6mik8diws")))

