(define-module (crates-io sc il scilla-parser) #:use-module (crates-io))

(define-public crate-scilla-parser-0.1.0 (c (n "scilla-parser") (v "0.1.0") (d (list (d (n "lexpr") (r "^0.2.7") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "12waxvl8wiyksz9id329a7521yssjr8z319imln6mxwsz4yg0hhd")))

(define-public crate-scilla-parser-0.2.0 (c (n "scilla-parser") (v "0.2.0") (d (list (d (n "lexpr") (r "^0.2.7") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "02icrpla564vwl3xyvr4l9sanp9s9phmzfq6mcasd788ppzi0gcq")))

(define-public crate-scilla-parser-0.3.0 (c (n "scilla-parser") (v "0.3.0") (d (list (d (n "lexpr") (r "^0.2.7") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "1a44p1a9p8yy5h855hsy73dgclld993girizv3kiwbnd05gj3vwv")))

(define-public crate-scilla-parser-0.4.0 (c (n "scilla-parser") (v "0.4.0") (d (list (d (n "lexpr") (r "^0.2.7") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "15xlbsvnsrbaz1cmr3x9h7ic0s1s0wkdbhyk8jk8izyq1wa3chil")))

(define-public crate-scilla-parser-0.5.0 (c (n "scilla-parser") (v "0.5.0") (d (list (d (n "lexpr") (r "^0.2.7") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "0sibl2hm6as20y09vabz8zpkn4m5izah75kkvzhkz1hn0dixym2g")))

(define-public crate-scilla-parser-0.6.0 (c (n "scilla-parser") (v "0.6.0") (d (list (d (n "lexpr") (r "^0.2.7") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "1d7g350ryijcni07vy7f0rblz7rsjqh3xb8wpm67qinq3mwxgdl9")))

(define-public crate-scilla-parser-0.7.0 (c (n "scilla-parser") (v "0.7.0") (d (list (d (n "lexpr") (r "^0.2.7") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "0rb6q8jps25jg70nc9q6xvmdzzaj1pflrvj8gslvj892hm6gbxsj")))

(define-public crate-scilla-parser-0.8.0 (c (n "scilla-parser") (v "0.8.0") (d (list (d (n "lexpr") (r "^0.2.7") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "1513hms8n8rfmbix2dn0b6li2ng93zrbs9h7zmas1vlrryvvf51z")))

(define-public crate-scilla-parser-0.9.0 (c (n "scilla-parser") (v "0.9.0") (d (list (d (n "lexpr") (r "^0.2.7") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "1lnd0zl08kxhd87i9b0albpm9fy15yin2zb7xzfdlrk0rdv31v9z")))

(define-public crate-scilla-parser-0.10.0 (c (n "scilla-parser") (v "0.10.0") (d (list (d (n "lexpr") (r "^0.2.7") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "134lwgf1kigim85k2hn25axflvsy1x1dbf12n3ppw1j2js7wbq3j")))

(define-public crate-scilla-parser-0.11.0 (c (n "scilla-parser") (v "0.11.0") (d (list (d (n "lalrpop") (r "^0.20.0") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.20.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "17d8a7iqysaykz3vrzng5x2zvkky520iaahiargr1pscv5103nvm")))

(define-public crate-scilla-parser-1.0.0 (c (n "scilla-parser") (v "1.0.0") (d (list (d (n "lalrpop") (r "^0.20.0") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.20.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "0w4k4hvp2cav527wqwz5qrld12x746ha44gpvb59rlgyhhrw8cr7")))

