(define-module (crates-io sc il scilib) #:use-module (crates-io))

(define-public crate-scilib-0.1.0 (c (n "scilib") (v "0.1.0") (h "1kpzgwgpj7pms9vjbhx7rjxgkv3hc1vzczi1jqcf0n3z0dfzn382")))

(define-public crate-scilib-0.1.1 (c (n "scilib") (v "0.1.1") (h "1f2ffy0a02rhswc5vmcra9ygmx50bfad68d3jwbkydl7s1sj2wsp") (y #t)))

(define-public crate-scilib-0.0.0 (c (n "scilib") (v "0.0.0") (h "1wx54yrjazygcdw8j1vn06bjcas58lb8pnj09j3w3ip9yy9jilk5") (y #t)))

(define-public crate-scilib-0.2.0 (c (n "scilib") (v "0.2.0") (h "0qnilsvfrhrxmd02bhfiw0vfnhls01vxdbmq07cm3q96rm98qapb") (y #t)))

(define-public crate-scilib-0.2.1 (c (n "scilib") (v "0.2.1") (h "1jqvgjc1bm400rkiysxy50fz4v1l401zkdb64w991nxhrvj9xg3x") (y #t)))

(define-public crate-scilib-0.2.2 (c (n "scilib") (v "0.2.2") (h "1rhspn9wwlmvanqml0x63fvpzah51mykvl00b9pbv6q7x8xaz1qx")))

(define-public crate-scilib-0.2.3 (c (n "scilib") (v "0.2.3") (h "0bamhhb9vpl1f8z5m4581aaky0zh072vic741ac7cxb666fidgfl")))

(define-public crate-scilib-0.2.4 (c (n "scilib") (v "0.2.4") (h "06n263l1j5k422fc1xi82rjl6sff6q2732bdplyxyf3ngzgycvjm") (y #t)))

(define-public crate-scilib-0.2.45 (c (n "scilib") (v "0.2.45") (h "0xfv8mrnawkbzsq2v2zc9pdqziwl91dlq1h4qziw5zcm782imfi3") (y #t)))

(define-public crate-scilib-0.2.5 (c (n "scilib") (v "0.2.5") (h "08znwxk61zx1k3rrvk5x7x22gkxv37x85x8dpixr9hvm87rd080f")))

(define-public crate-scilib-0.3.0 (c (n "scilib") (v "0.3.0") (h "1w28gc20lha9k6zhng8mq6d0vqd76jv3scc8519j0kqc3fz586bm")))

(define-public crate-scilib-0.4.0 (c (n "scilib") (v "0.4.0") (h "0mdw274dcxddpdnpv4r2vr767gnpbmfk9ssrd1ac7j7x7cd7i63i")))

(define-public crate-scilib-0.4.1 (c (n "scilib") (v "0.4.1") (h "0z2s1v1l954xa8shp0714f5al68vahc05gpfhg69wcgjfi83k8p9")))

(define-public crate-scilib-0.4.2 (c (n "scilib") (v "0.4.2") (h "0hmg5p8j8aj4khr36q667m953xls4zjya9im62qsvmqy9d8rwvqv")))

(define-public crate-scilib-0.4.3 (c (n "scilib") (v "0.4.3") (h "0n0y6ssr236s9kig2hp9413xfl95m1n3ad492wllnj49zz00q6bh")))

(define-public crate-scilib-0.4.4 (c (n "scilib") (v "0.4.4") (h "1zw37drzva2ljh81aabjmvbnb1flxgrr658y4q0w6hqsp1z2f5ic")))

(define-public crate-scilib-0.5.0 (c (n "scilib") (v "0.5.0") (d (list (d (n "num-complex") (r "^0.4.1") (d #t) (k 0)))) (h "1m9lgrvamahnhjvppghqd3lxxpiibrdi1bpnvzv258p085vn0a7c")))

(define-public crate-scilib-0.6.0 (c (n "scilib") (v "0.6.0") (d (list (d (n "num-complex") (r "^0.4.2") (d #t) (k 0)))) (h "01d0hy7nf4328d1apmpg2zawlfg6w80yxcmll3qflkiz109v31sh")))

(define-public crate-scilib-0.6.1 (c (n "scilib") (v "0.6.1") (d (list (d (n "num-complex") (r "^0.4.2") (d #t) (k 0)))) (h "0123c1p4cznnnv0iwkxkk7d780pj5fyqzrp6y2y52yslzrhn5y24")))

(define-public crate-scilib-0.6.2 (c (n "scilib") (v "0.6.2") (d (list (d (n "num-complex") (r "^0.4.2") (d #t) (k 0)))) (h "0vb3450y3mcp3gkl0310j5nnhhm2j7bf7cm01y6g7ikpjaxx935h")))

(define-public crate-scilib-0.7.0 (c (n "scilib") (v "0.7.0") (d (list (d (n "num-complex") (r "^0.4.2") (d #t) (k 0)))) (h "0j1p7lrch6cf7al3fi5lzyaz6xj9vigib2fs2kay29isp3hq39v4")))

(define-public crate-scilib-0.7.1 (c (n "scilib") (v "0.7.1") (d (list (d (n "num-complex") (r "^0.4.2") (d #t) (k 0)))) (h "0gzviig0pnj8nzqd9yq8x4s1yb3myh9l1gjpv5hhhsdjgf7mskwx")))

(define-public crate-scilib-1.0.0 (c (n "scilib") (v "1.0.0") (d (list (d (n "num-complex") (r "^0.4.2") (d #t) (k 0)))) (h "1cvwjfs0ybjm7l2612ad3v2rdyp4v6rwfyk3ljs0pwrvv01k7w5c")))

