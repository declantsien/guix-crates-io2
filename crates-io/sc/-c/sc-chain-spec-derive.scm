(define-module (crates-io sc -c sc-chain-spec-derive) #:use-module (crates-io))

(define-public crate-sc-chain-spec-derive-2.0.0-alpha.2 (c (n "sc-chain-spec-derive") (v "2.0.0-alpha.2") (d (list (d (n "proc-macro-crate") (r "^0.1.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.7") (d #t) (k 0)))) (h "1hxfm0z1qs2gy88c0mbv15zdfr0nibgsb5nygl6balvd4cbkb1za")))

(define-public crate-sc-chain-spec-derive-2.0.0-alpha.3 (c (n "sc-chain-spec-derive") (v "2.0.0-alpha.3") (d (list (d (n "proc-macro-crate") (r "^0.1.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.7") (d #t) (k 0)))) (h "065dy6vyhf5fyjr7ncb38ilhbcalsrds6vz24y4cky608iwa5imd")))

(define-public crate-sc-chain-spec-derive-2.0.0-alpha.5 (c (n "sc-chain-spec-derive") (v "2.0.0-alpha.5") (d (list (d (n "proc-macro-crate") (r "^0.1.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.7") (d #t) (k 0)))) (h "16h0v21dxw2ppxcyz4xi23hdp6di0gig0diyc33d9h8xj150dnar")))

(define-public crate-sc-chain-spec-derive-2.0.0-alpha.6 (c (n "sc-chain-spec-derive") (v "2.0.0-alpha.6") (d (list (d (n "proc-macro-crate") (r "^0.1.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.7") (d #t) (k 0)))) (h "1fli7qcmdgnibsqyfim1ckzi269fka9pdpq80knh2dpm64pz00r7")))

(define-public crate-sc-chain-spec-derive-2.0.0-alpha.7 (c (n "sc-chain-spec-derive") (v "2.0.0-alpha.7") (d (list (d (n "proc-macro-crate") (r "^0.1.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.7") (d #t) (k 0)))) (h "0vq7hx3zjfwvxrld648fkqlkvbaizsg374lgf0gdcd724ipw46x1")))

(define-public crate-sc-chain-spec-derive-2.0.0-alpha.8 (c (n "sc-chain-spec-derive") (v "2.0.0-alpha.8") (d (list (d (n "proc-macro-crate") (r "^0.1.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.7") (d #t) (k 0)))) (h "0xl02p0d0sl1b9cmr5pjsvzr7sfpaxxivi69wynkx8ji1x2hqmzm")))

(define-public crate-sc-chain-spec-derive-2.0.0-rc1 (c (n "sc-chain-spec-derive") (v "2.0.0-rc1") (d (list (d (n "proc-macro-crate") (r "^0.1.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.7") (d #t) (k 0)))) (h "1i1lzrwx2fc2livw3hy09hi435a83yw7cj0bqv2jzj930yc6d4j0")))

(define-public crate-sc-chain-spec-derive-2.0.0-rc2 (c (n "sc-chain-spec-derive") (v "2.0.0-rc2") (d (list (d (n "proc-macro-crate") (r "^0.1.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.7") (d #t) (k 0)))) (h "18s2hnhhciwm5l1j43pa92kdrmmpinh66lzlpqg5aw3wjwb7wbp6")))

(define-public crate-sc-chain-spec-derive-2.0.0-rc3 (c (n "sc-chain-spec-derive") (v "2.0.0-rc3") (d (list (d (n "proc-macro-crate") (r "^0.1.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.7") (d #t) (k 0)))) (h "1hd05hc2qdj566lwyqspn0wavmbijkzqihyngfay7ij478n5zyfp")))

(define-public crate-sc-chain-spec-derive-2.0.0-rc4 (c (n "sc-chain-spec-derive") (v "2.0.0-rc4") (d (list (d (n "proc-macro-crate") (r "^0.1.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.7") (d #t) (k 0)))) (h "1jm50qsh0cvyqsh6qsg6sl2lcnwdsgdrgdxx1r935w309drr067b")))

(define-public crate-sc-chain-spec-derive-2.0.0-rc5 (c (n "sc-chain-spec-derive") (v "2.0.0-rc5") (d (list (d (n "proc-macro-crate") (r "^0.1.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.7") (d #t) (k 0)))) (h "0m4skqkp001fffhkp0hibmbjf6d61d4ilawmxz4sv5z78xpllps0")))

(define-public crate-sc-chain-spec-derive-2.0.0-rc6 (c (n "sc-chain-spec-derive") (v "2.0.0-rc6") (d (list (d (n "proc-macro-crate") (r "^0.1.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.7") (d #t) (k 0)))) (h "0vd8qwqwy3sralp6kr6mggs8qvw3iic485k61rq1284p98jjgzd7")))

(define-public crate-sc-chain-spec-derive-2.0.0 (c (n "sc-chain-spec-derive") (v "2.0.0") (d (list (d (n "proc-macro-crate") (r "^0.1.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.7") (d #t) (k 0)))) (h "00x4lxpcicjjyp9601xixij4vyj8qryx45pcl0mgmlp2i6kjrbrq")))

(define-public crate-sc-chain-spec-derive-2.0.1 (c (n "sc-chain-spec-derive") (v "2.0.1") (d (list (d (n "proc-macro-crate") (r "^0.1.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.7") (d #t) (k 0)))) (h "0c0q072cjkmj4hx2yxjrsg1sdyspyjsb72avvws3gnjk1k7jb1sf")))

(define-public crate-sc-chain-spec-derive-3.0.0 (c (n "sc-chain-spec-derive") (v "3.0.0") (d (list (d (n "proc-macro-crate") (r "^0.1.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.58") (d #t) (k 0)))) (h "1i42g6dhays6sz74craxa4x6n74gl34bsn7axpy9i5yv2darh52z")))

(define-public crate-sc-chain-spec-derive-4.0.0 (c (n "sc-chain-spec-derive") (v "4.0.0") (d (list (d (n "proc-macro-crate") (r "^1.1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (d #t) (k 0)))) (h "0js3l16hhimd6mfvw7jscqv9lq2rjr80q6jgybmfkz5pkvn3jr6p")))

(define-public crate-sc-chain-spec-derive-5.0.0 (c (n "sc-chain-spec-derive") (v "5.0.0") (d (list (d (n "proc-macro-crate") (r "^1.1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (d #t) (k 0)))) (h "1flcgfhwlqsqmjsy00kfbchnzr7jb3s2cf83qqip13qkni9f11bi")))

(define-public crate-sc-chain-spec-derive-6.0.0 (c (n "sc-chain-spec-derive") (v "6.0.0") (d (list (d (n "proc-macro-crate") (r "^1.1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.14") (d #t) (k 0)))) (h "0wps93y0014dhhihqzlklz5gci33na24xvmnynbf5b1h9mm14w0c")))

(define-public crate-sc-chain-spec-derive-6.1.0-dev1 (c (n "sc-chain-spec-derive") (v "6.1.0-dev1") (d (list (d (n "proc-macro-crate") (r "^1.1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.16") (d #t) (k 0)))) (h "1yzkkn8z18wgrp8vr37aqii8qww4d73bxpfwn0zmpba0pdryaxsq")))

(define-public crate-sc-chain-spec-derive-6.1.0-dev.2 (c (n "sc-chain-spec-derive") (v "6.1.0-dev.2") (d (list (d (n "proc-macro-crate") (r "^1.1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.16") (d #t) (k 0)))) (h "053qb2f4zk9869q9c2vnwlgxyq4kd8z4fhvia1q32lp75ax4wk9f")))

(define-public crate-sc-chain-spec-derive-6.1.0-dev.3 (c (n "sc-chain-spec-derive") (v "6.1.0-dev.3") (d (list (d (n "proc-macro-crate") (r "^1.1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.16") (d #t) (k 0)))) (h "10d1ywv1kv7vxbmfmrb0h29j721slhpbm8wi27389qyfgm7l035h")))

(define-public crate-sc-chain-spec-derive-6.1.0-dev.4 (c (n "sc-chain-spec-derive") (v "6.1.0-dev.4") (d (list (d (n "proc-macro-crate") (r "^1.1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.16") (d #t) (k 0)))) (h "15dwhxsfz1snkxd7yj2gkrj577w9ppj2346r4svlzyyyql97zyq0")))

(define-public crate-sc-chain-spec-derive-6.1.0-dev.5 (c (n "sc-chain-spec-derive") (v "6.1.0-dev.5") (d (list (d (n "proc-macro-crate") (r "^1.1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.16") (d #t) (k 0)))) (h "1glbn2lalmkcidb2l9vbvvifq16fj4wbpw6yh8hc4lqkya5glsaa")))

(define-public crate-sc-chain-spec-derive-6.1.0-dev.6 (c (n "sc-chain-spec-derive") (v "6.1.0-dev.6") (d (list (d (n "proc-macro-crate") (r "^1.1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.16") (d #t) (k 0)))) (h "063gc6w2vjyrbp22s3wywbjy87g4iava6ar9ga5ip40lg6mc2y78")))

(define-public crate-sc-chain-spec-derive-7.0.0 (c (n "sc-chain-spec-derive") (v "7.0.0") (d (list (d (n "proc-macro-crate") (r "^1.1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.16") (d #t) (k 0)))) (h "0ybqh834k0556rwbvxmk3zypx6fq1wvmibhw7p2d99jgbiqvwsnp")))

(define-public crate-sc-chain-spec-derive-8.0.0 (c (n "sc-chain-spec-derive") (v "8.0.0") (d (list (d (n "proc-macro-crate") (r "^1.1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.31") (d #t) (k 0)))) (h "0g5xsxjxmq8sv10hwbaj9ysp5kwn95n1vv8ycsalrg095fbaw0bv")))

(define-public crate-sc-chain-spec-derive-9.0.0-dev.1 (c (n "sc-chain-spec-derive") (v "9.0.0-dev.1") (d (list (d (n "proc-macro-crate") (r "^1.1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (d #t) (k 0)))) (h "0kfzk7qjfgf2r5665pdrig580xdcw302gb487a3f5f86j8j2j1k8")))

(define-public crate-sc-chain-spec-derive-9.0.0 (c (n "sc-chain-spec-derive") (v "9.0.0") (d (list (d (n "proc-macro-crate") (r "^1.1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (d #t) (k 0)))) (h "0xyqkc8qpjidlgbxsfjdinr7ynhs6vfrlk1in5c3rh0p3n4p9848")))

(define-public crate-sc-chain-spec-derive-10.0.0 (c (n "sc-chain-spec-derive") (v "10.0.0") (d (list (d (n "proc-macro-crate") (r "^1.1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (d #t) (k 0)))) (h "0pfrlgzpjz6plkl78dhr3zfwlhdmk1akf8lkv8aqgd0yg67ia98z")))

(define-public crate-sc-chain-spec-derive-11.0.0 (c (n "sc-chain-spec-derive") (v "11.0.0") (d (list (d (n "proc-macro-crate") (r "^3.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (d #t) (k 0)))) (h "1f0r62p78iv85ns06qk70b8mmhipj0wfq3jb9cnpd551msyhzs7j")))

