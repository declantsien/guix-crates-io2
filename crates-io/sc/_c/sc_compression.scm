(define-module (crates-io sc _c sc_compression) #:use-module (crates-io))

(define-public crate-sc_compression-0.1.0 (c (n "sc_compression") (v "0.1.0") (d (list (d (n "lzham") (r "^0.1.1") (d #t) (k 0)) (d (n "rust-lzma") (r "^0.5") (d #t) (k 0)))) (h "00zlhjdb6jiprw2np20ykg08bdc48jr6prap42rg401w5mac0q3z")))

