(define-module (crates-io sc -a sc-api) #:use-module (crates-io))

(define-public crate-sc-api-0.1.0 (c (n "sc-api") (v "0.1.0") (d (list (d (n "async-mutex") (r "^1.4.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)) (d (n "tokio") (r "^1.24.2") (f (quote ("full"))) (d #t) (k 2)))) (h "17pvz58azsw9qj58dy9m4lmg82zp9m69k0hzamj0qywz8gfi1fj7")))

(define-public crate-sc-api-0.1.1 (c (n "sc-api") (v "0.1.1") (d (list (d (n "async-mutex") (r "^1.4.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)) (d (n "tokio") (r "^1.24.2") (f (quote ("full"))) (d #t) (k 2)))) (h "151mijxz9xfmd6m3vafpnwsl49drvjifh6pi5jlxlhskb61zajzx")))

