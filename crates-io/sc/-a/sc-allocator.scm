(define-module (crates-io sc -a sc-allocator) #:use-module (crates-io))

(define-public crate-sc-allocator-4.0.0-dev (c (n "sc-allocator") (v "4.0.0-dev") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "sp-core") (r "^3.0.0") (d #t) (k 0)) (d (n "sp-wasm-interface") (r "^3.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.21") (d #t) (k 0)))) (h "1p81i8vzdxb7c5598pxmlm5hh8p2crlh49pcrcb4f8cl3xgnjbrp")))

(define-public crate-sc-allocator-4.1.0 (c (n "sc-allocator") (v "4.1.0") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "sp-core") (r "^8.0.0") (d #t) (k 0)) (d (n "sp-wasm-interface") (r "^8.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1z6jm02yjis0ycljgcpbd2zsy4j8kp40q8xaxwpdw81xc76gr3lw")))

(define-public crate-sc-allocator-5.0.0 (c (n "sc-allocator") (v "5.0.0") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "sp-core") (r "^9.0.0") (d #t) (k 0)) (d (n "sp-wasm-interface") (r "^8.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1jz8yx024fkbk4qc1fh3bazdci17xa7shwxl9grjwaz969cih7mc")))

(define-public crate-sc-allocator-6.0.0 (c (n "sc-allocator") (v "6.0.0") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "sp-core") (r "^10.0.0") (d #t) (k 0)) (d (n "sp-wasm-interface") (r "^8.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0by3m2cnjik6kab4h7i4jlap43rah9j27xznc1ipy5cdm6sq9sdm")))

(define-public crate-sc-allocator-7.0.0 (c (n "sc-allocator") (v "7.0.0") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "sp-core") (r "^11.0.0") (d #t) (k 0)) (d (n "sp-wasm-interface") (r "^8.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0zyl0mdpxpd4lwv20gjla88g2fhsmks6kshh7lb20z779nkzy8xh")))

(define-public crate-sc-allocator-8.0.0 (c (n "sc-allocator") (v "8.0.0") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "sp-core") (r "^12.0.0") (d #t) (k 0)) (d (n "sp-wasm-interface") (r "^8.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1snmkn7bzkfkbj2k1lq12k269avikdsf874lj99ibic1q8vfv3a3")))

(define-public crate-sc-allocator-9.0.0 (c (n "sc-allocator") (v "9.0.0") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "sp-core") (r "^13.0.0") (d #t) (k 0)) (d (n "sp-wasm-interface") (r "^9.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0jddy1s7iqykbv3p7ayq9al84px6125nrqqc75i7a635vsyy06zh")))

(define-public crate-sc-allocator-10.0.0 (c (n "sc-allocator") (v "10.0.0") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "sp-core") (r "^14.0.0") (d #t) (k 0)) (d (n "sp-wasm-interface") (r "^9.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0id92mcdlch4xvfnjqs0g4ai7nqf8d7n6ankbvflmn8r1m3icb95")))

(define-public crate-sc-allocator-11.0.0 (c (n "sc-allocator") (v "11.0.0") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "sp-core") (r "^15.0.0") (d #t) (k 0)) (d (n "sp-wasm-interface") (r "^9.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "00r3wdxqdbp42rd35lrpimg1z8ns1rg8mjhqhsjpx0pga0h8nwvm")))

(define-public crate-sc-allocator-12.0.0 (c (n "sc-allocator") (v "12.0.0") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "sp-core") (r "^16.0.0") (d #t) (k 0)) (d (n "sp-wasm-interface") (r "^10.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0y063555ga508lmw5aa4v9racklry1fidc9m17gvsasnpz2wipcy")))

(define-public crate-sc-allocator-13.0.0 (c (n "sc-allocator") (v "13.0.0") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "sp-core") (r "^17.0.0") (d #t) (k 0)) (d (n "sp-wasm-interface") (r "^11.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0m6nlp4myvp82ipc8yg8dga8hdrcnbq9v75qbwnfkqz938mpccqr")))

(define-public crate-sc-allocator-14.0.0 (c (n "sc-allocator") (v "14.0.0") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "sp-core") (r "^18.0.0") (d #t) (k 0)) (d (n "sp-wasm-interface") (r "^12.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "01wif14vrffzj8p6lyy751y7xcb358b53hzl9mm76j5aad5pia2s")))

(define-public crate-sc-allocator-15.0.0 (c (n "sc-allocator") (v "15.0.0") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "sp-core") (r "^19.0.0") (d #t) (k 0)) (d (n "sp-wasm-interface") (r "^13.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0gyyf0lsgmksmizbv68a0va0va8w9b9wiawx5rgziqz2vb3h1n2m")))

(define-public crate-sc-allocator-16.0.0 (c (n "sc-allocator") (v "16.0.0") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "sp-core") (r "^20.0.0") (d #t) (k 0)) (d (n "sp-wasm-interface") (r "^13.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1g62y66mg3ac4lfc6hqjqjc832ak8k4px8ij98li62f1c85c89ci")))

(define-public crate-sc-allocator-16.1.0-dev.2 (c (n "sc-allocator") (v "16.1.0-dev.2") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "sp-core") (r "^21.1.0-dev.2") (d #t) (k 0)) (d (n "sp-wasm-interface") (r "^14.1.0-dev.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "16sc5c19sy8j3ybx0cwcrl6wf5b532vcbqid0055n2gpcvp4axjl")))

(define-public crate-sc-allocator-16.1.0-dev.3 (c (n "sc-allocator") (v "16.1.0-dev.3") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "sp-core") (r "^21.1.0-dev.3") (d #t) (k 0)) (d (n "sp-wasm-interface") (r "^14.1.0-dev.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0gbyvq9v3pr8sqlaibxdavimbnirr79dfdj87xwbhhjh9rin3088")))

(define-public crate-sc-allocator-16.1.0-dev.4 (c (n "sc-allocator") (v "16.1.0-dev.4") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "sp-core") (r "^21.1.0-dev.4") (d #t) (k 0)) (d (n "sp-wasm-interface") (r "^14.1.0-dev.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1cywvyr22bakhshwnfkzq0j9mq103sppq881n7qblcmq9nzha837")))

(define-public crate-sc-allocator-16.1.0-dev.5 (c (n "sc-allocator") (v "16.1.0-dev.5") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "sp-core") (r "=21.1.0-dev.5") (d #t) (k 0)) (d (n "sp-wasm-interface") (r "=14.1.0-dev.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0dn1hr1wgnzyfhap7q0klj9k1n27vzknsiaiwpg6xwvgmbjvkv1d")))

(define-public crate-sc-allocator-16.1.0-dev.6 (c (n "sc-allocator") (v "16.1.0-dev.6") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "sp-core") (r "=21.1.0-dev.6") (d #t) (k 0)) (d (n "sp-wasm-interface") (r "=14.1.0-dev.6") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0sbc08sfa88zmigi5513h416sbszrgfbvzjl6yk0866d7kgarv5d")))

(define-public crate-sc-allocator-17.0.0 (c (n "sc-allocator") (v "17.0.0") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "sp-core") (r "^22.0.0") (d #t) (k 0)) (d (n "sp-wasm-interface") (r "^15.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1c70czyb9y22v279yr4fdm2i6lnlgb6fzlhf8da7jzl2icymkhv7")))

(define-public crate-sc-allocator-18.0.0 (c (n "sc-allocator") (v "18.0.0") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "sp-core") (r "^23.0.0") (d #t) (k 0)) (d (n "sp-wasm-interface") (r "^16.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)))) (h "0z77jg6vaz32wf4y9v6bi7xd74qi7kyrasliida4xkv5bwai2pgp")))

(define-public crate-sc-allocator-19.0.0-dev.1 (c (n "sc-allocator") (v "19.0.0-dev.1") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "sp-core") (r "=24.0.0-dev.1") (d #t) (k 0)) (d (n "sp-wasm-interface") (r "=17.0.0-dev.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)))) (h "1vg8g85s8rp6y1nq3f33mnkxfbdzn93qgirqh5r01790dw5kf45k")))

(define-public crate-sc-allocator-19.0.0 (c (n "sc-allocator") (v "19.0.0") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "sp-core") (r "^24.0.0") (d #t) (k 0)) (d (n "sp-wasm-interface") (r "^17.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)))) (h "0ggpijy8pnh0zcfwyms837p0cycsxmnlb75ldpkymcfwj24ybmib")))

(define-public crate-sc-allocator-20.0.0 (c (n "sc-allocator") (v "20.0.0") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "sp-core") (r "^25.0.0") (d #t) (k 0)) (d (n "sp-wasm-interface") (r "^18.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)))) (h "0x44y3sgl20z07wgjfvg5f33fy7ac9m2fin94k7pzzwwdabwbd36")))

(define-public crate-sc-allocator-21.0.0 (c (n "sc-allocator") (v "21.0.0") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "sp-core") (r "^26.0.0") (d #t) (k 0)) (d (n "sp-wasm-interface") (r "^18.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)))) (h "1lb4gzh3ca8sll6k9vdlcqkg6acxx1jn2h23gqfs66xjalpa1lfl")))

(define-public crate-sc-allocator-22.0.0 (c (n "sc-allocator") (v "22.0.0") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "sp-core") (r "^27.0.0") (d #t) (k 0)) (d (n "sp-wasm-interface") (r "^19.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)))) (h "0xnyk9i9lbyr98zjx651437wkm8a4srlj62l5wh65nwqsf8qgbbr")))

(define-public crate-sc-allocator-23.0.0 (c (n "sc-allocator") (v "23.0.0") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "sp-core") (r "^28.0.0") (d #t) (k 0)) (d (n "sp-wasm-interface") (r "^20.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)))) (h "1fhc0xsyp80715rahlzgh93wsxfq8cb2sqc3i5gmsq3hpha5gc23")))

(define-public crate-sc-allocator-24.0.0 (c (n "sc-allocator") (v "24.0.0") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "sp-core") (r "^29.0.0") (d #t) (k 0)) (d (n "sp-wasm-interface") (r "^20.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)))) (h "0w4byi71ji0zi452ggrvwk6kpcdm18qf7xl2wld6vvbk2g4jfw9m")))

(define-public crate-sc-allocator-25.0.0 (c (n "sc-allocator") (v "25.0.0") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "sp-core") (r "^30.0.0") (d #t) (k 0)) (d (n "sp-wasm-interface") (r "^20.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)))) (h "0crr281l42521j5adw7853jgs3m10axlzinr6cxx3c9b3g18rrwp")))

(define-public crate-sc-allocator-26.0.0 (c (n "sc-allocator") (v "26.0.0") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "sp-core") (r "^31.0.0") (d #t) (k 0)) (d (n "sp-wasm-interface") (r "^20.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)))) (h "0brrja9i38q0l8x4wrgcylzd1drrnf5544k33yi2m1ni5gdzs5a7")))

(define-public crate-sc-allocator-27.0.0 (c (n "sc-allocator") (v "27.0.0") (d (list (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "sp-core") (r "^32.0.0") (d #t) (k 0)) (d (n "sp-wasm-interface") (r "^21.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)))) (h "1xq2jlmcdd7m3icchwfrs0jdgmhqawgz1fxg07bd94f4pdqqgrwp")))

(define-public crate-sc-allocator-28.0.0 (c (n "sc-allocator") (v "28.0.0") (d (list (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "sp-core") (r "^33.0.0") (d #t) (k 0)) (d (n "sp-wasm-interface") (r "^21.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)))) (h "1qp4j32nir220b7aakny9rpjv05cjmwrh22zpqb7k99ywwc15w53")))

