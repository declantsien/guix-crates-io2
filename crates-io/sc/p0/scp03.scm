(define-module (crates-io sc p0 scp03) #:use-module (crates-io))

(define-public crate-scp03-0.0.0 (c (n "scp03") (v "0.0.0") (d (list (d (n "aesni") (r "^0.2") (d #t) (k 0)) (d (n "byteorder") (r "^1.2") (k 0)) (d (n "clear_on_drop") (r "^0.2") (d #t) (k 0)) (d (n "cmac") (r "^0.1") (d #t) (k 0)) (d (n "constant_time_eq") (r "^0.1") (d #t) (k 0)) (d (n "hmac") (r "^0.4") (d #t) (k 0)) (d (n "pbkdf2") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)) (d (n "sha2") (r "^0.6") (d #t) (k 0)))) (h "0lnzqvsjjq6xvi2ali89bp2cx5f3qiiic88f4nm1582x5zagwmza") (y #t)))

