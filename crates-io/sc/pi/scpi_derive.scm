(define-module (crates-io sc pi scpi_derive) #:use-module (crates-io))

(define-public crate-scpi_derive-0.1.0 (c (n "scpi_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1jc5nzzyzh0wjcdb22q3xjqx1lpfhgrd864b69z233m1nxpgsl5z")))

(define-public crate-scpi_derive-0.1.1 (c (n "scpi_derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0h2364bl14r7x31vgb7vq92kb129mkz6mlck6xlrczmhlh6j2rac")))

(define-public crate-scpi_derive-0.3.0 (c (n "scpi_derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0qnjvvq20lq2yvrn15ph54w5mlp41lbsmgq2f8ppzcyd0p96hwzs")))

(define-public crate-scpi_derive-0.3.1 (c (n "scpi_derive") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0q61bh2snwn8kdggyh4s5vj1l0yk6g66qzlrlipizgmmzfryrjh3")))

(define-public crate-scpi_derive-0.3.2 (c (n "scpi_derive") (v "0.3.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "11g1bsnng0dkrgryn803f22flad5la0ny56vym4223lafz9midr2")))

(define-public crate-scpi_derive-0.3.3 (c (n "scpi_derive") (v "0.3.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1ljs9v1af4v1cibp6jk0353dq4y8rfaifdb62zsyn45z8x536ffl")))

(define-public crate-scpi_derive-0.3.4 (c (n "scpi_derive") (v "0.3.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "03a6cr37z5f1106rag03b6kgpsvhgbbwgvr1329qcdznmj0jp9x2")))

(define-public crate-scpi_derive-0.3.5 (c (n "scpi_derive") (v "0.3.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0wdhmvbvn422ncwqiis8wq8awn345k887zixax42v2a0lr4wiifw")))

(define-public crate-scpi_derive-0.4.1 (c (n "scpi_derive") (v "0.4.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0y1hl8b38clv64mg0vfrslgrbsp3wn7z85d3bfvaqv6h98fvl1gg")))

(define-public crate-scpi_derive-0.4.2 (c (n "scpi_derive") (v "0.4.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1vyy13nks9r526b1wanx8z420qwabkv6p2xbdqp48hg9hlq542zh")))

(define-public crate-scpi_derive-0.4.3 (c (n "scpi_derive") (v "0.4.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1qr0c2mws847fx9sdaggd9zfvr0c6dczrcq8p4fnmvfk924pgy22")))

(define-public crate-scpi_derive-0.4.4 (c (n "scpi_derive") (v "0.4.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1lz60msw5pp5va68y3g49pi402idpjfgxxkr6p9cc79b30w0xgm8")))

(define-public crate-scpi_derive-1.0.0 (c (n "scpi_derive") (v "1.0.0") (d (list (d (n "document-features") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "11s3b0cfhkif46m1njswmq1hp51j5px0qkbw52rdzcn5gwym2lcm") (f (quote (("_private"))))))

(define-public crate-scpi_derive-1.0.1 (c (n "scpi_derive") (v "1.0.1") (d (list (d (n "document-features") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1hl7xpjrpnvxwbnzfhvpz5c0kcad3h80v22wq6nlfv0x8m8417kd") (f (quote (("_private"))))))

