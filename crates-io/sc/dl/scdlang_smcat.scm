(define-module (crates-io sc dl scdlang_smcat) #:use-module (crates-io))

(define-public crate-scdlang_smcat-0.2.0 (c (n "scdlang_smcat") (v "0.2.0") (d (list (d (n "assert-json-diff") (r "^1") (d #t) (k 2)) (d (n "scdlang") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "serde_with") (r "^1") (f (quote ("json"))) (d #t) (k 0)))) (h "18221w8y4zyf1xdddyszfvs5w9i2ma1bgm4zf0nrr2g6vnwwv4ag")))

(define-public crate-scdlang_smcat-0.2.1 (c (n "scdlang_smcat") (v "0.2.1") (d (list (d (n "assert-json-diff") (r "^1") (d #t) (k 2)) (d (n "scdlang") (r "^0.2.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "serde_with") (r "^1") (f (quote ("json"))) (d #t) (k 0)))) (h "1fwh5i790lfja69fh07yw1rhzwjnp054m5gbk3g2blpg5hi4nd6y")))

