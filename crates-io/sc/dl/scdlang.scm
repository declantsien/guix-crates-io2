(define-module (crates-io sc dl scdlang) #:use-module (crates-io))

(define-public crate-scdlang-0.1.0 (c (n "scdlang") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "pest") (r "^2") (d #t) (k 0)) (d (n "pest_derive") (r "^2") (d #t) (k 0)))) (h "1kq1wkqlm7zfy53cskp6dzr7211b8gq9bjiimrh9rf5szc4fz1hq")))

(define-public crate-scdlang-0.2.0 (c (n "scdlang") (v "0.2.0") (d (list (d (n "either") (r "^1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "pest") (r "^2") (d #t) (k 0)) (d (n "pest_derive") (r "^2") (d #t) (k 0)))) (h "17rkv01rkal9hjwci25i22mjdmcrrh2wdi68g62c7nc2npz6lqzq")))

(define-public crate-scdlang-0.2.1 (c (n "scdlang") (v "0.2.1") (d (list (d (n "either") (r "^1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "pest") (r "^2") (d #t) (k 0)) (d (n "pest_derive") (r "^2") (d #t) (k 0)))) (h "0r0rjgyrb4k5prpzmrpimcfn0p1ig8wqvj9ym1ip0k1gr3lvd4bh")))

