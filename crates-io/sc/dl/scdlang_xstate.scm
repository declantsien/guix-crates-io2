(define-module (crates-io sc dl scdlang_xstate) #:use-module (crates-io))

(define-public crate-scdlang_xstate-0.1.0 (c (n "scdlang_xstate") (v "0.1.0") (d (list (d (n "assert-json-diff") (r "^1") (d #t) (k 2)) (d (n "from-pest") (r "^0.3") (d #t) (k 0)) (d (n "pest") (r "^2") (d #t) (k 0)) (d (n "pest-ast") (r "^0.3") (d #t) (k 0)) (d (n "scdlang") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "voca_rs") (r "^1") (d #t) (k 0)))) (h "09q55cb2aki0ggx9anbgwscvhk5ad0ia4c24fqsqvw9v3q46qbwp")))

(define-public crate-scdlang_xstate-0.2.0 (c (n "scdlang_xstate") (v "0.2.0") (d (list (d (n "assert-json-diff") (r "^1") (d #t) (k 2)) (d (n "scdlang") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "voca_rs") (r "^1") (d #t) (k 0)))) (h "0d9z10gk7yz6d1m5c66bcjkskhmrnjl59lhd4fs0ibm8k030qxii")))

(define-public crate-scdlang_xstate-0.2.1 (c (n "scdlang_xstate") (v "0.2.1") (d (list (d (n "assert-json-diff") (r "^1") (d #t) (k 2)) (d (n "scdlang") (r "^0.2.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "voca_rs") (r "^1") (d #t) (k 0)))) (h "0nqckicjdf114yfzbg79da407jcy568jbccvpg9kwq05vp5wb6vd")))

