(define-module (crates-io sc -n sc-network-types) #:use-module (crates-io))

(define-public crate-sc-network-types-0.0.0 (c (n "sc-network-types") (v "0.0.0") (h "1fvs9vzf76lqms5knimcjsjkgcjn46ci33p9l9hlg8d989a069fy")))

(define-public crate-sc-network-types-0.10.0 (c (n "sc-network-types") (v "0.10.0") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "libp2p-identity") (r "^0.1.3") (f (quote ("ed25519" "peerid"))) (d #t) (k 0)) (d (n "litep2p") (r "^0.3.0") (d #t) (k 0)) (d (n "multiaddr") (r "^0.17.0") (d #t) (k 0)) (d (n "multihash") (r "^0.17.0") (f (quote ("identity" "multihash-impl" "sha2" "std"))) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)))) (h "1lb2liyzmfq2hkdj89y6hncl6j3827680hly05wmbxlkafk77d56")))

(define-public crate-sc-network-types-0.11.0 (c (n "sc-network-types") (v "0.11.0") (d (list (d (n "bs58") (r "^0.5.0") (d #t) (k 0)) (d (n "libp2p-identity") (r "^0.1.3") (f (quote ("ed25519" "peerid"))) (d #t) (k 0)) (d (n "litep2p") (r "=0.4.0-rc.1") (d #t) (k 0)) (d (n "multiaddr") (r "^0.17.0") (d #t) (k 0)) (d (n "multihash") (r "^0.17.0") (f (quote ("identity" "multihash-impl" "sha2" "std"))) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)))) (h "0bbg9qgg7im8i8cnqh26lmmihb54ad275rr1vm0ardksn2jqr2n7")))

