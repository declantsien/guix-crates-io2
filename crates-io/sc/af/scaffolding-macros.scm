(define-module (crates-io sc af scaffolding-macros) #:use-module (crates-io))

(define-public crate-scaffolding-macros-0.0.1 (c (n "scaffolding-macros") (v "0.0.1") (d (list (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.27") (d #t) (k 2)) (d (n "syn") (r "^2.0.53") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "uuid") (r "^1.5.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "0n3nhsbzk0p0sfzvim83rddvvx43dnqwpyb4km0q8bnllvh203kv")))

(define-public crate-scaffolding-macros-0.0.2 (c (n "scaffolding-macros") (v "0.0.2") (d (list (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.27") (d #t) (k 2)) (d (n "syn") (r "^2.0.53") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "uuid") (r "^1.5.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "0rnh6a1qcapip8qhgyza9c1wdwjvmwxl7sgk6mywpj3cgh023s0h")))

(define-public crate-scaffolding-macros-0.0.3 (c (n "scaffolding-macros") (v "0.0.3") (d (list (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.27") (d #t) (k 2)) (d (n "syn") (r "^2.0.53") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "uuid") (r "^1.5.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "1041l8sjdslsk4i8n1kf0r7w2ass1711zgnvmfdl2gq36rjmwilc")))

(define-public crate-scaffolding-macros-0.1.0 (c (n "scaffolding-macros") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.27") (d #t) (k 2)) (d (n "syn") (r "^2.0.53") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "04xbyimqn8cr8hxraxbbhgjnywipn8jmbi9dcy3i178i0x2swlki")))

(define-public crate-scaffolding-macros-0.2.0 (c (n "scaffolding-macros") (v "0.2.0") (d (list (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.27") (d #t) (k 2)) (d (n "syn") (r "^2.0.53") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1d240miar3b723bsfsnn91kbazxljblc8cyacaqlghjs89c2xwsq")))

(define-public crate-scaffolding-macros-0.3.0 (c (n "scaffolding-macros") (v "0.3.0") (d (list (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.27") (d #t) (k 2)) (d (n "syn") (r "^2.0.53") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1pwsjp5isfbp7cl24gcg3kf5blww92r2jykaah5m4glvhv21im4m")))

(define-public crate-scaffolding-macros-0.4.0 (c (n "scaffolding-macros") (v "0.4.0") (d (list (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.27") (d #t) (k 2)) (d (n "syn") (r "^2.0.53") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1mym38zny03bl606vj4q8hgvmda2shzc8470a5lsws7iwg7h7vmd")))

