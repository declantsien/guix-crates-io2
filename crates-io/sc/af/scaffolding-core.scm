(define-module (crates-io sc af scaffolding-core) #:use-module (crates-io))

(define-public crate-scaffolding-core-0.0.1 (c (n "scaffolding-core") (v "0.0.1") (d (list (d (n "chrono") (r "^0.4.35") (d #t) (k 0)) (d (n "scaffolding-macros") (r "^0.0.1") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.27") (d #t) (k 2)) (d (n "uuid") (r "^1.5.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "0nl18ww9bvvyrr873vg3l342v138p1hwmlakibb45hgfz1mrw0f0")))

(define-public crate-scaffolding-core-0.0.2 (c (n "scaffolding-core") (v "0.0.2") (d (list (d (n "chrono") (r "^0.4.35") (d #t) (k 0)) (d (n "scaffolding-macros") (r "^0.0.2") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.27") (d #t) (k 2)) (d (n "uuid") (r "^1.5.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "1sh13qq85vj4qxpyd59h4kq9yhyvnszzlzzr2663x59xbn901b0m")))

(define-public crate-scaffolding-core-0.0.3 (c (n "scaffolding-core") (v "0.0.3") (d (list (d (n "chrono") (r "^0.4.35") (d #t) (k 0)) (d (n "scaffolding-macros") (r "^0.0.3") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.27") (d #t) (k 2)) (d (n "uuid") (r "^1.5.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "1b2cbscrcdv6h4w3zd4xmzzmp2vkw2qpgc680ayinlymz95v49wd")))

(define-public crate-scaffolding-core-0.1.0 (c (n "scaffolding-core") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.35") (d #t) (k 0)) (d (n "scaffolding-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.27") (d #t) (k 2)) (d (n "uuid") (r "^1.5.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "037cvqkcws94ma0zv2imp5q6akvwxq1z1kpp8rcw4322ispyfpcd")))

(define-public crate-scaffolding-core-0.2.0 (c (n "scaffolding-core") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.35") (d #t) (k 0)) (d (n "scaffolding-macros") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.27") (d #t) (k 2)) (d (n "uuid") (r "^1.5.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "087qq0w5irr5cwphdqbnln3g521hp257fm3v3s7yh39lyfbm2zqf")))

(define-public crate-scaffolding-core-0.3.0 (c (n "scaffolding-core") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4.35") (d #t) (k 0)) (d (n "scaffolding-macros") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.27") (d #t) (k 2)) (d (n "uuid") (r "^1.5.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "17cc9frfynfq0y70pspfyr0y7m91xnhplk566c58pi6c3v9f144v")))

(define-public crate-scaffolding-core-0.4.0 (c (n "scaffolding-core") (v "0.4.0") (d (list (d (n "chrono") (r "^0.4.35") (d #t) (k 0)) (d (n "scaffolding-macros") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.27") (d #t) (k 2)) (d (n "uuid") (r "^1.5.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "0ipvmxava399n2xqlgpbm8c4cmdxpf1zdf4i08r5cgljbkmbxjym")))

