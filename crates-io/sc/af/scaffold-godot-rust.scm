(define-module (crates-io sc af scaffold-godot-rust) #:use-module (crates-io))

(define-public crate-scaffold-godot-rust-0.1.0 (c (n "scaffold-godot-rust") (v "0.1.0") (d (list (d (n "cliclack") (r "^0.1.10") (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)))) (h "0aixd1bbf29dgqbck88y8ny893b5vv9dbl3ciqd81gbyh5a4s1xb")))

(define-public crate-scaffold-godot-rust-0.1.1 (c (n "scaffold-godot-rust") (v "0.1.1") (d (list (d (n "cliclack") (r "^0.1.10") (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)))) (h "09v0mxnppy3jq3zip6aakskqfgqqvyhykldzv7ngwfwmi94k39dn")))

(define-public crate-scaffold-godot-rust-0.1.2 (c (n "scaffold-godot-rust") (v "0.1.2") (d (list (d (n "cliclack") (r "^0.1.10") (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)))) (h "053sfaylnwhya6ib155q20gsjbfj58yq4gc22imn6xd33jq6ssfk")))

(define-public crate-scaffold-godot-rust-0.1.3 (c (n "scaffold-godot-rust") (v "0.1.3") (d (list (d (n "cliclack") (r "^0.1.10") (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)))) (h "156laa48760i9vppq77qqz86a81zc9mr39wfmp0c376rcb8ahsis")))

(define-public crate-scaffold-godot-rust-0.1.4 (c (n "scaffold-godot-rust") (v "0.1.4") (d (list (d (n "cliclack") (r "^0.1.10") (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)))) (h "050chxij20v9a1vmw3sq8hb5wijyvy9lny5lanahvgma35sbjcm6")))

(define-public crate-scaffold-godot-rust-0.1.5 (c (n "scaffold-godot-rust") (v "0.1.5") (d (list (d (n "cliclack") (r "^0.1.10") (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)))) (h "05v7rr9i0sdwfvf15nhvcqdl95mn4krzchryffh8d0cj0920113m")))

(define-public crate-scaffold-godot-rust-0.1.6 (c (n "scaffold-godot-rust") (v "0.1.6") (d (list (d (n "clean-path") (r "^0.2.1") (d #t) (k 0)) (d (n "cliclack") (r "^0.1.10") (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)))) (h "1rh1ggf1x9i17b7568ldg0s7w6454pkv0g6r7vbd4cqxvdjcn5fm")))

