(define-module (crates-io sc ss scss_mass_compiler) #:use-module (crates-io))

(define-public crate-scss_mass_compiler-1.0.0 (c (n "scss_mass_compiler") (v "1.0.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "grass") (r "^0.11.0") (d #t) (k 0)))) (h "0l0c0rfpcmfd0rdyzyxkh9lyxw1c97s9dss0gp2c58ig16d5q0xs")))

(define-public crate-scss_mass_compiler-1.0.1 (c (n "scss_mass_compiler") (v "1.0.1") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "grass") (r "^0.11.0") (d #t) (k 0)))) (h "1i4m6rpvqv9m77b21sfv3rmp9b3y6ax27ywadz7f5jqyy3mglkah")))

(define-public crate-scss_mass_compiler-1.0.2 (c (n "scss_mass_compiler") (v "1.0.2") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "grass") (r "^0.11.0") (d #t) (k 0)))) (h "1rjx2ng1zk2aniicsc17jjfsizfqlsgzha4wy4kc7rabwxn9v7r3")))

(define-public crate-scss_mass_compiler-1.0.3 (c (n "scss_mass_compiler") (v "1.0.3") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "grass") (r "^0.11.0") (d #t) (k 0)))) (h "1wp9f8j1r3fxh0d3yxr2170lb0hr0pd83qwdcybmqs4slsd62g3m")))

(define-public crate-scss_mass_compiler-1.0.4 (c (n "scss_mass_compiler") (v "1.0.4") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "grass") (r "^0.11.0") (d #t) (k 0)))) (h "1i7mp0a9vcvyfsxh48n1ir8wmvq3rifrwp3hk3yfspszqnhz1f0m")))

