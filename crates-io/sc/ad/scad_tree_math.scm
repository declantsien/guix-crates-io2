(define-module (crates-io sc ad scad_tree_math) #:use-module (crates-io))

(define-public crate-scad_tree_math-0.1.0 (c (n "scad_tree_math") (v "0.1.0") (h "046w14p9d3368n5prfklh54k4d55is6f8jp0qfz32cjvffvw3bv5")))

(define-public crate-scad_tree_math-0.1.1 (c (n "scad_tree_math") (v "0.1.1") (h "078hprvxpqsg2m4q09rkc5092047wlih199jifqsff5pdslp8rkr")))

(define-public crate-scad_tree_math-0.1.2 (c (n "scad_tree_math") (v "0.1.2") (h "0rlmw1cymgjyxydijk16wf6ws8pzxws388pdqf4qfzha8klsw115")))

(define-public crate-scad_tree_math-0.1.3 (c (n "scad_tree_math") (v "0.1.3") (h "0yyk5bqi3k9rpf7fcsfal4d4f4jqch2qj4p9anr52k7lz50sfvyv")))

