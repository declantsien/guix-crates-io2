(define-module (crates-io sc ad scad_tree) #:use-module (crates-io))

(define-public crate-scad_tree-0.1.0 (c (n "scad_tree") (v "0.1.0") (d (list (d (n "scad_tree_math") (r "^0.1.0") (d #t) (k 0)))) (h "16zh6lw3bgaxsrk9zqg6m9rm0irf4mc93fyxmxspfq7kikax5ivd")))

(define-public crate-scad_tree-0.1.1 (c (n "scad_tree") (v "0.1.1") (d (list (d (n "scad_tree_math") (r "^0.1.0") (d #t) (k 0)))) (h "1i3gq2dswgvrgyais99i0qdc6h88ckrp97yvm28c318pll80bvss")))

(define-public crate-scad_tree-0.1.2 (c (n "scad_tree") (v "0.1.2") (d (list (d (n "scad_tree_math") (r "0.1.*") (d #t) (k 0)))) (h "0l12zhmnmqnd9q8spy8a8jy5gk8ja4jcqm5gjx5xnpkzncmzgg9w")))

(define-public crate-scad_tree-0.1.3 (c (n "scad_tree") (v "0.1.3") (d (list (d (n "scad_tree_math") (r "0.1.*") (d #t) (k 0)))) (h "0badbds1vx3k3jrps60jgwi1jkrlb0b2xblpzgg9vn2xhmphq55z")))

(define-public crate-scad_tree-0.1.4 (c (n "scad_tree") (v "0.1.4") (d (list (d (n "scad_tree_math") (r "0.1.*") (d #t) (k 0)))) (h "0ibba0j4j5h8v25l5xvp3ld7ia6nggba4xfzwhiiv4yiki8cw2s7")))

(define-public crate-scad_tree-0.2.0 (c (n "scad_tree") (v "0.2.0") (d (list (d (n "scad_tree_math") (r "0.1.*") (d #t) (k 0)))) (h "16cmrsrd923l1avjkp9pmybz8w23rga8kdn375cqdb6imk773gqp")))

(define-public crate-scad_tree-0.3.0 (c (n "scad_tree") (v "0.3.0") (d (list (d (n "scad_tree_math") (r "0.1.*") (d #t) (k 0)))) (h "0l3dl3g2j0s2cpscpjmxmbbnqwx0n30v99sirhzwpxkmm7lcrmdn")))

(define-public crate-scad_tree-0.3.1 (c (n "scad_tree") (v "0.3.1") (d (list (d (n "scad_tree_math") (r "0.1.*") (d #t) (k 0)))) (h "1gpyq4iajikdcp516zq4p3j2nlf1gj3hj4pb80c4bf39pf0ic8v0")))

(define-public crate-scad_tree-0.3.2 (c (n "scad_tree") (v "0.3.2") (d (list (d (n "scad_tree_math") (r "0.1.*") (d #t) (k 0)))) (h "1cllpsg9wv2fnwm3ffsy93i9drhnz0pzzy9vz2xlddx69jgmzahb")))

(define-public crate-scad_tree-0.3.3 (c (n "scad_tree") (v "0.3.3") (d (list (d (n "scad_tree_math") (r "0.1.*") (d #t) (k 0)))) (h "0hmdv4ms0gx570vvxs3pvx37bhwby5vgvq210kj7kw5cg1l4crzv")))

(define-public crate-scad_tree-0.4.0 (c (n "scad_tree") (v "0.4.0") (d (list (d (n "scad_tree_math") (r "0.1.*") (d #t) (k 0)))) (h "0n4cqramcm1s81wpk8mz2yd6f9f5ygpskiyyw4475w9na88f38sh")))

