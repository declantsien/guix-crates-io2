(define-module (crates-io sc ad scad) #:use-module (crates-io))

(define-public crate-scad-1.0.0 (c (n "scad") (v "1.0.0") (d (list (d (n "nalgebra") (r "^0.13") (d #t) (k 0)))) (h "0z1nc619cdp89548mbq4i2fir8a0akwg237dgs8y2407kwi10083")))

(define-public crate-scad-1.0.1 (c (n "scad") (v "1.0.1") (d (list (d (n "nalgebra") (r "^0.13") (d #t) (k 0)))) (h "10b03wrkyvfvvmslmwfgjqfm7zwnkdn76zl179gv1ppjnad3knwv")))

(define-public crate-scad-1.0.3 (c (n "scad") (v "1.0.3") (d (list (d (n "nalgebra") (r "^0.16.8") (d #t) (k 0)))) (h "0icw6yp50dlz6dvm09r2fd9yapirg324i64gl4vg8dbdwsflpxcw")))

(define-public crate-scad-1.1.0 (c (n "scad") (v "1.1.0") (d (list (d (n "nalgebra") (r "^0.16.8") (d #t) (k 0)))) (h "0pa8nbljg7alzcfs2snrpr5p2ly6mrq1j9hif382ial30q3by5z1")))

(define-public crate-scad-1.2.0 (c (n "scad") (v "1.2.0") (d (list (d (n "nalgebra") (r "^0.16.8") (d #t) (k 0)))) (h "1dn3gl2zdr1ymr69v4ar1s8a386wy7m1xjnn3f56q4lcmsqd8lpz") (y #t)))

(define-public crate-scad-1.2.1 (c (n "scad") (v "1.2.1") (d (list (d (n "nalgebra") (r "^0.16.8") (d #t) (k 0)))) (h "0jkiys26nlcs8cvz2hl21jypp9m5cd5nhhmrlvinwx17a6vlz5lr")))

(define-public crate-scad-1.2.2 (c (n "scad") (v "1.2.2") (d (list (d (n "nalgebra") (r "^0.16.8") (d #t) (k 0)))) (h "1yvy7ckfd7r261iywm75na1ykd9cl8h0q8ajb1iwg1jmnbs6vry6")))

