(define-module (crates-io sc op scoped_name) #:use-module (crates-io))

(define-public crate-scoped_name-0.2.0 (c (n "scoped_name") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "ustr") (r "^0.7.0") (d #t) (k 0)))) (h "126pr8kq57dzyrpxw7ajq31q3xbiqmlhjnw8xk0m8bh9bf7g4vkn")))

