(define-module (crates-io sc op scoped_thread_local) #:use-module (crates-io))

(define-public crate-scoped_thread_local-0.1.0 (c (n "scoped_thread_local") (v "0.1.0") (d (list (d (n "reborrow") (r "^0.5.5") (d #t) (k 0)) (d (n "scopeguard") (r "^1.2.0") (d #t) (k 0)))) (h "12a64g0knmbs8b707fxvd0qjhdkvn4h6cv0cfai5fcl5syp012fq")))

(define-public crate-scoped_thread_local-0.1.1 (c (n "scoped_thread_local") (v "0.1.1") (d (list (d (n "reborrow") (r "^0.5.5") (d #t) (k 0)) (d (n "scopeguard") (r "^1.2.0") (d #t) (k 0)))) (h "1ic5b24z352dsvhja9kgvqnhnhb1xc0fq9mrxb41lazwndpas81q")))

(define-public crate-scoped_thread_local-0.1.2 (c (n "scoped_thread_local") (v "0.1.2") (d (list (d (n "scopeguard") (r "^1.2.0") (d #t) (k 0)))) (h "0s21d9ck4s6hbavh6mc9gs59lxbskg309w4y1bn4ibbp9agj697y")))

(define-public crate-scoped_thread_local-0.1.4 (c (n "scoped_thread_local") (v "0.1.4") (h "13snbxf4wnxz63r93kx7q8l5i6qh0d3w5r3cr97ybc3ah376kv9d")))

(define-public crate-scoped_thread_local-0.1.5 (c (n "scoped_thread_local") (v "0.1.5") (h "10rw532wdk81xw1xfd1mr9h9z17s8xw63sv5lbc440gag6cvdvbl")))

(define-public crate-scoped_thread_local-0.1.6 (c (n "scoped_thread_local") (v "0.1.6") (h "0r7s6kd48wjaaj75sima2s49pr9wd5dnj6azvrxxd0fcsj0h6ci9")))

(define-public crate-scoped_thread_local-0.2.0 (c (n "scoped_thread_local") (v "0.2.0") (h "0zzk31wf6a993h3kw52d7hiqbnmndfymcqk2kskfwij42i2cmz0h") (y #t)))

(define-public crate-scoped_thread_local-0.3.0 (c (n "scoped_thread_local") (v "0.3.0") (h "0wpzb7pppii6x7lyhc65j3n54kgs177bsd1610n8hsb82fwqxyrd")))

