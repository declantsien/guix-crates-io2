(define-module (crates-io sc op scoped_tasks_prototype) #:use-module (crates-io))

(define-public crate-scoped_tasks_prototype-0.1.0 (c (n "scoped_tasks_prototype") (v "0.1.0") (d (list (d (n "futures") (r "^0.3.28") (d #t) (k 2)) (d (n "tokio") (r "^1.28.2") (f (quote ("rt" "macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0m5585i1wbxhsjnjk1kfp0l1gx007wf6lb6kf2ypq096vh6m9a44") (r "1.65")))

