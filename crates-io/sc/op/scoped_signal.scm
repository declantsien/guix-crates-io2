(define-module (crates-io sc op scoped_signal) #:use-module (crates-io))

(define-public crate-scoped_signal-0.1.0 (c (n "scoped_signal") (v "0.1.0") (d (list (d (n "array-macro") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.66") (d #t) (k 0)) (d (n "nix") (r "^0.17") (d #t) (k 0)))) (h "1jpp82i6fyv5bdm6k27zhnxjkyh6hn8b4jhhr88qxbksqp3cwkk8")))

(define-public crate-scoped_signal-0.1.1 (c (n "scoped_signal") (v "0.1.1") (d (list (d (n "array-macro") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.66") (d #t) (k 0)) (d (n "nix") (r "^0.17") (d #t) (k 0)))) (h "0699wmsyjl6vp9x1s7lln8fsqi2z1bnw8j63bl0b4p8i7y7wih3c")))

(define-public crate-scoped_signal-0.1.2 (c (n "scoped_signal") (v "0.1.2") (d (list (d (n "arr_macro") (r "^0.1.3") (d #t) (k 0)) (d (n "array-macro") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.66") (d #t) (k 0)) (d (n "nix") (r "^0.17") (d #t) (k 0)))) (h "1fd61qw5494raf5gxxh8jl1xqy1ha1khc9snmjf63f2b4q4qi902")))

