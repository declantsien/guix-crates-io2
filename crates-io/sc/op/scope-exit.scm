(define-module (crates-io sc op scope-exit) #:use-module (crates-io))

(define-public crate-scope-exit-0.1.0 (c (n "scope-exit") (v "0.1.0") (h "04j49ph4dry26rivwnxgz171fj56vcq7air4ikjp2ymc0qqyvpsg")))

(define-public crate-scope-exit-0.2.0 (c (n "scope-exit") (v "0.2.0") (h "16hflc3jfm8ckf061c6rwn8h3igh2dpmygb37qqqvi5nclv5d6r5")))

