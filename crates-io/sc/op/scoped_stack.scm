(define-module (crates-io sc op scoped_stack) #:use-module (crates-io))

(define-public crate-scoped_stack-1.0.0 (c (n "scoped_stack") (v "1.0.0") (h "08xjng8ifc760hlxhfljvcz6w71rsk1g7xkm8j1yzfp4bnv8syjn")))

(define-public crate-scoped_stack-1.0.1 (c (n "scoped_stack") (v "1.0.1") (h "0jbb8icndjzvx4dmjcklsb2daid6qld4cp8nywr0nij0ljsl9kia")))

(define-public crate-scoped_stack-1.0.2 (c (n "scoped_stack") (v "1.0.2") (h "07zvrsw5bwz5zc63wa0z31mg37z1jmmdld5qq27551s076d810rr")))

(define-public crate-scoped_stack-1.0.3 (c (n "scoped_stack") (v "1.0.3") (h "02akih5yyfgamj5mlcmdhfl6ccysc23225njjq7vs1c1hcvnz4rs")))

(define-public crate-scoped_stack-1.0.4 (c (n "scoped_stack") (v "1.0.4") (h "19rxlzv04gk8npjprmdlbg2jpyfscs6yafjdc51adw783pq6ajwm")))

(define-public crate-scoped_stack-1.0.5 (c (n "scoped_stack") (v "1.0.5") (h "040wipk3jbj1dk6ycfnyna04dagqs2rgmhzv2v0kadkwvjwywyz1")))

(define-public crate-scoped_stack-1.0.6 (c (n "scoped_stack") (v "1.0.6") (h "0ak77xaqhgy1k0rrxk4mxb55jih005lmx1504zphlgax36q8rq16")))

(define-public crate-scoped_stack-1.1.0 (c (n "scoped_stack") (v "1.1.0") (h "0ganah1x2x9gqvxf9yg4hh6cjwjw57axp1s1bqd58rlwx4970xlq")))

(define-public crate-scoped_stack-1.1.1 (c (n "scoped_stack") (v "1.1.1") (h "15chirm9hamnsp1vfafrkr1ib58kv2403s6va34ri1b9v24z3n4r")))

(define-public crate-scoped_stack-1.1.2 (c (n "scoped_stack") (v "1.1.2") (h "0247isrks28xap7kcq56zsh9js9qj4zc5h7i2k1syyr98mmhchb1")))

(define-public crate-scoped_stack-1.2.0 (c (n "scoped_stack") (v "1.2.0") (h "15fcwl1pnvb1m8j2hh6bi3xhjb38f0sjjs0qkxcjwgcbpg4zywg2")))

(define-public crate-scoped_stack-1.2.1 (c (n "scoped_stack") (v "1.2.1") (h "0mi79p57wmxfrbi4r64fg91z5w830ng2228dba38y4nya982fnas")))

