(define-module (crates-io sc op scoped-gc) #:use-module (crates-io))

(define-public crate-scoped-gc-0.1.0 (c (n "scoped-gc") (v "0.1.0") (h "0sw5qpb7mirf2jy934bc9bn60ln4x0w0mrhzyddd1npzgn2s3bcz")))

(define-public crate-scoped-gc-0.1.1 (c (n "scoped-gc") (v "0.1.1") (h "14cf4j7xfd2v7xbi4bims5g0a9m4fff5bllnxsdvjq5zpyz9w0d4")))

(define-public crate-scoped-gc-0.1.2 (c (n "scoped-gc") (v "0.1.2") (h "1hkcnl5frig24rmcjjmgsf8p3vdsnrsiv3hp92f8d2q0w88c524z")))

(define-public crate-scoped-gc-0.1.3 (c (n "scoped-gc") (v "0.1.3") (h "0kgz827c5ckjv9mbmsi5s7270md5ymkaz2rvyqf9k1i6n39xpy2s")))

(define-public crate-scoped-gc-0.1.4 (c (n "scoped-gc") (v "0.1.4") (h "0g35k3ksg1km0bq0kqi0v4w73i76nn87asw0pj5xjzifv70y1160")))

(define-public crate-scoped-gc-0.1.5 (c (n "scoped-gc") (v "0.1.5") (h "088qqmhmgpiv6yd2kdxsyp7i3sm13f6b8jjqh42dwpbb9lg3c0if")))

