(define-module (crates-io sc op scoped-trace) #:use-module (crates-io))

(define-public crate-scoped-trace-0.1.0 (c (n "scoped-trace") (v "0.1.0") (d (list (d (n "backtrace") (r "^0.3") (d #t) (k 0)) (d (n "indexmap") (r "^1.9.2") (d #t) (k 0)))) (h "0sz74zbnbi7sqnl53zby3gydpdv2q3d0ikq0jfla3my4xnxpmhkx") (r "1.61")))

