(define-module (crates-io sc op scoped_threadpool) #:use-module (crates-io))

(define-public crate-scoped_threadpool-0.1.0 (c (n "scoped_threadpool") (v "0.1.0") (d (list (d (n "lazy_static") (r "*") (d #t) (k 2)))) (h "0szx3zvja72p0pqg7ri14f707p2acr9sacp8v3bhrh7gs85hxwdl") (f (quote (("nightly"))))))

(define-public crate-scoped_threadpool-0.1.1 (c (n "scoped_threadpool") (v "0.1.1") (d (list (d (n "lazy_static") (r "*") (d #t) (k 2)))) (h "1kh43pilmmih5iykk7q76av685yign2rr6rgckhvwdbrhzz9c4v2") (f (quote (("nightly"))))))

(define-public crate-scoped_threadpool-0.1.2 (c (n "scoped_threadpool") (v "0.1.2") (d (list (d (n "lazy_static") (r "*") (d #t) (k 2)))) (h "1wbcsxgfwfj20xaw3zlpszfj5av8r3v6k30yiqhxicfvzhn3jylb") (f (quote (("nightly"))))))

(define-public crate-scoped_threadpool-0.1.3 (c (n "scoped_threadpool") (v "0.1.3") (d (list (d (n "lazy_static") (r "*") (d #t) (k 2)))) (h "0z9r63q7p0fkbinhmnx846hwqcq8884mchpp9fb0b3illqvgqxqs") (f (quote (("nightly"))))))

(define-public crate-scoped_threadpool-0.1.4 (c (n "scoped_threadpool") (v "0.1.4") (d (list (d (n "lazy_static") (r "*") (d #t) (k 2)))) (h "00njmq94k2mawcm3jp3dfc8b3hvnh5q9083rr8c8gdvr107ihc55") (f (quote (("nightly"))))))

(define-public crate-scoped_threadpool-0.1.5 (c (n "scoped_threadpool") (v "0.1.5") (d (list (d (n "lazy_static") (r "*") (d #t) (k 2)))) (h "0bvwa1w9yzy5l7lxacxf7ws13ri1mpy0l0a19952hlvzdlwjn7k3") (f (quote (("nightly"))))))

(define-public crate-scoped_threadpool-0.1.6 (c (n "scoped_threadpool") (v "0.1.6") (d (list (d (n "lazy_static") (r "*") (d #t) (k 2)) (d (n "rustc_version") (r "^0.1") (d #t) (k 1)))) (h "172vyijlfaad7zymqyllrs5iccrhrfm39vahnywxrn0sqbi28wnq") (f (quote (("nightly"))))))

(define-public crate-scoped_threadpool-0.1.7 (c (n "scoped_threadpool") (v "0.1.7") (d (list (d (n "lazy_static") (r "^0.1.15") (d #t) (k 2)))) (h "0nm8rp9ds93g29pbjywpcpr6nfmbgx19bs4njsmbg31yi749kwry") (f (quote (("nightly"))))))

(define-public crate-scoped_threadpool-0.1.8 (c (n "scoped_threadpool") (v "0.1.8") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 2)))) (h "1401ns7pgwk73drvfkibq7qky7c9c2cc8is8ac4ixw7g7kz5k92f") (f (quote (("nightly"))))))

(define-public crate-scoped_threadpool-0.1.9 (c (n "scoped_threadpool") (v "0.1.9") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 2)))) (h "1a26d3lk40s9mrf4imhbik7caahmw2jryhhb6vqv6fplbbgzal8x") (f (quote (("nightly"))))))

