(define-module (crates-io sc op scoped) #:use-module (crates-io))

(define-public crate-scoped-0.1.0 (c (n "scoped") (v "0.1.0") (h "06vk8ai5mpl1x7hfxgq5y0q2jvz4lcj6az0y5jqhbqk2pymmvgyk")))

(define-public crate-scoped-0.1.1 (c (n "scoped") (v "0.1.1") (h "0p7gz8s66nxih4lnhbm5j4algc6xxgpsgcnqskz8v27745q278b3")))

(define-public crate-scoped-0.1.2 (c (n "scoped") (v "0.1.2") (h "14lbl43ivswmhvhnkx0002c76y040frh2pnhwgcbp7yv10idkznk")))

