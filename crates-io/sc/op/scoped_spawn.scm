(define-module (crates-io sc op scoped_spawn) #:use-module (crates-io))

(define-public crate-scoped_spawn-0.1.0 (c (n "scoped_spawn") (v "0.1.0") (d (list (d (n "futures") (r "^0.3.3") (d #t) (k 0)))) (h "0wma51d30j5wvcjm75w6fg3pm7ja14c7bzf2hmhlkjrl5gijh3rp")))

(define-public crate-scoped_spawn-0.1.1 (c (n "scoped_spawn") (v "0.1.1") (d (list (d (n "futures") (r "^0.3.3") (f (quote ("alloc"))) (k 0)) (d (n "tokio") (r "^0.2.2") (f (quote ("macros" "rt-core" "time"))) (d #t) (k 2)))) (h "0zqcavsv5jncwvcic4hpvkj8gkgr5nk85dkd446bv9g8sly7d4a1")))

(define-public crate-scoped_spawn-0.2.0 (c (n "scoped_spawn") (v "0.2.0") (d (list (d (n "futures") (r "^0.3.3") (f (quote ("alloc"))) (k 0)) (d (n "tokio") (r "^0.2.2") (f (quote ("macros" "rt-core" "time"))) (d #t) (k 2)))) (h "0md7xz92hz0vd4w85qagdklif0g4qsg7g93csm6i8l3lacazfdnh")))

(define-public crate-scoped_spawn-0.2.1 (c (n "scoped_spawn") (v "0.2.1") (d (list (d (n "futures") (r "^0.3.3") (f (quote ("alloc"))) (k 0)) (d (n "tokio") (r "^0.2.2") (f (quote ("macros" "rt-core" "time"))) (d #t) (k 2)))) (h "08b6rpw60yvk1sv7g413wvbqh66s7vm8flwcjl4a3rqwm5avyxvf")))

