(define-module (crates-io sc op scope_gc) #:use-module (crates-io))

(define-public crate-scope_gc-0.1.0 (c (n "scope_gc") (v "0.1.0") (h "1z3c89x75g3p323lz27jh4nb6cv73hjxzynka4iia8wjynj8hhqn") (y #t)))

(define-public crate-scope_gc-0.1.1 (c (n "scope_gc") (v "0.1.1") (h "04x1x3m4qgzzszv137bf0pnapwhpjx3194rgn281p9jang2x4h22")))

(define-public crate-scope_gc-0.1.2 (c (n "scope_gc") (v "0.1.2") (h "1jfgigmx40bxkw5igc49yvi0pwwz6b6mz8wq669wn0s1x5vfxbyr")))

(define-public crate-scope_gc-0.2.0 (c (n "scope_gc") (v "0.2.0") (h "0qsrbhf2w5nisinjkykzkkxw3plb2lpv9l23ja9sb5zazyd9zbss")))

(define-public crate-scope_gc-0.2.1 (c (n "scope_gc") (v "0.2.1") (h "1ivqr09mywza3arhahsbgm1d0350kg6dcmbzjzgfn6nfi19fl60v")))

(define-public crate-scope_gc-0.2.2 (c (n "scope_gc") (v "0.2.2") (h "0n7zqa60iwa0jfnykxj20zkp0zi91dnx2w16m3rxp0h0dnbwajzq") (f (quote (("root_ref_coerce_unsized" "_nightly") ("_nightly"))))))

(define-public crate-scope_gc-0.2.3 (c (n "scope_gc") (v "0.2.3") (h "0022v0cs88q23lspawm35376whw9faim19sxn760l4mgjm1zm192") (f (quote (("root_ref_coerce_unsized" "_unsize" "_coerce_unsized") ("_unsize") ("_coerce_unsized"))))))

(define-public crate-scope_gc-0.2.5 (c (n "scope_gc") (v "0.2.5") (h "1mip50y47nqqss975k5xq59qfnw4yh76svd6bqnavn4ac1gmcdz0") (y #t)))

(define-public crate-scope_gc-0.2.6 (c (n "scope_gc") (v "0.2.6") (h "1clagm2pgj7ldhlk5mjvr9kg62qw9g38xlp48g5im7d4jg4amxky") (f (quote (("root_ref_coerce_unsized" "_unsize" "_coerce_unsized") ("_unsize") ("_coerce_unsized"))))))

