(define-module (crates-io sc op scoped-arena) #:use-module (crates-io))

(define-public crate-scoped-arena-0.1.0 (c (n "scoped-arena") (v "0.1.0") (h "0i0a747facfb4skd6ld7lji4dpvgwsxilwc62clysi84b84qnip1") (f (quote (("nightly") ("default" "alloc") ("alloc"))))))

(define-public crate-scoped-arena-0.1.1 (c (n "scoped-arena") (v "0.1.1") (h "1wws5bmy704vqiafd7ygg34ssirdmrswqmp75bfkmngdrvdpj27h") (f (quote (("nightly") ("default" "alloc") ("alloc"))))))

(define-public crate-scoped-arena-0.2.0 (c (n "scoped-arena") (v "0.2.0") (h "1xwxmp8x1bxmv5dgm91sir1hkwjv54s407mcq4r4fpdp2wpmlc23") (f (quote (("default" "alloc") ("allocator_api") ("alloc"))))))

(define-public crate-scoped-arena-0.2.1 (c (n "scoped-arena") (v "0.2.1") (h "09yxh8jcjhrv9j2qjvz1mf0dapax1is9lr87qrbhd2ajrhssa8nx") (f (quote (("default" "alloc") ("allocator_api") ("alloc"))))))

(define-public crate-scoped-arena-0.3.0 (c (n "scoped-arena") (v "0.3.0") (h "1gaa9n95lmyqbhhm9smpmlf2y59vkc7bgvqrv5xkxzviac1wjxsn") (f (quote (("default" "alloc") ("allocator_api") ("alloc"))))))

(define-public crate-scoped-arena-0.4.0 (c (n "scoped-arena") (v "0.4.0") (d (list (d (n "bumpalo") (r "^3.7") (f (quote ("allocator_api"))) (d #t) (k 2)) (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.0") (d #t) (k 2)) (d (n "quickcheck") (r "^1.0") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1v2y2hg18qpibxqk85nb7xxn90nby3bppr194v4049bfc0kjgkjr") (f (quote (("default" "alloc") ("allocator_api") ("alloc"))))))

(define-public crate-scoped-arena-0.4.1 (c (n "scoped-arena") (v "0.4.1") (d (list (d (n "bumpalo") (r "^3.7") (f (quote ("allocator_api"))) (d #t) (k 2)) (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.0") (d #t) (k 2)) (d (n "quickcheck") (r "^1.0") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0rhx1zayjnghdiljvfqnx4v4j1im56ccx3mkg3hbnfnmyb90ypb4") (f (quote (("default" "alloc") ("allocator_api") ("alloc"))))))

