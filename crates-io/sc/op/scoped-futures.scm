(define-module (crates-io sc op scoped-futures) #:use-module (crates-io))

(define-public crate-scoped-futures-0.1.0 (c (n "scoped-futures") (v "0.1.0") (d (list (d (n "futures") (r "^0.3.24") (f (quote ("alloc"))) (o #t) (d #t) (k 0)) (d (n "pin-utils") (r "^0.1.0") (d #t) (k 0)))) (h "13yc96vhaz714bygg0j57rv6h8i2ykndly5jzviccagd5qm2fwbn") (f (quote (("std" "futures"))))))

(define-public crate-scoped-futures-0.1.1 (c (n "scoped-futures") (v "0.1.1") (d (list (d (n "futures") (r "^0.3.24") (f (quote ("alloc"))) (o #t) (d #t) (k 0)) (d (n "pin-utils") (r "^0.1.0") (d #t) (k 0)))) (h "1fdg86lacgkvzrs46lkx5bb39ddghnrb9d8imlvvpj4iag7qdv0m") (f (quote (("std" "futures"))))))

(define-public crate-scoped-futures-0.1.2 (c (n "scoped-futures") (v "0.1.2") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.25") (f (quote ("executor"))) (k 2)) (d (n "pin-utils") (r "^0.1.0") (d #t) (k 0)))) (h "16jwr5sfpns04m28fl1vg8j0v71fnz1qc8z06j51gw3jf4i0alv5") (f (quote (("std"))))))

(define-public crate-scoped-futures-0.1.3 (c (n "scoped-futures") (v "0.1.3") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (f (quote ("executor"))) (k 2)) (d (n "pin-utils") (r "^0.1") (d #t) (k 0)))) (h "0rx4ggkh63d1syxp5xl9b84kxv4ix85j4qw7sfdhr59pqqj3wixi") (f (quote (("std") ("default" "std"))))))

