(define-module (crates-io sc op scoped-thread-pool) #:use-module (crates-io))

(define-public crate-scoped-thread-pool-1.0.1 (c (n "scoped-thread-pool") (v "1.0.1") (d (list (d (n "crossbeam") (r "^0.7") (d #t) (k 0)) (d (n "itertools") (r "^0.4") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "scopeguard") (r "^0.1") (d #t) (k 0)) (d (n "variance") (r "^0.1") (d #t) (k 0)))) (h "0w2w4ibjidinj0x1wwpi4k84wr7sbrbvrx0cvpqna2w7s2n3vyk3") (y #t)))

(define-public crate-scoped-thread-pool-1.0.2 (c (n "scoped-thread-pool") (v "1.0.2") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "itertools") (r "^0.4") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "scopeguard") (r "^0.1") (d #t) (k 0)) (d (n "variance") (r "^0.1") (d #t) (k 0)))) (h "0l3kqqci3kdadqjy4f0v8zcb6nvfr3zawli5zhzgcr87qnh45x54") (y #t)))

(define-public crate-scoped-thread-pool-1.0.3 (c (n "scoped-thread-pool") (v "1.0.3") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "scopeguard") (r "^1.1") (d #t) (k 0)) (d (n "variance") (r "^0.1") (d #t) (k 0)))) (h "119d5q6d3md342azvf3s4ywpl4mpkyss6x4b2k9xp322k3rbzw8q") (y #t)))

(define-public crate-scoped-thread-pool-1.0.4 (c (n "scoped-thread-pool") (v "1.0.4") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "scopeguard") (r "^1.1") (d #t) (k 0)) (d (n "variance") (r "^0.1") (d #t) (k 0)))) (h "1axj2p1563v5m2pl5dyxfa4ajdkbvz8w9bqw73f6bsfmc7xvcyzl")))

