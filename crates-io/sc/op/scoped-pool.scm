(define-module (crates-io sc op scoped-pool) #:use-module (crates-io))

(define-public crate-scoped-pool-0.1.0 (c (n "scoped-pool") (v "0.1.0") (d (list (d (n "crossbeam") (r "^0.2") (d #t) (k 0)) (d (n "variance") (r "^0.1") (d #t) (k 0)))) (h "09nkxvm45j2lpr8jw4fknl540av8adc48wqa7jnb6j0yf83j014b")))

(define-public crate-scoped-pool-0.1.1 (c (n "scoped-pool") (v "0.1.1") (d (list (d (n "crossbeam") (r "^0.2") (d #t) (k 0)) (d (n "variance") (r "^0.1") (d #t) (k 0)))) (h "1is3wvbqcmc2690s7ydf1h2pfx313rpkld6s0c0q2plhqwp53bz7")))

(define-public crate-scoped-pool-0.1.2 (c (n "scoped-pool") (v "0.1.2") (d (list (d (n "crossbeam") (r "^0.2") (d #t) (k 0)) (d (n "variance") (r "^0.1") (d #t) (k 0)))) (h "198lnrx9kkjd83i7ib82si7kaip0g1lia5qfnq2pvsh20ivc6g26")))

(define-public crate-scoped-pool-0.1.3 (c (n "scoped-pool") (v "0.1.3") (d (list (d (n "crossbeam") (r "^0.2") (d #t) (k 0)) (d (n "variance") (r "^0.1") (d #t) (k 0)))) (h "1vkxyw6z0wqm3rykwbz63pjgj811xrs4s0lgxs0jinlqzrcjh2vy")))

(define-public crate-scoped-pool-0.1.4 (c (n "scoped-pool") (v "0.1.4") (d (list (d (n "crossbeam") (r "^0.2") (d #t) (k 0)) (d (n "variance") (r "^0.1") (d #t) (k 0)))) (h "1amfc4gsy98g8kbmjbr1g72nngj7mqihgp0pg2mnn8anj18vjl4w")))

(define-public crate-scoped-pool-0.1.5 (c (n "scoped-pool") (v "0.1.5") (d (list (d (n "crossbeam") (r "^0.2") (d #t) (k 0)) (d (n "variance") (r "^0.1") (d #t) (k 0)))) (h "08f7d3vvhq53d0pv6yl5jcsx0ak3gqwjm75jmclbx1bc0qis9wap")))

(define-public crate-scoped-pool-0.1.6 (c (n "scoped-pool") (v "0.1.6") (d (list (d (n "crossbeam") (r "^0.2") (d #t) (k 0)) (d (n "variance") (r "^0.1") (d #t) (k 0)))) (h "0jj2nncf9dn7w20d2xl1g9426gs271lnsrrv3gigr2jm9vhi0hpk")))

(define-public crate-scoped-pool-0.1.7 (c (n "scoped-pool") (v "0.1.7") (d (list (d (n "crossbeam") (r "^0.2") (d #t) (k 0)) (d (n "scopeguard") (r "^0.1") (d #t) (k 0)) (d (n "variance") (r "^0.1") (d #t) (k 0)))) (h "1fwkrdd96bjnqd5gdz1w0kjpwhfx9gy45ygy0fx2v6n083my4pzg")))

(define-public crate-scoped-pool-0.1.8 (c (n "scoped-pool") (v "0.1.8") (d (list (d (n "crossbeam") (r "^0.2") (d #t) (k 0)) (d (n "scopeguard") (r "^0.1") (d #t) (k 0)) (d (n "variance") (r "^0.1") (d #t) (k 0)))) (h "1mgg4s4qdvlk3sgg2j85r3pzxr3zsg0dk20wsc3rw5w6ybv84xcv")))

(define-public crate-scoped-pool-0.1.9 (c (n "scoped-pool") (v "0.1.9") (d (list (d (n "crossbeam") (r "^0.2") (d #t) (k 0)) (d (n "itertools") (r "^0.4") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "scopeguard") (r "^0.1") (d #t) (k 0)) (d (n "variance") (r "^0.1") (d #t) (k 0)))) (h "1avf7ihgh72nd1nw15yzwrbmv770n3779l4x9m66m12j3my9cs2j")))

(define-public crate-scoped-pool-0.1.10 (c (n "scoped-pool") (v "0.1.10") (d (list (d (n "crossbeam") (r "^0.2") (d #t) (k 0)) (d (n "itertools") (r "^0.4") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "scopeguard") (r "^0.1") (d #t) (k 0)) (d (n "variance") (r "^0.1") (d #t) (k 0)))) (h "08dniyq2p0knwkafsc6aabxs3aa843d8m7z59553qcxrzy43n74i")))

(define-public crate-scoped-pool-1.0.0 (c (n "scoped-pool") (v "1.0.0") (d (list (d (n "crossbeam") (r "^0.2") (d #t) (k 0)) (d (n "itertools") (r "^0.4") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "scopeguard") (r "^0.1") (d #t) (k 0)) (d (n "variance") (r "^0.1") (d #t) (k 0)))) (h "0hqbq2k5idk7csrvivwvhvbs9g9jbl5cddfjkvjmqm04wwaklyl1")))

