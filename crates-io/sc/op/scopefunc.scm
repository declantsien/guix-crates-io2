(define-module (crates-io sc op scopefunc) #:use-module (crates-io))

(define-public crate-scopefunc-0.1.0 (c (n "scopefunc") (v "0.1.0") (h "1xxclpzc5ipdhhf2xkrxzc0n4kzr1w1spixcx76l4cb5gqv8h7wp")))

(define-public crate-scopefunc-0.1.1 (c (n "scopefunc") (v "0.1.1") (h "0qm7db39xrk7wx3zrnw0vghg84npzdpbpgknkfj12g73nc0x6f4q")))

