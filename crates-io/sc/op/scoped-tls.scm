(define-module (crates-io sc op scoped-tls) #:use-module (crates-io))

(define-public crate-scoped-tls-0.1.0 (c (n "scoped-tls") (v "0.1.0") (h "0z8lgy007ffc9rdl6rhl7jxx6imxx641fmm7i044bsb3y0nw45zl")))

(define-public crate-scoped-tls-0.1.1 (c (n "scoped-tls") (v "0.1.1") (h "190qj9zdxidsjc49d6blwycy3jcc36zs6x7lfqj9x234r4wx8x46") (f (quote (("nightly"))))))

(define-public crate-scoped-tls-0.1.2 (c (n "scoped-tls") (v "0.1.2") (h "0a2bn9d2mb07c6l16sadijy4p540g498zddfxyiq4rsqpwrglbrk") (f (quote (("nightly"))))))

(define-public crate-scoped-tls-1.0.0 (c (n "scoped-tls") (v "1.0.0") (h "1hj8lifzvivdb1z02lfnzkshpvk85nkgzxsy2hc0zky9wf894spa")))

(define-public crate-scoped-tls-1.0.1 (c (n "scoped-tls") (v "1.0.1") (h "15524h04mafihcvfpgxd8f4bgc3k95aclz8grjkg9a0rxcvn9kz1") (r "1.59")))

