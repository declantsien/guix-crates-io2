(define-module (crates-io sc op scope-guard) #:use-module (crates-io))

(define-public crate-scope-guard-1.0.0 (c (n "scope-guard") (v "1.0.0") (h "0d3c58jn19s6gldqp86icdmyrghvyxqldl1mni0s4jqmn1yp0mmb")))

(define-public crate-scope-guard-1.1.0 (c (n "scope-guard") (v "1.1.0") (h "18ngvr6n5rjmza4yni54x58bm9m00zj59qs4w4vsn0w059zhs8b5")))

(define-public crate-scope-guard-1.2.0 (c (n "scope-guard") (v "1.2.0") (h "0b08kvvcgndwrqg6qqi0zdf5jhr179q9m7qz97j5g8fkmc8dmkwa") (f (quote (("std"))))))

