(define-module (crates-io sc op scopelint) #:use-module (crates-io))

(define-public crate-scopelint-0.0.1 (c (n "scopelint") (v "0.0.1") (d (list (d (n "taplo") (r "^0.11.0") (d #t) (k 0)))) (h "0qw7j06lmhya710vx26v6dd90njf8iih1qdfaag4dm635sa8jr22")))

(define-public crate-scopelint-0.0.2 (c (n "scopelint") (v "0.0.2") (d (list (d (n "grep") (r "^0.2.10") (d #t) (k 0)) (d (n "taplo") (r "^0.11.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "135jcg4m17z80j8lmyss4gnqpyj9cn6bd0d8i5sivmn1nfa296gn")))

(define-public crate-scopelint-0.0.3 (c (n "scopelint") (v "0.0.3") (d (list (d (n "grep") (r "^0.2.10") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "taplo") (r "^0.11.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "1z984607qc7hsy704gws7wcilvnyrkbs77bdbkgyjb23kxv3qxr7")))

(define-public crate-scopelint-0.0.4 (c (n "scopelint") (v "0.0.4") (d (list (d (n "grep") (r "^0.2.10") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "taplo") (r "^0.11.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "1xsf226j8znp18d9mklfyspsa4csgc606zqmi7q290dvwma5yj3j")))

(define-public crate-scopelint-0.0.5 (c (n "scopelint") (v "0.0.5") (d (list (d (n "grep") (r "^0.2.10") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "taplo") (r "^0.11.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "04dlqimqv3dfqlaa19gcwa1afkn3ij55kwlwg0mnjgd7n2lavi2d")))

(define-public crate-scopelint-0.0.6 (c (n "scopelint") (v "0.0.6") (d (list (d (n "grep") (r "^0.2.10") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "taplo") (r "^0.11.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "0vdxa2lcnq8aydrnxv9aa3r77r5v43imfss4z790nr1p395vq8ly")))

(define-public crate-scopelint-0.0.7 (c (n "scopelint") (v "0.0.7") (d (list (d (n "grep") (r "^0.2.10") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "taplo") (r "^0.11.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "1rhbbw3qclyv7fb5bhizvhk322z8i9r5wxlc305qyq4ryyyrxg5r")))

(define-public crate-scopelint-0.0.8 (c (n "scopelint") (v "0.0.8") (d (list (d (n "grep") (r "^0.2.10") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "taplo") (r "^0.11.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "1wxpb0asyhhcm8f3kdzywyybaa18d1rz6wm71hjz33wkd1gmkdz1")))

(define-public crate-scopelint-0.0.9 (c (n "scopelint") (v "0.0.9") (d (list (d (n "grep") (r "^0.2.10") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "taplo") (r "^0.11.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "00ckk4b0bcbabqrgyn1df40300nn66n12sfl7jmg9451116khxwb")))

(define-public crate-scopelint-0.0.10 (c (n "scopelint") (v "0.0.10") (d (list (d (n "grep") (r "^0.2.10") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "taplo") (r "^0.11.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "0q9x0ba3vj23z9k0hxgk94i0hd6x7ypyn2rcnczk4gz2fq1r622g")))

(define-public crate-scopelint-0.0.11 (c (n "scopelint") (v "0.0.11") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "grep") (r "^0.2.10") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "taplo") (r "^0.11.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "1bmcmz4f1925vzryg9wdapnv955lb2260ji2fi1g0dl3xqc7zwwx")))

(define-public crate-scopelint-0.0.12 (c (n "scopelint") (v "0.0.12") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.16.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "solang-parser") (r "^0.1.18") (d #t) (k 0)) (d (n "taplo") (r "^0.11.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "1sr827lshr4qh7dxc4x02ldkgq6i9b3x3cdrdcw9w3lqb63ssa9s")))

(define-public crate-scopelint-0.0.13 (c (n "scopelint") (v "0.0.13") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.16.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "solang-parser") (r "^0.1.18") (d #t) (k 0)) (d (n "taplo") (r "^0.11.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "128f33i5f16gf3rfsphg98asxyi98x8jm419x76pnhg3yr8lrkz2")))

(define-public crate-scopelint-0.0.14 (c (n "scopelint") (v "0.0.14") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.16.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "solang-parser") (r "^0.1.18") (d #t) (k 0)) (d (n "taplo") (r "^0.11.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "00k5j3ahzgf3hnsm12n7aws0zdrisig6qph9m7nkrw740wx81a79")))

(define-public crate-scopelint-0.0.15 (c (n "scopelint") (v "0.0.15") (d (list (d (n "clap") (r "^4.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.16.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "solang-parser") (r "^0.1.18") (d #t) (k 0)) (d (n "taplo") (r "^0.11.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "0y77asnrrap70zs9ix0vnalcqw5738gfflyshi4fvyr9am81zrkx")))

(define-public crate-scopelint-0.0.16 (c (n "scopelint") (v "0.0.16") (d (list (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.16.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "solang-parser") (r "^0.2.3") (d #t) (k 0)) (d (n "taplo") (r "^0.11.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "0kbdmn64zn9lj1b0a8iw1pr1062yl4rrbzm63pp385ydqnvhaga3")))

(define-public crate-scopelint-0.0.17 (c (n "scopelint") (v "0.0.17") (d (list (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.16.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "solang-parser") (r "^0.2.3") (d #t) (k 0)) (d (n "taplo") (r "^0.11.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "09k72y6qfp9542mynzb9yh7qc3zb0f5scnyl83dav46x02criyz0")))

(define-public crate-scopelint-0.0.18 (c (n "scopelint") (v "0.0.18") (d (list (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.16.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "solang-parser") (r "^0.2.3") (d #t) (k 0)) (d (n "taplo") (r "^0.11.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "1s5yyk79r3imx3lkxnpxsny5xkc81xm141crd0lyj1hysqjkw8wq")))

(define-public crate-scopelint-0.0.19 (c (n "scopelint") (v "0.0.19") (d (list (d (n "clap") (r "^4.4.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.16.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "solang-parser") (r "^0.3.2") (d #t) (k 0)) (d (n "taplo") (r "^0.12.1") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "03y7ml0i63gphxv4mbq6qx84g4mdj6lmpzp3dc8n37wkz7asb63k")))

(define-public crate-scopelint-0.0.20 (c (n "scopelint") (v "0.0.20") (d (list (d (n "clap") (r "^4.4.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.16.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "solang-parser") (r "^0.3.2") (d #t) (k 0)) (d (n "taplo") (r "^0.12.1") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "0h4503ja1w7f33d0pwlpkjdvj7k6acgcjwg9l9xhw565s5860lx2")))

