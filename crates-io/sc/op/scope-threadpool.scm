(define-module (crates-io sc op scope-threadpool) #:use-module (crates-io))

(define-public crate-scope-threadpool-0.1.0 (c (n "scope-threadpool") (v "0.1.0") (h "1myl34xwwpl2zac4g77zkl3rmkrpanm5czi5v1mlv4lan0hg4cm1") (y #t)))

(define-public crate-scope-threadpool-0.2.0 (c (n "scope-threadpool") (v "0.2.0") (h "1pwg9ykizjr29iczf7s7hsfpcnbw8cklrfr3iml5nrgh5yxpi1h2") (y #t)))

(define-public crate-scope-threadpool-0.2.1 (c (n "scope-threadpool") (v "0.2.1") (h "0z505y2za1ljmlcnjywdlb0pgpkikaxh7q8hml04zs5aki9j2hcs") (y #t)))

(define-public crate-scope-threadpool-0.2.2 (c (n "scope-threadpool") (v "0.2.2") (h "02pmrfzj45sj2j8s6ijfarbsm9yl43wgipa8w0709nnip8csm54i") (y #t)))

(define-public crate-scope-threadpool-0.3.0 (c (n "scope-threadpool") (v "0.3.0") (h "1r7xq8v1rv6aagx8sfl08d7yqldxcfqd4awqaqly6qhzxbsr2bnw") (y #t)))

(define-public crate-scope-threadpool-0.3.1 (c (n "scope-threadpool") (v "0.3.1") (h "0n10i0x12hcfz461167h434lry0dr1j7dgz6s8jc2wqg9fwh4ny6")))

