(define-module (crates-io sc op scop) #:use-module (crates-io))

(define-public crate-scop-1.0.0 (c (n "scop") (v "1.0.0") (d (list (d (n "indexmap") (r "^1.6.1") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.119") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "1qp0b0ilsgfz1hc0m70i2ba2bq8ydm85gqv0dq3hxlnxlin16kx5")))

(define-public crate-scop-1.0.1 (c (n "scop") (v "1.0.1") (d (list (d (n "indexmap") (r "^1.6.1") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.119") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "0gw6cdgnkvz8x6glpkhvdyb74h0lw4533i2qkr8krh7pyra1na3c")))

(define-public crate-scop-1.0.2 (c (n "scop") (v "1.0.2") (d (list (d (n "indexmap") (r "^1.6.1") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.119") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "083ja3gwx8i6ywlijciqwibrw3qkh41d069wnbqhzdz5h32davnj")))

(define-public crate-scop-1.0.3 (c (n "scop") (v "1.0.3") (d (list (d (n "indexmap") (r "^1.6.1") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.119") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "095k6rsyywh20w4snil1k55kag02cljc7ran6bcp7idzs6z2l402")))

(define-public crate-scop-1.0.31 (c (n "scop") (v "1.0.31") (d (list (d (n "indexmap") (r "^1.6.1") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.119") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "0fglcbarrl5aqygfdl7a1j6688dgpssnyjabf21m35gk890hk16y")))

(define-public crate-scop-1.0.32 (c (n "scop") (v "1.0.32") (d (list (d (n "indexmap") (r "^1.6.1") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.119") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "0sbzhxm0mnnygahjbrq639xc8z5c3a974az24v7nnn55p4sx0nv5")))

(define-public crate-scop-1.0.33 (c (n "scop") (v "1.0.33") (d (list (d (n "indexmap") (r "^1.6.1") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.119") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "07z42rhj456sgavc5qmhg36g7jnr7dnr5akfx3rhjrdqpdy8m5ls")))

(define-public crate-scop-1.0.34 (c (n "scop") (v "1.0.34") (d (list (d (n "indexmap") (r "^1.6.1") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.119") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "03nbbkj7ggsp3wrz5z1qfqrznjvfgzj08mri9yr74lwlkf70nwvj")))

(define-public crate-scop-1.0.35 (c (n "scop") (v "1.0.35") (d (list (d (n "indexmap") (r "^1.6.1") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.119") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "13i03s5v6b4lyfxc0smnrfa3xkkc1ik5fckgkbqc7jqsf1npq4c7")))

(define-public crate-scop-1.0.36 (c (n "scop") (v "1.0.36") (d (list (d (n "indexmap") (r "^1.6.1") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.119") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "145gcnzhg3pqzc176rc31yx146rhbxfdqn3zsfin08jxd6a9n9by")))

(define-public crate-scop-1.0.37 (c (n "scop") (v "1.0.37") (d (list (d (n "indexmap") (r "^1.6.1") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.119") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "10n7yqilj3qkslwppnai0d3jwx7pkkgqjaf9whbkhd7ihksr25pq")))

(define-public crate-scop-1.0.38 (c (n "scop") (v "1.0.38") (d (list (d (n "indexmap") (r "^1.6.1") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.119") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "1yc02527qb11vmg4h9j9yjqfpdx9s344m3znfk7rz0d28amhb317")))

(define-public crate-scop-1.0.39 (c (n "scop") (v "1.0.39") (d (list (d (n "indexmap") (r "^1.6.1") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.119") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "1lckrx5sxdscjmzvqq05wk3xam819ag35gby4ia43vzxjqh2v4vf")))

(define-public crate-scop-1.0.40 (c (n "scop") (v "1.0.40") (d (list (d (n "indexmap") (r "^1.6.1") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.119") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "1337bs3gc4124yfr78hiib4gv8lv58ri6pb3jbzpr4kwmp38rwly")))

(define-public crate-scop-1.0.41 (c (n "scop") (v "1.0.41") (d (list (d (n "indexmap") (r "^1.6.1") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.119") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4" "stdweb"))) (d #t) (k 0)))) (h "0xd3mxszmlhs84zf6zd0raax9l1xg2dhqrb07i4j7yn0ak3x3mq7")))

(define-public crate-scop-1.0.42 (c (n "scop") (v "1.0.42") (d (list (d (n "indexmap") (r "^1.6.1") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.119") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4" "stdweb"))) (d #t) (k 0)))) (h "19n6n41l6wdl55lyw72i0psmnimpbghfidnbh10z6la9isg6smix")))

(define-public crate-scop-1.0.43 (c (n "scop") (v "1.0.43") (d (list (d (n "indexmap") (r "^1.6.1") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.119") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4" "stdweb"))) (d #t) (k 0)))) (h "0cksaqk5i1hah8zq3gw154bwi6bh1gbxj2hx6g73cnzf9zhxklvk")))

(define-public crate-scop-1.0.44 (c (n "scop") (v "1.0.44") (d (list (d (n "indexmap") (r "^1.6.1") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.119") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4" "stdweb"))) (d #t) (k 0)))) (h "1ingbvj4y11jn7spdy1ddxsxf71934zx6zmk8h0i7zz0mnkvkl6i")))

(define-public crate-scop-1.0.45 (c (n "scop") (v "1.0.45") (d (list (d (n "indexmap") (r "^1.6.1") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.119") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4" "stdweb"))) (d #t) (k 0)))) (h "1slfdl2l5ys3yr72yz3a43zz4zlyabrradqbps6bxdpgv7clr08q")))

(define-public crate-scop-1.0.46 (c (n "scop") (v "1.0.46") (d (list (d (n "indexmap") (r "^1.6.1") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.119") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4" "stdweb"))) (d #t) (k 0)))) (h "1p8a1nq114yk2d45wbz04600rnfvkgldffqx68fc6lzx3d56i47i")))

(define-public crate-scop-1.0.47 (c (n "scop") (v "1.0.47") (d (list (d (n "indexmap") (r "^1.6.1") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.119") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4" "stdweb"))) (d #t) (k 0)))) (h "0iihgaglrfbp03s2dd6kh7m9ngq6d04b80p49ir5fpj29dwfqli5")))

