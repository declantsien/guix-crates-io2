(define-module (crates-io sc op scope-lock) #:use-module (crates-io))

(define-public crate-scope-lock-0.1.0 (c (n "scope-lock") (v "0.1.0") (h "0vp1q8ifs6j8p01q6mb9h6ygcxbvqkj5ylc0vxq5c1kdb40rcx0p") (y #t)))

(define-public crate-scope-lock-0.2.0 (c (n "scope-lock") (v "0.2.0") (h "00f06wbn5iq5jlsb0syypc3y19pnfrr4ilb5hxbxcw53bc8i1x14") (y #t)))

(define-public crate-scope-lock-0.2.1 (c (n "scope-lock") (v "0.2.1") (d (list (d (n "parking_lot") (r "^0.12.2") (f (quote ("send_guard"))) (d #t) (k 0)))) (h "175vccww4vmk5d7kzmxdafp5b4qvxfy4fsab6yck9axx7dw1v13c") (y #t)))

(define-public crate-scope-lock-0.2.2 (c (n "scope-lock") (v "0.2.2") (d (list (d (n "parking_lot") (r "^0.12.2") (f (quote ("send_guard"))) (d #t) (k 0)))) (h "0zcg32xqiir0m413abslrdcxrz5v2ajg2qjnzgqr24r4aa1bf37j") (y #t)))

(define-public crate-scope-lock-0.2.3 (c (n "scope-lock") (v "0.2.3") (d (list (d (n "parking_lot") (r "^0.12.2") (f (quote ("send_guard"))) (d #t) (k 0)))) (h "1grgqcmipsjqfsxvwpvgs8pg9xaaa9vc73cxzw8k4d1yipivjdpg") (y #t)))

(define-public crate-scope-lock-0.2.4 (c (n "scope-lock") (v "0.2.4") (d (list (d (n "parking_lot") (r "^0.12.2") (f (quote ("send_guard"))) (d #t) (k 0)))) (h "0ly9h3340xl0mskd3yqf852pdh1fq2mqkp3dl3f5jmibvd0nypav")))

