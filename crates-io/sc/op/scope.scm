(define-module (crates-io sc op scope) #:use-module (crates-io))

(define-public crate-scope-0.0.1 (c (n "scope") (v "0.0.1") (h "18fhm477rzqfg465qailx9ngph0xrwm3dzz205ak9s5d7zabryqs")))

(define-public crate-scope-0.0.2 (c (n "scope") (v "0.0.2") (h "0ahqkdiw0i4065jkxyj6jppf6fqid7vfim3lm2n9j4pjjx07l3j1")))

