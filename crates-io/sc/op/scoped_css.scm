(define-module (crates-io sc op scoped_css) #:use-module (crates-io))

(define-public crate-scoped_css-0.0.1 (c (n "scoped_css") (v "0.0.1") (d (list (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "nom") (r "^5.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.1") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)))) (h "1cb1cdzhiap2a8qm9xmlhwjxvvk6s5mmx5v512dp8yji6bzz0sz8")))

