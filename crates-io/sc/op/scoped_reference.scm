(define-module (crates-io sc op scoped_reference) #:use-module (crates-io))

(define-public crate-scoped_reference-0.1.0 (c (n "scoped_reference") (v "0.1.0") (h "1q8liqx1d211rqwcgm1g4nryi71va2vk2d2iskrjxncw5p88alw5")))

(define-public crate-scoped_reference-0.1.1 (c (n "scoped_reference") (v "0.1.1") (h "0p2sv5zl8d319g1pxrv60mpas2d55ziz857zxjpbl8wx804zqyfi")))

(define-public crate-scoped_reference-0.1.2 (c (n "scoped_reference") (v "0.1.2") (h "06abf948saa73w6hwbfvr715vny10inlhisdqmq04mj15absc9fh")))

(define-public crate-scoped_reference-0.1.3 (c (n "scoped_reference") (v "0.1.3") (h "02f4s28znv2lcnrzrg4ycfyblmij6c2fd43s2wd0rma8d0qjk3j8") (f (quote (("std") ("default" "std"))))))

