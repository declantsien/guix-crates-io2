(define-module (crates-io sc op scoped-callback) #:use-module (crates-io))

(define-public crate-scoped-callback-0.1.0 (c (n "scoped-callback") (v "0.1.0") (d (list (d (n "futures-util") (r "^0.3") (d #t) (k 0)))) (h "0qqg2b8nrhgr8d4j41dk0fh2n8x2rcchnf6r9pvh9lk6md407vdn")))

(define-public crate-scoped-callback-0.1.1 (c (n "scoped-callback") (v "0.1.1") (d (list (d (n "futures-util") (r "^0.3") (d #t) (k 0)))) (h "1x1zmzq3i0n86avkkzri9ar9b5qgpicljjsjs8if4jrpvycmwsxd")))

(define-public crate-scoped-callback-0.2.0 (c (n "scoped-callback") (v "0.2.0") (d (list (d (n "futures-util") (r "^0.3") (o #t) (d #t) (k 0)))) (h "0wi29j63h0z37y20lm9sx6z5hl8a6sq3qga7f8rv5kwcq9phhwwb") (f (quote (("std") ("default" "async" "std") ("async" "futures-util"))))))

