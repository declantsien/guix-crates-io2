(define-module (crates-io sc op scoped_stateful_threadpool) #:use-module (crates-io))

(define-public crate-scoped_stateful_threadpool-0.1.8 (c (n "scoped_stateful_threadpool") (v "0.1.8") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 2)))) (h "1m7vxigmgm3pickv6xzdj2r5r2yk5i3rambk897k08py2a38gimg") (f (quote (("nightly")))) (y #t)))

