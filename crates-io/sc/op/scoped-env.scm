(define-module (crates-io sc op scoped-env) #:use-module (crates-io))

(define-public crate-scoped-env-1.0.0 (c (n "scoped-env") (v "1.0.0") (h "0s5lbpnmaclcfs7ic9327i03babm4j6rnq5fyjzx69spfk08dpkq")))

(define-public crate-scoped-env-2.0.0 (c (n "scoped-env") (v "2.0.0") (h "1wsa9lc3dx8zqradpccpy4kaj652h2zk2zc9gmwlnyjyvswhry8j")))

(define-public crate-scoped-env-2.1.0 (c (n "scoped-env") (v "2.1.0") (h "09n0q6v9plj1s3vmd8s3nh8wclcwirayrx0bnwdswn4hinkkhqx8")))

