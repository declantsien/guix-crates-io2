(define-module (crates-io sc op scope_timer) #:use-module (crates-io))

(define-public crate-scope_timer-0.1.0 (c (n "scope_timer") (v "0.1.0") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "18bdp38r48sy637nbfb4162fhbxp88v87x95hjkbf2dcm5pin2jm")))

(define-public crate-scope_timer-0.1.1 (c (n "scope_timer") (v "0.1.1") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "0k6wshjvg6jidwkvrckas0xw2dhvr46mgm5mxgjvc49yp75gsdh7")))

(define-public crate-scope_timer-0.2.0 (c (n "scope_timer") (v "0.2.0") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "1qk9kd4cryq0dpskw504h8qr32w3qlz2kd0rc29gxi1z2p17snya")))

(define-public crate-scope_timer-0.2.1 (c (n "scope_timer") (v "0.2.1") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "067jxr012a884syh5d6i3sp5wp88f6c9wpc1d9hghiqmgwhnqc0s") (y #t)))

(define-public crate-scope_timer-0.2.2 (c (n "scope_timer") (v "0.2.2") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "0m5k7jmkvnl6870rsc8a0sxl9cy5p55i93fh7zdl3jklmvhpw76q") (y #t)))

(define-public crate-scope_timer-0.2.3 (c (n "scope_timer") (v "0.2.3") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "1ygrvx56vqa1s4lahq2brznr78im89xjp78hgcnzhcinkbwi4kgb")))

