(define-module (crates-io sc op scoped_async_spawn) #:use-module (crates-io))

(define-public crate-scoped_async_spawn-0.1.0 (c (n "scoped_async_spawn") (v "0.1.0") (d (list (d (n "pin-cell") (r "^0.2.0") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2.9") (d #t) (k 0)) (d (n "pin-utils") (r "^0.1") (d #t) (k 2)) (d (n "pin-weak") (r "^1.1.0") (d #t) (k 0)) (d (n "scoped-tls") (r "^1.0.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.9.0") (f (quote ("union" "const_generics"))) (d #t) (k 0)))) (h "0sbw6x94i78k2scfn31gjjrbi0g03g67r7rnyfrvgzp7x2jhd7pi")))

