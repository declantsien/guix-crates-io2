(define-module (crates-io sc op scoped_allocator) #:use-module (crates-io))

(define-public crate-scoped_allocator-0.1.0 (c (n "scoped_allocator") (v "0.1.0") (h "1mmzf0s9rk2bdmnh2a8xxbli6gi33sx6cxamfi9vcms0bncwpr5n")))

(define-public crate-scoped_allocator-0.1.1 (c (n "scoped_allocator") (v "0.1.1") (h "1l3w0arkmffb9ka8mg6qvcybhbdfivafzhibg3q9v3v4yykdw1q9")))

(define-public crate-scoped_allocator-0.1.2 (c (n "scoped_allocator") (v "0.1.2") (h "0vhqxrn0zi3splw75ca9g37vqhrvsyhwq17f7n35ifyfrjp93lav")))

(define-public crate-scoped_allocator-0.1.3 (c (n "scoped_allocator") (v "0.1.3") (h "1l7zwbk9qkb3nnliadsgb8j13f4yjy1lk29q4vzfahfrk9y8ykpb")))

(define-public crate-scoped_allocator-0.1.4 (c (n "scoped_allocator") (v "0.1.4") (h "13a11zqnvm4kw4l5sk9apy6i5vqc6dxqvwng2af2n9cd3014k5m4")))

