(define-module (crates-io sc op scoped-threadpool-std) #:use-module (crates-io))

(define-public crate-scoped-threadpool-std-0.1.0 (c (n "scoped-threadpool-std") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.121") (d #t) (k 0)) (d (n "pwhash") (r "^1") (d #t) (k 0)))) (h "0hpjbcs2247mp51n3zywxm53dvd8f6r5zx4q983shfz0ah4fj4ha")))

(define-public crate-scoped-threadpool-std-0.1.1 (c (n "scoped-threadpool-std") (v "0.1.1") (d (list (d (n "pwhash") (r "^1") (d #t) (k 2)))) (h "0n2smwm2hrwdcw8i4qdc8lq21wfjqyqrjsknqycq5c8q770lzzcy")))

