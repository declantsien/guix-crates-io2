(define-module (crates-io sc op scopetime) #:use-module (crates-io))

(define-public crate-scopetime-0.1.0 (c (n "scopetime") (v "0.1.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1ln1vjjs0cc7kvzcmsl9fb0ykb71gqfms4iv0hs7ccay3nqngic9") (f (quote (("enabled") ("default"))))))

(define-public crate-scopetime-0.1.1 (c (n "scopetime") (v "0.1.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0s4fg704zlc1xmwx2y5zifhndpb2ahfh4kg966gyr1kbwcd2hz0q") (f (quote (("enabled") ("default"))))))

(define-public crate-scopetime-0.1.2 (c (n "scopetime") (v "0.1.2") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1ngkasx6l072cvigx7q5r33i8acjardr4g8jnwdrcym4758f5vb6") (f (quote (("enabled") ("default"))))))

