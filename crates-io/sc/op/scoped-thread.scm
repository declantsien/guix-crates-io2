(define-module (crates-io sc op scoped-thread) #:use-module (crates-io))

(define-public crate-scoped-thread-0.1.0 (c (n "scoped-thread") (v "0.1.0") (h "0lx61xfnkkkm9kd844a7n299zpk0dx8836bbaz7li69drgs3sz7y")))

(define-public crate-scoped-thread-0.1.1 (c (n "scoped-thread") (v "0.1.1") (h "0y20azsfp9nayhzhx59x38kq2s4xiiivivbg087adnfixy4s77ac")))

