(define-module (crates-io sc op scopeguard) #:use-module (crates-io))

(define-public crate-scopeguard-0.1.0 (c (n "scopeguard") (v "0.1.0") (h "1d3rxqgv4kpgg9rg7iw2q547csl2hzny3v4m1ndb16ca4pz2p6bk")))

(define-public crate-scopeguard-0.1.1 (c (n "scopeguard") (v "0.1.1") (h "0yc9hk3nfkk3iyd703x9q9i1hiia9vmr9vghgjrskxmn7h80460n")))

(define-public crate-scopeguard-0.1.2 (c (n "scopeguard") (v "0.1.2") (h "0mvc3ac86gmfnymmvjsdvl8djrb9xr8m2n6yv1hwab8yghapd82r")))

(define-public crate-scopeguard-0.2.0 (c (n "scopeguard") (v "0.2.0") (h "12i95gcv612pkb6g19b65910aq1h93gb6lza70qjbyigh7jwf72m")))

(define-public crate-scopeguard-0.3.0 (c (n "scopeguard") (v "0.3.0") (h "0vbi63vqi424g545883p7kkmig8lzkbnp03m5vflfb5xkyk8w46f") (f (quote (("use_std") ("default" "use_std")))) (y #t)))

(define-public crate-scopeguard-0.3.1 (c (n "scopeguard") (v "0.3.1") (h "1frh1bf69kikkdhqfav7klbzfbdwzsaqb74wyvqlcwyzjg9di9bs") (f (quote (("use_std") ("default" "use_std"))))))

(define-public crate-scopeguard-0.3.2 (c (n "scopeguard") (v "0.3.2") (h "06799fck72ba7rkda5099jp8xgc8bfng7rw0v9y51hjbmk1v57n7") (f (quote (("use_std") ("default" "use_std"))))))

(define-public crate-scopeguard-0.3.3 (c (n "scopeguard") (v "0.3.3") (h "09sy9wbqp409pkwmqni40qmwa99ldqpl48pp95m1xw8sc19qy9cl") (f (quote (("use_std") ("default" "use_std"))))))

(define-public crate-scopeguard-1.0.0 (c (n "scopeguard") (v "1.0.0") (h "03aay84r1f6w87ckbpj6cc4rnsxkxcfs13n5ynxjia0qkgjiabml") (f (quote (("use_std") ("default" "use_std"))))))

(define-public crate-scopeguard-1.1.0 (c (n "scopeguard") (v "1.1.0") (h "1kbqm85v43rq92vx7hfiay6pmcga03vrjbbfwqpyj3pwsg3b16nj") (f (quote (("use_std") ("default" "use_std"))))))

(define-public crate-scopeguard-1.2.0 (c (n "scopeguard") (v "1.2.0") (h "0jcz9sd47zlsgcnm1hdw0664krxwb5gczlif4qngj2aif8vky54l") (f (quote (("use_std") ("default" "use_std"))))))

