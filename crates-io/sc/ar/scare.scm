(define-module (crates-io sc ar scare) #:use-module (crates-io))

(define-public crate-scare-1.0.8 (c (n "scare") (v "1.0.8") (d (list (d (n "console") (r "^0.15.7") (d #t) (k 0)))) (h "1kypw9z2lnvglghp463v9sw2s3mlh5rhzszgnx9rq0scw1avjg3l")))

(define-public crate-scare-1.1.0 (c (n "scare") (v "1.1.0") (d (list (d (n "console") (r "^0.15.7") (d #t) (k 0)))) (h "0ih3x79kl0ji53nx2klx2ichqjsgcpw3gpj1n2zfmpqi4xi7rn9a")))

