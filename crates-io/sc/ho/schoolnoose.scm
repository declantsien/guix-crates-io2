(define-module (crates-io sc ho schoolnoose) #:use-module (crates-io))

(define-public crate-schoolnoose-0.0.1 (c (n "schoolnoose") (v "0.0.1") (d (list (d (n "reqwest") (r "^0.10") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1qxf3kdvx3sas79mc5id3a2mgqx7x2ga83dsbdi1kjr551x8nv7j") (y #t)))

(define-public crate-schoolnoose-0.0.2 (c (n "schoolnoose") (v "0.0.2") (d (list (d (n "reqwest") (r "^0.10") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "18pas5pmqvcprjxckjwi8isysir7ddxl2z3z6d7m5jbmknvs564z") (y #t)))

(define-public crate-schoolnoose-0.0.5 (c (n "schoolnoose") (v "0.0.5") (d (list (d (n "awc") (r "^1.0") (f (quote ("openssl"))) (d #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1ffj7b2vv6qdylaz2yk24czhv9m9p9xvc9wdhwxg7szwa5ckxdc2") (y #t)))

(define-public crate-schoolnoose-1.0.0 (c (n "schoolnoose") (v "1.0.0") (h "09bwxdprcy135lfda6km69k9bm4k809d27cv5mwddy8l83f37bsb")))

