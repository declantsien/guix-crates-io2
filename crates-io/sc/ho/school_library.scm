(define-module (crates-io sc ho school_library) #:use-module (crates-io))

(define-public crate-school_library-1.0.0 (c (n "school_library") (v "1.0.0") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1h922583z87mm6l3s7pwd5izihhib4xb1nl0m7jqqnh3gxqfr4hi")))

(define-public crate-school_library-1.0.1 (c (n "school_library") (v "1.0.1") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0kx3nld3hn872bg206gdyds8hqlc1frwifiwcl81xp3739d3r8iv") (y #t)))

(define-public crate-school_library-1.0.2 (c (n "school_library") (v "1.0.2") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "11g2k02x47kk3vb75hrgc1izbsxhlr33ay6p6if562ik347xijyg")))

(define-public crate-school_library-1.0.3 (c (n "school_library") (v "1.0.3") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "195ndqk0lj84yizgs8pdnj6ycr30cxsnx675nn7dqa75frfrb7kx")))

