(define-module (crates-io sc ho schoenerd) #:use-module (crates-io))

(define-public crate-schoenerd-0.1.0 (c (n "schoenerd") (v "0.1.0") (d (list (d (n "clap") (r "^4.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap") (r "^4.3") (f (quote ("derive"))) (d #t) (k 1)) (d (n "clap_complete") (r "^4.3") (d #t) (k 1)) (d (n "clap_mangen") (r "^0.2") (d #t) (k 1)) (d (n "csv") (r "^1.2") (d #t) (k 0)) (d (n "csv") (r "^1.2") (d #t) (k 1)) (d (n "prettytable-rs") (r "^0.10") (d #t) (k 0)))) (h "02g7ifh5r6cdb6zbxxw2rwjx34kjhbqv48vfvkqvlwg6lsdy7hbg") (r "1.70.0")))

