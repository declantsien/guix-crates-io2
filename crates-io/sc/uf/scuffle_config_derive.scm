(define-module (crates-io sc uf scuffle_config_derive) #:use-module (crates-io))

(define-public crate-scuffle_config_derive-0.0.1 (c (n "scuffle_config_derive") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0xvz8d7rn5d8yqqga9wb2fnq0w69rdv1q69yv8g81rldxrpqrxf2")))

