(define-module (crates-io sc te scte35) #:use-module (crates-io))

(define-public crate-scte35-0.1.0 (c (n "scte35") (v "0.1.0") (d (list (d (n "bitstream-io") (r "^1.3.0") (d #t) (k 0)) (d (n "crc") (r "^3.0.0") (d #t) (k 0)))) (h "0f7cn3q62v8kyvf5h4gbk3mv2hqz130ay9y476h1prky6xgjj2d9")))

