(define-module (crates-io sc ul scule) #:use-module (crates-io))

(define-public crate-scule-1.0.0 (c (n "scule") (v "1.0.0") (d (list (d (n "docopt") (r "~0.6.11") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "09qq9rxh7lfkgr8046vjfrirwlr4dnyygvgf8vlvkr2sshx9bmqz")))

