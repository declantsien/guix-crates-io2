(define-module (crates-io sc -p sc-proposer-metrics) #:use-module (crates-io))

(define-public crate-sc-proposer-metrics-0.8.0-rc1 (c (n "sc-proposer-metrics") (v "0.8.0-rc1") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "prometheus-endpoint") (r "^0.8.0-rc1") (d #t) (k 0) (p "substrate-prometheus-endpoint")))) (h "0ib1wpim9vq0qp4c8p3wxg93g2ar5iw1fa117r8yn21brskjw88c")))

(define-public crate-sc-proposer-metrics-0.8.0-rc2 (c (n "sc-proposer-metrics") (v "0.8.0-rc2") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "prometheus-endpoint") (r "^0.8.0-rc2") (d #t) (k 0) (p "substrate-prometheus-endpoint")))) (h "0xvh9ig3fwpq6ng59r3wc1715im9qnh9hpr6sl3zylz2w5wkw7q4")))

(define-public crate-sc-proposer-metrics-0.8.0-rc3 (c (n "sc-proposer-metrics") (v "0.8.0-rc3") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "prometheus-endpoint") (r "^0.8.0-rc3") (d #t) (k 0) (p "substrate-prometheus-endpoint")))) (h "16sf16s1gs04lz7p5klwxi2h8r6wn5wypip8kmwcacgnibb4zs0f")))

(define-public crate-sc-proposer-metrics-0.8.0-rc4 (c (n "sc-proposer-metrics") (v "0.8.0-rc4") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "prometheus-endpoint") (r "^0.8.0-rc4") (d #t) (k 0) (p "substrate-prometheus-endpoint")))) (h "18xbhc6198c4c5xpwlaj7h8gadybnlkjk8dgzln7vkfgg5wm2jsy")))

(define-public crate-sc-proposer-metrics-0.8.0-rc5 (c (n "sc-proposer-metrics") (v "0.8.0-rc5") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "prometheus-endpoint") (r "^0.8.0-rc5") (d #t) (k 0) (p "substrate-prometheus-endpoint")))) (h "0z44ycbdffd4d7qk69qhn57ynppxzncggjcrcb0is2qvrd0h54g4")))

(define-public crate-sc-proposer-metrics-0.8.0-rc6 (c (n "sc-proposer-metrics") (v "0.8.0-rc6") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "prometheus-endpoint") (r "^0.8.0-rc6") (d #t) (k 0) (p "substrate-prometheus-endpoint")))) (h "0ydm7wpf4wxwxrvyq4gw3njc4zkqy4cfghs0n7z3g0c40cpcypsw")))

(define-public crate-sc-proposer-metrics-0.8.0 (c (n "sc-proposer-metrics") (v "0.8.0") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "prometheus-endpoint") (r "^0.8.0") (d #t) (k 0) (p "substrate-prometheus-endpoint")))) (h "0vrcqa6wa33hb0k4i38cn06249j1cd7l7l9j9ayv85mmh0j98b6l")))

(define-public crate-sc-proposer-metrics-0.8.1 (c (n "sc-proposer-metrics") (v "0.8.1") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "prometheus-endpoint") (r "^0.8.0") (d #t) (k 0) (p "substrate-prometheus-endpoint")))) (h "0s51z9dhm62g2la175grv6jrki259afi7cbi2kxhyn2pfs4iwkn8")))

(define-public crate-sc-proposer-metrics-0.9.0 (c (n "sc-proposer-metrics") (v "0.9.0") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "prometheus-endpoint") (r "^0.9.0") (d #t) (k 0) (p "substrate-prometheus-endpoint")))) (h "0gng9lj4j687z4p71p1rybdkr6mda0xpgsfc148wn3nwqmn2rz7c")))

(define-public crate-sc-proposer-metrics-0.10.0 (c (n "sc-proposer-metrics") (v "0.10.0") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "prometheus-endpoint") (r "^0.10.0") (d #t) (k 0) (p "substrate-prometheus-endpoint")))) (h "115qkzxqdqdwgb4ap9miac0y7yrpm7a2bsnr5612z6242wnzc6pg")))

(define-public crate-sc-proposer-metrics-0.11.0 (c (n "sc-proposer-metrics") (v "0.11.0") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "prometheus-endpoint") (r "^0.11.0") (d #t) (k 0) (p "substrate-prometheus-endpoint")))) (h "01nzw5k5dnc0si87293alm69pi3qamlwsqdg80cyxbb1gcwgj2i8")))

(define-public crate-sc-proposer-metrics-0.12.0 (c (n "sc-proposer-metrics") (v "0.12.0") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "prometheus-endpoint") (r "^0.12.0") (d #t) (k 0) (p "substrate-prometheus-endpoint")))) (h "1pnhyrkxsaadf850dysmw4zhkl7kizw4wdw6zci7g3gdrkf24y9c")))

(define-public crate-sc-proposer-metrics-0.13.0-dev1 (c (n "sc-proposer-metrics") (v "0.13.0-dev1") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "prometheus-endpoint") (r "^0.13.0-dev1") (d #t) (k 0) (p "substrate-prometheus-endpoint")))) (h "09pzbmkrmpqh5kag1rdv377agb5wg65pxwnc08d0cvhpni6hkpnd")))

(define-public crate-sc-proposer-metrics-0.13.0-dev.2 (c (n "sc-proposer-metrics") (v "0.13.0-dev.2") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "prometheus-endpoint") (r "^0.13.0-dev.2") (d #t) (k 0) (p "substrate-prometheus-endpoint")))) (h "141by360jyyqqjlpwrvmsahwzv425jq3fxyq8w4x02fjy88vwc3c")))

(define-public crate-sc-proposer-metrics-0.13.0-dev.3 (c (n "sc-proposer-metrics") (v "0.13.0-dev.3") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "prometheus-endpoint") (r "^0.13.0-dev.3") (d #t) (k 0) (p "substrate-prometheus-endpoint")))) (h "0m4r21xcv6pf9gqmy2cl4vc9axm3kynvq2y9c6m7w0xp5v93x0mm")))

(define-public crate-sc-proposer-metrics-0.13.0-dev.4 (c (n "sc-proposer-metrics") (v "0.13.0-dev.4") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "prometheus-endpoint") (r "^0.13.0-dev.4") (d #t) (k 0) (p "substrate-prometheus-endpoint")))) (h "1h0rcxpi8q47g005q00ppc3jkwjx471icnqbfb6285rvv7xj1g5n")))

(define-public crate-sc-proposer-metrics-0.13.0-dev.5 (c (n "sc-proposer-metrics") (v "0.13.0-dev.5") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "prometheus-endpoint") (r "=0.13.0-dev.5") (d #t) (k 0) (p "substrate-prometheus-endpoint")))) (h "18z3lb6xr9ii9g22nkszbvbzsnzimr9ka6lpbnk5qlfq6axh71fi")))

(define-public crate-sc-proposer-metrics-0.13.0-dev.6 (c (n "sc-proposer-metrics") (v "0.13.0-dev.6") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "prometheus-endpoint") (r "=0.13.0-dev.6") (d #t) (k 0) (p "substrate-prometheus-endpoint")))) (h "1n7a0r6b1j2wgbhwbcylqf1m2v3minfgdd27rnayqdl0rjp6jmgy")))

(define-public crate-sc-proposer-metrics-0.13.0 (c (n "sc-proposer-metrics") (v "0.13.0") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "prometheus-endpoint") (r "^0.13.0") (d #t) (k 0) (p "substrate-prometheus-endpoint")))) (h "0vy60gph7w3d3cjra72gxvz0v10syqqgxf666p0jzdfv3vsmkflf")))

(define-public crate-sc-proposer-metrics-0.14.0 (c (n "sc-proposer-metrics") (v "0.14.0") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "prometheus-endpoint") (r "^0.14.0") (d #t) (k 0) (p "substrate-prometheus-endpoint")))) (h "1hcv6mm2grsdlnhjayjlc5jv8calp1p61wzl0bbqhsc542rl9mfn")))

(define-public crate-sc-proposer-metrics-0.15.0-dev.1 (c (n "sc-proposer-metrics") (v "0.15.0-dev.1") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "prometheus-endpoint") (r "=0.15.0-dev.1") (d #t) (k 0) (p "substrate-prometheus-endpoint")))) (h "0jdnfl7k7xm7rdfkgmwjl7kinnmdql17qms45hjxra8asp92l97a")))

(define-public crate-sc-proposer-metrics-0.15.0 (c (n "sc-proposer-metrics") (v "0.15.0") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "prometheus-endpoint") (r "^0.15.0") (d #t) (k 0) (p "substrate-prometheus-endpoint")))) (h "1rvf1qmy1rfrsfhlwgyappwl99ympf7gfi6zli79v9229b3kbz7n")))

(define-public crate-sc-proposer-metrics-0.16.0 (c (n "sc-proposer-metrics") (v "0.16.0") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "prometheus-endpoint") (r "^0.16.0") (d #t) (k 0) (p "substrate-prometheus-endpoint")))) (h "16w75zvcx98swy3ab4pgkbrcyxxkk6332wszr9zbbbg7wkf4a612")))

(define-public crate-sc-proposer-metrics-0.17.0 (c (n "sc-proposer-metrics") (v "0.17.0") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "prometheus-endpoint") (r "^0.17.0") (d #t) (k 0) (p "substrate-prometheus-endpoint")))) (h "07110zypzzwm0ybf755xyf9cgpp1bp6smmna116nwcasmsrav3gv")))

(define-public crate-sc-proposer-metrics-0.18.0 (c (n "sc-proposer-metrics") (v "0.18.0") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "prometheus-endpoint") (r "^0.17.0") (d #t) (k 0) (p "substrate-prometheus-endpoint")))) (h "0g4dq9wx1b0ahcqa16w5i45ggmc5p9v66914hs4ikavxssza107n")))

