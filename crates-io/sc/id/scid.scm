(define-module (crates-io sc id scid) #:use-module (crates-io))

(define-public crate-scid-0.3.0 (c (n "scid") (v "0.3.0") (d (list (d (n "integer-encoding") (r "~1.0.3") (d #t) (k 0)) (d (n "multibase") (r "~0.6.0") (d #t) (k 0)) (d (n "multihash") (r "~0.8.0") (d #t) (k 0)))) (h "00xbd1rl88370hn9iqfx00bv9f4izy90y9dbx1x217d6z1g4mima")))

