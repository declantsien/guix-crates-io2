(define-module (crates-io sc id scidec) #:use-module (crates-io))

(define-public crate-scidec-0.0.1 (c (n "scidec") (v "0.0.1") (h "0qwijkwp0gnl2v3sqgff3lb7j9a3zc2llpmj8kgrx1psf7bbnqdk")))

(define-public crate-scidec-0.1.0 (c (n "scidec") (v "0.1.0") (h "0cc3rlfg6jv70v7ay7n0lmqxmmnhv8v6mxki4cw8s5pa6n7zlcah") (f (quote (("default") ("coverage"))))))

(define-public crate-scidec-0.1.1 (c (n "scidec") (v "0.1.1") (h "0qjdkw3ig5w0j7yx0vdlw4if9rk4h6wyynr7mfgd988hy0dhq34g") (f (quote (("default") ("coverage"))))))

(define-public crate-scidec-0.1.2 (c (n "scidec") (v "0.1.2") (h "1xqi6as5gad0jvrm8zzjiykcad0a3bcym6py7cjvx2a3fc7zrwph") (f (quote (("default") ("coverage"))))))

(define-public crate-scidec-0.1.3 (c (n "scidec") (v "0.1.3") (h "1pwlh1rbas3rz9bx8hqwadiknwdj0az0v8d6f2zx9cbbryg1vwik") (f (quote (("default") ("coverage"))))))

(define-public crate-scidec-0.1.4 (c (n "scidec") (v "0.1.4") (h "0k092ny8qwyiqnnimp89ff3a55yv11h7wr5cr7xxw1jfvim8r4cg")))

(define-public crate-scidec-0.1.5 (c (n "scidec") (v "0.1.5") (h "1h9cih6adz1cayjb08wc5rxqmb796lzsg3jkw5nivaw01ccdl14d")))

(define-public crate-scidec-0.1.7 (c (n "scidec") (v "0.1.7") (h "0066x730q2bi79pigxsmz2f69vdai1kix1wp3xin2g6s0svvc7za")))

