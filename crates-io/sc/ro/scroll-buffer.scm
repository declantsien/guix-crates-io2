(define-module (crates-io sc ro scroll-buffer) #:use-module (crates-io))

(define-public crate-scroll-buffer-0.1.0 (c (n "scroll-buffer") (v "0.1.0") (d (list (d (n "doc-comment") (r "^0.3.3") (d #t) (k 2)) (d (n "scroll") (r "^0.11.0") (d #t) (k 0)))) (h "0scmpnzr2ncf3iilwbmqlp4z0h5ab1g3h188zlfxvi7sqlacbd09")))

(define-public crate-scroll-buffer-0.2.0 (c (n "scroll-buffer") (v "0.2.0") (d (list (d (n "doc-comment") (r "^0.3.3") (d #t) (k 2)) (d (n "scroll") (r "^0.11.0") (d #t) (k 0)))) (h "1r8fp1qz666jd83dwk20cc1aizvp6q25kplfzjnbnmlsa3qq0bds")))

(define-public crate-scroll-buffer-0.3.0 (c (n "scroll-buffer") (v "0.3.0") (d (list (d (n "doc-comment") (r "^0.3.3") (d #t) (k 2)) (d (n "scroll") (r "^0.11.0") (d #t) (k 0)))) (h "0jlwm6y38mrbjdlbdf1yxllyhmgxka0hf49irf9kdyhgjsh5vqnn")))

(define-public crate-scroll-buffer-0.3.1 (c (n "scroll-buffer") (v "0.3.1") (d (list (d (n "doc-comment") (r "^0.3.3") (d #t) (k 2)) (d (n "scroll") (r "^0.11.0") (d #t) (k 0)))) (h "0k2q2hq0srvj32b3pjrxi0ika3hhkgry477g726g24fvdx93k386")))

