(define-module (crates-io sc ro scroll_phat_hd) #:use-module (crates-io))

(define-public crate-scroll_phat_hd-0.1.0 (c (n "scroll_phat_hd") (v "0.1.0") (d (list (d (n "i2cdev") (r "^0.3") (d #t) (k 0)) (d (n "termion") (r "^1.0") (d #t) (k 0)))) (h "0y4b3zizfaw3w0m7s66b989gz42ah7w5g9qss9mapa514a8ral8m")))

(define-public crate-scroll_phat_hd-0.2.0 (c (n "scroll_phat_hd") (v "0.2.0") (d (list (d (n "i2cdev") (r "^0.3") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "termion") (r "^1.0") (d #t) (k 0)))) (h "0lad0710f4vjiyc7wq1vdbl7c6pm9qgg3kghgf2pacg4qwp5xiij")))

(define-public crate-scroll_phat_hd-0.2.1 (c (n "scroll_phat_hd") (v "0.2.1") (d (list (d (n "i2cdev") (r "^0.3") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "termion") (r "^1.0") (d #t) (k 0)))) (h "1q6hz4hz3b9chl0jafm6b713j303sv7nyf8lirrcfshvcdb8pawk")))

(define-public crate-scroll_phat_hd-0.2.2 (c (n "scroll_phat_hd") (v "0.2.2") (d (list (d (n "i2cdev") (r "^0.3") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "termion") (r "^1.0") (d #t) (k 0)))) (h "1m8bgvgv4x6bjgpych0sgag5vavy23wihw3hlqav0c51giz42jm7")))

(define-public crate-scroll_phat_hd-0.2.3 (c (n "scroll_phat_hd") (v "0.2.3") (d (list (d (n "i2cdev") (r "^0.3") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "termion") (r "^1.0") (d #t) (k 0)))) (h "08wlpfbj670givw6bj8l5wv8l7zz1v8n4fdmgdfqxmzzzfzj86jq")))

(define-public crate-scroll_phat_hd-0.3.0 (c (n "scroll_phat_hd") (v "0.3.0") (d (list (d (n "i2cdev") (r "^0.3") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "termion") (r "^1.0") (d #t) (k 0)))) (h "1i7ryfshf7qww0hkyi5xb7a7p2zgdpyydcxwrh9vri9b7svmqfzf")))

(define-public crate-scroll_phat_hd-0.3.1 (c (n "scroll_phat_hd") (v "0.3.1") (d (list (d (n "i2cdev") (r "^0.3") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "termion") (r "^1.0") (d #t) (k 0)))) (h "1f4nnyn2n7d9wpgfldxc7x1vlmdc954q649yf0c182a30agd8ar0")))

(define-public crate-scroll_phat_hd-0.3.2 (c (n "scroll_phat_hd") (v "0.3.2") (d (list (d (n "i2cdev") (r "^0.3") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "termion") (r "^1.0") (d #t) (k 0)))) (h "0h89q59sz9436wblxlpzhfiy5szsrp9gis4l7hm7cqjj9dc3bjb5")))

