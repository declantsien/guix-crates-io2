(define-module (crates-io sc ro scron) #:use-module (crates-io))

(define-public crate-scron-0.1.1 (c (n "scron") (v "0.1.1") (d (list (d (n "chrono") (r "~0.4") (d #t) (k 0)) (d (n "error-chain") (r "~0.11.0") (d #t) (k 0)) (d (n "nom") (r "~2.1") (d #t) (k 0)))) (h "0rc6whw868xyhxnd2hs7qkdmwl7pr4syicx9rjlihynwyivdlwll")))

(define-public crate-scron-0.1.2 (c (n "scron") (v "0.1.2") (d (list (d (n "chrono") (r "~0.4") (d #t) (k 0)) (d (n "error-chain") (r "~0.11.0") (d #t) (k 0)) (d (n "nom") (r "~2.1") (d #t) (k 0)))) (h "0zcvaklqrrx2yfjb9hsv11w0p3l3i9wjafaycn8ixmpx1da23ry2")))

(define-public crate-scron-0.1.3 (c (n "scron") (v "0.1.3") (d (list (d (n "chrono") (r "~0.4") (d #t) (k 0)) (d (n "error-chain") (r "~0.11.0") (d #t) (k 0)) (d (n "nom") (r "~2.1") (d #t) (k 0)))) (h "1673gvsv6m47w1v1yl3ygjvcmqzilr6z78ig1qfjx86g2i92nqr7")))

(define-public crate-scron-0.1.4 (c (n "scron") (v "0.1.4") (d (list (d (n "chrono") (r "~0.4") (d #t) (k 0)) (d (n "error-chain") (r "~0.11.0") (d #t) (k 0)) (d (n "nom") (r "~2.1") (d #t) (k 0)))) (h "1xfmwj0w2v00ynnd1ml2b1wgwzmbv70k9b89vpp07v7aq3q8livn")))

(define-public crate-scron-0.1.5 (c (n "scron") (v "0.1.5") (d (list (d (n "chrono") (r "~0.4") (d #t) (k 0)) (d (n "error-chain") (r "~0.11.0") (d #t) (k 0)) (d (n "nom") (r "~2.1") (d #t) (k 0)))) (h "0y1bz7lg720bn6yrfn0lx6vqr750iisc768akhhry96xzw7pcfws")))

(define-public crate-scron-1.0.0 (c (n "scron") (v "1.0.0") (d (list (d (n "chrono") (r "~0.4") (d #t) (k 0)) (d (n "error-chain") (r "~0.11.0") (d #t) (k 0)) (d (n "nom") (r "~2.1") (d #t) (k 0)))) (h "1lvrv5xq41nliaim460hzdzanyc1p3045p9f5a407j8nv81bngs5")))

(define-public crate-scron-1.0.1 (c (n "scron") (v "1.0.1") (d (list (d (n "chrono") (r "~0.4") (d #t) (k 0)) (d (n "error-chain") (r "~0.11.0") (d #t) (k 0)) (d (n "nom") (r "~2.1") (d #t) (k 0)))) (h "0miv5cwagygpqxvwdxizmgv49wfkmjaiddijy67lfrj9a1v1bi9l")))

