(define-module (crates-io sc ro scroll-ssg) #:use-module (crates-io))

(define-public crate-scroll-ssg-0.1.0 (c (n "scroll-ssg") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "notify") (r "^4.0.15") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0.97") (f (quote ("derive"))) (d #t) (k 0)) (d (n "simple-server") (r "^0.4.0") (d #t) (k 0)) (d (n "tinytemplate") (r "^1.0.3") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "12c20rngqihnh60y9d88jraxivnp0lf9388bgbakkr57vs9njb3v")))

