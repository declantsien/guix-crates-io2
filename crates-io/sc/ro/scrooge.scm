(define-module (crates-io sc ro scrooge) #:use-module (crates-io))

(define-public crate-scrooge-0.1.0 (c (n "scrooge") (v "0.1.0") (d (list (d (n "config") (r "^0.9") (d #t) (k 0)) (d (n "futures") (r "^0.1.21") (d #t) (k 0)) (d (n "human-size") (r "^0.4") (d #t) (k 0)) (d (n "hyper") (r "^0.12") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)) (d (n "toml") (r "^0.4.1") (d #t) (k 0)) (d (n "unicase") (r "^2.3") (d #t) (k 0)))) (h "02km7pihhndk8f9g88v3729vjjn1r3k456mrfyxq7p2w2cqqmqg8")))

