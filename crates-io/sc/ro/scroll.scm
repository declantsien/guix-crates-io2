(define-module (crates-io sc ro scroll) #:use-module (crates-io))

(define-public crate-scroll-0.1.0 (c (n "scroll") (v "0.1.0") (h "0b028kiihnyf68fk28922xfslswnk99i0bkbvj9pfd25q9pg3l1a")))

(define-public crate-scroll-0.2.0 (c (n "scroll") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 2)) (d (n "rayon") (r "^0.6.0") (d #t) (k 2)))) (h "1c385zd6i8x6hr931fmk055z0ayqf4hiymqff3h8p6qvw5d8bvq1") (f (quote (("std") ("default" "std"))))))

(define-public crate-scroll-0.3.0 (c (n "scroll") (v "0.3.0") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 2)) (d (n "rayon") (r "^0.6.0") (d #t) (k 2)))) (h "078hzhfnzad70vy4v84g35g7zyp9c8nkhcch83dnp9gn102jvj1r") (f (quote (("std") ("default" "std"))))))

(define-public crate-scroll-0.4.0 (c (n "scroll") (v "0.4.0") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 2)) (d (n "rayon") (r "^0.6.0") (d #t) (k 2)))) (h "005lkv5vzi54f5rwbrlizhkqbixhrim8k68vd1c583q2ypsg629y") (f (quote (("std") ("default" "std"))))))

(define-public crate-scroll-0.5.0 (c (n "scroll") (v "0.5.0") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 2)) (d (n "rayon") (r "^0.6.0") (d #t) (k 2)) (d (n "scroll_derive") (r "^0.4.0") (d #t) (k 2)))) (h "09zaz0i7q0c1xl11sng6fqqvdr9ga9pzwaiilygmak4ds5snm48x") (f (quote (("std") ("default" "std"))))))

(define-public crate-scroll-0.6.0 (c (n "scroll") (v "0.6.0") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 2)) (d (n "rayon") (r "^0.6.0") (d #t) (k 2)) (d (n "scroll_derive") (r "^0.4.0") (d #t) (k 2)))) (h "062cyadzlwrc7li3qdkh6y1bmd3bw6xaa1l186w2yi6allwi85ml") (f (quote (("std") ("default" "std"))))))

(define-public crate-scroll-0.7.0 (c (n "scroll") (v "0.7.0") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 2)) (d (n "rayon") (r "^0.8.0") (d #t) (k 2)))) (h "1gyyr6y1czv4l5q03ign3mgd46x9spbc2y2v76gi1yz9xzz06zwb") (f (quote (("std") ("default" "std"))))))

(define-public crate-scroll-0.8.0 (c (n "scroll") (v "0.8.0") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 2)) (d (n "rayon") (r "^0.9.0") (d #t) (k 2)) (d (n "scroll_derive") (r "^0.8") (o #t) (d #t) (k 0)))) (h "1s2rap1g0ca3kd7aw8yyv5zhz5y4s6w15m86f8fndvdkw3hn8f5i") (f (quote (("std") ("derive" "std" "scroll_derive") ("default" "std"))))))

(define-public crate-scroll-0.8.1 (c (n "scroll") (v "0.8.1") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 2)) (d (n "rayon") (r "^0.9.0") (d #t) (k 2)) (d (n "scroll_derive") (r "^0.8.1") (o #t) (d #t) (k 0)))) (h "09bcj6lz0x7gbhfq06f1a1x08pvxcqck5p1lz65qf28y4y0p89fk") (f (quote (("std") ("derive" "std" "scroll_derive") ("default" "std")))) (y #t)))

(define-public crate-scroll-0.9.0 (c (n "scroll") (v "0.9.0") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 2)) (d (n "rayon") (r "^0.9.0") (d #t) (k 2)) (d (n "scroll_derive") (r "^0.9") (o #t) (d #t) (k 0)))) (h "0x4hjr0nk4hgwh8skiw21hcn2ky8k7cdp27nf2w6wiayrjl29w36") (f (quote (("std") ("derive" "scroll_derive") ("default" "std"))))))

(define-public crate-scroll-0.9.1 (c (n "scroll") (v "0.9.1") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 2)) (d (n "rayon") (r "^1.0.0") (d #t) (k 2)) (d (n "scroll_derive") (r "^0.9") (o #t) (d #t) (k 0)))) (h "1jk4m79dbkjbdbxgfslmy178n6x8zi0pg5ypnp5642d3w2q50ic7") (f (quote (("std") ("derive" "scroll_derive") ("default" "std"))))))

(define-public crate-scroll-0.9.2 (c (n "scroll") (v "0.9.2") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 2)) (d (n "rayon") (r "^1.0.0") (d #t) (k 2)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "scroll_derive") (r "^0.9") (o #t) (d #t) (k 0)))) (h "10q3w86bn22xrjlfg1c90dfi9c26qjkzn26nad0i9z8pxwad311g") (f (quote (("std") ("derive" "scroll_derive") ("default" "std"))))))

(define-public crate-scroll-0.10.0 (c (n "scroll") (v "0.10.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 2)) (d (n "rayon") (r "^1") (d #t) (k 2)) (d (n "scroll_derive") (r "^0.10") (o #t) (d #t) (k 0)))) (h "0x1khn8wxpn7n618sp4731rxww6sf3fc52aqyi5jb07g7qqbk15j") (f (quote (("std") ("derive" "scroll_derive") ("default" "std")))) (y #t)))

(define-public crate-scroll-0.10.1 (c (n "scroll") (v "0.10.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 2)) (d (n "rayon") (r "^1") (d #t) (k 2)) (d (n "scroll_derive") (r "^0.10") (o #t) (d #t) (k 0)))) (h "1cbcns8538sqmfnmdbphqy0fd4j8z75z802pvmz3zlwmnln37cmb") (f (quote (("std") ("derive" "scroll_derive") ("default" "std"))))))

(define-public crate-scroll-0.10.2 (c (n "scroll") (v "0.10.2") (d (list (d (n "byteorder") (r "^1") (d #t) (k 2)) (d (n "rayon") (r "^1") (d #t) (k 2)) (d (n "scroll_derive") (r "^0.10") (o #t) (d #t) (k 0)))) (h "1v61drdig30qfx2xh8bn7qdk2xgqbmmhwyrznjl0gf1h915qv8px") (f (quote (("std") ("derive" "scroll_derive") ("default" "std"))))))

(define-public crate-scroll-0.11.0 (c (n "scroll") (v "0.11.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 2)) (d (n "rayon") (r "^1") (d #t) (k 2)) (d (n "scroll_derive") (r "^0.11") (o #t) (d #t) (k 0)))) (h "1nhrhpzf95pxbcjjy222blwf8rl3adws6vsqax0yzyxsa6snbi84") (f (quote (("std") ("derive" "scroll_derive") ("default" "std"))))))

(define-public crate-scroll-0.12.0 (c (n "scroll") (v "0.12.0") (d (list (d (n "scroll_derive") (r "^0.12") (o #t) (d #t) (k 0)))) (h "19mix9vm4k23jkknpgbi0ylmhpf2hnlpzzrfj9wqcj88lj55kf3a") (f (quote (("std") ("default" "std")))) (s 2) (e (quote (("derive" "dep:scroll_derive")))) (r "1.63")))

