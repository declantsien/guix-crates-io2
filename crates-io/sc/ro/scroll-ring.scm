(define-module (crates-io sc ro scroll-ring) #:use-module (crates-io))

(define-public crate-scroll-ring-0.1.0 (c (n "scroll-ring") (v "0.1.0") (d (list (d (n "ringbuf") (r "^0.3.2") (k 0)) (d (n "try-lock") (r "^0.2.4") (d #t) (k 0)))) (h "0j7nv93ay1n4m5g9bbdvz7x8w0rr1502wq18laqd4g2lmp096gfb") (y #t) (r "1.65")))

(define-public crate-scroll-ring-0.1.1 (c (n "scroll-ring") (v "0.1.1") (d (list (d (n "ringbuf") (r "^0.3.2") (k 0)) (d (n "try-lock") (r "^0.2.4") (d #t) (k 0)))) (h "1wkxn4ry14xz20yzaic4hip3pddrrr3yns5a6aqa6sq3czzhvnjy") (r "1.65")))

