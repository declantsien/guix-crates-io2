(define-module (crates-io sc ro scroll_derive) #:use-module (crates-io))

(define-public crate-scroll_derive-0.1.0 (c (n "scroll_derive") (v "0.1.0") (d (list (d (n "quote") (r "^0.3.10") (d #t) (k 0)) (d (n "scroll") (r "^0.2.0") (d #t) (k 2)) (d (n "syn") (r "^0.10.5") (d #t) (k 0)))) (h "1way2945zq6ynscgwpxdwnf21isy235hzrxpiiwpz2pmrjqm8z34")))

(define-public crate-scroll_derive-0.3.0 (c (n "scroll_derive") (v "0.3.0") (d (list (d (n "quote") (r "^0.3.10") (d #t) (k 0)) (d (n "scroll") (r "^0.3.0") (d #t) (k 2)) (d (n "syn") (r "^0.10.5") (d #t) (k 0)))) (h "0mazlx2qgz2a71hwhmx87vsdryhr0bp7v36i6n7ijbh51hjljcw4")))

(define-public crate-scroll_derive-0.4.0 (c (n "scroll_derive") (v "0.4.0") (d (list (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "scroll") (r "^0.4.0") (d #t) (k 2)) (d (n "syn") (r "^0.11.9") (d #t) (k 0)))) (h "068g81kl15sr339ylkgm4mpb48rhwplvpq4kxmm7v0ilkgag2vk8")))

(define-public crate-scroll_derive-0.5.0 (c (n "scroll_derive") (v "0.5.0") (d (list (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "scroll") (r "^0.5.0") (d #t) (k 2)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "0pgslx9fah45ra575sgq79lqqph6r3iwp33c5scwalmadqi4him5")))

(define-public crate-scroll_derive-0.6.0 (c (n "scroll_derive") (v "0.6.0") (d (list (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "scroll") (r "^0.6.0") (d #t) (k 2)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "0xfiq8hbcr1x5hqgwcaaznssg8l4cppssfg7sjjjllsj9q4gpma8")))

(define-public crate-scroll_derive-0.7.0 (c (n "scroll_derive") (v "0.7.0") (d (list (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "scroll") (r "^0.7.0") (d #t) (k 2)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "1f52qnyhrcsr80mj1p8971rn279qcjxpnbmw35ms0ldgvakkbdkq")))

(define-public crate-scroll_derive-0.8.0 (c (n "scroll_derive") (v "0.8.0") (d (list (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "scroll") (r "^0.7.0") (d #t) (k 2)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "07a1x5ayy77y3d7gv1wxr87xl6vmyk1jxh643wqr8km4n9nhhrra")))

(define-public crate-scroll_derive-0.8.1 (c (n "scroll_derive") (v "0.8.1") (d (list (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "scroll") (r "^0.8") (d #t) (k 2)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "1lknmapfnqlsn93zl660qqllk02rca56pi9z1f0d64c6pxv1n3ih") (y #t)))

(define-public crate-scroll_derive-0.9.0 (c (n "scroll_derive") (v "0.9.0") (d (list (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "scroll") (r "^0.8") (d #t) (k 2)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "02nxh17y2619svcq7fk8hvcjr22a0y17lsv6y0nbf75jx598hkly")))

(define-public crate-scroll_derive-0.9.1 (c (n "scroll_derive") (v "0.9.1") (d (list (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "scroll") (r "^0.9") (d #t) (k 2)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "0mmz9mcdkpdr91bqrjpg0wacl255cyrh72vcfb2c92jfl83khj5i")))

(define-public crate-scroll_derive-0.9.2 (c (n "scroll_derive") (v "0.9.2") (d (list (d (n "quote") (r "^0.4") (d #t) (k 0)) (d (n "scroll") (r "^0.9") (d #t) (k 2)) (d (n "syn") (r "^0.12") (d #t) (k 0)))) (h "0g7xhqcq0mmwikmwgxa5hyliinl3d38ka7x0svxgcn753bbrdr0s")))

(define-public crate-scroll_derive-0.9.3 (c (n "scroll_derive") (v "0.9.3") (d (list (d (n "quote") (r "^0.5") (d #t) (k 0)) (d (n "scroll") (r "^0.9") (d #t) (k 2)) (d (n "syn") (r "^0.13") (d #t) (k 0)))) (h "0ci8ap7zgk5cx53w8fj6yksnkzj1lm27ixk0cv8450bgx5l0g139")))

(define-public crate-scroll_derive-0.9.4 (c (n "scroll_derive") (v "0.9.4") (d (list (d (n "proc-macro2") (r "^0.4.2") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "scroll") (r "^0.9") (d #t) (k 2)) (d (n "syn") (r "^0.14") (d #t) (k 0)))) (h "1igpw2dfyhmnhy1gm8vngl9cg8ks4nriv9nmjzq2zr4rvpsm78zr")))

(define-public crate-scroll_derive-0.9.5 (c (n "scroll_derive") (v "0.9.5") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "scroll") (r "^0.9") (d #t) (k 2)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "1jqg5mm8nvii6avl1z1rc89agzh2kwkppgpsnwfakxg78mnaj6lg")))

(define-public crate-scroll_derive-0.10.1 (c (n "scroll_derive") (v "0.10.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0a7f0xybi27p1njs4bqmxh9zyb2dqal4dbvgnhjjix4zkgm4wn7q")))

(define-public crate-scroll_derive-0.10.2 (c (n "scroll_derive") (v "0.10.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "scroll") (r "^0.10") (d #t) (k 2)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1f8m9frpw990ax6ch4w27mp8baqa5212pfh4qyhzyr28jcpn4rz3")))

(define-public crate-scroll_derive-0.10.3 (c (n "scroll_derive") (v "0.10.3") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "scroll") (r "^0.10") (d #t) (k 2)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0vgvn184j8h27m4rsms36kmfrfp2az46miwmkq9dnd0hag8ybzbd")))

(define-public crate-scroll_derive-0.10.4 (c (n "scroll_derive") (v "0.10.4") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "scroll") (r "^0.10") (d #t) (k 2)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "03ackd66daji7flgq9cc07xf6mls1nnr56xsqylgvkf7jh5x4axi")))

(define-public crate-scroll_derive-0.10.5 (c (n "scroll_derive") (v "0.10.5") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "scroll") (r "^0.10") (d #t) (k 2)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "187zrrwrp9gnvp7gjxazg6ahs9691fppj6bzzd6484dkigryiama")))

(define-public crate-scroll_derive-0.11.0 (c (n "scroll_derive") (v "0.11.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "scroll") (r "^0.10") (d #t) (k 2)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "03i5qn4jfcl2iwxhfvw9kf48a656ycbf5km99xr1wcnibjnadgdx")))

(define-public crate-scroll_derive-0.11.1 (c (n "scroll_derive") (v "0.11.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "scroll") (r "^0.11") (d #t) (k 2)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1bi5ljnzksvqhic6j7i2a2ap41s78xr0gifkgjxdxlj63pw4kc8x")))

(define-public crate-scroll_derive-0.12.0 (c (n "scroll_derive") (v "0.12.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "scroll") (r "^0.11") (d #t) (k 2)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0cmr3hxk318s2ivv37cik2l1r0d8r0qhahnin5lpxbr5w3yw50bz")))

