(define-module (crates-io sc d3 scd30pi) #:use-module (crates-io))

(define-public crate-scd30pi-0.3.2 (c (n "scd30pi") (v "0.3.2") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rppal") (r "^0.11.3") (d #t) (k 0)))) (h "1dhgyc79f1ybkpfikbzzv65ir2jksj26jrrqpkn1b9jfx9f8vlyw")))

(define-public crate-scd30pi-0.4.0 (c (n "scd30pi") (v "0.4.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rppal") (r "^0.11.3") (d #t) (k 0)))) (h "1810r901h1kdfnq6c19fma82hirndm4i0msb7401q0c97cwcfya1")))

(define-public crate-scd30pi-0.4.1 (c (n "scd30pi") (v "0.4.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rppal") (r "^0.11.3") (d #t) (k 0)))) (h "1v0acjy7bvaqz0svm2mhw5pz7zh9fakzpnipnqkj2n3cv0ssr6i8")))

(define-public crate-scd30pi-0.4.2 (c (n "scd30pi") (v "0.4.2") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rppal") (r "^0.11.3") (d #t) (k 0)))) (h "1i1xm58px4kl8svglkk745gvdw16sfnlv2s2d9p5m8liribv7q94")))

