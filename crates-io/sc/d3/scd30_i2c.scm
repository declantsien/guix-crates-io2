(define-module (crates-io sc d3 scd30_i2c) #:use-module (crates-io))

(define-public crate-scd30_i2c-0.1.0 (c (n "scd30_i2c") (v "0.1.0") (d (list (d (n "i2cdev") (r "^0.6.0") (d #t) (k 0)))) (h "1jcijrhsbfrvjnch7191q6dw4a9bvpxwi1ak7kkh26dgfciyhbs2")))

(define-public crate-scd30_i2c-0.1.1 (c (n "scd30_i2c") (v "0.1.1") (d (list (d (n "i2cdev") (r "^0.6.0") (d #t) (k 0)))) (h "1z5g82qfjl30506j5n8q4girsk4mxdmwghl7929n7hyyc09z60f9")))

(define-public crate-scd30_i2c-0.1.2 (c (n "scd30_i2c") (v "0.1.2") (d (list (d (n "i2cdev") (r "^0.6.0") (d #t) (k 0)))) (h "1waxlriif57lf4qjp77yim5kypm6qblkp043bvkk0z3m11a205gh")))

