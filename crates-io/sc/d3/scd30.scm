(define-module (crates-io sc d3 scd30) #:use-module (crates-io))

(define-public crate-scd30-0.2.0 (c (n "scd30") (v "0.2.0") (d (list (d (n "embedded-hal") (r "^0.2.2") (d #t) (k 0)) (d (n "heapless") (r "^0.4.2") (d #t) (k 0)))) (h "0gi2644qdhh6l96i0bbcfy7hlk0sk2ikzixk8166gp7xjj4cj0n8")))

(define-public crate-scd30-0.2.1 (c (n "scd30") (v "0.2.1") (d (list (d (n "embedded-hal") (r "^0.2.2") (d #t) (k 0)) (d (n "heapless") (r "^0.4.2") (d #t) (k 0)))) (h "105kac88dn8rq2wh4v5dc3jl3mpay5j9jb7p0s38nl1yw7ylw4l2")))

(define-public crate-scd30-0.2.2 (c (n "scd30") (v "0.2.2") (d (list (d (n "embedded-hal") (r "^0.2.2") (d #t) (k 0)) (d (n "heapless") (r "^0.4.2") (d #t) (k 0)))) (h "0k0nn0railvj8y9lrkdliph92dfpp8mk3vq8afbd22ziwxi6lc4q")))

(define-public crate-scd30-0.2.3 (c (n "scd30") (v "0.2.3") (d (list (d (n "embedded-hal") (r "^0.2.2") (d #t) (k 0)) (d (n "heapless") (r "^0.4.2") (d #t) (k 0)))) (h "1xkkqkk4pbyw4l393h74fxn7hglsdl906bckl4rkc093k86l4s0c")))

(define-public crate-scd30-0.3.0 (c (n "scd30") (v "0.3.0") (d (list (d (n "crc_all") (r "^0.1.0") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.2") (d #t) (k 0)) (d (n "heapless") (r "^0.4.2") (f (quote ("const-fn"))) (d #t) (k 0)))) (h "17k3n7bvknyrzz2c1j28kbg6d9bd2cym6zhs5m3cx3ir00b295ia")))

