(define-module (crates-io sc d3 scd30-modbus) #:use-module (crates-io))

(define-public crate-scd30-modbus-0.1.0 (c (n "scd30-modbus") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)) (d (n "nb") (r "^1.0.0") (d #t) (k 0)) (d (n "smol") (r "^1.2.5") (d #t) (k 0)))) (h "09wgj3hyzc1cxzql7djb4n5cj0kwpgrhwgs7z6c2s6an4m54mkk5") (f (quote (("std") ("default"))))))

(define-public crate-scd30-modbus-0.1.1 (c (n "scd30-modbus") (v "0.1.1") (d (list (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)) (d (n "nb") (r "^1.0.0") (d #t) (k 0)) (d (n "smol") (r "^1.2.5") (d #t) (k 0)))) (h "01v5xi081zc5f2lb9izn7kgz3wqi1rijy5cfxiwp4p2lkd38pr3k") (f (quote (("std") ("default"))))))

(define-public crate-scd30-modbus-0.1.2 (c (n "scd30-modbus") (v "0.1.2") (d (list (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)) (d (n "futures-lite") (r "^1.12.0") (d #t) (k 0)) (d (n "nb") (r "^1.0.0") (d #t) (k 0)) (d (n "smol") (r "^1.2.5") (d #t) (k 0)))) (h "0pijv0g19zlpbq8ddzlh1yalgk76axly7ks6rkry52jag8sxgs9q") (f (quote (("std") ("default"))))))

(define-public crate-scd30-modbus-0.2.0 (c (n "scd30-modbus") (v "0.2.0") (d (list (d (n "embedded-hal-nb") (r "^1.0.0-alpha.2") (d #t) (k 0)) (d (n "futures-lite") (r "^1.12.0") (d #t) (k 0)) (d (n "nb") (r "^1.0.0") (d #t) (k 0)) (d (n "smol") (r "^1.2.5") (d #t) (k 0)))) (h "0gb18xh2mid21j672j9pan8f0jycsf4srfhv6vchip03biy08rqx") (f (quote (("std") ("default"))))))

(define-public crate-scd30-modbus-0.3.0 (c (n "scd30-modbus") (v "0.3.0") (d (list (d (n "embedded-io-async") (r "^0.6.1") (d #t) (k 0)) (d (n "smol") (r "^2.0.0") (d #t) (k 0)))) (h "1kfy48j7kfj1rbwr687vi95k8il2mkapvq305hrf158c15ys1hcg") (f (quote (("std") ("default"))))))

