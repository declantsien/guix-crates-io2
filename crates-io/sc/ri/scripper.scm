(define-module (crates-io sc ri scripper) #:use-module (crates-io))

(define-public crate-scripper-0.1.0 (c (n "scripper") (v "0.1.0") (h "00iddajn5942q1j9bwz45xdc285ykkqz790abc4bkyaldlvzckvs")))

(define-public crate-scripper-0.1.1 (c (n "scripper") (v "0.1.1") (h "0zx25pj6zq1833nk3k3qgfzzqn5gqfihhxp63626d2brpkc1yqp2")))

(define-public crate-scripper-0.1.2 (c (n "scripper") (v "0.1.2") (h "1v4cy1sz4bwngg1rrygm2xas5v6srkp2bl7jclhfq7x358g9vv2k")))

(define-public crate-scripper-0.1.21 (c (n "scripper") (v "0.1.21") (d (list (d (n "clap") (r "^4.0.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "1d79xb886p2rp5r495l4qrj8ivg1555dng11g2jpirqagmlsyfdg")))

