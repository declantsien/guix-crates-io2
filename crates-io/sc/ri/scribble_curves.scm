(define-module (crates-io sc ri scribble_curves) #:use-module (crates-io))

(define-public crate-scribble_curves-0.1.0 (c (n "scribble_curves") (v "0.1.0") (d (list (d (n "druid") (r "^0.6.0") (f (quote ("im"))) (d #t) (k 0)) (d (n "gstreamer") (r "^0.15.4") (d #t) (k 0)) (d (n "im") (r "^15.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "serde_cbor") (r "^0.11.1") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.53") (d #t) (k 2)))) (h "1vrs5fjglbnwhj6zw341f0cfc89hksacx205z6qlz4633dfh88xp")))

