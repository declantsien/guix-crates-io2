(define-module (crates-io sc ri scriptplan-core) #:use-module (crates-io))

(define-public crate-scriptplan-core-1.0.0 (c (n "scriptplan-core") (v "1.0.0") (d (list (d (n "async-recursion") (r "^0.3.2") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.50") (d #t) (k 0)) (d (n "conch-runtime") (r "^0.1.6") (d #t) (k 0)) (d (n "futures") (r "^0.3.15") (d #t) (k 0)))) (h "0kmjwpcb2jp5sipz0fdlcqs1sgivjw0wjygjilkz1lqmdgvx5171")))

(define-public crate-scriptplan-core-1.0.1 (c (n "scriptplan-core") (v "1.0.1") (d (list (d (n "async-recursion") (r "^0.3.2") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.50") (d #t) (k 0)) (d (n "conch-runtime-pshaw") (r "^0.1.6") (d #t) (k 0)) (d (n "futures") (r "^0.3.15") (d #t) (k 0)))) (h "03bbh9bdg4yh4j89clyryywwvfqds51dxscfi6a9wad7jgf5k31q")))

(define-public crate-scriptplan-core-2.0.0 (c (n "scriptplan-core") (v "2.0.0") (d (list (d (n "async-recursion") (r "^0.3.2") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.50") (d #t) (k 0)) (d (n "futures") (r "^0.3.15") (d #t) (k 0)))) (h "1vmkqbr730fr540g9h657acph7gxvbpkh9wji3sh4fw38aw8w1sq")))

(define-public crate-scriptplan-core-3.0.0 (c (n "scriptplan-core") (v "3.0.0") (d (list (d (n "async-recursion") (r "^0.3.2") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.50") (d #t) (k 0)) (d (n "futures") (r "^0.3.15") (d #t) (k 0)))) (h "1zwkqv50hsx8ip9rwv7w4nd1ws6dvijzz2v5mbznis34i6drzw9c")))

(define-public crate-scriptplan-core-3.0.1 (c (n "scriptplan-core") (v "3.0.1") (d (list (d (n "async-recursion") (r "^1.0.0") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.53") (d #t) (k 0)) (d (n "futures") (r "^0.3.21") (d #t) (k 0)))) (h "1hik5flsbfg2b4wja3abfs6cijg2187kq98wjqldr0lpq58pl3c8")))

(define-public crate-scriptplan-core-3.0.2 (c (n "scriptplan-core") (v "3.0.2") (d (list (d (n "async-recursion") (r "^1.0.0") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.53") (d #t) (k 0)) (d (n "futures") (r "^0.3.21") (d #t) (k 0)))) (h "0zvwgb1cm93kb5722gr173l66d1wfvragnwknv61wmah1cnifibc")))

(define-public crate-scriptplan-core-3.0.3 (c (n "scriptplan-core") (v "3.0.3") (d (list (d (n "async-recursion") (r "^1.0.0") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.53") (d #t) (k 0)) (d (n "futures") (r "^0.3.21") (d #t) (k 0)))) (h "1q910fbc5j6dvyfq015hxbcy7y7z5icic3q0fk5aij6nfqhzhxbh")))

(define-public crate-scriptplan-core-4.0.0 (c (n "scriptplan-core") (v "4.0.0") (d (list (d (n "async-recursion") (r "^1.0.0") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.53") (d #t) (k 0)) (d (n "futures") (r "^0.3.21") (d #t) (k 0)))) (h "1537s2550b5d7af1ijrqkpdc0qfyplsbw5pm7cxvam45cww5ajwl")))

(define-public crate-scriptplan-core-5.0.0 (c (n "scriptplan-core") (v "5.0.0") (d (list (d (n "async-recursion") (r "^1.0.0") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.53") (d #t) (k 0)) (d (n "futures") (r "^0.3.21") (d #t) (k 0)))) (h "1a4dm7g6py8l8lsqvnic46dz1vsz8z8d26ywgx1fixp7f9zxcgy3")))

(define-public crate-scriptplan-core-6.0.0 (c (n "scriptplan-core") (v "6.0.0") (d (list (d (n "async-recursion") (r "^1.0.0") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.53") (d #t) (k 0)) (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "scriptplan-lang-utils") (r "^1.0.0") (d #t) (k 0)))) (h "1ya35z6kv1gl5474jp7l5p1wwgrmk4wly9jsg8qcs8xf8l2qr6gw")))

