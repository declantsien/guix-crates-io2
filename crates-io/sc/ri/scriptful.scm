(define-module (crates-io sc ri scriptful) #:use-module (crates-io))

(define-public crate-scriptful-0.1.0 (c (n "scriptful") (v "0.1.0") (d (list (d (n "failure") (r "^0.1.6") (d #t) (k 0)) (d (n "smallvec") (r "^1.1.0") (d #t) (k 0)))) (h "187q6jrcjzlvvq3d79r8rkfvw1rhh9qfn184c7in5z42ax9nl9w2")))

(define-public crate-scriptful-0.1.1 (c (n "scriptful") (v "0.1.1") (d (list (d (n "smallvec") (r "^1.1.0") (d #t) (k 0)))) (h "1csbp3nzz1xb95iwdxh2989cfr1jf6wsc0ic8h1431p2vf4h38yf")))

(define-public crate-scriptful-0.1.2 (c (n "scriptful") (v "0.1.2") (d (list (d (n "smallvec") (r "^1.1.0") (d #t) (k 0)))) (h "12q593q8lvmwqcxy1n0lmma9dwx2wn6cvj2vp37iv36n757ki693")))

(define-public crate-scriptful-0.2.0 (c (n "scriptful") (v "0.2.0") (d (list (d (n "smallvec") (r "^1.1.0") (d #t) (k 0)))) (h "0hyclpnplvf4qv78dl6y2lpvpfd97gbphkh4cffl43d1bi2dr5w6")))

(define-public crate-scriptful-0.2.1 (c (n "scriptful") (v "0.2.1") (h "0zr8awyfwz8dmmca7h0gga8qhx345xpmlnyvj9x2nl72a0dybv9h")))

(define-public crate-scriptful-0.3.0 (c (n "scriptful") (v "0.3.0") (h "075c31qx27dqky1gsriv1dayh75xnxf97mkgfbh631w418dafdgj") (f (quote (("default" "codecs") ("codecs"))))))

(define-public crate-scriptful-0.3.1 (c (n "scriptful") (v "0.3.1") (h "1qh674vshg1kywc4jak8cb30l26nii460svqcjjgmnhnb50n9v19") (f (quote (("default" "codecs") ("codecs"))))))

(define-public crate-scriptful-0.4.0 (c (n "scriptful") (v "0.4.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0ccpcwkfj9pjz8gqri5zzsp446g6lirxpfifj3mylf8bm282i88p") (f (quote (("use_serde" "serde") ("default" "codecs") ("codecs"))))))

