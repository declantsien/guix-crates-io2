(define-module (crates-io sc ri scribl_widget) #:use-module (crates-io))

(define-public crate-scribl_widget-0.3.0 (c (n "scribl_widget") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "druid") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "0ysl0n5mrb9fvgwq4g4iyhxvcdlam6pwxc1z6m2z5j41qahjy79h")))

(define-public crate-scribl_widget-0.3.1 (c (n "scribl_widget") (v "0.3.1") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "druid") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "0bqxb3n7p1r3486s65mpjld4kkibfspng0lajy3qxkabx8n05fgb")))

