(define-module (crates-io sc ri scripthookv-rs) #:use-module (crates-io))

(define-public crate-scripthookv-rs-0.1.0 (c (n "scripthookv-rs") (v "0.1.0") (d (list (d (n "libscripthookv-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("std" "minwindef"))) (d #t) (k 0)))) (h "1pkadxawfp5ndsn39v4p4lshcrsaxymw6i4ypyy1xvjx8g3chg68")))

(define-public crate-scripthookv-rs-0.1.1 (c (n "scripthookv-rs") (v "0.1.1") (d (list (d (n "libscripthookv-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("std" "minwindef"))) (d #t) (k 0)))) (h "0c3kzqy4vd42vl3lzcmwckxpbnykcbf0rma3x50482j0vdgdhpkd")))

(define-public crate-scripthookv-rs-0.1.2 (c (n "scripthookv-rs") (v "0.1.2") (d (list (d (n "libscripthookv-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("minwindef"))) (d #t) (k 0)))) (h "1pj6dz8fyj5dcdm9zzfq0giprz9d303pnrnv6pmspg68p22gsn7z")))

(define-public crate-scripthookv-rs-0.1.3 (c (n "scripthookv-rs") (v "0.1.3") (d (list (d (n "libscripthookv-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("minwindef"))) (d #t) (k 0)))) (h "09gsqyh0jn8yfi9xy416i3miz17jqdy46chspcwlv0rsrsv69lfv")))

(define-public crate-scripthookv-rs-0.1.4 (c (n "scripthookv-rs") (v "0.1.4") (d (list (d (n "libscripthookv-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("minwindef"))) (d #t) (k 0)))) (h "1y6jgqsxhclymd2r3mp7vjb0ssk7m3sf71y1a0z9nv3sc65pfqy2")))

(define-public crate-scripthookv-rs-0.1.5 (c (n "scripthookv-rs") (v "0.1.5") (d (list (d (n "libscripthookv-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("minwindef"))) (d #t) (k 0)))) (h "0g5cg52xx1bazyraqw2z5v2bbv7f2jrq5qb43q57ji40dxmrr108")))

(define-public crate-scripthookv-rs-0.1.6 (c (n "scripthookv-rs") (v "0.1.6") (d (list (d (n "libscripthookv-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("minwindef"))) (d #t) (k 0)))) (h "0r8wbl8lvb0akfp0kymkqr9s4p993zhfixb41n3zqqjwz57chl6y")))

(define-public crate-scripthookv-rs-0.2.0 (c (n "scripthookv-rs") (v "0.2.0") (d (list (d (n "libscripthookv-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "scripthookv-rs-macros") (r "^0.1.0") (d #t) (k 0)))) (h "1bc70141s5f99qqn317hwf77npwzb7c0dy21kr1nq1yvwpbwqxp3") (y #t)))

(define-public crate-scripthookv-rs-0.2.1 (c (n "scripthookv-rs") (v "0.2.1") (d (list (d (n "libscripthookv-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "scripthookv-rs-macros") (r "^0.1.0") (d #t) (k 0)))) (h "139gwv45smirjxgc2xsxvp49j66811habgr2xrapgggwgdymkngi")))

(define-public crate-scripthookv-rs-0.3.0 (c (n "scripthookv-rs") (v "0.3.0") (d (list (d (n "async-trait") (r "^0.1.52") (d #t) (k 0)) (d (n "libscripthookv-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "scripthookv-rs-macros") (r "^0.2.0") (d #t) (k 0)) (d (n "smol") (r "^1.2.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("psapi" "processthreadsapi" "libloaderapi"))) (d #t) (k 0)))) (h "1mipynycy2pbfxkjrk41md6wkbyhdg87qr2g9g6dkn2mbpblwq92")))

