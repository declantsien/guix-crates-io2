(define-module (crates-io sc ri scribe-rust) #:use-module (crates-io))

(define-public crate-scribe-rust-0.1.0 (c (n "scribe-rust") (v "0.1.0") (h "18i6gsahkv4fgc2jhhzk37sswp23cjvmzgzd7q80914pawzphz7h")))

(define-public crate-scribe-rust-0.1.1 (c (n "scribe-rust") (v "0.1.1") (h "0rn5xwxl2g4k6hwyk0amlhm9fgy2mrrdpck63ryp2d9ybpp0nmyj")))

(define-public crate-scribe-rust-0.2.0 (c (n "scribe-rust") (v "0.2.0") (h "09gdy04nmsd423ca8063xdmvfqdg9gjp7nn87zph18f0k2n79sfp")))

