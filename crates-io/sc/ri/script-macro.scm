(define-module (crates-io sc ri script-macro) #:use-module (crates-io))

(define-public crate-script-macro-0.1.0 (c (n "script-macro") (v "0.1.0") (d (list (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "rhai") (r "^1.13.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "rhai-fs") (r "^0.1.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.21") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "1vadxvhyhi1g40pc7r6hy29aygmwkxfwll7c300fyb2gixmdr7h5")))

