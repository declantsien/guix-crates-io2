(define-module (crates-io sc ri scripter) #:use-module (crates-io))

(define-public crate-scripter-0.1.0 (c (n "scripter") (v "0.1.0") (h "1f18aqgiv6zkg47r3d10i56y40dqw61wil86mvrzdgvla39nghg6")))

(define-public crate-scripter-0.2.0 (c (n "scripter") (v "0.2.0") (h "001pjvydzaaxhv1ljln2fpvbza7cppikkak7nmgf8aa7wx19plnq")))

(define-public crate-scripter-0.3.0 (c (n "scripter") (v "0.3.0") (h "1klc8wqg34isym54d8v8z2iy3m1nd6wrwpnx4jxhvnxj90fk9cq3")))

(define-public crate-scripter-0.4.0 (c (n "scripter") (v "0.4.0") (h "0baf2z8ihdkqpk1dkmiiq566mp6vhp9c28qqkivfp4568adnpl3i")))

(define-public crate-scripter-0.4.1 (c (n "scripter") (v "0.4.1") (h "1zzg0asjdfm424rb5f70l7646qlhncwck0c3w0vg120w8x3cg39h")))

