(define-module (crates-io sc ri scriptit) #:use-module (crates-io))

(define-public crate-scriptit-0.1.0 (c (n "scriptit") (v "0.1.0") (d (list (d (n "rusty_v8") (r "^0.9.1") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "wasm-bindgen") (r "^0.2.67") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.17") (d #t) (k 2)))) (h "1by4amvfs8lzd9sdqki4frwd58w0fdyq25s3zsrzpa6yf0j1hfjf")))

(define-public crate-scriptit-0.2.0 (c (n "scriptit") (v "0.2.0") (d (list (d (n "rusty_v8") (r "^0.9.1") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "wasm-bindgen") (r "^0.2.67") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.17") (d #t) (k 2)))) (h "0bjbkk2l2mw5zfmy0ni49zsqs93i4mj0sa1w9wcjs3181hz1rv5x")))

(define-public crate-scriptit-0.3.0 (c (n "scriptit") (v "0.3.0") (d (list (d (n "rusty_v8") (r "^0.9.1") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4" "wasm-bindgen"))) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.67") (f (quote ("serde-serialize"))) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.17") (d #t) (k 2)))) (h "0qkvq78qakwgraac28ykimrmwrrdwi1syhm20ss53z0fz9iw37li")))

(define-public crate-scriptit-0.4.0 (c (n "scriptit") (v "0.4.0") (d (list (d (n "rusty_v8") (r "^0.9.1") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4" "wasm-bindgen"))) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.67") (f (quote ("serde-serialize"))) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.17") (d #t) (k 2)))) (h "1g6npdd45nqy2r7nww37pn03gbk24zk86frw47xwi22v6sqab3i2")))

