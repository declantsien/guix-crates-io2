(define-module (crates-io sc ri scriptkit) #:use-module (crates-io))

(define-public crate-scriptkit-0.1.0 (c (n "scriptkit") (v "0.1.0") (h "0lxnvql23mgsr0875icvws3bs156x0lldr905andrk1wvxzk7w9z") (y #t)))

(define-public crate-scriptkit-0.1.1 (c (n "scriptkit") (v "0.1.1") (h "0dfjr00m64d3y659hd2yw0w86bmxkk7nqg2xbw80rllk4dp6x195") (y #t)))

(define-public crate-scriptkit-0.1.2 (c (n "scriptkit") (v "0.1.2") (h "0dkcvd599qa3xy24zj1fasx4q85m5xk7dq4v2p9navi67i4xcbgp") (y #t)))

(define-public crate-scriptkit-0.1.3 (c (n "scriptkit") (v "0.1.3") (h "0rdiy8n13pg78g43npb5i3r51xp3jph5bbkr2x565qgnskcfz9lh") (y #t)))

(define-public crate-scriptkit-0.1.4 (c (n "scriptkit") (v "0.1.4") (h "0zhh907ad0gvk2vzcnma83k7wdq46s8kczm4svxns7rylwwn4gkf") (y #t)))

(define-public crate-scriptkit-0.1.5 (c (n "scriptkit") (v "0.1.5") (h "03gnjfclsbnbqzba1hf1r8lag02fgy7s7wprkcsavcsajvghhzp6") (y #t)))

(define-public crate-scriptkit-0.1.6 (c (n "scriptkit") (v "0.1.6") (h "0kjb0dh86b5idp1adwx966qqxvc72imwm60im073nxq90283zw97") (y #t)))

(define-public crate-scriptkit-0.1.7 (c (n "scriptkit") (v "0.1.7") (h "1mcaf6pws2rjp2m4dp6k734cm055l9wvqximcc5xxfq5445ppy42") (y #t)))

(define-public crate-scriptkit-0.1.8 (c (n "scriptkit") (v "0.1.8") (h "12f9ysh1i260alwqq4bzw3m0jf79x8rw0x8sx68vj9alghn4kpl1") (y #t)))

(define-public crate-scriptkit-0.1.9 (c (n "scriptkit") (v "0.1.9") (h "0r1gk32hls3z8qrgvr6394nd4y114mybfjvfhk1l4fq3kwi33a34") (y #t)))

(define-public crate-scriptkit-1.0.0 (c (n "scriptkit") (v "1.0.0") (h "1qjasraaia9f6lc1jsjd5hjkpksndicl0yw5i46964xgjs20c6b0") (y #t)))

(define-public crate-scriptkit-1.0.1 (c (n "scriptkit") (v "1.0.1") (h "03vk45zhqlv1jq7i11ff4v7kh29d3g70dpdgm60smya7mpx3ankp") (y #t)))

