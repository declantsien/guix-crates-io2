(define-module (crates-io sc ri scriptx) #:use-module (crates-io))

(define-public crate-scriptx-0.1.0 (c (n "scriptx") (v "0.1.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "10iaxd8036zpwc1r0yg05kh36fzdd5dffl8a881q7q01j3zq7q7s")))

(define-public crate-scriptx-0.1.1 (c (n "scriptx") (v "0.1.1") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "07w9bd21rhnaldj5qf1iwb567r7yka9h5wfqhjgyv39pg7zzlwia")))

(define-public crate-scriptx-0.2.0 (c (n "scriptx") (v "0.2.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1afv7p19ngl36bldlgqc4632vgcvwb0hx1c2hghj4wld526r0ma2")))

(define-public crate-scriptx-0.2.1 (c (n "scriptx") (v "0.2.1") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0fgl4q77fqfcmnxzmdr4igkv0rp4wsbscnj9l2sg1b8sp0cac4n8")))

(define-public crate-scriptx-0.2.2 (c (n "scriptx") (v "0.2.2") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1q840i4nm852fkbxfqmhdz4awzc0l417gqiax60yp45rvzi3khcr")))

(define-public crate-scriptx-0.2.3 (c (n "scriptx") (v "0.2.3") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "03a8lwm0kwrsqpl4fcclrix81w9y8qg8j5lxwapy4lpj3w55g7z4")))

(define-public crate-scriptx-0.2.4 (c (n "scriptx") (v "0.2.4") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0243nsgs40h5wfnb0fm3zaqczkv637gx65i0d8ljkwlswai4wcdp")))

(define-public crate-scriptx-0.3.0 (c (n "scriptx") (v "0.3.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1wz9y9jxvqynrzpm0kl2haihfnm8p78ww3c80cbqz1ch8ja7h1is")))

(define-public crate-scriptx-0.3.1 (c (n "scriptx") (v "0.3.1") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0dkmcl6dbl95aw4mrbp1fg23g09r7vax894hnzami3qnn9wh4kc2")))

(define-public crate-scriptx-0.3.2 (c (n "scriptx") (v "0.3.2") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "04zxazva1l3sbr8vam7668mkc7ci0s0p51pj0a34f4b0fhbqc5vg")))

(define-public crate-scriptx-0.3.3 (c (n "scriptx") (v "0.3.3") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1rn01685c2vdi844dac0v05gb1c6yh1msmx943s68vmfm32f4l5n") (y #t)))

(define-public crate-scriptx-0.4.0 (c (n "scriptx") (v "0.4.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "168fz0g3nr75nhmqngzsnpsw9fd2mjvhxyfgi0vri7hbsrim69mp")))

(define-public crate-scriptx-0.4.1 (c (n "scriptx") (v "0.4.1") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "08kmylqywaj8gpimiylmg1djy0fr43v30jjd6c3jhv0gz3w1mlnz")))

(define-public crate-scriptx-0.4.2 (c (n "scriptx") (v "0.4.2") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "indicatif") (r "^0.16.2") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0z8hri8a55z9q9wif31ya81mbjd7s2pih3vr1990cr1cqndq48g0")))

(define-public crate-scriptx-0.4.3 (c (n "scriptx") (v "0.4.3") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "indicatif") (r "^0.16.2") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0czmql06qfxzj6w7crrq0jsrhldv3yr0smqqsag2i64r4v0l7xih")))

(define-public crate-scriptx-0.4.4 (c (n "scriptx") (v "0.4.4") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "indicatif") (r "^0.16.2") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0svgzdrsn32v8fppymk2f6xcsrchgqwrrf2729py2bfdzpxzvpgc")))

(define-public crate-scriptx-0.4.20 (c (n "scriptx") (v "0.4.20") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "indicatif") (r "^0.16.2") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "099slspa6gqfp3bp1lbw95a235wxya2w06v97wn2wal884yg6wh9") (y #t)))

(define-public crate-scriptx-0.4.5 (c (n "scriptx") (v "0.4.5") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "indicatif") (r "^0.16.2") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1wzfphas16r119vxn00r2y2cpvzi7fm40414y3cf81vz42l0arjm")))

(define-public crate-scriptx-0.4.7 (c (n "scriptx") (v "0.4.7") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "indicatif") (r "^0.16.2") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0gqplmm188wssy5lkg6y3d3ihwcm10l6n92k5fdghrvyn5nw3cxq")))

(define-public crate-scriptx-0.4.8 (c (n "scriptx") (v "0.4.8") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "indicatif") (r "^0.16.2") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0irpnq4mx9igznmnm34jmvg63km8pzd871rzb20684g9pkwxhn5f")))

(define-public crate-scriptx-0.4.9 (c (n "scriptx") (v "0.4.9") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "indicatif") (r "^0.16.2") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1dl4dpl3hi3dw9vgnbl574f2xnx2cbnwl0v87qjz2vk8lw2j757g")))

(define-public crate-scriptx-0.4.10 (c (n "scriptx") (v "0.4.10") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "indicatif") (r "^0.16.2") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "11a44hm545q5cr0x4h68ga5g3f9f94kcibfh0ksd3mc08ik86xpy")))

