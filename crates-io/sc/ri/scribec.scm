(define-module (crates-io sc ri scribec) #:use-module (crates-io))

(define-public crate-scribec-0.1.0 (c (n "scribec") (v "0.1.0") (d (list (d (n "argh") (r "^0.1.7") (d #t) (k 0)) (d (n "thrift") (r "^0.15.0") (d #t) (k 0)))) (h "092m77bdnrmsgph3h1dc5xv63ri60fvsj86h8yq46hspcz2xrfp0")))

