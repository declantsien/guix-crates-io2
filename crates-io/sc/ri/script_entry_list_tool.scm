(define-module (crates-io sc ri script_entry_list_tool) #:use-module (crates-io))

(define-public crate-script_entry_list_tool-1.0.0 (c (n "script_entry_list_tool") (v "1.0.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "pmd_script_entry_list") (r "^1.0.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.48") (d #t) (k 0)))) (h "0ndmh0a4q5rgqsa5cg484af9m8wzz33xpyv51s1w6hxzag02ibx4")))

