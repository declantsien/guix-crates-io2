(define-module (crates-io sc ri scripthookv-rs-macros) #:use-module (crates-io))

(define-public crate-scripthookv-rs-macros-0.1.0 (c (n "scripthookv-rs-macros") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "18ija75xm97fb7rqgxj21sqahk1f8f69jqqi4qn45gzpijc19c6z")))

(define-public crate-scripthookv-rs-macros-0.2.0 (c (n "scripthookv-rs-macros") (v "0.2.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0jx2qiqmc0wpbnwc0zqaadyrhsa04vzgpws4d02isryvhqv806ff")))

(define-public crate-scripthookv-rs-macros-0.2.1 (c (n "scripthookv-rs-macros") (v "0.2.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1lvaflf6jybms9n0d79xgxwvamy85p331yvwbal1bkb7g3j319zw")))

