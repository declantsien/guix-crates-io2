(define-module (crates-io sc ri scrivener) #:use-module (crates-io))

(define-public crate-scrivener-0.1.0 (c (n "scrivener") (v "0.1.0") (d (list (d (n "confy") (r "^0.4.0") (d #t) (k 0)) (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.7") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8.0") (d #t) (k 0)) (d (n "scrawl") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3.14") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)))) (h "1kjhnai46grkxnvgxpj100gkz522qz9yw90bbggbq66x0c2lalhb")))

