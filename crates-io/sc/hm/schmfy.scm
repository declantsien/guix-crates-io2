(define-module (crates-io sc hm schmfy) #:use-module (crates-io))

(define-public crate-schmfy-0.1.0 (c (n "schmfy") (v "0.1.0") (h "0a640bq2hhb0b5463bgjdiyb88g1l848rwia63f00kq4660z6l4j")))

(define-public crate-schmfy-0.1.1 (c (n "schmfy") (v "0.1.1") (h "05mll8144336lvqmlab2x2qk6mxisc9d7m2ypx6ys2pgm4nxfqac")))

(define-public crate-schmfy-0.2.0 (c (n "schmfy") (v "0.2.0") (h "032yml2hmp8pzrld2cdzf84nyqxsjv0gwqvzvrab0p15i4gxazrr")))

(define-public crate-schmfy-0.2.1 (c (n "schmfy") (v "0.2.1") (h "033zhami76qjxiv6sn6gkawfmxfd8qrdaybizxpfdv7w9ngm4n52")))

(define-public crate-schmfy-0.2.2 (c (n "schmfy") (v "0.2.2") (h "1h6dn1k5s5slgf3lbg1yrzp7qlxqzcppcckrvpcrvkaynlnbvr7r")))

(define-public crate-schmfy-0.3.0 (c (n "schmfy") (v "0.3.0") (d (list (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)))) (h "1z2cr1k1cavpyn19c23sfg7hxzz3892a93jbkq8a578lhwpdbzh1")))

