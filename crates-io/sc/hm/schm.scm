(define-module (crates-io sc hm schm) #:use-module (crates-io))

(define-public crate-schm-0.1.0 (c (n "schm") (v "0.1.0") (h "0mfd28waagrawymkgjmdpanbs4z4rssmlrlhl1dsakaqk50r2860") (y #t)))

(define-public crate-schm-0.1.1 (c (n "schm") (v "0.1.1") (h "0n6hyf4i981q8k6f8v4vmck7nbj4l2mblb8m67zjhx0mvb4ib2rm") (y #t)))

(define-public crate-schm-0.1.2 (c (n "schm") (v "0.1.2") (h "1mpvrmqm68kn95ib6pykqb352jf3m83zl3c6pkkis32qqx47i7c0") (y #t)))

