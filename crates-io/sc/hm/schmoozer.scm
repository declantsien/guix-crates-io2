(define-module (crates-io sc hm schmoozer) #:use-module (crates-io))

(define-public crate-schmoozer-0.1.1 (c (n "schmoozer") (v "0.1.1") (d (list (d (n "async-trait") (r "^0.1.77") (d #t) (k 0)) (d (n "killswitch") (r "^0.4.2") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("macros" "net" "rt-multi-thread" "time"))) (d #t) (k 2)))) (h "10g4pxxv41p2q29krywwfxq5my171xzfy19xyigwvf0m4bmh3x28") (s 2) (e (quote (("tcpconn" "dep:killswitch" "tokio/macros" "tokio/net" "tokio/time")))) (r "1.74")))

(define-public crate-schmoozer-0.1.2 (c (n "schmoozer") (v "0.1.2") (d (list (d (n "async-trait") (r "^0.1.77") (d #t) (k 0)) (d (n "killswitch") (r "^0.4.2") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("macros" "net" "rt-multi-thread" "time"))) (d #t) (k 2)))) (h "0b1x4d1kl0cxv86p3cr4ynvnziix9jvbbyv89h8blvk6kc92qjwi") (s 2) (e (quote (("tcpconn" "dep:killswitch" "tokio/macros" "tokio/net" "tokio/time")))) (r "1.74")))

