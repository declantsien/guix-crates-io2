(define-module (crates-io sc yt scythe_platform) #:use-module (crates-io))

(define-public crate-scythe_platform-0.1.0 (c (n "scythe_platform") (v "0.1.0") (h "1bscdlz2cc4v1gpi80y0164dxn3v0ad9fmd8vg16fzbhnkmychr8")))

(define-public crate-scythe_platform-0.1.1 (c (n "scythe_platform") (v "0.1.1") (h "0r4isg5q57n7yzwws4bgglg0fjyp40lk3lr0qn1mschlppb8qr25")))

