(define-module (crates-io sc yt scythe_paths) #:use-module (crates-io))

(define-public crate-scythe_paths-0.1.0 (c (n "scythe_paths") (v "0.1.0") (h "0988sbi21fml9m54nws9nlcqidd1v1f2i9nrbaz8jwgrmvshj19v")))

(define-public crate-scythe_paths-0.1.1 (c (n "scythe_paths") (v "0.1.1") (h "04bmvq7y828zlqh7xhps81hwf784l03ikqwq2v5a57yz905qiph6")))

