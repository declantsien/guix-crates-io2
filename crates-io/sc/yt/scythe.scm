(define-module (crates-io sc yt scythe) #:use-module (crates-io))

(define-public crate-scythe-0.1.0 (c (n "scythe") (v "0.1.0") (h "0mwfd87b4q5gxkj0zgpsr9hjyv0sinq5yyn9srv113wfs29mx0yw")))

(define-public crate-scythe-0.1.1 (c (n "scythe") (v "0.1.1") (d (list (d (n "scythe_data") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "scythe_paths") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "scythe_platform") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "1h84bvfw6jqwhxmbpq7hyvxqpzi3shsgj0ciwjp50r8hqzfxw0g0") (f (quote (("platform" "scythe_platform") ("paths" "scythe_paths") ("default" "data" "paths" "platform") ("data" "scythe_data"))))))

