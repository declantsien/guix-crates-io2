(define-module (crates-io sc yt scytale) #:use-module (crates-io))

(define-public crate-scytale-0.1.0 (c (n "scytale") (v "0.1.0") (d (list (d (n "arrayvec") (r "^0.7.4") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.18") (d #t) (k 0)) (d (n "serde") (r "^1.0.198") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.116") (d #t) (k 0)) (d (n "serde_json_path") (r "^0.6.7") (d #t) (k 0)))) (h "0aba8hh1wmh5rnwgfl98r88ydc1gpa4zw0d2qp4khmp8v818n228")))

