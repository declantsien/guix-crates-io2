(define-module (crates-io sc yt scythe_data) #:use-module (crates-io))

(define-public crate-scythe_data-0.1.0 (c (n "scythe_data") (v "0.1.0") (h "1fw8by5imcqxfkqmqn87x18aynyk5wn5lva5szzb4qdprf559pvx")))

(define-public crate-scythe_data-0.1.1 (c (n "scythe_data") (v "0.1.1") (h "0k0zwxj0b5bal1mf3k6nl367kf6wx9nz0r7flbgh21rvmsccp80v")))

