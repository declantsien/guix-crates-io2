(define-module (crates-io sc hu schuppe) #:use-module (crates-io))

(define-public crate-schuppe-0.0.1 (c (n "schuppe") (v "0.0.1") (h "10mk46k4d1k315wndw4fckjy2hjvwllqszlxp72gdpikff5vshmq")))

(define-public crate-schuppe-0.0.2 (c (n "schuppe") (v "0.0.2") (h "1c8xnp5vzd4ymhzaqkafrkj20kmlfwbwmi8a2z98lpckga5rwnkb")))

