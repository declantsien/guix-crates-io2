(define-module (crates-io sc hr schrift-rs) #:use-module (crates-io))

(define-public crate-schrift-rs-0.1.0 (c (n "schrift-rs") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.69") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1mzyrb7cmh541bx1kpr1wqvs0hky7ycjmklwyfg5jzf1srv96fvm")))

