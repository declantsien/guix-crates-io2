(define-module (crates-io sc hr schroedinger_box) #:use-module (crates-io))

(define-public crate-schroedinger_box-0.0.0 (c (n "schroedinger_box") (v "0.0.0") (h "1p6zf2pppcv21yppc29jrbc3418xiivz7lwdi7misk3wrjfw1rj7")))

(define-public crate-schroedinger_box-0.0.1 (c (n "schroedinger_box") (v "0.0.1") (h "0fv1sa5vzwj8pnd2szzijg1krcj11w118xmmf26vlm3ffcdh5zh6")))

(define-public crate-schroedinger_box-0.0.2 (c (n "schroedinger_box") (v "0.0.2") (d (list (d (n "rand") (r "*") (d #t) (k 0)))) (h "03xkwn51sm0dk5xajpwkhvx9b8aymq2vwzc3q84ih59jjm8smcmj")))

