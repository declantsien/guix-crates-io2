(define-module (crates-io sc im scim_v2) #:use-module (crates-io))

(define-public crate-scim_v2-0.2.1 (c (n "scim_v2") (v "0.2.1") (d (list (d (n "automod") (r "^1.0.14") (d #t) (k 2)) (d (n "indoc") (r "^2.0.4") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "ref-cast") (r "^1.0.22") (d #t) (k 2)) (d (n "rustversion") (r "^1.0.14") (d #t) (k 2)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)))) (h "0qxw99mgm1vp3vjfn5k8cp9qlz5wv1pdhzy1nj3ya27bipvp57ml") (y #t) (r "1.56")))

(define-public crate-scim_v2-0.2.2 (c (n "scim_v2") (v "0.2.2") (d (list (d (n "automod") (r "^1.0.14") (d #t) (k 2)) (d (n "indoc") (r "^2.0.4") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "ref-cast") (r "^1.0.22") (d #t) (k 2)) (d (n "rustversion") (r "^1.0.14") (d #t) (k 2)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)))) (h "0k164x5l7mdr3wipf66n13bvvcf6ip4s4mfw3v9k52nbmk3bsvjc") (r "1.65")))

(define-public crate-scim_v2-0.2.3 (c (n "scim_v2") (v "0.2.3") (d (list (d (n "automod") (r "^1.0.14") (d #t) (k 2)) (d (n "indoc") (r "^2.0.4") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "ref-cast") (r "^1.0.22") (d #t) (k 2)) (d (n "rustversion") (r "^1.0.14") (d #t) (k 2)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)))) (h "0j0wdhwgx47j1ry0djswn0mdnla1jyh4nif3h02xxb1m9yykccbv") (r "1.65")))

