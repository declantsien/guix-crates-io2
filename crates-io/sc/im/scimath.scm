(define-module (crates-io sc im scimath) #:use-module (crates-io))

(define-public crate-scimath-0.1.0 (c (n "scimath") (v "0.1.0") (h "0yx6j9wdifmirm78v7npxsdq8bqy8hjpsy044312p3nvp6fzfgzn")))

(define-public crate-scimath-0.1.1 (c (n "scimath") (v "0.1.1") (h "0bc61p91pf30hg9f1g77gsmpn14xc6483w06c8bpgbw3fkhswaqa")))

(define-public crate-scimath-0.1.2 (c (n "scimath") (v "0.1.2") (h "0gr467gnrymzkyibkpqzpn5qrk9qy53vhyr1fi2ivskajsygpql8")))

