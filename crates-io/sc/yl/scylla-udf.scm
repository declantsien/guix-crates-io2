(define-module (crates-io sc yl scylla-udf) #:use-module (crates-io))

(define-public crate-scylla-udf-0.1.0 (c (n "scylla-udf") (v "0.1.0") (d (list (d (n "bigdecimal") (r "^0.2.0") (d #t) (k 0)) (d (n "bytes") (r "^1.2.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "libc") (r "^0.2.119") (d #t) (k 0)) (d (n "num-bigint") (r "^0.3") (d #t) (k 0)) (d (n "scylla-cql") (r "^0.0.4") (d #t) (k 0)) (d (n "scylla-udf-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "uuid") (r "^1.0") (d #t) (k 0)))) (h "19c81r4k720i9jy123zxigsf1i97qiv715d04d5fs1fq38vhccfd") (r "1.66.1")))

