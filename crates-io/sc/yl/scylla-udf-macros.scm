(define-module (crates-io sc yl scylla-udf-macros) #:use-module (crates-io))

(define-public crate-scylla-udf-macros-0.1.0 (c (n "scylla-udf-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("full"))) (d #t) (k 0)))) (h "020xf9ixcp216hdv393v8dy01c9g6hvavg544rp9m5rslbrj4ibv") (r "1.66.1")))

