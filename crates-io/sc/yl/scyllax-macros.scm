(define-module (crates-io sc yl scyllax-macros) #:use-module (crates-io))

(define-public crate-scyllax-macros-0.1.0-alpha (c (n "scyllax-macros") (v "0.1.0-alpha") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "darling") (r "^0.20") (f (quote ("suggestions"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "derive" "extra-traits"))) (d #t) (k 0)))) (h "1dzzr5fl9mflvaj4crci3vavwxqjsb7lch2xib1nd9nxp5rihbmz")))

(define-public crate-scyllax-macros-0.1.1-alpha (c (n "scyllax-macros") (v "0.1.1-alpha") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "darling") (r "^0.20") (f (quote ("suggestions"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "derive" "extra-traits"))) (d #t) (k 0)))) (h "0fcc03r6xfkkaslmg1f0ccq8dikl14s6gh1bmj6fg3y3krlck59n")))

(define-public crate-scyllax-macros-0.1.2-alpha (c (n "scyllax-macros") (v "0.1.2-alpha") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "darling") (r "^0.20") (f (quote ("suggestions"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "derive" "extra-traits"))) (d #t) (k 0)))) (h "0rp307qjrndc2bgjar38rc6fhda3h3hf0dic6im77qx62g9ygqd8")))

(define-public crate-scyllax-macros-0.1.3-alpha (c (n "scyllax-macros") (v "0.1.3-alpha") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "darling") (r "^0.20") (f (quote ("suggestions"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "derive" "extra-traits"))) (d #t) (k 0)))) (h "0jlhyr55afzaqpcczjwv8zxzq15jzsbrx7rfdhahbyr2hw86y1ia")))

(define-public crate-scyllax-macros-0.1.4-alpha (c (n "scyllax-macros") (v "0.1.4-alpha") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "darling") (r "^0.20") (f (quote ("suggestions"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "derive" "extra-traits"))) (d #t) (k 0)))) (h "0qxsa0440diw1jyc4shs85m71b24pb6gqpn8clm9zbs3sjmxj9aa")))

(define-public crate-scyllax-macros-0.1.5-alpha (c (n "scyllax-macros") (v "0.1.5-alpha") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "darling") (r "^0.20") (f (quote ("suggestions"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "derive" "extra-traits"))) (d #t) (k 0)))) (h "00pkm90sg46x16lc0035phkigd3ik7x2cdwzqksppnzfmkf0hzhg")))

(define-public crate-scyllax-macros-0.1.6-alpha (c (n "scyllax-macros") (v "0.1.6-alpha") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "darling") (r "^0.20") (f (quote ("suggestions"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "derive" "extra-traits"))) (d #t) (k 0)))) (h "0bdyabx7x8iix3707nvbdfid35x9b4z0vh0hxi3h4s9sirhaadq8")))

(define-public crate-scyllax-macros-0.1.7-alpha (c (n "scyllax-macros") (v "0.1.7-alpha") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "darling") (r "^0.20") (f (quote ("suggestions"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "derive" "extra-traits"))) (d #t) (k 0)))) (h "18awy0h26p79mxiyqcazxkg9gdllw3p1zkb8hjlmpayn4w7ppqxf")))

(define-public crate-scyllax-macros-0.1.8-alpha (c (n "scyllax-macros") (v "0.1.8-alpha") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "darling") (r "^0.20") (f (quote ("suggestions"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "derive" "extra-traits"))) (d #t) (k 0)))) (h "1wlj8diss2ihxjjvxj7qpspdkym80bb0ryy79vlgd87qz66sahng")))

(define-public crate-scyllax-macros-0.1.9-alpha (c (n "scyllax-macros") (v "0.1.9-alpha") (d (list (d (n "scyllax-macros-core") (r "^0.1.9-alpha") (d #t) (k 0)))) (h "0d0l8n6pni7gjy64piqj6mxy5bdjyj192250lmwc9wkx1ikab6j6")))

(define-public crate-scyllax-macros-0.1.9-alpha.2 (c (n "scyllax-macros") (v "0.1.9-alpha.2") (d (list (d (n "scyllax-macros-core") (r "^0.1.9-alpha.2") (d #t) (k 0)))) (h "0dxapipjagxrvv0z99nlqm36n45161gp87f8sx65palq4bzcj2hw")))

(define-public crate-scyllax-macros-0.1.9-alpha.3 (c (n "scyllax-macros") (v "0.1.9-alpha.3") (d (list (d (n "scyllax-macros-core") (r "^0.1.9-alpha.2") (d #t) (k 0)))) (h "0rr269r5q79g0047kzpznz34hdp1bsrbi8n02qw4mgaxd9w9cdsy")))

(define-public crate-scyllax-macros-0.1.9-alpha.4 (c (n "scyllax-macros") (v "0.1.9-alpha.4") (d (list (d (n "scyllax-macros-core") (r "^0.1.9-alpha.4") (d #t) (k 0)))) (h "1yrjnjsyz7zqzl1k8ah801lrj0p9cimp3r0c7bb71cfmvy8qnb3h")))

(define-public crate-scyllax-macros-0.1.10-alpha (c (n "scyllax-macros") (v "0.1.10-alpha") (d (list (d (n "scyllax-macros-core") (r "^0.1.10-alpha") (d #t) (k 0)))) (h "0d1qwhjmbfvbnm6hzf2axyjvsnxd760q2iq373l4y17qsdlk468r")))

(define-public crate-scyllax-macros-0.1.11-alpha (c (n "scyllax-macros") (v "0.1.11-alpha") (d (list (d (n "scyllax-macros-core") (r "^0.1.11-alpha") (d #t) (k 0)))) (h "0yb8px2d2chx32fhqrr86h9dzakz92qsd2l1gf91ph2pf5l6wq60")))

(define-public crate-scyllax-macros-0.1.0 (c (n "scyllax-macros") (v "0.1.0") (d (list (d (n "scyllax-macros-core") (r "^0.1.0") (d #t) (k 0)))) (h "1hplsjzd3nv0if5xzjs8bzyw0cnl6sqhgv2s32q68wmw436i6nwr")))

(define-public crate-scyllax-macros-0.1.1-alpha.2 (c (n "scyllax-macros") (v "0.1.1-alpha.2") (d (list (d (n "scyllax-macros-core") (r "^0.1.1-alpha") (d #t) (k 0)))) (h "06pfhavddkfsl6vj1gmj9hx6w9sg4ajzz1fzxq7w08nzr9d7n79q")))

(define-public crate-scyllax-macros-0.1.1 (c (n "scyllax-macros") (v "0.1.1") (d (list (d (n "scyllax-macros-core") (r "^0.1.1") (d #t) (k 0)))) (h "1z0i8l846p95972v9l9hfsg9dh6jjbzn232j158dv50kcrwa3vhk")))

(define-public crate-scyllax-macros-0.2.0-alpha (c (n "scyllax-macros") (v "0.2.0-alpha") (d (list (d (n "scyllax-macros-core") (r "^0.2.0-alpha") (d #t) (k 0)))) (h "1hhi986in4bd4nrw79rm2kp5yp2mggxkb3bnsrgqj955rx3i72wx")))

(define-public crate-scyllax-macros-0.2.0-alpha.1 (c (n "scyllax-macros") (v "0.2.0-alpha.1") (d (list (d (n "scyllax-macros-core") (r "^0.2.0-alpha.1") (d #t) (k 0)))) (h "13yg0wx517panxp97wy6grinmyv19id654vmvlwln13x76mfyvgz")))

(define-public crate-scyllax-macros-0.2.0-alpha.3 (c (n "scyllax-macros") (v "0.2.0-alpha.3") (d (list (d (n "scyllax-macros-core") (r "^0.2.0-alpha.3") (d #t) (k 0)))) (h "1fr7y8nfy4cav0zgrrsv581w7w5f19j0rr18xybhl6b66izwk65z")))

(define-public crate-scyllax-macros-0.2.0 (c (n "scyllax-macros") (v "0.2.0") (d (list (d (n "scyllax-macros-core") (r "^0.2.0") (d #t) (k 0)))) (h "05yq32ln2ia0fy7ij2srxqr3q08a02ba2h3kj819xmis9yxj2pvw")))

(define-public crate-scyllax-macros-0.2.1 (c (n "scyllax-macros") (v "0.2.1") (d (list (d (n "scyllax-macros-core") (r "^0.2.1") (d #t) (k 0)))) (h "0p2djfqm8v8hqdbhrplb9g667ps0nvws72q4zna1f8b7801bz6ry")))

