(define-module (crates-io sc yl scylla_orm_query_parser) #:use-module (crates-io))

(define-public crate-scylla_orm_query_parser-0.1.0 (c (n "scylla_orm_query_parser") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "scylla") (r "^0.3.0") (d #t) (k 0)) (d (n "scylla_orm") (r "^0.1") (d #t) (k 0)) (d (n "scylla_orm_table_to_struct") (r "^0.1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)) (d (n "uuid") (r "^0.8") (d #t) (k 0)))) (h "1cb10513lhzw9a77wfgd9x0mdh0ini50q38ijxyd64j4k2kg9cr3")))

