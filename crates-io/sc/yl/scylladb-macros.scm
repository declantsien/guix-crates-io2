(define-module (crates-io sc yl scylladb-macros) #:use-module (crates-io))

(define-public crate-scylladb-macros-0.1.0 (c (n "scylladb-macros") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "scylladb-parse") (r "^0.1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 2)))) (h "0754n9pii9lc386v7q3aljidwcy61sgc0188x9imjh96kclsyqpm")))

