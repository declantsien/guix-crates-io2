(define-module (crates-io sc yl scyllax-parser) #:use-module (crates-io))

(define-public crate-scyllax-parser-0.1.8-alpha (c (n "scyllax-parser") (v "0.1.8-alpha") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.1.0") (d #t) (k 2)))) (h "04rh01jdyib167hjbrifh532qp7dv8gcy1kfklr3wln8gd3na21p")))

(define-public crate-scyllax-parser-0.1.9-alpha (c (n "scyllax-parser") (v "0.1.9-alpha") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)))) (h "0sf931kj1dr6lqn15q0z6mc4f6hcm3l03hish8fwkinr2hpqrg1b")))

(define-public crate-scyllax-parser-0.1.9-alpha.2 (c (n "scyllax-parser") (v "0.1.9-alpha.2") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)))) (h "1ycxyfs8n1vhwpxnjabkjivmdnpcqlgks0h1ri3v7lbl8nds75is")))

(define-public crate-scyllax-parser-0.1.9-alpha.3 (c (n "scyllax-parser") (v "0.1.9-alpha.3") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)))) (h "07h35jwgw2xx184g70545hkxl5mwray2imm47l5nn0vn4mz7jcdv")))

(define-public crate-scyllax-parser-0.1.9-alpha.4 (c (n "scyllax-parser") (v "0.1.9-alpha.4") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)))) (h "16pww116sahdzibkz0ysp8flrl9vzmqvqdsgmdlfxl2qy3wz5jwg")))

(define-public crate-scyllax-parser-0.1.10-alpha (c (n "scyllax-parser") (v "0.1.10-alpha") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)))) (h "0hlhr9x7yvslwzdvxygzp59cssdxl8ch66jz056d8lnvqivmpi43")))

(define-public crate-scyllax-parser-0.1.11-alpha (c (n "scyllax-parser") (v "0.1.11-alpha") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)))) (h "1brlz6xa5vwqya0w92wjbpwy7qzlx46b43zkq57pdpmv1q7slnfm")))

(define-public crate-scyllax-parser-0.1.0 (c (n "scyllax-parser") (v "0.1.0") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)))) (h "0x0g3hv5bmy14kchkr4sjbsm0aikfdi2rd48yf7prgzkby5ihx66")))

(define-public crate-scyllax-parser-0.1.1-alpha (c (n "scyllax-parser") (v "0.1.1-alpha") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)))) (h "1i6vdavwrkbh6an4jxibb11b5dfx9l30p2x8pgc1v7vaxiahmkv9")))

(define-public crate-scyllax-parser-0.1.1 (c (n "scyllax-parser") (v "0.1.1") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)))) (h "18faikk5yrafc1avvrkahw1is5ws9b2vd6bvi7gqqwrd657pppwz")))

(define-public crate-scyllax-parser-0.2.0-alpha (c (n "scyllax-parser") (v "0.2.0-alpha") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)))) (h "1sk7zmympakgzaab4ln2ghv9fs8iybdjndlj2ldy84c7vzbrbc1a")))

(define-public crate-scyllax-parser-0.2.0 (c (n "scyllax-parser") (v "0.2.0") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)))) (h "00bn31kwn8p3vhl6b0n2k2sgcx5lj0m9hwf4ys0rn2qw37qlj4kc")))

(define-public crate-scyllax-parser-0.2.1 (c (n "scyllax-parser") (v "0.2.1") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)))) (h "1lz6vzl2q3df39pmjd4fa21kmv651cj9cim3252jpm2h07j2bvz1")))

