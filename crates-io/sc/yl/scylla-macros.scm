(define-module (crates-io sc yl scylla-macros) #:use-module (crates-io))

(define-public crate-scylla-macros-0.1.0 (c (n "scylla-macros") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "196fyphqr8n5y99dsqdnpndhkcvnl122g6q76z2lgsa01z1krrk6")))

(define-public crate-scylla-macros-0.1.1 (c (n "scylla-macros") (v "0.1.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "073h1aaqfk2llndw9bbrsnjsg9birdh34pk1vyrzwx0jzfphrp0k")))

(define-public crate-scylla-macros-0.1.2 (c (n "scylla-macros") (v "0.1.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1cl7aj8xy25vjbq9n0yrsbznwpkfjk94diqkj51qb457v8cklfz0")))

(define-public crate-scylla-macros-0.2.0 (c (n "scylla-macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0cnz0rnz8b02dnwynvv878k8saa6a44mlkza4haks5ppvgd7gmrj")))

(define-public crate-scylla-macros-0.2.1 (c (n "scylla-macros") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0zjs892w4dakdll1rcmj21g5nmid0x6hiw8zgp56f2divz9xwmsp")))

(define-public crate-scylla-macros-0.2.2 (c (n "scylla-macros") (v "0.2.2") (d (list (d (n "darling") (r "^0.20.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1p8w3j295jybjfzfz947fkpx7vajvjrs4awydspgj5rr2mjrinki") (y #t)))

(define-public crate-scylla-macros-0.3.0 (c (n "scylla-macros") (v "0.3.0") (d (list (d (n "darling") (r "^0.20.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1kmc0c2g0nbs4cabwgvycxbq7h93y3cx9v8r6ywcad7dvsf6bdi2")))

(define-public crate-scylla-macros-0.2.3 (c (n "scylla-macros") (v "0.2.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0mycn9vl2hzfhbcqdm2sr8wrvcmhq8rmhb58nna8i0zffhwarfv6")))

(define-public crate-scylla-macros-0.4.0 (c (n "scylla-macros") (v "0.4.0") (d (list (d (n "darling") (r "^0.20.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1yffz67m4350cvvv45napl8nzf2a2qwiv41670bfbmrzkkzqaq7b")))

(define-public crate-scylla-macros-0.5.0 (c (n "scylla-macros") (v "0.5.0") (d (list (d (n "darling") (r "^0.20.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "17vvlbkv060f97g9b9sqbp11izv53aw1ikkvlahydsxdi79y2pvy")))

