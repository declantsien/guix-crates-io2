(define-module (crates-io sc yl scylla-macros-flex) #:use-module (crates-io))

(define-public crate-scylla-macros-flex-1.0.0 (c (n "scylla-macros-flex") (v "1.0.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1bb2rxfca6cg5mqqj082zkrr3f6y37zwq2wpy3qbgywp1db0hn1m")))

(define-public crate-scylla-macros-flex-1.1.0 (c (n "scylla-macros-flex") (v "1.1.0") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "00z1nn2nr9i9fpywm402aszxcwmxkd3025xib8w2mqf9qcnlsjkc") (f (quote (("speedy") ("serde_json") ("rmp-serde") ("default") ("all" "serde_json" "rmp-serde" "speedy"))))))

(define-public crate-scylla-macros-flex-1.2.0 (c (n "scylla-macros-flex") (v "1.2.0") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1965m26gq1zyfqzylm999had4glkwbjm2sb5r3190dsr7nr0pjiz") (f (quote (("speedy") ("serde_json") ("rmp-serde") ("default") ("all" "serde_json" "rmp-serde" "speedy"))))))

(define-public crate-scylla-macros-flex-1.3.0 (c (n "scylla-macros-flex") (v "1.3.0") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0mzrj8679ln6dqqjdabxcgagg0g6a6jl01arvw26i6ginlhhlbcm") (f (quote (("speedy") ("serde_json") ("rmp-serde") ("default") ("all" "serde_json" "rmp-serde" "speedy"))))))

(define-public crate-scylla-macros-flex-1.3.1 (c (n "scylla-macros-flex") (v "1.3.1") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0fl204pg3wc7rkhhkjmisr0zwavj14kdx8yn1x7r0fvys80xhadx") (f (quote (("speedy") ("serde_json") ("rmp-serde") ("default") ("all" "serde_json" "rmp-serde" "speedy"))))))

(define-public crate-scylla-macros-flex-1.3.2 (c (n "scylla-macros-flex") (v "1.3.2") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1v0g0j6mkhpa0mgda6qmj34kg3g3f6dksidg0jr393ay7a80grl0") (f (quote (("speedy") ("serde_json") ("rmp-serde") ("default") ("all" "serde_json" "rmp-serde" "speedy"))))))

(define-public crate-scylla-macros-flex-1.4.0 (c (n "scylla-macros-flex") (v "1.4.0") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1p8k2yx887xbj85n9mjg1m5adbskkpjwa8zmf0ihfwnvwqyxi8sm") (f (quote (("speedy") ("serde_json") ("rmp-serde") ("default") ("all" "serde_json" "rmp-serde" "speedy"))))))

(define-public crate-scylla-macros-flex-1.4.1 (c (n "scylla-macros-flex") (v "1.4.1") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0d58lyzwvy8k4r3xjgrp97w16innk3mxmqin7zn85sdcyaj4wwwf") (f (quote (("speedy") ("serde_json") ("rmp-serde") ("default") ("all" "serde_json" "rmp-serde" "speedy"))))))

(define-public crate-scylla-macros-flex-1.4.2 (c (n "scylla-macros-flex") (v "1.4.2") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0z4wadngf8f98a20b976z89f23sgmcdvb9yfzfz21kwiw7s7a125") (f (quote (("speedy") ("serde_json") ("rmp-serde") ("default") ("all" "serde_json" "rmp-serde" "speedy"))))))

(define-public crate-scylla-macros-flex-1.5.0 (c (n "scylla-macros-flex") (v "1.5.0") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "070xx5g2q3w95vh7fa7zirbv21ygxd6m153wg2f35975xy4lfp3l") (f (quote (("speedy") ("serde_json") ("rmp-serde") ("protobuf") ("default") ("all" "serde_json" "rmp-serde" "speedy" "protobuf"))))))

(define-public crate-scylla-macros-flex-1.5.1 (c (n "scylla-macros-flex") (v "1.5.1") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0hh85wpnf06mcymd3cfchf4yfcrq3svgfxj4c4s3j5cldgamc1vg") (f (quote (("speedy") ("serde_json") ("rmp-serde") ("protobuf") ("default") ("all" "serde_json" "rmp-serde" "speedy" "protobuf"))))))

(define-public crate-scylla-macros-flex-1.5.2 (c (n "scylla-macros-flex") (v "1.5.2") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0lzcb36sqxd9x9f8aqglwcqca8an22q7s7zvh4zivscf3g7vk7z5") (f (quote (("speedy") ("serde_json") ("rmp-serde") ("protobuf") ("default") ("all" "serde_json" "rmp-serde" "speedy" "protobuf"))))))

(define-public crate-scylla-macros-flex-1.5.3 (c (n "scylla-macros-flex") (v "1.5.3") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1j1r30qr7d4hs0p4dkabiwx85ccwg1hrvgixiq5kiszk52xqxxgb") (f (quote (("speedy") ("serde_json") ("rmp-serde") ("protobuf") ("default") ("all" "serde_json" "rmp-serde" "speedy" "protobuf"))))))

(define-public crate-scylla-macros-flex-1.5.4 (c (n "scylla-macros-flex") (v "1.5.4") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1r0f99zxsf1ddwy3gkd1b88jyji0xcd06pw1029wqmc1kxnmnsfd") (f (quote (("speedy") ("serde_json") ("rmp-serde") ("protobuf") ("default") ("all" "serde_json" "rmp-serde" "speedy" "protobuf"))))))

