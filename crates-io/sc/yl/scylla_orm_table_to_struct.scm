(define-module (crates-io sc yl scylla_orm_table_to_struct) #:use-module (crates-io))

(define-public crate-scylla_orm_table_to_struct-0.1.0 (c (n "scylla_orm_table_to_struct") (v "0.1.0") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "scylla") (r "^0.3.0") (d #t) (k 0)) (d (n "scylla_orm") (r "^0.1") (d #t) (k 0)))) (h "0jj2n0f7y29pjbjprmskqmxlkx3fmj5g16a37bar2xg4565pkjc5")))

(define-public crate-scylla_orm_table_to_struct-0.1.1 (c (n "scylla_orm_table_to_struct") (v "0.1.1") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "scylla") (r "^0.3") (d #t) (k 0)) (d (n "scylla_orm") (r "^0.1") (d #t) (k 0)))) (h "108xp6wgwx8d5ikd94am57vm4d5kk2061a5xbf04d5adi96lq40l")))

(define-public crate-scylla_orm_table_to_struct-0.1.2 (c (n "scylla_orm_table_to_struct") (v "0.1.2") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "scylla") (r "^0.3") (d #t) (k 0)) (d (n "scylla_orm") (r "^0.1") (d #t) (k 0)))) (h "0qsj8lnhshkf07akm64hpg3kspy8rvn1z08azphlr09fhyljml9m")))

