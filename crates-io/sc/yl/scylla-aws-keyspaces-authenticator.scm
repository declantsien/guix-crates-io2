(define-module (crates-io sc yl scylla-aws-keyspaces-authenticator) #:use-module (crates-io))

(define-public crate-scylla-aws-keyspaces-authenticator-0.1.0 (c (n "scylla-aws-keyspaces-authenticator") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "aws-config") (r "^0.52") (d #t) (k 0)) (d (n "aws-types") (r "^0.52") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "data-encoding") (r "^2") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (d #t) (k 2)) (d (n "ring") (r "^0.16") (d #t) (k 0)) (d (n "scylla") (r "^0.7") (d #t) (k 0)) (d (n "scylla") (r "^0.7") (f (quote ("ssl"))) (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread"))) (d #t) (k 2)))) (h "01jk5i0zrnzh2gx5f3q2xr5b42x2wrb9ib6pvkjkl0n11qz90sp7")))

