(define-module (crates-io sc el sceller) #:use-module (crates-io))

(define-public crate-sceller-0.1.0 (c (n "sceller") (v "0.1.0") (d (list (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0m3ldvqp32cw1bpyrvb60z8mxc817nvk88k35icv3m97qc393lng")))

(define-public crate-sceller-0.1.1 (c (n "sceller") (v "0.1.1") (d (list (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0x42ics558aysc0nxxandls67m25gg93ak9w0pp2fgjzpxb9hffr")))

(define-public crate-sceller-0.1.2 (c (n "sceller") (v "0.1.2") (d (list (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0clha2vy05bzndvklq839chhvxhfcls637bs8b6j0wwcrcjj1gl2")))

(define-public crate-sceller-0.2.0 (c (n "sceller") (v "0.2.0") (d (list (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0i05gpz6a1yylldrsxmkv4syml7lc2i96rnga6qz5lv97dn57c22")))

(define-public crate-sceller-0.2.1 (c (n "sceller") (v "0.2.1") (d (list (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0zfhc6gm2iw0p52mz591f43a61qx0ma8n9spp08nfvf44rbcnam9")))

(define-public crate-sceller-0.3.0 (c (n "sceller") (v "0.3.0") (d (list (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "15jllrl4dvpasssrlr62nxvi89j0ckv2g4q296jpz8b5p3sjw0gx")))

