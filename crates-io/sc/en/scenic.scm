(define-module (crates-io sc en scenic) #:use-module (crates-io))

(define-public crate-scenic-0.1.0 (c (n "scenic") (v "0.1.0") (d (list (d (n "assert_cmd") (r "^2") (d #t) (k 2)) (d (n "cargo-husky") (r "^1") (f (quote ("user-hooks"))) (k 2)) (d (n "tectonic") (r "^0.14") (d #t) (k 0)))) (h "1aqw803n579xq577f8ajzn7nihfg49gpjx2nazxhm10kbs8c82zr")))

(define-public crate-scenic-0.1.1 (c (n "scenic") (v "0.1.1") (d (list (d (n "assert_cmd") (r "^2") (d #t) (k 2)) (d (n "cargo-husky") (r "^1") (f (quote ("user-hooks"))) (k 2)) (d (n "tectonic") (r "^0.14") (d #t) (k 0)))) (h "1vpmjzyi02l19jndbjw2m1mig270av7mzmy4wqljml5z4rhra27j")))

