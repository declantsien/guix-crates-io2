(define-module (crates-io sc en scene-graph) #:use-module (crates-io))

(define-public crate-scene-graph-0.1.0 (c (n "scene-graph") (v "0.1.0") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "thunderdome") (r "^0.6.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "petgraph") (r "^0.6.2") (d #t) (k 2)))) (h "10dax5lfi6la8c26wb6xf6mvs2g9g685r808mncm19a0jszrpnm4")))

