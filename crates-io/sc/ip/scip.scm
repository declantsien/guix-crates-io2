(define-module (crates-io sc ip scip) #:use-module (crates-io))

(define-public crate-scip-0.1.0 (c (n "scip") (v "0.1.0") (d (list (d (n "pretty_assertions") (r "^1.2.1") (d #t) (k 2)) (d (n "protobuf") (r "=3.1.0") (d #t) (k 0)))) (h "05v462hl8fib689l5ra3i0czh31nalwz5pw4s1rddhin2c70k6in") (r "1.60.0")))

(define-public crate-scip-0.1.1 (c (n "scip") (v "0.1.1") (d (list (d (n "pretty_assertions") (r "^1.2.1") (d #t) (k 2)) (d (n "protobuf") (r "=3.1.0") (d #t) (k 0)))) (h "0sqk96c9y0lz8w6gwnc3g1bzk6w3nw211dwdqzbzlsbg508bpgxj") (r "1.60.0")))

(define-public crate-scip-0.3.1 (c (n "scip") (v "0.3.1") (d (list (d (n "pretty_assertions") (r "^1.2.1") (d #t) (k 2)) (d (n "protobuf") (r "=3.2.0") (d #t) (k 0)))) (h "10qqg1y451950kvipbf6c4i2nnq01imw7j3hi3ahifm3c88d511y") (r "1.60.0")))

(define-public crate-scip-0.3.2 (c (n "scip") (v "0.3.2") (d (list (d (n "pretty_assertions") (r "^1.2.1") (d #t) (k 2)) (d (n "protobuf") (r "=3.2.0") (d #t) (k 0)))) (h "04w74hg5nva7lf5203w35hln28zqi33gkai18n0zzl8hc5vmkfh5") (r "1.60.0")))

(define-public crate-scip-0.3.3 (c (n "scip") (v "0.3.3") (d (list (d (n "pretty_assertions") (r "^1.2.1") (d #t) (k 2)) (d (n "protobuf") (r "=3.2.0") (d #t) (k 0)))) (h "1vqmzr6nhzqrrp1dw0ib302jcv45sbfkc95n9bw3l4s9cvb1pp75") (r "1.60.0")))

