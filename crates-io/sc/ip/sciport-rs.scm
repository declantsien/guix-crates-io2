(define-module (crates-io sc ip sciport-rs) #:use-module (crates-io))

(define-public crate-sciport-rs-0.0.1 (c (n "sciport-rs") (v "0.0.1") (d (list (d (n "complex-bessel-rs") (r "^0.0.4") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (f (quote ("rayon"))) (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "14dx1ipdm49wxwh7b5qq8mj2d7k7vis55d42jwjk8m9vhbz4jr26")))

(define-public crate-sciport-rs-0.0.2 (c (n "sciport-rs") (v "0.0.2") (d (list (d (n "complex-bessel-rs") (r "^0.0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "ndarray") (r "^0.15.6") (f (quote ("rayon"))) (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "numpy") (r "^0.19.0") (d #t) (k 0)) (d (n "pyo3") (r "^0.19.1") (f (quote ("full" "auto-initialize"))) (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1zvgri9aprnagbwrda3f6fbi4xws0yjp47cf57rzj3dv9qzxshfx")))

