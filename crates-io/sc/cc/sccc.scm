(define-module (crates-io sc cc sccc) #:use-module (crates-io))

(define-public crate-sccc-0.1.0 (c (n "sccc") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (d #t) (k 0)) (d (n "serde") (r "^1.0.171") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.102") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.43") (d #t) (k 0)) (d (n "tokio") (r "^1.29.1") (f (quote ("full"))) (d #t) (k 0)))) (h "0ysi0l6rlaf0jcalqpvpcvvpgmx10h33cx216ggj0s6zkiva0jw6")))

