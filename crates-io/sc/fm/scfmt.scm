(define-module (crates-io sc fm scfmt) #:use-module (crates-io))

(define-public crate-scfmt-0.1.0 (c (n "scfmt") (v "0.1.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "phf") (r "^0.10.1") (f (quote ("macros"))) (d #t) (k 0)))) (h "126fz1gbczyrpvqgrw1ivfnvkq3k4m0chl2q9p9z4rakmljhmqq7") (y #t)))

(define-public crate-scfmt-0.1.2 (c (n "scfmt") (v "0.1.2") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "phf") (r "^0.10.1") (f (quote ("macros"))) (d #t) (k 0)))) (h "08ka1v8iy51q7yj1rl4krv0sl9xzm5qz1wq8cm57brkc2s3arxld")))

(define-public crate-scfmt-0.2.0 (c (n "scfmt") (v "0.2.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "phf") (r "^0.10.1") (f (quote ("macros"))) (d #t) (k 0)))) (h "078pw9q2z8kgwzsb7cnda4mmaqyz757p6ilxxrif5jzlvilajqmy")))

(define-public crate-scfmt-0.2.1 (c (n "scfmt") (v "0.2.1") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "phf") (r "^0.10.1") (f (quote ("macros"))) (d #t) (k 0)) (d (n "version") (r "^3.0.0") (d #t) (k 0)))) (h "1iapaqv511afs8y6x03lddwg32rz4n3xglx03riiw6x756rab5n7")))

(define-public crate-scfmt-0.3.0 (c (n "scfmt") (v "0.3.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "phf") (r "^0.10.1") (f (quote ("macros"))) (d #t) (k 0)) (d (n "version") (r "^3.0.0") (d #t) (k 0)))) (h "0srz7pcqch4pclzw8g8y0xak8dgnc1n0wgk7pq3yfmxp3wjvhl5n")))

(define-public crate-scfmt-0.3.1 (c (n "scfmt") (v "0.3.1") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "phf") (r "^0.10.1") (f (quote ("macros"))) (d #t) (k 0)) (d (n "version") (r "^3.0.0") (d #t) (k 0)))) (h "15i3ic05v4r52ps8vmz9qhysyg9bkk0ncznl9njmhpaks8c7pa8j")))

