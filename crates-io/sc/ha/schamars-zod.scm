(define-module (crates-io sc ha schamars-zod) #:use-module (crates-io))

(define-public crate-schamars-zod-0.1.0 (c (n "schamars-zod") (v "0.1.0") (d (list (d (n "schemars") (r "^0.8") (f (quote ("chrono"))) (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "0yvc9hgd1fvi74wcq18c5q52ry5c8bgrlcfsb5aq56v3d66xzznd") (y #t)))

(define-public crate-schamars-zod-0.1.1 (c (n "schamars-zod") (v "0.1.1") (d (list (d (n "schemars") (r "^0.8") (f (quote ("chrono"))) (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "0rgwskmfcnj71vvh3vxpij9amdj2vy9q90wgmmgjr8v8cd8dgyl3") (y #t)))

(define-public crate-schamars-zod-0.1.2 (c (n "schamars-zod") (v "0.1.2") (d (list (d (n "schemars") (r "^0.8") (f (quote ("chrono"))) (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "0gq7av6zxddjp6ijij5ama90ypb2nyis4pxrz48b48ll5fk74fq7") (y #t)))

