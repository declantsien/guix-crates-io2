(define-module (crates-io sc ha schablone) #:use-module (crates-io))

(define-public crate-schablone-0.1.0 (c (n "schablone") (v "0.1.0") (d (list (d (n "clap") (r "~2.33") (d #t) (k 0)) (d (n "snafu") (r "~0.6") (d #t) (k 0)) (d (n "tera") (r "~1.5") (d #t) (k 0)))) (h "09z6qimy27xi1cvv91hnvqf5wcd9y7qxxhm7016n4hkzv7vrnnsq")))

(define-public crate-schablone-0.1.1 (c (n "schablone") (v "0.1.1") (d (list (d (n "clap") (r "~2.33") (d #t) (k 0)) (d (n "snafu") (r "~0.6") (d #t) (k 0)) (d (n "tera") (r "~1.5") (d #t) (k 0)))) (h "0r8iym21rg8xbrmz2shgqdzzy2d052n52ip6baylddzhbya2xald")))

(define-public crate-schablone-0.1.2 (c (n "schablone") (v "0.1.2") (d (list (d (n "clap") (r "~2.33") (d #t) (k 0)) (d (n "env_logger") (r "~0.7") (d #t) (k 0)) (d (n "log") (r "~0.4") (d #t) (k 0)) (d (n "snafu") (r "~0.6") (d #t) (k 0)) (d (n "tera") (r "~1.5") (d #t) (k 0)))) (h "1993spcypjqld7baqca4l26b36igyzfhwck367aiiz5q6k6pcir6")))

(define-public crate-schablone-0.2.0 (c (n "schablone") (v "0.2.0") (d (list (d (n "clap") (r "~2.33") (d #t) (k 0)) (d (n "env_logger") (r "~0.7") (d #t) (k 0)) (d (n "log") (r "~0.4") (d #t) (k 0)) (d (n "serde_json") (r "~1.0") (d #t) (k 0)) (d (n "snafu") (r "~0.6") (d #t) (k 0)) (d (n "tera") (r "~1.5") (d #t) (k 0)))) (h "135y3ab0x3spjrx5fzp8p4vbih16sylgvws16sccnsvbxbmkg0ph")))

(define-public crate-schablone-0.2.1 (c (n "schablone") (v "0.2.1") (d (list (d (n "clap") (r "~2.33") (d #t) (k 0)) (d (n "env_logger") (r "~0.7") (d #t) (k 0)) (d (n "log") (r "~0.4") (d #t) (k 0)) (d (n "serde_json") (r "~1.0") (d #t) (k 0)) (d (n "snafu") (r "~0.6") (d #t) (k 0)) (d (n "tera") (r "~1.5") (d #t) (k 0)))) (h "01mzqalp4glqzrx632ij33p1c8cw3p8p7n8nhp72a194dvnvg278")))

