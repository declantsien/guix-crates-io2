(define-module (crates-io sc sy scsys-stores) #:use-module (crates-io))

(define-public crate-scsys-stores-0.2.0 (c (n "scsys-stores") (v "0.2.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)) (d (n "smart-default") (r "^0.7") (d #t) (k 0)) (d (n "strum") (r "^0.26") (f (quote ("derive"))) (d #t) (k 0)))) (h "15qh8bhqygplcxjxy35d8a0gs3hkxylqrapbm19zzv2fi7dard98") (f (quote (("default")))) (s 2) (e (quote (("serde-ext" "dep:serde_json") ("serde" "dep:serde" "serde-ext"))))))

(define-public crate-scsys-stores-0.2.1 (c (n "scsys-stores") (v "0.2.1") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)) (d (n "smart-default") (r "^0.7") (d #t) (k 0)) (d (n "strum") (r "^0.26") (f (quote ("derive"))) (d #t) (k 0)))) (h "19ylzmrhgq4hhgp31a92ym5q04qmb96395hn29aq13agiigjklk7") (f (quote (("std") ("full" "default" "serde") ("default" "std")))) (s 2) (e (quote (("serde-ext" "dep:serde_json") ("serde" "dep:serde" "serde-ext"))))))

