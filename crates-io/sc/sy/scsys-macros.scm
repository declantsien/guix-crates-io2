(define-module (crates-io sc sy scsys-macros) #:use-module (crates-io))

(define-public crate-scsys-macros-0.1.0 (c (n "scsys-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full"))) (d #t) (k 0)))) (h "0jkpqzq8sakpkqxx324gn1ay2av79hbniwwdyb83ac5p453dzgxd")))

(define-public crate-scsys-macros-0.1.1 (c (n "scsys-macros") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full"))) (d #t) (k 0)))) (h "0x2r9nyrmcvqy18gny83c5f3dqk6s8gndhsfpmr47ixyj21war0z")))

(define-public crate-scsys-macros-0.1.2 (c (n "scsys-macros") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full"))) (d #t) (k 0)))) (h "03ya5jf5lw6ynl42xqwx631f7fz4aywb0ps3nzs8qmbi9j0441dm")))

(define-public crate-scsys-macros-0.1.3 (c (n "scsys-macros") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full"))) (d #t) (k 0)))) (h "0kd74l9i6ll1b4pzf31390wij9cpwfg2n4flhp1p2g506qjicz3c")))

(define-public crate-scsys-macros-0.1.4 (c (n "scsys-macros") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("full"))) (d #t) (k 0)))) (h "1af0ijpi3c7vrpn2pl2dcxbv7w3m5z3zlaz894zks14nfk3mkqlx")))

(define-public crate-scsys-macros-0.1.5 (c (n "scsys-macros") (v "0.1.5") (d (list (d (n "bson") (r "^2.4.0") (f (quote ("chrono-0_4" "serde_with" "uuid-0_8"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("full"))) (d #t) (k 0)))) (h "1a0v6inids9a8mrsrfy6ypfg6m2p0pabr47sm310hl3x7bqq6na7")))

(define-public crate-scsys-macros-0.1.6 (c (n "scsys-macros") (v "0.1.6") (d (list (d (n "bson") (r "^2.4.0") (f (quote ("chrono-0_4" "serde_with" "uuid-0_8"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("full"))) (d #t) (k 0)))) (h "0bx7f6rxd9b9kkcrjh2wkgyn2cvqqldrgwgi5n26kmxz1lqcwvpl")))

(define-public crate-scsys-macros-0.1.7 (c (n "scsys-macros") (v "0.1.7") (d (list (d (n "bson") (r "^2.4.0") (f (quote ("chrono-0_4" "serde_with" "uuid-0_8"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("full"))) (d #t) (k 0)))) (h "078k7siwf31kbxx35p32ra089rim8im4cx3x9gpciyxdafi09dv3")))

(define-public crate-scsys-macros-0.1.8 (c (n "scsys-macros") (v "0.1.8") (d (list (d (n "bson") (r "^2.4.0") (f (quote ("chrono-0_4" "serde_with" "uuid-0_8"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("full"))) (d #t) (k 0)))) (h "11k4f7cmif0qniid9ydynwbb57k20ykfgly59cn8sgs0n40xzc4f")))

(define-public crate-scsys-macros-0.1.9 (c (n "scsys-macros") (v "0.1.9") (d (list (d (n "bson") (r "^2.4.0") (f (quote ("chrono-0_4" "serde_with" "uuid-0_8"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("full"))) (d #t) (k 0)))) (h "132004a2c2rmwpwyvhf55cwnplq2h5n2h722gnz6cvpky7rlf3dm")))

(define-public crate-scsys-macros-0.1.20 (c (n "scsys-macros") (v "0.1.20") (d (list (d (n "bson") (r "^2.4.0") (f (quote ("chrono-0_4" "serde_with" "uuid-0_8"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.46") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.102") (f (quote ("full"))) (d #t) (k 0)))) (h "0nkyla4pw2k7sy2hcafkwmyxhq31cgw12wlnh5hlf9af47ngy02w")))

(define-public crate-scsys-macros-0.1.21 (c (n "scsys-macros") (v "0.1.21") (d (list (d (n "bson") (r "^2.4.0") (f (quote ("chrono-0_4" "serde_with" "uuid-0_8"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.46") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.102") (f (quote ("full"))) (d #t) (k 0)))) (h "188xk6wrxmil17gfk1i4a352mfq6504pacmzrcw6jwn4lsryfblc")))

(define-public crate-scsys-macros-0.1.22 (c (n "scsys-macros") (v "0.1.22") (d (list (d (n "bson") (r "^2.4.0") (f (quote ("chrono-0_4" "serde_with" "uuid-0_8"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.46") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.102") (f (quote ("full"))) (d #t) (k 0)))) (h "1m0ns91yn6y63y5gqn4nyzbi32qx5cj1gz2zamvzzmh3qvbsd9jz")))

(define-public crate-scsys-macros-0.1.23 (c (n "scsys-macros") (v "0.1.23") (d (list (d (n "bson") (r "^2.4.0") (f (quote ("chrono-0_4" "serde_with" "uuid-0_8"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.46") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "syn") (r "^1.0.102") (f (quote ("full"))) (d #t) (k 0)))) (h "06sb0w7rlq2zglh7ln6mj9qx2fy1rzjpm6yybp53vf8ixviahb5k")))

(define-public crate-scsys-macros-0.1.24 (c (n "scsys-macros") (v "0.1.24") (d (list (d (n "bson") (r "^2.4.0") (f (quote ("chrono-0_4" "serde_with" "uuid-0_8"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.46") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "syn") (r "^1.0.102") (f (quote ("full"))) (d #t) (k 0)))) (h "1xaa4hy6mp0vvczmbwzij1jp64g3ckgyl5gcmi8rk9nkkpj5kaks")))

(define-public crate-scsys-macros-0.1.25 (c (n "scsys-macros") (v "0.1.25") (d (list (d (n "bson") (r "^2.4.0") (f (quote ("chrono-0_4" "serde_with" "uuid-0_8"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.46") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "syn") (r "^1.0.102") (f (quote ("full"))) (d #t) (k 0)))) (h "144wz20rsryxrkzmmazv0lis25m9zqcsiq7qhym46il2izp5pcya")))

(define-public crate-scsys-macros-0.1.26 (c (n "scsys-macros") (v "0.1.26") (d (list (d (n "bson") (r "^2.4.0") (f (quote ("chrono-0_4" "serde_with" "uuid-0_8"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.46") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "syn") (r "^1.0.102") (f (quote ("full"))) (d #t) (k 0)))) (h "0ssvhnvmv066z9q83mzf3ic0508sqpanqg8n1yp8phm4hc7279zz")))

(define-public crate-scsys-macros-0.1.27 (c (n "scsys-macros") (v "0.1.27") (d (list (d (n "bson") (r "^2.4.0") (f (quote ("chrono-0_4" "serde_with" "uuid-0_8"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.46") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "syn") (r "^1.0.102") (f (quote ("full"))) (d #t) (k 0)))) (h "10557rvbw4bdkjsp41vnhzisrfsf91rrkvnvq6c6xxx72ypickn3")))

(define-public crate-scsys-macros-0.1.28 (c (n "scsys-macros") (v "0.1.28") (d (list (d (n "bson") (r "^2.4.0") (f (quote ("chrono-0_4" "serde_with" "uuid-0_8"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.46") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "syn") (r "^1.0.102") (f (quote ("full"))) (d #t) (k 0)))) (h "1f6c9qp3p5lnypl6xbmmqi4qwg0lyx9hlhdcv1mviz7gbymm502w")))

(define-public crate-scsys-macros-0.1.29 (c (n "scsys-macros") (v "0.1.29") (d (list (d (n "bson") (r "^2.4.0") (f (quote ("chrono-0_4" "serde_with" "uuid-0_8"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.46") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "syn") (r "^1.0.102") (f (quote ("full"))) (d #t) (k 0)))) (h "1pi4fp762mk8l0ky6njnbqvz1r830s5vg0g258fd1xkar9dkjb7z")))

(define-public crate-scsys-macros-0.1.30 (c (n "scsys-macros") (v "0.1.30") (d (list (d (n "bson") (r "^2.4.0") (f (quote ("chrono-0_4" "serde_with" "uuid-0_8"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.46") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "syn") (r "^1.0.102") (f (quote ("full"))) (d #t) (k 0)))) (h "0fdb1j5gc8y9kns9r7knym4p4qg606wxy37q1bnwfsiskslcngl6")))

(define-public crate-scsys-macros-0.1.31 (c (n "scsys-macros") (v "0.1.31") (d (list (d (n "bson") (r "^2.4.0") (f (quote ("chrono-0_4" "serde_with" "uuid-0_8"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.46") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "syn") (r "^1.0.102") (f (quote ("full"))) (d #t) (k 0)))) (h "0slb2yga3fixw7rg9sxyabidpavnlfc4h81gj52ahpybv26s5w7r")))

(define-public crate-scsys-macros-0.1.32 (c (n "scsys-macros") (v "0.1.32") (d (list (d (n "bson") (r "^2.4.0") (f (quote ("chrono-0_4" "serde_with" "uuid-0_8"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.46") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "syn") (r "^1.0.102") (f (quote ("full"))) (d #t) (k 0)))) (h "0djkp72fycahv6ww8gg1qnnqil89jfslyb09c4p9gkr0w1mk03nq")))

(define-public crate-scsys-macros-0.1.33 (c (n "scsys-macros") (v "0.1.33") (d (list (d (n "bson") (r "^2.4.0") (f (quote ("chrono-0_4" "serde_with" "uuid-0_8"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.46") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "syn") (r "^1.0.102") (f (quote ("full"))) (d #t) (k 0)))) (h "1cjsdr38nyf6f11w8iqvfbhfyvw1ymlk74bh2k7d4kmdpgajkkds")))

(define-public crate-scsys-macros-0.1.34 (c (n "scsys-macros") (v "0.1.34") (d (list (d (n "bson") (r "^2.4.0") (f (quote ("chrono-0_4" "serde_with" "uuid-0_8"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.46") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "syn") (r "^1.0.102") (f (quote ("full"))) (d #t) (k 0)))) (h "1y80q08qz82msc4k2gagkrwp7f4a1r0wi00ayiw1ndzrs8s2gr08")))

(define-public crate-scsys-macros-0.1.35 (c (n "scsys-macros") (v "0.1.35") (d (list (d (n "bson") (r "^2.4.0") (f (quote ("chrono-0_4" "serde_with" "uuid-0_8"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.46") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "syn") (r "^1.0.102") (f (quote ("full"))) (d #t) (k 0)))) (h "1h9dyiahb5y8z6w636b3vqazlki1p13wz0h7qk8ndd8ls7fw7419")))

(define-public crate-scsys-macros-0.1.36 (c (n "scsys-macros") (v "0.1.36") (d (list (d (n "bson") (r "^2.4.0") (f (quote ("chrono-0_4" "serde_with" "uuid-0_8"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.46") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "syn") (r "^1.0.102") (f (quote ("full"))) (d #t) (k 0)))) (h "0zwbr7zbf4s131pwbyngl1wmvp7ggvrqxk340f7zpjgy93ws47jr")))

(define-public crate-scsys-macros-0.1.37 (c (n "scsys-macros") (v "0.1.37") (d (list (d (n "bson") (r "^2.4.0") (f (quote ("chrono-0_4" "serde_with" "uuid-0_8"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.46") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "syn") (r "^1.0.102") (f (quote ("full"))) (d #t) (k 0)))) (h "1hmsc9rrhajnqcb3qppk85dv6hnva1rr50xkfl0yfkajqfd2h72b")))

(define-public crate-scsys-macros-0.1.38 (c (n "scsys-macros") (v "0.1.38") (d (list (d (n "bson") (r "^2.4.0") (f (quote ("chrono-0_4" "serde_with" "uuid-0_8"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.49") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("full"))) (d #t) (k 0)))) (h "1yclsnff3rkjq6p2dhc7x5rk3mkynf41wz4g17hn80q4i1pmhvbb")))

(define-public crate-scsys-macros-0.1.39 (c (n "scsys-macros") (v "0.1.39") (d (list (d (n "bson") (r "^2.4.0") (f (quote ("chrono-0_4" "serde_with" "uuid-0_8"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.49") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("full"))) (d #t) (k 0)))) (h "188wwm1lv54kkybj2pqagmchblb0r6czw9abrg19kprmc62ishla")))

(define-public crate-scsys-macros-0.1.40 (c (n "scsys-macros") (v "0.1.40") (d (list (d (n "bson") (r "^2.4.0") (f (quote ("chrono-0_4" "serde_with" "uuid-0_8"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.49") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("full"))) (d #t) (k 0)))) (h "198i6785wyqfynbiydb9d587d4gb7rpg4nf4l9g9b23mcb585z6x")))

(define-public crate-scsys-macros-0.1.41 (c (n "scsys-macros") (v "0.1.41") (d (list (d (n "bson") (r "^2.4.0") (f (quote ("chrono-0_4" "serde_with" "uuid-0_8"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.49") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("full"))) (d #t) (k 0)))) (h "0pz90w423qx0pr69rcb6xg0idmb14rkxdkg402ssx050g6xqp6pk")))

(define-public crate-scsys-macros-0.1.42 (c (n "scsys-macros") (v "0.1.42") (d (list (d (n "bson") (r "^2.4.0") (f (quote ("chrono-0_4" "serde_with" "uuid-0_8"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.49") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("full"))) (d #t) (k 0)))) (h "0glmhqhi4l0mrgb2axhmh9hh1x7cmignrmrjg43ri73sywmhr5i5")))

(define-public crate-scsys-macros-0.2.0-alpha (c (n "scsys-macros") (v "0.2.0-alpha") (d (list (d (n "getrandom") (r "^0.2") (f (quote ("js"))) (d #t) (t "wasm32-unknown-unknown") (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "19k06s2mw0hrvk37bdxyjw4qq3pj9fch73lx6qcrpjpn5nj0xb00") (f (quote (("gen") ("full" "gen") ("default"))))))

(define-public crate-scsys-macros-0.2.0 (c (n "scsys-macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1pdqb3xjrbkxrykpwsxl8hmjhxb3s26z9860zi6yyiaqdi0wvrda")))

(define-public crate-scsys-macros-0.2.1 (c (n "scsys-macros") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0g6zdlj12imjq8w9s5bp1h8dqw20isg9dj9qggd9y9kx2pk4jick")))

(define-public crate-scsys-macros-0.2.2 (c (n "scsys-macros") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "06h2mwk5a148yvaqiqwpmzwxp2bdwc5lk6448i1gz8889pd4s59q")))

