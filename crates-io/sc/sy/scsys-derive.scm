(define-module (crates-io sc sy scsys-derive) #:use-module (crates-io))

(define-public crate-scsys-derive-0.1.0 (c (n "scsys-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full"))) (d #t) (k 0)))) (h "0dhbb5vwlchw26kq50nmck0m48yk0ld3hliifr5hlzj4vhsnd351")))

(define-public crate-scsys-derive-0.1.1 (c (n "scsys-derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full"))) (d #t) (k 0)))) (h "05j507dh713r199791bdd7jvga8ajqml57gdnvhhkwsbfrrg1yn7")))

(define-public crate-scsys-derive-0.1.2 (c (n "scsys-derive") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full"))) (d #t) (k 0)))) (h "019bbdr1ba5804biznpzr8xn241gl2pxgz9dbbcbdqwqlahjdai9")))

(define-public crate-scsys-derive-0.1.3 (c (n "scsys-derive") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full"))) (d #t) (k 0)))) (h "1zxiggawgqfxr7wplgr5y3xphllhkkcyfl48dvpnmkxyldks52sk")))

(define-public crate-scsys-derive-0.1.4 (c (n "scsys-derive") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("full"))) (d #t) (k 0)))) (h "1xz1860wgy7cycsjiq50514gkdsxvjpb53n1hzxcnfbxv1znyhs4")))

(define-public crate-scsys-derive-0.1.5 (c (n "scsys-derive") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("full"))) (d #t) (k 0)))) (h "0wxn555hr5gdkzy82scd69pa9pryq2dw92c9bqf1n7garjb1xrz6")))

(define-public crate-scsys-derive-0.1.6 (c (n "scsys-derive") (v "0.1.6") (d (list (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("full"))) (d #t) (k 0)))) (h "1d728b4zymr99ml2myyyk3vj4hfzw37v2ml3p3qx7x10ch8blhc3")))

(define-public crate-scsys-derive-0.1.7 (c (n "scsys-derive") (v "0.1.7") (d (list (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("full"))) (d #t) (k 0)))) (h "00mjqr8vyy0zzwrzfwz6pc17jzgz14l6ckskpc5w6v9b5f3ynsw6")))

(define-public crate-scsys-derive-0.1.8 (c (n "scsys-derive") (v "0.1.8") (d (list (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("full"))) (d #t) (k 0)))) (h "0w86x6yhhbmb4zrw2bs1i8kxwhqpsbk57hj6x78hzrxmldz3b6w3")))

(define-public crate-scsys-derive-0.1.9 (c (n "scsys-derive") (v "0.1.9") (d (list (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("full"))) (d #t) (k 0)))) (h "1g18szw3jv8xhxwhq6d6fwhjny3f2nszbj24r20lqilcnrnp1721")))

(define-public crate-scsys-derive-0.1.20 (c (n "scsys-derive") (v "0.1.20") (d (list (d (n "proc-macro2") (r "^1.0.46") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.102") (f (quote ("full"))) (d #t) (k 0)))) (h "1c55k2xcvrdf5vlz2xvsprf3l8pvp1a915h5rh1dd5n5idybpa1b")))

(define-public crate-scsys-derive-0.1.21 (c (n "scsys-derive") (v "0.1.21") (d (list (d (n "proc-macro2") (r "^1.0.46") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.102") (f (quote ("full"))) (d #t) (k 0)))) (h "160609z28m5wvvajbvxy22vys967y8plvkjc862fd5wich40pkxh")))

(define-public crate-scsys-derive-0.1.22 (c (n "scsys-derive") (v "0.1.22") (d (list (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.102") (f (quote ("full"))) (d #t) (k 0)))) (h "18n8lxrvq9m768jh1a362zzv2pmqx4xc30s7f870ha703j3cfssy")))

(define-public crate-scsys-derive-0.1.23 (c (n "scsys-derive") (v "0.1.23") (d (list (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.102") (f (quote ("full"))) (d #t) (k 0)))) (h "071zjlyn98pynws5y8h5mfvm7sa5c1zysw3dikf47yn0cwqmhh6z")))

(define-public crate-scsys-derive-0.1.24 (c (n "scsys-derive") (v "0.1.24") (d (list (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.102") (f (quote ("full"))) (d #t) (k 0)))) (h "1361qf3gp3pvx6zmc4jzmz5qhqfgg63kaxizkxak7kzppjqc7whg")))

(define-public crate-scsys-derive-0.1.25 (c (n "scsys-derive") (v "0.1.25") (d (list (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("full"))) (d #t) (k 0)))) (h "1b4m056sdfzwcdszrzghxnc70xf8gdnklgwhvgirdys21qvb15jw")))

(define-public crate-scsys-derive-0.1.26 (c (n "scsys-derive") (v "0.1.26") (d (list (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("full"))) (d #t) (k 0)))) (h "02wqs2qwbh82kb1b0p20ddp0yfp52239dvj9l4f9ci4zqp60j3ql")))

(define-public crate-scsys-derive-0.1.27 (c (n "scsys-derive") (v "0.1.27") (d (list (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("full"))) (d #t) (k 0)))) (h "0wxvn5bqrxgb7m4aswip7q9zhb0wrv1mzc52m1k9nkbsqwdf01xk")))

(define-public crate-scsys-derive-0.1.28 (c (n "scsys-derive") (v "0.1.28") (d (list (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("full"))) (d #t) (k 0)))) (h "10mvm0ki9ddw894kqhr0ssnlfan9ppyqfqk03vvnjrlf0wgas2g9")))

(define-public crate-scsys-derive-0.1.29 (c (n "scsys-derive") (v "0.1.29") (d (list (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("full"))) (d #t) (k 0)))) (h "1y6zdy5b6cdzy7i7kfafnp6bydar4dlzl94dgki5vqd2642p1bnk")))

(define-public crate-scsys-derive-0.1.30 (c (n "scsys-derive") (v "0.1.30") (d (list (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("full"))) (d #t) (k 0)))) (h "1xz6w5jxp5k99jamy3r6633iqs0nal1sryx9xpdig2g00m4y3xd6")))

(define-public crate-scsys-derive-0.1.31 (c (n "scsys-derive") (v "0.1.31") (d (list (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("full"))) (d #t) (k 0)))) (h "08fnkg722vkcbnpgwppgfnd7p4ilv5wjmfkh87wnvakvhjysnkb3")))

(define-public crate-scsys-derive-0.1.32 (c (n "scsys-derive") (v "0.1.32") (d (list (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("full"))) (d #t) (k 0)))) (h "0y5fwlxy78h1jhjx1gis302cdfyajqfnilnsi3pniyi6jrsibnd2")))

(define-public crate-scsys-derive-0.1.33 (c (n "scsys-derive") (v "0.1.33") (d (list (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("full"))) (d #t) (k 0)))) (h "1ykgg9h2443nwkxnakin93922c71px7f37056srv6fznqygrzssi")))

(define-public crate-scsys-derive-0.1.34 (c (n "scsys-derive") (v "0.1.34") (d (list (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("full"))) (d #t) (k 0)))) (h "032f3gbc66yiks05a39ga6kqrcm4rl5x45mk7498ay9gd4fqw041")))

(define-public crate-scsys-derive-0.1.35 (c (n "scsys-derive") (v "0.1.35") (d (list (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("full"))) (d #t) (k 0)))) (h "1gl7myd90rmwy3cj02c6d8axsmp1cmxp8s7s39dm2ijj50ma9r9n")))

(define-public crate-scsys-derive-0.1.36 (c (n "scsys-derive") (v "0.1.36") (d (list (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("full"))) (d #t) (k 0)))) (h "1ln158ignq5fjgpqw71gqvf59pd9qwr1725z9l48s6i743mmshfm")))

(define-public crate-scsys-derive-0.1.37 (c (n "scsys-derive") (v "0.1.37") (d (list (d (n "proc-macro2") (r "^1.0.48") (d #t) (k 0)) (d (n "quote") (r "^1.0.22") (d #t) (k 0)) (d (n "syn") (r "^1.0.106") (f (quote ("full"))) (d #t) (k 0)))) (h "0fpnz2y02csvcyas4ic92lkb6xi8s0y56vbjlv7y9d6xvqf771fg")))

(define-public crate-scsys-derive-0.1.38 (c (n "scsys-derive") (v "0.1.38") (d (list (d (n "proc-macro2") (r "^1.0.48") (d #t) (k 0)) (d (n "quote") (r "^1.0.22") (d #t) (k 0)) (d (n "syn") (r "^1.0.106") (f (quote ("full"))) (d #t) (k 0)))) (h "1rwsf3j2w24zgh7mbln54cmgjh5i6knxwrs8g83l56k2ldr6qxsx")))

(define-public crate-scsys-derive-0.1.39 (c (n "scsys-derive") (v "0.1.39") (d (list (d (n "proc-macro2") (r "^1.0.48") (d #t) (k 0)) (d (n "quote") (r "^1.0.22") (d #t) (k 0)) (d (n "syn") (r "^1.0.106") (f (quote ("full"))) (d #t) (k 0)))) (h "0kw49my1pb6ahpm69i8bil1ss9yvr29vc8x69bpx6r6q65bn991q")))

(define-public crate-scsys-derive-0.1.40 (c (n "scsys-derive") (v "0.1.40") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1h39xn0wm5yprwgg79pxwyd1fjv2k9mnfxxdb3c1hn5bvyivacnz")))

(define-public crate-scsys-derive-0.1.41 (c (n "scsys-derive") (v "0.1.41") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0mc970fxl7icglvr1x1qm6hnlbr1h74nwavqh0s8zd282lc6ci7r")))

(define-public crate-scsys-derive-0.1.42 (c (n "scsys-derive") (v "0.1.42") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1gwgyv3czaj62v48l1q1bd7rbiz7yy556gv4acn4621xnbivmipw")))

(define-public crate-scsys-derive-0.2.0-alpha (c (n "scsys-derive") (v "0.2.0-alpha") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "10a30pivvmqfwsgj43m6iv2wqjyfrbpdl711cmpi41h916nzmvp8")))

(define-public crate-scsys-derive-0.2.0 (c (n "scsys-derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0a53c6xj07pl6ag2pav573lfwf2z21jwzlssnn6d4zwbd5ps42pf")))

(define-public crate-scsys-derive-0.2.1 (c (n "scsys-derive") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0s7qsp48ya6ybd0jggmpm5mfv7y7wzgyx14x4appsxgjn75dmmi3")))

(define-public crate-scsys-derive-0.2.2 (c (n "scsys-derive") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1s1fal7q8fq08375qrxfx232c5d22prg782m7rp4wv18hdx9wma6")))

