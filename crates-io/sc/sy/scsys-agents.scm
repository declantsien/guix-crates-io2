(define-module (crates-io sc sy scsys-agents) #:use-module (crates-io))

(define-public crate-scsys-agents-0.1.27 (c (n "scsys-agents") (v "0.1.27") (d (list (d (n "chrono") (r "^0.4.22") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1zhm6682hl1dxka5jpx6dam3fc42vd9dni0vg3afldkgp4hwipqy") (f (quote (("default"))))))

(define-public crate-scsys-agents-0.1.28 (c (n "scsys-agents") (v "0.1.28") (d (list (d (n "chrono") (r "^0.4.22") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (d #t) (k 0)))) (h "007y55cjjpds6l4fph410ddsiy1c9p263pxa4vwi9f4cqd9q3pyq") (f (quote (("wasm-ext") ("wasm" "wasm-ext") ("full" "core" "extras" "wasm") ("extras") ("default" "core" "wasm") ("core"))))))

