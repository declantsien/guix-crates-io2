(define-module (crates-io sc si scsir) #:use-module (crates-io))

(define-public crate-scsir-0.1.0 (c (n "scsir") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "modular-bitfield-msb") (r "^0.11.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)) (d (n "nix") (r "^0.25.0") (d #t) (t "cfg(unix)") (k 0)) (d (n "windows") (r "^0.43.0") (f (quote ("Win32_Foundation" "Win32_System_IO" "Win32_System_Ioctl" "Win32_Storage_FileSystem" "Win32_Storage_IscsiDisc"))) (d #t) (t "cfg(windows)") (k 0)))) (h "07cck75lv40c2mf45ha454y10idcpxxv3cq4g49kl45dqwa7qxzc")))

