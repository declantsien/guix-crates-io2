(define-module (crates-io sc si scsi) #:use-module (crates-io))

(define-public crate-scsi-0.1.0 (c (n "scsi") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.2.7") (k 0)))) (h "0nr5bci4ifqcf4fk4bqj96jd0by85plyk1qpvxncr2q17rcshvwy")))

(define-public crate-scsi-0.1.1 (c (n "scsi") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.2.7") (k 0)))) (h "08x6j928n25i86dc9yj2mbw87qg5znbngqh8h2z2jcgqksklnxn5")))

(define-public crate-scsi-0.2.0 (c (n "scsi") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.2.7") (k 0)))) (h "0m5id8w5p8vnmwlmw6a24vc51wfs4x6w1mmrrb8az9i3lyi8g7lf")))

(define-public crate-scsi-0.2.1 (c (n "scsi") (v "0.2.1") (d (list (d (n "byteorder") (r "^1.2.7") (k 0)))) (h "0dmx6a4izq3vsbihdh9mbyqhfsabs67vwsnx1dxk09h4lhl8nsxj")))

