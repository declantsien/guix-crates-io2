(define-module (crates-io ty me tyme4rs) #:use-module (crates-io))

(define-public crate-tyme4rs-1.0.0 (c (n "tyme4rs") (v "1.0.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)))) (h "1vf0vhkby9hgdri2n3iaqnhi0h2m53mx7clil6yn2lq5bvpgbkcr")))

(define-public crate-tyme4rs-1.0.1 (c (n "tyme4rs") (v "1.0.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)))) (h "0s2g7haplw6afwk69ai299r64sbpzxlw86gplb5gy8n58n3in5wv")))

(define-public crate-tyme4rs-1.0.2 (c (n "tyme4rs") (v "1.0.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)))) (h "1x71l1z7vma5fpxldxhnkzjbsrbv8lsqwxccvia6lh1fsla9z633")))

