(define-module (crates-io ty po typo-eq) #:use-module (crates-io))

(define-public crate-typo-eq-0.1.1 (c (n "typo-eq") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.22") (d #t) (k 0)) (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)) (d (n "quick-xml") (r "^0.25.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0w9p3ps42h3j2v05nr6ymy4xc4yl49xis8v1cph64v142wzh757x")))

(define-public crate-typo-eq-0.2.0 (c (n "typo-eq") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.22") (d #t) (k 0)) (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)) (d (n "quick-xml") (r "^0.25.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0k4a6fli8ybq24j9gxwjxkpr30d186ayiv21z93yayd49yblrv4v")))

(define-public crate-typo-eq-0.3.0 (c (n "typo-eq") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4.22") (d #t) (k 0)) (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)) (d (n "quick-xml") (r "^0.25.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0nwvbxi3v61ilb393wlqac1f2h23jl2796wds963il543l5a333h")))

