(define-module (crates-io ty po typos-json-to-checkstyle) #:use-module (crates-io))

(define-public crate-typos-json-to-checkstyle-0.1.0 (c (n "typos-json-to-checkstyle") (v "0.1.0") (d (list (d (n "checkstyle_formatter") (r "^1.0.2") (d #t) (k 0)) (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 0)))) (h "102fc4qqg2shinllnd2a2ymx4nj6kafj4qpy2hsqwzcpwl60lv43")))

(define-public crate-typos-json-to-checkstyle-0.1.1 (c (n "typos-json-to-checkstyle") (v "0.1.1") (d (list (d (n "checkstyle_formatter") (r "^1.0.2") (d #t) (k 0)) (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 0)))) (h "074nawfpd70hysz4fc5wimlgjkx1ychj9lm5jli5lvfinv1zl68x")))

