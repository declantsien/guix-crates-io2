(define-module (crates-io ty po typout) #:use-module (crates-io))

(define-public crate-typout-0.1.0 (c (n "typout") (v "0.1.0") (h "18qamzdsb3ypb0vz53qr1nakwzddryl3jr6dj4nzll48j33mpxza")))

(define-public crate-typout-0.1.1 (c (n "typout") (v "0.1.1") (h "0q0vxwapq8cl881d4ahad57ij5awj8930l0l6gg3qq72zxlsd21k")))

(define-public crate-typout-0.1.2 (c (n "typout") (v "0.1.2") (h "1m64xp1aygr90qrcy3iww17kxzzlxxsc1h39bwf1mcy0kvmp60qg")))

(define-public crate-typout-0.1.3 (c (n "typout") (v "0.1.3") (d (list (d (n "ansistr") (r "^0.1.1") (d #t) (k 0)))) (h "077jkmhxxm6bi9nfnjnv9dlisyw4q1hdpmmk46v0l09vg5faddzw")))

(define-public crate-typout-0.2.0 (c (n "typout") (v "0.2.0") (d (list (d (n "crossterm") (r "^0.17.6") (d #t) (k 0)))) (h "1ncw7f8c4y39y99wln41vz1hvcrdk9j35mdfs3qzfbxnpy6b3rjj")))

(define-public crate-typout-0.2.1 (c (n "typout") (v "0.2.1") (d (list (d (n "crossterm") (r "^0.17.6") (d #t) (k 0)))) (h "0l3378av8vrlk1im0103cr7n6czxgakahp8v3sv8x2lir6apcz25")))

(define-public crate-typout-0.2.2 (c (n "typout") (v "0.2.2") (d (list (d (n "crossterm") (r "^0.17.6") (d #t) (k 0)))) (h "1px2i5570fdxz83v04wrx129sanrmhsl3z9magymq0jlnp16ylqp")))

