(define-module (crates-io ty po typographic_linter) #:use-module (crates-io))

(define-public crate-typographic_linter-0.1.0 (c (n "typographic_linter") (v "0.1.0") (d (list (d (n "regex") (r "^0.2.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0") (d #t) (k 0)))) (h "1b5dw6lglddhvk84df5f7m8l3sllqjxfjl9sqwb1x32m02x2ifgg")))

(define-public crate-typographic_linter-0.2.0 (c (n "typographic_linter") (v "0.2.0") (d (list (d (n "regex") (r "^0.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "18hqwady9lmgqds6inl2g3lz715a988lflfr6bffjkf2wbiqx96x")))

