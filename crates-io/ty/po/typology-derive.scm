(define-module (crates-io ty po typology-derive) #:use-module (crates-io))

(define-public crate-typology-derive-0.1.0 (c (n "typology-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0w8vybxljmbgfj8drvpfb2ax0vz6f8ccz93y2j45qbn9wqcngam0") (r "1.71")))

(define-public crate-typology-derive-0.1.1 (c (n "typology-derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1k78dv765clxcidwf0855xgw6hw6azd9k26lxl3h2myw5q8sd0dv") (r "1.71")))

