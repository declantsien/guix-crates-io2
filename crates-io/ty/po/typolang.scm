(define-module (crates-io ty po typolang) #:use-module (crates-io))

(define-public crate-typolang-0.4.6 (c (n "typolang") (v "0.4.6") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0dlniahgm4zxpb9kr28cvmavjljvn8m86fi45yvci56s0wmkf77r") (y #t)))

(define-public crate-typolang-0.1.0 (c (n "typolang") (v "0.1.0") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "13p2hg58i4m53kimawgghda1wqrq8nb5yamndafq38dxzf30qwla") (y #t)))

(define-public crate-typolang-0.1.1 (c (n "typolang") (v "0.1.1") (d (list (d (n "clap") (r "^2.34") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "17ailw8lwmmzw77q4dp83kls0j672wzrciy9zy0zlh0d9z14w1i9") (y #t)))

(define-public crate-typolang-0.1.2 (c (n "typolang") (v "0.1.2") (d (list (d (n "clap") (r "^2.34") (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)))) (h "19zxxhw3cxy7js5cb46s7qrvg83qp1240kyk6md2qvggrnwhjfa2") (y #t)))

