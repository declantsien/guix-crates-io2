(define-module (crates-io ty po typos-git-commit) #:use-module (crates-io))

(define-public crate-typos-git-commit-0.1.0 (c (n "typos-git-commit") (v "0.1.0") (d (list (d (n "clap") (r "^4.0.32") (f (quote ("derive" "color"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)))) (h "1ik8nd7nl2ndwffc83gccrs6763c7qmvm25jmsm68f3qx2ag4n82")))

(define-public crate-typos-git-commit-0.1.1 (c (n "typos-git-commit") (v "0.1.1") (d (list (d (n "clap") (r "^4.0.32") (f (quote ("derive" "color"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)))) (h "1mvv9269qal0a9fa5vkdcpvg5wriii7wpg1h6cdjf5djrj822ll7")))

(define-public crate-typos-git-commit-0.1.2 (c (n "typos-git-commit") (v "0.1.2") (d (list (d (n "clap") (r "^4.0.32") (f (quote ("derive" "color"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)))) (h "073l9ksfjg5m8qyx5y5ffd4yfmbrm0qy767arklwfw69v2icvwf6")))

(define-public crate-typos-git-commit-0.1.3 (c (n "typos-git-commit") (v "0.1.3") (d (list (d (n "clap") (r "^4.0.32") (f (quote ("derive" "color"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)))) (h "18fc3rzswllrmx5wifwhb47b6bjp8976riwca4fnfllb89yhb04b")))

(define-public crate-typos-git-commit-0.1.4 (c (n "typos-git-commit") (v "0.1.4") (d (list (d (n "clap") (r "^4.0.32") (f (quote ("derive" "color"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)))) (h "03ix9fgx84wm9i30im54xja2mc6af1yc7bn0kja2qvjgr386i8c1")))

(define-public crate-typos-git-commit-0.1.5 (c (n "typos-git-commit") (v "0.1.5") (d (list (d (n "clap") (r "^4.0.32") (f (quote ("derive" "color"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)))) (h "01dgax825n780lgr16086ffn2i6dnsml5bhlw1jvhkmfdk30pill")))

(define-public crate-typos-git-commit-0.1.6 (c (n "typos-git-commit") (v "0.1.6") (d (list (d (n "clap") (r "^4.0.32") (f (quote ("derive" "color"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)))) (h "0xfy7ak1qfl5mp7pymjy6qkwr00964r97yg4lj9ji19j8yg9hqcx")))

(define-public crate-typos-git-commit-0.2.0 (c (n "typos-git-commit") (v "0.2.0") (d (list (d (n "clap") (r "^4.0.32") (f (quote ("derive" "color"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)))) (h "06wjg5jrammlqrcfma9ym198h4scgall3268ai2nzjrf86ip3apl")))

(define-public crate-typos-git-commit-0.3.0 (c (n "typos-git-commit") (v "0.3.0") (d (list (d (n "clap") (r "^4.0.32") (f (quote ("derive" "color"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)))) (h "10hlj3qfdxancs26qqkz4qnykvvlhp1ydqxwzhd3ib312k7papwb")))

(define-public crate-typos-git-commit-0.3.1 (c (n "typos-git-commit") (v "0.3.1") (d (list (d (n "clap") (r "^4.0.32") (f (quote ("derive" "color" "cargo"))) (d #t) (k 0)) (d (n "clap_complete") (r "^4.4.9") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)))) (h "11wdr1zc09gmg3plsjwq27ivc8kflji30iafvhf77243kmqnl07f")))

(define-public crate-typos-git-commit-0.4.0 (c (n "typos-git-commit") (v "0.4.0") (d (list (d (n "clap") (r "^4.0.32") (f (quote ("derive" "color" "cargo"))) (d #t) (k 0)) (d (n "clap_complete") (r "^4.4.9") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)))) (h "0718iafy3bipm7k954flki7lgdha4p1ikr6pnlf6p4is07xma6xh")))

(define-public crate-typos-git-commit-0.5.0 (c (n "typos-git-commit") (v "0.5.0") (d (list (d (n "clap") (r "^4.0.32") (f (quote ("derive" "color" "cargo"))) (d #t) (k 0)) (d (n "clap_complete") (r "^4.4.9") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)))) (h "1jxpggk7lmm7qrxwmq0vggdv0i839374y9w9vjw82iy1ff97zga8")))

