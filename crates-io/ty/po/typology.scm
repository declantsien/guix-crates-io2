(define-module (crates-io ty po typology) #:use-module (crates-io))

(define-public crate-typology-0.1.0 (c (n "typology") (v "0.1.0") (d (list (d (n "typology-derive") (r "^0.1") (d #t) (k 0)))) (h "0zj5r276nlxx81206qh8g5fw0jv1lkwfssn9h74x5w0l8ws97b0l") (r "1.71")))

(define-public crate-typology-0.1.1 (c (n "typology") (v "0.1.1") (d (list (d (n "typology-derive") (r "^0.1") (d #t) (k 0)))) (h "14k5b8lw0fkgli9y79vzqcmzb17pxrly7h86h1llmrivkvn3cl5j") (r "1.71")))

