(define-module (crates-io ty po typoscale) #:use-module (crates-io))

(define-public crate-typoscale-0.1.0 (c (n "typoscale") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "045zpb3jaa8kczvzqjhksy1lff3g4qvn929z5vvj86dc5zc7w5mz")))

(define-public crate-typoscale-0.2.0 (c (n "typoscale") (v "0.2.0") (d (list (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "1bhq9x5x73chimr11py33znaw5nks49gaibcb257p326z2ff6l21")))

(define-public crate-typoscale-0.2.1 (c (n "typoscale") (v "0.2.1") (d (list (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "0f1aa59wjlvaw1s5bg8va80lspgpx7rjfqy7zskp8ml2cjmh4vn6")))

