(define-module (crates-io ty la tylar) #:use-module (crates-io))

(define-public crate-tylar-0.1.0 (c (n "tylar") (v "0.1.0") (h "0bppxq631hi63gf8nfar5wdb9a6f7imd071rmnwrd2cxcalr13sk")))

(define-public crate-tylar-0.1.1 (c (n "tylar") (v "0.1.1") (h "1w1b0jcdn0vkz2wa6wkjk81larly3xxk7i1mn8g7cmmn7r12ggh4") (f (quote (("nightly"))))))

(define-public crate-tylar-0.1.2 (c (n "tylar") (v "0.1.2") (h "0q0i624hpxpjvwa84llanm9nkbxjk1yv10q0c13k44w9z1gq7g1a") (f (quote (("nightly"))))))

(define-public crate-tylar-0.2.0 (c (n "tylar") (v "0.2.0") (h "13n1s8pydqzik7nrhxvr6npj9lmlmwa4lzplbbmz8a4awyvridrs") (f (quote (("nightly"))))))

(define-public crate-tylar-0.2.1 (c (n "tylar") (v "0.2.1") (h "08jnf3g2dgdswr7awdv5sp1iq0nck5z918k4mf60q9hzs2kr5c87") (f (quote (("nightly"))))))

(define-public crate-tylar-0.2.2 (c (n "tylar") (v "0.2.2") (h "172cybq1vabwln1q18n72ch1d45jlimhmg9wmnhwdhx6diw6cmqh") (f (quote (("nightly"))))))

