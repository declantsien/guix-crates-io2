(define-module (crates-io ty py typycal) #:use-module (crates-io))

(define-public crate-typycal-0.1.0 (c (n "typycal") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "termion") (r "^1.5.6") (d #t) (k 0)))) (h "1cy7myrmrx77iigd7v9h6vnqbvarh79lhzzjpdpvi1pczlsjd9w7")))

(define-public crate-typycal-0.2.0 (c (n "typycal") (v "0.2.0") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (k 0)) (d (n "termion") (r "^1.5.6") (d #t) (k 0)))) (h "1sgbgm00g704j1l0p2dfa6j38djbslk4a09ihfwsmqcdl7zy6lg2")))

