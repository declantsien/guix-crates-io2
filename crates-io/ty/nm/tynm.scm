(define-module (crates-io ty nm tynm) #:use-module (crates-io))

(define-public crate-tynm-0.1.0 (c (n "tynm") (v "0.1.0") (d (list (d (n "nom") (r "^5.0.1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)))) (h "0cv0qhx8r0vclg6v4iarbzs6ywbnb8hsrlfpwrzviym7m0zknjgb")))

(define-public crate-tynm-0.1.1 (c (n "tynm") (v "0.1.1") (d (list (d (n "nom") (r "^5.0.1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)))) (h "0vm04jzx4p46lgkcvvy9w8di0v098y792davz5lcrmkff6vxdafh")))

(define-public crate-tynm-0.1.2 (c (n "tynm") (v "0.1.2") (d (list (d (n "nom") (r "^5.1.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)))) (h "0v2m3hi09va5mk2gqnnxjjnpczxv5k0j9dsc4c08mbdjxxq6hrjj")))

(define-public crate-tynm-0.1.3 (c (n "tynm") (v "0.1.3") (d (list (d (n "nom") (r "^5.1.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)))) (h "0a6yg0z3pm7slyilj8xzrqkdirgrb5cjxv2qhyiax85i6pzxi1sy")))

(define-public crate-tynm-0.1.4 (c (n "tynm") (v "0.1.4") (d (list (d (n "nom") (r "^5.1.1") (f (quote ("std"))) (k 0)) (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)))) (h "0knk747j6hl2fw2i008z135sxjki315waqik1alv8q9rjs0vfzrn")))

(define-public crate-tynm-0.1.5 (c (n "tynm") (v "0.1.5") (d (list (d (n "nom") (r "^5.1.1") (f (quote ("std"))) (k 0)) (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)))) (h "02pawkxxf0ibh3nyrl62d4ll2s7gvvrxx4dhn7jh9b855bdv2c8m")))

(define-public crate-tynm-0.1.6 (c (n "tynm") (v "0.1.6") (d (list (d (n "nom") (r "^5.1.1") (f (quote ("std"))) (k 0)) (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)))) (h "0gb2a06qbcls9j1ynymafnbj5as0rps83a8vckvx3hy95nm2rpx4")))

(define-public crate-tynm-0.1.7 (c (n "tynm") (v "0.1.7") (d (list (d (n "nom") (r "^7.1.3") (f (quote ("std"))) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)))) (h "1cpk4q81rspn31mgr041rn4v9qvhk87xsaxrr2ah8k393vgfqnfd")))

(define-public crate-tynm-0.1.8 (c (n "tynm") (v "0.1.8") (d (list (d (n "nom") (r "^7.1.3") (f (quote ("alloc"))) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)))) (h "0i9n1012dcr5hpn4gh1026bkvcr2fqcgac4f0dykq7cnhpy3y6qb") (f (quote (("default"))))))

(define-public crate-tynm-0.1.9 (c (n "tynm") (v "0.1.9") (d (list (d (n "nom") (r "^7.1.3") (f (quote ("alloc"))) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)))) (h "0s90k7fwwlkwlr8pxai3jwvwi7ws86185rnxsjalcap4d4g4826c") (f (quote (("default"))))))

(define-public crate-tynm-0.1.10 (c (n "tynm") (v "0.1.10") (d (list (d (n "nom") (r "^7.1.3") (f (quote ("alloc"))) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.31") (d #t) (k 2)))) (h "1vc12w4b1imk2gbbiy9cpk785v6g14s8ayiyzq9qwiyid5gd0c5x") (f (quote (("info") ("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

