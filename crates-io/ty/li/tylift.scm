(define-module (crates-io ty li tylift) #:use-module (crates-io))

(define-public crate-tylift-0.1.0 (c (n "tylift") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4.26") (d #t) (k 0)) (d (n "quote") (r "^0.6.11") (d #t) (k 0)) (d (n "syn") (r "^0.15.26") (d #t) (k 0)))) (h "0iqvnrsk6irqn50rvma2gr4ppz2yws3vi3x8kavfz6447vah5bd5")))

(define-public crate-tylift-0.2.0 (c (n "tylift") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^0.4.26") (d #t) (k 0)) (d (n "quote") (r "^0.6.11") (d #t) (k 0)) (d (n "syn") (r "^0.15.26") (d #t) (k 0)))) (h "1gcan1y2phx8m7krc7x9aglgc2wskgkylynjb124gwlcb73xyg4i")))

(define-public crate-tylift-0.3.0 (c (n "tylift") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^0.4.26") (d #t) (k 0)) (d (n "quote") (r "^0.6.11") (d #t) (k 0)) (d (n "syn") (r "^0.15.26") (f (quote ("full"))) (d #t) (k 0)))) (h "0fclm71gf8m3nysmvpsp7mf4776ljcaa2hnvmk8w2as9g83n3z2l") (f (quote (("span_errors"))))))

(define-public crate-tylift-0.3.1 (c (n "tylift") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^0.4.30") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "syn") (r "^0.15.34") (f (quote ("full"))) (d #t) (k 0)))) (h "0i0pz2g4595xzl1ai4m7gqd5bxzbp8mrla56wiwfcz6j706llvg3") (f (quote (("span_errors"))))))

(define-public crate-tylift-0.3.2 (c (n "tylift") (v "0.3.2") (d (list (d (n "proc-macro2") (r "^1.0.7") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.13") (f (quote ("full"))) (d #t) (k 0)))) (h "0h2zcyck49pp1mkrzwd30vnrjmf294l6v2in02imgdgfs779qh1i") (f (quote (("span_errors"))))))

(define-public crate-tylift-0.3.3 (c (n "tylift") (v "0.3.3") (d (list (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.35") (f (quote ("full"))) (d #t) (k 0)))) (h "0b5sqwvl9r0r6ickljnf619gxcsx13bnmdds6fgv571v48d4ankp") (f (quote (("span_errors"))))))

(define-public crate-tylift-0.3.4 (c (n "tylift") (v "0.3.4") (d (list (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.35") (f (quote ("full"))) (d #t) (k 0)))) (h "1akcckx7h89zp3h6iqny9gfn5jgqkb7q12yvfspabdmxisidn7pv") (f (quote (("span_errors"))))))

(define-public crate-tylift-0.3.5 (c (n "tylift") (v "0.3.5") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.63") (f (quote ("full"))) (d #t) (k 0)))) (h "1697qsrglfayznchvi17sjg27wqq4yp4yb3bxg8qjm8kbl0pk0lv") (f (quote (("span_errors"))))))

