(define-module (crates-io ty li tylisp) #:use-module (crates-io))

(define-public crate-tylisp-0.1.0 (c (n "tylisp") (v "0.1.0") (d (list (d (n "frunk") (r "^0.3.1") (o #t) (d #t) (k 0)) (d (n "typenum") (r "^1.12.0") (d #t) (k 0)) (d (n "typenum-uuid") (r "^0.1.0") (d #t) (k 0)))) (h "0n93w0kyvg0wgyna1w80lw0d1chqmn5nxqrlmms8gcb42kpy8k7x") (f (quote (("const"))))))

