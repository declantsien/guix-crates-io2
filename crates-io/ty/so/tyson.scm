(define-module (crates-io ty so tyson) #:use-module (crates-io))

(define-public crate-tyson-0.1.0 (c (n "tyson") (v "0.1.0") (d (list (d (n "custom_error") (r "^1.9.2") (d #t) (k 0)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "13hinskan4bx71ajp991nima4kyd3mgy8c0l1kjw9vn2rcq8wqn6")))

