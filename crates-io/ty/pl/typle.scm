(define-module (crates-io ty pl typle) #:use-module (crates-io))

(define-public crate-typle-0.1.0 (c (n "typle") (v "0.1.0") (d (list (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "08gpf5dm2jj4ds3ahddpnwca6h91622v704nq5ckkvj5x8wlmi59")))

(define-public crate-typle-0.2.0 (c (n "typle") (v "0.2.0") (d (list (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1sda4gx7ymnrljg9nkrynrvklxvrmdl58pv2xj51qnfc3hsv7yjy")))

(define-public crate-typle-0.2.1 (c (n "typle") (v "0.2.1") (d (list (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1ydjs8kxw6n5i6pc3378wkjgx4r3dzf8bz9p3k12rk42ddwfhdsj")))

(define-public crate-typle-0.2.2 (c (n "typle") (v "0.2.2") (d (list (d (n "macrotest") (r "^1") (d #t) (k 2)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1lby2hqpz462fv8mrmzjmyph2cld00ih2yj959d4m8xzz5iywp7v")))

(define-public crate-typle-0.2.3 (c (n "typle") (v "0.2.3") (d (list (d (n "macrotest") (r "^1") (d #t) (k 2)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1i1n191b40lrzr4js96crg1r4530i0hd51hn2br260x1lw6mkqwz")))

(define-public crate-typle-0.2.4 (c (n "typle") (v "0.2.4") (d (list (d (n "macrotest") (r "^1") (d #t) (k 2)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1qnyh8yrjd05x3capy2wbbc691b423ic5fwqfbmzd04bnsb8xydc") (r "1.65")))

(define-public crate-typle-0.2.5 (c (n "typle") (v "0.2.5") (d (list (d (n "macrotest") (r "^1") (d #t) (k 2)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "04mjccw1y8d9mwjina21ri9afkk7d6d2k5yj7awyf74y9pncapsl") (r "1.65")))

(define-public crate-typle-0.3.0 (c (n "typle") (v "0.3.0") (d (list (d (n "macrotest") (r "^1") (d #t) (k 2)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0pz58qfi441d522wkrz16pg47d9cqnrinzf6kjxwqimmlzz3353l") (r "1.65")))

(define-public crate-typle-0.4.0 (c (n "typle") (v "0.4.0") (d (list (d (n "macrotest") (r "^1") (d #t) (k 2)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0wxfnrxlc47gw7rcpr579020m72g0gsmiraqcrn1pn4vf6k1l60n") (r "1.65")))

(define-public crate-typle-0.4.1 (c (n "typle") (v "0.4.1") (d (list (d (n "macrotest") (r "^1") (d #t) (k 2)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1fsj5mwbgcrmxr0jqs50a7w539nprmf736n2cq0ggy7gjklfiirs") (r "1.65")))

(define-public crate-typle-0.5.0 (c (n "typle") (v "0.5.0") (d (list (d (n "macrotest") (r "^1") (d #t) (k 2)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1w20c8668fkm77jpl30szlq70aypslnj992yd24qpvan5ddw1pya") (r "1.65")))

(define-public crate-typle-0.5.1 (c (n "typle") (v "0.5.1") (d (list (d (n "macrotest") (r "^1") (d #t) (k 2)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0n886wrf3fcx2ispqq1ybjib8vvk9z454mb6h3nffd6r9g7i8b95") (r "1.65")))

(define-public crate-typle-0.5.2 (c (n "typle") (v "0.5.2") (d (list (d (n "macrotest") (r "^1") (d #t) (k 2)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0b0psd2pjjzfgby29ihdv5nr4niizf33in01kyk7s68wfj2s62va") (r "1.65")))

(define-public crate-typle-0.6.0 (c (n "typle") (v "0.6.0") (d (list (d (n "macrotest") (r "^1") (d #t) (k 2)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1yk3vzwpvbaga0jmas4w4srwkpjgvlhm9lxi71jay39fxnzdlzg7") (r "1.65")))

(define-public crate-typle-0.6.1 (c (n "typle") (v "0.6.1") (d (list (d (n "macrotest") (r "^1") (d #t) (k 2)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1v48qkyvrhpvsbicg42h914g80ihs701mbkspg5rgcnbpnwg1p7z") (r "1.65")))

(define-public crate-typle-0.7.0 (c (n "typle") (v "0.7.0") (d (list (d (n "macrotest") (r "^1") (d #t) (k 2)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "05wz6dcfw4sfaajqi6gk092rp1j5838vjbby22v6mcj4b38gh9dj") (r "1.65")))

(define-public crate-typle-0.7.1 (c (n "typle") (v "0.7.1") (d (list (d (n "macrotest") (r "^1") (d #t) (k 2)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0g5z4i7ra0jkddvr9mcg5ihx1s39hv3z4wljg0slml0gc20kmwrr") (r "1.65")))

(define-public crate-typle-0.7.2 (c (n "typle") (v "0.7.2") (d (list (d (n "macrotest") (r "^1") (d #t) (k 2)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0s2pykkfm2ikbfmnmpyyqzqgv4ncl3ml9dsy1rsxmigj90789hfm") (r "1.65")))

(define-public crate-typle-0.7.3 (c (n "typle") (v "0.7.3") (d (list (d (n "macrotest") (r "^1") (d #t) (k 2)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.43") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "10j8q0j5qgy6iyv9jkx5ygyisjas8144ba0x35kmm0vw4klw4d2s") (r "1.65")))

(define-public crate-typle-0.7.4 (c (n "typle") (v "0.7.4") (d (list (d (n "macrotest") (r "^1") (d #t) (k 2)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.43") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "122kr40jc50c8hvn1gp73nj2iwy2pjqsm8wi93x8bw5hahxv6i9a") (r "1.65")))

(define-public crate-typle-0.7.5 (c (n "typle") (v "0.7.5") (d (list (d (n "macrotest") (r "^1") (d #t) (k 2)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.43") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0mrjdj0dxf6v4i3q99k48d4n7zf335hk2v0qzkvmb6mz9yxpj2xp") (r "1.65")))

(define-public crate-typle-0.7.6 (c (n "typle") (v "0.7.6") (d (list (d (n "macrotest") (r "^1") (d #t) (k 2)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.43") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "17zvqgc1jbx27c8q8an9i37wl5pr19qxlyddh6a60nln36zqwxbz") (r "1.65")))

(define-public crate-typle-0.7.7 (c (n "typle") (v "0.7.7") (d (list (d (n "macrotest") (r "^1") (d #t) (k 2)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.43") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "101s49hdgnh90c8nv0hj1dgk0x7ig8pzmli4p5bf71m4vfz2c6sm") (r "1.65")))

(define-public crate-typle-0.7.8 (c (n "typle") (v "0.7.8") (d (list (d (n "macrotest") (r "^1") (d #t) (k 2)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.43") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0c6a0f9zz5c8gi7dah2ihy88vxd00p9ka4xmk8hq2indyz3fin6x") (r "1.65")))

(define-public crate-typle-0.7.9 (c (n "typle") (v "0.7.9") (d (list (d (n "macrotest") (r "^1") (d #t) (k 2)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.43") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "097l0m8r1834b1yk3vsads3z4yqa8ixq0ss09c2l2pqb400qxxfs") (r "1.65")))

(define-public crate-typle-0.7.10 (c (n "typle") (v "0.7.10") (d (list (d (n "macrotest") (r "^1") (d #t) (k 2)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.43") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0bchdnx4fmsn0z6804l1jps8va1kl1yx7s6b8xfpxfmwfi8z2v5m") (r "1.65")))

(define-public crate-typle-0.7.11 (c (n "typle") (v "0.7.11") (d (list (d (n "macrotest") (r "^1") (d #t) (k 2)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.43") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1fz7680bl1lnpwv1fbfdqs4k7bmkns7kv5v62rjjb5hscaixp6h8") (r "1.65")))

(define-public crate-typle-0.7.12 (c (n "typle") (v "0.7.12") (d (list (d (n "macrotest") (r "^1") (d #t) (k 2)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.43") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0m4zr6504wr35cv8704xyvz3ywmd7cxgv1h0r31l2nii1kdck4k3") (r "1.65")))

(define-public crate-typle-0.8.0 (c (n "typle") (v "0.8.0") (d (list (d (n "macrotest") (r "^1") (d #t) (k 2)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.43") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1aa44dzjnpvyhkhnnp66mxa7fl22qsm51flmvfvd3mqcb31zci0w") (r "1.65")))

(define-public crate-typle-0.8.1 (c (n "typle") (v "0.8.1") (d (list (d (n "macrotest") (r "^1") (d #t) (k 2)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.43") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "14adp1wv3h1imkix33s2vnx70yhnjiidiw62n0mdbhyg0nxdw96z") (r "1.65")))

(define-public crate-typle-0.8.2 (c (n "typle") (v "0.8.2") (d (list (d (n "macrotest") (r "^1") (d #t) (k 2)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.43") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "06s73s09l99jybsfj59x3k28v4k1bim4a4dc9ms5w1ijqsai9grg") (r "1.65")))

(define-public crate-typle-0.8.3 (c (n "typle") (v "0.8.3") (d (list (d (n "macrotest") (r "^1") (d #t) (k 2)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.43") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "11qsc7rjwnl4kzfy999z5qcm61mk328n7a53gm4nrdabn5bshgw3") (r "1.65")))

(define-public crate-typle-0.8.4 (c (n "typle") (v "0.8.4") (d (list (d (n "macrotest") (r "^1") (d #t) (k 2)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.43") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0jdg5311w89kkpbr70jl34g87xwcjb5pas1x7ly07g7941r4g7aq") (r "1.65")))

(define-public crate-typle-0.8.5 (c (n "typle") (v "0.8.5") (d (list (d (n "macrotest") (r "^1") (d #t) (k 2)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.43") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0a0q821p9xw2pwnq24n5n9akrphxi3x3qz6a7cqmg9yn1bnh9css") (r "1.65")))

(define-public crate-typle-0.9.0 (c (n "typle") (v "0.9.0") (d (list (d (n "macrotest") (r "^1") (d #t) (k 2)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.43") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0m0qlmrdxp4ra50dbsj6j4rc68hm3vdradbdx50g6kby0sb782cv") (r "1.65")))

(define-public crate-typle-0.9.1 (c (n "typle") (v "0.9.1") (d (list (d (n "macrotest") (r "^1") (d #t) (k 2)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.43") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0p3yaang4i7vhqq53nppm5z2v5p825rqvimi8k0r4rjmrxpkjjix") (r "1.65")))

(define-public crate-typle-0.9.2 (c (n "typle") (v "0.9.2") (d (list (d (n "macrotest") (r "^1") (d #t) (k 2)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.43") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1nymkg66fi0m0dbn0pkdp96fm4flz9bjz743wap4awak9q8zycmj") (r "1.65")))

(define-public crate-typle-0.9.3 (c (n "typle") (v "0.9.3") (d (list (d (n "macrotest") (r "^1") (d #t) (k 2)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.43") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0i1njzzk9g9xalvcg96zrwpbzjsfy2qnwld9d88n5h9jdnxc73x9") (r "1.65")))

(define-public crate-typle-0.9.4 (c (n "typle") (v "0.9.4") (d (list (d (n "macrotest") (r "^1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.43") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "02zn2y5xbih3ldzwjdgs8hp5z4wfmgp3r02nivrk7jkd6vi00blx") (r "1.65")))

(define-public crate-typle-0.9.5 (c (n "typle") (v "0.9.5") (d (list (d (n "macrotest") (r "^1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.43") (f (quote ("full"))) (d #t) (k 0)))) (h "0f6kr65xfazprp8ml07sdg47piy3i8c8w8w3va9hb6sbfndqrcpg") (r "1.65")))

(define-public crate-typle-0.9.6 (c (n "typle") (v "0.9.6") (d (list (d (n "macrotest") (r "^1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.43") (f (quote ("full"))) (d #t) (k 0)))) (h "1qw5g81ps0lwgmw8gwn19yyfaqzd1xd6hslfzrrfmd74mx5ysb9b") (r "1.65")))

(define-public crate-typle-0.9.7 (c (n "typle") (v "0.9.7") (d (list (d (n "macrotest") (r "^1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.43") (f (quote ("full"))) (d #t) (k 0)))) (h "0110ja58dyjnvgjrl20ld4c1qahbh1rww563izqhl59yfs5vb1yc") (r "1.65")))

(define-public crate-typle-0.9.8 (c (n "typle") (v "0.9.8") (d (list (d (n "macrotest") (r "^1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.43") (f (quote ("full"))) (d #t) (k 0)))) (h "0qg2jcwma2a74f3vsy7q0677bv9sbkyf4yq4rx0d7c6a7zxs114q") (r "1.65")))

(define-public crate-typle-0.9.9 (c (n "typle") (v "0.9.9") (d (list (d (n "macrotest") (r "^1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.43") (f (quote ("full"))) (d #t) (k 0)))) (h "1l3vlwajqcyg26mjbh6cjki0vrwb4iysd279gnl3yaakhmksm08g") (r "1.65")))

(define-public crate-typle-0.9.10 (c (n "typle") (v "0.9.10") (d (list (d (n "macrotest") (r "^1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.43") (f (quote ("full"))) (d #t) (k 0)))) (h "16wzwiifjv1s2bwczfvm0pdas79brc0h9sphw8lvqq3yidvw7zwl") (r "1.65")))

(define-public crate-typle-0.9.11 (c (n "typle") (v "0.9.11") (d (list (d (n "macrotest") (r "^1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.43") (f (quote ("full" "visit-mut" "extra-traits"))) (d #t) (k 0)))) (h "161f2hrmw39gr28ivl11377xz10ab0s3hzzqygbgprfs3rb0szf7") (r "1.65")))

(define-public crate-typle-0.9.12 (c (n "typle") (v "0.9.12") (d (list (d (n "macrotest") (r "^1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.43") (f (quote ("full"))) (d #t) (k 0)))) (h "1y4nm3fvh42msmxl8fcai9fr12r8l7b5s1mvmck450gklh95dpki") (r "1.65")))

(define-public crate-typle-0.9.13 (c (n "typle") (v "0.9.13") (d (list (d (n "macrotest") (r "^1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.43") (f (quote ("full"))) (d #t) (k 0)))) (h "1yr7hipk379dnfqy92z3942ydnnhm05k2gafdzvhp5a1xq60zdbn") (y #t) (r "1.65")))

(define-public crate-typle-0.10.0 (c (n "typle") (v "0.10.0") (d (list (d (n "macrotest") (r "^1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.43") (f (quote ("full"))) (d #t) (k 0)))) (h "0339daimfhgb2mw8lymlnk1d01dabfiaf3nm0rbgipbnrc176jdh") (y #t) (r "1.65")))

(define-public crate-typle-0.10.1 (c (n "typle") (v "0.10.1") (d (list (d (n "macrotest") (r "^1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.43") (f (quote ("full"))) (d #t) (k 0)))) (h "02cz51irwq9ychz3bbpvc0320ywjif1q408c8gqc6h6gpfvdv1ai") (r "1.65")))

(define-public crate-typle-0.9.14 (c (n "typle") (v "0.9.14") (d (list (d (n "macrotest") (r "^1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.43") (f (quote ("full"))) (d #t) (k 0)))) (h "0npi5dxk7jxirpxiny09izmnjblpkxf8g49qgxp1rm00qvi878g0") (r "1.65")))

(define-public crate-typle-0.10.2 (c (n "typle") (v "0.10.2") (d (list (d (n "macrotest") (r "^1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.43") (f (quote ("full"))) (d #t) (k 0)) (d (n "zip_clone") (r "^0.1.0") (d #t) (k 0)))) (h "1mmkvzk5xbipz329wk58yrl9w4jn03zqn1sqswm92dsn1m39l9l1") (r "1.65")))

(define-public crate-typle-0.10.3 (c (n "typle") (v "0.10.3") (d (list (d (n "macrotest") (r "^1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.43") (f (quote ("full"))) (d #t) (k 0)) (d (n "zip_clone") (r "^0.1.0") (d #t) (k 0)))) (h "1q4k09771syj8fckmsrdb4sx6y5dvfvp2qn4drjzkrivg4wj20px") (r "1.65")))

