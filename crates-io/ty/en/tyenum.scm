(define-module (crates-io ty en tyenum) #:use-module (crates-io))

(define-public crate-tyenum-0.1.0 (c (n "tyenum") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4.27") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "syn") (r "^0.15.30") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "00mzgbd7av09lw5ahbgakiclh4jjrxg7xm71jh1v30jkqdv1q1v8")))

(define-public crate-tyenum-0.2.0 (c (n "tyenum") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^0.4.27") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "syn") (r "^0.15.31") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "00w16n1rwsgp6kaa6bxi862ls1cv5dqwws7g4glgy70w7d9c9ix2")))

(define-public crate-tyenum-0.2.1 (c (n "tyenum") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^0.4.27") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "syn") (r "^0.15.31") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "09gxf4v9a1dwzc9qcz284m1csc247d7v47f4sis7m9scy2qkmm9a")))

(define-public crate-tyenum-0.5.0 (c (n "tyenum") (v "0.5.0") (d (list (d (n "tyenum_attribute") (r "^0.5.0") (d #t) (k 0)))) (h "12ids4b0qz02sqgfdw6ilrkcmwkf42np5ws7qgy63vlgvnq61b8j")))

