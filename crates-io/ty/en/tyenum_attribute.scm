(define-module (crates-io ty en tyenum_attribute) #:use-module (crates-io))

(define-public crate-tyenum_attribute-0.5.0 (c (n "tyenum_attribute") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^0.4.27") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "syn") (r "^0.15.31") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "127cv3i7fzaz3wgqlqc9a8nlqr52fqba7x22kdr2i09p3vr85vz7")))

