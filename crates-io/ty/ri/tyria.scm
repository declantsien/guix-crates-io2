(define-module (crates-io ty ri tyria) #:use-module (crates-io))

(define-public crate-tyria-0.0.1 (c (n "tyria") (v "0.0.1") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "hyper") (r "^0.11") (d #t) (k 0)) (d (n "reqwest") (r "^0.6.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0kakiw6ancsxlrlzgalrcl5pl91pzymk4dxqs16nizv3lfff8mx4")))

