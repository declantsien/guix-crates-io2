(define-module (crates-io ty di tydi) #:use-module (crates-io))

(define-public crate-tydi-0.0.0 (c (n "tydi") (v "0.0.0") (h "1g60jjm43dk4f5p597rx9y72az9nbk77c2fwwd4rsam5vwcw013f")))

(define-public crate-tydi-0.0.1 (c (n "tydi") (v "0.0.1") (d (list (d (n "indexmap") (r "^1.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^5") (o #t) (d #t) (k 0)) (d (n "structopt") (r "^0.3") (o #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "039anmah96886gxd04blqgpfjm5ihjqy96ynp56rir0i37jcgmg0") (f (quote (("parser" "nom") ("generator") ("default" "generator" "parser") ("cli" "structopt" "parser"))))))

(define-public crate-tydi-0.0.2 (c (n "tydi") (v "0.0.2") (d (list (d (n "indexmap") (r "^1.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^5") (o #t) (d #t) (k 0)) (d (n "structopt") (r "^0.3") (o #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1b06v9azpm3zjr1hyzbxza58assdzjpg6aqcjrh72jz2543h2zpv") (f (quote (("parser" "nom") ("generator") ("default" "generator" "parser") ("cli" "structopt" "parser"))))))

(define-public crate-tydi-0.0.3 (c (n "tydi") (v "0.0.3") (d (list (d (n "indexmap") (r "^1.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^5") (o #t) (d #t) (k 0)) (d (n "structopt") (r "^0.3") (o #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "06vi3s9zda89vx5xjcwz130c6qadk7xinbhjphvfhxnqhp1yc8jv") (f (quote (("parser" "nom") ("generator") ("default" "generator" "parser") ("cli" "structopt" "parser"))))))

(define-public crate-tydi-0.0.4 (c (n "tydi") (v "0.0.4") (d (list (d (n "indexmap") (r "^1.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^5") (o #t) (d #t) (k 0)) (d (n "structopt") (r "^0.3") (o #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1wm33396xnvfhddh8xj77b34nmka9ldk3ynzd84yjx10fklnjkvi") (f (quote (("parser" "nom") ("generator") ("default" "generator" "parser") ("cli" "structopt" "parser"))))))

(define-public crate-tydi-0.0.5 (c (n "tydi") (v "0.0.5") (d (list (d (n "indexmap") (r "^1.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^5") (o #t) (d #t) (k 0)) (d (n "structopt") (r "^0.3") (o #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1dsbaw3l06v57c7x9x3hk037bf4v5008qj69br41bk5lfip81ix4") (f (quote (("parser" "nom") ("generator") ("default" "generator" "parser") ("cli" "structopt" "parser"))))))

(define-public crate-tydi-0.0.6 (c (n "tydi") (v "0.0.6") (d (list (d (n "indexmap") (r "^1.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^5") (o #t) (d #t) (k 0)) (d (n "structopt") (r "^0.3") (o #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1h2cx7j62b45q6683a32l6khm81wpv9n9nix70q27885arskp7k4") (f (quote (("parser" "nom") ("generator") ("default" "generator" "parser") ("cli" "structopt" "parser"))))))

(define-public crate-tydi-0.0.7 (c (n "tydi") (v "0.0.7") (d (list (d (n "indexmap") (r "^1.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^5") (o #t) (d #t) (k 0)) (d (n "structopt") (r "^0.3") (o #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1bw0xkzvsqnwblwxrxfzzfcr06xcjib9sxmy7dpdb05ig43zf1g3") (f (quote (("parser" "nom") ("generator") ("default" "generator" "parser") ("cli" "structopt" "parser"))))))

