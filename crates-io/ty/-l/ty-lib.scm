(define-module (crates-io ty -l ty-lib) #:use-module (crates-io))

(define-public crate-ty-lib-0.1.0 (c (n "ty-lib") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1fi7a4qfnp3bz29r6ib3f2id7qkbszix0qqcv729slg5jjcw695q")))

(define-public crate-ty-lib-0.2.0 (c (n "ty-lib") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "validator") (r "^0.12") (f (quote ("derive"))) (d #t) (k 0)))) (h "1ny0c7p5mdhrabzv3k3gizy4jfn0dirg926ywsfmpm87x517nwl5")))

