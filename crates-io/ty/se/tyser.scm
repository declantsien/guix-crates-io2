(define-module (crates-io ty se tyser) #:use-module (crates-io))

(define-public crate-tyser-0.1.0 (c (n "tyser") (v "0.1.0") (d (list (d (n "md-tangle-engine") (r "^0.1") (d #t) (k 1)) (d (n "org-tangle-engine") (r "^0.1") (d #t) (k 1)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1s19xcfx64inlvhv3bi7mf0gmc2cyh4266dvbbvyw06jia3dr5ij") (y #t)))

