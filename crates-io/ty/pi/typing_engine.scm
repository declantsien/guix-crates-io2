(define-module (crates-io ty pi typing_engine) #:use-module (crates-io))

(define-public crate-typing_engine-0.1.0 (c (n "typing_engine") (v "0.1.0") (d (list (d (n "once_cell") (r "^1.13.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.143") (f (quote ("derive"))) (d #t) (k 0)))) (h "1wlin0f1cx9613y4y0vv99a6phhni6q9l68i6hi0sx530laxbnc9")))

(define-public crate-typing_engine-0.1.1 (c (n "typing_engine") (v "0.1.1") (d (list (d (n "once_cell") (r "^1.13.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.143") (f (quote ("derive"))) (d #t) (k 0)))) (h "0dbab3p91qdmlrp89ra78g7hk7qvpvqz73dramn47fqb6qgvm68f")))

