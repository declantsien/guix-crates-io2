(define-module (crates-io ty pi typify_gostruct) #:use-module (crates-io))

(define-public crate-typify_gostruct-1.0.0 (c (n "typify_gostruct") (v "1.0.0") (h "0lm06mxga85s8ixn9qfd9c10pv2582j6qg27arl5iwj4cph12cbl")))

(define-public crate-typify_gostruct-1.0.1 (c (n "typify_gostruct") (v "1.0.1") (h "0hb3pij206kxyljwhrs11bf5hq6fimanyrhw0dg1hyfyvpkdx4jr")))

(define-public crate-typify_gostruct-1.0.5 (c (n "typify_gostruct") (v "1.0.5") (h "06dhp5zqc0w7kl2q3jrnwzrgq36slhpbcmr8lsrba29vagdax8gh")))

