(define-module (crates-io ty pi typist-rust) #:use-module (crates-io))

(define-public crate-typist-rust-0.2.0 (c (n "typist-rust") (v "0.2.0") (d (list (d (n "generic-matrix") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "0l0073x3rbnpdnljzap4d12jggp2cl4fxx3f11195xzzn1kgpk8z")))

