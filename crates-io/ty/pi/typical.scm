(define-module (crates-io ty pi typical) #:use-module (crates-io))

(define-public crate-typical-0.0.0 (c (n "typical") (v "0.0.0") (h "1217nwl5pmi7inhlhfi6pnwr6i7v7aln97al6rs0kq78abcdhn3r")))

(define-public crate-typical-0.0.1 (c (n "typical") (v "0.0.1") (d (list (d (n "clap") (r "^2") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "colored") (r "^1") (d #t) (k 0)) (d (n "pad") (r "^0.1") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.6") (d #t) (k 0)))) (h "0i2vmm0g0lxy6jfdfs9cq58xq7km8n5iirvsm2bxqz2r0r2bf2vz")))

(define-public crate-typical-0.0.2 (c (n "typical") (v "0.0.2") (d (list (d (n "clap") (r "^2") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "colored") (r "^1") (d #t) (k 0)) (d (n "pad") (r "^0.1") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.6") (d #t) (k 0)))) (h "1mbyb2hignr7dqsqm2a2llvwxp0p1s6rhmqy7a9dvp8lfjrh3n2d")))

(define-public crate-typical-0.0.3 (c (n "typical") (v "0.0.3") (d (list (d (n "clap") (r "^2") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "colored") (r "^1") (d #t) (k 0)) (d (n "pad") (r "^0.1") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.6") (d #t) (k 0)))) (h "1c17qfhmah8gpc6ilvhskniyqai68wzwp3ibdnfcfpn85f98q4a3")))

(define-public crate-typical-0.0.4 (c (n "typical") (v "0.0.4") (d (list (d (n "clap") (r "^2") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "colored") (r "^1") (d #t) (k 0)) (d (n "pad") (r "^0.1") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.6") (d #t) (k 0)))) (h "143z2cs5dk0hmcgizb211hbakscyqs9wf9xmavlqz9y1xf62fdy3")))

(define-public crate-typical-0.0.5 (c (n "typical") (v "0.0.5") (d (list (d (n "clap") (r "^2") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "colored") (r "^1") (d #t) (k 0)) (d (n "pad") (r "^0.1") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.6") (d #t) (k 0)))) (h "0x19nyhqqjcriwkmkb3id3c06yinc299gl7707zb7c6lxa225f6g")))

(define-public crate-typical-0.0.6 (c (n "typical") (v "0.0.6") (d (list (d (n "clap") (r "^2") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "colored") (r "^1") (d #t) (k 0)) (d (n "pad") (r "^0.1") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.6") (d #t) (k 0)))) (h "1wd209sq73yhd9pcr24x99yhbl1ycg1lqshmxlicqzgr3bx36bdr")))

(define-public crate-typical-0.0.7 (c (n "typical") (v "0.0.7") (d (list (d (n "clap") (r "^2") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "colored") (r "^1") (d #t) (k 0)) (d (n "pad") (r "^0.1") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.6") (d #t) (k 0)))) (h "0xxgdznnzxaf7myrq0vfy1m5pvphjcrg3w9ksr0frmzfhlykqvqp")))

(define-public crate-typical-0.1.0 (c (n "typical") (v "0.1.0") (d (list (d (n "clap") (r "^2") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "colored") (r "^1") (d #t) (k 0)) (d (n "pad") (r "^0.1") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.6") (d #t) (k 0)))) (h "0ph3d4n6d57x229jard5wbj0cwa6z5zrbi1w47zpfyy7f16bkxnn")))

(define-public crate-typical-0.1.1 (c (n "typical") (v "0.1.1") (d (list (d (n "clap") (r "^2") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "colored") (r "^1") (d #t) (k 0)) (d (n "pad") (r "^0.1") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.6") (d #t) (k 0)))) (h "0b0ni3dvvfnkm86pxd3xvf5dvc65hrqvhsb39my6rbv7wc288pmq")))

(define-public crate-typical-0.1.2 (c (n "typical") (v "0.1.2") (d (list (d (n "clap") (r "^2") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "colored") (r "^1") (d #t) (k 0)) (d (n "pad") (r "^0.1") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.6") (d #t) (k 0)))) (h "1flvv3bg61llwhhqr7kwg1lfzf698c1p27z1w9p2x7f05bkw13v6")))

(define-public crate-typical-0.2.0 (c (n "typical") (v "0.2.0") (d (list (d (n "clap") (r "^2") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "colored") (r "^1") (d #t) (k 0)) (d (n "pad") (r "^0.1") (d #t) (k 0)) (d (n "textwrap") (r "^0.14") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1") (d #t) (k 0)))) (h "1m09430bli78aa7ir0f39zdqigb2mz7kfys26wi9lfbzbdhxadr7")))

(define-public crate-typical-0.2.1 (c (n "typical") (v "0.2.1") (d (list (d (n "clap") (r "^2") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "colored") (r "^1") (d #t) (k 0)) (d (n "pad") (r "^0.1") (d #t) (k 0)) (d (n "textwrap") (r "^0.14") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1") (d #t) (k 0)))) (h "1722rwy78h1q3726lhsl03zilxy6j29fz66xryh9011f44f2zqmc")))

(define-public crate-typical-0.2.2 (c (n "typical") (v "0.2.2") (d (list (d (n "clap") (r "^2") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "colored") (r "^1") (d #t) (k 0)) (d (n "pad") (r "^0.1") (d #t) (k 0)) (d (n "textwrap") (r "^0.14") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1") (d #t) (k 0)))) (h "0s2857bk3gva0kxw3f96fbjvx2n9nk9kya9d6rdxy4sl2bx4ad62")))

(define-public crate-typical-0.3.0 (c (n "typical") (v "0.3.0") (d (list (d (n "clap") (r "^2") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "colored") (r "^1") (d #t) (k 0)) (d (n "pad") (r "^0.1") (d #t) (k 0)) (d (n "textwrap") (r "^0.14") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1") (d #t) (k 0)))) (h "1p8xw6pmi8qni8zbbf7whi67zky9h08x11whz3j7kgmxkrpvp21m")))

(define-public crate-typical-0.4.0 (c (n "typical") (v "0.4.0") (d (list (d (n "clap") (r "^2") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "colored") (r "^1") (d #t) (k 0)) (d (n "pad") (r "^0.1") (d #t) (k 0)) (d (n "textwrap") (r "^0.14") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1") (d #t) (k 0)))) (h "1ywjaivj02kmzrgj0m69qxngdwan8lnxfyxyw96nip4ydl7hvh1c")))

(define-public crate-typical-0.5.0 (c (n "typical") (v "0.5.0") (d (list (d (n "clap") (r "^2") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "colored") (r "^1") (d #t) (k 0)) (d (n "pad") (r "^0.1") (d #t) (k 0)) (d (n "textwrap") (r "^0.14") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1") (d #t) (k 0)))) (h "1ni80pmsmf388hc5gm9z1xsk5hyhr4phpgc5dkryv6m5gfl051l6")))

(define-public crate-typical-0.6.0 (c (n "typical") (v "0.6.0") (d (list (d (n "clap") (r "^2") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "colored") (r "^1") (d #t) (k 0)) (d (n "pad") (r "^0.1") (d #t) (k 0)) (d (n "textwrap") (r "^0.14") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1") (d #t) (k 0)))) (h "070g051pff4ld51gb7gr6h4bqq0q8apy2yg6aqla42ikqpksx615")))

(define-public crate-typical-0.7.0 (c (n "typical") (v "0.7.0") (d (list (d (n "clap") (r "^2") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "colored") (r "^1") (d #t) (k 0)) (d (n "pad") (r "^0.1") (d #t) (k 0)) (d (n "textwrap") (r "^0.14") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1") (d #t) (k 0)))) (h "0y26jnzy68d6mih7vd6j57kp8cgm8j1x29dnnd9a57nj3cnngwfa")))

(define-public crate-typical-0.7.1 (c (n "typical") (v "0.7.1") (d (list (d (n "clap") (r "^2") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "colored") (r "^1") (d #t) (k 0)) (d (n "pad") (r "^0.1") (d #t) (k 0)) (d (n "textwrap") (r "^0.14") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1") (d #t) (k 0)))) (h "1qmckdpdjvhbg7zlmr2vzji1xh1gfa8likbyca4fw1pl7i49zvki")))

(define-public crate-typical-0.7.2 (c (n "typical") (v "0.7.2") (d (list (d (n "clap") (r "^2") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "colored") (r "^1") (d #t) (k 0)) (d (n "pad") (r "^0.1") (d #t) (k 0)) (d (n "textwrap") (r "^0.14") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1") (d #t) (k 0)))) (h "0xsndiz0iwr6ay113p3wx7gmxqwp767msgc4slddbcvn6sav9b98")))

(define-public crate-typical-0.8.0 (c (n "typical") (v "0.8.0") (d (list (d (n "clap") (r "^2") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "colored") (r "^1") (d #t) (k 0)) (d (n "pad") (r "^0.1") (d #t) (k 0)) (d (n "textwrap") (r "^0.14") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1") (d #t) (k 0)))) (h "0hcvn5f12pg1qlp457aarhpla22zki0a3kqibi5p6916y4amyaws")))

(define-public crate-typical-0.8.1 (c (n "typical") (v "0.8.1") (d (list (d (n "clap") (r "^2") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "colored") (r "^1") (d #t) (k 0)) (d (n "pad") (r "^0.1") (d #t) (k 0)) (d (n "textwrap") (r "^0.14") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1") (d #t) (k 0)))) (h "1cah57723adhmd8akz58xhnv4gyqg53milahhn909ns6xhq58c9y")))

(define-public crate-typical-0.8.2 (c (n "typical") (v "0.8.2") (d (list (d (n "clap") (r "^2") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "colored") (r "^1") (d #t) (k 0)) (d (n "pad") (r "^0.1") (d #t) (k 0)) (d (n "textwrap") (r "^0.14") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1") (d #t) (k 0)))) (h "04vbvh5s9r91gs3g06j0p3fqf6h4ylbbhx2qlmpfi65jx9d151s6")))

(define-public crate-typical-0.8.3 (c (n "typical") (v "0.8.3") (d (list (d (n "clap") (r "^2") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "colored") (r "^1") (d #t) (k 0)) (d (n "pad") (r "^0.1") (d #t) (k 0)) (d (n "textwrap") (r "^0.14") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1") (d #t) (k 0)))) (h "0ai42zklkpk0c1aq7lxpxmf7za3d0bsy1b915ni8f9pd2f8qhcz3")))

(define-public crate-typical-0.8.4 (c (n "typical") (v "0.8.4") (d (list (d (n "clap") (r "^2") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "colored") (r "^1") (d #t) (k 0)) (d (n "pad") (r "^0.1") (d #t) (k 0)) (d (n "textwrap") (r "^0.14") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1") (d #t) (k 0)))) (h "090a7905g9ix4idyk26fg5yakkaqrwvyl2by6sxx681lhc76fgwd")))

(define-public crate-typical-0.8.5 (c (n "typical") (v "0.8.5") (d (list (d (n "clap") (r "^2") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "colored") (r "^1") (d #t) (k 0)) (d (n "pad") (r "^0.1") (d #t) (k 0)) (d (n "textwrap") (r "^0.14") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1") (d #t) (k 0)))) (h "0p1am8h1yb1yp1d5k4v0gxwwglkjrid3bjbf6qnymzgckqag305h")))

(define-public crate-typical-0.8.6 (c (n "typical") (v "0.8.6") (d (list (d (n "clap") (r "^2") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "colored") (r "^1") (d #t) (k 0)) (d (n "pad") (r "^0.1") (d #t) (k 0)) (d (n "textwrap") (r "^0.14") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1") (d #t) (k 0)))) (h "16s1yll8xzn6hiqg47r06f5nv5jkcsshbrmsb77gv7fsyaf1k06j")))

(define-public crate-typical-0.8.7 (c (n "typical") (v "0.8.7") (d (list (d (n "clap") (r "^2") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "colored") (r "^1") (d #t) (k 0)) (d (n "pad") (r "^0.1") (d #t) (k 0)) (d (n "textwrap") (r "^0.14") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1") (d #t) (k 0)))) (h "1krygk19f7ikx08hqg7pbhicc5g1ckhwgw4b4m7a4ssbcm1pkan2")))

(define-public crate-typical-0.9.0 (c (n "typical") (v "0.9.0") (d (list (d (n "clap") (r "^2") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "colored") (r "^1") (d #t) (k 0)) (d (n "pad") (r "^0.1") (d #t) (k 0)) (d (n "textwrap") (r "^0.14") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1") (d #t) (k 0)))) (h "0jpvxhjd2x68fha458mc8j1mw1cdkzkphd3s1czlbf67w5373ild")))

(define-public crate-typical-0.9.1 (c (n "typical") (v "0.9.1") (d (list (d (n "clap") (r "^2") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "colored") (r "^1") (d #t) (k 0)) (d (n "pad") (r "^0.1") (d #t) (k 0)) (d (n "textwrap") (r "^0.14") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1") (d #t) (k 0)))) (h "0idm864gfd751wq8003wppk20iylkzvpimnybrnxr1lnrbqpm4bl")))

(define-public crate-typical-0.9.2 (c (n "typical") (v "0.9.2") (d (list (d (n "clap") (r "^2") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "colored") (r "^1") (d #t) (k 0)) (d (n "pad") (r "^0.1") (d #t) (k 0)) (d (n "textwrap") (r "^0.14") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1") (d #t) (k 0)))) (h "00ba77mqjgbw050pj8crg5602z8ml2f1p14pqz0z55zja0wy4b13")))

(define-public crate-typical-0.9.3 (c (n "typical") (v "0.9.3") (d (list (d (n "clap") (r "^2") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "colored") (r "^1") (d #t) (k 0)) (d (n "pad") (r "^0.1") (d #t) (k 0)) (d (n "textwrap") (r "^0.14") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1") (d #t) (k 0)))) (h "12yqh27avwn9w3py6yfabzg7x1z1ki1dsj0bqgqkq82mlrbmwbfc")))

(define-public crate-typical-0.9.4 (c (n "typical") (v "0.9.4") (d (list (d (n "clap") (r "^2") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "colored") (r "^1") (d #t) (k 0)) (d (n "pad") (r "^0.1") (d #t) (k 0)) (d (n "textwrap") (r "^0.14") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1") (d #t) (k 0)))) (h "1di7dy1r4aqqdjf06905sjcsmxz6w46cw95vgjb5cj7hzvigbdvb")))

(define-public crate-typical-0.9.5 (c (n "typical") (v "0.9.5") (d (list (d (n "clap") (r "^2") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "colored") (r "^1") (d #t) (k 0)) (d (n "pad") (r "^0.1") (d #t) (k 0)) (d (n "textwrap") (r "^0.14") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1") (d #t) (k 0)))) (h "1pyxfg26d7k9kkjya0j2i43axf5xjpv9f8dyqay76bfm5wpgb6zq")))

(define-public crate-typical-0.9.6 (c (n "typical") (v "0.9.6") (d (list (d (n "clap") (r "^2") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "colored") (r "^1") (d #t) (k 0)) (d (n "pad") (r "^0.1") (d #t) (k 0)) (d (n "textwrap") (r "^0.14") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1") (d #t) (k 0)))) (h "0gspcrrm8gf36mn3x9ph9x8fvk34crh16vnqbfvknmxxc9lml0v9")))

(define-public crate-typical-0.9.7 (c (n "typical") (v "0.9.7") (d (list (d (n "clap") (r "^2") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "colored") (r "^1") (d #t) (k 0)) (d (n "pad") (r "^0.1") (d #t) (k 0)) (d (n "textwrap") (r "^0.14") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1") (d #t) (k 0)))) (h "0j70vcyx6a2vq9qrpcjzzpran1llmy4i1kdifdrswzryy8lsnaph")))

