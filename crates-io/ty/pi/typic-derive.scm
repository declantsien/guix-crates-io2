(define-module (crates-io ty pi typic-derive) #:use-module (crates-io))

(define-public crate-typic-derive-0.1.0 (c (n "typic-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "18bnvz1h7s5wv19lvqpk9niq89gx87w2aws3wwzji6fgr3jy9m7i")))

(define-public crate-typic-derive-0.2.0 (c (n "typic-derive") (v "0.2.0") (d (list (d (n "if_chain") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits" "visit"))) (d #t) (k 0)))) (h "0wcbv23riqlb3zg3xx4r0jkj027y8wr2yv4g44rqvfjabl5jk6c9")))

(define-public crate-typic-derive-0.2.1 (c (n "typic-derive") (v "0.2.1") (d (list (d (n "if_chain") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits" "visit"))) (d #t) (k 0)))) (h "1d7l7g7h1vln8wjmcfnd21jzhxs136sc6miv5im5x3k7dfk9frk3")))

(define-public crate-typic-derive-0.2.2 (c (n "typic-derive") (v "0.2.2") (d (list (d (n "if_chain") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits" "visit"))) (d #t) (k 0)))) (h "040gj9hr2qf1cv1sbagrih0jmlmanmafkimzjxhx8abc46ach9fv")))

(define-public crate-typic-derive-0.3.0 (c (n "typic-derive") (v "0.3.0") (d (list (d (n "if_chain") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits" "visit"))) (d #t) (k 0)))) (h "01b49hhr11s24qrmz0s3gagv3aa4qs82rf5df1pma27mqrkc014f")))

