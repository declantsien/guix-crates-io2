(define-module (crates-io ty pi typing-reader) #:use-module (crates-io))

(define-public crate-typing-reader-0.1.0 (c (n "typing-reader") (v "0.1.0") (d (list (d (n "clap") (r "^3.2.17") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)) (d (n "stable-eyre") (r "^0.2.2") (d #t) (k 0)) (d (n "tui") (r "^0.19.0") (d #t) (k 0)))) (h "0pddshfaic1v6m316c742x7bzdzzlr3alzpdwmbgcrw20zf0nk4p")))

(define-public crate-typing-reader-0.2.0 (c (n "typing-reader") (v "0.2.0") (d (list (d (n "clap") (r "^3.2.17") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)) (d (n "stable-eyre") (r "^0.2.2") (d #t) (k 0)) (d (n "tui") (r "^0.19.0") (d #t) (k 0)))) (h "114l5ljp82b93gpxy1k0zpfhij354pggfkhc96rrqfq6563a3yi9")))

