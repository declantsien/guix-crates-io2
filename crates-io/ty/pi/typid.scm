(define-module (crates-io ty pi typid) #:use-module (crates-io))

(define-public crate-typid-1.0.0 (c (n "typid") (v "1.0.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "0gk3a34wsf76ia895kz4jksadmh1dkrd9fqi1a5ngjr4z3yc42iy")))

(define-public crate-typid-1.0.1 (c (n "typid") (v "1.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "0lz63m3qfv935pnpj1qh41xsgsg2cfx070azlyw7ic3vqx19v80d") (f (quote (("web" "uuid/wasm-bindgen"))))))

(define-public crate-typid-1.1.0 (c (n "typid") (v "1.1.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "1p1wr5cgdnblp1cy2xpp53bd0823lddk9gv075m3sn9ryw8g2kl7") (f (quote (("web" "uuid/wasm-bindgen"))))))

(define-public crate-typid-1.1.1 (c (n "typid") (v "1.1.1") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "1697f0dcml703kynmzp47n9s167563r4d82hpl2297sl51pvakcr") (f (quote (("web" "uuid/wasm-bindgen"))))))

