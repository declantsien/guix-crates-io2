(define-module (crates-io ty mb tymbark-gui) #:use-module (crates-io))

(define-public crate-tymbark-gui-0.1.0 (c (n "tymbark-gui") (v "0.1.0") (d (list (d (n "clap") (r "^3.1.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clipboard-win") (r "^4.4.1") (d #t) (k 0)) (d (n "eframe") (r "^0.17.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("winuser"))) (d #t) (k 0)))) (h "0pc6ck6mpin06vrxxjgqw4rzrfnvsvsczz58igr52dngyhmdk2pv")))

(define-public crate-tymbark-gui-0.1.1 (c (n "tymbark-gui") (v "0.1.1") (d (list (d (n "clap") (r "^3.1.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clipboard-win") (r "^4.4.1") (d #t) (k 0)) (d (n "eframe") (r "^0.17.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("winuser"))) (d #t) (k 0)))) (h "1c40kfsjdmpm0yq0clcwvp4pqyn2h7059yq8jlrdv81v005ik4j4")))

