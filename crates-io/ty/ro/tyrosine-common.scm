(define-module (crates-io ty ro tyrosine-common) #:use-module (crates-io))

(define-public crate-tyrosine-common-0.0.1 (c (n "tyrosine-common") (v "0.0.1") (d (list (d (n "ordered-float") (r "^4.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "sorted-vec") (r "^0.8.3") (d #t) (k 0)))) (h "0jzwc1yr73kbisxx8n4lasfa1f4h8ndldiw4ajhvf7ivb69y5vff")))

