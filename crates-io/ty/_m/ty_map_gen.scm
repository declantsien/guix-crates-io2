(define-module (crates-io ty _m ty_map_gen) #:use-module (crates-io))

(define-public crate-ty_map_gen-0.1.0 (c (n "ty_map_gen") (v "0.1.0") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "dyn-clone") (r "^1.0.17") (d #t) (k 0)) (d (n "dyn-hash") (r "^0.2.0") (d #t) (k 0)) (d (n "erased-serde") (r "^0.4.5") (o #t) (d #t) (k 0)) (d (n "paste") (r "^1.0.15") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 2)))) (h "156r8zhqyhbxm72dwj0xnhjf1k27cljyn8f0bniyq5lcd8zijwxa") (y #t)))

(define-public crate-ty_map_gen-0.1.1 (c (n "ty_map_gen") (v "0.1.1") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "dyn-clone") (r "^1.0.17") (d #t) (k 0)) (d (n "dyn-hash") (r "^0.2.0") (d #t) (k 0)) (d (n "erased-serde") (r "^0.4.5") (o #t) (d #t) (k 0)) (d (n "paste") (r "^1.0.15") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 2)))) (h "00xz593v3nw7qcn97v9jbyqbhpdy3amnyv2qpnib34cyyhfxf5ah") (y #t)))

(define-public crate-ty_map_gen-0.1.2 (c (n "ty_map_gen") (v "0.1.2") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "dyn-clone") (r "^1.0.17") (d #t) (k 0)) (d (n "dyn-hash") (r "^0.2.0") (d #t) (k 0)) (d (n "erased-serde") (r "^0.4.5") (o #t) (d #t) (k 0)) (d (n "paste") (r "^1.0.15") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 2)))) (h "0n4zc3h2wc24g4yl59vgbbm4ybs9g853p0l3ba7gx79hnw4dc5aq") (y #t)))

(define-public crate-ty_map_gen-0.1.3 (c (n "ty_map_gen") (v "0.1.3") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "dyn-clone") (r "^1.0.17") (d #t) (k 0)) (d (n "dyn-hash") (r "^0.2.0") (d #t) (k 0)) (d (n "erased-serde") (r "^0.4.5") (o #t) (d #t) (k 0)) (d (n "paste") (r "^1.0.15") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 2)))) (h "131jdr74mv9hr2pyaq67b3jaijgwhp6z2iha5xsacrh5xifdy8lj") (y #t)))

(define-public crate-ty_map_gen-0.1.4 (c (n "ty_map_gen") (v "0.1.4") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "dyn-clone") (r "^1.0.17") (d #t) (k 0)) (d (n "dyn-hash") (r "^0.2.0") (d #t) (k 0)) (d (n "erased-serde") (r "^0.4.5") (o #t) (d #t) (k 0)) (d (n "paste") (r "^1.0.15") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 2)))) (h "1h5aby813krjp64zj388p9hwafc44xis3qrrqn8c5s9ck4kh2afd")))

(define-public crate-ty_map_gen-0.1.5 (c (n "ty_map_gen") (v "0.1.5") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "dyn-clone") (r "^1.0.17") (d #t) (k 0)) (d (n "dyn-hash") (r "^0.2.0") (d #t) (k 0)) (d (n "erased-serde") (r "^0.4.5") (o #t) (d #t) (k 0)) (d (n "paste") (r "^1.0.15") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 2)))) (h "0ngv3syn3clm1zcfkg056hyw5db61049slz52ba1nnhdgbd6s3a5")))

(define-public crate-ty_map_gen-0.1.6 (c (n "ty_map_gen") (v "0.1.6") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "dyn-clone") (r "^1.0.17") (d #t) (k 0)) (d (n "dyn-hash") (r "^0.2.0") (d #t) (k 0)) (d (n "erased-serde") (r "^0.4.5") (o #t) (d #t) (k 0)) (d (n "paste") (r "^1.0.15") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 2)))) (h "0ajrhc761hbfznqigw37zj05f224f7l04g5rn2my2bld8rn8kfi5")))

