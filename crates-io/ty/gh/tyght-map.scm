(define-module (crates-io ty gh tyght-map) #:use-module (crates-io))

(define-public crate-tyght-map-0.1.0-alpha.0 (c (n "tyght-map") (v "0.1.0-alpha.0") (d (list (d (n "static_assertions") (r "^1.1.0") (d #t) (k 2)))) (h "0rvpll3iv29ypg7hry9va9706grw8akg9biz0bjv8j1xrib2dfh5")))

(define-public crate-tyght-map-0.1.0-alpha.1 (c (n "tyght-map") (v "0.1.0-alpha.1") (d (list (d (n "static_assertions") (r "^1.1.0") (d #t) (k 2)))) (h "1fmn6x5igz4cz33y5la34gwndc3kcmp7chwh7k4pg9fixyh7faxa")))

(define-public crate-tyght-map-0.1.0-alpha.2 (c (n "tyght-map") (v "0.1.0-alpha.2") (d (list (d (n "static_assertions") (r "^1.1.0") (d #t) (k 2)))) (h "0dhc8g50bnr5i4wvm8gfpbg1nvwnh7djm63a5fi3j3ndyfvmsrgv")))

(define-public crate-tyght-map-0.1.0-alpha.3 (c (n "tyght-map") (v "0.1.0-alpha.3") (d (list (d (n "static_assertions") (r "^1.1.0") (d #t) (k 2)))) (h "1w6jwibp3ppwnwbgv4k29nzcmsbjwd8gmsjz94mx23pfmzlx9ifd")))

(define-public crate-tyght-map-0.1.0-alpha.4 (c (n "tyght-map") (v "0.1.0-alpha.4") (d (list (d (n "static_assertions") (r "^1.1.0") (d #t) (k 2)))) (h "1vw8pnp14r68cbhrqp4zj34ibdl6wm6mmf8np9z6vmq389prv84h") (f (quote (("size-32" "size-16") ("size-16") ("defaults" "size-32"))))))

(define-public crate-tyght-map-0.1.0 (c (n "tyght-map") (v "0.1.0") (d (list (d (n "static_assertions") (r "^1.1.0") (d #t) (k 2)))) (h "11kjkm9mjq8al9wkpng4nf04zpam43bkzzdwg1dr9bsl3cb9bgay") (f (quote (("size-32" "size-16") ("size-16") ("default" "size-32"))))))

(define-public crate-tyght-map-0.1.1 (c (n "tyght-map") (v "0.1.1") (d (list (d (n "static_assertions") (r "^1.1.0") (d #t) (k 2)))) (h "0dcm9q0qihqqbk3gf5wl7wf6d4n04lq4s5mkyw9qb8hks4sdx6is") (f (quote (("size-32" "size-16") ("size-16") ("default" "size-32"))))))

(define-public crate-tyght-map-0.2.0 (c (n "tyght-map") (v "0.2.0") (d (list (d (n "static_assertions") (r "^1.1.0") (d #t) (k 2)))) (h "0866x98gg90f70a9q1ki0jdsla64y3ajkafh23f62faqm3mdk27c")))

