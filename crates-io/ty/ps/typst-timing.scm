(define-module (crates-io ty ps typst-timing) #:use-module (crates-io))

(define-public crate-typst-timing-0.0.1 (c (n "typst-timing") (v "0.0.1") (h "1xinc2v0rcmz1s02f9l4981xykjrnmiqvl5zsi05flgjg83dhrpc")))

(define-public crate-typst-timing-0.11.0-rc1 (c (n "typst-timing") (v "0.11.0-rc1") (d (list (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.184") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "typst-syntax") (r "^0.11.0-rc1") (d #t) (k 0)))) (h "0rr5190q5r69mbl2wxaqmxgcfqfi2v7f07j7l0apvjxyhfpbcixs") (r "1.74")))

(define-public crate-typst-timing-0.11.0 (c (n "typst-timing") (v "0.11.0") (d (list (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.184") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "typst-syntax") (r "^0.11.0") (d #t) (k 0)))) (h "1n2zgl0sbw14fgn5vk2ndj8cn1mh7g490yv28ff2jvyy7j9jj9kb") (r "1.74")))

(define-public crate-typst-timing-0.11.1 (c (n "typst-timing") (v "0.11.1") (d (list (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.184") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "typst-syntax") (r "^0.11.1") (d #t) (k 0)))) (h "0rzfmxzkan0sa2n89qwwjjr0iw3hrvpd7rmckaiv5b5wj9qy2n2v") (r "1.74")))

