(define-module (crates-io ty ps typst-ide) #:use-module (crates-io))

(define-public crate-typst-ide-0.1.0 (c (n "typst-ide") (v "0.1.0") (h "1zcfrgwgqm99jj6plgyzbil9sadql903hg7gkcxqljrfdkbb9m2j")))

(define-public crate-typst-ide-0.11.0-rc1 (c (n "typst-ide") (v "0.11.0-rc1") (d (list (d (n "comemo") (r "^0.4") (d #t) (k 0)) (d (n "ecow") (r "^0.2") (f (quote ("serde"))) (d #t) (k 0)) (d (n "if_chain") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.184") (f (quote ("derive"))) (d #t) (k 0)) (d (n "typst") (r "^0.11.0-rc1") (d #t) (k 0)) (d (n "unscanny") (r "^0.1") (d #t) (k 0)))) (h "0cnl2qlkry7m1zpq8lhn8qxyw59r8rng0gyrmqllmxg9q9x5hl00") (r "1.74")))

(define-public crate-typst-ide-0.11.0 (c (n "typst-ide") (v "0.11.0") (d (list (d (n "comemo") (r "^0.4") (d #t) (k 0)) (d (n "ecow") (r "^0.2") (f (quote ("serde"))) (d #t) (k 0)) (d (n "if_chain") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.184") (f (quote ("derive"))) (d #t) (k 0)) (d (n "typst") (r "^0.11.0") (d #t) (k 0)) (d (n "unscanny") (r "^0.1") (d #t) (k 0)))) (h "0pmycrbr3kj5mq7y3176b4yv14l3ans2gi7hkc7c43g0j56nbdfw") (r "1.74")))

(define-public crate-typst-ide-0.11.1 (c (n "typst-ide") (v "0.11.1") (d (list (d (n "comemo") (r "^0.4") (d #t) (k 0)) (d (n "ecow") (r "^0.2") (f (quote ("serde"))) (d #t) (k 0)) (d (n "if_chain") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1.0.184") (f (quote ("derive"))) (d #t) (k 0)) (d (n "typst") (r "^0.11.1") (d #t) (k 0)) (d (n "typst-assets") (r "^0.11.1") (d #t) (k 2)) (d (n "unscanny") (r "^0.1") (d #t) (k 0)))) (h "05l0apwvvxjjf76fg9rzyiwq8w4gibbshrm1fm7icklj0dqff89r") (r "1.74")))

