(define-module (crates-io ty ps typsy) #:use-module (crates-io))

(define-public crate-typsy-0.1.0 (c (n "typsy") (v "0.1.0") (d (list (d (n "macros") (r "^0.1") (o #t) (d #t) (k 0) (p "typsy-macros")))) (h "1hh0vbzlsdj3vynfkm3km2qnzad49zcn8m2hnlcg7p4zljgqj6jr") (f (quote (("nightly") ("extreme_tuples" "bigger_tuples") ("default" "macros" "alloc") ("bigger_tuples") ("alloc"))))))

