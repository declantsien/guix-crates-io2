(define-module (crates-io ty ps typsy-macros) #:use-module (crates-io))

(define-public crate-typsy-macros-0.1.0 (c (n "typsy-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1x5iasp1pvychjf6a5zgs1zlzxcigasfb50bg97sja58xn4an5s0")))

