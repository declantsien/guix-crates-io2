(define-module (crates-io ty ps typst-assets) #:use-module (crates-io))

(define-public crate-typst-assets-0.0.1 (c (n "typst-assets") (v "0.0.1") (h "1i584236iixi26dswkd8x95j0s4v6jw7v9kvx1b6bvmvvy27q4ym")))

(define-public crate-typst-assets-0.11.0-rc1 (c (n "typst-assets") (v "0.11.0-rc1") (h "1bh4qw0xrj1h9dizjw558mj0fsayprxmjpkkwclw6sbr3pn9hfz4") (f (quote (("fonts"))))))

(define-public crate-typst-assets-0.11.0 (c (n "typst-assets") (v "0.11.0") (h "0v1cbfzkjk4j70lz6lzbzp8vd9fzfbrgmznpgn259ni80cv8agzi") (f (quote (("fonts"))))))

(define-public crate-typst-assets-0.11.1 (c (n "typst-assets") (v "0.11.1") (h "172dxzw8nvps72p9mi43568v8p25812v56hw933yxs38sbw62c1b") (f (quote (("fonts"))))))

