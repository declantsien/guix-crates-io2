(define-module (crates-io ty ps typst-ansi-hl) #:use-module (crates-io))

(define-public crate-typst-ansi-hl-0.1.0 (c (n "typst-ansi-hl") (v "0.1.0") (d (list (d (n "ansi_colours") (r "^1.2.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "syntect") (r "^5.2.0") (f (quote ("parsing" "regex-fancy"))) (k 0)) (d (n "termcolor") (r "^1.4.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)) (d (n "two-face") (r "^0.3.0") (f (quote ("syntect-fancy"))) (k 0)) (d (n "typst-syntax") (r "^0.11.0") (d #t) (k 0)))) (h "1xf27isfpnrl607rvvg75r60ad4bsh7zq4jzkbmms55bzlgplafx")))

