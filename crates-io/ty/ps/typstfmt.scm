(define-module (crates-io ty ps typstfmt) #:use-module (crates-io))

(define-public crate-typstfmt-0.1.0 (c (n "typstfmt") (v "0.1.0") (d (list (d (n "globmatch") (r "^0.2.3") (d #t) (k 0)) (d (n "insta") (r "^1.30.0") (d #t) (k 2)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "lexopt") (r "^0.3.0") (d #t) (k 0)) (d (n "paste") (r "^1.0.13") (d #t) (k 2)) (d (n "regex") (r "^1.7.3") (d #t) (k 0)) (d (n "similar-asserts") (r "^1.4.2") (d #t) (k 0)) (d (n "toml") (r "^0.7.3") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.17") (d #t) (k 2)) (d (n "unicode-segmentation") (r "^1.10.1") (d #t) (k 0)))) (h "1mpv2mxir48gvw0g6448sf9yxjbshdj932jdqqgd3812kmbdifbf") (r "1.56")))

