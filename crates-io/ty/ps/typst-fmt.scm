(define-module (crates-io ty ps typst-fmt) #:use-module (crates-io))

(define-public crate-typst-fmt-0.1.0 (c (n "typst-fmt") (v "0.1.0") (d (list (d (n "regex") (r "^1.7.3") (d #t) (k 0)) (d (n "toml") (r "^0.7.3") (d #t) (k 0)) (d (n "typst") (r "^0.1.0") (d #t) (k 0)))) (h "0hnwb8hrsl70didcrv3zz34fv07ypicff5d5kyby894q4pnxk00h") (r "1.56")))

