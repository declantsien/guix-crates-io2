(define-module (crates-io ty ps typst-macros) #:use-module (crates-io))

(define-public crate-typst-macros-0.1.0 (c (n "typst-macros") (v "0.1.0") (h "11zvs42a9b3s84470dbapwsnvzi4jm4nk6q5w345yxaij0aq94wk")))

(define-public crate-typst-macros-0.11.0-rc1 (c (n "typst-macros") (v "0.11.0-rc1") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0m9j0qygqgqaxz2cqxbmzfy55crjjh4k7rv8kkgws0czpidyikqp") (r "1.74")))

(define-public crate-typst-macros-0.11.0 (c (n "typst-macros") (v "0.11.0") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "19wvi9dpsy504rlir31y4iysqgg4dbpslq2rb478mx5bdpfqzr2l") (r "1.74")))

(define-public crate-typst-macros-0.11.1 (c (n "typst-macros") (v "0.11.1") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1z33hci300cvllg6i2j9qf5pwwwc8fwfa5a2iq7hp4ml8vyzv875") (r "1.74")))

