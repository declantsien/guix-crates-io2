(define-module (crates-io ty ps typst-ts-pdf-exporter) #:use-module (crates-io))

(define-public crate-typst-ts-pdf-exporter-0.2.1 (c (n "typst-ts-pdf-exporter") (v "0.2.1") (d (list (d (n "typst") (r "^0.4.0") (d #t) (k 0)) (d (n "typst-ts-core") (r "^0.2") (d #t) (k 0)))) (h "0b4hx75x02ljgjaf9fqpmrd50flr8pz9r9ydj714qpn3hhgrjy0s")))

(define-public crate-typst-ts-pdf-exporter-0.2.2 (c (n "typst-ts-pdf-exporter") (v "0.2.2") (d (list (d (n "typst") (r "^0.5.0") (d #t) (k 0)) (d (n "typst-ts-core") (r "^0.2") (d #t) (k 0)))) (h "1paanlqhk6lxwbjya2q4irsyplj58k6cwxxksxn0pw34grywjyal")))

