(define-module (crates-io ty ps typst-ts-ast-exporter) #:use-module (crates-io))

(define-public crate-typst-ts-ast-exporter-0.2.1 (c (n "typst-ts-ast-exporter") (v "0.2.1") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "typst") (r "^0.4.0") (d #t) (k 0)) (d (n "typst-ts-core") (r "^0.2") (d #t) (k 0)))) (h "1c3y37q2l0yqnql209x7cprsmk9q371mpkklxh34q26x60zd94ds")))

(define-public crate-typst-ts-ast-exporter-0.2.2 (c (n "typst-ts-ast-exporter") (v "0.2.2") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "typst") (r "^0.5.0") (d #t) (k 0)) (d (n "typst-ts-core") (r "^0.2") (d #t) (k 0)))) (h "0ngpa03g99kgxwz6ni4ks6xn56d5fgg2j1sxpnybvy44mcva7mv9")))

