(define-module (crates-io ty pe typesafe-builders) #:use-module (crates-io))

(define-public crate-typesafe-builders-0.1.1 (c (n "typesafe-builders") (v "0.1.1") (d (list (d (n "typesafe-builders-core") (r "^0.1.1") (d #t) (k 0)) (d (n "typesafe-builders-derive") (r "^0.1.1") (d #t) (k 0)))) (h "133kq8wdppbkbpq3smz39cpwrfi0k6qcwxfsm8r9xnq515pk9yww")))

(define-public crate-typesafe-builders-0.1.2 (c (n "typesafe-builders") (v "0.1.2") (d (list (d (n "typesafe-builders-core") (r "^0.1.2") (d #t) (k 0)) (d (n "typesafe-builders-derive") (r "^0.1.2") (d #t) (k 0)))) (h "188a1hfwjn7pnrl6r4drfq8qc6y2whf2ps2dgz71g8q05qcr6zx7")))

(define-public crate-typesafe-builders-0.1.3 (c (n "typesafe-builders") (v "0.1.3") (d (list (d (n "typesafe-builders-core") (r "^0.1.3") (d #t) (k 0)) (d (n "typesafe-builders-derive") (r "^0.1.3") (d #t) (k 0)))) (h "1wi5kqc40njsjc326jymi6qhb2g4qm3i0y7dgzvqn4cqn7i86b2c")))

(define-public crate-typesafe-builders-0.2.1 (c (n "typesafe-builders") (v "0.2.1") (d (list (d (n "typesafe-builders-core") (r "^0.2.1") (d #t) (k 0)) (d (n "typesafe-builders-derive") (r "^0.2.1") (d #t) (k 0)))) (h "0qf3vz7v715p6i713ab55v161kmk8i1kjfnr0c7h63mdjwx11m1m")))

(define-public crate-typesafe-builders-0.3.0 (c (n "typesafe-builders") (v "0.3.0") (d (list (d (n "trybuild") (r "^1.0.80") (d #t) (k 2)) (d (n "typesafe-builders-core") (r "^0.3.0") (d #t) (k 0)) (d (n "typesafe-builders-derive") (r "^0.3.0") (d #t) (k 0)))) (h "1xiyg8hkh3kxhh4r18y3cwji0mh06mb58zl5r5gh1j7a8vpwpism")))

(define-public crate-typesafe-builders-0.4.0 (c (n "typesafe-builders") (v "0.4.0") (d (list (d (n "trybuild") (r "^1.0.80") (d #t) (k 2)) (d (n "typesafe-builders-core") (r "^0.4.0") (d #t) (k 0)) (d (n "typesafe-builders-derive") (r "^0.4.0") (d #t) (k 0)))) (h "1a5j6nykw5mjsshy3s0vdzj8411cp501igjkb0cplpy4bfc76zi0")))

(define-public crate-typesafe-builders-0.4.1 (c (n "typesafe-builders") (v "0.4.1") (d (list (d (n "trybuild") (r "^1.0.80") (d #t) (k 2)) (d (n "typesafe-builders-core") (r "^0.4.1") (d #t) (k 0)) (d (n "typesafe-builders-derive") (r "^0.4.1") (d #t) (k 0)))) (h "1w8jddlyrrkb79mhzx0m7rllwch8v6bc6wq84nwjj1csj6q6vg9v")))

(define-public crate-typesafe-builders-0.5.0 (c (n "typesafe-builders") (v "0.5.0") (d (list (d (n "trybuild") (r "^1.0.80") (d #t) (k 2)) (d (n "typesafe-builders-core") (r "^0.5.0") (d #t) (k 0)) (d (n "typesafe-builders-derive") (r "^0.5.0") (d #t) (k 0)))) (h "0fimlm2kp9j9vj6y4zyrccl0z2jzn5g2ry5xg39lrjkny0171ly9") (r "1.65.0")))

