(define-module (crates-io ty pe type-vec) #:use-module (crates-io))

(define-public crate-type-vec-0.1.0 (c (n "type-vec") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "typ") (r "^0.1") (d #t) (k 0)) (d (n "typenum") (r "^1.12") (d #t) (k 0)))) (h "0r4g7ndg76f7dp4b2qpkgzhhqb549dqfd3njb18hq0zv4a04dv3c")))

