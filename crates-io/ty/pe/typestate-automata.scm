(define-module (crates-io ty pe typestate-automata) #:use-module (crates-io))

(define-public crate-typestate-automata-0.2.0 (c (n "typestate-automata") (v "0.2.0") (h "162lxk53s2smib1cw32xnb5nshv7xj5r0xp1vpy1zi6s4iz4rjjg") (f (quote (("dot"))))))

(define-public crate-typestate-automata-0.3.0 (c (n "typestate-automata") (v "0.3.0") (h "0m6xwypfcnini3ypk744rj9yy4w3avd3h3xwfwls3qc4mh39cxx0") (f (quote (("dot"))))))

(define-public crate-typestate-automata-0.3.1 (c (n "typestate-automata") (v "0.3.1") (h "1ykbc1f7pdd2hab6z3m75hxdlj95cpf5px6906jymc596l9abmwa") (f (quote (("dot"))))))

(define-public crate-typestate-automata-0.3.2 (c (n "typestate-automata") (v "0.3.2") (h "08ssb8f177bv1mcn9wdzzvfc4l7mp69jcmv7n55syflrh9qhwnfk") (f (quote (("dot"))))))

(define-public crate-typestate-automata-0.4.1 (c (n "typestate-automata") (v "0.4.1") (h "17d3m39j9zbljrxqk7ymsc20s52clb4g8jig3j6a748zwjichjsk") (f (quote (("plantuml") ("dot"))))))

(define-public crate-typestate-automata-0.4.2 (c (n "typestate-automata") (v "0.4.2") (h "1nvhvmnclsxa4vdbrjvfgpk19dn055w6aiskl1bdnp0iqcsx6n5s") (f (quote (("plantuml") ("dot"))))))

(define-public crate-typestate-automata-0.4.3 (c (n "typestate-automata") (v "0.4.3") (h "05748xa6xh919zjgv084r8a2a2hpbfpzzdyxz5fc6wib1hwam9l7") (f (quote (("plantuml") ("dot"))))))

(define-public crate-typestate-automata-0.4.4 (c (n "typestate-automata") (v "0.4.4") (h "10nhqnlm9xndv7y9dmw15g11qq1dsn1crcm6jfrf2vjac965lv43") (f (quote (("plantuml") ("mermaid") ("dot"))))))

(define-public crate-typestate-automata-0.4.5 (c (n "typestate-automata") (v "0.4.5") (h "09118xclch47ljq2zbzckq87irm5p25wyiy7jjgxwk0bi113by7m") (f (quote (("plantuml") ("mermaid") ("dot"))))))

(define-public crate-typestate-automata-0.5.0 (c (n "typestate-automata") (v "0.5.0") (h "1m5d3jwr08s7i64qq3j6dpy8hgjsbx3mh098wh092q8kasaimcmb") (f (quote (("plantuml") ("mermaid") ("dot"))))))

