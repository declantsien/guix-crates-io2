(define-module (crates-io ty pe typed-urls) #:use-module (crates-io))

(define-public crate-typed-urls-0.1.0 (c (n "typed-urls") (v "0.1.0") (d (list (d (n "actix-web") (r "^3.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tera") (r "^1.5") (o #t) (d #t) (k 0)))) (h "1pkrpn0x826cv13fw8wi9n2paqh26nbkcc95q05yvpm4fqk5nvjj") (f (quote (("tera-templates" "tera") ("default") ("actix-routes" "actix-web"))))))

