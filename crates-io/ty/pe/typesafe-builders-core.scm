(define-module (crates-io ty pe typesafe-builders-core) #:use-module (crates-io))

(define-public crate-typesafe-builders-core-0.1.1 (c (n "typesafe-builders-core") (v "0.1.1") (h "1mmg6hqw747p0f68hijmvyg8q4i9slz457ag0vhk98d3lc7msgla")))

(define-public crate-typesafe-builders-core-0.1.2 (c (n "typesafe-builders-core") (v "0.1.2") (h "179pbb80wr5v883pcambr93v71xf52np89p7w3xzbk6i4sm1px51")))

(define-public crate-typesafe-builders-core-0.1.3 (c (n "typesafe-builders-core") (v "0.1.3") (h "1jwqdsf6xlvk9cx4hyr93qrkx16rqq9fcczn43lvhncq29v6cbpm")))

(define-public crate-typesafe-builders-core-0.2.1 (c (n "typesafe-builders-core") (v "0.2.1") (d (list (d (n "derive-syn-parse") (r "^0.1.5") (k 0)) (d (n "proc-macro2") (r "^1.0.56") (k 0)) (d (n "quote") (r "^1.0.26") (k 0)) (d (n "syn") (r "^2.0.13") (d #t) (k 0)))) (h "1rypczkjwgxnrqxb85j00avz4bd7yxgl40d8147jbkxyqb0vg7jc")))

(define-public crate-typesafe-builders-core-0.3.0 (c (n "typesafe-builders-core") (v "0.3.0") (d (list (d (n "derive-syn-parse") (r "^0.1.5") (k 0)) (d (n "proc-macro2") (r "^1.0.56") (k 0)) (d (n "quote") (r "^1.0.26") (k 0)) (d (n "syn") (r "^2.0.13") (d #t) (k 0)))) (h "0z8fc5v4dxnf08rplpnsviff5lchka6b22rkinncvs4xidj6y309")))

(define-public crate-typesafe-builders-core-0.4.0 (c (n "typesafe-builders-core") (v "0.4.0") (d (list (d (n "derive-syn-parse") (r "^0.1.5") (k 0)) (d (n "proc-macro2") (r "^1.0.56") (k 0)) (d (n "quote") (r "^1.0.26") (k 0)) (d (n "syn") (r "^2.0.13") (d #t) (k 0)))) (h "0ddcvjgk2gwx5d96cqf5x82arfgvq2wxc9fjz3514z4f0n6x4r3f")))

(define-public crate-typesafe-builders-core-0.4.1 (c (n "typesafe-builders-core") (v "0.4.1") (d (list (d (n "derive-syn-parse") (r "^0.1.5") (k 0)) (d (n "proc-macro2") (r "^1.0.56") (k 0)) (d (n "quote") (r "^1.0.26") (k 0)) (d (n "syn") (r "^2.0.15") (d #t) (k 0)) (d (n "typesafe-builders") (r "^0.4.0") (d #t) (k 2)))) (h "0p3yjlws8q54yi9vwwvrhgzjcw6llgl1dym12mf3yvxmhgkwpvwg")))

(define-public crate-typesafe-builders-core-0.5.0 (c (n "typesafe-builders-core") (v "0.5.0") (d (list (d (n "derive-syn-parse") (r "^0.1.5") (k 0)) (d (n "proc-macro2") (r "^1.0.56") (k 0)) (d (n "quote") (r "^1.0.26") (k 0)) (d (n "syn") (r "^2.0.15") (d #t) (k 0)))) (h "1d14x0nj1byxf64mzzwzgm9a0pfa2zw45kmjdvjp9sjffdayyjvq") (r "1.65.0")))

