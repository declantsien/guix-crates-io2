(define-module (crates-io ty pe type-nats) #:use-module (crates-io))

(define-public crate-type-nats-0.0.1 (c (n "type-nats") (v "0.0.1") (h "1g5anjccawnl640pfhkk11h1a2paysag8aapbi2b652z3s0nf28r")))

(define-public crate-type-nats-0.0.2 (c (n "type-nats") (v "0.0.2") (h "175dz3drs8k3fka2zwwwizccrfmz0n5538ww7b12mgghpcn05iww")))

