(define-module (crates-io ty pe type_hash_macros) #:use-module (crates-io))

(define-public crate-type_hash_macros-0.1.0 (c (n "type_hash_macros") (v "0.1.0") (d (list (d (n "either") (r "^1.6") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 2)) (d (n "type_hash_core") (r "=0.1.0") (d #t) (k 0)))) (h "019grjb1a3rq25rc9kzrn90wr1xlp5ibj5lvgld963rpm7yss2gy")))

(define-public crate-type_hash_macros-0.2.0 (c (n "type_hash_macros") (v "0.2.0") (d (list (d (n "either") (r "^1.6") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 2)))) (h "1wmw8llxaqznmxn1pg0amjs9syjirxrxl7ir2d3v5f15i31lhh5k")))

(define-public crate-type_hash_macros-0.2.1 (c (n "type_hash_macros") (v "0.2.1") (d (list (d (n "either") (r "^1.6") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 2)))) (h "1p4scs5kcjqnj1g4yv74lfz8751l6dpnix4c8rgqk8mzrlmr1v3l")))

(define-public crate-type_hash_macros-0.2.2 (c (n "type_hash_macros") (v "0.2.2") (d (list (d (n "either") (r "^1.6") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 2)))) (h "0f7p0z89g9rk0qa81d6v6si7ykaapd9cms0p0ajrfr138skbw3ns")))

(define-public crate-type_hash_macros-0.3.0 (c (n "type_hash_macros") (v "0.3.0") (d (list (d (n "either") (r "^1.6") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 2)))) (h "07n4vfqrpyzc5jvkr3smz8h97z8gm1x9z65khzq3wj3nw1jc2vvl")))

