(define-module (crates-io ty pe typeql_lang) #:use-module (crates-io))

(define-public crate-typeql_lang-2.24.0 (c (n "typeql_lang") (v "2.24.0") (d (list (d (n "chrono") (r "0.4.23") (k 0)) (d (n "itertools") (r "0.10.3") (k 0)) (d (n "pest") (r "2.4.0") (k 0)) (d (n "pest_derive") (r "2.4.0") (k 0)) (d (n "regex") (r "1.6.0") (k 0)))) (h "00j99n1q009cfmvapppq02jk3gfy60zf9b3ih64m2w6qsldxqrsw") (y #t)))

