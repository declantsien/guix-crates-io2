(define-module (crates-io ty pe typesafe_repository_macro) #:use-module (crates-io))

(define-public crate-typesafe_repository_macro-0.1.0 (c (n "typesafe_repository_macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "02gscyhsys4fvxpzgkvcd04nh88gm4fzcca9w17n2pv5mkhp9i61")))

(define-public crate-typesafe_repository_macro-0.1.1 (c (n "typesafe_repository_macro") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "typesafe_repository") (r "^0.1") (d #t) (k 2)))) (h "19iha2yg4krs8iikjlbs47bmp765740c77ml6nqbjw3kl9bynnm8")))

(define-public crate-typesafe_repository_macro-0.3.0 (c (n "typesafe_repository_macro") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)) (d (n "typesafe_repository") (r "^0.3.0") (d #t) (k 2)))) (h "176vklm8dmnp8r564s2sq53zf7bpdxz3yjp5dh4xh2p9ch3zl13z")))

