(define-module (crates-io ty pe typesafe-derive-builder) #:use-module (crates-io))

(define-public crate-typesafe-derive-builder-0.1.0 (c (n "typesafe-derive-builder") (v "0.1.0") (d (list (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "rand") (r "^0.3.18") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "0ih3ijqf3av4izs9hvqqnl3zv42cckz4ppqic5cid81hsb2d79sn")))

