(define-module (crates-io ty pe typed-qb) #:use-module (crates-io))

(define-public crate-typed-qb-0.1.0 (c (n "typed-qb") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "concat-idents") (r "^1.1.3") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mysql") (r "^20") (d #t) (k 0)) (d (n "typed-qb-procmacro") (r "^0.1") (d #t) (k 0)))) (h "1j4v0c665qwy2k4p3f53x7343bhgwrml7wk6d57ypdix2gkngpyd")))

(define-public crate-typed-qb-0.1.1 (c (n "typed-qb") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "concat-idents") (r "^1.1.3") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mysql") (r "^20") (d #t) (k 0)) (d (n "typed-qb-procmacro") (r "^0.1") (d #t) (k 0)))) (h "11270mdjch2y4cwjvy5vmpb3bknp2ss9ps0d8gnrfy3mnbx5ghy4")))

(define-public crate-typed-qb-0.2.0 (c (n "typed-qb") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "concat-idents") (r "^1.1.3") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mysql") (r "^20") (d #t) (k 0)) (d (n "typed-qb-procmacro") (r "^0.2") (d #t) (k 0)))) (h "1g7lvy7wv4j5d8l3pkkqn40l417qp1hhrvcs7ldvdib31g0ks0jb")))

