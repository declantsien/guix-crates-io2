(define-module (crates-io ty pe type_name_value) #:use-module (crates-io))

(define-public crate-type_name_value-0.1.0 (c (n "type_name_value") (v "0.1.0") (d (list (d (n "derivative") (r "^2.1.1") (d #t) (k 0)))) (h "0h46jdnd8xymr2xmy80gj4al1s8r9gi8fybra1vhkcqdxxfib10b")))

(define-public crate-type_name_value-0.1.1 (c (n "type_name_value") (v "0.1.1") (d (list (d (n "derivative") (r "^2.1.1") (d #t) (k 0)))) (h "0a6zxpkbkyg517bibbbakgpmfnlz4d6jscb4knv1lvcw281w0w09")))

