(define-module (crates-io ty pe type-factory) #:use-module (crates-io))

(define-public crate-type-factory-0.1.0 (c (n "type-factory") (v "0.1.0") (h "0naygr9sxxjjcmmmfg4h5i3l4xdq4rq5qhpcb8hf0zjvr812n969") (y #t) (r "1.75")))

(define-public crate-type-factory-0.2.0 (c (n "type-factory") (v "0.2.0") (h "0x99krazjkzacbjdq4nz6pzwlbq364rb0dalkgvggdav65fps1nv") (y #t) (r "1.56")))

(define-public crate-type-factory-0.2.1 (c (n "type-factory") (v "0.2.1") (h "1xima26bpdjywvrq0ahdlb4gvq427dvxrgqc5g4figh91499alz3") (y #t) (r "1.56")))

(define-public crate-type-factory-0.1.1 (c (n "type-factory") (v "0.1.1") (h "0jbq706l8gf3f3jkfvfhwpcbicwjgy7ixvhi5ss7w49g60i3l1ji") (y #t) (r "1.75")))

(define-public crate-type-factory-0.1.2 (c (n "type-factory") (v "0.1.2") (h "1b8vcajlzcwki76lfg15cxw2lfg652sdv4mfn2lml4bkgfv42prx") (y #t) (r "1.75")))

(define-public crate-type-factory-0.1.3 (c (n "type-factory") (v "0.1.3") (h "0ad32jrshf91ni473yb2kd96r6sj9fh4n3vsc3a149slfjrxc5xs") (y #t) (r "1.75")))

(define-public crate-type-factory-0.3.0 (c (n "type-factory") (v "0.3.0") (h "18zg10vc96wfq8cd32iq501gsz1kr5jjdmvgq8y31d04nw3kjl9d") (r "1.56")))

(define-public crate-type-factory-0.3.1 (c (n "type-factory") (v "0.3.1") (h "1iv99ml0m4zw4glqwy8vbnz8ikrd6lfrbkaz6cldf0gvppjvcnym") (r "1.56")))

