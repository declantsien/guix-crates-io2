(define-module (crates-io ty pe typeenum) #:use-module (crates-io))

(define-public crate-typeenum-0.1.0 (c (n "typeenum") (v "0.1.0") (d (list (d (n "typeenum_derive") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "0k7wlnpr8k3f55fgmw650cn0x21c27hji9p4yakw2b3l16ia4r00") (f (quote (("default" "derive")))) (y #t) (s 2) (e (quote (("derive" "dep:typeenum_derive"))))))

(define-public crate-typeenum-0.1.1 (c (n "typeenum") (v "0.1.1") (d (list (d (n "typeenum_derive") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "18hgcrlvx9m86mqkw2dj6kwgjyh5qzmdby37zfhcx4rx499gmrrd") (f (quote (("default" "derive")))) (s 2) (e (quote (("derive" "dep:typeenum_derive"))))))

