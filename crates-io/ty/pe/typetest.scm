(define-module (crates-io ty pe typetest) #:use-module (crates-io))

(define-public crate-typetest-1.0.0 (c (n "typetest") (v "1.0.0") (d (list (d (n "iced") (r "^0.3") (f (quote ("glow" "smol"))) (k 0)) (d (n "iced_native") (r "^0.4") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "typetest_core") (r "^1.0") (d #t) (k 0)) (d (n "typetest_themes") (r "^1.0") (d #t) (k 0)))) (h "1z8wsvbv3578dhsz53dkhl4ij7c4dzrpghs5d555936krh5qlgz2") (y #t)))

(define-public crate-typetest-1.0.1 (c (n "typetest") (v "1.0.1") (d (list (d (n "iced") (r "^0.3") (f (quote ("glow" "smol"))) (k 0)) (d (n "iced_native") (r "^0.4") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "typetest_core") (r "^1.0") (d #t) (k 0)) (d (n "typetest_themes") (r "^1.0") (d #t) (k 0)))) (h "1v4h50c258s1vagndk9m1khr4ma8ic7r8azw0vjiwg17njh81ibi")))

(define-public crate-typetest-1.0.2 (c (n "typetest") (v "1.0.2") (d (list (d (n "iced") (r "^0.3") (f (quote ("glow" "smol"))) (k 0)) (d (n "iced_native") (r "^0.4") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "typetest_core") (r "^1.0") (d #t) (k 0)) (d (n "typetest_themes") (r "^1.0") (d #t) (k 0)))) (h "1ay198q5kpz68adv1n528k88l098v8w2ak4k0mlp9046jn4f86ak")))

