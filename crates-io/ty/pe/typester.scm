(define-module (crates-io ty pe typester) #:use-module (crates-io))

(define-public crate-typester-0.1.0 (c (n "typester") (v "0.1.0") (d (list (d (n "clap") (r "^4.0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0.101") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1s4cddfl8xk3cfcf28icvmimnp2pvf5fl5pphac6jlv6pgjbfzlj") (r "1.63")))

(define-public crate-typester-0.1.1 (c (n "typester") (v "0.1.1") (d (list (d (n "clap") (r "^4.0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0.101") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0x6pgq6rvnab1c5jz78h5ns2n1s3yv401bfph545m0yzfamv1lw4") (r "1.63")))

