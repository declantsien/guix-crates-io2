(define-module (crates-io ty pe typed_key) #:use-module (crates-io))

(define-public crate-typed_key-0.1.0 (c (n "typed_key") (v "0.1.0") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 2)) (d (n "serde") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "09visnwa683bhkwl3f54b411fb6fnipzqr6fvnn3hsgcynp6ksq0") (f (quote (("nightly"))))))

(define-public crate-typed_key-0.1.1 (c (n "typed_key") (v "0.1.1") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 2)) (d (n "serde") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "09zxc3rvqmpd0mfw70mhfvbf8dhpava3rzcblqqirdrwm9bs3lkz") (f (quote (("nightly"))))))

