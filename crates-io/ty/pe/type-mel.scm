(define-module (crates-io ty pe type-mel) #:use-module (crates-io))

(define-public crate-type-mel-0.6.0 (c (n "type-mel") (v "0.6.0") (d (list (d (n "melodium-core") (r "^0.6.0") (d #t) (k 0)) (d (n "melodium-macro") (r "^0.6.0") (d #t) (k 0)))) (h "1ipw07krm3angynrb89smrrzs0fyiid4siqaw32m3x4izms2bj1s") (r "1.60")))

(define-public crate-type-mel-0.7.0-rc1 (c (n "type-mel") (v "0.7.0-rc1") (d (list (d (n "melodium-core") (r "=0.7.0-rc1") (d #t) (k 0)) (d (n "melodium-macro") (r "=0.7.0-rc1") (d #t) (k 0)))) (h "0v2yy14qvd9yxz8jwy7sczhgpvw2cnw01hpv2rwgfsp74q80k1sm") (f (quote (("real") ("plugin") ("mock")))) (r "1.60")))

(define-public crate-type-mel-0.7.0 (c (n "type-mel") (v "0.7.0") (d (list (d (n "melodium-core") (r "=0.7.0") (d #t) (k 0)) (d (n "melodium-macro") (r "=0.7.0") (d #t) (k 0)))) (h "0mfp0zlrprqznzrlrh2lgswhsy45pa4q6684vhf3bc1gh44g4z78") (f (quote (("real") ("plugin") ("mock")))) (r "1.60")))

(define-public crate-type-mel-0.7.1 (c (n "type-mel") (v "0.7.1") (d (list (d (n "melodium-core") (r "^0.7.1") (d #t) (k 0)) (d (n "melodium-macro") (r "^0.7.1") (d #t) (k 0)))) (h "0cj0irkcarw7275g1g0skkgkbizyqgdri0a4wdwk16azqdzszwb6") (f (quote (("real") ("plugin") ("mock")))) (r "1.60")))

