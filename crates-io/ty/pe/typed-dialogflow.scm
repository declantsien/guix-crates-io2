(define-module (crates-io ty pe typed-dialogflow) #:use-module (crates-io))

(define-public crate-typed-dialogflow-0.1.0 (c (n "typed-dialogflow") (v "0.1.0") (d (list (d (n "gcp_auth") (r "^0.7") (d #t) (k 0)) (d (n "language-tags") (r "^0.3") (f (quote ("serde"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.17") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1p64pqf8c8k8vvh5h4k5rhhbkrbn2g757ligywz753mcd6yxwllz")))

