(define-module (crates-io ty pe typemap_core) #:use-module (crates-io))

(define-public crate-typemap_core-0.1.0 (c (n "typemap_core") (v "0.1.0") (d (list (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "trybuild") (r "^1.0.39") (d #t) (k 2)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "0c519i2xj39xlijvkvcbi3m1v47gj5a148zbvm9lnynyy934a14w")))

