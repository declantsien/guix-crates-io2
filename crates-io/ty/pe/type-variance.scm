(define-module (crates-io ty pe type-variance) #:use-module (crates-io))

(define-public crate-type-variance-0.0.1 (c (n "type-variance") (v "0.0.1") (h "0k1gj4jrcj5557agadxp2bcw6kim3h7019mx3r743bss4792c6bj")))

(define-public crate-type-variance-0.0.2 (c (n "type-variance") (v "0.0.2") (d (list (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0lx8dmlsj7zksnlhbnqffs6iql6clvhj5z0kjj7k3v598qlf3ghl")))

(define-public crate-type-variance-0.0.3 (c (n "type-variance") (v "0.0.3") (d (list (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0v16cr63gacjlp26n45wgzf1qdraxdd7jjnb9ls1x0x8ih1hj4k0")))

(define-public crate-type-variance-0.1.0 (c (n "type-variance") (v "0.1.0") (d (list (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1ilc4c6mbvfmj453vm13f5bhwy97par0ni6w4zh8zrdf571qwydf")))

