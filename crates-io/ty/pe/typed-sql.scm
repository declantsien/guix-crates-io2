(define-module (crates-io ty pe typed-sql) #:use-module (crates-io))

(define-public crate-typed-sql-0.1.0 (c (n "typed-sql") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.50") (d #t) (k 0)) (d (n "typed-sql-derive") (r "^0.1.0") (d #t) (k 0)))) (h "0hr5ldiagvfliiqkzjpa4k8rrrqq9ahnpkix2h6yz22d8q27piwk")))

(define-public crate-typed-sql-0.1.1 (c (n "typed-sql") (v "0.1.1") (d (list (d (n "async-trait") (r "^0.1.50") (d #t) (k 0)) (d (n "typed-sql-derive") (r "^0.1.1") (d #t) (k 0)))) (h "1x043w0c9rcks53zldfs34vnkqvaj79mhdn2j1y9wwq65hjvycw1")))

(define-public crate-typed-sql-0.1.2 (c (n "typed-sql") (v "0.1.2") (d (list (d (n "async-trait") (r "^0.1.50") (d #t) (k 0)) (d (n "typed-sql-derive") (r "^0.1.1") (d #t) (k 0)))) (h "0gid9p0w96x92ssm74sxwj92nv8x5xn8jrxc647hy6slzrp69981")))

(define-public crate-typed-sql-0.2.0 (c (n "typed-sql") (v "0.2.0") (d (list (d (n "async-trait") (r "^0.1.50") (d #t) (k 0)) (d (n "typed-sql-derive") (r "^0.1.2") (d #t) (k 0)))) (h "0rx674a4pxfycmcn4cj015nxi6zsp9y54fz871qlw234qc3xsg8s")))

(define-public crate-typed-sql-0.2.1 (c (n "typed-sql") (v "0.2.1") (d (list (d (n "async-trait") (r "^0.1.50") (d #t) (k 0)) (d (n "sqlx") (r "^0.5.5") (f (quote ("runtime-tokio-native-tls"))) (o #t) (d #t) (k 0)) (d (n "typed-sql-derive") (r "^0.1.2") (d #t) (k 0)))) (h "16rhnmckdhvbiy55a7bamj0avq9xxjfvhaixv2vpgxjdgc15l8xv") (f (quote (("default" "sqlx"))))))

