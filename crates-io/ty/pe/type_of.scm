(define-module (crates-io ty pe type_of) #:use-module (crates-io))

(define-public crate-type_of-0.1.0 (c (n "type_of") (v "0.1.0") (h "1yi5xryjpbmy7w2jjcyhrskjs8b1h7s7dkl7l43bg6wkygpqly8i")))

(define-public crate-type_of-0.1.1 (c (n "type_of") (v "0.1.1") (h "0q36xh877d9hpinzrsj9sv2agqfv6ccbd1gwa333vxw6rs46zxba")))

