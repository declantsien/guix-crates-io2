(define-module (crates-io ty pe typefun) #:use-module (crates-io))

(define-public crate-typefun-0.1.0 (c (n "typefun") (v "0.1.0") (h "0x0yj3xba8k57v6nkps87l5dr1rd8k6d94xyjhj40cm5k2mvh7hp")))

(define-public crate-typefun-0.2.0 (c (n "typefun") (v "0.2.0") (h "0yb92glfc63hzn6mrbjqllnbp7rp6nnadiwyfh7djaws6b10m86n")))

(define-public crate-typefun-0.2.1 (c (n "typefun") (v "0.2.1") (h "1200jbxiwkwc7h3ccnm6v2w0xqq35nayfiijnvw8bp201maviki4")))

(define-public crate-typefun-0.3.0 (c (n "typefun") (v "0.3.0") (h "1abd7p36daza3z1w16i8wq1xsx6zxk90shafqsbh3rv31nnh9kp5")))

(define-public crate-typefun-0.3.1 (c (n "typefun") (v "0.3.1") (h "068sl0zw9dk7db24i09x3g3rzf2bbgp027yvrg2y49r1pydndang")))

(define-public crate-typefun-0.4.0 (c (n "typefun") (v "0.4.0") (h "0g79dj4yaz3lza0bfqxg1yv8yqlwbzny24vkfda5ab6n1p8i0dz9")))

