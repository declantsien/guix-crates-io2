(define-module (crates-io ty pe typeshare) #:use-module (crates-io))

(define-public crate-typeshare-0.0.1 (c (n "typeshare") (v "0.0.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.30") (d #t) (k 0)) (d (n "quote") (r "^0.6.13") (d #t) (k 0)) (d (n "syn") (r "^0.15.42") (f (quote ("full" "visit" "printing"))) (d #t) (k 0)))) (h "0sv003lgz6k8dm0rn222z1c3m1f7gx91bhab2r4c330j53nr60qg")))

(define-public crate-typeshare-0.0.2 (c (n "typeshare") (v "0.0.2") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.30") (d #t) (k 0)) (d (n "quote") (r "^0.6.13") (d #t) (k 0)) (d (n "syn") (r "^0.15.42") (f (quote ("full" "visit" "printing"))) (d #t) (k 0)))) (h "1nii626wd23fdb4a0i5qp7x27gv47n8k0wgc0qanrz3vg3j718w7")))

(define-public crate-typeshare-0.0.3 (c (n "typeshare") (v "0.0.3") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.30") (d #t) (k 0)) (d (n "quote") (r "^0.6.13") (d #t) (k 0)) (d (n "syn") (r "^0.15.42") (f (quote ("full" "visit" "printing" "extra-traits"))) (d #t) (k 0)))) (h "1kl5g1sd6iirwafkwijcnawhgqsm1if5r5q0nm3096dzgxqshp4f")))

(define-public crate-typeshare-0.0.4 (c (n "typeshare") (v "0.0.4") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.30") (d #t) (k 0)) (d (n "quote") (r "^0.6.13") (d #t) (k 0)) (d (n "syn") (r "^0.15.42") (f (quote ("full" "visit" "printing" "extra-traits"))) (d #t) (k 0)))) (h "07j1d6c2dsga831cglv06jbhhwgy9q8aqsfym1jbylzilnh55gba")))

(define-public crate-typeshare-0.0.5 (c (n "typeshare") (v "0.0.5") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.30") (d #t) (k 0)) (d (n "quote") (r "^0.6.13") (d #t) (k 0)) (d (n "syn") (r "^0.15.42") (f (quote ("full" "visit" "printing" "extra-traits"))) (d #t) (k 0)))) (h "0b73ww4q0l1shjsqbzk2dp78gcfn89j50kq5mhklgq8rzdrigv4q")))

(define-public crate-typeshare-0.0.6 (c (n "typeshare") (v "0.0.6") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.30") (d #t) (k 0)) (d (n "quote") (r "^0.6.13") (d #t) (k 0)) (d (n "syn") (r "^0.15.42") (f (quote ("full" "visit" "printing" "extra-traits"))) (d #t) (k 0)))) (h "1ksr3gflkh8zmkwysrbbnz3kccz3c7ag8p3rdlza4s3k071j704m")))

(define-public crate-typeshare-0.0.7 (c (n "typeshare") (v "0.0.7") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.30") (d #t) (k 0)) (d (n "quote") (r "^0.6.13") (d #t) (k 0)) (d (n "syn") (r "^0.15.42") (f (quote ("full" "visit" "printing" "extra-traits"))) (d #t) (k 0)) (d (n "text-diff") (r "^0.4.0") (d #t) (k 2)))) (h "10rxqw434h6l0kysrzri95xmpc5kvgq2sdc3kbar1bbkjv4dl75c")))

(define-public crate-typeshare-0.0.8 (c (n "typeshare") (v "0.0.8") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.30") (d #t) (k 0)) (d (n "quote") (r "^0.6.13") (d #t) (k 0)) (d (n "syn") (r "^0.15.42") (f (quote ("full" "visit" "printing" "extra-traits"))) (d #t) (k 0)) (d (n "text-diff") (r "^0.4.0") (d #t) (k 2)))) (h "1sr3y409j43kwa5dic56j9h4lzaw6nqhbqanxibbq51gbz3rzwy9")))

(define-public crate-typeshare-0.0.9 (c (n "typeshare") (v "0.0.9") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.30") (d #t) (k 0)) (d (n "quote") (r "^0.6.13") (d #t) (k 0)) (d (n "syn") (r "^0.15.42") (f (quote ("full" "visit" "printing" "extra-traits"))) (d #t) (k 0)) (d (n "text-diff") (r "^0.4.0") (d #t) (k 2)))) (h "13d0jsi8mc4bm5y36m5ak1i1y8k7r29cjyj06aqx52yk8z7bqcni")))

(define-public crate-typeshare-0.0.10 (c (n "typeshare") (v "0.0.10") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.30") (d #t) (k 0)) (d (n "quote") (r "^0.6.13") (d #t) (k 0)) (d (n "syn") (r "^0.15.42") (f (quote ("full" "visit" "printing" "extra-traits"))) (d #t) (k 0)) (d (n "text-diff") (r "^0.4.0") (d #t) (k 2)))) (h "1ss6x530ss1gxiip5riyd2279y050dhl3malfqxr1zl3g5l8852y")))

(define-public crate-typeshare-0.0.11 (c (n "typeshare") (v "0.0.11") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.30") (d #t) (k 0)) (d (n "quote") (r "^0.6.13") (d #t) (k 0)) (d (n "syn") (r "^0.15.42") (f (quote ("full" "visit" "printing" "extra-traits"))) (d #t) (k 0)) (d (n "text-diff") (r "^0.4.0") (d #t) (k 2)))) (h "0zmmi7zn98lnx0hpapdb76wb3xgwqgnclb83rhbvhzjy6i4cqlwf")))

(define-public crate-typeshare-1.0.0 (c (n "typeshare") (v "1.0.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("clock" "std" "wasmbind"))) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "typeshare-annotation") (r "^1.0.0") (d #t) (k 0)))) (h "1mlby09yfpadq7bwrfqmbjbla0s8sqi2q7hrhkkaq1rj4pbrdw6p") (r "1.57")))

(define-public crate-typeshare-1.0.1 (c (n "typeshare") (v "1.0.1") (d (list (d (n "chrono") (r "^0.4") (f (quote ("clock" "std" "wasmbind"))) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "typeshare-annotation") (r "^1.0.0") (d #t) (k 0)))) (h "1mi7snkx2b4g84x8vx38v1myg5r6g48c865j0nz5zcsc8lpilkgl") (r "1.57")))

(define-public crate-typeshare-1.0.2 (c (n "typeshare") (v "1.0.2") (d (list (d (n "chrono") (r "^0.4") (f (quote ("clock" "std" "wasmbind"))) (k 0)) (d (n "serde") (r "^1.0.164") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "typeshare-annotation") (r "^1.0.3") (d #t) (k 0)))) (h "0gvklyz5rgz9h7xrzipqxzcrxznp1xwq07hhcwm0f98bfxk7m1wr") (r "1.57")))

(define-public crate-typeshare-1.0.3 (c (n "typeshare") (v "1.0.3") (d (list (d (n "chrono") (r "^0.4") (f (quote ("clock" "std" "wasmbind"))) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "typeshare-annotation") (r "^1.0.4") (d #t) (k 0)))) (h "11riglm8incm0vq7ciyd907w1sc6frfn7h7ab0yp8bkcnycp7w84")))

