(define-module (crates-io ty pe typed-builder) #:use-module (crates-io))

(define-public crate-typed-builder-0.0.0 (c (n "typed-builder") (v "0.0.0") (d (list (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "12z83i1x16f6sqkfflyx38jpxmq46crigpv1wy5gclqr3lvzdl8j")))

(define-public crate-typed-builder-0.1.0 (c (n "typed-builder") (v "0.1.0") (d (list (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "1lacicnij3w8lpvm32fd5c7b1206zj0i26znym6nj6gq1pzsh5qa")))

(define-public crate-typed-builder-0.1.1 (c (n "typed-builder") (v "0.1.1") (d (list (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "05xg23qkn4634nkh5az769hk6jhqy6rjf0f0dwaa25by2qv3z1ch")))

(define-public crate-typed-builder-0.2.0 (c (n "typed-builder") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^0.4.27") (d #t) (k 0)) (d (n "quote") (r "^0.6.11") (d #t) (k 0)) (d (n "syn") (r "^0.15.26") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "133942rip9nhpx541lp0bba5diakmz6cqlcdxjwvzf8wrmny6qry")))

(define-public crate-typed-builder-0.3.0 (c (n "typed-builder") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^0.4.27") (d #t) (k 0)) (d (n "quote") (r "^0.6.11") (d #t) (k 0)) (d (n "syn") (r "^0.15.26") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1ilwi1s09sjirq6pz45h1q6k5yazc4cn4q9rmn73pvvs8dx50f3y")))

(define-public crate-typed-builder-0.4.0 (c (n "typed-builder") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "154apnkkjzsw3h3kqaqk53lc3skni8x7sdfgg0kvya20mwai2lri")))

(define-public crate-typed-builder-0.4.1 (c (n "typed-builder") (v "0.4.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1fc7aycz6yajhvgccbzcxwqnm638lnjz8llg69zm8yncgbr5bjfz")))

(define-public crate-typed-builder-0.5.0 (c (n "typed-builder") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1jkg02wjqmfwy4r68n44nnqyx7alq90vprsdka40g4hj9hfwpay0")))

(define-public crate-typed-builder-0.5.1 (c (n "typed-builder") (v "0.5.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1zi9mmkz2vwg5cn8x9738vli42h21jyspvfi83y2sa6lvlja5kkq")))

(define-public crate-typed-builder-0.6.0 (c (n "typed-builder") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1yq009zk3dbadfnvirgkc765q3kf5rjaav3cxx9ilqhw35cl9z45")))

(define-public crate-typed-builder-0.7.0 (c (n "typed-builder") (v "0.7.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1v6nqq5dlmjx5xrzl4402kpjgm60pyjk9az2xhzjpxj07pvc6hnr")))

(define-public crate-typed-builder-0.7.1 (c (n "typed-builder") (v "0.7.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0cw5ijb5r73lziapvk7ag9cqlf2y874fqangq3ra6jglyiq44pzq")))

(define-public crate-typed-builder-0.8.0 (c (n "typed-builder") (v "0.8.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1fp3zbh4hfv50h6j5k0chmcdvhvzl8fwzw8mgz8pdp6qihdndiif")))

(define-public crate-typed-builder-0.9.0 (c (n "typed-builder") (v "0.9.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0vwwd8z49xhp5wi9a9q781an0nqz5ndcf1sh1jv5b8va833jcm1l")))

(define-public crate-typed-builder-0.9.1 (c (n "typed-builder") (v "0.9.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1i88rxhh6yg56i212sknhz207dw2rgnyg54wpqqr3xvgf2yyavm4")))

(define-public crate-typed-builder-0.10.0 (c (n "typed-builder") (v "0.10.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "036v5045xsc8akqbqm0npyxw9pvxwqiq9aix7cwpx4vvnqb1g1c9")))

(define-public crate-typed-builder-0.11.0 (c (n "typed-builder") (v "0.11.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "07nlizmjyya5f792qy0zdrqa4lz0ildxc34h421rxwxz1nj2d8a7")))

(define-public crate-typed-builder-0.12.0 (c (n "typed-builder") (v "0.12.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "12sjyj5vh2jh1h02g5kl0g6bzk5sr5qz6c4gfqm28h8nk0xk6yb1")))

(define-public crate-typed-builder-0.13.0 (c (n "typed-builder") (v "0.13.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "06lgxjsx6fbgh02wcgqwl3rim92qvj1hmzj96zql0pglrdyi0p5x")))

(define-public crate-typed-builder-0.14.0 (c (n "typed-builder") (v "0.14.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0w2801bz5zrs05ki4cc6g9gg68w2j61rxr4d0k5ccywvrcia7jv4")))

(define-public crate-typed-builder-0.15.0 (c (n "typed-builder") (v "0.15.0") (d (list (d (n "typed-builder-macro") (r "=0.15.0") (d #t) (k 0)))) (h "10saxhzfpglsibcqlfkf65x4d7wvg3bkwr33gk92b0d21ih3a8ad")))

(define-public crate-typed-builder-0.15.1 (c (n "typed-builder") (v "0.15.1") (d (list (d (n "typed-builder-macro") (r "=0.15.1") (d #t) (k 0)))) (h "1f2kj6p5j5p5j4vcsv0y4rlcmdnh0031vi6cby1x4vl98z0y3ncq")))

(define-public crate-typed-builder-0.15.2 (c (n "typed-builder") (v "0.15.2") (d (list (d (n "typed-builder-macro") (r "=0.15.2") (d #t) (k 0)))) (h "1lk6jnkx5lwmg3dlfxnls26d68svy28air4wrg2fhxaqm22krs3z")))

(define-public crate-typed-builder-0.16.0 (c (n "typed-builder") (v "0.16.0") (d (list (d (n "typed-builder-macro") (r "=0.16.0") (d #t) (k 0)))) (h "1wc5j2mqc2p69lbr0hrnfdvzi0dil6l7a1psgw94f2ffdjjsl1b6")))

(define-public crate-typed-builder-0.16.1 (c (n "typed-builder") (v "0.16.1") (d (list (d (n "typed-builder-macro") (r "=0.16.1") (d #t) (k 0)))) (h "14zm2lkygkif9n41jaw30yxizrkgsmv153hri952g0388ypjgxvn")))

(define-public crate-typed-builder-0.16.2 (c (n "typed-builder") (v "0.16.2") (d (list (d (n "typed-builder-macro") (r "=0.16.2") (d #t) (k 0)))) (h "05ny1brm9ff3hxrps3n328w28myk4lz0h24jhxx64dhyjhbmq21l")))

(define-public crate-typed-builder-0.17.0 (c (n "typed-builder") (v "0.17.0") (d (list (d (n "typed-builder-macro") (r "=0.17.0") (d #t) (k 0)))) (h "1wr4g76d6yqiyb4jhxk8h0012ddds7s1r97x8gqsdmnklq3a1imi")))

(define-public crate-typed-builder-0.18.0 (c (n "typed-builder") (v "0.18.0") (d (list (d (n "typed-builder-macro") (r "=0.18.0") (d #t) (k 0)))) (h "12pkf3h3hahfqm8g6k3kl2qicl346szqq24qa74vfqcq2jb08z74")))

(define-public crate-typed-builder-0.18.1 (c (n "typed-builder") (v "0.18.1") (d (list (d (n "typed-builder-macro") (r "=0.18.1") (d #t) (k 0)))) (h "0glkkhmdgyyxyvyq5zriz5a8h3ybb0j0ks3hhwbcp4qv0548fka4")))

(define-public crate-typed-builder-0.18.2 (c (n "typed-builder") (v "0.18.2") (d (list (d (n "typed-builder-macro") (r "=0.18.2") (d #t) (k 0)))) (h "1p9s9p7f3mnylrzdqbxj73d9dw95syma6pnnyfp3ys801s49qwvp")))

