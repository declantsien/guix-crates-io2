(define-module (crates-io ty pe typestates-comparison) #:use-module (crates-io))

(define-public crate-typestates-comparison-0.5.0 (c (n "typestates-comparison") (v "0.5.0") (d (list (d (n "typestate") (r "^0.8.0") (d #t) (k 0)))) (h "120a0q4ffijcgylisvkz0bj7np312v6fps62brmw73968fkwas5r")))

(define-public crate-typestates-comparison-0.5.1 (c (n "typestates-comparison") (v "0.5.1") (d (list (d (n "typestate") (r "^0.8.0") (d #t) (k 0)))) (h "1lga19qvy05ac77dahlw72f710frsav92p08h7fzm82qyj9qid39")))

