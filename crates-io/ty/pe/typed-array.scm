(define-module (crates-io ty pe typed-array) #:use-module (crates-io))

(define-public crate-typed-array-0.1.0 (c (n "typed-array") (v "0.1.0") (d (list (d (n "err-derive") (r "^0.1.5") (d #t) (k 0)) (d (n "js-sys") (r "^0.3") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)))) (h "1haz0vcg65n0c5cnyjjmy46zr6r4dz5abz76kaq5hd3xpgygnyg4")))

(define-public crate-typed-array-0.2.0 (c (n "typed-array") (v "0.2.0") (d (list (d (n "err-derive") (r "^0.1.5") (d #t) (k 0)) (d (n "js-sys") (r "^0.3") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.2") (d #t) (k 2)))) (h "16lpj7kvn1r573503v5795hcnj8zj616qzp2s0jpjwj2sx0ingcg")))

