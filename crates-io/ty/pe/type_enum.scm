(define-module (crates-io ty pe type_enum) #:use-module (crates-io))

(define-public crate-type_enum-0.1.0 (c (n "type_enum") (v "0.1.0") (d (list (d (n "const_list") (r "^0.1.0") (d #t) (k 0)))) (h "0v1rxhvp617xk0gc7d4l6i20ipkjk5aggpvkck3r8731hgkv6wv3")))

(define-public crate-type_enum-0.1.1 (c (n "type_enum") (v "0.1.1") (d (list (d (n "const_list") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.190") (o #t) (k 0)))) (h "0g8d771djdiz5zq3f7k69j6lq48wyh003vn21v4847fy9k6s8nwr")))

(define-public crate-type_enum-0.1.2 (c (n "type_enum") (v "0.1.2") (d (list (d (n "const_list") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.190") (o #t) (k 0)))) (h "1l730mc8wfc2zx0brcpdsbjnms1zkvrmm7w06ycjhswcv3vpljkq")))

(define-public crate-type_enum-0.1.3 (c (n "type_enum") (v "0.1.3") (d (list (d (n "const_list") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.190") (o #t) (k 0)))) (h "0s5mxyrsb7x1gl0prak8y84066l31xkl3991s2kj5w34yrk8xxal")))

(define-public crate-type_enum-0.1.4 (c (n "type_enum") (v "0.1.4") (d (list (d (n "const_list") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.190") (o #t) (k 0)))) (h "1mvpswgnnljqlyaq17829s03axc6v50qb6n39qaqzqarzagrpjs3")))

(define-public crate-type_enum-0.1.5 (c (n "type_enum") (v "0.1.5") (d (list (d (n "const_list") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.190") (o #t) (k 0)))) (h "1xk5nn2jz9szryrsxsm3lps72c6y5ca2a124srjaqicpaqb9p932")))

(define-public crate-type_enum-0.1.6 (c (n "type_enum") (v "0.1.6") (d (list (d (n "const_list") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.190") (o #t) (k 0)))) (h "0301z5i3ksacl1yzyg1i13pj5asw3590nnh732qrh4kiw7fvvbac")))

