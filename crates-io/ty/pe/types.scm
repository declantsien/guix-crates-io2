(define-module (crates-io ty pe types) #:use-module (crates-io))

(define-public crate-types-0.1.0 (c (n "types") (v "0.1.0") (h "1vj8sdi8172g88vx80n44q2magnhphfghqr249dq434jbah15qjg")))

(define-public crate-types-0.1.1 (c (n "types") (v "0.1.1") (h "0d0kdk1icjxzd2n5l908ih4g4sdg156kgys2ildqazpsx2fg9hkx")))

(define-public crate-types-0.1.2 (c (n "types") (v "0.1.2") (h "146a6fc0ybwhw5iannpmb6wmgd63sbppbqmk9iibd88x43asqxnh")))

