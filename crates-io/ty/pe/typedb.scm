(define-module (crates-io ty pe typedb) #:use-module (crates-io))

(define-public crate-typedb-0.6.0 (c (n "typedb") (v "0.6.0") (d (list (d (n "bincode") (r "^0.7") (d #t) (k 0)) (d (n "fs2") (r "^0.4.1") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 0)))) (h "0if481krjgyns78jw3gc7zwnz1dv1qm1mx6849vrllzals8sxxmh")))

(define-public crate-typedb-0.6.1 (c (n "typedb") (v "0.6.1") (d (list (d (n "bincode") (r "^0.7") (d #t) (k 0)) (d (n "fs2") (r "^0.4.1") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 0)))) (h "1jzyhg6kdbmiybr14gjw4h2v5605rr5i4xak5mlacpb92dh5inkn")))

(define-public crate-typedb-0.7.0 (c (n "typedb") (v "0.7.0") (d (list (d (n "bincode") (r "^0.7") (d #t) (k 0)) (d (n "fs2") (r "^0.4.1") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 0)))) (h "1ww1vbchwhfgvr57mms2aa7ikbgibah7r0jn6z4k70r94z1sfkxk")))

(define-public crate-typedb-0.7.1 (c (n "typedb") (v "0.7.1") (d (list (d (n "bincode") (r "^0.8") (d #t) (k 0)) (d (n "fs2") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1vf8z08dp68w0i5axrscnysqrpfr2nka1i2fxhpipp3wjcs0i152")))

(define-public crate-typedb-0.8.0 (c (n "typedb") (v "0.8.0") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "persy") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1m9vzz654f07cyzcvskxa82f20gw22i8n2nr703xf4h3m92c9sxb")))

(define-public crate-typedb-0.8.1 (c (n "typedb") (v "0.8.1") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "persy") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1d1lx48iabp6gx6xscg2c29ih1d054h96bcrm9qn1sifnx4bj1y0")))

(define-public crate-typedb-0.9.0 (c (n "typedb") (v "0.9.0") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "persy") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1p7j28c2k7rimkp7wyjks9y4s6wpm9g52q467lj2cbs872ifr02p")))

