(define-module (crates-io ty pe typedef) #:use-module (crates-io))

(define-public crate-typedef-0.0.1 (c (n "typedef") (v "0.0.1") (h "062jyi6kgjlkikmy4ch7r6k7q61m3630xxgnimzjdy9b2hnh2f7f")))

(define-public crate-typedef-0.0.2 (c (n "typedef") (v "0.0.2") (h "0qmpd13yyq2y02aj0kb0sc7g6238sdcabvf32h15dlfq6ian2jsa")))

(define-public crate-typedef-0.1.0 (c (n "typedef") (v "0.1.0") (h "1s8sawg32zp6q7zsxq0zsgiz57jyvgjaq2xqvz9ms0s4f43n6an2")))

(define-public crate-typedef-0.1.1 (c (n "typedef") (v "0.1.1") (h "1136v1llbld75xk090yf6z18132g9hir4nrm3l85jnm29gv8cw6d")))

(define-public crate-typedef-0.1.2 (c (n "typedef") (v "0.1.2") (h "0rbz843fmfyp0w6i48mavkx602lq9lybav6f4hlani2myqn00kw7")))

(define-public crate-typedef-0.1.3 (c (n "typedef") (v "0.1.3") (h "0rfhsr33r054g24wnj86dm559w5gjf1xvkimlzb081ihpgb5g250")))

(define-public crate-typedef-0.2.0 (c (n "typedef") (v "0.2.0") (h "1bym85addkrw7lv656lj0xg6ivsd7iwv7cw8bxbqgfa3nxlc4zg2")))

(define-public crate-typedef-0.2.1 (c (n "typedef") (v "0.2.1") (h "1yxmvzsmmwffkr3qycqrihjz007x1rkrg2pvhpm3r3vnigd07w98")))

(define-public crate-typedef-0.3.0 (c (n "typedef") (v "0.3.0") (h "0088ck6864k16c07qan45hwgmawjb96bn2mp21z2sws3szi7rqy2") (f (quote (("nightly") ("default"))))))

(define-public crate-typedef-0.3.1 (c (n "typedef") (v "0.3.1") (h "1vi5f8qvr767zflvcjzfdsa87hxyncg9yxrjln6vs4rwaqm8wixf") (f (quote (("nightly") ("default"))))))

(define-public crate-typedef-0.3.2 (c (n "typedef") (v "0.3.2") (h "19kzs88x4d2vdvqv0pzaf1s5myiywcshb8jbfpw5p0vs8f8fy35h") (f (quote (("nightly") ("default"))))))

