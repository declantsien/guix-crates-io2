(define-module (crates-io ty pe typed-sql-derive) #:use-module (crates-io))

(define-public crate-typed-sql-derive-0.1.0 (c (n "typed-sql-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.26") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.72") (d #t) (k 0)))) (h "18wpmg7vqsiwvxc0n4vj99lc2wk3brfn0f1mgs1sjm05fkfkprqf")))

(define-public crate-typed-sql-derive-0.1.1 (c (n "typed-sql-derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.26") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.72") (d #t) (k 0)))) (h "0cdpn7mqjp3r3x0sk6pymgmx2nwkbg71dam11a3b8z2b7385d050")))

(define-public crate-typed-sql-derive-0.1.2 (c (n "typed-sql-derive") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.26") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.72") (d #t) (k 0)))) (h "1lh090qqfcmbi64dykhynaglzlfxarhygbqmyw9r7crysqqi7287")))

