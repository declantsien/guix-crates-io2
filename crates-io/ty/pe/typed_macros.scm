(define-module (crates-io ty pe typed_macros) #:use-module (crates-io))

(define-public crate-typed_macros-1.0.0 (c (n "typed_macros") (v "1.0.0") (h "0zzvfygmbxg89c3j6d2cjsdlrz7v5jjck2q4qaxsrn2czj6zj18p")))

(define-public crate-typed_macros-1.0.1 (c (n "typed_macros") (v "1.0.1") (h "1ca01xxg2vclql3rfqs79q0a34v3i09in9njjy3iamijqnjm4mp4")))

(define-public crate-typed_macros-1.0.2 (c (n "typed_macros") (v "1.0.2") (h "0q9r9zqwz0c3bmcbw2gvr37b9sra0xw9cp40bwhzag4qfrjybzbi")))

(define-public crate-typed_macros-1.0.3 (c (n "typed_macros") (v "1.0.3") (h "0wk9hiq28pkzknr904b77v6plx71k994im6p7aid4ky1s243sv2j")))

(define-public crate-typed_macros-1.0.4 (c (n "typed_macros") (v "1.0.4") (h "1wi6ks5q5s1xgashsawmg9pmylvhl399adk71c8fla7wr6x3cc8j")))

(define-public crate-typed_macros-1.0.5 (c (n "typed_macros") (v "1.0.5") (h "1c6r5lkcxp9gf8cixsjrkf1aic1mhicz4h5f5vizs2bac6bd33mv")))

(define-public crate-typed_macros-1.0.6 (c (n "typed_macros") (v "1.0.6") (h "1k0kzn5kg1i28qy69pd08vd3wwajl4z69pc18qp3nijfyfy20sn0")))

