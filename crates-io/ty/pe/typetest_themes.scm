(define-module (crates-io ty pe typetest_themes) #:use-module (crates-io))

(define-public crate-typetest_themes-1.0.0 (c (n "typetest_themes") (v "1.0.0") (d (list (d (n "iced_core") (r "^0.4") (d #t) (k 0)) (d (n "iced_style") (r "^0.3") (d #t) (k 0)))) (h "0jcv9dwqv06rmc29w4cnx913nfcfharwd1b9k6fsvs4i1xf8ifw4")))

