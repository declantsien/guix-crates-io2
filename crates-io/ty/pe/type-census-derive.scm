(define-module (crates-io ty pe type-census-derive) #:use-module (crates-io))

(define-public crate-type-census-derive-0.1.0 (c (n "type-census-derive") (v "0.1.0") (d (list (d (n "darling") (r "^0.13.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0wgywmvfjbh3ksjir951wraz82hv9pa7jl3pf2xfz79l48brw2n5")))

(define-public crate-type-census-derive-0.1.1 (c (n "type-census-derive") (v "0.1.1") (d (list (d (n "darling") (r "^0.13.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "17m1r4ywxa3r0ll05z322ysvpdwxirgb939pgzq4bhkbg5hz39w9")))

