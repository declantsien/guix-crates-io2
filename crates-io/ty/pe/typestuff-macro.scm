(define-module (crates-io ty pe typestuff-macro) #:use-module (crates-io))

(define-public crate-typestuff-macro-0.1.0 (c (n "typestuff-macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.102") (f (quote ("full"))) (d #t) (k 0)))) (h "1cqr4fl9ng3cv7g2i26c22ygq3dhxc3317i50p4j0kynkkcscykn")))

(define-public crate-typestuff-macro-0.2.0 (c (n "typestuff-macro") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.102") (f (quote ("full"))) (d #t) (k 0)))) (h "0fi5npqc4m0591iqgffpfnyk34fvq17laah4cf93srh2wbrhif7j")))

(define-public crate-typestuff-macro-0.3.0 (c (n "typestuff-macro") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.102") (f (quote ("full"))) (d #t) (k 0)))) (h "1cvi75vqhib210svgw6krq8p0jczz55i6swkzqgxqamv6p5wmp0g")))

