(define-module (crates-io ty pe type_dispatch_macros) #:use-module (crates-io))

(define-public crate-type_dispatch_macros-0.0.0 (c (n "type_dispatch_macros") (v "0.0.0") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1pc4d10mixllf255fc9yrs89jam586lijwrkx8hs03miqq4szsz8")))

