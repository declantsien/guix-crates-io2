(define-module (crates-io ty pe type_description_derive) #:use-module (crates-io))

(define-public crate-type_description_derive-0.1.0 (c (n "type_description_derive") (v "0.1.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.38") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.93") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0zdmrvl386f6ngr341pdg6sn3sir3adhiiiq64sr747xc39adh27")))

(define-public crate-type_description_derive-0.2.0 (c (n "type_description_derive") (v "0.2.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0d3pxb26xnnbnl04iaq8adshh523q0f81njg59g6l8jw87jf35p2")))

(define-public crate-type_description_derive-0.3.0 (c (n "type_description_derive") (v "0.3.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "16hl6y73qd5q23d4a7v2shp9nn9kw99s9awavdsw9dhhiyia7c52")))

(define-public crate-type_description_derive-0.4.0 (c (n "type_description_derive") (v "0.4.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1hwa9gg1xg1kx4slls6fybfcif6cnywqxqjf4lccwyapn6zzfiq0")))

(define-public crate-type_description_derive-0.5.0 (c (n "type_description_derive") (v "0.5.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.27") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1dmxg15myd9bx25kqdx65s0xcxfhigqz3i2nizw18z4wm6msdymb")))

