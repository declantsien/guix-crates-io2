(define-module (crates-io ty pe typeface) #:use-module (crates-io))

(define-public crate-typeface-0.0.1 (c (n "typeface") (v "0.0.1") (h "1hcbh17bqv9azwhfnws60bxhwr8y1fslg51amydlx5q5xf9j2254")))

(define-public crate-typeface-0.1.0 (c (n "typeface") (v "0.1.0") (h "03nmw1jz65rlh0lbfd0jvj642i9slrpzvfz3a1gbdf3430mz6cvn")))

(define-public crate-typeface-0.1.1 (c (n "typeface") (v "0.1.1") (h "07ay4q7n77il4nc5bhg2y5arcpwbdsqyi6rafwp491ly8yvgszv5")))

(define-public crate-typeface-0.2.0 (c (n "typeface") (v "0.2.0") (h "0i1scnyc4dw3c5mgr4xpjlikc98671wdgmdvimnjncalina21p61")))

(define-public crate-typeface-0.2.1 (c (n "typeface") (v "0.2.1") (h "1h1srxsp94ybhmj9ls1r5zsgg048nzf798n4cflwipqxhns9qdv3")))

(define-public crate-typeface-0.2.2 (c (n "typeface") (v "0.2.2") (h "1609p1iza5i37qnkpbv7nrdq0i72hm34hhq6flm3j0pbrdjczqpd")))

(define-public crate-typeface-0.2.3 (c (n "typeface") (v "0.2.3") (h "04mfjavjclbjr7f3c9w635v42jhq6gz3w40yv59yy4dmb61h0vvi")))

(define-public crate-typeface-0.2.4 (c (n "typeface") (v "0.2.4") (h "19zmd9b13mgigysi8mc9g1qwzispkm77qav6kxj5dvd7ic8jlyjy")))

(define-public crate-typeface-0.2.5 (c (n "typeface") (v "0.2.5") (h "08apc22nhm027c1p64klclndv6aib2j8gwvbq1i1xk25k9pmnpwp")))

(define-public crate-typeface-0.2.6 (c (n "typeface") (v "0.2.6") (h "1jn7zx00v1hkkixg7xl5iqiyqvkq0f6iy6yyz8s9r4yf5ykbl72y")))

(define-public crate-typeface-0.2.7 (c (n "typeface") (v "0.2.7") (h "16viidah17059rhb55gki68q8xza83fv7ic4r5f0fr9vqa60wqdz")))

(define-public crate-typeface-0.3.0 (c (n "typeface") (v "0.3.0") (h "1ifiafp6vjna0d241gjdw42z2f3snr2g7jjmmismcvck52bsmsb9")))

(define-public crate-typeface-0.3.1 (c (n "typeface") (v "0.3.1") (h "04lqyqck96v6lswlskcjkyndz81z76djx60ixn535qq42v0n180y")))

(define-public crate-typeface-0.3.2 (c (n "typeface") (v "0.3.2") (h "1mxvjdh5w2maslmj77h2fk635x6xi0d3905samyhirm02lwjlsx7")))

(define-public crate-typeface-0.3.3 (c (n "typeface") (v "0.3.3") (h "06h3201c1l5iqv1a74zcbd6j5j6538w44dlfd29j8h5c6n3dqjjd")))

(define-public crate-typeface-0.3.4 (c (n "typeface") (v "0.3.4") (h "1f6f875l87sa4s75i5b2rv5nfzlbgfsp8af2dybakpviaa3vyjcz") (y #t)))

(define-public crate-typeface-0.3.5 (c (n "typeface") (v "0.3.5") (h "0jkvznp5j79a6fhi1c40afpzq2y9rb8fg6kn386fcd3dyn35hmnq") (y #t)))

(define-public crate-typeface-0.3.6 (c (n "typeface") (v "0.3.6") (h "18gbd4ra1qnlcm3dwr7zci4pmi6p2acww9zwp2n8g5g02sn2a23z")))

(define-public crate-typeface-0.4.0 (c (n "typeface") (v "0.4.0") (h "0fkvcix1jfbkl07hy2iwlj2nx9alkpwairsmvyd146xf024cpj81")))

(define-public crate-typeface-0.4.1 (c (n "typeface") (v "0.4.1") (h "05lcjnx1fmckg5irg52nq0wlkqnqyna91a4dlkk7pdl5dmkzsl9s")))

(define-public crate-typeface-0.4.2 (c (n "typeface") (v "0.4.2") (h "0l40bv77da5w2m78xc1mcldhk11blrrnvhqviz23dgz2m99vrzg1")))

