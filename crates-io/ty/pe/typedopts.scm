(define-module (crates-io ty pe typedopts) #:use-module (crates-io))

(define-public crate-typedopts-0.0.1 (c (n "typedopts") (v "0.0.1") (h "12hvlswgw5cdxcqlqf21f05a1slkk42m2jnzw9c5nn2ykfrsnh1r")))

(define-public crate-typedopts-1.0.0 (c (n "typedopts") (v "1.0.0") (d (list (d (n "getopts") (r "~0.1.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "~0.2.8") (d #t) (k 0)))) (h "1qj34lixbdkznbia3w53v5nrj5jb4z8mb31v02laxfqpv4hp1kb6")))

(define-public crate-typedopts-1.0.1 (c (n "typedopts") (v "1.0.1") (d (list (d (n "getopts") (r "~0.1.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "~0.2.8") (d #t) (k 0)))) (h "0qb68l73lavai90rkd8m42fvn3i94ys8ayc7074pmy5mgqfiwa23")))

(define-public crate-typedopts-1.1.0 (c (n "typedopts") (v "1.1.0") (d (list (d (n "getopts") (r "~0.2.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "~0.2.8") (d #t) (k 0)))) (h "0592z389n9nfh4x6b7f6ma3b6if2pncyxg5khznaym123r02kvv4")))

(define-public crate-typedopts-1.1.1 (c (n "typedopts") (v "1.1.1") (d (list (d (n "getopts") (r "~0.2.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "~0.3.0") (d #t) (k 0)))) (h "0iivlk2ydzvkv9ibmikb20ml785iiv0lrs3nv4lh85n4mlm1dvxk")))

(define-public crate-typedopts-1.1.2 (c (n "typedopts") (v "1.1.2") (d (list (d (n "getopts") (r "~0.2.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "~0.3.0") (d #t) (k 0)))) (h "1ig4719r29i2y083ljlgm3v7gn9fcd4w4zpiwqfmv5cvpdczrmja")))

