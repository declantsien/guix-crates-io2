(define-module (crates-io ty pe typed-arena) #:use-module (crates-io))

(define-public crate-typed-arena-1.0.0 (c (n "typed-arena") (v "1.0.0") (h "0pb8rny2f1arvzbqjyllr9dsm221iy88jhgsbkibgp0j6rj47r8z")))

(define-public crate-typed-arena-1.0.1 (c (n "typed-arena") (v "1.0.1") (h "0dagkym8cfpz0xwzz8l1zdaql5ypfqccmbxziznc0bbd5v00x3ss")))

(define-public crate-typed-arena-1.1.0 (c (n "typed-arena") (v "1.1.0") (h "1b4598sb8ccsiz9rcm4gdnhfwixd14flghg9061pxqqd4m513l08")))

(define-public crate-typed-arena-1.2.0 (c (n "typed-arena") (v "1.2.0") (h "002ijqlvs4bhrv994cjadyi84xfmqz9fmlwszipxdyd41p4rsbwf")))

(define-public crate-typed-arena-1.3.0 (c (n "typed-arena") (v "1.3.0") (h "0rzniw8y31zi83dq35g4jk9j0fs85nzxc836snls9gn179n7fd2r")))

(define-public crate-typed-arena-1.4.0 (c (n "typed-arena") (v "1.4.0") (h "1bmcksakw3ipyc3wjj0qj46i8gw79np55vvn5jfz0242qk4diizs") (f (quote (("std") ("default" "std"))))))

(define-public crate-typed-arena-1.4.1 (c (n "typed-arena") (v "1.4.1") (h "1i8yczhwcy0nnrxqck1lql3i7hvg95l0vw0dbgfb92zkms96mh66") (f (quote (("std") ("default" "std"))))))

(define-public crate-typed-arena-1.5.0 (c (n "typed-arena") (v "1.5.0") (d (list (d (n "criterion") (r "^0.2.4") (d #t) (k 2)))) (h "0im62vrmyzbr8xq66bcyr86ka4z2x8psn9z4982bq4fc8v1zaw3z") (f (quote (("std") ("default" "std"))))))

(define-public crate-typed-arena-1.6.0 (c (n "typed-arena") (v "1.6.0") (d (list (d (n "criterion") (r "^0.2.4") (d #t) (k 2)))) (h "0ngdx7rrg1x0lngbh135ddkvsqxziggi81bjljwbkrr9mlxhp1qf") (f (quote (("std") ("default" "std"))))))

(define-public crate-typed-arena-1.6.1 (c (n "typed-arena") (v "1.6.1") (d (list (d (n "criterion") (r "^0.2.4") (d #t) (k 2)))) (h "01z6l9xv9izbyycxmwxiz4g6ibjb52dm2zc47mfpj56kk6kh81ww") (f (quote (("std") ("default" "std"))))))

(define-public crate-typed-arena-1.7.0 (c (n "typed-arena") (v "1.7.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0va4q7439qzlxh9acd9nba7m7sljdh7xz1gp8l0i597b0y025cm9") (f (quote (("std") ("default" "std"))))))

(define-public crate-typed-arena-2.0.0-rc1 (c (n "typed-arena") (v "2.0.0-rc1") (d (list (d (n "criterion") (r "^0.3.0") (d #t) (k 2)))) (h "1m8086m1yc5wk917kvwj4g06cd4rp6z193xq1fsw4s130423sh8c") (f (quote (("std") ("default" "std"))))))

(define-public crate-typed-arena-2.0.0 (c (n "typed-arena") (v "2.0.0") (d (list (d (n "criterion") (r "^0.3.0") (d #t) (k 2)))) (h "1lmig0sli4lqa2lql1wm6bzc7fr2fx2rgcfn1dgfr0ryvdb53bp2") (f (quote (("std") ("default" "std"))))))

(define-public crate-typed-arena-2.0.1 (c (n "typed-arena") (v "2.0.1") (d (list (d (n "criterion") (r "^0.3.0") (d #t) (k 2)))) (h "1bnhphrksp9krxhsfhfimrxvkbah2pa6rf9ygmpw5lalbm6wi186") (f (quote (("std") ("default" "std"))))))

(define-public crate-typed-arena-2.0.2 (c (n "typed-arena") (v "2.0.2") (d (list (d (n "criterion") (r "^0.3.4") (d #t) (k 2)))) (h "0shj0jpmglhgw2f1i4b33ycdzwd1z205pbs1rd5wx7ks2qhaxxka") (f (quote (("std") ("default" "std"))))))

