(define-module (crates-io ty pe type-toppings) #:use-module (crates-io))

(define-public crate-type-toppings-0.1.0 (c (n "type-toppings") (v "0.1.0") (d (list (d (n "error_reporter") (r "^1") (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 2)))) (h "0prqpn1vwc25j2x1bds28ngdzs0qidvyizl2lqg71afrknvf191h") (f (quote (("default" "futures"))))))

(define-public crate-type-toppings-0.1.1 (c (n "type-toppings") (v "0.1.1") (d (list (d (n "error_reporter") (r "^1") (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 2)))) (h "186jd9jyh5cvlbrlx4k7w30dhinwj4z8fpzyyqr0rn4dvbhkjqll") (f (quote (("default" "futures"))))))

