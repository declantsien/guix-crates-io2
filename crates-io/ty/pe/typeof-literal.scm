(define-module (crates-io ty pe typeof-literal) #:use-module (crates-io))

(define-public crate-typeof-literal-1.0.0 (c (n "typeof-literal") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1.0.67") (f (quote ("proc-macro"))) (k 0)) (d (n "quote") (r "^1.0.33") (k 0)) (d (n "syn") (r "^2.0.33") (f (quote ("full" "parsing" "proc-macro" "printing"))) (k 0)))) (h "1gkhm44hl6q8qcc74lhba1p4kq35rjbl4j8w23zblgzg7fqwp44r")))

