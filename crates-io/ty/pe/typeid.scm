(define-module (crates-io ty pe typeid) #:use-module (crates-io))

(define-public crate-typeid-0.0.1 (c (n "typeid") (v "0.0.1") (h "12fql7xblh8l7kb648ar05q1bzigkdymvpcqfzz61xczm0san53r")))

(define-public crate-typeid-1.0.0 (c (n "typeid") (v "1.0.0") (h "1ky97g0dwzdhmbcwzy098biqh26vhlc98l5x6zy44yhyk7687785") (r "1.61")))

