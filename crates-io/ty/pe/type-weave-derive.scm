(define-module (crates-io ty pe type-weave-derive) #:use-module (crates-io))

(define-public crate-type-weave-derive-0.1.0 (c (n "type-weave-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "017gmxfa5q2rzpjrj1d3p7ps4dg3d4q37bqnsc5pyxb2m0g78nii")))

(define-public crate-type-weave-derive-0.2.0 (c (n "type-weave-derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0xsnxak9qnp111g080adk2nchhb4w0bk0h7l1bcj1myqcs0y5j5w")))

(define-public crate-type-weave-derive-0.3.0 (c (n "type-weave-derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "139s3y1kjhrl5vxld7v1ca7m35ak5alz8lx2qn6r15v45424n9mi")))

