(define-module (crates-io ty pe type-uuid-derive) #:use-module (crates-io))

(define-public crate-type-uuid-derive-0.1.0 (c (n "type-uuid-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.23") (d #t) (k 0)) (d (n "uuid") (r "^0.7.1") (d #t) (k 0)))) (h "1whr0wwhi6pdmy33q72kflrafbnw3vyjnbcrdi62mbw77dg6p70h")))

(define-public crate-type-uuid-derive-0.1.1 (c (n "type-uuid-derive") (v "0.1.1") (d (list (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.23") (d #t) (k 0)) (d (n "uuid") (r "^0.7.1") (d #t) (k 0)))) (h "0mdkm4gwia51n4smkqmhgqg182n9gpz7wxcybqms96yf515wh7lk")))

(define-public crate-type-uuid-derive-0.1.2 (c (n "type-uuid-derive") (v "0.1.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (k 0)))) (h "1hbdg4bwz87p97gn340q9pasmzwk4h19ddbzrxp7874nazl0w2cf")))

