(define-module (crates-io ty pe type-rules-derive) #:use-module (crates-io))

(define-public crate-type-rules-derive-0.1.0 (c (n "type-rules-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "extra-traits"))) (d #t) (k 0)))) (h "0l0rrhj0ag6p82dm6qx3fjfxri9pcgpbvcnm7hp1nfj1cvjmiq3w") (y #t)))

(define-public crate-type-rules-derive-0.1.1 (c (n "type-rules-derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "extra-traits"))) (d #t) (k 0)))) (h "02j4m3bnj2rq75cfia3hzyiyfi39b08mxqibq7prc181419lmk22")))

(define-public crate-type-rules-derive-0.1.2 (c (n "type-rules-derive") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "extra-traits"))) (d #t) (k 0)))) (h "17dsxj83wlzwqk8n3424fi7iyjcafh4j5p8a4ir6nj4c5njbibwj")))

(define-public crate-type-rules-derive-0.2.0 (c (n "type-rules-derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "extra-traits" "full"))) (d #t) (k 0)))) (h "1g7m866i2j9a22vbrcbxcy8k68cilv94gjk34631c83912g34hsi")))

(define-public crate-type-rules-derive-0.2.1 (c (n "type-rules-derive") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "extra-traits" "full"))) (d #t) (k 0)))) (h "14g0r88cq6mqm3b16n70gwm9ixa81fsdwz9zp5v5pz116sjfnx55")))

(define-public crate-type-rules-derive-0.2.2 (c (n "type-rules-derive") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "extra-traits" "full"))) (d #t) (k 0)))) (h "0wxnz29nc08iy7kcdanm3w1f8x7v130wn6wnwlj5qawmmqz27vbs")))

(define-public crate-type-rules-derive-0.2.3 (c (n "type-rules-derive") (v "0.2.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "extra-traits" "full"))) (d #t) (k 0)))) (h "0j5yf3gcw8ajgiwg5w9lr2qdsgzii782nl61al9gblba9n02q29p")))

