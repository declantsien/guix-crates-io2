(define-module (crates-io ty pe typenum) #:use-module (crates-io))

(define-public crate-typenum-0.0.1 (c (n "typenum") (v "0.0.1") (h "10wy13lvlbab19ichc5l6j9ixsyfjaahm7sr7rpsv3kfibxid70g")))

(define-public crate-typenum-0.0.2 (c (n "typenum") (v "0.0.2") (h "0qknyrywrin5qis1l639sfr2zgzvfgkcp3vgfqmfday1iffffdm4")))

(define-public crate-typenum-0.1.0 (c (n "typenum") (v "0.1.0") (h "1xamihv7nxxnr2a1z4isyi996bahf3x8mz2nv26mv468n7rnv0d4")))

(define-public crate-typenum-0.1.1 (c (n "typenum") (v "0.1.1") (h "1vsa9qnd1sdnmw78rf7j1iv1if626hxm4rb6sq02bmwbhhl06jj5")))

(define-public crate-typenum-1.0.0 (c (n "typenum") (v "1.0.0") (h "07y7am1wzqf4fmv0j8vc9fwyfivlzqb5vq5vmr15liwpx3a6z5bb")))

(define-public crate-typenum-1.0.1 (c (n "typenum") (v "1.0.1") (h "02jwxjmch55qd5vcd1hvbhzffpwxar150gn2ysc9frirqi4mmlyr")))

(define-public crate-typenum-1.0.2 (c (n "typenum") (v "1.0.2") (h "0rb3hvpj4zgbjxlyh508zjkyiky8hw8525wbqa7i2dhlxpxp01k0")))

(define-public crate-typenum-1.1.0 (c (n "typenum") (v "1.1.0") (h "0bl60378m131kjpl6zqa8m4wvzgj4lrmnp46cghq0ym213jv0vs0") (f (quote (("no_std"))))))

(define-public crate-typenum-1.2.0 (c (n "typenum") (v "1.2.0") (h "0ran8rjr1kq97cb4jgiiqmj9av6fq9b2248mw34bklfqsgvjrypi") (f (quote (("no_std"))))))

(define-public crate-typenum-1.3.0 (c (n "typenum") (v "1.3.0") (h "02dkp6l7hwjg8h1j89bncfh8j8spd4zrbbvjs9jlyyqv693llhd1") (f (quote (("no_std"))))))

(define-public crate-typenum-1.3.1 (c (n "typenum") (v "1.3.1") (h "0lc9xdilvq35j14ppxn6pgiqqvn01hmahq326z8g5r46crv1lkjf") (f (quote (("no_std"))))))

(define-public crate-typenum-1.3.2 (c (n "typenum") (v "1.3.2") (h "0pkvm39kpgasf5fq7v337bsywgkxsdipv9wrar3gpvbv0pcidwr0") (f (quote (("no_std"))))))

(define-public crate-typenum-1.4.0 (c (n "typenum") (v "1.4.0") (h "0sdbhsdrg544nvyf33yvgivniswbqkzjx55lv632478jx06795q5") (f (quote (("no_std"))))))

(define-public crate-typenum-1.5.0 (c (n "typenum") (v "1.5.0") (d (list (d (n "clippy") (r "^0.0.97") (o #t) (d #t) (k 0)))) (h "149w0fdi3qabjq8dlgpcp4cyhs4sb82kkx510mpmn4x4l068cm4g") (f (quote (("no_std"))))))

(define-public crate-typenum-1.5.1 (c (n "typenum") (v "1.5.1") (d (list (d (n "clippy") (r "^0.0.97") (o #t) (d #t) (k 0)))) (h "0vf3hplms1v9vc74j8gy3yyxzlvycdxdh10xfb9pg7idzkv8g3sc") (f (quote (("no_std"))))))

(define-public crate-typenum-1.5.2 (c (n "typenum") (v "1.5.2") (d (list (d (n "clippy") (r "^0.0.113") (o #t) (d #t) (k 0)))) (h "054wgk3j9rpg9bl8ccn2ma86r3dcxhwsyy3xhhh3dl9igj2sfhkj") (f (quote (("no_std"))))))

(define-public crate-typenum-1.6.0 (c (n "typenum") (v "1.6.0") (d (list (d (n "clippy") (r "^0.0.114") (o #t) (d #t) (k 0)))) (h "1qm9pf29r43ma4p2ac3hdd3wld43xa3dfhlzrm73i4d3rks7qj37") (f (quote (("no_std"))))))

(define-public crate-typenum-1.7.0 (c (n "typenum") (v "1.7.0") (d (list (d (n "clippy") (r "^0.0.121") (o #t) (d #t) (k 0)))) (h "1a0cmbhk5qhr1mi062x384dhppn9dg2633pr8dp0mm69cdpn3kd6") (f (quote (("no_std"))))))

(define-public crate-typenum-1.8.0 (c (n "typenum") (v "1.8.0") (d (list (d (n "clippy") (r "^0.0.122") (o #t) (d #t) (k 0)))) (h "1xdhixfhljv4z34pk5afc74gc34isa5y53bfhfb68qhaqflhlszh") (f (quote (("no_std"))))))

(define-public crate-typenum-1.9.0 (c (n "typenum") (v "1.9.0") (d (list (d (n "clippy") (r "^0.0.133") (o #t) (d #t) (k 0)))) (h "1ggbc0g0byfa9gg7955f5jvl1257sbwnr0hbg1w3rwqfg339va8k") (f (quote (("no_std") ("i128"))))))

(define-public crate-typenum-1.10.0 (c (n "typenum") (v "1.10.0") (h "0sc1jirllfhdi52z1xv9yqzxzpk6v7vadd13n7wvs1wnjipn6bb1") (f (quote (("strict") ("no_std") ("i128"))))))

(define-public crate-typenum-1.11.0 (c (n "typenum") (v "1.11.0") (h "0w5a6gypsx01pyn9brxbl98y86jp1gvv4l3qrikkfq5j7xj1dwcs") (f (quote (("strict") ("no_std") ("i128"))))))

(define-public crate-typenum-1.11.1 (c (n "typenum") (v "1.11.1") (h "18zs1jbp488jicpzp8bwv64pd605qch8nvvx1qfl9ypqx8bsfd0n") (f (quote (("strict") ("no_std") ("i128"))))))

(define-public crate-typenum-1.11.2 (c (n "typenum") (v "1.11.2") (h "1ybmfpp7j37zmaw50w35wiwx66lbpr0yp1312c0i333b5pz869vd") (f (quote (("strict") ("no_std") ("i128"))))))

(define-public crate-typenum-1.12.0 (c (n "typenum") (v "1.12.0") (h "0cvbksljz61ian21fnn0h51kphl0pwpzb932bv4s0rwy1wh8lg1p") (f (quote (("strict") ("no_std") ("i128") ("force_unix_path_separator"))))))

(define-public crate-typenum-1.13.0 (c (n "typenum") (v "1.13.0") (h "01lbbspn4080yg8wp6y7q3xcqih1c1dmkkx4pwax4z1a9436k7w7") (f (quote (("strict") ("no_std") ("i128") ("force_unix_path_separator"))))))

(define-public crate-typenum-1.14.0 (c (n "typenum") (v "1.14.0") (h "1v2r349b2dr0pknpjk3nkrbi3mhaa3wl7zi7bdbla4zmcni0hdxn") (f (quote (("strict") ("no_std") ("i128") ("force_unix_path_separator"))))))

(define-public crate-typenum-1.15.0 (c (n "typenum") (v "1.15.0") (d (list (d (n "scale-info") (r "^1.0") (o #t) (k 0)))) (h "11yrvz1vd43gqv738yw1v75rzngjbs7iwcgzjy3cq5ywkv2imy6w") (f (quote (("strict") ("scale_info" "scale-info/derive") ("no_std") ("i128") ("force_unix_path_separator"))))))

(define-public crate-typenum-1.16.0 (c (n "typenum") (v "1.16.0") (d (list (d (n "scale-info") (r "^1.0") (o #t) (k 0)))) (h "1fhb9iaqyjn4dzn2vl86kxjhp4xpw5gynczlnqzf4x6rjgpn2ya9") (f (quote (("strict") ("scale_info" "scale-info/derive") ("no_std") ("i128") ("force_unix_path_separator") ("const-generics")))) (r "1.37.0")))

(define-public crate-typenum-1.17.0 (c (n "typenum") (v "1.17.0") (d (list (d (n "scale-info") (r "^1.0") (o #t) (k 0)))) (h "09dqxv69m9lj9zvv6xw5vxaqx15ps0vxyy5myg33i0kbqvq0pzs2") (f (quote (("strict") ("scale_info" "scale-info/derive") ("no_std") ("i128") ("force_unix_path_separator") ("const-generics")))) (r "1.37.0")))

