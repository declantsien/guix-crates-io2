(define-module (crates-io ty pe typetag-impl) #:use-module (crates-io))

(define-public crate-typetag-impl-0.1.0 (c (n "typetag-impl") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "0dymzw7n5a5qhgnhh2jkv9mzwqx4ay4xyfx95c7v2x0kpkzlk02r")))

(define-public crate-typetag-impl-0.1.1 (c (n "typetag-impl") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "1fxnywjzs9lcf1wvnhbjvbr864sgwzd8kn1nyaciglpyh1xb3yzj")))

(define-public crate-typetag-impl-0.1.2 (c (n "typetag-impl") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "0izw5k7s6alydji1d1s47fnwd2jjdghk3cnqsy0yda1x40falswd")))

(define-public crate-typetag-impl-0.1.3 (c (n "typetag-impl") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "1p5n9n6bx89sqp5q7q8d8rja2yylfnln1hyjv55ikv3h21g60jgg")))

(define-public crate-typetag-impl-0.1.4 (c (n "typetag-impl") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "01020077hq6afzf6wkx8fszpnr9s5j7bnph51g7wa3jdkrwx8gxn")))

(define-public crate-typetag-impl-0.1.5 (c (n "typetag-impl") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "05yyds63vww9d4ynqc0z07qs16l0f485vjg6ad0nd0hx7gd2q8yw")))

(define-public crate-typetag-impl-0.1.6 (c (n "typetag-impl") (v "0.1.6") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1ycrkypllxip8wz3c8dc0dx2z26ng6jrpy30a0580yxhhzy6c91z")))

(define-public crate-typetag-impl-0.1.7 (c (n "typetag-impl") (v "0.1.7") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1v0zpw8g65zgccpj4795zz0ib3k6jqcphr3q49vc7hbczqk9cksh")))

(define-public crate-typetag-impl-0.1.8 (c (n "typetag-impl") (v "0.1.8") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "03lw15ad39bgr4m6fmr5b9lb4wapkcfsnfxsbz0362635iw4f0g6")))

(define-public crate-typetag-impl-0.2.0 (c (n "typetag-impl") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0aw52giax4v41jlijmw5zi32makwy4pk78y6cgl899hixgkmx4rc")))

(define-public crate-typetag-impl-0.2.1 (c (n "typetag-impl") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1zar9mcm6k47gx4rbzywqk9gavlkymj1grr2d8mp058qdnqh093n")))

(define-public crate-typetag-impl-0.2.2 (c (n "typetag-impl") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "07ankszpvvx3imc3fv03qhrbxjkkzz1hqpnc383nap80ym9z4jym")))

(define-public crate-typetag-impl-0.2.3 (c (n "typetag-impl") (v "0.2.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1x1nzkpygkrxwizby4x5n7irk7nlam9p4bvx8g9nc20rfkv8ykgx")))

(define-public crate-typetag-impl-0.2.4 (c (n "typetag-impl") (v "0.2.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0ciijgcg5y13r7ayzn0d05xrhanmv2l7dwm782drbd03ch525zg5")))

(define-public crate-typetag-impl-0.2.5 (c (n "typetag-impl") (v "0.2.5") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0bj9h2wq0qpi6c1cpkxdb7bazl4wcxa5rf4k14zfir8d3xhni5cg")))

(define-public crate-typetag-impl-0.2.6 (c (n "typetag-impl") (v "0.2.6") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0cqa832af9q661qsb8g8n9ka144kmkg00xf2qra25p2nb4i5z7xz")))

(define-public crate-typetag-impl-0.2.7 (c (n "typetag-impl") (v "0.2.7") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "079w521ici1i0900hag5qfnjrk9w7wk5d68spfx1fpizrh7vc0dv")))

(define-public crate-typetag-impl-0.2.8 (c (n "typetag-impl") (v "0.2.8") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1rzkq31cgx0q2bpw24p0r8d2pp6vxwhsfdvxgxczq96xrqq1qgic")))

(define-public crate-typetag-impl-0.2.9 (c (n "typetag-impl") (v "0.2.9") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1w9m637468b8hwqi5by2rfjfvincw2v90z8mycv0dgjmsn12mq4a")))

(define-public crate-typetag-impl-0.2.10 (c (n "typetag-impl") (v "0.2.10") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0vy3nh7llln6qjqw1n36jxjj7bv68bl5ry61lfx0rnbi5w1wsdnq")))

(define-public crate-typetag-impl-0.2.11 (c (n "typetag-impl") (v "0.2.11") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1jmd9ljiyhcahnvwyl4yph3gn7w6wpqxxm4lmckd3y003jgkk86b")))

(define-public crate-typetag-impl-0.2.12 (c (n "typetag-impl") (v "0.2.12") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "03q02sqgv9nw962lvzd3z2m7gjl03fy1hrph6sf3gy5l2ml9mi1h")))

(define-public crate-typetag-impl-0.2.13 (c (n "typetag-impl") (v "0.2.13") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "03xlldhpzr1034s5xsdfifb9ni1xyjn4s1x31lh9b9n41m2kvhdz")))

(define-public crate-typetag-impl-0.2.14 (c (n "typetag-impl") (v "0.2.14") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1apkrk9wjgwb2qg8w5265jkii8b5773hf7kv9k21893y2djngsif")))

(define-public crate-typetag-impl-0.2.15 (c (n "typetag-impl") (v "0.2.15") (d (list (d (n "proc-macro2") (r "^1.0.74") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.46") (f (quote ("full"))) (d #t) (k 0)))) (h "141kbnddz4hlppylvqj4pr1y6r3nqv54gq1nsq80r17l3albh799")))

(define-public crate-typetag-impl-0.2.16 (c (n "typetag-impl") (v "0.2.16") (d (list (d (n "proc-macro2") (r "^1.0.74") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.46") (f (quote ("full"))) (d #t) (k 0)))) (h "1cabnvm526bcgh1sh34js5ils0gz4xwlgvwhm992acdr8xzqhwxc")))

