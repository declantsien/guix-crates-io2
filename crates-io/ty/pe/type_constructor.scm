(define-module (crates-io ty pe type_constructor) #:use-module (crates-io))

(define-public crate-type_constructor-0.1.3 (c (n "type_constructor") (v "0.1.3") (d (list (d (n "test_tools") (r "~0.1") (d #t) (k 2)))) (h "1k3hhh6wdhpzih28khzyzw74q28bx84fgxxq4jmklgb4jnfcsxbs")))

(define-public crate-type_constructor-0.1.4 (c (n "type_constructor") (v "0.1.4") (d (list (d (n "test_tools") (r "~0.1") (d #t) (k 2)))) (h "04nfvxa4s1vpd2aa109kw8qcqysd1mrm2r788q7ypncj9mih8ad3")))

(define-public crate-type_constructor-0.1.5 (c (n "type_constructor") (v "0.1.5") (d (list (d (n "test_tools") (r "~0.1") (d #t) (k 2)))) (h "0yafpzwvajgdd95alyxikrq70vzca4gh8vhymmcvmrlm0jfypy47")))

(define-public crate-type_constructor-0.1.6 (c (n "type_constructor") (v "0.1.6") (d (list (d (n "test_tools") (r "~0.1") (d #t) (k 2)))) (h "129li31mdc3gy5jdv2jcz6nxj0hmcaisgc3xgdbnw7f4js08vqy1") (f (quote (("types") ("make") ("default" "make" "types") ("all" "make" "types"))))))

(define-public crate-type_constructor-0.1.7 (c (n "type_constructor") (v "0.1.7") (d (list (d (n "test_tools") (r "~0.1") (d #t) (k 2)))) (h "0dcbvqrmcq5bfyd240ck7yywpcbk8mj7allah6vbxy6k3h7bgqc9") (f (quote (("types") ("make") ("default" "make" "types") ("all" "make" "types"))))))

(define-public crate-type_constructor-0.1.8 (c (n "type_constructor") (v "0.1.8") (d (list (d (n "test_tools") (r "~0.1") (d #t) (k 2)))) (h "0jijrfq43zd1ivmyn12f1bkazr4w6xknlaijwdmfaig2f1bm3471") (f (quote (("types") ("make") ("default" "make" "types") ("all" "make" "types"))))))

(define-public crate-type_constructor-0.1.9 (c (n "type_constructor") (v "0.1.9") (d (list (d (n "test_tools") (r "~0.1") (d #t) (k 2)))) (h "1cb4nqwkjshz1yy3a7ishv1nr1q5a5sbzafqp1zassinxd3n59vy") (f (quote (("make") ("default" "make") ("all" "make"))))))

(define-public crate-type_constructor-0.1.10 (c (n "type_constructor") (v "0.1.10") (d (list (d (n "test_tools") (r "~0.1") (d #t) (k 2)))) (h "0xdip3zb25f22jhwf3s1psir4z6rdni8f730lpzjnv8cn330s97s") (f (quote (("make") ("default" "make") ("all" "make"))))))

(define-public crate-type_constructor-0.1.11 (c (n "type_constructor") (v "0.1.11") (d (list (d (n "test_tools") (r "~0.1") (d #t) (k 2)))) (h "0mbyy7h1nggslfinjhyrb6v0zi8nz7n6wpnhpr493dws5f17rd54") (f (quote (("make") ("default" "make") ("all" "make"))))))

(define-public crate-type_constructor-0.1.12 (c (n "type_constructor") (v "0.1.12") (d (list (d (n "test_tools") (r "~0.1") (d #t) (k 2)))) (h "1dargcdyd9j6pnb8iy901f7na5klb3jp5wy7n71ajhg30myxkzmf") (f (quote (("make") ("default" "make") ("all" "make"))))))

(define-public crate-type_constructor-0.1.13 (c (n "type_constructor") (v "0.1.13") (d (list (d (n "test_tools") (r "~0.1") (d #t) (k 2)))) (h "179zsnvdn8kysaqmvarh5baaln8mxwkmrrkxmncv8qlkhwav3xbi") (f (quote (("make") ("default" "make") ("all" "make"))))))

(define-public crate-type_constructor-0.1.14 (c (n "type_constructor") (v "0.1.14") (d (list (d (n "test_tools") (r "~0.1") (d #t) (k 2)))) (h "18v9ni1v1nb2i3xfqi01c9za0k2k2ygy6v1dp4nfa8h1igphbh92") (f (quote (("use_std") ("use_alloc") ("make") ("full" "use_std" "make") ("default" "use_std" "make"))))))

(define-public crate-type_constructor-0.1.15 (c (n "type_constructor") (v "0.1.15") (d (list (d (n "test_tools") (r "~0.1") (d #t) (k 2)))) (h "0jz9njbkv04kkvxha9avj3z8dv7qnaq2ag7bvy71fmmm39v3c4am") (f (quote (("use_std") ("use_alloc") ("make") ("full" "use_std" "make") ("default" "use_std" "make"))))))

(define-public crate-type_constructor-0.1.16 (c (n "type_constructor") (v "0.1.16") (d (list (d (n "test_tools") (r "~0.1") (d #t) (k 2)))) (h "11ik40p0zx072z213yg2c3nf8ip8h25ck2mc9irja35bsljlxpql") (f (quote (("use_std") ("use_alloc") ("many") ("make") ("full" "use_std" "many" "make") ("default" "use_std" "many" "make"))))))

(define-public crate-type_constructor-0.1.17 (c (n "type_constructor") (v "0.1.17") (d (list (d (n "test_tools") (r "~0.1") (d #t) (k 2)))) (h "026s6j39cjp48n452zg8by750qlxcvk1mc88zyv4g6x3cwvgk1x0") (f (quote (("vectorized_from") ("use_std") ("use_alloc") ("many") ("make") ("full" "use_std" "many" "make" "vectorized_from") ("default" "use_std" "many" "make" "vectorized_from"))))))

(define-public crate-type_constructor-0.1.18 (c (n "type_constructor") (v "0.1.18") (d (list (d (n "test_tools") (r "~0.1") (d #t) (k 2)))) (h "03r4cln50jmkiqyhfcw2n5k1ca5zvq70zy63h51ilvig3f395xgd") (f (quote (("vectorized_from") ("use_std") ("use_alloc") ("many") ("make") ("full" "use_std" "use_alloc" "many" "make" "vectorized_from") ("default" "use_std" "many" "make" "vectorized_from"))))))

(define-public crate-type_constructor-0.1.19 (c (n "type_constructor") (v "0.1.19") (d (list (d (n "test_tools") (r "~0.1") (d #t) (k 2)))) (h "0xm0d1da04vl897l8jbsn5yws373zs2d5mm5jg15zmz71814j3ra") (f (quote (("vectorized_from") ("use_std") ("use_alloc") ("many") ("make") ("full" "use_std" "use_alloc" "many" "make" "vectorized_from") ("default" "use_std" "many" "make" "vectorized_from"))))))

(define-public crate-type_constructor-0.2.0 (c (n "type_constructor") (v "0.2.0") (d (list (d (n "derive_tools") (r "~0.10.0") (f (quote ("enabled" "enabled" "type_variadic_from" "derive_variadic_from"))) (k 0)) (d (n "test_tools") (r "~0.5.0") (d #t) (k 2)))) (h "0m1d3yas5krar3r9bd1avkg0nid0x8rrm37d4lzf2a69cnp7fw58") (f (quote (("vectorized_from") ("use_alloc") ("no_std") ("many") ("make") ("full" "enabled" "use_alloc" "many" "make" "vectorized_from") ("enabled") ("default" "enabled" "many" "make" "vectorized_from"))))))

(define-public crate-type_constructor-0.3.0 (c (n "type_constructor") (v "0.3.0") (d (list (d (n "derive_tools") (r "~0.16.0") (f (quote ("enabled" "enabled" "type_variadic_from" "derive_variadic_from"))) (k 0)) (d (n "test_tools") (r "~0.6.0") (d #t) (k 2)))) (h "0zv9bh9pmwd7mmirg49qmv69lj4cjhnil2rdczwgkdgvqb77ab8b") (f (quote (("vectorized_from") ("use_alloc") ("no_std") ("many") ("make") ("full" "enabled" "use_alloc" "many" "make" "vectorized_from") ("enabled") ("default" "enabled" "many" "make" "vectorized_from"))))))

