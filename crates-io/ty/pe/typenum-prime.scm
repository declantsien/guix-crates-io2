(define-module (crates-io ty pe typenum-prime) #:use-module (crates-io))

(define-public crate-typenum-prime-0.1.0 (c (n "typenum-prime") (v "0.1.0") (d (list (d (n "typenum") (r "^1") (d #t) (k 0)))) (h "03z23c4qfw0vcc7pp6cb9g4v6v5apas8154cjan3p3x68wqwrz0l")))

(define-public crate-typenum-prime-0.2.0 (c (n "typenum-prime") (v "0.2.0") (d (list (d (n "typenum") (r "^1.9") (d #t) (k 0)))) (h "18zrssb5llbid6kaj61zcz6canqx76xjpki7im0jzzprv4f3p3r5")))

