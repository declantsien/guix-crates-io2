(define-module (crates-io ty pe typenum-promote) #:use-module (crates-io))

(define-public crate-typenum-promote-0.1.0 (c (n "typenum-promote") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "typenum") (r "^1.11") (d #t) (k 0)))) (h "0rac7kkl04pk69pq9b3z4i8wwbqqgn4wabw6k2bfn5fkd996my92")))

