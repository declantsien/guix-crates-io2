(define-module (crates-io ty pe type-registry) #:use-module (crates-io))

(define-public crate-type-registry-0.1.0 (c (n "type-registry") (v "0.1.0") (d (list (d (n "generic_static") (r "^0.2.0") (d #t) (k 0)) (d (n "linkme") (r "^0.3.16") (d #t) (k 0)) (d (n "type-registry-register-macro") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "1974jf80iqamybz1yhdj67p4ck44q5h8hc22jbl99a4cf8va1jcj") (f (quote (("macro" "type-registry-register-macro") ("default" "macro"))))))

(define-public crate-type-registry-0.1.1 (c (n "type-registry") (v "0.1.1") (d (list (d (n "generic_static") (r "^0.2.0") (d #t) (k 0)) (d (n "linkme") (r "^0.3.16") (d #t) (k 0)) (d (n "type-registry-register-macro") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "11q1jkc4jw5zizp706mn61chi33far7mbllhyf3jp07xbl8k4hvp") (f (quote (("macro" "type-registry-register-macro") ("default" "macro"))))))

(define-public crate-type-registry-0.2.0 (c (n "type-registry") (v "0.2.0") (d (list (d (n "generic_static") (r "^0.2.0") (d #t) (k 0)) (d (n "linkme") (r "^0.3.16") (d #t) (k 0)) (d (n "type-registry-register-macro") (r "^0.2.0") (o #t) (d #t) (k 0)))) (h "10kapk94372bvsjv9k84msk9xa4xgydnvvrghd0lwqi4n3nyb754") (f (quote (("macro" "type-registry-register-macro") ("default" "macro"))))))

