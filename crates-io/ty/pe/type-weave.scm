(define-module (crates-io ty pe type-weave) #:use-module (crates-io))

(define-public crate-type-weave-0.1.0 (c (n "type-weave") (v "0.1.0") (d (list (d (n "type-weave-derive") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "1azgzinhnn4gp86xa7fwc27ibcfri4lqw176361fnc4gnil1cb9j") (f (quote (("default" "derive")))) (s 2) (e (quote (("derive" "dep:type-weave-derive"))))))

(define-public crate-type-weave-0.2.0 (c (n "type-weave") (v "0.2.0") (d (list (d (n "type-weave-derive") (r "^0.2.0") (o #t) (d #t) (k 0)))) (h "1jk8jazlrq092hi6grq9xhn16mq9shsb91w61jkn4g1qslrd49kc") (f (quote (("default" "derive")))) (y #t) (s 2) (e (quote (("derive" "dep:type-weave-derive"))))))

(define-public crate-type-weave-0.3.0 (c (n "type-weave") (v "0.3.0") (d (list (d (n "type-weave-derive") (r "^0.3.0") (o #t) (d #t) (k 0)))) (h "14dsdg1bfb73zf5ygmilybqzs4x7isnbs1xfslda95ssfvpivbij") (f (quote (("default" "derive")))) (s 2) (e (quote (("derive" "dep:type-weave-derive"))))))

