(define-module (crates-io ty pe typesize-derive) #:use-module (crates-io))

(define-public crate-typesize-derive-0.1.0 (c (n "typesize-derive") (v "0.1.0") (d (list (d (n "either") (r "^1.9.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (d #t) (k 0)))) (h "0q12bz69bppgck23zp49v13f3b6ad2h3bch33hv5zbszqd2jcblm")))

(define-public crate-typesize-derive-0.1.2 (c (n "typesize-derive") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (d #t) (k 0)))) (h "17w40fhakpbz6df9w7a41qlv1lqhcr6w974lywxvh5pxhbay8x2j") (f (quote (("details"))))))

(define-public crate-typesize-derive-0.1.3 (c (n "typesize-derive") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (d #t) (k 0)))) (h "1khfl3v061qsjf1cbpbgxf49c37pj4aa5faix5xlka2v6s2244hb") (f (quote (("details"))))))

(define-public crate-typesize-derive-0.1.7 (c (n "typesize-derive") (v "0.1.7") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (d #t) (k 0)))) (h "1a0ypva9lwmyfgvng2iq87j2gw2ishhm2jbysmmnh9yclk18hplh") (f (quote (("details")))) (r "1.65")))

