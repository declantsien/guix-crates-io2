(define-module (crates-io ty pe typemap_rev) #:use-module (crates-io))

(define-public crate-typemap_rev-0.1.0 (c (n "typemap_rev") (v "0.1.0") (h "1y83spvask10nqm5mcqj7b39v5c9354q7af22jqs9gk64ksaw5n7")))

(define-public crate-typemap_rev-0.1.1 (c (n "typemap_rev") (v "0.1.1") (h "1296my2m4awhwrayn09rcvhv6yr7i2nmpp3cvvy51az07cgsl6ha")))

(define-public crate-typemap_rev-0.1.2 (c (n "typemap_rev") (v "0.1.2") (h "03ycp4gkmd34r7adyb4dih8qk49hxvdw277z7npjwdwvan2s09s2")))

(define-public crate-typemap_rev-0.1.3 (c (n "typemap_rev") (v "0.1.3") (h "1jbag7kq77ffpcy97zq8yix6cm4vgfbl98fgpr38fj118c943387")))

(define-public crate-typemap_rev-0.1.4 (c (n "typemap_rev") (v "0.1.4") (h "07hk3bdz3f2i5zvya8nipmr4nymamwsk7rd3sjgdqfhn292b2prk")))

(define-public crate-typemap_rev-0.1.5 (c (n "typemap_rev") (v "0.1.5") (h "0mg1bljnddkwh8ahy1xq1bgrmc4k8fcvdavr19c58m2blbq78nzd")))

(define-public crate-typemap_rev-0.2.0 (c (n "typemap_rev") (v "0.2.0") (h "1d8ganccl9vg8fvqr32d7bbqc018c02qhbbrv7pryk4912b5xi2g")))

(define-public crate-typemap_rev-0.3.0 (c (n "typemap_rev") (v "0.3.0") (h "161935l8j5jxzjz64g4z21z3x7aj9ljhadjwdbqilf2p2868pc3l")))

