(define-module (crates-io ty pe typed-num) #:use-module (crates-io))

(define-public crate-typed-num-0.1.0 (c (n "typed-num") (v "0.1.0") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "toml") (r "^0.7") (d #t) (k 2)))) (h "0h7mdlwg4akqb7h2czl1wmw41pkfgpjzwkw4zg61kihcrfarvvza")))

(define-public crate-typed-num-0.2.0 (c (n "typed-num") (v "0.2.0") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "toml") (r "^0.7") (d #t) (k 2)))) (h "143w6drah9lwqcv6nhk4vkfh1n5qzqc6vgm06ykh9n312zvwq4qs")))

