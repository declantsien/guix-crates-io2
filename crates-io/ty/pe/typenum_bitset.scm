(define-module (crates-io ty pe typenum_bitset) #:use-module (crates-io))

(define-public crate-typenum_bitset-0.1.0 (c (n "typenum_bitset") (v "0.1.0") (d (list (d (n "clippy") (r "^0.0.175") (o #t) (d #t) (k 0)) (d (n "generic-array") (r "^0.9.0") (d #t) (k 0)) (d (n "typenum") (r "^1.9.0") (d #t) (k 0)))) (h "037v2qkgm136rc0cwhwy08c9bi3x6960pw3klc7nl0wzbv7aijcj") (f (quote (("default"))))))

