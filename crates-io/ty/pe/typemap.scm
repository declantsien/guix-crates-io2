(define-module (crates-io ty pe typemap) #:use-module (crates-io))

(define-public crate-typemap-0.0.0 (c (n "typemap") (v "0.0.0") (d (list (d (n "phantom") (r "> 0.0.0") (d #t) (k 0)) (d (n "unsafe-any") (r "> 0.0.0") (d #t) (k 0)))) (h "0ajgkals3609yajfd9pffikgxif7czr6xj5wk323nbbin2nbqs50")))

(define-public crate-typemap-0.0.1 (c (n "typemap") (v "0.0.1") (d (list (d (n "phantom") (r "> 0.0.0") (d #t) (k 0)) (d (n "unsafe-any") (r "> 0.0.0") (d #t) (k 0)))) (h "0412rnix6klngznglfsv1qh91mczm4hk0qpwzw33chjzq8fvghnd")))

(define-public crate-typemap-0.0.3 (c (n "typemap") (v "0.0.3") (d (list (d (n "phantom") (r "*") (d #t) (k 0)) (d (n "unsafe-any") (r "*") (d #t) (k 0)))) (h "016bzbi2dxyrvbk4if2jqggx6qfjb9yz6i86vl3sm5idd7kbkz5h")))

(define-public crate-typemap-0.0.4 (c (n "typemap") (v "0.0.4") (d (list (d (n "phantom") (r "*") (d #t) (k 0)) (d (n "unsafe-any") (r "*") (d #t) (k 0)))) (h "1n4y337fqmg2y7rlryjzgvlsf4sh78jmm8hl2aadgpdb2s3r3k2s")))

(define-public crate-typemap-0.0.5 (c (n "typemap") (v "0.0.5") (d (list (d (n "phantom") (r "*") (d #t) (k 0)) (d (n "unsafe-any") (r "*") (d #t) (k 0)))) (h "12lk2jdp9sf2r79mdmpqa3sjnsn7x6vhvg784gpy138f29646j4n")))

(define-public crate-typemap-0.0.6 (c (n "typemap") (v "0.0.6") (d (list (d (n "phantom") (r "*") (d #t) (k 0)) (d (n "unsafe-any") (r "*") (d #t) (k 0)))) (h "01mla9rxcbb63zhairky05wdik879qp2fyzgc371k17jbw2v5kkf")))

(define-public crate-typemap-0.0.7 (c (n "typemap") (v "0.0.7") (d (list (d (n "phantom") (r "*") (d #t) (k 0)) (d (n "unsafe-any") (r "*") (d #t) (k 0)))) (h "121d8gidl2180j3zw1zzqskq8mk31fhnbirf614knp7m0a1js8ac")))

(define-public crate-typemap-0.0.8 (c (n "typemap") (v "0.0.8") (d (list (d (n "phantom") (r "*") (d #t) (k 0)) (d (n "unsafe-any") (r "*") (d #t) (k 0)))) (h "0mp8jndcw0rr78dpb289hli1dl0dq66701s9mlh5m34hrad6aj6h")))

(define-public crate-typemap-0.0.9 (c (n "typemap") (v "0.0.9") (d (list (d (n "unsafe-any") (r "*") (d #t) (k 0)))) (h "0p7kaldrd6vbmh9f6c5rfir353f1zvncp49c6wf2bb8yfh3c6yrj")))

(define-public crate-typemap-0.0.10 (c (n "typemap") (v "0.0.10") (d (list (d (n "unsafe-any") (r "*") (d #t) (k 0)))) (h "1la86a1x4y1qm17lfia4zq94bhv32dm43d1gsfqviyl22cm44nf0")))

(define-public crate-typemap-0.0.11 (c (n "typemap") (v "0.0.11") (d (list (d (n "unsafe-any") (r "*") (d #t) (k 0)))) (h "125v3y4nzyslsihhc2vkmcx29clpv0rwk54y4k7qmgvi55ml0i0v")))

(define-public crate-typemap-0.0.12 (c (n "typemap") (v "0.0.12") (d (list (d (n "unsafe-any") (r "*") (d #t) (k 0)))) (h "1jpm5517x5km9jnl57gsk62dlny1q770a7n4gkk252cs2fnm16qk")))

(define-public crate-typemap-0.1.0 (c (n "typemap") (v "0.1.0") (d (list (d (n "unsafe-any") (r "*") (d #t) (k 0)))) (h "02ya35vz445hllmlf8h8xv045g5xyjhbviv86zrx48adanndbb0m")))

(define-public crate-typemap-0.2.0 (c (n "typemap") (v "0.2.0") (d (list (d (n "unsafe-any") (r "*") (d #t) (k 0)))) (h "0ibkxhl9n6hdrhbq8b76l3hb9yl1h4g9mvpdma9h916cll5sapwi")))

(define-public crate-typemap-0.2.1 (c (n "typemap") (v "0.2.1") (d (list (d (n "unsafe-any") (r "*") (d #t) (k 0)))) (h "1lv32yiy53q8y8yhk2zpssfhp48g58zvzarzcpas8lmj3sgl70yc")))

(define-public crate-typemap-0.3.0 (c (n "typemap") (v "0.3.0") (d (list (d (n "unsafe-any") (r "*") (d #t) (k 0)))) (h "1myqi2h0b3hhs74mfd5fnqlfcqq1n6di3399ism04mi55kkpfj5w")))

(define-public crate-typemap-0.3.1 (c (n "typemap") (v "0.3.1") (d (list (d (n "unsafe-any") (r "*") (d #t) (k 0)))) (h "1j5bjlpzkgwip6yf1yb0g29zq2v0yqs7zg4xfvgw0bpq65l6802n")))

(define-public crate-typemap-0.3.2 (c (n "typemap") (v "0.3.2") (d (list (d (n "unsafe-any") (r "*") (d #t) (k 0)))) (h "1diir5x832n25vphg1c325k7hlqdvym2qhjqilqzidhi0irs9lnh")))

(define-public crate-typemap-0.3.3 (c (n "typemap") (v "0.3.3") (d (list (d (n "unsafe-any") (r "*") (d #t) (k 0)))) (h "1xm1gbvz9qisj1l6d36hrl9pw8imr8ngs6qyanjnsad3h0yfcfv5")))

