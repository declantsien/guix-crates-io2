(define-module (crates-io ty pe type_traits) #:use-module (crates-io))

(define-public crate-type_traits-0.1.0 (c (n "type_traits") (v "0.1.0") (h "06s2bi8q7a1yskcgy7wl8jq70p6dpph7kqcys3jfwrxvx7n9519j")))

(define-public crate-type_traits-0.2.0 (c (n "type_traits") (v "0.2.0") (h "12q0ghqbj2lyjs0a79m21m0r23kysm1y3sk71hp4dy7nn9pwxkhw")))

(define-public crate-type_traits-0.3.0 (c (n "type_traits") (v "0.3.0") (h "1ad3pkmc85p65vkywr54myzi2vkpbfpas1vg4iwz3aj28qnwhjpw")))

