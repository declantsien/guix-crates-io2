(define-module (crates-io ty pe type_reflect_macros) #:use-module (crates-io))

(define-public crate-type_reflect_macros-0.1.0 (c (n "type_reflect_macros") (v "0.1.0") (d (list (d (n "Inflector") (r "^0.11") (k 0)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "type_reflect_core") (r "^0.1.0") (d #t) (k 0)))) (h "0xfflb5nwz7vjw70lfsrjq8fm08lbq2gzg8q6461fyd2x5npkaik")))

(define-public crate-type_reflect_macros-0.1.1 (c (n "type_reflect_macros") (v "0.1.1") (d (list (d (n "Inflector") (r "^0.11") (k 0)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "type_reflect_core") (r "^0.1.1") (d #t) (k 0)))) (h "1b8qc83s1q6p5lcwxzrk3q62lbxyc7hk0iiills1bihmszac2jrs")))

(define-public crate-type_reflect_macros-0.1.2 (c (n "type_reflect_macros") (v "0.1.2") (d (list (d (n "Inflector") (r "^0.11") (k 0)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "type_reflect_core") (r "^0.1.2") (d #t) (k 0)))) (h "0ss4jjb531bms75m3rg5w1jvm1j59x4jk1f87vbkabrjhm0rbazr")))

(define-public crate-type_reflect_macros-0.1.3 (c (n "type_reflect_macros") (v "0.1.3") (d (list (d (n "Inflector") (r "^0.11") (k 0)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "type_reflect_core") (r "^0.1.2") (d #t) (k 0)))) (h "0d1xbcj5x63ia6qihc888zcw2b36mivsgrm5i2fizr9fgd4brgv7")))

(define-public crate-type_reflect_macros-0.3.0 (c (n "type_reflect_macros") (v "0.3.0") (d (list (d (n "Inflector") (r "^0.11") (k 0)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "type_reflect_core") (r "^0.3.0") (d #t) (k 0)))) (h "1cjcmcf45llp41rfq64cxnwybnf1g4d7l5mppkjn35g15yg714wi")))

