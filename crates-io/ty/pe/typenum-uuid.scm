(define-module (crates-io ty pe typenum-uuid) #:use-module (crates-io))

(define-public crate-typenum-uuid-0.1.0 (c (n "typenum-uuid") (v "0.1.0") (d (list (d (n "typenum") (r "^1.12.0") (f (quote ("i128"))) (d #t) (k 2)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "14iw40x0d7kykvral8gy3m401pjb1a2qlw5r3hd6jddq9wcly71g")))

