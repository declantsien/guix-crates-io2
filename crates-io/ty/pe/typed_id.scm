(define-module (crates-io ty pe typed_id) #:use-module (crates-io))

(define-public crate-typed_id-0.1.0 (c (n "typed_id") (v "0.1.0") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1qgwmc5jbmcbmww0jdvblv8wd802vi2r4bfl7pwh0qblv78ww939")))

