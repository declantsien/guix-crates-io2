(define-module (crates-io ty pe type-census) #:use-module (crates-io))

(define-public crate-type-census-0.1.0 (c (n "type-census") (v "0.1.0") (d (list (d (n "dashmap") (r "^5.2.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)))) (h "023d2fzsrqg22nrx6kl3564bdqf9yins5r84fmrm19nkqf7r01zc")))

(define-public crate-type-census-0.2.0 (c (n "type-census") (v "0.2.0") (h "0lgxixkn5yw8gvdb79b1gaxm8dfq3rhx98sbxhawccw5gpb7ll28")))

(define-public crate-type-census-0.2.1 (c (n "type-census") (v "0.2.1") (h "0npb78j5bf75iajqv4y0q41xrf0cysb7gkshlydd76c0sxjzb49j")))

(define-public crate-type-census-0.2.2 (c (n "type-census") (v "0.2.2") (h "1cr8fg71d5vh5wnm4jhaz59iyi9caplfrxrf897lkp2knwzipgcn")))

(define-public crate-type-census-0.3.0 (c (n "type-census") (v "0.3.0") (d (list (d (n "crossbeam-utils") (r "^0.8.8") (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "type-census-derive") (r "^0.1.0") (d #t) (k 0)))) (h "1vhysklknl7yfispk9zvzlrmjvkl2q9z7vgzzv3yny86snfqfhzc")))

(define-public crate-type-census-0.3.1 (c (n "type-census") (v "0.3.1") (d (list (d (n "crossbeam-utils") (r "^0.8.8") (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "type-census-derive") (r "^0.1.1") (d #t) (k 0)))) (h "1mc2z7wibmqgh6qiawiaqwz755l2vprrj2xkrbxzdiv7nsymm79g")))

