(define-module (crates-io ty pe typerat) #:use-module (crates-io))

(define-public crate-typerat-0.0.1 (c (n "typerat") (v "0.0.1") (d (list (d (n "char-buf") (r "^0.1") (d #t) (k 2)) (d (n "typenum") (r "^1.17") (d #t) (k 0)))) (h "1cqx3dmygzcp3glzsvf6y2hfah9ymqzdwnw3sp3s19g7qny95ffr")))

(define-public crate-typerat-0.0.2 (c (n "typerat") (v "0.0.2") (d (list (d (n "char-buf") (r "^0.1") (d #t) (k 2)) (d (n "typenum") (r "^1.17") (d #t) (k 0)))) (h "0ckq4vmljz2cyj8gj47bi4qy4hkknani5gmar65a7z3ip89ybl4i")))

(define-public crate-typerat-0.0.3 (c (n "typerat") (v "0.0.3") (d (list (d (n "char-buf") (r "^0.1") (d #t) (k 2)) (d (n "typenum") (r "^1.17") (d #t) (k 0)))) (h "0d71xaqvq1c1003vh50rq0vg8sv4dav0v94rfp5l0wrwmv1vkpmy")))

(define-public crate-typerat-0.0.4 (c (n "typerat") (v "0.0.4") (d (list (d (n "char-buf") (r "^0.1") (d #t) (k 2)) (d (n "typenum") (r "^1.17") (d #t) (k 0)))) (h "0ypnm34xx75mr3kqfpv2jprryk1i3xm9a0fnwb8hs07jyfn8zc80")))

