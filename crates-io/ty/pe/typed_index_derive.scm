(define-module (crates-io ty pe typed_index_derive) #:use-module (crates-io))

(define-public crate-typed_index_derive-0.1.1 (c (n "typed_index_derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.14") (d #t) (k 0)))) (h "1vnz5ffjfi39sr00qiwkfsmapwzhjkaz0p80r24l2y1zi0kxi3n3")))

(define-public crate-typed_index_derive-0.1.2 (c (n "typed_index_derive") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.14") (d #t) (k 0)))) (h "1vx1d3r29xnc6k010zbyivvn4qnyw3i2k797c1cwgd7j0gmdxdbf")))

(define-public crate-typed_index_derive-0.1.3 (c (n "typed_index_derive") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.14") (d #t) (k 0)))) (h "1awylqp0sl2vqf0490x9fjxy92hchi5glkypv1dzwia9s220nm22") (y #t)))

(define-public crate-typed_index_derive-0.1.4 (c (n "typed_index_derive") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.14") (d #t) (k 0)))) (h "1sqccwm5004i8jmdr1b5vkw1kbyfs8cmk7254rp35pddaka4dx1a")))

