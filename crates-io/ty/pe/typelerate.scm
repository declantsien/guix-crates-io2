(define-module (crates-io ty pe typelerate) #:use-module (crates-io))

(define-public crate-typelerate-0.1.0-alpha.1 (c (n "typelerate") (v "0.1.0-alpha.1") (d (list (d (n "clap") (r "^4.3.19") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.27") (d #t) (k 0)) (d (n "csv") (r "^1.2.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "ratatui") (r "^0.22.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.183") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.6") (d #t) (k 0)) (d (n "xdg") (r "^2.5.2") (d #t) (k 0)))) (h "1ylf53bk39z06h6hhi3nim0d6rlxv7vib8ya9lbk943n3isjcyk1") (f (quote (("devel"))))))

