(define-module (crates-io ty pe typelayout) #:use-module (crates-io))

(define-public crate-typelayout-0.1.0 (c (n "typelayout") (v "0.1.0") (d (list (d (n "frunk") (r "^0.3.0") (d #t) (k 0)) (d (n "frunk_core") (r "^0.3.0") (d #t) (k 0)) (d (n "typenum") (r "^1.10.0") (d #t) (k 0)))) (h "05bf0pdvn37073fdmnk4vi005hvw1a6799kn9l65qf1alkc40h5b")))

