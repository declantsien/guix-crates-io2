(define-module (crates-io ty pe typed-path) #:use-module (crates-io))

(define-public crate-typed-path-0.1.0 (c (n "typed-path") (v "0.1.0") (h "0ph7zxayh3zchq4zf750238iy9jl5rm8m7g9gjz774ipxzkby0q3")))

(define-public crate-typed-path-0.2.0 (c (n "typed-path") (v "0.2.0") (h "0f0lm1p7nq4ljybhkm32sr4s3psg6cf5wrv94ng1vh8wvj10gyn9")))

(define-public crate-typed-path-0.2.1 (c (n "typed-path") (v "0.2.1") (h "16qhw2jjnhgz5n5d5sc2y2znawqipbsz2qkkrl9j250jq1lscba7")))

(define-public crate-typed-path-0.3.0 (c (n "typed-path") (v "0.3.0") (h "0q4i2alqnkail95sbzfwpac5qjybh6fcbwqaawkfc012s3qzkhzv")))

(define-public crate-typed-path-0.3.1 (c (n "typed-path") (v "0.3.1") (h "1mspbldq991yl69kzjwb0ad9ja8qxclwlpy9c6iiydsa6012rq8n")))

(define-public crate-typed-path-0.3.2 (c (n "typed-path") (v "0.3.2") (h "0kj8c61wg95b7s5bwmv4mljzcx69pz5c482qh0g1x9ci56ddn9jb")))

(define-public crate-typed-path-0.4.0 (c (n "typed-path") (v "0.4.0") (h "17qp12fpc9v35pv1r1cd3zd33j0wvl4sdlvhphrl5bd40z7svffy")))

(define-public crate-typed-path-0.4.1 (c (n "typed-path") (v "0.4.1") (h "06qn1y7dx1sn9zhj4nfhwsjchzzsa8zlyz9mjl0rffxq4hlc8fbn")))

(define-public crate-typed-path-0.4.2 (c (n "typed-path") (v "0.4.2") (h "0pprjih62hzrl5bwr7ynq89spsp7qhdp55zqgchyb41k15j1ipp2")))

(define-public crate-typed-path-0.5.0 (c (n "typed-path") (v "0.5.0") (h "094wrwzfqc78k7sln54p2kccszy74zh62986jdkvnljx90g7c5y1")))

(define-public crate-typed-path-0.6.0 (c (n "typed-path") (v "0.6.0") (h "10d77f2nf53s4b29l0n6vi1hlhd6j0mhi44r3zr4z2a2h8xx3fdv")))

(define-public crate-typed-path-0.7.0 (c (n "typed-path") (v "0.7.0") (h "06v4z9ai4693ci6ki9l46iy5jz6cw58rfibnmgnpidns11hp542a") (f (quote (("std") ("default" "std"))))))

(define-public crate-typed-path-0.7.1 (c (n "typed-path") (v "0.7.1") (h "02jhvk9r4sy50wmzamw5n86m1zc0qablz4zqyr3pcs1cgich9136") (f (quote (("std") ("default" "std"))))))

(define-public crate-typed-path-0.8.0 (c (n "typed-path") (v "0.8.0") (h "0hvvyaycraq5drlpil4617igrz4n564fhrx0bzzx87r43p6f4sb0") (f (quote (("std") ("default" "std"))))))

