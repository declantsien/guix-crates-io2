(define-module (crates-io ty pe type-name-derive) #:use-module (crates-io))

(define-public crate-type-name-derive-0.1.0 (c (n "type-name-derive") (v "0.1.0") (d (list (d (n "darling") (r "^0.13.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "00gnc7rb1a88a9f1jbbm2pna7ajzhvqyv73sribzmval5zqhn8n8")))

