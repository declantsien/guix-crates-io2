(define-module (crates-io ty pe typed-builder-macro) #:use-module (crates-io))

(define-public crate-typed-builder-macro-0.15.0 (c (n "typed-builder-macro") (v "0.15.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "09n5f5f2y3d9vrk21a2f5b14kpqac2rwvig8a9jkyg2cspjhh8cm")))

(define-public crate-typed-builder-macro-0.15.1 (c (n "typed-builder-macro") (v "0.15.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "10z0ssm7ph8asvjmdm5wwna0frnlr57q5jg7xm1fpjvwkacjxfkd")))

(define-public crate-typed-builder-macro-0.15.2 (c (n "typed-builder-macro") (v "0.15.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1qncxrwrhsznqyk5b8bkcvq0wmil8k1av60z04ykxcfh84f1b8r9")))

(define-public crate-typed-builder-macro-0.16.0 (c (n "typed-builder-macro") (v "0.16.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0w9q1ddl6pyc6g1l58hyzdqdrsrhix4ww86cml9a1458ys26hsia")))

(define-public crate-typed-builder-macro-0.16.1 (c (n "typed-builder-macro") (v "0.16.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1wn2vag45va05kbr82hwkscyr3lv7jlpr2gzin7zlfbn10ic3vfq")))

(define-public crate-typed-builder-macro-0.16.2 (c (n "typed-builder-macro") (v "0.16.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0vp94vzcnrqlz93swkai13w9dmklpdh2c2800zpjnvi0735s8g7h")))

(define-public crate-typed-builder-macro-0.17.0 (c (n "typed-builder-macro") (v "0.17.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1vkl2sri6lhw9vca40vd5mvpcdmkscgisbqxcdyihii3bvp5984g")))

(define-public crate-typed-builder-macro-0.18.0 (c (n "typed-builder-macro") (v "0.18.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "14mlf9733bmdf6bqzmhrzc1hbxfazp0mxvs751wckdaifccy8blq")))

(define-public crate-typed-builder-macro-0.18.1 (c (n "typed-builder-macro") (v "0.18.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0lhk4qsj9yrqkprv9wf0wdyalvl9cv7dwszkms05djcf4f43nfsn")))

(define-public crate-typed-builder-macro-0.18.2 (c (n "typed-builder-macro") (v "0.18.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0qwfq0q2lkg4bkmcpsqajy3ss2sb2h47dj5zhfwvbp27ygx8sw8z")))

