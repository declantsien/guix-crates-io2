(define-module (crates-io ty pe type-info-derive) #:use-module (crates-io))

(define-public crate-type-info-derive-0.1.0 (c (n "type-info-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.3.6") (d #t) (k 0)) (d (n "quote") (r "^0.5.1") (d #t) (k 0)) (d (n "syn") (r "^0.13.1") (d #t) (k 0)) (d (n "syn") (r "^0.13.1") (f (quote ("full"))) (d #t) (k 2)) (d (n "type-info") (r "^0.1.0") (d #t) (k 0)))) (h "07s83mzk8hd9vljpyh1r1xwnrgv2ji67nvqb0380zr530y0i7cw4") (f (quote (("nightly" "proc-macro2/nightly"))))))

(define-public crate-type-info-derive-0.2.0 (c (n "type-info-derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^0.3.6") (d #t) (k 0)) (d (n "quote") (r "^0.5.1") (d #t) (k 0)) (d (n "syn") (r "^0.13.1") (d #t) (k 0)) (d (n "syn") (r "^0.13.1") (f (quote ("full"))) (d #t) (k 2)) (d (n "tempfile") (r "^3.0.1") (d #t) (k 2)) (d (n "type-info") (r "^0.2.0") (d #t) (k 0)))) (h "151wfrc82wrg26gfwx9z08zyqp92wm8fqjdgq5bzlpczgggbx05s") (f (quote (("nightly" "proc-macro2/nightly"))))))

