(define-module (crates-io ty pe type-sitter-lib) #:use-module (crates-io))

(define-public crate-type-sitter-lib-0.1.0 (c (n "type-sitter-lib") (v "0.1.0") (d (list (d (n "tree-sitter") (r "^0.20.10") (d #t) (k 0)) (d (n "tree-sitter-rust") (r "^0.20.3") (d #t) (k 2)))) (h "1c87xycwysryhlrb4vr6dmw9mlybs9ich3zhgpcb6q77nqd6bhr3") (f (quote (("tree-sitter-wrapper"))))))

(define-public crate-type-sitter-lib-0.1.1 (c (n "type-sitter-lib") (v "0.1.1") (d (list (d (n "tree-sitter") (r "^0.20.10") (d #t) (k 0)) (d (n "tree-sitter-rust") (r "^0.20.3") (d #t) (k 2)))) (h "06xrh8ynfrwp16g3gysqrcpsa8mncj3212b72xfzdiwi65p4ancf") (f (quote (("tree-sitter-wrapper"))))))

(define-public crate-type-sitter-lib-0.2.0 (c (n "type-sitter-lib") (v "0.2.0") (d (list (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "streaming-iterator") (r "^0.1.9") (d #t) (k 0)) (d (n "tree-sitter") (r "^0.20.10") (d #t) (k 0)) (d (n "tree-sitter-json") (r "^0.19.0") (d #t) (k 2)) (d (n "tree-sitter-rust") (r "^0.20.3") (d #t) (k 2)))) (h "0sx9897af0462q61nmsrcqy03l0s8h5dykykjm0zr685crcr1y51") (f (quote (("tree-sitter-wrapper") ("default" "tree-sitter-wrapper"))))))

(define-public crate-type-sitter-lib-0.3.0 (c (n "type-sitter-lib") (v "0.3.0") (d (list (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "streaming-iterator") (r "^0.1.9") (d #t) (k 0)) (d (n "tree-sitter") (r "^0.20.10") (d #t) (k 0)) (d (n "tree-sitter-json") (r "^0.19.0") (d #t) (k 2)) (d (n "tree-sitter-rust") (r "^0.20.3") (d #t) (k 2)) (d (n "yak-sitter") (r "^0.3.0") (o #t) (d #t) (k 0)))) (h "09yly8dza3r3qw6397grs1wf82ybwyiv32r2m9xqh6k52g5ynhgk") (s 2) (e (quote (("yak-sitter" "dep:yak-sitter"))))))

(define-public crate-type-sitter-lib-0.3.1 (c (n "type-sitter-lib") (v "0.3.1") (d (list (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "streaming-iterator") (r "^0.1.9") (d #t) (k 0)) (d (n "tree-sitter") (r "^0.22.2") (d #t) (k 0)) (d (n "tree-sitter-json") (r "^0.20.2") (d #t) (k 2)) (d (n "tree-sitter-rust") (r "^0.20.4") (d #t) (k 2)) (d (n "yak-sitter") (r "^0.3.1") (o #t) (d #t) (k 0)))) (h "0qi15q20r7b31mmjmrjgr2n5wp22x9gqwhmgh26aw4gd9hs5g5ir") (y #t) (s 2) (e (quote (("yak-sitter" "dep:yak-sitter"))))))

(define-public crate-type-sitter-lib-0.4.0 (c (n "type-sitter-lib") (v "0.4.0") (d (list (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "streaming-iterator") (r "^0.1.9") (d #t) (k 0)) (d (n "tree-sitter") (r "^0.22.2") (d #t) (k 0)) (d (n "tree-sitter-json") (r "^0.20.2") (d #t) (k 2)) (d (n "tree-sitter-rust") (r "^0.20.4") (d #t) (k 2)) (d (n "yak-sitter") (r "^0.4.0") (o #t) (d #t) (k 0)))) (h "0c9pap1my2mxxxmbawxgk3gigwy7vk03qzv04qmikdczn6imdbsr") (s 2) (e (quote (("yak-sitter" "dep:yak-sitter"))))))

