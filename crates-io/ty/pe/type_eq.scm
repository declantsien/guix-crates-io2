(define-module (crates-io ty pe type_eq) #:use-module (crates-io))

(define-public crate-type_eq-0.1.0 (c (n "type_eq") (v "0.1.0") (h "0ykzfqq49122ad66m5irwn8hwnzpc39rxsn566vnl8a37la3vzfs")))

(define-public crate-type_eq-0.1.1 (c (n "type_eq") (v "0.1.1") (h "16jy30pywi029qsn0wyqg4dc2ay7274vvs9pm6xsrslmqbaikfgm")))

(define-public crate-type_eq-0.1.2 (c (n "type_eq") (v "0.1.2") (h "1mh8z30svxlj8zv04nhyspyyq2xblagidnhbj040ifxskv3j3vd6")))

