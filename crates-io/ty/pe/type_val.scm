(define-module (crates-io ty pe type_val) #:use-module (crates-io))

(define-public crate-type_val-0.1.0 (c (n "type_val") (v "0.1.0") (h "1gr3w9j3m2l0cfhdpmmf5d22ksp9y9z241d6433sbr3zm6fpai3n")))

(define-public crate-type_val-0.1.1 (c (n "type_val") (v "0.1.1") (h "1k308r362h855vmrqznjwg2pg8nmmlri45171hni3m329ybx70aj")))

(define-public crate-type_val-0.2.1 (c (n "type_val") (v "0.2.1") (h "07zsfz8lm7ypbbz5h9bin2k4i7y56hv89a7lv4k50w9f7klsz0r4")))

(define-public crate-type_val-0.3.1 (c (n "type_val") (v "0.3.1") (h "0cg9q3mmvyah5j438cdawm9hhb42hap3garb361r1galwlmradwx")))

(define-public crate-type_val-0.3.2 (c (n "type_val") (v "0.3.2") (h "1c794gjw3xl0xqh90i8xv32r96zq7icwbvsjcj2kgbfmdzfcmp5c")))

(define-public crate-type_val-0.3.3 (c (n "type_val") (v "0.3.3") (h "1gh8hmqhr0g2y5hyd2gmj53mnfvji1r94pq2y2j3jcppiqcny5h5")))

