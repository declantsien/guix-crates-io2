(define-module (crates-io ty pe typestat) #:use-module (crates-io))

(define-public crate-typestat-0.1.0 (c (n "typestat") (v "0.1.0") (h "195kcxn3w2px85abdnbgd4ngh1vnk61vn5q6mx7816byjlzz78dk")))

(define-public crate-typestat-0.1.1 (c (n "typestat") (v "0.1.1") (h "1nwdwhgch272z8av2qnw9np98mkpjkcwx7cyyzhp7k93pwn0qf87")))

