(define-module (crates-io ty pe type-operators) #:use-module (crates-io))

(define-public crate-type-operators-0.1.0 (c (n "type-operators") (v "0.1.0") (h "1iz97nar2c9bg99dra3pgd02qifp97yzb7fw7m9f48f8ggxidacw")))

(define-public crate-type-operators-0.1.1 (c (n "type-operators") (v "0.1.1") (h "02jr3m2ydlgwmcd7995zshnrpxvkacf4sq43d4lhbpihsyr36dv2")))

(define-public crate-type-operators-0.1.2 (c (n "type-operators") (v "0.1.2") (h "11hwijlv8nwjgvhcqd1cydkg1wqg8k1k5kbfbmsipdskxxlvzcwf") (f (quote (("specialization") ("nightly" "specialization") ("default"))))))

(define-public crate-type-operators-0.1.3 (c (n "type-operators") (v "0.1.3") (h "18b93hzvk7b7sffk9vqxnxa9lwf7kx2hfyxbapnwji1l8i522401") (f (quote (("specialization") ("nightly" "specialization") ("default"))))))

(define-public crate-type-operators-0.2.0 (c (n "type-operators") (v "0.2.0") (h "0gxq47xizd0vapljdiprm2fxa45q3kg4w173s9ka1qfjzfw0n36y") (f (quote (("specialization") ("nightly" "specialization") ("default")))) (y #t)))

(define-public crate-type-operators-0.2.1 (c (n "type-operators") (v "0.2.1") (h "1bdirb1nw46wdav25rm3rybd34z1m5nzl2pg3lnrk5pak3yd5vln") (f (quote (("specialization") ("nightly" "specialization") ("default"))))))

(define-public crate-type-operators-0.3.0 (c (n "type-operators") (v "0.3.0") (h "0njs299p4b9290clh0k1vvnr4j21vclikrqx8lh83k6a7rw15sxn") (f (quote (("specialization") ("nightly" "specialization") ("default"))))))

(define-public crate-type-operators-0.3.1 (c (n "type-operators") (v "0.3.1") (h "0f7jvpqfb5nrpgdj1yrj7ppxxygx4817sgh7z71lmrjcnmd65zv2") (f (quote (("specialization") ("nightly" "specialization") ("default")))) (y #t)))

(define-public crate-type-operators-0.3.2 (c (n "type-operators") (v "0.3.2") (h "057zrnx8lhv3cppfmzmsqmjpjqd4w1lq1iml1rp76vffz8vpsz25") (f (quote (("specialization") ("nightly" "specialization") ("default"))))))

(define-public crate-type-operators-0.3.3 (c (n "type-operators") (v "0.3.3") (h "1w9mbcrwq62a8wxa5v4hr0jn1a2da6krka1ly2s7zvgx2gdvx3f4") (f (quote (("specialization") ("nightly" "specialization") ("default")))) (y #t)))

(define-public crate-type-operators-0.3.4 (c (n "type-operators") (v "0.3.4") (h "1mvhkj2icvihgkh201gffz3m76zzawm53gwml1gx633hli272g4i") (f (quote (("specialization") ("nightly" "specialization") ("default"))))))

(define-public crate-type-operators-0.3.5 (c (n "type-operators") (v "0.3.5") (d (list (d (n "bit-vec") (r "^0.4.3") (d #t) (k 2)))) (h "0v333mlzdcz515l3qbqwh9rdn8pksb0dz26r09ls2zraf0pvcf70") (f (quote (("specialization") ("nightly" "specialization") ("default"))))))

