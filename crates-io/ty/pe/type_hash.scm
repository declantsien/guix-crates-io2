(define-module (crates-io ty pe type_hash) #:use-module (crates-io))

(define-public crate-type_hash-0.1.0 (c (n "type_hash") (v "0.1.0") (d (list (d (n "type_hash_core") (r "=0.1.0") (d #t) (k 0)) (d (n "type_hash_macros") (r "=0.1.0") (d #t) (k 0)))) (h "1zzfkpcifmivy60cvhw0l2pxzsic2s8acm04cbpaw7p73sg4ka6v")))

(define-public crate-type_hash-0.2.0 (c (n "type_hash") (v "0.2.0") (d (list (d (n "type_hash_core") (r "=0.2.0") (d #t) (k 0)) (d (n "type_hash_macros") (r "=0.2.0") (d #t) (k 0)))) (h "1qnsk8nklbza3914fmrcc9nsyd0rfxgzj83r23ijh0ad8mahl2hf")))

(define-public crate-type_hash-0.2.1 (c (n "type_hash") (v "0.2.1") (d (list (d (n "type_hash_core") (r "=0.2.0") (d #t) (k 0)) (d (n "type_hash_macros") (r "=0.2.1") (d #t) (k 0)))) (h "0gf59hksximnlr02xvr1ryiv86q8xpz3834iqnpq57hdr85bmrb0")))

(define-public crate-type_hash-0.2.2 (c (n "type_hash") (v "0.2.2") (d (list (d (n "type_hash_core") (r "=0.2.0") (d #t) (k 0)) (d (n "type_hash_macros") (r "=0.2.2") (d #t) (k 0)))) (h "076wl47yp1292drmflgbjblqgv03nxrph695ndl6yas97cjx2g30")))

(define-public crate-type_hash-0.3.0 (c (n "type_hash") (v "0.3.0") (d (list (d (n "type_hash_core") (r "=0.2.0") (d #t) (k 0)) (d (n "type_hash_macros") (r "=0.3.0") (d #t) (k 0)))) (h "1impn39cmg1vb38h1w2gjhihnv3k4p5n731mg7ix74hry546zj03")))

