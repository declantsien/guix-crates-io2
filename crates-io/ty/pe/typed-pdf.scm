(define-module (crates-io ty pe typed-pdf) #:use-module (crates-io))

(define-public crate-typed-pdf-0.1.0 (c (n "typed-pdf") (v "0.1.0") (d (list (d (n "itertools") (r "0.10.*") (d #t) (k 0)) (d (n "pdf") (r "0.7.*") (d #t) (k 0)) (d (n "structopt") (r "0.3.*") (d #t) (k 0)))) (h "11qxkypk0plvq1sjz5jdp6nrq1rak57ics5cfg8ikiqbgxq4yj0f")))

