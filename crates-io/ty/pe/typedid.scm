(define-module (crates-io ty pe typedid) #:use-module (crates-io))

(define-public crate-typedid-1.0.0 (c (n "typedid") (v "1.0.0") (d (list (d (n "data-encoding") (r "^2.4.0") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "smol_str") (r "^0.2.0") (d #t) (k 0)) (d (n "uuid") (r "^1.4.0") (f (quote ("v7"))) (d #t) (k 0)))) (h "1vir3sp58lvzin428883yb30qa6ynbcgjzxm2vq5s66835zdsvvk") (f (quote (("default" "std")))) (y #t) (s 2) (e (quote (("std" "uuid/std" "smol_str/std" "serde?/std"))))))

