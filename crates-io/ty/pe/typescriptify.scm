(define-module (crates-io ty pe typescriptify) #:use-module (crates-io))

(define-public crate-typescriptify-0.1.0 (c (n "typescriptify") (v "0.1.0") (d (list (d (n "typescriptify-derive") (r "^0.1") (d #t) (k 0)))) (h "0bn7niibra91n2w7rin1vymmh08k2a501rfy85z67i6ms0708fsy")))

(define-public crate-typescriptify-0.1.1 (c (n "typescriptify") (v "0.1.1") (d (list (d (n "typescriptify-derive") (r "^0.1") (d #t) (k 0)))) (h "0l5m8ipx7k156abzma0c4db95zf6k0ga0ply1krk07x313bg7412")))

(define-public crate-typescriptify-0.1.2 (c (n "typescriptify") (v "0.1.2") (d (list (d (n "typescriptify-derive") (r "^0.1") (d #t) (k 0)))) (h "0i8syirk82l54gy8hz5wyir0c03q3pq78s54jprjz3s7x9wzr2h1")))

(define-public crate-typescriptify-0.1.3 (c (n "typescriptify") (v "0.1.3") (d (list (d (n "typescriptify-derive") (r "^0.1") (d #t) (k 0)))) (h "0h7a3c0xb1his0zlbkdflxyzqj30wnxxhrzy71dx0jw82l4dxw8v")))

(define-public crate-typescriptify-0.1.4 (c (n "typescriptify") (v "0.1.4") (d (list (d (n "typescriptify-derive") (r "^0.1") (d #t) (k 0)))) (h "13j85g8wz10wf0bmzg3y5alxybbr4xamwks6miq06id94r99d7n0")))

(define-public crate-typescriptify-0.1.5 (c (n "typescriptify") (v "0.1.5") (d (list (d (n "typescriptify-derive") (r "^0.1") (d #t) (k 0)))) (h "0qcrw9np8wwwzcdlzhgv263plf9yv9ar80vaypg3jh75s3pyiqay")))

