(define-module (crates-io ty pe typed-sled) #:use-module (crates-io))

(define-public crate-typed-sled-0.1.0 (c (n "typed-sled") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.51") (d #t) (k 0)) (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "sled") (r "^0.34.6") (d #t) (k 0)))) (h "13wakp4rk1xihw3klym04c334i0f6rh0pa1xxqcblw6gpznwsk95")))

(define-public crate-typed-sled-0.1.1 (c (n "typed-sled") (v "0.1.1") (d (list (d (n "async-trait") (r "^0.1.51") (d #t) (k 0)) (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "sled") (r "^0.34.6") (d #t) (k 0)))) (h "1pgyjfypcly3asqx8lzpid3b6v197m303ygb512g20h88y3c23bg")))

(define-public crate-typed-sled-0.1.2 (c (n "typed-sled") (v "0.1.2") (d (list (d (n "async-trait") (r "^0.1.51") (d #t) (k 0)) (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sled") (r "^0.34.6") (d #t) (k 0)))) (h "1bnbcmx33w0nyp8vhl0md7q0qjhi6lpwq2vax4nap7n6fgpydcjw")))

(define-public crate-typed-sled-0.1.3 (c (n "typed-sled") (v "0.1.3") (d (list (d (n "async-trait") (r "^0.1.51") (d #t) (k 0)) (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sled") (r "^0.34.6") (d #t) (k 0)))) (h "1a457x9n44cc80z05pwg9zmg90qwm1crl72wx2i7xr5nym7961vs")))

(define-public crate-typed-sled-0.1.4 (c (n "typed-sled") (v "0.1.4") (d (list (d (n "async-trait") (r "^0.1.51") (d #t) (k 0)) (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sled") (r "^0.34.6") (d #t) (k 0)))) (h "1x5ncy8lm5x7yb5azkr4a223kli7rh8rmi1z3sczzl3xv2mqvryn")))

(define-public crate-typed-sled-0.1.5 (c (n "typed-sled") (v "0.1.5") (d (list (d (n "async-trait") (r "^0.1.51") (d #t) (k 0)) (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sled") (r "^0.34.6") (d #t) (k 0)))) (h "11ihyrhbyplp2j2672kzxl56wv11p8vikmqffzmd5ndg2xwwlpnb")))

(define-public crate-typed-sled-0.1.6 (c (n "typed-sled") (v "0.1.6") (d (list (d (n "async-trait") (r "^0.1.51") (d #t) (k 0)) (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sled") (r "^0.34.6") (d #t) (k 0)))) (h "1zcs23vy9r53jl0c82igmr5w9ywazx0c7n9gxgqlam7ff8xsariy")))

(define-public crate-typed-sled-0.1.7 (c (n "typed-sled") (v "0.1.7") (d (list (d (n "async-trait") (r "^0.1.51") (d #t) (k 0)) (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sled") (r "^0.34.6") (d #t) (k 0)))) (h "093b4as099274cizv6dmi1xf9j1vwpxc90474498q5p7h5gj03fm")))

(define-public crate-typed-sled-0.1.8 (c (n "typed-sled") (v "0.1.8") (d (list (d (n "async-trait") (r "^0.1.51") (d #t) (k 0)) (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sled") (r "^0.34.6") (d #t) (k 0)))) (h "10pzy3gnviddfvjw30gk7p79wz10i9l4hn6yhrqqs63kkkb997sc")))

(define-public crate-typed-sled-0.1.9 (c (n "typed-sled") (v "0.1.9") (d (list (d (n "async-trait") (r "^0.1.51") (d #t) (k 0)) (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sled") (r "^0.34.6") (d #t) (k 0)))) (h "1hd4c4j3giakjvz3mlsz7lziwzc3jnr307rdjhmzgxv3hnknpnsp")))

(define-public crate-typed-sled-0.1.10 (c (n "typed-sled") (v "0.1.10") (d (list (d (n "async-trait") (r "^0.1.51") (d #t) (k 0)) (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sled") (r "^0.34.6") (d #t) (k 0)))) (h "046c6fxsgbgp0x8xhlgqjmr2n3h6kyrsfrcdvfahp1gll7l70jq2")))

(define-public crate-typed-sled-0.1.11 (c (n "typed-sled") (v "0.1.11") (d (list (d (n "async-trait") (r "^0.1.51") (d #t) (k 0)) (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sled") (r "^0.34.6") (d #t) (k 0)))) (h "059fiz0a2d8zikrpqwz17d0nwj1fbq83ia3f56h6275ryw3m9n5l") (f (quote (("convert"))))))

(define-public crate-typed-sled-0.1.12 (c (n "typed-sled") (v "0.1.12") (d (list (d (n "async-trait") (r "^0.1.51") (d #t) (k 0)) (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sled") (r "^0.34.6") (d #t) (k 0)))) (h "11icsina17jf4b5ydxhav8lglz2h4lk9pc98kj1q771siawihal6") (f (quote (("convert"))))))

(define-public crate-typed-sled-0.1.13 (c (n "typed-sled") (v "0.1.13") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "bincode") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sled") (r "^0.34") (d #t) (k 0)))) (h "1a3kjx9dam1l7snkbi8szf17mniz7fbkynbbiycqs2h06wmcj6cf") (f (quote (("convert"))))))

(define-public crate-typed-sled-0.1.14 (c (n "typed-sled") (v "0.1.14") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "bincode") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sled") (r "^0.34") (d #t) (k 0)))) (h "1fqd8l1v50w0xllfzscxa8ijzblfij423qi44zf8cd1nkkya5009") (f (quote (("key-generating") ("convert"))))))

(define-public crate-typed-sled-0.2.0 (c (n "typed-sled") (v "0.2.0") (d (list (d (n "bincode") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (o #t) (d #t) (k 0)) (d (n "pin-project") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sled") (r "^0.34") (d #t) (k 0)) (d (n "tantivy") (r "^0.16") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1hl7xscrqlwmnps7xwav64d9zi1bv75q2bbn61wcfrfzx4aslqhk") (f (quote (("search" "tantivy") ("key-generating") ("convert"))))))

(define-public crate-typed-sled-0.2.1 (c (n "typed-sled") (v "0.2.1") (d (list (d (n "bincode") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (o #t) (d #t) (k 0)) (d (n "pin-project") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sled") (r "^0.34") (d #t) (k 0)) (d (n "tantivy") (r "^0.16") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1yjwvhm7hqczqhs7xhqxvgyfyshr7py8slsw59a5dkcgfamh7f9j") (f (quote (("search" "tantivy") ("key-generating") ("convert"))))))

(define-public crate-typed-sled-0.2.3 (c (n "typed-sled") (v "0.2.3") (d (list (d (n "bincode") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (o #t) (d #t) (k 0)) (d (n "pin-project") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sled") (r "^0.34") (d #t) (k 0)) (d (n "tantivy") (r "^0.19") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "03ah6i05rwkcwwq6ccbr42k96ni29yq522d6imsmpv2h8idg0q0h") (f (quote (("search" "tantivy") ("key-generating") ("convert"))))))

