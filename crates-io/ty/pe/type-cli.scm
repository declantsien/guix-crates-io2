(define-module (crates-io ty pe type-cli) #:use-module (crates-io))

(define-public crate-type-cli-0.0.1 (c (n "type-cli") (v "0.0.1") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "type-cli-derive") (r "^0.0.1") (d #t) (k 0)))) (h "06sfm30xzj3sd3aa95mjnfv9pzxjr6yxvp3ryj31w2hgr8j9nfgn")))

(define-public crate-type-cli-0.0.2 (c (n "type-cli") (v "0.0.2") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "type-cli-derive") (r "^0.0.1") (d #t) (k 0)))) (h "0rcnmsxzy3nxra6hcwbdjrsbi3qxskahqi5i8kxfjq3hf4f98ln9") (y #t)))

(define-public crate-type-cli-0.0.3 (c (n "type-cli") (v "0.0.3") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "type-cli-derive") (r "^0.0.1") (d #t) (k 0)))) (h "1gs9cypllsxh3zyqai941lmn6sqzrc3rlk88ra1l75d58v81jsp5")))

