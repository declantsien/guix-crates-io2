(define-module (crates-io ty pe type-fn) #:use-module (crates-io))

(define-public crate-type-fn-0.1.0 (c (n "type-fn") (v "0.1.0") (h "0hyycdgxr8q2njmdz38a4bnc8hb6f01a2x2visdy5pc9gyy3c7f8")))

(define-public crate-type-fn-0.1.1 (c (n "type-fn") (v "0.1.1") (h "1maf44h55l2niq7jcyv8g0mh4zlsz0443hhsj2vjvi0ccdm5d4g4")))

(define-public crate-type-fn-0.1.2 (c (n "type-fn") (v "0.1.2") (h "1y4lyxbkdrb3mnb7ywpjpzbibfbj2svdqrkjb6flx3rxmwcz8k90")))

