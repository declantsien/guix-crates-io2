(define-module (crates-io ty pe type_defender) #:use-module (crates-io))

(define-public crate-type_defender-0.2.0 (c (n "type_defender") (v "0.2.0") (d (list (d (n "crossterm") (r "^0.25") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rust-embed") (r "^6.6.1") (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (f (quote ("derive" "strum_macros"))) (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.3") (d #t) (k 0)) (d (n "tui") (r "^0.19.0") (d #t) (k 0)) (d (n "tui-textarea") (r "^0.2.0") (d #t) (k 0)))) (h "0l0sh8pp74daa9r5jz5dyi9hcj97aq89qvaxfwvs2zx2affnbrfn")))

