(define-module (crates-io ty pe typed_floats_macros) #:use-module (crates-io))

(define-public crate-typed_floats_macros-0.1.0 (c (n "typed_floats_macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0fjlhl9yavpgmryyafy7n9bybam2197wgx60y0m34l9idbn1kf0v") (y #t)))

(define-public crate-typed_floats_macros-0.1.1 (c (n "typed_floats_macros") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1yykvjx9hy2nhmgmibd98iivsmzfkj58q0899vd6zg30n61nph9v") (y #t)))

(define-public crate-typed_floats_macros-0.1.2 (c (n "typed_floats_macros") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0sjldy80dfhmw5yhnj9lmafv4za7nx6v5p54byvdc3vrpggnndvy") (y #t)))

(define-public crate-typed_floats_macros-0.1.3 (c (n "typed_floats_macros") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1mazd1539j0akand13cs50kyqllny0vm196b1f3yl381dwgj58mf") (y #t)))

(define-public crate-typed_floats_macros-0.1.4 (c (n "typed_floats_macros") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0pg7rhgc594zlq6nwh8pp4zvp7hw31rr5cbyhrgsz24iy4qc85gn") (y #t)))

(define-public crate-typed_floats_macros-0.1.5 (c (n "typed_floats_macros") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0jbxw6zrlspbdfk12wrbkdapnq592c7s804h4zlr26inhzw1sjgr") (y #t)))

(define-public crate-typed_floats_macros-0.1.6 (c (n "typed_floats_macros") (v "0.1.6") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1hljxg6142dzibwv2g2gljk230gxd7z1h5q3lsrsn4cy0983610b") (y #t)))

(define-public crate-typed_floats_macros-0.1.7 (c (n "typed_floats_macros") (v "0.1.7") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0zg2663maf4bkcz61fp2q5mkvaa5nsdk4pfwb49blypj6gzj9yny") (y #t)))

(define-public crate-typed_floats_macros-0.1.8 (c (n "typed_floats_macros") (v "0.1.8") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0lrpxjljzilip5rwcpvk98c4a1jfgli4qp5n05af4klyigar6cl8") (y #t)))

(define-public crate-typed_floats_macros-0.1.9 (c (n "typed_floats_macros") (v "0.1.9") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "133x1h3199cd954z187k914vhvmlwcbsfjf7ykm89rlq853qy920") (y #t)))

(define-public crate-typed_floats_macros-0.1.10 (c (n "typed_floats_macros") (v "0.1.10") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1lx976i7lkb9k8wy3hhxrf872bdcxf7xw5mmfwrhcz3nfahnmq8z") (y #t)))

(define-public crate-typed_floats_macros-0.1.11 (c (n "typed_floats_macros") (v "0.1.11") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "043xjbfxcpn3rd0743635gmicx5h7rigmqw92dmvnh8nq7ik3yqk") (y #t)))

(define-public crate-typed_floats_macros-0.1.12 (c (n "typed_floats_macros") (v "0.1.12") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0jrlb3pb97m36vzqqdr81mcv4ls4xr98yjxs99kikjmf3gk5jzhn") (y #t)))

(define-public crate-typed_floats_macros-0.1.13 (c (n "typed_floats_macros") (v "0.1.13") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1gjhwcsgp282vmmkvl7g1wxkbj4rab3b5qi0pq9fzbj4y1m44c9v") (y #t)))

(define-public crate-typed_floats_macros-0.1.14 (c (n "typed_floats_macros") (v "0.1.14") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1a3q2z0fhai2m4xqa4j7ssiar6309ka65whl6yrgc4hrlgc64fhh") (y #t)))

(define-public crate-typed_floats_macros-0.1.15 (c (n "typed_floats_macros") (v "0.1.15") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "17cy5x4vfa0fya3lcyck0fw3a1w3g8cmp2wwdq4m1finlpvqz7gl") (y #t)))

(define-public crate-typed_floats_macros-0.1.16 (c (n "typed_floats_macros") (v "0.1.16") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0iq1nm7cvbaa48drwndvaj23xdc134ggyb5gd3qa5pqvdfwmb6rq") (y #t)))

(define-public crate-typed_floats_macros-0.1.17 (c (n "typed_floats_macros") (v "0.1.17") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0mhk2kn0ks50wz5mkwv1nmr898wp81240vkgysldrbji30r86lz6") (y #t)))

(define-public crate-typed_floats_macros-0.1.18 (c (n "typed_floats_macros") (v "0.1.18") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "02x2rlhvcbj3k79n06sv7y13iyvpf8igy4g98fziy6y7dyrvakn1") (y #t)))

(define-public crate-typed_floats_macros-0.1.19 (c (n "typed_floats_macros") (v "0.1.19") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1xvkrd226md4k354bnzp08ppp45xisza6x0vxx5grq4wz92anx25") (y #t)))

(define-public crate-typed_floats_macros-0.1.20 (c (n "typed_floats_macros") (v "0.1.20") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "04nhvw4q5y03i7kpsnjl11r9g95n2wbaj8vq14b67pzj62b0w8fy") (y #t)))

(define-public crate-typed_floats_macros-0.1.21 (c (n "typed_floats_macros") (v "0.1.21") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0sx906nrjpfc61p4gpbf8fsykzr9w7s0b3fxrqg10v3j5bcv28h8") (y #t)))

(define-public crate-typed_floats_macros-0.1.22 (c (n "typed_floats_macros") (v "0.1.22") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "12x1dspqjk1fy8840zynvajs9kd3ghixir59jdbjqjcg8c9inhky") (y #t)))

(define-public crate-typed_floats_macros-0.1.23 (c (n "typed_floats_macros") (v "0.1.23") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1rkghh8m3hzif2lda0xlsjyx9c3i3n5k7fk8m3dpaysl2cd8c4na") (y #t)))

(define-public crate-typed_floats_macros-0.1.24 (c (n "typed_floats_macros") (v "0.1.24") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0qs4nfv5gj0p4ab2scx45dps3vcyz8h77slv66lhnxb31hkm6wa9") (y #t)))

(define-public crate-typed_floats_macros-0.1.25 (c (n "typed_floats_macros") (v "0.1.25") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0giryrx1sl0jamhzc8xmq5g52gmvcvv2lb0dgcrqqk3aps66d377") (y #t)))

(define-public crate-typed_floats_macros-0.1.26 (c (n "typed_floats_macros") (v "0.1.26") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "091bpqmzcrbzvdkkg3rqkynddz2jld5ijlk6r3spgshzzvaiyd50") (y #t)))

(define-public crate-typed_floats_macros-0.1.27 (c (n "typed_floats_macros") (v "0.1.27") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1372c3vwbszs0m74g5mv7hy0pnjkljk7qrjnibfhv215074i4xhy") (y #t)))

(define-public crate-typed_floats_macros-0.1.28 (c (n "typed_floats_macros") (v "0.1.28") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0l4005hm8vgnp39ivignhnc1536i06z1mrxv953kcaqh94fz92ms")))

(define-public crate-typed_floats_macros-0.1.29 (c (n "typed_floats_macros") (v "0.1.29") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0diq6k9b52hay7mac1sdb0kch4agm5lnifi6x887v4cn4kzgxzm9")))

(define-public crate-typed_floats_macros-0.1.30 (c (n "typed_floats_macros") (v "0.1.30") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0pg3rdc04dlnhmz8qqd80y0w3h8zw7dhmmbfwnq0qly7dwc1alm1")))

(define-public crate-typed_floats_macros-0.1.31 (c (n "typed_floats_macros") (v "0.1.31") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1szqlsfszbkrvpl2mffyh2rzpyalvic2ffzw08pcrjqwbfn5n08l")))

(define-public crate-typed_floats_macros-0.1.32 (c (n "typed_floats_macros") (v "0.1.32") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0qbziskk7a5fa4bk7jqssvyhn5gmda2bxpnccszrvjjs2srbkgnf")))

(define-public crate-typed_floats_macros-0.1.33 (c (n "typed_floats_macros") (v "0.1.33") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1mvd8537mblzbvakswilx5dqj040ga34vmnqq0wzagdbf8jrk1k5")))

(define-public crate-typed_floats_macros-0.1.34 (c (n "typed_floats_macros") (v "0.1.34") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1kvhywv9cfg90jramp49n5algqwyrcfxy7pj29jfia7jffgzg4ir")))

(define-public crate-typed_floats_macros-0.1.35 (c (n "typed_floats_macros") (v "0.1.35") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0907ijmk849vp7mg3mp4x18d9rd5ihnvs64zk1ys3kp9xnzf5xsn")))

(define-public crate-typed_floats_macros-0.1.36 (c (n "typed_floats_macros") (v "0.1.36") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0zzgh1zqmfh6bs327wpc1213wa35kg2klkg3irqd5cp05c769q43")))

(define-public crate-typed_floats_macros-0.1.37 (c (n "typed_floats_macros") (v "0.1.37") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0m08xa18ai6kdkdbww6kxvsxmnam4r6gvlqgl1j392vlvymk48vw")))

(define-public crate-typed_floats_macros-0.2.2 (c (n "typed_floats_macros") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1sm09b4lyyzn9n4s1bhpjpwf9j8rvls3ikhrizwdb3g4bi6m40x4")))

(define-public crate-typed_floats_macros-0.2.3 (c (n "typed_floats_macros") (v "0.2.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1czjj3vgwmwjyhhg5vj6rih2dh1h9m9ryqp0c6vx3yvy8908rvyp")))

(define-public crate-typed_floats_macros-0.2.4 (c (n "typed_floats_macros") (v "0.2.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "164kzcii9yjd0y6yln22c5kv4inwm4bxipwf2vl8d74izap8r516")))

(define-public crate-typed_floats_macros-0.3.0 (c (n "typed_floats_macros") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0p9bwcbapa1xfbil2l6jvs8yyvp0ibgxwzzzmld045w56f1dqhni")))

(define-public crate-typed_floats_macros-0.4.0 (c (n "typed_floats_macros") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "14v60qrl01wskfwaacxw4khy8c9yn37j3xxnkk8lgxqn0icj6myb")))

(define-public crate-typed_floats_macros-0.5.0 (c (n "typed_floats_macros") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0izann5hnllz9gikly8wk82m5xlllgmqij1m8s40jhmd7xs2klpa") (f (quote (("std") ("libm"))))))

(define-public crate-typed_floats_macros-0.6.0 (c (n "typed_floats_macros") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0kpl0qpinmj6pgyvs8c232xq498yjhc0ryg2mfkyxybn5g1shhm9") (f (quote (("std") ("libm"))))))

(define-public crate-typed_floats_macros-0.7.0 (c (n "typed_floats_macros") (v "0.7.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1d4ian59di6y06wzf451i4dkkzh5hnsbn8wlagfkibkrsm82aq68") (f (quote (("std") ("libm"))))))

(define-public crate-typed_floats_macros-0.7.1 (c (n "typed_floats_macros") (v "0.7.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "192sdy7xifwmj9slnx5j846rk767z125pcm90100yfcp80c0rx77") (f (quote (("std") ("libm"))))))

(define-public crate-typed_floats_macros-0.7.2 (c (n "typed_floats_macros") (v "0.7.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0l9fipg4wrd1mby8mkhjp4xzr5rm48yllpqk3xsk1y381qndi0ps") (f (quote (("std") ("libm"))))))

(define-public crate-typed_floats_macros-0.7.3 (c (n "typed_floats_macros") (v "0.7.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0g4qnv8f628755wqw5ly0gxw4njhifr1dqa0lr30vvpavda084jw") (f (quote (("std") ("libm"))))))

(define-public crate-typed_floats_macros-0.7.4 (c (n "typed_floats_macros") (v "0.7.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "03p6ra3lzzx7cf1ddnlgji8m2gz1s57v3pc9y5h18a6lwkc8bb3p") (f (quote (("std") ("libm")))) (r "1.70")))

(define-public crate-typed_floats_macros-1.0.0 (c (n "typed_floats_macros") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1a6c3y0nw610sabgkfh37gbhv0ihbi19knydrnb2bk7xg6nanvl9") (f (quote (("std") ("libm")))) (r "1.70")))

(define-public crate-typed_floats_macros-1.0.1 (c (n "typed_floats_macros") (v "1.0.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0hi93a3m2l76g1j848sx4kpx46kj5mlw63qgc8axk0c6l9rlj9mw") (f (quote (("std") ("libm")))) (r "1.70")))

