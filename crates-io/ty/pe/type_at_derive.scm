(define-module (crates-io ty pe type_at_derive) #:use-module (crates-io))

(define-public crate-type_at_derive-0.1.0 (c (n "type_at_derive") (v "0.1.0") (d (list (d (n "proc-macro-crate") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("derive" "parsing" "proc-macro" "printing"))) (k 0)))) (h "1i4slq39pwdxr39dzlnn7pvir395ks5322gnl3hqaaxg6ax5d93d")))

