(define-module (crates-io ty pe type-sets) #:use-module (crates-io))

(define-public crate-type-sets-0.0.1 (c (n "type-sets") (v "0.0.1") (h "1xllbw4bp4ygkaqhqbrp00jdr4ycy8dg9k0yqfpw4p62yn06rn64")))

(define-public crate-type-sets-0.0.2 (c (n "type-sets") (v "0.0.2") (h "0xgh0aqvkl2fq88xry15xfyyw044q591d40q45h0i80axvffs5lq")))

(define-public crate-type-sets-0.0.3 (c (n "type-sets") (v "0.0.3") (h "050s001hfh31snqi4pmlhpi8ml3lg9wjm5981wwhapfxzgdcm5yn")))

(define-public crate-type-sets-0.0.4 (c (n "type-sets") (v "0.0.4") (h "03rrsr1ygxypw57nfh1pz2qsksc32rq69skgs5vksb1llmy2qgly")))

(define-public crate-type-sets-0.0.5 (c (n "type-sets") (v "0.0.5") (d (list (d (n "csf") (r "^0.1") (d #t) (k 0)) (d (n "phf") (r "^0.11.0") (d #t) (k 0)))) (h "1sifqnp1ja6w69h7sicxhvl8hq4hb41sznx7fpn8l7cpbws50yci")))

(define-public crate-type-sets-0.0.6 (c (n "type-sets") (v "0.0.6") (d (list (d (n "csf") (r "^0.1") (d #t) (k 0)) (d (n "phf") (r "^0.11.0") (d #t) (k 0)))) (h "082xl2ckvpi2fhfl9h0hjfhy7kcm5ykqi2mvak79qkipvyzj3c5s")))

