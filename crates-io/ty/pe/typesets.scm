(define-module (crates-io ty pe typesets) #:use-module (crates-io))

(define-public crate-typesets-0.0.2 (c (n "typesets") (v "0.0.2") (d (list (d (n "rustfmt-wrapper") (r "^0.1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "typesets-macro") (r "^0.0.2") (d #t) (k 0)))) (h "00xhcd5hzz02kv83qsm53b4d5rm13ywfahapdjbb8mrwszayhxrp")))

