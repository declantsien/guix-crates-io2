(define-module (crates-io ty pe type_ulid_macros) #:use-module (crates-io))

(define-public crate-type_ulid_macros-0.1.0 (c (n "type_ulid_macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.100") (f (quote ("extra-traits"))) (d #t) (k 0)) (d (n "ulid") (r "^1.0.0") (k 0)))) (h "0x0h99wa0b9rvjg9mh4pwp9pcqnx12msg7vw2qbd3jzq03iwxmgb")))

(define-public crate-type_ulid_macros-0.2.0 (c (n "type_ulid_macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.100") (f (quote ("extra-traits"))) (d #t) (k 0)) (d (n "ulid") (r "^1.0.0") (k 0)))) (h "0lvagdhs1ngv1mjcklxmhj0xny7v5wx3qng058yqzb4qwalhyny6")))

