(define-module (crates-io ty pe typeql) #:use-module (crates-io))

(define-public crate-typeql-2.24.5 (c (n "typeql") (v "2.24.5") (d (list (d (n "chrono") (r "0.4.23") (k 0)) (d (n "itertools") (r "0.10.3") (k 0)) (d (n "pest") (r "2.4.0") (k 0)) (d (n "pest_derive") (r "2.4.0") (k 0)) (d (n "regex") (r "1.6.0") (k 0)))) (h "0simhsrmzv1lj2xj3yczybrx5rfgkdcc38wvjcp7ijwj90i56yan")))

(define-public crate-typeql-2.24.8 (c (n "typeql") (v "2.24.8") (d (list (d (n "chrono") (r "0.4.23") (k 0)) (d (n "itertools") (r "0.10.3") (k 0)) (d (n "pest") (r "2.7.4") (k 0)) (d (n "pest_derive") (r "2.7.4") (k 0)) (d (n "regex") (r "1.6.0") (k 0)))) (h "174shf08va1grm5bkmvs25cqrfy3xavh0yyxk4s9q9xz5ksspd40")))

(define-public crate-typeql-2.24.9 (c (n "typeql") (v "2.24.9") (d (list (d (n "chrono") (r "=0.4.23") (d #t) (k 0)) (d (n "itertools") (r "=0.10.3") (d #t) (k 0)) (d (n "pest") (r "=2.7.4") (d #t) (k 0)) (d (n "pest_derive") (r "=2.7.4") (d #t) (k 0)) (d (n "regex") (r "=1.6.0") (d #t) (k 0)))) (h "03g2crs17ia97d0p3nnw7s041lxg0kcm8vi8fn0hcd5rdxfr52gq")))

(define-public crate-typeql-2.24.10 (c (n "typeql") (v "2.24.10") (d (list (d (n "chrono") (r "=0.4.23") (d #t) (k 0)) (d (n "itertools") (r "=0.10.3") (d #t) (k 0)) (d (n "pest") (r "=2.7.4") (d #t) (k 0)) (d (n "pest_derive") (r "=2.7.4") (d #t) (k 0)) (d (n "regex") (r "=1.6.0") (d #t) (k 0)))) (h "18q6cb804j5qfrjsx7cb0wcn8dzfb00zsd55523q40b5ayw5pgff")))

(define-public crate-typeql-2.24.11 (c (n "typeql") (v "2.24.11") (d (list (d (n "chrono") (r "=0.4.23") (d #t) (k 0)) (d (n "itertools") (r "=0.10.3") (d #t) (k 0)) (d (n "pest") (r "=2.7.4") (d #t) (k 0)) (d (n "pest_derive") (r "=2.7.4") (d #t) (k 0)) (d (n "regex") (r "=1.6.0") (d #t) (k 0)))) (h "162cr8zp24isgxlbyxfaaxz65vhpbzjgcfcx56fcdqw8gfb4cdkn")))

(define-public crate-typeql-2.25.0 (c (n "typeql") (v "2.25.0") (d (list (d (n "chrono") (r "=0.4.23") (d #t) (k 0)) (d (n "itertools") (r "=0.10.3") (d #t) (k 0)) (d (n "pest") (r "=2.7.4") (d #t) (k 0)) (d (n "pest_derive") (r "=2.7.4") (d #t) (k 0)) (d (n "regex") (r "=1.6.0") (d #t) (k 0)))) (h "12adp0c4y1wqkprfhlgniswfyjbhb4i8z5awwy1n1lpfxr8ksgd5")))

(define-public crate-typeql-2.25.8 (c (n "typeql") (v "2.25.8") (d (list (d (n "chrono") (r "=0.4.23") (d #t) (k 0)) (d (n "itertools") (r "=0.10.3") (d #t) (k 0)) (d (n "pest") (r "=2.7.4") (d #t) (k 0)) (d (n "pest_derive") (r "=2.7.4") (d #t) (k 0)) (d (n "regex") (r "=1.6.0") (d #t) (k 0)))) (h "0rd8kmlqxhcn0q61hd35laqlwm5zgk8py0flhvhdyxpb9i84yayl")))

(define-public crate-typeql-2.26.6-rc0 (c (n "typeql") (v "2.26.6-rc0") (d (list (d (n "chrono") (r "=0.4.23") (d #t) (k 0)) (d (n "itertools") (r "=0.10.3") (d #t) (k 0)) (d (n "pest") (r "=2.7.4") (d #t) (k 0)) (d (n "pest_derive") (r "=2.7.4") (d #t) (k 0)) (d (n "regex") (r "=1.6.0") (d #t) (k 0)))) (h "0y4yy59yn1j67lkgc5gp0186idyh9amlq4k1asyvrdpsd2yngn0k")))

(define-public crate-typeql-2.26.6 (c (n "typeql") (v "2.26.6") (d (list (d (n "chrono") (r "=0.4.23") (d #t) (k 0)) (d (n "itertools") (r "=0.10.3") (d #t) (k 0)) (d (n "pest") (r "=2.7.4") (d #t) (k 0)) (d (n "pest_derive") (r "=2.7.4") (d #t) (k 0)) (d (n "regex") (r "=1.6.0") (d #t) (k 0)))) (h "064mg8ifbmy37xffpm4fdzh935a6jk5nw783z6jlgla3wrzz9wwx")))

(define-public crate-typeql-2.27.0-rc0 (c (n "typeql") (v "2.27.0-rc0") (d (list (d (n "chrono") (r "=0.4.23") (d #t) (k 0)) (d (n "itertools") (r "=0.10.3") (d #t) (k 0)) (d (n "pest") (r "=2.7.4") (d #t) (k 0)) (d (n "pest_derive") (r "=2.7.4") (d #t) (k 0)) (d (n "regex") (r "=1.6.0") (d #t) (k 0)))) (h "0j9g868kxfkaiv7nbk51ajjscv6sgn8lv43f0c2i0fyqmc8vv0jg")))

(define-public crate-typeql-2.27.0 (c (n "typeql") (v "2.27.0") (d (list (d (n "chrono") (r "=0.4.23") (d #t) (k 0)) (d (n "itertools") (r "=0.10.3") (d #t) (k 0)) (d (n "pest") (r "=2.7.4") (d #t) (k 0)) (d (n "pest_derive") (r "=2.7.4") (d #t) (k 0)) (d (n "regex") (r "=1.6.0") (d #t) (k 0)))) (h "1z7llxlqkzr2n8ipr5kl37wn0350piyx88gc4gwgl830hc5hv0s4")))

(define-public crate-typeql-2.28.0 (c (n "typeql") (v "2.28.0") (d (list (d (n "chrono") (r "=0.4.23") (d #t) (k 0)) (d (n "itertools") (r "=0.10.3") (d #t) (k 0)) (d (n "pest") (r "=2.7.4") (d #t) (k 0)) (d (n "pest_derive") (r "=2.7.4") (d #t) (k 0)) (d (n "regex") (r "=1.6.0") (d #t) (k 0)))) (h "1lkgzbnsdjzhl7lz2lpj6dc2ys7znx6whjvlhgb3krz8h8wkqz62")))

(define-public crate-typeql-2.28.1 (c (n "typeql") (v "2.28.1") (d (list (d (n "chrono") (r "=0.4.23") (d #t) (k 0)) (d (n "itertools") (r "=0.10.3") (d #t) (k 0)) (d (n "pest") (r "=2.7.4") (d #t) (k 0)) (d (n "pest_derive") (r "=2.7.4") (d #t) (k 0)) (d (n "regex") (r "=1.6.0") (d #t) (k 0)))) (h "1asd1vils8483h84czm1cjzm9dglfvirc7rx5rd0465m8xhgirib")))

