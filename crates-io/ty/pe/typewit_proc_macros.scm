(define-module (crates-io ty pe typewit_proc_macros) #:use-module (crates-io))

(define-public crate-typewit_proc_macros-1.8.0 (c (n "typewit_proc_macros") (v "1.8.0") (h "04qqqilsi323s36qwxgk1hab7am0ffrk2vw8v0df1xaq42br3p01") (r "1.57.0")))

(define-public crate-typewit_proc_macros-1.8.1 (c (n "typewit_proc_macros") (v "1.8.1") (h "1mlkh4mhbn4b7xg9640blk74bm5ddaa44ihvl0sljw1w5gm86sp3") (r "1.57.0")))

