(define-module (crates-io ty pe typebitset) #:use-module (crates-io))

(define-public crate-typebitset-0.1.0 (c (n "typebitset") (v "0.1.0") (h "1n26zm44gi2af66bs9i89rj5s9l6ygzn43hkb4nmq8cpyybrx52m")))

(define-public crate-typebitset-0.2.0 (c (n "typebitset") (v "0.2.0") (h "1ni10xldb574rbpx1mfqgafjwyhf5jlvxwqcdvnax60qllan740g")))

(define-public crate-typebitset-0.3.0 (c (n "typebitset") (v "0.3.0") (h "1rhd5gh1v2vv6mcnlkqiy2q93j0nxdqmbi7g602j0flm22zdza8q")))

(define-public crate-typebitset-0.4.0 (c (n "typebitset") (v "0.4.0") (h "029z29gf77qlqiv7ggdqv7zx1z4c2x01pqsm7dkk8a5i945shs6g")))

(define-public crate-typebitset-0.5.0 (c (n "typebitset") (v "0.5.0") (h "1x4rfx07rxsk6rvywwqlvnx4ddni9yxvc2d3lclgjyqi0099m6n4")))

(define-public crate-typebitset-0.5.1 (c (n "typebitset") (v "0.5.1") (h "0f1b4ls744kfw3vfn6zf8djyfgpgv4x0xwi1h3x53rnvqn56vwmz") (r "1.54")))

