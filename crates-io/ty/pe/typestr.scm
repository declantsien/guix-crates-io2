(define-module (crates-io ty pe typestr) #:use-module (crates-io))

(define-public crate-typestr-0.1.0 (c (n "typestr") (v "0.1.0") (d (list (d (n "itertools") (r "^0.12.1") (k 2)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 2)))) (h "1fxnbkk038zhx0fc36bnm7s7g0r8kafrqj61qf8wp02dn2nssswf") (f (quote (("std") ("default" "std"))))))

