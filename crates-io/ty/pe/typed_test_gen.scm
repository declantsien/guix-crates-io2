(define-module (crates-io ty pe typed_test_gen) #:use-module (crates-io))

(define-public crate-typed_test_gen-0.1.0 (c (n "typed_test_gen") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0w13rqqwx4m7ajgc4ald39j6v4jwfx7f0ynalifwz8i4j97ad462")))

