(define-module (crates-io ty pe type-handle) #:use-module (crates-io))

(define-public crate-type-handle-0.1.0 (c (n "type-handle") (v "0.1.0") (h "082lwx47p0yzfmfl10zfz8jxdwqsclxpm1ch1r5llvjlpac8i4sw") (f (quote (("send_sync") ("default" "send_sync"))))))

(define-public crate-type-handle-0.1.1 (c (n "type-handle") (v "0.1.1") (h "165drcmv3mhq17yazl912mrhc9vvs7bvfxdwjw3hglypl26nm31x") (f (quote (("send_sync") ("default" "send_sync"))))))

(define-public crate-type-handle-0.1.2 (c (n "type-handle") (v "0.1.2") (h "1xyafjkjiv8i77gidiphxjp31dxq6b2lii5zf00wri53n3fl3jp1") (f (quote (("send_sync") ("default" "send_sync"))))))

