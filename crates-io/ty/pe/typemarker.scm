(define-module (crates-io ty pe typemarker) #:use-module (crates-io))

(define-public crate-typemarker-0.1.0 (c (n "typemarker") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.34") (d #t) (k 0)) (d (n "typemarker_core") (r "^0.1.0") (d #t) (k 0)))) (h "1k6whs0yxhnjmmr0wynbx4f58mmkg88miwxv7mkis9zlzqlpa8k4")))

(define-public crate-typemarker-0.1.1 (c (n "typemarker") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.34") (d #t) (k 0)) (d (n "typemarker_core") (r "^0.1.1") (d #t) (k 0)))) (h "0ngrni8dcgrv4wff475wi6zm459mx0aly5wh84qr76vxhsbw6jdh")))

