(define-module (crates-io ty pe type-freak) #:use-module (crates-io))

(define-public crate-type-freak-0.1.0 (c (n "type-freak") (v "0.1.0") (d (list (d (n "typenum") (r "~1") (d #t) (k 0)))) (h "12wvw0318h3lmy75qlf0fjwv0azmcvcmw5cz97k0nnvxv2gqb49x")))

(define-public crate-type-freak-0.1.1 (c (n "type-freak") (v "0.1.1") (d (list (d (n "typenum") (r "~1") (d #t) (k 0)))) (h "1m3np7sbs18cr4mr5q1nzy7f6izzjhdcbns7ahm8npsx3jnqs7nh")))

(define-public crate-type-freak-0.1.2 (c (n "type-freak") (v "0.1.2") (d (list (d (n "typenum") (r "~1") (d #t) (k 0)))) (h "19ajr2bksf2sqdmkwj3khv7iqvgm03g7gvaikficd20vka9i6gsq")))

(define-public crate-type-freak-0.2.0 (c (n "type-freak") (v "0.2.0") (d (list (d (n "typenum") (r "~1") (d #t) (k 0)))) (h "1p5xswdm69di5kid58fhhnj72xxwh6vl568fjsiab1nxllg1igrl")))

