(define-module (crates-io ty pe typed-urls-derive) #:use-module (crates-io))

(define-public crate-typed-urls-derive-0.1.0 (c (n "typed-urls-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "typed-urls") (r "^0.1") (d #t) (k 0)))) (h "0b6jlhdi48hfvykjhfxqwxyh3r46i67x35vn0p11rv26bdqhqwzr")))

