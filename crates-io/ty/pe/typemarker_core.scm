(define-module (crates-io ty pe typemarker_core) #:use-module (crates-io))

(define-public crate-typemarker_core-0.1.0 (c (n "typemarker_core") (v "0.1.0") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.34") (d #t) (k 0)) (d (n "quote") (r "^1.0.22") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full"))) (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full" "extra-traits"))) (d #t) (k 2)))) (h "07ixqih66r80gdw2m0gagznmh1izkyc1sdsxmlniqmf6giz0nx9a")))

(define-public crate-typemarker_core-0.1.1 (c (n "typemarker_core") (v "0.1.1") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.34") (d #t) (k 0)) (d (n "quote") (r "^1.0.22") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full"))) (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full" "extra-traits"))) (d #t) (k 2)))) (h "18qhb88d4xl040687bg764sn793k5lamjbbssmsy06aiggfcmcs3")))

