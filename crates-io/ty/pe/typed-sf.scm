(define-module (crates-io ty pe typed-sf) #:use-module (crates-io))

(define-public crate-typed-sf-1.0.0 (c (n "typed-sf") (v "1.0.0") (h "1cqb3alhljxxcx4v71g4fq4gfix4pmkrnv2iafksh9lvaj2xiky1") (f (quote (("require_docs") ("no_std") ("default" "require_docs")))) (y #t)))

(define-public crate-typed-sf-1.0.1 (c (n "typed-sf") (v "1.0.1") (h "0pc44hihvyniaprwpyldvzkkx3gs2ws35cmfd2z1y3ci531dzkfp") (f (quote (("require_docs") ("no_std") ("default" "require_docs"))))))

(define-public crate-typed-sf-1.0.2 (c (n "typed-sf") (v "1.0.2") (h "1ciiaxqd5gxpwx97crx9lf8hwm6h9myj1k973dvd31r7dak0jgjr") (f (quote (("require_docs") ("no_std") ("default" "require_docs"))))))

(define-public crate-typed-sf-1.0.3 (c (n "typed-sf") (v "1.0.3") (h "1896n88bqyzipsc4j7qm8djjh3pjw6hkyi52mzmnmhk36xnilvna") (f (quote (("require_docs") ("no_std") ("default" "require_docs"))))))

(define-public crate-typed-sf-2.0.0 (c (n "typed-sf") (v "2.0.0") (h "1hql8ag38yb723xyshrmj0f1cmfsby375rmzqawa8rx4dpj3d5s0") (f (quote (("require_docs") ("no_std") ("default" "require_docs"))))))

