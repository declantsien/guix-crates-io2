(define-module (crates-io ty pe typer) #:use-module (crates-io))

(define-public crate-typer-0.1.0 (c (n "typer") (v "0.1.0") (d (list (d (n "image") (r "^0.20.1") (d #t) (k 2)) (d (n "rusttype") (r "^0.7.3") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.0") (d #t) (k 0)))) (h "1ynng2pq2dq60kfkxz073q6yaz00qmhgklg0wfgwfvjqif9597xy")))

(define-public crate-typer-0.1.1 (c (n "typer") (v "0.1.1") (d (list (d (n "image") (r "^0.20.1") (d #t) (k 2)) (d (n "rusttype") (r "^0.7.3") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.0") (d #t) (k 0)))) (h "0dvndbd9nm294cr5gf6i5s4zxpwryhz47p32qw6cf7ks7ms4y2kl")))

