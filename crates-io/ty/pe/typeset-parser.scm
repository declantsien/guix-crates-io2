(define-module (crates-io ty pe typeset-parser) #:use-module (crates-io))

(define-public crate-typeset-parser-2.0.0 (c (n "typeset-parser") (v "2.0.0") (d (list (d (n "proc-macro2") (r "^1.0.53") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.8") (f (quote ("full"))) (d #t) (k 0)) (d (n "typeset") (r "^2.0.0") (d #t) (k 2)))) (h "1bv16245j3zaw8w6awcy7ci01apmrxqvyrr1wksa3fnk9hysdgjf")))

(define-public crate-typeset-parser-2.0.1 (c (n "typeset-parser") (v "2.0.1") (d (list (d (n "proc-macro2") (r "^1.0.60") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (f (quote ("full"))) (d #t) (k 0)) (d (n "typeset") (r "^2.0.0") (d #t) (k 2)))) (h "0ldhgz63wjxsqmjwnrsz4fvy3iq3lv8szlypmlgrlqxygkqs4dqy")))

(define-public crate-typeset-parser-2.0.2 (c (n "typeset-parser") (v "2.0.2") (d (list (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "syn") (r "^2.0.23") (f (quote ("full"))) (d #t) (k 0)) (d (n "typeset") (r "^2.0.0") (d #t) (k 2)))) (h "1xv9ksr6lfa4915n5j015b2srz18v22hdwvb7ddrpllaqvvbzhjy")))

(define-public crate-typeset-parser-2.0.3 (c (n "typeset-parser") (v "2.0.3") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full"))) (d #t) (k 0)) (d (n "typeset") (r "^2.0.3") (d #t) (k 2)))) (h "013lbmyxvyyh1yar03f0wjspafg0d4lbpacszrqabspn7dimwjnx")))

(define-public crate-typeset-parser-2.0.4 (c (n "typeset-parser") (v "2.0.4") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.58") (f (quote ("full"))) (d #t) (k 0)) (d (n "typeset") (r "^2.0.3") (d #t) (k 2)))) (h "0mnlmp9sxgv9kp1fv1mh6k2486cs26sbz5jsb1rydh6ybkm2vyjl")))

