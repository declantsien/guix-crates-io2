(define-module (crates-io ty pe typeslice-macros) #:use-module (crates-io))

(define-public crate-typeslice-macros-0.1.1 (c (n "typeslice-macros") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.52") (f (quote ("parsing" "printing" "proc-macro"))) (k 0)))) (h "0l981mfxbys9rarifwyq3frha76jq4r3882y0nigpgaidlp5lmr7")))

(define-public crate-typeslice-macros-0.1.2 (c (n "typeslice-macros") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.52") (f (quote ("parsing" "printing" "proc-macro"))) (k 0)))) (h "0mkai6vk28sl9shkykmn1k4fg64ja1k6xhgp3iwhbzzshmxjnhg2")))

