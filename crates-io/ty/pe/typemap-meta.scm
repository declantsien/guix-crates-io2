(define-module (crates-io ty pe typemap-meta) #:use-module (crates-io))

(define-public crate-typemap-meta-0.1.0 (c (n "typemap-meta") (v "0.1.0") (d (list (d (n "typemap-meta-derive") (r "^0.1") (d #t) (k 0)))) (h "0r6s1gl6zvwmxqhqr080kn60knjdjn6gwjpv0q64r8agxpb1x7nv")))

(define-public crate-typemap-meta-0.2.0 (c (n "typemap-meta") (v "0.2.0") (d (list (d (n "typemap-meta-derive") (r "^0.2") (d #t) (k 0)))) (h "1qr95ni6iwicqkz8ci2ikwp4a0z16fa7qwshjzms5q5vczg5qk20")))

