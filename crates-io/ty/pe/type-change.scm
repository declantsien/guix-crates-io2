(define-module (crates-io ty pe type-change) #:use-module (crates-io))

(define-public crate-type-change-0.1.0 (c (n "type-change") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.28") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.74") (d #t) (k 0)))) (h "0an8lbcysag6jb2bkcpx4zd1qjj8q24mv83zia38w4zcaznpmj25")))

