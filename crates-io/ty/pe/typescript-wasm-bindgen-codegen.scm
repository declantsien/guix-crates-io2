(define-module (crates-io ty pe typescript-wasm-bindgen-codegen) #:use-module (crates-io))

(define-public crate-typescript-wasm-bindgen-codegen-0.1.0 (c (n "typescript-wasm-bindgen-codegen") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "swc_common") (r "^0.10") (d #t) (k 0)) (d (n "swc_ecma_ast") (r "^0.36") (d #t) (k 0)) (d (n "swc_ecma_parser") (r "^0.44") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1sli8ggld3fc0vm4xaa2s0ibn4x24mngsf4gha4c2hagsrs45mlc")))

(define-public crate-typescript-wasm-bindgen-codegen-0.1.1 (c (n "typescript-wasm-bindgen-codegen") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "swc_common") (r "^0.10") (d #t) (k 0)) (d (n "swc_ecma_ast") (r "^0.36") (d #t) (k 0)) (d (n "swc_ecma_parser") (r "^0.44") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1mznjq2nzrirm5rz0c5ygdnx4yg2jygmwaffs12n09mvvldrilbp")))

