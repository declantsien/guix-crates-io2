(define-module (crates-io ty pe type-uuid) #:use-module (crates-io))

(define-public crate-type-uuid-0.1.0 (c (n "type-uuid") (v "0.1.0") (d (list (d (n "amethyst") (r "^0.10.0") (o #t) (d #t) (k 0)) (d (n "type-uuid-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "uuid") (r "^0.7.1") (d #t) (k 2)))) (h "0jbm25w01ch1fpi1v8q19wacydbpascyajjhr8a89hpwmgyaq4yd")))

(define-public crate-type-uuid-0.1.1 (c (n "type-uuid") (v "0.1.1") (d (list (d (n "amethyst") (r "^0.10.0") (o #t) (d #t) (k 0)) (d (n "type-uuid-derive") (r "^0.1.1") (d #t) (k 0)) (d (n "uuid") (r "^0.7.1") (d #t) (k 2)))) (h "1h8822k1zx7f80n3pi09szdfwl9ll1k04lgpdc4hdwcxzbd7q5vy")))

(define-public crate-type-uuid-0.1.2 (c (n "type-uuid") (v "0.1.2") (d (list (d (n "amethyst") (r "^0.10.0") (o #t) (d #t) (k 0)) (d (n "type-uuid-derive") (r "^0.1.1") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (k 2)))) (h "0lf0yk8rmxrw4rmw76gzm3jbi0ax81d3cp85n34hacpl8753n10m")))

