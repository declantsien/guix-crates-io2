(define-module (crates-io ty pe typehints-my-apple) #:use-module (crates-io))

(define-public crate-typehints-my-apple-0.1.0 (c (n "typehints-my-apple") (v "0.1.0") (h "02qs7qvlmyyzg7831xiksbyr6gna8k3zg6mfhsssrin09ihws4x2")))

(define-public crate-typehints-my-apple-0.1.1 (c (n "typehints-my-apple") (v "0.1.1") (h "1adhrpkwgmlrbq818s3ckcarddg971g17qp0ng6svpd1q7gl4j65")))

(define-public crate-typehints-my-apple-0.1.2 (c (n "typehints-my-apple") (v "0.1.2") (h "0p0b849k8s12bpxllnybrxagw29d3n11p1wvp6kfvlff4x6fjlzr")))

