(define-module (crates-io ty pe typescriptify-derive) #:use-module (crates-io))

(define-public crate-typescriptify-derive-0.1.0 (c (n "typescriptify-derive") (v "0.1.0") (d (list (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "1m681lxkp64j5yvn8v4575aqqj3028l3bggakqwcljgg0yfsjjl8")))

(define-public crate-typescriptify-derive-0.1.1 (c (n "typescriptify-derive") (v "0.1.1") (d (list (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "0rj7bshpvr5ym0c63isds53lmc7cbjnkkwx3p03dj5vjijssqhpn")))

(define-public crate-typescriptify-derive-0.1.2 (c (n "typescriptify-derive") (v "0.1.2") (d (list (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "0p656qlj76569w0451zamr4ilpc2mdx536c300ibrzhvr7hpfkcf")))

(define-public crate-typescriptify-derive-0.1.3 (c (n "typescriptify-derive") (v "0.1.3") (d (list (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "08n42v2v5pmbaf1vyirmp65xrcrjhbncs15aj78i6z4jr2q6x0kd")))

(define-public crate-typescriptify-derive-0.1.4 (c (n "typescriptify-derive") (v "0.1.4") (d (list (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "1cds3qj5gn4cxvadmf989ldv4lznjzh6lzqvmg656wm20xj87ax6")))

(define-public crate-typescriptify-derive-0.1.5 (c (n "typescriptify-derive") (v "0.1.5") (d (list (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "1h8i8lry87pyflav79g1rlf07g30n045lq0h41dxymr646crgm56")))

