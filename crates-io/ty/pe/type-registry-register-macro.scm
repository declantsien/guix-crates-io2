(define-module (crates-io ty pe type-registry-register-macro) #:use-module (crates-io))

(define-public crate-type-registry-register-macro-0.1.0 (c (n "type-registry-register-macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.81") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.60") (f (quote ("full"))) (d #t) (k 0)))) (h "0dh0g7a2z6c1kmvfvwzidc1nzf4nqhz3nwrqnfg40556n35rjlzp")))

(define-public crate-type-registry-register-macro-0.2.0 (c (n "type-registry-register-macro") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.81") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.60") (f (quote ("full"))) (d #t) (k 0)))) (h "1kpcgdivdibwzzifrqilzwcip1ngn8055hwf88kbbdarwj0ckn5r")))

