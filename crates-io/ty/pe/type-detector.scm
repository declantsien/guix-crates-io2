(define-module (crates-io ty pe type-detector) #:use-module (crates-io))

(define-public crate-type-detector-0.1.0 (c (n "type-detector") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "1rxwdk78197hnfv0qsa0d74l6pwlg6wi4d6n4qjj28lbpqk6wnv1")))

(define-public crate-type-detector-0.1.1 (c (n "type-detector") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)))) (h "053l3ixz2bz5pf5sjgry131jjj19nr58wc938xbd56dpbgwh38mh")))

(define-public crate-type-detector-0.1.3 (c (n "type-detector") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0nbbhc695vaz95r3yy88hppyr8bp0l7zf3w6annhf52grim2yc89")))

(define-public crate-type-detector-0.1.4 (c (n "type-detector") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1fh5pymcsqqa1ppqq309sk9k22kr2ysyp6pgi0agjgzb8msx9dca")))

(define-public crate-type-detector-0.1.6 (c (n "type-detector") (v "0.1.6") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0j5iirh87f1jnkrhsikfv39lnlf9dwd9dsf9pjgciy5vb8wpki59")))

