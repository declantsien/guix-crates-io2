(define-module (crates-io ty pe typed-fields) #:use-module (crates-io))

(define-public crate-typed-fields-0.1.0 (c (n "typed-fields") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.60") (d #t) (k 0)) (d (n "quote") (r "^1.0.25") (d #t) (k 0)) (d (n "secrecy") (r "^0.4.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.0") (d #t) (k 2)) (d (n "syn") (r "^2.0.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1ak73v46jw2ibsa0clxabmlav2jyl466f90iyl7l15rxdacha0i6") (s 2) (e (quote (("serde" "dep:serde" "secrecy?/serde") ("secret" "dep:secrecy")))) (r "1.60")))

