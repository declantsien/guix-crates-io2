(define-module (crates-io ty pe typetest_core) #:use-module (crates-io))

(define-public crate-typetest_core-1.0.0 (c (n "typetest_core") (v "1.0.0") (d (list (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "rstest") (r "^0.10") (d #t) (k 2)))) (h "0dzvn05k20dz6qd3h0ws03phxd8a4g4kj46xqlvrrnpw3av0kvw7")))

