(define-module (crates-io ty pe typesafe-builders-derive) #:use-module (crates-io))

(define-public crate-typesafe-builders-derive-0.1.0 (c (n "typesafe-builders-derive") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.13") (d #t) (k 0)))) (h "1jv7d09h258hh9ihixlh6dmw1n87rva00xs6qj3iqdq5lickwha5")))

(define-public crate-typesafe-builders-derive-0.1.1 (c (n "typesafe-builders-derive") (v "0.1.1") (d (list (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.13") (d #t) (k 0)))) (h "1fdcviv117axgsmqd260d590aaqrkjb5zxrw3mrz5zzy46n0qdam")))

(define-public crate-typesafe-builders-derive-0.1.2 (c (n "typesafe-builders-derive") (v "0.1.2") (d (list (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.13") (d #t) (k 0)))) (h "09wmkqq57vna3411j4l6icy85kkc2xlj47archb2gqz127k65na0")))

(define-public crate-typesafe-builders-derive-0.1.3 (c (n "typesafe-builders-derive") (v "0.1.3") (d (list (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.13") (d #t) (k 0)))) (h "19f5crrn87b93vxj8pfc4b6krqs4cc326rw05zsg8wpqb7ximvzn")))

(define-public crate-typesafe-builders-derive-0.2.1 (c (n "typesafe-builders-derive") (v "0.2.1") (d (list (d (n "quote") (r "^1.0.26") (k 0)) (d (n "syn") (r "^2.0.13") (d #t) (k 0)) (d (n "typesafe-builders-core") (r "^0.2.1") (d #t) (k 0)))) (h "1s72qphqi3bs9rzbkaa83n0y6msa7mf7w9837whsdlfa5f3jax2g")))

(define-public crate-typesafe-builders-derive-0.3.0 (c (n "typesafe-builders-derive") (v "0.3.0") (d (list (d (n "quote") (r "^1.0.26") (k 0)) (d (n "syn") (r "^2.0.13") (d #t) (k 0)) (d (n "typesafe-builders-core") (r "^0.3.0") (d #t) (k 0)))) (h "1dfl3sqrdn7sp1h9igry1sb3fz7m3q6hjjkl19aybrxg6w45kni4")))

(define-public crate-typesafe-builders-derive-0.4.0 (c (n "typesafe-builders-derive") (v "0.4.0") (d (list (d (n "quote") (r "^1.0.26") (k 0)) (d (n "syn") (r "^2.0.13") (d #t) (k 0)) (d (n "typesafe-builders-core") (r "^0.4.0") (d #t) (k 0)))) (h "158nrcbpnb7m11nwwk5xbr1hn0icp18lhyk9svxi61br168z6hpj")))

(define-public crate-typesafe-builders-derive-0.4.1 (c (n "typesafe-builders-derive") (v "0.4.1") (d (list (d (n "quote") (r "^1.0.26") (k 0)) (d (n "syn") (r "^2.0.15") (d #t) (k 0)) (d (n "typesafe-builders") (r "^0.4.0") (d #t) (k 2)) (d (n "typesafe-builders-core") (r "^0.4.1") (d #t) (k 0)))) (h "0w339x4jjzmpizm6wi0f0i1bkhgv4pcqglf7l7iwigml5ky64i15")))

(define-public crate-typesafe-builders-derive-0.5.0 (c (n "typesafe-builders-derive") (v "0.5.0") (d (list (d (n "quote") (r "^1.0.26") (k 0)) (d (n "syn") (r "^2.0.15") (d #t) (k 0)) (d (n "typesafe-builders-core") (r "^0.5.0") (d #t) (k 0)))) (h "1vvsl64nk81d5fih5577n707z3q80mr61f0xs3gb8ycbn3m1py0q") (r "1.65.0")))

