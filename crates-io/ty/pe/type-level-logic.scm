(define-module (crates-io ty pe type-level-logic) #:use-module (crates-io))

(define-public crate-type-level-logic-0.1.0 (c (n "type-level-logic") (v "0.1.0") (d (list (d (n "type-operators") (r "^0.3.4") (d #t) (k 0)))) (h "1n7azfgdy3va9irkbz4ai1l9ph23y573qs044knjpfsykbcz1van") (f (quote (("specialization" "type-operators/specialization") ("nightly" "specialization") ("default"))))))

