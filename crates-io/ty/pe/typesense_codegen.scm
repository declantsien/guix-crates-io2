(define-module (crates-io ty pe typesense_codegen) #:use-module (crates-io))

(define-public crate-typesense_codegen-0.23.0 (c (n "typesense_codegen") (v "0.23.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "0nymf8czrj70vzav3lvc5sfma9yzcaykwzzgzyfqgakxzdj7y249")))

