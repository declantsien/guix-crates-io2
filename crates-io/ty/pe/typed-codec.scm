(define-module (crates-io ty pe typed-codec) #:use-module (crates-io))

(define-public crate-typed-codec-0.1.0 (c (n "typed-codec") (v "0.1.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 2)) (d (n "des") (r "^0.7.0") (d #t) (k 2)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1.0.0") (d #t) (k 2)))) (h "0hxmfj6gcrk9rkrcg5v2jcl3yxxwfj5wrccyah8vizh28glcy3pb")))

