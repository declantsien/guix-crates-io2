(define-module (crates-io ty pe typecoders) #:use-module (crates-io))

(define-public crate-typecoders-0.1.0 (c (n "typecoders") (v "0.1.0") (h "1lgx7brfqsr7njknwbp4b7kfdaz3si2j2aqcx8n3gkfr02vgsz6d")))

(define-public crate-typecoders-0.1.1 (c (n "typecoders") (v "0.1.1") (h "15hljdijndahxnmifska8qbkymrbgzgp1c07xmydwllpmlg3dkjz")))

(define-public crate-typecoders-0.1.2 (c (n "typecoders") (v "0.1.2") (h "1r31cfslaxzk4h3dvb2bixal8pv9cqm38ax0lbyrw4ylcy1zgycx")))

