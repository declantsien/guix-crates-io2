(define-module (crates-io ty pe typeform-rs) #:use-module (crates-io))

(define-public crate-typeform-rs-0.1.0 (c (n "typeform-rs") (v "0.1.0") (d (list (d (n "isahc") (r "^1.4") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "06mnk9nc0jk7kww79fv13w6y2kbzlnz4pgdsn48cg0ss4ggy3gdm")))

(define-public crate-typeform-rs-0.2.0 (c (n "typeform-rs") (v "0.2.0") (d (list (d (n "eyre") (r "^0.6") (d #t) (k 0)) (d (n "isahc") (r "^1.4") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0s3mj8gp0c9ivk441jsqhwaih2nwkpxrf3xhdhdqfkivngb33f5c")))

(define-public crate-typeform-rs-0.2.1 (c (n "typeform-rs") (v "0.2.1") (d (list (d (n "eyre") (r "^0.6") (d #t) (k 0)) (d (n "isahc") (r "^1.4") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "urlencoding") (r "^1.0") (d #t) (k 0)))) (h "0gajbpcz96dcjn022xama53p8din10qb55yxngsb4djyvwf0x93q")))

