(define-module (crates-io ty pe typed_index_collection) #:use-module (crates-io))

(define-public crate-typed_index_collection-1.0.0 (c (n "typed_index_collection") (v "1.0.0") (d (list (d (n "derivative") (r "^2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "067ab5yqnpf92bsz3dlsa3b074kabzsd9rk27jfn0rs1p83inrni")))

(define-public crate-typed_index_collection-1.0.1 (c (n "typed_index_collection") (v "1.0.1") (d (list (d (n "derivative") (r "^2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0n6pvbgsdla5gdsq8vbnq490dnjbkcdjccs7vhxjamc4ahwc0wpj")))

(define-public crate-typed_index_collection-1.1.0 (c (n "typed_index_collection") (v "1.1.0") (d (list (d (n "derivative") (r "^2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1ncpfs0z4vvsjz40f4n7hb5wzpranvhn42w6m4i2dn8m6ni95j90")))

(define-public crate-typed_index_collection-1.2.1 (c (n "typed_index_collection") (v "1.2.1") (d (list (d (n "derivative") (r "^2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0kahg2fdxxzc4vijs582nmp5lw5yp45ysk1w2g26h5w8g345xr2f")))

(define-public crate-typed_index_collection-1.2.0 (c (n "typed_index_collection") (v "1.2.0") (d (list (d (n "derivative") (r "^2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1yzv7b07bdvz8ykncy6gqiz7svl4kxq9qy63qxdyw9skwh06vx3i")))

(define-public crate-typed_index_collection-2.0.0 (c (n "typed_index_collection") (v "2.0.0") (d (list (d (n "derivative") (r "^2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "13ifs8296wfj0djhzghbr7vd698rj990176rq258j5a4vx860qhs")))

(define-public crate-typed_index_collection-2.1.0 (c (n "typed_index_collection") (v "2.1.0") (d (list (d (n "derivative") (r "^2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0gkv2bhipj718vl5y3zdfjclskjg4jh5vj5jjwbscsnrrpfjsn6m") (f (quote (("expose-inner"))))))

(define-public crate-typed_index_collection-2.2.0 (c (n "typed_index_collection") (v "2.2.0") (d (list (d (n "derivative") (r "^2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "testing_logger") (r "^0.1") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (f (quote ("log"))) (d #t) (k 0)))) (h "0wjcaywgm0l4437msxxyfqg5r02hqgbhsyfxyspja1p6y8vv8m9p") (f (quote (("expose-inner"))))))

(define-public crate-typed_index_collection-2.2.1 (c (n "typed_index_collection") (v "2.2.1") (d (list (d (n "derivative") (r "^2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (f (quote ("log"))) (d #t) (k 0)) (d (n "testing_logger") (r "^0.1") (d #t) (k 2)))) (h "1gk8nx3pc70p1w0gqp4gywcq64xcpwvrmnxfqffgv1bfb3nvlx5g") (f (quote (("expose-inner"))))))

