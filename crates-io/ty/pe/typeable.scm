(define-module (crates-io ty pe typeable) #:use-module (crates-io))

(define-public crate-typeable-0.0.1 (c (n "typeable") (v "0.0.1") (h "16x89aargra535y11vm72km0kn1r0wb88q63gaqzi5ifb0qyydwy")))

(define-public crate-typeable-0.0.2 (c (n "typeable") (v "0.0.2") (h "192fjgb433c0kfhn2ill6iq1dmak2wwjrg4y9s14jlh4wp6mwvni")))

(define-public crate-typeable-0.0.3 (c (n "typeable") (v "0.0.3") (h "1w4548hdnlfvq0a2gjgajmcdqcyprmck9kfc0b9j1p3jy0w7dss0")))

(define-public crate-typeable-0.0.4 (c (n "typeable") (v "0.0.4") (h "1ysyx7xsljqy8ij89bjk7a5h0qkgp5j1a0ivpp44ssd5dv6mqxdq")))

(define-public crate-typeable-0.0.5 (c (n "typeable") (v "0.0.5") (h "0pbxgaslc6zx5yj2z6zsll6mahwpjn3napp131wf6ffbmcjsf9i4")))

(define-public crate-typeable-0.0.6 (c (n "typeable") (v "0.0.6") (h "0kk39kkdm2h62n4rw443gyahdsc2nbcshrbyc1c9kvkqi8wkwm6s")))

(define-public crate-typeable-0.0.7 (c (n "typeable") (v "0.0.7") (h "1xiqj2wq3aqkim51cnldcq1cvf2j9m7gcjjawj37hr5qfljc57j0")))

(define-public crate-typeable-0.0.8 (c (n "typeable") (v "0.0.8") (h "1lsvqanj315wsy4k7h8zp4r2m99c7dj5cdnxzczc0q0s0b3rasvz")))

(define-public crate-typeable-0.0.9 (c (n "typeable") (v "0.0.9") (h "14ixqx4h51hq55gllbmpvqxg2zqkf26a0r4jbh4xgh96xfsg3k9r")))

(define-public crate-typeable-0.1.0 (c (n "typeable") (v "0.1.0") (h "0vvgkhabhkqmcs1nihycd7jgkymda1v2594zw92ddal16vqah3sg")))

(define-public crate-typeable-0.1.1 (c (n "typeable") (v "0.1.1") (h "02kfgz246sbqxmwvkrpb9ddffq97y5346d6qzsn0mkka69qkznws")))

(define-public crate-typeable-0.1.2 (c (n "typeable") (v "0.1.2") (h "11w8dywgnm32hb291izjvh4zjd037ccnkk77ahk63l913zwzc40l")))

