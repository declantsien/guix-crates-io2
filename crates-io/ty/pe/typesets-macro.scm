(define-module (crates-io ty pe typesets-macro) #:use-module (crates-io))

(define-public crate-typesets-macro-0.0.1 (c (n "typesets-macro") (v "0.0.1") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rustfmt-wrapper") (r "^0.1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.130") (d #t) (k 0)) (d (n "serde_tokenstream") (r "^0.1.2") (d #t) (k 0)) (d (n "similar-asserts") (r "^1.1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0.81") (f (quote ("full"))) (d #t) (k 0)))) (h "0gmmx09vm4nmfc8s1bb1sh1xisqq6qvjq12vj42i9dyk2vqhqmss")))

(define-public crate-typesets-macro-0.0.2 (c (n "typesets-macro") (v "0.0.2") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rustfmt-wrapper") (r "^0.1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.130") (d #t) (k 0)) (d (n "serde_tokenstream") (r "^0.1.2") (d #t) (k 0)) (d (n "similar-asserts") (r "^1.1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0.81") (f (quote ("full"))) (d #t) (k 0)))) (h "19hh45x3r6agxs0q2rmvv3wzd80d2y8kl4df651kjwsm3wbwm287")))

