(define-module (crates-io ty pe type-cli-derive) #:use-module (crates-io))

(define-public crate-type-cli-derive-0.0.1 (c (n "type-cli-derive") (v "0.0.1") (d (list (d (n "proc-macro-crate") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive" "full"))) (d #t) (k 0)))) (h "0snfjgjkcwd3c2ipp6r227v6c5a4jqcpdw99bj8nf7h7596clg8f")))

