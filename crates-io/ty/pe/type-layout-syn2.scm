(define-module (crates-io ty pe type-layout-syn2) #:use-module (crates-io))

(define-public crate-type-layout-syn2-0.2.0 (c (n "type-layout-syn2") (v "0.2.0") (d (list (d (n "memoffset") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0.116") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "type-layout-derive") (r "^0.2.0") (d #t) (k 0)))) (h "1wqfg5m8s64z5s9d7ciwfm078s7f24pvxw4qp0q1p32rmzcvlb50") (f (quote (("serde1" "serde")))) (y #t) (r "1.60.0")))

(define-public crate-type-layout-syn2-0.2.1 (c (n "type-layout-syn2") (v "0.2.1") (d (list (d (n "memoffset") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0.116") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "type-layout-derive") (r "^0.2.0") (d #t) (k 0) (p "type-layout-syn2-derive")))) (h "1k9kd9yz86yma0qxv2h3p23ncjd8cq4cmcgrshqhz9z0vwvv2i7y") (f (quote (("serde1" "serde")))) (r "1.60.0")))

