(define-module (crates-io ty pe typename_derive) #:use-module (crates-io))

(define-public crate-typename_derive-0.1.0 (c (n "typename_derive") (v "0.1.0") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "1zm2g0qckk07jx3rx3s8gjnl8ix90z8xwba99vqinfh5srvh3iml")))

(define-public crate-typename_derive-0.1.1 (c (n "typename_derive") (v "0.1.1") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)) (d (n "typename") (r "^0.1.0") (k 2)))) (h "1w9sg8x729vllpxjaqzzkdcl2fla9c5vb6hcgy6f6cvyw7p97qlv")))

(define-public crate-typename_derive-0.1.2 (c (n "typename_derive") (v "0.1.2") (d (list (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)) (d (n "typename") (r "^0.1.0") (k 2)))) (h "1ajdrj94hglymj721id9mn6zlxkz006hdksl9q2q8739ajpzfgjd")))

(define-public crate-typename_derive-0.1.3 (c (n "typename_derive") (v "0.1.3") (d (list (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)) (d (n "typename") (r "^0.1.0") (k 2)))) (h "1dj58rkrgr8rkclg7ygjw1v87zymmilw179ml3xahn1j397b3wzn")))

(define-public crate-typename_derive-0.1.4 (c (n "typename_derive") (v "0.1.4") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "typename") (r "^0.1.1") (k 2)))) (h "02lvyy09yzwwav0aar5jvfwkyn2qflbywwvysjxf4cladqpwz2n6")))

