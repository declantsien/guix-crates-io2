(define-module (crates-io ty pe typenum_loops) #:use-module (crates-io))

(define-public crate-typenum_loops-0.1.0 (c (n "typenum_loops") (v "0.1.0") (d (list (d (n "typenum") (r "^1.5") (d #t) (k 0)))) (h "07q3kn09x7hxp58wwxk066dzpshz6zcpv30aib8zqkdf2awifhs8")))

(define-public crate-typenum_loops-0.1.1 (c (n "typenum_loops") (v "0.1.1") (d (list (d (n "typenum") (r "^1.5") (d #t) (k 0)))) (h "0bx754v8whmigpbc3gb46wcs2qwzh74jnpr58gl1az0vxpakzy2y")))

(define-public crate-typenum_loops-0.1.2 (c (n "typenum_loops") (v "0.1.2") (d (list (d (n "typenum") (r "^1.5") (d #t) (k 0)))) (h "166vss66j6yqndpg5dvm8p67hr7hx18sq8vzbg86sv5c9v0693sg")))

(define-public crate-typenum_loops-0.1.3 (c (n "typenum_loops") (v "0.1.3") (d (list (d (n "typenum") (r "^1.9") (d #t) (k 0)))) (h "0b5d8bb0h72sg6f6azmcgc3qdaadz84052hqbvjvprvzfbczdg4m")))

(define-public crate-typenum_loops-0.2.0 (c (n "typenum_loops") (v "0.2.0") (d (list (d (n "typenum") (r "^1.9") (d #t) (k 0)))) (h "0i6pygbinbqrn0s51gsl6pbw0q4wpg4f1brvjb7x4a6ijjd2d9z8")))

(define-public crate-typenum_loops-0.3.0 (c (n "typenum_loops") (v "0.3.0") (d (list (d (n "typenum") (r "^1.9") (d #t) (k 0)))) (h "1r8c7w9qzyyqjlncdlibk1zjza5dglnk478nw8dph07qzrbi5wb0")))

