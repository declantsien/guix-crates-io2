(define-module (crates-io ty pe typechain-macros) #:use-module (crates-io))

(define-public crate-typechain-macros-0.1.0 (c (n "typechain-macros") (v "0.1.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1x9qlfw35kh23a9lwx0xyx5fnag40vs5c5fj1hv37n6vglgabyf7")))

