(define-module (crates-io ty pe typesum-macros) #:use-module (crates-io))

(define-public crate-typesum-macros-0.1.0 (c (n "typesum-macros") (v "0.1.0") (d (list (d (n "convert_case") (r "^0.6.0") (o #t) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.32") (d #t) (k 0)) (d (n "syn") (r "^2.0.27") (d #t) (k 0)))) (h "0qrpwjam8pda46njmlgnfcqs73jrmxaywddslypvd6lvk97gl8zn") (f (quote (("sumtype" "syn/extra-traits" "convert_case") ("kinded") ("default" "kinded" "sumtype"))))))

(define-public crate-typesum-macros-0.2.0 (c (n "typesum-macros") (v "0.2.0") (d (list (d (n "convert_case") (r "^0.6.0") (o #t) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.32") (d #t) (k 0)) (d (n "syn") (r "^2.0.27") (d #t) (k 0)))) (h "17nlqb5rnhjkn47qhkbc12mxg5yfh916pf3zb2blahks4yc5gbr7") (f (quote (("sumtype" "syn/extra-traits" "convert_case") ("kinded") ("default" "kinded" "sumtype"))))))

