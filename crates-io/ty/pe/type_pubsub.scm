(define-module (crates-io ty pe type_pubsub) #:use-module (crates-io))

(define-public crate-type_pubsub-0.1.0 (c (n "type_pubsub") (v "0.1.0") (d (list (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("rt-multi-thread" "macros" "sync" "time"))) (d #t) (k 0)))) (h "0qmqpafdld2hq9zgihmn206nd4vjvinhqwvi02h9ngpj23zfqnbp")))

(define-public crate-type_pubsub-0.2.0 (c (n "type_pubsub") (v "0.2.0") (d (list (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("rt-multi-thread" "macros" "sync" "time"))) (d #t) (k 0)))) (h "1mp49jim9r4k4i6sk49az8y5crl0jmpxcj0s921ff2bsby1d1v5i")))

