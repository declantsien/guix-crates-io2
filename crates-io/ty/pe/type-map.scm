(define-module (crates-io ty pe type-map) #:use-module (crates-io))

(define-public crate-type-map-0.2.0 (c (n "type-map") (v "0.2.0") (d (list (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)))) (h "08xg0hfn7lflkb4kpfbwmqjv28pywhnn460pciijfqx8wz7553lr")))

(define-public crate-type-map-0.3.0 (c (n "type-map") (v "0.3.0") (d (list (d (n "fxhash") (r "^0.2") (d #t) (k 0)))) (h "05v7cdylx4gj7aip5i89hh98wz4pqfib1qziq6apscjc8yql29wx")))

(define-public crate-type-map-0.4.0 (c (n "type-map") (v "0.4.0") (d (list (d (n "rustc-hash") (r "^1") (d #t) (k 0)))) (h "0ilsqq7pcl3k9ggxv2x5fbxxfd6x7ljsndrhc38jmjwnbr63dlxn")))

(define-public crate-type-map-0.5.0 (c (n "type-map") (v "0.5.0") (d (list (d (n "rustc-hash") (r "^1") (d #t) (k 0)))) (h "17qaga12nkankr7hi2mv43f4lnc78hg480kz6j9zmy4g0h28ddny")))

