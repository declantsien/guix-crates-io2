(define-module (crates-io ty pe type_utils) #:use-module (crates-io))

(define-public crate-type_utils-0.1.0 (c (n "type_utils") (v "0.1.0") (d (list (d (n "nullable-result") (r "^0.7.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("full"))) (d #t) (k 0)))) (h "1whq4il8swllkr3cph9jmbhqs2gjl0bzh2fppnasjzn4y91x3b41")))

(define-public crate-type_utils-0.2.0 (c (n "type_utils") (v "0.2.0") (d (list (d (n "nullable-result") (r "^0.7.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("full"))) (d #t) (k 0)))) (h "12wxzml69yw0s15v4f61zg6lifxkm4mdxi9iqq9lnhmzhgpgzcrr")))

(define-public crate-type_utils-0.3.0 (c (n "type_utils") (v "0.3.0") (d (list (d (n "nullable-result") (r "^0.7.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("full"))) (d #t) (k 0)))) (h "1c6liwb8kh40bsa437wfm7pknj09asn84wkrxlwb03n9rhzpp90n")))

