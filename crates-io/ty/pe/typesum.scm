(define-module (crates-io ty pe typesum) #:use-module (crates-io))

(define-public crate-typesum-0.1.0 (c (n "typesum") (v "0.1.0") (d (list (d (n "typesum-macros") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "0clfacq2zda34shagsvy56csq0a1qfldcp9sbn35zz83hihrwczn") (f (quote (("sumtype" "typesum-macros/sumtype") ("kinded" "typesum-macros/kinded") ("default" "sumtype" "kinded"))))))

(define-public crate-typesum-0.2.0 (c (n "typesum") (v "0.2.0") (d (list (d (n "impl-tools") (r "^0.9.0") (d #t) (k 0)) (d (n "typesum-macros") (r "^0.2.0") (k 0)))) (h "0ayyyh71n3p14xjq8ry1k9scdy1vjy8mx0v93cdpd9n2crbh3sdy") (f (quote (("sumtype" "typesum-macros/sumtype") ("kinded" "typesum-macros/kinded") ("default" "sumtype" "kinded"))))))

