(define-module (crates-io ty pe type_ulid) #:use-module (crates-io))

(define-public crate-type_ulid-0.0.0 (c (n "type_ulid") (v "0.0.0") (h "0imbwrvrrvav9xrilxb25hgam98rnsglj7n5a0n505dn7gn72b1n")))

(define-public crate-type_ulid-0.1.0 (c (n "type_ulid") (v "0.1.0") (d (list (d (n "type_ulid_macros") (r "^0.1.0") (d #t) (k 0)) (d (n "ulid") (r "^1.0.0") (k 0)))) (h "0w21smmgzpbwamk83nrchy8ichzy77m2s04li33y7c9cgq8dwjaa") (f (quote (("std") ("default" "std"))))))

(define-public crate-type_ulid-0.2.0 (c (n "type_ulid") (v "0.2.0") (d (list (d (n "type_ulid_macros") (r "^0.2.0") (d #t) (k 0)) (d (n "ulid") (r "^1.0") (k 0)))) (h "1bi4ihkkpdqw6xg10v28q54pb2m713w6wwrfh96v1kycpns7va75") (f (quote (("std") ("default" "std"))))))

