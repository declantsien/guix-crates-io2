(define-module (crates-io ty pe typed-headers) #:use-module (crates-io))

(define-public crate-typed-headers-0.1.0 (c (n "typed-headers") (v "0.1.0") (d (list (d (n "base64") (r "^0.9") (d #t) (k 0)) (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "http") (r "^0.1") (d #t) (k 0)) (d (n "mime") (r "^0.3") (d #t) (k 0)))) (h "1swny4qfhl4lz1arh61b4w5jmpfxq3sx0w4mgi685a7xcs8cifcz")))

(define-public crate-typed-headers-0.1.1 (c (n "typed-headers") (v "0.1.1") (d (list (d (n "base64") (r "^0.10") (d #t) (k 0)) (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "http") (r "^0.1.15") (d #t) (k 0)) (d (n "mime") (r "^0.3") (d #t) (k 0)))) (h "0g40nlq5iw0zxhwb7nfmfbr9m86abgwwhxwhzrm10nfq6bsmlvxx")))

(define-public crate-typed-headers-0.2.0 (c (n "typed-headers") (v "0.2.0") (d (list (d (n "base64") (r "^0.11") (d #t) (k 0)) (d (n "bytes") (r "^0.5.2") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "mime") (r "^0.3") (d #t) (k 0)))) (h "0jm2xzvvml3a9hhvzf9q4v22l5ifrxrx2kspy7aymknckqgacy9i")))

