(define-module (crates-io ty pe typescript-wasm-bindgen-macros) #:use-module (crates-io))

(define-public crate-typescript-wasm-bindgen-macros-0.1.0 (c (n "typescript-wasm-bindgen-macros") (v "0.1.0") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "typescript-wasm-bindgen-codegen") (r "^0.1.0") (d #t) (k 0)))) (h "1jv2na893yckh0k2cidzsiaii8s5ldp5hpbryj1prhp4h7ldc7wm")))

(define-public crate-typescript-wasm-bindgen-macros-0.1.1 (c (n "typescript-wasm-bindgen-macros") (v "0.1.1") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "typescript-wasm-bindgen-codegen") (r "^0.1.1") (d #t) (k 0)))) (h "10rnqbjbszj6fcgl20kqfn0ia2gdnmriv008z7k2q4ikzsyk1fqv")))

