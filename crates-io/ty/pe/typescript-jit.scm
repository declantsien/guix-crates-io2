(define-module (crates-io ty pe typescript-jit) #:use-module (crates-io))

(define-public crate-typescript-jit-0.1.0 (c (n "typescript-jit") (v "0.1.0") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "llvm-sys") (r "^140") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "pest") (r "^2.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.3") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)))) (h "1p77227ra81ml87bl42nfhnvfvpjxi096q0j2nmzkjpnyhcrr49i") (f (quote (("trace") ("default" "trace")))) (y #t)))

(define-public crate-typescript-jit-0.0.1 (c (n "typescript-jit") (v "0.0.1") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "llvm-sys") (r "^140") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)) (d (n "typescript-ast") (r "^0.0.1") (d #t) (k 0)))) (h "0n2g2cgyhqad6pzw2f6fj5vn9br9c5k99djhfan7iyxcds46fy04") (f (quote (("trace") ("default")))) (y #t)))

