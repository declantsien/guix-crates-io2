(define-module (crates-io ty pe typederror) #:use-module (crates-io))

(define-public crate-typederror-0.1.0 (c (n "typederror") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "assert_matches") (r "^1") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 2)))) (h "0fj07g4q85r3n6f6v026p2qagg49vbs83wi9zphvry5ysxa21j1b")))

(define-public crate-typederror-0.2.0 (c (n "typederror") (v "0.2.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "assert_matches") (r "^1") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 2)))) (h "18apq5ap8841vz6pn8sa319p65qbh4vc8pj1by3bgx2rk9z3r738")))

(define-public crate-typederror-0.2.1 (c (n "typederror") (v "0.2.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "assert_matches") (r "^1") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 2)))) (h "0y48bn36w2rf3f4zd2bv503cs2ls7swswzh8fz2fisc8k5lli3xb")))

(define-public crate-typederror-0.2.2 (c (n "typederror") (v "0.2.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "assert_matches") (r "^1") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 2)))) (h "1my8prjx0hd4nc7qp98b1z9q4zvmgg2lf648dj2qn580ixh5n1vk")))

