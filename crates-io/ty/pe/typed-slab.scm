(define-module (crates-io ty pe typed-slab) #:use-module (crates-io))

(define-public crate-typed-slab-0.1.0 (c (n "typed-slab") (v "0.1.0") (d (list (d (n "slab") (r "^0.4.2") (d #t) (k 0)))) (h "1kkckrjkh8j86wq59265sxygsh61g27r2n9jkxi3yr7bhpj78vf1")))

(define-public crate-typed-slab-0.1.1 (c (n "typed-slab") (v "0.1.1") (d (list (d (n "slab") (r "^0.4.2") (d #t) (k 0)))) (h "164ikqwh3pakm2xlfihaf9062fknjhrqvp035p737xd9a53wl0yv")))

(define-public crate-typed-slab-0.1.2 (c (n "typed-slab") (v "0.1.2") (d (list (d (n "slab") (r "^0.4.2") (d #t) (k 0)))) (h "0hx95y50nfpm9s0rgbqrp07cfabysx0y8a05nw56b6nxnbqsv48v")))

(define-public crate-typed-slab-0.1.3 (c (n "typed-slab") (v "0.1.3") (d (list (d (n "slab") (r "^0.4.3") (d #t) (k 0)))) (h "1p5vb41xsz7vngaqpgy8kxxhyk32aqxhqc294b54azsyypl1d6yj")))

(define-public crate-typed-slab-0.1.4 (c (n "typed-slab") (v "0.1.4") (d (list (d (n "slab") (r "^0.4.4") (d #t) (k 0)))) (h "05vvbjwpdilfg5ncsb719jjrnsqmix0qbh6xi06kx3baips0rz87")))

(define-public crate-typed-slab-0.1.5 (c (n "typed-slab") (v "0.1.5") (d (list (d (n "slab") (r "^0.4.5") (d #t) (k 0)))) (h "1krz996b8za9yyzsx6vz5x2wcc8x70d8fp49jk6b3nj3nxpzh50q")))

(define-public crate-typed-slab-0.1.6 (c (n "typed-slab") (v "0.1.6") (d (list (d (n "slab") (r "^0.4.5") (d #t) (k 0)))) (h "0fa7v95a12z1bkml8nnjvbka8fgzfc7s0bxglfkngr8bf691lb5q")))

(define-public crate-typed-slab-0.1.7 (c (n "typed-slab") (v "0.1.7") (d (list (d (n "slab") (r "^0.4.5") (d #t) (k 0)))) (h "1f7nrj6zzyvracyr4k41cv5nahh0gygw02d94ykw30d5ahk4a08b")))

(define-public crate-typed-slab-0.1.8 (c (n "typed-slab") (v "0.1.8") (d (list (d (n "slab") (r "^0.4.6") (d #t) (k 0)))) (h "0psr92hhga59yryy84dsvz86cblwmdv68gllfizsmwan6bg9kfj1")))

(define-public crate-typed-slab-0.2.0 (c (n "typed-slab") (v "0.2.0") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "slab") (r "^0.4.9") (d #t) (k 0)))) (h "16hi6q7jfkn622jm52nnq7zmgi91cgwins585gnsnfwlxp3l59l3")))

