(define-module (crates-io ty pe typed_arch) #:use-module (crates-io))

(define-public crate-typed_arch-0.1.0 (c (n "typed_arch") (v "0.1.0") (h "1ml0j8i3jh9jpdfm5iw94zybyhjcymjqg6gy9cwmw3awdqlj5f29")))

(define-public crate-typed_arch-0.1.1 (c (n "typed_arch") (v "0.1.1") (h "192f036nviipk3qmh0v1ajgyf581ks64rcbm6gr6cd1gz0cdssa5")))

