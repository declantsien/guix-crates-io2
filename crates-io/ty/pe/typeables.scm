(define-module (crates-io ty pe typeables) #:use-module (crates-io))

(define-public crate-typeables-1.0.0 (c (n "typeables") (v "1.0.0") (h "01d10b50drywnwrxar7x3zijqr83nxj4bqrjris2jqdiaf42q5jy")))

(define-public crate-typeables-1.1.0 (c (n "typeables") (v "1.1.0") (h "0fzazq4c2vspanbpv7q98j7dsympa92fn1bbpwiaf7cy4cgij8j3")))

(define-public crate-typeables-1.2.0 (c (n "typeables") (v "1.2.0") (h "1ma6kry4yiws9l81yi2hl0z365qzd7kv2mhjywzx7x70g35scvr1")))

(define-public crate-typeables-1.3.0 (c (n "typeables") (v "1.3.0") (h "0y5k14ns38wp25jgk1y9wspgw4b73ixd64x94kp9i3n0n31fqhy9")))

(define-public crate-typeables-2.0.0 (c (n "typeables") (v "2.0.0") (h "0rq35i9hlj824zszhadn4n9liidlvkd839fwzbih397zsyady5gh") (y #t)))

(define-public crate-typeables-2.0.3 (c (n "typeables") (v "2.0.3") (h "07xkgksj8xqvaf13129xingan7ffn50a91jfg6kxc0cjy3x8bi8f")))

(define-public crate-typeables-2.2.0 (c (n "typeables") (v "2.2.0") (h "15mfn5c269755i52152d4jh2jfamjqv7iij8zc71slh74qvi6yri")))

(define-public crate-typeables-2.3.0 (c (n "typeables") (v "2.3.0") (h "05vk8888k2jprbdaaik9n53j9z9l4fxjs8lsrazdw6yjvi2kig61")))

