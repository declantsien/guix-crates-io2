(define-module (crates-io ty pe typewit) #:use-module (crates-io))

(define-public crate-typewit-0.0.1 (c (n "typewit") (v "0.0.1") (h "1hifmdkdcl4b9d6hz4krab7p6gxiyv75rkqx302vmb8xdqbwb6wj") (f (quote (("nightly_mut_refs" "mut_refs") ("mut_refs") ("docsrs") ("alloc")))) (r "1.57.0")))

(define-public crate-typewit-1.0.0 (c (n "typewit") (v "1.0.0") (d (list (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0ba31fb9fnid59lprjvgbr9s6ivzi70x2n6hhnjvxd6zfqyhmqvw") (f (quote (("nightly_mut_refs" "mut_refs") ("mut_refs") ("docsrs") ("alloc") ("__ui_tests") ("__rust_stable")))) (r "1.61.0")))

(define-public crate-typewit-1.1.0 (c (n "typewit") (v "1.1.0") (d (list (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1s0s4x9awnxfrjp2sn2v61hdlgqiaj94pgqqz0swv5x7cmrgmvln") (f (quote (("rust_stable" "rust_1_61") ("rust_1_61") ("nightly_mut_refs" "mut_refs") ("mut_refs") ("docsrs") ("alloc") ("__ui_tests")))) (r "1.57.0")))

(define-public crate-typewit-1.1.1 (c (n "typewit") (v "1.1.1") (d (list (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "18w6zq9rmvnyifzw5x2hqdgjgwxb287zfqlnsgmdcgsxcvm6wdi2") (f (quote (("rust_stable" "rust_1_61") ("rust_1_61") ("nightly_mut_refs" "mut_refs") ("mut_refs") ("docsrs") ("alloc") ("__ui_tests")))) (r "1.57.0")))

(define-public crate-typewit-1.2.0 (c (n "typewit") (v "1.2.0") (d (list (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0spzhqfqws3h6kfbn1nb8wy93myz0zxgi9d56rnsq3ybh6k05pg6") (f (quote (("rust_stable" "rust_1_61") ("rust_1_61") ("nightly_mut_refs" "mut_refs") ("mut_refs") ("docsrs") ("default" "const_marker") ("const_marker") ("alloc") ("__ui_tests")))) (r "1.57.0")))

(define-public crate-typewit-1.3.0 (c (n "typewit") (v "1.3.0") (d (list (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0mpzcid95ds4smnmkhgn24fgq7nj4gsz7krg7f89by9hg35v8lkw") (f (quote (("rust_stable" "rust_1_61") ("rust_1_61") ("nightly_mut_refs" "mut_refs") ("mut_refs") ("docsrs") ("default" "const_marker") ("const_marker") ("alloc") ("__ui_tests")))) (r "1.57.0")))

(define-public crate-typewit-1.4.0 (c (n "typewit") (v "1.4.0") (d (list (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0bccy2fx0445dpn06iabilnk0d44zlm1h9f0rjl82c4g9l6s2qa0") (f (quote (("rust_stable" "rust_1_61") ("rust_1_61") ("nightly_mut_refs" "mut_refs") ("mut_refs") ("docsrs") ("default" "const_marker") ("const_marker") ("alloc") ("__ui_tests")))) (r "1.57.0")))

(define-public crate-typewit-1.5.0 (c (n "typewit") (v "1.5.0") (d (list (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1zw9am4dkdrisdi8p685mfpjyimq2d76qghay411wzf7ghsywp1y") (f (quote (("rust_stable" "rust_1_61") ("rust_1_61") ("nightly_mut_refs" "mut_refs") ("mut_refs") ("docsrs") ("default" "const_marker") ("const_marker") ("alloc") ("__ui_tests")))) (r "1.57.0")))

(define-public crate-typewit-1.6.0 (c (n "typewit") (v "1.6.0") (d (list (d (n "arrayvec") (r "^0.7") (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0njrfj2scpzdh857xzmak1a1q4kjiqnca95ydzngdwny326zanll") (f (quote (("rust_stable" "rust_1_61") ("rust_1_61") ("nightly_mut_refs" "rust_stable" "mut_refs") ("mut_refs") ("docsrs") ("default" "const_marker") ("const_marker") ("alloc") ("adt_const_marker" "rust_stable" "const_marker") ("__ui_tests")))) (r "1.57.0")))

(define-public crate-typewit-1.7.0 (c (n "typewit") (v "1.7.0") (d (list (d (n "arrayvec") (r "^0.7") (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0dgd018nl8ss3sxc6vbr5m76lqf5jwd5vq30abr1ir0sl27b99ls") (f (quote (("rust_stable" "rust_1_65") ("rust_1_65" "rust_1_61") ("rust_1_61") ("nightly_mut_refs" "rust_stable" "mut_refs") ("mut_refs") ("docsrs") ("default" "const_marker") ("const_marker") ("alloc") ("adt_const_marker" "rust_stable" "const_marker") ("__ui_tests")))) (r "1.57.0")))

(define-public crate-typewit-1.8.0 (c (n "typewit") (v "1.8.0") (d (list (d (n "arrayvec") (r "^0.7") (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)) (d (n "typewit_proc_macros") (r "=1.8.1") (o #t) (d #t) (k 0)))) (h "0ajrz5y5l18bd4k4mrwsbwclb7hwxd9s7a50fj13i9zrqnfacyb7") (f (quote (("rust_stable" "rust_1_65") ("rust_1_65" "rust_1_61") ("rust_1_61") ("proc_macros" "typewit_proc_macros") ("nightly_mut_refs" "mut_refs") ("mut_refs" "rust_stable") ("docsrs") ("default" "proc_macros") ("const_marker") ("alloc") ("adt_const_marker" "rust_stable") ("__ui_tests")))) (r "1.57.0")))

(define-public crate-typewit-1.9.0 (c (n "typewit") (v "1.9.0") (d (list (d (n "arrayvec") (r "^0.7") (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)) (d (n "typewit_proc_macros") (r "=1.8.1") (o #t) (d #t) (k 0)))) (h "093fgb1q5n48vr4nj3hggbhfi6jzab5048scs6jz1ynalgk9myy6") (f (quote (("rust_stable" "rust_1_65") ("rust_1_65" "rust_1_61") ("rust_1_61") ("proc_macros" "typewit_proc_macros") ("nightly_mut_refs" "mut_refs") ("mut_refs" "rust_stable") ("docsrs") ("default" "proc_macros") ("const_marker") ("alloc") ("adt_const_marker" "rust_stable") ("__ui_tests")))) (r "1.57.0")))

