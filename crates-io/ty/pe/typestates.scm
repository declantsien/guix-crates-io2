(define-module (crates-io ty pe typestates) #:use-module (crates-io))

(define-public crate-typestates-0.1.0-alpha (c (n "typestates") (v "0.1.0-alpha") (h "0r8bwryw86g9b5ywlm1abym0pbvrhn0vj9p388lxbp700s916lqb") (y #t)))

(define-public crate-typestates-0.1.1-alpha (c (n "typestates") (v "0.1.1-alpha") (h "0y12s1ialqh0rp92x242gig3yysvwcnvqy1m7d4lsdxpv3zfkq76") (y #t)))

(define-public crate-typestates-0.1.2-alpha (c (n "typestates") (v "0.1.2-alpha") (h "1hj7l4d6dq98khwgqsrrbqg40db1n56v3s6i3yz6rlcwh1bh0b7n") (y #t)))

(define-public crate-typestates-0.1.2-MOVED (c (n "typestates") (v "0.1.2-MOVED") (h "1rl204chsx957immbqj3xg5k8d6vlfc73nw46lrlhwkh4fmjan46")))

