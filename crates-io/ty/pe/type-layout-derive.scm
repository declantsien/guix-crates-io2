(define-module (crates-io ty pe type-layout-derive) #:use-module (crates-io))

(define-public crate-type-layout-derive-0.1.0 (c (n "type-layout-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.21") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.40") (d #t) (k 0)))) (h "06xiy6fx4d16kndfkk109a99slww3wyrpc8iyqibr5p592p7yqx0")))

(define-public crate-type-layout-derive-0.2.0 (c (n "type-layout-derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.21") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.40") (d #t) (k 0)))) (h "1kf6fnns7x9k5hhr3hffzczsac79m13yzi9i9cy9f878dkv1qjpw")))

