(define-module (crates-io ty pe type-equalities) #:use-module (crates-io))

(define-public crate-type-equalities-0.1.0 (c (n "type-equalities") (v "0.1.0") (h "0qn9g1rjmmnvlgmjkz4l7c83s5sbjdjar16vb44pbdgrbaad4ji9") (f (quote (("test-for-type-equality") ("std") ("default" "std"))))))

(define-public crate-type-equalities-0.1.1 (c (n "type-equalities") (v "0.1.1") (h "0mld261rkv576hvrvcldwi0q4vlccb7cd7xx1kz8fm2wx5xip7ls") (f (quote (("test-for-type-equality") ("std") ("default" "std"))))))

(define-public crate-type-equalities-0.1.2 (c (n "type-equalities") (v "0.1.2") (h "0cas9msj5y58dic9v4rj237z5g0ibz1azbxd4622sgq0jn5sagfn") (f (quote (("test-for-type-equality") ("std") ("default" "std"))))))

(define-public crate-type-equalities-0.1.3 (c (n "type-equalities") (v "0.1.3") (h "0vfpha6f37cc31g7c74m04m9c7h5iag8v4vpiyr22fpg4dhnifja") (f (quote (("test-for-type-equality") ("std") ("default" "std"))))))

(define-public crate-type-equalities-0.1.4 (c (n "type-equalities") (v "0.1.4") (h "1yz6ga9d2nzx82922286v6v2r05ghs5sds2qgfnxwbg8l3ak9qjf") (f (quote (("test-for-type-equality") ("std") ("default" "std"))))))

(define-public crate-type-equalities-0.2.0 (c (n "type-equalities") (v "0.2.0") (h "1hzg1117mg0gkj5h879w1cyg54pd9lir9pd3dvxx9bz9779wvc9g") (f (quote (("test-for-type-equality") ("std") ("default" "std"))))))

(define-public crate-type-equalities-0.3.0 (c (n "type-equalities") (v "0.3.0") (d (list (d (n "rustversion") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 1)))) (h "1pf86g91yzarc5r0myss641hzqfx56zhp2h5cljgk2nlx2pyczs1") (f (quote (("test-for-type-equality") ("default" "alloc") ("alloc"))))))

(define-public crate-type-equalities-0.3.1 (c (n "type-equalities") (v "0.3.1") (d (list (d (n "rustversion") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 1)))) (h "1z7xqyfjsk91x4f4indnllzq8i52gx66ggk0rzpp1f71ff0f3x2a") (f (quote (("test-for-type-equality") ("default" "alloc") ("alloc"))))))

