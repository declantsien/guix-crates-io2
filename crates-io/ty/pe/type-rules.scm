(define-module (crates-io ty pe type-rules) #:use-module (crates-io))

(define-public crate-type-rules-0.1.0 (c (n "type-rules") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 2)) (d (n "claim") (r "^0.5.0") (d #t) (k 2)) (d (n "regex_helper") (r "^1.5.4") (o #t) (d #t) (k 0) (p "regex")) (d (n "type-rules-derive") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "1qs5vpz2cqah0vp409ggzjpzph5bj2dws4nvyp7wpsz5my074h7y") (f (quote (("regex" "regex_helper") ("derive" "type-rules-derive"))))))

(define-public crate-type-rules-0.1.1 (c (n "type-rules") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 2)) (d (n "claim") (r "^0.5.0") (d #t) (k 2)) (d (n "regex_helper") (r "^1.5.4") (o #t) (d #t) (k 0) (p "regex")) (d (n "type-rules-derive") (r "^0.1.1") (o #t) (d #t) (k 0)))) (h "0yqf7h0vg1rwwd6f6z7rc07qsavhkkiir9fda6h2yry8h94b51yi") (f (quote (("regex" "regex_helper") ("derive" "type-rules-derive"))))))

(define-public crate-type-rules-0.1.2 (c (n "type-rules") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 2)) (d (n "claim") (r "^0.5.0") (d #t) (k 2)) (d (n "regex_helper") (r "^1.5.4") (o #t) (d #t) (k 0) (p "regex")) (d (n "type-rules-derive") (r "^0.1.2") (o #t) (d #t) (k 0)))) (h "06fgg0vy0ks9bm81g8fdgrgjv69nifh1s0b9gn3kd9cnxxq8rrgz") (f (quote (("regex" "regex_helper") ("derive" "type-rules-derive"))))))

(define-public crate-type-rules-0.2.0 (c (n "type-rules") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 2)) (d (n "claim") (r "^0.5.0") (d #t) (k 2)) (d (n "regex_helper") (r "^1.5.4") (o #t) (d #t) (k 0) (p "regex")) (d (n "type-rules-derive") (r "^0.2.0") (o #t) (d #t) (k 0)))) (h "19q7ydnzwkd2lrdwhbp96sphnj3ri1aswx6rakbsly7klmz4x30a") (f (quote (("regex" "regex_helper") ("derive" "type-rules-derive"))))))

(define-public crate-type-rules-0.2.1 (c (n "type-rules") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 2)) (d (n "claim") (r "^0.5.0") (d #t) (k 2)) (d (n "regex_helper") (r "^1.5.4") (o #t) (d #t) (k 0) (p "regex")) (d (n "type-rules-derive") (r "^0.2.1") (o #t) (d #t) (k 0)))) (h "15z8wvladmr49vcbj8scw0ncycgpmrfqql0vgq16wl2qr2s0h1zy") (f (quote (("regex" "regex_helper") ("derive" "type-rules-derive"))))))

(define-public crate-type-rules-0.2.2 (c (n "type-rules") (v "0.2.2") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 2)) (d (n "claim") (r "^0.5.0") (d #t) (k 2)) (d (n "regex_helper") (r "^1.5.4") (o #t) (d #t) (k 0) (p "regex")) (d (n "type-rules-derive") (r "^0.2.2") (o #t) (d #t) (k 0)))) (h "1jqhpjma8n0kgcf255ip3zppjp74yqpq6vj7p0ywn947cafspdc2") (f (quote (("regex" "regex_helper") ("derive" "type-rules-derive"))))))

(define-public crate-type-rules-0.2.3 (c (n "type-rules") (v "0.2.3") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 2)) (d (n "claim") (r "^0.5.0") (d #t) (k 2)) (d (n "regex") (r "^1.5.4") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.137") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 2)) (d (n "type-rules-derive") (r "^0.2.3") (o #t) (d #t) (k 0)))) (h "0j99fn39nj8xwy9j4p6v9anxgwy2z9lfc8y8890rvic9vrq8wfiy") (f (quote (("derive" "type-rules-derive"))))))

