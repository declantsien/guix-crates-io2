(define-module (crates-io ty pe typed-format-version) #:use-module (crates-io))

(define-public crate-typed-format-version-0.1.0 (c (n "typed-format-version") (v "0.1.0") (d (list (d (n "quick-error") (r "^2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "1v47fwwxr1yp357idmw0rpwkjgmk7j5yhg4ah5ck56g0sl72mcqm")))

(define-public crate-typed-format-version-0.2.0 (c (n "typed-format-version") (v "0.2.0") (d (list (d (n "quick-error") (r "^2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 2)) (d (n "toml") (r "^0.5.9") (d #t) (k 2)) (d (n "tracing") (r "^0.1.36") (d #t) (k 2)) (d (n "tracing-test") (r "^0.2.3") (d #t) (k 2)))) (h "0f9kvq8a6pxkagkdqn586jbz51jg5ckxlbq29mkpi3827q5rvgbc")))

(define-public crate-typed-format-version-0.2.1 (c (n "typed-format-version") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 2)) (d (n "tracing") (r "^0.1.36") (d #t) (k 2)) (d (n "tracing-test") (r "^0.2.3") (d #t) (k 2)))) (h "18ff6rl8xzl4ycjd6cimjr4rav61arzi784jjwnvr6iqkqbig41v")))

