(define-module (crates-io ty pe typestuff) #:use-module (crates-io))

(define-public crate-typestuff-0.1.0 (c (n "typestuff") (v "0.1.0") (d (list (d (n "typestuff-macro") (r "^0.1.0") (d #t) (k 0)))) (h "16fvw3d1qajmyzh9pffpkkb4sy8xll9svzh7xrz20v3g1aqrbnz2")))

(define-public crate-typestuff-0.2.0 (c (n "typestuff") (v "0.2.0") (d (list (d (n "typestuff-macro") (r "^0.2.0") (d #t) (k 0)))) (h "0hxxfi8r2455g7wjsb7bj5s3bgbwcfs97303x2gdr6q4hqybdzrq")))

(define-public crate-typestuff-0.3.0 (c (n "typestuff") (v "0.3.0") (d (list (d (n "typestuff-macro") (r "^0.3.0") (d #t) (k 0)))) (h "05zjl8z56pf1hsbq9sgbhvxvd866q5alixs2zzsvrp833nxqdn3h")))

