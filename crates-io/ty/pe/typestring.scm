(define-module (crates-io ty pe typestring) #:use-module (crates-io))

(define-public crate-typestring-0.0.0 (c (n "typestring") (v "0.0.0") (h "0y8vvqwxam69r0qrllyfphh581lh9p3718p5ji5xil3mpgig4yjy")))

(define-public crate-typestring-0.1.0 (c (n "typestring") (v "0.1.0") (h "1727snnxxqwzb22kxw7flb936lpvp0hqmc158s1zkcy8rv3bnzfk")))

