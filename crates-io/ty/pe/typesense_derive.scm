(define-module (crates-io ty pe typesense_derive) #:use-module (crates-io))

(define-public crate-typesense_derive-0.0.0 (c (n "typesense_derive") (v "0.0.0") (d (list (d (n "proc-macro2") (r "^1.0.26") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "18wj802bqb7rn69xb24krh6a591wndnkqcwd7yy47sk7svchcj86")))

(define-public crate-typesense_derive-0.1.0 (c (n "typesense_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.26") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0wkapwps3gjagfa6cw3a6zwp1gcmhrybc5wa3wrjf7rjgfqvj8i7")))

