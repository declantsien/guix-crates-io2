(define-module (crates-io ty pe typed-qb-procmacro) #:use-module (crates-io))

(define-public crate-typed-qb-procmacro-0.1.0 (c (n "typed-qb-procmacro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.30") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("full" "extra-traits" "printing"))) (d #t) (k 0)))) (h "19c1i7dznc15yh31a0zfsqzaczkhm7dawir2hh04zngmz7h3wwjb")))

(define-public crate-typed-qb-procmacro-0.1.1 (c (n "typed-qb-procmacro") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.30") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("full" "extra-traits" "printing"))) (d #t) (k 0)))) (h "1j5y1av5jbj7wnrm21hq00rdb8z3rn9zk7g5vwxa1r1cv7q7hbpz")))

(define-public crate-typed-qb-procmacro-0.2.0 (c (n "typed-qb-procmacro") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.30") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("full" "extra-traits" "printing"))) (d #t) (k 0)))) (h "16bsvyip4h8429yqmjji4bb4yar3qzk6qdxs7wijlxrrsf5q1yx1")))

