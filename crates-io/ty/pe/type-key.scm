(define-module (crates-io ty pe type-key) #:use-module (crates-io))

(define-public crate-type-key-0.1.0 (c (n "type-key") (v "0.1.0") (h "1p2za7m354c83ahyrxj7h56az91vqb14ywl6nj5sqj1mzzgjmwfa")))

(define-public crate-type-key-1.0.0 (c (n "type-key") (v "1.0.0") (h "0h7ik4pcyhiapg915ig9q3p4gmlv9lg9ss1bzal5p64aa6b8gjfb")))

