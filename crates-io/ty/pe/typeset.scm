(define-module (crates-io ty pe typeset) #:use-module (crates-io))

(define-public crate-typeset-0.1.0 (c (n "typeset") (v "0.1.0") (d (list (d (n "bumpalo") (r "^3") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "pest") (r "^2") (d #t) (k 0)) (d (n "pest_derive") (r "^2") (d #t) (k 0)))) (h "0a125r51hqd86lanvd71v4jv7nidvwyhxd37ad9hqaf0zzib820i") (y #t)))

(define-public crate-typeset-1.0.0 (c (n "typeset") (v "1.0.0") (d (list (d (n "bumpalo") (r "^3") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "pest") (r "^2") (d #t) (k 0)) (d (n "pest_derive") (r "^2") (d #t) (k 0)))) (h "02gv5mx17wwq1b8242v8v8m3wi70gv5lcnl7difbrdzabk0vq7n0") (y #t)))

(define-public crate-typeset-1.0.1 (c (n "typeset") (v "1.0.1") (d (list (d (n "bumpalo") (r "^3") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "pest") (r "^2") (d #t) (k 0)) (d (n "pest_derive") (r "^2") (d #t) (k 0)))) (h "09lxci0jj3sjfb3gaq6nsafkrlnja4ys07xfq2qbjv2j7gbzwrb4") (y #t)))

(define-public crate-typeset-1.0.5 (c (n "typeset") (v "1.0.5") (d (list (d (n "bumpalo") (r "^3.12.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "pest") (r "^2.5.6") (d #t) (k 0)) (d (n "pest_derive") (r "^2.5.6") (d #t) (k 0)))) (h "0vcfm34n3v7ck03fab75pwdjxhb34jpmj443568xyaji759hfwn3")))

(define-public crate-typeset-2.0.0 (c (n "typeset") (v "2.0.0") (d (list (d (n "bumpalo") (r "^3.12.0") (d #t) (k 0)))) (h "07sl9mlmspm24psqhn79ldyg14qgc67di2nfixzhya2k0rmbhwwb")))

(define-public crate-typeset-2.0.1 (c (n "typeset") (v "2.0.1") (d (list (d (n "bumpalo") (r "^3.13.0") (d #t) (k 0)))) (h "1325159s4xhqkizzax2kj26n02chcxxja3hm0d9xvzrwn8ajkw89")))

(define-public crate-typeset-2.0.2 (c (n "typeset") (v "2.0.2") (d (list (d (n "bumpalo") (r "^3.13.0") (d #t) (k 0)))) (h "016pch1szkr6d82nk9jxvsrsjkfclpidg1n5bfckp064xcavavqn")))

(define-public crate-typeset-2.0.3 (c (n "typeset") (v "2.0.3") (d (list (d (n "bumpalo") (r "^3.14.0") (d #t) (k 0)))) (h "1wb29mi7m72dnrpzp8h4qa7aijbqvyfbwhh1jcaxf1b4idd7q08w")))

(define-public crate-typeset-2.0.4 (c (n "typeset") (v "2.0.4") (d (list (d (n "bumpalo") (r "^3.16.0") (d #t) (k 0)))) (h "1yaw24dikr00m5vy6dvzbvd55igvf7i654gs9lpy9yisn97al86b")))

