(define-module (crates-io ty pe type-path) #:use-module (crates-io))

(define-public crate-type-path-0.1.0 (c (n "type-path") (v "0.1.0") (d (list (d (n "trybuild") (r "^1.0.0") (d #t) (k 2)))) (h "0a5b26b7wqf3zwr1gq3amqjrjifj9h76c07iwfv3gmmjkgwbbv3f")))

(define-public crate-type-path-0.1.1 (c (n "type-path") (v "0.1.1") (d (list (d (n "trybuild") (r "^1.0.0") (d #t) (k 2)))) (h "1fr40pbxbhnrri6wwv8i9yx7wrzmm8fnm75k980mxfhj55ysj9py")))

