(define-module (crates-io ty pe type-record) #:use-module (crates-io))

(define-public crate-type-record-1.0.0 (c (n "type-record") (v "1.0.0") (h "0ybl7b7019jz8lm0b16d3vgwmz1lq4b060syihvg1snkg04y3kvc")))

(define-public crate-type-record-1.0.1 (c (n "type-record") (v "1.0.1") (h "0ylvaqsm4fnkni628f4mfyqnr212jgnvfax3kr9ii1vkgv4l8bxa")))

