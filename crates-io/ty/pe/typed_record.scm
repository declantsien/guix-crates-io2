(define-module (crates-io ty pe typed_record) #:use-module (crates-io))

(define-public crate-typed_record-0.1.0 (c (n "typed_record") (v "0.1.0") (d (list (d (n "anymap2") (r "^0.13.0") (o #t) (d #t) (k 0)) (d (n "http") (r "^0.2.9") (o #t) (d #t) (k 0)))) (h "160qj23dv1z1pcb74a5dnlkfbvx9ghd9d28glknr4pzhsq9q6xjv") (s 2) (e (quote (("ext-http" "dep:http") ("ext-anymap" "dep:anymap2"))))))

(define-public crate-typed_record-0.1.1 (c (n "typed_record") (v "0.1.1") (d (list (d (n "anymap2") (r "^0.13.0") (o #t) (d #t) (k 0)) (d (n "http") (r "^0.2.9") (o #t) (d #t) (k 0)))) (h "0w63r5q2kbyylwsg5f9r2fj0si0gjs68p5xjj7mdm36397f2l1sk") (s 2) (e (quote (("ext-http" "dep:http") ("ext-anymap" "dep:anymap2"))))))

