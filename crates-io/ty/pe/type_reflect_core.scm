(define-module (crates-io ty pe type_reflect_core) #:use-module (crates-io))

(define-public crate-type_reflect_core-0.1.0 (c (n "type_reflect_core") (v "0.1.0") (d (list (d (n "Inflector") (r "^0.11") (k 0)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "11d4pmni613j2sql6cwir4zwjyl6xfvqp6965vklglbqd6nj4aji")))

(define-public crate-type_reflect_core-0.1.1 (c (n "type_reflect_core") (v "0.1.1") (d (list (d (n "Inflector") (r "^0.11") (k 0)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1fg3gay1p9sfv7k0vnnmy04qn31knf6x002r3bv5a7d47c3vbd8s")))

(define-public crate-type_reflect_core-0.1.2 (c (n "type_reflect_core") (v "0.1.2") (d (list (d (n "Inflector") (r "^0.11") (k 0)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "04bzm7c4armwl4g0d8h76hycq46f935qj3vgmlw1797rnjvcqar9")))

(define-public crate-type_reflect_core-0.1.3 (c (n "type_reflect_core") (v "0.1.3") (d (list (d (n "Inflector") (r "^0.11") (k 0)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0ik56rcf64w6s36nm46mzhma5m8xn9gvfh7lcfa23f90s52jicaz")))

(define-public crate-type_reflect_core-0.3.0 (c (n "type_reflect_core") (v "0.3.0") (d (list (d (n "Inflector") (r "^0.11") (k 0)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1pmzlb0s5s965j4647x59fplbbaf7lfyn2bxj4xv4np84g7m7jzk")))

