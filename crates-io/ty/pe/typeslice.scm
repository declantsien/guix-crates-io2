(define-module (crates-io ty pe typeslice) #:use-module (crates-io))

(define-public crate-typeslice-0.2.0 (c (n "typeslice") (v "0.2.0") (d (list (d (n "itertools") (r "^0.12.1") (k 2)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 2)))) (h "1irdlpdzar2hvf9kimx341wjilbkinq0zh5jink9dm644lq5l1w9") (f (quote (("std") ("default" "std"))))))

(define-public crate-typeslice-0.2.1 (c (n "typeslice") (v "0.2.1") (d (list (d (n "itertools") (r "^0.12.1") (k 2)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 2)))) (h "0q3g6nkvw06k056843vjy2nsyw7jk8p2vfa7rk47km3g6swlqwn8") (f (quote (("std") ("default" "std"))))))

(define-public crate-typeslice-0.2.2 (c (n "typeslice") (v "0.2.2") (d (list (d (n "itertools") (r "^0.12.1") (k 2)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 2)) (d (n "typeslice-macros") (r "^0.1.1") (o #t) (d #t) (k 0)))) (h "0r1lh346z70xf24lwfbnkd7ijggqgcrc3f7ikvrshpfllx50clhv") (f (quote (("std") ("default" "std" "macros")))) (s 2) (e (quote (("macros" "dep:typeslice-macros"))))))

(define-public crate-typeslice-0.2.3 (c (n "typeslice") (v "0.2.3") (d (list (d (n "itertools") (r "^0.12.1") (k 2)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 2)) (d (n "typeslice-macros") (r "^0.1.1") (o #t) (d #t) (k 0)))) (h "0qdh1ahhh0f981bv7ha8r8lkkm0zfq9j70kl7fjy18qv3wlv3q70") (f (quote (("std") ("default" "std" "macros")))) (s 2) (e (quote (("macros" "dep:typeslice-macros"))))))

(define-public crate-typeslice-0.2.4 (c (n "typeslice") (v "0.2.4") (d (list (d (n "itertools") (r "^0.12.1") (k 2)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 2)) (d (n "typeslice-macros") (r "^0.1.2") (o #t) (d #t) (k 0)))) (h "1224vnd15a5i58kfl144vvwykcqyk38z2fgi6jrx2xjd4p2n613y") (f (quote (("std") ("default" "std" "macros")))) (s 2) (e (quote (("macros" "dep:typeslice-macros")))) (r "1.71.1")))

(define-public crate-typeslice-0.2.5 (c (n "typeslice") (v "0.2.5") (d (list (d (n "itertools") (r "^0.12.1") (k 2)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 2)) (d (n "typeslice-macros") (r "^0.1.2") (o #t) (d #t) (k 0)))) (h "0mxx8b34kj39j3birkkx4d2vhz43gl7rda9d0lfjxz87431vdwh6") (f (quote (("std") ("default" "std" "macros")))) (s 2) (e (quote (("macros" "dep:typeslice-macros")))) (r "1.71.1")))

