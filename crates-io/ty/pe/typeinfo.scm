(define-module (crates-io ty pe typeinfo) #:use-module (crates-io))

(define-public crate-typeinfo-0.1.1 (c (n "typeinfo") (v "0.1.1") (d (list (d (n "clippy") (r "^0.0.37") (o #t) (d #t) (k 0)) (d (n "compiletest_rs") (r "^0.0.11") (o #t) (d #t) (k 0)))) (h "10iw07949vlynz1lc6hxlm0g1kb6vymn4kv6w81jl6l8ybpsjb1k") (f (quote (("unstable" "compiletest_rs" "clippy"))))))

