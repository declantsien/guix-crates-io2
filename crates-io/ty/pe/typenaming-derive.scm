(define-module (crates-io ty pe typenaming-derive) #:use-module (crates-io))

(define-public crate-typenaming-derive-0.1.0 (c (n "typenaming-derive") (v "0.1.0") (d (list (d (n "darling") (r "^0.14.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1vibi68i5r6ksfcmkww9l70gmixz44a6dyj76cchypa7914n9rx7")))

(define-public crate-typenaming-derive-0.1.1 (c (n "typenaming-derive") (v "0.1.1") (d (list (d (n "darling") (r "^0.14.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "typenaming") (r "^0.1") (d #t) (k 2)))) (h "14w13cmrprh6fjfizacxivvysya9c1rw2lmxlzlmb4fvjfb1b8ax")))

(define-public crate-typenaming-derive-0.2.0 (c (n "typenaming-derive") (v "0.2.0") (d (list (d (n "darling") (r "^0.14.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0iy9vyfbikwqlvg9lh674mjx44bs2jyvf2rwrr9qmy0z3mbzjjlr")))

(define-public crate-typenaming-derive-0.3.0 (c (n "typenaming-derive") (v "0.3.0") (d (list (d (n "darling") (r "^0.14.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0lp43rswn0h2z6ycmrrynr99m9lh7kg3hwg453f1iqdmcklq4y09")))

