(define-module (crates-io ty pe type-info) #:use-module (crates-io))

(define-public crate-type-info-0.1.0 (c (n "type-info") (v "0.1.0") (h "1nmmm9h8gr8bbj0bhny01p1fxk6p5ll9y87qyawkw46vdf1w2y79")))

(define-public crate-type-info-0.2.0 (c (n "type-info") (v "0.2.0") (d (list (d (n "type-info-derive") (r "^0.1.0") (d #t) (k 2)))) (h "1b8lz13rp2308bwb8x4nbvb802mahbzvcsd9llm15ypm5aka97f7")))

(define-public crate-type-info-0.2.1 (c (n "type-info") (v "0.2.1") (d (list (d (n "type-info-derive") (r "^0.2.0") (d #t) (k 2)))) (h "02gwi1qybw10b46jw2ir9a9fygwdqy38g7p3pbvrgi8x9c9ls6ar")))

