(define-module (crates-io ty pe type-protocol) #:use-module (crates-io))

(define-public crate-type-protocol-0.1.0 (c (n "type-protocol") (v "0.1.0") (d (list (d (n "rkyv") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "strum") (r "^0.25") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "unicode-ident") (r "^1") (d #t) (k 0)))) (h "1cnssc0ai6hyjicwh3xs9mrbnkii81ngc6b5bpgan9vwjifd6d8y") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde") ("rkyv" "dep:rkyv"))))))

