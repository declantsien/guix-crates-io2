(define-module (crates-io ty pe type_erased_vec) #:use-module (crates-io))

(define-public crate-type_erased_vec-0.1.0 (c (n "type_erased_vec") (v "0.1.0") (h "132w8my3f70ppahpsn66d29gkg44zw9lb5hz047ccwks5bd3xyrd") (y #t)))

(define-public crate-type_erased_vec-0.1.1 (c (n "type_erased_vec") (v "0.1.1") (h "06l8ybi5w52hipryim75y9vilfmacrp327j8lbmjqcbdangcqvzh")))

(define-public crate-type_erased_vec-0.1.2 (c (n "type_erased_vec") (v "0.1.2") (h "1q247iwf4xivjna58sx9xqp239l3zmxj49hkk1as100qhiqc5k1s")))

(define-public crate-type_erased_vec-0.1.3 (c (n "type_erased_vec") (v "0.1.3") (d (list (d (n "bytemuck") (r "^1") (d #t) (k 2)))) (h "0q6q3m5x063bpkzs48lqkx7mc4jqak9ix4wzwh6cnjmwd7fz8p8m")))

(define-public crate-type_erased_vec-0.1.4 (c (n "type_erased_vec") (v "0.1.4") (d (list (d (n "bytemuck") (r "^1") (d #t) (k 2)))) (h "053428g0hqw7wg58hij7d7nn8mrw5vg2blzjz6aaq37bpdk3xmnh")))

(define-public crate-type_erased_vec-0.2.0 (c (n "type_erased_vec") (v "0.2.0") (d (list (d (n "bytemuck") (r "^1") (d #t) (k 2)))) (h "1dip3nf42xbgb2p08fj1f2vbmr0a0hzwyc9868vh45zrqzp43vc2")))

