(define-module (crates-io ty pe type_hash_core) #:use-module (crates-io))

(define-public crate-type_hash_core-0.1.0 (c (n "type_hash_core") (v "0.1.0") (d (list (d (n "fnv") (r "^1.0.7") (d #t) (k 0)))) (h "1xz80hyl166hwqdhraz4jh4kax7iibjsski5qrf85jqr4d5wp5pi")))

(define-public crate-type_hash_core-0.2.0 (c (n "type_hash_core") (v "0.2.0") (d (list (d (n "fnv") (r "^1.0.7") (d #t) (k 0)))) (h "0ylrwi8rxrv3wdcmy9k0p6pfnvmazc9jhbdy5n4r0xyr5hzfkcc7")))

