(define-module (crates-io ty pe typed_floats) #:use-module (crates-io))

(define-public crate-typed_floats-0.1.0 (c (n "typed_floats") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "typed_floats_macros") (r "^0.1.0") (d #t) (k 0)))) (h "12w7j6ac291hhrhn3g68d1cchq81pz24pii5px35nwlwyb74isrz") (y #t) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-typed_floats-0.1.1 (c (n "typed_floats") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "typed_floats_macros") (r "^0.1.1") (d #t) (k 0)))) (h "16hif914rqih8iw1zk3dimkkj5rfh70jkyj1r1bgv2ialkhyrqi3") (y #t) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-typed_floats-0.1.2 (c (n "typed_floats") (v "0.1.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "typed_floats_macros") (r "^0.1.2") (d #t) (k 0)))) (h "0n0bn2bk8y4k447d6c8dypg3lhg0dz64m3cyrp9lvk5zhv2pv2df") (y #t) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-typed_floats-0.1.3 (c (n "typed_floats") (v "0.1.3") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "typed_floats_macros") (r "^0.1.3") (d #t) (k 0)))) (h "1qla2g9ika10867qgfq5xg1zhm8y0v41sfzhxsbdhcysv1qbfipm") (y #t) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-typed_floats-0.1.4 (c (n "typed_floats") (v "0.1.4") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "typed_floats_macros") (r "^0.1.4") (d #t) (k 0)))) (h "17c2bpi1cn4w385spd9cvlblbfbyh3c9r15zf42r82s8fpl7041f") (y #t)))

(define-public crate-typed_floats-0.1.6 (c (n "typed_floats") (v "0.1.6") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "typed_floats_macros") (r "^0.1.6") (d #t) (k 0)))) (h "1gazy510d5bmvv3ng310hmn0mccbpq3nhmbpzxi3vjgh7dfiag8s") (y #t)))

(define-public crate-typed_floats-0.1.7 (c (n "typed_floats") (v "0.1.7") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "typed_floats_macros") (r "^0.1.7") (d #t) (k 0)))) (h "034p0nmz222c4f14122frcpvy4vm1y7nv2fr5gi37k6v9brvbfab") (y #t)))

(define-public crate-typed_floats-0.1.8 (c (n "typed_floats") (v "0.1.8") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "typed_floats_macros") (r "^0.1.8") (d #t) (k 0)))) (h "0304ddgzkbixwk4fyxhffsffn6zd2y40pp0mq59qcn5185y8m22b") (y #t)))

(define-public crate-typed_floats-0.1.9 (c (n "typed_floats") (v "0.1.9") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "typed_floats_macros") (r "=0.1.9") (d #t) (k 0)))) (h "08hlxnmkqagkbvxnb0xaiba0n5nfd90rg07435fwnnnkh9bmc17l") (y #t)))

(define-public crate-typed_floats-0.1.10 (c (n "typed_floats") (v "0.1.10") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "typed_floats_macros") (r "=0.1.10") (d #t) (k 0)))) (h "0z798z8p0lz7hb8fgljg6h67h1vkbzlxrkn08znhh2414ng1vsp0") (y #t)))

(define-public crate-typed_floats-0.1.11 (c (n "typed_floats") (v "0.1.11") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "typed_floats_macros") (r "=0.1.11") (d #t) (k 0)))) (h "0zycn1vh6l98icirwb0mgfkhizxcq0gdqmb2wnqxvpslxml74sq8") (y #t)))

(define-public crate-typed_floats-0.1.12 (c (n "typed_floats") (v "0.1.12") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "typed_floats_macros") (r "=0.1.12") (d #t) (k 0)))) (h "06wn9plkpqmba6gl5pli2dg7slnqmd5pl26zzqrb9py0k7kcrycl") (y #t)))

(define-public crate-typed_floats-0.1.22 (c (n "typed_floats") (v "0.1.22") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "typed_floats_macros") (r "=0.1.22") (d #t) (k 0)))) (h "1vjwiq4rn9vldgwfal7hr3rvnm1l7xnk8k8b6yxyrja2852w1p46") (y #t)))

(define-public crate-typed_floats-0.1.23 (c (n "typed_floats") (v "0.1.23") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "typed_floats_macros") (r "=0.1.23") (d #t) (k 0)))) (h "0hgprswv78c683yq33ik7rv7n4mrqnkr1hi23yx95p2yxkjyvadh") (y #t)))

(define-public crate-typed_floats-0.1.24 (c (n "typed_floats") (v "0.1.24") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "typed_floats_macros") (r "=0.1.24") (d #t) (k 0)))) (h "030yylrgxl46q2cr4g0sp46mhsysix7xyplrqlf23j79lq02qq3s") (y #t)))

(define-public crate-typed_floats-0.1.25 (c (n "typed_floats") (v "0.1.25") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "typed_floats_macros") (r "=0.1.25") (d #t) (k 0)))) (h "0gbbh7g7rkvw3l9dxjxvxrz4rbfw1sbxwn3nqzjka0z42dd18jpb") (y #t)))

(define-public crate-typed_floats-0.1.26 (c (n "typed_floats") (v "0.1.26") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "typed_floats_macros") (r "=0.1.26") (d #t) (k 0)))) (h "0apf9vb6w6ygsjp8s2z5dbfbhr4x8b2nknpxqg1qzpjhssl1skpq") (y #t)))

(define-public crate-typed_floats-0.1.27 (c (n "typed_floats") (v "0.1.27") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "typed_floats_macros") (r "=0.1.27") (d #t) (k 0)))) (h "1h3hiih061z194id0yfx41wqfk48lgqnrzalzm9kbggnl5j76gp0") (y #t)))

(define-public crate-typed_floats-0.1.28 (c (n "typed_floats") (v "0.1.28") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "typed_floats_macros") (r "=0.1.28") (d #t) (k 0)))) (h "1v4wnky0iiqliv0mnpgs735mhqgxxbrqa9r38bia4a4zhzdi0cc5") (y #t)))

(define-public crate-typed_floats-0.1.29 (c (n "typed_floats") (v "0.1.29") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "typed_floats_macros") (r "=0.1.29") (d #t) (k 0)))) (h "0vidkmviindiqrxvkqlckflmy7r6lb87dzsvg9rdlp1dxw9xpl76") (y #t)))

(define-public crate-typed_floats-0.1.30 (c (n "typed_floats") (v "0.1.30") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "typed_floats_macros") (r "=0.1.30") (d #t) (k 0)))) (h "1rd54343blgqh5421pshldz5niqw6zjgaxy87wpkqij43hfd40jj") (y #t)))

(define-public crate-typed_floats-0.1.31 (c (n "typed_floats") (v "0.1.31") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "typed_floats_macros") (r "=0.1.31") (d #t) (k 0)))) (h "1fj7m7kk15aa8g68d4v38v9298iknn9wic68sj8mbd5irx4nxbhz") (y #t)))

(define-public crate-typed_floats-0.1.32 (c (n "typed_floats") (v "0.1.32") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "typed_floats_macros") (r "=0.1.32") (d #t) (k 0)))) (h "1vbqjv4mmfbgx56kjd06pvnf4i109aqqp5cnmzr0077mqscjpk8l") (y #t)))

(define-public crate-typed_floats-0.1.33 (c (n "typed_floats") (v "0.1.33") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "typed_floats_macros") (r "=0.1.33") (d #t) (k 0)))) (h "0f7n59avmx5z4i8nfdbjakp6g3yw5vaswxh0miv2bdlqsc9plvh7") (y #t)))

(define-public crate-typed_floats-0.1.34 (c (n "typed_floats") (v "0.1.34") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "typed_floats_macros") (r "=0.1.34") (d #t) (k 0)))) (h "1wbxzf8g278l4ypsvm1fsgsskxwcg6n63ikpysl29fd0rcpc5d3y") (y #t)))

(define-public crate-typed_floats-0.1.36 (c (n "typed_floats") (v "0.1.36") (d (list (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "typed_floats_macros") (r "=0.1.36") (d #t) (k 0)))) (h "0k44lxxfb8a6076jjd947742a0d00hm1kqy8i07zk1pj94rwb29f") (y #t)))

(define-public crate-typed_floats-0.1.37 (c (n "typed_floats") (v "0.1.37") (d (list (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "typed_floats_macros") (r "=0.1.37") (d #t) (k 0)))) (h "0qjyq8q93mf2g3382x9shrs8jz0fg3q7mp1pgxg99pxmc1r21xh2") (y #t)))

(define-public crate-typed_floats-0.2.2 (c (n "typed_floats") (v "0.2.2") (d (list (d (n "checked-float") (r "^0.1.4") (d #t) (k 2)) (d (n "ordered-float") (r "^4.1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "typed_floats_macros") (r "=0.2.2") (d #t) (k 0)))) (h "0xqgqswa50cf8hggfghxg9j5wb3a2cp0g1i6xjh4wrhb66wzy93n") (f (quote (("default")))) (y #t) (r "1.56")))

(define-public crate-typed_floats-0.2.3 (c (n "typed_floats") (v "0.2.3") (d (list (d (n "checked-float") (r "^0.1.4") (d #t) (k 2)) (d (n "ordered-float") (r "^4.1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "typed_floats_macros") (r "=0.2.3") (d #t) (k 0)))) (h "13z7l5449xwil040afpz472rxm5w1gz8nvap3w83hcjfp7qdcrix") (f (quote (("default")))) (y #t) (r "1.56")))

(define-public crate-typed_floats-0.2.4 (c (n "typed_floats") (v "0.2.4") (d (list (d (n "checked-float") (r "^0.1.4") (d #t) (k 2)) (d (n "ordered-float") (r "^4.1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "typed_floats_macros") (r "=0.2.4") (d #t) (k 0)))) (h "0m4jq75ak2mlbbdgifircwz1h6hfyigddjrlw5gzsfbsx69ymk3l") (f (quote (("default")))) (y #t) (r "1.56")))

(define-public crate-typed_floats-0.3.0 (c (n "typed_floats") (v "0.3.0") (d (list (d (n "checked-float") (r "^0.1.4") (d #t) (k 2)) (d (n "ordered-float") (r "^4.1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "typed_floats_macros") (r "=0.3.0") (d #t) (k 0)))) (h "165zzkbwvwhgk116lwj2i34rj4fswivcp1gkqnyc81k5ainapsrd") (f (quote (("default")))) (y #t) (r "1.56")))

(define-public crate-typed_floats-0.4.0 (c (n "typed_floats") (v "0.4.0") (d (list (d (n "checked-float") (r "^0.1.4") (d #t) (k 2)) (d (n "ordered-float") (r "^4.1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "typed_floats_macros") (r "=0.4.0") (d #t) (k 0)))) (h "0333hh2am100rs19bxl4ry7qh0sbi8gka56gryz83p2y55l8c7ks") (f (quote (("default")))) (y #t) (s 2) (e (quote (("serde" "dep:serde")))) (r "1.56")))

(define-public crate-typed_floats-0.5.0 (c (n "typed_floats") (v "0.5.0") (d (list (d (n "checked-float") (r "^0.1.4") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (f (quote ("libm"))) (o #t) (k 0)) (d (n "ordered-float") (r "^4.1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "typed_floats_macros") (r "=0.5.0") (d #t) (k 0)))) (h "0p0gi5j1nmw0mmwwffxhb5a2jzdfmvg67q72sy4lbhzfi21zvxwr") (f (quote (("std" "typed_floats_macros/std") ("default" "std")))) (y #t) (s 2) (e (quote (("serde" "dep:serde") ("libm" "dep:num-traits" "typed_floats_macros/libm")))) (r "1.70")))

(define-public crate-typed_floats-0.6.0 (c (n "typed_floats") (v "0.6.0") (d (list (d (n "checked-float") (r "^0.1.4") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (f (quote ("libm"))) (o #t) (k 0)) (d (n "ordered-float") (r "^4.1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "typed_floats_macros") (r "=0.6.0") (d #t) (k 0)))) (h "02hi7d213r0lb8g6bw692l2l1wh71q84nphkams2g95ksisr697f") (f (quote (("std" "typed_floats_macros/std") ("default" "std")))) (y #t) (s 2) (e (quote (("serde" "dep:serde") ("libm" "dep:num-traits" "typed_floats_macros/libm")))) (r "1.70")))

(define-public crate-typed_floats-0.7.0 (c (n "typed_floats") (v "0.7.0") (d (list (d (n "num-traits") (r "^0.2") (f (quote ("libm"))) (o #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "typed_floats_macros") (r "=0.7.0") (d #t) (k 0)))) (h "1n1rkmx3cc2wghnh1p5k6mblxs7wjjmyia0snxc69383fbhcnmiz") (f (quote (("std" "typed_floats_macros/std") ("default" "std" "compiler_hints") ("compiler_hints")))) (y #t) (s 2) (e (quote (("serde" "dep:serde") ("libm" "dep:num-traits" "typed_floats_macros/libm")))) (r "1.70")))

(define-public crate-typed_floats-0.7.1 (c (n "typed_floats") (v "0.7.1") (d (list (d (n "num-traits") (r "^0.2") (f (quote ("libm"))) (o #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "typed_floats_macros") (r "=0.7.1") (d #t) (k 0)))) (h "1ip6idm9dhr3qvir3zrj5mcrppkhv8ijwj246578g5f43hqyr8k0") (f (quote (("std" "typed_floats_macros/std") ("default" "std" "compiler_hints") ("compiler_hints")))) (y #t) (s 2) (e (quote (("serde" "dep:serde") ("libm" "dep:num-traits" "typed_floats_macros/libm")))) (r "1.70")))

(define-public crate-typed_floats-0.7.2 (c (n "typed_floats") (v "0.7.2") (d (list (d (n "num-traits") (r "^0.2") (f (quote ("libm"))) (o #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "typed_floats_macros") (r "=0.7.2") (d #t) (k 0)))) (h "0rrx8dxkxiqjz66v3ay5yzws2x89mgq8vgfvvwvdiyvrv2qiannr") (f (quote (("std" "typed_floats_macros/std") ("default" "std" "compiler_hints") ("compiler_hints")))) (y #t) (s 2) (e (quote (("serde" "dep:serde") ("libm" "dep:num-traits" "typed_floats_macros/libm")))) (r "1.70")))

(define-public crate-typed_floats-0.7.3 (c (n "typed_floats") (v "0.7.3") (d (list (d (n "num-traits") (r "^0.2") (f (quote ("libm"))) (o #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "typed_floats_macros") (r "=0.7.3") (d #t) (k 0)))) (h "1dcvv1lnmyrhj468i737hpwlwmsb7lbmckq4zz7jqj716m8p0b73") (f (quote (("std" "typed_floats_macros/std") ("default" "std" "compiler_hints") ("compiler_hints")))) (y #t) (s 2) (e (quote (("serde" "dep:serde") ("libm" "dep:num-traits" "typed_floats_macros/libm")))) (r "1.70")))

(define-public crate-typed_floats-0.7.4 (c (n "typed_floats") (v "0.7.4") (d (list (d (n "num-traits") (r "^0.2") (f (quote ("libm"))) (o #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "typed_floats_macros") (r "=0.7.4") (d #t) (k 0)))) (h "04am9v9ilqgn2hgn06w6iz3q1gyrf6v9wb9210m0cdqlj5i081k4") (f (quote (("std" "typed_floats_macros/std") ("default" "std" "compiler_hints") ("compiler_hints")))) (s 2) (e (quote (("serde" "dep:serde") ("libm" "dep:num-traits" "typed_floats_macros/libm")))) (r "1.70")))

(define-public crate-typed_floats-1.0.0 (c (n "typed_floats") (v "1.0.0") (d (list (d (n "num-traits") (r "^0.2") (f (quote ("libm"))) (o #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "typed_floats_macros") (r "=1.0.0") (d #t) (k 0)))) (h "1lgxjpl87snxvraij4lnkzqxcfa2b6fx2j2i29avilyd0r3wn47g") (f (quote (("std" "typed_floats_macros/std") ("ensure_no_undefined_behavior") ("default" "std" "compiler_hints") ("compiler_hints")))) (s 2) (e (quote (("serde" "dep:serde") ("libm" "dep:num-traits" "typed_floats_macros/libm")))) (r "1.70")))

(define-public crate-typed_floats-1.0.1 (c (n "typed_floats") (v "1.0.1") (d (list (d (n "num-traits") (r "^0.2") (f (quote ("libm"))) (o #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "typed_floats_macros") (r "=1.0.1") (d #t) (k 0)))) (h "0gxir14jbhlyxxzrvjk9an2xzl4a0jzqs110jns62d3f0c1cqhcd") (f (quote (("std" "typed_floats_macros/std") ("ensure_no_undefined_behavior") ("default" "std" "compiler_hints") ("compiler_hints")))) (s 2) (e (quote (("serde" "dep:serde") ("libm" "dep:num-traits" "typed_floats_macros/libm")))) (r "1.70")))

