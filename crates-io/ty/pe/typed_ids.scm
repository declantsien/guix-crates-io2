(define-module (crates-io ty pe typed_ids) #:use-module (crates-io))

(define-public crate-typed_ids-0.1.0 (c (n "typed_ids") (v "0.1.0") (d (list (d (n "pretty_assertions") (r "^1.0.0") (d #t) (k 2)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 2)))) (h "03rspzsa7g28rchh95sm622pyhnc8m9iwc1w13761bhg9xcww8vy")))

(define-public crate-typed_ids-0.2.0 (c (n "typed_ids") (v "0.2.0") (d (list (d (n "flume") (r "^0.10.9") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.0.0") (d #t) (k 2)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 2)) (d (n "tokio") (r "^1.12.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "0vxqh1abp8j2gp533dqswpsnqhiyczkzfniryld2b4g7k2zh0qsr")))

