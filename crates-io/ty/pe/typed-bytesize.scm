(define-module (crates-io ty pe typed-bytesize) #:use-module (crates-io))

(define-public crate-typed-bytesize-0.1.0 (c (n "typed-bytesize") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1nw3r00yczl7klb633134zcsi2ng34b2bs3dhzrrjq527fwg8kqs") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-typed-bytesize-0.1.1 (c (n "typed-bytesize") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1r6nhbagp6zibbs9d5pk1y52iz3b8f16if1258ra7kv7m9qkc9vc") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-typed-bytesize-0.1.2 (c (n "typed-bytesize") (v "0.1.2") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0fdrb34npmcj3s2b8r0wfq28q6x7jpvq5lpnl81cxs6aq62xczqq") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

