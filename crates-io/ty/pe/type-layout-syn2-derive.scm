(define-module (crates-io ty pe type-layout-syn2-derive) #:use-module (crates-io))

(define-public crate-type-layout-syn2-derive-0.2.0 (c (n "type-layout-syn2-derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.21") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0s0053rbfrihj4npcix4mpvxxlw09427pnw18j35vnhayhwn7wlm")))

