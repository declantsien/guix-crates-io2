(define-module (crates-io ty pe typename) #:use-module (crates-io))

(define-public crate-typename-0.1.0 (c (n "typename") (v "0.1.0") (d (list (d (n "typename_derive") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "0bs8fhk9cy2m8xmr96kfg8rq1466m9a0js158i0337ps0i74kih7") (f (quote (("derive" "typename_derive") ("default" "derive"))))))

(define-public crate-typename-0.1.1 (c (n "typename") (v "0.1.1") (d (list (d (n "typename_derive") (r "^0.1.3") (o #t) (d #t) (k 0)))) (h "1gcklp311s5dj7vw04vld88fh52dqayafbm6b87b68x11qlm9yww") (f (quote (("derive" "typename_derive") ("default" "derive"))))))

(define-public crate-typename-0.1.2 (c (n "typename") (v "0.1.2") (d (list (d (n "typename_derive") (r "^0.1.4") (o #t) (d #t) (k 0)))) (h "06alqlvk3gs79jhxp532x0ax5040fw0n49k2y2aznp6gz0ddsmf2") (f (quote (("derive" "typename_derive") ("default" "derive"))))))

