(define-module (crates-io ty pe typescript-wasm-bindgen) #:use-module (crates-io))

(define-public crate-typescript-wasm-bindgen-0.1.0 (c (n "typescript-wasm-bindgen") (v "0.1.0") (d (list (d (n "typescript-wasm-bindgen-codegen") (r "^0.1.0") (d #t) (k 0)) (d (n "typescript-wasm-bindgen-macros") (r "^0.1.0") (d #t) (k 0)))) (h "1ig09hc3ba0rycckv1i8awbnxvaws8ibd9d4f6wi4v4xa6a8fssv")))

(define-public crate-typescript-wasm-bindgen-0.1.1 (c (n "typescript-wasm-bindgen") (v "0.1.1") (d (list (d (n "typescript-wasm-bindgen-codegen") (r "^0.1.1") (d #t) (k 0)) (d (n "typescript-wasm-bindgen-macros") (r "^0.1.1") (d #t) (k 0)))) (h "1yjl6bqm839krry4smv2dsbcplwlxp46f69i58zg48dplcphdhq4")))

