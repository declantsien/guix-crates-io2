(define-module (crates-io ty pe type-sitter-proc) #:use-module (crates-io))

(define-public crate-type-sitter-proc-0.1.0 (c (n "type-sitter-proc") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (d #t) (k 0)) (d (n "tree-sitter") (r "^0.20.10") (d #t) (k 2)) (d (n "type-sitter-gen") (r "^0.1.0") (d #t) (k 0)) (d (n "type-sitter-lib") (r "^0.1.0") (d #t) (k 2)))) (h "03d3g0inphbj792kr6b5y7g2767i3zp7ga29rxpharykdbwf8pk5")))

(define-public crate-type-sitter-proc-0.1.1 (c (n "type-sitter-proc") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (d #t) (k 0)) (d (n "tree-sitter") (r "^0.20.10") (d #t) (k 2)) (d (n "type-sitter-gen") (r "^0.1.1") (d #t) (k 0)) (d (n "type-sitter-lib") (r "^0.1.1") (d #t) (k 2)))) (h "1aiahm1cim0dj5gfm1q56lv2m1v79kfzhakfw5ab2ci20chhayg8")))

(define-public crate-type-sitter-proc-0.1.2 (c (n "type-sitter-proc") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (d #t) (k 0)) (d (n "tree-sitter") (r "^0.20.10") (d #t) (k 2)) (d (n "type-sitter-gen") (r "^0.1.2") (d #t) (k 0)) (d (n "type-sitter-lib") (r "^0.1.1") (d #t) (k 2)))) (h "1nnyi63v6a48rjkd8n37rcihqgwr9yga1fqp6a0016wmdh79kq7k")))

(define-public crate-type-sitter-proc-0.2.0 (c (n "type-sitter-proc") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (d #t) (k 0)) (d (n "type-sitter-gen") (r "^0.2.0") (d #t) (k 0)))) (h "0inpaks8rr8nkykfb9wwa6gxk1py644pja40chwf7bxkwgmf4a2d")))

(define-public crate-type-sitter-proc-0.2.1 (c (n "type-sitter-proc") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (d #t) (k 0)) (d (n "type-sitter-gen") (r "^0.2.1") (d #t) (k 0)))) (h "1s1hajhg0qfgmdd6g7jvp8aqvljhiy35n6ll3ngc47dmb45m05gs")))

(define-public crate-type-sitter-proc-0.3.0 (c (n "type-sitter-proc") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (d #t) (k 0)) (d (n "type-sitter-gen") (r "^0.3.0") (d #t) (k 0)))) (h "1d17an64msy31znh1157nm9d6r00n9y87vpmkcjpv5v550y5acp5")))

(define-public crate-type-sitter-proc-0.3.1 (c (n "type-sitter-proc") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (d #t) (k 0)) (d (n "type-sitter-gen") (r "^0.3.1") (d #t) (k 0)))) (h "06mqsp4l8fh6fzz07klasx982d0wsnpxwx2a697gv018rwd3v4nf") (y #t)))

(define-public crate-type-sitter-proc-0.4.0 (c (n "type-sitter-proc") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (d #t) (k 0)) (d (n "type-sitter-gen") (r "^0.4.0") (d #t) (k 0)))) (h "1z2y86v4awbl0krh3nhf9kvwrdivs6hjbhq0882bhzl07d79v07m")))

