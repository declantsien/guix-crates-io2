(define-module (crates-io ty pe typetrait) #:use-module (crates-io))

(define-public crate-typetrait-0.1.0 (c (n "typetrait") (v "0.1.0") (d (list (d (n "convert_case") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "021pfmjqymjn4a8j4izgzq2d48kw8m7ib8hn75lssq7dbbsq7zvb")))

(define-public crate-typetrait-0.1.1 (c (n "typetrait") (v "0.1.1") (d (list (d (n "convert_case") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1r121dv18qy2xlnc4cx0ldyhz9jxay5v608lwnd3g2xbavm4v6ma")))

