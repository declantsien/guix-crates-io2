(define-module (crates-io ty pe types-primitive) #:use-module (crates-io))

(define-public crate-types-primitive-0.2.1 (c (n "types-primitive") (v "0.2.1") (d (list (d (n "codec-impl") (r "^0.2") (o #t) (k 0)) (d (n "fixed-hash") (r "^0.3") (k 0)) (d (n "impl-rlp") (r "^0.1") (o #t) (k 0)) (d (n "impl-serde") (r "^0.1") (o #t) (k 0)) (d (n "uint") (r "^0.6") (k 0)))) (h "1i9lix3kzn5q107vwsprmcm4gfpcyw9c65xpahkx5lywlascxk7n") (f (quote (("std" "uint/std" "fixed-hash/std" "codec-impl/std") ("serde" "std" "impl-serde") ("rustc-hex" "fixed-hash/rustc-hex") ("rlp" "std" "impl-rlp") ("libc" "fixed-hash/libc") ("heapsize" "uint/heapsize" "fixed-hash/heapsize") ("default" "std") ("codec" "codec-impl") ("byteorder" "fixed-hash/byteorder"))))))

