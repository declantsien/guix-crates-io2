(define-module (crates-io ty pe typetw) #:use-module (crates-io))

(define-public crate-typetw-0.1.0 (c (n "typetw") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.27") (d #t) (k 0)) (d (n "ratatui") (r "^0.23") (d #t) (k 0)))) (h "0p9vqq96p0asr2amf5v4hysa4wrc267chms4z5js5v531rwck429")))

(define-public crate-typetw-0.1.1 (c (n "typetw") (v "0.1.1") (d (list (d (n "crossterm") (r "^0.27") (d #t) (k 0)) (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)) (d (n "ratatui") (r "^0.23") (d #t) (k 0)))) (h "0i374yh4yppxczx33w8ygnqyxgp3a7gn5q6sgl65n6v6gd035r8v")))

(define-public crate-typetw-0.1.2 (c (n "typetw") (v "0.1.2") (d (list (d (n "crossterm") (r "^0.27") (d #t) (k 0)) (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)) (d (n "ratatui") (r "^0.23") (d #t) (k 0)))) (h "0mx37sbmk64fsjl4lcf9jd14nv68vblkk9dmps267fjwcprqralh")))

(define-public crate-typetw-0.1.3 (c (n "typetw") (v "0.1.3") (d (list (d (n "crossterm") (r "^0.27") (d #t) (k 0)) (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)) (d (n "ratatui") (r "^0.23") (d #t) (k 0)))) (h "1l89f6pyc06s1b26ydlybmm4b0vcy652qlh5rgvg1545yqzka4ii")))

(define-public crate-typetw-0.1.4 (c (n "typetw") (v "0.1.4") (d (list (d (n "crossterm") (r "^0.27") (d #t) (k 0)) (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "ratatui") (r "^0.23") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.106") (d #t) (k 0)))) (h "0jbs9fq657i8s4g38nhbwxbk5jbv2rnj0vps47xw89a2a7iwh0rg")))

(define-public crate-typetw-0.1.5 (c (n "typetw") (v "0.1.5") (d (list (d (n "crossterm") (r "^0.27") (d #t) (k 0)) (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "ratatui") (r "^0.23") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.106") (d #t) (k 0)))) (h "0ghfhhys42zphbgdb3cxgk2bdb1lg6jgymqx7n3jwbsrbpi57gpf")))

(define-public crate-typetw-0.2.0 (c (n "typetw") (v "0.2.0") (d (list (d (n "crossterm") (r "^0.27") (d #t) (k 0)) (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "ratatui") (r "^0.23") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.106") (d #t) (k 0)))) (h "16r8950ds0bil89bim70fk03sl1p1avc0df90c4bmnkqb40462z5")))

(define-public crate-typetw-0.2.1 (c (n "typetw") (v "0.2.1") (d (list (d (n "crossterm") (r "^0.27") (d #t) (k 0)) (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "ratatui") (r "^0.23") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.106") (d #t) (k 0)))) (h "1gappixq3ym7z5bicq1zifmxllf75kxwmq2dpr2ynwjx7dkpfcs8")))

