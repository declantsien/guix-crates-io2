(define-module (crates-io ty pe typemaster) #:use-module (crates-io))

(define-public crate-typemaster-0.2.1 (c (n "typemaster") (v "0.2.1") (d (list (d (n "console_engine") (r "^1.4.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "0h9qwn8n4h277va9xrk63lm6ajd4fp1zf6brxws10mjmh5qnp0v8")))

(define-public crate-typemaster-0.2.2 (c (n "typemaster") (v "0.2.2") (d (list (d (n "console_engine") (r "^1.4.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "0cxaf2hwkkyxbik7ny2y9rs8897jzi0dzrzwg0g61nar6nvfyvvd")))

(define-public crate-typemaster-0.2.3 (c (n "typemaster") (v "0.2.3") (d (list (d (n "console_engine") (r "^1.4.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "1dxfy547h65bdfva5z7kybycdnbalcakv29d23mrnq20brj3rfm9")))

