(define-module (crates-io ty pe type-sitter-cli) #:use-module (crates-io))

(define-public crate-type-sitter-cli-0.1.0 (c (n "type-sitter-cli") (v "0.1.0") (d (list (d (n "clap") (r "^4.2.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "type-sitter-gen") (r "^0.1.0") (d #t) (k 0)))) (h "195ccxj6pn2964q77y9rfps63h4qrq7miig1h4b4km3kh6llc5bc")))

(define-public crate-type-sitter-cli-0.1.1 (c (n "type-sitter-cli") (v "0.1.1") (d (list (d (n "clap") (r "^4.2.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "type-sitter-gen") (r "^0.1.1") (d #t) (k 0)))) (h "0f6pj40r046wxpmdcg5vxw20krgfig01zdiw1lzyd59wh3lgckv0")))

(define-public crate-type-sitter-cli-0.1.2 (c (n "type-sitter-cli") (v "0.1.2") (d (list (d (n "clap") (r "^4.2.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rust-format") (r "^0.3.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "type-sitter-gen") (r "^0.1.2") (d #t) (k 0)))) (h "1dxhq6bhsp3nwnmzdn3c1ig9w55k781w2wviclqhm5a0jrf2lpkd")))

(define-public crate-type-sitter-cli-0.2.0 (c (n "type-sitter-cli") (v "0.2.0") (d (list (d (n "clap") (r "^4.2.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rust-format") (r "^0.3.4") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "type-sitter-gen") (r "^0.2.0") (d #t) (k 0)))) (h "0xz6s1b290f1217mcrpb38gmvr56vxgmxqyqmhklhqr00f49qjn6")))

(define-public crate-type-sitter-cli-0.2.1 (c (n "type-sitter-cli") (v "0.2.1") (d (list (d (n "clap") (r "^4.2.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rust-format") (r "^0.3.4") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "type-sitter-gen") (r "^0.2.1") (d #t) (k 0)))) (h "0gbzdlmwjhz3548n4if1h4p9kklzhifnynffdn8mkc3lpgkmkxh5")))

(define-public crate-type-sitter-cli-0.3.0 (c (n "type-sitter-cli") (v "0.3.0") (d (list (d (n "clap") (r "^4.2.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rust-format") (r "^0.3.4") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "type-sitter-gen") (r "^0.3.0") (d #t) (k 0)))) (h "1daq7npr7jsl59mr982ja5y5ddw7zc8z3yp59p9g7df9diflx99h")))

(define-public crate-type-sitter-cli-0.3.1 (c (n "type-sitter-cli") (v "0.3.1") (d (list (d (n "clap") (r "^4.2.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rust-format") (r "^0.3.4") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "type-sitter-gen") (r "^0.3.1") (d #t) (k 0)))) (h "0mbxk3z8znaqc3zc3pyg4s7k45llms4gd5n9xf5khipxnjy3gq9x") (y #t)))

(define-public crate-type-sitter-cli-0.4.0 (c (n "type-sitter-cli") (v "0.4.0") (d (list (d (n "clap") (r "^4.2.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rust-format") (r "^0.3.4") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "type-sitter-gen") (r "^0.4.0") (d #t) (k 0)))) (h "092qh4xm7rv4di4v7d0a2cpjhvvdnd54pf8srpqza7g7n97cx9gy")))

