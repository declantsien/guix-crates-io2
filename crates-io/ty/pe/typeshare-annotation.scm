(define-module (crates-io ty pe typeshare-annotation) #:use-module (crates-io))

(define-public crate-typeshare-annotation-1.0.0 (c (n "typeshare-annotation") (v "1.0.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "proc-macro"))) (d #t) (k 0)))) (h "1v39s77f42c330wqm1ny6sq7j25yxzjr7dp3d0i7cz0dswsh56rq") (r "1.57")))

(define-public crate-typeshare-annotation-1.0.1 (c (n "typeshare-annotation") (v "1.0.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "proc-macro"))) (d #t) (k 0)))) (h "17c2vkfw2x1kskb099sl7jx0ip2fnghnrn1sn9sa53zc29nqc2v7") (r "1.57")))

(define-public crate-typeshare-annotation-1.0.2 (c (n "typeshare-annotation") (v "1.0.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "proc-macro"))) (d #t) (k 0)))) (h "1adpfhyz3lqjjbq2ym69mv62ymqyd5651gxlqdy8aa446l70srzw") (r "1.57")))

(define-public crate-typeshare-annotation-1.0.3 (c (n "typeshare-annotation") (v "1.0.3") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (f (quote ("parsing" "proc-macro"))) (d #t) (k 0)))) (h "0icmq1migbfbgr224zfhrhpm311ds8k14k4zj12dramfm3g2bkpc") (r "1.57")))

(define-public crate-typeshare-annotation-1.0.4 (c (n "typeshare-annotation") (v "1.0.4") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("parsing" "proc-macro"))) (d #t) (k 0)))) (h "0kx38ah6638pkqq5cac7nmvbg6x43v7fj5jgibla4lj8fv1dc5d6")))

