(define-module (crates-io ty pe typemap-meta-derive) #:use-module (crates-io))

(define-public crate-typemap-meta-derive-0.1.0 (c (n "typemap-meta-derive") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0n5r1x6l19dnssi9rvm66s3qg9v0bc26zzwxs7zg1ic1bl5pkczi")))

(define-public crate-typemap-meta-derive-0.2.0 (c (n "typemap-meta-derive") (v "0.2.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1526gx8yzx6s9ykqblp5zxz0zssf77w4kc85a4z8n50wddrq5kbw")))

