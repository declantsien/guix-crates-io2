(define-module (crates-io ty oz tyozo) #:use-module (crates-io))

(define-public crate-tyozo-0.1.0 (c (n "tyozo") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "1lmxy2wi70vzxi21ib829rkrlg6xq9fxla7fyfwfd6lacfgbn3yl")))

