(define-module (crates-io ty ch tyche) #:use-module (crates-io))

(define-public crate-tyche-0.1.0 (c (n "tyche") (v "0.1.0") (d (list (d (n "ariadne") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "chumsky") (r "^1.0.0-alpha.7") (f (quote ("label"))) (o #t) (d #t) (k 0)) (d (n "fastrand") (r "^2.0.1") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)))) (h "05rv825zrfy90d917wz3abmjbxcvxxp037w1mq0pvhr5i4rb7is9") (f (quote (("default" "parse" "fastrand")))) (s 2) (e (quote (("parse" "dep:chumsky") ("fastrand" "dep:fastrand") ("build-binary" "parse" "fastrand" "dep:ariadne"))))))

(define-public crate-tyche-0.2.0 (c (n "tyche") (v "0.2.0") (d (list (d (n "ariadne") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "chumsky") (r "^1.0.0-alpha.7") (f (quote ("label"))) (o #t) (d #t) (k 0)) (d (n "fastrand") (r "^2.0.1") (o #t) (d #t) (k 0)) (d (n "pretty-readme") (r "^0.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)))) (h "0fh7kjlgja3nwcqcjfvqzrlkymf6p0f32y3dzvhj63062vyg0j5b") (f (quote (("default" "parse" "fastrand")))) (s 2) (e (quote (("parse" "dep:chumsky") ("fastrand" "dep:fastrand") ("build-binary" "parse" "fastrand" "dep:ariadne"))))))

