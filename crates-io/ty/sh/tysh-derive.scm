(define-module (crates-io ty sh tysh-derive) #:use-module (crates-io))

(define-public crate-tysh-derive-0.1.0 (c (n "tysh-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0bfwlv9wz25dzapphqg0l02h9b9mhzy4z4wqhjm0c0h455p1hi2x") (r "1.65")))

