(define-module (crates-io ty sh tysh) #:use-module (crates-io))

(define-public crate-tysh-0.1.0 (c (n "tysh") (v "0.1.0") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "smallvec") (r "^1") (o #t) (d #t) (k 0)) (d (n "smallvec") (r "^1") (f (quote ("serde"))) (d #t) (k 2)) (d (n "tysh-derive") (r "^0.1.0") (d #t) (k 0)))) (h "13z996wqcbmk8ha4igdxf9an7adhwapv196129xqblhk59zhzixh") (f (quote (("std" "alloc") ("int128") ("default" "std") ("alloc")))) (r "1.65")))

(define-public crate-tysh-0.1.1 (c (n "tysh") (v "0.1.1") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "smallvec") (r "^1") (o #t) (d #t) (k 0)) (d (n "smallvec") (r "^1") (f (quote ("serde"))) (d #t) (k 2)) (d (n "tysh-derive") (r "^0.1.0") (d #t) (k 0)))) (h "1sbaqs5izhwcqjhx4xpv3xg6nqss37y949irljjp3jx9qjdwqda6") (f (quote (("std" "alloc") ("int128") ("default" "std") ("alloc")))) (r "1.65")))

