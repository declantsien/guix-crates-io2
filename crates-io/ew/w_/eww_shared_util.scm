(define-module (crates-io ew w_ eww_shared_util) #:use-module (crates-io))

(define-public crate-eww_shared_util-0.1.0 (c (n "eww_shared_util") (v "0.1.0") (d (list (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "ref-cast") (r "^1.0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "152m1a6s6kl1p57fd3hi91j0rxjmfclv3j0xw3lqpbizma0m9892")))

