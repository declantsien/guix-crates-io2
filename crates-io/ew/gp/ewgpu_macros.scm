(define-module (crates-io ew gp ewgpu_macros) #:use-module (crates-io))

(define-public crate-ewgpu_macros-0.1.0 (c (n "ewgpu_macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0yyzxkjpz4d8afxy68akbjhcpg1svcvdqhjgb02k3inqyg360kk0")))

