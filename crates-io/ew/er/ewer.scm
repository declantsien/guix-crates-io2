(define-module (crates-io ew er ewer) #:use-module (crates-io))

(define-public crate-ewer-0.0.1 (c (n "ewer") (v "0.0.1") (h "0ggld0d6pyrvmmvbxzwzbcsqy7d9bwffz9xadmw1csalylgm7gnf")))

(define-public crate-ewer-0.0.2 (c (n "ewer") (v "0.0.2") (d (list (d (n "tiny_http") (r "^0.7.0") (d #t) (k 0)))) (h "04xk0h0bai7pvlk56cx6zml4f2lqkkv08a9v0w2fyrv4jmm8pf41")))

