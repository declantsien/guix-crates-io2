(define-module (crates-io ew ma ewma) #:use-module (crates-io))

(define-public crate-ewma-0.1.0 (c (n "ewma") (v "0.1.0") (h "1mqxjjmpfz8ngrx2wacyqg25mddja91p32xrv37z46xjsz8n5q2r")))

(define-public crate-ewma-0.1.1 (c (n "ewma") (v "0.1.1") (h "0m67yk0xscbzxkfssyyllm2624kr5q00iif0a5qqnrwb79zjc81g")))

