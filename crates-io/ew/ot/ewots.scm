(define-module (crates-io ew ot ewots) #:use-module (crates-io))

(define-public crate-ewots-0.1.0 (c (n "ewots") (v "0.1.0") (d (list (d (n "cortex-m") (r "^0.6.0") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.10") (d #t) (k 0)) (d (n "cortex-m-semihosting") (r "^0.3.3") (d #t) (k 0)) (d (n "panic-halt") (r "^0.2.0") (d #t) (k 0)) (d (n "wyhash") (r "^0.3.0") (d #t) (k 0)))) (h "1smn1iwnnimh0czdffsfhhbx05rja75qb988na50v2lrk9x29yzw") (f (quote (("256") ("16"))))))

