(define-module (crates-io ew as ewasm_api) #:use-module (crates-io))

(define-public crate-ewasm_api-0.4.0 (c (n "ewasm_api") (v "0.4.0") (h "1y26d3ic0mm7plcvpyrrlcg8fvznln77ib86b4z17pb6qalmq462")))

(define-public crate-ewasm_api-0.5.0 (c (n "ewasm_api") (v "0.5.0") (h "0nmskmcmmmi8x52j4srqggc7578z76wwl7xngmgi91851518ahys")))

(define-public crate-ewasm_api-0.5.1 (c (n "ewasm_api") (v "0.5.1") (h "00lnxpks758a16hhhn0xzyv4ik7yh2cdmwd7ibkfsbv4qh6qzykk")))

(define-public crate-ewasm_api-0.5.2 (c (n "ewasm_api") (v "0.5.2") (h "0z23x7ykav3ncj2d207nbyrh984yxxml514a6xxr85rbbb3x0hp5")))

(define-public crate-ewasm_api-0.6.0 (c (n "ewasm_api") (v "0.6.0") (h "0lh584yqmdncy63shipmm7ci8cb25jwwjzjf62bsxfk9sgs6zwa8") (f (quote (("std") ("default" "std"))))))

(define-public crate-ewasm_api-0.7.0 (c (n "ewasm_api") (v "0.7.0") (h "1gb0a8ww5ds5a5djy5y9nwn8cp1c83l4awxpp3vk8gkfr5jv3h5d") (f (quote (("std") ("default" "std"))))))

(define-public crate-ewasm_api-0.8.0 (c (n "ewasm_api") (v "0.8.0") (h "00kjbvw1cjzbmiqxc4p6ncmh252cv7gi311ydn19h8lfhc5qd66y") (f (quote (("std") ("default" "std") ("debug"))))))

(define-public crate-ewasm_api-0.9.0 (c (n "ewasm_api") (v "0.9.0") (d (list (d (n "cfg-if") (r "^0.1.7") (d #t) (k 0)) (d (n "qimalloc") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4.4") (o #t) (d #t) (k 0)))) (h "06jbark65pgn6xyhj4d50vgpwvsvjpnh9q6ml3df8jsa54bidiri") (f (quote (("std") ("experimental") ("default" "std" "wee_alloc") ("debug"))))))

(define-public crate-ewasm_api-0.10.0 (c (n "ewasm_api") (v "0.10.0") (d (list (d (n "cfg-if") (r "^0.1.7") (d #t) (k 0)) (d (n "qimalloc") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4.4") (o #t) (d #t) (k 0)))) (h "0mswjra84drj645r03isa6c3vzi3lf3biwc9zmgikk2sff36knkk") (f (quote (("std") ("experimental") ("eth2") ("default" "std" "wee_alloc") ("debug"))))))

(define-public crate-ewasm_api-0.11.0 (c (n "ewasm_api") (v "0.11.0") (d (list (d (n "cfg-if") (r "^0.1.7") (d #t) (k 0)) (d (n "qimalloc") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4.4") (o #t) (d #t) (k 0)))) (h "0ykzlhj1na51fhkq4fa00zrz44cph58rpwg3yq58sk7va2hc52xi") (f (quote (("std") ("experimental") ("eth2") ("default" "std" "wee_alloc") ("debug"))))))

