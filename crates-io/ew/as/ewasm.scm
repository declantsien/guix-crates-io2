(define-module (crates-io ew as ewasm) #:use-module (crates-io))

(define-public crate-ewasm-0.1.0 (c (n "ewasm") (v "0.1.0") (d (list (d (n "wasmi") (r "^0.5.0") (d #t) (k 0)))) (h "0fkvlmyk81fahf6bmb0hg77gh061dhnp8i79sc4vg11bvpbd7s9z")))

(define-public crate-ewasm-0.1.1 (c (n "ewasm") (v "0.1.1") (d (list (d (n "wasmi") (r "^0.5.0") (d #t) (k 0)))) (h "0mj4c6snwk01jyh25yig8gfpynqwyfssbk371wpxam58v48kfbdj")))

(define-public crate-ewasm-0.1.2 (c (n "ewasm") (v "0.1.2") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "wasmi") (r "^0.5.0") (d #t) (k 0)))) (h "1liyaf5cb0harmks0z3q57f7dlp4yjxr62q8zyglkplzh0abhfnl")))

(define-public crate-ewasm-0.1.3 (c (n "ewasm") (v "0.1.3") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "wabt") (r "^0.9.2") (d #t) (k 0)) (d (n "wasmi") (r "^0.5.0") (d #t) (k 0)))) (h "1sqpa4i9s9w8nv35pdybmyqh557402660a1jrf04gi4kracrlw7v") (f (quote (("extra-pages"))))))

(define-public crate-ewasm-0.2.0 (c (n "ewasm") (v "0.2.0") (d (list (d (n "arrayref") (r "^0.3.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "typed-builder") (r "^0.3.0") (d #t) (k 0)) (d (n "wabt") (r "^0.9.2") (d #t) (k 0)) (d (n "wasmi") (r "^0.5.0") (d #t) (k 0)))) (h "0dn7djpw5rpq091mk9lsvh7pkn0xldkmyi54kb9h1j8h7k33inbh")))

(define-public crate-ewasm-0.2.1 (c (n "ewasm") (v "0.2.1") (d (list (d (n "arrayref") (r "^0.3.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "typed-builder") (r "^0.3.0") (d #t) (k 0)) (d (n "wabt") (r "^0.9.2") (d #t) (k 0)) (d (n "wasmi") (r "^0.5.0") (d #t) (k 0)))) (h "195nk2myxahn5arl9yqkcpd8lvhkg9crik5b26f2cr47xspww63a") (f (quote (("extra-pages"))))))

(define-public crate-ewasm-0.2.2 (c (n "ewasm") (v "0.2.2") (d (list (d (n "arrayref") (r "^0.3.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "typed-builder") (r "^0.3.0") (d #t) (k 0)) (d (n "wabt") (r "^0.9.2") (d #t) (k 0)) (d (n "wasmi") (r "^0.5.0") (d #t) (k 0)))) (h "1l1d6cz1pzqlg98h6p17n2vzzf43dmi19nrqil4l0qk53sw26jxl") (f (quote (("extra-pages"))))))

(define-public crate-ewasm-0.2.3 (c (n "ewasm") (v "0.2.3") (d (list (d (n "arrayref") (r "^0.3.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "typed-builder") (r "^0.3.0") (d #t) (k 0)) (d (n "wabt") (r "^0.9.2") (d #t) (k 0)) (d (n "wasmi") (r "^0.5.0") (d #t) (k 0)))) (h "1znc4xd6jpdqlb444zxkxz2pqsr3dj4s8vimhrs4aa4lp0q4dbsc") (f (quote (("extra-pages"))))))

