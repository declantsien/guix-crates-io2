(define-module (crates-io jo ul joule) #:use-module (crates-io))

(define-public crate-joule-0.1.0 (c (n "joule") (v "0.1.0") (d (list (d (n "clap") (r "^3.0.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "powercap") (r "^0.3.5") (f (quote ("with-serde"))) (d #t) (k 0)) (d (n "powercap") (r "^0.3.5") (f (quote ("with-serde" "mock"))) (d #t) (k 2)) (d (n "procfs") (r "^0.11") (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "temp-dir") (r "^0.1") (d #t) (k 2)))) (h "0jdqp7in4m1s9gkwx37n0mqmwrj37a8z07yd6628jpi2jgvhrzmm")))

(define-public crate-joule-0.2.0 (c (n "joule") (v "0.2.0") (d (list (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "powercap") (r "^0.3") (d #t) (k 0)) (d (n "powercap") (r "^0.3") (f (quote ("mock"))) (d #t) (k 2)) (d (n "procfs") (r "^0.12") (k 0)) (d (n "temp-dir") (r "^0.1") (d #t) (k 2)))) (h "19xgh5nzbif9s1lsc7h5cy3qcav3rwmlld95hfv7dpqdacvcpazl")))

