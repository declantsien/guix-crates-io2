(define-module (crates-io jo xi joxide) #:use-module (crates-io))

(define-public crate-joxide-0.0.1 (c (n "joxide") (v "0.0.1") (d (list (d (n "argh") (r "^0.1.9") (d #t) (k 0)))) (h "1960alwa4griyyqwajkmiv1g2wf7kqvlzy7yinbb3x4id7qbw025")))

(define-public crate-joxide-0.0.2 (c (n "joxide") (v "0.0.2") (d (list (d (n "argh") (r "^0.1.9") (d #t) (k 0)))) (h "120rkl01a7wyb1rnlmwznxdwhq5g1c7mqfq83rjj11ql1d4r85zw")))

(define-public crate-joxide-0.0.3 (c (n "joxide") (v "0.0.3") (d (list (d (n "argh") (r "^0.1.9") (d #t) (k 0)))) (h "16wldi2gp1fyf6dkflddp07lb3qgb29hsks0zvkhqhicxkp1nrln")))

(define-public crate-joxide-0.1.3 (c (n "joxide") (v "0.1.3") (d (list (d (n "argh") (r "^0.1.9") (d #t) (k 0)) (d (n "glob") (r "^0.3.1") (d #t) (k 0)))) (h "1hxq5bbb2z36kizx88wx1zr19nag68s0c1wzd96mzinpl8cyrq3d")))

