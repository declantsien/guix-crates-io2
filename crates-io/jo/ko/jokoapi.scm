(define-module (crates-io jo ko jokoapi) #:use-module (crates-io))

(define-public crate-jokoapi-0.1.0 (c (n "jokoapi") (v "0.1.0") (d (list (d (n "const_format") (r "^0.2.14") (d #t) (k 0)) (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "ureq") (r "^2.1.1") (f (quote ("json"))) (d #t) (k 0)) (d (n "url") (r "^2.2.2") (f (quote ("serde"))) (d #t) (k 0)))) (h "1xdwkwybxzs6y86hblzgpxq182krxw1sq9f9jajbddkmjq9mfqrg")))

(define-public crate-jokoapi-0.1.1 (c (n "jokoapi") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.41") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.50") (d #t) (k 0)) (d (n "const_format") (r "^0.2.14") (d #t) (k 0)) (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.4") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "tokio") (r "^1.7.1") (f (quote ("rt" "macros"))) (d #t) (k 0)) (d (n "url") (r "^2.2.2") (f (quote ("serde"))) (d #t) (k 0)))) (h "1w00rraglqq5wcips64jzd2ciqr7anxqcp2z3cd6scpm8g48hb43")))

