(define-module (crates-io jo ur journald-to-cloudwatch) #:use-module (crates-io))

(define-public crate-journald-to-cloudwatch-0.9.0 (c (n "journald-to-cloudwatch") (v "0.9.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "rusoto_core") (r "^0.40") (d #t) (k 0)) (d (n "rusoto_ec2") (r "^0.40") (d #t) (k 0)) (d (n "rusoto_logs") (r "^0.40") (d #t) (k 0)) (d (n "systemd") (r "^0.4.0") (d #t) (k 0)))) (h "0hpjraza7bfysj4nlq8vnf837gs2xkb3zgc37g0kvvgmfwfj9syq")))

(define-public crate-journald-to-cloudwatch-1.0.0 (c (n "journald-to-cloudwatch") (v "1.0.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "rusoto_core") (r "^0.40") (d #t) (k 0)) (d (n "rusoto_ec2") (r "^0.40") (d #t) (k 0)) (d (n "rusoto_logs") (r "^0.40") (d #t) (k 0)) (d (n "systemd") (r "^0.4") (d #t) (k 0)))) (h "0wyhnyhvngx4xlq64ljmhy9lkxd3fimk9nwb2j44v2ywcig1l1x5")))

(define-public crate-journald-to-cloudwatch-1.1.0 (c (n "journald-to-cloudwatch") (v "1.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "rusoto_core") (r "^0.40") (d #t) (k 0)) (d (n "rusoto_ec2") (r "^0.40") (d #t) (k 0)) (d (n "rusoto_logs") (r "^0.40") (d #t) (k 0)) (d (n "systemd") (r "^0.4") (d #t) (k 0)))) (h "03hm8m59lsigrcf9zn4jwd7zgmynw14296f68gw4q694mfsg5838")))

(define-public crate-journald-to-cloudwatch-1.2.0 (c (n "journald-to-cloudwatch") (v "1.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "rusoto_core") (r "^0.40") (d #t) (k 0)) (d (n "rusoto_ec2") (r "^0.40") (d #t) (k 0)) (d (n "rusoto_logs") (r "^0.40") (d #t) (k 0)) (d (n "systemd") (r "^0.4") (d #t) (k 0)))) (h "11pl2hcpp0h4nsjbw5nnzkk1w4smqfbh5vv4rpm2sp01r14gybll")))

(define-public crate-journald-to-cloudwatch-1.2.1 (c (n "journald-to-cloudwatch") (v "1.2.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "rusoto_core") (r "^0.40") (d #t) (k 0)) (d (n "rusoto_ec2") (r "^0.40") (d #t) (k 0)) (d (n "rusoto_logs") (r "^0.40") (d #t) (k 0)) (d (n "systemd") (r "^0.4") (d #t) (k 0)))) (h "15p25r542254p6m7zdfm58ani8b7m15knqfbz0nz7q3gwhq6i2n0")))

