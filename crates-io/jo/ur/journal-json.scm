(define-module (crates-io jo ur journal-json) #:use-module (crates-io))

(define-public crate-journal-json-0.1.0 (c (n "journal-json") (v "0.1.0") (d (list (d (n "getset") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "028z22qrvbqgi1fqmkp8yf9jghyh1w460qlqnjlb594pv3f0z8h9")))

