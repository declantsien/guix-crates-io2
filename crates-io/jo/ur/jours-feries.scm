(define-module (crates-io jo ur jours-feries) #:use-module (crates-io))

(define-public crate-jours-feries-0.1.0 (c (n "jours-feries") (v "0.1.0") (d (list (d (n "chrono") (r "^0") (f (quote ("serde"))) (o #t) (d #t) (k 1)) (d (n "reqwest") (r "^0") (o #t) (d #t) (k 1)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 1)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 1)) (d (n "tokio") (r "^1") (f (quote ("full"))) (o #t) (d #t) (k 1)))) (h "0ayph9frx67zr0jhmiil2lfizzbjvlffls51wvmqyyvzlmhd00bn") (f (quote (("generate" "reqwest" "tokio" "serde_json" "chrono" "serde"))))))

(define-public crate-jours-feries-0.1.1 (c (n "jours-feries") (v "0.1.1") (d (list (d (n "chrono") (r "^0") (f (quote ("serde"))) (o #t) (d #t) (k 1)) (d (n "reqwest") (r "^0") (o #t) (d #t) (k 1)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 1)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 1)) (d (n "tokio") (r "^1") (f (quote ("full"))) (o #t) (d #t) (k 1)))) (h "1r4k4zfgzwaqg0djxhv2lxgvffz9kd8axgb0yha1856fjfmngw31") (f (quote (("generate" "reqwest" "tokio" "serde_json" "chrono" "serde"))))))

