(define-module (crates-io jo ur journey) #:use-module (crates-io))

(define-public crate-journey-0.0.1 (c (n "journey") (v "0.0.1") (h "19pcbjv3fira342dfafrq1pm2qyx6im7ixm4si9fb5ks332xqqay") (y #t)))

(define-public crate-journey-0.0.2 (c (n "journey") (v "0.0.2") (d (list (d (n "chrono") (r "^0.4.10") (d #t) (k 0)) (d (n "failure") (r "^0.1.6") (d #t) (k 0)) (d (n "rustyline") (r "^6.0.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.11") (d #t) (k 0)) (d (n "termcolor") (r "^1.1.0") (d #t) (k 0)))) (h "0g675vh1lk9cxapf1i118s0wv5z03dnn2jxydxv9wd36ynzql4yk") (y #t)))

