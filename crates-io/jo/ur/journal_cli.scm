(define-module (crates-io jo ur journal_cli) #:use-module (crates-io))

(define-public crate-journal_cli-0.1.0 (c (n "journal_cli") (v "0.1.0") (d (list (d (n "clap") (r "^2.29") (d #t) (k 0)) (d (n "structopt") (r "^0.2.10") (d #t) (k 0)))) (h "0fa59qh5z3fwgvrg446pql7qv3bncayig9f6qjyhp9b4ha7pr42h")))

(define-public crate-journal_cli-0.1.1 (c (n "journal_cli") (v "0.1.1") (d (list (d (n "clap") (r "^2.29") (d #t) (k 0)) (d (n "structopt") (r "^0.2.10") (d #t) (k 0)))) (h "0iv9n8300iwc8vfknsyjam69bw6iw4annwa4aix5ssv5npq2ncg8")))

(define-public crate-journal_cli-0.1.2 (c (n "journal_cli") (v "0.1.2") (d (list (d (n "clap") (r "^2.29") (d #t) (k 0)) (d (n "structopt") (r "^0.2.10") (d #t) (k 0)))) (h "1smry2vd3kzjlabbzi2r3mvw6cnbwp7q2bhxs0gpxb7r8qmk0shd")))

(define-public crate-journal_cli-0.1.3 (c (n "journal_cli") (v "0.1.3") (d (list (d (n "clap") (r "^2.29") (d #t) (k 0)) (d (n "structopt") (r "^0.2.10") (d #t) (k 0)))) (h "1vfkvhz68whp0f68d6667yz8mz87xhlrqvmagk01pfldf8p2s6zz")))

