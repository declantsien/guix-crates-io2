(define-module (crates-io jo ur journal_entry) #:use-module (crates-io))

(define-public crate-journal_entry-0.1.0 (c (n "journal_entry") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "fstrings") (r "^0.2.3") (d #t) (k 0)) (d (n "markdown-gen") (r "^1.2.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "1df9ncbg411ya8y1zsbsdz8izsjmzpv30732crqx6ngw4vaprnvg")))

(define-public crate-journal_entry-0.1.1 (c (n "journal_entry") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "fstrings") (r "^0.2.3") (d #t) (k 0)) (d (n "markdown-gen") (r "^1.2.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "0qdffn2x8s5ymrr1y35gjb43grxv17kxxhwxg5cqymkv79q3sl36")))

