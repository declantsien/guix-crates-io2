(define-module (crates-io jo ur journal-env-logger) #:use-module (crates-io))

(define-public crate-journal-env-logger-0.1.0 (c (n "journal-env-logger") (v "0.1.0") (d (list (d (n "rusty-hook") (r "^0.11") (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-journald") (r "^0.3") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("env-filter"))) (d #t) (k 0)))) (h "1mv0cj7zq224wwm2h0c3fqlnksc5l604120v9v3agsafl6yrvpk3")))

