(define-module (crates-io jo ur journal_gateway) #:use-module (crates-io))

(define-public crate-journal_gateway-0.2.0 (c (n "journal_gateway") (v "0.2.0") (d (list (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)) (d (n "hyper") (r "^0.10.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^1.4.1") (d #t) (k 0)))) (h "0n1gyvxxj3srxwqmjv9wjszyv1hvbn0nhc7srs33mvxnpz359q30")))

(define-public crate-journal_gateway-0.2.1 (c (n "journal_gateway") (v "0.2.1") (d (list (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)) (d (n "hyper") (r "^0.10.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^1.4.1") (d #t) (k 0)))) (h "1cy0s0lva4l8fkmwbfz7z4wzaihnkka0qdlhaw114f2ni5xvv284")))

