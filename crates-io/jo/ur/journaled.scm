(define-module (crates-io jo ur journaled) #:use-module (crates-io))

(define-public crate-journaled-0.0.1 (c (n "journaled") (v "0.0.1") (d (list (d (n "log") (r "^0.4") (f (quote ("std"))) (o #t) (d #t) (k 0)) (d (n "nix") (r "^0.26") (f (quote ("fs"))) (d #t) (k 0)) (d (n "once_cell") (r "^1") (o #t) (d #t) (k 0)) (d (n "slog") (r "^2.7") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "libsystemd") (r "^0.6") (d #t) (k 2)))) (h "0agj48ma5g00dgx1kfyhnv7yly4x3068jqry93hkvz1kjmyajix9") (s 2) (e (quote (("stdlog" "dep:log" "dep:once_cell") ("slog" "dep:slog"))))))

