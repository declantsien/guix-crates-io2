(define-module (crates-io jo ff joff) #:use-module (crates-io))

(define-public crate-joff-0.1.0 (c (n "joff") (v "0.1.0") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "kv") (r "^0.22.0") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)) (d (n "quicli") (r "^0.4.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "rocket") (r "^0.4.7") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "url") (r "^2.2.1") (d #t) (k 0)))) (h "1p65gq1pygljxc6822pr16a75f28nqpw5g0c4rv06ds1f5k5nzph")))

