(define-module (crates-io jo sa josa) #:use-module (crates-io))

(define-public crate-josa-0.1.0 (c (n "josa") (v "0.1.0") (d (list (d (n "hangul") (r "^0.1.2") (d #t) (k 0)))) (h "1qn9svf1bshwh7h8g27prbw1d0q74dmn7h1axs9808c4pp0994sw")))

(define-public crate-josa-0.1.1 (c (n "josa") (v "0.1.1") (d (list (d (n "hangul") (r "^0.1.3") (d #t) (k 0)))) (h "16n243avpsk7mylra8vfm49amphm7m71cjfvbi3fg55cy49myq4b")))

(define-public crate-josa-0.1.2 (c (n "josa") (v "0.1.2") (d (list (d (n "hangul") (r "^0.1.3") (d #t) (k 0)))) (h "0jfr3fq5x51lghd7q7x7m5nsnalsvcnsqjcvfcld7a11j9g115wb")))

