(define-module (crates-io jo z- joz-logger) #:use-module (crates-io))

(define-public crate-joz-logger-0.1.0 (c (n "joz-logger") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)))) (h "0pcgv16jk7xrmlaz5xr0bqj1zdg7b0ibfh8aricl16v9j78nnp1y")))

(define-public crate-joz-logger-0.1.1 (c (n "joz-logger") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)))) (h "09hmiq71ba5hhkgfcnjy4jd5hcgz7039482kvcn3a8d9dzb1pz9r")))

(define-public crate-joz-logger-0.2.0 (c (n "joz-logger") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1c93gccdd2gnrwsjx636fcb58bazn3f501c8pkj4r8sw916q7vdq")))

(define-public crate-joz-logger-0.2.1 (c (n "joz-logger") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "18ps1f5mb5ffcg43zblz4mcb79y9f4360n334pw5dn1l5vlrcvgl")))

(define-public crate-joz-logger-0.2.2 (c (n "joz-logger") (v "0.2.2") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0883qq9xdp287jqxbmfqq04485drj372rvby4873pkz8d863fdl0")))

