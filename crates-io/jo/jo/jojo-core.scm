(define-module (crates-io jo jo jojo-core) #:use-module (crates-io))

(define-public crate-jojo-core-0.1.0 (c (n "jojo-core") (v "0.1.0") (d (list (d (n "dic") (r "^0.1.5") (d #t) (k 0)) (d (n "org-tangle") (r "^0.1") (d #t) (k 1)) (d (n "uuid") (r "^0.7") (f (quote ("v4"))) (d #t) (k 0)))) (h "13sl6lz497x3swj2bjidi2zx3pxyxnar8xkqi9hs2bvhg38jf2rb") (y #t)))

