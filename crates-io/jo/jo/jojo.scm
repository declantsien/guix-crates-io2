(define-module (crates-io jo jo jojo) #:use-module (crates-io))

(define-public crate-jojo-0.1.0 (c (n "jojo") (v "0.1.0") (h "1az6gibvs7wwk45ngvkm8z6yjvi2gkmf5a9j37rzbrhpdrki9ggi") (y #t)))

(define-public crate-jojo-0.1.1 (c (n "jojo") (v "0.1.1") (d (list (d (n "dic") (r "^0.1.0") (d #t) (k 0)))) (h "0q38bbs8g0cbhmv7sz71i11ay2j1zwwcqs5s3zv6fq0v2kkxv54g") (y #t)))

(define-public crate-jojo-0.1.2 (c (n "jojo") (v "0.1.2") (d (list (d (n "dic") (r "^0.1.3") (d #t) (k 0)))) (h "12y2xpcs0kqs2rx4krp0rmnksa6fikywvkpzxqr64ahmfi2zrzr1") (y #t)))

(define-public crate-jojo-0.1.3 (c (n "jojo") (v "0.1.3") (d (list (d (n "dic") (r "^0.1.3") (d #t) (k 0)))) (h "1i9b9dah9kwk0gyk283xgam4jixsaj4317f0wdkmpnid2g3q6d8p") (y #t)))

(define-public crate-jojo-0.1.4 (c (n "jojo") (v "0.1.4") (d (list (d (n "dic") (r "^0.1.4") (d #t) (k 0)))) (h "1r0gvwra160x9zz93a7mlwqh8fys8j944iw3lhhc7jpkxw7bf2ma") (y #t)))

(define-public crate-jojo-0.1.6 (c (n "jojo") (v "0.1.6") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "dic") (r "^0.1.4") (d #t) (k 0)) (d (n "org-tangle") (r "^0.1.0") (d #t) (k 1)) (d (n "uuid") (r "^0.7") (f (quote ("v4"))) (d #t) (k 0)))) (h "10z79kcwh6w54kr52pl8gfwcc3ahcqcw3ns19nshw6pci2li541q") (y #t)))

(define-public crate-jojo-0.1.7 (c (n "jojo") (v "0.1.7") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "dic") (r "^0.1.4") (d #t) (k 0)) (d (n "linefeed") (r "^0.5") (d #t) (k 0)) (d (n "org-tangle") (r "^0.1.0") (d #t) (k 1)) (d (n "uuid") (r "^0.7") (f (quote ("v4"))) (d #t) (k 0)))) (h "0cg6aya51mwm02c3nwah9gglyg9iw2wm96pcg6cpy9qq8islbpzp") (y #t)))

(define-public crate-jojo-0.1.8 (c (n "jojo") (v "0.1.8") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "dic") (r "^0.1.4") (d #t) (k 0)) (d (n "linefeed") (r "^0.5") (d #t) (k 0)) (d (n "org-tangle") (r "^0.1.0") (d #t) (k 1)) (d (n "uuid") (r "^0.7") (f (quote ("v4"))) (d #t) (k 0)))) (h "00mvlalaqb678c2b6aqpnzzqgipblvy9fs6dbvnwp2hjrgzrr103") (y #t)))

(define-public crate-jojo-0.1.9 (c (n "jojo") (v "0.1.9") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "dic") (r "^0.1.5") (d #t) (k 0)) (d (n "linefeed") (r "^0.5") (d #t) (k 0)) (d (n "org-tangle") (r "^0.1.0") (d #t) (k 1)) (d (n "uuid") (r "^0.7") (f (quote ("v4"))) (d #t) (k 0)))) (h "00crqzrr0bp62mgwn1fjllg9abpb5rn6zpj6rkfd6mjsbsj3r255") (y #t)))

(define-public crate-jojo-0.1.10 (c (n "jojo") (v "0.1.10") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "dic") (r "^0.1.5") (d #t) (k 0)) (d (n "linefeed") (r "^0.5") (d #t) (k 0)) (d (n "org-tangle") (r "^0.1") (d #t) (k 1)) (d (n "uuid") (r "^0.7") (f (quote ("v4"))) (d #t) (k 0)))) (h "014hz4xxbxi6mz2rbwmis653qdgs22yf8lqzw7lj01m9dgj4809c") (y #t)))

(define-public crate-jojo-0.1.11 (c (n "jojo") (v "0.1.11") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "dic") (r "^0.1.5") (d #t) (k 0)) (d (n "linefeed") (r "^0.5") (d #t) (k 0)) (d (n "org-tangle") (r "^0.1") (d #t) (k 1)) (d (n "uuid") (r "^0.7") (f (quote ("v4"))) (d #t) (k 0)))) (h "0vhds2zxvw7p4cffy3c4dy4p413hp986mmv47b8y39jsaa42rsz9") (y #t)))

(define-public crate-jojo-0.1.12 (c (n "jojo") (v "0.1.12") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "dic") (r "^0.1.5") (d #t) (k 0)) (d (n "linefeed") (r "^0.5") (d #t) (k 0)) (d (n "org-tangle") (r "^0.1") (d #t) (k 1)) (d (n "uuid") (r "^0.7") (f (quote ("v4"))) (d #t) (k 0)))) (h "1ygfwyhyr5l59nszfyfv8vjk3b6w0zy8nmqa676f8x1v8ji2dsis") (y #t)))

(define-public crate-jojo-0.1.13 (c (n "jojo") (v "0.1.13") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "jojo-core") (r "^0.1") (d #t) (k 0)) (d (n "linefeed") (r "^0.5") (d #t) (k 0)) (d (n "org-tangle") (r "^0.1") (d #t) (k 1)))) (h "0r38s0zjg3q4d8xvi0xglb7d9khy3s7z8kspz5hgaj7gygxz3h88") (y #t)))

(define-public crate-jojo-0.1.14 (c (n "jojo") (v "0.1.14") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "jojo-core") (r "^0.1") (d #t) (k 0)) (d (n "linefeed") (r "^0.5") (d #t) (k 0)) (d (n "org-tangle") (r "^0.1") (d #t) (k 1)))) (h "1y5akc30xjbslckhp6yng0b3n1nhhpi9l4305l0w9x0hnwab382c") (y #t)))

