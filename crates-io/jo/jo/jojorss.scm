(define-module (crates-io jo jo jojorss) #:use-module (crates-io))

(define-public crate-jojorss-0.1.0 (c (n "jojorss") (v "0.1.0") (d (list (d (n "dirs-next") (r "^2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "rss") (r "^1") (d #t) (k 0)))) (h "01phj1billxichmnxzs182qzp1824531x7a539hij3bj3gkmaprl")))

(define-public crate-jojorss-0.1.1 (c (n "jojorss") (v "0.1.1") (d (list (d (n "dirs-next") (r "^2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "rss") (r "^1") (d #t) (k 0)))) (h "0qmy9za23cq8cy8zzmxq47s62j6jc17ly8wppbh39sm5rx4bsw8n")))

(define-public crate-jojorss-0.1.2 (c (n "jojorss") (v "0.1.2") (d (list (d (n "dirs-next") (r "^2.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "rss") (r "^1.10") (d #t) (k 0)))) (h "0wq1qfqpbdgws0klwzmgzl17yghpj8yra3xkl28wc4ayswg2lf0x")))

(define-public crate-jojorss-0.1.3 (c (n "jojorss") (v "0.1.3") (d (list (d (n "dirs-next") (r "^2.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "rss") (r "^1.10") (d #t) (k 0)))) (h "05rmv4pbrmp7mcnmvlcq2jkmr6kjxk26zl94zfak7pqysc2hdbvy")))

