(define-module (crates-io jo in joinable) #:use-module (crates-io))

(define-public crate-joinable-0.1.0 (c (n "joinable") (v "0.1.0") (h "1zdivlikwhgxi56hqjhqjkva6yi713isqry2v37128wkf4v75vxg")))

(define-public crate-joinable-0.2.0-rc.1 (c (n "joinable") (v "0.2.0-rc.1") (d (list (d (n "irisdata") (r "^0.1") (d #t) (k 2)))) (h "13n5b7dxdd0d868b037qdfga4hsppv5b643ajgmwg1cjy9d9y2lc")))

(define-public crate-joinable-0.2.0 (c (n "joinable") (v "0.2.0") (d (list (d (n "irisdata") (r "^0.1") (d #t) (k 2)))) (h "1v7q029lm8lvv71pvl5xy8ilz7z33pk0bzyi45b73jyxq90gslya")))

