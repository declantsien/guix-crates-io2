(define-module (crates-io jo in joinkit) #:use-module (crates-io))

(define-public crate-joinkit-0.1.0 (c (n "joinkit") (v "0.1.0") (d (list (d (n "clap") (r "^1.5") (d #t) (k 0)) (d (n "itertools") (r "^0.4") (d #t) (k 0)))) (h "10d3byclfrf2kbjqaa90p7qp53rns72b0f052ab2zfsj0wb8nlxy")))

(define-public crate-joinkit-0.2.0 (c (n "joinkit") (v "0.2.0") (d (list (d (n "clap") (r "2.*") (d #t) (k 0)) (d (n "itertools") (r "^0.4") (d #t) (k 0)))) (h "1gv77lwy8k1igyg5rx2yslnd3cjaq02b4cwaqgzrad8nhylc6nkl")))

