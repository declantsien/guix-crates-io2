(define-module (crates-io jo in join_to_string) #:use-module (crates-io))

(define-public crate-join_to_string-0.1.0 (c (n "join_to_string") (v "0.1.0") (h "1nb3qa0gnbq1f7v49anf9l5hn0zk358ygh39qcc6gag83c5b2q9d")))

(define-public crate-join_to_string-0.1.1 (c (n "join_to_string") (v "0.1.1") (h "0cayr4r0x4ahxn0q54x9brwnvhyjdpz2wz3lbm5xssgxyf2wipbv")))

(define-public crate-join_to_string-0.1.2 (c (n "join_to_string") (v "0.1.2") (h "1llm12xrfh76cd2584piqylrwicrlfbzfsfcxbhybw8qgyypgwnv") (y #t)))

(define-public crate-join_to_string-0.1.3 (c (n "join_to_string") (v "0.1.3") (h "13x966rwhjp3cr7iwm6ijv5p74gna1nlb7z45g70c9lc1qlsbisd")))

