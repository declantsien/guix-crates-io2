(define-module (crates-io jo in join-string) #:use-module (crates-io))

(define-public crate-join-string-0.1.0 (c (n "join-string") (v "0.1.0") (h "0sk5v11hn85ps415h252sf6p70nv44z2pzk4v5qy6wlgh2xqam5j")))

(define-public crate-join-string-0.1.1 (c (n "join-string") (v "0.1.1") (h "13yypl3i844if7ipamfghn472d3khbw63yschbq7d51cx2nl9pqa")))

(define-public crate-join-string-0.2.0 (c (n "join-string") (v "0.2.0") (h "1zj361mhyyjlhf93wwzjxd2lch1ndsgn5j29c06030ich5h4gpj4")))

(define-public crate-join-string-0.2.1 (c (n "join-string") (v "0.2.1") (h "1sphm8bp0xm93kscwz98nrhmnvwiyz8qnxf95lb66mys5byv2lsi")))

(define-public crate-join-string-0.2.2 (c (n "join-string") (v "0.2.2") (h "1ily9a3srwnc2i46jb772pcb28l1xpxbfvqgdi09jam09magxwsi")))

(define-public crate-join-string-0.2.3 (c (n "join-string") (v "0.2.3") (h "0l5qgrwy5fqyhd78b7aiyfzix170h3kla02qi9ky1fj2d2ki4h8j")))

(define-public crate-join-string-0.3.0 (c (n "join-string") (v "0.3.0") (h "04pw6kvjkl6dbn0a7qmvbmlz35wq88zksamqw9ia81v6p2hnw99w")))

