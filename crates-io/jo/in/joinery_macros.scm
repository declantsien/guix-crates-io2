(define-module (crates-io jo in joinery_macros) #:use-module (crates-io))

(define-public crate-joinery_macros-0.0.1 (c (n "joinery_macros") (v "0.0.1") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.68") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0vv7jg5vkpyw9rj51219xb1ygr953k1jys210dca9mqhxynw83by") (y #t)))

