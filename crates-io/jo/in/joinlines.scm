(define-module (crates-io jo in joinlines) #:use-module (crates-io))

(define-public crate-joinlines-0.1.0 (c (n "joinlines") (v "0.1.0") (h "09dazd0qszjg4j2bp37xacxyaf31hf5jqq0qi2zhscjm9hd0m3ym")))

(define-public crate-joinlines-0.2.0 (c (n "joinlines") (v "0.2.0") (h "1rrfvrq1gq7y7ngxj8pm8yb95c4m3xablsag9v2byk6ncnvll8sl")))

(define-public crate-joinlines-0.3.0 (c (n "joinlines") (v "0.3.0") (h "057i10963iwd9d15w4rnqxg7jx8mbznsics4azvka7mi67j1lhhj")))

(define-public crate-joinlines-0.3.1 (c (n "joinlines") (v "0.3.1") (h "18mr29ijjdzlx6axcnp83lp41i02rkn93adhydl3brnjp52y22yn")))

