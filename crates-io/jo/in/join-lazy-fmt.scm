(define-module (crates-io jo in join-lazy-fmt) #:use-module (crates-io))

(define-public crate-join-lazy-fmt-0.9.0 (c (n "join-lazy-fmt") (v "0.9.0") (h "0xi2qya6s67y3pbzixs70xc29gji2v994jmzzn8g52i5y1xh0957") (f (quote (("nightly") ("default"))))))

(define-public crate-join-lazy-fmt-0.9.1 (c (n "join-lazy-fmt") (v "0.9.1") (h "0zk86lydv7ps7lm656yl8hs9dlf3bzq74d5s1hyly5yyl5v8dxv5") (f (quote (("nightly") ("default"))))))

(define-public crate-join-lazy-fmt-0.9.2 (c (n "join-lazy-fmt") (v "0.9.2") (h "1gq33nnzj0lcqs5l1y5bdykp41fypq7jj8585k7aba32yfx6c3z9") (f (quote (("nightly") ("default"))))))

