(define-module (crates-io jo in join_compile_commands_json) #:use-module (crates-io))

(define-public crate-join_compile_commands_json-0.1.0 (c (n "join_compile_commands_json") (v "0.1.0") (d (list (d (n "tokio") (r "^1.14.0") (f (quote ("fs" "macros" "rt" "rt-multi-thread" "sync"))) (d #t) (k 0)))) (h "15a3w1cva4h0wj1pvimcwybmpy29q26ljzsnws0wlfgl9klr4p67")))

