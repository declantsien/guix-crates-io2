(define-module (crates-io jo in joinery) #:use-module (crates-io))

(define-public crate-joinery-0.1.0 (c (n "joinery") (v "0.1.0") (h "0q3ji9za6ds4f6s8ar82b2gvwhwp3sx3yylgx2liaagcz5xk679m") (f (quote (("try_fold") ("trusted_len"))))))

(define-public crate-joinery-0.2.0 (c (n "joinery") (v "0.2.0") (h "0bc72x9cb2d2lwigflkh87yjvqdrfhnj6fzgqs4k59gn5lj6np0w") (f (quote (("nightly"))))))

(define-public crate-joinery-0.2.1 (c (n "joinery") (v "0.2.1") (h "06fff54rh1jxdpw00y13yf6my7vwwd6kr1pviins1fhg2dc326xm") (f (quote (("nightly"))))))

(define-public crate-joinery-0.2.2 (c (n "joinery") (v "0.2.2") (h "07vizdy3vxrpfyzvnmiasccgaynj5d2qy9j4631ml9wpcrdyiq18") (f (quote (("nightly"))))))

(define-public crate-joinery-0.2.3 (c (n "joinery") (v "0.2.3") (h "15k5gx1qmgi2nypnbgljjbh90mmwqrh2va9s14nzg9hpzqr8i6f1") (f (quote (("nightly"))))))

(define-public crate-joinery-1.0.0 (c (n "joinery") (v "1.0.0") (h "0r5zkqdkgc0dzg6fmlh6885b5ks3563dvpradkaxkh2la5zq3p45") (f (quote (("nightly"))))))

(define-public crate-joinery-1.1.0 (c (n "joinery") (v "1.1.0") (h "1xfnpibg367pdjyx4xbkansj5zqzkpg9z2l5sw2cdrfhyiyv7q3g") (f (quote (("nightly"))))))

(define-public crate-joinery-1.1.1 (c (n "joinery") (v "1.1.1") (h "00vgcbrjxxjfq6hwmncpsvrx2lbkah9nr9r9kllwfh0j20cwf7ij") (f (quote (("nightly"))))))

(define-public crate-joinery-1.1.2 (c (n "joinery") (v "1.1.2") (h "17r9kx97jqk00zdgnsxzzzz1ps0x1h187ry3vcy7bljhicbjgfs4") (f (quote (("nightly"))))))

(define-public crate-joinery-1.2.0 (c (n "joinery") (v "1.2.0") (h "0fan4qf33kzz9i6q1ij403zlx6l8kfdfcn0hg5mr5pg6qi6bdjyx") (f (quote (("nightly"))))))

(define-public crate-joinery-1.2.1 (c (n "joinery") (v "1.2.1") (h "0ccxad0spmc48vwyq7l698ff32q7h734bcm1jpwf2rm01fvpaz8j") (f (quote (("nightly"))))))

(define-public crate-joinery-1.2.2 (c (n "joinery") (v "1.2.2") (h "0qbsxf9sr9m8c76zqjx5llggc884gl4fq7naj6bg7wf7yxfz9vrh") (f (quote (("nightly"))))))

(define-public crate-joinery-2.0.0 (c (n "joinery") (v "2.0.0") (h "1fbpjk2b5xjj5mv5wlw1iigjvps5613sn7x2bna6lcz8n5lck9c5") (f (quote (("nightly"))))))

(define-public crate-joinery-2.1.0 (c (n "joinery") (v "2.1.0") (d (list (d (n "proc-macro2") (r "^1.0.27") (o #t) (k 0)) (d (n "quote") (r "^1.0.9") (o #t) (k 0)) (d (n "syn") (r "^1.0.72") (f (quote ("printing"))) (o #t) (k 0)))) (h "1xg4cjnz8cd6ya9hynb9wx79ijd3j6307f47aijviqzwyml7s5kj") (f (quote (("token-stream" "quote" "proc-macro2" "syn") ("nightly"))))))

(define-public crate-joinery-3.0.0 (c (n "joinery") (v "3.0.0") (d (list (d (n "proc-macro2") (r "^1.0.27") (o #t) (k 0)) (d (n "quote") (r "^1.0.9") (o #t) (k 0)))) (h "1lxyyyls8dim332m3zss85n6b69nqrivrfy1j5v6ygdrjc8300xc") (f (quote (("token-stream" "quote" "proc-macro2") ("nightly"))))))

(define-public crate-joinery-3.1.0 (c (n "joinery") (v "3.1.0") (d (list (d (n "proc-macro2") (r "^1.0.27") (o #t) (k 0)) (d (n "quote") (r "^1.0.9") (o #t) (k 0)))) (h "0xy35vwgqmsavsj06aihgqwdyhl8lvs8z2h6rxiaai5z5ghbvn2p") (f (quote (("token-stream" "quote" "proc-macro2") ("nightly"))))))

