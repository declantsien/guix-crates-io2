(define-module (crates-io jo ps jops) #:use-module (crates-io))

(define-public crate-jops-0.1.0 (c (n "jops") (v "0.1.0") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0mj1qga5kl7f7wq98k9j78fnkcq2bf0whshjn1fbsjscfzm1d6sz")))

(define-public crate-jops-0.1.1 (c (n "jops") (v "0.1.1") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1535ij34ckklm4pyvzg5yc3kw4n4lxdy7xc3jlna4vxvkwrhq2mx")))

(define-public crate-jops-0.2.0 (c (n "jops") (v "0.2.0") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0svzka2q4jclwihipcgskprrg2v6gvz48zq296w8g61ld1jd23g6")))

