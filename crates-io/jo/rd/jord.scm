(define-module (crates-io jo rd jord) #:use-module (crates-io))

(define-public crate-jord-0.1.0 (c (n "jord") (v "0.1.0") (h "0gqac2vrq81mlpi4xmpr3873k2sl7rxzxjc06jx5k1rlj93mj5vb")))

(define-public crate-jord-0.2.0 (c (n "jord") (v "0.2.0") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "1bkzw2w9xp05g34gjyqs7gi8dgk0nr726xcnw8v9hsjm4rb9m99a")))

(define-public crate-jord-0.3.0 (c (n "jord") (v "0.3.0") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "04h6mnvxwd9535vimx6qkigx0rh99k2kfaa2wl20ldh48mgyr1af") (r "1.65")))

(define-public crate-jord-0.4.0 (c (n "jord") (v "0.4.0") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "12mpz9nykqsajlk83ax6wc8l93l1ikj2y5himpqhnn3yisgn7h6g") (r "1.65")))

(define-public crate-jord-0.5.0 (c (n "jord") (v "0.5.0") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "0idlr5yas2icisvadn6aivzarjq1slycc51lxs0bvggh0srw6iin") (r "1.65")))

(define-public crate-jord-0.6.0 (c (n "jord") (v "0.6.0") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "0n5mn175792qsmlrfp2jjc78ddbsx27b0linvwwpk1jrlbi9fhnb") (r "1.65")))

(define-public crate-jord-0.7.0 (c (n "jord") (v "0.7.0") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "06dn3nwi1ymxxd1bvrvza736aiv57sfpfhhpkaa9n51kn3l4qpni") (r "1.65")))

(define-public crate-jord-0.8.0 (c (n "jord") (v "0.8.0") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "1mg1x4vzgdz4l1qlwkkmrpmb724lfy55pv77why3bx2ym2pf3mxm") (r "1.65")))

(define-public crate-jord-0.9.0 (c (n "jord") (v "0.9.0") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "0827idhy7bx7yv55zlx6wg0cd6swy4kfzpmc8ii4llk0ikpmq9p2") (r "1.65")))

(define-public crate-jord-0.10.0 (c (n "jord") (v "0.10.0") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "0ampmq7wkxic0jb0bg05bvw3vm2wr1kzzy468vr6n96y1fs0r907") (r "1.65")))

(define-public crate-jord-0.11.0 (c (n "jord") (v "0.11.0") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "01q80p20z739ikc8mx8rc3j00wxv6l8iwkklf55gzhx8qdls0irc") (r "1.65")))

(define-public crate-jord-0.12.0 (c (n "jord") (v "0.12.0") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "0if0rm42a19r2ald2383x9850rl3cavsxjf8rd670zx3cikzvxg7") (r "1.65")))

(define-public crate-jord-0.13.0 (c (n "jord") (v "0.13.0") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "0x5xrgijb2yhln79cghxfd5hy69ixlgskb0nhb3jw89x8ddq07ra") (r "1.65")))

(define-public crate-jord-0.14.0 (c (n "jord") (v "0.14.0") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "1a2dgp58hi23f2nii0acjn2dhyjrz4r6b0sz5778izf126mr3y10") (r "1.65")))

