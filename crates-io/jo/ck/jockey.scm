(define-module (crates-io jo ck jockey) #:use-module (crates-io))

(define-public crate-jockey-0.0.0 (c (n "jockey") (v "0.0.0") (h "17r9sasyp383rb0z2ag2ywkxaksq4kig5by36mi356yh0d19gjjq") (y #t)))

(define-public crate-jockey-0.1.0 (c (n "jockey") (v "0.1.0") (h "1v3xnazbhz59gdvkmfrdgmr68js3z164s6qhsd6h2avrzx20pmhh")))

(define-public crate-jockey-0.2.0 (c (n "jockey") (v "0.2.0") (h "11qc6g0kpkcn5vkffyfil1wf1z8axv1p0imzd7ws9a5bp7nyizfq")))

(define-public crate-jockey-0.2.1 (c (n "jockey") (v "0.2.1") (h "130a3awrxjxn8vfhrmy97d7c14gglil5fk1prn8xgfg80iyh8jm5")))

(define-public crate-jockey-0.3.0 (c (n "jockey") (v "0.3.0") (d (list (d (n "jockey_derive") (r "^0") (d #t) (k 2)))) (h "0ri7mbjscna1nqi3z5yny8m1cxx5sk533i23pwfr7n0k4sx0fb4f")))

