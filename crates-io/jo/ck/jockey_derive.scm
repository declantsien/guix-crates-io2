(define-module (crates-io jo ck jockey_derive) #:use-module (crates-io))

(define-public crate-jockey_derive-0.0.0 (c (n "jockey_derive") (v "0.0.0") (h "0206vs3y7v2bnhrn4m6ih0aylav9bn9dsfm0ks2ckvdym2nrsars") (y #t)))

(define-public crate-jockey_derive-0.1.0 (c (n "jockey_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (d #t) (k 0)))) (h "0kkm07ahpfdzndxnd4gm4kv1hr94i65nk4043bw1r066caxzjrc5")))

(define-public crate-jockey_derive-0.2.0 (c (n "jockey_derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (d #t) (k 0)))) (h "1zrcjgknpzm5sr6nkqksxgwsk85cc2jcc8amxxbfcjzjad3kpza1")))

(define-public crate-jockey_derive-0.2.1 (c (n "jockey_derive") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (d #t) (k 0)))) (h "1brbcmfbl7jmr8ycmci88q963hasz5a8bahp0dza9322z3p0vqmd")))

(define-public crate-jockey_derive-0.3.0 (c (n "jockey_derive") (v "0.3.0") (d (list (d (n "jockey") (r "^0") (d #t) (k 2)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (d #t) (k 0)))) (h "1z3fi0fczvfg28318ac25llg9lsk63cv4kqq854hy4n7hmvkk83d")))

