(define-module (crates-io jo ke joketeller) #:use-module (crates-io))

(define-public crate-joketeller-0.1.0 (c (n "joketeller") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.5.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "ureq") (r "^2.4.0") (f (quote ("json"))) (d #t) (k 0)))) (h "0i4rlgygsj6l056wa31iah8kcdi0wx4sggsldb578z16cr1kcf2f")))

(define-public crate-joketeller-0.2.0 (c (n "joketeller") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.5.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "ureq") (r "^2.4.0") (f (quote ("json"))) (d #t) (k 0)))) (h "1sfig5yibgchrmz5l7w42visj6x9bjam7djfq0d3mchgnfcpx5mv")))

(define-public crate-joketeller-0.2.1 (c (n "joketeller") (v "0.2.1") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.5.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "ureq") (r "^2.4.0") (f (quote ("json"))) (d #t) (k 0)))) (h "0cb4v0sbdfgxqr9v0acxgy8yk97pz2l0z707sb4dv34xkqmch6fb")))

