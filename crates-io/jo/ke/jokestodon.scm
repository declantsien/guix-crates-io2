(define-module (crates-io jo ke jokestodon) #:use-module (crates-io))

(define-public crate-jokestodon-0.1.0 (c (n "jokestodon") (v "0.1.0") (d (list (d (n "elefren") (r ">=0.22.0, <0.23.0") (f (quote ("toml"))) (d #t) (k 0)) (d (n "libjokes") (r ">=0.1.0, <0.2.0") (d #t) (k 0)))) (h "0048vwsylnzwj4r648ppfikyw7k76f20z0cf92mc2jv0rblscj4h")))

