(define-module (crates-io jo ke joke-generator) #:use-module (crates-io))

(define-public crate-joke-generator-0.1.0 (c (n "joke-generator") (v "0.1.0") (d (list (d (n "clap") (r "^4.3.19") (f (quote ("derive"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.104") (d #t) (k 0)) (d (n "tokio") (r "^1.29.1") (f (quote ("full"))) (d #t) (k 0)))) (h "17fgjwv0kgsq38vlvy0abaq1v2bj9cf04qd3514mkcbimc2f7hk5")))

(define-public crate-joke-generator-0.1.1 (c (n "joke-generator") (v "0.1.1") (d (list (d (n "clap") (r "^4.3.19") (f (quote ("derive"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.104") (d #t) (k 0)) (d (n "tokio") (r "^1.29.1") (f (quote ("full"))) (d #t) (k 0)))) (h "07v07xnfdlb76p3gg29s3ci8qg8anygmip8m6sgpksv0kxg3mhwh")))

