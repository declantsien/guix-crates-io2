(define-module (crates-io jo ke joker_query) #:use-module (crates-io))

(define-public crate-joker_query-0.1.0 (c (n "joker_query") (v "0.1.0") (d (list (d (n "dashmap") (r "^5.5.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "uuid") (r "^1.5.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "1dd151l4j7in8sr5ikv91fjqwm653ai213fl27gs0gf3kjj3iiyw")))

(define-public crate-joker_query-0.1.1 (c (n "joker_query") (v "0.1.1") (d (list (d (n "dashmap") (r "^5.5.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "uuid") (r "^1.5.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "1q3rmc5a43giwxizzv6bjxyyfawqlnhmwr5q7ph59vg39vhk2s65")))

(define-public crate-joker_query-1.0.0 (c (n "joker_query") (v "1.0.0") (h "0z8p6jcqyg42cgyy6hvfg3vymx6kw11ghl417nf3mdaqm6naryhs")))

