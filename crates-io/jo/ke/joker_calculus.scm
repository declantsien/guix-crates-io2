(define-module (crates-io jo ke joker_calculus) #:use-module (crates-io))

(define-public crate-joker_calculus-0.1.0 (c (n "joker_calculus") (v "0.1.0") (d (list (d (n "cuckoofilter") (r "^0.5.0") (d #t) (k 2)) (d (n "num-bigint") (r "^0.4.3") (d #t) (k 2)))) (h "01gfw0iirl2f8ngz76984b8p9cjf4vbl8hf9q0190n7q33vsvhsp")))

(define-public crate-joker_calculus-0.2.0 (c (n "joker_calculus") (v "0.2.0") (d (list (d (n "cuckoofilter") (r "^0.5.0") (d #t) (k 2)) (d (n "num-bigint") (r "^0.4.3") (d #t) (k 2)))) (h "1bb868fssl55by4yqazlvng22wxsqbyrndq70fkkg15y3nsc3dh9")))

(define-public crate-joker_calculus-0.3.0 (c (n "joker_calculus") (v "0.3.0") (d (list (d (n "cuckoofilter") (r "^0.5.0") (d #t) (k 2)) (d (n "num-bigint") (r "^0.4.3") (d #t) (k 2)))) (h "1k29f5l6svxvqq2108si9xqv35jdv9495kpmqiqqysh1ak2xpkf0")))

(define-public crate-joker_calculus-0.4.0 (c (n "joker_calculus") (v "0.4.0") (d (list (d (n "cuckoofilter") (r "^0.5.0") (d #t) (k 2)) (d (n "num-bigint") (r "^0.4.3") (d #t) (k 2)))) (h "1kak44nnf0rsv484yv9i0wd5dfm2iflm0dq48lfvy66l24yr2b40")))

(define-public crate-joker_calculus-0.5.0 (c (n "joker_calculus") (v "0.5.0") (d (list (d (n "cuckoofilter") (r "^0.5.0") (d #t) (k 2)) (d (n "num-bigint") (r "^0.4.3") (d #t) (k 2)))) (h "1n320hzz00kfxiqafgnlcvsnww52dm8k4q52ac6lvhrvmcz7q6m5")))

(define-public crate-joker_calculus-0.6.0 (c (n "joker_calculus") (v "0.6.0") (d (list (d (n "cuckoofilter") (r "^0.5.0") (d #t) (k 2)) (d (n "num-bigint") (r "^0.4.3") (d #t) (k 2)))) (h "1kca440b6iw535qsi1flymb7mfli0payzas3j6k5qbz5l8ad07jf")))

(define-public crate-joker_calculus-0.7.0 (c (n "joker_calculus") (v "0.7.0") (d (list (d (n "cuckoofilter") (r "^0.5.0") (d #t) (k 2)) (d (n "num-bigint") (r "^0.4.3") (d #t) (k 2)))) (h "0vfv1gd33wyah8f8ph39gzfrjapy0vkkmxyhqrmwg5dac8qsab9f")))

(define-public crate-joker_calculus-0.7.1 (c (n "joker_calculus") (v "0.7.1") (d (list (d (n "cuckoofilter") (r "^0.5.0") (d #t) (k 2)) (d (n "num-bigint") (r "^0.4.3") (d #t) (k 2)))) (h "175pg2hlza49kgv7cyvdfkfkykdgwayghydbk3mrh7x3bv5np2zl")))

(define-public crate-joker_calculus-0.8.0 (c (n "joker_calculus") (v "0.8.0") (d (list (d (n "cuckoofilter") (r "^0.5.0") (d #t) (k 2)) (d (n "num-bigint") (r "^0.4.3") (d #t) (k 2)))) (h "1niadfx7g8fkldd4gvc30lf0dj8n8zrg180cbbpwhjpjfzgydgj5")))

(define-public crate-joker_calculus-0.9.0 (c (n "joker_calculus") (v "0.9.0") (d (list (d (n "cuckoofilter") (r "^0.5.0") (d #t) (k 2)) (d (n "num-bigint") (r "^0.4.3") (d #t) (k 2)))) (h "1ckwnzsrhkbnwgjmrcgnyfbpx4mgr2l06jcvb5dfs2l6rvv13vbs")))

(define-public crate-joker_calculus-0.10.0 (c (n "joker_calculus") (v "0.10.0") (d (list (d (n "cuckoofilter") (r "^0.5.0") (d #t) (k 2)) (d (n "num-bigint") (r "^0.4.3") (d #t) (k 2)))) (h "1wz0dvmdkj3nsspnazlaj87fsjfgzgazyiwi06qzmpmr1z3aj77y")))

(define-public crate-joker_calculus-0.11.0 (c (n "joker_calculus") (v "0.11.0") (d (list (d (n "cuckoofilter") (r "^0.5.0") (d #t) (k 2)) (d (n "num-bigint") (r "^0.4.3") (d #t) (k 2)))) (h "194pv6z18xpi3smkbsxqdpid3201khr6b1wjv0jhkxlp7ykkx6am")))

