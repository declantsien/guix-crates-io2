(define-module (crates-io jo ke jokes) #:use-module (crates-io))

(define-public crate-jokes-0.1.0 (c (n "jokes") (v "0.1.0") (d (list (d (n "json") (r ">=0.12.4, <0.13.0") (d #t) (k 0)) (d (n "reqwest") (r ">=0.10.4, <0.11.0") (f (quote ("blocking" "json"))) (d #t) (k 0)))) (h "0w5i85hwk271h7i98n74328fh78i0z3v3gmbrgil4m6nnalnksha")))

