(define-module (crates-io jo ke joker) #:use-module (crates-io))

(define-public crate-joker-0.0.1 (c (n "joker") (v "0.0.1") (d (list (d (n "serde_json") (r "^0.5.1") (d #t) (k 0)) (d (n "unjson") (r "^0.0.1") (d #t) (k 0)))) (h "0rjkrxwva2b9r5z24id81dc4asbq9bsiln6gw08wm1wykcrgfd71")))

(define-public crate-joker-0.0.2 (c (n "joker") (v "0.0.2") (d (list (d (n "serde_json") (r "^0.5.1") (d #t) (k 0)) (d (n "unjson") (r "^0.0.1") (d #t) (k 0)))) (h "05i2jh3i2ng93ddg2p017a465i7g4nj4rkxxfligdy7j1m39ljhq")))

(define-public crate-joker-0.0.3 (c (n "joker") (v "0.0.3") (d (list (d (n "serde_json") (r "^0.5.1") (d #t) (k 0)) (d (n "unjson") (r "^0.0.3") (d #t) (k 0)))) (h "0b3sy9h9q2paxmx3dmydcrnkx9gm3qk51cg7paxbw9zxdjgq3svl")))

(define-public crate-joker-0.0.4 (c (n "joker") (v "0.0.4") (d (list (d (n "serde_json") (r "^0.7.1") (d #t) (k 0)) (d (n "unjson") (r "^0.0.4") (d #t) (k 0)))) (h "1hd8z5f85g62xr6cd7vl3qna140kk1qvz1p460adb6sgw94nd7kd")))

(define-public crate-joker-0.0.5 (c (n "joker") (v "0.0.5") (d (list (d (n "serde_json") (r "^0.7.1") (d #t) (k 0)) (d (n "unjson") (r "^0.0.5") (d #t) (k 0)))) (h "0j10qlxl2yj6jqw35g6awq56ans9dghkzn508ik4qmi6bmnx5ycd")))

