(define-module (crates-io jo ke jokeapi_rs) #:use-module (crates-io))

(define-public crate-jokeapi_rs-0.1.0 (c (n "jokeapi_rs") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.6.1") (f (quote ("full"))) (d #t) (k 0)))) (h "0h8cs3bmdrsq019hjn4z8i9kpbblylhk3kh1cvbsmhik227j2r7l")))

(define-public crate-jokeapi_rs-0.1.1 (c (n "jokeapi_rs") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.6.1") (f (quote ("full"))) (d #t) (k 0)))) (h "0f34zlf03yl0s86l4w1wkspxcmncvvwd40733mbcrxlc9h2fcxkp")))

(define-public crate-jokeapi_rs-0.1.2 (c (n "jokeapi_rs") (v "0.1.2") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.6.1") (f (quote ("full"))) (d #t) (k 0)))) (h "10avkxvb89b3xwr5rcbbx5a9cpx9g6dpksfdx7098b4psqi7swbf")))

(define-public crate-jokeapi_rs-0.1.3 (c (n "jokeapi_rs") (v "0.1.3") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.6.1") (f (quote ("full"))) (d #t) (k 0)))) (h "06wcs47xp2simwm6j1m0wd7b4vvk2r0c1dq2m30iw2svy7hhrxmx")))

(define-public crate-jokeapi_rs-0.1.4 (c (n "jokeapi_rs") (v "0.1.4") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.6.1") (f (quote ("full"))) (d #t) (k 0)))) (h "02d5jv7ygsyngy5gbjvwp6fz8s6b736gzab6y5c55cnf96ihxkhh")))

