(define-module (crates-io jo b_ job_abstrs) #:use-module (crates-io))

(define-public crate-job_abstrs-0.1.0 (c (n "job_abstrs") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.77") (d #t) (k 0)) (d (n "either") (r "^1.10.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1sxkf774nppff0c7w28whfs1rg3759pvsv9yrsijf4q89xf0va7p")))

