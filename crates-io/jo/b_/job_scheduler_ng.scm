(define-module (crates-io jo b_ job_scheduler_ng) #:use-module (crates-io))

(define-public crate-job_scheduler_ng-2.0.0 (c (n "job_scheduler_ng") (v "2.0.0") (d (list (d (n "chrono") (r "~0.4") (f (quote ("clock"))) (k 0)) (d (n "cron") (r "~0.11") (d #t) (k 0)) (d (n "tokio") (r ">=1.18") (f (quote ("macros" "time" "rt-multi-thread"))) (d #t) (k 2)) (d (n "uuid") (r "~1.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "0jnam52vwxsmpm92qibskd51x5r5czkqkrvhz4vzxrikvni3v4xw")))

(define-public crate-job_scheduler_ng-2.0.1 (c (n "job_scheduler_ng") (v "2.0.1") (d (list (d (n "chrono") (r "~0.4") (f (quote ("clock"))) (k 0)) (d (n "cron") (r "^0.11") (d #t) (k 0)) (d (n "tokio") (r ">=1.18") (f (quote ("macros" "time" "rt-multi-thread"))) (d #t) (k 2)) (d (n "uuid") (r "^1") (f (quote ("v4"))) (d #t) (k 0)))) (h "0kfamxwpg5zgb8drqn825sf50xwwdjixzzkvl2krahn40yy8nj3f")))

(define-public crate-job_scheduler_ng-2.0.2 (c (n "job_scheduler_ng") (v "2.0.2") (d (list (d (n "chrono") (r "~0.4.20") (f (quote ("clock"))) (k 0)) (d (n "cron") (r "^0.12.0") (d #t) (k 0)) (d (n "tokio") (r ">=1.21") (f (quote ("macros" "time" "rt-multi-thread"))) (d #t) (k 2)) (d (n "uuid") (r "^1") (f (quote ("v4"))) (d #t) (k 0)))) (h "0hpvzzrn87qa7g95vklp1myhgpc8k7rxf73y3bk6x1hhqwv30k45")))

(define-public crate-job_scheduler_ng-2.0.3 (c (n "job_scheduler_ng") (v "2.0.3") (d (list (d (n "chrono") (r "~0.4.20") (f (quote ("clock"))) (k 0)) (d (n "cron") (r "^0.12.0") (d #t) (k 0)) (d (n "tokio") (r ">=1.23") (f (quote ("macros" "time" "rt-multi-thread"))) (d #t) (k 2)) (d (n "uuid") (r "^1") (f (quote ("v4"))) (d #t) (k 0)))) (h "149qr8mjb26zvbfhsl70nydbzfq0smvndhw1s2i4kfnw421vn3l3")))

(define-public crate-job_scheduler_ng-2.0.4 (c (n "job_scheduler_ng") (v "2.0.4") (d (list (d (n "chrono") (r "~0.4.20") (f (quote ("clock"))) (k 0)) (d (n "cron") (r "^0.12.0") (d #t) (k 0)) (d (n "tokio") (r ">=1.25.0") (f (quote ("macros" "time" "rt-multi-thread"))) (d #t) (k 2)) (d (n "uuid") (r "^1") (f (quote ("v4"))) (d #t) (k 0)))) (h "1bnambdhbgx647hpa15kl1pwfrfjaxq8n8b68qzybfqkam2dzfqh") (r "1.56.1")))

(define-public crate-job_scheduler_ng-2.0.5 (c (n "job_scheduler_ng") (v "2.0.5") (d (list (d (n "chrono") (r "~0.4.34") (f (quote ("clock"))) (k 0)) (d (n "cron") (r "^0.12.0") (d #t) (k 0)) (d (n "tokio") (r ">=1.37") (f (quote ("macros" "time" "rt-multi-thread"))) (d #t) (k 2)) (d (n "uuid") (r "^1") (f (quote ("v4"))) (d #t) (k 0)))) (h "0siw182x6b8cka89d65a7v6hixpqryyrwxc7s2b2jgijgwh55hl7") (r "1.61.0")))

