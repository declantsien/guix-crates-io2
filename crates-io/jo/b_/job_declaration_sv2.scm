(define-module (crates-io jo b_ job_declaration_sv2) #:use-module (crates-io))

(define-public crate-job_declaration_sv2-0.1.0 (c (n "job_declaration_sv2") (v "0.1.0") (d (list (d (n "binary_sv2") (r "^0.1.6") (d #t) (k 0)) (d (n "const_sv2") (r "^0.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.89") (o #t) (k 0)))) (h "1mrzy5jqhmpsxk6x51nyqn9ffcm6vw5wd35mn2ql06gyga88hk94") (f (quote (("with_serde" "binary_sv2/with_serde" "serde"))))))

(define-public crate-job_declaration_sv2-1.0.0 (c (n "job_declaration_sv2") (v "1.0.0") (d (list (d (n "binary_sv2") (r "^1.0.0") (d #t) (k 0)) (d (n "const_sv2") (r "^1.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.89") (o #t) (k 0)))) (h "0mmhk5p0dysw77zhwagrjn62zh7ibg7pvbv4rdhsjna64pcrlqkc") (f (quote (("with_serde" "binary_sv2/with_serde" "serde"))))))

