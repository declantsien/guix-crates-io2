(define-module (crates-io jo yc joycon-driver) #:use-module (crates-io))

(define-public crate-joycon-driver-0.0.0 (c (n "joycon-driver") (v "0.0.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "enumflags2") (r "^0.7") (d #t) (k 0)) (d (n "hidapi") (r "^1.3") (k 0)) (d (n "nalgebra") (r "^0.29") (d #t) (k 0)) (d (n "nom") (r "^7.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0x5dg3b9321zdsb8149szb1x9yp6fb5z7jax79dvyh4rindlf6gr") (f (quote (("linux-static-hidraw" "hidapi/linux-static-hidraw") ("default" "hidapi/linux-static-hidraw")))) (y #t)))

