(define-module (crates-io jo at joat-path) #:use-module (crates-io))

(define-public crate-joat-path-0.0.6 (c (n "joat-path") (v "0.0.6") (h "00m23b7sqqd90sj6bb0wmbwbs4hs2ipxdmf4z5q8d0j276hyd0dv")))

(define-public crate-joat-path-0.0.7 (c (n "joat-path") (v "0.0.7") (h "0a88fak8mfq88bnxb3cvvp3xjkmq2x6zraipgwwi6x1r2gfjnymj")))

(define-public crate-joat-path-0.0.8 (c (n "joat-path") (v "0.0.8") (h "1lb8k9d7h9nmljps3m184y48n28z6hmqbbl3pzvyqjsb3d6and2d")))

(define-public crate-joat-path-0.0.9 (c (n "joat-path") (v "0.0.9") (h "1rvfdczpiph9d2l1ynz5j087mlkyz1dxjxsa6ww66gkabmldnh29")))

