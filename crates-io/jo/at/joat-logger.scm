(define-module (crates-io jo at joat-logger) #:use-module (crates-io))

(define-public crate-joat-logger-0.0.0 (c (n "joat-logger") (v "0.0.0") (d (list (d (n "indicatif") (r "^0.17.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (f (quote ("std"))) (d #t) (k 0)) (d (n "uuid") (r "^1.3.3") (f (quote ("v4"))) (d #t) (k 0)))) (h "0r4lb0wpbghpn6gzm8mbgg072ximk4xv5l35b6ir5qxars872d7g")))

(define-public crate-joat-logger-0.0.1 (c (n "joat-logger") (v "0.0.1") (d (list (d (n "indicatif") (r "^0.17.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.18") (f (quote ("std"))) (d #t) (k 0)) (d (n "uuid") (r "^1.3.3") (f (quote ("v4"))) (d #t) (k 0)))) (h "1727s96xmdka25zziagjhf2zyfn5z82jjcn4ck3dfmp6bnyksr0v")))

(define-public crate-joat-logger-0.0.2 (c (n "joat-logger") (v "0.0.2") (d (list (d (n "indicatif") (r "^0.17.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (f (quote ("std"))) (d #t) (k 0)) (d (n "uuid") (r "^1.4.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "02k5r9x9br2ismj0aqv7pkv0nq010qmqk1iyjjs2vk37l7l5kdqq")))

(define-public crate-joat-logger-0.0.3 (c (n "joat-logger") (v "0.0.3") (d (list (d (n "indicatif") (r "^0.17.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (f (quote ("std"))) (d #t) (k 0)) (d (n "uuid") (r "^1.4.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "1rl4br3l8fic6f6rxna0hnxxgpfsbxj9c1kp555cb1saj1wds9bv")))

(define-public crate-joat-logger-0.0.4 (c (n "joat-logger") (v "0.0.4") (d (list (d (n "indicatif") (r "^0.17.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (f (quote ("std"))) (d #t) (k 0)) (d (n "uuid") (r "^1.4.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "1qpgizm6kisdc2c50vwjgp1qw2001dw48p966y7ihgwngnwsasra")))

(define-public crate-joat-logger-0.0.5 (c (n "joat-logger") (v "0.0.5") (d (list (d (n "indicatif") (r "^0.17.8") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (f (quote ("std"))) (d #t) (k 0)) (d (n "uuid") (r "^1.8.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "1q9vcdsk1mg12fpk6lnv0b5iwqcxpi3zq6s4qrnx69nvxhi9gyrl")))

