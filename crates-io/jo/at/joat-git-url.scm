(define-module (crates-io jo at joat-git-url) #:use-module (crates-io))

(define-public crate-joat-git-url-0.0.1 (c (n "joat-git-url") (v "0.0.1") (h "1lcnkf73z1z0zz8ds6ghlv026xjssd9ijivlbvmlnis05s2wlsj5")))

(define-public crate-joat-git-url-0.0.2 (c (n "joat-git-url") (v "0.0.2") (h "1s7apykys56mgc4ynfsj263sazl66a1kvjn9ana8a3akrqlzmr83")))

(define-public crate-joat-git-url-0.0.3 (c (n "joat-git-url") (v "0.0.3") (h "1p5rggp5wvr9f1qqma8mvvfffc2c627qrk7b7jql16mgg30fhn06")))

(define-public crate-joat-git-url-0.0.4 (c (n "joat-git-url") (v "0.0.4") (h "0xc2r50db5l314x4j95jbywf0hwv75pmmzsf5fcb84i3nda4l7va")))

(define-public crate-joat-git-url-0.0.5 (c (n "joat-git-url") (v "0.0.5") (h "0mgc6iqrfll0rikx8kqwgdqh8z3f4b1hxd73k6f3qaf3dpbd1rf9")))

