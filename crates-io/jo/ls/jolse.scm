(define-module (crates-io jo ls jolse) #:use-module (crates-io))

(define-public crate-jolse-0.1.4 (c (n "jolse") (v "0.1.4") (d (list (d (n "zstd") (r "^0.13.1") (d #t) (k 0)))) (h "1pfn59799vgg5l5jp8rdv7s1s8437y6zhcdsd5a4547kw6f4w8sw") (y #t) (r "1.77.2")))

(define-public crate-jolse-0.1.0 (c (n "jolse") (v "0.1.0") (d (list (d (n "zstd") (r "^0.13.1") (d #t) (k 0)))) (h "0s24irbvpm6lj2bf3caq85427m4zd887dk8w422n19w91psjkpdx") (r "1.77.2")))

