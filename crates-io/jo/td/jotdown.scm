(define-module (crates-io jo td jotdown) #:use-module (crates-io))

(define-public crate-jotdown-0.1.0 (c (n "jotdown") (v "0.1.0") (h "10a2z3w0210vczdjvx0pglq64fk59fsz85ngj9xc1gll0g0dy73r") (f (quote (("suite_bench") ("suite") ("html") ("deterministic") ("default" "html"))))))

(define-public crate-jotdown-0.2.0 (c (n "jotdown") (v "0.2.0") (h "1647hvkqqrf8jmjrrny0962ff5q7l17n6kciscaxbinzczggfjvq") (f (quote (("suite_bench") ("suite") ("html") ("deterministic") ("default" "html"))))))

(define-public crate-jotdown-0.2.1 (c (n "jotdown") (v "0.2.1") (h "1wqnk4hn596kkrn1zk99zs1dngkz5iidb00ly11637c9pmhslcp2") (f (quote (("suite_bench") ("suite") ("html") ("deterministic") ("default" "html"))))))

(define-public crate-jotdown-0.3.0 (c (n "jotdown") (v "0.3.0") (h "09dfrmqlxrag4iy4lmrgzidxr83kdpb5r9rc27f655g6fs8f4cxy") (f (quote (("html") ("deterministic") ("default" "html"))))))

(define-public crate-jotdown-0.3.1 (c (n "jotdown") (v "0.3.1") (h "0qv0vxpdr92dl0zd820fwz3izk40x2dvm2bg4m2wx745hdqvhf2r") (f (quote (("html") ("deterministic") ("default" "html"))))))

(define-public crate-jotdown-0.3.2 (c (n "jotdown") (v "0.3.2") (h "15n928lkbfj06gayh49nswcnkxgkr9j33jc2398czlk54qgbihfk") (f (quote (("html") ("deterministic") ("default" "html"))))))

(define-public crate-jotdown-0.4.0 (c (n "jotdown") (v "0.4.0") (h "00zw3xmqzv56hry56l07xr70mwy3bqa2bbz4kbxrbmghjd3l2j6w") (f (quote (("html") ("deterministic") ("default" "html")))) (r "1.56")))

