(define-module (crates-io jo ne jones) #:use-module (crates-io))

(define-public crate-jones-0.1.0 (c (n "jones") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3.13") (d #t) (k 0)))) (h "00gl5by12wzj5gxz9lcms7sd6gfq171gp9c6hdps86zs3vgbp4wa")))

(define-public crate-jones-0.1.1 (c (n "jones") (v "0.1.1") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3.13") (d #t) (k 0)))) (h "0f1j6pic3q2s49rpi7kz6g1p3imijaj3jp7vldgdzc5rrkqsn4r9")))

(define-public crate-jones-0.1.3 (c (n "jones") (v "0.1.3") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3.13") (d #t) (k 0)))) (h "0crcsklwncywj8zfyi3g27szajnncmqsysz26b127scn5wb0bivf")))

(define-public crate-jones-0.1.4 (c (n "jones") (v "0.1.4") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3.13") (d #t) (k 0)))) (h "00bc1qbkbw9cl17v1q1r7j75gry1l22ph73hbgggnvpgiz7iqch4")))

(define-public crate-jones-0.2.0 (c (n "jones") (v "0.2.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)) (d (n "structopt") (r "^0.3.13") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "0ml2rg56l1pg7wivzcigrmalny3vi3b4x76v28m7yvzkcbqzhc57")))

