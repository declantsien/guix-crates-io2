(define-module (crates-io jo hn john-de-silencio) #:use-module (crates-io))

(define-public crate-john-de-silencio-0.1.0 (c (n "john-de-silencio") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 2)) (d (n "cucumber") (r "^0.20.2") (d #t) (k 2)) (d (n "fantoccini") (r "^0.19.3") (d #t) (k 2)) (d (n "leptos") (r "^0.6.9") (f (quote ("csr" "nightly"))) (d #t) (k 0)) (d (n "leptos_meta") (r "^0.6.9") (f (quote ("nightly" "csr"))) (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.115") (d #t) (k 2)) (d (n "tokio") (r "^1.37.0") (f (quote ("macros" "rt-multi-thread" "time"))) (d #t) (k 2)))) (h "081qpwdp2c4cai2ig1lckjlpjz5n6f1ls2wlc44yxwrw0zba8wrs") (r "1.56.1")))

