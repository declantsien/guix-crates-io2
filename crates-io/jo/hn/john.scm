(define-module (crates-io jo hn john) #:use-module (crates-io))

(define-public crate-john-0.1.0 (c (n "john") (v "0.1.0") (d (list (d (n "bpaf") (r "^0.9.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (d #t) (k 0)) (d (n "sitemap") (r "^0.4.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)) (d (n "tokio") (r "^1.29.1") (f (quote ("macros" "rt-multi-thread" "fs"))) (d #t) (k 0)) (d (n "url") (r "^2.4.0") (d #t) (k 0)))) (h "18rijka0dn2nab4wf5h8c690zkfjlr18v5avw1j4k42b0v6a22c5")))

(define-public crate-john-0.2.0 (c (n "john") (v "0.2.0") (d (list (d (n "bpaf") (r "^0.9.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (d #t) (k 0)) (d (n "sitemap") (r "^0.4.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)) (d (n "tokio") (r "^1.29.1") (f (quote ("macros" "rt-multi-thread" "fs"))) (d #t) (k 0)) (d (n "url") (r "^2.4.0") (d #t) (k 0)))) (h "1vk750skv5jhny5bmff1pqnfrlixarl85r232vrjs9zyjh19pywf")))

