(define-module (crates-io jo na jonases-tracing-util) #:use-module (crates-io))

(define-public crate-jonases-tracing-util-0.1.0 (c (n "jonases-tracing-util") (v "0.1.0") (d (list (d (n "actix-rt") (r "^1.1.1") (d #t) (k 0)) (d (n "actix-web") (r "^3.3.2") (d #t) (k 0)) (d (n "futures") (r "^0.3.13") (d #t) (k 0)) (d (n "tracing") (r "^0.1.25") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.2.16") (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("v4" "serde"))) (d #t) (k 0)))) (h "04v58rpdb1dhwvch0lyphlqmhalj6cmpywab6pig88mipix36pf8")))

(define-public crate-jonases-tracing-util-0.2.0 (c (n "jonases-tracing-util") (v "0.2.0") (d (list (d (n "actix-rt") (r "^1.1.1") (d #t) (k 0)) (d (n "actix-web") (r "^3.3.2") (d #t) (k 0)) (d (n "futures") (r "^0.3.13") (d #t) (k 0)) (d (n "tracing") (r "^0.1.25") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.2.16") (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("v4" "serde"))) (d #t) (k 0)))) (h "0ms65r1ac3718893n644qwnkcvwpzbgyx92m3a3hfllm1dywzzgx")))

