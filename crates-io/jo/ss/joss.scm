(define-module (crates-io jo ss joss) #:use-module (crates-io))

(define-public crate-joss-0.0.0 (c (n "joss") (v "0.0.0") (h "0nqz4s8azcyp177g2dvclq94iyv0nq4isqv6ijv8qw5v6shpws6j")))

(define-public crate-joss-0.0.1 (c (n "joss") (v "0.0.1") (d (list (d (n "cstring") (r "^0.0.2") (d #t) (k 0)))) (h "01i9zwl241qyaqimwhsgrf6vnz9hs0jas4phskw2mad4shq7pa4r")))

(define-public crate-joss-0.0.2 (c (n "joss") (v "0.0.2") (d (list (d (n "cstring") (r "^0.0.2") (d #t) (k 0)))) (h "11g937xhnzw23i814xzpal31rihipc8lbcl5j71kb2nj6x0y1j9p")))

