(define-module (crates-io jo tt jotter) #:use-module (crates-io))

(define-public crate-jotter-0.1.0 (c (n "jotter") (v "0.1.0") (d (list (d (n "pallete") (r "^1.1.0") (d #t) (k 0)) (d (n "rocket") (r "^0.5.0-rc.1") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.66") (d #t) (k 0)))) (h "196m9gddpg4i4196ksbm4v0r1baknbkcxvapkfigs1mzy0xcfgli")))

