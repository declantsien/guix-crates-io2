(define-module (crates-io jo es joestar-html) #:use-module (crates-io))

(define-public crate-joestar-html-0.1.0 (c (n "joestar-html") (v "0.1.0") (d (list (d (n "joestar") (r "^0.1.0") (d #t) (k 0)))) (h "0h5mxbhy7f5pdz5k2izzw1dbsb0dlr1x8gcbh1q3xpwaqi51cq60") (y #t)))

(define-public crate-joestar-html-0.1.1 (c (n "joestar-html") (v "0.1.1") (d (list (d (n "joestar") (r "^0.1.1") (d #t) (k 0)))) (h "1p7dbg1yg2vyqx6pfac52pbd5avgxcihn4rpm0vl1msblzjpqy7d") (y #t)))

(define-public crate-joestar-html-0.1.2 (c (n "joestar-html") (v "0.1.2") (d (list (d (n "joestar") (r "^0.1.2") (d #t) (k 0)))) (h "1sqhbiziizj8pl8f6jj2zya1p586gr184y2sy7lb965rlvg957g8") (y #t)))

(define-public crate-joestar-html-0.1.3 (c (n "joestar-html") (v "0.1.3") (d (list (d (n "joestar") (r "^0.1.2") (d #t) (k 0)))) (h "0svk075mnyravbn5bghhg1j9v8j0cr40sb2jny6wylznyp3skvxd") (y #t)))

