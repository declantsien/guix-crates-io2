(define-module (crates-io jo es joestar) #:use-module (crates-io))

(define-public crate-joestar-0.1.0 (c (n "joestar") (v "0.1.0") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "wry") (r "^0.27.0") (d #t) (k 0)))) (h "04s8s90nalb3g3m215xswl2a78nhzv8qidhz2g42bqh93vdyqp77") (y #t)))

(define-public crate-joestar-0.1.1 (c (n "joestar") (v "0.1.1") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "wry") (r "^0.27.0") (d #t) (k 0)))) (h "11a68nfpq2scp7f9bj4zpiqsicq91zw0ljpwj60h9x5laxmy524c") (y #t)))

(define-public crate-joestar-0.1.2 (c (n "joestar") (v "0.1.2") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "wry") (r "^0.27.0") (d #t) (k 0)))) (h "0g0i820h65pr1krzh4rpmlyiz8ia0ikr4l9xlv41byif5l1g7921") (y #t)))

