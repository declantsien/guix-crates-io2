(define-module (crates-io jo yd joydev-sys) #:use-module (crates-io))

(define-public crate-joydev-sys-0.1.0 (c (n "joydev-sys") (v "0.1.0") (d (list (d (n "input-event-codes") (r "^0.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.60") (d #t) (k 0)) (d (n "nix") (r "^0.15.0") (d #t) (k 0)))) (h "1v1cjvca0xp8m80nq9acd16qxnzr8n6ghsckbas4qsr4wi0h663n")))

(define-public crate-joydev-sys-0.1.1 (c (n "joydev-sys") (v "0.1.1") (d (list (d (n "input-event-codes") (r "^0.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.60") (d #t) (k 0)) (d (n "nix") (r "^0.15.0") (d #t) (k 0)))) (h "0gapr3xgqnyk7hjjswsvf25dg4ci23s7sqx60722sgbq8mgza8vg") (y #t)))

(define-public crate-joydev-sys-0.1.2 (c (n "joydev-sys") (v "0.1.2") (d (list (d (n "input-event-codes") (r "^0.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.60") (d #t) (k 0)) (d (n "nix") (r "^0.15.0") (d #t) (k 0)))) (h "1rxbmyqcfz9sr3cfx64i4aiwcd4s3i8w3xyqh23lxwq1mgx8snlr")))

(define-public crate-joydev-sys-0.1.3 (c (n "joydev-sys") (v "0.1.3") (d (list (d (n "input-event-codes") (r "^0.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.60") (d #t) (k 0)) (d (n "nix") (r "^0.15.0") (d #t) (k 0)))) (h "1qxf7p5rmavvs60s4sny823gw85cj69m9ihl2i3mfb4zd0ix306f")))

(define-public crate-joydev-sys-0.2.0 (c (n "joydev-sys") (v "0.2.0") (d (list (d (n "input-event-codes") (r "^0.2.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.66") (k 0)) (d (n "nix") (r "^0.17.0") (d #t) (k 0)))) (h "1z2f76k2j4i7gy90sbmpydjrbpbjj1d83z964655r17amvw4fwc9") (f (quote (("extra_traits" "libc/extra_traits") ("default"))))))

(define-public crate-joydev-sys-0.2.1 (c (n "joydev-sys") (v "0.2.1") (d (list (d (n "input-event-codes") (r "^0.2.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.66") (k 0)) (d (n "nix") (r "^0.17.0") (d #t) (k 0)))) (h "0miimp2hhlx0m9201jv4j2i9z7rp2gi8jl9kn03a41k2mbcwnvvy") (f (quote (("extra_traits" "libc/extra_traits") ("default"))))))

