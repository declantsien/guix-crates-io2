(define-module (crates-io jo yd joydev-rs) #:use-module (crates-io))

(define-public crate-joydev-rs-0.1.0 (c (n "joydev-rs") (v "0.1.0") (d (list (d (n "arrayref") (r "^0.3.5") (d #t) (k 0)) (d (n "ctrlc") (r "^3") (d #t) (k 2)) (d (n "input-event-codes") (r "^0.1.0") (d #t) (k 0)) (d (n "joydev-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.60") (d #t) (k 0)) (d (n "nix") (r "^0.15.0") (d #t) (k 0)))) (h "18kbqcfw5fl5lfgi35x0fbmqygisfibj4kjb0k6mfs2h4bsngc5n") (y #t)))

(define-public crate-joydev-rs-0.1.1 (c (n "joydev-rs") (v "0.1.1") (d (list (d (n "arrayref") (r "^0.3.5") (d #t) (k 0)) (d (n "ctrlc") (r "^3") (d #t) (k 2)) (d (n "input-event-codes") (r "^0.1.0") (d #t) (k 0)) (d (n "joydev-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.60") (d #t) (k 0)) (d (n "nix") (r "^0.15.0") (d #t) (k 0)))) (h "00fjqkm6xkqdhf8a8p817fj81dcv144sm9j37j5ay09fqchssy3k") (y #t)))

(define-public crate-joydev-rs-0.2.0 (c (n "joydev-rs") (v "0.2.0") (d (list (d (n "arrayref") (r "^0.3.5") (d #t) (k 0)) (d (n "ctrlc") (r "^3") (d #t) (k 2)) (d (n "input-event-codes") (r "^0.1.0") (d #t) (k 0)) (d (n "joydev-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.60") (d #t) (k 0)) (d (n "nix") (r "^0.15.0") (d #t) (k 0)))) (h "0xfi73q5jr5v8h05ikwngilp4cxrbh9w1bz956haaiz1nxfr93nw") (y #t)))

