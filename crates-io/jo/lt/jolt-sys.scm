(define-module (crates-io jo lt jolt-sys) #:use-module (crates-io))

(define-public crate-jolt-sys-0.1.0 (c (n "jolt-sys") (v "0.1.0") (h "1p60y5ch4b2mmymdyi46zv8pk6p7qpscd2xhh9hzlbjp68lx5vgx") (y #t)))

(define-public crate-jolt-sys-0.1.2 (c (n "jolt-sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)) (d (n "cmake") (r "^0.1.48") (d #t) (k 1)) (d (n "normpath") (r "^0.3") (d #t) (k 1)))) (h "18mdk1pqw7gdq1b96mm3d7ipfhmfk4yrz6f2n8r2xiwwvbgqbd9n") (f (quote (("default")))) (y #t) (l "jolt-wrapper")))

(define-public crate-jolt-sys-0.1.5 (c (n "jolt-sys") (v "0.1.5") (d (list (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)) (d (n "cmake") (r "^0.1.48") (d #t) (k 1)) (d (n "normpath") (r "^0.3") (d #t) (k 1)))) (h "0z2bp1b8x9467zk5kxkfnvv8sbwq5q4ikh07d06sdl5j7g2pzasz") (f (quote (("default")))) (l "jolt-wrapper")))

