(define-module (crates-io jo lt jolt-physics) #:use-module (crates-io))

(define-public crate-jolt-physics-0.1.0 (c (n "jolt-physics") (v "0.1.0") (d (list (d (n "jolt-sys") (r "^0.1.0") (d #t) (k 0)))) (h "0bl59x5rndf3r331dmppy78c2smxrr3r3smxbr1v8id9yg8b8i66") (y #t)))

(define-public crate-jolt-physics-0.1.1 (c (n "jolt-physics") (v "0.1.1") (d (list (d (n "jolt-sys") (r "^0.1.0") (d #t) (k 0)))) (h "1569jwwq6wa8hr1ys4bindy1d8hr0m3qxqsv3gmggm95wc3d4ny8") (y #t)))

(define-public crate-jolt-physics-0.1.2 (c (n "jolt-physics") (v "0.1.2") (d (list (d (n "jolt-sys") (r "^0.1.0") (d #t) (k 0)))) (h "0klsdnrmar5c8p9z4xfcnaca3kkv3cnckpzkr00w0my4y43ldw45") (y #t)))

(define-public crate-jolt-physics-0.1.3 (c (n "jolt-physics") (v "0.1.3") (d (list (d (n "jolt-sys") (r "^0.1.2") (d #t) (k 0)))) (h "04gi64kswyfx291x3djcdm3kqgla1z6yvql5468zd61n2mp8r069") (y #t)))

(define-public crate-jolt-physics-0.1.5 (c (n "jolt-physics") (v "0.1.5") (d (list (d (n "jolt-sys") (r "^0.1.5") (d #t) (k 0)))) (h "0gr8cdlm8dcng5nx1fannpqswrdlmiar8fhs2jlf3clcjwz8h86r")))

(define-public crate-jolt-physics-0.1.4 (c (n "jolt-physics") (v "0.1.4") (d (list (d (n "jolt-sys") (r "^0.1.4") (d #t) (k 0)))) (h "1yrk55x9sszwqh7y66k7q96fpfh8gvv2g4y555ss5h456hiblq49")))

