(define-module (crates-io jo bb jobber3) #:use-module (crates-io))

(define-public crate-jobber3-0.1.0 (c (n "jobber3") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "clap") (r "^4.1.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)))) (h "15f15c29plyfay2syrds252gy5m1jjdn786l3xa99z80r2cqwd0d") (y #t)))

