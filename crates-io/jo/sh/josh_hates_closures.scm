(define-module (crates-io jo sh josh_hates_closures) #:use-module (crates-io))

(define-public crate-josh_hates_closures-0.1.0 (c (n "josh_hates_closures") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.17") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0fpq608crhb1hdsmpz14mdjh5nm326ndznn2wnl3dp6ngbv3l1f6")))

(define-public crate-josh_hates_closures-0.1.1 (c (n "josh_hates_closures") (v "0.1.1") (d (list (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.17") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "152hk4dr9q9jpwzkb01jchlm5hk2676asliknh5snrr9clm5fwh0")))

