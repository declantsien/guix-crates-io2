(define-module (crates-io jo bi jobicolet) #:use-module (crates-io))

(define-public crate-jobicolet-0.1.0 (c (n "jobicolet") (v "0.1.0") (d (list (d (n "wee_alloc") (r "^0.4.5") (d #t) (k 0)))) (h "1d31j2hrc50pq0h8zn6zrpmmp6fxf0jnvzss8ra74yrjlk69p8i2")))

(define-public crate-jobicolet-0.1.1 (c (n "jobicolet") (v "0.1.1") (d (list (d (n "wee_alloc") (r "^0.4.5") (d #t) (k 0)))) (h "1jxapda2m38jzxnybyiiahvhxdx32cgz78hawiraqlq4a2bkw82w")))

(define-public crate-jobicolet-0.1.2 (c (n "jobicolet") (v "0.1.2") (d (list (d (n "wee_alloc") (r "^0.4.5") (d #t) (k 0)))) (h "106p02ybxjsky1qf7dprvfdikfckml9mrzqr5lw5g8j7ywiwfqi9")))

