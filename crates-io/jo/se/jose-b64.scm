(define-module (crates-io jo se jose-b64) #:use-module (crates-io))

(define-public crate-jose-b64-0.0.0 (c (n "jose-b64") (v "0.0.0") (h "14jcn5q8jirjh2iidjjc5lllmvmqb72mfnkbp2blz2z8p2a1x0f5")))

(define-public crate-jose-b64-0.1.0 (c (n "jose-b64") (v "0.1.0") (d (list (d (n "base64ct") (r "^1.6.0") (f (quote ("alloc"))) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("alloc" "derive"))) (o #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (f (quote ("alloc"))) (o #t) (k 0)) (d (n "subtle") (r "^2.5.0") (o #t) (k 0)) (d (n "zeroize") (r "^1.6.0") (f (quote ("alloc" "serde"))) (o #t) (k 0)))) (h "0l1i9wfa3bsghh5f1z4a7y14il2q3gsgdng9yf1gchyf36jfsy6n") (s 2) (e (quote (("serde" "dep:serde") ("secret" "serde" "dep:zeroize" "dep:subtle") ("json" "serde" "dep:serde_json")))) (r "1.65")))

(define-public crate-jose-b64-0.1.1 (c (n "jose-b64") (v "0.1.1") (d (list (d (n "base64ct") (r "^1.6.0") (f (quote ("alloc"))) (k 0)) (d (n "serde") (r "^1.0.160, <1.0.172") (f (quote ("alloc" "derive"))) (o #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (f (quote ("alloc"))) (o #t) (k 0)) (d (n "subtle") (r "^2.5.0") (o #t) (k 0)) (d (n "zeroize") (r "^1.6.0") (f (quote ("alloc" "serde"))) (o #t) (k 0)))) (h "0fxabgs5a1xqlzdr2ygq81f63vvvzcpal8i3r1q9c0zggx9xhw6c") (s 2) (e (quote (("serde" "dep:serde") ("secret" "serde" "dep:zeroize" "dep:subtle") ("json" "serde" "dep:serde_json")))) (r "1.65")))

(define-public crate-jose-b64-0.1.2 (c (n "jose-b64") (v "0.1.2") (d (list (d (n "base64ct") (r "^1.6.0") (f (quote ("alloc"))) (k 0)) (d (n "serde") (r "^1.0.185") (f (quote ("alloc" "derive"))) (o #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (f (quote ("alloc"))) (o #t) (k 0)) (d (n "subtle") (r "^2.5.0") (o #t) (k 0)) (d (n "zeroize") (r "^1.6.0") (f (quote ("alloc" "serde"))) (o #t) (k 0)))) (h "0mhvh1b9cz6i0y3iypv0nwnjv7slcz76asf745mnc2c76rsr7imy") (s 2) (e (quote (("serde" "dep:serde") ("secret" "serde" "dep:zeroize" "dep:subtle") ("json" "serde" "dep:serde_json")))) (r "1.65")))

