(define-module (crates-io jo se jose-jws) #:use-module (crates-io))

(define-public crate-jose-jws-0.0.0 (c (n "jose-jws") (v "0.0.0") (h "0csk86jd5dq7y0vfcqin11q26h0gjkq8sag449vsql1b8ckfy93g")))

(define-public crate-jose-jws-0.1.0 (c (n "jose-jws") (v "0.1.0") (d (list (d (n "jose-b64") (r "^0.1") (f (quote ("json"))) (k 0)) (d (n "jose-jwa") (r "^0.1") (d #t) (k 0)) (d (n "jose-jwk") (r "^0.1") (k 0)) (d (n "rand_core") (r "^0.6.4") (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("alloc" "derive"))) (k 0)) (d (n "serde_json") (r "^1.0.96") (k 0)))) (h "19056jv00yf399hzbbjqhk05p9dv07fvf4akq0s6i7cbkmwi6y8z") (r "1.65")))

(define-public crate-jose-jws-0.1.1 (c (n "jose-jws") (v "0.1.1") (d (list (d (n "jose-b64") (r "^0.1") (f (quote ("json"))) (k 0)) (d (n "jose-jwa") (r "^0.1") (d #t) (k 0)) (d (n "jose-jwk") (r "^0.1") (k 0)) (d (n "rand_core") (r "^0.6.4") (k 0)) (d (n "serde") (r "^1.0.160, <1.0.172") (f (quote ("alloc" "derive"))) (k 0)) (d (n "serde_json") (r "^1.0.96") (k 0)))) (h "0c23ypf01xhxb5558mfs67vm37sja2g94ri652xnf724bv2yf9m0") (r "1.65")))

(define-public crate-jose-jws-0.1.2 (c (n "jose-jws") (v "0.1.2") (d (list (d (n "jose-b64") (r "^0.1") (f (quote ("json"))) (k 0)) (d (n "jose-jwa") (r "^0.1") (d #t) (k 0)) (d (n "jose-jwk") (r "^0.1") (k 0)) (d (n "rand_core") (r "^0.6.4") (k 0)) (d (n "serde") (r "^1.0.185") (f (quote ("alloc" "derive"))) (k 0)) (d (n "serde_json") (r "^1.0.96") (k 0)))) (h "09hwgi0jg8dc1w0x71h0847sqyjwvrd1ay73l8d82fjm9zgljp8v") (r "1.65")))

