(define-module (crates-io jo se josephine_derive) #:use-module (crates-io))

(define-public crate-josephine_derive-0.1.0 (c (n "josephine_derive") (v "0.1.0") (d (list (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)) (d (n "synstructure") (r "^0.5") (d #t) (k 0)))) (h "0zfdy71npaini27n8j1g32mnr167aya9i1r0g8k1xzv39n83jmd9")))

