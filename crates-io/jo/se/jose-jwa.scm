(define-module (crates-io jo se jose-jwa) #:use-module (crates-io))

(define-public crate-jose-jwa-0.0.0 (c (n "jose-jwa") (v "0.0.0") (h "0cxspqcsg1rs9qzxfinq115s8glnl7qzghq1ifarm5vn4qrd92am")))

(define-public crate-jose-jwa-0.1.0 (c (n "jose-jwa") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.160") (f (quote ("alloc" "derive"))) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 2)))) (h "08vsy7by84yi0gw96ph0j7k8k5rc5p9djxhp9j1jzm13d5g382ka") (r "1.65")))

(define-public crate-jose-jwa-0.1.1 (c (n "jose-jwa") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.160, <1.0.172") (f (quote ("alloc" "derive"))) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 2)))) (h "1nkcq8spbb2k9phdv315s7cd79bwa27b888fbnwpr6ah2zxs1w9y") (r "1.65")))

(define-public crate-jose-jwa-0.1.2 (c (n "jose-jwa") (v "0.1.2") (d (list (d (n "serde") (r "^1.0.185") (f (quote ("alloc" "derive"))) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 2)))) (h "19xklicfgmskv0sfn1i9p7fd1y80q2ad3w3wsr8s71p87w2qxdws") (r "1.65")))

