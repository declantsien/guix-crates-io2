(define-module (crates-io jo se josephine) #:use-module (crates-io))

(define-public crate-josephine-0.1.1 (c (n "josephine") (v "0.1.1") (d (list (d (n "env_logger") (r "^0.4") (d #t) (k 2)) (d (n "josephine_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.30") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "mozjs") (r "^0.1.7") (d #t) (k 0)) (d (n "skeptic") (r "^0.13") (d #t) (k 1)) (d (n "skeptic") (r "^0.13") (d #t) (k 2)))) (h "0wxw99d5rggjsqnv0p0lxfjqy4i95lsmc9ysfdswwi40686a02ij")))

(define-public crate-josephine-0.2.0 (c (n "josephine") (v "0.2.0") (d (list (d (n "env_logger") (r "^0.4") (d #t) (k 2)) (d (n "josephine_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.30") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "mozjs") (r "< 0.3") (d #t) (k 0)))) (h "0an7656f8ah7zsbwl4phlqk9wyhfnfva5rdhzs6nzg1hg9mb0a86") (f (quote (("smup") ("debugmozjs" "mozjs/debugmozjs"))))))

