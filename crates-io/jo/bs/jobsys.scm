(define-module (crates-io jo bs jobsys) #:use-module (crates-io))

(define-public crate-jobsys-0.1.0 (c (n "jobsys") (v "0.1.0") (h "1rkzldsv7pnvzl0hb74sjg9kacyv69f9qr3zwynbnand6ndvr24r") (f (quote (("job_storage_64") ("job_storage_128") ("default" "job_storage_64") ("debug_queue")))) (y #t)))

(define-public crate-jobsys-0.1.1 (c (n "jobsys") (v "0.1.1") (h "0cy9h05d4qbk61n9p1y9v30gwx76l4n1695ys75q1hwwl8jwjkrg") (f (quote (("job_storage_64") ("job_storage_128") ("default" "job_storage_64") ("debug_queue")))) (y #t)))

(define-public crate-jobsys-0.2.0 (c (n "jobsys") (v "0.2.0") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "0brwnqi88gssn70hdh2lvvcvrx6hm8z3mvdj011084sabah8mpxj") (f (quote (("job_storage_64") ("job_storage_128") ("default" "job_storage_64") ("debug_queue"))))))

