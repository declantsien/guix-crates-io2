(define-module (crates-io jo bs jobsteal) #:use-module (crates-io))

(define-public crate-jobsteal-0.1.0 (c (n "jobsteal") (v "0.1.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "00lsxcbi1vj3020axlvzg7ih1jf42pqv6bbv07ci3x544z2nx74z") (y #t)))

(define-public crate-jobsteal-0.2.0 (c (n "jobsteal") (v "0.2.0") (d (list (d (n "crossbeam") (r "^0.1.6") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0nb340b936dl1dnbb61bl3ngfdn61b2ylfnfsq7xdg7cpf728ysi") (f (quote (("nightly"))))))

(define-public crate-jobsteal-0.2.1 (c (n "jobsteal") (v "0.2.1") (h "1x8rv1r0kcnafs9va2rz1jz2sc14yma7m8ihj7a93579bmbvn3la") (f (quote (("nightly"))))))

(define-public crate-jobsteal-0.2.2 (c (n "jobsteal") (v "0.2.2") (h "1gjd7058d65xbankiv8vxskyid8rlvr4zh32xpxrr3jp6k0xbpxc") (f (quote (("nightly"))))))

(define-public crate-jobsteal-0.2.3 (c (n "jobsteal") (v "0.2.3") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1ysx1l1fvbi76xbnjh1r2ci787h3i41jwa10cv0ag1d6qc6mksgk") (f (quote (("nightly"))))))

(define-public crate-jobsteal-0.2.4 (c (n "jobsteal") (v "0.2.4") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1bwp326vq2z9sfmgbl0hzkdn33yzb1qbdi8clrlndppkdz1qfg7q") (f (quote (("nightly"))))))

(define-public crate-jobsteal-0.3.1 (c (n "jobsteal") (v "0.3.1") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0qbmmh0p28l32bvwhwrr2lwz6j4y6nyzbcm92l98ivpipi1jqx6x") (f (quote (("nightly")))) (y #t)))

(define-public crate-jobsteal-0.4.0 (c (n "jobsteal") (v "0.4.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0mpvvrvv2qbjlaj0j0mqkb2wsxdjflh5jazz135ra2p3jbd15arx") (f (quote (("nightly"))))))

(define-public crate-jobsteal-0.4.1 (c (n "jobsteal") (v "0.4.1") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1vk4cvh98rkb5a8g74w6mdsz4va7ivm0pxx7x1zqyrkqn76qnq3y") (f (quote (("nightly"))))))

(define-public crate-jobsteal-0.4.2 (c (n "jobsteal") (v "0.4.2") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1fwl46i06myk0h3rcbfhxi8cixxkd7r2a97xr0ih7zkqdc9cqvpc") (f (quote (("nightly"))))))

(define-public crate-jobsteal-0.4.3 (c (n "jobsteal") (v "0.4.3") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1g2nffb5yz9klav92n9jqsjpalwiilr28lk0qyg201rhlkr8zy00") (f (quote (("nightly"))))))

(define-public crate-jobsteal-0.5.0 (c (n "jobsteal") (v "0.5.0") (d (list (d (n "crossbeam") (r "^0.2.9") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1nmcchbf4lds33gznk027iplarzdyppy3py6w6s2qhfc6x4sh90z") (f (quote (("nightly"))))))

(define-public crate-jobsteal-0.5.1 (c (n "jobsteal") (v "0.5.1") (d (list (d (n "crossbeam") (r "^0.2.9") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1zyy4kxq5ypykwmqw70f3xd9sa4ciaiysrf3aswz380d66bp40yh") (f (quote (("nightly"))))))

