(define-module (crates-io jo b- job-pool) #:use-module (crates-io))

(define-public crate-job-pool-0.1.0 (c (n "job-pool") (v "0.1.0") (h "0d2hvdavq5ywqbs7pw4qng4w15kzkaw5k9dx25q09ssb1b2m7vsb")))

(define-public crate-job-pool-0.1.1 (c (n "job-pool") (v "0.1.1") (h "1dbfvi1rmbhfgql6mj3rwv4ggqjwpklsrvgrk35pz3gflsk7y7np")))

(define-public crate-job-pool-0.1.2 (c (n "job-pool") (v "0.1.2") (h "0vkphf2bk7ijybs4lp4pb8f2v9cs4ipk621lqm7xyqaxhqgld6bz")))

(define-public crate-job-pool-0.1.3 (c (n "job-pool") (v "0.1.3") (h "1w0asyv02ba2x51sihszqz1gn4qhvrw76lk486r0hzmdax0liq3l")))

