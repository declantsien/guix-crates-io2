(define-module (crates-io jo b- job-dispatcher) #:use-module (crates-io))

(define-public crate-job-dispatcher-0.1.0 (c (n "job-dispatcher") (v "0.1.0") (d (list (d (n "futures") (r "^0.3.24") (d #t) (k 0)) (d (n "tokio") (r "^1.21.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0nw2zjdiwld8zrjpyd75l3wcvrjmxxl6rpzvfxryxxhjrik4jq82")))

(define-public crate-job-dispatcher-0.2.0 (c (n "job-dispatcher") (v "0.2.0") (d (list (d (n "futures") (r "^0.3.24") (d #t) (k 0)) (d (n "tokio") (r "^1.21.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1byya2p2ganavm46vgv8xz9g109q29scwvnsrjnnh5qgry75hfyj")))

(define-public crate-job-dispatcher-0.3.0 (c (n "job-dispatcher") (v "0.3.0") (d (list (d (n "tokio") (r "^1.21.0") (f (quote ("process"))) (d #t) (k 0)))) (h "1xr1kfsw6jiklvqhpbr82w5623i4chjvlbwxmlrnw8fbiwylc17b")))

(define-public crate-job-dispatcher-0.4.0 (c (n "job-dispatcher") (v "0.4.0") (d (list (d (n "tokio") (r "^1.21.0") (f (quote ("process"))) (d #t) (k 0)))) (h "1043x4bw9x4xv7cjzpnwc8bcs83kd71v1pd4pw007x7vcy9mplpi")))

(define-public crate-job-dispatcher-0.4.1 (c (n "job-dispatcher") (v "0.4.1") (d (list (d (n "tokio") (r "^1.21.0") (f (quote ("process"))) (d #t) (k 0)))) (h "0wjw8r3kjva6x9vj1aah40v54va41svz3bnq2bb46f90pkn1y9cq")))

