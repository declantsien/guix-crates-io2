(define-module (crates-io jo b- job-security-protocol) #:use-module (crates-io))

(define-public crate-job-security-protocol-0.1.0 (c (n "job-security-protocol") (v "0.1.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "bytes") (r "^1.4.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "tokio-util") (r "^0.7.7") (f (quote ("codec"))) (d #t) (k 0)))) (h "0pgbjx5zqrxwhin5zl2gvc4lgb0q7w6rnyyp9q538ws1wwhr89gh")))

(define-public crate-job-security-protocol-0.1.1 (c (n "job-security-protocol") (v "0.1.1") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "bytes") (r "^1.4.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "tokio-util") (r "^0.7.7") (f (quote ("codec"))) (d #t) (k 0)))) (h "0p2aa6nk3n8z99ycpyqdif22hwzv5pp29wz331wj9grk3n8a8rbh")))

