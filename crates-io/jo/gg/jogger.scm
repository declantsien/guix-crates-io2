(define-module (crates-io jo gg jogger) #:use-module (crates-io))

(define-public crate-jogger-0.1.0 (c (n "jogger") (v "0.1.0") (d (list (d (n "cursive") (r "^0.20.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.14") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "time") (r "^0.3.20") (f (quote ("macros"))) (d #t) (k 0)))) (h "0ssm9pyf2k4mpwaf9qbhg909a30nz3slz7r2v8srmfs0k6jv5xpw")))

(define-public crate-jogger-0.1.1 (c (n "jogger") (v "0.1.1") (d (list (d (n "cursive") (r "^0.20.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.14") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "time") (r "^0.3.20") (f (quote ("macros"))) (d #t) (k 0)))) (h "1ahzqwzdz9a0blq5r35r2789y62ic8bj792a8i8a2wj2dmj2ibm5")))

(define-public crate-jogger-0.1.2 (c (n "jogger") (v "0.1.2") (d (list (d (n "cursive") (r "^0.20.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.14") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "time") (r "^0.3.20") (f (quote ("macros"))) (d #t) (k 0)))) (h "0rrq313l09cd6aswqqz4nh258ygzrrb55y1a5k28vnyfmsz51lr5") (y #t)))

(define-public crate-jogger-0.1.3 (c (n "jogger") (v "0.1.3") (d (list (d (n "cursive") (r "^0.20.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.14") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "time") (r "^0.3.20") (f (quote ("macros"))) (d #t) (k 0)))) (h "1mf05akwgw2kkqldj24x45bln8lr3cvlh3d7hjd972y2am79py58")))

(define-public crate-jogger-0.1.4 (c (n "jogger") (v "0.1.4") (d (list (d (n "cursive") (r "^0.20.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.14") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "time") (r "^0.3.20") (f (quote ("macros"))) (d #t) (k 0)))) (h "19ij5n1ald6sbfmlkhxzm87igpgmxx6lq9j6kdqjhmawbnv7lchd")))

(define-public crate-jogger-0.2.0 (c (n "jogger") (v "0.2.0") (d (list (d (n "b64-rs") (r "^1.0.3") (d #t) (k 0)) (d (n "cursive") (r "^0.20.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.14") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.99") (d #t) (k 0)) (d (n "time") (r "^0.3.20") (f (quote ("macros"))) (d #t) (k 0)))) (h "0dqqp3pscz0m7aqj873mhkd8rdkaf41azhavvcmhz8wd53m9xrni")))

