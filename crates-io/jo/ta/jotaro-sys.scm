(define-module (crates-io jo ta jotaro-sys) #:use-module (crates-io))

(define-public crate-jotaro-sys-0.1.0 (c (n "jotaro-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0gsw8dpgnvdiy22qr428qw7k2qa6rjd6b4f3v0mjda0w86wspi1q")))

(define-public crate-jotaro-sys-0.1.1 (c (n "jotaro-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0xfiqzp5m9cbspc49f7kijx8bhiw4l93x5mkxzlbm2njl7r9zcpw")))

(define-public crate-jotaro-sys-0.1.2 (c (n "jotaro-sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0zbrpsw9636fsdxr30iyw2g0jn3srala45wsdl085jn6v2fgz5c4")))

(define-public crate-jotaro-sys-0.1.3 (c (n "jotaro-sys") (v "0.1.3") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "openssl-sys") (r "^0.9.99") (f (quote ("vendored"))) (d #t) (k 0)))) (h "04wqmi3gkwvznxk3385vcl2v9gkbmc6sd23y3xzr7w8hsn5h4ar6")))

