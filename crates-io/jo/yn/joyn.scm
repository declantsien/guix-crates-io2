(define-module (crates-io jo yn joyn) #:use-module (crates-io))

(define-public crate-joyn-0.1.0 (c (n "joyn") (v "0.1.0") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "assert_cmd") (r "^2") (d #t) (k 2)) (d (n "predicates") (r "^2") (d #t) (k 2)))) (h "00h49g3wbj8bb1ga5yh4hacixksxynarc1wzpnvshs6g905z6k23")))

(define-public crate-joyn-0.1.10 (c (n "joyn") (v "0.1.10") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "assert_cmd") (r "^2") (d #t) (k 2)) (d (n "predicates") (r "^2") (d #t) (k 2)))) (h "0i5k5xgi1s5m63wq6wjsr2f7z62spsbpbh7wpvjy0milxks9pdns") (y #t)))

(define-public crate-joyn-0.1.1 (c (n "joyn") (v "0.1.1") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "assert_cmd") (r "^2") (d #t) (k 2)) (d (n "predicates") (r "^2") (d #t) (k 2)))) (h "0jx69r413c7z7c8972jdwr4w7rijazrnw8j3nz4v34bhrsgbajm8")))

(define-public crate-joyn-0.2.0 (c (n "joyn") (v "0.2.0") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "assert_cmd") (r "^2") (d #t) (k 2)) (d (n "predicates") (r "^2") (d #t) (k 2)))) (h "0bs4j3zpybq6k4xs9dnrs0jagqi7rbw2xd487ajzihbin515d6fn")))

(define-public crate-joyn-0.2.2 (c (n "joyn") (v "0.2.2") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "assert_cmd") (r "^2") (d #t) (k 2)) (d (n "predicates") (r "^2") (d #t) (k 2)))) (h "0n4x2zhqrzsiss7r5l92mjxh4j6lhfrfhj93pgkm5sw86wghf0dm")))

(define-public crate-joyn-0.2.3 (c (n "joyn") (v "0.2.3") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "assert_cmd") (r "^2") (d #t) (k 2)) (d (n "predicates") (r "^2") (d #t) (k 2)))) (h "04389ady6bhzzrdljrbgz92v558g101df6flarm0688vrc0r7m4i") (y #t)))

(define-public crate-joyn-0.2.4 (c (n "joyn") (v "0.2.4") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "assert_cmd") (r "^2") (d #t) (k 2)) (d (n "predicates") (r "^2") (d #t) (k 2)))) (h "1x5v9p0k8kifc0spl3ljng7541sini7j3rk1qvkgj09f0xyqizss")))

(define-public crate-joyn-0.4.0 (c (n "joyn") (v "0.4.0") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "assert_cmd") (r "^2") (d #t) (k 2)) (d (n "predicates") (r "^2") (d #t) (k 2)))) (h "0ngncwzyk03clc2gp2hrprkccgq43z3q48lp06jvhsk3n508wi3w")))

