(define-module (crates-io jo ye joyent-tokio-zookeeper) #:use-module (crates-io))

(define-public crate-joyent-tokio-zookeeper-0.2.0 (c (n "joyent-tokio-zookeeper") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "slog") (r "^2.3.2") (d #t) (k 0)) (d (n "slog-async") (r "^2.3.0") (d #t) (k 2)) (d (n "slog-term") (r "^2.4.0") (d #t) (k 2)) (d (n "tokio") (r "^0.1") (d #t) (k 0)))) (h "0iml8hzhmhvyx8f2wxvpgxshwjdirqag1n2xkbsgj00ai0f6nr7i")))

(define-public crate-joyent-tokio-zookeeper-0.1.3 (c (n "joyent-tokio-zookeeper") (v "0.1.3") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "slog") (r "^2.3.2") (d #t) (k 0)) (d (n "slog-async") (r "^2.3.0") (d #t) (k 2)) (d (n "slog-term") (r "^2.4.0") (d #t) (k 2)) (d (n "tokio") (r "^0.1") (d #t) (k 0)))) (h "13gnnh06qxhhmsn4aw7rqaxrvq8wh3ibnmikvnqi897as20szd12")))

