(define-module (crates-io jo yf joyful_minigrep) #:use-module (crates-io))

(define-public crate-joyful_minigrep-0.1.0 (c (n "joyful_minigrep") (v "0.1.0") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "term") (r "^0.7.0") (d #t) (k 0)))) (h "1mgpcwxmj44kcxx5f1p894dziwhdn5rp8zdq32wxf3wddxvn8jg6")))

(define-public crate-joyful_minigrep-0.1.1 (c (n "joyful_minigrep") (v "0.1.1") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "term") (r "^0.7.0") (d #t) (k 0)))) (h "0kcirq9vv49fh5ppwx5a6g039xgilpcprydk7ii3vzp7h25agij5")))

(define-public crate-joyful_minigrep-0.1.2 (c (n "joyful_minigrep") (v "0.1.2") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "term") (r "^0.7.0") (d #t) (k 0)))) (h "0gsphij65yppl28lwswzg3j7hmr7vn50idvk01z7v7055a8zjzm4")))

(define-public crate-joyful_minigrep-0.1.3 (c (n "joyful_minigrep") (v "0.1.3") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "term") (r "^0.7.0") (d #t) (k 0)))) (h "0xw0qd7h8s48p1ig85srdv1cn0lajk8hrrrnibnd1ddv0jxiz561")))

