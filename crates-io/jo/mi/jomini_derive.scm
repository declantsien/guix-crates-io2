(define-module (crates-io jo mi jomini_derive) #:use-module (crates-io))

(define-public crate-jomini_derive-0.1.0 (c (n "jomini_derive") (v "0.1.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (f (quote ("diff"))) (d #t) (k 2)))) (h "1q511ig5bcljkg95w4m9piigzyb2y0cqq05gwz0yxksqr5zswvm7")))

(define-public crate-jomini_derive-0.1.1 (c (n "jomini_derive") (v "0.1.1") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (f (quote ("diff"))) (d #t) (k 2)))) (h "05d8hr5a4xp97x042rvga570n9i29gm4nbzdkp4dps5m3b5057rh")))

(define-public crate-jomini_derive-0.1.2 (c (n "jomini_derive") (v "0.1.2") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (f (quote ("diff"))) (d #t) (k 2)))) (h "0ijlql926r2fy8v32874rrsk3mgig2cl24zxhbdiyjxdszdhfipz")))

(define-public crate-jomini_derive-0.2.1 (c (n "jomini_derive") (v "0.2.1") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (f (quote ("diff"))) (d #t) (k 2)))) (h "025ifydry6n5x9ahdfv2km2g6sqr2bx9rb6wv399rshgv5gpan9y")))

(define-public crate-jomini_derive-0.2.2 (c (n "jomini_derive") (v "0.2.2") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (f (quote ("diff"))) (d #t) (k 2)))) (h "0f1c8d6ybxafx058vxi2jnh3vd8lbw4an3y7ll5jx8jc6makd2vh")))

(define-public crate-jomini_derive-0.2.3 (c (n "jomini_derive") (v "0.2.3") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (f (quote ("diff"))) (d #t) (k 2)))) (h "1v1sfh78zqjybxrhh2diaglckvw5s1f30vx876kyp44nwlzjzl99")))

(define-public crate-jomini_derive-0.2.4 (c (n "jomini_derive") (v "0.2.4") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (f (quote ("diff"))) (d #t) (k 2)))) (h "0y2mjz4ga6n2nw7jwc8h0qbqnagcn7lsif895p2nq0bw7zi61yl6")))

