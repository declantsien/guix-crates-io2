(define-module (crates-io jo bc jobclock) #:use-module (crates-io))

(define-public crate-jobclock-1.0.0 (c (n "jobclock") (v "1.0.0") (d (list (d (n "chrono") (r "^0.4.35") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)))) (h "0vrrcdxaxi7j6gwy0ja5678v0y4mlvxdz1mcv96arywyz9l2rqyz")))

(define-public crate-jobclock-1.0.1 (c (n "jobclock") (v "1.0.1") (d (list (d (n "chrono") (r "^0.4.35") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)))) (h "0swf6zsc2zwms6giyl3gl9j48wp8ymkdcyg4lbp72r49g4mli33c")))

(define-public crate-jobclock-1.0.2 (c (n "jobclock") (v "1.0.2") (d (list (d (n "chrono") (r "^0.4.35") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)))) (h "0b7hz08cglypky3a4634vi3xc7kg2nc2x23l8wb6njc73hh41qpa")))

(define-public crate-jobclock-1.1.0 (c (n "jobclock") (v "1.1.0") (d (list (d (n "chrono") (r "^0.4.35") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)))) (h "1lpaz5fgjxb8lcmwqvfbls2wwgblr24ia5i6y8cwl36j3i632wsy")))

(define-public crate-jobclock-1.2.0 (c (n "jobclock") (v "1.2.0") (d (list (d (n "chrono") (r "^0.4.35") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)))) (h "0y65fk7z5plk6nhi6wwwzhiaq1pglk12qx9j1lvy7nf2w86nwqcj")))

(define-public crate-jobclock-1.3.0 (c (n "jobclock") (v "1.3.0") (d (list (d (n "chrono") (r "^0.4.35") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)))) (h "1srx79v1hkpl35anni2dmqj3kcifl20gggb359a1f2gaxf9mbksw")))

(define-public crate-jobclock-1.3.1 (c (n "jobclock") (v "1.3.1") (d (list (d (n "chrono") (r "^0.4.35") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)))) (h "150mav94lp1vcknlzvklfr781fj1pqvqrbqyysaml97k525vdda7")))

