(define-module (crates-io lg bm lgbm) #:use-module (crates-io))

(define-public crate-lgbm-0.0.1 (c (n "lgbm") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 2)) (d (n "derive-ex") (r "^0.1.4") (d #t) (k 0)) (d (n "lgbm-sys") (r "^0.0.1") (d #t) (k 0)) (d (n "parse-display") (r "^0.8.0") (d #t) (k 0)) (d (n "text-grid") (r "^0.3.0") (d #t) (k 0)))) (h "0ikb8z2pl606agrp8isl95p63kjw407vpzl9hiaxrg83wz14i6c2")))

(define-public crate-lgbm-0.0.2 (c (n "lgbm") (v "0.0.2") (d (list (d (n "anyhow") (r "^1.0.80") (d #t) (k 2)) (d (n "derive-ex") (r "^0.1.7") (d #t) (k 0)) (d (n "lgbm-sys") (r "^0.0.1") (d #t) (k 0)) (d (n "parse-display") (r "^0.9.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "text-grid") (r "^0.4.1") (d #t) (k 0)))) (h "0aah0n8c95kg4da9ib4cr9hgvh7ya6qb946h7ljmsgb7n9cxxq2b")))

(define-public crate-lgbm-0.0.3 (c (n "lgbm") (v "0.0.3") (d (list (d (n "anyhow") (r "^1.0.86") (d #t) (k 2)) (d (n "derive-ex") (r "^0.1.8") (d #t) (k 0)) (d (n "lgbm-sys") (r "^0.0.1") (d #t) (k 0)) (d (n "parse-display") (r "^0.9.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.202") (f (quote ("derive"))) (d #t) (k 0)) (d (n "text-grid") (r "^0.4.1") (d #t) (k 0)))) (h "1i38i8w3dywmccn12zf4z3q2fznwki5q82isbaw4la7llv122fv3")))

