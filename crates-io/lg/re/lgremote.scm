(define-module (crates-io lg re lgremote) #:use-module (crates-io))

(define-public crate-LGremote-0.1.0 (c (n "LGremote") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11.8") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "10gl4i9w3ljkn736w84w50svk242h6kl3ndzzpsrn850wvpfh1kg")))

(define-public crate-LGremote-0.1.1 (c (n "LGremote") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.11.8") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "17z8149wsqlnlzwv6k8y8v0hf8fgbyv64x2zcw051q2d6c56w0l9")))

(define-public crate-LGremote-0.1.2 (c (n "LGremote") (v "0.1.2") (d (list (d (n "phf") (r "^0.10") (f (quote ("macros"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11.8") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0krzr430mvmy4ms11j2ay7nx53yr02bl859hqgq1mh0fy50b2xw2")))

(define-public crate-LGremote-0.2.0 (c (n "LGremote") (v "0.2.0") (d (list (d (n "phf") (r "^0.10") (f (quote ("macros"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11.8") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "09d9afylixpr6z6gfdfvsqic7wpnj02vg81476fzq9m1blrb8g7n")))

(define-public crate-LGremote-0.2.1 (c (n "LGremote") (v "0.2.1") (d (list (d (n "phf") (r "^0.10") (f (quote ("macros"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11.8") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0f1614f69nlifrg0kzjc76n0dg1viw84szv9q3ddh281xi9kdx1p")))

