(define-module (crates-io lg eo lgeo) #:use-module (crates-io))

(define-public crate-lgeo-1.0.0 (c (n "lgeo") (v "1.0.0") (d (list (d (n "lmaths") (r "^1.0.4") (d #t) (k 0)) (d (n "smallvec") (r "^1.6.1") (d #t) (k 0)))) (h "0f36rfpb9llwhfhh0q5jr93vcm8y03v4238nslagdhs0l2lkjh2l")))

(define-public crate-lgeo-1.0.1 (c (n "lgeo") (v "1.0.1") (d (list (d (n "lmaths") (r "^1.0.4") (d #t) (k 0)) (d (n "smallvec") (r "^1.6.1") (d #t) (k 0)))) (h "19accd0lm3n7ng93pws1fx4b3zbm35rfyakj92z82hdps89j5jsa")))

(define-public crate-lgeo-1.0.2 (c (n "lgeo") (v "1.0.2") (d (list (d (n "lmaths") (r "^1.0.4") (d #t) (k 0)) (d (n "smallvec") (r "^1.6.1") (d #t) (k 0)))) (h "1zn06nf2l18sx0vyzi82bj35q4airqhj5ixhxx584q9x2kb6cx7r")))

(define-public crate-lgeo-1.0.3 (c (n "lgeo") (v "1.0.3") (d (list (d (n "lmaths") (r "^1.0.4") (d #t) (k 0)) (d (n "smallvec") (r "^1.6.1") (d #t) (k 0)))) (h "0nylnnqqy22g3700zag1kgha8a9bgwcyibv9jp6c4gdkcayg2kwh")))

(define-public crate-lgeo-1.0.4 (c (n "lgeo") (v "1.0.4") (d (list (d (n "lmaths") (r "^1.0.4") (d #t) (k 0)) (d (n "smallvec") (r "^1.6.1") (d #t) (k 0)))) (h "1ccab1k69sjqawrd3klg4k1csh9k4182xbdy71qslq63df101v85")))

(define-public crate-lgeo-1.0.5 (c (n "lgeo") (v "1.0.5") (d (list (d (n "lmaths") (r "^1.0.4") (d #t) (k 0)) (d (n "smallvec") (r "^1.6.1") (d #t) (k 0)))) (h "17zjh3ircyil8a4shcpipc8mial3fnmpcdz7xa78d2g617g6cvf1")))

(define-public crate-lgeo-1.0.6 (c (n "lgeo") (v "1.0.6") (d (list (d (n "lmaths") (r "^1.0.4") (d #t) (k 0)) (d (n "smallvec") (r "^1.6.1") (d #t) (k 0)))) (h "0v70ayq80nj0yxd4i3b7vgwhq6azqm4zas05i66yjxw46mqxa3rs")))

(define-public crate-lgeo-1.0.7 (c (n "lgeo") (v "1.0.7") (d (list (d (n "lmaths") (r "^1.0.4") (d #t) (k 0)) (d (n "smallvec") (r "^1.6.1") (d #t) (k 0)))) (h "1cc08cvkwl17z0899b3fd96kb71d7fcysrwq6rpsifsy0mndj46w")))

