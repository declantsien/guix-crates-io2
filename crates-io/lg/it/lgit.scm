(define-module (crates-io lg it lgit) #:use-module (crates-io))

(define-public crate-lgit-0.1.0 (c (n "lgit") (v "0.1.0") (d (list (d (n "clap") (r "^4.3.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "01ksc61b512c8rrmp8p9vzma1xgddd30mk6mbk66lv05322c6nkf")))

(define-public crate-lgit-0.2.0 (c (n "lgit") (v "0.2.0") (d (list (d (n "clap") (r "^4.3.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0dpmcdxdajgry3sp25ihrmv7lmmkb013jqbr41g6fp6b39i8dkdl")))

(define-public crate-lgit-0.2.1 (c (n "lgit") (v "0.2.1") (d (list (d (n "clap") (r "^4.3.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0xjf3hyf6xgrf7ynbjk4685s9bwr5q7sajxyv14cphp6483cr886")))

(define-public crate-lgit-0.3.0 (c (n "lgit") (v "0.3.0") (d (list (d (n "clap") (r "^4.3.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0bd1hy4rm7iixlyyffd4cmqkk5z3w843sbdgw1pm9890nkjbr1bv")))

(define-public crate-lgit-0.4.0 (c (n "lgit") (v "0.4.0") (d (list (d (n "clap") (r "^4.3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "inquire") (r "^0.6.2") (d #t) (k 0)))) (h "0yzbjzwbj0i2g36ymjr988wwfiyqjayk53hzkc23js7jrlwihm8y")))

(define-public crate-lgit-0.4.1 (c (n "lgit") (v "0.4.1") (d (list (d (n "clap") (r "^4.3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "inquire") (r "^0.6.2") (d #t) (k 0)))) (h "0xvbrkl1jf3jmvy4qbn074fgzg8y31s9w0qk0qg0v8nnafl9kf11")))

(define-public crate-lgit-0.5.0 (c (n "lgit") (v "0.5.0") (d (list (d (n "clap") (r "^4.4.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dialoguer") (r "^0.11.0") (f (quote ("fuzzy-select"))) (d #t) (k 0)) (d (n "mockall") (r "^0.12.1") (d #t) (k 0)))) (h "0p9hc2wi7pf3jd6zx694r7zwkfd2cpshvv5zc12m5hp5wzr6wp5x")))

(define-public crate-lgit-0.6.0 (c (n "lgit") (v "0.6.0") (d (list (d (n "clap") (r "^4.4.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dialoguer") (r "^0.11.0") (f (quote ("fuzzy-select"))) (d #t) (k 0)) (d (n "mockall") (r "^0.12.1") (d #t) (k 0)))) (h "1xqn7gd4136gs3ck4a7rn214rj7g803rdc589irrmqya1b0d76xf")))

(define-public crate-lgit-0.7.0 (c (n "lgit") (v "0.7.0") (d (list (d (n "clap") (r "^4.4.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dialoguer") (r "^0.11.0") (f (quote ("fuzzy-select"))) (d #t) (k 0)) (d (n "mockall") (r "^0.12.1") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "1hyk3afi7cqq06aza0s6iyac0lhqa21bmfcnpxds4csnyfvff9af")))

(define-public crate-lgit-0.7.1 (c (n "lgit") (v "0.7.1") (d (list (d (n "clap") (r "^4.4.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dialoguer") (r "^0.11.0") (f (quote ("fuzzy-select"))) (d #t) (k 0)) (d (n "mockall") (r "^0.12.1") (d #t) (k 0)))) (h "0s7sbsqlcca9l794m40fphx837z34b1bq1r2hpz8m8rvvg3s60h2")))

(define-public crate-lgit-0.7.2 (c (n "lgit") (v "0.7.2") (d (list (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dialoguer") (r "^0.11.0") (f (quote ("fuzzy-select"))) (d #t) (k 0)) (d (n "mockall") (r "^0.12.1") (d #t) (k 0)))) (h "0603j8d5h7msgr421s7gsad9rz9d4by1xlxm1jpajvp04fc1g6rn")))

(define-public crate-lgit-0.7.3 (c (n "lgit") (v "0.7.3") (d (list (d (n "clap") (r "^4.5.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dialoguer") (r "^0.11.0") (f (quote ("fuzzy-select"))) (d #t) (k 0)) (d (n "mockall") (r "^0.12.1") (d #t) (k 0)))) (h "1lim9jmkas8cqgr4y5ic7y0cn8df7wmjsn1kizn6vr34s4c1wx78")))

