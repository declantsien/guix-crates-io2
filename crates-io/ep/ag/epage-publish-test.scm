(define-module (crates-io ep ag epage-publish-test) #:use-module (crates-io))

(define-public crate-epage-publish-test-0.1.0 (c (n "epage-publish-test") (v "0.1.0") (h "1map8c98qbxdjlxnyfh0pav6yxif20p4fdc8949lypl40dc0g739")))

(define-public crate-epage-publish-test-0.1.1 (c (n "epage-publish-test") (v "0.1.1") (h "1lnpkrdjpawi69v0l8zvf8whh00vbfcp2vbfjay904c43vrklyla")))

(define-public crate-epage-publish-test-0.1.2 (c (n "epage-publish-test") (v "0.1.2") (h "1gyjrb2vskx1ymi5armpc11skh241n8691c03ixvq8s7sx6nj0hy")))

(define-public crate-epage-publish-test-0.1.3 (c (n "epage-publish-test") (v "0.1.3") (h "0c8ahdc6rnjq5ba5xaaih6wi3dq944qp3bzvj32g1jkj0z5lg4j4")))

(define-public crate-epage-publish-test-0.1.4 (c (n "epage-publish-test") (v "0.1.4") (h "1ln8dgv8008yg60whpizy82crc6w6nl9rv11863ns4lqd84krvcq")))

(define-public crate-epage-publish-test-0.1.5 (c (n "epage-publish-test") (v "0.1.5") (h "0nq6h62mwj4rifd5yi5fx7iq6jsi09silg2dzv6nf36gmmp7lv8v")))

(define-public crate-epage-publish-test-0.1.6 (c (n "epage-publish-test") (v "0.1.6") (h "1smjl9718bk685iw8syx91905z7kf1rbgnq0pgxlbiwb5lyrka5k")))

(define-public crate-epage-publish-test-0.1.7 (c (n "epage-publish-test") (v "0.1.7") (h "1na5lyixp43cd68zd9xx3ndg1ppiprrd1rm17gf33kldjy48jxla")))

(define-public crate-epage-publish-test-0.1.8 (c (n "epage-publish-test") (v "0.1.8") (h "1n1pp7ngapj4j85r5br6wiqwblw2f294q315qwwghwwxz0n5iy9x")))

(define-public crate-epage-publish-test-0.1.9 (c (n "epage-publish-test") (v "0.1.9") (h "05l7bv8hc8ay8yxhpw9i2zl6xxgh6dd71vy8mqcwiy4pbkkc30z8")))

(define-public crate-epage-publish-test-0.1.10 (c (n "epage-publish-test") (v "0.1.10") (h "196hgyklyc7nni9329a216xmmid92p9iqkv1yj0i3yw4m2aclchr")))

(define-public crate-epage-publish-test-0.1.11 (c (n "epage-publish-test") (v "0.1.11") (h "0vb0nz6jax3xv3lshq9ggf54fgpx5rlplm9ynwhjss4y98mas82v")))

(define-public crate-epage-publish-test-0.1.12 (c (n "epage-publish-test") (v "0.1.12") (h "1vqd7mpk63wj3rl7h9wc52rn2rlz6lga9z7nnylf1nazgc1gp7mh")))

(define-public crate-epage-publish-test-0.1.14 (c (n "epage-publish-test") (v "0.1.14") (h "12152xhmm5gb1qh59v5rac1878dl5mjpi05m15vdsfaqpxf1sq4b")))

(define-public crate-epage-publish-test-0.1.15+deadbeef (c (n "epage-publish-test") (v "0.1.15+deadbeef") (h "0703wakx3c35h6071hbaj6d37vylb5qc3p43xgkid553l7mynxk5")))

