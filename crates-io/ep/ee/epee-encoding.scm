(define-module (crates-io ep ee epee-encoding) #:use-module (crates-io))

(define-public crate-epee-encoding-0.1.0 (c (n "epee-encoding") (v "0.1.0") (d (list (d (n "epee-encoding-derive") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 2)) (d (n "sealed") (r "^0.5.0") (d #t) (k 0)))) (h "0c2yq0z49yvgdd7fcr3vbvzzj61axhlyfpd7yidqyx26wdcsk9lx") (f (quote (("default" "derive")))) (y #t) (s 2) (e (quote (("derive" "dep:epee-encoding-derive"))))))

(define-public crate-epee-encoding-0.2.0 (c (n "epee-encoding") (v "0.2.0") (d (list (d (n "epee-encoding-derive") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 2)) (d (n "sealed") (r "^0.5.0") (d #t) (k 0)))) (h "1f8d9cjgjc8bsqis7113ppnwvxc7vdfj3rvbscd5a6z7a198z5dv") (f (quote (("default" "derive")))) (s 2) (e (quote (("derive" "dep:epee-encoding-derive")))) (r "1.60")))

(define-public crate-epee-encoding-0.3.0 (c (n "epee-encoding") (v "0.3.0") (d (list (d (n "epee-encoding-derive") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 2)) (d (n "sealed") (r "^0.5.0") (d #t) (k 0)))) (h "1bhnhy1i4j0cpki0ibs464yq5iflfy73i0dym27mbj1i1abrr5v2") (f (quote (("default" "derive")))) (s 2) (e (quote (("derive" "dep:epee-encoding-derive")))) (r "1.60")))

(define-public crate-epee-encoding-0.4.0 (c (n "epee-encoding") (v "0.4.0") (d (list (d (n "epee-encoding-derive") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 2)) (d (n "sealed") (r "^0.5.0") (d #t) (k 0)))) (h "0acqfpr1za0pplznb7lk9wpl3dy3jrz13c6612mv37j3pqjcmg63") (f (quote (("default" "derive")))) (s 2) (e (quote (("derive" "dep:epee-encoding-derive")))) (r "1.60")))

(define-public crate-epee-encoding-0.5.0 (c (n "epee-encoding") (v "0.5.0") (d (list (d (n "epee-encoding-derive") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 2)) (d (n "sealed") (r "^0.5.0") (d #t) (k 0)) (d (n "thiserror") (r "^1") (o #t) (d #t) (k 0)))) (h "1xylhmikpgm9mcl8xyi2041w2crfpvwl4fiv9np2lli973jxjjy4") (f (quote (("default" "derive" "std")))) (s 2) (e (quote (("std" "dep:thiserror") ("derive" "dep:epee-encoding-derive")))) (r "1.60")))

