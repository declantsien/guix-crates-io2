(define-module (crates-io ep ee epee-encoding-derive) #:use-module (crates-io))

(define-public crate-epee-encoding-derive-0.1.0 (c (n "epee-encoding-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0kifbg17wxlc8mfz63al3i3gb4clz2pyxmyqyzcq2w0i2vvsn67k")))

(define-public crate-epee-encoding-derive-0.2.0 (c (n "epee-encoding-derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1v14z9ma33w7y7npf0848zn0a62c76gnzdw158sh0cckl6mqs5f2")))

(define-public crate-epee-encoding-derive-0.3.0 (c (n "epee-encoding-derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0a2m838s4q1jkawzs37lk3l35cxd4yiyzfr6q1glc8g38i4hfmqj")))

(define-public crate-epee-encoding-derive-0.4.0 (c (n "epee-encoding-derive") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0bzvsdfzvvvaw22i58vrl0450w9h3hgz9lzd8fxfk026fxsnnfvm")))

