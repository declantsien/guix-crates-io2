(define-module (crates-io ep ac epac-utils) #:use-module (crates-io))

(define-public crate-epac-utils-0.1.0 (c (n "epac-utils") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.63") (o #t) (d #t) (k 0)) (d (n "find_folder") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "piston_window") (r "^0.124.0") (o #t) (d #t) (k 0)) (d (n "tracing") (r "^0.1.36") (o #t) (d #t) (k 0)))) (h "1839j5qrdxrpnwc9fn1rh3mh4g6isgkd2z9bpwxwr59y1xcna5c1") (f (quote (("default" "anyhow" "tracing")))) (y #t) (s 2) (e (quote (("tracing" "dep:tracing") ("piston_cacher" "dep:piston_window" "dep:find_folder") ("anyhow" "dep:anyhow"))))))

