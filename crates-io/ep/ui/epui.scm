(define-module (crates-io ep ui epui) #:use-module (crates-io))

(define-public crate-epui-0.1.0 (c (n "epui") (v "0.1.0") (h "072cqcv6h5s1a674mphlpcxkxshdwaisah9gpfyw9wql3ldxl470") (r "1.56.1")))

(define-public crate-epui-0.1.1 (c (n "epui") (v "0.1.1") (h "0a9w95n30ry541bh9jfj355kyvs91bz3fxca4ln6j1aa8li5s37l") (r "1.56.1")))

