(define-module (crates-io ep oc epoch) #:use-module (crates-io))

(define-public crate-epoch-0.0.0 (c (n "epoch") (v "0.0.0") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)))) (h "05sg97cdm4n1vi2fhaxsbzbi1jb2v68fpsmcblppgf11x6f3nbgb")))

(define-public crate-epoch-0.0.1 (c (n "epoch") (v "0.0.1") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)))) (h "1ipjzkv8mqlharkvi04wfv3h86m0sxg3nrh9zsf29z06k9ky6wz0")))

(define-public crate-epoch-0.0.2 (c (n "epoch") (v "0.0.2") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)))) (h "1xrr0rf36bcx595bjvvx83mqpqw25vwzqkdhv9jdgiz911p1ysza")))

