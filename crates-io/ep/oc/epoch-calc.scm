(define-module (crates-io ep oc epoch-calc) #:use-module (crates-io))

(define-public crate-epoch-calc-0.1.0 (c (n "epoch-calc") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.27") (d #t) (k 0)))) (h "01bs59qc1gm1d82in435yklgj9yni3iz3myvlbi4wmvva24cy5nc")))

(define-public crate-epoch-calc-0.1.1 (c (n "epoch-calc") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.27") (d #t) (k 0)))) (h "1i0pik65ndrl6wg0v63nxmyd747i2sq2vp7vdc2s7nlcp80md1v5")))

(define-public crate-epoch-calc-0.1.2 (c (n "epoch-calc") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.27") (d #t) (k 0)))) (h "11i16gqg8mwxpjrdf9dp28b9g52bfqr5fq3506w8h4k3bhi1jlii")))

