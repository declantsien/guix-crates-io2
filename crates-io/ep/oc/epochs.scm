(define-module (crates-io ep oc epochs) #:use-module (crates-io))

(define-public crate-epochs-0.2.0 (c (n "epochs") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "0d66x9qs8s9ihm31fgmy3kyb843wp6hhp1gy8n13smkmiaza3x8b")))

(define-public crate-epochs-0.2.1 (c (n "epochs") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "04dwscymksm7gl0c7870zbf2hbp7nghxyxh23d1yq0sfmxvsymrs")))

(define-public crate-epochs-0.2.2 (c (n "epochs") (v "0.2.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "0av8bga0b853q9yf85dvbjpca7bg7pdmcd2nnc748c7pj9v81lyi")))

(define-public crate-epochs-0.2.3 (c (n "epochs") (v "0.2.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "12z992x8nxdxrq8sjvlgynfikhkk1b3264nxndh8p6d6j0wlj2w4")))

(define-public crate-epochs-0.2.4 (c (n "epochs") (v "0.2.4") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "0r071qigfcrx1i6his5wbmczbc500hhv215m40qn0nw6a5wy746h")))

