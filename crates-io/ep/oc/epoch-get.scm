(define-module (crates-io ep oc epoch-get) #:use-module (crates-io))

(define-public crate-epoch-get-0.1.0 (c (n "epoch-get") (v "0.1.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "11hmy7kp0xblxwyhj56z5khyppibnw1ps1ynhmsgnkkqhg8rf2fs")))

(define-public crate-epoch-get-0.1.1 (c (n "epoch-get") (v "0.1.1") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "0bnp0a0pb8v8w1x3340nq5dm0x52w8kzfph416rk7lrgzv78j755")))

(define-public crate-epoch-get-0.2.0 (c (n "epoch-get") (v "0.2.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "1wmr91gz3vr9hc11lnnc7jzi6w82a83z4xb1xmxgd6yi5y7g9fv0")))

(define-public crate-epoch-get-0.3.0 (c (n "epoch-get") (v "0.3.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "085f694l887mw3gfpr2xdkb9gibfrd4zy3a7mmy64ghaj34rw4x7")))

(define-public crate-epoch-get-0.4.0 (c (n "epoch-get") (v "0.4.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "1ylmnp4kcsrrw5wi8xwxz6lax1ydbl9jhm5q3q8247r5q0pqah0d")))

(define-public crate-epoch-get-0.4.1 (c (n "epoch-get") (v "0.4.1") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "1b4a55kds5kilzgwvskcld176p76zn448807wm8gnss7i7csg1av")))

