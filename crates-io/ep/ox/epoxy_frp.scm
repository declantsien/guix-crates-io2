(define-module (crates-io ep ox epoxy_frp) #:use-module (crates-io))

(define-public crate-epoxy_frp-0.3.0 (c (n "epoxy_frp") (v "0.3.0") (d (list (d (n "epoxy_macros") (r "^0.3.0") (d #t) (k 0)) (d (n "epoxy_streams") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)))) (h "1zl0bn8h3am1psqkdqpiplrwqsmds0np389k7q1iv2fa51gcrkv7")))

(define-public crate-epoxy_frp-0.3.1 (c (n "epoxy_frp") (v "0.3.1") (d (list (d (n "epoxy_macros") (r "^0.3.1") (d #t) (k 0)) (d (n "epoxy_streams") (r "^0.3.1") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)))) (h "1592pnhc0pj2bjs67dqzcgckmbifdg0s7cqifgp0g8ldcgiin3k8")))

