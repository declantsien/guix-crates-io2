(define-module (crates-io ep ox epoxy_streams) #:use-module (crates-io))

(define-public crate-epoxy_streams-0.3.0 (c (n "epoxy_streams") (v "0.3.0") (h "1d51wyjm48gvlzssa7hpy05gpk1qm2sa9sr8nm36wggxzpciclc7")))

(define-public crate-epoxy_streams-0.3.1 (c (n "epoxy_streams") (v "0.3.1") (h "1rp14cbzsfhlfjjlzczqk5v686ngjf0l49acrs5rjbh00m8xr8rv")))

