(define-module (crates-io ep ox epoxy_macros) #:use-module (crates-io))

(define-public crate-epoxy_macros-0.3.0 (c (n "epoxy_macros") (v "0.3.0") (d (list (d (n "epoxy_streams") (r "^0.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "1rpxk3qs2xhjkhy6yy45bcpi8pz8qc793wqixjn60iar0qcl26g7")))

(define-public crate-epoxy_macros-0.3.1 (c (n "epoxy_macros") (v "0.3.1") (d (list (d (n "epoxy_streams") (r "^0.3.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "0n3scsvsfmmpggs6ql6afyj6clv59kfbl7issz141sv5mxawlhd1")))

