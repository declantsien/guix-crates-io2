(define-module (crates-io ep rn eprng) #:use-module (crates-io))

(define-public crate-eprng-0.1.0 (c (n "eprng") (v "0.1.0") (h "0japb66kn3cx72pl8haxzw74ch71js28v1bmf1glj35ykk5zzlng") (f (quote (("distribution") ("default" "distribution")))) (y #t)))

(define-public crate-eprng-0.1.1 (c (n "eprng") (v "0.1.1") (h "09vz01jrrqssxnfzwznl62d69igva9jv7apvqi461s597h11jnjz") (f (quote (("distribution") ("default" "distribution"))))))

(define-public crate-eprng-0.1.2 (c (n "eprng") (v "0.1.2") (h "1gmlcz17lrcz105cky7vv61kji973bsj0piaikr3vj3p3vhw2q8g") (f (quote (("distribution") ("default" "distribution"))))))

