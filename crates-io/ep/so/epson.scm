(define-module (crates-io ep so epson) #:use-module (crates-io))

(define-public crate-epson-0.1.0 (c (n "epson") (v "0.1.0") (d (list (d (n "image") (r "^0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("io-util"))) (o #t) (k 0)))) (h "0709h5px8ags73yxn7v5f7hp1x8kdsd57vhsbb84grng52n2rp3i") (s 2) (e (quote (("tokio" "dep:tokio"))))))

(define-public crate-epson-0.1.1 (c (n "epson") (v "0.1.1") (d (list (d (n "image") (r "^0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("io-util"))) (o #t) (k 0)))) (h "0g05p0671188sc9yjgm9hzz2ym502b06wsjkdawzyixh2yxlwsca") (s 2) (e (quote (("tokio" "dep:tokio"))))))

(define-public crate-epson-0.1.2 (c (n "epson") (v "0.1.2") (d (list (d (n "image") (r "^0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("io-util"))) (o #t) (k 0)))) (h "1y2xal3ycp0l9s1nvrfcsqwd42q9jzghsj4d7ibj63s7q6piqwhf") (s 2) (e (quote (("tokio" "dep:tokio"))))))

(define-public crate-epson-0.1.3 (c (n "epson") (v "0.1.3") (d (list (d (n "image") (r "^0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("io-util"))) (o #t) (k 0)))) (h "0zkdgb93sr0hd1l59qqid90yl6qvhr7gg66qds7yxizyzn1wv7ca") (s 2) (e (quote (("tokio" "dep:tokio"))))))

(define-public crate-epson-0.1.4 (c (n "epson") (v "0.1.4") (d (list (d (n "image") (r "^0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("io-util"))) (o #t) (k 0)))) (h "0cdcwrihnqgn1qblyqcmd9mr1fnlaxzz8p1pqi3pkiwwwczaidz0") (s 2) (e (quote (("tokio" "dep:tokio"))))))

(define-public crate-epson-0.1.5 (c (n "epson") (v "0.1.5") (d (list (d (n "image") (r "^0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("io-util"))) (o #t) (k 0)))) (h "01ywd5sznjssdqx0wvx463pzq6ilmds7fqfz2a3nnaff0q6pqf80") (s 2) (e (quote (("tokio" "dep:tokio"))))))

(define-public crate-epson-0.1.6 (c (n "epson") (v "0.1.6") (d (list (d (n "image") (r "^0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("io-util"))) (o #t) (k 0)))) (h "17b8zda190gbjv1bandf0bf4rddqpkji88a86ksm739cd8fhixly") (s 2) (e (quote (("tokio" "dep:tokio"))))))

(define-public crate-epson-0.1.7 (c (n "epson") (v "0.1.7") (d (list (d (n "image") (r "^0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("io-util"))) (o #t) (k 0)))) (h "0gbpfny2pnclvzwhffk7paqc871aqm74jknmy0080sns3xlsmicd") (s 2) (e (quote (("tokio" "dep:tokio"))))))

