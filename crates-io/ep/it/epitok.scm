(define-module (crates-io ep it epitok) #:use-module (crates-io))

(define-public crate-epitok-0.1.0 (c (n "epitok") (v "0.1.0") (d (list (d (n "async-std") (r "^1.6.2") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "chrono") (r "^0.4.11") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.6") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0ijq33lrl9rf8db6w91l31i5sas5zv8ldiwfgwic1cxzs9vj1pg2") (y #t)))

(define-public crate-epitok-0.2.0 (c (n "epitok") (v "0.2.0") (d (list (d (n "async-std") (r "^1.6.2") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "chrono") (r "^0.4.11") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.6") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "169vl6njvafcxglpkhibmzl9xxxmd1lf6abzhdl5jn5bz1vya1d6") (y #t)))

