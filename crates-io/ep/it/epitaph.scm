(define-module (crates-io ep it epitaph) #:use-module (crates-io))

(define-public crate-epitaph-0.1.0 (c (n "epitaph") (v "0.1.0") (d (list (d (n "alternate-future") (r "*") (o #t) (d #t) (k 0)))) (h "0mdnsiykky1h3h7v63mv6xg4x5wwf1aln93vpchc2l3gb425ar51")))

(define-public crate-epitaph-0.1.1 (c (n "epitaph") (v "0.1.1") (d (list (d (n "alternate-future") (r "*") (o #t) (d #t) (k 0)))) (h "0xqf0cqh0cn0jpq8x78ryqx3dg2kncl80x3in8hrqc63023y0qhy")))

(define-public crate-epitaph-0.1.2 (c (n "epitaph") (v "0.1.2") (d (list (d (n "alternate-future") (r "*") (o #t) (d #t) (k 0)))) (h "0c2qb4r739ma23dmrmmdb6pvv3c2wswycq8m7rdm63hfkpgcxpm3")))

(define-public crate-epitaph-0.2.0 (c (n "epitaph") (v "0.2.0") (d (list (d (n "alternate-future") (r "*") (o #t) (d #t) (k 0)))) (h "0hrx7aky7i93dyi6h2q79q40csjm903hrvql4xhy8ykhagn72kkl")))

