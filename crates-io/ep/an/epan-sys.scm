(define-module (crates-io ep an epan-sys) #:use-module (crates-io))

(define-public crate-epan-sys-0.1.0 (c (n "epan-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.66") (o #t) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1jpr6kxnq4l6h4vsdx3d8scavlw5y4qy1vcphhg7vwsk55nvwfiz") (l "wireshark") (s 2) (e (quote (("bindgen" "dep:bindgen"))))))

