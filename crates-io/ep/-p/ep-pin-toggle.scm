(define-module (crates-io ep -p ep-pin-toggle) #:use-module (crates-io))

(define-public crate-ep-pin-toggle-0.1.0 (c (n "ep-pin-toggle") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "embedded-profiling") (r "^0.2") (d #t) (k 0)))) (h "1w6bav50mvlr95kqdj82csys2yss3i1myzjrp8c6b6hxdv537l5c")))

(define-public crate-ep-pin-toggle-0.1.1 (c (n "ep-pin-toggle") (v "0.1.1") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "embedded-profiling") (r "^0.2") (d #t) (k 0)))) (h "0dm4r2v1rxgyyrg4h1i02rghlvxw34fqgaz1456cs1c5i8lk730v")))

(define-public crate-ep-pin-toggle-0.1.2 (c (n "ep-pin-toggle") (v "0.1.2") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 2)) (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "embedded-profiling") (r "^0.2") (d #t) (k 0)))) (h "18b2jamgh6jwhfcrpmy41dsbqznaml4rrpmwn4c3hllw3p5ciwnr") (f (quote (("proc-macros" "embedded-profiling/proc-macros"))))))

(define-public crate-ep-pin-toggle-0.1.3 (c (n "ep-pin-toggle") (v "0.1.3") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 2)) (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "embedded-profiling") (r "^0.2") (d #t) (k 0)))) (h "0i98xb7sjakpki0wl15cbp1ypqdcickqzd0jncq5jpxfmwk41zlv") (f (quote (("proc-macros" "embedded-profiling/proc-macros"))))))

(define-public crate-ep-pin-toggle-0.1.4 (c (n "ep-pin-toggle") (v "0.1.4") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 2)) (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "embedded-profiling") (r "^0.2") (d #t) (k 0)))) (h "0wc8dr6c82j9wh5kxahapwlck6ci50g1hhsydsal1qfnj0fqg1jp") (f (quote (("proc-macros" "embedded-profiling/proc-macros"))))))

(define-public crate-ep-pin-toggle-0.1.5 (c (n "ep-pin-toggle") (v "0.1.5") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 2)) (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "embedded-profiling") (r "^0.2") (d #t) (k 0)))) (h "1zmkwx8g029fp8ligd0b1ni9cxjrzfw4xhxwfrp8n69ql38irnvf") (f (quote (("proc-macros" "embedded-profiling/proc-macros"))))))

(define-public crate-ep-pin-toggle-0.2.0 (c (n "ep-pin-toggle") (v "0.2.0") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 2)) (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "embedded-profiling") (r "^0.3") (d #t) (k 0)))) (h "108ykwy5sxlfb7sz0d0137q5vf4kbzx3s6majfkyq23sl1izb1q2") (f (quote (("proc-macros" "embedded-profiling/proc-macros")))) (r "1.57")))

(define-public crate-ep-pin-toggle-0.3.0 (c (n "ep-pin-toggle") (v "0.3.0") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 2)) (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "embedded-profiling") (r "^0.3") (d #t) (k 0)))) (h "0hwz7cwz1gip6h100wld3vnsa2vbz9if28fgvs0j2qxkcr5y5g0v") (f (quote (("proc-macros" "embedded-profiling/proc-macros")))) (r "1.61")))

