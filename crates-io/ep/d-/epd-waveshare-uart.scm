(define-module (crates-io ep d- epd-waveshare-uart) #:use-module (crates-io))

(define-public crate-epd-waveshare-uart-0.1.0 (c (n "epd-waveshare-uart") (v "0.1.0") (d (list (d (n "arrayvec") (r "^0.5.1") (d #t) (k 0)) (d (n "embedded-graphics") (r "^0.6.0-alpha.2") (o #t) (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.1") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8.20") (d #t) (k 0)))) (h "0qj8677f91prgdn4jwb12ha0gkk0is5jad9ndml2i77gjm3qa124") (f (quote (("graphics" "embedded-graphics") ("epd4in3") ("default" "epd4in3" "graphics"))))))

