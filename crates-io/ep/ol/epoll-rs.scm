(define-module (crates-io ep ol epoll-rs) #:use-module (crates-io))

(define-public crate-epoll-rs-0.2.0 (c (n "epoll-rs") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (f (quote ("extra_traits"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "1dji285m7v4ck3i39vs56r1mwldds25njpj8v7f4kbhbni9g0yj1")))

(define-public crate-epoll-rs-0.2.1 (c (n "epoll-rs") (v "0.2.1") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "0rp7397qy408iimns49hk3walnlmd0qn1gxndm8ghcsg0ylq5iyz")))

(define-public crate-epoll-rs-0.2.2 (c (n "epoll-rs") (v "0.2.2") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "1jpkpccy1dqzjq96g1m6rzbxra2fiaiplpg4al4qxca24dr96c8s")))

