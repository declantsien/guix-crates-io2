(define-module (crates-io ep ol epoll_test) #:use-module (crates-io))

(define-public crate-epoll_test-0.1.0 (c (n "epoll_test") (v "0.1.0") (h "030zc9x1wy6jyw713vb8f9p49g877x9x01bl5v06yr8z6ik2ppsy")))

(define-public crate-epoll_test-0.1.1 (c (n "epoll_test") (v "0.1.1") (d (list (d (n "rayon") (r "^1.9.0") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (d #t) (k 0)))) (h "1zwa68psx191f6fbbbg6vnrjhz3pjm5p6wd308dpn32mjqj1sypw")))

