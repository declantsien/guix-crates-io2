(define-module (crates-io ep ol epoll) #:use-module (crates-io))

(define-public crate-epoll-0.1.0 (c (n "epoll") (v "0.1.0") (d (list (d (n "errno") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "0r6dj15z0n1qsz7xmxw7pbn9m8vwg2nxrk3fmvdajb89q6j2dpq6")))

(define-public crate-epoll-0.2.0 (c (n "epoll") (v "0.2.0") (d (list (d (n "errno") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "1x9767ijfl4vdl3m85al351l44xz38b81jbm6iz8drv8ligb3vdx")))

(define-public crate-epoll-0.2.1 (c (n "epoll") (v "0.2.1") (d (list (d (n "errno") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "0cyi374jwl7asba04z1g92lynxnnalgdrav6nwqg7dsxc0y5hin6")))

(define-public crate-epoll-0.2.2 (c (n "epoll") (v "0.2.2") (d (list (d (n "errno") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "02qmnvhn552g7plg9gcpv2j0disj628l0syg5k9vzmmav5cl7pfd")))

(define-public crate-epoll-0.2.3 (c (n "epoll") (v "0.2.3") (d (list (d (n "errno") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "13dpd9p720ixjjgglyl4gyg04znzi13wp0mk02wg5433kclmwdqg")))

(define-public crate-epoll-0.2.4 (c (n "epoll") (v "0.2.4") (d (list (d (n "errno") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "1dcxv6sabysa9nkdg20j0hxfxdiwn4yf3wy78i5dwk5cci5ixpbn")))

(define-public crate-epoll-0.2.5 (c (n "epoll") (v "0.2.5") (d (list (d (n "errno") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "0dqqwg7gw7hawnxh8s19ijv88hq7riwrsc69ksc9nf2iy0bw9v37")))

(define-public crate-epoll-0.3.0 (c (n "epoll") (v "0.3.0") (d (list (d (n "errno") (r "^0.1.6") (d #t) (k 0)) (d (n "libc") (r "^0.2.9") (d #t) (k 0)))) (h "0amya475mjphp71xlwp95cn62gyn4rwmxi4mz81wh5y5fd8z4318")))

(define-public crate-epoll-0.4.0 (c (n "epoll") (v "0.4.0") (d (list (d (n "bitflags") (r "^0.7") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "simple-slab") (r "^0.2") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1s71d3j4klbamynqfsv1sgz6l7gjkbw06031q2cngv90jfzrwjra")))

(define-public crate-epoll-0.4.1 (c (n "epoll") (v "0.4.1") (d (list (d (n "bitflags") (r "^0.7") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "simple-slab") (r "^0.2") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1paxl392a12zmix77wm0nkd3lm5h6vhwx67kncz88q6vb9vsa6lr")))

(define-public crate-epoll-0.4.2 (c (n "epoll") (v "0.4.2") (d (list (d (n "bitflags") (r "^0.7") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "simple-slab") (r "^0.2") (d #t) (k 0)))) (h "0ai5qy6in50272aav951k2rpigvdgkpbg28z30fwp3lvx8cp8pvc")))

(define-public crate-epoll-0.5.0 (c (n "epoll") (v "0.5.0") (d (list (d (n "bitflags") (r "^0.7") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "simple-slab") (r "^0.2") (d #t) (k 0)))) (h "0kjcrzch86pyf53zhapqw4k7pc5pjzh36arwdlfv8kv5kyvl8wdv")))

(define-public crate-epoll-0.6.0 (c (n "epoll") (v "0.6.0") (d (list (d (n "bitflags") (r "^0.7") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "simple-slab") (r "^0.2") (d #t) (k 0)))) (h "0fyy396rg30nga9wm738kjhlnj5i6gk1wsk9h7waaykx0jan5s7g")))

(define-public crate-epoll-1.0.0 (c (n "epoll") (v "1.0.0") (d (list (d (n "bitflags") (r "^0.7") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0chgh4x6wdzfvqpd01zwmk4vr1xs5927f3whf5fdd7wmh7m541xz")))

(define-public crate-epoll-2.0.0 (c (n "epoll") (v "2.0.0") (d (list (d (n "bitflags") (r "^0.7") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0rj0csz1850l80qvwqmd31s4mcxwx5pxcl7nkycqcfqq6bg2wvkb")))

(define-public crate-epoll-2.1.0 (c (n "epoll") (v "2.1.0") (d (list (d (n "bitflags") (r "^0.7") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1al3bcaqwgdq9kfk9xhkdg4nhjbhjz64r51y4v89qc02bbgz8f7l")))

(define-public crate-epoll-3.0.0 (c (n "epoll") (v "3.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1grx2qxmw49arbngigyalh53qaf2ahf87lf3kq09k56a8znrp1nv")))

(define-public crate-epoll-3.1.0 (c (n "epoll") (v "3.1.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1cqk20z0wwlrfbnadygfffj2bjbyp10sjx17cpdnx9z78fwqgslx")))

(define-public crate-epoll-3.1.1 (c (n "epoll") (v "3.1.1") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1msrn0df1fj8q5wbgckk883vr344cl6qrf0s1y8lijqb5akcah7w")))

(define-public crate-epoll-4.0.0 (c (n "epoll") (v "4.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "010yc719inslnxizlivd75jibjb7aazmcf38bsig2kzbvfgra6bj")))

(define-public crate-epoll-4.0.1 (c (n "epoll") (v "4.0.1") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "12my7d79x6y1lxb2yqwarz8ilpp4aiya4s46gbx1fakg587niw7k")))

(define-public crate-epoll-4.1.0 (c (n "epoll") (v "4.1.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1186f57fl1fqk1y32q46pj6mc0hxyrqgkhv8vsg6d2gadgicy2wr")))

(define-public crate-epoll-4.2.0 (c (n "epoll") (v "4.2.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0q08mirx2dd8rwjsvfr0a0cd1fx7m7bcib2qblg9p3gz4sh0nnvy")))

(define-public crate-epoll-4.3.0 (c (n "epoll") (v "4.3.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "05fr1grpnlfax8glqyx9zll1qi35psb2brk9j92grdxv8dq7dz7j")))

(define-public crate-epoll-4.3.1 (c (n "epoll") (v "4.3.1") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1l0wa4vzbh7jsswx4mbrq89jjp912mmswvsdkphzf104f0y6kpr0")))

(define-public crate-epoll-4.3.2 (c (n "epoll") (v "4.3.2") (d (list (d (n "bitflags") (r "^2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1xff2jbimwvc91cwy1vmiai64laxmc713xjvz9plx5ifpq0vsdz8") (y #t)))

(define-public crate-epoll-4.3.3 (c (n "epoll") (v "4.3.3") (d (list (d (n "bitflags") (r "^2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0ygs486sbpxnhgrjy98knqf3ghla4qnh9q184v6zc7zaj8riqdbl")))

