(define-module (crates-io ep he ephemeral) #:use-module (crates-io))

(define-public crate-ephemeral-0.1.0 (c (n "ephemeral") (v "0.1.0") (h "0aqm4yw9y9aq6yz5ddv1hhx62v7yhglk4g1gpkrkb3abw2wkzxb1")))

(define-public crate-ephemeral-0.2.0 (c (n "ephemeral") (v "0.2.0") (h "158z7ykq9ylxm4acipkhfwngfwc330k7ypa8wc1a871a2ck3kzdx")))

(define-public crate-ephemeral-0.2.1 (c (n "ephemeral") (v "0.2.1") (h "11rmf7127hb9b7pf0zy7xlbsq9qvb13znwi0q3mbwhz29zyadkzy")))

