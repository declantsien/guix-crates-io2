(define-module (crates-io ep he ephemeropt) #:use-module (crates-io))

(define-public crate-ephemeropt-0.1.0 (c (n "ephemeropt") (v "0.1.0") (d (list (d (n "mock_instant") (r "^0.3.1") (d #t) (k 2)) (d (n "sysinfo") (r "^0.29.7") (k 2)) (d (n "tokio") (r "^1.30.0") (f (quote ("rt-multi-thread" "macros" "sync" "time"))) (d #t) (k 2)))) (h "0lbqaa5fiwdxx37fi6lvm0wq36jq4diq7fr36ik1b8r8wksxrw2r")))

(define-public crate-ephemeropt-0.2.0 (c (n "ephemeropt") (v "0.2.0") (d (list (d (n "mock_instant") (r "^0.3.1") (d #t) (k 2)) (d (n "sysinfo") (r "^0.29.7") (k 2)) (d (n "tokio") (r "^1.30.0") (f (quote ("rt-multi-thread" "macros" "sync" "time"))) (d #t) (k 2)))) (h "0bx1xifvspi80qqal1za3b30cd189n7lckfg8ji36hzw9ixijrli")))

(define-public crate-ephemeropt-0.2.1 (c (n "ephemeropt") (v "0.2.1") (d (list (d (n "mock_instant") (r "^0.3.1") (d #t) (k 2)) (d (n "sysinfo") (r "^0.29.7") (k 2)) (d (n "tokio") (r "^1.30.0") (f (quote ("rt-multi-thread" "macros" "sync" "time"))) (d #t) (k 2)))) (h "0h07bqh3469v1y3lskd3cff40pbrgasx5f9x2kdq5vdi3hl4qrx8")))

(define-public crate-ephemeropt-0.2.2 (c (n "ephemeropt") (v "0.2.2") (d (list (d (n "mock_instant") (r "^0.3.1") (d #t) (k 2)) (d (n "sysinfo") (r "^0.29.7") (k 2)) (d (n "tokio") (r "^1.30.0") (f (quote ("rt-multi-thread" "macros" "sync" "time"))) (d #t) (k 2)))) (h "1znv2217p2j9pvcd2d91fkhiv0fg34i7szjhc43mfqc7fcbaxcxl")))

(define-public crate-ephemeropt-0.3.0 (c (n "ephemeropt") (v "0.3.0") (d (list (d (n "mock_instant") (r "^0.3.1") (d #t) (k 2)) (d (n "sysinfo") (r "^0.29.7") (k 2)) (d (n "tokio") (r "^1.30.0") (f (quote ("rt-multi-thread" "macros" "sync" "time"))) (d #t) (k 2)))) (h "1naqb0hyaxk5g1nmzsq3fpvnparsmnqwiyirvha2vj90cykxq7ac")))

