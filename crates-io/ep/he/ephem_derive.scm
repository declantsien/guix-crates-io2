(define-module (crates-io ep he ephem_derive) #:use-module (crates-io))

(define-public crate-ephem_derive-0.1.0-alpha (c (n "ephem_derive") (v "0.1.0-alpha") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "05vpg3r7gpjrw84hg7lrb607825xn2lkk3aivyhsx6ixb6qmdhdf")))

