(define-module (crates-io ep im epimetheus) #:use-module (crates-io))

(define-public crate-epimetheus-0.1.0 (c (n "epimetheus") (v "0.1.0") (d (list (d (n "crossbeam-channel") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "mio") (r "^0.6.19") (d #t) (k 0)))) (h "1n197dzw5j37wq8g7illnbmlxwdhisc4mb99rywnamy554irkf57")))

(define-public crate-epimetheus-0.2.0 (c (n "epimetheus") (v "0.2.0") (d (list (d (n "crossbeam-channel") (r "^0.3") (d #t) (k 0)) (d (n "env_logger") (r "^0.6") (d #t) (k 2)) (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)))) (h "0jchdm96hr9fnma65hwpajpl0ils59j9mp8rnmf851vak0yji4qs")))

(define-public crate-epimetheus-0.2.1 (c (n "epimetheus") (v "0.2.1") (d (list (d (n "crossbeam-channel") (r "^0.4") (d #t) (k 0)) (d (n "env_logger") (r "^0.7") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "once_cell") (r "^1.2") (d #t) (k 0)))) (h "110c85afw75h8r8pbzkr91zbhd1kk5il7vny3cy9hr8qnhmdgpn1")))

(define-public crate-epimetheus-0.3.0 (c (n "epimetheus") (v "0.3.0") (d (list (d (n "crossbeam-channel") (r "^0.4") (d #t) (k 0)) (d (n "env_logger") (r "^0.7") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "once_cell") (r "^1.2") (d #t) (k 0)))) (h "190lp3wpsnpxrciqdsinhgib0d3137vc1pq1v57cmr6s77w7pasj")))

(define-public crate-epimetheus-0.3.1 (c (n "epimetheus") (v "0.3.1") (d (list (d (n "crossbeam-channel") (r "^0.4") (d #t) (k 0)) (d (n "env_logger") (r "^0.7") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "once_cell") (r "^1.2") (d #t) (k 0)))) (h "03n640jvyh8zmqykkzxd84i94bw6h1a9ya15l26mxfwip23ac74f")))

(define-public crate-epimetheus-0.4.0 (c (n "epimetheus") (v "0.4.0") (d (list (d (n "crossbeam-channel") (r "^0.4") (d #t) (k 0)) (d (n "env_logger") (r "^0.7") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mio") (r "^0.7") (f (quote ("tcp" "os-poll"))) (d #t) (k 0)) (d (n "once_cell") (r "^1.4") (d #t) (k 0)))) (h "0pfn8gxd453zj3vmqz8f79cw1xvcbfcjpgr08fh1qd1viwqgrag0")))

(define-public crate-epimetheus-0.5.0 (c (n "epimetheus") (v "0.5.0") (d (list (d (n "crossbeam-channel") (r "^0.4") (d #t) (k 0)) (d (n "env_logger") (r "^0.7") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.4") (d #t) (k 0)))) (h "0m1ba2ynfgxsh4rrkb0n2s1y2i5rwwy9zkyghlvgrzwg0cgizk8d")))

(define-public crate-epimetheus-0.6.0 (c (n "epimetheus") (v "0.6.0") (d (list (d (n "crossbeam-channel") (r "^0.4") (d #t) (k 0)) (d (n "env_logger") (r "^0.7") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.4") (d #t) (k 0)))) (h "1j941lrl83sz1lj919c2kr9kjwnxmmw73j4jrxmwbgymp3bb1v8w")))

(define-public crate-epimetheus-0.7.0 (c (n "epimetheus") (v "0.7.0") (d (list (d (n "crossbeam-channel") (r "^0.4") (d #t) (k 0)) (d (n "env_logger") (r "^0.7") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.4") (d #t) (k 0)))) (h "05fqnz5yb7dkf9917pjdc34blgjrc3wv0gnvn13d7c8ab06wnszz")))

(define-public crate-epimetheus-0.7.1 (c (n "epimetheus") (v "0.7.1") (d (list (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.8") (d #t) (k 0)))) (h "1mpwzlvwlpm9v8z2wiria80w2h6yjahfh7mh81cc8s33dbb3nsyz")))

(define-public crate-epimetheus-0.8.0 (c (n "epimetheus") (v "0.8.0") (d (list (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "once_cell") (r "^1.8") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 2)))) (h "1acm86iidwaa8d5vrhyzf0py7747amfabd097hcris95janx6101")))

