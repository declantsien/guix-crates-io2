(define-module (crates-io ep ir epir) #:use-module (crates-io))

(define-public crate-epir-0.0.1 (c (n "epir") (v "0.0.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "curve25519-dalek") (r "^3") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13") (d #t) (k 0)) (d (n "rand_core") (r "^0.5") (f (quote ("getrandom"))) (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)) (d (n "sha2") (r "^0.9") (d #t) (k 2)))) (h "0s7ly3sq001xgva5xk3pn0zaig1nvd2c3nwndvz3wychv2cbjbfh")))

