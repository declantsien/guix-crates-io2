(define-module (crates-io ep as epaste) #:use-module (crates-io))

(define-public crate-epaste-1.0.0 (c (n "epaste") (v "1.0.0") (d (list (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "sodiumoxide") (r "^0.0.10") (d #t) (k 0)))) (h "0cl2plinjk01kdzch9wwl23y738xmmwwrqy9lcr98hh8w45m8mnv")))

(define-public crate-epaste-1.0.1 (c (n "epaste") (v "1.0.1") (d (list (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "sodiumoxide") (r "^0.0.10") (d #t) (k 0)))) (h "00nk966nrdgjdkcqqnhlky6225kmxxjrp62hvz9nimjdfpa4rnsa")))

(define-public crate-epaste-1.0.2 (c (n "epaste") (v "1.0.2") (d (list (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "sodiumoxide") (r "^0.0.10") (d #t) (k 0)))) (h "0rm08ip5gylz6nwjnvvgp3s7vblgc0qddcjrar2n3ml3d9p56za8")))

(define-public crate-epaste-1.1.0 (c (n "epaste") (v "1.1.0") (d (list (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "sodiumoxide") (r "^0.0.15") (d #t) (k 0)))) (h "0m7gf34yyj55llks54lwi07csgl4p8wlx8yarxm30ks0fjv9kg09")))

(define-public crate-epaste-2.0.0 (c (n "epaste") (v "2.0.0") (d (list (d (n "assert_cmd") (r "^0.6") (d #t) (k 2)) (d (n "base64") (r "^0.9") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.7") (d #t) (k 2)) (d (n "rand") (r "^0.5") (d #t) (k 2)) (d (n "size") (r "^0.1") (d #t) (k 0)) (d (n "sodiumoxide") (r "^0.0.16") (d #t) (k 0)))) (h "0gffbmrv4bx8fvl69l45agzmwvg8idykl2rqwn7x7jxd78d0hf2c")))

