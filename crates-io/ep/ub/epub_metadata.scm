(define-module (crates-io ep ub epub_metadata) #:use-module (crates-io))

(define-public crate-epub_metadata-0.1.0 (c (n "epub_metadata") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0aw54i95lj0dwj5cmhr0nxclszgw1gjzp65irhikv1jxhwplnypf")))

