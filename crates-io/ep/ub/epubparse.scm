(define-module (crates-io ep ub epubparse) #:use-module (crates-io))

(define-public crate-epubparse-0.1.0 (c (n "epubparse") (v "0.1.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "xmltree") (r "^0.10.2") (d #t) (k 0)) (d (n "zip") (r "^0.5") (f (quote ("deflate"))) (k 0)))) (h "1q6in6jvrzn320b4dha2il2l3si2m5i467h857ilwf80h2aswvpa")))

(define-public crate-epubparse-0.2.0 (c (n "epubparse") (v "0.2.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "xmltree") (r "^0.10.2") (d #t) (k 0)) (d (n "zip") (r "^0.5") (f (quote ("deflate"))) (k 0)))) (h "0i9aslh0wrj14rlbdny182r1h17qmrx9wip543var4p5az09rb6f")))

(define-public crate-epubparse-0.2.1 (c (n "epubparse") (v "0.2.1") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "xmltree") (r "^0.10.2") (d #t) (k 0)) (d (n "zip") (r "^0.5") (f (quote ("deflate"))) (k 0)))) (h "08asqf9g2ria3irrfnbgcrmcy28lv1dzsf5vy3vq17cxllqwsm2d")))

(define-public crate-epubparse-0.2.2 (c (n "epubparse") (v "0.2.2") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 1)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "xmltree") (r "^0.10.3") (d #t) (k 0) (p "xmltree-parse_with_config")) (d (n "zip") (r "^0.5") (f (quote ("deflate"))) (k 0)))) (h "1fdqqvjp54ijgd2n9d4rb2iz6l5k6x8f399gbslfvmbh7x30s9dv")))

