(define-module (crates-io ep ub epub_bundler) #:use-module (crates-io))

(define-public crate-epub_bundler-0.1.0 (c (n "epub_bundler") (v "0.1.0") (d (list (d (n "bookbinder_common") (r "^0.1.0") (d #t) (k 0)) (d (n "epub_metadata") (r "^0.1.0") (d #t) (k 0)) (d (n "mime") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 0)) (d (n "temp_file_name") (r "^0.1.0") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)) (d (n "zip") (r "^0.5.4") (k 0)))) (h "19za26dc1z93bk6a4b3f54n1g4g5gcvsl5cys24qfd1hp203gd3r")))

(define-public crate-epub_bundler-0.1.1 (c (n "epub_bundler") (v "0.1.1") (d (list (d (n "bookbinder_common") (r "^0.1.1") (d #t) (k 0)) (d (n "epub_metadata") (r "^0.1.0") (d #t) (k 0)) (d (n "mime") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 0)) (d (n "temp_file_name") (r "^0.1.0") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)) (d (n "zip") (r "^0.5.4") (k 0)))) (h "152mbc1d0c4wmz41a5z5n4wph9lvyd8dprsh0ikb5hy7sk29hrqy")))

