(define-module (crates-io ep in epingm) #:use-module (crates-io))

(define-public crate-epingm-0.1.0 (c (n "epingm") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.34") (f (quote ("alloc" "std" "clock"))) (d #t) (k 0)) (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "oneshot") (r "^0.1.6") (f (quote ("std"))) (d #t) (k 0)) (d (n "pnet") (r "^0.34.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "013zh72mhvy1s7cjbjll10jy3k9xs3q8hh2pbv2mzg8ybb0dnfmd")))

(define-public crate-epingm-0.1.1 (c (n "epingm") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.34") (f (quote ("alloc" "std" "clock"))) (d #t) (k 0)) (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "oneshot") (r "^0.1.6") (f (quote ("std"))) (d #t) (k 0)) (d (n "pnet") (r "^0.34.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0m88xgssz93cr9c7czi1ry0wc2smjkvdm86ifbsk9p0arphqi88q")))

(define-public crate-epingm-0.2.0 (c (n "epingm") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.34") (f (quote ("alloc" "std" "clock"))) (d #t) (k 0)) (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "oneshot") (r "^0.1.6") (f (quote ("std"))) (d #t) (k 0)) (d (n "pnet") (r "^0.34.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "textplots") (r "^0.8.6") (d #t) (k 0)))) (h "0p2bsj2abkx2h9p3942dr5py4k8bbfw42lhqszi7l73hbwywzcrc")))

