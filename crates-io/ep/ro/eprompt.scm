(define-module (crates-io ep ro eprompt) #:use-module (crates-io))

(define-public crate-eprompt-0.1.0 (c (n "eprompt") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)))) (h "1ys2bmp0x8xlmm33ir9fgxcvxgfsxq7pmrdfpgz7hswr1q8brwgr")))

(define-public crate-eprompt-0.1.1 (c (n "eprompt") (v "0.1.1") (d (list (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)))) (h "10zfjbggzbq5ss5hrpzlcl9x98j7amsfc6ms159k2z7dn9fldfs1")))

(define-public crate-eprompt-0.1.2 (c (n "eprompt") (v "0.1.2") (d (list (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)))) (h "1rb331c875whqrd9pkg1cl4jwn4gsynmmsc2x80nc31dypq848r3")))

(define-public crate-eprompt-0.1.3 (c (n "eprompt") (v "0.1.3") (d (list (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)))) (h "0l60lrbwjxp9x63a5mz2cn6b7f2dcw9b03qh69wi8sv9nm369a68")))

(define-public crate-eprompt-0.2.0 (c (n "eprompt") (v "0.2.0") (d (list (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)))) (h "04945kx7xvnjvwcd487292ajwjdr9flsrbx4x3bj168dd5rn7acw")))

(define-public crate-eprompt-0.3.0 (c (n "eprompt") (v "0.3.0") (d (list (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)))) (h "003hcap8w71i2x55hhn7bzxmlvls16cqcr715ydkvfg1q0bj50qd")))

