(define-module (crates-io ep ce epcexplorer) #:use-module (crates-io))

(define-public crate-epcexplorer-0.1.0 (c (n "epcexplorer") (v "0.1.0") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "gs1") (r "^0.1.3") (d #t) (k 0)) (d (n "hex") (r "^0.3.1") (d #t) (k 0)) (d (n "ru5102") (r "^0.1.0") (d #t) (k 0)) (d (n "termion") (r "^1.5") (d #t) (k 0)) (d (n "tui") (r "^0.6.2") (d #t) (k 0)))) (h "0ymqx1ccq8z3ska8sgy0x2699a0vlr98s9skdxilsvy4s2gi05x6")))

(define-public crate-epcexplorer-0.1.1 (c (n "epcexplorer") (v "0.1.1") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "gs1") (r "^0.1.4") (d #t) (k 0)) (d (n "hex") (r "^0.3.1") (d #t) (k 0)) (d (n "ru5102") (r "^0.1.0") (d #t) (k 0)) (d (n "termion") (r "^1.5") (d #t) (k 0)) (d (n "tui") (r "^0.6.2") (d #t) (k 0)))) (h "05am6i2046yb8i3dk3q9sys4g5816sbrjb2v4zxjkfzgydidy1np")))

(define-public crate-epcexplorer-0.1.2 (c (n "epcexplorer") (v "0.1.2") (d (list (d (n "backtrace") (r "^0.3.38") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "flexi_logger") (r "^0.14.4") (d #t) (k 0)) (d (n "gs1") (r "^0.1.4") (d #t) (k 0)) (d (n "hex") (r "^0.3.1") (d #t) (k 0)) (d (n "invelion") (r "^0.1.2") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "ru5102") (r "^0.1.1") (d #t) (k 0)) (d (n "termion") (r "^1.5") (d #t) (k 0)) (d (n "tui") (r "^0.6.2") (d #t) (k 0)))) (h "1cp0ydfkpy5mwjsmkzhcqa3xlsidhb8ham1pw3plhdqw1lqxrgq2")))

