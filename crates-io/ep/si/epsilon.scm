(define-module (crates-io ep si epsilon) #:use-module (crates-io))

(define-public crate-epsilon-0.0.1 (c (n "epsilon") (v "0.0.1") (h "1c1qam29mrzcg05jcbqypv7z480ck1i7r17g8d2sagj09iph0afd")))

(define-public crate-epsilon-0.3.13 (c (n "epsilon") (v "0.3.13") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)))) (h "1yppgxw8rl3h6162lpq32yscl4jg0frjsaahsq30idzjhpmn8vfb")))

(define-public crate-epsilon-0.313.0 (c (n "epsilon") (v "0.313.0") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)))) (h "1bhwx0pwpz7ynqlf01c4jdwjgsnfgnxgwksrlsniwxm4picph73v")))

(define-public crate-epsilon-0.313.1 (c (n "epsilon") (v "0.313.1") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)))) (h "0p0j2j1xccdmpi36xfy2biwpkyyw4bvhx7xyj3w94i2ia3w1i6ih")))

(define-public crate-epsilon-0.313.2 (c (n "epsilon") (v "0.313.2") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)))) (h "1qqbihyrsmqq55dw396ssjvx10a3p83sj4xji5ili6f1x5zx75fj")))

(define-public crate-epsilon-0.313.3 (c (n "epsilon") (v "0.313.3") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)))) (h "1fgpk8sbriqpgm6qpnx8khab9sqi7bapvwbfl8fkvm3bbz98cg95")))

(define-public crate-epsilon-0.313.4 (c (n "epsilon") (v "0.313.4") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)))) (h "1csra5njbn0c9ramv6dzngdfmqpyms05dyhz58c7d2hrmv6nz5yh")))

