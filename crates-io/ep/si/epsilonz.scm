(define-module (crates-io ep si epsilonz) #:use-module (crates-io))

(define-public crate-epsilonz-0.0.1 (c (n "epsilonz") (v "0.0.1") (d (list (d (n "epsilonz_algebra") (r "*") (d #t) (k 0)) (d (n "fingertree") (r "*") (d #t) (k 0)) (d (n "free") (r "*") (d #t) (k 0)) (d (n "monad") (r "*") (d #t) (k 0)) (d (n "pipes") (r "*") (d #t) (k 0)) (d (n "pretty") (r "*") (d #t) (k 0)) (d (n "rope") (r "*") (d #t) (k 0)) (d (n "tailrec") (r "*") (d #t) (k 0)))) (h "1cvnivsd90ki76bnq253by68dl6da6km2svdzpba735sk780qmix")))

(define-public crate-epsilonz-0.0.2 (c (n "epsilonz") (v "0.0.2") (d (list (d (n "epsilonz_algebra") (r "*") (d #t) (k 0)) (d (n "fingertree") (r "*") (d #t) (k 0)) (d (n "free") (r "*") (d #t) (k 0)) (d (n "free_macros") (r "*") (d #t) (k 0)) (d (n "monad") (r "*") (d #t) (k 0)) (d (n "monad_macros") (r "*") (d #t) (k 0)) (d (n "morphism") (r "*") (d #t) (k 0)) (d (n "pipes") (r "*") (d #t) (k 0)) (d (n "pretty") (r "*") (d #t) (k 0)) (d (n "rope") (r "*") (d #t) (k 0)) (d (n "tailrec") (r "*") (d #t) (k 0)))) (h "12wgdsaa0fiim6zc7kh29pq94pxnwa4y1cs5prlcnjchivb2wb98")))

