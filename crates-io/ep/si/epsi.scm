(define-module (crates-io ep si epsi) #:use-module (crates-io))

(define-public crate-epsi-0.1.0 (c (n "epsi") (v "0.1.0") (h "0rzzp4f9nrqp5vqgsailzkic2xkkn04cyfsd5fv96xn7h77hvrpq") (r "1.56.1")))

(define-public crate-epsi-0.1.1 (c (n "epsi") (v "0.1.1") (h "0nwjvbkxnd9hxa7g415zhy0dj0mbkkpm1x5zxvybnqa7rzjpqyli") (r "1.56.1")))

