(define-module (crates-io ep p- epp-client-macros) #:use-module (crates-io))

(define-public crate-epp-client-macros-0.1.0 (c (n "epp-client-macros") (v "0.1.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1alj04a4dm8wzbwc2z9yrm65c9syzsihnh2b2kil9nwzjh1ycn27")))

