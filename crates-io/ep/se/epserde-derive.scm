(define-module (crates-io ep se epserde-derive) #:use-module (crates-io))

(define-public crate-epserde-derive-0.1.0 (c (n "epserde-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0xav5pv4d1wx79hrxs7kr96id219fj4dcl9zkbn6lq1fqknhasq3")))

(define-public crate-epserde-derive-0.1.1 (c (n "epserde-derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "14045c7agrc6b81x2wkmcywifxz4zjn213p5nrb3gm4aj50lzpmp")))

(define-public crate-epserde-derive-0.1.2 (c (n "epserde-derive") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0hydy5bdza9vajh4nmssncxaja7rzd4ww4qnk19kw0vjzadj5qm7")))

(define-public crate-epserde-derive-0.2.0 (c (n "epserde-derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0s6b89lawfkzm32xfz8lwngz1w47jqkr0in1xx9v17vvvg89ginw")))

(define-public crate-epserde-derive-0.2.1 (c (n "epserde-derive") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0rhmnqm3blrk5mwggd368xv7f0l0splxq17qq367wy4qyllafzcc")))

(define-public crate-epserde-derive-0.2.2 (c (n "epserde-derive") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1bzqc8g4y1nq2w2scx2478ddlc63hlwib7bf20i35ry0wqaa40ww")))

(define-public crate-epserde-derive-0.3.0 (c (n "epserde-derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0zcbi1lnjjyn88ql2h4g71ly3lq0r3s7fljd6rva6m93q00b65bp")))

(define-public crate-epserde-derive-0.3.1 (c (n "epserde-derive") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "07i1bh6m20jzzyg1hpbpp0d4aidl73cz603kaakpks5n0wnbwahg")))

