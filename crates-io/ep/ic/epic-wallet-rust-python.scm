(define-module (crates-io ep ic epic-wallet-rust-python) #:use-module (crates-io))

(define-public crate-epic-wallet-rust-python-0.1.0 (c (n "epic-wallet-rust-python") (v "0.1.0") (d (list (d (n "epic_wallet_rust_lib") (r "^0.2.0") (d #t) (k 0)) (d (n "pyo3") (r "^0.17.3") (f (quote ("extension-module"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1i9mqkmbzv9jj9mnb4vb7ga2h4ddywhvxvb82j8m3s5hrm38b5rw")))

(define-public crate-epic-wallet-rust-python-0.2.0 (c (n "epic-wallet-rust-python") (v "0.2.0") (d (list (d (n "epic_wallet_rust_lib") (r "^0.2.0") (d #t) (k 0)) (d (n "pyo3") (r "^0.17.3") (f (quote ("extension-module"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1wi2sdby1rqjpzkcm7nlci3kcsfwlld1qlbrm7nn4pfymg3nfm6j")))

