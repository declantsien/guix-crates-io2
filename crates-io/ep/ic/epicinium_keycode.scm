(define-module (crates-io ep ic epicinium_keycode) #:use-module (crates-io))

(define-public crate-epicinium_keycode-1.1.0 (c (n "epicinium_keycode") (v "1.1.0") (d (list (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1zr5gxn6db6xj8yfipjmpmhqj1008lc9qah15yw4gn7fzj9fc7jm") (f (quote (("defaults"))))))

(define-public crate-epicinium_keycode-1.2.0 (c (n "epicinium_keycode") (v "1.2.0") (d (list (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1bh5bi1rlfrhqlbdg5gryzcvwis93kxbp42l9v9bnmm8clmdf891") (f (quote (("defaults"))))))

