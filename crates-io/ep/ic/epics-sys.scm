(define-module (crates-io ep ic epics-sys) #:use-module (crates-io))

(define-public crate-epics-sys-0.0.1 (c (n "epics-sys") (v "0.0.1") (h "1cf7fpq02l176zp7mlgivx79p3dn9zsabwwswv25qgmnkbs5fhh7")))

(define-public crate-epics-sys-0.0.2 (c (n "epics-sys") (v "0.0.2") (d (list (d (n "paste") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1k4ll8ij1kalrk8sw2w2zswn51p004arni5wr71q42rjyvfnw7j4")))

(define-public crate-epics-sys-0.0.3 (c (n "epics-sys") (v "0.0.3") (d (list (d (n "paste") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "07qzayqjqbb1467v33ryncs5hamgs6wqvjw38xp8ndyxvavrc57j")))

(define-public crate-epics-sys-0.0.4 (c (n "epics-sys") (v "0.0.4") (d (list (d (n "paste") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "08x4sr46hrnxr7x2qh68fyc8swsnpbgg64x278vji3s2r861xbh3")))

(define-public crate-epics-sys-0.0.5 (c (n "epics-sys") (v "0.0.5") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0vmqklmqjfhh59pds3fzpppbkkwbc3c94kxlr2cx4kpbqn6pwm49")))

