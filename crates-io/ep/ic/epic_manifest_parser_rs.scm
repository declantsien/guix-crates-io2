(define-module (crates-io ep ic epic_manifest_parser_rs) #:use-module (crates-io))

(define-public crate-epic_manifest_parser_rs-0.1.5 (c (n "epic_manifest_parser_rs") (v "0.1.5") (d (list (d (n "flate2") (r "^1.0.28") (d #t) (k 0)) (d (n "sha1") (r "^0.10.6") (d #t) (k 0)) (d (n "widestring") (r "^1.0.2") (d #t) (k 0)))) (h "0hs63ilhmj637az06cbj4fhfw3dnlk9whlxmk4cklkybggf8xfvj")))

(define-public crate-epic_manifest_parser_rs-0.1.6 (c (n "epic_manifest_parser_rs") (v "0.1.6") (d (list (d (n "flate2") (r "^1.0.28") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sha1") (r "^0.10.6") (d #t) (k 0)) (d (n "widestring") (r "^1.0.2") (d #t) (k 0)))) (h "0bx6k4ar8ayndpkq7d4fdhibjdkf34d7hy7sism3za7gz3r516kn")))

(define-public crate-epic_manifest_parser_rs-0.1.7 (c (n "epic_manifest_parser_rs") (v "0.1.7") (d (list (d (n "flate2") (r "^1.0.28") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sha1") (r "^0.10.6") (d #t) (k 0)) (d (n "widestring") (r "^1.0.2") (d #t) (k 0)))) (h "0a1km5ak0ngmb9h3pwnkgzjval400rzjlvhgpzd24h8x0mm4m7in")))

