(define-module (crates-io ep ic epicenter) #:use-module (crates-io))

(define-public crate-epicenter-0.0.1 (c (n "epicenter") (v "0.0.1") (d (list (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)) (d (n "tokio") (r "^1.35.0") (f (quote ("sync"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.35.0") (f (quote ("macros" "rt" "rt-multi-thread"))) (d #t) (k 2)))) (h "1961jk9z4h1xcp7sjjl4vl5vnh6vhq4wlsmlbkpz4q1ggvaaf418") (f (quote (("default" "async")))) (s 2) (e (quote (("async" "dep:tokio"))))))

(define-public crate-epicenter-0.1.0 (c (n "epicenter") (v "0.1.0") (d (list (d (n "futures") (r "^0.3.29") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)) (d (n "tokio") (r "^1.35.0") (f (quote ("sync"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.35.0") (f (quote ("macros" "rt" "rt-multi-thread"))) (d #t) (k 2)))) (h "1iiks4y6y77bczbcppl4ahhwk6gc2rp4awqvhs0y394la00npsld") (f (quote (("default" "async")))) (s 2) (e (quote (("async" "dep:tokio" "dep:futures"))))))

