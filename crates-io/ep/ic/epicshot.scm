(define-module (crates-io ep ic epicshot) #:use-module (crates-io))

(define-public crate-epicshot-0.1.0 (c (n "epicshot") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "image") (r "^0.24.7") (d #t) (k 0)) (d (n "xcb") (r "^1.2.2") (f (quote ("randr"))) (d #t) (k 0)))) (h "06f36k462fqy9d8nibvh5454vk8y3kqsimsmhknq0g13v6sx8ml1") (r "1.74.0")))

(define-public crate-epicshot-0.1.1 (c (n "epicshot") (v "0.1.1") (d (list (d (n "clap") (r "^4.4.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "image") (r "^0.24.7") (d #t) (k 0)) (d (n "xcb") (r "^1.2.2") (f (quote ("randr"))) (d #t) (k 0)))) (h "1ghrnip4bh9qs8bbmibbadmw796zrx86bpimq77dl89dbih90cs6") (y #t) (r "1.74.0")))

(define-public crate-epicshot-0.1.2 (c (n "epicshot") (v "0.1.2") (d (list (d (n "clap") (r "^4.4.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "image") (r "^0.24.7") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.5.0") (d #t) (k 0)) (d (n "xcb") (r "^1.2.2") (f (quote ("randr"))) (d #t) (k 0)))) (h "1z62704a7d9skyd68ysr5gh7j764aasmrd6jhlhav74r7sdkgl12") (r "1.74.0")))

(define-public crate-epicshot-0.1.3 (c (n "epicshot") (v "0.1.3") (d (list (d (n "clap") (r "^4.4.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "image") (r "^0.24.7") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.5.0") (d #t) (k 0)) (d (n "xcb") (r "^1.2.2") (f (quote ("randr"))) (d #t) (k 0)))) (h "17hl3c4hjfc92lzpdpax349zwrl5ah4cf9v6sl4hchhwbc27ghg6") (r "1.74.0")))

