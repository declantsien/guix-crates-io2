(define-module (crates-io ep ic epic-mickey-lib-rs) #:use-module (crates-io))

(define-public crate-epic-mickey-lib-rs-0.1.1 (c (n "epic-mickey-lib-rs") (v "0.1.1") (d (list (d (n "flate2") (r "^1.0.28") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.116") (d #t) (k 0)))) (h "1q9dwir482q4f9g3m7m5qiymavkgjysdknsfn3mwbm4hw5m10kvl")))

(define-public crate-epic-mickey-lib-rs-0.1.2 (c (n "epic-mickey-lib-rs") (v "0.1.2") (d (list (d (n "flate2") (r "^1.0.28") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.116") (d #t) (k 0)))) (h "031icimbcp9fgw6zvqvnfwf75mcfsvk9gks7x3bhkl554fy4zjld")))

(define-public crate-epic-mickey-lib-rs-0.1.3 (c (n "epic-mickey-lib-rs") (v "0.1.3") (d (list (d (n "flate2") (r "^1.0.28") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.116") (d #t) (k 0)))) (h "0lwf3ycqcspwsj8hf26m0c455n258l92sxf02066vv6gqfn0kvsb")))

