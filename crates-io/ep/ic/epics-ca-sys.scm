(define-module (crates-io ep ic epics-ca-sys) #:use-module (crates-io))

(define-public crate-epics-ca-sys-0.1.0 (c (n "epics-ca-sys") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "065qlxaws1j5wdm8f89rjbym7g5d6620dnpjd9asi4dvcvki6vny") (f (quote (("test" "cc")))) (l "ca")))

(define-public crate-epics-ca-sys-0.1.1 (c (n "epics-ca-sys") (v "0.1.1") (d (list (d (n "cc") (r "^1.0") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1dvwn2r6s103cpgasf4j0gh01dh8ph0aj8ilklxmxy1pym2di5si") (f (quote (("test" "cc")))) (l "ca")))

