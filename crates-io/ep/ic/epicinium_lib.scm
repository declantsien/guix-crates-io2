(define-module (crates-io ep ic epicinium_lib) #:use-module (crates-io))

(define-public crate-epicinium_lib-0.35.0 (c (n "epicinium_lib") (v "0.35.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "09rnxz8yrki3cql2850kzlcybb1mb5xs6i3lnwfqafg7wfzyyv55") (f (quote (("version-is-dev") ("defaults") ("candidate"))))))

(define-public crate-epicinium_lib-0.35.1 (c (n "epicinium_lib") (v "0.35.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1ynpkl65mr6j0jf657gvmac8aaxa2zjjm0cikaml7s43w9lkzi3c") (f (quote (("version-is-dev") ("defaults") ("candidate"))))))

(define-public crate-epicinium_lib-1.0.0 (c (n "epicinium_lib") (v "1.0.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1ifk81402923wgdrp449844naqy8xxxr52wgw4gvhi61991w17vn") (f (quote (("version-is-dev") ("defaults") ("candidate"))))))

(define-public crate-epicinium_lib-1.0.3 (c (n "epicinium_lib") (v "1.0.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1icadapzkzffrz2k0vpd8kvasa8wcdzmc7s0ak7vcj48i3xsxqry") (f (quote (("version-is-dev") ("defaults") ("candidate"))))))

(define-public crate-epicinium_lib-1.0.4 (c (n "epicinium_lib") (v "1.0.4") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0wqzhvnxzbk13h5kq0q3afbxxnk0azijyhzmcd2w36a85z3qcij4") (f (quote (("version-is-dev") ("defaults") ("candidate"))))))

(define-public crate-epicinium_lib-1.0.8 (c (n "epicinium_lib") (v "1.0.8") (d (list (d (n "libc") (r ">=0.2.0, <0.3.0") (d #t) (k 0)) (d (n "log") (r ">=0.4.0, <0.5.0") (d #t) (k 0)) (d (n "serde") (r ">=1.0.0, <2.0.0") (d #t) (k 0)) (d (n "serde_derive") (r ">=1.0.0, <2.0.0") (d #t) (k 0)) (d (n "serde_json") (r ">=1.0.0, <2.0.0") (d #t) (k 0)))) (h "143ymwgahbphrc646848xrqv74wy6p0nljb2hz934b9hk808n4zd") (f (quote (("version-is-dev") ("defaults") ("candidate")))) (y #t)))

(define-public crate-epicinium_lib-1.0.9 (c (n "epicinium_lib") (v "1.0.9") (d (list (d (n "libc") (r ">=0.2.0, <0.3.0") (d #t) (k 0)) (d (n "log") (r ">=0.4.0, <0.5.0") (d #t) (k 0)) (d (n "serde") (r ">=1.0.0, <2.0.0") (d #t) (k 0)) (d (n "serde_derive") (r ">=1.0.0, <2.0.0") (d #t) (k 0)) (d (n "serde_json") (r ">=1.0.0, <2.0.0") (d #t) (k 0)))) (h "19jbbg5lbdkwrfm9c34ms83i926sf04dbw114i1mksq2r4yiyn49") (f (quote (("version-is-dev") ("defaults") ("candidate"))))))

(define-public crate-epicinium_lib-1.0.10 (c (n "epicinium_lib") (v "1.0.10") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1y6glkmirnlf84faz3l8wa95zqw0ibddnqfv1jh2i2dbjprf86cf") (f (quote (("version-is-dev") ("defaults") ("candidate"))))))

(define-public crate-epicinium_lib-1.0.11 (c (n "epicinium_lib") (v "1.0.11") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "03klhxzwnbcgl1ywykp3p75sl3d3vf7lwp1x8g2jr3ac08lvx35i") (f (quote (("version-is-dev") ("defaults") ("candidate"))))))

(define-public crate-epicinium_lib-1.0.12 (c (n "epicinium_lib") (v "1.0.12") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0m3hnppkhfap3g42r2wicfzkbnal8nvyiyj99hlr6fbiv4ywajd4") (f (quote (("version-is-dev") ("defaults") ("candidate"))))))

(define-public crate-epicinium_lib-1.1.0 (c (n "epicinium_lib") (v "1.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "07wsxlfz6qz4m26hp1zmyki5kqlq39wdfdz96kmf8ajpvnj4xj8i") (f (quote (("version-is-dev") ("defaults") ("candidate"))))))

