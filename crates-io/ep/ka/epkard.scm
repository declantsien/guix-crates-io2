(define-module (crates-io ep ka epkard) #:use-module (crates-io))

(define-public crate-epkard-0.1.1 (c (n "epkard") (v "0.1.1") (d (list (d (n "colored") (r "^1.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.89") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.89") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.8") (d #t) (k 0)) (d (n "terminal_size") (r "^0.1.8") (d #t) (k 0)))) (h "18dmx47qgzy3vbzkgpmqx74cjjnqzq1hkx22k2qiixrg0rczsil4")))

