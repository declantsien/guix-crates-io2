(define-module (crates-io zc hu zchunk) #:use-module (crates-io))

(define-public crate-zchunk-0.1.0 (c (n "zchunk") (v "0.1.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)) (d (n "tempfile") (r "^3.8.1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)) (d (n "zstd") (r "^0.13.0") (d #t) (k 0)))) (h "0g7hq3dvk5hy1fkyn1cg9l5xah1dmhh4yd3w3gwhap1g5n8xaigz")))

(define-public crate-zchunk-0.2.0 (c (n "zchunk") (v "0.2.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)) (d (n "tempfile") (r "^3.8.1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)) (d (n "zstd") (r "^0.13.0") (d #t) (k 0)))) (h "0nzi0z10hakcnackmwn37w3wqzznx0i9s15r0kx045dha6zv0x1v")))

