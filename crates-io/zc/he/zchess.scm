(define-module (crates-io zc he zchess) #:use-module (crates-io))

(define-public crate-zchess-0.1.0 (c (n "zchess") (v "0.1.0") (d (list (d (n "bitflags") (r "^2.3.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.43") (d #t) (k 0)))) (h "0rqk70s5c9ds2drf6swswsykcnrd1pfcyl2hhsql8j49mzfrgbg9")))

