(define-module (crates-io zc _i zc_io) #:use-module (crates-io))

(define-public crate-zc_io-0.1.0 (c (n "zc_io") (v "0.1.0") (h "0q0qmf37nf6lzbq0am3xlz1yy31wkf9c3fqxdv83h3dd05mvmmvf") (f (quote (("std") ("default" "std"))))))

(define-public crate-zc_io-0.1.1 (c (n "zc_io") (v "0.1.1") (h "15b37lrjl0smmynm3cshj715rgnv7lprzn10f233ib9yqacjvqsd") (f (quote (("std") ("default" "std"))))))

(define-public crate-zc_io-0.2.0 (c (n "zc_io") (v "0.2.0") (h "0g6l70j90cs4zmbx2kji17z9ijcvwsrfr33g8g12xhly0svf54qf") (f (quote (("std") ("default" "std"))))))

