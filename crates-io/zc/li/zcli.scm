(define-module (crates-io zc li zcli) #:use-module (crates-io))

(define-public crate-zcli-0.1.0 (c (n "zcli") (v "0.1.0") (d (list (d (n "clone") (r "^0.1.2") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "command-macros") (r "^0.2.9") (d #t) (k 0)) (d (n "ifaces") (r "^0.1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1f4d9vlirvq30k9a5c31grfjgxjl5hwgc6x4ahnvnss3jn1rn2y8") (y #t)))

(define-public crate-zcli-0.1.1 (c (n "zcli") (v "0.1.1") (d (list (d (n "clone") (r "^0.1.2") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "command-macros") (r "^0.2.9") (d #t) (k 0)) (d (n "html5ever") (r "^0.22.5") (d #t) (k 0)) (d (n "ifaces") (r "^0.1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "soup") (r "^0.5.1") (d #t) (k 0)))) (h "0qifmm9b0aq07bv5aqpls1k48y5n3a3k6kx1asr99va76qg5gzqk") (y #t)))

