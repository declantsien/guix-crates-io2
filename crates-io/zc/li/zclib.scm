(define-module (crates-io zc li zclib) #:use-module (crates-io))

(define-public crate-zclib-0.1.0 (c (n "zclib") (v "0.1.0") (d (list (d (n "assert_cmd") (r "^1") (d #t) (k 2)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "predicates") (r "^1") (d #t) (k 2)))) (h "1m8q63i9nzqm28j24r3w3265s68wdc8dkgazcv0pm8c6xqgifshf")))

(define-public crate-zclib-0.1.1 (c (n "zclib") (v "0.1.1") (d (list (d (n "assert_cmd") (r "^1") (d #t) (k 2)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "predicates") (r "^1") (d #t) (k 2)))) (h "0lzlgxy2jarx7m0qn2wd0gyqmwk004j6625i90bxb293zkhmy128")))

