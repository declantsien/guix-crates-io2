(define-module (crates-io zc on zconvertenumn) #:use-module (crates-io))

(define-public crate-zconvertenumn-1.0.0 (c (n "zconvertenumn") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1.0.74") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.46") (d #t) (k 0)))) (h "02adjympk28dg2j7ggr36n4i2n8masdr1wni4bakn57pyhb63ra2") (r "1.56")))

(define-public crate-zconvertenumn-1.0.1 (c (n "zconvertenumn") (v "1.0.1") (d (list (d (n "proc-macro2") (r "^1.0.74") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.46") (d #t) (k 0)))) (h "04hlz9x8v9wg36fwj13gb50vilspz9f1j6bmiq0yid0aa7cnlx32") (r "1.56")))

(define-public crate-zconvertenumn-1.0.2 (c (n "zconvertenumn") (v "1.0.2") (d (list (d (n "enumnvictorhowisers") (r "^0.1.13") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.74") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.46") (d #t) (k 0)))) (h "1mp4af659sy9n0kx2y72zjr6k9g754r21d46zb62nswf8amf7hvh") (r "1.56")))

