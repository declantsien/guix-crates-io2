(define-module (crates-io zc as zcash_history) #:use-module (crates-io))

(define-public crate-zcash_history-0.0.1 (c (n "zcash_history") (v "0.0.1") (d (list (d (n "assert_matches") (r "^1.3.0") (d #t) (k 2)) (d (n "bigint") (r "^4") (d #t) (k 0)) (d (n "blake2") (r "^0.5") (d #t) (k 0) (p "blake2b_simd")) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.8") (d #t) (k 2)))) (h "0j0761wkz7mnc0rzzdsmqk27pjg4r7r52d67qhcj4vh1pmwap002")))

(define-public crate-zcash_history-0.2.0 (c (n "zcash_history") (v "0.2.0") (d (list (d (n "assert_matches") (r "^1.3.0") (d #t) (k 2)) (d (n "bigint") (r "^4") (d #t) (k 0)) (d (n "blake2") (r "^0.5") (d #t) (k 0) (p "blake2b_simd")) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.8") (d #t) (k 2)))) (h "1x6jrhkddfq0hjdrjnz2mfs0skbiq5kammcq62zln0dsrjdapyxb")))

(define-public crate-zcash_history-0.3.0 (c (n "zcash_history") (v "0.3.0") (d (list (d (n "assert_matches") (r "^1.3.0") (d #t) (k 2)) (d (n "blake2") (r "^1") (d #t) (k 0) (p "blake2b_simd")) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "primitive-types") (r "^0.11") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)))) (h "0yb65dcbzi5jxypmsd7nv9kvfydjsr246bvixqawffp1lhl1lqgb")))

(define-public crate-zcash_history-0.4.0 (c (n "zcash_history") (v "0.4.0") (d (list (d (n "assert_matches") (r "^1.5") (d #t) (k 2)) (d (n "blake2b_simd") (r "^1") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "primitive-types") (r "^0.12") (k 0)) (d (n "proptest") (r "^1") (o #t) (d #t) (k 0)) (d (n "proptest") (r "^1") (d #t) (k 2)))) (h "0vd725y3l72ps1lzqnqvcvbmf0l82kd30driddsrqbvrafzigpig") (s 2) (e (quote (("test-dependencies" "dep:proptest")))) (r "1.65")))

