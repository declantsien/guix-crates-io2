(define-module (crates-io zc as zcash_lightwallet_proto) #:use-module (crates-io))

(define-public crate-zcash_lightwallet_proto-0.1.0 (c (n "zcash_lightwallet_proto") (v "0.1.0") (d (list (d (n "protobuf") (r "^2.28.0") (d #t) (k 0)) (d (n "protobuf-codegen-pure") (r "^2.28.0") (d #t) (k 1)))) (h "1kym08fv5nhpdq23d228y0gs33p9cb8839pbyrn4c3d6n5991n3f")))

(define-public crate-zcash_lightwallet_proto-0.1.1 (c (n "zcash_lightwallet_proto") (v "0.1.1") (d (list (d (n "protobuf") (r "^2.28.0") (d #t) (k 0)) (d (n "protobuf-codegen-pure") (r "^2.28.0") (d #t) (k 1)))) (h "1nhvk8nrqnlmih1mxcs8qn1ymg4m8bj2fxzx9xficzycvx21zk61")))

