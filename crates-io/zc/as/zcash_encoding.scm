(define-module (crates-io zc as zcash_encoding) #:use-module (crates-io))

(define-public crate-zcash_encoding-0.0.0 (c (n "zcash_encoding") (v "0.0.0") (h "1cc43fgl8687cddgrsvd2008ghiqwnmljva0031fxxrwibwgd4jn")))

(define-public crate-zcash_encoding-0.1.0 (c (n "zcash_encoding") (v "0.1.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "nonempty") (r "^0.7") (d #t) (k 0)))) (h "0447ivf2smmilkv7ygavb5w4kp8ijjlmv3h6q85bqfdmisl1xdlz")))

(define-public crate-zcash_encoding-0.2.0 (c (n "zcash_encoding") (v "0.2.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "nonempty") (r "^0.7") (d #t) (k 0)))) (h "1h5aql09pyj83h9dqr93m7xvc8ihi0h1lrn0dbx5x1r72yw92czh") (r "1.56.1")))

