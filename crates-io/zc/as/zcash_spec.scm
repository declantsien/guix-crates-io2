(define-module (crates-io zc as zcash_spec) #:use-module (crates-io))

(define-public crate-zcash_spec-0.0.0 (c (n "zcash_spec") (v "0.0.0") (h "1jdadginbxb844fkl6bjacqh9xqblc0m2lhf49a0gmpw6ysrphs9") (r "1.56")))

(define-public crate-zcash_spec-0.1.0 (c (n "zcash_spec") (v "0.1.0") (d (list (d (n "blake2b_simd") (r "^1") (d #t) (k 0)))) (h "16wyjhw8i3lx4gb71nmhg8ckm4lqb4sbl2dfv2n3vjvknrcbz8xp") (r "1.56")))

