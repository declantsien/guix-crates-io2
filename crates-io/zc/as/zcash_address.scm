(define-module (crates-io zc as zcash_address) #:use-module (crates-io))

(define-public crate-zcash_address-0.0.0 (c (n "zcash_address") (v "0.0.0") (h "0221ndwjmwr4db8nchsfl53990p5wvlp5c3i3z16ppxsvpidci5y")))

(define-public crate-zcash_address-0.1.0 (c (n "zcash_address") (v "0.1.0") (d (list (d (n "assert_matches") (r "^1.3.0") (d #t) (k 2)) (d (n "bech32") (r "^0.8") (d #t) (k 0)) (d (n "bs58") (r "^0.4") (f (quote ("check"))) (d #t) (k 0)) (d (n "f4jumble") (r "^0.1") (d #t) (k 0)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "zcash_encoding") (r "^0.1") (d #t) (k 0)))) (h "00n65h8yj71x9m6c1fik0lyzik66qmfsii0c25zhiw2pnwqjlcni")))

(define-public crate-zcash_address-0.2.0 (c (n "zcash_address") (v "0.2.0") (d (list (d (n "assert_matches") (r "^1.3.0") (d #t) (k 2)) (d (n "bech32") (r "^0.8") (d #t) (k 0)) (d (n "bs58") (r "^0.4") (f (quote ("check"))) (d #t) (k 0)) (d (n "f4jumble") (r "^0.1") (d #t) (k 0)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "zcash_encoding") (r "^0.2") (d #t) (k 0)))) (h "1xzrdc30jgp6kd37csdsaypg9p8admw85kp2sc4zqr5n0bknhhl0") (f (quote (("test-dependencies")))) (r "1.52")))

(define-public crate-zcash_address-0.2.1 (c (n "zcash_address") (v "0.2.1") (d (list (d (n "assert_matches") (r "^1.3.0") (d #t) (k 2)) (d (n "bech32") (r "^0.9") (d #t) (k 0)) (d (n "bs58") (r "^0.4") (f (quote ("check"))) (d #t) (k 0)) (d (n "f4jumble") (r "^0.1") (d #t) (k 0)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "zcash_encoding") (r "^0.2") (d #t) (k 0)))) (h "1cpjhdc413w5pda0w7mgx3pxibzdkyfgysv4g01li79n0ni3bgjj") (f (quote (("test-dependencies")))) (r "1.52")))

(define-public crate-zcash_address-0.3.0 (c (n "zcash_address") (v "0.3.0") (d (list (d (n "assert_matches") (r "^1.3.0") (d #t) (k 2)) (d (n "bech32") (r "^0.9") (d #t) (k 0)) (d (n "bs58") (r "^0.5") (f (quote ("check"))) (d #t) (k 0)) (d (n "f4jumble") (r "^0.1") (d #t) (k 0)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "zcash_encoding") (r "^0.2") (d #t) (k 0)))) (h "1mgq5mvca3iynwwacdcdajc1nl1532762m5d41qf7wkc41fayi49") (f (quote (("test-dependencies")))) (r "1.52")))

(define-public crate-zcash_address-0.3.1 (c (n "zcash_address") (v "0.3.1") (d (list (d (n "assert_matches") (r "^1.3.0") (d #t) (k 2)) (d (n "bech32") (r "^0.9") (d #t) (k 0)) (d (n "bs58") (r "^0.5") (f (quote ("check"))) (d #t) (k 0)) (d (n "f4jumble") (r "^0.1") (d #t) (k 0)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "zcash_encoding") (r "^0.2") (d #t) (k 0)))) (h "1x4f4bwf61349mc2i690vjkffc8m61rsihqb65p80kzdv7qp7qdw") (f (quote (("test-dependencies")))) (r "1.52")))

(define-public crate-zcash_address-0.3.2 (c (n "zcash_address") (v "0.3.2") (d (list (d (n "assert_matches") (r "^1.3.0") (d #t) (k 2)) (d (n "bech32") (r "^0.9") (d #t) (k 0)) (d (n "bs58") (r "^0.5") (f (quote ("check"))) (d #t) (k 0)) (d (n "f4jumble") (r "^0.1") (d #t) (k 0)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "zcash_encoding") (r "^0.2") (d #t) (k 0)) (d (n "zcash_protocol") (r "^0.1") (d #t) (k 0)))) (h "1s81xrzsdlgxbwrc57fwmwl2513s1ihzz4g98h6rz9p3yyhifz42") (f (quote (("test-dependencies")))) (r "1.52")))

