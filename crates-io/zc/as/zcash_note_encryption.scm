(define-module (crates-io zc as zcash_note_encryption) #:use-module (crates-io))

(define-public crate-zcash_note_encryption-0.0.0 (c (n "zcash_note_encryption") (v "0.0.0") (h "0bbdmc9ckcfmng1q3npcnwnvg5rx3rjd6zz7zkbjf6c547bp1ghf")))

(define-public crate-zcash_note_encryption-0.1.0 (c (n "zcash_note_encryption") (v "0.1.0") (d (list (d (n "chacha20") (r "^0.8") (k 0)) (d (n "chacha20poly1305") (r "^0.9") (k 0)) (d (n "ff") (r "^0.11") (k 2)) (d (n "jubjub") (r "^0.8") (d #t) (k 2)) (d (n "rand_core") (r "^0.6") (k 0)) (d (n "subtle") (r "^2.2.3") (k 0)))) (h "0fjf6i66l5inn2fiv1x0afaysibhnw37yll29k3qlnph73jlmy1k") (f (quote (("pre-zip-212") ("default" "alloc") ("alloc"))))))

(define-public crate-zcash_note_encryption-0.2.0 (c (n "zcash_note_encryption") (v "0.2.0") (d (list (d (n "chacha20") (r "^0.9") (k 0)) (d (n "chacha20poly1305") (r "^0.10") (k 0)) (d (n "cipher") (r "^0.4") (k 0)) (d (n "rand_core") (r "^0.6") (k 0)) (d (n "subtle") (r "^2.3") (k 0)))) (h "0ry87sgzgidx0x3mi3xl4gggl5vcjfvqy1mphqvx17rq68jw3s9b") (f (quote (("pre-zip-212") ("default" "alloc") ("alloc")))) (r "1.56.1")))

(define-public crate-zcash_note_encryption-0.3.0 (c (n "zcash_note_encryption") (v "0.3.0") (d (list (d (n "chacha20") (r "^0.9") (k 0)) (d (n "chacha20poly1305") (r "^0.10") (k 0)) (d (n "cipher") (r "^0.4") (k 0)) (d (n "rand_core") (r "^0.6") (k 0)) (d (n "subtle") (r "^2.3") (k 0)))) (h "065k5px6lqwbyd5fhzrgc1a5bhx82lbn0z5qqlvfxyymdjg19cif") (f (quote (("pre-zip-212") ("default" "alloc") ("alloc")))) (r "1.56.1")))

(define-public crate-zcash_note_encryption-0.4.0 (c (n "zcash_note_encryption") (v "0.4.0") (d (list (d (n "chacha20") (r "^0.9") (k 0)) (d (n "chacha20poly1305") (r "^0.10") (k 0)) (d (n "cipher") (r "^0.4") (k 0)) (d (n "rand_core") (r "^0.6") (k 0)) (d (n "subtle") (r "^2.3") (k 0)))) (h "1if1i6n71ynxwqsdnsw1a0b7j8wdprlk3i6s452f84pfdk6q0iav") (f (quote (("pre-zip-212") ("default" "alloc") ("alloc")))) (r "1.56.1")))

