(define-module (crates-io zc as zcash_protocol) #:use-module (crates-io))

(define-public crate-zcash_protocol-0.0.0 (c (n "zcash_protocol") (v "0.0.0") (h "09aaz79dn1z3whb5z8hfbnz4zdz2hbflw9mrpvb9zjv538lb21n3") (r "1.65")))

(define-public crate-zcash_protocol-0.1.0 (c (n "zcash_protocol") (v "0.1.0") (d (list (d (n "document-features") (r "^0.2") (d #t) (k 0)) (d (n "incrementalmerkletree") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "memuse") (r "^0.2.1") (d #t) (k 0)) (d (n "proptest") (r "^1") (o #t) (d #t) (k 0)) (d (n "proptest") (r "^1") (d #t) (k 2)))) (h "1gvmllls7qcrnxaay2bdh1ik19rsrn6i0y0i47jx371ibpwzjlf6") (f (quote (("zfuture") ("unstable-nu6") ("local-consensus")))) (s 2) (e (quote (("test-dependencies" "dep:incrementalmerkletree" "dep:proptest" "incrementalmerkletree?/test-dependencies")))) (r "1.65")))

(define-public crate-zcash_protocol-0.1.1 (c (n "zcash_protocol") (v "0.1.1") (d (list (d (n "document-features") (r "^0.2") (d #t) (k 0)) (d (n "incrementalmerkletree") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "memuse") (r "^0.2.1") (d #t) (k 0)) (d (n "proptest") (r "^1") (o #t) (d #t) (k 0)) (d (n "proptest") (r "^1") (d #t) (k 2)))) (h "1g6srl39xrw4d8qj7mhw9g40vfvl72gyhx9vxwxams04lga8k0cg") (f (quote (("local-consensus")))) (s 2) (e (quote (("test-dependencies" "dep:incrementalmerkletree" "dep:proptest" "incrementalmerkletree?/test-dependencies")))) (r "1.65")))

