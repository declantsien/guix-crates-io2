(define-module (crates-io zc as zcash_client) #:use-module (crates-io))

(define-public crate-zcash_client-0.0.1 (c (n "zcash_client") (v "0.0.1") (d (list (d (n "assert_cmd") (r "^0.10") (d #t) (k 2)) (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "ra_common") (r "^0.0.43") (d #t) (k 0)) (d (n "simple_logger") (r "^1.6.0") (d #t) (k 0)))) (h "0fv2vaqh6q79z7vi8qrwy3f1pdi1k41xmwllr5j8d98zs0rqi0kq")))

