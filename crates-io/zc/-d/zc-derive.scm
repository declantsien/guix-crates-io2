(define-module (crates-io zc -d zc-derive) #:use-module (crates-io))

(define-public crate-zc-derive-0.1.0 (c (n "zc-derive") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "14fjl9lyyvda5jgbpgjsi582j3klxh27d4rgds76rcdrqqp3j363")))

(define-public crate-zc-derive-0.2.0 (c (n "zc-derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0hindy45p5nmcbvyilh0iwn19zbwjyak0lf02gq5v17dqdad51yc")))

(define-public crate-zc-derive-0.3.0 (c (n "zc-derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1znwi617pkxxcjgmy1qs1v80fwdfd6hvi3fzadj3pzqrhlxpnjdp")))

(define-public crate-zc-derive-0.4.0 (c (n "zc-derive") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0") (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "14l4ibiw3kgxc652ah6l91671q2ji5d5ffkxzmiwrfb7y4jwplax")))

