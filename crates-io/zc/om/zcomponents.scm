(define-module (crates-io zc om zcomponents) #:use-module (crates-io))

(define-public crate-zcomponents-0.1.0 (c (n "zcomponents") (v "0.1.0") (h "198rz0d336cvf52m1wqvhr0z0adgq7i1p0zr7n2ax33r71mxl5cy")))

(define-public crate-zcomponents-0.1.1 (c (n "zcomponents") (v "0.1.1") (h "1866nvsxfyzggc3fgsz7s1x1wbydkzj5mahry4ym9r0p00jcpi7c")))

(define-public crate-zcomponents-0.2.0 (c (n "zcomponents") (v "0.2.0") (h "1h6v0fp8hc154mncacv2z40yxwrqik8816fqcn1yrxmndyh3n1xf")))

