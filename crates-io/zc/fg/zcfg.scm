(define-module (crates-io zc fg zcfg) #:use-module (crates-io))

(define-public crate-zcfg-0.1.0 (c (n "zcfg") (v "0.1.0") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)))) (h "09pghy51vk1hfhzcy4zdz26kx4ff3a95m6w9skhdg399sjm5rrsa")))

(define-public crate-zcfg-0.2.0 (c (n "zcfg") (v "0.2.0") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)))) (h "0pbndcrbaivg8gznvx3rl9dqwby8am5mk3p7idqj1xl2bi7iv94s")))

