(define-module (crates-io zc ac zcache) #:use-module (crates-io))

(define-public crate-zcache-0.0.1 (c (n "zcache") (v "0.0.1") (d (list (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "006wzf4sw73977wqdql4nny7xi5jwz0kq265gk8bbdgnjcgksydb")))

(define-public crate-zcache-0.0.2 (c (n "zcache") (v "0.0.2") (d (list (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "0r45cpv948mxlr6m4llv9bki3m0cb994kcsa60rd16wi2r9ah0k0")))

(define-public crate-zcache-0.0.3 (c (n "zcache") (v "0.0.3") (d (list (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "131l57ci313k31ixz0ncnx7kgyz3mig6xr4ar6f3x6yg0y2v4vrd")))

(define-public crate-zcache-0.0.4 (c (n "zcache") (v "0.0.4") (d (list (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "0l6b5py3ds8cv7i5qjl571hg9m363c0a2z5zhkc4mdragj0r2raz") (y #t)))

(define-public crate-zcache-0.0.5 (c (n "zcache") (v "0.0.5") (d (list (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1wckns426jwfa8036h9z9bhp6w4rdb7hm878wrylp1idadrs69lf")))

(define-public crate-zcache-0.0.6 (c (n "zcache") (v "0.0.6") (d (list (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1m1n5l12p410zr60jiqkkc0v4c9c5j71rk6clqx3hiix5cyqgcdw")))

(define-public crate-zcache-0.0.7 (c (n "zcache") (v "0.0.7") (d (list (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1wz8cn5393gjy9hmmfdrvxqz94c8g6b62lpwb6bj179q5175d98l")))

(define-public crate-zcache-0.0.8 (c (n "zcache") (v "0.0.8") (d (list (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "0dkw0afv3q19fvs73rm6bradwb2j394irzhanc323pk7m0zxdvxb")))

