(define-module (crates-io sx #{15}# sx1509) #:use-module (crates-io))

(define-public crate-sx1509-0.1.0 (c (n "sx1509") (v "0.1.0") (d (list (d (n "embedded-hal") (r "~0.1") (d #t) (k 0)))) (h "081mqjk6yhx69lbfsqvgrinpb1g547rji5r7bk4d6wvp4jwqb90v")))

(define-public crate-sx1509-0.2.0 (c (n "sx1509") (v "0.2.0") (d (list (d (n "embedded-hal") (r "~0.2") (d #t) (k 0)))) (h "0jgncbpfdnlw03vnvj1kjddjk1haarmlcrhv5gvlm43lp6kpi43h")))

