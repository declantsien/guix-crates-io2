(define-module (crates-io sx #{12}# sx127x_lora) #:use-module (crates-io))

(define-public crate-sx127x_lora-0.1.0 (c (n "sx127x_lora") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "rppal") (r "^0.8") (d #t) (k 0)) (d (n "spidev") (r "^0.3.0") (d #t) (k 0)))) (h "0aindbdyqfwkk04dgnxcx7fcbmrbv8zvjn3dzf3b3y2s303i50pf")))

(define-public crate-sx127x_lora-0.1.1 (c (n "sx127x_lora") (v "0.1.1") (d (list (d (n "rppal") (r "^0.8") (d #t) (k 0)) (d (n "spidev") (r "^0.3.0") (d #t) (k 0)))) (h "1p5dsmry835m53pjl60phpmxybdbrqdxs08gkzri65dy7mxrhcg1")))

(define-public crate-sx127x_lora-0.1.2 (c (n "sx127x_lora") (v "0.1.2") (d (list (d (n "rppal") (r "^0.8") (d #t) (k 0)) (d (n "spidev") (r "^0.3.0") (d #t) (k 0)))) (h "152x23gvf3zrpsr2jdlgjdqma2a6n4pjz31dz50ylnihxvrsq6qj")))

(define-public crate-sx127x_lora-0.2.0 (c (n "sx127x_lora") (v "0.2.0") (d (list (d (n "bit_field") (r "^0.9.0") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.2") (d #t) (k 0)))) (h "1jhq4w7rgbw1prqgwgscdw5n2bgsjwlhrln4dma06flnc3ncmyl1")))

(define-public crate-sx127x_lora-0.3.0 (c (n "sx127x_lora") (v "0.3.0") (d (list (d (n "bit_field") (r "^0.9.0") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.2") (d #t) (k 0)))) (h "14qn1xxazjmsrvmqiyrnaf1vn5spk27vgn6m4w91n7zd7qwmjifg")))

(define-public crate-sx127x_lora-0.3.1 (c (n "sx127x_lora") (v "0.3.1") (d (list (d (n "bit_field") (r "^0.9.0") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.2") (d #t) (k 0)))) (h "15j8579ghj6fq9r64j88h0zs65wgrivi460djgrm1kwa0j2nzm8v")))

