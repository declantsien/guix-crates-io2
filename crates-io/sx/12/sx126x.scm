(define-module (crates-io sx #{12}# sx126x) #:use-module (crates-io))

(define-public crate-sx126x-0.1.0 (c (n "sx126x") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.32") (k 0)) (d (n "cortex-m") (r "^0.6.2") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.12") (d #t) (k 2)) (d (n "cortex-m-semihosting") (r "^0.3.5") (d #t) (k 2)) (d (n "embedded-hal") (r "^0.2.3") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "nb") (r "^0.1.2") (d #t) (k 2)) (d (n "panic-semihosting") (r "^0.5.3") (d #t) (k 2)) (d (n "stm32f1xx-hal") (r "^0.6.0") (f (quote ("stm32f103" "rt" "medium"))) (d #t) (k 2)))) (h "13i7zckdp5bv4pcbm6zrsg3v13qabb41ggn1nmz0pii37z9fs6rd")))

(define-public crate-sx126x-0.1.1 (c (n "sx126x") (v "0.1.1") (d (list (d (n "cortex-m") (r "^0.6.2") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.12") (d #t) (k 2)) (d (n "cortex-m-semihosting") (r "^0.3.5") (d #t) (k 2)) (d (n "embedded-hal") (r "^0.2.3") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "nb") (r "^0.1.2") (d #t) (k 2)) (d (n "panic-semihosting") (r "^0.5.3") (d #t) (k 2)) (d (n "stm32f1xx-hal") (r "^0.6.0") (f (quote ("stm32f103" "rt" "medium"))) (d #t) (k 2)))) (h "1zxrm18a54aflgl0rc9fxlmrmf56smflq4x4av5gjsckpzj4sq89")))

