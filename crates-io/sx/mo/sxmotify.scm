(define-module (crates-io sx mo sxmotify) #:use-module (crates-io))

(define-public crate-sxmotify-0.1.0 (c (n "sxmotify") (v "0.1.0") (d (list (d (n "directories") (r "~4.0") (d #t) (k 0)))) (h "0c4yc6v3lcsyfa0zp11dm7x4ggla89hb5kdmrqb2sxm2vkbs7z93")))

(define-public crate-sxmotify-0.1.1 (c (n "sxmotify") (v "0.1.1") (d (list (d (n "directories") (r "~4.0") (d #t) (k 0)))) (h "0faqr7bha72wld5vidcx2ik439h8qxijxx8762ngfn1bpwr443hk")))

