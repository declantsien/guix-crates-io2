(define-module (crates-io sx #{13}# sx13xx-conf) #:use-module (crates-io))

(define-public crate-sx13xx-conf-0.1.0 (c (n "sx13xx-conf") (v "0.1.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0h89hgxwrxcdknyrhyjd8xg0a97kn1k6c8j0571s2an5gfjdhv0z")))

