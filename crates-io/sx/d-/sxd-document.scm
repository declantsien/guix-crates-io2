(define-module (crates-io sx d- sxd-document) #:use-module (crates-io))

(define-public crate-sxd-document-0.1.0 (c (n "sxd-document") (v "0.1.0") (d (list (d (n "peresil") (r "^0.2.0") (d #t) (k 0)) (d (n "typed-arena") (r "^1.0") (d #t) (k 0)))) (h "1k024010ydvq2916m87n5124501wlq29jqad2crn2ppddj1niv5w") (f (quote (("unstable") ("compile_failure"))))))

(define-public crate-sxd-document-0.1.1 (c (n "sxd-document") (v "0.1.1") (d (list (d (n "peresil") (r "^0.2.1") (d #t) (k 0)) (d (n "typed-arena") (r "^1.0") (d #t) (k 0)))) (h "1kfjnbh8vbxpi205d8x8qw8h3j3q9sr26kn3nw3037f15sp44nwb") (f (quote (("unstable") ("compile_failure"))))))

(define-public crate-sxd-document-0.1.2 (c (n "sxd-document") (v "0.1.2") (d (list (d (n "peresil") (r "^0.2.1") (d #t) (k 0)) (d (n "typed-arena") (r "^1.0") (d #t) (k 0)))) (h "1m0h7w6qx1f27x775m0la32l33lq76xj71r9ylpx2drim9nxr2by") (f (quote (("unstable") ("compile_failure"))))))

(define-public crate-sxd-document-0.2.0 (c (n "sxd-document") (v "0.2.0") (d (list (d (n "peresil") (r "^0.3.0") (d #t) (k 0)) (d (n "typed-arena") (r "^1.0") (d #t) (k 0)))) (h "0c004rjdl5bl9g3jv9005xdqldg7ywms548k742mg1m6gb0zrm74") (f (quote (("unstable") ("compile_failure"))))))

(define-public crate-sxd-document-0.2.1 (c (n "sxd-document") (v "0.2.1") (d (list (d (n "peresil") (r "^0.3.0") (d #t) (k 0)) (d (n "typed-arena") (r "^1.0") (d #t) (k 0)))) (h "10f1z165x09fr4ajsxzm7b4y76xcqrmcplrvrwf06jrvrciykanm") (f (quote (("unstable") ("compile_failure"))))))

(define-public crate-sxd-document-0.2.2 (c (n "sxd-document") (v "0.2.2") (d (list (d (n "peresil") (r "^0.3.0") (d #t) (k 0)) (d (n "typed-arena") (r "^1.0") (d #t) (k 0)))) (h "0p1r31k2gq9p18h7fyq1p1qnpb94r42q5dadcvxyapksc0qsm90l") (f (quote (("unstable") ("compile_failure"))))))

(define-public crate-sxd-document-0.2.3 (c (n "sxd-document") (v "0.2.3") (d (list (d (n "peresil") (r "^0.3.0") (d #t) (k 0)) (d (n "typed-arena") (r "^1.0") (d #t) (k 0)))) (h "19rfv9isnknhhrvgfvbkj0hqd4l919rwbis31zh8i0g584zasw19") (f (quote (("unstable") ("compile_failure"))))))

(define-public crate-sxd-document-0.2.4 (c (n "sxd-document") (v "0.2.4") (d (list (d (n "peresil") (r "^0.3.0") (d #t) (k 0)) (d (n "typed-arena") (r "^1.0") (d #t) (k 0)))) (h "07v0b2ahbgyh953dr5hpabxhmscnwqc765fqsn5i0qi3hnkjbp36") (f (quote (("unstable") ("compile_failure"))))))

(define-public crate-sxd-document-0.2.5 (c (n "sxd-document") (v "0.2.5") (d (list (d (n "peresil") (r "^0.3.0") (d #t) (k 0)) (d (n "typed-arena") (r "^1.0") (d #t) (k 0)))) (h "0y03wdif5xcsci9smn53j0g4mdjxjrwnyf9xn5kcbngzcisl3iiz") (f (quote (("unstable") ("compile_failure"))))))

(define-public crate-sxd-document-0.2.6 (c (n "sxd-document") (v "0.2.6") (d (list (d (n "peresil") (r "^0.3.0") (d #t) (k 0)) (d (n "typed-arena") (r "^1.0") (d #t) (k 0)))) (h "18p028mjmqrgjdq0nrpcl8knssxrp9ysj7440rsq8wxifljhsdhy") (f (quote (("unstable") ("compile_failure"))))))

(define-public crate-sxd-document-0.3.0 (c (n "sxd-document") (v "0.3.0") (d (list (d (n "peresil") (r "^0.3.0") (d #t) (k 0)) (d (n "typed-arena") (r "^1.0") (d #t) (k 0)))) (h "1yd46vdvffmvibk58a96i3ln4vak3m7mvz81n3f90mhk0igsm7bz") (f (quote (("unstable") ("compile_failure"))))))

(define-public crate-sxd-document-0.3.1 (c (n "sxd-document") (v "0.3.1") (d (list (d (n "peresil") (r "^0.3.0") (d #t) (k 0)) (d (n "typed-arena") (r "^1.0") (d #t) (k 0)))) (h "138z1qxjygaf4kgjd90s3k4shj03vb98wmar0hqzds62an1ld7m2") (f (quote (("unstable") ("compile_failure"))))))

(define-public crate-sxd-document-0.3.2 (c (n "sxd-document") (v "0.3.2") (d (list (d (n "peresil") (r "^0.3.0") (d #t) (k 0)) (d (n "typed-arena") (r "^1.0") (d #t) (k 0)))) (h "0y10shqmy9xb73g403rg1108wsagny9d8jrcm081pbwzpqvjzn4l") (f (quote (("unstable") ("compile_failure"))))))

