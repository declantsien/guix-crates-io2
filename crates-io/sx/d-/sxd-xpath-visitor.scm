(define-module (crates-io sx d- sxd-xpath-visitor) #:use-module (crates-io))

(define-public crate-sxd-xpath-visitor-0.4.3 (c (n "sxd-xpath-visitor") (v "0.4.3") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "peresil") (r "^0.3.0") (d #t) (k 0)) (d (n "snafu") (r "^0.5.0") (d #t) (k 0)) (d (n "sxd-document") (r ">=0.2, <0.4") (d #t) (k 0)))) (h "1qwz1i9scvbs1m0famiirzlgqcv520r4vjm5vgfl8y6f7i1jm4fm") (f (quote (("unstable"))))))

