(define-module (crates-io sx d- sxd-xpath) #:use-module (crates-io))

(define-public crate-sxd-xpath-0.1.0 (c (n "sxd-xpath") (v "0.1.0") (d (list (d (n "getopts") (r "^0.2.4") (d #t) (k 0)) (d (n "peresil") (r "^0.2.0") (d #t) (k 0)) (d (n "sxd-document") (r "^0.1.0") (d #t) (k 0)))) (h "04b5p8sahv206mvqwdy4kirinbmnqyvj10lxgb7xsf5nyhdagl7j") (f (quote (("unstable"))))))

(define-public crate-sxd-xpath-0.1.1 (c (n "sxd-xpath") (v "0.1.1") (d (list (d (n "getopts") (r "^0.2.4") (d #t) (k 0)) (d (n "peresil") (r "^0.2.0") (d #t) (k 0)) (d (n "sxd-document") (r "^0.1.0") (d #t) (k 0)))) (h "042f1rcfmqpf6rh4nsrsc535p2zbkvbn05sldwyzk3xpzqiby2i5") (f (quote (("unstable"))))))

(define-public crate-sxd-xpath-0.1.2 (c (n "sxd-xpath") (v "0.1.2") (d (list (d (n "getopts") (r "^0.2.4") (d #t) (k 0)) (d (n "peresil") (r "^0.2.0") (d #t) (k 0)) (d (n "sxd-document") (r "^0.1.0") (d #t) (k 0)))) (h "14hak02x5fcnzgcnhgvd5mj5qb8gldmqzij0h14aibwsd5v04b3k") (f (quote (("unstable"))))))

(define-public crate-sxd-xpath-0.2.0 (c (n "sxd-xpath") (v "0.2.0") (d (list (d (n "getopts") (r "^0.2.4") (d #t) (k 0)) (d (n "peresil") (r "^0.3.0") (d #t) (k 0)) (d (n "sxd-document") (r "^0.2.0") (d #t) (k 0)))) (h "051lmy1asprcnxn2mmnyd4117131ac8bxgv5gph6fiddhwxjvcb9") (f (quote (("unstable"))))))

(define-public crate-sxd-xpath-0.2.1 (c (n "sxd-xpath") (v "0.2.1") (d (list (d (n "getopts") (r "^0.2.4") (d #t) (k 0)) (d (n "peresil") (r "^0.3.0") (d #t) (k 0)) (d (n "sxd-document") (r "^0.2.0") (d #t) (k 0)))) (h "0z9pxd29dlmdf15bfrwap83ml8k6gwrk70j53055r1ml4cn5xpdp") (f (quote (("unstable"))))))

(define-public crate-sxd-xpath-0.3.0 (c (n "sxd-xpath") (v "0.3.0") (d (list (d (n "getopts") (r "^0.2.4") (d #t) (k 0)) (d (n "peresil") (r "^0.3.0") (d #t) (k 0)) (d (n "quick-error") (r "^1.1.0") (d #t) (k 0)) (d (n "sxd-document") (r "^0.2.0") (d #t) (k 0)))) (h "0qwvgrc5llllys6ay5k5ajr7kpn6kbm2sl9qn2pbqsm2w736xnys") (f (quote (("unstable"))))))

(define-public crate-sxd-xpath-0.4.0 (c (n "sxd-xpath") (v "0.4.0") (d (list (d (n "peresil") (r "^0.3.0") (d #t) (k 0)) (d (n "quick-error") (r "^1.1.0") (d #t) (k 0)) (d (n "sxd-document") (r "^0.2.0") (d #t) (k 0)))) (h "1zc5z6k5dbxj5qixgjjgqdw7rgh0xcw5a91zpd8kngrhpar7klpr") (f (quote (("unstable"))))))

(define-public crate-sxd-xpath-0.4.1 (c (n "sxd-xpath") (v "0.4.1") (d (list (d (n "peresil") (r "^0.3.0") (d #t) (k 0)) (d (n "quick-error") (r "^1.1.0") (d #t) (k 0)) (d (n "sxd-document") (r "^0.2.0") (d #t) (k 0)))) (h "1gzyf1h62n09fv25c7dja6yzsfs0qk4p6i69bpm1mi0zahp8rkb5") (f (quote (("unstable"))))))

(define-public crate-sxd-xpath-0.4.2 (c (n "sxd-xpath") (v "0.4.2") (d (list (d (n "peresil") (r "^0.3.0") (d #t) (k 0)) (d (n "quick-error") (r "^1.1.0") (d #t) (k 0)) (d (n "sxd-document") (r ">= 0.2, < 0.4") (d #t) (k 0)))) (h "1sin3g8lzans065gjcwrpm7gdpwdpdg4rpi91rlvb1q8sfjrvqrn") (f (quote (("unstable"))))))

