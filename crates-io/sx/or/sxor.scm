(define-module (crates-io sx or sxor) #:use-module (crates-io))

(define-public crate-sxor-1.0.0 (c (n "sxor") (v "1.0.0") (d (list (d (n "clap") (r "^2.33.0") (f (quote ("color"))) (d #t) (k 0)))) (h "1626k6wb2hlll83yhhv4cy6yisid26wcvq42xsayy2bylpjsw64a")))

(define-public crate-sxor-1.0.1 (c (n "sxor") (v "1.0.1") (d (list (d (n "clap") (r "^2.33.0") (f (quote ("color"))) (d #t) (k 0)))) (h "111r7hp843pl1s4rxx1p1ibmfrzbkl084g9vxapa6b78xxrai2b1")))

