(define-module (crates-io sx d_ sxd_html) #:use-module (crates-io))

(define-public crate-sxd_html-0.1.0 (c (n "sxd_html") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 2)) (d (n "html5ever") (r ">=0.24.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.13") (f (quote ("blocking"))) (d #t) (k 2)) (d (n "sxd-document") (r ">=0.3.0") (d #t) (k 0)) (d (n "sxd-xpath") (r "^0.4.2") (d #t) (k 2)))) (h "0mfgy8z6by33vs59cxph1ibrn426bwcibn4xnc61dkknzmx3c60l")))

(define-public crate-sxd_html-0.1.1 (c (n "sxd_html") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 2)) (d (n "html5ever") (r "^0.26.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.13") (f (quote ("blocking"))) (d #t) (k 2)) (d (n "sxd-document") (r "^0.3.2") (d #t) (k 0)) (d (n "sxd-xpath") (r "^0.4.2") (d #t) (k 2)))) (h "1mw9qrmwf9zvq86g2ri41gcvwcky1a1mxfrk94bsb0b3dpjdvvwf")))

