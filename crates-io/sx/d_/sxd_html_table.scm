(define-module (crates-io sx d_ sxd_html_table) #:use-module (crates-io))

(define-public crate-sxd_html_table-0.1.0 (c (n "sxd_html_table") (v "0.1.0") (d (list (d (n "csv") (r "^1.1.6") (d #t) (k 0)) (d (n "sxd-document") (r "^0.3.2") (d #t) (k 0)) (d (n "sxd-xpath") (r "^0.4.2") (d #t) (k 0)) (d (n "sxd_html") (r "^0.1.1") (d #t) (k 0)))) (h "1j3f5fyxx3r0pizzdy2yi7b1y6421nv70sgzq1bl8aj7l5j6cfj6")))

