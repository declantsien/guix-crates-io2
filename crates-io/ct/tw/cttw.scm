(define-module (crates-io ct tw cttw) #:use-module (crates-io))

(define-public crate-cttw-0.0.8 (c (n "cttw") (v "0.0.8") (d (list (d (n "oauth-client") (r "*") (d #t) (k 0)) (d (n "rust-crypto") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "twitter-api") (r "*") (d #t) (k 0)))) (h "0f6xw3v8yip3vrw31zhw6figbrnsjklkjd5j8rmpphygsn39gmny")))

(define-public crate-cttw-0.0.10 (c (n "cttw") (v "0.0.10") (d (list (d (n "oauth-client") (r "^0.1") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "twitter-api") (r "^0.2") (d #t) (k 0)))) (h "0h55mdwpvws5wqy9f3rq1m1xg83bs50qp8gl84yp2q7bbz112v8w")))

