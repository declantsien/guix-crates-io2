(define-module (crates-io ct ty ctty) #:use-module (crates-io))

(define-public crate-ctty-0.1.0 (c (n "ctty") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (t "cfg(any(target_os = \"freebsd\", target_os = \"macos\"))") (k 1)) (d (n "glob") (r "^0.3") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(any(target_os = \"freebsd\", target_os = \"macos\"))") (k 0)) (d (n "nix") (r "^0.19") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0csgya5xvpi01d8gizb8xky09ab1kafvmx0mja0hwgsiqldwg4hq")))

