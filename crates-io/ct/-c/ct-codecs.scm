(define-module (crates-io ct -c ct-codecs) #:use-module (crates-io))

(define-public crate-ct-codecs-0.1.0 (c (n "ct-codecs") (v "0.1.0") (h "1jvk0bbbrvfq8s3b95mzs4pm9rmncnkxnmh49x8wr63adk32541z") (f (quote (("std") ("default" "std"))))))

(define-public crate-ct-codecs-0.1.1 (c (n "ct-codecs") (v "0.1.1") (h "0fk7c2d5ca25rvpyk85jk0iysmwqbig0j20mr1fxjk9zi2lw4hz9") (f (quote (("std") ("default" "std"))))))

(define-public crate-ct-codecs-1.0.0 (c (n "ct-codecs") (v "1.0.0") (h "0mi39i5lzql8vkfr5dda70iv06xjgh4cgwsbl5vnz0imxji32cr7") (f (quote (("std") ("default" "std"))))))

(define-public crate-ct-codecs-1.1.0 (c (n "ct-codecs") (v "1.1.0") (h "1qz5ayqfp1zr0m898wmcqzg6d277gh8mk3f39zlw5xq7zrn43gpp") (f (quote (("std") ("default" "std"))))))

(define-public crate-ct-codecs-1.1.1 (c (n "ct-codecs") (v "1.1.1") (h "1pvmrkk95jadmhhd5mn88mq2dfnq0yng8mk3pfd5l6dq0i2fpdzk") (f (quote (("std") ("default" "std"))))))

