(define-module (crates-io ct l1 ctl10n) #:use-module (crates-io))

(define-public crate-ctl10n-0.1.0 (c (n "ctl10n") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.2") (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)))) (h "1nbs986xqmhckidjk8z28haqsalnx69fxadvscfg29vp3q967hqq")))

(define-public crate-ctl10n-0.2.0 (c (n "ctl10n") (v "0.2.0") (d (list (d (n "quote") (r "^1.0.2") (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)))) (h "03r9g6xbd7ji5hrarh2hgq2dfpqjkn8zvwgdzfqghyw22wfdncmq")))

