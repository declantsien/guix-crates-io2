(define-module (crates-io ct ft ctftools) #:use-module (crates-io))

(define-public crate-ctftools-0.1.0 (c (n "ctftools") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.28") (d #t) (k 0)) (d (n "checksec") (r "^0.0.4") (d #t) (k 0)) (d (n "goblin") (r "^0.2.1") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "sysexit") (r "^0.2.0") (d #t) (k 0)))) (h "0c1f7w9xaafffkvkb34vp4f0i17gxdgik3nvb8i0v6xxlg3hc4cj")))

(define-public crate-ctftools-0.1.1 (c (n "ctftools") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.28") (d #t) (k 0)) (d (n "checksec") (r "^0.0.4") (d #t) (k 0)) (d (n "goblin") (r "^0.2.1") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "sysexit") (r "^0.2.0") (d #t) (k 0)))) (h "100i93lz08sad2prf20w70w0szs27n481plni687cv3chnyvxacz")))

(define-public crate-ctftools-0.1.2 (c (n "ctftools") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.28") (d #t) (k 0)) (d (n "checksec") (r "^0.0.4") (d #t) (k 0)) (d (n "goblin") (r "^0.2.1") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "sysexit") (r "^0.2.0") (d #t) (k 0)))) (h "0gmmixq9j4qs68v7qjniap24fvsk69bz7yidcrg3ghx5ccsvxkj5")))

(define-public crate-ctftools-0.1.3 (c (n "ctftools") (v "0.1.3") (d (list (d (n "checksec") (r "^0.0.8") (d #t) (k 0)) (d (n "goblin") (r "^0.3.4") (d #t) (k 0)) (d (n "ropr") (r "^0.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "1p253akh5x2bnawk2nnk9c6gz24p07fidx2rh0l23vqfqgy6mx4a")))

(define-public crate-ctftools-0.1.4 (c (n "ctftools") (v "0.1.4") (d (list (d (n "checksec") (r "^0.0.8") (d #t) (k 0)) (d (n "goblin") (r "^0.3.4") (d #t) (k 0)) (d (n "ropr") (r "^0.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "0jyvn9x7rv4c8ibvrw4913qxwabrdfvhh4ql55x9qlp02bv49sid")))

(define-public crate-ctftools-0.1.5 (c (n "ctftools") (v "0.1.5") (d (list (d (n "checksec") (r "^0.0.8") (d #t) (k 0)) (d (n "goblin") (r "^0.3.4") (d #t) (k 0)) (d (n "ropr") (r "^0.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "0kw9dbq3dwag6krlxv83z1vg04g271p3cxpkwrfsfmvz9bdzkn69")))

(define-public crate-ctftools-0.1.6 (c (n "ctftools") (v "0.1.6") (d (list (d (n "checksec") (r "^0.0.8") (d #t) (k 0)) (d (n "goblin") (r "^0.3.4") (d #t) (k 0)) (d (n "ropr") (r "^0.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "1cgmlcavhvvvhr91mpdwqzi6av0avy7hnihbx71lml2mzdv4rnn1")))

