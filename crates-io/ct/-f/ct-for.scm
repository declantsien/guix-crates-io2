(define-module (crates-io ct -f ct-for) #:use-module (crates-io))

(define-public crate-ct-for-0.1.0 (c (n "ct-for") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)))) (h "0hg2mgd28r0qj64hp57a6fadb7xf41jmxkf4zwvpwz6cqfijal5r")))

(define-public crate-ct-for-0.1.1 (c (n "ct-for") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)))) (h "06gys4fl7v5lm8lfp98l0m7c0fx8dpp8fr0rbfdg9qvgkpbld9wg")))

