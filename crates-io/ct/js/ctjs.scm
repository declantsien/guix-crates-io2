(define-module (crates-io ct js ctjs) #:use-module (crates-io))

(define-public crate-ctjs-0.0.1 (c (n "ctjs") (v "0.0.1") (d (list (d (n "ctjs_macros") (r "^0.0.1") (d #t) (k 0)))) (h "0x3c5rfgig14fs0l6m0wignz800jyvx05q7ckjxfccac1hlnnglj")))

(define-public crate-ctjs-0.0.2 (c (n "ctjs") (v "0.0.2") (d (list (d (n "ctjs_macros") (r "=0.0.2") (d #t) (k 0)))) (h "1340iyr5zdv4y719q7q8rlprs58v1ifkl33idy34xd24ffbzhxcv")))

