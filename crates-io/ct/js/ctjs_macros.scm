(define-module (crates-io ct js ctjs_macros) #:use-module (crates-io))

(define-public crate-ctjs_macros-0.0.1 (c (n "ctjs_macros") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quick-js") (r "^0.4.1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("derive" "extra-traits"))) (d #t) (k 0)) (d (n "syn-serde") (r "^0.2") (f (quote ("json"))) (d #t) (k 0)))) (h "0gvknwimg0bwcm1lvranc9y10pz14z1qcngzh8nwri20f7j0za66")))

(define-public crate-ctjs_macros-0.0.2 (c (n "ctjs_macros") (v "0.0.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quick-js") (r "^0.4.1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("derive" "extra-traits"))) (d #t) (k 0)) (d (n "syn-serde") (r "^0.2") (f (quote ("json"))) (d #t) (k 0)))) (h "013z71y71rif5k398zabrblqg9ikii98njzva9h4cs1cwikj1g6x")))

