(define-module (crates-io ct fl ctflag) #:use-module (crates-io))

(define-public crate-ctflag-0.1.0 (c (n "ctflag") (v "0.1.0") (d (list (d (n "ctflag_derive") (r "^0.1.0") (d #t) (k 0)))) (h "1pnjvvyr296pzkzm2jx3xipbrda63yflkw3swq3aars00iwg1p8v")))

(define-public crate-ctflag-0.1.1 (c (n "ctflag") (v "0.1.1") (d (list (d (n "ctflag_derive") (r "^0.1.1") (d #t) (k 0)))) (h "1ivdpj8s8d74dx6kc7mvgarnzbs6yxrpm4815a1bbv3ll8cqz2ap")))

(define-public crate-ctflag-0.1.2 (c (n "ctflag") (v "0.1.2") (d (list (d (n "ctflag_derive") (r "^0.1.2") (d #t) (k 0)))) (h "00j6a2plxd4isflryajsx0q8pdm8i1f9agifdfk27as4zmbkbapy")))

