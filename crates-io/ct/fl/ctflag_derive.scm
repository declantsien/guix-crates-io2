(define-module (crates-io ct fl ctflag_derive) #:use-module (crates-io))

(define-public crate-ctflag_derive-0.1.0 (c (n "ctflag_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("extra-traits"))) (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.3") (d #t) (k 0)))) (h "12g6zb9mppihjd0yryjc7nrypiaj0vndxacx22k5agskvn1bm8nf")))

(define-public crate-ctflag_derive-0.1.1 (c (n "ctflag_derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("extra-traits"))) (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.3") (d #t) (k 0)))) (h "0831ihh9rqy45m9nrdfp32py301dlg2g9d8fvsv9jl1zqwbzsn3y")))

(define-public crate-ctflag_derive-0.1.2 (c (n "ctflag_derive") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.3") (d #t) (k 0)))) (h "1vmphnsbrjxk9jgmbqrm0av4z8hrmiw3q96rjvkr7kmh4cywpfq8")))

