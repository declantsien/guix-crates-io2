(define-module (crates-io ct -m ct-merkle) #:use-module (crates-io))

(define-public crate-ct-merkle-0.1.0 (c (n "ct-merkle") (v "0.1.0") (d (list (d (n "digest") (r "^0.10") (d #t) (k 0)) (d (n "generic-array") (r "^0.14") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 2)) (d (n "rand") (r "^0.8") (f (quote ("std_rng"))) (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "sha2") (r "^0.10") (d #t) (k 2)) (d (n "subtle") (r "^2") (k 0)))) (h "1mpnn496ks2ygzz78q5q57xhwhwm40xh59dkpzwg5lm0y60hg4pn") (f (quote (("std" "digest/std") ("default")))) (s 2) (e (quote (("serde" "dep:serde" "generic-array/serde"))))))

