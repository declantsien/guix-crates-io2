(define-module (crates-io ct xe ctxerr) #:use-module (crates-io))

(define-public crate-ctxerr-0.1.0 (c (n "ctxerr") (v "0.1.0") (d (list (d (n "ctxerr_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.39") (d #t) (k 0)))) (h "02fm6kr0cgaksr03nnl4ln9gifsv9zmzjkklnj05xy4w5rky5rav")))

(define-public crate-ctxerr-0.2.0 (c (n "ctxerr") (v "0.2.0") (d (list (d (n "ctxerr_derive") (r "^0.3.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.39") (d #t) (k 0)))) (h "11yfj04rm3d5pl9fi6ns6k67x0gqbq3v4dh7bk7c9s4mcwiz8i1b")))

(define-public crate-ctxerr-0.2.1 (c (n "ctxerr") (v "0.2.1") (d (list (d (n "ctxerr_derive") (r "^0.3.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.39") (d #t) (k 0)))) (h "1lqmsy0fc0mqxlvnwrnpbca61drjz5z4726qdxyyzld0kskmzgii")))

(define-public crate-ctxerr-0.2.2 (c (n "ctxerr") (v "0.2.2") (d (list (d (n "ctxerr_derive") (r "^0.3.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.39") (d #t) (k 0)))) (h "10avp0amd1bharrkvq1fm2g7d7i5m8cprxi1151hsj95yky3r1rm")))

(define-public crate-ctxerr-0.2.3 (c (n "ctxerr") (v "0.2.3") (d (list (d (n "ctxerr_derive") (r "^0.4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "1s3d82w6civnyjr37wk5y7mn2xaysan5ik8v24vl4az656f1flqk")))

(define-public crate-ctxerr-0.2.4 (c (n "ctxerr") (v "0.2.4") (d (list (d (n "ctxerr_derive") (r "^0.4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0g74kyf5dci5vmxxsmc9rqgldj44fxm70k82jmb8fnv1i5w0kxk7")))

(define-public crate-ctxerr-0.2.5 (c (n "ctxerr") (v "0.2.5") (d (list (d (n "ctxerr_derive") (r "^0.5.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "07kssbpxbfvyb0qvgmwsacaka576s8nmzsk9mjshqv3rg7fwcbnq")))

(define-public crate-ctxerr-0.2.6 (c (n "ctxerr") (v "0.2.6") (d (list (d (n "ctxerr_derive") (r "^0.5.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "1zr5l34sl2c9axji2dc8bs9mlj1yc6ln2sq6rg9pj01maa523j1n") (y #t)))

(define-public crate-ctxerr-0.2.7 (c (n "ctxerr") (v "0.2.7") (d (list (d (n "ctxerr_derive") (r "^0.5.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "122w28p7i2b3pwrfs3ihfxwajwv8jdl0g1d00wvxgir1vwja5v9v") (y #t)))

(define-public crate-ctxerr-0.2.8 (c (n "ctxerr") (v "0.2.8") (d (list (d (n "ctxerr_derive") (r "^0.5.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "13l7q1lhkzdng088379n20blgpgp3786ymj37yjp6krsa8wz4c6w")))

