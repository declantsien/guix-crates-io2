(define-module (crates-io ct xe ctxerr_derive) #:use-module (crates-io))

(define-public crate-ctxerr_derive-0.1.0 (c (n "ctxerr_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0rpznl3z32pvmbc20wc5lkpcmbbyxk0chzzxd9k0q52ckq7dnalv")))

(define-public crate-ctxerr_derive-0.2.0 (c (n "ctxerr_derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1b67kfi97km1sazlq38wd00122mqwsvwd1im8z2zc9smnljgvdqs")))

(define-public crate-ctxerr_derive-0.3.0 (c (n "ctxerr_derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0vdc2hqyb7ijw4wx3bd8cwkm82qqw5rrbr0y8hyv4i8vdkpmcwm1")))

(define-public crate-ctxerr_derive-0.4.0 (c (n "ctxerr_derive") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "079m75xclyyy09mazq7nvxcb1sqvpnmcg5w9c28467b8dkagbg4m")))

(define-public crate-ctxerr_derive-0.5.0 (c (n "ctxerr_derive") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1dilbrm0a1rnyca5m1ycp4jf28abm3sbd1z5vasrvfwi31bbmdld")))

(define-public crate-ctxerr_derive-0.5.1 (c (n "ctxerr_derive") (v "0.5.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "140k2v6m1pw1pzjk0wr9nw80vyab9jldhhd8j8wqi8kmjhck0l5m")))

(define-public crate-ctxerr_derive-0.5.2 (c (n "ctxerr_derive") (v "0.5.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1pddxg5pvy31rxb60zndj40hyam3sa5r829pzx8j0lk3d2cdnm6f") (y #t)))

(define-public crate-ctxerr_derive-0.5.3 (c (n "ctxerr_derive") (v "0.5.3") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1n5hrfakpav11g4x1cjjp6c3d6j34i9di4dk8w4r3xg6c7g8iwns")))

(define-public crate-ctxerr_derive-0.5.4 (c (n "ctxerr_derive") (v "0.5.4") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1z6184x56d0akbvrpsi7wc4x7ilcyl8kkl859ndd45shjyzi16ay")))

