(define-module (crates-io ct co ctcore) #:use-module (crates-io))

(define-public crate-ctcore-0.1.0 (c (n "ctcore") (v "0.1.0") (d (list (d (n "pretty_assertions") (r "^1.2.1") (d #t) (k 2)))) (h "18hfqimcbwk0gac45jc5bmqgrvli619yc4lljdqk7qbmmf71y5n7")))

(define-public crate-ctcore-0.2.0 (c (n "ctcore") (v "0.2.0") (d (list (d (n "pretty_assertions") (r "^1.2.1") (d #t) (k 2)))) (h "0hg9ry28cs15rx8m5q7pv58wfrblyx6z624vgzllbi96wwd70mf8")))

