(define-module (crates-io ct tt cttt-parser) #:use-module (crates-io))

(define-public crate-cttt-parser-0.1.0 (c (n "cttt-parser") (v "0.1.0") (d (list (d (n "insta") (r "^1.31.0") (f (quote ("yaml"))) (d #t) (k 2)) (d (n "pest") (r "^2.7.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.171") (f (quote ("derive"))) (d #t) (k 0)))) (h "0pxwzl2kl6pkj43lfgah78rfr1mpqv79babfnfsq4pjnl6axfd74") (y #t) (r "1.67")))

(define-public crate-cttt-parser-0.1.1 (c (n "cttt-parser") (v "0.1.1") (d (list (d (n "insta") (r "^1.31.0") (f (quote ("yaml"))) (d #t) (k 2)) (d (n "pest") (r "^2.7.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.171") (f (quote ("derive"))) (d #t) (k 0)))) (h "1icafsh4kymxc2gml1p9iyl4ji31ppyfnmi1h0fv6y2qy4121fjd") (y #t) (r "1.67")))

(define-public crate-cttt-parser-0.1.2 (c (n "cttt-parser") (v "0.1.2") (d (list (d (n "insta") (r "^1.31.0") (f (quote ("yaml"))) (d #t) (k 2)) (d (n "pest") (r "^2.7.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.171") (f (quote ("derive"))) (d #t) (k 0)))) (h "1dixyfna12n1bzdk67irjig4aysddsam56lc7cnmms36r4wm3id1") (r "1.67")))

