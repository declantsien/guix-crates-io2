(define-module (crates-io ct ro ctron) #:use-module (crates-io))

(define-public crate-ctron-0.1.0 (c (n "ctron") (v "0.1.0") (h "061rbgnj016jg0mxky3f29w58d819bvp8adlnnv2axw6pmgiii0y")))

(define-public crate-ctron-0.2.0 (c (n "ctron") (v "0.2.0") (d (list (d (n "colored") (r "^2") (d #t) (k 0)))) (h "0glq2nccdybg1k2508fs8zlvl477kkc15x9v88rfib3kv3wc043y")))

