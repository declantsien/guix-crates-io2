(define-module (crates-io ct xb ctxbuilder) #:use-module (crates-io))

(define-public crate-ctxbuilder-0.1.0 (c (n "ctxbuilder") (v "0.1.0") (d (list (d (n "uuid") (r "^1.7.0") (f (quote ("v4"))) (o #t) (d #t) (k 0)))) (h "1wbq6nl5jjgsfpcqgvvzfdqj32wnjy1jj1fiya4nkg6dx0irylcv") (f (quote (("default" "uuid")))) (s 2) (e (quote (("uuid" "dep:uuid"))))))

(define-public crate-ctxbuilder-0.2.0 (c (n "ctxbuilder") (v "0.2.0") (d (list (d (n "uuid") (r "^1.7.0") (f (quote ("v4"))) (o #t) (d #t) (k 0)))) (h "1pijbjnydryyngxqdb22fpasqdg35riz46dgrckdc371qsff0h2j") (f (quote (("default" "uuid")))) (s 2) (e (quote (("uuid" "dep:uuid"))))))

