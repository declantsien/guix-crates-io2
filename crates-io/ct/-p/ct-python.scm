(define-module (crates-io ct -p ct-python) #:use-module (crates-io))

(define-public crate-ct-python-0.5.0 (c (n "ct-python") (v "0.5.0") (d (list (d (n "inline-python-macros") (r "^0.5.1") (d #t) (k 0)))) (h "194kp188gfyyqmbcswnrcn4s11ca8h9b91y1sc01pnbbnr81246l")))

(define-public crate-ct-python-0.5.1 (c (n "ct-python") (v "0.5.1") (d (list (d (n "inline-python-macros") (r "^0.5.1") (d #t) (k 0)))) (h "1xzq86kyaip3yd09z45jgnx97zi54iyshl7grg3mka4jcznmnwp3")))

(define-public crate-ct-python-0.5.2 (c (n "ct-python") (v "0.5.2") (d (list (d (n "inline-python-macros") (r "=0.7.0") (d #t) (k 0)))) (h "1jfzc91gl3zxyid6lgwxlwm3k211wxjj2216afc8l9q4rrsz0cl6")))

(define-public crate-ct-python-0.5.3 (c (n "ct-python") (v "0.5.3") (d (list (d (n "inline-python-macros") (r "=0.7.1") (d #t) (k 0)))) (h "0d33d478h4gx9hlyq3aqhcwl8zls0m6y0kaz85raxy8a3fg0kj2s")))

(define-public crate-ct-python-0.5.4 (c (n "ct-python") (v "0.5.4") (d (list (d (n "inline-python-macros") (r "=0.8.0") (d #t) (k 0)))) (h "1fs0n8fmnj4rgdb9kd8xmdk36kw4l0m3lbqsmss2ayg53jwy7jlf")))

(define-public crate-ct-python-0.5.5 (c (n "ct-python") (v "0.5.5") (d (list (d (n "inline-python-macros") (r "=0.10.0") (d #t) (k 0)))) (h "01m0nlrpnwgp1jyvsar82q89kh653iqz0bfgdzcw1zpxd5n7vasq")))

(define-public crate-ct-python-0.5.6 (c (n "ct-python") (v "0.5.6") (d (list (d (n "inline-python-macros") (r "=0.11.0") (d #t) (k 0)))) (h "18ccvvxk9r4g6ni4hqzajl6jc5vj92b1b26yf12s84ymy6qz753s")))

(define-public crate-ct-python-0.5.7 (c (n "ct-python") (v "0.5.7") (d (list (d (n "inline-python-macros") (r "=0.12.0") (d #t) (k 0)))) (h "09rpb9pdqzj9sb2f0bl4albs9ycyp5gc8rimrgyzvlfnr1z1snis")))

