(define-module (crates-io ct rs ctrs) #:use-module (crates-io))

(define-public crate-ctrs-1.0.0 (c (n "ctrs") (v "1.0.0") (h "167nhqk1igr0l0wpm0s68zqayrlq4a083sci0508lkbwb73h9x8r")))

(define-public crate-ctrs-1.0.1 (c (n "ctrs") (v "1.0.1") (h "10xha2vga1y7qs6zz2x6xnqpra9is7r7czwk3j5grkmjg1ws0cgx")))

(define-public crate-ctrs-1.0.2 (c (n "ctrs") (v "1.0.2") (h "0jg43gg7jxr4i3w2hq17y7p0grl7bw9i1ixc5md3z7prfq1xkv9f")))

