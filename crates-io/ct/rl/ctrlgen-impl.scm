(define-module (crates-io ct rl ctrlgen-impl) #:use-module (crates-io))

(define-public crate-ctrlgen-impl-0.2.0 (c (n "ctrlgen-impl") (v "0.2.0") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1i8lvhs7m00mdyf7307pg78k2hd9pd74b44jf7h8nvk65cg4rvp9")))

(define-public crate-ctrlgen-impl-0.2.1 (c (n "ctrlgen-impl") (v "0.2.1") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "048jq3wbrqdp2p8wb71lj76gkzlmrc0rr8dpb89r5p170g3i0488")))

(define-public crate-ctrlgen-impl-0.2.3 (c (n "ctrlgen-impl") (v "0.2.3") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 0)) (d (n "insta") (r "^1.21.0") (f (quote ("yaml"))) (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0rys76pg67a485hxvj8zhv2xbgq3jmp2fhh0xds9yrfdbsn75y9i")))

(define-public crate-ctrlgen-impl-0.2.4 (c (n "ctrlgen-impl") (v "0.2.4") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 0)) (d (n "insta") (r "^1.21.0") (f (quote ("yaml"))) (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "05vi2z4kyxryyr8j150xgcxp9v7d6kjraiq8cjn1bczv010jnc5z")))

(define-public crate-ctrlgen-impl-0.3.0 (c (n "ctrlgen-impl") (v "0.3.0") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 0)) (d (n "insta") (r "^1.21.0") (f (quote ("yaml"))) (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "09fxaqzzi52f51zc12h0z48kxmzfw7m579mj2505rz56pdaacpbf")))

(define-public crate-ctrlgen-impl-0.3.1 (c (n "ctrlgen-impl") (v "0.3.1") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 0)) (d (n "insta") (r "^1.21.0") (f (quote ("yaml"))) (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1s2rqr0ppj32h583bljdhklb0d211r9rxqbd7hp3r29hhck1mw8a")))

(define-public crate-ctrlgen-impl-0.3.2 (c (n "ctrlgen-impl") (v "0.3.2") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 0)) (d (n "insta") (r "^1.21.0") (f (quote ("yaml"))) (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0izi0pnv4brycf0jv0nqszy8j37bwfdd27ljnj25jf8hjdrhzf8r")))

