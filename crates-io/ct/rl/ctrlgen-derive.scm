(define-module (crates-io ct rl ctrlgen-derive) #:use-module (crates-io))

(define-public crate-ctrlgen-derive-0.2.0 (c (n "ctrlgen-derive") (v "0.2.0") (d (list (d (n "ctrlgen-impl") (r "^0.2.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)))) (h "0g19cgpq7vmw0n3j8amsxfk8a58cjz409axcm01q1af5r1qa058b")))

(define-public crate-ctrlgen-derive-0.2.1 (c (n "ctrlgen-derive") (v "0.2.1") (d (list (d (n "ctrlgen-impl") (r "^0.2.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)))) (h "0nv1cdpj5i3cjldmx7ak23dndcrrwlyy9sfk0x6hr69g5kgi75y4")))

(define-public crate-ctrlgen-derive-0.2.3 (c (n "ctrlgen-derive") (v "0.2.3") (d (list (d (n "ctrlgen-impl") (r "^0.2.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)))) (h "18855igbhp0w2vidzx55izqk66ksgr5xllqdspfgxp9rrixjk77g")))

(define-public crate-ctrlgen-derive-0.2.4 (c (n "ctrlgen-derive") (v "0.2.4") (d (list (d (n "ctrlgen-impl") (r "^0.2.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)))) (h "0nl87z6a0k1sr5hjy5lz971clgxd7dkzmrbqhx1r7xzqhphxrfns")))

(define-public crate-ctrlgen-derive-0.3.0 (c (n "ctrlgen-derive") (v "0.3.0") (d (list (d (n "ctrlgen-impl") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)))) (h "0g749f6yav9gyypj12kz33ix6440y9havvw8j5pim44c9mrjkglh")))

(define-public crate-ctrlgen-derive-0.3.1 (c (n "ctrlgen-derive") (v "0.3.1") (d (list (d (n "ctrlgen-impl") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)))) (h "08l3jgaqnw47jpad40d7qay3f58wzkjm70x6v33148750050hihv")))

(define-public crate-ctrlgen-derive-0.3.2 (c (n "ctrlgen-derive") (v "0.3.2") (d (list (d (n "ctrlgen-impl") (r "^0.3.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)))) (h "06zvzczdi3hbs84nms8p0h7nm3isl858wk05v6cvja58g8ryaasy")))

