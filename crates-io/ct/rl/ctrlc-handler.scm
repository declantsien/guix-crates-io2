(define-module (crates-io ct rl ctrlc-handler) #:use-module (crates-io))

(define-public crate-ctrlc-handler-0.1.1 (c (n "ctrlc-handler") (v "0.1.1") (d (list (d (n "ctrlc") (r "^3.2.1") (d #t) (k 0)))) (h "1riaf23qvlh8a6988b4dglmlkdvb5j0fk5vjljxdv95szwla5q4j")))

(define-public crate-ctrlc-handler-0.1.2 (c (n "ctrlc-handler") (v "0.1.2") (d (list (d (n "crossbeam-channel") (r "^0.5.3") (d #t) (k 0)) (d (n "ctrlc") (r "^3.2.1") (d #t) (k 0)))) (h "0k0fnz4lgqqbhnwjj605wgc9mmki47pllbmqk54wl9jk1j7zm5ms")))

