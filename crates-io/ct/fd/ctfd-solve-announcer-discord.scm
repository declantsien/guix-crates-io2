(define-module (crates-io ct fd ctfd-solve-announcer-discord) #:use-module (crates-io))

(define-public crate-ctfd-solve-announcer-discord-0.1.0 (c (n "ctfd-solve-announcer-discord") (v "0.1.0") (d (list (d (n "clap") (r "^4.0.22") (f (quote ("derive"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11.12") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serenity") (r "^0.11") (d #t) (k 0)) (d (n "sqlite") (r "^0.28.1") (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)))) (h "15xdmazg7wb6344a96ha3vq5y3lzc25mg4bakl21dxzd88s5mjns")))

