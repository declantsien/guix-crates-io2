(define-module (crates-io ct sh ctsh-proc) #:use-module (crates-io))

(define-public crate-ctsh-proc-0.0.1 (c (n "ctsh-proc") (v "0.0.1") (d (list (d (n "duct") (r "^0.13") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "0a3lm0hvqk81zkpfdzlps2hv5dgzs22xw8818r04ngbzp7jawih2") (f (quote (("default"))))))

