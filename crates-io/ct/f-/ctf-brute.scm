(define-module (crates-io ct f- ctf-brute) #:use-module (crates-io))

(define-public crate-ctf-brute-0.1.0 (c (n "ctf-brute") (v "0.1.0") (h "0in7rlklp38013ss7hgrr849yz25hmdkg1gh1j28hcg1qhdixy7p")))

(define-public crate-ctf-brute-0.2.0 (c (n "ctf-brute") (v "0.2.0") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "logos") (r "^0.12.1") (d #t) (k 0)) (d (n "nonempty") (r "^0.8.0") (d #t) (k 0)))) (h "16701xril2fkz1wrs0fy3nx92icqrzi61n7m316gqwjhivmh5kcy")))

(define-public crate-ctf-brute-0.2.1 (c (n "ctf-brute") (v "0.2.1") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "logos") (r "^0.12.1") (d #t) (k 0)) (d (n "nonempty") (r "^0.8.0") (d #t) (k 0)))) (h "1vmzd0l7xj2i1967fxzwljh5wv8h26hagzmpx7iik261yslvqcrr")))

