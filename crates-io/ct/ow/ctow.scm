(define-module (crates-io ct ow ctow) #:use-module (crates-io))

(define-public crate-ctow-0.1.0 (c (n "ctow") (v "0.1.0") (h "02s9zf10x9gv16379p3wxjv1s3dxj1h94gqinj0hcch9gkyv7y3j")))

(define-public crate-ctow-0.1.1 (c (n "ctow") (v "0.1.1") (h "01lwdkj6lmjc881mm28l14pl8knm9k6aflgffn4zcgk6nz214s0d")))

(define-public crate-ctow-0.1.2 (c (n "ctow") (v "0.1.2") (h "0r8qmn71nc5lb27gl0kcnfmr606lwjxg2zqww566g4zclqz49fjg")))

(define-public crate-ctow-0.1.3 (c (n "ctow") (v "0.1.3") (h "10a62qk60p2l1j9hx60q9q11l7dn6v6l0rwxjaxhbiv9qviy8v4j")))

(define-public crate-ctow-0.1.4 (c (n "ctow") (v "0.1.4") (h "0brj38q3qd8zf7adn8d833y4ps8i0ahxx0vmwz723lvklpgdzi92")))

(define-public crate-ctow-0.1.5 (c (n "ctow") (v "0.1.5") (h "13h3r7fx0006gf34sxn7hyrz64q6ys80l2igizs02vlz7llanyms")))

