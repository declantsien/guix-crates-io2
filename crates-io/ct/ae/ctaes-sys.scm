(define-module (crates-io ct ae ctaes-sys) #:use-module (crates-io))

(define-public crate-ctaes-sys-0.1.0 (c (n "ctaes-sys") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "generic-array") (r "^0.9") (f (quote ("serde"))) (d #t) (k 0)) (d (n "hex") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "typenum") (r "^1.9") (d #t) (k 0)))) (h "1x0v0jiq9p0w8b17yjwyy6q2ln1ry7jh2swv9w31rlnkyna6n2k7")))

