(define-module (crates-io ct ee ctee) #:use-module (crates-io))

(define-public crate-ctee-0.1.0 (c (n "ctee") (v "0.1.0") (d (list (d (n "clap") (r "^3.0.13") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strip-ansi-escapes") (r "^0.1.1") (d #t) (k 0)))) (h "1h9ary4mjfp1rjq3mmmac221cgfyck2iwwbf083a0ipwsqva46vy")))

(define-public crate-ctee-0.2.0 (c (n "ctee") (v "0.2.0") (d (list (d (n "clap") (r "^3.2.25") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strip-ansi-escapes") (r "^0.1.1") (d #t) (k 0)))) (h "0sy8f1r9kilmkjajgc9z27dgvm36szrjk0wzhp0r4ywzwnkd8vcb")))

