(define-module (crates-io ct ap ctaphid-dispatch) #:use-module (crates-io))

(define-public crate-ctaphid-dispatch-0.0.0-unreleased (c (n "ctaphid-dispatch") (v "0.0.0-unreleased") (h "0bvv7cn09p2jqspvshsq726brg7gpqkm5dldh0wdrryl27q87770")))

(define-public crate-ctaphid-dispatch-0.1.0 (c (n "ctaphid-dispatch") (v "0.1.0") (d (list (d (n "delog") (r "^0.1") (d #t) (k 0)) (d (n "heapless") (r "^0.7") (d #t) (k 0)) (d (n "heapless-bytes") (r "^0.3") (d #t) (k 0)) (d (n "interchange") (r "^0.2") (d #t) (k 0)))) (h "0nix96s36k29bps7gwl8sypn72qnhd2jb0n8r45xmv721h87iwr4") (f (quote (("std" "delog/std") ("log-warn") ("log-none") ("log-info") ("log-error") ("log-debug") ("log-all") ("default"))))))

(define-public crate-ctaphid-dispatch-0.1.1 (c (n "ctaphid-dispatch") (v "0.1.1") (d (list (d (n "delog") (r "^0.1") (d #t) (k 0)) (d (n "heapless") (r "^0.7") (d #t) (k 0)) (d (n "heapless-bytes") (r "^0.3") (d #t) (k 0)) (d (n "interchange") (r "^0.2.2") (d #t) (k 0)))) (h "1pmx0hh33c59676l6f5h59hhhf7vsdqhfgp2m4sa30isgkv7brzr") (f (quote (("std" "delog/std") ("log-warn") ("log-none") ("log-info") ("log-error") ("log-debug") ("log-all") ("default"))))))

