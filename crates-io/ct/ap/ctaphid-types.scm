(define-module (crates-io ct ap ctaphid-types) #:use-module (crates-io))

(define-public crate-ctaphid-types-0.1.0 (c (n "ctaphid-types") (v "0.1.0") (h "0mh1nhffvs4mkljvaiz0fym345d12jc75qiwzbavs75arymq0k7m") (f (quote (("std") ("default" "std"))))))

(define-public crate-ctaphid-types-0.1.1 (c (n "ctaphid-types") (v "0.1.1") (h "14gd9s75wvhrh0y798znaf1v6m4h0wqq5ksc6r8j3z3imqvzk76g") (f (quote (("std") ("default" "std"))))))

(define-public crate-ctaphid-types-0.2.0 (c (n "ctaphid-types") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)))) (h "0l4k4gry2aik77ra55kys3n52s0avds6gapcklmm6d2idpccs527") (f (quote (("std") ("default" "std"))))))

