(define-module (crates-io ct ap ctap) #:use-module (crates-io))

(define-public crate-ctap-0.1.0 (c (n "ctap") (v "0.1.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "cbor-codec") (r "^0.7") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "num-derive") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "ring") (r "^0.13") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)) (d (n "untrusted") (r "^0.6") (d #t) (k 0)))) (h "1mq0j1dsarkmc50n81ii9kx60vrhqgc8qsa27m2nqcy0sjhf8chc")))

