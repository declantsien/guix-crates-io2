(define-module (crates-io ct es ctest2) #:use-module (crates-io))

(define-public crate-ctest2-0.3.0 (c (n "ctest2") (v "0.3.0") (d (list (d (n "cc") (r "^1.0.1") (d #t) (k 0)) (d (n "garando_syntax") (r "^0.1") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 0)))) (h "07n9h2adv67z78c29jv52k2811nyasax4grv1cfd8743h9ny2rkn")))

(define-public crate-ctest2-0.4.0 (c (n "ctest2") (v "0.4.0") (d (list (d (n "cc") (r "^1.0.1") (d #t) (k 0)) (d (n "garando_syntax") (r "^0.1") (d #t) (k 0)) (d (n "rustc_version") (r "^0.3.2") (d #t) (k 0)))) (h "0cddv8sr6523p0r4m87cq4djr4gcbih0im0mj8k4rr3krd566nph")))

(define-public crate-ctest2-0.4.1 (c (n "ctest2") (v "0.4.1") (d (list (d (n "cc") (r "^1.0.1") (d #t) (k 0)) (d (n "garando_syntax") (r "^0.1") (d #t) (k 0)) (d (n "rustc_version") (r "^0.3.2") (d #t) (k 0)))) (h "1gm09bgnkja911l0p4c7hgfzdcrqq6iq7s7pcndbc48i376bf84x")))

(define-public crate-ctest2-0.4.2 (c (n "ctest2") (v "0.4.2") (d (list (d (n "cc") (r "^1.0.1") (d #t) (k 0)) (d (n "garando_syntax") (r "^0.1") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 0)))) (h "17f5h71h1vbih84vq1j4zgpivcqssspm2rkr4pcgjwbszax0chgf")))

(define-public crate-ctest2-0.4.3 (c (n "ctest2") (v "0.4.3") (d (list (d (n "cc") (r "^1.0.1") (d #t) (k 0)) (d (n "garando_syntax") (r "^0.1") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 0)))) (h "0b2d8gfhnp17dq2l8s2prfxfmvfcdlj13a62xqmb08nkdhxc025s")))

(define-public crate-ctest2-0.4.4 (c (n "ctest2") (v "0.4.4") (d (list (d (n "cc") (r "^1.0.1") (d #t) (k 0)) (d (n "garando_syntax") (r "^0.1") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 0)))) (h "162svcck20whfvbs0v4vwrbfdq31nqbx80vjr0v32kkavqkh1l9p")))

(define-public crate-ctest2-0.4.5 (c (n "ctest2") (v "0.4.5") (d (list (d (n "cc") (r "^1.0.1") (d #t) (k 0)) (d (n "garando_syntax") (r "^0.1") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 0)))) (h "1iqmfd3smlwrfpai1b1i1l553jh18w3iiqh8ll67ga9yykg674wa")))

(define-public crate-ctest2-0.4.6 (c (n "ctest2") (v "0.4.6") (d (list (d (n "cc") (r "^1.0.1") (d #t) (k 0)) (d (n "garando_syntax") (r "^0.1") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 0)))) (h "1lasaj89gcd0g5b9d6h038ak4rqlyykkx4kyi7ljbm7n94sysskw") (r "1.56.0")))

(define-public crate-ctest2-0.4.7 (c (n "ctest2") (v "0.4.7") (d (list (d (n "cc") (r "^1.0.1") (d #t) (k 0)) (d (n "garando_syntax") (r "^0.1") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 0)))) (h "112xzvzfhl9ddrshi679jxnfhvi1w860is9qzppvzcchwjkgmbgj") (r "1.56.0")))

(define-public crate-ctest2-0.4.8 (c (n "ctest2") (v "0.4.8") (d (list (d (n "cc") (r "^1.0.1") (d #t) (k 0)) (d (n "garando_syntax") (r "^0.1") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 0)))) (h "15760ijsmq7wj9bc21lkd1v6bnykfii4z7r949pfmxbq0y01li75") (r "1.56.0")))

