(define-module (crates-io ct es ctest) #:use-module (crates-io))

(define-public crate-ctest-0.1.0 (c (n "ctest") (v "0.1.0") (d (list (d (n "gcc") (r "^0.3.14") (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.19.1") (d #t) (k 0)))) (h "1sxxbzqw0v0a3hasajygasbs5pkkp2s66f0rp4sshlgvp528x3hd")))

(define-public crate-ctest-0.1.1 (c (n "ctest") (v "0.1.1") (d (list (d (n "gcc") (r "^0.3.14") (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.19.1") (d #t) (k 0)))) (h "0wlsw2ab36jqdcdzwxd58pir782449b937kljhxl49y7jzf2ssqm")))

(define-public crate-ctest-0.1.2 (c (n "ctest") (v "0.1.2") (d (list (d (n "gcc") (r "^0.3.14") (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.27.0") (d #t) (k 0)))) (h "0zbg1lhfci68xr7dp44jww29v7s7bahzz2crp30qv6v1ypncj9sd")))

(define-public crate-ctest-0.1.3 (c (n "ctest") (v "0.1.3") (d (list (d (n "gcc") (r "^0.3.14") (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.27.0") (d #t) (k 0)))) (h "08ky2xfy674zbj01h68y6vzfa4fa1j0p5xyr1d08g95ma6idw527")))

(define-public crate-ctest-0.1.4 (c (n "ctest") (v "0.1.4") (d (list (d (n "gcc") (r "^0.3.14") (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.27.0") (d #t) (k 0)))) (h "149mywl382gz1hskwygnx4dvfh1vx3zywk43a5y6p7bx1ihnv9kc")))

(define-public crate-ctest-0.1.5 (c (n "ctest") (v "0.1.5") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.59.1") (d #t) (k 0)))) (h "1sjwvqgqi80znjjzhs73nv0wvvlf42haq5fnjqvpcj3a90razfk7")))

(define-public crate-ctest-0.1.6 (c (n "ctest") (v "0.1.6") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.59.1") (d #t) (k 0)))) (h "0rc3yb5xdzqh7zgndy149a1c4k4plz87si2qnirqchyfd4g2cqdd")))

(define-public crate-ctest-0.1.7 (c (n "ctest") (v "0.1.7") (d (list (d (n "cc") (r "^1.0.1") (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.59.1") (d #t) (k 0)))) (h "0id4qd5z3ndszsypbvbiz43d23ixq99bi7pwp7rdqanywnk9szz5")))

(define-public crate-ctest-0.2.0 (c (n "ctest") (v "0.2.0") (d (list (d (n "cc") (r "^1.0.1") (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.59.1") (d #t) (k 0)))) (h "0308n4dk21qwxllcylszr9j16k870s2kyg8sn8faag5sicnbdddz")))

(define-public crate-ctest-0.2.1 (c (n "ctest") (v "0.2.1") (d (list (d (n "cc") (r "^1.0.1") (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.59.1") (d #t) (k 0)))) (h "1f1w3b2qkhq7xhysblcbsh89n7jv2pqknnirbz9i7pvf2s572jvz") (y #t)))

(define-public crate-ctest-0.2.2 (c (n "ctest") (v "0.2.2") (d (list (d (n "cc") (r "^1.0.1") (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.59.1") (d #t) (k 0)))) (h "0zq0a1f2wnbj58gvad09xdqdcd53jni1x5p8g99pswi4xzjy8hl6")))

(define-public crate-ctest-0.2.3 (c (n "ctest") (v "0.2.3") (d (list (d (n "cc") (r "^1.0.1") (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.59.1") (d #t) (k 0)))) (h "1lp7r4pq5gvqfx94qjxbqm2iwxn7hpbvli9ig8lm0pqckqdqdblz")))

(define-public crate-ctest-0.2.4 (c (n "ctest") (v "0.2.4") (d (list (d (n "cc") (r "^1.0.1") (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.59.1") (d #t) (k 0)))) (h "01xv2z63839jswmlmbdwpw89583hzf14i19dvfzdlgsfiq8fkap2")))

(define-public crate-ctest-0.2.5 (c (n "ctest") (v "0.2.5") (d (list (d (n "cc") (r "^1.0.1") (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.59.1") (d #t) (k 0)))) (h "1p2ydws88h7pb9rs43iynhbwmipvkp3d50d25l8y5gy0gcvyh6ig")))

(define-public crate-ctest-0.2.6 (c (n "ctest") (v "0.2.6") (d (list (d (n "cc") (r "^1.0.1") (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.59.1") (d #t) (k 0)))) (h "13cias43lgwkqyybbrg4x9bx7xxnzihqy730cjk7pfp0fs7y6dr1")))

(define-public crate-ctest-0.2.7 (c (n "ctest") (v "0.2.7") (d (list (d (n "cc") (r "^1.0.1") (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.59.1") (d #t) (k 0)))) (h "0yi8r33h59yj3ncd2yr48y98gvnzr6drkh9mnk751mh4ns7qjjhj")))

(define-public crate-ctest-0.2.8 (c (n "ctest") (v "0.2.8") (d (list (d (n "cc") (r "^1.0.1") (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.59.1") (d #t) (k 0)))) (h "1f5m9rmx9sml3z1zwazpmnzb0l96mqsqy8z4s58cn98wr7nw0x1m")))

(define-public crate-ctest-0.2.10 (c (n "ctest") (v "0.2.10") (d (list (d (n "cc") (r "^1.0.1") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.59.1") (d #t) (k 0)))) (h "0awhcmr1jqx3sp6iaam8k37np2i8lr3jpiyx6lxphm73v6085mmx")))

(define-public crate-ctest-0.2.11 (c (n "ctest") (v "0.2.11") (d (list (d (n "cc") (r "^1.0.1") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.59.1") (d #t) (k 0)))) (h "0hif067w09qcfigyv43ki0rpmmg32hh6qglwda1bisr3ym669f68")))

(define-public crate-ctest-0.2.12 (c (n "ctest") (v "0.2.12") (d (list (d (n "cc") (r "^1.0.1") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.59.1") (d #t) (k 0)))) (h "1zmfmaappagwyc72cdh2jjcbnr9807ynica4lj6gxywcf8zch3b6")))

(define-public crate-ctest-0.2.13 (c (n "ctest") (v "0.2.13") (d (list (d (n "cc") (r "^1.0.1") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.59.1") (d #t) (k 0)))) (h "1fq1nik51cqdcq3wmglwhyx89s31c52sraz0qaw3pnzby0569pgv")))

(define-public crate-ctest-0.2.14 (c (n "ctest") (v "0.2.14") (d (list (d (n "cc") (r "^1.0.1") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.59.1") (d #t) (k 0)))) (h "19g3qzln43v6w98yr3gkchzikpzvccl3433h5jpl4s0vnnil063z")))

(define-public crate-ctest-0.2.15 (c (n "ctest") (v "0.2.15") (d (list (d (n "cc") (r "^1.0.1") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 0)) (d (n "syntex_syntax2") (r "^0.0.2") (d #t) (k 0)))) (h "0n8w8jgbsq7xx3bqz719vr455sdqha2pn5q0sj3wmwpnydfbzql2")))

(define-public crate-ctest-0.2.16 (c (n "ctest") (v "0.2.16") (d (list (d (n "cc") (r "^1.0.1") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 0)) (d (n "syntex_syntax2") (r "^0.0.2") (d #t) (k 0)))) (h "1c15cl58i991q78181glmb4z34bsffbbcrkb5710pniyr6ha93ia")))

(define-public crate-ctest-0.2.17 (c (n "ctest") (v "0.2.17") (d (list (d (n "cc") (r "^1.0.1") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 0)) (d (n "syntex_syntax2") (r "^0.0.2") (d #t) (k 0)))) (h "1rfhmn6f059nw6sg2r3g9qvbwqa05niik14dnbvhrwhkhcyfsd53")))

(define-public crate-ctest-0.2.18 (c (n "ctest") (v "0.2.18") (d (list (d (n "cc") (r "^1.0.1") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 0)) (d (n "syntex_syntax2") (r "^0.0.2") (d #t) (k 0)))) (h "1nf5a2jsh3lg9vx5aaf60kdfh2l669f3g4ppqrzslimqixlqh7wc")))

(define-public crate-ctest-0.2.19 (c (n "ctest") (v "0.2.19") (d (list (d (n "cc") (r "^1.0.1") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 0)) (d (n "syntex_syntax2") (r "^0.0.2") (d #t) (k 0)))) (h "04k4scaygdvaqikj0sqg1gw8rhx54skpkp250d84p84brmxgjf2j")))

(define-public crate-ctest-0.2.20 (c (n "ctest") (v "0.2.20") (d (list (d (n "cc") (r "^1.0.1") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 0)) (d (n "syntex_syntax2") (r "^0.0.2") (d #t) (k 0)))) (h "083kxk810zhqyk3dddrl9nlms28hfbbm5dl6zwphsbdqn1iffn5h")))

(define-public crate-ctest-0.2.21 (c (n "ctest") (v "0.2.21") (d (list (d (n "cc") (r "^1.0.1") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 0)) (d (n "syntex_syntax2") (r "^0.0.2") (d #t) (k 0)))) (h "0l0cli2py89rv2af72wbd10w0hjvrn1qvcv6dqvm8k3bzdmnjd4z")))

(define-public crate-ctest-0.2.22 (c (n "ctest") (v "0.2.22") (d (list (d (n "cc") (r "^1.0.1") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 0)) (d (n "syntex_syntax2") (r "^0.0.2") (d #t) (k 0)))) (h "1w5rwf78zmk29bsd8h8ir3qh7ynbkpwy4jhrknzaj9v015r7b1i8")))

