(define-module (crates-io ct -t ct-tracker-lib) #:use-module (crates-io))

(define-public crate-ct-tracker-lib-0.1.0 (c (n "ct-tracker-lib") (v "0.1.0") (d (list (d (n "app_dirs") (r "^1.2.1") (d #t) (k 0)) (d (n "bunt") (r "^0.2.4") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (f (quote ("serde"))) (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "1cvmlkmxz2jap3laabc3ijyykv8psqjcyk9zzpspl7gnjd1sx5ms")))

(define-public crate-ct-tracker-lib-0.1.1 (c (n "ct-tracker-lib") (v "0.1.1") (d (list (d (n "app_dirs") (r "^1.2.1") (d #t) (k 0)) (d (n "bunt") (r "^0.2.4") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (f (quote ("serde"))) (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "0i1gapqrngrnvi1x8ja4v7p0cmywmbryabhm6wvafcw9llaa2fsq")))

