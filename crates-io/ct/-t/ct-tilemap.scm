(define-module (crates-io ct -t ct-tilemap) #:use-module (crates-io))

(define-public crate-ct-tilemap-1.0.0 (c (n "ct-tilemap") (v "1.0.0") (d (list (d (n "bytemuck") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "const-str") (r "^0.5") (d #t) (k 2)) (d (n "libflate") (r "^2") (d #t) (k 0)))) (h "1j2dwmavwd1x1chkzvff8l3lmqv0fnr54fp52651hg0ysnqjj5qm")))

(define-public crate-ct-tilemap-1.1.0 (c (n "ct-tilemap") (v "1.1.0") (d (list (d (n "bytemuck") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "const-str") (r "^0.5") (d #t) (k 2)) (d (n "libflate") (r "^2") (d #t) (k 0)))) (h "1dhmn9j156gbsw3a1xzwr7j1fbs3zilw016n4q6aq87109b4yj9n")))

(define-public crate-ct-tilemap-1.1.1 (c (n "ct-tilemap") (v "1.1.1") (d (list (d (n "bytemuck") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "const-str") (r "^0.5") (d #t) (k 2)) (d (n "libflate") (r "^2") (d #t) (k 0)))) (h "0hn1rxa2ixisw8nc3jn3miz71lf48n6gmqz1160l7mjfzp40jlbc")))

(define-public crate-ct-tilemap-1.1.2 (c (n "ct-tilemap") (v "1.1.2") (d (list (d (n "bytemuck") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "const-str") (r "^0.5") (d #t) (k 2)) (d (n "libflate") (r "^2") (d #t) (k 0)))) (h "12sh490ny166xcfwv3xys14psk81pa1yf3sz7y4qphm4q4062hps")))

