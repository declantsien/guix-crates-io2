(define-module (crates-io ct re ctre-sys) #:use-module (crates-io))

(define-public crate-ctre-sys-5.4.0 (c (n "ctre-sys") (v "5.4.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1z737rnvc7w2j6qa1yi8k80pwjj7xp2q80jk6r4x5h28bawr5zdf") (l "CTRE_PhoenixCCI")))

