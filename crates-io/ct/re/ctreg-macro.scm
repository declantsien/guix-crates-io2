(define-module (crates-io ct re ctreg-macro) #:use-module (crates-io))

(define-public crate-ctreg-macro-1.0.0 (c (n "ctreg-macro") (v "1.0.0") (d (list (d (n "lazy_format") (r "^2.0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.81") (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "regex-automata") (r "^0.4.6") (f (quote ("meta"))) (d #t) (k 0)) (d (n "regex-syntax") (r "^0.8.2") (d #t) (k 0)) (d (n "syn") (r "^2.0.60") (f (quote ("parsing" "proc-macro"))) (k 0)) (d (n "thiserror") (r "^1.0.59") (d #t) (k 0)))) (h "1ll3nfgyw4xxnqjdmssqzbnanfp99l7n03bzcwdmx137bi3cw8xc")))

