(define-module (crates-io ct re ctreg) #:use-module (crates-io))

(define-public crate-ctreg-1.0.0 (c (n "ctreg") (v "1.0.0") (d (list (d (n "cool_asserts") (r "^2.0.3") (d #t) (k 2)) (d (n "ctreg-macro") (r "^1.0.0") (d #t) (k 0)) (d (n "regex-automata") (r "^0.4.6") (f (quote ("meta"))) (d #t) (k 0)) (d (n "regex-syntax") (r "^0.8.2") (d #t) (k 0)))) (h "0k6i9y2j4djhy1k4xdicaa2ys749in6ri6yav8wr6fqxmwmwxrhy")))

(define-public crate-ctreg-1.0.1 (c (n "ctreg") (v "1.0.1") (d (list (d (n "cool_asserts") (r "^2.0.3") (d #t) (k 2)) (d (n "ctreg-macro") (r "^1.0.0") (d #t) (k 0)) (d (n "regex-automata") (r "^0.4.6") (f (quote ("meta"))) (d #t) (k 0)) (d (n "regex-syntax") (r "^0.8.2") (d #t) (k 0)))) (h "1zbajbb4x53k4fgna1d14sv6izahrclgq852dckl2rfs5gynh4w3")))

(define-public crate-ctreg-1.0.2 (c (n "ctreg") (v "1.0.2") (d (list (d (n "cool_asserts") (r "^2.0.3") (d #t) (k 2)) (d (n "ctreg-macro") (r "^1.0.0") (d #t) (k 0)) (d (n "regex-automata") (r "^0.4.6") (f (quote ("meta"))) (d #t) (k 0)) (d (n "regex-syntax") (r "^0.8.2") (d #t) (k 0)))) (h "0vy6fn9jla238xzgpdbdkxcm19xkz1mgdlg2wb9g8dibnlrzqr8l")))

