(define-module (crates-io ct re ctrem) #:use-module (crates-io))

(define-public crate-ctrem-0.1.0 (c (n "ctrem") (v "0.1.0") (d (list (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "088y2y9z5vf70x6b4zk8pfjb2jbh6fpxkbfd02grb3w1mfary0p5")))

(define-public crate-ctrem-0.1.1 (c (n "ctrem") (v "0.1.1") (d (list (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "1bsrxrknsa8phi8lsrc6hj1cajdzxyms9ydb1q2h47j1yzyb2hdv")))

(define-public crate-ctrem-0.1.2 (c (n "ctrem") (v "0.1.2") (d (list (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "1p7qwhv3q5ljxvmx4f5p8lh27qpqa049jbblvn11dblglhkz4hgh")))

(define-public crate-ctrem-0.1.3 (c (n "ctrem") (v "0.1.3") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "0v01yqx2sr2sb3x0cp7s0llbbz1xsymimj3ryx588fr7zhkflz5k")))

(define-public crate-ctrem-0.1.4 (c (n "ctrem") (v "0.1.4") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "0q5yv35r4d45irmkiliffczb7v31b2zlpdvr2ryj1vh6ynj0wrrb")))

