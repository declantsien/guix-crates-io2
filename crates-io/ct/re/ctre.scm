(define-module (crates-io ct re ctre) #:use-module (crates-io))

(define-public crate-ctre-0.6.1 (c (n "ctre") (v "0.6.1") (d (list (d (n "ctre-sys") (r "^5.4.0") (d #t) (k 0)) (d (n "wpilib-sys") (r "^0.2.3") (o #t) (d #t) (k 0)))) (h "0l20ilfsp8ikbvxlfgjzli3lhy6sjiflfqvj76cn42anx4w90066") (f (quote (("usage-reporting" "wpilib-sys") ("serde" "ctre-sys/serde") ("default" "usage-reporting"))))))

