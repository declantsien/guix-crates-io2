(define-module (crates-io ct cl ctclib) #:use-module (crates-io))

(define-public crate-ctclib-0.1.0 (c (n "ctclib") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "ctclib-kenlm-sys") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "dhat") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "libm") (r "~0.2") (d #t) (k 0)) (d (n "ordered-float") (r "^2.0") (d #t) (k 0)) (d (n "pdqselect") (r "~0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0yb1zwh9j6k9567iry8qwrq86jwyalfxwigjx6mr2ncz7ypmp618") (f (quote (("kenlm" "ctclib-kenlm-sys") ("dhat-heap" "dhat") ("default" "kenlm"))))))

