(define-module (crates-io ct f_ ctf_tcp_utils) #:use-module (crates-io))

(define-public crate-ctf_tcp_utils-0.1.0 (c (n "ctf_tcp_utils") (v "0.1.0") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0srd14h8vhckwzia06n0cb3bb7vya850ygha8ps9743kmnwkw86k")))

(define-public crate-ctf_tcp_utils-0.2.0 (c (n "ctf_tcp_utils") (v "0.2.0") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0s8vm1r77cvbw32qa3lf9mnjagswl9gmnyz4y5llawzhyybbz932")))

(define-public crate-ctf_tcp_utils-0.2.1 (c (n "ctf_tcp_utils") (v "0.2.1") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1b1dz0x9hl46mx16m5b8qiiqz057bl1039kb9znfjcn1cnm2k7r7") (y #t)))

(define-public crate-ctf_tcp_utils-0.2.2 (c (n "ctf_tcp_utils") (v "0.2.2") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1i9my463mifilsrg3ax00v0ip5s0425rxrnnyb1n4gmridjm3lzc")))

(define-public crate-ctf_tcp_utils-0.2.3 (c (n "ctf_tcp_utils") (v "0.2.3") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0g2s4h3sa3j0zcc9zfsg9v7ql24j4yhx2g612cwa7k391mqy8png")))

(define-public crate-ctf_tcp_utils-0.2.4 (c (n "ctf_tcp_utils") (v "0.2.4") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "04w279nvfsmgp0gimn4bwklpi5jww602wh8csxc2lyfghfp3ivv1") (y #t)))

(define-public crate-ctf_tcp_utils-0.3.0 (c (n "ctf_tcp_utils") (v "0.3.0") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1agjdc5bl8c04q5589wxc58h4mk5ia1957rp241qi19hgnj5p8jp")))

(define-public crate-ctf_tcp_utils-0.4.0 (c (n "ctf_tcp_utils") (v "0.4.0") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "13p8y54kqxq41l7xmphv12qcx2bwg5f0qbxnx4rdr52gwv8gax36")))

(define-public crate-ctf_tcp_utils-0.4.1 (c (n "ctf_tcp_utils") (v "0.4.1") (d (list (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0a7zyd7pdb43vpcdspa6ifvr5hxcffl8a1743k8cx0n1wxzj9ld7")))

