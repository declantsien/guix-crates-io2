(define-module (crates-io ct ch ctchi_codegen) #:use-module (crates-io))

(define-public crate-ctchi_codegen-0.1.0 (c (n "ctchi_codegen") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits" "parsing" "proc-macro"))) (d #t) (k 0)))) (h "05li99x94hjk57af5ajbyxhrv447gy4yayr310v6190db9anlhnl")))

(define-public crate-ctchi_codegen-0.2.0 (c (n "ctchi_codegen") (v "0.2.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits" "parsing" "proc-macro"))) (d #t) (k 0)))) (h "0lysl5ds5amn47smyk15l3naamh4qp7w35v0k3dy9r84hd50srzn")))

