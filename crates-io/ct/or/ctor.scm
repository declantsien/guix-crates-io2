(define-module (crates-io ct or ctor) #:use-module (crates-io))

(define-public crate-ctor-0.1.0 (c (n "ctor") (v "0.1.0") (d (list (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full" "fold" "parsing" "printing" "proc-macro"))) (k 0)))) (h "128fpiizsa6z13l4b7rdpk0s0dr3b05m03iawshp8n3jlqb6gmqb")))

(define-public crate-ctor-0.1.1 (c (n "ctor") (v "0.1.1") (d (list (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full" "fold" "parsing" "printing" "proc-macro"))) (k 0)))) (h "1c06ah2gkc9w53mxbqwy3w81m2p8ifc3a7i5sxxgz6yjaw08snad")))

(define-public crate-ctor-0.1.2 (c (n "ctor") (v "0.1.2") (d (list (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full" "fold" "parsing" "printing" "proc-macro"))) (k 0)))) (h "0y960bgnx7w418zvw399hkgvsvdry50338n9lj4lrb7z6n91nv6d")))

(define-public crate-ctor-0.1.3 (c (n "ctor") (v "0.1.3") (d (list (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full" "fold" "parsing" "printing" "proc-macro"))) (k 0)))) (h "083nwp6qm8xzdwi0rz9x14b5x3l17wz2rl9i3gwdslvcr3c8bmcx")))

(define-public crate-ctor-0.1.4 (c (n "ctor") (v "0.1.4") (d (list (d (n "libc") (r "^0.2.44") (d #t) (k 2)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full" "fold" "parsing" "printing" "proc-macro"))) (k 0)))) (h "1kc4l5y2ii5ljcw8sxqdvcpxylsgl2bz2p30v5ln194j5ysgm84l")))

(define-public crate-ctor-0.1.5 (c (n "ctor") (v "0.1.5") (d (list (d (n "libc") (r "^0.2.44") (d #t) (k 2)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full" "fold" "parsing" "printing" "proc-macro"))) (k 0)))) (h "0gjp5bvlzvw5qkk03f79rmz1gdnwvrchfzy3dv488saa11flvqjx")))

(define-public crate-ctor-0.1.6 (c (n "ctor") (v "0.1.6") (d (list (d (n "libc-print") (r "^0.1.7") (d #t) (k 2)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full" "fold" "parsing" "printing" "proc-macro"))) (k 0)))) (h "0c0lmnb909yicvk5bnwb92j5m14dvd1j2pbp0cdglfkgd3vhlms7")))

(define-public crate-ctor-0.1.7 (c (n "ctor") (v "0.1.7") (d (list (d (n "libc-print") (r "^0.1.7") (d #t) (k 2)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full" "fold" "parsing" "printing" "proc-macro"))) (k 0)))) (h "1ma66pwryyi4f3n56pkhyc6ywxz432jr5j38l1mdrbswp8mxnhws")))

(define-public crate-ctor-0.1.8 (c (n "ctor") (v "0.1.8") (d (list (d (n "libc-print") (r "^0.1.7") (d #t) (k 2)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full" "fold" "parsing" "printing" "proc-macro"))) (k 0)))) (h "12x5gmw0100b35qc0r9v4bi7alq60qnsh7ffa737kycvfmy1rk75")))

(define-public crate-ctor-0.1.9 (c (n "ctor") (v "0.1.9") (d (list (d (n "libc-print") (r "^0.1.7") (d #t) (k 2)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full" "fold" "parsing" "printing" "proc-macro"))) (k 0)))) (h "0b096a0i9q00gbkwhmqw2jn7zp9r4sw88039bwmjbha3jrhifk1v")))

(define-public crate-ctor-0.1.10 (c (n "ctor") (v "0.1.10") (d (list (d (n "libc-print") (r "^0.1.7") (d #t) (k 2)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "fold" "parsing" "printing" "proc-macro"))) (k 0)))) (h "0a2m6vjpdg4k3z1bhnzc8hnnz4cgrqrcax1ls2zyyafca93jyssv")))

(define-public crate-ctor-0.1.11 (c (n "ctor") (v "0.1.11") (d (list (d (n "libc-print") (r "^0.1.7") (d #t) (k 2)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "fold" "parsing" "printing" "proc-macro"))) (k 0)))) (h "1rvxl9i9a2j40m00zcjcsfa31n4zzyjvj9vwmk6bp0zgxckif1iy")))

(define-public crate-ctor-0.1.12 (c (n "ctor") (v "0.1.12") (d (list (d (n "libc-print") (r "^0.1.7") (d #t) (k 2)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "parsing" "printing" "proc-macro"))) (k 0)))) (h "1p7fd2zp3lkb098sn740jlf3np8qg5ivycsc037b4jhqsixf736d")))

(define-public crate-ctor-0.1.13 (c (n "ctor") (v "0.1.13") (d (list (d (n "libc-print") (r "^0.1.7") (d #t) (k 2)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "parsing" "printing" "proc-macro"))) (k 0)))) (h "1qgwkwyxishpp3wkbwq5i27zdxz539ii0sz129xj061ffnnfbia7")))

(define-public crate-ctor-0.1.14 (c (n "ctor") (v "0.1.14") (d (list (d (n "libc-print") (r "^0.1.7") (d #t) (k 2)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "parsing" "printing" "proc-macro"))) (k 0)))) (h "17fd8dzlcrgc07367yvlqw5jddzzrzwfzcmdsxa5r6f1kbp2asyg")))

(define-public crate-ctor-0.1.15 (c (n "ctor") (v "0.1.15") (d (list (d (n "libc-print") (r "^0.1.7") (d #t) (k 2)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "parsing" "printing" "proc-macro"))) (k 0)))) (h "09x2my9x33srjdip8yf4lm5gq7xqis2694abvpa64r60pajqm19r")))

(define-public crate-ctor-0.1.16 (c (n "ctor") (v "0.1.16") (d (list (d (n "libc-print") (r "^0.1.7") (d #t) (k 2)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "parsing" "printing" "proc-macro"))) (k 0)))) (h "11040byxccmjc8c83mhrp90y3a5bqvjkna8i4csm0c4m5knapfkz")))

(define-public crate-ctor-0.1.17 (c (n "ctor") (v "0.1.17") (d (list (d (n "libc-print") (r "^0.1.15") (d #t) (k 2)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0.56") (f (quote ("full" "parsing" "printing" "proc-macro"))) (k 0)))) (h "19bdcqvfj9ia0k1qw46zysrlqps2v2vh2xqhyqq94bkfa3cqhg1p")))

(define-public crate-ctor-0.1.18 (c (n "ctor") (v "0.1.18") (d (list (d (n "libc-print") (r "^0.1.15") (d #t) (k 2)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0.58") (f (quote ("full" "parsing" "printing" "proc-macro"))) (k 0)))) (h "0q2mbp4vg3kx7361v5r7q7f4rdh6571fllxvzym04w5zvkbvkg0h")))

(define-public crate-ctor-0.1.19 (c (n "ctor") (v "0.1.19") (d (list (d (n "libc-print") (r "^0.1.15") (d #t) (k 2)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("full" "parsing" "printing" "proc-macro"))) (k 0)))) (h "06bbsv7lm9c9mfdg896vdxmdkkammc0sa56n2x4fzg0psjd5vx78")))

(define-public crate-ctor-0.1.20 (c (n "ctor") (v "0.1.20") (d (list (d (n "libc-print") (r "^0.1.15") (d #t) (k 2)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.64") (f (quote ("full" "parsing" "printing" "proc-macro"))) (k 0)))) (h "0v80naiw5fp81xkyfkds6jpyamf3wx43kz4nif936bkq3any562y")))

(define-public crate-ctor-0.1.21 (c (n "ctor") (v "0.1.21") (d (list (d (n "libc-print") (r "^0.1.15") (d #t) (k 2)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("full" "parsing" "printing" "proc-macro"))) (k 0)))) (h "1am0a8m1gkaa0fii3w3s5wsymjljvg4sv5c50bscssl2kf5a9h6c")))

(define-public crate-ctor-0.1.22 (c (n "ctor") (v "0.1.22") (d (list (d (n "libc-print") (r "^0.1.15") (d #t) (k 2)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("full" "parsing" "printing" "proc-macro"))) (k 0)))) (h "1712zdlrmcfkfw38dkj67xg72fd0p9slyqqi64c6n94zgi7vwxzq")))

(define-public crate-ctor-0.1.23 (c (n "ctor") (v "0.1.23") (d (list (d (n "libc-print") (r "^0.1.20") (d #t) (k 2)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full" "parsing" "printing" "proc-macro"))) (k 0)))) (h "1jzlplcl2fi951hw0mpjsx3a8gi90bjkz0vgd7wi06jj3mzfizyd")))

(define-public crate-ctor-0.1.24 (c (n "ctor") (v "0.1.24") (d (list (d (n "libc-print") (r "^0.1.20") (d #t) (k 2)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full" "parsing" "printing" "proc-macro"))) (k 0)))) (h "1g40gn2vb0vamzbnax0ywv1dfkfndpmvbamkx50ipvrz7a78ncjl")))

(define-public crate-ctor-0.1.25 (c (n "ctor") (v "0.1.25") (d (list (d (n "libc-print") (r "^0.1.20") (d #t) (k 2)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full" "parsing" "printing" "proc-macro"))) (k 0)))) (h "1fdlar4ig3crqj15m8q17sq321br93aj7yw4bl3fz2rlssbcq60g")))

(define-public crate-ctor-0.1.26 (c (n "ctor") (v "0.1.26") (d (list (d (n "libc-print") (r "^0.1.20") (d #t) (k 2)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full" "parsing" "printing" "proc-macro"))) (k 0)))) (h "15m0wqhv12p25xkxz5dxvg23r7a6bkh7p8zi1cdhgswjhdl028vd")))

(define-public crate-ctor-0.2.0 (c (n "ctor") (v "0.2.0") (d (list (d (n "libc-print") (r "^0.1.20") (d #t) (k 2)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^2.0.10") (f (quote ("full" "parsing" "printing" "proc-macro"))) (k 0)))) (h "06rhrw85py0gkk7g99qk124mk6d5isq95nn3abc84fyf7zv5ch6x")))

(define-public crate-ctor-0.2.1 (c (n "ctor") (v "0.2.1") (d (list (d (n "libc-print") (r "^0.1.20") (d #t) (k 2)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^2.0.12") (f (quote ("full" "parsing" "printing" "proc-macro"))) (k 0)))) (h "0xgfy3i7m0zcdipjsz5i1gsi4isppp20y300lqj9f96z19s402lr") (f (quote (("used_linker"))))))

(define-public crate-ctor-0.2.2 (c (n "ctor") (v "0.2.2") (d (list (d (n "libc-print") (r "^0.1.20") (d #t) (k 2)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^2.0.12") (f (quote ("full" "parsing" "printing" "proc-macro"))) (k 0)))) (h "00g9qvyj19afvsjvya74ms0acnzcz90llns7czv43aqxidhgm1hm") (f (quote (("used_linker"))))))

(define-public crate-ctor-0.2.3 (c (n "ctor") (v "0.2.3") (d (list (d (n "libc-print") (r "^0.1.20") (d #t) (k 2)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^2.0.12") (f (quote ("full" "parsing" "printing" "proc-macro"))) (k 0)))) (h "1i88nr3p2qdi5kzp2l15js9qd5924jfbywlw3q95jx9wv7qgzmgf") (f (quote (("used_linker"))))))

(define-public crate-ctor-0.2.4 (c (n "ctor") (v "0.2.4") (d (list (d (n "libc-print") (r "^0.1.20") (d #t) (k 2)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^2.0.12") (f (quote ("full" "parsing" "printing" "proc-macro"))) (k 0)))) (h "0fqrmajny82r6bmhvnb6pa2iqkfgrwzcps4xkqvlb1nbkfdbld0z") (f (quote (("used_linker"))))))

(define-public crate-ctor-0.2.5 (c (n "ctor") (v "0.2.5") (d (list (d (n "libc-print") (r "^0.1.20") (d #t) (k 2)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^2.0.12") (f (quote ("full" "parsing" "printing" "proc-macro"))) (k 0)))) (h "10zmxqyng9jj928ihflcb67w9797dfv1z6dhaj3xscndz2zndqrp") (f (quote (("used_linker"))))))

(define-public crate-ctor-0.2.6 (c (n "ctor") (v "0.2.6") (d (list (d (n "libc-print") (r "^0.1.20") (d #t) (k 2)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^2.0.12") (f (quote ("full" "parsing" "printing" "proc-macro"))) (k 0)))) (h "17hrvsrkwzpnz4px1r179bffc2lmiwav159igrvhf5w63rrb7lih") (f (quote (("used_linker"))))))

(define-public crate-ctor-0.2.7 (c (n "ctor") (v "0.2.7") (d (list (d (n "libc-print") (r "^0.1.20") (d #t) (k 2)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^2.0.12") (f (quote ("full" "parsing" "printing" "proc-macro"))) (k 0)))) (h "0p289www67xwnxsqgvlh3mzi0zmdysxsqf4cx2kvgfcj96kiladd") (f (quote (("used_linker"))))))

(define-public crate-ctor-0.2.8 (c (n "ctor") (v "0.2.8") (d (list (d (n "libc-print") (r "^0.1.20") (d #t) (k 2)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^2.0.12") (f (quote ("full" "parsing" "printing" "proc-macro"))) (k 0)))) (h "17yxfh3qkj9l6a1rmb8crw8189f1rw4a5m5c2xdy8gigh9j93d7d") (f (quote (("used_linker"))))))

