(define-module (crates-io ct or ctor-lite) #:use-module (crates-io))

(define-public crate-ctor-lite-0.1.0 (c (n "ctor-lite") (v "0.1.0") (d (list (d (n "macro_rules_attribute") (r "^0.2.0") (d #t) (k 2)) (d (n "rustix") (r "^0.38.34") (f (quote ("stdio"))) (k 2)))) (h "0fwyn69gb0ly5lzhbxjdsp90h1s6jhaxw0rrw2n7gchs401ihy8z") (f (quote (("std" "alloc") ("default" "std") ("alloc")))) (r "1.36.0")))

