(define-module (crates-io ct or ctor-used-linker) #:use-module (crates-io))

(define-public crate-ctor-used-linker-0.2.0 (c (n "ctor-used-linker") (v "0.2.0") (d (list (d (n "libc-print") (r "^0.1.20") (d #t) (k 2)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^2.0.12") (f (quote ("full" "parsing" "printing" "proc-macro"))) (k 0)))) (h "172xw2xm28nr15qmzazmsx15w1k4ls8yx16cpcyagka8rqr4kqqs")))

