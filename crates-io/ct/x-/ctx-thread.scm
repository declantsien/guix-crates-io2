(define-module (crates-io ct x- ctx-thread) #:use-module (crates-io))

(define-public crate-ctx-thread-0.1.0 (c (n "ctx-thread") (v "0.1.0") (d (list (d (n "cfg-if") (r "^1") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.8") (d #t) (k 0)))) (h "0rmkv8ikxmlarv610ps4rib82h5p4q4dc3rwa59zjq0pkvaybv8j")))

(define-public crate-ctx-thread-0.1.1 (c (n "ctx-thread") (v "0.1.1") (d (list (d (n "cfg-if") (r "^1") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.8") (d #t) (k 0)))) (h "0rx5wvn0zc7qwlbk300nlxdcnqdrwk0dnmwykyx8by9vwswjnzls")))

