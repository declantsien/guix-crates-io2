(define-module (crates-io ct xm ctxmap) #:use-module (crates-io))

(define-public crate-ctxmap-0.1.0 (c (n "ctxmap") (v "0.1.0") (d (list (d (n "ctor") (r "^0.1.21") (d #t) (k 0)) (d (n "inventory") (r "^0.1.10") (d #t) (k 0)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)))) (h "00wbyb2w7jm7vwnpjmd2vc8rd77p6g0y44nj7kpi60wkm8ngmzyj") (y #t)))

(define-public crate-ctxmap-0.2.0 (c (n "ctxmap") (v "0.2.0") (d (list (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)))) (h "1saqg3z0w5ks8h3kxjn4wlpfvlqavd04d8cinvm9hjf40vyps389") (y #t)))

(define-public crate-ctxmap-0.3.0 (c (n "ctxmap") (v "0.3.0") (d (list (d (n "once_cell") (r "^1.12.0") (d #t) (k 0)))) (h "169ccrxz2vyphv3xr65i00i10p1y2j5q2b2vbsdask07l6bmpqv6")))

(define-public crate-ctxmap-0.4.0 (c (n "ctxmap") (v "0.4.0") (d (list (d (n "once_cell") (r "^1.12.0") (d #t) (k 0)))) (h "0b9sn9rajkv4w7im6rkjw7ykxnb513zr6w3b1b10v72ciyn635md")))

(define-public crate-ctxmap-0.5.0 (c (n "ctxmap") (v "0.5.0") (d (list (d (n "once_cell") (r "^1.12.0") (d #t) (k 0)))) (h "15w9a79y4mng317angdf5avqzzb7p96wgxb9p1fjld8xcdjlrz26")))

