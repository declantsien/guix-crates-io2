(define-module (crates-io wo kw wokwi_chip_ll) #:use-module (crates-io))

(define-public crate-wokwi_chip_ll-0.1.0 (c (n "wokwi_chip_ll") (v "0.1.0") (h "1ixidha52g6d3crkbvhqay1jqgag4n0jqlq26059336l6x9qbfyb")))

(define-public crate-wokwi_chip_ll-0.1.1 (c (n "wokwi_chip_ll") (v "0.1.1") (h "0f13naq4lspxmyaf0jzzbjyv63a6ix341lyy9p8rli21waa1prlw")))

(define-public crate-wokwi_chip_ll-0.1.2 (c (n "wokwi_chip_ll") (v "0.1.2") (h "1yqjnnb09r8jcz846grfbp0q2sf2c50v8938vsfhnm7461w2qs2f")))

(define-public crate-wokwi_chip_ll-0.1.3 (c (n "wokwi_chip_ll") (v "0.1.3") (h "0ppgwpb6vgsh1y904s15ssiswkn5cq2chw0c9h9vhhm2lxa050y9")))

