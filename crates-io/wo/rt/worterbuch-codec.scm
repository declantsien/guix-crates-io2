(define-module (crates-io wo rt worterbuch-codec) #:use-module (crates-io))

(define-public crate-worterbuch-codec-0.15.0 (c (n "worterbuch-codec") (v "0.15.0") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0.140") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)) (d (n "tokio") (r "^1.20.1") (f (quote ("io-util"))) (d #t) (k 0)) (d (n "tokio-test") (r "^0.4.2") (d #t) (k 2)))) (h "1g3i983cpj9rq75jrynbq63i1ms04rbhmw0v5bvgg9qhyc0m99qj")))

(define-public crate-worterbuch-codec-0.15.1 (c (n "worterbuch-codec") (v "0.15.1") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0.140") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)) (d (n "tokio") (r "^1.20.1") (f (quote ("io-util"))) (d #t) (k 0)) (d (n "tokio-test") (r "^0.4.2") (d #t) (k 2)))) (h "1myn0fpxgv6m82ils18yz6gd89sgav4bvsgp4nm614jlxbivlqww")))

(define-public crate-worterbuch-codec-0.17.0 (c (n "worterbuch-codec") (v "0.17.0") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0.140") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)) (d (n "tokio") (r "^1.20.1") (f (quote ("io-util"))) (d #t) (k 0)) (d (n "tokio-test") (r "^0.4.2") (d #t) (k 2)))) (h "1s2wrlrm8jwdzb28771j3r4lk5ra5wx5wx1vwsq47mvjsw6r6gmc")))

(define-public crate-worterbuch-codec-0.19.0 (c (n "worterbuch-codec") (v "0.19.0") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0.140") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)) (d (n "tokio") (r "^1.20.1") (f (quote ("io-util"))) (d #t) (k 0)) (d (n "tokio-test") (r "^0.4.2") (d #t) (k 2)))) (h "1d9gwv5p68hgb017b2az7lk7g8vr6012qfvxnmcwxbhz3wl2rgr2")))

(define-public crate-worterbuch-codec-0.20.0 (c (n "worterbuch-codec") (v "0.20.0") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0.140") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)) (d (n "tokio") (r "^1.20.1") (f (quote ("io-util"))) (d #t) (k 0)) (d (n "tokio-test") (r "^0.4.2") (d #t) (k 2)))) (h "0r0mzrp92hijsln8hcqk7acbcgz09ypkgpcm5rzdc4mhj6qccjq1")))

(define-public crate-worterbuch-codec-0.21.0 (c (n "worterbuch-codec") (v "0.21.0") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0.140") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)) (d (n "tokio") (r "^1.20.1") (f (quote ("io-util"))) (d #t) (k 0)) (d (n "tokio-test") (r "^0.4.2") (d #t) (k 2)))) (h "08fgvpfsjhdixjjks02anq6b01b94plqp0832nrlhlcsnd61n6w9")))

