(define-module (crates-io wo rt worterbuch-openapi) #:use-module (crates-io))

(define-public crate-worterbuch-openapi-0.34.0 (c (n "worterbuch-openapi") (v "0.34.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)) (d (n "uuid") (r "^1.0") (f (quote ("serde"))) (d #t) (k 0)))) (h "0z3wsxkj2dfhphjh0q3079cyyw3znz8a6dljpp188aplvlry4a9d")))

