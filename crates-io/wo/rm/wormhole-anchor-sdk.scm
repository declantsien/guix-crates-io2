(define-module (crates-io wo rm wormhole-anchor-sdk) #:use-module (crates-io))

(define-public crate-wormhole-anchor-sdk-0.1.0-alpha.1 (c (n "wormhole-anchor-sdk") (v "0.1.0-alpha.1") (d (list (d (n "anchor-lang") (r "^0.28.0") (d #t) (k 0)) (d (n "anchor-spl") (r "^0.28.0") (d #t) (k 0)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)))) (h "1xii4kmlg1755dv37v71zndil502p4ip3djwjkibc4r1znwrwy6i") (f (quote (("token-bridge") ("tilt-devnet") ("solana-devnet") ("mainnet") ("default" "mainnet"))))))

(define-public crate-wormhole-anchor-sdk-0.1.0-alpha.2 (c (n "wormhole-anchor-sdk") (v "0.1.0-alpha.2") (d (list (d (n "anchor-lang") (r "^0.28.0") (d #t) (k 0)) (d (n "anchor-spl") (r "^0.28.0") (d #t) (k 0)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)))) (h "18inawh7i29va63g80d1m7k14ng3ng8yriibarxzvc2dcxfm2nyx") (f (quote (("token-bridge") ("tilt-devnet") ("solana-devnet") ("mainnet") ("default" "mainnet"))))))

(define-public crate-wormhole-anchor-sdk-0.29.0-alpha.1 (c (n "wormhole-anchor-sdk") (v "0.29.0-alpha.1") (d (list (d (n "anchor-lang") (r "^0.29.0") (d #t) (k 0)) (d (n "anchor-spl") (r "^0.29.0") (d #t) (k 0)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)))) (h "1w4plwldcjfzyr0lj05684f9z10rz6jdyq93bsqshwhhmk4xaw24") (f (quote (("token-bridge") ("tilt-devnet") ("solana-devnet") ("mainnet") ("default" "mainnet"))))))

