(define-module (crates-io wo rm wormhole-solana-utils) #:use-module (crates-io))

(define-public crate-wormhole-solana-utils-0.2.0-alpha.6 (c (n "wormhole-solana-utils") (v "0.2.0-alpha.6") (d (list (d (n "anchor-lang") (r "^0.28.0") (d #t) (k 0)))) (h "0052rf7lvrcr49l237zzcwn8w2vvc4mxgva2l88xgs3jxjv3gwad") (f (quote (("cpi")))) (r "1.68")))

(define-public crate-wormhole-solana-utils-0.2.0-alpha.7 (c (n "wormhole-solana-utils") (v "0.2.0-alpha.7") (d (list (d (n "anchor-lang") (r "^0.28.0") (o #t) (d #t) (k 0)) (d (n "solana-program") (r "^1.16.27") (d #t) (k 0)))) (h "01v6xbpag0wry75fjnn21jy8zssq9nsd1kp5hdkvgqv8ff50hnx9") (s 2) (e (quote (("anchor" "dep:anchor-lang")))) (r "1.68")))

(define-public crate-wormhole-solana-utils-0.2.0-alpha.8 (c (n "wormhole-solana-utils") (v "0.2.0-alpha.8") (d (list (d (n "anchor-lang") (r "^0.28.0") (o #t) (d #t) (k 0)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "solana-program") (r "^1.16.27") (d #t) (k 0)))) (h "0jnjy9r1x5sy3s0cgynrsjbima5am76xxz81va79s4fslg9y7liy") (s 2) (e (quote (("anchor" "dep:anchor-lang")))) (r "1.68")))

(define-public crate-wormhole-solana-utils-0.2.0-alpha.9 (c (n "wormhole-solana-utils") (v "0.2.0-alpha.9") (d (list (d (n "anchor-lang") (r ">=0.28.0") (o #t) (d #t) (k 0)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "solana-program") (r "^1.16.27") (d #t) (k 0)))) (h "0dnlssicrjyr2xqs43lk6ywm4i6zps1g3r2qn2gjxs65h0kibg3c") (s 2) (e (quote (("anchor" "dep:anchor-lang")))) (r "1.68")))

(define-public crate-wormhole-solana-utils-0.2.0-alpha.10 (c (n "wormhole-solana-utils") (v "0.2.0-alpha.10") (d (list (d (n "anchor-lang") (r ">=0.28.0, <0.30.0") (o #t) (d #t) (k 0)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "solana-program") (r "^1.16.27") (d #t) (k 0)))) (h "1v8fia4gxbgx6pa5jb9vajz7i6dnaqmbdwdz5iab6j9gmflimrbi") (s 2) (e (quote (("anchor" "dep:anchor-lang")))) (r "1.68")))

(define-public crate-wormhole-solana-utils-0.2.0-alpha.11 (c (n "wormhole-solana-utils") (v "0.2.0-alpha.11") (d (list (d (n "anchor-lang") (r ">=0.28.0, <0.30.0") (o #t) (d #t) (k 0)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "solana-program") (r "^1.16.27") (d #t) (k 0)))) (h "0j55fn67aa90kscdwx51w9j69rwp21kdlfmzr846v5mnp28knkgj") (f (quote (("default" "anchor")))) (s 2) (e (quote (("anchor" "dep:anchor-lang")))) (r "1.68")))

(define-public crate-wormhole-solana-utils-0.2.0-alpha.12 (c (n "wormhole-solana-utils") (v "0.2.0-alpha.12") (d (list (d (n "anchor-lang") (r ">=0.28.0, <0.30.0") (o #t) (d #t) (k 0)) (d (n "solana-program") (r "^1.16.27") (d #t) (k 0)))) (h "18lwpcb7jrsnd207ax63vhs3l3f6h1c65v80jyabcfmkxdd6yc21") (f (quote (("default" "anchor")))) (s 2) (e (quote (("anchor" "dep:anchor-lang")))) (r "1.68")))

(define-public crate-wormhole-solana-utils-0.2.0-alpha.14 (c (n "wormhole-solana-utils") (v "0.2.0-alpha.14") (d (list (d (n "anchor-lang") (r ">=0.28.0, <0.30.0") (o #t) (d #t) (k 0)) (d (n "solana-program") (r "^1.16.27") (d #t) (k 0)))) (h "1lwwr55j1pydqf96xrlng08dp41wigzaa6zcyxjy93xvnb0lv7h5") (f (quote (("default" "anchor")))) (s 2) (e (quote (("anchor" "dep:anchor-lang")))) (r "1.68")))

(define-public crate-wormhole-solana-utils-0.2.0-alpha.15 (c (n "wormhole-solana-utils") (v "0.2.0-alpha.15") (d (list (d (n "anchor-lang") (r ">=0.28.0, <0.30.0") (o #t) (d #t) (k 0)) (d (n "solana-program") (r "^1.16.27") (d #t) (k 0)))) (h "0x2pf58ykv594mpm40570h17z3vxv8f605c2wlpqaww7p0ml3r7a") (f (quote (("default" "anchor")))) (s 2) (e (quote (("anchor" "dep:anchor-lang")))) (r "1.68")))

(define-public crate-wormhole-solana-utils-0.2.0-alpha.16 (c (n "wormhole-solana-utils") (v "0.2.0-alpha.16") (d (list (d (n "anchor-lang") (r ">=0.28.0, <0.31.0") (o #t) (d #t) (k 0)) (d (n "solana-program") (r "^1.16.27") (d #t) (k 0)))) (h "0469rq4kdxn5wm96y7p2zi9zypbfb3gsvxrmsd7z7ci0i8nyb7nx") (f (quote (("default" "anchor")))) (s 2) (e (quote (("anchor" "dep:anchor-lang")))) (r "1.68")))

(define-public crate-wormhole-solana-utils-0.3.0-alpha.0 (c (n "wormhole-solana-utils") (v "0.3.0-alpha.0") (d (list (d (n "anchor-lang") (r ">=0.29.0") (o #t) (d #t) (k 0)) (d (n "solana-program") (r "^1.17.20") (d #t) (k 0)))) (h "0daj5srv08zfnrcd924aldz2y67fysgr8alixrr2gn58f98a4wjk") (f (quote (("default" "anchor")))) (s 2) (e (quote (("anchor" "dep:anchor-lang")))) (r "1.68")))

(define-public crate-wormhole-solana-utils-0.3.0-alpha.1 (c (n "wormhole-solana-utils") (v "0.3.0-alpha.1") (d (list (d (n "anchor-lang") (r ">=0.29.0") (o #t) (d #t) (k 0)) (d (n "solana-program") (r "^1.18.10") (d #t) (k 0)))) (h "1kzr5cvmwhihg5gz8d71k88aq78ihvmdkdlcagbccwrx0apx36yl") (f (quote (("default" "anchor")))) (s 2) (e (quote (("anchor" "dep:anchor-lang")))) (r "1.75")))

