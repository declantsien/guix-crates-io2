(define-module (crates-io wo rm wormhole) #:use-module (crates-io))

(define-public crate-wormhole-0.1.0 (c (n "wormhole") (v "0.1.0") (d (list (d (n "shiplift") (r "^0.2") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.3.3") (d #t) (k 0)))) (h "1caab9k420ri932pxjkx5dg15n4lnfr65q4vbih5cqhpzxbnvbdi")))

