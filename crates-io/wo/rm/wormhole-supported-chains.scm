(define-module (crates-io wo rm wormhole-supported-chains) #:use-module (crates-io))

(define-public crate-wormhole-supported-chains-0.0.0-alpha.1 (c (n "wormhole-supported-chains") (v "0.0.0-alpha.1") (d (list (d (n "serde") (r "^1") (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1j7wr3bh5dg8rwhq8imshkp1qsa51n2k4c10h7dk59l2sm59lks0")))

(define-public crate-wormhole-supported-chains-0.1.0 (c (n "wormhole-supported-chains") (v "0.1.0") (d (list (d (n "serde") (r "^1") (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "192br5r71000smsjz95197wq4zdqbc8vh2kvgp63fa914h5ahhiz")))

