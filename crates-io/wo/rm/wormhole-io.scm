(define-module (crates-io wo rm wormhole-io) #:use-module (crates-io))

(define-public crate-wormhole-io-0.0.15 (c (n "wormhole-io") (v "0.0.15") (d (list (d (n "alloy-primitives") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "ruint") (r "=1.8") (o #t) (k 0)) (d (n "ruint-macro") (r "=1.0.2") (o #t) (d #t) (k 0)))) (h "0aq22wq6219ljn71f1yc0nhpz4bvvcvv6canf2ldkjw8mvhfncvw") (y #t) (s 2) (e (quote (("ruint" "dep:ruint" "dep:ruint-macro") ("alloy" "dep:alloy-primitives")))) (r "1.65")))

(define-public crate-wormhole-io-0.0.1 (c (n "wormhole-io") (v "0.0.1") (d (list (d (n "alloy-primitives") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "ruint") (r "=1.8") (o #t) (k 0)) (d (n "ruint-macro") (r "=1.0.2") (o #t) (d #t) (k 0)))) (h "1r9wsia0lwmcw5grq3ay7xnrbq2ckrikbfv8cig2vii7s6j19jm5") (y #t) (s 2) (e (quote (("ruint" "dep:ruint" "dep:ruint-macro") ("alloy" "dep:alloy-primitives")))) (r "1.65")))

(define-public crate-wormhole-io-0.0.16 (c (n "wormhole-io") (v "0.0.16") (d (list (d (n "alloy-primitives") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "ruint") (r "=1.8") (o #t) (k 0)) (d (n "ruint-macro") (r "=1.0.2") (o #t) (d #t) (k 0)))) (h "1355qlrs1dxm29dnj24yampprj95viddnpksbfh7cz91az5yqrx0") (y #t) (s 2) (e (quote (("ruint" "dep:ruint" "dep:ruint-macro") ("alloy" "dep:alloy-primitives"))))))

(define-public crate-wormhole-io-0.0.0-alpha.1 (c (n "wormhole-io") (v "0.0.0-alpha.1") (d (list (d (n "alloy-primitives") (r "^0.2.0") (o #t) (d #t) (k 0)))) (h "1qhyrb7bka0xwfz2cq9ncgz2mrpq8smrsdm5kkv84yc466dx240z") (y #t) (s 2) (e (quote (("alloy" "dep:alloy-primitives"))))))

(define-public crate-wormhole-io-0.1.0 (c (n "wormhole-io") (v "0.1.0") (d (list (d (n "alloy-primitives") (r "^0.2.0") (o #t) (d #t) (k 0)))) (h "03w31slzkdvj8iigp1l3drh4zkyhnrw17cfhn2xm1pw3nfhbc9k9") (s 2) (e (quote (("alloy" "dep:alloy-primitives"))))))

(define-public crate-wormhole-io-0.1.1 (c (n "wormhole-io") (v "0.1.1") (d (list (d (n "alloy-primitives") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "hex-literal") (r "^0.4.1") (d #t) (k 2)))) (h "0rawy8vnjs1m7r50gkxf92iqs1mp7rp7qczr5hybxplrg9pz8ra9") (s 2) (e (quote (("alloy" "dep:alloy-primitives"))))))

(define-public crate-wormhole-io-0.1.2 (c (n "wormhole-io") (v "0.1.2") (d (list (d (n "alloy-primitives") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "hex-literal") (r "^0.4.1") (d #t) (k 2)))) (h "0qkiyy4y7d4flq83swv5whyal825gndv4h5n76ahh3x4fx8zr2x0") (s 2) (e (quote (("alloy" "dep:alloy-primitives"))))))

(define-public crate-wormhole-io-0.1.4 (c (n "wormhole-io") (v "0.1.4") (d (list (d (n "alloy-primitives") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "hex-literal") (r "^0.4.1") (d #t) (k 2)))) (h "0pv01pkl2adb4l803bbzlxahdamhgblaa4ns7chxsxzp0w5hcmmz") (y #t) (s 2) (e (quote (("alloy" "dep:alloy-primitives"))))))

(define-public crate-wormhole-io-0.1.3 (c (n "wormhole-io") (v "0.1.3") (d (list (d (n "alloy-primitives") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "hex-literal") (r "^0.4.1") (d #t) (k 2)))) (h "1wyq7al6z5vsf0j5fv3j1vcksrjadi3d90czxlbrbvxwlx7a28dh") (s 2) (e (quote (("alloy" "dep:alloy-primitives"))))))

(define-public crate-wormhole-io-0.2.0-alpha.1 (c (n "wormhole-io") (v "0.2.0-alpha.1") (d (list (d (n "hex-literal") (r "^0.4.1") (d #t) (k 2)))) (h "1c49q569g8ljndaqb17ms6n3v6p460sj621by6gvjwikyl9nrhbg") (r "1.68")))

(define-public crate-wormhole-io-0.2.0-alpha.2 (c (n "wormhole-io") (v "0.2.0-alpha.2") (d (list (d (n "alloy-primitives") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "hex-literal") (r "^0.4.1") (d #t) (k 2)))) (h "0jl9knd1z5v2lyv8npvirrzs9lrb80iasx9lg9p5qjdfa938p6fg") (s 2) (e (quote (("alloy" "dep:alloy-primitives"))))))

(define-public crate-wormhole-io-0.2.0-alpha.3 (c (n "wormhole-io") (v "0.2.0-alpha.3") (d (list (d (n "alloy-primitives") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "hex-literal") (r "^0.4.1") (d #t) (k 2)))) (h "0fa7xb4030fdsvwxzc7fhjal739b6f7sawwgdqiyz4zpf59dklma") (s 2) (e (quote (("alloy" "dep:alloy-primitives"))))))

(define-public crate-wormhole-io-0.3.0-alpha.0 (c (n "wormhole-io") (v "0.3.0-alpha.0") (d (list (d (n "hex-literal") (r "^0.4.1") (d #t) (k 2)))) (h "07hhwr4p0ks9rv5ljhghjhzqjd0a6pq86fk8d06g5wbh92s77a5r") (r "1.68")))

(define-public crate-wormhole-io-0.3.0-alpha.1 (c (n "wormhole-io") (v "0.3.0-alpha.1") (d (list (d (n "hex-literal") (r "^0.4.1") (d #t) (k 2)))) (h "101i6wnf2x5kphpzgiv7s9b7kcanqchq511g4sqhhjym7pk892in") (r "1.75")))

