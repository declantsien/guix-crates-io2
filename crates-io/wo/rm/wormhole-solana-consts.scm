(define-module (crates-io wo rm wormhole-solana-consts) #:use-module (crates-io))

(define-public crate-wormhole-solana-consts-0.2.0-alpha.2 (c (n "wormhole-solana-consts") (v "0.2.0-alpha.2") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "solana-program") (r "^1.16.16") (d #t) (k 0)))) (h "0f7ang9pw1174p4pk12hb7pyg9mfk0gqnrdl3w8n38czdfnhcfqa") (f (quote (("testnet") ("mainnet") ("localnet")))) (r "1.68")))

(define-public crate-wormhole-solana-consts-0.2.0-alpha.11 (c (n "wormhole-solana-consts") (v "0.2.0-alpha.11") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "solana-program") (r "^1.16.27") (d #t) (k 0)))) (h "08xlp2gvn59wi26wdw4rpml2m86zqrc27dfck3c1hnxlbj4wqz83") (f (quote (("testnet") ("mainnet") ("localnet")))) (r "1.68")))

(define-public crate-wormhole-solana-consts-0.3.0-alpha.0 (c (n "wormhole-solana-consts") (v "0.3.0-alpha.0") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "solana-program") (r "^1.17.20") (d #t) (k 0)))) (h "0x6f056zg5b0p8ik0nssk4q5yhrdzw439gh5r15jc3422xxm9xbn") (f (quote (("testnet") ("mainnet") ("localnet")))) (r "1.68")))

(define-public crate-wormhole-solana-consts-0.3.0-alpha.1 (c (n "wormhole-solana-consts") (v "0.3.0-alpha.1") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "solana-program") (r "^1.18.10") (d #t) (k 0)))) (h "0jbnlf5hwvsx79l43blfky9cjm8axkbqllknj9wjw79s4frp1blm") (f (quote (("testnet") ("mainnet") ("localnet")))) (r "1.75")))

