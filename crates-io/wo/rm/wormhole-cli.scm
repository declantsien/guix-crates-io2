(define-module (crates-io wo rm wormhole-cli) #:use-module (crates-io))

(define-public crate-wormhole-cli-0.1.0 (c (n "wormhole-cli") (v "0.1.0") (d (list (d (n "clap") (r "^4.1.8") (f (quote ("derive" "env" "string" "debug"))) (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "wormhole-common") (r "^0.1.0") (d #t) (k 0)))) (h "0037wj3nkzfq38vrmvnvknmby9jqgpa1ajyb6vgrgrchzgw3bpxd")))

(define-public crate-wormhole-cli-0.2.0 (c (n "wormhole-cli") (v "0.2.0") (d (list (d (n "clap") (r "^4.1.8") (f (quote ("derive" "env" "string" "debug"))) (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "wormhole-common") (r "^0.2.0") (d #t) (k 0)))) (h "1xccr7pk1wqm61g0ij021shzlibj8wz2a9r6lbhs4r0c310kgvzx")))

