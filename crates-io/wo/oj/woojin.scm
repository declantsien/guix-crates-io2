(define-module (crates-io wo oj woojin) #:use-module (crates-io))

(define-public crate-woojin-0.1.0 (c (n "woojin") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "nom") (r "^7.1.0") (d #t) (k 0)) (d (n "regex") (r "^1.8.1") (d #t) (k 0)))) (h "0ig54x7c37r03ac8hl3n3a9abc0sg03n0axcm2ysf7plgsrsxj63") (y #t)))

(define-public crate-woojin-0.1.1 (c (n "woojin") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "nom") (r "^7.1.0") (d #t) (k 0)) (d (n "regex") (r "^1.8.1") (d #t) (k 0)))) (h "1i1dfd1nxwxgx4jzrb2na2wkzsb3y339cm8kh8vyzmjw3rc2rddk") (y #t)))

(define-public crate-woojin-0.1.2 (c (n "woojin") (v "0.1.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "nom") (r "^7.1.0") (d #t) (k 0)) (d (n "regex") (r "^1.8.1") (d #t) (k 0)))) (h "012hsqdrribfdf5wnpqks2axb7kc2w8dm3japp9v70sh9w5gw7an") (y #t)))

(define-public crate-woojin-0.1.3 (c (n "woojin") (v "0.1.3") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "nom") (r "^7.1.0") (d #t) (k 0)) (d (n "regex") (r "^1.8.1") (d #t) (k 0)))) (h "1wg4995miq4mfn81skykprnd9gd23hw9pfr6jjnkcg0lz970q79n") (y #t)))

(define-public crate-woojin-0.1.4 (c (n "woojin") (v "0.1.4") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "nom") (r "^7.1.0") (d #t) (k 0)) (d (n "regex") (r "^1.8.1") (d #t) (k 0)))) (h "0gayn5ljcjskx5agjsjzv6lnl6nrc3pavlr6dk4wnp8xh483kpig") (y #t)))

(define-public crate-woojin-0.1.5 (c (n "woojin") (v "0.1.5") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "nom") (r "^7.1.0") (d #t) (k 0)) (d (n "regex") (r "^1.8.1") (d #t) (k 0)))) (h "0c545n68mamlv4685rg97c0k0f15g0znbmr03j6w46n2s6g2hm7v")))

