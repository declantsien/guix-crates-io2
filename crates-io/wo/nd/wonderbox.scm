(define-module (crates-io wo nd wonderbox) #:use-module (crates-io))

(define-public crate-wonderbox-0.1.0 (c (n "wonderbox") (v "0.1.0") (d (list (d (n "wonderbox-codegen") (r "^0.1.0") (d #t) (k 0)))) (h "1xavhqfalahdmirwdj6wlr9dvzwh27f1nsgci4n0aafzjcbpdq4g")))

(define-public crate-wonderbox-0.2.0 (c (n "wonderbox") (v "0.2.0") (d (list (d (n "wonderbox-codegen") (r "^0.1.0") (d #t) (k 0)))) (h "12i9icjhqg6nm9aqa5zc657ci4cd2kiwhxzlnzlf254j0qqmqxa4")))

(define-public crate-wonderbox-0.3.0 (c (n "wonderbox") (v "0.3.0") (d (list (d (n "wonderbox-codegen") (r "^0.3.0") (d #t) (k 0)))) (h "09by18d1v659gfm8mvhqnjnl7ykny63b38g44fggqs6gjw5q7wkj")))

(define-public crate-wonderbox-0.4.0 (c (n "wonderbox") (v "0.4.0") (d (list (d (n "trybuild") (r "^1.0") (d #t) (k 2)) (d (n "wonderbox-codegen") (r "^0.4.0") (d #t) (k 0)))) (h "1405sf0d2gnycz6p3ds0xc91m2l1rg3g4m1x8l2r6ddkjv9n09v8")))

(define-public crate-wonderbox-0.5.0 (c (n "wonderbox") (v "0.5.0") (h "0hzn02jpvd36hgwc4zb6xc0rlpg81mbmq4j4hv55jdm6k5j2jpp0")))

