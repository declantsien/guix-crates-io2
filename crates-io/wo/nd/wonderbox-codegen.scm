(define-module (crates-io wo nd wonderbox-codegen) #:use-module (crates-io))

(define-public crate-wonderbox-codegen-0.1.0 (c (n "wonderbox-codegen") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "syn") (r "^0.15.33") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1cl2imfaly1p2s3k95j0lbx6bclha2nr0lpsw6377s8xym8ddkjn")))

(define-public crate-wonderbox-codegen-0.3.0 (c (n "wonderbox-codegen") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "syn") (r "^0.15.33") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0hp6k9hjv2g9kvwy19n7c3i8n769jx0pp76d4cx27q4k0kyb7sax")))

(define-public crate-wonderbox-codegen-0.4.0 (c (n "wonderbox-codegen") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "syn") (r "^0.15.33") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0ss55ha9zriv6j7nza6pi3wa3d4jy0p8izf9zii11vgfnhlkm320")))

