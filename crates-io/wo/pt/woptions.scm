(define-module (crates-io wo pt woptions) #:use-module (crates-io))

(define-public crate-woptions-0.1.0 (c (n "woptions") (v "0.1.0") (d (list (d (n "anyhow") (r "~1.0") (d #t) (k 2)) (d (n "former") (r "~0.1") (d #t) (k 0)) (d (n "rustversion") (r "~1.0") (d #t) (k 2)) (d (n "trybuild") (r "~1.0") (f (quote ("diff"))) (d #t) (k 2)) (d (n "woptions_meta") (r "~0.1") (d #t) (k 0)) (d (n "woptions_runtime") (r "~0.1") (d #t) (k 0)) (d (n "wtest_basic") (r "~0.1") (d #t) (k 2)))) (h "1l206j7fyghbxy7b61x1pzgiszq30q8wraq2j7wkfw7nl7hvvgg0")))

(define-public crate-woptions-0.1.2 (c (n "woptions") (v "0.1.2") (d (list (d (n "former") (r "~0.1") (d #t) (k 0)) (d (n "test_tools") (r "~0.1") (d #t) (k 2)) (d (n "woptions_meta") (r "~0.1") (d #t) (k 0)) (d (n "woptions_runtime") (r "~0.1") (d #t) (k 0)))) (h "1acqkxbpbjr23i0j795kgy9jiy4v3zn5pdwgyq17pf3cbv845gd8")))

(define-public crate-woptions-0.1.3 (c (n "woptions") (v "0.1.3") (d (list (d (n "former") (r "~0.1") (d #t) (k 0)) (d (n "test_tools") (r "~0.1") (d #t) (k 2)) (d (n "woptions_meta") (r "~0.1") (d #t) (k 0)) (d (n "woptions_runtime") (r "~0.1") (d #t) (k 0)))) (h "16npmcklq8knc8rr5ll8kswp324hx5fnxrh261icjfi9zgxz279v") (f (quote (("default") ("all"))))))

