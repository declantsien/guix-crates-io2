(define-module (crates-io wo pt woptions_runtime) #:use-module (crates-io))

(define-public crate-woptions_runtime-0.1.0 (c (n "woptions_runtime") (v "0.1.0") (d (list (d (n "anyhow") (r "~1.0") (d #t) (k 2)) (d (n "former") (r "~0.1") (d #t) (k 0)) (d (n "wtest_basic") (r "~0.1") (d #t) (k 2)))) (h "0ab4fy3dwpx2grlpdkzk1zcfyxrfj3b19nd4jlq7ldx6x41q0qxk")))

(define-public crate-woptions_runtime-0.1.1 (c (n "woptions_runtime") (v "0.1.1") (d (list (d (n "former") (r "~0.1") (d #t) (k 0)) (d (n "test_tools") (r "~0.1") (d #t) (k 2)))) (h "0jvc85nib8mazp5kb12mz372n0ss4468vbsd3s5hywiwq1rp3v4f")))

(define-public crate-woptions_runtime-0.1.3 (c (n "woptions_runtime") (v "0.1.3") (d (list (d (n "former") (r "~0.1") (d #t) (k 0)) (d (n "test_tools") (r "~0.1") (d #t) (k 2)))) (h "1dpxf1crznc0z5z9hdlw8bj7p1w53sja5iw1ywh9v461bzq1riq0")))

