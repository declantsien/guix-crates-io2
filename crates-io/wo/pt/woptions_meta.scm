(define-module (crates-io wo pt woptions_meta) #:use-module (crates-io))

(define-public crate-woptions_meta-0.1.0 (c (n "woptions_meta") (v "0.1.0") (d (list (d (n "meta_tools") (r "~0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "~1.0") (d #t) (k 0)) (d (n "quote") (r "~1.0") (d #t) (k 0)) (d (n "syn") (r "~1.0") (f (quote ("full" "extra-traits" "parsing" "printing"))) (d #t) (k 0)) (d (n "trybuild") (r "~1.0") (f (quote ("diff"))) (d #t) (k 2)) (d (n "wproc_macro") (r "~0.1") (d #t) (k 0)))) (h "1v2yc6gg7gb90nv3rwk1lk09dh9wg2s9gyyh023046zfa9js4j4n")))

(define-public crate-woptions_meta-0.1.2 (c (n "woptions_meta") (v "0.1.2") (d (list (d (n "convert_case") (r "~0.5") (d #t) (k 0)) (d (n "iter_tools") (r "~0.1") (d #t) (k 0)) (d (n "meta_tools") (r "~0.2") (d #t) (k 0)) (d (n "proc_macro_tools") (r "~0.1") (d #t) (k 0)))) (h "1cz6m0m6k4hznfzj9cyiwvl3yb20x749j0311s4nn1msbw1c0gf5")))

(define-public crate-woptions_meta-0.1.3 (c (n "woptions_meta") (v "0.1.3") (d (list (d (n "convert_case") (r "~0.5") (d #t) (k 0)) (d (n "iter_tools") (r "~0.1") (d #t) (k 0)) (d (n "meta_tools_min") (r "~0.2") (d #t) (k 0)) (d (n "proc_macro_tools") (r "~0.1") (d #t) (k 0)))) (h "0cscnf445c823bnys5p0h3c7l1923681qmqzrp4ap8pxyivin7ji")))

