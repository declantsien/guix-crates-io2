(define-module (crates-io wo od woodblocks) #:use-module (crates-io))

(define-public crate-woodblocks-0.1.0 (c (n "woodblocks") (v "0.1.0") (d (list (d (n "hound") (r "^3.4.0") (d #t) (k 0)))) (h "1703xpkb3xj1njz8d7h4hnf8kac8j69dqr02l9z5rmq2wdsabfr5") (y #t)))

(define-public crate-woodblocks-0.1.1 (c (n "woodblocks") (v "0.1.1") (d (list (d (n "hound") (r "^3.4.0") (d #t) (k 0)))) (h "1rp3pgma7xdp3nnqlh3iswrbpl6awn8x9ksv08byc6pk404sqscb") (y #t)))

(define-public crate-woodblocks-1.0.0 (c (n "woodblocks") (v "1.0.0") (d (list (d (n "hound") (r "^3.4.0") (d #t) (k 0)))) (h "1yyiynpj2x6mkgzw5df21apmmwi954yy8npwnwdpxg8hi0si392n") (y #t)))

