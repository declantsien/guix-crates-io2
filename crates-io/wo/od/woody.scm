(define-module (crates-io wo od woody) #:use-module (crates-io))

(define-public crate-woody-0.1.0 (c (n "woody") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serial_test") (r "^0.10") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("full" "rt"))) (d #t) (k 2)))) (h "1zchnhz9hxrc5z7wfa5z8ibsmmfh1619f773vx0yc9nbf9n7gka3")))

(define-public crate-woody-0.1.1 (c (n "woody") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serial_test") (r "^3.1.0") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("full" "rt"))) (d #t) (k 2)))) (h "0xaslvrcvlsw2n7i54d22fx7j9f4h2bhayzgv1rfg1x8bl3yi948")))

(define-public crate-woody-0.1.2 (c (n "woody") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.38") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serial_test") (r "^3.1.0") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("full" "rt"))) (d #t) (k 2)))) (h "0k9d6w85sa05bh93pq4lnx7fb5a94dql8247grz5s2hmhcqjrnx2")))

