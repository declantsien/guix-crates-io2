(define-module (crates-io wo od woodo) #:use-module (crates-io))

(define-public crate-woodo-1.0.0 (c (n "woodo") (v "1.0.0") (d (list (d (n "nix") (r "^0.23.1") (d #t) (k 0)))) (h "0ghf3xlz75vw4xfhdhs094ybdy991g6y4bgmdh26wa8yk118kaxn")))

(define-public crate-woodo-1.0.1 (c (n "woodo") (v "1.0.1") (d (list (d (n "nix") (r "^0.23.1") (d #t) (k 0)))) (h "0zpapkpqy0pq1wgzxhrlvq8ykdvfd4sibk5498ila9pdj9c1d5pn")))

