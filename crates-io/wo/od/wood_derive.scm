(define-module (crates-io wo od wood_derive) #:use-module (crates-io))

(define-public crate-wood_derive-0.2.1 (c (n "wood_derive") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^0.4.29") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "syn") (r "^0.15.33") (d #t) (k 0)) (d (n "wood") (r "^0.2.1") (d #t) (k 0)))) (h "1clqnfak2azvic1wwrvmfw8cqifzx1ql5vlzsz4fdsxbgfg504qv")))

(define-public crate-wood_derive-0.3.0 (c (n "wood_derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^0.4.29") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "syn") (r "^0.15.33") (d #t) (k 0)) (d (n "wood") (r "^0.3.0") (d #t) (k 0)))) (h "1i2j0hy7xhg6p3mii1ag5jvmafyabk60zmygrapzzica8y6p26pc")))

(define-public crate-wood_derive-0.4.0 (c (n "wood_derive") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^0.4.29") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "syn") (r "^0.15.33") (d #t) (k 0)) (d (n "wood") (r "^0.4.0") (d #t) (k 0)))) (h "1gnr30ybh3x9lashv3dngf7nxmcrqy8c16aly3c919yasryyhir4")))

(define-public crate-wood_derive-0.4.2 (c (n "wood_derive") (v "0.4.2") (d (list (d (n "proc-macro2") (r "^0.4.29") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "syn") (r "^0.15.33") (d #t) (k 0)) (d (n "wood") (r "^0.4.2") (d #t) (k 0)))) (h "06cmcq3fbl55p6pnfx33a7ci4ih0nsnf8xzxm2bkxlgpzkqkdki5")))

(define-public crate-wood_derive-0.5.0 (c (n "wood_derive") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^0.4.29") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "syn") (r "^0.15.33") (d #t) (k 0)) (d (n "wood") (r "^0.5.0") (d #t) (k 0)))) (h "197a1c12rj9078zfdx6z575xj8qx7bjjxjcc1bxfbsydva8ci4iv")))

(define-public crate-wood_derive-0.5.1 (c (n "wood_derive") (v "0.5.1") (d (list (d (n "proc-macro2") (r "^0.4.29") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "syn") (r "^0.15.33") (d #t) (k 0)) (d (n "wood") (r "^0.5.1") (d #t) (k 0)))) (h "1wg64j80bp3q5p7lgqs5pwwwd6kcqwvjmnkl95a6sk8g0jqlz3cc")))

(define-public crate-wood_derive-0.5.2 (c (n "wood_derive") (v "0.5.2") (d (list (d (n "proc-macro2") (r "^0.4.29") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "syn") (r "^0.15.33") (d #t) (k 0)) (d (n "wood") (r "^0.5.2") (d #t) (k 0)))) (h "014ib9a0p83y0axsfxas7qgdjr0pmkpsj28k4g6ndzx3adrvf3w2")))

(define-public crate-wood_derive-0.6.0 (c (n "wood_derive") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^0.4.29") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "syn") (r "^0.15.33") (d #t) (k 0)) (d (n "wood") (r "^0.6.0") (d #t) (k 0)))) (h "0l503c26m4bj5bas7a60pn11daack1a8gpzr8d27h5j5zsbqdn91")))

(define-public crate-wood_derive-0.8.0 (c (n "wood_derive") (v "0.8.0") (d (list (d (n "proc-macro2") (r "^0.4.29") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "syn") (r "^0.15.33") (d #t) (k 0)) (d (n "wood") (r "^0.8.0") (d #t) (k 0)))) (h "17ynrzgjmcmq5rkqzsyv8pih13495mv5j3gj78wn1gk0grxdnpps")))

(define-public crate-wood_derive-0.9.0 (c (n "wood_derive") (v "0.9.0") (d (list (d (n "proc-macro2") (r "^0.4.29") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "syn") (r "^0.15.33") (d #t) (k 0)) (d (n "wood") (r "^0.9.0") (d #t) (k 0)))) (h "00im5nfcwnp4r7hcxjl2mf0n01ll5k3kjhh077q00cjamav1hf55")))

