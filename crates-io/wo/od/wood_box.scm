(define-module (crates-io wo od wood_box) #:use-module (crates-io))

(define-public crate-wood_box-0.1.0 (c (n "wood_box") (v "0.1.0") (h "1szch54kiyf7a90xhkrp0k5ziyg2x18si79aidnhsinmn9jkiz2x")))

(define-public crate-wood_box-0.2.0 (c (n "wood_box") (v "0.2.0") (h "05x759cb4whbr66jy02zvvhc57dcjq91x2r05nd7pf4n9djhpk32")))

(define-public crate-wood_box-0.3.0 (c (n "wood_box") (v "0.3.0") (h "0nri2my4raxvkn1bib565wp1ihniqmz7f0zp8vslfvgzyrjsg1hs")))

