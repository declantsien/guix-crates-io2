(define-module (crates-io wo od woodpecker) #:use-module (crates-io))

(define-public crate-woodpecker-0.1.0 (c (n "woodpecker") (v "0.1.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "chrono") (r "^0.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.4") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.5") (d #t) (k 2)) (d (n "time") (r "^0.1.36") (d #t) (k 0)))) (h "1fv78ada27zqc1aj1ndxzbkb6vmznz8vs3y9myfvnb967q858lzw")))

(define-public crate-woodpecker-0.1.1 (c (n "woodpecker") (v "0.1.1") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "chrono") (r "^0.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.4") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.5") (d #t) (k 2)) (d (n "time") (r "^0.1.36") (d #t) (k 0)))) (h "13b72yhmbn22ndxmx2vrp450xlfklgh2ina40h4l5gzlvp5152cd")))

(define-public crate-woodpecker-0.1.2 (c (n "woodpecker") (v "0.1.2") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "chrono") (r "^0.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.4") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.5") (d #t) (k 2)) (d (n "time") (r "^0.1.36") (d #t) (k 0)))) (h "1x3qc89v2m0vvfm6dyps531wcp514sqydyw847kckw85jwzca0d6")))

(define-public crate-woodpecker-0.2.0 (c (n "woodpecker") (v "0.2.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "chrono") (r "^0.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.4") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.5") (d #t) (k 2)) (d (n "time") (r "^0.1.36") (d #t) (k 0)))) (h "0nz6fx60q5zvr3qxz9cv8j7akpcm6vhn8sqkx7hpdavrk4v2lyl1") (f (quote (("test-thread-log") ("default"))))))

(define-public crate-woodpecker-0.3.0 (c (n "woodpecker") (v "0.3.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "chrono") (r "^0.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.4") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.5") (d #t) (k 2)) (d (n "time") (r "^0.1.36") (d #t) (k 0)))) (h "0jzdhz4xqdpwaagr0dd6nfnpv9n2j11hbyrxqwp8frvkjciy0yrj") (f (quote (("test-thread-log") ("default"))))))

(define-public crate-woodpecker-0.4.0 (c (n "woodpecker") (v "0.4.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "chrono") (r "^0.3") (d #t) (k 0)) (d (n "crossbeam") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "parking_lot") (r "^0.4") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.2") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.5") (d #t) (k 2)) (d (n "thread-id") (r "^3.0.0") (d #t) (k 0)) (d (n "time") (r "^0.1.36") (d #t) (k 0)))) (h "0rvgpdyk1qji0993dpqywnma15s17q4z4k013z85v8f85pl4prfl") (f (quote (("test-thread-log") ("default"))))))

