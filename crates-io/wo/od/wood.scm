(define-module (crates-io wo od wood) #:use-module (crates-io))

(define-public crate-wood-0.2.1 (c (n "wood") (v "0.2.1") (d (list (d (n "ref_slice") (r "^1.1.1") (d #t) (k 0)))) (h "0dn0a460krjmwiyb3wm49n25gfb6r6hbx7f8k3607a6awkb255db")))

(define-public crate-wood-0.3.0 (c (n "wood") (v "0.3.0") (d (list (d (n "ref_slice") (r "^1.1.1") (d #t) (k 0)))) (h "06xhsbcy769nq9jmagqnh0qsab3hy3ygdrhh6h6vmpqr4mhlaabx")))

(define-public crate-wood-0.4.0 (c (n "wood") (v "0.4.0") (d (list (d (n "ref_slice") (r "^1.1.1") (d #t) (k 0)))) (h "0v8hmczkpi3zxrq1495qs3m3jc247hmhw97rhqa9mi8qk0w43mkz")))

(define-public crate-wood-0.4.2 (c (n "wood") (v "0.4.2") (d (list (d (n "ref_slice") (r "^1.1.1") (d #t) (k 0)))) (h "0nm2b5izldniya5vksrkldjq1jcdkgkgi6qd7j257p8bjif2p5vm")))

(define-public crate-wood-0.4.3 (c (n "wood") (v "0.4.3") (d (list (d (n "ref_slice") (r "^1.1.1") (d #t) (k 0)))) (h "0x1fwg8za0fxz44b1g0mbm3izis6mrpqyspfday5pgpxqhadmx9d")))

(define-public crate-wood-0.5.0 (c (n "wood") (v "0.5.0") (d (list (d (n "ref_slice") (r "^1.1.1") (d #t) (k 0)))) (h "14s50r3x3c8238ybmcmql004ap6ga9qilqm6idyqbvikgdwfwq8k")))

(define-public crate-wood-0.5.1 (c (n "wood") (v "0.5.1") (d (list (d (n "ref_slice") (r "^1.1.1") (d #t) (k 0)))) (h "03xbbcdi135vpmxvkympxm2jp33xc094g165bcfqy27f4fcjpp4w")))

(define-public crate-wood-0.5.2 (c (n "wood") (v "0.5.2") (d (list (d (n "ref_slice") (r "^1.1.1") (d #t) (k 0)))) (h "05fdw9blsy0aa4m6axpycccdrp03ja1q0jifbxv6zmcvjm0za2n2")))

(define-public crate-wood-0.5.3 (c (n "wood") (v "0.5.3") (d (list (d (n "smallvec") (r "^1.8.0") (d #t) (k 0)))) (h "10zr8rzvpgrn8zfjyhqxq4vayi7452rx4h6zxwaw7ssz86kvmyhm")))

(define-public crate-wood-0.6.0 (c (n "wood") (v "0.6.0") (d (list (d (n "smallvec") (r "^1.8.0") (d #t) (k 0)))) (h "1nri6hynldg03bwb8ggbjwa296323z5whwxvrh3g5h7lb446az1h")))

(define-public crate-wood-0.7.0 (c (n "wood") (v "0.7.0") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)))) (h "0rkd5kvc5xy5p8bbjb7hqg0xh8r2v7h0masmbl3y2001qpkccfc3")))

(define-public crate-wood-0.8.0 (c (n "wood") (v "0.8.0") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)))) (h "1dygdzpdynpy1dgkjv5i105njbf6x826h1abbsdqy8pl4f2c3j49")))

(define-public crate-wood-0.9.0 (c (n "wood") (v "0.9.0") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)))) (h "0p14ric7sma99bw5rimlgw4rmkn0agiplmfl0v9fch8iyas5q4vn")))

(define-public crate-wood-0.10.0 (c (n "wood") (v "0.10.0") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)))) (h "0v20hdcql47vmzjlxyxwj6iyflm0d8dhj6fr3m43237sm7rrd2lq")))

