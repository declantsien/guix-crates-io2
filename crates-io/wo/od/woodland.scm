(define-module (crates-io wo od woodland) #:use-module (crates-io))

(define-public crate-woodland-0.0.1 (c (n "woodland") (v "0.0.1") (h "0cz271452pd1mgqqlybxhgrcfps311d239gr8b4mnklvymy7g3fi")))

(define-public crate-woodland-0.0.2 (c (n "woodland") (v "0.0.2") (h "1q2rjm5x0nhrcc0a48iq1fqriyf6257hqlxhgxdyfk1ik0z3zmc9")))

(define-public crate-woodland-0.0.3 (c (n "woodland") (v "0.0.3") (h "01bh4lxnbpv3sif6rm6alrihs9fb15khbag9pqlv6xzw5jif6fvw")))

(define-public crate-woodland-0.0.4 (c (n "woodland") (v "0.0.4") (h "1xndydhmn3q410aq8yl5bqj5dx4mcmbzbfkj8np2h0rmzn0b5ckg")))

