(define-module (crates-io wo rl world-count) #:use-module (crates-io))

(define-public crate-world-count-0.1.0 (c (n "world-count") (v "0.1.0") (d (list (d (n "aoko") (r "^0.2") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)))) (h "0ckb9k51ij74xi2p0x13qsky8iiqxz4k4al4cm43grkzyz3lxa07")))

(define-public crate-world-count-0.1.1 (c (n "world-count") (v "0.1.1") (d (list (d (n "aoko") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.5") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)))) (h "0ka06r76nrmvw3q1069ggw2milza4nzj4hlwlclwppqnnmn7h77w")))

(define-public crate-world-count-0.1.2 (c (n "world-count") (v "0.1.2") (d (list (d (n "aoko") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.5") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)))) (h "04nys9ilv5vv4jp5dz8a0lv4fkhxnny09cw7g12fcwqjbisa5afl")))

(define-public crate-world-count-0.1.3 (c (n "world-count") (v "0.1.3") (d (list (d (n "aoko") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-rc.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)))) (h "09fwhq7hl5rl2vli2h30glhwrrf5sb0vlm7p7px5ygfr9wgdp1nq")))

(define-public crate-world-count-0.1.4 (c (n "world-count") (v "0.1.4") (d (list (d (n "aoko") (r "^0.3.0-alpha.1") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "tokio") (r "^1.28") (f (quote ("full"))) (d #t) (k 0)))) (h "04hhnnwj691x9qdz1jmmvb8bf9xxici5k13j7m08xsqra60flzxw")))

