(define-module (crates-io wo rl world-transmuter-engine) #:use-module (crates-io))

(define-public crate-world-transmuter-engine-0.1.0 (c (n "world-transmuter-engine") (v "0.1.0") (d (list (d (n "valence_nbt") (r "^0.7") (d #t) (k 0)) (d (n "valence_nbt") (r "^0.7") (f (quote ("snbt"))) (d #t) (k 2)))) (h "1s66jygk5b4ldksh1080a93r593wnpw9fv1dl2nas9mk0qimmwrl")))

(define-public crate-world-transmuter-engine-0.2.0 (c (n "world-transmuter-engine") (v "0.2.0") (d (list (d (n "valence_nbt") (r "^0.7") (d #t) (k 0)) (d (n "valence_nbt") (r "^0.7") (f (quote ("snbt"))) (d #t) (k 2)))) (h "0rjqg5a8szh1y1vg7gqksbphmh9yn8yyglmbxrqv8nq0ycbirxp7")))

(define-public crate-world-transmuter-engine-0.3.0 (c (n "world-transmuter-engine") (v "0.3.0") (d (list (d (n "java_string") (r "^0.1") (d #t) (k 0)) (d (n "valence_nbt") (r "^0.8") (f (quote ("java_string"))) (d #t) (k 0)) (d (n "valence_nbt") (r "^0.8") (f (quote ("java_string" "snbt"))) (d #t) (k 2)))) (h "1vlsdm1wqwc1v08iyb9qksi6ni2sb03vnfwyxxdqsnqmj1zm5ai0")))

(define-public crate-world-transmuter-engine-0.4.0 (c (n "world-transmuter-engine") (v "0.4.0") (d (list (d (n "java_string") (r "^0.1") (d #t) (k 0)) (d (n "valence_nbt") (r "^0.8") (f (quote ("java_string"))) (d #t) (k 0)) (d (n "valence_nbt") (r "^0.8") (f (quote ("java_string" "snbt"))) (d #t) (k 2)))) (h "1yyp93v7nsgxqn10sa9qxxdpcd1ps72k9510xf5nxpr14npjjgbj")))

(define-public crate-world-transmuter-engine-0.5.0 (c (n "world-transmuter-engine") (v "0.5.0") (d (list (d (n "java_string") (r "^0.1") (d #t) (k 0)) (d (n "valence_nbt") (r "^0.8") (f (quote ("java_string"))) (d #t) (k 0)) (d (n "valence_nbt") (r "^0.8") (f (quote ("java_string" "snbt"))) (d #t) (k 2)))) (h "004vzmkvgphqy7x0a65gwsmx16lj1cz5q03z0x3sk933aiqjj50z")))

(define-public crate-world-transmuter-engine-0.6.0 (c (n "world-transmuter-engine") (v "0.6.0") (d (list (d (n "java_string") (r "^0.1") (d #t) (k 0)) (d (n "valence_nbt") (r "^0.8") (f (quote ("java_string"))) (d #t) (k 0)) (d (n "valence_nbt") (r "^0.8") (f (quote ("java_string" "snbt"))) (d #t) (k 2)))) (h "1kcw57751blhkm8j2w0qw0vz1p7f171mys0mgbvnl5vklyvryq2p")))

(define-public crate-world-transmuter-engine-0.6.1 (c (n "world-transmuter-engine") (v "0.6.1") (d (list (d (n "java_string") (r "^0.1") (d #t) (k 0)) (d (n "valence_nbt") (r "^0.8") (f (quote ("java_string"))) (d #t) (k 0)) (d (n "valence_nbt") (r "^0.8") (f (quote ("java_string" "snbt"))) (d #t) (k 2)))) (h "1c2kpfc14v67jb04pp1nnw4l0b6sprp3m86f3xb1qfc26dsf5954")))

(define-public crate-world-transmuter-engine-0.7.0 (c (n "world-transmuter-engine") (v "0.7.0") (d (list (d (n "java_string") (r "^0.1") (d #t) (k 0)) (d (n "valence_nbt") (r "^0.8") (f (quote ("java_string"))) (d #t) (k 0)) (d (n "valence_nbt") (r "^0.8") (f (quote ("java_string" "snbt"))) (d #t) (k 2)))) (h "0psk6zwdslisrqg90n3zxgbvj86a5089bh4q521vz9b0zwqhmhwk")))

(define-public crate-world-transmuter-engine-0.7.1 (c (n "world-transmuter-engine") (v "0.7.1") (d (list (d (n "java_string") (r "^0.1") (d #t) (k 0)) (d (n "valence_nbt") (r "^0.8") (f (quote ("java_string"))) (d #t) (k 0)) (d (n "valence_nbt") (r "^0.8") (f (quote ("java_string" "snbt"))) (d #t) (k 2)))) (h "12lb337b0k6cnx5hyc805xxq4r0q7hifxzs7c3mcbjrx4w9h1aps")))

(define-public crate-world-transmuter-engine-0.7.2 (c (n "world-transmuter-engine") (v "0.7.2") (d (list (d (n "java_string") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "valence_nbt") (r "^0.8") (f (quote ("java_string"))) (d #t) (k 0)) (d (n "valence_nbt") (r "^0.8") (f (quote ("java_string" "snbt"))) (d #t) (k 2)))) (h "1qh6l51hlfmgr6gh6nmhwz4d3gi134pvpcn2v2n87c4abqyj9zfp") (y #t)))

(define-public crate-world-transmuter-engine-0.8.0 (c (n "world-transmuter-engine") (v "0.8.0") (d (list (d (n "java_string") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "valence_nbt") (r "^0.8") (f (quote ("java_string"))) (d #t) (k 0)) (d (n "valence_nbt") (r "^0.8") (f (quote ("java_string" "snbt"))) (d #t) (k 2)))) (h "16rp6qvp1yvqif6pjf0rcq6ykgrns3b07y4kriwvf9gmjlwf1mhc")))

