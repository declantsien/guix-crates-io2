(define-module (crates-io wo rl world-tables-base) #:use-module (crates-io))

(define-public crate-world-tables-base-0.1.0 (c (n "world-tables-base") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "dbent") (r "^0.1") (f (quote ("rusqlite"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rusqlite") (r "^0.28") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "1x2jfpkk61yg0h38jm1f7is07hs9x6f4l519vq9wyrrzvlivlrj1")))

