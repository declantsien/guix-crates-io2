(define-module (crates-io wo rl world_id) #:use-module (crates-io))

(define-public crate-world_id-0.1.0 (c (n "world_id") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "ipfs-api") (r "^0.4.0-alpha.3") (d #t) (k 0)) (d (n "pem") (r "^0.5") (d #t) (k 0)) (d (n "ring") (r "^0.13.0-alpha") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_cbor") (r "^0.8") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)) (d (n "untrusted") (r "^0.6") (d #t) (k 0)))) (h "0a7jg0v87rg77wcb6c9kdbjvm4x0bnz9h0w4wj0vnsf67976z2vc")))

