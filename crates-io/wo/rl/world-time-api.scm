(define-module (crates-io wo rl world-time-api) #:use-module (crates-io))

(define-public crate-world-time-api-1.3.0 (c (n "world-time-api") (v "1.3.0") (d (list (d (n "libtzfile") (r "^2.0") (f (quote ("json"))) (d #t) (k 0)) (d (n "rocket") (r "^0.5.0-rc.2") (f (quote ("json"))) (d #t) (k 0)))) (h "0dx0489ky6f78pdzx6pwjz15nqjraxqjy875vhvfilnx39di39dz")))

(define-public crate-world-time-api-1.4.0 (c (n "world-time-api") (v "1.4.0") (d (list (d (n "case_convert") (r "^0.1.0") (d #t) (k 0)) (d (n "libtzfile") (r "^2.0") (f (quote ("json"))) (d #t) (k 0)) (d (n "rocket") (r "^0.5.0") (f (quote ("json"))) (d #t) (k 0)))) (h "1bs7scc0s3sdyj3b5wyy265il1d01r364pr1bzh8fg64yc8i6k8m")))

(define-public crate-world-time-api-1.4.1 (c (n "world-time-api") (v "1.4.1") (d (list (d (n "case_convert") (r "^0.1.0") (d #t) (k 0)) (d (n "libtzfile") (r "^3") (f (quote ("json"))) (d #t) (k 0)) (d (n "rocket") (r "^0.5.0") (f (quote ("json"))) (d #t) (k 0)))) (h "131ywa5lf0apls1l13fnvsbq59wirz9bnkf2wi63s3q5zd135xf9")))

