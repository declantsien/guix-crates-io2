(define-module (crates-io wo rl world-tables-data) #:use-module (crates-io))

(define-public crate-world-tables-data-0.1.0 (c (n "world-tables-data") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "csv") (r "^1.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rusqlite") (r "^0.28") (d #t) (k 0)) (d (n "rusqlite_migration") (r "^1") (d #t) (k 0)) (d (n "world-tables-base") (r "^0.1") (d #t) (k 0)))) (h "0zaa83kj58rj9v4r9l89p470kz1gyfxag9r8g4fr9mkv5phrqpby")))

