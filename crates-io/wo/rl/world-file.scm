(define-module (crates-io wo rl world-file) #:use-module (crates-io))

(define-public crate-world-file-0.0.1 (c (n "world-file") (v "0.0.1") (h "18qk5p8gvzl7xqipk1w81r7x4mb794pzl60wpif2dap3b1dimagv")))

(define-public crate-world-file-0.0.2 (c (n "world-file") (v "0.0.2") (h "06a8h052p1i4vw0834c3fkixpkl728q1g5gvbqx5am28pxk8bqja")))

(define-public crate-world-file-0.1.0 (c (n "world-file") (v "0.1.0") (h "1702xxs5z8qpmx2jfl43n2660sq8lph8sfhzv3wprv5qm82xspsl")))

