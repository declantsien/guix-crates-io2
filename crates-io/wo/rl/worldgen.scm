(define-module (crates-io wo rl worldgen) #:use-module (crates-io))

(define-public crate-worldgen-0.1.0 (c (n "worldgen") (v "0.1.0") (d (list (d (n "gcc") (r "^0.3.8") (d #t) (k 1)) (d (n "lazy_static") (r "^0.1.11") (d #t) (k 1)) (d (n "libc") (r "^0.1.8") (d #t) (k 0)))) (h "194pppnlf2zn0gglwmfsdi68qvrv3x4b6nfdd7acqsyapnm4v3wj")))

(define-public crate-worldgen-0.1.1 (c (n "worldgen") (v "0.1.1") (d (list (d (n "gcc") (r "^0.3.8") (d #t) (k 1)) (d (n "libc") (r "^0.1.8") (d #t) (k 0)))) (h "1620i62yjcxjhdd3sggwksbhk3jn67nkvgsay6bk1p54a2p6acz5")))

(define-public crate-worldgen-0.1.2 (c (n "worldgen") (v "0.1.2") (d (list (d (n "gcc") (r "^0.3.8") (d #t) (k 1)) (d (n "libc") (r "^0.1.8") (d #t) (k 0)))) (h "1s0pkq6c42dy2hxgl8avm8v9f9y8hqxqkv36jli5dbd8zbnxjkbm")))

(define-public crate-worldgen-0.2.0 (c (n "worldgen") (v "0.2.0") (d (list (d (n "wrapping_macros") (r "*") (d #t) (k 0)))) (h "08ffp4yck8xgmqs3s8k6rmhaxy98znl956csf0pnaw47z1b6l7iz")))

(define-public crate-worldgen-0.3.0 (c (n "worldgen") (v "0.3.0") (d (list (d (n "wrapping_macros") (r "^0.4.4") (d #t) (k 0)))) (h "0xnm64jqy1f0j7a7jmdvsfqfps5yl2n92xrc3msnx786pkr48akc")))

(define-public crate-worldgen-0.3.1 (c (n "worldgen") (v "0.3.1") (d (list (d (n "wrapping_macros") (r "^0.4.5") (d #t) (k 0)))) (h "1325xifwchwl8sfwwk9qndy2l1yknjprm9kkjvyv82lmjcvxilhs")))

(define-public crate-worldgen-0.3.2 (c (n "worldgen") (v "0.3.2") (d (list (d (n "wrapping_macros") (r "^0.4.5") (d #t) (k 0)))) (h "1fdbmgpiwcr4pcclfj5728xwdjg0ipdr4x6vj4ixwsizkg9l270z")))

(define-public crate-worldgen-0.4.0 (c (n "worldgen") (v "0.4.0") (d (list (d (n "wrapping_macros") (r "^0.4.5") (d #t) (k 0)))) (h "1mrfnwcyn0h5y3mz6fm5yvgmndpxdig0j0xbnqnafrkbqgm77768")))

(define-public crate-worldgen-0.5.0 (c (n "worldgen") (v "0.5.0") (h "0iwyikbiq8gdi3hhvndk8qm8ds6qhs4zsiy94xhbkmfl0qcwc7x7")))

(define-public crate-worldgen-0.5.1 (c (n "worldgen") (v "0.5.1") (h "15l7ligxza3xrh3c9mb3s6qrws9syfgymb7i8c0dnz1yvznv8zww")))

(define-public crate-worldgen-0.5.2 (c (n "worldgen") (v "0.5.2") (h "02ym03a1rmjghm0fs933r2sbnji80vv54ps48asgh31phyxxcx89")))

(define-public crate-worldgen-0.5.3 (c (n "worldgen") (v "0.5.3") (h "0ra0q2yr1dbv1a49zjf339nih54icg2z1hcny85bcwy84jx7zdx9")))

