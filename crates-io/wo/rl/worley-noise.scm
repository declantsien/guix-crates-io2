(define-module (crates-io wo rl worley-noise) #:use-module (crates-io))

(define-public crate-worley-noise-0.1.0 (c (n "worley-noise") (v "0.1.0") (d (list (d (n "rand") (r "^0.4.2") (d #t) (k 0)))) (h "0ifkw79vydnh0bbwq6v5mz0bpdd96rc5gpgw2li1l29pypx75b90")))

(define-public crate-worley-noise-1.0.0 (c (n "worley-noise") (v "1.0.0") (d (list (d (n "rand") (r "^0.4.2") (d #t) (k 0)))) (h "0wphxvf3mxiz5qgrbafcn239cwfxnj51bdqlv7dgl36xzxcw1jq8")))

(define-public crate-worley-noise-1.0.1 (c (n "worley-noise") (v "1.0.1") (d (list (d (n "rand") (r "^0.4.2") (d #t) (k 0)))) (h "0gfmbfirk99mrmp1ngmphqd8prshjlyx308ny901lvv77lrx6lpf")))

(define-public crate-worley-noise-1.0.2 (c (n "worley-noise") (v "1.0.2") (d (list (d (n "rand") (r "^0.4.2") (d #t) (k 0)))) (h "0innfpk9i1w2pr1l061312znjhqf97313iripvk74mr6k9chbmzg")))

(define-public crate-worley-noise-1.1.0 (c (n "worley-noise") (v "1.1.0") (d (list (d (n "rand") (r "^0.4.2") (d #t) (k 0)))) (h "1kff083xp3wris9y1cynb01wrgi0yixy9b9bs6lyb8c5l55xkf8j")))

(define-public crate-worley-noise-1.1.1 (c (n "worley-noise") (v "1.1.1") (d (list (d (n "rand") (r "^0.4.2") (d #t) (k 0)))) (h "0cdwzr7m8inmfq5l3vb75vncjn3krr2i06ib603i079ln3hb6ad1")))

(define-public crate-worley-noise-1.1.2 (c (n "worley-noise") (v "1.1.2") (d (list (d (n "image") (r "^0.18.0") (d #t) (k 2)) (d (n "rand") (r "^0.4.2") (d #t) (k 0)))) (h "0cjk1fr7fyd6dbcr3rb0qvmiarzjdc9n1qbr5j599mn895wzl5by")))

(define-public crate-worley-noise-1.1.3 (c (n "worley-noise") (v "1.1.3") (d (list (d (n "image") (r "^0.18.0") (d #t) (k 2)) (d (n "rand") (r "^0.4.2") (d #t) (k 0)))) (h "0c00l36f3skfq59ansvwmr23nsdpxd3lsx0d1dcp9kagplj0i2xj")))

(define-public crate-worley-noise-1.2.0 (c (n "worley-noise") (v "1.2.0") (d (list (d (n "image") (r "^0.18.0") (d #t) (k 2)) (d (n "rand") (r "^0.4.2") (d #t) (k 0)))) (h "1p978ghbnj1gxa6ih6q301fjyj4gj86y2sn5bkmcq417h7f5r024")))

(define-public crate-worley-noise-1.3.0 (c (n "worley-noise") (v "1.3.0") (d (list (d (n "image") (r "^0.18.0") (d #t) (k 2)) (d (n "rand") (r "^0.4.2") (d #t) (k 0)) (d (n "rayon") (r "^0.9.0") (d #t) (k 0)))) (h "07x5f95gw0v8zgpw37x9s1yxpr0c2khxxgcc1rpjxjb994g3r34k")))

(define-public crate-worley-noise-1.3.1 (c (n "worley-noise") (v "1.3.1") (d (list (d (n "image") (r "^0.18.0") (d #t) (k 2)) (d (n "rand") (r "^0.4.2") (d #t) (k 0)) (d (n "rayon") (r "^0.9.0") (d #t) (k 0)))) (h "0rivzy9xfn2w8kr9vpmxxg28jxl4v9aj6zjdl9rfzlv8lcx0jdsk")))

(define-public crate-worley-noise-1.3.2 (c (n "worley-noise") (v "1.3.2") (d (list (d (n "image") (r "^0.18.0") (d #t) (k 2)) (d (n "rand") (r "^0.4.2") (d #t) (k 0)) (d (n "rayon") (r "^0.9.0") (d #t) (k 0)))) (h "1lydxa4kr8lnpaj5kyzh99a7apc8g8x94y5a6rnlmyq73l5mkm1m")))

(define-public crate-worley-noise-1.3.3 (c (n "worley-noise") (v "1.3.3") (d (list (d (n "image") (r "^0.18.0") (d #t) (k 2)) (d (n "rand") (r "^0.4.2") (d #t) (k 0)) (d (n "rayon") (r "^0.9.0") (d #t) (k 0)))) (h "1jhffli1hhlk7k0d6cn7riv0dkzm4i6rlxiriw196fd3lirj1vkx")))

(define-public crate-worley-noise-1.3.5 (c (n "worley-noise") (v "1.3.5") (d (list (d (n "image") (r "^0.18.0") (d #t) (k 2)) (d (n "rand") (r "^0.4.2") (d #t) (k 0)) (d (n "rayon") (r "^0.9.0") (d #t) (k 0)))) (h "0n0q3dr5a0z36vzsprb52bnpl5bwxljsniyhkq2h6wc7f9giyyl4")))

(define-public crate-worley-noise-1.3.6 (c (n "worley-noise") (v "1.3.6") (d (list (d (n "chashmap") (r "^2.2.0") (d #t) (k 0)) (d (n "image") (r "^0.18.0") (d #t) (k 2)) (d (n "rand") (r "^0.4.2") (d #t) (k 0)) (d (n "rayon") (r "^0.9.0") (d #t) (k 0)))) (h "0y9qki0c95s0aq6jzap55iy8cyncaihnxavcav4icwnhdc68ccsl")))

(define-public crate-worley-noise-2.0.1 (c (n "worley-noise") (v "2.0.1") (d (list (d (n "concurrent-hashmap") (r "^0.2.1") (k 0)) (d (n "fnv") (r "^1.0.6") (d #t) (k 0)) (d (n "image") (r "^0.18.0") (d #t) (k 2)) (d (n "rand") (r "^0.5.2") (f (quote ("i128_support"))) (d #t) (k 0)) (d (n "rayon") (r "^1.0.1") (d #t) (k 0)))) (h "046dgvcxpsqq516npxvyw7qrdw4407m5xpmj4bh7bdqbv7hxa5hd")))

(define-public crate-worley-noise-2.0.2 (c (n "worley-noise") (v "2.0.2") (d (list (d (n "concurrent-hashmap") (r "^0.2.1") (k 0)) (d (n "fnv") (r "^1.0.6") (d #t) (k 0)) (d (n "image") (r "^0.20.0") (d #t) (k 2)) (d (n "rand") (r "^0.5.5") (f (quote ("i128_support"))) (d #t) (k 0)) (d (n "rayon") (r "^1.0.2") (d #t) (k 0)))) (h "1mrfrgy3jlvqn0ndvrkzhfbp7nz7r27b96lfq9nnygjf8s9wc8w0")))

(define-public crate-worley-noise-2.0.3 (c (n "worley-noise") (v "2.0.3") (d (list (d (n "concurrent-hashmap") (r "^0.2.1") (k 0)) (d (n "fnv") (r "^1.0.6") (d #t) (k 0)) (d (n "image") (r "^0.20.0") (d #t) (k 2)) (d (n "rand") (r "^0.5.5") (f (quote ("i128_support"))) (d #t) (k 0)) (d (n "rayon") (r "^1.0.2") (d #t) (k 0)))) (h "1rqj6lknb6kmznnx5hc65z9kq61ymhs0w8nlndc6l6yz9p4iss4k")))

(define-public crate-worley-noise-2.0.5 (c (n "worley-noise") (v "2.0.5") (d (list (d (n "concurrent-hashmap") (r "^0.2.1") (k 0)) (d (n "fnv") (r "^1.0.6") (d #t) (k 0)) (d (n "image") (r "^0.20.0") (d #t) (k 2)) (d (n "rand") (r "^0.5.5") (f (quote ("i128_support"))) (d #t) (k 0)) (d (n "rayon") (r "^1.0.2") (d #t) (k 0)))) (h "1imxa37kdvl7llxcs89zr6809dmy0hd6jbxxvcas7h4x9xmq4ll7")))

(define-public crate-worley-noise-2.1.0 (c (n "worley-noise") (v "2.1.0") (d (list (d (n "concurrent-hashmap") (r "^0.2.1") (k 0)) (d (n "fnv") (r "^1.0.6") (d #t) (k 0)) (d (n "image") (r "^0.20.0") (d #t) (k 2)) (d (n "rand") (r "^0.6.5") (f (quote ("i128_support"))) (d #t) (k 0)) (d (n "rand_xorshift") (r "^0.1.1") (d #t) (k 0)) (d (n "rayon") (r "^1.0.3") (d #t) (k 0)))) (h "02z1g657an3i61m4pmsp4scn29blnhbvh0pk4kxf7vfjngf0vqgy") (f (quote (("web" "rand/stdweb"))))))

(define-public crate-worley-noise-2.2.0 (c (n "worley-noise") (v "2.2.0") (d (list (d (n "concurrent-hashmap") (r "^0.2.1") (k 0)) (d (n "fnv") (r "^1.0.6") (d #t) (k 0)) (d (n "image") (r "^0.20.0") (d #t) (k 2)) (d (n "rand") (r "^0.6.5") (f (quote ("i128_support"))) (d #t) (k 0)) (d (n "rand_xorshift") (r "^0.1.1") (d #t) (k 0)) (d (n "rayon") (r "^1.0.3") (d #t) (k 0)))) (h "1ih4p09d48dp3m7pi59c0yb7sc5lnxw8j0hxw6l451w2f24f67ak") (f (quote (("web" "rand/stdweb"))))))

(define-public crate-worley-noise-3.0.0 (c (n "worley-noise") (v "3.0.0") (d (list (d (n "concurrent-hashmap") (r "^0.2.1") (k 0)) (d (n "fnv") (r "^1.0.6") (d #t) (k 0)) (d (n "image") (r "^0.20.0") (d #t) (k 2)) (d (n "rand") (r "^0.6.5") (f (quote ("i128_support"))) (d #t) (k 0)) (d (n "rand_xorshift") (r "^0.1.1") (d #t) (k 0)) (d (n "rayon") (r "^1.0.3") (d #t) (k 0)))) (h "0pgrm9plj0m0zf32ivcsj8wbidxzsvkqyxabdk9wbynh56wpgyy3") (f (quote (("web" "rand/stdweb"))))))

(define-public crate-worley-noise-3.1.0 (c (n "worley-noise") (v "3.1.0") (d (list (d (n "concurrent-hashmap") (r "^0.2.1") (k 0)) (d (n "fnv") (r "^1.0.6") (d #t) (k 0)) (d (n "image") (r "^0.20.0") (d #t) (k 2)) (d (n "rand") (r "^0.6.5") (f (quote ("i128_support"))) (d #t) (k 0)) (d (n "rand_xorshift") (r "^0.1.1") (d #t) (k 0)) (d (n "rayon") (r "^1.0.3") (d #t) (k 0)))) (h "0pbbgfjdvbalkkq092r3d3srhmbjh4fkld1d0v38qcncwmg7ngy1") (f (quote (("web" "rand/stdweb"))))))

(define-public crate-worley-noise-3.1.1 (c (n "worley-noise") (v "3.1.1") (d (list (d (n "concurrent-hashmap") (r "^0.2.1") (k 0)) (d (n "fnv") (r "^1.0.6") (d #t) (k 0)) (d (n "gif") (r "^0.10.1") (d #t) (k 2)) (d (n "image") (r "^0.21.0") (d #t) (k 2)) (d (n "rand") (r "^0.6.5") (f (quote ("i128_support"))) (d #t) (k 0)) (d (n "rand_xorshift") (r "^0.1.1") (d #t) (k 0)) (d (n "rayon") (r "^1.0.3") (d #t) (k 0)))) (h "0n2ci9wg3w45vjgq9df33as10m7x4l58smmqb41dmxj54wd77vq5") (f (quote (("web" "rand/stdweb"))))))

(define-public crate-worley-noise-3.2.1 (c (n "worley-noise") (v "3.2.1") (d (list (d (n "concurrent-hashmap") (r "^0.2.1") (k 0)) (d (n "fnv") (r "^1.0.6") (d #t) (k 0)) (d (n "gif") (r "^0.10.1") (d #t) (k 2)) (d (n "image") (r "^0.21.0") (d #t) (k 2)) (d (n "rand") (r "^0.6.5") (f (quote ("i128_support"))) (d #t) (k 0)) (d (n "rand_xorshift") (r "^0.1.1") (d #t) (k 0)) (d (n "rayon") (r "^1.0.3") (d #t) (k 0)))) (h "0rmih75248fz15ihxkajdbr3i2rapn2i1kjlcm32a6g0fjzg7q0s") (f (quote (("web" "rand/stdweb"))))))

(define-public crate-worley-noise-3.3.0 (c (n "worley-noise") (v "3.3.0") (d (list (d (n "concurrent-hashmap") (r "^0.2.1") (k 0)) (d (n "criterion") (r "^0.2.10") (d #t) (k 2)) (d (n "fnv") (r "^1.0.6") (d #t) (k 0)) (d (n "gif") (r "^0.10.1") (d #t) (k 2)) (d (n "image") (r "^0.21.0") (d #t) (k 2)) (d (n "rand") (r "^0.6.5") (f (quote ("i128_support"))) (d #t) (k 0)) (d (n "rand_xorshift") (r "^0.1.1") (d #t) (k 0)) (d (n "rayon") (r "^1.0.3") (d #t) (k 0)))) (h "18w01dh5lvyhpprr92av0s44g84p7mhla24kpid30q415xib9p01") (f (quote (("web" "rand/stdweb"))))))

(define-public crate-worley-noise-3.4.0 (c (n "worley-noise") (v "3.4.0") (d (list (d (n "concurrent-hashmap") (r "^0.2.1") (k 0)) (d (n "criterion") (r "^0.2.10") (d #t) (k 2)) (d (n "fnv") (r "^1.0.6") (d #t) (k 0)) (d (n "gif") (r "^0.10.1") (d #t) (k 2)) (d (n "image") (r "^0.21.0") (d #t) (k 2)) (d (n "rand") (r "^0.6.5") (f (quote ("i128_support"))) (d #t) (k 0)) (d (n "rand_xorshift") (r "^0.1.1") (d #t) (k 0)) (d (n "rayon") (r "^1.0.3") (d #t) (k 0)))) (h "0lzwbf3rpl7zglzcksy015pqny3hkr4bfisyyf206a9irsg31gqs") (f (quote (("web" "rand/stdweb"))))))

(define-public crate-worley-noise-3.4.1 (c (n "worley-noise") (v "3.4.1") (d (list (d (n "concurrent-hashmap") (r "^0.2.2") (k 0)) (d (n "criterion") (r "^0.2.11") (d #t) (k 2)) (d (n "fnv") (r "^1.0.6") (d #t) (k 0)) (d (n "gif") (r "^0.10.2") (d #t) (k 2)) (d (n "image") (r "^0.21.1") (d #t) (k 2)) (d (n "rand") (r "^0.7.0") (d #t) (k 0)) (d (n "rand_xorshift") (r "^0.2.0") (d #t) (k 0)) (d (n "rayon") (r "^1.1.0") (d #t) (k 0)))) (h "063z5964iajkqqvx2zsndpl75msy91jkfihds02wff20paql0if4") (f (quote (("web" "rand/stdweb") ("nightly" "concurrent-hashmap/default"))))))

(define-public crate-worley-noise-3.5.0 (c (n "worley-noise") (v "3.5.0") (d (list (d (n "concurrent-hashmap") (r "^0.2.2") (k 0)) (d (n "criterion") (r "^0.2.11") (d #t) (k 2)) (d (n "fnv") (r "^1.0.6") (d #t) (k 0)) (d (n "gif") (r "^0.10.2") (d #t) (k 2)) (d (n "image") (r "^0.21.1") (d #t) (k 2)) (d (n "rand") (r "^0.7.0") (d #t) (k 0)) (d (n "rand_xorshift") (r "^0.2.0") (d #t) (k 0)) (d (n "rayon") (r "^1.1.0") (d #t) (k 0)))) (h "1vsp8rm78w33rb4r0ihd1gv1hm96q0l8ivz0imqypbgv2anaf0jx") (f (quote (("web" "rand/stdweb") ("nightly" "concurrent-hashmap/default"))))))

(define-public crate-worley-noise-3.5.1 (c (n "worley-noise") (v "3.5.1") (d (list (d (n "concurrent-hashmap") (r "^0.2.2") (k 0)) (d (n "criterion") (r "^0.2.11") (d #t) (k 2)) (d (n "fnv") (r "^1.0.6") (d #t) (k 0)) (d (n "gif") (r "^0.10.2") (d #t) (k 2)) (d (n "image") (r "^0.21.1") (d #t) (k 2)) (d (n "rand") (r "^0.7.0") (d #t) (k 0)) (d (n "rand_xorshift") (r "^0.2.0") (d #t) (k 0)) (d (n "rayon") (r "^1.1.0") (d #t) (k 0)))) (h "090009wh1pyq1w2cpkkcj8acizx0hhmdj7i5byrf9qbq1df9ncbz") (f (quote (("web" "rand/stdweb") ("nightly" "concurrent-hashmap/default"))))))

(define-public crate-worley-noise-3.6.0 (c (n "worley-noise") (v "3.6.0") (d (list (d (n "concurrent-hashmap") (r "^0.2.2") (k 0)) (d (n "criterion") (r "^0.3.2") (d #t) (k 2)) (d (n "fnv") (r "^1.0.7") (d #t) (k 0)) (d (n "gif") (r "^0.10.3") (d #t) (k 2)) (d (n "image") (r "^0.23.4") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "rand_xorshift") (r "^0.2.0") (d #t) (k 0)) (d (n "rayon") (r "^1.3.0") (d #t) (k 0)))) (h "1km25nxhh2d6gq8mqcv0z98zq30347k54r8aw853dpp6l7bmmvld") (f (quote (("web" "rand/stdweb") ("nightly" "concurrent-hashmap/default"))))))

(define-public crate-worley-noise-3.7.0 (c (n "worley-noise") (v "3.7.0") (d (list (d (n "concurrent-hashmap") (r "^0.2.2") (k 0)) (d (n "criterion") (r "^0.3.2") (d #t) (k 2)) (d (n "fnv") (r "^1.0.7") (d #t) (k 0)) (d (n "getrandom") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "gif") (r "^0.10.3") (d #t) (k 2)) (d (n "image") (r "^0.23.4") (d #t) (k 2)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "rand_xorshift") (r "^0.3.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5.0") (d #t) (k 0)))) (h "0in0glgkszbl0x7mdplw0m5471jfmzxxq36cri3d3svdyyh3835p") (f (quote (("web" "getrandom" "getrandom/js") ("nightly" "concurrent-hashmap/default"))))))

(define-public crate-worley-noise-3.7.2 (c (n "worley-noise") (v "3.7.2") (d (list (d (n "concurrent-hashmap") (r "^0.2.2") (k 0)) (d (n "criterion") (r "^0.3.2") (d #t) (k 2)) (d (n "fnv") (r "^1.0.7") (d #t) (k 0)) (d (n "getrandom") (r "^0.2.3") (o #t) (d #t) (k 0)) (d (n "gif") (r "^0.10.3") (d #t) (k 2)) (d (n "image") (r "^0.23.4") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rand_xorshift") (r "^0.3.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)))) (h "12crpidnznwc8mkcqzarymnvinnf3qp200rlpj051ad12a8a7749") (f (quote (("web" "getrandom" "getrandom/js"))))))

