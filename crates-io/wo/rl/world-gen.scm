(define-module (crates-io wo rl world-gen) #:use-module (crates-io))

(define-public crate-world-gen-0.1.0 (c (n "world-gen") (v "0.1.0") (d (list (d (n "noise") (r "^0.5.1") (d #t) (k 0)))) (h "0lacb2vg75rmh2jlyyna0i5mph2wvlcjjx15w0nscmcmv563c3bw")))

(define-public crate-world-gen-0.1.1 (c (n "world-gen") (v "0.1.1") (d (list (d (n "noise") (r "^0.5.1") (d #t) (k 0)))) (h "05y6jk4m8bzy5m0sh3r2nixlqfzgfa73jsw75v2fph7a50fwrnch")))

(define-public crate-world-gen-0.1.2 (c (n "world-gen") (v "0.1.2") (d (list (d (n "noise") (r "^0.5.1") (d #t) (k 0)))) (h "0d524sxf2vhkf94j6mfjacw32rb95171d2qyc35w086q4ln4pv9h")))

