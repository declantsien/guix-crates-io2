(define-module (crates-io wo rl world-painter) #:use-module (crates-io))

(define-public crate-world-painter-0.1.0 (c (n "world-painter") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "carlog") (r "^0.1.0") (d #t) (k 0)) (d (n "color-art") (r "^0.3.1") (d #t) (k 0)) (d (n "image") (r "^0.24.6") (d #t) (k 0)) (d (n "mca-parser") (r "^0.2.0") (d #t) (k 0)) (d (n "progress_bar") (r "^1.0.3") (d #t) (k 0)))) (h "1ybhd5vaj36xpyc3qp8l3z67glfiwqh1964dqps1yp0hhmic927q")))

