(define-module (crates-io wo lo wololo) #:use-module (crates-io))

(define-public crate-wololo-0.1.0 (c (n "wololo") (v "0.1.0") (d (list (d (n "argh") (r "^0.1.3") (d #t) (k 0)) (d (n "eui48") (r "^1.0.1") (d #t) (k 0)))) (h "0j70rqp4z06znzc0nkwwl6sgwa2ca9iincdjj6insr77aghsnfmv")))

(define-public crate-wololo-0.1.1 (c (n "wololo") (v "0.1.1") (d (list (d (n "argh") (r "^0.1.3") (d #t) (k 0)) (d (n "eui48") (r "^1.0.1") (d #t) (k 0)))) (h "1xd3li2rqljhr73731hdg5xy8s1zsyms1zrq9m9ik36ic7xiz14l")))

(define-public crate-wololo-0.2.0 (c (n "wololo") (v "0.2.0") (d (list (d (n "argh") (r "^0.1.3") (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (d #t) (k 0)) (d (n "eui48") (r "^1.0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)) (d (n "tracing") (r "^0.1.36") (d #t) (k 0)) (d (n "tracing-error") (r "^0.2.0") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.15") (f (quote ("registry" "env-filter"))) (d #t) (k 0)))) (h "1anq1d5656amrsym5ykjp2z6vlnjxw33inxgap2zwr0pll2l05rj")))

