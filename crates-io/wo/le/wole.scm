(define-module (crates-io wo le wole) #:use-module (crates-io))

(define-public crate-wole-0.1.2 (c (n "wole") (v "0.1.2") (h "1cyn91l4mgiqsj66kf45bp9gn50pimnqky6d9rjq1rn20ras76m3")))

(define-public crate-wole-0.1.3 (c (n "wole") (v "0.1.3") (h "103fsfg571ms377xpdhk2ac7ijy4v4s5g5y361pq3fv0ik2k418k")))

(define-public crate-wole-0.1.4 (c (n "wole") (v "0.1.4") (h "1yznycspxg57qhc33nccjm5cfjas5nbmjir2vnl4ryd72bdpd396")))

(define-public crate-wole-0.1.5 (c (n "wole") (v "0.1.5") (h "0cw934249gh5gmfcmpnsdyh252sixnnw41vwfv9yyl4x32clkb9s")))

(define-public crate-wole-1.2.6 (c (n "wole") (v "1.2.6") (h "1iqll8dn96s6dsr0wy2vkiyj5qijacgwyzf90b92i63bpxri91xj")))

(define-public crate-wole-1.2.7 (c (n "wole") (v "1.2.7") (h "0y8vr1290h4rz45v667rq5m1h576sk6gv40jxvrwnf0ph5d5w1mf")))

(define-public crate-wole-1.2.8 (c (n "wole") (v "1.2.8") (h "0zy0gi1zlh2gkrswk9gjc7rc4j2yq1qq2rlczizxgxacwxk3zkdf")))

