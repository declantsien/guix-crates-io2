(define-module (crates-io wo t_ wot_types) #:use-module (crates-io))

(define-public crate-wot_types-0.1.0 (c (n "wot_types") (v "0.1.0") (d (list (d (n "num_enum") (r "^0.5.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("serde_derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11.7") (d #t) (k 0)) (d (n "serde_with") (r "^2.0.1") (f (quote ("hex"))) (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (d #t) (k 0)))) (h "18nqyd08wlfjc9lp5i1g582bc0jxgwv7zkr8vidjmx7189wm5n38")))

