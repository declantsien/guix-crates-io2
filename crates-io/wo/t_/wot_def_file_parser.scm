(define-module (crates-io wo t_ wot_def_file_parser) #:use-module (crates-io))

(define-public crate-wot_def_file_parser-0.1.0 (c (n "wot_def_file_parser") (v "0.1.0") (d (list (d (n "roxmltree") (r "^0.14.1") (d #t) (k 0)) (d (n "utils") (r "^0.1.0") (d #t) (k 0) (p "wot-battle-results-parser-utils")))) (h "0v38n2vw1w95sjjdv8z0qm5zq7ax40a3sdb6gf8jsl1z7v7jv6q5")))

