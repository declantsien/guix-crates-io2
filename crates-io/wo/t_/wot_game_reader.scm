(define-module (crates-io wo t_ wot_game_reader) #:use-module (crates-io))

(define-public crate-wot_game_reader-0.1.0 (c (n "wot_game_reader") (v "0.1.0") (d (list (d (n "merge-hashmap") (r "^0.1.2") (d #t) (k 0)) (d (n "poreader") (r "^1.1.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.6") (d #t) (k 0)) (d (n "serde_variant") (r "^0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0jjj1gmxan4x9lqc9d0an1mgj93335h5dsx5czv22b2wwsfxvv9g")))

