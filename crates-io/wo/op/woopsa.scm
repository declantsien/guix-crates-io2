(define-module (crates-io wo op woopsa) #:use-module (crates-io))

(define-public crate-woopsa-0.1.0 (c (n "woopsa") (v "0.1.0") (h "1ik3gplrygqgksk7c69in49zijm813iaq813rz8m90a0xmyvx1n1")))

(define-public crate-woopsa-0.1.1 (c (n "woopsa") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1rmv47gyvhgp5bdwlrfgn2hrkihiivrys1py0gyfdw7bdk040qzk")))

