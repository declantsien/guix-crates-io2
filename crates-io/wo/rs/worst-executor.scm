(define-module (crates-io wo rs worst-executor) #:use-module (crates-io))

(define-public crate-worst-executor-0.1.0 (c (n "worst-executor") (v "0.1.0") (d (list (d (n "async-channel") (r "^1") (d #t) (k 2)) (d (n "async-fs") (r "^1") (d #t) (k 2)) (d (n "async-net") (r "^1") (d #t) (k 2)) (d (n "futures") (r "^0.3") (f (quote ("std" "async-await"))) (k 2)))) (h "09z58qf98yrlqvpkwqz150camhhr1ic4x3lyj7h4vyq41cdpv1nz")))

(define-public crate-worst-executor-0.1.1 (c (n "worst-executor") (v "0.1.1") (d (list (d (n "async-channel") (r "^1") (d #t) (k 2)) (d (n "async-fs") (r "^1") (d #t) (k 2)) (d (n "async-net") (r "^1") (d #t) (k 2)) (d (n "futures") (r "^0.3") (f (quote ("std" "async-await"))) (k 2)))) (h "0h6dkgs3rm1k2ci714agp0a6wjhfy29410xy4vwiks79arxhvyx0")))

