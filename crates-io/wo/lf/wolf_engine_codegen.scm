(define-module (crates-io wo lf wolf_engine_codegen) #:use-module (crates-io))

(define-public crate-wolf_engine_codegen-0.1.0 (c (n "wolf_engine_codegen") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.76") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (d #t) (k 0)))) (h "1gm8n9bb5svv00gqcpdqa54sd9halaba9khxm6bhirmqxzzkb01g")))

