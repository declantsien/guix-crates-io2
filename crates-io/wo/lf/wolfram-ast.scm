(define-module (crates-io wo lf wolfram-ast) #:use-module (crates-io))

(define-public crate-wolfram-ast-0.0.0 (c (n "wolfram-ast") (v "0.0.0") (d (list (d (n "pratt") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.190") (o #t) (d #t) (k 0)) (d (n "wolfram-error") (r "0.0.*") (d #t) (k 0)) (d (n "yggdrasil-rt") (r "0.0.*") (o #t) (d #t) (k 0)))) (h "034ab3bgfvq2d65wqd854g8rsp93b5pky30izp251wvsri814k41") (f (quote (("parser" "yggdrasil-rt" "pratt") ("default" "parser")))) (s 2) (e (quote (("serde" "dep:serde"))))))

