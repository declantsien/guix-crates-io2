(define-module (crates-io wo lf wolf_engine_sdl2) #:use-module (crates-io))

(define-public crate-wolf_engine_sdl2-0.1.0 (c (n "wolf_engine_sdl2") (v "0.1.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mockall") (r "^0.11") (d #t) (k 2)) (d (n "sdl2") (r "^0.35") (f (quote ("image"))) (d #t) (k 0)) (d (n "wolf_engine") (r "^0.15") (d #t) (k 0)))) (h "1zcqwyk9ha5r0w44mc099mh2w7sm4rdx3rxhgc5x3syb4p97ch0l")))

(define-public crate-wolf_engine_sdl2-0.1.1 (c (n "wolf_engine_sdl2") (v "0.1.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mockall") (r "^0.11") (d #t) (k 2)) (d (n "sdl2") (r "^0.35") (f (quote ("image"))) (d #t) (k 0)) (d (n "wolf_engine") (r "^0.15") (d #t) (k 0)))) (h "0i0z6ammv1icysrnlyn7dc0jbkvi3gqqxx3gwvq18b6iz2vj86jb")))

(define-public crate-wolf_engine_sdl2-0.2.1 (c (n "wolf_engine_sdl2") (v "0.2.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mockall") (r "^0.11") (d #t) (k 2)) (d (n "sdl2") (r "^0.35") (f (quote ("image" "gfx"))) (d #t) (k 0)) (d (n "wolf_engine") (r "^0.15") (d #t) (k 0)))) (h "1j363ir5lz5g0sqb3vwx1dc9984s05cwd3ayskmzgnfac88jzf3g")))

(define-public crate-wolf_engine_sdl2-0.2.2 (c (n "wolf_engine_sdl2") (v "0.2.2") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mockall") (r "^0.11") (d #t) (k 2)) (d (n "sdl2") (r "^0.35") (f (quote ("image" "gfx"))) (d #t) (k 0)) (d (n "wolf_engine") (r "0.*") (d #t) (k 0)))) (h "06xhc2da1xjb4rx06gli333a2bj8p2gl9ch1mp519pawljlidyhb")))

(define-public crate-wolf_engine_sdl2-0.2.3 (c (n "wolf_engine_sdl2") (v "0.2.3") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mockall") (r "^0.11") (d #t) (k 2)) (d (n "sdl2") (r "^0.35") (f (quote ("image" "gfx"))) (d #t) (k 0)) (d (n "wolf_engine") (r "0.*") (d #t) (k 0)))) (h "124rrgykwzpr1z3yaanb62iaq08czy02k8p33qg7n71nzh074da1")))

(define-public crate-wolf_engine_sdl2-0.3.0 (c (n "wolf_engine_sdl2") (v "0.3.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "sdl2") (r ">=0.35") (d #t) (k 0)) (d (n "wolf_engine") (r ">=0.17") (d #t) (k 0)))) (h "0pcwfmm4x3wyv98grn9x3vsh6anxqribx2alxpy18hi7bn30w84l")))

(define-public crate-wolf_engine_sdl2-0.4.0 (c (n "wolf_engine_sdl2") (v "0.4.0") (d (list (d (n "colors-transform") (r "^0.2") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "sdl2") (r ">=0.35") (d #t) (k 0)) (d (n "sdl2") (r ">=0.35") (f (quote ("gfx" "mixer"))) (d #t) (k 2)) (d (n "wolf_engine") (r ">=0.17") (d #t) (k 0)) (d (n "wolf_engine") (r ">=0.17") (f (quote ("logging"))) (d #t) (k 2)))) (h "0gn21imgiw8px32l4qyjb09ynl3s8nmmsd6vscmjhzhbfh1a9w73") (f (quote (("mixer" "sdl2/mixer"))))))

(define-public crate-wolf_engine_sdl2-0.5.0 (c (n "wolf_engine_sdl2") (v "0.5.0") (d (list (d (n "colors-transform") (r "^0.2") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "sdl2") (r ">=0.35") (d #t) (k 0)) (d (n "sdl2") (r ">=0.35") (f (quote ("gfx" "mixer"))) (d #t) (k 2)) (d (n "wolf_engine") (r ">=0.17") (d #t) (k 0)) (d (n "wolf_engine") (r ">=0.17") (f (quote ("logging"))) (d #t) (k 2)))) (h "1p9l1l0klk4h5daprg1p1apq1b0w1nbm7ap0941j66qnvznz9rf9") (f (quote (("mixer" "sdl2/mixer"))))))

(define-public crate-wolf_engine_sdl2-0.7.0 (c (n "wolf_engine_sdl2") (v "0.7.0") (d (list (d (n "colors-transform") (r "^0.2") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "sdl2") (r ">=0.35") (d #t) (k 0)) (d (n "sdl2") (r ">=0.35") (f (quote ("gfx" "mixer"))) (d #t) (k 2)) (d (n "wolf_engine") (r ">=0.21") (d #t) (k 0)) (d (n "wolf_engine") (r ">=0.21") (f (quote ("logging"))) (d #t) (k 2)))) (h "1z2mgxfc6fh8l3md11m8xyz7qvqlsd5qdbds9rvz95575p4x3kkg") (f (quote (("mixer" "sdl2/mixer"))))))

(define-public crate-wolf_engine_sdl2-0.7.1 (c (n "wolf_engine_sdl2") (v "0.7.1") (d (list (d (n "colors-transform") (r "^0.2") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "sdl2") (r ">=0.35") (d #t) (k 0)) (d (n "sdl2") (r ">=0.35") (f (quote ("gfx" "mixer"))) (d #t) (k 2)) (d (n "wolf_engine") (r ">=0.21") (d #t) (k 0)) (d (n "wolf_engine") (r ">=0.21") (f (quote ("logging"))) (d #t) (k 2)))) (h "03jx6hhny62bigfdw0vvv4k6g77dz46s62ybbnfwqwd3fwcpjrsz") (f (quote (("mixer" "sdl2/mixer"))))))

