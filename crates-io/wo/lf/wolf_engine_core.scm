(define-module (crates-io wo lf wolf_engine_core) #:use-module (crates-io))

(define-public crate-wolf_engine_core-0.24.1 (c (n "wolf_engine_core") (v "0.24.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mockall") (r "^0.11") (d #t) (k 2)) (d (n "ntest") (r "^0.8") (d #t) (k 2)) (d (n "simple_logger") (r "^1.13") (o #t) (d #t) (k 0)) (d (n "test-case") (r "^1.2") (d #t) (k 2)))) (h "03d1ghdyn2jw2f5vazxz46wbhkmxk6n3ihrbq6xyay6a95f26mgw") (f (quote (("logging" "simple_logger"))))))

(define-public crate-wolf_engine_core-0.25.0 (c (n "wolf_engine_core") (v "0.25.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mockall") (r "^0.11") (d #t) (k 2)) (d (n "ntest") (r "^0.8") (d #t) (k 2)) (d (n "simple_logger") (r "^1.13") (o #t) (d #t) (k 0)) (d (n "test-case") (r "^1.2") (d #t) (k 2)))) (h "0ccmgkc717fh4gpwq8vhc9xqmz6pgppn5q0ykd5h2l44pmrx5djg") (f (quote (("logging" "simple_logger"))))))

