(define-module (crates-io wo lf wolfsay) #:use-module (crates-io))

(define-public crate-wolfsay-0.1.1 (c (n "wolfsay") (v "0.1.1") (d (list (d (n "assert_cmd") (r "^1.0.2") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.8") (d #t) (k 0)) (d (n "predicates") (r "^1.0.6") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "0isrr8bkfck4vb9p0dz7pinqgwswzpwxwigx41anap9cad7dmy7i")))

