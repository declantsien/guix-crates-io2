(define-module (crates-io wo lf wolfram-library-link-macros) #:use-module (crates-io))

(define-public crate-wolfram-library-link-macros-0.1.2 (c (n "wolfram-library-link-macros") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.14") (d #t) (k 0)) (d (n "syn") (r "^1.0.85") (f (quote ("full"))) (d #t) (k 0)))) (h "1xabw11p6g0k5pfqw7ic3y5kf96lq40zldb98zbrg8jfskjqjby1")))

(define-public crate-wolfram-library-link-macros-0.2.0 (c (n "wolfram-library-link-macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.14") (d #t) (k 0)) (d (n "syn") (r "^1.0.85") (f (quote ("full"))) (d #t) (k 0)))) (h "024gc1xzl3a8s90vfz9mwy00r2kn4q0d9bp018744904js0ixbzp")))

(define-public crate-wolfram-library-link-macros-0.2.1 (c (n "wolfram-library-link-macros") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.14") (d #t) (k 0)) (d (n "syn") (r "^1.0.85") (f (quote ("full"))) (d #t) (k 0)))) (h "0ak5j3jc3vnglcwb3ayy04z36dcyanlz07v4h0d2dj8mjqmj2bhf")))

(define-public crate-wolfram-library-link-macros-0.2.2 (c (n "wolfram-library-link-macros") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.14") (d #t) (k 0)) (d (n "syn") (r "^1.0.85") (f (quote ("full"))) (d #t) (k 0)))) (h "0s7f8sgr6xmz85fwdp2p1n5i3fbw9rhdm862d7a7lnly2xm6a54z")))

(define-public crate-wolfram-library-link-macros-0.2.3 (c (n "wolfram-library-link-macros") (v "0.2.3") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.14") (d #t) (k 0)) (d (n "syn") (r "^1.0.85") (f (quote ("full"))) (d #t) (k 0)))) (h "1v3jj0q8kwalgrsldcwnidyxvd6mi78p0dw832xwl73s7jy0z9g8")))

(define-public crate-wolfram-library-link-macros-0.2.4 (c (n "wolfram-library-link-macros") (v "0.2.4") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.14") (d #t) (k 0)) (d (n "syn") (r "^1.0.85") (f (quote ("full"))) (d #t) (k 0)))) (h "1mp6dd8l89gjc18grj5lbwra7kwp61zg3rcv4npl9vvfpsh2varv")))

(define-public crate-wolfram-library-link-macros-0.2.5 (c (n "wolfram-library-link-macros") (v "0.2.5") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.14") (d #t) (k 0)) (d (n "syn") (r "^1.0.85") (f (quote ("full"))) (d #t) (k 0)))) (h "00mbrbymhprap7wj5i4ln168q8bga4mfka6j3vszw3f9a3yzz52c")))

(define-public crate-wolfram-library-link-macros-0.2.6 (c (n "wolfram-library-link-macros") (v "0.2.6") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.14") (d #t) (k 0)) (d (n "syn") (r "^1.0.85") (f (quote ("full"))) (d #t) (k 0)))) (h "0czd4ww0ppjnw48gnhay7yv6271a6i7h6qyy8vrnkjc8ya9k4bf5")))

(define-public crate-wolfram-library-link-macros-0.2.7 (c (n "wolfram-library-link-macros") (v "0.2.7") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.14") (d #t) (k 0)) (d (n "syn") (r "^1.0.85") (f (quote ("full"))) (d #t) (k 0)))) (h "159srgg0hwh2sva1hvl38qkq0yqpdalahzd90vjrz7yxjrxkbh9b")))

(define-public crate-wolfram-library-link-macros-0.2.8 (c (n "wolfram-library-link-macros") (v "0.2.8") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.14") (d #t) (k 0)) (d (n "syn") (r "^1.0.85") (f (quote ("full"))) (d #t) (k 0)))) (h "0zbjz5l4vndiny6syyhib108h469s5gnpv0kj690mgkcp258qi2f")))

(define-public crate-wolfram-library-link-macros-0.2.9 (c (n "wolfram-library-link-macros") (v "0.2.9") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.14") (d #t) (k 0)) (d (n "syn") (r "^1.0.85") (f (quote ("full"))) (d #t) (k 0)))) (h "0psiqww83hk2a99a6sc6xzivwhf9a3qfv0gxhw88nvwjp3d6z9n8") (f (quote (("default") ("automate-function-loading-boilerplate"))))))

(define-public crate-wolfram-library-link-macros-0.2.10 (c (n "wolfram-library-link-macros") (v "0.2.10") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.14") (d #t) (k 0)) (d (n "syn") (r "^1.0.85") (f (quote ("full"))) (d #t) (k 0)))) (h "01mfk4phd2pvvvnwccdgq2jd1vb7lwfca23x2z9mw1nv2iqj7npr") (f (quote (("default") ("automate-function-loading-boilerplate"))))))

