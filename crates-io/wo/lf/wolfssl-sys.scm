(define-module (crates-io wo lf wolfssl-sys) #:use-module (crates-io))

(define-public crate-wolfssl-sys-0.0.1 (c (n "wolfssl-sys") (v "0.0.1") (h "089x05rk4171gzg41v3a73jkircv57gq9flgqzf6b7b91vw6dyp4")))

(define-public crate-wolfssl-sys-0.1.0 (c (n "wolfssl-sys") (v "0.1.0") (d (list (d (n "autotools") (r "^0.2") (d #t) (k 1)) (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)))) (h "0xv3ik32mgh77di49bdx4jlfbvl9yqmkqlnzbb2m96yd4i44d5hv")))

(define-public crate-wolfssl-sys-0.1.1 (c (n "wolfssl-sys") (v "0.1.1") (d (list (d (n "autotools") (r "^0.2") (d #t) (k 1)) (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)))) (h "0iy28jnx8g5x6990k8msni92ns3pyvqzbvlnb1iwk2avhbvc171w") (l "wolfssl")))

(define-public crate-wolfssl-sys-0.1.2 (c (n "wolfssl-sys") (v "0.1.2") (d (list (d (n "autotools") (r "^0.2") (d #t) (k 1)) (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)))) (h "0zcpzv2bcm3ywvkqwgs9j93vh2dyjkl0hjp74d11n1v2b6ix4w4m") (l "wolfssl")))

(define-public crate-wolfssl-sys-0.1.3 (c (n "wolfssl-sys") (v "0.1.3") (d (list (d (n "autotools") (r "^0.2") (d #t) (k 1)) (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)))) (h "1yni2whhqwmf3w9bw741jnkqjgl03835kwwsdgi2hjav4ifyb8ab") (l "wolfssl")))

(define-public crate-wolfssl-sys-0.1.4 (c (n "wolfssl-sys") (v "0.1.4") (d (list (d (n "autotools") (r "^0.2") (d #t) (k 1)) (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)) (d (n "build-target") (r "^0.4.0") (d #t) (k 1)))) (h "1m78ni17adpcydqzhkvymsqb8dvy1f4r81npwx7ndmzh9zr4p1vk") (l "wolfssl")))

(define-public crate-wolfssl-sys-0.1.5 (c (n "wolfssl-sys") (v "0.1.5") (d (list (d (n "autotools") (r "^0.2") (d #t) (k 1)) (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)) (d (n "build-target") (r "^0.4.0") (d #t) (k 1)))) (h "04xdq8nw1z56d970jiln0wak1m8v4c5pzm1l0mxhm1m4cdb8xhg0") (l "wolfssl")))

(define-public crate-wolfssl-sys-0.1.6 (c (n "wolfssl-sys") (v "0.1.6") (d (list (d (n "autotools") (r "^0.2") (d #t) (k 1)) (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)) (d (n "build-target") (r "^0.4.0") (d #t) (k 1)) (d (n "oqs-sys") (r "^0.7.1") (f (quote ("kems" "sigs"))) (o #t) (k 0)))) (h "1f21vch63aqldk2ckk8pr3hm7qccq7jzbswcyph9k4l0qbf6v8gg") (f (quote (("default")))) (l "wolfssl") (s 2) (e (quote (("postquantum" "dep:oqs-sys"))))))

(define-public crate-wolfssl-sys-0.1.7 (c (n "wolfssl-sys") (v "0.1.7") (d (list (d (n "autotools") (r "^0.2") (d #t) (k 1)) (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)) (d (n "build-target") (r "^0.4.0") (d #t) (k 1)) (d (n "oqs-sys") (r "^0.7.1") (f (quote ("kems" "sigs"))) (o #t) (k 0)))) (h "06jqnyamnd7jfb38f9ixsdjw6imwvpcjar7jvxls0law7q4qxvpc") (f (quote (("default")))) (l "wolfssl") (s 2) (e (quote (("postquantum" "dep:oqs-sys"))))))

(define-public crate-wolfssl-sys-0.1.8 (c (n "wolfssl-sys") (v "0.1.8") (d (list (d (n "autotools") (r "^0.2") (d #t) (k 1)) (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)) (d (n "build-target") (r "^0.4.0") (d #t) (k 1)) (d (n "oqs-sys") (r "^0.7.1") (f (quote ("kems" "sigs"))) (o #t) (k 0)))) (h "1mscgk5917n8bqghr3dsc9h954i1n10zgdgx1r5vvd8ls7wsanva") (f (quote (("default")))) (l "wolfssl") (s 2) (e (quote (("postquantum" "dep:oqs-sys"))))))

(define-public crate-wolfssl-sys-0.1.9 (c (n "wolfssl-sys") (v "0.1.9") (d (list (d (n "autotools") (r "^0.2") (d #t) (k 1)) (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)) (d (n "build-target") (r "^0.4.0") (d #t) (k 1)) (d (n "oqs-sys") (r "^0.7.2") (f (quote ("kems" "sigs"))) (o #t) (k 0)))) (h "0gjfxk6ppgdigfiyfwq6bmlwaqpj06z29r0c7jlabgdjk0d4yldq") (f (quote (("default")))) (l "wolfssl") (s 2) (e (quote (("postquantum" "dep:oqs-sys"))))))

(define-public crate-wolfssl-sys-0.1.10 (c (n "wolfssl-sys") (v "0.1.10") (d (list (d (n "autotools") (r "^0.2") (d #t) (k 1)) (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)) (d (n "build-target") (r "^0.4.0") (d #t) (k 1)) (d (n "oqs-sys") (r "^0.7.2") (f (quote ("kems" "sigs"))) (o #t) (k 0)))) (h "1s2d9g8jym952l0lzl2gji4cabanah00scshdy51z68mdxd1srya") (f (quote (("default")))) (l "wolfssl") (s 2) (e (quote (("postquantum" "dep:oqs-sys"))))))

(define-public crate-wolfssl-sys-0.1.12 (c (n "wolfssl-sys") (v "0.1.12") (d (list (d (n "autotools") (r "^0.2") (d #t) (k 1)) (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)) (d (n "build-target") (r "^0.4.0") (d #t) (k 1)) (d (n "oqs-sys") (r "^0.7.2") (f (quote ("kems" "sigs"))) (o #t) (k 0)))) (h "0rm8b1968cy7gr51z6sicj9aa4fmsp5dg7h0rmv9byxzb4p9ri02") (f (quote (("default")))) (l "wolfssl") (s 2) (e (quote (("postquantum" "dep:oqs-sys"))))))

(define-public crate-wolfssl-sys-0.1.13 (c (n "wolfssl-sys") (v "0.1.13") (d (list (d (n "autotools") (r "^0.2") (d #t) (k 1)) (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)) (d (n "build-target") (r "^0.4.0") (d #t) (k 1)) (d (n "oqs-sys") (r "^0.7.2") (f (quote ("kems" "sigs"))) (o #t) (k 0)))) (h "0zdjgkdgkpjr5vwj3cd2zsp9kv706907rhk2v8r6qzlqbdi7km8y") (f (quote (("default")))) (l "wolfssl") (s 2) (e (quote (("postquantum" "dep:oqs-sys"))))))

(define-public crate-wolfssl-sys-0.1.14 (c (n "wolfssl-sys") (v "0.1.14") (d (list (d (n "autotools") (r "^0.2") (d #t) (k 1)) (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)) (d (n "build-target") (r "^0.4.0") (d #t) (k 1)) (d (n "oqs-sys") (r "^0.7.2") (f (quote ("kems" "sigs"))) (o #t) (k 0)))) (h "1j3mp09f75zcgs54m012d1c41k46h5kq7z1b6x6q43dbqpzrgrp6") (f (quote (("default")))) (l "wolfssl") (s 2) (e (quote (("postquantum" "dep:oqs-sys"))))))

(define-public crate-wolfssl-sys-0.1.15 (c (n "wolfssl-sys") (v "0.1.15") (d (list (d (n "autotools") (r "^0.2") (d #t) (k 1)) (d (n "bindgen") (r "^0.69") (d #t) (k 1)) (d (n "build-target") (r "^0.4.0") (d #t) (k 1)) (d (n "oqs-sys") (r "^0.9.1") (f (quote ("kems" "sigs"))) (o #t) (k 0)))) (h "0qaq0f1ibsmzpvznili21ypjzx6jd5ix8sb4nxnqq1mlf9f8iwaf") (f (quote (("default") ("debug")))) (l "wolfssl") (s 2) (e (quote (("postquantum" "dep:oqs-sys"))))))

(define-public crate-wolfssl-sys-0.1.16 (c (n "wolfssl-sys") (v "0.1.16") (d (list (d (n "autotools") (r "^0.2") (d #t) (k 1)) (d (n "bindgen") (r "^0.69") (d #t) (k 1)) (d (n "build-target") (r "^0.4.0") (d #t) (k 1)) (d (n "oqs-sys") (r "^0.9.1") (f (quote ("kems" "sigs"))) (o #t) (k 0)))) (h "1sl27dqnbyw7bz8vj9yr8v6f28xyif3bw5x65nzzh2rkz8s36l0q") (f (quote (("default") ("debug")))) (l "wolfssl") (s 2) (e (quote (("postquantum" "dep:oqs-sys"))))))

(define-public crate-wolfssl-sys-1.0.0 (c (n "wolfssl-sys") (v "1.0.0") (d (list (d (n "autotools") (r "^0.2") (d #t) (k 1)) (d (n "bindgen") (r "^0.69") (d #t) (k 1)) (d (n "build-target") (r "^0.4.0") (d #t) (k 1)) (d (n "oqs-sys") (r "^0.9.1") (f (quote ("kems" "sigs"))) (o #t) (k 0)))) (h "02ws6azhbg9lfmf870yz0a8avh8nxqmcmb2rbkkp9jfyicddbmxd") (f (quote (("default") ("debug")))) (l "wolfssl") (s 2) (e (quote (("postquantum" "dep:oqs-sys"))))))

