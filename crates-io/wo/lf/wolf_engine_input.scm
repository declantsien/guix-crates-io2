(define-module (crates-io wo lf wolf_engine_input) #:use-module (crates-io))

(define-public crate-wolf_engine_input-0.0.0 (c (n "wolf_engine_input") (v "0.0.0") (h "0h1ayi4wxhb9bnrpivlhgq6xk70xamnp3fw79pv40pcm4h26dvqq")))

(define-public crate-wolf_engine_input-0.1.0 (c (n "wolf_engine_input") (v "0.1.0") (d (list (d (n "test-case") (r "^3.3.1") (d #t) (k 2)) (d (n "winit") (r "^0.29.14") (o #t) (d #t) (k 0)))) (h "05gps9q38m5x5ihi622xp4psjdn9cqs4v8z9vykfg103w4ysjmh0")))

(define-public crate-wolf_engine_input-0.1.1 (c (n "wolf_engine_input") (v "0.1.1") (d (list (d (n "test-case") (r "^3.3.1") (d #t) (k 2)) (d (n "winit") (r "^0.29") (o #t) (d #t) (k 0)))) (h "1qc9lnvsjdmw96jidnm3dvl6gmfx27ql2xkvw0lfskqfbp0j2xlf")))

