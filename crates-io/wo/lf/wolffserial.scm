(define-module (crates-io wo lf wolffserial) #:use-module (crates-io))

(define-public crate-wolffserial-0.1.1 (c (n "wolffserial") (v "0.1.1") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "serialport") (r "^4") (d #t) (k 0)))) (h "0shs51mwwdd7rvzi7y03zyi96gg5kwlr4ax78fslnhh9icnd0vgd")))

(define-public crate-wolffserial-0.1.2 (c (n "wolffserial") (v "0.1.2") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "serialport") (r "^4") (d #t) (k 0)))) (h "01hab34mr0p5v9j3vqf7cd91w3ryzrpla6fwwlh4j0ak55a2j1b5")))

(define-public crate-wolffserial-0.2.1 (c (n "wolffserial") (v "0.2.1") (d (list (d (n "clap") (r "^3") (d #t) (k 0)) (d (n "serialport") (r "^4") (d #t) (k 0)))) (h "0y2hnki8l5ppw9ysvrx1bpifym2vj90iva5iww04hg7ydfrnx8v5")))

