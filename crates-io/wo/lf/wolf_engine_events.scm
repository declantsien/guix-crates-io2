(define-module (crates-io wo lf wolf_engine_events) #:use-module (crates-io))

(define-public crate-wolf_engine_events-0.1.0 (c (n "wolf_engine_events") (v "0.1.0") (d (list (d (n "downcast-rs") (r "^1.2.0") (o #t) (d #t) (k 0)) (d (n "ntest") (r "^0.9.0") (d #t) (k 2)) (d (n "test-case") (r "^3.3.1") (d #t) (k 2)) (d (n "wolf_engine_codegen") (r "^0.1.0") (d #t) (k 0)))) (h "13mbss4xprlchkqdqhi25fwpswwjnp5mxhp2rmfa1vk5hq8x3d0n") (f (quote (("dynamic" "downcast-rs") ("default" "dynamic"))))))

