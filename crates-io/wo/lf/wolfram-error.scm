(define-module (crates-io wo lf wolfram-error) #:use-module (crates-io))

(define-public crate-wolfram-error-0.0.0 (c (n "wolfram-error") (v "0.0.0") (d (list (d (n "diagnostic") (r "0.5.*") (d #t) (k 0)) (d (n "validatus") (r "0.0.*") (d #t) (k 0)))) (h "1rwrnyx0pbkn3j9hab1w8sdvs4ljp003wwbqq0z60xbb8isxqrq3")))

