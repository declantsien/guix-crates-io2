(define-module (crates-io wo lf wolfssl) #:use-module (crates-io))

(define-public crate-wolfssl-0.0.1 (c (n "wolfssl") (v "0.0.1") (h "0lawpdp0b77lyjqck5zp3mph5c43fba8yzqdpv4fqvkiqx5ig5j5")))

(define-public crate-wolfssl-1.0.0 (c (n "wolfssl") (v "1.0.0") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 2)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "env_logger") (r "^0.11.0") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "test-case") (r "^3.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.31.0") (f (quote ("rt" "net" "macros"))) (d #t) (k 2)) (d (n "wolfssl-sys") (r "^1.0.0") (d #t) (k 0)))) (h "0469hqd29g2dkhf1qjfgvy1cc2wwlm0rvdn4rfwgk4pjz31blj5d") (f (quote (("postquantum" "wolfssl-sys/postquantum") ("default" "postquantum") ("debug" "wolfssl-sys/debug"))))))

