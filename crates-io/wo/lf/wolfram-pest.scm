(define-module (crates-io wo lf wolfram-pest) #:use-module (crates-io))

(define-public crate-wolfram-pest-0.1.0 (c (n "wolfram-pest") (v "0.1.0") (d (list (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_generator") (r "^2.1") (d #t) (k 2)) (d (n "quote") (r "^1.0") (d #t) (k 2)))) (h "0gc4za4f7jhflczbrbk31nmvzf98dvcf43zc5wflv7sybbmyjxxq")))

(define-public crate-wolfram-pest-0.2.0 (c (n "wolfram-pest") (v "0.2.0") (d (list (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_generator") (r "^2.1") (d #t) (k 2)) (d (n "quote") (r "^1.0") (d #t) (k 2)))) (h "0abhl7ka079cac6db3c0xjxfsgwq8krvygqy3d9y0mlxvpscd21b")))

