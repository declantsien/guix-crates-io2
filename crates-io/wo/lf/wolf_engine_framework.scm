(define-module (crates-io wo lf wolf_engine_framework) #:use-module (crates-io))

(define-public crate-wolf_engine_framework-0.24.1 (c (n "wolf_engine_framework") (v "0.24.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mockall") (r "^0.11") (d #t) (k 2)) (d (n "ntest") (r "^0.8") (d #t) (k 2)) (d (n "test-case") (r "^1.2") (d #t) (k 2)) (d (n "wolf_engine_core") (r "^0.24.1") (d #t) (k 0)))) (h "1680xc66wc6x1c7q46npy5pdy84yhgpvg103cbsk4dzph30ivjwk")))

(define-public crate-wolf_engine_framework-0.25.0 (c (n "wolf_engine_framework") (v "0.25.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mockall") (r "^0.11") (d #t) (k 2)) (d (n "ntest") (r "^0.8") (d #t) (k 2)) (d (n "test-case") (r "^1.2") (d #t) (k 2)) (d (n "wolf_engine_core") (r "^0.25.0") (d #t) (k 0)))) (h "00ssc0wc6j3ryazcbxvd7lg0af8h5z381b5galm6p4iaf531k5iv")))

