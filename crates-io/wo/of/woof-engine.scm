(define-module (crates-io wo of woof-engine) #:use-module (crates-io))

(define-public crate-woof-engine-0.0.1 (c (n "woof-engine") (v "0.0.1") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "macro_rules_attribute") (r "^0.1.3") (d #t) (k 2)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)))) (h "1qsjw8339n33vagavqb2vaskiyf8shhy6qvlhqlvygvi505ka7q5")))

