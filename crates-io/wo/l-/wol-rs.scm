(define-module (crates-io wo l- wol-rs) #:use-module (crates-io))

(define-public crate-wol-rs-0.0.1 (c (n "wol-rs") (v "0.0.1") (d (list (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)))) (h "1il83plwj95cnhyql6nisxn5yy2l6jzd0wcs150552mshv1bsr1a")))

(define-public crate-wol-rs-0.0.2 (c (n "wol-rs") (v "0.0.2") (d (list (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)))) (h "0sbz7v4lyi1z5fssnlapa86x730zh33khsb54s3z7b3hqrfgwz0y")))

(define-public crate-wol-rs-0.9.0 (c (n "wol-rs") (v "0.9.0") (d (list (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)))) (h "0zigf49sqfl5kf2yf92fjarsimdqrq10clnahvf3nk56yv091476")))

(define-public crate-wol-rs-0.9.1 (c (n "wol-rs") (v "0.9.1") (d (list (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)))) (h "1wp19ibfz396n0xym3rnm1526bhkax8c4wi4n37nq9cbn9lpxyfp")))

(define-public crate-wol-rs-1.0.0 (c (n "wol-rs") (v "1.0.0") (d (list (d (n "clap") (r "^3") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0rd7hhp98hwfhlmzks1z6abcd7pmigfhqdwda4aib8rldr45xp28") (s 2) (e (quote (("bin" "dep:clap"))))))

(define-public crate-wol-rs-1.0.1 (c (n "wol-rs") (v "1.0.1") (d (list (d (n "clap") (r "^3") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "13qbhw0300fwgxj4zc29mj9jcazdb0cpcijrign0icpr7q1qlniw") (s 2) (e (quote (("bin" "dep:clap"))))))

