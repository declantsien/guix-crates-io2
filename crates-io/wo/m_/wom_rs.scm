(define-module (crates-io wo m_ wom_rs) #:use-module (crates-io))

(define-public crate-wom_rs-0.1.0 (c (n "wom_rs") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "chrono") (r "^0.4.33") (f (quote ("serde"))) (d #t) (k 0)) (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "httpmock") (r "^0.7.0") (d #t) (k 2)) (d (n "reqwest") (r "^0.11.20") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.113") (d #t) (k 0)) (d (n "strum") (r "^0.26.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (d #t) (k 2)))) (h "1bh574k1fc16s2scmwd0hj8kdsni6wkk8sjq2pdmc5x50yxb5mhr")))

