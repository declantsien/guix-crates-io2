(define-module (crates-io wo t- wot-consume) #:use-module (crates-io))

(define-public crate-wot-consume-0.1.0 (c (n "wot-consume") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11.11") (d #t) (k 0)) (d (n "tokio") (r "^1.20.1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "wot-td") (r "^0.1") (d #t) (k 0)))) (h "1cr7d48wsar4fzggpix2rdsw2mn18jmyz8qlzinyn6zcr3kd6mai")))

