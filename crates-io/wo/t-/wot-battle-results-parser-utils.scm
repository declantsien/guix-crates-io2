(define-module (crates-io wo t- wot-battle-results-parser-utils) #:use-module (crates-io))

(define-public crate-wot-battle-results-parser-utils-0.1.0 (c (n "wot-battle-results-parser-utils") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.52") (d #t) (k 0)) (d (n "miniz_oxide") (r "^0.6.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-pickle") (r "^1.1.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "1d2bvykql1m9q0rvvpmah5c845zvqx21l26prjd9lkzgbfy0ycd9")))

(define-public crate-wot-battle-results-parser-utils-0.2.0 (c (n "wot-battle-results-parser-utils") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.52") (d #t) (k 0)) (d (n "miniz_oxide") (r "^0.6.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-pickle") (r "^1.1.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "06g2vx414r7lmfgpnhn0zrmx4x5yh6c5vln9z0y5ilkpim1cgmjm")))

