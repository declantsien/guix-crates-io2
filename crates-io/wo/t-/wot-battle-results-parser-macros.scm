(define-module (crates-io wo t- wot-battle-results-parser-macros) #:use-module (crates-io))

(define-public crate-wot-battle-results-parser-macros-0.1.0 (c (n "wot-battle-results-parser-macros") (v "0.1.0") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.93") (f (quote ("full"))) (d #t) (k 0)))) (h "1i7nx0jfj295bbixxvvra45nvhxwcb04difdlhqr07xqml4d0n9x")))

