(define-module (crates-io wo rd wordsworth) #:use-module (crates-io))

(define-public crate-wordsworth-0.1.0 (c (n "wordsworth") (v "0.1.0") (h "1d70k8jyx4zn35c2l7x9s4v4xjck50zlxriwl3h7a0cm36givsqn")))

(define-public crate-wordsworth-0.1.1 (c (n "wordsworth") (v "0.1.1") (h "0rc917mv9f8h7i4dwzgxvybnr0cli5nba2sxvm28qgrbq7rlvhz6")))

