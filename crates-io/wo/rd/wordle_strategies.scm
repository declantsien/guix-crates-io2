(define-module (crates-io wo rd wordle_strategies) #:use-module (crates-io))

(define-public crate-wordle_strategies-0.1.0 (c (n "wordle_strategies") (v "0.1.0") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "wordle_rs") (r "^0.1") (d #t) (k 0)))) (h "0hyy5lcdclxi3vjbzs2dk2vpjq5j06c0mrwv1nsfhrwn4c1b8s91") (r "1.58")))

(define-public crate-wordle_strategies-0.1.1 (c (n "wordle_strategies") (v "0.1.1") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "wordle_rs") (r "^0.1") (d #t) (k 0)))) (h "09wpwhcvlp600yl2bz2qp5irb0jh6v3pqmk7cba7ql3x4vzcfpnc") (r "1.58")))

(define-public crate-wordle_strategies-0.1.2 (c (n "wordle_strategies") (v "0.1.2") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "wordle_rs") (r "^0.1") (d #t) (k 0)))) (h "0j57llqldg2w84g6wpf3dmmv4qs0aydli5kv0vm2sp0kw18d374q") (r "1.58")))

(define-public crate-wordle_strategies-0.2.0 (c (n "wordle_strategies") (v "0.2.0") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "wordle_rs") (r "^0.2") (d #t) (k 0)))) (h "01vii70fq52gx1vcadsy4gzjh4z29afxjvvqz0k3d6b0hqh56rzf") (r "1.58")))

