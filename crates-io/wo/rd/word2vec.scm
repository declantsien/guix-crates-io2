(define-module (crates-io wo rd word2vec) #:use-module (crates-io))

(define-public crate-word2vec-0.1.0 (c (n "word2vec") (v "0.1.0") (h "0mzrf6wcsp8rig9ysrvhn9z05dpy81937kazzx5k0w0jbrwpzxnv")))

(define-public crate-word2vec-0.2.0 (c (n "word2vec") (v "0.2.0") (h "1zar023a16zkxf1ajf84fr18cs0jbcssl39rgbv5w79did2x51mx")))

(define-public crate-word2vec-0.2.1 (c (n "word2vec") (v "0.2.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)))) (h "0lvmvzga48857wvfwl3pn9czl99ysmk2d2ns0dxykhyamm5hpxcj")))

(define-public crate-word2vec-0.3.0 (c (n "word2vec") (v "0.3.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)))) (h "1ldxdwpkcv9qyladwc67dkwbaxdfcjgkpb51j6c47qwy13h80lyw")))

(define-public crate-word2vec-0.3.1 (c (n "word2vec") (v "0.3.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)))) (h "0rpwbal7dhycx36hw2gpkvg4mwgqnljy7k5zczxb656wyy973wc5")))

(define-public crate-word2vec-0.3.2 (c (n "word2vec") (v "0.3.2") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)))) (h "04pxk7i798iigc95qxlb6q41dff8yj6wxhhhg8zw8f0mx5x02qjj")))

(define-public crate-word2vec-0.3.3 (c (n "word2vec") (v "0.3.3") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)))) (h "0k0g8hp1s2wayl4pdwhm39z8w0f78lwmjm98ihy1ilnd80wa8lyz")))

