(define-module (crates-io wo rd wordmarkov) #:use-module (crates-io))

(define-public crate-wordmarkov-0.1.0 (c (n "wordmarkov") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "05mssn5c91ncxpak4map7887bmwjsnkmaafajbzniw70b4a5qfgi")))

(define-public crate-wordmarkov-0.1.0-r1 (c (n "wordmarkov") (v "0.1.0-r1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "12dbsqncfff3yrkhcfv3mswdi221fc3a6j2fvc6pvbvg1wn484xm")))

(define-public crate-wordmarkov-0.1.1 (c (n "wordmarkov") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1fr56mp7qpakk5yhdwrfmwrr7jkdp2ig215g30s5vw8y9lyl49zv")))

(define-public crate-wordmarkov-0.1.2 (c (n "wordmarkov") (v "0.1.2") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0a473nsislzc29m3vbccg63c6qg3aascm9v7202ah2r44rqwaqfl")))

(define-public crate-wordmarkov-0.1.3 (c (n "wordmarkov") (v "0.1.3") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "15nad1srqkaa81swyhqarxcmkymfdy7xglfw0w729nfi97f7k46y")))

(define-public crate-wordmarkov-0.1.4 (c (n "wordmarkov") (v "0.1.4") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "141wb67l93sf8awx2nnh778r9i2spkscbjypa7dy74xmi9ws4z03")))

