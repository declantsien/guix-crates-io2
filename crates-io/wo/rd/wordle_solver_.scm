(define-module (crates-io wo rd wordle_solver_) #:use-module (crates-io))

(define-public crate-wordle_solver_-0.1.0 (c (n "wordle_solver_") (v "0.1.0") (h "1hkg2rabljj15v19855j5r101if85n786q55inasfr4b58yxgnzm")))

(define-public crate-wordle_solver_-1.0.0 (c (n "wordle_solver_") (v "1.0.0") (h "1x64gkdgrfcb7na1j1xmxdyc8xb6kzkg96fhvp6psahl515g6wm4")))

(define-public crate-wordle_solver_-1.0.5 (c (n "wordle_solver_") (v "1.0.5") (h "19r5n9kkv7z7dcqx29li8qs40vzkggkjilqv7x99y6920f4b5ql6")))

(define-public crate-wordle_solver_-2.0.0 (c (n "wordle_solver_") (v "2.0.0") (h "1rhzmag7d9g6bwifwgp1mpk3hn6d104i6vd4ik0r44yyr526kcq5")))

