(define-module (crates-io wo rd wordrs) #:use-module (crates-io))

(define-public crate-wordrs-0.1.0 (c (n "wordrs") (v "0.1.0") (h "02sivw2ib13dlh0vdi4bzdzhik9il01am89vixjvrwqhr1lxw5f0")))

(define-public crate-wordrs-0.1.1 (c (n "wordrs") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "hyper") (r "^0.12.33") (d #t) (k 0)) (d (n "serde") (r "^1.0.97") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.97") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)))) (h "0293lglbidji0wgyn8sdfr3fgjy11158di62g395xgdmn9n97svb")))

