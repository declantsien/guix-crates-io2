(define-module (crates-io wo rd wordnet) #:use-module (crates-io))

(define-public crate-wordnet-0.1.0 (c (n "wordnet") (v "0.1.0") (h "0j4a5p2qpbwrl5lac6mgk77hsh2l7pmdgl3v5lhn768wnyamg6c8")))

(define-public crate-wordnet-0.1.1 (c (n "wordnet") (v "0.1.1") (h "0xml35jvysnzqmpnwm7a21rdxndhzf163bkwvrib7f9y3ccgcsdk")))

(define-public crate-wordnet-0.1.2 (c (n "wordnet") (v "0.1.2") (h "009w0cbc3kb4mndglk1izf4h6c3l07cy6sx2lr6zggghc60xk399")))

