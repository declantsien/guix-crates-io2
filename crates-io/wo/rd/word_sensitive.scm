(define-module (crates-io wo rd word_sensitive) #:use-module (crates-io))

(define-public crate-word_sensitive-0.1.0 (c (n "word_sensitive") (v "0.1.0") (h "0ghhz5m1hc6f0gqnw0dckbj2ky4szpxij85j5vqk6bbs9p2zkx05")))

(define-public crate-word_sensitive-0.1.2 (c (n "word_sensitive") (v "0.1.2") (h "0zfyxx4plfrn2ajbbgh5f16xcrqv9b5dwndqs643v6vw48qi5yxf")))

