(define-module (crates-io wo rd wordler) #:use-module (crates-io))

(define-public crate-wordler-0.2.0 (c (n "wordler") (v "0.2.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.53") (d #t) (k 0)) (d (n "indexmap") (r "^1.8.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)))) (h "02rzlg41fvffhgwvqaxvs6cd1f40hn1xycrx0s6s8lhksvr2zan9")))

(define-public crate-wordler-0.2.1 (c (n "wordler") (v "0.2.1") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.53") (d #t) (k 0)) (d (n "indexmap") (r "^1.8.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)))) (h "0i8n13iv0mczdlnf349xwnd7jzab1l8yc7433jjw20sms3clcsyv")))

(define-public crate-wordler-0.2.2 (c (n "wordler") (v "0.2.2") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.53") (d #t) (k 0)) (d (n "indexmap") (r "^1.8.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)))) (h "1xy65lv3fp6l97jb9r3rm3sgxzmpllfxb6khqlv34y4jc4g4alwm")))

(define-public crate-wordler-0.2.3 (c (n "wordler") (v "0.2.3") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.53") (d #t) (k 0)) (d (n "indexmap") (r "^1.8.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)))) (h "0bn43ys78jixbxzyrxl5r8jvjfbq3922bmhas9hjypkss8x9chjw")))

(define-public crate-wordler-0.2.4 (c (n "wordler") (v "0.2.4") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.53") (d #t) (k 0)) (d (n "indexmap") (r "^1.8.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)))) (h "0d5fivznarhacb680f225m4nvjvh2rbscywc06x389fmgy2k1qra")))

(define-public crate-wordler-0.3.0 (c (n "wordler") (v "0.3.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.53") (d #t) (k 0)) (d (n "indexmap") (r "^1.8.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)))) (h "0czggmbrz1r7sj15g9xg13sgyr9m7bh34dk4a1mq1gyhl8a4rbgv")))

