(define-module (crates-io wo rd word_generator) #:use-module (crates-io))

(define-public crate-word_generator-0.1.0 (c (n "word_generator") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "0lgvva445v24p7wgy68q5khvscdz0s351pz3z2yck0a2c52xgfah")))

(define-public crate-word_generator-0.1.1 (c (n "word_generator") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "0xqjdjjxbnn5srzp25j5v3dzjylps9f4dqbkgn7nbdnal0apkiqk")))

