(define-module (crates-io wo rd wordnet-lmf) #:use-module (crates-io))

(define-public crate-wordnet-lmf-0.1.0 (c (n "wordnet-lmf") (v "0.1.0") (d (list (d (n "expect-test") (r "^1.1") (d #t) (k 2)) (d (n "quick-xml") (r "^0.22") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0g76srg7sc58ab1wy5iifv8yv1faww3a07hkcz7q8wgr1llclzq4")))

