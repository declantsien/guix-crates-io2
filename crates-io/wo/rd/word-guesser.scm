(define-module (crates-io wo rd word-guesser) #:use-module (crates-io))

(define-public crate-word-guesser-0.1.0 (c (n "word-guesser") (v "0.1.0") (d (list (d (n "console") (r "^0.13.0") (d #t) (k 0)) (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3.20") (d #t) (k 0)))) (h "1d25d91ly4r9ixfgghkgzi8mqb6crl41qqz4ghpcnnxx59av3xc9")))

(define-public crate-word-guesser-0.1.1 (c (n "word-guesser") (v "0.1.1") (d (list (d (n "console") (r "^0.13.0") (d #t) (k 0)) (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3.20") (d #t) (k 0)))) (h "1i10kcjxcqfi3hvnf4lpcasi89clk5raimmg3p8wk59nha2qxlk9")))

