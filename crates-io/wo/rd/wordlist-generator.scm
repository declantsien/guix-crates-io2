(define-module (crates-io wo rd wordlist-generator) #:use-module (crates-io))

(define-public crate-wordlist-generator-0.1.0 (c (n "wordlist-generator") (v "0.1.0") (d (list (d (n "clap") (r "^2.24") (d #t) (k 0)) (d (n "itertools") (r "^0.6") (d #t) (k 0)) (d (n "permutohedron") (r "^0.2") (d #t) (k 0)))) (h "01rllfd7n4bkj33r9q0aazhd4q8incrl3zb2mi3z4pj9r38g93ij")))

(define-public crate-wordlist-generator-0.1.1 (c (n "wordlist-generator") (v "0.1.1") (d (list (d (n "clap") (r "^2.24") (d #t) (k 0)) (d (n "itertools") (r "^0.6") (d #t) (k 0)) (d (n "permutohedron") (r "^0.2") (d #t) (k 0)))) (h "1vza5wkvz7xnc4slbfqa6vmcgp3ngw4wyhhicllr6f30mxh7bfwf")))

