(define-module (crates-io wo rd word-segmenters) #:use-module (crates-io))

(define-public crate-word-segmenters-0.1.0 (c (n "word-segmenters") (v "0.1.0") (d (list (d (n "ahash") (r ">=0.3.5, <0.4.0") (d #t) (k 0)) (d (n "bencher") (r ">=0.1.5, <0.2.0") (d #t) (k 2)) (d (n "err-derive") (r ">=0.2.4, <0.3.0") (d #t) (k 0)) (d (n "once_cell") (r ">=1.4.0, <2.0.0") (d #t) (k 2)))) (h "0w414g8lk3lsyklas7q76w73xppr1rxiwh7nbzjr9zfys5fr3vpc")))

(define-public crate-word-segmenters-0.1.1 (c (n "word-segmenters") (v "0.1.1") (d (list (d (n "ahash") (r ">=0.5.7, <0.6.0") (d #t) (k 0)) (d (n "bencher") (r ">=0.1.5, <0.2.0") (d #t) (k 2)) (d (n "once_cell") (r ">=1.4.0, <2.0.0") (d #t) (k 2)) (d (n "thiserror") (r ">=1.0.22, <2.0.0") (d #t) (k 0)))) (h "0cq14j0nq0dcf3g82rpbc0wab1g5vbfmclyvjxlxfpqmy36hb1zm")))

(define-public crate-word-segmenters-0.2.0 (c (n "word-segmenters") (v "0.2.0") (d (list (d (n "ahash") (r ">=0.5.7, <0.6.0") (d #t) (k 0)) (d (n "bencher") (r ">=0.1.5, <0.2.0") (d #t) (k 2)) (d (n "once_cell") (r ">=1.4.0, <2.0.0") (d #t) (k 2)) (d (n "smartstring") (r ">=0.2.5, <0.3.0") (d #t) (k 0)) (d (n "thiserror") (r ">=1.0.22, <2.0.0") (d #t) (k 0)))) (h "0s6v7rzpwk5xhb4rya2c56r66h9hm5fm5p3kf0578lfgbx6qjsyh")))

(define-public crate-word-segmenters-0.2.1 (c (n "word-segmenters") (v "0.2.1") (d (list (d (n "ahash") (r ">=0.6.1, <0.7.0") (d #t) (k 0)) (d (n "bencher") (r ">=0.1.5, <0.2.0") (d #t) (k 2)) (d (n "once_cell") (r ">=1.4.0, <2.0.0") (d #t) (k 2)) (d (n "smartstring") (r ">=0.2.5, <0.3.0") (d #t) (k 0)) (d (n "thiserror") (r ">=1.0.22, <2.0.0") (d #t) (k 0)))) (h "0y7zxngwcz4g2b2g1xd5vyfaz02650k19ivnm92i9pgj9a6iy1vc") (f (quote (("__test_data"))))))

(define-public crate-word-segmenters-0.3.0 (c (n "word-segmenters") (v "0.3.0") (d (list (d (n "ahash") (r "^0.6.1") (d #t) (k 0)) (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "once_cell") (r "^1.4") (d #t) (k 2)) (d (n "smartstring") (r "^0.2.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "1yfv7nzdjf9qm6b2qhi8sx87nl6kb7k16zmfx7514n7xljsw9lyj") (f (quote (("__test_data"))))))

(define-public crate-word-segmenters-0.3.2 (c (n "word-segmenters") (v "0.3.2") (d (list (d (n "ahash") (r "^0.6.1") (d #t) (k 0)) (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "once_cell") (r "^1.4") (d #t) (k 2)) (d (n "smartstring") (r "^0.2.5") (d #t) (k 0)))) (h "15br1si14yzyxi4g8nmr5kg31a5hymx53y7i29asw3aavln194sw") (f (quote (("__test_data"))))))

