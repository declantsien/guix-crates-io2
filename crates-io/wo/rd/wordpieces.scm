(define-module (crates-io wo rd wordpieces) #:use-module (crates-io))

(define-public crate-wordpieces-0.1.0 (c (n "wordpieces") (v "0.1.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "fst") (r "^0.3") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)))) (h "1fnw6xfkq8l7vlpf2rr8b3lm55wc3i6zgic6lssn54kxw72wn18j")))

(define-public crate-wordpieces-0.1.1 (c (n "wordpieces") (v "0.1.1") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "fst") (r "^0.3") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)))) (h "1khn1i3plmbhjs80xp0aqkdnawas3x44lhzv6cf3i5gjfjz0p6ci")))

(define-public crate-wordpieces-0.2.0 (c (n "wordpieces") (v "0.2.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "fst") (r "^0.3") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)))) (h "07jf70sawf3baxk5n9ka4ddnzg1k0rfrdk3cayk0588bl18x92km")))

(define-public crate-wordpieces-0.2.1 (c (n "wordpieces") (v "0.2.1") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "fst") (r "^0.3") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)))) (h "0hglvqhb8kw8198wf4jzcnbhi2mc6ppwyr6b5y6gy0vgkyk8r6c1")))

(define-public crate-wordpieces-0.3.0 (c (n "wordpieces") (v "0.3.0") (d (list (d (n "fst") (r "^0.3") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0bq379aj1c1xjf61slxdq4iyqlhyim3fnp19i75dcl05i3wf4v9g")))

(define-public crate-wordpieces-0.4.0 (c (n "wordpieces") (v "0.4.0") (d (list (d (n "fst") (r "^0.4") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0pzz6rvn3abpfhj98brrhh95s8x1bvkrfsmd3w8w9ffmpd2bxims")))

(define-public crate-wordpieces-0.4.1 (c (n "wordpieces") (v "0.4.1") (d (list (d (n "fst") (r "^0.4") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "054gf84iap06an93npr3ga43qbwyqg3f185lx7jmi0vgl2qk3ki4")))

(define-public crate-wordpieces-0.5.0 (c (n "wordpieces") (v "0.5.0") (d (list (d (n "fst") (r "^0.4") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "17kv4k05b5815p1l66cqn7b583gpnjmjxpfvvgxf1dafh47bb3xj")))

(define-public crate-wordpieces-0.6.0 (c (n "wordpieces") (v "0.6.0") (d (list (d (n "fst") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "17bfr9dg63lca1pa7izny3hnii7y00mj0p160yydnrd875yfm1kr") (y #t)))

(define-public crate-wordpieces-0.6.1 (c (n "wordpieces") (v "0.6.1") (d (list (d (n "fst") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1wdbwqxim70grxf4rp7xlbpangvdx4j1dh66c4bwk35vvsnj6fg2")))

