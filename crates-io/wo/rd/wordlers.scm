(define-module (crates-io wo rd wordlers) #:use-module (crates-io))

(define-public crate-wordlers-0.1.0 (c (n "wordlers") (v "0.1.0") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)))) (h "153x0dm5hf325xxkfghn3lwl6wn8iyfwjz6zsc689fm2sgz8i2a5")))

(define-public crate-wordlers-0.1.1 (c (n "wordlers") (v "0.1.1") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)))) (h "0xshylpmsipkgw58d5gpxa7rbfa1ikj8zlkixlzp8xmc2ki2alk8")))

(define-public crate-wordlers-0.2.1 (c (n "wordlers") (v "0.2.1") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)))) (h "0daigvsnj0mkn4k95gr7pg71simhk5jxc44nfy6pqxmdfi10vzz4")))

(define-public crate-wordlers-0.2.2 (c (n "wordlers") (v "0.2.2") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)))) (h "0jhivipy3wjx1r377gak2df80ml93lkjn3pafjhf5ihddkzgcryj")))

