(define-module (crates-io wo rd wordle-automaton) #:use-module (crates-io))

(define-public crate-wordle-automaton-0.2.0 (c (n "wordle-automaton") (v "0.2.0") (d (list (d (n "fst") (r "^0.4.7") (d #t) (k 0)))) (h "1wl13dmrbhasv4y3zrq6096n0yc9d28yd1f22ggrg8379l3g1jpi")))

(define-public crate-wordle-automaton-0.3.0 (c (n "wordle-automaton") (v "0.3.0") (d (list (d (n "fst") (r "^0.4.7") (d #t) (k 0)) (d (n "smallvec") (r "^1.7.0") (f (quote ("const_generics" "const_new" "union"))) (d #t) (k 0)))) (h "0f3752l3i92hq0k4xd7qdmy3sy3il7pzaqxj7d9iympzyfym0af4")))

(define-public crate-wordle-automaton-0.4.0 (c (n "wordle-automaton") (v "0.4.0") (d (list (d (n "fst") (r "^0.4.7") (d #t) (k 0)) (d (n "smallvec") (r "^1.7.0") (f (quote ("const_generics" "const_new" "union"))) (d #t) (k 0)))) (h "1276ihlwbki00xyh6v04999bvnkkyqp3gwzd4cjiv16g1x3zh86a")))

(define-public crate-wordle-automaton-0.5.0 (c (n "wordle-automaton") (v "0.5.0") (d (list (d (n "fst") (r "^0.4.7") (d #t) (k 0)) (d (n "smallvec") (r "^1.7.0") (f (quote ("const_generics" "const_new" "union"))) (d #t) (k 0)))) (h "1ibias5qxg5lqpqnpf2mq2vwf674i6kdicrsd6ycpvmyd40zqpfp")))

(define-public crate-wordle-automaton-0.6.0 (c (n "wordle-automaton") (v "0.6.0") (d (list (d (n "fst") (r "^0.4.7") (d #t) (k 0)) (d (n "smallvec") (r "^1.7.0") (f (quote ("const_generics" "const_new" "union"))) (d #t) (k 0)))) (h "0w211n6pznqg0979qvy1kqnpvmkqk1f9xj45x3bmmx21mhpr6bjr")))

(define-public crate-wordle-automaton-0.7.0 (c (n "wordle-automaton") (v "0.7.0") (d (list (d (n "fst") (r "^0.4.7") (d #t) (k 0)) (d (n "smallvec") (r "^1.7.0") (f (quote ("const_generics" "const_new" "union"))) (d #t) (k 0)))) (h "0byhh3dvm9wp61mh54cybhppzg8i45nlyanv9s2yi8mfi04aj1gs")))

(define-public crate-wordle-automaton-0.8.0 (c (n "wordle-automaton") (v "0.8.0") (d (list (d (n "fst") (r "^0.4.7") (d #t) (k 0)) (d (n "smallvec") (r "^1.7.0") (f (quote ("const_generics" "const_new" "union"))) (d #t) (k 0)))) (h "1b4dbmbvhs5m8nqv2bkxi83xgy50jsqp24i5zk00bg776c2b2lll")))

(define-public crate-wordle-automaton-0.9.0 (c (n "wordle-automaton") (v "0.9.0") (d (list (d (n "fst") (r "^0.4.7") (d #t) (k 0)) (d (n "smallvec") (r "^1.7.0") (f (quote ("const_generics" "const_new" "union"))) (d #t) (k 0)))) (h "11mmjh8xyzk43n5x3p2rnw91fvc6z1fhid5j27ba2fxhjrj78pwj")))

