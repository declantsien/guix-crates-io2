(define-module (crates-io wo rd wordcrab) #:use-module (crates-io))

(define-public crate-wordcrab-0.1.0 (c (n "wordcrab") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.3") (d #t) (k 0)))) (h "1yqbnblpiwvlrk26kpjpqpjinmq0x6mdpnikfk4b89pvxpabi296")))

(define-public crate-wordcrab-0.2.0 (c (n "wordcrab") (v "0.2.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.101") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)) (d (n "structopt") (r "^0.3.3") (d #t) (k 0)))) (h "01vd1q85zl1axdl9fisk55w2rky31xhncgb1mjcrl3p4y318bsgb")))

(define-public crate-wordcrab-0.2.1 (c (n "wordcrab") (v "0.2.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.101") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)) (d (n "structopt") (r "^0.3.3") (d #t) (k 0)))) (h "1ldg66zs91gswzqnhixcj7zc8fwqm29zkc2dh3nr5kj754jqrhil")))

(define-public crate-wordcrab-0.3.0 (c (n "wordcrab") (v "0.3.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.101") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)) (d (n "structopt") (r "^0.3.3") (d #t) (k 0)))) (h "1ls686xwaa0blzmg6045y3lmsfwda6gpfls6pwp7lp0ij6yn5fry")))

(define-public crate-wordcrab-0.4.0 (c (n "wordcrab") (v "0.4.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.101") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.101") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)) (d (n "structopt") (r "^0.3.3") (d #t) (k 0)))) (h "1fq1ihils1nkviipv9sph5byb7ymzv28h681il98wdyqd0ypb92h")))

(define-public crate-wordcrab-0.5.0 (c (n "wordcrab") (v "0.5.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "rayon") (r "^1.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.101") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.101") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)) (d (n "structopt") (r "^0.3.3") (d #t) (k 0)))) (h "0i59b1l1sycsb098dnaa053b34jfk4y4y9v52xmf6qxxd3yv9v1c")))

(define-public crate-wordcrab-0.6.0 (c (n "wordcrab") (v "0.6.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "rayon") (r "^1.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.101") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.101") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)) (d (n "structopt") (r "^0.3.3") (d #t) (k 0)))) (h "0k5f316qy2v2n236llkr78a1ypwal8k4ihw8hdnx7vv053xgk0v5")))

(define-public crate-wordcrab-0.7.0 (c (n "wordcrab") (v "0.7.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "rayon") (r "^1.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.101") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.101") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.11") (d #t) (k 0)) (d (n "structopt") (r "^0.3.3") (d #t) (k 0)))) (h "1v5khmai0qbgdhnxwc6dnd71pxs91hypz6s54qnv3nc7s9pll9zh")))

(define-public crate-wordcrab-0.7.1 (c (n "wordcrab") (v "0.7.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "rayon") (r "^1.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.101") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.101") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.11") (d #t) (k 0)) (d (n "structopt") (r "^0.3.3") (d #t) (k 0)))) (h "0w7xpqm05chdbk0l08symk0cfb4xdqqiybn1zvfbpzsaa89gm08g")))

(define-public crate-wordcrab-0.8.0 (c (n "wordcrab") (v "0.8.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "rayon") (r "^1.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.101") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.101") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.11") (d #t) (k 0)) (d (n "structopt") (r "^0.3.3") (d #t) (k 0)))) (h "0sz4jihwvnkfc2avxmd4apqhyimg7050qs585zv7xg76l97s8qyl")))

(define-public crate-wordcrab-0.9.0 (c (n "wordcrab") (v "0.9.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "rayon") (r "^1.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.101") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.101") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.11") (d #t) (k 0)) (d (n "structopt") (r "^0.3.3") (d #t) (k 0)))) (h "1n5wamhrp8fd7z490r7pjj108r1lphz0r3x7fv2jwdamx59jhal1")))

(define-public crate-wordcrab-0.10.0 (c (n "wordcrab") (v "0.10.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "rayon") (r "^1.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.101") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.101") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.11") (d #t) (k 0)) (d (n "structopt") (r "^0.3.3") (d #t) (k 0)))) (h "1m1kkcsd7ljkhhsdrd83jjz9hl1ajflnmsfj1bbf3ndqlfiqbc9y")))

(define-public crate-wordcrab-0.10.1 (c (n "wordcrab") (v "0.10.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "rayon") (r "^1.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.101") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.101") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.11") (d #t) (k 0)) (d (n "structopt") (r "^0.3.3") (d #t) (k 0)))) (h "15q2vm171225dq55b63v89w0fl4mfmsing52xa3rrsm1yxqp5s10")))

(define-public crate-wordcrab-0.10.2 (c (n "wordcrab") (v "0.10.2") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "rayon") (r "^1.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.101") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.101") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.11") (d #t) (k 0)) (d (n "structopt") (r "^0.3.3") (d #t) (k 0)))) (h "0lrk626jp3c0jvqr89ddhlm5j4hmrw44bbfx7rbj71p0mlnnncq7")))

(define-public crate-wordcrab-0.11.0 (c (n "wordcrab") (v "0.11.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "rayon") (r "^1.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.104") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.46") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.11") (d #t) (k 0)) (d (n "structopt") (r "^0.3.8") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)))) (h "1j77b6iqsyp0mm3k5gg0y05bhvfq332gvvg8zlam6n44n06as2sx")))

(define-public crate-wordcrab-0.11.1 (c (n "wordcrab") (v "0.11.1") (d (list (d (n "clap") (r "^3.0.0-beta.1") (d #t) (k 0)) (d (n "rayon") (r "^1.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.113") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.113") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.55") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.13") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)))) (h "0nnf989nidrzllwy4v5phbikaragmx1zvb6xyr8njhwx6pahqds8")))

(define-public crate-wordcrab-0.12.0 (c (n "wordcrab") (v "0.12.0") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "rayon") (r "^1.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.122") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.122") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.15") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "00cljq63ccg7a34hcqlxxgjx42zxxnn5zdrsncpjzadj9ngygf89")))

(define-public crate-wordcrab-0.13.0 (c (n "wordcrab") (v "0.13.0") (d (list (d (n "clap") (r "^4.3.4") (f (quote ("default" "derive" "color"))) (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "toml") (r "^0.5.11") (d #t) (k 0)))) (h "1fj84rblls7q3cxjjy0kjl444zrwidgjzfzkv6nmimb11rpqm7wn")))

