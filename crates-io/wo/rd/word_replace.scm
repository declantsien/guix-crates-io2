(define-module (crates-io wo rd word_replace) #:use-module (crates-io))

(define-public crate-word_replace-0.0.1 (c (n "word_replace") (v "0.0.1") (d (list (d (n "clap") (r "^2.26") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "1y5qjcnfxrvs99kxidydz5xdxzil9320r7iwwv5y56fghzyz28gb")))

(define-public crate-word_replace-0.0.2 (c (n "word_replace") (v "0.0.2") (d (list (d (n "clap") (r "^2.26") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "18xpkvxd0p1alyq4d8wv1hj1gkz3xm58clj4ipwbwwaxxsaw6b31")))

(define-public crate-word_replace-0.0.3 (c (n "word_replace") (v "0.0.3") (d (list (d (n "clap") (r "^2.26") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "0yaai38bgv6559kw3kq3906097jvrkl8jabsmsn2zhwbvn435ilp")))

