(define-module (crates-io wo rd wordfeud-ocr) #:use-module (crates-io))

(define-public crate-wordfeud-ocr-0.1.0 (c (n "wordfeud-ocr") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "image") (r "^0.23") (f (quote ("png" "pnm"))) (k 0)) (d (n "imageproc") (r "^0.22") (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1cmhaqz64h8f4xnz1wsnlswnald0kpkg080rm9nw4lrcxsf94r56")))

(define-public crate-wordfeud-ocr-0.1.1 (c (n "wordfeud-ocr") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "image") (r "^0.23") (f (quote ("png" "pnm"))) (k 0)) (d (n "imageproc") (r "^0.22") (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1n3djdi15v8sdgwadywhkd9xzqjz7hv8x8fk6qqjzlyfwdxzgma1")))

