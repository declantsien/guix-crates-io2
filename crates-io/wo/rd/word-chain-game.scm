(define-module (crates-io wo rd word-chain-game) #:use-module (crates-io))

(define-public crate-word-chain-game-0.1.0 (c (n "word-chain-game") (v "0.1.0") (d (list (d (n "colored") (r "^2.1.0") (d #t) (k 0)))) (h "0g60p8a37vkw25xxjvqspb06gbm9zksfmb5an1dwq2pvk1wbgzfj")))

(define-public crate-word-chain-game-0.2.0 (c (n "word-chain-game") (v "0.2.0") (d (list (d (n "colored") (r "^2.1.0") (d #t) (k 0)))) (h "1ynpcc2h78ydxb4s13nq952fy7j9l38506k8ljzn1mfcgrq78b5j")))

(define-public crate-word-chain-game-1.0.0 (c (n "word-chain-game") (v "1.0.0") (d (list (d (n "colored") (r "^2.1.0") (d #t) (k 0)))) (h "0nzbvjx2pvlc0bc5pz531ss7xs1mss1k1lqq1ars92mvgd3gpacr")))

(define-public crate-word-chain-game-1.0.1 (c (n "word-chain-game") (v "1.0.1") (d (list (d (n "assert_cmd") (r "^2.0.12") (d #t) (k 2)) (d (n "assert_fs") (r "^1.1.0") (d #t) (k 2)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "predicates") (r "^3.0.4") (d #t) (k 2)))) (h "003f1a23v1sqb34125b28vcn5hs0n6njazkq7z2a8gjsrmywz2wv")))

