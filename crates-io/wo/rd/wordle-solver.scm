(define-module (crates-io wo rd wordle-solver) #:use-module (crates-io))

(define-public crate-wordle-solver-0.1.0 (c (n "wordle-solver") (v "0.1.0") (d (list (d (n "assets_manager") (r "^0.7.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "166chb2xkfjjjnz5g2dj45q53mymjqwd8wbzm2v5s295wdbhm99g") (y #t) (r "1.56.1")))

(define-public crate-wordle-solver-0.1.1 (c (n "wordle-solver") (v "0.1.1") (d (list (d (n "assets_manager") (r "^0.7.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "0vf7jd9m7svkf2hkwml8yq4kw92kny6fvia75fly1njkz6y4j551") (r "1.56.1")))

(define-public crate-wordle-solver-0.2.1 (c (n "wordle-solver") (v "0.2.1") (d (list (d (n "assets_manager") (r "^0.7.2") (d #t) (k 0)) (d (n "clap") (r "^3.0.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "16psxkhwswgymkz2nimh7yr2p5v6wnn6fqhv9bf59hjiz5rr8v40") (r "1.56.1")))

(define-public crate-wordle-solver-0.3.0 (c (n "wordle-solver") (v "0.3.0") (d (list (d (n "assets_manager") (r "^0.8.1") (d #t) (k 0)) (d (n "clap") (r "^3.0.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.6") (d #t) (k 0)))) (h "0czafkgfdbf5x2f0h5aldnv23sdf8cb38c6ffpi905qqhmch23nl") (r "1.56.1")))

