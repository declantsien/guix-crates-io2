(define-module (crates-io wo rd words-game) #:use-module (crates-io))

(define-public crate-words-game-0.1.0 (c (n "words-game") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0xj3b4m2mw5cm7ip1s1399lrqdffffprl8y2n9h4f8nw4bz92lda")))

