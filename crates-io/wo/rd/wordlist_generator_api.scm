(define-module (crates-io wo rd wordlist_generator_api) #:use-module (crates-io))

(define-public crate-wordlist_generator_api-0.1.0 (c (n "wordlist_generator_api") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "0jxk7lhmln3bljdwwfxn9g9glfr7g2zl9wid99gj6zglrb9kmqgf")))

(define-public crate-wordlist_generator_api-0.1.1 (c (n "wordlist_generator_api") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "1ac5kyd2jxdqy0vlr32wh118ms911pqar4vxf7b58756hs6cv28g")))

(define-public crate-wordlist_generator_api-0.1.2 (c (n "wordlist_generator_api") (v "0.1.2") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "0kxhib32nkxb9pwqs0w1qkfl59n8ka0dn7scnfmvb60kjm80pgvm")))

