(define-module (crates-io wo rd wordle-suggest) #:use-module (crates-io))

(define-public crate-wordle-suggest-0.1.0 (c (n "wordle-suggest") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.53") (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0.4") (d #t) (k 2)) (d (n "assert_fs") (r "^1.0.7") (d #t) (k 2)) (d (n "clap") (r "^3.0.13") (f (quote ("derive"))) (d #t) (k 0)) (d (n "nom") (r "^7.1.0") (d #t) (k 0)) (d (n "predicates") (r "^2.1.1") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (f (quote ("std_rng"))) (d #t) (k 0)))) (h "1zvzzczvh5d4xz9ryzhbd4q6ldaj65mnv9bg3vrvl2igf47xpsww")))

(define-public crate-wordle-suggest-0.1.1 (c (n "wordle-suggest") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.53") (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0.4") (d #t) (k 2)) (d (n "assert_fs") (r "^1.0.7") (d #t) (k 2)) (d (n "clap") (r "^3.0.13") (f (quote ("derive"))) (d #t) (k 0)) (d (n "nom") (r "^7.1.0") (d #t) (k 0)) (d (n "predicates") (r "^2.1.1") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (f (quote ("std_rng"))) (d #t) (k 0)))) (h "1lngnmgwhmqvqnkdz5m66fknrf97v8zg39nxlgwcqni4bd7l3c3g")))

