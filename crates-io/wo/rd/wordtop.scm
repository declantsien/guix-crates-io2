(define-module (crates-io wo rd wordtop) #:use-module (crates-io))

(define-public crate-wordtop-0.1.0 (c (n "wordtop") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "ctrlc") (r "^3.2.1") (d #t) (k 0)))) (h "0l1whrp0k897lmc79sffrwdvqgk3hnxp0mscvfnrhv19b0bkdph4")))

(define-public crate-wordtop-0.1.1 (c (n "wordtop") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "ctrlc") (r "^3.2.1") (d #t) (k 0)))) (h "1w51y7b3izg38j1g19kz1cnlzlxp66nkya4xc3by1n6s5a29vx1q")))

(define-public crate-wordtop-0.1.2 (c (n "wordtop") (v "0.1.2") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "ctrlc") (r "^3.2.1") (d #t) (k 0)))) (h "167q7g8cxjxfz9fp45i2ydrp80rhd1wazhw4jq6qdidvxfj1ibky")))

(define-public crate-wordtop-0.1.3 (c (n "wordtop") (v "0.1.3") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "ctrlc") (r "^3.2.1") (d #t) (k 0)))) (h "0fir31bq7n58q8srlncyx35rqk1y7nbfpmxlnh6pxxpyx3l1z480")))

(define-public crate-wordtop-0.1.4 (c (n "wordtop") (v "0.1.4") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "ctrlc") (r "^3.2.1") (d #t) (k 0)))) (h "1mg7wd1msqs467sn0h4paxavab0vrc387dbl9bk0j6adj99ff54c")))

(define-public crate-wordtop-0.1.5 (c (n "wordtop") (v "0.1.5") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "ctrlc") (r "^3.2.1") (d #t) (k 0)))) (h "16ramri0b0hfaq44wjamxqrdc8avap18z9iwzd2b57i04a5h2mfs")))

(define-public crate-wordtop-0.1.6 (c (n "wordtop") (v "0.1.6") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "ctrlc") (r "^3.2.1") (d #t) (k 0)))) (h "07m7kwbp1jkasnbmxv611r83z40l506b26vixrkgsqy2wwwhxj8j")))

