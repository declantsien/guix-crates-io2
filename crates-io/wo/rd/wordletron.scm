(define-module (crates-io wo rd wordletron) #:use-module (crates-io))

(define-public crate-wordletron-0.1.0 (c (n "wordletron") (v "0.1.0") (h "1mxdn56061yb23arxa5r93yskjhkl8vgpb6s189132nciyyyc549") (y #t)))

(define-public crate-wordletron-0.1.1 (c (n "wordletron") (v "0.1.1") (d (list (d (n "custom_derive") (r "^0.1.7") (d #t) (k 0)) (d (n "indicatif") (r "^0.16.2") (f (quote ("rayon"))) (d #t) (k 0)) (d (n "newtype_derive") (r "^0.1.6") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)))) (h "03sw2pfds9bq5sv3xpqj586z0s73ka4gz0b6vqpcqkkzj1wjfi43") (y #t)))

(define-public crate-wordletron-0.1.2 (c (n "wordletron") (v "0.1.2") (d (list (d (n "custom_derive") (r "^0.1.7") (d #t) (k 0)) (d (n "indicatif") (r "^0.16.2") (f (quote ("rayon"))) (d #t) (k 0)) (d (n "newtype_derive") (r "^0.1.6") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)))) (h "0m5ic9mhpm6ch7ni7ik53phdqjcl7awfdvhpz3vffjfha7g276lc") (y #t)))

(define-public crate-wordletron-0.1.3 (c (n "wordletron") (v "0.1.3") (d (list (d (n "custom_derive") (r "^0.1.7") (d #t) (k 0)) (d (n "indicatif") (r "^0.16.2") (f (quote ("rayon"))) (d #t) (k 0)) (d (n "newtype_derive") (r "^0.1.6") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)))) (h "0sg360is9jpsv3ghw99lwmvh2f948968j8l570rciwlb94rfdm8v") (y #t)))

(define-public crate-wordletron-0.1.4 (c (n "wordletron") (v "0.1.4") (d (list (d (n "custom_derive") (r "^0.1.7") (d #t) (k 0)) (d (n "indicatif") (r "^0.16.2") (f (quote ("rayon"))) (d #t) (k 0)) (d (n "newtype_derive") (r "^0.1.6") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)))) (h "1kvn9scq26npc8lpbnkkrsy90y7k7hw8r07i5xdv04srjm0n0b6q")))

(define-public crate-wordletron-0.1.5 (c (n "wordletron") (v "0.1.5") (d (list (d (n "custom_derive") (r "^0.1.7") (d #t) (k 0)) (d (n "indicatif") (r "^0.16.2") (f (quote ("rayon"))) (d #t) (k 0)) (d (n "newtype_derive") (r "^0.1.6") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)))) (h "1mpaqlcfzkr5vw0gimanmpy7qpm83zdwjfprp3xlpg5ilmnxa2cd")))

(define-public crate-wordletron-0.1.6 (c (n "wordletron") (v "0.1.6") (d (list (d (n "custom_derive") (r "^0.1.7") (d #t) (k 0)) (d (n "indicatif") (r "^0.16.2") (f (quote ("rayon"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "newtype_derive") (r "^0.1.6") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)))) (h "1crqhj32qsgyczrz6frhp7lq30l16y1cj8zkk9i9kvxjfq6k1z4a")))

(define-public crate-wordletron-0.1.7 (c (n "wordletron") (v "0.1.7") (d (list (d (n "custom_derive") (r "^0.1.7") (d #t) (k 0)) (d (n "indicatif") (r "^0.16.2") (f (quote ("rayon"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "newtype_derive") (r "^0.1.6") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)))) (h "146wwv461vjasknirjgq7llnr1rc8vs3s6zghchrprdawdbvp5aa")))

(define-public crate-wordletron-0.2.0 (c (n "wordletron") (v "0.2.0") (d (list (d (n "custom_derive") (r "^0.1.7") (d #t) (k 0)) (d (n "indicatif") (r "^0.16.2") (f (quote ("rayon"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "newtype_derive") (r "^0.1.6") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)) (d (n "rpassword") (r "^6.0.1") (d #t) (k 0)))) (h "17q3hmgcbxlkgpixvc34b189wmc0czz6ygy9gaic0f1n3kjf6h08")))

