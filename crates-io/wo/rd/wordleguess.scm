(define-module (crates-io wo rd wordleguess) #:use-module (crates-io))

(define-public crate-wordleguess-0.1.0 (c (n "wordleguess") (v "0.1.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "text_io") (r "^0.1.9") (d #t) (k 0)))) (h "0v2cx713w0fdgg3z879aq8h59rd9bvlsfkjrdqzf0xl1wccgb6h0")))

(define-public crate-wordleguess-0.2.0 (c (n "wordleguess") (v "0.2.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "float-ord") (r "^0.3.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "text_io") (r "^0.1.9") (d #t) (k 0)))) (h "071xnp6bnhnw7zj90la0636758wgsad0v08z7an1agl9x0i8arfq")))

(define-public crate-wordleguess-0.3.0 (c (n "wordleguess") (v "0.3.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "float-ord") (r "^0.3.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "text_io") (r "^0.1.9") (d #t) (k 0)))) (h "032jr9n9sjwj1lr6nabqbhs7ymai2y9w48jqh2zpfxn3vwl5z8fx")))

