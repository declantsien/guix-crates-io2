(define-module (crates-io wo rd wordle) #:use-module (crates-io))

(define-public crate-wordle-0.1.0 (c (n "wordle") (v "0.1.0") (d (list (d (n "clearscreen") (r "^1.0.9") (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "1b5pc1knakpx473g6kix453lzg63dzf6893qgj277wakw1vq1wgx")))

(define-public crate-wordle-1.0.0 (c (n "wordle") (v "1.0.0") (d (list (d (n "clearscreen") (r "^1.0.9") (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "1b9rwi6km61dgz8gmp3yd1f84dbh9bvcbdycm9r5mnp7y039asr3")))

(define-public crate-wordle-1.0.1 (c (n "wordle") (v "1.0.1") (d (list (d (n "clearscreen") (r "^1.0.9") (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "0mfq5y3p0q65v7zaxfak4ilzn7bg34j1cibl07sdl317f2f6nbgs")))

