(define-module (crates-io wo rd word_counter) #:use-module (crates-io))

(define-public crate-word_counter-0.1.0 (c (n "word_counter") (v "0.1.0") (d (list (d (n "prettytable-rs") (r "^0.8") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "16pdn9my38m86lj2pkgajhn9225kx2gkcxpf76x2328j0hiky2bk")))

(define-public crate-word_counter-0.2.0 (c (n "word_counter") (v "0.2.0") (d (list (d (n "prettytable-rs") (r "^0.8") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "17rql9p8gn1nvvpdiy5dyh81a921gd2qw8g5v3bj3vbh554gi2l5")))

(define-public crate-word_counter-0.3.0 (c (n "word_counter") (v "0.3.0") (d (list (d (n "prettytable-rs") (r "^0.8") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "17hs3v6h3sx0y8jdl9vda88l3a61y12w8x2zf2655ykwpxy48zm3")))

