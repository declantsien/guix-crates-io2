(define-module (crates-io wo rd wordreference_scraper) #:use-module (crates-io))

(define-public crate-wordreference_scraper-0.1.0 (c (n "wordreference_scraper") (v "0.1.0") (d (list (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.14") (d #t) (k 0)) (d (n "scraper") (r "^0.14.0") (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1ajjc7m4p4ngk6l9rh6kbcwfg90lyppba1jlm230ila1sclj3dly")))

(define-public crate-wordreference_scraper-0.1.1 (c (n "wordreference_scraper") (v "0.1.1") (d (list (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.14") (d #t) (k 0)) (d (n "scraper") (r "^0.14.0") (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0s5amvk29lkinzh7zagvbkdp8kmzyiaad6z3kc8ahsc4yrhzwqgr")))

