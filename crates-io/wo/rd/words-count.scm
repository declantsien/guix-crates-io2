(define-module (crates-io wo rd words-count) #:use-module (crates-io))

(define-public crate-words-count-0.1.0 (c (n "words-count") (v "0.1.0") (d (list (d (n "unicode-blocks") (r "^0.1") (d #t) (k 0)))) (h "08h56s4xnrw79rgcax7vc1k72cjjp9fh4iznp9nh02d3nhqxcgjv") (y #t)))

(define-public crate-words-count-0.1.1 (c (n "words-count") (v "0.1.1") (d (list (d (n "unicode-blocks") (r "^0.1") (d #t) (k 0)))) (h "18hmkkb70hwfznvhr9akwbx9q5j80w6yg4ng2rkr7ifzns5mq3yy")))

(define-public crate-words-count-0.1.2 (c (n "words-count") (v "0.1.2") (d (list (d (n "unicode-blocks") (r "^0.1") (d #t) (k 0)))) (h "1wq6q03plc710shxklwqf4r6i2fbfq7kx3y7yxqsav0mmxwmifip")))

(define-public crate-words-count-0.1.3 (c (n "words-count") (v "0.1.3") (d (list (d (n "unicode-blocks") (r "^0.1") (d #t) (k 0)))) (h "0sq8g4pq1qmy30h2hhv6p959wihnql417mmy23c9n345qd9gav0f")))

(define-public crate-words-count-0.1.4 (c (n "words-count") (v "0.1.4") (d (list (d (n "unicode-blocks") (r "^0.1") (d #t) (k 0)))) (h "1axfngmr8gh3r2l04vy3lhdlhyijx4qcqx363mklvibxqz0ammja")))

(define-public crate-words-count-0.1.5 (c (n "words-count") (v "0.1.5") (d (list (d (n "unicode-blocks") (r "^0.1") (d #t) (k 0)))) (h "1xcc5sk7msrd11nj01c6cjf3wvdd3cw36xp5mk4zpbz9kp2aqsf5") (r "1.62")))

(define-public crate-words-count-0.1.6 (c (n "words-count") (v "0.1.6") (d (list (d (n "unicode-blocks") (r "^0.1") (d #t) (k 0)))) (h "0sa87s3wnqw9n8l34hhw4fdsldczw5503r039b27am6ymvfm71nj") (r "1.56.1")))

