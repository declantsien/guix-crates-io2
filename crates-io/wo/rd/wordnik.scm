(define-module (crates-io wo rd wordnik) #:use-module (crates-io))

(define-public crate-wordnik-0.1.0 (c (n "wordnik") (v "0.1.0") (h "1vjz8pxg3xciqnq1zla68f8qdr4whdpw06khzbj6p70n1bb2lghr")))

(define-public crate-wordnik-0.1.1 (c (n "wordnik") (v "0.1.1") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 2)) (d (n "reqwest") (r "^0.11.4") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.67") (d #t) (k 0)))) (h "0hqc0fmbgjikcbjbh96bykf6dsn7p17mnqb4nfnkn3b0ix2p7zcm")))

(define-public crate-wordnik-0.1.2 (c (n "wordnik") (v "0.1.2") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 2)) (d (n "reqwest") (r "^0.11.4") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.67") (d #t) (k 0)))) (h "1lkc9wd4cyw6q5n7wx4ri4plmg1y7l3z0zfvqa487zji22nig9af")))

