(define-module (crates-io wo rd word-kit) #:use-module (crates-io))

(define-public crate-word-kit-0.1.0 (c (n "word-kit") (v "0.1.0") (d (list (d (n "quick-xml") (r "^0.30.0") (d #t) (k 0)))) (h "1rza2yjyc85qs936zv2il87wk3jrv5j0kfbsm283vi71ls7v5gpi") (y #t) (r "1.64.0")))

(define-public crate-word-kit-0.1.1 (c (n "word-kit") (v "0.1.1") (d (list (d (n "quick-xml") (r "^0.30.0") (d #t) (k 0)))) (h "0x9lz1ylm5a0qafxv4bwwlhgvjjl5k2l970f3z21gza9qr92qlk7") (r "1.64.0")))

(define-public crate-word-kit-0.1.2 (c (n "word-kit") (v "0.1.2") (d (list (d (n "quick-xml") (r "^0.30.0") (d #t) (k 0)))) (h "06039l3mq89ndgk9g1vg5yc35ydm3d03awixyl5zkczm5zd41nz9") (r "1.64.0")))

