(define-module (crates-io wo rd word-star-puzzle-solver) #:use-module (crates-io))

(define-public crate-word-star-puzzle-solver-0.2.0 (c (n "word-star-puzzle-solver") (v "0.2.0") (d (list (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)))) (h "0rmz9sgs4dzrrxy1z9h4zdjd22a472ipy358hcc4px20gl7y0a42")))

(define-public crate-word-star-puzzle-solver-0.2.1 (c (n "word-star-puzzle-solver") (v "0.2.1") (d (list (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)))) (h "06wfc0zzdd1zq2jwqj0bsbfz0mnskgyp1cicwdffdyqhxk4yj82v")))

(define-public crate-word-star-puzzle-solver-0.2.2 (c (n "word-star-puzzle-solver") (v "0.2.2") (d (list (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)))) (h "1fvibyi5l4y63n0s6fywdzklbr6r28ajpdwq64dgqdkspy3ghgbv")))

