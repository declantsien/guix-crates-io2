(define-module (crates-io wo rd wordle-solvers) #:use-module (crates-io))

(define-public crate-wordle-solvers-0.1.0 (c (n "wordle-solvers") (v "0.1.0") (d (list (d (n "clap") (r "^3.0.6") (f (quote ("std" "color"))) (k 0)) (d (n "color-eyre") (r "^0.5.11") (k 0)) (d (n "dialoguer") (r "^0.9.0") (k 0)) (d (n "fst") (r "^0.4.7") (d #t) (k 0)) (d (n "worlde-automaton") (r "^0.1.0") (d #t) (k 0)))) (h "1z1v4kmf761hw99jkvb8fwqiq5dkg3nsw3m1l518i6lf1qs8i5jb")))

(define-public crate-wordle-solvers-0.2.0 (c (n "wordle-solvers") (v "0.2.0") (d (list (d (n "clap") (r "^3.0.6") (f (quote ("std" "color"))) (k 0)) (d (n "color-eyre") (r "^0.5.11") (k 0)) (d (n "dialoguer") (r "^0.9.0") (k 0)) (d (n "fst") (r "^0.4.7") (d #t) (k 0)) (d (n "wordle-automaton") (r "^0.2.0") (d #t) (k 0)))) (h "19vnvivqam1vvl1zl2dzdi772750rp3lxf3ss7bc6v1p0cxfr42v")))

(define-public crate-wordle-solvers-0.3.0 (c (n "wordle-solvers") (v "0.3.0") (d (list (d (n "clap") (r "^3.0.6") (f (quote ("std"))) (k 0)) (d (n "dialoguer") (r "^0.9.0") (k 0)) (d (n "eyre") (r "^0.6.5") (k 0)) (d (n "fst") (r "^0.4.7") (d #t) (k 0)) (d (n "wordle-automaton") (r "^0.3.0") (d #t) (k 0)))) (h "1niaifcjsnkaxzqpk71m1nsxdflksglk2zx3h8p39kyfnjggzq1f")))

(define-public crate-wordle-solvers-0.4.0 (c (n "wordle-solvers") (v "0.4.0") (d (list (d (n "clap") (r "^3.0.6") (f (quote ("std"))) (k 0)) (d (n "dialoguer") (r "^0.9.0") (k 0)) (d (n "eyre") (r "^0.6.5") (k 0)) (d (n "fst") (r "^0.4.7") (d #t) (k 0)) (d (n "wordle-automaton") (r "^0.4.0") (d #t) (k 0)))) (h "07205vjxjs7x3x7xsdq7w692rprcbn3dsc6204iagj8n7dldv8h6") (f (quote (("generate") ("default"))))))

(define-public crate-wordle-solvers-0.5.0 (c (n "wordle-solvers") (v "0.5.0") (d (list (d (n "clap") (r "^3.0.6") (f (quote ("std"))) (k 0)) (d (n "dialoguer") (r "^0.9.0") (k 0)) (d (n "eyre") (r "^0.6.5") (k 0)) (d (n "fst") (r "^0.4.7") (d #t) (k 0)) (d (n "wordle-automaton") (r "^0.5.0") (d #t) (k 0)))) (h "1wlz0l847pmf7ynkw976c9z51czrk4vgh0n514iciix32wblhr6c") (f (quote (("generate") ("default"))))))

(define-public crate-wordle-solvers-0.6.0 (c (n "wordle-solvers") (v "0.6.0") (d (list (d (n "clap") (r "^3.0.6") (f (quote ("std"))) (k 0)) (d (n "dialoguer") (r "^0.9.0") (k 0)) (d (n "eyre") (r "^0.6.5") (k 0)) (d (n "fst") (r "^0.4.7") (d #t) (k 0)) (d (n "wordle-automaton") (r "^0.6.0") (d #t) (k 0)))) (h "04f82nsgz747iq46j0hkfary5klxs8vlzv2swl2dwng84c1zdpsm") (f (quote (("generate") ("default"))))))

(define-public crate-wordle-solvers-0.7.0 (c (n "wordle-solvers") (v "0.7.0") (d (list (d (n "clap") (r "^3.0.6") (f (quote ("std"))) (k 0)) (d (n "dialoguer") (r "^0.9.0") (k 0)) (d (n "eyre") (r "^0.6.5") (k 0)) (d (n "fst") (r "^0.4.7") (d #t) (k 0)) (d (n "wordle-automaton") (r "^0.7.0") (d #t) (k 0)))) (h "1hl2x02dbq0ip6bqdp2kdkl3h011i2p78gq6prpjwh6hjvjqbvpi")))

(define-public crate-wordle-solvers-0.8.0 (c (n "wordle-solvers") (v "0.8.0") (d (list (d (n "clap") (r "^3.0.6") (f (quote ("std"))) (k 0)) (d (n "dialoguer") (r "^0.9.0") (k 0)) (d (n "eyre") (r "^0.6.5") (k 0)) (d (n "fst") (r "^0.4.7") (d #t) (k 0)) (d (n "wordle-automaton") (r "^0.8.0") (d #t) (k 0)))) (h "1c2byclm2aa9np1nbrx6054grnp1xxkj65qagfvhnj607pz6c572")))

(define-public crate-wordle-solvers-0.9.0 (c (n "wordle-solvers") (v "0.9.0") (d (list (d (n "clap") (r "^3.0.6") (f (quote ("std"))) (k 0)) (d (n "dialoguer") (r "^0.9.0") (k 0)) (d (n "eyre") (r "^0.6.5") (k 0)) (d (n "fst") (r "^0.4.7") (d #t) (k 0)) (d (n "wordle-automaton") (r "^0.9.0") (d #t) (k 0)))) (h "1pmknfz0aiirjbq872ysplxmh3g33g9sdgrkv6vrkbhfa0y8h920")))

