(define-module (crates-io wo rd wordbreaker) #:use-module (crates-io))

(define-public crate-wordbreaker-0.1.0 (c (n "wordbreaker") (v "0.1.0") (d (list (d (n "fst") (r "^0.4") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.20") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.9.0") (d #t) (k 0)))) (h "1z5v5vw4hd2mq3h9qhcjzwfmlcw8yvv4l296pf62ycchxwqlpvx5")))

(define-public crate-wordbreaker-0.2.0 (c (n "wordbreaker") (v "0.2.0") (d (list (d (n "fst") (r "^0.4") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.20") (k 0)) (d (n "unicode-segmentation") (r "^1.9.0") (d #t) (k 0)))) (h "15j8jmzgmsmvdc9yyn0rdbd88dk842ch66br86x6d34mbp4n691a")))

(define-public crate-wordbreaker-0.3.0 (c (n "wordbreaker") (v "0.3.0") (d (list (d (n "fst") (r "^0.4") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1") (k 0)) (d (n "unicode-segmentation") (r "^1.10") (d #t) (k 0)) (d (n "with-bench") (r "^0.4") (o #t) (d #t) (k 0) (p "criterion")))) (h "15kp4sjcfh2ydv634ss7rngjfnz5lfdfw8x7fj0l5rky8xgmqabm")))

