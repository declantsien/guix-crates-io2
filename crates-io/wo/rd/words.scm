(define-module (crates-io wo rd words) #:use-module (crates-io))

(define-public crate-words-0.1.0 (c (n "words") (v "0.1.0") (h "0h4ga9wilm8jw656d57x7py2z2q2wnwn44h1ssqxyxgsmsv026rm")))

(define-public crate-words-0.1.1 (c (n "words") (v "0.1.1") (h "0ldx0s4790qjs3ra9cb23h2npmvbdvz5rzbca3gdg817cl1zzs07")))

(define-public crate-words-0.1.2 (c (n "words") (v "0.1.2") (h "0633hybk0zc28rxpplyifq369xhj79qdxfwcqpm9xpv2xbiw2jni")))

