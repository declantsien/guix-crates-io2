(define-module (crates-io wo rd wordcel) #:use-module (crates-io))

(define-public crate-wordcel-2.0.1 (c (n "wordcel") (v "2.0.1") (d (list (d (n "anchor-lang") (r "^0.24.2") (d #t) (k 0)))) (h "1jhbkf5l4mvyi7s3c58d3x5vqdcn7s0y76n9vmlnpxcs90f1ynqr") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("cpi" "no-entrypoint"))))))

(define-public crate-wordcel-2.0.2 (c (n "wordcel") (v "2.0.2") (d (list (d (n "anchor-lang") (r "^0.24.2") (d #t) (k 0)))) (h "0xwqj0vy03szidrsb5bx5lx5c04p3g84rsv372pibzb2ygcvjcn8") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("mainnet") ("devnet") ("default") ("cpi" "no-entrypoint"))))))

