(define-module (crates-io wo rd wordlebot) #:use-module (crates-io))

(define-public crate-wordlebot-1.0.1 (c (n "wordlebot") (v "1.0.1") (d (list (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1d73zskr1gn56zmpy8lwx02cvs9dkh0dpkaz8mzr6kjcvif84gy9")))

(define-public crate-wordlebot-1.0.2 (c (n "wordlebot") (v "1.0.2") (d (list (d (n "clap") (r "^3") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1x9pibwl1yy94bsqn32wcq0z24vy0j1vk0yl4g1f3dhnksn6qbqj")))

(define-public crate-wordlebot-1.0.3 (c (n "wordlebot") (v "1.0.3") (d (list (d (n "clap") (r "^3") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1nhi3vmc53lmivn4hwazy3jvfa7hg5x4zx1pkagxwq5fwl50pbz3")))

