(define-module (crates-io wo rd wordle_clone) #:use-module (crates-io))

(define-public crate-wordle_clone-0.1.0 (c (n "wordle_clone") (v "0.1.0") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0mpafqq5g468qcf7mbd37v91kxhk3z2zlydjz8hm713ixwvgdwq8")))

(define-public crate-wordle_clone-0.1.1 (c (n "wordle_clone") (v "0.1.1") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0fazzrdi7ln6rlswx1f05qy3ivxbh0c1ylmb3ak7p64kyw5pzyds")))

(define-public crate-wordle_clone-0.1.2 (c (n "wordle_clone") (v "0.1.2") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "19794yv4v0g054x9d8k5fvzcy6962phr5bf519acr7px5zcqakcc")))

(define-public crate-wordle_clone-0.1.3 (c (n "wordle_clone") (v "0.1.3") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "12iilwx4a5am3kv9mzwjkynk0i0ihvkdzvl5i9si5c5mbdgyv2qk")))

(define-public crate-wordle_clone-0.1.4 (c (n "wordle_clone") (v "0.1.4") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0il9nrzr1ka8cnv9g9nxlz9xp1qhrv4jampa61k7wm44szvbb4h2")))

(define-public crate-wordle_clone-0.1.5 (c (n "wordle_clone") (v "0.1.5") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1ndqmspml79hcwrlgszxx9ar8h2nwljw5hz5gc9al9d8gm6phg4v")))

