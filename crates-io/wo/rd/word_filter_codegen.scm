(define-module (crates-io wo rd word_filter_codegen) #:use-module (crates-io))

(define-public crate-word_filter_codegen-0.5.0 (c (n "word_filter_codegen") (v "0.5.0") (d (list (d (n "hashbrown") (r "^0.11.2") (d #t) (k 0)) (d (n "new_debug_unreachable") (r "^1.0.4") (d #t) (k 0)) (d (n "str_overlap") (r "^0.4.3") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.42") (d #t) (k 2)) (d (n "unicode-segmentation") (r "^1.7.1") (d #t) (k 0)))) (h "1qdnqwj7k3arpih1px54bha09nhb6fnd8v3hzsjszr88giiifd33")))

(define-public crate-word_filter_codegen-0.5.1 (c (n "word_filter_codegen") (v "0.5.1") (d (list (d (n "hashbrown") (r "^0.11.2") (d #t) (k 0)) (d (n "new_debug_unreachable") (r "^1.0.4") (d #t) (k 0)) (d (n "str_overlap") (r "^0.4.3") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.42") (d #t) (k 2)) (d (n "unicode-segmentation") (r "^1.7.1") (d #t) (k 0)))) (h "0zcg6symw1xp0bsqcbcd21f26f54fb75w382bj56gg3whbb9mw73")))

(define-public crate-word_filter_codegen-0.6.0 (c (n "word_filter_codegen") (v "0.6.0") (d (list (d (n "hashbrown") (r "^0.11.2") (d #t) (k 0)) (d (n "indoc") (r "^1.0.3") (d #t) (k 2)) (d (n "new_debug_unreachable") (r "^1.0.4") (d #t) (k 0)) (d (n "str_overlap") (r "^0.4.3") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.42") (d #t) (k 2)) (d (n "unicode-segmentation") (r "^1.7.1") (d #t) (k 0)) (d (n "word_filter") (r "^0.6.0") (d #t) (k 2)))) (h "0hamjldnybcwj7hv3vhi4zyn25n8klb482ix7qccrdhqckkq71ld")))

(define-public crate-word_filter_codegen-0.7.0 (c (n "word_filter_codegen") (v "0.7.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "hashbrown") (r "^0.11.2") (d #t) (k 0)) (d (n "indoc") (r "^1.0.3") (d #t) (k 2)) (d (n "new_debug_unreachable") (r "^1.0.4") (d #t) (k 0)) (d (n "str_overlap") (r "^0.4.3") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.42") (d #t) (k 2)) (d (n "unicode-segmentation") (r "^1.7.1") (d #t) (k 0)) (d (n "word_filter") (r "^0.7.0") (d #t) (k 2)))) (h "03mw0142p8hkji74rfah1iq6sqcwi1kknb515bmh3vg8rbq1g7p8")))

