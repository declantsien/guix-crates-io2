(define-module (crates-io wo rd wordpass) #:use-module (crates-io))

(define-public crate-wordpass-0.1.0 (c (n "wordpass") (v "0.1.0") (d (list (d (n "Inflector") (r "^0.11") (d #t) (k 0)) (d (n "argh") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "1piqqjihn3cvwfaq08v6ap11lbsaiglv0l5fsp0qm1g5iziiqlf1") (f (quote (("build-binary" "argh"))))))

