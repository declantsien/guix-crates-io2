(define-module (crates-io wo rd wordle-rust) #:use-module (crates-io))

(define-public crate-wordle-rust-0.1.0 (c (n "wordle-rust") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "enable-ansi-support") (r "^0.1.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "winres") (r "^0.1") (d #t) (t "cfg(windows)") (k 1)))) (h "11x5sra6qc96h8xn3zixkh3cah2wcfh8jd9n222y1mnmyhyvy0hb")))

(define-public crate-wordle-rust-0.1.1 (c (n "wordle-rust") (v "0.1.1") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "enable-ansi-support") (r "^0.1.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "winres") (r "^0.1") (d #t) (t "cfg(windows)") (k 1)))) (h "0j895z54hybxq201282ijy66skjkbwygd95bcglpph1difrs50al")))

