(define-module (crates-io wo rd wordlet) #:use-module (crates-io))

(define-public crate-wordlet-0.1.0 (c (n "wordlet") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^3.0.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.19") (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "tui") (r "^0.14") (f (quote ("crossterm"))) (k 0)))) (h "0qmx6ci7dbxvz8amm65dyg9vlnbrqw96zknjh69vw3l39sc8m33f")))

(define-public crate-wordlet-0.2.0 (c (n "wordlet") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^3.0.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.19") (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "tui") (r "^0.14") (f (quote ("crossterm"))) (k 0)))) (h "1g2ydghd0mzz0r783g8plsr7wwssmgyf1727w5cwwsam02g02ncd")))

