(define-module (crates-io wo rd wordic) #:use-module (crates-io))

(define-public crate-wordic-0.1.0 (c (n "wordic") (v "0.1.0") (d (list (d (n "clap") (r "^3.2.17") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs") (r "^4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1zxpf9rygxf3adh46091sxn5vdw67ck3va6wd7xrn2h1zn8lx75s")))

(define-public crate-wordic-0.1.1 (c (n "wordic") (v "0.1.1") (d (list (d (n "clap") (r "^3.2.17") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs") (r "^4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0x2dbbxzc62mzpc0dzc74m5zs24f7j35zr16apnbjr0izg2z39ln")))

(define-public crate-wordic-0.1.2 (c (n "wordic") (v "0.1.2") (d (list (d (n "clap") (r "^3.2.17") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs") (r "^4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "18afcrralvrz4zx5kz5ndd13w0fpmrpl2ashyv99vhs45jl0bj9m")))

(define-public crate-wordic-0.1.3 (c (n "wordic") (v "0.1.3") (d (list (d (n "clap") (r "^3.2.17") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs") (r "^4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0xkq4hzhvdkkk9kp620np72ih7h5s5p2mvh3z2rxjjz125jgdj4s")))

(define-public crate-wordic-0.1.4 (c (n "wordic") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "clap") (r "^3.2.17") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs") (r "^4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1vh1aj7mfi0m18k8vxldkm4zbpssca4lqgimg75yaisy9i8k9xjv")))

(define-public crate-wordic-0.1.5 (c (n "wordic") (v "0.1.5") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "clap") (r "^3.2.17") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs") (r "^4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "00zb5i7f5vsh5bngaqy1ms6fl96f3s31v59b7b918mjb5q5c84dw")))

