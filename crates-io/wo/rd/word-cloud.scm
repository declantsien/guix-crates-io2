(define-module (crates-io wo rd word-cloud) #:use-module (crates-io))

(define-public crate-word-cloud-0.1.99 (c (n "word-cloud") (v "0.1.99") (d (list (d (n "image") (r "^0.25.1") (d #t) (k 0)) (d (n "imageproc") (r "^0.24.0") (d #t) (k 0)) (d (n "quadtree_rs") (r "^0.1.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 0)) (d (n "rusttype") (r "^0.9.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.117") (d #t) (k 0)))) (h "0s4c7l4lkkyd6zdp1lhzpazrak1dq6w80pm0x3i2rf51jad398xk") (y #t)))

(define-public crate-word-cloud-0.1.100 (c (n "word-cloud") (v "0.1.100") (d (list (d (n "image") (r "^0.25.1") (d #t) (k 0)) (d (n "imageproc") (r "^0.24.0") (d #t) (k 0)) (d (n "quadtree_rs") (r "^0.1.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 0)) (d (n "rusttype") (r "^0.9.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.117") (d #t) (k 0)))) (h "13v4gknd8fk39ncs6jd0x03537aqflq06z8vl2p2i2xrdx3ily82")))

