(define-module (crates-io wo rd wordlea) #:use-module (crates-io))

(define-public crate-wordlea-0.1.0 (c (n "wordlea") (v "0.1.0") (h "1yi89d1f6hppas12dals162qm7a6gq6lba89sggz5r1s9hrxhxss")))

(define-public crate-wordlea-0.1.1 (c (n "wordlea") (v "0.1.1") (h "0a2bkhskigvfrq2k4s3jcqprjcm8177wggaglpf9047sas7l6h4w")))

(define-public crate-wordlea-0.1.2 (c (n "wordlea") (v "0.1.2") (h "0glclbm42nyi53x43vxxf0vg3yxz6d4fxrrbnv25idpjrmf2vm9l")))

(define-public crate-wordlea-0.1.3 (c (n "wordlea") (v "0.1.3") (h "0rpp0312vy340drid1vmbd36cdy7ijxbnkmdr5vmz9vf0j1dv1cm")))

(define-public crate-wordlea-0.1.4 (c (n "wordlea") (v "0.1.4") (h "0hswybws2rdvlpc35pbgi2i7qp2va5qx2alqd8pi5bq5vdhv7agm")))

