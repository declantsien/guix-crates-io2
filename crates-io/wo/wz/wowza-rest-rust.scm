(define-module (crates-io wo wz wowza-rest-rust) #:use-module (crates-io))

(define-public crate-wowza-rest-rust-0.0.0 (c (n "wowza-rest-rust") (v "0.0.0") (h "14f7h51x6xv19h3m6rjcnw4w0g157cnm7a7wdbcz6i6gqw56fd7d")))

(define-public crate-wowza-rest-rust-0.1.0 (c (n "wowza-rest-rust") (v "0.1.0") (d (list (d (n "isahc") (r "^0.7") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "12i45p62n6gsyd40ik5m60ikcinrgys7yl2p45pn0x12hzhcm420")))

(define-public crate-wowza-rest-rust-0.2.0 (c (n "wowza-rest-rust") (v "0.2.0") (d (list (d (n "isahc") (r "^0.7") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0pp92nxxh5bgn82ddq5rqv2kgzb0fbi7fm1iirfn549w8vw1xwmn")))

(define-public crate-wowza-rest-rust-0.2.1 (c (n "wowza-rest-rust") (v "0.2.1") (d (list (d (n "isahc") (r "^0.7") (f (quote ("json" "static-curl"))) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1508pfnax2mls0jaa7503b16lg230pl1gq018fmsivrm80fmizpv")))

