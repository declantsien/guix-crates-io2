(define-module (crates-io wo ok wookong-solo) #:use-module (crates-io))

(define-public crate-wookong-solo-0.1.2 (c (n "wookong-solo") (v "0.1.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0hdkb56g7y7xzbspnwanggr0ws66r0kcd1d12qaz1wlyv79icgha")))

(define-public crate-wookong-solo-0.1.3 (c (n "wookong-solo") (v "0.1.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1ywplds779c1nq33nvr223r3gf9i2cdnp9rvzz59mzvw9ajjn8gd")))

(define-public crate-wookong-solo-0.1.4 (c (n "wookong-solo") (v "0.1.4") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0scj7qhrjlid5sawmsx64a133fl7w0bqs1zwlp0mjimz90p9f8m5")))

