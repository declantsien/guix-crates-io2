(define-module (crates-io wo ok wookie) #:use-module (crates-io))

(define-public crate-wookie-0.1.0 (c (n "wookie") (v "0.1.0") (d (list (d (n "futures-micro") (r "^0.5") (d #t) (k 2)) (d (n "pin-project-lite") (r "^0.2") (d #t) (k 0)))) (h "1k4w79aflflwiy6x20fpgp7pdnpw18z7rny2syk4w8bwzmvn9j3s")))

(define-public crate-wookie-0.1.1 (c (n "wookie") (v "0.1.1") (d (list (d (n "futures-micro") (r "^0.5") (d #t) (k 2)) (d (n "pin-project-lite") (r "^0.2") (d #t) (k 0)))) (h "0ijf3pcnyxibniw9pfzmgv6h4m8f8plic7m74bwna0c6rlykja8b")))

(define-public crate-wookie-0.2.0 (c (n "wookie") (v "0.2.0") (d (list (d (n "futures-micro") (r "^0.5") (d #t) (k 2)) (d (n "pin-project-lite") (r "^0.2.7") (d #t) (k 0)))) (h "0cvh9m8wb2qmlw49jkpnpz3z19axfqca78lrslf8n6lykag5m21k")))

(define-public crate-wookie-0.3.0 (c (n "wookie") (v "0.3.0") (d (list (d (n "dummy-waker") (r "^1") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2.7") (d #t) (k 0)))) (h "144040w6xhf58i6ld2y6v63bwabki33swsgbgzzsfibl00z5a7z7") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-wookie-0.3.1 (c (n "wookie") (v "0.3.1") (d (list (d (n "dummy-waker") (r "^1") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2.7") (d #t) (k 0)))) (h "12fy8rfliz7350vkw7hhzvpd9403fcw43gza675702js598v8sjc") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-wookie-0.3.2 (c (n "wookie") (v "0.3.2") (d (list (d (n "dummy-waker") (r "^1") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2.7") (d #t) (k 0)))) (h "0kkjz26dmmixcvf1dl42ph6i4jy7k2h2xbqy1ry0br0n1an1y0xs") (f (quote (("default" "alloc") ("alloc"))))))

