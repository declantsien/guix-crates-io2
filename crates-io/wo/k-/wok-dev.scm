(define-module (crates-io wo k- wok-dev) #:use-module (crates-io))

(define-public crate-wok-dev-0.3.0-dev (c (n "wok-dev") (v "0.3.0-dev") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "assert_fs") (r "^1.0.7") (d #t) (k 2)) (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "git2") (r "^0.17.2") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.2.1") (d #t) (k 2)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)) (d (n "serde") (r "^1.0.143") (f (quote ("derive"))) (d #t) (k 0)) (d (n "shell-words") (r "^1.1.0") (d #t) (k 2)) (d (n "toml") (r "^0.7.2") (d #t) (k 0)))) (h "1v24mjn2b0xhbvc47005zan0bcm44waabnzn43fcv8w53z25c72z")))

