(define-module (crates-io wo bb wobbly) #:use-module (crates-io))

(define-public crate-wobbly-0.1.0 (c (n "wobbly") (v "0.1.0") (h "1xm8mi8prp5wk0g32gz14z19gzlndsglzpar3f392x2zs2dzbs2z") (f (quote (("std")))) (r "1.56")))

(define-public crate-wobbly-0.1.1 (c (n "wobbly") (v "0.1.1") (h "1mmxvg0bdzfnd18rnc1grqb3wnyz9sivgmkcxry6l3r6nkk7xi0h") (f (quote (("std")))) (r "1.56")))

