(define-module (crates-io wo on woongdle) #:use-module (crates-io))

(define-public crate-woongdle-0.1.0 (c (n "woongdle") (v "0.1.0") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "termion") (r "^1.5") (d #t) (k 0)))) (h "0ww89lzrgfsg2n372x7fxihq5ngc32id5yi6nzwxikdkv94jikdi")))

(define-public crate-woongdle-0.1.2 (c (n "woongdle") (v "0.1.2") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "termion") (r "^1.5") (d #t) (k 0)))) (h "1436jsjqal4cywj5qp8ac8wkdw75hdi44i2sfq594s7fnwwabrms")))

(define-public crate-woongdle-0.1.3 (c (n "woongdle") (v "0.1.3") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "termion") (r "^1.5") (d #t) (k 0)))) (h "1mbxmjs86iqr463z06aqs1ysv4hdj4cp6dd96v7z64b5xbdnhs9f")))

