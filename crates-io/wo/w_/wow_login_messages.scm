(define-module (crates-io wo w_ wow_login_messages) #:use-module (crates-io))

(define-public crate-wow_login_messages-0.1.0 (c (n "wow_login_messages") (v "0.1.0") (h "01pf3s962c7prhjy45x3vv8dl4ys5yv1zddy6b0l8yl5jhl3cbbd")))

(define-public crate-wow_login_messages-0.2.0 (c (n "wow_login_messages") (v "0.2.0") (d (list (d (n "async-std") (r "^1.11") (f (quote ("async-io" "std"))) (o #t) (k 0)) (d (n "async-std") (r "^1.11") (f (quote ("attributes" "default"))) (k 2)) (d (n "tokio") (r "^1") (f (quote ("io-util"))) (o #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt"))) (k 2)))) (h "16j4j5m9ihv6rhg0gf3c4q94d1nbz0fh84ixr18iq4mk6c93jbp7") (f (quote (("sync") ("default"))))))

(define-public crate-wow_login_messages-0.3.0 (c (n "wow_login_messages") (v "0.3.0") (d (list (d (n "async-std") (r "^1.11") (f (quote ("async-io" "std"))) (o #t) (k 0)) (d (n "async-std") (r "^1.11") (f (quote ("attributes" "default"))) (k 2)) (d (n "tokio") (r "^1") (f (quote ("io-util"))) (o #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt"))) (k 2)))) (h "0qk2c2z6r6gic793qc90p7if74l72jbbj3w5kajki5icqbkrd5m6") (f (quote (("sync") ("default"))))))

(define-public crate-wow_login_messages-0.4.0 (c (n "wow_login_messages") (v "0.4.0") (d (list (d (n "async-std") (r "^1.11") (f (quote ("async-io" "std"))) (o #t) (k 0)) (d (n "async-std") (r "^1.11") (f (quote ("attributes" "default"))) (k 2)) (d (n "tokio") (r "^1") (f (quote ("io-util"))) (o #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt"))) (k 2)))) (h "0x95wxxvgcjmr746d8hhb2wgw7py04x6b1ndmgmj5051xhzz2mzd") (f (quote (("sync") ("print-testcase") ("default"))))))

