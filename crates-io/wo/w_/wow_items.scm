(define-module (crates-io wo w_ wow_items) #:use-module (crates-io))

(define-public crate-wow_items-0.1.0 (c (n "wow_items") (v "0.1.0") (d (list (d (n "wow_world_base") (r "^0.1.0") (k 0)))) (h "16mwjckyfrk5csnnc0qgqybh0psxny1h7f7zmmczvppi02y32ksz") (f (quote (("wrath" "wow_world_base/wrath") ("vanilla" "wow_world_base/vanilla") ("unobtainable") ("tbc" "wow_world_base/tbc") ("default"))))))

(define-public crate-wow_items-0.2.0 (c (n "wow_items") (v "0.2.0") (d (list (d (n "wow_world_base") (r "^0.2.0") (k 0)))) (h "03cfj1i2havgvzxbfqw0kdl5j789r62y1gy1y1637ai062pfassr") (f (quote (("wrath" "wow_world_base/wrath") ("vanilla" "wow_world_base/vanilla") ("tbc" "wow_world_base/tbc") ("default"))))))

