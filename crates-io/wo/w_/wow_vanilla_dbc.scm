(define-module (crates-io wo w_ wow_vanilla_dbc) #:use-module (crates-io))

(define-public crate-wow_vanilla_dbc-0.1.0 (c (n "wow_vanilla_dbc") (v "0.1.0") (h "1fsqflj1pp6199l3fhhbs89mjbncn4lp03igd5lz4jlv7yxyhvr5")))

(define-public crate-wow_vanilla_dbc-0.1.1 (c (n "wow_vanilla_dbc") (v "0.1.1") (h "04qiss01241nvr52bqmpdnamjzp8qpygyqpp189xq62pj5ingvcg")))

