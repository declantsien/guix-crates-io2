(define-module (crates-io wo w_ wow_spells) #:use-module (crates-io))

(define-public crate-wow_spells-0.1.0 (c (n "wow_spells") (v "0.1.0") (d (list (d (n "wow_world_base") (r "^0.1.0") (k 0)))) (h "1y21x1vxmk12pm88hrnwjl71yfh72zqwzhi4r4iw6lkwm171pqy0") (f (quote (("wrath" "wow_world_base/wrath") ("vanilla" "wow_world_base/vanilla") ("tbc" "wow_world_base/tbc") ("default"))))))

(define-public crate-wow_spells-0.2.0 (c (n "wow_spells") (v "0.2.0") (d (list (d (n "wow_world_base") (r "^0.2.0") (k 0)))) (h "1skn2hff8iv7ayrbrlycx5m6xj16xm03zy80k11rmnhrsc18b5p9") (f (quote (("wrath" "wow_world_base/wrath") ("vanilla" "wow_world_base/vanilla") ("tbc" "wow_world_base/tbc") ("default"))))))

