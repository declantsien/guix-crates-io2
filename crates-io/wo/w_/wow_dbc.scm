(define-module (crates-io wo w_ wow_dbc) #:use-module (crates-io))

(define-public crate-wow_dbc-0.1.0 (c (n "wow_dbc") (v "0.1.0") (h "0c4h1a1ymv2zcfvbh7ndh1zf2aq26fa8fzdy2x9k03zx1xhxfxpd") (f (quote (("wrath") ("vanilla") ("tbc") ("default" "vanilla" "tbc" "wrath"))))))

(define-public crate-wow_dbc-0.2.0 (c (n "wow_dbc") (v "0.2.0") (h "1jcbwbklaza48jg257ag4ysw0qga0a14jzi7qcsmladsaj1n5bgm") (f (quote (("wrath") ("vanilla") ("tbc") ("default"))))))

(define-public crate-wow_dbc-0.3.0 (c (n "wow_dbc") (v "0.3.0") (d (list (d (n "wow_world_base") (r "^0.2.0") (d #t) (k 0)))) (h "0dzjw7cycyq2gp0cdx6p1f09dsrq808zh28d2mrvia6r62djardj") (f (quote (("wrath" "wow_world_base/wrath") ("vanilla" "wow_world_base/vanilla") ("tbc" "wow_world_base/tbc") ("default"))))))

