(define-module (crates-io wo w_ wow_world_base) #:use-module (crates-io))

(define-public crate-wow_world_base-0.1.0 (c (n "wow_world_base") (v "0.1.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "10k2qcqa3zy8ix1h5hy50yvz07jlk361lqinzdnv0r48ph9yrfqf") (f (quote (("wrath") ("vanilla") ("tbc") ("extended"))))))

(define-public crate-wow_world_base-0.1.1 (c (n "wow_world_base") (v "0.1.1") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1kb74g5x8hgbqqa1r5xjwzx9a80y7r5q1dvmavhypjhlgfym103a") (f (quote (("wrath") ("vanilla") ("tbc") ("extended"))))))

(define-public crate-wow_world_base-0.2.0 (c (n "wow_world_base") (v "0.2.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (k 0)))) (h "0yhizd1gv3ybpqzl5wvxard6ivmvx1dyjrd80yj9hj2gnfzvpac1") (f (quote (("wrath") ("vanilla") ("tbc") ("shared") ("print-testcase") ("extended"))))))

