(define-module (crates-io wo ab woab-macros) #:use-module (crates-io))

(define-public crate-woab-macros-0.1.0 (c (n "woab-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.39") (d #t) (k 0)))) (h "1rnmmycyqilakg6af8knm91146m5x5ia4wqk0lyrqk67f74z98y5")))

(define-public crate-woab-macros-0.2.1 (c (n "woab-macros") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.64") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1nfbrf7ivp7i1wn20f1bzgr46gk7x6yiizlbvalmg550jkmnli19")))

(define-public crate-woab-macros-0.3.0 (c (n "woab-macros") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.64") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1yc760z6b6wns9sfbz6mblh2z4f4pbd8vkiqq5v5ypqw574l1p3j")))

(define-public crate-woab-macros-0.4.0 (c (n "woab-macros") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.64") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0g3v5cp6il8yszprmv9wjrf8diw250aww6dwgakrkbzql69k1icx")))

(define-public crate-woab-macros-0.5.0 (c (n "woab-macros") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.64") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1zf3z5lmhxlfa8w2ihx9py335r5n49b7ha4z47l7l872xgc2dlki")))

(define-public crate-woab-macros-0.6.0 (c (n "woab-macros") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.64") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1flw000si0wicvapx9i1pjkb3v08827mpjgbbbsd1cahihy00n4a")))

(define-public crate-woab-macros-0.7.0 (c (n "woab-macros") (v "0.7.0") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "10z47gg0dya93lh5awnrhgbcfgmsnwasprz8ggd464ygw49qgw4w")))

(define-public crate-woab-macros-0.9.0 (c (n "woab-macros") (v "0.9.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "084bzm9hlp05phjm111f1qa4ax7xyn9lgv31rkq11q5d5r09hy4i")))

