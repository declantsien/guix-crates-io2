(define-module (crates-io wo ts wots-rs) #:use-module (crates-io))

(define-public crate-wots-rs-0.1.0 (c (n "wots-rs") (v "0.1.0") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "sha256-rs") (r "^1.0") (d #t) (k 0)))) (h "00lnmmvbdph5avyi0hnah4ymga0pblb7ys9isp8bjj57iznmqpf0")))

(define-public crate-wots-rs-0.2.0 (c (n "wots-rs") (v "0.2.0") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "sha256-rs") (r "^1.0") (d #t) (k 0)))) (h "0rwcbidrk29qdd1bakd18k8m2cxfwj2r86s5gp2a8476fwkd1r18")))

