(define-module (crates-io wo tw wotw_seedgen_derive) #:use-module (crates-io))

(define-public crate-wotw_seedgen_derive-0.1.0 (c (n "wotw_seedgen_derive") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0mrkzm011bcfcclrqg9blrnhl7jbv72sa0ivkglxd90m1vpk9l75")))

(define-public crate-wotw_seedgen_derive-0.1.1 (c (n "wotw_seedgen_derive") (v "0.1.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "042swabykafl4laahhxwasr5j8nndbassj5rzrlpd2abba9v6q1f")))

