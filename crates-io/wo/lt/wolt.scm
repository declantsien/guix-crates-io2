(define-module (crates-io wo lt wolt) #:use-module (crates-io))

(define-public crate-wolt-0.1.0 (c (n "wolt") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.41") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.3") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "tokio") (r "^1.7.1") (f (quote ("macros"))) (d #t) (k 0)))) (h "09jrfq7kymn47xl35chhmdvccrl8fk2jrz4ykpyg36iiql8kmnns")))

