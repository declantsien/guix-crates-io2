(define-module (crates-io wo nk wonky) #:use-module (crates-io))

(define-public crate-wonky-0.1.0 (c (n "wonky") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tinybit") (r "^0.1.1") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "0psfhqbg4h2aa8dwdd7kxga5vnp6rqs1rnxkyyga11m1ikpn3q6p")))

(define-public crate-wonky-0.1.1 (c (n "wonky") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tinybit") (r "^0.1.1") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "02l052b080jiha41q8w69klgnjjgn9zr26pcnc7bdkaapq3n6pj6")))

(define-public crate-wonky-1.0.0 (c (n "wonky") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tinybit") (r "^0.1.1") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "10i9zwgnik6rl6kfng3hm4f9cmqrysjw4zrq6wmjqllyzzgr5gfk")))

(define-public crate-wonky-1.0.1 (c (n "wonky") (v "1.0.1") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tinybit") (r "^0.1.1") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "18pj76zpx71d1lpaqyl3rgk9qc5n8wyfzxpxn2wvxlvyp6dmhfjp")))

