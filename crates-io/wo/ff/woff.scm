(define-module (crates-io wo ff woff) #:use-module (crates-io))

(define-public crate-woff-0.0.1 (c (n "woff") (v "0.0.1") (h "0582ba9a7plh2pv1c7jyky71i46c41s2rpbi2j9gklgx5ggwbmk8")))

(define-public crate-woff-0.1.0 (c (n "woff") (v "0.1.0") (d (list (d (n "arguments") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "brotli2") (r "^0.3") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1v16s8gbwplzqx5mamjacnww43j9cgj44x5a20g9xr9ygh9xfmzq") (f (quote (("binary" "arguments"))))))

(define-public crate-woff-0.2.0 (c (n "woff") (v "0.2.0") (d (list (d (n "arguments") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "brotli2") (r "^0.3") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "04zld7khfxsp159lbh17bxg3p3f06hih90wq69b7sswiqmhz51cz") (f (quote (("binary" "arguments"))))))

(define-public crate-woff-0.2.2 (c (n "woff") (v "0.2.2") (d (list (d (n "arguments") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "12mqy2nnakbdq3v7pvzmlyz6zd77hvjjz31q1bjabwsgc2wc8r0l") (f (quote (("binary" "arguments"))))))

(define-public crate-woff-0.2.3 (c (n "woff") (v "0.2.3") (d (list (d (n "arguments") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "11rx59v32vp0g2z4ak73n1lr1i2qzy7nfc9chw6jk1id9ng71sih") (f (quote (("binary" "arguments"))))))

(define-public crate-woff-0.3.0 (c (n "woff") (v "0.3.0") (d (list (d (n "arguments") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0hk5byvnlsy8wj2g9vjb7x8d7s1qj2kknhwy57vp6isvcakc193k") (f (quote (("binary" "arguments"))))))

(define-public crate-woff-0.3.1 (c (n "woff") (v "0.3.1") (d (list (d (n "arguments") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0zwqrwp2ci2y9vxfx8jx34ll0cbsiy3r20inbqpr52kx2f47jqlr") (f (quote (("binary" "arguments"))))))

(define-public crate-woff-0.3.3 (c (n "woff") (v "0.3.3") (d (list (d (n "arguments") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1xx9hivdq7nybc7wy4sbcyjzlsb9y6v5w1hsrbi1kplhynibw5zi") (f (quote (("binary" "arguments"))))))

