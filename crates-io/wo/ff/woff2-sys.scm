(define-module (crates-io wo ff woff2-sys) #:use-module (crates-io))

(define-public crate-woff2-sys-0.0.1 (c (n "woff2-sys") (v "0.0.1") (d (list (d (n "brotli-sys") (r "^0.3") (d #t) (k 0)) (d (n "cc") (r "^1.0.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "cpp") (r "^0.3") (d #t) (k 0)) (d (n "cpp_build") (r "^0.3") (d #t) (k 1)))) (h "1liam4hs524nqcqrcbrr5jg2s14nwh49mq56vadxhgi4lvyrr2rx")))

