(define-module (crates-io wo ff woff2-patched) #:use-module (crates-io))

(define-public crate-woff2-patched-0.3.0 (c (n "woff2-patched") (v "0.3.0") (d (list (d (n "bitvec") (r "^1.0.0") (d #t) (k 0)) (d (n "brotli") (r "^3.3.3") (f (quote ("std"))) (k 0)) (d (n "bytes") (r "^1.1.0") (d #t) (k 0)) (d (n "clap") (r "^3.1.6") (f (quote ("derive"))) (d #t) (k 2)) (d (n "four-cc") (r "^0.2.0") (d #t) (k 0)) (d (n "safer-bytes") (r "^0.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "ttf-parser") (r "^0.15.0") (d #t) (k 2)))) (h "0a9njk4k9pa99x2dhp1inx43q5f9x5aqrghr3vdc6w6kfa2f5bkn")))

