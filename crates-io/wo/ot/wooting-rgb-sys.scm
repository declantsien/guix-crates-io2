(define-module (crates-io wo ot wooting-rgb-sys) #:use-module (crates-io))

(define-public crate-wooting-rgb-sys-0.2.0 (c (n "wooting-rgb-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.65") (d #t) (k 1)) (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1l5pkk8g7g81l7z9br96xw6hcmmix805r2b8alfs3548cqn1710c")))

(define-public crate-wooting-rgb-sys-0.3.0 (c (n "wooting-rgb-sys") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.65") (d #t) (k 1)) (d (n "cc") (r "^1") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0v3irnns0zj9ahd5qy8si8rd87585345xfbdk1x2l2mk0l504fpp")))

(define-public crate-wooting-rgb-sys-0.3.1 (c (n "wooting-rgb-sys") (v "0.3.1") (d (list (d (n "bindgen") (r "^0.65") (d #t) (k 1)) (d (n "cc") (r "^1") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "17lzjdkaysya1m7ixwz5jfhyry578b9ph3s3zj459axw499if9zj")))

(define-public crate-wooting-rgb-sys-0.3.2 (c (n "wooting-rgb-sys") (v "0.3.2") (d (list (d (n "bindgen") (r "^0.65") (d #t) (k 1)) (d (n "cc") (r "^1") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1jb66q1c3867rz7mc6dvb5895cv015az253f091snnk680ax6v1z")))

