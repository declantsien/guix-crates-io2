(define-module (crates-io wo ot wooting-analog-common) #:use-module (crates-io))

(define-public crate-wooting-analog-common-0.1.0 (c (n "wooting-analog-common") (v "0.1.0") (d (list (d (n "enum_primitive") (r "^0.1.1") (d #t) (k 0)) (d (n "ffi-support") (r "^0.3.5") (d #t) (k 0)) (d (n "log") (r "^0.4.7") (d #t) (k 0)))) (h "0729ppp8kj3sfsgd4fp7sy549asv6ixdvmwcbczpy5w90fw5lzrr")))

(define-public crate-wooting-analog-common-0.3.0 (c (n "wooting-analog-common") (v "0.3.0") (d (list (d (n "enum_primitive") (r "^0.1.1") (d #t) (k 0)) (d (n "ffi-support") (r "^0.3.5") (d #t) (k 0)) (d (n "log") (r "^0.4.7") (d #t) (k 0)))) (h "1ah1j6kq4giirk9f0jy1a2hhszrkanq0r6xhn685hvcnlymg7c0h")))

(define-public crate-wooting-analog-common-0.4.0 (c (n "wooting-analog-common") (v "0.4.0") (d (list (d (n "enum_primitive") (r "^0.1.1") (d #t) (k 0)) (d (n "ffi-support") (r "^0.3.5") (d #t) (k 0)) (d (n "log") (r "^0.4.7") (d #t) (k 0)))) (h "13qzcnpajmw8s4irz9n4jc235vlny834n33sd9mwck9d6wgfjdhs")))

(define-public crate-wooting-analog-common-0.5.0 (c (n "wooting-analog-common") (v "0.5.0") (d (list (d (n "enum-primitive-derive") (r "^0.2") (d #t) (k 0)) (d (n "ffi-support") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "08hlxb1qgz6nm8bcqdjv8sx7223jm1crshbiycm68r6mzz4c8zz8")))

(define-public crate-wooting-analog-common-0.6.0 (c (n "wooting-analog-common") (v "0.6.0") (d (list (d (n "enum-primitive-derive") (r "^0.2") (d #t) (k 0)) (d (n "ffi-support") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "028g6g2xdj7chvbskwc4m13qq5s84vqd83p6bbqakdpdmyqadbxz")))

(define-public crate-wooting-analog-common-0.7.0 (c (n "wooting-analog-common") (v "0.7.0") (d (list (d (n "enum-primitive-derive") (r "^0.2") (d #t) (k 0)) (d (n "ffi-support") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1wdwswn4nrczn0r0ss125apjphakb3vzhysgmhgmcxiakglhfa96") (f (quote (("serdes" "serde"))))))

(define-public crate-wooting-analog-common-0.7.1 (c (n "wooting-analog-common") (v "0.7.1") (d (list (d (n "enum-primitive-derive") (r "^0.2") (d #t) (k 0)) (d (n "ffi-support") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1nc2nfdlfsagcfs6dqq4dn2mqra8l72hv98gqa4m5zyjbdi31iaz") (f (quote (("serdes" "serde"))))))

