(define-module (crates-io wo ot wooting-rgb-sdk-sys) #:use-module (crates-io))

(define-public crate-wooting-rgb-sdk-sys-0.1.0 (c (n "wooting-rgb-sdk-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.47.2") (d #t) (k 1)) (d (n "cc") (r "^1.0.29") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.14") (d #t) (k 1)))) (h "1n9ivkhvgf3h9afi48gwx0d8qq5gdc32vsrhrba41x1vr9h64yip")))

