(define-module (crates-io wo ot wooting-analog-sdk-sys) #:use-module (crates-io))

(define-public crate-wooting-analog-sdk-sys-0.1.0 (c (n "wooting-analog-sdk-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.47.2") (d #t) (k 1)) (d (n "cc") (r "^1.0.29") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.14") (d #t) (k 1)))) (h "1a7wzf7spc6ba5r71f6hngl21j29s8wy9h4ndgcpjxpjyp7dqqbx")))

