(define-module (crates-io wo ot wooting-sdk) #:use-module (crates-io))

(define-public crate-wooting-sdk-0.1.0 (c (n "wooting-sdk") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "wooting-analog-sdk-sys") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "wooting-rgb-sdk-sys") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "0a1d8ka020k2j37c4fyldjzm75arwwq1xi63mbzx1zfmaf03cqdg") (f (quote (("rgb" "wooting-rgb-sdk-sys") ("default" "analog" "rgb") ("analog" "wooting-analog-sdk-sys"))))))

(define-public crate-wooting-sdk-0.1.1 (c (n "wooting-sdk") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "wooting-analog-sdk-sys") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "wooting-rgb-sdk-sys") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "0gb84gxyqsqbwzqsyz3681v86h22gyi89bwblmssnrcgd8wr2kgn") (f (quote (("rgb" "wooting-rgb-sdk-sys") ("default" "analog" "rgb") ("analog" "wooting-analog-sdk-sys"))))))

