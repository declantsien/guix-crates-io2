(define-module (crates-io wo ot wooting-rgb) #:use-module (crates-io))

(define-public crate-wooting-rgb-0.2.0 (c (n "wooting-rgb") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "wooting-rgb-sys") (r "^0.2.0") (d #t) (k 0)))) (h "0l4qkacf3m0kqlgda8ngf0dc9vkhbdpj70v2fm12sp5cga6lxfac")))

(define-public crate-wooting-rgb-0.3.0 (c (n "wooting-rgb") (v "0.3.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "wooting-rgb-sys") (r "^0.3") (d #t) (k 0)))) (h "1kmx6xsccini3igv5q7hf5azwdlpqc7wj8gmnpyips1n92n7cgk2")))

(define-public crate-wooting-rgb-0.3.1 (c (n "wooting-rgb") (v "0.3.1") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "wooting-rgb-sys") (r "^0.3") (d #t) (k 0)))) (h "0bz06s1ka00qi19pvis76n6vhdfdszzzrif7aw17kx3xyd0zacvq")))

(define-public crate-wooting-rgb-0.3.2 (c (n "wooting-rgb") (v "0.3.2") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "wooting-rgb-sys") (r "^0.3") (d #t) (k 0)))) (h "1bs4n44a0snika1c306h29hj1984zhqh7sgnizff92ipkwvwvic4")))

