(define-module (crates-io wo ot wooting-analog-plugin-dev) #:use-module (crates-io))

(define-public crate-wooting-analog-plugin-dev-0.1.0 (c (n "wooting-analog-plugin-dev") (v "0.1.0") (d (list (d (n "ffi-support") (r "^0.3.5") (d #t) (k 0)) (d (n "wooting-analog-common") (r "^0.1.0") (d #t) (k 0)))) (h "1rm2qwj0rr6pwh82gsmgvllfxbg1cq8b0pdrphp66900ijbf2fhj")))

(define-public crate-wooting-analog-plugin-dev-0.3.0 (c (n "wooting-analog-plugin-dev") (v "0.3.0") (d (list (d (n "ffi-support") (r "^0.3.5") (d #t) (k 0)) (d (n "wooting-analog-common") (r "^0.3.0") (d #t) (k 0)))) (h "0jr6kr2fp7xc6rc0pk94vjg19qhv6rankf0yb19cjf0140532nrq")))

(define-public crate-wooting-analog-plugin-dev-0.4.0 (c (n "wooting-analog-plugin-dev") (v "0.4.0") (d (list (d (n "ffi-support") (r "^0.3.5") (d #t) (k 0)) (d (n "wooting-analog-common") (r "^0.4.0") (d #t) (k 0)))) (h "1vcf2i4idbn7vylfqvhq5y6gi6s18a1q6c4w9ph2wvrghxwbmwp9")))

(define-public crate-wooting-analog-plugin-dev-0.5.0 (c (n "wooting-analog-plugin-dev") (v "0.5.0") (d (list (d (n "ffi-support") (r "^0.4") (d #t) (k 0)) (d (n "wooting-analog-common") (r "^0.5.0") (d #t) (k 0)))) (h "0qgx96lb1w2airp8fi9d70c2r5s7mbd9cak3i9q4264j9hc6ilbi")))

(define-public crate-wooting-analog-plugin-dev-0.6.0 (c (n "wooting-analog-plugin-dev") (v "0.6.0") (d (list (d (n "ffi-support") (r "^0.4") (d #t) (k 0)) (d (n "wooting-analog-common") (r "^0.6.0") (d #t) (k 0)))) (h "0wc92j8k5pgxmv1dksnpvgk5hz4qbw2fp5wlrn44yf4q8pgshaj1")))

(define-public crate-wooting-analog-plugin-dev-0.7.0 (c (n "wooting-analog-plugin-dev") (v "0.7.0") (d (list (d (n "ffi-support") (r "^0.4") (d #t) (k 0)) (d (n "wooting-analog-common") (r "^0.7.0") (d #t) (k 0)))) (h "0lc7385h96z1gy81f0b4nvbnsxd2ws9qpyy53pgz37cimlxkh460")))

(define-public crate-wooting-analog-plugin-dev-0.7.1 (c (n "wooting-analog-plugin-dev") (v "0.7.1") (d (list (d (n "ffi-support") (r "^0.4") (d #t) (k 0)) (d (n "wooting-analog-common") (r "^0.7.1") (d #t) (k 0)))) (h "1arqlvnkhhnhvs6bg6wsc7ynaray33nflxaks802hvp8i0by1b6f")))

