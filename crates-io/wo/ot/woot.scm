(define-module (crates-io wo ot woot) #:use-module (crates-io))

(define-public crate-woot-0.1.0 (c (n "woot") (v "0.1.0") (d (list (d (n "itertools") (r "^0.5") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1l42vbarxq237h5r4pcnzk8rwlwh2sblhywf9nckq5vffrph947r")))

(define-public crate-woot-0.1.2 (c (n "woot") (v "0.1.2") (d (list (d (n "itertools") (r "^0.5") (d #t) (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "10qq7mp9q1yx39v34333bw54bzaxa3bvl8jp176jb9sbhasgv50y")))

