(define-module (crates-io wo wn wownav-parser) #:use-module (crates-io))

(define-public crate-wownav-parser-0.1.0 (c (n "wownav-parser") (v "0.1.0") (d (list (d (n "bytemuck") (r "^1.12.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ctor") (r "^0.1.23") (d #t) (k 2)) (d (n "insta") (r "^1.21.0") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 0)))) (h "15gda5pylr96vk1gac2a7f4x46wr6wmrfa82iajx8nbaahzkb0bp")))

