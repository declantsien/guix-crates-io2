(define-module (crates-io wo wn wownow) #:use-module (crates-io))

(define-public crate-wownow-0.1.0 (c (n "wownow") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.34") (f (quote ("serde"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11.24") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "scraper") (r "^0.18.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)))) (h "1nka69kxp8k1ff7bkd5y0bd2bgfwxqrlp8778j26kk1s1z4ldavk")))

(define-public crate-wownow-0.2.0 (c (n "wownow") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.34") (f (quote ("serde"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11.24") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "scraper") (r "^0.18.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)))) (h "17i3p94yma4n3gnpc2pcxsibxmka4cjfiw4d92nq6bbixcadk6gs")))

(define-public crate-wownow-0.3.0 (c (n "wownow") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4.34") (f (quote ("serde"))) (d #t) (k 0)) (d (n "clap") (r "^4.5.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)))) (h "14nqblfpfziwlf5h89aig8mq795r90qbkmzfzqfdhc99wh5vjlfv")))

