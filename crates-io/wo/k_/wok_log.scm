(define-module (crates-io wo k_ wok_log) #:use-module (crates-io))

(define-public crate-wok_log-0.0.0 (c (n "wok_log") (v "0.0.0") (d (list (d (n "wasm-bindgen") (r "^0.2.82") (f (quote ("std"))) (k 0)) (d (n "web-sys") (r "^0.3.59") (f (quote ("console"))) (d #t) (k 0)))) (h "1bs7v2fl6x02csx701736rrqpppvnszwzx03kvv9dmza4rhn92za") (f (quote (("wrapped") ("macros" "wrapped"))))))

