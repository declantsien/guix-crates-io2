(define-module (crates-io wo rk worker-plus-build) #:use-module (crates-io))

(define-public crate-worker-plus-build-0.0.8 (c (n "worker-plus-build") (v "0.0.8") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "dirs-next") (r "^2.0.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0.24") (d #t) (k 0)) (d (n "tar") (r "^0.4.38") (d #t) (k 0)) (d (n "ureq") (r "^2.4.0") (f (quote ("tls" "gzip"))) (d #t) (k 0)) (d (n "wasm-bindgen-cli-support") (r "^0.2.80") (d #t) (k 2)))) (h "12dpgd7xyr5pdv14zjjwiln6cg0ma8yw3adjckc38nrq4bxhp7qp")))

(define-public crate-worker-plus-build-0.0.9 (c (n "worker-plus-build") (v "0.0.9") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "dirs-next") (r "^2.0.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0.24") (d #t) (k 0)) (d (n "tar") (r "^0.4.38") (d #t) (k 0)) (d (n "ureq") (r "^2.4.0") (f (quote ("tls" "gzip"))) (d #t) (k 0)) (d (n "wasm-bindgen-cli-support") (r "^0.2.80") (d #t) (k 2)))) (h "0xagaj704n722gl01gl1ynx8wzj5n2aa1mzfr1rjmcw28qghaa82")))

