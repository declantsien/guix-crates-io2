(define-module (crates-io wo rk workerbee) #:use-module (crates-io))

(define-public crate-workerbee-0.1.0 (c (n "workerbee") (v "0.1.0") (d (list (d (n "tokio") (r "^1.24.1") (f (quote ("rt-multi-thread" "rt" "net" "sync"))) (d #t) (k 0)) (d (n "uuid") (r "^1.2.2") (d #t) (k 0)))) (h "1gc9vznpag0y90bjmf1d5yp9wf2c97hignzynqlc9izsz6lpvg8h")))

