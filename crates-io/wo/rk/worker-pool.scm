(define-module (crates-io wo rk worker-pool) #:use-module (crates-io))

(define-public crate-worker-pool-0.1.0 (c (n "worker-pool") (v "0.1.0") (h "126dssasiw3lqg4lzkdy47v1sdampxhrycqj0kphq0klhwpz5dr1")))

(define-public crate-worker-pool-0.2.0 (c (n "worker-pool") (v "0.2.0") (h "0x0mva261vaaac2p1klafpy9znq0jkgnvb7rmsbx8svj8v22j8rl")))

(define-public crate-worker-pool-0.2.1 (c (n "worker-pool") (v "0.2.1") (h "1wcskl13kjhl40xaca57lwnl57s8f0rxxwp7vmiwdjyvgj3k0i5f")))

