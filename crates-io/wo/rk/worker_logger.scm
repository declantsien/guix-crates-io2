(define-module (crates-io wo rk worker_logger) #:use-module (crates-io))

(define-public crate-worker_logger-0.1.0 (c (n "worker_logger") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "humantime") (r "^2.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "worker") (r "^0.0") (d #t) (k 0)))) (h "14p00hil9c6vpjrs33qa4n839lhcjcg4b7ayw0l17yj5gdy5zq09") (y #t)))

(define-public crate-worker_logger-0.1.1 (c (n "worker_logger") (v "0.1.1") (d (list (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "humantime") (r "^2.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "worker") (r "^0.0") (d #t) (k 0)))) (h "1dnkl4b58r5p2cyl2i07pw4p2596969v0marwszkhsdybwh5fblh") (y #t)))

(define-public crate-worker_logger-0.1.2 (c (n "worker_logger") (v "0.1.2") (d (list (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "worker") (r "^0.0") (d #t) (k 0)))) (h "07sv9ggaj36yakspdv91kyilgq52r2jd4xkzi4rz5hqyppi6pdmg")))

(define-public crate-worker_logger-0.2.0 (c (n "worker_logger") (v "0.2.0") (d (list (d (n "colored") (r "^2.0") (o #t) (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "worker") (r "^0.0") (d #t) (k 0)))) (h "0c9lyjra5nbsvq05wm3x6ba9llsln8q8s580h5fkzxa4kn5yxsiz") (f (quote (("env_logger_string" "env_logger") ("default") ("color" "colored"))))))

