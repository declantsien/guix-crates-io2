(define-module (crates-io wo rk workpool) #:use-module (crates-io))

(define-public crate-workpool-0.1.0 (c (n "workpool") (v "0.1.0") (d (list (d (n "crossbeam-channel") (r "^0.2") (d #t) (k 0)) (d (n "published_value") (r "^0.1.1") (d #t) (k 0)))) (h "0km8v6p5imcjq5ml3ydpkyrv91zf6inv7f7bh6w380s6hkhdr3gs")))

(define-public crate-workpool-0.1.1 (c (n "workpool") (v "0.1.1") (d (list (d (n "crossbeam-channel") (r "^0.2") (d #t) (k 0)) (d (n "published_value") (r "^0.1.2") (d #t) (k 0)))) (h "1380d1hxbgwr9v8sqg4k03pv98k02m2k2j3jcf5jly02dbsnjh0v")))

(define-public crate-workpool-0.2.1 (c (n "workpool") (v "0.2.1") (d (list (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "published_value") (r "^0.1.2") (d #t) (k 0)))) (h "07ndpk0pfzfdglz6hkb81drk8dygnd38y7zpyz2sl79y63ck5vg0")))

