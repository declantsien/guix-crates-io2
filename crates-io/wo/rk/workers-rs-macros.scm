(define-module (crates-io wo rk workers-rs-macros) #:use-module (crates-io))

(define-public crate-workers-rs-macros-0.0.1 (c (n "workers-rs-macros") (v "0.0.1") (d (list (d (n "async-trait") (r "^0.1.51") (d #t) (k 0)) (d (n "edgeworker-sys") (r "^0.0.1") (d #t) (k 0)) (d (n "libworker") (r "^0.0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.28") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.74") (d #t) (k 0)) (d (n "wasm-bindgen") (r "=0.2.76") (d #t) (k 0)) (d (n "wasm-bindgen-futures") (r "^0.4.24") (d #t) (k 0)) (d (n "wasm-bindgen-macro-support") (r "^0.2.76") (d #t) (k 0)))) (h "0dncba83n1k6hbargfg6xhbz5fdr7lhav7fal34qnc5c3aa31kq3") (y #t)))

