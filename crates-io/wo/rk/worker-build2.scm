(define-module (crates-io wo rk worker-build2) #:use-module (crates-io))

(define-public crate-worker-build2-0.0.8 (c (n "worker-build2") (v "0.0.8") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "dirs-next") (r "^2.0.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0.24") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)) (d (n "tar") (r "^0.4.38") (d #t) (k 0)) (d (n "ureq") (r "^2.4.0") (f (quote ("tls" "gzip"))) (d #t) (k 0)) (d (n "wasm-bindgen-cli-support") (r "^0.2.80") (d #t) (k 2)))) (h "010raz6asczaj2bfi4l9w8ipbfsaks583f5f32p66jjvwilng05w")))

(define-public crate-worker-build2-0.0.9 (c (n "worker-build2") (v "0.0.9") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "dirs-next") (r "^2.0.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0.24") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)) (d (n "tar") (r "^0.4.38") (d #t) (k 0)) (d (n "ureq") (r "^2.4.0") (f (quote ("tls" "gzip"))) (d #t) (k 0)) (d (n "wasm-bindgen-cli-support") (r "^0.2.80") (d #t) (k 2)))) (h "14323b76l0ygm0y0ja2n23aah6crsyas5fygm8acv5kv9vgbn8rb")))

(define-public crate-worker-build2-0.0.10 (c (n "worker-build2") (v "0.0.10") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "dirs-next") (r "^2.0.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0.24") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)) (d (n "tar") (r "^0.4.38") (d #t) (k 0)) (d (n "ureq") (r "^2.4.0") (f (quote ("tls" "gzip"))) (d #t) (k 0)) (d (n "wasm-bindgen-cli-support") (r "^0.2.80") (d #t) (k 2)))) (h "0bn48700wyyxni7sjp09xzaywalwc74m74fkp8qbd4pbn8xyxlcp")))

