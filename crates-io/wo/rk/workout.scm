(define-module (crates-io wo rk workout) #:use-module (crates-io))

(define-public crate-workout-0.1.0 (c (n "workout") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rusty_audio") (r "^1.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.21") (d #t) (k 0)) (d (n "terminal-menu") (r "^2.0.0") (d #t) (k 0)) (d (n "termion") (r "^1.5.6") (d #t) (k 0)))) (h "0nialgl9iai72npr0r3ks915d4ihgk3kmb7m0a1dg6g44visbclx")))

(define-public crate-workout-0.1.1 (c (n "workout") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rusty_audio") (r "^1.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.21") (d #t) (k 0)) (d (n "terminal-menu") (r "^2.0.0") (d #t) (k 0)) (d (n "termion") (r "^1.5.6") (d #t) (k 0)))) (h "0z3hvs8yz3nbz78sm7qgdf46lg3xzbvfj1zbm3hvk8mwfm94wnkc")))

(define-public crate-workout-0.1.2 (c (n "workout") (v "0.1.2") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rusty_audio") (r "^1.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.21") (d #t) (k 0)) (d (n "terminal-menu") (r "^2.0.0") (d #t) (k 0)) (d (n "termion") (r "^1.5.6") (d #t) (k 0)))) (h "1g70gdw76dy0rd9rfzfkxnzicj5glpqhrpcfzcfn7slbprs1bw62")))

(define-public crate-workout-0.1.3 (c (n "workout") (v "0.1.3") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rusty_audio") (r "^1.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.21") (d #t) (k 0)) (d (n "terminal-menu") (r "^2.0.0") (d #t) (k 0)) (d (n "termion") (r "^1.5.6") (d #t) (k 0)))) (h "162csxq14c5xs13snwpiadhc6mlkvy0rj2c6bxpj4b7qm1ayhk5j")))

