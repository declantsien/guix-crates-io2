(define-module (crates-io wo rk workers_pool) #:use-module (crates-io))

(define-public crate-workers_pool-0.1.0 (c (n "workers_pool") (v "0.1.0") (d (list (d (n "crossbeam") (r "^0.8.1") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.1") (d #t) (k 0)))) (h "06j184xpxbslf09aygk8cif7qzfya6hwl9xclq3gxc5pbbkyiq7k") (y #t)))

(define-public crate-workers_pool-0.1.1 (c (n "workers_pool") (v "0.1.1") (d (list (d (n "crossbeam") (r "^0.8.1") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.1") (d #t) (k 0)))) (h "1kwl9kc25sh8zkls2fdkgf3m0rv2q90rn6834mhn9a2f8vp4scss")))

