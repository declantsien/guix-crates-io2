(define-module (crates-io wo rk workerpool-rs) #:use-module (crates-io))

(define-public crate-workerpool-rs-0.1.0 (c (n "workerpool-rs") (v "0.1.0") (h "0ps98036ysypwjac1ywh9pvdjz9yw616l75rg86whlg9aqfi2z8v") (y #t)))

(define-public crate-workerpool-rs-0.1.1 (c (n "workerpool-rs") (v "0.1.1") (h "1k2ma5gl64xvdy9dn26c5an1pgz0zw9iflsls4a46yjwx93415db") (y #t)))

(define-public crate-workerpool-rs-0.1.2 (c (n "workerpool-rs") (v "0.1.2") (h "06h6dzal8gizj32jwdz9vxknf26rzjprg7na5d2p0rgn3byg7cfj") (y #t)))

(define-public crate-workerpool-rs-0.1.3 (c (n "workerpool-rs") (v "0.1.3") (h "0cap99a8mn67lcpf96723132kr5b80fpp3mrv99mfh77zfj9hyd6") (y #t)))

(define-public crate-workerpool-rs-0.2.0 (c (n "workerpool-rs") (v "0.2.0") (h "1nw9xz0sl8l5jx4y39ihl4dncsshc2v7wxhnwigjlnzab3jf7c8i")))

(define-public crate-workerpool-rs-0.2.1 (c (n "workerpool-rs") (v "0.2.1") (h "1ys9hbxs9ilrgfcvbf5nsnh17s27m3zw80c2373qjpyw8alq51wj")))

