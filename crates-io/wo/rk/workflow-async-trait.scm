(define-module (crates-io wo rk workflow-async-trait) #:use-module (crates-io))

(define-public crate-workflow-async-trait-0.1.68 (c (n "workflow-async-trait") (v "0.1.68") (d (list (d (n "futures") (r "^0.3.25") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0.96") (f (quote ("full" "visit-mut"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.14") (d #t) (k 2)) (d (n "tracing-attributes") (r "^0.1.14") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)))) (h "18m4dz69jw4l39ijc7zp1hv6i0b4m4w8xgcvqs48isv2frqf3jvs") (r "1.39")))

