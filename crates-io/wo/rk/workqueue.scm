(define-module (crates-io wo rk workqueue) #:use-module (crates-io))

(define-public crate-workqueue-0.1.0 (c (n "workqueue") (v "0.1.0") (d (list (d (n "crossbeam-queue") (r "^0.2.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "tokio") (r "^0.2.18") (f (quote ("rt-core"))) (k 0)))) (h "0dckfmfy1nyw6zr0cn1z6f81xf326xdf4ida79sf48rnmzh999vs")))

(define-public crate-workqueue-0.1.1 (c (n "workqueue") (v "0.1.1") (d (list (d (n "crossbeam-queue") (r "^0.2.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "tokio") (r "^0.2.18") (f (quote ("rt-core"))) (k 0)))) (h "1l6ky3900cmy150pbmlmzzfyma83a2abqz9iby9pc8gxjr7hqgnk")))

