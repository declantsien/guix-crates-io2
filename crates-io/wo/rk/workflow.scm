(define-module (crates-io wo rk workflow) #:use-module (crates-io))

(define-public crate-workflow-0.1.0 (c (n "workflow") (v "0.1.0") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "simple_logger") (r "^1.9.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "0gw8ppdilsqjb1nnl5b4kqyd86pl5zh1248p76kzkah49ky6d29a")))

(define-public crate-workflow-0.2.0 (c (n "workflow") (v "0.2.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "simple_logger") (r "^1.9.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "028lllia7f2mislxqfppd79c9kqidxy5qsfga4k9p172y1zbmjmd")))

(define-public crate-workflow-0.3.0 (c (n "workflow") (v "0.3.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "simple_logger") (r "^1.9.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "02yh1h2h0hhcvxpz311lp5ds3fjqr390f06pmnkc10l1ddxcgw6x")))

