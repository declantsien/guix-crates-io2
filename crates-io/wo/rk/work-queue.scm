(define-module (crates-io wo rk work-queue) #:use-module (crates-io))

(define-public crate-work-queue-0.1.0 (c (n "work-queue") (v "0.1.0") (d (list (d (n "concurrent-queue") (r "^1.2.2") (d #t) (k 0)))) (h "127k30k8k17ahrvsl1iv5fq7w6qr0g8p21q6spr4v24qifz0nij2")))

(define-public crate-work-queue-0.1.1 (c (n "work-queue") (v "0.1.1") (d (list (d (n "concurrent-queue") (r "^1.2.2") (d #t) (k 0)))) (h "12ja7frja2gyfp0wza15khmy1xahizl1y0r143w2hw4l8mq8zykg")))

(define-public crate-work-queue-0.1.2 (c (n "work-queue") (v "0.1.2") (d (list (d (n "array-init") (r "^2.0.0") (d #t) (t "cfg(loom)") (k 2)) (d (n "cobb") (r "^0.0.1") (d #t) (t "cfg(not(loom))") (k 2)) (d (n "concurrent-queue") (r "^1.2.2") (d #t) (t "cfg(not(loom))") (k 0)) (d (n "loom") (r "^0.4.1") (d #t) (t "cfg(loom)") (k 0)))) (h "1dclaig239pr9d7ndz6nji865aqkjxig7nps4b2i7xy48s4gfpkm")))

(define-public crate-work-queue-0.1.3 (c (n "work-queue") (v "0.1.3") (d (list (d (n "array-init") (r "^2.0.0") (d #t) (t "cfg(loom)") (k 2)) (d (n "cobb") (r "^0.0.1") (d #t) (t "cfg(not(loom))") (k 2)) (d (n "concurrent-queue") (r "^1.2.2") (d #t) (t "cfg(not(loom))") (k 0)) (d (n "loom") (r "^0.4.1") (d #t) (t "cfg(loom)") (k 0)))) (h "06djdvz9clyl7ixyp7rn26rxcchkjphgcx0zxy1dvv2v9yjiakgm")))

(define-public crate-work-queue-0.1.4 (c (n "work-queue") (v "0.1.4") (d (list (d (n "array-init") (r "^2.1.0") (d #t) (t "cfg(loom)") (k 2)) (d (n "cobb") (r "^0.0.1") (d #t) (t "cfg(not(loom))") (k 2)) (d (n "concurrent-queue") (r "^2.2.0") (d #t) (t "cfg(not(loom))") (k 0)) (d (n "loom") (r "^0.6.1") (d #t) (t "cfg(loom)") (k 0)))) (h "0gb2vgpgz19iamw7fv70pzd2wnb38ljdbka74fj9srhsn7mdbj7i")))

