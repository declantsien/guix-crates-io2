(define-module (crates-io wo rk worker-ratelimit) #:use-module (crates-io))

(define-public crate-worker-ratelimit-0.1.0 (c (n "worker-ratelimit") (v "0.1.0") (d (list (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)) (d (n "worker") (r "^0.0.21") (o #t) (d #t) (k 0)) (d (n "worker-kv") (r "^0.6.0") (d #t) (k 0)))) (h "0q8w6kml1sg31n2m9shaw4pziiyah8ar118wcr8kr56ndy3n3q45") (f (quote (("worker-sdk" "worker") ("default" "worker-sdk"))))))

(define-public crate-worker-ratelimit-0.2.0 (c (n "worker-ratelimit") (v "0.2.0") (d (list (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)) (d (n "worker") (r "^0.0.23") (o #t) (d #t) (k 0)) (d (n "worker-kv") (r "^0.6.0") (d #t) (k 0)))) (h "0g3j740hadvla6k31dmxfyclhb49z4zydjkw4rbhqhcwc2723zfg") (f (quote (("worker-sdk" "worker") ("default" "worker-sdk"))))))

(define-public crate-worker-ratelimit-0.3.0 (c (n "worker-ratelimit") (v "0.3.0") (d (list (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)) (d (n "worker") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "worker-kv") (r "^0.6.0") (d #t) (k 0)))) (h "0dysrd6amq4pfcbh1qbdb8an20p0adxrwmxyrpg7jl77qf82l66s") (f (quote (("worker-sdk" "worker") ("default" "worker-sdk"))))))

