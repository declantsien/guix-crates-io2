(define-module (crates-io wo rk workflow-i18n) #:use-module (crates-io))

(define-public crate-workflow-i18n-0.0.0 (c (n "workflow-i18n") (v "0.0.0") (h "0zw298nq4dgsg94g689xaivxd07myqkbfc0xvrrpzw12lfcp5ask")))

(define-public crate-workflow-i18n-0.1.0 (c (n "workflow-i18n") (v "0.1.0") (d (list (d (n "ritehash") (r "^0.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.32") (d #t) (k 0)))) (h "173yxx7s55aw0ldkiqhiqx7z6inv4hrzl2s2ygcf2qc4qrjkwsbz")))

(define-public crate-workflow-i18n-0.3.2 (c (n "workflow-i18n") (v "0.3.2") (d (list (d (n "ritehash") (r "^0.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "09p1b984dh51988810d7f3cssavbjasi481w549xcj3a5455c886")))

(define-public crate-workflow-i18n-0.3.6 (c (n "workflow-i18n") (v "0.3.6") (d (list (d (n "ritehash") (r "^0.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "1dgbagwgjb0v92f4m5nra813yn8cj0063a55hg49rgiss4j9xgfb")))

(define-public crate-workflow-i18n-0.3.7 (c (n "workflow-i18n") (v "0.3.7") (d (list (d (n "ritehash") (r "^0.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "1vi27waxnpq81v2yrxpm14cxf2lcv8vdibbxkza2r4zxmxgld5kn")))

(define-public crate-workflow-i18n-0.3.10 (c (n "workflow-i18n") (v "0.3.10") (d (list (d (n "ritehash") (r "^0.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "1dqi3p8jq2j8v58fzb4kims2j2gb13b3wxbm4k23b2rmq0b2yzdc")))

(define-public crate-workflow-i18n-0.3.12 (c (n "workflow-i18n") (v "0.3.12") (d (list (d (n "ritehash") (r "^0.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "0cyrbp90xwvmrzfsrqwllnyllx8m9ainwiy19ady060dfqwjwy0f")))

(define-public crate-workflow-i18n-0.3.14 (c (n "workflow-i18n") (v "0.3.14") (d (list (d (n "ritehash") (r "^0.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "0z8695qnnmz14mnbpjxz83z1r47sp3vl4vs299kabhyzra210d2z")))

(define-public crate-workflow-i18n-0.3.15 (c (n "workflow-i18n") (v "0.3.15") (d (list (d (n "ritehash") (r "^0.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "1bsfkdjrcyz8n8x0ri5d5ppn6lx0qzszb1ncyb4ppsjy8bhak0i6")))

(define-public crate-workflow-i18n-0.3.16 (c (n "workflow-i18n") (v "0.3.16") (d (list (d (n "ritehash") (r "^0.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "0na2x45p29lx9nlh6ccdjhwz30ykbfh671zpfyqv4xwpfai14vn3")))

(define-public crate-workflow-i18n-0.3.17 (c (n "workflow-i18n") (v "0.3.17") (d (list (d (n "ritehash") (r "^0.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "178xsz6wkzvdgz88nbd0v09irddsd4sa0rpykmnhx9dh5npmmvjx")))

(define-public crate-workflow-i18n-0.4.0 (c (n "workflow-i18n") (v "0.4.0") (d (list (d (n "ritehash") (r "^0.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.43") (d #t) (k 0)))) (h "1qyy0lrl6ck61z39xiw3mphnbrgr51vbk2n23b8pbclhs0a69gf9")))

(define-public crate-workflow-i18n-0.5.0 (c (n "workflow-i18n") (v "0.5.0") (d (list (d (n "ritehash") (r "^0.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.43") (d #t) (k 0)))) (h "19b5ayr35mx72m43chv4f6wrkpx9ak3wbbgf3qdlyxghj56x1gyd")))

(define-public crate-workflow-i18n-0.5.3 (c (n "workflow-i18n") (v "0.5.3") (d (list (d (n "ritehash") (r "^0.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.43") (d #t) (k 0)))) (h "1ddshbg52hz1bs9a69pf6ds3klr3mcbi49gm583ixpsqhvw5c5kp")))

(define-public crate-workflow-i18n-0.5.4 (c (n "workflow-i18n") (v "0.5.4") (d (list (d (n "ritehash") (r "^0.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.47") (d #t) (k 0)))) (h "021xfnmga4f7gr3alkr3v50dq43b2l36hhshs6sfw15mjawdxx74")))

(define-public crate-workflow-i18n-0.6.0 (c (n "workflow-i18n") (v "0.6.0") (d (list (d (n "ritehash") (r "^0.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.47") (d #t) (k 0)))) (h "0rkj7igf6jaw9y27nvpjpbn6f0vsr3ilkypp7xzhxns6mlhm29h9")))

(define-public crate-workflow-i18n-0.7.0 (c (n "workflow-i18n") (v "0.7.0") (d (list (d (n "ritehash") (r "^0.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.47") (d #t) (k 0)))) (h "1xx5idgnx6bm32m71jsggaiwdml71qr90fsfznrswqzch477ccsi")))

(define-public crate-workflow-i18n-0.8.0 (c (n "workflow-i18n") (v "0.8.0") (d (list (d (n "ritehash") (r "^0.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.47") (d #t) (k 0)))) (h "0jgrpzz9ickkkmk2pjm8kv3hyllwjqm13wqkq9mm97da1acsq6qw")))

(define-public crate-workflow-i18n-0.8.1 (c (n "workflow-i18n") (v "0.8.1") (d (list (d (n "ritehash") (r "^0.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "0li3hpsbw6zgy35z5q4bpscjhcl75i2w5vsci99jyzbk8h4jbc4a")))

(define-public crate-workflow-i18n-0.10.0 (c (n "workflow-i18n") (v "0.10.0") (d (list (d (n "arc-swap") (r "^1.6.0") (d #t) (k 0)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "ritehash") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.190") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "1wx2236jr9p631v5s5x2sgpksxl6p7cahki79qlm0yx3h15z6g3n") (f (quote (("thread-safe") ("preserve-order" "serde_json/preserve_order") ("default"))))))

(define-public crate-workflow-i18n-0.10.1 (c (n "workflow-i18n") (v "0.10.1") (d (list (d (n "arc-swap") (r "^1.6.0") (d #t) (k 0)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "ritehash") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.190") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "16w3xav0sssi6p3341srhd0rdr3hpcn05sh12319l2v1s1vbhf9s") (f (quote (("thread-safe") ("preserve-order" "serde_json/preserve_order") ("default"))))))

(define-public crate-workflow-i18n-0.10.2 (c (n "workflow-i18n") (v "0.10.2") (d (list (d (n "arc-swap") (r "^1.6.0") (d #t) (k 0)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "ritehash") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.190") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "169lyqxqmsgzqsqpfw7nzc4gj42hgzisc2pr26alzwb7psvnjk9q") (f (quote (("thread-safe") ("preserve-order" "serde_json/preserve_order") ("default"))))))

(define-public crate-workflow-i18n-0.10.3 (c (n "workflow-i18n") (v "0.10.3") (d (list (d (n "arc-swap") (r "^1.6.0") (d #t) (k 0)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "ritehash") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.190") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "1z1599agiq9mr36ykbxnxlhfwxg1gv8azg50rg6d6lr0kvar0xp5") (f (quote (("thread-safe") ("preserve-order" "serde_json/preserve_order") ("default"))))))

(define-public crate-workflow-i18n-0.11.0 (c (n "workflow-i18n") (v "0.11.0") (d (list (d (n "arc-swap") (r "^1.6.0") (d #t) (k 0)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "ritehash") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.190") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "0iqka49vsvsz5p5m1hkhcvmsbziwk6c275dpxshbnd6x6hgkd3c0") (f (quote (("thread-safe") ("default"))))))

(define-public crate-workflow-i18n-0.11.1 (c (n "workflow-i18n") (v "0.11.1") (d (list (d (n "arc-swap") (r "^1.6.0") (d #t) (k 0)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "ritehash") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.190") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "0s561lb8017pmd673gac6wx8qm8x51z1wqm78ljnpmgizwfnibyg") (f (quote (("thread-safe") ("default"))))))

(define-public crate-workflow-i18n-0.12.0 (c (n "workflow-i18n") (v "0.12.0") (d (list (d (n "arc-swap") (r "^1.6.0") (d #t) (k 0)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "ritehash") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.190") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "0g9xfn4hhfzsn5w2gr7iyin47b4i4m02010gqgnc7wbgr03jv6zb") (f (quote (("thread-safe") ("default"))))))

(define-public crate-workflow-i18n-0.12.1 (c (n "workflow-i18n") (v "0.12.1") (d (list (d (n "arc-swap") (r "^1.6.0") (d #t) (k 0)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "ritehash") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.190") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "153swhpqn7d1iwc9vzfyxjd96j6ciai0g9s5b3kzi27sp9y2xdky") (f (quote (("thread-safe") ("default"))))))

