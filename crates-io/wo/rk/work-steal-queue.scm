(define-module (crates-io wo rk work-steal-queue) #:use-module (crates-io))

(define-public crate-work-steal-queue-0.1.0 (c (n "work-steal-queue") (v "0.1.0") (d (list (d (n "cfg-if") (r "^1") (d #t) (k 0)) (d (n "crossbeam-epoch") (r "^0.9.13") (o #t) (k 0)) (d (n "crossbeam-utils") (r "^0.8.14") (o #t) (k 0)) (d (n "num_cpus") (r "^1.14.0") (d #t) (k 0)) (d (n "timer-utils") (r "^0.0.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1gff0xh00nx1wqgd80pw9k54c9c43c6d99p8l7vc0sm8jl0kmqcn") (f (quote (("std" "crossbeam-epoch/std" "crossbeam-utils/std") ("default" "std"))))))

(define-public crate-work-steal-queue-0.1.1 (c (n "work-steal-queue") (v "0.1.1") (d (list (d (n "crossbeam-deque") (r "^0.8.2") (d #t) (k 0)) (d (n "num_cpus") (r "^1.14.0") (d #t) (k 0)) (d (n "st3") (r "^0.4.1") (d #t) (k 0)))) (h "1w8sx9ihdbi298n9wnb6plk1rvycgichpbj1gm5vq86qq4flqb2y")))

(define-public crate-work-steal-queue-0.1.2 (c (n "work-steal-queue") (v "0.1.2") (d (list (d (n "crossbeam-deque") (r "^0.8.2") (d #t) (k 0)) (d (n "num_cpus") (r "^1.14.0") (d #t) (k 0)) (d (n "st3") (r "^0.4.1") (d #t) (k 0)))) (h "1bl1ihcypkjzx1gi3hi9ym0jpg4nk5mfww8a17z9v2f1rxxq4v4n")))

