(define-module (crates-io wo rk worker-codegen) #:use-module (crates-io))

(define-public crate-worker-codegen-0.1.0 (c (n "worker-codegen") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "prettyplease") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.60") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.17") (f (quote ("extra-traits"))) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.91") (d #t) (k 0)) (d (n "wasm-bindgen-futures") (r "^0.4.39") (d #t) (k 0)) (d (n "wasm-bindgen-macro-support") (r "^0.2.91") (d #t) (k 0)) (d (n "wit-parser") (r "^0.205") (d #t) (k 0)))) (h "1mm982x1sg1cg70mdwn6k1kqc0ilaamma8y389b59c5zqgfk12hf")))

