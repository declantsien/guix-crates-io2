(define-module (crates-io wo rk workerpool) #:use-module (crates-io))

(define-public crate-workerpool-1.0.0 (c (n "workerpool") (v "1.0.0") (d (list (d (n "num_cpus") (r "^1.6") (d #t) (k 0)))) (h "0yvbz7cswkfncr6q81j1yi0vnsr6rcmdazxxly2336l6p8dd69mk")))

(define-public crate-workerpool-1.0.1 (c (n "workerpool") (v "1.0.1") (d (list (d (n "num_cpus") (r "^1.7") (d #t) (k 0)))) (h "17jpk7ihh6b5iz7b27512hg5jw5iqgn6n25b719s0hkby6li4w5f")))

(define-public crate-workerpool-1.0.2 (c (n "workerpool") (v "1.0.2") (d (list (d (n "num_cpus") (r "^1.7") (d #t) (k 0)))) (h "14ahbv5nv2dlqic82p1v6hmjj5c6qfi8nw0bg91nl26hphf6lqzf")))

(define-public crate-workerpool-1.0.3 (c (n "workerpool") (v "1.0.3") (d (list (d (n "num_cpus") (r "^1.7") (d #t) (k 0)))) (h "1hnrdrsbgpqn83xqwj8jp2hrl9qg394i3smxykmwyv96pg1sr0nb")))

(define-public crate-workerpool-1.0.4 (c (n "workerpool") (v "1.0.4") (d (list (d (n "num_cpus") (r "^1.7") (d #t) (k 0)))) (h "1k15nckgkrlwrhxkjkin24qs2cmkm29444wbc2i05pb4zxsr8khw")))

(define-public crate-workerpool-1.1.0 (c (n "workerpool") (v "1.1.0") (d (list (d (n "num_cpus") (r "^1.7") (d #t) (k 0)))) (h "1hd74pixhj7szb499ccaj4k72v0ymji1x0fqmwfhx9495zp71191")))

(define-public crate-workerpool-1.1.1 (c (n "workerpool") (v "1.1.1") (d (list (d (n "num_cpus") (r "^1.7") (d #t) (k 0)))) (h "09f6m3x1vhrdnry0vjchgb8ymxy0lp7p1cwmzwcxwyv18rk7ajag")))

(define-public crate-workerpool-1.2.0 (c (n "workerpool") (v "1.2.0") (d (list (d (n "crossbeam-channel") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "num_cpus") (r "^1.7") (d #t) (k 0)) (d (n "parking_lot") (r "^0.7") (d #t) (k 0)))) (h "0p2hcj983f5pdzxa8hir7701yq4xibg9r6v72j8va37mrlknci6d") (f (quote (("crossbeam" "crossbeam-channel"))))))

(define-public crate-workerpool-1.2.1 (c (n "workerpool") (v "1.2.1") (d (list (d (n "crossbeam-channel") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "num_cpus") (r "^1.7") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)))) (h "1ls34mlc9cyxlicc4l1ip63xc16sajvmsmkcix7j501pfm6c1b17") (f (quote (("crossbeam" "crossbeam-channel")))) (r "1.60")))

