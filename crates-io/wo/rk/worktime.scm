(define-module (crates-io wo rk worktime) #:use-module (crates-io))

(define-public crate-worktime-0.1.0 (c (n "worktime") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.26.1") (d #t) (k 0)))) (h "139kn128dvs8adz42mwh3gk7v2bhglwsydwg0il9sblilv0x8csb")))

(define-public crate-worktime-0.1.1 (c (n "worktime") (v "0.1.1") (d (list (d (n "crossterm") (r "^0.26.1") (d #t) (k 0)))) (h "1rymarxh417cglggm2pgmzpf7dr75a5lxqlilalqs007wqllai03")))

