(define-module (crates-io wo rk work) #:use-module (crates-io))

(define-public crate-work-0.2.0 (c (n "work") (v "0.2.0") (d (list (d (n "clap") (r "^3.0.0-beta.1") (d #t) (k 0)) (d (n "confy") (r "^0.4.0") (d #t) (k 0)) (d (n "rusqlite") (r "^0.23.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "skim") (r "^0.8.2") (d #t) (k 0)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)))) (h "0g3839gb5wq2gh4dd6njz5f92pzy6wnandprlzj7p08k92j3hlkj")))

(define-public crate-work-0.3.0 (c (n "work") (v "0.3.0") (d (list (d (n "clap") (r "^3.0.0-beta.1") (d #t) (k 0)) (d (n "confy") (r "^0.4.0") (d #t) (k 0)) (d (n "rusqlite") (r "^0.23.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "skim") (r "^0.8.2") (d #t) (k 0)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)))) (h "0xvs02kc8rjvjnnl918hwr423ppv0kd4rvxi003696kwsl634b9g")))

(define-public crate-work-0.3.1 (c (n "work") (v "0.3.1") (d (list (d (n "clap") (r "^3.0.0-beta.1") (d #t) (k 0)) (d (n "confy") (r "^0.4.0") (d #t) (k 0)) (d (n "rusqlite") (r "^0.23.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "skim") (r "^0.8.2") (d #t) (k 0)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)))) (h "0805fc5rv6rxahsgf7s9qmzzm768yfb4nqxgc5g8d0q3j6pk01yw")))

(define-public crate-work-0.4.1 (c (n "work") (v "0.4.1") (d (list (d (n "clap") (r "^3.0.0-beta.1") (d #t) (k 0)) (d (n "confy") (r "^0.4.0") (d #t) (k 0)) (d (n "rusqlite") (r "^0.23.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "skim") (r "^0.8.2") (d #t) (k 0)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)))) (h "03j05dz866qbafjgk6m9bpcy564hafy8nshfq8xkdbzis5v39r43")))

(define-public crate-work-0.4.2 (c (n "work") (v "0.4.2") (d (list (d (n "clap") (r "^3.0.0-beta.1") (d #t) (k 0)) (d (n "confy") (r "^0.4.0") (d #t) (k 0)) (d (n "rusqlite") (r "^0.23.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "skim") (r "^0.8.2") (d #t) (k 0)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)))) (h "1bb30qxrdk6akgvgs0lbv7ilab142yn43fyh4q94dffib9bk46y1")))

