(define-module (crates-io wo rk workspaces-rs) #:use-module (crates-io))

(define-public crate-workspaces-rs-0.1.0 (c (n "workspaces-rs") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "dialoguer") (r "^0.11.0") (f (quote ("fuzzy-select"))) (d #t) (k 0)) (d (n "directories") (r "^0.4.0") (d #t) (k 0)))) (h "1c8iy6hh6h6pimqb4p4r253yi8sklczn1dyg1ajqb7sdn00mnj6k")))

(define-public crate-workspaces-rs-0.1.1 (c (n "workspaces-rs") (v "0.1.1") (d (list (d (n "clap") (r "^4.4.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "dialoguer") (r "^0.11.0") (f (quote ("fuzzy-select"))) (d #t) (k 0)) (d (n "directories") (r "^0.4.0") (d #t) (k 0)))) (h "0mys8wyw4yzvmfsf92j26g67rcvwwvd65qxhimbh45pmj52ab290")))

