(define-module (crates-io wo rk workflow-testing) #:use-module (crates-io))

(define-public crate-workflow-testing-0.1.1-alpha.1 (c (n "workflow-testing") (v "0.1.1-alpha.1") (h "03ng8r40dwrgvvmdhacqadsa1ff0vykaw54b88kkqg7xpcvcrw2m") (y #t)))

(define-public crate-workflow-testing-0.1.0 (c (n "workflow-testing") (v "0.1.0") (h "024f8yj8kl9n6r9f8c5ndp697m0a1l0sg3wn15yraknbhy8zla9v") (y #t)))

