(define-module (crates-io wo rk working_dir) #:use-module (crates-io))

(define-public crate-working_dir-0.1.0 (c (n "working_dir") (v "0.1.0") (d (list (d (n "path_no_alloc") (r "^0.1.1") (d #t) (k 0)))) (h "125ygq0hw07pahb02w55faib5hvk983cinjn4ag4d7xsn2ir232a")))

(define-public crate-working_dir-0.1.1 (c (n "working_dir") (v "0.1.1") (d (list (d (n "path_no_alloc") (r "^0.1.1") (d #t) (k 0)))) (h "0fb23wvrq91l48hgznvv5g0x925j8f93xwh50g1p7z6c73ba1vg9")))

