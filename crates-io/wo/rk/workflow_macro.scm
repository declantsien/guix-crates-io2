(define-module (crates-io wo rk workflow_macro) #:use-module (crates-io))

(define-public crate-workflow_macro-0.0.1 (c (n "workflow_macro") (v "0.0.1") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.95") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1ycidhwqc43v3kp4w9brz6qdka3hbnm4x910j38q1wg48y77h19b")))

(define-public crate-workflow_macro-0.0.2 (c (n "workflow_macro") (v "0.0.2") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.95") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0291zpv9k83lf27z5vkbx56m37pwk9f5p2yfmqbvm1jwgqm3w52i")))

(define-public crate-workflow_macro-0.0.3 (c (n "workflow_macro") (v "0.0.3") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.95") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0kv6454f8v9njm8jhbgd2pd2ahnw9rys0jglnscnb9wn71c8x58z")))

