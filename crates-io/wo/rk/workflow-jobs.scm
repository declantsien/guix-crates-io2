(define-module (crates-io wo rk workflow-jobs) #:use-module (crates-io))

(define-public crate-workflow-jobs-0.1.0 (c (n "workflow-jobs") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)))) (h "1cgpzhaqw431sc11zaba1r9rmxn7zp8ap0ly0n4hkl5hs28ra56r")))

(define-public crate-workflow-jobs-0.1.1 (c (n "workflow-jobs") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)))) (h "13lc0m300al6ns7rzp8ipq5lyqsx4z2s7g8n2g2kld25n8c044j3")))

(define-public crate-workflow-jobs-0.1.2 (c (n "workflow-jobs") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)))) (h "0kcpbmsmzw4aj3hsxjzfwd1g8c17qgdqhzvs9n4d1nj2a9hf0kps")))

