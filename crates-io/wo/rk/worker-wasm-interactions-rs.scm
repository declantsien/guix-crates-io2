(define-module (crates-io wo rk worker-wasm-interactions-rs) #:use-module (crates-io))

(define-public crate-worker-wasm-interactions-rs-0.0.1 (c (n "worker-wasm-interactions-rs") (v "0.0.1") (d (list (d (n "ed25519-dalek") (r "^1.0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3.24") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.12") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.86") (d #t) (k 0)) (d (n "twilight-model") (r "^0.13.5") (d #t) (k 0)) (d (n "worker") (r "^0.0.9") (d #t) (k 0)))) (h "0l57qrgdwif1ghz23svhncb9v37g8yqw8gv1kycwiyd3rhyjwbx6")))

