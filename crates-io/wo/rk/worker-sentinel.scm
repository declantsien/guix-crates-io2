(define-module (crates-io wo rk worker-sentinel) #:use-module (crates-io))

(define-public crate-worker-sentinel-0.3.0 (c (n "worker-sentinel") (v "0.3.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "094dihy4c47hygliaww9nhajaj4gk3pk3mvlsapry31hnn65r5iy")))

(define-public crate-worker-sentinel-0.3.1 (c (n "worker-sentinel") (v "0.3.1") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "0cnvrfyabbgc21s8yw32a4874lv8wr766xy0qxhlmf508ji985dv")))

