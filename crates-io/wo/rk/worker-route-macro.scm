(define-module (crates-io wo rk worker-route-macro) #:use-module (crates-io))

(define-public crate-worker-route-macro-0.0.1 (c (n "worker-route-macro") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "1x02xr797r7da3bsc9p3731hblbyyi15qxyj4f65swqmsza07s5a")))

(define-public crate-worker-route-macro-0.0.2 (c (n "worker-route-macro") (v "0.0.2") (d (list (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "0p5pv1zapacvz9s5rjvnns4kp8k0iis9s4y0hz6w5qz20xb0rviq")))

(define-public crate-worker-route-macro-0.0.3 (c (n "worker-route-macro") (v "0.0.3") (d (list (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "0cglzxm30kshv0420qnc4w5j3wnlgx35fs8d8zs1ind4hz7lb58x")))

