(define-module (crates-io wo rk worker-build) #:use-module (crates-io))

(define-public crate-worker-build-0.0.1 (c (n "worker-build") (v "0.0.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)))) (h "15krmhi6hjd7sifipzywmszns3svc6a4rm1054m5apyiymiv9q6y")))

(define-public crate-worker-build-0.0.2 (c (n "worker-build") (v "0.0.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)))) (h "04662s4hizl9i0kshyqbwgyhdyg33a99jp4gvnf35gvhh8r5iwjx")))

(define-public crate-worker-build-0.0.3 (c (n "worker-build") (v "0.0.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)))) (h "150g4aw4a5k80xvdcn8pi2cvw8fqc6xrfi3b9wihhja3nmvy2sxr")))

(define-public crate-worker-build-0.0.4 (c (n "worker-build") (v "0.0.4") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)))) (h "1lmv0cj77z4akjjpd67kpbm4r2r2cnydr9dby5kwgdc511c9fbwg")))

(define-public crate-worker-build-0.0.5 (c (n "worker-build") (v "0.0.5") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "wasm-bindgen-cli-support") (r "=0.2.78") (d #t) (k 2)))) (h "1xnpzvynxxivs9hw5p21zdwjywqlkwwyxdgjl3n61r210j6d1r6j")))

(define-public crate-worker-build-0.0.6 (c (n "worker-build") (v "0.0.6") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "swc") (r "^0.186.0") (d #t) (k 0)) (d (n "swc_bundler") (r "^0.151.0") (d #t) (k 0)) (d (n "swc_common") (r "^0.18.8") (d #t) (k 0)) (d (n "swc_ecma_codegen") (r "^0.109.1") (d #t) (k 0)) (d (n "swc_ecma_loader") (r "^0.30.2") (d #t) (k 0)) (d (n "swc_ecma_parser") (r "^0.105.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)) (d (n "wasm-bindgen-cli-support") (r "^0.2.80") (d #t) (k 2)))) (h "0fsc2nfmnbzxqhh3xi3fr6770g27c00qsrfbppxiqv53vfq64lyn")))

(define-public crate-worker-build-0.0.7 (c (n "worker-build") (v "0.0.7") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "dirs-next") (r "^2.0.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0.24") (d #t) (k 0)) (d (n "tar") (r "^0.4.38") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)) (d (n "ureq") (r "^2.4.0") (f (quote ("tls" "gzip"))) (d #t) (k 0)) (d (n "wasm-bindgen-cli-support") (r "^0.2.80") (d #t) (k 2)))) (h "0hny22dhbc9k7nfypq8aan390rggnq8v6dfgnipgzsdx7960agbq")))

(define-public crate-worker-build-0.0.8 (c (n "worker-build") (v "0.0.8") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "dirs-next") (r "^2.0.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0.24") (d #t) (k 0)) (d (n "tar") (r "^0.4.38") (d #t) (k 0)) (d (n "ureq") (r "^2.4.0") (f (quote ("tls" "gzip"))) (d #t) (k 0)) (d (n "wasm-bindgen-cli-support") (r "^0.2.80") (d #t) (k 2)))) (h "09gfm4v01v6ckgvxj2xqhaky4m9hfbaayba598jr0l4c14qdc644")))

(define-public crate-worker-build-0.0.9 (c (n "worker-build") (v "0.0.9") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "dirs-next") (r "^2.0.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0.24") (d #t) (k 0)) (d (n "tar") (r "^0.4.38") (d #t) (k 0)) (d (n "ureq") (r "^2.4.0") (f (quote ("tls" "gzip"))) (d #t) (k 0)) (d (n "wasm-bindgen-cli-support") (r "^0.2.80") (d #t) (k 2)))) (h "0npwq8ffvfm6di8l3615m3f1aj0rlnjy6z1qi10m70any852f8bh")))

(define-public crate-worker-build-0.0.10 (c (n "worker-build") (v "0.0.10") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "dirs-next") (r "^2.0.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0.26") (d #t) (k 0)) (d (n "tar") (r "^0.4.38") (d #t) (k 0)) (d (n "ureq") (r "^2.6.2") (f (quote ("tls" "gzip"))) (d #t) (k 0)) (d (n "wasm-bindgen-cli-support") (r "^0.2.86") (d #t) (k 2)))) (h "0aildsgm5clngv9fyp4pm2ahndjkxcyg1j5blmsfgvwzsvwkifb9")))

(define-public crate-worker-build-0.1.0 (c (n "worker-build") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "clap") (r "^4.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs-next") (r "^2.0.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0.26") (d #t) (k 0)) (d (n "tar") (r "^0.4.38") (d #t) (k 0)) (d (n "ureq") (r "^2.6.2") (f (quote ("tls" "gzip"))) (d #t) (k 0)) (d (n "wasm-bindgen-cli-support") (r "^0.2.91") (d #t) (k 2)) (d (n "worker-codegen") (r "^0.1.0") (d #t) (k 0)))) (h "0b2xwazddbvzd4sksn47bkx7sx9l2kyjgbrjjly7yrj14ps9763j")))

