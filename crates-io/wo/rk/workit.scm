(define-module (crates-io wo rk workit) #:use-module (crates-io))

(define-public crate-workit-0.2.4 (c (n "workit") (v "0.2.4") (d (list (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)))) (h "052f53r6ahb8yp9lks40aacij1hyk2bpxzhv8iq1af54cy396i7s")))

(define-public crate-workit-0.2.5 (c (n "workit") (v "0.2.5") (d (list (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)))) (h "0ncbblxrwwq9prqwjyshzi2riklrd3r3j1rnm8kjawjpahn1nkvv")))

