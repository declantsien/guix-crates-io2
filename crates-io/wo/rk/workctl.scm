(define-module (crates-io wo rk workctl) #:use-module (crates-io))

(define-public crate-workctl-0.1.0 (c (n "workctl") (v "0.1.0") (h "042nlvp1hxgxc172y92baha564axi1lcg1c8scv3l948agzvpwd2")))

(define-public crate-workctl-0.1.1 (c (n "workctl") (v "0.1.1") (h "13ad9fv233sas1d5prlcsr7fakl1yvdhzh93y317xqv6rgmfrnfy")))

(define-public crate-workctl-0.2.0 (c (n "workctl") (v "0.2.0") (h "0103n3g5d0wnfl16sfq1mihwnznl073ma1ww0vh26pfrnmmmql2z")))

