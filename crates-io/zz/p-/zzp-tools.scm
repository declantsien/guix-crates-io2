(define-module (crates-io zz p- zzp-tools) #:use-module (crates-io))

(define-public crate-zzp-tools-0.1.0 (c (n "zzp-tools") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "structopt") (r "^0.3.14") (d #t) (k 0)) (d (n "yansi") (r "^0.5.0") (d #t) (k 0)) (d (n "zzp") (r "^0.1.0") (d #t) (k 0)))) (h "031jzggm32ddjrhvi7krz4ybyz9n385pwhwksypsv8djnpcvz0wb")))

