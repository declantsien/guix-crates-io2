(define-module (crates-io zz z_ zzz_lib) #:use-module (crates-io))

(define-public crate-zzz_lib-0.1.0 (c (n "zzz_lib") (v "0.1.0") (h "1wa3bm11i8a4nfc8h060n4rlz4n3bghv014dam4r4064dhp7rbj9")))

(define-public crate-zzz_lib-0.1.1+rc.1 (c (n "zzz_lib") (v "0.1.1+rc.1") (h "0h6xvilz0bbyw0b7anvxv1pa6qqdkhv851igbdljfz7ys9ny93d6")))

(define-public crate-zzz_lib-0.1.1-1 (c (n "zzz_lib") (v "0.1.1-1") (h "1fchcayv1s2ynzrlvai454gc8mwj4i5acvv001dn9x37f037smvc")))

(define-public crate-zzz_lib-0.1.0-1 (c (n "zzz_lib") (v "0.1.0-1") (h "0ksqhww9nbk713s4lla28nyn73vmmll711mx5znqqi40rvlylhil")))

(define-public crate-zzz_lib-0.1.1-2 (c (n "zzz_lib") (v "0.1.1-2") (h "1srxj5kzylfid4wx5a07z7ys1rswi4l1si3iqmvi5ps97mxzwbpq")))

(define-public crate-zzz_lib-0.1.1-rc.1 (c (n "zzz_lib") (v "0.1.1-rc.1") (h "1mpslgvzsrqg3d0jn7y80pnhmakn8911qh7ix28psvsdgh2mp7gq")))

(define-public crate-zzz_lib-0.1.1-rc.2 (c (n "zzz_lib") (v "0.1.1-rc.2") (h "09197x0iisgj6afj51n77xiy94c734g2fdzspqcwjzdbsvv55a16")))

(define-public crate-zzz_lib-0.1.2 (c (n "zzz_lib") (v "0.1.2") (h "13i06mwwji6mqbd2xdnysg9dk5fvsc315b5w3ppx4nlhn45l3xd9")))

(define-public crate-zzz_lib-0.1.3+refact (c (n "zzz_lib") (v "0.1.3+refact") (h "1yx7zfwd864skx44c4mj2fjr34pc6zbp0fa7f42lqani3dzslwmi")))

