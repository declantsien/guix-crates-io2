(define-module (crates-io xl sx xlsx2json) #:use-module (crates-io))

(define-public crate-xlsx2json-0.0.1 (c (n "xlsx2json") (v "0.0.1") (d (list (d (n "calamine") (r "^0.16.1") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.52") (d #t) (k 0)))) (h "1d670a231zx77a24jwm97pnhivd4qq10ga9v0f66gqfnd4wlfygx")))

(define-public crate-xlsx2json-0.0.2 (c (n "xlsx2json") (v "0.0.2") (d (list (d (n "calamine") (r "^0.16.1") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.52") (d #t) (k 0)))) (h "0c1sjpnmx5zc8hkiyjl1ylw5fskbimbwnbiphq0sikw7cms1sn2g")))

