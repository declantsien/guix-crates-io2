(define-module (crates-io xl sx xlsx_group_write_macro_derive) #:use-module (crates-io))

(define-public crate-xlsx_group_write_macro_derive-0.1.0 (c (n "xlsx_group_write_macro_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "16x1bib5c5i2rysfx85hg2k60k6vwaivjhwn0m7id8s52ilrgf9r")))

(define-public crate-xlsx_group_write_macro_derive-0.2.0 (c (n "xlsx_group_write_macro_derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1gz3n69cz3ymw7wrjyz5ds3bfpqlx8v28bf5srhkr35a95zjfx24")))

(define-public crate-xlsx_group_write_macro_derive-0.2.1 (c (n "xlsx_group_write_macro_derive") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "11h0b204l4phwb7zwaswmp6l8388840r0v3zqkdzcglk07frphzg") (f (quote (("default") ("date"))))))

(define-public crate-xlsx_group_write_macro_derive-0.3.0 (c (n "xlsx_group_write_macro_derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "02djbj0qflvvsf3dffc65m0f3wraaynz25m5gfss3jav6vlcs261") (f (quote (("default") ("date"))))))

(define-public crate-xlsx_group_write_macro_derive-0.3.1 (c (n "xlsx_group_write_macro_derive") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0ja6rj5ppdhpyyaqc9ypqd8b9pxsg68ln45fpgmdf41izx09mzvs") (f (quote (("default") ("date"))))))

(define-public crate-xlsx_group_write_macro_derive-0.4.0 (c (n "xlsx_group_write_macro_derive") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0x74nqj45x2w831l7vyyap1z6rzdyz56is77kk6mh8msz30wdplv") (f (quote (("default") ("date"))))))

(define-public crate-xlsx_group_write_macro_derive-0.4.1 (c (n "xlsx_group_write_macro_derive") (v "0.4.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "03qgk1sk405c3zn16ihgwzg8bgz9sdh9l51wh0rqmnm9sp488dns") (f (quote (("default") ("date"))))))

(define-public crate-xlsx_group_write_macro_derive-0.4.2 (c (n "xlsx_group_write_macro_derive") (v "0.4.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1gppzk7qc7aac0sw2w2vzmhgsnml2jppq80dsgigjazib2w937bd") (f (quote (("default") ("date"))))))

(define-public crate-xlsx_group_write_macro_derive-0.4.3 (c (n "xlsx_group_write_macro_derive") (v "0.4.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "15g8scng4dnaxcyyp8fhr27x93pnrf7djgq700wm8qq2kvi4g3d5") (f (quote (("default") ("date"))))))

