(define-module (crates-io xl sx xlsx_writer) #:use-module (crates-io))

(define-public crate-xlsx_writer-0.1.0 (c (n "xlsx_writer") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "dirs") (r "^3.0") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8.28") (d #t) (k 0)) (d (n "rm_rf") (r "^0.6.1") (d #t) (k 0)))) (h "08zlpbgxahq89i3azsj4nyk7ihc6y2m03hw262fb6ckwd7y6yrl5")))

(define-public crate-xlsx_writer-0.1.1 (c (n "xlsx_writer") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "dirs") (r "^3.0") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8.28") (d #t) (k 0)) (d (n "rm_rf") (r "^0.6.1") (d #t) (k 0)))) (h "0pbz5g9cqaxb1nrqdh9jim5j0dhbhj3df01g6dbhsvf811qss036")))

(define-public crate-xlsx_writer-0.1.2 (c (n "xlsx_writer") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "dirs") (r "^3.0") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8.28") (d #t) (k 0)) (d (n "rm_rf") (r "^0.6.1") (d #t) (k 0)))) (h "1s1rjg81slzjxffpb1r6bk1pl5234n8z0zxn779c3mjbckzl11kf")))

(define-public crate-xlsx_writer-0.1.3 (c (n "xlsx_writer") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "dirs") (r "^3.0") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8.28") (d #t) (k 0)) (d (n "rm_rf") (r "^0.6.1") (d #t) (k 0)))) (h "1992pfyxg4sdaysggwf6rra3knjd3ad66nkvjqxljykldiq7sgh3")))

(define-public crate-xlsx_writer-0.1.5 (c (n "xlsx_writer") (v "0.1.5") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "dirs") (r "^3.0") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8.28") (d #t) (k 0)) (d (n "rm_rf") (r "^0.6.1") (d #t) (k 0)))) (h "0dqy03y10ddwxzi5vdyc3c2kpna0avyy9frr85987bzkhwrgmhyj")))

