(define-module (crates-io xl sx xlsxfin) #:use-module (crates-io))

(define-public crate-xlsxfin-0.1.0 (c (n "xlsxfin") (v "0.1.0") (h "1ynz0sdkzlqa092858qjqw5qrqlawf3llw12znad5b183h5zn42s")))

(define-public crate-xlsxfin-0.1.1 (c (n "xlsxfin") (v "0.1.1") (h "1zmiljhnj7mq1cyc0d1bq0j4jq0a01k77b7a5nbdbvcxgf563w5g")))

