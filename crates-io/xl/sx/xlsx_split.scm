(define-module (crates-io xl sx xlsx_split) #:use-module (crates-io))

(define-public crate-xlsx_split-1.0.0 (c (n "xlsx_split") (v "1.0.0") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "msoffice_crypt") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "umya-spreadsheet") (r "^0.9") (d #t) (k 0)))) (h "0ql52si6iqxsqkcznk9hz14x32xbkarncildzff4blhsma7g2zsh") (f (quote (("default" "encrypt" "bin")))) (s 2) (e (quote (("encrypt" "dep:msoffice_crypt") ("bin" "dep:clap"))))))

(define-public crate-xlsx_split-1.0.1 (c (n "xlsx_split") (v "1.0.1") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "msoffice_crypt") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "umya-spreadsheet") (r "^1.2") (d #t) (k 0)))) (h "05kiqava74374iw42fkzn7f7i9f8dk2fc8zl1hfq0d8icgasqyf7") (f (quote (("default" "encrypt" "bin")))) (s 2) (e (quote (("encrypt" "dep:msoffice_crypt") ("bin" "dep:clap"))))))

