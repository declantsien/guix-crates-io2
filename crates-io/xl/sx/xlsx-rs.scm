(define-module (crates-io xl sx xlsx-rs) #:use-module (crates-io))

(define-public crate-xlsx-rs-0.1.0 (c (n "xlsx-rs") (v "0.1.0") (h "196ysm694a858y1vxli7wvlhpjgp7ik46cdnikc82kpwl0i6fnav") (y #t)))

(define-public crate-xlsx-rs-0.1.1 (c (n "xlsx-rs") (v "0.1.1") (d (list (d (n "xmlx") (r "^0.1.1") (d #t) (k 0)) (d (n "zipx") (r "^0.1.1") (d #t) (k 0)))) (h "1kys88bi9mc2y3qcdz9gg0rcqivc9d350zxfbp3jama2dzs9qqmr") (y #t)))

