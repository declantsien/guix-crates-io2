(define-module (crates-io xl sx xlsx_to_twmap) #:use-module (crates-io))

(define-public crate-xlsx_to_twmap-0.1.0 (c (n "xlsx_to_twmap") (v "0.1.0") (d (list (d (n "calamine") (r "^0.24.0") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "twmap") (r "^0.12.0") (d #t) (k 0)))) (h "1f6rb5gkixf7svxzrhfwqn1fgcw8k82i9l45mj627as9629b922z")))

