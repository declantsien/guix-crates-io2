(define-module (crates-io xl sx xlsx_reader) #:use-module (crates-io))

(define-public crate-xlsx_reader-1.0.0 (c (n "xlsx_reader") (v "1.0.0") (d (list (d (n "serde_xml") (r "^0.6.3") (d #t) (k 0)) (d (n "zip") (r "^0.1.17") (d #t) (k 0)))) (h "1y31dig31crv6c1g2h65dnl2xs6pzr9zba5fva5rlkjirawx93wa")))

(define-public crate-xlsx_reader-1.0.1 (c (n "xlsx_reader") (v "1.0.1") (d (list (d (n "serde_xml") (r "^0.9.1") (d #t) (k 0)) (d (n "zip") (r "^0.2.0") (d #t) (k 0)))) (h "18mr7r41a2ida361rk49hq94ib4jjysjz8qsc2i4k68jqrbnc6cx")))

(define-public crate-xlsx_reader-1.0.2 (c (n "xlsx_reader") (v "1.0.2") (d (list (d (n "serde_xml") (r "^0.9.1") (d #t) (k 0)) (d (n "zip") (r "^0.2.0") (d #t) (k 0)))) (h "10zcjd9n6s13lylz5kwcadprhsl55wsrfkfrwdqhwwbgzkmknrrd")))

(define-public crate-xlsx_reader-1.1.0 (c (n "xlsx_reader") (v "1.1.0") (d (list (d (n "serde_xml") (r "^0.9.1") (d #t) (k 0)) (d (n "zip") (r "^0.2.0") (d #t) (k 0)))) (h "0xiavlbr5j0l4jp4qhm313hdg9jxn39vw5lgzlf2f2ds1axdn9i1")))

(define-public crate-xlsx_reader-1.1.1 (c (n "xlsx_reader") (v "1.1.1") (d (list (d (n "serde_xml") (r "^0.9.1") (d #t) (k 0)) (d (n "zip") (r "^0.2.0") (d #t) (k 0)))) (h "0hsgspax5dpb0vrdgpnkdqx4qjz8zqp4vm40c0mlwz7rjgxfi6dn")))

(define-public crate-xlsx_reader-1.1.2 (c (n "xlsx_reader") (v "1.1.2") (d (list (d (n "serde_xml") (r "^0.9.1") (d #t) (k 0)) (d (n "zip") (r "^0.2.0") (d #t) (k 0)))) (h "1gw6s4w8jag523cg3sj0nq7igrh7lrq6q17rm93g277alvwdryvg")))

(define-public crate-xlsx_reader-1.2.0 (c (n "xlsx_reader") (v "1.2.0") (d (list (d (n "serde_xml") (r "^0.9.1") (d #t) (k 0)) (d (n "zip") (r "^0.2.0") (d #t) (k 0)))) (h "047dmy11jgdx551lz7g98n9aazynax18qpgw3574kgkdg7zw8h84")))

(define-public crate-xlsx_reader-1.3.0 (c (n "xlsx_reader") (v "1.3.0") (d (list (d (n "serde_xml") (r "^0.9.1") (d #t) (k 0)) (d (n "zip") (r "^0.2.0") (d #t) (k 0)))) (h "14fbm70yfl6f68nzgahq0kyhqi86pjszv15vlh9n6arc5lpsrji6")))

(define-public crate-xlsx_reader-2.0.0 (c (n "xlsx_reader") (v "2.0.0") (d (list (d (n "serde_xml") (r "^0.9.1") (d #t) (k 0)) (d (n "zip") (r "^0.2.0") (d #t) (k 0)))) (h "0p84l0icaagcipc1bipkgcrhrlwryl87yhxdsp1758la0rzbhx7c")))

(define-public crate-xlsx_reader-2.1.0 (c (n "xlsx_reader") (v "2.1.0") (d (list (d (n "serde_xml") (r "^0.9.1") (d #t) (k 0)) (d (n "zip") (r "^0.2.0") (d #t) (k 0)))) (h "068izw59jqwnxn4gmxc7zpkz9hh3i98i8pdx5x89s09dmaawl7pc")))

(define-public crate-xlsx_reader-2.2.0 (c (n "xlsx_reader") (v "2.2.0") (d (list (d (n "serde_xml") (r "^0.9.1") (d #t) (k 0)) (d (n "zip") (r "^0.2.0") (d #t) (k 0)))) (h "0szf5rj6smskhjzmcv149pa0jrzzcyqhls80zv7qcw0q4fi9na37")))

(define-public crate-xlsx_reader-2.3.0 (c (n "xlsx_reader") (v "2.3.0") (d (list (d (n "serde_xml") (r "^0.9.1") (d #t) (k 0)) (d (n "zip") (r "^0.2.0") (d #t) (k 0)))) (h "008kd9gfny2glyxw471114rad7ssdk7r5p1qk5x3zazyvph76zb4")))

(define-public crate-xlsx_reader-2.4.0 (c (n "xlsx_reader") (v "2.4.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.2.1") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "zip") (r "^0.2.0") (d #t) (k 0)))) (h "0rap238b5mbxcvnsbxhvl6dxp4qsn54zzjipa0b3mdj7ax1kkm65")))

(define-public crate-xlsx_reader-2.4.1 (c (n "xlsx_reader") (v "2.4.1") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.2.1") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "zip") (r "^0.2.0") (d #t) (k 0)))) (h "0rq2i3d61rwqsnjbl2dgh834k4c00vr081z8h8188hyyby6q87ch")))

(define-public crate-xlsx_reader-2.5.0 (c (n "xlsx_reader") (v "2.5.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.2.1") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "zip") (r "^0.2.0") (d #t) (k 0)))) (h "19qbwqwqky5r1fid6g7nqzjfpkp5qpscragqbjjk04bjrxmni1wx")))

(define-public crate-xlsx_reader-2.6.0 (c (n "xlsx_reader") (v "2.6.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.2.1") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "zip") (r "^0.2.0") (d #t) (k 0)))) (h "1s4m1rqyfysl5rqvxkkjkdak6sih5sp16zsh427k12my85a2lgyy")))

(define-public crate-xlsx_reader-2.7.0 (c (n "xlsx_reader") (v "2.7.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.2.1") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "zip") (r "^0.2.0") (d #t) (k 0)))) (h "01nkw3hjs0i5jkm2wwmpcj3v51hfndrk3sv9rv6crh4i3m2hmn14")))

(define-public crate-xlsx_reader-2.8.0 (c (n "xlsx_reader") (v "2.8.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.2.1") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "zip") (r "^0.2.0") (d #t) (k 0)))) (h "1n1zlsg5iw3722p9h2jqqgx9wv9c69yryir4a3rz387k38cdi1vx")))

(define-public crate-xlsx_reader-2.9.0 (c (n "xlsx_reader") (v "2.9.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.2.1") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "zip") (r "^0.2.0") (d #t) (k 0)))) (h "1hzfz2d11afy8p4mv91lzxxklyy5xiv8zlrckx19yf41ca07a75r")))

(define-public crate-xlsx_reader-2.10.0 (c (n "xlsx_reader") (v "2.10.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.2.1") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "zip") (r "^0.2.0") (d #t) (k 0)))) (h "0f7lddk7y6d7cb740lr1drhbxxcnixqgfpkx1fggdw6gf81a1c62")))

(define-public crate-xlsx_reader-2.11.0 (c (n "xlsx_reader") (v "2.11.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.2.1") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "zip") (r "^0.2.0") (d #t) (k 0)))) (h "1v5naapjxy4ba7hdm5gm40ln9zbn7xajvdckmsdfyszb38jq86iq")))

(define-public crate-xlsx_reader-2.12.0 (c (n "xlsx_reader") (v "2.12.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.2.1") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "zip") (r "^0.2.0") (d #t) (k 0)))) (h "0yq37jd8s0gc83qad79lrjaaybd0bh6yijkfnc61mdqnvmbamda1")))

(define-public crate-xlsx_reader-3.0.0 (c (n "xlsx_reader") (v "3.0.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.2.1") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "zip") (r "^0.2.0") (d #t) (k 0)))) (h "1rk7prn6rw0ayblw7gzc4zfbn5f47q0da4dvrsjycn840rfcxq25")))

(define-public crate-xlsx_reader-3.1.0 (c (n "xlsx_reader") (v "3.1.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.2.1") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "zip") (r "^0.2.0") (d #t) (k 0)))) (h "0zw51v816kyaqlsd8ggi5mg4nv69x5ygwwhj73zskxlm355vcsdl")))

(define-public crate-xlsx_reader-3.1.1 (c (n "xlsx_reader") (v "3.1.1") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.2.1") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "zip") (r "^0.2.0") (d #t) (k 0)))) (h "1nkzf4yv7ncsm3s98lh6ibaz5f8hbbxzq36i0v2bn60bwjk06dv0")))

