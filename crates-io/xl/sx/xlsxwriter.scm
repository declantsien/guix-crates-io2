(define-module (crates-io xl sx xlsxwriter) #:use-module (crates-io))

(define-public crate-xlsxwriter-0.1.0 (c (n "xlsxwriter") (v "0.1.0") (d (list (d (n "libxlsxwriter-sys") (r "^0.8.7") (d #t) (k 0)))) (h "089pc7018ivcsq4ama2la3ipk7brr2c6n650kc5bm5xl6yc3fb3s")))

(define-public crate-xlsxwriter-0.2.0 (c (n "xlsxwriter") (v "0.2.0") (d (list (d (n "libxlsxwriter-sys") (r "^0.8.7") (d #t) (k 0)))) (h "0ap1z6r3h8dk0asgxnzcm5k6qvmjyqljgmhr6pfg62v2q9ac8aq6")))

(define-public crate-xlsxwriter-0.3.0 (c (n "xlsxwriter") (v "0.3.0") (d (list (d (n "libxlsxwriter-sys") (r "^0.9.4") (d #t) (k 0)))) (h "023i6nyvjj5q9gnaijqy4d1swf9z7sqwdvk55mravwkiq6x4i2w1")))

(define-public crate-xlsxwriter-0.3.1 (c (n "xlsxwriter") (v "0.3.1") (d (list (d (n "libxlsxwriter-sys") (r "^0.9.4") (d #t) (k 0)))) (h "1vdf0dcwm5dhpz08ashx63dl45pl64pwnzmcni8v2n6bjxgxdary")))

(define-public crate-xlsxwriter-0.3.2 (c (n "xlsxwriter") (v "0.3.2") (d (list (d (n "libxlsxwriter-sys") (r "^1.0.0") (d #t) (k 0)))) (h "1car0f1r4q02hhp78w4lghbr754yxy9928c4bxsk91fdv034yim8")))

(define-public crate-xlsxwriter-0.3.3 (c (n "xlsxwriter") (v "0.3.3") (d (list (d (n "libxlsxwriter-sys") (r "^1.0.2") (d #t) (k 0)))) (h "1f7l5p91ii11qx4gk69y5n9zykg0z8j0qflbribx3cxacxls8nmr")))

(define-public crate-xlsxwriter-0.3.4 (c (n "xlsxwriter") (v "0.3.4") (d (list (d (n "libxlsxwriter-sys") (r "^1.0.2") (d #t) (k 0)))) (h "1kymadkqbs28f8g0hm6wa1n22zswz1ws4v7q8yh9gfnm14334cwg")))

(define-public crate-xlsxwriter-0.3.5 (c (n "xlsxwriter") (v "0.3.5") (d (list (d (n "libxlsxwriter-sys") (r "^1.0.2") (d #t) (k 0)))) (h "08qzhvlsq2315k4w5g2lhq92cij4bivb6k2b3bbzw7vs0van7jwx") (f (quote (("use-openssl-md5" "libxlsxwriter-sys/use-openssl-md5") ("system-zlib" "libxlsxwriter-sys/system-zlib") ("no-md5" "libxlsxwriter-sys/no-md5"))))))

(define-public crate-xlsxwriter-0.4.0 (c (n "xlsxwriter") (v "0.4.0") (d (list (d (n "libxlsxwriter-sys") (r "^1.1.4") (d #t) (k 0)))) (h "1j6jx2zzrz27mpsx85j9casf6rgnkds8vxknpia49p3kg7vi6i9p") (f (quote (("use-openssl-md5" "libxlsxwriter-sys/use-openssl-md5") ("system-zlib" "libxlsxwriter-sys/system-zlib") ("no-md5" "libxlsxwriter-sys/no-md5"))))))

(define-public crate-xlsxwriter-0.5.0 (c (n "xlsxwriter") (v "0.5.0") (d (list (d (n "libxlsxwriter-sys") (r "^1.1.4") (d #t) (k 0)))) (h "0ns3yqd71c7iid93ykvwx7kxv2kb8cdc0mgzqk4pqjd91ljypxr1") (f (quote (("use-openssl-md5" "libxlsxwriter-sys/use-openssl-md5") ("system-zlib" "libxlsxwriter-sys/system-zlib") ("no-md5" "libxlsxwriter-sys/no-md5"))))))

(define-public crate-xlsxwriter-0.6.0 (c (n "xlsxwriter") (v "0.6.0") (d (list (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "libxlsxwriter-sys") (r "^1.1.5") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 2)))) (h "192rxlr8p3c9sx5lvhnkz7fq2a2qxr0mfprrjda55a1yc1022z27") (f (quote (("use-openssl-md5" "libxlsxwriter-sys/use-openssl-md5") ("system-zlib" "libxlsxwriter-sys/system-zlib") ("no-md5" "libxlsxwriter-sys/no-md5") ("default" "chrono")))) (s 2) (e (quote (("chrono" "dep:chrono"))))))

