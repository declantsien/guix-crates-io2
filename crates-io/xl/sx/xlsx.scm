(define-module (crates-io xl sx xlsx) #:use-module (crates-io))

(define-public crate-xlsx-0.1.0 (c (n "xlsx") (v "0.1.0") (d (list (d (n "xml_writer") (r "^0.1.2") (d #t) (k 0)) (d (n "zip") (r "^0.1.8") (d #t) (k 0)))) (h "1amcsmi746l1v7fcsgmcmnqyd10f6ijcjv4fy66vv7if1aq0i1yg")))

