(define-module (crates-io xl oc xloc) #:use-module (crates-io))

(define-public crate-xloc-0.1.0 (c (n "xloc") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)))) (h "1mjvd0sihzcvmhsp70cdcl9gzfh1qrdkawdrm83hm6hc5xiczi6y")))

(define-public crate-xloc-0.1.1 (c (n "xloc") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)))) (h "0c83b4xisw047v81m6ikdpzlc6ybg0qz86rz7zxxjac0xkjca0xs")))

(define-public crate-xloc-0.1.2 (c (n "xloc") (v "0.1.2") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)))) (h "0wax0dmkndj7g1dbnf0xbkfss0ms18iy95pqkfidx23y84l7pgcp")))

(define-public crate-xloc-0.2.0 (c (n "xloc") (v "0.2.0") (d (list (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "predicates") (r "^2.0") (d #t) (k 2)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "0nhjhnnf84ixasd47csypdbhyip1mlrzl12hz3gs5agp1rjalsba")))

