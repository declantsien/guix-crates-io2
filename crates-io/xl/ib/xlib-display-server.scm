(define-module (crates-io xl ib xlib-display-server) #:use-module (crates-io))

(define-public crate-xlib-display-server-0.1.0 (c (n "xlib-display-server") (v "0.1.0") (d (list (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "leftwm-core") (r "^0.4.1") (d #t) (k 0)) (d (n "mio") (r "^0.8.0") (f (quote ("os-ext"))) (d #t) (k 0)) (d (n "tokio") (r "^1.2.0") (f (quote ("sync" "time"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.36") (d #t) (k 0)) (d (n "x11-dl") (r "^2.18.4") (d #t) (k 0)))) (h "0ikfb58rffa7hgsmcj1krkph91kpilsc7k9ad9c0r4gykrdg4ssa")))

(define-public crate-xlib-display-server-0.1.1 (c (n "xlib-display-server") (v "0.1.1") (d (list (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "leftwm-core") (r "^0.4.2") (d #t) (k 0)) (d (n "mio") (r "^0.8.0") (f (quote ("os-ext"))) (d #t) (k 0)) (d (n "tokio") (r "^1.2.0") (f (quote ("sync" "time"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.36") (d #t) (k 0)) (d (n "x11-dl") (r "^2.18.4") (d #t) (k 0)))) (h "01a5r3difqzhqvp0c96hj5qbn4lmkc14bgk40bz3qcdzxr832cwk")))

(define-public crate-xlib-display-server-0.1.2 (c (n "xlib-display-server") (v "0.1.2") (d (list (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "leftwm-core") (r "^0.5.0") (d #t) (k 0)) (d (n "mio") (r "^0.8.0") (f (quote ("os-ext"))) (d #t) (k 0)) (d (n "tokio") (r "^1.2.0") (f (quote ("sync" "time"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.36") (d #t) (k 0)) (d (n "x11-dl") (r "^2.18.4") (d #t) (k 0)))) (h "1ikwgzmxcmz5q85d3pqly1n1k4zwlp8qr7if8sy6ni8hs0v8wc91")))

