(define-module (crates-io xl ib xlib) #:use-module (crates-io))

(define-public crate-xlib-0.0.1 (c (n "xlib") (v "0.0.1") (h "0ibfryfvc7k8ny37pmfvrsck9vzpndhrz3f7gp7wkma690wbd1j2") (y #t)))

(define-public crate-xlib-0.0.2 (c (n "xlib") (v "0.0.2") (h "1k0vj2inkcrhqrq7pkz1pha9x7vnmzri53xxxvqbdf8zdj1h1np7") (y #t)))

(define-public crate-xlib-0.0.3 (c (n "xlib") (v "0.0.3") (h "076gcffnihbbawi3h6dk3q9h963l8wfqdfkwd73fxaqpxpm2iyzy") (y #t)))

(define-public crate-xlib-0.0.4 (c (n "xlib") (v "0.0.4") (h "0i3i8fywgg689422rphwk3v995s2knr4ybxm19d061xk7gcv4bwr") (y #t)))

(define-public crate-xlib-0.0.5 (c (n "xlib") (v "0.0.5") (h "0qsrgdr4sxikzhjcpzbc87c2zs19vz5rbzqp4khdrayx5bvfzmzk") (y #t)))

(define-public crate-xlib-0.1.0 (c (n "xlib") (v "0.1.0") (h "16b1fcks9zd1405ikwhvjjbivabx1kwcnyd0pqxx5iqd0w7kn59k") (y #t)))

