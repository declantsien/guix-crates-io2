(define-module (crates-io xl in xlint) #:use-module (crates-io))

(define-public crate-xlint-0.2.1 (c (n "xlint") (v "0.2.1") (d (list (d (n "camino") (r "^1.0.3") (d #t) (k 0)) (d (n "guppy") (r "^0.13.0") (d #t) (k 0)) (d (n "hakari") (r "^0.9.0") (d #t) (k 0)))) (h "1xa0pxs4vh17qzvh1l0qp0h7jbcgsraxw29vxagl4n8cywdzjy1l") (y #t)))

(define-public crate-xlint-0.2.2 (c (n "xlint") (v "0.2.2") (d (list (d (n "camino") (r "^1.0.3") (d #t) (k 0)) (d (n "guppy") (r "^0.13.0") (d #t) (k 0)) (d (n "hakari") (r "^0.9.0") (d #t) (k 0)))) (h "02s56l9fylaklxn3rrizv9w4dvxs1md0zbv4nbd78r2dq8lljvvm") (y #t)))

