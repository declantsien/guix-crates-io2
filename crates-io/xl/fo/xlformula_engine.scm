(define-module (crates-io xl fo xlformula_engine) #:use-module (crates-io))

(define-public crate-xlformula_engine-0.1.0 (c (n "xlformula_engine") (v "0.1.0") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 0)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "1xpmahm1m739h6sm3pdm54m2kw2845igilxkqaxnl22618spfy0x")))

(define-public crate-xlformula_engine-0.1.1 (c (n "xlformula_engine") (v "0.1.1") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 0)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "1x3mwczkh3agiidbrbdynalycjj10fall2b46mdrh3bwxlxsj3qb")))

(define-public crate-xlformula_engine-0.1.2 (c (n "xlformula_engine") (v "0.1.2") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 0)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "0zng95vbqbkgsq01gd1mnbdn0gh2jyzm4j48k91cs3f6lx7jbajw")))

(define-public crate-xlformula_engine-0.1.3 (c (n "xlformula_engine") (v "0.1.3") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 0)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "0gnb8qqrirq5kmwm5hzmxhcgbwb5zhc475qxl5c1zy0rr5mhkqsw")))

(define-public crate-xlformula_engine-0.1.4 (c (n "xlformula_engine") (v "0.1.4") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 0)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "1g5mkhvq0cicxmxbq5gb0adsbd0zli89hlajp4x7bag6vbdfl59k")))

(define-public crate-xlformula_engine-0.1.5 (c (n "xlformula_engine") (v "0.1.5") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 0)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "1c7hq9fdjgwdb1ycw4dr1b01yjlq27gcxk7rhigl86vf47d1nafd")))

(define-public crate-xlformula_engine-0.1.6 (c (n "xlformula_engine") (v "0.1.6") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 0)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "1wwgicsq879mmjfhg6fafiyqvqsnj42ss1clnibfwbw3vmgcvs0i")))

(define-public crate-xlformula_engine-0.1.7 (c (n "xlformula_engine") (v "0.1.7") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 0)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "0ss2a6mn6zn6c69ah6inkmlmp5wn309xia5933hyfgg8hjyl0bbs")))

(define-public crate-xlformula_engine-0.1.8 (c (n "xlformula_engine") (v "0.1.8") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "10hpzkdx7my8h1z9f6aqc5dlj4llm7y5j56vy581znx9ryikq1gy")))

(define-public crate-xlformula_engine-0.1.9 (c (n "xlformula_engine") (v "0.1.9") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "08lkc9c4b5bfqcjd59likd20qmx3rdyf21wryq2hqwa7sf3ap9n1")))

(define-public crate-xlformula_engine-0.1.10 (c (n "xlformula_engine") (v "0.1.10") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "1mmv6fg2q7fbsiyn2hsv2m1rnf269b0f0iix3b73v5h9qziyzv7a")))

(define-public crate-xlformula_engine-0.1.11 (c (n "xlformula_engine") (v "0.1.11") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "1sflbdj3n7hj51nzgrl9q4i6idj86z2ir9xxcibj6ix94msq5g2w")))

(define-public crate-xlformula_engine-0.1.12 (c (n "xlformula_engine") (v "0.1.12") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "1k493jbkrjn9433ya53lylg7wcqzaycjx082wrbkg997qbxs1fxz")))

(define-public crate-xlformula_engine-0.1.13 (c (n "xlformula_engine") (v "0.1.13") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "0czp83p97krkc4rlhy8b5x9jxb2a3vpa0f9sh1dcwb1kg9wn7j6l")))

(define-public crate-xlformula_engine-0.1.14 (c (n "xlformula_engine") (v "0.1.14") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "1fpj7ycqslabrcmmg4dryr9rgj88bj24zn4znk063piz3ly5yw7z")))

(define-public crate-xlformula_engine-0.1.15 (c (n "xlformula_engine") (v "0.1.15") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "17swaqs4rr496p8848iy1ymiqlgq8fx5j40gwgzdyrqdg37yns7q")))

(define-public crate-xlformula_engine-0.1.16 (c (n "xlformula_engine") (v "0.1.16") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "1c2wpyfvnaj30dnwjq4wnvksfng6fxxc4ifz2vbhbvzzidw2qs31")))

(define-public crate-xlformula_engine-0.1.17 (c (n "xlformula_engine") (v "0.1.17") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "0wi1k4mbblic5g2lbjn4xa0rp4q8kbl85lvxj3zvhza60nzwmlxk")))

(define-public crate-xlformula_engine-0.1.18 (c (n "xlformula_engine") (v "0.1.18") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "1zc7yyfg3fccy1vx1rwqwdlh0d4xi63qgjs84vzxlfghan8p9m8w")))

