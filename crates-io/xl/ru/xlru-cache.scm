(define-module (crates-io xl ru xlru-cache) #:use-module (crates-io))

(define-public crate-xlru-cache-0.1.2 (c (n "xlru-cache") (v "0.1.2") (d (list (d (n "heapsize") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "linked-hash-map") (r "^0.5") (d #t) (k 0)))) (h "1zdzwpq0m4irz17n403mzm9mskwr03k5rgryr1wqpz75h62zsjm4") (f (quote (("heapsize_impl" "heapsize" "linked-hash-map/heapsize_impl"))))))

