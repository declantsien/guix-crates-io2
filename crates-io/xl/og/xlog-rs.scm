(define-module (crates-io xl og xlog-rs) #:use-module (crates-io))

(define-public crate-xlog-rs-0.1.0 (c (n "xlog-rs") (v "0.1.0") (h "0ll3z8j2a791ivxgb9wmzjvkj6spmic966ijd26g3k9rm9wnlm2r")))

(define-public crate-xlog-rs-0.2.0 (c (n "xlog-rs") (v "0.2.0") (h "10yjj5xd74y0ri53a14xnpmvj3xb09nawgbjniy5d63yywx6gn00")))

(define-public crate-xlog-rs-0.3.0 (c (n "xlog-rs") (v "0.3.0") (h "1cdmd2dgab1rkhnw81df7a46qglanz32r3rg23lbn0vml9q76sf6")))

(define-public crate-xlog-rs-0.3.1 (c (n "xlog-rs") (v "0.3.1") (h "16iss7czbghbvmxdzxl64jadm2anpm2iyn8nwjjqygxalprkrdq2")))

