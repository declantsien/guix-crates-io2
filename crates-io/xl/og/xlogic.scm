(define-module (crates-io xl og xlogic) #:use-module (crates-io))

(define-public crate-xlogic-0.0.0 (c (n "xlogic") (v "0.0.0") (h "01qxdyc76ryh7w23ayy0gg58gkg0ba9v6hh3rm9bsl7fp4pqz0kr")))

(define-public crate-xlogic-0.1.0 (c (n "xlogic") (v "0.1.0") (h "0l7dspzpk84k77pycrbnp36n29qbkcd9wyzrjz4q32cxykvh7q6k")))

