(define-module (crates-io xl og xlog) #:use-module (crates-io))

(define-public crate-xlog-0.1.2 (c (n "xlog") (v "0.1.2") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "simple_logger") (r "^1.0") (d #t) (k 2)))) (h "0w4w15a7acv95lnq31qyp5whmli3bnssxv6kyqncrqswfdzzad9j")))

(define-public crate-xlog-0.1.3 (c (n "xlog") (v "0.1.3") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "simple_logger") (r "^1.0") (d #t) (k 2)))) (h "0wj5prspi8rrj9fvx5q40ynf9s6qbcg1dxnz7wlj82y3bza7n54y")))

(define-public crate-xlog-0.1.4 (c (n "xlog") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "simple_logger") (r "^1.0") (d #t) (k 2)))) (h "0fh37731wvrqlwsksrqq774gsngx0qwmgxb4k1b8i40jx3s91lpx")))

(define-public crate-xlog-0.1.5 (c (n "xlog") (v "0.1.5") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "simple_logger") (r "^1.0") (d #t) (k 2)))) (h "1vq5w5a9fbmcpczbab2g39iz5va33n2i92i0gn7nznyfan02bgg2")))

(define-public crate-xlog-0.1.6 (c (n "xlog") (v "0.1.6") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "femme") (r "^1.3") (d #t) (k 2)) (d (n "log") (r "^0.4") (f (quote ("kv_unstable"))) (d #t) (k 0)))) (h "0lk247i3vikgb7apl74wa83lf5msd6988xam8s0f8dqjqmrk483b")))

(define-public crate-xlog-0.1.7 (c (n "xlog") (v "0.1.7") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "femme") (r "^1.3") (d #t) (k 2)) (d (n "log") (r "^0.4") (f (quote ("kv_unstable"))) (d #t) (k 0)))) (h "17fpjxqja7r1nw0n6rd37fnpbblhgji8drmgdgy09d31jmz18gx0")))

(define-public crate-xlog-0.1.8 (c (n "xlog") (v "0.1.8") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "femme") (r "^1.3") (d #t) (k 2)) (d (n "log") (r "^0.4") (f (quote ("kv_unstable"))) (d #t) (k 0)))) (h "14k7rwn6vlmg0wpizpp9jil3d59lmx0rx49ajbmpghks5g8j4wqs")))

(define-public crate-xlog-0.2.0 (c (n "xlog") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "femme") (r "^1.3") (d #t) (k 2)) (d (n "log") (r "^0.4.11") (f (quote ("kv_unstable"))) (d #t) (k 0)))) (h "06ad7rsrba9wyf0j7kpscmfyd6ikhm51n8byn6rybw98ragd2mrr")))

(define-public crate-xlog-0.2.1 (c (n "xlog") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "femme") (r "^1.3") (d #t) (k 2)) (d (n "log") (r "^0.4.11") (f (quote ("kv_unstable"))) (d #t) (k 0)))) (h "0x4apk1qnrl6amk5ixawcv604my872dsgdkiyw95ilipjbiwjb4g")))

(define-public crate-xlog-0.2.2 (c (n "xlog") (v "0.2.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "femme") (r "^1.3") (d #t) (k 2)) (d (n "log") (r "^0.4.11") (f (quote ("kv_unstable"))) (d #t) (k 0)))) (h "1y7ql7hhmvya0gb2vfisgim4giflll48wnr2nynp0i4b292fb4xn")))

