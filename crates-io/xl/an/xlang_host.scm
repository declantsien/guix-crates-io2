(define-module (crates-io xl an xlang_host) #:use-module (crates-io))

(define-public crate-xlang_host-0.1.0 (c (n "xlang_host") (v "0.1.0") (h "0z749qcdipjz5ddhmjs8jf8kiw3sfc4brqn7gdg6ailnzlyn4w2p")))

(define-public crate-xlang_host-0.1.1 (c (n "xlang_host") (v "0.1.1") (h "1j95aw76dz55zrglbri0qs64qlsfwbk3x5rk9dil725sqs3mmf6s")))

(define-public crate-xlang_host-0.2.0 (c (n "xlang_host") (v "0.2.0") (d (list (d (n "cfg-match") (r "^0.2.1") (d #t) (k 0)))) (h "07zp86vgm80jz6ck6qa00bhmfc8xjhv1al49mjar3mblj9h7k72l")))

(define-public crate-xlang_host-0.2.1 (c (n "xlang_host") (v "0.2.1") (d (list (d (n "cfg-match") (r "^0.2.1") (d #t) (k 0)))) (h "01qj9f3yvfknr3pn48g49dw6kl0m5phgp0kx45nkpssdvck1zk38")))

