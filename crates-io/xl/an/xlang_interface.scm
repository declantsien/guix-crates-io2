(define-module (crates-io xl an xlang_interface) #:use-module (crates-io))

(define-public crate-xlang_interface-0.1.0 (c (n "xlang_interface") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "xlang_abi") (r "^0.2.0") (d #t) (k 0)) (d (n "xlang_host") (r "^0.2.0") (d #t) (k 0)) (d (n "xlang_targets") (r "^0.1.0") (d #t) (k 0)))) (h "0hlgak073swmffhwcsj4c23irlm38c0xm7k6aikz76wfc47ddd7r")))

