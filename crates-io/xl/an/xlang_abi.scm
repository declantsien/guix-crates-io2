(define-module (crates-io xl an xlang_abi) #:use-module (crates-io))

(define-public crate-xlang_abi-0.1.0 (c (n "xlang_abi") (v "0.1.0") (d (list (d (n "xlang_host") (r "^0.1.0") (d #t) (k 0)))) (h "1qqz4zrhi7rhn0ikcn8r66id8q6k61w771l3whq0yc8l4ddnxcpk") (y #t)))

(define-public crate-xlang_abi-0.1.1 (c (n "xlang_abi") (v "0.1.1") (d (list (d (n "xlang_host") (r "^0.1.0") (d #t) (k 0)))) (h "0jvc4xvcfibi4q4ga2fipnqib5iqv3wlba155g1l5sxkin5jvxph") (y #t)))

(define-public crate-xlang_abi-0.1.2 (c (n "xlang_abi") (v "0.1.2") (d (list (d (n "xlang_host") (r "^0.1.0") (d #t) (k 0)))) (h "1awnyw56fgq035m1i1d58llyyd9r1n90q4684s2580z0scj18dn7") (y #t)))

(define-public crate-xlang_abi-0.1.3 (c (n "xlang_abi") (v "0.1.3") (d (list (d (n "xlang_host") (r "^0.1.1") (d #t) (k 0)))) (h "0isrwm0lvrmsfjj4lhjnlpagddvr6g8c5h3f8q6n7g7zcfq25hyz") (y #t)))

(define-public crate-xlang_abi-0.1.4 (c (n "xlang_abi") (v "0.1.4") (d (list (d (n "xlang_host") (r "^0.1.1") (d #t) (k 0)))) (h "1pxr913mx80j0xrvvd9an14yi07pbggirwfqdvff72zrm9nvi0x5") (y #t)))

(define-public crate-xlang_abi-0.1.5 (c (n "xlang_abi") (v "0.1.5") (d (list (d (n "xlang_host") (r "^0.1.1") (d #t) (k 0)))) (h "1yij060r4kp09gcwd3qfd3icw2zlb9dlqmpdgyjhdl4c43a8hg3d") (y #t)))

(define-public crate-xlang_abi-0.1.6 (c (n "xlang_abi") (v "0.1.6") (d (list (d (n "xlang_host") (r "^0.1.1") (d #t) (k 0)))) (h "12gv31m5vj7pbqppgl4qvd74i6danmrbmcpyyakca2pvpyz1p7n6") (y #t)))

(define-public crate-xlang_abi-0.1.7 (c (n "xlang_abi") (v "0.1.7") (d (list (d (n "xlang_host") (r "^0.1.1") (d #t) (k 0)))) (h "0van3wnqnh0mid3s9wnyy050sj161h61i4j220b1a3f0j5df7yvf") (y #t)))

(define-public crate-xlang_abi-0.1.8 (c (n "xlang_abi") (v "0.1.8") (d (list (d (n "xlang_host") (r "^0.1.1") (d #t) (k 0)))) (h "1f9rnsj05255jhsky5wlfvsv8fkjlfkl97y3074c0mafiwfvvcg1")))

(define-public crate-xlang_abi-0.2.0 (c (n "xlang_abi") (v "0.2.0") (d (list (d (n "xlang_host") (r "^0.2") (d #t) (k 0)))) (h "18xwhbqg6y04x0fq8fihh30sgidwhqr9qg4fp9vs3mrfvdy40hrr")))

