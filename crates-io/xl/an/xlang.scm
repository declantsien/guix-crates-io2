(define-module (crates-io xl an xlang) #:use-module (crates-io))

(define-public crate-xlang-0.0.0-alpha (c (n "xlang") (v "0.0.0-alpha") (h "0npqjajhj2pflqrwz9pqr20skhacrrh85bfvivifnk961f06jlsz")))

(define-public crate-xlang-0.0.1-alpha (c (n "xlang") (v "0.0.1-alpha") (d (list (d (n "clap") (r "^3.2.22") (f (quote ("derive"))) (d #t) (k 0)) (d (n "xlang-syntax") (r "^0.0.1-alpha") (d #t) (k 0)))) (h "1870331a8nj7hjq7qnwybrl87khd620v96c38v03frly243l3cy1")))

(define-public crate-xlang-0.0.2-alpha (c (n "xlang") (v "0.0.2-alpha") (d (list (d (n "clap") (r "^3.2.22") (f (quote ("derive"))) (d #t) (k 0)) (d (n "xlang-syntax") (r "^0.0.2-alpha") (d #t) (k 0)))) (h "080c7wm10avdwyf7p9cn5v32110627plnqr59wbk55l9d454rh9q")))

(define-public crate-xlang-0.0.3-alpha (c (n "xlang") (v "0.0.3-alpha") (d (list (d (n "clap") (r "^3.2.22") (f (quote ("derive"))) (d #t) (k 0)) (d (n "xlang-syntax") (r "^0.0.3-alpha") (d #t) (k 0)))) (h "14ibdlp7fdfi00g7vhriz85sid2vaznf7f9kx8j3hg226p9yy0ir")))

(define-public crate-xlang-0.0.4-alpha (c (n "xlang") (v "0.0.4-alpha") (d (list (d (n "clap") (r "^3.2.22") (f (quote ("derive"))) (d #t) (k 0)) (d (n "xlang-syntax") (r "^0.0.4-alpha") (d #t) (k 0)))) (h "17cjig31pibrv3j96jhzwccvcqxzc71mxb93s6wxkcwr0yzcah5i")))

(define-public crate-xlang-0.0.5-alpha (c (n "xlang") (v "0.0.5-alpha") (d (list (d (n "clap") (r "^3.2.22") (f (quote ("derive"))) (d #t) (k 0)) (d (n "xlang-syntax") (r "^0.0.5-alpha") (d #t) (k 0)) (d (n "xlang-vm") (r "^0.0.5-alpha") (d #t) (k 0)))) (h "0i7lysppv82cwcls1j3m0mffw6h3f0vih8nv1dhjdgsjjyd84i5l")))

