(define-module (crates-io xl an xlang_targets) #:use-module (crates-io))

(define-public crate-xlang_targets-0.1.0 (c (n "xlang_targets") (v "0.1.0") (d (list (d (n "target-tuples") (r "^0.5.5") (d #t) (k 0)) (d (n "xlang_abi") (r "^0.2.0") (d #t) (k 0)))) (h "0dmfz7czm5lnq2vs1hmvlr0ifmnhpnzvi7wy4vrbffkr07km7dx1")))

