(define-module (crates-io xl an xlang_struct) #:use-module (crates-io))

(define-public crate-xlang_struct-0.1.0 (c (n "xlang_struct") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "fake-enum") (r "^0.1.4") (d #t) (k 0)) (d (n "xlang_abi") (r "^0.2.0") (d #t) (k 0)) (d (n "xlang_targets") (r "^0.1.0") (d #t) (k 0)))) (h "1w2xns12kfs8za4fdiksgxqwnpfzn87madmrarbd02b51apifjz7")))

