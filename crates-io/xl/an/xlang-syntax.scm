(define-module (crates-io xl an xlang-syntax) #:use-module (crates-io))

(define-public crate-xlang-syntax-0.0.1-alpha (c (n "xlang-syntax") (v "0.0.1-alpha") (d (list (d (n "lalrpop") (r "^0.19.8") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.19.8") (f (quote ("lexer"))) (d #t) (k 0)))) (h "1r2wz1pz7x515fjs5i2pvzc8piagsplha99gz3ng8sgznilhr6bj")))

(define-public crate-xlang-syntax-0.0.2-alpha (c (n "xlang-syntax") (v "0.0.2-alpha") (d (list (d (n "lalrpop") (r "^0.19.8") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.19.8") (f (quote ("lexer"))) (d #t) (k 0)))) (h "12980w91m3x5k2gj12lgb6dsnx6nd7fw7l45vzxa50sxds29sjc7")))

(define-public crate-xlang-syntax-0.0.3-alpha (c (n "xlang-syntax") (v "0.0.3-alpha") (d (list (d (n "lalrpop") (r "^0.19.8") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.19.8") (f (quote ("lexer"))) (d #t) (k 0)))) (h "1p70xkyqhpr16hsvyil8zrig9gjq1zxabc22c93ydhzg1qy08ja7")))

(define-public crate-xlang-syntax-0.0.4-alpha (c (n "xlang-syntax") (v "0.0.4-alpha") (d (list (d (n "lalrpop") (r "^0.19.8") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.19.8") (f (quote ("lexer"))) (d #t) (k 0)))) (h "19w7qb7yvrjl19z561qi430yxwcry4ya97hjhiabi6saqp0b0s4d")))

(define-public crate-xlang-syntax-0.0.5-alpha (c (n "xlang-syntax") (v "0.0.5-alpha") (d (list (d (n "lalrpop") (r "^0.19.8") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.19.8") (f (quote ("lexer"))) (d #t) (k 0)))) (h "09xjw1j1d635rc0f7fm6s68l9x9ir8cswi1zfb57by1am2d50zds")))

