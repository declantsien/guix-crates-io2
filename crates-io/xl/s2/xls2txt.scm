(define-module (crates-io xl s2 xls2txt) #:use-module (crates-io))

(define-public crate-xls2txt-1.0.0 (c (n "xls2txt") (v "1.0.0") (d (list (d (n "calamine") (r "^0.16") (d #t) (k 0)) (d (n "clap") (r "^2.23") (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)))) (h "0ln21j5vbcbjx1kl4c2lcn94bw9wm6bwzk201ir3fai9zhxkxh8n")))

(define-public crate-xls2txt-1.0.1 (c (n "xls2txt") (v "1.0.1") (d (list (d (n "calamine") (r "^0.16.1") (d #t) (k 0)) (d (n "clap") (r "^2.23") (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)))) (h "1b3ak0sk3llssvpi23dxk635sx898apd7175k0q7l96p4m1x150c")))

(define-public crate-xls2txt-1.0.2 (c (n "xls2txt") (v "1.0.2") (d (list (d (n "calamine") (r "^0.19.1") (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)))) (h "06k6v1n7qvn8s8d0gx87ai7nahvj8aijnaly3dwcl8xg82rkdr4k")))

(define-public crate-xls2txt-1.1.0 (c (n "xls2txt") (v "1.1.0") (d (list (d (n "calamine") (r "^0.19.1") (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "guard") (r "^0.5.1") (d #t) (k 0)))) (h "06z9498piqmrbs18ng68bcqn4i95npisvywfxh9savm5397xznk5")))

(define-public crate-xls2txt-1.2.0 (c (n "xls2txt") (v "1.2.0") (d (list (d (n "calamine") (r "^0.19.1") (d #t) (k 0)) (d (n "clap") (r "^4.1") (f (quote ("cargo" "derive"))) (d #t) (k 0)) (d (n "csv") (r "^1.2") (d #t) (k 0)) (d (n "guard") (r "^0.5.1") (d #t) (k 0)))) (h "1g44cda7aifffa0zmjbhfz3y19xxfk0gmfqyzgn7ac2hwhjbcln3")))

(define-public crate-xls2txt-1.2.1 (c (n "xls2txt") (v "1.2.1") (d (list (d (n "assert_cmd") (r "^2.0.11") (d #t) (k 2)) (d (n "calamine") (r "^0.20") (d #t) (k 0)) (d (n "clap") (r "^4.1") (f (quote ("cargo" "derive"))) (d #t) (k 0)) (d (n "csv") (r "^1.2") (d #t) (k 0)) (d (n "guard") (r "^0.5") (d #t) (k 0)) (d (n "predicates") (r "^3.0.3") (d #t) (k 2)))) (h "05b6q9v963sapldn497z2jw58cmcqqyr80yhsik83kp3yzs1r0wi")))

