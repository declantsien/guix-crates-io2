(define-module (crates-io xl ad xladd-util) #:use-module (crates-io))

(define-public crate-xladd-util-0.1.0 (c (n "xladd-util") (v "0.1.0") (d (list (d (n "winapi") (r "^0.3.5") (f (quote ("winuser" "libloaderapi" "debugapi"))) (d #t) (k 0)) (d (n "xladd") (r "^0.1.0") (d #t) (k 0)))) (h "15hy4aswbhbbd6bvd4kw0b6sr90j6bqzw4liklnm1sal6avgg0ig")))

