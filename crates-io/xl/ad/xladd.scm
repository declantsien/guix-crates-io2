(define-module (crates-io xl ad xladd) #:use-module (crates-io))

(define-public crate-xladd-0.1.0 (c (n "xladd") (v "0.1.0") (d (list (d (n "widestring") (r "^0.4.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3.5") (f (quote ("winuser" "libloaderapi" "debugapi"))) (d #t) (k 0)))) (h "1wqgyga7f26awndrg5dc5ig766ggiprdksv5x5sdiz20dg5s8j5s")))

(define-public crate-xladd-0.1.1 (c (n "xladd") (v "0.1.1") (d (list (d (n "widestring") (r "^0.4.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3.5") (f (quote ("winuser" "libloaderapi" "debugapi"))) (d #t) (k 0)))) (h "13jr6hliqvv1rmg3w6njslf0cqp7fiysjb096adcsr0galnzbcw1")))

(define-public crate-xladd-0.1.2 (c (n "xladd") (v "0.1.2") (d (list (d (n "widestring") (r "^0.4.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3.5") (f (quote ("winuser" "libloaderapi" "debugapi"))) (d #t) (k 0)))) (h "0ax24hc6v1r4pqyzn1hb7hga5n0npjr9ldids4qnlxcmahn2hzxz")))

