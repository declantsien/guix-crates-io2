(define-module (crates-io xl is xlist) #:use-module (crates-io))

(define-public crate-xlist-0.0.0 (c (n "xlist") (v "0.0.0") (d (list (d (n "assert_cmd") (r "^2.0") (d #t) (k 0)) (d (n "clap") (r "^3.2") (f (quote ("derive" "env" "unicode" "cargo"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1nbrwnkgbxfkq4bhr018s94rw7za8q8cy1iazxzs86bs43vng0h8")))

