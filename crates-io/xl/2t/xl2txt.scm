(define-module (crates-io xl #{2t}# xl2txt) #:use-module (crates-io))

(define-public crate-xl2txt-0.1.0 (c (n "xl2txt") (v "0.1.0") (d (list (d (n "calamine") (r "^0.11.3") (d #t) (k 0)) (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)))) (h "0ga54dfi1zsvwvj5nqka7k5dxx40p203sc74j7vl12nmf6gxpm3b")))

(define-public crate-xl2txt-0.1.1 (c (n "xl2txt") (v "0.1.1") (d (list (d (n "calamine") (r "^0.11.4") (d #t) (k 0)) (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)))) (h "18zcy3kv80ijwb6nrqnq80rk85hhwc216i29dl1hq6isy9iifcz3")))

