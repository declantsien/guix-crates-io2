(define-module (crates-io xl _t xl_to_csv) #:use-module (crates-io))

(define-public crate-xl_to_csv-0.1.0 (c (n "xl_to_csv") (v "0.1.0") (d (list (d (n "calamine") (r "^0.19.1") (d #t) (k 0)) (d (n "csv") (r "^1.2.1") (d #t) (k 0)) (d (n "doe") (r "^0.1.32") (d #t) (k 0)))) (h "0vzlnd5mm9cvi15pi338gsr1b8gxrmvnghlyn36grhh6j85b32ln")))

(define-public crate-xl_to_csv-0.1.1 (c (n "xl_to_csv") (v "0.1.1") (d (list (d (n "calamine") (r "^0.19.1") (d #t) (k 0)) (d (n "csv") (r "^1.2.1") (d #t) (k 0)) (d (n "doe") (r "^0.1.32") (d #t) (k 0)))) (h "0r79lwmbym2zvh0id2br7x5cgrc8igm4dmikimndq26h9wvj5f80")))

(define-public crate-xl_to_csv-0.1.2 (c (n "xl_to_csv") (v "0.1.2") (d (list (d (n "calamine") (r "^0.19.1") (d #t) (k 0)) (d (n "csv") (r "^1.2.1") (d #t) (k 0)) (d (n "doe") (r "^0.1.32") (d #t) (k 0)))) (h "100rf1gb8rbj2bwdabb26cp88sj9dks6iqzp9qhbi3j818j8wsdc")))

