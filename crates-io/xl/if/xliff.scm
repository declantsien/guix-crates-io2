(define-module (crates-io xl if xliff) #:use-module (crates-io))

(define-public crate-xliff-0.1.0 (c (n "xliff") (v "0.1.0") (d (list (d (n "quick-xml") (r "^0.12.0") (d #t) (k 0)))) (h "1sag7gdrjhb334lacbirvpbgqyycndamp3k8pv4gw25dbwm1924f")))

(define-public crate-xliff-0.1.1 (c (n "xliff") (v "0.1.1") (d (list (d (n "quick-xml") (r "^0.15.0") (d #t) (k 0)))) (h "0xrf4h6g20mk4z5acfslprh7wi5sh70qbiim8ndyrc1r37vpwlha")))

(define-public crate-xliff-0.2.0 (c (n "xliff") (v "0.2.0") (d (list (d (n "quick-xml") (r "^0.15.0") (d #t) (k 0)))) (h "1rsxsqf7nfpngrvxxkf95yy2a3c8szbgqyxxxjpbdar7pjxzf036")))

(define-public crate-xliff-0.3.0-alpha.0 (c (n "xliff") (v "0.3.0-alpha.0") (d (list (d (n "quick-xml") (r "^0.17") (d #t) (k 0)))) (h "009pwjgnfp3ndsi9xih4ky5i8sxq74iz50ki1in4qgyh85vhxn85")))

(define-public crate-xliff-0.3.0-alpha.1 (c (n "xliff") (v "0.3.0-alpha.1") (d (list (d (n "quick-xml") (r "^0.17") (d #t) (k 0)))) (h "078s1vs8d7xz336p4xr1bml0r6n1x2sfa3glpvbcn7k49ss89xlh")))

(define-public crate-xliff-0.3.0-alpha.2 (c (n "xliff") (v "0.3.0-alpha.2") (d (list (d (n "quick-xml") (r "^0.17") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.63") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "1zs0lg0lcydx93paic8g42zs8mzxpkd9i4fkyhk31c504wxik348")))

(define-public crate-xliff-0.3.0-alpha.3 (c (n "xliff") (v "0.3.0-alpha.3") (d (list (d (n "quick-xml") (r "^0.17") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.63") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "1md7i24ff096l0kbky512fixpx3smsbkj30w4dy5bz35pyc0zw7q")))

(define-public crate-xliff-0.3.0-alpha.4 (c (n "xliff") (v "0.3.0-alpha.4") (d (list (d (n "quick-xml") (r "^0.17") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.63") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "014babsg4lgip97niwrbr04q7b5szx0wvnh2k45yldrdcfx5kqc3")))

