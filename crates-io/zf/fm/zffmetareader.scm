(define-module (crates-io zf fm zffmetareader) #:use-module (crates-io))

(define-public crate-zffmetareader-0.9.0 (c (n "zffmetareader") (v "0.9.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)) (d (n "zff") (r "^0.9") (d #t) (k 0)))) (h "0kcxyr3i4q0lxaidh2aqjjz9srgx17iii2dfrhd9ai5agycnqliv")))

(define-public crate-zffmetareader-0.10.0 (c (n "zffmetareader") (v "0.10.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)) (d (n "zff") (r "^0.10") (d #t) (k 0)))) (h "0kv1hnfk0dp4n6w976hs6k4jrp54dmpyr63acr7xnl6hxfyqmy39") (r "1.56.1")))

(define-public crate-zffmetareader-0.10.1 (c (n "zffmetareader") (v "0.10.1") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)) (d (n "zff") (r "^0.10") (d #t) (k 0)))) (h "0ksjdkw3rqjz3jkknd6fk696yp82b46qrckvaiimx619q3hvli45") (r "1.56.1")))

