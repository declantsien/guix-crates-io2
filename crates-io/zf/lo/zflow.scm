(define-module (crates-io zf lo zflow) #:use-module (crates-io))

(define-public crate-zflow-0.1.0 (c (n "zflow") (v "0.1.0") (d (list (d (n "assert-json-diff") (r "^2.0.2") (d #t) (k 0)) (d (n "beady") (r "^0.6.0") (d #t) (k 0)) (d (n "foreach") (r "^0.3.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nuid") (r "^0.3.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "03ssvvzjy2m47bbhjfks310i4c1ki0sn71qj4xsjlycx9gamccfc") (y #t)))

(define-public crate-zflow-0.1.1 (c (n "zflow") (v "0.1.1") (d (list (d (n "assert-json-diff") (r "^2.0.2") (d #t) (k 0)) (d (n "beady") (r "^0.6.0") (d #t) (k 0)) (d (n "foreach") (r "^0.3.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nuid") (r "^0.3.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0l6apj78c536kr2c45kwf59fdmyssf6ybw11bdvb12ahyyv690n9")))

(define-public crate-zflow-0.1.12 (c (n "zflow") (v "0.1.12") (d (list (d (n "assert-json-diff") (r "^2.0.2") (d #t) (k 0)) (d (n "beady") (r "^0.6.0") (d #t) (k 0)) (d (n "foreach") (r "^0.3.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nuid") (r "^0.3.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "19f3sx42mj4yjw9pazpzd5c68g8b1k6cypz9x211vk4xiq0f3231")))

