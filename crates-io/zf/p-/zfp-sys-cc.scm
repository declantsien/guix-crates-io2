(define-module (crates-io zf p- zfp-sys-cc) #:use-module (crates-io))

(define-public crate-zfp-sys-cc-0.1.0 (c (n "zfp-sys-cc") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.53") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "11gjx5z0lyxagrr1dj1qd9rlpnhg3sn7hrwsi2jrnglhqxiggl9q") (f (quote (("default") ("cuda"))))))

(define-public crate-zfp-sys-cc-0.1.1 (c (n "zfp-sys-cc") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.54") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1h4h7ykaslnlkv847b4zy6y3j7pf956m7b9l63zch5l9cqvsx5kz") (f (quote (("default") ("cuda"))))))

(define-public crate-zfp-sys-cc-0.1.2 (c (n "zfp-sys-cc") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.55") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "06ck40hjqf2zqknzas31jjslhnfsnqfs8fhcmr35h5gs5m2m5lxj") (f (quote (("default") ("cuda"))))))

(define-public crate-zfp-sys-cc-0.1.3 (c (n "zfp-sys-cc") (v "0.1.3") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0dwcdyhb9h1klkndhi3b6nmhkhzlanfjqdcjwayay192smmsh2n8") (f (quote (("default") ("cuda"))))))

(define-public crate-zfp-sys-cc-0.1.4 (c (n "zfp-sys-cc") (v "0.1.4") (d (list (d (n "bindgen") (r "^0.66.0") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0vml50ya15macl3prqiyv87bhxns2vzzvv319n1mx3jgklpp32nz") (f (quote (("default") ("cuda"))))))

