(define-module (crates-io zf il zfilexfer) #:use-module (crates-io))

(define-public crate-zfilexfer-0.0.2 (c (n "zfilexfer") (v "0.0.2") (d (list (d (n "crc") (r "^1.2") (d #t) (k 0)) (d (n "czmq") (r "^0.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "tempfile") (r "^2.1") (d #t) (k 2)) (d (n "zdaemon") (r "^0.0.2") (d #t) (k 0)))) (h "1dr9m5vq8n5f62pdyswmdnqhswp7lhvmxw3lr05a228783i3fgja") (y #t)))

