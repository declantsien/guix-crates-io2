(define-module (crates-io zf la zflags) #:use-module (crates-io))

(define-public crate-zflags-0.1.0 (c (n "zflags") (v "0.1.0") (d (list (d (n "clap") (r "~2.33") (d #t) (k 0)) (d (n "comn-pms") (r "^0.1.0") (d #t) (k 0)) (d (n "ctor") (r "~0.1.21") (d #t) (k 0)))) (h "04ghkrqdfavcy572vd065kfh6c7bg22fq3xjxsfxr5j7ld94pj9y")))

(define-public crate-zflags-0.1.1 (c (n "zflags") (v "0.1.1") (d (list (d (n "clap") (r "~2.33") (d #t) (k 0)) (d (n "comn-pms") (r "^0.1.0") (d #t) (k 0)) (d (n "ctor") (r "~0.1.21") (d #t) (k 0)))) (h "0kki8amjgaaqpww4pfmf54zr5i8amjgrldkpsr62wr2dwvqzyrd8")))

(define-public crate-zflags-0.1.2 (c (n "zflags") (v "0.1.2") (d (list (d (n "clap") (r "~2.33") (d #t) (k 0)) (d (n "comn-pms") (r "^0.1.0") (d #t) (k 0)) (d (n "ctor") (r "~0.1.21") (d #t) (k 0)))) (h "03ysxqyla9m2l5fzgfvqp4wj1m5zvp4c9jlkiz29jrcnwdz9wplb")))

