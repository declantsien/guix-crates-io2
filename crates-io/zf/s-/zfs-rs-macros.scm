(define-module (crates-io zf s- zfs-rs-macros) #:use-module (crates-io))

(define-public crate-zfs-rs-macros-0.0.0 (c (n "zfs-rs-macros") (v "0.0.0") (d (list (d (n "nvpair-sys") (r "^0.0.0") (d #t) (k 0) (p "zfs-rs-nvpair-sys")) (d (n "zfs-sys") (r "^0.0.0") (d #t) (k 0) (p "zfs-rs-zfs-sys")))) (h "06vixs8qygxdzn53jvayjyq1ln44nclrc1v7lqhkay3467xhdlqk")))

