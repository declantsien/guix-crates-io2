(define-module (crates-io zf s- zfs-cmd-api) #:use-module (crates-io))

(define-public crate-zfs-cmd-api-0.1.0 (c (n "zfs-cmd-api") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.1.0") (d #t) (k 0)) (d (n "enumflags2") (r "^0.5.2") (d #t) (k 0)) (d (n "enumflags2_derive") (r "^0.5.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.5") (d #t) (k 0)) (d (n "fmt-extra") (r "^0.2.1") (d #t) (k 0)) (d (n "shell-words") (r "^0.1.0") (d #t) (k 0)))) (h "1c1as060sqqijzjbdi305insd9230l87fy2lr6si0ckrj5c1yqc9")))

(define-public crate-zfs-cmd-api-0.2.0 (c (n "zfs-cmd-api") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.1.0") (d #t) (k 0)) (d (n "enumflags2") (r "^0.6.2") (d #t) (k 0)) (d (n "enumflags2_derive") (r "^0.6.2") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.5") (d #t) (k 0)) (d (n "fmt-extra") (r "^0.2.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "shell-words") (r "^0.1.0") (d #t) (k 0)))) (h "0sv8vjpygwl8m5w3gn3i531z6kjxg0xmb332hvsml5arc9dw406h")))

