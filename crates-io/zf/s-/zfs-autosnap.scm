(define-module (crates-io zf s- zfs-autosnap) #:use-module (crates-io))

(define-public crate-zfs-autosnap-0.3.1 (c (n "zfs-autosnap") (v "0.3.1") (d (list (d (n "byte-unit") (r "^4") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "subprocess") (r "^0.2") (d #t) (k 0)))) (h "1rzmlv5n30kdw0gcdn8rskvxxsi2wq1p3jla620sqlyx2mgvbmdq")))

