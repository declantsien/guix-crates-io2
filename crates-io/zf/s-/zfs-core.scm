(define-module (crates-io zf s- zfs-core) #:use-module (crates-io))

(define-public crate-zfs-core-0.2.0 (c (n "zfs-core") (v "0.2.0") (d (list (d (n "cstr-argument") (r "^0.1") (d #t) (k 0)) (d (n "foreign-types") (r "^0.3") (d #t) (k 0)) (d (n "nvpair") (r "^0.3") (d #t) (k 0)) (d (n "zfs-core-sys") (r "^0.1") (d #t) (k 0)))) (h "0wj0vp4qj31z9pv7w85ax2r7fv8nsz9hcpx8f2inrh4l5w4vrcm0")))

(define-public crate-zfs-core-0.4.0 (c (n "zfs-core") (v "0.4.0") (d (list (d (n "cstr-argument") (r "^0.1") (d #t) (k 0)) (d (n "foreign-types") (r "^0.5.0") (d #t) (k 0)) (d (n "nvpair") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "zfs-core-sys") (r "^0.4.0") (d #t) (k 0)))) (h "05q1dfip0a5rhgsyx3zmqsvjcq8n2975cqnbrx094lx34i5ynfs1")))

(define-public crate-zfs-core-0.5.0 (c (n "zfs-core") (v "0.5.0") (d (list (d (n "cstr-argument") (r "^0.1") (d #t) (k 0)) (d (n "foreign-types") (r "^0.5.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 2)) (d (n "nvpair") (r "^0.5.0") (d #t) (k 0)) (d (n "os_pipe") (r "^0.9") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "snafu") (r "^0.6") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "zfs-core-sys") (r "^0.5.0") (d #t) (k 0)))) (h "14xz7x3hq8dkfbxmgs3b2iszmpqv235fjdig9mv4gzbj6nk07fyp") (f (quote (("v2_00"))))))

