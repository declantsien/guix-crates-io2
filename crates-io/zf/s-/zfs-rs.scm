(define-module (crates-io zf s- zfs-rs) #:use-module (crates-io))

(define-public crate-zfs-rs-0.0.0 (c (n "zfs-rs") (v "0.0.0") (d (list (d (n "nvpair-sys") (r "^0.0.0") (d #t) (k 0) (p "zfs-rs-nvpair-sys")) (d (n "zfs-macros") (r "^0.0.0") (d #t) (k 0) (p "zfs-rs-macros")) (d (n "zfs-sys") (r "^0.0.0") (d #t) (k 0) (p "zfs-rs-zfs-sys")))) (h "04k4hl4468ng2p8x35i6qpllwg6awymx0m4n7z1fp0r6kpiz0yyy")))

