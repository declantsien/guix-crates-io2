(define-module (crates-io zf s- zfs-undelete) #:use-module (crates-io))

(define-public crate-zfs-undelete-0.0.1 (c (n "zfs-undelete") (v "0.0.1") (d (list (d (n "path-absolutize") (r "^3.0.14") (d #t) (k 0)))) (h "0ncg25fl05i44z0lnrqz9844i2xkx0wnrnqfcdgirv5zmgdfxvr4")))

(define-public crate-zfs-undelete-0.0.2 (c (n "zfs-undelete") (v "0.0.2") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "path-absolutize") (r "^3.0.14") (d #t) (k 0)))) (h "1b5ldfcsqh8xbg51x3z522brdv658c3qjhpymxx7jxa4820q3qpy")))

(define-public crate-zfs-undelete-0.0.3 (c (n "zfs-undelete") (v "0.0.3") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "path-absolutize") (r "^3.0.14") (d #t) (k 0)))) (h "063bwm48mn04dwd3p8an61pvdfshfqg4ha9r432lxac4xfc1pykb") (y #t)))

(define-public crate-zfs-undelete-0.1.0 (c (n "zfs-undelete") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "path-absolutize") (r "^3.0.14") (d #t) (k 0)))) (h "05i78b6c57bk8chsvap14rpj6dyp91qmclcab0dry8yr57wqvzms")))

