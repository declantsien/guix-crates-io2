(define-module (crates-io f1 #{28}# f128_input) #:use-module (crates-io))

(define-public crate-f128_input-0.1.0 (c (n "f128_input") (v "0.1.0") (d (list (d (n "f128_internal") (r "^0.1.0") (d #t) (k 0)))) (h "03dmvd9aqlz1hlcaprnrvbravfp1awbcxx7hhrm0ia8c2n1fr92m")))

(define-public crate-f128_input-0.2.0 (c (n "f128_input") (v "0.2.0") (d (list (d (n "f128_internal") (r "^0.2.0") (d #t) (k 0)))) (h "109pskx1wxxfnfpy872bzvhck2b4p35f7bbvscyrhmfmbryyv0nx")))

(define-public crate-f128_input-0.2.1 (c (n "f128_input") (v "0.2.1") (d (list (d (n "f128_internal") (r "^0.2.1") (d #t) (k 0)))) (h "1fqh8nlz9v8iap4mwwk3xvbfgrhkynl35jcriix60ia7yyk23a0q")))

