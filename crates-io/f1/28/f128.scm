(define-module (crates-io f1 #{28}# f128) #:use-module (crates-io))

(define-public crate-f128-0.1.0 (c (n "f128") (v "0.1.0") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)))) (h "11rpraiijd2jgi59zz7vlpr4jvxbx7ddj02cqr4bw0ib3ydsa4jw")))

(define-public crate-f128-0.1.1 (c (n "f128") (v "0.1.1") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)))) (h "18zs8inkyv60p541514iyhnmrjn7g5030wxsz9h0y4dl5vnq7azv")))

(define-public crate-f128-0.1.2 (c (n "f128") (v "0.1.2") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "1pn1n1wyaylpx5b36anabwa7zziawiiks360kwkx7c7zc0x9hhdl")))

(define-public crate-f128-0.1.3 (c (n "f128") (v "0.1.3") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "0dq6wa9cyhp546vsgsq1mfqddid98mac9gq18g8kdjc964n0izm3")))

(define-public crate-f128-0.2.0 (c (n "f128") (v "0.2.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.6") (d #t) (k 0)))) (h "1bh210zrycfh0jssfg42m19f6lbbcip4fqm2nhn5xyj12aqml0z4")))

(define-public crate-f128-0.2.1 (c (n "f128") (v "0.2.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.6") (d #t) (k 0)))) (h "1sn8i8nps0wlma89pc29mmjx997dfaqjhxa4qpwnijl21b42a30p")))

(define-public crate-f128-0.2.2 (c (n "f128") (v "0.2.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.6") (d #t) (k 0)))) (h "1cn0s9wip5vlqzs7i0kn37wlnnpc86jlh4w3386pn1p2szpf1mbk")))

(define-public crate-f128-0.2.3 (c (n "f128") (v "0.2.3") (d (list (d (n "f128_input") (r "^0.1.0") (d #t) (k 0)) (d (n "f128_internal") (r "^0.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.6") (d #t) (k 0)))) (h "1w3gv4f4jwpziia146v4cyn30iap4i0xmfpwk8pyz8g8d0pdh7xa")))

(define-public crate-f128-0.2.4 (c (n "f128") (v "0.2.4") (d (list (d (n "f128_input") (r "^0.1.0") (d #t) (k 0)) (d (n "f128_internal") (r "^0.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.6") (d #t) (k 0)))) (h "0spag0fm8acm6c1wwlgrkb2n8hxmwq0w0zs0hp9naiq66dh9c0i5")))

(define-public crate-f128-0.2.5 (c (n "f128") (v "0.2.5") (d (list (d (n "f128_input") (r "^0.2.0") (d #t) (k 0)) (d (n "f128_internal") (r "^0.2.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.6") (d #t) (k 0)))) (h "173845w3cxnx3b45amirmms7f3war1ih8hrrivvir0lmrvryd3pk")))

(define-public crate-f128-0.2.6 (c (n "f128") (v "0.2.6") (d (list (d (n "f128_input") (r "^0.2.1") (d #t) (k 0)) (d (n "f128_internal") (r "^0.2.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.6") (d #t) (k 0)))) (h "0nicb9xzi8j8ick2c042rr1k7vr4qms5mch7jnqy0i9ycf5figgq")))

(define-public crate-f128-0.2.7 (c (n "f128") (v "0.2.7") (d (list (d (n "f128_input") (r "^0.2.1") (d #t) (k 0)) (d (n "f128_internal") (r "^0.2.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.6") (d #t) (k 0)))) (h "02dbz9vhvi9w4wswp88w1yh527phiplyfjxc5mv47sss9s10ypvb")))

(define-public crate-f128-0.2.8 (c (n "f128") (v "0.2.8") (d (list (d (n "f128_input") (r "^0.2.1") (d #t) (k 0)) (d (n "f128_internal") (r "^0.2.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.6") (d #t) (k 0)))) (h "1inp8hp8fgw5i1ci1wb87k7nk81b8jjjkw6c13nv82pfggai6ncg")))

(define-public crate-f128-0.2.9 (c (n "f128") (v "0.2.9") (d (list (d (n "f128_input") (r "^0.2.1") (d #t) (k 0)) (d (n "f128_internal") (r "^0.2.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.6") (d #t) (k 0)))) (h "0b3dfs7y04wvcjr9rckykqpcg6n6mfahrcfckvjbkj440x9jjz8b")))

