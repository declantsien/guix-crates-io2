(define-module (crates-io lf ml lfml) #:use-module (crates-io))

(define-public crate-lfml-0.1.0 (c (n "lfml") (v "0.1.0") (d (list (d (n "axum-core") (r "^0.4.1") (o #t) (d #t) (k 0)) (d (n "http") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "itoa") (r "^1.0.9") (d #t) (k 0)) (d (n "lfml-escape") (r "^0.1.0") (d #t) (k 0)) (d (n "lfml-html5") (r "^0.1.0") (d #t) (k 0)) (d (n "lfml-macros") (r "^0.1.0") (d #t) (k 0)))) (h "0lc1axr5g3g16xgs5ha6hc647nbkbzgayv3x842jjx8l6fq5c9d4") (f (quote (("default" "axum") ("axum" "axum-core" "http"))))))

