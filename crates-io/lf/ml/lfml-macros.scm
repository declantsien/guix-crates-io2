(define-module (crates-io lf ml lfml-macros) #:use-module (crates-io))

(define-public crate-lfml-macros-0.1.0 (c (n "lfml-macros") (v "0.1.0") (d (list (d (n "lfml-escape") (r "^0.1.0") (d #t) (k 0)) (d (n "lfml-html5") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.70") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "02wwnp9mg4mmrghdmjmxmfzj46rrrls6s233wzhfgw3ybfdkn7ws")))

