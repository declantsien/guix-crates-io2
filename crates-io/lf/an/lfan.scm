(define-module (crates-io lf an lfan) #:use-module (crates-io))

(define-public crate-lfan-0.0.5 (c (n "lfan") (v "0.0.5") (d (list (d (n "linked-hash-map") (r "^0.5") (d #t) (k 0)) (d (n "tokio") (r "^1.2") (f (quote ("sync"))) (o #t) (d #t) (k 0)))) (h "1glqmrywdiw2nxxv8j9p9gv67fif2p9cxir37j8s8ccp096sf6hw") (f (quote (("default") ("async" "tokio"))))))

(define-public crate-lfan-0.0.6 (c (n "lfan") (v "0.0.6") (d (list (d (n "linked-hash-map") (r "^0.5") (d #t) (k 0)) (d (n "tokio") (r "^1.2") (f (quote ("sync"))) (o #t) (d #t) (k 0)))) (h "0c269ywwnlkpvidjhs42f0i3ymaqcqxy4cm27ivfxx61kap3w8py") (f (quote (("default") ("async" "tokio"))))))

(define-public crate-lfan-0.0.7 (c (n "lfan") (v "0.0.7") (d (list (d (n "linked-hash-map") (r "^0.5") (d #t) (k 0)) (d (n "tokio") (r "^1.2") (f (quote ("sync"))) (o #t) (d #t) (k 0)))) (h "156d1429h3fwh9as40lhspifm1r9h3lfz3z0bhqbd2d35xfz9wia") (f (quote (("default") ("async" "tokio"))))))

(define-public crate-lfan-0.0.8 (c (n "lfan") (v "0.0.8") (d (list (d (n "linked-hash-map") (r "^0.5") (d #t) (k 0)) (d (n "tokio") (r "^1.2") (f (quote ("sync"))) (o #t) (d #t) (k 0)))) (h "1x3n6m2ynsjgv0dpygrsxlbwbyhvnnzh9h3y5sj1i1zi4h3vngc3") (f (quote (("default") ("async" "tokio"))))))

(define-public crate-lfan-0.0.9 (c (n "lfan") (v "0.0.9") (d (list (d (n "linked-hash-map") (r "^0.5") (d #t) (k 0)) (d (n "tokio") (r "^1.2") (f (quote ("sync"))) (o #t) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (f (quote ("max_level_trace" "release_max_level_debug"))) (d #t) (k 0)))) (h "0zp5ibpax9fpnpcxfiz67jhqi9dap6hs137rpms4dm7ijg3myv53") (f (quote (("default") ("async" "tokio"))))))

(define-public crate-lfan-0.0.10 (c (n "lfan") (v "0.0.10") (d (list (d (n "linked-hash-map") (r "^0.5") (d #t) (k 0)) (d (n "tokio") (r "^1.2") (f (quote ("sync"))) (o #t) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (f (quote ("max_level_trace" "release_max_level_debug"))) (d #t) (k 0)))) (h "1s49wz23xrx3rbw0mjlnbkh6r4ad2b4422jvp68q9i5ywvs756mk") (f (quote (("default") ("async" "tokio"))))))

(define-public crate-lfan-0.1.0 (c (n "lfan") (v "0.1.0") (d (list (d (n "linked-hash-map") (r "^0.5") (d #t) (k 0)) (d (n "tokio") (r "^1.16") (f (quote ("sync"))) (o #t) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (f (quote ("max_level_trace" "release_max_level_debug"))) (d #t) (k 0)))) (h "1m85y8ckvrd81yb6xs36pdg8ijc4v8xd7365lxjgrygaika6l45d") (f (quote (("default") ("async" "tokio"))))))

(define-public crate-lfan-0.1.1 (c (n "lfan") (v "0.1.1") (d (list (d (n "linked-hash-map") (r "^0.5") (d #t) (k 0)) (d (n "tokio") (r "^1.16") (f (quote ("sync"))) (o #t) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (f (quote ("max_level_trace" "release_max_level_debug"))) (d #t) (k 0)))) (h "1sc4ypl9kanwd23nqqdjkkp0p9dsv5mplsbaf28gmxkbf1bfgd95") (f (quote (("default") ("async" "tokio"))))))

(define-public crate-lfan-0.2.4 (c (n "lfan") (v "0.2.4") (d (list (d (n "linked-hash-map") (r "^0.5") (d #t) (k 0)) (d (n "tokio") (r "^1.16") (f (quote ("sync"))) (o #t) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (f (quote ("max_level_trace" "release_max_level_debug"))) (d #t) (k 0)))) (h "01wavpxbm42zq8bv6l3c3v6ig0h75bdrlzaxfkzgwvv1nyp6l3vh") (f (quote (("default") ("async" "tokio"))))))

(define-public crate-lfan-0.2.5 (c (n "lfan") (v "0.2.5") (d (list (d (n "linked-hash-map") (r "^0.5") (d #t) (k 0)) (d (n "tokio") (r "^1.16") (f (quote ("sync"))) (o #t) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (f (quote ("max_level_trace" "release_max_level_debug"))) (d #t) (k 0)))) (h "1v5fkk4al7kl4nqhqdmml9h7vkkjh266b66n94w4mbkm71br3x23") (f (quote (("default") ("async" "tokio"))))))

(define-public crate-lfan-0.2.6 (c (n "lfan") (v "0.2.6") (d (list (d (n "linked-hash-map") (r "^0.5") (d #t) (k 0)) (d (n "tokio") (r "^1.16") (f (quote ("sync"))) (o #t) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (f (quote ("max_level_trace" "release_max_level_debug"))) (d #t) (k 0)))) (h "1d33gvva0wg7g0hvbmg0qh6pm6ab1xa4cp4bjmlnjy7hs9pliah2") (f (quote (("default") ("async" "tokio"))))))

