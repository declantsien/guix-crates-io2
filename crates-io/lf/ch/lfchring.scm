(define-module (crates-io lf ch lfchring) #:use-module (crates-io))

(define-public crate-lfchring-0.1.0 (c (n "lfchring") (v "0.1.0") (d (list (d (n "crossbeam-epoch") (r "~0.9.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.8") (d #t) (k 2)) (d (n "hex-literal") (r "^0.3") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "07k2d529jndpkqy5n6cq8rs9jfhfh2w5xnyp4f9cca291jml3avp") (y #t)))

(define-public crate-lfchring-0.1.1 (c (n "lfchring") (v "0.1.1") (d (list (d (n "crossbeam-epoch") (r "~0.9.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.8") (d #t) (k 2)) (d (n "hex-literal") (r "^0.3") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1772gwp97axmcda0jf968bm48n79cwf17nkg28hd0jzqsv7dhsbz") (y #t)))

(define-public crate-lfchring-0.1.2 (c (n "lfchring") (v "0.1.2") (d (list (d (n "crossbeam-epoch") (r "~0.9.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.8") (d #t) (k 2)) (d (n "hex-literal") (r "^0.3") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0m0jpj0sxk5lymmpgyblggsirr5l3mgpdiqi4ljhib7j79d4yag8")))

(define-public crate-lfchring-0.1.3 (c (n "lfchring") (v "0.1.3") (d (list (d (n "blake2b_simd") (r "~0.5.11") (o #t) (d #t) (k 0)) (d (n "blake3") (r "~0.3.7") (o #t) (d #t) (k 0)) (d (n "crossbeam-epoch") (r "~0.9.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.8") (d #t) (k 2)) (d (n "hex-literal") (r "^0.3") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0sgb7d4wcrkgh3kki4p0js6gw2rrazyjb5hmngz9rgpa7nw2i1is") (f (quote (("default") ("blake3-hash" "blake3") ("blake2b-hash" "blake2b_simd"))))))

