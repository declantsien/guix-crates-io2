(define-module (crates-io lf -s lf-sll) #:use-module (crates-io))

(define-public crate-lf-sll-0.0.1 (c (n "lf-sll") (v "0.0.1") (h "10z3lnvwwg0sjid1h6k2hpzmikhj3fjy7xk3hnpqn2fpb1zrksjr")))

(define-public crate-lf-sll-0.0.2 (c (n "lf-sll") (v "0.0.2") (h "0hb7ddbndnszyjwj7cdywz6jswba654jff7bxrc9r3ds7xh2mcp5")))

(define-public crate-lf-sll-0.0.3 (c (n "lf-sll") (v "0.0.3") (h "00z9a262bpa0z3c1027770xq4i5g8dw70blnd4iblfsrmifa2z5c")))

