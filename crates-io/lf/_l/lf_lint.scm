(define-module (crates-io lf _l lf_lint) #:use-module (crates-io))

(define-public crate-lf_lint-0.1.0 (c (n "lf_lint") (v "0.1.0") (d (list (d (n "clap") (r "^4.1.13") (f (quote ("derive"))) (d #t) (k 0)) (d (n "content_inspector") (r "^0.2.4") (d #t) (k 0)) (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.3") (d #t) (k 0)))) (h "0xck8wvsvgk57jwwkxjapvxfizkp6f1h8anqy7w3y7dy1dn9wfhw")))

