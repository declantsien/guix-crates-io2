(define-module (crates-io lf s- lfs-core) #:use-module (crates-io))

(define-public crate-lfs-core-0.1.0 (c (n "lfs-core") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "092i9h58xd19i7akdjw3kg0379gr8ws8sx18krnmbk7rpc923dmj")))

(define-public crate-lfs-core-0.1.1 (c (n "lfs-core") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0gn0lpdiqhiqxwwpx20al5vjgafdqikqjgpi2pip3id9a4c4qchk")))

(define-public crate-lfs-core-0.2.0 (c (n "lfs-core") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "19rp11ayirr8n1gchppndmqj48ljvkqqymqh5x3yvzspsk9m8a96")))

(define-public crate-lfs-core-0.3.0 (c (n "lfs-core") (v "0.3.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0xsj5vcagsga4kq5c1njqg0fv7pgf9xg9bpg90wjn74kdaphgc41")))

(define-public crate-lfs-core-0.3.1 (c (n "lfs-core") (v "0.3.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1ajwr63n8yg8p9qbsw8yvymgllihg3rqqdhrvlkl62mg57888kpp")))

(define-public crate-lfs-core-0.3.2 (c (n "lfs-core") (v "0.3.2") (d (list (d (n "lazy-regex") (r "^2.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0qans7r1z8yhy1ay9sgvzh0s7vf0m77a94s59n95spkq6ghif350")))

(define-public crate-lfs-core-0.3.3 (c (n "lfs-core") (v "0.3.3") (d (list (d (n "lazy-regex") (r "^2.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "08f6gmns0c1mcrmssdbc43gm0kcqiycnyhdabinhcw7lpbkis24f")))

(define-public crate-lfs-core-0.3.4 (c (n "lfs-core") (v "0.3.4") (d (list (d (n "lazy-regex") (r "^2.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "07l0cd85l70fkf1009lqa7129329g22i32sq8xzvnp4sr6zmj5xz")))

(define-public crate-lfs-core-0.4.0 (c (n "lfs-core") (v "0.4.0") (d (list (d (n "lazy-regex") (r "^2.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1y05kd7axr4gzslwjxab7km1gmrgdylm01929db2k3hnsw0szbm1")))

(define-public crate-lfs-core-0.4.1 (c (n "lfs-core") (v "0.4.1") (d (list (d (n "lazy-regex") (r "^2.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "011p0j3pwshqj2dqmamm2bar8bjlqr12alr3i7q6q6cngwhgp7yj")))

(define-public crate-lfs-core-0.4.2 (c (n "lfs-core") (v "0.4.2") (d (list (d (n "lazy-regex") (r "^2.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0b13biijyg517zniwnnmhg70j81v8jbagsyrzi8jpcampm91kw6x")))

(define-public crate-lfs-core-0.5.0 (c (n "lfs-core") (v "0.5.0") (d (list (d (n "lazy-regex") (r "^2.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1mjhdwnbsl659wn4jggqvp8w29vixqq07i80dip3cf6ski721bna")))

(define-public crate-lfs-core-0.5.1 (c (n "lfs-core") (v "0.5.1") (d (list (d (n "lazy-regex") (r "^2.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0xxx2anbz1k6qrvlj416yaz0szcx79xr2zfapf9ijb9jak1y767l")))

(define-public crate-lfs-core-0.6.0 (c (n "lfs-core") (v "0.6.0") (d (list (d (n "lazy-regex") (r "^2.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0z3vjsdz4yfyfs7nf33hi2dpxmg5cpy72rirg3jinkv0za4x298h")))

(define-public crate-lfs-core-0.7.0 (c (n "lfs-core") (v "0.7.0") (d (list (d (n "lazy-regex") (r "^2.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0vbz49z6z88al8igqc4yk3k93g5d97zg6a321360pifhrif0v0x7")))

(define-public crate-lfs-core-0.8.0 (c (n "lfs-core") (v "0.8.0") (d (list (d (n "lazy-regex") (r "^2.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "snafu") (r "^0.7") (d #t) (k 0)))) (h "1mhl7rqbin389143bg5nwcxai45zacwp4sa2z6ag1fb8f0vh0fmi")))

(define-public crate-lfs-core-0.8.1 (c (n "lfs-core") (v "0.8.1") (d (list (d (n "lazy-regex") (r "^2.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "snafu") (r "^0.7") (d #t) (k 0)))) (h "1xbzyi30i7l2xx05djn8cq4r50rx4ad843vb1w4p6np93zwmws2i")))

(define-public crate-lfs-core-0.9.0 (c (n "lfs-core") (v "0.9.0") (d (list (d (n "lazy-regex") (r "^2.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "snafu") (r "^0.7") (d #t) (k 0)))) (h "0b75bwawjjr3qcswn5svs71qq8kwapvdrx9q1pn98q01ma0f7fps")))

(define-public crate-lfs-core-0.9.1 (c (n "lfs-core") (v "0.9.1") (d (list (d (n "lazy-regex") (r "^2.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "snafu") (r "^0.7") (d #t) (k 0)))) (h "18lcbpfd3rll69s0jf7nm60ml70g1xvpnwjx015l34nxk6gzpfrr")))

(define-public crate-lfs-core-0.10.0 (c (n "lfs-core") (v "0.10.0") (d (list (d (n "lazy-regex") (r "^2.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "snafu") (r "^0.7") (d #t) (k 0)))) (h "0dny5yp7g0qx2f73vrsq725gl3gqcy1q4n544asf8yhnc6f9qqn1")))

(define-public crate-lfs-core-0.10.1 (c (n "lfs-core") (v "0.10.1") (d (list (d (n "lazy-regex") (r "^2.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "snafu") (r "^0.7") (d #t) (k 0)))) (h "0kjk8742fxp39spkg7yvaij39w7gdl68f2mz1s4bb0j1ar9g5hgq")))

(define-public crate-lfs-core-0.10.2 (c (n "lfs-core") (v "0.10.2") (d (list (d (n "lazy-regex") (r "^2.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "snafu") (r "^0.7") (d #t) (k 0)))) (h "0g1yqdf9pv9qmimhpj8v250ywyhafbyjh4khkvrgqgsg12l0lgpc")))

(define-public crate-lfs-core-0.11.0 (c (n "lfs-core") (v "0.11.0") (d (list (d (n "lazy-regex") (r "^2.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "snafu") (r "^0.7") (d #t) (k 0)))) (h "17l8f4my559w09j3kapfihh5ja0qk2frq4ljzci95hhi72lw0l7k")))

(define-public crate-lfs-core-0.11.1 (c (n "lfs-core") (v "0.11.1") (d (list (d (n "lazy-regex") (r "^2.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "snafu") (r "^0.7") (d #t) (k 0)))) (h "07yl7ih12nqj2nbbxzjbbr1i51yffglnn7k3vhprvxchfk9l2i1c")))

(define-public crate-lfs-core-0.11.2 (c (n "lfs-core") (v "0.11.2") (d (list (d (n "lazy-regex") (r "^3.0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "snafu") (r "^0.7") (d #t) (k 0)))) (h "0nj5rcpawbiys2f178r9ajy39k59ncl6zp8b4w28l5z1aqd9zakm")))

