(define-module (crates-io lf u_ lfu_cache) #:use-module (crates-io))

(define-public crate-lfu_cache-1.0.0 (c (n "lfu_cache") (v "1.0.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1dc5gm8drhxcsfjavav7qgdgvyvkw4bdi5216qhqb1v1rrzm3h4j") (y #t)))

(define-public crate-lfu_cache-1.1.0 (c (n "lfu_cache") (v "1.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0f9mkmdqz01jps4d5lhn469ky86d6asdxniqfa87r0dyrk14jyzv") (y #t)))

(define-public crate-lfu_cache-1.1.1 (c (n "lfu_cache") (v "1.1.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0d7z41n7688b3m6fwi9savjx8j485mm761rgn45mh0v2mavsbj87")))

(define-public crate-lfu_cache-1.2.0 (c (n "lfu_cache") (v "1.2.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1pxbzngvhk2444l0ia50i2snwwidjp4kshbxy7hsp9rs1mcadgrk")))

(define-public crate-lfu_cache-1.2.1 (c (n "lfu_cache") (v "1.2.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "11k4y67s73v9j3762qa0yiqqzfkdpfc2wpnqhq2sizg5jmpybli9")))

(define-public crate-lfu_cache-1.2.2 (c (n "lfu_cache") (v "1.2.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "00zczlgrv5zf8y7npxrh74q4xkgd8jlf1ivj3yvsvjyggpg6hzk8")))

(define-public crate-lfu_cache-1.3.0 (c (n "lfu_cache") (v "1.3.0") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)))) (h "0290avpwkdri0xsk67qymgshnmw5fz878135il4k3xz8k8y3qcqq")))

