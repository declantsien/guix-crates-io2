(define-module (crates-io lf sr lfsr-base) #:use-module (crates-io))

(define-public crate-lfsr-base-0.2.0 (c (n "lfsr-base") (v "0.2.0") (h "1b8ndrs4sj0yxbgcdv7hbm9wlqp6khvhgqy8ps3mrmairwx8m3pn")))

(define-public crate-lfsr-base-0.3.0 (c (n "lfsr-base") (v "0.3.0") (h "0v4l5m5i5kcnbldvqk89zddaf9qhgcciciqm3zsb4s8cxzjghm8k")))

