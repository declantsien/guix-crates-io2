(define-module (crates-io lf sr lfsr-macro-generate) #:use-module (crates-io))

(define-public crate-lfsr-macro-generate-0.2.0 (c (n "lfsr-macro-generate") (v "0.2.0") (d (list (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "lfsr-base") (r "^0.2.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.30") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "syn") (r "^0.15.34") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1x3bs85axjvn79jjbf3vvskg96g7dl4fi39fvm5z194dxfx145bx")))

(define-public crate-lfsr-macro-generate-0.3.0 (c (n "lfsr-macro-generate") (v "0.3.0") (d (list (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "lfsr-base") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.30") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "syn") (r "^0.15.34") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1xng1qq3m8lxvmayp306lm0qh8lc8r4l158q7qxh2zaxkrsbx5xp")))

