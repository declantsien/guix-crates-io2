(define-module (crates-io lf sr lfsr) #:use-module (crates-io))

(define-public crate-lfsr-0.1.0 (c (n "lfsr") (v "0.1.0") (h "1d652b9cizyyafh72fb0mcnb4q6wij877p0npqg8gmywihnqm9hc")))

(define-public crate-lfsr-0.2.0 (c (n "lfsr") (v "0.2.0") (d (list (d (n "lfsr-base") (r "^0.2.0") (d #t) (k 0)) (d (n "lfsr-instances") (r "^0.2.0") (d #t) (k 0)) (d (n "lfsr-macro-generate") (r "^0.2.0") (d #t) (k 0)) (d (n "lfsr-macro-lookup") (r "^0.2.0") (d #t) (k 0)))) (h "1l5vk7bhs528kscdfv7ih95z7ix5s3kzj6pb6plq7nfjh5d2x53v")))

(define-public crate-lfsr-0.3.0 (c (n "lfsr") (v "0.3.0") (d (list (d (n "lfsr-base") (r "^0.3.0") (d #t) (k 0)) (d (n "lfsr-instances") (r "^0.3.0") (d #t) (k 0)) (d (n "lfsr-macro-generate") (r "^0.3.0") (d #t) (k 0)) (d (n "lfsr-macro-lookup") (r "^0.3.0") (d #t) (k 0)))) (h "0mvw0k85klmhwzcbgr34kvpbxg7m8xbbhimag0m547y9kxawa20l")))

