(define-module (crates-io lf sr lfsr-macro-lookup) #:use-module (crates-io))

(define-public crate-lfsr-macro-lookup-0.2.0 (c (n "lfsr-macro-lookup") (v "0.2.0") (d (list (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "lfsr-base") (r "^0.2.0") (d #t) (k 0)) (d (n "lfsr-instances") (r "^0.2.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.30") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "syn") (r "^0.15.34") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "10adxwa43p9011gbll93a52z0p310vj7y8bk4fl0062fd3g67rxg")))

(define-public crate-lfsr-macro-lookup-0.3.0 (c (n "lfsr-macro-lookup") (v "0.3.0") (d (list (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "lfsr-base") (r "^0.3.0") (d #t) (k 0)) (d (n "lfsr-instances") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.30") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "syn") (r "^0.15.34") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1y6dc8sdjkcyp030silizp4amsk5nm5c3bclkwfciam7hmm3lfmc")))

