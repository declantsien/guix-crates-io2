(define-module (crates-io lf sr lfsr-instances) #:use-module (crates-io))

(define-public crate-lfsr-instances-0.2.0 (c (n "lfsr-instances") (v "0.2.0") (d (list (d (n "lfsr-base") (r "^0.2.0") (d #t) (k 0)) (d (n "lfsr-macro-generate") (r "^0.2.0") (d #t) (k 0)))) (h "0xjz4qp7kdhwh8nszvbgi04s2kd09ay15x0w5279hq7mpkq81dgj")))

(define-public crate-lfsr-instances-0.3.0 (c (n "lfsr-instances") (v "0.3.0") (d (list (d (n "lfsr-base") (r "^0.3.0") (d #t) (k 0)) (d (n "lfsr-macro-generate") (r "^0.3.0") (d #t) (k 0)))) (h "0xgvbfaayx9r0bjzsss66zx8pdj6yfqzz76qqsawki147d7gvhj0")))

