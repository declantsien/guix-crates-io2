(define-module (crates-io lf t- lft-rust) #:use-module (crates-io))

(define-public crate-lft-rust-0.1.0 (c (n "lft-rust") (v "0.1.0") (d (list (d (n "crossbeam-channel") (r "^0.5.4") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 2)) (d (n "num_cpus") (r "^1.13") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 2)))) (h "05s0wnqn11cd9fmc1f0jy82yhcb1qjqsyk1xw3jzdj60vj9rh1v3") (r "1.57.0")))

