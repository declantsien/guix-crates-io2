(define-module (crates-io lf a_ lfa_derive) #:use-module (crates-io))

(define-public crate-lfa_derive-0.1.0 (c (n "lfa_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "0srzivw3irdxwyb29v9lasxrhdnycj066nn2l9v4mhaggwdnirad")))

(define-public crate-lfa_derive-0.12.0 (c (n "lfa_derive") (v "0.12.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "15gh3p4kp14s285vxig6fnb6k8v854qsaqg2mwfwc60m43bd9jf1")))

(define-public crate-lfa_derive-0.13.0 (c (n "lfa_derive") (v "0.13.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "07a831fc7xrnd499x5npf6i47hi8xbjdsahc571bk4axhs9nl2xj")))

