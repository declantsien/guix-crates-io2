(define-module (crates-io lf cl lfclib) #:use-module (crates-io))

(define-public crate-lfclib-0.0.1 (c (n "lfclib") (v "0.0.1") (d (list (d (n "libusb") (r "^0.3") (d #t) (k 0)))) (h "0bxc2j40r1m4m06m75wzqcq1q6sfwmm138rbha6rjrjlm4wzg15v")))

(define-public crate-lfclib-0.1.1 (c (n "lfclib") (v "0.1.1") (d (list (d (n "hidapi") (r "~0.4") (d #t) (k 0)))) (h "0dip0i3xmkziqnkw5ycdf57clqrssj41qxw14g01bw2qi7hxiwkg")))

