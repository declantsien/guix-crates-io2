(define-module (crates-io lf cl lfcl) #:use-module (crates-io))

(define-public crate-lfcl-0.0.1 (c (n "lfcl") (v "0.0.1") (d (list (d (n "clap") (r "~2.26") (k 0)) (d (n "lfclib") (r "^0.0.1") (d #t) (k 0)))) (h "1adxpxhqq5nvwc81hacbxhfmaizik2sj1gl8vfnp12s3fhmwspzv")))

(define-public crate-lfcl-0.0.2 (c (n "lfcl") (v "0.0.2") (d (list (d (n "clap") (r "~2.26") (k 0)) (d (n "lfclib") (r "^0.1.1") (d #t) (k 0)))) (h "1vhjx65d6li811mnsnzhi1vp2ra4pjk0x1bhdg5855w8p574xlx7")))

