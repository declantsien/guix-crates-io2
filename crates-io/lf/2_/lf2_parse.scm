(define-module (crates-io lf #{2_}# lf2_parse) #:use-module (crates-io))

(define-public crate-lf2_parse-0.1.0 (c (n "lf2_parse") (v "0.1.0") (d (list (d (n "lf2_codec") (r "^0.2.1") (d #t) (k 0)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)) (d (n "tinyvec") (r "^1.0.1") (f (quote ("alloc"))) (d #t) (k 0)))) (h "0ylf58faldr04c2xzhj3pjhxng5zj0zc2vzj57mljgdvxr3r772a")))

