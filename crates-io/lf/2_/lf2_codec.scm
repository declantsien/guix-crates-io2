(define-module (crates-io lf #{2_}# lf2_codec) #:use-module (crates-io))

(define-public crate-lf2_codec-0.1.0 (c (n "lf2_codec") (v "0.1.0") (h "1n1k9jizsh79ivcy0r5ajljkj0madahf3sfni98ckmqsyi82ld62")))

(define-public crate-lf2_codec-0.2.0 (c (n "lf2_codec") (v "0.2.0") (h "1j03q4qnw50zljcn12ca8bqw61rijw7mwrgzzfljznp0mm9wh13x")))

(define-public crate-lf2_codec-0.2.1 (c (n "lf2_codec") (v "0.2.1") (h "0ys1ib1a0561wya4mpaq9dfq25zliabkjrbqphkpx6ig29a6hwjh")))

