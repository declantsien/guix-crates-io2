(define-module (crates-io sg it sgit) #:use-module (crates-io))

(define-public crate-sgit-0.1.0 (c (n "sgit") (v "0.1.0") (d (list (d (n "clap") (r "^3.2.14") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "0jj9kghryprv3kk2pyvywk32y3kcqhl9rdp2whjrsv695dx2zhr5")))

(define-public crate-sgit-0.1.1 (c (n "sgit") (v "0.1.1") (d (list (d (n "clap") (r "^3.2.14") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 0)) (d (n "rust-ini") (r "^0.18") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "1clb7s4bqi85xdy23achwyyk8qk48w65ankkcf7x84favyjd7aii")))

(define-public crate-sgit-0.1.2 (c (n "sgit") (v "0.1.2") (d (list (d (n "clap") (r "^3.2.14") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "octocrab") (r "^0.16") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 0)) (d (n "rust-ini") (r "^0.18") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "tokio") (r "^1.20.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0hwnx8l4pa5nxwdmhaljy8lb5x0djkcpnmw01gzkfygm2zgpr82y")))

