(define-module (crates-io sg gi sggit) #:use-module (crates-io))

(define-public crate-sggit-0.0.1 (c (n "sggit") (v "0.0.1") (d (list (d (n "clap") (r "^3.2.13") (f (quote ("derive"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0pkzk07jy1253ya0bb7xpa5gswr00mfpi4r7y3afmlgpjnf6y059")))

(define-public crate-sggit-0.0.2 (c (n "sggit") (v "0.0.2") (d (list (d (n "clap") (r "^3.2.13") (f (quote ("derive"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tilde-expand") (r "^0.1.1") (d #t) (k 0)))) (h "09bi7bzlwccp1x3rrwhr2yn9if624y0l5ccw3z3dvcapgv77wgpc")))

(define-public crate-sggit-0.0.3 (c (n "sggit") (v "0.0.3") (d (list (d (n "clap") (r "^3.2.13") (f (quote ("derive"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tilde-expand") (r "^0.1.1") (d #t) (k 0)))) (h "1vgq1pjs3v4v8dv038vm42lb2wg4n1j3zxjf7647qj8mlyd360fz")))

(define-public crate-sggit-0.0.4 (c (n "sggit") (v "0.0.4") (d (list (d (n "clap") (r "^3.2.13") (f (quote ("derive"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tilde-expand") (r "^0.1.1") (d #t) (k 0)))) (h "151ch10bx1vsvmfankss2zlp5i1r1hva5gs108v6cyx88qrn7vk3")))

