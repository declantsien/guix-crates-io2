(define-module (crates-io sg -m sg-metadata) #:use-module (crates-io))

(define-public crate-sg-metadata-0.14.0 (c (n "sg-metadata") (v "0.14.0") (d (list (d (n "schemars") (r "^0.8.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (k 0)))) (h "131rdm1k47ms65j2a8h790fvqdli9208nhvdx94hbzxwig2lf9c6")))

(define-public crate-sg-metadata-0.20.0 (c (n "sg-metadata") (v "0.20.0") (d (list (d (n "cosmwasm-schema") (r "^1.1.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.133") (f (quote ("derive"))) (k 0)))) (h "0srivhq12cixcnj1zcf0mn9frx4250m2jldszm8k09zh40fyjgbb")))

(define-public crate-sg-metadata-0.21.0 (c (n "sg-metadata") (v "0.21.0") (d (list (d (n "cosmwasm-schema") (r "^1.1.5") (d #t) (k 0)) (d (n "schemars") (r "^0.8.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.133") (f (quote ("derive"))) (k 0)))) (h "0rrp7fh7j4n64dzidq9s7r8zqj1mb6hpqgkd33gsgl8z10z97jf2")))

(define-public crate-sg-metadata-0.21.1 (c (n "sg-metadata") (v "0.21.1") (d (list (d (n "cosmwasm-schema") (r "^1.1.5") (d #t) (k 0)) (d (n "schemars") (r "^0.8.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.133") (f (quote ("derive"))) (k 0)))) (h "152bbsckzglx5snx3scg1ya3ajd3lm6h0gkvvg6hgs48xw8nh213")))

(define-public crate-sg-metadata-0.21.2 (c (n "sg-metadata") (v "0.21.2") (d (list (d (n "cosmwasm-schema") (r "^1.1.5") (d #t) (k 0)) (d (n "schemars") (r "^0.8.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.133") (f (quote ("derive"))) (k 0)))) (h "10dw6cxyrfyvs9ang9jvriia1r7zlsxf5ayhk4rrknbhpbx95pfy")))

(define-public crate-sg-metadata-0.21.3 (c (n "sg-metadata") (v "0.21.3") (d (list (d (n "cosmwasm-schema") (r "^1.1.5") (d #t) (k 0)) (d (n "schemars") (r "^0.8.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.133") (f (quote ("derive"))) (k 0)))) (h "0bw3bzrqp5k8q36l8vp644wr6nay2qg46512wj1m7ws65ibqnwjz")))

(define-public crate-sg-metadata-0.21.4 (c (n "sg-metadata") (v "0.21.4") (d (list (d (n "cosmwasm-schema") (r "^1.1.5") (d #t) (k 0)) (d (n "schemars") (r "^0.8.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.133") (f (quote ("derive"))) (k 0)))) (h "161r2az0nkwyq1ss3xqr8p5y2rjxjlf3pahyc6a33r8hdi2xayhg")))

(define-public crate-sg-metadata-0.21.5 (c (n "sg-metadata") (v "0.21.5") (d (list (d (n "cosmwasm-schema") (r "^1.1.5") (d #t) (k 0)) (d (n "schemars") (r "^0.8.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.133") (f (quote ("derive"))) (k 0)))) (h "15jksf2yxws0glmhiccca4y4jh85yb90y0ygmw1rzw8p8yyjj6z4")))

(define-public crate-sg-metadata-0.21.6 (c (n "sg-metadata") (v "0.21.6") (d (list (d (n "cosmwasm-schema") (r "^1.1.5") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)))) (h "1rynwj3swahnj9ljxk0l3afbdk0vgrx28dfxqs0x1y3ldk73bwp6")))

(define-public crate-sg-metadata-0.21.7 (c (n "sg-metadata") (v "0.21.7") (d (list (d (n "cosmwasm-schema") (r "^1.1.5") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)))) (h "16qqw409wsqy1m6y98l5qg2z3hi0fbjdjkgms71hz6i0m569i90v")))

(define-public crate-sg-metadata-0.21.8 (c (n "sg-metadata") (v "0.21.8") (d (list (d (n "cosmwasm-schema") (r "^1.1.5") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)))) (h "0qvnlx8hg53br8bps7xy6nlchyc2bbcdwzqiigh1dr4ciswq9yn5")))

(define-public crate-sg-metadata-0.22.0 (c (n "sg-metadata") (v "0.22.0") (d (list (d (n "cosmwasm-schema") (r "^1.1.5") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)))) (h "0mq6bhkbrrcxcqiism5lacg92dqiiy3dqql7z9lwj89k7xgzly4y")))

(define-public crate-sg-metadata-0.21.9 (c (n "sg-metadata") (v "0.21.9") (d (list (d (n "cosmwasm-schema") (r "^1.1.5") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)))) (h "1fb4anza0q35xfk0b3z1r69xh64siibrrzv439f07p3529p6jh1c")))

(define-public crate-sg-metadata-0.21.10 (c (n "sg-metadata") (v "0.21.10") (d (list (d (n "cosmwasm-schema") (r "^1.1.5") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)))) (h "099yq2prphjz7z49hwahzbhfsvb18as5ywdys9m6kqq39ifw7g94")))

(define-public crate-sg-metadata-0.21.12 (c (n "sg-metadata") (v "0.21.12") (d (list (d (n "cosmwasm-schema") (r "^1.1.5") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)))) (h "09fy9q4rczsiwgh4ixslfd2vgrwpqm8qiysnn7x212azsk4p30pm")))

(define-public crate-sg-metadata-0.22.1 (c (n "sg-metadata") (v "0.22.1") (d (list (d (n "cosmwasm-schema") (r "^1.2.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)))) (h "0cb1yd278y54nbf9lxzmkk4ywscpy4cqdvayb2vk44ylh4xx1pas")))

(define-public crate-sg-metadata-0.22.2 (c (n "sg-metadata") (v "0.22.2") (d (list (d (n "cosmwasm-schema") (r "^1.2.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)))) (h "1h3kdp1il0wqvfvd1y9vhy3z3n2n77ln50kc60gz7axkga93b63b")))

(define-public crate-sg-metadata-0.22.3 (c (n "sg-metadata") (v "0.22.3") (d (list (d (n "cosmwasm-schema") (r "^1.2.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)))) (h "1q90dgjhjszxh235waqlnw8l126ppnm9mmh15fk2gc5aiz0fb1gi")))

(define-public crate-sg-metadata-0.22.4 (c (n "sg-metadata") (v "0.22.4") (d (list (d (n "cosmwasm-schema") (r "^1.2.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)))) (h "1cvm91c6d9l2rsd9a73ravj5mrazdja20m9zf1hqblca4d80w1c7")))

(define-public crate-sg-metadata-0.22.5 (c (n "sg-metadata") (v "0.22.5") (d (list (d (n "cosmwasm-schema") (r "^1.2.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)))) (h "0ywkccjfhyxgfkkq8n3ig5hb54b0mim1gabr8gm3p0plnzpday7z")))

(define-public crate-sg-metadata-0.22.6 (c (n "sg-metadata") (v "0.22.6") (d (list (d (n "cosmwasm-schema") (r "^1.2.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)))) (h "08mczxq7757k8f40p78mr9mnqvqzd5aaal3dw88ki5kidbn4az4i")))

(define-public crate-sg-metadata-0.22.7 (c (n "sg-metadata") (v "0.22.7") (d (list (d (n "cosmwasm-schema") (r "^1.2.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)))) (h "0if8s0g4xxzxnn3mmkwq5kj3rhxzmsd62spwzlywbhgi0y112fsy")))

(define-public crate-sg-metadata-0.22.8 (c (n "sg-metadata") (v "0.22.8") (d (list (d (n "cosmwasm-schema") (r "^1.2.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)))) (h "13b98drp58x1mag5gaignk9blxlqjmpksh1x0wrl74gn0gydgm2b")))

(define-public crate-sg-metadata-0.22.9 (c (n "sg-metadata") (v "0.22.9") (d (list (d (n "cosmwasm-schema") (r "^1.2.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)))) (h "12fi4qrbv5zgyvzx0avvvv6f050sixhakk0idf7y2y0i8nygv4v4")))

(define-public crate-sg-metadata-0.22.10 (c (n "sg-metadata") (v "0.22.10") (d (list (d (n "cosmwasm-schema") (r "^1.2.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)))) (h "0yyf8ixh75n2gpvfqc0mk14kba7xhx08c6bfcq8bxpvi6pf5r9zk")))

(define-public crate-sg-metadata-0.22.11 (c (n "sg-metadata") (v "0.22.11") (d (list (d (n "cosmwasm-schema") (r "^1.2.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)))) (h "09g0j2kkfzhj53yl5iwkg14fi10waipm3y25mbxii50d7j3ayfdy")))

(define-public crate-sg-metadata-0.23.0 (c (n "sg-metadata") (v "0.23.0") (d (list (d (n "cosmwasm-schema") (r "^1.2.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)))) (h "0n14jh0ys55aydcm3kp4b82b4xsf7iwmlxaa51azl7kf03a22zba")))

(define-public crate-sg-metadata-0.23.1 (c (n "sg-metadata") (v "0.23.1") (d (list (d (n "cosmwasm-schema") (r "^1.2.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)))) (h "1hglxhsfw02kj3wd5mzyfmqg5qqsr6f12vn1f5vbqzv9k9lfbi3g")))

(define-public crate-sg-metadata-0.24.0 (c (n "sg-metadata") (v "0.24.0") (d (list (d (n "cosmwasm-schema") (r "^1.2.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)))) (h "1w01a3iyj46564z1hf01xxwgc3m1h2bjw6x7z6iglwk9rmigkdbh")))

(define-public crate-sg-metadata-0.24.1 (c (n "sg-metadata") (v "0.24.1") (d (list (d (n "cosmwasm-schema") (r "^1.2.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)))) (h "009xd3gz3g8160xig9wb54y7jggsnljwqmizhz8zw68681pqdmbc")))

(define-public crate-sg-metadata-0.25.0 (c (n "sg-metadata") (v "0.25.0") (d (list (d (n "cosmwasm-schema") (r "^1.2.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)))) (h "0bmllkk276phppmq80zvcj5azjmhy5cmbzs116zzvahv85nlfksq")))

(define-public crate-sg-metadata-2.1.0 (c (n "sg-metadata") (v "2.1.0") (d (list (d (n "cosmwasm-schema") (r "^1.2.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)))) (h "0sfnbskjpbkfigp1rf6j4ihpdcc5rsnlflwbjnb2pqmwd4fw6ca2")))

(define-public crate-sg-metadata-2.2.0 (c (n "sg-metadata") (v "2.2.0") (d (list (d (n "cosmwasm-schema") (r "^1.2.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)))) (h "1kmsi3a1q4ilzvmngrnd36yvkh8gradwf690y245m5kax6jwpklg")))

(define-public crate-sg-metadata-2.3.0 (c (n "sg-metadata") (v "2.3.0") (d (list (d (n "cosmwasm-schema") (r "^1.2.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)))) (h "1gljjwsh6n51zyxymgyz7gw8xb38fzkwwapshxl1qwy1anr905n1")))

(define-public crate-sg-metadata-2.3.1 (c (n "sg-metadata") (v "2.3.1") (d (list (d (n "cosmwasm-schema") (r "^1.2.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)))) (h "0vbbq43qamyqdvngnayr2w7czyzw6yyg1p9pxzv6l8pcib89wrkh")))

(define-public crate-sg-metadata-3.0.0 (c (n "sg-metadata") (v "3.0.0") (d (list (d (n "cosmwasm-schema") (r "^1.2.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)))) (h "1m4fxl1ma3mv4wz8sn84nc2v8a9bdmlj7c4shz5lrky6051fvdvf")))

(define-public crate-sg-metadata-2.4.0 (c (n "sg-metadata") (v "2.4.0") (d (list (d (n "cosmwasm-schema") (r "^1.2.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)))) (h "09qgq8y8lyascsbhs8n12injcmgw4llr7ar6jy4j74mdw6i6bar3")))

(define-public crate-sg-metadata-3.1.0 (c (n "sg-metadata") (v "3.1.0") (d (list (d (n "cosmwasm-schema") (r "^1.2.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)))) (h "1q9k5072w31hd1grxmjfc108xhf75w0bg701pnq4sc28q40xb640")))

(define-public crate-sg-metadata-3.2.0 (c (n "sg-metadata") (v "3.2.0") (d (list (d (n "cosmwasm-schema") (r "^1.2.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)))) (h "1nhnqqbf3200gba0ga7sl8w2l5lavszwqz07cffzqmhkvihjvwcz")))

(define-public crate-sg-metadata-3.2.1 (c (n "sg-metadata") (v "3.2.1") (d (list (d (n "cosmwasm-schema") (r "^1.2.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)))) (h "13p8pbxzv6kpbka4hmi86730r11p01zsgxlsa08m4d656laqqspm")))

(define-public crate-sg-metadata-3.2.2 (c (n "sg-metadata") (v "3.2.2") (d (list (d (n "cosmwasm-schema") (r "^1.2.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)))) (h "0l33qj8pk2bbbhcjzhqxyjxgi3k6mp4x8ax6ndnp6n68m7jzwmbd")))

(define-public crate-sg-metadata-3.2.3 (c (n "sg-metadata") (v "3.2.3") (d (list (d (n "cosmwasm-schema") (r "^1.2.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)))) (h "0a6i1qgmsdzn037cpx4qymq38ym637a04lry8mkp201ggkvl04ck")))

(define-public crate-sg-metadata-3.2.4 (c (n "sg-metadata") (v "3.2.4") (d (list (d (n "cosmwasm-schema") (r "^1.2.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)))) (h "0zlm31f7vsfanq0mx969sqlglhfvj3wacb4hkw0qj6n55ahzqvk8")))

(define-public crate-sg-metadata-3.2.5 (c (n "sg-metadata") (v "3.2.5") (d (list (d (n "cosmwasm-schema") (r "^1.2.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)))) (h "0q3ffmyzszhkfv0hhnsyfjqy4p36ijzpdbhj8482fw3c01iil54w")))

(define-public crate-sg-metadata-3.2.6 (c (n "sg-metadata") (v "3.2.6") (d (list (d (n "cosmwasm-schema") (r "^1.2.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)))) (h "08ah8gqsj72z9g7kaan5gz6y801cmpszppf2zsj385241szx9qic")))

(define-public crate-sg-metadata-3.2.7 (c (n "sg-metadata") (v "3.2.7") (d (list (d (n "cosmwasm-schema") (r "^1.2.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)))) (h "17zhxjz080cqqzjp07cjcybi00f7m36wiq1y8lyf9i38vwpiq0p4")))

(define-public crate-sg-metadata-3.2.8 (c (n "sg-metadata") (v "3.2.8") (d (list (d (n "cosmwasm-schema") (r "^1.2.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)))) (h "03qxdkib6g0rigba6da3771d9jfq9rjb827gsjf7zgx870d1d27j")))

(define-public crate-sg-metadata-3.2.9 (c (n "sg-metadata") (v "3.2.9") (d (list (d (n "cosmwasm-schema") (r "^1.2.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)))) (h "0wcibdfk39ngas4sgrsgh7mxjh58pqrs154679cddni48lh1amjq")))

(define-public crate-sg-metadata-3.3.0 (c (n "sg-metadata") (v "3.3.0") (d (list (d (n "cosmwasm-schema") (r "^1.2.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)))) (h "193g1n12b5sqsqfj9lja5klr41h7bfqhizhhcqn67ynk7yry7b8d")))

(define-public crate-sg-metadata-3.4.0 (c (n "sg-metadata") (v "3.4.0") (d (list (d (n "cosmwasm-schema") (r "^1.2.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)))) (h "057kn7cvnwx3q27pjspijp3cbvqhl0nxcs47gxc7wkpzcix0yzzl")))

(define-public crate-sg-metadata-3.5.0 (c (n "sg-metadata") (v "3.5.0") (d (list (d (n "cosmwasm-schema") (r "^1.2.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)))) (h "1hx1ndfyaj5n0i98rnq98iq69h4cbgia46aqalqd4yqxqkk3d2f4")))

(define-public crate-sg-metadata-3.6.0 (c (n "sg-metadata") (v "3.6.0") (d (list (d (n "cosmwasm-schema") (r "^1.2.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)))) (h "0jnz0wxhd5jmw1j8my30nqibs4ryy26vjl3mka3sik7iq4km9kzj")))

(define-public crate-sg-metadata-3.7.0 (c (n "sg-metadata") (v "3.7.0") (d (list (d (n "cosmwasm-schema") (r "^1.2.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)))) (h "0nvdb9rxhidn5d7jsg2fsrc3mxfx0b5zn2ypp98q1qmyscgf4zsa")))

(define-public crate-sg-metadata-3.8.0 (c (n "sg-metadata") (v "3.8.0") (d (list (d (n "cosmwasm-schema") (r "^1.2.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)))) (h "0n7kbr477bd62s442rf50s64r4cx9m47f8z0hs8lz9db7a7924a4")))

(define-public crate-sg-metadata-3.9.0 (c (n "sg-metadata") (v "3.9.0") (d (list (d (n "cosmwasm-schema") (r "^1.2.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)))) (h "12n4mk7g8m9asqyqsfz3kjq30f113zn3lhg6j2265hl951ky33i7")))

(define-public crate-sg-metadata-3.10.0 (c (n "sg-metadata") (v "3.10.0") (d (list (d (n "cosmwasm-schema") (r "^1.2.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)))) (h "156z7k2cswkcn6ypgfr06fcx179ibcf5s7m4gbv78kagcrahbdci")))

(define-public crate-sg-metadata-3.11.0 (c (n "sg-metadata") (v "3.11.0") (d (list (d (n "cosmwasm-schema") (r "^1.2.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)))) (h "1d21xsilp7yirnchg2sagy8clwsgsgv6rlbx4hwnk804m22mxh32")))

(define-public crate-sg-metadata-3.12.0 (c (n "sg-metadata") (v "3.12.0") (d (list (d (n "cosmwasm-schema") (r "^1.2.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)))) (h "0i2q00jn95s6v587ip6jzwyf89n15pnwjcq9kvcb1880abn8rdb0")))

(define-public crate-sg-metadata-3.13.0 (c (n "sg-metadata") (v "3.13.0") (d (list (d (n "cosmwasm-schema") (r "^1.2.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)))) (h "1vnv03747dl1ymn5mmdg1gkqv062cb1mkhlpfvwxz74wrk61kz0f")))

