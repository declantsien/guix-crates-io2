(define-module (crates-io sg -m sg-mutable-whitelist) #:use-module (crates-io))

(define-public crate-sg-mutable-whitelist-0.1.0 (c (n "sg-mutable-whitelist") (v "0.1.0") (d (list (d (n "cosmwasm-schema") (r "^1.2.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2.1") (d #t) (k 0)) (d (n "cw-ownable") (r "^0.5.0") (d #t) (k 0)) (d (n "sg-basic-whitelist") (r "^0.1.0") (d #t) (k 0)))) (h "02h97m80x682w99r156ygwigkab5ad1h77fs0r7g32x048bcvfbp") (f (quote (("library") ("backtraces" "cosmwasm-std/backtraces"))))))

