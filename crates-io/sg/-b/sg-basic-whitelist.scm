(define-module (crates-io sg -b sg-basic-whitelist) #:use-module (crates-io))

(define-public crate-sg-basic-whitelist-0.1.0 (c (n "sg-basic-whitelist") (v "0.1.0") (d (list (d (n "cosmwasm-schema") (r "^1.2.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2.1") (d #t) (k 0)) (d (n "cw-ownable") (r "^0.5.0") (d #t) (k 0)) (d (n "sg-basic-whitelist-derive") (r "^0.1.0") (d #t) (k 0)))) (h "13qd9cmmpzm4iaabmgy87vjacq80hmfvldgr6v4qqddwhv12i6v3") (f (quote (("library") ("backtraces" "cosmwasm-std/backtraces"))))))

