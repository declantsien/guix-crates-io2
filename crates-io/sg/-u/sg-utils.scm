(define-module (crates-io sg -u sg-utils) #:use-module (crates-io))

(define-public crate-sg-utils-0.20.0 (c (n "sg-utils") (v "0.20.0") (d (list (d (n "bech32") (r "^0.8.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.0") (d #t) (k 0)))) (h "0vy2bydyg1yax77bxc8sj5lvcds5n8cdm5673a5am15zl8xpzci4")))

(define-public crate-sg-utils-0.21.0 (c (n "sg-utils") (v "0.21.0") (d (list (d (n "bech32") (r "^0.8.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.5") (d #t) (k 0)))) (h "1k2iirsv315w9vfsk770npm5nd9kyhbhb2l8xryzq5badang3619")))

(define-public crate-sg-utils-0.21.1 (c (n "sg-utils") (v "0.21.1") (d (list (d (n "bech32") (r "^0.8.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.5") (d #t) (k 0)))) (h "0vfcirk16wj9kkv4zafyi5q2crszd930lpx4lpkbwxpyw3izl2cd")))

(define-public crate-sg-utils-0.21.2 (c (n "sg-utils") (v "0.21.2") (d (list (d (n "bech32") (r "^0.8.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.5") (d #t) (k 0)))) (h "1qa3jn5aiws7mjc4lkfz31qcbxcxwd6409ix9v4glf7aha96b529")))

(define-public crate-sg-utils-0.21.3 (c (n "sg-utils") (v "0.21.3") (d (list (d (n "bech32") (r "^0.8.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.5") (d #t) (k 0)))) (h "0m3d5fww959gfn5grfc19qh9z4vrcs9qnkq7kw7ixxzq945741i3")))

(define-public crate-sg-utils-0.21.4 (c (n "sg-utils") (v "0.21.4") (d (list (d (n "bech32") (r "^0.8.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.5") (d #t) (k 0)))) (h "1kny80zxgb4x0z8aksrzi7zamrh34w0xkma2n5i5psi26aa89f42")))

(define-public crate-sg-utils-0.21.5 (c (n "sg-utils") (v "0.21.5") (d (list (d (n "bech32") (r "^0.8.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.5") (d #t) (k 0)))) (h "0n4vrh0jhl6gmpihrr936xq8hsldgpslh17zs9258pl8ci8xpdk2")))

(define-public crate-sg-utils-0.21.6 (c (n "sg-utils") (v "0.21.6") (d (list (d (n "bech32") (r "^0.8.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.5") (d #t) (k 0)))) (h "1c4h6y6rnqrd76jjb2knr9llz6b30scrvffmckww1y42vhal2mpn")))

(define-public crate-sg-utils-0.21.7 (c (n "sg-utils") (v "0.21.7") (d (list (d (n "bech32") (r "^0.8.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.5") (d #t) (k 0)))) (h "1rrp3hghgkfbjlx722lbf155n5l5b29hic8211z6qkn8rljranp6")))

(define-public crate-sg-utils-0.21.8 (c (n "sg-utils") (v "0.21.8") (d (list (d (n "bech32") (r "^0.8.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.5") (d #t) (k 0)))) (h "0qmwmvcs0gxgb8hriq0pwh6n2nc0iqxm4y2jsn8r4dd1znsvai2g")))

(define-public crate-sg-utils-0.22.0 (c (n "sg-utils") (v "0.22.0") (d (list (d (n "bech32") (r "^0.8.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.5") (d #t) (k 0)))) (h "1jvs59fn6j6pkjbaxp9lair881jrb9x5c72yflzp25y213af3amk")))

(define-public crate-sg-utils-0.21.9 (c (n "sg-utils") (v "0.21.9") (d (list (d (n "bech32") (r "^0.8.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.5") (d #t) (k 0)))) (h "0k7206z8cajbmyjhy3i625br09di344lhdd2zvhhg0507g71dicz")))

(define-public crate-sg-utils-0.21.10 (c (n "sg-utils") (v "0.21.10") (d (list (d (n "bech32") (r "^0.8.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.5") (d #t) (k 0)))) (h "1jygp3bcxa59xycr7idp23f1i1q5f1xil689782nxlvvicjyasqx")))

(define-public crate-sg-utils-0.21.12 (c (n "sg-utils") (v "0.21.12") (d (list (d (n "bech32") (r "^0.8.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.5") (d #t) (k 0)))) (h "1wn8xqxhavpm6ssrvab3bbh1iic8i5jv7zpf1zij0x2cb8736r6s")))

(define-public crate-sg-utils-0.22.1 (c (n "sg-utils") (v "0.22.1") (d (list (d (n "bech32") (r "^0.8.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2.1") (d #t) (k 0)))) (h "1iiaxcdr31w741glw4aizwgcj1bgnij153a0ldaffv5q5zzzvnz1")))

(define-public crate-sg-utils-0.22.2 (c (n "sg-utils") (v "0.22.2") (d (list (d (n "bech32") (r "^0.8.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2.1") (d #t) (k 0)))) (h "16p77jp8m50sd12f3wirpfcd1clflvfpb01l052bhyd2aw8yd7hi")))

(define-public crate-sg-utils-0.22.3 (c (n "sg-utils") (v "0.22.3") (d (list (d (n "bech32") (r "^0.8.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2.1") (d #t) (k 0)))) (h "0jy2438p76pm6ly50s9gyzahqp675a1apsx47wwzk7mhbi7nfd3d")))

(define-public crate-sg-utils-0.22.4 (c (n "sg-utils") (v "0.22.4") (d (list (d (n "bech32") (r "^0.8.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2.1") (d #t) (k 0)))) (h "0h2s0h33mjj47kppc1iylx6pv44k4xgzz1bg01b2s7hb1kx6q5xl")))

(define-public crate-sg-utils-0.22.5 (c (n "sg-utils") (v "0.22.5") (d (list (d (n "bech32") (r "^0.8.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2.1") (d #t) (k 0)))) (h "01wh5xcfdz67zz3xq0hdn2bzmjkqjgwj4nk02s98f0s570x5ysdn")))

(define-public crate-sg-utils-0.22.6 (c (n "sg-utils") (v "0.22.6") (d (list (d (n "bech32") (r "^0.8.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2.1") (d #t) (k 0)))) (h "00g7kafcjz358drq0dgbszf9aqgj6wis4bmr9bi9xwi74a2n7w2x")))

(define-public crate-sg-utils-0.22.7 (c (n "sg-utils") (v "0.22.7") (d (list (d (n "bech32") (r "^0.8.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2.1") (d #t) (k 0)))) (h "1yidxs9amgvk1d1qia6psjx53dfcb9x619ihzbj8ajg7ffqh9y0s")))

(define-public crate-sg-utils-0.22.8 (c (n "sg-utils") (v "0.22.8") (d (list (d (n "bech32") (r "^0.8.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2.1") (d #t) (k 0)))) (h "1fxyb0sx4yswg2hcn2r9gavw724mis92p0553qp6n6hkdvr43cn6")))

(define-public crate-sg-utils-0.22.9 (c (n "sg-utils") (v "0.22.9") (d (list (d (n "bech32") (r "^0.8.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2.1") (d #t) (k 0)))) (h "0y13szcp0hx7crviwifmqiw44j2yxws1n42ajqkqh87k29wra5mp")))

(define-public crate-sg-utils-0.22.10 (c (n "sg-utils") (v "0.22.10") (d (list (d (n "bech32") (r "^0.8.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2.1") (d #t) (k 0)))) (h "0spiw7pjsddb0pi0z2jgbmbr0h2fadpwl7y9cff3q50vwyw414gz")))

(define-public crate-sg-utils-0.22.11 (c (n "sg-utils") (v "0.22.11") (d (list (d (n "bech32") (r "^0.8.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2.1") (d #t) (k 0)))) (h "12nzxn8vm2nmb0ycqmcd8nalsyzhpncazr0fyd90695ygny19p3j")))

(define-public crate-sg-utils-0.23.0 (c (n "sg-utils") (v "0.23.0") (d (list (d (n "bech32") (r "^0.8.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2.1") (d #t) (k 0)))) (h "1avmc8dwkp3ji46j5a55y54jsvnrl81b1ld1my6r28cimfamkcgn")))

(define-public crate-sg-utils-0.23.1 (c (n "sg-utils") (v "0.23.1") (d (list (d (n "bech32") (r "^0.8.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2.1") (d #t) (k 0)))) (h "0fjkc4h18w7q3ji8xbzlqfwvdi306ia5vnpml8mm86i2zywpwkvl")))

(define-public crate-sg-utils-0.24.0 (c (n "sg-utils") (v "0.24.0") (d (list (d (n "bech32") (r "^0.8.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2.1") (d #t) (k 0)))) (h "05wv6xsaazgx7m32xf71k8dz84in88m9y5dvmss5r7rsa566ax96")))

(define-public crate-sg-utils-0.24.1 (c (n "sg-utils") (v "0.24.1") (d (list (d (n "bech32") (r "^0.8.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2.1") (d #t) (k 0)))) (h "0v6q18ix0pc4l921n3ss3pzw580j4dlqni0n2f42pq55cvcpj57c")))

(define-public crate-sg-utils-0.25.0 (c (n "sg-utils") (v "0.25.0") (d (list (d (n "bech32") (r "^0.8.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2.1") (d #t) (k 0)))) (h "06v2sdvn4622h2v58653q3xypzinxs9hsi8f6v0ig59cfh4f1fys")))

(define-public crate-sg-utils-2.1.0 (c (n "sg-utils") (v "2.1.0") (d (list (d (n "bech32") (r "^0.8.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2.1") (d #t) (k 0)))) (h "0pzfjn2xyvqphgjn79ypjnaqi6lv6hvn79j84plmav6r5flyqndd")))

(define-public crate-sg-utils-2.2.0 (c (n "sg-utils") (v "2.2.0") (d (list (d (n "bech32") (r "^0.8.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2.1") (d #t) (k 0)))) (h "15bx713x2bn1x4qm5b2jm9s0ab3byqz8h09iw9ng4xpix96lirw7")))

(define-public crate-sg-utils-2.3.0 (c (n "sg-utils") (v "2.3.0") (d (list (d (n "bech32") (r "^0.8.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2.1") (d #t) (k 0)))) (h "06s7rrv09zhyr8r89l40bj6a4a5ssc4yjm1wyabf6z5z9nva52va")))

(define-public crate-sg-utils-2.3.1 (c (n "sg-utils") (v "2.3.1") (d (list (d (n "bech32") (r "^0.8.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2.1") (d #t) (k 0)))) (h "13z38wfcn52gq660hypjkdh138j3iznwb38bcy6nwvz39npg1ai4")))

(define-public crate-sg-utils-3.0.0 (c (n "sg-utils") (v "3.0.0") (d (list (d (n "bech32") (r "^0.8.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2.1") (d #t) (k 0)))) (h "1hsl3j2qf0qsqiycz8p7r35g6ggnnqmc8sqbb1lx40i868zkwlrl")))

(define-public crate-sg-utils-2.4.0 (c (n "sg-utils") (v "2.4.0") (d (list (d (n "bech32") (r "^0.8.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2.1") (d #t) (k 0)))) (h "0rsf6vanbasd5n3dg4i3k5di6zzy250d2hzx7cv92igz0hcl1ms7")))

(define-public crate-sg-utils-3.1.0 (c (n "sg-utils") (v "3.1.0") (d (list (d (n "bech32") (r "^0.8.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2.1") (d #t) (k 0)))) (h "1i3r99vsrw2lmylx1rrdp9c2wij2hdpaqf1bgn9rxlwx4x2bh6la")))

