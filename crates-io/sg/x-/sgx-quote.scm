(define-module (crates-io sg x- sgx-quote) #:use-module (crates-io))

(define-public crate-sgx-quote-0.0.9 (c (n "sgx-quote") (v "0.0.9") (d (list (d (n "nom") (r "^5.0") (d #t) (k 0)))) (h "1x31vwl6a9rwhgqv8hr48hbigdczn594vnhl1hndwl5fnlfkrcyy")))

(define-public crate-sgx-quote-0.1.0 (c (n "sgx-quote") (v "0.1.0") (d (list (d (n "nom") (r "^5.0") (d #t) (k 0)))) (h "08cfnp48khv59vn1wdcgj7ip4ydp0q8b7i20nc6x30bvmxvhar51")))

