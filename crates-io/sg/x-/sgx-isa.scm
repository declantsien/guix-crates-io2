(define-module (crates-io sg x- sgx-isa) #:use-module (crates-io))

(define-public crate-sgx-isa-0.1.0 (c (n "sgx-isa") (v "0.1.0") (d (list (d (n "bitflags") (r "^0.5") (d #t) (k 0)))) (h "1bap80mxi54w919sp4jsrdi41p3xi1ljmxpjprr14skd6apyljl4")))

(define-public crate-sgx-isa-0.1.1 (c (n "sgx-isa") (v "0.1.1") (d (list (d (n "bitflags") (r "^0.5") (d #t) (k 0)))) (h "1s6fkvbsjxq88fs3ww7w9rbkja55iqmq82kk10d4xmn9kxf1ahqr") (f (quote (("large_array_derive"))))))

(define-public crate-sgx-isa-0.1.2 (c (n "sgx-isa") (v "0.1.2") (d (list (d (n "bitflags") (r "^0.5") (d #t) (k 0)))) (h "1gnylk497wqh7xfgjavl9ipd9957dpp9n5xgknjk1cmvqx72g9v3") (f (quote (("try_from") ("large_array_derive"))))))

(define-public crate-sgx-isa-0.2.0-rc1 (c (n "sgx-isa") (v "0.2.0-rc1") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)))) (h "1wf4sq9k13lzg1phrs4iw3id03j7j83qvq7idvh5yyw8lmw2mgyh") (f (quote (("try_from") ("large_array_derive"))))))

(define-public crate-sgx-isa-0.2.0 (c (n "sgx-isa") (v "0.2.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)))) (h "0s4g028dziqpk73skbzx1y7yh94d22zwdq65s1g24177kpinlxfh") (f (quote (("try_from") ("sgxstd") ("large_array_derive"))))))

(define-public crate-sgx-isa-0.3.0 (c (n "sgx-isa") (v "0.3.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)))) (h "1cydwmnb8xy8gg6p9z8ja2kj9hpy4s01hjjpcv7mv3vkdvi79pz2") (f (quote (("sgxstd") ("large_array_derive"))))))

(define-public crate-sgx-isa-0.3.1 (c (n "sgx-isa") (v "0.3.1") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)))) (h "03n3h9fwyc49ksklxccjv4xjwaf5v4hycv2dfb8bi1n7gwq5k6bl") (f (quote (("sgxstd") ("large_array_derive"))))))

(define-public crate-sgx-isa-0.3.2 (c (n "sgx-isa") (v "0.3.2") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "mbedtls") (r "^0.5") (f (quote ("sgx"))) (k 2)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0ix8fmqjs68jxav1h7yq9142ch78zly4mc5kp5z1zcigy2s7s2vm") (f (quote (("sgxstd") ("nightly") ("large_array_derive"))))))

(define-public crate-sgx-isa-0.3.3 (c (n "sgx-isa") (v "0.3.3") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "mbedtls") (r "^0.5") (f (quote ("sgx"))) (k 2)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0xxv776gmmh24zbwy2vmhv0gphb7wnr4mj5kk2n3x8s80jag1bam") (f (quote (("sgxstd") ("nightly") ("large_array_derive"))))))

(define-public crate-sgx-isa-0.4.0 (c (n "sgx-isa") (v "0.4.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "mbedtls") (r "^0.8") (f (quote ("std"))) (k 2)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1pzhr0ykb02xgcx7yl7b727liapxd5iwh3w9i056qcjhjq1nxx80") (f (quote (("sgxstd") ("large_array_derive"))))))

(define-public crate-sgx-isa-0.4.1 (c (n "sgx-isa") (v "0.4.1") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "mbedtls") (r "^0.12") (f (quote ("std"))) (k 2)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1j9zp2ibya5b8i33f2za3jg4p7g2y0kknr5nzmklwrmg3k2nlx7h") (f (quote (("sgxstd") ("large_array_derive"))))))

