(define-module (crates-io sg x- sgx-decode-pib) #:use-module (crates-io))

(define-public crate-sgx-decode-pib-0.1.0 (c (n "sgx-decode-pib") (v "0.1.0") (d (list (d (n "hex") (r "^0.3") (d #t) (k 0)) (d (n "itertools") (r "^0.7.8") (d #t) (k 0)))) (h "19g38ng648n15dlajibnk7g2amjz7yczknds2h22x6v44hx40ypx")))

(define-public crate-sgx-decode-pib-0.1.1 (c (n "sgx-decode-pib") (v "0.1.1") (d (list (d (n "hex") (r "^0.3") (d #t) (k 0)) (d (n "itertools") (r "^0.7.8") (d #t) (k 0)))) (h "1nbdxqsfxywqbl264ianwfmx1sqnpnqbxhq6f8gj77di7pm2gqna")))

(define-public crate-sgx-decode-pib-0.1.2 (c (n "sgx-decode-pib") (v "0.1.2") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "itertools") (r "^0.8") (d #t) (k 0)))) (h "0w9winhdxmprlr1m5dwka1i36zl0cfl8svlgcpz624r4mi28inxi")))

