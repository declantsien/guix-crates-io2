(define-module (crates-io sg x- sgx-keyreq) #:use-module (crates-io))

(define-public crate-sgx-keyreq-0.1.0 (c (n "sgx-keyreq") (v "0.1.0") (d (list (d (n "cc") (r "^1.0.71") (d #t) (k 1)) (d (n "sgx-isa") (r "^0.3.3") (d #t) (k 0)))) (h "0585klsfbj5q42lg2r7xqfsf2b533r3i3b32k5x47caqlb6xp7yd")))

(define-public crate-sgx-keyreq-0.2.0 (c (n "sgx-keyreq") (v "0.2.0") (d (list (d (n "cc") (r "^1.0.71") (d #t) (k 1)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "rdrand") (r "^0.8.1") (d #t) (k 2)) (d (n "sgx-isa") (r "^0.3.3") (d #t) (k 0)))) (h "17d47b9vlda4yxpa5l1d741h4knpgqgv9yfmy1x4nnvzr6rh9m67") (f (quote (("std") ("default" "std"))))))

(define-public crate-sgx-keyreq-0.2.1 (c (n "sgx-keyreq") (v "0.2.1") (d (list (d (n "cc") (r "^1.0.71") (d #t) (k 1)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "rdrand") (r "^0.8.1") (d #t) (k 2)) (d (n "sgx-isa") (r "^0.3.3") (d #t) (k 0)))) (h "04xf53cddh8kasl9v0293bla1pm4a5xz44lq006jl8j6v6rh8ipb") (f (quote (("std") ("default" "std"))))))

