(define-module (crates-io sg x- sgx-cpu) #:use-module (crates-io))

(define-public crate-sgx-cpu-0.1.0 (c (n "sgx-cpu") (v "0.1.0") (d (list (d (n "raw-cpuid") (r "^9") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0nwv9qs3qhij30hkfw2yldr8w702c50v3s7zmzfp96sca25l7j5m")))

(define-public crate-sgx-cpu-0.1.1 (c (n "sgx-cpu") (v "0.1.1") (d (list (d (n "console") (r "^0.14") (d #t) (k 0)) (d (n "raw-cpuid") (r "^9") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0lrc8ixw3wb5n5k715g41d4ib24mv631szr6wihk1ham4cdjv3ws") (y #t)))

(define-public crate-sgx-cpu-0.1.2 (c (n "sgx-cpu") (v "0.1.2") (d (list (d (n "console") (r "^0.14") (d #t) (k 0)) (d (n "raw-cpuid") (r "^9") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1y90xklsa40x12jd9h7r2b721dd4cjjkj8bdvz5856s1wzxwk5a5")))

