(define-module (crates-io sg x- sgx-panic-backtrace) #:use-module (crates-io))

(define-public crate-sgx-panic-backtrace-0.1.0 (c (n "sgx-panic-backtrace") (v "0.1.0") (d (list (d (n "backtrace") (r "^0.3.65") (k 0)))) (h "10bcvw68mh5mr6lkgc9mq2vmiz813vd7k51ihydbsz876pzcpaq6")))

