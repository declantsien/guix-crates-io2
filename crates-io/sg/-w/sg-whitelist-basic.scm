(define-module (crates-io sg -w sg-whitelist-basic) #:use-module (crates-io))

(define-public crate-sg-whitelist-basic-0.4.0 (c (n "sg-whitelist-basic") (v "0.4.0") (d (list (d (n "cosmwasm-schema") (r "^1.1.4") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.4") (d #t) (k 0)) (d (n "schemars") (r "^0.8.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (k 0)))) (h "0hf9rdpmfj2c9scna6kljsq4bplgbzxmzkfnpmlhya26ba3228j6")))

(define-public crate-sg-whitelist-basic-0.5.0 (c (n "sg-whitelist-basic") (v "0.5.0") (d (list (d (n "cosmwasm-schema") (r "^1.1.4") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.4") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)))) (h "156k8f6lranz8nkw2kwv34zkl8cyhbid5rx2nd4vzwxvfva6m3w5")))

(define-public crate-sg-whitelist-basic-0.9.0 (c (n "sg-whitelist-basic") (v "0.9.0") (d (list (d (n "cosmwasm-schema") (r "^1.1.4") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.4") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)))) (h "0f9bp4mf1kaxz56yfs51g18vdijw9fr6dryma5r3fd4z11rb91ag")))

(define-public crate-sg-whitelist-basic-0.11.0 (c (n "sg-whitelist-basic") (v "0.11.0") (d (list (d (n "cosmwasm-schema") (r "^1.1.5") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.5") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)))) (h "1dlm05dpqq6vhasjgkcnhai0hdvmpzsr2kpi29nc72f49zngnp3p")))

(define-public crate-sg-whitelist-basic-0.12.0 (c (n "sg-whitelist-basic") (v "0.12.0") (d (list (d (n "cosmwasm-schema") (r "^1.1.5") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.5") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)))) (h "1fc7ynfscf8725ag8ww4250i89gpiarcw61wm97j52a4vsq701mz")))

(define-public crate-sg-whitelist-basic-0.13.0 (c (n "sg-whitelist-basic") (v "0.13.0") (d (list (d (n "cosmwasm-schema") (r "^1.1.5") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.5") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)))) (h "0nrl3p7nb3chilkvhazs04jvblwzikmn00pv32nz7d8xb24rxry7")))

(define-public crate-sg-whitelist-basic-0.15.0 (c (n "sg-whitelist-basic") (v "0.15.0") (d (list (d (n "cosmwasm-schema") (r "^1.1.5") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.5") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)))) (h "07z16q0y7r9m8j8vsw92qrja19g7pyqfmhm52jad1prsvs4hf9d9")))

(define-public crate-sg-whitelist-basic-0.17.0 (c (n "sg-whitelist-basic") (v "0.17.0") (d (list (d (n "cosmwasm-schema") (r "^1.1.5") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.5") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)))) (h "0glng381lj6kl2irqrm5lb6dvj249vs0n6k2am5grm0vwdnlbva7")))

(define-public crate-sg-whitelist-basic-0.18.0 (c (n "sg-whitelist-basic") (v "0.18.0") (d (list (d (n "cosmwasm-schema") (r "^1.1.5") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.5") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)))) (h "0809v4ha92f5f3pi0ml1nakafgwhq3kds2ylcdv5d6n7vjvbzpk9")))

(define-public crate-sg-whitelist-basic-0.19.0 (c (n "sg-whitelist-basic") (v "0.19.0") (d (list (d (n "cosmwasm-schema") (r "^1.1.5") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.5") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)))) (h "0lnh6b44wq9mhyayqh1x93y8j50mdi5zfl8ripx75zg46i08ni65")))

(define-public crate-sg-whitelist-basic-0.20.0 (c (n "sg-whitelist-basic") (v "0.20.0") (d (list (d (n "cosmwasm-schema") (r "^1.1.5") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.5") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)))) (h "0ns5aizvksj6ijdyl9frqap6j3j4d5y4ncxhx8ppi1ps9y9qcima")))

(define-public crate-sg-whitelist-basic-1.1.0 (c (n "sg-whitelist-basic") (v "1.1.0") (d (list (d (n "cosmwasm-schema") (r "^1.1.5") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.5") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)))) (h "0rldnkjh8a6ygbnhxmdcvwgpjawg9yyqmmn1jg9fnf90y1y4ajwv")))

(define-public crate-sg-whitelist-basic-1.1.1 (c (n "sg-whitelist-basic") (v "1.1.1") (d (list (d (n "cosmwasm-schema") (r "^1.1.5") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.5") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)))) (h "0pifsf1scpjd594zfdp9arh6qni67zamxlhwgyc8qdwrc85mmamm")))

(define-public crate-sg-whitelist-basic-1.2.2 (c (n "sg-whitelist-basic") (v "1.2.2") (d (list (d (n "cosmwasm-schema") (r "^1.2.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)))) (h "1rcbk5z2j7sl7s84ghyjn74yqsq955zkdala1bx44byj6gigzlni")))

(define-public crate-sg-whitelist-basic-1.2.3 (c (n "sg-whitelist-basic") (v "1.2.3") (d (list (d (n "cosmwasm-schema") (r "^1.2.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)))) (h "1cl121n08rvbynwfn1pv9gsgycn9izvccrjpi9a52z3lx1h87wd8")))

(define-public crate-sg-whitelist-basic-1.2.4 (c (n "sg-whitelist-basic") (v "1.2.4") (d (list (d (n "cosmwasm-schema") (r "^1.2.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)))) (h "0iacnn48x6xxb3sszzqr306hmin43m81bdlwyz57ick211klp1nr")))

(define-public crate-sg-whitelist-basic-1.2.5 (c (n "sg-whitelist-basic") (v "1.2.5") (d (list (d (n "cosmwasm-schema") (r "^1.2.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)))) (h "0bbr9bbwqinr27c43h9fvbb6z8z149zpdhln1vr5mvvjhgsj9qmj")))

