(define-module (crates-io sg -n sg-name-minter) #:use-module (crates-io))

(define-public crate-sg-name-minter-0.9.0 (c (n "sg-name-minter") (v "0.9.0") (d (list (d (n "cosmwasm-schema") (r "^1.1.4") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.4") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)))) (h "1phk5v0k848mczspq48pb9frdr29b6k281mww7r9pmfmrin2sbsb")))

(define-public crate-sg-name-minter-0.11.0 (c (n "sg-name-minter") (v "0.11.0") (d (list (d (n "cosmwasm-schema") (r "^1.1.5") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.5") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)))) (h "0x1zfvbr0w3dwdnmhhmkxnvmps61b8ya2n0kpzkcy7j35pncrwfn")))

(define-public crate-sg-name-minter-0.12.0 (c (n "sg-name-minter") (v "0.12.0") (d (list (d (n "cosmwasm-schema") (r "^1.1.5") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.5") (d #t) (k 0)) (d (n "cw-controllers") (r "^0.15.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)))) (h "0wsd996dc1nnmcrd2q9kbh8dnick6w5g3xmjv7mrjj9bp5k3gh2f")))

(define-public crate-sg-name-minter-0.13.0 (c (n "sg-name-minter") (v "0.13.0") (d (list (d (n "cosmwasm-schema") (r "^1.1.5") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.5") (d #t) (k 0)) (d (n "cw-controllers") (r "^0.16.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)))) (h "0hxd82mxq9qpazln6p9zwdmwzyq8r8dbyd9j25mvs3h96gbb4ipm")))

(define-public crate-sg-name-minter-0.15.0 (c (n "sg-name-minter") (v "0.15.0") (d (list (d (n "cosmwasm-schema") (r "^1.1.5") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.5") (d #t) (k 0)) (d (n "cw-controllers") (r "^0.16.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)))) (h "1ng2dqz5z8cp5z7v3waal19vh24zcjr7ilzry5l8fryg7r1dz8hp")))

(define-public crate-sg-name-minter-0.17.0 (c (n "sg-name-minter") (v "0.17.0") (d (list (d (n "cosmwasm-schema") (r "^1.1.5") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.5") (d #t) (k 0)) (d (n "cw-controllers") (r "^0.16.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)))) (h "1wj4b3vrja7idpabvfjkva9hs5j9nkyjz4chqncgmiin70gahqyr")))

(define-public crate-sg-name-minter-0.18.0 (c (n "sg-name-minter") (v "0.18.0") (d (list (d (n "cosmwasm-schema") (r "^1.1.5") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.5") (d #t) (k 0)) (d (n "cw-controllers") (r "^0.16.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)))) (h "159np8wq9hbnmi44i7kz6jhb2dpsrx6n8hhz8w6fc7c8hmwz8jc1")))

(define-public crate-sg-name-minter-0.19.0 (c (n "sg-name-minter") (v "0.19.0") (d (list (d (n "cosmwasm-schema") (r "^1.1.5") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.5") (d #t) (k 0)) (d (n "cw-controllers") (r "^0.16.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)))) (h "14hyfv5m0slr965pz84pgxpansnkw4p6gkv8i7jpzmbk6qcgpd4y")))

(define-public crate-sg-name-minter-0.20.0 (c (n "sg-name-minter") (v "0.20.0") (d (list (d (n "cosmwasm-schema") (r "^1.1.5") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.5") (d #t) (k 0)) (d (n "cw-controllers") (r "^0.16.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)))) (h "0f400s3j8jvwjn496jrad3k9lmb6575wi109hqkzng76bjzwpyy4")))

(define-public crate-sg-name-minter-1.1.0 (c (n "sg-name-minter") (v "1.1.0") (d (list (d (n "cosmwasm-schema") (r "^1.1.5") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.5") (d #t) (k 0)) (d (n "cw-controllers") (r "^0.16.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)))) (h "16f6spz5a7k9kbm2wl27gnfml00aydvssmcij0bshhvbncg7scbb")))

(define-public crate-sg-name-minter-1.1.1 (c (n "sg-name-minter") (v "1.1.1") (d (list (d (n "cosmwasm-schema") (r "^1.1.5") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.5") (d #t) (k 0)) (d (n "cw-controllers") (r "^0.16.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)))) (h "005amf33xkdq0rzy8hghnsvqjhjcy7pjpijl05zcdm66prila18n")))

(define-public crate-sg-name-minter-1.2.2 (c (n "sg-name-minter") (v "1.2.2") (d (list (d (n "cosmwasm-schema") (r "^1.2.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2.1") (d #t) (k 0)) (d (n "cw-controllers") (r "^0.16.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)))) (h "13ggjwpjs2iw9gn6682k9gkpk5g32h5v7ha31raz8kq5f4mblm73")))

(define-public crate-sg-name-minter-1.2.3 (c (n "sg-name-minter") (v "1.2.3") (d (list (d (n "cosmwasm-schema") (r "^1.2.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2.1") (d #t) (k 0)) (d (n "cw-controllers") (r "^0.16.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)))) (h "0ppcc2czjny0jxqnysnqkwk9b7h5giiccmnzn8iirh9d3i9mq7xx")))

(define-public crate-sg-name-minter-1.2.4 (c (n "sg-name-minter") (v "1.2.4") (d (list (d (n "cosmwasm-schema") (r "^1.2.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2.1") (d #t) (k 0)) (d (n "cw-controllers") (r "^0.16.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)))) (h "1h2g7ymy3nzyxnc1li6vhdv932rfxrbmgrd5cafhswh41df40jy4")))

(define-public crate-sg-name-minter-1.2.5 (c (n "sg-name-minter") (v "1.2.5") (d (list (d (n "cosmwasm-schema") (r "^1.2.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2.1") (d #t) (k 0)) (d (n "cw-controllers") (r "^0.16.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)))) (h "0h69sgascxmlz8cv3nwpwna41g28x4phvxxmh1c4k3zpl7ms8rs9")))

