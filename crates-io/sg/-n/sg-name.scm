(define-module (crates-io sg -n sg-name) #:use-module (crates-io))

(define-public crate-sg-name-0.2.0 (c (n "sg-name") (v "0.2.0") (d (list (d (n "cosmwasm-std") (r "^1.1.4") (d #t) (k 0)) (d (n "schemars") (r "^0.8.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (k 0)))) (h "1f3r6d6z1rlkpy5mmwmib4fkp8zihn74fkyhn8ci02826p8i776q")))

(define-public crate-sg-name-0.3.0 (c (n "sg-name") (v "0.3.0") (d (list (d (n "cosmwasm-schema") (r "^1.1.4") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.4") (d #t) (k 0)) (d (n "schemars") (r "^0.8.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (k 0)))) (h "09jx448l6c5m2qaayn3xam1v1wy3qm0gs5y2z44gns08xb4hb1r0")))

(define-public crate-sg-name-0.4.0 (c (n "sg-name") (v "0.4.0") (d (list (d (n "cosmwasm-schema") (r "^1.1.4") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.4") (d #t) (k 0)) (d (n "schemars") (r "^0.8.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (k 0)))) (h "01vf30b46id0wn2708da4dvriabzg2agdkwmi1nblrgfdr5jv2g5")))

(define-public crate-sg-name-0.5.0 (c (n "sg-name") (v "0.5.0") (d (list (d (n "cosmwasm-schema") (r "^1.1.4") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.4") (d #t) (k 0)) (d (n "schemars") (r "^0.8.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (k 0)))) (h "05ka2pxsykfski5xrcgr2d4jlyxf7y4ngfzdzsy15903py4mzf5k")))

(define-public crate-sg-name-0.6.0 (c (n "sg-name") (v "0.6.0") (d (list (d (n "cosmwasm-schema") (r "^1.1.4") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.4") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)))) (h "0q62s6i7vwpqhzns5mcfk88kg8n4by4pnd4vj4d90hzwdgs9ml57")))

(define-public crate-sg-name-0.7.0 (c (n "sg-name") (v "0.7.0") (d (list (d (n "cosmwasm-schema") (r "^1.1.4") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.4") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)))) (h "1yrknkdhkw3g6b9ccqb30sasaqb8x0xv7mcqndcnp0gcj7fwxnk4")))

(define-public crate-sg-name-0.9.0 (c (n "sg-name") (v "0.9.0") (d (list (d (n "cosmwasm-schema") (r "^1.1.4") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.4") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)))) (h "0ai2hfryas6zp41fa41bjnz09qkni3w6sicxy1gq0mypr44dccg2")))

(define-public crate-sg-name-0.11.0 (c (n "sg-name") (v "0.11.0") (d (list (d (n "cosmwasm-schema") (r "^1.1.5") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.5") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)))) (h "0q9ivk7vrcc4bw95by4x2k5zl975r5dpl6bymzm7f3hgv98jx3r5")))

(define-public crate-sg-name-0.12.0 (c (n "sg-name") (v "0.12.0") (d (list (d (n "cosmwasm-schema") (r "^1.1.5") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.5") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)))) (h "0mp17m4wk5vgbb8xfjvgdjgf999f6mz7znnr7h7bp7g5fnjgsymk")))

(define-public crate-sg-name-0.13.0 (c (n "sg-name") (v "0.13.0") (d (list (d (n "cosmwasm-schema") (r "^1.1.5") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.5") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)))) (h "1725mzqclyjqlhvalg4ajyil20jhqpki8m658hqzyy5awq47vjy5")))

(define-public crate-sg-name-0.15.0 (c (n "sg-name") (v "0.15.0") (d (list (d (n "cosmwasm-schema") (r "^1.1.5") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.5") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)))) (h "15q9k3192i534p2im45yvvc6ypgy0jz0p82f42j4pd77zryc7xw6")))

(define-public crate-sg-name-0.17.0 (c (n "sg-name") (v "0.17.0") (d (list (d (n "cosmwasm-schema") (r "^1.1.5") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.5") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)))) (h "0gh3hjqf1dxnq484nwxpilh9gs77bgkfxzbbgwd6chwln82j6bgg")))

(define-public crate-sg-name-0.18.0 (c (n "sg-name") (v "0.18.0") (d (list (d (n "cosmwasm-schema") (r "^1.1.5") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.5") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)))) (h "1dbllw06xhn5qi8s21qa0fprpk98h79v2nfal3wyvd0ms0mspv2v")))

(define-public crate-sg-name-0.19.0 (c (n "sg-name") (v "0.19.0") (d (list (d (n "cosmwasm-schema") (r "^1.1.5") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.5") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)))) (h "0axww0l43nafh1i3kv3m593zkph6lwm8zmzjbqg1qw1c75hjb1sy")))

(define-public crate-sg-name-0.20.0 (c (n "sg-name") (v "0.20.0") (d (list (d (n "cosmwasm-schema") (r "^1.1.5") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.5") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)))) (h "1nwzni1y5ag8rz3jz87jxnrq5qfkxl7bdn3p81vw7ylwsfgnqy9j")))

(define-public crate-sg-name-1.1.0 (c (n "sg-name") (v "1.1.0") (d (list (d (n "cosmwasm-schema") (r "^1.1.5") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.5") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)))) (h "05i31j6rv9yqsxw15c5qgd749q0s7sj5sw856wypwz4n3zy9qssg")))

(define-public crate-sg-name-1.1.1 (c (n "sg-name") (v "1.1.1") (d (list (d (n "cosmwasm-schema") (r "^1.1.5") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.5") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)))) (h "1k3kmyk5s8rkh5pdq02c3xyd0kxlmjfw3kymjga3fw345gn5m665")))

(define-public crate-sg-name-1.2.2 (c (n "sg-name") (v "1.2.2") (d (list (d (n "cosmwasm-schema") (r "^1.2.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)))) (h "1d9f5lnm6ary8aa4g50s9wcwhhjqkd2qyc3hdg2y02hmxljhlxsa")))

(define-public crate-sg-name-1.2.3 (c (n "sg-name") (v "1.2.3") (d (list (d (n "cosmwasm-schema") (r "^1.2.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)))) (h "1v4g6wdmw26lm1q143y74ln552r0ljrw8vfyx0a01h834iglba8b")))

(define-public crate-sg-name-1.2.4 (c (n "sg-name") (v "1.2.4") (d (list (d (n "cosmwasm-schema") (r "^1.2.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)))) (h "1813r75n68i84qzk2cr2npig8fnl2wxizilrwggk68671711w8p3")))

(define-public crate-sg-name-1.2.5 (c (n "sg-name") (v "1.2.5") (d (list (d (n "cosmwasm-schema") (r "^1.2.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)))) (h "14vb7832jwik1ydqsjgj694skxx9h4qkld89z1ksf3062bipk8gv")))

