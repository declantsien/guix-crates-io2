(define-module (crates-io sg -n sg-name-market) #:use-module (crates-io))

(define-public crate-sg-name-market-0.4.0 (c (n "sg-name-market") (v "0.4.0") (d (list (d (n "cosmwasm-schema") (r "^1.1.4") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.4") (d #t) (k 0)) (d (n "schemars") (r "^0.8.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (k 0)))) (h "15d8bp6mpkqz5lf1b46zzdp3ac7x117zjij8jnp32va4xl7l2ina")))

(define-public crate-sg-name-market-0.5.0 (c (n "sg-name-market") (v "0.5.0") (d (list (d (n "cosmwasm-schema") (r "^1.1.4") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.4") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)))) (h "0kr25zv9g4lg63py43lhpm402ah5qpw0835iw3ky3gahi2fai712")))

(define-public crate-sg-name-market-0.6.0 (c (n "sg-name-market") (v "0.6.0") (d (list (d (n "cosmwasm-schema") (r "^1.1.4") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.4") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)))) (h "1ipan10ba273rf935c9l1j29xip82zvf00grm6hl1awjrj73zpa8")))

(define-public crate-sg-name-market-0.9.0 (c (n "sg-name-market") (v "0.9.0") (d (list (d (n "cosmwasm-schema") (r "^1.1.4") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.4") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)))) (h "14bk09gd9qdkjm0w02iyinw1v1y2plhgw2kgvj5k5rq6b47zkjpa")))

(define-public crate-sg-name-market-0.11.0 (c (n "sg-name-market") (v "0.11.0") (d (list (d (n "cosmwasm-schema") (r "^1.1.5") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.5") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)))) (h "17nwbszxz4r89wq69i2844gr2zbpaq79f4jx010i7g5xqk0gq1k5")))

(define-public crate-sg-name-market-0.12.0 (c (n "sg-name-market") (v "0.12.0") (d (list (d (n "cosmwasm-schema") (r "^1.1.5") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.5") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)))) (h "1rjpj6vn5paz0qcsd8mw8qwjl47ixl1jx7n96f1i5y5in8lfcp47")))

(define-public crate-sg-name-market-0.13.0 (c (n "sg-name-market") (v "0.13.0") (d (list (d (n "cosmwasm-schema") (r "^1.1.5") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.5") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)))) (h "1k766whc48fr5bdss88c7ckjzl5r4fzpnq4m0vz54ihk072236gv")))

(define-public crate-sg-name-market-0.15.0 (c (n "sg-name-market") (v "0.15.0") (d (list (d (n "cosmwasm-schema") (r "^1.1.5") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.5") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)))) (h "0jd2nxahj6mgsl35b2d2lmvhkw9gzkilybhqf05yxy4dkqdqpba1")))

(define-public crate-sg-name-market-0.17.0 (c (n "sg-name-market") (v "0.17.0") (d (list (d (n "cosmwasm-schema") (r "^1.1.5") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.5") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)))) (h "1sk465l557sqnnmcfkxzhh9hbbhviiaf5gzy1bh4wlpryyqf429f")))

(define-public crate-sg-name-market-0.18.0 (c (n "sg-name-market") (v "0.18.0") (d (list (d (n "cosmwasm-schema") (r "^1.1.5") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.5") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)))) (h "1v6741j6fq606rzh4cz2mk3k0msfhr8xrq93rq08fxh5gi46fzyi")))

(define-public crate-sg-name-market-0.19.0 (c (n "sg-name-market") (v "0.19.0") (d (list (d (n "cosmwasm-schema") (r "^1.1.5") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.5") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)))) (h "1d4w9hg13zi7drh15br5rzzqkszv3sy4fg727gsrfr2yxm2gawrn")))

(define-public crate-sg-name-market-0.20.0 (c (n "sg-name-market") (v "0.20.0") (d (list (d (n "cosmwasm-schema") (r "^1.1.5") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.5") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)))) (h "1llnrwx185hc5l8i4vcbjzmsfjkphslavz0b3a6zi2zwh49z8k6p")))

(define-public crate-sg-name-market-1.1.0 (c (n "sg-name-market") (v "1.1.0") (d (list (d (n "cosmwasm-schema") (r "^1.1.5") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.5") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)))) (h "152ncpc006q1cn0k9nh83mas8s91czfmfk4ql2255sp7y4vldsjm")))

(define-public crate-sg-name-market-1.1.1 (c (n "sg-name-market") (v "1.1.1") (d (list (d (n "cosmwasm-schema") (r "^1.1.5") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.5") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)))) (h "0vl1gmd0rvy89b1knws9q2dp0k6bw55qpykmccgzpxsvva1fd9cy")))

(define-public crate-sg-name-market-1.2.2 (c (n "sg-name-market") (v "1.2.2") (d (list (d (n "cosmwasm-schema") (r "^1.2.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)))) (h "0injidmx35yi0nhh9ni7xd1a5v1d9i0lfsbzg8f8vds7hrsxfgi9")))

(define-public crate-sg-name-market-1.2.3 (c (n "sg-name-market") (v "1.2.3") (d (list (d (n "cosmwasm-schema") (r "^1.2.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)))) (h "1gqfzbmxpa6s9ylyaya3hkxaczrh0ky0lnns2hwfx5ws3rk76gxx")))

(define-public crate-sg-name-market-1.2.4 (c (n "sg-name-market") (v "1.2.4") (d (list (d (n "cosmwasm-schema") (r "^1.2.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)))) (h "1c9b9xgs1pf9vjiv8gbf0v2nldkd7c9v0sxm5jqijndlmdv68dm8")))

(define-public crate-sg-name-market-1.2.5 (c (n "sg-name-market") (v "1.2.5") (d (list (d (n "cosmwasm-schema") (r "^1.2.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)))) (h "1ymylgm14x3wik0nva8xw9jzcm2rhcaypah5rs3avn220yyd7hfp")))

