(define-module (crates-io sg r_ sgr_macros) #:use-module (crates-io))

(define-public crate-sgr_macros-0.3.0 (c (n "sgr_macros") (v "0.3.0") (d (list (d (n "const_format") (r "^0.2.26") (o #t) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (d #t) (k 0)))) (h "0zin8ajgx6b1p5jybrql47h0v5x6p4sgirawh15vs3c0ashwxpxk") (f (quote (("default") ("const" "const_format"))))))

