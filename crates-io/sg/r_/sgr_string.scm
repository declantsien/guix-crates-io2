(define-module (crates-io sg r_ sgr_string) #:use-module (crates-io))

(define-public crate-sgr_string-0.1.0 (c (n "sgr_string") (v "0.1.0") (h "1wph2ybam4rblg7436468n1agj725bnz209m45pspbfqfb84dr8c")))

(define-public crate-sgr_string-0.1.1 (c (n "sgr_string") (v "0.1.1") (h "0r3nkqqxr3c2dqpmw72a0d2lcxhrzrj8mxrd56ivzjc02khhil0j")))

(define-public crate-sgr_string-0.1.2 (c (n "sgr_string") (v "0.1.2") (h "06f1nl27kqa667ppvqizsllvz48d948wvj487n6hp3bmgiqq4xbj")))

