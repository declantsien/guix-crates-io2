(define-module (crates-io sg f- sgf-parse) #:use-module (crates-io))

(define-public crate-sgf-parse-0.1.0 (c (n "sgf-parse") (v "0.1.0") (h "1qnlz9fkrwh8612bk9kz62vp2da5gn6k90g5k9cd0ggbh4pscavs")))

(define-public crate-sgf-parse-0.2.0 (c (n "sgf-parse") (v "0.2.0") (h "16ygch438z6zk71ic8vq2safvcbfsfj8f2vr1l5a2axfciwky6vk")))

(define-public crate-sgf-parse-0.2.1 (c (n "sgf-parse") (v "0.2.1") (h "02ip71gwnd9w328b3v3w5pd6jbskyhsvzaz3xsh7ir139cd6v9r3")))

(define-public crate-sgf-parse-0.3.0 (c (n "sgf-parse") (v "0.3.0") (h "0rqdm6jq0gb8rqzjlq2651y8lz0awd0ax47xddywjf8m5q2x0lw7")))

(define-public crate-sgf-parse-0.3.1 (c (n "sgf-parse") (v "0.3.1") (h "0qs061zk0hf1ikdb8m97mwx9w57yb4l5bxfsf66c35fvjr60wzr6")))

(define-public crate-sgf-parse-0.3.2 (c (n "sgf-parse") (v "0.3.2") (h "0xwhcg05nszc32x5yd0imn9gswlc95mchkh06khgmc6wv86jygpp")))

(define-public crate-sgf-parse-1.0.0 (c (n "sgf-parse") (v "1.0.0") (h "1hqxzv10vq7675jr6q1madccawvpkg5sb40hcfi7wf15867m8yq3")))

(define-public crate-sgf-parse-2.0.0 (c (n "sgf-parse") (v "2.0.0") (h "1lz6c5j85bah95kyjjnfpcvjhyiabhygq9c6ggj05gvs65fbsjjm")))

(define-public crate-sgf-parse-2.0.1 (c (n "sgf-parse") (v "2.0.1") (h "1bmyrmrgmyv8wbjvjky2ij849v6r1cng7py78q08y07la57k9ysl")))

(define-public crate-sgf-parse-2.0.2 (c (n "sgf-parse") (v "2.0.2") (h "07hdw5dzv3k5q3ky8mpkajjbbch6vfai2sj1b3wp8dmaad8vsl3a")))

(define-public crate-sgf-parse-2.0.3 (c (n "sgf-parse") (v "2.0.3") (h "00im4hmmmadfvlyshiz9vwv7gdh8f9wq9d24vf374ppfm7dyja19")))

(define-public crate-sgf-parse-2.0.4 (c (n "sgf-parse") (v "2.0.4") (h "0q5nia3c1v89bhbx7ybqvsr3br2lz9fncjbp0kp16qnzh6w9b3a9")))

(define-public crate-sgf-parse-3.0.0 (c (n "sgf-parse") (v "3.0.0") (h "0043kxy8jkcfmkdw0qn36rh14ddr0ca26mwx03fhzxiylhd1sw51")))

(define-public crate-sgf-parse-3.0.1 (c (n "sgf-parse") (v "3.0.1") (h "19rr48d34mf0k76ls7g9pm279rb3d3yxg1b1ddlfggihycb736hd")))

(define-public crate-sgf-parse-3.0.2 (c (n "sgf-parse") (v "3.0.2") (h "1pv8kdirj675wg5wp4l837hzvgy759db2fjy071ymddbmrmbcbik")))

(define-public crate-sgf-parse-3.1.0 (c (n "sgf-parse") (v "3.1.0") (h "13166fpb8m9z7l7n63yxycjq79f3s6f6p95npjyqhgafx2zdgabl")))

(define-public crate-sgf-parse-3.1.1 (c (n "sgf-parse") (v "3.1.1") (h "1r9rm6r9szb812n60pm99kms1i9xkrzwnbhahdq50sp0vx7sszzf")))

(define-public crate-sgf-parse-3.1.2 (c (n "sgf-parse") (v "3.1.2") (h "18mw0nhvwbzqjybx760jx6ai86mcbq7cmiwmhfsc2a485qk3a89z")))

(define-public crate-sgf-parse-3.1.3 (c (n "sgf-parse") (v "3.1.3") (h "0cjca2xq4ym54a2bw9iqzdyx2p3w8c4a1sdvym7b0p29w9pmp9i8")))

(define-public crate-sgf-parse-4.0.0 (c (n "sgf-parse") (v "4.0.0") (h "18bx7mjd3jdpqrdr7yp092l2x5k16n06is21sccccq6rdqwvls12")))

(define-public crate-sgf-parse-4.1.0 (c (n "sgf-parse") (v "4.1.0") (h "1drd9ndw00br0p9dxy127da96pxizbgj9wfq6pcqy31m2q761hbc")))

(define-public crate-sgf-parse-4.2.0 (c (n "sgf-parse") (v "4.2.0") (h "1g9hbrmq83lfbxapkgffj76rd6hgzmpaysk3apd9ga4j3nsn4g0j")))

(define-public crate-sgf-parse-4.2.1 (c (n "sgf-parse") (v "4.2.1") (h "1jm5dq482gk7qbmq5gmy7fa44mjwf30xc1b9f87f3mjnzrjv5vks")))

