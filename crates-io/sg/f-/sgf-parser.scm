(define-module (crates-io sg f- sgf-parser) #:use-module (crates-io))

(define-public crate-sgf-parser-0.1.0 (c (n "sgf-parser") (v "0.1.0") (d (list (d (n "nom") (r "^4.1.1") (d #t) (k 0)))) (h "159sv30ay0qw4d2sj0m7q9ik3b4i37hd1nvh2y6ci2igny36mfrb")))

(define-public crate-sgf-parser-0.2.0 (c (n "sgf-parser") (v "0.2.0") (d (list (d (n "derive_more") (r "^0.14.0") (d #t) (k 0)) (d (n "nom") (r "^4.1.1") (d #t) (k 0)) (d (n "pest") (r "^2.1.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "0cvg0b0ilgqp4an0j7v3rdbvzvx0iz66isgfj32gbmpc6vwyjqya")))

(define-public crate-sgf-parser-0.2.1 (c (n "sgf-parser") (v "0.2.1") (d (list (d (n "derive_more") (r "^0.14.0") (d #t) (k 0)) (d (n "pest") (r "^2.1.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "1km0jlknnd2y45n8z2wz3nj6y0lj4n2ycr0yc256fls75xqyfsbk")))

(define-public crate-sgf-parser-0.4.0 (c (n "sgf-parser") (v "0.4.0") (d (list (d (n "derive_more") (r "^0.14.0") (d #t) (k 0)) (d (n "pest") (r "^2.1.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "0biss9qcnfs4vvv9ipdrilnffk5fgk7wgh2g49716g8gg49v4avz")))

(define-public crate-sgf-parser-1.0.0 (c (n "sgf-parser") (v "1.0.0") (d (list (d (n "derive_more") (r "^0.14.0") (d #t) (k 0)) (d (n "pest") (r "^2.1.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "1gbanvvc06zg0hb6wxf23qpqkfrgh3qp59qz3pqy6czpzzyw96gx")))

(define-public crate-sgf-parser-2.0.0 (c (n "sgf-parser") (v "2.0.0") (d (list (d (n "derive_more") (r "^0.15.0") (d #t) (k 0)) (d (n "pest") (r "^2.1.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "15mmy6wmnilcy7fy2zkrsqg9i90drg7z5699llb138xa4lflgvcs")))

(define-public crate-sgf-parser-2.1.0 (c (n "sgf-parser") (v "2.1.0") (d (list (d (n "derive_more") (r "^0.15.0") (d #t) (k 0)) (d (n "pest") (r "^2.1.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "0nz4680zcqpinzjnl3jk4bfzxci5iyszcyfp8dkn4gv8a9bh780c")))

(define-public crate-sgf-parser-2.2.0 (c (n "sgf-parser") (v "2.2.0") (d (list (d (n "derive_more") (r "^0.15.0") (d #t) (k 0)) (d (n "pest") (r "^2.1.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "080s0j89mz7g0aw7afdsi86wipk36y49r8b9sc4ai6i7ljijp5y8")))

(define-public crate-sgf-parser-2.3.0 (c (n "sgf-parser") (v "2.3.0") (d (list (d (n "derive_more") (r "^0.15.0") (d #t) (k 0)) (d (n "pest") (r "^2.1.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "1m8gqb010cm84qi4bi1ykmhp41ap4xwa18pw4yk2abwz3g4i68mz")))

(define-public crate-sgf-parser-2.4.0 (c (n "sgf-parser") (v "2.4.0") (d (list (d (n "derive_more") (r "^0.99.2") (d #t) (k 0)) (d (n "pest") (r "^2.1.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "0jqi0lmi8qk7f9l6hng409dir9kjvshvsy07dp1v0a7w049hqsyv")))

(define-public crate-sgf-parser-2.5.0 (c (n "sgf-parser") (v "2.5.0") (d (list (d (n "derive_more") (r "^0.99.11") (d #t) (k 0)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "1p70vqbwng79m2z0vd778g5cz2z955bivmywbc5qwcrm3k9517y3")))

(define-public crate-sgf-parser-2.6.0 (c (n "sgf-parser") (v "2.6.0") (d (list (d (n "derive_more") (r "^0.99.11") (d #t) (k 0)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "17givsnrgs2ka1517lagv13x941md8n07qa38dm6wm5hb7gqysl6")))

