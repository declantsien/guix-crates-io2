(define-module (crates-io sg f- sgf-tool) #:use-module (crates-io))

(define-public crate-sgf-tool-1.0.0 (c (n "sgf-tool") (v "1.0.0") (d (list (d (n "pest") (r "^2.7.6") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum") (r "^0.25") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "1zna1ij6bfwiwbxq1fi7kw5f2i41kg7nsr8ly9c7a0a43wpdadbn") (r "1.74.1")))

(define-public crate-sgf-tool-1.0.1 (c (n "sgf-tool") (v "1.0.1") (d (list (d (n "pest") (r "^2.7.6") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum") (r "^0.25") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "02frs1zy0i6596r1v54acsd4ha4n9d6fv4ms1l4v3hfmgngx5n3g") (r "1.74.1")))

(define-public crate-sgf-tool-1.0.2 (c (n "sgf-tool") (v "1.0.2") (d (list (d (n "pest") (r "^2.7.6") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum") (r "^0.25") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "09pryd4sdhpskv883f6cv6fhpclzajb95lf8qb1ky4j6wnxxdl0l") (r "1.74.1")))

(define-public crate-sgf-tool-1.0.3 (c (n "sgf-tool") (v "1.0.3") (d (list (d (n "pest") (r "^2.7.6") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum") (r "^0.25") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "1c29y2cpdjd3r11sswr5bbckgksg9jgda11m7vx09mx5qrydvqx8") (r "1.74.1")))

(define-public crate-sgf-tool-1.0.4 (c (n "sgf-tool") (v "1.0.4") (d (list (d (n "pest") (r "^2.7.6") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum") (r "^0.25") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "03kqvnclr1inhwnjnqx0jbpjg6bn12lkwnyxkif8q2499s7d50zx") (r "1.74.1")))

