(define-module (crates-io sg da sgdata) #:use-module (crates-io))

(define-public crate-sgdata-0.1.0 (c (n "sgdata") (v "0.1.0") (d (list (d (n "owning_ref") (r "^0.3.3") (d #t) (k 0)))) (h "1f959g4n8y8djdqn0lrdp903r39w6yyshyikqwzwcxhnm22y86ah")))

(define-public crate-sgdata-0.2.0 (c (n "sgdata") (v "0.2.0") (d (list (d (n "owning_ref") (r "^0.4.1") (d #t) (k 0)))) (h "1c8ag70195ccn6fk5idpw20y7gnsan7zlqkciv8gr3x1znyn30q4")))

