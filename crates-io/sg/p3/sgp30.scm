(define-module (crates-io sg p3 sgp30) #:use-module (crates-io))

(define-public crate-sgp30-0.1.0 (c (n "sgp30") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "clippy") (r "^0.0.191") (o #t) (d #t) (k 0)) (d (n "embedded-hal") (r "^0.1") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.1") (d #t) (k 2)) (d (n "linux-embedded-hal") (r "^0.1") (d #t) (k 2)))) (h "193vqcj4k3160r5cpn7dwc4cbhzb0aic8l1pa7wlqa8l28iwj44z") (f (quote (("default"))))))

(define-public crate-sgp30-0.1.1 (c (n "sgp30") (v "0.1.1") (d (list (d (n "byteorder") (r "^1") (k 0)) (d (n "clippy") (r "^0.0.191") (o #t) (d #t) (k 0)) (d (n "embedded-hal") (r "^0.1") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.1") (d #t) (k 2)) (d (n "linux-embedded-hal") (r "^0.1") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (k 0)))) (h "11fcf3l6nqlyxa5n5k71npw79ksqck519vs71lln137flnwpfsvd") (f (quote (("default"))))))

(define-public crate-sgp30-0.2.0 (c (n "sgp30") (v "0.2.0") (d (list (d (n "byteorder") (r "^1") (k 0)) (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.2") (d #t) (k 2)) (d (n "linux-embedded-hal") (r "^0.2") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (k 0)))) (h "05nn79hgnb2l9fsjh0xhyycnc4nhac9shdhpn95gnkx2xh486lcx") (f (quote (("default"))))))

(define-public crate-sgp30-0.2.1 (c (n "sgp30") (v "0.2.1") (d (list (d (n "byteorder") (r "^1") (k 0)) (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.4") (d #t) (k 2)) (d (n "linux-embedded-hal") (r "^0.2") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (k 0)))) (h "18w5hrn0fip34isxrr2dmcag8cjwmspshdkyiaivshb55sfp6xn7") (f (quote (("default"))))))

(define-public crate-sgp30-0.3.0 (c (n "sgp30") (v "0.3.0") (d (list (d (n "byteorder") (r "^1") (k 0)) (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.7") (d #t) (k 2)) (d (n "linux-embedded-hal") (r "^0.3") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (k 0)))) (h "0mj1qmihmbzdsi5hyg43na2w2wy84xjr3czs657xsy9alwr7gvbq") (f (quote (("default"))))))

(define-public crate-sgp30-0.3.1 (c (n "sgp30") (v "0.3.1") (d (list (d (n "byteorder") (r "^1") (k 0)) (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.7") (d #t) (k 2)) (d (n "linux-embedded-hal") (r "^0.3") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (k 0)) (d (n "sensirion-i2c") (r "^0.1") (d #t) (k 0)))) (h "1fzlpfbqrway6v8rnbdmsfz9yiwb5cjwanfxfa7hchhswlyw2q9b") (f (quote (("default"))))))

(define-public crate-sgp30-0.3.2 (c (n "sgp30") (v "0.3.2") (d (list (d (n "byteorder") (r "^1") (k 0)) (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.10.0-rc.4") (f (quote ("eh0"))) (d #t) (k 2)) (d (n "linux-embedded-hal") (r "^0.3") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (k 0)) (d (n "sensirion-i2c") (r "^0.2") (d #t) (k 0)))) (h "0y5fylzvyh4s6cyzk1yn5daz1drcknjvci9h6v6x0fwhaxfyh04m") (f (quote (("default"))))))

