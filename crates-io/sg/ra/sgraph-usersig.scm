(define-module (crates-io sg ra sgraph-usersig) #:use-module (crates-io))

(define-public crate-sgraph-usersig-0.1.0 (c (n "sgraph-usersig") (v "0.1.0") (d (list (d (n "anchor-lang") (r "^0.26.0") (d #t) (k 0)) (d (n "sgraph") (r "^0.1.0") (f (quote ("cpi"))) (d #t) (k 0)) (d (n "solana-security-txt") (r "^1.1.0") (d #t) (k 0)))) (h "1hs7kkxaipa00aih76dp871qqh1xww60bc6il8ymsqp0fz9pi11b") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

