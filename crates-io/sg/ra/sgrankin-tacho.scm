(define-module (crates-io sg ra sgrankin-tacho) #:use-module (crates-io))

(define-public crate-sgrankin-tacho-0.5.0 (c (n "sgrankin-tacho") (v "0.5.0") (d (list (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "futures") (r "^0.3.5") (d #t) (k 0)) (d (n "hdrhistogram") (r "^7.1.0") (d #t) (k 0)) (d (n "indexmap") (r "^1.5.2") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "pin-project") (r "^0.4.23") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 2)) (d (n "tokio") (r "^0.2.22") (f (quote ("time" "stream"))) (d #t) (k 2)))) (h "1kh4b9y7927pys1p32lka3r3nfz9pfd2pig8407has4gipam0ban")))

(define-public crate-sgrankin-tacho-0.5.1 (c (n "sgrankin-tacho") (v "0.5.1") (d (list (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "futures") (r "^0.3.5") (d #t) (k 0)) (d (n "hdrhistogram") (r "^7.1.0") (d #t) (k 0)) (d (n "indexmap") (r "^1.5.2") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "pin-project") (r "^0.4.23") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 2)) (d (n "tokio") (r "^0.2.22") (f (quote ("time" "stream"))) (d #t) (k 2)))) (h "1scqg744v2vcad6dbz8mr96jmrgglmxaxxcmcyx8bpbvkrvjbcr9")))

