(define-module (crates-io sg fl sgflt) #:use-module (crates-io))

(define-public crate-sgflt-0.1.0 (c (n "sgflt") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.74") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "redis") (r "^0.24.0") (f (quote ("cluster-async" "tokio-rustls-comp"))) (d #t) (k 0)) (d (n "tokio") (r "^1.35.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "wd_log") (r "^0.2.0") (d #t) (k 0)) (d (n "wd_tools") (r "^0.9.0") (f (quote ("ptr" "uid" "point-free" "sync" "time" "md5"))) (d #t) (k 0)))) (h "0m0zvy7ghy6cc6sl1vx53p6pg7h2kx3jjld1mncl3gjy3885br7d")))

