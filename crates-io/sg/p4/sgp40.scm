(define-module (crates-io sg p4 sgp40) #:use-module (crates-io))

(define-public crate-sgp40-0.0.1 (c (n "sgp40") (v "0.0.1") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.7") (d #t) (k 2)) (d (n "linux-embedded-hal") (r "^0.3") (d #t) (k 2)) (d (n "sensirion-i2c") (r "^0.1") (d #t) (k 0)))) (h "0b5by3i1dsh5ndf3bdqb32shqzkrapmm00z9bwkz2saicpfp7d47")))

(define-public crate-sgp40-0.0.2 (c (n "sgp40") (v "0.0.2") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.7") (d #t) (k 2)) (d (n "linux-embedded-hal") (r "^0.3") (d #t) (k 2)) (d (n "sensirion-i2c") (r "^0.1") (d #t) (k 0)))) (h "0y97qwhbdavcqlnx09nc0hmz45mbl2hsb3xly1cyjp6q4pjbm549") (f (quote (("voc_index") ("default" "voc_index"))))))

(define-public crate-sgp40-0.0.3 (c (n "sgp40") (v "0.0.3") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.7") (d #t) (k 2)) (d (n "linux-embedded-hal") (r "^0.3") (d #t) (k 2)) (d (n "sensirion-i2c") (r "^0.1") (d #t) (k 0)))) (h "08rqhsmhghzjasbxhq41i3wv5xwhf95qnad0nqsb1scj5xlb416d") (f (quote (("voc_index") ("default" "voc_index"))))))

(define-public crate-sgp40-0.0.4 (c (n "sgp40") (v "0.0.4") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.7") (d #t) (k 2)) (d (n "fixed") (r "^1.6") (d #t) (k 0)) (d (n "fixed-sqrt") (r "^0.2") (d #t) (k 0)) (d (n "linux-embedded-hal") (r "^0.3") (d #t) (k 2)) (d (n "sensirion-i2c") (r "^0.1") (d #t) (k 0)))) (h "1mn16xqhcswjpjln1mla24mb8016mqzk9v5mmbwlwp2x03bqw273") (f (quote (("voc_index") ("default" "voc_index"))))))

