(define-module (crates-io sg p4 sgp4-rs) #:use-module (crates-io))

(define-public crate-sgp4-rs-0.1.0 (c (n "sgp4-rs") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.71") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "time") (r "^0.2.16") (d #t) (k 0)))) (h "062sdfb18gwnbcp3ldrq0nazgk84q0jqn5znldxkv9wii8775nd3") (l "static=sgp4")))

(define-public crate-sgp4-rs-0.2.1 (c (n "sgp4-rs") (v "0.2.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "chrono") (r "^0.4") (k 0)) (d (n "float-cmp") (r "^0.8") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0bv5xxc5nl3aaqqcdbnhm9iiwh4wwkv3594asicmlwssnyai9rw8") (l "static=sgp4")))

(define-public crate-sgp4-rs-0.3.0 (c (n "sgp4-rs") (v "0.3.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "chrono") (r "^0.4") (k 0)) (d (n "float-cmp") (r "^0.8") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "uom") (r "^0.31.1") (d #t) (k 0)))) (h "085dqykgdp781sljz0lfl0jp6gk179amdvvklgjcnhjawa9vsyky") (l "static=sgp4")))

(define-public crate-sgp4-rs-0.3.1 (c (n "sgp4-rs") (v "0.3.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "chrono") (r "^0.4") (k 0)) (d (n "float-cmp") (r "^0.8") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "uom") (r "^0.31.1") (d #t) (k 0)))) (h "052abypkvm3gczmjncpl7kdgh8rv7sjn7lnbqp092mbxfxagdbg3") (l "static=sgp4")))

(define-public crate-sgp4-rs-0.3.2 (c (n "sgp4-rs") (v "0.3.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "chrono") (r "^0.4") (k 0)) (d (n "float-cmp") (r "^0.9") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "uom") (r "^0.31.1") (d #t) (k 0)))) (h "18pgh6phsm7xv15pvy8sn1vlag47lajd6960r6sc89d6bhgh0ddv") (f (quote (("tlegen")))) (l "static=sgp4")))

(define-public crate-sgp4-rs-0.3.3 (c (n "sgp4-rs") (v "0.3.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "chrono") (r "^0.4") (k 0)) (d (n "float-cmp") (r "^0.9") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "uom") (r "^0.32.0") (d #t) (k 0)))) (h "0ka7ky5bzcjxqnvn4sidi3mjdq0j1m2v308jxpl334b9iflvbq2f") (f (quote (("tlegen")))) (l "static=sgp4")))

(define-public crate-sgp4-rs-0.3.4 (c (n "sgp4-rs") (v "0.3.4") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "chrono") (r "^0.4") (k 0)) (d (n "float-cmp") (r "^0.9") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "uom") (r "^0.32.0") (d #t) (k 0)))) (h "1wrniiqh8q6s6ypr5y0074v6m62fqfsddnc5dwb23g4b89awwxv8") (f (quote (("tlegen")))) (l "static=sgp4")))

(define-public crate-sgp4-rs-0.3.5 (c (n "sgp4-rs") (v "0.3.5") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "chrono") (r "^0.4") (k 0)) (d (n "float-cmp") (r "^0.9") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "uom") (r "^0.32.0") (d #t) (k 0)))) (h "1n6bwsjwa2jxrhq5axvxb68j4v1cv4bil877zy02vzy2pq2akcq0") (f (quote (("tlegen")))) (l "static=sgp4")))

(define-public crate-sgp4-rs-0.3.6 (c (n "sgp4-rs") (v "0.3.6") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "chrono") (r "^0.4") (k 0)) (d (n "float-cmp") (r "^0.9") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "uom") (r "^0.33.0") (d #t) (k 0)))) (h "1rzjx0kn8lgyyk1jf29s3b3x6znciy77znj3g8594ssdylmvc7vj") (f (quote (("tlegen")))) (l "static=sgp4")))

(define-public crate-sgp4-rs-0.3.7 (c (n "sgp4-rs") (v "0.3.7") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "chrono") (r "^0.4") (k 0)) (d (n "float-cmp") (r "^0.9") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "uom") (r "^0.34.0") (d #t) (k 0)))) (h "08h7n1hpnamjc7ga55nxi5k05fr49h1jp13x5jmhs0dvssxfrh5q") (f (quote (("tlegen")))) (l "static=sgp4")))

(define-public crate-sgp4-rs-0.3.8 (c (n "sgp4-rs") (v "0.3.8") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "chrono") (r "^0.4") (k 0)) (d (n "float-cmp") (r "^0.9") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "uom") (r "^0.34.0") (d #t) (k 0)))) (h "08dzsmijdclwwyk56dvkx3n39nbg5q8wj76cbrn8x0lhps2xl1yq") (f (quote (("tlegen")))) (l "static=sgp4")))

(define-public crate-sgp4-rs-0.4.0 (c (n "sgp4-rs") (v "0.4.0") (d (list (d (n "argmin") (r "^0.8.1") (o #t) (d #t) (k 0)) (d (n "argmin-math") (r "^0.3.0") (f (quote ("ndarray_latest-serde" "nalgebra_latest-serde"))) (o #t) (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "chrono") (r "^0.4.23") (k 0)) (d (n "float-cmp") (r "^0.9") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "uom") (r "^0.34.0") (d #t) (k 0)))) (h "184mq411lqpffr451klcd9xm8cvjwiarqc175w8m2hd8iqqclbv6") (l "static=sgp4") (s 2) (e (quote (("tlegen" "dep:argmin" "dep:argmin-math"))))))

