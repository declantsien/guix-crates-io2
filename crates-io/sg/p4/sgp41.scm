(define-module (crates-io sg p4 sgp41) #:use-module (crates-io))

(define-public crate-sgp41-0.1.0 (c (n "sgp41") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.8") (d #t) (k 2)) (d (n "linux-embedded-hal") (r "^0.3") (d #t) (k 2)) (d (n "sensirion-i2c") (r "^0.1") (d #t) (k 0)))) (h "0njnw17f1kkz0wici4447l6w09vb7wg9594553nrmg660f72g6cw")))

(define-public crate-sgp41-0.1.1 (c (n "sgp41") (v "0.1.1") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.8") (d #t) (k 2)) (d (n "linux-embedded-hal") (r "^0.3") (d #t) (k 2)) (d (n "sensirion-i2c") (r "^0.1") (d #t) (k 0)))) (h "0d9cdp4d8cp19l3cnacdh571fbb32hgm0ifn3wf34yngpkxh5vjs")))

