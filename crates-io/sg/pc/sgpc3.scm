(define-module (crates-io sg pc sgpc3) #:use-module (crates-io))

(define-public crate-sgpc3-0.0.1 (c (n "sgpc3") (v "0.0.1") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.7") (d #t) (k 2)) (d (n "linux-embedded-hal") (r "^0.3") (d #t) (k 2)) (d (n "sensirion-i2c") (r "^0.1") (d #t) (k 0)))) (h "1qaji4i5in7zkz9w0vg1xliidv78nfz8yh9djv028rcs2pb9a18y")))

(define-public crate-sgpc3-0.0.2 (c (n "sgpc3") (v "0.0.2") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.7") (d #t) (k 2)) (d (n "linux-embedded-hal") (r "^0.3") (d #t) (k 2)) (d (n "sensirion-i2c") (r "^0.1") (d #t) (k 0)))) (h "10bb26wwinyp8rybhrqg60xpnlm6g3f7sknk4z2bby06kwa27blk")))

(define-public crate-sgpc3-0.0.3 (c (n "sgpc3") (v "0.0.3") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.7") (d #t) (k 2)) (d (n "linux-embedded-hal") (r "^0.3") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.14") (f (quote ("libm"))) (k 0)) (d (n "sensirion-i2c") (r "^0.1") (d #t) (k 0)))) (h "15y5bmd7rc8kdjpxym02wlng3z0qrnzcba4wl1kj5wghyf4w1avf")))

(define-public crate-sgpc3-1.0.0 (c (n "sgpc3") (v "1.0.0") (d (list (d (n "cordic") (r "^0.1") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.7") (d #t) (k 2)) (d (n "fixed") (r "^1") (d #t) (k 0)) (d (n "linux-embedded-hal") (r "^0.3") (d #t) (k 2)) (d (n "sensirion-i2c") (r "^0.1") (d #t) (k 0)))) (h "0bsyclhzl4zwf204bdjvwn8dprjprv2rxj642xx81b5s445sbq7g")))

