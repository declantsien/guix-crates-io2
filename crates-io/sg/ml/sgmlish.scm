(define-module (crates-io sg ml sgmlish) #:use-module (crates-io))

(define-public crate-sgmlish-0.1.0 (c (n "sgmlish") (v "0.1.0") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "never") (r "^0.1.0") (k 0)) (d (n "nom") (r "^7.0") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.15") (f (quote ("serde-str"))) (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "simple_logger") (r "^1.13.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0g9pc10hc2f4j6hxap1d0spz2pksrk0pxqcjqmzylwmjnacva8bs") (f (quote (("deserialize" "serde") ("default" "deserialize"))))))

(define-public crate-sgmlish-0.2.0 (c (n "sgmlish") (v "0.2.0") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "nom") (r "^7.0") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.15") (f (quote ("serde-str"))) (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "simple_logger") (r "^1.13.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "15xzkikxvi1ms9il05f28vrv8g2y0xmrnlkxb1744zvan9l81h0m") (f (quote (("default" "serde"))))))

