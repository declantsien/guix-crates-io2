(define-module (crates-io sg m- sgm-rs) #:use-module (crates-io))

(define-public crate-sgm-rs-0.1.0 (c (n "sgm-rs") (v "0.1.0") (d (list (d (n "image") (r "^0.23.12") (d #t) (k 0)) (d (n "ndarray") (r "^0.14.0") (d #t) (k 0)))) (h "07nbqjc1i9i96999cjwyix55by7y1wck4b6llwdaf222rfc8mnal")))

