(define-module (crates-io sg x_ sgx_tse) #:use-module (crates-io))

(define-public crate-sgx_tse-0.9.8 (c (n "sgx_tse") (v "0.9.8") (d (list (d (n "sgx_types") (r "^0.9.8") (d #t) (k 0)))) (h "1d05pw6gla1ndxhwg529w4wsy0pc03c8bybzw0961b5ndwvfvb4s") (f (quote (("default"))))))

(define-public crate-sgx_tse-1.0.0 (c (n "sgx_tse") (v "1.0.0") (d (list (d (n "sgx_types") (r "= 1.0.0") (d #t) (k 0)))) (h "1p0x0mlf6i7z41y6xvvhhh6dzkq3cbr5hsvxkjgjvqjh1p24iqnx") (f (quote (("default"))))))

(define-public crate-sgx_tse-1.0.1 (c (n "sgx_tse") (v "1.0.1") (d (list (d (n "sgx_types") (r "= 1.0.1") (d #t) (k 0)))) (h "0p3ivi54nv9vsqkmvaxl538fv60r0lgj8642skx8arsbqxc2jzf3") (f (quote (("default"))))))

(define-public crate-sgx_tse-1.0.4 (c (n "sgx_tse") (v "1.0.4") (d (list (d (n "sgx_types") (r "= 1.0.4") (d #t) (k 0)))) (h "1m5ra0b32lkqsqg2myhipf88raxsjcx9p6axrcy0p4z81rz86v3h") (f (quote (("default"))))))

(define-public crate-sgx_tse-1.0.5 (c (n "sgx_tse") (v "1.0.5") (d (list (d (n "sgx_types") (r "= 1.0.5") (d #t) (k 0)))) (h "0z49w1cxcdfvcgg73y545xfqcrxpd7dajlasrxbd1cmxklm96nb6") (f (quote (("default"))))))

(define-public crate-sgx_tse-1.0.6 (c (n "sgx_tse") (v "1.0.6") (d (list (d (n "sgx_types") (r "= 1.0.6") (d #t) (k 0)))) (h "0n5487mcfp8m7ifkhrp2fa9ghlxr3fyl9kjq2y25cc2dv2a2k608") (f (quote (("default"))))))

(define-public crate-sgx_tse-1.0.7 (c (n "sgx_tse") (v "1.0.7") (d (list (d (n "sgx_types") (r "= 1.0.7") (d #t) (k 0)))) (h "1krv4xckwr9qlg9m4yw38pwqr940r59i40jgivnqa8sr34z02v32") (f (quote (("default"))))))

(define-public crate-sgx_tse-1.0.8 (c (n "sgx_tse") (v "1.0.8") (d (list (d (n "sgx_types") (r "= 1.0.8") (d #t) (t "cfg(not(target_env = \"sgx\"))") (k 0)))) (h "0pvixfd46mdsdbjkc5nhnwvp4wv0nf8rcglh4gx1706kjpkn47ri") (f (quote (("default"))))))

(define-public crate-sgx_tse-1.0.9 (c (n "sgx_tse") (v "1.0.9") (d (list (d (n "sgx_types") (r "= 1.0.9") (d #t) (t "cfg(not(target_env = \"sgx\"))") (k 0)))) (h "0lkbi9svfw9fdw33fl34fjpskfmgi1w9j5imfg8zlrcv72f03wf2") (f (quote (("default"))))))

(define-public crate-sgx_tse-1.1.0 (c (n "sgx_tse") (v "1.1.0") (d (list (d (n "sgx_types") (r "= 1.1.0") (d #t) (t "cfg(not(target_env = \"sgx\"))") (k 0)))) (h "00190hv5iz5wqyy2az3xzj7zjv0mc1p97dy1cdh85l1m9gzm3av3") (f (quote (("default"))))))

(define-public crate-sgx_tse-1.1.1 (c (n "sgx_tse") (v "1.1.1") (d (list (d (n "sgx_types") (r "= 1.1.1") (d #t) (t "cfg(not(target_env = \"sgx\"))") (k 0)))) (h "1i5lbj9v8rjy241c7ai7pn62yqhd41fhak37hnhlk1q2pb5fxcbb") (f (quote (("default"))))))

