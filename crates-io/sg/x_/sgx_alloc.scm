(define-module (crates-io sg x_ sgx_alloc) #:use-module (crates-io))

(define-public crate-sgx_alloc-0.9.8 (c (n "sgx_alloc") (v "0.9.8") (d (list (d (n "sgx_trts") (r "^0.9.8") (d #t) (k 0)))) (h "1kcxaxnf257v6fqbmfaqwci5p4rm5v7wqq0962d7w0rfqzv2i19g") (f (quote (("default"))))))

(define-public crate-sgx_alloc-1.0.0 (c (n "sgx_alloc") (v "1.0.0") (d (list (d (n "sgx_trts") (r "= 1.0.0") (d #t) (k 0)))) (h "0pg380nlbi4h4wrw02y5xdaivflgyar2yycs86rysznh5g5nah02") (f (quote (("default"))))))

(define-public crate-sgx_alloc-1.0.1 (c (n "sgx_alloc") (v "1.0.1") (d (list (d (n "sgx_trts") (r "= 1.0.1") (d #t) (k 0)))) (h "05gd5zn41wcyc0q840sjh2g2bnki9008m9fzwn36vyr5hhm2f6m5") (f (quote (("default"))))))

(define-public crate-sgx_alloc-1.0.4 (c (n "sgx_alloc") (v "1.0.4") (d (list (d (n "sgx_trts") (r "= 1.0.4") (d #t) (k 0)))) (h "1a7z1jwxi5b0zwjbb8s0agqkbm1vcrvp5i9kigbjhmrs9ajyfi13") (f (quote (("default"))))))

(define-public crate-sgx_alloc-1.0.5 (c (n "sgx_alloc") (v "1.0.5") (d (list (d (n "sgx_trts") (r "= 1.0.5") (d #t) (k 0)))) (h "1b5pkii7ngx5ravcxadf2y5yk9k7mjk9magm869lg7f9ilk3a1nc") (f (quote (("default"))))))

(define-public crate-sgx_alloc-1.0.6 (c (n "sgx_alloc") (v "1.0.6") (d (list (d (n "sgx_trts") (r "= 1.0.6") (d #t) (k 0)))) (h "0qqcfvgyva3g72m90n1rk9r9myzpwcxbglw6pkad20rc1krdaqgh") (f (quote (("default"))))))

(define-public crate-sgx_alloc-1.0.7 (c (n "sgx_alloc") (v "1.0.7") (d (list (d (n "sgx_trts") (r "= 1.0.7") (d #t) (k 0)))) (h "07cwz0f55cp8gbchaqz5y1brfgpnhcadkfzgizzw1dlw80d0ahc6") (f (quote (("default"))))))

(define-public crate-sgx_alloc-1.0.8 (c (n "sgx_alloc") (v "1.0.8") (d (list (d (n "sgx_trts") (r "= 1.0.8") (d #t) (t "cfg(not(target_env = \"sgx\"))") (k 0)))) (h "18rw08jwc17bg7faz6yvhbvjbxcw9jfyq3q4whgqc4cqbicah7fh") (f (quote (("default"))))))

(define-public crate-sgx_alloc-1.0.9 (c (n "sgx_alloc") (v "1.0.9") (d (list (d (n "sgx_trts") (r "= 1.0.9") (d #t) (t "cfg(not(target_env = \"sgx\"))") (k 0)))) (h "1lnk2g6gq341fg58d5mjydndrysscxxirfwpv6nkm5qncy01rxif") (f (quote (("default"))))))

(define-public crate-sgx_alloc-1.1.0 (c (n "sgx_alloc") (v "1.1.0") (h "072r42rx21vvcy7w1v3vpdz8xgfdanigq3dsfj4i4zhzxll960i3") (f (quote (("default"))))))

(define-public crate-sgx_alloc-1.1.1 (c (n "sgx_alloc") (v "1.1.1") (h "1fpg1m57aavric2qldfxm0iqq4ca1h825jc8rka0bbxn9j04m044") (f (quote (("default"))))))

