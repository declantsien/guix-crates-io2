(define-module (crates-io sg x_ sgx_demangle) #:use-module (crates-io))

(define-public crate-sgx_demangle-1.0.9 (c (n "sgx_demangle") (v "1.0.9") (h "1g9h57qr9z362zg1qgf8x4ix727b01v1vif91cb36myq2hwl42ds")))

(define-public crate-sgx_demangle-1.1.0 (c (n "sgx_demangle") (v "1.1.0") (h "167imkaxjqi7q31x2fr5pmicm4g0j811mcdzcwny1cnwvycml5az")))

(define-public crate-sgx_demangle-1.1.1 (c (n "sgx_demangle") (v "1.1.1") (h "0464v5gzn6naag7hmgczf13l8hap9zf3jmlr1wc5glaqhb392anz")))

