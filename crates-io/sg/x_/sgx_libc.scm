(define-module (crates-io sg x_ sgx_libc) #:use-module (crates-io))

(define-public crate-sgx_libc-0.0.1 (c (n "sgx_libc") (v "0.0.1") (h "1q73m5rpfjjp14c3931k477draqkkj3cpks6xpwfdwr7gx6y9n2m")))

(define-public crate-sgx_libc-1.0.5 (c (n "sgx_libc") (v "1.0.5") (d (list (d (n "sgx_types") (r "= 1.0.5") (d #t) (t "cfg(all(target_os = \"linux\", target_arch = \"x86_64\"))") (k 0)))) (h "1b3y5lqgb6ixwpfgi6v8i7rir60bc28brgx9cv8c6mnv62xckifq") (f (quote (("default" "align") ("align"))))))

(define-public crate-sgx_libc-1.0.5-fix (c (n "sgx_libc") (v "1.0.5-fix") (d (list (d (n "sgx_types") (r "= 1.0.5") (d #t) (t "cfg(all(target_os = \"linux\", target_arch = \"x86_64\"))") (k 0)))) (h "1spplgqp99calrc966vxxxwva6mss9gq8xiyh96anlxw70c2778x") (f (quote (("default" "align") ("align"))))))

(define-public crate-sgx_libc-1.0.6 (c (n "sgx_libc") (v "1.0.6") (d (list (d (n "sgx_types") (r "= 1.0.6") (d #t) (t "cfg(all(target_os = \"linux\", target_arch = \"x86_64\"))") (k 0)))) (h "15n33cr2crg46qcqlcah0m430wv4b8w8z2xin1a80v2aza445a4y") (f (quote (("default" "align") ("align"))))))

(define-public crate-sgx_libc-1.0.7 (c (n "sgx_libc") (v "1.0.7") (d (list (d (n "sgx_types") (r "= 1.0.7") (d #t) (t "cfg(all(target_os = \"linux\", target_arch = \"x86_64\"))") (k 0)))) (h "0pfd20qbic74j81x9ivnadj6m6ayzkxfsyz681yj00f4ghpr29c8") (f (quote (("default" "align") ("align"))))))

(define-public crate-sgx_libc-1.0.8 (c (n "sgx_libc") (v "1.0.8") (d (list (d (n "sgx_types") (r "= 1.0.8") (d #t) (t "cfg(all(not(target_env = \"sgx\"), target_os = \"linux\", target_arch = \"x86_64\"))") (k 0)))) (h "0pbn4b6g7njv7npk0ldx8hfg3avx6c4h7igzgm1mirb2rxh239hm") (f (quote (("default" "align") ("align"))))))

(define-public crate-sgx_libc-1.0.9 (c (n "sgx_libc") (v "1.0.9") (d (list (d (n "sgx_types") (r "= 1.0.9") (d #t) (t "cfg(all(not(target_env = \"sgx\"), target_os = \"linux\", target_arch = \"x86_64\"))") (k 0)))) (h "1c25v5m92k1pgxjpawfzi88z9arlzjgggxfamwjcwn7nhrpc9cy6") (f (quote (("default" "align") ("align"))))))

(define-public crate-sgx_libc-1.1.0 (c (n "sgx_libc") (v "1.1.0") (d (list (d (n "sgx_types") (r "= 1.1.0") (d #t) (t "cfg(all(not(target_env = \"sgx\"), target_os = \"linux\", target_arch = \"x86_64\"))") (k 0)))) (h "0zy0q1bq6bns96y7xgaik53ljc2jkzcwg4zsnfc797rmcginnhh6") (f (quote (("default" "align") ("align"))))))

(define-public crate-sgx_libc-1.1.1 (c (n "sgx_libc") (v "1.1.1") (d (list (d (n "sgx_types") (r "= 1.1.1") (d #t) (t "cfg(all(not(target_env = \"sgx\"), target_os = \"linux\", target_arch = \"x86_64\"))") (k 0)))) (h "0br415chwg2xqrjwr17blwzcc3fc1094fi7sg7c1wccyw75w9qpy") (f (quote (("default" "align") ("align"))))))

