(define-module (crates-io sg x_ sgx_serialize_derive_internals) #:use-module (crates-io))

(define-public crate-sgx_serialize_derive_internals-0.9.8 (c (n "sgx_serialize_derive_internals") (v "0.9.8") (d (list (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "18hnc9ab1yb73zbjg3i6vsls9kkrbbkrkj2gardhbxcc4b8w9p9w")))

(define-public crate-sgx_serialize_derive_internals-1.0.0 (c (n "sgx_serialize_derive_internals") (v "1.0.0") (d (list (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "06bmwjmh3md3zfg4q14pa3vhw5r40wgbhf7vkh6cwhwnl5yz7knc")))

(define-public crate-sgx_serialize_derive_internals-1.0.1 (c (n "sgx_serialize_derive_internals") (v "1.0.1") (d (list (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "0say98wrwiyzd8varaq0wjy8kq74qf6ipzn6mx82arzjxvhsvy18")))

(define-public crate-sgx_serialize_derive_internals-1.0.4 (c (n "sgx_serialize_derive_internals") (v "1.0.4") (d (list (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "0llz7rwm9azdsrgmysp9ha01zdhr55a0w76p9g536l9vf8bbfw5n")))

(define-public crate-sgx_serialize_derive_internals-1.0.5 (c (n "sgx_serialize_derive_internals") (v "1.0.5") (d (list (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "1q04nf5x2bwzwcap5798kpf9s8r2fk5ifx4cn8spnrglhn5sp2ma")))

(define-public crate-sgx_serialize_derive_internals-1.0.6 (c (n "sgx_serialize_derive_internals") (v "1.0.6") (d (list (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "1v5j5gc2iqwlw60n9ky97gfkvknsxxl1lajyf4lyawd9b6mz4075")))

(define-public crate-sgx_serialize_derive_internals-1.0.7 (c (n "sgx_serialize_derive_internals") (v "1.0.7") (d (list (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "1dx0axg8ils9flk8wz81271cv8mhqyym33gvf1nanwjxysg5kbni")))

(define-public crate-sgx_serialize_derive_internals-1.0.8 (c (n "sgx_serialize_derive_internals") (v "1.0.8") (d (list (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "1gbswsqlhy6i1xnxch0186bm4p4nynzycrhgyqb5xp3q4g2i69b4")))

(define-public crate-sgx_serialize_derive_internals-1.0.9 (c (n "sgx_serialize_derive_internals") (v "1.0.9") (d (list (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "1jfqhsmn1gs9c5anp3anbfr2sc9xxf2wbmwz46zas2bpj04q9aj6")))

(define-public crate-sgx_serialize_derive_internals-1.1.0 (c (n "sgx_serialize_derive_internals") (v "1.1.0") (d (list (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "1zy7p5z28jwm7vzvbh4wqd3wp9mbcmyk4pgfvslskzravayyhfsm")))

(define-public crate-sgx_serialize_derive_internals-1.1.1 (c (n "sgx_serialize_derive_internals") (v "1.1.1") (d (list (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "0jrqsigzm23mlph2jx126903xy9s0rchar5hqp8279966fl3y7nd")))

