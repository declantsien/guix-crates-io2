(define-module (crates-io sg x_ sgx_rand) #:use-module (crates-io))

(define-public crate-sgx_rand-0.9.8 (c (n "sgx_rand") (v "0.9.8") (d (list (d (n "sgx_trts") (r "^0.9.8") (d #t) (k 0)) (d (n "sgx_tstd") (r "^0.9.8") (d #t) (k 0)) (d (n "sgx_types") (r "^0.9.8") (d #t) (k 0)))) (h "0sikblbv057pxkpd4ss4ham6ygqnma71gfyb7za84h8lasyc40ba") (f (quote (("default"))))))

(define-public crate-sgx_rand-1.0.0 (c (n "sgx_rand") (v "1.0.0") (d (list (d (n "sgx_trts") (r "= 1.0.0") (d #t) (k 0)) (d (n "sgx_tstd") (r "= 1.0.0") (d #t) (k 0)) (d (n "sgx_types") (r "= 1.0.0") (d #t) (k 0)))) (h "0cyfbr6ch714qf0sjgraxx6shk6dqd4qhbmsddrpy80ksjz8zz5a") (f (quote (("default"))))))

(define-public crate-sgx_rand-1.0.1 (c (n "sgx_rand") (v "1.0.1") (d (list (d (n "sgx_trts") (r "= 1.0.1") (d #t) (k 0)) (d (n "sgx_tstd") (r "= 1.0.1") (d #t) (k 0)) (d (n "sgx_types") (r "= 1.0.1") (d #t) (k 0)))) (h "0dirbyraabykvqi752naffflqznnrkkinxk2wskbn6b52cq42j74") (f (quote (("default"))))))

(define-public crate-sgx_rand-1.0.4 (c (n "sgx_rand") (v "1.0.4") (d (list (d (n "sgx_trts") (r "= 1.0.4") (d #t) (k 0)) (d (n "sgx_tstd") (r "= 1.0.4") (d #t) (k 0)) (d (n "sgx_types") (r "= 1.0.4") (d #t) (k 0)))) (h "05dqwh1v1z1iqq5zbl0hhhq518x5cdvvrz3wl286q3r58jf0gfl1") (f (quote (("default"))))))

(define-public crate-sgx_rand-1.0.5 (c (n "sgx_rand") (v "1.0.5") (d (list (d (n "sgx_trts") (r "= 1.0.5") (d #t) (k 0)) (d (n "sgx_tstd") (r "= 1.0.5") (d #t) (k 0)) (d (n "sgx_types") (r "= 1.0.5") (d #t) (k 0)))) (h "0iy106rzf96jsi5ymhg8nk85cak1gz8y7852a4v023cgqq8f7g7n") (f (quote (("default"))))))

(define-public crate-sgx_rand-1.0.6 (c (n "sgx_rand") (v "1.0.6") (d (list (d (n "sgx_trts") (r "= 1.0.6") (d #t) (k 0)) (d (n "sgx_tstd") (r "= 1.0.6") (d #t) (k 0)) (d (n "sgx_types") (r "= 1.0.6") (d #t) (k 0)))) (h "1nk0k9s0zjj3w3gn8bhgx969kd5cnjd2ryqx3m7az9zyidzwnhnc") (f (quote (("default"))))))

(define-public crate-sgx_rand-1.0.7 (c (n "sgx_rand") (v "1.0.7") (d (list (d (n "sgx_trts") (r "= 1.0.7") (d #t) (k 0)) (d (n "sgx_tstd") (r "= 1.0.7") (d #t) (k 0)) (d (n "sgx_types") (r "= 1.0.7") (d #t) (k 0)))) (h "0w6cpas00fl7rvsadfnnhh1lwzb86vs24q1knmkpr91d8gzwmdm6") (f (quote (("default"))))))

(define-public crate-sgx_rand-1.0.8 (c (n "sgx_rand") (v "1.0.8") (d (list (d (n "sgx_trts") (r "= 1.0.8") (d #t) (t "cfg(not(target_env = \"sgx\"))") (k 0)) (d (n "sgx_tstd") (r "= 1.0.8") (d #t) (t "cfg(not(target_env = \"sgx\"))") (k 0)) (d (n "sgx_types") (r "= 1.0.8") (d #t) (t "cfg(not(target_env = \"sgx\"))") (k 0)))) (h "0lyhj41l9rh06b2yj1kia76dg9kd5kn7rdd6q8qqw5gcqh9ns11a") (f (quote (("default"))))))

(define-public crate-sgx_rand-1.0.9 (c (n "sgx_rand") (v "1.0.9") (d (list (d (n "sgx_trts") (r "= 1.0.9") (d #t) (t "cfg(not(target_env = \"sgx\"))") (k 0)) (d (n "sgx_tstd") (r "= 1.0.9") (d #t) (t "cfg(not(target_env = \"sgx\"))") (k 0)) (d (n "sgx_types") (r "= 1.0.9") (d #t) (t "cfg(not(target_env = \"sgx\"))") (k 0)))) (h "15lrkyv8726p8bppyq4ijncr2xsd9b47av9w8p4zj4lsrrl034ab") (f (quote (("default"))))))

(define-public crate-sgx_rand-1.1.0 (c (n "sgx_rand") (v "1.1.0") (d (list (d (n "sgx_trts") (r "= 1.1.0") (d #t) (t "cfg(not(target_env = \"sgx\"))") (k 0)) (d (n "sgx_tstd") (r "= 1.1.0") (d #t) (t "cfg(not(target_env = \"sgx\"))") (k 0)) (d (n "sgx_types") (r "= 1.1.0") (d #t) (t "cfg(not(target_env = \"sgx\"))") (k 0)))) (h "1rjfd5djinsn477mwc1ns4jvlv1a1khcrp8ylaby1zbn6gymsk7g") (f (quote (("default"))))))

(define-public crate-sgx_rand-1.1.1 (c (n "sgx_rand") (v "1.1.1") (d (list (d (n "sgx_trts") (r "= 1.1.1") (d #t) (t "cfg(not(target_env = \"sgx\"))") (k 0)) (d (n "sgx_tstd") (r "= 1.1.1") (d #t) (t "cfg(not(target_env = \"sgx\"))") (k 0)) (d (n "sgx_types") (r "= 1.1.1") (d #t) (t "cfg(not(target_env = \"sgx\"))") (k 0)))) (h "1zssj18cjcd27pnfi8q5m2i92hxxag1kgr7pvml7fyqx4zc74h6c") (f (quote (("default"))))))

