(define-module (crates-io sg x_ sgx_align_struct_attribute) #:use-module (crates-io))

(define-public crate-sgx_align_struct_attribute-1.1.1 (c (n "sgx_align_struct_attribute") (v "1.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "114x8i896gwlk8aglbybda3k2x78icx2959bbf0jh2i1qxlv3lj9")))

