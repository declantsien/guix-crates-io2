(define-module (crates-io sg x_ sgx_urts) #:use-module (crates-io))

(define-public crate-sgx_urts-0.9.8 (c (n "sgx_urts") (v "0.9.8") (d (list (d (n "libc") (r "^0.2.40") (d #t) (k 0)) (d (n "sgx_types") (r "^0.9.8") (d #t) (k 0)))) (h "0kbfxfldmqw7bmmk0w59ww4bkm2akigdkwvwvh9276hdnwy9wc3p") (f (quote (("global_init") ("global_exit") ("default"))))))

(define-public crate-sgx_urts-1.0.0 (c (n "sgx_urts") (v "1.0.0") (d (list (d (n "libc") (r "^0.2.40") (d #t) (k 0)) (d (n "sgx_types") (r "= 1.0.0") (d #t) (k 0)))) (h "19ii5nxrm1fl4ma7mihp7ij68adxc9ph08j8pqpcsjghwhd33s49") (f (quote (("global_init") ("global_exit") ("default"))))))

(define-public crate-sgx_urts-1.0.1 (c (n "sgx_urts") (v "1.0.1") (d (list (d (n "libc") (r "^0.2.40") (d #t) (k 0)) (d (n "sgx_types") (r "= 1.0.1") (d #t) (k 0)))) (h "1fl4d7s5q074sw3l549v4xvic11yy8jnir9x5xg92xlg0ixx378d") (f (quote (("global_init") ("global_exit") ("default"))))))

(define-public crate-sgx_urts-1.0.4 (c (n "sgx_urts") (v "1.0.4") (d (list (d (n "libc") (r "^0.2.40") (d #t) (k 0)) (d (n "sgx_types") (r "= 1.0.4") (d #t) (k 0)))) (h "0vv1iyi8aky9qw45wipig0w047cfqgwndr3zwbsh5k9v9b3jbsxd") (f (quote (("global_init") ("global_exit") ("default"))))))

(define-public crate-sgx_urts-1.0.5 (c (n "sgx_urts") (v "1.0.5") (d (list (d (n "libc") (r "^0.2.40") (d #t) (k 0)) (d (n "sgx_types") (r "= 1.0.5") (d #t) (k 0)))) (h "0m6kzc84n7jwzc1dqa3ycijnq2jwnjm2rh8bjhn8y9a3y8i61n81") (f (quote (("global_init") ("global_exit") ("default"))))))

(define-public crate-sgx_urts-1.0.6 (c (n "sgx_urts") (v "1.0.6") (d (list (d (n "libc") (r "^0.2.40") (d #t) (k 0)) (d (n "sgx_types") (r "= 1.0.6") (d #t) (k 0)))) (h "0sdqv1sgf1yw3zxgh77gj5m9fc3q99f2gxr5gmm16vr60yw50ixm") (f (quote (("global_init") ("global_exit") ("default"))))))

(define-public crate-sgx_urts-1.0.7 (c (n "sgx_urts") (v "1.0.7") (d (list (d (n "libc") (r "^0.2.40") (d #t) (k 0)) (d (n "sgx_types") (r "= 1.0.7") (d #t) (k 0)))) (h "03qp85z64yh04jpykf6n36w65jfdmhhmyjrjg5zscvnxpm47mp8d") (f (quote (("global_init") ("global_exit") ("default"))))))

(define-public crate-sgx_urts-1.0.8 (c (n "sgx_urts") (v "1.0.8") (d (list (d (n "libc") (r "^0.2.40") (d #t) (k 0)) (d (n "sgx_types") (r "= 1.0.8") (d #t) (k 0)))) (h "0vim7hghqhswz6ffjqszdjpn90700n01fsa9xhll6nb4axrljfxz") (f (quote (("global_init") ("global_exit") ("default"))))))

(define-public crate-sgx_urts-1.0.9 (c (n "sgx_urts") (v "1.0.9") (d (list (d (n "libc") (r "^0.2.40") (d #t) (k 0)) (d (n "sgx_types") (r "= 1.0.9") (d #t) (k 0)))) (h "0n5fd9pkq6bwh2v7g0hkzcia3jc09pl8vb9555w0z7s3v27f87qh") (f (quote (("global_init") ("global_exit") ("default"))))))

(define-public crate-sgx_urts-1.1.0 (c (n "sgx_urts") (v "1.1.0") (d (list (d (n "libc") (r "^0.2.40") (d #t) (k 0)) (d (n "sgx_types") (r "= 1.1.0") (d #t) (k 0)))) (h "003mjgkpxs8cfl87vzc2b5130d0vq1grah7ka12lzrk4rn749x76") (f (quote (("global_init") ("global_exit") ("default"))))))

(define-public crate-sgx_urts-1.1.1 (c (n "sgx_urts") (v "1.1.1") (d (list (d (n "libc") (r "^0.2.40") (d #t) (k 0)) (d (n "sgx_types") (r "= 1.1.1") (d #t) (k 0)))) (h "1237wksxxnn2h3nr6hld0hyv9d6kmxr0c8nbzr0ggn4wix5hwrks") (f (quote (("global_init") ("global_exit") ("default"))))))

