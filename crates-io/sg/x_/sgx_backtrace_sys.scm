(define-module (crates-io sg x_ sgx_backtrace_sys) #:use-module (crates-io))

(define-public crate-sgx_backtrace_sys-1.0.9 (c (n "sgx_backtrace_sys") (v "1.0.9") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "sgx_build_helper") (r "^0.1.1") (d #t) (k 1)) (d (n "sgx_libc") (r "= 1.0.9") (d #t) (t "cfg(not(target_env = \"sgx\"))") (k 0)))) (h "0xik1kqnhyklz0ya18cnpnr4v5mchb1w9kfs3jin0k1ql0ngmx5m") (f (quote (("default"))))))

(define-public crate-sgx_backtrace_sys-1.1.0 (c (n "sgx_backtrace_sys") (v "1.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "sgx_build_helper") (r "^0.1") (d #t) (k 1)) (d (n "sgx_libc") (r "= 1.1.0") (d #t) (t "cfg(not(target_env = \"sgx\"))") (k 0)))) (h "11wdx5v9d99lya8z9kx6bl4p3qjbk5fk93a6g5z8z6z2s8mp0a1s") (f (quote (("default"))))))

(define-public crate-sgx_backtrace_sys-1.1.1 (c (n "sgx_backtrace_sys") (v "1.1.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "sgx_build_helper") (r "^0.1.3") (d #t) (k 1)) (d (n "sgx_libc") (r "= 1.1.1") (d #t) (t "cfg(not(target_env = \"sgx\"))") (k 0)))) (h "1y7wfx7m7vfxsm2xvfldhkcm3rbqk45z7f8rhqj401nrdy8ccc6i") (f (quote (("default"))))))

