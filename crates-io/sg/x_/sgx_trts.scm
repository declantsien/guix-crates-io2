(define-module (crates-io sg x_ sgx_trts) #:use-module (crates-io))

(define-public crate-sgx_trts-0.9.8 (c (n "sgx_trts") (v "0.9.8") (d (list (d (n "sgx_types") (r "^0.9.8") (d #t) (k 0)))) (h "1sh30s9lcgqn5kgrzkik67shp5qnnxih2kvh3i0p315mrs8a94qj") (f (quote (("default"))))))

(define-public crate-sgx_trts-1.0.0 (c (n "sgx_trts") (v "1.0.0") (d (list (d (n "sgx_types") (r "= 1.0.0") (d #t) (k 0)))) (h "0z83w72jv0yswz71hifd8zji3ds8vnxv7xagnii219xa08alhgsw") (f (quote (("default"))))))

(define-public crate-sgx_trts-1.0.1 (c (n "sgx_trts") (v "1.0.1") (d (list (d (n "sgx_types") (r "= 1.0.1") (d #t) (k 0)))) (h "1prsi7km8hq71lxqf86a470zx37n2025q2ws0nq3z3sfrdvsw1sd") (f (quote (("default"))))))

(define-public crate-sgx_trts-1.0.4 (c (n "sgx_trts") (v "1.0.4") (d (list (d (n "sgx_types") (r "= 1.0.4") (d #t) (k 0)))) (h "05nv0jf18fyxnlxfqfjxmcj3vm4jlwvrh8pcmzwhdj6yq8a3daj8") (f (quote (("default"))))))

(define-public crate-sgx_trts-1.0.5 (c (n "sgx_trts") (v "1.0.5") (d (list (d (n "sgx_libc") (r "= 1.0.5") (d #t) (k 0)) (d (n "sgx_types") (r "= 1.0.5") (d #t) (k 0)))) (h "0z3k9h3z9ydx23b5s524d5jxs6vnb9sqgn0snzgbh8nkpw7j7q06") (f (quote (("default"))))))

(define-public crate-sgx_trts-1.0.5-fix (c (n "sgx_trts") (v "1.0.5-fix") (d (list (d (n "sgx_libc") (r "= 1.0.5-fix") (d #t) (k 0)) (d (n "sgx_types") (r "= 1.0.5") (d #t) (k 0)))) (h "1mc0g0219l001kj4g2ljcyz0l2yksppd3cgrq70n5fgg1ibxy2cg") (f (quote (("default"))))))

(define-public crate-sgx_trts-1.0.6 (c (n "sgx_trts") (v "1.0.6") (d (list (d (n "sgx_libc") (r "= 1.0.6") (d #t) (k 0)) (d (n "sgx_types") (r "= 1.0.6") (d #t) (k 0)))) (h "0bnbg79r4hwjz10ixgaxfqbhmnvcwf9w5d828zr8lbzajv4kkrbz") (f (quote (("default"))))))

(define-public crate-sgx_trts-1.0.7 (c (n "sgx_trts") (v "1.0.7") (d (list (d (n "sgx_libc") (r "= 1.0.7") (d #t) (k 0)) (d (n "sgx_types") (r "= 1.0.7") (d #t) (k 0)))) (h "1aalzia5dwx3fj08zzplvifn6dvq82rjqakd608lhi5flp56p73k") (f (quote (("default"))))))

(define-public crate-sgx_trts-1.0.8 (c (n "sgx_trts") (v "1.0.8") (d (list (d (n "sgx_libc") (r "= 1.0.8") (d #t) (t "cfg(not(target_env = \"sgx\"))") (k 0)) (d (n "sgx_types") (r "= 1.0.8") (d #t) (t "cfg(not(target_env = \"sgx\"))") (k 0)))) (h "0c1cx2pvx03fmlzn8rdshl9dg0qc1v3xbg3bcwwamz4srjcjpgji") (f (quote (("default"))))))

(define-public crate-sgx_trts-1.0.9 (c (n "sgx_trts") (v "1.0.9") (d (list (d (n "sgx_libc") (r "= 1.0.9") (d #t) (t "cfg(not(target_env = \"sgx\"))") (k 0)) (d (n "sgx_types") (r "= 1.0.9") (d #t) (t "cfg(not(target_env = \"sgx\"))") (k 0)))) (h "0mwbzizinrayahb9xwyj87kj3ak68sn81dkd7pcxvbn55i6ilmgp") (f (quote (("default"))))))

(define-public crate-sgx_trts-1.1.0 (c (n "sgx_trts") (v "1.1.0") (d (list (d (n "sgx_libc") (r "= 1.1.0") (d #t) (t "cfg(not(target_env = \"sgx\"))") (k 0)) (d (n "sgx_types") (r "= 1.1.0") (d #t) (t "cfg(not(target_env = \"sgx\"))") (k 0)))) (h "1xzjszb6hmq65snmczf99ix0p7870lphbx9kxz11gd4g6gw606ah") (f (quote (("default"))))))

(define-public crate-sgx_trts-1.1.1 (c (n "sgx_trts") (v "1.1.1") (d (list (d (n "sgx_libc") (r "= 1.1.1") (d #t) (t "cfg(not(target_env = \"sgx\"))") (k 0)) (d (n "sgx_types") (r "= 1.1.1") (d #t) (t "cfg(not(target_env = \"sgx\"))") (k 0)))) (h "1g4l1ax4lsya7i3y4nc2kdhrm6m2wkf5fg6qq3wm20fcnx7siqv3") (f (quote (("default"))))))

