(define-module (crates-io sg x_ sgx_serialize_derive) #:use-module (crates-io))

(define-public crate-sgx_serialize_derive-0.9.8 (c (n "sgx_serialize_derive") (v "0.9.8") (d (list (d (n "quote") (r "^0.3.8") (d #t) (k 0)) (d (n "sgx_serialize_derive_internals") (r "^0.9.8") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "031js8v66d8s1wcv4vxddikyp6fx3sqn23gqx38xs4xjpwr3w398")))

(define-public crate-sgx_serialize_derive-1.0.0 (c (n "sgx_serialize_derive") (v "1.0.0") (d (list (d (n "quote") (r "^0.3.8") (d #t) (k 0)) (d (n "sgx_serialize_derive_internals") (r "= 1.0.0") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "13bgfpwwk6i6402y4n7956qzm7vf98hjh0x2dafqc1kf9a4l75cm")))

(define-public crate-sgx_serialize_derive-1.0.1 (c (n "sgx_serialize_derive") (v "1.0.1") (d (list (d (n "quote") (r "^0.3.8") (d #t) (k 0)) (d (n "sgx_serialize_derive_internals") (r "= 1.0.1") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "1bn6k9dqj4j139hscpcm84vp64a1clfai7d6igzwr6snas2kl0pm")))

(define-public crate-sgx_serialize_derive-1.0.4 (c (n "sgx_serialize_derive") (v "1.0.4") (d (list (d (n "quote") (r "^0.3.8") (d #t) (k 0)) (d (n "sgx_serialize_derive_internals") (r "= 1.0.4") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "0lgal9c467zzgm28fxbaff4hhkcx7xgnrw8pjablad3qqbrk8iir")))

(define-public crate-sgx_serialize_derive-1.0.5 (c (n "sgx_serialize_derive") (v "1.0.5") (d (list (d (n "quote") (r "^0.3.8") (d #t) (k 0)) (d (n "sgx_serialize_derive_internals") (r "= 1.0.5") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "1c35lkzz5ngfaqq4gnf7xbkp1ag4lln03px1xgpwv79va9rwi67r")))

(define-public crate-sgx_serialize_derive-1.0.6 (c (n "sgx_serialize_derive") (v "1.0.6") (d (list (d (n "quote") (r "^0.3.8") (d #t) (k 0)) (d (n "sgx_serialize_derive_internals") (r "= 1.0.6") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "09bkxx5yjmjg79ms6i009yklihdr1h6bvc27ajbm0xrmbgcbslz1")))

(define-public crate-sgx_serialize_derive-1.0.7 (c (n "sgx_serialize_derive") (v "1.0.7") (d (list (d (n "quote") (r "^0.3.8") (d #t) (k 0)) (d (n "sgx_serialize_derive_internals") (r "= 1.0.7") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "0n7sig13kn6dfm76vw12r6kwn8xcjfzf71w770d1hxn787mginl9")))

(define-public crate-sgx_serialize_derive-1.0.8 (c (n "sgx_serialize_derive") (v "1.0.8") (d (list (d (n "quote") (r "^0.3.8") (d #t) (k 0)) (d (n "sgx_serialize_derive_internals") (r "= 1.0.8") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "0rlz63r61f1kgdn4izggl4dag2w8fkg0c78sw19mwfnyks4sgbqk")))

(define-public crate-sgx_serialize_derive-1.0.9 (c (n "sgx_serialize_derive") (v "1.0.9") (d (list (d (n "quote") (r "^0.3.8") (d #t) (k 0)) (d (n "sgx_serialize_derive_internals") (r "= 1.0.9") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "1v6c55ksjdmfz5r9z1lrap9kc0jmkxnr8l0hkn44bh415123k8v2")))

(define-public crate-sgx_serialize_derive-1.1.0 (c (n "sgx_serialize_derive") (v "1.1.0") (d (list (d (n "quote") (r "^0.3.8") (d #t) (k 0)) (d (n "sgx_serialize_derive_internals") (r "= 1.1.0") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "0ida8mimih52n0c7rz36qg2nas0s9ryyk6w72whblxkispi1bap4")))

(define-public crate-sgx_serialize_derive-1.1.1 (c (n "sgx_serialize_derive") (v "1.1.1") (d (list (d (n "quote") (r "^0.3.8") (d #t) (k 0)) (d (n "sgx_serialize_derive_internals") (r "= 1.1.1") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "1prdsf6ddbp4mh7cmny4bq4bddzicgqs20a3w0jxsq8d8i4bc1a0")))

