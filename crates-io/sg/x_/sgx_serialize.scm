(define-module (crates-io sg x_ sgx_serialize) #:use-module (crates-io))

(define-public crate-sgx_serialize-0.9.8 (c (n "sgx_serialize") (v "0.9.8") (d (list (d (n "sgx_tstd") (r "^0.9.8") (d #t) (k 0)))) (h "0162g8704ymbi2mqy6j79pgymw28inw6kqfjbhyirzl42z1b69wn") (f (quote (("default"))))))

(define-public crate-sgx_serialize-1.0.0 (c (n "sgx_serialize") (v "1.0.0") (d (list (d (n "sgx_tstd") (r "= 1.0.0") (d #t) (k 0)))) (h "11l77958c655p1swfnznfm7kdnm88w9iffmbs3a6sswnhgxl3895") (f (quote (("default"))))))

(define-public crate-sgx_serialize-1.0.1 (c (n "sgx_serialize") (v "1.0.1") (d (list (d (n "sgx_tstd") (r "= 1.0.1") (d #t) (k 0)))) (h "1j722lnn72sm86hdz3w66ffvm2vml7qfdrjgp57bwsyln44bjjx6") (f (quote (("default"))))))

(define-public crate-sgx_serialize-1.0.4 (c (n "sgx_serialize") (v "1.0.4") (d (list (d (n "sgx_tstd") (r "= 1.0.4") (d #t) (k 0)))) (h "0yb83bhcr8wdfrybga4drraiblslb67vrj1zcm7mki6azik5c2q2") (f (quote (("default"))))))

(define-public crate-sgx_serialize-1.0.5 (c (n "sgx_serialize") (v "1.0.5") (d (list (d (n "sgx_tstd") (r "= 1.0.5") (d #t) (k 0)))) (h "1izfjsrfqzy7vib1bqnzncbhz3rx7lr37x424n9knpyi0naqfx4r") (f (quote (("default"))))))

(define-public crate-sgx_serialize-1.0.6 (c (n "sgx_serialize") (v "1.0.6") (d (list (d (n "sgx_tstd") (r "= 1.0.6") (d #t) (k 0)))) (h "1b3mlcnizy8w61gyvd0f81s6rlrapry74g3rq75l5vwzifb14n88") (f (quote (("default"))))))

(define-public crate-sgx_serialize-1.0.7 (c (n "sgx_serialize") (v "1.0.7") (d (list (d (n "sgx_tstd") (r "= 1.0.7") (d #t) (k 0)))) (h "0h7dbb29f0dvkhlljma6xzim8dlfmi27whwlq789jlgvlflllwh8") (f (quote (("default"))))))

(define-public crate-sgx_serialize-1.0.8 (c (n "sgx_serialize") (v "1.0.8") (d (list (d (n "sgx_tstd") (r "= 1.0.8") (d #t) (t "cfg(not(target_env = \"sgx\"))") (k 0)))) (h "1af79v9vifinh4qibl53l13qqhyn7vmrbhqv2qlhiamz2n0ymxlm") (f (quote (("default"))))))

(define-public crate-sgx_serialize-1.0.9 (c (n "sgx_serialize") (v "1.0.9") (d (list (d (n "sgx_tstd") (r "= 1.0.9") (d #t) (t "cfg(not(target_env = \"sgx\"))") (k 0)))) (h "0xmcp4329l0vkkfz57a6ipm1q5xd0x46izxbdmm7fmlr57fbxj29") (f (quote (("default"))))))

(define-public crate-sgx_serialize-1.1.0 (c (n "sgx_serialize") (v "1.1.0") (d (list (d (n "sgx_tstd") (r "= 1.1.0") (d #t) (t "cfg(not(target_env = \"sgx\"))") (k 0)))) (h "01kgvawkrr328i298j52nmzry4pvipr3yryrhwfmw4d81xdlq2qj") (f (quote (("default"))))))

(define-public crate-sgx_serialize-1.1.1 (c (n "sgx_serialize") (v "1.1.1") (d (list (d (n "sgx_tstd") (r "= 1.1.1") (d #t) (t "cfg(not(target_env = \"sgx\"))") (k 0)))) (h "0rs2q9fb4k8zwp8lm3wfj083bqx2wibd2hk4c5yx5x2szrps7lqi") (f (quote (("default"))))))

