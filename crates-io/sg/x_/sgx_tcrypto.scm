(define-module (crates-io sg x_ sgx_tcrypto) #:use-module (crates-io))

(define-public crate-sgx_tcrypto-0.9.8 (c (n "sgx_tcrypto") (v "0.9.8") (d (list (d (n "sgx_types") (r "^0.9.8") (d #t) (k 0)))) (h "119xagyzkl6n8fh7pbs6jq73abzvh7knq0wpvgbk9m7zrcq39cvz") (f (quote (("default"))))))

(define-public crate-sgx_tcrypto-1.0.0 (c (n "sgx_tcrypto") (v "1.0.0") (d (list (d (n "sgx_types") (r "= 1.0.0") (d #t) (k 0)))) (h "0jk4apydy9gbd87i7qp8i1k8qnbiqk02c101qc05xy9s8x9fflpv") (f (quote (("default"))))))

(define-public crate-sgx_tcrypto-1.0.1 (c (n "sgx_tcrypto") (v "1.0.1") (d (list (d (n "sgx_types") (r "= 1.0.1") (d #t) (k 0)))) (h "021lha86pq8cjn4xcd2sh4difl0sbl1yxjqllfgb02668hxx0xas") (f (quote (("default"))))))

(define-public crate-sgx_tcrypto-1.0.4 (c (n "sgx_tcrypto") (v "1.0.4") (d (list (d (n "sgx_types") (r "= 1.0.4") (d #t) (k 0)))) (h "1mkw9dmn90xrz9hcj3k6b2r8xkn8kq9ak10z11pbnqp7gcidcgg2") (f (quote (("default"))))))

(define-public crate-sgx_tcrypto-1.0.5 (c (n "sgx_tcrypto") (v "1.0.5") (d (list (d (n "sgx_types") (r "= 1.0.5") (d #t) (k 0)))) (h "1s8dvqm9dz729zpfzxpc002xh7byx4gs7yagcf1pg6f8i54rw3hn") (f (quote (("default"))))))

(define-public crate-sgx_tcrypto-1.0.6 (c (n "sgx_tcrypto") (v "1.0.6") (d (list (d (n "sgx_types") (r "= 1.0.6") (d #t) (k 0)))) (h "0v8ihfkh1wsisj5gwmj49y8q7bk4lcq0g9pf7khkx2arxzj5dii0") (f (quote (("default"))))))

(define-public crate-sgx_tcrypto-1.0.7 (c (n "sgx_tcrypto") (v "1.0.7") (d (list (d (n "sgx_types") (r "= 1.0.7") (d #t) (k 0)))) (h "1qc29law4hjy511zifph1fzrmcyzcrvj5hbkphnp01x3kms1fmlg") (f (quote (("default"))))))

(define-public crate-sgx_tcrypto-1.0.8 (c (n "sgx_tcrypto") (v "1.0.8") (d (list (d (n "sgx_types") (r "= 1.0.8") (d #t) (t "cfg(not(target_env = \"sgx\"))") (k 0)))) (h "0ndq3w628zl407pcgdbfh81sl3qhx7xrq48hqdwcblcrz5zx4651") (f (quote (("default"))))))

(define-public crate-sgx_tcrypto-1.0.9 (c (n "sgx_tcrypto") (v "1.0.9") (d (list (d (n "sgx_types") (r "= 1.0.9") (d #t) (t "cfg(not(target_env = \"sgx\"))") (k 0)))) (h "1228k3336yvzm1g97x4hxaa0xbbap5bar3p8n6iac47ihx1xl2py") (f (quote (("default"))))))

(define-public crate-sgx_tcrypto-1.1.0 (c (n "sgx_tcrypto") (v "1.1.0") (d (list (d (n "sgx_types") (r "= 1.1.0") (d #t) (t "cfg(not(target_env = \"sgx\"))") (k 0)))) (h "1ipgzkli877rhbrjxbwsrh1ww935jhm7ady6d1dkb5l3yk752yzy") (f (quote (("default"))))))

(define-public crate-sgx_tcrypto-1.1.1 (c (n "sgx_tcrypto") (v "1.1.1") (d (list (d (n "sgx_types") (r "= 1.1.1") (d #t) (t "cfg(not(target_env = \"sgx\"))") (k 0)))) (h "1biv1g8m9f22pwpivpjr3alkn87iksmwcycq9qzw24caq8h53dzs") (f (quote (("default"))))))

