(define-module (crates-io sg x_ sgx_tkey_exchange) #:use-module (crates-io))

(define-public crate-sgx_tkey_exchange-0.9.8 (c (n "sgx_tkey_exchange") (v "0.9.8") (d (list (d (n "sgx_types") (r "^0.9.8") (d #t) (k 0)))) (h "0nam63zhi4k633xyng9vrvn1qqvfk2p7qykhqkb47yp2f00c9f51") (f (quote (("default"))))))

(define-public crate-sgx_tkey_exchange-1.0.0 (c (n "sgx_tkey_exchange") (v "1.0.0") (d (list (d (n "sgx_types") (r "= 1.0.0") (d #t) (k 0)))) (h "1s8q49sjb4y4qwbnvxa20611lcxwhp0avkdlapw53xfxn8rlys47") (f (quote (("default"))))))

(define-public crate-sgx_tkey_exchange-1.0.1 (c (n "sgx_tkey_exchange") (v "1.0.1") (d (list (d (n "sgx_types") (r "= 1.0.1") (d #t) (k 0)))) (h "1m5qhdbr2nvdxvmv62nbklmyg6w3vykciqdp040nimvm4d6wf2rd") (f (quote (("default"))))))

(define-public crate-sgx_tkey_exchange-1.0.4 (c (n "sgx_tkey_exchange") (v "1.0.4") (d (list (d (n "sgx_types") (r "= 1.0.4") (d #t) (k 0)))) (h "17fzmir5kqr6zv9l8fk82x2vb5rp6527a989hjw1h12k3mx0z1ff") (f (quote (("default"))))))

(define-public crate-sgx_tkey_exchange-1.0.5 (c (n "sgx_tkey_exchange") (v "1.0.5") (d (list (d (n "sgx_types") (r "= 1.0.5") (d #t) (k 0)))) (h "1j55mwz829x158yn1wjxkfai8dkv3fcd43mdqfwsa4nnc5241qnn") (f (quote (("default"))))))

(define-public crate-sgx_tkey_exchange-1.0.6 (c (n "sgx_tkey_exchange") (v "1.0.6") (d (list (d (n "sgx_types") (r "= 1.0.6") (d #t) (k 0)))) (h "1cjkf8iqyjfwb4bnlcaksxs6l3a71avynavqc4bhgydc0f78hyfg") (f (quote (("default"))))))

(define-public crate-sgx_tkey_exchange-1.0.7 (c (n "sgx_tkey_exchange") (v "1.0.7") (d (list (d (n "sgx_types") (r "= 1.0.7") (d #t) (k 0)))) (h "08mb4ahqxv2mpmam0lx0md34zfnnl9vrlfi41g2gm3m2bswzkqqk") (f (quote (("default"))))))

(define-public crate-sgx_tkey_exchange-1.0.8 (c (n "sgx_tkey_exchange") (v "1.0.8") (d (list (d (n "sgx_types") (r "= 1.0.8") (d #t) (t "cfg(not(target_env = \"sgx\"))") (k 0)))) (h "01b8y0q5pdcp9wa2cxhgd6z61g79xrbiig88qpfaaf2f847gkqhz") (f (quote (("default"))))))

(define-public crate-sgx_tkey_exchange-1.0.9 (c (n "sgx_tkey_exchange") (v "1.0.9") (d (list (d (n "sgx_types") (r "= 1.0.9") (d #t) (t "cfg(not(target_env = \"sgx\"))") (k 0)))) (h "1kywrmmajx5d6sazjdwxd6mmxijq63ns375nxf6l7s7nsxdcgbmc") (f (quote (("default"))))))

(define-public crate-sgx_tkey_exchange-1.1.0 (c (n "sgx_tkey_exchange") (v "1.1.0") (d (list (d (n "sgx_types") (r "= 1.1.0") (d #t) (t "cfg(not(target_env = \"sgx\"))") (k 0)))) (h "0pbhz9dbkrwm2vjadg2k1x69gz2mzgh1qgrlvbb9vpyyr644h9h8") (f (quote (("default"))))))

(define-public crate-sgx_tkey_exchange-1.1.1 (c (n "sgx_tkey_exchange") (v "1.1.1") (d (list (d (n "sgx_types") (r "= 1.1.1") (d #t) (t "cfg(not(target_env = \"sgx\"))") (k 0)))) (h "0lmnz4ssli2k20mk05zg9qi1rvip3z32jgqgvjcv2lwym7c6vszw") (f (quote (("default"))))))

