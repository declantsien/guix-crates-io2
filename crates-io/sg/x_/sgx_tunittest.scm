(define-module (crates-io sg x_ sgx_tunittest) #:use-module (crates-io))

(define-public crate-sgx_tunittest-0.9.8 (c (n "sgx_tunittest") (v "0.9.8") (d (list (d (n "sgx_tstd") (r "^0.9.8") (d #t) (k 0)))) (h "0f8jcqlgwbm6hyrnmi0vwrib2g5xbpfzqjlcqlcl6dc9pkqibaan") (f (quote (("default"))))))

(define-public crate-sgx_tunittest-1.0.0 (c (n "sgx_tunittest") (v "1.0.0") (d (list (d (n "sgx_tstd") (r "= 1.0.0") (d #t) (k 0)))) (h "1y542xays7x58pn1zsa59b75l2fpq7aly9iwx7pzc4ir6qv3nfj7") (f (quote (("default"))))))

(define-public crate-sgx_tunittest-1.0.1 (c (n "sgx_tunittest") (v "1.0.1") (d (list (d (n "sgx_tstd") (r "= 1.0.1") (d #t) (k 0)))) (h "0rnpd44j88x4bnl46w5f76dw89h059gsrhyb3d2xvjpccwy3xg3z") (f (quote (("default"))))))

(define-public crate-sgx_tunittest-1.0.4 (c (n "sgx_tunittest") (v "1.0.4") (d (list (d (n "sgx_tstd") (r "= 1.0.4") (d #t) (k 0)))) (h "1ripis1bbnzvj0bkvwdvcgbhm9w94rbnn15rfjdn09apjvqzdnqq") (f (quote (("default"))))))

(define-public crate-sgx_tunittest-1.0.5 (c (n "sgx_tunittest") (v "1.0.5") (d (list (d (n "sgx_tstd") (r "= 1.0.5") (d #t) (k 0)))) (h "1r47vjggqfy0vrzpz01z1ahyn5hkf0ab9a0svihgnkfcc57d00gn") (f (quote (("default"))))))

(define-public crate-sgx_tunittest-1.0.5-fix (c (n "sgx_tunittest") (v "1.0.5-fix") (d (list (d (n "sgx_tstd") (r "= 1.0.5") (d #t) (k 0)))) (h "156kqswhsjgchj100l1dgyl1k9cl1zvchhlqqzdgfnqij3g3bmml") (f (quote (("default"))))))

(define-public crate-sgx_tunittest-1.0.6 (c (n "sgx_tunittest") (v "1.0.6") (d (list (d (n "sgx_tstd") (r "= 1.0.6") (d #t) (k 0)))) (h "0000pcxrljp3jz2kgc2hizli3234zx9a7jfk9mp2xcv94wiv7b0g") (f (quote (("default"))))))

(define-public crate-sgx_tunittest-1.0.7 (c (n "sgx_tunittest") (v "1.0.7") (d (list (d (n "sgx_tstd") (r "= 1.0.7") (d #t) (k 0)))) (h "1hg884s9w0s5lri4b600vyx15q21njaf7brqp2w2jvbxbbn7fqga") (f (quote (("default"))))))

(define-public crate-sgx_tunittest-1.0.8 (c (n "sgx_tunittest") (v "1.0.8") (d (list (d (n "sgx_tstd") (r "= 1.0.8") (d #t) (t "cfg(not(target_env = \"sgx\"))") (k 0)))) (h "1ddw3p5jb83xgca7dmxi0rhnflddjnp6n68cz008l5xxcbncafz4") (f (quote (("default"))))))

(define-public crate-sgx_tunittest-1.0.9 (c (n "sgx_tunittest") (v "1.0.9") (d (list (d (n "sgx_tstd") (r "= 1.0.9") (d #t) (t "cfg(not(target_env = \"sgx\"))") (k 0)))) (h "0sgr3f2sf2zgaglibl9fj6y8h7mwq9k2pswh8w587khbx07b9ndk") (f (quote (("default"))))))

(define-public crate-sgx_tunittest-1.1.0 (c (n "sgx_tunittest") (v "1.1.0") (d (list (d (n "sgx_tstd") (r "= 1.1.0") (d #t) (t "cfg(not(target_env = \"sgx\"))") (k 0)))) (h "1gpnc9pk4y2bnhl04cfhy26d7d19487zyj226jmyz7bg16bkqzq5") (f (quote (("default"))))))

(define-public crate-sgx_tunittest-1.1.1 (c (n "sgx_tunittest") (v "1.1.1") (d (list (d (n "sgx_tstd") (r "= 1.1.1") (d #t) (t "cfg(not(target_env = \"sgx\"))") (k 0)))) (h "1fz02ixi0jqa9nnk7iix5h8ka4rg21piw8i0ny6vlx136zsm77xq") (f (quote (("default"))))))

