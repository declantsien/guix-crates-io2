(define-module (crates-io sg x_ sgx_pck_extension) #:use-module (crates-io))

(define-public crate-sgx_pck_extension-0.1.0 (c (n "sgx_pck_extension") (v "0.1.0") (d (list (d (n "asn1") (r "^0.15") (d #t) (k 0)) (d (n "asn1-rs") (r "^0.5") (d #t) (k 0)) (d (n "x509-parser") (r "^0.15") (d #t) (k 0)))) (h "1xssham7q6zc6r8bf113qnardy0rxg49mzgrbzj7y6qakdaq4wrs")))

(define-public crate-sgx_pck_extension-0.1.1 (c (n "sgx_pck_extension") (v "0.1.1") (d (list (d (n "asn1") (r "^0.15") (d #t) (k 0)) (d (n "asn1-rs") (r "^0.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "x509-parser") (r "^0.15") (d #t) (k 0)))) (h "0bv7h43vzxs4jd6zr9p5wrgxwxsnj4ccfv89smc8iwdz7vvj5adq")))

