(define-module (crates-io sg x_ sgx_tprotected_fs) #:use-module (crates-io))

(define-public crate-sgx_tprotected_fs-0.9.8 (c (n "sgx_tprotected_fs") (v "0.9.8") (d (list (d (n "sgx_trts") (r "^0.9.8") (d #t) (k 0)) (d (n "sgx_types") (r "^0.9.8") (d #t) (k 0)))) (h "1zx1zcd2sdbk3m1np0z7xlic9ydhjh7sf6iqa50qjlfsyhzzzi81") (f (quote (("default"))))))

(define-public crate-sgx_tprotected_fs-1.0.0 (c (n "sgx_tprotected_fs") (v "1.0.0") (d (list (d (n "sgx_trts") (r "= 1.0.0") (d #t) (k 0)) (d (n "sgx_types") (r "= 1.0.0") (d #t) (k 0)))) (h "0ws9bsc9r9nr2i0n8g64dfh5qrzj9arrq59zgfgslv0sqh4j9xzq") (f (quote (("default"))))))

(define-public crate-sgx_tprotected_fs-1.0.1 (c (n "sgx_tprotected_fs") (v "1.0.1") (d (list (d (n "sgx_trts") (r "= 1.0.1") (d #t) (k 0)) (d (n "sgx_types") (r "= 1.0.1") (d #t) (k 0)))) (h "0aqhj72i5fpk23bvd4jbzv360qjdzpvvm7l7v5clfgv36gsnsyrf") (f (quote (("default"))))))

(define-public crate-sgx_tprotected_fs-1.0.4 (c (n "sgx_tprotected_fs") (v "1.0.4") (d (list (d (n "sgx_trts") (r "= 1.0.4") (d #t) (k 0)) (d (n "sgx_types") (r "= 1.0.4") (d #t) (k 0)))) (h "17yqvvjn6mg2z81ym5fa7bazmcc56qsis754vi1ndyjnqja97zwg") (f (quote (("default"))))))

(define-public crate-sgx_tprotected_fs-1.0.5 (c (n "sgx_tprotected_fs") (v "1.0.5") (d (list (d (n "sgx_trts") (r "= 1.0.5") (d #t) (k 0)) (d (n "sgx_types") (r "= 1.0.5") (d #t) (k 0)))) (h "1bflflp2c1n450g9kjz6q1rsymk27cavs85k6k3w8brndml8sykl") (f (quote (("default"))))))

(define-public crate-sgx_tprotected_fs-1.0.6 (c (n "sgx_tprotected_fs") (v "1.0.6") (d (list (d (n "sgx_trts") (r "= 1.0.6") (d #t) (k 0)) (d (n "sgx_types") (r "= 1.0.6") (d #t) (k 0)))) (h "13kv2npic15ddm6wv9dnhwvh4zqj51r9byc924pfng1mx2hw5kjc") (f (quote (("default"))))))

(define-public crate-sgx_tprotected_fs-1.0.7 (c (n "sgx_tprotected_fs") (v "1.0.7") (d (list (d (n "sgx_trts") (r "= 1.0.7") (d #t) (k 0)) (d (n "sgx_types") (r "= 1.0.7") (d #t) (k 0)))) (h "1dnilliqnzap5l6ma3sjjdhjgcb76w64zmj8lcxxjndrwm6nc07j") (f (quote (("default"))))))

(define-public crate-sgx_tprotected_fs-1.0.8 (c (n "sgx_tprotected_fs") (v "1.0.8") (d (list (d (n "sgx_trts") (r "= 1.0.8") (d #t) (t "cfg(not(target_env = \"sgx\"))") (k 0)) (d (n "sgx_types") (r "= 1.0.8") (d #t) (t "cfg(not(target_env = \"sgx\"))") (k 0)))) (h "13xv6s8adz865r4a7gjrvg0dv2ii16b9mr6z6qqbqzigsmi9r1ar") (f (quote (("default"))))))

(define-public crate-sgx_tprotected_fs-1.0.9 (c (n "sgx_tprotected_fs") (v "1.0.9") (d (list (d (n "sgx_trts") (r "= 1.0.9") (d #t) (t "cfg(not(target_env = \"sgx\"))") (k 0)) (d (n "sgx_types") (r "= 1.0.9") (d #t) (t "cfg(not(target_env = \"sgx\"))") (k 0)))) (h "1c6wa1kdyr0acdykp4g2wnfys1mk2wis6a3pcrryppi960756qmc") (f (quote (("default"))))))

(define-public crate-sgx_tprotected_fs-1.1.0 (c (n "sgx_tprotected_fs") (v "1.1.0") (d (list (d (n "sgx_trts") (r "= 1.1.0") (d #t) (t "cfg(not(target_env = \"sgx\"))") (k 0)) (d (n "sgx_types") (r "= 1.1.0") (d #t) (t "cfg(not(target_env = \"sgx\"))") (k 0)))) (h "1764yl59lxlh0zyrhxqlj3sd1hlzgkdy1hqiy264kj3injpq4baf") (f (quote (("default"))))))

(define-public crate-sgx_tprotected_fs-1.1.1 (c (n "sgx_tprotected_fs") (v "1.1.1") (d (list (d (n "sgx_trts") (r "= 1.1.1") (d #t) (t "cfg(not(target_env = \"sgx\"))") (k 0)) (d (n "sgx_types") (r "= 1.1.1") (d #t) (t "cfg(not(target_env = \"sgx\"))") (k 0)))) (h "039h05vy4cyk78qxpjha0f2zb16wzwfcrrd5ispikmfsh3qj55b3") (f (quote (("default"))))))

