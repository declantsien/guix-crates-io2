(define-module (crates-io sg x_ sgx_types) #:use-module (crates-io))

(define-public crate-sgx_types-0.9.7 (c (n "sgx_types") (v "0.9.7") (h "1016xqn12wdblwpdcjbkql01p0zn2ckw0hx6n9jcpa1daaw9fxxb") (f (quote (("default"))))))

(define-public crate-sgx_types-0.9.8 (c (n "sgx_types") (v "0.9.8") (h "1a91pgvg18q4w3gfxm3sv67ngaxwg1nvkymdlf0hmryd85zj3sfs") (f (quote (("default"))))))

(define-public crate-sgx_types-1.0.0 (c (n "sgx_types") (v "1.0.0") (h "0hfdgpcvj1l4r7mmx7mvivg96h6azvz9y9ya9fqhsriinx46cv45") (f (quote (("default"))))))

(define-public crate-sgx_types-1.0.1 (c (n "sgx_types") (v "1.0.1") (h "1svwsxgd99qp9b56isp65r7qsv47vwv7jcqawmqw2fvfc8fhw0bd") (f (quote (("default"))))))

(define-public crate-sgx_types-1.0.4 (c (n "sgx_types") (v "1.0.4") (h "0ggdja89cdz0yw2d21vqkzhihks1xdnmbhhdfn3nlhwsv3yx2klv") (f (quote (("default"))))))

(define-public crate-sgx_types-1.0.5 (c (n "sgx_types") (v "1.0.5") (h "1zcdvfbgan88mv3ky7jvkhm0yfp9mh30jxdzhc305y4b0ymxdfsg") (f (quote (("default"))))))

(define-public crate-sgx_types-1.0.5-fix (c (n "sgx_types") (v "1.0.5-fix") (h "1iscnman2qzlkq83h66c9pby1g8vr2ar2g3csjvpw6xqhcnfhwih") (f (quote (("default"))))))

(define-public crate-sgx_types-1.0.6 (c (n "sgx_types") (v "1.0.6") (h "0cblzkbj1lm1npzn380px4lg1x8x04vcjgbk1i66q6lnfgjgmd29") (f (quote (("default"))))))

(define-public crate-sgx_types-1.0.7 (c (n "sgx_types") (v "1.0.7") (h "0ddyp5k1mi8m3i26mfwha3mgv3mhxziwgmgj8r5n1l227jzhz9iz") (f (quote (("default"))))))

(define-public crate-sgx_types-1.0.8 (c (n "sgx_types") (v "1.0.8") (h "0s6lfd1nmbw1ljnh3pi2jfxvh633r2d9r6vm8w06cf06scycv84h") (f (quote (("default"))))))

(define-public crate-sgx_types-1.0.9 (c (n "sgx_types") (v "1.0.9") (h "044w34l5pcyk23srw6lhiiqm4zgbv0cm4gbnl3564gw00x370b7f") (f (quote (("default"))))))

(define-public crate-sgx_types-1.1.0 (c (n "sgx_types") (v "1.1.0") (h "18spkn16vwi7r2x499m8vs4is0sv169ki9sskm5pmr5rc0dwvfw3") (f (quote (("default"))))))

(define-public crate-sgx_types-1.1.1 (c (n "sgx_types") (v "1.1.1") (h "0qnk17z4d24wdixh7jslvjk711w7d2222f97g1j2jsjx8lkr2cnz") (f (quote (("default"))))))

(define-public crate-sgx_types-1.1.2 (c (n "sgx_types") (v "1.1.2") (h "0p14b1s42d01v86y5ka0ja1p3j6jldnfwz96y4bmx3v8rjdph96i") (f (quote (("default"))))))

