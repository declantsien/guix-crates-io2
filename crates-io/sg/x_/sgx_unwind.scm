(define-module (crates-io sg x_ sgx_unwind) #:use-module (crates-io))

(define-public crate-sgx_unwind-0.0.0 (c (n "sgx_unwind") (v "0.0.0") (d (list (d (n "sgx_trts") (r "^0.9.8") (d #t) (k 0)))) (h "092scvbfbqf79kgr8fckh48s8vavf5pm99j1g1f3xlcsc5c7351n")))

(define-public crate-sgx_unwind-0.0.1 (c (n "sgx_unwind") (v "0.0.1") (d (list (d (n "sgx_trts") (r "= 1.0.0") (d #t) (k 0)))) (h "0xlmkgkpik3r4cvw8hv2v1zpcc9rjfkqj9a1qz73cwigdicryw30")))

(define-public crate-sgx_unwind-0.0.2 (c (n "sgx_unwind") (v "0.0.2") (d (list (d (n "sgx_trts") (r "^1") (d #t) (k 0)))) (h "0hmzvaz6hsjbn6b0jn3y5d6bsc6bi930k6x3m5x02ydmdcdz7700")))

(define-public crate-sgx_unwind-0.0.3 (c (n "sgx_unwind") (v "0.0.3") (d (list (d (n "sgx_trts") (r "^1.0") (d #t) (k 0)))) (h "1yl9a7vlznqy3qxsfvih24090g7wf1mf5lxk5qr8xiqd38f6w402")))

(define-public crate-sgx_unwind-0.0.4 (c (n "sgx_unwind") (v "0.0.4") (d (list (d (n "sgx_build_helper") (r "^0.1.1") (d #t) (k 1)) (d (n "sgx_trts") (r "= 1.0.9") (d #t) (t "cfg(not(target_env = \"sgx\"))") (k 0)))) (h "0c32ryk3i2lc5yxbn3i5in10xxkl6c0rpznihaj9z6i6d9zz4swf")))

(define-public crate-sgx_unwind-0.1.0 (c (n "sgx_unwind") (v "0.1.0") (d (list (d (n "sgx_build_helper") (r "^0.1") (d #t) (k 1)))) (h "1lwnlpyzb35bqzv74dp2k63xishmhwszm02hlpz1lgv07vci1d3k")))

(define-public crate-sgx_unwind-0.1.1 (c (n "sgx_unwind") (v "0.1.1") (d (list (d (n "sgx_build_helper") (r "^0.1.3") (d #t) (k 1)))) (h "00xjsgv52naqkr8d0x561bfcg31xkmnzq8h7cwkj1i5kba2v17c7")))

