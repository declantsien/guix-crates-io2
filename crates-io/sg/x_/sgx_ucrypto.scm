(define-module (crates-io sg x_ sgx_ucrypto) #:use-module (crates-io))

(define-public crate-sgx_ucrypto-1.0.5 (c (n "sgx_ucrypto") (v "1.0.5") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rand_core") (r "^0.3") (d #t) (k 0)) (d (n "rdrand") (r "^0.4") (d #t) (k 0)) (d (n "sgx_types") (r "= 1.0.5") (d #t) (k 0)))) (h "1kpw7gnpdqb4w741l8fjb4agb3v9gxlclalw56dizc2fhrnwff00") (f (quote (("default"))))))

(define-public crate-sgx_ucrypto-1.0.6 (c (n "sgx_ucrypto") (v "1.0.6") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rand_core") (r "^0.3") (d #t) (k 0)) (d (n "rdrand") (r "^0.4") (d #t) (k 0)) (d (n "sgx_types") (r "= 1.0.6") (d #t) (k 0)))) (h "18qg02s6cxa40p5a5200hqyrdyf1nyxq29v198n33102vi2k5f15") (f (quote (("default"))))))

(define-public crate-sgx_ucrypto-1.0.7 (c (n "sgx_ucrypto") (v "1.0.7") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rand_core") (r "^0.3") (d #t) (k 0)) (d (n "rdrand") (r "^0.4") (d #t) (k 0)) (d (n "sgx_types") (r "= 1.0.7") (d #t) (k 0)))) (h "0vlrcfvx0hbbzqhn0wys3brpqvdqlnamqw2wxb0n7znv4vl9da48") (f (quote (("default"))))))

(define-public crate-sgx_ucrypto-1.0.8 (c (n "sgx_ucrypto") (v "1.0.8") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rand_core") (r "^0.3") (d #t) (k 0)) (d (n "rdrand") (r "^0.4") (d #t) (k 0)) (d (n "sgx_types") (r "= 1.0.8") (d #t) (k 0)))) (h "1kjxdp57mj6g8xp891za8vj6nvq26wj1a1zi70i032m1qlsnl80g") (f (quote (("default"))))))

(define-public crate-sgx_ucrypto-1.0.9 (c (n "sgx_ucrypto") (v "1.0.9") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rand_core") (r "^0.3") (d #t) (k 0)) (d (n "rdrand") (r "^0.6") (d #t) (k 0)) (d (n "sgx_types") (r "= 1.0.9") (d #t) (k 0)))) (h "0z7kcc7a4nxy8vwwhqxxjzx9brz7p4pmgfp42b4k2z68z1q90a9m") (f (quote (("default"))))))

(define-public crate-sgx_ucrypto-1.1.0 (c (n "sgx_ucrypto") (v "1.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rand_core") (r "^0.3") (d #t) (k 0)) (d (n "rdrand") (r "^0.6") (d #t) (k 0)) (d (n "sgx_types") (r "= 1.1.0") (d #t) (k 0)))) (h "19laa5z05al3qzd5y87nggwh55ds1mpk0yhw99a7blgjc6swyyjh") (f (quote (("default"))))))

(define-public crate-sgx_ucrypto-1.1.1 (c (n "sgx_ucrypto") (v "1.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rand_core") (r "^0.3") (d #t) (k 0)) (d (n "rdrand") (r "^0.6") (d #t) (k 0)) (d (n "sgx_types") (r "= 1.1.1") (d #t) (k 0)))) (h "1w7viq1blm2h2f4wxxqq767mbyvbi0mda5hl28cpsfspaij9gyj5") (f (quote (("default"))))))

