(define-module (crates-io sg x_ sgx_tservice) #:use-module (crates-io))

(define-public crate-sgx_tservice-0.9.8 (c (n "sgx_tservice") (v "0.9.8") (d (list (d (n "sgx_types") (r "^0.9.8") (d #t) (k 0)))) (h "0h9c0si8rmdkcka5zqymb13906srm2083lm033kzg3sawlv3pm1c") (f (quote (("default"))))))

(define-public crate-sgx_tservice-1.0.0 (c (n "sgx_tservice") (v "1.0.0") (d (list (d (n "sgx_types") (r "= 1.0.0") (d #t) (k 0)))) (h "1jsb3p9f0gxqbim7f1vrv8pncq91v08808sz1b3qfwkvdaalisl8") (f (quote (("default"))))))

(define-public crate-sgx_tservice-1.0.1 (c (n "sgx_tservice") (v "1.0.1") (d (list (d (n "sgx_types") (r "= 1.0.1") (d #t) (k 0)))) (h "0bzxpl5jiz4kkxvfnfy53qgy08iibm5v2vpbr4g8504z90gr3w0z") (f (quote (("default"))))))

(define-public crate-sgx_tservice-1.0.4 (c (n "sgx_tservice") (v "1.0.4") (d (list (d (n "sgx_types") (r "= 1.0.4") (d #t) (k 0)))) (h "015p9cg6yan2r4v37xxxjcki4dxqjc698jhdsqhks5f2ymaiq9i6") (f (quote (("default"))))))

(define-public crate-sgx_tservice-1.0.5 (c (n "sgx_tservice") (v "1.0.5") (d (list (d (n "sgx_types") (r "= 1.0.5") (d #t) (k 0)))) (h "1cb9w01pl12b4awm3k96cvpwnc8lnz9067bsi8xasp3s6sqqz2vm") (f (quote (("default"))))))

(define-public crate-sgx_tservice-1.0.6 (c (n "sgx_tservice") (v "1.0.6") (d (list (d (n "sgx_types") (r "= 1.0.6") (d #t) (k 0)))) (h "1gyspcshbxbx3768mfc7gvfv2n9hdg3liq525dn3zyx66i82ykag") (f (quote (("default"))))))

(define-public crate-sgx_tservice-1.0.7 (c (n "sgx_tservice") (v "1.0.7") (d (list (d (n "sgx_types") (r "= 1.0.7") (d #t) (k 0)))) (h "0an07z33dk5hvksigs6kj70dylza7pr5vfm0i5ji2xskcxkk2vhj") (f (quote (("default"))))))

(define-public crate-sgx_tservice-1.0.8 (c (n "sgx_tservice") (v "1.0.8") (d (list (d (n "sgx_types") (r "= 1.0.8") (d #t) (t "cfg(not(target_env = \"sgx\"))") (k 0)))) (h "0p8545qjrss751p4qxmy5m5yhw94gzsq0jj1qxb3wcnn4d5vhhc9") (f (quote (("default"))))))

(define-public crate-sgx_tservice-1.0.9 (c (n "sgx_tservice") (v "1.0.9") (d (list (d (n "sgx_types") (r "= 1.0.9") (d #t) (t "cfg(not(target_env = \"sgx\"))") (k 0)))) (h "000604nvh4xgrjh8bqizri9x8afgz07z9qjkrq9jj8vgfcv5j04h") (f (quote (("default"))))))

(define-public crate-sgx_tservice-1.1.0 (c (n "sgx_tservice") (v "1.1.0") (d (list (d (n "sgx_types") (r "= 1.1.0") (d #t) (t "cfg(not(target_env = \"sgx\"))") (k 0)))) (h "1njdvvladphxdwh3853c0izj0lsgl526z6f8s16lqacyhndffv0m") (f (quote (("default"))))))

