(define-module (crates-io sg x_ sgx_rand_derive) #:use-module (crates-io))

(define-public crate-sgx_rand_derive-0.9.8 (c (n "sgx_rand_derive") (v "0.9.8") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "1wavfwv7wyngip4mv6ialbgxqdyq54z6pb6mi24b4pacnc4q1ry5")))

(define-public crate-sgx_rand_derive-1.0.0 (c (n "sgx_rand_derive") (v "1.0.0") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "1y7fyw4i353p1qbpghwgwg3qx2gk0bs742rchzbzx4mmawg20913")))

(define-public crate-sgx_rand_derive-1.0.1 (c (n "sgx_rand_derive") (v "1.0.1") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "0qhf0baih8kf5rxpbsl254yixad0nmsmxaawis97aqqrildyj3a8")))

(define-public crate-sgx_rand_derive-1.0.4 (c (n "sgx_rand_derive") (v "1.0.4") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "154733v4rfhc59jnisyn951p348jpa3w62a5zi8pbfrh7yx9kz3z")))

(define-public crate-sgx_rand_derive-1.0.5 (c (n "sgx_rand_derive") (v "1.0.5") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "03wbs2p3n4jnp72cwlvj6h7ddh03rxxbwigp4bk8cs8ps4j3ag20")))

(define-public crate-sgx_rand_derive-1.0.6 (c (n "sgx_rand_derive") (v "1.0.6") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "0bxjf7c59rb3giag5j2h7c65ddiwdjdv6n0nprhajp14fxjiz8md")))

(define-public crate-sgx_rand_derive-1.0.7 (c (n "sgx_rand_derive") (v "1.0.7") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "14m3gv351dzvzmx03kj2wqx7991300ijra37any5c268n3bd4yfh")))

(define-public crate-sgx_rand_derive-1.0.8 (c (n "sgx_rand_derive") (v "1.0.8") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "09f2sjmqng59jf5d7qkx7nsaw7pd985p99kjw3lhzjdmlw5vm28w")))

(define-public crate-sgx_rand_derive-1.0.9 (c (n "sgx_rand_derive") (v "1.0.9") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "1zs9n5fbh83kpvbk3vnxqbh4s1sqbbw89py32ynn9axpkziz1dha")))

(define-public crate-sgx_rand_derive-1.1.0 (c (n "sgx_rand_derive") (v "1.1.0") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "15a2fd60qp5zwq533dyi0s936lbki18faphz3qiv6ijif20lb04l")))

(define-public crate-sgx_rand_derive-1.1.1 (c (n "sgx_rand_derive") (v "1.1.1") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "0qr0dm539jnsw14r6ba2307y66nnhwinq46ailjc5nx8yiqnjn91")))

