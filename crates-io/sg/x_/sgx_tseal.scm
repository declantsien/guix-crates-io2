(define-module (crates-io sg x_ sgx_tseal) #:use-module (crates-io))

(define-public crate-sgx_tseal-0.9.8 (c (n "sgx_tseal") (v "0.9.8") (d (list (d (n "sgx_tcrypto") (r "^0.9.8") (d #t) (k 0)) (d (n "sgx_trts") (r "^0.9.8") (d #t) (k 0)) (d (n "sgx_tse") (r "^0.9.8") (d #t) (k 0)) (d (n "sgx_types") (r "^0.9.8") (d #t) (k 0)))) (h "07s9m6j7yrlkzsb80vqlrj6z93wcphv53fxc39fc4v7yi8xg2y3z") (f (quote (("default"))))))

(define-public crate-sgx_tseal-1.0.0 (c (n "sgx_tseal") (v "1.0.0") (d (list (d (n "sgx_tcrypto") (r "= 1.0.0") (d #t) (k 0)) (d (n "sgx_trts") (r "= 1.0.0") (d #t) (k 0)) (d (n "sgx_tse") (r "= 1.0.0") (d #t) (k 0)) (d (n "sgx_types") (r "= 1.0.0") (d #t) (k 0)))) (h "16q9zak85rnlgaqvkfk7w7sc77nlmlxz4v53039cn4j4qdnlsazs") (f (quote (("default"))))))

(define-public crate-sgx_tseal-1.0.1 (c (n "sgx_tseal") (v "1.0.1") (d (list (d (n "sgx_tcrypto") (r "= 1.0.1") (d #t) (k 0)) (d (n "sgx_trts") (r "= 1.0.1") (d #t) (k 0)) (d (n "sgx_tse") (r "= 1.0.1") (d #t) (k 0)) (d (n "sgx_types") (r "= 1.0.1") (d #t) (k 0)))) (h "1f91f7qh6wbv2pr6m2sy4z7mrsfmljjr5d19c8bwxiqa7w7f3ain") (f (quote (("default"))))))

(define-public crate-sgx_tseal-1.0.2 (c (n "sgx_tseal") (v "1.0.2") (d (list (d (n "sgx_tcrypto") (r "= 1.0.1") (d #t) (k 0)) (d (n "sgx_trts") (r "= 1.0.1") (d #t) (k 0)) (d (n "sgx_tse") (r "= 1.0.1") (d #t) (k 0)) (d (n "sgx_types") (r "= 1.0.1") (d #t) (k 0)))) (h "19rbxq051cym6blakdkn8krfsyaclihqwiv9nvxaha2hy2bvzynp") (f (quote (("default"))))))

(define-public crate-sgx_tseal-1.0.3 (c (n "sgx_tseal") (v "1.0.3") (d (list (d (n "sgx_tcrypto") (r "= 1.0.1") (d #t) (k 0)) (d (n "sgx_trts") (r "= 1.0.1") (d #t) (k 0)) (d (n "sgx_tse") (r "= 1.0.1") (d #t) (k 0)) (d (n "sgx_types") (r "= 1.0.1") (d #t) (k 0)))) (h "1xwnjqfv1a3cl0rv9yhia5l70yg0p1q3pjhnk57y0970ni94k77l") (f (quote (("default"))))))

(define-public crate-sgx_tseal-1.0.4 (c (n "sgx_tseal") (v "1.0.4") (d (list (d (n "sgx_tcrypto") (r "= 1.0.4") (d #t) (k 0)) (d (n "sgx_trts") (r "= 1.0.4") (d #t) (k 0)) (d (n "sgx_tse") (r "= 1.0.4") (d #t) (k 0)) (d (n "sgx_types") (r "= 1.0.4") (d #t) (k 0)))) (h "04b6kjgv69fkqd1myzizljqphfjx0nmky5f3gf21alpxvfnh77ai") (f (quote (("default"))))))

(define-public crate-sgx_tseal-1.0.5 (c (n "sgx_tseal") (v "1.0.5") (d (list (d (n "sgx_tcrypto") (r "= 1.0.5") (d #t) (k 0)) (d (n "sgx_trts") (r "= 1.0.5") (d #t) (k 0)) (d (n "sgx_tse") (r "= 1.0.5") (d #t) (k 0)) (d (n "sgx_types") (r "= 1.0.5") (d #t) (k 0)))) (h "1r7fdl9kmdkzkqbmidxihj48i7d21yfr6dkfj9rvrp78y804i65k") (f (quote (("default"))))))

(define-public crate-sgx_tseal-1.0.6 (c (n "sgx_tseal") (v "1.0.6") (d (list (d (n "sgx_tcrypto") (r "= 1.0.6") (d #t) (k 0)) (d (n "sgx_trts") (r "= 1.0.6") (d #t) (k 0)) (d (n "sgx_tse") (r "= 1.0.6") (d #t) (k 0)) (d (n "sgx_types") (r "= 1.0.6") (d #t) (k 0)))) (h "0is8jwrcy1873bp7q3l9hl292jka30ksyvcc0i5frc17v3rqfsl2") (f (quote (("default"))))))

(define-public crate-sgx_tseal-1.0.7 (c (n "sgx_tseal") (v "1.0.7") (d (list (d (n "sgx_tcrypto") (r "= 1.0.7") (d #t) (k 0)) (d (n "sgx_trts") (r "= 1.0.7") (d #t) (k 0)) (d (n "sgx_tse") (r "= 1.0.7") (d #t) (k 0)) (d (n "sgx_types") (r "= 1.0.7") (d #t) (k 0)))) (h "0fc0ng66j3gw2yppvs89zb0xwma2ifqdvynkgq027njy4bjjhld8") (f (quote (("default"))))))

(define-public crate-sgx_tseal-1.0.8 (c (n "sgx_tseal") (v "1.0.8") (d (list (d (n "sgx_tcrypto") (r "= 1.0.8") (d #t) (t "cfg(not(target_env = \"sgx\"))") (k 0)) (d (n "sgx_trts") (r "= 1.0.8") (d #t) (t "cfg(not(target_env = \"sgx\"))") (k 0)) (d (n "sgx_tse") (r "= 1.0.8") (d #t) (t "cfg(not(target_env = \"sgx\"))") (k 0)) (d (n "sgx_types") (r "= 1.0.8") (d #t) (t "cfg(not(target_env = \"sgx\"))") (k 0)))) (h "159jj6ahw33f0ia6yij1alpvm58qgzxrdg2sys045bviy73is1ys") (f (quote (("default"))))))

(define-public crate-sgx_tseal-1.0.9 (c (n "sgx_tseal") (v "1.0.9") (d (list (d (n "sgx_tcrypto") (r "= 1.0.9") (d #t) (t "cfg(not(target_env = \"sgx\"))") (k 0)) (d (n "sgx_trts") (r "= 1.0.9") (d #t) (t "cfg(not(target_env = \"sgx\"))") (k 0)) (d (n "sgx_tse") (r "= 1.0.9") (d #t) (t "cfg(not(target_env = \"sgx\"))") (k 0)) (d (n "sgx_types") (r "= 1.0.9") (d #t) (t "cfg(not(target_env = \"sgx\"))") (k 0)))) (h "0afs6ib3qwg65lkj4iigcbn1ls8aq75h581bd6kbvq5myvc13kzn") (f (quote (("default"))))))

(define-public crate-sgx_tseal-1.1.0 (c (n "sgx_tseal") (v "1.1.0") (d (list (d (n "sgx_tcrypto") (r "= 1.1.0") (d #t) (t "cfg(not(target_env = \"sgx\"))") (k 0)) (d (n "sgx_trts") (r "= 1.1.0") (d #t) (t "cfg(not(target_env = \"sgx\"))") (k 0)) (d (n "sgx_tse") (r "= 1.1.0") (d #t) (t "cfg(not(target_env = \"sgx\"))") (k 0)) (d (n "sgx_types") (r "= 1.1.0") (d #t) (t "cfg(not(target_env = \"sgx\"))") (k 0)))) (h "1g6z8kqpq0wm1pab0a0clq0kj0hlkg6kiy4lsdkrd0810lkghswl") (f (quote (("default"))))))

(define-public crate-sgx_tseal-1.1.1 (c (n "sgx_tseal") (v "1.1.1") (d (list (d (n "sgx_tcrypto") (r "= 1.1.1") (d #t) (t "cfg(not(target_env = \"sgx\"))") (k 0)) (d (n "sgx_trts") (r "= 1.1.1") (d #t) (t "cfg(not(target_env = \"sgx\"))") (k 0)) (d (n "sgx_tse") (r "= 1.1.1") (d #t) (t "cfg(not(target_env = \"sgx\"))") (k 0)) (d (n "sgx_types") (r "= 1.1.1") (d #t) (t "cfg(not(target_env = \"sgx\"))") (k 0)))) (h "1h1v7p7hwl0v4ay6vnqhwnnmyp110ngklc9xxcjrrra1wi2wzp3c") (f (quote (("default"))))))

