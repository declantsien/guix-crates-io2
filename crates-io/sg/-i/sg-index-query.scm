(define-module (crates-io sg #{-i}# sg-index-query) #:use-module (crates-io))

(define-public crate-sg-index-query-0.1.0 (c (n "sg-index-query") (v "0.1.0") (d (list (d (n "cosmwasm-schema") (r "^1.2.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2.1") (d #t) (k 0)) (d (n "cw-storage-plus") (r "^1.1.0") (d #t) (k 0)))) (h "1zbywanaaqf6hky0bd2kjj9kijh0by5x6bdiq2q4lfh45w8jv0f7") (f (quote (("library") ("backtraces" "cosmwasm-std/backtraces"))))))

(define-public crate-sg-index-query-0.1.1 (c (n "sg-index-query") (v "0.1.1") (d (list (d (n "cosmwasm-schema") (r "^1.2.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2.1") (d #t) (k 0)) (d (n "cw-storage-plus") (r "^1.1.0") (d #t) (k 0)))) (h "1iq73mdxscjf9pqbglndi42gspy1q1aih318x9pgg89fjvwmvz2b") (f (quote (("library") ("backtraces" "cosmwasm-std/backtraces"))))))

