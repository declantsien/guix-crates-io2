(define-module (crates-io sg -p sg-prost-derive) #:use-module (crates-io))

(define-public crate-sg-prost-derive-0.11.2 (c (n "sg-prost-derive") (v "0.11.2") (d (list (d (n "anyhow") (r "^1.0.1") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.3") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1v8jjr1rwa3730rms9dyhm2gwlf5y9cj9acdszl2yxmwglhhcpzd") (r "1.56")))

