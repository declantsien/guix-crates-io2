(define-module (crates-io sg -p sg-prost) #:use-module (crates-io))

(define-public crate-sg-prost-0.11.3 (c (n "sg-prost") (v "0.11.3") (d (list (d (n "bytes") (r "^1") (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "env_logger") (r "^0.8") (k 2)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "sg-prost-derive") (r "^0.11.0") (o #t) (d #t) (k 0)))) (h "134f5bdi2j47f85pv2hl0bjarvli2vak2xi4vkbdwlwp8pixqxbb") (f (quote (("std") ("no-recursion-limit") ("default" "sg-prost-derive" "std")))) (r "1.56")))

