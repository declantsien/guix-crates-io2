(define-module (crates-io sg ri sgrif-test-publish) #:use-module (crates-io))

(define-public crate-sgrif-test-publish-0.1.0 (c (n "sgrif-test-publish") (v "0.1.0") (h "17ry509kcgsl3qbipmkpi35ip83w61x2qg6hycslrdb0fsfjhpia") (y #t)))

(define-public crate-sgrif-test-publish-0.1.1 (c (n "sgrif-test-publish") (v "0.1.1") (h "1arlywjbbfh9lb4h6hc7fhsj4nkp522q2h662l8ihnlp1g256wil") (y #t)))

