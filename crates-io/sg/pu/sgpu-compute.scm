(define-module (crates-io sg pu sgpu-compute) #:use-module (crates-io))

(define-public crate-sgpu-compute-0.1.0 (c (n "sgpu-compute") (v "0.1.0") (d (list (d (n "bytemuck") (r "^1.14") (f (quote ("min_const_generics" "derive"))) (d #t) (k 0)) (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "flume") (r "^0.11.0") (d #t) (k 0)) (d (n "pollster") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "pollster") (r "^0.3.0") (f (quote ("macro"))) (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rayon") (r "^1.9") (d #t) (k 2)) (d (n "wgpu") (r "^0.19") (d #t) (k 0)))) (h "1hy29r543b0i9lp6gqi74wk4zrrmhps7ys708m9z2mbbg3mf22a9") (f (quote (("default" "blocking")))) (s 2) (e (quote (("blocking" "dep:pollster"))))))

