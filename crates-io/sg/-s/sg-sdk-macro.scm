(define-module (crates-io sg -s sg-sdk-macro) #:use-module (crates-io))

(define-public crate-sg-sdk-macro-0.0.1 (c (n "sg-sdk-macro") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1s1rp12bp641yrvmdi4kj7sj6fndjvhdsijnpwmcmm2kfanxsipd")))

