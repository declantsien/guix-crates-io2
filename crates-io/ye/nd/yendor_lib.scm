(define-module (crates-io ye nd yendor_lib) #:use-module (crates-io))

(define-public crate-yendor_lib-0.1.0 (c (n "yendor_lib") (v "0.1.0") (h "09l9mz75b6n9k4wrxgqjjg0n776448zh3ciz3r6qvyq86887blm7") (y #t)))

(define-public crate-yendor_lib-0.0.0 (c (n "yendor_lib") (v "0.0.0") (h "0fnzxc5vakgksx89yqiygycrqls84an9123qj58d0ma062r8jwxb")))

