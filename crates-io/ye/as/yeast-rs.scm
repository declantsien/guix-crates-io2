(define-module (crates-io ye as yeast-rs) #:use-module (crates-io))

(define-public crate-yeast-rs-0.3.0 (c (n "yeast-rs") (v "0.3.0") (d (list (d (n "async-std") (r "^1.9.0") (o #t) (d #t) (k 0)) (d (n "async-std") (r "^1.9.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "criterion") (r "^0.3.4") (f (quote ("async_tokio" "async_std"))) (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serial_test") (r "^0.5.1") (d #t) (k 2)) (d (n "tokio") (r "^1.8.1") (f (quote ("sync"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.8.1") (f (quote ("rt" "rt-multi-thread"))) (d #t) (k 2)))) (h "1rf9b0sw9zsxch33sk2q2p0mjigdlka3527m27ybdkhz2zlj0f4y") (f (quote (("tokio-runtime" "tokio") ("default") ("async-std-runtime" "async-std"))))))

