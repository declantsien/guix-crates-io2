(define-module (crates-io ye as yeast) #:use-module (crates-io))

(define-public crate-yeast-0.20.0 (c (n "yeast") (v "0.20.0") (d (list (d (n "ansi_term") (r "^0.11") (d #t) (k 0)) (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.37") (d #t) (k 0)) (d (n "rand") (r "^0.4.2") (d #t) (k 0)))) (h "0p6j32iinib3apik4p2axyxglcsl6cw6sy03kq8wh5rn742cdgbx")))

(define-public crate-yeast-0.20.1 (c (n "yeast") (v "0.20.1") (d (list (d (n "ansi_term") (r "^0.11") (d #t) (k 0)) (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.37") (d #t) (k 0)) (d (n "rand") (r "^0.4.2") (d #t) (k 0)))) (h "06argq8kvw0v58dhqq18s7m8g66wi20igk1bdan26wyvszgf9dvh")))

(define-public crate-yeast-0.20.2 (c (n "yeast") (v "0.20.2") (d (list (d (n "ansi_term") (r "^0.11") (d #t) (k 0)) (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.37") (d #t) (k 0)) (d (n "rand") (r "^0.4.2") (d #t) (k 0)))) (h "1rqpdqnfmc23m9hbgdd9c8x1d95rinrg0l9d69lvg3nx0wrmw3v4")))

