(define-module (crates-io ye wd yewdux-input) #:use-module (crates-io))

(define-public crate-yewdux-input-0.1.0 (c (n "yewdux-input") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.22") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (d #t) (k 0)) (d (n "yew") (r "^0.20") (d #t) (k 0)) (d (n "yewdux") (r "^0.9") (d #t) (k 0)))) (h "0ky4y1ganjryq9s40xdbrjxgi0kpi660skb4mwlama8baxvc5fix")))

