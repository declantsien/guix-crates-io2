(define-module (crates-io ye wd yewdux-utils) #:use-module (crates-io))

(define-public crate-yewdux-utils-0.9.3 (c (n "yewdux-utils") (v "0.9.3") (d (list (d (n "yewdux") (r "^0.9.3") (d #t) (k 0)))) (h "01jwpkrnm5wfkmzqgzkabc1ar818hmqkr53z7qq8pmhy4y0ppnfj")))

(define-public crate-yewdux-utils-0.9.4 (c (n "yewdux-utils") (v "0.9.4") (d (list (d (n "yewdux") (r "^0.9.4") (d #t) (k 0)))) (h "054h309vzqbmgxz6qxayxxnn7wpw6qp8xfhbq7lcx8n3r0m87i6d")))

(define-public crate-yewdux-utils-0.10.0 (c (n "yewdux-utils") (v "0.10.0") (d (list (d (n "yewdux") (r "^0.10.0") (d #t) (k 0)))) (h "11y7i0rlnrvzbprq48655gnwxbdhxlyh2z4160qqjxj0vjc5c55c")))

