(define-module (crates-io ye wd yewdux-functional) #:use-module (crates-io))

(define-public crate-yewdux-functional-0.1.0 (c (n "yewdux-functional") (v "0.1.0") (d (list (d (n "yew") (r "^0.19") (d #t) (k 0)) (d (n "yewdux") (r "^0.7") (d #t) (k 0)))) (h "0y4vqhdpik13hm1yk7srmkx351wgi38j4yq805prng66m9cqpf34")))

