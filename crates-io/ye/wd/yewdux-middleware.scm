(define-module (crates-io ye wd yewdux-middleware) #:use-module (crates-io))

(define-public crate-yewdux-middleware-0.1.0 (c (n "yewdux-middleware") (v "0.1.0") (d (list (d (n "anymap") (r "^1.0.0-beta.2") (d #t) (k 0)) (d (n "yewdux") (r "^0.8.3") (k 0)))) (h "0hfwai3zb9m9nv3qpxpgq5mn1bhhrxcq01n5p4ldl1j9nzr872bl")))

(define-public crate-yewdux-middleware-0.2.0 (c (n "yewdux-middleware") (v "0.2.0") (d (list (d (n "anymap") (r "^1.0.0-beta.2") (d #t) (k 0)) (d (n "yew") (r "^0.21") (k 0)) (d (n "yewdux") (r "^0.9") (k 0)))) (h "1064r5x32m8hi495np6wawgvrdqi663rwii5y2wrlm4yzp5jwrgj")))

(define-public crate-yewdux-middleware-0.3.0 (c (n "yewdux-middleware") (v "0.3.0") (d (list (d (n "anymap") (r "^1.0.0-beta.2") (d #t) (k 0)) (d (n "yew") (r "^0.21") (k 0)) (d (n "yewdux") (r "^0.10") (k 0)))) (h "112r4ja88s96zj6avzg65rq3g0vzf1939a6m6c5jjyw3268gvi7c")))

