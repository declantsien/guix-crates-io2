(define-module (crates-io ye wd yewdux-macros) #:use-module (crates-io))

(define-public crate-yewdux-macros-0.8.0 (c (n "yewdux-macros") (v "0.8.0") (d (list (d (n "darling") (r "^0.13.3") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.14") (d #t) (k 0)) (d (n "syn") (r "^1.0.84") (d #t) (k 0)))) (h "0bsb4z827zi2li86drnhpazxpkb4ig0cw7i0748sjbyv7p82kf56")))

(define-public crate-yewdux-macros-0.8.1 (c (n "yewdux-macros") (v "0.8.1") (d (list (d (n "darling") (r "^0.13.3") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.14") (d #t) (k 0)) (d (n "syn") (r "^1.0.84") (d #t) (k 0)))) (h "02s5fib49zfqb0mj7q69xjkdnzldc31gzxfc44ar4qkg221w5ggj")))

(define-public crate-yewdux-macros-0.8.2 (c (n "yewdux-macros") (v "0.8.2") (d (list (d (n "darling") (r "^0.13.3") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.14") (d #t) (k 0)) (d (n "syn") (r "^1.0.84") (d #t) (k 0)))) (h "00r7svdkyj7lmyr8j6lgb3n089d7b0nlpaxwbzxclj4l3az0cqpj")))

(define-public crate-yewdux-macros-0.8.3 (c (n "yewdux-macros") (v "0.8.3") (d (list (d (n "darling") (r "^0.13.3") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.14") (d #t) (k 0)) (d (n "syn") (r "^1.0.84") (d #t) (k 0)))) (h "1ygv1f1x0kj8g2vbwz6ra3hmz43kl4945w127i85rr24kbddr7kb")))

(define-public crate-yewdux-macros-0.9.0 (c (n "yewdux-macros") (v "0.9.0") (d (list (d (n "darling") (r "^0.13.3") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.14") (d #t) (k 0)) (d (n "syn") (r "^1.0.84") (d #t) (k 0)))) (h "1vvwbahr9x2glnhhh4rkrk3n4hv0iwcwb6g2g8mhsq7n4v98bhdk")))

(define-public crate-yewdux-macros-0.9.1 (c (n "yewdux-macros") (v "0.9.1") (d (list (d (n "darling") (r "^0.13.3") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.14") (d #t) (k 0)) (d (n "syn") (r "^1.0.84") (d #t) (k 0)))) (h "1ys4xpqm6r4g5qg614k7xyr6l4x67f23c41mq7ayvd4ndvdxwi0i") (y #t)))

(define-public crate-yewdux-macros-0.9.2 (c (n "yewdux-macros") (v "0.9.2") (d (list (d (n "darling") (r "^0.13.3") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.14") (d #t) (k 0)) (d (n "syn") (r "^1.0.84") (d #t) (k 0)))) (h "0f9794bphbqqfhap23kl69lnngbfqdy6bn7smys5ra7amhixkg15")))

(define-public crate-yewdux-macros-0.9.3 (c (n "yewdux-macros") (v "0.9.3") (d (list (d (n "darling") (r "^0.13.3") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.14") (d #t) (k 0)) (d (n "syn") (r "^1.0.84") (d #t) (k 0)))) (h "0vdbxasn3d9h7aayz52pf2p3j74z7qajdlgca7b8742awdr80cfc")))

(define-public crate-yewdux-macros-0.9.4 (c (n "yewdux-macros") (v "0.9.4") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.14") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0bbw671w2s1k2jcyblc0f52hfrc7zapl1k9fbhdadsp3scr1hayj")))

(define-public crate-yewdux-macros-0.10.0 (c (n "yewdux-macros") (v "0.10.0") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.14") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0v0mkzrmjz8i9rs94x0zj58l62k5la43pkcz7aarql81vn71yw60")))

