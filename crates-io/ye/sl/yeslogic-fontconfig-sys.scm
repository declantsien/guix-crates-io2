(define-module (crates-io ye sl yeslogic-fontconfig-sys) #:use-module (crates-io))

(define-public crate-yeslogic-fontconfig-sys-2.11.1 (c (n "yeslogic-fontconfig-sys") (v "2.11.1") (d (list (d (n "const-cstr") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1lz32b4cry6nfy4ifbpyk6ss37vrm6n2d7gdbqhqfp0vrp2s1571") (l "fontconfig")))

(define-public crate-yeslogic-fontconfig-sys-2.11.2 (c (n "yeslogic-fontconfig-sys") (v "2.11.2") (d (list (d (n "const-cstr") (r "^0.3") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "13bzwn12dmxnc8iqd6za6q3j2ililnqjcplnlrfblbca4ia73r1q") (l "fontconfig")))

(define-public crate-yeslogic-fontconfig-sys-3.0.0 (c (n "yeslogic-fontconfig-sys") (v "3.0.0") (d (list (d (n "const-cstr") (r "^0.3") (d #t) (k 0)) (d (n "dlib") (r "^0.5.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.9.0") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0hkq5v2lb2p0fcs1dijzq6f082r3ykfr78gqhdm6yqxx0mgsyzsh") (f (quote (("dlopen")))) (l "fontconfig") (r "1.46")))

(define-public crate-yeslogic-fontconfig-sys-3.0.1 (c (n "yeslogic-fontconfig-sys") (v "3.0.1") (d (list (d (n "const-cstr") (r "^0.3") (d #t) (k 0)) (d (n "dlib") (r "^0.5.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.9.0") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1f6b9qww6k9yh40q3p7y9j7dhzr1nj993dm1sd86dvqvqf8mlgyb") (f (quote (("dlopen")))) (l "fontconfig") (r "1.46")))

(define-public crate-yeslogic-fontconfig-sys-3.1.0 (c (n "yeslogic-fontconfig-sys") (v "3.1.0") (d (list (d (n "const-cstr") (r "^0.3") (d #t) (k 0)) (d (n "dlib") (r "^0.5.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.9.0") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0arhwcjy3awp757vcak25z8wpj344013zlgv3gq5czhwhx1q607v") (f (quote (("dlopen")))) (l "fontconfig") (r "1.46")))

(define-public crate-yeslogic-fontconfig-sys-3.2.0 (c (n "yeslogic-fontconfig-sys") (v "3.2.0") (d (list (d (n "const-cstr") (r "^0.3") (d #t) (k 0)) (d (n "dlib") (r "^0.5.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.9.0") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "11n3126s717rjqxhf5js3hc0qq8qv7jbicbiyszyp5yk6s8ddfzj") (f (quote (("dlopen")))) (l "fontconfig") (r "1.46")))

(define-public crate-yeslogic-fontconfig-sys-4.0.0 (c (n "yeslogic-fontconfig-sys") (v "4.0.0") (d (list (d (n "const-cstr") (r "^0.3") (d #t) (k 0)) (d (n "dlib") (r "^0.5.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.9.0") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1v1czbrz0p59s1pc7aw7w1vi7q4kyk92jd82d1cvy13jv48gj3yq") (f (quote (("dlopen")))) (l "fontconfig") (r "1.56")))

(define-public crate-yeslogic-fontconfig-sys-4.0.1 (c (n "yeslogic-fontconfig-sys") (v "4.0.1") (d (list (d (n "const-cstr") (r "^0.3") (d #t) (k 0)) (d (n "dlib") (r "^0.5.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.9.0") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0wsgzlvgknvkjw3m02nvp3x47ccmvfri13mwqzssxgmw5g9pyrgc") (f (quote (("dlopen")))) (l "fontconfig") (r "1.56")))

(define-public crate-yeslogic-fontconfig-sys-5.0.0 (c (n "yeslogic-fontconfig-sys") (v "5.0.0") (d (list (d (n "cstr") (r "^0.2.11") (d #t) (k 0)) (d (n "dlib") (r "^0.5.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.9.0") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0yiwnf2gapqaprp3icvv6b1jjh5d356vpis7pybskcd8k4wv5dpz") (f (quote (("dlopen")))) (l "fontconfig") (r "1.64")))

