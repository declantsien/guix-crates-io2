(define-module (crates-io ye sl yeslogic-fontconfig) #:use-module (crates-io))

(define-public crate-yeslogic-fontconfig-0.1.0 (c (n "yeslogic-fontconfig") (v "0.1.0") (d (list (d (n "yeslogic-fontconfig-sys") (r "^2.11.1") (d #t) (k 0)))) (h "13nakbfgvcy0gnz0k03kdzgd4rj444s2ry4p8c41xgp664d546j9")))

(define-public crate-yeslogic-fontconfig-0.1.1 (c (n "yeslogic-fontconfig") (v "0.1.1") (d (list (d (n "yeslogic-fontconfig-sys") (r "^2.11.1") (d #t) (k 0)))) (h "1q1w67xwq2xc6zlhq5c3b508imz99ixdzv8xl7qynny94c3wfi44")))

