(define-module (crates-io ye sl yeslogic-ucd-parse) #:use-module (crates-io))

(define-public crate-yeslogic-ucd-parse-0.1.5 (c (n "yeslogic-ucd-parse") (v "0.1.5") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "084v4graavrp7p6j2bdgvdiyv99x7ad8g54w3hsmwpn8m193c7ha")))

(define-public crate-yeslogic-ucd-parse-0.1.8 (c (n "yeslogic-ucd-parse") (v "0.1.8") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (f (quote ("std" "unicode"))) (k 0)))) (h "0g4f7fzbxwnaj2g3mhw18qx7ljcwxm5ycpygdq7xv8y1hkv0pgs5")))

(define-public crate-yeslogic-ucd-parse-0.1.10 (c (n "yeslogic-ucd-parse") (v "0.1.10") (d (list (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (f (quote ("std" "unicode"))) (k 0)))) (h "16m7kddbqn30mkg5aibja94chflycrspzzx5j7lkj9krf616r5m5")))

