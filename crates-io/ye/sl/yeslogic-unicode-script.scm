(define-module (crates-io ye sl yeslogic-unicode-script) #:use-module (crates-io))

(define-public crate-yeslogic-unicode-script-0.4.0 (c (n "yeslogic-unicode-script") (v "0.4.0") (d (list (d (n "harfbuzz-sys") (r "^0.3") (o #t) (d #t) (k 0)))) (h "0mk7krzfck0pvxwq439jgd6rhdv5i5n35gabrywqhc26n84cj8ni") (f (quote (("harfbuzz" "harfbuzz-sys"))))))

(define-public crate-yeslogic-unicode-script-0.5.0 (c (n "yeslogic-unicode-script") (v "0.5.0") (d (list (d (n "harfbuzz-sys") (r "^0.5") (o #t) (d #t) (k 0)))) (h "1q08l16167dx821jl6zqjnmcziqjk24dx9cx2zhx4bd4yafn2mnv") (f (quote (("harfbuzz" "harfbuzz-sys"))))))

(define-public crate-yeslogic-unicode-script-0.6.0 (c (n "yeslogic-unicode-script") (v "0.6.0") (d (list (d (n "harfbuzz-sys") (r "^0.5") (o #t) (d #t) (k 0)))) (h "0j03g9xin57n2dnv56mbwzr1prbjilcy8r1yv9nz8xd90a6y74kw") (f (quote (("harfbuzz" "harfbuzz-sys"))))))

(define-public crate-yeslogic-unicode-script-0.7.0 (c (n "yeslogic-unicode-script") (v "0.7.0") (d (list (d (n "harfbuzz-sys") (r "^0.5") (o #t) (d #t) (k 0)))) (h "10z36lwn2x7abl1vsf4xqgvrf12dnh8d775s54mlzslafrx7rfvd") (f (quote (("harfbuzz" "harfbuzz-sys"))))))

