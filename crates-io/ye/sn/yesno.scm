(define-module (crates-io ye sn yesno) #:use-module (crates-io))

(define-public crate-yesno-0.1.0 (c (n "yesno") (v "0.1.0") (d (list (d (n "arrayref") (r "^0.3.6") (d #t) (k 0)) (d (n "borsh") (r "^0.9.1") (d #t) (k 0)) (d (n "borsh-derive") (r "^0.9.1") (d #t) (k 0)) (d (n "proptest") (r "^1.0") (d #t) (k 2)) (d (n "solana-program") (r "^1.7.11") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.7.11") (d #t) (k 2)) (d (n "spl-token") (r "^3.2.0") (f (quote ("no-entrypoint"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "1ykb3klfkdxp5k1xnrsjvaw84igs0q9cj5awbxzwlcrd3gjd1rrk") (f (quote (("no-entrypoint"))))))

