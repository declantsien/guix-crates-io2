(define-module (crates-io ye ar year-helper) #:use-module (crates-io))

(define-public crate-year-helper-0.1.0 (c (n "year-helper") (v "0.1.0") (h "1sr3yjr98qhfdnj21lx8ipkjrkdjv43kqvxmqzvig9064r0g6qan")))

(define-public crate-year-helper-0.1.1 (c (n "year-helper") (v "0.1.1") (h "0b9mv76in0m2as39jr1lapz5yj49546wh7hr1qwvc9a9lc4x67k3")))

(define-public crate-year-helper-0.2.0 (c (n "year-helper") (v "0.2.0") (h "1dvb8dgb4gipzwsxz2vlir102n8jwv8ddw6xxqjf9xyl58d7dnvy")))

(define-public crate-year-helper-0.2.1 (c (n "year-helper") (v "0.2.1") (h "1wq5ilsibh726zdw4p9772h2czska7gv4szi5cx1gr2c7ikv5kjx")))

