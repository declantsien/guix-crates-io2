(define-module (crates-io ye ar yearfrac) #:use-module (crates-io))

(define-public crate-yearfrac-0.1.0 (c (n "yearfrac") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0iy865mb9b8yqaqj77s3063z25c0vz22iv16gbzd1k3kqbind3kn")))

(define-public crate-yearfrac-0.1.1 (c (n "yearfrac") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "10pa48qy33s6nymhd5r6mhwnk7czhlvi3l08mx996l7j62hl91d4")))

(define-public crate-yearfrac-0.1.2 (c (n "yearfrac") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "049xq9hpqwm01a9gsgyimpw971y90zgxnyh274sp1ydzwvk2svdl")))

(define-public crate-yearfrac-0.1.3 (c (n "yearfrac") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0wma4lf29hw6k645chkh75ml2dlni1znbl0yf5pr75cxmz1mcjga")))

(define-public crate-yearfrac-0.1.4 (c (n "yearfrac") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4.22") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "07yi6qmn8vshrhbsdxkwiw6fn9s46fk79mjswvbc722r5j7jh93m")))

(define-public crate-yearfrac-0.1.5 (c (n "yearfrac") (v "0.1.5") (d (list (d (n "chrono") (r "^0.4.22") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1z4nrfix3amgvynklyaqb440scff5cxyg8xjzfsaqynnhfsdfpl7")))

(define-public crate-yearfrac-0.1.6 (c (n "yearfrac") (v "0.1.6") (d (list (d (n "chrono") (r "^0.4.22") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "19zynk47lrbnpwzs0hxa53mcw7hjgly7rd4fb4kx4ipfcrqnbfkk")))

(define-public crate-yearfrac-0.1.7 (c (n "yearfrac") (v "0.1.7") (d (list (d (n "chrono") (r "^0.4.22") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "utoipa") (r "^3.3.0") (o #t) (d #t) (k 0)))) (h "1hxvxcmfd1rdyvlmxy7vbi9k97zda8c9s8vwl1angvqdr5gqzng9") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde") ("openapi" "dep:utoipa"))))))

(define-public crate-yearfrac-0.2.0 (c (n "yearfrac") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.22") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "utoipa") (r "^3.3.0") (o #t) (d #t) (k 0)))) (h "1yqf2ckgfdskdn2j08cjqcy860x6vnq2zipwpy4vhx49i7w5h3hv") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde") ("openapi" "dep:utoipa"))))))

