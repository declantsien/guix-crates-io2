(define-module (crates-io ye ar year_update) #:use-module (crates-io))

(define-public crate-year_update-1.0.0 (c (n "year_update") (v "1.0.0") (h "0dgs0p3wl90k53mnq1kxm60mkfbhjqidcxpqk4pdfbkqfzimg7lh")))

(define-public crate-year_update-1.1.0 (c (n "year_update") (v "1.1.0") (h "19yz1clal7gmrsh3lj1icxyc932s2l8m5ybxkr5c4hdmy546glwh")))

(define-public crate-year_update-1.1.1 (c (n "year_update") (v "1.1.1") (h "0rm731accac5r6fhmj0l7g7chs0ls91hmilsvhpykz9688zw3lbg")))

(define-public crate-year_update-1.2.0 (c (n "year_update") (v "1.2.0") (h "0r5vn6qg20yjynkgkq1mygs552picjng6ibgb6z1mxk00qqar0mr")))

