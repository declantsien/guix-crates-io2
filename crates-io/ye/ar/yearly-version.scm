(define-module (crates-io ye ar yearly-version) #:use-module (crates-io))

(define-public crate-yearly-version-0.0.0 (c (n "yearly-version") (v "0.0.0") (d (list (d (n "semver") (r "^1.0.22") (d #t) (k 2)))) (h "0ihjx3bsxr8yhplzjx7v17s40gjqn4gnx039l3nikdfdarln9gd6") (f (quote (("default"))))))

(define-public crate-yearly-version-0.0.1 (c (n "yearly-version") (v "0.0.1") (d (list (d (n "semver") (r "^1.0.22") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (o #t) (d #t) (k 0)))) (h "1nf4f5hy880a3ix25r62hhzj8338r8rfzga84dahii6yfjsnpa36") (f (quote (("default"))))))

(define-public crate-yearly-version-0.0.2 (c (n "yearly-version") (v "0.0.2") (d (list (d (n "schemars") (r "^0.8.16") (o #t) (d #t) (k 0)) (d (n "semver") (r "^1.0.22") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.198") (o #t) (d #t) (k 0)))) (h "1ca3jwsknqwsrfd6s7fjgk3fpzsk3ka4sx5y2r8jn33j45ks4qy7") (f (quote (("default"))))))

(define-public crate-yearly-version-0.0.4 (c (n "yearly-version") (v "0.0.4") (d (list (d (n "schemars") (r "^0.8.16") (o #t) (d #t) (k 0)) (d (n "semver") (r "^1.0.22") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.198") (o #t) (d #t) (k 0)))) (h "117cbx23ijy7irnd1n7iyqfzwvawhj81wiq3mw30a33llwxj15jq") (f (quote (("default"))))))

(define-public crate-yearly-version-0.0.5 (c (n "yearly-version") (v "0.0.5") (d (list (d (n "schemars") (r "^0.8.16") (o #t) (d #t) (k 0)) (d (n "semver") (r "^1.0.22") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.198") (o #t) (d #t) (k 0)))) (h "15ks3r14d4dnghvqdk6pbl1jhcfj47f2qdsvvhcn0j0hi8j1y2z2") (f (quote (("default"))))))

(define-public crate-yearly-version-0.0.6 (c (n "yearly-version") (v "0.0.6") (d (list (d (n "schemars") (r "^0.8.16") (o #t) (d #t) (k 0)) (d (n "semver") (r "^1.0.22") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.198") (o #t) (d #t) (k 0)))) (h "10cxijkjvqgkv6gb8jwc3yx6qan5wdd49jc5nndx8g8z5zzy1dwz") (f (quote (("default"))))))

