(define-module (crates-io ye ar yearsfx) #:use-module (crates-io))

(define-public crate-yearsfx-0.1.0 (c (n "yearsfx") (v "0.1.0") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0br0pjsfzb0xrw9i1ny7447715df3dh4v5pili8ak1s1wsqqb3mx")))

