(define-module (crates-io ye nc yenc) #:use-module (crates-io))

(define-public crate-yenc-0.0.1 (c (n "yenc") (v "0.0.1") (h "0lp78x5xsn5qpbs538rhbzawy5v81ha3ajp0q7pk8pzx49cjyhqf")))

(define-public crate-yenc-0.0.2 (c (n "yenc") (v "0.0.2") (h "0rgnd2h90qkiw3qm78w55nrs9an80h6a4mn30gpwais6d5rf4yrj")))

(define-public crate-yenc-0.0.3 (c (n "yenc") (v "0.0.3") (h "0jgfhmnqckvhbi7pbdilfq45ps8df68mpkh4ha6s64sxihc9yls5")))

(define-public crate-yenc-0.0.4 (c (n "yenc") (v "0.0.4") (h "1dl45m807h6c7mf6lyb2574arlilrfflplbbkyy3dlhymw6vx40z")))

(define-public crate-yenc-0.1.0 (c (n "yenc") (v "0.1.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "lazy_static") (r "^1.1") (d #t) (k 2)) (d (n "rand") (r "^0.4.2") (d #t) (k 2)))) (h "0rcjhpcd632wfqnw2v8v746yvw91xqyqin0zkxi9izzczlgfwdgf")))

(define-public crate-yenc-0.1.1 (c (n "yenc") (v "0.1.1") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "lazy_static") (r "^1.1") (d #t) (k 2)) (d (n "rand") (r "^0.4.2") (d #t) (k 2)))) (h "031d7q0wfc1miilbx309i5a3fibi2a672dbw57n5apb23palnbb1")))

(define-public crate-yenc-0.2.0 (c (n "yenc") (v "0.2.0") (d (list (d (n "criterion") (r "^0.2.10") (d #t) (k 2)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 2)) (d (n "rand") (r "^0.6.5") (d #t) (k 2)))) (h "0314k64adgczqx95xlpk06jyvggpdnxff0c2p07p1gi66r8l7f8j")))

(define-public crate-yenc-0.2.1 (c (n "yenc") (v "0.2.1") (d (list (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.7.2") (d #t) (k 2)))) (h "121dj0cc406gzqpjr40637mx0xyaj8n3n6vh29jqmx5nh47vm2ih")))

(define-public crate-yenc-0.2.2 (c (n "yenc") (v "0.2.2") (d (list (d (n "crc32fast") (r "^1.2.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "08aba6idl26yl876c2hs0n7w16bzxzf1igb6k3d2pv7xs2dqvcgj")))

