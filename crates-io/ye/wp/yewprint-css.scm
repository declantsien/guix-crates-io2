(define-module (crates-io ye wp yewprint-css) #:use-module (crates-io))

(define-public crate-yewprint-css-0.0.0 (c (n "yewprint-css") (v "0.0.0") (h "0izf3qapnhsmbdjks5q91qyrw8k433z5xkpx9kwd0l4jrq5rlpq6")))

(define-public crate-yewprint-css-0.1.0 (c (n "yewprint-css") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)))) (h "0d30hvbs7247vayk9338i4bxnvm6xqhczayf5vav42n4hm3wcqkr")))

(define-public crate-yewprint-css-0.1.1 (c (n "yewprint-css") (v "0.1.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)))) (h "1syj339fvmz89b4k8g4iic3g2nqmryh3z41s9xw35r6c7xsc5p64")))

(define-public crate-yewprint-css-0.2.0 (c (n "yewprint-css") (v "0.2.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)))) (h "0dgwh8f77knjvazin892k3dly6gw02pbmnfrnlckp004ndsym1vb")))

(define-public crate-yewprint-css-0.2.1 (c (n "yewprint-css") (v "0.2.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)) (d (n "ureq") (r "^2.4") (f (quote ("json"))) (d #t) (k 0)))) (h "0jy6w1cmi78gjypcyiny9djskd3vlvv9137z83misq6z9yv1ldk6")))

(define-public crate-yewprint-css-0.4.0 (c (n "yewprint-css") (v "0.4.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)) (d (n "ureq") (r "^2.5") (f (quote ("json"))) (d #t) (k 0)))) (h "1bzcslj52igdnrzvlgpd9whyv3269k4i4b3m1mmsr6mk8vl4z564")))

