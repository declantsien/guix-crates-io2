(define-module (crates-io ye te yeter-macros) #:use-module (crates-io))

(define-public crate-yeter-macros-0.1.0 (c (n "yeter-macros") (v "0.1.0") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0waglk8a83w7df8kz5j7cvy7bnxa97kr7gchbvxk9qgw6vwx06lp")))

(define-public crate-yeter-macros-0.4.0 (c (n "yeter-macros") (v "0.4.0") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1a4dk26bxd9z62vmm6q1j1zmr3rzrn2rpi3rvidinfb34pf7rb8g")))

(define-public crate-yeter-macros-0.5.0 (c (n "yeter-macros") (v "0.5.0") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1i32kg6bbr6g5friw1fsfrq1c9bfjn5iz6vwnw7a1sc95bj4py4w")))

