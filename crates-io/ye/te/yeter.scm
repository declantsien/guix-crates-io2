(define-module (crates-io ye te yeter) #:use-module (crates-io))

(define-public crate-yeter-0.1.0 (c (n "yeter") (v "0.1.0") (h "12ia222i22yiqnvwbdifz84xdh6wwwxyb112bvbx9wj9fiflj41s")))

(define-public crate-yeter-0.2.0 (c (n "yeter") (v "0.2.0") (h "1ywqg6wf0z6g3byfhybrd2q1kwsmhka8m2dx89nh6s9j24qss3gx")))

(define-public crate-yeter-0.3.0 (c (n "yeter") (v "0.3.0") (d (list (d (n "state") (r "^0.5") (d #t) (k 0)))) (h "1w948b3w15vp4fkia9p70fq1xmlykk1ijga14v6avsqai2rn4b92")))

(define-public crate-yeter-0.4.0 (c (n "yeter") (v "0.4.0") (d (list (d (n "state") (r "^0.5") (d #t) (k 0)) (d (n "yeter-macros") (r "^0.1.0") (d #t) (k 0)))) (h "014bsli7vrzqxk765lfjlm2plsd2qp3nqlnqlyzpafklhfbfc6l4")))

(define-public crate-yeter-0.5.0 (c (n "yeter") (v "0.5.0") (d (list (d (n "state") (r "^0.5") (d #t) (k 0)) (d (n "yeter-macros") (r "^0.4.0") (d #t) (k 0)))) (h "0b3c68px5avran6im3ccn1fsmwk1vdnn2nnqa4k0cyn8473d6dz6")))

(define-public crate-yeter-0.6.0 (c (n "yeter") (v "0.6.0") (d (list (d (n "anymap2") (r "^0.13.0") (d #t) (k 0)) (d (n "thread_local") (r "^1.1.7") (d #t) (k 0)) (d (n "yeter-macros") (r "^0.5.0") (d #t) (k 0)))) (h "1wgk2lyglcd5dlwl8nrqzfkj1nlqj0jqna0f3n050747m8rcf21y")))

