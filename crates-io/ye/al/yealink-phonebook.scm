(define-module (crates-io ye al yealink-phonebook) #:use-module (crates-io))

(define-public crate-yealink-phonebook-0.1.0 (c (n "yealink-phonebook") (v "0.1.0") (d (list (d (n "ical") (r "^0.7") (f (quote ("vcard"))) (d #t) (k 0)) (d (n "minidom") (r "^0.14") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (f (quote ("stream"))) (d #t) (k 0)) (d (n "rocket") (r "^0.5.0-rc.2") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "1jvy3p6wpqc5wa0sgx0fa09zp3naz2g5aps5npcqildqx1rpb55f")))

(define-public crate-yealink-phonebook-0.1.1 (c (n "yealink-phonebook") (v "0.1.1") (d (list (d (n "ical") (r "^0.7") (f (quote ("vcard"))) (d #t) (k 0)) (d (n "minidom") (r "^0.14") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (f (quote ("stream"))) (d #t) (k 0)) (d (n "rocket") (r "^0.5.0-rc.2") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "0v906dpwmj33j2fb8aj7gzk9ffvwprn808fj70sajr71y8rma1fd")))

