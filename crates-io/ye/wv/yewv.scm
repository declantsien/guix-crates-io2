(define-module (crates-io ye wv yewv) #:use-module (crates-io))

(define-public crate-yewv-0.1.0 (c (n "yewv") (v "0.1.0") (d (list (d (n "yew") (r "^0.19") (d #t) (k 0)))) (h "13hd0wfqhgjjhw3wz0ya8qfzis0h7nppw1gym5a760lnw14dsgfm") (r "1.56.0")))

(define-public crate-yewv-0.1.1 (c (n "yewv") (v "0.1.1") (d (list (d (n "yew") (r "^0.19") (d #t) (k 0)))) (h "1dp4axpazhjpbckmlfd17d4345y1rbzccrs77qqzxqcxins40amg")))

(define-public crate-yewv-0.2.0 (c (n "yewv") (v "0.2.0") (d (list (d (n "yew") (r "^0.19") (d #t) (k 0)))) (h "139dibbnmkfaw5dn5a0l6kzibg468zhm5sm3xv56n9n6n5n535sk")))

(define-public crate-yewv-0.2.1 (c (n "yewv") (v "0.2.1") (d (list (d (n "gloo") (r "^0.6") (f (quote ("futures"))) (d #t) (k 2)) (d (n "gloo-utils") (r "^0.1.0") (d #t) (k 2)) (d (n "wasm-bindgen-test") (r "^0.3") (d #t) (k 2)) (d (n "yew") (r "^0.19") (d #t) (k 0)))) (h "138gxx7aa8zla43k0fkc9r52banj6wkqzzlxi3qan4m51s4kphi9")))

(define-public crate-yewv-0.2.2 (c (n "yewv") (v "0.2.2") (d (list (d (n "gloo") (r "^0.6") (f (quote ("futures"))) (d #t) (k 2)) (d (n "gloo-utils") (r "^0.1.0") (d #t) (k 2)) (d (n "wasm-bindgen-test") (r "^0.3") (d #t) (k 2)) (d (n "yew") (r "^0.19") (d #t) (k 0)))) (h "1sxssy68iy4zya0m9azrxfzkmrx6gdqyss4nzbi88ack3dafv1fz")))

(define-public crate-yewv-0.2.3 (c (n "yewv") (v "0.2.3") (d (list (d (n "gloo") (r "^0.6") (f (quote ("futures"))) (d #t) (k 2)) (d (n "gloo-utils") (r "^0.1.0") (d #t) (k 2)) (d (n "wasm-bindgen-test") (r "^0.3") (d #t) (k 2)) (d (n "yew") (r "^0.19") (d #t) (k 0)))) (h "1q7xpkxa4fnixa8qfq4yjkyjik932l4csjf098m5mw8gxc9fr4wf")))

