(define-module (crates-io ye ti yeti) #:use-module (crates-io))

(define-public crate-yeti-0.1.0 (c (n "yeti") (v "0.1.0") (h "0yb3whw3v4kkmq97q7w1gr8zbax26mnnpbksl1k6fax6cnag777l")))

(define-public crate-yeti-0.2.0 (c (n "yeti") (v "0.2.0") (h "0sflsy1w03a0pw89xp6hx630hpkwm1jdx0y3dm8lr8ly4sbh39s8")))

