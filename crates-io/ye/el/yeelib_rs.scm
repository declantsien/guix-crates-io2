(define-module (crates-io ye el yeelib_rs) #:use-module (crates-io))

(define-public crate-yeelib_rs-0.1.0 (c (n "yeelib_rs") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.37") (d #t) (k 2)) (d (n "fastrand") (r "^1.4.0") (d #t) (k 0)) (d (n "httparse") (r "^1.3.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)))) (h "196xf32i37imbckxzrbn8rzk5hmqkz1lgj9aj2zjqdxvfjii0i02")))

(define-public crate-yeelib_rs-0.1.1 (c (n "yeelib_rs") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.37") (d #t) (k 2)) (d (n "fastrand") (r "^1.4.0") (d #t) (k 0)) (d (n "httparse") (r "^1.3.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)))) (h "1i3vv0f63vsakpdlk3gp56yffa8laa24yc9fiqzga2f8c5wrad36")))

