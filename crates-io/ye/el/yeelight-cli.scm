(define-module (crates-io ye el yeelight-cli) #:use-module (crates-io))

(define-public crate-yeelight-cli-0.1.0 (c (n "yeelight-cli") (v "0.1.0") (d (list (d (n "structopt") (r "^0.3.15") (d #t) (k 0)) (d (n "yeelight") (r "^0.2.3") (f (quote ("from-str"))) (d #t) (k 0)))) (h "1p9j9rzy2w4gc3gax33iaj78rvalfn7061wmavq7v4yw8mhvsjzh")))

(define-public crate-yeelight-cli-0.1.1 (c (n "yeelight-cli") (v "0.1.1") (d (list (d (n "structopt") (r "^0.3.15") (d #t) (k 0)) (d (n "yeelight") (r "^0.2.4") (f (quote ("from-str"))) (d #t) (k 0)))) (h "031sdls545a9wlrzywnlzs1bvy755v8jm385c872asln1lsy7knc")))

(define-public crate-yeelight-cli-0.1.2 (c (n "yeelight-cli") (v "0.1.2") (d (list (d (n "structopt") (r "^0.3.15") (d #t) (k 0)) (d (n "yeelight") (r "^0.2.4") (f (quote ("from-str"))) (d #t) (k 0)))) (h "0d6cfsjczkxsgrqnifk30r4yzirna0jlbhd6yckq8y4zrym4a511")))

(define-public crate-yeelight-cli-0.2.0 (c (n "yeelight-cli") (v "0.2.0") (d (list (d (n "structopt") (r "^0.3.15") (d #t) (k 0)) (d (n "tokio") (r "^0.2.21") (f (quote ("macros"))) (d #t) (k 0)) (d (n "yeelight") (r "^0.3.0") (f (quote ("from-str"))) (d #t) (k 0)))) (h "0c6rqkpfs0mbnfw1wid7dkbhnp171706s60dp1yjycbr6y4dcq0h")))

