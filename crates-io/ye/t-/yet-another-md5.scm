(define-module (crates-io ye t- yet-another-md5) #:use-module (crates-io))

(define-public crate-yet-another-md5-1.0.0 (c (n "yet-another-md5") (v "1.0.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "log") (r "^0.4.16") (d #t) (k 0)) (d (n "rstest") (r "^0.12.0") (d #t) (k 2)) (d (n "serial_test") (r "^0.6.0") (d #t) (k 2)) (d (n "simplelog") (r "^0.11.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.8.1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "07mhjvz87fwf9fdrqxcrkygy1k12vykc7zd0mz97wkr2979cig5p")))

(define-public crate-yet-another-md5-2.0.0 (c (n "yet-another-md5") (v "2.0.0") (d (list (d (n "ctor") (r "^0.2.7") (d #t) (k 2)) (d (n "env_logger") (r "^0.11.3") (d #t) (k 2)) (d (n "log") (r "^0.4.16") (d #t) (k 0)) (d (n "rstest") (r "^0.12.0") (d #t) (k 2)) (d (n "tempfile") (r "^3.8.1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "19vyqmls8nn3dsc375az8pj8908krcccni1vjrgn0x1rkj2f1adv")))

