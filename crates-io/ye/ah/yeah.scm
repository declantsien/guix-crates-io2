(define-module (crates-io ye ah yeah) #:use-module (crates-io))

(define-public crate-yeah-0.1.0 (c (n "yeah") (v "0.1.0") (d (list (d (n "regex") (r "^1.3.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)))) (h "0h7blzqihqh4zvdq3pwg56gw7bpy8xds04glwv4b2ri7mjxaqk5g") (y #t)))

(define-public crate-yeah-0.1.1 (c (n "yeah") (v "0.1.1") (d (list (d (n "regex") (r "^1.3.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)))) (h "0rz2l55qra80fvqxc465rks7h8bybp51j7fljj0d1xmz6rx4cfgp") (y #t)))

(define-public crate-yeah-0.2.0 (c (n "yeah") (v "0.2.0") (d (list (d (n "pico-args") (r "^0.4.2") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (f (quote ("blocking"))) (d #t) (k 0)))) (h "17sk2k60zhfhv66marvmicrxja7fnsdp011ma9vs7bs7a225n1gh") (y #t)))

(define-public crate-yeah-0.2.1 (c (n "yeah") (v "0.2.1") (d (list (d (n "pico-args") (r "^0.4.2") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (f (quote ("blocking"))) (d #t) (k 0)))) (h "0j2zw6pxh5k972dh2yvvbmqg6qa6qvizvmbympnzxsylwxgy5r6h")))

(define-public crate-yeah-0.2.2 (c (n "yeah") (v "0.2.2") (d (list (d (n "pico-args") (r "^0.5.0") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (f (quote ("blocking"))) (d #t) (k 0)))) (h "182gbb5srsdl4fsii9yi0wmcsv4py5zlvk6wwbiy22wv7m71indd")))

