(define-module (crates-io ye w- yew-chart) #:use-module (crates-io))

(define-public crate-yew-chart-0.5.0 (c (n "yew-chart") (v "0.5.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "gloo-events") (r "^0.1") (d #t) (k 0)) (d (n "gloo-utils") (r "^0.1") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("DomRect" "Element" "SvgElement"))) (d #t) (k 0)) (d (n "yew") (r "^0.19") (d #t) (k 0)))) (h "0vkb8gfgzxgz8q3sixg3rfx6yslj4i2wwvap3ziaxjvwq24x31p8") (f (quote (("custom-tooltip"))))))

(define-public crate-yew-chart-0.6.0 (c (n "yew-chart") (v "0.6.0") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "gloo-events") (r "^0.2") (d #t) (k 0)) (d (n "gloo-utils") (r "^0.2") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.88") (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("DomRect" "Element" "SvgElement"))) (d #t) (k 0)) (d (n "yew") (r "^0.21") (d #t) (k 0)))) (h "0m8sajnb39ka9waj760rs0zffhmgvalw4lihalkzpc129rxacs9a") (f (quote (("custom-tooltip"))))))

