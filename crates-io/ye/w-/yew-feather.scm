(define-module (crates-io ye w- yew-feather) #:use-module (crates-io))

(define-public crate-yew-feather-0.1.0 (c (n "yew-feather") (v "0.1.0") (d (list (d (n "yew") (r "^0.17.4") (d #t) (k 0)))) (h "1fx39czclhx1lmw8kbf79cc7z2ly1kypmpx5k8pg7hh3gf26rymy")))

(define-public crate-yew-feather-0.1.1 (c (n "yew-feather") (v "0.1.1") (d (list (d (n "yew") (r "^0.17.4") (d #t) (k 0)))) (h "0myjxlsvjz5byagbvc460hy9kp00w2qs90n5nbcdrc16j9xkpphw")))

(define-public crate-yew-feather-0.1.2 (c (n "yew-feather") (v "0.1.2") (d (list (d (n "yew") (r "^0.18.0") (d #t) (k 0)))) (h "0yl7w7gsvalqhj9nkfl431v0y9ncka1m4csra7qaz9ni871n97zj")))

(define-public crate-yew-feather-0.2.0 (c (n "yew-feather") (v "0.2.0") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 1)) (d (n "serde_json") (r "^1.0.78") (d #t) (k 1)) (d (n "yew") (r "^0.19.3") (d #t) (k 0)))) (h "1w8pjs0lsa3bh9lpqfdyw8zkrwrarsv76g66i87x2iydscw9lk0y")))

(define-public crate-yew-feather-0.2.1 (c (n "yew-feather") (v "0.2.1") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 1)) (d (n "serde_json") (r "^1.0.78") (d #t) (k 1)) (d (n "yew") (r "^0.19.3") (d #t) (k 0)))) (h "1z5yx1j6s0bx1k79g91vd7fgkh9m8mp6615j47zzq9ikmdaq7kdh")))

(define-public crate-yew-feather-1.0.0 (c (n "yew-feather") (v "1.0.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 1)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 1)) (d (n "which") (r "^4.3.0") (d #t) (k 1)) (d (n "yew") (r "^0.20.0") (d #t) (k 0)))) (h "04vbxfrsd83ixamijqbyzbd2xx07y40ixjxh3gwl7j2qbf8xka3s")))

