(define-module (crates-io ye w- yew-component-size) #:use-module (crates-io))

(define-public crate-yew-component-size-0.1.0 (c (n "yew-component-size") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("Element" "DomRect" "HtmlIFrameElement" "Window"))) (d #t) (k 0)) (d (n "yew") (r "^0.17") (d #t) (k 0)))) (h "1yapqsvinwsbgvab6c98ngwis52xsgns272f5f4fz5pkmiqlbbb7") (f (quote (("default"))))))

