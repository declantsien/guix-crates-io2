(define-module (crates-io ye w- yew-alt-html) #:use-module (crates-io))

(define-public crate-yew-alt-html-0.1.0 (c (n "yew-alt-html") (v "0.1.0") (h "08l47vy6jks8vpvglcnxn8iyrlwzfhhq78f5y3xhcp52j6ys7fbn")))

(define-public crate-yew-alt-html-0.2.0 (c (n "yew-alt-html") (v "0.2.0") (h "0ys3z59w7716808h43yayj2m7lxl83rjvwr54fh4szwhxgmr63ii")))

(define-public crate-yew-alt-html-0.3.0 (c (n "yew-alt-html") (v "0.3.0") (h "09k8smpzzqadz4k6vlvmzlfpg95ldl22pywrhqy6468040nx5yys")))

(define-public crate-yew-alt-html-0.4.0 (c (n "yew-alt-html") (v "0.4.0") (h "1xpg25y4cx75mzyy92av1mxhmgccfjxm153x0jl1q0iwjysa4b82")))

