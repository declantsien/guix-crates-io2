(define-module (crates-io ye w- yew-alert) #:use-module (crates-io))

(define-public crate-yew-alert-0.1.0 (c (n "yew-alert") (v "0.1.0") (d (list (d (n "gloo") (r "^0.11.0") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.67") (f (quote ("Window"))) (d #t) (k 0)) (d (n "yew") (r "^0.21.0") (d #t) (k 0)))) (h "0y5h63k1wwk4kzcbnxp2ki5cpq9mkvcb519rmlqyckysjqa34kpw")))

