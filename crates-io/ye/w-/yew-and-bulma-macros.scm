(define-module (crates-io ye w- yew-and-bulma-macros) #:use-module (crates-io))

(define-public crate-yew-and-bulma-macros-0.1.0 (c (n "yew-and-bulma-macros") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.11") (f (quote ("derive" "full" "parsing" "printing" "clone-impls" "extra-traits" "proc-macro"))) (d #t) (k 0)))) (h "1lzg29mxbkxlbhzrpkc4swdpn03rk15s5rdkhxn7977dg5fa3isp") (r "1.60")))

(define-public crate-yew-and-bulma-macros-0.1.1 (c (n "yew-and-bulma-macros") (v "0.1.1") (d (list (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.11") (f (quote ("derive" "full" "parsing" "printing" "clone-impls" "extra-traits" "proc-macro"))) (d #t) (k 0)))) (h "0pcs0b6v06irwm9xsxir9qhxw5p156074p9hmnwjw5rlk68324sj") (r "1.60")))

(define-public crate-yew-and-bulma-macros-0.1.2 (c (n "yew-and-bulma-macros") (v "0.1.2") (d (list (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.11") (f (quote ("derive" "full" "parsing" "printing" "clone-impls" "extra-traits" "proc-macro"))) (d #t) (k 0)))) (h "0h9qzb46wp8r5sfshdxp2f8s6q395pzlljq9slvx4cp6lf6n9acp") (r "1.60")))

