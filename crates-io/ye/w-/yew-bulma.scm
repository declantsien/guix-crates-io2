(define-module (crates-io ye w- yew-bulma) #:use-module (crates-io))

(define-public crate-yew-bulma-0.0.1 (c (n "yew-bulma") (v "0.0.1") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde" "js-sys"))) (d #t) (k 0)) (d (n "float-cmp") (r "^0.8") (d #t) (k 0)) (d (n "include_dir") (r "^0.6") (d #t) (k 0)) (d (n "js-sys") (r "^0.3") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.8") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (d #t) (k 0)) (d (n "yew") (r "^0.17") (d #t) (k 0)))) (h "0ypbwr43gcypp6ls5c0sibka50hdid2h9q89zds9r7n48y9ym7xn")))

(define-public crate-yew-bulma-0.0.2 (c (n "yew-bulma") (v "0.0.2") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde" "js-sys"))) (d #t) (k 0)) (d (n "float-cmp") (r "^0.8") (d #t) (k 0)) (d (n "include_dir") (r "^0.6") (d #t) (k 0)) (d (n "js-sys") (r "^0.3") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.8") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (d #t) (k 0)) (d (n "yew") (r "^0.17") (d #t) (k 0)))) (h "0lml358xyixmydzq4x63g68bhmzda18s3lkxcivilb4ihypfy8jn") (y #t)))

