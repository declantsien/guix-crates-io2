(define-module (crates-io ye w- yew-agent-macro) #:use-module (crates-io))

(define-public crate-yew-agent-macro-0.2.0 (c (n "yew-agent-macro") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rustversion") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "0xwb3c9yk4fzmwsgjyrqq632hvbylbpdh3zd4n95rlinjin0zl3s") (r "1.64.0")))

