(define-module (crates-io ye w- yew-vdom-gen) #:use-module (crates-io))

(define-public crate-yew-vdom-gen-0.1.0 (c (n "yew-vdom-gen") (v "0.1.0") (d (list (d (n "gloo-console") (r "^0.2.0") (d #t) (k 2)) (d (n "gloo-utils") (r "^0.1.0") (d #t) (k 2)) (d (n "wasm-bindgen-test") (r "^0.3") (d #t) (k 2)) (d (n "web-sys") (r "^0.3") (d #t) (k 0)) (d (n "yew") (r "^0.19") (d #t) (k 0)))) (h "1g8szsycg8gfrhn1ih9xr0g1sljn984lcms1ldynwl8w2pxhjir0")))

