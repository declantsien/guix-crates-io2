(define-module (crates-io ye w- yew-style-in-rs-core) #:use-module (crates-io))

(define-public crate-yew-style-in-rs-core-0.2.0 (c (n "yew-style-in-rs-core") (v "0.2.0") (h "086b1b5xay8wxc68mnny3qc5bl0018fvqwfxfj1rg2ij56abm82c")))

(define-public crate-yew-style-in-rs-core-0.3.0 (c (n "yew-style-in-rs-core") (v "0.3.0") (h "02nwrym96rm3zj6a3v1ch3cqr44n6d4mqppn9qc20dazmd19r995")))

(define-public crate-yew-style-in-rs-core-0.3.1 (c (n "yew-style-in-rs-core") (v "0.3.1") (h "0bsjxh6pajma8lmsfm1q04l2czvzzcf1pi8pgvl3dp5vyywjf53k")))

(define-public crate-yew-style-in-rs-core-0.3.2 (c (n "yew-style-in-rs-core") (v "0.3.2") (h "1xsb4npm6ydmy75lvzj9zgig93x8qz0s4w1mna1yz93cn7bf7axh")))

(define-public crate-yew-style-in-rs-core-0.4.0 (c (n "yew-style-in-rs-core") (v "0.4.0") (h "1h1ldpqxvllxdgj6jw3b2cs4wpbjqb4a8qdb5mbwd7ldp7b4f1m9")))

(define-public crate-yew-style-in-rs-core-0.4.1 (c (n "yew-style-in-rs-core") (v "0.4.1") (h "1a1mpx0p4kp9zyd5qvlzvhpc9xl4i74xiic7mx7vjflwknh3h5hm")))

