(define-module (crates-io ye w- yew-commons) #:use-module (crates-io))

(define-public crate-yew-commons-0.1.0 (c (n "yew-commons") (v "0.1.0") (d (list (d (n "wasm-bindgen") (r "=0.2.83") (f (quote ("serde-serialize"))) (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3") (d #t) (k 2)) (d (n "yew") (r "^0.20") (d #t) (k 0)))) (h "18b9h1xbsphn2pdp68j11hgxp4hpma8w6b0c1mgigig4fb6yrrzz")))

(define-public crate-yew-commons-0.1.1 (c (n "yew-commons") (v "0.1.1") (d (list (d (n "wasm-bindgen") (r "0.2.*") (f (quote ("serde-serialize"))) (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3") (d #t) (k 2)) (d (n "yew") (r "^0.20") (d #t) (k 0)))) (h "06ysv7nigm9zfk0n5la5n2zxcwhd51g54l0g9an9073zlvs034di")))

