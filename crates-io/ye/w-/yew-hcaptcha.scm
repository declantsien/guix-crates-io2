(define-module (crates-io ye w- yew-hcaptcha) #:use-module (crates-io))

(define-public crate-yew-hcaptcha-0.1.0 (c (n "yew-hcaptcha") (v "0.1.0") (d (list (d (n "gloo-utils") (r "^0.1.5") (d #t) (k 0)) (d (n "js-sys") (r "^0.3.64") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.87") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.64") (d #t) (k 0)) (d (n "yew") (r "^0.20.0") (f (quote ("csr"))) (d #t) (k 0)))) (h "1qqfpg33g1r897rbd2qwp2hznsh445dzckrc866fn6if0iakkaw0")))

