(define-module (crates-io ye w- yew-components) #:use-module (crates-io))

(define-public crate-yew-components-0.1.0 (c (n "yew-components") (v "0.1.0") (d (list (d (n "wasm-bindgen") (r "^0.2.60") (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (d #t) (k 0)) (d (n "yew") (r "^0.15") (d #t) (k 0)))) (h "1qrcsgp6yhz1kzvq5c0z2zpad2wff0rqz2pafkgj2gqs0nhd5ahw")))

(define-public crate-yew-components-0.1.1 (c (n "yew-components") (v "0.1.1") (d (list (d (n "wasm-bindgen") (r "^0.2.60") (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (d #t) (k 0)) (d (n "yew") (r "^0.16") (d #t) (k 0)))) (h "1xdrhyg5knckv3kzyia4rmc7nk74adwszz5n4b8d95nzg24r9vq6")))

(define-public crate-yew-components-0.1.2 (c (n "yew-components") (v "0.1.2") (d (list (d (n "wasm-bindgen") (r "^0.2.60") (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (d #t) (k 0)) (d (n "yew") (r "^0.16") (d #t) (k 0)))) (h "1h0vwf10pwr94kyzypngbzqa58vcj4py24c03azybxfq3b27qb4s")))

(define-public crate-yew-components-0.2.0 (c (n "yew-components") (v "0.2.0") (d (list (d (n "wasm-bindgen") (r "^0.2.60") (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (d #t) (k 0)) (d (n "yew") (r "^0.17") (d #t) (k 0)))) (h "13ka8pdw67hf99v3mj0nr8qlfch5h1k6qr75z4vwd20rcc5vaa5s")))

(define-public crate-yew-components-0.3.0 (c (n "yew-components") (v "0.3.0") (d (list (d (n "wasm-bindgen") (r "^0.2.60") (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (d #t) (k 0)) (d (n "yew") (r "^0.18") (d #t) (k 0)))) (h "12a95a19fzh011am53ql4z0lsb3y13vzwkhy215jz6rfgchrss7h")))

