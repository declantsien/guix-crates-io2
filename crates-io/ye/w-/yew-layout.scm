(define-module (crates-io ye w- yew-layout) #:use-module (crates-io))

(define-public crate-yew-layout-0.1.0 (c (n "yew-layout") (v "0.1.0") (d (list (d (n "css-style") (r "^0.9.0") (d #t) (k 0)) (d (n "yew") (r "^0.17.4") (d #t) (k 0)))) (h "0lk971fbpjxz1xkc604vck60q6ls3igg330i87pvf4sjk55pb1yl")))

(define-public crate-yew-layout-0.2.0 (c (n "yew-layout") (v "0.2.0") (d (list (d (n "css-style") (r "^0.9.0") (d #t) (k 0)) (d (n "yew") (r "^0.19.0") (d #t) (k 0)))) (h "1v8sr4w642czhfjiiwsn1r06xs72m5mp8ck7j3qbixxfdn96qmhr")))

(define-public crate-yew-layout-0.3.0 (c (n "yew-layout") (v "0.3.0") (d (list (d (n "css-style") (r "^0.9.0") (d #t) (k 0)) (d (n "yew") (r "^0.19.0") (d #t) (k 0)))) (h "1vl4pk345b91491mc3a9l5ha0jx7650j00wn0ixa9j1vh5nkzv73")))

(define-public crate-yew-layout-0.4.0 (c (n "yew-layout") (v "0.4.0") (d (list (d (n "css-style") (r "^0.10.0") (d #t) (k 0)) (d (n "yew") (r "^0.19.0") (d #t) (k 0)))) (h "0rc3jkkqwrvdz34l47jwyav1vf1ngm4dppc0cwxrsbp027id2bhh")))

(define-public crate-yew-layout-0.5.0 (c (n "yew-layout") (v "0.5.0") (d (list (d (n "css-style") (r "^0.11.0") (d #t) (k 0)) (d (n "yew") (r "^0.19.0") (d #t) (k 0)))) (h "00ld76jw4827rmy58lssapwg4n6ksvzhw68h6r3zrliqw6xjh585")))

(define-public crate-yew-layout-0.6.0 (c (n "yew-layout") (v "0.6.0") (d (list (d (n "css-style") (r "^0.11.0") (d #t) (k 0)) (d (n "yew") (r "^0.19.0") (d #t) (k 0)))) (h "0nl4kh98qykp1774pzas9wmv4li4gl10z2b7yiwimfdy6f6flj13")))

(define-public crate-yew-layout-0.7.1 (c (n "yew-layout") (v "0.7.1") (d (list (d (n "css-style") (r "^0.12.1") (d #t) (k 0)) (d (n "yew") (r "^0.19.0") (d #t) (k 0)))) (h "0sbbvc0nwbhphw8gim8mhrnc47b85klm6nj050g2r2lrqy91b1cl")))

(define-public crate-yew-layout-0.7.2 (c (n "yew-layout") (v "0.7.2") (d (list (d (n "css-style") (r "^0.13.1") (d #t) (k 0)) (d (n "yew") (r "^0.19.0") (d #t) (k 0)))) (h "04vqxkk55wwpjmznrnrvy1910hk48yhljcl07hg3mp1d5xksxdai")))

(define-public crate-yew-layout-0.7.3 (c (n "yew-layout") (v "0.7.3") (d (list (d (n "css-style") (r "^0.13.1") (d #t) (k 0)) (d (n "yew") (r "^0.19.3") (d #t) (k 0)))) (h "0j81x1gfpv5qp2qbx81mqn90l0fa04phx5x0w6ggi5n30rh39mgm")))

