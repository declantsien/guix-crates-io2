(define-module (crates-io ye w- yew-virtualized) #:use-module (crates-io))

(define-public crate-yew-virtualized-0.1.0 (c (n "yew-virtualized") (v "0.1.0") (d (list (d (n "gloo-timers") (r "^0.2") (d #t) (k 0)) (d (n "js-sys") (r "^0.3") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("DomRectReadOnly"))) (d #t) (k 0)) (d (n "yew") (r "^0.19") (d #t) (k 0)))) (h "0hhb476l415cdggbw2x40z2p2nvi9jji9ddfac6g1i9wrf1iw8ym")))

(define-public crate-yew-virtualized-0.2.0 (c (n "yew-virtualized") (v "0.2.0") (d (list (d (n "gloo-timers") (r "^0.2") (d #t) (k 0)) (d (n "js-sys") (r "^0.3") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("DomRectReadOnly"))) (d #t) (k 0)) (d (n "yew") (r "^0.20") (d #t) (k 0)))) (h "0x8xp16pliqwdxpac9aw5fw3fzhcs0idbpcg3bshxcj8n1cja1nd")))

