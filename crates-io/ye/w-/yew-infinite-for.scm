(define-module (crates-io ye w- yew-infinite-for) #:use-module (crates-io))

(define-public crate-yew-infinite-for-0.1.0 (c (n "yew-infinite-for") (v "0.1.0") (d (list (d (n "web-sys") (r "^0.3.59") (f (quote ("HtmlInputElement" "HtmlDivElement"))) (d #t) (k 0)) (d (n "yew") (r "^0.19") (d #t) (k 0)))) (h "16bmxwszrx6365jjwxv9nqax2mhmicc3shrf2gm70jlasyxgg9kv")))

(define-public crate-yew-infinite-for-0.1.1 (c (n "yew-infinite-for") (v "0.1.1") (d (list (d (n "web-sys") (r "^0.3.59") (f (quote ("HtmlInputElement" "HtmlDivElement"))) (d #t) (k 0)) (d (n "yew") (r "^0.19") (d #t) (k 0)))) (h "1qzvhq11sa1y13ds5898848pa99hc49wq538kr7qf93m3nz3ija8")))

(define-public crate-yew-infinite-for-0.1.2 (c (n "yew-infinite-for") (v "0.1.2") (d (list (d (n "web-sys") (r "^0.3.59") (f (quote ("HtmlInputElement" "HtmlDivElement"))) (d #t) (k 0)) (d (n "yew") (r "^0.19") (d #t) (k 0)))) (h "0d73cmm8909kf8i4b1gkasdm9492axxx554ixlcqpaqa5q23prq4")))

(define-public crate-yew-infinite-for-0.1.3 (c (n "yew-infinite-for") (v "0.1.3") (d (list (d (n "web-sys") (r "^0.3.59") (f (quote ("HtmlInputElement" "HtmlDivElement"))) (d #t) (k 0)) (d (n "yew") (r "^0.19") (d #t) (k 0)))) (h "0gq9f2f47sn31vm0cn5mgxfsfvx2nxvdmmnbk0v2kzicicxv5r9m")))

