(define-module (crates-io ye w- yew-interop-macro) #:use-module (crates-io))

(define-public crate-yew-interop-macro-0.1.0 (c (n "yew-interop-macro") (v "0.1.0") (d (list (d (n "convert_case") (r "^0.4") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "yew-interop-core") (r "^0.1") (d #t) (k 0)))) (h "092xdq97aach6vf6ybq65b2s5sllylgwyp2sivxjrhc6isbq7wsq")))

(define-public crate-yew-interop-macro-0.2.0 (c (n "yew-interop-macro") (v "0.2.0") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)) (d (n "yew-interop-core") (r "^0.1.0") (d #t) (k 0)))) (h "1slv4xmpxzlmdmqy1wa7gy557zck028d95vwvhabn683dsrmzd5p") (f (quote (("script"))))))

(define-public crate-yew-interop-macro-0.3.0 (c (n "yew-interop-macro") (v "0.3.0") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)) (d (n "yew") (r "^0.19") (d #t) (k 2)) (d (n "yew-interop-core") (r "^0.2") (d #t) (k 0)))) (h "11vw8njpd8mmdggibbbsnakaxh3dcvxwvbb6yjb2wrp5adn59izx") (f (quote (("script"))))))

