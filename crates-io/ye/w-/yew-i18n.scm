(define-module (crates-io ye w- yew-i18n) #:use-module (crates-io))

(define-public crate-yew-i18n-0.1.0 (c (n "yew-i18n") (v "0.1.0") (d (list (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)) (d (n "yew") (r "^0.21.0") (f (quote ("csr"))) (d #t) (k 0)))) (h "0mn2ms975mnpnhd2xs6d288c3fhpmg277b9ahfy97h70raw83a5m")))

(define-public crate-yew-i18n-0.1.1 (c (n "yew-i18n") (v "0.1.1") (d (list (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)) (d (n "yew") (r "^0.21.0") (f (quote ("csr"))) (d #t) (k 0)))) (h "0n10a24y07ynp5rzanzjaw2qzyn788g6bk5m26v3mpyr4acnpqwn")))

(define-public crate-yew-i18n-0.1.2 (c (n "yew-i18n") (v "0.1.2") (d (list (d (n "serde_json") (r "^1.0.113") (d #t) (k 0)) (d (n "yew") (r "^0.21.0") (d #t) (k 0)))) (h "14zs1d15r6k6jriqi88basxfqqvjygvs0k18jh515yzynsgha040")))

