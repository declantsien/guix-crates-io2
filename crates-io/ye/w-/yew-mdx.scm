(define-module (crates-io ye w- yew-mdx) #:use-module (crates-io))

(define-public crate-yew-mdx-0.1.0-alpha.1 (c (n "yew-mdx") (v "0.1.0-alpha.1") (d (list (d (n "markdown") (r "^1.0.0-alpha.7") (d #t) (k 0)) (d (n "yew") (r "^0.20.0") (d #t) (k 0)))) (h "05nhpxiy37ykf8prh68jxgdv7fdfn9a0as6iwlvyp043z9ghzl0v")))

