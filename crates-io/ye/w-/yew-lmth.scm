(define-module (crates-io ye w- yew-lmth) #:use-module (crates-io))

(define-public crate-yew-lmth-0.0.0 (c (n "yew-lmth") (v "0.0.0") (d (list (d (n "proc-macro2") (r "^1.0.76") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1p5yfcg9xwc4ml1wmazfvldrj9942k7zjcfvg0hw7ma4n3kapazn")))

(define-public crate-yew-lmth-0.0.1 (c (n "yew-lmth") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1.0.76") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "019xk0qh63z90m5891lv4cf648lz3qj5fjc84a7hk0j545dmaflm")))

(define-public crate-yew-lmth-0.1.0-alpha (c (n "yew-lmth") (v "0.1.0-alpha") (d (list (d (n "proc-macro2") (r "^1.0.76") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "09i5j8cd13k4xzmilfmqr7j35qkw0w45jj8cqs4cvprqy6jwqinw")))

(define-public crate-yew-lmth-0.1.0 (c (n "yew-lmth") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1mx0fdcgqd1wfs8ykrs0zx536kccmfwa82s6pass6w2bbic4imil")))

