(define-module (crates-io ye w- yew-input) #:use-module (crates-io))

(define-public crate-yew-input-0.1.0 (c (n "yew-input") (v "0.1.0") (d (list (d (n "js-sys") (r "^0.3.44") (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (d #t) (k 0)) (d (n "yew") (r "^0.17.2") (d #t) (k 0)) (d (n "yew-state") (r "^0.2.4") (d #t) (k 0)))) (h "05kfx7n4qj929cav0yj0ljc6i0537r325fa37fgd50l7s54lls91")))

