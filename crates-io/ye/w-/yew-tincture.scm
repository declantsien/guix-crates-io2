(define-module (crates-io ye w- yew-tincture) #:use-module (crates-io))

(define-public crate-yew-tincture-0.1.0 (c (n "yew-tincture") (v "0.1.0") (d (list (d (n "uuid") (r "^1.2.2") (f (quote ("v4" "wasm-bindgen"))) (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("HtmlInputElement"))) (d #t) (k 0)) (d (n "yew") (r "^0.20") (d #t) (k 0)))) (h "089m4ndwdwwblnlnrh0cq43654z4k5f63bh0n6b0x4ygfcxxbsg8")))

(define-public crate-yew-tincture-0.2.0 (c (n "yew-tincture") (v "0.2.0") (d (list (d (n "uuid") (r "^1.2.2") (f (quote ("v4" "wasm-bindgen"))) (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("HtmlInputElement"))) (d #t) (k 0)) (d (n "yew") (r "^0.21") (d #t) (k 0)))) (h "08j2mlqk5xz1d4dg1qrq18z3dsvyr8kcy8sm8h12p0hv0cfvg81y")))

(define-public crate-yew-tincture-0.1.1 (c (n "yew-tincture") (v "0.1.1") (d (list (d (n "uuid") (r "^1.7") (f (quote ("v4" "js"))) (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("HtmlInputElement"))) (d #t) (k 0)) (d (n "yew") (r "^0.20") (d #t) (k 0)))) (h "1rpbp7k2xandgsscg101za0d56534mj677qqy605yg51gv440k57")))

(define-public crate-yew-tincture-0.2.1 (c (n "yew-tincture") (v "0.2.1") (d (list (d (n "uuid") (r "^1.7.0") (f (quote ("v4" "js"))) (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("HtmlInputElement"))) (d #t) (k 0)) (d (n "yew") (r "^0.21") (d #t) (k 0)))) (h "0bqp5q0amq4pazappx1m1n59si3hrafmv6bnpb090baj7s3frw1g")))

(define-public crate-yew-tincture-0.2.2 (c (n "yew-tincture") (v "0.2.2") (d (list (d (n "uuid") (r "^1.7.0") (f (quote ("v4" "js"))) (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("HtmlInputElement"))) (d #t) (k 0)) (d (n "yew") (r "^0.21") (d #t) (k 0)))) (h "13qzk503fgfrnk1f2wyxilzf2hfnr17697gy1z7g4z7rrqj28hni")))

