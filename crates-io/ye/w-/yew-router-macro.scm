(define-module (crates-io ye w- yew-router-macro) #:use-module (crates-io))

(define-public crate-yew-router-macro-0.6.0 (c (n "yew-router-macro") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^1.0.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.1") (d #t) (k 0)) (d (n "syn") (r "^1.0.2") (d #t) (k 0)) (d (n "yew-router-route-parser") (r "^0.6.0") (d #t) (k 0)))) (h "19743d843wmd1rg9ag5vrjdwi557y09r2x91w37z5zn9wrbhy3wr")))

(define-public crate-yew-router-macro-0.6.1 (c (n "yew-router-macro") (v "0.6.1") (d (list (d (n "proc-macro2") (r "^1.0.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.1") (d #t) (k 0)) (d (n "syn") (r "^1.0.2") (d #t) (k 0)) (d (n "yew-router") (r "^0.6.0") (d #t) (k 2)) (d (n "yew-router-route-parser") (r "^0.6.1") (d #t) (k 0)))) (h "0m19hx37igpw163rgm6dhrdy3w3sabcn2ydz5wyz94lzr0m6qy9a")))

(define-public crate-yew-router-macro-0.7.0 (c (n "yew-router-macro") (v "0.7.0") (d (list (d (n "proc-macro2") (r "^1.0.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.1") (d #t) (k 0)) (d (n "syn") (r "^1.0.2") (d #t) (k 0)) (d (n "yew-router-route-parser") (r "^0.7.0") (d #t) (k 0)))) (h "0hrw8sbhrqbyqzk2gw6lxq9if88pxpps52f1q7kflznr9j084srh")))

(define-public crate-yew-router-macro-0.8.0 (c (n "yew-router-macro") (v "0.8.0") (d (list (d (n "proc-macro2") (r "^1.0.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.1") (d #t) (k 0)) (d (n "syn") (r "^1.0.2") (d #t) (k 0)) (d (n "yew-router-route-parser") (r "^0.8.0") (d #t) (k 0)))) (h "1h3gx34kqxgfacba8rm3iz4i3saxmc0j617x3193rd8w9sky5n06")))

(define-public crate-yew-router-macro-0.9.0 (c (n "yew-router-macro") (v "0.9.0") (d (list (d (n "proc-macro2") (r "^1.0.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.1") (d #t) (k 0)) (d (n "syn") (r "^1.0.2") (d #t) (k 0)) (d (n "yew-router-route-parser") (r "^0.9.0") (d #t) (k 0)))) (h "14vdigj7i35sdknp135jzm9jlaw10hhh4sm13lxpsysh3lakd9zq")))

(define-public crate-yew-router-macro-0.10.0 (c (n "yew-router-macro") (v "0.10.0") (d (list (d (n "proc-macro2") (r "^1.0.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.1") (d #t) (k 0)) (d (n "syn") (r "^1.0.2") (d #t) (k 0)) (d (n "yew-router-route-parser") (r "^0.10.0") (d #t) (k 0)))) (h "1w8jvf3b4a94jq9h9kf53m835lcybda6hshn6c3hjvvwr0v93mc4")))

(define-public crate-yew-router-macro-0.11.0 (c (n "yew-router-macro") (v "0.11.0") (d (list (d (n "proc-macro2") (r "^1.0.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.1") (d #t) (k 0)) (d (n "syn") (r "^1.0.2") (d #t) (k 0)) (d (n "yew-router-route-parser") (r "^0.11.0") (d #t) (k 0)))) (h "1rxkzdcxgdafqxms29dp9sqwpcl0gah6qvy4y9qar91yi9328asy")))

(define-public crate-yew-router-macro-0.12.0 (c (n "yew-router-macro") (v "0.12.0") (d (list (d (n "proc-macro2") (r "^1.0.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.1") (d #t) (k 0)) (d (n "syn") (r "^1.0.2") (d #t) (k 0)) (d (n "yew-router-route-parser") (r "^0.12.0") (d #t) (k 0)))) (h "1h916hgi6wxmnz2zx9vnzpf2p4wkvillwaf2a1vv3cg8czk0r0l8")))

(define-public crate-yew-router-macro-0.13.0 (c (n "yew-router-macro") (v "0.13.0") (d (list (d (n "proc-macro2") (r "^1.0.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.1") (d #t) (k 0)) (d (n "syn") (r "^1.0.2") (d #t) (k 0)) (d (n "yew-router-route-parser") (r "^0.13.0") (d #t) (k 0)))) (h "0iz3a074iz01fgccm5f5cbv3fyg6m7szkzxhwn05235lwv3752ra")))

(define-public crate-yew-router-macro-0.14.0 (c (n "yew-router-macro") (v "0.14.0") (d (list (d (n "proc-macro2") (r "^1.0.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.1") (d #t) (k 0)) (d (n "syn") (r "^1.0.2") (d #t) (k 0)) (d (n "yew-router-route-parser") (r "^0.14.0") (d #t) (k 0)))) (h "19z5rg2gll658md2djg8j81nynld4lpzdrf28a3ql3ckdkqr71kn")))

(define-public crate-yew-router-macro-0.15.0 (c (n "yew-router-macro") (v "0.15.0") (d (list (d (n "proc-macro2") (r "^1.0.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.1") (d #t) (k 0)) (d (n "syn") (r "^1.0.2") (d #t) (k 0)) (d (n "yew-router-route-parser") (r "^0.15.0") (d #t) (k 0)))) (h "1fid6ami1x759i0m5k8d0i0dhnia0bkjxrn0s7i7b8dp4hlww2jc")))

(define-public crate-yew-router-macro-0.16.0 (c (n "yew-router-macro") (v "0.16.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rustversion") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "16p1lwwf8kb44zaypp2y1g4l5l86i3q6p4c0zi7svsjj7ccrs11r")))

(define-public crate-yew-router-macro-0.17.0 (c (n "yew-router-macro") (v "0.17.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rustversion") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "1qvadrfsdrh1vjglg4s7crs46ljl31wdrpj40spxs34yng6lkcl9") (r "1.60.0")))

(define-public crate-yew-router-macro-0.18.0 (c (n "yew-router-macro") (v "0.18.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rustversion") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "134pcmiyi5x6v8s8rnr3fg03v033qhx2piflgkgcza3wl28d3gs2") (r "1.64.0")))

