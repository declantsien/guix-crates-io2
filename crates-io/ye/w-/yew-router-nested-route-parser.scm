(define-module (crates-io ye w- yew-router-nested-route-parser) #:use-module (crates-io))

(define-public crate-yew-router-nested-route-parser-0.16.0 (c (n "yew-router-nested-route-parser") (v "0.16.0") (d (list (d (n "nom") (r "^5.0.0") (d #t) (k 0)))) (h "0pfkcmabmmc26mmv1d578vrjbinblm35by96h8d762lwa9an9zq6")))

(define-public crate-yew-router-nested-route-parser-0.16.1 (c (n "yew-router-nested-route-parser") (v "0.16.1") (d (list (d (n "nom") (r "^5.0.0") (d #t) (k 0)))) (h "0pkynz17229pyz3jmlgmm0f7s0vqlyny48x5ix4cb2909sbkwcws")))

