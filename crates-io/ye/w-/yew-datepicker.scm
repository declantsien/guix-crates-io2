(define-module (crates-io ye w- yew-datepicker) #:use-module (crates-io))

(define-public crate-yew-datepicker-0.1.0 (c (n "yew-datepicker") (v "0.1.0") (d (list (d (n "calendarize") (r "^1.1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "chronoutil") (r "^0.2.6") (d #t) (k 0)) (d (n "yew") (r "^0.21.0") (f (quote ("csr"))) (d #t) (k 0)))) (h "0zvqkdyyif3k74vl636nwylcaw2vidkp5ipwv9hiry6gh3j50s3h")))

(define-public crate-yew-datepicker-0.1.1 (c (n "yew-datepicker") (v "0.1.1") (d (list (d (n "calendarize") (r "^1.1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.31") (f (quote ("unstable-locales"))) (d #t) (k 0)) (d (n "chronoutil") (r "^0.2.6") (d #t) (k 0)) (d (n "yew") (r "^0.21.0") (f (quote ("csr"))) (d #t) (k 0)))) (h "0y82nbxb2dw0qb1wqg8wdsvayp4dgalsj002bp5lqxv66ii8frbj")))

(define-public crate-yew-datepicker-0.1.2 (c (n "yew-datepicker") (v "0.1.2") (d (list (d (n "calendarize") (r "^1.1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.31") (f (quote ("unstable-locales"))) (d #t) (k 0)) (d (n "chronoutil") (r "^0.2.6") (d #t) (k 0)) (d (n "yew") (r "^0.21.0") (f (quote ("csr"))) (d #t) (k 0)))) (h "0g5x9xyrsfyh4z786iqggmfqaadqkjj4z12jn6azkdx4r7m9xgc6")))

(define-public crate-yew-datepicker-0.2.0 (c (n "yew-datepicker") (v "0.2.0") (d (list (d (n "calendarize") (r "^1.1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.31") (f (quote ("unstable-locales"))) (d #t) (k 0)) (d (n "chronoutil") (r "^0.2.6") (d #t) (k 0)) (d (n "yew") (r "^0.21.0") (f (quote ("csr"))) (d #t) (k 0)))) (h "0jh5b5x39h6id6k68v8819iivm2y8yhvj5fj8sbim4905dy8zlxn")))

(define-public crate-yew-datepicker-0.2.1 (c (n "yew-datepicker") (v "0.2.1") (d (list (d (n "calendarize") (r "^1.1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.31") (f (quote ("unstable-locales"))) (d #t) (k 0)) (d (n "chronoutil") (r "^0.2.6") (d #t) (k 0)) (d (n "yew") (r "^0.21.0") (f (quote ("csr"))) (d #t) (k 0)))) (h "094fjkc27vzb4j542mg5a58dr2wg0gff57wk8sfzzflsjvk2d4px")))

(define-public crate-yew-datepicker-0.2.2 (c (n "yew-datepicker") (v "0.2.2") (d (list (d (n "calendarize") (r "^1.1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.31") (f (quote ("unstable-locales"))) (d #t) (k 0)) (d (n "chronoutil") (r "^0.2.6") (d #t) (k 0)) (d (n "gloo") (r "^0.11.0") (d #t) (k 0)) (d (n "stylist") (r "^0.13.0") (d #t) (k 0)) (d (n "yew") (r "^0.21.0") (f (quote ("csr"))) (d #t) (k 0)) (d (n "yew-template") (r "^0.10.0") (d #t) (k 0)))) (h "0l3j2hzh51vfb7m1qvdx3nfvhdg30hhy6gxcrk20f817qv2kc7pn")))

(define-public crate-yew-datepicker-0.2.3 (c (n "yew-datepicker") (v "0.2.3") (d (list (d (n "calendarize") (r "^1.1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.31") (f (quote ("unstable-locales"))) (d #t) (k 0)) (d (n "chronoutil") (r "^0.2.6") (d #t) (k 0)) (d (n "gloo") (r "^0.11.0") (d #t) (k 0)) (d (n "stylist") (r "^0.13.0") (d #t) (k 0)) (d (n "yew") (r "^0.21.0") (f (quote ("csr"))) (d #t) (k 0)) (d (n "yew-template") (r "^0.10.0") (d #t) (k 0)))) (h "0l10cm1cxdqn2mir8rz35jzaj41dnjz1bjzg07sbqpy63d9bsch9")))

(define-public crate-yew-datepicker-0.2.4 (c (n "yew-datepicker") (v "0.2.4") (d (list (d (n "calendarize") (r "^1.1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.31") (f (quote ("unstable-locales"))) (d #t) (k 0)) (d (n "chronoutil") (r "^0.2.6") (d #t) (k 0)) (d (n "gloo") (r "^0.11.0") (d #t) (k 0)) (d (n "stylist") (r "^0.13.0") (d #t) (k 0)) (d (n "yew") (r "^0.21.0") (f (quote ("csr"))) (d #t) (k 0)) (d (n "yew-template") (r "^0.10.0") (d #t) (k 0)))) (h "0v00cqml3c1byrm7qyq30gykvdafna3xjmiwk4ikjag8ag66s348")))

