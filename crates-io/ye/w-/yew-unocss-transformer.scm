(define-module (crates-io ye w- yew-unocss-transformer) #:use-module (crates-io))

(define-public crate-yew-unocss-transformer-0.0.1 (c (n "yew-unocss-transformer") (v "0.0.1") (d (list (d (n "once_cell") (r "^1.14.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (d #t) (k 0)) (d (n "yew") (r "^0.19.3") (d #t) (k 0)))) (h "1i1sr4xg9kg5fa38sbxlsnnz2g915m0fv77azcx30n8fx858b235") (y #t)))

(define-public crate-yew-unocss-transformer-0.0.2 (c (n "yew-unocss-transformer") (v "0.0.2") (d (list (d (n "once_cell") (r "^1.14.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (d #t) (k 0)) (d (n "yew") (r "^0.19.3") (d #t) (k 0)))) (h "1zjwwynrl97s1k905i4378r45qyzyv6hbbszn2pif65hflhy7hr2") (y #t)))

