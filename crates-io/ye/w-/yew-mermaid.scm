(define-module (crates-io ye w- yew-mermaid) #:use-module (crates-io))

(define-public crate-yew-mermaid-0.1.0 (c (n "yew-mermaid") (v "0.1.0") (d (list (d (n "mermaid-wasmbind") (r "^0.1") (d #t) (k 0)) (d (n "yew") (r "^0.17") (d #t) (k 0)))) (h "04rcgq4pv112vlyija88vlwkan8ync3nvzjvvsr38n8ys81dh21g") (f (quote (("default"))))))

(define-public crate-yew-mermaid-0.2.0 (c (n "yew-mermaid") (v "0.2.0") (d (list (d (n "mermaid-wasmbind") (r "^0.1") (d #t) (k 0)) (d (n "yew") (r "^0.17") (d #t) (k 0)))) (h "1y36hlcz0rd1vqfan8m4fzq8sh38yc7dkyn8k5xmhrjinr3dzzdb") (f (quote (("default"))))))

