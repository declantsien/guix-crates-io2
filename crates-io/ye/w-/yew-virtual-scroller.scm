(define-module (crates-io ye w- yew-virtual-scroller) #:use-module (crates-io))

(define-public crate-yew-virtual-scroller-0.1.0 (c (n "yew-virtual-scroller") (v "0.1.0") (d (list (d (n "web-sys") (r "^0.3") (f (quote ("Element" "DomRect"))) (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 2)) (d (n "yew") (r "^0.17") (d #t) (k 0)) (d (n "yew-component-size") (r "^0.1") (d #t) (k 0)))) (h "0snyyj1s9xmnam6qw5qppqbgjh9wygnyppv3y1pydh2lvnlf8jw7")))

