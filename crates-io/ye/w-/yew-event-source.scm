(define-module (crates-io ye w- yew-event-source) #:use-module (crates-io))

(define-public crate-yew-event-source-0.1.0 (c (n "yew-event-source") (v "0.1.0") (d (list (d (n "gloo") (r "^0.2.1") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.58") (f (quote ("serde-serialize"))) (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("EventSource" "DomRect" "Element" "DomTokenList" "HtmlCanvasElement" "HtmlImageElement"))) (d #t) (k 0)) (d (n "yew") (r "^0.17.4") (d #t) (k 0)))) (h "1dcv2jc47f5fdx6sn0pdgki3gx254520d9ynzb3dlay6nmlfk4s2")))

(define-public crate-yew-event-source-0.1.1 (c (n "yew-event-source") (v "0.1.1") (d (list (d (n "gloo") (r "^0.2.1") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.58") (f (quote ("serde-serialize"))) (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("EventSource" "DomRect" "Element" "DomTokenList" "HtmlCanvasElement" "HtmlImageElement"))) (d #t) (k 0)) (d (n "yew") (r "^0.17.4") (d #t) (k 0)))) (h "1h17pd3c5j67z14kwgqs386kp60w9f626x6ii3l3yw1pnkf2kvwp")))

(define-public crate-yew-event-source-0.2.0 (c (n "yew-event-source") (v "0.2.0") (d (list (d (n "gloo") (r "^0.2.1") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.58") (f (quote ("serde-serialize"))) (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("EventSource" "DomRect" "Element" "DomTokenList" "HtmlCanvasElement" "HtmlImageElement"))) (d #t) (k 0)) (d (n "yew") (r "^0.18") (d #t) (k 0)))) (h "1fmswxbrq3nfa48bg3drg5yx1ykljqb6c4xgrr9qjvcg7wb2rz14")))

(define-public crate-yew-event-source-0.2.1 (c (n "yew-event-source") (v "0.2.1") (d (list (d (n "gloo-events") (r "^0.1.1") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.58") (f (quote ("serde-serialize"))) (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("EventSource" "DomRect" "Element" "DomTokenList" "HtmlCanvasElement" "HtmlImageElement"))) (d #t) (k 0)) (d (n "yew") (r "^0.18") (d #t) (k 0)))) (h "1zg4207fbsa2kpjzrfk1nd922fia5m05wna32cq2aj4shxrmv4av")))

