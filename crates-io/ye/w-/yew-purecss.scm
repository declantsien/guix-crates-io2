(define-module (crates-io ye w- yew-purecss) #:use-module (crates-io))

(define-public crate-yew-purecss-0.1.0 (c (n "yew-purecss") (v "0.1.0") (d (list (d (n "yew") (r "^0.20.0") (f (quote ("csr"))) (d #t) (k 0)))) (h "0lmb1qqb2w0wq2z8x7b1pa80z38d1d0grg69wjka4ikyamynzp8c") (y #t) (r "1.63.0")))

(define-public crate-yew-purecss-0.1.5 (c (n "yew-purecss") (v "0.1.5") (d (list (d (n "yew") (r "^0.20.0") (f (quote ("csr"))) (d #t) (k 0)))) (h "0hs0r9ychdmkwr3lwb0298smfyx77i2fdahcc6ad6vw5mwaq8rq5") (y #t) (r "1.63.0")))

(define-public crate-yew-purecss-0.1.8 (c (n "yew-purecss") (v "0.1.8") (d (list (d (n "yew") (r "^0.20.0") (f (quote ("csr"))) (d #t) (k 0)))) (h "1my73l5dwadzi26f8nsksk3bf4i3frxkp3kid41vh1zk4mhp2zv2") (y #t) (r "1.63.0")))

(define-public crate-yew-purecss-0.1.12 (c (n "yew-purecss") (v "0.1.12") (d (list (d (n "yew") (r "^0.20.0") (f (quote ("csr"))) (d #t) (k 0)))) (h "0q9p9pd4lcjp3pn1ci21kc9j5anm8whm9ycrga65a60rjd8cv13i") (y #t) (r "1.63.0")))

(define-public crate-yew-purecss-0.1.13 (c (n "yew-purecss") (v "0.1.13") (d (list (d (n "yew") (r "^0.20.0") (f (quote ("csr"))) (d #t) (k 0)))) (h "1n1dk9k4xj4d4zwsgfpdcnpr1v8i4nw7jbk0lkzi4n72rfvgj3yy") (y #t) (r "1.63.0")))

(define-public crate-yew-purecss-0.1.17 (c (n "yew-purecss") (v "0.1.17") (d (list (d (n "yew") (r "^0.20") (d #t) (k 0)))) (h "087sdxrsqy6hqrj3x424kc8024j3s4jx81igxz2z99fy4s2nxazr") (y #t) (r "1.63.0")))

