(define-module (crates-io ye w- yew-autoprops) #:use-module (crates-io))

(define-public crate-yew-autoprops-0.1.0 (c (n "yew-autoprops") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.58") (d #t) (k 0)) (d (n "quote") (r "^1.0.27") (d #t) (k 0)) (d (n "syn") (r "^2.0.16") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)) (d (n "yew") (r "^0.20") (d #t) (k 2)))) (h "0rkvizyj0fd43x48lwl5anvmbqfvr4idjqdj46sjwz1vzbdwc792")))

(define-public crate-yew-autoprops-0.2.0 (c (n "yew-autoprops") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.58") (d #t) (k 0)) (d (n "quote") (r "^1.0.27") (d #t) (k 0)) (d (n "syn") (r "^2.0.16") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)) (d (n "yew") (r "^0.20") (d #t) (k 2)))) (h "0mc5b0i3c0jbaixrhfwzpw29h3xc5kdk1vhzhykgn85cxxihb6an")))

(define-public crate-yew-autoprops-0.2.1 (c (n "yew-autoprops") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0.58") (d #t) (k 0)) (d (n "quote") (r "^1.0.27") (d #t) (k 0)) (d (n "syn") (r "^2.0.16") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)) (d (n "yew") (r "^0.20") (d #t) (k 2)))) (h "1fi4pxp09jjgnzdpicy5qbvk4xq8kp2kam94alclfv4fzq92fng7")))

(define-public crate-yew-autoprops-0.2.2 (c (n "yew-autoprops") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1.0.58") (d #t) (k 0)) (d (n "quote") (r "^1.0.27") (d #t) (k 0)) (d (n "syn") (r "^2.0.16") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)) (d (n "yew") (r "^0.20") (d #t) (k 2)))) (h "0qlppx6a3pf4m57hqggldw1s22985wzqk666axx1ckpr0lfjnfx5")))

(define-public crate-yew-autoprops-0.3.0 (c (n "yew-autoprops") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)) (d (n "yew") (r "^0.20") (d #t) (k 2)))) (h "07kzrjw4glhn7b3rg87ddl8mf0cpx4rpy3yw90kaisj0zzqfhn01")))

(define-public crate-yew-autoprops-0.4.0 (c (n "yew-autoprops") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)) (d (n "yew") (r "^0.21") (d #t) (k 2)))) (h "1xbz8wf2whalzp0rzvqxsxri0jxbwfra6wj5jfg68xnblbflarbz") (y #t) (r "1.64.0")))

(define-public crate-yew-autoprops-0.4.1 (c (n "yew-autoprops") (v "0.4.1") (d (list (d (n "implicit-clone") (r "^0.4.6") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rustversion") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)) (d (n "yew") (r "^0.21") (d #t) (k 2)))) (h "14hp2fk80n22ij7k98ir1y9k438p4yb5rq3lja1sv9kh4j7mm1jq") (r "1.64.0")))

