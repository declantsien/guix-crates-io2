(define-module (crates-io ye w- yew-router-route-parser) #:use-module (crates-io))

(define-public crate-yew-router-route-parser-0.6.0 (c (n "yew-router-route-parser") (v "0.6.0") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "nom") (r "^5.0.0") (d #t) (k 0)))) (h "0wczphn56dmpvgg8d6ad2iix1grniiyw87bkx7wqg07an2q9skah")))

(define-public crate-yew-router-route-parser-0.6.1 (c (n "yew-router-route-parser") (v "0.6.1") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "nom") (r "^5.0.0") (d #t) (k 0)))) (h "0sz6h43gia46c64wm4jg4aki7z1x0ygwnrbl1pvxwb62y3n1m5ja")))

(define-public crate-yew-router-route-parser-0.7.0 (c (n "yew-router-route-parser") (v "0.7.0") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "nom") (r "^5.0.0") (d #t) (k 0)))) (h "1sa5xijdsa6s3i93jdyd63hsfjnj8h9zpq9jj9xk3r6xklka4jrx")))

(define-public crate-yew-router-route-parser-0.8.0 (c (n "yew-router-route-parser") (v "0.8.0") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "nom") (r "^5.0.0") (d #t) (k 0)))) (h "1fil1m04hznh0mq2mhs4s2jpa5rczmz2dka6y7qrd72az7hp1n8j")))

(define-public crate-yew-router-route-parser-0.9.0 (c (n "yew-router-route-parser") (v "0.9.0") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "nom") (r "^5.0.0") (d #t) (k 0)))) (h "0af8j0jcnqzlck0zl0zs9l9i2vxczvz60d6svbh51i2fxc37y1il")))

(define-public crate-yew-router-route-parser-0.10.0 (c (n "yew-router-route-parser") (v "0.10.0") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "nom") (r "^5.0.0") (d #t) (k 0)))) (h "0wcr5mpygmxlpb2m6kj92d450b5fvc18nvxffrxfm6fpgaj5z3sr")))

(define-public crate-yew-router-route-parser-0.11.0 (c (n "yew-router-route-parser") (v "0.11.0") (d (list (d (n "nom") (r "^5.0.0") (d #t) (k 0)))) (h "1n38y9qyjw85cvhqspi24hfqhlbwc90v281znczk30w5xgb02s4f")))

(define-public crate-yew-router-route-parser-0.12.0 (c (n "yew-router-route-parser") (v "0.12.0") (d (list (d (n "nom") (r "^5.0.0") (d #t) (k 0)))) (h "0ahrrbnxv2grsjf3y476ysx96ljgbs0ckn63p6152cnj2n1p1i5k")))

(define-public crate-yew-router-route-parser-0.13.0 (c (n "yew-router-route-parser") (v "0.13.0") (d (list (d (n "nom") (r "^5.0.0") (d #t) (k 0)))) (h "1jnrby050pfh0qzc7y8l889dqcpjz80a19x3rdc59yh3yzjxnqln")))

(define-public crate-yew-router-route-parser-0.14.0 (c (n "yew-router-route-parser") (v "0.14.0") (d (list (d (n "nom") (r "^5.0.0") (d #t) (k 0)))) (h "0wmng4i4fy0643xxif4wry8s4nnxmqgzl8vmcg8na7g6y1k2zgs9")))

(define-public crate-yew-router-route-parser-0.15.0 (c (n "yew-router-route-parser") (v "0.15.0") (d (list (d (n "nom") (r "^5.0.0") (d #t) (k 0)))) (h "15snf7n7wd58ryclg8wdq7gpkwq1if9rfwway4590sxliwh6fjny")))

