(define-module (crates-io ye w- yew-aplayer) #:use-module (crates-io))

(define-public crate-yew-aplayer-0.1.0 (c (n "yew-aplayer") (v "0.1.0") (d (list (d (n "aplayer-wasmbind") (r "^0.2") (d #t) (k 0)) (d (n "unescape") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "yew") (r "^0.17") (d #t) (k 0)))) (h "1dgv9fl50ma8qmx42gbvbnb7ygckm4cyz3ddqcf4czg7c8x13yvq") (f (quote (("meting" "unescape") ("default" "auto-cdn" "meting") ("auto-cdn"))))))

