(define-module (crates-io ye w- yew-table) #:use-module (crates-io))

(define-public crate-yew-table-0.1.0 (c (n "yew-table") (v "0.1.0") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "smart-default") (r "^0.5.2") (d #t) (k 0)) (d (n "yew") (r "^0.6.0") (d #t) (k 0)))) (h "033d9dhckirnaw6gck2pfa7y4lpw09dnjrjp6y22l1jzrg5b5ydv")))

(define-public crate-yew-table-0.1.1 (c (n "yew-table") (v "0.1.1") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "smart-default") (r "^0.5.2") (d #t) (k 0)) (d (n "yew") (r "^0.6.0") (d #t) (k 0)))) (h "0k6sinv0v25dafmafwhbxf5vgi25byfhh5h0h876n3bkg1xaaq19")))

(define-public crate-yew-table-0.1.2 (c (n "yew-table") (v "0.1.2") (d (list (d (n "serde") (r "^1.0.93") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-value") (r "^0.6.0") (d #t) (k 0)) (d (n "yew") (r "^0.6.0") (d #t) (k 0)))) (h "08samvddi8rdyx24h1r4wlxylzl96n52qb34dp6x4bn1xnqi1nig")))

