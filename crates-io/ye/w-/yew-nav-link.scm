(define-module (crates-io ye w- yew-nav-link) #:use-module (crates-io))

(define-public crate-yew-nav-link-0.1.0 (c (n "yew-nav-link") (v "0.1.0") (d (list (d (n "yew") (r "^0.21.0") (f (quote ("csr"))) (d #t) (k 0)) (d (n "yew-router") (r "^0.18.0") (d #t) (k 0)))) (h "1754jgdmsf2blgva39r7dfairar64id56ghpbcqi3cv0v610lgca") (r "1.75")))

(define-public crate-yew-nav-link-0.2.0 (c (n "yew-nav-link") (v "0.2.0") (d (list (d (n "yew") (r "^0.21.0") (f (quote ("csr"))) (d #t) (k 0)) (d (n "yew-router") (r "^0.18.0") (d #t) (k 0)))) (h "07jcj91jrfa2fx577i115j2lxbn3sr4wb8kajd8x4c4r8gyakp22") (r "1.75")))

