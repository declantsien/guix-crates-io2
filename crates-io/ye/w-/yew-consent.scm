(define-module (crates-io ye w- yew-consent) #:use-module (crates-io))

(define-public crate-yew-consent-0.1.0 (c (n "yew-consent") (v "0.1.0") (d (list (d (n "gloo-storage") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("console"))) (d #t) (k 0)) (d (n "yew") (r "^0.20") (d #t) (k 0)))) (h "11rfhd3s7ysp2p6i2hv3qn2zd852bzc8lzslcn32nvbq1xapcl49")))

(define-public crate-yew-consent-0.1.1 (c (n "yew-consent") (v "0.1.1") (d (list (d (n "gloo-storage") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("console"))) (d #t) (k 0)) (d (n "yew") (r "^0.20") (d #t) (k 0)))) (h "0075i6z3parhqbvsa5zw06vzsjys9kiq4yaf2yca3zr6g6dbg72p")))

(define-public crate-yew-consent-0.2.0 (c (n "yew-consent") (v "0.2.0") (d (list (d (n "gloo-storage") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("console"))) (d #t) (k 0)) (d (n "yew") (r "^0.21") (d #t) (k 0)))) (h "0gkja4ndif9djwzzxq6j0pzwzs0fm7hwd03sz737h0asqisdglwp")))

