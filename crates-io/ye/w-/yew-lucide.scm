(define-module (crates-io ye w- yew-lucide) #:use-module (crates-io))

(define-public crate-yew-lucide-0.17.16 (c (n "yew-lucide") (v "0.17.16") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 1)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.78") (d #t) (k 1)) (d (n "yew") (r "^0.19.3") (d #t) (k 0)))) (h "0sbawh31h84kmmf3s0x6m9l9094adm4gfw57s9xdv61q7b96lalm")))

(define-public crate-yew-lucide-0.35.0 (c (n "yew-lucide") (v "0.35.0") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 1)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.78") (d #t) (k 1)) (d (n "yew") (r "^0.19.3") (d #t) (k 0)))) (h "0cna5c3065g93iqmhzp0kmvqavg836h5nqcrwqs4kph70q2ychdg")))

(define-public crate-yew-lucide-0.37.0 (c (n "yew-lucide") (v "0.37.0") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 1)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.78") (d #t) (k 1)) (d (n "yew") (r "^0.19.3") (d #t) (k 0)))) (h "1pl9pzriqjasrwwl6qvzszy74j8mm7cccnn5wi4rn5wprapnjp7m")))

(define-public crate-yew-lucide-0.38.0 (c (n "yew-lucide") (v "0.38.0") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 1)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.78") (d #t) (k 1)) (d (n "yew") (r "^0.19.3") (d #t) (k 0)))) (h "1giki3g6i0m7i8mdqc5gg83vbdx0igagidnn2kijj20wdxim66vp")))

(define-public crate-yew-lucide-0.41.0 (c (n "yew-lucide") (v "0.41.0") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 1)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.78") (d #t) (k 1)) (d (n "yew") (r "^0.19.3") (d #t) (k 0)))) (h "0hclmcg9dh2z4j6akdyiamd2bavffbwnmnjv06aky8wfvf9kpn88")))

(define-public crate-yew-lucide-0.61.0 (c (n "yew-lucide") (v "0.61.0") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 1)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.78") (d #t) (k 1)) (d (n "yew") (r "^0.19.3") (d #t) (k 0)))) (h "0lfgvw04v0fgl4yh5mjbkw9sj23pcw62ic8fq6f6d65mhx2xwkax")))

(define-public crate-yew-lucide-0.72.0 (c (n "yew-lucide") (v "0.72.0") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 1)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.78") (d #t) (k 1)) (d (n "yew") (r "^0.19.3") (d #t) (k 0)))) (h "1svq94q8g7mbkdixh7mx0qys29bhfqkjp6088iklp1fswx87b788")))

(define-public crate-yew-lucide-0.73.0 (c (n "yew-lucide") (v "0.73.0") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 1)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.78") (d #t) (k 1)) (d (n "yew") (r "^0.19.3") (d #t) (k 0)))) (h "0f989cbq37lg1xjr611kcjhv836kirgvvwbi4crczrh0a7s3dx0r")))

(define-public crate-yew-lucide-0.75.0 (c (n "yew-lucide") (v "0.75.0") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 1)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.78") (d #t) (k 1)) (d (n "yew") (r "^0.19.3") (d #t) (k 0)))) (h "0ysxy2g9s6jg0y71jl1ridsf3q39g1zm7dy90ixvr9q1qbwmxjgq")))

(define-public crate-yew-lucide-0.77.0 (c (n "yew-lucide") (v "0.77.0") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 1)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.78") (d #t) (k 1)) (d (n "yew") (r "^0.19.3") (d #t) (k 0)))) (h "0vbzcaxwb3nwiafpjvj50ncnm8yi5m5chv9ycd2pgbi63q98sysv")))

(define-public crate-yew-lucide-0.79.0 (c (n "yew-lucide") (v "0.79.0") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 1)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.78") (d #t) (k 1)) (d (n "yew") (r "^0.19.3") (d #t) (k 0)))) (h "1lc27sagh8k4sils7s9dp944w27jraccz4ih3ky7fq4ksj55rvdf")))

(define-public crate-yew-lucide-0.81.0 (c (n "yew-lucide") (v "0.81.0") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 1)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.78") (d #t) (k 1)) (d (n "yew") (r "^0.19.3") (d #t) (k 0)))) (h "0lhahxiidqw16lx9nxnfnc8w4dvqarr2yrjypdmf44dqa4vj40pj")))

(define-public crate-yew-lucide-0.83.0 (c (n "yew-lucide") (v "0.83.0") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 1)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.78") (d #t) (k 1)) (d (n "yew") (r "^0.19.3") (d #t) (k 0)))) (h "1savhym0bys43rzfxhh75ga00c6snavgvwi8m8dcnwqxna80k4af")))

(define-public crate-yew-lucide-0.84.0 (c (n "yew-lucide") (v "0.84.0") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 1)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.78") (d #t) (k 1)) (d (n "yew") (r "^0.19.3") (d #t) (k 0)))) (h "0d7cjr5wfaxw6izcq6rc49qx270ki08bd3m762r487n77dp0jyzy")))

(define-public crate-yew-lucide-0.88.0 (c (n "yew-lucide") (v "0.88.0") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 1)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.78") (d #t) (k 1)) (d (n "yew") (r "^0.19.3") (d #t) (k 0)))) (h "1lnpj8f4nndnga7hccyhix7smfys3jyfjc1f3zlj8sijn1vi6pvg")))

(define-public crate-yew-lucide-0.89.0 (c (n "yew-lucide") (v "0.89.0") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 1)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.78") (d #t) (k 1)) (d (n "yew") (r "^0.19.3") (d #t) (k 0)))) (h "0zdnanb5byq8c7x4r34n204vx7wpnkxykrfr3649pnd4yn7w0aa9")))

(define-public crate-yew-lucide-0.90.0 (c (n "yew-lucide") (v "0.90.0") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 1)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.78") (d #t) (k 1)) (d (n "yew") (r "^0.19.3") (d #t) (k 0)))) (h "016msdbfjqsk7lpb3nszsqpi1674w4mqz0nfxh1v597jlmbpqq1z")))

(define-public crate-yew-lucide-0.91.0 (c (n "yew-lucide") (v "0.91.0") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 1)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.78") (d #t) (k 1)) (d (n "yew") (r "^0.19.3") (d #t) (k 0)))) (h "0sxmm29z03y5m30ncnnz69bfr26bllmlfh5wnn08b1bf7yd18r2j")))

(define-public crate-yew-lucide-0.100.0 (c (n "yew-lucide") (v "0.100.0") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 1)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.78") (d #t) (k 1)) (d (n "yew") (r "^0.19.3") (d #t) (k 0)))) (h "1vjaq24p5qvz3a376p2cny3r6kxi0ahf8hvvibfgbgjjr2immwj4")))

(define-public crate-yew-lucide-0.101.0 (c (n "yew-lucide") (v "0.101.0") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 1)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.78") (d #t) (k 1)) (d (n "yew") (r "^0.19.3") (d #t) (k 0)))) (h "0a2qnc141a89kcm13l27cq6dkszrpfsq6c637h196zn7qfv7c922")))

(define-public crate-yew-lucide-0.102.0 (c (n "yew-lucide") (v "0.102.0") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 1)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.78") (d #t) (k 1)) (d (n "yew") (r "^0.19.3") (d #t) (k 0)))) (h "1dksscn2nhnmlhd1ql442wpcpfjllkvllk7fxn3qj5n1prp28rml")))

(define-public crate-yew-lucide-0.103.0 (c (n "yew-lucide") (v "0.103.0") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 1)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.78") (d #t) (k 1)) (d (n "yew") (r "^0.20") (d #t) (k 0)))) (h "0kxa457j7g72ggldjmmg4fjyvj8xfja2v2cx9s7x5rs874rxjyl7")))

(define-public crate-yew-lucide-0.104.0 (c (n "yew-lucide") (v "0.104.0") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 1)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.78") (d #t) (k 1)) (d (n "yew") (r "^0.20") (d #t) (k 0)))) (h "15lrsgs0xfdaz931dmv96pk1fczywb20hwbywz999v96yz8i8wmd")))

(define-public crate-yew-lucide-0.105.0 (c (n "yew-lucide") (v "0.105.0") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 1)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.78") (d #t) (k 1)) (d (n "yew") (r "^0.20") (d #t) (k 0)))) (h "06sf7w2qz1anr4w3amcy1vhg0raiimlhf87hm220ym7bx3r5x4n0")))

(define-public crate-yew-lucide-0.108.0 (c (n "yew-lucide") (v "0.108.0") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 1)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.78") (d #t) (k 1)) (d (n "yew") (r "^0.20") (d #t) (k 0)))) (h "05insb7jn39kvymyl60c3b3wvc2rrjqgkhmrc7f22lkfvg0690bq")))

(define-public crate-yew-lucide-0.109.0 (c (n "yew-lucide") (v "0.109.0") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 1)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.78") (d #t) (k 1)) (d (n "yew") (r "^0.20") (d #t) (k 0)))) (h "0pp70qh1m0q2m141wbmrf56zazh1313v90gb7mgpd510x092jaf1")))

(define-public crate-yew-lucide-0.112.0 (c (n "yew-lucide") (v "0.112.0") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 1)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.78") (d #t) (k 1)) (d (n "yew") (r "^0.20") (d #t) (k 0)))) (h "1pxplzfbnywhnlnbia5my7hhq65ri58b1haywfvvyy0nakmnphs7")))

(define-public crate-yew-lucide-0.113.0 (c (n "yew-lucide") (v "0.113.0") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 1)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.78") (d #t) (k 1)) (d (n "yew") (r "^0.20") (d #t) (k 0)))) (h "0ff7zsx2jdl8aiqrqplmd2cv4mancd80k551af7a5fi666zysnxy")))

(define-public crate-yew-lucide-0.115.0 (c (n "yew-lucide") (v "0.115.0") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 1)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.78") (d #t) (k 1)) (d (n "yew") (r "^0.20") (d #t) (k 0)))) (h "1dy05038zlqgbj1ricf5hjrhkan76khccd614367qqjd9f2c0kjd")))

(define-public crate-yew-lucide-0.118.0 (c (n "yew-lucide") (v "0.118.0") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 1)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.78") (d #t) (k 1)) (d (n "yew") (r "^0.20") (d #t) (k 0)))) (h "09kahb6hsxz7fdskrzqd9l73nxi0h3qvwb7namfg4hqpsaj2y2yp")))

(define-public crate-yew-lucide-0.119.0 (c (n "yew-lucide") (v "0.119.0") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 1)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.78") (d #t) (k 1)) (d (n "yew") (r "^0.20") (d #t) (k 0)))) (h "0a9wlsln8lpg7l8z0lxqvlj4i1g4fm4hp9jb79ajav9n3ppyhn1f")))

(define-public crate-yew-lucide-0.121.0 (c (n "yew-lucide") (v "0.121.0") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 1)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.78") (d #t) (k 1)) (d (n "yew") (r "^0.20") (d #t) (k 0)))) (h "1qbnrax7ppqj40l99yfcaqnp8prjj38bmsnr2mmz60145p12qsp6")))

(define-public crate-yew-lucide-0.122.0 (c (n "yew-lucide") (v "0.122.0") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 1)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.78") (d #t) (k 1)) (d (n "yew") (r "^0.20") (d #t) (k 0)))) (h "1srd6ny0fcyr3dbxm993n5wkdspms6ygzpm7fldbd4314gplzpph")))

(define-public crate-yew-lucide-0.123.0 (c (n "yew-lucide") (v "0.123.0") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 1)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.78") (d #t) (k 1)) (d (n "yew") (r "^0.20") (d #t) (k 0)))) (h "0i11k6acm2kadg5r49z7v5zxzgssvcs9hsl9bssg8dq29n92g89y")))

(define-public crate-yew-lucide-0.124.0 (c (n "yew-lucide") (v "0.124.0") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 1)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.78") (d #t) (k 1)) (d (n "yew") (r "^0.20") (d #t) (k 0)))) (h "0fjkzac4vgfmwk6sq8qaiq5vkhi58cnk6rf3gjdxd1is0a1kii6v")))

(define-public crate-yew-lucide-0.125.0 (c (n "yew-lucide") (v "0.125.0") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 1)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.78") (d #t) (k 1)) (d (n "yew") (r "^0.20") (d #t) (k 0)))) (h "0annvday1zr9j3cqvir7817gav4263ljf6g2vkvfq4f5ci8vxk63")))

(define-public crate-yew-lucide-0.126.0 (c (n "yew-lucide") (v "0.126.0") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 1)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.78") (d #t) (k 1)) (d (n "yew") (r "^0.20") (d #t) (k 0)))) (h "0dqsx0ivc8q2ldaadxdyfqfzxxjlk4s99bfj3via1npv08d41sib")))

(define-public crate-yew-lucide-0.127.0 (c (n "yew-lucide") (v "0.127.0") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 1)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.78") (d #t) (k 1)) (d (n "yew") (r "^0.20") (d #t) (k 0)))) (h "0by3ik51dv845klpnmnpqswc8lnb41bf4i6p3wgygbkrrmcrrm66")))

(define-public crate-yew-lucide-0.128.0 (c (n "yew-lucide") (v "0.128.0") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 1)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.78") (d #t) (k 1)) (d (n "yew") (r "^0.20") (d #t) (k 0)))) (h "1kxfczlzlagwdakmalq3ip091dxxxw6nng3gs5wrl4nn0lqnbz3l")))

(define-public crate-yew-lucide-0.129.0 (c (n "yew-lucide") (v "0.129.0") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 1)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.78") (d #t) (k 1)) (d (n "yew") (r "^0.20") (d #t) (k 0)))) (h "1z290ha63i3js0nrgry6mvx89fnlz96zr2lgz0mxycx7y2h500vy")))

(define-public crate-yew-lucide-0.130.0 (c (n "yew-lucide") (v "0.130.0") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 1)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.78") (d #t) (k 1)) (d (n "yew") (r "^0.20") (d #t) (k 0)))) (h "0wlq7zgv6kn1cg0dzsi5byiikn4l34a7hj4ysifivhnhdjs7h0ml")))

(define-public crate-yew-lucide-0.137.0 (c (n "yew-lucide") (v "0.137.0") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 1)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.78") (d #t) (k 1)) (d (n "yew") (r "^0.20") (d #t) (k 0)))) (h "19pi930518bpg3ihl2ig5pjm4c8vbfyv5a03r3jg8b15lwnw74ai")))

(define-public crate-yew-lucide-0.139.0 (c (n "yew-lucide") (v "0.139.0") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 1)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.78") (d #t) (k 1)) (d (n "yew") (r "^0.20") (d #t) (k 0)))) (h "058l4380zj7xxmlxlw8gh49y7gqgp8x5mqwycyvi2kylamz0l5lz")))

(define-public crate-yew-lucide-0.140.0 (c (n "yew-lucide") (v "0.140.0") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 1)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.78") (d #t) (k 1)) (d (n "yew") (r "^0.20") (d #t) (k 0)))) (h "0wjx04mmh4ih3pnr8x9sk51lq8816vnx8j5vj2281s3kzykibk8q")))

(define-public crate-yew-lucide-0.144.0 (c (n "yew-lucide") (v "0.144.0") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 1)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.78") (d #t) (k 1)) (d (n "yew") (r "^0.20") (d #t) (k 0)))) (h "1h66ndy2knx7492mpas92pgr9x8lal0pfnk3p7kbxcrkbhxvny8x")))

(define-public crate-yew-lucide-0.145.0 (c (n "yew-lucide") (v "0.145.0") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 1)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.78") (d #t) (k 1)) (d (n "yew") (r "^0.20") (d #t) (k 0)))) (h "0a851a8xg73s9p4qnqix9pnxk0c0nnc1312s6vq7x41yv515c0j4")))

(define-public crate-yew-lucide-0.161.0 (c (n "yew-lucide") (v "0.161.0") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 1)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.78") (d #t) (k 1)) (d (n "yew") (r "^0.20") (d #t) (k 0)))) (h "07lfz40bx30rhv7ivfa8z530n99lm0jjc72108qgmfp23zsj77h8")))

(define-public crate-yew-lucide-0.162.0 (c (n "yew-lucide") (v "0.162.0") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 1)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.78") (d #t) (k 1)) (d (n "yew") (r "^0.20") (d #t) (k 0)))) (h "07b6zv8f0vwhlqjh7fxmshdpcw817iar09cr31izrjmy1356ab3l")))

(define-public crate-yew-lucide-0.166.0 (c (n "yew-lucide") (v "0.166.0") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 1)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.78") (d #t) (k 1)) (d (n "yew") (r "^0.20") (d #t) (k 0)))) (h "039p6dzrifg4348hxpm91hzfsci7x40k8azd03ayfr7bk0akxqz8")))

(define-public crate-yew-lucide-0.171.0 (c (n "yew-lucide") (v "0.171.0") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 1)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.78") (d #t) (k 1)) (d (n "yew") (r "^0.20") (d #t) (k 0)))) (h "1rgrsjijil22kyry8b6ddd5phslxkbjvhn7qp8072085x2v6m0zv")))

(define-public crate-yew-lucide-0.172.0 (c (n "yew-lucide") (v "0.172.0") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 1)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.78") (d #t) (k 1)) (d (n "yew") (r "^0.20") (d #t) (k 0)))) (h "0p0kx119zmav4yh6ishkd3rvslc12wj4xks8ny9gchi8xg8hfvk9")))

(define-public crate-yew-lucide-0.263.0 (c (n "yew-lucide") (v "0.263.0") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 1)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.78") (d #t) (k 1)) (d (n "yew") (r "^0.20") (d #t) (k 0)))) (h "0niy0a2arjhal29camb9acw6205bah3s5878z91w8zpzdb1hd91i")))

(define-public crate-yew-lucide-0.268.0 (c (n "yew-lucide") (v "0.268.0") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 1)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.78") (d #t) (k 1)) (d (n "yew") (r "^0.20") (d #t) (k 0)))) (h "1hncksk4z2w4rplwda2i6cjb6nircjlsmxl0vbfsmccdjhjd3dkb")))

(define-public crate-yew-lucide-0.269.0 (c (n "yew-lucide") (v "0.269.0") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 1)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.78") (d #t) (k 1)) (d (n "yew") (r "^0.20") (d #t) (k 0)))) (h "128qs0rswm0kpkdrqk4c903b4jbrxdwxr9x5whm6lx21yis1bh1g")))

(define-public crate-yew-lucide-0.270.0 (c (n "yew-lucide") (v "0.270.0") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 1)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.78") (d #t) (k 1)) (d (n "yew") (r "^0.20") (d #t) (k 0)))) (h "1gh4w7mlh32myrff30fmpgnqhrg1dlz10z38yp619pka064ybpkn")))

(define-public crate-yew-lucide-0.271.0 (c (n "yew-lucide") (v "0.271.0") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 1)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.78") (d #t) (k 1)) (d (n "yew") (r "^0.20") (d #t) (k 0)))) (h "0ksyyvijh7smdphswwwh19f5rfzmjylh6b501b9xaz8y8ri5niiz")))

(define-public crate-yew-lucide-0.272.0 (c (n "yew-lucide") (v "0.272.0") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 1)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.78") (d #t) (k 1)) (d (n "yew") (r "^0.20") (d #t) (k 0)))) (h "1dvmnx7kp0kl6pjh50z1b3igbbb35b454sdkxi7xfn3p1khqi21s")))

(define-public crate-yew-lucide-0.274.0 (c (n "yew-lucide") (v "0.274.0") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 1)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.78") (d #t) (k 1)) (d (n "yew") (r "^0.20") (d #t) (k 0)))) (h "17y1p2g4wp91bjyplpvgal1kvcp7bxsy9nrqjvrx1smzh9b0kicb")))

(define-public crate-yew-lucide-0.276.0 (c (n "yew-lucide") (v "0.276.0") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 1)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.78") (d #t) (k 1)) (d (n "yew") (r "^0.20") (d #t) (k 0)))) (h "1jr7m5m9b3d3kb51zbfg1ikgdifan4635k6yrz2qzjx1ymsabz6i")))

(define-public crate-yew-lucide-0.277.0 (c (n "yew-lucide") (v "0.277.0") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 1)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.78") (d #t) (k 1)) (d (n "yew") (r "^0.20") (d #t) (k 0)))) (h "0p34dladhqlrgiiakmzfi0jzq45f3gayal6wn0laknnmq4g80zz7")))

(define-public crate-yew-lucide-0.279.0 (c (n "yew-lucide") (v "0.279.0") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 1)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.78") (d #t) (k 1)) (d (n "yew") (r "^0.20") (d #t) (k 0)))) (h "1gwgdmf0k2vj1xv982hbrb3bb4b8mjg3pvdi1xxc1yb9949k8air")))

(define-public crate-yew-lucide-0.282.0 (c (n "yew-lucide") (v "0.282.0") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 1)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.78") (d #t) (k 1)) (d (n "yew") (r "^0.20") (d #t) (k 0)))) (h "0mi61ak0z7w4ijwiwm6cmcmb1s6b2gmsjnjh5ivjchpcdsr5qmlr")))

(define-public crate-yew-lucide-0.284.0 (c (n "yew-lucide") (v "0.284.0") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 1)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.78") (d #t) (k 1)) (d (n "yew") (r "^0.20") (d #t) (k 0)))) (h "1wrvbgxzh9vvyj5bi081m7zn1g4i25c4xx8lpq06kndq7xpza8kr")))

(define-public crate-yew-lucide-0.285.0 (c (n "yew-lucide") (v "0.285.0") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 1)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.78") (d #t) (k 1)) (d (n "yew") (r "^0.20") (d #t) (k 0)))) (h "1yqgjdk6fi9p9w1y98b40n02zma22n4zikif45dh4fan629pd5fz")))

(define-public crate-yew-lucide-0.286.0 (c (n "yew-lucide") (v "0.286.0") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 1)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.78") (d #t) (k 1)) (d (n "yew") (r "^0.20") (d #t) (k 0)))) (h "0ykg03rmgba98wfdssqfgcia4zpxziw5nfrgzwh82chy2h7avwcs")))

(define-public crate-yew-lucide-0.287.0 (c (n "yew-lucide") (v "0.287.0") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 1)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.78") (d #t) (k 1)) (d (n "yew") (r "^0.20") (d #t) (k 0)))) (h "0bwcyph8nlbvp2cdgz77a09v2lr9gqaiq41wmjmwpa1invjbwv19")))

(define-public crate-yew-lucide-0.288.0 (c (n "yew-lucide") (v "0.288.0") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 1)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.78") (d #t) (k 1)) (d (n "yew") (r "^0.20") (d #t) (k 0)))) (h "0rdjcgl1kwm3kspakdhid4984y997wgggays4g4f18cr0dmx3fn0")))

(define-public crate-yew-lucide-0.290.0 (c (n "yew-lucide") (v "0.290.0") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 1)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.78") (d #t) (k 1)) (d (n "yew") (r "^0.20") (d #t) (k 0)))) (h "0y8zd6pkhz12ldq1ks7zpmvmq3n6g6dkmivp53kpjic80xwrf70i")))

(define-public crate-yew-lucide-0.291.0 (c (n "yew-lucide") (v "0.291.0") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 1)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.78") (d #t) (k 1)) (d (n "yew") (r "^0.20") (d #t) (k 0)))) (h "0j5322crj2xpyznjkhvi39cc4507cy1zlqnvhwxv252hzzhak7j6")))

(define-public crate-yew-lucide-0.292.0 (c (n "yew-lucide") (v "0.292.0") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 1)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.78") (d #t) (k 1)) (d (n "yew") (r "^0.20") (d #t) (k 0)))) (h "1giydmiryd1ykfg0l9alvh6macsjsf9h3ch01bd6zsz9b3k34wmw")))

(define-public crate-yew-lucide-0.293.0 (c (n "yew-lucide") (v "0.293.0") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 1)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.78") (d #t) (k 1)) (d (n "yew") (r "^0.20") (d #t) (k 0)))) (h "19ygimcpx010p7j0ikydsp7141q7j6s31myhnqvbs3f92v6jyz4z")))

(define-public crate-yew-lucide-0.294.0 (c (n "yew-lucide") (v "0.294.0") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 1)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.78") (d #t) (k 1)) (d (n "yew") (r "^0.20") (d #t) (k 0)))) (h "18ks46nv14dy6gbl381n2n913hmnamsfixibgvgd29mfki0hdwj4")))

(define-public crate-yew-lucide-0.295.0 (c (n "yew-lucide") (v "0.295.0") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 1)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.78") (d #t) (k 1)) (d (n "yew") (r "^0.20") (d #t) (k 0)))) (h "0lmwxkwafzg3ajwgkjj76j8a21jc4rqrhi7njbwk49jbr4irr0s8")))

(define-public crate-yew-lucide-0.297.0 (c (n "yew-lucide") (v "0.297.0") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 1)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.78") (d #t) (k 1)) (d (n "yew") (r "^0.20") (d #t) (k 0)))) (h "12qz26ajr971wfjkk7jq4ib6gm3w6q4jaj0kpplivv6py3x8f3jh")))

(define-public crate-yew-lucide-0.298.0 (c (n "yew-lucide") (v "0.298.0") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 1)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.78") (d #t) (k 1)) (d (n "yew") (r "^0.20") (d #t) (k 0)))) (h "0d1114bd6d445q2fcphzfc7kq11052yxa2fk6rpb0hinylpkc2lp")))

(define-public crate-yew-lucide-0.299.0 (c (n "yew-lucide") (v "0.299.0") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 1)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.78") (d #t) (k 1)) (d (n "yew") (r "^0.20") (d #t) (k 0)))) (h "0ajpl0a3gcpmn8pyp0vz95ahk5ynd4m9s6iw7msm5ia3df2qbg59")))

(define-public crate-yew-lucide-0.300.0 (c (n "yew-lucide") (v "0.300.0") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 1)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.78") (d #t) (k 1)) (d (n "yew") (r "^0.20") (d #t) (k 0)))) (h "1swxwywgfwygy44316qvii9vy8pbghkpjpybyq0w1i5xsv0v0xv0")))

(define-public crate-yew-lucide-0.303.0 (c (n "yew-lucide") (v "0.303.0") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 1)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.78") (d #t) (k 1)) (d (n "yew") (r "^0.20") (d #t) (k 0)))) (h "09r9gspgyss6cjbsf1w7vblky6xgsbc8ka8ji9dhy4y5qzp3250q")))

(define-public crate-yew-lucide-0.304.0 (c (n "yew-lucide") (v "0.304.0") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 1)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.78") (d #t) (k 1)) (d (n "yew") (r "^0.20") (d #t) (k 0)))) (h "0vl6j848cf09idd1gm94xzyffdxxa9v15xmydvrlgchfr1vlnzx2")))

(define-public crate-yew-lucide-0.306.0 (c (n "yew-lucide") (v "0.306.0") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 1)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.78") (d #t) (k 1)) (d (n "yew") (r "^0.20") (d #t) (k 0)))) (h "1rh1w3vxvc8z4s0p5zvyng8sgrvnfblfj62qdijs1n3h6aqychkq")))

(define-public crate-yew-lucide-0.307.0 (c (n "yew-lucide") (v "0.307.0") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 1)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.78") (d #t) (k 1)) (d (n "yew") (r "^0.20") (d #t) (k 0)))) (h "1nphx1wm3yhmlnqscrf9fjg5d8009x8yrh644rvw5g4i6hhmpv3z")))

(define-public crate-yew-lucide-0.308.0 (c (n "yew-lucide") (v "0.308.0") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 1)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.78") (d #t) (k 1)) (d (n "yew") (r "^0.20") (d #t) (k 0)))) (h "055gc3gissam3pa5akpn0l86shcsjbjq1ydnvkg8qm4isi8b4yf5")))

(define-public crate-yew-lucide-0.309.0 (c (n "yew-lucide") (v "0.309.0") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 1)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.78") (d #t) (k 1)) (d (n "yew") (r "^0.20") (d #t) (k 0)))) (h "1aciw06npsph6n2cyy5rn8lbb5npsc0k00a9j5fkhn6dnp19vc8h")))

(define-public crate-yew-lucide-0.311.0 (c (n "yew-lucide") (v "0.311.0") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 1)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.78") (d #t) (k 1)) (d (n "yew") (r "^0.20") (d #t) (k 0)))) (h "19fp4n8rmrcvz1mwngid0zyyrc87xfwq5cvx229jwzfhq0748aak")))

(define-public crate-yew-lucide-0.312.0 (c (n "yew-lucide") (v "0.312.0") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 1)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.78") (d #t) (k 1)) (d (n "yew") (r "^0.20") (d #t) (k 0)))) (h "1fp8yrp8c77m4hx03p6pcxy6n6qxnjikbpszlmxr7i8bq6wg0as2")))

(define-public crate-yew-lucide-0.314.0 (c (n "yew-lucide") (v "0.314.0") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 1)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.78") (d #t) (k 1)) (d (n "yew") (r "^0.20") (d #t) (k 0)))) (h "03g85fgxd14rsk3n4rj8y4hpfwiamar1apmzmnfvf7q6z41b9w0y")))

(define-public crate-yew-lucide-0.315.0 (c (n "yew-lucide") (v "0.315.0") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 1)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.78") (d #t) (k 1)) (d (n "yew") (r "^0.20") (d #t) (k 0)))) (h "1kvqw6ivnj30nk12yksl30mqiaj169d2ln5s9g0lpp5al37s2bid")))

(define-public crate-yew-lucide-0.316.0 (c (n "yew-lucide") (v "0.316.0") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 1)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.78") (d #t) (k 1)) (d (n "yew") (r "^0.20") (d #t) (k 0)))) (h "1wi519j5wb5rdck8c027s5f4jjgwjyjlh3xnd054qqv9skr6x66b")))

(define-public crate-yew-lucide-0.317.0 (c (n "yew-lucide") (v "0.317.0") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 1)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.78") (d #t) (k 1)) (d (n "yew") (r "^0.20") (d #t) (k 0)))) (h "18dnlsk4v05wbdqdfm3s4yc2gq0fi3xm25i0w7f9paqq77f3hlrx")))

(define-public crate-yew-lucide-0.320.0 (c (n "yew-lucide") (v "0.320.0") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 1)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.78") (d #t) (k 1)) (d (n "yew") (r "^0.20") (d #t) (k 0)))) (h "0wn83p6cz0qh3cl153q7h89l9mw5y58d2b7waf6qpsh5q2qhsv1y")))

(define-public crate-yew-lucide-0.321.0 (c (n "yew-lucide") (v "0.321.0") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 1)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.78") (d #t) (k 1)) (d (n "yew") (r "^0.20") (d #t) (k 0)))) (h "12xx5x8baknawfic95mn1f4b82y3zibrbw277z55pjd29mma323i")))

(define-public crate-yew-lucide-0.323.0 (c (n "yew-lucide") (v "0.323.0") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 1)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.78") (d #t) (k 1)) (d (n "yew") (r "^0.20") (d #t) (k 0)))) (h "1rviq56p4lnl46cwdmpgyy4b00wnx1ayrga7sh2v0p9qrg82m42j")))

(define-public crate-yew-lucide-0.325.0 (c (n "yew-lucide") (v "0.325.0") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 1)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.78") (d #t) (k 1)) (d (n "yew") (r "^0.20") (d #t) (k 0)))) (h "00wmn2ynizvn3d2f87cc35dzj91dd9whkk9nj2jp1yx1i6d87hmq")))

(define-public crate-yew-lucide-0.330.0 (c (n "yew-lucide") (v "0.330.0") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 1)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.78") (d #t) (k 1)) (d (n "yew") (r "^0.20") (d #t) (k 0)))) (h "1ma6kfg4mxw6jnjc1ix0xg47hv3w3jh03lh50nix1zbi5f7gff6l")))

