(define-module (crates-io ye w- yew-linkify) #:use-module (crates-io))

(define-public crate-yew-linkify-0.0.1 (c (n "yew-linkify") (v "0.0.1") (d (list (d (n "linkify") (r "^0.9.0") (d #t) (k 0)) (d (n "yew") (r "^0.20.0") (f (quote ("csr"))) (d #t) (k 0)))) (h "1xa742ql3qsbrajmjsqjfy3c3alqnmc0c4xkm3qkb9bwr32yfksl")))

