(define-module (crates-io ye w- yew-custom-components) #:use-module (crates-io))

(define-public crate-yew-custom-components-0.1.0 (c (n "yew-custom-components") (v "0.1.0") (d (list (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde-value") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "yew") (r "^0.21.0") (f (quote ("csr"))) (d #t) (k 0)))) (h "01qzcnhyzgpfqmlbaxbrh7ci33ir7l2klrvbxkgwrisxp5lpbqi6") (f (quote (("tabs")))) (s 2) (e (quote (("table" "dep:serde" "dep:serde-value"))))))

(define-public crate-yew-custom-components-0.2.1 (c (n "yew-custom-components") (v "0.2.1") (d (list (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde-value") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "yew") (r "^0.21.0") (f (quote ("csr"))) (d #t) (k 0)))) (h "1v8x8g64f0ddnqifjchgb5vs07f5rp0hnljzx787wmnn5gk796p5") (f (quote (("tabs") ("pagination")))) (s 2) (e (quote (("table" "dep:serde" "dep:serde-value"))))))

(define-public crate-yew-custom-components-0.2.2 (c (n "yew-custom-components") (v "0.2.2") (d (list (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde-value") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "yew") (r "^0.21.0") (f (quote ("csr"))) (d #t) (k 0)))) (h "036l2ldkqaiwrn9j0hdmpfl0sady213nm89b8wqsx4javjlv7wjf") (f (quote (("tabs") ("pagination")))) (s 2) (e (quote (("table" "dep:serde" "dep:serde-value"))))))

