(define-module (crates-io ye w- yew-os) #:use-module (crates-io))

(define-public crate-yew-os-0.1.0 (c (n "yew-os") (v "0.1.0") (d (list (d (n "web-sys") (r "^0.3.61") (f (quote ("Navigator" "Window"))) (d #t) (k 0)) (d (n "yew") (r "^0.20.0") (f (quote ("csr"))) (d #t) (k 0)))) (h "1af3jgb83f76p5cqc6a3jvsqpsam4468l6gkhg4pgr4jp912prg3")))

(define-public crate-yew-os-0.1.1 (c (n "yew-os") (v "0.1.1") (d (list (d (n "web-sys") (r "^0.3.61") (f (quote ("Navigator" "Window"))) (d #t) (k 0)) (d (n "yew") (r "^0.20.0") (f (quote ("csr"))) (d #t) (k 0)))) (h "1q7lim52axrvnf63fciyn4pymi752plij7188f183yh4v7qsczda")))

(define-public crate-yew-os-0.2.1 (c (n "yew-os") (v "0.2.1") (d (list (d (n "web-sys") (r "^0.3.61") (f (quote ("Navigator" "Window"))) (d #t) (k 0)) (d (n "yew") (r "^0.20.0") (f (quote ("csr"))) (d #t) (k 0)))) (h "1ayxkmwrr820xss3i8ja2rblzwvdszz6zfn5d01p5l02i3bj95d2")))

(define-public crate-yew-os-0.3.1 (c (n "yew-os") (v "0.3.1") (d (list (d (n "web-sys") (r "^0.3.61") (f (quote ("Navigator" "Window"))) (d #t) (k 0)) (d (n "yew") (r "^0.20.0") (f (quote ("csr"))) (d #t) (k 0)))) (h "0a7vw84cwjsjbsdhc0q4x37xsinp6zl7rpin86kss4kwzxv6sxw7")))

(define-public crate-yew-os-0.3.2 (c (n "yew-os") (v "0.3.2") (d (list (d (n "web-sys") (r "^0.3.61") (f (quote ("Navigator" "Window"))) (d #t) (k 0)) (d (n "yew") (r "^0.20.0") (f (quote ("csr"))) (d #t) (k 0)))) (h "1qdw8kg4b3253wr4vfb3hmrbmp74yya234pq3a72mprn2acskx16")))

