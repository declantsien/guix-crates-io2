(define-module (crates-io ye w- yew-octicons) #:use-module (crates-io))

(define-public crate-yew-octicons-0.1.0 (c (n "yew-octicons") (v "0.1.0") (d (list (d (n "heck") (r "^0.3.1") (d #t) (k 1)) (d (n "proc-macro2") (r "^1.0.18") (d #t) (k 1)) (d (n "quote") (r "^1.0.7") (d #t) (k 1)) (d (n "scraper") (r "^0.12.0") (d #t) (k 1)) (d (n "yew") (r "^0.17.2") (d #t) (k 0)))) (h "0bm8zgjkjd1npwfgy6pmziip10s0gj8wm598r7y7ixmndk9ld4h5")))

(define-public crate-yew-octicons-0.1.1 (c (n "yew-octicons") (v "0.1.1") (d (list (d (n "heck") (r "^0.3.1") (d #t) (k 1)) (d (n "proc-macro2") (r "^1.0.18") (d #t) (k 1)) (d (n "quote") (r "^1.0.7") (d #t) (k 1)) (d (n "scraper") (r "^0.12.0") (d #t) (k 1)) (d (n "yew") (r "^0.17.2") (d #t) (k 0)))) (h "0qhvdk0558qly55y651a6v646q2qcqsq76aqbb387bd0hk8pl1si")))

(define-public crate-yew-octicons-0.2.0 (c (n "yew-octicons") (v "0.2.0") (d (list (d (n "heck") (r "^0.3.2") (d #t) (k 1)) (d (n "proc-macro2") (r "^1.0.26") (d #t) (k 1)) (d (n "quote") (r "^1.0.9") (d #t) (k 1)) (d (n "scraper") (r "^0.12.0") (d #t) (k 1)) (d (n "yew") (r "^0.18.0") (d #t) (k 0)))) (h "1m1pabnwmzwjv8s7zlnd5v222ak19krp6qdn42rslbq0xcl5lln3")))

(define-public crate-yew-octicons-0.3.1 (c (n "yew-octicons") (v "0.3.1") (d (list (d (n "heck") (r "^0.4.0") (d #t) (k 1)) (d (n "proc-macro2") (r "^1.0.33") (d #t) (k 1)) (d (n "quote") (r "^1.0.10") (d #t) (k 1)) (d (n "scraper") (r "^0.12.0") (d #t) (k 1)) (d (n "yew") (r "^0.19.3") (d #t) (k 0)))) (h "1jgsb8abwr4vnjy3rvxprgaj2f56xv1pp5kg5ha79wa7zj9lkrhw")))

(define-public crate-yew-octicons-0.4.0 (c (n "yew-octicons") (v "0.4.0") (d (list (d (n "yew") (r "^0.20.0") (d #t) (k 0)) (d (n "heck") (r "^0.4.1") (d #t) (k 1)) (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 1)) (d (n "quote") (r "^1.0.23") (d #t) (k 1)) (d (n "scraper") (r "^0.14.0") (d #t) (k 1)))) (h "04h5rzd1jyykk7x3mqqyfkcsgy0zh9wjzp5pl622iqrvpx2q87wg")))

