(define-module (crates-io ye w- yew-katex) #:use-module (crates-io))

(define-public crate-yew-katex-0.1.0 (c (n "yew-katex") (v "0.1.0") (d (list (d (n "katex-wasmbind") (r "^0.6") (d #t) (k 0)) (d (n "yew") (r "^0.17") (d #t) (k 0)))) (h "1f2ip69k1dgp6v356x55ak9h8j3l5qgny04qf51jndk2d5ippvff") (f (quote (("default"))))))

(define-public crate-yew-katex-0.2.0 (c (n "yew-katex") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "katex-wasmbind") (r "^0.8") (d #t) (k 0)) (d (n "yew") (r "^0.17") (d #t) (k 0)))) (h "0w6pzm25r1rxjwa9yj28h6varyvb03rlc3285gcqjsjlmcycf12s") (f (quote (("default" "auto-cdn") ("auto-cdn"))))))

(define-public crate-yew-katex-0.19.0 (c (n "yew-katex") (v "0.19.0") (d (list (d (n "gloo-utils") (r "^0.1.5") (d #t) (k 0)) (d (n "katex-wasmbind") (r "^0.10.0") (d #t) (k 0)) (d (n "yew") (r "^0.19.3") (d #t) (k 0)))) (h "0y1zp46cwalaq1asrbsljm0xq2ab5i3r99q9f8qdd9v02mlminrn") (f (quote (("default" "auto-cdn") ("auto-cdn"))))))

