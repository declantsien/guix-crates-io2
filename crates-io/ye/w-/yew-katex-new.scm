(define-module (crates-io ye w- yew-katex-new) #:use-module (crates-io))

(define-public crate-yew-katex-new-0.19.0 (c (n "yew-katex-new") (v "0.19.0") (d (list (d (n "gloo-utils") (r "^0.1.5") (d #t) (k 0)) (d (n "katex-wasmbind") (r "^0.10.0") (d #t) (k 0)) (d (n "yew") (r "~0.20.0") (d #t) (k 0)))) (h "15xx4kcsjgldzcka5fkpv6xaj5z513izvgij9897qqjmwh5icqjv") (f (quote (("default" "auto-cdn") ("auto-cdn"))))))

