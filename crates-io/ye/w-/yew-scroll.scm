(define-module (crates-io ye w- yew-scroll) #:use-module (crates-io))

(define-public crate-yew-scroll-0.1.0 (c (n "yew-scroll") (v "0.1.0") (d (list (d (n "gloo") (r "^0.11.0") (f (quote ("utils"))) (d #t) (k 0)) (d (n "yew") (r "^0.21.0") (k 0)))) (h "1sky0vz3s7jywmss1fsvvjlv8aaz0g8s7irc61qssph7hihwxzb4")))

