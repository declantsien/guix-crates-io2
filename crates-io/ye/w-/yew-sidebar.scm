(define-module (crates-io ye w- yew-sidebar) #:use-module (crates-io))

(define-public crate-yew-sidebar-0.1.0 (c (n "yew-sidebar") (v "0.1.0") (d (list (d (n "yew") (r "^0.21.0") (f (quote ("csr"))) (d #t) (k 0)))) (h "07ycf8iisyb2pkzjf5czll91cqs0jmmhgmn3ji8kzachb8pxvrvq")))

(define-public crate-yew-sidebar-0.1.1 (c (n "yew-sidebar") (v "0.1.1") (d (list (d (n "yew") (r "^0.21.0") (f (quote ("csr"))) (d #t) (k 0)))) (h "16qr1rdlz9sdlm662wkxwiln6bzfj6jjjwa2b8vchr0qwdk4w2mc")))

(define-public crate-yew-sidebar-0.1.2 (c (n "yew-sidebar") (v "0.1.2") (d (list (d (n "yew") (r "^0.21.0") (f (quote ("csr"))) (d #t) (k 0)) (d (n "yew-accordion") (r "^0.1.0") (d #t) (k 0)))) (h "1x2s8kvsadgvh5a1wd4370wgpy50isgrc7j61l31p6srgrmvri4w")))

(define-public crate-yew-sidebar-0.1.3 (c (n "yew-sidebar") (v "0.1.3") (d (list (d (n "yew") (r "^0.21.0") (f (quote ("csr"))) (d #t) (k 0)) (d (n "yew-accordion") (r "^0.1.0") (d #t) (k 0)))) (h "00azm2wd40i17aj8wdrjmi5kh9dsm8wmv01g5kpspbr8hg589rq9")))

(define-public crate-yew-sidebar-0.1.4 (c (n "yew-sidebar") (v "0.1.4") (d (list (d (n "yew") (r "^0.21.0") (d #t) (k 0)) (d (n "yew-accordion") (r "^0.1.0") (d #t) (k 0)))) (h "19bf9fwk06r469ph48yp8il81ia3g3g7mhmilq8m541jcdd2r27g")))

