(define-module (crates-io ye w- yew-bootstrap-v3) #:use-module (crates-io))

(define-public crate-yew-bootstrap-v3-0.1.0 (c (n "yew-bootstrap-v3") (v "0.1.0") (d (list (d (n "yew") (r "^0.20.0") (f (quote ("csr"))) (d #t) (k 0)))) (h "11k37zgrq0mkw67c367bjbaag5ky2p1d61a1l8bz9cawwcybdiwl") (y #t) (r "1.63.0")))

(define-public crate-yew-bootstrap-v3-0.1.13 (c (n "yew-bootstrap-v3") (v "0.1.13") (d (list (d (n "yew") (r "^0.20.0") (f (quote ("csr"))) (d #t) (k 0)))) (h "0bbpybksmpprf9mndr9hzq0r52n65b19v6fbicclx1cvsacqwvjq") (y #t) (r "1.63.0")))

