(define-module (crates-io ye w- yew-router-min-macro) #:use-module (crates-io))

(define-public crate-yew-router-min-macro-0.8.0 (c (n "yew-router-min-macro") (v "0.8.0") (d (list (d (n "proc-macro2") (r "^1.0.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.1") (d #t) (k 0)) (d (n "syn") (r "^1.0.2") (d #t) (k 0)) (d (n "yew-router-route-parser") (r "^0.8.0") (d #t) (k 0) (p "yew-router-min-route-parser")))) (h "15fd3bcc0ghj9dnw09bkg4h0ij4hb8fdkdiyh7vn3amn2r0prksh")))

