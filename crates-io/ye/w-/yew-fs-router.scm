(define-module (crates-io ye w- yew-fs-router) #:use-module (crates-io))

(define-public crate-yew-fs-router-0.1.0 (c (n "yew-fs-router") (v "0.1.0") (d (list (d (n "yew-fs-router-macro") (r "^0.1.0") (d #t) (k 0)))) (h "1q6xvbw6zzs5bwy14n8iapjwmksgsp4icrifq78dgdinrbs0hp9b")))

(define-public crate-yew-fs-router-0.2.0 (c (n "yew-fs-router") (v "0.2.0") (d (list (d (n "yew-fs-router-macro") (r "^0.2.0") (d #t) (k 0)))) (h "0y9w52xsi4xh88yxkjm44990ys47ppn0vniia8lpx8pgi0062cws")))

(define-public crate-yew-fs-router-0.3.0 (c (n "yew-fs-router") (v "0.3.0") (d (list (d (n "yew") (r "^0.19") (d #t) (k 0)) (d (n "yew-fs-router-macro") (r "^0.3.0") (d #t) (k 0)))) (h "15l4a83w6l32vw66wbff6r1i7cn7in4xxdgw9g5sq8vd4wbv0ynd")))

(define-public crate-yew-fs-router-0.4.0 (c (n "yew-fs-router") (v "0.4.0") (d (list (d (n "yew") (r "^0.19") (d #t) (k 0)) (d (n "yew-fs-router-macro") (r "^0.4.0") (d #t) (k 0)))) (h "0ax7wkdbpfz5dayimw7p2a6mi38dc7q3567ir1lb7qjxdsbwk43k")))

