(define-module (crates-io ye w- yew-transition-group) #:use-module (crates-io))

(define-public crate-yew-transition-group-0.0.1 (c (n "yew-transition-group") (v "0.0.1") (d (list (d (n "gloo-timers") (r "^0.2") (f (quote ("futures"))) (d #t) (k 0)) (d (n "yew") (r "^0.19") (d #t) (k 0)))) (h "123gp06bii5mfjj5736mwkknxma3r4nqn8lmg810d2ri7zscibb5")))

