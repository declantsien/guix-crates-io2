(define-module (crates-io ye w- yew-state) #:use-module (crates-io))

(define-public crate-yew-state-0.1.0 (c (n "yew-state") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.114") (f (quote ("rc"))) (d #t) (k 0)) (d (n "yew") (r "^0.17") (d #t) (k 0)))) (h "1q8ny7vcr2g7b6c4iggcbb530plmaw236si8x1479rhnflzp6z6s")))

(define-public crate-yew-state-0.2.0 (c (n "yew-state") (v "0.2.0") (d (list (d (n "serde") (r "^1.0.114") (f (quote ("rc"))) (d #t) (k 0)) (d (n "yew") (r "^0.17") (d #t) (k 0)))) (h "093mjbl8263shr1dik5hi108dnq7cblkaiq6d513nbn69fci1a7q")))

(define-public crate-yew-state-0.2.1 (c (n "yew-state") (v "0.2.1") (d (list (d (n "serde") (r "^1.0.114") (f (quote ("rc"))) (d #t) (k 0)) (d (n "yew") (r "^0.17") (d #t) (k 0)))) (h "08h1gzfkrv2vy91xiny8swixcyqxxiwf1viav1582wlzddsiq0bl")))

(define-public crate-yew-state-0.2.2 (c (n "yew-state") (v "0.2.2") (d (list (d (n "serde") (r "^1.0.114") (f (quote ("rc"))) (d #t) (k 0)) (d (n "yew") (r "^0.17") (d #t) (k 0)))) (h "06kg35wr22arfnb62s30ximvmv7mplcvv7fiyzkxmc19v9h8ax83")))

(define-public crate-yew-state-0.2.3 (c (n "yew-state") (v "0.2.3") (d (list (d (n "serde") (r "^1.0.114") (f (quote ("rc"))) (d #t) (k 0)) (d (n "yew") (r "^0.17") (d #t) (k 0)))) (h "1pjddqf57s8wg0607vpf46w0mypypq89nddnf4k0gdzlvccmc7in")))

(define-public crate-yew-state-0.2.4 (c (n "yew-state") (v "0.2.4") (d (list (d (n "serde") (r "^1.0.114") (f (quote ("rc"))) (d #t) (k 0)) (d (n "yew") (r "^0.17") (d #t) (k 0)))) (h "1gywr1anwzg1ppn8p0j5wg0y7h7d1iav6x33yx6gxp6jc9yy325z")))

(define-public crate-yew-state-0.3.0 (c (n "yew-state") (v "0.3.0") (d (list (d (n "serde") (r "^1.0.114") (f (quote ("rc"))) (d #t) (k 0)) (d (n "yew") (r "^0.17") (d #t) (k 0)))) (h "12iybyfrlzmzdkdqmp450kvr92kk6m02g4358hdi0lz44jsswlf6")))

(define-public crate-yew-state-0.4.0 (c (n "yew-state") (v "0.4.0") (d (list (d (n "serde") (r "^1.0.114") (f (quote ("rc"))) (d #t) (k 0)) (d (n "yew") (r "^0.17") (d #t) (k 0)))) (h "0gx1lyrgpmrmi8ddpvrswia52c3489ms8d4hgm9jk778fmqvmb0a")))

(define-public crate-yew-state-0.5.0 (c (n "yew-state") (v "0.5.0") (d (list (d (n "serde") (r "^1.0.114") (f (quote ("rc"))) (d #t) (k 0)) (d (n "yew") (r "^0.17") (d #t) (k 0)) (d (n "yewtil") (r "^0.3.2") (o #t) (d #t) (k 0)))) (h "1zdwcr3d829mcrhassbbx6d8qvng9s3hrb365vkmb216n8fg1xs6") (f (quote (("future" "yewtil") ("default"))))))

(define-public crate-yew-state-0.5.1 (c (n "yew-state") (v "0.5.1") (d (list (d (n "serde") (r "^1.0.114") (f (quote ("rc"))) (d #t) (k 0)) (d (n "yew") (r "^0.17") (d #t) (k 0)) (d (n "yewtil") (r "^0.3.2") (o #t) (d #t) (k 0)))) (h "1yfgmxni512macn77ly49rka3jkhaqy8m5aq6qc4nsinnwv7rimb") (f (quote (("future" "yewtil") ("default"))))))

(define-public crate-yew-state-0.5.2 (c (n "yew-state") (v "0.5.2") (d (list (d (n "serde") (r "^1.0.114") (f (quote ("rc"))) (d #t) (k 0)) (d (n "yew") (r "^0.17") (d #t) (k 0)) (d (n "yewtil") (r "^0.3.2") (o #t) (d #t) (k 0)))) (h "055f97bwg0q4vqg3q2sj1n3xwk380y5f0c31jvpa3d9r3vnsmcgj") (f (quote (("future" "yewtil") ("default"))))))

