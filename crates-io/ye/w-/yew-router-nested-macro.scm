(define-module (crates-io ye w- yew-router-nested-macro) #:use-module (crates-io))

(define-public crate-yew-router-nested-macro-0.16.0 (c (n "yew-router-nested-macro") (v "0.16.0") (d (list (d (n "proc-macro2") (r "^1.0.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.1") (d #t) (k 0)) (d (n "syn") (r "^1.0.2") (d #t) (k 0)) (d (n "yew-router-route-parser") (r "^0.16.0") (d #t) (k 0) (p "yew-router-nested-route-parser")))) (h "0x98cqp6f221xj3qmqkrr1clc3m5vdivfycfxm8ri04n97q0177s")))

(define-public crate-yew-router-nested-macro-0.16.1 (c (n "yew-router-nested-macro") (v "0.16.1") (d (list (d (n "proc-macro2") (r "^1.0.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.1") (d #t) (k 0)) (d (n "syn") (r "^1.0.2") (d #t) (k 0)) (d (n "yew-router-route-parser") (r "^0.16.0") (d #t) (k 0) (p "yew-router-nested-route-parser")))) (h "1hif1gg08wqx954g9lql5jdv3acz6c9hqz4vrmjj3bmjgr3jq387")))

