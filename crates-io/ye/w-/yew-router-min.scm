(define-module (crates-io ye w- yew-router-min) #:use-module (crates-io))

(define-public crate-yew-router-min-0.8.0 (c (n "yew-router-min") (v "0.8.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^5.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "yew-router-macro") (r "^0.8.0") (d #t) (k 0) (p "yew-router-min-macro")) (d (n "yew-router-route-parser") (r "^0.8.0") (d #t) (k 0) (p "yew-router-min-route-parser")))) (h "1hrh53z9aw8kipap14ygxqw83xy94m6g0i31s01kwh133zcqbk6m")))

