(define-module (crates-io ye w- yew-route-breadcrumbs) #:use-module (crates-io))

(define-public crate-yew-route-breadcrumbs-0.1.0 (c (n "yew-route-breadcrumbs") (v "0.1.0") (d (list (d (n "yew-route-breadcrumbs-derive") (r "^0.1.1") (d #t) (k 0)))) (h "1hm355rrvynjf5230mn3rr6ckz2a4hgwsqjjdmjgad6a6wz50ij0")))

(define-public crate-yew-route-breadcrumbs-0.1.2 (c (n "yew-route-breadcrumbs") (v "0.1.2") (d (list (d (n "yew-route-breadcrumbs-derive") (r "^0.1.2") (d #t) (k 0)))) (h "1z1nvvqi5ywa0q7vzmnz1l3mmddb8f68x66dklq128iqjr063b76")))

