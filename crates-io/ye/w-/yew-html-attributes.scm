(define-module (crates-io ye w- yew-html-attributes) #:use-module (crates-io))

(define-public crate-yew-html-attributes-0.1.0 (c (n "yew-html-attributes") (v "0.1.0") (d (list (d (n "wasm-bindgen") (r "^0.2.78") (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("HtmlElement"))) (d #t) (k 0)) (d (n "yew") (r "^0.20") (f (quote ("csr"))) (d #t) (k 0)) (d (n "yew-html-attributes-macro-derive") (r "^0.1.0") (d #t) (k 0)))) (h "17wnmy7gmy0d0ri685sk96njgj7nk5fakp2887zl4wgxp0znjz8r") (y #t)))

(define-public crate-yew-html-attributes-0.1.1 (c (n "yew-html-attributes") (v "0.1.1") (d (list (d (n "wasm-bindgen") (r "^0.2.78") (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("HtmlElement"))) (d #t) (k 0)) (d (n "yew") (r "^0.20") (f (quote ("csr"))) (d #t) (k 0)) (d (n "yew-html-attributes-macro-derive") (r "^0.1.1") (d #t) (k 0)))) (h "1mihxyn3d8bxa8va9i8h6vxr5zl0wwd36mj97isnwml3ddm8171y")))

