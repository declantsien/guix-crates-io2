(define-module (crates-io ye w- yew-route-breadcrumbs-derive) #:use-module (crates-io))

(define-public crate-yew-route-breadcrumbs-derive-0.1.0 (c (n "yew-route-breadcrumbs-derive") (v "0.1.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "00mvrnr6li4i6dq17wi5dnqg75g8xzjq6z4zzh7q9x397v23vmlk")))

(define-public crate-yew-route-breadcrumbs-derive-0.1.1 (c (n "yew-route-breadcrumbs-derive") (v "0.1.1") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "159bvj8ydadnhwrxng1xnd8f2b6780j8qfj1l0757q442615kn23")))

(define-public crate-yew-route-breadcrumbs-derive-0.1.2 (c (n "yew-route-breadcrumbs-derive") (v "0.1.2") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1bsr64y90clkzwbsfaji2dlakbkvfn4w1flfs9z81npmj5wdkaa9")))

