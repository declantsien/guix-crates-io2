(define-module (crates-io ye w- yew-side-effect) #:use-module (crates-io))

(define-public crate-yew-side-effect-0.1.0 (c (n "yew-side-effect") (v "0.1.0") (d (list (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "yew") (r "^0.18.0") (f (quote ("web_sys"))) (k 0)) (d (n "yewtil") (r "^0.4.0") (d #t) (k 0)))) (h "1yqxmsd3ir3q6yik9mlpwr9wyrkb59jpxbxj52mra5yr9lcxq16y")))

(define-public crate-yew-side-effect-0.2.0 (c (n "yew-side-effect") (v "0.2.0") (d (list (d (n "gloo-utils") (r "^0.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "yew") (r "^0.19.3") (d #t) (k 0)))) (h "1jabi8h9gwm9i9s043ph2v55jrahcw8d8fgyjmp57yf5m4h0s6y9")))

