(define-module (crates-io ye w- yew-utils) #:use-module (crates-io))

(define-public crate-yew-utils-0.1.0 (c (n "yew-utils") (v "0.1.0") (d (list (d (n "gloo-utils") (r "^0.1.3") (d #t) (k 2)) (d (n "web-sys") (r "^0.3.57") (f (quote ("HtmlSelectElement"))) (d #t) (k 0)) (d (n "yew") (r "^0.19.3") (d #t) (k 0)) (d (n "yew-router") (r "^0.16.0") (o #t) (d #t) (k 0)))) (h "0hvib5g29ib40z72lf96zma9gq19r1nkb6q4z1ff9j18q2awz1na") (f (quote (("default" "yew-router")))) (s 2) (e (quote (("yew-router" "dep:yew-router"))))))

(define-public crate-yew-utils-0.2.0 (c (n "yew-utils") (v "0.2.0") (d (list (d (n "gloo-utils") (r "^0.1.3") (d #t) (k 2)) (d (n "web-sys") (r "^0.3.57") (f (quote ("HtmlSelectElement"))) (d #t) (k 0)) (d (n "yew") (r "^0.19.3") (d #t) (k 0)) (d (n "yew-router") (r "^0.16.0") (o #t) (d #t) (k 0)))) (h "1vavqxgakwz11fz2zh4hhw41hr8qlc4n05vn3rgm1jwy3kawk9wp") (f (quote (("mui-css") ("default")))) (s 2) (e (quote (("yew-router" "dep:yew-router"))))))

(define-public crate-yew-utils-0.3.0 (c (n "yew-utils") (v "0.3.0") (d (list (d (n "web-sys") (r "^0.3.57") (f (quote ("HtmlSelectElement"))) (d #t) (k 0)) (d (n "yew") (r "^0.20") (d #t) (k 0)) (d (n "yew-router") (r "^0.17.0") (o #t) (d #t) (k 0)) (d (n "gloo-utils") (r "^0.1.3") (d #t) (k 2)) (d (n "yew") (r "^0.20.0") (f (quote ("csr"))) (d #t) (k 2)))) (h "0q0vybx5zh786b7fg5iw96c3gchqgp3wl5n4gfl28cvyxqhz90pd") (f (quote (("mui-css") ("default")))) (s 2) (e (quote (("yew-router" "dep:yew-router"))))))

