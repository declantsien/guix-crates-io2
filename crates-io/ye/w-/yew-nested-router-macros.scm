(define-module (crates-io ye w- yew-nested-router-macros) #:use-module (crates-io))

(define-public crate-yew-nested-router-macros-0.0.1 (c (n "yew-nested-router-macros") (v "0.0.1") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1h3id0xqfzpspsc03qi9qzysvan2skgclkv9iz4ql9bb8vahfad5")))

(define-public crate-yew-nested-router-macros-0.0.2 (c (n "yew-nested-router-macros") (v "0.0.2") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "10ays8qbbzlxy79hh8yvy638f2s1fx5v8v36d7wrs5q594wxc9my")))

(define-public crate-yew-nested-router-macros-0.0.3 (c (n "yew-nested-router-macros") (v "0.0.3") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0ah5xlkzjyswbwsz2j7b5m3pqfrkvpar5hfc9crisymccfxcv8ya")))

(define-public crate-yew-nested-router-macros-0.1.0 (c (n "yew-nested-router-macros") (v "0.1.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0w60f85j14c76zf0n6vw0sv9532p4fq9ic3j2b62hn29rqy2y5p3")))

(define-public crate-yew-nested-router-macros-0.1.2 (c (n "yew-nested-router-macros") (v "0.1.2") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1c1vria5fgmm9arbprjwc9qj6yj4qphq400b5dgrxiz2y2fx0ay7")))

(define-public crate-yew-nested-router-macros-0.2.0 (c (n "yew-nested-router-macros") (v "0.2.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1ng48kll9l2f3ad254gch89n4vrpfnxcqyrzfn60i8hb2pvpfhdp")))

(define-public crate-yew-nested-router-macros-0.2.1 (c (n "yew-nested-router-macros") (v "0.2.1") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1ckdpyq3pa6q4rkfdpjsfdrkfvgxrgffzqvp7h6f5waxjdan7mql")))

(define-public crate-yew-nested-router-macros-0.2.2 (c (n "yew-nested-router-macros") (v "0.2.2") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1dspfrchfwmpjqzsaafw832qxl61rq4v9fxvny1dygij2s6mlvi7")))

(define-public crate-yew-nested-router-macros-0.3.0 (c (n "yew-nested-router-macros") (v "0.3.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0jnr1szx9k7gq4ypv8jgyg43wvzvkzxzl1765qvdyk7av7gby1yn")))

(define-public crate-yew-nested-router-macros-0.4.0 (c (n "yew-nested-router-macros") (v "0.4.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1zz270icq6hg98w3b8wdb4lgblw52kbvbk9wfh9i2sfld2lbwkzm")))

(define-public crate-yew-nested-router-macros-0.5.0 (c (n "yew-nested-router-macros") (v "0.5.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1ipz9nyvz7hfq7bmg7sakag07a4ik5v91cns0ik2za135jcl6nyl")))

(define-public crate-yew-nested-router-macros-0.6.1 (c (n "yew-nested-router-macros") (v "0.6.1") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "187rc5n0zhygyjmkql1h06h2m86iz3qxcjwyjn61j20vfvrhx1cy")))

(define-public crate-yew-nested-router-macros-0.6.2 (c (n "yew-nested-router-macros") (v "0.6.2") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "17828aarp7ariww3wkj0d3mw2nkw98pv1m944wgc0wn1pwr2ib2s")))

(define-public crate-yew-nested-router-macros-0.6.3 (c (n "yew-nested-router-macros") (v "0.6.3") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0bbwks6n8fnlps0bifhr0hp8m1z3i6jrjjic2g2vph7rkjvdanq7")))

(define-public crate-yew-nested-router-macros-0.7.0-alpha.1 (c (n "yew-nested-router-macros") (v "0.7.0-alpha.1") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1hzj7mbf817k106ndql9qwaq2mkrkdclc3vf3i8s0fbdrfsj17sc")))

(define-public crate-yew-nested-router-macros-0.7.0 (c (n "yew-nested-router-macros") (v "0.7.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1961sv9wqb3rjc81ps4mpkvgfdv0293iw68q5vm0hgd5djiyrl66")))

