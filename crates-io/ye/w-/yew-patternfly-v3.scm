(define-module (crates-io ye w- yew-patternfly-v3) #:use-module (crates-io))

(define-public crate-yew-patternfly-v3-0.1.0 (c (n "yew-patternfly-v3") (v "0.1.0") (d (list (d (n "yew") (r "^0.20.0") (f (quote ("csr"))) (d #t) (k 0)) (d (n "yew-bootstrap-v3") (r "^0.1.0") (d #t) (k 0)))) (h "17fin8j7fp6jzrlgjzvf03g5f2p3kc02bvl5j9l3zgcims5snp60") (y #t) (r "1.63.0")))

