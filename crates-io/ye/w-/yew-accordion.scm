(define-module (crates-io ye w- yew-accordion) #:use-module (crates-io))

(define-public crate-yew-accordion-0.1.0 (c (n "yew-accordion") (v "0.1.0") (d (list (d (n "yew") (r "^0.21.0") (f (quote ("csr"))) (d #t) (k 0)))) (h "1wc217q623q4cdiqzp9pcpid02cxnnyjbbzwsfs82rvwhjvapljv")))

(define-public crate-yew-accordion-0.1.1 (c (n "yew-accordion") (v "0.1.1") (d (list (d (n "yew") (r "^0.21.0") (d #t) (k 0)))) (h "1jb4hsixgmzigsfj59bm87prd1qikbgnl51gwh9jbf74gwxkynr4")))

