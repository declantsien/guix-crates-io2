(define-module (crates-io ye w- yew-bootstrap) #:use-module (crates-io))

(define-public crate-yew-bootstrap-0.1.0 (c (n "yew-bootstrap") (v "0.1.0") (d (list (d (n "yew") (r "^0.18") (d #t) (k 0)) (d (n "yewtil") (r "^0.4.0") (d #t) (k 0)))) (h "1h5w01kr3nq3dxbxi65wpj2lvg53xx5yf0iyrgy0zz003jq0w63q")))

(define-public crate-yew-bootstrap-0.1.1 (c (n "yew-bootstrap") (v "0.1.1") (d (list (d (n "yew") (r "^0.18") (d #t) (k 0)) (d (n "yewtil") (r "^0.4.0") (d #t) (k 0)))) (h "1rykvfhshxl6nd4wf4cvh3f8h13mk65cjbhylhx29402v18cikyh")))

(define-public crate-yew-bootstrap-0.2.0 (c (n "yew-bootstrap") (v "0.2.0") (d (list (d (n "yew") (r "^0.18") (d #t) (k 0)) (d (n "yewtil") (r "^0.4.0") (d #t) (k 0)))) (h "15d776hafyx1kjwc6mx8x40v4mgpppvg6gx9pd6wn7hwc3ryvq66")))

(define-public crate-yew-bootstrap-0.3.0 (c (n "yew-bootstrap") (v "0.3.0") (d (list (d (n "yew") (r "^0.18") (d #t) (k 0)) (d (n "yewtil") (r "^0.4.0") (d #t) (k 0)))) (h "09hwcpzf9g6vp5c80sddygp6qi0mlw2f5lcsza4baq8pbvc7vhjx")))

(define-public crate-yew-bootstrap-0.3.1 (c (n "yew-bootstrap") (v "0.3.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "yew") (r "^0.18") (d #t) (k 0)) (d (n "yewtil") (r "^0.4.0") (d #t) (k 0)))) (h "0xk38l77p69kmmfnb1gnw2d9gza9p7gc4n9km7606ys59xh8q63f")))

(define-public crate-yew-bootstrap-0.4.0 (c (n "yew-bootstrap") (v "0.4.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "yew") (r "^0.19") (d #t) (k 0)))) (h "1mcpcw3ksld01cvd2grwb1587kpyzy8yzlizcyrf7jg5qx8xzsbz")))

(define-public crate-yew-bootstrap-0.5.1 (c (n "yew-bootstrap") (v "0.5.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "yew") (r "^0.20") (f (quote ("csr"))) (d #t) (k 0)))) (h "1r58ll45bdf5pmjhw42dwfk883v83sk8lvkr1d1dfc1sw4y1d504")))

(define-public crate-yew-bootstrap-0.5.2 (c (n "yew-bootstrap") (v "0.5.2") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "yew") (r "^0.20") (f (quote ("csr"))) (d #t) (k 0)))) (h "1y0k5dmvs7cbpix1m9s96zr3n7s21lx8fv89sl6pzsjn7j6drj9c")))

(define-public crate-yew-bootstrap-0.5.3 (c (n "yew-bootstrap") (v "0.5.3") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "yew") (r "^0.20") (f (quote ("csr"))) (d #t) (k 0)))) (h "0cdrrjsss0xlrnmihzqxv1rpyajrcfk0ic5k1nlm17zd2kc2lc6d")))

(define-public crate-yew-bootstrap-0.5.4 (c (n "yew-bootstrap") (v "0.5.4") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "yew") (r "^0.20") (f (quote ("csr"))) (d #t) (k 0)))) (h "1vk850i5prwp87r9w2zlizd6d4j3qjnkiwbykcajcnbimcjy5pm3")))

(define-public crate-yew-bootstrap-0.5.6 (c (n "yew-bootstrap") (v "0.5.6") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "wasm-bindgen") (r "0.2.*") (d #t) (k 2)) (d (n "web-sys") (r "0.3.*") (f (quote ("HtmlTextAreaElement" "HtmlSelectElement"))) (d #t) (k 2)) (d (n "yew") (r "^0.20") (f (quote ("csr"))) (d #t) (k 0)))) (h "1gsl2kr4ql7n5an924hp1mdwpfpmbs50y9my9n773kbl4ifdvjhb")))

(define-public crate-yew-bootstrap-0.5.7 (c (n "yew-bootstrap") (v "0.5.7") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "wasm-bindgen") (r "0.2.*") (d #t) (k 2)) (d (n "web-sys") (r "0.3.*") (f (quote ("HtmlTextAreaElement" "HtmlSelectElement"))) (d #t) (k 2)) (d (n "yew") (r "^0.20") (f (quote ("csr"))) (d #t) (k 0)))) (h "0liicfgjfip0mp322cflagn24khskb7z9zplpqms6pqylfzwjxhj")))

(define-public crate-yew-bootstrap-0.5.8 (c (n "yew-bootstrap") (v "0.5.8") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "wasm-bindgen") (r "0.2.*") (d #t) (k 2)) (d (n "web-sys") (r "0.3.*") (f (quote ("HtmlTextAreaElement" "HtmlSelectElement"))) (d #t) (k 2)) (d (n "yew") (r "^0.20") (f (quote ("csr"))) (d #t) (k 0)))) (h "08hvas4pchf2lghh1j86ny73xiqmggn7sr7wyjk4l44pfj41jhv8")))

(define-public crate-yew-bootstrap-0.5.10 (c (n "yew-bootstrap") (v "0.5.10") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "wasm-bindgen") (r "0.2.*") (d #t) (k 2)) (d (n "web-sys") (r "0.3.*") (f (quote ("HtmlTextAreaElement" "HtmlSelectElement"))) (d #t) (k 2)) (d (n "yew") (r "^0.20") (f (quote ("csr"))) (d #t) (k 0)))) (h "044ydynr308i0higs73yic08lf6z2nqwnh65spap6kjbzbh1rsjd")))

(define-public crate-yew-bootstrap-0.5.11 (c (n "yew-bootstrap") (v "0.5.11") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "wasm-bindgen") (r "0.2.*") (d #t) (k 2)) (d (n "web-sys") (r "0.3.*") (f (quote ("HtmlTextAreaElement" "HtmlSelectElement"))) (d #t) (k 2)) (d (n "yew") (r "^0.20") (f (quote ("csr"))) (d #t) (k 0)))) (h "01xablwbs76rfx8mb576ib6317cw7w7qvp6f5gw40dzwh4j11gh4")))

(define-public crate-yew-bootstrap-0.5.12 (c (n "yew-bootstrap") (v "0.5.12") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "wasm-bindgen") (r "0.2.*") (d #t) (k 2)) (d (n "web-sys") (r "0.3.*") (f (quote ("HtmlTextAreaElement" "HtmlSelectElement"))) (d #t) (k 2)) (d (n "yew") (r "^0.20") (f (quote ("csr"))) (d #t) (k 0)))) (h "1mnk7nrk149by6a6ijrcw3x96ny5bk84cjv2bx2vafwx4f8q5rlr") (y #t)))

(define-public crate-yew-bootstrap-0.5.13 (c (n "yew-bootstrap") (v "0.5.13") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "wasm-bindgen") (r "0.2.*") (d #t) (k 2)) (d (n "web-sys") (r "0.3.*") (f (quote ("HtmlTextAreaElement" "HtmlSelectElement"))) (d #t) (k 2)) (d (n "yew") (r "^0.20") (f (quote ("csr"))) (d #t) (k 0)))) (h "0xs4s0srwd9mkp4wpj161c238s3wb4nyxgz8aif1i5b6yxq8js6p")))

(define-public crate-yew-bootstrap-0.5.14 (c (n "yew-bootstrap") (v "0.5.14") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "wasm-bindgen") (r "0.2.*") (d #t) (k 2)) (d (n "web-sys") (r "0.3.*") (f (quote ("HtmlTextAreaElement" "HtmlSelectElement"))) (d #t) (k 2)) (d (n "yew") (r "^0.20") (f (quote ("csr"))) (d #t) (k 0)))) (h "192zyzr51x0yb9xv6qcinsmzgw85dpn3v53l75g4xa7qkjsjci42")))

(define-public crate-yew-bootstrap-0.5.15 (c (n "yew-bootstrap") (v "0.5.15") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "wasm-bindgen") (r "0.2.*") (d #t) (k 2)) (d (n "web-sys") (r "0.3.*") (f (quote ("HtmlTextAreaElement" "HtmlSelectElement"))) (d #t) (k 2)) (d (n "yew") (r "^0.20") (f (quote ("csr"))) (d #t) (k 0)))) (h "0kyd12zpinwsyg1n3gqg5ah4ibfg3lz80b8ci6999xvkgqd7lvmv")))

(define-public crate-yew-bootstrap-0.5.16 (c (n "yew-bootstrap") (v "0.5.16") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "wasm-bindgen") (r "0.2.*") (d #t) (k 2)) (d (n "web-sys") (r "0.3.*") (f (quote ("HtmlTextAreaElement" "HtmlSelectElement"))) (d #t) (k 2)) (d (n "yew") (r "^0.20") (f (quote ("csr"))) (d #t) (k 0)))) (h "173azr7s15bd2333j89id8vfbg59zrsn6br14s2wxsij5py9vnb1")))

(define-public crate-yew-bootstrap-0.5.17 (c (n "yew-bootstrap") (v "0.5.17") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "wasm-bindgen") (r "0.2.*") (d #t) (k 2)) (d (n "web-sys") (r "0.3.*") (f (quote ("HtmlTextAreaElement" "HtmlSelectElement"))) (d #t) (k 2)) (d (n "yew") (r "^0.20") (f (quote ("csr"))) (d #t) (k 0)))) (h "091hqblgs2c7alad576xkfdqwrmczw7kxbgy3bn1irvi7270jmll")))

(define-public crate-yew-bootstrap-0.5.18 (c (n "yew-bootstrap") (v "0.5.18") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "wasm-bindgen") (r "0.2.*") (d #t) (k 2)) (d (n "web-sys") (r "0.3.*") (f (quote ("HtmlTextAreaElement" "HtmlSelectElement"))) (d #t) (k 2)) (d (n "yew") (r "^0.20") (f (quote ("csr"))) (d #t) (k 0)))) (h "09ly56mizhcb5wnnyygyambw6gjylbbqabsvlqg3shwgn2gv1ma9")))

(define-public crate-yew-bootstrap-0.6.0 (c (n "yew-bootstrap") (v "0.6.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "wasm-bindgen") (r "0.2.*") (d #t) (k 2)) (d (n "web-sys") (r "0.3.*") (f (quote ("HtmlTextAreaElement" "HtmlSelectElement"))) (d #t) (k 2)) (d (n "yew") (r "^0.21") (f (quote ("csr"))) (d #t) (k 0)))) (h "0979zp9lz420qsfddc8wfd16jch1mb5l6j8jfmsad89ljqddqbbl")))

(define-public crate-yew-bootstrap-0.6.1 (c (n "yew-bootstrap") (v "0.6.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "wasm-bindgen") (r "0.2.*") (d #t) (k 2)) (d (n "web-sys") (r "0.3.*") (f (quote ("HtmlTextAreaElement" "HtmlSelectElement"))) (d #t) (k 2)) (d (n "yew") (r "^0.21") (f (quote ("csr"))) (d #t) (k 0)))) (h "0x9gv81kp7dc4a9014qgmpnaxa9fkq4qd32fmfbyz5nv6k2is3yy")))

(define-public crate-yew-bootstrap-0.6.2 (c (n "yew-bootstrap") (v "0.6.2") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "wasm-bindgen") (r "0.2.*") (d #t) (k 2)) (d (n "web-sys") (r "0.3.*") (f (quote ("HtmlTextAreaElement" "HtmlSelectElement"))) (d #t) (k 2)) (d (n "yew") (r "^0.21") (f (quote ("csr"))) (d #t) (k 0)))) (h "01zzmb65xwzdg551hh8ifdjf8jg2jw93l34xyyg08bbjbg0xz01a")))

(define-public crate-yew-bootstrap-0.6.3 (c (n "yew-bootstrap") (v "0.6.3") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "wasm-bindgen") (r "0.2.*") (d #t) (k 2)) (d (n "web-sys") (r "0.3.*") (f (quote ("HtmlTextAreaElement" "HtmlSelectElement"))) (d #t) (k 2)) (d (n "yew") (r "^0.21") (f (quote ("csr"))) (d #t) (k 0)))) (h "0g6q4hckhdndfbsk3jbvqk9as8zlwfcr67n0rg4jaai7028lqkcv")))

(define-public crate-yew-bootstrap-0.7.0 (c (n "yew-bootstrap") (v "0.7.0") (d (list (d (n "anyhow") (r "^1.0.75") (f (quote ("std"))) (k 1)) (d (n "convert_case") (r "^0.6.0") (k 1)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "wasm-bindgen") (r "0.2.*") (d #t) (k 2)) (d (n "web-sys") (r "0.3.*") (f (quote ("HtmlTextAreaElement" "HtmlSelectElement"))) (d #t) (k 2)) (d (n "yew") (r "^0.21") (f (quote ("csr"))) (d #t) (k 0)))) (h "0jjvgbgdawgj1qs837l4hahq9idyvvgqvmhn0gfi0pq5r9cw9irv")))

(define-public crate-yew-bootstrap-0.8.0 (c (n "yew-bootstrap") (v "0.8.0") (d (list (d (n "anyhow") (r "^1.0.75") (f (quote ("std"))) (k 1)) (d (n "convert_case") (r "^0.6.0") (k 1)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "wasm-bindgen") (r "0.2.*") (d #t) (k 2)) (d (n "web-sys") (r "0.3.*") (f (quote ("HtmlTextAreaElement" "HtmlSelectElement"))) (d #t) (k 2)) (d (n "yew") (r "^0.21") (f (quote ("csr"))) (d #t) (k 0)))) (h "0yklc5fpr7si7n2s8mxflgci1xy2iqv3zrp2wm88527kwb3d0q9q")))

(define-public crate-yew-bootstrap-0.8.1 (c (n "yew-bootstrap") (v "0.8.1") (d (list (d (n "anyhow") (r "^1.0.75") (f (quote ("std"))) (k 1)) (d (n "convert_case") (r "^0.6.0") (k 1)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "wasm-bindgen") (r "0.2.*") (d #t) (k 2)) (d (n "web-sys") (r "0.3.*") (f (quote ("HtmlTextAreaElement" "HtmlSelectElement"))) (d #t) (k 2)) (d (n "yew") (r "^0.21") (f (quote ("csr"))) (d #t) (k 0)))) (h "0n6q01rgfb4xvnwfixgsp8ci90kxzq110cl0kp6nl7jkmc8qi0f5")))

(define-public crate-yew-bootstrap-0.9.0 (c (n "yew-bootstrap") (v "0.9.0") (d (list (d (n "anyhow") (r "^1.0.75") (f (quote ("std"))) (k 1)) (d (n "convert_case") (r "^0.6.0") (k 1)) (d (n "gloo-console") (r "^0.3") (d #t) (k 0)) (d (n "wasm-bindgen") (r "0.2.*") (d #t) (k 2)) (d (n "web-sys") (r "0.3.*") (f (quote ("HtmlTextAreaElement" "HtmlSelectElement"))) (d #t) (k 2)) (d (n "yew") (r "^0.21") (f (quote ("csr"))) (d #t) (k 0)))) (h "1g89rvmwbknsic7jly50m2jjdf90pxgaxzx9yaq4hincanh983ai")))

(define-public crate-yew-bootstrap-0.10.0 (c (n "yew-bootstrap") (v "0.10.0") (d (list (d (n "anyhow") (r "^1.0.75") (f (quote ("std"))) (k 1)) (d (n "convert_case") (r "^0.6.0") (k 1)) (d (n "gloo-console") (r "^0.3") (d #t) (k 0)) (d (n "wasm-bindgen") (r "0.2.*") (d #t) (k 2)) (d (n "web-sys") (r "0.3.*") (f (quote ("HtmlTextAreaElement" "HtmlSelectElement"))) (d #t) (k 2)) (d (n "yew") (r "^0.21") (f (quote ("csr"))) (d #t) (k 0)))) (h "02vg26cnghp8s7g3dabg2xgiisdg1hil9i3skka4q7nl6w9phm08")))

