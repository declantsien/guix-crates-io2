(define-module (crates-io ye w- yew-and-bulma) #:use-module (crates-io))

(define-public crate-yew-and-bulma-0.1.0 (c (n "yew-and-bulma") (v "0.1.0") (d (list (d (n "test-case") (r "^3.0.0") (d #t) (k 2)) (d (n "yew") (r "^0.20.0") (f (quote ("csr"))) (d #t) (k 0)))) (h "1bszc5xpsiingbm6yz49rnxnzf2ngz39y7dcp6d5rmxqg4jjdpq8") (r "1.60")))

(define-public crate-yew-and-bulma-0.2.0 (c (n "yew-and-bulma") (v "0.2.0") (d (list (d (n "test-case") (r "^3.0.0") (d #t) (k 2)) (d (n "yew") (r "^0.20.0") (f (quote ("csr"))) (d #t) (k 0)))) (h "1fbkwjmqjz2x89rvl5r0mvbkzssmlcbp3hb4izlf8ljl38zl53cv") (r "1.60")))

(define-public crate-yew-and-bulma-0.2.1 (c (n "yew-and-bulma") (v "0.2.1") (d (list (d (n "test-case") (r "^3.0.0") (d #t) (k 2)) (d (n "yew") (r "^0.20.0") (f (quote ("csr"))) (d #t) (k 0)))) (h "0nvxcsb98r1dcw4qcvqlvq2bpznzhzfmd8ha6cdxw762sdwr2m2l") (r "1.60")))

(define-public crate-yew-and-bulma-0.3.0 (c (n "yew-and-bulma") (v "0.3.0") (d (list (d (n "test-case") (r "^3.0.0") (d #t) (k 2)) (d (n "yew") (r "^0.20.0") (f (quote ("csr"))) (d #t) (k 0)) (d (n "yew-and-bulma-macros") (r "^0.1.2") (d #t) (k 0)))) (h "1wanzf383c25ii6b0dply5gxa7zbrzl48xp7k8a8in2hqjvrifsn") (r "1.60")))

(define-public crate-yew-and-bulma-0.4.0 (c (n "yew-and-bulma") (v "0.4.0") (d (list (d (n "test-case") (r "^3.0.0") (d #t) (k 2)) (d (n "yew") (r "^0.20.0") (f (quote ("csr"))) (d #t) (k 0)) (d (n "yew-and-bulma-macros") (r "^0.1.2") (d #t) (k 0)))) (h "004qjfr8vwz1qr084sm4hrpfj8k6qp0zfm1d76g6xpfy9psimhb6") (r "1.60")))

