(define-module (crates-io ye w- yew-canvas) #:use-module (crates-io))

(define-public crate-yew-canvas-0.1.0 (c (n "yew-canvas") (v "0.1.0") (d (list (d (n "gloo") (r "^0.8.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.82") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.59") (f (quote ("HtmlCanvasElement"))) (d #t) (k 0)) (d (n "yew") (r "^0.19") (d #t) (k 0)))) (h "16gh77rz4ih6d44a82vgsialjdf4b6a3zlfwc8ki3jpavjqan9gw")))

(define-public crate-yew-canvas-0.1.1 (c (n "yew-canvas") (v "0.1.1") (d (list (d (n "gloo") (r "^0.8.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.82") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.59") (f (quote ("HtmlCanvasElement"))) (d #t) (k 0)) (d (n "yew") (r "^0.19") (d #t) (k 0)))) (h "1f7hpklag3q6py9dk0q9hr1v93ifir050yqbkw9dc0cvf4s042jd")))

(define-public crate-yew-canvas-0.1.2 (c (n "yew-canvas") (v "0.1.2") (d (list (d (n "gloo") (r "^0.8.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.82") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.59") (f (quote ("HtmlCanvasElement"))) (d #t) (k 0)) (d (n "yew") (r "^0.19") (d #t) (k 0)))) (h "1q0rbnj98v8fi4bl6xsdbidh6blh04g2skdw1x2flb5z6cvwwii3")))

(define-public crate-yew-canvas-0.1.3 (c (n "yew-canvas") (v "0.1.3") (d (list (d (n "gloo") (r "^0.8.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.82") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.59") (f (quote ("HtmlCanvasElement"))) (d #t) (k 0)) (d (n "yew") (r "^0.19") (d #t) (k 0)))) (h "07iz5iarn0jxrb9cx8dpd5qnsdfx4dxv8rs6rrlg93db4ggf8bc8")))

(define-public crate-yew-canvas-0.2.0 (c (n "yew-canvas") (v "0.2.0") (d (list (d (n "gloo") (r "^0.8.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.82") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.59") (f (quote ("HtmlCanvasElement"))) (d #t) (k 0)) (d (n "yew") (r "^0.19") (d #t) (k 0)))) (h "0s7n888qyj6q31kqfrp2w0pfqghsd5k8pl81rldysj869rh9fdbw")))

(define-public crate-yew-canvas-0.2.1 (c (n "yew-canvas") (v "0.2.1") (d (list (d (n "gloo") (r "^0.8.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.82") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.59") (f (quote ("HtmlCanvasElement"))) (d #t) (k 0)) (d (n "yew") (r "^0.19") (d #t) (k 0)))) (h "1lhn8njwvmc42g6jhnahhs5aijgdzcig713qmcsmy7w76v47db31")))

(define-public crate-yew-canvas-0.2.2 (c (n "yew-canvas") (v "0.2.2") (d (list (d (n "gloo") (r "^0.8.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.82") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.59") (f (quote ("HtmlCanvasElement"))) (d #t) (k 0)) (d (n "yew") (r "^0.19") (d #t) (k 0)))) (h "1x7cfhygf79azq2d9sd3z41pq60493nxidlprw51sy9zzrb1fnlw")))

(define-public crate-yew-canvas-0.2.3 (c (n "yew-canvas") (v "0.2.3") (d (list (d (n "gloo") (r "^0.8.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.82") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.59") (f (quote ("HtmlCanvasElement"))) (d #t) (k 0)) (d (n "yew") (r "^0.19") (d #t) (k 0)))) (h "1cg0r8w9q5qz6vmidmiskg9f1psr1id9b456mkzvf4mw08x0460b")))

(define-public crate-yew-canvas-0.2.4 (c (n "yew-canvas") (v "0.2.4") (d (list (d (n "gloo") (r "^0.8.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.82") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.59") (f (quote ("HtmlCanvasElement"))) (d #t) (k 0)) (d (n "yew") (r "^0.19") (d #t) (k 0)))) (h "11mgbimav2pj0xcfivf6lh4rdbvn7ki4kwwg4birl23bx8csc9v3")))

