(define-module (crates-io ye w- yew-askama) #:use-module (crates-io))

(define-public crate-yew-askama-0.1.0 (c (n "yew-askama") (v "0.1.0") (d (list (d (n "askama") (r "^0.12.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("full"))) (d #t) (k 0)) (d (n "yew") (r "^0.21.0") (d #t) (k 0)))) (h "0xaaa0kap39axk7kqgypzzla6xwrrpi8317nbw4kvpd7kg8v7sk1")))

