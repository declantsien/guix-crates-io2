(define-module (crates-io ye w- yew-ansi) #:use-module (crates-io))

(define-public crate-yew-ansi-0.1.0 (c (n "yew-ansi") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "yew") (r "^0.17") (o #t) (d #t) (k 0)))) (h "11i14cpbx15n8slgrfz92nmd54gcrr76dpaqcx1vdw18p4z8bl2i") (f (quote (("default" "yew"))))))

(define-public crate-yew-ansi-0.2.0 (c (n "yew-ansi") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "yew") (r "^0.20") (o #t) (d #t) (k 0)))) (h "16qw4j19vy1368ip40hd0i7hbml8rjdbb1rhxm3z333h68bc7md4") (f (quote (("default" "yew"))))))

