(define-module (crates-io ye w- yew-router-min-route-parser) #:use-module (crates-io))

(define-public crate-yew-router-min-route-parser-0.8.0 (c (n "yew-router-min-route-parser") (v "0.8.0") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "nom") (r "^5.0.0") (d #t) (k 0)))) (h "0mkl5j9ajk541acjsmknqzhfyml9h9irjzzqbgshhyzkbgclm1aq")))

