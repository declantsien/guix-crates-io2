(define-module (crates-io ye w- yew-fs-router-macro) #:use-module (crates-io))

(define-public crate-yew-fs-router-macro-0.1.0 (c (n "yew-fs-router-macro") (v "0.1.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0zm5p760ghvnslpvk4g8v2m71ykzf9yhjfrq1f2j09r2bbxwjpfq")))

(define-public crate-yew-fs-router-macro-0.2.0 (c (n "yew-fs-router-macro") (v "0.2.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0gnk08xss9yzfjlisrl8yqfljdhwnwpwc9gkc332s15r5d6m3mxv")))

(define-public crate-yew-fs-router-macro-0.3.0 (c (n "yew-fs-router-macro") (v "0.3.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "13qh3yqsff0y3iam8qrc8c9zcgymm0lp74bfyxcs52gpl6hlkbyl")))

(define-public crate-yew-fs-router-macro-0.4.0 (c (n "yew-fs-router-macro") (v "0.4.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0zp9xglsk25qxd286p5pryihfvhcqb4dcm253i7mx7q2r5wv1rxc")))

