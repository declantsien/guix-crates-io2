(define-module (crates-io ye w- yew-plotly) #:use-module (crates-io))

(define-public crate-yew-plotly-0.1.0 (c (n "yew-plotly") (v "0.1.0") (d (list (d (n "plotly") (r "=0.8.1") (f (quote ("wasm"))) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.83") (d #t) (k 0)) (d (n "yew") (r "^0.19.3") (d #t) (k 0)))) (h "0wqyvv655bzpid7145wzqjl02zjjgbnkp9sw0i3v8wdich0ckkl4")))

(define-public crate-yew-plotly-0.1.1 (c (n "yew-plotly") (v "0.1.1") (d (list (d (n "plotly") (r "=0.8.1") (f (quote ("wasm"))) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.83") (d #t) (k 0)) (d (n "yew") (r "^0.19.3") (d #t) (k 0)))) (h "1lfmwkhi70mply1qvw5zdf4v961yxwxlv9gwviljmq7vmfca415v")))

(define-public crate-yew-plotly-0.2.0 (c (n "yew-plotly") (v "0.2.0") (d (list (d (n "plotly") (r "^0.8.3") (f (quote ("wasm"))) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.83") (d #t) (k 0)) (d (n "yew") (r "^0.20.0") (d #t) (k 0)))) (h "0zj92j6shszrir1hq4bpkzg86ydkgzy1ry262fkj7q5kavs6rr97")))

(define-public crate-yew-plotly-0.3.0 (c (n "yew-plotly") (v "0.3.0") (d (list (d (n "plotly") (r "^0.8.4") (f (quote ("wasm"))) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.91") (d #t) (k 0)) (d (n "yew") (r "^0.21.0") (d #t) (k 0)))) (h "03mxbl6szd5vjck9z4z8xgyjzkyhyi0y4cklrjvy99pqmmqcjfbq")))

