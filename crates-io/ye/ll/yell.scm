(define-module (crates-io ye ll yell) #:use-module (crates-io))

(define-public crate-yell-0.1.0 (c (n "yell") (v "0.1.0") (d (list (d (n "mio") (r "^0.6") (d #t) (k 0)))) (h "0px1klhvdb696a4yjwri3x79ywpjg8mzxnalxk7g55fy6k6jzibh")))

(define-public crate-yell-0.2.0 (c (n "yell") (v "0.2.0") (d (list (d (n "get_if_addrs") (r "^0.4") (d #t) (k 0)) (d (n "json_macros") (r "^0.3") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0xrgbbc1z7afjszhn7zvmnz29clhx1y8hpzdi99ln7pic36ihki8")))

(define-public crate-yell-0.2.1 (c (n "yell") (v "0.2.1") (d (list (d (n "get_if_addrs") (r "^0.4") (d #t) (k 0)) (d (n "json_macros") (r "^0.3") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0qqjbrhh2479sjyylh7zcvph0drac1syq7fgyqmkry2m0qmparj6")))

(define-public crate-yell-0.3.0 (c (n "yell") (v "0.3.0") (d (list (d (n "get_if_addrs") (r "^0.4") (d #t) (k 0)) (d (n "json_macros") (r "^0.3") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1qwjqbml60zk3pchk2wi24x4ba1mm9v5b80ni2r8v7qsrxkiwvpn")))

(define-public crate-yell-0.4.0 (c (n "yell") (v "0.4.0") (d (list (d (n "get_if_addrs") (r "^0.4") (d #t) (k 0)) (d (n "json_macros") (r "^0.3") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1qmay88bar546vsjn22sm04s05jq4nm55ym0z7fx0yby4b62jm1q")))

(define-public crate-yell-0.4.1 (c (n "yell") (v "0.4.1") (d (list (d (n "get_if_addrs") (r "^0.4") (d #t) (k 0)) (d (n "json_macros") (r "^0.3") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0iq06wcbia0a967hs00xzzrqd02mra9lvyr8v9b0h7wzg2isn3qf")))

