(define-module (crates-io ye eh yeehaw) #:use-module (crates-io))

(define-public crate-yeehaw-0.0.1 (c (n "yeehaw") (v "0.0.1") (h "19xr6yqp9ygqysrqy0r74rv6vxyyn4d4m5xaykx036mlfbxvxqlz") (y #t)))

(define-public crate-yeehaw-0.0.2 (c (n "yeehaw") (v "0.0.2") (h "1l23mgbb7dfgjc6zsbd17pv9gd8rcwxcx5gvkaqk74cbhwz9dazg") (y #t)))

