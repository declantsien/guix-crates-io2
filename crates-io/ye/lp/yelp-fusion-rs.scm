(define-module (crates-io ye lp yelp-fusion-rs) #:use-module (crates-io))

(define-public crate-yelp-fusion-rs-0.1.0 (c (n "yelp-fusion-rs") (v "0.1.0") (d (list (d (n "bytes") (r "^1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json" "rustls-tls"))) (k 0)) (d (n "serde") (r "^1.0.139") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1fkix564bfidyvd2zsii00k8kiiz5882hddlf4q7ck1dy1ak96d2") (r "1.63")))

(define-public crate-yelp-fusion-rs-0.1.1 (c (n "yelp-fusion-rs") (v "0.1.1") (d (list (d (n "bytes") (r "^1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json" "rustls-tls"))) (k 0)) (d (n "serde") (r "^1.0.139") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "19d4jd5bx1pdy7siy1fjjqps30vdlrgnd2fihmfizc5v2q11ijg3") (r "1.63")))

(define-public crate-yelp-fusion-rs-0.1.3 (c (n "yelp-fusion-rs") (v "0.1.3") (d (list (d (n "bytes") (r "^1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json" "rustls-tls"))) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "077h7s2vf8297xx0y7k5vwn5hhl0zfxlc00g8iq0fjj1valqaf0q") (r "1.67")))

(define-public crate-yelp-fusion-rs-0.2.0 (c (n "yelp-fusion-rs") (v "0.2.0") (d (list (d (n "bytes") (r "^1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json" "rustls-tls" "blocking"))) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0igcymn20hp7gasp8ak816qdqjkx3r54w945q3cpll7vz4f4brp0") (r "1.67")))

(define-public crate-yelp-fusion-rs-0.2.1 (c (n "yelp-fusion-rs") (v "0.2.1") (d (list (d (n "bytes") (r "^1.5.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.24") (f (quote ("json" "rustls-tls" "blocking"))) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.113") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0ryv7nbd1v7zz0r18d3wjc7p0l4b2qpf0snwwgkjnv3scsbw2a7w") (r "1.75")))

