(define-module (crates-io ye lp yelp-fusion-rs-2) #:use-module (crates-io))

(define-public crate-yelp-fusion-rs-2-0.1.1 (c (n "yelp-fusion-rs-2") (v "0.1.1") (d (list (d (n "bytes") (r "^1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json" "rustls-tls"))) (k 0)) (d (n "serde") (r "^1.0.139") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0kxcgqvfn2qbhbd4dxi0k60ayx5zrvz5g6jhxmkzv3lmwfqxqwlk") (r "1.65")))

(define-public crate-yelp-fusion-rs-2-0.1.2 (c (n "yelp-fusion-rs-2") (v "0.1.2") (d (list (d (n "bytes") (r "^1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json" "rustls-tls"))) (k 0)) (d (n "serde") (r "^1.0.139") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "14if94ql9g95z461qg99h5ns8ac68sn768mblaxv3cv577rix6hw") (r "1.65")))

(define-public crate-yelp-fusion-rs-2-0.1.3 (c (n "yelp-fusion-rs-2") (v "0.1.3") (d (list (d (n "bytes") (r "^1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json" "rustls-tls"))) (k 0)) (d (n "serde") (r "^1.0.139") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "001sa29bxhmismhcdjakinh9mdd0xap33w8jwfs8cskdz1mr096z") (r "1.65")))

