(define-module (crates-io ye rp yerpc-tide) #:use-module (crates-io))

(define-public crate-yerpc-tide-0.4.2 (c (n "yerpc-tide") (v "0.4.2") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "async-std") (r "^1.11.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "tide") (r "^0.16.0") (d #t) (k 0)) (d (n "tide-websockets") (r "^0.4.0") (d #t) (k 0)) (d (n "yerpc") (r "^0.4") (d #t) (k 0)))) (h "1hmn9l7ipa63s3qxi41qq8jnix54gjh904jymqa7pnknavjfdw10")))

(define-public crate-yerpc-tide-0.4.3 (c (n "yerpc-tide") (v "0.4.3") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "async-std") (r "^1.11.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "tide") (r "^0.16.0") (d #t) (k 0)) (d (n "tide-websockets") (r "^0.4.0") (d #t) (k 0)) (d (n "yerpc") (r "^0.4") (d #t) (k 0)))) (h "0gqpam3dz99xhv3c3pwxrzynfmxmwvlb5hg26r08lys3w5n9j0g0")))

(define-public crate-yerpc-tide-0.5.0 (c (n "yerpc-tide") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "async-std") (r "^1.11.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "tide") (r "^0.16.0") (d #t) (k 0)) (d (n "tide-websockets") (r "^0.4.0") (d #t) (k 0)) (d (n "yerpc") (r "^0.5") (d #t) (k 0)))) (h "098nj9gmvqm5zk5bjaa6hnic7i661pmidp2ynx3h7air5vdhd7js")))

