(define-module (crates-io ye rp yerpc_derive) #:use-module (crates-io))

(define-public crate-yerpc_derive-0.3.0 (c (n "yerpc_derive") (v "0.3.0") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 0)) (d (n "darling") (r "^0.14.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.91") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "144922rp3lgqs6gzcc71cqm9v76lyrw990w6vifwb6vqlr6983z7")))

(define-public crate-yerpc_derive-0.4.0 (c (n "yerpc_derive") (v "0.4.0") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 0)) (d (n "darling") (r "^0.14.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.91") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "1klbbs3vai0im9193lscz6cmk69h5zavlclhdnc3d0m5wyl0spnd")))

(define-public crate-yerpc_derive-0.4.2 (c (n "yerpc_derive") (v "0.4.2") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 0)) (d (n "darling") (r "^0.14.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.91") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "1gxiwgspi1hjsi3b1hhzxkqgw4ybqdcfnpnd6d77hza2r37l175y")))

(define-public crate-yerpc_derive-0.4.3 (c (n "yerpc_derive") (v "0.4.3") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 0)) (d (n "darling") (r "^0.14.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.91") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "0856v7b70l2yf9y0nsbdaifzv0xrxkvmnly2j9xnk61n0pwkzmbb")))

(define-public crate-yerpc_derive-0.5.0 (c (n "yerpc_derive") (v "0.5.0") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 0)) (d (n "darling") (r "^0.14.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.91") (f (quote ("full" "parsing" "printing"))) (d #t) (k 0)))) (h "157vbjnv6spxd9piagd8syljwaspb16sv03x9pl9ywg8j3j8qsvd") (f (quote (("openrpc"))))))

(define-public crate-yerpc_derive-0.5.2 (c (n "yerpc_derive") (v "0.5.2") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 0)) (d (n "darling") (r "^0.14.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.91") (f (quote ("full" "parsing" "printing"))) (d #t) (k 0)))) (h "1zmf9zx8vmhbdxv51mp69q2qn7sz6zj99af50spndc4gf9gvn8gk") (f (quote (("openrpc"))))))

(define-public crate-yerpc_derive-0.5.3 (c (n "yerpc_derive") (v "0.5.3") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 0)) (d (n "darling") (r "^0.14.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.91") (f (quote ("full" "parsing" "printing"))) (d #t) (k 0)))) (h "0233zv7ai3q6c3g9ifq9wvxy9z1k3408zil2p5jfjyxw8nh0ll8f") (f (quote (("openrpc"))))))

