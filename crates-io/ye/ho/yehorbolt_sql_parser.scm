(define-module (crates-io ye ho yehorbolt_sql_parser) #:use-module (crates-io))

(define-public crate-yehorbolt_sql_parser-0.1.0 (c (n "yehorbolt_sql_parser") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.8") (d #t) (k 0)) (d (n "pest") (r "^2.7.5") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "1xa1c2y41g6w1anmdgz8vmbnamja3ba1lgmnn9f8794mq1l0lskv")))

(define-public crate-yehorbolt_sql_parser-0.1.1 (c (n "yehorbolt_sql_parser") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.8") (d #t) (k 0)) (d (n "pest") (r "^2.7.5") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "193fbrbag4pfii4483288216gjmj4hr5abqzr6cbx0rq33bm5vvq")))

(define-public crate-yehorbolt_sql_parser-0.1.2 (c (n "yehorbolt_sql_parser") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.8") (d #t) (k 0)) (d (n "pest") (r "^2.7.5") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "0bbmh1cwyazza4p35al47p81wj50b4ggbwbnc3yzc4xn9lqxfr5w")))

(define-public crate-yehorbolt_sql_parser-0.1.3 (c (n "yehorbolt_sql_parser") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.8") (d #t) (k 0)) (d (n "pest") (r "^2.7.5") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "094b9nldk3va0pchb7krldq7r0vm096042mc5xzn50w1xwl9nl2b")))

(define-public crate-yehorbolt_sql_parser-0.1.4 (c (n "yehorbolt_sql_parser") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.8") (d #t) (k 0)) (d (n "pest") (r "^2.7.5") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "086a9n9qjlcikzkg8r05c0py94a79x0aa906wg07s3wyc76zvpr5")))

(define-public crate-yehorbolt_sql_parser-0.1.5 (c (n "yehorbolt_sql_parser") (v "0.1.5") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.8") (d #t) (k 0)) (d (n "pest") (r "^2.7.5") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "1367v0i7ly4iiq21k6n594i2zfybix7wjm8zv43s85vd5p2i3vrc")))

(define-public crate-yehorbolt_sql_parser-0.1.6 (c (n "yehorbolt_sql_parser") (v "0.1.6") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.8") (d #t) (k 0)) (d (n "pest") (r "^2.7.5") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "0xi8n5dpf2wx42lybrc8rs88pkxlxh6bz2vjjx1cap96kfvh5dib")))

(define-public crate-yehorbolt_sql_parser-0.1.7 (c (n "yehorbolt_sql_parser") (v "0.1.7") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "pest") (r "^2.7.5") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "16c2myx9b3b0mmvd9svf0xvakgfn0z1c87lxvw33jv0q030h3bk7")))

(define-public crate-yehorbolt_sql_parser-0.1.8 (c (n "yehorbolt_sql_parser") (v "0.1.8") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "pest") (r "^2.7.5") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "0mcrim77z0rvr90jlj0bqnbjk8jwmqpjw269zlvpnay47bi9m6p1")))

(define-public crate-yehorbolt_sql_parser-0.1.9 (c (n "yehorbolt_sql_parser") (v "0.1.9") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "pest") (r "^2.7.5") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "1wimqj6d035yanna5fka5k9z3isf5pv4mfxcnavf2760lp3blk4s")))

(define-public crate-yehorbolt_sql_parser-0.1.11 (c (n "yehorbolt_sql_parser") (v "0.1.11") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "pest") (r "^2.7.5") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "1ndk8lqm1js7w8kqw9nxsrbns425rsd3rwczd82zli1hallr4f1v")))

