(define-module (crates-io ye w_ yew_vnode_struct) #:use-module (crates-io))

(define-public crate-yew_vnode_struct-0.1.0 (c (n "yew_vnode_struct") (v "0.1.0") (d (list (d (n "web-sys") (r "^0.3.36") (f (quote ("Node"))) (d #t) (k 0)) (d (n "yew") (r "^0.14") (f (quote ("toml" "yaml" "msgpack" "cbor" "web_sys"))) (d #t) (k 0)))) (h "1n34k12kn5j3vkqcqmqbfi6x9sgdbipf5n2x5dh6kfqkzzlg26gq")))

(define-public crate-yew_vnode_struct-0.1.1 (c (n "yew_vnode_struct") (v "0.1.1") (d (list (d (n "web-sys") (r "^0.3.36") (f (quote ("Node"))) (d #t) (k 0)) (d (n "yew") (r "^0.14") (f (quote ("toml" "yaml" "msgpack" "cbor" "web_sys"))) (d #t) (k 0)))) (h "02hq91vyr4wa546gv1fj445ag65r1arbakvqys9svi7b5gjrsrir")))

