(define-module (crates-io ye w_ yew_prism) #:use-module (crates-io))

(define-public crate-yew_prism-0.1.0 (c (n "yew_prism") (v "0.1.0") (d (list (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3") (d #t) (k 2)) (d (n "web-sys") (r "^0.3") (d #t) (k 0)) (d (n "yew") (r "^0.14") (f (quote ("toml" "yaml" "msgpack" "cbor" "web_sys"))) (d #t) (k 0)))) (h "03l597sfsablrcwd3z7kwlzc748lvvyamaw60kv84pfimdr6c45x")))

(define-public crate-yew_prism-0.2.0 (c (n "yew_prism") (v "0.2.0") (d (list (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3") (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (d #t) (k 0)) (d (n "yew") (r "^0.15") (f (quote ("toml" "yaml" "msgpack" "cbor" "web_sys"))) (d #t) (k 0)))) (h "1fzv2dz2vg6zhnkz63yyrjqc7pwla278hwgk7xil9dgwjp34kwrb")))

(define-public crate-yew_prism-0.2.1 (c (n "yew_prism") (v "0.2.1") (d (list (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3") (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (d #t) (k 0)) (d (n "yew") (r "^0.15") (f (quote ("toml" "yaml" "msgpack" "cbor" "web_sys"))) (d #t) (k 0)))) (h "1plra6l1h5zqvvw740axg716jpq320v2xvxlmjlxxcydwz6rlwhq")))

(define-public crate-yew_prism-0.3.0 (c (n "yew_prism") (v "0.3.0") (d (list (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("Document" "HtmlCollection" "NodeList"))) (d #t) (k 0)) (d (n "yew") (r "^0.16") (f (quote ("toml" "yaml" "msgpack" "cbor" "web_sys"))) (d #t) (k 0)))) (h "1id6c30yxx84d32qwzhhhlrfvb7h942zisp3vjqvpfxwmmg5vzmf")))

(define-public crate-yew_prism-0.4.0 (c (n "yew_prism") (v "0.4.0") (d (list (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("Document" "HtmlCollection" "NodeList"))) (d #t) (k 0)) (d (n "yew") (r "^0.17") (f (quote ("toml" "yaml" "msgpack" "cbor" "web_sys"))) (d #t) (k 0)))) (h "09kjzc5p9xvvazikw4w9wvynng1n709bgkc1a4fx3nbfy481h4dv")))

(define-public crate-yew_prism-0.4.1 (c (n "yew_prism") (v "0.4.1") (d (list (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("Document" "HtmlCollection" "NodeList"))) (d #t) (k 0)) (d (n "yew") (r "^0.17") (f (quote ("toml" "yaml" "msgpack" "cbor" "web_sys"))) (d #t) (k 0)))) (h "14b4g831azcbk2c2n91zqdd65kn5hq6id819fcv1d8wwqbgjrgck")))

(define-public crate-yew_prism-0.4.2 (c (n "yew_prism") (v "0.4.2") (d (list (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("Document" "HtmlCollection" "NodeList"))) (d #t) (k 0)) (d (n "yew") (r "^0.18") (f (quote ("toml" "yaml" "msgpack" "cbor" "web_sys"))) (d #t) (k 0)))) (h "1nl6zgq76ksczgb22jibmg9fli23sla46z6a88sr88hzw3wzqrly")))

