(define-module (crates-io ye w_ yew_audio) #:use-module (crates-io))

(define-public crate-yew_audio-0.1.0 (c (n "yew_audio") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.0") (d #t) (k 0)) (d (n "stdweb") (r "^0.4.10") (d #t) (k 0)) (d (n "stdweb-derive") (r "^0.5.0") (d #t) (k 0)) (d (n "yew") (r "^0.4.0") (d #t) (k 0)))) (h "1sik84qylkwqp99v6hd4l2g90mrj34xlj44smhycwyb4hk1x8g44")))

(define-public crate-yew_audio-0.1.1 (c (n "yew_audio") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.0") (d #t) (k 0)) (d (n "stdweb") (r "^0.4.10") (d #t) (k 0)) (d (n "stdweb-derive") (r "^0.5.0") (d #t) (k 0)) (d (n "yew") (r "^0.4.0") (d #t) (k 0)))) (h "0jqbvk1dp4bmmrl5hw9rlr0qz0hswc2mqjm8ias8p16vqiwk71nr")))

(define-public crate-yew_audio-0.1.2 (c (n "yew_audio") (v "0.1.2") (d (list (d (n "serde") (r "^1.0.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.0") (d #t) (k 0)) (d (n "stdweb") (r "^0.4.10") (d #t) (k 0)) (d (n "stdweb-derive") (r "^0.5.0") (d #t) (k 0)) (d (n "yew") (r "^0.4.0") (d #t) (k 0)))) (h "05fq9kayy6ndymx9wfiqj049xxbkd2jh927vgcmp7mp7bki8rgly")))

