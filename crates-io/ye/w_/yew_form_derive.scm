(define-module (crates-io ye w_ yew_form_derive) #:use-module (crates-io))

(define-public crate-yew_form_derive-0.1.0 (c (n "yew_form_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.9") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "yew_form") (r "^0.1") (d #t) (k 0)))) (h "088275xnhh14pwxqph8bbchbak9z0w5hiw3k7ldf06iyfpbi7xd2")))

(define-public crate-yew_form_derive-0.1.4 (c (n "yew_form_derive") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0.9") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "yew_form") (r "^0.1") (d #t) (k 0)))) (h "0df084i0dgcp5d8ii34vbjk6pnl8i8fswnsb4kqm35srd8dp3i3m")))

(define-public crate-yew_form_derive-0.1.5 (c (n "yew_form_derive") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^1.0.9") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "yew_form") (r "^0.1") (d #t) (k 0)))) (h "16y52hrp8brihjn9zb9qjdmas60cglq0rwwl3h7iq85spqvhaw19")))

(define-public crate-yew_form_derive-0.1.7 (c (n "yew_form_derive") (v "0.1.7") (d (list (d (n "proc-macro2") (r "^1.0.9") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "yew_form") (r "^0.1") (d #t) (k 0)))) (h "1bppyn43ym8dlyg76hzllbvhi6sr000ickimr4drxzpadhsv6l4g")))

