(define-module (crates-io ye w_ yew_components_library) #:use-module (crates-io))

(define-public crate-yew_components_library-0.1.0 (c (n "yew_components_library") (v "0.1.0") (d (list (d (n "web-sys") (r "^0.3") (f (quote ("Window"))) (d #t) (k 0)) (d (n "yew") (r "^0.20.0") (d #t) (k 0)) (d (n "yew-router") (r "^0.17.0") (d #t) (k 0)))) (h "0jxv73mahp5pr916l3igm40q983wysrn4gkjfrsnl1495w36fq1r")))

