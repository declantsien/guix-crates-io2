(define-module (crates-io ye w_ yew_heroicons) #:use-module (crates-io))

(define-public crate-yew_heroicons-0.1.0 (c (n "yew_heroicons") (v "0.1.0") (d (list (d (n "yew") (r "^0.19") (d #t) (k 0)))) (h "1r3ckvqja0wbrl3nlma7s5anpavg7rbaywrjfjkim4pk4vdi7vwi")))

(define-public crate-yew_heroicons-0.2.0 (c (n "yew_heroicons") (v "0.2.0") (d (list (d (n "yew") (r "^0.19") (d #t) (k 0)))) (h "1rw20q384slqbn3ydq3677zx3aj1c1bn3fapr0mdcvi4vngd3f93")))

(define-public crate-yew_heroicons-0.3.0 (c (n "yew_heroicons") (v "0.3.0") (d (list (d (n "yew") (r "^0.20") (d #t) (k 0)))) (h "0iqf6ip1xwifiycf3a0x6wwzwsqjqvqsr4m15c583mbv2fdgki1b")))

