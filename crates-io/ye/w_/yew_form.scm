(define-module (crates-io ye w_ yew_form) #:use-module (crates-io))

(define-public crate-yew_form-0.1.0 (c (n "yew_form") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "stdweb") (r "^0.4.20") (d #t) (k 0)) (d (n "validator") (r "^0.10.0") (d #t) (k 0)) (d (n "validator_derive") (r "^0.10.0") (d #t) (k 0)) (d (n "yew") (r "^0.11.0") (d #t) (k 0)))) (h "1prvg7bzafdjcjhsr76yyr2a09x3n68vc4ab445abkp1ryvkvjfi")))

(define-public crate-yew_form-0.1.1 (c (n "yew_form") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "stdweb") (r "^0.4.20") (d #t) (k 0)) (d (n "validator") (r "^0.10.0") (d #t) (k 0)) (d (n "validator_derive") (r "^0.10.0") (d #t) (k 0)) (d (n "yew") (r "^0.11.0") (d #t) (k 0)))) (h "0pjv9mipbgiyhklp8g0dkdmdqmdsy9za6qivyxjdcq6gb3pl9vzp")))

(define-public crate-yew_form-0.1.2 (c (n "yew_form") (v "0.1.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "stdweb") (r "^0.4.20") (d #t) (k 0)) (d (n "validator") (r "^0.10.0") (d #t) (k 0)) (d (n "validator_derive") (r "^0.10.0") (d #t) (k 0)) (d (n "yew") (r "^0.12.0") (d #t) (k 0)))) (h "0glhhr4fvld424b1a7gx58a3yml3n2s8y1ll2j12qfpqjh0h7q7d")))

(define-public crate-yew_form-0.1.3 (c (n "yew_form") (v "0.1.3") (d (list (d (n "stdweb") (r "^0.4.20") (d #t) (k 0)) (d (n "validator") (r "^0.10.0") (d #t) (k 0)) (d (n "validator_derive") (r "^0.10.0") (d #t) (k 0)) (d (n "yew") (r "^0.12.0") (d #t) (k 0)))) (h "1skmsyqqss8x68h2ahl3q6z9zhcfnhd6ivx45ca754jxzhnhdxdf")))

(define-public crate-yew_form-0.1.4 (c (n "yew_form") (v "0.1.4") (d (list (d (n "stdweb") (r "^0.4.20") (d #t) (k 0)) (d (n "validator") (r "^0.10.0") (d #t) (k 0)) (d (n "validator_derive") (r "^0.10.0") (d #t) (k 0)) (d (n "yew") (r "^0.12.0") (d #t) (k 0)))) (h "0flcyfifdcxk3phnxn0zpls1jxar81b7g5aac7zzzh0spnzs25b4")))

(define-public crate-yew_form-0.1.5 (c (n "yew_form") (v "0.1.5") (d (list (d (n "validator") (r "^0.10.0") (d #t) (k 0)) (d (n "validator_derive") (r "^0.10.0") (d #t) (k 0)) (d (n "yew") (r "^0.17") (d #t) (k 0)))) (h "1ih2zf4dd9vl6a926lvkd748ds4q3lmbbza2y6gppzpq6sfjdzaf")))

(define-public crate-yew_form-0.1.7 (c (n "yew_form") (v "0.1.7") (d (list (d (n "validator") (r "^0.10.0") (d #t) (k 0)) (d (n "validator_derive") (r "^0.10.0") (d #t) (k 0)) (d (n "yew") (r "^0.17") (d #t) (k 0)))) (h "1cqirzn9w1dqygx8nw69y0wcalxf76m5nwrwf12j70f6w1k4fbgi")))

