(define-module (crates-io ye w_ yew_quick) #:use-module (crates-io))

(define-public crate-yew_quick-0.1.0 (c (n "yew_quick") (v "0.1.0") (d (list (d (n "regex") (r "^1.7") (d #t) (k 0)) (d (n "stylist") (r "^0.12") (f (quote ("yew_integration"))) (d #t) (k 0)) (d (n "yew") (r "^0.20.0") (f (quote ("csr"))) (d #t) (k 0)))) (h "05j5gmv605c2k7al09fq05gxmnf6824d7w2iwy92blmlwcqmp1cs")))

(define-public crate-yew_quick-0.2.0 (c (n "yew_quick") (v "0.2.0") (d (list (d (n "regex") (r "^1.7") (d #t) (k 0)) (d (n "stylist") (r "^0.12") (f (quote ("yew_integration"))) (d #t) (k 0)) (d (n "yew") (r "^0.20.0") (f (quote ("csr"))) (d #t) (k 0)))) (h "1nm98njbvlcwzwhbrr4mwv75xm4lfmzfq0m9lclgp2cbi1yby3ax")))

(define-public crate-yew_quick-0.2.2 (c (n "yew_quick") (v "0.2.2") (d (list (d (n "stylist") (r "^0.12") (f (quote ("yew_integration"))) (d #t) (k 0)) (d (n "yew") (r "^0.20.0") (f (quote ("csr"))) (d #t) (k 0)))) (h "07pf2fdmkc4ypr4cgiiikc32hxn83ly304dn1baymlr9bmnsqmqc")))

