(define-module (crates-io ye w_ yew_spline) #:use-module (crates-io))

(define-public crate-yew_spline-0.2.1 (c (n "yew_spline") (v "0.2.1") (d (list (d (n "wasm-bindgen") (r "^0.2.83") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.60") (f (quote ("HtmlCanvasElement"))) (d #t) (k 0)) (d (n "yew") (r "^0.19.3") (d #t) (k 0)))) (h "0hs25q2hg2i1zfp93hf50ldrm4j417610asfzffbpb22709h8i22")))

