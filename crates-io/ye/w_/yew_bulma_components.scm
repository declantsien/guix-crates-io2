(define-module (crates-io ye w_ yew_bulma_components) #:use-module (crates-io))

(define-public crate-yew_bulma_components-0.1.0 (c (n "yew_bulma_components") (v "0.1.0") (d (list (d (n "wasm-bindgen") (r "^0.2.74") (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("Window"))) (d #t) (k 0)) (d (n "yew") (r "^0.18.0") (d #t) (k 0)) (d (n "yew-router") (r "^0.15.0") (d #t) (k 0)) (d (n "yewtil") (r "^0.4.0") (d #t) (k 0)))) (h "094ajngbajc5jmcvrcq3ddwzn88vhmngybdwcnv2xl4rjahz656m")))

(define-public crate-yew_bulma_components-0.1.1 (c (n "yew_bulma_components") (v "0.1.1") (d (list (d (n "wasm-bindgen") (r "^0.2.74") (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("Window"))) (d #t) (k 0)) (d (n "yew") (r "^0.18.0") (d #t) (k 0)) (d (n "yew-router") (r "^0.15.0") (d #t) (k 0)) (d (n "yewtil") (r "^0.4.0") (d #t) (k 0)))) (h "190yfw37nxihxww6xpghsk0i5p8bfx05vd2875fnjxadbmzx86hv")))

