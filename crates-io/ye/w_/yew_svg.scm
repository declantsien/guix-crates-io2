(define-module (crates-io ye w_ yew_svg) #:use-module (crates-io))

(define-public crate-yew_svg-0.1.0 (c (n "yew_svg") (v "0.1.0") (d (list (d (n "stdweb") (r "^0.4") (d #t) (k 0)) (d (n "yew") (r "^0.4") (d #t) (k 0)))) (h "1cv2db49zvsyhymnfmwss6szi9y8ql4kx1agg8rrrw3k5h3l0iy0")))

(define-public crate-yew_svg-0.2.0 (c (n "yew_svg") (v "0.2.0") (d (list (d (n "stdweb") (r "^0.4") (d #t) (k 0)) (d (n "yew") (r "^0.5") (d #t) (k 0)))) (h "1357p2igf3zndy5aj9a6h86npmfi50r4r9ajg2nakqglh71fqlj3")))

(define-public crate-yew_svg-0.2.1 (c (n "yew_svg") (v "0.2.1") (d (list (d (n "stdweb") (r "^0.4") (d #t) (k 0)) (d (n "yew") (r ">= 0.5") (d #t) (k 0)))) (h "100wfwvm1jlmx5mcjy8dabcfsy37yky0kq2aaljr3jx2gazkyb9w")))

