(define-module (crates-io ye ss yesser-todo-cli) #:use-module (crates-io))

(define-public crate-yesser-todo-cli-1.0.1 (c (n "yesser-todo-cli") (v "1.0.1") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "console") (r "^0.15.8") (d #t) (k 0)) (d (n "copy_to_output") (r "^2.2.0") (d #t) (k 1)) (d (n "platform-dirs") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.115") (d #t) (k 0)) (d (n "winres") (r "^0.1.12") (d #t) (k 1)))) (h "1slk6147m0ci2743b88y4d0rba4jyx8xmazx2cdlhds537nz544f")))

(define-public crate-yesser-todo-cli-1.0.2 (c (n "yesser-todo-cli") (v "1.0.2") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "console") (r "^0.15.8") (d #t) (k 0)) (d (n "platform-dirs") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.115") (d #t) (k 0)))) (h "0sa9igxwqjjqk5gpwpdx76yl3bfr2sgax9ixr28dswbwj50qznjm")))

