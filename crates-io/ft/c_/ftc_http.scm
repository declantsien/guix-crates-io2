(define-module (crates-io ft c_ ftc_http) #:use-module (crates-io))

(define-public crate-ftc_http-2.2.0 (c (n "ftc_http") (v "2.2.0") (d (list (d (n "confy") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "cookies" "gzip" "json" "multipart" "rustls-tls"))) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "043wlj7rc71kyq9hmp370226ws039i2axb41h1zh3wzpvswfb1na")))

