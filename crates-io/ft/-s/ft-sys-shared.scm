(define-module (crates-io ft -s ft-sys-shared) #:use-module (crates-io))

(define-public crate-ft-sys-shared-0.1.0 (c (n "ft-sys-shared") (v "0.1.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "http") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0lqxkfzvbs6svwfld535vdq4d7byx8sbh4cd3b7x8vsix50bmf82")))

(define-public crate-ft-sys-shared-0.1.1-alpha.1 (c (n "ft-sys-shared") (v "0.1.1-alpha.1") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "http") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1pph1pkf4g39yk4gryirgg025qqackv22abnlfcb2ay7rx7x91ip") (f (quote (("rusqlite"))))))

(define-public crate-ft-sys-shared-0.1.1-alpha.2 (c (n "ft-sys-shared") (v "0.1.1-alpha.2") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (k 0)) (d (n "http") (r "^1") (d #t) (k 0)) (d (n "rusqlite") (r "^0.31") (o #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1fks85b9r6z69mrp0rq0iv9myslwscnqg0fsp1511ps37jpfrzx4") (s 2) (e (quote (("rusqlite" "dep:rusqlite"))))))

(define-public crate-ft-sys-shared-0.1.1-alpha.3 (c (n "ft-sys-shared") (v "0.1.1-alpha.3") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (k 0)) (d (n "http") (r "^1") (d #t) (k 0)) (d (n "rusqlite") (r "^0.31") (o #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1vlprshzdh0m11iyhx93qap37khy6p4ycv2x87i146k4masazdlh") (s 2) (e (quote (("rusqlite" "dep:rusqlite"))))))

(define-public crate-ft-sys-shared-0.1.1-alpha.4 (c (n "ft-sys-shared") (v "0.1.1-alpha.4") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (k 0)) (d (n "http") (r "^1") (d #t) (k 0)) (d (n "rusqlite") (r "^0.31") (o #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1340cxnm1mm9pfmgb24j35lfkqb7sz7x7hkba6i6wccp65wl802h") (s 2) (e (quote (("rusqlite" "dep:rusqlite"))))))

(define-public crate-ft-sys-shared-0.1.2 (c (n "ft-sys-shared") (v "0.1.2") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (k 0)) (d (n "http") (r "^1") (d #t) (k 0)) (d (n "rusqlite") (r "^0.31") (o #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0pd02y5kzpyzsyh2h15d7zkjajkli40d60gahh5fzynx1j7ykdhp") (s 2) (e (quote (("rusqlite" "dep:rusqlite"))))))

