(define-module (crates-io ft cl ftcli) #:use-module (crates-io))

(define-public crate-ftcli-0.1.0 (c (n "ftcli") (v "0.1.0") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.3") (d #t) (k 0)))) (h "0nqn78krfj6hb222q1yg6gx057xyxql5n4sd82bzx770mc5k8j5j")))

(define-public crate-ftcli-0.1.1 (c (n "ftcli") (v "0.1.1") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.3") (d #t) (k 0)))) (h "0f4jy8pxwnvczbgd564bdac0a1x3xqvpwpr9ci62lkaq71a4y2fm")))

(define-public crate-ftcli-0.1.2 (c (n "ftcli") (v "0.1.2") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.3") (d #t) (k 0)))) (h "0ic7b9sdr5xi65ii78jfm057s66sq2pjfs17cczv8bnljqbsqzp9")))

(define-public crate-ftcli-0.1.3 (c (n "ftcli") (v "0.1.3") (d (list (d (n "clap") (r "^4.2.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.3") (d #t) (k 0)))) (h "0ijzplgjayr1q1vm7ni47glana4mpjknv7yvbbw3lhxxgba043bg")))

(define-public crate-ftcli-0.1.4 (c (n "ftcli") (v "0.1.4") (d (list (d (n "clap") (r "^4.2.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.3") (d #t) (k 0)))) (h "04zy89v766idab4190s80lbvmb9ywxnxcjhiw4gwzirhpskyn5z1")))

