(define-module (crates-io ft ld ftldat) #:use-module (crates-io))

(define-public crate-ftldat-0.1.0 (c (n "ftldat") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "memmap2") (r "^0.5.10") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.32") (d #t) (k 0)))) (h "1dbcn5cbwg0ydsjymb1fmgz1k2sdl2wz0y8xz9lc7s4hfpssvqji")))

(define-public crate-ftldat-0.1.1 (c (n "ftldat") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "memmap2") (r "^0.5.10") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.32") (d #t) (k 0)))) (h "0i11cm6121d8n6nh6c7jggbw0j7p3phnw1lpqcr8205h57h3g14l")))

