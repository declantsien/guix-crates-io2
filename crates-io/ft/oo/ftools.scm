(define-module (crates-io ft oo ftools) #:use-module (crates-io))

(define-public crate-ftools-0.1.0 (c (n "ftools") (v "0.1.0") (h "13lx6ah5qz9r2askwg7f94lxm6zd5qw033ihlqp2ki4dlkp5hzvr")))

(define-public crate-ftools-0.1.1 (c (n "ftools") (v "0.1.1") (d (list (d (n "futures") (r "0.3.*") (d #t) (k 2)) (d (n "tokio") (r "1.4.*") (f (quote ("full"))) (d #t) (k 2)))) (h "1mx1pzlngh3j74r69pddjbiqpv17wwd6f4c5dj6amp8f64bpajby")))

