(define-module (crates-io ft p4 ftp4) #:use-module (crates-io))

(define-public crate-ftp4-4.0.0 (c (n "ftp4") (v "4.0.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "native-tls") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)))) (h "08vwxgpnnjxy6zp8kmzq1kzw9fsk9l2397vlj0brlj1h9sj5saj1") (f (quote (("secure" "native-tls") ("debug_print")))) (y #t)))

(define-public crate-ftp4-4.0.1 (c (n "ftp4") (v "4.0.1") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "native-tls") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)))) (h "0b6lq4pfnf7gj8349ippzxs1q8nbnyqyby3vrdyf1djmawavs633") (f (quote (("secure" "native-tls") ("debug_print")))) (y #t)))

(define-public crate-ftp4-4.0.2 (c (n "ftp4") (v "4.0.2") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "native-tls") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)))) (h "0vwhr65cwfb47s0lkm3jgsh4x9fajn20h7nzka7n2x0fg95660vy") (f (quote (("secure" "native-tls") ("debug_print")))) (y #t)))

