(define-module (crates-io ft vf ftvf) #:use-module (crates-io))

(define-public crate-ftvf-0.5.0 (c (n "ftvf") (v "0.5.0") (h "1mvifnqbjzzmf9ql85dxsxdfk6mrlqdsvbp11lvpmfn4ja3b0813")))

(define-public crate-ftvf-0.6.0 (c (n "ftvf") (v "0.6.0") (h "1kmz615a315hzaj3bb35wjm9vlmqlxa883sggbzbchp3g9i735bf") (f (quote (("no_std") ("default"))))))

