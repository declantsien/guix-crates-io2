(define-module (crates-io ft yp ftype) #:use-module (crates-io))

(define-public crate-ftype-0.0.0 (c (n "ftype") (v "0.0.0") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "proptest") (r "^1.0") (d #t) (k 2)) (d (n "tempfile") (r "^3.2") (d #t) (k 2)))) (h "0qsmixhn2sqi1ghv7cz08xq30l82g33paj0x5538wjj347zl7z0m")))

