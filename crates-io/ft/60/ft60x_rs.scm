(define-module (crates-io ft #{60}# ft60x_rs) #:use-module (crates-io))

(define-public crate-ft60x_rs-0.1.0 (c (n "ft60x_rs") (v "0.1.0") (d (list (d (n "dirs") (r "^5.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.139") (d #t) (k 0)) (d (n "libloading") (r "^0.7.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "rust-embed") (r "^6.8.1") (d #t) (k 0)) (d (n "tempfile") (r "^3.7.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "179yhf71lv40a5gxism7va73flivlk6gs0gcc10c9g14gxgb23rm")))

