(define-module (crates-io ft #{6x}# ft6x36) #:use-module (crates-io))

(define-public crate-ft6x36-0.1.0 (c (n "ft6x36") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.6") (k 0)))) (h "07bqxf2ki2c7zbch8raq2ipjg68fzyacgp52qs1a43xz5jzh6akb")))

(define-public crate-ft6x36-0.2.0 (c (n "ft6x36") (v "0.2.0") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.6") (k 0)))) (h "0sx7swm4ir8ghxw33nm4maa7nffrnzpwrxsczfxd8xmnvix3g7lv")))

(define-public crate-ft6x36-0.3.0 (c (n "ft6x36") (v "0.3.0") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.6") (k 0)))) (h "1r511xgbwx6dxyigggxcxsia8c773hvpc0asx7s5p9fq5jh4hcn9") (f (quote (("event_process") ("default" "event_process"))))))

(define-public crate-ft6x36-0.4.0 (c (n "ft6x36") (v "0.4.0") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.6") (k 0)))) (h "1irhc6lzyyl8cnvv7cjzhzvmia48z19bdww2pnanszf212hxzyc0") (f (quote (("event_process") ("default" "event_process"))))))

(define-public crate-ft6x36-0.5.0 (c (n "ft6x36") (v "0.5.0") (d (list (d (n "embedded-hal") (r "^1.0.0-rc.1") (d #t) (k 0)) (d (n "num_enum") (r "^0.7.0") (k 0)))) (h "1insqq8lrmg4ypa3j04r286hcx39yjhdp9ph3p26dar5hacs2byx") (f (quote (("event_process") ("default" "event_process"))))))

(define-public crate-ft6x36-0.6.0 (c (n "ft6x36") (v "0.6.0") (d (list (d (n "embedded-hal") (r "^1.0.0-rc.1") (d #t) (k 0)) (d (n "num_enum") (r "^0.7.0") (k 0)) (d (n "serde") (r "^1.0.193") (o #t) (k 0)))) (h "1fnl12jvr7slj8dda26pgpgs26nad8kvk7sv80zcbd5mbv6k0d8s") (f (quote (("event_process") ("default" "event_process")))) (s 2) (e (quote (("serde" "event_process" "dep:serde" "serde?/derive"))))))

(define-public crate-ft6x36-0.6.1 (c (n "ft6x36") (v "0.6.1") (d (list (d (n "embedded-hal") (r "^1.0.0") (d #t) (k 0)) (d (n "num_enum") (r "^0.7.0") (k 0)) (d (n "serde") (r "^1.0.193") (o #t) (k 0)))) (h "16yczjnqs6iwby5ylhpsqz3hvsldyi9r80c3habkkg0sszymhdf3") (f (quote (("event_process") ("default" "event_process")))) (s 2) (e (quote (("serde" "event_process" "dep:serde" "serde?/derive"))))))

