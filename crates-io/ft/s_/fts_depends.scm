(define-module (crates-io ft s_ fts_depends) #:use-module (crates-io))

(define-public crate-fts_depends-0.0.1 (c (n "fts_depends") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^4.0.15") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.9.0") (d #t) (k 0)) (d (n "ptree") (r "^0.4.0") (d #t) (k 0)) (d (n "subprocess") (r "^0.2.9") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)) (d (n "which") (r "^4.3.0") (d #t) (k 0)))) (h "1v580x262jx47rdpclfm7n75dsjvjwhc69c29aj37h7fp0pawman")))

(define-public crate-fts_depends-0.0.2 (c (n "fts_depends") (v "0.0.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^4.0.15") (f (quote ("derive"))) (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.9.0") (d #t) (k 0)) (d (n "ptree") (r "^0.4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)) (d (n "which") (r "^4.3.0") (d #t) (k 0)))) (h "1fki9mx9v4c9qlmyhgd6bp5lbmf6bb8l0cxr3z2cdqd781668j0y")))

