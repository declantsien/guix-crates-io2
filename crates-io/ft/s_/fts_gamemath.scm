(define-module (crates-io ft s_ fts_gamemath) #:use-module (crates-io))

(define-public crate-fts_gamemath-0.1.0 (c (n "fts_gamemath") (v "0.1.0") (d (list (d (n "fts_units") (r "^0.1.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.5") (d #t) (k 0)) (d (n "typenum") (r "^1.10.0") (d #t) (k 0)))) (h "0f2rnlmrdynm0k5ir4y941p2ps4prj70xc294xflwlbhfvbxhdc3")))

(define-public crate-fts_gamemath-0.1.1 (c (n "fts_gamemath") (v "0.1.1") (d (list (d (n "fts_units") (r "^0.1.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.5") (d #t) (k 0)) (d (n "typenum") (r "^1.10.0") (d #t) (k 0)))) (h "19qxxkcyn1yrmiyb5sm5dd526vywj6904iflkxp1n63x5rc1bdf2")))

