(define-module (crates-io ft s_ fts_units) #:use-module (crates-io))

(define-public crate-fts_units-0.1.0 (c (n "fts_units") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.2.5") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)) (d (n "typenum") (r "^1.10.0") (d #t) (k 0)))) (h "1j16xg26166sar27rdwm2rc9mbp29lqiaz7pqrwikwfxdhld4ngj")))

(define-public crate-fts_units-0.1.1 (c (n "fts_units") (v "0.1.1") (d (list (d (n "num-traits") (r "^0.2.5") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)) (d (n "typenum") (r "^1.10.0") (d #t) (k 0)))) (h "179iia7jz2sgnsbph8ljxkdhh68bva91fykz8zplx6yjsgdp348w")))

