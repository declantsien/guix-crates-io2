(define-module (crates-io ft #{62}# ft6236) #:use-module (crates-io))

(define-public crate-ft6236-0.0.1 (c (n "ft6236") (v "0.0.1") (d (list (d (n "defmt") (r "^0.3.4") (o #t) (d #t) (k 0)) (d (n "embedded-hal-02") (r "^0.2") (f (quote ("unproven"))) (d #t) (k 0) (p "embedded-hal")) (d (n "embedded-hal-1") (r "^1.0.0-alpha.10") (d #t) (k 0) (p "embedded-hal")))) (h "1i29xzipnzbqngx39dr8y3h2jf7iv3bw5knlmv6wwa4x2wnimf5c")))

(define-public crate-ft6236-0.0.2 (c (n "ft6236") (v "0.0.2") (d (list (d (n "defmt") (r "^0.3.4") (o #t) (d #t) (k 0)) (d (n "embedded-hal-02") (r "^0.2") (f (quote ("unproven"))) (d #t) (k 0) (p "embedded-hal")) (d (n "embedded-hal-1") (r "^1.0.0-alpha.10") (d #t) (k 0) (p "embedded-hal")))) (h "0jvxgxlrw0knvs83j9lsss19rqxvrzjz7rqsmigl6mxw55h0sn3x")))

(define-public crate-ft6236-0.0.3 (c (n "ft6236") (v "0.0.3") (d (list (d (n "defmt") (r "^0.3.4") (o #t) (d #t) (k 0)) (d (n "embedded-hal-02") (r "^0.2") (f (quote ("unproven"))) (d #t) (k 0) (p "embedded-hal")) (d (n "embedded-hal-1") (r "^1.0.0-alpha.10") (d #t) (k 0) (p "embedded-hal")))) (h "1r9wb0cjnvz5zgdf48dc3yxi21xr3w7i6fxkpdfpgjhvyccpgr2k")))

