(define-module (crates-io ft sw ftswarm_macros) #:use-module (crates-io))

(define-public crate-ftswarm_macros-0.1.0 (c (n "ftswarm_macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0gg4sdl8s7q517i5893vkjz058471agyr94f9y20s3xxszahpc34")))

(define-public crate-ftswarm_macros-0.2.0 (c (n "ftswarm_macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1adjh9ja2c8spps86c3djx7wni993lkd0fpcrsjfssd0ngnpp86b")))

(define-public crate-ftswarm_macros-0.2.1 (c (n "ftswarm_macros") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "16xmiywpvjxr7g5mkk8829s6dha5j31v0jw3df8m5lw8ilsg26zh")))

(define-public crate-ftswarm_macros-0.2.2 (c (n "ftswarm_macros") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1dmljrxa48jnjsk5mwzyw6dsfhj4ljyy7zcyp8gm8wcpf6p5cm17")))

