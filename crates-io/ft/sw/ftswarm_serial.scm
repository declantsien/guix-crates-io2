(define-module (crates-io ft sw ftswarm_serial) #:use-module (crates-io))

(define-public crate-ftswarm_serial-0.1.0 (c (n "ftswarm_serial") (v "0.1.0") (d (list (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "serialport") (r "^4.3.0") (d #t) (k 0)))) (h "1j938qxpg2jf5n3md387la9wharlbvk7i43d5mwzfd8rxrfifs9r")))

(define-public crate-ftswarm_serial-0.2.0 (c (n "ftswarm_serial") (v "0.2.0") (d (list (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "serialport") (r "^4.3.0") (d #t) (k 0)))) (h "05ln4aj3jjgb7vcnsfik1gqfwms416ck64zg2vka7f9wwfgav5pg")))

(define-public crate-ftswarm_serial-0.2.1 (c (n "ftswarm_serial") (v "0.2.1") (d (list (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "serialport") (r "^4.3.0") (d #t) (k 0)))) (h "0vmafzh7v121sjz2gp40b5w8slc38aw21my7fdrknlnidfzlijsi")))

(define-public crate-ftswarm_serial-0.2.2 (c (n "ftswarm_serial") (v "0.2.2") (d (list (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "serialport") (r "^4.3.0") (d #t) (k 0)))) (h "0xcxgcg3za6c2pzf3pypamr3vyp3p8hc0zyszsfnibins8c7xm32")))

