(define-module (crates-io ft sw ftswarm_emulator) #:use-module (crates-io))

(define-public crate-ftswarm_emulator-0.2.0 (c (n "ftswarm_emulator") (v "0.2.0") (d (list (d (n "env_logger") (r "^0.11.3") (d #t) (k 2)) (d (n "ftswarm") (r "^0.2.0") (d #t) (k 2)) (d (n "ftswarm_proto") (r "^0.2.0") (d #t) (k 0)) (d (n "ftswarm_serial") (r "^0.2.0") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "004w3g1xd1awsj2nma6dg5g0g6gc56m40nj1v6wc4g936g7bkq1y")))

(define-public crate-ftswarm_emulator-0.2.1 (c (n "ftswarm_emulator") (v "0.2.1") (d (list (d (n "env_logger") (r "^0.11.3") (d #t) (k 2)) (d (n "ftswarm") (r "^0.2.1") (d #t) (k 2)) (d (n "ftswarm_proto") (r "^0.2.1") (d #t) (k 0)) (d (n "ftswarm_serial") (r "^0.2.1") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "034c1rc2n6fwgd769y4gry54c4c7k4k3kq22m66vbaq4vrvp1ncg")))

(define-public crate-ftswarm_emulator-0.2.2 (c (n "ftswarm_emulator") (v "0.2.2") (d (list (d (n "env_logger") (r "^0.11.3") (d #t) (k 2)) (d (n "ftswarm") (r "^0.2.2") (d #t) (k 2)) (d (n "ftswarm_proto") (r "^0.2.2") (d #t) (k 0)) (d (n "ftswarm_serial") (r "^0.2.2") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "013g3rc20dsr0xpls2daggw4pg03n7bijxdr88gd1j0fjw72haih")))

(define-public crate-ftswarm_emulator-0.2.3 (c (n "ftswarm_emulator") (v "0.2.3") (d (list (d (n "env_logger") (r "^0.11.3") (d #t) (k 2)) (d (n "ftswarm") (r "^0.2.2") (d #t) (k 2)) (d (n "ftswarm_proto") (r "^0.2.2") (d #t) (k 0)) (d (n "ftswarm_serial") (r "^0.2.2") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "0cxd9rsyznwwv3i1hw4rixlfaw83cmpi6qr0cr3lm6g7nxd7f8h6")))

(define-public crate-ftswarm_emulator-0.2.4 (c (n "ftswarm_emulator") (v "0.2.4") (d (list (d (n "env_logger") (r "^0.11.3") (d #t) (k 2)) (d (n "ftswarm") (r "^0.2.2") (d #t) (k 2)) (d (n "ftswarm_proto") (r "^0.2.2") (d #t) (k 0)) (d (n "ftswarm_serial") (r "^0.2.2") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1rc101zvffqsbj5nfh0hc8pqyqabzasz4gfls68n3i3jdazmmi4m")))

(define-public crate-ftswarm_emulator-0.2.5 (c (n "ftswarm_emulator") (v "0.2.5") (d (list (d (n "env_logger") (r "^0.11.3") (d #t) (k 2)) (d (n "ftswarm") (r "^0.2.2") (d #t) (k 2)) (d (n "ftswarm_proto") (r "^0.2.2") (d #t) (k 0)) (d (n "ftswarm_serial") (r "^0.2.2") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "19ad72fwc7zz1zbfdmfqzl4bir62nn67g2varn9qfs6aapln00n8")))

