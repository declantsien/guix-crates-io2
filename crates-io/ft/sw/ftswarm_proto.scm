(define-module (crates-io ft sw ftswarm_proto) #:use-module (crates-io))

(define-public crate-ftswarm_proto-0.1.0 (c (n "ftswarm_proto") (v "0.1.0") (h "07whcpc5n0i5cyk5h4c9qdbcvawjh25ayj9byah4wijxdkblalsy")))

(define-public crate-ftswarm_proto-0.2.0 (c (n "ftswarm_proto") (v "0.2.0") (d (list (d (n "strum") (r "^0.26.2") (d #t) (k 0)) (d (n "strum_macros") (r "^0.26.2") (d #t) (k 0)))) (h "038r9wjdx8s5z18wvdm1qw7wfwp0hc41659mqawflhmqy6x7zx71")))

(define-public crate-ftswarm_proto-0.2.1 (c (n "ftswarm_proto") (v "0.2.1") (d (list (d (n "strum") (r "^0.26.2") (d #t) (k 0)) (d (n "strum_macros") (r "^0.26.2") (d #t) (k 0)))) (h "0g95a82i67b72wycvl3ax2bypb5x6yjfhlfs99phy9wkngga5sb8")))

(define-public crate-ftswarm_proto-0.2.2 (c (n "ftswarm_proto") (v "0.2.2") (d (list (d (n "strum") (r "^0.26.2") (d #t) (k 0)) (d (n "strum_macros") (r "^0.26.2") (d #t) (k 0)))) (h "1xcjcima3hcrx2k9m5lgwgx4grsylkq4ryg3vzr3kir49fsy5gsm")))

