(define-module (crates-io ft #{53}# ft5336) #:use-module (crates-io))

(define-public crate-ft5336-0.1.0 (c (n "ft5336") (v "0.1.0") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r ">=0.6.15, <0.8") (d #t) (k 2)) (d (n "embedded-hal") (r "^0.2.1") (d #t) (k 0)) (d (n "panic-semihosting") (r "^0.5.2") (d #t) (k 0)) (d (n "rtt-target") (r "^0.3.1") (f (quote ("cortex-m"))) (d #t) (k 2)) (d (n "stm32f7xx-hal") (r "^0.6.0") (f (quote ("stm32f746"))) (d #t) (k 2)))) (h "193vxmf2szg51khxzwphx6srbakk2mh7ln3smk6xgaki0im1605d")))

(define-public crate-ft5336-0.2.0 (c (n "ft5336") (v "0.2.0") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 2)) (d (n "cortex-m-rt") (r ">=0.6.15, <0.8") (d #t) (k 2)) (d (n "embedded-hal") (r "^0.2.1") (d #t) (k 0)) (d (n "panic-semihosting") (r "^0.5.2") (d #t) (k 2)) (d (n "rtt-target") (r "^0.3.1") (f (quote ("cortex-m"))) (d #t) (k 2)) (d (n "stm32f7xx-hal") (r "^0.6.0") (f (quote ("stm32f746"))) (d #t) (k 2)))) (h "1m53b39z1p563hk9psq4kzhgsb32lzdyjj0h9s6xg2ipl4xdjb23")))

