(define-module (crates-io ft di ftdi2) #:use-module (crates-io))

(define-public crate-ftdi2-0.9.0 (c (n "ftdi2") (v "0.9.0") (d (list (d (n "libftdi-sys") (r "^1.4.0") (d #t) (k 0)))) (h "0h20gizbim1qga6pp09dmkxr47az3xi1y4maq8abyqrgz3mb3igr")))

(define-public crate-ftdi2-0.9.2 (c (n "ftdi2") (v "0.9.2") (d (list (d (n "libftdi1-sys") (r "^0.1.0") (d #t) (k 0)))) (h "0ismc881pl9scp9nj5lcyibj2vcycbxjmaf6cilhqppsprwxj9r0")))

(define-public crate-ftdi2-0.10.1 (c (n "ftdi2") (v "0.10.1") (d (list (d (n "libftdi1-sys") (r "^1.0.0-alpha3") (f (quote ("vendored" "libusb1-sys"))) (d #t) (k 0)) (d (n "libusb1-sys") (r ">=0.0.1") (d #t) (k 0)))) (h "0ph8wqkb97r5sd20vzdd7fzf24q8n793g59hfpmsx1ll3wk6h8b8")))

(define-public crate-ftdi2-0.11.0 (c (n "ftdi2") (v "0.11.0") (d (list (d (n "libftdi1-sys") (r "^1.0.0-alpha3") (f (quote ("libusb1-sys"))) (d #t) (k 0)) (d (n "libusb1-sys") (r ">=0.0.1") (d #t) (k 0)))) (h "0czp067bkqr14a92qvbkcmscspq8slfqff1xfl4b2f9mpzwdcb69")))

