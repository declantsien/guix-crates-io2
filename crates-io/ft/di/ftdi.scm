(define-module (crates-io ft di ftdi) #:use-module (crates-io))

(define-public crate-ftdi-0.0.1 (c (n "ftdi") (v "0.0.1") (d (list (d (n "libftdi1-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "num") (r "^0.1.30") (d #t) (k 0)))) (h "0d02422hmhhps25s428ak5byvswynf5kr455m147yz6f48w3rxxp")))

(define-public crate-ftdi-0.0.2 (c (n "ftdi") (v "0.0.2") (d (list (d (n "libftdi1-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "num") (r "^0.1.30") (d #t) (k 0)))) (h "0z1kiaqf3sqi4zkx05l1xcx35j1099ackkc4awkxhndls9c6zff2")))

(define-public crate-ftdi-0.1.0 (c (n "ftdi") (v "0.1.0") (d (list (d (n "libftdi1-sys") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.15") (d #t) (k 0)))) (h "0vpfx4dn12wjlhkpx2qypfldhcwrh5556crgmv7qhqrh428fbs4i") (f (quote (("libusb1-sys" "libftdi1-sys/libusb1-sys"))))))

(define-public crate-ftdi-0.1.1 (c (n "ftdi") (v "0.1.1") (d (list (d (n "libftdi1-sys") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.15") (d #t) (k 0)))) (h "1vacygb0c10f9h0spfbm1xbmp2ch1j5jlkd3h5klaf5ip35q5cki") (f (quote (("vendored" "libftdi1-sys/vendored") ("libusb1-sys" "libftdi1-sys/libusb1-sys"))))))

(define-public crate-ftdi-0.1.2 (c (n "ftdi") (v "0.1.2") (d (list (d (n "libftdi1-sys") (r "^1.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.15") (d #t) (k 0)))) (h "0pq1kdkkclwcl6p7ysdf94fc71vi1r1l07zd1ndjx226b2mq2sf8") (f (quote (("vendored" "libftdi1-sys/vendored") ("libusb1-sys" "libftdi1-sys/libusb1-sys"))))))

(define-public crate-ftdi-0.1.3 (c (n "ftdi") (v "0.1.3") (d (list (d (n "ftdi-mpsse") (r "^0.1.0") (d #t) (k 0)) (d (n "libftdi1-sys") (r "^1.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.15") (d #t) (k 0)))) (h "0jkwxyqicbjdjpcqcp0qnk1f80x1p0mbp36df37391kbbii8r71g") (f (quote (("vendored" "libftdi1-sys/vendored") ("libusb1-sys" "libftdi1-sys/libusb1-sys"))))))

