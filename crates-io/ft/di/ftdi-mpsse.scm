(define-module (crates-io ft di ftdi-mpsse) #:use-module (crates-io))

(define-public crate-ftdi-mpsse-0.1.0 (c (n "ftdi-mpsse") (v "0.1.0") (d (list (d (n "libftd2xx") (r "~0.31.0") (d #t) (k 2)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "1wbgs8fapgwns16kab4ji58f626s61w2a3sq2198axzqs5gimk5c")))

(define-public crate-ftdi-mpsse-0.1.1 (c (n "ftdi-mpsse") (v "0.1.1") (d (list (d (n "libftd2xx") (r "~0.32.0") (d #t) (k 2)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "12h7nnzpfz9j0i4ysd1rqyl7jk7y6j9gnzyxvhpqs2lkd7dgqz7s")))

