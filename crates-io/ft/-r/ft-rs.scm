(define-module (crates-io ft -r ft-rs) #:use-module (crates-io))

(define-public crate-ft-rs-0.0.1 (c (n "ft-rs") (v "0.0.1") (h "0fjjxgr8ib9llf33n7bjkq611cxrn1fpd2l4lxrfliyy9qr2a4q2") (r "1.56")))

(define-public crate-ft-rs-0.0.2 (c (n "ft-rs") (v "0.0.2") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0i5qmpjcbc3slww43zsr9y5yxqln07gi2fwlz9zn0b5xgw7w9l63") (f (quote (("default")))) (r "1.56")))

(define-public crate-ft-rs-0.0.3 (c (n "ft-rs") (v "0.0.3") (d (list (d (n "chrono") (r "^0.4.33") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.13") (f (quote ("full"))) (d #t) (k 2)))) (h "1m7hyv6hlkl8111xas8vvm7flqm7vbqxvgjd6digafz5w8i1y6qm") (r "1.56")))

