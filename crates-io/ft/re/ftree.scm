(define-module (crates-io ft re ftree) #:use-module (crates-io))

(define-public crate-ftree-0.1.0 (c (n "ftree") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.151") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "10hb0ncbwhpkkwch5rcs3jvpyv17cw5h8dwbn4l3b8izix569gma") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-ftree-0.1.1 (c (n "ftree") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.151") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1mjg4jrlprpidlv63h0afiv7ah5904azgwkahs6v27yfhw8s5hva") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-ftree-1.0.0 (c (n "ftree") (v "1.0.0") (d (list (d (n "serde") (r "^1.0.151") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "04yw3m8v8yvbqbxablkwryi1k4pfcvc9wxsy1awbgcnww2symm6a") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-ftree-1.0.1 (c (n "ftree") (v "1.0.1") (d (list (d (n "ntest") (r "^0.9") (d #t) (k 2)) (d (n "serde") (r "^1.0.151") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "07ky21mi6jvnkc99mfzj6awj0ykw7lvp4clgcjmiq8sq2x7xdqvc") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-ftree-1.1.0 (c (n "ftree") (v "1.1.0") (d (list (d (n "ntest") (r "^0.9") (d #t) (k 2)) (d (n "serde") (r "^1.0.151") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1xxbw5jf33mgwr5qmsh7zil1z9hj9f49z7lqd6rc0rwy13crs7r4") (s 2) (e (quote (("serde" "dep:serde"))))))

