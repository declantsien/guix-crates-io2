(define-module (crates-io ft p- ftp-cmd-list-parse) #:use-module (crates-io))

(define-public crate-ftp-cmd-list-parse-0.1.0 (c (n "ftp-cmd-list-parse") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.15") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)))) (h "1348zn03xs90cc3ha0df6ag6q4xq7mhg334pivllz8d4ai96007j")))

(define-public crate-ftp-cmd-list-parse-0.2.0 (c (n "ftp-cmd-list-parse") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 0)))) (h "15mlzfppg4s8cmlkrwjfri4jm0d4pycc4ymffkk1yzaa2h3xa604")))

(define-public crate-ftp-cmd-list-parse-0.3.0 (c (n "ftp-cmd-list-parse") (v "0.3.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 0)))) (h "0946a9n7sy6gyiaj6cbl4a8hfcr6n62g234c6kaw2570gsad7v1v")))

(define-public crate-ftp-cmd-list-parse-0.3.1 (c (n "ftp-cmd-list-parse") (v "0.3.1") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 0)))) (h "0i422k6rkkajnqk2z2ng3mcc68nibx972ndzsd7va3gkmsxy7gig")))

