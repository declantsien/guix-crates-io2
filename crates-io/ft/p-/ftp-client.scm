(define-module (crates-io ft p- ftp-client) #:use-module (crates-io))

(define-public crate-ftp-client-0.1.0 (c (n "ftp-client") (v "0.1.0") (d (list (d (n "derive-error") (r "^0") (d #t) (k 0)) (d (n "derive_more") (r "^0") (d #t) (k 0)) (d (n "log") (r "^0") (d #t) (k 0)) (d (n "native-tls") (r "^0") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)))) (h "0x2pidaxdpdk8hbi4aghyb90jkyzsb63ydsmg4vjrns6gg4yax2z")))

(define-public crate-ftp-client-0.1.1 (c (n "ftp-client") (v "0.1.1") (d (list (d (n "derive-error") (r "^0") (d #t) (k 0)) (d (n "derive_more") (r "^0") (d #t) (k 0)) (d (n "log") (r "^0") (d #t) (k 0)) (d (n "native-tls") (r "^0") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 2)))) (h "06wndic8hxdgv3wa5hnggbj0pb83vsc6lniwhia75vk0292wyrw8")))

(define-public crate-ftp-client-0.1.2 (c (n "ftp-client") (v "0.1.2") (d (list (d (n "derive-error") (r "^0") (d #t) (k 0)) (d (n "derive_more") (r "^0") (d #t) (k 0)) (d (n "log") (r "^0") (d #t) (k 0)) (d (n "native-tls") (r "^0") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 2)))) (h "0iafkii76flrwa1idjhbfbfcn9msd2pa8yzbpa3hb1s3iq6sjf6b")))

