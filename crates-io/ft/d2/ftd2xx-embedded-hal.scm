(define-module (crates-io ft d2 ftd2xx-embedded-hal) #:use-module (crates-io))

(define-public crate-ftd2xx-embedded-hal-0.1.0 (c (n "ftd2xx-embedded-hal") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "libftd2xx") (r "~0.16.1") (d #t) (k 0)) (d (n "version-sync") (r "^0.9.1") (d #t) (k 2)))) (h "08mvh33isqc5w6y1r0iqbacqar2ml4an8bcp054lfzhw027b8zf5")))

(define-public crate-ftd2xx-embedded-hal-0.2.0 (c (n "ftd2xx-embedded-hal") (v "0.2.0") (d (list (d (n "embedded-hal") (r "^0.2.4") (d #t) (k 0)) (d (n "libftd2xx") (r "~0.17.0") (d #t) (k 0)) (d (n "nb") (r "^0.1.3") (d #t) (k 0)) (d (n "version-sync") (r "^0.9.1") (d #t) (k 2)))) (h "0vp3hf86v9ziq7i4vqzi88j10fl3qqms3is9sqdwrk4jfvnxnyya")))

(define-public crate-ftd2xx-embedded-hal-0.3.0 (c (n "ftd2xx-embedded-hal") (v "0.3.0") (d (list (d (n "embedded-hal") (r "~0.2.4") (d #t) (k 0)) (d (n "libftd2xx") (r "~0.24.0") (d #t) (k 0)) (d (n "nb") (r "^1") (d #t) (k 0)) (d (n "version-sync") (r "~0.9.2") (d #t) (k 2)))) (h "0yjjhj2dgcay0l85bmnjvb2cy1dn85p3sj842z7487bsa220nycn")))

(define-public crate-ftd2xx-embedded-hal-0.4.0 (c (n "ftd2xx-embedded-hal") (v "0.4.0") (d (list (d (n "embedded-hal") (r "~0.2.4") (d #t) (k 0)) (d (n "libftd2xx") (r "~0.25.0") (d #t) (k 0)) (d (n "nb") (r "^1") (d #t) (k 0)) (d (n "version-sync") (r "~0.9.2") (d #t) (k 2)))) (h "07awgm2ffnis456ckbc25s1gizf9qymnr9yg2bm0rb08lx0zd0dm")))

(define-public crate-ftd2xx-embedded-hal-0.5.0 (c (n "ftd2xx-embedded-hal") (v "0.5.0") (d (list (d (n "embedded-hal") (r "~0.2.4") (d #t) (k 0)) (d (n "libftd2xx") (r "~0.26.0") (d #t) (k 0)) (d (n "nb") (r "^1") (d #t) (k 0)) (d (n "version-sync") (r "~0.9.2") (d #t) (k 2)))) (h "16bvpidlf6rzhm31bz6ikh8800bcnplwwagyzmz2wrqlrcfk7ly8")))

(define-public crate-ftd2xx-embedded-hal-0.5.1 (c (n "ftd2xx-embedded-hal") (v "0.5.1") (d (list (d (n "embedded-hal") (r "~0.2.4") (d #t) (k 0)) (d (n "libftd2xx") (r "~0.26.0") (d #t) (k 0)) (d (n "nb") (r "^1") (d #t) (k 0)) (d (n "version-sync") (r "~0.9.2") (d #t) (k 2)))) (h "0vqbvmrlfs8mllx2z8sa924pxj2xbvgw2zx2f69b9z3bix1ddsrp")))

(define-public crate-ftd2xx-embedded-hal-0.6.0 (c (n "ftd2xx-embedded-hal") (v "0.6.0") (d (list (d (n "embedded-hal") (r "~0.2.4") (d #t) (k 0)) (d (n "libftd2xx") (r "~0.28.0") (d #t) (k 0)) (d (n "nb") (r "^1") (d #t) (k 0)) (d (n "version-sync") (r "~0.9.2") (d #t) (k 2)))) (h "0i7iy60gn71pa74gnhq3kkckm2wfgi56dh2hsgxd0ilzsz2aknf8") (f (quote (("static" "libftd2xx/static"))))))

(define-public crate-ftd2xx-embedded-hal-0.7.0 (c (n "ftd2xx-embedded-hal") (v "0.7.0") (d (list (d (n "embedded-hal") (r "~0.2.4") (d #t) (k 0)) (d (n "libftd2xx") (r "~0.28.0") (d #t) (k 0)) (d (n "nb") (r "^1") (d #t) (k 0)) (d (n "version-sync") (r "~0.9.2") (d #t) (k 2)))) (h "0frrg0z9ajh6mck05fzg87m8ig1p7w9921219w8cq651gnhdhym8") (f (quote (("static" "libftd2xx/static"))))))

(define-public crate-ftd2xx-embedded-hal-0.8.0 (c (n "ftd2xx-embedded-hal") (v "0.8.0") (d (list (d (n "embedded-hal") (r "~0.2.4") (d #t) (k 0)) (d (n "libftd2xx") (r "~0.29.0") (d #t) (k 0)) (d (n "nb") (r "^1") (d #t) (k 0)) (d (n "version-sync") (r "~0.9.2") (d #t) (k 2)))) (h "0rinz9l3731g9ffiy6ymchnxiyi9y193clqmrclgxbvig25rdga6") (f (quote (("static" "libftd2xx/static"))))))

(define-public crate-ftd2xx-embedded-hal-0.9.0 (c (n "ftd2xx-embedded-hal") (v "0.9.0") (d (list (d (n "embedded-hal") (r "~0.2.4") (d #t) (k 0)) (d (n "libftd2xx") (r "~0.31.0") (d #t) (k 0)) (d (n "nb") (r "^1") (d #t) (k 0)) (d (n "version-sync") (r "~0.9.2") (d #t) (k 2)))) (h "1y2d54bsxifvlgski4wyfi4x6szn62a50dsqy5d3jxl8qb4w09hf") (f (quote (("static" "libftd2xx/static"))))))

(define-public crate-ftd2xx-embedded-hal-0.9.1 (c (n "ftd2xx-embedded-hal") (v "0.9.1") (d (list (d (n "embedded-hal") (r "~0.2.4") (d #t) (k 0)) (d (n "libftd2xx") (r "~0.31.0") (d #t) (k 0)) (d (n "nb") (r "^1") (d #t) (k 0)) (d (n "version-sync") (r "~0.9.2") (d #t) (k 2)))) (h "136j661p40534xxl5fiz36y2d9yzra0pw0gj1rryqsid5x20w397") (f (quote (("static" "libftd2xx/static"))))))

