(define-module (crates-io ft #{26}# ft260hid) #:use-module (crates-io))

(define-public crate-ft260hid-0.1.0 (c (n "ft260hid") (v "0.1.0") (d (list (d (n "bitflags") (r "^2.5.0") (d #t) (k 0)) (d (n "hidapi") (r "^2.6.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serial_test") (r "^3.1.1") (d #t) (k 0)))) (h "0pzi0ddi584a5rrcxpj84y404447k0z8pc8v6lqil7xik6jzvsdb")))

