(define-module (crates-io ft #{26}# ft260) #:use-module (crates-io))

(define-public crate-ft260-0.1.0 (c (n "ft260") (v "0.1.0") (d (list (d (n "hidapi") (r "^2.6.0") (d #t) (k 0)) (d (n "rusb") (r "^0.9.1") (d #t) (k 0)))) (h "063h9np5b7cnynq4nj3ph6w7ma46y00hv4fg0iislwhmhqhli7yd")))

