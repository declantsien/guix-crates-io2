(define-module (crates-io ft s- fts-sys) #:use-module (crates-io))

(define-public crate-fts-sys-0.1.0 (c (n "fts-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.57") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "12igc53wqvkzf4nwifyhysafd3qbjgc8rakajlggdmjq9vacjham") (l "c")))

(define-public crate-fts-sys-0.2.0 (c (n "fts-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.58") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)))) (h "09d5lf0scwsg4w4bq67vqfhskq1mgiiihqdwlrv4klmc9digpdzr") (l "c")))

(define-public crate-fts-sys-0.2.1 (c (n "fts-sys") (v "0.2.1") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)))) (h "1jfaclykcqf3cfillbdyv5142lal5w87pbhzkbj0w9qfb3qwj7nk") (l "c")))

(define-public crate-fts-sys-0.2.2 (c (n "fts-sys") (v "0.2.2") (d (list (d (n "bindgen") (r "^0.60") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)))) (h "19azvzq0pmsb1d337k68rrzpg5y32dm5h1vh1qadrbnzsp6ssnfx") (l "c")))

(define-public crate-fts-sys-0.2.3 (c (n "fts-sys") (v "0.2.3") (d (list (d (n "bindgen") (r "^0.62") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)))) (h "00vajscb7k4j3jb57f13ai0nscq5rgsfnwl20h5jqx0h7lrrig9j") (l "c")))

(define-public crate-fts-sys-0.2.4 (c (n "fts-sys") (v "0.2.4") (d (list (d (n "bindgen") (r "^0.63") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)))) (h "1lcxyicvk2g98k9rhb0kiw0sfh2grx1jd8acgg420krl3sic0rls") (l "c")))

(define-public crate-fts-sys-0.2.5 (c (n "fts-sys") (v "0.2.5") (d (list (d (n "bindgen") (r "^0.65") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)))) (h "0wig9a0hm4ib9588rvrlm2iq1p83g69b34b0d2f324121jjf5r73") (l "c")))

(define-public crate-fts-sys-0.2.6 (c (n "fts-sys") (v "0.2.6") (d (list (d (n "bindgen") (r "^0.65") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)))) (h "0nbkqmrn9p94l21v80b135zdkcb7zm70vwp66vi6da3ivdb4gxay") (l "c")))

(define-public crate-fts-sys-0.2.7 (c (n "fts-sys") (v "0.2.7") (d (list (d (n "bindgen") (r "^0.68") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)))) (h "1zdizq3ccb6jrihn19p4y4ygbl7zhdsjqhfb79zwxx2fb9prn43s") (l "c")))

(define-public crate-fts-sys-0.2.8 (c (n "fts-sys") (v "0.2.8") (d (list (d (n "bindgen") (r "^0.69") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)))) (h "18np47bdqrjj9qskg9bvcbwd84cidvfdqcsc7p8k49jgcympkx5r") (l "c")))

(define-public crate-fts-sys-0.2.9 (c (n "fts-sys") (v "0.2.9") (d (list (d (n "bindgen") (r "^0.69") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)))) (h "1fry2lbb8vdszh7qjj2fbisvrw15injzkdmg4qzpj69xb5gls62f") (l "c")))

