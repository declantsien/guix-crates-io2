(define-module (crates-io ft ab ftabutil) #:use-module (crates-io))

(define-public crate-ftabutil-0.1.0 (c (n "ftabutil") (v "0.1.0") (d (list (d (n "clap") (r "^4.0") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "simple_logger") (r "^4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "08hjf6q1b749whyb5108h06fmy9j0d63vrz8y5704fp6wl0rq4lp")))

(define-public crate-ftabutil-0.2.0 (c (n "ftabutil") (v "0.2.0") (d (list (d (n "clap") (r "^4.0") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "dialoguer") (r "^0.10") (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "simple_logger") (r "^4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0h1vlyf2zxs1yz75lx629k5rfcb4b4mh62av3mxncck7348pp4ad")))

