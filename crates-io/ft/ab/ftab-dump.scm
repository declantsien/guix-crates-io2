(define-module (crates-io ft ab ftab-dump) #:use-module (crates-io))

(define-public crate-ftab-dump-1.0.0 (c (n "ftab-dump") (v "1.0.0") (d (list (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)))) (h "0iqw24ym5ip4gbfkv1scyckcmvv18zyy6ybcq7sbg0c8xykg7gbb")))

(define-public crate-ftab-dump-1.0.1 (c (n "ftab-dump") (v "1.0.1") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)))) (h "1yir3cwgi2884wzbh1aag467yapdjki4ds6qnvv9aawkzidy79mb")))

(define-public crate-ftab-dump-1.0.2 (c (n "ftab-dump") (v "1.0.2") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)))) (h "1kamad3h9ang1v53s7fd8pcsjs6s75v5z3v4ni93v449hyv7a67j")))

(define-public crate-ftab-dump-1.0.3 (c (n "ftab-dump") (v "1.0.3") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)))) (h "1did2zm5344w8qgn41x7sgvqvyhsg05gln64bmi380bn55m94ng0")))

