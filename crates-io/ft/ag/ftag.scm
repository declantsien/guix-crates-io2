(define-module (crates-io ft ag ftag) #:use-module (crates-io))

(define-public crate-ftag-0.1.0 (c (n "ftag") (v "0.1.0") (d (list (d (n "clap") (r "^4.4") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "edit") (r "^0.1") (d #t) (k 0)) (d (n "glob-match") (r "^0.2") (d #t) (k 0)) (d (n "opener") (r "^0.6") (d #t) (k 0)) (d (n "ratatui") (r "^0.24.0") (d #t) (k 0)))) (h "1zh0snlgxm813rdhk35a2s2v7y4jmasgrn3ygdh29jcnb48zfn77")))

(define-public crate-ftag-0.1.1 (c (n "ftag") (v "0.1.1") (d (list (d (n "clap") (r "^4.4") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "edit") (r "^0.1") (d #t) (k 0)) (d (n "glob-match") (r "^0.2") (d #t) (k 0)) (d (n "opener") (r "^0.6") (d #t) (k 0)) (d (n "ratatui") (r "^0.24.0") (d #t) (k 0)))) (h "0vqvn7v6d4i2bdj2i0ly2bb89yz724pwjs3wx1mbjydrh8whh3p3")))

(define-public crate-ftag-0.1.2 (c (n "ftag") (v "0.1.2") (d (list (d (n "clap") (r "^4.4") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "edit") (r "^0.1") (d #t) (k 0)) (d (n "glob-match") (r "^0.2") (d #t) (k 0)) (d (n "opener") (r "^0.6") (d #t) (k 0)) (d (n "ratatui") (r "^0.24.0") (d #t) (k 0)))) (h "1bcwmwk0b613wm0zyzsy538ah8bbpa8489fgs0fq7g4s43ifjxdz")))

(define-public crate-ftag-0.1.3 (c (n "ftag") (v "0.1.3") (d (list (d (n "clap") (r "^4.4") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "edit") (r "^0.1") (d #t) (k 0)) (d (n "glob-match") (r "^0.2") (d #t) (k 0)) (d (n "opener") (r "^0.6") (d #t) (k 0)) (d (n "ratatui") (r "^0.24.0") (d #t) (k 0)))) (h "1417n1yjhphfxpaf27scspx0axmgq795gjg6wkjg822n7fwwwwfd")))

(define-public crate-ftag-0.1.4 (c (n "ftag") (v "0.1.4") (d (list (d (n "clap") (r "^4.4") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "edit") (r "^0.1") (d #t) (k 0)) (d (n "glob-match") (r "^0.2") (d #t) (k 0)) (d (n "opener") (r "^0.6") (d #t) (k 0)) (d (n "ratatui") (r "^0.24.0") (d #t) (k 0)))) (h "0fsi68iqq3f1iw4g17cja57hnbblwjx9pn91wap9mc0nlgvzsklq")))

(define-public crate-ftag-0.1.5 (c (n "ftag") (v "0.1.5") (d (list (d (n "clap") (r "^4.4") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "edit") (r "^0.1") (d #t) (k 0)) (d (n "glob-match") (r "^0.2") (d #t) (k 0)) (d (n "opener") (r "^0.6") (d #t) (k 0)) (d (n "ratatui") (r "^0.24.0") (d #t) (k 0)))) (h "00fi73q5pvwj4im1z6grq78r51lfhqkhm4jlw1zw25nv9j1kpnkq")))

(define-public crate-ftag-0.1.6 (c (n "ftag") (v "0.1.6") (d (list (d (n "clap") (r "^4.4") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "edit") (r "^0.1") (d #t) (k 0)) (d (n "glob-match") (r "^0.2") (d #t) (k 0)) (d (n "opener") (r "^0.6") (d #t) (k 0)) (d (n "ratatui") (r "^0.26") (d #t) (k 0)))) (h "0113qzg80z7sq3baadimskpbj597kdrxklk1is9s44dc5aadwz7x")))

