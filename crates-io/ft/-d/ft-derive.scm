(define-module (crates-io ft -d ft-derive) #:use-module (crates-io))

(define-public crate-ft-derive-0.1.0 (c (n "ft-derive") (v "0.1.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1r03w3mlbg8g4wb2f31zh4xm8fini7scf3vhlgiyh7slihfs9k1r")))

