(define-module (crates-io ft -a ft-api) #:use-module (crates-io))

(define-public crate-ft-api-0.1.0 (c (n "ft-api") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "maplit") (r "^1") (d #t) (k 0)) (d (n "realm-client") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "10smicz59wb37k4av8j38mfkjdk9hqvj2r14jsym7752r6jc4pvw")))

