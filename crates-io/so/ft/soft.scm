(define-module (crates-io so ft soft) #:use-module (crates-io))

(define-public crate-soft-0.1.0 (c (n "soft") (v "0.1.0") (h "1zd2lxif4v94x8x7g5hxak22186ii2fx0p8vz4yrpsah1ysgx252")))

(define-public crate-soft-0.1.1 (c (n "soft") (v "0.1.1") (h "0h9m38dd7zngnr64w2m9hwrklrf969jaajhzzrbjq20ajc53nj32")))

