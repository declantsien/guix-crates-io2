(define-module (crates-io so ft softtri) #:use-module (crates-io))

(define-public crate-softtri-0.1.0 (c (n "softtri") (v "0.1.0") (d (list (d (n "indexmap") (r "^2.0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rayon") (r "^1.8.0") (d #t) (k 0)) (d (n "softbuffer") (r "^0.3") (d #t) (k 2)) (d (n "winit") (r "^0.28") (d #t) (k 2)))) (h "1lsjp0f9nnir8v7fax55130jm7cq2likz2hbir0k316hwn1lsd29")))

