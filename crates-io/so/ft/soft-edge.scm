(define-module (crates-io so ft soft-edge) #:use-module (crates-io))

(define-public crate-soft-edge-0.1.0 (c (n "soft-edge") (v "0.1.0") (d (list (d (n "arrayvec") (r "^0.7.2") (d #t) (k 0)) (d (n "bitvec") (r "^0.22.3") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 2)) (d (n "nalgebra") (r "^0.29.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0vsx003vjmbjbfkq6klbj2r49ia895kcrjhag2fj3z3x4yjmv1d6")))

(define-public crate-soft-edge-0.2.0 (c (n "soft-edge") (v "0.2.0") (d (list (d (n "arrayvec") (r "^0.7.2") (d #t) (k 0)) (d (n "bitvec") (r "^0.22.3") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 2)) (d (n "nalgebra") (r "^0.29.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1cn4jl89fjkm8nckb27qnr0863nrhb8zxlck0far3pw7i445dqqw")))

(define-public crate-soft-edge-0.2.1 (c (n "soft-edge") (v "0.2.1") (d (list (d (n "arrayvec") (r "^0.7.2") (d #t) (k 0)) (d (n "bitvec") (r "^0.22.3") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 2)) (d (n "nalgebra") (r "^0.29.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1pal4dgbzd3lrkxjb0jjihkhll3nd23786g55qnd89jb126kafzg")))

(define-public crate-soft-edge-0.2.2 (c (n "soft-edge") (v "0.2.2") (d (list (d (n "arrayvec") (r "^0.7.2") (d #t) (k 0)) (d (n "bitvec") (r "^0.22.3") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 2)) (d (n "nalgebra") (r "^0.29.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0mryz6k1q56r62dw88mjinxc4s8n01bmgvmjm73w6zi8zl7j4aqi")))

(define-public crate-soft-edge-0.2.3 (c (n "soft-edge") (v "0.2.3") (d (list (d (n "arrayvec") (r "^0.7.2") (d #t) (k 0)) (d (n "bitvec") (r "^0.22.3") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 2)) (d (n "nalgebra") (r "^0.29.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1l46vlq7yp0qqjal76iicw21ypk6wm93g2siy9nmrpxdzjglwma8")))

