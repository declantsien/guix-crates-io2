(define-module (crates-io so ft software1-checker) #:use-module (crates-io))

(define-public crate-software1-checker-0.1.0 (c (n "software1-checker") (v "0.1.0") (h "1a4m19cq2xbcbvfjbkm97ww510sgzv96pdzmbp364mfqnlig80rs")))

(define-public crate-software1-checker-0.1.1 (c (n "software1-checker") (v "0.1.1") (d (list (d (n "loading") (r "^0.3.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.12") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 0)))) (h "01hy59wr1pw4ixvjsffkzwv82gprchlbzw9fax1cg8dkdphk4s44")))

