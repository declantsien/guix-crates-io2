(define-module (crates-io so ft soft-ascii-string) #:use-module (crates-io))

(define-public crate-soft-ascii-string-0.1.0 (c (n "soft-ascii-string") (v "0.1.0") (h "02cgb9ssn56j35p7cqwzj2r8farvg33c45mvvpkj4j80d4km3zac")))

(define-public crate-soft-ascii-string-0.1.1 (c (n "soft-ascii-string") (v "0.1.1") (h "1rvrjdqjn5q8g92swsxwd7vnq1v4hnd6n73lp2cmgardq0lmnd2s")))

(define-public crate-soft-ascii-string-0.2.0 (c (n "soft-ascii-string") (v "0.2.0") (h "0hsyx7id4nldc7m6h0jj85qyyj0f076a9ck9w6axd7m3qbzgnscg")))

(define-public crate-soft-ascii-string-1.0.0 (c (n "soft-ascii-string") (v "1.0.0") (h "19k8xlnnvzzv3fw6bvh5jr5fmfp8wsn8m7p10hirpmhgx4ykfiwx")))

(define-public crate-soft-ascii-string-1.0.1 (c (n "soft-ascii-string") (v "1.0.1") (h "0isa6iys9x3xxgf3pgx2md950lzphqkd8f3dmbavhhx1b9wfk3if")))

(define-public crate-soft-ascii-string-1.1.0 (c (n "soft-ascii-string") (v "1.1.0") (h "14axqciibkbpg20fllsrajvzavs538akwq21hxx3qkzcj52lx7m0")))

