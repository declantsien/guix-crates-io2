(define-module (crates-io so ft softfloat-wrapper-riscv) #:use-module (crates-io))

(define-public crate-softfloat-wrapper-riscv-0.1.0 (c (n "softfloat-wrapper-riscv") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.2.12") (d #t) (k 0)) (d (n "simple-soft-float") (r "^0.1.0") (d #t) (k 2)) (d (n "softfloat-sys-riscv") (r "^0.1.0") (d #t) (k 0)))) (h "0z84fc3w123vpryb1z1j22g7n08wlxl3ilmlnk03vy2bmkljan6r")))

