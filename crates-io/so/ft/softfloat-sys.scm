(define-module (crates-io so ft softfloat-sys) #:use-module (crates-io))

(define-public crate-softfloat-sys-0.1.0 (c (n "softfloat-sys") (v "0.1.0") (d (list (d (n "c99") (r "^0.1.1") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0iqd7dqk48l9cp6135im0fz7kyvjcpp00kmg5pn94ih9sfpzz6kh")))

(define-public crate-softfloat-sys-0.1.1 (c (n "softfloat-sys") (v "0.1.1") (d (list (d (n "c99") (r "^0.1.1") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0w5km2kzg4rxxm4c84j69h04m7hk1pybqznjhdrcd2cfjkkykp70")))

(define-public crate-softfloat-sys-0.1.2 (c (n "softfloat-sys") (v "0.1.2") (d (list (d (n "c99") (r "^0.1.1") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "cc-version") (r "^0.1.0") (d #t) (k 1)))) (h "19dz3b92c35bvy2gk0a4h39lkzvsabh56dy6arxkrvxzr2k226i4")))

(define-public crate-softfloat-sys-0.1.3 (c (n "softfloat-sys") (v "0.1.3") (d (list (d (n "c99") (r "^0.1.1") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "cc-version") (r "^0.1.0") (d #t) (k 1)))) (h "0kmarfq72vl3270jbnddq8d94vv5k4yzhnrii5fbfi57m5p1f82w") (f (quote (("riscv") ("default" "8086-sse") ("arm-vfpv2-defaultnan") ("arm-vfpv2") ("8086-sse") ("8086"))))))

(define-public crate-softfloat-sys-0.1.4 (c (n "softfloat-sys") (v "0.1.4") (d (list (d (n "c99") (r "^0.1.1") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "cc-version") (r "^0.1.0") (d #t) (k 1)))) (h "0662rj5ma2w4k9fn9vvcnx2g4fl6p8sv87kkwszybidr5k8vqglb") (f (quote (("riscv") ("default" "8086-sse") ("arm-vfpv2-defaultnan") ("arm-vfpv2") ("8086-sse") ("8086"))))))

