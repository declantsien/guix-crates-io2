(define-module (crates-io so ft soft_matrix) #:use-module (crates-io))

(define-public crate-soft_matrix-1.0.0 (c (n "soft_matrix") (v "1.0.0") (d (list (d (n "keepawake") (r "^0.4.3") (d #t) (k 0)) (d (n "nix") (r "^0.26.4") (f (quote ("user"))) (d #t) (k 0)) (d (n "rustfft") (r "^6.0.1") (d #t) (k 0)) (d (n "wave_stream") (r "^0.5.0") (d #t) (k 0)))) (h "000vbzbdgkclvzxhf4fizfk6210mmhwb6ia9y22dbqswbali2x3j")))

(define-public crate-soft_matrix-1.0.1 (c (n "soft_matrix") (v "1.0.1") (d (list (d (n "keepawake") (r "^0.4.3") (d #t) (k 0)) (d (n "nix") (r "^0.26.4") (f (quote ("user"))) (d #t) (k 0)) (d (n "rustfft") (r "^6.0.1") (d #t) (k 0)) (d (n "wave_stream") (r "^0.5.0") (d #t) (k 0)))) (h "1dfqcapz0y4gv9gj00ijjzwqs5wnl9jg3x60gk5b4wb5k2mqvf6p")))

(define-public crate-soft_matrix-1.0.2 (c (n "soft_matrix") (v "1.0.2") (d (list (d (n "keepawake") (r "^0.4.3") (d #t) (k 0)) (d (n "nix") (r "^0.26.4") (f (quote ("user"))) (d #t) (k 0)) (d (n "rustfft") (r "^6.0.1") (d #t) (k 0)) (d (n "wave_stream") (r "^0.5.0") (d #t) (k 0)))) (h "0idvndx1j4jhw01i7k7f5zlh0ffh308vjm6g2b2864ldyx27sd8m")))

