(define-module (crates-io so ft softaes) #:use-module (crates-io))

(define-public crate-softaes-0.1.0 (c (n "softaes") (v "0.1.0") (h "0glx7ppcnii3y8yknlc1km0vahw7xk8r4kpi3rr13zxq6xflqx7z")))

(define-public crate-softaes-0.1.1 (c (n "softaes") (v "0.1.1") (h "1pnbfyr4sbdzasc6wj2p8pdh8y4wi11p1gapa66xc4pvv6h957f9")))

(define-public crate-softaes-0.1.2 (c (n "softaes") (v "0.1.2") (h "034xq7rqlpmj01xccdj977diqwnj42xsnnnibjzj2csbbrnkqndy")))

(define-public crate-softaes-0.1.3 (c (n "softaes") (v "0.1.3") (h "1pmkar1jp816mm7022n5rxm4y0sa0nlnfwc8dh5k8v5kmvx63x7y")))

