(define-module (crates-io so ft soft_assert) #:use-module (crates-io))

(define-public crate-soft_assert-0.1.0 (c (n "soft_assert") (v "0.1.0") (h "1cyrqf0550dxmlypyaa2jdq5j001a9nx7swhrgbzhxpfng03lb2f")))

(define-public crate-soft_assert-0.1.1 (c (n "soft_assert") (v "0.1.1") (h "15vxbp1j0n908yffvm3xacbcdl212j7k95md85ai663jxb3pw2dm")))

