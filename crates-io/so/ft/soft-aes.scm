(define-module (crates-io so ft soft-aes) #:use-module (crates-io))

(define-public crate-soft-aes-0.1.0 (c (n "soft-aes") (v "0.1.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 2)))) (h "1k44mmhgb8ynndnl9jj9y8k88ss14gv6r6bwxb6cmyjhrsd90cqx")))

(define-public crate-soft-aes-0.2.0 (c (n "soft-aes") (v "0.2.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 2)))) (h "1mcfa0rbkmwnaxb0z0p0pbwl9mx89dh2kpwyxcb05f2a1rdi4g7i")))

(define-public crate-soft-aes-0.2.1 (c (n "soft-aes") (v "0.2.1") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 2)))) (h "14fvv3cp6xvxn7g2pzdv16xi2mvdr92dhfxhq39azkq1km0f3id6")))

(define-public crate-soft-aes-0.2.2 (c (n "soft-aes") (v "0.2.2") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 2)))) (h "0828r9qlzpv76fr548ivn34db8ckwd196i8kcbl5q37xfvwnkx84")))

