(define-module (crates-io so ft soft_skia) #:use-module (crates-io))

(define-public crate-soft_skia-0.1.0 (c (n "soft_skia") (v "0.1.0") (d (list (d (n "base64") (r "^0.21.0") (d #t) (k 0)) (d (n "png") (r "^0.17.5") (d #t) (k 0)) (d (n "tiny-skia") (r "^0.9.0") (d #t) (k 0)))) (h "1ma54bngb82q4r6480f074952cic9f81lqdfdaa2hbsf8hxynj0g")))

(define-public crate-soft_skia-0.2.0 (c (n "soft_skia") (v "0.2.0") (d (list (d (n "base64") (r "^0.21.0") (d #t) (k 0)) (d (n "png") (r "^0.17.5") (d #t) (k 0)) (d (n "tiny-skia") (r "^0.9.0") (d #t) (k 0)))) (h "016lf67lvqfa1fr7g0ihjlhkm6kd8mv07g9f9030xvngxkn6ylvm")))

(define-public crate-soft_skia-0.6.0 (c (n "soft_skia") (v "0.6.0") (d (list (d (n "base64") (r "^0.21.0") (d #t) (k 0)) (d (n "png") (r "^0.17.5") (d #t) (k 0)) (d (n "tiny-skia") (r "^0.9.0") (d #t) (k 0)))) (h "0c98dd8kg8xixxkzpxql7r412hwgszdmq793a0zmqs3gsp60kkmi")))

(define-public crate-soft_skia-0.7.0 (c (n "soft_skia") (v "0.7.0") (d (list (d (n "base64") (r "^0.21.0") (d #t) (k 0)) (d (n "fontdue") (r "^0.7.3") (d #t) (k 0)) (d (n "png") (r "^0.17.5") (d #t) (k 0)) (d (n "tiny-skia") (r "^0.10.0") (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("console"))) (d #t) (k 0)))) (h "1c48wbwrf6hz329hwkwi8zdjy2m7s5p8siqxsp388fgkrjypjq4c")))

(define-public crate-soft_skia-0.8.0 (c (n "soft_skia") (v "0.8.0") (d (list (d (n "base64") (r "^0.21.0") (d #t) (k 0)) (d (n "fontdue") (r "^0.7.3") (d #t) (k 0)) (d (n "png") (r "^0.17.5") (d #t) (k 0)) (d (n "tiny-skia") (r "^0.10.0") (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("console"))) (d #t) (k 0)))) (h "0rvifijs1cvawjcpfcay4yq81gnq4x5n52cva78s4yl507x2ss52")))

(define-public crate-soft_skia-0.9.0 (c (n "soft_skia") (v "0.9.0") (d (list (d (n "base64") (r "^0.21.0") (d #t) (k 0)) (d (n "fontdue") (r "^0.7.3") (d #t) (k 0)) (d (n "image") (r "^0.25.1") (d #t) (k 0)) (d (n "png") (r "^0.17.5") (d #t) (k 0)) (d (n "tiny-skia") (r "^0.10.0") (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("console"))) (d #t) (k 0)))) (h "02hqmfmdcbp55pf4z9s6b9arl7ddcl8x5say8vvvnvvw74hxfrnd")))

(define-public crate-soft_skia-0.10.0 (c (n "soft_skia") (v "0.10.0") (d (list (d (n "base64") (r "^0.21.0") (d #t) (k 0)) (d (n "fontdue") (r "^0.7.3") (d #t) (k 0)) (d (n "image") (r "^0.25.1") (d #t) (k 0)) (d (n "png") (r "^0.17.5") (d #t) (k 0)) (d (n "tiny-skia") (r "^0.10.0") (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("console"))) (d #t) (k 0)))) (h "17b1agbb89psh373sqvsqd8fq6ngdpi5jqxl7hizbcv1kki85060")))

