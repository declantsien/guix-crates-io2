(define-module (crates-io so ft soft-test-failures) #:use-module (crates-io))

(define-public crate-soft-test-failures-0.1.0 (c (n "soft-test-failures") (v "0.1.0") (h "1l4n9ri0nbjlnyw50b9jlxd0lcs248y167i19ib06cnj03kj9qxz")))

(define-public crate-soft-test-failures-0.2.0 (c (n "soft-test-failures") (v "0.2.0") (h "0v6idih6hlsr2shagpxrgbi9a54jb2p5ljvi93js9vdsnvzszfxs")))

