(define-module (crates-io so ft softfloat-sys-riscv) #:use-module (crates-io))

(define-public crate-softfloat-sys-riscv-0.1.0 (c (n "softfloat-sys-riscv") (v "0.1.0") (d (list (d (n "c99") (r "^0.1.1") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "cc-version") (r "^0.1.0") (d #t) (k 1)))) (h "0j9x8y51fssbjyvr1x413lpclxv6y360vhcyz6q55agdih3dh22c")))

