(define-module (crates-io so ft softrender) #:use-module (crates-io))

(define-public crate-softrender-0.1.0 (c (n "softrender") (v "0.1.0") (d (list (d (n "image") (r "^0.13") (o #t) (d #t) (k 0)) (d (n "image") (r "^0.13.0") (d #t) (k 2)) (d (n "nalgebra") (r "^0.12") (d #t) (k 0)) (d (n "rayon") (r "^0.7.0") (d #t) (k 0)) (d (n "tobj") (r "^0.1.3") (d #t) (k 2)))) (h "10drxxhdywnx3a7dkkm0x2722bdbvvmk8rh3fx57ga2qsg2p0qil") (f (quote (("image_compat" "image") ("default"))))))

