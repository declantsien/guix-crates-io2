(define-module (crates-io so em soem) #:use-module (crates-io))

(define-public crate-soem-0.1.0 (c (n "soem") (v "0.1.0") (d (list (d (n "SOEM-sys") (r "^0.1.1") (d #t) (k 0)) (d (n "boolinator") (r "^2.4") (d #t) (k 0)) (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "num-derive") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1pnf6k0x2sk0vkfg1h6gwz3lxigzgw6y0vzhi22442m23cqw30gh")))

(define-public crate-soem-0.2.0 (c (n "soem") (v "0.2.0") (d (list (d (n "SOEM-sys") (r "^0.2") (d #t) (k 0)) (d (n "boolinator") (r "^2.4") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 2)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "num-derive") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1q3g18wlmhmj0mmjymi9g45nl0w2yirjha93w4g8sb1y9d84zqhz")))

(define-public crate-soem-0.3.0 (c (n "soem") (v "0.3.0") (d (list (d (n "SOEM-sys") (r "^0.2") (d #t) (k 0)) (d (n "boolinator") (r "^2.4") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 2)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "num-derive") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1f72rclg0rkpxgvdyllpni86k6w1gfbsph42dmslsijbjbvk508q")))

