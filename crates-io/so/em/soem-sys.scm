(define-module (crates-io so em soem-sys) #:use-module (crates-io))

(define-public crate-SOEM-sys-0.1.0 (c (n "SOEM-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.42.2") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1zqlfkwy9rfwn3qwfwrzzd2qb7jx60qdhdhdqay0hzldgd4cbfgq")))

(define-public crate-SOEM-sys-0.1.1 (c (n "SOEM-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.42.2") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "05hb6qvhrx7rgqc9vmm4rxnyasnm71jjj2yvqdmm7g5nyaqbqqkl")))

(define-public crate-SOEM-sys-0.1.2 (c (n "SOEM-sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.42.2") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1wcn2zwg7v8ybda11nwc8bylf235mny5sscb63807kifb1c1gkwg")))

(define-public crate-SOEM-sys-0.2.0 (c (n "SOEM-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.53.2") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0alyjnmq4hwwf2165g36hijinf6jz1cbnq1vjv2bvjjqzxbp2mbb")))

