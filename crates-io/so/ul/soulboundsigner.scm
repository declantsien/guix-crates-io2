(define-module (crates-io so ul soulboundsigner) #:use-module (crates-io))

(define-public crate-soulboundsigner-0.1.0 (c (n "soulboundsigner") (v "0.1.0") (d (list (d (n "anchor-lang") (r ">=0.22, <=0.24") (d #t) (k 0)) (d (n "anchor-spl") (r ">=0.22, <=0.24") (d #t) (k 0)) (d (n "vipers") (r "^2.0") (d #t) (k 0)))) (h "0fipyk9ccmnsizv6q38qh9zz1jbk4hzfcgb5gl9a3gyilvmp0fzj") (f (quote (("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

