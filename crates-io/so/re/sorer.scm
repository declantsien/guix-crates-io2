(define-module (crates-io so re sorer) #:use-module (crates-io))

(define-public crate-sorer-0.1.0 (c (n "sorer") (v "0.1.0") (d (list (d (n "nom") (r "^5.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0k6fbkvbw5d1yirqw0vxz8amascbzrj1bj6szf7lnvh2103bk67i")))

(define-public crate-sorer-0.1.1 (c (n "sorer") (v "0.1.1") (d (list (d (n "bytecount") (r "^0.6.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3.1") (d #t) (k 2)) (d (n "deepsize") (r "^0.1.2") (d #t) (k 0)) (d (n "easy_reader") (r "^0.5.0") (d #t) (k 0)) (d (n "nom") (r "^5.1.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.12.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.105") (f (quote ("derive"))) (d #t) (k 0)))) (h "1z0f14a3r56q0fcf753vpkrkwbabq73zzdhh6g3sqjc8a6ic1653")))

