(define-module (crates-io so re sorensen) #:use-module (crates-io))

(define-public crate-sorensen-0.1.0 (c (n "sorensen") (v "0.1.0") (d (list (d (n "assert_float_eq") (r "^1.1.3") (d #t) (k 0)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "hashbrown") (r "^0.1.8") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 2)))) (h "00dmm6vkyiv2czmh51xfccasv4qs8pnkxgfsnmbmy0kp2lgy9747") (y #t)))

(define-public crate-sorensen-0.1.1 (c (n "sorensen") (v "0.1.1") (d (list (d (n "assert_float_eq") (r "^1.1.3") (d #t) (k 0)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "hashbrown") (r "^0.1.8") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 2)))) (h "0mlfchs2ird6n2945nq7zvxxm6wq35a4j110ycy3xjbqlv953xw2") (y #t)))

(define-public crate-sorensen-0.1.2 (c (n "sorensen") (v "0.1.2") (d (list (d (n "assert_float_eq") (r "^1.1.3") (d #t) (k 0)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "hashbrown") (r "^0.1.8") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 2)))) (h "0xigxyh7lbj3xgsijibwbnqs8pypbckj2av2sfnf6h3h3f01281g") (y #t)))

(define-public crate-sorensen-0.1.3 (c (n "sorensen") (v "0.1.3") (d (list (d (n "assert_float_eq") (r "^1.1.3") (d #t) (k 0)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "hashbrown") (r "^0.1.8") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 2)))) (h "1ldh48yny4zbihxr6w4snpldrq6gs9fcccchadv3qrv7m023rkyy") (y #t)))

(define-public crate-sorensen-0.1.4 (c (n "sorensen") (v "0.1.4") (d (list (d (n "assert_float_eq") (r "^1.1.3") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "hashbrown") (r "^0.12.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0k7kl19s52yhwbz23cjdf6ib189271b011p4f8n36bkbn0fsnypz") (y #t)))

(define-public crate-sorensen-0.1.5 (c (n "sorensen") (v "0.1.5") (d (list (d (n "assert_float_eq") (r "^1.1.3") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "hashbrown") (r "^0.12.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1nkc5lmfiv8p5na47z1mgn04a8f167i9jh5nsksw97qxfif9hx34") (y #t)))

(define-public crate-sorensen-0.2.1 (c (n "sorensen") (v "0.2.1") (d (list (d (n "assert_float_eq") (r "^1.1.3") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "hashbrown") (r "^0.12.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0a7dpjmjis53w4l62v3iyx27lmsjqgf5cvw3w898y929hjqdykmj") (y #t)))

(define-public crate-sorensen-0.2.0 (c (n "sorensen") (v "0.2.0") (d (list (d (n "assert_float_eq") (r "^1.1.3") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "hashbrown") (r "^0.12.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1f7dp8cg07g9wsxzs352vq1c8bwv7wbjfc9lrgk0d9rqam9ab07l")))

