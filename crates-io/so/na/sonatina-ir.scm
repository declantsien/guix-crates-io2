(define-module (crates-io so na sonatina-ir) #:use-module (crates-io))

(define-public crate-sonatina-ir-0.0.1-alpha (c (n "sonatina-ir") (v "0.0.1-alpha") (d (list (d (n "cranelift-entity") (r "^0.77") (d #t) (k 0)) (d (n "dyn-clone") (r "^1.0.4") (d #t) (k 0)) (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "indexmap") (r "^1.9.1") (d #t) (k 0)) (d (n "primitive-types") (r "^0.10") (d #t) (k 0)) (d (n "smallvec") (r "^1.7.0") (d #t) (k 0)) (d (n "sonatina-triple") (r "^0.0.1-alpha") (d #t) (k 0)))) (h "0m9w81i73ypsj72a1k9siidavblkm0p4v642sfqxqndj35x6njbd")))

(define-public crate-sonatina-ir-0.0.2-alpha (c (n "sonatina-ir") (v "0.0.2-alpha") (d (list (d (n "cranelift-entity") (r "^0.77") (d #t) (k 0)) (d (n "dyn-clone") (r "^1.0.4") (d #t) (k 0)) (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "indexmap") (r "^1.9.1") (d #t) (k 0)) (d (n "primitive-types") (r "^0.10") (d #t) (k 0)) (d (n "smallvec") (r "^1.7.0") (d #t) (k 0)) (d (n "sonatina-triple") (r "^0.0.2-alpha") (d #t) (k 0)))) (h "1bw5rkw6js10xg64csv2jnvzlcamc86bg5wz7j7y19br24xwx59k")))

(define-public crate-sonatina-ir-0.0.3-alpha (c (n "sonatina-ir") (v "0.0.3-alpha") (d (list (d (n "cranelift-entity") (r "^0.89") (d #t) (k 0)) (d (n "dyn-clone") (r "^1.0.4") (d #t) (k 0)) (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "indexmap") (r "^1.9.1") (d #t) (k 0)) (d (n "primitive-types") (r "^0.12") (k 0)) (d (n "smallvec") (r "^1.7.0") (d #t) (k 0)) (d (n "sonatina-triple") (r "^0.0.3-alpha") (d #t) (k 0)))) (h "144x996mdp9ds649l6lgbw4xd6zip0dcsdr68732rx72dmih1m6k")))

