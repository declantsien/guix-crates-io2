(define-module (crates-io so na sonar-as-code) #:use-module (crates-io))

(define-public crate-sonar-as-code-0.0.1 (c (n "sonar-as-code") (v "0.0.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "clap-verbosity-flag") (r "^0.3.1") (d #t) (k 0)) (d (n "envsubst") (r "^0.2.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "retry") (r "^1.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.105") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.7") (d #t) (k 0)) (d (n "structopt") (r "^0.3.11") (d #t) (k 0)))) (h "0h2i8fgzklbmmw7zkw0zlw0zjk1cxskjwx4vqw1x075sgmw0jvak")))

