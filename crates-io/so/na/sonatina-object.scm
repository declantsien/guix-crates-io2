(define-module (crates-io so na sonatina-object) #:use-module (crates-io))

(define-public crate-sonatina-object-0.0.1-alpha (c (n "sonatina-object") (v "0.0.1-alpha") (h "1vaa00jvffzgk4yn08v9rlxgm35qwx1vd69d2dzn3vb7vk0nr826")))

(define-public crate-sonatina-object-0.0.2-alpha (c (n "sonatina-object") (v "0.0.2-alpha") (h "19i32w8wswj262w1gb9zaazdsbp6wd5y8dgp6mpvkk2n12vq9j7s")))

(define-public crate-sonatina-object-0.0.3-alpha (c (n "sonatina-object") (v "0.0.3-alpha") (h "0k02cy139n87d9nj948wv5wn8svklwlhl7wx03rcm7yjml19byrz")))

