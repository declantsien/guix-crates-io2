(define-module (crates-io so na sonatina-triple) #:use-module (crates-io))

(define-public crate-sonatina-triple-0.0.1-alpha (c (n "sonatina-triple") (v "0.0.1-alpha") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0vqbkbs4h8jx7xdl50wci5nmxs2gzbnq7swnha8zi964dlhg68mx")))

(define-public crate-sonatina-triple-0.0.2-alpha (c (n "sonatina-triple") (v "0.0.2-alpha") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1blimgghkx4q5a3vqfjggjg1i1fdmly5q762b9wy49d29m838r3c")))

(define-public crate-sonatina-triple-0.0.3-alpha (c (n "sonatina-triple") (v "0.0.3-alpha") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1pch4vcp5a79hq80dr21d59qyxfm5afppi8c61g69kic3m4icf74")))

