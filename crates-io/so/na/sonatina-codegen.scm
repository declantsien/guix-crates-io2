(define-module (crates-io so na sonatina-codegen) #:use-module (crates-io))

(define-public crate-sonatina-codegen-0.0.1-alpha (c (n "sonatina-codegen") (v "0.0.1-alpha") (d (list (d (n "cranelift-entity") (r "^0.77") (d #t) (k 0)) (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "primitive-types") (r "^0.10") (d #t) (k 0)) (d (n "smallvec") (r "^1.7.0") (d #t) (k 0)) (d (n "sonatina-ir") (r "^0.0.1-alpha") (d #t) (k 0)) (d (n "sonatina-triple") (r "^0.0.1-alpha") (d #t) (k 0)))) (h "13467dkrhkxxwiraiczwjdlxgjzncqnazplgyramkv2jra3rrki3")))

(define-public crate-sonatina-codegen-0.0.2-alpha (c (n "sonatina-codegen") (v "0.0.2-alpha") (d (list (d (n "cranelift-entity") (r "^0.77") (d #t) (k 0)) (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "primitive-types") (r "^0.10") (d #t) (k 0)) (d (n "smallvec") (r "^1.7.0") (d #t) (k 0)) (d (n "sonatina-ir") (r "^0.0.2-alpha") (d #t) (k 0)) (d (n "sonatina-triple") (r "^0.0.2-alpha") (d #t) (k 0)))) (h "0yl7n3in4jd3ipz4a50gvmashxc16x0wmmqdwhv8j4z0j550ks2k")))

(define-public crate-sonatina-codegen-0.0.3-alpha (c (n "sonatina-codegen") (v "0.0.3-alpha") (d (list (d (n "cranelift-entity") (r "^0.89") (d #t) (k 0)) (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "smallvec") (r "^1.7.0") (d #t) (k 0)) (d (n "sonatina-ir") (r "^0.0.3-alpha") (d #t) (k 0)) (d (n "sonatina-triple") (r "^0.0.3-alpha") (d #t) (k 0)))) (h "1m9v1gjazkv17xzcnz2z2dhvn9ql0lcw418dvc6xl100a3sjhasm")))

