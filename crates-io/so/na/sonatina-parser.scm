(define-module (crates-io so na sonatina-parser) #:use-module (crates-io))

(define-public crate-sonatina-parser-0.0.1-alpha (c (n "sonatina-parser") (v "0.0.1-alpha") (d (list (d (n "cranelift-entity") (r "^0.77") (d #t) (k 0)) (d (n "smallvec") (r "^1.7.0") (d #t) (k 0)) (d (n "sonatina-ir") (r "^0.0.1-alpha") (d #t) (k 0)) (d (n "sonatina-triple") (r "^0.0.1-alpha") (d #t) (k 0)))) (h "18a1dkhk87cx6wqvngsq00jdqvryy3lk5q4bza69f8ykdcp533pb")))

(define-public crate-sonatina-parser-0.0.2-alpha (c (n "sonatina-parser") (v "0.0.2-alpha") (d (list (d (n "cranelift-entity") (r "^0.77") (d #t) (k 0)) (d (n "smallvec") (r "^1.7.0") (d #t) (k 0)) (d (n "sonatina-ir") (r "^0.0.2-alpha") (d #t) (k 0)) (d (n "sonatina-triple") (r "^0.0.2-alpha") (d #t) (k 0)))) (h "0pv9j0jwh4db6z8gi6mxi70b1xdb98q5x6nsgr02k65h3dbddadl")))

(define-public crate-sonatina-parser-0.0.3-alpha (c (n "sonatina-parser") (v "0.0.3-alpha") (d (list (d (n "cranelift-entity") (r "^0.89") (d #t) (k 0)) (d (n "smallvec") (r "^1.7.0") (d #t) (k 0)) (d (n "sonatina-ir") (r "^0.0.3-alpha") (d #t) (k 0)) (d (n "sonatina-triple") (r "^0.0.3-alpha") (d #t) (k 0)))) (h "1rl5zv4zfbr0xcdf6id9iqgi4939hlyjphgjv1bdz631x4mj7fl2")))

