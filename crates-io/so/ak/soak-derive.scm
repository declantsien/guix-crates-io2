(define-module (crates-io so ak soak-derive) #:use-module (crates-io))

(define-public crate-soak-derive-0.1.0 (c (n "soak-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "090p6pq60lzjzfv70qqrgz1p7sqfbmmii7r1vhzzrrsj3x0cspq4")))

(define-public crate-soak-derive-0.2.0 (c (n "soak-derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "170iy8qdhydws8vcqs18qn205bmasfg223j8dm5d24gjg32wvi1n")))

