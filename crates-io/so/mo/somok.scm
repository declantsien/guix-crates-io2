(define-module (crates-io so mo somok) #:use-module (crates-io))

(define-public crate-somok-1.0.0 (c (n "somok") (v "1.0.0") (h "069ndx49vgil1bkglgr3rkjdbwfnbf47f0bj31b5b7in1did2nr5")))

(define-public crate-somok-1.1.0 (c (n "somok") (v "1.1.0") (h "1s1474bdk9wxla9wvlpaay0qgff66bi6p4p3b7npyl4xqvxg6ysb")))

(define-public crate-somok-1.1.1 (c (n "somok") (v "1.1.1") (h "1mnfqb64ayh31bx8l5bz7rsv1ki6gm9kfnpdg63iryks526r2dng")))

(define-public crate-somok-1.1.2 (c (n "somok") (v "1.1.2") (h "1ylkilwd81dgxc57yi890lj6vx0dhg69sxhj8hx7hllpwcys07fd")))

(define-public crate-somok-1.1.3 (c (n "somok") (v "1.1.3") (h "10mq7q73nggrvp4jk5sdpqlb28d6ra3z22h31148wnhlvl05a8f3")))

(define-public crate-somok-1.1.4 (c (n "somok") (v "1.1.4") (h "07pbrsawi2p4nq0nn7jmvjbi8523xg4cgb0dd0c2fchl4rbxnjc6")))

(define-public crate-somok-1.2.0 (c (n "somok") (v "1.2.0") (h "11zr2wqj3g72hd85s63zbjzci5nnik32gqlhk45gpizi2ywlr4rj")))

(define-public crate-somok-1.3.0 (c (n "somok") (v "1.3.0") (h "0zgxcds6w2w9w7zqwssjdyg151i811qslsfznx22brx2lsk937li")))

(define-public crate-somok-1.4.0 (c (n "somok") (v "1.4.0") (h "086shcn1b97rj7kpzf34zab4rmr8jwyifdr0sjljswzy35r99lg6")))

(define-public crate-somok-1.5.0 (c (n "somok") (v "1.5.0") (h "1zzial9kkgk2nxlypns3qqdzmbd5gs56x7x8gcwbz50qz4pjcqf7")))

