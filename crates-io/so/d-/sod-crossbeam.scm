(define-module (crates-io so d- sod-crossbeam) #:use-module (crates-io))

(define-public crate-sod-crossbeam-0.2.1 (c (n "sod-crossbeam") (v "0.2.1") (d (list (d (n "crossbeam") (r "^0.8.2") (d #t) (k 0)) (d (n "sod") (r "^0.2.1") (d #t) (k 0)))) (h "0w57fmihmf1lqgsjq0hwfvb1d62w18hnr9jfar3fm3nnc1i3n4ym")))

(define-public crate-sod-crossbeam-0.2.2 (c (n "sod-crossbeam") (v "0.2.2") (d (list (d (n "crossbeam") (r "^0.8.2") (d #t) (k 0)) (d (n "sod") (r "^0.2.1") (d #t) (k 0)))) (h "0a8q53w934y1bdfa5kzg57xsp14wwjkfpqhnia3zhlvhqif6jzic")))

(define-public crate-sod-crossbeam-0.2.3 (c (n "sod-crossbeam") (v "0.2.3") (d (list (d (n "crossbeam") (r "^0.8.2") (d #t) (k 0)) (d (n "sod") (r "^0.2.1") (d #t) (k 0)))) (h "1yz7v9ys6sdi1hr5dyv97lrbznmh0fqmpgfrzg8w6jvpmzan9ix9")))

(define-public crate-sod-crossbeam-0.3.1 (c (n "sod-crossbeam") (v "0.3.1") (d (list (d (n "crossbeam") (r "^0.8.2") (d #t) (k 0)) (d (n "sod") (r "^0.3.1") (d #t) (k 0)))) (h "0rajd0a5g9ns0y3nl5plb034acb1fyk7nl1gx3dm5z26g02pv5zl")))

