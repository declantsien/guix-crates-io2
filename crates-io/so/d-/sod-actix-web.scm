(define-module (crates-io so d- sod-actix-web) #:use-module (crates-io))

(define-public crate-sod-actix-web-0.2.1 (c (n "sod-actix-web") (v "0.2.1") (d (list (d (n "actix") (r "^0.13.0") (d #t) (k 0)) (d (n "actix-web") (r "^4.3.1") (d #t) (k 0)) (d (n "actix-web-actors") (r "^4.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.175") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0.175") (d #t) (k 2)) (d (n "sod") (r "^0.2.3") (d #t) (k 0)))) (h "0zmnnpirqqaw1a84wb7vdr9yq1qjwk5sj9jpggl2fh6kjh64qmlr")))

(define-public crate-sod-actix-web-0.2.2 (c (n "sod-actix-web") (v "0.2.2") (d (list (d (n "actix") (r "^0.13.0") (d #t) (k 0)) (d (n "actix-web") (r "^4.3.1") (d #t) (k 0)) (d (n "actix-web-actors") (r "^4.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.175") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0.175") (d #t) (k 2)) (d (n "sod") (r "^0.2.3") (d #t) (k 0)))) (h "1msi7ag779wk0g3k5g1q31jj5zgbg8wr2yb8r8bfwpiy5afh4rr9")))

(define-public crate-sod-actix-web-0.3.1 (c (n "sod-actix-web") (v "0.3.1") (d (list (d (n "actix") (r "^0.13.0") (d #t) (k 0)) (d (n "actix-web") (r "^4.3.1") (d #t) (k 0)) (d (n "actix-web-actors") (r "^4.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.175") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0.175") (d #t) (k 2)) (d (n "sod") (r "^0.3.1") (d #t) (k 0)))) (h "06qjk7i2s0d1sil5kllbi6z76zivjmx1kfxjb0lb1zimink5vdny")))

