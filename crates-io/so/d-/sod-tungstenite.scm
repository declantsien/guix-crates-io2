(define-module (crates-io so d- sod-tungstenite) #:use-module (crates-io))

(define-public crate-sod-tungstenite-0.2.1 (c (n "sod-tungstenite") (v "0.2.1") (d (list (d (n "sod") (r "^0.2.2") (d #t) (k 0)) (d (n "tungstenite") (r "^0.19.0") (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 2)))) (h "1j7jzwvilgsq0rcv0vnqwlpczjpc3a5hkpdbnljwcwxk8hci53ic")))

(define-public crate-sod-tungstenite-0.2.2 (c (n "sod-tungstenite") (v "0.2.2") (d (list (d (n "sod") (r "^0.2.2") (d #t) (k 0)) (d (n "tungstenite") (r "^0.19.0") (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 2)))) (h "0ak8rcdghl3z5bbw0bih7x1mpkxkl5rz6q0zn7kh0c3lcdk7hgzq")))

(define-public crate-sod-tungstenite-0.3.1 (c (n "sod-tungstenite") (v "0.3.1") (d (list (d (n "sod") (r "^0.3.1") (d #t) (k 0)) (d (n "tungstenite") (r "^0.20.1") (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 2)))) (h "0wpln3139pw52mg1xqz0zqdvrgpkwqw36i0y8qb72yqas5k5pg5c")))

(define-public crate-sod-tungstenite-0.3.2 (c (n "sod-tungstenite") (v "0.3.2") (d (list (d (n "sod") (r "^0.3.1") (d #t) (k 0)) (d (n "tungstenite") (r "^0.20.1") (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 2)))) (h "0gpg8syvvpv873l5bpxarrnh3b0anic272qfn8g5xj0gdsbxi6ng")))

(define-public crate-sod-tungstenite-0.3.3 (c (n "sod-tungstenite") (v "0.3.3") (d (list (d (n "sod") (r "^0.3.1") (d #t) (k 0)) (d (n "tungstenite") (r "^0.20.1") (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 2)))) (h "0bzyh2sv79fvancf8r5nqkkyyzbmwfv9wnlfgvad60pkpvi40fky")))

