(define-module (crates-io so d- sod-log) #:use-module (crates-io))

(define-public crate-sod-log-0.2.1 (c (n "sod-log") (v "0.2.1") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "sod") (r "^0.2.1") (d #t) (k 0)))) (h "1wl8hbix5fckxhxr2b4d5r5z7l6rhjnc2myb8qjv6z752rghv5m2")))

(define-public crate-sod-log-0.2.2 (c (n "sod-log") (v "0.2.2") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "sod") (r "^0.2.1") (d #t) (k 0)))) (h "1kghy8zbkj3dpc0a6scyhcic0xmr2bszz71w6wl3wn4xg1yjq8id")))

(define-public crate-sod-log-0.2.3 (c (n "sod-log") (v "0.2.3") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "sod") (r "^0.2.1") (d #t) (k 0)))) (h "11vs7bki3a8279ywa28grjipcmbfv05bk7i4sw7jznm5i4hgqvbm")))

(define-public crate-sod-log-0.3.1 (c (n "sod-log") (v "0.3.1") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "sod") (r "^0.3.1") (d #t) (k 0)))) (h "1fvvs0pnmx6j5m4rsb3dszqfdhcvs01q94dfxdiifzxkhk08rzy4")))

