(define-module (crates-io so d- sod-mpsc) #:use-module (crates-io))

(define-public crate-sod-mpsc-0.1.0 (c (n "sod-mpsc") (v "0.1.0") (d (list (d (n "sod") (r "^0.1.2") (d #t) (k 0)))) (h "155w61xgv0y5lfdzxij0l9112h2ypqnrzfgyaqkhk07skws855l8") (y #t)))

(define-public crate-sod-mpsc-0.1.1 (c (n "sod-mpsc") (v "0.1.1") (d (list (d (n "sod") (r "^0.1.2") (d #t) (k 0)))) (h "027lzh6vz27ci0ydnhala17hza2r4fv8prfgl50gf4zb5xlx7l5z")))

(define-public crate-sod-mpsc-0.1.2 (c (n "sod-mpsc") (v "0.1.2") (d (list (d (n "sod") (r "^0.1.2") (d #t) (k 0)))) (h "0hxm88a7vyk04hjmjnk2kjy147yxy2i0k3zvsybdqv6rmm4rzvwn")))

(define-public crate-sod-mpsc-0.2.1 (c (n "sod-mpsc") (v "0.2.1") (d (list (d (n "sod") (r "^0.2.1") (d #t) (k 0)))) (h "0fzvy5fghwljx65vhr8qkii2839m3mjxdxz8v69vflxk3q2zqiz0")))

(define-public crate-sod-mpsc-0.2.2 (c (n "sod-mpsc") (v "0.2.2") (d (list (d (n "sod") (r "^0.2.1") (d #t) (k 0)))) (h "1fzxy2i213h19asy09xn4w1dkndf36929kn68xbb0fv075q7jcpi")))

(define-public crate-sod-mpsc-0.2.3 (c (n "sod-mpsc") (v "0.2.3") (d (list (d (n "sod") (r "^0.2.1") (d #t) (k 0)))) (h "0hv4as1j68am7yrrf07c2283v4rc1yw4xh1c65jm1hyr9psi86bx")))

(define-public crate-sod-mpsc-0.3.1 (c (n "sod-mpsc") (v "0.3.1") (d (list (d (n "sod") (r "^0.3.1") (d #t) (k 0)))) (h "09ii6py8v514ikr6cwk5njyl797qz39kdhhlk5lzg72sk2yfdrp4")))

