(define-module (crates-io so d- sod-bus) #:use-module (crates-io))

(define-public crate-sod-bus-0.2.1 (c (n "sod-bus") (v "0.2.1") (d (list (d (n "bus") (r "^2.4.0") (d #t) (k 0)) (d (n "sod") (r "^0.2.1") (d #t) (k 0)))) (h "12q1m8s4sz6zlqcxl8lw5y9yyxp8lxd9vq459s2ixzflhm8ds07z")))

(define-public crate-sod-bus-0.2.2 (c (n "sod-bus") (v "0.2.2") (d (list (d (n "bus") (r "^2.4.0") (d #t) (k 0)) (d (n "sod") (r "^0.2.1") (d #t) (k 0)))) (h "1risizk8s2l1kvs4sdbjgg1drd1xgqa8d7wb1cr51kgjcgy8sa57")))

(define-public crate-sod-bus-0.2.3 (c (n "sod-bus") (v "0.2.3") (d (list (d (n "bus") (r "^2.4.0") (d #t) (k 0)) (d (n "sod") (r "^0.2.1") (d #t) (k 0)))) (h "00h0wjr3hrlsfphj6xci2si3ccv2vsc62r8zq81bap1kcqkg9vfi")))

(define-public crate-sod-bus-0.2.4 (c (n "sod-bus") (v "0.2.4") (d (list (d (n "bus") (r "^2.4.0") (d #t) (k 0)) (d (n "sod") (r "^0.2.1") (d #t) (k 0)))) (h "1w0znlz4qdd43pnpm0nqs44mb7pljj1qc6fvnq4vimv3sdh2j6a9")))

(define-public crate-sod-bus-0.3.1 (c (n "sod-bus") (v "0.3.1") (d (list (d (n "bus") (r "^2.4.0") (d #t) (k 0)) (d (n "sod") (r "^0.3.1") (d #t) (k 0)))) (h "16szggk8kinrf49fa2fm6i6j1n3j0q93dndzc3gmh065hnmi1a5q")))

