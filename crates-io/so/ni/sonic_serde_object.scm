(define-module (crates-io so ni sonic_serde_object) #:use-module (crates-io))

(define-public crate-sonic_serde_object-0.1.0 (c (n "sonic_serde_object") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.29") (d #t) (k 0)))) (h "068c6jlagj7gxnlhazafz7agnbhbilzrkxnigqji87h6z6rkzcvs")))

(define-public crate-sonic_serde_object-0.1.1 (c (n "sonic_serde_object") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.29") (d #t) (k 0)))) (h "1vp43linlv5zmp2g6magxbl34nzf72fwqgbyv8p83pjizx0a4pd1")))

(define-public crate-sonic_serde_object-0.2.1 (c (n "sonic_serde_object") (v "0.2.1") (d (list (d (n "inline-proc") (r "^0.1.0") (f (quote ("ron"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.29") (d #t) (k 0)))) (h "16pwdixjw6mk0bp3w9f40jclzljjww3nzkqxkk3z3rddp3mqlqm2")))

(define-public crate-sonic_serde_object-0.2.2 (c (n "sonic_serde_object") (v "0.2.2") (d (list (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sonic_serde_macros") (r "^0.1.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.29") (d #t) (k 0)))) (h "0vhrpw4a37jrn7i6vk04vxz65hn2n26cfpw0i7fmwcki5pi50v9f")))

(define-public crate-sonic_serde_object-0.2.3 (c (n "sonic_serde_object") (v "0.2.3") (d (list (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sonic_serde_macros") (r "^0.1.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.29") (d #t) (k 0)))) (h "1djixaf4y3ny4cfzy567f1ly6g1yxxxk3ljcnb58yjgk19xqjxq1")))

(define-public crate-sonic_serde_object-0.2.4 (c (n "sonic_serde_object") (v "0.2.4") (d (list (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sonic_serde_macros") (r "^0.1.3") (d #t) (k 0)))) (h "0qqbgkvqvlldyaw0kgg1ggmic5v6512m29ydhrbqvzhblffypxc8")))

(define-public crate-sonic_serde_object-0.2.5 (c (n "sonic_serde_object") (v "0.2.5") (d (list (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sonic_serde_macros") (r "^0.1.4") (d #t) (k 0)))) (h "0h38hxsb89vm7splgn9isj1nc7lsbmdqx7v59zj92j7z71dd3x8m")))

(define-public crate-sonic_serde_object-0.2.6 (c (n "sonic_serde_object") (v "0.2.6") (d (list (d (n "ordered-float") (r "^2.8.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sonic_serde_macros") (r "^0.1.6") (d #t) (k 0)))) (h "1ci71gz85saaiynfsb0rfvv5zx6z8921zs8x4y4m2hnqqxg82gwk")))

