(define-module (crates-io so ni sonicobject) #:use-module (crates-io))

(define-public crate-sonicobject-0.1.0 (c (n "sonicobject") (v "0.1.0") (d (list (d (n "acid-store") (r "^0.8.0") (f (quote ("store-directory"))) (d #t) (k 0)) (d (n "rmp-serde") (r "^0.15.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "sonic_serde_object") (r "^0.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.28") (d #t) (k 0)))) (h "12vz7hbdhdxpyzipgyhxmi5ai103y9z2gwg570md2py8p7qfjy6w")))

(define-public crate-sonicobject-0.1.1 (c (n "sonicobject") (v "0.1.1") (d (list (d (n "acid-store") (r "^0.8.0") (f (quote ("store-directory"))) (d #t) (k 0)) (d (n "rmp-serde") (r "^0.15.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "sonic_serde_object") (r "^0.1.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.28") (d #t) (k 0)))) (h "0xplz5h43d18f1b87yyz3x1xizf6s122xsqdsrlw6jvxpv0fpi37")))

(define-public crate-sonicobject-0.2.0 (c (n "sonicobject") (v "0.2.0") (d (list (d (n "rmp-serde") (r "^0.15.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sonic_serde_object") (r "^0.2.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.28") (d #t) (k 0)))) (h "0g1fy4jsv7y8v3whz7xibybya4k240fk3adm7amrhik3xlm972va")))

(define-public crate-sonicobject-0.2.1 (c (n "sonicobject") (v "0.2.1") (d (list (d (n "rmp-serde") (r "^0.15.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sonic_serde_object") (r "^0.2.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.28") (d #t) (k 0)))) (h "1qxpi8jff1k2vp54jnm1vb6xkb93pcfw8zm33j3hf3sfsr84qghg")))

(define-public crate-sonicobject-0.2.2 (c (n "sonicobject") (v "0.2.2") (d (list (d (n "rmp-serde") (r "^0.15.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sonic_serde_object") (r "^0.2.6") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.28") (d #t) (k 0)))) (h "0f83mfnnfpzjsqblgk09s692zd8kmzgsj9izzs56wf0amkrc3vfm")))

