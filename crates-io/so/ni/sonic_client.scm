(define-module (crates-io so ni sonic_client) #:use-module (crates-io))

(define-public crate-sonic_client-0.1.0 (c (n "sonic_client") (v "0.1.0") (d (list (d (n "mio") (r "^0.6.16") (d #t) (k 0)))) (h "0hnjnbrl5xi0scjc3ad5ykp6xlidzwqrhr933qsmnll44mb4akbg")))

(define-public crate-sonic_client-0.1.1 (c (n "sonic_client") (v "0.1.1") (d (list (d (n "mio") (r "^0.6.16") (d #t) (k 0)))) (h "119ky03rvmp5r51ilwbnvr34is5qjf2bw937yynzypiddr7gvj9x")))

(define-public crate-sonic_client-0.1.2 (c (n "sonic_client") (v "0.1.2") (d (list (d (n "mio") (r "^0.6.16") (d #t) (k 0)))) (h "0bjj0zgha6304lyni1zfsr74h5q2ja9bhs3kmyr5n25zh4j5c0dd")))

