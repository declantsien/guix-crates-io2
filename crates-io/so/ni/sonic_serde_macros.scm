(define-module (crates-io so ni sonic_serde_macros) #:use-module (crates-io))

(define-public crate-sonic_serde_macros-0.1.0 (c (n "sonic_serde_macros") (v "0.1.0") (h "0bsdgmhd5a1j5ckfjsq52ah2yy0w06djfxawf0w1r0f17m0dfzqx")))

(define-public crate-sonic_serde_macros-0.1.1 (c (n "sonic_serde_macros") (v "0.1.1") (h "0yjvjx02j4ykqwbxq3ijfpyqmyy3nm4ddkbjg3ay9l2n041lfqxv")))

(define-public crate-sonic_serde_macros-0.1.2 (c (n "sonic_serde_macros") (v "0.1.2") (h "05ddrh45nrhmkxdvdyl0c3m4v86yr9pspfdxjc64nhrp0vml6pkr")))

(define-public crate-sonic_serde_macros-0.1.3 (c (n "sonic_serde_macros") (v "0.1.3") (h "16g6yd3kwqyskpgkqdndci0n17jgr3ab393ni01xn307kiydbiqq")))

(define-public crate-sonic_serde_macros-0.1.4 (c (n "sonic_serde_macros") (v "0.1.4") (h "1glnwc6m77dfs01nskm4iyxkj0vzl85arbba8jk6271m9q257n61")))

(define-public crate-sonic_serde_macros-0.1.5 (c (n "sonic_serde_macros") (v "0.1.5") (d (list (d (n "ordered-float") (r "^2.8.0") (f (quote ("serde"))) (d #t) (k 0)))) (h "0h378nycqlc4gr3ja2xql5k9gzlbj68nlwlvz7fkv8bd47gisy0x")))

(define-public crate-sonic_serde_macros-0.1.6 (c (n "sonic_serde_macros") (v "0.1.6") (h "11nwi9dmyjlbd2mswlnlaxp7rsgvhv3g5wc604hnjgw3bmm96qkz")))

