(define-module (crates-io so ni sonic_spin) #:use-module (crates-io))

(define-public crate-sonic_spin-0.1.0 (c (n "sonic_spin") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4.4") (k 0)) (d (n "quote") (r "^0.6.11") (d #t) (k 0)) (d (n "syn") (r "= 0.15.30") (f (quote ("full" "derive" "parsing" "clone-impls" "printing"))) (d #t) (k 0) (p "syn-pub-items")))) (h "04l81m2kn2wdhagc4ilv24vlsg3mrik79r914xzxmkr5zr9ikdb0") (f (quote (("visit-mut") ("visit") ("printing") ("parsing") ("full") ("fold") ("extra-traits") ("derive") ("default" "full" "derive" "parsing" "clone-impls" "printing") ("clone-impls"))))))

