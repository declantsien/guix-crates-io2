(define-module (crates-io so ni sonic-channel) #:use-module (crates-io))

(define-public crate-sonic-channel-0.1.0-rc1 (c (n "sonic-channel") (v "0.1.0-rc1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.4") (d #t) (k 0)))) (h "105g3kxmhsqz79lw5fmd97kyaa3skf5rqz5b02jqrqk6ph8nvd7f") (f (quote (("search") ("ingest") ("default" "search") ("control")))) (y #t)))

(define-public crate-sonic-channel-0.1.0-rc2 (c (n "sonic-channel") (v "0.1.0-rc2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.4") (d #t) (k 0)))) (h "1mhdgz5f092j31d9h6802rns7qxn262visqxq8lsmzhvhwaxs0cl") (f (quote (("search") ("ingest") ("default" "search") ("control")))) (y #t)))

(define-public crate-sonic-channel-0.1.0-rc3 (c (n "sonic-channel") (v "0.1.0-rc3") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.4") (d #t) (k 0)))) (h "1iksvwwb4xcj2q94ml91v9h6qhfy7qbzjikj2qxlzvmhgwfalf1x") (f (quote (("search") ("ingest") ("default" "search") ("control")))) (y #t)))

(define-public crate-sonic-channel-0.1.0 (c (n "sonic-channel") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.4") (d #t) (k 0)))) (h "12kgha6cdxjxwxvmmk2xxbkvq04xkn5q23vjlrn3wrj0rdwiiby6") (f (quote (("search") ("ingest") ("default" "search") ("control"))))))

(define-public crate-sonic-channel-0.2.0 (c (n "sonic-channel") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.4") (o #t) (d #t) (k 0)))) (h "03nbz53iqczjahc02apw5bmz91isdldmcmwqj20bpf83x052v8ap") (f (quote (("search" "regex") ("ingest" "regex") ("default" "search") ("control"))))))

(define-public crate-sonic-channel-0.2.1 (c (n "sonic-channel") (v "0.2.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.4") (o #t) (d #t) (k 0)))) (h "1d0n1c98yl43c4bvkqqnkgp97dc4p785lzb9a97c2jk5rzpd1raw") (f (quote (("search" "regex") ("ingest" "regex") ("default" "search") ("control"))))))

(define-public crate-sonic-channel-0.2.2 (c (n "sonic-channel") (v "0.2.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.4") (d #t) (k 0)))) (h "0fh24nsljr6pn55cfgmy5hfjp24iig438x1yfnsy5mimy91w9r73") (f (quote (("search") ("ingest") ("default" "search") ("control"))))))

(define-public crate-sonic-channel-0.3.0 (c (n "sonic-channel") (v "0.3.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.4") (d #t) (k 0)))) (h "0s1rqknzdy6g64q158abcvrprbrix6l3l82sv8byqy6mrxvb4jlc") (f (quote (("search") ("ingest") ("default" "search") ("control"))))))

(define-public crate-sonic-channel-0.3.1 (c (n "sonic-channel") (v "0.3.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.4") (d #t) (k 0)))) (h "1s8gqyb1cyy53crc8ifv1qndph9s8q80kplq0aim29hjfji31mri") (f (quote (("search") ("ingest") ("default" "search") ("control"))))))

(define-public crate-sonic-channel-0.3.2 (c (n "sonic-channel") (v "0.3.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.4") (d #t) (k 0)))) (h "0jdkph38wmcw2lampfyv022v9a2fc8j2gyrqly40affpfiy8q2h5") (f (quote (("search") ("ingest") ("default" "search") ("control"))))))

(define-public crate-sonic-channel-0.4.0 (c (n "sonic-channel") (v "0.4.0") (d (list (d (n "lazy_static") (r ">=1.4.0, <2.0.0") (d #t) (k 0)) (d (n "regex") (r ">=1.3.4, <2.0.0") (d #t) (k 0)))) (h "1nnpyd42rfva6wvafb6nw9ynj21nyij2y1kf0qswc0b89i2dcph8") (f (quote (("search") ("ingest") ("default" "search") ("control"))))))

(define-public crate-sonic-channel-0.4.1 (c (n "sonic-channel") (v "0.4.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.4") (d #t) (k 0)))) (h "19xbfh0h7a6n3r2cj8h99qqgjvbji62wxwiz6pi7pcqlhhpx3xs0") (f (quote (("search") ("ingest") ("default" "search") ("control"))))))

(define-public crate-sonic-channel-0.4.2 (c (n "sonic-channel") (v "0.4.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.4") (d #t) (k 0)))) (h "1622x0zjk197s4iac2kj5y7ppmp790lsn3mdd60smkynq09h5vdj") (f (quote (("search") ("ingest") ("default" "search") ("control"))))))

(define-public crate-sonic-channel-0.5.0 (c (n "sonic-channel") (v "0.5.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.4") (d #t) (k 0)))) (h "10f95bbsdgf9dc060zs1n41anzsnq4hbc7dn4bxn7j72n9szrvn9") (f (quote (("search") ("ingest") ("default" "search") ("control"))))))

(define-public crate-sonic-channel-0.6.0 (c (n "sonic-channel") (v "0.6.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.4") (d #t) (k 0)) (d (n "whatlang") (r "^0.12.0") (d #t) (k 0)))) (h "00jbpfkyps689s6r6r3vpxszcrfzc8sx41p1r7s9p89ay750l3an") (f (quote (("search") ("ingest") ("default" "search") ("control"))))))

(define-public crate-sonic-channel-1.0.0 (c (n "sonic-channel") (v "1.0.0") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "whatlang") (r "^0.12.0") (d #t) (k 0)))) (h "0qhjlfr1iqa145w0add1y74s1cy9009235wb0xgbzdjrwhrhjlrz") (f (quote (("search") ("ingest") ("default" "search") ("control"))))))

(define-public crate-sonic-channel-1.0.1 (c (n "sonic-channel") (v "1.0.1") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "whatlang") (r "^0.12.0") (d #t) (k 0)))) (h "1fgzynqbigww5ljh9sn4na6gjd637900dlwzyjmv28x2ihd8pq9d") (f (quote (("search") ("ingest") ("default" "search") ("control"))))))

(define-public crate-sonic-channel-1.1.0 (c (n "sonic-channel") (v "1.1.0") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "whatlang") (r "^0.16.1") (d #t) (k 0)))) (h "0bjp3jlm1xqq394m8d790mnvziadwb8hbqh5vvyfn7yhrn294vb3") (f (quote (("search") ("ingest") ("default" "search") ("control"))))))

