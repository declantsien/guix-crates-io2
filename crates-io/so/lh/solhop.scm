(define-module (crates-io so lh solhop) #:use-module (crates-io))

(define-public crate-solhop-0.1.0 (c (n "solhop") (v "0.1.0") (d (list (d (n "msat") (r "=0.1.1") (d #t) (k 0)) (d (n "rsat") (r "=0.1.12") (d #t) (k 0)) (d (n "solhop-types") (r "=0.1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1fwrcgxn3a1j1yrz256516j1h64f646b2lg5rijybwprkrqbs43k")))

