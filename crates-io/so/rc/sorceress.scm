(define-module (crates-io so rc sorceress) #:use-module (crates-io))

(define-public crate-sorceress-0.1.0 (c (n "sorceress") (v "0.1.0") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)) (d (n "rosc") (r "^0.4.2") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)))) (h "0vyijq7n45r1bwzwpzxrw3hn482ak9ma09q3876rcxc8fv1jhv6q")))

(define-public crate-sorceress-0.2.0 (c (n "sorceress") (v "0.2.0") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)) (d (n "rosc") (r "^0.4.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.124") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "14d5gvb1jpk118b3ni7vyk064zlj9avd3184p2a9vwpg6k1xjg8i")))

