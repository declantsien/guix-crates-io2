(define-module (crates-io so a- soa-vec) #:use-module (crates-io))

(define-public crate-soa-vec-0.1.0 (c (n "soa-vec") (v "0.1.0") (d (list (d (n "second-stack") (r "^0.2.0") (f (quote ("experimental"))) (d #t) (k 0)) (d (n "testdrop") (r "^0.1.2") (d #t) (k 2)))) (h "1pqjcmcggfgs7w671vc53p0a3fcq2dsz723g47p7n6xc38zzklv1")))

(define-public crate-soa-vec-0.2.0 (c (n "soa-vec") (v "0.2.0") (d (list (d (n "second-stack") (r "^0.2.0") (f (quote ("experimental"))) (d #t) (k 0)) (d (n "testdrop") (r "^0.1.2") (d #t) (k 2)))) (h "0cxq0rn0cw5zrif0jzcnn6yql7n2pjq5s0yjzg2f809a11jsg6dd")))

(define-public crate-soa-vec-0.3.0 (c (n "soa-vec") (v "0.3.0") (d (list (d (n "second-stack") (r "^0.2.0") (f (quote ("experimental"))) (d #t) (k 0)) (d (n "testdrop") (r "^0.1.2") (d #t) (k 2)))) (h "11438pln8rrg91ndnr0qmq8scvj1f8xh70ixbv6p26v7d92c3rgb")))

(define-public crate-soa-vec-0.4.0 (c (n "soa-vec") (v "0.4.0") (d (list (d (n "second-stack") (r "^0.2.0") (f (quote ("experimental"))) (d #t) (k 0)) (d (n "testdrop") (r "^0.1.2") (d #t) (k 2)))) (h "1xpzcaql6lchhxmn1qrqyy3vm8hd7licczd8b4385x3sl2d6rnmh")))

(define-public crate-soa-vec-0.5.0 (c (n "soa-vec") (v "0.5.0") (d (list (d (n "second-stack") (r "^0.2.0") (f (quote ("experimental"))) (d #t) (k 0)) (d (n "testdrop") (r "^0.1.2") (d #t) (k 2)))) (h "0vvif24vr7n36w99jkfxnlk81r1zmg2lciafh1vs190yr8jqpvfb")))

