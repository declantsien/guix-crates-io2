(define-module (crates-io so a- soa-rs) #:use-module (crates-io))

(define-public crate-soa-rs-0.3.0 (c (n "soa-rs") (v "0.3.0") (d (list (d (n "soa-rs-derive") (r "^0.3.0") (d #t) (k 0)))) (h "1xakljbvmnbx6npqwzsb5rwr1k29b52rm4p9hglvpja8y4spfp32")))

(define-public crate-soa-rs-0.3.1 (c (n "soa-rs") (v "0.3.1") (d (list (d (n "soa-rs-derive") (r "^0.3.1") (d #t) (k 0)))) (h "0piwzga99yw02rzwmsdpwlbaa9za898d4fmkbg3ik8zmbzlmif2f")))

(define-public crate-soa-rs-0.4.0 (c (n "soa-rs") (v "0.4.0") (d (list (d (n "soa-rs-derive") (r "^0.4.0") (d #t) (k 0)))) (h "19pvl40hmgr6987q9424f5jy4yki6k36i9zmzvqsd5lh95z1zqg2")))

(define-public crate-soa-rs-0.5.0 (c (n "soa-rs") (v "0.5.0") (d (list (d (n "serde") (r "^1.0.199") (o #t) (d #t) (k 0)) (d (n "soa-rs-derive") (r "^0.4.0") (d #t) (k 0)))) (h "15qpg1n9m6dznndf8g8pqbvw7mz0vyr1ffk0r6knfbcfjamjngwa") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-soa-rs-0.5.1 (c (n "soa-rs") (v "0.5.1") (d (list (d (n "serde") (r "^1.0.199") (o #t) (d #t) (k 0)) (d (n "soa-rs-derive") (r "^0.4.0") (d #t) (k 0)))) (h "1p7nf7gmvwnpl21whsk7svyrsl0ia9vlgasjk2i52hw9mhsiamy8") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-soa-rs-0.6.0 (c (n "soa-rs") (v "0.6.0") (d (list (d (n "serde") (r "^1.0.199") (o #t) (d #t) (k 0)) (d (n "soa-rs-derive") (r "^0.6.0") (d #t) (k 0)))) (h "0n536xc7188d2msmk6hqil233xbdlk6zikxhqal1x2ngfd47381h") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-soa-rs-0.6.1 (c (n "soa-rs") (v "0.6.1") (d (list (d (n "serde") (r "^1.0.199") (o #t) (d #t) (k 0)) (d (n "soa-rs-derive") (r "^0.6.0") (d #t) (k 0)))) (h "14ydga3hbjp55k59m2w58z8lgk8l5qckx8l2dd0hv9h9zxfnhxjw") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

