(define-module (crates-io so a- soa-rs-derive) #:use-module (crates-io))

(define-public crate-soa-rs-derive-0.3.0 (c (n "soa-rs-derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0qi1kvkbk50dmm16dkwyf3n2587fr2gykh76jxf8gfap6m3lh69w")))

(define-public crate-soa-rs-derive-0.3.1 (c (n "soa-rs-derive") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0xj58kg1xsk4v5hr1hs1xw6sq88xgrppsxafhbh6xz4q2zg2h8s2")))

(define-public crate-soa-rs-derive-0.4.0 (c (n "soa-rs-derive") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0wb9clpah2kym4ggnwdiwcsvhhkmq5q5s6nn8p9nk4yjjlvzlaf7")))

(define-public crate-soa-rs-derive-0.6.0 (c (n "soa-rs-derive") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0xqmhg53s96dc3qil3l3krqi5b5bzybg5nnahipcrrm7bk91ya54")))

