(define-module (crates-io so fa sofa-sys) #:use-module (crates-io))

(define-public crate-sofa-sys-2020.7.21-beta.1 (c (n "sofa-sys") (v "2020.7.21-beta.1") (d (list (d (n "bindgen") (r "^0.56.0") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0.66") (d #t) (k 1)))) (h "042pmaaa9kszlnf6s6gcb8mfp17sff0nvvbfrd6aqys4hwfw8q7k") (f (quote (("generate" "bindgen")))) (y #t) (l "sofa_c")))

(define-public crate-sofa-sys-2020.7.21-beta.2 (c (n "sofa-sys") (v "2020.7.21-beta.2") (d (list (d (n "bindgen") (r "^0.56.0") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0.66") (d #t) (k 1)))) (h "1dq0gy0b1wa4j6sy4cgm5gfs1mb4i3m0wvw3h4dmcj579lh2yzs1") (f (quote (("generate" "bindgen")))) (l "sofa_c")))

