(define-module (crates-io so fa sofa) #:use-module (crates-io))

(define-public crate-sofa-0.5.1 (c (n "sofa") (v "0.5.1") (d (list (d (n "pretty_assertions") (r "^0.5") (d #t) (k 2)) (d (n "reqwest") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "167fj5kn79xa1cb1i4k6idf6hz2ry4nmcwzy49vbppqy47v8iqnd")))

(define-public crate-sofa-0.5.2 (c (n "sofa") (v "0.5.2") (d (list (d (n "pretty_assertions") (r "^0.5") (d #t) (k 2)) (d (n "reqwest") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0wazyrs1l1hm7kk9jc87ampycf2j6vphc3icp6rapqbkqajkcqg8")))

(define-public crate-sofa-0.6.0 (c (n "sofa") (v "0.6.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.5") (d #t) (k 2)) (d (n "reqwest") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "05i04hmf3k6k5q2fw6kb1y7syxkxxfa8vg6hiqxkmvrmsxq3vids")))

