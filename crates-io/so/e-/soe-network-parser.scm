(define-module (crates-io so e- soe-network-parser) #:use-module (crates-io))

(define-public crate-soe-network-parser-1.0.0 (c (n "soe-network-parser") (v "1.0.0") (d (list (d (n "clap") (r "^3.2.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "h1emu-core") (r "^0.8.14") (f (quote ("soeprotocol"))) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "path-absolutize") (r "^3.0.13") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.137") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)))) (h "0bxqwpmm119andkkndq5v9flyz391cyns7dxm6zxsd9fcaspj3cb")))

