(define-module (crates-io so la solana_anchor_gateway) #:use-module (crates-io))

(define-public crate-solana_anchor_gateway-2.0.0 (c (n "solana_anchor_gateway") (v "2.0.0") (d (list (d (n "anchor-lang") (r "^0.25.0") (d #t) (k 0)) (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "borsh") (r "^0.9.3") (d #t) (k 0)))) (h "1fhgajskbpp3g14wj635fqnqr5g2lizw7j9ysyi7rbnxd3z6j1vz") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint")))) (y #t)))

(define-public crate-solana_anchor_gateway-2.0.1 (c (n "solana_anchor_gateway") (v "2.0.1") (d (list (d (n "anchor-lang") (r "^0.25.0") (d #t) (k 0)) (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "borsh") (r "^0.9.3") (d #t) (k 0)))) (h "0lgyl3q0q2cqly779xwhxyawviw9kwm1yhd1gxj1vnmwy5i26ncc") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-solana_anchor_gateway-2.0.2 (c (n "solana_anchor_gateway") (v "2.0.2") (d (list (d (n "anchor-lang") (r "^0.25.0") (d #t) (k 0)) (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "borsh") (r "^0.9.3") (d #t) (k 0)))) (h "0igid47kcyq81sx1f9szdnhi83dmaqy6j3ljaipwk2rgnb1740pd") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-solana_anchor_gateway-2.0.3 (c (n "solana_anchor_gateway") (v "2.0.3") (d (list (d (n "anchor-lang") (r "^0.26.0") (d #t) (k 0)) (d (n "anchor-spl") (r "^0.26.0") (d #t) (k 0)) (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "borsh") (r "^0.9.3") (d #t) (k 0)) (d (n "solana-program") (r "^1.14.11") (d #t) (k 0)) (d (n "spl-token") (r "^3.3.1") (d #t) (k 0)))) (h "0r0dcrivhhp78v9wi2m0lh01cqslmpy355mcv99ir3f9ia4kcdsv") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-solana_anchor_gateway-2.0.5 (c (n "solana_anchor_gateway") (v "2.0.5") (d (list (d (n "anchor-lang") (r "^0.26.0") (d #t) (k 0)) (d (n "anchor-spl") (r "^0.26.0") (d #t) (k 0)) (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "borsh") (r "^0.9.3") (d #t) (k 0)) (d (n "solana-program") (r "^1.14.11") (d #t) (k 0)) (d (n "spl-token") (r "^3.3.1") (d #t) (k 0)))) (h "0bmf84bcpvxbavw57f9zpi4zfs8flxx8wd9h0b1kcvw17vlgzpag") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

