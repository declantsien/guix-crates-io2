(define-module (crates-io so la solana_libra_failure_macros) #:use-module (crates-io))

(define-public crate-solana_libra_failure_macros-0.0.0-sol7 (c (n "solana_libra_failure_macros") (v "0.0.0-sol7") (h "1sw7d4yvr5mznkb82q7iqiypjfh29f2ajnqnib318ln4c4ypc1gj") (y #t)))

(define-public crate-solana_libra_failure_macros-0.0.0-sol8 (c (n "solana_libra_failure_macros") (v "0.0.0-sol8") (h "1grx1636ni762garzgrc4xqpppmj40w6cvnglwqpgqiq65kzqisd") (y #t)))

(define-public crate-solana_libra_failure_macros-0.0.0-sol9 (c (n "solana_libra_failure_macros") (v "0.0.0-sol9") (h "03ghjmdgfq7qgfs6x9kjdk1b17v9wn4nf3bspv8p46zn7gn70n89") (y #t)))

(define-public crate-solana_libra_failure_macros-0.0.0-sol10 (c (n "solana_libra_failure_macros") (v "0.0.0-sol10") (h "0pjrzy6w0dd2bq1s9yyshx6hhmxnqj48qfrqsvwlv1vnb0v2jhk6") (y #t)))

(define-public crate-solana_libra_failure_macros-0.0.0-sol11 (c (n "solana_libra_failure_macros") (v "0.0.0-sol11") (h "1zw63bxzz1d5n54c25ddvxy0hgl5qr8cccvdzs5mz5483qmpsqqm") (y #t)))

(define-public crate-solana_libra_failure_macros-0.0.0-sol12 (c (n "solana_libra_failure_macros") (v "0.0.0-sol12") (h "14wf6cnh36jakcr5f6wvbkd2w692a282wqakh87bvf2rnivjgnc5") (y #t)))

(define-public crate-solana_libra_failure_macros-0.0.0-sol13 (c (n "solana_libra_failure_macros") (v "0.0.0-sol13") (h "0qw9j1y7n3rjrqav6zbcsvbkg827m474da6p9kx3nlzyknifw3sc") (y #t)))

(define-public crate-solana_libra_failure_macros-0.0.0-sol14 (c (n "solana_libra_failure_macros") (v "0.0.0-sol14") (h "07r6hirvvm6cpwcbpyczxzpjf66pw5wy3ajl4hy826bq4vrplbz1") (y #t)))

(define-public crate-solana_libra_failure_macros-0.0.0-sol15 (c (n "solana_libra_failure_macros") (v "0.0.0-sol15") (h "13bxil35pmyphiyk28vx57wxysvrqqz731af332qf08j190yr01h")))

(define-public crate-solana_libra_failure_macros-0.0.0 (c (n "solana_libra_failure_macros") (v "0.0.0") (h "10wrwcwvpa8pirf5xxqvskxb95xjh84h05s6w7nvps2qfppfw6h6")))

(define-public crate-solana_libra_failure_macros-0.0.1-sol2 (c (n "solana_libra_failure_macros") (v "0.0.1-sol2") (h "1y2hyn1sbc0xjxrshj6dgwsih3c0p3pmw5s1q504rc1nr0cnsm5l")))

(define-public crate-solana_libra_failure_macros-0.0.1-sol3 (c (n "solana_libra_failure_macros") (v "0.0.1-sol3") (h "0srf23rvlm89lx3chxgw8xravql54dm9fh6crf148nv3m7ls1lga")))

(define-public crate-solana_libra_failure_macros-0.0.1-sol4 (c (n "solana_libra_failure_macros") (v "0.0.1-sol4") (h "0zk06h875l651a50kymgd16cnd0xalkr8wbl01zw2lx8c50jpyzr")))

(define-public crate-solana_libra_failure_macros-0.0.1-sol5 (c (n "solana_libra_failure_macros") (v "0.0.1-sol5") (h "1bqnsxr6hwzin5nhvxb5svpnvwajyc2n45ig4d0yb2y6cs4x7wab")))

