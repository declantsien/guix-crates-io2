(define-module (crates-io so la solarti-discriminator-derive) #:use-module (crates-io))

(define-public crate-solarti-discriminator-derive-0.1.1 (c (n "solarti-discriminator-derive") (v "0.1.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "solarti-discriminator-syn") (r "^0.1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0ddb6jvapc4jgpglw8yh7ngns2i1nqwsbnih463nz5jq8a9shsfi")))

(define-public crate-solarti-discriminator-derive-0.1.2 (c (n "solarti-discriminator-derive") (v "0.1.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "solarti-discriminator-syn") (r "^0.1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0kg3gkg7b93vfvnxbvdih3rg9clzd3i4ihikaf0qppwl6k7kvkmn")))

(define-public crate-solarti-discriminator-derive-0.1.3 (c (n "solarti-discriminator-derive") (v "0.1.3") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "solarti-discriminator-syn") (r "^0.1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0vbi9crg6a9d5bzjdn8rh0lmpf183zqxsydlm62qwg60c2mwqabp")))

(define-public crate-solarti-discriminator-derive-0.1.4 (c (n "solarti-discriminator-derive") (v "0.1.4") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "solarti-discriminator-syn") (r "^0.1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0pfry86alxi0yiykglinv3h79k4v0pgfzdr7g55k4446lg08hzgm")))

(define-public crate-solarti-discriminator-derive-0.1.5 (c (n "solarti-discriminator-derive") (v "0.1.5") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "solarti-discriminator-syn") (r "^0.1.5") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "12iad4c7fxa0xkxx3vs1mdgkvf8rd4ar89j64zyib4p7piha8yx4")))

(define-public crate-solarti-discriminator-derive-0.1.6 (c (n "solarti-discriminator-derive") (v "0.1.6") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "solarti-discriminator-syn") (r "^0.1.6") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0pqh875cigg48yjr2sg276l9wf8rz84gr9lq5cbymdj643zb1r21")))

