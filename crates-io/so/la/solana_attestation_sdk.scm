(define-module (crates-io so la solana_attestation_sdk) #:use-module (crates-io))

(define-public crate-solana_attestation_sdk-0.1.0 (c (n "solana_attestation_sdk") (v "0.1.0") (d (list (d (n "anchor-lang") (r "^0.27.0") (d #t) (k 0)) (d (n "bytemuck") (r "^1.7.2") (d #t) (k 0)) (d (n "solana-program") (r "^1.14.18") (d #t) (k 0)))) (h "1xwr7zfiazagqbrf636dv8qb2a2xd4grwwg2875dsmwnpad1fala")))

