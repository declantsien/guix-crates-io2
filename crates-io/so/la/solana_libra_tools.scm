(define-module (crates-io so la solana_libra_tools) #:use-module (crates-io))

(define-public crate-solana_libra_tools-0.0.1-sol3 (c (n "solana_libra_tools") (v "0.0.1-sol3") (d (list (d (n "hex") (r "^0.3.2") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)))) (h "073ijwzkc6vll7p343dmr5m87kfcb9simggpmyph3dd45s3hwp5r")))

(define-public crate-solana_libra_tools-0.0.1-sol4 (c (n "solana_libra_tools") (v "0.0.1-sol4") (d (list (d (n "hex") (r "^0.3.2") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)))) (h "0p2vblr39dmspim0p3v54rrbgr2jf8l6nfmps7fh6yq99alc852j")))

(define-public crate-solana_libra_tools-0.0.1-sol5 (c (n "solana_libra_tools") (v "0.0.1-sol5") (d (list (d (n "hex") (r "^0.3.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 0)))) (h "0pm0hxxz77zzsr2akr342grnpa45q9jw1jirhsjlbzjcslnmlhdk")))

