(define-module (crates-io so la solana-program-types) #:use-module (crates-io))

(define-public crate-solana-program-types-0.1.0 (c (n "solana-program-types") (v "0.1.0") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "solana-program") (r "^1.18") (o #t) (d #t) (k 0)))) (h "15yp6c4r6k7hw7n81n4d408wgxh1dskkkjxz1by1g7qll411y1gs") (f (quote (("default" "legacy")))) (s 2) (e (quote (("legacy" "dep:solana-program"))))))

