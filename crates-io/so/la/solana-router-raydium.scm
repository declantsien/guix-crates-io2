(define-module (crates-io so la solana-router-raydium) #:use-module (crates-io))

(define-public crate-solana-router-raydium-1.0.0 (c (n "solana-router-raydium") (v "1.0.0") (d (list (d (n "arrayref") (r "^0.3.6") (d #t) (k 0)) (d (n "solana-farm-sdk") (r "^1.0.0") (d #t) (k 0)) (d (n "solana-program") (r "^1.9.18") (d #t) (k 0)) (d (n "solana-program-test") (r "^1.9.18") (d #t) (k 2)))) (h "10v0i48w50hn0qz5afhy2c6kzais6sccyf63jcwdzmaja25h11in") (f (quote (("no-entrypoint") ("debug")))) (y #t)))

(define-public crate-solana-router-raydium-1.1.0 (c (n "solana-router-raydium") (v "1.1.0") (d (list (d (n "arrayref") (r "^0.3.6") (d #t) (k 0)) (d (n "solana-farm-sdk") (r "^1.1.0") (d #t) (k 0)) (d (n "solana-program") (r "^1.9.18") (d #t) (k 0)) (d (n "solana-program-test") (r "^1.9.18") (d #t) (k 2)) (d (n "solana-security-txt") (r "^1.0.1") (d #t) (k 0)))) (h "0ygqlnp36w05xbbkfmcp292fvrm8vw4fb7cjmhqh7316py88lz2c") (f (quote (("no-entrypoint") ("debug")))) (y #t)))

(define-public crate-solana-router-raydium-1.1.1 (c (n "solana-router-raydium") (v "1.1.1") (d (list (d (n "arrayref") (r "^0.3.6") (d #t) (k 0)) (d (n "solana-farm-sdk") (r "^1.1.1") (d #t) (k 0)) (d (n "solana-program") (r "^1.9.18") (d #t) (k 0)) (d (n "solana-program-test") (r "^1.9.18") (d #t) (k 2)) (d (n "solana-security-txt") (r "^1.0.1") (d #t) (k 0)))) (h "0x0jmd3g0h6jsmij9ahdr95nfivlzvjpj64h36xkynis7s0avy2w") (f (quote (("no-entrypoint") ("debug"))))))

(define-public crate-solana-router-raydium-1.1.2 (c (n "solana-router-raydium") (v "1.1.2") (d (list (d (n "arrayref") (r "^0.3.6") (d #t) (k 0)) (d (n "solana-farm-sdk") (r "^1.1.2") (d #t) (k 0)) (d (n "solana-program") (r "^1.9.18") (d #t) (k 0)) (d (n "solana-program-test") (r "^1.9.18") (d #t) (k 2)) (d (n "solana-security-txt") (r "^1.0.1") (d #t) (k 0)))) (h "1i10wa0f65gniq6nzzw811lyv6mixykbgdfmqgr74z2ck57b8lj1") (f (quote (("no-entrypoint") ("debug"))))))

(define-public crate-solana-router-raydium-1.1.3 (c (n "solana-router-raydium") (v "1.1.3") (d (list (d (n "arrayref") (r "^0.3.6") (d #t) (k 0)) (d (n "solana-farm-sdk") (r "^1.1.3") (d #t) (k 0)) (d (n "solana-program") (r "^1.9.18") (d #t) (k 0)) (d (n "solana-program-test") (r "^1.9.18") (d #t) (k 2)) (d (n "solana-security-txt") (r "^1.0.1") (d #t) (k 0)))) (h "1f4864jn78sylw1iw53jhjngqpfgp4lz502m89q7zhr54r2lqd52") (f (quote (("no-entrypoint") ("debug"))))))

