(define-module (crates-io so la solana_libra_vm_cache_map) #:use-module (crates-io))

(define-public crate-solana_libra_vm_cache_map-0.0.0-sol7 (c (n "solana_libra_vm_cache_map") (v "0.0.0-sol7") (d (list (d (n "chashmap") (r "^2.2.2") (d #t) (k 0)) (d (n "crossbeam") (r "^0.7") (d #t) (k 2)) (d (n "proptest") (r "^0.9") (d #t) (k 2)) (d (n "rand") (r "^0.6.5") (d #t) (k 2)) (d (n "typed-arena") (r "^1.4.1") (d #t) (k 0)))) (h "1h6kk9zi44y04ia2mk6n5bzv9v4d3mxv1cdhi7hzk4xcgy9prcl2") (y #t)))

(define-public crate-solana_libra_vm_cache_map-0.0.0-sol8 (c (n "solana_libra_vm_cache_map") (v "0.0.0-sol8") (d (list (d (n "chashmap") (r "^2.2.2") (d #t) (k 0)) (d (n "crossbeam") (r "^0.7") (d #t) (k 2)) (d (n "proptest") (r "^0.9") (d #t) (k 2)) (d (n "rand") (r "^0.6.5") (d #t) (k 2)) (d (n "typed-arena") (r "^1.4.1") (d #t) (k 0)))) (h "100mcpnjirbr7kj9rgjwv075sfm7kaqw7ijf0a0pi8p05slxwd4p") (y #t)))

(define-public crate-solana_libra_vm_cache_map-0.0.0-sol9 (c (n "solana_libra_vm_cache_map") (v "0.0.0-sol9") (d (list (d (n "chashmap") (r "^2.2.2") (d #t) (k 0)) (d (n "crossbeam") (r "^0.7") (d #t) (k 2)) (d (n "proptest") (r "^0.9") (d #t) (k 2)) (d (n "rand") (r "^0.6.5") (d #t) (k 2)) (d (n "typed-arena") (r "^1.4.1") (d #t) (k 0)))) (h "1z3axw2d6r70ggacr02g0zggx0szr4pks69b5flwvqf8gywsiz3j") (y #t)))

(define-public crate-solana_libra_vm_cache_map-0.0.0-sol10 (c (n "solana_libra_vm_cache_map") (v "0.0.0-sol10") (d (list (d (n "chashmap") (r "^2.2.2") (d #t) (k 0)) (d (n "crossbeam") (r "^0.7") (d #t) (k 2)) (d (n "proptest") (r "^0.9") (d #t) (k 2)) (d (n "rand") (r "^0.6.5") (d #t) (k 2)) (d (n "typed-arena") (r "^1.4.1") (d #t) (k 0)))) (h "1h5p36fwjmifm3w5czii82pqhw8ayic9a5lh9scjp1azpqa2z0b8") (y #t)))

(define-public crate-solana_libra_vm_cache_map-0.0.0-sol11 (c (n "solana_libra_vm_cache_map") (v "0.0.0-sol11") (d (list (d (n "chashmap") (r "^2.2.2") (d #t) (k 0)) (d (n "crossbeam") (r "^0.7") (d #t) (k 2)) (d (n "proptest") (r "^0.9") (d #t) (k 2)) (d (n "rand") (r "^0.6.5") (d #t) (k 2)) (d (n "typed-arena") (r "^1.4.1") (d #t) (k 0)))) (h "01vr26k5sgfnq6as8pipvscpzsx2kwaia9r03164dx5kg1wihwsd") (y #t)))

(define-public crate-solana_libra_vm_cache_map-0.0.0-sol12 (c (n "solana_libra_vm_cache_map") (v "0.0.0-sol12") (d (list (d (n "chashmap") (r "^2.2.2") (d #t) (k 0)) (d (n "crossbeam") (r "^0.7") (d #t) (k 2)) (d (n "proptest") (r "^0.9") (d #t) (k 2)) (d (n "rand") (r "^0.6.5") (d #t) (k 2)) (d (n "typed-arena") (r "^1.4.1") (d #t) (k 0)))) (h "0w9kp4sx00zb33283arjf5h2ianfcw1qy6g40hllms4aj2n51kxb") (y #t)))

(define-public crate-solana_libra_vm_cache_map-0.0.0-sol13 (c (n "solana_libra_vm_cache_map") (v "0.0.0-sol13") (d (list (d (n "chashmap") (r "^2.2.2") (d #t) (k 0)) (d (n "crossbeam") (r "^0.7") (d #t) (k 2)) (d (n "proptest") (r "^0.9") (d #t) (k 2)) (d (n "rand") (r "^0.6.5") (d #t) (k 2)) (d (n "typed-arena") (r "^1.4.1") (d #t) (k 0)))) (h "0hism2hc58ga9yf74grpnqwavd6p73xdzh8x1bxhasyggnpi2apz") (y #t)))

(define-public crate-solana_libra_vm_cache_map-0.0.0-sol14 (c (n "solana_libra_vm_cache_map") (v "0.0.0-sol14") (d (list (d (n "chashmap") (r "^2.2.2") (d #t) (k 0)) (d (n "crossbeam") (r "^0.7") (d #t) (k 2)) (d (n "proptest") (r "^0.9") (d #t) (k 2)) (d (n "rand") (r "^0.6.5") (d #t) (k 2)) (d (n "typed-arena") (r "^1.4.1") (d #t) (k 0)))) (h "12i3gg5a8kl1ys2nm0pjpmcnpqybw416f3wpar12c62i34z82917") (y #t)))

(define-public crate-solana_libra_vm_cache_map-0.0.0-sol15 (c (n "solana_libra_vm_cache_map") (v "0.0.0-sol15") (d (list (d (n "chashmap") (r "^2.2.2") (d #t) (k 0)) (d (n "crossbeam") (r "^0.7") (d #t) (k 2)) (d (n "proptest") (r "^0.9") (d #t) (k 2)) (d (n "rand") (r "^0.6.5") (d #t) (k 2)) (d (n "typed-arena") (r "^1.4.1") (d #t) (k 0)))) (h "0asxz3jnlq67vxxpjv99mwsvvhhvibs2qqspa0d26k8nd2xdk2kz")))

(define-public crate-solana_libra_vm_cache_map-0.0.0 (c (n "solana_libra_vm_cache_map") (v "0.0.0") (d (list (d (n "chashmap") (r "^2.2.2") (d #t) (k 0)) (d (n "crossbeam") (r "^0.7") (d #t) (k 2)) (d (n "proptest") (r "^0.9") (d #t) (k 2)) (d (n "rand") (r "^0.6.5") (d #t) (k 2)) (d (n "typed-arena") (r "^1.4.1") (d #t) (k 0)))) (h "15mz6jh3brrz58zijjh1p54iz801zbqy6r07kv57cz9cqwnbs2lz")))

(define-public crate-solana_libra_vm_cache_map-0.0.1-sol3 (c (n "solana_libra_vm_cache_map") (v "0.0.1-sol3") (d (list (d (n "chashmap") (r "^2.2.2") (d #t) (k 0)) (d (n "crossbeam") (r "^0.7.2") (d #t) (k 2)) (d (n "proptest") (r "^0.9.4") (d #t) (k 2)) (d (n "rand") (r "^0.6.5") (d #t) (k 2)) (d (n "typed-arena") (r "^1.5.0") (d #t) (k 0)))) (h "1ibb4q83nb3v13ic7p86ixapgks8141jra2p541h4znn1vk4mkn4")))

(define-public crate-solana_libra_vm_cache_map-0.0.1-sol4 (c (n "solana_libra_vm_cache_map") (v "0.0.1-sol4") (d (list (d (n "chashmap") (r "^2.2.2") (d #t) (k 0)) (d (n "crossbeam") (r "^0.7.2") (d #t) (k 2)) (d (n "proptest") (r "^0.9.4") (d #t) (k 2)) (d (n "rand") (r "^0.6.5") (d #t) (k 2)) (d (n "typed-arena") (r "^1.5.0") (d #t) (k 0)))) (h "142638489vyni2b9f8r57dq7k4m8h5grl539476i8xd800gjxyip")))

(define-public crate-solana_libra_vm_cache_map-0.0.1-sol5 (c (n "solana_libra_vm_cache_map") (v "0.0.1-sol5") (d (list (d (n "chashmap") (r "^2.2.2") (d #t) (k 0)) (d (n "crossbeam") (r "^0.7.2") (d #t) (k 2)) (d (n "proptest") (r "^0.9.4") (d #t) (k 2)) (d (n "rand") (r "^0.7.0") (d #t) (k 2)) (d (n "typed-arena") (r "^1.5.0") (d #t) (k 0)))) (h "0bc5vidy5q2i9di2v6hp72pwsf3k1hcmsjgvx6p747ac9imdsb1k")))

