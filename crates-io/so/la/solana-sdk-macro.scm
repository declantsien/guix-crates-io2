(define-module (crates-io so la solana-sdk-macro) #:use-module (crates-io))

(define-public crate-solana-sdk-macro-0.21.1 (c (n "solana-sdk-macro") (v "0.21.1") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1mjlsj7zcfqch132kxj3v0mq3x6ms5aqlnagg2pjc2352l15a5xy")))

(define-public crate-solana-sdk-macro-0.21.3 (c (n "solana-sdk-macro") (v "0.21.3") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "09310m6b2dindf899r01023d2iwklb7cdfiizkcliznr8ynn2rq5")))

(define-public crate-solana-sdk-macro-0.21.4 (c (n "solana-sdk-macro") (v "0.21.4") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0zr9s90bq555p7w0x8f12lwz3nrsjxxaikdgj2mgh9a8b6r4wl6c")))

(define-public crate-solana-sdk-macro-0.21.5 (c (n "solana-sdk-macro") (v "0.21.5") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "02z8m76nib53hm1w13h8lb9qan3r69xz05fy1z7q1538iai42d9r")))

(define-public crate-solana-sdk-macro-0.22.0 (c (n "solana-sdk-macro") (v "0.22.0") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0949fff1z0nbjyhd5r47wjbm4m6792rcbjrb175xdqbfbkfhc2l0")))

(define-public crate-solana-sdk-macro-0.21.6 (c (n "solana-sdk-macro") (v "0.21.6") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0gkd603lw7dib72annn39l0cvphdsnasmids23sw2xdmhlcyp5l5")))

(define-public crate-solana-sdk-macro-0.22.1 (c (n "solana-sdk-macro") (v "0.22.1") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0anpdirm445bdvmk3ij3mhl6g9wlaizimf6fzr76aq1dw2mgkn0l")))

(define-public crate-solana-sdk-macro-0.22.2 (c (n "solana-sdk-macro") (v "0.22.2") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0v4b7a0sk80a9pm2kss525lf7jkpky6zfcfdhxg8xr8ws8xvx8bv")))

(define-public crate-solana-sdk-macro-0.21.7 (c (n "solana-sdk-macro") (v "0.21.7") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "02lpnxxmm6601kksmq8z6q83h5ysykqn032yjcjmlsn65slnp6j8")))

(define-public crate-solana-sdk-macro-0.22.3 (c (n "solana-sdk-macro") (v "0.22.3") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1yfb85gda38gg1j0c4fmlh8ccc39rwzqag43nf32z7l689n8vscw")))

(define-public crate-solana-sdk-macro-0.22.4 (c (n "solana-sdk-macro") (v "0.22.4") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0z3gfy0dyhh0y1qbpc8bjcqppgld0x5kmlvhkygwmzz1pwh304j2")))

(define-public crate-solana-sdk-macro-0.23.0 (c (n "solana-sdk-macro") (v "0.23.0") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1srbzq5wsxv95pzvwiriid4wh2ijc3y1wccvvwl4ciszpjg6y7v4")))

(define-public crate-solana-sdk-macro-0.22.5 (c (n "solana-sdk-macro") (v "0.22.5") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1f8m4rzxckv2x0g1r6j05armc4jcm2sc6sln0m3m1ywvsjl1lyhh")))

(define-public crate-solana-sdk-macro-0.22.6 (c (n "solana-sdk-macro") (v "0.22.6") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0v7b6nijjvwjfhd28jw6xcpvbr3xlsx1qq8yppp2c7x1y1d0nic4")))

(define-public crate-solana-sdk-macro-0.23.1 (c (n "solana-sdk-macro") (v "0.23.1") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "10vafhq2a1pnjm000wwwvcja4ihggy2zclnrdj1q92dzdiqf1cir")))

(define-public crate-solana-sdk-macro-0.23.2 (c (n "solana-sdk-macro") (v "0.23.2") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0f01nkhwzrkvrj3w7m30a4q5yll926g964ckkaiycylr187zvj0m")))

(define-public crate-solana-sdk-macro-0.22.7 (c (n "solana-sdk-macro") (v "0.22.7") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "05w8zm95ls67qhgz9g9amaz9dxcll8lm97ir38kpih85wy068wq7")))

(define-public crate-solana-sdk-macro-0.23.3 (c (n "solana-sdk-macro") (v "0.23.3") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1010466jpqx2kc4n4b2was8r9q72h95dx9qh0yvpi9ix994p2cm8")))

(define-public crate-solana-sdk-macro-0.23.4 (c (n "solana-sdk-macro") (v "0.23.4") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "07c7n5s7wh591lrrg3pm5bz2sq2sabp4ip9ngx8prx351fm9nz0z")))

(define-public crate-solana-sdk-macro-0.22.8 (c (n "solana-sdk-macro") (v "0.22.8") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "16mz8ds33fcvnlvyyx629q4bgk5c255raqkvqppmmizzwzy4fzbq")))

(define-public crate-solana-sdk-macro-0.23.5 (c (n "solana-sdk-macro") (v "0.23.5") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "18gqim3w8dymbk49d77m3312z2qrixzqzn5gi6i8hxcy4m0p4f2s")))

(define-public crate-solana-sdk-macro-0.23.6 (c (n "solana-sdk-macro") (v "0.23.6") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "13b19ip87crvc47xfpqmx0sjs41xi1lkharda6sqf897fv2jhzpl")))

(define-public crate-solana-sdk-macro-1.0.0 (c (n "solana-sdk-macro") (v "1.0.0") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1wdhfx7ikpvdav80pa442lvvdc7jr0ywgn19w27y42jdf3vbimzd")))

(define-public crate-solana-sdk-macro-0.23.7 (c (n "solana-sdk-macro") (v "0.23.7") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "16lv4w3wjk2m4p006p11ml9f4h7grvln5yg5qi0wfs68hq25hw5g")))

(define-public crate-solana-sdk-macro-0.23.8 (c (n "solana-sdk-macro") (v "0.23.8") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1zgqc69akgy10sc97wrxhfas47rcv2vwc66r1mmgp5zjl2xxw6dl")))

(define-public crate-solana-sdk-macro-1.0.1 (c (n "solana-sdk-macro") (v "1.0.1") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0xzg5yvyl561v4hhd1l454mdf3wi13g358dndaiwwsxskpn4xbgy")))

(define-public crate-solana-sdk-macro-1.0.2 (c (n "solana-sdk-macro") (v "1.0.2") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1z34i9j7ddsypprakpbx0npcrr4iyh67ml2x58f2bzlckxkfjd2n")))

(define-public crate-solana-sdk-macro-1.0.3 (c (n "solana-sdk-macro") (v "1.0.3") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1ns6j5qh1bnqpzrgssby3ig5akp6lvk6k8vzi3agv7jxrf73ls5q")))

(define-public crate-solana-sdk-macro-1.0.4 (c (n "solana-sdk-macro") (v "1.0.4") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0y0rzzdbmfbjy893c3swaj05p9qgmjzjf3irwxwx55yh1z4mil14")))

(define-public crate-solana-sdk-macro-1.0.5 (c (n "solana-sdk-macro") (v "1.0.5") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0rb0bkvpzz4zr7x6nw8h73sm8za89q81wmppmfwc6sw9ghdn5w76")))

(define-public crate-solana-sdk-macro-1.0.6 (c (n "solana-sdk-macro") (v "1.0.6") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "155h91h1rv15l04v01xbnvxs7rqfdyrb9y93rjw4kkbvbh73xvqp")))

(define-public crate-solana-sdk-macro-1.0.7 (c (n "solana-sdk-macro") (v "1.0.7") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0g6h88ypg7ngqf4mq5sr42g3nb0bmd8r58r241l3nh4rzb7cz7j8")))

(define-public crate-solana-sdk-macro-1.0.8 (c (n "solana-sdk-macro") (v "1.0.8") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0bxk50v1kxcs31zhkc6m2sv94yxapq6cj0rbganjkrysnwhg5fps")))

(define-public crate-solana-sdk-macro-1.0.9 (c (n "solana-sdk-macro") (v "1.0.9") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0060vcfpgh3vr09b60fakcnd883dr46kc7vy7ig339cwayx6i3gj")))

(define-public crate-solana-sdk-macro-1.0.10 (c (n "solana-sdk-macro") (v "1.0.10") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1q08x4in1ws4yc9b9jrmfx6b5yaiz0y2y2y6qfcm6b5kaf5nc4l6")))

(define-public crate-solana-sdk-macro-1.0.11 (c (n "solana-sdk-macro") (v "1.0.11") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0si9yn7izd3sjhaz61ksp0w33xwxgr54npkj6ff6v42gyj4zdqy5")))

(define-public crate-solana-sdk-macro-1.0.12 (c (n "solana-sdk-macro") (v "1.0.12") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1h6yng4ll46hlzcqmaixpx1hznybwfnf2nif7cyyzas4hf34k76x")))

(define-public crate-solana-sdk-macro-1.1.0 (c (n "solana-sdk-macro") (v "1.1.0") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "00sp747zkznm7f4g4frv4ni5r2jh4lxsjyifwnchw29dg6w28cfv")))

(define-public crate-solana-sdk-macro-1.1.1 (c (n "solana-sdk-macro") (v "1.1.1") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0v8vkv6szjfhjmmymbv1n3bl0y3kq313ipswz0fzdwb43m1w811d")))

(define-public crate-solana-sdk-macro-1.0.13 (c (n "solana-sdk-macro") (v "1.0.13") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0cja2rah6dsc370jvmb5n31dkr7dmbc4l2vcvqjx104p1sxf12zw")))

(define-public crate-solana-sdk-macro-1.1.2 (c (n "solana-sdk-macro") (v "1.1.2") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "11mbd669yhi3z5b2gdwa46s99fax5pg7lkhw0hx3gj4pgbldl1p1")))

(define-public crate-solana-sdk-macro-1.0.14 (c (n "solana-sdk-macro") (v "1.0.14") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "175lqigcjcm9ka01rzcvazpix8vvkkbq6289ihhj6jn11z0lg8hs")))

(define-public crate-solana-sdk-macro-1.0.15 (c (n "solana-sdk-macro") (v "1.0.15") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1lv0bcp9h72c0wmmjqywv1f218blfx7s707iwhw78is61rbri95z")))

(define-public crate-solana-sdk-macro-1.0.16 (c (n "solana-sdk-macro") (v "1.0.16") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "02dy31b1ya3m9nqbqxmzj769wjr78960fk5kc2zzhzpbs6bwv282")))

(define-public crate-solana-sdk-macro-1.1.3 (c (n "solana-sdk-macro") (v "1.1.3") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0zig9p5lrsy7rbqiv3vxp70v09xkd68sxq2csmrq48046lm8q3g8")))

(define-public crate-solana-sdk-macro-1.0.17 (c (n "solana-sdk-macro") (v "1.0.17") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "16wwl1xm9f7ljgrsaiks80xirly7g32sgqqyn1qqp3kkj1wa2pyv")))

(define-public crate-solana-sdk-macro-1.0.18 (c (n "solana-sdk-macro") (v "1.0.18") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1bs05mnjpdf6din5ws0wj3wixzal9nps7x4w6r6w7iyxz7khshw9")))

(define-public crate-solana-sdk-macro-1.1.5 (c (n "solana-sdk-macro") (v "1.1.5") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0r4xnv1whflp98kqs7yxa0yl15l5sxarvkmi18b0s4v9v2dz7pn1")))

(define-public crate-solana-sdk-macro-1.1.6 (c (n "solana-sdk-macro") (v "1.1.6") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "12d0byfm9i4r76m99nss7zzd66250bi867k655msxky242a6wkyn")))

(define-public crate-solana-sdk-macro-1.1.7 (c (n "solana-sdk-macro") (v "1.1.7") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0ffc9k1ydrx7p2yx42215vrjkgp3wyi7pkydvm7136ibpi0x3dcf")))

(define-public crate-solana-sdk-macro-1.0.20 (c (n "solana-sdk-macro") (v "1.0.20") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0sdccgx8l7li0dh8pvp00vlj1bzl7bhl6w043mfcjda9d2fpf66g")))

(define-public crate-solana-sdk-macro-1.0.21 (c (n "solana-sdk-macro") (v "1.0.21") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0xzk979dv1a7wsr50vcki9dmdbfckqpaf13lizyxhr9jb83ak46j")))

(define-public crate-solana-sdk-macro-1.1.8 (c (n "solana-sdk-macro") (v "1.1.8") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0qz1i7xd81940grl1wr5brdy8zgibb0d5g633045jrj33kl9j5lc")))

(define-public crate-solana-sdk-macro-1.1.9 (c (n "solana-sdk-macro") (v "1.1.9") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1l2zc6i335mi5k92hvcyf20j4wwd91j13prprvdy85hvyhi5xriz")))

(define-public crate-solana-sdk-macro-1.1.10 (c (n "solana-sdk-macro") (v "1.1.10") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0vjy6292cvr9pzawqr0y4xyaii12a3hgh86alsgi7bparf2ylvpi")))

(define-public crate-solana-sdk-macro-1.0.22 (c (n "solana-sdk-macro") (v "1.0.22") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1wp1dn8ggyihhw3hyib2fcwjlc4c923vw24x2dpx56aya5mzihpx")))

(define-public crate-solana-sdk-macro-1.1.11 (c (n "solana-sdk-macro") (v "1.1.11") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0rj42x3j76mx5swiij6mfkq400cm643ifkqlycd4jdk1h5kbv7xl")))

(define-public crate-solana-sdk-macro-1.1.12 (c (n "solana-sdk-macro") (v "1.1.12") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1axsk29zrdf72y5fr6cxzg350pihk74cyqvnzxx03ny1b61w0zba")))

(define-public crate-solana-sdk-macro-1.1.13 (c (n "solana-sdk-macro") (v "1.1.13") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "14nm41gihvjl2xnfwmvfyja6kz362w8lswa2bjn7i224cqbbz85z")))

(define-public crate-solana-sdk-macro-1.0.23 (c (n "solana-sdk-macro") (v "1.0.23") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "09h3ydr9sls0vj3rfkkxml3ni05r8gfkqk7vssjqfably667jf4l")))

(define-public crate-solana-sdk-macro-1.0.24 (c (n "solana-sdk-macro") (v "1.0.24") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0202gpc9bx7pyzrrvcvvpbyfviv9z9b52b28z313w0r6bqz1z89i")))

(define-public crate-solana-sdk-macro-1.2.0 (c (n "solana-sdk-macro") (v "1.2.0") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0vavwm1kqc640briycddvqzmk4k6403vk2va551rk0n057xgpwnw")))

(define-public crate-solana-sdk-macro-1.1.15 (c (n "solana-sdk-macro") (v "1.1.15") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "02d29q63svd1aylan2fqm4cgcq062yxaidjnypzjf30qmq82245n")))

(define-public crate-solana-sdk-macro-1.1.17 (c (n "solana-sdk-macro") (v "1.1.17") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1v2rxi6vnmsn8z3847kzjwn3p73p5hvs7j7rdnzamlvyz3q21s0x")))

(define-public crate-solana-sdk-macro-1.2.1 (c (n "solana-sdk-macro") (v "1.2.1") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "169pdcx872m69svlkhvzfmy9zhnyclbvwyrn4k0y5wm6zrynwpqz")))

(define-public crate-solana-sdk-macro-1.1.18 (c (n "solana-sdk-macro") (v "1.1.18") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1dy9js7wkgiakpa70h226dy3cfp5dp1hv1w86ibyrxbhlvg689pj")))

(define-public crate-solana-sdk-macro-1.2.3 (c (n "solana-sdk-macro") (v "1.2.3") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0yns9a2dvm6bcvpl8lyzzzz7c0h65ciw27n1f80kbpcs92fg4xly")))

(define-public crate-solana-sdk-macro-1.2.4 (c (n "solana-sdk-macro") (v "1.2.4") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0w567wvjn66q8qb2c6psdk2s40r51hbni31lmnq3wcimfwg32pys")))

(define-public crate-solana-sdk-macro-1.1.19 (c (n "solana-sdk-macro") (v "1.1.19") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0zbkrkfc34nn1pmk2bf6d55f09w8sbzk444ckkp0j6m5cbj934mx")))

(define-public crate-solana-sdk-macro-1.2.5 (c (n "solana-sdk-macro") (v "1.2.5") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1fcpinxvf14lz393hp9pb31cgv7hifwix7hblzh9sci0ljhqfj3c")))

(define-public crate-solana-sdk-macro-1.2.6 (c (n "solana-sdk-macro") (v "1.2.6") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1v666ikjc1h27p8zzwynncrhp9dykgsnb0ijkp9dlna01cxja5kv")))

(define-public crate-solana-sdk-macro-1.2.7 (c (n "solana-sdk-macro") (v "1.2.7") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0ja4m6afgwc6l97czb79ihf9d2mv81gfqnvirv6lxarqkqamlpgf")))

(define-public crate-solana-sdk-macro-1.2.8 (c (n "solana-sdk-macro") (v "1.2.8") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1xkin142fyma01lhcp4zfzvgnn864xfn5ndg1mvcw2fsw35n0gy7")))

(define-public crate-solana-sdk-macro-1.2.9 (c (n "solana-sdk-macro") (v "1.2.9") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "12l33sh5hjnqwhcssajaaa7hkc2i2ichhgm9w1g0icjq7klbjiqx")))

(define-public crate-solana-sdk-macro-1.2.10 (c (n "solana-sdk-macro") (v "1.2.10") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "00s1rh0rdqihn1z10aqzzp66kbcx5llxzlngr6qx3rwfnjaa9yyk")))

(define-public crate-solana-sdk-macro-1.1.20 (c (n "solana-sdk-macro") (v "1.1.20") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "07ri8dhd6id0nvjvc6jac3d89q7qw37shdi9xq7p8cyrwzkw2414")))

(define-public crate-solana-sdk-macro-1.2.12 (c (n "solana-sdk-macro") (v "1.2.12") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0j5wv5jkld9v4dxqrp7p5b88yqvmp1zp9lbz2ih1m1q1i591mkl1")))

(define-public crate-solana-sdk-macro-1.2.13 (c (n "solana-sdk-macro") (v "1.2.13") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "09w4z9d58z2khwgsd8g47z7dcy4d4v1i802kiac8flk8y9lsy949")))

(define-public crate-solana-sdk-macro-1.2.14 (c (n "solana-sdk-macro") (v "1.2.14") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0vxmm5hdsvjfmjqnhgchw4g4lc4vhwaflhc36q7iwm08rvihdnlh")))

(define-public crate-solana-sdk-macro-1.1.23 (c (n "solana-sdk-macro") (v "1.1.23") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1lmmivflxdqz5mzym6nybvfw38aba571xdisc6n8zr4vczbphckj")))

(define-public crate-solana-sdk-macro-1.2.15 (c (n "solana-sdk-macro") (v "1.2.15") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0qgajn4yl75si1s9ia8pjsrja3bj9v4knhylhxskpqfn8i3ha6a2")))

(define-public crate-solana-sdk-macro-1.2.16 (c (n "solana-sdk-macro") (v "1.2.16") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1970na44ghvdc3akq3vfpkrqj6xf13xl23fxmf1pmglppxl8fcg8")))

(define-public crate-solana-sdk-macro-1.2.17 (c (n "solana-sdk-macro") (v "1.2.17") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1j52sqw4q9bvgjlqb9x9am2dk8ql3jkrc1lwl9i2asfy41h3ig1q")))

(define-public crate-solana-sdk-macro-1.2.18 (c (n "solana-sdk-macro") (v "1.2.18") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0ypn8qbpb3d1wn7nl6gm6rkkjk0gyai4d46x33yi5dkk3n9549w2")))

(define-public crate-solana-sdk-macro-1.2.19 (c (n "solana-sdk-macro") (v "1.2.19") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0w1wdbsxwqxkbxxf04z1ffkdibhbhbqnvh108afwv91ly73mz7ia")))

(define-public crate-solana-sdk-macro-1.2.20 (c (n "solana-sdk-macro") (v "1.2.20") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1j915aln9ja45bd1m44ymmw96i04sfw8d7frb2632z1614j6l5s9")))

(define-public crate-solana-sdk-macro-1.3.0 (c (n "solana-sdk-macro") (v "1.3.0") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1qkbm5gxkwjxxapvai18z9mb3bhb8dwgaixbknbgfi005581nmb0")))

(define-public crate-solana-sdk-macro-1.3.1 (c (n "solana-sdk-macro") (v "1.3.1") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "031kkp29hka7pmwbnbv7i666mg2nh7ayn2xhc2g0b68imfh45sml")))

(define-public crate-solana-sdk-macro-1.2.21 (c (n "solana-sdk-macro") (v "1.2.21") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0vfpk4pdlx3rbwq1r1hqrym96q0apy1q8mk8qmn85x8m50j8iq63")))

(define-public crate-solana-sdk-macro-1.2.22 (c (n "solana-sdk-macro") (v "1.2.22") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "12grsrs3r7xghn8cg9jqhd1qkp471jpc6ym1h2cajzmhbi2g4ah0")))

(define-public crate-solana-sdk-macro-1.3.2 (c (n "solana-sdk-macro") (v "1.3.2") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1vpazh155qpvdf9h827zqqvl2g6d0qpckx611y8pxwbzw78cx3gr")))

(define-public crate-solana-sdk-macro-1.2.23 (c (n "solana-sdk-macro") (v "1.2.23") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1dxm7gz92sp5drahf5f5hbby7hnjrsm9wq7qp90zkx192difac92")))

(define-public crate-solana-sdk-macro-1.3.3 (c (n "solana-sdk-macro") (v "1.3.3") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1snl3vkacd6xpvgv68pssb2saryn0x9prpll2j1z3i3zf4z01b8a")))

(define-public crate-solana-sdk-macro-1.2.24 (c (n "solana-sdk-macro") (v "1.2.24") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0hs1p7mhh0yi477h0llcvd09qf1h6pzwdj1lbp3iik1g9iwb0d1n")))

(define-public crate-solana-sdk-macro-1.2.25 (c (n "solana-sdk-macro") (v "1.2.25") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1vhq401lc35i3s606wf26288d8dp52sf5rymf6lqf52ks2h3vvxn")))

(define-public crate-solana-sdk-macro-1.3.4 (c (n "solana-sdk-macro") (v "1.3.4") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0wfgba18mry38s7z4lgypq3j140c7dfysdifqgnjhamjrcm3pvym")))

(define-public crate-solana-sdk-macro-1.2.26 (c (n "solana-sdk-macro") (v "1.2.26") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "00p3asj1288cxbdhfigzlcxvajfh5bspgl2dff71918d9mv7mi7w")))

(define-public crate-solana-sdk-macro-1.3.5 (c (n "solana-sdk-macro") (v "1.3.5") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0sc4ms844mjg0pn7fjr47nj7ifnichhzj8pwar7f0qsmicvjs3fw")))

(define-public crate-solana-sdk-macro-1.3.6 (c (n "solana-sdk-macro") (v "1.3.6") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0zcqzjsbnmrqpvdwvsh0gnpnvjmy80zqqp2pkxrwc6xns8q9xy8g")))

(define-public crate-solana-sdk-macro-1.3.7 (c (n "solana-sdk-macro") (v "1.3.7") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1dn1bsfgbnqbgpmxizfgwr23x9yy5f8ipdrn62gksc61r9k013wn")))

(define-public crate-solana-sdk-macro-1.2.27 (c (n "solana-sdk-macro") (v "1.2.27") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1kdhij6yqdrxk7nc9bfdxc7i7h4dxaa0kbi73853c1jsp28dwrjy")))

(define-public crate-solana-sdk-macro-1.3.8 (c (n "solana-sdk-macro") (v "1.3.8") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0npz7v61x89w42yk6vwk7bxrp8bz0q04fv9q8yq7l9x4cxqwxgwh")))

(define-public crate-solana-sdk-macro-1.2.28 (c (n "solana-sdk-macro") (v "1.2.28") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1l1yzx6y75w7hc083v0rqai8i3n48jbfgyyn2jcqnixkj7bwmc23")))

(define-public crate-solana-sdk-macro-1.3.9 (c (n "solana-sdk-macro") (v "1.3.9") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0rpq29gkpmaca4l40fi8vn0c4ac33w81qgx2xhn24ghrmndqpzfm")))

(define-public crate-solana-sdk-macro-1.3.10 (c (n "solana-sdk-macro") (v "1.3.10") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1cw3ic6mvyhfh5yz3qc2xk110x75cnch5w2isc2zzf9bwgsp8fr1")))

(define-public crate-solana-sdk-macro-1.3.11 (c (n "solana-sdk-macro") (v "1.3.11") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1b74lman28xf3272863m78zn8fkq8bhh8g3f9jbqay3p6yv85yw1")))

(define-public crate-solana-sdk-macro-1.2.29 (c (n "solana-sdk-macro") (v "1.2.29") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "10p27s5bnibaqi89j6ad2ndi50qlj70mj2mnlqzn4w802vx2lvvj")))

(define-public crate-solana-sdk-macro-1.3.12 (c (n "solana-sdk-macro") (v "1.3.12") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1jdbc95jhkqq12harlqjmgi0i5g88qsx0jh7p4s3f4l11my8xi51")))

(define-public crate-solana-sdk-macro-1.3.13 (c (n "solana-sdk-macro") (v "1.3.13") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "120q77pvlk5gfgqfn9x4jmiliwai90q347wlbi3xnd8rqrv3l57m")))

(define-public crate-solana-sdk-macro-1.2.30 (c (n "solana-sdk-macro") (v "1.2.30") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0lglaazinvh564ysg6hay5c63f4mhkqpgbj03crvdlsgah359mkn")))

(define-public crate-solana-sdk-macro-1.3.14 (c (n "solana-sdk-macro") (v "1.3.14") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1zdpxjd0acwaj72s8ghih2vvif10m0yvymw9bzyrclvd8kyysy16")))

(define-public crate-solana-sdk-macro-1.2.31 (c (n "solana-sdk-macro") (v "1.2.31") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "08qcc6i87s666qnibmrd4b9i2c9ykvak37hxbw22ib2d3015j9ad")))

(define-public crate-solana-sdk-macro-1.2.32 (c (n "solana-sdk-macro") (v "1.2.32") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0rpksl9ijjzh5qvg28ycg4maa7624b1f2kfxgfixwr8x8sa7p5l9")))

(define-public crate-solana-sdk-macro-1.3.15 (c (n "solana-sdk-macro") (v "1.3.15") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0yghgv906gwfzndl24vcvwivpldblvw750mmds0fz480xhskjijp")))

(define-public crate-solana-sdk-macro-1.3.16 (c (n "solana-sdk-macro") (v "1.3.16") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "107cjzvk5nyymisz1aqi79a6vrgq45a5ixf8hkl7k8fcjr0bcimy")))

(define-public crate-solana-sdk-macro-1.3.17 (c (n "solana-sdk-macro") (v "1.3.17") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "122vrncxqqvhyjz7hd107m4idsahb1s57agnm7vl10f309cd25fy")))

(define-public crate-solana-sdk-macro-1.4.0 (c (n "solana-sdk-macro") (v "1.4.0") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0jjax674rxsrg4m0n9a0hhpdqwwz4x2rm6aswa0fbsgkmhc3w0vp")))

(define-public crate-solana-sdk-macro-1.4.1 (c (n "solana-sdk-macro") (v "1.4.1") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "020ddcwbv5brz3hqz5zsazjq5068wlv6h5bmgzx9y4dlyg2zcf2g")))

(define-public crate-solana-sdk-macro-1.3.18 (c (n "solana-sdk-macro") (v "1.3.18") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1zdm6674mr0q18hbaqm9zibk9aafr51qcy7p3cygx3hiszjn2anv")))

(define-public crate-solana-sdk-macro-1.3.19 (c (n "solana-sdk-macro") (v "1.3.19") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "18ikwbvhb2caa5jnmsxn3r9pjdmqd04qdky3dfwhh18529n3x5cb")))

(define-public crate-solana-sdk-macro-1.4.2 (c (n "solana-sdk-macro") (v "1.4.2") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1p3djsmf0dnf4c55hml4v7rc4rijpwddpb09wy8wkfjxjk0628g7")))

(define-public crate-solana-sdk-macro-1.4.3 (c (n "solana-sdk-macro") (v "1.4.3") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0ffcxghkd3f7f33x0nlhjp7bkc95irpp3b9r5rarih63v32qjbd0")))

(define-public crate-solana-sdk-macro-1.4.4 (c (n "solana-sdk-macro") (v "1.4.4") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1k3waj097ngsp4i62rhc6pr8paphhagycvadkfk335ig335hvnpq")))

(define-public crate-solana-sdk-macro-1.4.5 (c (n "solana-sdk-macro") (v "1.4.5") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1dxrxhybg259skb0a0xw3fhl98c5j0nz4brn258jpkyfik71ln2f")))

(define-public crate-solana-sdk-macro-1.4.6 (c (n "solana-sdk-macro") (v "1.4.6") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "06g539xd0kn3m9yq8nsiwj483ni31ibd6aidafxdhww4wjhfl6n6")))

(define-public crate-solana-sdk-macro-1.3.20 (c (n "solana-sdk-macro") (v "1.3.20") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1yz4gh55hppki7n0qwj6816jywv1d7i5cw5111h5k4jgbz45iswb")))

(define-public crate-solana-sdk-macro-1.4.7 (c (n "solana-sdk-macro") (v "1.4.7") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0hz6j875x75mw609g5vf92cq10vx87nvy06qbw7apfh91905hw8s")))

(define-public crate-solana-sdk-macro-1.3.21 (c (n "solana-sdk-macro") (v "1.3.21") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0qfp6s7xwf6g183f0krmi67jwfzh97jhb2s1k2pascigy1prrcyc")))

(define-public crate-solana-sdk-macro-1.4.8 (c (n "solana-sdk-macro") (v "1.4.8") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1wx8c027jvdy51y1lrzqc7sjljz48ysh84762z6n07czqyfg2nvw")))

(define-public crate-solana-sdk-macro-1.4.9 (c (n "solana-sdk-macro") (v "1.4.9") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1gknl83j4nva3b638mrraffvy5qzwbr9j5f4r41n5jh8bca7j6fh")))

(define-public crate-solana-sdk-macro-1.4.10 (c (n "solana-sdk-macro") (v "1.4.10") (d (list (d (n "bs58") (r ">=0.3.0, <0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r ">=1.0.19, <2.0.0") (d #t) (k 0)) (d (n "quote") (r ">=1.0.0, <2.0.0") (d #t) (k 0)) (d (n "rustversion") (r ">=1.0.3, <2.0.0") (d #t) (k 0)) (d (n "syn") (r ">=1.0.0, <2.0.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1h2h5p2yagry4gyhdjv67zpcc7z59lv462hy53j17xf5c5jkxjlf")))

(define-public crate-solana-sdk-macro-1.4.11 (c (n "solana-sdk-macro") (v "1.4.11") (d (list (d (n "bs58") (r ">=0.3.0, <0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r ">=1.0.19, <2.0.0") (d #t) (k 0)) (d (n "quote") (r ">=1.0.0, <2.0.0") (d #t) (k 0)) (d (n "rustversion") (r ">=1.0.3, <2.0.0") (d #t) (k 0)) (d (n "syn") (r ">=1.0.0, <2.0.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0i1sva02qnl4lddga2c63hfi9frwpjz7p4w5lj4klvplxh0wylh2")))

(define-public crate-solana-sdk-macro-1.4.12 (c (n "solana-sdk-macro") (v "1.4.12") (d (list (d (n "bs58") (r ">=0.3.0, <0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r ">=1.0.19, <2.0.0") (d #t) (k 0)) (d (n "quote") (r ">=1.0.0, <2.0.0") (d #t) (k 0)) (d (n "rustversion") (r ">=1.0.3, <2.0.0") (d #t) (k 0)) (d (n "syn") (r ">=1.0.0, <2.0.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "11q1wk27v3yn6hcay08fyvzvdqvlwfcgl1m55nmfzqpycca39gv2")))

(define-public crate-solana-sdk-macro-1.3.23 (c (n "solana-sdk-macro") (v "1.3.23") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0lyc6blbgy7a3aq2jp64a5d75mwvlxpj7828j8585r7299x19fxz")))

(define-public crate-solana-sdk-macro-1.4.13 (c (n "solana-sdk-macro") (v "1.4.13") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "18kq0bwzmnlld7ivrscsj2sa5ass1bbfvk7x0by3syfl7j531k2c")))

(define-public crate-solana-sdk-macro-1.4.14 (c (n "solana-sdk-macro") (v "1.4.14") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "031l9zi7cmdk0dix4dmbjyms3lr2hrppq01f8mbf5wkms466hnj7")))

(define-public crate-solana-sdk-macro-1.4.15 (c (n "solana-sdk-macro") (v "1.4.15") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "04hhqmfyaxnl2yhplx1xkd0r3q6b31sfvzq3x7ka4j1wvgz94wg5")))

(define-public crate-solana-sdk-macro-1.4.16 (c (n "solana-sdk-macro") (v "1.4.16") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0cpwhhpzsmrii7hq8ayiazywaw4xyx5wkpyrbqiv327zxifvwvjp")))

(define-public crate-solana-sdk-macro-1.4.17 (c (n "solana-sdk-macro") (v "1.4.17") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "13xbknxxcgmkkb68gn43a7hrh9qk4bjgm3a8hgx11463b9d0j3h9")))

(define-public crate-solana-sdk-macro-1.5.0 (c (n "solana-sdk-macro") (v "1.5.0") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1ijs5lbsyw1913ixg3pbmr94cyy8hqpxg4h5h4f5g8p5g903yah1")))

(define-public crate-solana-sdk-macro-1.4.18 (c (n "solana-sdk-macro") (v "1.4.18") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "05gizk9h4ydd743kwjv5ajwksifbcxi29wkhllxdbk2yi1dz8ia2")))

(define-public crate-solana-sdk-macro-1.4.19 (c (n "solana-sdk-macro") (v "1.4.19") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "13i8an4sp6nxr4a044783jys8ral3bpdx35lwspb7l05szljbyna")))

(define-public crate-solana-sdk-macro-1.4.20 (c (n "solana-sdk-macro") (v "1.4.20") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1p17jpph2d7lqqhm283dcsm4y7dxw8bn1r2lp9r66drzfbchvkld")))

(define-public crate-solana-sdk-macro-1.5.1 (c (n "solana-sdk-macro") (v "1.5.1") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1hli0fwwv7v8fc1cxxh6yrp97xjyjp8w8vd9skchmxkqgqarqj0i")))

(define-public crate-solana-sdk-macro-1.4.21 (c (n "solana-sdk-macro") (v "1.4.21") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1ksbnjgm4w3yvgl2jaddzfmb4gcas17bwhqvdha2qjc1wh5rm94i")))

(define-public crate-solana-sdk-macro-1.4.22 (c (n "solana-sdk-macro") (v "1.4.22") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0pz4g14bn9crb60iwbx8yy8qbczjwcglx3gk9zrsc1g6rpdhyj3g")))

(define-public crate-solana-sdk-macro-1.5.2 (c (n "solana-sdk-macro") (v "1.5.2") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1rljpsivylrc4awi3hf5477nikqjyn9w5lnm7v7hh1pvi2dlx1yr")))

(define-public crate-solana-sdk-macro-1.4.23 (c (n "solana-sdk-macro") (v "1.4.23") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1gq0xfqm64rsfqqn3qhvfww41ca0l9xxqxckjcmclk4sdz1rqcj0")))

(define-public crate-solana-sdk-macro-1.5.3 (c (n "solana-sdk-macro") (v "1.5.3") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "15xfigfrq0gwgxbg300ipjj098hg80l66q687y0q1y34rx4354qk")))

(define-public crate-solana-sdk-macro-1.5.4 (c (n "solana-sdk-macro") (v "1.5.4") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0khxzs58sg483w81h6ajd2nm6iir088k6d9p1znz0bq9xrmzp80f")))

(define-public crate-solana-sdk-macro-1.5.5 (c (n "solana-sdk-macro") (v "1.5.5") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1aq5vwngir359m8r0w4iz38chqgnap49lamccpq05jkm9sig4qyl")))

(define-public crate-solana-sdk-macro-1.4.25 (c (n "solana-sdk-macro") (v "1.4.25") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1sqfcjx1p61avir9mzwgmwi6453lid5nr5kf789ivslg2bnglqq9")))

(define-public crate-solana-sdk-macro-1.4.26 (c (n "solana-sdk-macro") (v "1.4.26") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1gx0a8nifw135x9acjmxkfqhnm9km38k48rarhdd9gy13nd4vqm3")))

(define-public crate-solana-sdk-macro-1.5.6 (c (n "solana-sdk-macro") (v "1.5.6") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0ycdm8xs2j4g208fdljhzkfk4by43gwn0w7vqy1fac3n48ljdc9i")))

(define-public crate-solana-sdk-macro-1.4.27 (c (n "solana-sdk-macro") (v "1.4.27") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "04zclq0x9jw7l0a7672cscf42ry0q9xq35f01qw25cp7bid5ghgj")))

(define-public crate-solana-sdk-macro-1.5.7 (c (n "solana-sdk-macro") (v "1.5.7") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0pg3i471aqyfxai10dc65l9cl6lqh1n8gam5h3a14jrqhvzcq5b5")))

(define-public crate-solana-sdk-macro-1.5.8 (c (n "solana-sdk-macro") (v "1.5.8") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1fyaxm23284qy41sxjbkdbri3h4k8qfss3xvi0dzzj4q2xgn79mq")))

(define-public crate-solana-sdk-macro-1.4.28 (c (n "solana-sdk-macro") (v "1.4.28") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "14xf78jkw9dpr78zcip99mm8fa7bwhslvfmgp03ik74rsm6abm75")))

(define-public crate-solana-sdk-macro-1.5.9 (c (n "solana-sdk-macro") (v "1.5.9") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1c0p40jzq61g66ji3kfpa169qpxl06l5i83vnn3lliywb0hyqya0") (y #t)))

(define-public crate-solana-sdk-macro-1.5.10 (c (n "solana-sdk-macro") (v "1.5.10") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0x90kng1a7ia3vc5bya3yd7d94kf72p5r4ahpz1c9lgj8p0j3qzc")))

(define-public crate-solana-sdk-macro-1.5.11 (c (n "solana-sdk-macro") (v "1.5.11") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1h5z41qcfjnh2smvvkrp4d3cp92idlpigfa8xbbp764naq3vmmid")))

(define-public crate-solana-sdk-macro-1.5.12 (c (n "solana-sdk-macro") (v "1.5.12") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1s6hxjb4j6x2rwgn8gcj6rcybd9s83vhnr8whyxlwzrpdvpcz76d")))

(define-public crate-solana-sdk-macro-1.5.13 (c (n "solana-sdk-macro") (v "1.5.13") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0nl6g3xqiip298ph7k9cyz8mbsvwn86v2ia2q5mbz0r83dmqz36z")))

(define-public crate-solana-sdk-macro-1.5.14 (c (n "solana-sdk-macro") (v "1.5.14") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "018mhkdxb29ss68qyfgracclnhy91x95091csxf4p37banlph754")))

(define-public crate-solana-sdk-macro-1.6.0 (c (n "solana-sdk-macro") (v "1.6.0") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "10511k4qr6nhidbjj9fkj320kg9z8xb6mfccw03bnqf6qfq2f8jp") (y #t)))

(define-public crate-solana-sdk-macro-1.5.15 (c (n "solana-sdk-macro") (v "1.5.15") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "02qb93wbzsbgxgq31p9hsn1lr0i86fsc4mymr1n9bdgm3f920qd2")))

(define-public crate-solana-sdk-macro-1.6.1 (c (n "solana-sdk-macro") (v "1.6.1") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0ra18a9bd77hkjkazcwlhnvjh03c9ns5zjjkd9ykiwka32k1gp15")))

(define-public crate-solana-sdk-macro-1.5.16 (c (n "solana-sdk-macro") (v "1.5.16") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1ry9j07h6c4qmfdb1fqv8q46p2psyh02r5njn59ijbqg922kg6qy")))

(define-public crate-solana-sdk-macro-1.5.17 (c (n "solana-sdk-macro") (v "1.5.17") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1d38jwmnx01b91ydn06b86s2fqm6p7xymf96fgha41d6apx3qxkd")))

(define-public crate-solana-sdk-macro-1.6.2 (c (n "solana-sdk-macro") (v "1.6.2") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1hcki2qd9cacl65kvxa2232p0x2g8w7bm0vqmlra095p21aly64g")))

(define-public crate-solana-sdk-macro-1.5.18 (c (n "solana-sdk-macro") (v "1.5.18") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1n1ka6w4l64kif157bw64pcikbj7pkrrl9b08mdp7pdzl4r18d3k")))

(define-public crate-solana-sdk-macro-1.6.3 (c (n "solana-sdk-macro") (v "1.6.3") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "12z66rdmgyisbm58lczqqsq1lymd8w1hy1jfyknciif8wv13qgmf")))

(define-public crate-solana-sdk-macro-1.6.4 (c (n "solana-sdk-macro") (v "1.6.4") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0w4wii33pad4wcmyas7h484spmd06c44fbqbrk08wzv768g5z01w")))

(define-public crate-solana-sdk-macro-1.6.5 (c (n "solana-sdk-macro") (v "1.6.5") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0sk1icq8x5ymi4lxg7smn6rzfyy3d9xq3az31vcali52514yajan")))

(define-public crate-solana-sdk-macro-1.6.6 (c (n "solana-sdk-macro") (v "1.6.6") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0r9v3ni0bwxn3nwgqh45q0p87mh4xdi9zp6hrhfzmg6banb18r42")))

(define-public crate-solana-sdk-macro-1.5.19 (c (n "solana-sdk-macro") (v "1.5.19") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "054krfvspn6i2y26chii173bdwbg9dw4picmysgprin8l22ssk0i")))

(define-public crate-solana-sdk-macro-1.6.7 (c (n "solana-sdk-macro") (v "1.6.7") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "18kxkhad7zkzr5iargvy286r4v396i9hcmvyg1k9flrkvmcr1vim")))

(define-public crate-solana-sdk-macro-1.6.8 (c (n "solana-sdk-macro") (v "1.6.8") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1zad25ps56f5bj2wvcd5h786m35z5y88j3g0r7gvjwf2n3iklp5z")))

(define-public crate-solana-sdk-macro-1.6.9 (c (n "solana-sdk-macro") (v "1.6.9") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "15h575bcycbsgpicv6zdy0di3gv784c6wilw3388gb8qnc9scwxf")))

(define-public crate-solana-sdk-macro-1.6.10 (c (n "solana-sdk-macro") (v "1.6.10") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "01f0y2bhy7smfsgp3kqbggxms32yjk663d7v7q7c8q6v62v64rwy")))

(define-public crate-solana-sdk-macro-1.6.11 (c (n "solana-sdk-macro") (v "1.6.11") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0lrkqwbywm7n5vnzwq68iw1i8kvkm3g7acq32k87894fm93mn85r")))

(define-public crate-solana-sdk-macro-1.7.0 (c (n "solana-sdk-macro") (v "1.7.0") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1amaqjx7h7gi7qy1mhc3640gzzxwgfb9dps9bsf94xlnl2r2cx6j")))

(define-public crate-solana-sdk-macro-1.7.1 (c (n "solana-sdk-macro") (v "1.7.1") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0d0my50nawka68js2xnqzxhd210lf8h37078hi65c82m8kxdhwv4")))

(define-public crate-solana-sdk-macro-1.6.12 (c (n "solana-sdk-macro") (v "1.6.12") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0002822w4yz3hwnjvqzsdygba0ymgglihvl6x8p4frvl6yn50x2a")))

(define-public crate-solana-sdk-macro-1.6.13 (c (n "solana-sdk-macro") (v "1.6.13") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "01v8x2afvgqs7fvkfiizap18ip0wsx21ixmmdfbm0xxflanf6mzj")))

(define-public crate-solana-sdk-macro-1.7.2 (c (n "solana-sdk-macro") (v "1.7.2") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "08qcng6lf46jmswlwahm0262szwwg56zaixxs7r7aqa9k4jhlx7a")))

(define-public crate-solana-sdk-macro-1.6.14 (c (n "solana-sdk-macro") (v "1.6.14") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0insnnka0w6j34nd0jw20fabj39r652qv4hf4zyw0dly2whfsmal")))

(define-public crate-solana-sdk-macro-1.7.3 (c (n "solana-sdk-macro") (v "1.7.3") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1zj859lvw33a1vczmjazp7l1rnwc1j8vf4lhpx0fr630yq59rvl5")))

(define-public crate-solana-sdk-macro-1.6.15 (c (n "solana-sdk-macro") (v "1.6.15") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0wa175gi4lb6kj1xlx2kgxg7wljb01dwbkzwdkdbbajy8dmd6y1l")))

(define-public crate-solana-sdk-macro-1.7.4 (c (n "solana-sdk-macro") (v "1.7.4") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1xn82i5gp2p3nc0l6mw945ph8i72w4773xav469rmfws9rqzwyis")))

(define-public crate-solana-sdk-macro-1.6.16 (c (n "solana-sdk-macro") (v "1.6.16") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0c31600iig7i7fh41nwvvhn8bdf8slzl437f168dvbhgw7rj0gl0")))

(define-public crate-solana-sdk-macro-1.6.17 (c (n "solana-sdk-macro") (v "1.6.17") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0nlla2cffw82fsva3aiarhjvx3acgw25x5c8k20vbi0j57yjlwms")))

(define-public crate-solana-sdk-macro-1.7.5 (c (n "solana-sdk-macro") (v "1.7.5") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0spi276vkazq1n35bl892c93y22sxbalf5wxwcxqm3clsj68qlha")))

(define-public crate-solana-sdk-macro-1.7.6 (c (n "solana-sdk-macro") (v "1.7.6") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1fb5nhrfgdkl70jv29qgajr0n219snmfkn095fgwvk11bbj0qwc4")))

(define-public crate-solana-sdk-macro-1.6.18 (c (n "solana-sdk-macro") (v "1.6.18") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1p4mk5i8q0kbwqn45yqyr698vgwhambl27sx1h3f7lky46phwcy6")))

(define-public crate-solana-sdk-macro-1.6.19 (c (n "solana-sdk-macro") (v "1.6.19") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1gcm1vlbg523pkij6naca2ihnd2cb5q742rlcqdp2crixdqf2kfb")))

(define-public crate-solana-sdk-macro-1.7.7 (c (n "solana-sdk-macro") (v "1.7.7") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0ka54vfa389yxprx57xq4fnqamhmixhhzx2j42v3cni1dkgkla59")))

(define-public crate-solana-sdk-macro-1.7.8 (c (n "solana-sdk-macro") (v "1.7.8") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "16ywm9v0f8bk5sp958yyhgvx4336ys9axqg5nd4x7d5mvpf0ksgy")))

(define-public crate-solana-sdk-macro-1.6.20 (c (n "solana-sdk-macro") (v "1.6.20") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "19h8g39m6ixi180x60lq8i1py5mrc7jrzrqgz1cw8355sd1fh2a9")))

(define-public crate-solana-sdk-macro-1.7.9 (c (n "solana-sdk-macro") (v "1.7.9") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "086mvxd8s831fv65wg0mipi9halmlypwl0ncj0qcl847jgbqisb4")))

(define-public crate-solana-sdk-macro-1.7.10 (c (n "solana-sdk-macro") (v "1.7.10") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0hj6x491gxsnl2kxkhdfc3vyshd6zzxa0x8ngy78fqriixmvqf9q")))

(define-public crate-solana-sdk-macro-1.6.21 (c (n "solana-sdk-macro") (v "1.6.21") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "19piw4bfvf2shh6la0966hghq7gsgliwv5v1hkpx6fds0lkq3c54")))

(define-public crate-solana-sdk-macro-1.6.22 (c (n "solana-sdk-macro") (v "1.6.22") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "031ahxhnfzplbqn9k43qg4wccps9ngvkw2hgj3sy27y2rpbrbxfq")))

(define-public crate-solana-sdk-macro-1.7.11 (c (n "solana-sdk-macro") (v "1.7.11") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0ff0nmkdbidhhkn5jdn9n5dxqmglc51zsg3yqiv1cyv1c2hxqlxl")))

(define-public crate-solana-sdk-macro-1.6.23 (c (n "solana-sdk-macro") (v "1.6.23") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0z7sfwysvmzgsldbp1bsfdi2jrs3ca94lm1gchixmxg22wakp69j")))

(define-public crate-solana-sdk-macro-1.6.24 (c (n "solana-sdk-macro") (v "1.6.24") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1qcmqb9i2n671igvvz15d230kd1hx8amgk6xqg4v8w0lw0vq3zgw")))

(define-public crate-solana-sdk-macro-1.6.25 (c (n "solana-sdk-macro") (v "1.6.25") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0bz7npmk750crvrw3dxv5a63jw1l19jpkdn9vglm0b0iwyrxfwm3")))

(define-public crate-solana-sdk-macro-1.7.12 (c (n "solana-sdk-macro") (v "1.7.12") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1axwwgzmby1nsyd47chm196x942gymwngfp5yy076zkyz83lvnbg")))

(define-public crate-solana-sdk-macro-1.6.26 (c (n "solana-sdk-macro") (v "1.6.26") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1sprvrgivb9fis8q96nvfd5ywk7cz92qk25b330xvldk0k6h2x72")))

(define-public crate-solana-sdk-macro-1.6.27 (c (n "solana-sdk-macro") (v "1.6.27") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0kyx82bjvkbs0r44mzz7jinfpqkkwm9qxdyb1pzxb7pczqaxya16")))

(define-public crate-solana-sdk-macro-1.7.13 (c (n "solana-sdk-macro") (v "1.7.13") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0qb5rma1mcg4q83n0kh5y5hr9ax652ni3kkbp2mh5x67k8fig6zc")))

(define-public crate-solana-sdk-macro-1.7.14 (c (n "solana-sdk-macro") (v "1.7.14") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1pvfxg42whvpjz893jryrfjngi35b6najk4rgamhfnvxzbklvaih")))

(define-public crate-solana-sdk-macro-1.8.0 (c (n "solana-sdk-macro") (v "1.8.0") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0ycyzfxmy1kfd7nh34zl09ip2icqis7y10qmyzalg76m6ggmw2zc")))

(define-public crate-solana-sdk-macro-1.6.28 (c (n "solana-sdk-macro") (v "1.6.28") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0mca8fynlvd6f18q2n7c8947zj2la5lrz4k3gly1jxbcff33a0mm")))

(define-public crate-solana-sdk-macro-1.7.15 (c (n "solana-sdk-macro") (v "1.7.15") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0l2lri1mim6bc38mjs9zlxn541yb35zh9dpwv5frnk4v0ydq9zbl")))

(define-public crate-solana-sdk-macro-1.8.1 (c (n "solana-sdk-macro") (v "1.8.1") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0y2id71pcrn437h5nklgrb409nzylsidca2fkwiqz15n7cwg4hq7")))

(define-public crate-solana-sdk-macro-1.7.16 (c (n "solana-sdk-macro") (v "1.7.16") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0k6qpmkdwln63n50gn159mnl3y0ysshh7wc098d38cqfvs3bc174")))

(define-public crate-solana-sdk-macro-1.7.17 (c (n "solana-sdk-macro") (v "1.7.17") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "08bd2m2agkv5jiadvzkdks61pz81ss1l69jflahfxx21d1fh94s0")))

(define-public crate-solana-sdk-macro-1.8.2 (c (n "solana-sdk-macro") (v "1.8.2") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "12xp0dwf13z782wwb9c2sn7srvfbyg3ccyl16j45vyaa3xh99pj9")))

(define-public crate-solana-sdk-macro-1.8.3 (c (n "solana-sdk-macro") (v "1.8.3") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0pzxzsam4h0vfrm1zfn6pkldg1z4n5vdx9kky71i7rin8bfgkp5c")))

(define-public crate-solana-sdk-macro-1.8.4 (c (n "solana-sdk-macro") (v "1.8.4") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0gd6k84dx46mwp08mxqwld7imgcvz0gcc4777zw81v6y946p3acq")))

(define-public crate-solana-sdk-macro-1.8.5 (c (n "solana-sdk-macro") (v "1.8.5") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "16rgjsbwhdrndmiv5vd529bszxyy32sdnmlzr4n38nkx2dwkd64i")))

(define-public crate-solana-sdk-macro-1.8.6 (c (n "solana-sdk-macro") (v "1.8.6") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0yzd8947wr0ss3ad3injv0vjbmhjl5ksgamq2m4lnhj3pgyxx346")))

(define-public crate-solana-sdk-macro-1.8.7 (c (n "solana-sdk-macro") (v "1.8.7") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1gawvazavza1xy7l1d8nnl06ky6c2bxz4hknsxhi95wv7msawivz")))

(define-public crate-solana-sdk-macro-1.8.8 (c (n "solana-sdk-macro") (v "1.8.8") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0j15mbhd7gncnypww49xq737jczc7w8plla5q1lv9bl5ir9pvh48")))

(define-public crate-solana-sdk-macro-1.8.9 (c (n "solana-sdk-macro") (v "1.8.9") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1p93yv8i3lap8q5nzlvssmg6i4fsh7xx5ml6ahd9x50464n0d0i6")))

(define-public crate-solana-sdk-macro-1.9.0 (c (n "solana-sdk-macro") (v "1.9.0") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1gynvsw7kclsrv7b3gld4z7rn38jp86cals5n5pllbgxjf3065h7")))

(define-public crate-solana-sdk-macro-1.8.10 (c (n "solana-sdk-macro") (v "1.8.10") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1smnr7bgkqhxxf0zri2m0can72c8wjsivzdnm40a1nmj2ppq4psz")))

(define-public crate-solana-sdk-macro-1.8.11 (c (n "solana-sdk-macro") (v "1.8.11") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0bhb8al0jfyw4y2i9nga2l9jr0kp5w84mp2agbs0rgr4l2vqv1p2")))

(define-public crate-solana-sdk-macro-1.9.1 (c (n "solana-sdk-macro") (v "1.9.1") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0hbqivq1fssynv3p90gcn9izrayiihnm6k633ymvys6p16dphr06")))

(define-public crate-solana-sdk-macro-1.9.2 (c (n "solana-sdk-macro") (v "1.9.2") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0i6qbrc7m6sbj5kd7hg76nvs8xy1qh81rwl0vw10dpmv8ibqcq5z")))

(define-public crate-solana-sdk-macro-1.9.3 (c (n "solana-sdk-macro") (v "1.9.3") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "088bq05pq6qfxxsf2dlrrpvvpqvv6zahv4q4xb8ck938skb2hf99")))

(define-public crate-solana-sdk-macro-1.8.12 (c (n "solana-sdk-macro") (v "1.8.12") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0w3ncwljrlhgsrn9n5lqyr1i1wvqzrfkgx1g1nmgbrpsbxiqfkh4")))

(define-public crate-solana-sdk-macro-1.9.4 (c (n "solana-sdk-macro") (v "1.9.4") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1bb4h8cxxs2nis7a13jv16a41bm9ysi1arq4l9v37gisqwjaj8pc")))

(define-public crate-solana-sdk-macro-1.8.13 (c (n "solana-sdk-macro") (v "1.8.13") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "15saiyxxpb3nv4lah2gjfxjg6zrmgsc0gk7bi48s54wyscf27z9s")))

(define-public crate-solana-sdk-macro-1.9.5 (c (n "solana-sdk-macro") (v "1.9.5") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0ffcz1cqi5mzfgrbqdq90xw2ysi2n1xk06jcv4mkrxw608sr72r3")))

(define-public crate-solana-sdk-macro-1.8.14 (c (n "solana-sdk-macro") (v "1.8.14") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1n8vd5fxs5x64sh49x2alwg9xdjy1g448bhm5xipm4jqhin25z0i")))

(define-public crate-solana-sdk-macro-1.9.6 (c (n "solana-sdk-macro") (v "1.9.6") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1wza0r7b8whwcq91h7k9zla7szkk62r6p70vmcsiqs4r81bwppvk")))

(define-public crate-solana-sdk-macro-1.9.7 (c (n "solana-sdk-macro") (v "1.9.7") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0sa0q9q8lan71587c7ycxmz00qyq00b05jl52phchyi9nqdijcab")))

(define-public crate-solana-sdk-macro-1.8.16 (c (n "solana-sdk-macro") (v "1.8.16") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1dfn252z6xfcyx62k1nxlk33z0qphr4f00089w36jfvgjcga08m1")))

(define-public crate-solana-sdk-macro-1.9.8 (c (n "solana-sdk-macro") (v "1.9.8") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "061vcajalljd77c3fmcksaxyiqydgv4xp86678ay79zmcdfk225c")))

(define-public crate-solana-sdk-macro-1.9.9 (c (n "solana-sdk-macro") (v "1.9.9") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1gshzq7x2j57rqyr61m6ijlmj3aw0zjsm2wai25rqzz7yq4sc42w")))

(define-public crate-solana-sdk-macro-1.10.0 (c (n "solana-sdk-macro") (v "1.10.0") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0diphy99kbi51mh9flrqv7lvmghwanc0vqpkjcrl30qfd6qyk213")))

(define-public crate-solana-sdk-macro-1.9.10 (c (n "solana-sdk-macro") (v "1.9.10") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "01cj8v5cj0nm03yday1bpf79zysd024n3g2jp9ifc9319qsp38dx")))

(define-public crate-solana-sdk-macro-1.9.11 (c (n "solana-sdk-macro") (v "1.9.11") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1i2q6pib8gfdziip6i2v9rjiwgfr7530p98mvw40d8yz5cn860fk")))

(define-public crate-solana-sdk-macro-1.9.12 (c (n "solana-sdk-macro") (v "1.9.12") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0g2ksjj4xac20rhlf3qjqqhldyxwj11wlmdsga9hjv1qb34jcdrk")))

(define-public crate-solana-sdk-macro-1.10.1 (c (n "solana-sdk-macro") (v "1.10.1") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "06hiww1yx67bd2xxwfjrn6s22x09sr2ailf601faq4ba9y0vjb8k")))

(define-public crate-solana-sdk-macro-1.10.2 (c (n "solana-sdk-macro") (v "1.10.2") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "193bgavjhr43zm16nz7qgd50w4kybqzwifwxb3q59993iqx3py44")))

(define-public crate-solana-sdk-macro-1.9.13 (c (n "solana-sdk-macro") (v "1.9.13") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "16y9s423scr1i64prbx6dwq78sl8bwv4nd1750xk1w411bc5jmxj")))

(define-public crate-solana-sdk-macro-1.10.3 (c (n "solana-sdk-macro") (v "1.10.3") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1600kdgk99p88pfgkmq7kbrmqrkc37650b3wxzjqy7vzj83qlwy8")))

(define-public crate-solana-sdk-macro-1.9.14 (c (n "solana-sdk-macro") (v "1.9.14") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1pryiacfz5frln9v8s3ih9zcsicxacjy9f2zxhpsijfp6rd812wi")))

(define-public crate-solana-sdk-macro-1.10.4 (c (n "solana-sdk-macro") (v "1.10.4") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "07bmkhqsl30ia1mk9i1bw7jmhbcxmvhyzrbcp1cmk2az1c7jjc7j")))

(define-public crate-solana-sdk-macro-1.10.5 (c (n "solana-sdk-macro") (v "1.10.5") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0pd9qaiv8vkjxmyaa7gpv7a3ab6liy20ylca4vhkxfsnvs6igq5f")))

(define-public crate-solana-sdk-macro-1.10.6 (c (n "solana-sdk-macro") (v "1.10.6") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "06cman7fizp30mjirypmzll9j8f8k6fkfbgs5zdcmdm7ybpf593x")))

(define-public crate-solana-sdk-macro-1.9.15 (c (n "solana-sdk-macro") (v "1.9.15") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "07djmwm4zby72p6jmxhm4sp37hl0ldqwm8wlwmsrb4cadhrclh33")))

(define-public crate-solana-sdk-macro-1.10.7 (c (n "solana-sdk-macro") (v "1.10.7") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "03rrlsids20pzfjxlpg60kzsaiv96snq1h3av3y4bdjj3mv01n6l")))

(define-public crate-solana-sdk-macro-1.10.8 (c (n "solana-sdk-macro") (v "1.10.8") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0850bif80vb1shb0j8kq3hxyzdqa8h307gd7rkfmkkdl04vwj0nd")))

(define-public crate-solana-sdk-macro-1.9.16 (c (n "solana-sdk-macro") (v "1.9.16") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0vg1cqwly5mkbdlnk7afhfklp2qfzjfl5w51dvbqak1fxdhcpj5s")))

(define-public crate-solana-sdk-macro-1.9.17 (c (n "solana-sdk-macro") (v "1.9.17") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1n9cm01hsa7mv0v4y1i1zz7bw6bkr5zl6in1y7ax85aabb8f1f98")))

(define-public crate-solana-sdk-macro-1.10.9 (c (n "solana-sdk-macro") (v "1.10.9") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1ddvvbl83yik8w84b2m9jqs9qyhfl0an09i8zmd719fyn9rnqkaz")))

(define-public crate-solana-sdk-macro-1.9.18 (c (n "solana-sdk-macro") (v "1.9.18") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "14bjx7737g7f81j3xrm0hngn7qibiv7z6zfgscm4pk0nhx6ib13m")))

(define-public crate-solana-sdk-macro-1.10.10 (c (n "solana-sdk-macro") (v "1.10.10") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "14qraxd0plgbkyzxxha48f0mv9vn5njrv5v8wp0z7x01s3a1gqfh")))

(define-public crate-solana-sdk-macro-1.10.11 (c (n "solana-sdk-macro") (v "1.10.11") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0575j42vq7sf624ycmd0g7m64sprn18rlzbna665yx8njda5pylj")))

(define-public crate-solana-sdk-macro-1.9.19 (c (n "solana-sdk-macro") (v "1.9.19") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1phdgqai3cyqwjv4gyi5q5rlwj0zm11rpcm4007lczkjg3bwwg3a")))

(define-public crate-solana-sdk-macro-1.10.12 (c (n "solana-sdk-macro") (v "1.10.12") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0yxwdydaclym4ak2whjsls7fx2prf9vb8dfn11chnxf9jkwranai")))

(define-public crate-solana-sdk-macro-1.9.20 (c (n "solana-sdk-macro") (v "1.9.20") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1nrwx3gd5ny1c33b2ccxwxmjywkyhhzqpl5f2cyb24f95bhb8d68")))

(define-public crate-solana-sdk-macro-1.10.13 (c (n "solana-sdk-macro") (v "1.10.13") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0jk94jgnw4apxlzbl0lq01rkg4f2m60npg9k1y3qgi9q773fgryf")))

(define-public crate-solana-sdk-macro-1.9.21 (c (n "solana-sdk-macro") (v "1.9.21") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "14bvgqprp02s3qzylb3c0mfda8adrkl9zjr1pad4yg0j7b1bd7v1")))

(define-public crate-solana-sdk-macro-1.10.14 (c (n "solana-sdk-macro") (v "1.10.14") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0rx7xk2cdrwr3lgia0b6b8xqm88gaxbbqisbdr0kr14m1av8g6jc")))

(define-public crate-solana-sdk-macro-1.9.22 (c (n "solana-sdk-macro") (v "1.9.22") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1ic8wmbypqncf1q37i8fhhyrhmv58mahii2hjlgs6jn0rqwvlvwv")))

(define-public crate-solana-sdk-macro-1.10.15 (c (n "solana-sdk-macro") (v "1.10.15") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0k231739ba1i3pfri5k70f8y9cgnjjpmwwr1lmm8z1aandw9nz79")))

(define-public crate-solana-sdk-macro-1.10.16 (c (n "solana-sdk-macro") (v "1.10.16") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0dlbwb3z0x4q940imvpiqwj6zbwmm7d80rv7915j5nmx5d13dxv0")))

(define-public crate-solana-sdk-macro-1.10.17 (c (n "solana-sdk-macro") (v "1.10.17") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "05hyxw96993jd1cjvsk81wqdfrd7qrfzhv1ivlhd6nahfcmjzpkx")))

(define-public crate-solana-sdk-macro-1.10.18 (c (n "solana-sdk-macro") (v "1.10.18") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "17rl6lpy5qpwjk22gm8l21d8k83jx9p6cimkfjkhxxxw4zhzp3ld")))

(define-public crate-solana-sdk-macro-1.9.23 (c (n "solana-sdk-macro") (v "1.9.23") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0qmvgsqcd67qayr9736whs9g0wcf6nij5yqga7j4p3iqymd9n8m9")))

(define-public crate-solana-sdk-macro-1.10.19 (c (n "solana-sdk-macro") (v "1.10.19") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1anplqq1hycdf1bap98amlv8izgydk23djlzlqfmb8xqycm4ld1c")))

(define-public crate-solana-sdk-macro-1.9.24 (c (n "solana-sdk-macro") (v "1.9.24") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0kjpcljp9yp9sxkpyns5grxfz8q999acmck760c6ih3y39vb2y9x")))

(define-public crate-solana-sdk-macro-1.9.25 (c (n "solana-sdk-macro") (v "1.9.25") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1ps9cz0y567lgwclfhzmri38l94lfhjlsgyir7vrakrz0hh4cfl6")))

(define-public crate-solana-sdk-macro-1.10.20 (c (n "solana-sdk-macro") (v "1.10.20") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1mkswlbkiqhf63l8y76d9p2y1qk1l5kw508wbpdr7bnblm8ha5b1")))

(define-public crate-solana-sdk-macro-1.9.26 (c (n "solana-sdk-macro") (v "1.9.26") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0x1q06p2klphiynsbvap2niw7dvm6gqr0ndyl7hyf994vn8779bw")))

(define-public crate-solana-sdk-macro-1.10.21 (c (n "solana-sdk-macro") (v "1.10.21") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1125h0gvky2jmndmq96jrcs8xh4gn883gdw67wb2fy6b6jk5izp4")))

(define-public crate-solana-sdk-macro-1.10.22 (c (n "solana-sdk-macro") (v "1.10.22") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1x2wncc69izgrkq3n8mpxhi5mnr264x68q67lk3f3944c9pgk4sd")))

(define-public crate-solana-sdk-macro-1.9.28 (c (n "solana-sdk-macro") (v "1.9.28") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "11zc03gmbfsm6rkhypvph9lz56wxgx5x6izh04bcj91krwk6gmhp")))

(define-public crate-solana-sdk-macro-1.10.23 (c (n "solana-sdk-macro") (v "1.10.23") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0darqj9ydaknjj3y7h978hq271s893l9jbghxlj25klwqm274llr")))

(define-public crate-solana-sdk-macro-1.10.24 (c (n "solana-sdk-macro") (v "1.10.24") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1mq1qmdxj1sqhgblg9g129rk9lmh7jan20xicjlcj7sk0ggg22cx")))

(define-public crate-solana-sdk-macro-1.10.25 (c (n "solana-sdk-macro") (v "1.10.25") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0b6p9ksqlw3j7b4aw9wsyx8b7xpmnjxxy4ff6hkivzgid0mwj8n1")))

(define-public crate-solana-sdk-macro-1.9.29 (c (n "solana-sdk-macro") (v "1.9.29") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "05rglqw6v1x4i53pql4ny5a9k1cs2y3gzrjgsl52k49wshxwkd1x")))

(define-public crate-solana-sdk-macro-1.10.26 (c (n "solana-sdk-macro") (v "1.10.26") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0s1lfpzl0yili4zdf6l83md5w843r0dlrlccq8lawfzda3sxdaq3")))

(define-public crate-solana-sdk-macro-1.11.0 (c (n "solana-sdk-macro") (v "1.11.0") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1skcf6bb3cs55zqpa6aj9gym3jszdgjrpm7nnac52aqzwihc80ky")))

(define-public crate-solana-sdk-macro-1.10.27 (c (n "solana-sdk-macro") (v "1.10.27") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0bz752grmyl2p072djgb8pkl7kmda0h9qxfj2cfw3q5wzy079g3z")))

(define-public crate-solana-sdk-macro-1.11.1 (c (n "solana-sdk-macro") (v "1.11.1") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1xflgv3cbrwc0b0qwd0bclj9fw4j8wmlya21xm3j195zb7pcggya")))

(define-public crate-solana-sdk-macro-1.10.28 (c (n "solana-sdk-macro") (v "1.10.28") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1dmcd0wvlwrqa43l7iz48plfg32d32py1q28mq5dmakdcp4q8mil")))

(define-public crate-solana-sdk-macro-1.10.29 (c (n "solana-sdk-macro") (v "1.10.29") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1kbwd34gvqdijx5dwmj0krqdjnslhal5a6z2l9apvjkv5vwlmnfq")))

(define-public crate-solana-sdk-macro-1.10.30 (c (n "solana-sdk-macro") (v "1.10.30") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1gxgjxaa7ilf2gazjn5kwwf2i0p1i298bdp9fm9w0nmm6il6320a")))

(define-public crate-solana-sdk-macro-1.11.2 (c (n "solana-sdk-macro") (v "1.11.2") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1d6img2gn9wmpc58a1f7csdvsdhmjyfam4mxghlcfq9fh257hyzb")))

(define-public crate-solana-sdk-macro-1.10.31 (c (n "solana-sdk-macro") (v "1.10.31") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1kjl8ycp7abpvvhkx94033071mbrapvh6v12k9jcx1mambnm4p17")))

(define-public crate-solana-sdk-macro-1.11.3 (c (n "solana-sdk-macro") (v "1.11.3") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1hpq077hk9c6yir59yqvf91qak0wcvhgncyd4qj5vmpy2sx1938l")))

(define-public crate-solana-sdk-macro-1.10.32 (c (n "solana-sdk-macro") (v "1.10.32") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1xcd757d022wjq08yq7d7hdbhc73i5a0fyrhhbqm1cbhagkl9qgj")))

(define-public crate-solana-sdk-macro-1.11.4 (c (n "solana-sdk-macro") (v "1.11.4") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1d6cv1ha2mvbgyj62brm9x325kd0z5cnqhss2iaq5jx0hv54f7g9")))

(define-public crate-solana-sdk-macro-1.10.33 (c (n "solana-sdk-macro") (v "1.10.33") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0c6f6b2lwarnzr1xzrgkrv76ylwzvykk5s8kq0fz4i26772cm2rb")))

(define-public crate-solana-sdk-macro-1.10.34 (c (n "solana-sdk-macro") (v "1.10.34") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1ampyfnwj5c8bc0xznzf669rvsxi26gv1izga4lg1sdv10hzrjvr")))

(define-public crate-solana-sdk-macro-1.11.5 (c (n "solana-sdk-macro") (v "1.11.5") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "04x6xgign4df8cqkzikfb33h2lbssmb4hl6zn8bwbp3fqhdyinfh")))

(define-public crate-solana-sdk-macro-1.10.35 (c (n "solana-sdk-macro") (v "1.10.35") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "17jxjz3xn6lks6j61rz55zic3x23k24s3m2qicsxga15bbkqsnxn")))

(define-public crate-solana-sdk-macro-1.11.6 (c (n "solana-sdk-macro") (v "1.11.6") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "11jp538xfl99bskagzkagm2xpx4l28f2px66p7b1y0k3wb42djd9")))

(define-public crate-solana-sdk-macro-1.11.7 (c (n "solana-sdk-macro") (v "1.11.7") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1l0s7m5nmwdm6svc26hs1457l7sr2aa6g75wkdb9f5spg8bax753")))

(define-public crate-solana-sdk-macro-1.11.8 (c (n "solana-sdk-macro") (v "1.11.8") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0rp64abnmma0pj9vyssd5fg9g0wibn87wjjr8jg607sy491g3d59")))

(define-public crate-solana-sdk-macro-1.11.10 (c (n "solana-sdk-macro") (v "1.11.10") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "02sjvf1i3h7p9b6yx65jkd9wjf01az2c09ynwy95jcwcpcj9zpk5")))

(define-public crate-solana-sdk-macro-1.10.38 (c (n "solana-sdk-macro") (v "1.10.38") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "10n5s94n1crcdjzbmh3flx9rplisvvwrr5dp4nvprbnlncazrayv")))

(define-public crate-solana-sdk-macro-1.13.0 (c (n "solana-sdk-macro") (v "1.13.0") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1lgxp12bw83dmha4nca7lrz8nfwhqlpfcfzqgifj981xb2q39qy8")))

(define-public crate-solana-sdk-macro-1.14.0 (c (n "solana-sdk-macro") (v "1.14.0") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1skqfy896577z8wy0qaa81rhj8pra73qlc1h8pbppz5b45v8xkrm")))

(define-public crate-solana-sdk-macro-1.14.1 (c (n "solana-sdk-macro") (v "1.14.1") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0jpv1hj2djs6jydjjw4drd0hq5988jz3lnbwv8p4s67jc4shkqb9")))

(define-public crate-solana-sdk-macro-1.10.39 (c (n "solana-sdk-macro") (v "1.10.39") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0y55jkc3aq08k1ym7ykhnipxgs96hb266r5l641rym170cdj9ac9")))

(define-public crate-solana-sdk-macro-1.14.2 (c (n "solana-sdk-macro") (v "1.14.2") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0g6n69b53mfi7drva3wzd8hbq1shwralc4h74b0v35h73d80zn31")))

(define-public crate-solana-sdk-macro-1.13.1 (c (n "solana-sdk-macro") (v "1.13.1") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0zvjn0hip622vqfqfcg819afw23za469klwyhlgq7ldmlgj2j6sb")))

(define-public crate-solana-sdk-macro-1.14.3 (c (n "solana-sdk-macro") (v "1.14.3") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "135ccfmcw5raqdd7cnyycfdgslpyfh7p1r246nc40haf5qajacga")))

(define-public crate-solana-sdk-macro-1.10.40 (c (n "solana-sdk-macro") (v "1.10.40") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0q4yp9zrisr56p4rsasqgg1jpinzs6hx01az5vbrxpdjcpsf3zsh")))

(define-public crate-solana-sdk-macro-1.14.4 (c (n "solana-sdk-macro") (v "1.14.4") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1ihyzxgygyap085k55wb91nvi8r12m9vbjx7nmyncq4cdlbpbc2g")))

(define-public crate-solana-sdk-macro-1.13.2 (c (n "solana-sdk-macro") (v "1.13.2") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0cpnqalszczm22sf6q01d5w9ywp990mgqapp483m3j0ipr8apj33")))

(define-public crate-solana-sdk-macro-1.14.5 (c (n "solana-sdk-macro") (v "1.14.5") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0p3hk199lh6a65hcf9g77gaj2x7czzm11jmsh2bn44vkbfdxxzwj")))

(define-public crate-solana-sdk-macro-1.10.41 (c (n "solana-sdk-macro") (v "1.10.41") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "05a5rapmxqxl319n0j69s7my2nb618fnmdcsxha02mz5a8x6w0wd")))

(define-public crate-solana-sdk-macro-1.13.3 (c (n "solana-sdk-macro") (v "1.13.3") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0hrmh2v7407s524iiaz6z344g4l0z9n4wn8bg8vhjbr79bra975x")))

(define-public crate-solana-sdk-macro-1.13.4 (c (n "solana-sdk-macro") (v "1.13.4") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1dibfgdlna0ipmn8xssxj5clyinjvprq24wz7hzfcka11gwqwivj")))

(define-public crate-solana-sdk-macro-1.14.6 (c (n "solana-sdk-macro") (v "1.14.6") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1gghqa6mr9azc7683hji4bnjkxfb7np46h32qn9rsrxsppqapy8j")))

(define-public crate-solana-sdk-macro-1.14.7 (c (n "solana-sdk-macro") (v "1.14.7") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0ia753iw5vx5czw3d3r96c2zww57skr7yv510zmvkv04920c9350")))

(define-public crate-solana-sdk-macro-1.13.5 (c (n "solana-sdk-macro") (v "1.13.5") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0zhfpq0bbrrvgn59x9qrhazvvjr6jiz0kbnp5z6sqdbvcl5a2dnz")))

(define-public crate-solana-sdk-macro-1.14.8 (c (n "solana-sdk-macro") (v "1.14.8") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0dx1ni6aginwbjy52992jjn6acx8jngbzvimm1mwcprilz8id3vn")))

(define-public crate-solana-sdk-macro-1.14.9 (c (n "solana-sdk-macro") (v "1.14.9") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0gbq4xhs7zxbv1vwqrz7k4ycaa3a96bfz9fbxxnfqwsvrrp05fv2")))

(define-public crate-solana-sdk-macro-1.14.10 (c (n "solana-sdk-macro") (v "1.14.10") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1ks86hf5hdzm09k3s0608hbzxpvinpkxjy0fn46vqgmbvv5aw4gm")))

(define-public crate-solana-sdk-macro-1.14.11 (c (n "solana-sdk-macro") (v "1.14.11") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0x7215q309w3s25a68885vdbbaa9g38xxwa9irlhpis9wqiq78zs")))

(define-public crate-solana-sdk-macro-1.14.12 (c (n "solana-sdk-macro") (v "1.14.12") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1hs00v7xakf9cngb0k6px92rj9yqx4jh2573kvhqyw77y6l196pq")))

(define-public crate-solana-sdk-macro-1.13.6 (c (n "solana-sdk-macro") (v "1.13.6") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0il2c8iqdy0yn2snfwccn3d1bk1m1zp12gj56h7c02x0xkvmpskl")))

(define-public crate-solana-sdk-macro-1.14.13 (c (n "solana-sdk-macro") (v "1.14.13") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1han5j9lx8wn0j0xcb2jff1hrjvd6dj778pk4fhkw2b2v2xarl1k")))

(define-public crate-solana-sdk-macro-1.15.0 (c (n "solana-sdk-macro") (v "1.15.0") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0mh66x4jg0wxgrrvc2gn3x7f2wj4as8an0px1a1bvi0j5hvidqp9") (y #t)))

(define-public crate-solana-sdk-macro-1.14.14 (c (n "solana-sdk-macro") (v "1.14.14") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "036b2hf59mvzhp9j98s0m2nx8ziqgqxlc6pa09zgpjidd7iymcb6")))

(define-public crate-solana-sdk-macro-1.14.15 (c (n "solana-sdk-macro") (v "1.14.15") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "081h3l8n1kicbw21rsaiy0dg21i21vd228n6nxp7rlsf9bcjhzva")))

(define-public crate-solana-sdk-macro-1.15.1 (c (n "solana-sdk-macro") (v "1.15.1") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1s3xw6z19fx4wc2z2d3477h859ssq0x2npllzb98nm504mdi493f") (y #t)))

(define-public crate-solana-sdk-macro-1.15.2 (c (n "solana-sdk-macro") (v "1.15.2") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "03lygwghdwz8ah12ddyv53cp04djhx6vz3n018x7rnnmb29k22gq") (y #t)))

(define-public crate-solana-sdk-macro-1.14.16 (c (n "solana-sdk-macro") (v "1.14.16") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0z4wg3cy2il2lq9rghap6wnvz7p9vqm1k2n7cggs9l7ckjds0hbx")))

(define-public crate-solana-sdk-macro-1.14.17 (c (n "solana-sdk-macro") (v "1.14.17") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1j890bn7jkb1l4lp0y8n9x9mpiajqbhgqgrrpxr78lvx5r858gxp")))

(define-public crate-solana-sdk-macro-1.13.7 (c (n "solana-sdk-macro") (v "1.13.7") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1ypbav5kyx5ssc4wfx6pwizd4yyzcmwmg98ccr1dk8d9ixp70g49")))

(define-public crate-solana-sdk-macro-1.14.18 (c (n "solana-sdk-macro") (v "1.14.18") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0g6daaa7m1gqgxh2ckl3ds03wf41yydn9bvvvvqr8igrl45zdks3")))

(define-public crate-solana-sdk-macro-1.16.0 (c (n "solana-sdk-macro") (v "1.16.0") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.12") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "12hjikfaj114vr5a8v1szjdh3h3ayqbfr9kc1287lgny88nnpimf")))

(define-public crate-solana-sdk-macro-1.16.1 (c (n "solana-sdk-macro") (v "1.16.1") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.12") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0z64xyihczp01xxv1k24q6k6pbiv5yr69yanzkwpxig7d9nix6da")))

(define-public crate-solana-sdk-macro-1.14.19 (c (n "solana-sdk-macro") (v "1.14.19") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "003zm0w9j6qpg5m3hxrzq0pycf2ffzi8nhw00v1k2k4pz4jmppr5")))

(define-public crate-solana-sdk-macro-1.16.2 (c (n "solana-sdk-macro") (v "1.16.2") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.12") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "19lcngjryzlsxssndgm1rrnh5lr4yqsbffjzyx119fjfridxp66m")))

(define-public crate-solana-sdk-macro-1.16.3 (c (n "solana-sdk-macro") (v "1.16.3") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.12") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1wyj34v7cw6vihaypwmybyfqyydqyd9i5z4yjv85j1vv0968i2p3")))

(define-public crate-solana-sdk-macro-1.14.20 (c (n "solana-sdk-macro") (v "1.14.20") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0jc0sf344l0n7vf18i2jr0mxpl19sw3hlnpx30d0w28c0yrkmg2b")))

(define-public crate-solana-sdk-macro-1.16.4 (c (n "solana-sdk-macro") (v "1.16.4") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.12") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1lwqvhjad58vcy84jca4csnl0vnzp2hwwjx93d3spl396zd3ssxw")))

(define-public crate-solana-sdk-macro-1.16.5 (c (n "solana-sdk-macro") (v "1.16.5") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.12") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1ihdf916ppxhcdxw0g15yfc4q51nal8v8y0by1rjcpay87pp6dls")))

(define-public crate-solana-sdk-macro-1.14.21 (c (n "solana-sdk-macro") (v "1.14.21") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0p39cg24jwrmvi296c57dsp00xkc9zrs0j7zd7g1wk4r7pd27ahr")))

(define-public crate-solana-sdk-macro-1.14.22 (c (n "solana-sdk-macro") (v "1.14.22") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "15nzd7d168zv5vlf712m9sbiqqd23kx0jjalcxrmxlqi95fr10wa")))

(define-public crate-solana-sdk-macro-1.16.6 (c (n "solana-sdk-macro") (v "1.16.6") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.12") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1b3lf2aqyzvp1fl5cny7ci5w7rmg2amggdmbw5js5zsrv0lixq8i")))

(define-public crate-solana-sdk-macro-1.16.7 (c (n "solana-sdk-macro") (v "1.16.7") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.12") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0plfifkpr94b4y9k53gh7nnn9npfs23vnxyd21jwv52n81p6hz9f")))

(define-public crate-solana-sdk-macro-1.14.23 (c (n "solana-sdk-macro") (v "1.14.23") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0gvn3227kkw8fnbkfqj6lfdwc3rv5s9gf6g6fskdgbfyjb968m4d")))

(define-public crate-solana-sdk-macro-1.16.8 (c (n "solana-sdk-macro") (v "1.16.8") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.12") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0ag41sgjyqxhvvkimrypg7wajscdiiyyms6b0hvifrg7s74mw38g")))

(define-public crate-solana-sdk-macro-1.14.24 (c (n "solana-sdk-macro") (v "1.14.24") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0lw665ammzqz43bqh9kswp04mmm7gz84dzdxvi05k4bqk996va59")))

(define-public crate-solana-sdk-macro-1.16.9 (c (n "solana-sdk-macro") (v "1.16.9") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.12") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0bvdy4kcpq8hxrwqv5mskbdnxxr18ah4gzagy8sps779x73sirg3")))

(define-public crate-solana-sdk-macro-1.16.10 (c (n "solana-sdk-macro") (v "1.16.10") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.12") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0ypjjbc63d1k0a8svj7nyma9dd0d60if8agi27adcrhhargmgv4i")))

(define-public crate-solana-sdk-macro-1.14.25 (c (n "solana-sdk-macro") (v "1.14.25") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1g9rnd65bi3jrabgjlpgb1k993cs3xkawlindbp2vhj7mzj763mh")))

(define-public crate-solana-sdk-macro-1.16.11 (c (n "solana-sdk-macro") (v "1.16.11") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.12") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1wq7zpi4vah4fqz59196grnxpgwnpyqnd0hv89rc276awg2p8639")))

(define-public crate-solana-sdk-macro-1.14.26 (c (n "solana-sdk-macro") (v "1.14.26") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1p29a10fwk9mv13wzq7ypa42wba9np2nmazgf5mcz8ki8d6l1bp4")))

(define-public crate-solana-sdk-macro-1.16.12 (c (n "solana-sdk-macro") (v "1.16.12") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.12") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "038nwbadrscg9pls3v0g5yp269xf8x3czq6l96cjr43ab12bsxwh")))

(define-public crate-solana-sdk-macro-1.14.27 (c (n "solana-sdk-macro") (v "1.14.27") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "16myaj3jvp5h7i7kixrdg7wqr60xr78281qc9c3fnzm3nbgg2jc3")))

(define-public crate-solana-sdk-macro-1.16.13 (c (n "solana-sdk-macro") (v "1.16.13") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.12") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0iy2z3hvaygyyh23zbp3qqr8blmqpi52vp1xl9jlmykhciqk6nx7")))

(define-public crate-solana-sdk-macro-1.16.14 (c (n "solana-sdk-macro") (v "1.16.14") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.12") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0r7s29xx3wy5qzw6asxn46n0dbpzk74cwv9s2zcjzc7dnzady3vn")))

(define-public crate-solana-sdk-macro-1.14.28 (c (n "solana-sdk-macro") (v "1.14.28") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1a3zrvpbskkjj6wylwvk5d7vy61gsnbzp120q23l7a390phvp4ci")))

(define-public crate-solana-sdk-macro-1.14.29 (c (n "solana-sdk-macro") (v "1.14.29") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0sr07l6x476vgqqs1cx8sxz0irzfrsw3yyvp0rkfw0zms1kqna22")))

(define-public crate-solana-sdk-macro-1.16.15 (c (n "solana-sdk-macro") (v "1.16.15") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.12") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1lgqljh8fi9jb20vf24z32n0kxpgd3b2gh4j28pkc2iszds7l5xx")))

(define-public crate-solana-sdk-macro-1.17.0 (c (n "solana-sdk-macro") (v "1.17.0") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.14") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "07bxr41invpfx8ppms0ak7w6hkrxjc3n9w0xz3dy97n045s4mnm1")))

(define-public crate-solana-sdk-macro-1.16.16 (c (n "solana-sdk-macro") (v "1.16.16") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.12") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1fj8rsyq63w7vj7za6yx72kbzizl47dhi8db4gipr8x45lxvchab")))

(define-public crate-solana-sdk-macro-1.17.1 (c (n "solana-sdk-macro") (v "1.17.1") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.14") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "15f184mn7nhliwbaysvc3lsswr0c0pxp45mjkmf2ak0jj8zj5lcd")))

(define-public crate-solana-sdk-macro-1.16.17 (c (n "solana-sdk-macro") (v "1.16.17") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.12") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "13z7bqxqb38x7yffg8zz7av5gdwbsqn6xci04bmig5w5lc30hmhy")))

(define-public crate-solana-sdk-macro-1.17.2 (c (n "solana-sdk-macro") (v "1.17.2") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.14") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "12f7hh1372vs9h0dy2r1gf7ac2n1cwl7m8rzq1s5rfk2v23kq3qp")))

(define-public crate-solana-sdk-macro-1.16.18 (c (n "solana-sdk-macro") (v "1.16.18") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.12") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1bipd3bvk05yjpvwkzjlpqdkv5q1nhfp2f8a5245yr9dyv8y5ym1")))

(define-public crate-solana-sdk-macro-1.17.3 (c (n "solana-sdk-macro") (v "1.17.3") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.14") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1rl1swhl2c09qj84q5726az599df5zyx3db88h1ky8gc9jga8p01")))

(define-public crate-solana-sdk-macro-1.17.4 (c (n "solana-sdk-macro") (v "1.17.4") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.14") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1lxypcamx7cmg5n08n5c2d4hl3d263w7pcz0dydcsfyp8ziyip4p")))

(define-public crate-solana-sdk-macro-1.16.19 (c (n "solana-sdk-macro") (v "1.16.19") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.12") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1v0vw9r5jgxz3a0rff7ryn6r3qd8y3p96v9jw1alqplj4famkjnv")))

(define-public crate-solana-sdk-macro-1.17.5 (c (n "solana-sdk-macro") (v "1.17.5") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.14") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0i7czyfpjqs5s2pzfc6k5pnb5yj0hzs17d3ha1i6rdl1yxv5365p")))

(define-public crate-solana-sdk-macro-1.17.6 (c (n "solana-sdk-macro") (v "1.17.6") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.14") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0ks6mdsz4d1qinz22m64ggfy9p67s4k76gpav612rydarqd3pkkw")))

(define-public crate-solana-sdk-macro-1.16.20 (c (n "solana-sdk-macro") (v "1.16.20") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.12") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "17b8n2rpgx2s3kh4126kahsdnkjpcfq1iallxhjm79q34lykdr4z")))

(define-public crate-solana-sdk-macro-1.17.7 (c (n "solana-sdk-macro") (v "1.17.7") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.14") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0j76vhw2l0ldgasjfl8dlgc81cfgy5prrcyhmcw0537ys9j464dk")))

(define-public crate-solana-sdk-macro-1.16.21 (c (n "solana-sdk-macro") (v "1.16.21") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.12") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1jnxai2cy2gpmdhgy6qc1brvsybwn6k3j7i9mblmdzqbm4h6lvmi")))

(define-public crate-solana-sdk-macro-1.17.8 (c (n "solana-sdk-macro") (v "1.17.8") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.14") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1rxdm3jwwkw7pz6acaaca1gh1a2h298wdj24iycx091gpg0jszb7")))

(define-public crate-solana-sdk-macro-1.16.22 (c (n "solana-sdk-macro") (v "1.16.22") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.12") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0kq203xn6cin7hxl9a5m7qci3k6s6phzxs0vwhqzbmbxqlx49z60")))

(define-public crate-solana-sdk-macro-1.16.23 (c (n "solana-sdk-macro") (v "1.16.23") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.12") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0cq55qrcrk2927np2kd34p2glb1k6c1ivnpin3ca2v2dnxwrjx3d")))

(define-public crate-solana-sdk-macro-1.17.9 (c (n "solana-sdk-macro") (v "1.17.9") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.14") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "10996jhf5h8ja1235iyhvxgsrg5kxbgz6hz1p5a52lxddkfk136g")))

(define-public crate-solana-sdk-macro-1.17.10 (c (n "solana-sdk-macro") (v "1.17.10") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.14") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0iy6pqrsd7lcmis5fds3bqdsildf7zdlvd22l86d7n6mjrqm75mc")))

(define-public crate-solana-sdk-macro-1.17.11 (c (n "solana-sdk-macro") (v "1.17.11") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.14") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0npgkghg7d92c6cjy16mmb0gi66ccvllngc2i7ri93pmkb80dda4")))

(define-public crate-solana-sdk-macro-1.17.12 (c (n "solana-sdk-macro") (v "1.17.12") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.14") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0zszfarspv77pg7vznsvwh6pk6qkqsf11jwjlm88vzdjxgr1yypl")))

(define-public crate-solana-sdk-macro-1.16.24 (c (n "solana-sdk-macro") (v "1.16.24") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.12") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1ypaqfy4yp3c2m17jm2nq60vj00r5rj9l6dhr9yyqzay5cgsxifm")))

(define-public crate-solana-sdk-macro-1.17.13 (c (n "solana-sdk-macro") (v "1.17.13") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.14") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1m1znxvpav2gap35zgsqm6gx4r4ljxk76drfvp7fd3xxpa871vjz")))

(define-public crate-solana-sdk-macro-1.17.14 (c (n "solana-sdk-macro") (v "1.17.14") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.14") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "04k7g0shbqxqkyc4zc3s0x5n71l5mnjxyvw2kf63px29x638gxb0")))

(define-public crate-solana-sdk-macro-1.17.15 (c (n "solana-sdk-macro") (v "1.17.15") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.14") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1d9fvcc06a1fk1jd680rx7nsz8lg3263sahgh0l1ixhh69baznwj")))

(define-public crate-solana-sdk-macro-1.16.25 (c (n "solana-sdk-macro") (v "1.16.25") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.12") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1fqr7jz63nrb3nv9hnjb6bi5sryfcyg4wkqxk8bzkc44x5z0w3if")))

(define-public crate-solana-sdk-macro-1.16.26 (c (n "solana-sdk-macro") (v "1.16.26") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.12") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1piwq9mmh98l9wqxvbns3m9ki7vx03cavyil0s7xap8rf57larc5")))

(define-public crate-solana-sdk-macro-1.16.27 (c (n "solana-sdk-macro") (v "1.16.27") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.12") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0akfbqicn4i6pdhdiymkfk2gri3gwz5ll9r295x170v1wxpqs63z")))

(define-public crate-solana-sdk-macro-1.17.16 (c (n "solana-sdk-macro") (v "1.17.16") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.14") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1h5cjslwcjd5hmy082h0fbgkhk9zb304vwxbwy5zap97yj418184")))

(define-public crate-solana-sdk-macro-1.17.17 (c (n "solana-sdk-macro") (v "1.17.17") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.14") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "00mmp4g1bl31r37lm5m5hk2jhi0zh3v577k236yvz9q147x8jx51")))

(define-public crate-solana-sdk-macro-1.17.18 (c (n "solana-sdk-macro") (v "1.17.18") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.14") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0hcjqjymqkrzyivpc6gf8b9zflzwqw0c2aimx0l0qx0gdyp698l6")))

(define-public crate-solana-sdk-macro-1.18.0 (c (n "solana-sdk-macro") (v "1.18.0") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.14") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0kdfganhvj2bv8yj7kl2dl53lrwjzqlz881cggv93fp9v6gaw8yi")))

(define-public crate-solana-sdk-macro-1.18.1 (c (n "solana-sdk-macro") (v "1.18.1") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.14") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1i9gzpgq5amvsyb4qylp2czdrd0iycwrryf38f9yjrkac7wcvqx3")))

(define-public crate-solana-sdk-macro-1.17.20 (c (n "solana-sdk-macro") (v "1.17.20") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.14") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1ggxpqxnwbjfjvyg1gh6rhvyb4zw43xh7zv0j7km8bnv794db9y4")))

(define-public crate-solana-sdk-macro-1.17.22 (c (n "solana-sdk-macro") (v "1.17.22") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.14") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1jh9z8j0cypr2my3dv075c7mnhxhcs6craky85kls7y4x84xfki0")))

(define-public crate-solana-sdk-macro-1.18.2 (c (n "solana-sdk-macro") (v "1.18.2") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.14") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "07jiqycy78a7x6il0vvi237cqq5pklz5x52dnv4jwasy8lgrd593")))

(define-public crate-solana-sdk-macro-1.17.23 (c (n "solana-sdk-macro") (v "1.17.23") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.14") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0wilqzcyldhx4056n21w3nl1nlvxv7xgmh9d91r19j57hinz4hsj")))

(define-public crate-solana-sdk-macro-1.18.3 (c (n "solana-sdk-macro") (v "1.18.3") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.14") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0k9s9fvh2ay31pjx86ix07ljmn9j7jhdq47qzw7iyympbfn8wa8r")))

(define-public crate-solana-sdk-macro-1.18.4 (c (n "solana-sdk-macro") (v "1.18.4") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.14") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "14p5rx1zl028m324zw0jyncqy6qpbdjnbxyfhac1gfim1qijr5fz")))

(define-public crate-solana-sdk-macro-1.17.24 (c (n "solana-sdk-macro") (v "1.17.24") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.14") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "18ypdaml5vk906ji64daagj58h64zxfzc4msc15y60ygan1xi2mx")))

(define-public crate-solana-sdk-macro-1.17.25 (c (n "solana-sdk-macro") (v "1.17.25") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.14") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1m8wnimqpg4n4cc6ahzy0m8pb5d7s6yqgmjj5xgkxkw5nz25al4v")))

(define-public crate-solana-sdk-macro-1.18.5 (c (n "solana-sdk-macro") (v "1.18.5") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.14") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0vxl68kf542yj1vhr1k3216b7378hvzd8swmghcr981mhc77dhg1")))

(define-public crate-solana-sdk-macro-1.17.26 (c (n "solana-sdk-macro") (v "1.17.26") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.14") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "18vzryb92zcnwjdr4lz6xjv7k9iga4sr8b9zy27w2rwfj78i8as2")))

(define-public crate-solana-sdk-macro-1.18.6 (c (n "solana-sdk-macro") (v "1.18.6") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.14") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "07fncyq9968yavlccxh5ydwkg2fp35gmqpl4h0457j9zl2ar3bs8")))

(define-public crate-solana-sdk-macro-1.17.27 (c (n "solana-sdk-macro") (v "1.17.27") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.14") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0ky92gigjnx3k2zb1i4s5p7iz4cfidwim8w2yzi53c76d457nya4")))

(define-public crate-solana-sdk-macro-1.18.7 (c (n "solana-sdk-macro") (v "1.18.7") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.14") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0jwv9j7kfrji9rapv8dh1x81b97kcdgm5g3jgfx9x1bv1ggj6zj5")))

(define-public crate-solana-sdk-macro-1.18.8 (c (n "solana-sdk-macro") (v "1.18.8") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.14") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1cmn3fnyfi2vp6l73gywv9wgnl7zcg7kkrpzpj7045a8qzp29zsw")))

(define-public crate-solana-sdk-macro-1.17.28 (c (n "solana-sdk-macro") (v "1.17.28") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.14") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1177w45dzpym5iabwcp72lnj7i3j89mpziifldh34jh8qhrh97az")))

(define-public crate-solana-sdk-macro-1.18.9 (c (n "solana-sdk-macro") (v "1.18.9") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.14") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "15dpb83dy9q3df117p5gyv9gpx057n5856kih4mr3s10d3s28fvg")))

(define-public crate-solana-sdk-macro-1.17.29 (c (n "solana-sdk-macro") (v "1.17.29") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.14") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1sg2v7glyr9xf3h9wb7xpj8w5x8c4c0ka6skpsq58zykn54cccrs")))

(define-public crate-solana-sdk-macro-1.17.30 (c (n "solana-sdk-macro") (v "1.17.30") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.14") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "086l0r51r90ds4xsdjf3y04sm6m0r0f9dnffvv7kh0dv8jhx4m7m")))

(define-public crate-solana-sdk-macro-1.18.10 (c (n "solana-sdk-macro") (v "1.18.10") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.14") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1pc64bdvnr8shsi882lgpq2f4srpg9272yh6lcsf0pa1fiwgn69a")))

(define-public crate-solana-sdk-macro-1.18.11 (c (n "solana-sdk-macro") (v "1.18.11") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.14") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1pmcrc9x18lvxvgfx6ilr2l7ckqaw02rx3d0jm6pvlqqbz087546")))

(define-public crate-solana-sdk-macro-1.17.31 (c (n "solana-sdk-macro") (v "1.17.31") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.14") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1xv4yajqg8w5x39mc5638nn3c54l49mq8k0ailnlg9d0pmmw8p5w")))

(define-public crate-solana-sdk-macro-1.18.12 (c (n "solana-sdk-macro") (v "1.18.12") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.14") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0q9qc47s0f7qzq21rl84gg16ika1a4r93myf4dpmm9n0z6r9kc2w")))

(define-public crate-solana-sdk-macro-1.17.32 (c (n "solana-sdk-macro") (v "1.17.32") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.14") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1k144cj048fwapxi9zaa6iz93f1gb5lq0xgkbm1vsxx74jr92kj2")))

(define-public crate-solana-sdk-macro-1.17.33 (c (n "solana-sdk-macro") (v "1.17.33") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.14") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1spzz9qq0ag0js7mva8xwb484amd7swhslz9r7b113zf839hg2df")))

(define-public crate-solana-sdk-macro-1.18.13 (c (n "solana-sdk-macro") (v "1.18.13") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.14") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1g87vvgvbjsg2l96rvg890yn7vvag95ai8glyqbsvx84zy50hvgz")))

(define-public crate-solana-sdk-macro-1.18.14 (c (n "solana-sdk-macro") (v "1.18.14") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.14") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0wcw9klfd4zkfchrmm3n19bv59lwvmzkwsrfpgzx41kgxp1b4x99")))

(define-public crate-solana-sdk-macro-1.17.34 (c (n "solana-sdk-macro") (v "1.17.34") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.14") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "07sil5k9fb3p7i7nb7gvwbr4va8gad2jrsyz4fdz01vcgycwq6c0")))

