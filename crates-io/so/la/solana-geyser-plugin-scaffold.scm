(define-module (crates-io so la solana-geyser-plugin-scaffold) #:use-module (crates-io))

(define-public crate-solana-geyser-plugin-scaffold-1.11.1 (c (n "solana-geyser-plugin-scaffold") (v "1.11.1") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "solana-geyser-plugin-interface") (r "=1.11.1") (d #t) (k 0)) (d (n "solana-logger") (r "=1.11.1") (d #t) (k 0)))) (h "0wf97pd2gvav9lvm672pcgrcfg3rwpbjksbm1syjvl91hh87vgj1") (y #t)))

(define-public crate-solana-geyser-plugin-scaffold-1.11.2 (c (n "solana-geyser-plugin-scaffold") (v "1.11.2") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "solana-geyser-plugin-interface") (r "=1.11.1") (d #t) (k 0)) (d (n "solana-logger") (r "=1.11.1") (d #t) (k 0)))) (h "0s2icasyjhqjsq89mh8vr1ysf3zic4cfr5f73g44z3wxwkg6lhap")))

