(define-module (crates-io so la solana-fixed-buf) #:use-module (crates-io))

(define-public crate-solana-fixed-buf-0.18.0-pre2 (c (n "solana-fixed-buf") (v "0.18.0-pre2") (d (list (d (n "bincode") (r "^1.1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.99") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.99") (d #t) (k 2)))) (h "0h6sh9vvfdv3cmc21n1n6rrvqgdzb6qp2ln977yi7m6sbl0vsisq")))

(define-public crate-solana-fixed-buf-0.18.0 (c (n "solana-fixed-buf") (v "0.18.0") (d (list (d (n "bincode") (r "^1.1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.99") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.99") (d #t) (k 2)))) (h "0cgjwzm11k63r25namdl465nz4ypwd6p5rv6rmwqyr1j1ih98igb")))

(define-public crate-solana-fixed-buf-0.18.1 (c (n "solana-fixed-buf") (v "0.18.1") (d (list (d (n "bincode") (r "^1.1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.99") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.99") (d #t) (k 2)))) (h "0gz90b2c8afi1yxjqwrjgm022s6skmjg79c8nncmwlrbvkv2a8hj")))

(define-public crate-solana-fixed-buf-0.19.1 (c (n "solana-fixed-buf") (v "0.19.1") (d (list (d (n "bincode") (r "^1.1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.101") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.101") (d #t) (k 2)))) (h "05689i1w3p0k1r9ypzljb3dh3g4p2qhjjyzhm4p40js5slns1440")))

(define-public crate-solana-fixed-buf-0.20.1 (c (n "solana-fixed-buf") (v "0.20.1") (d (list (d (n "bincode") (r "^1.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.101") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.101") (d #t) (k 2)))) (h "08c89n70dcxazc0qsvx165fphfmp796s2clgdb77zn777k8rjigm")))

(define-public crate-solana-fixed-buf-0.20.2 (c (n "solana-fixed-buf") (v "0.20.2") (d (list (d (n "bincode") (r "^1.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.101") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.101") (d #t) (k 2)))) (h "10m74xm8pmlmi0431389aa9g1s1d25ydb6djn8w1ibmsslfrkpr4")))

(define-public crate-solana-fixed-buf-0.20.3 (c (n "solana-fixed-buf") (v "0.20.3") (d (list (d (n "bincode") (r "^1.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.101") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.101") (d #t) (k 2)))) (h "0vvfvkr9rqgqrwmflxff0srfxdgk0hc7z18czqddpbjklanqzmlb")))

(define-public crate-solana-fixed-buf-0.20.4 (c (n "solana-fixed-buf") (v "0.20.4") (d (list (d (n "bincode") (r "^1.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.101") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.101") (d #t) (k 2)))) (h "0dn7rslgx7h22ws1qgqnmxdg2wpiwasahs3v3pz5jxc8anjp97pj")))

(define-public crate-solana-fixed-buf-0.20.5 (c (n "solana-fixed-buf") (v "0.20.5") (d (list (d (n "bincode") (r "^1.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.101") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.101") (d #t) (k 2)))) (h "1yynw584ryy36bhvv4v91wlgmwn94ddx1g0qg24xbqp7qb8svqky")))

(define-public crate-solana-fixed-buf-0.21.0 (c (n "solana-fixed-buf") (v "0.21.0") (d (list (d (n "bincode") (r "^1.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.102") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.102") (d #t) (k 2)))) (h "133bizjy2h2m9fgl1arg07dgangncbx2cb098ggkxivb8xqwfaf9")))

(define-public crate-solana-fixed-buf-0.21.1 (c (n "solana-fixed-buf") (v "0.21.1") (d (list (d (n "bincode") (r "^1.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.102") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.102") (d #t) (k 2)))) (h "0pd6pyry422skn4dl3j1qbfw539wvh4zi4339fza4cvarrmp3awn")))

(define-public crate-solana-fixed-buf-0.21.2 (c (n "solana-fixed-buf") (v "0.21.2") (d (list (d (n "bincode") (r "^1.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.102") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.102") (d #t) (k 2)))) (h "15l4cpzs38rfnair00djg4z8dqbwx0n5g2jf930gj4l51xrk9qw8")))

(define-public crate-solana-fixed-buf-0.21.3 (c (n "solana-fixed-buf") (v "0.21.3") (d (list (d (n "bincode") (r "^1.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.102") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.102") (d #t) (k 2)))) (h "0hh1yhk6ck50xmrg0nrwdzji9snsxf4jniq0l1z82wfq150hjlqx")))

(define-public crate-solana-fixed-buf-0.21.4 (c (n "solana-fixed-buf") (v "0.21.4") (d (list (d (n "bincode") (r "^1.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.102") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.102") (d #t) (k 2)))) (h "0zq8bpwxvwnyh29sjxy7pgycgl6hbpacngbyyfycf7ljb1n6s0rw")))

(define-public crate-solana-fixed-buf-0.21.5 (c (n "solana-fixed-buf") (v "0.21.5") (d (list (d (n "bincode") (r "^1.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.102") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.102") (d #t) (k 2)))) (h "090dp793b742g780n8rg9rfz5jrq72y24fzlqnnys6ihprmwn3cp")))

(define-public crate-solana-fixed-buf-0.22.0 (c (n "solana-fixed-buf") (v "0.22.0") (d (list (d (n "bincode") (r "^1.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.103") (d #t) (k 2)))) (h "171v9srxkr5rb7ira7rja3g0xwns34xa9cxr4pk7ihx7qikbx7qz")))

(define-public crate-solana-fixed-buf-0.21.6 (c (n "solana-fixed-buf") (v "0.21.6") (d (list (d (n "bincode") (r "^1.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.102") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.102") (d #t) (k 2)))) (h "0902yc7f64cqzk9sllb2vbrf8kpqhivlxwhcsj13qplbn129lf0q")))

(define-public crate-solana-fixed-buf-0.22.1 (c (n "solana-fixed-buf") (v "0.22.1") (d (list (d (n "bincode") (r "^1.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.103") (d #t) (k 2)))) (h "1mfipgrnfqx1xvc7dnp5fin0xc9y4w1dvli7h05d8yiyrjpxknwj")))

(define-public crate-solana-fixed-buf-0.22.2 (c (n "solana-fixed-buf") (v "0.22.2") (d (list (d (n "bincode") (r "^1.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.103") (d #t) (k 2)))) (h "0q35p58pxmy42982lngq9bjdbsk3hk9g10mhcz6aw61skvjny6jy")))

(define-public crate-solana-fixed-buf-0.21.7 (c (n "solana-fixed-buf") (v "0.21.7") (d (list (d (n "bincode") (r "^1.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.102") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.102") (d #t) (k 2)))) (h "14rzbc5q6ri866248nvspsbr6yib01r4g539rv8nrar5h49lzp23")))

(define-public crate-solana-fixed-buf-0.22.3 (c (n "solana-fixed-buf") (v "0.22.3") (d (list (d (n "bincode") (r "^1.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.103") (d #t) (k 2)))) (h "1i2z1ikf2wl82padisc4qk9500iic2lgm43zrs12zsl8m8xi13dj")))

(define-public crate-solana-fixed-buf-0.22.4 (c (n "solana-fixed-buf") (v "0.22.4") (d (list (d (n "bincode") (r "^1.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.103") (d #t) (k 2)))) (h "06zmadpqbxzz6zhp1s0gvfnfb7kbr76mf8rndxgrjljyih65il8h")))

(define-public crate-solana-fixed-buf-0.22.5 (c (n "solana-fixed-buf") (v "0.22.5") (d (list (d (n "bincode") (r "^1.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.103") (d #t) (k 2)))) (h "1i7wc21d0jal116pk5p85qjwnqaqwj2ppmyk7hbm4s742pawkpd2")))

(define-public crate-solana-fixed-buf-0.22.6 (c (n "solana-fixed-buf") (v "0.22.6") (d (list (d (n "bincode") (r "^1.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.103") (d #t) (k 2)))) (h "1cj9wnh3vssvz5fxs3l2hzb85xvkmdqji6jg0klz2xdrl6ahjxkf")))

(define-public crate-solana-fixed-buf-0.22.7 (c (n "solana-fixed-buf") (v "0.22.7") (d (list (d (n "bincode") (r "^1.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.103") (d #t) (k 2)))) (h "19a4w0krvjpj5fkys18cz4w54v9lgk3qkdjfjd0y5qy7imv8hm8j")))

(define-public crate-solana-fixed-buf-0.22.8 (c (n "solana-fixed-buf") (v "0.22.8") (d (list (d (n "bincode") (r "^1.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.103") (d #t) (k 2)))) (h "0baynlpsskniw18l3xhhsvmxpfh5pz28hicv8y9505dqmwi23ir1")))

(define-public crate-solana-fixed-buf-0.22.9 (c (n "solana-fixed-buf") (v "0.22.9") (d (list (d (n "bincode") (r "^1.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.103") (d #t) (k 2)))) (h "1czwah06p6akg4lz287i8qpmsak891lyqabivhym4bsfzi0n960x")))

