(define-module (crates-io so la solana-noop) #:use-module (crates-io))

(define-public crate-solana-noop-0.10.0-pre2 (c (n "solana-noop") (v "0.10.0-pre2") (d (list (d (n "solana-sdk") (r "^0.10.0-pre2") (d #t) (k 0)))) (h "0b82z2sasb1190c5cfpnjwvdnssn15slzzsbm57m1bq2bk8v1xsf")))

(define-public crate-solana-noop-0.10.0 (c (n "solana-noop") (v "0.10.0") (d (list (d (n "solana-sdk") (r "^0.10.0") (d #t) (k 0)))) (h "0s03pmbj0sbx7z5wrzpn425g11dppkp9qjwcvlsparsd26kjbnx4")))

(define-public crate-solana-noop-0.10.1 (c (n "solana-noop") (v "0.10.1") (d (list (d (n "solana-sdk") (r "^0.10.1") (d #t) (k 0)))) (h "0slbwnmaivxghh8il5sg5b1wn5fbdvay8gl75rg87g7x7fdlq89s")))

(define-public crate-solana-noop-0.10.2 (c (n "solana-noop") (v "0.10.2") (d (list (d (n "solana-sdk") (r "^0.10.2") (d #t) (k 0)))) (h "09vzagi3b2rjswznfw03jkp0y470avy6ll8ai1832mc9skqqj5mm")))

(define-public crate-solana-noop-0.10.3 (c (n "solana-noop") (v "0.10.3") (d (list (d (n "solana-sdk") (r "^0.10.3") (d #t) (k 0)))) (h "1jbf7c0br1njhr12i8zxb5d1kk72qfnq5w7g532rp91wql67062a")))

(define-public crate-solana-noop-0.10.4 (c (n "solana-noop") (v "0.10.4") (d (list (d (n "solana-sdk") (r "^0.10.4") (d #t) (k 0)))) (h "1cvbbzrdywb7q6wb2brnd509bl2bw5cpnzr070cvb7z3xksxf63f")))

(define-public crate-solana-noop-0.11.0 (c (n "solana-noop") (v "0.11.0") (d (list (d (n "log") (r "^0.4.2") (d #t) (k 0)) (d (n "solana-logger") (r "^0.11.0") (d #t) (k 0)) (d (n "solana-sdk") (r "^0.11.0") (d #t) (k 0)))) (h "09ihmh37mxgqhrplc20vi4ijwmsrni3a1j85mfafqvns308yv1a0")))

