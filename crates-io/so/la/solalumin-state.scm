(define-module (crates-io so la solalumin-state) #:use-module (crates-io))

(define-public crate-solalumin-state-0.1.0-alpha.1 (c (n "solalumin-state") (v "0.1.0-alpha.1") (d (list (d (n "base64") (r "^0.21.1") (d #t) (k 0)) (d (n "borsh") (r "^0.10.3") (d #t) (k 0)) (d (n "borsh-derive") (r "^0.10.3") (d #t) (k 0)) (d (n "bs58") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.189") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "sha2") (r "^0.10.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)) (d (n "version") (r "^3.0.0") (d #t) (k 0)))) (h "1lgivz2j7ki3q0hah596x7a0qc36xp05rqznawn78k3799s5ggi3") (r "1.60")))

