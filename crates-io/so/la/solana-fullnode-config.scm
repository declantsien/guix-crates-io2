(define-module (crates-io so la solana-fullnode-config) #:use-module (crates-io))

(define-public crate-solana-fullnode-config-0.11.0 (c (n "solana-fullnode-config") (v "0.11.0") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "dirs") (r "^1.0.2") (d #t) (k 0)) (d (n "log") (r "^0.4.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.82") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.82") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.10") (d #t) (k 0)) (d (n "solana-netutil") (r "^0.11.0") (d #t) (k 0)) (d (n "solana-sdk") (r "^0.11.0") (d #t) (k 0)) (d (n "untrusted") (r "^0.6.2") (d #t) (k 0)))) (h "0p6q0l86vpbc2z3sdxcc6p38pbf1ccxqla5lys9xh2mc9gkp80ll") (f (quote (("cuda"))))))

