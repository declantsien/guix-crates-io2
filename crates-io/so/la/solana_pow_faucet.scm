(define-module (crates-io so la solana_pow_faucet) #:use-module (crates-io))

(define-public crate-solana_pow_faucet-0.0.1 (c (n "solana_pow_faucet") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "shellexpand") (r "^3.1.0") (d #t) (k 0)) (d (n "solana-cli-config") (r "^1.16.8") (d #t) (k 0)) (d (n "solana-client") (r "^1.16.8") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.16.8") (d #t) (k 0)))) (h "0m6b4fz7kf1qxfc3840y2nx1h7ibzlcic6nalwsh2c3zj410f524")))

