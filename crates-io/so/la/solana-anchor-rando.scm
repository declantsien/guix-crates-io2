(define-module (crates-io so la solana-anchor-rando) #:use-module (crates-io))

(define-public crate-solana-anchor-rando-0.1.0 (c (n "solana-anchor-rando") (v "0.1.0") (d (list (d (n "anchor-lang") (r "^0.17.0") (d #t) (k 0)) (d (n "spl-math") (r "^0.1.0") (f (quote ("no-entrypoint"))) (d #t) (k 0)))) (h "08nzbsrii54f6p2xis6ng8rnjc1b5hs3ylh7v7fhhv9yr3p6h83l") (f (quote (("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-solana-anchor-rando-0.1.1 (c (n "solana-anchor-rando") (v "0.1.1") (d (list (d (n "anchor-lang") (r "^0.17.0") (d #t) (k 0)) (d (n "spl-math") (r "^0.1.0") (f (quote ("no-entrypoint"))) (d #t) (k 0)))) (h "0iyiwkx6qr9880n2fw6k3rhr56p59rlizw35w8ykfcv3y7zn45g0") (f (quote (("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-solana-anchor-rando-0.1.2 (c (n "solana-anchor-rando") (v "0.1.2") (d (list (d (n "anchor-lang") (r "^0.17.0") (d #t) (k 0)))) (h "18x3c6k4a5rgyq8mp78jg5milkj2mqi2z3g1slxc5dx8hxpxff3d") (f (quote (("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

