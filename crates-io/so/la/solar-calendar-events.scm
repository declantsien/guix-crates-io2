(define-module (crates-io so la solar-calendar-events) #:use-module (crates-io))

(define-public crate-solar-calendar-events-0.1.0 (c (n "solar-calendar-events") (v "0.1.0") (d (list (d (n "time") (r "^0.3.20") (f (quote ("macros"))) (d #t) (k 0)))) (h "1kyp871vww25bjpfibcd8qmagjnwvj53jda8pp80z33dsyx2qf2i")))

(define-public crate-solar-calendar-events-0.1.1 (c (n "solar-calendar-events") (v "0.1.1") (d (list (d (n "time") (r "^0.3.20") (f (quote ("macros"))) (d #t) (k 0)))) (h "0kk5ly1100lg20v1426kxwl0mnx3a8qjv9anag7dwys7ai8lzkyn")))

(define-public crate-solar-calendar-events-0.1.2 (c (n "solar-calendar-events") (v "0.1.2") (d (list (d (n "time") (r "^0.3.20") (f (quote ("macros"))) (d #t) (k 0)))) (h "1jpc4q141nxyvw1hrajgbmvgbiwj9ws9x3bmgcdhhpky1278hlsa")))

(define-public crate-solar-calendar-events-0.1.3 (c (n "solar-calendar-events") (v "0.1.3") (d (list (d (n "time") (r "^0.3.20") (f (quote ("macros"))) (d #t) (k 0)))) (h "0rmfjraadfzvfz4idn6jw7v4ki525gqap0lgdkwmpmya33w04vpw")))

(define-public crate-solar-calendar-events-0.1.4 (c (n "solar-calendar-events") (v "0.1.4") (d (list (d (n "time") (r "^0.3.20") (f (quote ("macros"))) (d #t) (k 0)))) (h "0sw90axmpk20slrqj0ngqlxakqs7syk0ffrdrz4m1gn378xmdrk9")))

