(define-module (crates-io so la solana_libra_state_view) #:use-module (crates-io))

(define-public crate-solana_libra_state_view-0.0.0-sol14 (c (n "solana_libra_state_view") (v "0.0.0-sol14") (d (list (d (n "failure") (r "^0.0.0-sol14") (d #t) (k 0) (p "solana_libra_failure_ext")) (d (n "types") (r "^0.0.0-sol14") (d #t) (k 0) (p "solana_libra_types")))) (h "1973m5hlvrnf7wrisd3l299yyk9j5i1qz1vzshrgmlsbgb6nqbr2") (y #t)))

(define-public crate-solana_libra_state_view-0.0.0-sol15 (c (n "solana_libra_state_view") (v "0.0.0-sol15") (d (list (d (n "failure") (r "^0.0.0-sol15") (d #t) (k 0) (p "solana_libra_failure_ext")) (d (n "types") (r "^0.0.0-sol15") (d #t) (k 0) (p "solana_libra_types")))) (h "1ang3vja2sml24ynsfk3i2czw30a8nr8s3iv4ip6ayi3dplgy5r8")))

(define-public crate-solana_libra_state_view-0.0.0 (c (n "solana_libra_state_view") (v "0.0.0") (d (list (d (n "failure") (r "^0.0.0") (d #t) (k 0) (p "solana_libra_failure_ext")) (d (n "types") (r "^0.0.0") (d #t) (k 0) (p "solana_libra_types")))) (h "00axxdf7n4yz31c8953jla36zvrbjhsrb9fln60l90y8ig7307dq")))

(define-public crate-solana_libra_state_view-0.0.1-sol4 (c (n "solana_libra_state_view") (v "0.0.1-sol4") (d (list (d (n "failure") (r "^0.0.1-sol4") (d #t) (k 0) (p "solana_libra_failure_ext")) (d (n "solana_libra_types") (r "^0.0.1-sol4") (d #t) (k 0)) (d (n "solana_libra_types") (r "^0.0.1-sol4") (f (quote ("testing"))) (d #t) (k 2)))) (h "06gn28674ah6c5813vzcscsaxjl9bxy6a6rigbvpkj2xqfr2cbal")))

(define-public crate-solana_libra_state_view-0.0.1-sol5 (c (n "solana_libra_state_view") (v "0.0.1-sol5") (d (list (d (n "failure") (r "^0.0.1-sol5") (d #t) (k 0) (p "solana_libra_failure_ext")) (d (n "solana_libra_types") (r "^0.0.1-sol5") (d #t) (k 0)) (d (n "solana_libra_types") (r "^0.0.1-sol5") (f (quote ("testing"))) (d #t) (k 2)))) (h "0la0x9gg6vyjzz1yyifvlkh49x6q7swxrafnh7vg9vbk4blsrncw")))

