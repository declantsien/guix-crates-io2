(define-module (crates-io so la solana-invoke) #:use-module (crates-io))

(define-public crate-solana-invoke-0.1.0 (c (n "solana-invoke") (v "0.1.0") (d (list (d (n "solana-nostd-entrypoint") (r "^0.3.0") (d #t) (k 0)) (d (n "solana-program") (r "^1.18.1") (d #t) (k 0)))) (h "0n14rlsi23zyqvssadimf7sl76mbg91j62kzj5kgh626isx6jb6a")))

