(define-module (crates-io so la solarti-discriminator) #:use-module (crates-io))

(define-public crate-solarti-discriminator-0.1.0 (c (n "solarti-discriminator") (v "0.1.0") (d (list (d (n "borsh") (r "^0.9.3") (o #t) (d #t) (k 0)) (d (n "bytemuck") (r "^1.14.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "miraland-program") (r "^1.14.17") (d #t) (k 0)) (d (n "solarti-discriminator-derive") (r "^0.1.0") (d #t) (k 0)))) (h "1cm9f68dgw1gb0s04v1vh4np7qffbas51rak0k9j7p77lc1m8qmp") (s 2) (e (quote (("borsh" "dep:borsh"))))))

(define-public crate-solarti-discriminator-0.1.2 (c (n "solarti-discriminator") (v "0.1.2") (d (list (d (n "borsh") (r "^0.10.3") (o #t) (d #t) (k 0)) (d (n "bytemuck") (r "^1.14.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "miraland-program") (r "=1.14.17") (d #t) (k 0)) (d (n "solarti-discriminator-derive") (r "^0.1.0") (d #t) (k 0)))) (h "0dqnr66wpbcfyjyjh1qyncviladbmrssybxf9xcvj8xwabnix3k5") (s 2) (e (quote (("borsh" "dep:borsh"))))))

(define-public crate-solarti-discriminator-0.1.3 (c (n "solarti-discriminator") (v "0.1.3") (d (list (d (n "borsh") (r "^0.10.3") (o #t) (d #t) (k 0)) (d (n "bytemuck") (r "^1.14.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "miraland-program") (r "=1.14.18") (d #t) (k 0)) (d (n "solarti-discriminator-derive") (r "^0.1.0") (d #t) (k 0)))) (h "10ba4d7f4wza140ypcl8m00cdpv6d1dij82vin22ajgzg77xmb0y") (s 2) (e (quote (("borsh" "dep:borsh"))))))

(define-public crate-solarti-discriminator-0.1.4 (c (n "solarti-discriminator") (v "0.1.4") (d (list (d (n "borsh") (r "^0.10.3") (o #t) (d #t) (k 0)) (d (n "bytemuck") (r "^1.14.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "miraland-program") (r "^1.14.18") (d #t) (k 0)) (d (n "solarti-discriminator-derive") (r "^0.1.0") (d #t) (k 0)))) (h "0g0jbw1jqp980is41jxpcrgvq932h2x1yiiln56isrsprh9gfya3") (s 2) (e (quote (("borsh" "dep:borsh"))))))

(define-public crate-solarti-discriminator-0.1.5 (c (n "solarti-discriminator") (v "0.1.5") (d (list (d (n "borsh") (r "^1.3.0") (o #t) (d #t) (k 0)) (d (n "bytemuck") (r "^1.14.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "miraland-program") (r "^1.18.2") (d #t) (k 0)) (d (n "solarti-discriminator-derive") (r "^0.1.5") (d #t) (k 0)))) (h "14iwijvlsysf8qn80mr799smxz4bgrflwpvgvvgr4rw5iqg548bd") (s 2) (e (quote (("borsh" "dep:borsh"))))))

(define-public crate-solarti-discriminator-0.1.6 (c (n "solarti-discriminator") (v "0.1.6") (d (list (d (n "borsh") (r "^1.3.0") (o #t) (d #t) (k 0)) (d (n "bytemuck") (r "^1.14.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "miraland-program") (r ">=1.18.2") (d #t) (k 0)) (d (n "solarti-discriminator-derive") (r "^0.1.6") (d #t) (k 0)))) (h "02084y9yczmrya0pplw0q6yhxk3lg6ibhgyfkldifsnqy16rlvka") (s 2) (e (quote (("borsh" "dep:borsh"))))))

