(define-module (crates-io so la solana-banks-interface) #:use-module (crates-io))

(define-public crate-solana-banks-interface-1.3.5 (c (n "solana-banks-interface") (v "1.3.5") (d (list (d (n "serde") (r "^1.0.112") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "^1.3.5") (d #t) (k 0)) (d (n "tarpc") (r "^0.21.0") (f (quote ("full"))) (d #t) (k 0)))) (h "001qh6c03xxn14a9f75a8ykxcp30qyvs5zhgfxiypir3l5zjfd2h")))

(define-public crate-solana-banks-interface-1.3.6 (c (n "solana-banks-interface") (v "1.3.6") (d (list (d (n "serde") (r "^1.0.112") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "^1.3.6") (d #t) (k 0)) (d (n "tarpc") (r "^0.21.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1hdwq7mdshfpjrpyzdfvfvypxp4h61kj32zf9p070y2n8fn7l57v")))

(define-public crate-solana-banks-interface-1.3.7 (c (n "solana-banks-interface") (v "1.3.7") (d (list (d (n "serde") (r "^1.0.112") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "^1.3.7") (d #t) (k 0)) (d (n "tarpc") (r "^0.21.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1wlxvnsn9gvaa5bjf9b8d51c50xnfaz7w4p0x9y3465r4k4yj6kw")))

(define-public crate-solana-banks-interface-1.3.8 (c (n "solana-banks-interface") (v "1.3.8") (d (list (d (n "serde") (r "^1.0.112") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "^1.3.8") (d #t) (k 0)) (d (n "tarpc") (r "^0.21.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1k5nqjzv2pq1wfflpm4aijlwqvqj2y3rvj76kqcf15jb41a9gaji")))

(define-public crate-solana-banks-interface-1.3.9 (c (n "solana-banks-interface") (v "1.3.9") (d (list (d (n "serde") (r "^1.0.112") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "^1.3.9") (d #t) (k 0)) (d (n "tarpc") (r "^0.21.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1ygn4pwjzms7aqnyxl2qf23i2b2vrmlcx6cqqasrmjrgac0kqkf6")))

(define-public crate-solana-banks-interface-1.3.10 (c (n "solana-banks-interface") (v "1.3.10") (d (list (d (n "serde") (r "^1.0.112") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "^1.3.10") (d #t) (k 0)) (d (n "tarpc") (r "^0.21.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0gzwxmpqq3haaja7w6vzii5sxzpd3xqr6h8j166dj0yq96gw8dza")))

(define-public crate-solana-banks-interface-1.3.11 (c (n "solana-banks-interface") (v "1.3.11") (d (list (d (n "serde") (r "^1.0.112") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "^1.3.11") (d #t) (k 0)) (d (n "tarpc") (r "^0.21.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0vl6q67827lay3i1ya77154pnvay7m6lj31jd64ky0fwdpnip080")))

(define-public crate-solana-banks-interface-1.3.12 (c (n "solana-banks-interface") (v "1.3.12") (d (list (d (n "serde") (r "^1.0.112") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "^1.3.12") (d #t) (k 0)) (d (n "tarpc") (r "^0.21.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1fqwpir644aw7igjwzl085rxx4zj28zicgcrs2db7k0dq4kdc8sd")))

(define-public crate-solana-banks-interface-1.3.13 (c (n "solana-banks-interface") (v "1.3.13") (d (list (d (n "serde") (r "^1.0.112") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "^1.3.13") (d #t) (k 0)) (d (n "tarpc") (r "^0.21.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0fyk7zksipjw3cb12z3kzkzmcxavk86ms4gfcvgn5zgpmvh16ww7")))

(define-public crate-solana-banks-interface-1.3.14 (c (n "solana-banks-interface") (v "1.3.14") (d (list (d (n "serde") (r "^1.0.112") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "^1.3.14") (d #t) (k 0)) (d (n "tarpc") (r "^0.21.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1y9nairwxvlygikvqhpl5m9gddc826r9pg2l0f9n8wm1yn7iq48c")))

(define-public crate-solana-banks-interface-1.3.17 (c (n "solana-banks-interface") (v "1.3.17") (d (list (d (n "serde") (r "^1.0.112") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "^1.3.17") (d #t) (k 0)) (d (n "tarpc") (r "^0.21.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0lki1vxj5ahxwjh7xwnvhp928jvk96m7qp2f4i67czk6j9qy18cd")))

(define-public crate-solana-banks-interface-1.4.0 (c (n "solana-banks-interface") (v "1.4.0") (d (list (d (n "serde") (r "^1.0.112") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "^1.4.0") (d #t) (k 0)) (d (n "tarpc") (r "^0.22.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1lrfnc9sirkxb3fawznfss6pm1574ynasql1rpnn76jdsb0c6gra")))

(define-public crate-solana-banks-interface-1.4.1 (c (n "solana-banks-interface") (v "1.4.1") (d (list (d (n "serde") (r "^1.0.112") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "^1.4.1") (d #t) (k 0)) (d (n "tarpc") (r "^0.22.0") (f (quote ("full"))) (d #t) (k 0)))) (h "19djjdvmzqrjmxmv9f88lb7y8qgdq58qyhsw4856drl4mr53xlbd")))

(define-public crate-solana-banks-interface-1.3.18 (c (n "solana-banks-interface") (v "1.3.18") (d (list (d (n "serde") (r "^1.0.112") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "^1.3.18") (d #t) (k 0)) (d (n "tarpc") (r "^0.21.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0xmc7f95zv0pw9mv9rcj87bnbk0lhkqwakd5fnbng5yrw20pdjxb")))

(define-public crate-solana-banks-interface-1.3.19 (c (n "solana-banks-interface") (v "1.3.19") (d (list (d (n "serde") (r "^1.0.112") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "^1.3.19") (d #t) (k 0)) (d (n "tarpc") (r "^0.21.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0862d39dp4p89xpbkrq1mc1vam2lc6ryk6hz5l5mrkjbqymnz8ay")))

(define-public crate-solana-banks-interface-1.4.3 (c (n "solana-banks-interface") (v "1.4.3") (d (list (d (n "serde") (r "^1.0.112") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "^1.4.3") (d #t) (k 0)) (d (n "tarpc") (r "^0.22.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1qifz9yn209l880cm7dnjkc06sp2hhyp4hplbvwkhpylhh3yla30")))

(define-public crate-solana-banks-interface-1.4.4 (c (n "solana-banks-interface") (v "1.4.4") (d (list (d (n "serde") (r "^1.0.112") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "^1.4.4") (d #t) (k 0)) (d (n "tarpc") (r "^0.23.0") (f (quote ("full"))) (d #t) (k 0)))) (h "17vkkib7pca830vd1mjir6fknadqgfwz0fcjnmp8baw8ypbll64y")))

(define-public crate-solana-banks-interface-1.4.5 (c (n "solana-banks-interface") (v "1.4.5") (d (list (d (n "serde") (r "^1.0.112") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "^1.4.5") (d #t) (k 0)) (d (n "tarpc") (r "^0.23.0") (f (quote ("full"))) (d #t) (k 0)))) (h "13n7bzmnkvg6b4lfsfpxdrmbgjxvnjwb4880lgvl2pmjx8x19b6f")))

(define-public crate-solana-banks-interface-1.4.6 (c (n "solana-banks-interface") (v "1.4.6") (d (list (d (n "serde") (r "^1.0.112") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "^1.4.6") (d #t) (k 0)) (d (n "tarpc") (r "^0.23.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0yghxk6l7sg9pv9hvm7rsii4lz0c0f216s646y6czpa5r9104ydg")))

(define-public crate-solana-banks-interface-1.3.20 (c (n "solana-banks-interface") (v "1.3.20") (d (list (d (n "serde") (r "^1.0.112") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "^1.3.20") (d #t) (k 0)) (d (n "tarpc") (r "^0.21.0") (f (quote ("full"))) (d #t) (k 0)))) (h "03y47v6rd21r94zhif0fvv2kqkimffdd72k2sdfp4p0ja257xdrl")))

(define-public crate-solana-banks-interface-1.4.7 (c (n "solana-banks-interface") (v "1.4.7") (d (list (d (n "serde") (r "^1.0.112") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "^1.4.7") (d #t) (k 0)) (d (n "tarpc") (r "^0.23.0") (f (quote ("full"))) (d #t) (k 0)))) (h "04g8rqw43y0k499m2jgh7dxs1mx865l1llqjbp33kfj1pv70ladp")))

(define-public crate-solana-banks-interface-1.3.21 (c (n "solana-banks-interface") (v "1.3.21") (d (list (d (n "serde") (r "^1.0.112") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "^1.3.21") (d #t) (k 0)) (d (n "tarpc") (r "^0.21.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1pr794lvbhd94wsq520l497scx56bfgcmjr7jnaannrr1q6b8zm4")))

(define-public crate-solana-banks-interface-1.4.8 (c (n "solana-banks-interface") (v "1.4.8") (d (list (d (n "serde") (r "^1.0.112") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "^1.4.8") (d #t) (k 0)) (d (n "tarpc") (r "^0.23.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0xgqj59slrs58sqz2gfj78ap31s0a9hb2d5m3f899aczqlzsbyyk")))

(define-public crate-solana-banks-interface-1.4.9 (c (n "solana-banks-interface") (v "1.4.9") (d (list (d (n "serde") (r "^1.0.112") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "^1.4.9") (d #t) (k 0)) (d (n "tarpc") (r "^0.23.0") (f (quote ("full"))) (d #t) (k 0)))) (h "09b72m69zgaqxayli9m1ghl8acvn2c8ypq864bp3i7y55q6xsnij")))

(define-public crate-solana-banks-interface-1.4.10 (c (n "solana-banks-interface") (v "1.4.10") (d (list (d (n "serde") (r ">=1.0.112, <2.0.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r ">=1.4.10, <2.0.0") (d #t) (k 0)) (d (n "tarpc") (r ">=0.23.0, <0.24.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0lhviydfrwsnc7h4n8v91by5i0s5k4x8fx6j720vckw17dc5kh8y")))

(define-public crate-solana-banks-interface-1.4.11 (c (n "solana-banks-interface") (v "1.4.11") (d (list (d (n "serde") (r ">=1.0.112, <2.0.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r ">=1.4.11, <2.0.0") (d #t) (k 0)) (d (n "tarpc") (r ">=0.23.0, <0.24.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1j5mvblf5hzirxr2f2cjciwzfh85d8nd29w9bvsb5mkciir0x21c")))

(define-public crate-solana-banks-interface-1.4.12 (c (n "solana-banks-interface") (v "1.4.12") (d (list (d (n "serde") (r ">=1.0.112, <2.0.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r ">=1.4.12, <2.0.0") (d #t) (k 0)) (d (n "tarpc") (r ">=0.23.0, <0.24.0") (f (quote ("full"))) (d #t) (k 0)))) (h "16xv7ji3za7kj7h3ggylylgd4hrg93jy1k19yrz4j6ni8fyp98lk")))

(define-public crate-solana-banks-interface-1.3.23 (c (n "solana-banks-interface") (v "1.3.23") (d (list (d (n "serde") (r "^1.0.112") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "^1.3.23") (d #t) (k 0)) (d (n "tarpc") (r "^0.21.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1pk1svhappxj9i61rgmjdakyak9lr6vkplqgh401sf7dkjn9b28c")))

(define-public crate-solana-banks-interface-1.4.13 (c (n "solana-banks-interface") (v "1.4.13") (d (list (d (n "serde") (r "^1.0.112") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "^1.4.13") (d #t) (k 0)) (d (n "tarpc") (r "^0.23.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1n7vd444fgkxz8yl430z3f8d3p0j50g7kny3af3ribsdbx30q1y5")))

(define-public crate-solana-banks-interface-1.4.14 (c (n "solana-banks-interface") (v "1.4.14") (d (list (d (n "serde") (r "^1.0.112") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "^1.4.14") (d #t) (k 0)) (d (n "tarpc") (r "^0.23.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0swrbax8a8fkkv1xqjmdpy8ik7mdk0xjkw9m2xcjj3p81xril1kk")))

(define-public crate-solana-banks-interface-1.4.15 (c (n "solana-banks-interface") (v "1.4.15") (d (list (d (n "serde") (r "^1.0.112") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "^1.4.15") (d #t) (k 0)) (d (n "tarpc") (r "^0.23.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1jhbix0gsn6zkvc45ggpcq4qphslzw75r90nkl3p2adg14dc36cd")))

(define-public crate-solana-banks-interface-1.4.16 (c (n "solana-banks-interface") (v "1.4.16") (d (list (d (n "serde") (r "^1.0.112") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "^1.4.16") (d #t) (k 0)) (d (n "tarpc") (r "^0.23.0") (f (quote ("full"))) (d #t) (k 0)))) (h "18kbxcgq6l8mx8daw3gl1pic9kfv9bkpdrp1c1hjmb3lxg0617a3")))

(define-public crate-solana-banks-interface-1.4.17 (c (n "solana-banks-interface") (v "1.4.17") (d (list (d (n "mio") (r "^0.7.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.112") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "^1.4.17") (d #t) (k 0)) (d (n "tarpc") (r "^0.23.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1svhd11lzrvnnj8h1v813dmhhpbvk7wgjh24m49v60shx8lllapi")))

(define-public crate-solana-banks-interface-1.5.0 (c (n "solana-banks-interface") (v "1.5.0") (d (list (d (n "mio") (r "^0.7.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.112") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "^1.5.0") (d #t) (k 0)) (d (n "tarpc") (r "^0.23.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^0.3") (f (quote ("full"))) (d #t) (k 2)))) (h "1kw2vppssss78y6r41axhkspgs71clzm76ish96hpbg965gwmk8y")))

(define-public crate-solana-banks-interface-1.4.18 (c (n "solana-banks-interface") (v "1.4.18") (d (list (d (n "mio") (r "^0.7.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.112") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "^1.4.18") (d #t) (k 0)) (d (n "tarpc") (r "^0.23.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1khp647l3fpmfigwkv9ppn4kfq906rhbj1gd1724v7il6564k4z2")))

(define-public crate-solana-banks-interface-1.4.19 (c (n "solana-banks-interface") (v "1.4.19") (d (list (d (n "mio") (r "^0.7.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.112") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "^1.4.19") (d #t) (k 0)) (d (n "tarpc") (r "^0.23.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1fhk0by6bdyqay37ng36iz4g240y10vxwiv20vdkfa7acwkbsahc")))

(define-public crate-solana-banks-interface-1.4.20 (c (n "solana-banks-interface") (v "1.4.20") (d (list (d (n "mio") (r "^0.7.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.112") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "^1.4.20") (d #t) (k 0)) (d (n "tarpc") (r "^0.23.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1mphnx22zma4p32jggygscibp3m6yz1jj9crck3s1w52gvn0xxfk")))

(define-public crate-solana-banks-interface-1.5.1 (c (n "solana-banks-interface") (v "1.5.1") (d (list (d (n "mio") (r "^0.7.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.112") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "^1.5.1") (d #t) (k 0)) (d (n "tarpc") (r "^0.23.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^0.3.5") (f (quote ("full"))) (d #t) (k 2)))) (h "09l2w2qkqq70zwh3gzg167q6ai7mp3cdys267wdfnr9cybzf0q1x")))

(define-public crate-solana-banks-interface-1.4.21 (c (n "solana-banks-interface") (v "1.4.21") (d (list (d (n "mio") (r "^0.7.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.112") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "^1.4.21") (d #t) (k 0)) (d (n "tarpc") (r "^0.23.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1jli9g8w4772akjij3f9v4hax2pc2bwsqr1d7yz5rl78xf8mn461")))

(define-public crate-solana-banks-interface-1.4.22 (c (n "solana-banks-interface") (v "1.4.22") (d (list (d (n "mio") (r "^0.7.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.112") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "^1.4.22") (d #t) (k 0)) (d (n "tarpc") (r "^0.23.0") (f (quote ("full"))) (d #t) (k 0)))) (h "04rly9jnlinkd27fc0vgrjbqz74ksn7x1x536fk52cwqcr95y95b")))

(define-public crate-solana-banks-interface-1.5.2 (c (n "solana-banks-interface") (v "1.5.2") (d (list (d (n "mio") (r "^0.7.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.112") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "^1.5.2") (d #t) (k 0)) (d (n "tarpc") (r "^0.23.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^0.3.5") (f (quote ("full"))) (d #t) (k 2)))) (h "1b7jmwfpd7mx2j23ymbw4pi6dkw3mdjsyxcnrw5ffgfw3fsgj58s")))

(define-public crate-solana-banks-interface-1.4.23 (c (n "solana-banks-interface") (v "1.4.23") (d (list (d (n "mio") (r "^0.7.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.112") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "^1.4.23") (d #t) (k 0)) (d (n "tarpc") (r "^0.23.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0d3g3k2ywkbbzhdjnbzb29xa4zwlbgav0fgsmi2f88ndwk8hr84h")))

(define-public crate-solana-banks-interface-1.5.3 (c (n "solana-banks-interface") (v "1.5.3") (d (list (d (n "mio") (r "^0.7.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.112") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "^1.5.3") (d #t) (k 0)) (d (n "tarpc") (r "^0.23.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^0.3.5") (f (quote ("full"))) (d #t) (k 2)))) (h "1vsxkzf1i94qn4ax6hi0a7xgvdvnqihw6xydqlx7f1h1lfs6zysc")))

(define-public crate-solana-banks-interface-1.5.4 (c (n "solana-banks-interface") (v "1.5.4") (d (list (d (n "mio") (r "^0.7.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.112") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "^1.5.4") (d #t) (k 0)) (d (n "tarpc") (r "^0.23.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^0.3.5") (f (quote ("full"))) (d #t) (k 2)))) (h "02bwa4iiqp8sl21d7gi5hd7sbyaqp99pn42lgpkc8a6d66ifg5xf")))

(define-public crate-solana-banks-interface-1.5.5 (c (n "solana-banks-interface") (v "1.5.5") (d (list (d (n "mio") (r "^0.7.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.112") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "^1.5.5") (d #t) (k 0)) (d (n "tarpc") (r "^0.23.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^0.3.5") (f (quote ("full"))) (d #t) (k 2)))) (h "0374h83fl0zm7chdjih3zz9h8f6nbpgdjmin5i294pbqvzq8z679")))

(define-public crate-solana-banks-interface-1.4.25 (c (n "solana-banks-interface") (v "1.4.25") (d (list (d (n "mio") (r "^0.7.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.112") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "^1.4.25") (d #t) (k 0)) (d (n "tarpc") (r "^0.23.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0rmcbvkqd7m26d13pmqrbcxf03bzji69445q6a1qmdkqvxrkwb3f")))

(define-public crate-solana-banks-interface-1.4.26 (c (n "solana-banks-interface") (v "1.4.26") (d (list (d (n "mio") (r "^0.7.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.112") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "^1.4.26") (d #t) (k 0)) (d (n "tarpc") (r "^0.23.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0x1zkjjklvh436kbwpma0kbnf5mrzj5sqp1ghfnaian89722s7kj")))

(define-public crate-solana-banks-interface-1.5.6 (c (n "solana-banks-interface") (v "1.5.6") (d (list (d (n "mio") (r "^0.7.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.112") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "^1.5.6") (d #t) (k 0)) (d (n "tarpc") (r "^0.23.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^0.3.5") (f (quote ("full"))) (d #t) (k 2)))) (h "19hki8v52fvp24z56rqiix5yii8zf384ryikwv4zbf6l3q709m43")))

(define-public crate-solana-banks-interface-1.4.27 (c (n "solana-banks-interface") (v "1.4.27") (d (list (d (n "mio") (r "^0.7.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.112") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "^1.4.27") (d #t) (k 0)) (d (n "tarpc") (r "^0.23.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0nc9l3h5jr7mnn5x4w1jiasjyaqhzdsqwa88fa3p8ipn9dbr1rpb")))

(define-public crate-solana-banks-interface-1.5.7 (c (n "solana-banks-interface") (v "1.5.7") (d (list (d (n "mio") (r "^0.7.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.112") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "^1.5.7") (d #t) (k 0)) (d (n "tarpc") (r "^0.23.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^0.3.5") (f (quote ("full"))) (d #t) (k 2)))) (h "18gsp2acdmlsh586a71p1fv35kr4jba3hy8zpp68522m0l29ljj8")))

(define-public crate-solana-banks-interface-1.5.8 (c (n "solana-banks-interface") (v "1.5.8") (d (list (d (n "mio") (r "^0.7.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.112") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "^1.5.8") (d #t) (k 0)) (d (n "tarpc") (r "^0.23.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^0.3.5") (f (quote ("full"))) (d #t) (k 2)))) (h "100n730m69gv8pyj89pjbh9pq5ag3ajy5jg0grsfg3642wfcrv3b")))

(define-public crate-solana-banks-interface-1.4.28 (c (n "solana-banks-interface") (v "1.4.28") (d (list (d (n "mio") (r "^0.7.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.112") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "^1.4.28") (d #t) (k 0)) (d (n "tarpc") (r "^0.23.0") (f (quote ("full"))) (d #t) (k 0)))) (h "051q45d2wwbkk2dxay816mpawkv3lsafknrqqqlv7f05zzwlw04l")))

(define-public crate-solana-banks-interface-1.5.9 (c (n "solana-banks-interface") (v "1.5.9") (d (list (d (n "mio") (r "^0.7.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "^1.5.9") (d #t) (k 0)) (d (n "tarpc") (r "^0.23.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^0.3.5") (f (quote ("full"))) (d #t) (k 2)))) (h "1sjgs93r123r388wnhrkbx9l611jhll2319rf2g2876j0nqvkggs")))

(define-public crate-solana-banks-interface-1.5.10 (c (n "solana-banks-interface") (v "1.5.10") (d (list (d (n "mio") (r "^0.7.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "^1.5.10") (d #t) (k 0)) (d (n "tarpc") (r "^0.23.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^0.3.5") (f (quote ("full"))) (d #t) (k 2)))) (h "11barcgm82vgq3s1akq9gh49knxajjwwf86rj12lfffkl4glxbi0")))

(define-public crate-solana-banks-interface-1.5.11 (c (n "solana-banks-interface") (v "1.5.11") (d (list (d (n "mio") (r "^0.7.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "^1.5.11") (d #t) (k 0)) (d (n "tarpc") (r "^0.23.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^0.3.5") (f (quote ("full"))) (d #t) (k 2)))) (h "04rpsjcfvkzgpb4ihfrmwdyvd8bx9rp4cryk7209cs6f9n5ywgzb")))

(define-public crate-solana-banks-interface-1.5.12 (c (n "solana-banks-interface") (v "1.5.12") (d (list (d (n "mio") (r "^0.7.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "^1.5.12") (d #t) (k 0)) (d (n "tarpc") (r "^0.23.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^0.3.5") (f (quote ("full"))) (d #t) (k 2)))) (h "0nz7qbh8dq8ik4kp3xjsfx1rfm2k10p5kz7c0x55jnl519l2yii0")))

(define-public crate-solana-banks-interface-1.5.13 (c (n "solana-banks-interface") (v "1.5.13") (d (list (d (n "mio") (r "^0.7.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "^1.5.13") (d #t) (k 0)) (d (n "tarpc") (r "^0.23.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^0.3.5") (f (quote ("full"))) (d #t) (k 2)))) (h "13yvaz8n55amy7s4calhyxgp7h91z8s234xiskwn4jg0gk4fdv5r")))

(define-public crate-solana-banks-interface-1.5.14 (c (n "solana-banks-interface") (v "1.5.14") (d (list (d (n "mio") (r "^0.7.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "^1.5.14") (d #t) (k 0)) (d (n "tarpc") (r "^0.23.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^0.3.5") (f (quote ("full"))) (d #t) (k 2)))) (h "0ffpaqxmycsxa44q45mcshb5ncjmiw02mmvhxrvl886j1jch9a39")))

(define-public crate-solana-banks-interface-1.6.0 (c (n "solana-banks-interface") (v "1.6.0") (d (list (d (n "mio") (r "^0.7.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.122") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "^1.6.0") (d #t) (k 0)) (d (n "tarpc") (r "^0.24.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1.1") (f (quote ("full"))) (d #t) (k 2)))) (h "00ai2nf0s69s8yc9qk73fa3d5c0jhl83v8fh06z7qc5jnldilmh8")))

(define-public crate-solana-banks-interface-1.5.15 (c (n "solana-banks-interface") (v "1.5.15") (d (list (d (n "mio") (r "^0.7.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.5.15") (d #t) (k 0)) (d (n "tarpc") (r "^0.23.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^0.3.5") (f (quote ("full"))) (d #t) (k 2)))) (h "08k15xcdvycx449wxr167p9l5cyskl49pm3fwh5lna8ik5nijv0h")))

(define-public crate-solana-banks-interface-1.6.1 (c (n "solana-banks-interface") (v "1.6.1") (d (list (d (n "mio") (r "^0.7.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.122") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.6.1") (d #t) (k 0)) (d (n "tarpc") (r "^0.24.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1.1") (f (quote ("full"))) (d #t) (k 2)))) (h "0l8ksy9xy33wpas9qjqm8g15apc7k0pxiixwxqn6nh5kxw4w84pm")))

(define-public crate-solana-banks-interface-1.5.16 (c (n "solana-banks-interface") (v "1.5.16") (d (list (d (n "mio") (r "^0.7.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.5.16") (d #t) (k 0)) (d (n "tarpc") (r "^0.23.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^0.3.5") (f (quote ("full"))) (d #t) (k 2)))) (h "0l2cr5p22j70m3awp4rgw9254m030mipgls4y5404064j6yxlpfh")))

(define-public crate-solana-banks-interface-1.5.17 (c (n "solana-banks-interface") (v "1.5.17") (d (list (d (n "mio") (r "^0.7.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.5.17") (d #t) (k 0)) (d (n "tarpc") (r "^0.23.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^0.3.5") (f (quote ("full"))) (d #t) (k 2)))) (h "12wk78v94xxsv33piwbny7mphwlrp1gzrplpbaficwls3k8j3bkd")))

(define-public crate-solana-banks-interface-1.6.2 (c (n "solana-banks-interface") (v "1.6.2") (d (list (d (n "mio") (r "^0.7.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.122") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.6.2") (d #t) (k 0)) (d (n "tarpc") (r "^0.24.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1.1") (f (quote ("full"))) (d #t) (k 2)))) (h "0raxvdd5yy9nkf7yf9fbkgbibn8h554lzm78nkbqj4pdgwxjqkwa")))

(define-public crate-solana-banks-interface-1.6.3 (c (n "solana-banks-interface") (v "1.6.3") (d (list (d (n "mio") (r "^0.7.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.122") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.6.3") (d #t) (k 0)) (d (n "tarpc") (r "^0.24.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1.1") (f (quote ("full"))) (d #t) (k 2)))) (h "1l16rwbd2vr18nx2jcxly47yvsqgxq7qk8igb1qd19miyk934wa6")))

(define-public crate-solana-banks-interface-1.6.4 (c (n "solana-banks-interface") (v "1.6.4") (d (list (d (n "mio") (r "^0.7.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.122") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.6.4") (d #t) (k 0)) (d (n "tarpc") (r "^0.24.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1.1") (f (quote ("full"))) (d #t) (k 2)))) (h "1g5dk8x5f352fpdk559k9l4k67pfabns77vf5jzl4025hjzn28xn")))

(define-public crate-solana-banks-interface-1.6.5 (c (n "solana-banks-interface") (v "1.6.5") (d (list (d (n "mio") (r "^0.7.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.122") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.6.5") (d #t) (k 0)) (d (n "tarpc") (r "^0.24.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "0am3qiqk3lg14hl05y4an7rc2dd6l5fd6qq0r5am8jjvh02nmzn2")))

(define-public crate-solana-banks-interface-1.6.6 (c (n "solana-banks-interface") (v "1.6.6") (d (list (d (n "mio") (r "^0.7.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.122") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.6.6") (d #t) (k 0)) (d (n "tarpc") (r "^0.24.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "0m8d44fvfnwsivxc4jl1p1wi8rhsijipdvixakd1j41lgpr6b81l")))

(define-public crate-solana-banks-interface-1.5.19 (c (n "solana-banks-interface") (v "1.5.19") (d (list (d (n "mio") (r "^0.7.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.5.19") (d #t) (k 0)) (d (n "tarpc") (r "^0.23.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^0.3.5") (f (quote ("full"))) (d #t) (k 2)))) (h "12cdriz1zwg6zjklpmp7shzc1ljv6av6gc68bij3ij9vj48z6zhf")))

(define-public crate-solana-banks-interface-1.6.7 (c (n "solana-banks-interface") (v "1.6.7") (d (list (d (n "mio") (r "^0.7.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.122") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.6.7") (d #t) (k 0)) (d (n "tarpc") (r "^0.24.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "0fq6bgkcz0cg3nag47zkzw90gqfkcwsxl5rikbwilr7rm3al9rbw")))

(define-public crate-solana-banks-interface-1.6.8 (c (n "solana-banks-interface") (v "1.6.8") (d (list (d (n "mio") (r "^0.7.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.122") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.6.8") (d #t) (k 0)) (d (n "tarpc") (r "^0.24.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "0smb3dyad64ssh7ww9srp5db7975dpckksk8dh0qfcbpizc92z7b")))

(define-public crate-solana-banks-interface-1.6.9 (c (n "solana-banks-interface") (v "1.6.9") (d (list (d (n "mio") (r "^0.7.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.122") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.6.9") (d #t) (k 0)) (d (n "tarpc") (r "^0.24.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "08p0ljixacwalnqj4lq493gh3jgdf5ljkcxipf18bncgx0crj0d4")))

(define-public crate-solana-banks-interface-1.6.10 (c (n "solana-banks-interface") (v "1.6.10") (d (list (d (n "mio") (r "^0.7.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.122") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.6.10") (d #t) (k 0)) (d (n "tarpc") (r "^0.24.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1p1hv5qsz6vg8j5vpfxsrpbmzvj17d4mrc8jmv1i3ixjlhg5nn43")))

(define-public crate-solana-banks-interface-1.6.11 (c (n "solana-banks-interface") (v "1.6.11") (d (list (d (n "mio") (r "^0.7.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.122") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.6.11") (d #t) (k 0)) (d (n "tarpc") (r "^0.24.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "0xgjvp5lk1455chsq0pyw1a951vnmb0sy9gybh4in52m4rybsrh0")))

(define-public crate-solana-banks-interface-1.7.0 (c (n "solana-banks-interface") (v "1.7.0") (d (list (d (n "mio") (r "^0.7.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.122") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.7.0") (d #t) (k 0)) (d (n "tarpc") (r "^0.24.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1acbq5fjinvig0pqrrb2m2gyvkdjy9llrlgda2qkxrb02ca7xr2x")))

(define-public crate-solana-banks-interface-1.7.1 (c (n "solana-banks-interface") (v "1.7.1") (d (list (d (n "mio") (r "^0.7.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.122") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.7.1") (d #t) (k 0)) (d (n "tarpc") (r "^0.24.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "073a7j89lf26agmrsqrb816zysqb0kmmdfxkdgbb6vl0qylymhzl")))

(define-public crate-solana-banks-interface-1.6.12 (c (n "solana-banks-interface") (v "1.6.12") (d (list (d (n "mio") (r "^0.7.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.122") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.6.12") (d #t) (k 0)) (d (n "tarpc") (r "^0.24.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "0q08c4plwj3ajxvx2gylx9kwdh029ln1xdaxkkblf51m65brxmld")))

(define-public crate-solana-banks-interface-1.6.13 (c (n "solana-banks-interface") (v "1.6.13") (d (list (d (n "mio") (r "^0.7.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.122") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.6.13") (d #t) (k 0)) (d (n "tarpc") (r "^0.24.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "14yycmjal5hbs67q2w8h09r1nbz0kqbmxmmxz8fm5asgj1kf4s9w")))

(define-public crate-solana-banks-interface-1.7.2 (c (n "solana-banks-interface") (v "1.7.2") (d (list (d (n "mio") (r "^0.7.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.122") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.7.2") (d #t) (k 0)) (d (n "tarpc") (r "^0.24.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "0wd1g3g1mrfgw3z45jw7z3c2dawrqjjglf2wjmm260xlv493n6kq")))

(define-public crate-solana-banks-interface-1.6.14 (c (n "solana-banks-interface") (v "1.6.14") (d (list (d (n "mio") (r "^0.7.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.122") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.6.14") (d #t) (k 0)) (d (n "tarpc") (r "^0.24.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "0p6k71s76wdj2zmq32vsswzjxaygw7hycxg6h7hg4xvrqjsk03c5")))

(define-public crate-solana-banks-interface-1.7.3 (c (n "solana-banks-interface") (v "1.7.3") (d (list (d (n "mio") (r "^0.7.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.122") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.7.3") (d #t) (k 0)) (d (n "tarpc") (r "^0.24.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1wjd55n8dbg1mlghxkw6x3y7nkxdm649k0l6n4x3gvryspj57cpk")))

(define-public crate-solana-banks-interface-1.6.15 (c (n "solana-banks-interface") (v "1.6.15") (d (list (d (n "mio") (r "^0.7.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.122") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.6.15") (d #t) (k 0)) (d (n "tarpc") (r "^0.24.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "15c1y97cfw36jaf9iqs5ky1lpgrnf5859a9s1zxmw6pwmlbsm6rz")))

(define-public crate-solana-banks-interface-1.7.4 (c (n "solana-banks-interface") (v "1.7.4") (d (list (d (n "mio") (r "^0.7.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.122") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.7.4") (d #t) (k 0)) (d (n "tarpc") (r "^0.24.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "0ilnavd9f9jhxgys8yw4mnbc4dkk6qhm8zs9bannvnqqgay5lzd8")))

(define-public crate-solana-banks-interface-1.6.16 (c (n "solana-banks-interface") (v "1.6.16") (d (list (d (n "mio") (r "^0.7.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.122") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.6.16") (d #t) (k 0)) (d (n "tarpc") (r "^0.24.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "030z5v1n50ync3292228az0syh6nn1jrnji3sw9zf8kvm0g9h25y")))

(define-public crate-solana-banks-interface-1.6.17 (c (n "solana-banks-interface") (v "1.6.17") (d (list (d (n "mio") (r "^0.7.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.122") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.6.17") (d #t) (k 0)) (d (n "tarpc") (r "^0.24.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "0n5zip43kjkkzwc5md8vbcpkcm839vf5xnq46krn17gwn4b8q5m0")))

(define-public crate-solana-banks-interface-1.7.5 (c (n "solana-banks-interface") (v "1.7.5") (d (list (d (n "mio") (r "^0.7.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.122") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.7.5") (d #t) (k 0)) (d (n "tarpc") (r "^0.24.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "17m6qrrca4javdhg2ka0b1yhryggl2w1mbi7zwr1nqz2ifxkp1hw")))

(define-public crate-solana-banks-interface-1.7.6 (c (n "solana-banks-interface") (v "1.7.6") (d (list (d (n "mio") (r "^0.7.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.122") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.7.6") (d #t) (k 0)) (d (n "tarpc") (r "^0.24.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "12qqcnjg9r7yslszrz7b2j7ws6bbi34g2923jg95vcsd58041fij")))

(define-public crate-solana-banks-interface-1.6.18 (c (n "solana-banks-interface") (v "1.6.18") (d (list (d (n "mio") (r "^0.7.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.122") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.6.18") (d #t) (k 0)) (d (n "tarpc") (r "^0.24.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "018p1r71dy7dfam57ir243xxbh97vayyhw5qi7dwfnml0iwv4yph")))

(define-public crate-solana-banks-interface-1.6.19 (c (n "solana-banks-interface") (v "1.6.19") (d (list (d (n "mio") (r "^0.7.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.122") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.6.19") (d #t) (k 0)) (d (n "tarpc") (r "^0.24.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "0macms205rir28c858397154r8hybyj2p9l5qwpwyd1qm7dapfsn")))

(define-public crate-solana-banks-interface-1.7.7 (c (n "solana-banks-interface") (v "1.7.7") (d (list (d (n "mio") (r "^0.7.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.122") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.7.7") (d #t) (k 0)) (d (n "tarpc") (r "^0.24.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "0d816cqblqffvag6wmxs55m6gx9hx0gy0kp4dqj2rjviqwvp1ma4")))

(define-public crate-solana-banks-interface-1.7.8 (c (n "solana-banks-interface") (v "1.7.8") (d (list (d (n "mio") (r "^0.7.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.122") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.7.8") (d #t) (k 0)) (d (n "tarpc") (r "^0.24.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "0s3205hp6n8g57mwgdwh7s8vjgaalfd14hpyx3yrsl6z8fi38fpp")))

(define-public crate-solana-banks-interface-1.6.20 (c (n "solana-banks-interface") (v "1.6.20") (d (list (d (n "mio") (r "^0.7.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.122") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.6.20") (d #t) (k 0)) (d (n "tarpc") (r "^0.24.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "0xyh8mchz2rh8zv4cf5fvwliyd561lklqqlx6h0399zr8nzvdh2c")))

(define-public crate-solana-banks-interface-1.7.9 (c (n "solana-banks-interface") (v "1.7.9") (d (list (d (n "mio") (r "^0.7.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.122") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.7.9") (d #t) (k 0)) (d (n "tarpc") (r "^0.24.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "0lypjjvn1xj70db2583m5cf23853a26blmgfav650i6llm2k6wsc")))

(define-public crate-solana-banks-interface-1.7.10 (c (n "solana-banks-interface") (v "1.7.10") (d (list (d (n "mio") (r "^0.7.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.122") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.7.10") (d #t) (k 0)) (d (n "tarpc") (r "^0.24.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "07h4dc9da69n31h0zbm9v5s963lrcvkidfpnswkpwz5r4w5mzivh")))

(define-public crate-solana-banks-interface-1.6.21 (c (n "solana-banks-interface") (v "1.6.21") (d (list (d (n "mio") (r "^0.7.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.122") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.6.21") (d #t) (k 0)) (d (n "tarpc") (r "^0.24.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1g752db0iqcz6v8wk2fb744l5s4f5ccz4dhk8niyip3pn1603a5g")))

(define-public crate-solana-banks-interface-1.7.11 (c (n "solana-banks-interface") (v "1.7.11") (d (list (d (n "mio") (r "^0.7.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.122") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.7.11") (d #t) (k 0)) (d (n "tarpc") (r "^0.24.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "12kqp7b4yhzgwpa76j000fnx7alfa28drjmrn8bw2dyzwqmp0apc")))

(define-public crate-solana-banks-interface-1.6.22 (c (n "solana-banks-interface") (v "1.6.22") (d (list (d (n "mio") (r "^0.7.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.122") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.6.22") (d #t) (k 0)) (d (n "tarpc") (r "^0.24.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1kq7m08zi199v4bj9575lhk9ahm03k785gg1ih7bw6l4lbm38jz5")))

(define-public crate-solana-banks-interface-1.6.24 (c (n "solana-banks-interface") (v "1.6.24") (d (list (d (n "mio") (r "^0.7.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.122") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.6.24") (d #t) (k 0)) (d (n "tarpc") (r "^0.24.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "09jlv35w1briq3hhrccqfk9i3dxg1aya7sim77p07q0j5bc02ssa")))

(define-public crate-solana-banks-interface-1.6.25 (c (n "solana-banks-interface") (v "1.6.25") (d (list (d (n "mio") (r "^0.7.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.122") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.6.25") (d #t) (k 0)) (d (n "tarpc") (r "^0.24.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "17wqvghpbpbm78vhcdyzipnp4y2jjxjfl6am47y4xk60a5a8s83w")))

(define-public crate-solana-banks-interface-1.7.12 (c (n "solana-banks-interface") (v "1.7.12") (d (list (d (n "mio") (r "^0.7.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.122") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.7.12") (d #t) (k 0)) (d (n "tarpc") (r "^0.24.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "0shgswsib8xa8cqncc6h1mqgr3rd3sz1c8dfh797yc26yd67wbcz")))

(define-public crate-solana-banks-interface-1.6.26 (c (n "solana-banks-interface") (v "1.6.26") (d (list (d (n "mio") (r "^0.7.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.122") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.6.26") (d #t) (k 0)) (d (n "tarpc") (r "^0.24.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1sjysv5vv1bz0qh75nlkkwl55pd6jv4ia3nvzr4iihq5fc5saxmy")))

(define-public crate-solana-banks-interface-1.6.27 (c (n "solana-banks-interface") (v "1.6.27") (d (list (d (n "mio") (r "^0.7.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.122") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.6.27") (d #t) (k 0)) (d (n "tarpc") (r "^0.24.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1sziivpr6lx127knrkgqq6asqjjhk05jpvq9d91zywyjc5mhn3w5")))

(define-public crate-solana-banks-interface-1.7.13 (c (n "solana-banks-interface") (v "1.7.13") (d (list (d (n "mio") (r "^0.7.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.122") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.7.13") (d #t) (k 0)) (d (n "tarpc") (r "^0.24.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1fbfhxr91g62qyd83a3xnxwx8g202k83idp5i09wjc87c5443ngq")))

(define-public crate-solana-banks-interface-1.7.14 (c (n "solana-banks-interface") (v "1.7.14") (d (list (d (n "mio") (r "^0.7.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.122") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.7.14") (d #t) (k 0)) (d (n "tarpc") (r "^0.24.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "0qgy3fabfqvfpjj7371afqf998p96hmg67533m9l8gb755vcajf4")))

(define-public crate-solana-banks-interface-1.8.0 (c (n "solana-banks-interface") (v "1.8.0") (d (list (d (n "mio") (r "^0.7.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.122") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.8.0") (d #t) (k 0)) (d (n "tarpc") (r "^0.24.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "09q48xf3bl988kx12ls75x2razafvfpr8r8qnvikq10f597n78wc")))

(define-public crate-solana-banks-interface-1.6.28 (c (n "solana-banks-interface") (v "1.6.28") (d (list (d (n "mio") (r "^0.7.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.122") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.6.28") (d #t) (k 0)) (d (n "tarpc") (r "^0.24.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1dak7170ynr1jhnm1z3ifpxgf3qd0cdvf5wci7vh9aydzqdc3ac3")))

(define-public crate-solana-banks-interface-1.7.15 (c (n "solana-banks-interface") (v "1.7.15") (d (list (d (n "mio") (r "^0.7.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.122") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.7.15") (d #t) (k 0)) (d (n "tarpc") (r "^0.24.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1glvzrkx3r3g66bs3cpvhyg0l8rdz4zdh8ppd8ivlvv234rlwfa1")))

(define-public crate-solana-banks-interface-1.8.1 (c (n "solana-banks-interface") (v "1.8.1") (d (list (d (n "mio") (r "^0.7.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.122") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.8.1") (d #t) (k 0)) (d (n "tarpc") (r "^0.24.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1bryn3qpz9wpgbpfyllxflspd6jjsgvib21fzlb1gr26ih1ynhr4")))

(define-public crate-solana-banks-interface-1.7.16 (c (n "solana-banks-interface") (v "1.7.16") (d (list (d (n "mio") (r "^0.7.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.122") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.7.16") (d #t) (k 0)) (d (n "tarpc") (r "^0.24.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "15fffz4jcfvn0j6nfw5jrsdan6v3x67sfwc04wwwqkmx4rc0zyvw")))

(define-public crate-solana-banks-interface-1.7.17 (c (n "solana-banks-interface") (v "1.7.17") (d (list (d (n "mio") (r "^0.7.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.122") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.7.17") (d #t) (k 0)) (d (n "tarpc") (r "^0.24.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "0zffzs06nkc1baz2dl5abrmwp1xah1cqadlkhb2r7mc1cbp65vxa")))

(define-public crate-solana-banks-interface-1.8.2 (c (n "solana-banks-interface") (v "1.8.2") (d (list (d (n "mio") (r "^0.7.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.122") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.8.2") (d #t) (k 0)) (d (n "tarpc") (r "^0.24.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "11jmdnh979qxib3xww6lwz7b18a32cg1533fby1hgvrxfxpjq1km")))

(define-public crate-solana-banks-interface-1.8.3 (c (n "solana-banks-interface") (v "1.8.3") (d (list (d (n "mio") (r "^0.7.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.122") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.8.3") (d #t) (k 0)) (d (n "tarpc") (r "^0.24.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "132fb4ybk1zlq245rw7hf8zipr57jry2zgzpjhdbswk224jqsj3a")))

(define-public crate-solana-banks-interface-1.8.4 (c (n "solana-banks-interface") (v "1.8.4") (d (list (d (n "mio") (r "^0.7.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.122") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.8.4") (d #t) (k 0)) (d (n "tarpc") (r "^0.24.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1g2j9balwrbmm9x06gxwg4idb3riap6iaggffrsviiwchc6khw19")))

(define-public crate-solana-banks-interface-1.8.5 (c (n "solana-banks-interface") (v "1.8.5") (d (list (d (n "mio") (r "^0.7.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.122") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.8.5") (d #t) (k 0)) (d (n "tarpc") (r "^0.24.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1x7rbnngnk3v3aas7q6pfgh4mf8ybqych524fv1igbsp0c4x87m8")))

(define-public crate-solana-banks-interface-1.8.6 (c (n "solana-banks-interface") (v "1.8.6") (d (list (d (n "mio") (r "^0.7.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.122") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.8.6") (d #t) (k 0)) (d (n "tarpc") (r "^0.24.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "160q1vx92zzkylik58qw21sp9qxwri6m852z0s6pc9qs0sr9n6ac")))

(define-public crate-solana-banks-interface-1.8.7 (c (n "solana-banks-interface") (v "1.8.7") (d (list (d (n "mio") (r "^0.7.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.122") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.8.7") (d #t) (k 0)) (d (n "tarpc") (r "^0.24.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "14ph7znlh5frps0y88asbcz9mm9qd4qlbg2x9yw4zlaa14r4106p")))

(define-public crate-solana-banks-interface-1.8.8 (c (n "solana-banks-interface") (v "1.8.8") (d (list (d (n "mio") (r "^0.7.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.122") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.8.8") (d #t) (k 0)) (d (n "tarpc") (r "^0.24.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "0wnwdhvn05bncxlrzr42lbqsvc8g3kisl62aki4cxflr0hm7b8yi")))

(define-public crate-solana-banks-interface-1.8.9 (c (n "solana-banks-interface") (v "1.8.9") (d (list (d (n "mio") (r "^0.7.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.122") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.8.9") (d #t) (k 0)) (d (n "tarpc") (r "^0.24.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1xnp0vhlwdz983mg5nlgrb43gdh6s6rim3v3fsdfvr2cfkmh5c8s")))

(define-public crate-solana-banks-interface-1.9.0 (c (n "solana-banks-interface") (v "1.9.0") (d (list (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.9.0") (d #t) (k 0)) (d (n "tarpc") (r "^0.26.2") (f (quote ("full"))) (d #t) (k 0)))) (h "008qzpg5cvqm82kzywaxrq1v09yh7dspx41n7y1ab108bv1zfr0a")))

(define-public crate-solana-banks-interface-1.8.10 (c (n "solana-banks-interface") (v "1.8.10") (d (list (d (n "mio") (r "^0.7.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.122") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.8.10") (d #t) (k 0)) (d (n "tarpc") (r "^0.24.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "17w3l7aw60bzx8i3dln5v7lxm702zrjy4mylbp2kzz358jsza3xn")))

(define-public crate-solana-banks-interface-1.8.11 (c (n "solana-banks-interface") (v "1.8.11") (d (list (d (n "mio") (r "^0.7.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.122") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.8.11") (d #t) (k 0)) (d (n "tarpc") (r "^0.24.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1fwfnpn46hzmxb1jps6zfy809hxck2k5gigp50cs613lwxgh54hc")))

(define-public crate-solana-banks-interface-1.9.1 (c (n "solana-banks-interface") (v "1.9.1") (d (list (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.9.1") (d #t) (k 0)) (d (n "tarpc") (r "^0.26.2") (f (quote ("full"))) (d #t) (k 0)))) (h "0zv6s62xnwb2b3y85r0s2la5isvi1g805vwcm0ajarl7z5a4ygch")))

(define-public crate-solana-banks-interface-1.9.2 (c (n "solana-banks-interface") (v "1.9.2") (d (list (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.9.2") (d #t) (k 0)) (d (n "tarpc") (r "^0.27.2") (f (quote ("full"))) (d #t) (k 0)))) (h "1ln5b0l5w071lczkhlzpcrfis0j0axjb42yl4i2fy807pdgg87zl")))

(define-public crate-solana-banks-interface-1.9.3 (c (n "solana-banks-interface") (v "1.9.3") (d (list (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.9.3") (d #t) (k 0)) (d (n "tarpc") (r "^0.27.2") (f (quote ("full"))) (d #t) (k 0)))) (h "1hlwfn074is76mlf79gx6yyifi0c8f0a444g4z2kbbqf7lnhnkbc")))

(define-public crate-solana-banks-interface-1.8.12 (c (n "solana-banks-interface") (v "1.8.12") (d (list (d (n "mio") (r "^0.7.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.122") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.8.12") (d #t) (k 0)) (d (n "tarpc") (r "^0.24.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "0xq44mymhnqjqk2dyrfisj2ddg62hihc76jdl00qkq5fxs9hksx5")))

(define-public crate-solana-banks-interface-1.9.4 (c (n "solana-banks-interface") (v "1.9.4") (d (list (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.9.4") (d #t) (k 0)) (d (n "tarpc") (r "^0.27.2") (f (quote ("full"))) (d #t) (k 0)))) (h "1l4fmrc7222jj70q6bhpcgllip6iaqrcynyp7z99q21bpcrr4p8n")))

(define-public crate-solana-banks-interface-1.8.13 (c (n "solana-banks-interface") (v "1.8.13") (d (list (d (n "mio") (r "^0.7.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.122") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.8.13") (d #t) (k 0)) (d (n "tarpc") (r "^0.24.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "001zwybdmlji1qdvg8dyiic5s6ipxap3cqvzcbvh3xry3d4z9cwh")))

(define-public crate-solana-banks-interface-1.9.5 (c (n "solana-banks-interface") (v "1.9.5") (d (list (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.9.5") (d #t) (k 0)) (d (n "tarpc") (r "^0.27.2") (f (quote ("full"))) (d #t) (k 0)))) (h "1311ivqq1b353ggpzk05ihwxcrnzd8fxaxlccyc0c2qg93njzzvz")))

(define-public crate-solana-banks-interface-1.8.14 (c (n "solana-banks-interface") (v "1.8.14") (d (list (d (n "mio") (r "^0.7.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.122") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.8.14") (d #t) (k 0)) (d (n "tarpc") (r "^0.24.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1a55lfnxfyq1arbrn6w1nhwpigf73ab3j61bz68iv5dak1bzj7xd")))

(define-public crate-solana-banks-interface-1.9.6 (c (n "solana-banks-interface") (v "1.9.6") (d (list (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.9.6") (d #t) (k 0)) (d (n "tarpc") (r "^0.27.2") (f (quote ("full"))) (d #t) (k 0)))) (h "1nbfng3yrh8qrsnwdy1i91yq08vdz6j6b3rc0q72azmshqpl61mj")))

(define-public crate-solana-banks-interface-1.9.7 (c (n "solana-banks-interface") (v "1.9.7") (d (list (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.9.7") (d #t) (k 0)) (d (n "tarpc") (r "^0.27.2") (f (quote ("full"))) (d #t) (k 0)))) (h "1iqmr1cdam7vbjam0f3sdsrb0zyf81mw82kw20n3qmcsjg6fzyk4")))

(define-public crate-solana-banks-interface-1.8.16 (c (n "solana-banks-interface") (v "1.8.16") (d (list (d (n "mio") (r "^0.7.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.122") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.8.16") (d #t) (k 0)) (d (n "tarpc") (r "^0.24.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "0zr64v0aq1hrafdxmb1rwsim8r0svybvxybs9ibx7wvxb7d9s0la")))

(define-public crate-solana-banks-interface-1.9.8 (c (n "solana-banks-interface") (v "1.9.8") (d (list (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.9.8") (d #t) (k 0)) (d (n "tarpc") (r "^0.27.2") (f (quote ("full"))) (d #t) (k 0)))) (h "00aad8ksrcjddy7s2w5sqm014ia9v6b02bbgdwnyb4lgxzzjcn5g")))

(define-public crate-solana-banks-interface-1.9.9 (c (n "solana-banks-interface") (v "1.9.9") (d (list (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.9.9") (d #t) (k 0)) (d (n "tarpc") (r "^0.27.2") (f (quote ("full"))) (d #t) (k 0)))) (h "0r68hyhw2xcqxcw8h3lirnb1x1xvpvnlgdb0n9idxaifiyw9si4i")))

(define-public crate-solana-banks-interface-1.10.0 (c (n "solana-banks-interface") (v "1.10.0") (d (list (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.10.0") (d #t) (k 0)) (d (n "tarpc") (r "^0.27.2") (f (quote ("full"))) (d #t) (k 0)))) (h "1z4gbaa8kn57bhdff5q26xdnfg2gf2h8g8wm1g35rb5q25q172yz")))

(define-public crate-solana-banks-interface-1.9.10 (c (n "solana-banks-interface") (v "1.9.10") (d (list (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.9.10") (d #t) (k 0)) (d (n "tarpc") (r "^0.27.2") (f (quote ("full"))) (d #t) (k 0)))) (h "0z6bzdp1j2iwyylirhb03rcqckryn01s3dwbzwx4kbj0cm5pvpc8")))

(define-public crate-solana-banks-interface-1.9.11 (c (n "solana-banks-interface") (v "1.9.11") (d (list (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.9.11") (d #t) (k 0)) (d (n "tarpc") (r "^0.27.2") (f (quote ("full"))) (d #t) (k 0)))) (h "17z47q6b536vjc0jjbd4r8p863vj81fkak432ss3k1fk04h9kim7")))

(define-public crate-solana-banks-interface-1.9.12 (c (n "solana-banks-interface") (v "1.9.12") (d (list (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.9.12") (d #t) (k 0)) (d (n "tarpc") (r "^0.27.2") (f (quote ("full"))) (d #t) (k 0)))) (h "03dp46skqpkf1isc2nbz8jl2w0racgiy8yn74v1xfhqh9rhyi84b")))

(define-public crate-solana-banks-interface-1.10.1 (c (n "solana-banks-interface") (v "1.10.1") (d (list (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.10.1") (d #t) (k 0)) (d (n "tarpc") (r "^0.27.2") (f (quote ("full"))) (d #t) (k 0)))) (h "1zmbvw5wva5kz9cj8hs1qsbdpyr0xmgnhbp8fsx0i8hvrcf9b74z")))

(define-public crate-solana-banks-interface-1.10.2 (c (n "solana-banks-interface") (v "1.10.2") (d (list (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.10.2") (d #t) (k 0)) (d (n "tarpc") (r "^0.27.2") (f (quote ("full"))) (d #t) (k 0)))) (h "0qbgvspp12y0z1wlfmdlhbzfhz4rgdsayx39djbchgw4x267bvkv")))

(define-public crate-solana-banks-interface-1.9.13 (c (n "solana-banks-interface") (v "1.9.13") (d (list (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.9.13") (d #t) (k 0)) (d (n "tarpc") (r "^0.27.2") (f (quote ("full"))) (d #t) (k 0)))) (h "0997wlkqr31wdhs2kbpj8rypwvh7sizh0jmkr1f996zaawng7lkg")))

(define-public crate-solana-banks-interface-1.10.3 (c (n "solana-banks-interface") (v "1.10.3") (d (list (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.10.3") (d #t) (k 0)) (d (n "tarpc") (r "^0.27.2") (f (quote ("full"))) (d #t) (k 0)))) (h "1c4i23lsc3nk6b8i896l2801bs9bzq1xn4gaabvqf12zais9z3cw")))

(define-public crate-solana-banks-interface-1.9.14 (c (n "solana-banks-interface") (v "1.9.14") (d (list (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.9.14") (d #t) (k 0)) (d (n "tarpc") (r "^0.27.2") (f (quote ("full"))) (d #t) (k 0)))) (h "0rp30inbkbdc72hmkb6zjdlbn6s3nb9ds0x71q61gskm8i9bn422")))

(define-public crate-solana-banks-interface-1.10.4 (c (n "solana-banks-interface") (v "1.10.4") (d (list (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.10.4") (d #t) (k 0)) (d (n "tarpc") (r "^0.27.2") (f (quote ("full"))) (d #t) (k 0)))) (h "1l71yhik1xq6l0q93b6yym9phb164cwygj36zcqxx4x678g25sda")))

(define-public crate-solana-banks-interface-1.10.5 (c (n "solana-banks-interface") (v "1.10.5") (d (list (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.10.5") (d #t) (k 0)) (d (n "tarpc") (r "^0.27.2") (f (quote ("full"))) (d #t) (k 0)))) (h "0pwdwcryrccnnf752nzdcbv41yvjk5yd5dy8444yz0hdz9r4cj62")))

(define-public crate-solana-banks-interface-1.10.6 (c (n "solana-banks-interface") (v "1.10.6") (d (list (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.10.6") (d #t) (k 0)) (d (n "tarpc") (r "^0.27.2") (f (quote ("full"))) (d #t) (k 0)))) (h "05hd2v3bghlz5bsnvpd54sqq7fj627fx7k56kancha9vzpvg21wq")))

(define-public crate-solana-banks-interface-1.9.15 (c (n "solana-banks-interface") (v "1.9.15") (d (list (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.9.15") (d #t) (k 0)) (d (n "tarpc") (r "^0.27.2") (f (quote ("full"))) (d #t) (k 0)))) (h "1qh1l7zzdkpd7lnakhj9wa5q6wxsyk6l7ssv03g6g6dbp2hv4wps")))

(define-public crate-solana-banks-interface-1.10.7 (c (n "solana-banks-interface") (v "1.10.7") (d (list (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.10.7") (d #t) (k 0)) (d (n "tarpc") (r "^0.27.2") (f (quote ("full"))) (d #t) (k 0)))) (h "0x78hfmazs9x7v6wrs62c9vykm6ry8076qgsizy8i98a623zkik1")))

(define-public crate-solana-banks-interface-1.10.8 (c (n "solana-banks-interface") (v "1.10.8") (d (list (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.10.8") (d #t) (k 0)) (d (n "tarpc") (r "^0.27.2") (f (quote ("full"))) (d #t) (k 0)))) (h "1ixh3pzlbm6izjq7n8n7m07xc7nlr5sscr42w1vv4kgkrhfllcws")))

(define-public crate-solana-banks-interface-1.9.16 (c (n "solana-banks-interface") (v "1.9.16") (d (list (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.9.16") (d #t) (k 0)) (d (n "tarpc") (r "^0.27.2") (f (quote ("full"))) (d #t) (k 0)))) (h "1qzysz450gadz2j5s6kphbjay9366ya1126czyk33m73rvvx2aw1")))

(define-public crate-solana-banks-interface-1.9.17 (c (n "solana-banks-interface") (v "1.9.17") (d (list (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.9.17") (d #t) (k 0)) (d (n "tarpc") (r "^0.27.2") (f (quote ("full"))) (d #t) (k 0)))) (h "1aq0vvqlcs6d92jhk90w1k45l1lrqcss509yl9rqffriyc5ymdwn")))

(define-public crate-solana-banks-interface-1.10.9 (c (n "solana-banks-interface") (v "1.10.9") (d (list (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.10.9") (d #t) (k 0)) (d (n "tarpc") (r "^0.27.2") (f (quote ("full"))) (d #t) (k 0)))) (h "0wp9i8fa00qj9d6966pqa6sff6rgg5lpndqy5x3jxrpi3kqpq1f0")))

(define-public crate-solana-banks-interface-1.9.18 (c (n "solana-banks-interface") (v "1.9.18") (d (list (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.9.18") (d #t) (k 0)) (d (n "tarpc") (r "^0.27.2") (f (quote ("full"))) (d #t) (k 0)))) (h "14n8c5dywdny2xphlqx9rqk33zflak2aharsiahlw3kz8n7yxs17")))

(define-public crate-solana-banks-interface-1.10.10 (c (n "solana-banks-interface") (v "1.10.10") (d (list (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.10.10") (d #t) (k 0)) (d (n "tarpc") (r "^0.27.2") (f (quote ("full"))) (d #t) (k 0)))) (h "1w598hwc52jzjy5sn2m65qqyazqhxfpksrji8mx3l8kbpslgkvm4")))

(define-public crate-solana-banks-interface-1.10.11 (c (n "solana-banks-interface") (v "1.10.11") (d (list (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.10.11") (d #t) (k 0)) (d (n "tarpc") (r "^0.27.2") (f (quote ("full"))) (d #t) (k 0)))) (h "09z53w2cq7zij91qcs2wis1qf8nmbf94yr1mzki8rzfwkcnfv370")))

(define-public crate-solana-banks-interface-1.9.19 (c (n "solana-banks-interface") (v "1.9.19") (d (list (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.9.19") (d #t) (k 0)) (d (n "tarpc") (r "^0.27.2") (f (quote ("full"))) (d #t) (k 0)))) (h "0w8qp3ngqlhrcyz9xrb2q3yxk0r61bd401bvffs0xd5hzdxhw7r4")))

(define-public crate-solana-banks-interface-1.10.12 (c (n "solana-banks-interface") (v "1.10.12") (d (list (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.10.12") (d #t) (k 0)) (d (n "tarpc") (r "^0.27.2") (f (quote ("full"))) (d #t) (k 0)))) (h "03zqs9zpl1n1lyvk2sjyw546glnk4gdp1q42n2128lglrpgs9v7f")))

(define-public crate-solana-banks-interface-1.9.20 (c (n "solana-banks-interface") (v "1.9.20") (d (list (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.9.20") (d #t) (k 0)) (d (n "tarpc") (r "^0.27.2") (f (quote ("full"))) (d #t) (k 0)))) (h "1mpzg13m869xl4f7ib3b1lmpxilhq6q43zxpmv9s7cv27a22pqc0")))

(define-public crate-solana-banks-interface-1.10.13 (c (n "solana-banks-interface") (v "1.10.13") (d (list (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.10.13") (d #t) (k 0)) (d (n "tarpc") (r "^0.27.2") (f (quote ("full"))) (d #t) (k 0)))) (h "0gfxh8qkd6b61whjwjsg03ivkqf4v363l7x8hzxz9q8d1aygpk2s")))

(define-public crate-solana-banks-interface-1.9.21 (c (n "solana-banks-interface") (v "1.9.21") (d (list (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.9.21") (d #t) (k 0)) (d (n "tarpc") (r "^0.27.2") (f (quote ("full"))) (d #t) (k 0)))) (h "08vikj6n6dsywqhfvd1gkjmkl5wshbpn9iqk49yayw96lvrnrymc")))

(define-public crate-solana-banks-interface-1.10.14 (c (n "solana-banks-interface") (v "1.10.14") (d (list (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.10.14") (d #t) (k 0)) (d (n "tarpc") (r "^0.27.2") (f (quote ("full"))) (d #t) (k 0)))) (h "1q5ki10bvhgl4jj4h96q03dh0ngpq6ax9xcldr3v14igck889vaq")))

(define-public crate-solana-banks-interface-1.9.22 (c (n "solana-banks-interface") (v "1.9.22") (d (list (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.9.22") (d #t) (k 0)) (d (n "tarpc") (r "^0.27.2") (f (quote ("full"))) (d #t) (k 0)))) (h "0prv9glr5xym3yd3p8ph3ib4ibwfw37q07cfg60n6slwcq0f69nv")))

(define-public crate-solana-banks-interface-1.10.15 (c (n "solana-banks-interface") (v "1.10.15") (d (list (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.10.15") (d #t) (k 0)) (d (n "tarpc") (r "^0.27.2") (f (quote ("full"))) (d #t) (k 0)))) (h "15545mll1zdpsg3bqi1lb947bmspasgsak3r9xa96v1vyiihnwxn")))

(define-public crate-solana-banks-interface-1.10.16 (c (n "solana-banks-interface") (v "1.10.16") (d (list (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.10.16") (d #t) (k 0)) (d (n "tarpc") (r "^0.27.2") (f (quote ("full"))) (d #t) (k 0)))) (h "17ql9z3prmd26hqlzx4ga5svgw32nkyivsn1xqr6z19cls0626rm")))

(define-public crate-solana-banks-interface-1.10.17 (c (n "solana-banks-interface") (v "1.10.17") (d (list (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.10.17") (d #t) (k 0)) (d (n "tarpc") (r "^0.27.2") (f (quote ("full"))) (d #t) (k 0)))) (h "1wdsvhci3k70b4xnbfd9zsckgqx1ww00gg4aljkhm9kklvl9xy18")))

(define-public crate-solana-banks-interface-1.10.18 (c (n "solana-banks-interface") (v "1.10.18") (d (list (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.10.18") (d #t) (k 0)) (d (n "tarpc") (r "^0.27.2") (f (quote ("full"))) (d #t) (k 0)))) (h "05aq8br8kq9drfpb0cxd6i588gybsbjdwx42kqd29vh3v7bn6aa6")))

(define-public crate-solana-banks-interface-1.9.23 (c (n "solana-banks-interface") (v "1.9.23") (d (list (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.9.23") (d #t) (k 0)) (d (n "tarpc") (r "^0.27.2") (f (quote ("full"))) (d #t) (k 0)))) (h "16pgrffbln2sg3c9xx0nnhxps06fn96dfic3cjm2vq81ys69xszz")))

(define-public crate-solana-banks-interface-1.10.19 (c (n "solana-banks-interface") (v "1.10.19") (d (list (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.10.19") (d #t) (k 0)) (d (n "tarpc") (r "^0.27.2") (f (quote ("full"))) (d #t) (k 0)))) (h "14yna332afk9202frkxlxcwipqw1hcp6lrlyvv4achb23bzsq4yq")))

(define-public crate-solana-banks-interface-1.9.24 (c (n "solana-banks-interface") (v "1.9.24") (d (list (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.9.24") (d #t) (k 0)) (d (n "tarpc") (r "^0.27.2") (f (quote ("full"))) (d #t) (k 0)))) (h "11z5b9rkkj1y3a7na0r9k7dl3qgbjphlnjaxr25n38cr49jmqma7")))

(define-public crate-solana-banks-interface-1.9.25 (c (n "solana-banks-interface") (v "1.9.25") (d (list (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.9.25") (d #t) (k 0)) (d (n "tarpc") (r "^0.27.2") (f (quote ("full"))) (d #t) (k 0)))) (h "0gy1n1axscpq4jrq0xvh0mj6b06a9zgn9f8q8vp38gxqjhrwafq1")))

(define-public crate-solana-banks-interface-1.10.20 (c (n "solana-banks-interface") (v "1.10.20") (d (list (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.10.20") (d #t) (k 0)) (d (n "tarpc") (r "^0.27.2") (f (quote ("full"))) (d #t) (k 0)))) (h "1l3w3fwr3l92h0jf1hk2d2wnfcwdb7b9w49swnl06gxlfq9wv09s")))

(define-public crate-solana-banks-interface-1.9.26 (c (n "solana-banks-interface") (v "1.9.26") (d (list (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.9.26") (d #t) (k 0)) (d (n "tarpc") (r "^0.27.2") (f (quote ("full"))) (d #t) (k 0)))) (h "10i2i20gl9hs6nz71bjgijxib3y8asgv362074zfk6yn1h22pbp9")))

(define-public crate-solana-banks-interface-1.10.21 (c (n "solana-banks-interface") (v "1.10.21") (d (list (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.10.21") (d #t) (k 0)) (d (n "tarpc") (r "^0.27.2") (f (quote ("full"))) (d #t) (k 0)))) (h "010d937pzl4y8flg1bf3sfxmsa6nay3cwy2alvjf9yqvhz4r96qy")))

(define-public crate-solana-banks-interface-1.9.28 (c (n "solana-banks-interface") (v "1.9.28") (d (list (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.9.28") (d #t) (k 0)) (d (n "tarpc") (r "^0.27.2") (f (quote ("full"))) (d #t) (k 0)))) (h "1ydl1rj5vk0lf5azccvrhm058dlv9j4c91nhlfswimw6zybc7ym2")))

(define-public crate-solana-banks-interface-1.10.23 (c (n "solana-banks-interface") (v "1.10.23") (d (list (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.10.23") (d #t) (k 0)) (d (n "tarpc") (r "^0.27.2") (f (quote ("full"))) (d #t) (k 0)))) (h "0s4gzxq4mkffnd99059rf6ym5yfa2yqppi5427g6rncssbrkwhs6")))

(define-public crate-solana-banks-interface-1.10.24 (c (n "solana-banks-interface") (v "1.10.24") (d (list (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.10.24") (d #t) (k 0)) (d (n "tarpc") (r "^0.27.2") (f (quote ("full"))) (d #t) (k 0)))) (h "03vmqkm464jv5qcq0rzvh6gr8g3iwsjfwvm4qg4782l135b5dpdk")))

(define-public crate-solana-banks-interface-1.10.25 (c (n "solana-banks-interface") (v "1.10.25") (d (list (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.10.25") (d #t) (k 0)) (d (n "tarpc") (r "^0.27.2") (f (quote ("full"))) (d #t) (k 0)))) (h "0m9i892wvb70scrxarzw06qlzvlrl0pwydahggbx4rcnar0p6l48")))

(define-public crate-solana-banks-interface-1.9.29 (c (n "solana-banks-interface") (v "1.9.29") (d (list (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.9.29") (d #t) (k 0)) (d (n "tarpc") (r "^0.27.2") (f (quote ("full"))) (d #t) (k 0)))) (h "1jjbjcymz6iqlzrq3kp7xvy0hc02cb9sz2pdi9qc2n7kqf6cqrlh")))

(define-public crate-solana-banks-interface-1.10.26 (c (n "solana-banks-interface") (v "1.10.26") (d (list (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.10.26") (d #t) (k 0)) (d (n "tarpc") (r "^0.27.2") (f (quote ("full"))) (d #t) (k 0)))) (h "1s5w0y134j2mvb2z5rksb9kg3fzq47mfdn98n5gaa5ca6lkq3hik")))

(define-public crate-solana-banks-interface-1.11.0 (c (n "solana-banks-interface") (v "1.11.0") (d (list (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.11.0") (d #t) (k 0)) (d (n "tarpc") (r "^0.29.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0dkz6gi3dqjy6zvvnbxdi927l0xgqm884fdbi1705qp7gsy8aaxl")))

(define-public crate-solana-banks-interface-1.10.27 (c (n "solana-banks-interface") (v "1.10.27") (d (list (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.10.27") (d #t) (k 0)) (d (n "tarpc") (r "^0.27.2") (f (quote ("full"))) (d #t) (k 0)))) (h "0kn3rgl3v5j3pc2gqfc4riawpiwxckp99h2vwj4q4m7wjlj6r1d0")))

(define-public crate-solana-banks-interface-1.11.1 (c (n "solana-banks-interface") (v "1.11.1") (d (list (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.11.1") (d #t) (k 0)) (d (n "tarpc") (r "^0.29.0") (f (quote ("full"))) (d #t) (k 0)))) (h "06blcwav5kzh0mic5vrvkrmqxzsnyya2qfdg4r4w8x2qa0m283wp")))

(define-public crate-solana-banks-interface-1.10.28 (c (n "solana-banks-interface") (v "1.10.28") (d (list (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.10.28") (d #t) (k 0)) (d (n "tarpc") (r "^0.27.2") (f (quote ("full"))) (d #t) (k 0)))) (h "1h8ps31hgrwwqxdcn1szmnxw7mpix9478axsix7fyr0mnj2001iy")))

(define-public crate-solana-banks-interface-1.10.29 (c (n "solana-banks-interface") (v "1.10.29") (d (list (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.10.29") (d #t) (k 0)) (d (n "tarpc") (r "^0.27.2") (f (quote ("full"))) (d #t) (k 0)))) (h "1w6g3znhfkfyachzaazbcl4l4izsljkj3pl3b5yn8c1h01lsyyc8")))

(define-public crate-solana-banks-interface-1.10.30 (c (n "solana-banks-interface") (v "1.10.30") (d (list (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.10.30") (d #t) (k 0)) (d (n "tarpc") (r "^0.27.2") (f (quote ("full"))) (d #t) (k 0)))) (h "0i27bks3q537nhnwvmnfpfi9crpq6fnqqfwpqgs5fx64653s6jbf")))

(define-public crate-solana-banks-interface-1.11.2 (c (n "solana-banks-interface") (v "1.11.2") (d (list (d (n "serde") (r "^1.0.138") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.11.2") (d #t) (k 0)) (d (n "tarpc") (r "^0.29.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0fd2sbc50qrlfq89nmrlys1qz9pl06f1cxz6v6kly6xxj6wyv1im")))

(define-public crate-solana-banks-interface-1.10.31 (c (n "solana-banks-interface") (v "1.10.31") (d (list (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.10.31") (d #t) (k 0)) (d (n "tarpc") (r "^0.27.2") (f (quote ("full"))) (d #t) (k 0)))) (h "0bi23lnchm7qhpqq7ixd1r4gmphxc294636cdx4agiy6dgx8ycpy")))

(define-public crate-solana-banks-interface-1.11.3 (c (n "solana-banks-interface") (v "1.11.3") (d (list (d (n "serde") (r "^1.0.138") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.11.3") (d #t) (k 0)) (d (n "tarpc") (r "^0.29.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0l04brpbpg5bz8mk64nkm50ydal9g8mq4yml49fykcqs5gww842f")))

(define-public crate-solana-banks-interface-1.10.32 (c (n "solana-banks-interface") (v "1.10.32") (d (list (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.10.32") (d #t) (k 0)) (d (n "tarpc") (r "^0.27.2") (f (quote ("full"))) (d #t) (k 0)))) (h "1pi7dm169zjsls1gdwaqjcm5rcjs784gjh4mdggywmms2v9xd07x")))

(define-public crate-solana-banks-interface-1.11.4 (c (n "solana-banks-interface") (v "1.11.4") (d (list (d (n "serde") (r "^1.0.138") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.11.4") (d #t) (k 0)) (d (n "tarpc") (r "^0.29.0") (f (quote ("full"))) (d #t) (k 0)))) (h "04iklgxn9hkn9z57ms6w4222s62fsw9asjiawcz7n283zac7km60")))

(define-public crate-solana-banks-interface-1.10.33 (c (n "solana-banks-interface") (v "1.10.33") (d (list (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.10.33") (d #t) (k 0)) (d (n "tarpc") (r "^0.27.2") (f (quote ("full"))) (d #t) (k 0)))) (h "10zxqmcspy6hdikp6kf7s858n60hnqdmz0jsvwyh8x73xnhf4y2i")))

(define-public crate-solana-banks-interface-1.10.34 (c (n "solana-banks-interface") (v "1.10.34") (d (list (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.10.34") (d #t) (k 0)) (d (n "tarpc") (r "^0.27.2") (f (quote ("full"))) (d #t) (k 0)))) (h "0rnprl6554hhh05014wprccac0hfcmjfg09q94ahhj7wxv1dr0n3")))

(define-public crate-solana-banks-interface-1.11.5 (c (n "solana-banks-interface") (v "1.11.5") (d (list (d (n "serde") (r "^1.0.138") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.11.5") (d #t) (k 0)) (d (n "tarpc") (r "^0.29.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1pb5y2s42ri1lizsf66l55avknshk6jqn7n91n4wm48zbhciwram")))

(define-public crate-solana-banks-interface-1.10.35 (c (n "solana-banks-interface") (v "1.10.35") (d (list (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.10.35") (d #t) (k 0)) (d (n "tarpc") (r "^0.27.2") (f (quote ("full"))) (d #t) (k 0)))) (h "0nsvijpw75h42msaapsiyfbcbz895x7anlywby4k0z9fzqhjfb9x")))

(define-public crate-solana-banks-interface-1.11.6 (c (n "solana-banks-interface") (v "1.11.6") (d (list (d (n "serde") (r "^1.0.138") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.11.6") (d #t) (k 0)) (d (n "tarpc") (r "^0.29.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0a6ggpdcg6hvank9mz11gcfmacixvjg4kaa7gxrziva33qkq48sl")))

(define-public crate-solana-banks-interface-1.11.7 (c (n "solana-banks-interface") (v "1.11.7") (d (list (d (n "serde") (r "^1.0.138") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.11.7") (d #t) (k 0)) (d (n "tarpc") (r "^0.29.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0jr5mdsx74yjiy9f45vlyqcsj3m90rh6yd7qyyzbgpscfk12wwmk")))

(define-public crate-solana-banks-interface-1.11.8 (c (n "solana-banks-interface") (v "1.11.8") (d (list (d (n "serde") (r "^1.0.138") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.11.8") (d #t) (k 0)) (d (n "tarpc") (r "^0.29.0") (f (quote ("full"))) (d #t) (k 0)))) (h "00pcw6ih7nfsd0l6l7rp3ik5z7rfqq7lz1lvjpyb7rcy4qlqqb4n")))

(define-public crate-solana-banks-interface-1.11.10 (c (n "solana-banks-interface") (v "1.11.10") (d (list (d (n "serde") (r "^1.0.138") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.11.10") (d #t) (k 0)) (d (n "tarpc") (r "^0.29.0") (f (quote ("full"))) (d #t) (k 0)))) (h "05ds1dhlrpxwnw78p72v69jq4dxhf26v5vdxzcxpn0wa1a1hs5mh")))

(define-public crate-solana-banks-interface-1.10.38 (c (n "solana-banks-interface") (v "1.10.38") (d (list (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.10.38") (d #t) (k 0)) (d (n "tarpc") (r "^0.27.2") (f (quote ("full"))) (d #t) (k 0)))) (h "0h2lhkya8r4q534d5if4rncdixwxpr4fmwwhjgpwilac1r7wj3ih")))

(define-public crate-solana-banks-interface-1.13.0 (c (n "solana-banks-interface") (v "1.13.0") (d (list (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.13.0") (d #t) (k 0)) (d (n "tarpc") (r "^0.27.2") (f (quote ("full"))) (d #t) (k 0)))) (h "0akdj9i33mgx6j65w104h82wxw7vvvh0jzpx2vhzz7jm4l82x9mg")))

(define-public crate-solana-banks-interface-1.14.0 (c (n "solana-banks-interface") (v "1.14.0") (d (list (d (n "serde") (r "^1.0.138") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.14.0") (d #t) (k 0)) (d (n "tarpc") (r "^0.29.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0hm2jddzidzs2kiiwaj8rvvd46rl0hxak2129viviqyd7ly1q5y3")))

(define-public crate-solana-banks-interface-1.14.1 (c (n "solana-banks-interface") (v "1.14.1") (d (list (d (n "serde") (r "^1.0.138") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.14.1") (d #t) (k 0)) (d (n "tarpc") (r "^0.29.0") (f (quote ("full"))) (d #t) (k 0)))) (h "15f1xp3908v478dbv1cwxp15k0z8k8fwk141qpnq56jndh2mg272")))

(define-public crate-solana-banks-interface-1.10.39 (c (n "solana-banks-interface") (v "1.10.39") (d (list (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.10.39") (d #t) (k 0)) (d (n "tarpc") (r "^0.27.2") (f (quote ("full"))) (d #t) (k 0)))) (h "055xa6xpxgrm3v277n81791ald5256shd4kmrys18cs57spc1kcq")))

(define-public crate-solana-banks-interface-1.14.2 (c (n "solana-banks-interface") (v "1.14.2") (d (list (d (n "serde") (r "^1.0.138") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.14.2") (d #t) (k 0)) (d (n "tarpc") (r "^0.29.0") (f (quote ("full"))) (d #t) (k 0)))) (h "16jya02b3li733zrdkd846y8yy6j97zwmg0cbkq0lxsvxv18jfhz")))

(define-public crate-solana-banks-interface-1.13.1 (c (n "solana-banks-interface") (v "1.13.1") (d (list (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.13.1") (d #t) (k 0)) (d (n "tarpc") (r "^0.27.2") (f (quote ("full"))) (d #t) (k 0)))) (h "0zm2dqkjfqbi9a9ckp28i26mfppfrcambxdyk72vy8ywmdwrjbip")))

(define-public crate-solana-banks-interface-1.14.3 (c (n "solana-banks-interface") (v "1.14.3") (d (list (d (n "serde") (r "^1.0.138") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.14.3") (d #t) (k 0)) (d (n "tarpc") (r "^0.29.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0s5b392h3l8gq1vwap6jhakbg2vxj0yfj04pszdvby48kgq9s79l")))

(define-public crate-solana-banks-interface-1.10.40 (c (n "solana-banks-interface") (v "1.10.40") (d (list (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.10.40") (d #t) (k 0)) (d (n "tarpc") (r "^0.27.2") (f (quote ("full"))) (d #t) (k 0)))) (h "0ylpi8bajmf23m0lxdlcb0nrzr2iaah5fshp95kzs5pbxxfvgmrs")))

(define-public crate-solana-banks-interface-1.14.4 (c (n "solana-banks-interface") (v "1.14.4") (d (list (d (n "serde") (r "^1.0.138") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.14.4") (d #t) (k 0)) (d (n "tarpc") (r "^0.29.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1bmvdd9fwkv1vdrnn45ii49kszixd1fd70dj0ss3hm19chvg8001")))

(define-public crate-solana-banks-interface-1.13.2 (c (n "solana-banks-interface") (v "1.13.2") (d (list (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.13.2") (d #t) (k 0)) (d (n "tarpc") (r "^0.27.2") (f (quote ("full"))) (d #t) (k 0)))) (h "0fi9nssf7lr2dhs9q0bk5j77k7ays47wb9gv95cjcdi886xc7wdz")))

(define-public crate-solana-banks-interface-1.14.5 (c (n "solana-banks-interface") (v "1.14.5") (d (list (d (n "serde") (r "^1.0.138") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.14.5") (d #t) (k 0)) (d (n "tarpc") (r "^0.29.0") (f (quote ("full"))) (d #t) (k 0)))) (h "01vavnr9dinx5cah9775166v5bdzyc13gyqsr47xsydp2d5a14dw")))

(define-public crate-solana-banks-interface-1.10.41 (c (n "solana-banks-interface") (v "1.10.41") (d (list (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.10.41") (d #t) (k 0)) (d (n "tarpc") (r "^0.27.2") (f (quote ("full"))) (d #t) (k 0)))) (h "12xkj7whkx6kys8qvc759i4m6xqw77kc8mm28q28s776ys29ji2k")))

(define-public crate-solana-banks-interface-1.13.3 (c (n "solana-banks-interface") (v "1.13.3") (d (list (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.13.3") (d #t) (k 0)) (d (n "tarpc") (r "^0.27.2") (f (quote ("full"))) (d #t) (k 0)))) (h "01z3gg1653ih77mfcs360id80mjav70lcp0j6c4fakd4w476mi0c")))

(define-public crate-solana-banks-interface-1.13.4 (c (n "solana-banks-interface") (v "1.13.4") (d (list (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.13.4") (d #t) (k 0)) (d (n "tarpc") (r "^0.27.2") (f (quote ("full"))) (d #t) (k 0)))) (h "0r58qs8ra7fiqkg1yqz55cqj576nzhk1834rw4h8vfzmsv2dxllj")))

(define-public crate-solana-banks-interface-1.14.6 (c (n "solana-banks-interface") (v "1.14.6") (d (list (d (n "serde") (r "^1.0.138") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.14.6") (d #t) (k 0)) (d (n "tarpc") (r "^0.29.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0i02xvixkcsg37jhgqi1m99z3plybn5n75bl9y7zl9xwn2kaj05q")))

(define-public crate-solana-banks-interface-1.14.7 (c (n "solana-banks-interface") (v "1.14.7") (d (list (d (n "serde") (r "^1.0.138") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.14.7") (d #t) (k 0)) (d (n "tarpc") (r "^0.29.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1zdfa2zsqilp37az7g7cf0d13xssdl9kld736q3wdrqg8mi7h4qa")))

(define-public crate-solana-banks-interface-1.13.5 (c (n "solana-banks-interface") (v "1.13.5") (d (list (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.13.5") (d #t) (k 0)) (d (n "tarpc") (r "^0.27.2") (f (quote ("full"))) (d #t) (k 0)))) (h "087hzhzwjhjjj8ihi9ywb82m17l9vxykliv8w0lchbqw3fv7xbkf")))

(define-public crate-solana-banks-interface-1.14.8 (c (n "solana-banks-interface") (v "1.14.8") (d (list (d (n "serde") (r "^1.0.138") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.14.8") (d #t) (k 0)) (d (n "tarpc") (r "^0.29.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1gahw9q0xm2jl73knbvdkmbpwvdmz2ywzny0mg70l59q3c9g42jv")))

(define-public crate-solana-banks-interface-1.14.9 (c (n "solana-banks-interface") (v "1.14.9") (d (list (d (n "serde") (r "^1.0.138") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.14.9") (d #t) (k 0)) (d (n "tarpc") (r "^0.29.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1pcv956nk6dyjgwn0v6iwj84i57ff19m923mgqm90d7p9g0cr7mj")))

(define-public crate-solana-banks-interface-1.14.10 (c (n "solana-banks-interface") (v "1.14.10") (d (list (d (n "serde") (r "^1.0.138") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.14.10") (d #t) (k 0)) (d (n "tarpc") (r "^0.29.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1wpgqwgkjdr2jw34g3b9r30m86jv1rk2z1rdz3vwdjmiv3q3pf51")))

(define-public crate-solana-banks-interface-1.14.11 (c (n "solana-banks-interface") (v "1.14.11") (d (list (d (n "serde") (r "^1.0.138") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.14.11") (d #t) (k 0)) (d (n "tarpc") (r "^0.29.0") (f (quote ("full"))) (d #t) (k 0)))) (h "044s025dc10m3c5avbh48l3m43zmbpbkxkfbb6m5mfvy456d30y2")))

(define-public crate-solana-banks-interface-1.14.12 (c (n "solana-banks-interface") (v "1.14.12") (d (list (d (n "serde") (r "^1.0.138") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.14.12") (d #t) (k 0)) (d (n "tarpc") (r "^0.29.0") (f (quote ("full"))) (d #t) (k 0)))) (h "07bbdwbyzk2kwdqyhp5lk7j2haylq8av9q7wlrxhjlfbk5d4xqji")))

(define-public crate-solana-banks-interface-1.13.6 (c (n "solana-banks-interface") (v "1.13.6") (d (list (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.13.6") (d #t) (k 0)) (d (n "tarpc") (r "^0.27.2") (f (quote ("full"))) (d #t) (k 0)))) (h "0ackvf5cyxn8kfi6g4av1fiq44y86kimp5ghzmc5qmgxr5pvlrga")))

(define-public crate-solana-banks-interface-1.14.13 (c (n "solana-banks-interface") (v "1.14.13") (d (list (d (n "serde") (r "^1.0.138") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.14.13") (d #t) (k 0)) (d (n "tarpc") (r "^0.29.0") (f (quote ("full"))) (d #t) (k 0)))) (h "07jgfdnfv6a20006c9j6rnxkw3yjk0ssk5hi0j25hvv52dbb631f")))

(define-public crate-solana-banks-interface-1.15.0 (c (n "solana-banks-interface") (v "1.15.0") (d (list (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.15.0") (d #t) (k 0)) (d (n "tarpc") (r "^0.29.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0pvab5x8vz8is9vybfdps7l1z0q09rfn7fgffkljnd46kyj3h094") (y #t)))

(define-public crate-solana-banks-interface-1.14.14 (c (n "solana-banks-interface") (v "1.14.14") (d (list (d (n "serde") (r "^1.0.138") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.14.14") (d #t) (k 0)) (d (n "tarpc") (r "^0.29.0") (f (quote ("full"))) (d #t) (k 0)))) (h "072v5rw01gsclijcqbl4hga78yfl0n2alkm5jbx9m4g7xh00xgn9")))

(define-public crate-solana-banks-interface-1.14.15 (c (n "solana-banks-interface") (v "1.14.15") (d (list (d (n "serde") (r "^1.0.138") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.14.15") (d #t) (k 0)) (d (n "tarpc") (r "^0.29.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0b8ndh4zr90as13cs9m25z3i9wvdarkcc5j27l0yczlsd11nrwwl")))

(define-public crate-solana-banks-interface-1.15.1 (c (n "solana-banks-interface") (v "1.15.1") (d (list (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.15.1") (d #t) (k 0)) (d (n "tarpc") (r "^0.29.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0hqgw9y0lp3zsdldwqg7pqq1h8j3rnm16zjzr7ypywpfm92p45dn") (y #t)))

(define-public crate-solana-banks-interface-1.15.2 (c (n "solana-banks-interface") (v "1.15.2") (d (list (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.15.2") (d #t) (k 0)) (d (n "tarpc") (r "^0.29.0") (f (quote ("full"))) (d #t) (k 0)))) (h "18v8acld9h2rz1abmlgkx19qgmabgvrx1zhv8nby29kd1raqhyr1") (y #t)))

(define-public crate-solana-banks-interface-1.14.16 (c (n "solana-banks-interface") (v "1.14.16") (d (list (d (n "serde") (r "^1.0.138") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.14.16") (d #t) (k 0)) (d (n "tarpc") (r "^0.29.0") (f (quote ("full"))) (d #t) (k 0)))) (h "07lai5y5qjhmqv64q3slpz862scy0zrawjvq9kalkchqg2421gi3")))

(define-public crate-solana-banks-interface-1.14.17 (c (n "solana-banks-interface") (v "1.14.17") (d (list (d (n "serde") (r "^1.0.138") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.14.17") (d #t) (k 0)) (d (n "tarpc") (r "^0.29.0") (f (quote ("full"))) (d #t) (k 0)))) (h "057sj8nmsdjijl1cmvq3vk5g5244aimrcqfh5yh9559qbplbkqmw")))

(define-public crate-solana-banks-interface-1.13.7 (c (n "solana-banks-interface") (v "1.13.7") (d (list (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.13.7") (d #t) (k 0)) (d (n "tarpc") (r "^0.27.2") (f (quote ("full"))) (d #t) (k 0)))) (h "1k48p3rjvwn2vkvln8kda34knxdfnzhjklb6am6zwhkicnjmkxs4")))

(define-public crate-solana-banks-interface-1.14.18 (c (n "solana-banks-interface") (v "1.14.18") (d (list (d (n "serde") (r "^1.0.138") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.14.18") (d #t) (k 0)) (d (n "tarpc") (r "^0.29.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1v8iq9lcd4w210rkas183rmml9p8j5ivqgvg0bsl1dw3qgpk0f5f")))

(define-public crate-solana-banks-interface-1.16.0 (c (n "solana-banks-interface") (v "1.16.0") (d (list (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.16.0") (d #t) (k 0)) (d (n "tarpc") (r "^0.29.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1jnnwkg7xim28hh7vm046l52d9bc9j23b19y6gpfnrv5qzzahqq0")))

(define-public crate-solana-banks-interface-1.16.1 (c (n "solana-banks-interface") (v "1.16.1") (d (list (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.16.1") (d #t) (k 0)) (d (n "tarpc") (r "^0.29.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0bl9kfdgg12qym7q66r70pwnf45aiwbxlvs100xm5s4c9q5qpkbr")))

(define-public crate-solana-banks-interface-1.14.19 (c (n "solana-banks-interface") (v "1.14.19") (d (list (d (n "serde") (r "^1.0.138") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.14.19") (d #t) (k 0)) (d (n "tarpc") (r "^0.29.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1qinj76ddahvwdn98irif0p12wf0p51lhfzhrnjililvhdn2hmmc")))

(define-public crate-solana-banks-interface-1.16.2 (c (n "solana-banks-interface") (v "1.16.2") (d (list (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.16.2") (d #t) (k 0)) (d (n "tarpc") (r "^0.29.0") (f (quote ("full"))) (d #t) (k 0)))) (h "13ix1xmbj76bdmhb7cdw1pgr9pdwgghaq0iaxk5a19d2xknxbkg4")))

(define-public crate-solana-banks-interface-1.16.3 (c (n "solana-banks-interface") (v "1.16.3") (d (list (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.16.3") (d #t) (k 0)) (d (n "tarpc") (r "^0.29.0") (f (quote ("full"))) (d #t) (k 0)))) (h "179n8lr2jz8rd351f9amn62c4dpbffv75a8d38wbzbyiigighh12")))

(define-public crate-solana-banks-interface-1.14.20 (c (n "solana-banks-interface") (v "1.14.20") (d (list (d (n "serde") (r "^1.0.138") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.14.20") (d #t) (k 0)) (d (n "tarpc") (r "^0.29.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0vm6kmid32s2l458p3asjkq2mi74nzi58dbx11fjll7xn10z88qp")))

(define-public crate-solana-banks-interface-1.16.4 (c (n "solana-banks-interface") (v "1.16.4") (d (list (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.16.4") (d #t) (k 0)) (d (n "tarpc") (r "^0.29.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1na3k3ys0gfcib4i4iqhmhk7pp2iwp307afv3rywpky1shsjbfap")))

(define-public crate-solana-banks-interface-1.16.5 (c (n "solana-banks-interface") (v "1.16.5") (d (list (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.16.5") (d #t) (k 0)) (d (n "tarpc") (r "^0.29.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1kv1rmafrfs34c56ki9lyq75nzxan9386grdx2w4y3np22nlzcv1")))

(define-public crate-solana-banks-interface-1.14.21 (c (n "solana-banks-interface") (v "1.14.21") (d (list (d (n "serde") (r "^1.0.138") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.14.21") (d #t) (k 0)) (d (n "tarpc") (r "^0.29.0") (f (quote ("full"))) (d #t) (k 0)))) (h "01pfmwv57l88j41szjj5p7izw7bf7cy1bd47r1yfsg5c5q5vximz")))

(define-public crate-solana-banks-interface-1.14.22 (c (n "solana-banks-interface") (v "1.14.22") (d (list (d (n "serde") (r "^1.0.138") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.14.22") (d #t) (k 0)) (d (n "tarpc") (r "^0.29.0") (f (quote ("full"))) (d #t) (k 0)))) (h "080csp2l594c6rcbqclysabirlrbh9lkhylkxglayfmcfyrnrksz")))

(define-public crate-solana-banks-interface-1.16.6 (c (n "solana-banks-interface") (v "1.16.6") (d (list (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.16.6") (d #t) (k 0)) (d (n "tarpc") (r "^0.29.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1p0xgik05i0xyq2c4cwwg2dq237f5sm0lqyfhpsy0ijr9r1c0nwv")))

(define-public crate-solana-banks-interface-1.16.7 (c (n "solana-banks-interface") (v "1.16.7") (d (list (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.16.7") (d #t) (k 0)) (d (n "tarpc") (r "^0.29.0") (f (quote ("full"))) (d #t) (k 0)))) (h "18jlkdds8bmai4sjjqhzskqql81lxj819rdfymzp02vgybqibkxc")))

(define-public crate-solana-banks-interface-1.14.23 (c (n "solana-banks-interface") (v "1.14.23") (d (list (d (n "serde") (r "^1.0.138") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.14.23") (d #t) (k 0)) (d (n "tarpc") (r "^0.29.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1ghpdqi10mbxych2b6mz5sjrpkq11xsahi0rcb6ysmxcl7i9qdjv")))

(define-public crate-solana-banks-interface-1.16.8 (c (n "solana-banks-interface") (v "1.16.8") (d (list (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.16.8") (d #t) (k 0)) (d (n "tarpc") (r "^0.29.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0q8irmwxzxg8gqhpjk2rzqrx66cbv815zrdw3c2nmbrvqm7sz4qh")))

(define-public crate-solana-banks-interface-1.14.24 (c (n "solana-banks-interface") (v "1.14.24") (d (list (d (n "serde") (r "^1.0.138") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.14.24") (d #t) (k 0)) (d (n "tarpc") (r "^0.29.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0sn57h5l0kv5qwvgf79pirpcsvsypj1jqcw6rydpdw3bhbdcaynx")))

(define-public crate-solana-banks-interface-1.16.9 (c (n "solana-banks-interface") (v "1.16.9") (d (list (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.16.9") (d #t) (k 0)) (d (n "tarpc") (r "^0.29.0") (f (quote ("full"))) (d #t) (k 0)))) (h "16swda0k9dnha6nw53wgbkh8dwfp36ly8sz9rfd25syxpblci4jg")))

(define-public crate-solana-banks-interface-1.16.10 (c (n "solana-banks-interface") (v "1.16.10") (d (list (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.16.10") (d #t) (k 0)) (d (n "tarpc") (r "^0.29.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0a45raaxzkyqaxivrips6yp5h0134zr3sf2i20v4c4vsqsvbk9i2")))

(define-public crate-solana-banks-interface-1.14.25 (c (n "solana-banks-interface") (v "1.14.25") (d (list (d (n "serde") (r "^1.0.138") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.14.25") (d #t) (k 0)) (d (n "tarpc") (r "^0.29.0") (f (quote ("full"))) (d #t) (k 0)))) (h "10qp04ajz6l48q8a2g66f40fha8xln5yfmhd1dckwg5bjryrjaxv")))

(define-public crate-solana-banks-interface-1.16.11 (c (n "solana-banks-interface") (v "1.16.11") (d (list (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.16.11") (d #t) (k 0)) (d (n "tarpc") (r "^0.29.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1j8pszlfwjr8a9qlw6qns2zxjvhrav6w01wl3lkrzx1i95138lyv")))

(define-public crate-solana-banks-interface-1.14.26 (c (n "solana-banks-interface") (v "1.14.26") (d (list (d (n "serde") (r "^1.0.138") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.14.26") (d #t) (k 0)) (d (n "tarpc") (r "^0.29.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1q77ihr0pn29vibxmx1in60ay9cpl5ci7pk3ji6wqfz8nnykkdyv")))

(define-public crate-solana-banks-interface-1.16.12 (c (n "solana-banks-interface") (v "1.16.12") (d (list (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.16.12") (d #t) (k 0)) (d (n "tarpc") (r "^0.29.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0xy7hncn5dl78004z4nyf0mrm2jgcv7dz2m3qhm37azp04qd05m2")))

(define-public crate-solana-banks-interface-1.14.27 (c (n "solana-banks-interface") (v "1.14.27") (d (list (d (n "serde") (r "^1.0.138") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.14.27") (d #t) (k 0)) (d (n "tarpc") (r "^0.29.0") (f (quote ("full"))) (d #t) (k 0)))) (h "13xb24ssm6ygyk0fqsx9ggvci2vg9vz22brbv1bxyhc2sh3q6sv6")))

(define-public crate-solana-banks-interface-1.16.13 (c (n "solana-banks-interface") (v "1.16.13") (d (list (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.16.13") (d #t) (k 0)) (d (n "tarpc") (r "^0.29.0") (f (quote ("full"))) (d #t) (k 0)))) (h "17gcaz5wrm9rx74vi6dixjklc10xrnk1q0kg5jh107i35186isic")))

(define-public crate-solana-banks-interface-1.16.14 (c (n "solana-banks-interface") (v "1.16.14") (d (list (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.16.14") (d #t) (k 0)) (d (n "tarpc") (r "^0.29.0") (f (quote ("full"))) (d #t) (k 0)))) (h "020b25xyf4i2xw5b11x70nz5yajf5qi53dcdlb65inay6pbb965j")))

(define-public crate-solana-banks-interface-1.14.28 (c (n "solana-banks-interface") (v "1.14.28") (d (list (d (n "serde") (r "^1.0.138") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.14.28") (d #t) (k 0)) (d (n "tarpc") (r "^0.29.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1b9z3m4grjyw9akiwgnnir9ds4nvha8g18mi9c15iag8cc08j04b")))

(define-public crate-solana-banks-interface-1.14.29 (c (n "solana-banks-interface") (v "1.14.29") (d (list (d (n "serde") (r "^1.0.138") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.14.29") (d #t) (k 0)) (d (n "tarpc") (r "^0.29.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0a0jwr8f1cp8ww50sf8mw4n7x2mcl0j1v4glj42f2982b1pd3nal")))

(define-public crate-solana-banks-interface-1.16.15 (c (n "solana-banks-interface") (v "1.16.15") (d (list (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.16.15") (d #t) (k 0)) (d (n "tarpc") (r "^0.29.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0wg3km9rh0b22w7zlxp3wql9pa4rs297cny9c55nh4dnkxp21386")))

(define-public crate-solana-banks-interface-1.17.0 (c (n "solana-banks-interface") (v "1.17.0") (d (list (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.0") (d #t) (k 0)) (d (n "tarpc") (r "^0.29.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0lapb17yrhc1gap5fxzl4aiwx36q9hh6bwyycfy0vd6a2ln6scm0")))

(define-public crate-solana-banks-interface-1.16.16 (c (n "solana-banks-interface") (v "1.16.16") (d (list (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.16.16") (d #t) (k 0)) (d (n "tarpc") (r "^0.29.0") (f (quote ("full"))) (d #t) (k 0)))) (h "113a2w3paw8pvwhx1b2mlg2v93g62gc86ijm8qk36ms6jyca96a1")))

(define-public crate-solana-banks-interface-1.17.1 (c (n "solana-banks-interface") (v "1.17.1") (d (list (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.1") (d #t) (k 0)) (d (n "tarpc") (r "^0.29.0") (f (quote ("full"))) (d #t) (k 0)))) (h "13xmf64p2jcda5cnv6rm97x3rml73fq9bg6iaf52dbxqg27yankr")))

(define-public crate-solana-banks-interface-1.16.17 (c (n "solana-banks-interface") (v "1.16.17") (d (list (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.16.17") (d #t) (k 0)) (d (n "tarpc") (r "^0.29.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1sypmghq4d8jwfh6bq9wracvbrqrfysm2hx41ihgqyc52rlnsamw")))

(define-public crate-solana-banks-interface-1.17.2 (c (n "solana-banks-interface") (v "1.17.2") (d (list (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.2") (d #t) (k 0)) (d (n "tarpc") (r "^0.29.0") (f (quote ("full"))) (d #t) (k 0)))) (h "15kxfjsdw10bqby706dj9mbhvhxm9j45wvyxl6bwavk474cakphz")))

(define-public crate-solana-banks-interface-1.16.18 (c (n "solana-banks-interface") (v "1.16.18") (d (list (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.16.18") (d #t) (k 0)) (d (n "tarpc") (r "^0.29.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1hn3vd7w2nbvv84dcfriy9h2y9d3acmghh8myiznzwl9byqc6ym0")))

(define-public crate-solana-banks-interface-1.17.3 (c (n "solana-banks-interface") (v "1.17.3") (d (list (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.3") (d #t) (k 0)) (d (n "tarpc") (r "^0.29.0") (f (quote ("full"))) (d #t) (k 0)))) (h "011ani6hjj8rs0267nl8g0pr47d76lwxss3jghpy4mx4cfs9x4cd")))

(define-public crate-solana-banks-interface-1.17.4 (c (n "solana-banks-interface") (v "1.17.4") (d (list (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.4") (d #t) (k 0)) (d (n "tarpc") (r "^0.29.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1lvbn7b3yi0hcfr1igi7k9bfwxdvaay344bymach8g84yjr4aiik")))

(define-public crate-solana-banks-interface-1.16.19 (c (n "solana-banks-interface") (v "1.16.19") (d (list (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.16.19") (d #t) (k 0)) (d (n "tarpc") (r "^0.29.0") (f (quote ("full"))) (d #t) (k 0)))) (h "08f8jg8kbiavavcamks4gk4h7qj257bqd5qkjckxj89and8jnqkb")))

(define-public crate-solana-banks-interface-1.17.5 (c (n "solana-banks-interface") (v "1.17.5") (d (list (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.5") (d #t) (k 0)) (d (n "tarpc") (r "^0.29.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1p5jcsjqwl6n3is9zv277fawgcgzqnl5xiamdbj7zqckcm2djryp")))

(define-public crate-solana-banks-interface-1.17.6 (c (n "solana-banks-interface") (v "1.17.6") (d (list (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.6") (d #t) (k 0)) (d (n "tarpc") (r "^0.29.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1j8ni5dnkm0rh49ldc4mz9kdw7n3347x4jkynbhq3w56h3hdygn0")))

(define-public crate-solana-banks-interface-1.16.20 (c (n "solana-banks-interface") (v "1.16.20") (d (list (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.16.20") (d #t) (k 0)) (d (n "tarpc") (r "^0.29.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0p15a9fya1dxafkpzy7w6vnb17mcps6q2hjrdfvg46nbjkd5z9nf")))

(define-public crate-solana-banks-interface-1.17.7 (c (n "solana-banks-interface") (v "1.17.7") (d (list (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.7") (d #t) (k 0)) (d (n "tarpc") (r "^0.29.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1d0i7z80vmp3zqby7nvvkr3zgfkqw03wvr4a6s2cgy34agqbd35c")))

(define-public crate-solana-banks-interface-1.16.21 (c (n "solana-banks-interface") (v "1.16.21") (d (list (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.16.21") (d #t) (k 0)) (d (n "tarpc") (r "^0.29.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0gwjdp0svq5b9hj6j6x42d0chqqn4q1ilxdkhz39mxl7rrhflc0l")))

(define-public crate-solana-banks-interface-1.17.8 (c (n "solana-banks-interface") (v "1.17.8") (d (list (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.8") (d #t) (k 0)) (d (n "tarpc") (r "^0.29.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1nlrzrvqyrr2d935nhvpx4igcf1j1vz7c5kc0av25khnqfwcg1gc")))

(define-public crate-solana-banks-interface-1.16.22 (c (n "solana-banks-interface") (v "1.16.22") (d (list (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.16.22") (d #t) (k 0)) (d (n "tarpc") (r "^0.29.0") (f (quote ("full"))) (d #t) (k 0)))) (h "043zrlscw0kxyi1wa80swi0dprs75cg5yg78s4jqb09zs2hgrr6g")))

(define-public crate-solana-banks-interface-1.16.23 (c (n "solana-banks-interface") (v "1.16.23") (d (list (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.16.23") (d #t) (k 0)) (d (n "tarpc") (r "^0.29.0") (f (quote ("full"))) (d #t) (k 0)))) (h "10sydx749k1nqilwjs633fcmdf7gfs371fxlqpqnvxa4z7zzc4yb")))

(define-public crate-solana-banks-interface-1.17.9 (c (n "solana-banks-interface") (v "1.17.9") (d (list (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.9") (d #t) (k 0)) (d (n "tarpc") (r "^0.29.0") (f (quote ("full"))) (d #t) (k 0)))) (h "08aghzq0q572h9xbi2w8n30mg49p8mqj8im94ld5vgppj67f5rjf")))

(define-public crate-solana-banks-interface-1.17.10 (c (n "solana-banks-interface") (v "1.17.10") (d (list (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.10") (d #t) (k 0)) (d (n "tarpc") (r "^0.29.0") (f (quote ("full"))) (d #t) (k 0)))) (h "05h2w44mxnab8dbapzmjb20p14843c5i9fclk1fdcznfzzzfw6ik")))

(define-public crate-solana-banks-interface-1.17.11 (c (n "solana-banks-interface") (v "1.17.11") (d (list (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.11") (d #t) (k 0)) (d (n "tarpc") (r "^0.29.0") (f (quote ("full"))) (d #t) (k 0)))) (h "169b3lxlzcp4246n0lyy9097j7mg2h94m6hri5v8xmldabg738py")))

(define-public crate-solana-banks-interface-1.17.12 (c (n "solana-banks-interface") (v "1.17.12") (d (list (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.12") (d #t) (k 0)) (d (n "tarpc") (r "^0.29.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1f1a3ak6lzl1wdavygaspcg8g9xkljfws6v66ainywhq2visba8g")))

(define-public crate-solana-banks-interface-1.16.24 (c (n "solana-banks-interface") (v "1.16.24") (d (list (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.16.24") (d #t) (k 0)) (d (n "tarpc") (r "^0.29.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0nakdk168gc4v1adp6983fi4g5m39w7ss6vhb6rmvamw5gxzjmb8")))

(define-public crate-solana-banks-interface-1.17.13 (c (n "solana-banks-interface") (v "1.17.13") (d (list (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.13") (d #t) (k 0)) (d (n "tarpc") (r "^0.29.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0whdyv2h2hi0zrlfsf3m3nc2cj89zl52py6jg3d7hsy03p61k2dq")))

(define-public crate-solana-banks-interface-1.17.14 (c (n "solana-banks-interface") (v "1.17.14") (d (list (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.14") (d #t) (k 0)) (d (n "tarpc") (r "^0.29.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0hx1vk2dlba6hsn6iq9k5dbbhb2rnz198kcfw1ix2mfdil0549kw")))

(define-public crate-solana-banks-interface-1.17.15 (c (n "solana-banks-interface") (v "1.17.15") (d (list (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.15") (d #t) (k 0)) (d (n "tarpc") (r "^0.29.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0dc3vd2cpljv06nsnxcdxb26y1xw59ks89v4kxrzqqw93cz9z5ax")))

(define-public crate-solana-banks-interface-1.16.25 (c (n "solana-banks-interface") (v "1.16.25") (d (list (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.16.25") (d #t) (k 0)) (d (n "tarpc") (r "^0.29.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0xyhn4b63k9lh6a62cwf3f2w7rwbz9k8il7pq1kap8l9wpzdf406")))

(define-public crate-solana-banks-interface-1.16.27 (c (n "solana-banks-interface") (v "1.16.27") (d (list (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.16.27") (d #t) (k 0)) (d (n "tarpc") (r "^0.29.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0v35a87lcpnbwj1spydycbb4yl38ysm5k835hv1i0vas5snw32bc")))

(define-public crate-solana-banks-interface-1.17.16 (c (n "solana-banks-interface") (v "1.17.16") (d (list (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.16") (d #t) (k 0)) (d (n "tarpc") (r "^0.29.0") (f (quote ("full"))) (d #t) (k 0)))) (h "08b3ynl0v6iw01kwzp6xhps1y102xzjawlqhc1grnafc7wz1wda6")))

(define-public crate-solana-banks-interface-1.17.17 (c (n "solana-banks-interface") (v "1.17.17") (d (list (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.17") (d #t) (k 0)) (d (n "tarpc") (r "^0.29.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1br3m19d95mkqgyy6dwf9y1ljdnw8djms9g7gxs7h9msk10amnvr")))

(define-public crate-solana-banks-interface-1.17.18 (c (n "solana-banks-interface") (v "1.17.18") (d (list (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.18") (d #t) (k 0)) (d (n "tarpc") (r "^0.29.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1xpgrb9qwjyl83hjbnr9hlj6f3zyg1qqigii6vz0h9ww824n70rz")))

(define-public crate-solana-banks-interface-1.18.0 (c (n "solana-banks-interface") (v "1.18.0") (d (list (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.18.0") (d #t) (k 0)) (d (n "tarpc") (r "^0.29.0") (f (quote ("full"))) (d #t) (k 0)))) (h "10x7yh98hjwvrcrbbns6h3chk4v1dcj2cpmf1q822pvawv913jp9")))

(define-public crate-solana-banks-interface-1.18.1 (c (n "solana-banks-interface") (v "1.18.1") (d (list (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.18.1") (d #t) (k 0)) (d (n "tarpc") (r "^0.29.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1gc2xkbndp8vf23lsj15fhi5gyxijkabwlg657kmpp3624vmi1qm")))

(define-public crate-solana-banks-interface-1.17.20 (c (n "solana-banks-interface") (v "1.17.20") (d (list (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.20") (d #t) (k 0)) (d (n "tarpc") (r "^0.29.0") (f (quote ("full"))) (d #t) (k 0)))) (h "05mar54pwchl00zd1i3h7xd07dv5isbz3ky5j9d5aqa225kr1ab8")))

(define-public crate-solana-banks-interface-1.17.22 (c (n "solana-banks-interface") (v "1.17.22") (d (list (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.22") (d #t) (k 0)) (d (n "tarpc") (r "^0.29.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0jx5x9wjbjq9yy1n96nmvgmpykx3a1zmx669wslj5bwaqxh7ys4v")))

(define-public crate-solana-banks-interface-1.18.2 (c (n "solana-banks-interface") (v "1.18.2") (d (list (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.18.2") (d #t) (k 0)) (d (n "tarpc") (r "^0.29.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1jjkskqacxrshzva3y0f80fb4w5rpj5nylyw7yryzzfjmgi16pnq")))

(define-public crate-solana-banks-interface-1.17.23 (c (n "solana-banks-interface") (v "1.17.23") (d (list (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.23") (d #t) (k 0)) (d (n "tarpc") (r "^0.29.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1kqi95v772fiqwd8bkwf8j9j9hrdqp6p5bizzdq08nrii51pcln3")))

(define-public crate-solana-banks-interface-1.18.3 (c (n "solana-banks-interface") (v "1.18.3") (d (list (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.18.3") (d #t) (k 0)) (d (n "tarpc") (r "^0.29.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1zymwa2jih1r5j0y2zsyq3vlq746lm23rcfs5fy8b9171vhs49mn")))

(define-public crate-solana-banks-interface-1.18.4 (c (n "solana-banks-interface") (v "1.18.4") (d (list (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.18.4") (d #t) (k 0)) (d (n "tarpc") (r "^0.29.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0y7kgb6hc9683sm8x1bfq84zh7i2r9lfi7flva8cnw4rs8mjjf5l")))

(define-public crate-solana-banks-interface-1.17.24 (c (n "solana-banks-interface") (v "1.17.24") (d (list (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.24") (d #t) (k 0)) (d (n "tarpc") (r "^0.29.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0hgyxzxha6kma075n80avji4k14a332bvc97zrcmvp5gzl5svlvf")))

(define-public crate-solana-banks-interface-1.17.25 (c (n "solana-banks-interface") (v "1.17.25") (d (list (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.25") (d #t) (k 0)) (d (n "tarpc") (r "^0.29.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0gqlsx59mnb4v1bhhxpq79kzi3sl4k52lcpc6wzr6zm485xld61a")))

(define-public crate-solana-banks-interface-1.18.5 (c (n "solana-banks-interface") (v "1.18.5") (d (list (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.18.5") (d #t) (k 0)) (d (n "tarpc") (r "^0.29.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0v1i57qgf7yy71yxlzbzkzjh6i964cjw54xpp91kzwahnlsxb1kf")))

(define-public crate-solana-banks-interface-1.17.26 (c (n "solana-banks-interface") (v "1.17.26") (d (list (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.26") (d #t) (k 0)) (d (n "tarpc") (r "^0.29.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0c2jykjah5fbapz4d6y6vxprm5y2bphskai677p434fvhdzr903f")))

(define-public crate-solana-banks-interface-1.18.6 (c (n "solana-banks-interface") (v "1.18.6") (d (list (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.18.6") (d #t) (k 0)) (d (n "tarpc") (r "^0.29.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1h8f2driqhhsrdfqq7xc7x18wx863przc2qy6q4f1l637ihdld48")))

(define-public crate-solana-banks-interface-1.17.27 (c (n "solana-banks-interface") (v "1.17.27") (d (list (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.27") (d #t) (k 0)) (d (n "tarpc") (r "^0.29.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0d92y5f0rkkhagv19zbgrs7rfqqnx4jir3ih72siblfxg2g3f257")))

(define-public crate-solana-banks-interface-1.18.7 (c (n "solana-banks-interface") (v "1.18.7") (d (list (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.18.7") (d #t) (k 0)) (d (n "tarpc") (r "^0.29.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0ama76qaw477yy0bara6dsfnkkwyhfa88985r66c5jb2i3ds8dbb")))

(define-public crate-solana-banks-interface-1.18.8 (c (n "solana-banks-interface") (v "1.18.8") (d (list (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.18.8") (d #t) (k 0)) (d (n "tarpc") (r "^0.29.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1qrjvarbngdmfyl538d54zas73q4dgv0wajsrgblibyz8axbqhfd")))

(define-public crate-solana-banks-interface-1.17.28 (c (n "solana-banks-interface") (v "1.17.28") (d (list (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.28") (d #t) (k 0)) (d (n "tarpc") (r "^0.29.0") (f (quote ("full"))) (d #t) (k 0)))) (h "060rm3ahhwc5y9gzxc7qvsajvjblnsixvprwd522q85m431yxffa")))

(define-public crate-solana-banks-interface-1.18.9 (c (n "solana-banks-interface") (v "1.18.9") (d (list (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.18.9") (d #t) (k 0)) (d (n "tarpc") (r "^0.29.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1mli435bk3m8ar0k0iln50hlny16g488hdb4ipk7g68r4hs129bn")))

(define-public crate-solana-banks-interface-1.17.29 (c (n "solana-banks-interface") (v "1.17.29") (d (list (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.29") (d #t) (k 0)) (d (n "tarpc") (r "^0.29.0") (f (quote ("full"))) (d #t) (k 0)))) (h "025x66yrbgk7nih2gph1qk69wszc6b1qg6zrnvpwq1jf3vn937kk")))

(define-public crate-solana-banks-interface-1.17.30 (c (n "solana-banks-interface") (v "1.17.30") (d (list (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.30") (d #t) (k 0)) (d (n "tarpc") (r "^0.29.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1hipk0bzhhyg0p17c37awgxs1ap7cwwsza4vypv0fia08ivgccy8")))

(define-public crate-solana-banks-interface-1.18.10 (c (n "solana-banks-interface") (v "1.18.10") (d (list (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.18.10") (d #t) (k 0)) (d (n "tarpc") (r "^0.29.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0wq2sxh4rgvxbqnckcj844cjy9qbmd086gsj62w6lsv56rb9hsxk")))

(define-public crate-solana-banks-interface-1.18.11 (c (n "solana-banks-interface") (v "1.18.11") (d (list (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.18.11") (d #t) (k 0)) (d (n "tarpc") (r "^0.29.0") (f (quote ("full"))) (d #t) (k 0)))) (h "13b4dd6hbxakyx739xn3nbviw8aydxricj9179mv2bkrdcfyyvkc")))

(define-public crate-solana-banks-interface-1.17.31 (c (n "solana-banks-interface") (v "1.17.31") (d (list (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.31") (d #t) (k 0)) (d (n "tarpc") (r "^0.29.0") (f (quote ("full"))) (d #t) (k 0)))) (h "03lzs33542rcyjis0x1ibknm67ma3lpyb5an0ic3bfxc33ghi0p4")))

(define-public crate-solana-banks-interface-1.18.12 (c (n "solana-banks-interface") (v "1.18.12") (d (list (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.18.12") (d #t) (k 0)) (d (n "tarpc") (r "^0.29.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0kg7lnz6aqbaihq26r64wli3f5ly31xxp4g52wyimspmkfh7j4kl")))

(define-public crate-solana-banks-interface-1.17.32 (c (n "solana-banks-interface") (v "1.17.32") (d (list (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.32") (d #t) (k 0)) (d (n "tarpc") (r "^0.29.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1hcd11hlfwrnpvnirjjr5nhxri01drwxwgq01bjk4c1m8qka2q4g")))

(define-public crate-solana-banks-interface-1.17.33 (c (n "solana-banks-interface") (v "1.17.33") (d (list (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.33") (d #t) (k 0)) (d (n "tarpc") (r "^0.29.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0izr4afzmvrlnd6yachaqi0sqi5bxa3b9nvjjqkmj185xxba3yg9")))

(define-public crate-solana-banks-interface-1.18.13 (c (n "solana-banks-interface") (v "1.18.13") (d (list (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.18.13") (d #t) (k 0)) (d (n "tarpc") (r "^0.29.0") (f (quote ("full"))) (d #t) (k 0)))) (h "08lbbdhj7jz0f4490ldrhlsw5rfn3b85djfvmps2y4nivdir59ri")))

(define-public crate-solana-banks-interface-1.18.14 (c (n "solana-banks-interface") (v "1.18.14") (d (list (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.18.14") (d #t) (k 0)) (d (n "tarpc") (r "^0.29.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1i5qkvdc5vzrbqf4nwcmlimp6wbvhs5w3gckd798b1v00p29mgsi")))

(define-public crate-solana-banks-interface-1.17.34 (c (n "solana-banks-interface") (v "1.17.34") (d (list (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.34") (d #t) (k 0)) (d (n "tarpc") (r "^0.29.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1abif715kvlzq95i7lp3059h3xhnmpfdy9k3hb6796lh8f9r66x6")))

