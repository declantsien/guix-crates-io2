(define-module (crates-io so la solana_libra_crypto-derive) #:use-module (crates-io))

(define-public crate-solana_libra_crypto-derive-0.0.0-sol9 (c (n "solana_libra_crypto-derive") (v "0.0.0-sol9") (d (list (d (n "quote") (r "^0.6.13") (d #t) (k 0)) (d (n "syn") (r "^0.15.39") (f (quote ("derive"))) (d #t) (k 0)))) (h "0w0winfwb4fa0v58mppza93q0bh5m8hymymg5n8brpnxm3fvygqa") (y #t)))

(define-public crate-solana_libra_crypto-derive-0.0.0-sol10 (c (n "solana_libra_crypto-derive") (v "0.0.0-sol10") (d (list (d (n "quote") (r "^0.6.13") (d #t) (k 0)) (d (n "syn") (r "^0.15.39") (f (quote ("derive"))) (d #t) (k 0)))) (h "0vd28125qdxmccixvv5jyswn2mcqr4lmisdl29i0xlz5wb3a19bj") (y #t)))

(define-public crate-solana_libra_crypto-derive-0.0.0-sol11 (c (n "solana_libra_crypto-derive") (v "0.0.0-sol11") (d (list (d (n "quote") (r "^0.6.13") (d #t) (k 0)) (d (n "syn") (r "^0.15.39") (f (quote ("derive"))) (d #t) (k 0)))) (h "1v2lg2sm3zlck1g2n43ggsqqcwjjcw1g9xaqp1mhiwcml60p0c8w") (y #t)))

(define-public crate-solana_libra_crypto-derive-0.0.0-sol12 (c (n "solana_libra_crypto-derive") (v "0.0.0-sol12") (d (list (d (n "quote") (r "^0.6.13") (d #t) (k 0)) (d (n "syn") (r "^0.15.39") (f (quote ("derive"))) (d #t) (k 0)))) (h "1wfb1zfac0gp1jb4vsrnjpx30nysfm61dzyf70qjg6b2p2cbdgh5") (y #t)))

(define-public crate-solana_libra_crypto-derive-0.0.0-sol13 (c (n "solana_libra_crypto-derive") (v "0.0.0-sol13") (d (list (d (n "quote") (r "^0.6.13") (d #t) (k 0)) (d (n "syn") (r "^0.15.39") (f (quote ("derive"))) (d #t) (k 0)))) (h "19pd0iq0nz9vbqs2ckxna5sczvrgc7qb64d49dyvg7ghiflp26zg") (y #t)))

(define-public crate-solana_libra_crypto-derive-0.0.0-sol14 (c (n "solana_libra_crypto-derive") (v "0.0.0-sol14") (d (list (d (n "quote") (r "^0.6.13") (d #t) (k 0)) (d (n "syn") (r "^0.15.39") (f (quote ("derive"))) (d #t) (k 0)))) (h "1ni42n50japiprfv1i7vrzcw4appfg2lbma390jbzngwybmma4sb") (y #t)))

(define-public crate-solana_libra_crypto-derive-0.0.0-sol15 (c (n "solana_libra_crypto-derive") (v "0.0.0-sol15") (d (list (d (n "quote") (r "^0.6.13") (d #t) (k 0)) (d (n "syn") (r "^0.15.39") (f (quote ("derive"))) (d #t) (k 0)))) (h "0qcj9j9qzhvn8y1wbgw5spdxyvk6qjk4wi21nlizw0w03cgy0wwa")))

(define-public crate-solana_libra_crypto-derive-0.0.0 (c (n "solana_libra_crypto-derive") (v "0.0.0") (d (list (d (n "quote") (r "^0.6.13") (d #t) (k 0)) (d (n "syn") (r "^0.15.39") (f (quote ("derive"))) (d #t) (k 0)))) (h "0rwb57c7p9z6flhi6y1fgjq4wsanqmr4bqdwqnknal2bsl2fh53j")))

(define-public crate-solana_libra_crypto-derive-0.0.1-sol4 (c (n "solana_libra_crypto-derive") (v "0.0.1-sol4") (d (list (d (n "failure") (r "^0.0.1-sol4") (d #t) (k 2) (p "solana_libra_failure_ext")) (d (n "proc-macro2") (r "^1.0.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1vinxdj4y00pddgsc2mxxn3l5kj9ivjlh654q97ylnjnc89fdl8p")))

(define-public crate-solana_libra_crypto-derive-0.0.1-sol5 (c (n "solana_libra_crypto-derive") (v "0.0.1-sol5") (d (list (d (n "failure") (r "^0.0.1-sol5") (d #t) (k 2) (p "solana_libra_failure_ext")) (d (n "proc-macro2") (r "^1.0.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1cfqvhham3v354g8gizlp2dlpypapimi4zd7r8q0vvmdns2i1037")))

