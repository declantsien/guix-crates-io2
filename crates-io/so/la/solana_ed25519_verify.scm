(define-module (crates-io so la solana_ed25519_verify) #:use-module (crates-io))

(define-public crate-solana_ed25519_verify-0.1.0 (c (n "solana_ed25519_verify") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "arrayref") (r "^0.3") (d #t) (k 0)) (d (n "curve25519-dalek") (r "^4") (d #t) (k 0)) (d (n "getrandom") (r "^0.1") (f (quote ("dummy"))) (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)) (d (n "solana-program") (r "^1.18") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.18") (d #t) (k 2)) (d (n "solana-zk-token-sdk") (r "^1.18") (d #t) (k 0)))) (h "0xcnja2ymgk6va0pzwbb2jf0ckn5z6gcbzyvckshnvyaxb7z8z1m")))

(define-public crate-solana_ed25519_verify-0.1.1 (c (n "solana_ed25519_verify") (v "0.1.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "arrayref") (r "^0.3") (d #t) (k 0)) (d (n "curve25519-dalek") (r "^4") (d #t) (k 0)) (d (n "getrandom") (r "^0.1") (f (quote ("dummy"))) (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)) (d (n "solana-program") (r "^1.18") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.18") (d #t) (k 2)) (d (n "solana-zk-token-sdk") (r "^1.18") (d #t) (k 0)))) (h "1y81ajlvh5p54p3s9cagj62fl0gzyqf5q0a3gg10wpb3f2l16ap6")))

