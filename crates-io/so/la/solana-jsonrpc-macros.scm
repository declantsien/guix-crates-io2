(define-module (crates-io so la solana-jsonrpc-macros) #:use-module (crates-io))

(define-public crate-solana-jsonrpc-macros-0.1.0 (c (n "solana-jsonrpc-macros") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "solana-jsonrpc-core") (r "^0.1") (d #t) (k 0)) (d (n "solana-jsonrpc-pubsub") (r "^0.1") (d #t) (k 0)) (d (n "solana-jsonrpc-tcp-server") (r "^0.1") (d #t) (k 2)))) (h "02q20p9jhyl1xjhliccwmwmy63jp863niy928bds75215x8avv9i") (y #t)))

(define-public crate-solana-jsonrpc-macros-0.1.1 (c (n "solana-jsonrpc-macros") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "solana-jsonrpc-core") (r "^0.1") (d #t) (k 0)) (d (n "solana-jsonrpc-pubsub") (r "^0.1") (d #t) (k 0)) (d (n "solana-jsonrpc-tcp-server") (r "^0.1") (d #t) (k 2)))) (h "1z9k4fdd9p71z60pyy37pa79zfqlvrb3b995xwgj96rqy49ca4vb") (y #t)))

(define-public crate-solana-jsonrpc-macros-0.1.2 (c (n "solana-jsonrpc-macros") (v "0.1.2") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "solana-jsonrpc-core") (r "^0.1") (d #t) (k 0)) (d (n "solana-jsonrpc-pubsub") (r "^0.1") (d #t) (k 0)) (d (n "solana-jsonrpc-tcp-server") (r "^0.1") (d #t) (k 2)))) (h "0kp8im061z16ij80ap1p0d18dq81czq3h97dqnkg369psr1sqhc8") (y #t)))

(define-public crate-solana-jsonrpc-macros-0.1.3 (c (n "solana-jsonrpc-macros") (v "0.1.3") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "solana-jsonrpc-core") (r "^0.1") (d #t) (k 0)) (d (n "solana-jsonrpc-pubsub") (r "^0.1") (d #t) (k 0)) (d (n "solana-jsonrpc-tcp-server") (r "^0.1") (d #t) (k 2)))) (h "03m296vizfb3bs02ivd03lv5sdxcfd0vjpv9x65pwvln13a453cb") (y #t)))

(define-public crate-solana-jsonrpc-macros-0.2.0 (c (n "solana-jsonrpc-macros") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "solana-jsonrpc-core") (r "^0.2.0") (d #t) (k 0)) (d (n "solana-jsonrpc-pubsub") (r "^0.2.0") (d #t) (k 0)) (d (n "solana-jsonrpc-tcp-server") (r "^0.2.0") (d #t) (k 2)))) (h "1nv4xnlyx712hmhzmlxqxc8x32fc3y3d2c8f14pd6gfvbp66bn8n") (y #t)))

(define-public crate-solana-jsonrpc-macros-0.3.0 (c (n "solana-jsonrpc-macros") (v "0.3.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "solana-jsonrpc-core") (r "^0.3.0") (d #t) (k 0)) (d (n "solana-jsonrpc-pubsub") (r "^0.3.0") (d #t) (k 0)) (d (n "solana-jsonrpc-tcp-server") (r "^0.3.0") (d #t) (k 2)))) (h "04d5v1619nvrbs93sml9jnfvi4cj0vpahh8s49vjp8vgpvghmblq") (y #t)))

(define-public crate-solana-jsonrpc-macros-0.4.0 (c (n "solana-jsonrpc-macros") (v "0.4.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "solana-jsonrpc-core") (r "^0.4.0") (d #t) (k 0)) (d (n "solana-jsonrpc-pubsub") (r "^0.4.0") (d #t) (k 0)) (d (n "solana-jsonrpc-tcp-server") (r "^0.4.0") (d #t) (k 2)))) (h "1fy9ywvj8wjhma4iqy7fxdiq7p0zb25waga2jnmmy7y1kr4dycfz") (y #t)))

