(define-module (crates-io so la solana_idl) #:use-module (crates-io))

(define-public crate-solana_idl-0.0.1 (c (n "solana_idl") (v "0.0.1") (d (list (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)))) (h "10a42x0j06frj5yzn4x2i14c2v6f7wjqyb5pjcq4hddnzpxyg68b")))

(define-public crate-solana_idl-0.0.2 (c (n "solana_idl") (v "0.0.2") (d (list (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)))) (h "14hglr8ff3bqcd0km8ng1m5xkc1qww8jfj3pjc7h5zw33m3w89s5")))

(define-public crate-solana_idl-0.0.3 (c (n "solana_idl") (v "0.0.3") (d (list (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)))) (h "0ggw00z3j9gy24kfi95h7l37yacvj4mx8s5bi1vxy47yfqmsxm9x")))

(define-public crate-solana_idl-0.0.4 (c (n "solana_idl") (v "0.0.4") (d (list (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)))) (h "1x50i2hxasi95lqcddag4691imrxs6rjl3f3zymf6cz1wq7v3sfb")))

