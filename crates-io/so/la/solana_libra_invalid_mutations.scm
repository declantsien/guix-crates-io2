(define-module (crates-io so la solana_libra_invalid_mutations) #:use-module (crates-io))

(define-public crate-solana_libra_invalid_mutations-0.0.0-sol14 (c (n "solana_libra_invalid_mutations") (v "0.0.0-sol14") (d (list (d (n "proptest") (r "^0.9") (d #t) (k 0)) (d (n "proptest_helpers") (r "^0.0.0-sol14") (d #t) (k 0) (p "solana_libra_proptest_helpers")) (d (n "vm") (r "^0.0.0-sol14") (d #t) (k 0) (p "solana_libra_vm")))) (h "02rpgy3r7kmkp8fwvg2alzvgwmhh160h8jrdagr38696nbsic98w") (y #t)))

(define-public crate-solana_libra_invalid_mutations-0.0.0-sol15 (c (n "solana_libra_invalid_mutations") (v "0.0.0-sol15") (d (list (d (n "proptest") (r "^0.9") (d #t) (k 0)) (d (n "proptest_helpers") (r "^0.0.0-sol15") (d #t) (k 0) (p "solana_libra_proptest_helpers")) (d (n "vm") (r "^0.0.0-sol15") (d #t) (k 0) (p "solana_libra_vm")))) (h "08rinn3g0mcd061amci7zinka5z6f2bwcg0s70i22bska6kfdrpq")))

(define-public crate-solana_libra_invalid_mutations-0.0.0 (c (n "solana_libra_invalid_mutations") (v "0.0.0") (d (list (d (n "proptest") (r "^0.9") (d #t) (k 0)) (d (n "proptest_helpers") (r "^0.0.0") (d #t) (k 0) (p "solana_libra_proptest_helpers")) (d (n "vm") (r "^0.0.0") (d #t) (k 0) (p "solana_libra_vm")))) (h "1ijdkx4pnjbq0hs6w189dqx1kpz1bmiipxc7lslv8x0cpl6nm7kw")))

(define-public crate-solana_libra_invalid_mutations-0.0.1-sol4 (c (n "solana_libra_invalid_mutations") (v "0.0.1-sol4") (d (list (d (n "proptest") (r "^0.9") (d #t) (k 0)) (d (n "solana_libra_proptest_helpers") (r "^0.0.1-sol4") (d #t) (k 0)) (d (n "solana_libra_types") (r "^0.0.1-sol4") (d #t) (k 0)) (d (n "solana_libra_vm") (r "^0.0.1-sol4") (d #t) (k 0)) (d (n "solana_libra_vm") (r "^0.0.1-sol4") (f (quote ("testing"))) (d #t) (k 2)))) (h "0107czbrzx7ldwfl8z51sf6pnb074amf7l7jf4dljh34478ljvwl")))

(define-public crate-solana_libra_invalid_mutations-0.0.1-sol5 (c (n "solana_libra_invalid_mutations") (v "0.0.1-sol5") (d (list (d (n "proptest") (r "^0.9") (d #t) (k 0)) (d (n "solana_libra_proptest_helpers") (r "^0.0.1-sol5") (d #t) (k 0)) (d (n "solana_libra_types") (r "^0.0.1-sol5") (d #t) (k 0)) (d (n "solana_libra_vm") (r "^0.0.1-sol5") (d #t) (k 0)) (d (n "solana_libra_vm") (r "^0.0.1-sol5") (f (quote ("testing"))) (d #t) (k 2)))) (h "0rbpqfkzfd42vwimfq63jfisk7byh78p52ns8lgnj71s5ysm6ap2")))

