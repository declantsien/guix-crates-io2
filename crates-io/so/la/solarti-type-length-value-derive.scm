(define-module (crates-io so la solarti-type-length-value-derive) #:use-module (crates-io))

(define-public crate-solarti-type-length-value-derive-0.1.0 (c (n "solarti-type-length-value-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1q967b7m45az59vs63clbhc079cp3r7ks6zak4gc7j26l3mb6dxl")))

(define-public crate-solarti-type-length-value-derive-0.3.1 (c (n "solarti-type-length-value-derive") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1mkm8vl12nccyxjqm633clqv1ca5wc424ab3n5nbad6p1hbpi0n4")))

(define-public crate-solarti-type-length-value-derive-0.3.2 (c (n "solarti-type-length-value-derive") (v "0.3.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0k60q2j90gyskacsnw9xc31mcs28mpsjpfhhrcf8vjxxkm3w4l5n")))

(define-public crate-solarti-type-length-value-derive-0.3.3 (c (n "solarti-type-length-value-derive") (v "0.3.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1g9ikf9hfl9v60av31nna19p7pmzk7nh4v1s30h7xzlk8pmnxdp5")))

(define-public crate-solarti-type-length-value-derive-0.3.4 (c (n "solarti-type-length-value-derive") (v "0.3.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0343k9vsrff49a4h3j8kicqfhdl0q38l8qfpv089gbx5lv4zxh8n")))

(define-public crate-solarti-type-length-value-derive-0.3.5 (c (n "solarti-type-length-value-derive") (v "0.3.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1bm5153x7wajxfikxd35114rh05zjmvzcyyvwmrm0ypqcvz7dhjn")))

