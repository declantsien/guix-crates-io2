(define-module (crates-io so la solana_libra_prost_ext) #:use-module (crates-io))

(define-public crate-solana_libra_prost_ext-0.0.1-sol3 (c (n "solana_libra_prost_ext") (v "0.0.1-sol3") (d (list (d (n "bytes") (r "^0.4.12") (d #t) (k 0)) (d (n "prost") (r "^0.5.0") (d #t) (k 0)))) (h "1pf98nhr6lzny7cv7n8j3xw34abv9za7568ihp61vy2dzaahkmgk")))

(define-public crate-solana_libra_prost_ext-0.0.1-sol4 (c (n "solana_libra_prost_ext") (v "0.0.1-sol4") (d (list (d (n "bytes") (r "^0.4.12") (d #t) (k 0)) (d (n "prost") (r "^0.5.0") (d #t) (k 0)))) (h "0whdllawpdcvqnks74cc5dhl632fzwxd1wiy0lk8wvwd214zx7hk")))

(define-public crate-solana_libra_prost_ext-0.0.1-sol5 (c (n "solana_libra_prost_ext") (v "0.0.1-sol5") (d (list (d (n "bytes") (r "^0.4.12") (d #t) (k 0)) (d (n "prost") (r "^0.5.0") (d #t) (k 0)))) (h "1agw8kspp5kbkclfimn8qf8qi2zdkvxq9bm2imd89m2zxn7n4s4s")))

