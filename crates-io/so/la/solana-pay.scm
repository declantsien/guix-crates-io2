(define-module (crates-io so la solana-pay) #:use-module (crates-io))

(define-public crate-solana-pay-0.1.0 (c (n "solana-pay") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "solana-program") (r ">=1.4.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (f (quote ("serde"))) (d #t) (k 0)))) (h "1vp7zbs8b3zj6znlzckvjsk5a76za4kniycwc8jp0hwzn1xl97h5")))

(define-public crate-solana-pay-0.1.1 (c (n "solana-pay") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "solana-program") (r ">=1.4.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (f (quote ("serde"))) (d #t) (k 0)))) (h "0jd96bp5bhdf0x3gshckwz17f2flv9gcz2piwvyrqh98bqz0lkch")))

