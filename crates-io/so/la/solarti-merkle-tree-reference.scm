(define-module (crates-io so la solarti-merkle-tree-reference) #:use-module (crates-io))

(define-public crate-solarti-merkle-tree-reference-0.1.0 (c (n "solarti-merkle-tree-reference") (v "0.1.0") (d (list (d (n "solana-program") (r "^1.10.33") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "12j9g8a2vqbkvk3cagzgbb1mfg4xs8kshxw5r3bb77g1l94x615j")))

(define-public crate-solarti-merkle-tree-reference-0.1.1 (c (n "solarti-merkle-tree-reference") (v "0.1.1") (d (list (d (n "miraland-program") (r "=1.14.17-rc4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1x9v51g3qc1f8p6ajr8wd32w794hr89qpv4q7lwakqz87shvhc2f")))

(define-public crate-solarti-merkle-tree-reference-0.1.2 (c (n "solarti-merkle-tree-reference") (v "0.1.2") (d (list (d (n "miraland-program") (r "=1.14.18") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "09vfnbklgnm7bvwvxvmlfc9cy0v1clh3gsihlxg47g12j03r0r5k")))

(define-public crate-solarti-merkle-tree-reference-0.1.3 (c (n "solarti-merkle-tree-reference") (v "0.1.3") (d (list (d (n "miraland-program") (r "^1.14.18") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "169hmka9ap9iklh0j2krc7z4hivkcbsn8rf2s4w9plwa14y524my")))

(define-public crate-solarti-merkle-tree-reference-0.1.4 (c (n "solarti-merkle-tree-reference") (v "0.1.4") (d (list (d (n "miraland-program") (r ">=1.18.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0gqss6v73m1iw4qg5br39c1xqrf7k106y9q4arvifpiacdj5kqg5")))

