(define-module (crates-io so la solana-jsonrpc-pubsub) #:use-module (crates-io))

(define-public crate-solana-jsonrpc-pubsub-0.1.0 (c (n "solana-jsonrpc-pubsub") (v "0.1.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "parking_lot") (r "^0.6") (d #t) (k 0)) (d (n "solana-jsonrpc-core") (r "^0.1") (d #t) (k 0)) (d (n "solana-jsonrpc-tcp-server") (r "^0.1") (d #t) (k 2)))) (h "0kjhipyrqcnjx0xwqdrmk9ysh133xd39c26jbssciw5vsff6hvil") (y #t)))

(define-public crate-solana-jsonrpc-pubsub-0.1.1 (c (n "solana-jsonrpc-pubsub") (v "0.1.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "parking_lot") (r "^0.6") (d #t) (k 0)) (d (n "solana-jsonrpc-core") (r "^0.1") (d #t) (k 0)) (d (n "solana-jsonrpc-tcp-server") (r "^0.1") (d #t) (k 2)))) (h "0srzwb0qdzmla0anhgqjail5w89iyfxcfnb7zmw5dyaa574cj9jj") (y #t)))

(define-public crate-solana-jsonrpc-pubsub-0.1.2 (c (n "solana-jsonrpc-pubsub") (v "0.1.2") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "parking_lot") (r "^0.6") (d #t) (k 0)) (d (n "solana-jsonrpc-core") (r "^0.1") (d #t) (k 0)) (d (n "solana-jsonrpc-tcp-server") (r "^0.1") (d #t) (k 2)))) (h "1bv6hkxn74n5jsa91n9159if5dlc2cryqf3wqj3wpl4bgsv59fkx") (y #t)))

(define-public crate-solana-jsonrpc-pubsub-0.1.3 (c (n "solana-jsonrpc-pubsub") (v "0.1.3") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "parking_lot") (r "^0.6") (d #t) (k 0)) (d (n "solana-jsonrpc-core") (r "^0.1") (d #t) (k 0)) (d (n "solana-jsonrpc-tcp-server") (r "^0.1") (d #t) (k 2)))) (h "0530hghzh32aysbrkv0say1fw7vddgw741xs8gpkrjkdychzd8qq") (y #t)))

(define-public crate-solana-jsonrpc-pubsub-0.2.0 (c (n "solana-jsonrpc-pubsub") (v "0.2.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "parking_lot") (r "^0.6") (d #t) (k 0)) (d (n "solana-jsonrpc-core") (r "^0.2.0") (d #t) (k 0)) (d (n "solana-jsonrpc-tcp-server") (r "^0.2.0") (d #t) (k 2)))) (h "0gzjkyhb2m0m6hwhd6x03i56fqrhfprba72h8n7jb41prqv8cr8i") (y #t)))

(define-public crate-solana-jsonrpc-pubsub-0.3.0 (c (n "solana-jsonrpc-pubsub") (v "0.3.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "parking_lot") (r "^0.6") (d #t) (k 0)) (d (n "solana-jsonrpc-core") (r "^0.3.0") (d #t) (k 0)) (d (n "solana-jsonrpc-tcp-server") (r "^0.3.0") (d #t) (k 2)))) (h "0skfi9748jq9y6984b8ix57pc0l0dxsz56bsxh82pbx6c8nlx53h") (y #t)))

(define-public crate-solana-jsonrpc-pubsub-0.4.0 (c (n "solana-jsonrpc-pubsub") (v "0.4.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "parking_lot") (r "^0.6") (d #t) (k 0)) (d (n "solana-jsonrpc-core") (r "^0.4.0") (d #t) (k 0)) (d (n "solana-jsonrpc-tcp-server") (r "^0.4.0") (d #t) (k 2)))) (h "0crrhncnxw930sjv8kil43dpccnsqklaz7ayd0hqqzsvi3kl0sx0") (y #t)))

