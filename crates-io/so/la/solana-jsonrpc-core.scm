(define-module (crates-io so la solana-jsonrpc-core) #:use-module (crates-io))

(define-public crate-solana-jsonrpc-core-0.1.0 (c (n "solana-jsonrpc-core") (v "0.1.0") (d (list (d (n "futures") (r "^0.1.6") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "11bhba06cswbc0x4rmfkm1f1rs40m6kszazzxishi57m9zlp271a") (y #t)))

(define-public crate-solana-jsonrpc-core-0.1.1 (c (n "solana-jsonrpc-core") (v "0.1.1") (d (list (d (n "futures") (r "~0.1.6") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0z3bn465n6sc6rd8yfmq5c3s396dzq72nq66x45vm65g42kjxbal") (y #t)))

(define-public crate-solana-jsonrpc-core-0.1.2 (c (n "solana-jsonrpc-core") (v "0.1.2") (d (list (d (n "futures") (r "~0.1.6") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1p52mnyci1410z66g6g32a1iacafnfralkzklclqris2kixhixf3") (y #t)))

(define-public crate-solana-jsonrpc-core-0.2.0 (c (n "solana-jsonrpc-core") (v "0.2.0") (d (list (d (n "futures") (r "~0.1.6") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0wavjpl845pzgz7f8b1c1jr0psagf0v7p7zmja2mlr2h089xkmac") (y #t)))

(define-public crate-solana-jsonrpc-core-0.3.0 (c (n "solana-jsonrpc-core") (v "0.3.0") (d (list (d (n "futures") (r "~0.1.6") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "19aamcj04q0svjim3430y0ba3n3ilx4bpjf7ig2yhz6w2kcrwzi4") (y #t)))

(define-public crate-solana-jsonrpc-core-0.4.0 (c (n "solana-jsonrpc-core") (v "0.4.0") (d (list (d (n "futures") (r "~0.1.6") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "16wpb5smfm8ghy7mk8w2ikg0jqlv6yx6qab7rki7w58ljvijgvrs") (y #t)))

