(define-module (crates-io so la solana-nostd-entrypoint) #:use-module (crates-io))

(define-public crate-solana-nostd-entrypoint-0.1.0 (c (n "solana-nostd-entrypoint") (v "0.1.0") (d (list (d (n "arrayvec") (r "^0.7.4") (k 0)) (d (n "solana-program") (r "^1.18.1") (d #t) (k 0)) (d (n "solana-program-test") (r "^1.18.1") (d #t) (k 2)) (d (n "solana-sdk") (r "^1.18.1") (d #t) (k 2)) (d (n "tokio") (r "^1.35.1") (f (quote ("full"))) (d #t) (k 2)))) (h "051gvlk1591yd1sm2dyy7fp217h5l04v4bb9hmjvfk45kgrp9ikl") (f (quote (("example-program"))))))

(define-public crate-solana-nostd-entrypoint-0.2.0 (c (n "solana-nostd-entrypoint") (v "0.2.0") (d (list (d (n "arrayvec") (r "^0.7.4") (k 0)) (d (n "solana-program") (r "^1.18.1") (d #t) (k 0)))) (h "19q9wm199sch6n2s4pzcz9p98bq2dmmnaiick29hqp9q208n6k1i")))

(define-public crate-solana-nostd-entrypoint-0.3.0 (c (n "solana-nostd-entrypoint") (v "0.3.0") (d (list (d (n "arrayvec") (r "^0.7.4") (k 0)) (d (n "solana-program") (r "^1.18.1") (d #t) (k 0)))) (h "17qrrqpzx1im84i90ri99n6yp9qr3a7lg8plwfk5y5970hsq7s3q")))

