(define-module (crates-io so la solana-cargo-test-bpf) #:use-module (crates-io))

(define-public crate-solana-cargo-test-bpf-1.4.5 (c (n "solana-cargo-test-bpf") (v "1.4.5") (d (list (d (n "cargo_metadata") (r "^0.12.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)))) (h "08xijbrj8r42c7d9yryybnqsi8p8dgic1sd092w05f53ckggs2pa") (y #t)))

(define-public crate-solana-cargo-test-bpf-1.4.6 (c (n "solana-cargo-test-bpf") (v "1.4.6") (d (list (d (n "cargo_metadata") (r "^0.12.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)))) (h "03273vsdmv2fb7454xzbvc16b2xsg1prbadi8184syd12qlr1h8y") (y #t)))

(define-public crate-solana-cargo-test-bpf-1.4.7 (c (n "solana-cargo-test-bpf") (v "1.4.7") (d (list (d (n "cargo_metadata") (r "^0.12.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)))) (h "1w1j59rb96x71z8whq94chy2f7xnnmfrcn7vfxrgyhk1053cf35n") (y #t)))

(define-public crate-solana-cargo-test-bpf-1.4.8 (c (n "solana-cargo-test-bpf") (v "1.4.8") (d (list (d (n "cargo_metadata") (r "^0.12.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)))) (h "0wib7lpil3jnkrvj83nq462jra30mzx022k0f4qxra1799hd153n") (y #t)))

(define-public crate-solana-cargo-test-bpf-1.4.9 (c (n "solana-cargo-test-bpf") (v "1.4.9") (d (list (d (n "cargo_metadata") (r "^0.12.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)))) (h "1bkq7q474bhfvm4j6ky6c0hsk4xsjfzam7nbkxj3vbqrna7p2qc3") (y #t)))

