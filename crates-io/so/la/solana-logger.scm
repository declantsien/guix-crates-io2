(define-module (crates-io so la solana-logger) #:use-module (crates-io))

(define-public crate-solana-logger-0.11.0 (c (n "solana-logger") (v "0.11.0") (d (list (d (n "env_logger") (r "^0.6.0") (d #t) (k 0)))) (h "187w6zb4hzmbgiybrv97bghcs5m3ffg5awji08dgx44d75f4lirj")))

(define-public crate-solana-logger-0.12.0 (c (n "solana-logger") (v "0.12.0") (d (list (d (n "env_logger") (r "^0.6.1") (d #t) (k 0)))) (h "04aq6sf6qlhp30q21vka3wj231d26425g288wp0yh7pfmh8684gr")))

(define-public crate-solana-logger-0.12.1 (c (n "solana-logger") (v "0.12.1") (d (list (d (n "env_logger") (r "^0.6.1") (d #t) (k 0)))) (h "02dsq7d11vp943hd5izjpb5djbfbix67b1f8wwqnzk19gvxpgq78")))

(define-public crate-solana-logger-0.12.2 (c (n "solana-logger") (v "0.12.2") (d (list (d (n "env_logger") (r "^0.6.1") (d #t) (k 0)))) (h "1s9aff8n9k80qnljfv0vi4f57i2p7kb92rny5ghicp1mms3iy3c8")))

(define-public crate-solana-logger-0.12.3 (c (n "solana-logger") (v "0.12.3") (d (list (d (n "env_logger") (r "^0.6.1") (d #t) (k 0)))) (h "127kp2s0759k0ri2li552c7gxgma98gzwigzahxrmpcjxl6cd3j3")))

(define-public crate-solana-logger-0.13.1 (c (n "solana-logger") (v "0.13.1") (d (list (d (n "env_logger") (r "^0.6.1") (d #t) (k 0)))) (h "0g1lh7bphdkn1mcd0pmc88q8vyfwyid58jxb8l2khijxr5czifgl")))

(define-public crate-solana-logger-0.13.2 (c (n "solana-logger") (v "0.13.2") (d (list (d (n "env_logger") (r "^0.6.1") (d #t) (k 0)))) (h "127qa56lbskzlqcmy8lh00npmx58w1r1jmapa3539y3xg7qwqgp9")))

(define-public crate-solana-logger-0.14.0 (c (n "solana-logger") (v "0.14.0") (d (list (d (n "env_logger") (r "^0.6.1") (d #t) (k 0)))) (h "0blpjwypmwlsiaa3qk8gld71cb186zc3d08f0jjc6kil5gy2p8vh")))

(define-public crate-solana-logger-0.14.1 (c (n "solana-logger") (v "0.14.1") (d (list (d (n "env_logger") (r "^0.6.1") (d #t) (k 0)))) (h "1fnhn20fd621qzzyrqs5y1gq333j30cm781rf845830zvf1dpc3k")))

(define-public crate-solana-logger-0.14.2 (c (n "solana-logger") (v "0.14.2") (d (list (d (n "env_logger") (r "^0.6.1") (d #t) (k 0)))) (h "13abkpv5802acyp7068l1i9v8ab9h39v997qfcnlj4gcwqypshqp")))

(define-public crate-solana-logger-0.15.0 (c (n "solana-logger") (v "0.15.0") (d (list (d (n "env_logger") (r "^0.6.1") (d #t) (k 0)))) (h "07k5c1sh586c9xkjhhrxlqv01wji3r3d4svl5lbc8ls0r95m5rmy")))

(define-public crate-solana-logger-0.15.1 (c (n "solana-logger") (v "0.15.1") (d (list (d (n "env_logger") (r "^0.6.1") (d #t) (k 0)))) (h "18awgvnpkjxnz8c554jairzp7smxcvjprixwi1pkkz4mvxpkarwx")))

(define-public crate-solana-logger-0.15.2 (c (n "solana-logger") (v "0.15.2") (d (list (d (n "env_logger") (r "^0.6.1") (d #t) (k 0)))) (h "1jm0ndm803k9fvsmzy1q403azx3gd74vl0wc288q90d9i28svic7")))

(define-public crate-solana-logger-0.15.3 (c (n "solana-logger") (v "0.15.3") (d (list (d (n "env_logger") (r "^0.6.1") (d #t) (k 0)))) (h "0w4c0jga71vgs7apb4s1ql677lqnlbsz0l4xnl33aa29js7jd99c")))

(define-public crate-solana-logger-0.16.0 (c (n "solana-logger") (v "0.16.0") (d (list (d (n "env_logger") (r "^0.6.1") (d #t) (k 0)))) (h "001xl2110kdm6d0j6awqr1fl4yk2barlhs9rwyhiz60cdnh47vyj")))

(define-public crate-solana-logger-0.16.1 (c (n "solana-logger") (v "0.16.1") (d (list (d (n "env_logger") (r "^0.6.1") (d #t) (k 0)))) (h "1yl9lfm4c9v31bsdvqjqlkcmai82d700gdr8cx2g2gkyq4awcvg4")))

(define-public crate-solana-logger-0.16.2 (c (n "solana-logger") (v "0.16.2") (d (list (d (n "env_logger") (r "^0.6.1") (d #t) (k 0)))) (h "1q6y0rwxf4p0sf4khsfx3kcxsr87nzs5dl31l6ifngsfzpgsws6p")))

(define-public crate-solana-logger-0.16.3 (c (n "solana-logger") (v "0.16.3") (d (list (d (n "env_logger") (r "^0.6.1") (d #t) (k 0)))) (h "1qzmvyicmmdlpiyqbyzpxnhxy97kzb0v0hyk6amz1g2mbiz5ihl9")))

(define-public crate-solana-logger-0.16.4 (c (n "solana-logger") (v "0.16.4") (d (list (d (n "env_logger") (r "^0.6.1") (d #t) (k 0)))) (h "1imb7lj4vlcfy8rmx6g8rwwj66ziq1fd713lgixfl5i7war89l71")))

(define-public crate-solana-logger-0.16.5 (c (n "solana-logger") (v "0.16.5") (d (list (d (n "env_logger") (r "^0.6.1") (d #t) (k 0)))) (h "12y87r2zx81bv9wgphq58ri2pdnw78x2qnjdzhhayw49yg27h6ki")))

(define-public crate-solana-logger-0.16.6 (c (n "solana-logger") (v "0.16.6") (d (list (d (n "env_logger") (r "^0.6.1") (d #t) (k 0)))) (h "1z129axbqy2lgcx76x5z2v4vld0fc75g862h4van6p65my0sz7xa")))

(define-public crate-solana-logger-0.17.0 (c (n "solana-logger") (v "0.17.0") (d (list (d (n "env_logger") (r "^0.6.2") (d #t) (k 0)))) (h "0dblbi4pplnpg6d9n7s3ascbqfy73l3sl3f273x42ds11kiq2pjp")))

(define-public crate-solana-logger-0.17.1 (c (n "solana-logger") (v "0.17.1") (d (list (d (n "env_logger") (r "^0.6.2") (d #t) (k 0)))) (h "002915sbp80a7n0j5fv31rm2ka40y5qc867d5iisslf0wkjwc8q6")))

(define-public crate-solana-logger-0.18.0-pre0 (c (n "solana-logger") (v "0.18.0-pre0") (d (list (d (n "env_logger") (r "^0.6.2") (d #t) (k 0)))) (h "0knahxdhp1jwxv3xa55flpcm7m8505scbl238y33k0220wvyrlnf")))

(define-public crate-solana-logger-0.18.0-pre1 (c (n "solana-logger") (v "0.18.0-pre1") (d (list (d (n "env_logger") (r "^0.6.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "12qkwwiy001vy2j2q5471czpgdcwzqb6py100frmav1cm3rz5062")))

(define-public crate-solana-logger-0.17.2 (c (n "solana-logger") (v "0.17.2") (d (list (d (n "env_logger") (r "^0.6.2") (d #t) (k 0)))) (h "18kiw4sl705lf3vv29g850l6x6h1g8yirx91pwp6f6z758iq40x8")))

(define-public crate-solana-logger-0.18.0-pre2 (c (n "solana-logger") (v "0.18.0-pre2") (d (list (d (n "env_logger") (r "^0.6.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "00qg95pwi3rnc275mhx4cmm3fyq0vki4cdn3kj9a3bfmxgqpwqix")))

(define-public crate-solana-logger-0.18.0 (c (n "solana-logger") (v "0.18.0") (d (list (d (n "env_logger") (r "^0.6.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "04lax7s85f6mx2i9xmayrxqa4wh98q4d3ja1qryn7f447vxmicsf")))

(define-public crate-solana-logger-0.18.1 (c (n "solana-logger") (v "0.18.1") (d (list (d (n "env_logger") (r "^0.6.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "1hcj9l9andszg0ncx8scipv6698n6dvhzsssa1cgnsnh0xxbzq99")))

(define-public crate-solana-logger-0.19.1 (c (n "solana-logger") (v "0.19.1") (d (list (d (n "env_logger") (r "^0.6.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "0vbwvd1646nnjjd5vsc5br1jmxj0hk33ip9kwqsk8kyg4z7sqi2v")))

(define-public crate-solana-logger-0.20.1 (c (n "solana-logger") (v "0.20.1") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "17w3jw1jm78hzr9sd9x4qddggy3478y4lh4bjdcp9glj58g37vri")))

(define-public crate-solana-logger-0.20.2 (c (n "solana-logger") (v "0.20.2") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "1qvwbzwd6jn5avdf67shckscad5d18ycnwplkpcyqcnncjlhdy2p")))

(define-public crate-solana-logger-0.20.3 (c (n "solana-logger") (v "0.20.3") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "0da0j8ajjyl08lw7b18rl7syjfphvzxh1i4y647hah0kgvr2dwjs")))

(define-public crate-solana-logger-0.20.4 (c (n "solana-logger") (v "0.20.4") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "12sqrwkhn058q9zprdihmxdm9680725s3nvpa9yg536hakqp7yz7")))

(define-public crate-solana-logger-0.20.5 (c (n "solana-logger") (v "0.20.5") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "0k7csjv2h59ach2s0jn096zl31dsgw1x7xg0g08v36mh3l03m9q8")))

(define-public crate-solana-logger-0.21.0 (c (n "solana-logger") (v "0.21.0") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "18m2nxdmxkadpc5f5gqbczbbmm10g0kqdb97vca8lhkgj5f3xlra")))

(define-public crate-solana-logger-0.21.1 (c (n "solana-logger") (v "0.21.1") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "0d8w0wb32dqxyd13rrwb6991vkfxzc1dwgn7m4pp77sfcj9jli36")))

(define-public crate-solana-logger-0.21.3 (c (n "solana-logger") (v "0.21.3") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "1581ngxpk2lf5c3g59yqkbnzvf4m4mnfjql8p4cpcg6mr5zv9y7g")))

(define-public crate-solana-logger-0.21.4 (c (n "solana-logger") (v "0.21.4") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "0s0q7id11dwhr94gmzgmxcc0n2kc2hjj8l1v68fq9ryxrwz2pnf6")))

(define-public crate-solana-logger-0.21.5 (c (n "solana-logger") (v "0.21.5") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "0023djbh750x1k4rnsym6jilx10f7wnzw2rkm5ibih4j9pgd85c5")))

(define-public crate-solana-logger-0.22.0 (c (n "solana-logger") (v "0.22.0") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "12f2hwcwv4y7xnj879hk4b5sfaz8vv30pa4m5fymlfc78afas4cg")))

(define-public crate-solana-logger-0.21.6 (c (n "solana-logger") (v "0.21.6") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "14x0z9yphk3h01fpiiy0xckix6v48dk4k84ihyk0n0j8i4wlv8pg")))

(define-public crate-solana-logger-0.22.1 (c (n "solana-logger") (v "0.22.1") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "07bfn4941c7mlgfipwqgjmz7vyk25hm7gw247d2cyxvm93fkggg2")))

(define-public crate-solana-logger-0.22.2 (c (n "solana-logger") (v "0.22.2") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "1kyhx4s0h8ml96diivf77d8rmcllksnkd4pzwjg1cnd7i96m22nf")))

(define-public crate-solana-logger-0.21.7 (c (n "solana-logger") (v "0.21.7") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "1hwwwbznz6n6sn9niinagwc0asp4qv9b1fi8cfswd17vi1cravyk")))

(define-public crate-solana-logger-0.22.3 (c (n "solana-logger") (v "0.22.3") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "0nbw2dp2zzn7689kn89gwl44kfppaf6azpyri2rr4pbmh6q44wbz")))

(define-public crate-solana-logger-0.22.4 (c (n "solana-logger") (v "0.22.4") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "16r13l5k07q100k375c24mqfd2gzs4md8qdw8aly88x70szwzsqv")))

(define-public crate-solana-logger-0.23.0 (c (n "solana-logger") (v "0.23.0") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "093xa3p6y9cxk4nn7qgi7q5p0yzcfsskf9v7bzs9pcysr03g8c6k")))

(define-public crate-solana-logger-0.22.5 (c (n "solana-logger") (v "0.22.5") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "1qhqvykxlppbfksn7nb1a1sgl8l8yg1k4sycvx4qp0jxk8wsz4bx")))

(define-public crate-solana-logger-0.22.6 (c (n "solana-logger") (v "0.22.6") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "1jv50q26sfpgcnix0lzcss5nmfwr4m7s839mbbic8dndv6a2l8yy")))

(define-public crate-solana-logger-0.23.1 (c (n "solana-logger") (v "0.23.1") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "1qd1w8zq9bcwb5f3x55w924p17iyyc3xamnnwdkyq2h77z51ji0f")))

(define-public crate-solana-logger-0.23.2 (c (n "solana-logger") (v "0.23.2") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "1nkr7slrdz8k4vjd3ir03w1b5mincm5rpciichm1p4p6b17vkj1y")))

(define-public crate-solana-logger-0.22.7 (c (n "solana-logger") (v "0.22.7") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "1f3v9lw1hf1ckrc8d3vcdnjc5cqnva035408lkybm4wc53jkgl2p")))

(define-public crate-solana-logger-0.23.3 (c (n "solana-logger") (v "0.23.3") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "088l8g85knlr6z11i9wdaab4k7f4ias1527px1ypvp2xv3il766v")))

(define-public crate-solana-logger-0.23.4 (c (n "solana-logger") (v "0.23.4") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "1nbrkf4hahj5afpmj0b51jkqlz3h25434rvgrknz86xk3vbw78fz")))

(define-public crate-solana-logger-0.22.8 (c (n "solana-logger") (v "0.22.8") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "1m8i63y7111lqbk04frf32al8kd55rbrycx6nxyinpz4v2lax2a3")))

(define-public crate-solana-logger-0.23.5 (c (n "solana-logger") (v "0.23.5") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "1vx22j0mb4mq7gx3q9lv51gckbrx159a508zw6vcvrb1188ws34j")))

(define-public crate-solana-logger-0.23.6 (c (n "solana-logger") (v "0.23.6") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "1q1yrmgfkd0psifc5nlzhzwg1pca1gw60y76d50yfpnhm0c16bgj")))

(define-public crate-solana-logger-1.0.0 (c (n "solana-logger") (v "1.0.0") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "1s0d3dmcgvpxmrkl8zifa7zynrd2l1pbrdv57xi2z84yvldwvc9w")))

(define-public crate-solana-logger-0.23.7 (c (n "solana-logger") (v "0.23.7") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "1cpqcd9nc3glv9kcffj0a5gry0r3xli88nniw5yczqb2f4csvs4p")))

(define-public crate-solana-logger-0.23.8 (c (n "solana-logger") (v "0.23.8") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "1vp6x3drbjhv7z711lqk9zrn15qprf8x1v6lx9zy73245g1dip9b")))

(define-public crate-solana-logger-1.0.1 (c (n "solana-logger") (v "1.0.1") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "197skm6s1db052gjcg9kgxfmr6vq0r59bibh7vn2adybwvlknp6d")))

(define-public crate-solana-logger-1.0.2 (c (n "solana-logger") (v "1.0.2") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "072mzcpr62lfs54zdpzc3nppwrz4wazvzkawf9qjz4zjw6rwsx4r")))

(define-public crate-solana-logger-1.0.3 (c (n "solana-logger") (v "1.0.3") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "1c7ynf5r0ava376a6i093gfdj3lm6sj0yy5ysj49xgwaf58jqisy")))

(define-public crate-solana-logger-1.0.4 (c (n "solana-logger") (v "1.0.4") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "1l7vimp2ngn4vbmyfi2skjh35a91880dcrh4g4m7x1k35nvccrd5")))

(define-public crate-solana-logger-1.0.5 (c (n "solana-logger") (v "1.0.5") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "1ix7x2ra0id5cl491sf4jlag15yacsi7rn7k6pfliyzci24ickjb")))

(define-public crate-solana-logger-1.0.6 (c (n "solana-logger") (v "1.0.6") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "0jki54rvy7v757m1dmwmxfp62i4v9359ldpppsbp0jzg53jn38vp")))

(define-public crate-solana-logger-1.0.7 (c (n "solana-logger") (v "1.0.7") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "1bk35ax7lbr4vallp50hhr4fnq95xi1mmyx87hpcfm2ry8vyxaqr")))

(define-public crate-solana-logger-1.0.8 (c (n "solana-logger") (v "1.0.8") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "1c4kzs4bc0r1kwzsizpb12lw2647kv75b4qq2fcvpqn1c4nc1kx7")))

(define-public crate-solana-logger-1.0.9 (c (n "solana-logger") (v "1.0.9") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "0wfk8zjw9jgm6sx3p2y3w3rak8gv9ys5zb9gmh627z13cf5arapm")))

(define-public crate-solana-logger-1.0.10 (c (n "solana-logger") (v "1.0.10") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "0d1a59c6hg8dckr5fx3pmk1apinbd85x74jjm2gqgi8fhqzcwibj")))

(define-public crate-solana-logger-1.0.11 (c (n "solana-logger") (v "1.0.11") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "11knzw9n7jrd4ckawbbcr56y92b3s0g4mmkq4s038s1rfd9i9x57")))

(define-public crate-solana-logger-1.0.12 (c (n "solana-logger") (v "1.0.12") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "1qv3c2z0jdldkmc9wjv8kpizxq6gxw2s3w5ab203j23pj9nnw8lr")))

(define-public crate-solana-logger-1.1.0 (c (n "solana-logger") (v "1.1.0") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "0bidmmn4gddmnb906lf36bls4l1sp9zcn55691f6s0y9ql9p6bxy")))

(define-public crate-solana-logger-1.1.1 (c (n "solana-logger") (v "1.1.1") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "0x1wakzacvl9pzmsmqqywxcgkpk5ilplb5452kjvafnav2z3mfmm")))

(define-public crate-solana-logger-1.0.13 (c (n "solana-logger") (v "1.0.13") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "09p95ahcrqa0aicxf4m1sy095jigxrihk14d6zsskv8kah62ghsh")))

(define-public crate-solana-logger-1.1.2 (c (n "solana-logger") (v "1.1.2") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "12b01k0sk7yxd8r7iah2asm13dnc2a31wq990kzl08cj33nagz5b")))

(define-public crate-solana-logger-1.0.14 (c (n "solana-logger") (v "1.0.14") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "124ry57rw3cnf64c0wnrpm19ndlqik5zanw5p9bglg86jhq2sw7c")))

(define-public crate-solana-logger-1.0.15 (c (n "solana-logger") (v "1.0.15") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "178lxs1ipqc3dw86jc7wawkw4wv2l6j67bhqab60x97hv7c1xica")))

(define-public crate-solana-logger-1.0.16 (c (n "solana-logger") (v "1.0.16") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "0hiq9z0pa2qhhwzf99mkm5i6hq2vx93qk0zw79dvjl4jjvrdn0gz")))

(define-public crate-solana-logger-1.1.3 (c (n "solana-logger") (v "1.1.3") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "11lfxnayh2w8fdk8pbgy122kggg0s4riaf056xgg8d24py2f7aan")))

(define-public crate-solana-logger-1.0.17 (c (n "solana-logger") (v "1.0.17") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "1rfr0bj9bayzkdy9dcx371fib5xwgr8spjly23ic387nikbpa0m5")))

(define-public crate-solana-logger-1.1.5 (c (n "solana-logger") (v "1.1.5") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "0mpcjhgs12jah2ykiiwirva2nw5fzpyz6q1bwrpddyz9cmc3xvx2")))

(define-public crate-solana-logger-1.1.6 (c (n "solana-logger") (v "1.1.6") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "15hab20gpdw1cg77sfhs1pzzd5v6mrd8a305ad3sf8swd96p0032")))

(define-public crate-solana-logger-1.1.7 (c (n "solana-logger") (v "1.1.7") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "060jh44mg09fwjkh322s1ixc1kpz9hxxdcpvvcc1hhdmg2vmimc9")))

(define-public crate-solana-logger-1.0.20 (c (n "solana-logger") (v "1.0.20") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "0f3sc45a4pqb2af1x3vsbjbcskf3xg4bxzjmiw3cbqiaixl0m2k8")))

(define-public crate-solana-logger-1.0.21 (c (n "solana-logger") (v "1.0.21") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "0azdb8ahmpafkhx9m9d6pydwxj87y0ak96i6g81yzdmr1sw9pcpy")))

(define-public crate-solana-logger-1.1.8 (c (n "solana-logger") (v "1.1.8") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "02l1bdi2l2qkz0izmx0fhvjia8q6clfd2vggqhbcm5br8llzp5sv")))

(define-public crate-solana-logger-1.1.9 (c (n "solana-logger") (v "1.1.9") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "0644ghvx4wfk0kg04g9i7m6dy7ngrkjx7my0sjm1r3qi1n0fzr9l")))

(define-public crate-solana-logger-1.1.10 (c (n "solana-logger") (v "1.1.10") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "0vnxfkq7v62klyplb48pajfqwy1zcbn21f6qrxnsrgqmlx2mvxkd")))

(define-public crate-solana-logger-1.0.22 (c (n "solana-logger") (v "1.0.22") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "148s7mr6fc16ad2slqh17chhhzgfajdwhm21idh8g8s70zmlq9nw")))

(define-public crate-solana-logger-1.1.11 (c (n "solana-logger") (v "1.1.11") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "1qsf29rkxyyildlzqix7fbihajsc54ya9mwa9dkbi98xvdsv8zgm")))

(define-public crate-solana-logger-1.1.12 (c (n "solana-logger") (v "1.1.12") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "0kpf7ijxqxvnli6p85ybwp3v2lg5ykr95iq5dbir5fhp72qnrcn4")))

(define-public crate-solana-logger-1.1.13 (c (n "solana-logger") (v "1.1.13") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "0hn3cmajyvw9q7q86rrxq9hnpga57d0jg0zpbdfn9l511x2sljgw")))

(define-public crate-solana-logger-1.0.23 (c (n "solana-logger") (v "1.0.23") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "01vyl27cjvbiicpz1s00yqs7vp6z0v9q371xkx5gvqmh7gr7j0py")))

(define-public crate-solana-logger-1.0.24 (c (n "solana-logger") (v "1.0.24") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "10qjmmld2f62z35p8sjivbncnpm72pxikdx2v26924l6kwzsdnsb")))

(define-public crate-solana-logger-1.2.0 (c (n "solana-logger") (v "1.2.0") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "0pzdwj30msw0khwf64l7v40wawhi9xzqgaazhh79ibm1g2b818wd")))

(define-public crate-solana-logger-1.1.15 (c (n "solana-logger") (v "1.1.15") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "1pbranwnvpc8wdmrqc1caxapgan6l0dlkd5gjzbv27nw20352yp4")))

(define-public crate-solana-logger-1.1.17 (c (n "solana-logger") (v "1.1.17") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "0lh9110xi6r0y363r19pidsgd78x806kn4fpzfdvy5i19lxmpj65")))

(define-public crate-solana-logger-1.2.1 (c (n "solana-logger") (v "1.2.1") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "0wq80w5s8swcx2bijx2avhwjban2lbrp4wzp67l9cvmj8gipk8ld")))

(define-public crate-solana-logger-1.1.18 (c (n "solana-logger") (v "1.1.18") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "0x6n7cppwi177v7v837rksawaxxqk80ak1q6wj47knlji6i6fwaa")))

(define-public crate-solana-logger-1.2.3 (c (n "solana-logger") (v "1.2.3") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "0kp6dc7lbsb3iavk41f0g69xpy760x1yw5k3lp57sy5sz2yq3i65")))

(define-public crate-solana-logger-1.2.4 (c (n "solana-logger") (v "1.2.4") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "1fhs8x1ysy1zzsby9sag2sb1kgk3lr7b940b5yy2x8ppj1x06lly")))

(define-public crate-solana-logger-1.1.19 (c (n "solana-logger") (v "1.1.19") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "15wcgfq8wcyzy2m7bdmahb36w766y74vs44x8dx4i8yp3zj4bi87")))

(define-public crate-solana-logger-1.2.5 (c (n "solana-logger") (v "1.2.5") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "13d0wc02hzg3izi8qhqpd73hvrvp4a591zqlqvcslzkbqxfrid71")))

(define-public crate-solana-logger-1.2.6 (c (n "solana-logger") (v "1.2.6") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "0lgls048q8nc8rgq4w4b74812mr87igy5cjqly59rmvnsm32vfv1")))

(define-public crate-solana-logger-1.2.7 (c (n "solana-logger") (v "1.2.7") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "0l3gmszlip27kq2n3kwgnlzlv34fv8kx5sy5r18r950m00cfi29b")))

(define-public crate-solana-logger-1.2.8 (c (n "solana-logger") (v "1.2.8") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "1plgin4zny7v1g5a86bbgk2lpkpp5hkysf8h35mdgvxa1kmsay3v")))

(define-public crate-solana-logger-1.2.9 (c (n "solana-logger") (v "1.2.9") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "093x857aj3hm6m4mmvdq2xlc09w58yf4y8w6lw1kg7gq74f0b58r")))

(define-public crate-solana-logger-1.2.10 (c (n "solana-logger") (v "1.2.10") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "1xxaswp8jrq4cirddxlp5yq1sivqggyq4rirp71qp98mn2d4bq0g")))

(define-public crate-solana-logger-1.1.20 (c (n "solana-logger") (v "1.1.20") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "1rhc45rhcb59a2vvrfh8bcbv7sr68vp5iafhiybx7bvyflpplfdj")))

(define-public crate-solana-logger-1.2.12 (c (n "solana-logger") (v "1.2.12") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "002i92633kz26fzsm3wik3zrnb33qv5qx20pi6lji1bpg71bdxdp")))

(define-public crate-solana-logger-1.2.13 (c (n "solana-logger") (v "1.2.13") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "140qwxljh0939rc2m5am3lxxaalrksadd94vk97kx4ac7rzmn71f")))

(define-public crate-solana-logger-1.2.14 (c (n "solana-logger") (v "1.2.14") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "1s1j9xwab84szg0w1vpnz5kqa9vw95jxn3f3hs8wahcsr1nd2v2y")))

(define-public crate-solana-logger-1.1.23 (c (n "solana-logger") (v "1.1.23") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "1cxv5a6fbfpd8gawwmnl596c9xjyhbbldw39a60p9bv8nbgrr9md")))

(define-public crate-solana-logger-1.2.15 (c (n "solana-logger") (v "1.2.15") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "1s0q89ziy4csvbv31s0c0iwypnhb1gh29xsd128x7adqzly716nh")))

(define-public crate-solana-logger-1.2.16 (c (n "solana-logger") (v "1.2.16") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "1b8j6yifrg6k71l0khc74a3kxzqj4yz78r264qcapwkx9v3zc08i")))

(define-public crate-solana-logger-1.2.17 (c (n "solana-logger") (v "1.2.17") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "1j6bnkldikvp3f5lc3fx6zs8m73s7a6aqcnzf0zp37byh2r3qw5w")))

(define-public crate-solana-logger-1.2.18 (c (n "solana-logger") (v "1.2.18") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "0kv4x7c3aa4k2wm5sg53r773i9z6xq7ss9843nmb6kc9y1anyxpp")))

(define-public crate-solana-logger-1.2.19 (c (n "solana-logger") (v "1.2.19") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "0qp8b0v7abq6kqi6r2h2c56f30m0g6sb64rmp0vnvkcdb5jhia55")))

(define-public crate-solana-logger-1.2.20 (c (n "solana-logger") (v "1.2.20") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "0zi0pkgdizix9jp4d9yxdfa7kr669inrg2xqgfxmh6m8sj9jhd3d")))

(define-public crate-solana-logger-1.3.0 (c (n "solana-logger") (v "1.3.0") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "0m3870d45igp4962d0ld7qsmraxf0d8lknb022yqk1zqdp9gzbmr")))

(define-public crate-solana-logger-1.3.1 (c (n "solana-logger") (v "1.3.1") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "1957h6fnapqxvgzsbwlczzggd6cwnvw433fdlq8rlqiparlariz4")))

(define-public crate-solana-logger-1.2.21 (c (n "solana-logger") (v "1.2.21") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "0qqvjlj087bpx6afnacggwngchx0znkwmbjsazjbn52a7l0kca44")))

(define-public crate-solana-logger-1.2.22 (c (n "solana-logger") (v "1.2.22") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "03rgajc6zshzsym46ry4b94nav1kcybhy0l2wmlps5snd00n8h2g")))

(define-public crate-solana-logger-1.3.2 (c (n "solana-logger") (v "1.3.2") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "1kbyicfflfk095xdf7dgxj40z9ycikddj56vzkgwgq90sqx232sj")))

(define-public crate-solana-logger-1.2.23 (c (n "solana-logger") (v "1.2.23") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "0qxmjxkbrah0kd1rxdmrnvhn6iqif53vvihdgv6vwmxa8y4y4dkf")))

(define-public crate-solana-logger-1.3.3 (c (n "solana-logger") (v "1.3.3") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "1czjqhd2mnva13w1ks7x5k4fgz4r79g3n5wznhl4ya5ym0j94b4i")))

(define-public crate-solana-logger-1.2.24 (c (n "solana-logger") (v "1.2.24") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "1kqvvvzfjqibjdqxv9walrfaai8ibami5cvx774vgqc6rjrv7cwi")))

(define-public crate-solana-logger-1.2.25 (c (n "solana-logger") (v "1.2.25") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "07sa14z5izbfipa81v3dmvzp1aic8qx8k3m275bv4n64gfsagcwx")))

(define-public crate-solana-logger-1.3.4 (c (n "solana-logger") (v "1.3.4") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "1rn1jbxmq2kfdh4vh97dc7ymp41y2fshvx5v3bwnnlrh1jpjg3bw")))

(define-public crate-solana-logger-1.2.26 (c (n "solana-logger") (v "1.2.26") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "1xdy4pcgrchhiaq36whcd11xz3m9qbhczj5iwbr7wy3764wy4mr1")))

(define-public crate-solana-logger-1.3.5 (c (n "solana-logger") (v "1.3.5") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "0vap6cfw22rwlsqxhnaimld29lzgvbnpma0sgawjcw02wzn1r7p7")))

(define-public crate-solana-logger-1.3.6 (c (n "solana-logger") (v "1.3.6") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "1r2rxkffqnj3sm80hqlhywxx0k6m80plwyhi84fj7ix9fhkzybp7")))

(define-public crate-solana-logger-1.3.7 (c (n "solana-logger") (v "1.3.7") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "15vxzcxv5x50m03xd29w5fdkz62hc2xkjxjgcmh5x0vrpmijh9av")))

(define-public crate-solana-logger-1.2.27 (c (n "solana-logger") (v "1.2.27") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "199dlsyjqczy9cd6mngfgs8gci7wgxf9kp5did341rz2a9jlp4q9")))

(define-public crate-solana-logger-1.3.8 (c (n "solana-logger") (v "1.3.8") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "17qddp5b7pha86yfgc7s7v9jnw6a55d748wkf8ci9wcjnc83s64g")))

(define-public crate-solana-logger-1.2.28 (c (n "solana-logger") (v "1.2.28") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "0yakk807nicv40sfn2111dgynqi29k8n33vaxf0nd7nfxa3rqjzn")))

(define-public crate-solana-logger-1.3.9 (c (n "solana-logger") (v "1.3.9") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "1bf5fnvbbnrc9dqaig4jjwvmm56x2ppscwszbyv0pgswr6mkig38")))

(define-public crate-solana-logger-1.3.10 (c (n "solana-logger") (v "1.3.10") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "18k8dls6gcapn7i6266b2i86b3c9j727cdwk2dw4rddwdqil7499")))

(define-public crate-solana-logger-1.3.11 (c (n "solana-logger") (v "1.3.11") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "1dj1y7112amvsmgk641g5hdd32hzhx7a1d5rzndbqn9cnygihias")))

(define-public crate-solana-logger-1.2.29 (c (n "solana-logger") (v "1.2.29") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "1cvfpiin6fdk5i2vjfqyg87c6pxpnxi0gpcwyswpqh5ama7qxj8h")))

(define-public crate-solana-logger-1.3.12 (c (n "solana-logger") (v "1.3.12") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "1f612rcxyjk9q0n89r728r3yl90w68hz9sks1qin679dq8aidyhh")))

(define-public crate-solana-logger-1.3.13 (c (n "solana-logger") (v "1.3.13") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "0xgbz96jaqbl3gk0w5km9s684yawx1xr9fny6hdzd5wb061yfi52")))

(define-public crate-solana-logger-1.2.30 (c (n "solana-logger") (v "1.2.30") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "09pj77255slsnpwhwwy3lw9a35mcj1h6dn4pv1as2arp6kxnqc2i")))

(define-public crate-solana-logger-1.3.14 (c (n "solana-logger") (v "1.3.14") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "179br938isy2ay78b93wlqzxxrnjiyhapm44lr01k72j142mlzlj")))

(define-public crate-solana-logger-1.2.31 (c (n "solana-logger") (v "1.2.31") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "1qa15f27qzp6j2s4fn9a312v365n07i3di57dg2b6s8y155020w6")))

(define-public crate-solana-logger-1.2.32 (c (n "solana-logger") (v "1.2.32") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "0c688wlq4684q2i9jnw997l58m9g5jkm85rlyfj34jivq3qzvb2l")))

(define-public crate-solana-logger-1.3.15 (c (n "solana-logger") (v "1.3.15") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "1z4xr3ab6z6kd89j4bqgx64xpn7xiw3hsnbqkqpvvyndhmb779fj")))

(define-public crate-solana-logger-1.3.16 (c (n "solana-logger") (v "1.3.16") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "0mhvgppv5q6zbphklbp3a8xhn2j0sb5pawqky1ra0nj18c5llgsq")))

(define-public crate-solana-logger-1.3.17 (c (n "solana-logger") (v "1.3.17") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "08kmm154llirbls6danj57hzwn6n399yvrnq8b76w0253yq7g7km")))

(define-public crate-solana-logger-1.4.0 (c (n "solana-logger") (v "1.4.0") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "1ljgk9rld60zp0ns4wgdk2ywd8c75y5f1wi6s1kjsl2d1c2rrvri")))

(define-public crate-solana-logger-1.4.1 (c (n "solana-logger") (v "1.4.1") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "0k2ld5666ac840yk2k0lkbhn8nxmdwm21mzhzapzvg305s0si4da")))

(define-public crate-solana-logger-1.3.18 (c (n "solana-logger") (v "1.3.18") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "0r4h46lbnanw5b5mpq2i54h2vlw6bmmv44yjkdvxm2fzfb98343c")))

(define-public crate-solana-logger-1.3.19 (c (n "solana-logger") (v "1.3.19") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "024m18fl70jpfql17b72ymp0j4axawzsy4r1hdx8gyf1fvwvcqx1")))

(define-public crate-solana-logger-1.4.2 (c (n "solana-logger") (v "1.4.2") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "19mjhdx4w3yr9272bavk4bxpq1sn8pya9h4dlgkm3wwvl4z7pc2x")))

(define-public crate-solana-logger-1.4.3 (c (n "solana-logger") (v "1.4.3") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "0h8hsgmk8r0y4slijblmmfg90jc9kscmxvznqgsmp9a6dp153wpq")))

(define-public crate-solana-logger-1.4.4 (c (n "solana-logger") (v "1.4.4") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "04h000wl57vqlg9mmlj8b7h2kjyy1kqnwd5xm6mzdch24a3d3rnj")))

(define-public crate-solana-logger-1.4.5 (c (n "solana-logger") (v "1.4.5") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "0pvb4ppahzzf42k9b2yiq5c9qgfar3hz20x3laybdz5h22ai139d")))

(define-public crate-solana-logger-1.4.6 (c (n "solana-logger") (v "1.4.6") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "16vzc1aa781w10zn8s0gcxm05xpfrdq18vbjimaf20p7q39v5mr5")))

(define-public crate-solana-logger-1.3.20 (c (n "solana-logger") (v "1.3.20") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "04wcirx1yx5zdqk3p4b1k7frqzd14a4a1izxk2j5dg8i7wmkpgwy")))

(define-public crate-solana-logger-1.4.7 (c (n "solana-logger") (v "1.4.7") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "06hgq1hvpyhvf03bqb7lmg9240x8p722wyhkacb1kgqmh8rdzlxz")))

(define-public crate-solana-logger-1.3.21 (c (n "solana-logger") (v "1.3.21") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "11xyxymgbva4p6h1r7yrjxldfgrfhq7m3ggwklnyh3141vzbyq0l")))

(define-public crate-solana-logger-1.4.8 (c (n "solana-logger") (v "1.4.8") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "1lhpx9zfi0dm4fzlna1b4hpwqisiqg9zb31jhlp1f4g2js0944k1")))

(define-public crate-solana-logger-1.4.9 (c (n "solana-logger") (v "1.4.9") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "0hvnywa4xps9djl1ihi9md9l28va5h8ng50wnrjxpm6qf2lcjbd4")))

(define-public crate-solana-logger-1.4.10 (c (n "solana-logger") (v "1.4.10") (d (list (d (n "env_logger") (r ">=0.7.1, <0.8.0") (d #t) (k 0)) (d (n "lazy_static") (r ">=1.4.0, <2.0.0") (d #t) (k 0)) (d (n "log") (r ">=0.4.8, <0.5.0") (d #t) (k 0)))) (h "0z1m03l4x3dwdxgic4gff9jsy1dxj03q2srcfb6i54wpjir8bzpk")))

(define-public crate-solana-logger-1.4.11 (c (n "solana-logger") (v "1.4.11") (d (list (d (n "env_logger") (r ">=0.7.1, <0.8.0") (d #t) (k 0)) (d (n "lazy_static") (r ">=1.4.0, <2.0.0") (d #t) (k 0)) (d (n "log") (r ">=0.4.8, <0.5.0") (d #t) (k 0)))) (h "1afan5l9iiblygy97g9ssrm4vlmj1r9my937l35lhf0xz0353aar")))

(define-public crate-solana-logger-1.4.12 (c (n "solana-logger") (v "1.4.12") (d (list (d (n "env_logger") (r ">=0.7.1, <0.8.0") (d #t) (k 0)) (d (n "lazy_static") (r ">=1.4.0, <2.0.0") (d #t) (k 0)) (d (n "log") (r ">=0.4.8, <0.5.0") (d #t) (k 0)))) (h "17s27x6h2dk9mbv1qzn1zgv895q41m4val5bw16j34sl988dvgxj")))

(define-public crate-solana-logger-1.3.23 (c (n "solana-logger") (v "1.3.23") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "0mlv9bsjl6nqb4k000iad8x9vigx7pyn2fg15q4g8jn7fzv91r0s")))

(define-public crate-solana-logger-1.4.13 (c (n "solana-logger") (v "1.4.13") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "1f5kk6zzlj1l85j9zvzmnvjwix4q6q9xwsa7y7dzkixvivm3syky")))

(define-public crate-solana-logger-1.4.14 (c (n "solana-logger") (v "1.4.14") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "0sg76s5sn9jpiw1fxpz3qi8hgafj843xj9xlb1bsygmjs1br350d")))

(define-public crate-solana-logger-1.4.15 (c (n "solana-logger") (v "1.4.15") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "07r1cq1ml9dvhdihixk4hk4iic65xhd4i5cnhaa1dbd9ld3qmhdw")))

(define-public crate-solana-logger-1.4.16 (c (n "solana-logger") (v "1.4.16") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "00ian45pq6vdvkcs7x0fjlllgm94fkm88f8h1qmss666ax1h5hb5")))

(define-public crate-solana-logger-1.4.17 (c (n "solana-logger") (v "1.4.17") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "0scbn35dgpqi4jx62ka2nb8xzrj4v9m0c6qlldkiyz2fn5d0b5qb")))

(define-public crate-solana-logger-1.5.0 (c (n "solana-logger") (v "1.5.0") (d (list (d (n "env_logger") (r "^0.8.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "085fx34v1f4h44nqk201yr8fb2j11bm9wnwpmsmqrn9bxkbffasc")))

(define-public crate-solana-logger-1.4.18 (c (n "solana-logger") (v "1.4.18") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "1yq2lzdysz81rkv6qyvg3fvz2xfscfrbdpbi8j5al6xwwnpkwlkn")))

(define-public crate-solana-logger-1.4.19 (c (n "solana-logger") (v "1.4.19") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "1r8jwl6k3vi902v79a02h49rpi1xawmibymla9sphafp51w44imz")))

(define-public crate-solana-logger-1.4.20 (c (n "solana-logger") (v "1.4.20") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "094gzwwwws6nv31bmq3p7n3rgznm326pg1sqcydcmnwn7lvscjl1")))

(define-public crate-solana-logger-1.5.1 (c (n "solana-logger") (v "1.5.1") (d (list (d (n "env_logger") (r "^0.8.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "0gdjk67jhcfq6illaz1r28dgzrdmsk5nwx6f1v83c4q0grnnx2hq")))

(define-public crate-solana-logger-1.4.21 (c (n "solana-logger") (v "1.4.21") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "0x0sc518flfsyc5c5s7z394pymwmpdpwfb21d76xbjqiyq4dpi4k")))

(define-public crate-solana-logger-1.4.22 (c (n "solana-logger") (v "1.4.22") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "11j296m88fv360960b6dsfcr9ba15zsbjw81c8f1pfb9wpxlbsn9")))

(define-public crate-solana-logger-1.5.2 (c (n "solana-logger") (v "1.5.2") (d (list (d (n "env_logger") (r "^0.8.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "0qvb683xw5jgnv0nplfhdjvrisi3vqb3w6imz6wq5kn11sf0cdcg")))

(define-public crate-solana-logger-1.4.23 (c (n "solana-logger") (v "1.4.23") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "1i7pgc6b445h3fw6vb4i594lhrjvrdpm1ahn6zadfwmzawdxwhz6")))

(define-public crate-solana-logger-1.5.3 (c (n "solana-logger") (v "1.5.3") (d (list (d (n "env_logger") (r "^0.8.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "07fkl9rz2zsswpz46i2wrnd37hq206kdfgp1wmnf722vyd4fhg6a")))

(define-public crate-solana-logger-1.5.4 (c (n "solana-logger") (v "1.5.4") (d (list (d (n "env_logger") (r "^0.8.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "09knqyzjzf42ysqrzpjij533mqijfjdn1smz8dr3myn3jbljvsbx")))

(define-public crate-solana-logger-1.5.5 (c (n "solana-logger") (v "1.5.5") (d (list (d (n "env_logger") (r "^0.8.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "0im5p2cqq10rb8pby7gq1aqlarm1sbxkh034gxls9zgns8ang95p")))

(define-public crate-solana-logger-1.4.25 (c (n "solana-logger") (v "1.4.25") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "0syb7xbn1xchkq1iy9993jf6b4p02qp14h39paynnxk9mfjj05im")))

(define-public crate-solana-logger-1.4.26 (c (n "solana-logger") (v "1.4.26") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "1ikc0ydzqfdw2q5jpjpv6vwss365h55y18x39hr6kcg5iyyfpfwh")))

(define-public crate-solana-logger-1.5.6 (c (n "solana-logger") (v "1.5.6") (d (list (d (n "env_logger") (r "^0.8.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "0as0wlqplsxqail7zmqgp8rm1xm0v1zyji61pdw858y8kzcl9sdc")))

(define-public crate-solana-logger-1.4.27 (c (n "solana-logger") (v "1.4.27") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "06fpch7xxvqg6qk0hzd4l0mgh6ii5ll51wqq4lz1w5pxys5xss6m")))

(define-public crate-solana-logger-1.5.7 (c (n "solana-logger") (v "1.5.7") (d (list (d (n "env_logger") (r "^0.8.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "0ikzpn5wgrcs5kjacgrwspm2i7icpvd0q4lni2llydc0ry1a7zb9")))

(define-public crate-solana-logger-1.5.8 (c (n "solana-logger") (v "1.5.8") (d (list (d (n "env_logger") (r "^0.8.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "08adfg7k3dbbz6a89xwzf8fdgz6khcrmfhai8yj18mfsjxnh0fnq")))

(define-public crate-solana-logger-1.4.28 (c (n "solana-logger") (v "1.4.28") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "13p3yhsx87w76d2mbl1anc1xydczv5xxam3nmjd1npf3kybk2s3m")))

(define-public crate-solana-logger-1.5.9 (c (n "solana-logger") (v "1.5.9") (d (list (d (n "env_logger") (r "^0.8.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "0mp9dc34r89drzr6qslyl545rlasnaf6pgk8rz5kpsnngr0swsnd") (y #t)))

(define-public crate-solana-logger-1.5.10 (c (n "solana-logger") (v "1.5.10") (d (list (d (n "env_logger") (r "^0.8.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "0rrz6vd9wib1msmcfw363sp78yw44zgwfkggvkzzr0sz93yqkpdk")))

(define-public crate-solana-logger-1.5.11 (c (n "solana-logger") (v "1.5.11") (d (list (d (n "env_logger") (r "^0.8.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "1b23zzja4c5gcaqzwb6qvc6lh31031izlsh1ibcllmshig9nlxyz")))

(define-public crate-solana-logger-1.5.12 (c (n "solana-logger") (v "1.5.12") (d (list (d (n "env_logger") (r "^0.8.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "0pk5zrf60qrnlx7n8158c3zprfvz3qj60l4sq1xlhc5d6hhbcjsc")))

(define-public crate-solana-logger-1.5.13 (c (n "solana-logger") (v "1.5.13") (d (list (d (n "env_logger") (r "^0.8.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "0bf74jssbg4gsnvabj1w8j3pp5n558s1v4kj7mp9a6h8anykb9vc")))

(define-public crate-solana-logger-1.5.14 (c (n "solana-logger") (v "1.5.14") (d (list (d (n "env_logger") (r "^0.8.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "01pi2bhhfa0prr435v8q8kavl9jsrrc9j499psglcscqvnmip6bh")))

(define-public crate-solana-logger-1.6.0 (c (n "solana-logger") (v "1.6.0") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "07kgi24glm09a3wdcazk6hh7nhmchl3d7sxjki2pk4ch0g2yh9rn") (y #t)))

(define-public crate-solana-logger-1.5.15 (c (n "solana-logger") (v "1.5.15") (d (list (d (n "env_logger") (r "^0.8.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "10hz844qdj2kxwywx1rzchs7lsvxqzjx8pdrmn8x63arimw78dd4")))

(define-public crate-solana-logger-1.6.1 (c (n "solana-logger") (v "1.6.1") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "0875slp3xx7k7ml4yd2k344a1zgakisq8bcjys4w4b44w7y1qpbd")))

(define-public crate-solana-logger-1.5.16 (c (n "solana-logger") (v "1.5.16") (d (list (d (n "env_logger") (r "^0.8.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "011dar9x3c9j523qs6gkv02mayw7mi8zivk69xg5pddbsb3r4sra")))

(define-public crate-solana-logger-1.5.17 (c (n "solana-logger") (v "1.5.17") (d (list (d (n "env_logger") (r "^0.8.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "1ap600q6gvhn9gj4knydvvz51hb1wxibpm55md2zjppkvkfrra6j")))

(define-public crate-solana-logger-1.6.2 (c (n "solana-logger") (v "1.6.2") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "0lf9biznc2m2dlr282i94wa00fqwhl7qih43y9i2ss12vvyq3b5c")))

(define-public crate-solana-logger-1.5.18 (c (n "solana-logger") (v "1.5.18") (d (list (d (n "env_logger") (r "^0.8.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "107xlfyhsch80yxyaqfchz18fh0m22hjwas8aacm6cy00s2j86c3")))

(define-public crate-solana-logger-1.6.3 (c (n "solana-logger") (v "1.6.3") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "1b8wnzqql2k615wjwla8kxpagnjq06w1zlrzgb3jlv24wz2sardw")))

(define-public crate-solana-logger-1.6.4 (c (n "solana-logger") (v "1.6.4") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "0n6h1j6f3bgmc18zpbsrhn4613dwsr3rpbzl7rp6rhl9ggfvv9vz")))

(define-public crate-solana-logger-1.6.5 (c (n "solana-logger") (v "1.6.5") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "01rmac9drc5dlibb7xlw492cvj0xwp4i48kv4w0dyl80q5vs4xbx")))

(define-public crate-solana-logger-1.6.6 (c (n "solana-logger") (v "1.6.6") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "19zg7kwbgnbi4dhdzy4ayq4ldnsh350xyd2sz669izp5ya4zl5rw")))

(define-public crate-solana-logger-1.5.19 (c (n "solana-logger") (v "1.5.19") (d (list (d (n "env_logger") (r "^0.8.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "1krcqqy1zcvbzgg8v738lx7viv64c3zvkm9428mmz4z76sb4wgpj")))

(define-public crate-solana-logger-1.6.7 (c (n "solana-logger") (v "1.6.7") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "0mym8jp94y8ypvhqw1gl0wqnnqf55wvhgycdpjndg168w9zv1v1g")))

(define-public crate-solana-logger-1.6.8 (c (n "solana-logger") (v "1.6.8") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "0j3b6pfjp9450k0pzfcjjp1nwc6y0mczkravl87gw0s6n0qray0i")))

(define-public crate-solana-logger-1.6.9 (c (n "solana-logger") (v "1.6.9") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "1wjj09rfyddwwjq98cw6qragfds90f0hwn866hs5aa0qf9n3qfjw")))

(define-public crate-solana-logger-1.6.10 (c (n "solana-logger") (v "1.6.10") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "0zz7xz9ni130ffc3afkf8idbjdy1y8kqri2q1f3k96vqbka4zxiz")))

(define-public crate-solana-logger-1.6.11 (c (n "solana-logger") (v "1.6.11") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "0ban6v2x9qlail6cw7anl5krp661xkh07cqxsg9nsdfafg6mwm01")))

(define-public crate-solana-logger-1.7.0 (c (n "solana-logger") (v "1.7.0") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "0yxn52pb61lhxbvvr74r0yykzfh1qld58npgx4fv9dybpla2nglb")))

(define-public crate-solana-logger-1.7.1 (c (n "solana-logger") (v "1.7.1") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "1ip54hh6l2psq62sr638g6wb9g6qrxrkfjwjb5wg8ab630p979by")))

(define-public crate-solana-logger-1.6.12 (c (n "solana-logger") (v "1.6.12") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "0lzbpkhjyg6s4q5r5v1wyl1ga4ikgxmgjzk5lvdcwfzpf07h2qaa")))

(define-public crate-solana-logger-1.6.13 (c (n "solana-logger") (v "1.6.13") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "1gx99f8gaf9c6480449612jvhbh2sa5r9k7zcp8mg2vs0fwk2a9r")))

(define-public crate-solana-logger-1.7.2 (c (n "solana-logger") (v "1.7.2") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "1jx7dkp5d7qr25fm9082gy66iw0zsfhqsqb1qnvagvxk6b8hn37b")))

(define-public crate-solana-logger-1.6.14 (c (n "solana-logger") (v "1.6.14") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "0nsskx15gif7x1965l6sz34bsq8bnx7b4nxwc5nwqnkcd261pbcc")))

(define-public crate-solana-logger-1.7.3 (c (n "solana-logger") (v "1.7.3") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "1knpvxbv9hdz2d49dfgnsi4ciqgqsh32d7fjv20idxh22s9f9y32")))

(define-public crate-solana-logger-1.6.15 (c (n "solana-logger") (v "1.6.15") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "0x1m1f69pyz1580p3ki6k90yv7d3nsv11shzbfqlyiii87kymdzx")))

(define-public crate-solana-logger-1.7.4 (c (n "solana-logger") (v "1.7.4") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "18ywx9wfjnxqj5xwqbvccyyfj3x1g359yf7mq5lzfx8m44asvyks")))

(define-public crate-solana-logger-1.6.16 (c (n "solana-logger") (v "1.6.16") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "18bhyvg55dxhbcyax79ph2ga0ai2y9mrw8p0h8zfwbdfi62w52rg")))

(define-public crate-solana-logger-1.6.17 (c (n "solana-logger") (v "1.6.17") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "1awyf50dkbk3i9qbn6rrq540785kxdgypflfqabaq1sp9v57wzyl")))

(define-public crate-solana-logger-1.7.5 (c (n "solana-logger") (v "1.7.5") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "0m2dizm4scvp8b6qsvzxd561ii76zbdxsdjb9l0f7gpi8j6icgzq")))

(define-public crate-solana-logger-1.7.6 (c (n "solana-logger") (v "1.7.6") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "1gwvy38qvdzwg69xgn93nx6jczy625brzhc8z915133zwm7m2z7c")))

(define-public crate-solana-logger-1.6.18 (c (n "solana-logger") (v "1.6.18") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "1lx6lnb3jlm0rj9fcafqkax3y6cqyzgaf82h7bhlh87k5glipyi7")))

(define-public crate-solana-logger-1.6.19 (c (n "solana-logger") (v "1.6.19") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "1rkfdm7851sfpnd0jb05jhfq5rmlk4kfzg5qqjv6355viwid0r5y")))

(define-public crate-solana-logger-1.7.7 (c (n "solana-logger") (v "1.7.7") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "1gif46n885qzsjxff99awrfawgq7qyxryb2c3fjs44l7278lnncp")))

(define-public crate-solana-logger-1.7.8 (c (n "solana-logger") (v "1.7.8") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "1ivpmx8skd7fpjanfl87q0d5p8dzhafvm0v5pjczrh10nmcikbw0")))

(define-public crate-solana-logger-1.6.20 (c (n "solana-logger") (v "1.6.20") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "09l5mx855nfqalg9bpksf86ar7fy1hvi3rgfl2ygibmwa2g7smhc")))

(define-public crate-solana-logger-1.7.9 (c (n "solana-logger") (v "1.7.9") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "1kcc7i2y7l5xq5fmgsdx345srsrwh2pc4n42wdjahrq1yd53m0j6")))

(define-public crate-solana-logger-1.7.10 (c (n "solana-logger") (v "1.7.10") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "06657kdzh1fg3jkhdgk6dwhrzz2cllgics43bv6k794mzk1n5naw")))

(define-public crate-solana-logger-1.6.21 (c (n "solana-logger") (v "1.6.21") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "19qj1isy6d0gnfs5s2wbnppq9gxm991dn4kif27x5ri9vykqhjcz")))

(define-public crate-solana-logger-1.6.22 (c (n "solana-logger") (v "1.6.22") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "0i64rhdj4yh8wmpzrgqmc10jsprp16hmm5jl4jnaxlraqa3z2zls")))

(define-public crate-solana-logger-1.7.11 (c (n "solana-logger") (v "1.7.11") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "151yz975gwyiwvnj2lzk85591lyimhxgs2hbzydm47jjrj87584q")))

(define-public crate-solana-logger-1.6.23 (c (n "solana-logger") (v "1.6.23") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "1w6c0gf8axfilwx26b43vbvyyf5rdcskrbf8xqmfps87k7bfg0v5")))

(define-public crate-solana-logger-1.6.24 (c (n "solana-logger") (v "1.6.24") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "171hfm1splc8bga3aan8akng679axmrfnk2pqbybwsw9z144r131")))

(define-public crate-solana-logger-1.6.25 (c (n "solana-logger") (v "1.6.25") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "0gqvila6nwir2d1yqrx6bk61zr4za9jaazdjy15yik8zifar56ma")))

(define-public crate-solana-logger-1.7.12 (c (n "solana-logger") (v "1.7.12") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "1vbgsmh8j8qf7nk69ndpb2rfbbcdn7px6bl0iq98qmnd2bjylhsg")))

(define-public crate-solana-logger-1.6.26 (c (n "solana-logger") (v "1.6.26") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "1kajy2aq8v544brlz1mbaliq0fkv1n7xw44mzha27pmzx4i5sbky")))

(define-public crate-solana-logger-1.6.27 (c (n "solana-logger") (v "1.6.27") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "1vf0hshr6dggssw94j3kkcqqw7x1xg62kp9vmi2wzvg0q1zxrnr8")))

(define-public crate-solana-logger-1.7.13 (c (n "solana-logger") (v "1.7.13") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "0hjs5cm3dqpz52d823hkillcfl17q7vjc92fry3y6qqs4pg4s8h1")))

(define-public crate-solana-logger-1.7.14 (c (n "solana-logger") (v "1.7.14") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "1mhkypc0w52mzcsi26w0gyiszz8a74w179plzfhi6dhdd89fi79x")))

(define-public crate-solana-logger-1.8.0 (c (n "solana-logger") (v "1.8.0") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "04d56m0q2k8hg2i4ks0irzp0x82jkzh5s68v1bqsf3dv07d917ms")))

(define-public crate-solana-logger-1.6.28 (c (n "solana-logger") (v "1.6.28") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "0n0ap90lfndwqkwm9xf15rlslg1569g5m5q2mmrsisg6pvwcz3xl")))

(define-public crate-solana-logger-1.7.15 (c (n "solana-logger") (v "1.7.15") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "188m420ws2lr6slhcy2cl0nn1dc57g9r2a668yaq06h1ihszc53w")))

(define-public crate-solana-logger-1.8.1 (c (n "solana-logger") (v "1.8.1") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "0m1ylfy168yqvdbzgi0crg7bds3nv0bx88npnh6s9rq3pyxpnffh")))

(define-public crate-solana-logger-1.7.16 (c (n "solana-logger") (v "1.7.16") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "10pxzkqbc2fbb8smisl3vxzs9759rgn2r23zri22z7yy23wlzxks")))

(define-public crate-solana-logger-1.7.17 (c (n "solana-logger") (v "1.7.17") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "1wl6kjid7g3xlqwqfln06n4xqv9i3fj40ws3hvbxb5ki07smqyc3")))

(define-public crate-solana-logger-1.8.2 (c (n "solana-logger") (v "1.8.2") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "08k4jjg8bn61fzjyc7hzm28wb3d2n5r64ziw1xynxllmafyc8vrm")))

(define-public crate-solana-logger-1.8.3 (c (n "solana-logger") (v "1.8.3") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "0y9ks09k1al8q4dn2jbd1wygj3r6w5py873ad2hnfrha0v4cyayk")))

(define-public crate-solana-logger-1.8.4 (c (n "solana-logger") (v "1.8.4") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "06qdkcl654piadgbmw3hzxwd42f2fkksk58xxf18pvrw4scvqa2c")))

(define-public crate-solana-logger-1.8.5 (c (n "solana-logger") (v "1.8.5") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "164a1rs65jjpva50wzmwfhqy2ci0b2hz3pa8z8bxy167qzs8b219")))

(define-public crate-solana-logger-1.8.6 (c (n "solana-logger") (v "1.8.6") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "0wf63ris2wp2pf8crmii837f22vpyvcq8xl5vdg8hnras42f4anq")))

(define-public crate-solana-logger-1.8.7 (c (n "solana-logger") (v "1.8.7") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "1rbrzwwmq3vm2z3q2bxj5lcxrdv62c73hla1ahjd0pbyj0nz6z69")))

(define-public crate-solana-logger-1.8.8 (c (n "solana-logger") (v "1.8.8") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "11757blsjsc03bm1mfwv6cn8hq82vmy1i5ryx5dlkjk1malg4m08")))

(define-public crate-solana-logger-1.8.9 (c (n "solana-logger") (v "1.8.9") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "0gclvh8fnbkcq0a2pkqpvxklz3x8pj8ki9xcs2f4hy0iadf5w4wg")))

(define-public crate-solana-logger-1.9.0 (c (n "solana-logger") (v "1.9.0") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "0spvrdrnz5jklk5rvq95l42p9pd8c0bhxi459zhknsr2a3shffq5")))

(define-public crate-solana-logger-1.8.10 (c (n "solana-logger") (v "1.8.10") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "0k09vfq5i6zraxq6ki72qaa0l6q9vs9x570pdk7irsi7qmxkc24j")))

(define-public crate-solana-logger-1.8.11 (c (n "solana-logger") (v "1.8.11") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "0awd8kinq764anxlbnffhhx14mip400g3a2lc301ryvai577g45q")))

(define-public crate-solana-logger-1.9.1 (c (n "solana-logger") (v "1.9.1") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "0p0jbfsh7xdp0vg3dgwpi41d9gj3c6gdgby7ld6bgsh5zvcb99g0")))

(define-public crate-solana-logger-1.9.2 (c (n "solana-logger") (v "1.9.2") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "05i5j7y3lyi5hhhzfs7nr2hrv5rwc2c87dyhw3sh8lcagp2dpwm6")))

(define-public crate-solana-logger-1.9.3 (c (n "solana-logger") (v "1.9.3") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "1fv5fshc2k2104f0bhnnpzxb6yadfm11v8aa835y5m50iqf39kni")))

(define-public crate-solana-logger-1.8.12 (c (n "solana-logger") (v "1.8.12") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "1azzs4p1swl11x33qqb9p71dvfm88l1v2c7d7rxmdcqw8mm6j6fp")))

(define-public crate-solana-logger-1.9.4 (c (n "solana-logger") (v "1.9.4") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "1i3ivahj4vnipbr0rxfidylp1n6h02lwi8nfdzwldpk5nrdr5bvf")))

(define-public crate-solana-logger-1.8.13 (c (n "solana-logger") (v "1.8.13") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "15k2af07dzrf7wc867fqdaxwsk70yic3qgpf568x2g8g53ny9zxf")))

(define-public crate-solana-logger-1.9.5 (c (n "solana-logger") (v "1.9.5") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "0fz6n5fmk71a26kzx4gj352gmi2bl643kimkmlp7y1x2q423pdkj")))

(define-public crate-solana-logger-1.8.14 (c (n "solana-logger") (v "1.8.14") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "0yddc2nbgn13zzz0smsfwim4jcib64zgz3vik6cd7ff69jxarbdl")))

(define-public crate-solana-logger-1.9.6 (c (n "solana-logger") (v "1.9.6") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "14awmw1zmsjfndmn7wv1b7lizgzclsjw2mg5736ph2a0zb8vbcv5")))

(define-public crate-solana-logger-1.9.7 (c (n "solana-logger") (v "1.9.7") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "11a65638yrbrdn7hr8jkkql98f39mfyl48cgvnkmkksw6sy00017")))

(define-public crate-solana-logger-1.8.16 (c (n "solana-logger") (v "1.8.16") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "1swcckhr350nahacbrnbxlfsk25ng3mg89md3r456nlzqim877b5")))

(define-public crate-solana-logger-1.9.8 (c (n "solana-logger") (v "1.9.8") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "0xp57fzlncs089qkcdyamhx437i5gyvzkangplkim8y0zh7ccr34")))

(define-public crate-solana-logger-1.9.9 (c (n "solana-logger") (v "1.9.9") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "0lgq8ljyklpwrfzq7n5w0mws993jq253n3pzqic2xs2ss7g91z59")))

(define-public crate-solana-logger-1.10.0 (c (n "solana-logger") (v "1.10.0") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "1fhy9gx4d2zk1gnzgj5b6kigbkfnq7fw9idlc7bzss2hggqad5qd")))

(define-public crate-solana-logger-1.9.10 (c (n "solana-logger") (v "1.9.10") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "1n7zvcjhwiign9xic5iw6xrzylq37izd50q5qxr5w5rlr53v4iyd")))

(define-public crate-solana-logger-1.9.11 (c (n "solana-logger") (v "1.9.11") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "115nk8yivjdxvmk3x5ii29w5jfkrphpq8vds0lslv8ip0zhriqmc")))

(define-public crate-solana-logger-1.9.12 (c (n "solana-logger") (v "1.9.12") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "0pdcl4nnmrrqj2fg5kkija6dik7wfx803n8dswk4kahnyrxiqgiy")))

(define-public crate-solana-logger-1.10.1 (c (n "solana-logger") (v "1.10.1") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "1cdixhf75wf3jjx1s92bji1ilcqcb3p1wlqpk7xf45g2pmplif7z")))

(define-public crate-solana-logger-1.10.2 (c (n "solana-logger") (v "1.10.2") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "0xnrmkhcmkfv03hl2ix9fww1lmi0dnvpjldk6c98wmdsfsjvrgjg")))

(define-public crate-solana-logger-1.9.13 (c (n "solana-logger") (v "1.9.13") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "1m2x1lkpxp8n8hdf4kvpbb3bca2jknpns9aimxmwx2jbyw9s3bmm")))

(define-public crate-solana-logger-1.10.3 (c (n "solana-logger") (v "1.10.3") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "1h3hxisv0bg9gpz81ilgc4lmn4wpx2rl4ii63xcc4qczyzrqzb4m")))

(define-public crate-solana-logger-1.9.14 (c (n "solana-logger") (v "1.9.14") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "0lxid0l3gvv50801bvvg99i2hfw9j9046g9j868rkgs4336j0mjx")))

(define-public crate-solana-logger-1.10.4 (c (n "solana-logger") (v "1.10.4") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "1qaqrms69m9l4l82l846l5kza7ddsdbkmviih171ci3hh1gh0ina")))

(define-public crate-solana-logger-1.10.5 (c (n "solana-logger") (v "1.10.5") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "160cn7bfq38rvybwhvi8y93ay7py9ck31bgbkxglyjqgrazm6fh5")))

(define-public crate-solana-logger-1.10.6 (c (n "solana-logger") (v "1.10.6") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "0mkvs2jdgad2bxf8is6i9gg3lq5yxnmn6fdpxg7fc0r8i69y7rwf")))

(define-public crate-solana-logger-1.9.15 (c (n "solana-logger") (v "1.9.15") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "02gca9c5yyvhahgi0m4c01f1lw1ywvf0akj4m7rqwnb0yf3hb2xa")))

(define-public crate-solana-logger-1.10.7 (c (n "solana-logger") (v "1.10.7") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "1s1nwfimf4237436dh24g6wmn6rxp7fvlnk6dnp2bny5c93ywnxp")))

(define-public crate-solana-logger-1.10.8 (c (n "solana-logger") (v "1.10.8") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "1gi6g2s0wvs1dshb64j6kldsw9xfpw7wnip25017s7j662f25gvk")))

(define-public crate-solana-logger-1.9.16 (c (n "solana-logger") (v "1.9.16") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "1qlhcia31dazrkg2pps7zmqj6mzzkl6sapyzmnbh6x6xyi70mjsp")))

(define-public crate-solana-logger-1.9.17 (c (n "solana-logger") (v "1.9.17") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "1m9sxkqd8aif1fbqpxajij2m7q01y6njqlvmwyslnf3mp74x1fyh")))

(define-public crate-solana-logger-1.10.9 (c (n "solana-logger") (v "1.10.9") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "1w0pv272frkhm0wacnfgs62l29lhm504g75zjcyjadarqabhwggn")))

(define-public crate-solana-logger-1.9.18 (c (n "solana-logger") (v "1.9.18") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "1z5nf18l7l18d7vjdb76wbgh5zz0ajkyvyy5raivp2ikwm4xllc1")))

(define-public crate-solana-logger-1.10.10 (c (n "solana-logger") (v "1.10.10") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "1mj44mc7zq2j39hywbpx8wksh4g62hxj66hmmfzppisw2jjflsnc")))

(define-public crate-solana-logger-1.10.11 (c (n "solana-logger") (v "1.10.11") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "1xr75rxxcybmj11m62wvxypw6mzl771bzmw2adcqqqdlqk8dbqx8")))

(define-public crate-solana-logger-1.9.19 (c (n "solana-logger") (v "1.9.19") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "0warrzj3nypys0sf4r2dpxrh9s1k1ihch0r3070r567d9xq8vwmj")))

(define-public crate-solana-logger-1.10.12 (c (n "solana-logger") (v "1.10.12") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "0ika4wm67546c6is5p1y3ibhdkf2j9ig4mbwdiics139avycbayh")))

(define-public crate-solana-logger-1.9.20 (c (n "solana-logger") (v "1.9.20") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "17r7q79jh9fd9r781g50rq8xqrbbcicbcl906rwd2rnsr6gwabcl")))

(define-public crate-solana-logger-1.10.13 (c (n "solana-logger") (v "1.10.13") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "1jbgcc2gj5sfpx42kpxkb5gnfwbmi5i4xb151alvmi0c33hsxddd")))

(define-public crate-solana-logger-1.9.21 (c (n "solana-logger") (v "1.9.21") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "1w2lipd8wrwnwvzssfdqmsrihdgm1v5m6jjfzv7qz4vf1vinxi1s")))

(define-public crate-solana-logger-1.10.14 (c (n "solana-logger") (v "1.10.14") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "0amzn24ks0cb64j205s4jm2y2w5bq2m71jpqd5aibp753rndnqpp")))

(define-public crate-solana-logger-1.9.22 (c (n "solana-logger") (v "1.9.22") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "09fqkwxjhp1g7gxgqcy27hya2wyk653zrn9paak83wa0mp2s6p8p")))

(define-public crate-solana-logger-1.10.15 (c (n "solana-logger") (v "1.10.15") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "0vi45dikldxdwrp7k0wlis4n4bmyvqk6pwn8ghdxnl196bh6b6w2")))

(define-public crate-solana-logger-1.10.16 (c (n "solana-logger") (v "1.10.16") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "1v2mx2a942zm921mviyc8xv7z8i6j9zlcgjxv76zrv76s98xb41p")))

(define-public crate-solana-logger-1.10.17 (c (n "solana-logger") (v "1.10.17") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "1aqf7qjz32djyp028l6704ilk14430968jcm1mm5snna85d8nl7l")))

(define-public crate-solana-logger-1.10.18 (c (n "solana-logger") (v "1.10.18") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "04dcifn805n3l6f8jkk31i2qdjzniyhzqglkb25z90ip3q4003rr")))

(define-public crate-solana-logger-1.9.23 (c (n "solana-logger") (v "1.9.23") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "0wbi9z4j0s1ympywmqx0k75h8bn0c56mkw90qz8i4s98bayyx6b9")))

(define-public crate-solana-logger-1.10.19 (c (n "solana-logger") (v "1.10.19") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "0v5yd862l37pn1jwagvj4x4mrq9lmyfq934hwmfm7j97awaddk61")))

(define-public crate-solana-logger-1.9.24 (c (n "solana-logger") (v "1.9.24") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "17j2hd76sy00qag3gcs66vw1lswc9lzc9ff23ch0yxvbnjn8ha4r")))

(define-public crate-solana-logger-1.9.25 (c (n "solana-logger") (v "1.9.25") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "1vgjhs0mgxsnc412adwqzxy7iqr85kl9scf85rpw9bk0dzpa7d80")))

(define-public crate-solana-logger-1.10.20 (c (n "solana-logger") (v "1.10.20") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "0pjan8pcs0zpicijmmlvqjmkwjklmxdy7mq1r6nwa13g8s4y29vg")))

(define-public crate-solana-logger-1.9.26 (c (n "solana-logger") (v "1.9.26") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "1b124javsns85f9045yn61isck4xc9klf5gx2zrfxdx62gr3k9sz")))

(define-public crate-solana-logger-1.10.21 (c (n "solana-logger") (v "1.10.21") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "0582s5v9gbpnzxzlf85gi9af6pqsbzq8q5znh0qqazf4dxvprr3b")))

(define-public crate-solana-logger-1.10.22 (c (n "solana-logger") (v "1.10.22") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "15v6687vvchg402y4g75swfda8agarzsi98rpq5yrcqzb0dwdq2v")))

(define-public crate-solana-logger-1.9.28 (c (n "solana-logger") (v "1.9.28") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "19cfpxx7ksn3fsxkkmcz1cp4whkf8jfiq1gv4fssjzi01xwnmmxv")))

(define-public crate-solana-logger-1.10.23 (c (n "solana-logger") (v "1.10.23") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "1hv912n7y98inf3sgvq5r3mr1k8pn7fphsdxazxyrn7mqmfkpc5n")))

(define-public crate-solana-logger-1.10.24 (c (n "solana-logger") (v "1.10.24") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "1wxf8snvml3j2zwda27g91qbzq09v95n8mzjp1qqv2hfx5yqq4jk")))

(define-public crate-solana-logger-1.10.25 (c (n "solana-logger") (v "1.10.25") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "01g5jlxmnlsvvwmzyy88pkk62yv97bbklqnjjf2dyjfvgm0r7aks")))

(define-public crate-solana-logger-1.9.29 (c (n "solana-logger") (v "1.9.29") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "1qj3dsnmb29ka1vyq3gvawdlfhcg1j2wf0s89jl7g0pwa9fq1qcw")))

(define-public crate-solana-logger-1.10.26 (c (n "solana-logger") (v "1.10.26") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "1md8ji3vz1cqzwf05xicw2s9pvp8yvd9ns6sszln3zn6bc8cxwb2")))

(define-public crate-solana-logger-1.11.0 (c (n "solana-logger") (v "1.11.0") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "1pl3g04k887x49cn7qy3pw41gkhw3y4mwpqrf8zzdljj73nzib92")))

(define-public crate-solana-logger-1.10.27 (c (n "solana-logger") (v "1.10.27") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "1qyf60sdc1k7mjv2w680ggli02plwn95ax7m9nw304a1bh1hzqrl")))

(define-public crate-solana-logger-1.11.1 (c (n "solana-logger") (v "1.11.1") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "1vswcwasc1rn1dqkiwnllvn2mfk5k4jnr1vj7nd7jy0vmrqcxaya")))

(define-public crate-solana-logger-1.10.28 (c (n "solana-logger") (v "1.10.28") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "1xwjpslg524z0nx4806s4na79mczcrrpv4fh4grdq1k2707s1lx2")))

(define-public crate-solana-logger-1.10.29 (c (n "solana-logger") (v "1.10.29") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "17r6223jl6y0l0qmrjxwbn3gvf4ms21mzkaddv1ngbdx278831r4")))

(define-public crate-solana-logger-1.10.30 (c (n "solana-logger") (v "1.10.30") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "0489lxrlagar08bpirkz0dvgx42bjs5ydjzvi3y70cvwk6c03951")))

(define-public crate-solana-logger-1.11.2 (c (n "solana-logger") (v "1.11.2") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "147ksydb8i2h4iyk2ksf8fnvziy65paxvphvngdi8a9dqn6f6vzh")))

(define-public crate-solana-logger-1.10.31 (c (n "solana-logger") (v "1.10.31") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "1l726f6y8mwcq0gifx2666g92m92l2srmly8gcijzdz56d0lwg63")))

(define-public crate-solana-logger-1.11.3 (c (n "solana-logger") (v "1.11.3") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "1yrachp96xqvna7vk204xx5p8nrf0z5drb06gs1cazaqzvcskb1z")))

(define-public crate-solana-logger-1.10.32 (c (n "solana-logger") (v "1.10.32") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "1jmnwld53vgbbnz2hwjggxcnql0p0h1mk0dx1986nj88z9gr0gbs")))

(define-public crate-solana-logger-1.11.4 (c (n "solana-logger") (v "1.11.4") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "0y57mk8hqrcmfy858v9b9mka5j67qab5ir6bcqvrks5gydzajzf4")))

(define-public crate-solana-logger-1.10.33 (c (n "solana-logger") (v "1.10.33") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "0wya7vp7nx1x6jsa3lkwbrqzccn55a68p4kc6mnqgjgry7kbcb5i")))

(define-public crate-solana-logger-1.10.34 (c (n "solana-logger") (v "1.10.34") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "0zz4n0l8ngd48k9vpjf8cvkbd0iixjz2kgazc00x2i7lhf5dslp1")))

(define-public crate-solana-logger-1.11.5 (c (n "solana-logger") (v "1.11.5") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "0mwv505h4q66jpan4ryg7hjfq4znsy54r7ci5n33vlv3ip36zsp7")))

(define-public crate-solana-logger-1.10.35 (c (n "solana-logger") (v "1.10.35") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "150zpvivf3fzbmm0z4vj8f0fag10vilrqglrmawx310nsf6953pl")))

(define-public crate-solana-logger-1.11.6 (c (n "solana-logger") (v "1.11.6") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "1i9a3h1k9c0j3rqd4xwp06aqssqjixz2nfdq8bgf087awfikkm3r")))

(define-public crate-solana-logger-1.11.7 (c (n "solana-logger") (v "1.11.7") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "16rdvi7mxhg9ffda26rvs6ivr9l3avsm30nvj1bf85mksh2g7pjf")))

(define-public crate-solana-logger-1.11.8 (c (n "solana-logger") (v "1.11.8") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "050l5zlgkw0jfazfhws5qji3s49lrvd1nhpbsh4k6pyyfpmz78pc")))

(define-public crate-solana-logger-1.11.10 (c (n "solana-logger") (v "1.11.10") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "15pfqv93gnljhxh7fwgwq0scxnk8dvxpgw9hmia033s3pf7p6sn0")))

(define-public crate-solana-logger-1.10.38 (c (n "solana-logger") (v "1.10.38") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "1dn1n7cdvwxkgzjaji1warfb4jr3fad8sy47zszi9cdf5b9s9id0")))

(define-public crate-solana-logger-1.13.0 (c (n "solana-logger") (v "1.13.0") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "0pyrlp1zyv1sp5gllw74cdnss10wykbx7hrzwzqywj9x4g8zfby7")))

(define-public crate-solana-logger-1.14.0 (c (n "solana-logger") (v "1.14.0") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "04b1kdqd5yfs2l6fwlmwh0iravljv7bykixw8pkqy1f9b7z87wcf")))

(define-public crate-solana-logger-1.14.1 (c (n "solana-logger") (v "1.14.1") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "0xvgm38y9j5yf3pblnwv63vm2d1vj10l22j6l2qiyz424f2zw98g")))

(define-public crate-solana-logger-1.10.39 (c (n "solana-logger") (v "1.10.39") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "1yfz841p4d6s3w2f7w0ycfgp0xr3sq426pbc1h8bpvi0nlyynphi")))

(define-public crate-solana-logger-1.14.2 (c (n "solana-logger") (v "1.14.2") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "1gllyfh0bz7m9kb915sbb3q9nsydwjvv89wwj93rbi51gzpfj7n8")))

(define-public crate-solana-logger-1.13.1 (c (n "solana-logger") (v "1.13.1") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "0z4j2m644i3irhk7l2ziyl68h5q29mqqghwyyhls8z6v40iy2f0k")))

(define-public crate-solana-logger-1.14.3 (c (n "solana-logger") (v "1.14.3") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "0pb1wr2avnmcr45g52wqgjky5ac7fv1mry3lmnchb0mzdrjbvb7i")))

(define-public crate-solana-logger-1.10.40 (c (n "solana-logger") (v "1.10.40") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "14ibvwf2dw02jj8dh6jqn4wx9yxkwgbq6pxd15vm9wr6kjfrrr5w")))

(define-public crate-solana-logger-1.14.4 (c (n "solana-logger") (v "1.14.4") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "1c1xrshfbk6wgsg2adywj7xy5xgspbagzp3w26mz3kq9jmzl3c1k")))

(define-public crate-solana-logger-1.13.2 (c (n "solana-logger") (v "1.13.2") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "0xnqva26afrhlf177xvkkvkdaqlf3d3kxjdhjrhlmflbgfr5crqv")))

(define-public crate-solana-logger-1.14.5 (c (n "solana-logger") (v "1.14.5") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "09vgigd8fj7gmfhq1hcvd9f4nwgzd3vak04q49qjvp9vcapad466")))

(define-public crate-solana-logger-1.10.41 (c (n "solana-logger") (v "1.10.41") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "1wjdag32f1gf2qbwgfa7xrw8pdrh6pfpnk0r8r1glsn44d0xc0bj")))

(define-public crate-solana-logger-1.13.3 (c (n "solana-logger") (v "1.13.3") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "0ynd0pvgv47mr6bnb7cxp08dii45ww44zsg9bfssjbhfdzw8x217")))

(define-public crate-solana-logger-1.13.4 (c (n "solana-logger") (v "1.13.4") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "1a4ql1lmkb91y40nbq694sczx1kyd3dm40zdldprc50ghm0bavn5")))

(define-public crate-solana-logger-1.14.6 (c (n "solana-logger") (v "1.14.6") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "0lamr94fjq9nk9s8nbxz9lwsjgkq98qg350050sal7hf1hzvfmfj")))

(define-public crate-solana-logger-1.14.7 (c (n "solana-logger") (v "1.14.7") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "0whhwcgx5p3rbvlx1ymdmzfvhj0h1c1jzfcnhv99wr88k62a2y0g")))

(define-public crate-solana-logger-1.13.5 (c (n "solana-logger") (v "1.13.5") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "1rdpijad401fimf71s0i704vydvxilw95xzr9yv1jr98xziisxq8")))

(define-public crate-solana-logger-1.14.8 (c (n "solana-logger") (v "1.14.8") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "1npz57kcwiizf7qjkysmfc79b5d16fh20aqhmkr8zrgyxmyjzj0y")))

(define-public crate-solana-logger-1.14.9 (c (n "solana-logger") (v "1.14.9") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "13738fwpgldysnrcwirr199n1q3s9f0vy7ksil13gwwaxydy074f")))

(define-public crate-solana-logger-1.14.10 (c (n "solana-logger") (v "1.14.10") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "1knpvs7a0yz74qj3pwwgv3i9fns6mxz5s8mnd1n8ffm8h7n3mv28")))

(define-public crate-solana-logger-1.14.11 (c (n "solana-logger") (v "1.14.11") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "0f9x34vf2syhms4qsgik445xpaysvhy3lkzzh2zyg0y6lamwnaqc")))

(define-public crate-solana-logger-1.14.12 (c (n "solana-logger") (v "1.14.12") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "14nmcdrm62zla470lckiyvggqnnkn2igr6096jj15xwhfj6qz88k")))

(define-public crate-solana-logger-1.13.6 (c (n "solana-logger") (v "1.13.6") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "0hxf5bhj9cgi7njbc23xi4r86yc3mczzpdfki9cx4hbywq2r90m6")))

(define-public crate-solana-logger-1.14.13 (c (n "solana-logger") (v "1.14.13") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "0jyyq9cgjllki7nj4frh4b8gy6hf1h2h2j7ldrrq6lqv1akicza4")))

(define-public crate-solana-logger-1.15.0 (c (n "solana-logger") (v "1.15.0") (d (list (d (n "env_logger") (r "^0.9.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "04b7blcihnrw1x7jl7wk1zx4vdmqk2v8c2rl5yj8dw5lf2aidqfs") (y #t)))

(define-public crate-solana-logger-1.14.14 (c (n "solana-logger") (v "1.14.14") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "04q3dlv08ad4r2x7dfhshv85yia6jasdqs1k7xbyakqlfwjv2ai0")))

(define-public crate-solana-logger-1.14.15 (c (n "solana-logger") (v "1.14.15") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "1mhbb9wa72zrz7r568fywgvb7m49g77jsxl3lkvhsrm791gghp1h")))

(define-public crate-solana-logger-1.15.1 (c (n "solana-logger") (v "1.15.1") (d (list (d (n "env_logger") (r "^0.9.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "01k6shra4flp328yqrf65qcaafhvahv895i1rcfplz4mr5xzhbh3") (y #t)))

(define-public crate-solana-logger-1.15.2 (c (n "solana-logger") (v "1.15.2") (d (list (d (n "env_logger") (r "^0.9.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "0ij4ybfcwmnlxrm9hs7ncqxlnrblmg418b3wymsxzr0j6v5181qp") (y #t)))

(define-public crate-solana-logger-1.14.16 (c (n "solana-logger") (v "1.14.16") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "0mg0cq5yvwlljysjgmflm4awmfkmak001s6ibl496gj994f71ah6")))

(define-public crate-solana-logger-1.14.17 (c (n "solana-logger") (v "1.14.17") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "1xb3xj2dpr7fqyv9z7hl6xijb6l15gby2i077iirk9w4prk2hl1b")))

(define-public crate-solana-logger-1.13.7 (c (n "solana-logger") (v "1.13.7") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "1ppi4ccq7r3w0vd9gdq4l4mi50f7lnlhs6nxxv55lcrjfdixz8i3")))

(define-public crate-solana-logger-1.14.18 (c (n "solana-logger") (v "1.14.18") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "011fwzfgyq7m7yih5v9ccgipj978w1wjpil65i4p9wrqnslhy28j")))

(define-public crate-solana-logger-1.16.0 (c (n "solana-logger") (v "1.16.0") (d (list (d (n "env_logger") (r "^0.9.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "11bbngrrfw55hs9gnw9y78n8qyv7gyhlpavjpfknq1l63gb45hsg")))

(define-public crate-solana-logger-1.16.1 (c (n "solana-logger") (v "1.16.1") (d (list (d (n "env_logger") (r "^0.9.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "1kmhk3la81n4samnplns0bl892ff9671jblzdfzwa1wp2959147m")))

(define-public crate-solana-logger-1.14.19 (c (n "solana-logger") (v "1.14.19") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "1bgd7q96xf09s9m3h7h6lmrasgbhgv0749wm4cdx81ml886amxi6")))

(define-public crate-solana-logger-1.16.2 (c (n "solana-logger") (v "1.16.2") (d (list (d (n "env_logger") (r "^0.9.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "0jnhjfnv596s8q8859v26l10mi4v5v4zy6jr1zn72jgs3ha2qg6s")))

(define-public crate-solana-logger-1.16.3 (c (n "solana-logger") (v "1.16.3") (d (list (d (n "env_logger") (r "^0.9.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "00qvb44k1gnsrx21cd9sh70d2y1m12ijwf67v5nfh5chii797jxk")))

(define-public crate-solana-logger-1.14.20 (c (n "solana-logger") (v "1.14.20") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "0ixf0r4py2x7hvzh65bnqzmll0kkvrz812x1f1dn3d8b92ychcl6")))

(define-public crate-solana-logger-1.16.4 (c (n "solana-logger") (v "1.16.4") (d (list (d (n "env_logger") (r "^0.9.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "1q2d4c7mbdb0b42kdr9c74ydydzmz4p07yn6x7wmfs577pxm2mvy")))

(define-public crate-solana-logger-1.16.5 (c (n "solana-logger") (v "1.16.5") (d (list (d (n "env_logger") (r "^0.9.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "0cdd43vix1y2w3x90m5lz8qphcrrngnv03mfixv57p9whw6vw0lk")))

(define-public crate-solana-logger-1.14.21 (c (n "solana-logger") (v "1.14.21") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "1qw1y4b2l4q67a30hnwav5fkm126xpgnhp06inn2905gm8g4v4mg")))

(define-public crate-solana-logger-1.14.22 (c (n "solana-logger") (v "1.14.22") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "1d18krrsqsv10rblprgcnx5nmaphvlx8fqvqzqk8gasc0gg7h7cs")))

(define-public crate-solana-logger-1.16.6 (c (n "solana-logger") (v "1.16.6") (d (list (d (n "env_logger") (r "^0.9.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "18kka11zdqpf3awvkhfrjy7brfbkzzjkdc7dy00s416qxa1anfl5")))

(define-public crate-solana-logger-1.16.7 (c (n "solana-logger") (v "1.16.7") (d (list (d (n "env_logger") (r "^0.9.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "0b7wims4qhfsxjw4grf06r7n9i80bw6zghl9znbmwip2g6hy2f8k")))

(define-public crate-solana-logger-1.14.23 (c (n "solana-logger") (v "1.14.23") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "1z3igsyclsxg0v3c8853a81xr88a57c424rykhyqkbv2kadkkivb")))

(define-public crate-solana-logger-1.16.8 (c (n "solana-logger") (v "1.16.8") (d (list (d (n "env_logger") (r "^0.9.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "16h1jigsfv7xxmbyaf274idmmkqvfs111yyh5j84ar1hj1gsfwdw")))

(define-public crate-solana-logger-1.14.24 (c (n "solana-logger") (v "1.14.24") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "1jr1v76win72s0hrkyh9wqls7hx4pwzi5jjg833dmp3q16m5cqvf")))

(define-public crate-solana-logger-1.16.9 (c (n "solana-logger") (v "1.16.9") (d (list (d (n "env_logger") (r "^0.9.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "1cwf7cyln2hwkh3ngxgyqqp2rb9ygcml97ifhl2vxla0dzp38bcs")))

(define-public crate-solana-logger-1.16.10 (c (n "solana-logger") (v "1.16.10") (d (list (d (n "env_logger") (r "^0.9.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "1jh9brsx4zbq98fq01xlwnsxcd18npshpqi0ym5h0yhjfz5xyl90")))

(define-public crate-solana-logger-1.14.25 (c (n "solana-logger") (v "1.14.25") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "1jardyqrlb2jysgv9wf7pvwzhr28673k0dssdws5qay7hvp6i2j6")))

(define-public crate-solana-logger-1.16.11 (c (n "solana-logger") (v "1.16.11") (d (list (d (n "env_logger") (r "^0.9.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "13r7iqaihf07q6g7xw8x2fx3p1bag6jqsn6314jf0l1i68dl5s6y")))

(define-public crate-solana-logger-1.14.26 (c (n "solana-logger") (v "1.14.26") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "1gqvand67jli4f99axyjpsv3bjak9h244a65x3x3yfki6mrk4v4w")))

(define-public crate-solana-logger-1.16.12 (c (n "solana-logger") (v "1.16.12") (d (list (d (n "env_logger") (r "^0.9.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "1pmhfjwb9k0mfsmwm751dx44vrazpc1l5sy8yva82h8lq1zpviwa")))

(define-public crate-solana-logger-1.14.27 (c (n "solana-logger") (v "1.14.27") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "1x8bvqvhs3kfnz7561a594cfjrfbp234bj8dqhmcpfnn467y8naf")))

(define-public crate-solana-logger-1.16.13 (c (n "solana-logger") (v "1.16.13") (d (list (d (n "env_logger") (r "^0.9.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "1yjfd72yg5hdphd007rm3s8mbd0lq4qc3xqnjlwl92pp6kkli2mz")))

(define-public crate-solana-logger-1.16.14 (c (n "solana-logger") (v "1.16.14") (d (list (d (n "env_logger") (r "^0.9.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "1q7yajiz2hgyrrhmb7pg4c2xd8ggfjdm0vrwvv485mw9lcvh1p14")))

(define-public crate-solana-logger-1.14.28 (c (n "solana-logger") (v "1.14.28") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "02gq4s2hs97cba11k11bj079kzji5cy83jkja3dgxfmk2rivs6rl")))

(define-public crate-solana-logger-1.14.29 (c (n "solana-logger") (v "1.14.29") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "1h15c3dphhxraaz0f447b207ak8nydqnsiyzs2w3g1ki034vpb8c")))

(define-public crate-solana-logger-1.16.15 (c (n "solana-logger") (v "1.16.15") (d (list (d (n "env_logger") (r "^0.9.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "1cjfxh71pzknlrv7jr1yk4nki6mg2dzjspfl5da7rx53lrxay3y0")))

(define-public crate-solana-logger-1.17.0 (c (n "solana-logger") (v "1.17.0") (d (list (d (n "env_logger") (r "^0.9.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "0y974l8f2hcjwshhyrmpa4rd313sn4ch6vhj07gs4fp83rmcq386")))

(define-public crate-solana-logger-1.16.16 (c (n "solana-logger") (v "1.16.16") (d (list (d (n "env_logger") (r "^0.9.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "0c7fn6h7g03s93hggm545bnjlb7xnbgkxbm94pmf2p2395khqc9c")))

(define-public crate-solana-logger-1.17.1 (c (n "solana-logger") (v "1.17.1") (d (list (d (n "env_logger") (r "^0.9.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "0dxnz5w4kijjbfv1sraajmal81b0br9l3wdjs7l5rnajfzc81n6m")))

(define-public crate-solana-logger-1.16.17 (c (n "solana-logger") (v "1.16.17") (d (list (d (n "env_logger") (r "^0.9.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "1mkrsw7chsvkf75hzqn7b6hfhhsicpayilgg3dicyvfhq8jzwxjg")))

(define-public crate-solana-logger-1.17.2 (c (n "solana-logger") (v "1.17.2") (d (list (d (n "env_logger") (r "^0.9.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "0y0nkkzgn5990jxm885cl14g35llchgiblxhg72jnz4yl2mmvynp")))

(define-public crate-solana-logger-1.16.18 (c (n "solana-logger") (v "1.16.18") (d (list (d (n "env_logger") (r "^0.9.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "1byph440sl9a12zkyk9yji6qxc9xb4wkqpwrdm5gmw0677r12ly9")))

(define-public crate-solana-logger-1.17.3 (c (n "solana-logger") (v "1.17.3") (d (list (d (n "env_logger") (r "^0.9.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "0h07q7pvy7y94jwdkg0zvj23z4jc6n930p2n6x9cv2k7jqb6m9c4")))

(define-public crate-solana-logger-1.17.4 (c (n "solana-logger") (v "1.17.4") (d (list (d (n "env_logger") (r "^0.9.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "0iq475bvpl7fvgch23gajygs9mhf1rikhn2if8298v0rhw54l8js")))

(define-public crate-solana-logger-1.16.19 (c (n "solana-logger") (v "1.16.19") (d (list (d (n "env_logger") (r "^0.9.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "0ls5sdg2lyrpdzzwzxsfqnh5zyna95sb0vyims9jylaca32g1izr")))

(define-public crate-solana-logger-1.17.5 (c (n "solana-logger") (v "1.17.5") (d (list (d (n "env_logger") (r "^0.9.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "0yzj51p7ljjjm4jn5bp4jf4v8pr65cr57d2c34718sps0yfv8yg6")))

(define-public crate-solana-logger-1.17.6 (c (n "solana-logger") (v "1.17.6") (d (list (d (n "env_logger") (r "^0.9.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "149rw72jaj8q9kywa9xi56237q6ll4v3sakbdxz83p4blz5448fg")))

(define-public crate-solana-logger-1.16.20 (c (n "solana-logger") (v "1.16.20") (d (list (d (n "env_logger") (r "^0.9.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "15z23yqf7fr1qv8sfrngx6wyhv29vzvj1gsj4j9ham7vsj66ay02")))

(define-public crate-solana-logger-1.17.7 (c (n "solana-logger") (v "1.17.7") (d (list (d (n "env_logger") (r "^0.9.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "05h3gqzxplyjllx08z6igz6m38k2xbhxjaq5n0k0jril0jy9dvhz")))

(define-public crate-solana-logger-1.16.21 (c (n "solana-logger") (v "1.16.21") (d (list (d (n "env_logger") (r "^0.9.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "1lri5sasji1agdciwl3r8pr9xy40dblzzffks7h56fa09vac0r6k")))

(define-public crate-solana-logger-1.17.8 (c (n "solana-logger") (v "1.17.8") (d (list (d (n "env_logger") (r "^0.9.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "1bdqdyf0x68qyjw9zxair2yg4lq4p1j5b2pxmhkv5cq55pnrd59q")))

(define-public crate-solana-logger-1.16.22 (c (n "solana-logger") (v "1.16.22") (d (list (d (n "env_logger") (r "^0.9.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "1ks21yl7bqhxr3anx2yhmxz903byl98npjjjafsrb2yxyzwmn21b")))

(define-public crate-solana-logger-1.16.23 (c (n "solana-logger") (v "1.16.23") (d (list (d (n "env_logger") (r "^0.9.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "0hbj7mif9m5kpxp5y2lqg6z6h31l5zaqy9z1iaaj01lv4y4kvf4x")))

(define-public crate-solana-logger-1.17.9 (c (n "solana-logger") (v "1.17.9") (d (list (d (n "env_logger") (r "^0.9.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "1s3k1f7ghb2wr6xc9m7fqrrnbcrirk18928vkpqlb95h37b3szw1")))

(define-public crate-solana-logger-1.17.10 (c (n "solana-logger") (v "1.17.10") (d (list (d (n "env_logger") (r "^0.9.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "0h39ck5ja3sqm9qvvkdbdkiqripbs6v24zzy1qvx9cil6508ky9z")))

(define-public crate-solana-logger-1.17.11 (c (n "solana-logger") (v "1.17.11") (d (list (d (n "env_logger") (r "^0.9.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "0gjqnxpj69rdcqz1vs0i69lzq8rk7ickpkswgg5m0l9vi5m3kd3s")))

(define-public crate-solana-logger-1.17.12 (c (n "solana-logger") (v "1.17.12") (d (list (d (n "env_logger") (r "^0.9.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "1mrb11f0pr9nlmyv13mik3lyrdar20q7c2ipibb53jf5rn04xdnk")))

(define-public crate-solana-logger-1.16.24 (c (n "solana-logger") (v "1.16.24") (d (list (d (n "env_logger") (r "^0.9.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "0ms7m0isgavx16j3gf6wjifjb8xba1454pz1gcfb74ks4b56p2gl")))

(define-public crate-solana-logger-1.17.13 (c (n "solana-logger") (v "1.17.13") (d (list (d (n "env_logger") (r "^0.9.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "0n8q12znshk5y6m83h90d4sdr0bhi0g2ij4548cjzza4d5i5gd6c")))

(define-public crate-solana-logger-1.17.14 (c (n "solana-logger") (v "1.17.14") (d (list (d (n "env_logger") (r "^0.9.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "1wxb5x4qwgk95rv1d2dslmrilapv28bpli66zfd0609s70d1nynk")))

(define-public crate-solana-logger-1.17.15 (c (n "solana-logger") (v "1.17.15") (d (list (d (n "env_logger") (r "^0.9.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "0dxiw9yqlnn5q0fcj9fcyzw3bnd8mcx4qnybd42z1icb12nkqzww")))

(define-public crate-solana-logger-1.16.25 (c (n "solana-logger") (v "1.16.25") (d (list (d (n "env_logger") (r "^0.9.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "11i4i9hbzhby8ad2p9lb0bnc9blw7k01kfp7iczlmamzfkvdwr3v")))

(define-public crate-solana-logger-1.16.26 (c (n "solana-logger") (v "1.16.26") (d (list (d (n "env_logger") (r "^0.9.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "1y3x15kmz8y63xwra50782ylbxn0k6y5vbniiix2x3c5svjm0a1q")))

(define-public crate-solana-logger-1.16.27 (c (n "solana-logger") (v "1.16.27") (d (list (d (n "env_logger") (r "^0.9.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "0wp92ncy7wjlm1hwl3nq083glzhqaasawz10cw37l3496r1rw0ww")))

(define-public crate-solana-logger-1.17.16 (c (n "solana-logger") (v "1.17.16") (d (list (d (n "env_logger") (r "^0.9.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "13ak6ls1pc3yg4ysm8n6z2mfsfwh8c456h3b9bjb6yld2jwa6s5q")))

(define-public crate-solana-logger-1.17.17 (c (n "solana-logger") (v "1.17.17") (d (list (d (n "env_logger") (r "^0.9.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "122n6vq649bhdhff9vf4zvl6gi1ind25ykr20j6p7v5hkwagfq39")))

(define-public crate-solana-logger-1.17.18 (c (n "solana-logger") (v "1.17.18") (d (list (d (n "env_logger") (r "^0.9.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "18wrcn94q876673fvjd825lkbc60j34pbgk0n9nqvrv6yafrhdfa")))

(define-public crate-solana-logger-1.18.0 (c (n "solana-logger") (v "1.18.0") (d (list (d (n "env_logger") (r "^0.9.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "13jf3qjwyir54w2m29yzsk5rbssh60b5314rb9d6waq2yd6nwr3s")))

(define-public crate-solana-logger-1.18.1 (c (n "solana-logger") (v "1.18.1") (d (list (d (n "env_logger") (r "^0.9.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "0a25m6zn2h0knvss311rkvxjdljkc61rpxcykb9zwggy4bdmr8ny")))

(define-public crate-solana-logger-1.17.20 (c (n "solana-logger") (v "1.17.20") (d (list (d (n "env_logger") (r "^0.9.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "1hq8zq38ixfibf8hyjs5abl6nkwf4fq5szmjg06lpxn2b7fc9kyz")))

(define-public crate-solana-logger-1.17.22 (c (n "solana-logger") (v "1.17.22") (d (list (d (n "env_logger") (r "^0.9.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "1m3i9dnjfwbqgdvnxrzirniw9pvqsv9lc6px8fa8cfz5fsdlgsnh")))

(define-public crate-solana-logger-1.18.2 (c (n "solana-logger") (v "1.18.2") (d (list (d (n "env_logger") (r "^0.9.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "187i66144b6j5bkzzcxb3zwikz557x2gjpqcmn883lj3817czsji")))

(define-public crate-solana-logger-1.17.23 (c (n "solana-logger") (v "1.17.23") (d (list (d (n "env_logger") (r "^0.9.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "0ybf7iv1ckgi35hjr1wqxn2q9plrfxxaaay11rlgb77arqrx3pvk")))

(define-public crate-solana-logger-1.18.3 (c (n "solana-logger") (v "1.18.3") (d (list (d (n "env_logger") (r "^0.9.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "1l4rbija1wk7zc5f2zfbv7b5f3cfxgpll0wa5nscly4gbyh673pd")))

(define-public crate-solana-logger-1.18.4 (c (n "solana-logger") (v "1.18.4") (d (list (d (n "env_logger") (r "^0.9.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "0lxaslyw1j9w49h9fxi482yqgv82hp2vzs7xfna31mqj1kq0gxf2")))

(define-public crate-solana-logger-1.17.24 (c (n "solana-logger") (v "1.17.24") (d (list (d (n "env_logger") (r "^0.9.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "10pb911ch5d6rzz5zz2rhj1qqrn8a46n6s24f492fcy8c36vq3wc")))

(define-public crate-solana-logger-1.17.25 (c (n "solana-logger") (v "1.17.25") (d (list (d (n "env_logger") (r "^0.9.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "1zf69d3lnw5cvh6byx0wmksrc9hwwgax9gazgp3hfi1d61s9g5gn")))

(define-public crate-solana-logger-1.18.5 (c (n "solana-logger") (v "1.18.5") (d (list (d (n "env_logger") (r "^0.9.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "1r277l2k5ynl2fdbpfky2wh8f49fkazcdccinl08yp2xalvvlhx4")))

(define-public crate-solana-logger-1.17.26 (c (n "solana-logger") (v "1.17.26") (d (list (d (n "env_logger") (r "^0.9.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "041pncvfghpidl8zz49ib01xa5fmql4jhgyfcv93rbdb5d4j2xz7")))

(define-public crate-solana-logger-1.18.6 (c (n "solana-logger") (v "1.18.6") (d (list (d (n "env_logger") (r "^0.9.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "1z28ybgnih2lv0pz46bq66v1qhxid5dv7ynca8ywsdkmzvb831iv")))

(define-public crate-solana-logger-1.17.27 (c (n "solana-logger") (v "1.17.27") (d (list (d (n "env_logger") (r "^0.9.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "0b0pb1kpk7w58fq6r56c6dd49ryzkbk6psy48vgqkvbx74xamn3b")))

(define-public crate-solana-logger-1.18.7 (c (n "solana-logger") (v "1.18.7") (d (list (d (n "env_logger") (r "^0.9.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "02041hm7zaaw60kjpb5r3k7xv8jciygigk0dwkjhf8cplafrhcnj")))

(define-public crate-solana-logger-1.18.8 (c (n "solana-logger") (v "1.18.8") (d (list (d (n "env_logger") (r "^0.9.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "18n72jc5pxd5ya8mz606gy663njxiimfjsbiz8cw5bfkmnp5jmbw")))

(define-public crate-solana-logger-1.17.28 (c (n "solana-logger") (v "1.17.28") (d (list (d (n "env_logger") (r "^0.9.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "1532qz3qni6vqqhzcq0f9ip2aimzadr96v2ni99xqclnyfd82gvd")))

(define-public crate-solana-logger-1.18.9 (c (n "solana-logger") (v "1.18.9") (d (list (d (n "env_logger") (r "^0.9.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "1sq2cr66xk6flaqxj9m0s8cd313rsxr347fb181w7hl2bpwfxczm")))

(define-public crate-solana-logger-1.17.29 (c (n "solana-logger") (v "1.17.29") (d (list (d (n "env_logger") (r "^0.9.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "1nrws509hx772jlzgwkpwb9dbyk77n412sv5lisvy0hg6rm58ggm")))

(define-public crate-solana-logger-1.17.30 (c (n "solana-logger") (v "1.17.30") (d (list (d (n "env_logger") (r "^0.9.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "0r6fkcrinfbpvdm01vpfha51dgxkls4qj5v9bf27xl26y2sdbh5z")))

(define-public crate-solana-logger-1.18.10 (c (n "solana-logger") (v "1.18.10") (d (list (d (n "env_logger") (r "^0.9.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "1y75svrl66j93px3cz97gb1wn05bb9isfay8p5667b3cx56mfh2m")))

(define-public crate-solana-logger-1.18.11 (c (n "solana-logger") (v "1.18.11") (d (list (d (n "env_logger") (r "^0.9.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "1pya9i4fxsagryfdwppsr2xaa6hzf94a4qpxl2hs8yznksc619gy")))

(define-public crate-solana-logger-1.17.31 (c (n "solana-logger") (v "1.17.31") (d (list (d (n "env_logger") (r "^0.9.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "0758gyc6lg99lafcgk0qgwbnmb8bswjxpzrrf47gag5q4j7pr5y5")))

(define-public crate-solana-logger-1.18.12 (c (n "solana-logger") (v "1.18.12") (d (list (d (n "env_logger") (r "^0.9.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "14nxps6yr0zix1np87ygff0n938jyd6si3dyjly1f4j6h0kfyyfx")))

(define-public crate-solana-logger-1.17.32 (c (n "solana-logger") (v "1.17.32") (d (list (d (n "env_logger") (r "^0.9.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "1px794hfdk4iij9p91d3kamav451kbcqp01h7awdwih99zgwm5lm")))

(define-public crate-solana-logger-1.17.33 (c (n "solana-logger") (v "1.17.33") (d (list (d (n "env_logger") (r "^0.9.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "0rvq6jy0yz23qfrn9haq1mj50yb0xrwjj4cg8053ag46nirgjxmy")))

(define-public crate-solana-logger-1.18.13 (c (n "solana-logger") (v "1.18.13") (d (list (d (n "env_logger") (r "^0.9.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "1i2ms2xacbkfnzymyay8lnqlg7fspzg8d4c1j3a8znaz1lq9g70a")))

(define-public crate-solana-logger-1.18.14 (c (n "solana-logger") (v "1.18.14") (d (list (d (n "env_logger") (r "^0.9.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "199j4qhmx1av1iypcs21hlqwqj8c3wa418wqmk2v3vhhaw8pm1cd")))

(define-public crate-solana-logger-1.17.34 (c (n "solana-logger") (v "1.17.34") (d (list (d (n "env_logger") (r "^0.9.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "137alxfd1ql6zj1xnwz5gnx96b5xns80ns35rkcycg96ya0z5d34")))

