(define-module (crates-io so la solana-frozen-abi-macro) #:use-module (crates-io))

(define-public crate-solana-frozen-abi-macro-1.4.2 (c (n "solana-frozen-abi-macro") (v "1.4.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "14237a1vfjl5cnx37i2zi7a7vnnylmany0mzfgy57zwrm9asw706")))

(define-public crate-solana-frozen-abi-macro-1.4.3 (c (n "solana-frozen-abi-macro") (v "1.4.3") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0jb9qxm430mwxpbqn54q3y3jlc7awri0agf836v2aq7lsmvs3d5x")))

(define-public crate-solana-frozen-abi-macro-1.4.4 (c (n "solana-frozen-abi-macro") (v "1.4.4") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1m1l2an6i9xsx5hhln8ia5hcpxi1anic2b2dyc7xcw8bdlqyd11w")))

(define-public crate-solana-frozen-abi-macro-1.4.5 (c (n "solana-frozen-abi-macro") (v "1.4.5") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0rkvpklzvf8lpmvhv3j73k7774m5cy2h8j6fxv2nw85nb1ix6l3v")))

(define-public crate-solana-frozen-abi-macro-1.4.6 (c (n "solana-frozen-abi-macro") (v "1.4.6") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "12izhhbc3hkpb1880rdmxfj89l3wxvsck9lvkkgwhc65n0dgwns8")))

(define-public crate-solana-frozen-abi-macro-1.4.7 (c (n "solana-frozen-abi-macro") (v "1.4.7") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0s02dyr4dk29gf3a7vxqifrs18v0cvqs97dp4p0mmc0wwgsz2372")))

(define-public crate-solana-frozen-abi-macro-1.4.8 (c (n "solana-frozen-abi-macro") (v "1.4.8") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1583z6fjy228wg0qhnlp1nsl593pc97bfjnxmszx88nk6nasrs7n")))

(define-public crate-solana-frozen-abi-macro-1.4.9 (c (n "solana-frozen-abi-macro") (v "1.4.9") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0x30g3mrh47776rjhd9ibryd33arrcrvnhd489g6qfy4zz7zkp3g")))

(define-public crate-solana-frozen-abi-macro-1.4.10 (c (n "solana-frozen-abi-macro") (v "1.4.10") (d (list (d (n "lazy_static") (r ">=1.4.0, <2.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r ">=1.0.0, <2.0.0") (d #t) (k 0)) (d (n "quote") (r ">=1.0.0, <2.0.0") (d #t) (k 0)) (d (n "rustc_version") (r ">=0.2.0, <0.3.0") (d #t) (k 1)) (d (n "syn") (r ">=1.0.0, <2.0.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1brqd84gx6j8jiw8gynywdnkvwjck434jk6x6m7jxdmmz4cw167a")))

(define-public crate-solana-frozen-abi-macro-1.4.11 (c (n "solana-frozen-abi-macro") (v "1.4.11") (d (list (d (n "lazy_static") (r ">=1.4.0, <2.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r ">=1.0.0, <2.0.0") (d #t) (k 0)) (d (n "quote") (r ">=1.0.0, <2.0.0") (d #t) (k 0)) (d (n "rustc_version") (r ">=0.2.0, <0.3.0") (d #t) (k 1)) (d (n "syn") (r ">=1.0.0, <2.0.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0pffga8mjpvv05im2d6vk8vnvzr5xrjb7nwr0zbkwzcr3wxks76j")))

(define-public crate-solana-frozen-abi-macro-1.4.12 (c (n "solana-frozen-abi-macro") (v "1.4.12") (d (list (d (n "lazy_static") (r ">=1.4.0, <2.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r ">=1.0.0, <2.0.0") (d #t) (k 0)) (d (n "quote") (r ">=1.0.0, <2.0.0") (d #t) (k 0)) (d (n "rustc_version") (r ">=0.2.0, <0.3.0") (d #t) (k 1)) (d (n "syn") (r ">=1.0.0, <2.0.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "049iqh4aa8ad6pcwify113fyzgy0hqnsm4lhivgvwm7kn5hb33fr")))

(define-public crate-solana-frozen-abi-macro-1.4.13 (c (n "solana-frozen-abi-macro") (v "1.4.13") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "14sqlybj90pp36bfvizmq9g2lxp70ia1m25n7qqwdw5n4pq0xmkx")))

(define-public crate-solana-frozen-abi-macro-1.4.14 (c (n "solana-frozen-abi-macro") (v "1.4.14") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1ryrvbda59ryvf0gchsfqdr3z6f6f587i2pdg4a50rwajfm0kapz")))

(define-public crate-solana-frozen-abi-macro-1.4.15 (c (n "solana-frozen-abi-macro") (v "1.4.15") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1kb741i1nigbk5hi3ka84411vw87yacm2ld9g1cjihbplday9rjm")))

(define-public crate-solana-frozen-abi-macro-1.4.16 (c (n "solana-frozen-abi-macro") (v "1.4.16") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1032ap2v9z6617f9fp807rv3n9ccdim750id6i5war0lbymlbdwr")))

(define-public crate-solana-frozen-abi-macro-1.4.17 (c (n "solana-frozen-abi-macro") (v "1.4.17") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0lc3axzdpnkf9lbl2cvv0fa9xwgw1a2bhavidzsmx5waai27ixhr")))

(define-public crate-solana-frozen-abi-macro-1.5.0 (c (n "solana-frozen-abi-macro") (v "1.5.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0zvjsi3ss9n98bgqsw8y2ji4vp4jalgayxs6bprxbcfgcqfnwqaz")))

(define-public crate-solana-frozen-abi-macro-1.4.18 (c (n "solana-frozen-abi-macro") (v "1.4.18") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0dbc880984spwf5rklzbm8yfrj3m8gygxa1zxhl4s352rzszb5pk")))

(define-public crate-solana-frozen-abi-macro-1.4.19 (c (n "solana-frozen-abi-macro") (v "1.4.19") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0fcf684ry22pvaa0bdfqd6df0ggn90pbqi29r2g7d0nrila678m6")))

(define-public crate-solana-frozen-abi-macro-1.4.20 (c (n "solana-frozen-abi-macro") (v "1.4.20") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1nkbh0fgwqxrhxvbqh366paj0hcvi433h5vk93ypchmnn1xj9ghm")))

(define-public crate-solana-frozen-abi-macro-1.5.1 (c (n "solana-frozen-abi-macro") (v "1.5.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "17s28dqw3rx2z2zjk14ksnkniishn19hcp92cjmavbqjalswalck")))

(define-public crate-solana-frozen-abi-macro-1.4.21 (c (n "solana-frozen-abi-macro") (v "1.4.21") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0ah13lp21x5qwzip50jiawmg8bm23avv64x2b9h9812wkypad1sc")))

(define-public crate-solana-frozen-abi-macro-1.4.22 (c (n "solana-frozen-abi-macro") (v "1.4.22") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0l8hx55s04fq75nf1caqbabvq1vfxfw1fylq3iqdp2r6pakc6hq9")))

(define-public crate-solana-frozen-abi-macro-1.5.2 (c (n "solana-frozen-abi-macro") (v "1.5.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "02l0nh3ly7ckhq12110qkn1shr8vgvk2zna58yf142p3b1wr9x4h")))

(define-public crate-solana-frozen-abi-macro-1.4.23 (c (n "solana-frozen-abi-macro") (v "1.4.23") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1iw9a49zycqxk9rrbp2vl5h9d5q8m1br5cqla81yp02cd2c7ia1v")))

(define-public crate-solana-frozen-abi-macro-1.5.3 (c (n "solana-frozen-abi-macro") (v "1.5.3") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0lm1db3kqkw47paz6av9726696zv3clkghkfzkbf47fvmr5g12hb")))

(define-public crate-solana-frozen-abi-macro-1.5.4 (c (n "solana-frozen-abi-macro") (v "1.5.4") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1my93ra25rjaayx6yzk2fvsfdfm1wg64s629cdx08nmlp7vl0saq")))

(define-public crate-solana-frozen-abi-macro-1.5.5 (c (n "solana-frozen-abi-macro") (v "1.5.5") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0id5h23kplzi76frkm2fscnb8ly96rn6a3696m2rx3p7d1a34i6b")))

(define-public crate-solana-frozen-abi-macro-1.4.25 (c (n "solana-frozen-abi-macro") (v "1.4.25") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "11mhi480cp2klz8qgmfwih38bg6jka90qwih5wqzz77dnxc070zq")))

(define-public crate-solana-frozen-abi-macro-1.4.26 (c (n "solana-frozen-abi-macro") (v "1.4.26") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0navcs44nczmnnd27q6s4n2skshv7929dby1bjjy5frhal2gwb5b")))

(define-public crate-solana-frozen-abi-macro-1.5.6 (c (n "solana-frozen-abi-macro") (v "1.5.6") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "159ds5aq65xk4y7ccz3dmwaaqivshrmssx9gdv4hncdi4pklw04z")))

(define-public crate-solana-frozen-abi-macro-1.4.27 (c (n "solana-frozen-abi-macro") (v "1.4.27") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0rzmdz4ljvjf5f21zhpy8kwcck2w70fjms7lz9fz7d794jy922a0")))

(define-public crate-solana-frozen-abi-macro-1.5.7 (c (n "solana-frozen-abi-macro") (v "1.5.7") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1m2kdm72vw9yracqfm9ix9imqq3fhacmihxmjx98v89mknnnfz2w")))

(define-public crate-solana-frozen-abi-macro-1.5.8 (c (n "solana-frozen-abi-macro") (v "1.5.8") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "067w2k60nwvyb2fkpgvk7561dfln7p8sj65hm15kxdgixydia1gr")))

(define-public crate-solana-frozen-abi-macro-1.4.28 (c (n "solana-frozen-abi-macro") (v "1.4.28") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "07w8zsz6aw3l1w44147d6hl8c3vriqyy0y9w1qr8pn7f1xr940rb")))

(define-public crate-solana-frozen-abi-macro-1.5.9 (c (n "solana-frozen-abi-macro") (v "1.5.9") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1lbqwvk7gzr3lm8jdgp2x2r9722w7kcq7j6l6xyhqfzx9qy6zgvh") (y #t)))

(define-public crate-solana-frozen-abi-macro-1.5.10 (c (n "solana-frozen-abi-macro") (v "1.5.10") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1mv80bdh88r854qcn0fihjj11j1agrd2aqxda79mslkgcqfhr15q")))

(define-public crate-solana-frozen-abi-macro-1.5.11 (c (n "solana-frozen-abi-macro") (v "1.5.11") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "086aarp2jp6msdwnkll7gxdlswqkckghqckavvkmqs1kazkngl2v")))

(define-public crate-solana-frozen-abi-macro-1.5.12 (c (n "solana-frozen-abi-macro") (v "1.5.12") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0b2jrjm153pd1xlmhdy53msc5syn997np7hzz1jlh3gx48dkl7wy")))

(define-public crate-solana-frozen-abi-macro-1.5.13 (c (n "solana-frozen-abi-macro") (v "1.5.13") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1j0z88pwidp0cvcd6nil980nxm5l45grpv18ymsb9n48g8h824g3")))

(define-public crate-solana-frozen-abi-macro-1.5.14 (c (n "solana-frozen-abi-macro") (v "1.5.14") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1b7z37li51a675rrw6rrdlkn9kg9k6kl89cr8hx8ydkz6kg7y4rk")))

(define-public crate-solana-frozen-abi-macro-1.6.0 (c (n "solana-frozen-abi-macro") (v "1.6.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1wc50cfa0az00hm6bcr76b9d0k61d2wckz6n5a7i899f20g2mbiz") (y #t)))

(define-public crate-solana-frozen-abi-macro-1.5.15 (c (n "solana-frozen-abi-macro") (v "1.5.15") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "11lj8611kixcv8v763h1vd0xz8sj8h4cf2rlqz3nxl4ga50vc5dd")))

(define-public crate-solana-frozen-abi-macro-1.6.1 (c (n "solana-frozen-abi-macro") (v "1.6.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1k8r4wj42mpzlswdbvvkq4jn4kisyh7a3chv0h9z8slxq0aylb0b")))

(define-public crate-solana-frozen-abi-macro-1.5.16 (c (n "solana-frozen-abi-macro") (v "1.5.16") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "132g5v9xfcsghpg14sdlrhdq0ni9vm7b0ygvyan4m6pmnjmmgs2i")))

(define-public crate-solana-frozen-abi-macro-1.5.17 (c (n "solana-frozen-abi-macro") (v "1.5.17") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0l4l2yfrn13mxw6243m25q7hcxj3rw80whdw0176wcv8r3yv23cq")))

(define-public crate-solana-frozen-abi-macro-1.6.2 (c (n "solana-frozen-abi-macro") (v "1.6.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1cklxry4lxcbs7wsvg1lksgac69riq9pn538pdm7n2403dwdrf5x")))

(define-public crate-solana-frozen-abi-macro-1.5.18 (c (n "solana-frozen-abi-macro") (v "1.5.18") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1i94gh3j4av5x15snj4a5dcxvwhn542zgxp6bwbq55k715amp8fh")))

(define-public crate-solana-frozen-abi-macro-1.6.3 (c (n "solana-frozen-abi-macro") (v "1.6.3") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1w4rk8jymxi3l4pqqb67nzlr277wsc590xprxzhkizpb9kcmj0p8")))

(define-public crate-solana-frozen-abi-macro-1.6.4 (c (n "solana-frozen-abi-macro") (v "1.6.4") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0dvli4nmrjq2d9jixgqh1aa1y8a6j8hhvayi5lxhyiwf0dcq2i5f")))

(define-public crate-solana-frozen-abi-macro-1.6.5 (c (n "solana-frozen-abi-macro") (v "1.6.5") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1sl9z96x0xaxjy02z0p2ccqy8acxvqr0rajdra6xk1fq60s2p8vi")))

(define-public crate-solana-frozen-abi-macro-1.6.6 (c (n "solana-frozen-abi-macro") (v "1.6.6") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1psyjvf6jbq2x9mx5jbzwz3rk96122cgy68rv84lpk0r9yivdx42")))

(define-public crate-solana-frozen-abi-macro-1.5.19 (c (n "solana-frozen-abi-macro") (v "1.5.19") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "08lrzf9rdd3v1mg40pjr9cjd9f2zpsizmjbil5c7vigpsznd1qxd")))

(define-public crate-solana-frozen-abi-macro-1.6.7 (c (n "solana-frozen-abi-macro") (v "1.6.7") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "17p8vcmsf1nk6an2isxb8xk15sih2xlnvvbbinm9470hqp6vzh64")))

(define-public crate-solana-frozen-abi-macro-1.6.8 (c (n "solana-frozen-abi-macro") (v "1.6.8") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "04icbgn67s7ghzy7mfdim4g1dbldk4ydpi6vdw0b2qcp7ing7rfl")))

(define-public crate-solana-frozen-abi-macro-1.6.9 (c (n "solana-frozen-abi-macro") (v "1.6.9") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0920vic1f4vplpddl3xl5bg1bvkzcid7lal8bvib0h1amfzm4vkn")))

(define-public crate-solana-frozen-abi-macro-1.6.10 (c (n "solana-frozen-abi-macro") (v "1.6.10") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1mcj0fd689mflinb1idx9zikbsqpxxxk5h88an4ywfawi4rd65vq")))

(define-public crate-solana-frozen-abi-macro-1.6.11 (c (n "solana-frozen-abi-macro") (v "1.6.11") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1hzgm8rj4z8sv8msb3s41kz3krbw3zs08mbkhcgi02drvr21hjxa")))

(define-public crate-solana-frozen-abi-macro-1.7.0 (c (n "solana-frozen-abi-macro") (v "1.7.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1qmz621773am2ii1y61kdanwvscqr5a99krzsrjldk3v32hdl5zn")))

(define-public crate-solana-frozen-abi-macro-1.7.1 (c (n "solana-frozen-abi-macro") (v "1.7.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "036xzcim90dhy6vcqsqgrj2vwjsyl089nhwk304cj2hn8z68n1pj")))

(define-public crate-solana-frozen-abi-macro-1.6.12 (c (n "solana-frozen-abi-macro") (v "1.6.12") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "057f5kwiiyggkphi2249lpbsx97rc44lx4qnq7wjqlfpmx771mhf")))

(define-public crate-solana-frozen-abi-macro-1.6.13 (c (n "solana-frozen-abi-macro") (v "1.6.13") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0qd44818df2r60qa5dmp3a4yfcq9xyl80196dknqq84g7jav0hr3")))

(define-public crate-solana-frozen-abi-macro-1.7.2 (c (n "solana-frozen-abi-macro") (v "1.7.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0pc4nfbc43y28silayp7jfywpjxcb6pr7ivmph6j9mp8arsk6nab")))

(define-public crate-solana-frozen-abi-macro-1.6.14 (c (n "solana-frozen-abi-macro") (v "1.6.14") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1c70azr7x0c0f7501d27g6bgnvps1zqv902g9hkazw28mn88gjib")))

(define-public crate-solana-frozen-abi-macro-1.7.3 (c (n "solana-frozen-abi-macro") (v "1.7.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1kss4rpdz64skap2r74m1cydi66xg2mk7mpsvilmwlb8mclilf5w")))

(define-public crate-solana-frozen-abi-macro-1.6.15 (c (n "solana-frozen-abi-macro") (v "1.6.15") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1qb2887hpa7a3v26hpfwv531r62kf1ppnkr8yswhh1blydagfkqy")))

(define-public crate-solana-frozen-abi-macro-1.7.4 (c (n "solana-frozen-abi-macro") (v "1.7.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "051m5nifxdibkd1j1jynvr710ggwfkgwn554x61nc6y9bslmrf5d")))

(define-public crate-solana-frozen-abi-macro-1.6.16 (c (n "solana-frozen-abi-macro") (v "1.6.16") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1bx24ik2p9gl0sf9pyckczzmv6y0scacdci859hgkab2zwxmz2qz")))

(define-public crate-solana-frozen-abi-macro-1.6.17 (c (n "solana-frozen-abi-macro") (v "1.6.17") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1wc8jdyw0yhpkflb7pwk8aim932xrlraa2gqshxd3y193gvfcfpq")))

(define-public crate-solana-frozen-abi-macro-1.7.5 (c (n "solana-frozen-abi-macro") (v "1.7.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "10w942sh2ckbf0bnsv7dny69axaxsqy650k3d6rsy0r6crlaxzjh")))

(define-public crate-solana-frozen-abi-macro-1.7.6 (c (n "solana-frozen-abi-macro") (v "1.7.6") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1m76d6xrlawbqdsbcmc0n24x6r6nm1lavmpkylpwk157s656xb6f")))

(define-public crate-solana-frozen-abi-macro-1.6.18 (c (n "solana-frozen-abi-macro") (v "1.6.18") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "17m6i1kqx3ws0r332a3xgdwnx43p7z4j7v7j7jnp7ik3dbclc01y")))

(define-public crate-solana-frozen-abi-macro-1.6.19 (c (n "solana-frozen-abi-macro") (v "1.6.19") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "01v6iavvjjbjwj79zclw9cvjdjg43c2yzbiq8my4p523pbw6mqr1")))

(define-public crate-solana-frozen-abi-macro-1.7.7 (c (n "solana-frozen-abi-macro") (v "1.7.7") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0cxcsvdvswi3gjcn57k04vqf7h4qq5q6q1xp2jkkiyy0hcmqfi81")))

(define-public crate-solana-frozen-abi-macro-1.7.8 (c (n "solana-frozen-abi-macro") (v "1.7.8") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1vvq93s875pzsfi13ay06h6q49329ilw5rplwvl43v3dq6bf5nx4")))

(define-public crate-solana-frozen-abi-macro-1.6.20 (c (n "solana-frozen-abi-macro") (v "1.6.20") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0ql8qns0b1h7g4a9qkrzjj70izpxsv13a6anj5kk9myq6axwf6yl")))

(define-public crate-solana-frozen-abi-macro-1.7.9 (c (n "solana-frozen-abi-macro") (v "1.7.9") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1zcc1vlc11bscni0ap5wq4aq5wlji6h7bm5kns5lmkzm17nzrzi4")))

(define-public crate-solana-frozen-abi-macro-1.7.10 (c (n "solana-frozen-abi-macro") (v "1.7.10") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0kjak6f7dj7c20bw0271zcin9ydzysgdwdr4k1mj3igxjbj4b2w8")))

(define-public crate-solana-frozen-abi-macro-1.6.21 (c (n "solana-frozen-abi-macro") (v "1.6.21") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1sbbc0pnjckskgy2c6r8phlvdrbafh3sbzmqkkkckvnr0k6anphj")))

(define-public crate-solana-frozen-abi-macro-1.6.22 (c (n "solana-frozen-abi-macro") (v "1.6.22") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1ja88hhawq3gm8w0n276ra1c5wkqwf4va1xc385nzqw65k8md9lc")))

(define-public crate-solana-frozen-abi-macro-1.7.11 (c (n "solana-frozen-abi-macro") (v "1.7.11") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0z5qpypgi79vmhwy9gpvnvdvn4ykprg6kq4f1ibfdplg54qslxm8")))

(define-public crate-solana-frozen-abi-macro-1.6.23 (c (n "solana-frozen-abi-macro") (v "1.6.23") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1a0rl6x8k0vqfllx5mmn1fyc5gb2vzp4jam6byf6fvhs1xi8v3h0")))

(define-public crate-solana-frozen-abi-macro-1.6.24 (c (n "solana-frozen-abi-macro") (v "1.6.24") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1kgvv699wmn970iqn84g02jyi11lxi7slg5a8rvbsp6wbs12apq4")))

(define-public crate-solana-frozen-abi-macro-1.6.25 (c (n "solana-frozen-abi-macro") (v "1.6.25") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1phq295hh3riypc625dvigd1b9wv66rh6bk57d10lsdv9sj881sa")))

(define-public crate-solana-frozen-abi-macro-1.7.12 (c (n "solana-frozen-abi-macro") (v "1.7.12") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0qbvcdb57kb6n05q0vg21nzcqdy1zgh65lc3xs3m7rmkykkmmvxa")))

(define-public crate-solana-frozen-abi-macro-1.6.26 (c (n "solana-frozen-abi-macro") (v "1.6.26") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1dlaxrwfs52l2sjr2hbrngrhv5b26ydbr826plg9xhhnd29ywzhh")))

(define-public crate-solana-frozen-abi-macro-1.6.27 (c (n "solana-frozen-abi-macro") (v "1.6.27") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1rql01pkmfl3vhqbl58ki1m9xd8klryn5hb3klfmrqh734xxa0l7")))

(define-public crate-solana-frozen-abi-macro-1.7.13 (c (n "solana-frozen-abi-macro") (v "1.7.13") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "09bdmjaxrm41kis4lrvp136m4s802pjk1sh58zn5hz13hmq56zv8")))

(define-public crate-solana-frozen-abi-macro-1.7.14 (c (n "solana-frozen-abi-macro") (v "1.7.14") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0ywhj2gy3ndjx8fxcw3fw0lvx9izj83zrqdxqf78a3w7a0mr4zfl")))

(define-public crate-solana-frozen-abi-macro-1.8.0 (c (n "solana-frozen-abi-macro") (v "1.8.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0a4nvznzw2yrldrq6yygxh5zrj6nd39hkpdb1fkiijf8fnwy52li")))

(define-public crate-solana-frozen-abi-macro-1.6.28 (c (n "solana-frozen-abi-macro") (v "1.6.28") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0bz87868223ciglmnm6jhxvgzvang4gc9jd3iyhsdjw10iq3mz7z")))

(define-public crate-solana-frozen-abi-macro-1.7.15 (c (n "solana-frozen-abi-macro") (v "1.7.15") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "056w2v9h5h5ml7n6wr7hmza7is1xbr054hmbnxfdw76akbqng7ik")))

(define-public crate-solana-frozen-abi-macro-1.8.1 (c (n "solana-frozen-abi-macro") (v "1.8.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1y9xdzgijcyqyd79qbg6bqvjyjr3mpkmf17wlq8a6213mf24qgys")))

(define-public crate-solana-frozen-abi-macro-1.7.16 (c (n "solana-frozen-abi-macro") (v "1.7.16") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0g91wxfz2m7l8zg8rzvm3bb2wr6lxgh01h12n8c9sp9bgqsv1f4w")))

(define-public crate-solana-frozen-abi-macro-1.7.17 (c (n "solana-frozen-abi-macro") (v "1.7.17") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1y9ax3wya5nqd2y6jk8xvjdn5hslb30hv3yyj21lifca1hnq1apv")))

(define-public crate-solana-frozen-abi-macro-1.8.2 (c (n "solana-frozen-abi-macro") (v "1.8.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0qczwv8aszgs965fafljm2z0x71qg12002i5cz04lq3h9whvacnm")))

(define-public crate-solana-frozen-abi-macro-1.8.3 (c (n "solana-frozen-abi-macro") (v "1.8.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "00pkx5yakqif6jl4a7zbn83a90sfpyh1ak1xdk5ykwy358l5qqwv")))

(define-public crate-solana-frozen-abi-macro-1.8.4 (c (n "solana-frozen-abi-macro") (v "1.8.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "016x6k5gq91jn3mpm6675cazs2imfjzy8hxhapfzgx15axdgxny8")))

(define-public crate-solana-frozen-abi-macro-1.8.5 (c (n "solana-frozen-abi-macro") (v "1.8.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "087rdg76vjgbi8qlri1z5asf7fg290m06ha003r87ayvblvfh0ic")))

(define-public crate-solana-frozen-abi-macro-1.8.6 (c (n "solana-frozen-abi-macro") (v "1.8.6") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0w5niw16b5gqngkrz2zavkcfqhasb14a6sgpz4bf5gavcam75wv9")))

(define-public crate-solana-frozen-abi-macro-1.8.7 (c (n "solana-frozen-abi-macro") (v "1.8.7") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0fpw28cng4724cvp5p929vj8z8cgjpk0rancgf6rddlzxjyymjs2")))

(define-public crate-solana-frozen-abi-macro-1.8.8 (c (n "solana-frozen-abi-macro") (v "1.8.8") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "00p17qisqsc4ang3vdjk3pr1kqwm10n0g88v4pxxyjl87l54c9q3")))

(define-public crate-solana-frozen-abi-macro-1.8.9 (c (n "solana-frozen-abi-macro") (v "1.8.9") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "05j0hjbfkl5a01csvlwjx7klq78im6nlph10bw1q5byznmkb57zf")))

(define-public crate-solana-frozen-abi-macro-1.9.0 (c (n "solana-frozen-abi-macro") (v "1.9.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1b7d148phnw79if9006kdbrj4sdbfizq7gydxdwh30zhyyj3nz8w")))

(define-public crate-solana-frozen-abi-macro-1.8.10 (c (n "solana-frozen-abi-macro") (v "1.8.10") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "15rsvqpl5ph1f0qc8gb5f57ghg9dkpy0zpyxqnss4h59159r73yx")))

(define-public crate-solana-frozen-abi-macro-1.8.11 (c (n "solana-frozen-abi-macro") (v "1.8.11") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "115xgcvmnqgl9yyqwnyq3g6m3bqkrrpi80k1i6sdbc2grz3kibyr")))

(define-public crate-solana-frozen-abi-macro-1.9.1 (c (n "solana-frozen-abi-macro") (v "1.9.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0amf544xjllayvap1hjpf3wjwk11z0kih756jyh1z0mdrywy28a2")))

(define-public crate-solana-frozen-abi-macro-1.9.2 (c (n "solana-frozen-abi-macro") (v "1.9.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "08x42k9injzhpq04zkimw0niqh8gxmczq2vqbsq4qsnr6hpipsdm")))

(define-public crate-solana-frozen-abi-macro-1.9.3 (c (n "solana-frozen-abi-macro") (v "1.9.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1cpnvq74g926pllygch2h0fcpxzcbha285xcma7qaqfxxgjf7mrg")))

(define-public crate-solana-frozen-abi-macro-1.8.12 (c (n "solana-frozen-abi-macro") (v "1.8.12") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "05f9pdw260v8gai9xdgd3vq7vrrga3msc1lgnjc8c3rl1658wydm")))

(define-public crate-solana-frozen-abi-macro-1.9.4 (c (n "solana-frozen-abi-macro") (v "1.9.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1a5342fx97zychkmwgfyrl081mfhj9gibpf4xhqkd69mv8qdd9sn")))

(define-public crate-solana-frozen-abi-macro-1.8.13 (c (n "solana-frozen-abi-macro") (v "1.8.13") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1dbzm8c5sghnn8b53wxc9shb7d7lc0f3xwxyjy6cv96h61q44n3m")))

(define-public crate-solana-frozen-abi-macro-1.9.5 (c (n "solana-frozen-abi-macro") (v "1.9.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0x2zsl5zd6a6i8d1av535zjh921wzw7qbaclpw0s0f1iw8g1973z")))

(define-public crate-solana-frozen-abi-macro-1.8.14 (c (n "solana-frozen-abi-macro") (v "1.8.14") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0f1iqi8qy0gxsr4qwacigar57amq8f628cq5rqzwjnrvwb83n6dj")))

(define-public crate-solana-frozen-abi-macro-1.9.6 (c (n "solana-frozen-abi-macro") (v "1.9.6") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1rx4a5gsadwwria1aha9lgplnwrz2dd1zyw0rcssvlq8kr5p9q0x")))

(define-public crate-solana-frozen-abi-macro-1.9.7 (c (n "solana-frozen-abi-macro") (v "1.9.7") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "01r2gamg8jk2282dcjcmydcbsbylf1jispfq06mnxc519zczjfif")))

(define-public crate-solana-frozen-abi-macro-1.8.16 (c (n "solana-frozen-abi-macro") (v "1.8.16") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "04xsi2vi88xqgf400g2sx8gxi6ljs5lcyq70ng4l431q6iy775wd")))

(define-public crate-solana-frozen-abi-macro-1.9.8 (c (n "solana-frozen-abi-macro") (v "1.9.8") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "18gyb7sz45wfvl0qfcb77ax3lzrp5lghdkzm1bjpnjklcfr5iqav")))

(define-public crate-solana-frozen-abi-macro-1.9.9 (c (n "solana-frozen-abi-macro") (v "1.9.9") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "12294sp2hw3ka7hfdc2kjpifz1r7p8n057qkfmb6xijw50g8mw50")))

(define-public crate-solana-frozen-abi-macro-1.10.0 (c (n "solana-frozen-abi-macro") (v "1.10.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "18kvyb7wfmidrj83dgrv0z7vfp6jy5xsg8wvfdcjq14cjdg09gwn")))

(define-public crate-solana-frozen-abi-macro-1.9.10 (c (n "solana-frozen-abi-macro") (v "1.9.10") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0hvrsqzq89hfjxa2ir4q2ncnf7hv3r0jsac21big0w2jkwxxw3qr")))

(define-public crate-solana-frozen-abi-macro-1.9.11 (c (n "solana-frozen-abi-macro") (v "1.9.11") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0jncdn44a4jbilkyrx4f088hl9nxb6gmhfasj136mvxyd7gc95rx")))

(define-public crate-solana-frozen-abi-macro-1.9.12 (c (n "solana-frozen-abi-macro") (v "1.9.12") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1rbzfgp5qncmnrmv4gn2pzsa9z6f19rl0a8584q5sncpsrz3gzsw")))

(define-public crate-solana-frozen-abi-macro-1.10.1 (c (n "solana-frozen-abi-macro") (v "1.10.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0hgd0wdlzbmj9crka02w4bbggrpgga66gzrs3ik85v3dc0hlkdjj")))

(define-public crate-solana-frozen-abi-macro-1.10.2 (c (n "solana-frozen-abi-macro") (v "1.10.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "09xjxbbyggnm1v08pg7355a33w9s68cdnyaj6aqrna4addd08y9m")))

(define-public crate-solana-frozen-abi-macro-1.9.13 (c (n "solana-frozen-abi-macro") (v "1.9.13") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "01lpv6i5jandzpwyl03iivcxgg7cpd76a4ycb8v0kl38y7lb6hyk")))

(define-public crate-solana-frozen-abi-macro-1.10.3 (c (n "solana-frozen-abi-macro") (v "1.10.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1p8wrc90zl3ixrxldbaziak48ax9zz1xvqa3fi0h35pcnalb7mmj")))

(define-public crate-solana-frozen-abi-macro-1.9.14 (c (n "solana-frozen-abi-macro") (v "1.9.14") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0xmpzjm4s8s7wnp4w0sb0k8f31yhc6s5jafvz76z8s9s1cnwiqzr")))

(define-public crate-solana-frozen-abi-macro-1.10.4 (c (n "solana-frozen-abi-macro") (v "1.10.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "11yp683061jnfgm3047b43xs7ma2688icrhmfvvn89p8w6gpw02v")))

(define-public crate-solana-frozen-abi-macro-1.10.5 (c (n "solana-frozen-abi-macro") (v "1.10.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1cgjw2bmzhn7z9sb96cbrvgp2di7fp9213cc2v6nmh9j6hqva71d")))

(define-public crate-solana-frozen-abi-macro-1.10.6 (c (n "solana-frozen-abi-macro") (v "1.10.6") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0lk9hllv3v3qdzzgla7hl5qialq0nlib4yyd8wpa9kb6c3sw7n18")))

(define-public crate-solana-frozen-abi-macro-1.9.15 (c (n "solana-frozen-abi-macro") (v "1.9.15") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0sl9zk3jdsk1dfxyk3cbxim6j8nxw2f6v5l3w9j5w4395y4bybpk")))

(define-public crate-solana-frozen-abi-macro-1.10.7 (c (n "solana-frozen-abi-macro") (v "1.10.7") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1h753h0f2p5ydszycgfbjappqykvxmfx2nd5k373k4prgvfgm6j8")))

(define-public crate-solana-frozen-abi-macro-1.10.8 (c (n "solana-frozen-abi-macro") (v "1.10.8") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0xwv7mm9mgpppl4xb2avga3k0z493cyp3hfvcxq2p8wxwfxz00qs")))

(define-public crate-solana-frozen-abi-macro-1.9.16 (c (n "solana-frozen-abi-macro") (v "1.9.16") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0rj488bdwwg6mp0j8qim2ib24idds5vw2rh850zaa5kz9jc60d82")))

(define-public crate-solana-frozen-abi-macro-1.9.17 (c (n "solana-frozen-abi-macro") (v "1.9.17") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1pfhm1225zwzx5qdnilnd9xlix0pwswkyqjfqfvnv1907934przq")))

(define-public crate-solana-frozen-abi-macro-1.10.9 (c (n "solana-frozen-abi-macro") (v "1.10.9") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0j3abvair9h7p8zqkxqkwv3yplbdhij8m0gp32rnc8zz4q88b2vr")))

(define-public crate-solana-frozen-abi-macro-1.9.18 (c (n "solana-frozen-abi-macro") (v "1.9.18") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "15izv2nxj9q8rfl135jm3s9iislax3x3wwp5gn2a7idwz3kxsgfh")))

(define-public crate-solana-frozen-abi-macro-1.10.10 (c (n "solana-frozen-abi-macro") (v "1.10.10") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1y0xwpi6cgpzv0v2bycmsz7vycna38fkr30vm2w1rcmiwpxx49np")))

(define-public crate-solana-frozen-abi-macro-1.10.11 (c (n "solana-frozen-abi-macro") (v "1.10.11") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0z3rg48rf0ig0pd03cnwi2j9hi50nknr80dr0437s4zysalm2x6l")))

(define-public crate-solana-frozen-abi-macro-1.9.19 (c (n "solana-frozen-abi-macro") (v "1.9.19") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0bm53dpjlhcar8khrgb5spimgb7220sppjfqxqywy4imyx0vwk7m")))

(define-public crate-solana-frozen-abi-macro-1.10.12 (c (n "solana-frozen-abi-macro") (v "1.10.12") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0j1098v4bpw0c9pmk5ycp1csdi3k4jchkxby8zcay63h5k1j38yd")))

(define-public crate-solana-frozen-abi-macro-1.9.20 (c (n "solana-frozen-abi-macro") (v "1.9.20") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "113kqkihsf3qzr4ndpxg5yidgyzczqcigz16vzk3blzm9fszybs0")))

(define-public crate-solana-frozen-abi-macro-1.10.13 (c (n "solana-frozen-abi-macro") (v "1.10.13") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1x3745wxsiccmrs1yycfcqi3i0x4dqkzvd7np30rmpdm3cdccdwb")))

(define-public crate-solana-frozen-abi-macro-1.9.21 (c (n "solana-frozen-abi-macro") (v "1.9.21") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "18ixp8s8mki3h0q1j0pffrcs87431gp7by9wjjqgfhjbnnlvz8b9")))

(define-public crate-solana-frozen-abi-macro-1.10.14 (c (n "solana-frozen-abi-macro") (v "1.10.14") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0v8khn0vg3j79h5x2i3c6frhk6sxpz5wk1d68kb9gycrd6xjq09h")))

(define-public crate-solana-frozen-abi-macro-1.9.22 (c (n "solana-frozen-abi-macro") (v "1.9.22") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1606q8r2izkqdxilw4ivj7vwqxnd11h02yssha2ix6mib3rhag80")))

(define-public crate-solana-frozen-abi-macro-1.10.15 (c (n "solana-frozen-abi-macro") (v "1.10.15") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0wyqn7iisjpbzancnzzlxyykdllr42q0b7j8fmvjchrcy568ymh9")))

(define-public crate-solana-frozen-abi-macro-1.10.16 (c (n "solana-frozen-abi-macro") (v "1.10.16") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0z2hwjcflv63477xgmm4n1j8cygpa3hmpx0cpw78byfbviif758v")))

(define-public crate-solana-frozen-abi-macro-1.10.17 (c (n "solana-frozen-abi-macro") (v "1.10.17") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1dmqczwpvyc3yh6w0mms1wlkv9gbg10a1j52d80q0rikn37kyn0b")))

(define-public crate-solana-frozen-abi-macro-1.10.18 (c (n "solana-frozen-abi-macro") (v "1.10.18") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1ikr0mdqg0rzidrf4vrcnvpbh2vx17ph8xkcddzszpcksxzd1nk0")))

(define-public crate-solana-frozen-abi-macro-1.9.23 (c (n "solana-frozen-abi-macro") (v "1.9.23") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0llcbzdrqx6q1y96wbz6kg9dc7cas9rwhsrcxdfbskmsbkrvs7r2")))

(define-public crate-solana-frozen-abi-macro-1.10.19 (c (n "solana-frozen-abi-macro") (v "1.10.19") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "17khckaylibvvpgrci7c4062yimqzr3m9dzvlsiasqphqxafsx9c")))

(define-public crate-solana-frozen-abi-macro-1.9.24 (c (n "solana-frozen-abi-macro") (v "1.9.24") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0a9a0hwk7z4n1mml1zpas3vl5a55vm6nbp9kgiwvfd1012q3n561")))

(define-public crate-solana-frozen-abi-macro-1.9.25 (c (n "solana-frozen-abi-macro") (v "1.9.25") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0mjxfz4jgk9899razzcjakhdv4y9xbbysirgzmlz0ia8xiv29b30")))

(define-public crate-solana-frozen-abi-macro-1.10.20 (c (n "solana-frozen-abi-macro") (v "1.10.20") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "194vdmy24infqirjyilp4clidf93gq7hnjrbqnsm5dfi74qww9kj")))

(define-public crate-solana-frozen-abi-macro-1.9.26 (c (n "solana-frozen-abi-macro") (v "1.9.26") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "17hk3gv4djk4q6hg3yl6kzrjmmwhx2948fabl6x11w2ih8rramfg")))

(define-public crate-solana-frozen-abi-macro-1.10.21 (c (n "solana-frozen-abi-macro") (v "1.10.21") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1smjsvgrjwbjigwaf3kcib8h7avpk0hzy4fr4r6y92g5lcc2v45q")))

(define-public crate-solana-frozen-abi-macro-1.10.22 (c (n "solana-frozen-abi-macro") (v "1.10.22") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1jsh3f4b0bk0nhil3nzyyxc6i8f3qg7f6l2y20gyghan22g1j5ci")))

(define-public crate-solana-frozen-abi-macro-1.9.28 (c (n "solana-frozen-abi-macro") (v "1.9.28") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1mjrfjp4shkh5fhnlkc8cih8g5pn93pixc88ags5vzq8349c1q55")))

(define-public crate-solana-frozen-abi-macro-1.10.23 (c (n "solana-frozen-abi-macro") (v "1.10.23") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0bjc9lciy1akdkl2ip8ixmmgfaw4plrg12r1yr11x718rv5b7w49")))

(define-public crate-solana-frozen-abi-macro-1.10.24 (c (n "solana-frozen-abi-macro") (v "1.10.24") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0wsgs7nfswdqbcwdag85bfqwr95srrlqy7zlqvc6m9aszjg06i4h")))

(define-public crate-solana-frozen-abi-macro-1.10.25 (c (n "solana-frozen-abi-macro") (v "1.10.25") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1hjqwwxvcwn263g3js5i4icpdykxxsfxc94bhqsf2b55dvbdgsbd")))

(define-public crate-solana-frozen-abi-macro-1.9.29 (c (n "solana-frozen-abi-macro") (v "1.9.29") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0zviwgnv91yynxfd1psgfkh6g0xqjy85n1ilvbccrv48vc0v2fnn")))

(define-public crate-solana-frozen-abi-macro-1.10.26 (c (n "solana-frozen-abi-macro") (v "1.10.26") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1zsbcgacw2hwvpfzj3lfzfww964y7d8arbprbn12jmxkx6f1ly8p")))

(define-public crate-solana-frozen-abi-macro-1.11.0 (c (n "solana-frozen-abi-macro") (v "1.11.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1b143c4zidiqsibc4xmnizyjami07y15xqd33clkadvzj9c1lkqa")))

(define-public crate-solana-frozen-abi-macro-1.10.27 (c (n "solana-frozen-abi-macro") (v "1.10.27") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "06pwx4ncpyslx3bbjzzyj8gsbr90hl6jrh5p55yvkqcc5rbc0m93")))

(define-public crate-solana-frozen-abi-macro-1.11.1 (c (n "solana-frozen-abi-macro") (v "1.11.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1xfa3867fhk5wx6jxkn3sl2m456xd89d8zyllcv44pywvvhk275h")))

(define-public crate-solana-frozen-abi-macro-1.10.28 (c (n "solana-frozen-abi-macro") (v "1.10.28") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1d89lralraigpsm0pkrzqrqwyvaizhnarqiy2d0smkjlla9pd8c5")))

(define-public crate-solana-frozen-abi-macro-1.10.29 (c (n "solana-frozen-abi-macro") (v "1.10.29") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1bvyii13k4vh0rlq61a3fc2frq5506z17gkfvcpdczcfkznk7i2y")))

(define-public crate-solana-frozen-abi-macro-1.10.30 (c (n "solana-frozen-abi-macro") (v "1.10.30") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0i05ra1a28k258ra9rfnbi2fkljv1zii7riz9ilfzh36wgz2x0dl")))

(define-public crate-solana-frozen-abi-macro-1.11.2 (c (n "solana-frozen-abi-macro") (v "1.11.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "198x9fk07pvlpn8adldc37v7bxln29mjy8gcv719pl8qsw1d3dss")))

(define-public crate-solana-frozen-abi-macro-1.10.31 (c (n "solana-frozen-abi-macro") (v "1.10.31") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1032arg2qjd0rj11h9gdf02093sy1d4i29j5s66s40vlhjnkmlhc")))

(define-public crate-solana-frozen-abi-macro-1.11.3 (c (n "solana-frozen-abi-macro") (v "1.11.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "1l5q4935mqgccddfjjgsmhagmhdkv6bx0pdxnb7prlc7yg98q7sg")))

(define-public crate-solana-frozen-abi-macro-1.10.32 (c (n "solana-frozen-abi-macro") (v "1.10.32") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "05nypzsqi8kivw5cvs6bchsfv6c5k95am6czqn3fxsad6vdfwrfb")))

(define-public crate-solana-frozen-abi-macro-1.11.4 (c (n "solana-frozen-abi-macro") (v "1.11.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "03bqwl64i50bf2wpc2mcinjg9fp6spia4qzfg0i3vgmmcdfxz1bp")))

(define-public crate-solana-frozen-abi-macro-1.10.33 (c (n "solana-frozen-abi-macro") (v "1.10.33") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0ka9201k2i2sg6sjk7yi37wsy25rd184c9f6fbr7ld5ijsn60p33")))

(define-public crate-solana-frozen-abi-macro-1.10.34 (c (n "solana-frozen-abi-macro") (v "1.10.34") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0n6c5f87mcbzkgg24pbhqjwxamdiddhq9rjddjac5g2akjnk8lxl")))

(define-public crate-solana-frozen-abi-macro-1.11.5 (c (n "solana-frozen-abi-macro") (v "1.11.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "06i96q7my5l14g843l0hph5k7fk998gsnl5jacg4nmwffiyq73vh")))

(define-public crate-solana-frozen-abi-macro-1.10.35 (c (n "solana-frozen-abi-macro") (v "1.10.35") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1x4bg7p9128vlmlq3nmbki9k9lcxvd7s3flvbf4b9p8k4ifwmnyq")))

(define-public crate-solana-frozen-abi-macro-1.11.6 (c (n "solana-frozen-abi-macro") (v "1.11.6") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "0w8ag8gl7qqi0zpbi9nx1i3s80g7cwdzhgsbmvjcagyz0g5pq27q")))

(define-public crate-solana-frozen-abi-macro-1.11.7 (c (n "solana-frozen-abi-macro") (v "1.11.7") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "1vghhfnqcp84ic693i4d2h7l85w31s55lx2g8z09x6qdnsj3rp5v")))

(define-public crate-solana-frozen-abi-macro-1.11.8 (c (n "solana-frozen-abi-macro") (v "1.11.8") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "0bckr69r4vfq930p4w1qapwhyh9zqrvgjry0cjq9wwpn6askmadc")))

(define-public crate-solana-frozen-abi-macro-1.11.10 (c (n "solana-frozen-abi-macro") (v "1.11.10") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "0w7l1gvj00n5gnnxwksman82vc2vvfw6kpjyadh7jsd2dw754878")))

(define-public crate-solana-frozen-abi-macro-1.10.38 (c (n "solana-frozen-abi-macro") (v "1.10.38") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1zcpqgs54p7zsghkxyz4qxp1fxa9av1v3mmjgs7k79c20sqzr718")))

(define-public crate-solana-frozen-abi-macro-1.13.0 (c (n "solana-frozen-abi-macro") (v "1.13.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1nia9w5qa780h16jiyp8fh16a1y8ln5r6c661bahs2z8x0zlc4lf")))

(define-public crate-solana-frozen-abi-macro-1.14.0 (c (n "solana-frozen-abi-macro") (v "1.14.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "1cp7nyw2bk3v3183fyb2zw48cbp02f0b24yjwlwgv5d7gm5r9qzz")))

(define-public crate-solana-frozen-abi-macro-1.14.1 (c (n "solana-frozen-abi-macro") (v "1.14.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "1lqv1k8cq6202q39pbshkbcinr1m9yaqgj7iaqjrf4a2llrxqpxs")))

(define-public crate-solana-frozen-abi-macro-1.10.39 (c (n "solana-frozen-abi-macro") (v "1.10.39") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1i8w2x2gjg2800k77r1bvnjbhgwk4wr108s35y9pgid41392r9xr")))

(define-public crate-solana-frozen-abi-macro-1.14.2 (c (n "solana-frozen-abi-macro") (v "1.14.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "1rrjlsv8xb4jdccqavv0kg55ddx3ykg526wcma0knlan8bkkgzay")))

(define-public crate-solana-frozen-abi-macro-1.13.1 (c (n "solana-frozen-abi-macro") (v "1.13.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1x0gpfx6lj8l4lh2jh7v14jkfsmgsd9g6hfg4fi2gpnjnh91k6py")))

(define-public crate-solana-frozen-abi-macro-1.14.3 (c (n "solana-frozen-abi-macro") (v "1.14.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "1x30kbm3ff5rbzmj50iy3nsk7pv23jgwm3hl1m9lygj5mz8wcls2")))

(define-public crate-solana-frozen-abi-macro-1.10.40 (c (n "solana-frozen-abi-macro") (v "1.10.40") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "02123clg9vkzakc2ys2rh69pj3nh7m8hn406wyhshxd8prl85h31")))

(define-public crate-solana-frozen-abi-macro-1.14.4 (c (n "solana-frozen-abi-macro") (v "1.14.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "1drxa5pfiqmaxk79r6caqxkxqw6gw7dhc340i1g9za1s1vvxpbvw")))

(define-public crate-solana-frozen-abi-macro-1.13.2 (c (n "solana-frozen-abi-macro") (v "1.13.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "00w88nnmm4chja9c0hyd7rldy9fpjwp35gkmbzbvxvff2a4xpi1p")))

(define-public crate-solana-frozen-abi-macro-1.14.5 (c (n "solana-frozen-abi-macro") (v "1.14.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "0bfhi95iv7hhkjb9fi3lczwamhllzhi52r3irasv798gv8lxbmyw")))

(define-public crate-solana-frozen-abi-macro-1.10.41 (c (n "solana-frozen-abi-macro") (v "1.10.41") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1g66zvxdizrf5w5f0hxw2xjbqsq4gzx0fa22rf7v05pzipn12wm3")))

(define-public crate-solana-frozen-abi-macro-1.13.3 (c (n "solana-frozen-abi-macro") (v "1.13.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "17z8m2gh50bvwsa74lvx5zkhlz49p0z1rw34259yrx8zjcxpx7s3")))

(define-public crate-solana-frozen-abi-macro-1.13.4 (c (n "solana-frozen-abi-macro") (v "1.13.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "07cp6il57rz99ws3qr7wmw0jbz8zlk71khd408d0fcqq33sbh5v8")))

(define-public crate-solana-frozen-abi-macro-1.14.6 (c (n "solana-frozen-abi-macro") (v "1.14.6") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "1yrmw9j11cknc0n6zslik5ad675km259vmndcxi09viaz7zd1772")))

(define-public crate-solana-frozen-abi-macro-1.14.7 (c (n "solana-frozen-abi-macro") (v "1.14.7") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "13fhi5a21s5izc4s5b48s27rskgk84xymi7bin2fr372bbz00iky")))

(define-public crate-solana-frozen-abi-macro-1.13.5 (c (n "solana-frozen-abi-macro") (v "1.13.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0jwjgzzbz2dm41ks802yj61jkbn51cmi2rvrk0rkxr8rwq3v69ql")))

(define-public crate-solana-frozen-abi-macro-1.14.8 (c (n "solana-frozen-abi-macro") (v "1.14.8") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "1qsgns8r8kr4rdq9iyiihlmz0zaxl0vdsgnkvz2crqipmdsf9ymn")))

(define-public crate-solana-frozen-abi-macro-1.14.9 (c (n "solana-frozen-abi-macro") (v "1.14.9") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "0akicfib4x9ly6a2m31vqdk9cchm7kazqwml3gryqyac5w3m8789")))

(define-public crate-solana-frozen-abi-macro-1.14.10 (c (n "solana-frozen-abi-macro") (v "1.14.10") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "0djg8ngzj8457945h31sakgajqp5dflwdh8fnklswjbvj93092n8")))

(define-public crate-solana-frozen-abi-macro-1.14.11 (c (n "solana-frozen-abi-macro") (v "1.14.11") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "1f3xal5wrqp49hdsvxxg8pma2w41nzyx4p4l01srcw7774jjhbh6")))

(define-public crate-solana-frozen-abi-macro-1.14.12 (c (n "solana-frozen-abi-macro") (v "1.14.12") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "1kywk98ii1as0hwfmv197igfmf73lx18djz2w0y7m3abgk13mm6s")))

(define-public crate-solana-frozen-abi-macro-1.13.6 (c (n "solana-frozen-abi-macro") (v "1.13.6") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1dxysb2n8ng0zs7qb35izwpwgrrnc7m547hky62jh9hi7x9jrgcf")))

(define-public crate-solana-frozen-abi-macro-1.14.13 (c (n "solana-frozen-abi-macro") (v "1.14.13") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "0qmsfly40ah60kfkqmi1xfw4dhaw1rhr9nwf6khxym1g71xcq8xy")))

(define-public crate-solana-frozen-abi-macro-1.15.0 (c (n "solana-frozen-abi-macro") (v "1.15.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "0lybqjinq8vgfk37rmykyhpnq4h5vv0ak8n3291k9kvlnq2lc257") (y #t)))

(define-public crate-solana-frozen-abi-macro-1.14.14 (c (n "solana-frozen-abi-macro") (v "1.14.14") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "0jc6987qsbcv1zjl0832byw13r2isda9w04gpfp9x4lspk5v3riw")))

(define-public crate-solana-frozen-abi-macro-1.14.15 (c (n "solana-frozen-abi-macro") (v "1.14.15") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "0jspj5q148qjxzx62sa7af27mz793bnv03yvk6vx94pmcsyv86rh")))

(define-public crate-solana-frozen-abi-macro-1.15.1 (c (n "solana-frozen-abi-macro") (v "1.15.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "1as05f8s7nx896q1mnbaaalgym7vvgzpv29b59b55apkn9kq5z7g") (y #t)))

(define-public crate-solana-frozen-abi-macro-1.15.2 (c (n "solana-frozen-abi-macro") (v "1.15.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "1rdvmqp6mfpi1yvap2jlzq0jvw56v1spx2jisbhss44q68l58f86") (y #t)))

(define-public crate-solana-frozen-abi-macro-1.14.16 (c (n "solana-frozen-abi-macro") (v "1.14.16") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "05b37nvgpmbwpj8wnkvk2r7lzlassxn5zq6bqcyssa044lw2b2ap")))

(define-public crate-solana-frozen-abi-macro-1.14.17 (c (n "solana-frozen-abi-macro") (v "1.14.17") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "0yi0czv7m6a1805qd4v69nfais2yyld7xznwpkj2ysn02cksmsns")))

(define-public crate-solana-frozen-abi-macro-1.13.7 (c (n "solana-frozen-abi-macro") (v "1.13.7") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1i3a8qsplyr7737q2bv67aaqn21zql6l64f2rn6r5s9vw5vy53zn")))

(define-public crate-solana-frozen-abi-macro-1.14.18 (c (n "solana-frozen-abi-macro") (v "1.14.18") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "1w5w4ki2jfp50yn9ydywh5mdwd5dijyd7x0jwic6j7fs0i320vsl")))

(define-public crate-solana-frozen-abi-macro-1.16.0 (c (n "solana-frozen-abi-macro") (v "1.16.0") (d (list (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "1j2bbixwkd73il5vhm9avngnpxdd412xpwshpw2pij3drfylnx49")))

(define-public crate-solana-frozen-abi-macro-1.16.1 (c (n "solana-frozen-abi-macro") (v "1.16.1") (d (list (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "1k49prg5a3c4iig368i2vc3ir23iq5z2vkfqr34cwd3j628iy4nl")))

(define-public crate-solana-frozen-abi-macro-1.14.19 (c (n "solana-frozen-abi-macro") (v "1.14.19") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "0mwiy13hssqllimbgyl3cjp9rqpsn575qdm9ws2ixd5bci0v7rrg")))

(define-public crate-solana-frozen-abi-macro-1.16.2 (c (n "solana-frozen-abi-macro") (v "1.16.2") (d (list (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "0lx04w7565fbszdbi7illvlwrd0z1z44ba34nl2vwgfpv5vh2ys7")))

(define-public crate-solana-frozen-abi-macro-1.16.3 (c (n "solana-frozen-abi-macro") (v "1.16.3") (d (list (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "0ai900bn6mc3v3zvqbij6bn3d2cc04ilm9dssn54xk6jdm5vx11g")))

(define-public crate-solana-frozen-abi-macro-1.14.20 (c (n "solana-frozen-abi-macro") (v "1.14.20") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "0hlaxlycinl79dbyy14kdx36m5qjflpjziqvs5kxsd8vc127ylhz")))

(define-public crate-solana-frozen-abi-macro-1.16.4 (c (n "solana-frozen-abi-macro") (v "1.16.4") (d (list (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "1pwsnqjz34qc3jzja9rfy0aswh7l2lws83nv7n1fnv28gdrhhla8")))

(define-public crate-solana-frozen-abi-macro-1.16.5 (c (n "solana-frozen-abi-macro") (v "1.16.5") (d (list (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "1las9ja0v9fd3pkw74f8w0zk5zp77yxwjjh3aaq53i2jsi3a3xf6")))

(define-public crate-solana-frozen-abi-macro-1.14.21 (c (n "solana-frozen-abi-macro") (v "1.14.21") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "0ykba1n8xwff6k49llwyqar87zr7plpdlgzf1z783v827qyfkg7z")))

(define-public crate-solana-frozen-abi-macro-1.14.22 (c (n "solana-frozen-abi-macro") (v "1.14.22") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "0c5ac394jr5qvx2b2v11k4w60i9rgkayiw6vdvqhw882wmslqi2j")))

(define-public crate-solana-frozen-abi-macro-1.16.6 (c (n "solana-frozen-abi-macro") (v "1.16.6") (d (list (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "178p7x2i1vslpn9jvl7gxdjrdwv6pjz972jcva921xb4ln0bmzmi")))

(define-public crate-solana-frozen-abi-macro-1.16.7 (c (n "solana-frozen-abi-macro") (v "1.16.7") (d (list (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "15pdl500si4iawg23hwa38kfdv5b9ijram0phv8mx59gjzmk89cl")))

(define-public crate-solana-frozen-abi-macro-1.14.23 (c (n "solana-frozen-abi-macro") (v "1.14.23") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "15wlz0sghi7v4i28py1g0kr04f4mbys4lp2zy51rlwll185j0q68")))

(define-public crate-solana-frozen-abi-macro-1.16.8 (c (n "solana-frozen-abi-macro") (v "1.16.8") (d (list (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "1ns7zgy9r67li29mgcy6965pi2vqssz8470k3w69g1p0nx1sky89")))

(define-public crate-solana-frozen-abi-macro-1.14.24 (c (n "solana-frozen-abi-macro") (v "1.14.24") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "0ii24fh47f264kc7qpl02fyrl3bxqcqhr7pvh5fc7p41y5ls9v69")))

(define-public crate-solana-frozen-abi-macro-1.16.9 (c (n "solana-frozen-abi-macro") (v "1.16.9") (d (list (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "0cgmjgm2klsyalbcjjrv443w97b61iynl27hs17asp375lyxi7dn")))

(define-public crate-solana-frozen-abi-macro-1.16.10 (c (n "solana-frozen-abi-macro") (v "1.16.10") (d (list (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "0rv8nz2xvj7ldy7vn41y0z05dg6nf72gv6x7m4mrmalbaf8h9w6q")))

(define-public crate-solana-frozen-abi-macro-1.14.25 (c (n "solana-frozen-abi-macro") (v "1.14.25") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "1hv4wf75jvaka4dpgim109xmdp15d80ihafhqvi1amkjlhmk9lgh")))

(define-public crate-solana-frozen-abi-macro-1.16.11 (c (n "solana-frozen-abi-macro") (v "1.16.11") (d (list (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "0zcfg0zd0qwrlnf2i534rcsgfyqfqysvyr3sq22ipjas5achxhw6")))

(define-public crate-solana-frozen-abi-macro-1.14.26 (c (n "solana-frozen-abi-macro") (v "1.14.26") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "0l9kbpg5xjzca5d9hhxgzfr94gv9xa9iqbhw5dpidg4g0rsv5x9s")))

(define-public crate-solana-frozen-abi-macro-1.16.12 (c (n "solana-frozen-abi-macro") (v "1.16.12") (d (list (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "1f2kh5agzq7if5r8xn8cylgb4z3qj71cjvcghyizikl8i08nmj4w")))

(define-public crate-solana-frozen-abi-macro-1.14.27 (c (n "solana-frozen-abi-macro") (v "1.14.27") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "12mk7pyan8rhhmf5fr38fz9z4ajcxmgzb364j04xlb52i2nh0v30")))

(define-public crate-solana-frozen-abi-macro-1.16.13 (c (n "solana-frozen-abi-macro") (v "1.16.13") (d (list (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "02r4j1fikhv3fbi8nnczkw6ya2hcpkzaa2lh9faw0z80yh5an26v")))

(define-public crate-solana-frozen-abi-macro-1.16.14 (c (n "solana-frozen-abi-macro") (v "1.16.14") (d (list (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "0pys96n4id5ihv3h1lj5zhdvmdsmd1gnv7dfhfq8x9l8pmzxxasd")))

(define-public crate-solana-frozen-abi-macro-1.14.28 (c (n "solana-frozen-abi-macro") (v "1.14.28") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "1afgbcsbl7gqpzkgjr24w218cglhmgqwprq59bc9pv4jk8v3vaid")))

(define-public crate-solana-frozen-abi-macro-1.14.29 (c (n "solana-frozen-abi-macro") (v "1.14.29") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "0x5szqq929yhfq94gwiwqdgdsqa6w7apy89wf27z4skmpcqgd40g")))

(define-public crate-solana-frozen-abi-macro-1.16.15 (c (n "solana-frozen-abi-macro") (v "1.16.15") (d (list (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "0948mp5la7lqjzr0svp9sj0z1qlmwa9f4hn5i2grys2hq1fqfpap")))

(define-public crate-solana-frozen-abi-macro-1.17.0 (c (n "solana-frozen-abi-macro") (v "1.17.0") (d (list (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "0a246fvwa5a0k0x5415b4b2al2l8vyan07shfjjd2q3w8g48q4c6")))

(define-public crate-solana-frozen-abi-macro-1.16.16 (c (n "solana-frozen-abi-macro") (v "1.16.16") (d (list (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "0r8pc59jgvgnpj27v8rvf45bhsb3ahq7jgavmp5l8n9839v191gj")))

(define-public crate-solana-frozen-abi-macro-1.17.1 (c (n "solana-frozen-abi-macro") (v "1.17.1") (d (list (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "185sraca3fmlxhgar06j6qbqjkrhpgx9558d7mg7vxkzi444pdvx")))

(define-public crate-solana-frozen-abi-macro-1.16.17 (c (n "solana-frozen-abi-macro") (v "1.16.17") (d (list (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "1pph5i6bj64hfr3s01i0xj8cypvj7fh89pn6jjsdq5ah2p2iizkd")))

(define-public crate-solana-frozen-abi-macro-1.17.2 (c (n "solana-frozen-abi-macro") (v "1.17.2") (d (list (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "09h04yxsw48jy28pl870x88j4r5f1pszwics81m2z8d3m24qrx65")))

(define-public crate-solana-frozen-abi-macro-1.16.18 (c (n "solana-frozen-abi-macro") (v "1.16.18") (d (list (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "0af88nllcvcfjaf39nirx79b3lshd3mprbkyzl956q110hy1pvdw")))

(define-public crate-solana-frozen-abi-macro-1.17.3 (c (n "solana-frozen-abi-macro") (v "1.17.3") (d (list (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "17a4287pqmk0aswln8mn5dhi76a1w7138k7bb6h7kg47snk8944r")))

(define-public crate-solana-frozen-abi-macro-1.17.4 (c (n "solana-frozen-abi-macro") (v "1.17.4") (d (list (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "1k94b747hm5j29190n9bz82yq6qjx1r7k4dn1ppkyfqbshjp72y2")))

(define-public crate-solana-frozen-abi-macro-1.16.19 (c (n "solana-frozen-abi-macro") (v "1.16.19") (d (list (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "1g0fsj233ki19c58n52rns1fy9pv3pdwwqgyygnm8h80zkz1295h")))

(define-public crate-solana-frozen-abi-macro-1.17.5 (c (n "solana-frozen-abi-macro") (v "1.17.5") (d (list (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "14d23rl7ny96c1s51wi8dkjrkc9rqp2b71brry5wrjbvjjbh8sga")))

(define-public crate-solana-frozen-abi-macro-1.17.6 (c (n "solana-frozen-abi-macro") (v "1.17.6") (d (list (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "08m3n56a9v5jmz4w1k08ac2z52vv800a72wl7w52vvp2551izci9")))

(define-public crate-solana-frozen-abi-macro-1.16.20 (c (n "solana-frozen-abi-macro") (v "1.16.20") (d (list (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "0kz93sby4jqndpxy697sfidx3s4s20vnmzha54ygs405kxmqcawr")))

(define-public crate-solana-frozen-abi-macro-1.17.7 (c (n "solana-frozen-abi-macro") (v "1.17.7") (d (list (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "0ddq7ga2siri1idq9x5hqyx6fdg9gdcylvcdd7jprv7pgajbbcwb")))

(define-public crate-solana-frozen-abi-macro-1.16.21 (c (n "solana-frozen-abi-macro") (v "1.16.21") (d (list (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "0qxmb693rz5hrhnlfsymlhbgdvj1lzbbsgm0k6cwxyrj8i7kwimc")))

(define-public crate-solana-frozen-abi-macro-1.17.8 (c (n "solana-frozen-abi-macro") (v "1.17.8") (d (list (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "1w948pndfis8a5h0w770vi545k6xyjkgws6b56wjpgbyggl044l6")))

(define-public crate-solana-frozen-abi-macro-1.16.22 (c (n "solana-frozen-abi-macro") (v "1.16.22") (d (list (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "18jkmc5qmfmzz8png2li6pw7x9vghw5jxgsd6czmrswcfhd5piyx")))

(define-public crate-solana-frozen-abi-macro-1.16.23 (c (n "solana-frozen-abi-macro") (v "1.16.23") (d (list (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "0rc9my2w0af5r5c0rzba8cd62072c0dpyqmx31gxk92kzvnlbsh7")))

(define-public crate-solana-frozen-abi-macro-1.17.9 (c (n "solana-frozen-abi-macro") (v "1.17.9") (d (list (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "0s613mixg0vkw55rczs2ix9bappqlbm4phvwm89r2a885aq69ca9")))

(define-public crate-solana-frozen-abi-macro-1.17.10 (c (n "solana-frozen-abi-macro") (v "1.17.10") (d (list (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "1n9k5pv0p36gasqspdv9anw17dbfnkwln90swzxi5mygy1niljq1")))

(define-public crate-solana-frozen-abi-macro-1.17.11 (c (n "solana-frozen-abi-macro") (v "1.17.11") (d (list (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "0mbckllcfmykxdzhp26mfl1fb86n2ybwmcm8d7gqwc9jy0l7mb12")))

(define-public crate-solana-frozen-abi-macro-1.17.12 (c (n "solana-frozen-abi-macro") (v "1.17.12") (d (list (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "12q611mjg9yhsnpxkgx9nrxm0bxccafs8wmk5ibad33rklxn21m1")))

(define-public crate-solana-frozen-abi-macro-1.16.24 (c (n "solana-frozen-abi-macro") (v "1.16.24") (d (list (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "086hx9a28mhph2hbsvvag84y5kk9jz6basy76ybpm5h614lshxdd")))

(define-public crate-solana-frozen-abi-macro-1.17.13 (c (n "solana-frozen-abi-macro") (v "1.17.13") (d (list (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "02996nlx52qcgqjdpwadjcdaafg2bdjis2b98lcvv4lzdp2rp6z6")))

(define-public crate-solana-frozen-abi-macro-1.17.14 (c (n "solana-frozen-abi-macro") (v "1.17.14") (d (list (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "0yj46d13xg0015k7q71vv6yg2wbga8r7xlg4mwhlr7j7wbq4qvi9")))

(define-public crate-solana-frozen-abi-macro-1.17.15 (c (n "solana-frozen-abi-macro") (v "1.17.15") (d (list (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "0armpbk5060n7cidrzxl4b0hjn34lvzq1dnxsz9sa7n3xk0wsnvn")))

(define-public crate-solana-frozen-abi-macro-1.16.25 (c (n "solana-frozen-abi-macro") (v "1.16.25") (d (list (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "18l81vrp7ml6d2vxiwgrav8h4zn9fl2ijry3wl6vfahs469gj5pm")))

(define-public crate-solana-frozen-abi-macro-1.16.26 (c (n "solana-frozen-abi-macro") (v "1.16.26") (d (list (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "1f7y663inscfpcyrgbv8m31v48zx2mmq3is8lrqybk75yxahk9pv")))

(define-public crate-solana-frozen-abi-macro-1.16.27 (c (n "solana-frozen-abi-macro") (v "1.16.27") (d (list (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "1b74b93q9s8j87cwkkv5dpgwnilpjvxjrj6dkmhbzsrvjrf7dsas")))

(define-public crate-solana-frozen-abi-macro-1.17.16 (c (n "solana-frozen-abi-macro") (v "1.17.16") (d (list (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "0qd1ijc9b9r8clprd7k4pd4didca5fwyf2iysakj77v3wqkhx3kc")))

(define-public crate-solana-frozen-abi-macro-1.17.17 (c (n "solana-frozen-abi-macro") (v "1.17.17") (d (list (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "07z7arn48y9i2zaj5h50nprbfd5hykz89q6av1y5qqzwirvn96va")))

(define-public crate-solana-frozen-abi-macro-1.17.18 (c (n "solana-frozen-abi-macro") (v "1.17.18") (d (list (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "0jpshv21bz6dhqy0710b4qa05dh1ksp2harx171vmki0xb9q9hwg")))

(define-public crate-solana-frozen-abi-macro-1.18.0 (c (n "solana-frozen-abi-macro") (v "1.18.0") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "0h196b99ni10imwnyx8wqigiv35xlp0fldq2px8x7kndxmnw0fp0")))

(define-public crate-solana-frozen-abi-macro-1.18.1 (c (n "solana-frozen-abi-macro") (v "1.18.1") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "0n2xzz3d7m41x9qc2yrb702vr5zlx2j3rxa27m1v2glhk2c0m5wj")))

(define-public crate-solana-frozen-abi-macro-1.17.20 (c (n "solana-frozen-abi-macro") (v "1.17.20") (d (list (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "0sqv7cm93xmfddxvpx9x9w3yzc2g8b4hgrvqppw13qkj39ipl0fi")))

(define-public crate-solana-frozen-abi-macro-1.17.22 (c (n "solana-frozen-abi-macro") (v "1.17.22") (d (list (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "1z9278sf736jlqlgaahg3lyihr72sgs4pwyqqyfh4528y2n6asjd")))

(define-public crate-solana-frozen-abi-macro-1.18.2 (c (n "solana-frozen-abi-macro") (v "1.18.2") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "1ldr1dyprn721z25c42lnzp484iy9ql09rdnxhqaaardhzxqkf92")))

(define-public crate-solana-frozen-abi-macro-1.17.23 (c (n "solana-frozen-abi-macro") (v "1.17.23") (d (list (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "13xqhnw88y65nwrkwqcqsmicxfm9par4nc7vp86aczd7nyjf49qs")))

(define-public crate-solana-frozen-abi-macro-1.18.3 (c (n "solana-frozen-abi-macro") (v "1.18.3") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "0fwa86lj1598rpk1vszayh7vp9yxkwihilrsw5nib6yqqifjirdd")))

(define-public crate-solana-frozen-abi-macro-1.18.4 (c (n "solana-frozen-abi-macro") (v "1.18.4") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "1wnk2pdzgpx7hcwjw76pfk2d8lm2n9qy66xv79zjhxx1v1dabiqw")))

(define-public crate-solana-frozen-abi-macro-1.17.24 (c (n "solana-frozen-abi-macro") (v "1.17.24") (d (list (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "13mrgm0ys25x72j9dcpr426xpiwyiw0fgh5b4gfzwa5vhh141i7r")))

(define-public crate-solana-frozen-abi-macro-1.17.25 (c (n "solana-frozen-abi-macro") (v "1.17.25") (d (list (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "1bvcr492gzg4rjy1l09w9vgqpn5lwxbvcr2mv1abwd2kc2232dz6")))

(define-public crate-solana-frozen-abi-macro-1.18.5 (c (n "solana-frozen-abi-macro") (v "1.18.5") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "0hg48g9x6gplqmvx6699qkril695rh5h7v740da8xi38rdhmifm5")))

(define-public crate-solana-frozen-abi-macro-1.17.26 (c (n "solana-frozen-abi-macro") (v "1.17.26") (d (list (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "0i72hpdl2c0acyd30pjrpcl4v93n5f062gpyj42j9yp40fgc3cd3")))

(define-public crate-solana-frozen-abi-macro-1.18.6 (c (n "solana-frozen-abi-macro") (v "1.18.6") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "04rjzyjm21nzlvwd70prkvmgk3b79hgh674rg250w82bvhqgcb2f")))

(define-public crate-solana-frozen-abi-macro-1.17.27 (c (n "solana-frozen-abi-macro") (v "1.17.27") (d (list (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "00klm3ljz2cz6jzhwillr0zv1r3jfj9435z6mnzf5l2z05rpfypn")))

(define-public crate-solana-frozen-abi-macro-1.18.7 (c (n "solana-frozen-abi-macro") (v "1.18.7") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "1cqgq7hqxrhx49ga4c0624cx92dmq70911nlzp58j1gvk4p8ij3z")))

(define-public crate-solana-frozen-abi-macro-1.18.8 (c (n "solana-frozen-abi-macro") (v "1.18.8") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "03wx5b28fs5sj8m6r7q9p1rsgsdw29g3dzal2jvw187pg4q1idxa")))

(define-public crate-solana-frozen-abi-macro-1.17.28 (c (n "solana-frozen-abi-macro") (v "1.17.28") (d (list (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "1qyviy9npr547bdh7sn4f5bbvym2vn9g9y2pqf03n49vffmi0fbr")))

(define-public crate-solana-frozen-abi-macro-1.18.9 (c (n "solana-frozen-abi-macro") (v "1.18.9") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "1sqh6xn103gm6l81iavcwxl545m7zhpmfql8zly45nh348yn0767")))

(define-public crate-solana-frozen-abi-macro-1.17.29 (c (n "solana-frozen-abi-macro") (v "1.17.29") (d (list (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "1ayb1y2ch1wpkissriq9nc57by7g97djb05xkjzlfx32qfn8s6zj")))

(define-public crate-solana-frozen-abi-macro-1.17.30 (c (n "solana-frozen-abi-macro") (v "1.17.30") (d (list (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "0243sk30ghgh3d8v6mx04x1755aqisg64vkx23axs0yxld4jpvd3")))

(define-public crate-solana-frozen-abi-macro-1.18.10 (c (n "solana-frozen-abi-macro") (v "1.18.10") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "0xvamrc0igici8vfynxhw6n8bfpqp39bhhql2lzlfhdwrjx8fz90")))

(define-public crate-solana-frozen-abi-macro-1.18.11 (c (n "solana-frozen-abi-macro") (v "1.18.11") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "1jdpznz3450ykjnvxrw6r2i0km1j0s4s92x66ih4rdqpmlf28s2a")))

(define-public crate-solana-frozen-abi-macro-1.17.31 (c (n "solana-frozen-abi-macro") (v "1.17.31") (d (list (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "0ajql21wdnj71f4dz9rssbimw5myh5xda6fh6mhh9zb4lj8i43ws")))

(define-public crate-solana-frozen-abi-macro-1.18.12 (c (n "solana-frozen-abi-macro") (v "1.18.12") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "0zzf1vj9mjjkp18bawga41fmzrn2m1vlxlhzk2r3lrnkcr00yq2i")))

(define-public crate-solana-frozen-abi-macro-1.17.32 (c (n "solana-frozen-abi-macro") (v "1.17.32") (d (list (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "17lz2xjhm4qj0hnk588d5q4k1fw0yp8gdl794dcdpvhph0b5z8hm")))

(define-public crate-solana-frozen-abi-macro-1.17.33 (c (n "solana-frozen-abi-macro") (v "1.17.33") (d (list (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "1p1frrpk0r6nli1xhg9vg7awz9q0v2ab81cb1h0kl028agf4cbmq")))

(define-public crate-solana-frozen-abi-macro-1.18.13 (c (n "solana-frozen-abi-macro") (v "1.18.13") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "0797pyld6fcr41p7icnklvj5zi9w6m3rbjqcrr558hi82ywfs91z")))

(define-public crate-solana-frozen-abi-macro-1.18.14 (c (n "solana-frozen-abi-macro") (v "1.18.14") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "0dr6nknp7y05k8z72p1bw65waw2yy3j87j8zhc3s8b9drffii1xw")))

(define-public crate-solana-frozen-abi-macro-1.17.34 (c (n "solana-frozen-abi-macro") (v "1.17.34") (d (list (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "05l61b7zag08lqixd0ajjwzb2dwpnkfvhgjb606c26nnbzff3r7z")))

