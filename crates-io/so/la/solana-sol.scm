(define-module (crates-io so la solana-sol) #:use-module (crates-io))

(define-public crate-solana-sol-0.1.0 (c (n "solana-sol") (v "0.1.0") (d (list (d (n "bip39") (r "^2.0.0") (d #t) (k 0)) (d (n "lettre") (r "^0.11.6") (d #t) (k 0)) (d (n "sdkman-cli-native") (r "^0.5.2") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.18.1") (d #t) (k 0)))) (h "1rh9fghqmv2rmhs8590x1qd084q81hmzkw6vmjpslmjhprywyra2")))

