(define-module (crates-io so la solana-noop-program) #:use-module (crates-io))

(define-public crate-solana-noop-program-0.15.0 (c (n "solana-noop-program") (v "0.15.0") (d (list (d (n "log") (r "^0.4.2") (d #t) (k 0)) (d (n "solana-logger") (r "^0.15.0") (d #t) (k 0)) (d (n "solana-sdk") (r "^0.15.0") (d #t) (k 0)))) (h "1wl0lrz8adkc34pgfgjay8knxiwm4wq6q4lwrh30957r5dr0njl7")))

(define-public crate-solana-noop-program-0.15.1 (c (n "solana-noop-program") (v "0.15.1") (d (list (d (n "log") (r "^0.4.2") (d #t) (k 0)) (d (n "solana-logger") (r "^0.15.1") (d #t) (k 0)) (d (n "solana-sdk") (r "^0.15.1") (d #t) (k 0)))) (h "1zvkrmz5wrm21fm23mq8nssqrpjfp22i3lvvrs0132dfc81cgg2y")))

(define-public crate-solana-noop-program-0.15.3 (c (n "solana-noop-program") (v "0.15.3") (d (list (d (n "log") (r "^0.4.2") (d #t) (k 0)) (d (n "solana-logger") (r "^0.15.3") (d #t) (k 0)) (d (n "solana-sdk") (r "^0.15.3") (d #t) (k 0)))) (h "0jx6vqrwy1hf5gmbbbhkb0ifxwsv1p243k38kaa4gspf7hxf25v8")))

(define-public crate-solana-noop-program-0.16.0 (c (n "solana-noop-program") (v "0.16.0") (d (list (d (n "log") (r "^0.4.2") (d #t) (k 0)) (d (n "solana-logger") (r "^0.16.0") (d #t) (k 0)) (d (n "solana-sdk") (r "^0.16.0") (d #t) (k 0)))) (h "0wp05y3sf7lf43aq719fglc213lpb6zls03b88105x93z0rq95d3")))

(define-public crate-solana-noop-program-0.16.1 (c (n "solana-noop-program") (v "0.16.1") (d (list (d (n "log") (r "^0.4.2") (d #t) (k 0)) (d (n "solana-logger") (r "^0.16.1") (d #t) (k 0)) (d (n "solana-sdk") (r "^0.16.1") (d #t) (k 0)))) (h "132wxkrw8pdw7x8ccv1l6j03jg59d3irqzi100kyrdpwc5ck5vkr")))

(define-public crate-solana-noop-program-0.16.2 (c (n "solana-noop-program") (v "0.16.2") (d (list (d (n "log") (r "^0.4.2") (d #t) (k 0)) (d (n "solana-logger") (r "^0.16.2") (d #t) (k 0)) (d (n "solana-sdk") (r "^0.16.2") (d #t) (k 0)))) (h "18fwbw1a61cq40i13vi964f8z7y7lv6fvpz4i0bh4y3qkv3p71y5")))

(define-public crate-solana-noop-program-0.16.3 (c (n "solana-noop-program") (v "0.16.3") (d (list (d (n "log") (r "^0.4.2") (d #t) (k 0)) (d (n "solana-logger") (r "^0.16.3") (d #t) (k 0)) (d (n "solana-sdk") (r "^0.16.3") (d #t) (k 0)))) (h "0jlvjrhr0zksb8bx3sw15vq9g1p003vcddwmyjna9jayh4gyhynh")))

(define-public crate-solana-noop-program-0.16.4 (c (n "solana-noop-program") (v "0.16.4") (d (list (d (n "log") (r "^0.4.2") (d #t) (k 0)) (d (n "solana-logger") (r "^0.16.4") (d #t) (k 0)) (d (n "solana-sdk") (r "^0.16.4") (d #t) (k 0)))) (h "0yfk3sfk23718h9glvw3zdmn147hyk581k5gm8y4rzskc3bxkdab")))

(define-public crate-solana-noop-program-0.16.6 (c (n "solana-noop-program") (v "0.16.6") (d (list (d (n "log") (r "^0.4.2") (d #t) (k 0)) (d (n "solana-logger") (r "^0.16.6") (d #t) (k 0)) (d (n "solana-sdk") (r "^0.16.6") (d #t) (k 0)))) (h "0z4m9m93b1j1fva1grrcvfkpbr2m6ih3jmz764fv6iamacmmn8na")))

(define-public crate-solana-noop-program-0.17.0 (c (n "solana-noop-program") (v "0.17.0") (d (list (d (n "log") (r "^0.4.7") (d #t) (k 0)) (d (n "solana-logger") (r "^0.17.0") (d #t) (k 0)) (d (n "solana-sdk") (r "^0.17.0") (d #t) (k 0)))) (h "1arpa0997id82zvv9wz07kx2zsn9kbka97yw80ayvk3xy7rwmswk")))

(define-public crate-solana-noop-program-0.17.1 (c (n "solana-noop-program") (v "0.17.1") (d (list (d (n "log") (r "^0.4.7") (d #t) (k 0)) (d (n "solana-logger") (r "^0.17.1") (d #t) (k 0)) (d (n "solana-sdk") (r "^0.17.1") (d #t) (k 0)))) (h "1xbmnkd0a1w3yn0wh54b27kshwl81ia112l09nv5nrvwkx6wvna1")))

(define-public crate-solana-noop-program-0.18.0-pre0 (c (n "solana-noop-program") (v "0.18.0-pre0") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-logger") (r "^0.18.0-pre0") (d #t) (k 0)) (d (n "solana-sdk") (r "^0.18.0-pre0") (d #t) (k 0)))) (h "06sp374ncbl15ngf4npipxnhad3704b5j5mbwk15g51nn8n81yk4")))

(define-public crate-solana-noop-program-0.18.0-pre1 (c (n "solana-noop-program") (v "0.18.0-pre1") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-logger") (r "^0.18.0-pre1") (d #t) (k 0)) (d (n "solana-sdk") (r "^0.18.0-pre1") (d #t) (k 0)))) (h "020fh4dad75f28xdkjwr1314swljf5rrsj7ckqh5wkjacmpd9wlg")))

(define-public crate-solana-noop-program-0.17.2 (c (n "solana-noop-program") (v "0.17.2") (d (list (d (n "log") (r "^0.4.7") (d #t) (k 0)) (d (n "solana-logger") (r "^0.17.2") (d #t) (k 0)) (d (n "solana-sdk") (r "^0.17.2") (d #t) (k 0)))) (h "1lf04cainvf65rzg82y0hhcp245plfa30gw6qwrfdz25ss6fkgbp")))

(define-public crate-solana-noop-program-0.18.0-pre2 (c (n "solana-noop-program") (v "0.18.0-pre2") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-logger") (r "^0.18.0-pre2") (d #t) (k 0)) (d (n "solana-sdk") (r "^0.18.0-pre2") (d #t) (k 0)))) (h "0hscl2d4fvjq85ssn3d66n62dgypbpf786hz5vri58mcjn2wqhnd")))

(define-public crate-solana-noop-program-0.18.0 (c (n "solana-noop-program") (v "0.18.0") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-logger") (r "^0.18.0") (d #t) (k 0)) (d (n "solana-sdk") (r "^0.18.0") (d #t) (k 0)))) (h "0ryqy2mqrpdl7xcx0jhi0d8nah33i5vzj7h075hxir16kda99dn7")))

(define-public crate-solana-noop-program-0.18.1 (c (n "solana-noop-program") (v "0.18.1") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-logger") (r "^0.18.1") (d #t) (k 0)) (d (n "solana-sdk") (r "^0.18.1") (d #t) (k 0)))) (h "1kcsppnlv9536w92x4r95z4q13fpvqhyrba867hcc5w934ayyi6p")))

(define-public crate-solana-noop-program-0.19.1 (c (n "solana-noop-program") (v "0.19.1") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-logger") (r "^0.19.1") (d #t) (k 0)) (d (n "solana-sdk") (r "^0.19.1") (d #t) (k 0)))) (h "1sxiyx0973qqfgpgpq1m2bl2qnpr8zxbl0smpph7y2ap4n55603a")))

(define-public crate-solana-noop-program-0.20.1 (c (n "solana-noop-program") (v "0.20.1") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-logger") (r "^0.20.1") (d #t) (k 0)) (d (n "solana-sdk") (r "^0.20.1") (d #t) (k 0)))) (h "1c17gznibs7x7lyjxpj0rnyn2ag6w7axmpf6m5ljw6b5l35lb2ad")))

(define-public crate-solana-noop-program-0.20.2 (c (n "solana-noop-program") (v "0.20.2") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-logger") (r "^0.20.2") (d #t) (k 0)) (d (n "solana-sdk") (r "^0.20.2") (d #t) (k 0)))) (h "1kawrc40dclsi6317m6g7ixrjzqrb74b25p812p9wd0lkbgr48f1")))

(define-public crate-solana-noop-program-0.20.3 (c (n "solana-noop-program") (v "0.20.3") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-logger") (r "^0.20.3") (d #t) (k 0)) (d (n "solana-sdk") (r "^0.20.3") (d #t) (k 0)))) (h "1hi9x3xzh7qhb97c2zs5nvswvvvvfbdy8pbx5676mchrwc6412a5")))

(define-public crate-solana-noop-program-0.20.4 (c (n "solana-noop-program") (v "0.20.4") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-logger") (r "^0.20.4") (d #t) (k 0)) (d (n "solana-sdk") (r "^0.20.4") (d #t) (k 0)))) (h "1da3ffjbzgnwnvbv5cmb06cq62b4gjkpx5la10wfyskfhb8vbysh")))

(define-public crate-solana-noop-program-0.20.5 (c (n "solana-noop-program") (v "0.20.5") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-logger") (r "^0.20.5") (d #t) (k 0)) (d (n "solana-sdk") (r "^0.20.5") (d #t) (k 0)))) (h "039yf7ffmrw9mk2pn3zswl52iss452c5cb9r218m0gb2q6x7cwcs")))

(define-public crate-solana-noop-program-0.21.0 (c (n "solana-noop-program") (v "0.21.0") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-logger") (r "^0.21.0") (d #t) (k 0)) (d (n "solana-sdk") (r "^0.21.0") (d #t) (k 0)))) (h "193cbfj1d7wm1zxyjz2w95h5l36akfh45lnhizynl0fds7l5fslh")))

(define-public crate-solana-noop-program-0.22.0 (c (n "solana-noop-program") (v "0.22.0") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-logger") (r "^0.22.0") (d #t) (k 0)) (d (n "solana-sdk") (r "^0.22.0") (d #t) (k 0)))) (h "1gkbqgrbb56wj4hvxkry4yg97rhc9if3vimcb8x8j4cl87rhdxbq")))

(define-public crate-solana-noop-program-0.22.1 (c (n "solana-noop-program") (v "0.22.1") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-logger") (r "^0.22.1") (d #t) (k 0)) (d (n "solana-sdk") (r "^0.22.1") (d #t) (k 0)))) (h "1icq57i2srhy5qw14dqpsda8p4n186yjdzi9jp0cqim3qkm472cv")))

(define-public crate-solana-noop-program-0.22.2 (c (n "solana-noop-program") (v "0.22.2") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-logger") (r "^0.22.2") (d #t) (k 0)) (d (n "solana-sdk") (r "^0.22.2") (d #t) (k 0)))) (h "1pv62hd5dhnn2qcxrym7p2hbaf9k18ak029xkp2wl814wbm8lxkb")))

(define-public crate-solana-noop-program-0.22.3 (c (n "solana-noop-program") (v "0.22.3") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-logger") (r "^0.22.3") (d #t) (k 0)) (d (n "solana-sdk") (r "^0.22.3") (d #t) (k 0)))) (h "0j0h3xvb2q4l3la65y3rfmclzkwx40r1jybfp23n2h7hkc53hpcz")))

(define-public crate-solana-noop-program-0.22.4 (c (n "solana-noop-program") (v "0.22.4") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-logger") (r "^0.22.4") (d #t) (k 0)) (d (n "solana-sdk") (r "^0.22.4") (d #t) (k 0)))) (h "1n4pnjr3ywprainr3k4xmbqva8qxcw1mrc411v30p50dh71vh2cb")))

(define-public crate-solana-noop-program-0.23.0 (c (n "solana-noop-program") (v "0.23.0") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-logger") (r "^0.23.0") (d #t) (k 0)) (d (n "solana-sdk") (r "^0.23.0") (d #t) (k 0)))) (h "1g8pkx03pf6rkkzrxzvn1lk5c76vqa5ja8pqrvbskv1a5yl9mnwm")))

(define-public crate-solana-noop-program-0.22.5 (c (n "solana-noop-program") (v "0.22.5") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-logger") (r "^0.22.5") (d #t) (k 0)) (d (n "solana-sdk") (r "^0.22.5") (d #t) (k 0)))) (h "0c063kqrb253vbsdrl5425q9r5j082gl8k5465ng460k9xcgynl2")))

(define-public crate-solana-noop-program-0.22.6 (c (n "solana-noop-program") (v "0.22.6") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-logger") (r "^0.22.6") (d #t) (k 0)) (d (n "solana-sdk") (r "^0.22.6") (d #t) (k 0)))) (h "0spj4lkq29v4q9rl0lxr8kii4yzcm1vs2hawv6zi6bb3n3r0dcx9")))

(define-public crate-solana-noop-program-0.23.1 (c (n "solana-noop-program") (v "0.23.1") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-logger") (r "^0.23.1") (d #t) (k 0)) (d (n "solana-sdk") (r "^0.23.1") (d #t) (k 0)))) (h "0vb9hbkc8d5mbszb3i6ks3dksyrmc8ak6g5ld0b0la4b74j69c8n")))

(define-public crate-solana-noop-program-0.23.2 (c (n "solana-noop-program") (v "0.23.2") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-logger") (r "^0.23.2") (d #t) (k 0)) (d (n "solana-sdk") (r "^0.23.2") (d #t) (k 0)))) (h "18mylp9kj2w4cfq2pvh109jb1nf9hl0ky1s0h05kh452ibqb4x0q")))

(define-public crate-solana-noop-program-0.22.7 (c (n "solana-noop-program") (v "0.22.7") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-logger") (r "^0.22.7") (d #t) (k 0)) (d (n "solana-sdk") (r "^0.22.7") (d #t) (k 0)))) (h "1lm777nilwq0dh8pkc2dfikbhf4dkygfhsmhdbp8b327d6nigbvc")))

(define-public crate-solana-noop-program-0.23.3 (c (n "solana-noop-program") (v "0.23.3") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-logger") (r "^0.23.3") (d #t) (k 0)) (d (n "solana-sdk") (r "^0.23.3") (d #t) (k 0)))) (h "1ylccjvgikvj0mgzx453cfz3l93pzlvbam66mlpwyfl3rw19b1g3")))

(define-public crate-solana-noop-program-0.23.4 (c (n "solana-noop-program") (v "0.23.4") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-logger") (r "^0.23.4") (d #t) (k 0)) (d (n "solana-sdk") (r "^0.23.4") (d #t) (k 0)))) (h "0pfsif979w94i5r1aqd61b58p29zxnx28xfwaxq08a0qdn3dp5b7")))

(define-public crate-solana-noop-program-0.22.8 (c (n "solana-noop-program") (v "0.22.8") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-logger") (r "^0.22.8") (d #t) (k 0)) (d (n "solana-sdk") (r "^0.22.8") (d #t) (k 0)))) (h "02j16g2xy3bm0y3apg7w0nl100zwgsl6jcw9jhv8kraij5mml20a")))

(define-public crate-solana-noop-program-0.23.5 (c (n "solana-noop-program") (v "0.23.5") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-logger") (r "^0.23.5") (d #t) (k 0)) (d (n "solana-sdk") (r "^0.23.5") (d #t) (k 0)))) (h "0y93rwpx0rvixjmil6qqxr7ckj3qzk6f9callgp33sfs31kdr5hg")))

(define-public crate-solana-noop-program-0.23.6 (c (n "solana-noop-program") (v "0.23.6") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-logger") (r "^0.23.6") (d #t) (k 0)) (d (n "solana-sdk") (r "^0.23.6") (d #t) (k 0)))) (h "18kcikdn7fpqzlx32pqghhal4ka5q1dnwrg3i888m7nzsbsj6wk9")))

(define-public crate-solana-noop-program-1.0.0 (c (n "solana-noop-program") (v "1.0.0") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-logger") (r "^1.0.0") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.0.0") (d #t) (k 0)))) (h "18jpmbi7b9z5w9ksa9xmm10iwy3czkgcmdba2nx8w1apjski72mq")))

(define-public crate-solana-noop-program-0.23.7 (c (n "solana-noop-program") (v "0.23.7") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-logger") (r "^0.23.7") (d #t) (k 0)) (d (n "solana-sdk") (r "^0.23.7") (d #t) (k 0)))) (h "0pbmxj6gxiiigqgzpy8209sxbpidlj0pqi1kf4djzfnp9hci9v3s")))

(define-public crate-solana-noop-program-0.23.8 (c (n "solana-noop-program") (v "0.23.8") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-logger") (r "^0.23.8") (d #t) (k 0)) (d (n "solana-sdk") (r "^0.23.8") (d #t) (k 0)))) (h "1darwb5hb8wjc6s9dgqrxi4r0almkx26zxswbn6m0rbzy61913bi")))

(define-public crate-solana-noop-program-1.0.1 (c (n "solana-noop-program") (v "1.0.1") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-logger") (r "^1.0.1") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.0.1") (d #t) (k 0)))) (h "0b58s04p2vam27br17aa9mpv0gvxv85z63wznpv1rc4bqnk4fj9b")))

(define-public crate-solana-noop-program-1.0.2 (c (n "solana-noop-program") (v "1.0.2") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-logger") (r "^1.0.2") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.0.2") (d #t) (k 0)))) (h "10b4jis3965x21ws8r75b364f66plcv0vfp5vkzaz5r4j8cyncl0")))

(define-public crate-solana-noop-program-1.0.3 (c (n "solana-noop-program") (v "1.0.3") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-logger") (r "^1.0.3") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.0.3") (d #t) (k 0)))) (h "0zw5x24k3y02hbygw72lx3kvf2w8g2np35rqprz7ldbm50s3mv2w")))

(define-public crate-solana-noop-program-1.0.4 (c (n "solana-noop-program") (v "1.0.4") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-logger") (r "^1.0.4") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.0.4") (d #t) (k 0)))) (h "0q8lqc4jmsf8l0vl7kl6s0fslkvhdzcgl3s2vans134qmhs0i038")))

(define-public crate-solana-noop-program-1.0.5 (c (n "solana-noop-program") (v "1.0.5") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-logger") (r "^1.0.5") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.0.5") (d #t) (k 0)))) (h "1ch0b7sm4g6hc3xwqh0l5zvnqw3cssazflpvpa1inwxiynqsbfcp")))

(define-public crate-solana-noop-program-1.0.6 (c (n "solana-noop-program") (v "1.0.6") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-logger") (r "^1.0.6") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.0.6") (d #t) (k 0)))) (h "0517alj8hgmhc4jddmdb6h86hbzpih5p373mr7msxzs4ss7j1dl0")))

(define-public crate-solana-noop-program-1.0.7 (c (n "solana-noop-program") (v "1.0.7") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-logger") (r "^1.0.7") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.0.7") (d #t) (k 0)))) (h "0w1ck9yfrd020jrpsl1zdanjk9m7l8wclpw5bflpb90nq14cfihc")))

(define-public crate-solana-noop-program-1.0.8 (c (n "solana-noop-program") (v "1.0.8") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-logger") (r "^1.0.8") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.0.8") (d #t) (k 0)))) (h "0svnvhb9xbgimsydxxkk81gki5877apgjwskigg8d90npmshvk0k")))

(define-public crate-solana-noop-program-1.0.9 (c (n "solana-noop-program") (v "1.0.9") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-logger") (r "^1.0.9") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.0.9") (d #t) (k 0)))) (h "16v89ih5r6zx9ppwchhwps2558l90j9w8d4qdsrp3xwp24iws2sv")))

(define-public crate-solana-noop-program-1.0.10 (c (n "solana-noop-program") (v "1.0.10") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-logger") (r "^1.0.10") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.0.10") (d #t) (k 0)))) (h "0g4rlhcwxgl2swh6pvrgw3m35dbv206rjwh273hpmb1waan7g7jk")))

(define-public crate-solana-noop-program-1.0.11 (c (n "solana-noop-program") (v "1.0.11") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-logger") (r "^1.0.11") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.0.11") (d #t) (k 0)))) (h "192kh7vs39zmb9vijyv8s3fxn5ncj35jw3ln0bb5sy5pvxb1a9wj")))

(define-public crate-solana-noop-program-1.0.12 (c (n "solana-noop-program") (v "1.0.12") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-logger") (r "^1.0.12") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.0.12") (d #t) (k 0)))) (h "1asq07swkhp7wpdhli97a9hpsc5m5n2657cbqlizb9zb8bbr2gpk")))

(define-public crate-solana-noop-program-1.1.0 (c (n "solana-noop-program") (v "1.1.0") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-logger") (r "^1.1.0") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.1.0") (d #t) (k 0)))) (h "15s9v2iallivxlwqx2s2sa90347h6pn5ipaw7w7i9l39z4c08dd3")))

(define-public crate-solana-noop-program-1.1.1 (c (n "solana-noop-program") (v "1.1.1") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-logger") (r "^1.1.1") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.1.1") (d #t) (k 0)))) (h "17yd0n3f8vyj9q3r4mmqhmhm6c2979csb0a0ajqqa0gf6smn7vix")))

(define-public crate-solana-noop-program-1.0.13 (c (n "solana-noop-program") (v "1.0.13") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-logger") (r "^1.0.13") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.0.13") (d #t) (k 0)))) (h "18rb53nm754jif110p2dgf8haync3xb3dy0pw5ymrn2s1mj5srl5")))

(define-public crate-solana-noop-program-1.1.2 (c (n "solana-noop-program") (v "1.1.2") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-logger") (r "^1.1.2") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.1.2") (d #t) (k 0)))) (h "0gw9cxyqbi286b77b99qf9dm6nwsxqq9ph1q7hvxby51hjcgfdkq")))

(define-public crate-solana-noop-program-1.0.14 (c (n "solana-noop-program") (v "1.0.14") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-logger") (r "^1.0.14") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.0.14") (d #t) (k 0)))) (h "1h2glk1ycxlgk5nd45f7j9bb8pgw1hdc1wf59x4iyv9n0la8w4sk")))

(define-public crate-solana-noop-program-1.0.15 (c (n "solana-noop-program") (v "1.0.15") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-logger") (r "^1.0.15") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.0.15") (d #t) (k 0)))) (h "1jcpv1dsszykwkxs7frhhkamqzyh2618ag3ishsln7bzhhvhpmww")))

(define-public crate-solana-noop-program-1.0.16 (c (n "solana-noop-program") (v "1.0.16") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-logger") (r "^1.0.16") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.0.16") (d #t) (k 0)))) (h "1n1bm2myhn6pnfk73764q4cv1ljw5l58g00m5irb3r3sjdv7zijd")))

(define-public crate-solana-noop-program-1.1.3 (c (n "solana-noop-program") (v "1.1.3") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-logger") (r "^1.1.3") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.1.3") (d #t) (k 0)))) (h "0cb1qwl0v1j0nbaipkbgilgaisvz09f1r1lpakhrzwsvik13vpgv")))

(define-public crate-solana-noop-program-1.0.17 (c (n "solana-noop-program") (v "1.0.17") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-logger") (r "^1.0.17") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.0.17") (d #t) (k 0)))) (h "031140yd37bgp8zvl22izmdx5l8s0imza7zql0c10w27b6lrlp5b")))

(define-public crate-solana-noop-program-1.0.18 (c (n "solana-noop-program") (v "1.0.18") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-logger") (r "^1.0.18") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.0.18") (d #t) (k 0)))) (h "1bwmzl2xkhzw62kd9pswk5d20mjp8xz1ka5w62lr8c1jgb0zl3vz")))

(define-public crate-solana-noop-program-1.1.5 (c (n "solana-noop-program") (v "1.1.5") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-logger") (r "^1.1.5") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.1.5") (d #t) (k 0)))) (h "0bj56jbc0j1dxpqa96iyrfgdk0kpr7fhlm117csams0chza82wml")))

(define-public crate-solana-noop-program-1.1.6 (c (n "solana-noop-program") (v "1.1.6") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-logger") (r "^1.1.6") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.1.6") (d #t) (k 0)))) (h "0pl2bmj3a3sxcd3j7js8j0hs05w56irn66bglnbcipyhcyb2xlr8")))

(define-public crate-solana-noop-program-1.1.7 (c (n "solana-noop-program") (v "1.1.7") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-logger") (r "^1.1.7") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.1.7") (d #t) (k 0)))) (h "116nwnmb1a95rslsj11gp5fhl66b3kzzcizv9cm7cw5ghmm7q5dc")))

(define-public crate-solana-noop-program-1.0.20 (c (n "solana-noop-program") (v "1.0.20") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-logger") (r "^1.0.20") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.0.20") (d #t) (k 0)))) (h "0569sjxk0qyfpvqx74jpdixwwyf2rfkng1qc4m8jyp6s6imdyqmm")))

(define-public crate-solana-noop-program-1.0.21 (c (n "solana-noop-program") (v "1.0.21") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-logger") (r "^1.0.21") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.0.21") (d #t) (k 0)))) (h "17xrwbbchbvqv2yalbkdnv7cbwzr0xgczh8hszi2mwfc4s89cdb9")))

(define-public crate-solana-noop-program-1.1.8 (c (n "solana-noop-program") (v "1.1.8") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-logger") (r "^1.1.8") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.1.8") (d #t) (k 0)))) (h "0g53fj944a4imcm362g6ph0cl335jp4kc63m8xb8lgr9igknmh1r")))

(define-public crate-solana-noop-program-1.1.9 (c (n "solana-noop-program") (v "1.1.9") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-logger") (r "^1.1.9") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.1.9") (d #t) (k 0)))) (h "0g9vsimmzfnq8dvz10q2vh8x3prfy6jimcyi5mkjkdjs4c5c27pi")))

(define-public crate-solana-noop-program-1.1.10 (c (n "solana-noop-program") (v "1.1.10") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-logger") (r "^1.1.10") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.1.10") (d #t) (k 0)))) (h "02n1lf2apc350s1rjjjz99v94km2cn2bd856v5v97gvvbyifxgad")))

(define-public crate-solana-noop-program-1.0.22 (c (n "solana-noop-program") (v "1.0.22") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-logger") (r "^1.0.22") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.0.22") (d #t) (k 0)))) (h "0n24cj6qlxxfzyj8pg4zcbs9vvq56lyhafjl9skn0gvyvs4sj30f")))

(define-public crate-solana-noop-program-1.1.11 (c (n "solana-noop-program") (v "1.1.11") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-logger") (r "^1.1.11") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.1.11") (d #t) (k 0)))) (h "0rp4d5zrjkhf0y2sxh91m6a8x3qp5j6fi5r2sh73fjv67jryfzd1")))

(define-public crate-solana-noop-program-1.1.12 (c (n "solana-noop-program") (v "1.1.12") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-logger") (r "^1.1.12") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.1.12") (d #t) (k 0)))) (h "0gm4qhs8bxnpl6cjj8bz7m0lykcikn6y4ildgcid0r7svab608ja")))

(define-public crate-solana-noop-program-1.1.13 (c (n "solana-noop-program") (v "1.1.13") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-logger") (r "^1.1.13") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.1.13") (d #t) (k 0)))) (h "1hkh3sfn7f50ln1li055xjb3r26p0pghayz2k06dvrwmyr1dp9rx")))

(define-public crate-solana-noop-program-1.0.24 (c (n "solana-noop-program") (v "1.0.24") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-logger") (r "^1.0.24") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.0.24") (d #t) (k 0)))) (h "1vp03gz2sw5v8z7bv5kfk8jq15z3b8l5dbzk13f037bg2ahys0ph")))

(define-public crate-solana-noop-program-1.2.0 (c (n "solana-noop-program") (v "1.2.0") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-logger") (r "^1.2.0") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.2.0") (d #t) (k 0)))) (h "1r27p1k2jarwdwlvfp075ld4lqp0ny88h2zzvjpy3ywi34iygjrp")))

(define-public crate-solana-noop-program-1.1.15 (c (n "solana-noop-program") (v "1.1.15") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-logger") (r "^1.1.15") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.1.15") (d #t) (k 0)))) (h "0rryxdjc4dnp0h1mnz68zn2rb17x3zg8crkw2kh0wn6k3phklg2a")))

(define-public crate-solana-noop-program-1.1.17 (c (n "solana-noop-program") (v "1.1.17") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-logger") (r "^1.1.17") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.1.17") (d #t) (k 0)))) (h "1b3n8sn9sq1c8jj2phiy5mnrwxgqpzxhnznjchd4ic28mj9bzdap")))

(define-public crate-solana-noop-program-1.2.1 (c (n "solana-noop-program") (v "1.2.1") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-logger") (r "^1.2.1") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.2.1") (d #t) (k 0)))) (h "1gdd7gbddy28hr3xqxgdqpnw5x4z6lwgmfc07fizdh4bh4kl19g6")))

(define-public crate-solana-noop-program-1.1.18 (c (n "solana-noop-program") (v "1.1.18") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-logger") (r "^1.1.18") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.1.18") (d #t) (k 0)))) (h "1jjl77wjsv8kv63cwjg0wfmkyfhf4580f00aylwnmaw0hdvqjhj6")))

(define-public crate-solana-noop-program-1.2.3 (c (n "solana-noop-program") (v "1.2.3") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-logger") (r "^1.2.3") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.2.3") (d #t) (k 0)))) (h "16p46ij490mwwnaj0ah8hiw8jghi9gx1b04mwhgj42cvwg36fip7")))

(define-public crate-solana-noop-program-1.2.4 (c (n "solana-noop-program") (v "1.2.4") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-logger") (r "^1.2.4") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.2.4") (d #t) (k 0)))) (h "0lxa6vjwimk24fsmnjx8r102prjdjgaf1sq63bb54jnwgdchqci6")))

(define-public crate-solana-noop-program-1.1.19 (c (n "solana-noop-program") (v "1.1.19") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-logger") (r "^1.1.19") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.1.19") (d #t) (k 0)))) (h "07s3kd9a864akvz22ajc4k6isdifqmdj1774bzmm31r10k5k172c")))

(define-public crate-solana-noop-program-1.2.5 (c (n "solana-noop-program") (v "1.2.5") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-logger") (r "^1.2.5") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.2.5") (d #t) (k 0)))) (h "0849b727zvvg0kc3nmfvs9y5alda9cb07wbydn44wq9dpc0xcsz8")))

(define-public crate-solana-noop-program-1.2.6 (c (n "solana-noop-program") (v "1.2.6") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-logger") (r "^1.2.6") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.2.6") (d #t) (k 0)))) (h "0p6vqvz9zs5w59x72pfr3q78wdcl2bh4bhjxciavffwshd1lkqs6")))

(define-public crate-solana-noop-program-1.2.7 (c (n "solana-noop-program") (v "1.2.7") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-logger") (r "^1.2.7") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.2.7") (d #t) (k 0)))) (h "1ik1255pnir076vp4kf8vl25x14v82viy4yvaj8gfcvgdnx527vf")))

(define-public crate-solana-noop-program-1.2.8 (c (n "solana-noop-program") (v "1.2.8") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-logger") (r "^1.2.8") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.2.8") (d #t) (k 0)))) (h "0fplsranbcr5sri9kqv804ir5587ggagry9vmhly09ih42vyh8pj")))

(define-public crate-solana-noop-program-1.2.9 (c (n "solana-noop-program") (v "1.2.9") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-logger") (r "^1.2.9") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.2.9") (d #t) (k 0)))) (h "1w8r21bx79igy8s1kc1zp1c5swbbwpviw8s5j6qwgssk2bnzbjwj")))

(define-public crate-solana-noop-program-1.2.10 (c (n "solana-noop-program") (v "1.2.10") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-logger") (r "^1.2.10") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.2.10") (d #t) (k 0)))) (h "192jngcn63ja1dvx0s1y18r8m6hh4qlxc2iz0fl1jlpvi85w9a4r")))

(define-public crate-solana-noop-program-1.1.20 (c (n "solana-noop-program") (v "1.1.20") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-logger") (r "^1.1.20") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.1.20") (d #t) (k 0)))) (h "12vlz100iay8p63wngd63bvf1hwigfb6j2c9gq4f7agh0wl37430")))

(define-public crate-solana-noop-program-1.2.12 (c (n "solana-noop-program") (v "1.2.12") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-logger") (r "^1.2.12") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.2.12") (d #t) (k 0)))) (h "10dzd4kiga7ir598jmq9jz8xrc3x2kzqgxrhm1z1dzhbf8z29z0h")))

(define-public crate-solana-noop-program-1.2.13 (c (n "solana-noop-program") (v "1.2.13") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-logger") (r "^1.2.13") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.2.13") (d #t) (k 0)))) (h "1a1n3hzkz69n60503ydswsxr0ii9whlxppjd8cm8jlfjwd1ya20j")))

(define-public crate-solana-noop-program-1.2.14 (c (n "solana-noop-program") (v "1.2.14") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-logger") (r "^1.2.14") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.2.14") (d #t) (k 0)))) (h "1gd8avxd6q0v16as79xhqwasv0g8nz8b0q65z3ip9ffih9in26y8")))

(define-public crate-solana-noop-program-1.1.23 (c (n "solana-noop-program") (v "1.1.23") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-logger") (r "^1.1.23") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.1.23") (d #t) (k 0)))) (h "15rqapfpmhyhmwgkdnhrhkmr8jafafykbpgpbl9i6wy7zr2av5lp")))

(define-public crate-solana-noop-program-1.2.15 (c (n "solana-noop-program") (v "1.2.15") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-logger") (r "^1.2.15") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.2.15") (d #t) (k 0)))) (h "0fhsh0j2g74pcvwpzs99qq2qil7l832a1qlqg65ra5g0j3lpl44b")))

(define-public crate-solana-noop-program-1.2.16 (c (n "solana-noop-program") (v "1.2.16") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-logger") (r "^1.2.16") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.2.16") (d #t) (k 0)))) (h "09l9j4hax381g5xyyjnw37a15z6rliw3sqhmmybrii76gh539y5h")))

(define-public crate-solana-noop-program-1.2.17 (c (n "solana-noop-program") (v "1.2.17") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-logger") (r "^1.2.17") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.2.17") (d #t) (k 0)))) (h "1lskziaq74fd2f52zli380vf5ww7qfds3mll84wssbapdfd9wccd")))

(define-public crate-solana-noop-program-1.2.18 (c (n "solana-noop-program") (v "1.2.18") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-logger") (r "^1.2.18") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.2.18") (d #t) (k 0)))) (h "1d6lnh22wmpvwh7ylwav5zhglrqaqv4i4qxpy07hwq0sf64zkgl5")))

(define-public crate-solana-noop-program-1.2.19 (c (n "solana-noop-program") (v "1.2.19") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-logger") (r "^1.2.19") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.2.19") (d #t) (k 0)))) (h "0v3wfdi9z7sksdbx409c2k1hjq58038h2jr2xhbmyv0b31pqz6f8")))

(define-public crate-solana-noop-program-1.2.20 (c (n "solana-noop-program") (v "1.2.20") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-logger") (r "^1.2.20") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.2.20") (d #t) (k 0)))) (h "1abdkak2g4v98dlcgwhmh3cdsn5jhavbrb5sf1rk7054p1hr0y12")))

(define-public crate-solana-noop-program-1.3.0 (c (n "solana-noop-program") (v "1.3.0") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-logger") (r "^1.3.0") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.3.0") (d #t) (k 0)))) (h "1jlgl14vi6psnka685s7vr1zxaxr84f376pqrrljs6lrkcfa4jpg")))

(define-public crate-solana-noop-program-1.3.1 (c (n "solana-noop-program") (v "1.3.1") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-logger") (r "^1.3.1") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.3.1") (d #t) (k 0)))) (h "14w7kz0jzp912n31874x98b8ac8vnzicxysqkp1cqh2amph8p8q1")))

(define-public crate-solana-noop-program-1.2.21 (c (n "solana-noop-program") (v "1.2.21") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-logger") (r "^1.2.21") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.2.21") (d #t) (k 0)))) (h "0dh4bs76y9gkkp31hmsjhbi908lbzp67fs5x0cd93k9wlbw74gxl")))

(define-public crate-solana-noop-program-1.3.2 (c (n "solana-noop-program") (v "1.3.2") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-logger") (r "^1.3.2") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.3.2") (d #t) (k 0)))) (h "009yw1fk3w9ah3qlwn9ci4qh1001xpq9q81d61p70zdiiq4y2f4b")))

(define-public crate-solana-noop-program-1.2.22 (c (n "solana-noop-program") (v "1.2.22") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-logger") (r "^1.2.22") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.2.22") (d #t) (k 0)))) (h "1fhnf08xvbqsz0mzaqd8bagaadn9h136jc9x6kmn54m1lqarkykn")))

(define-public crate-solana-noop-program-1.3.3 (c (n "solana-noop-program") (v "1.3.3") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-logger") (r "^1.3.3") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.3.3") (d #t) (k 0)))) (h "0dr10np1cbhgd1329pczcc8cal47lwszn7sjv0lj5kixm5mrvjw1")))

(define-public crate-solana-noop-program-1.2.23 (c (n "solana-noop-program") (v "1.2.23") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-logger") (r "^1.2.23") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.2.23") (d #t) (k 0)))) (h "11gir4lvpawnbhdzym15lg6c53l3arxwcf4sha8a5yw17iqx9l2z")))

(define-public crate-solana-noop-program-1.2.24 (c (n "solana-noop-program") (v "1.2.24") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-logger") (r "^1.2.24") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.2.24") (d #t) (k 0)))) (h "1rrm3rmipbpr92x40113zsi2lb8rdrv5yfy76xpvh4jlqdmra68w")))

(define-public crate-solana-noop-program-1.3.4 (c (n "solana-noop-program") (v "1.3.4") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-logger") (r "^1.3.4") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.3.4") (d #t) (k 0)))) (h "1fiw0qyffz7azjl7izggk3szwl66jvlmxg9krfgz8lchphcvfxvs")))

(define-public crate-solana-noop-program-1.2.25 (c (n "solana-noop-program") (v "1.2.25") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-logger") (r "^1.2.25") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.2.25") (d #t) (k 0)))) (h "0948msgg80iyzv9qmqhr3sk9cj0af185gn9b0m5wyg26zfhj01xc")))

(define-public crate-solana-noop-program-1.2.26 (c (n "solana-noop-program") (v "1.2.26") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-logger") (r "^1.2.26") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.2.26") (d #t) (k 0)))) (h "066mnfwr1r1ibrbhni63z0w18sk6x6lv5wais6ysa6lxdppbv5sh")))

(define-public crate-solana-noop-program-1.3.5 (c (n "solana-noop-program") (v "1.3.5") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-logger") (r "^1.3.5") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.3.5") (d #t) (k 0)))) (h "0lfbrgmsqbsl0dzm4hbj36lis7v3hzl2qkik1vfdallaisfldidf")))

(define-public crate-solana-noop-program-1.3.6 (c (n "solana-noop-program") (v "1.3.6") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-logger") (r "^1.3.6") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.3.6") (d #t) (k 0)))) (h "05f2by27flzddazkkf44vgfm2ydsyzp61d0ackpd5jmsaq3zfcf3")))

(define-public crate-solana-noop-program-1.3.7 (c (n "solana-noop-program") (v "1.3.7") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-logger") (r "^1.3.7") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.3.7") (d #t) (k 0)))) (h "15wkcwjwj5y3dg4l0k3wf6xj0dc5vbdx6rlx633h28xg45zis57j")))

(define-public crate-solana-noop-program-1.2.27 (c (n "solana-noop-program") (v "1.2.27") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-logger") (r "^1.2.27") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.2.27") (d #t) (k 0)))) (h "13r5awscxz75b7lkmskn9g1lvl8dhiy37qfzlm8jrccwmavqhmxg")))

(define-public crate-solana-noop-program-1.3.8 (c (n "solana-noop-program") (v "1.3.8") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-logger") (r "^1.3.8") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.3.8") (d #t) (k 0)))) (h "0hikzp3784c9z4w4gj34fyx78ib5rdxxgvcz5iw2bb5jvsh98c6h")))

(define-public crate-solana-noop-program-1.2.28 (c (n "solana-noop-program") (v "1.2.28") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-logger") (r "^1.2.28") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.2.28") (d #t) (k 0)))) (h "0czvnr70s85cljgli2ng49krmm7hh29ic9qhd2irgnfdjdwpycfw")))

(define-public crate-solana-noop-program-1.3.9 (c (n "solana-noop-program") (v "1.3.9") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-logger") (r "^1.3.9") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.3.9") (d #t) (k 0)))) (h "0fi28bmbbk8aj7yz0z4g78gx8ypsm9yaj3i4pmdmd3wcbk8hqa61")))

(define-public crate-solana-noop-program-1.3.10 (c (n "solana-noop-program") (v "1.3.10") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-logger") (r "^1.3.10") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.3.10") (d #t) (k 0)))) (h "1956gl99hv2nv09snwbjqsq7805l2jlsnd5dc39ag01l7f8b3vx1")))

(define-public crate-solana-noop-program-1.3.11 (c (n "solana-noop-program") (v "1.3.11") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-logger") (r "^1.3.11") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.3.11") (d #t) (k 0)))) (h "0zm0zp2p9lm01lhgwb4yfnls8824nxadwipladwv4r4lv209ggvs")))

(define-public crate-solana-noop-program-1.2.29 (c (n "solana-noop-program") (v "1.2.29") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-logger") (r "^1.2.29") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.2.29") (d #t) (k 0)))) (h "0dk7qww7c0vk7ga2v6l693wskpz4f9pmlr5nqms4ppk3hfmxi5ya")))

(define-public crate-solana-noop-program-1.3.12 (c (n "solana-noop-program") (v "1.3.12") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-logger") (r "^1.3.12") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.3.12") (d #t) (k 0)))) (h "0i1nhxk5djgdah0slx4x4q995m929y1k0vdmm86hcq7wm1h4a4j4")))

(define-public crate-solana-noop-program-1.3.13 (c (n "solana-noop-program") (v "1.3.13") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-logger") (r "^1.3.13") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.3.13") (d #t) (k 0)))) (h "01spl8cmimhkgab1zwyxs92sgwh93qrcspz4vbfcmzslgs19hghx")))

(define-public crate-solana-noop-program-1.2.30 (c (n "solana-noop-program") (v "1.2.30") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-logger") (r "^1.2.30") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.2.30") (d #t) (k 0)))) (h "0ycwky5pzfx4caqqq1a8m6x6g8y8bvvqlyrvapwib4lxhjj1xj17")))

(define-public crate-solana-noop-program-1.3.14 (c (n "solana-noop-program") (v "1.3.14") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-logger") (r "^1.3.14") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.3.14") (d #t) (k 0)))) (h "14096laxkqypvxkgcgflkjyllw0rmk1xsxmmqcln9k5bhf8a7dzv")))

(define-public crate-solana-noop-program-1.2.31 (c (n "solana-noop-program") (v "1.2.31") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-logger") (r "^1.2.31") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.2.31") (d #t) (k 0)))) (h "0mkmkkcp6d1vww2kdx81k83mym0przbswyynssh3v0szmr6g1m1i")))

(define-public crate-solana-noop-program-1.2.32 (c (n "solana-noop-program") (v "1.2.32") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-logger") (r "^1.2.32") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.2.32") (d #t) (k 0)))) (h "1ql3gpdg1xsvq1vfn2q30wm6w6yw5sc2yd3flbmpw4a8wmq2vvns")))

(define-public crate-solana-noop-program-1.3.17 (c (n "solana-noop-program") (v "1.3.17") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-logger") (r "^1.3.17") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.3.17") (d #t) (k 0)))) (h "1z9r5gk3dhl0ia3n6vn8yrr82j11mnjx7jii930v61l5j71d2j4c")))

(define-public crate-solana-noop-program-1.4.0 (c (n "solana-noop-program") (v "1.4.0") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-logger") (r "^1.4.0") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.4.0") (d #t) (k 0)))) (h "012qni29j9nbf3k3wxswd3zil9ga75324di966yka80ybhnvlvp3")))

(define-public crate-solana-noop-program-1.4.1 (c (n "solana-noop-program") (v "1.4.1") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-logger") (r "^1.4.1") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.4.1") (d #t) (k 0)))) (h "1jnz7mmqx5g8h6qxs63y9wbl9b2x4wps0xaxzlgx6dva5ln8y3qa")))

(define-public crate-solana-noop-program-1.3.18 (c (n "solana-noop-program") (v "1.3.18") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-logger") (r "^1.3.18") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.3.18") (d #t) (k 0)))) (h "0qlzg11x6zxxx37fw9s3lpcmykimc1100g1nknwp88qq3ay4ck5c")))

(define-public crate-solana-noop-program-1.3.19 (c (n "solana-noop-program") (v "1.3.19") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-logger") (r "^1.3.19") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.3.19") (d #t) (k 0)))) (h "03vcyi696wmbmdisnq3vs4zkqskf4a7zlam107a8zz7zhz0wrsk3")))

(define-public crate-solana-noop-program-1.4.3 (c (n "solana-noop-program") (v "1.4.3") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-logger") (r "^1.4.3") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.4.3") (d #t) (k 0)))) (h "0lq1rdk32qc7cjsgjpsqhfnfscq4gd189yj4zv420kq187impv37")))

(define-public crate-solana-noop-program-1.4.4 (c (n "solana-noop-program") (v "1.4.4") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-logger") (r "^1.4.4") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.4.4") (d #t) (k 0)))) (h "0pibvcbf5kjgdplbrk20i0xad6nlgxi75xck0sn233gg2q4fm43k")))

(define-public crate-solana-noop-program-1.4.5 (c (n "solana-noop-program") (v "1.4.5") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-logger") (r "^1.4.5") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.4.5") (d #t) (k 0)))) (h "0qksyaf8c26a33m0fhckmyingpbm52a95p99v44rq64fix485p0z")))

(define-public crate-solana-noop-program-1.4.6 (c (n "solana-noop-program") (v "1.4.6") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-logger") (r "^1.4.6") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.4.6") (d #t) (k 0)))) (h "1id4sw1ydv8awyamss2x32fim2vldgs7cxmzglzcsfixfh5s0fkg")))

(define-public crate-solana-noop-program-1.4.7 (c (n "solana-noop-program") (v "1.4.7") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-logger") (r "^1.4.7") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.4.7") (d #t) (k 0)))) (h "13nbpm1vz85cgymr1cvrbw6wg26j2smmwvk3a23rxmma4wrn8n4q")))

(define-public crate-solana-noop-program-1.4.8 (c (n "solana-noop-program") (v "1.4.8") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-logger") (r "^1.4.8") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.4.8") (d #t) (k 0)))) (h "1lzf49n4n11q7fql2k4pa6dqgn5g9xd60gbbyn4plf1gig0zsd3p")))

(define-public crate-solana-noop-program-1.4.9 (c (n "solana-noop-program") (v "1.4.9") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-logger") (r "^1.4.9") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.4.9") (d #t) (k 0)))) (h "195iblphk7nizhhvca6aia8ifs2msr4qk35hl4p1fblgnx1wddv6")))

(define-public crate-solana-noop-program-1.4.10 (c (n "solana-noop-program") (v "1.4.10") (d (list (d (n "log") (r ">=0.4.8, <0.5.0") (d #t) (k 0)) (d (n "solana-logger") (r ">=1.4.10, <2.0.0") (d #t) (k 0)) (d (n "solana-sdk") (r ">=1.4.10, <2.0.0") (d #t) (k 0)))) (h "0qpxpmigdf6fldwnh1q4ydwiyf2sn73vsm23vapp8gjjivm1bv8x")))

(define-public crate-solana-noop-program-1.4.11 (c (n "solana-noop-program") (v "1.4.11") (d (list (d (n "log") (r ">=0.4.8, <0.5.0") (d #t) (k 0)) (d (n "solana-logger") (r ">=1.4.11, <2.0.0") (d #t) (k 0)) (d (n "solana-sdk") (r ">=1.4.11, <2.0.0") (d #t) (k 0)))) (h "1m74nw4vnidvxyry2j987qpxjjdjaazdkslg8ghdv7d69mbc2xwx")))

(define-public crate-solana-noop-program-1.4.12 (c (n "solana-noop-program") (v "1.4.12") (d (list (d (n "log") (r ">=0.4.8, <0.5.0") (d #t) (k 0)) (d (n "solana-logger") (r ">=1.4.12, <2.0.0") (d #t) (k 0)) (d (n "solana-sdk") (r ">=1.4.12, <2.0.0") (d #t) (k 0)))) (h "1mdq87rykc8pyzkpqv1w65krf990l839yz2bh658by0m14qavrhp")))

(define-public crate-solana-noop-program-1.4.13 (c (n "solana-noop-program") (v "1.4.13") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-logger") (r "^1.4.13") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.4.13") (d #t) (k 0)))) (h "0xynr60nq7nx66nvmdcpd3dzbhm144ddlj9959dgxgya8p0vzxpl")))

(define-public crate-solana-noop-program-1.4.14 (c (n "solana-noop-program") (v "1.4.14") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-logger") (r "^1.4.14") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.4.14") (d #t) (k 0)))) (h "03nafa2285cclssnzk03vmd9jxqvd7myj0kpp9j2higil3svqkpk")))

(define-public crate-solana-noop-program-1.4.15 (c (n "solana-noop-program") (v "1.4.15") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-logger") (r "^1.4.15") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.4.15") (d #t) (k 0)))) (h "0n2r4g2l1dfk425581yjzi22p8cp95l8l3zxzcc6f2g4xh10g451")))

(define-public crate-solana-noop-program-1.4.16 (c (n "solana-noop-program") (v "1.4.16") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-logger") (r "^1.4.16") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.4.16") (d #t) (k 0)))) (h "1rhx2z2zmq3a7w7xqzvks8vhgakhvcank5whxfrq2rj9f5kabrz8")))

(define-public crate-solana-noop-program-1.4.17 (c (n "solana-noop-program") (v "1.4.17") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-logger") (r "^1.4.17") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.4.17") (d #t) (k 0)))) (h "1a16hqmwg87pghl1dpxff0i728g6ydblz5x54bviqmlpnpz128ig")))

(define-public crate-solana-noop-program-1.5.0 (c (n "solana-noop-program") (v "1.5.0") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-logger") (r "^1.5.0") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.5.0") (d #t) (k 0)))) (h "0n4bspd7h59alml2c3q52kq9qd3d1639y01g4g8li8ywz35d8zdx")))

(define-public crate-solana-noop-program-1.4.18 (c (n "solana-noop-program") (v "1.4.18") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-logger") (r "^1.4.18") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.4.18") (d #t) (k 0)))) (h "076zkb39q75ipy1g11kwbshi3lnxyg7m2dsq8zngzjnypxxhj25x")))

(define-public crate-solana-noop-program-1.4.19 (c (n "solana-noop-program") (v "1.4.19") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-logger") (r "^1.4.19") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.4.19") (d #t) (k 0)))) (h "1w3nysk3sakwja4dcia9wcy18ls94nmrgw28rzbk7qlhbha8bzpj")))

(define-public crate-solana-noop-program-1.4.20 (c (n "solana-noop-program") (v "1.4.20") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-logger") (r "^1.4.20") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.4.20") (d #t) (k 0)))) (h "1ic88pb2hcd1sajxsg4mjvwqshhh2slz8sl1fjw3iag0zxyrh59y")))

(define-public crate-solana-noop-program-1.5.1 (c (n "solana-noop-program") (v "1.5.1") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-logger") (r "^1.5.1") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.5.1") (d #t) (k 0)))) (h "0dc9znjyb1glcl7nvxvzq810182sgm2h3vy05pr74sgasns1kki4")))

(define-public crate-solana-noop-program-1.4.21 (c (n "solana-noop-program") (v "1.4.21") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-logger") (r "^1.4.21") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.4.21") (d #t) (k 0)))) (h "0z9zqmw1v632gs3j6cym71jdirxgq0x3dp8kbxqip9mhq228dfvm")))

(define-public crate-solana-noop-program-1.4.22 (c (n "solana-noop-program") (v "1.4.22") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-logger") (r "^1.4.22") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.4.22") (d #t) (k 0)))) (h "0m9j8s20l1w2xrdpy97nrhyhj1fliwl2wbp6v0q2v3s83j7bvn6y")))

(define-public crate-solana-noop-program-1.5.2 (c (n "solana-noop-program") (v "1.5.2") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-logger") (r "^1.5.2") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.5.2") (d #t) (k 0)))) (h "0bvqqydh4rbfwxbf2cnpp2b2azhrh4vdr319ij30ch8j4i4fi3bz")))

(define-public crate-solana-noop-program-1.4.23 (c (n "solana-noop-program") (v "1.4.23") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-logger") (r "^1.4.23") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.4.23") (d #t) (k 0)))) (h "18kv85dln9275n6vjz3a6kb22yn20jds3s7cgk3pgmsnl2i4158c")))

(define-public crate-solana-noop-program-1.5.3 (c (n "solana-noop-program") (v "1.5.3") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-logger") (r "^1.5.3") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.5.3") (d #t) (k 0)))) (h "18qhn8s9dbnjxw1f4bhk1z2ih8ldks5ngdq9kp0zv4ir8r5kx01r")))

(define-public crate-solana-noop-program-1.5.4 (c (n "solana-noop-program") (v "1.5.4") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-logger") (r "^1.5.4") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.5.4") (d #t) (k 0)))) (h "0shjxlc577nqpq1j8pfkxnndlwaznfbx8j7rb5avjp7qa3jnjy08")))

(define-public crate-solana-noop-program-1.5.5 (c (n "solana-noop-program") (v "1.5.5") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-logger") (r "^1.5.5") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.5.5") (d #t) (k 0)))) (h "1j2j3c0cf4id4mzkcfgv6qpalmidcs3b67schnfanhyjk255klp3")))

(define-public crate-solana-noop-program-1.4.25 (c (n "solana-noop-program") (v "1.4.25") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-logger") (r "^1.4.25") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.4.25") (d #t) (k 0)))) (h "1db5pixhiixdp07fgxixa8zk4328varjhshvmfllf5haxi6ili8x")))

(define-public crate-solana-noop-program-1.4.26 (c (n "solana-noop-program") (v "1.4.26") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-logger") (r "^1.4.26") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.4.26") (d #t) (k 0)))) (h "0n0c0x3ky57mjif0n04m82h9wn2l4xyh3qdl0xs3qbfifma1wvsq")))

(define-public crate-solana-noop-program-1.5.6 (c (n "solana-noop-program") (v "1.5.6") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-logger") (r "^1.5.6") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.5.6") (d #t) (k 0)))) (h "0xkkb1zs396q8glkxlkivd59qf3r4z4phjnpgxcj1292f9gvj4wd")))

(define-public crate-solana-noop-program-1.4.27 (c (n "solana-noop-program") (v "1.4.27") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-logger") (r "^1.4.27") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.4.27") (d #t) (k 0)))) (h "02sjhwy56mi15br00ddgaklm4iyn38rfpgz99015cgqpqzy24hqy")))

(define-public crate-solana-noop-program-1.5.7 (c (n "solana-noop-program") (v "1.5.7") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-logger") (r "^1.5.7") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.5.7") (d #t) (k 0)))) (h "1ri44yrgcaxi5xkwjzi3jdx21ryjl742ccp71nrab3ilv62wgh10")))

(define-public crate-solana-noop-program-1.5.8 (c (n "solana-noop-program") (v "1.5.8") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-logger") (r "^1.5.8") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.5.8") (d #t) (k 0)))) (h "0pazlrpddqw9iawg1pwrihdwl86m87l234v30ivpl0qs0av4d0sy")))

(define-public crate-solana-noop-program-1.4.28 (c (n "solana-noop-program") (v "1.4.28") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-logger") (r "^1.4.28") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.4.28") (d #t) (k 0)))) (h "01cck933026ry5yv6x8f7gvf1jykvkifln5g1apih49p5knmhh95")))

(define-public crate-solana-noop-program-1.5.9 (c (n "solana-noop-program") (v "1.5.9") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-logger") (r "^1.5.9") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.5.9") (d #t) (k 0)))) (h "0ygrgq1d60d3wk7iaii1kj8hlq24hhb6bszf292pcy7mfcm32951")))

(define-public crate-solana-noop-program-1.5.10 (c (n "solana-noop-program") (v "1.5.10") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-logger") (r "^1.5.10") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.5.10") (d #t) (k 0)))) (h "15bygsjhhz40imp68w3vqg8wfh0xlw7w0kbg88pkrpdlsbl1z3kb")))

(define-public crate-solana-noop-program-1.5.11 (c (n "solana-noop-program") (v "1.5.11") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-logger") (r "^1.5.11") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.5.11") (d #t) (k 0)))) (h "1sxc2q9i8x4f76x23da2fvmwiklb0r0jn837hxalk1cpd468l3c7")))

(define-public crate-solana-noop-program-1.5.12 (c (n "solana-noop-program") (v "1.5.12") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-logger") (r "^1.5.12") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.5.12") (d #t) (k 0)))) (h "0dyf5454as0s9hsm66hzfh1n98cv0y2a4hmdgk80kdf1yh22lw1n")))

(define-public crate-solana-noop-program-1.5.13 (c (n "solana-noop-program") (v "1.5.13") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-logger") (r "^1.5.13") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.5.13") (d #t) (k 0)))) (h "00q8nsjisl4njdkr11dgbd49sghaf05wgkv5z8xd8lvkdmcj9ix3")))

(define-public crate-solana-noop-program-1.5.14 (c (n "solana-noop-program") (v "1.5.14") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-logger") (r "^1.5.14") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.5.14") (d #t) (k 0)))) (h "0dqk57m4z3zh99v8hfa9f35z7waszvajlc5bs7bnhjr160b3cvpv")))

(define-public crate-solana-noop-program-1.6.0 (c (n "solana-noop-program") (v "1.6.0") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-logger") (r "^1.6.0") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.6.0") (d #t) (k 0)))) (h "1y4syp4hcb74cm30q939dbc4izwhfsynkc4w2ikbimjgpbphfp45")))

(define-public crate-solana-noop-program-1.5.15 (c (n "solana-noop-program") (v "1.5.15") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-logger") (r "=1.5.15") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.5.15") (d #t) (k 0)))) (h "0g3vmwvvy04pwxm2zwr6m3yxnjl83sljyhq293y7s6f4rnakjig0")))

(define-public crate-solana-noop-program-1.6.1 (c (n "solana-noop-program") (v "1.6.1") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-logger") (r "=1.6.1") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.6.1") (d #t) (k 0)))) (h "0nr735w986vs2nx1qx1k1w0mb2indyjgm21q7sdmvrg0la8bj3ja")))

(define-public crate-solana-noop-program-1.5.16 (c (n "solana-noop-program") (v "1.5.16") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-logger") (r "=1.5.16") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.5.16") (d #t) (k 0)))) (h "0kks8r9s787ysb3zd7pkpm9q92j6401szbg2cnlhim299xca29pl")))

(define-public crate-solana-noop-program-1.5.17 (c (n "solana-noop-program") (v "1.5.17") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-logger") (r "=1.5.17") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.5.17") (d #t) (k 0)))) (h "1k3fhwhga60hc1gs1jiq5jwr1jy715alcbwa5mnq1cg80mmc6538")))

(define-public crate-solana-noop-program-1.6.2 (c (n "solana-noop-program") (v "1.6.2") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-logger") (r "=1.6.2") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.6.2") (d #t) (k 0)))) (h "09z77jcnka2j29vpvzw8x30zfag39a4dzw18zg6d2328v5aha00r")))

(define-public crate-solana-noop-program-1.6.3 (c (n "solana-noop-program") (v "1.6.3") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-logger") (r "=1.6.3") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.6.3") (d #t) (k 0)))) (h "0jcwvbd3ryj2sg8hkzx4v9ahw5yhc01vx1i8wp5ajkz6s8s5h19a")))

(define-public crate-solana-noop-program-1.6.4 (c (n "solana-noop-program") (v "1.6.4") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-logger") (r "=1.6.4") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.6.4") (d #t) (k 0)))) (h "1bbqisa4x6743aarik5g6vma8qyfn0sa73b4mp1vrbajrf0f92i2")))

(define-public crate-solana-noop-program-1.6.5 (c (n "solana-noop-program") (v "1.6.5") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-logger") (r "=1.6.5") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.6.5") (d #t) (k 0)))) (h "11rsfr6cjw3c9yllsiki016blpjff6xls3c91llgbwgbzp1nzwvs")))

(define-public crate-solana-noop-program-1.6.6 (c (n "solana-noop-program") (v "1.6.6") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-logger") (r "=1.6.6") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.6.6") (d #t) (k 0)))) (h "1hs2y298xf4rmh28v3pxyq8hhgggkhkbsmiz7a2cilgm235508gx")))

(define-public crate-solana-noop-program-1.5.19 (c (n "solana-noop-program") (v "1.5.19") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-logger") (r "=1.5.19") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.5.19") (d #t) (k 0)))) (h "0n141wf8m2jpzky20qxxdczqaz4ayx5l2hg9kj650rimkc2y5wvl")))

(define-public crate-solana-noop-program-1.6.7 (c (n "solana-noop-program") (v "1.6.7") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-logger") (r "=1.6.7") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.6.7") (d #t) (k 0)))) (h "1x0n86305kx1188gxm1i56m403nvdknhh34lcsa1h3zqm5n53823")))

(define-public crate-solana-noop-program-1.6.8 (c (n "solana-noop-program") (v "1.6.8") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-logger") (r "=1.6.8") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.6.8") (d #t) (k 0)))) (h "0aqvwa7jjj7rdr5s7203bf7qqq2z64jljm3a7582ihgfphls6rh3")))

(define-public crate-solana-noop-program-1.6.9 (c (n "solana-noop-program") (v "1.6.9") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-logger") (r "=1.6.9") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.6.9") (d #t) (k 0)))) (h "0kbkzhmc3d7f7g1p0s4pcgq3aznzbfbpr8ws8ln61c9v388cyf6d")))

(define-public crate-solana-noop-program-1.6.10 (c (n "solana-noop-program") (v "1.6.10") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-logger") (r "=1.6.10") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.6.10") (d #t) (k 0)))) (h "0kfkgy7q3c9x1zkk3drsr07d63pr7g4ny131dzgv527y2p5y20f2")))

(define-public crate-solana-noop-program-1.6.11 (c (n "solana-noop-program") (v "1.6.11") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-logger") (r "=1.6.11") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.6.11") (d #t) (k 0)))) (h "1jcahylqg344g4biw7381k9h632a8y6d41h916vlirj8v4pyypxc")))

(define-public crate-solana-noop-program-1.7.0 (c (n "solana-noop-program") (v "1.7.0") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-logger") (r "=1.7.0") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.7.0") (d #t) (k 0)))) (h "1aj7wn1h5531y9wwy2wl5rhf1d8n36myvk35waihn27c2h4r1rq7")))

(define-public crate-solana-noop-program-1.7.1 (c (n "solana-noop-program") (v "1.7.1") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-logger") (r "=1.7.1") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.7.1") (d #t) (k 0)))) (h "1vpbjccl6m9qs403kszgyy43kwvbpfvzjcy9dnknils36gzlxxsj")))

(define-public crate-solana-noop-program-1.6.12 (c (n "solana-noop-program") (v "1.6.12") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-logger") (r "=1.6.12") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.6.12") (d #t) (k 0)))) (h "18dbdr4bwfy3z92hiy7chpby417gnc2szfpm85rpx6hiw8wbpflh")))

(define-public crate-solana-noop-program-1.6.13 (c (n "solana-noop-program") (v "1.6.13") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-logger") (r "=1.6.13") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.6.13") (d #t) (k 0)))) (h "1lxcikfm5zcz1ppsrm7d6a2s6x1r1x1lnfyx6105d8cfyhy3jafk")))

(define-public crate-solana-noop-program-1.7.2 (c (n "solana-noop-program") (v "1.7.2") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-logger") (r "=1.7.2") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.7.2") (d #t) (k 0)))) (h "16rqc21dk5hx6jz5czgkkniplqv6a2sfwmam4af47fx8ggvmk14a")))

(define-public crate-solana-noop-program-1.6.14 (c (n "solana-noop-program") (v "1.6.14") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-logger") (r "=1.6.14") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.6.14") (d #t) (k 0)))) (h "1hzvr9iv9gq60srldxcqdfk6lmih949ywdscm6cfm7ayr6rrrqh4")))

(define-public crate-solana-noop-program-1.7.3 (c (n "solana-noop-program") (v "1.7.3") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-logger") (r "=1.7.3") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.7.3") (d #t) (k 0)))) (h "1l0lp1cn5b3zq2xp4pxzfd9fvp7mgsijfhgh82b06y0ip9m0dwvf")))

(define-public crate-solana-noop-program-1.6.15 (c (n "solana-noop-program") (v "1.6.15") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-logger") (r "=1.6.15") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.6.15") (d #t) (k 0)))) (h "15pn9dvv59kbqbgff6pf1n7rpdg9r5n7cywp3fhf5y2cn109nz77")))

(define-public crate-solana-noop-program-1.7.4 (c (n "solana-noop-program") (v "1.7.4") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-logger") (r "=1.7.4") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.7.4") (d #t) (k 0)))) (h "1zn56i2b0mnhm0ad5kgz104fmi0wmqad837zrhk2sbcn0nln0pml")))

(define-public crate-solana-noop-program-1.6.16 (c (n "solana-noop-program") (v "1.6.16") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-logger") (r "=1.6.16") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.6.16") (d #t) (k 0)))) (h "0bgaxfdg4g1qp9a5dx7yvigg3c4c7dggg61h5d65yz0vl8ik1w22")))

(define-public crate-solana-noop-program-1.6.17 (c (n "solana-noop-program") (v "1.6.17") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-logger") (r "=1.6.17") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.6.17") (d #t) (k 0)))) (h "08qhrmp8a3p7n9r9caqfn9x3iajpvpai0n0qw4zj03776jmiwixw")))

(define-public crate-solana-noop-program-1.7.5 (c (n "solana-noop-program") (v "1.7.5") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-logger") (r "=1.7.5") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.7.5") (d #t) (k 0)))) (h "08m6cspgi73kv25an9pq6mp4ydjklzff5qvxy57n7d28bqfsp0x2")))

(define-public crate-solana-noop-program-1.7.6 (c (n "solana-noop-program") (v "1.7.6") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-logger") (r "=1.7.6") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.7.6") (d #t) (k 0)))) (h "0ylblffcqnvkfyafaya4952vwdcmd622pjil3yrsjkhql27dwj72")))

(define-public crate-solana-noop-program-1.6.18 (c (n "solana-noop-program") (v "1.6.18") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-logger") (r "=1.6.18") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.6.18") (d #t) (k 0)))) (h "0hxc9c4ipswlf6x9i76ng4b8pvbih6cdmf00pgd6r3g1gvn7ijxr")))

(define-public crate-solana-noop-program-1.6.19 (c (n "solana-noop-program") (v "1.6.19") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-logger") (r "=1.6.19") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.6.19") (d #t) (k 0)))) (h "17645mqa23dibw5w76gq3yrzgpprzp0gj2s2xx4yqsf8pprlam4f")))

(define-public crate-solana-noop-program-1.7.7 (c (n "solana-noop-program") (v "1.7.7") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-logger") (r "=1.7.7") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.7.7") (d #t) (k 0)))) (h "1nm777gwyiws9lyppxv5y7x0q5ajivhf7fa0478i6ws3nm5bxv6g")))

(define-public crate-solana-noop-program-1.7.8 (c (n "solana-noop-program") (v "1.7.8") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-logger") (r "=1.7.8") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.7.8") (d #t) (k 0)))) (h "08jvq447wnd52kpqr2fvwm7k1a66n3749v24i6q46lga7yhadn26")))

(define-public crate-solana-noop-program-1.6.20 (c (n "solana-noop-program") (v "1.6.20") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-logger") (r "=1.6.20") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.6.20") (d #t) (k 0)))) (h "17ynqpfvkvwlnqi19wipp062zmd77ymy2i8smhdxhjhr4y062xgs")))

(define-public crate-solana-noop-program-1.7.9 (c (n "solana-noop-program") (v "1.7.9") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-logger") (r "=1.7.9") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.7.9") (d #t) (k 0)))) (h "1xb0453jn6pij8gdvylr131y684ajqkrlfzrakcbv2a2f7ql02kn")))

(define-public crate-solana-noop-program-1.7.10 (c (n "solana-noop-program") (v "1.7.10") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-logger") (r "=1.7.10") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.7.10") (d #t) (k 0)))) (h "078a7nixh8m2f0rcdzdcyn55gs3cx05imj10ifb56p6fkh2cac61")))

(define-public crate-solana-noop-program-1.6.21 (c (n "solana-noop-program") (v "1.6.21") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-logger") (r "=1.6.21") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.6.21") (d #t) (k 0)))) (h "06nsq6fc2mf2i2vbicfjzvfxgwd532y6h4mca2xf5cqwjx2ix8ll")))

(define-public crate-solana-noop-program-1.7.11 (c (n "solana-noop-program") (v "1.7.11") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-logger") (r "=1.7.11") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.7.11") (d #t) (k 0)))) (h "1kl5pfv9365i5vmmbaiifcm42zic4h5jdbfc9ighg1vvylcl0n9m")))

(define-public crate-solana-noop-program-1.6.22 (c (n "solana-noop-program") (v "1.6.22") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-logger") (r "=1.6.22") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.6.22") (d #t) (k 0)))) (h "04pwqfp0bs2ayma9fjb4di156ib94xkplxaqpa446fc5ka5x8p0c")))

(define-public crate-solana-noop-program-1.6.24 (c (n "solana-noop-program") (v "1.6.24") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-logger") (r "=1.6.24") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.6.24") (d #t) (k 0)))) (h "13abg1zwzv71xyja33gj1nn5a2mbxlzy8f8yvhr7gqx6vry4v64n")))

(define-public crate-solana-noop-program-1.6.25 (c (n "solana-noop-program") (v "1.6.25") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-logger") (r "=1.6.25") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.6.25") (d #t) (k 0)))) (h "0lv52jvkqvzddmmx59bnp02k79mq9hdxhqsnbszjb8f1z8x4h1fb")))

(define-public crate-solana-noop-program-1.7.12 (c (n "solana-noop-program") (v "1.7.12") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-logger") (r "=1.7.12") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.7.12") (d #t) (k 0)))) (h "031sx9rrszxl10d1zs8yr8ikwgwryy3v82zp6hs2fh6a6g0zwgz4")))

(define-public crate-solana-noop-program-1.6.26 (c (n "solana-noop-program") (v "1.6.26") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-logger") (r "=1.6.26") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.6.26") (d #t) (k 0)))) (h "1821qz1yp8ga31xz9zglx0zsv3427z1xyf4rbklq5vjz8akkmv64")))

(define-public crate-solana-noop-program-1.6.27 (c (n "solana-noop-program") (v "1.6.27") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-logger") (r "=1.6.27") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.6.27") (d #t) (k 0)))) (h "0ba4wqwvkadyz773rgsa5hd2j2zkvxd5mrc473sp7kiahxj2nc3s")))

(define-public crate-solana-noop-program-1.7.13 (c (n "solana-noop-program") (v "1.7.13") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-logger") (r "=1.7.13") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.7.13") (d #t) (k 0)))) (h "02zc8f27ldy9cwk0v3ihzal5vgmm6w9d99g06gh2nhbrc6rl2v0m")))

(define-public crate-solana-noop-program-1.7.14 (c (n "solana-noop-program") (v "1.7.14") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-logger") (r "=1.7.14") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.7.14") (d #t) (k 0)))) (h "07kkyb6piyb65p5vvrd3q25wc8mw2rkm8ssg7bcg7770srbbs5v5")))

(define-public crate-solana-noop-program-1.8.0 (c (n "solana-noop-program") (v "1.8.0") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-logger") (r "=1.8.0") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.8.0") (d #t) (k 0)))) (h "07qsbyagka0dhhb50dhlf7mxjry15d60bq8ljhxr2wgq28dnix90")))

(define-public crate-solana-noop-program-1.6.28 (c (n "solana-noop-program") (v "1.6.28") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-logger") (r "=1.6.28") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.6.28") (d #t) (k 0)))) (h "028gqfxgfq9yzgac86qm4l84smqrxf3bjl1v1hfcf5f0vpwsibpv")))

(define-public crate-solana-noop-program-1.7.15 (c (n "solana-noop-program") (v "1.7.15") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-logger") (r "=1.7.15") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.7.15") (d #t) (k 0)))) (h "1ns28l307bd6acr50qg1rf8vm2xjkwfk6qf8qxsg8b0hv347f40f")))

