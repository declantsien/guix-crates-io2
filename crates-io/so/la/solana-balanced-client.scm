(define-module (crates-io so la solana-balanced-client) #:use-module (crates-io))

(define-public crate-solana-balanced-client-0.1.0 (c (n "solana-balanced-client") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 0)) (d (n "solana-account-decoder") (r "^1.14.7") (d #t) (k 0)) (d (n "solana-client") (r "^1.14.7") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.14.7") (d #t) (k 0)) (d (n "solana-transaction-status") (r "^1.14.7") (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("sync"))) (d #t) (k 0)))) (h "14vllvv6hx6f2jnj3jx5sn97qkramgz0l7m22b90fgw3zarmrqaz")))

