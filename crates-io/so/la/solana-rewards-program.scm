(define-module (crates-io so la solana-rewards-program) #:use-module (crates-io))

(define-public crate-solana-rewards-program-0.12.0 (c (n "solana-rewards-program") (v "0.12.0") (d (list (d (n "bincode") (r "^1.1.2") (d #t) (k 0)) (d (n "log") (r "^0.4.2") (d #t) (k 0)) (d (n "solana-logger") (r "^0.12.0") (d #t) (k 0)) (d (n "solana-rewards-api") (r "^0.12.0") (d #t) (k 0)) (d (n "solana-runtime") (r "^0.12.0") (d #t) (k 2)) (d (n "solana-sdk") (r "^0.12.0") (d #t) (k 0)) (d (n "solana-vote-api") (r "^0.12.0") (d #t) (k 0)))) (h "0zs7pgrrcax7wkj34n377zgay3ww9fa5icr14lms0zsbcfadsbzp")))

