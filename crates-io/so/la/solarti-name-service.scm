(define-module (crates-io so la solarti-name-service) #:use-module (crates-io))

(define-public crate-solarti-name-service-0.2.0 (c (n "solarti-name-service") (v "0.2.0") (d (list (d (n "borsh") (r "^0.9.1") (d #t) (k 0)) (d (n "num-derive") (r "^0.3.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "solana-program") (r "^1.14.12") (d #t) (k 0)) (d (n "solana-program-test") (r "^1.14.12") (d #t) (k 2)) (d (n "solana-sdk") (r "^1.14.12") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.29") (d #t) (k 0)))) (h "1jrfwc6vj4j2jgwgjk3mkiwhgcanc8mfkdq5x7gfcvi6w571jzy5") (f (quote (("test-sbf") ("no-entrypoint"))))))

(define-public crate-solarti-name-service-0.2.1 (c (n "solarti-name-service") (v "0.2.1") (d (list (d (n "borsh") (r "^0.9.1") (d #t) (k 0)) (d (n "miraland-program") (r "=1.14.17-rc4") (d #t) (k 0)) (d (n "miraland-program-test") (r "=1.14.17-rc4") (d #t) (k 2)) (d (n "miraland-sdk") (r "=1.14.17-rc4") (d #t) (k 2)) (d (n "num-derive") (r "^0.3.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.29") (d #t) (k 0)))) (h "153cqqbmymirkjz6w46xp44gwmm4kncmffd8j47l8fl7js3a2c01") (f (quote (("test-sbf") ("no-entrypoint"))))))

