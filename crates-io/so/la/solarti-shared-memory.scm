(define-module (crates-io so la solarti-shared-memory) #:use-module (crates-io))

(define-public crate-solarti-shared-memory-2.0.6 (c (n "solarti-shared-memory") (v "2.0.6") (d (list (d (n "arrayref") (r "^0.3.6") (d #t) (k 0)) (d (n "solana-program") (r "=1.14.12") (d #t) (k 0)) (d (n "solana-program-test") (r "=1.14.12") (d #t) (k 2)) (d (n "solana-sdk") (r "=1.14.12") (d #t) (k 2)))) (h "1zcgnm7fwrpcmwbmqzs7ks666s2v6k7n8571snsnks3j5g34db0j") (f (quote (("test-sbf"))))))

(define-public crate-solarti-shared-memory-2.0.7 (c (n "solarti-shared-memory") (v "2.0.7") (d (list (d (n "arrayref") (r "^0.3.6") (d #t) (k 0)) (d (n "miraland-program") (r "=1.14.17-rc4") (d #t) (k 0)) (d (n "miraland-program-test") (r "=1.14.17-rc4") (d #t) (k 2)) (d (n "miraland-sdk") (r "=1.14.17-rc4") (d #t) (k 2)))) (h "12hp1404z2k5mq2bn4b450zkhdkl7isykkynkxhmmfsnalchw5kr") (f (quote (("test-sbf"))))))

