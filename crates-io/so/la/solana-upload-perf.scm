(define-module (crates-io so la solana-upload-perf) #:use-module (crates-io))

(define-public crate-solana-upload-perf-0.15.0 (c (n "solana-upload-perf") (v "0.15.0") (d (list (d (n "log") (r "^0.4.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)) (d (n "solana-metrics") (r "^0.15.0") (d #t) (k 0)))) (h "07fggc9zb718vdwcr7a9m0621r1qgb3xs2px5yy5vzpk9fxsbxha")))

