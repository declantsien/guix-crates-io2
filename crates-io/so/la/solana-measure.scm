(define-module (crates-io so la solana-measure) #:use-module (crates-io))

(define-public crate-solana-measure-0.17.0 (c (n "solana-measure") (v "0.17.0") (d (list (d (n "solana-sdk") (r "^0.17.0") (d #t) (k 0)))) (h "0a1ilsv228scna4ckzw4qa7x2nb3y6rj9i9jmip6k9p98hj9fp0a")))

(define-public crate-solana-measure-0.17.1 (c (n "solana-measure") (v "0.17.1") (d (list (d (n "solana-sdk") (r "^0.17.1") (d #t) (k 0)))) (h "15kyhsvxirxfzy955iy95pj2n24wra8kklwi0b7db22qrn9kpby0")))

(define-public crate-solana-measure-0.18.0-pre0 (c (n "solana-measure") (v "0.18.0-pre0") (d (list (d (n "solana-sdk") (r "^0.18.0-pre0") (d #t) (k 0)))) (h "0466r6hkiig63p0fs827a5bzmn472jci4zk9yxca50g3v1y22ks8")))

(define-public crate-solana-measure-0.18.0-pre1 (c (n "solana-measure") (v "0.18.0-pre1") (d (list (d (n "solana-sdk") (r "^0.18.0-pre1") (d #t) (k 0)))) (h "0rxz21aj2s99vmx38kn2c17g70ac4y8nxafwsv8g0ja88fgq8v91")))

(define-public crate-solana-measure-0.17.2 (c (n "solana-measure") (v "0.17.2") (d (list (d (n "solana-sdk") (r "^0.17.2") (d #t) (k 0)))) (h "1slk7aish7xngl1ashn4s3lz5zfrccc4jjy4v2alhcinvl1yqfbi")))

(define-public crate-solana-measure-0.18.0-pre2 (c (n "solana-measure") (v "0.18.0-pre2") (d (list (d (n "solana-sdk") (r "^0.18.0-pre2") (d #t) (k 0)))) (h "0g6lppz8y60k39jj3s5izmbkr5h984q7r7pi7pg6qsn1cqajvkhd")))

(define-public crate-solana-measure-0.18.0 (c (n "solana-measure") (v "0.18.0") (d (list (d (n "solana-sdk") (r "^0.18.0") (d #t) (k 0)))) (h "0w06bw0vzbdy77cvh6z6ydliv9vd6gnzmyl090jy6325b28sazlc")))

(define-public crate-solana-measure-0.18.1 (c (n "solana-measure") (v "0.18.1") (d (list (d (n "solana-sdk") (r "^0.18.1") (d #t) (k 0)))) (h "0apiqpr7cx6hs51yndn3l28h0qp9md47ycwz0n8346mr7fjaj8ng")))

(define-public crate-solana-measure-0.19.1 (c (n "solana-measure") (v "0.19.1") (d (list (d (n "solana-sdk") (r "^0.19.1") (d #t) (k 0)))) (h "1f9y4xynrbby3ypj9xijrq3ya0pyzz4qic66394igkwpcx9pip0s")))

(define-public crate-solana-measure-0.20.1 (c (n "solana-measure") (v "0.20.1") (d (list (d (n "solana-sdk") (r "^0.20.1") (d #t) (k 0)))) (h "00xj9m5pzcdvxs8gqsjs0p0qbdm92h6r9c3b0zs7hn79m3xnkvcb")))

(define-public crate-solana-measure-0.20.2 (c (n "solana-measure") (v "0.20.2") (d (list (d (n "solana-sdk") (r "^0.20.2") (d #t) (k 0)))) (h "05dizrmjnjg9hzirdhlx8rshlknc5xsl6mh1lk9imkm7kq408axa")))

(define-public crate-solana-measure-0.20.3 (c (n "solana-measure") (v "0.20.3") (d (list (d (n "solana-sdk") (r "^0.20.3") (d #t) (k 0)))) (h "0rsbmd2lk7p4q8v2v34lbjj54g72dlsm7k640ib41259zg7vl6r3")))

(define-public crate-solana-measure-0.20.4 (c (n "solana-measure") (v "0.20.4") (d (list (d (n "solana-sdk") (r "^0.20.4") (d #t) (k 0)))) (h "1fsysxsry2l0h6ycsaanys5ahzgi54vz3fi9p78kww44fj092jd5")))

(define-public crate-solana-measure-0.20.5 (c (n "solana-measure") (v "0.20.5") (d (list (d (n "solana-sdk") (r "^0.20.5") (d #t) (k 0)))) (h "1c9x76yhmvgykvm5lvd78j144m4g8fa84yw8ll29p61411j0fyjj")))

(define-public crate-solana-measure-0.21.0 (c (n "solana-measure") (v "0.21.0") (d (list (d (n "solana-sdk") (r "^0.21.0") (d #t) (k 0)))) (h "1mr1mhd2qdrynnv8qkk80sbyfqfgy44r5aal0n13bfwd2309gfiq")))

(define-public crate-solana-measure-0.22.0 (c (n "solana-measure") (v "0.22.0") (d (list (d (n "solana-sdk") (r "^0.22.0") (d #t) (k 0)))) (h "0497db8yma471dczglrggkn8m5p0qikgawq9fvvq2l0yw7iad575")))

(define-public crate-solana-measure-0.22.1 (c (n "solana-measure") (v "0.22.1") (d (list (d (n "solana-sdk") (r "^0.22.1") (d #t) (k 0)))) (h "00whizv5x3g1vwambwn4s7c97agyvd2k8smb7vphn4mjk1ks6jrw")))

(define-public crate-solana-measure-0.22.2 (c (n "solana-measure") (v "0.22.2") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-metrics") (r "^0.22.2") (d #t) (k 0)) (d (n "solana-sdk") (r "^0.22.2") (d #t) (k 0)))) (h "0z5paa2wqs6zwdayfg8lj0a9w4kc88pd50bkdyiks7pwfmhi3ppi")))

(define-public crate-solana-measure-0.22.3 (c (n "solana-measure") (v "0.22.3") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-metrics") (r "^0.22.3") (d #t) (k 0)) (d (n "solana-sdk") (r "^0.22.3") (d #t) (k 0)))) (h "0sg12b16nbkb4mi361n08dcbm704y4krvvkld3wiry2wf1imc65r")))

(define-public crate-solana-measure-0.22.4 (c (n "solana-measure") (v "0.22.4") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-metrics") (r "^0.22.4") (d #t) (k 0)) (d (n "solana-sdk") (r "^0.22.4") (d #t) (k 0)))) (h "10fy2jzbf6mkbqiy35misg9lzjwmcvnd93xz9qq0k69gfmja9hl4")))

(define-public crate-solana-measure-0.23.0 (c (n "solana-measure") (v "0.23.0") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-metrics") (r "^0.23.0") (d #t) (k 0)) (d (n "solana-sdk") (r "^0.23.0") (d #t) (k 0)))) (h "06dhk1injj2s3apa58qzsg8w778acpaw9pcvwgp95dqcj8rcfli1")))

(define-public crate-solana-measure-0.22.5 (c (n "solana-measure") (v "0.22.5") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-metrics") (r "^0.22.5") (d #t) (k 0)) (d (n "solana-sdk") (r "^0.22.5") (d #t) (k 0)))) (h "18v3b97c3ni9v6v8rqn1ykfjllxx13bjq7x5ny1zdmva1k8rbqvy")))

(define-public crate-solana-measure-0.22.6 (c (n "solana-measure") (v "0.22.6") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-metrics") (r "^0.22.6") (d #t) (k 0)) (d (n "solana-sdk") (r "^0.22.6") (d #t) (k 0)))) (h "0vmwf8b7k7bxnl7r3h1z0vprv2f1ckb02q7y3cxfrwfpwc7pddrk")))

(define-public crate-solana-measure-0.23.1 (c (n "solana-measure") (v "0.23.1") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-metrics") (r "^0.23.1") (d #t) (k 0)) (d (n "solana-sdk") (r "^0.23.1") (d #t) (k 0)))) (h "1mbr7zhzg72i4r2717vk4678l2j0rq6p54gyag2k8wllfgl1ycpd")))

(define-public crate-solana-measure-0.23.2 (c (n "solana-measure") (v "0.23.2") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-metrics") (r "^0.23.2") (d #t) (k 0)) (d (n "solana-sdk") (r "^0.23.2") (d #t) (k 0)))) (h "1qrzx6glzywgs4gir758hcm6xdmzj89j1f1dnwzcx1w47vx1294q")))

(define-public crate-solana-measure-0.22.7 (c (n "solana-measure") (v "0.22.7") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-metrics") (r "^0.22.7") (d #t) (k 0)) (d (n "solana-sdk") (r "^0.22.7") (d #t) (k 0)))) (h "1h9hifids4q89gzdya8q5lwf2nkiiafcrx72ab1fk0cwmhd6mbm1")))

(define-public crate-solana-measure-0.23.3 (c (n "solana-measure") (v "0.23.3") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-metrics") (r "^0.23.3") (d #t) (k 0)) (d (n "solana-sdk") (r "^0.23.3") (d #t) (k 0)))) (h "1b4jk0hdbrqrcnzk0ysihwn05amxc7n3wd6y02pnwsfq3h724b9k")))

(define-public crate-solana-measure-0.23.4 (c (n "solana-measure") (v "0.23.4") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-metrics") (r "^0.23.4") (d #t) (k 0)) (d (n "solana-sdk") (r "^0.23.4") (d #t) (k 0)))) (h "06iia9s6awgxrqfqhxgv2vvs6nw35n1z1fyc30fi0k35w0k2k4k7")))

(define-public crate-solana-measure-0.22.8 (c (n "solana-measure") (v "0.22.8") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-metrics") (r "^0.22.8") (d #t) (k 0)) (d (n "solana-sdk") (r "^0.22.8") (d #t) (k 0)))) (h "05sfpyzmd3gpmrwpn3nqpw8pr5965acxw0wfhn1vf82z0r28rykl")))

(define-public crate-solana-measure-0.23.5 (c (n "solana-measure") (v "0.23.5") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-metrics") (r "^0.23.5") (d #t) (k 0)) (d (n "solana-sdk") (r "^0.23.5") (d #t) (k 0)))) (h "064qaavz1f5n6iy7kj1kzk23h2v4h3m0j108llk98jiyijs0hqkl")))

(define-public crate-solana-measure-0.23.6 (c (n "solana-measure") (v "0.23.6") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-metrics") (r "^0.23.6") (d #t) (k 0)) (d (n "solana-sdk") (r "^0.23.6") (d #t) (k 0)))) (h "0i98mf79s30w4kiq0pzsy3ma30l5lph12gzi3smsb80jhckxbszi")))

(define-public crate-solana-measure-1.0.0 (c (n "solana-measure") (v "1.0.0") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-metrics") (r "^1.0.0") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.0.0") (d #t) (k 0)))) (h "0dacy5dnag7m2lbw8hyfbjgncirkgfsmdapili3a5hryq0dzb413")))

(define-public crate-solana-measure-0.23.7 (c (n "solana-measure") (v "0.23.7") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-metrics") (r "^0.23.7") (d #t) (k 0)) (d (n "solana-sdk") (r "^0.23.7") (d #t) (k 0)))) (h "0vv3w718yslpbb8irr4hwcjw8dla8w3322v6iihfs7q4xl21m75m")))

(define-public crate-solana-measure-0.23.8 (c (n "solana-measure") (v "0.23.8") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-metrics") (r "^0.23.8") (d #t) (k 0)) (d (n "solana-sdk") (r "^0.23.8") (d #t) (k 0)))) (h "0yj6aj96rjwhrfs7kxay8jffaqgmq7p3caxa3ajrvvm4xqwas19r")))

(define-public crate-solana-measure-1.0.1 (c (n "solana-measure") (v "1.0.1") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-metrics") (r "^1.0.1") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.0.1") (d #t) (k 0)))) (h "1v8q842b19zp5s1hg0r6rmycynmhs2pxnyjbyn7f2y0v8ljm8167")))

(define-public crate-solana-measure-1.0.2 (c (n "solana-measure") (v "1.0.2") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-metrics") (r "^1.0.2") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.0.2") (d #t) (k 0)))) (h "02ihwhqsgzv0mkw1z6i3dfdd9vqb4aslmabrbrnd39ps3nj8nhwy")))

(define-public crate-solana-measure-1.0.3 (c (n "solana-measure") (v "1.0.3") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-metrics") (r "^1.0.3") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.0.3") (d #t) (k 0)))) (h "1ll89k4873cc751j0d4k7bsvc0rzgbgcyr7j3lq1sf8kld1var0p")))

(define-public crate-solana-measure-1.0.4 (c (n "solana-measure") (v "1.0.4") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-metrics") (r "^1.0.4") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.0.4") (d #t) (k 0)))) (h "1nv56m1jwjmwfhf0x1cabj8338hk39w4qy7d7f65b23rn97b7hiv")))

(define-public crate-solana-measure-1.0.5 (c (n "solana-measure") (v "1.0.5") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-metrics") (r "^1.0.5") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.0.5") (d #t) (k 0)))) (h "1w6g9m2nmpr2anyhmlhapl14jpfz3b9f0ddpr208iv1b8npxd8gg")))

(define-public crate-solana-measure-1.0.6 (c (n "solana-measure") (v "1.0.6") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-metrics") (r "^1.0.6") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.0.6") (d #t) (k 0)))) (h "1pzjry39d2ckxz5155c38jc5bs2kgrcvmxad77m2mwd7n154ksy7")))

(define-public crate-solana-measure-1.0.7 (c (n "solana-measure") (v "1.0.7") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-metrics") (r "^1.0.7") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.0.7") (d #t) (k 0)))) (h "0hps4fh8ln3mvikxkp1sqgm6jrg69161y57agqwngdjbwb15dagi")))

(define-public crate-solana-measure-1.0.8 (c (n "solana-measure") (v "1.0.8") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-metrics") (r "^1.0.8") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.0.8") (d #t) (k 0)))) (h "1xw386l5zv42gipba16c3c8r2pl0msw7qrxbspx33qwipf7ilfxh")))

(define-public crate-solana-measure-1.0.9 (c (n "solana-measure") (v "1.0.9") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-metrics") (r "^1.0.9") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.0.9") (d #t) (k 0)))) (h "1qc7jvjzz8mp1bycsnsdsav3gxq8y376mx93vcx2x8cjs6h1bal4")))

(define-public crate-solana-measure-1.0.10 (c (n "solana-measure") (v "1.0.10") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-metrics") (r "^1.0.10") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.0.10") (d #t) (k 0)))) (h "0wpxm7rwxv5mbaqpkxqwh7s7bc6vymsn65whw8f8ypwyp15gxjrp")))

(define-public crate-solana-measure-1.0.11 (c (n "solana-measure") (v "1.0.11") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-metrics") (r "^1.0.11") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.0.11") (d #t) (k 0)))) (h "0xxsgwfban87s1pbgrdx22phvs8iih959c6vdf14qxz8ck7fpcj9")))

(define-public crate-solana-measure-1.0.12 (c (n "solana-measure") (v "1.0.12") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-metrics") (r "^1.0.12") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.0.12") (d #t) (k 0)))) (h "1h4h9v6adkxb6cfl6z3s700zg3qrwz7cbmm55s91k1lb6zrqsi1a")))

(define-public crate-solana-measure-1.1.0 (c (n "solana-measure") (v "1.1.0") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-metrics") (r "^1.1.0") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.1.0") (d #t) (k 0)))) (h "1dpixcidk1pjqrmbzv1p4krq5c83zzj34wadipq8j3mmfnic968z")))

(define-public crate-solana-measure-1.1.1 (c (n "solana-measure") (v "1.1.1") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-metrics") (r "^1.1.1") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.1.1") (d #t) (k 0)))) (h "1wxra607c9m7pgs5gkaw78qpk67cg281b5gbcab10lp4pfpzbnxk")))

(define-public crate-solana-measure-1.0.13 (c (n "solana-measure") (v "1.0.13") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-metrics") (r "^1.0.13") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.0.13") (d #t) (k 0)))) (h "1nv3hyrfv16h0i9h4dp89j5j131cidwafnsszspvicjfnq893rzz")))

(define-public crate-solana-measure-1.1.2 (c (n "solana-measure") (v "1.1.2") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-metrics") (r "^1.1.2") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.1.2") (d #t) (k 0)))) (h "0419s0h43m5139w2v69vinqds32z45kc9f278kjgjffh844w63bg")))

(define-public crate-solana-measure-1.0.14 (c (n "solana-measure") (v "1.0.14") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-metrics") (r "^1.0.14") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.0.14") (d #t) (k 0)))) (h "0ma810q7jy7xrvp8qxxdankmysf2zw281k78xi64x3v4xqw7lq7c")))

(define-public crate-solana-measure-1.0.15 (c (n "solana-measure") (v "1.0.15") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-metrics") (r "^1.0.15") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.0.15") (d #t) (k 0)))) (h "0wijd6mln9nm9987dh2r6d3p3bfnbc9w29vd6ga314dzv19sm9qr")))

(define-public crate-solana-measure-1.0.16 (c (n "solana-measure") (v "1.0.16") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-metrics") (r "^1.0.16") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.0.16") (d #t) (k 0)))) (h "1nfxwlyin56myjrr09jpmpm47fr43p3wif9b1k2cnlr397zklqz8")))

(define-public crate-solana-measure-1.1.3 (c (n "solana-measure") (v "1.1.3") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-metrics") (r "^1.1.3") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.1.3") (d #t) (k 0)))) (h "043z0fxrs7m421pi1w126rw2x5gvlpwk8lq8mqmcvx5xz7m5mmav")))

(define-public crate-solana-measure-1.0.17 (c (n "solana-measure") (v "1.0.17") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-metrics") (r "^1.0.17") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.0.17") (d #t) (k 0)))) (h "1q9gqfixh4vrs9b9x87bw0nhwcnfy46dx8vv0klmafy0and0rc9f")))

(define-public crate-solana-measure-1.0.18 (c (n "solana-measure") (v "1.0.18") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-metrics") (r "^1.0.18") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.0.18") (d #t) (k 0)))) (h "03qk29gk6sncslsdszmn84rg3j5pc7hdcx0q08iv19sk8fawk076")))

(define-public crate-solana-measure-1.1.5 (c (n "solana-measure") (v "1.1.5") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-metrics") (r "^1.1.5") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.1.5") (d #t) (k 0)))) (h "0dqbi90922p0m94yv9ayqlxf3h5m7kry48mpp6jppva5r584klbv")))

(define-public crate-solana-measure-1.1.6 (c (n "solana-measure") (v "1.1.6") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-metrics") (r "^1.1.6") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.1.6") (d #t) (k 0)))) (h "049m0pwhwj316d7xdmz2y22ka0g93b3rz74ayrx2rlp4pp6xgdb6")))

(define-public crate-solana-measure-1.1.7 (c (n "solana-measure") (v "1.1.7") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-metrics") (r "^1.1.7") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.1.7") (d #t) (k 0)))) (h "18cvd1wbr37zsy5brfv75ih61ccp6z5drh8n9cmm4fwi8fd01mx3")))

(define-public crate-solana-measure-1.0.20 (c (n "solana-measure") (v "1.0.20") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-metrics") (r "^1.0.20") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.0.20") (d #t) (k 0)))) (h "1qgap6xfd5gq2n8vjzz5w23dl6bjpiv9j60g8379xdlkjipm8f8k")))

(define-public crate-solana-measure-1.0.21 (c (n "solana-measure") (v "1.0.21") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-metrics") (r "^1.0.21") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.0.21") (d #t) (k 0)))) (h "0nnb2kq0cykz21kivnc7nxgs766j24ajr21zyf99c6plavfmyhmz")))

(define-public crate-solana-measure-1.1.8 (c (n "solana-measure") (v "1.1.8") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-metrics") (r "^1.1.8") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.1.8") (d #t) (k 0)))) (h "0pkfd1vvwh22dmf4zjhryj0c45kk13wjzrg0h2bv0x939bazwimc")))

(define-public crate-solana-measure-1.1.9 (c (n "solana-measure") (v "1.1.9") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-metrics") (r "^1.1.9") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.1.9") (d #t) (k 0)))) (h "0krn9fzl9km1js0057f9s08xxzslinrxsw7zlfnm4wvcvx4bwhrl")))

(define-public crate-solana-measure-1.0.22 (c (n "solana-measure") (v "1.0.22") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-metrics") (r "^1.0.22") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.0.22") (d #t) (k 0)))) (h "0fa8ng609pvq1ipxi47cr0s6y1rhm1n7d50k8v8pivry3pxcqq0v")))

(define-public crate-solana-measure-1.1.11 (c (n "solana-measure") (v "1.1.11") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-metrics") (r "^1.1.11") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.1.11") (d #t) (k 0)))) (h "05c75g9bj6xjxcf0n0sz72n2ms7byy6jg4s5670lvfmaclz0dg35")))

(define-public crate-solana-measure-1.1.12 (c (n "solana-measure") (v "1.1.12") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-metrics") (r "^1.1.12") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.1.12") (d #t) (k 0)))) (h "14pa80pyh8nnn763bhjf4lpq16mgzxphdm851jsr48bli8mbzhn4")))

(define-public crate-solana-measure-1.1.13 (c (n "solana-measure") (v "1.1.13") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-metrics") (r "^1.1.13") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.1.13") (d #t) (k 0)))) (h "058kml2d2n9xr96d9kbws8lacgssvgkkfpa0vjdxddpyihbmkncq")))

(define-public crate-solana-measure-1.0.24 (c (n "solana-measure") (v "1.0.24") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-metrics") (r "^1.0.24") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.0.24") (d #t) (k 0)))) (h "0j4rag5y461yzd8vr2lgnvq67ffy88f7bq1vyqppzpw9g4d8f8bd")))

(define-public crate-solana-measure-1.2.0 (c (n "solana-measure") (v "1.2.0") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-metrics") (r "^1.2.0") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.2.0") (d #t) (k 0)))) (h "1ad92zsj1dhhaka2x07w0dkx7qikxm5mqc4k58i9x8l7vifk158j")))

(define-public crate-solana-measure-1.1.15 (c (n "solana-measure") (v "1.1.15") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-metrics") (r "^1.1.15") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.1.15") (d #t) (k 0)))) (h "1q1i38gw4z49fds6v629hi9a49z9h3636q4dip0vvbj5yfj3ld1l")))

(define-public crate-solana-measure-1.1.17 (c (n "solana-measure") (v "1.1.17") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-metrics") (r "^1.1.17") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.1.17") (d #t) (k 0)))) (h "0b4yvg29c0a3h7gwwn3ppmnbji4qhr7am7sas658mjcrh454hjfn")))

(define-public crate-solana-measure-1.2.1 (c (n "solana-measure") (v "1.2.1") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-metrics") (r "^1.2.1") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.2.1") (d #t) (k 0)))) (h "0c3ildqs0p54wc016fafcwhyd23j768q7myvgc45i81qqknwv6cc")))

(define-public crate-solana-measure-1.1.18 (c (n "solana-measure") (v "1.1.18") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-metrics") (r "^1.1.18") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.1.18") (d #t) (k 0)))) (h "10wv6ph8dp0f89h0w99ms2k218p6qlja9i7nvma8ib0riscwckhf")))

(define-public crate-solana-measure-1.2.3 (c (n "solana-measure") (v "1.2.3") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-metrics") (r "^1.2.3") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.2.3") (d #t) (k 0)))) (h "15l4m1zw6lig1gv4kmfslwggink968vz1qac9n8ynywsby4yf36d")))

(define-public crate-solana-measure-1.2.4 (c (n "solana-measure") (v "1.2.4") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-metrics") (r "^1.2.4") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.2.4") (d #t) (k 0)))) (h "19szh7lxnh9wn61wfqkk9dj3inxas6cajr0i5wbfizh29mdbm3mz")))

(define-public crate-solana-measure-1.1.19 (c (n "solana-measure") (v "1.1.19") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-metrics") (r "^1.1.19") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.1.19") (d #t) (k 0)))) (h "0hkavdccc4awlvrp2i58g2w2l329qgmlmfjg3h221944bz1dgiqb")))

(define-public crate-solana-measure-1.2.5 (c (n "solana-measure") (v "1.2.5") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-metrics") (r "^1.2.5") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.2.5") (d #t) (k 0)))) (h "01lsb36zshnpfly7hiwv7v9x8y5azz18s30fch0bij9mfkl7802k")))

(define-public crate-solana-measure-1.2.6 (c (n "solana-measure") (v "1.2.6") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-metrics") (r "^1.2.6") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.2.6") (d #t) (k 0)))) (h "1j1r32js1201mzmwdsbnmbd6dp9d4m2py494m4plwcvg2gkmqqqv")))

(define-public crate-solana-measure-1.2.7 (c (n "solana-measure") (v "1.2.7") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-metrics") (r "^1.2.7") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.2.7") (d #t) (k 0)))) (h "065p2d8plqs61pma8jjwydm2mnr4jy0nnzn5hc2rpw45sfzswh0m")))

(define-public crate-solana-measure-1.2.8 (c (n "solana-measure") (v "1.2.8") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-metrics") (r "^1.2.8") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.2.8") (d #t) (k 0)))) (h "06m9zss817fj3hky0nl861kphxcxzrd6l5x6m0ps21dbwf8nxkf9")))

(define-public crate-solana-measure-1.2.9 (c (n "solana-measure") (v "1.2.9") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-metrics") (r "^1.2.9") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.2.9") (d #t) (k 0)))) (h "1vamrc2012rayl0q1phi51ahglgkmif3ac0mm4skygps2nz067cd")))

(define-public crate-solana-measure-1.2.10 (c (n "solana-measure") (v "1.2.10") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-metrics") (r "^1.2.10") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.2.10") (d #t) (k 0)))) (h "1ksdyyl18vd1z0rp61i15d3vg3kq5gxvfsp8nrfhg731q31xziv7")))

(define-public crate-solana-measure-1.1.20 (c (n "solana-measure") (v "1.1.20") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-metrics") (r "^1.1.20") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.1.20") (d #t) (k 0)))) (h "0kqrfc61c3xn7yk6izv9qpvcmfapfgxzxajcwgrp9jx2qbhk2wbr")))

(define-public crate-solana-measure-1.2.12 (c (n "solana-measure") (v "1.2.12") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-metrics") (r "^1.2.12") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.2.12") (d #t) (k 0)))) (h "1b16ljq7dsmsnpr9k0shna11yrggpiqmxmpacgwqc2gkxp206phn")))

(define-public crate-solana-measure-1.2.13 (c (n "solana-measure") (v "1.2.13") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-metrics") (r "^1.2.13") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.2.13") (d #t) (k 0)))) (h "1rcvhfms3a4ja0qjckar1vn9mgjncgaqdkzhkbzsa867bm3zvdb2")))

(define-public crate-solana-measure-1.2.14 (c (n "solana-measure") (v "1.2.14") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-metrics") (r "^1.2.14") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.2.14") (d #t) (k 0)))) (h "1m23iw1nabza1gflqp6vc5hdcywiirkm0h9z8j1ddrvkw9n4gpyh")))

(define-public crate-solana-measure-1.1.23 (c (n "solana-measure") (v "1.1.23") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-metrics") (r "^1.1.23") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.1.23") (d #t) (k 0)))) (h "1xmdwivlsbmdcgd36j3igs4xgh3dyqicd60cjhc0kqm466cjgz8m")))

(define-public crate-solana-measure-1.2.15 (c (n "solana-measure") (v "1.2.15") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-metrics") (r "^1.2.15") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.2.15") (d #t) (k 0)))) (h "0g7zawzj77a52j342py6ghlv5jykgd8ny1pfxa2ihmijglfxj48w")))

(define-public crate-solana-measure-1.2.16 (c (n "solana-measure") (v "1.2.16") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-metrics") (r "^1.2.16") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.2.16") (d #t) (k 0)))) (h "15wv9l3rrv7lsa1m1jijjaqx3r12ckjbzkllash8pnqyfd3l3ay3")))

(define-public crate-solana-measure-1.2.17 (c (n "solana-measure") (v "1.2.17") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-metrics") (r "^1.2.17") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.2.17") (d #t) (k 0)))) (h "1hvqhs4ym7zrfbdmcy9nhxqqql3m2hkjlch1fgwnnxiqnp8g2697")))

(define-public crate-solana-measure-1.2.18 (c (n "solana-measure") (v "1.2.18") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-metrics") (r "^1.2.18") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.2.18") (d #t) (k 0)))) (h "031aw1wd2f3s7k5kwwx9w2sfzgwnznd5c6yh5sb4ry2plrvlizdy")))

(define-public crate-solana-measure-1.2.19 (c (n "solana-measure") (v "1.2.19") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-metrics") (r "^1.2.19") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.2.19") (d #t) (k 0)))) (h "1m6yxr3z6irc5a5j83yw2ha2r9bsvl8q283l9y28x745xhybibc4")))

(define-public crate-solana-measure-1.2.20 (c (n "solana-measure") (v "1.2.20") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-metrics") (r "^1.2.20") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.2.20") (d #t) (k 0)))) (h "0s0ygn85lpdhgds04a9446cihq9kks8fwghipac7y1pvbrcbcdfz")))

(define-public crate-solana-measure-1.3.0 (c (n "solana-measure") (v "1.3.0") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-metrics") (r "^1.3.0") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.3.0") (d #t) (k 0)))) (h "0m3xipvshrkqs7vyskfg4r7ffn9si5wppc4x2qjjc6h0qz6aqhwx")))

(define-public crate-solana-measure-1.3.1 (c (n "solana-measure") (v "1.3.1") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-metrics") (r "^1.3.1") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.3.1") (d #t) (k 0)))) (h "1mpc38nmbybmi4jr9a5gz03hngiasfxsga55zpbf5npffaq1rbbh")))

(define-public crate-solana-measure-1.2.21 (c (n "solana-measure") (v "1.2.21") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-metrics") (r "^1.2.21") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.2.21") (d #t) (k 0)))) (h "0yqmx7rff8wkd5ldv9kn2fznpvazl0dxmbwswy3xwhnxqdkr7gdg")))

(define-public crate-solana-measure-1.3.2 (c (n "solana-measure") (v "1.3.2") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-metrics") (r "^1.3.2") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.3.2") (d #t) (k 0)))) (h "1v6g5nfmngcjb4w58wf3mlc9wvyzb5ngb2g02gp86j433p7272a3")))

(define-public crate-solana-measure-1.2.22 (c (n "solana-measure") (v "1.2.22") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-metrics") (r "^1.2.22") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.2.22") (d #t) (k 0)))) (h "0s83dnry4x3s3hpn58ba4w2yg24vign2850lilslms0s76j9c7g3")))

(define-public crate-solana-measure-1.3.3 (c (n "solana-measure") (v "1.3.3") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-metrics") (r "^1.3.3") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.3.3") (d #t) (k 0)))) (h "1nxkgn4hkyia06317s7d93i269v76cs82n0akdj620irkn39z73i")))

(define-public crate-solana-measure-1.2.23 (c (n "solana-measure") (v "1.2.23") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-metrics") (r "^1.2.23") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.2.23") (d #t) (k 0)))) (h "11bp5g5yag9ksiqlc7ybipa0i0514jybc11lljr1gd1wpx0j298g")))

(define-public crate-solana-measure-1.2.24 (c (n "solana-measure") (v "1.2.24") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-metrics") (r "^1.2.24") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.2.24") (d #t) (k 0)))) (h "1f1idvvx75w41g2cldh79f6bmh6gdnwxmmc60n32qj4ynrfxqs72")))

(define-public crate-solana-measure-1.3.4 (c (n "solana-measure") (v "1.3.4") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-metrics") (r "^1.3.4") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.3.4") (d #t) (k 0)))) (h "0cpvpl5yw4p0vg55vsifcf1a4xp979idj7jzzw08ikbiipnq1yi0")))

(define-public crate-solana-measure-1.2.25 (c (n "solana-measure") (v "1.2.25") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-metrics") (r "^1.2.25") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.2.25") (d #t) (k 0)))) (h "0g98w9979r2z3xjygl63r09k6n0a2spp7fh91y6jc2f9hvkybykx")))

(define-public crate-solana-measure-1.2.26 (c (n "solana-measure") (v "1.2.26") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-metrics") (r "^1.2.26") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.2.26") (d #t) (k 0)))) (h "153i54ym6gdhrlvyjam6rcxb4saa3r8pzh6sk4m4c044f3653nsd")))

(define-public crate-solana-measure-1.3.5 (c (n "solana-measure") (v "1.3.5") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-metrics") (r "^1.3.5") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.3.5") (d #t) (k 0)))) (h "0rfjj2lkk5hjwim75aq7iwrnirg608b66amd2czzqiicgrra171k")))

(define-public crate-solana-measure-1.3.6 (c (n "solana-measure") (v "1.3.6") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-metrics") (r "^1.3.6") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.3.6") (d #t) (k 0)))) (h "0r4mjkf643xpf8lxp79ijaf39qrx969nh9pvz8llvyrmy7qzf95d")))

(define-public crate-solana-measure-1.3.7 (c (n "solana-measure") (v "1.3.7") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-metrics") (r "^1.3.7") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.3.7") (d #t) (k 0)))) (h "0n448izm9vwv6f59xzgb71sqj36vr4ln6fan38d0dpv7spkph2vi")))

(define-public crate-solana-measure-1.2.27 (c (n "solana-measure") (v "1.2.27") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-metrics") (r "^1.2.27") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.2.27") (d #t) (k 0)))) (h "06lakavaap026730kcpdwgzlq9bzcxkh6bwl2pk58zv5diqdh5sv")))

(define-public crate-solana-measure-1.3.8 (c (n "solana-measure") (v "1.3.8") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-metrics") (r "^1.3.8") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.3.8") (d #t) (k 0)))) (h "1z4v394j08rys7aka95bylwnws807117lax1gavyvwy3ppsq129h")))

(define-public crate-solana-measure-1.2.28 (c (n "solana-measure") (v "1.2.28") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-metrics") (r "^1.2.28") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.2.28") (d #t) (k 0)))) (h "18jp4w1q5lsy90d3l8q1556hcyf6rkgyz31smqdjlw97dv463g2y")))

(define-public crate-solana-measure-1.3.9 (c (n "solana-measure") (v "1.3.9") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-metrics") (r "^1.3.9") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.3.9") (d #t) (k 0)))) (h "0g5ni388s9b7bhgy0c5ami1hazrcwip8qbhkrbkzykhay2rj1knk")))

(define-public crate-solana-measure-1.3.10 (c (n "solana-measure") (v "1.3.10") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-metrics") (r "^1.3.10") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.3.10") (d #t) (k 0)))) (h "0nkzy46fy098fclkyf0p0wsm77bacmdf4dqxphxgh7yfq2wyngas")))

(define-public crate-solana-measure-1.3.11 (c (n "solana-measure") (v "1.3.11") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-metrics") (r "^1.3.11") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.3.11") (d #t) (k 0)))) (h "09r2gz7qp5wyfb48yhmk5lpz0zrgwkz3wi5jdfzrygmpjrw9rqbj")))

(define-public crate-solana-measure-1.2.29 (c (n "solana-measure") (v "1.2.29") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-metrics") (r "^1.2.29") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.2.29") (d #t) (k 0)))) (h "1263rflz7535miy95s104l7yzg9gfbvm7ks27j2zzwb0sxpxibxp")))

(define-public crate-solana-measure-1.3.12 (c (n "solana-measure") (v "1.3.12") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-metrics") (r "^1.3.12") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.3.12") (d #t) (k 0)))) (h "0myccfjxagrdn7s1rg982fz8b1z0pfzvnslxbhgbf1wjamr20gk0")))

(define-public crate-solana-measure-1.3.13 (c (n "solana-measure") (v "1.3.13") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-metrics") (r "^1.3.13") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.3.13") (d #t) (k 0)))) (h "1s1m2pb5vdg9bjjdaiyw56rh57iwwkv2icddcrlhlbzvd7z9yj3m")))

(define-public crate-solana-measure-1.2.30 (c (n "solana-measure") (v "1.2.30") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-metrics") (r "^1.2.30") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.2.30") (d #t) (k 0)))) (h "09q6x9k94fnr819j9ywrlyiah27ryamh66zfig4is2c3k3w15dil")))

(define-public crate-solana-measure-1.3.14 (c (n "solana-measure") (v "1.3.14") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-metrics") (r "^1.3.14") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.3.14") (d #t) (k 0)))) (h "0csmvj66ca3svlxxrlx16l32xryp5f2vcix4mlhckjlmk8gng7ih")))

(define-public crate-solana-measure-1.2.31 (c (n "solana-measure") (v "1.2.31") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-metrics") (r "^1.2.31") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.2.31") (d #t) (k 0)))) (h "0m09w8siblgbx7yb0vxs0g9w855w8m03vb0n8qvqf8hyxsrng10m")))

(define-public crate-solana-measure-1.2.32 (c (n "solana-measure") (v "1.2.32") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-metrics") (r "^1.2.32") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.2.32") (d #t) (k 0)))) (h "1vcc9pdqi8ykzpcjmj0p0aam4jsxwr29lsvhplqflgvnfifn9ypi")))

(define-public crate-solana-measure-1.3.17 (c (n "solana-measure") (v "1.3.17") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-metrics") (r "^1.3.17") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.3.17") (d #t) (k 0)))) (h "01rdcpiwj1w8fg1zayjbrn8lcly8h6xkzc5rwbjp606jvl11jrmj")))

(define-public crate-solana-measure-1.4.0 (c (n "solana-measure") (v "1.4.0") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-metrics") (r "^1.4.0") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.4.0") (d #t) (k 0)))) (h "1f1h2xamwsq4yxyhi8i8q69ninbvvwczqzrlp72ilsx9q7as3gzr")))

(define-public crate-solana-measure-1.4.1 (c (n "solana-measure") (v "1.4.1") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-metrics") (r "^1.4.1") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.4.1") (d #t) (k 0)))) (h "10sqpdqjxg7558rflhh21vpa1q7dpmqqkzni30mwbg740zrslccd")))

(define-public crate-solana-measure-1.3.18 (c (n "solana-measure") (v "1.3.18") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-metrics") (r "^1.3.18") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.3.18") (d #t) (k 0)))) (h "1i88nmmz8nk13j03hczxvrdvl9akzvj523aqb2a7fvv5jyvws6ki")))

(define-public crate-solana-measure-1.3.19 (c (n "solana-measure") (v "1.3.19") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-metrics") (r "^1.3.19") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.3.19") (d #t) (k 0)))) (h "0a99v860w016n4323xqgf14b20rrya5jql1zmig4y83ip12g0zdp")))

(define-public crate-solana-measure-1.4.3 (c (n "solana-measure") (v "1.4.3") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-metrics") (r "^1.4.3") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.4.3") (d #t) (k 0)))) (h "1x42z6zhwx757mgpkz5kr3bwlbfldx99k4flhq4n2lxvi3ncbb2v")))

(define-public crate-solana-measure-1.4.4 (c (n "solana-measure") (v "1.4.4") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-metrics") (r "^1.4.4") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.4.4") (d #t) (k 0)))) (h "0g0ivl3mzzzf1jdg7ci3qfwnzjiaqk0s587vl1k1ia2yrqdybvz1")))

(define-public crate-solana-measure-1.4.5 (c (n "solana-measure") (v "1.4.5") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-metrics") (r "^1.4.5") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.4.5") (d #t) (k 0)))) (h "1fk0ymly0amzkj00b4kcycgazrzq8wzkgydwz3b635a4zj7nk2fi")))

(define-public crate-solana-measure-1.4.6 (c (n "solana-measure") (v "1.4.6") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-metrics") (r "^1.4.6") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.4.6") (d #t) (k 0)))) (h "0mdyhn3cpw2kxhb23rjyjpqd0fjbrzwv87hfqacxy8375zkyvfm3")))

(define-public crate-solana-measure-1.3.20 (c (n "solana-measure") (v "1.3.20") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-metrics") (r "^1.3.20") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.3.20") (d #t) (k 0)))) (h "0rxrll6jyn39r2d4jv6milgqszqdbpfnzi52wcbhfw5lpav26ij0")))

(define-public crate-solana-measure-1.4.7 (c (n "solana-measure") (v "1.4.7") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-metrics") (r "^1.4.7") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.4.7") (d #t) (k 0)))) (h "0wrfmq9sh8jlqnzqdyldhjv9yc112i4ng6dac5v921kprb6gc8xw")))

(define-public crate-solana-measure-1.3.21 (c (n "solana-measure") (v "1.3.21") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-metrics") (r "^1.3.21") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.3.21") (d #t) (k 0)))) (h "1ij7n3nzj2z9aq3r2ccr2xv7ssmlywvxh763izbyvz3acq83v559")))

(define-public crate-solana-measure-1.4.8 (c (n "solana-measure") (v "1.4.8") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-metrics") (r "^1.4.8") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.4.8") (d #t) (k 0)))) (h "0kvgwbscbhxxbf9xgm05sn9g35816f3lx86rpm8v0iwpmjrrr0s8")))

(define-public crate-solana-measure-1.4.9 (c (n "solana-measure") (v "1.4.9") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-metrics") (r "^1.4.9") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.4.9") (d #t) (k 0)))) (h "0yzz9yiampqw30gl3ikb667adjw5cllzz92ymjq5zj4yr702q2qr")))

(define-public crate-solana-measure-1.4.10 (c (n "solana-measure") (v "1.4.10") (d (list (d (n "jemalloc-ctl") (r ">=0.3.2, <0.4.0") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r ">=0.3.2, <0.4.0") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r ">=0.4.8, <0.5.0") (d #t) (k 0)) (d (n "solana-metrics") (r ">=1.4.10, <2.0.0") (d #t) (k 0)) (d (n "solana-sdk") (r ">=1.4.10, <2.0.0") (d #t) (k 0)))) (h "1ddv7cpg49gbxknf8zc88kcqnzsq9i8cfdnxrq3q9i37sn2wqivx")))

(define-public crate-solana-measure-1.4.11 (c (n "solana-measure") (v "1.4.11") (d (list (d (n "jemalloc-ctl") (r ">=0.3.2, <0.4.0") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r ">=0.3.2, <0.4.0") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r ">=0.4.8, <0.5.0") (d #t) (k 0)) (d (n "solana-metrics") (r ">=1.4.11, <2.0.0") (d #t) (k 0)) (d (n "solana-sdk") (r ">=1.4.11, <2.0.0") (d #t) (k 0)))) (h "1n2q5igifnszh3v37hriqxvc1abv8vbb96ly35p9z6irizv5kg1n")))

(define-public crate-solana-measure-1.4.12 (c (n "solana-measure") (v "1.4.12") (d (list (d (n "jemalloc-ctl") (r ">=0.3.2, <0.4.0") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r ">=0.3.2, <0.4.0") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r ">=0.4.8, <0.5.0") (d #t) (k 0)) (d (n "solana-metrics") (r ">=1.4.12, <2.0.0") (d #t) (k 0)) (d (n "solana-sdk") (r ">=1.4.12, <2.0.0") (d #t) (k 0)))) (h "1m3m9jcncz4n4dwiinrc3n78qbzabpplal6mlsvg4mqlnr7cvv0z")))

(define-public crate-solana-measure-1.3.23 (c (n "solana-measure") (v "1.3.23") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-metrics") (r "^1.3.23") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.3.23") (d #t) (k 0)))) (h "1g41nlfs2v4a4j64z0ck4z1wp1z7m6kzgmbl7hj33sjx747xd2ha")))

(define-public crate-solana-measure-1.4.13 (c (n "solana-measure") (v "1.4.13") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-metrics") (r "^1.4.13") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.4.13") (d #t) (k 0)))) (h "00vdgjx3r29dvaal973pj9ia6520jwsvzkirxzdabzxmg3q9c55n")))

(define-public crate-solana-measure-1.4.14 (c (n "solana-measure") (v "1.4.14") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-metrics") (r "^1.4.14") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.4.14") (d #t) (k 0)))) (h "129d551lnxs450di1b1yzlyhlfs0fblm4jnzf61lkxij57lrghbn")))

(define-public crate-solana-measure-1.4.15 (c (n "solana-measure") (v "1.4.15") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-metrics") (r "^1.4.15") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.4.15") (d #t) (k 0)))) (h "0r9kar1jl47ihynh9bh75fnmkknsskly4c6h4k9d58ivbkm1nb4f")))

(define-public crate-solana-measure-1.4.16 (c (n "solana-measure") (v "1.4.16") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-metrics") (r "^1.4.16") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.4.16") (d #t) (k 0)))) (h "1z4hy105cly0147qgf0x6j2plnl2a25gb2vdps8sdis9wl6bv27h")))

(define-public crate-solana-measure-1.4.17 (c (n "solana-measure") (v "1.4.17") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-metrics") (r "^1.4.17") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.4.17") (d #t) (k 0)))) (h "1gzrilpprhwnan1h224vmsbg0rkab00halphanvk96l7p9jzgj4d")))

(define-public crate-solana-measure-1.5.0 (c (n "solana-measure") (v "1.5.0") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-metrics") (r "^1.5.0") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.5.0") (d #t) (k 0)))) (h "0ljwmr3bd1zm75qi2sn5y3dsbfns5dn8wp5rsr2kyh2hpw9qf8ir")))

(define-public crate-solana-measure-1.4.18 (c (n "solana-measure") (v "1.4.18") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-metrics") (r "^1.4.18") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.4.18") (d #t) (k 0)))) (h "0f2j9l428wx67qizyzxj77qp03lywg7d18lcpl4lywbjw46wyp2d")))

(define-public crate-solana-measure-1.4.19 (c (n "solana-measure") (v "1.4.19") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-metrics") (r "^1.4.19") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.4.19") (d #t) (k 0)))) (h "14lar1id17lmcrmrfpbyaqj6qdriifp20bczcbmhxvysqg0ywxl9")))

(define-public crate-solana-measure-1.4.20 (c (n "solana-measure") (v "1.4.20") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-metrics") (r "^1.4.20") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.4.20") (d #t) (k 0)))) (h "10ywx4h9hi4wydkbmm15l54gmn25y049mwzdlsl870qjivaxp2sk")))

(define-public crate-solana-measure-1.5.1 (c (n "solana-measure") (v "1.5.1") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-metrics") (r "^1.5.1") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.5.1") (d #t) (k 0)))) (h "07i5lhxqsyv0gmsl85nrafd6yymkcgzcy6wb4vk3b3yhzzbf5x6f")))

(define-public crate-solana-measure-1.4.21 (c (n "solana-measure") (v "1.4.21") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-metrics") (r "^1.4.21") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.4.21") (d #t) (k 0)))) (h "11vzgipv57fa1396y8nidmivmk0nxgbxp0w2jqr7si10spdjgxsb")))

(define-public crate-solana-measure-1.4.22 (c (n "solana-measure") (v "1.4.22") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-metrics") (r "^1.4.22") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.4.22") (d #t) (k 0)))) (h "1dhzjqvk4gh9bq5gmm6n1x9snkypns4pi7hr48pwdiyf7hmy4vh1")))

(define-public crate-solana-measure-1.5.2 (c (n "solana-measure") (v "1.5.2") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-metrics") (r "^1.5.2") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.5.2") (d #t) (k 0)))) (h "1pai0p653d2jn6rvx1zqikzjz89fqigzpaymdi60mlfzjbn0mvkl")))

(define-public crate-solana-measure-1.4.23 (c (n "solana-measure") (v "1.4.23") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-metrics") (r "^1.4.23") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.4.23") (d #t) (k 0)))) (h "08c08c5dr442409fmj0cxvriwj8hcgiam0kr0s38lgqdfq4scb0r")))

(define-public crate-solana-measure-1.5.3 (c (n "solana-measure") (v "1.5.3") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-metrics") (r "^1.5.3") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.5.3") (d #t) (k 0)))) (h "19qb87x6adrp0pr8zfn1hajn671s6g3msw9ppnjl65yvli3ag2dg")))

(define-public crate-solana-measure-1.5.4 (c (n "solana-measure") (v "1.5.4") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-metrics") (r "^1.5.4") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.5.4") (d #t) (k 0)))) (h "0shl8hvl920g5nigd777za0fw2mmy390lrfw6g38kqsb21zcxivw")))

(define-public crate-solana-measure-1.5.5 (c (n "solana-measure") (v "1.5.5") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-metrics") (r "^1.5.5") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.5.5") (d #t) (k 0)))) (h "0rhjmjbqdd78gzba5s8yqmxbb1vfzqm9c2hnd7smjhsqr7qzkq8r") (f (quote (("no-jemalloc"))))))

(define-public crate-solana-measure-1.4.25 (c (n "solana-measure") (v "1.4.25") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-metrics") (r "^1.4.25") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.4.25") (d #t) (k 0)))) (h "1wjrvl8wsq0d2k6knj3r4r86warh6f0xazx31kn440lmg473z7i0") (f (quote (("no-jemalloc"))))))

(define-public crate-solana-measure-1.4.26 (c (n "solana-measure") (v "1.4.26") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-metrics") (r "^1.4.26") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.4.26") (d #t) (k 0)))) (h "1n27b8vyfh9sxfrpxgd5a71awjhqxnkdbjy2r9djl95h1w3q96wn") (f (quote (("no-jemalloc"))))))

(define-public crate-solana-measure-1.5.6 (c (n "solana-measure") (v "1.5.6") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-metrics") (r "^1.5.6") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.5.6") (d #t) (k 0)))) (h "14lkvnsm4gz2c085n16xw5mkpsx41wyjzbz336vckirj11ryn868") (f (quote (("no-jemalloc"))))))

(define-public crate-solana-measure-1.4.27 (c (n "solana-measure") (v "1.4.27") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-metrics") (r "^1.4.27") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.4.27") (d #t) (k 0)))) (h "1s5dvx0pw4jqjms2icjvcazkdzvlb56bxqjwixj2bfcqr13gjhgh") (f (quote (("no-jemalloc"))))))

(define-public crate-solana-measure-1.5.7 (c (n "solana-measure") (v "1.5.7") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-metrics") (r "^1.5.7") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.5.7") (d #t) (k 0)))) (h "0n77fnnc6j0fazix6jnwjgg34392spr2ksyz2n617hkpiqq7307l") (f (quote (("no-jemalloc"))))))

(define-public crate-solana-measure-1.4.28 (c (n "solana-measure") (v "1.4.28") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-metrics") (r "^1.4.28") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.4.28") (d #t) (k 0)))) (h "1ifnpjw73zbq4ws97kaaxaq5xsflsypz0k678qz5vbvc6f1kc0hb") (f (quote (("no-jemalloc"))))))

(define-public crate-solana-measure-1.5.8 (c (n "solana-measure") (v "1.5.8") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-metrics") (r "^1.5.8") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.5.8") (d #t) (k 0)))) (h "099w1wxgbzqpl12mz352ccyxdf847bj4ihsgl5f2x808j7fbljc9") (f (quote (("no-jemalloc"))))))

(define-public crate-solana-measure-1.5.9 (c (n "solana-measure") (v "1.5.9") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-metrics") (r "^1.5.9") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.5.9") (d #t) (k 0)))) (h "05x4bv853niwznk8iyy7ifbc5675ycxnhcgmhzx41kdklx19kg1p") (f (quote (("no-jemalloc")))) (y #t)))

(define-public crate-solana-measure-1.5.10 (c (n "solana-measure") (v "1.5.10") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-metrics") (r "^1.5.10") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.5.10") (d #t) (k 0)))) (h "1y8m9rh2p3503xhxypkjvmfxjm3cic3xbiw468qrwzpdllfx7pnq") (f (quote (("no-jemalloc"))))))

(define-public crate-solana-measure-1.5.11 (c (n "solana-measure") (v "1.5.11") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-metrics") (r "^1.5.11") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.5.11") (d #t) (k 0)))) (h "1mqgx33yhxddxcw37j8jfiphwy5281pmziy7cv2pr5v04ysbvvmr") (f (quote (("no-jemalloc"))))))

(define-public crate-solana-measure-1.5.12 (c (n "solana-measure") (v "1.5.12") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-metrics") (r "^1.5.12") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.5.12") (d #t) (k 0)))) (h "105p61b664aqcflcrkwfrsikhjlmha01b39qnf9vp3j6sqmvvz0d") (f (quote (("no-jemalloc"))))))

(define-public crate-solana-measure-1.5.13 (c (n "solana-measure") (v "1.5.13") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-metrics") (r "^1.5.13") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.5.13") (d #t) (k 0)))) (h "1vr0n414yp8hv6b0knfnb9cifn3qq7r2i569jbr7zd97q7rh12ip") (f (quote (("no-jemalloc"))))))

(define-public crate-solana-measure-1.5.14 (c (n "solana-measure") (v "1.5.14") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-metrics") (r "^1.5.14") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.5.14") (d #t) (k 0)))) (h "00mc9dwdblpr3i7wrirx0z0g239qlpry7n3w3rkw4vi59v6gk25y") (f (quote (("no-jemalloc"))))))

(define-public crate-solana-measure-1.6.0 (c (n "solana-measure") (v "1.6.0") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-metrics") (r "^1.6.0") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.6.0") (d #t) (k 0)))) (h "1kpgb4hs4q2shhq1n9j4bnh690lc1crcgckphghljp7b2k8m634j") (f (quote (("no-jemalloc")))) (y #t)))

(define-public crate-solana-measure-1.5.15 (c (n "solana-measure") (v "1.5.15") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-metrics") (r "=1.5.15") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.5.15") (d #t) (k 0)))) (h "1b1vh7lr11vx8xxkz94kvwd1jqi3r253ls3i5h5pf1647fw8vl3k") (f (quote (("no-jemalloc"))))))

(define-public crate-solana-measure-1.6.1 (c (n "solana-measure") (v "1.6.1") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-metrics") (r "=1.6.1") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.6.1") (d #t) (k 0)))) (h "11s2y905flm33jwvzqgaac0p9idrsc6gvs5rf1vl3qxsqn9pr0z7") (f (quote (("no-jemalloc"))))))

(define-public crate-solana-measure-1.5.16 (c (n "solana-measure") (v "1.5.16") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-metrics") (r "=1.5.16") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.5.16") (d #t) (k 0)))) (h "1nckzwrysm3z191qfwgg4dkg16mx7s3jg3p0niwhkla2pnkylxi0") (f (quote (("no-jemalloc"))))))

(define-public crate-solana-measure-1.5.17 (c (n "solana-measure") (v "1.5.17") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-metrics") (r "=1.5.17") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.5.17") (d #t) (k 0)))) (h "00y54hif2qsa14lxbmz4x8lzig5dn496szya4kfv0yp4x098vznv") (f (quote (("no-jemalloc"))))))

(define-public crate-solana-measure-1.6.2 (c (n "solana-measure") (v "1.6.2") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-metrics") (r "=1.6.2") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.6.2") (d #t) (k 0)))) (h "021b470mlmfiv138zk1nbzn6jrll2sd97fm3xhbk7x1xxjym4fqs") (f (quote (("no-jemalloc"))))))

(define-public crate-solana-measure-1.6.3 (c (n "solana-measure") (v "1.6.3") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-metrics") (r "=1.6.3") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.6.3") (d #t) (k 0)))) (h "0xph4q9v1wm374kcqj655qf2klh50w3sj0mk35b5p7dh4vjxaszf") (f (quote (("no-jemalloc"))))))

(define-public crate-solana-measure-1.6.4 (c (n "solana-measure") (v "1.6.4") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-metrics") (r "=1.6.4") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.6.4") (d #t) (k 0)))) (h "1fbn87wj1iyjmcg475fc6z2ncgjh3ni7gda15jm386fza83q5c6y") (f (quote (("no-jemalloc"))))))

(define-public crate-solana-measure-1.6.5 (c (n "solana-measure") (v "1.6.5") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-metrics") (r "=1.6.5") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.6.5") (d #t) (k 0)))) (h "0ba001k333ihm9hk72cjrp4ldpscr1i455ryl1vpj2fnh5i0d6lf") (f (quote (("no-jemalloc"))))))

(define-public crate-solana-measure-1.6.6 (c (n "solana-measure") (v "1.6.6") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-metrics") (r "=1.6.6") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.6.6") (d #t) (k 0)))) (h "1h096hq4z6q5q3sjyygwgb1arxq8vj5xhmjwpps1j4q617prz54a") (f (quote (("no-jemalloc"))))))

(define-public crate-solana-measure-1.5.19 (c (n "solana-measure") (v "1.5.19") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-metrics") (r "=1.5.19") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.5.19") (d #t) (k 0)))) (h "02170b5ghchbd7c6j5ck3d694wpnjsh74hbiidgkd09sbgks6c78") (f (quote (("no-jemalloc"))))))

(define-public crate-solana-measure-1.6.7 (c (n "solana-measure") (v "1.6.7") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-metrics") (r "=1.6.7") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.6.7") (d #t) (k 0)))) (h "016vkwggqdz8d6lxh2cxx49rqq2jx1kyjzfh100xjkqxjy74537f") (f (quote (("no-jemalloc"))))))

(define-public crate-solana-measure-1.6.8 (c (n "solana-measure") (v "1.6.8") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-metrics") (r "=1.6.8") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.6.8") (d #t) (k 0)))) (h "09b28m12g1bva5f5p9agb8fbmqb6mqjd49z7krd23sw5n3il8gcw") (f (quote (("no-jemalloc"))))))

(define-public crate-solana-measure-1.6.9 (c (n "solana-measure") (v "1.6.9") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-metrics") (r "=1.6.9") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.6.9") (d #t) (k 0)))) (h "0rsc56ik4b3rc7520zl3xjqkx5rz3k7ibryp87qkrd40czy54g5h") (f (quote (("no-jemalloc"))))))

(define-public crate-solana-measure-1.6.10 (c (n "solana-measure") (v "1.6.10") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-metrics") (r "=1.6.10") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.6.10") (d #t) (k 0)))) (h "1s3ldj24qi1nn9ikczpf3d8aw05c383w21ljf7f6s6c7vvjr30bh") (f (quote (("no-jemalloc"))))))

(define-public crate-solana-measure-1.6.11 (c (n "solana-measure") (v "1.6.11") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-metrics") (r "=1.6.11") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.6.11") (d #t) (k 0)))) (h "1qhcn6dihw6c1v4jl287m3b7x8jj1888ghzfphiwgss73z725prd") (f (quote (("no-jemalloc"))))))

(define-public crate-solana-measure-1.7.0 (c (n "solana-measure") (v "1.7.0") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-metrics") (r "=1.7.0") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.7.0") (d #t) (k 0)))) (h "1d4npyy9rcvak7ia7w2g9bxsmpknasx9l240rciiqxj8zgr0pbfm")))

(define-public crate-solana-measure-1.7.1 (c (n "solana-measure") (v "1.7.1") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-metrics") (r "=1.7.1") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.7.1") (d #t) (k 0)))) (h "0ryww71nljlj33yrs0fscq8m470qcjfpybfnld2nhdz1b8j5q902")))

(define-public crate-solana-measure-1.6.12 (c (n "solana-measure") (v "1.6.12") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-metrics") (r "=1.6.12") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.6.12") (d #t) (k 0)))) (h "190madp67azdw19ziy4cyjv3wn5zx0jzffzn1664dh634jsfcd90") (f (quote (("no-jemalloc"))))))

(define-public crate-solana-measure-1.6.13 (c (n "solana-measure") (v "1.6.13") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-metrics") (r "=1.6.13") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.6.13") (d #t) (k 0)))) (h "1w0g43k30k86gpsmkzyvnxf39zhny1ziyx3q98ssfanhhr62ffa9") (f (quote (("no-jemalloc"))))))

(define-public crate-solana-measure-1.7.2 (c (n "solana-measure") (v "1.7.2") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-metrics") (r "=1.7.2") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.7.2") (d #t) (k 0)))) (h "12hzkfnv1q3aa0dsx6by0g8wbqnjnk8igi55c943zkc7jsc1vzaa")))

(define-public crate-solana-measure-1.6.14 (c (n "solana-measure") (v "1.6.14") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-metrics") (r "=1.6.14") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.6.14") (d #t) (k 0)))) (h "1na4lhkl01m1kkxhvlc9nwccfrvr5r5nd8skfq0cx4rwczpmlzs6") (f (quote (("no-jemalloc"))))))

(define-public crate-solana-measure-1.7.3 (c (n "solana-measure") (v "1.7.3") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-metrics") (r "=1.7.3") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.7.3") (d #t) (k 0)))) (h "114x5d275xdc6h1wx502nypjs2d165f9qklaqxqrqv94fymqv2wm")))

(define-public crate-solana-measure-1.6.15 (c (n "solana-measure") (v "1.6.15") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-metrics") (r "=1.6.15") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.6.15") (d #t) (k 0)))) (h "1p63q7bzk9ywykk6qfsqw5m1dvln2q976xcfmnpkjl6v08awkfxh") (f (quote (("no-jemalloc"))))))

(define-public crate-solana-measure-1.7.4 (c (n "solana-measure") (v "1.7.4") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-metrics") (r "=1.7.4") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.7.4") (d #t) (k 0)))) (h "1d97slgsv9akvvxykamsmm1njkvxhzl92fpikkazbchaih0j8g4p")))

(define-public crate-solana-measure-1.6.16 (c (n "solana-measure") (v "1.6.16") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-metrics") (r "=1.6.16") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.6.16") (d #t) (k 0)))) (h "0q9igb6jglx7cnqn36r72fmqg06qb4mxlfl1a9xvgrp1wv0z02wh") (f (quote (("no-jemalloc"))))))

(define-public crate-solana-measure-1.6.17 (c (n "solana-measure") (v "1.6.17") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-metrics") (r "=1.6.17") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.6.17") (d #t) (k 0)))) (h "1gap17536sm60r1p3qmkmyhmrxb1a6c507hyjfhln0ixqqx1y02c") (f (quote (("no-jemalloc"))))))

(define-public crate-solana-measure-1.7.5 (c (n "solana-measure") (v "1.7.5") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-metrics") (r "=1.7.5") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.7.5") (d #t) (k 0)))) (h "1yar6h2qfaiah8cmmyb20h9p7hbxl6xmmw43fj8mss3y0imq8b95")))

(define-public crate-solana-measure-1.7.6 (c (n "solana-measure") (v "1.7.6") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-metrics") (r "=1.7.6") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.7.6") (d #t) (k 0)))) (h "0qbd9vv3pn6xjvdgl5x7knpb95cd7cfy6svfcbvbpn8k4w93kqx6")))

(define-public crate-solana-measure-1.6.18 (c (n "solana-measure") (v "1.6.18") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-metrics") (r "=1.6.18") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.6.18") (d #t) (k 0)))) (h "1lkrfxkblv6k8wn97qfjbgzqrk0l1hms275lp1ig4gkmwz06f0gn") (f (quote (("no-jemalloc"))))))

(define-public crate-solana-measure-1.6.19 (c (n "solana-measure") (v "1.6.19") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-metrics") (r "=1.6.19") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.6.19") (d #t) (k 0)))) (h "0szpwlrrwp5kb2vyaivlbdibz8yb5pkvzhw6qvw66wr831sagc53") (f (quote (("no-jemalloc"))))))

(define-public crate-solana-measure-1.7.7 (c (n "solana-measure") (v "1.7.7") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-metrics") (r "=1.7.7") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.7.7") (d #t) (k 0)))) (h "0bm1swvk89srwifwcacf2pwvj12ql2v5n2c9zx01f97q8aqwgswq")))

(define-public crate-solana-measure-1.7.8 (c (n "solana-measure") (v "1.7.8") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-metrics") (r "=1.7.8") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.7.8") (d #t) (k 0)))) (h "1930yf5swrm9skc73lcbfl9myf49n5665a7zvqsqp51m1z91aafv")))

(define-public crate-solana-measure-1.6.20 (c (n "solana-measure") (v "1.6.20") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-metrics") (r "=1.6.20") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.6.20") (d #t) (k 0)))) (h "1nkn56qcd8cqkfdffzy7dgxn96mc1x5gjhiyfikn3n2awamrhxjj") (f (quote (("no-jemalloc"))))))

(define-public crate-solana-measure-1.7.9 (c (n "solana-measure") (v "1.7.9") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-metrics") (r "=1.7.9") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.7.9") (d #t) (k 0)))) (h "1004kznnr0kbb0mxdplvmr4ls377qn4c6z9yashwyvwl5j677m1h")))

(define-public crate-solana-measure-1.7.10 (c (n "solana-measure") (v "1.7.10") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-metrics") (r "=1.7.10") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.7.10") (d #t) (k 0)))) (h "1477fgvvirhr6ngah8snqa978qpsd83qvvm6lyp0snyd5rlha10v")))

(define-public crate-solana-measure-1.6.21 (c (n "solana-measure") (v "1.6.21") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-metrics") (r "=1.6.21") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.6.21") (d #t) (k 0)))) (h "1ch89vnkiz94bsg08nw9gavkjiq788y79vqx1l0p2f37mnmmxkz9") (f (quote (("no-jemalloc"))))))

(define-public crate-solana-measure-1.7.11 (c (n "solana-measure") (v "1.7.11") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-metrics") (r "=1.7.11") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.7.11") (d #t) (k 0)))) (h "116j49b8kjqncswjw434szjdd9jlm7k0c5hm5srzvclgbb5j9axm")))

(define-public crate-solana-measure-1.6.22 (c (n "solana-measure") (v "1.6.22") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-metrics") (r "=1.6.22") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.6.22") (d #t) (k 0)))) (h "163gqlvwsgqmch3plcr3467c5ypacxa8gvs8w703g6j8gcv73h7f") (f (quote (("no-jemalloc"))))))

(define-public crate-solana-measure-1.6.24 (c (n "solana-measure") (v "1.6.24") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-metrics") (r "=1.6.24") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.6.24") (d #t) (k 0)))) (h "15w0x0i85gsgn5k2ad47c0d4jhbrb1yfkzcsqk4dp5i06jlfasc5") (f (quote (("no-jemalloc"))))))

(define-public crate-solana-measure-1.6.25 (c (n "solana-measure") (v "1.6.25") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-metrics") (r "=1.6.25") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.6.25") (d #t) (k 0)))) (h "08836sdbzjxpbl737yvw0m9w3qr2z37a0fjgh2rwihwm24mzqb0z") (f (quote (("no-jemalloc"))))))

(define-public crate-solana-measure-1.7.12 (c (n "solana-measure") (v "1.7.12") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-metrics") (r "=1.7.12") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.7.12") (d #t) (k 0)))) (h "1izjiis5s9xv0sbw1fh2sm3rp8493x9jngdmbrpf65r6d6y58zgr")))

(define-public crate-solana-measure-1.6.26 (c (n "solana-measure") (v "1.6.26") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-metrics") (r "=1.6.26") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.6.26") (d #t) (k 0)))) (h "0v9d9rnia081dps64zmsdfy92q2rf14z78hyj5bp4wgb5mq5kmd2") (f (quote (("no-jemalloc"))))))

(define-public crate-solana-measure-1.6.27 (c (n "solana-measure") (v "1.6.27") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-metrics") (r "=1.6.27") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.6.27") (d #t) (k 0)))) (h "0g8bnhhyayrz3wmsxywbjkdrg1v5sph6grak51p61lm9bcr5a2wl") (f (quote (("no-jemalloc"))))))

(define-public crate-solana-measure-1.7.13 (c (n "solana-measure") (v "1.7.13") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-metrics") (r "=1.7.13") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.7.13") (d #t) (k 0)))) (h "07g59gizx8g4a4dl8lgvxc0nym6y65j9idxbppjikkhvm8vlcpgv")))

(define-public crate-solana-measure-1.7.14 (c (n "solana-measure") (v "1.7.14") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-metrics") (r "=1.7.14") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.7.14") (d #t) (k 0)))) (h "14k540f3gvb4nf62xfapv4mig85pr0gsavhr4khqkm0szs93y1x2")))

(define-public crate-solana-measure-1.8.0 (c (n "solana-measure") (v "1.8.0") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-metrics") (r "=1.8.0") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.8.0") (d #t) (k 0)))) (h "19is393p4pzx2svaj225wjrxcxfi468bsbanaialawnb3f68dhrw")))

(define-public crate-solana-measure-1.6.28 (c (n "solana-measure") (v "1.6.28") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-metrics") (r "=1.6.28") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.6.28") (d #t) (k 0)))) (h "0242y8fx72jx5wnyamzvq778mwg7gz09zhqnyh2jln6cjvkq1n8h") (f (quote (("no-jemalloc"))))))

(define-public crate-solana-measure-1.7.15 (c (n "solana-measure") (v "1.7.15") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-metrics") (r "=1.7.15") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.7.15") (d #t) (k 0)))) (h "0c9l11m4mxn8z8hrxxvzw3psv67icfvgnj5zpc2z20drvzvxnwyv")))

(define-public crate-solana-measure-1.8.1 (c (n "solana-measure") (v "1.8.1") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-metrics") (r "=1.8.1") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.8.1") (d #t) (k 0)))) (h "0jv31y46nq9gcixypb7ayr4i0zcbwyf28q6ff2njv0q7lll3wsx5")))

(define-public crate-solana-measure-1.7.16 (c (n "solana-measure") (v "1.7.16") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-metrics") (r "=1.7.16") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.7.16") (d #t) (k 0)))) (h "02xm7w54a3ci633mh54h8s25wiasmmss21iazjdz1jvjlwhywgpz")))

(define-public crate-solana-measure-1.7.17 (c (n "solana-measure") (v "1.7.17") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-metrics") (r "=1.7.17") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.7.17") (d #t) (k 0)))) (h "003q4km00qpk0dbndvdgqz0fcvj5vm6y6ppizprry1nwhngv5ldi")))

(define-public crate-solana-measure-1.8.2 (c (n "solana-measure") (v "1.8.2") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-metrics") (r "=1.8.2") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.8.2") (d #t) (k 0)))) (h "125j6xr8i67i6j0fp9wvj126ldzyx6ps79phsxhb52i07mq04rgq")))

(define-public crate-solana-measure-1.8.3 (c (n "solana-measure") (v "1.8.3") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-metrics") (r "=1.8.3") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.8.3") (d #t) (k 0)))) (h "11rwi69srrghzxrksxq79ivh4d7353lrcc3x75xks537ck0prprc")))

(define-public crate-solana-measure-1.8.4 (c (n "solana-measure") (v "1.8.4") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-metrics") (r "=1.8.4") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.8.4") (d #t) (k 0)))) (h "1p6zrs0i6796na9nd1vymaah5iiz4f1ar0m4lc9f3wxzz0gvpnvw")))

(define-public crate-solana-measure-1.8.5 (c (n "solana-measure") (v "1.8.5") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-metrics") (r "=1.8.5") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.8.5") (d #t) (k 0)))) (h "0g3dbmz0y9vmasrclrxpzxx1z5qxaagvgg88rhxcnyj6aifk05vb")))

(define-public crate-solana-measure-1.8.6 (c (n "solana-measure") (v "1.8.6") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-metrics") (r "=1.8.6") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.8.6") (d #t) (k 0)))) (h "03jhglf5p4z5kpw0rp1a36kx49jiisc84pw82j8vd741pq1nsv7j")))

(define-public crate-solana-measure-1.8.7 (c (n "solana-measure") (v "1.8.7") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-metrics") (r "=1.8.7") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.8.7") (d #t) (k 0)))) (h "1n755zxb0h5z83vg2lcgw2imnrfhym70qs11ydb4j4xcfd4g62jb")))

(define-public crate-solana-measure-1.8.8 (c (n "solana-measure") (v "1.8.8") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-metrics") (r "=1.8.8") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.8.8") (d #t) (k 0)))) (h "0szxxwdh0569c0ka7i95lqbfifzzywi5banymhcfv3y18v094smz")))

(define-public crate-solana-measure-1.8.9 (c (n "solana-measure") (v "1.8.9") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-metrics") (r "=1.8.9") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.8.9") (d #t) (k 0)))) (h "0ikcgmkqrrwcj6k6s69qqd7lk7rbb6d6ihxf8w0fkq8hvfgdqyh1")))

(define-public crate-solana-measure-1.9.0 (c (n "solana-measure") (v "1.9.0") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.9.0") (d #t) (k 0)))) (h "15zfrnn79a8b0ayfkv8dr9sg8h8bi7ynd4pn6x1pka1gf9apickq")))

(define-public crate-solana-measure-1.8.10 (c (n "solana-measure") (v "1.8.10") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-metrics") (r "=1.8.10") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.8.10") (d #t) (k 0)))) (h "0fxcpx8bck1v5lqzjs1l3v41cgkvxdiz3wkcs4im6bsv3g8qdxb7")))

(define-public crate-solana-measure-1.8.11 (c (n "solana-measure") (v "1.8.11") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-metrics") (r "=1.8.11") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.8.11") (d #t) (k 0)))) (h "014nj5a34x2qn82hj7z7mcb50y3v3vj7fpyr8k0rzwfxdwbz2k2h")))

(define-public crate-solana-measure-1.9.1 (c (n "solana-measure") (v "1.9.1") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.9.1") (d #t) (k 0)))) (h "17xmlwb5d8f58b7z9g8bm78fq3j9563x88i6113j7dx0ldw4in4m")))

(define-public crate-solana-measure-1.9.2 (c (n "solana-measure") (v "1.9.2") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.9.2") (d #t) (k 0)))) (h "1bjlfw1jizlqg2nx74nk9r32jipibpddwq32j1bm3pcn9jflks70")))

(define-public crate-solana-measure-1.9.3 (c (n "solana-measure") (v "1.9.3") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.9.3") (d #t) (k 0)))) (h "1fr9jp4cr4vkdxpxwnykafshzqy643yyszjq56hq8myfy469zlhp")))

(define-public crate-solana-measure-1.8.12 (c (n "solana-measure") (v "1.8.12") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-metrics") (r "=1.8.12") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.8.12") (d #t) (k 0)))) (h "1mhknikkb3c82pq3p9sdyfakyha8c1630xs8h8mc5aa7s2l9nyz7")))

(define-public crate-solana-measure-1.9.4 (c (n "solana-measure") (v "1.9.4") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.9.4") (d #t) (k 0)))) (h "0m7b68pgqsz602i009kgs6hpwm71fkxjiqsvr6yi2n0rgclv9kbc")))

(define-public crate-solana-measure-1.8.13 (c (n "solana-measure") (v "1.8.13") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-metrics") (r "=1.8.13") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.8.13") (d #t) (k 0)))) (h "0bf8xjw6rz5bsdlvfgsqy3pdg5sd4d9vxc7an1y1jp1rgmqi6ayd")))

(define-public crate-solana-measure-1.9.5 (c (n "solana-measure") (v "1.9.5") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.9.5") (d #t) (k 0)))) (h "04ypbjk904h1bwngqai9g1lcy29wxnhms1d4dja99a26sggbzcgq")))

(define-public crate-solana-measure-1.8.14 (c (n "solana-measure") (v "1.8.14") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-metrics") (r "=1.8.14") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.8.14") (d #t) (k 0)))) (h "021j51h30z6nb8i1gjkn8sw60arg0n4ii4faa9alzs6lb5lg3n5g")))

(define-public crate-solana-measure-1.9.6 (c (n "solana-measure") (v "1.9.6") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.9.6") (d #t) (k 0)))) (h "0lqiiz8j0mgk25p8cg8y6gc52ysq0hpyx0mw2d14mvsxngd1zdki")))

(define-public crate-solana-measure-1.9.7 (c (n "solana-measure") (v "1.9.7") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.9.7") (d #t) (k 0)))) (h "1q72d37w6sb7j17pq0w2zhagfma9k0bj0h7wb5n4mj4pj1p1432w")))

(define-public crate-solana-measure-1.8.16 (c (n "solana-measure") (v "1.8.16") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-metrics") (r "=1.8.16") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.8.16") (d #t) (k 0)))) (h "1yyb3sdybbaadvalh3cvc3q51wx2m4gbvnjlzmypkm46i15fd4w2")))

(define-public crate-solana-measure-1.9.8 (c (n "solana-measure") (v "1.9.8") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.9.8") (d #t) (k 0)))) (h "0fs40ni1w949qyy2djfw6f4rw58gy8k0kf220iibl422c7il025c")))

(define-public crate-solana-measure-1.9.9 (c (n "solana-measure") (v "1.9.9") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.9.9") (d #t) (k 0)))) (h "1d5pr602d8r6bfb5whvpghlz23iqfvds59vysm0km7sfmf7qxhsl")))

(define-public crate-solana-measure-1.10.0 (c (n "solana-measure") (v "1.10.0") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.10.0") (d #t) (k 0)))) (h "17fbi9ds9m3khxgsmq5cnldy079bh2qcb0amkj6hnmwr5n272c9v")))

(define-public crate-solana-measure-1.9.10 (c (n "solana-measure") (v "1.9.10") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.9.10") (d #t) (k 0)))) (h "142pgkw32jnkbxnv1rvhw78c3jfcbkrnwa43sm4hmqlaf9izcysc")))

(define-public crate-solana-measure-1.9.11 (c (n "solana-measure") (v "1.9.11") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.9.11") (d #t) (k 0)))) (h "1i8rzb08aidzl9svid6jmv39dkv722c4ibssi0lv27svsw0khc76")))

(define-public crate-solana-measure-1.9.12 (c (n "solana-measure") (v "1.9.12") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.9.12") (d #t) (k 0)))) (h "0rhixjm8a6pdh9cilhlh3qka8vasqjn0lviz8kvjziw1ihpcrs2p")))

(define-public crate-solana-measure-1.10.1 (c (n "solana-measure") (v "1.10.1") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.10.1") (d #t) (k 0)))) (h "09p55gwwhwk2nmcwqhszp1qi4i1fndg9ijqk8nix40h08gmsqd48")))

(define-public crate-solana-measure-1.10.2 (c (n "solana-measure") (v "1.10.2") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.10.2") (d #t) (k 0)))) (h "1f6s6lm0143mmq42pg2dxxg7jq6waihnb6dy2xglk8f1zshihssp")))

(define-public crate-solana-measure-1.9.13 (c (n "solana-measure") (v "1.9.13") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.9.13") (d #t) (k 0)))) (h "0b2d0swngp8741inl634lasm5cb8yl23d41lsp8pqr8zbd4ik0z4")))

(define-public crate-solana-measure-1.10.3 (c (n "solana-measure") (v "1.10.3") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.10.3") (d #t) (k 0)))) (h "0lhzgcil2180g6r8qsigcd6rq6p2i31bris3710ilp5sdzj9s3bf")))

(define-public crate-solana-measure-1.9.14 (c (n "solana-measure") (v "1.9.14") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.9.14") (d #t) (k 0)))) (h "14l630hd4024m5bhrhrgygzgxaxraw3si3ssqrzqyzckaw6xsxh5")))

(define-public crate-solana-measure-1.10.4 (c (n "solana-measure") (v "1.10.4") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.10.4") (d #t) (k 0)))) (h "147jqh0fc1hsdqcdwmjv1mag23szbsf8vmy2yb6nyfg5jb67jk2l")))

(define-public crate-solana-measure-1.10.5 (c (n "solana-measure") (v "1.10.5") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.10.5") (d #t) (k 0)))) (h "1ibc21jy2ix5xgn0r5441g1ad99vzav6xavkpp3db726ag3xrgg4")))

(define-public crate-solana-measure-1.10.6 (c (n "solana-measure") (v "1.10.6") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.10.6") (d #t) (k 0)))) (h "0a8kc7q2axakmkz1y8kbrcikyi2f6dq3yflgr66ldq7l68b0hmhd")))

(define-public crate-solana-measure-1.9.15 (c (n "solana-measure") (v "1.9.15") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.9.15") (d #t) (k 0)))) (h "1ngx4s4n95b0nyrzbpp1dmgfc49bh2szn3kmhffsvl08qc7cgnmr")))

(define-public crate-solana-measure-1.10.7 (c (n "solana-measure") (v "1.10.7") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.10.7") (d #t) (k 0)))) (h "1p05406r8iqpb2agvxzvs0qhddapnji066grpsmrxkf4bml0ln16")))

(define-public crate-solana-measure-1.10.8 (c (n "solana-measure") (v "1.10.8") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.10.8") (d #t) (k 0)))) (h "1cddz6df4sam30dshyq9f9mq1cxmhycx5lyd59q5vi5l4ngiacqq")))

(define-public crate-solana-measure-1.9.16 (c (n "solana-measure") (v "1.9.16") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.9.16") (d #t) (k 0)))) (h "155riay2nz2a5j9iwd86zylgzh8w3rgd1nc2c39fqypq12szgkcf")))

(define-public crate-solana-measure-1.9.17 (c (n "solana-measure") (v "1.9.17") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.9.17") (d #t) (k 0)))) (h "0b18n7yjcy6vag0hv5228v7zhqa2iv60h340pgwmrymvfcnxby54")))

(define-public crate-solana-measure-1.10.9 (c (n "solana-measure") (v "1.10.9") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.10.9") (d #t) (k 0)))) (h "0gbic268kjwnsdzhnh700zw4dp28k6blkzciln18jx62iqr8zdkp")))

(define-public crate-solana-measure-1.9.18 (c (n "solana-measure") (v "1.9.18") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.9.18") (d #t) (k 0)))) (h "1ckny9xgk2b3dsqcs4n480pk91yhn1xf9hmx001fpidcb3yb8asz")))

(define-public crate-solana-measure-1.10.10 (c (n "solana-measure") (v "1.10.10") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.10.10") (d #t) (k 0)))) (h "1b9ap7jsh2hi6a1ihzjcqy795qy1j0hr6n0d7hazhirfvd42gglc")))

(define-public crate-solana-measure-1.10.11 (c (n "solana-measure") (v "1.10.11") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.10.11") (d #t) (k 0)))) (h "1gwq6pgag9qvxk3q4zsv9p0gqv371sbp0ljqx9xz355hm0dsqbw7")))

(define-public crate-solana-measure-1.9.19 (c (n "solana-measure") (v "1.9.19") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.9.19") (d #t) (k 0)))) (h "1y0975d855dk2417p55s3i0yaxpm88f645nq94r1zfvr93356wwx")))

(define-public crate-solana-measure-1.10.12 (c (n "solana-measure") (v "1.10.12") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.10.12") (d #t) (k 0)))) (h "1l6g53ap03cll4y7dikxhr6n8yx3zll787jmp5ln4fmffz30krl3")))

(define-public crate-solana-measure-1.9.20 (c (n "solana-measure") (v "1.9.20") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.9.20") (d #t) (k 0)))) (h "1n9bd1pfhbi2892c52440ip8az9ghpyh78m5djfj85dig095pk9w")))

(define-public crate-solana-measure-1.10.13 (c (n "solana-measure") (v "1.10.13") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.10.13") (d #t) (k 0)))) (h "1syl4cgbgdmlvm3fy87pgxsja610z44l6zyjpwmfd09d6sxw5ypv")))

(define-public crate-solana-measure-1.9.21 (c (n "solana-measure") (v "1.9.21") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.9.21") (d #t) (k 0)))) (h "0y9hhnrp46559zrrir1b6b0wsxc2v78n8bz8mhqrwd78j030w5mz")))

(define-public crate-solana-measure-1.10.14 (c (n "solana-measure") (v "1.10.14") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.10.14") (d #t) (k 0)))) (h "1hj3v6frnrgwccjynbip6by25j0h73a2m7sy4mvhyx5r4d2j9ajd")))

(define-public crate-solana-measure-1.9.22 (c (n "solana-measure") (v "1.9.22") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.9.22") (d #t) (k 0)))) (h "0ryazgkipp459ivvxxrh3yhskw7jbsa8qv30pkjnqyvw6cb1xifv")))

(define-public crate-solana-measure-1.10.15 (c (n "solana-measure") (v "1.10.15") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.10.15") (d #t) (k 0)))) (h "00sr3f9rsf7cn7avsnrvi4hr3mmgr0gcyz46kfs0hw4c33afgl3n")))

(define-public crate-solana-measure-1.10.16 (c (n "solana-measure") (v "1.10.16") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.10.16") (d #t) (k 0)))) (h "0np8hpm8jvxxv03c9vsakjzvpr4nbh9fd7f56kwwgx7fbssxh95n")))

(define-public crate-solana-measure-1.10.17 (c (n "solana-measure") (v "1.10.17") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.10.17") (d #t) (k 0)))) (h "0dj0rg7jhf49hglm3lwsc9cy51jfvpqpqmnnrb7nfqizs8nc5c1v")))

(define-public crate-solana-measure-1.10.18 (c (n "solana-measure") (v "1.10.18") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.10.18") (d #t) (k 0)))) (h "1l30rk9msnx01szxjsa5wdq917qysxcv24kdkzirrjd26dll9716")))

(define-public crate-solana-measure-1.9.23 (c (n "solana-measure") (v "1.9.23") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.9.23") (d #t) (k 0)))) (h "1w1j2a2hc6gq7cipcqj9b6njsxxap70xjrpfwzjidw6d010flig2")))

(define-public crate-solana-measure-1.10.19 (c (n "solana-measure") (v "1.10.19") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.10.19") (d #t) (k 0)))) (h "0fdbg7w096lv60slshnrz5jz7crmsk0qcnkvq68dkgdh3v04lr4x")))

(define-public crate-solana-measure-1.9.24 (c (n "solana-measure") (v "1.9.24") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.9.24") (d #t) (k 0)))) (h "0rm25xrg3xchhnbbn2kqgjjl874p5y4y5dln264j3xgfj20pf2jv")))

(define-public crate-solana-measure-1.9.25 (c (n "solana-measure") (v "1.9.25") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.9.25") (d #t) (k 0)))) (h "13rvglx2z8hy0fmk3kzywmlzlfm1b885j661wkbha3307xma4fic")))

(define-public crate-solana-measure-1.10.20 (c (n "solana-measure") (v "1.10.20") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.10.20") (d #t) (k 0)))) (h "0gqs6sw82cppkb47pcfcwv3cpw2fx74clcjkj9db0hcq948ng01q")))

(define-public crate-solana-measure-1.9.26 (c (n "solana-measure") (v "1.9.26") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.9.26") (d #t) (k 0)))) (h "1v16qmgn0xac1sl7ii1m08zhq488y52vdmx6xmajqz3gm9mcs70s")))

(define-public crate-solana-measure-1.10.21 (c (n "solana-measure") (v "1.10.21") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.10.21") (d #t) (k 0)))) (h "1vqjyg717ckm3f202d9v4bdp9xl1gl7sz7z8ddaiwj6dw3va9104")))

(define-public crate-solana-measure-1.9.28 (c (n "solana-measure") (v "1.9.28") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.9.28") (d #t) (k 0)))) (h "0sb10s2dvswab5g035ddww3kyrfkv16d0rj2ndc2rx3s6sh16na0")))

(define-public crate-solana-measure-1.10.23 (c (n "solana-measure") (v "1.10.23") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.10.23") (d #t) (k 0)))) (h "0i1bc8bmixylmhz0ajc1vfz8ry03g0xsv2ppws997dxy70c1zcdh")))

(define-public crate-solana-measure-1.10.24 (c (n "solana-measure") (v "1.10.24") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.10.24") (d #t) (k 0)))) (h "1f7f55rjvff3a4jfldrgciv0g0wyg1c3gwhggsjl55dv7k83zfiv")))

(define-public crate-solana-measure-1.10.25 (c (n "solana-measure") (v "1.10.25") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.10.25") (d #t) (k 0)))) (h "0fg2vhrsn52rm2yachcvpq1xn93y95agy5ypy3dkp0firpd3qbsk")))

(define-public crate-solana-measure-1.9.29 (c (n "solana-measure") (v "1.9.29") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.9.29") (d #t) (k 0)))) (h "1n83h8jsfn609syjd8ywx1vy80mi2aw85j5a7h4zbvihrymbqgm2")))

(define-public crate-solana-measure-1.10.26 (c (n "solana-measure") (v "1.10.26") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.10.26") (d #t) (k 0)))) (h "023bn57qz06nd6hgdddvbxz6lrbv4lk18n4jgvmvaz19xfv84nmh")))

(define-public crate-solana-measure-1.11.0 (c (n "solana-measure") (v "1.11.0") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.11.0") (d #t) (k 0)))) (h "0zqjd8vs3nh1ksxh4ibz93rc8fb1l3m1djgcdx76w43jdpa76q0a")))

(define-public crate-solana-measure-1.10.27 (c (n "solana-measure") (v "1.10.27") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.10.27") (d #t) (k 0)))) (h "008yy8lq46qlwba39nmq8wngcwx5c56p7yrb88b74yzn21fj49hs")))

(define-public crate-solana-measure-1.11.1 (c (n "solana-measure") (v "1.11.1") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.11.1") (d #t) (k 0)))) (h "1290435rydsj21v5ah594pivyajy3mmhccc27wp540a0w4hz05z2")))

(define-public crate-solana-measure-1.10.28 (c (n "solana-measure") (v "1.10.28") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.10.28") (d #t) (k 0)))) (h "1g1kkmhqc4gflm45r5a6gggfmqrdgxccyhvsi7g182ab9wqrn0my")))

(define-public crate-solana-measure-1.10.29 (c (n "solana-measure") (v "1.10.29") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.10.29") (d #t) (k 0)))) (h "0zh4nxv5r7my67kmziz9fs3hyp9nj40igv7j98x0yn4hlsayn8nc")))

(define-public crate-solana-measure-1.10.30 (c (n "solana-measure") (v "1.10.30") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.10.30") (d #t) (k 0)))) (h "1899dwydmwlpy1f7cxrwcknl267c6vvpx23fnck1hss5piz668i9")))

(define-public crate-solana-measure-1.11.2 (c (n "solana-measure") (v "1.11.2") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.11.2") (d #t) (k 0)))) (h "1mcwgckmy2i886i5r948i1vpjbhzim7zy5lhq2ra8caw49vjsjqr")))

(define-public crate-solana-measure-1.10.31 (c (n "solana-measure") (v "1.10.31") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.10.31") (d #t) (k 0)))) (h "0ywm012x6a2fffwf9zzh20fhasjzvn9fvn6r8srv81hsgh94mmb7")))

(define-public crate-solana-measure-1.11.3 (c (n "solana-measure") (v "1.11.3") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.11.3") (d #t) (k 0)))) (h "05zz1m0kglxxr893w6yvq044w9chvhikxyykl5r2agkdz41dnspg")))

(define-public crate-solana-measure-1.10.32 (c (n "solana-measure") (v "1.10.32") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.10.32") (d #t) (k 0)))) (h "0va31vljg4qadzb7jhvdqiscl5m2359cfgn6bvbcisp6wbsaccpl")))

(define-public crate-solana-measure-1.11.4 (c (n "solana-measure") (v "1.11.4") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.11.4") (d #t) (k 0)))) (h "0vzjf3c0z5mdlmrf6xgxdhazp3i49myf94qnmnabr74cc5bdhgk9")))

(define-public crate-solana-measure-1.10.33 (c (n "solana-measure") (v "1.10.33") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.10.33") (d #t) (k 0)))) (h "0z7db554sfjc4hhw4dga65s4q6r2gdlig6bcjmllmhb9n93viph3")))

(define-public crate-solana-measure-1.10.34 (c (n "solana-measure") (v "1.10.34") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.10.34") (d #t) (k 0)))) (h "0n4sma2k9mb2npbglh182q3sb5pi8v4g7ll8pa28yjvj5nnfhl93")))

(define-public crate-solana-measure-1.11.5 (c (n "solana-measure") (v "1.11.5") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.11.5") (d #t) (k 0)))) (h "1dih4jjcgmxs713hw88j7ksn4xnbszipqp1gb0l89rjjb1x2qa43")))

(define-public crate-solana-measure-1.10.35 (c (n "solana-measure") (v "1.10.35") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.10.35") (d #t) (k 0)))) (h "16xdkl7cxivi07lycgzxmifi7b5dfvwicy748cv7lingwh3niyaz")))

(define-public crate-solana-measure-1.11.6 (c (n "solana-measure") (v "1.11.6") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.11.6") (d #t) (k 0)))) (h "1dz15z58gh39bc80kyqv5rn9hsbw054ri8cn6bjc4lp18xc34cqb")))

(define-public crate-solana-measure-1.11.7 (c (n "solana-measure") (v "1.11.7") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.11.7") (d #t) (k 0)))) (h "1z18d2msizl3d5r9sk50kf03a3kp8k4h1px9aq6mpv4fq9l2idn4")))

(define-public crate-solana-measure-1.11.8 (c (n "solana-measure") (v "1.11.8") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.11.8") (d #t) (k 0)))) (h "0ni921vz9d16h7f6km94b299wg8vwkigvw0azd8s7w30ggwmflkw")))

(define-public crate-solana-measure-1.11.10 (c (n "solana-measure") (v "1.11.10") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.11.10") (d #t) (k 0)))) (h "1bljnwqc1llk6h9wv9x689zb65lgzbxdimra0hbpl5h17f6xmhhi")))

(define-public crate-solana-measure-1.10.38 (c (n "solana-measure") (v "1.10.38") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.10.38") (d #t) (k 0)))) (h "17xrvjlq2mxsw1sqdybk417ic210cfn2l5ncjzk2nxjacn17qfxh")))

(define-public crate-solana-measure-1.13.0 (c (n "solana-measure") (v "1.13.0") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.13.0") (d #t) (k 0)))) (h "0jydharn0ql4b37jh94zgyxj0gwhqqc9wjqvk7942sbjswgcwks9")))

(define-public crate-solana-measure-1.14.0 (c (n "solana-measure") (v "1.14.0") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.14.0") (d #t) (k 0)))) (h "12s4rmlppfyzp0n72g36q4b0dz36amds8gd5myxlc2ll6whb2lql")))

(define-public crate-solana-measure-1.14.1 (c (n "solana-measure") (v "1.14.1") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.14.1") (d #t) (k 0)))) (h "1sp2kwr8rjv72hd0nkkmh4wdkflr1x6klwivwghxbs3cpiswnv93")))

(define-public crate-solana-measure-1.10.39 (c (n "solana-measure") (v "1.10.39") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.10.39") (d #t) (k 0)))) (h "1p0b2l2i0jbs8c6gdrdq84xh92dg88ck8252yj2bfp534xffpnkf")))

(define-public crate-solana-measure-1.14.2 (c (n "solana-measure") (v "1.14.2") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.14.2") (d #t) (k 0)))) (h "1lydi672wawic3zj8agnvdnz07fjy551dm4laxycvy505k8nwsxj")))

(define-public crate-solana-measure-1.13.1 (c (n "solana-measure") (v "1.13.1") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.13.1") (d #t) (k 0)))) (h "0ssfy8jdgjrl4h45q3hjhqv9ql3xkf2jf8pk9r9nnb7amfr6d2va")))

(define-public crate-solana-measure-1.14.3 (c (n "solana-measure") (v "1.14.3") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.14.3") (d #t) (k 0)))) (h "14nbcg5v41b7m5gxp7ywksxg4mdvnw0hfpm7gahzz0m9yfmilkcm")))

(define-public crate-solana-measure-1.10.40 (c (n "solana-measure") (v "1.10.40") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.10.40") (d #t) (k 0)))) (h "14qb0i1lnrs1nxxvnqh6rdy7gwqvwbwjc6hg7yy9315ibzjnycrv")))

(define-public crate-solana-measure-1.14.4 (c (n "solana-measure") (v "1.14.4") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.14.4") (d #t) (k 0)))) (h "0svv6mmvdx8dr5qafsqck6z282ijxjpllawwcpzdplgrd2p3np8l")))

(define-public crate-solana-measure-1.13.2 (c (n "solana-measure") (v "1.13.2") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.13.2") (d #t) (k 0)))) (h "1ggyzgdwpzncjdvf7m0zb9j03yjl2f7lb9jqggi5gzyddk3b4c6l")))

(define-public crate-solana-measure-1.14.5 (c (n "solana-measure") (v "1.14.5") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.14.5") (d #t) (k 0)))) (h "0fq8xwrg0lgigxkchfgvxv1vv1f1g9irr4k7q14dml0k7cxxghdd")))

(define-public crate-solana-measure-1.10.41 (c (n "solana-measure") (v "1.10.41") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.10.41") (d #t) (k 0)))) (h "0d6i88a4aqhj4m4hibld8l2s9fq2lcch9w4hghw9643fxyrr902q")))

(define-public crate-solana-measure-1.13.3 (c (n "solana-measure") (v "1.13.3") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.13.3") (d #t) (k 0)))) (h "1vw49ssz7zr63p51l80wa0v6l9iw2fvy3xkcb0s58l30xls2jxv6")))

(define-public crate-solana-measure-1.13.4 (c (n "solana-measure") (v "1.13.4") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.13.4") (d #t) (k 0)))) (h "179q0brc61l6j67xn16sk485fbnyz5kyqwaccgrslk6c8s54y4ng")))

(define-public crate-solana-measure-1.14.6 (c (n "solana-measure") (v "1.14.6") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.14.6") (d #t) (k 0)))) (h "0r2w2yidyz6flgprmalja4kgnzwqy3b6p0zvylljz1i7kj0a295a")))

(define-public crate-solana-measure-1.14.7 (c (n "solana-measure") (v "1.14.7") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.14.7") (d #t) (k 0)))) (h "0zwgmwpw26ad1qsxyn0sw0zv5pvna5grndips2m7fvpqfjygg5vm")))

(define-public crate-solana-measure-1.13.5 (c (n "solana-measure") (v "1.13.5") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.13.5") (d #t) (k 0)))) (h "0blah2zzih203w1w625g09sdvbdzz86hva1ykv5hgsbgzxa44pr7")))

(define-public crate-solana-measure-1.14.8 (c (n "solana-measure") (v "1.14.8") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.14.8") (d #t) (k 0)))) (h "01k6b97syhib9jn0a0mjdi4d7bbvdd4pi0rlhjvpvzmll3z6n4xs")))

(define-public crate-solana-measure-1.14.9 (c (n "solana-measure") (v "1.14.9") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.14.9") (d #t) (k 0)))) (h "0hn4pldzl62jckvdzm6d5vqjim7r3p3bb80ibm5qfj3z6d8zmczw")))

(define-public crate-solana-measure-1.14.10 (c (n "solana-measure") (v "1.14.10") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.14.10") (d #t) (k 0)))) (h "165ss240v4bc8rmcfsrskdrx3ydc6826wjv6j8p8xdhdkgghvk6f")))

(define-public crate-solana-measure-1.14.11 (c (n "solana-measure") (v "1.14.11") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.14.11") (d #t) (k 0)))) (h "0rhgplia13fh3ya87qk8fxdfihpwz9y6hvk3y8cgvk9pxvkv1frk")))

(define-public crate-solana-measure-1.14.12 (c (n "solana-measure") (v "1.14.12") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.14.12") (d #t) (k 0)))) (h "0hccxc96hfq443zrax1ims80gsf6rwpyj1cp9l92n7a5gmj6bqx3")))

(define-public crate-solana-measure-1.13.6 (c (n "solana-measure") (v "1.13.6") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.13.6") (d #t) (k 0)))) (h "1avnrgkrdqvb15m74rxd5zz214js9namy1ymvqfmsys32f8igfn2")))

(define-public crate-solana-measure-1.14.13 (c (n "solana-measure") (v "1.14.13") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.14.13") (d #t) (k 0)))) (h "0xvajh26qrkjn6jgd8gi4fi015j9l3g0w1jra1f61xqr999x4014")))

(define-public crate-solana-measure-1.15.0 (c (n "solana-measure") (v "1.15.0") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.15.0") (d #t) (k 0)))) (h "1bnvb077k7svhcc709a12rckcfcszm57d5kpxxhbi404c4xwxk1v") (y #t)))

(define-public crate-solana-measure-1.14.14 (c (n "solana-measure") (v "1.14.14") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.14.14") (d #t) (k 0)))) (h "17zaan6pr1rxbr64mhmya5fbh1f56ipalfyvgjdam6iz334zb2z9")))

(define-public crate-solana-measure-1.14.15 (c (n "solana-measure") (v "1.14.15") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.14.15") (d #t) (k 0)))) (h "1my62kgj9q24h6picqr3zv3g6swig1gwafxncq99367lp87c06as")))

(define-public crate-solana-measure-1.15.1 (c (n "solana-measure") (v "1.15.1") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.15.1") (d #t) (k 0)))) (h "04lw4sklcj6zcwbxk0yrxppv1i51cxp5xc59b0chyy0jaxpcckxh") (y #t)))

(define-public crate-solana-measure-1.15.2 (c (n "solana-measure") (v "1.15.2") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.15.2") (d #t) (k 0)))) (h "1qr4n5581m5i3rdl70n7yx8kndjms5c1fkkaq3jbn3sspql3bc2j") (y #t)))

(define-public crate-solana-measure-1.14.16 (c (n "solana-measure") (v "1.14.16") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.14.16") (d #t) (k 0)))) (h "0smahm0visj5g3p6221d6p3gbwglvyjl8s5xi0yb6dbnjn002c7p")))

(define-public crate-solana-measure-1.14.17 (c (n "solana-measure") (v "1.14.17") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.14.17") (d #t) (k 0)))) (h "1fqjbjdykg71k1gvwhx48569vr2l4zm2m9awsizf22qgpgx7i089")))

(define-public crate-solana-measure-1.13.7 (c (n "solana-measure") (v "1.13.7") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.13.7") (d #t) (k 0)))) (h "0m196p4jxlcr1dzw79lx11a4im2zg2smf5mwhs84b4r3xg2ad6qv")))

(define-public crate-solana-measure-1.14.18 (c (n "solana-measure") (v "1.14.18") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.14.18") (d #t) (k 0)))) (h "1wdqgydkvhv98gab51ndn9jh5x7p325gj231hh07qbizx0m8g2n9")))

(define-public crate-solana-measure-1.16.0 (c (n "solana-measure") (v "1.16.0") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.16.0") (d #t) (k 0)))) (h "14alq52ryaxj00d77xcjnfz3lmmg08sjfj5syjvxlvgs7qw3lcza")))

(define-public crate-solana-measure-1.16.1 (c (n "solana-measure") (v "1.16.1") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.16.1") (d #t) (k 0)))) (h "13mj88yrl3x8kdgc40wxs9qyj22br8ds7mc05375ila6xicn75r6")))

(define-public crate-solana-measure-1.14.19 (c (n "solana-measure") (v "1.14.19") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.14.19") (d #t) (k 0)))) (h "0w183zia6nfdbi4q77sd6j5fcbmzw8c4yjd0n1vs42ni9viiqsf4")))

(define-public crate-solana-measure-1.16.2 (c (n "solana-measure") (v "1.16.2") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.16.2") (d #t) (k 0)))) (h "12vznb0bdhnbn3g4cjbkb6qbjngvzq8vsyggdg16p5yfmxq8ph9m")))

(define-public crate-solana-measure-1.16.3 (c (n "solana-measure") (v "1.16.3") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.16.3") (d #t) (k 0)))) (h "19nvsqb8jc9hs1642ylk6j6h3w2kcj72bz4yhclmbkf9z9q8n0ra")))

(define-public crate-solana-measure-1.14.20 (c (n "solana-measure") (v "1.14.20") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.14.20") (d #t) (k 0)))) (h "18wkgvdnsi3xfvafbi2ra5nfhprp7biacdzw7zs58x0jj3l0jg9a")))

(define-public crate-solana-measure-1.16.4 (c (n "solana-measure") (v "1.16.4") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.16.4") (d #t) (k 0)))) (h "00bwr175vqffcxh8gg3vndgd0h8jm0bggcqqy1ing1cx74l3ymcg")))

(define-public crate-solana-measure-1.16.5 (c (n "solana-measure") (v "1.16.5") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.16.5") (d #t) (k 0)))) (h "16gc81glzgydf8jzaj4yv1v2lcbgy9bnn8lj96r2yspj9b7bkqfs")))

(define-public crate-solana-measure-1.14.21 (c (n "solana-measure") (v "1.14.21") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.14.21") (d #t) (k 0)))) (h "0xn3k1glw0wynn2qkzp0wkfdhws9gk919v7fl9iv5xlh42q6qvm9")))

(define-public crate-solana-measure-1.14.22 (c (n "solana-measure") (v "1.14.22") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.14.22") (d #t) (k 0)))) (h "08nqlh50xckbqbqfsw02avwjr5zj5i82vmpba93xrxax85vm2gjq")))

(define-public crate-solana-measure-1.16.6 (c (n "solana-measure") (v "1.16.6") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.16.6") (d #t) (k 0)))) (h "0ljyg34a0641m7bbf8w28czgi94q2mi4llb7xzkm63rv33ga5awv")))

(define-public crate-solana-measure-1.16.7 (c (n "solana-measure") (v "1.16.7") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.16.7") (d #t) (k 0)))) (h "1ws361yvpv8kml1pjarmd9dpim81q1igq4ksbpm391l6hjrylgvb")))

(define-public crate-solana-measure-1.14.23 (c (n "solana-measure") (v "1.14.23") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.14.23") (d #t) (k 0)))) (h "0a29jl486pln8mhv21dzp4h83mc0mvxd29bhfqdmg2a22jgj3ss8")))

(define-public crate-solana-measure-1.16.8 (c (n "solana-measure") (v "1.16.8") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.16.8") (d #t) (k 0)))) (h "1rz8rn20bwkvdwhkdfssamz24hrrqdgns1vbjqmv2aqsc2g1c9i4")))

(define-public crate-solana-measure-1.14.24 (c (n "solana-measure") (v "1.14.24") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.14.24") (d #t) (k 0)))) (h "15vd6zid46bk72hq45shxjrslxx4zw9j67sxjlxymi8zi1na912g")))

(define-public crate-solana-measure-1.16.9 (c (n "solana-measure") (v "1.16.9") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.16.9") (d #t) (k 0)))) (h "168kr7jwk2wd4wm4aj41078gpgwcqbsj8i7q2kpyqxv3c7d330c4")))

(define-public crate-solana-measure-1.16.10 (c (n "solana-measure") (v "1.16.10") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.16.10") (d #t) (k 0)))) (h "16p1ym9b2dr3ikvklsw8lm3af4b8si97ski89v4hsc2czvr8pbw4")))

(define-public crate-solana-measure-1.14.25 (c (n "solana-measure") (v "1.14.25") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.14.25") (d #t) (k 0)))) (h "1k26xmlzzcwd920wxmdk35xyg97kqxwbv9spyznkfnzm73q7bhws")))

(define-public crate-solana-measure-1.16.11 (c (n "solana-measure") (v "1.16.11") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.16.11") (d #t) (k 0)))) (h "1lcfjklcv328fr5xl24xjm6j69gd1si6akv8a8kzz9xa5vdr0r21")))

(define-public crate-solana-measure-1.14.26 (c (n "solana-measure") (v "1.14.26") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.14.26") (d #t) (k 0)))) (h "0gkn50yavd05a60d9jcrrq75araia0mzh963da4krq02lcsmg6dg")))

(define-public crate-solana-measure-1.16.12 (c (n "solana-measure") (v "1.16.12") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.16.12") (d #t) (k 0)))) (h "02hkq7rdl9ha3spchcy7ghy90n14f7nbapfzi7dgfgxjpv7fw7a4")))

(define-public crate-solana-measure-1.14.27 (c (n "solana-measure") (v "1.14.27") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.14.27") (d #t) (k 0)))) (h "12jkz1swl0j8hr89ail18h7njd7xy00g5wrvlvmsf4p28bk0gvbi")))

(define-public crate-solana-measure-1.16.13 (c (n "solana-measure") (v "1.16.13") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.16.13") (d #t) (k 0)))) (h "1z9z49w4ln20gi25agyy30hi8g4yrfm8h4f50wvxvviwyqprslnk")))

(define-public crate-solana-measure-1.16.14 (c (n "solana-measure") (v "1.16.14") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.16.14") (d #t) (k 0)))) (h "0q6sgrlnjqyx6l9qz3fqmbz0v48xvcbg5l22bgkf0098y4kkqmhr")))

(define-public crate-solana-measure-1.14.28 (c (n "solana-measure") (v "1.14.28") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.14.28") (d #t) (k 0)))) (h "0scwh0dnkr64mj8ywcy5gc0kgsl399qykh78bgsjpf9g59rgs1lj")))

(define-public crate-solana-measure-1.14.29 (c (n "solana-measure") (v "1.14.29") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.14.29") (d #t) (k 0)))) (h "03cwfmkx16m3z3aarv6mmd357j8nxdrmakrwxxs3wq0cq3rhhxgy")))

(define-public crate-solana-measure-1.16.15 (c (n "solana-measure") (v "1.16.15") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.16.15") (d #t) (k 0)))) (h "15awwk5qa4zjk5c0dz7c2nmr5ykn6np9aycsc49c5wsxnghwc68f")))

(define-public crate-solana-measure-1.17.0 (c (n "solana-measure") (v "1.17.0") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.0") (d #t) (k 0)))) (h "03wkfbdl0lc3jp0bpcs1047yrwcwmmhivn7jnppj1nqglg63p8l8")))

(define-public crate-solana-measure-1.16.16 (c (n "solana-measure") (v "1.16.16") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.16.16") (d #t) (k 0)))) (h "08yddyc1v45nyswvh33qw9jf0kg7yhjhwy4wl56alqhfb1bi65qd")))

(define-public crate-solana-measure-1.17.1 (c (n "solana-measure") (v "1.17.1") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.1") (d #t) (k 0)))) (h "07wd7p8qyl9a2h31y5csp2k5m60gvkkjhb2a01lfbd9i5av21v44")))

(define-public crate-solana-measure-1.16.17 (c (n "solana-measure") (v "1.16.17") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.16.17") (d #t) (k 0)))) (h "1xkcvjsvcsgljiyv0xnnja8ssg76zwc8miqiy2z0m12xgy55n5nv")))

(define-public crate-solana-measure-1.17.2 (c (n "solana-measure") (v "1.17.2") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.2") (d #t) (k 0)))) (h "1zyfrc5q39wkx3incm0lizjmljz1asgcly985cprzg9azfwh6x78")))

(define-public crate-solana-measure-1.16.18 (c (n "solana-measure") (v "1.16.18") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.16.18") (d #t) (k 0)))) (h "1gq03nn4g6c57fbmn3la32ay1wb20wa4kh2ss1bmr1b27ial8hll")))

(define-public crate-solana-measure-1.17.3 (c (n "solana-measure") (v "1.17.3") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.3") (d #t) (k 0)))) (h "0g394jhscz6a9ngkix3j2s5nasy9h12mn11bg6d40z3nxmcaxlya")))

(define-public crate-solana-measure-1.17.4 (c (n "solana-measure") (v "1.17.4") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.4") (d #t) (k 0)))) (h "16z7cahbsbakb3pi423mr8ny1lbhhm1f34n6lcq3il90xipdiwyv")))

(define-public crate-solana-measure-1.16.19 (c (n "solana-measure") (v "1.16.19") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.16.19") (d #t) (k 0)))) (h "0hwq3llp1ss064z72bs1qbzzr3jhhsn3k1h59i8ys9aimlr4mdff")))

(define-public crate-solana-measure-1.17.5 (c (n "solana-measure") (v "1.17.5") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.5") (d #t) (k 0)))) (h "1bbisy6m61915zdsiakyd3z4yfrps4gyrrmk4q3ag1m2dbcy7m6q")))

(define-public crate-solana-measure-1.17.6 (c (n "solana-measure") (v "1.17.6") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.6") (d #t) (k 0)))) (h "0pxx72db6ybivwapx80hrynaf7xsflc9z7bjjy1jippm87yqg25m")))

(define-public crate-solana-measure-1.16.20 (c (n "solana-measure") (v "1.16.20") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.16.20") (d #t) (k 0)))) (h "1i0b4ajgq6499kigkcvprwh70pk0pmfi8gy796hvmvbbwzrm05jd")))

(define-public crate-solana-measure-1.17.7 (c (n "solana-measure") (v "1.17.7") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.7") (d #t) (k 0)))) (h "0xahw0kpq0428n6xnd9v1w6wyrs1i1g801a044fvcx5pvgz1zk39")))

(define-public crate-solana-measure-1.16.21 (c (n "solana-measure") (v "1.16.21") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.16.21") (d #t) (k 0)))) (h "0n29xkbzsn0cnaqmrl7rm0c55sivkm892x4z2ylpqsdhqwdyg42i")))

(define-public crate-solana-measure-1.17.8 (c (n "solana-measure") (v "1.17.8") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.8") (d #t) (k 0)))) (h "19x04d9hzwscqhr7g3vmj4vc3i4jx8bd9imiczh3cy0prnjn94hs")))

(define-public crate-solana-measure-1.16.22 (c (n "solana-measure") (v "1.16.22") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.16.22") (d #t) (k 0)))) (h "1857clms4zlyid2b7k1k49wfh2pq2wxpv65ilsjca3fsr9ckb13y")))

(define-public crate-solana-measure-1.16.23 (c (n "solana-measure") (v "1.16.23") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.16.23") (d #t) (k 0)))) (h "15xb4fs8vfdww574c4qnpphcvm6wwcr5bb0i025xilmr32w4arwg")))

(define-public crate-solana-measure-1.17.9 (c (n "solana-measure") (v "1.17.9") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.9") (d #t) (k 0)))) (h "19y4xs4kxr81fkrmy8zgwynl9j10qh4540psn0w4rfh1phspfx2r")))

(define-public crate-solana-measure-1.17.10 (c (n "solana-measure") (v "1.17.10") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.10") (d #t) (k 0)))) (h "0l2mda59xm9mqismmxi7cwiwsxjgb0mwsv2bky3m8abknv6h3gf1")))

(define-public crate-solana-measure-1.17.11 (c (n "solana-measure") (v "1.17.11") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.11") (d #t) (k 0)))) (h "0189k5h9zayigcbqg5hyhicmm4lgmjp35alif8p42my5hbh4h2r0")))

(define-public crate-solana-measure-1.17.12 (c (n "solana-measure") (v "1.17.12") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.12") (d #t) (k 0)))) (h "0x069d8n9fpmyl4jsqaw7afc3hf0pn59s2cjprvmflgiicmh5l7r")))

(define-public crate-solana-measure-1.16.24 (c (n "solana-measure") (v "1.16.24") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.16.24") (d #t) (k 0)))) (h "18q89s57wqrlqjypaz4dsqjkqwjqa3mx0yjdj1rn35bg9ad3ic3p")))

(define-public crate-solana-measure-1.17.13 (c (n "solana-measure") (v "1.17.13") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.13") (d #t) (k 0)))) (h "0dj04vnv180c53as602f4h68lddg70f009q8qfqib92rxjrqnahb")))

(define-public crate-solana-measure-1.17.14 (c (n "solana-measure") (v "1.17.14") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.14") (d #t) (k 0)))) (h "0dn3ivk5j25d90b08j4yi2hwlcz5n0vmml107rf5y830sy9im0qr")))

(define-public crate-solana-measure-1.17.15 (c (n "solana-measure") (v "1.17.15") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.15") (d #t) (k 0)))) (h "0pgx8g192v6a1hgljhchwhcqib8z9q17q17avv05420j2qfh9r9d")))

(define-public crate-solana-measure-1.16.25 (c (n "solana-measure") (v "1.16.25") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.16.25") (d #t) (k 0)))) (h "1rxf0craijjp6mff21vlcswxf3dagr6md04flsxs72yg9dhbcbck")))

(define-public crate-solana-measure-1.16.27 (c (n "solana-measure") (v "1.16.27") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.16.27") (d #t) (k 0)))) (h "1zwrmc1wzr41rsswykwiabplhihrvy4az5jkvwya6cqcyj68l62g")))

(define-public crate-solana-measure-1.17.16 (c (n "solana-measure") (v "1.17.16") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.16") (d #t) (k 0)))) (h "0vgky2qasyxjqp7wz5zray6x45gz073lm9r56r53dw75z4z28nnd")))

(define-public crate-solana-measure-1.17.17 (c (n "solana-measure") (v "1.17.17") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.17") (d #t) (k 0)))) (h "0y69pbvklpbyj66d83qx8myj46srikycfkqpnjw7gn0b90z1650d")))

(define-public crate-solana-measure-1.17.18 (c (n "solana-measure") (v "1.17.18") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.18") (d #t) (k 0)))) (h "0kq6msl13zbgsav1fc1njvw6jpf08lj2gg2k2qg5k2f0pjgwa7ya")))

(define-public crate-solana-measure-1.18.0 (c (n "solana-measure") (v "1.18.0") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.18.0") (d #t) (k 0)))) (h "053zyn1mh80qnx70zs12sm7432afnmbrpkc2z85n9rvcli5ci78g")))

(define-public crate-solana-measure-1.18.1 (c (n "solana-measure") (v "1.18.1") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.18.1") (d #t) (k 0)))) (h "1m3y9xqsnfp7bfjw9nln1g59vkcy6bb4a6pqb6p46m8im6gfxafd")))

(define-public crate-solana-measure-1.17.20 (c (n "solana-measure") (v "1.17.20") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.20") (d #t) (k 0)))) (h "0afzhq1bh3vdm689phkkyzsjda2p7cq4zfg33c6vld3w3gqz8rjp")))

(define-public crate-solana-measure-1.17.22 (c (n "solana-measure") (v "1.17.22") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.22") (d #t) (k 0)))) (h "1i98ss485cqwnnwgw2hpanli5rv1fyndgrdh08vs43g02i1271xn")))

(define-public crate-solana-measure-1.18.2 (c (n "solana-measure") (v "1.18.2") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.18.2") (d #t) (k 0)))) (h "17vy07bv66z4bbnjw0img1514zb7zjiax5r50dnqmzdm9xh26drw")))

(define-public crate-solana-measure-1.17.23 (c (n "solana-measure") (v "1.17.23") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.23") (d #t) (k 0)))) (h "09kzmff7z3shww1gvp9dqynl8zk96b20rhmcd7xxh8yizri2xllv")))

(define-public crate-solana-measure-1.18.3 (c (n "solana-measure") (v "1.18.3") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.18.3") (d #t) (k 0)))) (h "116q7yi4bhl746k5k5bhs6pic86nr2c5cg33vxxf5gfbqw5k3s96")))

(define-public crate-solana-measure-1.18.4 (c (n "solana-measure") (v "1.18.4") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.18.4") (d #t) (k 0)))) (h "0q66haxw8yc531n6nanvjy0b10cqq4f23gzw7gxa2gb1m1aylwcn")))

(define-public crate-solana-measure-1.17.24 (c (n "solana-measure") (v "1.17.24") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.24") (d #t) (k 0)))) (h "1z79bc0qnj0pwh2a0anxzaxnfj8vbr81ap7ny964r7idgp5yzqbr")))

(define-public crate-solana-measure-1.17.25 (c (n "solana-measure") (v "1.17.25") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.25") (d #t) (k 0)))) (h "06aybzmganm8vja996yr1cw4sz37hxs9yi1ifl3r8w5ikh7yf9wk")))

(define-public crate-solana-measure-1.18.5 (c (n "solana-measure") (v "1.18.5") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.18.5") (d #t) (k 0)))) (h "1f4pxpifpbxc5dzb88mpzfwpbqchlph1hc5fb0mfqsvy47zdacsm")))

(define-public crate-solana-measure-1.17.26 (c (n "solana-measure") (v "1.17.26") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.26") (d #t) (k 0)))) (h "1n8mkascq8aa4ybhl3a3qc4nqp59z95692j8m5qwlx2hcd1w6102")))

(define-public crate-solana-measure-1.18.6 (c (n "solana-measure") (v "1.18.6") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.18.6") (d #t) (k 0)))) (h "1ib5iw8crsn84gmw3z9rz2wrg3j47vh3rh3v6xvf4ywn80957p1g")))

(define-public crate-solana-measure-1.17.27 (c (n "solana-measure") (v "1.17.27") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.27") (d #t) (k 0)))) (h "0416pn9dxd7y62masb2g57f7q5x3j1zmmy4fr9mrs4xivp6dhmwx")))

(define-public crate-solana-measure-1.18.7 (c (n "solana-measure") (v "1.18.7") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.18.7") (d #t) (k 0)))) (h "0fjprrp96r7rcpr0scx4ph3ppm9m02ikavsi5bkddxcgm7wmkbii")))

(define-public crate-solana-measure-1.18.8 (c (n "solana-measure") (v "1.18.8") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.18.8") (d #t) (k 0)))) (h "0h07zjlcjaxbgiv7ncp3rfjih9df2b1m8s2yykjdpm023qgv26h4")))

(define-public crate-solana-measure-1.17.28 (c (n "solana-measure") (v "1.17.28") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.28") (d #t) (k 0)))) (h "1l0nql16xfhpn0y8qdm1b4axcir7hbdkh0vfdy97svxi6l15y16b")))

(define-public crate-solana-measure-1.18.9 (c (n "solana-measure") (v "1.18.9") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.18.9") (d #t) (k 0)))) (h "0x8hklj1v3bwfiks0x7x3nfd5gqp2d4dk029a9ywn3c7mcij01xx")))

(define-public crate-solana-measure-1.17.29 (c (n "solana-measure") (v "1.17.29") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.29") (d #t) (k 0)))) (h "1fv5ymykfcgda571rmssiziyh66jndgvggkm7d3ykd53jis97g5y")))

(define-public crate-solana-measure-1.17.30 (c (n "solana-measure") (v "1.17.30") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.30") (d #t) (k 0)))) (h "0mmmjds6np9mhzp03az4ymci08ggdrx8dxjxqg7qndvbghbphwc5")))

(define-public crate-solana-measure-1.18.10 (c (n "solana-measure") (v "1.18.10") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.18.10") (d #t) (k 0)))) (h "1zj7hv4gjrkfsh36xgd50z9cm2g4nzjspaxgvngkvqirpfvhdhgr")))

(define-public crate-solana-measure-1.18.11 (c (n "solana-measure") (v "1.18.11") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.18.11") (d #t) (k 0)))) (h "19p7mw3xvyrg19dizvksf6k9x5kf2s2h1336kb3fl0xs418wdg9a")))

(define-public crate-solana-measure-1.17.31 (c (n "solana-measure") (v "1.17.31") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.31") (d #t) (k 0)))) (h "0mllvx3j0xlrqi29x9g3h0n8y9bkzlpn9chka3854mf6n80yyfbs")))

(define-public crate-solana-measure-1.18.12 (c (n "solana-measure") (v "1.18.12") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.18.12") (d #t) (k 0)))) (h "1n8ymrr8gwpxfy0pyal0zyf8h3bwgp6a025h5xb4nbqwbxm723rh")))

(define-public crate-solana-measure-1.17.32 (c (n "solana-measure") (v "1.17.32") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.32") (d #t) (k 0)))) (h "18962mp8wxi9ylzbbqfwy6l5qdhnblaachj4fry1sin5q54ppdpa")))

(define-public crate-solana-measure-1.17.33 (c (n "solana-measure") (v "1.17.33") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.33") (d #t) (k 0)))) (h "1xbca84hgqkgkr92x46axyf90f9d3klhlr757lwvpa80c9141mdb")))

(define-public crate-solana-measure-1.18.13 (c (n "solana-measure") (v "1.18.13") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.18.13") (d #t) (k 0)))) (h "0varc0h6a0kmyi1ahnnpa4851n50ba53q9ixgnv0d56nqgdnkgz9")))

(define-public crate-solana-measure-1.18.14 (c (n "solana-measure") (v "1.18.14") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.18.14") (d #t) (k 0)))) (h "05mn63bmralhjzxf5zm0izw49zk6bsj1fgw6fgqdyp90cjqjz8vi")))

(define-public crate-solana-measure-1.17.34 (c (n "solana-measure") (v "1.17.34") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.34") (d #t) (k 0)))) (h "1sls8f79x1skn7ng1iswwy5zj7sw9x1077xzmsla7dih2acfv8qq")))

