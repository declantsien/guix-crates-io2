(define-module (crates-io so la solarti-instruction-padding) #:use-module (crates-io))

(define-public crate-solarti-instruction-padding-0.1.0 (c (n "solarti-instruction-padding") (v "0.1.0") (d (list (d (n "num_enum") (r "^0.5.9") (d #t) (k 0)) (d (n "solana-program") (r "^1.14.4") (d #t) (k 0)) (d (n "solana-program-test") (r "^1.14.4") (d #t) (k 2)) (d (n "solana-sdk") (r "^1.14.4") (d #t) (k 2)))) (h "02m13vhm2r4a4zx0nsv0q8hdkv1qlf8z1z61vdwhlpcbkjp27w0a") (f (quote (("test-sbf") ("no-entrypoint"))))))

(define-public crate-solarti-instruction-padding-0.1.1 (c (n "solarti-instruction-padding") (v "0.1.1") (d (list (d (n "miraland-program") (r "=1.14.17-rc4") (d #t) (k 0)) (d (n "miraland-program-test") (r "=1.14.17-rc4") (d #t) (k 2)) (d (n "miraland-sdk") (r "=1.14.17-rc4") (d #t) (k 2)) (d (n "num_enum") (r "^0.5.9") (d #t) (k 0)))) (h "01xn7gp98qzp1wcl3m6wj5lmsh50wzm9k9mhyffbmzlpmqqc1law") (f (quote (("test-sbf") ("no-entrypoint"))))))

(define-public crate-solarti-instruction-padding-0.1.2 (c (n "solarti-instruction-padding") (v "0.1.2") (d (list (d (n "miraland-program") (r "=1.14.18") (d #t) (k 0)) (d (n "miraland-program-test") (r "=1.14.18") (d #t) (k 2)) (d (n "miraland-sdk") (r "=1.14.18") (d #t) (k 2)) (d (n "num_enum") (r "^0.5.9") (d #t) (k 0)))) (h "1yr0c8yrsixww2zamfwqqjab1nn6dg05qk9gqc3k979zlwykdi8b") (f (quote (("test-sbf") ("no-entrypoint"))))))

(define-public crate-solarti-instruction-padding-0.1.3 (c (n "solarti-instruction-padding") (v "0.1.3") (d (list (d (n "miraland-program") (r "^1.14.18") (d #t) (k 0)) (d (n "miraland-program-test") (r "^1.14.18") (d #t) (k 2)) (d (n "miraland-sdk") (r "^1.14.18") (d #t) (k 2)) (d (n "num_enum") (r "^0.5.9") (d #t) (k 0)))) (h "1xfpn9896ma3k3wqpz1f5fb2kn9pxd5pih8r9g9agc2xpfn168jj") (f (quote (("test-sbf") ("no-entrypoint"))))))

(define-public crate-solarti-instruction-padding-0.1.4 (c (n "solarti-instruction-padding") (v "0.1.4") (d (list (d (n "miraland-program") (r "^1.18.2") (d #t) (k 0)) (d (n "miraland-program-test") (r "^1.18.2") (d #t) (k 2)) (d (n "miraland-sdk") (r "^1.18.2") (d #t) (k 2)) (d (n "num_enum") (r "^0.7.1") (d #t) (k 0)))) (h "1akbl0y4c0i3jy64zj9q8yif5ym1saf377xv6v09w5lc20v1fb4i") (f (quote (("test-sbf") ("no-entrypoint"))))))

(define-public crate-solarti-instruction-padding-0.1.5 (c (n "solarti-instruction-padding") (v "0.1.5") (d (list (d (n "miraland-program") (r ">=1.18.2") (d #t) (k 0)) (d (n "miraland-program-test") (r ">=1.18.2") (d #t) (k 2)) (d (n "miraland-sdk") (r ">=1.18.2") (d #t) (k 2)) (d (n "num_enum") (r "^0.7.1") (d #t) (k 0)))) (h "029l047z0x0wzfndqz6fxvq41yp57k4ik4cnwlrmw4xaa1qrjkjs") (f (quote (("test-sbf") ("no-entrypoint"))))))

