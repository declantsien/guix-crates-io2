(define-module (crates-io so la solarti-program-error-derive) #:use-module (crates-io))

(define-public crate-solarti-program-error-derive-0.3.1 (c (n "solarti-program-error-derive") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1wladfr53wvias3v1rvz59vri4d4hcamgjibparlcnjj1ix56jm6")))

(define-public crate-solarti-program-error-derive-0.3.2 (c (n "solarti-program-error-derive") (v "0.3.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1r2gnn32yci7i9f8isn7pwhlvx06ccwm49gf89zp17619sljwnh4")))

(define-public crate-solarti-program-error-derive-0.3.3 (c (n "solarti-program-error-derive") (v "0.3.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0x11vp648yrmf49gb4waxq0f032a8x7iyxjp2qimkfkbkfjqi2gy")))

(define-public crate-solarti-program-error-derive-0.3.4 (c (n "solarti-program-error-derive") (v "0.3.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "18g6qmj8lqx0kywln1iv1f5m069971dnhp0ajqc0hcs6l0s94vad")))

(define-public crate-solarti-program-error-derive-0.3.5 (c (n "solarti-program-error-derive") (v "0.3.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0r2q34y9b2jqwr8fmgzbgincd9cj8rv4p99dgn069i6d6psnz1vs")))

(define-public crate-solarti-program-error-derive-0.3.6 (c (n "solarti-program-error-derive") (v "0.3.6") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1p8zxbvy2gx2nkz0pwddwxpq65k062x6hi1crs9angvgpv88cpbn")))

(define-public crate-solarti-program-error-derive-0.3.7 (c (n "solarti-program-error-derive") (v "0.3.7") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1r4s1y80r8lly5mzpgskcpadhk0m0q50mhfv3wa4gh8xa7ff0hh4")))

