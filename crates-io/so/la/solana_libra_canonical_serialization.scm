(define-module (crates-io so la solana_libra_canonical_serialization) #:use-module (crates-io))

(define-public crate-solana_libra_canonical_serialization-0.0.0-sol7 (c (n "solana_libra_canonical_serialization") (v "0.0.0-sol7") (d (list (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "failure") (r "^0.0.0-sol7") (d #t) (k 0) (p "solana_libra_failure_ext")) (d (n "hex") (r "^0.3") (d #t) (k 2)))) (h "1v2j056pl8gi189dxm58ngkpbifkkc321qh3ymmx1knycqhx4xxq") (y #t)))

(define-public crate-solana_libra_canonical_serialization-0.0.0-sol8 (c (n "solana_libra_canonical_serialization") (v "0.0.0-sol8") (d (list (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "failure") (r "^0.0.0-sol8") (d #t) (k 0) (p "solana_libra_failure_ext")) (d (n "hex") (r "^0.3") (d #t) (k 2)))) (h "0xcvg2hnb6l544mi2vfk13r0gk75hxzvz3b81c8ajhgm82k94l27") (y #t)))

(define-public crate-solana_libra_canonical_serialization-0.0.0-sol9 (c (n "solana_libra_canonical_serialization") (v "0.0.0-sol9") (d (list (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "failure") (r "^0.0.0-sol9") (d #t) (k 0) (p "solana_libra_failure_ext")) (d (n "hex") (r "^0.3") (d #t) (k 2)))) (h "0gy1brmllc7swwcvx3k8ifm2p74bv08i9mw20s1nwcky9j4jra57") (y #t)))

(define-public crate-solana_libra_canonical_serialization-0.0.0-sol10 (c (n "solana_libra_canonical_serialization") (v "0.0.0-sol10") (d (list (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "failure") (r "^0.0.0-sol10") (d #t) (k 0) (p "solana_libra_failure_ext")) (d (n "hex") (r "^0.3") (d #t) (k 2)))) (h "090v8chdp3d2yv43yddr56zq3yzgk8vhbg8q2swgpdfm8895ppnr") (y #t)))

(define-public crate-solana_libra_canonical_serialization-0.0.0-sol13 (c (n "solana_libra_canonical_serialization") (v "0.0.0-sol13") (d (list (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "failure") (r "^0.0.0-sol13") (d #t) (k 0) (p "solana_libra_failure_ext")) (d (n "hex") (r "^0.3") (d #t) (k 2)))) (h "0r515rpshi0mi7nhfzi6sv9x4w932l26cy7wwnv4vnbd9i8i42qb") (y #t)))

(define-public crate-solana_libra_canonical_serialization-0.0.0-sol14 (c (n "solana_libra_canonical_serialization") (v "0.0.0-sol14") (d (list (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "failure") (r "^0.0.0-sol14") (d #t) (k 0) (p "solana_libra_failure_ext")) (d (n "hex") (r "^0.3") (d #t) (k 2)))) (h "1xhv9n37a2chakr0dlynjgqszpi9v1h55ska9sl9i6828jhdbz9k") (y #t)))

(define-public crate-solana_libra_canonical_serialization-0.0.0-sol15 (c (n "solana_libra_canonical_serialization") (v "0.0.0-sol15") (d (list (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "failure") (r "^0.0.0-sol15") (d #t) (k 0) (p "solana_libra_failure_ext")) (d (n "hex") (r "^0.3") (d #t) (k 2)))) (h "058rm5q17g42p74rgwhcbliyvcbhz4pa7zqh7kx49sr5xrb8l13f")))

(define-public crate-solana_libra_canonical_serialization-0.0.0 (c (n "solana_libra_canonical_serialization") (v "0.0.0") (d (list (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "failure") (r "^0.0.0") (d #t) (k 0) (p "solana_libra_failure_ext")) (d (n "hex") (r "^0.3") (d #t) (k 2)))) (h "018j96wr6mdnj5gf7q7px4nm2g6yrah2yj601kbsra0cn9lmgvyn")))

(define-public crate-solana_libra_canonical_serialization-0.0.1-sol3 (c (n "solana_libra_canonical_serialization") (v "0.0.1-sol3") (d (list (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "failure") (r "^0.0.1-sol3") (d #t) (k 0) (p "solana_libra_failure_ext")) (d (n "hex") (r "^0.3") (d #t) (k 2)) (d (n "proptest") (r "^0.9") (d #t) (k 2)) (d (n "proptest-derive") (r "^0.1.1") (d #t) (k 2)))) (h "05avv3h1apb81rf88xjyf2jribqpsh5cizfm7gxw3pmz3p0x4mh3")))

(define-public crate-solana_libra_canonical_serialization-0.0.1-sol4 (c (n "solana_libra_canonical_serialization") (v "0.0.1-sol4") (d (list (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "failure") (r "^0.0.1-sol4") (d #t) (k 0) (p "solana_libra_failure_ext")) (d (n "hex") (r "^0.3") (d #t) (k 2)) (d (n "proptest") (r "^0.9") (d #t) (k 2)) (d (n "proptest-derive") (r "^0.1.1") (d #t) (k 2)))) (h "1ajlb91nhxz3fihyismjzbwf3cvixiihs7sh96mhnyyisafnhx66")))

(define-public crate-solana_libra_canonical_serialization-0.0.1-sol5 (c (n "solana_libra_canonical_serialization") (v "0.0.1-sol5") (d (list (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "failure") (r "^0.0.1-sol5") (d #t) (k 0) (p "solana_libra_failure_ext")) (d (n "hex") (r "^0.3") (d #t) (k 2)) (d (n "proptest") (r "^0.9") (d #t) (k 2)) (d (n "proptest-derive") (r "^0.1.1") (d #t) (k 2)))) (h "07xd88pv5gk3260aa399mkv18nvvgy44fqcqdy484dk2kkck1znl")))

