(define-module (crates-io so la solana_asset_splitter) #:use-module (crates-io))

(define-public crate-solana_asset_splitter-1.0.0 (c (n "solana_asset_splitter") (v "1.0.0") (d (list (d (n "borsh") (r ">=0.9, <0.11") (d #t) (k 0)) (d (n "solana-program") (r "^1.17.10") (d #t) (k 0)) (d (n "solana-program-test") (r ">=1.17.17, <=2") (d #t) (k 2)) (d (n "solana-sdk") (r ">=1.17.17, <=2") (d #t) (k 2)) (d (n "spl-token") (r "^4.0.0") (f (quote ("no-entrypoint"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.52") (d #t) (k 0)))) (h "1aiyaj47plfw41cbfr4layrlm7fbs3rg4k9s8v9h6x6alzhf8yf8") (f (quote (("test-sbf") ("no-entrypoint"))))))

