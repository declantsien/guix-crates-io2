(define-module (crates-io so la solana_data_structs) #:use-module (crates-io))

(define-public crate-solana_data_structs-0.1.0 (c (n "solana_data_structs") (v "0.1.0") (d (list (d (n "rkyv") (r "^0.7.2") (d #t) (k 0)))) (h "1v4wk86p4ikkkrhq0wp505a0si25629prj2kmgfwm57xwm3mn1rm")))

(define-public crate-solana_data_structs-0.1.1 (c (n "solana_data_structs") (v "0.1.1") (d (list (d (n "borsh") (r "^0.9.3") (d #t) (k 0)) (d (n "rkyv") (r "^0.7.2") (d #t) (k 0)))) (h "19hic9ja4zm5l6qqlyxcicyzi08rn92554917qp61hzq7i6llcar")))

(define-public crate-solana_data_structs-0.2.0 (c (n "solana_data_structs") (v "0.2.0") (d (list (d (n "borsh") (r "^0.9.3") (d #t) (k 0)) (d (n "rkyv") (r "^0.7.2") (d #t) (k 0)))) (h "0s3mq3g10s21ir29ga05v919z4cmbh1pfw6wx982mz0w7675v3ah")))

