(define-module (crates-io so la solana-security-txt) #:use-module (crates-io))

(define-public crate-solana-security-txt-0.1.0 (c (n "solana-security-txt") (v "0.1.0") (d (list (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "twoway") (r "^0.2.2") (d #t) (k 0)))) (h "1ydjckm8kvy1whp6nyzd1gxmrnhsqbqglfy5zw0g9hz2v0bf37ak")))

(define-public crate-solana-security-txt-0.1.1 (c (n "solana-security-txt") (v "0.1.1") (d (list (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "twoway") (r "^0.2.2") (d #t) (k 0)))) (h "11i5ys0im2crysclfk06p5147ckh5z2bx1j6y98c0lf9jj0d8sk3")))

(define-public crate-solana-security-txt-0.1.2 (c (n "solana-security-txt") (v "0.1.2") (d (list (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "twoway") (r "^0.2.2") (d #t) (k 0)))) (h "1n38bry5sffvsj473glj1kcl1ihyhhf5yhrnnwzvmf5mzyfgihrh")))

(define-public crate-solana-security-txt-0.1.3 (c (n "solana-security-txt") (v "0.1.3") (d (list (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "twoway") (r "^0.2.2") (d #t) (k 0)))) (h "1l4f0avz3l1xlyaaa59z0w8bsyqzvgy75m88c7hrcijz6zwa52fv")))

(define-public crate-solana-security-txt-0.1.4 (c (n "solana-security-txt") (v "0.1.4") (d (list (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "twoway") (r "^0.2.2") (d #t) (k 0)))) (h "1rakrfi7dgj9cvrpgnk215c8vx2mzgm6qavmbhahyif01ryadcfa")))

(define-public crate-solana-security-txt-0.1.5 (c (n "solana-security-txt") (v "0.1.5") (d (list (d (n "thiserror") (r "=1.0.30") (o #t) (d #t) (k 0)) (d (n "twoway") (r "=0.2.2") (o #t) (d #t) (k 0)))) (h "0vdnmjaf0mvjrjlyviyskd70mw03r7ix6xmlws20708r9sbh7c1m") (f (quote (("parser" "thiserror" "twoway"))))))

(define-public crate-solana-security-txt-1.0.0 (c (n "solana-security-txt") (v "1.0.0") (d (list (d (n "thiserror") (r "=1.0.30") (o #t) (d #t) (k 0)) (d (n "twoway") (r "=0.2.2") (o #t) (d #t) (k 0)))) (h "1al342s1fjkmqixg2a3zw81h0s35kqds9zh6x1bqhnrlsvwdkyi3") (f (quote (("parser" "thiserror" "twoway"))))))

(define-public crate-solana-security-txt-1.0.1 (c (n "solana-security-txt") (v "1.0.1") (d (list (d (n "thiserror") (r "=1.0.30") (o #t) (d #t) (k 0)) (d (n "twoway") (r "=0.2.2") (o #t) (d #t) (k 0)))) (h "1lchqksiw2bag6r1mbsvmdahii6dwv56q8zalxvkjzyz37gqjhgi") (f (quote (("parser" "thiserror" "twoway"))))))

(define-public crate-solana-security-txt-1.0.2 (c (n "solana-security-txt") (v "1.0.2") (d (list (d (n "thiserror") (r "=1.0.30") (o #t) (d #t) (k 0)) (d (n "twoway") (r "=0.2.2") (o #t) (d #t) (k 0)))) (h "1f0s6ihivsm30462fliq4jfc8fm9cxb02fh30g0jbswfjackvsbk") (f (quote (("parser" "thiserror" "twoway"))))))

(define-public crate-solana-security-txt-1.1.0 (c (n "solana-security-txt") (v "1.1.0") (d (list (d (n "thiserror") (r "=1.0.30") (o #t) (d #t) (k 0)) (d (n "twoway") (r "=0.2.2") (o #t) (d #t) (k 0)))) (h "01d6bw6qq2wnmlk2i2k8clnpgcvjajdx0g8b628qb7djmzrn213y") (f (quote (("parser" "thiserror" "twoway"))))))

(define-public crate-solana-security-txt-1.1.1 (c (n "solana-security-txt") (v "1.1.1") (d (list (d (n "thiserror") (r "=1.0.30") (o #t) (d #t) (k 0)) (d (n "twoway") (r "=0.2.2") (o #t) (d #t) (k 0)))) (h "10sifx6mrpcr6hs32vp810bxqc36xcx5qvb8gdxrn7yvgqxs92j6") (f (quote (("parser" "thiserror" "twoway"))))))

