(define-module (crates-io so la solarti-memo) #:use-module (crates-io))

(define-public crate-solarti-memo-1.0.0-rc1 (c (n "solarti-memo") (v "1.0.0-rc1") (d (list (d (n "miraland-program") (r "^1.14.17-rc2") (d #t) (k 0)) (d (n "miraland-sdk") (r "^1.14.17-rc2") (f (quote ("program"))) (k 0)))) (h "07j844s843wp68cnzdlshx40fa6ggc44ymdv8vgbq00nr78217an")))

(define-public crate-solarti-memo-3.0.1 (c (n "solarti-memo") (v "3.0.1") (d (list (d (n "solana-program") (r "^1.14.12") (d #t) (k 0)) (d (n "solana-program-test") (r "^1.14.12") (d #t) (k 2)) (d (n "solana-sdk") (r "^1.14.12") (d #t) (k 2)))) (h "0s4awmgz81bbwrzcviicwaq2bnliwjmcmajqn348s0n468s8xcrc") (f (quote (("test-sbf") ("no-entrypoint"))))))

(define-public crate-solarti-memo-1.0.0 (c (n "solarti-memo") (v "1.0.0") (d (list (d (n "miraland-program") (r "^1.14.17-rc1") (d #t) (k 0)) (d (n "miraland-sdk") (r "^1.14.17-rc1") (f (quote ("program"))) (k 0)))) (h "0qgwfc4cv1kib9qawnlc1jhq5vi1gdlby77d7kj1lr040718fcfb")))

(define-public crate-solarti-memo-3.0.2 (c (n "solarti-memo") (v "3.0.2") (d (list (d (n "miraland-program") (r "^1.14.17-rc3") (d #t) (k 0)) (d (n "miraland-sdk") (r "^1.14.17-rc3") (d #t) (k 2)) (d (n "solana-program-test") (r "=1.14.17") (d #t) (k 2)))) (h "1gwzm703d1lhj0gngsy692sizc1f52j3kk0nqch13hbdl1hg3qdj") (f (quote (("test-sbf") ("no-entrypoint"))))))

(define-public crate-solarti-memo-3.0.3 (c (n "solarti-memo") (v "3.0.3") (d (list (d (n "miraland-program") (r "=1.14.17-rc3") (d #t) (k 0)) (d (n "miraland-sdk") (r "=1.14.17-rc3") (d #t) (k 2)) (d (n "solana-program-test") (r "=1.14.17") (d #t) (k 2)))) (h "0bdvw2r4flhcax3833pmigdqplllj31njsy5mzlrrvma87axdj2a") (f (quote (("test-sbf") ("no-entrypoint"))))))

(define-public crate-solarti-memo-3.0.4 (c (n "solarti-memo") (v "3.0.4") (d (list (d (n "miraland-program") (r "=1.14.17-rc4") (d #t) (k 0)) (d (n "miraland-program-test") (r "=1.14.17-rc4") (d #t) (k 2)) (d (n "miraland-sdk") (r "=1.14.17-rc4") (d #t) (k 2)))) (h "02smrc1bs9ikgrs71xhbz8nqg2b5pavxflb01ipkm6gklx2ajbw6") (f (quote (("test-sbf") ("no-entrypoint"))))))

(define-public crate-solarti-memo-1.0.1 (c (n "solarti-memo") (v "1.0.1") (d (list (d (n "miraland-program") (r "^1.14.17-rc4") (d #t) (k 0)) (d (n "miraland-sdk") (r "^1.14.17-rc4") (f (quote ("program"))) (k 0)))) (h "0irx4phakqf77hxwwlij2877vcqjp5b90bdp15aglfc9bx8imxbg")))

(define-public crate-solarti-memo-3.0.5 (c (n "solarti-memo") (v "3.0.5") (d (list (d (n "miraland-program") (r "=1.14.17-rc5") (d #t) (k 0)) (d (n "miraland-program-test") (r "=1.14.17-rc5") (d #t) (k 2)) (d (n "miraland-sdk") (r "=1.14.17-rc5") (d #t) (k 2)))) (h "0q6nfpg6v23cjjz6z6xk20caspjgal8c7hi8p3xpqz950cvc6fm6") (f (quote (("test-sbf") ("no-entrypoint"))))))

(define-public crate-solarti-memo-3.0.6 (c (n "solarti-memo") (v "3.0.6") (d (list (d (n "miraland-program") (r "=1.14.18") (d #t) (k 0)) (d (n "miraland-program-test") (r "=1.14.18") (d #t) (k 2)) (d (n "miraland-sdk") (r "=1.14.18") (d #t) (k 2)))) (h "19vfysd1aiacy1q48lcax3hbl7dkgx0gq5hvj9hczjd8pwyap85j") (f (quote (("test-sbf") ("no-entrypoint"))))))

(define-public crate-solarti-memo-3.0.7 (c (n "solarti-memo") (v "3.0.7") (d (list (d (n "miraland-program") (r "^1.14.18") (d #t) (k 0)) (d (n "miraland-program-test") (r "^1.14.18") (d #t) (k 2)) (d (n "miraland-sdk") (r "^1.14.18") (d #t) (k 2)))) (h "00ppwz6f27ivsm2kfbcd73rpczr4xpyz9hfjs8rhxfgkl7fxmv54") (f (quote (("test-sbf") ("no-entrypoint"))))))

(define-public crate-solarti-memo-4.0.0 (c (n "solarti-memo") (v "4.0.0") (d (list (d (n "miraland-program") (r "^1.14.18") (d #t) (k 0)) (d (n "miraland-program-test") (r "^1.14.18") (d #t) (k 2)) (d (n "miraland-sdk") (r "^1.14.18") (d #t) (k 2)))) (h "1vbwz9airbmqysl3m9fvlkmawrfc219wpnm92h80n0dvaddmdw29") (f (quote (("test-sbf") ("no-entrypoint"))))))

(define-public crate-solarti-memo-4.0.1 (c (n "solarti-memo") (v "4.0.1") (d (list (d (n "miraland-program") (r "^1.18.2") (d #t) (k 0)) (d (n "miraland-program-test") (r "^1.18.2") (d #t) (k 2)) (d (n "miraland-sdk") (r "^1.18.2") (d #t) (k 2)))) (h "0qx34hbr4kig3a3iima43s5vjiwlcl6blyk6gv8yg57ln23f3g61") (f (quote (("test-sbf") ("no-entrypoint"))))))

(define-public crate-solarti-memo-4.0.2 (c (n "solarti-memo") (v "4.0.2") (d (list (d (n "miraland-program") (r ">=1.18.2") (d #t) (k 0)) (d (n "miraland-program-test") (r ">=1.18.2") (d #t) (k 2)) (d (n "miraland-sdk") (r ">=1.18.2") (d #t) (k 2)))) (h "01kvmjvqs2ick95m9n79q7ik707486xhz4q9fr1ik62z0pxvk8a2") (f (quote (("test-sbf") ("no-entrypoint"))))))

