(define-module (crates-io so la solalumin-attribute-event) #:use-module (crates-io))

(define-public crate-solalumin-attribute-event-0.1.0-alpha.1 (c (n "solalumin-attribute-event") (v "0.1.0-alpha.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "borsh") (r ">=0.9.3, <=0.10.3") (d #t) (k 0)) (d (n "borsh-derive") (r ">=0.9.3, <=0.10.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "solalumin-state") (r "^0.1.0-alpha.1") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("full"))) (d #t) (k 0)) (d (n "version") (r "^3.0.0") (d #t) (k 0)))) (h "0097pm0g6vl64ha7l3vvnvj2qm7pj74y1srpygz9b9y51ghd9y1q") (r "1.60")))

