(define-module (crates-io so la solana_libra_build_helpers) #:use-module (crates-io))

(define-public crate-solana_libra_build_helpers-0.0.0-sol7 (c (n "solana_libra_build_helpers") (v "0.0.0-sol7") (d (list (d (n "grpcio-client") (r "^0.0.0-sol7") (d #t) (k 0) (p "solana_libra_grpcio-client")) (d (n "protoc-grpcio") (r "^0.3.1") (d #t) (k 0)) (d (n "walkdir") (r "^2.2.8") (d #t) (k 0)))) (h "1y0fd0c1mxz0p2m6gab7n5rvmiscjm8p59jz8p3sy5y0iij33w30") (y #t)))

(define-public crate-solana_libra_build_helpers-0.0.0-sol8 (c (n "solana_libra_build_helpers") (v "0.0.0-sol8") (d (list (d (n "grpcio-client") (r "^0.0.0-sol8") (d #t) (k 0) (p "solana_libra_grpcio-client")) (d (n "protoc-grpcio") (r "^0.3.1") (d #t) (k 0)) (d (n "walkdir") (r "^2.2.8") (d #t) (k 0)))) (h "0nnm3camrbvmfjvjkf75z1sqdwwnaqmsg6inc4kv48wpx9vm7czq") (y #t)))

(define-public crate-solana_libra_build_helpers-0.0.0-sol9 (c (n "solana_libra_build_helpers") (v "0.0.0-sol9") (d (list (d (n "grpcio-client") (r "^0.0.0-sol9") (d #t) (k 0) (p "solana_libra_grpcio-client")) (d (n "protoc-grpcio") (r "^0.3.1") (d #t) (k 0)) (d (n "walkdir") (r "^2.2.8") (d #t) (k 0)))) (h "1h5sr3m14lrl6air98cr2kfpnqrbn0fs74293i66yhk8x5xlwvkp") (y #t)))

(define-public crate-solana_libra_build_helpers-0.0.0-sol10 (c (n "solana_libra_build_helpers") (v "0.0.0-sol10") (d (list (d (n "grpcio-client") (r "^0.0.0-sol10") (d #t) (k 0) (p "solana_libra_grpcio-client")) (d (n "protoc-grpcio") (r "^0.3.1") (d #t) (k 0)) (d (n "walkdir") (r "^2.2.8") (d #t) (k 0)))) (h "03c54rnrmqrgg0xi7y55d813glghbc3832pckxys37s6wcfa0p0z") (y #t)))

(define-public crate-solana_libra_build_helpers-0.0.0-sol12 (c (n "solana_libra_build_helpers") (v "0.0.0-sol12") (d (list (d (n "grpcio-client") (r "^0.0.0-sol12") (d #t) (k 0) (p "solana_libra_grpcio-client")) (d (n "protoc-grpcio") (r "^0.3.1") (d #t) (k 0)) (d (n "walkdir") (r "^2.2.8") (d #t) (k 0)))) (h "05fzl82f3kf0k4ka64rgn7xhnv1rfiak0hr3n3a50kipz9jxrb26") (y #t)))

(define-public crate-solana_libra_build_helpers-0.0.0-sol13 (c (n "solana_libra_build_helpers") (v "0.0.0-sol13") (d (list (d (n "grpcio-client") (r "^0.0.0-sol13") (d #t) (k 0) (p "solana_libra_grpcio-client")) (d (n "protoc-grpcio") (r "^0.3.1") (d #t) (k 0)) (d (n "walkdir") (r "^2.2.8") (d #t) (k 0)))) (h "1w4d7iakq5gdd84b43zjw9hfiaav6q1m6rphczxzwbp1w5apm9rp") (y #t)))

(define-public crate-solana_libra_build_helpers-0.0.0-sol14 (c (n "solana_libra_build_helpers") (v "0.0.0-sol14") (d (list (d (n "grpcio-client") (r "^0.0.0-sol14") (d #t) (k 0) (p "solana_libra_grpcio-client")) (d (n "protoc-grpcio") (r "^0.3.1") (d #t) (k 0)) (d (n "walkdir") (r "^2.2.8") (d #t) (k 0)))) (h "1qrwmrjrzg58n1zs3smsjsscg1625wbrys5424qqagjhj9zm74kd") (y #t)))

(define-public crate-solana_libra_build_helpers-0.0.0-sol15 (c (n "solana_libra_build_helpers") (v "0.0.0-sol15") (d (list (d (n "grpcio-client") (r "^0.0.0-sol15") (d #t) (k 0) (p "solana_libra_grpcio-client")) (d (n "protoc-grpcio") (r "^0.3.1") (d #t) (k 0)) (d (n "walkdir") (r "^2.2.8") (d #t) (k 0)))) (h "0npyhhhqbv9fj9g2v5jx82kc6aj9gi132qfglyf5k1vmlb7p1pbv")))

(define-public crate-solana_libra_build_helpers-0.0.0 (c (n "solana_libra_build_helpers") (v "0.0.0") (d (list (d (n "grpcio-client") (r "^0.0.0") (d #t) (k 0) (p "solana_libra_grpcio-client")) (d (n "protoc-grpcio") (r "^0.3.1") (d #t) (k 0)) (d (n "walkdir") (r "^2.2.8") (d #t) (k 0)))) (h "035yf0zzia5zlnnwip6n1b6jw0crmb2mj0z193y90vifdzr0lj50")))

