(define-module (crates-io so la solana-native-loader) #:use-module (crates-io))

(define-public crate-solana-native-loader-0.11.0 (c (n "solana-native-loader") (v "0.11.0") (d (list (d (n "bincode") (r "^1.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.45") (d #t) (k 0)) (d (n "libloading") (r "^0.5.0") (d #t) (k 0)) (d (n "log") (r "^0.4.2") (d #t) (k 0)) (d (n "solana-sdk") (r "^0.11.0") (d #t) (k 0)))) (h "052y06zxi6yw8kc0s1xac4y3ri089z5p46zw8fb9r0jw2m9b97r9")))

