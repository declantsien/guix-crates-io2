(define-module (crates-io so la solarti-binary-oracle-pair) #:use-module (crates-io))

(define-public crate-solarti-binary-oracle-pair-0.1.0 (c (n "solarti-binary-oracle-pair") (v "0.1.0") (d (list (d (n "borsh") (r "^0.9.1") (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "solana-program") (r "^1.14.12") (d #t) (k 0)) (d (n "solana-program-test") (r "^1.14.12") (d #t) (k 2)) (d (n "solana-sdk") (r "^1.14.12") (d #t) (k 2)) (d (n "solarti-token") (r "^3.5") (f (quote ("no-entrypoint"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "uint") (r "^0.9") (d #t) (k 0)))) (h "0xvf88w3fwkh5sqmcd9310fwr8w8314zl6d34gz4c1cz8sn6g95i") (f (quote (("test-sbf"))))))

