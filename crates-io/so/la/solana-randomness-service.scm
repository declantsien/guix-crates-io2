(define-module (crates-io so la solana-randomness-service) #:use-module (crates-io))

(define-public crate-solana-randomness-service-0.0.1 (c (n "solana-randomness-service") (v "0.0.1") (d (list (d (n "anchor-lang") (r "^0.29.0") (d #t) (k 0)) (d (n "anchor-spl") (r "^0.29.0") (d #t) (k 0)) (d (n "switchboard-solana") (r "^0.29.92") (d #t) (k 0)))) (h "1gpk0d6f39la4s207kvrs43jyjr18gz91nabq250a6403jz6y72p") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint") ("all"))))))

(define-public crate-solana-randomness-service-0.0.2 (c (n "solana-randomness-service") (v "0.0.2") (d (list (d (n "anchor-lang") (r "^0.29.0") (d #t) (k 0)) (d (n "anchor-spl") (r "^0.29.0") (d #t) (k 0)) (d (n "switchboard-solana") (r "^0.29.92") (d #t) (k 0)))) (h "053rhlpbg5zm2azjgbjag8c6wrablg2zw6pc2nvkgdmdd27bazwz") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint") ("all"))))))

(define-public crate-solana-randomness-service-0.0.3 (c (n "solana-randomness-service") (v "0.0.3") (d (list (d (n "anchor-lang") (r "^0.29.0") (d #t) (k 0)) (d (n "anchor-spl") (r "^0.29.0") (d #t) (k 0)) (d (n "switchboard-solana") (r "^0.29.92") (d #t) (k 0)))) (h "0zn1jjggvgamq92nv9wx1748n4ipzk52m0dfg6xxdj5kw79xvzmm") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint") ("all"))))))

(define-public crate-solana-randomness-service-0.0.4 (c (n "solana-randomness-service") (v "0.0.4") (d (list (d (n "anchor-lang") (r "^0.29.0") (d #t) (k 0)) (d (n "anchor-spl") (r "^0.29.0") (d #t) (k 0)) (d (n "switchboard-solana") (r "^0.29.92") (d #t) (k 0)))) (h "0aq67hrcwwcci93zvv4i1lmivgx1yh859sc3irphwz90yirvdvc7") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint") ("all"))))))

(define-public crate-solana-randomness-service-1.0.0 (c (n "solana-randomness-service") (v "1.0.0") (d (list (d (n "anchor-lang") (r "^0.29.0") (d #t) (k 0)) (d (n "anchor-spl") (r "^0.29.0") (d #t) (k 0)) (d (n "switchboard-solana") (r "^0.29.92") (d #t) (k 0)))) (h "1i4zng8drs62f8f1wg7lj1wgjfk3l8ck09hhv94bawl1sfh1fyh9") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint") ("all"))))))

(define-public crate-solana-randomness-service-1.0.1 (c (n "solana-randomness-service") (v "1.0.1") (d (list (d (n "anchor-lang") (r "^0.29.0") (d #t) (k 0)) (d (n "anchor-spl") (r "^0.29.0") (d #t) (k 0)) (d (n "switchboard-solana") (r "^0.29.98") (d #t) (k 0)))) (h "1jpsdvdwjp4c7nhjj8gfsadgfgd5lpwzmnrk1z0d1kn86d1nzlyd") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint") ("all"))))))

(define-public crate-solana-randomness-service-1.0.2 (c (n "solana-randomness-service") (v "1.0.2") (d (list (d (n "anchor-lang") (r "^0.29.0") (d #t) (k 0)) (d (n "anchor-spl") (r "^0.29.0") (d #t) (k 0)) (d (n "switchboard-solana") (r "^0.29.98") (d #t) (k 0)))) (h "1q41azb2a93ji2xjp0wyj8yv5j5iqrn9sxg6smz1c5931yj8brjg") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint") ("all"))))))

