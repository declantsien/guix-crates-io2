(define-module (crates-io so la solana-reed-solomon-erasure) #:use-module (crates-io))

(define-public crate-solana-reed-solomon-erasure-3.1.1 (c (n "solana-reed-solomon-erasure") (v "3.1.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "quickcheck") (r "^0.7") (d #t) (k 2)) (d (n "rand") (r "^0.5.4") (d #t) (k 2)) (d (n "rayon") (r "^1.0") (d #t) (k 0)) (d (n "smallvec") (r "^0.6") (d #t) (k 0)))) (h "1n3kjy5hrlx87gs92lijf0251ds3vdrsagn6l6agb2a8wcm6cj3r") (f (quote (("pure-rust"))))))

(define-public crate-solana-reed-solomon-erasure-4.0.1 (c (n "solana-reed-solomon-erasure") (v "4.0.1") (d (list (d (n "cc") (r "^1.0") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "rand") (r "^0.7.2") (d #t) (k 2)) (d (n "smallvec") (r "^0.6") (d #t) (k 0)))) (h "1bh77y2cayyc54dlmd56cxqzzgr16i07q1vv7psd36pg839ivl92") (f (quote (("simd-accel" "cc" "libc") ("default")))) (y #t)))

(define-public crate-solana-reed-solomon-erasure-4.0.1-1 (c (n "solana-reed-solomon-erasure") (v "4.0.1-1") (d (list (d (n "cc") (r "^1.0") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "rand") (r "^0.7.2") (d #t) (k 2)) (d (n "smallvec") (r "^0.6") (d #t) (k 0)))) (h "089qvklgp1cd29bki3r4795cxf30sg5h1cnh1v9sn2ag8v46hpms") (f (quote (("simd-accel" "cc" "libc") ("default")))) (y #t)))

(define-public crate-solana-reed-solomon-erasure-4.0.1-2 (c (n "solana-reed-solomon-erasure") (v "4.0.1-2") (d (list (d (n "cc") (r "^1.0") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "rand") (r "^0.7.2") (d #t) (k 2)) (d (n "smallvec") (r "^0.6") (d #t) (k 0)))) (h "0cg5g6lcgrl8355nbajd25h4hglj7fa8lg0axf21bj3pzxrq038y") (f (quote (("simd-accel" "cc" "libc") ("default")))) (y #t)))

(define-public crate-solana-reed-solomon-erasure-4.0.1-3 (c (n "solana-reed-solomon-erasure") (v "4.0.1-3") (d (list (d (n "cc") (r "^1.0") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "rand") (r "^0.7.2") (d #t) (k 2)) (d (n "smallvec") (r "^0.6") (d #t) (k 0)))) (h "17irfcz29zi6mk62v95d7bnhdg4w55ryxl6hly3zcani9lzspcym") (f (quote (("simd-accel" "cc" "libc") ("default"))))))

