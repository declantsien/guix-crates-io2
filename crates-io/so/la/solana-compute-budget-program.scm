(define-module (crates-io so la solana-compute-budget-program) #:use-module (crates-io))

(define-public crate-solana-compute-budget-program-1.8.0 (c (n "solana-compute-budget-program") (v "1.8.0") (d (list (d (n "solana-sdk") (r "=1.8.0") (d #t) (k 0)))) (h "063pk11x8vykjbn354ax6ny7rrh3y9270hs8pr6xpcmsmkqbjyqp")))

(define-public crate-solana-compute-budget-program-1.8.1 (c (n "solana-compute-budget-program") (v "1.8.1") (d (list (d (n "solana-sdk") (r "=1.8.1") (d #t) (k 0)))) (h "19wx9wi75la0738zvlwd5hyzziylwviybn7b4jn6askgygwymyy1")))

(define-public crate-solana-compute-budget-program-1.8.2 (c (n "solana-compute-budget-program") (v "1.8.2") (d (list (d (n "solana-sdk") (r "=1.8.2") (d #t) (k 0)))) (h "1g64l3zd4381ln3rzm9x25mfdbd9mk4hk015v1dpiblhcaq245wx")))

(define-public crate-solana-compute-budget-program-1.8.3 (c (n "solana-compute-budget-program") (v "1.8.3") (d (list (d (n "solana-sdk") (r "=1.8.3") (d #t) (k 0)))) (h "063adhgzxhwgyl3iddls7py6c7kwqbc74s75yqxdbaqq0dxyw60s")))

(define-public crate-solana-compute-budget-program-1.8.4 (c (n "solana-compute-budget-program") (v "1.8.4") (d (list (d (n "solana-sdk") (r "=1.8.4") (d #t) (k 0)))) (h "04mlbljv11xwq7iwa925gh38bwnzf3dk3g3vca3x5q691hbxpf9v")))

(define-public crate-solana-compute-budget-program-1.8.5 (c (n "solana-compute-budget-program") (v "1.8.5") (d (list (d (n "solana-sdk") (r "=1.8.5") (d #t) (k 0)))) (h "1ijm4v7csjk3bbfjd238h7xxjv4iw2m0shy274y6d02p7zn7dkm3")))

(define-public crate-solana-compute-budget-program-1.8.6 (c (n "solana-compute-budget-program") (v "1.8.6") (d (list (d (n "solana-sdk") (r "=1.8.6") (d #t) (k 0)))) (h "1c9lfhy96fv2m7pbmpahhjqmqh3hzs4m2wrxbvcg107c5wp6fbb7")))

(define-public crate-solana-compute-budget-program-1.8.7 (c (n "solana-compute-budget-program") (v "1.8.7") (d (list (d (n "solana-sdk") (r "=1.8.7") (d #t) (k 0)))) (h "1bpagz3flmksh5x49rrpfqzg56ry9qi3fxcwgz10py5qskb3xn7v")))

(define-public crate-solana-compute-budget-program-1.8.8 (c (n "solana-compute-budget-program") (v "1.8.8") (d (list (d (n "solana-sdk") (r "=1.8.8") (d #t) (k 0)))) (h "0lhx15jmb5vp7b6qzxg6i8rfffaxvwiafd2dlsq9bf7i4c7ryjvg")))

(define-public crate-solana-compute-budget-program-1.8.9 (c (n "solana-compute-budget-program") (v "1.8.9") (d (list (d (n "solana-sdk") (r "=1.8.9") (d #t) (k 0)))) (h "0gyq0n379m75kcb6faxqfz1znrbaflwa6aizfi7xm6w6n972fi1m")))

(define-public crate-solana-compute-budget-program-1.9.0 (c (n "solana-compute-budget-program") (v "1.9.0") (d (list (d (n "solana-program-runtime") (r "=1.9.0") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.9.0") (d #t) (k 0)))) (h "0dzly5dhx7l7c0b9fl0xa55cyblg21yc5k7in4dd0makyi7a77j1")))

(define-public crate-solana-compute-budget-program-1.8.10 (c (n "solana-compute-budget-program") (v "1.8.10") (d (list (d (n "solana-sdk") (r "=1.8.10") (d #t) (k 0)))) (h "1j605lzxhw5fbdlrf11drjms0xn68avqqsbi69yhaxmc91b67p4w")))

(define-public crate-solana-compute-budget-program-1.8.11 (c (n "solana-compute-budget-program") (v "1.8.11") (d (list (d (n "solana-sdk") (r "=1.8.11") (d #t) (k 0)))) (h "1l23r8czbjx5m0gzbfwzwancgzz2hgf73adk5121dp1ar9q5a3ri")))

(define-public crate-solana-compute-budget-program-1.9.1 (c (n "solana-compute-budget-program") (v "1.9.1") (d (list (d (n "solana-program-runtime") (r "=1.9.1") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.9.1") (d #t) (k 0)))) (h "1a458vldqk9d8mzdi5khslp2c7sx5yyqp6j9qnbdsg92mw9a6bry")))

(define-public crate-solana-compute-budget-program-1.9.2 (c (n "solana-compute-budget-program") (v "1.9.2") (d (list (d (n "solana-program-runtime") (r "=1.9.2") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.9.2") (d #t) (k 0)))) (h "1f60bxqp7fbf4i3lp5m5smggyq0g6jnk918v01bfags3ysab1q7c")))

(define-public crate-solana-compute-budget-program-1.9.3 (c (n "solana-compute-budget-program") (v "1.9.3") (d (list (d (n "solana-program-runtime") (r "=1.9.3") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.9.3") (d #t) (k 0)))) (h "0cbgy9wi58rn55v68zbcjfq3i6najbp201qh2x77vl9jffs73n8r")))

(define-public crate-solana-compute-budget-program-1.8.12 (c (n "solana-compute-budget-program") (v "1.8.12") (d (list (d (n "solana-sdk") (r "=1.8.12") (d #t) (k 0)))) (h "155lls8c5a420rfdr3k8l3kl8q58l11sxkx5ij04yr789z97067p")))

(define-public crate-solana-compute-budget-program-1.9.4 (c (n "solana-compute-budget-program") (v "1.9.4") (d (list (d (n "solana-program-runtime") (r "=1.9.4") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.9.4") (d #t) (k 0)))) (h "07898fl993bzybfbng34rzl2gpfxlj15mah0l70v39xs8vg5s9sc")))

(define-public crate-solana-compute-budget-program-1.8.13 (c (n "solana-compute-budget-program") (v "1.8.13") (d (list (d (n "solana-sdk") (r "=1.8.13") (d #t) (k 0)))) (h "0kqw79ydxxvixkv6fqw610fhic477l0yxj1zgmnlq5lnpkwn7dl9")))

(define-public crate-solana-compute-budget-program-1.9.5 (c (n "solana-compute-budget-program") (v "1.9.5") (d (list (d (n "solana-program-runtime") (r "=1.9.5") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.9.5") (d #t) (k 0)))) (h "0klvaxrv6vdvxgw70q8hvvl6ga2amw0i177lc829vwgp0r99bi33")))

(define-public crate-solana-compute-budget-program-1.8.14 (c (n "solana-compute-budget-program") (v "1.8.14") (d (list (d (n "solana-sdk") (r "=1.8.14") (d #t) (k 0)))) (h "0xxsb0dsrw1v7ihk7sd5i97w7gpqfv56pkl4jdmppfss4iyvmfq3")))

(define-public crate-solana-compute-budget-program-1.9.6 (c (n "solana-compute-budget-program") (v "1.9.6") (d (list (d (n "solana-program-runtime") (r "=1.9.6") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.9.6") (d #t) (k 0)))) (h "1lf3mcinjv35marmx94iw5mjkb8ard1lyf2ff380pf0gvzni7r9r")))

(define-public crate-solana-compute-budget-program-1.9.7 (c (n "solana-compute-budget-program") (v "1.9.7") (d (list (d (n "solana-program-runtime") (r "=1.9.7") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.9.7") (d #t) (k 0)))) (h "1i0w5n6gkvfcl4j81ixbpf50yl94961ykvy9irh67a0l7l2q4rrv")))

(define-public crate-solana-compute-budget-program-1.8.16 (c (n "solana-compute-budget-program") (v "1.8.16") (d (list (d (n "solana-sdk") (r "=1.8.16") (d #t) (k 0)))) (h "1ji0jlk9gywrc8n3vpvalipgplqmqw9gmp9fr0f91qgvf1qkfm1i")))

(define-public crate-solana-compute-budget-program-1.9.8 (c (n "solana-compute-budget-program") (v "1.9.8") (d (list (d (n "solana-program-runtime") (r "=1.9.8") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.9.8") (d #t) (k 0)))) (h "17d6fm930d2z40y91m9mf5fl1rr9f080qrwkf2cr87g7mp41rgmr")))

(define-public crate-solana-compute-budget-program-1.9.9 (c (n "solana-compute-budget-program") (v "1.9.9") (d (list (d (n "solana-program-runtime") (r "=1.9.9") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.9.9") (d #t) (k 0)))) (h "13wnkvmplzyjh1kvw3i6al8m41prrw9kddbvqj239mjzry0gg53v")))

(define-public crate-solana-compute-budget-program-1.10.0 (c (n "solana-compute-budget-program") (v "1.10.0") (d (list (d (n "solana-program-runtime") (r "=1.10.0") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.10.0") (d #t) (k 0)))) (h "1a9xm5dp1cz15n598gx96fgkqxnxnb3b5djdxlzxc73qrw2rfbk3")))

(define-public crate-solana-compute-budget-program-1.9.10 (c (n "solana-compute-budget-program") (v "1.9.10") (d (list (d (n "solana-program-runtime") (r "=1.9.10") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.9.10") (d #t) (k 0)))) (h "0v81wyb1grw7axzfarv16fbvnyscjp1p24g2baqhdql2rm4wnkdg")))

(define-public crate-solana-compute-budget-program-1.9.11 (c (n "solana-compute-budget-program") (v "1.9.11") (d (list (d (n "solana-program-runtime") (r "=1.9.11") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.9.11") (d #t) (k 0)))) (h "0cnd84j24n0lrb81d21zkv6a6xpmgd401jiawpf898a26bqrq6x3")))

(define-public crate-solana-compute-budget-program-1.9.12 (c (n "solana-compute-budget-program") (v "1.9.12") (d (list (d (n "solana-program-runtime") (r "=1.9.12") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.9.12") (d #t) (k 0)))) (h "03m3qss2ny1zawfqdsnwirj2330kyxy5hgvmqgkny2x8zi2vk6b6")))

(define-public crate-solana-compute-budget-program-1.10.1 (c (n "solana-compute-budget-program") (v "1.10.1") (d (list (d (n "solana-program-runtime") (r "=1.10.1") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.10.1") (d #t) (k 0)))) (h "07s29q5hsqqjd0hvykkxcshfk3vc0pcif5w97pdiz2abqqmbi78r")))

(define-public crate-solana-compute-budget-program-1.10.2 (c (n "solana-compute-budget-program") (v "1.10.2") (d (list (d (n "solana-program-runtime") (r "=1.10.2") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.10.2") (d #t) (k 0)))) (h "1lzkjln95jhmc2vw87vijhqbpckxhdspa273zh7g16wr4k5d4f2p")))

(define-public crate-solana-compute-budget-program-1.9.13 (c (n "solana-compute-budget-program") (v "1.9.13") (d (list (d (n "solana-program-runtime") (r "=1.9.13") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.9.13") (d #t) (k 0)))) (h "182hmvxhl9h5pb38xnhdw8i3v9l02qc90xn8nn94l4qn15mvzcy9")))

(define-public crate-solana-compute-budget-program-1.10.3 (c (n "solana-compute-budget-program") (v "1.10.3") (d (list (d (n "solana-program-runtime") (r "=1.10.3") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.10.3") (d #t) (k 0)))) (h "1krlg2k263ax1hajpvnf9q0w4ssbg3cjqwxqc4mm1frn1fnzm7ib")))

(define-public crate-solana-compute-budget-program-1.9.14 (c (n "solana-compute-budget-program") (v "1.9.14") (d (list (d (n "solana-program-runtime") (r "=1.9.14") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.9.14") (d #t) (k 0)))) (h "0raalswajxiv8zhw8ick227qcy5rlgk6v16k1wc362xanwvnqmyn")))

(define-public crate-solana-compute-budget-program-1.10.4 (c (n "solana-compute-budget-program") (v "1.10.4") (d (list (d (n "solana-program-runtime") (r "=1.10.4") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.10.4") (d #t) (k 0)))) (h "0b739b038dk7hjf5bqda372k2l3j8d0kd6dx416v9qgv2a9rg8rr")))

(define-public crate-solana-compute-budget-program-1.10.5 (c (n "solana-compute-budget-program") (v "1.10.5") (d (list (d (n "solana-program-runtime") (r "=1.10.5") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.10.5") (d #t) (k 0)))) (h "0cnryaf9pik1x9j0f6vvr7z6jjdbwy8j8bqrs6qa5g5n9z3zavm8")))

(define-public crate-solana-compute-budget-program-1.10.6 (c (n "solana-compute-budget-program") (v "1.10.6") (d (list (d (n "solana-program-runtime") (r "=1.10.6") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.10.6") (d #t) (k 0)))) (h "14b6z3b11n7rh8c89jq8m807ll3pg1dvf2pw05y5q393afsrhnym")))

(define-public crate-solana-compute-budget-program-1.9.15 (c (n "solana-compute-budget-program") (v "1.9.15") (d (list (d (n "solana-program-runtime") (r "=1.9.15") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.9.15") (d #t) (k 0)))) (h "12l8g1jzd5fh5w5xk41ssk35wx6c30agkxm2fklxjwqmypcwyliz")))

(define-public crate-solana-compute-budget-program-1.10.7 (c (n "solana-compute-budget-program") (v "1.10.7") (d (list (d (n "solana-program-runtime") (r "=1.10.7") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.10.7") (d #t) (k 0)))) (h "0hm0ikng1hp1b9ajg6qcb0yihjdj2nv0axiylfh1sn7222imq7mh")))

(define-public crate-solana-compute-budget-program-1.10.8 (c (n "solana-compute-budget-program") (v "1.10.8") (d (list (d (n "solana-program-runtime") (r "=1.10.8") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.10.8") (d #t) (k 0)))) (h "1j8ak8viabdlv8yxaa297ns51fwrzmc7cqabvlmbqbdimjrdh2r8")))

(define-public crate-solana-compute-budget-program-1.9.16 (c (n "solana-compute-budget-program") (v "1.9.16") (d (list (d (n "solana-program-runtime") (r "=1.9.16") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.9.16") (d #t) (k 0)))) (h "0jrywvfjn7wzkq158qlqwss1qvn6xhpy59amrgga3cilfbi56ia2")))

(define-public crate-solana-compute-budget-program-1.9.17 (c (n "solana-compute-budget-program") (v "1.9.17") (d (list (d (n "solana-program-runtime") (r "=1.9.17") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.9.17") (d #t) (k 0)))) (h "013nxkabz3irvw94d9xz6w1xfv15wg72hk0yabvy1ldb9gn2gjgh")))

(define-public crate-solana-compute-budget-program-1.10.9 (c (n "solana-compute-budget-program") (v "1.10.9") (d (list (d (n "solana-program-runtime") (r "=1.10.9") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.10.9") (d #t) (k 0)))) (h "02vvy1k2w3knpqkmxj6r0f4v4n82jyyd3sd47cla8hlabkh5y26b")))

(define-public crate-solana-compute-budget-program-1.9.18 (c (n "solana-compute-budget-program") (v "1.9.18") (d (list (d (n "solana-program-runtime") (r "=1.9.18") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.9.18") (d #t) (k 0)))) (h "1czd33iskyibsck052asfg6jl1wz51y9ggbydmkcz3xxzygxa52b")))

(define-public crate-solana-compute-budget-program-1.10.10 (c (n "solana-compute-budget-program") (v "1.10.10") (d (list (d (n "solana-program-runtime") (r "=1.10.10") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.10.10") (d #t) (k 0)))) (h "1i62yxr3xj1clmyaxmxbhslvs824ldxpbh65v4gizfpci21rwij6")))

(define-public crate-solana-compute-budget-program-1.10.11 (c (n "solana-compute-budget-program") (v "1.10.11") (d (list (d (n "solana-program-runtime") (r "=1.10.11") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.10.11") (d #t) (k 0)))) (h "1yzlmh9sdy451hixi4yq3kwfd1h3bp21ypns4ma94zcw7nyizwkc")))

(define-public crate-solana-compute-budget-program-1.9.19 (c (n "solana-compute-budget-program") (v "1.9.19") (d (list (d (n "solana-program-runtime") (r "=1.9.19") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.9.19") (d #t) (k 0)))) (h "0rzsqm70vf1zc22sqnnxfcsg2bhnpn05s9wwnj9pnqr1ipdyir4r")))

(define-public crate-solana-compute-budget-program-1.10.12 (c (n "solana-compute-budget-program") (v "1.10.12") (d (list (d (n "solana-program-runtime") (r "=1.10.12") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.10.12") (d #t) (k 0)))) (h "0zlvlkzl9pps3lcipg502l1kqvidvizvpkahc2mdawsqprymnhz5")))

(define-public crate-solana-compute-budget-program-1.9.20 (c (n "solana-compute-budget-program") (v "1.9.20") (d (list (d (n "solana-program-runtime") (r "=1.9.20") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.9.20") (d #t) (k 0)))) (h "07l9kymyy10jn6a0zkfb5qmi3b9n6384ayvz71j5pfzn4716fmb8")))

(define-public crate-solana-compute-budget-program-1.10.13 (c (n "solana-compute-budget-program") (v "1.10.13") (d (list (d (n "solana-program-runtime") (r "=1.10.13") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.10.13") (d #t) (k 0)))) (h "132wa81jxr68l1b19wc2cjprs1srb2dpkpmdvgx8aahbj1hh4smk")))

(define-public crate-solana-compute-budget-program-1.9.21 (c (n "solana-compute-budget-program") (v "1.9.21") (d (list (d (n "solana-program-runtime") (r "=1.9.21") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.9.21") (d #t) (k 0)))) (h "0rvjhrwxxs9540vqr0hk0qdpmr1n2v9i350bmwlz0w16an5ygj13")))

(define-public crate-solana-compute-budget-program-1.10.14 (c (n "solana-compute-budget-program") (v "1.10.14") (d (list (d (n "solana-program-runtime") (r "=1.10.14") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.10.14") (d #t) (k 0)))) (h "1nr0sdyi2mr3zk7fmcb5zpl3fz6ifz35lkj7k2vkmyhsbrjajcnz")))

(define-public crate-solana-compute-budget-program-1.9.22 (c (n "solana-compute-budget-program") (v "1.9.22") (d (list (d (n "solana-program-runtime") (r "=1.9.22") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.9.22") (d #t) (k 0)))) (h "1ka761aw6jrzwn7mxlxdzgwd2l24jbn6fr8a7361gjl4if52kckp")))

(define-public crate-solana-compute-budget-program-1.10.15 (c (n "solana-compute-budget-program") (v "1.10.15") (d (list (d (n "solana-program-runtime") (r "=1.10.15") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.10.15") (d #t) (k 0)))) (h "1yhg4p0ica2g835k948wvyj8g9d5dv6g77hrylj023s0ghwc8sis")))

(define-public crate-solana-compute-budget-program-1.10.16 (c (n "solana-compute-budget-program") (v "1.10.16") (d (list (d (n "solana-program-runtime") (r "=1.10.16") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.10.16") (d #t) (k 0)))) (h "14pllk0c69nlfbrxi4fda3wjbrmkjl5i41h2p0b6x1iakl5zpp10")))

(define-public crate-solana-compute-budget-program-1.10.17 (c (n "solana-compute-budget-program") (v "1.10.17") (d (list (d (n "solana-program-runtime") (r "=1.10.17") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.10.17") (d #t) (k 0)))) (h "0bfjlb6qacscll18b0901zahvjpaxrhif87wlav6lbi9c7dg57r7")))

(define-public crate-solana-compute-budget-program-1.10.18 (c (n "solana-compute-budget-program") (v "1.10.18") (d (list (d (n "solana-program-runtime") (r "=1.10.18") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.10.18") (d #t) (k 0)))) (h "1bgamaln0i0g8zgiy123fsc0849yjg0860bv5g6823jmi2p760ij")))

(define-public crate-solana-compute-budget-program-1.9.23 (c (n "solana-compute-budget-program") (v "1.9.23") (d (list (d (n "solana-program-runtime") (r "=1.9.23") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.9.23") (d #t) (k 0)))) (h "0g40gym4r0irj668i690yclmgzg3bx658imjgbwkwdl4440b4890")))

(define-public crate-solana-compute-budget-program-1.10.19 (c (n "solana-compute-budget-program") (v "1.10.19") (d (list (d (n "solana-program-runtime") (r "=1.10.19") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.10.19") (d #t) (k 0)))) (h "1ms5dr31s5i9gg9qn92a4p4wyx56xbdfwc2yzxpfyr8j2yz8hpxp")))

(define-public crate-solana-compute-budget-program-1.9.24 (c (n "solana-compute-budget-program") (v "1.9.24") (d (list (d (n "solana-program-runtime") (r "=1.9.24") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.9.24") (d #t) (k 0)))) (h "0z0n3m1kg7zy84snnaqjqr5mnn3x5qs05haj7m816gaj58wajr83")))

(define-public crate-solana-compute-budget-program-1.9.25 (c (n "solana-compute-budget-program") (v "1.9.25") (d (list (d (n "solana-program-runtime") (r "=1.9.25") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.9.25") (d #t) (k 0)))) (h "06jiq04ys46s4iywgw6zrk1k1r33jfc2f5z1hm4as2c70aykrx9n")))

(define-public crate-solana-compute-budget-program-1.10.20 (c (n "solana-compute-budget-program") (v "1.10.20") (d (list (d (n "solana-program-runtime") (r "=1.10.20") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.10.20") (d #t) (k 0)))) (h "14m2ay2c3f1pk3qi7b8k64vl4p021ixx55z4g9jfkasfiz3sy5di")))

(define-public crate-solana-compute-budget-program-1.9.26 (c (n "solana-compute-budget-program") (v "1.9.26") (d (list (d (n "solana-program-runtime") (r "=1.9.26") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.9.26") (d #t) (k 0)))) (h "0favmifsa21whgr3h8n4db27biq8fhcpxyjf2dgwza27jj05sj1r")))

(define-public crate-solana-compute-budget-program-1.10.21 (c (n "solana-compute-budget-program") (v "1.10.21") (d (list (d (n "solana-program-runtime") (r "=1.10.21") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.10.21") (d #t) (k 0)))) (h "1l6gjfic1yb9id5hia4s2hr4h696v7i0xv3lllh4zrxg1zmagf67")))

(define-public crate-solana-compute-budget-program-1.9.28 (c (n "solana-compute-budget-program") (v "1.9.28") (d (list (d (n "solana-program-runtime") (r "=1.9.28") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.9.28") (d #t) (k 0)))) (h "0jzk16dmr0mrk39s4fi40j5yhnqhzhq9kgf66a60pq4r50zd0lgy")))

(define-public crate-solana-compute-budget-program-1.10.23 (c (n "solana-compute-budget-program") (v "1.10.23") (d (list (d (n "solana-program-runtime") (r "=1.10.23") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.10.23") (d #t) (k 0)))) (h "058bv0x8sj1rck009jzx2sfp9znvjhrnzldb6ksl88g5mzny11zl")))

(define-public crate-solana-compute-budget-program-1.10.24 (c (n "solana-compute-budget-program") (v "1.10.24") (d (list (d (n "solana-program-runtime") (r "=1.10.24") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.10.24") (d #t) (k 0)))) (h "1az8gpfsrrcdd20j6yzn1m8r9bszlhz2kai9c2wlr1vvjs67hhvv")))

(define-public crate-solana-compute-budget-program-1.10.25 (c (n "solana-compute-budget-program") (v "1.10.25") (d (list (d (n "solana-program-runtime") (r "=1.10.25") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.10.25") (d #t) (k 0)))) (h "1f4hcsr2wlm0zv6qi6mr54i1635qaaxbmnjkics49c6d1mzkqa7g")))

(define-public crate-solana-compute-budget-program-1.9.29 (c (n "solana-compute-budget-program") (v "1.9.29") (d (list (d (n "solana-program-runtime") (r "=1.9.29") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.9.29") (d #t) (k 0)))) (h "0hgxqzj1aymx1my1x1r14wl37p0hgkbawfwxyp411pr3jabwnmhc")))

(define-public crate-solana-compute-budget-program-1.10.26 (c (n "solana-compute-budget-program") (v "1.10.26") (d (list (d (n "solana-program-runtime") (r "=1.10.26") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.10.26") (d #t) (k 0)))) (h "1dfixgraxr1lycd9ccdd39910jhmx3yy3lz5a9hmz31wazqk5z79")))

(define-public crate-solana-compute-budget-program-1.11.0 (c (n "solana-compute-budget-program") (v "1.11.0") (d (list (d (n "solana-program-runtime") (r "=1.11.0") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.11.0") (d #t) (k 0)))) (h "0b151axmxx0g711mnjf5fwq6v7f0nvy1xjqnxfrii876ghhr7rd3")))

(define-public crate-solana-compute-budget-program-1.10.27 (c (n "solana-compute-budget-program") (v "1.10.27") (d (list (d (n "solana-program-runtime") (r "=1.10.27") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.10.27") (d #t) (k 0)))) (h "0kmgig6h8niypjkfcsbbwgx0hkdlcjx4ryf4wkzsd0d9lmhj67hi")))

(define-public crate-solana-compute-budget-program-1.11.1 (c (n "solana-compute-budget-program") (v "1.11.1") (d (list (d (n "solana-program-runtime") (r "=1.11.1") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.11.1") (d #t) (k 0)))) (h "1ldssky8zfbf91z3439w8kl97r83ifa1gqzrgcq58h9pxyaqsxj9")))

(define-public crate-solana-compute-budget-program-1.10.28 (c (n "solana-compute-budget-program") (v "1.10.28") (d (list (d (n "solana-program-runtime") (r "=1.10.28") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.10.28") (d #t) (k 0)))) (h "1bkcng8xzgsl60vb9x1jg9q8gpgf1lvsz8xxqvry8lpv3s057jrf")))

(define-public crate-solana-compute-budget-program-1.10.29 (c (n "solana-compute-budget-program") (v "1.10.29") (d (list (d (n "solana-program-runtime") (r "=1.10.29") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.10.29") (d #t) (k 0)))) (h "1nc3hg9j2i9mlhqylv476n73231pki629pgmywd3pg4bsm1gnpim")))

(define-public crate-solana-compute-budget-program-1.10.30 (c (n "solana-compute-budget-program") (v "1.10.30") (d (list (d (n "solana-program-runtime") (r "=1.10.30") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.10.30") (d #t) (k 0)))) (h "19ibgcqsd354svc8g6jr8xkynhbzjg3qpvjza5f0hdqmasdgyysi")))

(define-public crate-solana-compute-budget-program-1.11.2 (c (n "solana-compute-budget-program") (v "1.11.2") (d (list (d (n "solana-program-runtime") (r "=1.11.2") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.11.2") (d #t) (k 0)))) (h "1w3az74lkmmngzcs15r24f6cr5xm46r66n1282sbnb10ayqn4wmr")))

(define-public crate-solana-compute-budget-program-1.10.31 (c (n "solana-compute-budget-program") (v "1.10.31") (d (list (d (n "solana-program-runtime") (r "=1.10.31") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.10.31") (d #t) (k 0)))) (h "1yw2h08ms7lsbhiyb700kzmmgp2k0savsfy75fqxqdvlkayg0kjh")))

(define-public crate-solana-compute-budget-program-1.11.3 (c (n "solana-compute-budget-program") (v "1.11.3") (d (list (d (n "solana-program-runtime") (r "=1.11.3") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.11.3") (d #t) (k 0)))) (h "1q0qiinzw69wksh0269wf8jwj42idzpnxzia869h7rf22xy73hd6")))

(define-public crate-solana-compute-budget-program-1.10.32 (c (n "solana-compute-budget-program") (v "1.10.32") (d (list (d (n "solana-program-runtime") (r "=1.10.32") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.10.32") (d #t) (k 0)))) (h "0iq26mqnmrbhq7qc8i5rc7rvhawhsfgb77xr63z5xw5nl4a63dmg")))

(define-public crate-solana-compute-budget-program-1.11.4 (c (n "solana-compute-budget-program") (v "1.11.4") (d (list (d (n "solana-program-runtime") (r "=1.11.4") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.11.4") (d #t) (k 0)))) (h "05n5z0wmw7xk40dpjqy97x26jlmyjj0r3l09fpgki7cc6q1qq50c")))

(define-public crate-solana-compute-budget-program-1.10.33 (c (n "solana-compute-budget-program") (v "1.10.33") (d (list (d (n "solana-program-runtime") (r "=1.10.33") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.10.33") (d #t) (k 0)))) (h "1hq8cdydq2bnmiryb3db0c59xnbqlj65aasm6k506vw2cgrj6nlw")))

(define-public crate-solana-compute-budget-program-1.10.34 (c (n "solana-compute-budget-program") (v "1.10.34") (d (list (d (n "solana-program-runtime") (r "=1.10.34") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.10.34") (d #t) (k 0)))) (h "1qghhcffa5lgavmqcfqkzrwdj2pnr2qkdzfqzjdsdm1b2044fh7i")))

(define-public crate-solana-compute-budget-program-1.11.5 (c (n "solana-compute-budget-program") (v "1.11.5") (d (list (d (n "solana-program-runtime") (r "=1.11.5") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.11.5") (d #t) (k 0)))) (h "0n6dw7lk7awc0dqb26k8mimprj7ahqlkc477fwrg5g97npqpygdk")))

(define-public crate-solana-compute-budget-program-1.10.35 (c (n "solana-compute-budget-program") (v "1.10.35") (d (list (d (n "solana-program-runtime") (r "=1.10.35") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.10.35") (d #t) (k 0)))) (h "1b27a8m1z7sn3yypwycw33wsi9dabdshj6vkzk020r2a8x8hisky")))

(define-public crate-solana-compute-budget-program-1.11.6 (c (n "solana-compute-budget-program") (v "1.11.6") (d (list (d (n "solana-program-runtime") (r "=1.11.6") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.11.6") (d #t) (k 0)))) (h "07jgpylfmrll4gk55xslf5nrvkavkrhp38fx4qiwpzg4qxwri0jz")))

(define-public crate-solana-compute-budget-program-1.11.7 (c (n "solana-compute-budget-program") (v "1.11.7") (d (list (d (n "solana-program-runtime") (r "=1.11.7") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.11.7") (d #t) (k 0)))) (h "1p7zaxjga3s86i3m10gmgn4syj4ixkgasvhd0q6sqlf4z17w15rg")))

(define-public crate-solana-compute-budget-program-1.11.8 (c (n "solana-compute-budget-program") (v "1.11.8") (d (list (d (n "solana-program-runtime") (r "=1.11.8") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.11.8") (d #t) (k 0)))) (h "08vqffrb2vgmns5a73vk0qk4lldpfpspb42idnc7ghvz0w2phsdm")))

(define-public crate-solana-compute-budget-program-1.11.10 (c (n "solana-compute-budget-program") (v "1.11.10") (d (list (d (n "solana-program-runtime") (r "=1.11.10") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.11.10") (d #t) (k 0)))) (h "09zygvsk1phf612gsqz7csv708az826ijrk2cz0kwqz4nsp2f9j9")))

(define-public crate-solana-compute-budget-program-1.10.38 (c (n "solana-compute-budget-program") (v "1.10.38") (d (list (d (n "solana-program-runtime") (r "=1.10.38") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.10.38") (d #t) (k 0)))) (h "1w64wdn2993pcm9q4zazqf4lw6cl75y9fnbqdvwn6svvhdivk647")))

(define-public crate-solana-compute-budget-program-1.13.0 (c (n "solana-compute-budget-program") (v "1.13.0") (d (list (d (n "solana-program-runtime") (r "=1.13.0") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.13.0") (d #t) (k 0)))) (h "0s91lk4d3mkh8irjyqmqjzm4db06wj18mfn2684vji98a3sqqag5")))

(define-public crate-solana-compute-budget-program-1.14.0 (c (n "solana-compute-budget-program") (v "1.14.0") (d (list (d (n "solana-program-runtime") (r "=1.14.0") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.14.0") (d #t) (k 0)))) (h "1b5hpjihl111nw57qljyz15rykpbrhag86lbi0x12ywlm2cpgrqr")))

(define-public crate-solana-compute-budget-program-1.14.1 (c (n "solana-compute-budget-program") (v "1.14.1") (d (list (d (n "solana-program-runtime") (r "=1.14.1") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.14.1") (d #t) (k 0)))) (h "0hpza7lfvkclf5fs27xcpvpkjfhv084jssfl51j6l6y265sq8lad")))

(define-public crate-solana-compute-budget-program-1.10.39 (c (n "solana-compute-budget-program") (v "1.10.39") (d (list (d (n "solana-program-runtime") (r "=1.10.39") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.10.39") (d #t) (k 0)))) (h "04nk3nyjhngs7589p3dpiqf9jw83n45xaqxqdgjw10sgdpmbcf7l")))

(define-public crate-solana-compute-budget-program-1.14.2 (c (n "solana-compute-budget-program") (v "1.14.2") (d (list (d (n "solana-program-runtime") (r "=1.14.2") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.14.2") (d #t) (k 0)))) (h "10r2s5nxciw3ag9j7gc76h9qw64ylgk25ksk44vq3mgw4k1y6a75")))

(define-public crate-solana-compute-budget-program-1.13.1 (c (n "solana-compute-budget-program") (v "1.13.1") (d (list (d (n "solana-program-runtime") (r "=1.13.1") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.13.1") (d #t) (k 0)))) (h "1hnsa83j2r0q3wq91c3kbp6kvcjzmg3k6kxww1fcwbvcam4fp43n")))

(define-public crate-solana-compute-budget-program-1.14.3 (c (n "solana-compute-budget-program") (v "1.14.3") (d (list (d (n "solana-program-runtime") (r "=1.14.3") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.14.3") (d #t) (k 0)))) (h "0bwl4a9r0as31wj40z1k7iywbjndscp6kvv3y8zmdnr7j3mz1z07")))

(define-public crate-solana-compute-budget-program-1.10.40 (c (n "solana-compute-budget-program") (v "1.10.40") (d (list (d (n "solana-program-runtime") (r "=1.10.40") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.10.40") (d #t) (k 0)))) (h "1bwc925j3a2kl8mgjqwri0cvljg9zhbs3cg8dyar4hq3ywdf6rzw")))

(define-public crate-solana-compute-budget-program-1.14.4 (c (n "solana-compute-budget-program") (v "1.14.4") (d (list (d (n "solana-program-runtime") (r "=1.14.4") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.14.4") (d #t) (k 0)))) (h "1lr5fcbxva6jhfs34mfr8rv6i92ks14ns3dn61igh0lsd0126lyr")))

(define-public crate-solana-compute-budget-program-1.13.2 (c (n "solana-compute-budget-program") (v "1.13.2") (d (list (d (n "solana-program-runtime") (r "=1.13.2") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.13.2") (d #t) (k 0)))) (h "08h9l7rxir8hqra3ri5z4xqra9si9br8a16sdssxvrv41svl16yy")))

(define-public crate-solana-compute-budget-program-1.14.5 (c (n "solana-compute-budget-program") (v "1.14.5") (d (list (d (n "solana-program-runtime") (r "=1.14.5") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.14.5") (d #t) (k 0)))) (h "0qvr61grmb0brx8a6hkal5dssb2fd8m7yxxb11dl9031dzqj6gf6")))

(define-public crate-solana-compute-budget-program-1.10.41 (c (n "solana-compute-budget-program") (v "1.10.41") (d (list (d (n "solana-program-runtime") (r "=1.10.41") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.10.41") (d #t) (k 0)))) (h "0hjb84mm4w5f92vxjar07rqyd4san26zc9yz0715ylgqscijz5dx")))

(define-public crate-solana-compute-budget-program-1.13.3 (c (n "solana-compute-budget-program") (v "1.13.3") (d (list (d (n "solana-program-runtime") (r "=1.13.3") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.13.3") (d #t) (k 0)))) (h "0cmzw8mcibqj7knbfpdqx192miic3wq6wh816kwkljwy8nvrg7gc")))

(define-public crate-solana-compute-budget-program-1.13.4 (c (n "solana-compute-budget-program") (v "1.13.4") (d (list (d (n "solana-program-runtime") (r "=1.13.4") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.13.4") (d #t) (k 0)))) (h "0ky14hbrq3h5jrhgys1k8jj6kyfazg3izg73szabsr8q26g1dp08")))

(define-public crate-solana-compute-budget-program-1.14.6 (c (n "solana-compute-budget-program") (v "1.14.6") (d (list (d (n "solana-program-runtime") (r "=1.14.6") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.14.6") (d #t) (k 0)))) (h "08g2psak98glfdp5dy3vg90pdlsmx5drpzl5galpqsw91qfpylvm")))

(define-public crate-solana-compute-budget-program-1.14.7 (c (n "solana-compute-budget-program") (v "1.14.7") (d (list (d (n "solana-program-runtime") (r "=1.14.7") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.14.7") (d #t) (k 0)))) (h "0qapwr8vww1sgvjjzs5c67q6jci9i8h7p4van0fdmfa2zczl17n6")))

(define-public crate-solana-compute-budget-program-1.13.5 (c (n "solana-compute-budget-program") (v "1.13.5") (d (list (d (n "solana-program-runtime") (r "=1.13.5") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.13.5") (d #t) (k 0)))) (h "0am6c294vrz2yzv7jh39bcgn5vysqd6wfaar9pywm0dz8ydv7aq9")))

(define-public crate-solana-compute-budget-program-1.14.8 (c (n "solana-compute-budget-program") (v "1.14.8") (d (list (d (n "solana-program-runtime") (r "=1.14.8") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.14.8") (d #t) (k 0)))) (h "1bcx3khmsywlyam0krvwnz55z8rz8civ80pwj5dd9gbz4x2xl70b")))

(define-public crate-solana-compute-budget-program-1.14.9 (c (n "solana-compute-budget-program") (v "1.14.9") (d (list (d (n "solana-program-runtime") (r "=1.14.9") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.14.9") (d #t) (k 0)))) (h "1pwh1jrspprrln5ay443sx3i3yyz9f1766bsll9gxlwxhdq4ilvv")))

(define-public crate-solana-compute-budget-program-1.14.10 (c (n "solana-compute-budget-program") (v "1.14.10") (d (list (d (n "solana-program-runtime") (r "=1.14.10") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.14.10") (d #t) (k 0)))) (h "054vdqga4khn9mymyz112791n9fvwjpwnazlb7bm9fx9bj1y2bbg")))

(define-public crate-solana-compute-budget-program-1.14.11 (c (n "solana-compute-budget-program") (v "1.14.11") (d (list (d (n "solana-program-runtime") (r "=1.14.11") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.14.11") (d #t) (k 0)))) (h "1a9zl2468cg2kayznk7qy150cvw65f4snd2xqq5cx9g3praz7fxb")))

(define-public crate-solana-compute-budget-program-1.14.12 (c (n "solana-compute-budget-program") (v "1.14.12") (d (list (d (n "solana-program-runtime") (r "=1.14.12") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.14.12") (d #t) (k 0)))) (h "1yjcib753gfgl7bq23icnlqh43q04f3ic7qkp1wd4i1x9wwjwwag")))

(define-public crate-solana-compute-budget-program-1.13.6 (c (n "solana-compute-budget-program") (v "1.13.6") (d (list (d (n "solana-program-runtime") (r "=1.13.6") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.13.6") (d #t) (k 0)))) (h "02yg02g1cm0imiqdl6yzn5mpf814aidb1qfs0rhh2wnj4iwfx04n")))

(define-public crate-solana-compute-budget-program-1.14.13 (c (n "solana-compute-budget-program") (v "1.14.13") (d (list (d (n "solana-program-runtime") (r "=1.14.13") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.14.13") (d #t) (k 0)))) (h "1nsw5c3n0fsg3xjwx2adn5lshrbw3fhkh0jjmjcl74ndwl83c42p")))

(define-public crate-solana-compute-budget-program-1.15.0 (c (n "solana-compute-budget-program") (v "1.15.0") (d (list (d (n "solana-program-runtime") (r "=1.15.0") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.15.0") (d #t) (k 0)))) (h "0p4y9rm88ymwqkqln6rqy4ncdsnxj118znwkfymk12ahvh970fnx") (y #t)))

(define-public crate-solana-compute-budget-program-1.14.14 (c (n "solana-compute-budget-program") (v "1.14.14") (d (list (d (n "solana-program-runtime") (r "=1.14.14") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.14.14") (d #t) (k 0)))) (h "0f8lyhj4sdqvdi1za3bswrxz1iamwdr3fb3ais6kfmv37mcx4wvq")))

(define-public crate-solana-compute-budget-program-1.14.15 (c (n "solana-compute-budget-program") (v "1.14.15") (d (list (d (n "solana-program-runtime") (r "=1.14.15") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.14.15") (d #t) (k 0)))) (h "0fa8ggj6xxaxj1c5px568jfvw3f2nhih52hm48in4vmlw6zcxb63")))

(define-public crate-solana-compute-budget-program-1.15.1 (c (n "solana-compute-budget-program") (v "1.15.1") (d (list (d (n "solana-program-runtime") (r "=1.15.1") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.15.1") (d #t) (k 0)))) (h "1jnps64kmi9ibk0fd19kqyqdxka1l64gmdvwya9w9pbna54lg0ry") (y #t)))

(define-public crate-solana-compute-budget-program-1.15.2 (c (n "solana-compute-budget-program") (v "1.15.2") (d (list (d (n "solana-program-runtime") (r "=1.15.2") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.15.2") (d #t) (k 0)))) (h "1xrl9a9z3kinfdz2776jd83yz9n01fjdjyria4dnqgi8939b0aip") (y #t)))

(define-public crate-solana-compute-budget-program-1.14.16 (c (n "solana-compute-budget-program") (v "1.14.16") (d (list (d (n "solana-program-runtime") (r "=1.14.16") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.14.16") (d #t) (k 0)))) (h "0q9m7cqicgl3sk8mryf53lf0nqvwy7wzcipby468vjra4x2ik019")))

(define-public crate-solana-compute-budget-program-1.14.17 (c (n "solana-compute-budget-program") (v "1.14.17") (d (list (d (n "solana-program-runtime") (r "=1.14.17") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.14.17") (d #t) (k 0)))) (h "0gcgcp5vlpls4jyp412azsdksk8hi9l4lwdwh7915fs5d9aazja7")))

(define-public crate-solana-compute-budget-program-1.13.7 (c (n "solana-compute-budget-program") (v "1.13.7") (d (list (d (n "solana-program-runtime") (r "=1.13.7") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.13.7") (d #t) (k 0)))) (h "0y8vdjaj3bq0h5al144whma0pkwvmbmkriz1cbpiwcws5qmjs0d1")))

(define-public crate-solana-compute-budget-program-1.14.18 (c (n "solana-compute-budget-program") (v "1.14.18") (d (list (d (n "solana-program-runtime") (r "=1.14.18") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.14.18") (d #t) (k 0)))) (h "15qvrjnqg71c2kizaj5rka8vz1icrhmxn6mb1qhhjd9259fz6jg0")))

(define-public crate-solana-compute-budget-program-1.16.0 (c (n "solana-compute-budget-program") (v "1.16.0") (d (list (d (n "solana-program-runtime") (r "=1.16.0") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.16.0") (d #t) (k 0)))) (h "1k1k7hzk6zxkzka1vfpsgqiyy5pp9f2lw7b628af1sm1cmz03329")))

(define-public crate-solana-compute-budget-program-1.16.1 (c (n "solana-compute-budget-program") (v "1.16.1") (d (list (d (n "solana-program-runtime") (r "=1.16.1") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.16.1") (d #t) (k 0)))) (h "1k8bh3hy7ni53g5crc2dlzvsdll77b963pa304hhbqs833cn704b")))

(define-public crate-solana-compute-budget-program-1.14.19 (c (n "solana-compute-budget-program") (v "1.14.19") (d (list (d (n "solana-program-runtime") (r "=1.14.19") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.14.19") (d #t) (k 0)))) (h "0rq400hg1qb39vvj86d6qp8w0bw4wihlxf35fkqcfna3g6l983yh")))

(define-public crate-solana-compute-budget-program-1.16.2 (c (n "solana-compute-budget-program") (v "1.16.2") (d (list (d (n "solana-program-runtime") (r "=1.16.2") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.16.2") (d #t) (k 0)))) (h "00iw4wnmh45pq9y424n1s4mmqazzcys0zlvf9vgvry6ah884y7z6")))

(define-public crate-solana-compute-budget-program-1.16.3 (c (n "solana-compute-budget-program") (v "1.16.3") (d (list (d (n "solana-program-runtime") (r "=1.16.3") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.16.3") (d #t) (k 0)))) (h "182a5zfmgqd010vahkfzk5368mn17b6vnpfbznxfj1md78a6w0j8")))

(define-public crate-solana-compute-budget-program-1.14.20 (c (n "solana-compute-budget-program") (v "1.14.20") (d (list (d (n "solana-program-runtime") (r "=1.14.20") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.14.20") (d #t) (k 0)))) (h "1g1vxqibwll67ygs35bjkrqxpirh81qx1v2njjfym1f6gx7iw7xc")))

(define-public crate-solana-compute-budget-program-1.16.4 (c (n "solana-compute-budget-program") (v "1.16.4") (d (list (d (n "solana-program-runtime") (r "=1.16.4") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.16.4") (d #t) (k 0)))) (h "0y9zbj49pyifkjgzv49f3rc8fdaihhqyf53phrfk32aaaxaa03qy")))

(define-public crate-solana-compute-budget-program-1.16.5 (c (n "solana-compute-budget-program") (v "1.16.5") (d (list (d (n "solana-program-runtime") (r "=1.16.5") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.16.5") (d #t) (k 0)))) (h "1rnwl5dsciwal3pf0pk50gb64d4ffbc654vfpmixzwz65v8hpqg8")))

(define-public crate-solana-compute-budget-program-1.14.21 (c (n "solana-compute-budget-program") (v "1.14.21") (d (list (d (n "solana-program-runtime") (r "=1.14.21") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.14.21") (d #t) (k 0)))) (h "15bwvpi9xfnn7yylqciha7is1vk2mbf79pj5rfjs7ab3qnf25xxp")))

(define-public crate-solana-compute-budget-program-1.14.22 (c (n "solana-compute-budget-program") (v "1.14.22") (d (list (d (n "solana-program-runtime") (r "=1.14.22") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.14.22") (d #t) (k 0)))) (h "1rfh4jzhbgml8qal9yd8rxi6gyzra86w2qwy8hqsvsv2qa0qlmdx")))

(define-public crate-solana-compute-budget-program-1.16.6 (c (n "solana-compute-budget-program") (v "1.16.6") (d (list (d (n "solana-program-runtime") (r "=1.16.6") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.16.6") (d #t) (k 0)))) (h "0pylkcdz3dzwq65zjh9y15krxd2nry0wpa70rq2wsrh4j8irgz97")))

(define-public crate-solana-compute-budget-program-1.16.7 (c (n "solana-compute-budget-program") (v "1.16.7") (d (list (d (n "solana-program-runtime") (r "=1.16.7") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.16.7") (d #t) (k 0)))) (h "167g7wxyjlxg3kpqa05b3qpi0bqgvs70z89xmhxw92i3qpi0pxrs")))

(define-public crate-solana-compute-budget-program-1.14.23 (c (n "solana-compute-budget-program") (v "1.14.23") (d (list (d (n "solana-program-runtime") (r "=1.14.23") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.14.23") (d #t) (k 0)))) (h "0ra9b438my3n95h14q7lv2srp4i1kjgymgkb4cxjswp0qkv3w2kf")))

(define-public crate-solana-compute-budget-program-1.16.8 (c (n "solana-compute-budget-program") (v "1.16.8") (d (list (d (n "solana-program-runtime") (r "=1.16.8") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.16.8") (d #t) (k 0)))) (h "1jicx9l8k4ggs3fc0hfmysfr57aswqzj3klcqz59p1yrnbm2wccq")))

(define-public crate-solana-compute-budget-program-1.14.24 (c (n "solana-compute-budget-program") (v "1.14.24") (d (list (d (n "solana-program-runtime") (r "=1.14.24") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.14.24") (d #t) (k 0)))) (h "0nw2zfw70s7w4z0v1cy0si5pa4mnqlx02y8g580k1la53alzkw1z")))

(define-public crate-solana-compute-budget-program-1.16.9 (c (n "solana-compute-budget-program") (v "1.16.9") (d (list (d (n "solana-program-runtime") (r "=1.16.9") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.16.9") (d #t) (k 0)))) (h "0njxr0rdzwp4xnrm1yyswwq4l4qr01miz6gcwfn799zjpcqr175v")))

(define-public crate-solana-compute-budget-program-1.16.10 (c (n "solana-compute-budget-program") (v "1.16.10") (d (list (d (n "solana-program-runtime") (r "=1.16.10") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.16.10") (d #t) (k 0)))) (h "15z2ni8kn4pwabkwn7lb4zckyk4dr03dzxq9jik5nr2ip1m3i7hh")))

(define-public crate-solana-compute-budget-program-1.14.25 (c (n "solana-compute-budget-program") (v "1.14.25") (d (list (d (n "solana-program-runtime") (r "=1.14.25") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.14.25") (d #t) (k 0)))) (h "0r40v7930adfkg6qzxyjjpdc4g5k2h9zaz307242wvlyxcz0pbdx")))

(define-public crate-solana-compute-budget-program-1.16.11 (c (n "solana-compute-budget-program") (v "1.16.11") (d (list (d (n "solana-program-runtime") (r "=1.16.11") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.16.11") (d #t) (k 0)))) (h "044f6d36lrzw8q16mkqgncsjm33ws4k8pr0qscb25b8j88g91fmn")))

(define-public crate-solana-compute-budget-program-1.14.26 (c (n "solana-compute-budget-program") (v "1.14.26") (d (list (d (n "solana-program-runtime") (r "=1.14.26") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.14.26") (d #t) (k 0)))) (h "0yvjj5myjzjvfjn2ahf65dkigrric45lz817a8g6jrq5ysq44ax0")))

(define-public crate-solana-compute-budget-program-1.16.12 (c (n "solana-compute-budget-program") (v "1.16.12") (d (list (d (n "solana-program-runtime") (r "=1.16.12") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.16.12") (d #t) (k 0)))) (h "0b0lv8c1r2fn33a95y14wx70qi94sc487lbqk31v303qrmchrvm1")))

(define-public crate-solana-compute-budget-program-1.14.27 (c (n "solana-compute-budget-program") (v "1.14.27") (d (list (d (n "solana-program-runtime") (r "=1.14.27") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.14.27") (d #t) (k 0)))) (h "0620b85hdpx674162lmg7g9c9d38g43k61lcgwfcqa3g8680am6h")))

(define-public crate-solana-compute-budget-program-1.16.13 (c (n "solana-compute-budget-program") (v "1.16.13") (d (list (d (n "solana-program-runtime") (r "=1.16.13") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.16.13") (d #t) (k 0)))) (h "177iwfmzmbcmdahd5yghqrzgcwa9ymgpy019laxbg2xx8zkpjbsm")))

(define-public crate-solana-compute-budget-program-1.16.14 (c (n "solana-compute-budget-program") (v "1.16.14") (d (list (d (n "solana-program-runtime") (r "=1.16.14") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.16.14") (d #t) (k 0)))) (h "15899x7y749zi973sxlnvwkvs2mg67f4wxwbp3z6szdssr5qcgx3")))

(define-public crate-solana-compute-budget-program-1.14.28 (c (n "solana-compute-budget-program") (v "1.14.28") (d (list (d (n "solana-program-runtime") (r "=1.14.28") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.14.28") (d #t) (k 0)))) (h "0gxpv19gpylkwbqsvi2cmgkjr856hzrkpc9s9isf1jy9wjvvyiib")))

(define-public crate-solana-compute-budget-program-1.14.29 (c (n "solana-compute-budget-program") (v "1.14.29") (d (list (d (n "solana-program-runtime") (r "=1.14.29") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.14.29") (d #t) (k 0)))) (h "04qhj2mj3mxd75g8vmx8n86xwnyj6l3jxfhsnnzk27f7ywgndb8a")))

(define-public crate-solana-compute-budget-program-1.16.15 (c (n "solana-compute-budget-program") (v "1.16.15") (d (list (d (n "solana-program-runtime") (r "=1.16.15") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.16.15") (d #t) (k 0)))) (h "1r23r20icmlnfxmr5rpas78h57yp3d4za2kn1axa8hfwmzd0zjcq")))

(define-public crate-solana-compute-budget-program-1.17.0 (c (n "solana-compute-budget-program") (v "1.17.0") (d (list (d (n "solana-program-runtime") (r "=1.17.0") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.0") (d #t) (k 0)))) (h "1p27m1q07yjvchknpy9hby49whw2cgqf0g77frjglxxs9lmkz4g5")))

(define-public crate-solana-compute-budget-program-1.16.16 (c (n "solana-compute-budget-program") (v "1.16.16") (d (list (d (n "solana-program-runtime") (r "=1.16.16") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.16.16") (d #t) (k 0)))) (h "1cd2kjsmwr6nblkdf193bya4aj3y4b8a84c2l04xadxwbjqvs34i")))

(define-public crate-solana-compute-budget-program-1.17.1 (c (n "solana-compute-budget-program") (v "1.17.1") (d (list (d (n "solana-program-runtime") (r "=1.17.1") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.1") (d #t) (k 0)))) (h "05vp0piqv00v4n6nxprdrcwwcj0zdrjyagc9jspr0r7bkwb9k7dm")))

(define-public crate-solana-compute-budget-program-1.16.17 (c (n "solana-compute-budget-program") (v "1.16.17") (d (list (d (n "solana-program-runtime") (r "=1.16.17") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.16.17") (d #t) (k 0)))) (h "0gz2002al040zbhplrw043jmm7753py1bqcq8lcg4d3b9l0nq75a")))

(define-public crate-solana-compute-budget-program-1.17.2 (c (n "solana-compute-budget-program") (v "1.17.2") (d (list (d (n "solana-program-runtime") (r "=1.17.2") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.2") (d #t) (k 0)))) (h "09ynsfyljf9wz21f2m4i1lj1gzpimfpjpws4cihibxv9frw51rik")))

(define-public crate-solana-compute-budget-program-1.16.18 (c (n "solana-compute-budget-program") (v "1.16.18") (d (list (d (n "solana-program-runtime") (r "=1.16.18") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.16.18") (d #t) (k 0)))) (h "1p98b4sf9a69zhrzr4mxm60qf3i92wv714y4gbh1nja319qxvg8l")))

(define-public crate-solana-compute-budget-program-1.17.3 (c (n "solana-compute-budget-program") (v "1.17.3") (d (list (d (n "solana-program-runtime") (r "=1.17.3") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.3") (d #t) (k 0)))) (h "01ip89bwsi0yq7icl8gddc2paz61bcyqwy72d4h0v7h5365lijn1")))

(define-public crate-solana-compute-budget-program-1.17.4 (c (n "solana-compute-budget-program") (v "1.17.4") (d (list (d (n "solana-program-runtime") (r "=1.17.4") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.4") (d #t) (k 0)))) (h "1bdb1fkjg84rdnq7zk47yhmqhlmscaj6fnlfwlizm9q33rl64qxn")))

(define-public crate-solana-compute-budget-program-1.16.19 (c (n "solana-compute-budget-program") (v "1.16.19") (d (list (d (n "solana-program-runtime") (r "=1.16.19") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.16.19") (d #t) (k 0)))) (h "1sd0il8ayz5319v041phpc4yfbi3y0pbamjlx0jwcnw3rak4hf2c")))

(define-public crate-solana-compute-budget-program-1.17.5 (c (n "solana-compute-budget-program") (v "1.17.5") (d (list (d (n "solana-program-runtime") (r "=1.17.5") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.5") (d #t) (k 0)))) (h "064qff115fx56kn5ayc9wvjgzx2k2kx60jw8p512msnm2j6ry006")))

(define-public crate-solana-compute-budget-program-1.17.6 (c (n "solana-compute-budget-program") (v "1.17.6") (d (list (d (n "solana-program-runtime") (r "=1.17.6") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.6") (d #t) (k 0)))) (h "0n7mhdlb6bik0bfsnavpb21drsdggcwl73ll9z9h86x4g3pvjpxh")))

(define-public crate-solana-compute-budget-program-1.16.20 (c (n "solana-compute-budget-program") (v "1.16.20") (d (list (d (n "solana-program-runtime") (r "=1.16.20") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.16.20") (d #t) (k 0)))) (h "1qpw3dv9kc0mvnvh64h08ad9jvp4nxq8ddgdkm6y1f94xc5rcmzx")))

(define-public crate-solana-compute-budget-program-1.17.7 (c (n "solana-compute-budget-program") (v "1.17.7") (d (list (d (n "solana-program-runtime") (r "=1.17.7") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.7") (d #t) (k 0)))) (h "1lssaxx667y0fqy63c91skiwjdjdvd19jzgd5x3r98162j975xj5")))

(define-public crate-solana-compute-budget-program-1.16.21 (c (n "solana-compute-budget-program") (v "1.16.21") (d (list (d (n "solana-program-runtime") (r "=1.16.21") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.16.21") (d #t) (k 0)))) (h "00916xnwykrdlm34lnx989iqwxw6qx5c8avs1ma2a11f1jf3z5c2")))

(define-public crate-solana-compute-budget-program-1.17.8 (c (n "solana-compute-budget-program") (v "1.17.8") (d (list (d (n "solana-program-runtime") (r "=1.17.8") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.8") (d #t) (k 0)))) (h "1q45snl02q3znv27ac8m9ffrbim1aiwysya15qfbzpbynjwf0r8a")))

(define-public crate-solana-compute-budget-program-1.16.22 (c (n "solana-compute-budget-program") (v "1.16.22") (d (list (d (n "solana-program-runtime") (r "=1.16.22") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.16.22") (d #t) (k 0)))) (h "1lys4f4afdri38nwcafzk8zjrgcnziwvw8y8cllpz6jh9p7iny4i")))

(define-public crate-solana-compute-budget-program-1.16.23 (c (n "solana-compute-budget-program") (v "1.16.23") (d (list (d (n "solana-program-runtime") (r "=1.16.23") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.16.23") (d #t) (k 0)))) (h "1m7ffmpwn19bplix2k508w1lxcpbinc0px6fc348v8x60ykvqk37")))

(define-public crate-solana-compute-budget-program-1.17.9 (c (n "solana-compute-budget-program") (v "1.17.9") (d (list (d (n "solana-program-runtime") (r "=1.17.9") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.9") (d #t) (k 0)))) (h "1l81gjkdlpgvr008lcclw762mir0zg6xi0miszllin909sw8pzch")))

(define-public crate-solana-compute-budget-program-1.17.10 (c (n "solana-compute-budget-program") (v "1.17.10") (d (list (d (n "solana-program-runtime") (r "=1.17.10") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.10") (d #t) (k 0)))) (h "1ibw6h0cdb0ga8qp2ii4786yc4b0g1541gwbylknyvz0f2xr12kg")))

(define-public crate-solana-compute-budget-program-1.17.11 (c (n "solana-compute-budget-program") (v "1.17.11") (d (list (d (n "solana-program-runtime") (r "=1.17.11") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.11") (d #t) (k 0)))) (h "1zr084ykphy2xknmr4qxdbs0dnr0h3xflpj6gssgsf9pv0fj9w97")))

(define-public crate-solana-compute-budget-program-1.17.12 (c (n "solana-compute-budget-program") (v "1.17.12") (d (list (d (n "solana-program-runtime") (r "=1.17.12") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.12") (d #t) (k 0)))) (h "14zklglq065whzpxr83lwni45clp0xsmc945xb7lbqcxyb5hdqzg")))

(define-public crate-solana-compute-budget-program-1.16.24 (c (n "solana-compute-budget-program") (v "1.16.24") (d (list (d (n "solana-program-runtime") (r "=1.16.24") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.16.24") (d #t) (k 0)))) (h "0nn37ip8b87g90jpszvr9ha9c1znz3zm4g6n7jlw871qly3hxngf")))

(define-public crate-solana-compute-budget-program-1.17.13 (c (n "solana-compute-budget-program") (v "1.17.13") (d (list (d (n "solana-program-runtime") (r "=1.17.13") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.13") (d #t) (k 0)))) (h "155sg75cf5wwlc8g3wg2m5w53jgn7jxy8b61n9v5acx5ys69ghhw")))

(define-public crate-solana-compute-budget-program-1.17.14 (c (n "solana-compute-budget-program") (v "1.17.14") (d (list (d (n "solana-program-runtime") (r "=1.17.14") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.14") (d #t) (k 0)))) (h "114zg95cfkqa63m5jr76hfgh9wm2d4iqkicd10r67yrfn90ld9hb")))

(define-public crate-solana-compute-budget-program-1.17.15 (c (n "solana-compute-budget-program") (v "1.17.15") (d (list (d (n "solana-program-runtime") (r "=1.17.15") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.15") (d #t) (k 0)))) (h "0hr116hgmygmrzp0my8vc4hxzz4wzq0y3cnmgr245gmfi5vz8dzv")))

(define-public crate-solana-compute-budget-program-1.16.25 (c (n "solana-compute-budget-program") (v "1.16.25") (d (list (d (n "solana-program-runtime") (r "=1.16.25") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.16.25") (d #t) (k 0)))) (h "09ni3la3dzclcqhq4bmhrsd9pp1gwf73zk24jh9k4v3m9qnlpzk1")))

(define-public crate-solana-compute-budget-program-1.16.27 (c (n "solana-compute-budget-program") (v "1.16.27") (d (list (d (n "solana-program-runtime") (r "=1.16.27") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.16.27") (d #t) (k 0)))) (h "0yllvyljv59z9lp2dh9xhk3ibl2hg72hhq05v5kdhyiakn3qd61b")))

(define-public crate-solana-compute-budget-program-1.17.16 (c (n "solana-compute-budget-program") (v "1.17.16") (d (list (d (n "solana-program-runtime") (r "=1.17.16") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.16") (d #t) (k 0)))) (h "01s41cgykrqx20cdj1nrghs91z9dqxrimj3sxq3pijziq25p03p6")))

(define-public crate-solana-compute-budget-program-1.17.17 (c (n "solana-compute-budget-program") (v "1.17.17") (d (list (d (n "solana-program-runtime") (r "=1.17.17") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.17") (d #t) (k 0)))) (h "0m9qjxvabxkgkcjanv63kjx4xwr4krg6qfa3xb0685fqfq6hx454")))

(define-public crate-solana-compute-budget-program-1.17.18 (c (n "solana-compute-budget-program") (v "1.17.18") (d (list (d (n "solana-program-runtime") (r "=1.17.18") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.18") (d #t) (k 0)))) (h "1kfqz2hwbmf6904h7k0xgd1hgd1r1x7j5fwp15wrw60ykwqshr34")))

(define-public crate-solana-compute-budget-program-1.18.0 (c (n "solana-compute-budget-program") (v "1.18.0") (d (list (d (n "solana-program-runtime") (r "=1.18.0") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.18.0") (d #t) (k 0)))) (h "0rrwyc1yshwqsvfqh587cqavlwfc3nlxcgcww6w1z436yf3q39w8")))

(define-public crate-solana-compute-budget-program-1.18.1 (c (n "solana-compute-budget-program") (v "1.18.1") (d (list (d (n "solana-program-runtime") (r "=1.18.1") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.18.1") (d #t) (k 0)))) (h "11ikjzswfqaz8ak0va2f1pixiaxgrxjc6bvffbfd0k1fdwb0z89g")))

(define-public crate-solana-compute-budget-program-1.17.20 (c (n "solana-compute-budget-program") (v "1.17.20") (d (list (d (n "solana-program-runtime") (r "=1.17.20") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.20") (d #t) (k 0)))) (h "1vxabjl60bq8ji3phbszy5r18idc6b08dki48s1jzhzay16i1xhv")))

(define-public crate-solana-compute-budget-program-1.17.22 (c (n "solana-compute-budget-program") (v "1.17.22") (d (list (d (n "solana-program-runtime") (r "=1.17.22") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.22") (d #t) (k 0)))) (h "0hbdb49wj60mnmdi1nbxc0nwnj4liljgycng483w5q2w15sgdqxx")))

(define-public crate-solana-compute-budget-program-1.18.2 (c (n "solana-compute-budget-program") (v "1.18.2") (d (list (d (n "solana-program-runtime") (r "=1.18.2") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.18.2") (d #t) (k 0)))) (h "1a3wfg7z7zx9ys18klg4jdhigmz4dwgzs02hn4skj5z1cl2x8p61")))

(define-public crate-solana-compute-budget-program-1.17.23 (c (n "solana-compute-budget-program") (v "1.17.23") (d (list (d (n "solana-program-runtime") (r "=1.17.23") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.23") (d #t) (k 0)))) (h "117c7gq0kx61fj6xk1k5znlqqm1g7xzgsdjnzx223s48ax0hg1vy")))

(define-public crate-solana-compute-budget-program-1.18.3 (c (n "solana-compute-budget-program") (v "1.18.3") (d (list (d (n "solana-program-runtime") (r "=1.18.3") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.18.3") (d #t) (k 0)))) (h "13adfvgyal4x64n3jb05cqsdpp9gr2ccz1ri4vv3i7r8ynda71vl")))

(define-public crate-solana-compute-budget-program-1.18.4 (c (n "solana-compute-budget-program") (v "1.18.4") (d (list (d (n "solana-program-runtime") (r "=1.18.4") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.18.4") (d #t) (k 0)))) (h "1dy7lcf4s5633i7rjsa6lnzw1miiyk7877pip3q9y2p9l1mazi73")))

(define-public crate-solana-compute-budget-program-1.17.24 (c (n "solana-compute-budget-program") (v "1.17.24") (d (list (d (n "solana-program-runtime") (r "=1.17.24") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.24") (d #t) (k 0)))) (h "18rwv6km3lhack9fp79cba1mlx1q4qa9w22k5p5nkz9zqgdwrg1x")))

(define-public crate-solana-compute-budget-program-1.17.25 (c (n "solana-compute-budget-program") (v "1.17.25") (d (list (d (n "solana-program-runtime") (r "=1.17.25") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.25") (d #t) (k 0)))) (h "1ydpihxk21nlrpp0ig6xw90qlwqg1716gvqmld5p4j3q9c5f4nnp")))

(define-public crate-solana-compute-budget-program-1.18.5 (c (n "solana-compute-budget-program") (v "1.18.5") (d (list (d (n "solana-program-runtime") (r "=1.18.5") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.18.5") (d #t) (k 0)))) (h "078wypck6ffa5jx4b9q7xwi1qa8n7gy3m5gksdj3sb6625nbrnxb")))

(define-public crate-solana-compute-budget-program-1.17.26 (c (n "solana-compute-budget-program") (v "1.17.26") (d (list (d (n "solana-program-runtime") (r "=1.17.26") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.26") (d #t) (k 0)))) (h "0k3x2l31z2c6hsk7vmx9wn7kidn0wy9qwridkjzwp84acyw7ccjx")))

(define-public crate-solana-compute-budget-program-1.18.6 (c (n "solana-compute-budget-program") (v "1.18.6") (d (list (d (n "solana-program-runtime") (r "=1.18.6") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.18.6") (d #t) (k 0)))) (h "06lfnw191vy2nbr2hz6yf6yf4i4khk7lsy2vv3bqvrn75kxz8f45")))

(define-public crate-solana-compute-budget-program-1.17.27 (c (n "solana-compute-budget-program") (v "1.17.27") (d (list (d (n "solana-program-runtime") (r "=1.17.27") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.27") (d #t) (k 0)))) (h "1zxkhwvk60kc5lsz8h4s8i0907jc10p8pap8576fz5nb59nnb6bz")))

(define-public crate-solana-compute-budget-program-1.18.7 (c (n "solana-compute-budget-program") (v "1.18.7") (d (list (d (n "solana-program-runtime") (r "=1.18.7") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.18.7") (d #t) (k 0)))) (h "1dnx5vj603d8vznl8zg2hxjl6mri2g8fhv2ip7xdm40w9qm0scls")))

(define-public crate-solana-compute-budget-program-1.18.8 (c (n "solana-compute-budget-program") (v "1.18.8") (d (list (d (n "solana-program-runtime") (r "=1.18.8") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.18.8") (d #t) (k 0)))) (h "12hjbfl5xjs5lf2772j3pnxsdv21f631mxfm4fqda1mqic6972b2")))

(define-public crate-solana-compute-budget-program-1.17.28 (c (n "solana-compute-budget-program") (v "1.17.28") (d (list (d (n "solana-program-runtime") (r "=1.17.28") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.28") (d #t) (k 0)))) (h "0sch1pabkwfqpbdjgkzqgna3zdwnbwbvk5vffr6dnj24gzy8rfxg")))

(define-public crate-solana-compute-budget-program-1.18.9 (c (n "solana-compute-budget-program") (v "1.18.9") (d (list (d (n "solana-program-runtime") (r "=1.18.9") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.18.9") (d #t) (k 0)))) (h "0rv0mqbrydkz1bxqnc74dp3yfm0yc5l6abgah9yfb04njh0d0ag7")))

(define-public crate-solana-compute-budget-program-1.17.29 (c (n "solana-compute-budget-program") (v "1.17.29") (d (list (d (n "solana-program-runtime") (r "=1.17.29") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.29") (d #t) (k 0)))) (h "0kmz3bi2c88pzl1mm0p91hvii2xi6kvg9z6bphln72sjjy8sapal")))

(define-public crate-solana-compute-budget-program-1.17.30 (c (n "solana-compute-budget-program") (v "1.17.30") (d (list (d (n "solana-program-runtime") (r "=1.17.30") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.30") (d #t) (k 0)))) (h "07dkjkgv9rsgrqiibs643f6phfyqksk0s24law7p12bk63r0bl0y")))

(define-public crate-solana-compute-budget-program-1.18.10 (c (n "solana-compute-budget-program") (v "1.18.10") (d (list (d (n "solana-program-runtime") (r "=1.18.10") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.18.10") (d #t) (k 0)))) (h "1cqkwc2dpfkm2kkwfslm6f0p5dsmd3n20psnrl7irhf6rbxdkh2g")))

(define-public crate-solana-compute-budget-program-1.18.11 (c (n "solana-compute-budget-program") (v "1.18.11") (d (list (d (n "solana-program-runtime") (r "=1.18.11") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.18.11") (d #t) (k 0)))) (h "0q513yl3l7li9al8ha3mhgf1n8z95hg00krng5k045kf3vwxbd85")))

(define-public crate-solana-compute-budget-program-1.17.31 (c (n "solana-compute-budget-program") (v "1.17.31") (d (list (d (n "solana-program-runtime") (r "=1.17.31") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.31") (d #t) (k 0)))) (h "1cr9w72py4cvkimw94bzsx1bq5nvy4fm9in5kmnx17lzv72f6fkp")))

(define-public crate-solana-compute-budget-program-1.18.12 (c (n "solana-compute-budget-program") (v "1.18.12") (d (list (d (n "solana-program-runtime") (r "=1.18.12") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.18.12") (d #t) (k 0)))) (h "0zsj9vrl7bfvjwk07l9s6yh89mmc9lhbr4cp1dgmyibyvlmhn46a")))

(define-public crate-solana-compute-budget-program-1.17.32 (c (n "solana-compute-budget-program") (v "1.17.32") (d (list (d (n "solana-program-runtime") (r "=1.17.32") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.32") (d #t) (k 0)))) (h "1bksz4d63xxpw397y960y5big7hm58bg2a6a882ywi0h0cd9khdq")))

(define-public crate-solana-compute-budget-program-1.17.33 (c (n "solana-compute-budget-program") (v "1.17.33") (d (list (d (n "solana-program-runtime") (r "=1.17.33") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.33") (d #t) (k 0)))) (h "04ffbcxjnw8vh0zk3npvqfm6g1pfyjdfr43h4phjarb96wynw1g7")))

(define-public crate-solana-compute-budget-program-1.18.13 (c (n "solana-compute-budget-program") (v "1.18.13") (d (list (d (n "solana-program-runtime") (r "=1.18.13") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.18.13") (d #t) (k 0)))) (h "0i7ddfsf49yyw6biydpaq710nrnd1lgd1p9d7jh88wxsmrs6w8l4")))

(define-public crate-solana-compute-budget-program-1.18.14 (c (n "solana-compute-budget-program") (v "1.18.14") (d (list (d (n "solana-program-runtime") (r "=1.18.14") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.18.14") (d #t) (k 0)))) (h "0kkp70805wj3g8mic64y05g18kf683k79s2igby3vx2j5989wn3f")))

(define-public crate-solana-compute-budget-program-1.17.34 (c (n "solana-compute-budget-program") (v "1.17.34") (d (list (d (n "solana-program-runtime") (r "=1.17.34") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.34") (d #t) (k 0)))) (h "0yg0fib8fmbkqxmnbw27bjg7dj9nicplpccdw5h4gp6z0qisj0w7")))

