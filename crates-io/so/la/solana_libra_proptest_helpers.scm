(define-module (crates-io so la solana_libra_proptest_helpers) #:use-module (crates-io))

(define-public crate-solana_libra_proptest_helpers-0.0.0-sol7 (c (n "solana_libra_proptest_helpers") (v "0.0.0-sol7") (d (list (d (n "crossbeam") (r "^0.7.1") (d #t) (k 0)) (d (n "proptest") (r "^0.9.4") (d #t) (k 0)) (d (n "proptest-derive") (r "^0.1.2") (d #t) (k 0)))) (h "1vf8ygr9yj09mgvbyg1whnmply5k5dvzc6a9dzi6p4a4k5x77247") (y #t)))

(define-public crate-solana_libra_proptest_helpers-0.0.0-sol8 (c (n "solana_libra_proptest_helpers") (v "0.0.0-sol8") (d (list (d (n "crossbeam") (r "^0.7.1") (d #t) (k 0)) (d (n "proptest") (r "^0.9.4") (d #t) (k 0)) (d (n "proptest-derive") (r "^0.1.2") (d #t) (k 0)))) (h "181hxx32pyk2bn4fjajfbkrvyi7imjk52zsj2shqs54cvxidqzr4") (y #t)))

(define-public crate-solana_libra_proptest_helpers-0.0.0-sol9 (c (n "solana_libra_proptest_helpers") (v "0.0.0-sol9") (d (list (d (n "crossbeam") (r "^0.7.1") (d #t) (k 0)) (d (n "proptest") (r "^0.9.4") (d #t) (k 0)) (d (n "proptest-derive") (r "^0.1.2") (d #t) (k 0)))) (h "1bqzliya2rglqb7dpk1n50qswz3lghamvz7ar07wihr1p6kssw0l") (y #t)))

(define-public crate-solana_libra_proptest_helpers-0.0.0-sol10 (c (n "solana_libra_proptest_helpers") (v "0.0.0-sol10") (d (list (d (n "crossbeam") (r "^0.7.1") (d #t) (k 0)) (d (n "proptest") (r "^0.9.4") (d #t) (k 0)) (d (n "proptest-derive") (r "^0.1.2") (d #t) (k 0)))) (h "03h6d409vqfi4cmr0za378d7vchflp980hplqabsl4c8ava1k7rv") (y #t)))

(define-public crate-solana_libra_proptest_helpers-0.0.0-sol11 (c (n "solana_libra_proptest_helpers") (v "0.0.0-sol11") (d (list (d (n "crossbeam") (r "^0.7.1") (d #t) (k 0)) (d (n "proptest") (r "^0.9.4") (d #t) (k 0)) (d (n "proptest-derive") (r "^0.1.2") (d #t) (k 0)))) (h "0842vb4nsbzm4z2k01hvczaaq949w5g30ifvly5alll4b29h4dy7") (y #t)))

(define-public crate-solana_libra_proptest_helpers-0.0.0-sol12 (c (n "solana_libra_proptest_helpers") (v "0.0.0-sol12") (d (list (d (n "crossbeam") (r "^0.7.1") (d #t) (k 0)) (d (n "proptest") (r "^0.9.4") (d #t) (k 0)) (d (n "proptest-derive") (r "^0.1.2") (d #t) (k 0)))) (h "0ry4l11zw2gnb66ysrhffaq0dxgqfd7x4ydsqn04cfw1sgnc00ni") (y #t)))

(define-public crate-solana_libra_proptest_helpers-0.0.0-sol13 (c (n "solana_libra_proptest_helpers") (v "0.0.0-sol13") (d (list (d (n "crossbeam") (r "^0.7.1") (d #t) (k 0)) (d (n "proptest") (r "^0.9.4") (d #t) (k 0)) (d (n "proptest-derive") (r "^0.1.2") (d #t) (k 0)))) (h "1psnxcivsyln8bm6li6l21f0acz37yr0n2x267yw825y5dqxb4f3") (y #t)))

(define-public crate-solana_libra_proptest_helpers-0.0.0-sol14 (c (n "solana_libra_proptest_helpers") (v "0.0.0-sol14") (d (list (d (n "crossbeam") (r "^0.7.1") (d #t) (k 0)) (d (n "proptest") (r "^0.9.4") (d #t) (k 0)) (d (n "proptest-derive") (r "^0.1.2") (d #t) (k 0)))) (h "0b1cj7qcy5mpn9f0j9ly3pvy7lkrdjq0qa361cqh8s9yk75y3j75") (y #t)))

(define-public crate-solana_libra_proptest_helpers-0.0.0-sol15 (c (n "solana_libra_proptest_helpers") (v "0.0.0-sol15") (d (list (d (n "crossbeam") (r "^0.7.1") (d #t) (k 0)) (d (n "proptest") (r "^0.9.4") (d #t) (k 0)) (d (n "proptest-derive") (r "^0.1.2") (d #t) (k 0)))) (h "1kqfwbiwzjv12vlkqvk2ayjjbmp1jnvkwc4mmdw0g7rxlnbm5jp6")))

(define-public crate-solana_libra_proptest_helpers-0.0.0 (c (n "solana_libra_proptest_helpers") (v "0.0.0") (d (list (d (n "crossbeam") (r "^0.7.1") (d #t) (k 0)) (d (n "proptest") (r "^0.9.4") (d #t) (k 0)) (d (n "proptest-derive") (r "^0.1.2") (d #t) (k 0)))) (h "07ay41ngz775i6g6hvk21q73vn0jxb8awjy01xbbrryhnmsmizic")))

(define-public crate-solana_libra_proptest_helpers-0.0.1-sol3 (c (n "solana_libra_proptest_helpers") (v "0.0.1-sol3") (d (list (d (n "crossbeam") (r "^0.7.2") (d #t) (k 0)) (d (n "proptest") (r "^0.9.4") (d #t) (k 0)) (d (n "proptest-derive") (r "^0.1.2") (d #t) (k 0)))) (h "01hpc7mqah43z6gffx7fkaamdw66gmj9lgvd362z332wv0jwf3dk")))

(define-public crate-solana_libra_proptest_helpers-0.0.1-sol4 (c (n "solana_libra_proptest_helpers") (v "0.0.1-sol4") (d (list (d (n "crossbeam") (r "^0.7.2") (d #t) (k 0)) (d (n "proptest") (r "^0.9.4") (d #t) (k 0)) (d (n "proptest-derive") (r "^0.1.2") (d #t) (k 0)))) (h "18xz9pqqccd92azkg1wq12bdryd6ycdyy0daxr1lpally1713pkb")))

(define-public crate-solana_libra_proptest_helpers-0.0.1-sol5 (c (n "solana_libra_proptest_helpers") (v "0.0.1-sol5") (d (list (d (n "crossbeam") (r "^0.7.2") (d #t) (k 0)) (d (n "proptest") (r "^0.9.4") (d #t) (k 0)) (d (n "proptest-derive") (r "^0.1.2") (d #t) (k 0)))) (h "1qk7al928cwkad0pg0hazn32sdvijbyr4xpn28m01z550divk670")))

