(define-module (crates-io so la solana-unified-scheduler-logic) #:use-module (crates-io))

(define-public crate-solana-unified-scheduler-logic-0.0.0 (c (n "solana-unified-scheduler-logic") (v "0.0.0") (h "1b0f5f4bc2057gfrml981rfg3ysyj05yf2vyfdhgildxlpzkzdi2")))

(define-public crate-solana-unified-scheduler-logic-1.18.0 (c (n "solana-unified-scheduler-logic") (v "1.18.0") (h "1v123xvaiql4x1y1ydcddz7gvaikplqp96396rc2sl00nh8s404n")))

(define-public crate-solana-unified-scheduler-logic-1.18.1 (c (n "solana-unified-scheduler-logic") (v "1.18.1") (h "0i5jrf2a9rnvvl3vr6rm319nxqi58xqf82iay32nkl9713my1c1m")))

(define-public crate-solana-unified-scheduler-logic-1.18.2 (c (n "solana-unified-scheduler-logic") (v "1.18.2") (h "1jraah3wpybc6zmh33f8bli1r2jjbxn5phgdj38xm1xzr04fyb58")))

(define-public crate-solana-unified-scheduler-logic-1.18.3 (c (n "solana-unified-scheduler-logic") (v "1.18.3") (h "10acy1rzn6ak8v465dn5y5kk65bgh2lz00p2qf6dbz95qfcs6529")))

(define-public crate-solana-unified-scheduler-logic-1.18.4 (c (n "solana-unified-scheduler-logic") (v "1.18.4") (h "04m67m298w2fmq608pvvm36sqlj7xh2hjwc8446m1i75lhvzdzb1")))

(define-public crate-solana-unified-scheduler-logic-1.18.5 (c (n "solana-unified-scheduler-logic") (v "1.18.5") (h "045cbp43k530895b8pgxq13hvfq9qylmm3if4w40nnpsmm2jzix6")))

(define-public crate-solana-unified-scheduler-logic-1.18.6 (c (n "solana-unified-scheduler-logic") (v "1.18.6") (h "1xpbrvla0wv93sqppdzwkjcf0jghyijdj71wyv2dvgwianw3h5vx")))

(define-public crate-solana-unified-scheduler-logic-1.18.7 (c (n "solana-unified-scheduler-logic") (v "1.18.7") (h "0svz2xrk7k2fmp7vzl4nhf56cb5acpcnxp0acbiaa9c8mkrg2y1x")))

(define-public crate-solana-unified-scheduler-logic-1.18.8 (c (n "solana-unified-scheduler-logic") (v "1.18.8") (h "12qygpkwsrd43f3vf5zq099m092iyhz6sr8czjhpsxchckyg4dzi")))

(define-public crate-solana-unified-scheduler-logic-1.18.9 (c (n "solana-unified-scheduler-logic") (v "1.18.9") (h "1w7gdj86qc4hms025gr4d3br5blbwgxf8bk92bli4xcj4wam1hy0")))

(define-public crate-solana-unified-scheduler-logic-1.18.10 (c (n "solana-unified-scheduler-logic") (v "1.18.10") (h "1pv4q0fcigf330zqvriy2019dgvfyw33608d3p70vm57aqiz2mvl")))

(define-public crate-solana-unified-scheduler-logic-1.18.11 (c (n "solana-unified-scheduler-logic") (v "1.18.11") (h "04455kz4qbl014qdmpbk832l4v4r703myzlln8sm25lvwb863lsz")))

(define-public crate-solana-unified-scheduler-logic-1.18.12 (c (n "solana-unified-scheduler-logic") (v "1.18.12") (h "155pmc1yd5wj4074fib9rjjr54pzqdrq9h7y1lkm6ddd45412pqr")))

(define-public crate-solana-unified-scheduler-logic-1.18.13 (c (n "solana-unified-scheduler-logic") (v "1.18.13") (h "0ncp091h0inhr4ya1s39pb6mgg94k5y54hk0wf0qqn7wc050rdrw")))

(define-public crate-solana-unified-scheduler-logic-1.18.14 (c (n "solana-unified-scheduler-logic") (v "1.18.14") (h "0ddaq2iygy9i9ihvkhs5z7yf2i59d8h1h3hcmffaa86y94bfwaf4")))

