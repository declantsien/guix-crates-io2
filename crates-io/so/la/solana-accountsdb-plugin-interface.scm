(define-module (crates-io so la solana-accountsdb-plugin-interface) #:use-module (crates-io))

(define-public crate-solana-accountsdb-plugin-interface-1.8.1 (c (n "solana-accountsdb-plugin-interface") (v "1.8.1") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.29") (d #t) (k 0)))) (h "0zj59xzx0p5ipm2f15j8g98192lg5q1cviz7nwlj7iga66q0d9mr")))

(define-public crate-solana-accountsdb-plugin-interface-1.8.2 (c (n "solana-accountsdb-plugin-interface") (v "1.8.2") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.29") (d #t) (k 0)))) (h "0v500ia4jmhl6ilxd3n7fa5idq2q6za1dcd3k8pdz5hrcbdj8jii")))

(define-public crate-solana-accountsdb-plugin-interface-1.8.3 (c (n "solana-accountsdb-plugin-interface") (v "1.8.3") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.29") (d #t) (k 0)))) (h "0qa04jhs6nmhpq9pnpy9gh9v28ii5cxikh7ms4livfnwn1fxdqfx")))

(define-public crate-solana-accountsdb-plugin-interface-1.8.4 (c (n "solana-accountsdb-plugin-interface") (v "1.8.4") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.29") (d #t) (k 0)))) (h "09v0ddfdpfs9q5351k064jw14d66bbrs07sl6rx1jsn058zm6r98")))

(define-public crate-solana-accountsdb-plugin-interface-1.8.5 (c (n "solana-accountsdb-plugin-interface") (v "1.8.5") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.29") (d #t) (k 0)))) (h "16s90dq42531d3qi23g1vhf30bq679gfc1f3c52k5cbdfgkapw4z")))

(define-public crate-solana-accountsdb-plugin-interface-1.8.6 (c (n "solana-accountsdb-plugin-interface") (v "1.8.6") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.29") (d #t) (k 0)))) (h "14nvmvdv17i074ki683l0wapsjwiqag2ypgpwkd1kc9f3ipr2hym")))

(define-public crate-solana-accountsdb-plugin-interface-1.8.7 (c (n "solana-accountsdb-plugin-interface") (v "1.8.7") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.29") (d #t) (k 0)))) (h "0hh5583i90gr2xq1va9msqmwd8lzs8vmc94pfsxhrrx8x504xbl2")))

(define-public crate-solana-accountsdb-plugin-interface-1.8.8 (c (n "solana-accountsdb-plugin-interface") (v "1.8.8") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.29") (d #t) (k 0)))) (h "1kly6vx5pb7q0afp9s2z5znz0gk5k7fx2gzqlav2jf4vrxfval50")))

(define-public crate-solana-accountsdb-plugin-interface-1.8.9 (c (n "solana-accountsdb-plugin-interface") (v "1.8.9") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.29") (d #t) (k 0)))) (h "1x2n0rwvk2jnfcgimqz73pjfqclhpgpw2sk2cqw2z0axxpywgfhf")))

(define-public crate-solana-accountsdb-plugin-interface-1.9.0 (c (n "solana-accountsdb-plugin-interface") (v "1.9.0") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.9.0") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.9.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0pxmb3h674y8lr3v363d1bmbdwl9pz0378gf3b7a3pxv473rr1h6")))

(define-public crate-solana-accountsdb-plugin-interface-1.8.10 (c (n "solana-accountsdb-plugin-interface") (v "1.8.10") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.29") (d #t) (k 0)))) (h "1zw4pr75k21xf83ybi65120cyn27y460df79sfv02bii4ppv7wnc")))

(define-public crate-solana-accountsdb-plugin-interface-1.8.11 (c (n "solana-accountsdb-plugin-interface") (v "1.8.11") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.29") (d #t) (k 0)))) (h "0a52q1nchc6sviipr8j0ccgfzdzkad7c5k9hb527xnmch3wnppb8")))

(define-public crate-solana-accountsdb-plugin-interface-1.9.1 (c (n "solana-accountsdb-plugin-interface") (v "1.9.1") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.9.1") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.9.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1bdqai0lkk9q55qgfwlynd4rhq2sc7mb2arw1xh5lj6bk20q8v75")))

(define-public crate-solana-accountsdb-plugin-interface-1.9.2 (c (n "solana-accountsdb-plugin-interface") (v "1.9.2") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.9.2") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.9.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "16l2ks74p6ys478ric5x6zlaw92wy5z6kxnph9hik6hhs0z3bq1y")))

(define-public crate-solana-accountsdb-plugin-interface-1.9.3 (c (n "solana-accountsdb-plugin-interface") (v "1.9.3") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.9.3") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.9.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0x5vv1jgfz42bdbkqr7b59y3d8a8d7y1zqpnia2qdg4894hi167j")))

(define-public crate-solana-accountsdb-plugin-interface-1.8.12 (c (n "solana-accountsdb-plugin-interface") (v "1.8.12") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.29") (d #t) (k 0)))) (h "0892y8ji5b26j4sqcc913r5m4yplnhilgrlaz6m32g376n1sck88")))

(define-public crate-solana-accountsdb-plugin-interface-1.9.4 (c (n "solana-accountsdb-plugin-interface") (v "1.9.4") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.9.4") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.9.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1iqymcqhdrch7klfqqff0p09isjb4iz8jggx4ia53mnc701d6yqw")))

(define-public crate-solana-accountsdb-plugin-interface-1.8.13 (c (n "solana-accountsdb-plugin-interface") (v "1.8.13") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.29") (d #t) (k 0)))) (h "0cl33nf3zwqg81gir38adrgg5fhlfc3bnn0q0qva1d5rll71g9jy")))

(define-public crate-solana-accountsdb-plugin-interface-1.9.5 (c (n "solana-accountsdb-plugin-interface") (v "1.9.5") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.9.5") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.9.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "01zk6dd2bz72gq99mn803pg35rvmnb3pha8156x9xcx5n7s8bdbi")))

(define-public crate-solana-accountsdb-plugin-interface-1.8.14 (c (n "solana-accountsdb-plugin-interface") (v "1.8.14") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.29") (d #t) (k 0)))) (h "0qrpjbq3cd91wi7l3ydmhz9bvbimd61gz6g54q5863vlsa393pds")))

(define-public crate-solana-accountsdb-plugin-interface-1.9.6 (c (n "solana-accountsdb-plugin-interface") (v "1.9.6") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.9.6") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.9.6") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1kdr8dhnb7096l9iih3ii41vm0anipcm6yx7kda5mqkpvrvx3phg")))

(define-public crate-solana-accountsdb-plugin-interface-1.9.7 (c (n "solana-accountsdb-plugin-interface") (v "1.9.7") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.9.7") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.9.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1a8g0523qyzidwr7j92b623kig3lm16vh4hyag4abqbzamy2h0az")))

(define-public crate-solana-accountsdb-plugin-interface-1.8.16 (c (n "solana-accountsdb-plugin-interface") (v "1.8.16") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.29") (d #t) (k 0)))) (h "0xa3wcmh7cr289i2asljzwvrxdgzxa5h766f65k3y6v84zw75afq")))

(define-public crate-solana-accountsdb-plugin-interface-1.9.8 (c (n "solana-accountsdb-plugin-interface") (v "1.9.8") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.9.8") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.9.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1gzn3nw4y6xl5plnaipbgxbmb8grczspb8kkq2kyz1ynybxdbnms")))

(define-public crate-solana-accountsdb-plugin-interface-1.9.9 (c (n "solana-accountsdb-plugin-interface") (v "1.9.9") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.9.9") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.9.9") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1kmcf31ddyxijadba946an408nnpk85995xfy0a1dl6csdc2pmxd")))

(define-public crate-solana-accountsdb-plugin-interface-1.10.0 (c (n "solana-accountsdb-plugin-interface") (v "1.10.0") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.10.0") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.10.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0dshi7jrnds0cpix7g8j5rblng1p7jgp35m5s1kl739jbfx253hw")))

(define-public crate-solana-accountsdb-plugin-interface-1.9.10 (c (n "solana-accountsdb-plugin-interface") (v "1.9.10") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.9.10") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.9.10") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1xfrkahllfi930f7m9dbqkf8zvm2i1y0hf2gcgrv1xkbp8mn1hgh")))

(define-public crate-solana-accountsdb-plugin-interface-1.9.11 (c (n "solana-accountsdb-plugin-interface") (v "1.9.11") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.9.11") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.9.11") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1gg0z4ndgd211yyccrick98agp526qv05xl3k3knsvmgjkkixklj")))

(define-public crate-solana-accountsdb-plugin-interface-1.9.12 (c (n "solana-accountsdb-plugin-interface") (v "1.9.12") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.9.12") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.9.12") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "171l5qznz4552529mva3iyklkadjp9ymg1hkz01kmmhzw0w5cgwh")))

(define-public crate-solana-accountsdb-plugin-interface-1.10.1 (c (n "solana-accountsdb-plugin-interface") (v "1.10.1") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.10.1") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.10.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0s43mf0gblag8bz24nhd8w69lss6kwcxjcm326aq21b3060fzd68")))

(define-public crate-solana-accountsdb-plugin-interface-1.10.2 (c (n "solana-accountsdb-plugin-interface") (v "1.10.2") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.10.2") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.10.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1n349fcplkid4b625g8znm85a8gf0wbb64sc5fr5vy3yzyhpy06x")))

