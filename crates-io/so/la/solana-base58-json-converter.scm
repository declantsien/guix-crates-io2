(define-module (crates-io so la solana-base58-json-converter) #:use-module (crates-io))

(define-public crate-solana-base58-json-converter-0.1.0 (c (n "solana-base58-json-converter") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "bs58") (r "^0.4") (d #t) (k 0)) (d (n "fehler") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "shellexpand") (r "^2") (d #t) (k 0)) (d (n "solana-sdk") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0011hnn24s0jmgn3ycr7ghs7m3zi2g8vnddwlpjia57r3zfmyzk2")))

