(define-module (crates-io so la solarti-discriminator-syn) #:use-module (crates-io))

(define-public crate-solarti-discriminator-syn-0.1.1 (c (n "solarti-discriminator-syn") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "14jfmjycfqsbysd3h5rc75n2nzv6nqqd08lz6vgdswz5q757m4vr")))

(define-public crate-solarti-discriminator-syn-0.1.2 (c (n "solarti-discriminator-syn") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1jpijms7lgq8qpz90m4hbbpa1q0lgbrfkan3nn40xnsd3xik1irq")))

(define-public crate-solarti-discriminator-syn-0.1.3 (c (n "solarti-discriminator-syn") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0z9ic95ivgx4gc9vnk4fg4ky9y7ivgsvkilvznm1x7f030x3kzk1")))

(define-public crate-solarti-discriminator-syn-0.1.4 (c (n "solarti-discriminator-syn") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0zbnrvfdmbc5fvqh64kcp99vkkmy2g2jx5iivg3qrp5nl4xavqfk")))

(define-public crate-solarti-discriminator-syn-0.1.5 (c (n "solarti-discriminator-syn") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1gp96kxf38732k2f9jzx73q3kc2qc8khhwmqds7v29pilq8pf92c")))

(define-public crate-solarti-discriminator-syn-0.1.6 (c (n "solarti-discriminator-syn") (v "0.1.6") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1bghy33vb10z9dqdxzjrf3x0y5wldlrdzbm8hkzwq6x0qqzc0b4r")))

