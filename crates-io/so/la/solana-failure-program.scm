(define-module (crates-io so la solana-failure-program) #:use-module (crates-io))

(define-public crate-solana-failure-program-0.15.0 (c (n "solana-failure-program") (v "0.15.0") (d (list (d (n "log") (r "^0.4.2") (d #t) (k 0)) (d (n "solana-runtime") (r "^0.15.0") (d #t) (k 2)) (d (n "solana-sdk") (r "^0.15.0") (d #t) (k 0)))) (h "0zsxfg8kz1wn5cixg81vf332mipylbf2lpyzljz7wap57ngyf0dd")))

(define-public crate-solana-failure-program-0.15.3 (c (n "solana-failure-program") (v "0.15.3") (d (list (d (n "log") (r "^0.4.2") (d #t) (k 0)) (d (n "solana-runtime") (r "^0.15.3") (d #t) (k 2)) (d (n "solana-sdk") (r "^0.15.3") (d #t) (k 0)))) (h "1174cl8q5225ycjw52kvh2w7qvhz5pamznl6nmrjhzk4h8r37qny")))

(define-public crate-solana-failure-program-0.16.0 (c (n "solana-failure-program") (v "0.16.0") (d (list (d (n "log") (r "^0.4.2") (d #t) (k 0)) (d (n "solana-runtime") (r "^0.16.0") (d #t) (k 2)) (d (n "solana-sdk") (r "^0.16.0") (d #t) (k 0)))) (h "13l9dd3qpj1wpyx0xrbgdlx5cq1nx0k7lvkil51a23vw4923bg3i")))

(define-public crate-solana-failure-program-0.16.1 (c (n "solana-failure-program") (v "0.16.1") (d (list (d (n "log") (r "^0.4.2") (d #t) (k 0)) (d (n "solana-runtime") (r "^0.16.1") (d #t) (k 2)) (d (n "solana-sdk") (r "^0.16.1") (d #t) (k 0)))) (h "0g5apdrpyiyylma00shknli8l0yajspdjrzdpj3xi3kcc2ynwia5")))

(define-public crate-solana-failure-program-0.16.3 (c (n "solana-failure-program") (v "0.16.3") (d (list (d (n "log") (r "^0.4.2") (d #t) (k 0)) (d (n "solana-runtime") (r "^0.16.3") (d #t) (k 2)) (d (n "solana-sdk") (r "^0.16.3") (d #t) (k 0)))) (h "0q3lxwkdv91nvmzs2dfga32x5rl1pp990sprcqmrpds5mrm2rb7m")))

(define-public crate-solana-failure-program-0.16.4 (c (n "solana-failure-program") (v "0.16.4") (d (list (d (n "log") (r "^0.4.2") (d #t) (k 0)) (d (n "solana-runtime") (r "^0.16.4") (d #t) (k 2)) (d (n "solana-sdk") (r "^0.16.4") (d #t) (k 0)))) (h "1psgj7y03q1ga69vpv28fw18cakm2nak3cfc6ixc34l0gjr77h75")))

(define-public crate-solana-failure-program-0.16.6 (c (n "solana-failure-program") (v "0.16.6") (d (list (d (n "log") (r "^0.4.2") (d #t) (k 0)) (d (n "solana-runtime") (r "^0.16.6") (d #t) (k 2)) (d (n "solana-sdk") (r "^0.16.6") (d #t) (k 0)))) (h "178p07y5yribrzi4rc43p39vbz3qqbsbpgqibw32qimks7sbhj74")))

(define-public crate-solana-failure-program-0.17.0 (c (n "solana-failure-program") (v "0.17.0") (d (list (d (n "log") (r "^0.4.7") (d #t) (k 0)) (d (n "solana-runtime") (r "^0.17.0") (d #t) (k 2)) (d (n "solana-sdk") (r "^0.17.0") (d #t) (k 0)))) (h "1ygdnrr7mdhn2z9mmr4pr0bwn2slcq56fmyz923rbbapg2i5aasm")))

(define-public crate-solana-failure-program-0.17.1 (c (n "solana-failure-program") (v "0.17.1") (d (list (d (n "log") (r "^0.4.7") (d #t) (k 0)) (d (n "solana-runtime") (r "^0.17.1") (d #t) (k 2)) (d (n "solana-sdk") (r "^0.17.1") (d #t) (k 0)))) (h "1md8nsm2niwrb2dai76imkilibs83xhp4ss7797ar1xaqqxp7q01")))

(define-public crate-solana-failure-program-0.18.0-pre1 (c (n "solana-failure-program") (v "0.18.0-pre1") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-runtime") (r "^0.18.0-pre1") (d #t) (k 2)) (d (n "solana-sdk") (r "^0.18.0-pre1") (d #t) (k 0)))) (h "0qikwl8yy36zk8fap1iym0c6gw3m9xaj41ankhfj9ypk2rjl37ng")))

(define-public crate-solana-failure-program-0.17.2 (c (n "solana-failure-program") (v "0.17.2") (d (list (d (n "log") (r "^0.4.7") (d #t) (k 0)) (d (n "solana-runtime") (r "^0.17.2") (d #t) (k 2)) (d (n "solana-sdk") (r "^0.17.2") (d #t) (k 0)))) (h "081475pw3402qlzr18cj5i3283br85qx6agv2s1v71r5v9w0pfx2")))

(define-public crate-solana-failure-program-0.18.0-pre2 (c (n "solana-failure-program") (v "0.18.0-pre2") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-runtime") (r "^0.18.0-pre2") (d #t) (k 2)) (d (n "solana-sdk") (r "^0.18.0-pre2") (d #t) (k 0)))) (h "0bciya7qp33fn7m990ccac3airil5ask9f5g3v9kcip5k4gwi3bs")))

(define-public crate-solana-failure-program-0.18.0 (c (n "solana-failure-program") (v "0.18.0") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-runtime") (r "^0.18.0") (d #t) (k 2)) (d (n "solana-sdk") (r "^0.18.0") (d #t) (k 0)))) (h "0pb2c55v3izkx56y666rw274bbbqiyxbpccfsj4pwd2km51s5n67")))

(define-public crate-solana-failure-program-0.18.1 (c (n "solana-failure-program") (v "0.18.1") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-runtime") (r "^0.18.1") (d #t) (k 2)) (d (n "solana-sdk") (r "^0.18.1") (d #t) (k 0)))) (h "1kf6a2fkbzg5j4lkbwlh8chj3ki0mccbb54m9kj7inka26lssr7z")))

(define-public crate-solana-failure-program-0.19.1 (c (n "solana-failure-program") (v "0.19.1") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-runtime") (r "^0.19.1") (d #t) (k 2)) (d (n "solana-sdk") (r "^0.19.1") (d #t) (k 0)))) (h "0hjalb8mjkbmwxpagxjv628is9zqx0gmalw6jqbr23bcf92rxl7q")))

(define-public crate-solana-failure-program-0.20.1 (c (n "solana-failure-program") (v "0.20.1") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-runtime") (r "^0.20.1") (d #t) (k 2)) (d (n "solana-sdk") (r "^0.20.1") (d #t) (k 0)))) (h "17ng3rm5zlzmqg44p46g80bshqkzkyqjprsdb9dzjrn8g5p686sn")))

(define-public crate-solana-failure-program-0.20.2 (c (n "solana-failure-program") (v "0.20.2") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-runtime") (r "^0.20.2") (d #t) (k 2)) (d (n "solana-sdk") (r "^0.20.2") (d #t) (k 0)))) (h "11wx0pmn0r5myrilzgmcxk7jqn14ckv4g3n27i8kna4h2sw0kqa8")))

(define-public crate-solana-failure-program-0.20.3 (c (n "solana-failure-program") (v "0.20.3") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-runtime") (r "^0.20.3") (d #t) (k 2)) (d (n "solana-sdk") (r "^0.20.3") (d #t) (k 0)))) (h "0nh58xqbsfgbwcg40cqdp0z2kswd8i05wpqlpikdbjvsld55m59c")))

(define-public crate-solana-failure-program-0.20.4 (c (n "solana-failure-program") (v "0.20.4") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-runtime") (r "^0.20.4") (d #t) (k 2)) (d (n "solana-sdk") (r "^0.20.4") (d #t) (k 0)))) (h "0p6vk9kz3hb759j96qfh4axsr28g29s5dxdx9qzyw0601xk8cg65")))

(define-public crate-solana-failure-program-0.20.5 (c (n "solana-failure-program") (v "0.20.5") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-runtime") (r "^0.20.5") (d #t) (k 2)) (d (n "solana-sdk") (r "^0.20.5") (d #t) (k 0)))) (h "1vzlf62c8wjsjjzdh5mi6r733yifkh6v4abd74rsyra8jpj1yyl0")))

(define-public crate-solana-failure-program-0.22.0 (c (n "solana-failure-program") (v "0.22.0") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-runtime") (r "^0.22.0") (d #t) (k 2)) (d (n "solana-sdk") (r "^0.22.0") (d #t) (k 0)))) (h "1bx2hf45brmlpa9hzf50wgrx46n9raj3ij4c7g9ikqvivcjmbzzk")))

(define-public crate-solana-failure-program-0.22.1 (c (n "solana-failure-program") (v "0.22.1") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-runtime") (r "^0.22.1") (d #t) (k 2)) (d (n "solana-sdk") (r "^0.22.1") (d #t) (k 0)))) (h "10qzn8811zvw8pw7ndj7ha075gv33l5bfvyg1ldj8v0vwnnsaliq")))

(define-public crate-solana-failure-program-0.22.2 (c (n "solana-failure-program") (v "0.22.2") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-runtime") (r "^0.22.2") (d #t) (k 2)) (d (n "solana-sdk") (r "^0.22.2") (d #t) (k 0)))) (h "0a21mzbjshjikyqh88i7yg9aywls8089dy0vij261z0kr4pv43ka")))

(define-public crate-solana-failure-program-0.22.3 (c (n "solana-failure-program") (v "0.22.3") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-runtime") (r "^0.22.3") (d #t) (k 2)) (d (n "solana-sdk") (r "^0.22.3") (d #t) (k 0)))) (h "0vdm1phkr3b47dyiakq8yjvxylpgz47sy29qhdx6a08qd1i2wh13")))

(define-public crate-solana-failure-program-0.22.4 (c (n "solana-failure-program") (v "0.22.4") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-runtime") (r "^0.22.4") (d #t) (k 2)) (d (n "solana-sdk") (r "^0.22.4") (d #t) (k 0)))) (h "1rxp3n2pivx8dvqr2833cy53yq8g8phj2q98idqqbfsvbar93l4a")))

(define-public crate-solana-failure-program-0.23.0 (c (n "solana-failure-program") (v "0.23.0") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-runtime") (r "^0.23.0") (d #t) (k 2)) (d (n "solana-sdk") (r "^0.23.0") (d #t) (k 0)))) (h "0vd5yf29g0gjnr586390k4bd2p9369qahn282ygr46wwfv1lbv77")))

(define-public crate-solana-failure-program-0.22.5 (c (n "solana-failure-program") (v "0.22.5") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-runtime") (r "^0.22.5") (d #t) (k 2)) (d (n "solana-sdk") (r "^0.22.5") (d #t) (k 0)))) (h "05kav142r7smr3ksprbc6z1x4d1igyc55x68w4bs5d065rbgiqqz")))

(define-public crate-solana-failure-program-0.22.6 (c (n "solana-failure-program") (v "0.22.6") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-runtime") (r "^0.22.6") (d #t) (k 2)) (d (n "solana-sdk") (r "^0.22.6") (d #t) (k 0)))) (h "1pj1achw1d9kl3x6xkhklwjh2rvylrckcxm4vmp8sdydzi2ypymk")))

(define-public crate-solana-failure-program-0.23.1 (c (n "solana-failure-program") (v "0.23.1") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-runtime") (r "^0.23.1") (d #t) (k 2)) (d (n "solana-sdk") (r "^0.23.1") (d #t) (k 0)))) (h "0wf7wv0rlvksq68nm1cyiyc2m4fn7jf225sq48cg0c0hpmcd3yzh")))

(define-public crate-solana-failure-program-0.22.7 (c (n "solana-failure-program") (v "0.22.7") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-runtime") (r "^0.22.7") (d #t) (k 2)) (d (n "solana-sdk") (r "^0.22.7") (d #t) (k 0)))) (h "01wrhv8y451yxm4vgn17h0jz975imgi3sdzpfxj8qwj5wpfxhw01")))

(define-public crate-solana-failure-program-0.23.2 (c (n "solana-failure-program") (v "0.23.2") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-runtime") (r "^0.23.2") (d #t) (k 2)) (d (n "solana-sdk") (r "^0.23.2") (d #t) (k 0)))) (h "0442las521d8b2jr71l4f1d1b48lixr5y7fxqki94b8l57ll7sh1")))

(define-public crate-solana-failure-program-0.23.3 (c (n "solana-failure-program") (v "0.23.3") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-runtime") (r "^0.23.3") (d #t) (k 2)) (d (n "solana-sdk") (r "^0.23.3") (d #t) (k 0)))) (h "1hwfl97cff7bnj3g59hdhidr66gkznxgdbx9103krlq3nh4b9jgv")))

(define-public crate-solana-failure-program-0.23.4 (c (n "solana-failure-program") (v "0.23.4") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-runtime") (r "^0.23.4") (d #t) (k 2)) (d (n "solana-sdk") (r "^0.23.4") (d #t) (k 0)))) (h "0lnyzndbarr0pq9aap8rdfkgchr685yypslx2d5srl3p7akpl3l8")))

(define-public crate-solana-failure-program-0.22.8 (c (n "solana-failure-program") (v "0.22.8") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-runtime") (r "^0.22.8") (d #t) (k 2)) (d (n "solana-sdk") (r "^0.22.8") (d #t) (k 0)))) (h "06lf0gkdzgixpm6g45xcdv9zqmzibmc313w6v4pjahxm9n7i1y4g")))

(define-public crate-solana-failure-program-0.23.5 (c (n "solana-failure-program") (v "0.23.5") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-runtime") (r "^0.23.5") (d #t) (k 2)) (d (n "solana-sdk") (r "^0.23.5") (d #t) (k 0)))) (h "1fffgpx92l3r2lgw2975s0ys3bf575yh1vc5gwi8cr82h56795in")))

(define-public crate-solana-failure-program-0.23.6 (c (n "solana-failure-program") (v "0.23.6") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-runtime") (r "^0.23.6") (d #t) (k 2)) (d (n "solana-sdk") (r "^0.23.6") (d #t) (k 0)))) (h "0gmym839bk71iipgq1wkx9yryapv978w4d5xgaahg81a3f4yf4s3")))

(define-public crate-solana-failure-program-1.0.0 (c (n "solana-failure-program") (v "1.0.0") (d (list (d (n "solana-runtime") (r "^1.0.0") (d #t) (k 2)) (d (n "solana-sdk") (r "^1.0.0") (d #t) (k 0)))) (h "0wzjlr4fn05h2x28w005xq28y9p6435nxn4yy5r5jvjr34xbc8kg")))

(define-public crate-solana-failure-program-0.23.7 (c (n "solana-failure-program") (v "0.23.7") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-runtime") (r "^0.23.7") (d #t) (k 2)) (d (n "solana-sdk") (r "^0.23.7") (d #t) (k 0)))) (h "1q22wqamlfwas5c8yq4c38qcr4hl80fs697klr0abiyfqhn4hmdq")))

(define-public crate-solana-failure-program-0.23.8 (c (n "solana-failure-program") (v "0.23.8") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-runtime") (r "^0.23.8") (d #t) (k 2)) (d (n "solana-sdk") (r "^0.23.8") (d #t) (k 0)))) (h "1jp7q856nga4gzkhq08dzn02caj45g1zq94xda8qarklr4q0ns8p")))

(define-public crate-solana-failure-program-1.0.1 (c (n "solana-failure-program") (v "1.0.1") (d (list (d (n "solana-runtime") (r "^1.0.1") (d #t) (k 2)) (d (n "solana-sdk") (r "^1.0.1") (d #t) (k 0)))) (h "1qh0sb0jl6h80ikq25cnw406l80bl8cx306gl4vqmviv8p2gzwfy")))

(define-public crate-solana-failure-program-1.0.2 (c (n "solana-failure-program") (v "1.0.2") (d (list (d (n "solana-runtime") (r "^1.0.2") (d #t) (k 2)) (d (n "solana-sdk") (r "^1.0.2") (d #t) (k 0)))) (h "0vry5yw83k36g9q4r9kmq6q0m2hpw7a3mk5vhfqvzmc4nmvv90a2")))

(define-public crate-solana-failure-program-1.0.3 (c (n "solana-failure-program") (v "1.0.3") (d (list (d (n "solana-runtime") (r "^1.0.3") (d #t) (k 2)) (d (n "solana-sdk") (r "^1.0.3") (d #t) (k 0)))) (h "1hxlfnkxz97ms5c1f87gq6ymknjk4zfmgi582nnbd13w3zancz8j")))

(define-public crate-solana-failure-program-1.0.4 (c (n "solana-failure-program") (v "1.0.4") (d (list (d (n "solana-runtime") (r "^1.0.4") (d #t) (k 2)) (d (n "solana-sdk") (r "^1.0.4") (d #t) (k 0)))) (h "1fbqbxwccf0gw0jy7h4v7kyr1drlpndw4l9q4r0jg805r36bzxhp")))

(define-public crate-solana-failure-program-1.0.5 (c (n "solana-failure-program") (v "1.0.5") (d (list (d (n "solana-runtime") (r "^1.0.5") (d #t) (k 2)) (d (n "solana-sdk") (r "^1.0.5") (d #t) (k 0)))) (h "06s98vfzvfwrhlqpy53jr141mi03fyk345pvbfdwiax6crqnzp8d")))

(define-public crate-solana-failure-program-1.0.6 (c (n "solana-failure-program") (v "1.0.6") (d (list (d (n "solana-runtime") (r "^1.0.6") (d #t) (k 2)) (d (n "solana-sdk") (r "^1.0.6") (d #t) (k 0)))) (h "0xv54dx9nkf0z6kxv3hns3fl5ngv45h4qc281lq5kk2if1rpy5l0")))

(define-public crate-solana-failure-program-1.0.7 (c (n "solana-failure-program") (v "1.0.7") (d (list (d (n "solana-runtime") (r "^1.0.7") (d #t) (k 2)) (d (n "solana-sdk") (r "^1.0.7") (d #t) (k 0)))) (h "0cvd9sbkfxi4pwaglwayr93zqv9gvcyzq64lhx5kay9wq0qc8rx1")))

(define-public crate-solana-failure-program-1.0.8 (c (n "solana-failure-program") (v "1.0.8") (d (list (d (n "solana-runtime") (r "^1.0.8") (d #t) (k 2)) (d (n "solana-sdk") (r "^1.0.8") (d #t) (k 0)))) (h "1ny3ylsavms1jmj5x2d6iljgp50m31yhdh1q3h0z27pv3x64vb7i")))

(define-public crate-solana-failure-program-1.0.9 (c (n "solana-failure-program") (v "1.0.9") (d (list (d (n "solana-runtime") (r "^1.0.9") (d #t) (k 2)) (d (n "solana-sdk") (r "^1.0.9") (d #t) (k 0)))) (h "1kkpdprkyddpfkzcb2f48q3kbh351wy9k7jbw0qvwpgd1pp2ry0g")))

(define-public crate-solana-failure-program-1.0.10 (c (n "solana-failure-program") (v "1.0.10") (d (list (d (n "solana-runtime") (r "^1.0.10") (d #t) (k 2)) (d (n "solana-sdk") (r "^1.0.10") (d #t) (k 0)))) (h "14hqhmdi9iql6ih7in7valf5psipj4l5w6r42ng8hazkzr9cnlfa")))

(define-public crate-solana-failure-program-1.0.11 (c (n "solana-failure-program") (v "1.0.11") (d (list (d (n "solana-runtime") (r "^1.0.11") (d #t) (k 2)) (d (n "solana-sdk") (r "^1.0.11") (d #t) (k 0)))) (h "0yb6xd45c8d0asnfk4ffnjvfyq0ywsk8iz4xb29kqhjp3ilazh3w")))

(define-public crate-solana-failure-program-1.1.0 (c (n "solana-failure-program") (v "1.1.0") (d (list (d (n "solana-runtime") (r "^1.1.0") (d #t) (k 2)) (d (n "solana-sdk") (r "^1.1.0") (d #t) (k 0)))) (h "1sycfykh47r76ghwh4fhxyl8frxgb1yq2li597mwv8rbljgpdri3")))

(define-public crate-solana-failure-program-1.1.1 (c (n "solana-failure-program") (v "1.1.1") (d (list (d (n "solana-runtime") (r "^1.1.1") (d #t) (k 2)) (d (n "solana-sdk") (r "^1.1.1") (d #t) (k 0)))) (h "0sxc4szzgy00zvfjxknlqgayad8v8q5112x551zy0g5sp50v16mc")))

(define-public crate-solana-failure-program-1.0.13 (c (n "solana-failure-program") (v "1.0.13") (d (list (d (n "solana-runtime") (r "^1.0.13") (d #t) (k 2)) (d (n "solana-sdk") (r "^1.0.13") (d #t) (k 0)))) (h "109h8m1i3f9s1bgkln1827s16rcw85s6i0lnh2fvqlk1p8inxi5d")))

(define-public crate-solana-failure-program-1.1.2 (c (n "solana-failure-program") (v "1.1.2") (d (list (d (n "solana-runtime") (r "^1.1.2") (d #t) (k 2)) (d (n "solana-sdk") (r "^1.1.2") (d #t) (k 0)))) (h "11rj9xadgp5rz0wbq48d9qys12wasdiyfvxwr5b2q3zd606k0pvk")))

(define-public crate-solana-failure-program-1.0.14 (c (n "solana-failure-program") (v "1.0.14") (d (list (d (n "solana-runtime") (r "^1.0.14") (d #t) (k 2)) (d (n "solana-sdk") (r "^1.0.14") (d #t) (k 0)))) (h "1x0shrhaj76xpng06frx77v027ksi86agk7qpsi1jkrf2qay35qf")))

(define-public crate-solana-failure-program-1.0.15 (c (n "solana-failure-program") (v "1.0.15") (d (list (d (n "solana-runtime") (r "^1.0.15") (d #t) (k 2)) (d (n "solana-sdk") (r "^1.0.15") (d #t) (k 0)))) (h "18x0kkmxn32a9rc69bcz7saqnwy0yjs2w6zdcfrpffzgq30hm2k5")))

(define-public crate-solana-failure-program-1.0.16 (c (n "solana-failure-program") (v "1.0.16") (d (list (d (n "solana-runtime") (r "^1.0.16") (d #t) (k 2)) (d (n "solana-sdk") (r "^1.0.16") (d #t) (k 0)))) (h "1vkmvb9s1kfvz885zdl21j3kndv56b2aws9b9xm26vh59aba8f0x")))

(define-public crate-solana-failure-program-1.1.3 (c (n "solana-failure-program") (v "1.1.3") (d (list (d (n "solana-runtime") (r "^1.1.3") (d #t) (k 2)) (d (n "solana-sdk") (r "^1.1.3") (d #t) (k 0)))) (h "1giw134sxz5403rxpppa151d70f4msk5wl5yplcklm4r48xj37m7")))

(define-public crate-solana-failure-program-1.0.17 (c (n "solana-failure-program") (v "1.0.17") (d (list (d (n "solana-runtime") (r "^1.0.17") (d #t) (k 2)) (d (n "solana-sdk") (r "^1.0.17") (d #t) (k 0)))) (h "09919q8wm12jnmpg17pmkxg6ln8jqfd9wxbcmqj4rj01pqazl3pk")))

(define-public crate-solana-failure-program-1.0.18 (c (n "solana-failure-program") (v "1.0.18") (d (list (d (n "solana-runtime") (r "^1.0.18") (d #t) (k 2)) (d (n "solana-sdk") (r "^1.0.18") (d #t) (k 0)))) (h "0f9104kb5ij290whfvfn9wfa91nrawrf0p8irsg60vma7xyf387k")))

(define-public crate-solana-failure-program-1.1.5 (c (n "solana-failure-program") (v "1.1.5") (d (list (d (n "solana-runtime") (r "^1.1.5") (d #t) (k 2)) (d (n "solana-sdk") (r "^1.1.5") (d #t) (k 0)))) (h "17a9bd6ddc9cq18srcsn5shikvbxjyi88cd6dva95x07dzbmcbxa")))

(define-public crate-solana-failure-program-1.1.6 (c (n "solana-failure-program") (v "1.1.6") (d (list (d (n "solana-runtime") (r "^1.1.6") (d #t) (k 2)) (d (n "solana-sdk") (r "^1.1.6") (d #t) (k 0)))) (h "084azghr42czb0sazajxznhs4hx0v96wwlhvyp0bb8ww4y62crb3")))

(define-public crate-solana-failure-program-1.1.7 (c (n "solana-failure-program") (v "1.1.7") (d (list (d (n "solana-runtime") (r "^1.1.7") (d #t) (k 2)) (d (n "solana-sdk") (r "^1.1.7") (d #t) (k 0)))) (h "0d0salpfa704agrjlpqk6qpb9nbp9fv0ax6vi0f9c1742jscv302")))

(define-public crate-solana-failure-program-1.0.20 (c (n "solana-failure-program") (v "1.0.20") (d (list (d (n "solana-runtime") (r "^1.0.20") (d #t) (k 2)) (d (n "solana-sdk") (r "^1.0.20") (d #t) (k 0)))) (h "0m6cc4zi4amqhzq1ayqzlxm70ifw90ll57mlrplxykdakij70v1v")))

(define-public crate-solana-failure-program-1.0.21 (c (n "solana-failure-program") (v "1.0.21") (d (list (d (n "solana-runtime") (r "^1.0.21") (d #t) (k 2)) (d (n "solana-sdk") (r "^1.0.21") (d #t) (k 0)))) (h "10g3hvinrnzkww02vxy4mvm8cbrjka2ld9xrcjhli711vswxzc4l")))

(define-public crate-solana-failure-program-1.1.8 (c (n "solana-failure-program") (v "1.1.8") (d (list (d (n "solana-runtime") (r "^1.1.8") (d #t) (k 2)) (d (n "solana-sdk") (r "^1.1.8") (d #t) (k 0)))) (h "0yg0jb5wfnmd8na4zhc3j5s8p9pvpqqq8mls5agps5br9z1wgvwm")))

(define-public crate-solana-failure-program-1.1.9 (c (n "solana-failure-program") (v "1.1.9") (d (list (d (n "solana-runtime") (r "^1.1.9") (d #t) (k 2)) (d (n "solana-sdk") (r "^1.1.9") (d #t) (k 0)))) (h "084vja23ac31l8b35dxqsragykq6gb4snxkrkimncbxxfj0vjmz5")))

(define-public crate-solana-failure-program-1.0.22 (c (n "solana-failure-program") (v "1.0.22") (d (list (d (n "solana-runtime") (r "^1.0.22") (d #t) (k 2)) (d (n "solana-sdk") (r "^1.0.22") (d #t) (k 0)))) (h "13wrarf71kpzr4x8np42llsigvc9rfgrjpal7nsnvdd9v278cpwn")))

(define-public crate-solana-failure-program-1.1.12 (c (n "solana-failure-program") (v "1.1.12") (d (list (d (n "solana-runtime") (r "^1.1.12") (d #t) (k 2)) (d (n "solana-sdk") (r "^1.1.12") (d #t) (k 0)))) (h "13cxpbxj87wpb7y4ylmnn3krrqdijg4gsh0ahkc9ybhm20k3bxb6")))

(define-public crate-solana-failure-program-1.1.13 (c (n "solana-failure-program") (v "1.1.13") (d (list (d (n "solana-runtime") (r "^1.1.13") (d #t) (k 2)) (d (n "solana-sdk") (r "^1.1.13") (d #t) (k 0)))) (h "1y8sh5b5r8zd5s9ncyn3b0rn9cf5bndg297c14i47g0xdkp1n99r")))

(define-public crate-solana-failure-program-1.0.24 (c (n "solana-failure-program") (v "1.0.24") (d (list (d (n "solana-runtime") (r "^1.0.24") (d #t) (k 2)) (d (n "solana-sdk") (r "^1.0.24") (d #t) (k 0)))) (h "04zyzww5zw59pfghjil7g0f4gmh51ny06xrkaa9hk4hjw477l44v")))

(define-public crate-solana-failure-program-1.2.0 (c (n "solana-failure-program") (v "1.2.0") (d (list (d (n "solana-runtime") (r "^1.2.0") (d #t) (k 2)) (d (n "solana-sdk") (r "^1.2.0") (d #t) (k 0)))) (h "0hpmjdfrpl7kln9v87b769a8q4zimarzy4v8rrmsmvdgbwab7as1")))

(define-public crate-solana-failure-program-1.1.15 (c (n "solana-failure-program") (v "1.1.15") (d (list (d (n "solana-runtime") (r "^1.1.15") (d #t) (k 2)) (d (n "solana-sdk") (r "^1.1.15") (d #t) (k 0)))) (h "0dy6vfryb5srlgkdbxk12k17146f8npzy457gmbxavm3ln7cm6wf")))

(define-public crate-solana-failure-program-1.1.17 (c (n "solana-failure-program") (v "1.1.17") (d (list (d (n "solana-runtime") (r "^1.1.17") (d #t) (k 2)) (d (n "solana-sdk") (r "^1.1.17") (d #t) (k 0)))) (h "0sl3w6rggb0i4n0nd51wzp889g48pf4hgb7bcwjyn9if4ygwy2lw")))

(define-public crate-solana-failure-program-1.2.1 (c (n "solana-failure-program") (v "1.2.1") (d (list (d (n "solana-runtime") (r "^1.2.1") (d #t) (k 2)) (d (n "solana-sdk") (r "^1.2.1") (d #t) (k 0)))) (h "0npglwpdv257hrcc3r0j82myp8wlcrv7x6327pzhz77kg50ghask")))

(define-public crate-solana-failure-program-1.1.18 (c (n "solana-failure-program") (v "1.1.18") (d (list (d (n "solana-runtime") (r "^1.1.18") (d #t) (k 2)) (d (n "solana-sdk") (r "^1.1.18") (d #t) (k 0)))) (h "16j2z1w2fkrw4i5wm8l5s7f40q4sq2y5a1xv68ccrn8qg1vhm5vq")))

(define-public crate-solana-failure-program-1.2.3 (c (n "solana-failure-program") (v "1.2.3") (d (list (d (n "solana-runtime") (r "^1.2.3") (d #t) (k 2)) (d (n "solana-sdk") (r "^1.2.3") (d #t) (k 0)))) (h "1qcwvi222g5nij6z41n44sp876wz7zaisggd3xyxqdyjyyyb7l4q")))

(define-public crate-solana-failure-program-1.2.4 (c (n "solana-failure-program") (v "1.2.4") (d (list (d (n "solana-runtime") (r "^1.2.4") (d #t) (k 2)) (d (n "solana-sdk") (r "^1.2.4") (d #t) (k 0)))) (h "1gca8f9dz2j44ns8j3hll3w4lzx4pm617qzgxnh04x40i64xkghz")))

(define-public crate-solana-failure-program-1.1.19 (c (n "solana-failure-program") (v "1.1.19") (d (list (d (n "solana-runtime") (r "^1.1.19") (d #t) (k 2)) (d (n "solana-sdk") (r "^1.1.19") (d #t) (k 0)))) (h "1wcq7gxb2nfvqr609kilh3j99qykzx0mc59cv2ksh5mf0m1na2j3")))

(define-public crate-solana-failure-program-1.2.5 (c (n "solana-failure-program") (v "1.2.5") (d (list (d (n "solana-runtime") (r "^1.2.5") (d #t) (k 2)) (d (n "solana-sdk") (r "^1.2.5") (d #t) (k 0)))) (h "0sixhvvvsam9xdpbsvsidrssv0571fhf19p8v3pviji8mqsp0mgy")))

(define-public crate-solana-failure-program-1.2.6 (c (n "solana-failure-program") (v "1.2.6") (d (list (d (n "solana-runtime") (r "^1.2.6") (d #t) (k 2)) (d (n "solana-sdk") (r "^1.2.6") (d #t) (k 0)))) (h "03s5nr776fr0arfrw1vjzzlbnqxaa0m3i70rlk9i3380r3qw14yf")))

(define-public crate-solana-failure-program-1.2.7 (c (n "solana-failure-program") (v "1.2.7") (d (list (d (n "solana-runtime") (r "^1.2.7") (d #t) (k 2)) (d (n "solana-sdk") (r "^1.2.7") (d #t) (k 0)))) (h "1gv9kr90sg3mds1v3254kmffbsm4lavygfl58z2jskzll5isg7k6")))

(define-public crate-solana-failure-program-1.2.8 (c (n "solana-failure-program") (v "1.2.8") (d (list (d (n "solana-runtime") (r "^1.2.8") (d #t) (k 2)) (d (n "solana-sdk") (r "^1.2.8") (d #t) (k 0)))) (h "1sar178kj656if3c9fagdg6j7i61br8rxlx9i6abmgg6w945zpvl")))

(define-public crate-solana-failure-program-1.2.9 (c (n "solana-failure-program") (v "1.2.9") (d (list (d (n "solana-runtime") (r "^1.2.9") (d #t) (k 2)) (d (n "solana-sdk") (r "^1.2.9") (d #t) (k 0)))) (h "1g451zm6bm6ha19xdxvyzr1q2mdrnrc6xxbmzsqwxz6g1gp543r3")))

(define-public crate-solana-failure-program-1.2.10 (c (n "solana-failure-program") (v "1.2.10") (d (list (d (n "solana-runtime") (r "^1.2.10") (d #t) (k 2)) (d (n "solana-sdk") (r "^1.2.10") (d #t) (k 0)))) (h "0glva78w3yqnydv12024v5s06a29kik0s04z444m35g5qwr31j98")))

(define-public crate-solana-failure-program-1.1.20 (c (n "solana-failure-program") (v "1.1.20") (d (list (d (n "solana-runtime") (r "^1.1.20") (d #t) (k 2)) (d (n "solana-sdk") (r "^1.1.20") (d #t) (k 0)))) (h "00cphzqsmvabgdf0y58fawrqx9mh1490grgqsyy9jlrsn1b906mv")))

(define-public crate-solana-failure-program-1.2.12 (c (n "solana-failure-program") (v "1.2.12") (d (list (d (n "solana-runtime") (r "^1.2.12") (d #t) (k 2)) (d (n "solana-sdk") (r "^1.2.12") (d #t) (k 0)))) (h "1xrjygl8jbydw8ixvy4gm10ad6986mspk8d3h386v0kbf3zb0ky1")))

(define-public crate-solana-failure-program-1.2.13 (c (n "solana-failure-program") (v "1.2.13") (d (list (d (n "solana-runtime") (r "^1.2.13") (d #t) (k 2)) (d (n "solana-sdk") (r "^1.2.13") (d #t) (k 0)))) (h "0bn30iwqjn1804d4bvqw2wfawypzmvndh2vig5kk3ybskbyvpjm9")))

(define-public crate-solana-failure-program-1.2.14 (c (n "solana-failure-program") (v "1.2.14") (d (list (d (n "solana-runtime") (r "^1.2.14") (d #t) (k 2)) (d (n "solana-sdk") (r "^1.2.14") (d #t) (k 0)))) (h "1afs6gjpp5d15dy9zlfn65v5rbjhcky52qz6p3c4zy5h052h54yp")))

(define-public crate-solana-failure-program-1.1.23 (c (n "solana-failure-program") (v "1.1.23") (d (list (d (n "solana-runtime") (r "^1.1.23") (d #t) (k 2)) (d (n "solana-sdk") (r "^1.1.23") (d #t) (k 0)))) (h "1yj95fyv90q2mhygivvly1jp5d40bv2d5s3gs09a0cfwhay5rnr3")))

(define-public crate-solana-failure-program-1.2.15 (c (n "solana-failure-program") (v "1.2.15") (d (list (d (n "solana-runtime") (r "^1.2.15") (d #t) (k 2)) (d (n "solana-sdk") (r "^1.2.15") (d #t) (k 0)))) (h "10xckmp4vffmmhr8h7sqclnj0mankpiz7qn5va36ay93krh2bv7m")))

(define-public crate-solana-failure-program-1.2.16 (c (n "solana-failure-program") (v "1.2.16") (d (list (d (n "solana-runtime") (r "^1.2.16") (d #t) (k 2)) (d (n "solana-sdk") (r "^1.2.16") (d #t) (k 0)))) (h "1ixzlzkcxv4ql4pgqsgxb16lpfiz4flkypf83wpajlwiq0yhfjkl")))

(define-public crate-solana-failure-program-1.2.17 (c (n "solana-failure-program") (v "1.2.17") (d (list (d (n "solana-runtime") (r "^1.2.17") (d #t) (k 2)) (d (n "solana-sdk") (r "^1.2.17") (d #t) (k 0)))) (h "164b1qqda129cqfx5avkwzbz0hbyhwx5b8kcjknjhnl2mh1r6r1s")))

(define-public crate-solana-failure-program-1.2.18 (c (n "solana-failure-program") (v "1.2.18") (d (list (d (n "solana-runtime") (r "^1.2.18") (d #t) (k 2)) (d (n "solana-sdk") (r "^1.2.18") (d #t) (k 0)))) (h "05cw9ah0xcch6xcr6abpfy1zbz9dk8zmjqbjcy22mfv1gqqc1w2w")))

(define-public crate-solana-failure-program-1.2.19 (c (n "solana-failure-program") (v "1.2.19") (d (list (d (n "solana-runtime") (r "^1.2.19") (d #t) (k 2)) (d (n "solana-sdk") (r "^1.2.19") (d #t) (k 0)))) (h "1alm4rd2pr6ybh69zwk4062wvg1g5a1cpv2ckrq88k131kpn9aqd")))

(define-public crate-solana-failure-program-1.3.0 (c (n "solana-failure-program") (v "1.3.0") (d (list (d (n "solana-runtime") (r "^1.3.0") (d #t) (k 2)) (d (n "solana-sdk") (r "^1.3.0") (d #t) (k 0)))) (h "07vvwakcjrmsk0xzfkckfgcws620mnlrjrffifabxzjz50knys3w")))

(define-public crate-solana-failure-program-1.3.1 (c (n "solana-failure-program") (v "1.3.1") (d (list (d (n "solana-runtime") (r "^1.3.1") (d #t) (k 2)) (d (n "solana-sdk") (r "^1.3.1") (d #t) (k 0)))) (h "1kvfb9hnrfphrmz1w4hnbalncydsq572l4wcaj50w0djg6nndn3m")))

(define-public crate-solana-failure-program-1.2.21 (c (n "solana-failure-program") (v "1.2.21") (d (list (d (n "solana-runtime") (r "^1.2.21") (d #t) (k 2)) (d (n "solana-sdk") (r "^1.2.21") (d #t) (k 0)))) (h "1y2r6yncqzf4m7z5xyx03vpsr2555fraypksq4rxxcxb0rk32fb0")))

(define-public crate-solana-failure-program-1.3.2 (c (n "solana-failure-program") (v "1.3.2") (d (list (d (n "solana-runtime") (r "^1.3.2") (d #t) (k 2)) (d (n "solana-sdk") (r "^1.3.2") (d #t) (k 0)))) (h "0zzs4d53pnq0ffc523rv2a2ckg7mbax02rrq344y0y3lr8bi7ynp")))

(define-public crate-solana-failure-program-1.2.22 (c (n "solana-failure-program") (v "1.2.22") (d (list (d (n "solana-runtime") (r "^1.2.22") (d #t) (k 2)) (d (n "solana-sdk") (r "^1.2.22") (d #t) (k 0)))) (h "0mxzz53grni5sdmnvidykn4bw4yppc8fib636vv4g28iw7np5cv9")))

(define-public crate-solana-failure-program-1.3.3 (c (n "solana-failure-program") (v "1.3.3") (d (list (d (n "solana-runtime") (r "^1.3.3") (d #t) (k 2)) (d (n "solana-sdk") (r "^1.3.3") (d #t) (k 0)))) (h "1fmj9fhd27kwgnc1y88zsdikb9j664kdjrz1107ka7i0pph0zfrj")))

(define-public crate-solana-failure-program-1.2.23 (c (n "solana-failure-program") (v "1.2.23") (d (list (d (n "solana-runtime") (r "^1.2.23") (d #t) (k 2)) (d (n "solana-sdk") (r "^1.2.23") (d #t) (k 0)))) (h "08an4n8ci5waxwzavb4i39s2xw4wjmkmr26g04392icvcrg1b2jr")))

(define-public crate-solana-failure-program-1.2.24 (c (n "solana-failure-program") (v "1.2.24") (d (list (d (n "solana-runtime") (r "^1.2.24") (d #t) (k 2)) (d (n "solana-sdk") (r "^1.2.24") (d #t) (k 0)))) (h "00lkw6fh601d2bvzcsasj64j9biw17rlpynn92f5rcijb281ammj")))

(define-public crate-solana-failure-program-1.3.4 (c (n "solana-failure-program") (v "1.3.4") (d (list (d (n "solana-runtime") (r "^1.3.4") (d #t) (k 2)) (d (n "solana-sdk") (r "^1.3.4") (d #t) (k 0)))) (h "11nd2jggjhrg3g7as8b0zih2035b1kcdki3vs0418ajyqbzyw6lv")))

(define-public crate-solana-failure-program-1.2.25 (c (n "solana-failure-program") (v "1.2.25") (d (list (d (n "solana-runtime") (r "^1.2.25") (d #t) (k 2)) (d (n "solana-sdk") (r "^1.2.25") (d #t) (k 0)))) (h "1wk3vszyi5nnlyism93br5pszsk8bfkds1q5bv46d98bpnq0drm9")))

(define-public crate-solana-failure-program-1.2.26 (c (n "solana-failure-program") (v "1.2.26") (d (list (d (n "solana-runtime") (r "^1.2.26") (d #t) (k 2)) (d (n "solana-sdk") (r "^1.2.26") (d #t) (k 0)))) (h "0h9pma0b6jc6yh3lrjnmky9h5kzbfnpfpflh2p87zjpq8fgd46hd")))

(define-public crate-solana-failure-program-1.3.5 (c (n "solana-failure-program") (v "1.3.5") (d (list (d (n "solana-runtime") (r "^1.3.5") (d #t) (k 2)) (d (n "solana-sdk") (r "^1.3.5") (d #t) (k 0)))) (h "1q4s4d22qs1m3sm7k3fh2hvavrn0pmqvhp5hbwjr1bgs75izxfaf")))

(define-public crate-solana-failure-program-1.3.6 (c (n "solana-failure-program") (v "1.3.6") (d (list (d (n "solana-runtime") (r "^1.3.6") (d #t) (k 2)) (d (n "solana-sdk") (r "^1.3.6") (d #t) (k 0)))) (h "1zv3vkzbz6bd8w6agv2ly9ags39k1bqfjlv1wkfir77l6nnpmq5l")))

(define-public crate-solana-failure-program-1.3.7 (c (n "solana-failure-program") (v "1.3.7") (d (list (d (n "solana-runtime") (r "^1.3.7") (d #t) (k 2)) (d (n "solana-sdk") (r "^1.3.7") (d #t) (k 0)))) (h "121i60ahj872z62bvchv60az34c1dz6ycpkcqkhnv44pz3pms418")))

(define-public crate-solana-failure-program-1.2.27 (c (n "solana-failure-program") (v "1.2.27") (d (list (d (n "solana-runtime") (r "^1.2.27") (d #t) (k 2)) (d (n "solana-sdk") (r "^1.2.27") (d #t) (k 0)))) (h "1b88fikw4msy34gdm6i7vx4kjfsd4zjrvzhlm6b47lri1s02nniq")))

(define-public crate-solana-failure-program-1.3.8 (c (n "solana-failure-program") (v "1.3.8") (d (list (d (n "solana-runtime") (r "^1.3.8") (d #t) (k 2)) (d (n "solana-sdk") (r "^1.3.8") (d #t) (k 0)))) (h "0ppwgg2lfgazksc149g44gw1l9ggkhb6bciwhf5kq8ysrnxckdkm")))

(define-public crate-solana-failure-program-1.2.28 (c (n "solana-failure-program") (v "1.2.28") (d (list (d (n "solana-runtime") (r "^1.2.28") (d #t) (k 2)) (d (n "solana-sdk") (r "^1.2.28") (d #t) (k 0)))) (h "1f069adnhpdakjk2d7i1b76alyaa64m289q9wdil6d4s2akda7sc")))

(define-public crate-solana-failure-program-1.3.9 (c (n "solana-failure-program") (v "1.3.9") (d (list (d (n "solana-runtime") (r "^1.3.9") (d #t) (k 2)) (d (n "solana-sdk") (r "^1.3.9") (d #t) (k 0)))) (h "1vx09f9hmz11fwpl1kav00rg67qjlvd1wkwkyk3g65xmjn0p69bx")))

(define-public crate-solana-failure-program-1.3.10 (c (n "solana-failure-program") (v "1.3.10") (d (list (d (n "solana-runtime") (r "^1.3.10") (d #t) (k 2)) (d (n "solana-sdk") (r "^1.3.10") (d #t) (k 0)))) (h "1a5qdjmf6vgscsgiprrh4vb8xyyznzg8ndrmcx8spjpyk4yyrnc9")))

(define-public crate-solana-failure-program-1.3.11 (c (n "solana-failure-program") (v "1.3.11") (d (list (d (n "solana-runtime") (r "^1.3.11") (d #t) (k 2)) (d (n "solana-sdk") (r "^1.3.11") (d #t) (k 0)))) (h "0ghvbbi53129x7fsapn685a2i5xrgvzjyllrba03qsfk0r0qnbxc")))

(define-public crate-solana-failure-program-1.2.29 (c (n "solana-failure-program") (v "1.2.29") (d (list (d (n "solana-runtime") (r "^1.2.29") (d #t) (k 2)) (d (n "solana-sdk") (r "^1.2.29") (d #t) (k 0)))) (h "12zajgazjijjv74c5b7sccgn0idb0h2mz5c62nji94haa1d3x45w")))

(define-public crate-solana-failure-program-1.3.12 (c (n "solana-failure-program") (v "1.3.12") (d (list (d (n "solana-runtime") (r "^1.3.12") (d #t) (k 2)) (d (n "solana-sdk") (r "^1.3.12") (d #t) (k 0)))) (h "12p7xkh507vwcycdg3xa0fd1ag16bgy4hl9zmiklskg5fq1q2qjc")))

(define-public crate-solana-failure-program-1.3.13 (c (n "solana-failure-program") (v "1.3.13") (d (list (d (n "solana-runtime") (r "^1.3.13") (d #t) (k 2)) (d (n "solana-sdk") (r "^1.3.13") (d #t) (k 0)))) (h "1xbk541hcs6ayaahhdfcr357vwbv30ivs4ah297a8sv7ajbp364b")))

(define-public crate-solana-failure-program-1.2.30 (c (n "solana-failure-program") (v "1.2.30") (d (list (d (n "solana-runtime") (r "^1.2.30") (d #t) (k 2)) (d (n "solana-sdk") (r "^1.2.30") (d #t) (k 0)))) (h "0a7gi3hln4yknqwg79k918151gz3v319ablildy9k0wfnn992h10")))

(define-public crate-solana-failure-program-1.3.14 (c (n "solana-failure-program") (v "1.3.14") (d (list (d (n "solana-runtime") (r "^1.3.14") (d #t) (k 2)) (d (n "solana-sdk") (r "^1.3.14") (d #t) (k 0)))) (h "039pc7wah7z63qlc8yylr29gpji4w5dbg4aijxb1r5f75nmgv314")))

(define-public crate-solana-failure-program-1.2.31 (c (n "solana-failure-program") (v "1.2.31") (d (list (d (n "solana-runtime") (r "^1.2.31") (d #t) (k 2)) (d (n "solana-sdk") (r "^1.2.31") (d #t) (k 0)))) (h "1yda1fc4xin72cf73ra7s7inn4fw4asvw1krffy4wxdgb293r4il")))

(define-public crate-solana-failure-program-1.2.32 (c (n "solana-failure-program") (v "1.2.32") (d (list (d (n "solana-runtime") (r "^1.2.32") (d #t) (k 2)) (d (n "solana-sdk") (r "^1.2.32") (d #t) (k 0)))) (h "0wc3n448rdql6wwggsrm48iqmqzr4rrcp6zy9d1hqi415vh2jwaz")))

(define-public crate-solana-failure-program-1.3.17 (c (n "solana-failure-program") (v "1.3.17") (d (list (d (n "solana-runtime") (r "^1.3.17") (d #t) (k 2)) (d (n "solana-sdk") (r "^1.3.17") (d #t) (k 0)))) (h "0qsaw8xbpl8j1lzq952r4mgsz6n1sdls8a29fajwxb8vr5dafq2l")))

(define-public crate-solana-failure-program-1.4.0 (c (n "solana-failure-program") (v "1.4.0") (d (list (d (n "solana-runtime") (r "^1.4.0") (d #t) (k 2)) (d (n "solana-sdk") (r "^1.4.0") (d #t) (k 0)))) (h "1ck8q57fs3fflq7qwy25gbql8qhai8wsal62rh0da52l6lq8k8h2")))

(define-public crate-solana-failure-program-1.4.1 (c (n "solana-failure-program") (v "1.4.1") (d (list (d (n "solana-runtime") (r "^1.4.1") (d #t) (k 2)) (d (n "solana-sdk") (r "^1.4.1") (d #t) (k 0)))) (h "1knmhv87qc7qalpm21i375ynjbcbrxbfpnq9wp2chcnirgf1nrb0")))

(define-public crate-solana-failure-program-1.3.18 (c (n "solana-failure-program") (v "1.3.18") (d (list (d (n "solana-runtime") (r "^1.3.18") (d #t) (k 2)) (d (n "solana-sdk") (r "^1.3.18") (d #t) (k 0)))) (h "1w9dz3m5pqd73kyfpw302qi7pw8vbsmg7a2dnj4s0ic0sadn9bfy")))

(define-public crate-solana-failure-program-1.3.19 (c (n "solana-failure-program") (v "1.3.19") (d (list (d (n "solana-runtime") (r "^1.3.19") (d #t) (k 2)) (d (n "solana-sdk") (r "^1.3.19") (d #t) (k 0)))) (h "1pkimg1yxrg09m5797wx675lpc79xmswzvrl833xpgy6isj5xbaf")))

(define-public crate-solana-failure-program-1.4.3 (c (n "solana-failure-program") (v "1.4.3") (d (list (d (n "solana-runtime") (r "^1.4.3") (d #t) (k 2)) (d (n "solana-sdk") (r "^1.4.3") (d #t) (k 0)))) (h "1sjm4jbf97apxsm63jhir4jsgncvma8y396f6cbwhm9wgfi3wdvn")))

(define-public crate-solana-failure-program-1.4.4 (c (n "solana-failure-program") (v "1.4.4") (d (list (d (n "solana-runtime") (r "^1.4.4") (d #t) (k 2)) (d (n "solana-sdk") (r "^1.4.4") (d #t) (k 0)))) (h "1xnb0igdjkb15gmknnzhggycp4r205y16c6f2mnklvngyknfc8g4")))

(define-public crate-solana-failure-program-1.4.5 (c (n "solana-failure-program") (v "1.4.5") (d (list (d (n "solana-runtime") (r "^1.4.5") (d #t) (k 2)) (d (n "solana-sdk") (r "^1.4.5") (d #t) (k 0)))) (h "19z9p9s6psv21wz70air9dsw64x5vwgxvv5s0a07l6k43zsfnq9g")))

(define-public crate-solana-failure-program-1.4.6 (c (n "solana-failure-program") (v "1.4.6") (d (list (d (n "solana-runtime") (r "^1.4.6") (d #t) (k 2)) (d (n "solana-sdk") (r "^1.4.6") (d #t) (k 0)))) (h "17bypn2cqh92m1dv1b3pzmi057qyhqanjy531fpl68gfws4qih5v")))

(define-public crate-solana-failure-program-1.4.7 (c (n "solana-failure-program") (v "1.4.7") (d (list (d (n "solana-runtime") (r "^1.4.7") (d #t) (k 2)) (d (n "solana-sdk") (r "^1.4.7") (d #t) (k 0)))) (h "1bna5aksjvp1lm6v5qnn1d4wjr0009n37q3xrvzihn68g0i21l89")))

(define-public crate-solana-failure-program-1.4.8 (c (n "solana-failure-program") (v "1.4.8") (d (list (d (n "solana-runtime") (r "^1.4.8") (d #t) (k 2)) (d (n "solana-sdk") (r "^1.4.8") (d #t) (k 0)))) (h "0rr02rckb3m4nbx301ql2v0afl2dby6vlf0km7b7r4zd610cqlxi")))

(define-public crate-solana-failure-program-1.4.9 (c (n "solana-failure-program") (v "1.4.9") (d (list (d (n "solana-runtime") (r "^1.4.9") (d #t) (k 2)) (d (n "solana-sdk") (r "^1.4.9") (d #t) (k 0)))) (h "1ccvhw3zl23zpaf2fcj3dqsp4k3jyhv39bdpfl44345b97j3slrg")))

(define-public crate-solana-failure-program-1.4.10 (c (n "solana-failure-program") (v "1.4.10") (d (list (d (n "solana-runtime") (r ">=1.4.10, <2.0.0") (d #t) (k 2)) (d (n "solana-sdk") (r ">=1.4.10, <2.0.0") (d #t) (k 0)))) (h "0x9zlpqk9rmcfdvg58h8r1j1lzprr8b3x42savxmwy373mm04jp0")))

(define-public crate-solana-failure-program-1.4.11 (c (n "solana-failure-program") (v "1.4.11") (d (list (d (n "solana-runtime") (r ">=1.4.11, <2.0.0") (d #t) (k 2)) (d (n "solana-sdk") (r ">=1.4.11, <2.0.0") (d #t) (k 0)))) (h "1v37v0dfc9z9wk4911y5b5a5nh2gcrd8g0bdaidr7mscp8dy92ij")))

(define-public crate-solana-failure-program-1.4.12 (c (n "solana-failure-program") (v "1.4.12") (d (list (d (n "solana-runtime") (r ">=1.4.12, <2.0.0") (d #t) (k 2)) (d (n "solana-sdk") (r ">=1.4.12, <2.0.0") (d #t) (k 0)))) (h "0qmxgf46i3agc80ad9q5ii1rq1pvma1d1l6zpswfjyqx5dqvh4z4")))

(define-public crate-solana-failure-program-1.4.13 (c (n "solana-failure-program") (v "1.4.13") (d (list (d (n "solana-runtime") (r "^1.4.13") (d #t) (k 2)) (d (n "solana-sdk") (r "^1.4.13") (d #t) (k 0)))) (h "1mnyhvkqb4zgww98parx3s24i020hzxzbrz81pw2qvhb77sa13bs")))

(define-public crate-solana-failure-program-1.4.14 (c (n "solana-failure-program") (v "1.4.14") (d (list (d (n "solana-runtime") (r "^1.4.14") (d #t) (k 2)) (d (n "solana-sdk") (r "^1.4.14") (d #t) (k 0)))) (h "1d59yipfm958igkpiqg31vmgpaabjlpk2slpdxghsaggfch69kq7")))

(define-public crate-solana-failure-program-1.4.15 (c (n "solana-failure-program") (v "1.4.15") (d (list (d (n "solana-runtime") (r "^1.4.15") (d #t) (k 2)) (d (n "solana-sdk") (r "^1.4.15") (d #t) (k 0)))) (h "0xdrj45spysybb9wc8gnyy11a04rrrrz50x6c2wqajwk3k134cmy")))

(define-public crate-solana-failure-program-1.4.16 (c (n "solana-failure-program") (v "1.4.16") (d (list (d (n "solana-runtime") (r "^1.4.16") (d #t) (k 2)) (d (n "solana-sdk") (r "^1.4.16") (d #t) (k 0)))) (h "1k8brijvvsyc1xdxhvmqm897ja1ddx3khzwv1scbgijrixbqvg51")))

(define-public crate-solana-failure-program-1.4.17 (c (n "solana-failure-program") (v "1.4.17") (d (list (d (n "solana-runtime") (r "^1.4.17") (d #t) (k 2)) (d (n "solana-sdk") (r "^1.4.17") (d #t) (k 0)))) (h "0kgi1m8yjy6cvx2krdhw34fib041fcr6bp8zik4j0hw3wixicw46")))

(define-public crate-solana-failure-program-1.5.0 (c (n "solana-failure-program") (v "1.5.0") (d (list (d (n "solana-runtime") (r "^1.5.0") (d #t) (k 2)) (d (n "solana-sdk") (r "^1.5.0") (d #t) (k 0)))) (h "1dnxrxny3m0vmj5lgha5rydy0hykc7xxv1irmc3q7fi21w3bz23i")))

(define-public crate-solana-failure-program-1.4.18 (c (n "solana-failure-program") (v "1.4.18") (d (list (d (n "solana-runtime") (r "^1.4.18") (d #t) (k 2)) (d (n "solana-sdk") (r "^1.4.18") (d #t) (k 0)))) (h "1hf6kxvgvv75cf7bg4v525qgqfpw1d4l15n56wv3l3dw76v02dh1")))

(define-public crate-solana-failure-program-1.4.19 (c (n "solana-failure-program") (v "1.4.19") (d (list (d (n "solana-runtime") (r "^1.4.19") (d #t) (k 2)) (d (n "solana-sdk") (r "^1.4.19") (d #t) (k 0)))) (h "16qkrkw6px22c7dq05xlc83ljk48j3236wrk42fa2v2mmvcl38cn")))

(define-public crate-solana-failure-program-1.4.20 (c (n "solana-failure-program") (v "1.4.20") (d (list (d (n "solana-runtime") (r "^1.4.20") (d #t) (k 2)) (d (n "solana-sdk") (r "^1.4.20") (d #t) (k 0)))) (h "0n7ik7133649x032ansql2r2nplm9bi28rvcfq0hfxpslx2k3ysl")))

(define-public crate-solana-failure-program-1.5.1 (c (n "solana-failure-program") (v "1.5.1") (d (list (d (n "solana-runtime") (r "^1.5.1") (d #t) (k 2)) (d (n "solana-sdk") (r "^1.5.1") (d #t) (k 0)))) (h "02fsz9kigigvlp1hzn1idsddwymdqg68b0sdb55gbbs3wqy2xfbj")))

(define-public crate-solana-failure-program-1.4.21 (c (n "solana-failure-program") (v "1.4.21") (d (list (d (n "solana-runtime") (r "^1.4.21") (d #t) (k 2)) (d (n "solana-sdk") (r "^1.4.21") (d #t) (k 0)))) (h "1kv11c87j46g62nn25bzrzi3cvifaqqmqz5nfk7y5lvzhbn00k9i")))

(define-public crate-solana-failure-program-1.4.22 (c (n "solana-failure-program") (v "1.4.22") (d (list (d (n "solana-runtime") (r "^1.4.22") (d #t) (k 2)) (d (n "solana-sdk") (r "^1.4.22") (d #t) (k 0)))) (h "0z50kinb64iq888k3hx2krkj4mwxf8whks3a6liwv0f136n6idlr")))

(define-public crate-solana-failure-program-1.5.2 (c (n "solana-failure-program") (v "1.5.2") (d (list (d (n "solana-runtime") (r "^1.5.2") (d #t) (k 2)) (d (n "solana-sdk") (r "^1.5.2") (d #t) (k 0)))) (h "1mfz16qxvci7jlgbg9bpw5873abj0r83j37rnh5jaw9fvjmg5g2c")))

(define-public crate-solana-failure-program-1.4.23 (c (n "solana-failure-program") (v "1.4.23") (d (list (d (n "solana-runtime") (r "^1.4.23") (d #t) (k 2)) (d (n "solana-sdk") (r "^1.4.23") (d #t) (k 0)))) (h "1677kd6cnga7iri2m0y5drwrsrcj9mdz8c4vd3hrr4fmfar0qsxk")))

(define-public crate-solana-failure-program-1.5.3 (c (n "solana-failure-program") (v "1.5.3") (d (list (d (n "solana-runtime") (r "^1.5.3") (d #t) (k 2)) (d (n "solana-sdk") (r "^1.5.3") (d #t) (k 0)))) (h "15bzn6rp2vbp2xp2dgdwjzkx29bgp2gvb9x2rif9yhfvmxm4ccwv")))

(define-public crate-solana-failure-program-1.5.4 (c (n "solana-failure-program") (v "1.5.4") (d (list (d (n "solana-runtime") (r "^1.5.4") (d #t) (k 2)) (d (n "solana-sdk") (r "^1.5.4") (d #t) (k 0)))) (h "1akjdnjhljq80il2gh0z4sh14izas9z66rrnzgkwxnmpd2r9vl7w")))

(define-public crate-solana-failure-program-1.5.5 (c (n "solana-failure-program") (v "1.5.5") (d (list (d (n "solana-runtime") (r "^1.5.5") (d #t) (k 2)) (d (n "solana-sdk") (r "^1.5.5") (d #t) (k 0)))) (h "1qkp56a4z68vjqgp170g6s6v1jk5r1gxdqq5pdharddqal3hcqrl")))

(define-public crate-solana-failure-program-1.4.25 (c (n "solana-failure-program") (v "1.4.25") (d (list (d (n "solana-runtime") (r "^1.4.25") (d #t) (k 2)) (d (n "solana-sdk") (r "^1.4.25") (d #t) (k 0)))) (h "0pwv6n43q6gdw38if6lansyv064jy7k7qg290c186x9fr44wwcgr")))

(define-public crate-solana-failure-program-1.4.26 (c (n "solana-failure-program") (v "1.4.26") (d (list (d (n "solana-runtime") (r "^1.4.26") (d #t) (k 2)) (d (n "solana-sdk") (r "^1.4.26") (d #t) (k 0)))) (h "1763x2i1slsr5q4i4ygknkb384i67ypn0wpibm3f5q2ckljy6901")))

(define-public crate-solana-failure-program-1.5.6 (c (n "solana-failure-program") (v "1.5.6") (d (list (d (n "solana-runtime") (r "^1.5.6") (d #t) (k 2)) (d (n "solana-sdk") (r "^1.5.6") (d #t) (k 0)))) (h "1dv3y8294j7pmj4c6za1kp74k90kqd87vay5af32lwhn7csn667q")))

(define-public crate-solana-failure-program-1.4.27 (c (n "solana-failure-program") (v "1.4.27") (d (list (d (n "solana-runtime") (r "^1.4.27") (d #t) (k 2)) (d (n "solana-sdk") (r "^1.4.27") (d #t) (k 0)))) (h "0pcm3mchwsa8i4s5dh0qzklg0nyqhigndas73cfmkg0wjij05yfs")))

(define-public crate-solana-failure-program-1.5.7 (c (n "solana-failure-program") (v "1.5.7") (d (list (d (n "solana-runtime") (r "^1.5.7") (d #t) (k 2)) (d (n "solana-sdk") (r "^1.5.7") (d #t) (k 0)))) (h "02c3g3w6x25lc940c75s8kar0087nyiw27x0qy7qn33wf8k8r86m")))

(define-public crate-solana-failure-program-1.5.8 (c (n "solana-failure-program") (v "1.5.8") (d (list (d (n "solana-runtime") (r "^1.5.8") (d #t) (k 2)) (d (n "solana-sdk") (r "^1.5.8") (d #t) (k 0)))) (h "0lp3c2pyx0k4v4p7gl84s2rsh4qmk91smsnaxqvi4mdd4ik6x4mv")))

(define-public crate-solana-failure-program-1.4.28 (c (n "solana-failure-program") (v "1.4.28") (d (list (d (n "solana-runtime") (r "^1.4.28") (d #t) (k 2)) (d (n "solana-sdk") (r "^1.4.28") (d #t) (k 0)))) (h "1nzbbgsjmis0ln9lfhggq9iafwhvdb4lisssshdc9j6qv2lhynkq")))

(define-public crate-solana-failure-program-1.5.9 (c (n "solana-failure-program") (v "1.5.9") (d (list (d (n "solana-runtime") (r "^1.5.9") (d #t) (k 2)) (d (n "solana-sdk") (r "^1.5.9") (d #t) (k 0)))) (h "000pnw4wfg8rfhfwf38mris6ic5aywxyxfjasma6hkq83mms2shb")))

(define-public crate-solana-failure-program-1.5.10 (c (n "solana-failure-program") (v "1.5.10") (d (list (d (n "solana-runtime") (r "^1.5.10") (d #t) (k 2)) (d (n "solana-sdk") (r "^1.5.10") (d #t) (k 0)))) (h "162x0nhxzrm2qp50cy5xci99qvk18crqwfd42y020wd63y8gdyjh")))

(define-public crate-solana-failure-program-1.5.11 (c (n "solana-failure-program") (v "1.5.11") (d (list (d (n "solana-runtime") (r "^1.5.11") (d #t) (k 2)) (d (n "solana-sdk") (r "^1.5.11") (d #t) (k 0)))) (h "1s2ynxlfk0r7gdd7zd24gvmyygkngzsgxx63lp9szhlkcplnkhan")))

(define-public crate-solana-failure-program-1.5.12 (c (n "solana-failure-program") (v "1.5.12") (d (list (d (n "solana-runtime") (r "^1.5.12") (d #t) (k 2)) (d (n "solana-sdk") (r "^1.5.12") (d #t) (k 0)))) (h "1myh9mgdnjszmpyqimchkgck7fs0pncj6kh2jnrjmhaw2j1jdicn")))

(define-public crate-solana-failure-program-1.5.13 (c (n "solana-failure-program") (v "1.5.13") (d (list (d (n "solana-runtime") (r "^1.5.13") (d #t) (k 2)) (d (n "solana-sdk") (r "^1.5.13") (d #t) (k 0)))) (h "1bac55qf6dmpqx94p2wckxmgbdrf1a7q838xa2arkkar54sll3iz")))

(define-public crate-solana-failure-program-1.5.14 (c (n "solana-failure-program") (v "1.5.14") (d (list (d (n "solana-runtime") (r "^1.5.14") (d #t) (k 2)) (d (n "solana-sdk") (r "^1.5.14") (d #t) (k 0)))) (h "1yi6m3nn0v4ivrhh162cr550wmkvlnm0f2lnamr8l0a14k5dc1iz")))

(define-public crate-solana-failure-program-1.6.0 (c (n "solana-failure-program") (v "1.6.0") (d (list (d (n "solana-runtime") (r "^1.6.0") (d #t) (k 2)) (d (n "solana-sdk") (r "^1.6.0") (d #t) (k 0)))) (h "00bggrhzz85wz1zd7apcl5dxl002znrjayj0y3lgzd5qgm16qjra")))

(define-public crate-solana-failure-program-1.5.15 (c (n "solana-failure-program") (v "1.5.15") (d (list (d (n "solana-runtime") (r "=1.5.15") (d #t) (k 2)) (d (n "solana-sdk") (r "=1.5.15") (d #t) (k 0)))) (h "0rp4f9p2lwrnispwjzrdc67icsyij853h82fq0gmjvsq7yiryyni")))

(define-public crate-solana-failure-program-1.6.1 (c (n "solana-failure-program") (v "1.6.1") (d (list (d (n "solana-runtime") (r "=1.6.1") (d #t) (k 2)) (d (n "solana-sdk") (r "=1.6.1") (d #t) (k 0)))) (h "1rr7yb6pf2lfykypiplidvq4dkk406hsjb15isw3xq568ywp4fca")))

(define-public crate-solana-failure-program-1.5.16 (c (n "solana-failure-program") (v "1.5.16") (d (list (d (n "solana-runtime") (r "=1.5.16") (d #t) (k 2)) (d (n "solana-sdk") (r "=1.5.16") (d #t) (k 0)))) (h "06wzkjp3fxjz2869x4dfz43l918rlv18n81kr7bdxypggmcpbcr4")))

(define-public crate-solana-failure-program-1.5.17 (c (n "solana-failure-program") (v "1.5.17") (d (list (d (n "solana-runtime") (r "=1.5.17") (d #t) (k 2)) (d (n "solana-sdk") (r "=1.5.17") (d #t) (k 0)))) (h "07rj83cqb43qb3lsi271bjg4idi8mgffc7l6nnai6bdafapfbh0c")))

(define-public crate-solana-failure-program-1.6.2 (c (n "solana-failure-program") (v "1.6.2") (d (list (d (n "solana-runtime") (r "=1.6.2") (d #t) (k 2)) (d (n "solana-sdk") (r "=1.6.2") (d #t) (k 0)))) (h "12a5rx45gpr8hmrf0r6gy2abpnzgi6kl28x14msk8gqiyd3ym4m3")))

(define-public crate-solana-failure-program-1.6.3 (c (n "solana-failure-program") (v "1.6.3") (d (list (d (n "solana-runtime") (r "=1.6.3") (d #t) (k 2)) (d (n "solana-sdk") (r "=1.6.3") (d #t) (k 0)))) (h "0fvmf29ihwngynp6s1ldwxyjgwikd78il99cnbbcpr20bq9gnkp1")))

(define-public crate-solana-failure-program-1.6.4 (c (n "solana-failure-program") (v "1.6.4") (d (list (d (n "solana-runtime") (r "=1.6.4") (d #t) (k 2)) (d (n "solana-sdk") (r "=1.6.4") (d #t) (k 0)))) (h "0dh3s50fyxv00qk41dns411lynj5r2iwwy0hmbsk5yzvphrl7yxp")))

(define-public crate-solana-failure-program-1.6.5 (c (n "solana-failure-program") (v "1.6.5") (d (list (d (n "solana-runtime") (r "=1.6.5") (d #t) (k 2)) (d (n "solana-sdk") (r "=1.6.5") (d #t) (k 0)))) (h "1cb0gh8bgi2pyd0f2qkfw00hrwwr3x7bckwrkvajgcf4qfi856nh")))

(define-public crate-solana-failure-program-1.6.6 (c (n "solana-failure-program") (v "1.6.6") (d (list (d (n "solana-runtime") (r "=1.6.6") (d #t) (k 2)) (d (n "solana-sdk") (r "=1.6.6") (d #t) (k 0)))) (h "124afr5sda2sf5864pphr24wj4pxqngxj0ba0hng3s425hr14imb")))

(define-public crate-solana-failure-program-1.5.19 (c (n "solana-failure-program") (v "1.5.19") (d (list (d (n "solana-runtime") (r "=1.5.19") (d #t) (k 2)) (d (n "solana-sdk") (r "=1.5.19") (d #t) (k 0)))) (h "13xaywgv591r9ydnnwxhgp7i97lm4x5vp5f2rgr5pjfzr88fjnw9")))

(define-public crate-solana-failure-program-1.6.7 (c (n "solana-failure-program") (v "1.6.7") (d (list (d (n "solana-runtime") (r "=1.6.7") (d #t) (k 2)) (d (n "solana-sdk") (r "=1.6.7") (d #t) (k 0)))) (h "1giahc8dprvvn4k675h3m1q91w1s0i9viys8n53wcwsalhx5ln63")))

(define-public crate-solana-failure-program-1.6.8 (c (n "solana-failure-program") (v "1.6.8") (d (list (d (n "solana-runtime") (r "=1.6.8") (d #t) (k 2)) (d (n "solana-sdk") (r "=1.6.8") (d #t) (k 0)))) (h "08i4mkf3dqc17x50q57vr9imllcr9dw5qmywvvii9jcnj8y4km3a")))

(define-public crate-solana-failure-program-1.6.9 (c (n "solana-failure-program") (v "1.6.9") (d (list (d (n "solana-runtime") (r "=1.6.9") (d #t) (k 2)) (d (n "solana-sdk") (r "=1.6.9") (d #t) (k 0)))) (h "1xck1qnya38mw1bmrp0g1smm44zsmcw5x4rqd3fxq8jwjimj7005")))

(define-public crate-solana-failure-program-1.6.10 (c (n "solana-failure-program") (v "1.6.10") (d (list (d (n "solana-runtime") (r "=1.6.10") (d #t) (k 2)) (d (n "solana-sdk") (r "=1.6.10") (d #t) (k 0)))) (h "0nvz65c24h3q84r3n9zj3h2kc48iivjfr4rl77va1r88qcjwxp8a")))

(define-public crate-solana-failure-program-1.6.11 (c (n "solana-failure-program") (v "1.6.11") (d (list (d (n "solana-runtime") (r "=1.6.11") (d #t) (k 2)) (d (n "solana-sdk") (r "=1.6.11") (d #t) (k 0)))) (h "1dps0yknjcqym7ql9szw8xcnz97fsydcf99m93alzjgki7297svk")))

(define-public crate-solana-failure-program-1.7.0 (c (n "solana-failure-program") (v "1.7.0") (d (list (d (n "solana-runtime") (r "=1.7.0") (d #t) (k 2)) (d (n "solana-sdk") (r "=1.7.0") (d #t) (k 0)))) (h "0ih7p9dqd5fpkqksh7kmwvm4ifglqfnam08ax1r1a6xfiv78s21c")))

(define-public crate-solana-failure-program-1.7.1 (c (n "solana-failure-program") (v "1.7.1") (d (list (d (n "solana-runtime") (r "=1.7.1") (d #t) (k 2)) (d (n "solana-sdk") (r "=1.7.1") (d #t) (k 0)))) (h "1df0dr0qvps92p9wf7pziv0kagvd89m2wgcz5s5yjprgglnxfmnq")))

(define-public crate-solana-failure-program-1.6.12 (c (n "solana-failure-program") (v "1.6.12") (d (list (d (n "solana-runtime") (r "=1.6.12") (d #t) (k 2)) (d (n "solana-sdk") (r "=1.6.12") (d #t) (k 0)))) (h "0nq3wk2b1pi7rklnhx9hxasbcl0ifrhnfm408w11hz1vq7k023lw")))

(define-public crate-solana-failure-program-1.6.13 (c (n "solana-failure-program") (v "1.6.13") (d (list (d (n "solana-runtime") (r "=1.6.13") (d #t) (k 2)) (d (n "solana-sdk") (r "=1.6.13") (d #t) (k 0)))) (h "02xhq1s3hsfzm5097jf6rhhf40va93yd1vm92d59y78gfx7yaq1w")))

(define-public crate-solana-failure-program-1.7.2 (c (n "solana-failure-program") (v "1.7.2") (d (list (d (n "solana-runtime") (r "=1.7.2") (d #t) (k 2)) (d (n "solana-sdk") (r "=1.7.2") (d #t) (k 0)))) (h "1xzvpnr7bc8h4b94fnbwa6wcx8ccw4ba85vh3aa260l26plkk9jj")))

(define-public crate-solana-failure-program-1.6.14 (c (n "solana-failure-program") (v "1.6.14") (d (list (d (n "solana-runtime") (r "=1.6.14") (d #t) (k 2)) (d (n "solana-sdk") (r "=1.6.14") (d #t) (k 0)))) (h "000qay098yn78p42f47ifn2f5jmy790p3k967wh884masrg32lja")))

(define-public crate-solana-failure-program-1.7.3 (c (n "solana-failure-program") (v "1.7.3") (d (list (d (n "solana-runtime") (r "=1.7.3") (d #t) (k 2)) (d (n "solana-sdk") (r "=1.7.3") (d #t) (k 0)))) (h "0wixjpamln9wj0zp67avijbm2bv6c8icz3ak4c7g4186zl3bwx4d")))

(define-public crate-solana-failure-program-1.6.15 (c (n "solana-failure-program") (v "1.6.15") (d (list (d (n "solana-runtime") (r "=1.6.15") (d #t) (k 2)) (d (n "solana-sdk") (r "=1.6.15") (d #t) (k 0)))) (h "02qsmb3mvmikcais4ghx0aqnvz3f0qn5pm8wh6yacs4cmg1binqb")))

(define-public crate-solana-failure-program-1.7.4 (c (n "solana-failure-program") (v "1.7.4") (d (list (d (n "solana-runtime") (r "=1.7.4") (d #t) (k 2)) (d (n "solana-sdk") (r "=1.7.4") (d #t) (k 0)))) (h "1w7w6qr7fphwfmv2h109pp7c28k3n6sarci3xfc39g361lpjkwwa")))

(define-public crate-solana-failure-program-1.6.16 (c (n "solana-failure-program") (v "1.6.16") (d (list (d (n "solana-runtime") (r "=1.6.16") (d #t) (k 2)) (d (n "solana-sdk") (r "=1.6.16") (d #t) (k 0)))) (h "00fc9lzjmvivsvcvbz0frsajhi6iwsaly7msyn6cq4lfw8cp7f74")))

(define-public crate-solana-failure-program-1.6.17 (c (n "solana-failure-program") (v "1.6.17") (d (list (d (n "solana-runtime") (r "=1.6.17") (d #t) (k 2)) (d (n "solana-sdk") (r "=1.6.17") (d #t) (k 0)))) (h "1qk6z9pql3pj8rir7pvpvcrp13a656kb5mz6cjpi3cihg0zrgd21")))

(define-public crate-solana-failure-program-1.7.5 (c (n "solana-failure-program") (v "1.7.5") (d (list (d (n "solana-runtime") (r "=1.7.5") (d #t) (k 2)) (d (n "solana-sdk") (r "=1.7.5") (d #t) (k 0)))) (h "18kl0xwmwxjz41ixcsxhrwzwr7ijgc5p20p6axm7sgw23wa7cnvs")))

(define-public crate-solana-failure-program-1.7.6 (c (n "solana-failure-program") (v "1.7.6") (d (list (d (n "solana-runtime") (r "=1.7.6") (d #t) (k 2)) (d (n "solana-sdk") (r "=1.7.6") (d #t) (k 0)))) (h "1nvrcy8xxhl7c8y7ycpmm0d2zsp012l6qd3n1mvc09r2f2kpjjjd")))

(define-public crate-solana-failure-program-1.6.18 (c (n "solana-failure-program") (v "1.6.18") (d (list (d (n "solana-runtime") (r "=1.6.18") (d #t) (k 2)) (d (n "solana-sdk") (r "=1.6.18") (d #t) (k 0)))) (h "08la0w057gymfbaf1k0dygh0s8bh8qnsmqnjp6sxz303xls10nhi")))

(define-public crate-solana-failure-program-1.6.19 (c (n "solana-failure-program") (v "1.6.19") (d (list (d (n "solana-runtime") (r "=1.6.19") (d #t) (k 2)) (d (n "solana-sdk") (r "=1.6.19") (d #t) (k 0)))) (h "03nv43wgyx04lncnfcjk2m72sp2kvayjcphdwzdwxy0v9851vl14")))

(define-public crate-solana-failure-program-1.7.7 (c (n "solana-failure-program") (v "1.7.7") (d (list (d (n "solana-runtime") (r "=1.7.7") (d #t) (k 2)) (d (n "solana-sdk") (r "=1.7.7") (d #t) (k 0)))) (h "0bnlmr9gqlz0wdvmsbgibg9y9hiyiylqf6r9cdqhjkyqc3bg7yhx")))

(define-public crate-solana-failure-program-1.7.8 (c (n "solana-failure-program") (v "1.7.8") (d (list (d (n "solana-runtime") (r "=1.7.8") (d #t) (k 2)) (d (n "solana-sdk") (r "=1.7.8") (d #t) (k 0)))) (h "1miys5c7nsg9zzz9z1vdm6vj7bma7jr2w6p99l4z68zijja099rc")))

(define-public crate-solana-failure-program-1.6.20 (c (n "solana-failure-program") (v "1.6.20") (d (list (d (n "solana-runtime") (r "=1.6.20") (d #t) (k 2)) (d (n "solana-sdk") (r "=1.6.20") (d #t) (k 0)))) (h "14jipb1sdgz0a15w8naybbbi3hjw9dimvjyj2cf5hwzh8r11m27k")))

(define-public crate-solana-failure-program-1.7.9 (c (n "solana-failure-program") (v "1.7.9") (d (list (d (n "solana-runtime") (r "=1.7.9") (d #t) (k 2)) (d (n "solana-sdk") (r "=1.7.9") (d #t) (k 0)))) (h "08gj5a44y1i9x4cjwwqhj84y5sib7rjbpxxg9lbz4x0jhn4q92nc")))

(define-public crate-solana-failure-program-1.7.10 (c (n "solana-failure-program") (v "1.7.10") (d (list (d (n "solana-runtime") (r "=1.7.10") (d #t) (k 2)) (d (n "solana-sdk") (r "=1.7.10") (d #t) (k 0)))) (h "09cpx41xxg5k8zay4lj20xrcdvq09d3dkm7h3048q0kmybfg8460")))

(define-public crate-solana-failure-program-1.6.21 (c (n "solana-failure-program") (v "1.6.21") (d (list (d (n "solana-runtime") (r "=1.6.21") (d #t) (k 2)) (d (n "solana-sdk") (r "=1.6.21") (d #t) (k 0)))) (h "0zbqsxwfgb39j60clvnwkz9bymj024x3bv5bcbr61a2r9famb5fl")))

(define-public crate-solana-failure-program-1.7.11 (c (n "solana-failure-program") (v "1.7.11") (d (list (d (n "solana-runtime") (r "=1.7.11") (d #t) (k 2)) (d (n "solana-sdk") (r "=1.7.11") (d #t) (k 0)))) (h "02ckvmlcidik87a1cg53xsq86mp6vk04jj4f6k77clyyfgqipmwz")))

(define-public crate-solana-failure-program-1.6.24 (c (n "solana-failure-program") (v "1.6.24") (d (list (d (n "solana-runtime") (r "=1.6.24") (d #t) (k 2)) (d (n "solana-sdk") (r "=1.6.24") (d #t) (k 0)))) (h "0a02ds58nsga0lqvcpljvjlb9c9rynm6p8mznqigp7lf271yq97i")))

(define-public crate-solana-failure-program-1.6.25 (c (n "solana-failure-program") (v "1.6.25") (d (list (d (n "solana-runtime") (r "=1.6.25") (d #t) (k 2)) (d (n "solana-sdk") (r "=1.6.25") (d #t) (k 0)))) (h "137isv059za8967qmqvb3cwd1w98mb80j5axcmnxcil1gbha3gcq")))

(define-public crate-solana-failure-program-1.7.12 (c (n "solana-failure-program") (v "1.7.12") (d (list (d (n "solana-runtime") (r "=1.7.12") (d #t) (k 2)) (d (n "solana-sdk") (r "=1.7.12") (d #t) (k 0)))) (h "12fbhg0mw38jmvgvwc8ccsg1fqzm3v3a6fsk6hg73dzp6fzrxgi9")))

(define-public crate-solana-failure-program-1.6.26 (c (n "solana-failure-program") (v "1.6.26") (d (list (d (n "solana-runtime") (r "=1.6.26") (d #t) (k 2)) (d (n "solana-sdk") (r "=1.6.26") (d #t) (k 0)))) (h "1avjdyrkch2dx9q6aw3g2m3mwl5za8cm5mv85lj62dqbifif34h7")))

(define-public crate-solana-failure-program-1.6.27 (c (n "solana-failure-program") (v "1.6.27") (d (list (d (n "solana-runtime") (r "=1.6.27") (d #t) (k 2)) (d (n "solana-sdk") (r "=1.6.27") (d #t) (k 0)))) (h "1yhpyqznv7vrg8lsk60r81z7i9srcc286a01xr6fk9mafms0j98l")))

(define-public crate-solana-failure-program-1.7.13 (c (n "solana-failure-program") (v "1.7.13") (d (list (d (n "solana-runtime") (r "=1.7.13") (d #t) (k 2)) (d (n "solana-sdk") (r "=1.7.13") (d #t) (k 0)))) (h "0k9yxf46wmbp2hrq7x5bqk0xhxwk2yqgcc0hk3q73nnh7v6nrizy")))

(define-public crate-solana-failure-program-1.7.14 (c (n "solana-failure-program") (v "1.7.14") (d (list (d (n "solana-runtime") (r "=1.7.14") (d #t) (k 2)) (d (n "solana-sdk") (r "=1.7.14") (d #t) (k 0)))) (h "1170y57z1r8cari496x4wizs2a24w0mc1pm6zlgiawnrkrjb00ij")))

(define-public crate-solana-failure-program-1.8.0 (c (n "solana-failure-program") (v "1.8.0") (d (list (d (n "solana-runtime") (r "=1.8.0") (d #t) (k 2)) (d (n "solana-sdk") (r "=1.8.0") (d #t) (k 0)))) (h "0dqm9q5b4ppscb1xrahhsfa5vafi6zzfndmwn519i0c6vsh8ls23")))

(define-public crate-solana-failure-program-1.6.28 (c (n "solana-failure-program") (v "1.6.28") (d (list (d (n "solana-runtime") (r "=1.6.28") (d #t) (k 2)) (d (n "solana-sdk") (r "=1.6.28") (d #t) (k 0)))) (h "08iwhmd5q06flbjc9swnh8fqia1wm8vp8rqqrk6gcfid8j0ch56h")))

(define-public crate-solana-failure-program-1.7.15 (c (n "solana-failure-program") (v "1.7.15") (d (list (d (n "solana-runtime") (r "=1.7.15") (d #t) (k 2)) (d (n "solana-sdk") (r "=1.7.15") (d #t) (k 0)))) (h "17fbnnzqbdypy377shqr8dyzrdksc8q3yfphyk21bmi9qp06ay28")))

