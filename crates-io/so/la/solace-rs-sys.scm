(define-module (crates-io so la solace-rs-sys) #:use-module (crates-io))

(define-public crate-solace-rs-sys-0.1.1 (c (n "solace-rs-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "cfg-if") (r "^1.0") (d #t) (k 1)) (d (n "flate2") (r "^1.0.26") (d #t) (k 1)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 1)) (d (n "tar") (r "^0.4.38") (d #t) (k 1)))) (h "17rndsmnkyxc4mm64yg84axlxm6d7chbri2v5gyfkxj2d1y3sv7g")))

(define-public crate-solace-rs-sys-0.1.2 (c (n "solace-rs-sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "cfg-if") (r "^1.0") (d #t) (k 1)) (d (n "flate2") (r "^1.0.26") (d #t) (k 1)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 1)) (d (n "tar") (r "^0.4.38") (d #t) (k 1)))) (h "1vdznyic8skqxjsv7vr426jb2d8h6cz3rrmjyfsm340azn5qn8if")))

(define-public crate-solace-rs-sys-0.1.3 (c (n "solace-rs-sys") (v "0.1.3") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "cfg-if") (r "^1.0") (d #t) (k 1)) (d (n "flate2") (r "^1.0.26") (d #t) (k 1)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 1)) (d (n "tar") (r "^0.4.38") (d #t) (k 1)))) (h "1sgw49bgydk92v6mi1scimxb2dg718rhz51zpqd63298d3db34ig")))

(define-public crate-solace-rs-sys-0.1.4 (c (n "solace-rs-sys") (v "0.1.4") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "cfg-if") (r "^1.0") (d #t) (k 1)) (d (n "flate2") (r "^1.0.26") (d #t) (k 1)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 1)) (d (n "tar") (r "^0.4.38") (d #t) (k 1)))) (h "07c5k4c57pk4vm9yznm2lczld4fiqwa0sksm00frirw2i8yvqzv7")))

(define-public crate-solace-rs-sys-0.1.5 (c (n "solace-rs-sys") (v "0.1.5") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "cfg-if") (r "^1.0") (d #t) (k 1)) (d (n "flate2") (r "^1.0.26") (d #t) (k 1)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 1)) (d (n "tar") (r "^0.4.38") (d #t) (k 1)))) (h "11gxjlk84w1kmy1cbq57k13g8i6pvrxsiiz0arddp4bggj0wqsn9")))

(define-public crate-solace-rs-sys-1.0.0 (c (n "solace-rs-sys") (v "1.0.0") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "cfg-if") (r "^1.0") (d #t) (k 1)) (d (n "flate2") (r "^1.0.26") (d #t) (k 1)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 1)) (d (n "tar") (r "^0.4.38") (d #t) (k 1)))) (h "0gy49hvv23sjrs8ji63b7fnnzrhf8jf1rhz1lmnsjwh3dg94x2n3") (r "1.70.0")))

