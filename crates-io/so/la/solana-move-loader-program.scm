(define-module (crates-io so la solana-move-loader-program) #:use-module (crates-io))

(define-public crate-solana-move-loader-program-0.17.0 (c (n "solana-move-loader-program") (v "0.17.0") (d (list (d (n "log") (r "^0.4.2") (d #t) (k 0)) (d (n "solana-logger") (r "^0.17.0") (d #t) (k 0)) (d (n "solana-move-loader-api") (r "^0.17.0") (d #t) (k 0)) (d (n "solana-sdk") (r "^0.17.0") (d #t) (k 0)))) (h "12bqm8lw7z28nsgnlcb6lhvlfqq5qvag8lcls68q3g84axvwwffp")))

(define-public crate-solana-move-loader-program-0.17.1 (c (n "solana-move-loader-program") (v "0.17.1") (d (list (d (n "log") (r "^0.4.2") (d #t) (k 0)) (d (n "solana-logger") (r "^0.17.1") (d #t) (k 0)) (d (n "solana-move-loader-api") (r "^0.17.1") (d #t) (k 0)) (d (n "solana-sdk") (r "^0.17.1") (d #t) (k 0)))) (h "09wqzhkrglq5v5vhb42m6hpmvgcr59af60wffmg2apslpq48cshp")))

(define-public crate-solana-move-loader-program-0.18.0-pre0 (c (n "solana-move-loader-program") (v "0.18.0-pre0") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-logger") (r "^0.18.0-pre0") (d #t) (k 0)) (d (n "solana-move-loader-api") (r "^0.18.0-pre0") (d #t) (k 0)) (d (n "solana-sdk") (r "^0.18.0-pre0") (d #t) (k 0)))) (h "1zlrhhk5lkva0zy8x3s1kkcp52ykrm06124jfgcazkp510a4rm67")))

(define-public crate-solana-move-loader-program-0.18.0-pre1 (c (n "solana-move-loader-program") (v "0.18.0-pre1") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-logger") (r "^0.18.0-pre1") (d #t) (k 0)) (d (n "solana-move-loader-api") (r "^0.18.0-pre1") (d #t) (k 0)) (d (n "solana-sdk") (r "^0.18.0-pre1") (d #t) (k 0)))) (h "16k1nq8ykq8dzfqp9lbwwx7ckgfsfs1bbs7yx8j5pxhayas4dg54")))

(define-public crate-solana-move-loader-program-0.17.2 (c (n "solana-move-loader-program") (v "0.17.2") (d (list (d (n "log") (r "^0.4.2") (d #t) (k 0)) (d (n "solana-logger") (r "^0.17.2") (d #t) (k 0)) (d (n "solana-move-loader-api") (r "^0.17.2") (d #t) (k 0)) (d (n "solana-sdk") (r "^0.17.2") (d #t) (k 0)))) (h "06vz2sxhyhfc6y4k5y2i8lm2cpd33igfhhqrvnlb1glazy1bbgil")))

(define-public crate-solana-move-loader-program-0.18.0-pre2 (c (n "solana-move-loader-program") (v "0.18.0-pre2") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-logger") (r "^0.18.0-pre2") (d #t) (k 0)) (d (n "solana-move-loader-api") (r "^0.18.0-pre2") (d #t) (k 0)) (d (n "solana-sdk") (r "^0.18.0-pre2") (d #t) (k 0)))) (h "1fainb00np6dkmc74qz327zw11rln4wq7hjf588yn1x085w7wq31")))

(define-public crate-solana-move-loader-program-0.18.0 (c (n "solana-move-loader-program") (v "0.18.0") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-logger") (r "^0.18.0") (d #t) (k 0)) (d (n "solana-move-loader-api") (r "^0.18.0") (d #t) (k 0)) (d (n "solana-sdk") (r "^0.18.0") (d #t) (k 0)))) (h "0p2hmcz85pprgbvg0rfk01cll8d5q9pmawryflfpw2ncm22k5zbv")))

(define-public crate-solana-move-loader-program-0.18.1 (c (n "solana-move-loader-program") (v "0.18.1") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-logger") (r "^0.18.1") (d #t) (k 0)) (d (n "solana-move-loader-api") (r "^0.18.1") (d #t) (k 0)) (d (n "solana-sdk") (r "^0.18.1") (d #t) (k 0)))) (h "0pd4c7rw56vsyjlbk0y045gbzqv45ndfd5zxmknnh0sbng257z64")))

(define-public crate-solana-move-loader-program-0.19.1 (c (n "solana-move-loader-program") (v "0.19.1") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-logger") (r "^0.19.1") (d #t) (k 0)) (d (n "solana-move-loader-api") (r "^0.19.1") (d #t) (k 0)) (d (n "solana-sdk") (r "^0.19.1") (d #t) (k 0)))) (h "143g6862y8zk2bi0ga2rwsisj58djj9a2d1grs7lpqksjmrc3kf0")))

(define-public crate-solana-move-loader-program-0.20.1 (c (n "solana-move-loader-program") (v "0.20.1") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "solana-logger") (r "^0.20.1") (d #t) (k 0)) (d (n "solana-move-loader-api") (r "^0.20.1") (d #t) (k 0)) (d (n "solana-sdk") (r "^0.20.1") (d #t) (k 0)))) (h "00hn6a1rhkalvni78r5j14xjh4zvw5g8nn9n8xw2bgqwxsc3683a")))

