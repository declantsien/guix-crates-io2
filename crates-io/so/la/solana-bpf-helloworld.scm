(define-module (crates-io so la solana-bpf-helloworld) #:use-module (crates-io))

(define-public crate-solana-bpf-helloworld-0.0.1 (c (n "solana-bpf-helloworld") (v "0.0.1") (d (list (d (n "borsh") (r "^0.9.1") (d #t) (k 0)) (d (n "borsh-derive") (r "^0.9.1") (d #t) (k 0)) (d (n "solana-program") (r "=1.7.9") (d #t) (k 0)) (d (n "solana-program-test") (r "=1.7.9") (d #t) (k 2)) (d (n "solana-sdk") (r "=1.7.9") (d #t) (k 2)))) (h "08krgszqnwanxapq4q09iwmngsq9r1f9r3rlgz6fwz3q4rypdz3w") (f (quote (("no-entrypoint"))))))

