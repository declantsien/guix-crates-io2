(define-module (crates-io so la solana-notifier) #:use-module (crates-io))

(define-public crate-solana-notifier-1.2.0 (c (n "solana-notifier") (v "1.2.0") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.4") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1x8w4cxsdhvi4braym1k1l4k6iv5gjr8gncv8m4hgppb9zpsrfzi")))

(define-public crate-solana-notifier-1.2.1 (c (n "solana-notifier") (v "1.2.1") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.4") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1rdxbmlv8j10vxn5jck76fp2wp6f6ais3hdjbjg7b9wgwqsyfjl0")))

(define-public crate-solana-notifier-1.2.3 (c (n "solana-notifier") (v "1.2.3") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.4") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0wznmlxz3jzh9qka5arqnqzqp7pv4ycsgp0481fwysz4v5rw4nzb")))

(define-public crate-solana-notifier-1.2.4 (c (n "solana-notifier") (v "1.2.4") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.4") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "01mxh349zia8m1jidqw6j9vbvy7nm2scaskldzwaia5lhpc2ip0x")))

(define-public crate-solana-notifier-1.2.5 (c (n "solana-notifier") (v "1.2.5") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.4") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1jq75gdcpr0h0g1cpqkbnd1a2zh9h56bhgn4wzyyprb9vq62kb7n")))

(define-public crate-solana-notifier-1.2.6 (c (n "solana-notifier") (v "1.2.6") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.4") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "130ahh201q3gilf34r9dcx5s2jmdkxhyjpx59rq8wffamzwhw4fz")))

(define-public crate-solana-notifier-1.2.7 (c (n "solana-notifier") (v "1.2.7") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.4") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1zddl6sh8yrwxqkq5njisqj1d93xpabqggq5y59ik755aw1501lm")))

(define-public crate-solana-notifier-1.2.8 (c (n "solana-notifier") (v "1.2.8") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.4") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1fvb5ra5a1a7hapdyzr1mmgw1wkgsjc4n87g91h4hy2r7jdp2vvk")))

(define-public crate-solana-notifier-1.2.9 (c (n "solana-notifier") (v "1.2.9") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.4") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0h692a5xnyqahnjp3xwm348cf7ap4dgvaxd2h829qibgdia8fas5")))

(define-public crate-solana-notifier-1.2.10 (c (n "solana-notifier") (v "1.2.10") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.4") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0bjm5m0yllll1bj4378vc2k4s6w6nbr85n72axr1qf6im0l4pfa3")))

(define-public crate-solana-notifier-1.2.12 (c (n "solana-notifier") (v "1.2.12") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.4") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0cabnfzayqn1xq4klir34kk743b4r6knbp2nznga8davwsjm2bxp")))

(define-public crate-solana-notifier-1.2.13 (c (n "solana-notifier") (v "1.2.13") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.4") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0xdbar7jbm2h3lxvdp75hfrgig3mjy377gj96fcis72xf0kpsp2y")))

(define-public crate-solana-notifier-1.2.14 (c (n "solana-notifier") (v "1.2.14") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.4") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "03dbb0wz853b0jyv2fbvxwci4vyzmpdjfwl50ys6c1cj3nifadfs")))

(define-public crate-solana-notifier-1.2.15 (c (n "solana-notifier") (v "1.2.15") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.4") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "15z6hivg41mrf2awc97j30q7591pcdpnxml70w4j9jmiskrsy320")))

(define-public crate-solana-notifier-1.2.16 (c (n "solana-notifier") (v "1.2.16") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.4") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1d908ygia3kxp7p78y7gk7cirbvkgcsh70lkjb1rhb9k4f9jmrx5")))

(define-public crate-solana-notifier-1.2.17 (c (n "solana-notifier") (v "1.2.17") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.4") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1dpqlylryrwsvbazs63j4jahvsm1kf2xy6ma8pl9sya4p8knm1xc")))

(define-public crate-solana-notifier-1.2.18 (c (n "solana-notifier") (v "1.2.18") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.4") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1kgpja6lp8d7bi7j426alxj8jc1ix0wjvc9vc1ijw8bb81bng0f9")))

(define-public crate-solana-notifier-1.2.19 (c (n "solana-notifier") (v "1.2.19") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.4") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "06bw9sajskcz2jijb7njc836yniji56pvib0ll0gvhdda5mhck3n")))

(define-public crate-solana-notifier-1.2.20 (c (n "solana-notifier") (v "1.2.20") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.4") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "11g3fhcsmxzlg7w159xii9yvn531i80a46ycqvx9sp4fgc3cg4np")))

(define-public crate-solana-notifier-1.3.0 (c (n "solana-notifier") (v "1.3.0") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.6") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "08x53jyg43n703ckgs9ajcl4216p658bybinrys319cpdzwpfddd")))

(define-public crate-solana-notifier-1.3.1 (c (n "solana-notifier") (v "1.3.1") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.6") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0hzdfl9nqficbzbipjcj1gvhza3mvmcqv6gkg6r78igaja1j93p1")))

(define-public crate-solana-notifier-1.2.21 (c (n "solana-notifier") (v "1.2.21") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.4") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1cg5f3qj91prd3b8gkkmsn5lg0wn66h872ljzyidfyydhlxj59fc")))

(define-public crate-solana-notifier-1.2.22 (c (n "solana-notifier") (v "1.2.22") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.4") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "02z1nfhvxnsxdr57k8yw03ng8zqz8r855g58qmziagpzycd3sdh1")))

(define-public crate-solana-notifier-1.3.2 (c (n "solana-notifier") (v "1.3.2") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.6") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1q0bq321nv60wq5gl7wrz7wa8w7sj1pk4ki69h97rcxz28zzjsw1")))

(define-public crate-solana-notifier-1.2.23 (c (n "solana-notifier") (v "1.2.23") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.4") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0d7hz98p518ldj06yrhy7r2cia4limh8a1c2r0s08l87vr4w41f2")))

(define-public crate-solana-notifier-1.3.3 (c (n "solana-notifier") (v "1.3.3") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.6") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "02md3mard377mfxfc02q1ajk1862hcf78931kskkf9jjw3da09z7")))

(define-public crate-solana-notifier-1.2.24 (c (n "solana-notifier") (v "1.2.24") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.4") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1ap5929qfss0xh9yvwj8b0j3s58m80pvdrzrs83w7lx3n1bm8qsm")))

(define-public crate-solana-notifier-1.2.25 (c (n "solana-notifier") (v "1.2.25") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.4") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "18zl97db2dvn1ljyzhndciic6k5q91izn5qj6gxjdf78l51768w6")))

(define-public crate-solana-notifier-1.3.4 (c (n "solana-notifier") (v "1.3.4") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.6") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "10wf9bbnd141r19cv55wlx7c8m55lfypjxdnjg3q5vwqrvr292i6")))

(define-public crate-solana-notifier-1.2.26 (c (n "solana-notifier") (v "1.2.26") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.4") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1ci0682nqb23drlsmnn2vq2g9hij2rf22338dd3cr0lv4zafxnmi")))

(define-public crate-solana-notifier-1.3.5 (c (n "solana-notifier") (v "1.3.5") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.6") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0703wghrby06qq1drfznvfbas5ajph68sw70bnnrssqzjq5mvw4y")))

(define-public crate-solana-notifier-1.3.6 (c (n "solana-notifier") (v "1.3.6") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.6") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1im9hfxd4g30ggh54vgwdlnbb3zyhlfj77ajjywy5qid6ibfmyk1")))

(define-public crate-solana-notifier-1.3.7 (c (n "solana-notifier") (v "1.3.7") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.6") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1ipx33jmi77dijgsmq4znzxgxrvpvlnkdgcm68k7vxhr3aynkh00")))

(define-public crate-solana-notifier-1.2.27 (c (n "solana-notifier") (v "1.2.27") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.4") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1qvj27pddgb0h72alcn0pvfv8pmpwc0vm4n0cpd01niap1ci0yqy")))

(define-public crate-solana-notifier-1.3.8 (c (n "solana-notifier") (v "1.3.8") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.6") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1h245nqv890n6n9bs0j544sn6la26s7kx4vy2a1bnmw61nxw3xia")))

(define-public crate-solana-notifier-1.2.28 (c (n "solana-notifier") (v "1.2.28") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.4") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0hi5m7sn9x3j7fascz3vyqf9qcngf6m5kkh4k96a2bdy0ngh7nzh")))

(define-public crate-solana-notifier-1.3.9 (c (n "solana-notifier") (v "1.3.9") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.6") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0k3jypdi1c9fll092sn904yhbay9hbxs3m2hxw148gcvw5zsq4hk")))

(define-public crate-solana-notifier-1.3.10 (c (n "solana-notifier") (v "1.3.10") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.6") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0zwgyyqiqdgj3jgpwfmc524dcaj3qkn1y911lplxcqq9ygxpw2nx")))

(define-public crate-solana-notifier-1.3.11 (c (n "solana-notifier") (v "1.3.11") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.6") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0kzpfjnc8xm56phm7v8nhzpxfzjwjs5cxag222kgsdkifvjb9nw5")))

(define-public crate-solana-notifier-1.2.29 (c (n "solana-notifier") (v "1.2.29") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.4") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0xn0ficphh4ip89xv3gz7zrfz34pzfjvcclxjxmfl7qjdg5yfkk6")))

(define-public crate-solana-notifier-1.3.12 (c (n "solana-notifier") (v "1.3.12") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.6") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "10n19ica6xxnx8x0cmndn0pvgv92shk9w1z71x569f2nf58p0ah8")))

(define-public crate-solana-notifier-1.3.13 (c (n "solana-notifier") (v "1.3.13") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.6") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1qmhkrsijp414rvdrd3wpb4di3bh43mv26rsrg6yipkjybfzzgq9")))

(define-public crate-solana-notifier-1.2.30 (c (n "solana-notifier") (v "1.2.30") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.4") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "19dkc04h7vx7ynd8z0cx6vnvscqbms73knvdcbmayrzkw46hlbb0")))

(define-public crate-solana-notifier-1.3.14 (c (n "solana-notifier") (v "1.3.14") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.6") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1qvr8vsrpjpqkmg0x0ygdf52hb90d57kg1yvk5pz2a8x64cb7al6")))

(define-public crate-solana-notifier-1.2.31 (c (n "solana-notifier") (v "1.2.31") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.4") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1cp20hszirvh1qnjv0lyg7wil10qbf3nfy647vbl1rd3i88xcshn")))

(define-public crate-solana-notifier-1.2.32 (c (n "solana-notifier") (v "1.2.32") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.4") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1saxf5x4yak5db2y4mjxaw6i1df05vh9qrqyhxq3nrs3zf3x3y1n")))

(define-public crate-solana-notifier-1.3.15 (c (n "solana-notifier") (v "1.3.15") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.6") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1k544x0fbcj1k970i380jmbny2n44j4pzyl288wvssshps0x2apz")))

(define-public crate-solana-notifier-1.3.16 (c (n "solana-notifier") (v "1.3.16") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.6") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "14hk5m0d4770pzyrmdlappi2j45s32r1h9gfbc5dg2hqj5vryy7q")))

(define-public crate-solana-notifier-1.3.17 (c (n "solana-notifier") (v "1.3.17") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.6") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1xq21m2ldfk69qb0yj9695xv2qkwaayr8lskfnv1zyz6sb9k878w")))

(define-public crate-solana-notifier-1.4.0 (c (n "solana-notifier") (v "1.4.0") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.8") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "08ggz3h03f9h6vpsvsywb00lja3cxvdjkjq63yiw5qwcqg83sd06")))

(define-public crate-solana-notifier-1.4.1 (c (n "solana-notifier") (v "1.4.1") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.8") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0x27azh8133c3pcafl4jxhcmh2a17zi20156n7cxn74qaj1mlslb")))

(define-public crate-solana-notifier-1.3.18 (c (n "solana-notifier") (v "1.3.18") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.6") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0hdw1dlqkkxln1gb55ck1jqdndwlzj8z60dbp230ajhmk88qk39v")))

(define-public crate-solana-notifier-1.3.19 (c (n "solana-notifier") (v "1.3.19") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.6") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1ipy00a731gis21jfw42rg9y94x6632gnfc66qkwzclrq56vjjvl")))

(define-public crate-solana-notifier-1.4.2 (c (n "solana-notifier") (v "1.4.2") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.8") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0l3bgc97xg1b819s4ck8q24ihh81gvli9rlmv1zv83yk6pm4f5qz")))

(define-public crate-solana-notifier-1.4.3 (c (n "solana-notifier") (v "1.4.3") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.8") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "172nxr49m5758b1j34hl9f1w08nzjlan4j71s1cxfxx8ccf9cq6x")))

(define-public crate-solana-notifier-1.4.4 (c (n "solana-notifier") (v "1.4.4") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.8") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0879h4iwr4hfb330105151l20caznx1fmsxn9zqpf3qfk8ibjxs3")))

(define-public crate-solana-notifier-1.4.5 (c (n "solana-notifier") (v "1.4.5") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.8") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1mhv0v4pll32mccy96jzh8ffabxhjka1safs3q57qb58dgl082bc")))

(define-public crate-solana-notifier-1.4.6 (c (n "solana-notifier") (v "1.4.6") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.8") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "00nfzfbnlr5adhgknmmdav2m3bb234p0c5299lcqw1yf9wjbgv4j")))

(define-public crate-solana-notifier-1.3.20 (c (n "solana-notifier") (v "1.3.20") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.6") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0rwvmn7fp06jpcmd9ar9cdamlb3qh4wpmqmg0gxdh3x7n38wpcz8")))

(define-public crate-solana-notifier-1.4.7 (c (n "solana-notifier") (v "1.4.7") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.8") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "06znxslf5gpbqpycv3s8m75pmjk8vjh8szzbdfyr72allbxwrc2f")))

(define-public crate-solana-notifier-1.3.21 (c (n "solana-notifier") (v "1.3.21") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.6") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "06md034z5xcjwnrk6iha3pv1jhbla1f0icchylyysclnam0bk1wh")))

(define-public crate-solana-notifier-1.4.8 (c (n "solana-notifier") (v "1.4.8") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.8") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "06kcng4kd945wb7yvhl9birziv51kldrj3nq682vybr3w7qk89z3")))

(define-public crate-solana-notifier-1.4.9 (c (n "solana-notifier") (v "1.4.9") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.8") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0sf27idnwwy84v3bswr6fm43jhjngy7pbc0vcy99jrf07ndgnzil")))

(define-public crate-solana-notifier-1.4.10 (c (n "solana-notifier") (v "1.4.10") (d (list (d (n "log") (r ">=0.4.8, <0.5.0") (d #t) (k 0)) (d (n "reqwest") (r ">=0.10.8, <0.11.0") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r ">=1.0.0, <2.0.0") (d #t) (k 0)))) (h "0vsjp9hrg33z7scy9jr1gxh1rrnaczs01wgw3gk5d0q945a3mxxy")))

(define-public crate-solana-notifier-1.4.11 (c (n "solana-notifier") (v "1.4.11") (d (list (d (n "log") (r ">=0.4.8, <0.5.0") (d #t) (k 0)) (d (n "reqwest") (r ">=0.10.8, <0.11.0") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r ">=1.0.0, <2.0.0") (d #t) (k 0)))) (h "168phm4k7k7qvldk21bqsklc50h4nvn7g0ihq0qpk25rjm9qn4xg")))

(define-public crate-solana-notifier-1.4.12 (c (n "solana-notifier") (v "1.4.12") (d (list (d (n "log") (r ">=0.4.8, <0.5.0") (d #t) (k 0)) (d (n "reqwest") (r ">=0.10.8, <0.11.0") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r ">=1.0.0, <2.0.0") (d #t) (k 0)))) (h "1agn51zjyjvdfgp8py7q5y1n2lhq2s156k0vcf9xn2mnlla9x10y")))

(define-public crate-solana-notifier-1.3.23 (c (n "solana-notifier") (v "1.3.23") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.6") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0kkmlaj36qkb68yyavkgp9d3nwfhrw2mf5ddshc2fvvvb7cwkzg8")))

(define-public crate-solana-notifier-1.4.13 (c (n "solana-notifier") (v "1.4.13") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.8") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0xvz88z9s6q04y3ri18pyh4yiyrnbwhi8d2wswxrirkk5bpp9qcx")))

(define-public crate-solana-notifier-1.4.14 (c (n "solana-notifier") (v "1.4.14") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.8") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1szz974sl72fdwy3k55kkr06kvxr0say0s6ai2mcfaia6zryw4wm")))

(define-public crate-solana-notifier-1.4.15 (c (n "solana-notifier") (v "1.4.15") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.8") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0fy6j91jj53fv93f5vajzbm37v5rcjj8bd8vk43yl3vr85z50dcy")))

(define-public crate-solana-notifier-1.4.16 (c (n "solana-notifier") (v "1.4.16") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.8") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0yayi1dyyk8ldnmrn150xv1xnzvihwb1yxmma3l9djinf6381dyf")))

(define-public crate-solana-notifier-1.4.17 (c (n "solana-notifier") (v "1.4.17") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.8") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1d2wjx127m2gqv3f40x8m85d09rjzz9fs942y3nfjywm792qsbff")))

(define-public crate-solana-notifier-1.5.0 (c (n "solana-notifier") (v "1.5.0") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.8") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "190jq4gyizs4s4m6cfqyrlaqkz0l9fyym8pi57z28i7yk6yqcvmm")))

(define-public crate-solana-notifier-1.4.18 (c (n "solana-notifier") (v "1.4.18") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.8") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0bp6rc02j9czi2gr42h0pccy7czng849j7ir8awy7ccq9bvpf1fq")))

(define-public crate-solana-notifier-1.4.19 (c (n "solana-notifier") (v "1.4.19") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.8") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1rh9c3hhzh4s426afrp927hgcxyyn2nxli39500ik6nb9zhl3f8d")))

(define-public crate-solana-notifier-1.4.20 (c (n "solana-notifier") (v "1.4.20") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.8") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "082z9rls27sjng95cdzh4yz8nm54ayr55bv6zskksfy8l0k8zsmn")))

(define-public crate-solana-notifier-1.5.1 (c (n "solana-notifier") (v "1.5.1") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.8") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0gc8imy7l9qgsyy5jg6gvbsraw0spgn31c5fzwqh6pg7k1cz5yl1")))

(define-public crate-solana-notifier-1.4.21 (c (n "solana-notifier") (v "1.4.21") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.8") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0n3mws99l0x3rfdz9p1bpccy842c68wvskmcqpl5mc23psr2dnv3")))

(define-public crate-solana-notifier-1.4.22 (c (n "solana-notifier") (v "1.4.22") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.8") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1jbzjmr0fcayn8xyxj8nrg7lx6i07i01nnz0zywiclglhf0kjysm")))

(define-public crate-solana-notifier-1.5.2 (c (n "solana-notifier") (v "1.5.2") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.8") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0256sxzn0drs5n5cz8s0prd62hb32sq2higr4ksk525wjpbaxxfr")))

(define-public crate-solana-notifier-1.4.23 (c (n "solana-notifier") (v "1.4.23") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.8") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "02nrv8azjdypicrb87p4pjx22h2k5lbl7y8dbp8y1bwbb4zzklxz")))

(define-public crate-solana-notifier-1.5.3 (c (n "solana-notifier") (v "1.5.3") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.8") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "079bk60jp4zz40mccvb5hg5kvnhamvmwffm56sxj5gq728i2py2q")))

(define-public crate-solana-notifier-1.5.4 (c (n "solana-notifier") (v "1.5.4") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.8") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1b920xx5h1qx14v54nqrfz8dl8v9mc621ljkaync5wxpnwwpn68z")))

(define-public crate-solana-notifier-1.5.5 (c (n "solana-notifier") (v "1.5.5") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.8") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1704k27v41xw6g4nznbfwfcp55dlnav861f2b8ha5yb4di5qbrbl")))

(define-public crate-solana-notifier-1.4.25 (c (n "solana-notifier") (v "1.4.25") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.8") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1nzd8mf2ij4bxz96jp57gf3h7hqkwwn1q6aj7rk3k22j4kyghg0g")))

(define-public crate-solana-notifier-1.4.26 (c (n "solana-notifier") (v "1.4.26") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.8") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0nw28nhbv0cpan87kh3sg09rikzd12qq1wr8r889glkl1bbxr821")))

(define-public crate-solana-notifier-1.5.6 (c (n "solana-notifier") (v "1.5.6") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.8") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1b5dij913fqciikbgdhfwjny34xcb55qrfsrgzdciabysnvbbq66")))

(define-public crate-solana-notifier-1.4.27 (c (n "solana-notifier") (v "1.4.27") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.8") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "10psz36l6xsxbqya3wg0yi8fq1kdnv9cq4728vb4xg1zcphyd7z9")))

(define-public crate-solana-notifier-1.5.7 (c (n "solana-notifier") (v "1.5.7") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.8") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0hwvbj0rb9jqvf0ig7pyvmcskffc6bgrk1in8jpvhmssgspcnhif")))

(define-public crate-solana-notifier-1.5.8 (c (n "solana-notifier") (v "1.5.8") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.8") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1iiwnxx2n27dkhwl24nqspaf0ga5ng5jxlh51sliv3s7gf67aqw8")))

(define-public crate-solana-notifier-1.4.28 (c (n "solana-notifier") (v "1.4.28") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.8") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0r4im64z9mcidflillzqgd95bjmgwbablwdd4nh3lhzrmv1s0b7y")))

(define-public crate-solana-notifier-1.5.9 (c (n "solana-notifier") (v "1.5.9") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.8") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "038q6v46yw6nl7b0gfs1fvwqpw6ac8xp2wdzx52yhjqw80zp7inl")))

(define-public crate-solana-notifier-1.5.10 (c (n "solana-notifier") (v "1.5.10") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.8") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0w0gdn1npwlaz19zaqjwicbynyswmfzpwyw57ddc8a1j9ixmppn8")))

(define-public crate-solana-notifier-1.5.11 (c (n "solana-notifier") (v "1.5.11") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.8") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "19bhm93lhk3m5w8z0axmc0f1cqdpvb9w8mfx18h7kcl45paqyhig")))

(define-public crate-solana-notifier-1.5.12 (c (n "solana-notifier") (v "1.5.12") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.8") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1cjxqzr57i81a3gvayiq1ixz1r6j88s6sczbx13vq2qym6bywaf5")))

(define-public crate-solana-notifier-1.5.13 (c (n "solana-notifier") (v "1.5.13") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.8") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1wszn4kdxkmva61kgxrjfc6abln61i8d7gg71px3z1g6l92r6n7k")))

(define-public crate-solana-notifier-1.5.14 (c (n "solana-notifier") (v "1.5.14") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.8") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "05czc4rbihwv5pmsigp26nkf5h2mqgn7jcp25bpbzrnapdz8hg4j")))

(define-public crate-solana-notifier-1.6.0 (c (n "solana-notifier") (v "1.6.0") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.8") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1snydwhf3f8dcpmwsrr4yk1ksnacpvnaaabl5w9bm8chx6vzm7xh")))

(define-public crate-solana-notifier-1.5.15 (c (n "solana-notifier") (v "1.5.15") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.8") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0f4vyvfw64l9alr49qn7vczgaskbnqbbz5bnda4ivj0cx3bs4kqz")))

(define-public crate-solana-notifier-1.6.1 (c (n "solana-notifier") (v "1.6.1") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.8") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1zcvdk9b300h5iyiizaaglpm07gc7h8sfry4qkaiw3pc9kzw7mvy")))

(define-public crate-solana-notifier-1.5.16 (c (n "solana-notifier") (v "1.5.16") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.8") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "079hz3b6s6q4kkq15f9n5kaks6zx5qvyhi5lg3gpss2wnvw4123n")))

(define-public crate-solana-notifier-1.5.17 (c (n "solana-notifier") (v "1.5.17") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.8") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0vzaz4cnk4ni6pjj5c7j31g1xxr9yy17hvlr616ykl7q2y9hn61n")))

(define-public crate-solana-notifier-1.6.2 (c (n "solana-notifier") (v "1.6.2") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.2") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "07804i1758c731y89lrakai130x9ih3paphji7aq4k7cdfc7qlb7")))

(define-public crate-solana-notifier-1.5.18 (c (n "solana-notifier") (v "1.5.18") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.8") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1y9admsz4l89vmccdh20v4gzkkxplhp3n3fcba8nvdxwlq6x3fq2")))

(define-public crate-solana-notifier-1.6.3 (c (n "solana-notifier") (v "1.6.3") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.2") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "00yf0lflakrdfd9y6f0k6zdg0b76qvqjsyk8p4hrk6vk6g2fqx0d")))

(define-public crate-solana-notifier-1.6.4 (c (n "solana-notifier") (v "1.6.4") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.2") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0k5c0ld6mfqbmvw52wjhc3vhs1qibpaxgbcaqdyl3nscqapfyk3h")))

(define-public crate-solana-notifier-1.6.5 (c (n "solana-notifier") (v "1.6.5") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.2") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "092qnfswvqhwcaa70zxd9b9ls94ys08kz8z6jacd00r7zijncrwn")))

(define-public crate-solana-notifier-1.6.6 (c (n "solana-notifier") (v "1.6.6") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.2") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0fw2azz1n88rbcyg2ivs8zi5dzg4x2bshxa7ccfx6h5gk5186m8n")))

(define-public crate-solana-notifier-1.5.19 (c (n "solana-notifier") (v "1.5.19") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.8") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1d278hiajvzpjs5023771mbhd2jbbsacg97gz7izbkjzjy2siclc")))

(define-public crate-solana-notifier-1.6.7 (c (n "solana-notifier") (v "1.6.7") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.2") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "07p41k96srdb2cfddsl2l9cl27ksb6sqghix63sz07kxikyf5zvj")))

(define-public crate-solana-notifier-1.6.8 (c (n "solana-notifier") (v "1.6.8") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.2") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0mh91jd04h3ay0ansc4n4biajrsi91gq8mzv12i3by92w90nkms2")))

(define-public crate-solana-notifier-1.6.9 (c (n "solana-notifier") (v "1.6.9") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.2") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1z7d6h2xmbk8ki6lra0gq72zfgn2932j2xc3s4z04agb1zylk5m1")))

(define-public crate-solana-notifier-1.6.10 (c (n "solana-notifier") (v "1.6.10") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.2") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0x066ab0svnanj6w0hwnxgzswf61fch5fxkc2yi02zhn0q75frga")))

(define-public crate-solana-notifier-1.6.11 (c (n "solana-notifier") (v "1.6.11") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.2") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1ay385vbbki99401mbshbm7lrgwmaxbcm0iwjqwbch40am7r1zhs")))

(define-public crate-solana-notifier-1.7.0 (c (n "solana-notifier") (v "1.7.0") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.2") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "02vp5fqrspk3s695hcl1djjxdlynxbf9qkh0zrdd9vspj261gasb")))

(define-public crate-solana-notifier-1.7.1 (c (n "solana-notifier") (v "1.7.1") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.2") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0m6pqvq86y3rlpd85zyc5yixd5mib2kyxadc47rvn92ygg9n3sc4")))

(define-public crate-solana-notifier-1.6.12 (c (n "solana-notifier") (v "1.6.12") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.2") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1jfrnkcynmfmbsj6h7nwnhmv65vs0fvarnsv1cj5n0q3y9xg3nj2")))

(define-public crate-solana-notifier-1.6.13 (c (n "solana-notifier") (v "1.6.13") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.2") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0y8jbnfddgbmrk0hnw6fwpqznv3ya8gxf9m604j78i3kwx5hcamj")))

(define-public crate-solana-notifier-1.7.2 (c (n "solana-notifier") (v "1.7.2") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.2") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0hp0fqcjb6fv347rzrmzxmxs79g45gd4lb2y5160p2h8j1qjnlb1")))

(define-public crate-solana-notifier-1.6.14 (c (n "solana-notifier") (v "1.6.14") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.2") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1pb39yi8j2b34f0f6pvzq876j8hxspw0fywdq60x5j7jsxsaff2y")))

(define-public crate-solana-notifier-1.7.3 (c (n "solana-notifier") (v "1.7.3") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.2") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0zplhykss9rcxwahh9s4a0p9053h17ybqsxw25jh5j380fy65a72")))

(define-public crate-solana-notifier-1.6.15 (c (n "solana-notifier") (v "1.6.15") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.2") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "04k1y9nrvrpx03kldh7k264lzrfdil25zvc723yx5cg2f80hv8ad")))

(define-public crate-solana-notifier-1.7.4 (c (n "solana-notifier") (v "1.7.4") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.2") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1y9fqa9vm0h549xjb7z1fy7l425m63gqqf07cjrnc84mj3jic8yg")))

(define-public crate-solana-notifier-1.6.16 (c (n "solana-notifier") (v "1.6.16") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.2") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1f6r6zkf8wjbmbhzmk0zdqx9qycdck3mpy5f8yj147zkv7vmibj7")))

(define-public crate-solana-notifier-1.6.17 (c (n "solana-notifier") (v "1.6.17") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.2") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1iri843lbrznqd80fpcgc7cj5v82pg2xd5wv3vxr86gb6g7fdxy4")))

(define-public crate-solana-notifier-1.7.5 (c (n "solana-notifier") (v "1.7.5") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.2") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0mra3b1y74pg3f8q5rzq6cyxah0g8w54lcwiqqw4q5zi912qf0fa")))

(define-public crate-solana-notifier-1.7.6 (c (n "solana-notifier") (v "1.7.6") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.2") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "03q5yzq7jiqdbk4iijnlq2lw0fh2syqlgyrrkv6c90rq523gxxyf")))

(define-public crate-solana-notifier-1.6.18 (c (n "solana-notifier") (v "1.6.18") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.2") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1wvkhzwimxjzzdj51k2fq7sjqh59dn3i19kqpy6fjsm2pbxc8g09")))

(define-public crate-solana-notifier-1.6.19 (c (n "solana-notifier") (v "1.6.19") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.2") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0rc0vjw5iqayvx08xl5pax529i3wzxq3qznn3s6qm47dzadqaxjb")))

(define-public crate-solana-notifier-1.7.7 (c (n "solana-notifier") (v "1.7.7") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.2") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1171da0i1iz6hyxg3wl48rgm22ywmzi74d17g5qkq1d2aqwzy9a0")))

(define-public crate-solana-notifier-1.7.8 (c (n "solana-notifier") (v "1.7.8") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.2") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "038y9kffslj6pchf8zmqc3nzqxjhi5vfm59v4pajrwl7fxmg3y5i")))

(define-public crate-solana-notifier-1.6.20 (c (n "solana-notifier") (v "1.6.20") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.2") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "05a2lcfk19s5bl9yp9yviyzm45h3a93h0v5gn8kf6mr06w3fnp5l")))

(define-public crate-solana-notifier-1.7.9 (c (n "solana-notifier") (v "1.7.9") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.2") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "03q78pnpr1r2aln318jmqrzc764k28r1ncwlxm860y3h65csxbsx")))

(define-public crate-solana-notifier-1.7.10 (c (n "solana-notifier") (v "1.7.10") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.2") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0cj8yldlydx0h9a3s5imq9mm55bzr531abvd5zwgycm1pydim7jp")))

(define-public crate-solana-notifier-1.6.21 (c (n "solana-notifier") (v "1.6.21") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.2") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0r4dl1yp27rlw8himl445fg2h6g1n75iv7amka3jacsx85jnc8yd")))

(define-public crate-solana-notifier-1.6.22 (c (n "solana-notifier") (v "1.6.22") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.2") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0583zdrkd0ay7h8drb93i6149x6yk9n9ism0l0rgmmhgr0lchfl3")))

(define-public crate-solana-notifier-1.7.11 (c (n "solana-notifier") (v "1.7.11") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.2") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1kgjgwlz9kjixizshnn5dfyk9k3fs82vg19g5jdh6dmdl8qkb5mj")))

(define-public crate-solana-notifier-1.6.23 (c (n "solana-notifier") (v "1.6.23") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.2") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "001d45jqp8nyjn1sdxqwp98z8q490gx80wnb363846qsfn2r4sy0")))

(define-public crate-solana-notifier-1.6.24 (c (n "solana-notifier") (v "1.6.24") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.2") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0bwj6hgx5kwjrfpw306prjn5ki34y5q669hrpjd3g6x64izfdd7y")))

(define-public crate-solana-notifier-1.6.25 (c (n "solana-notifier") (v "1.6.25") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.2") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "00sib5wla9pyk2wsf0nkfl065bv334vyl2ngqda874cjjw1vr1aa")))

(define-public crate-solana-notifier-1.7.12 (c (n "solana-notifier") (v "1.7.12") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.2") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1f22kizx0mlsfnknqz4hwkcfli7yai4lb7c4z9m2iij6jfnqhj38")))

(define-public crate-solana-notifier-1.6.26 (c (n "solana-notifier") (v "1.6.26") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.2") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1vynj94iwiq46q9043jw2gja5pr8ih6ix6zgj78gnsl5xflrnf33")))

(define-public crate-solana-notifier-1.6.27 (c (n "solana-notifier") (v "1.6.27") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.2") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0j1jh9wa9s6alc95ak85ai5j47aqxlk2zdiclajq20n449wnxzir")))

(define-public crate-solana-notifier-1.7.13 (c (n "solana-notifier") (v "1.7.13") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.2") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0b05w6l12h3s0356p3ap3k24lmjnv6ajn92b16lfv1601darnnwa")))

(define-public crate-solana-notifier-1.7.14 (c (n "solana-notifier") (v "1.7.14") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.2") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1wq5yg074953yqd1w54jvnanf1wsh7rh31sb1hl3afpvhpwhpc60")))

(define-public crate-solana-notifier-1.8.0 (c (n "solana-notifier") (v "1.8.0") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.2") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0h4fgrn4smd3p9ij61rzijpp0kw41j4wdfvvvnv3g8x07bfw13bd")))

(define-public crate-solana-notifier-1.6.28 (c (n "solana-notifier") (v "1.6.28") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.2") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0j9m292sk0d11kgwlccqbwmnxa6sl52vypv845va9n9y9prr667p")))

(define-public crate-solana-notifier-1.7.15 (c (n "solana-notifier") (v "1.7.15") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.2") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "08ss16wwv62rssjxlpr7aw19xa4hzmrmwdj603rql7hvwqrnqd45")))

(define-public crate-solana-notifier-1.8.1 (c (n "solana-notifier") (v "1.8.1") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.2") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "13pwfpwglbrvga10xq45kbf5657zypw889shcap5a9in4znxkfxr")))

(define-public crate-solana-notifier-1.7.16 (c (n "solana-notifier") (v "1.7.16") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.2") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1ww7xy3lrxbb2mqh741k570p950zb3wlc5yvl2mir571c65b6hp6")))

(define-public crate-solana-notifier-1.7.17 (c (n "solana-notifier") (v "1.7.17") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.2") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1wd0hgiqhc82gy14z4mzflcd5wvi3q1nk864570ymbh7crzg2fl8")))

(define-public crate-solana-notifier-1.8.2 (c (n "solana-notifier") (v "1.8.2") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.2") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "04g7ahfpckxc7i7cgyqgpmmn7n13m7vbmgi6m394z85ili3cphy7")))

(define-public crate-solana-notifier-1.8.3 (c (n "solana-notifier") (v "1.8.3") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.2") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1r4inplkirjkggqc7s50gy6m0g15pdvrhv7a47dy8zqgc0k3dmyn")))

(define-public crate-solana-notifier-1.8.4 (c (n "solana-notifier") (v "1.8.4") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.2") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0j70qgnk9fcxg1jhxfx1h8qap2r7pbp3nrxqsgpwq1y67clm6bnz")))

(define-public crate-solana-notifier-1.8.5 (c (n "solana-notifier") (v "1.8.5") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.2") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "06x0xpr6cp235k0945whrfcydpr87lnfcxgk5wzb29n5q8awwqzv")))

(define-public crate-solana-notifier-1.8.6 (c (n "solana-notifier") (v "1.8.6") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.2") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "019pdpg6y94b0rf0l0fpi6xh4hxq3s6yrqcs8z375ln1wvyr1m7a")))

(define-public crate-solana-notifier-1.8.7 (c (n "solana-notifier") (v "1.8.7") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.2") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "117rp8ipqv319ldcx163fcc3z4l39y0hmm3qn1hn40hggfjm9k4f")))

(define-public crate-solana-notifier-1.8.8 (c (n "solana-notifier") (v "1.8.8") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.2") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1f3k9d107hb1la87cmlrkgn6543wr8fdlxyhzjswknprphd97x26")))

(define-public crate-solana-notifier-1.8.9 (c (n "solana-notifier") (v "1.8.9") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.2") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0gwavd1zvybgybwcp5fb6b1533r1dik1p6dnvdppkvzl1gifnrx2")))

(define-public crate-solana-notifier-1.9.0 (c (n "solana-notifier") (v "1.9.0") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.6") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1fbhp7h3dfknj8740wcwq7819wbc6q4q5z5dswakaj4zjgn4njsn")))

(define-public crate-solana-notifier-1.8.10 (c (n "solana-notifier") (v "1.8.10") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.2") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1zxqkbrn2n67sr37bpav4d4cjxmm7hnk9x2hcplixkfjib7s5ink")))

(define-public crate-solana-notifier-1.8.11 (c (n "solana-notifier") (v "1.8.11") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.2") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0f8kk2mk6dpghl4rqcjzy9xnyzz9xqmz6wpwd8ml4sbaprnr2l7y")))

(define-public crate-solana-notifier-1.9.1 (c (n "solana-notifier") (v "1.9.1") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.6") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0938552cphcbnrf77cfmnxdwbxlhwi45ybi67xd7pgp63vh10xqs")))

(define-public crate-solana-notifier-1.9.2 (c (n "solana-notifier") (v "1.9.2") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.6") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1d0gsr14411j1m1qjdcm9pznmwv441mmnbymkmrahssc6da9f8zc")))

(define-public crate-solana-notifier-1.9.3 (c (n "solana-notifier") (v "1.9.3") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.6") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0v5d1nsglxvb7f7pyh7sa4rv7camm07gm2dnbh4map4h1gw10af8")))

(define-public crate-solana-notifier-1.8.12 (c (n "solana-notifier") (v "1.8.12") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.2") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1vwgwdjnxvfhyxcgkpq7l080gnb39s3z0irjbg7la9yqfj876s60")))

(define-public crate-solana-notifier-1.9.4 (c (n "solana-notifier") (v "1.9.4") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.6") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0pz9ffys7wh523b11y0wp0v31hhrxfhkrr2aj3j8awwvif4ssic1")))

(define-public crate-solana-notifier-1.8.13 (c (n "solana-notifier") (v "1.8.13") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.2") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0gxs0mhk8rbfg40hing8cr49x3ybq0mbix39ncslrkwp4c38b3vm")))

(define-public crate-solana-notifier-1.9.5 (c (n "solana-notifier") (v "1.9.5") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.6") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0rww44kv06h1hczsqcsy5pvz63v446ya72875j9dbgnlj0rj9rgl")))

(define-public crate-solana-notifier-1.8.14 (c (n "solana-notifier") (v "1.8.14") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.2") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1i43ggnr6dj085b48k58v27z6b1gv7x6zq8h3yw8wlmhwgpzcggb")))

(define-public crate-solana-notifier-1.9.6 (c (n "solana-notifier") (v "1.9.6") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.6") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1qfgdbnv016zwj5njwf51wz155qgzw0g19aavnc751xvkacdww9f")))

(define-public crate-solana-notifier-1.9.7 (c (n "solana-notifier") (v "1.9.7") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.6") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1fzlm774mvgqcjpnxzdhcqklci4f2m3qhi1ypyjb9qb8mz234m0z")))

(define-public crate-solana-notifier-1.8.16 (c (n "solana-notifier") (v "1.8.16") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.2") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0v8m1cxxyr86p5b92ls3gp6i5yxk35wwf9b2xhsxjk7ihrab0a0j")))

(define-public crate-solana-notifier-1.9.8 (c (n "solana-notifier") (v "1.9.8") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.6") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "07wsxa2461dwb7mpqczc3y302pazz449b7r4bnxs2mv2vh823km1")))

(define-public crate-solana-notifier-1.9.9 (c (n "solana-notifier") (v "1.9.9") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.6") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1ip1gsccxbxa1axznsq6iglqsx1hih3glzw8w98w780xanszskg1")))

(define-public crate-solana-notifier-1.10.0 (c (n "solana-notifier") (v "1.10.0") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.9") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1h0wv86pradbilpkmgs9sa0g0qnw62r3yqk5gfxvqrpg32rr161z")))

(define-public crate-solana-notifier-1.9.10 (c (n "solana-notifier") (v "1.9.10") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.6") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0mb0409hmbbshsq8brf2772mivladmd5kspxdn369p0kdni83zmg")))

(define-public crate-solana-notifier-1.9.11 (c (n "solana-notifier") (v "1.9.11") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.6") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1swhfy2nza13yvx0cdj8nxm8yyr1jj1cvf7r8fshkqalscs8zspz")))

(define-public crate-solana-notifier-1.9.12 (c (n "solana-notifier") (v "1.9.12") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.6") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1lcyw1lmggbxn2bz5bmr18bz60v64ck7gr4ih0an18ikmqhi0lr0")))

(define-public crate-solana-notifier-1.10.1 (c (n "solana-notifier") (v "1.10.1") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.9") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "01vq2jdchnj5h07q3qgs8xw71yycx9wwgpfqsb4rh3f451542fkv")))

(define-public crate-solana-notifier-1.10.2 (c (n "solana-notifier") (v "1.10.2") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.9") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "07jq6l3hqd595mxjk4cilbimbn4yg7hg4017zz8cp7i6szfvfqbr")))

(define-public crate-solana-notifier-1.9.13 (c (n "solana-notifier") (v "1.9.13") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.6") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1mvam79mnj4f1hk9wf285lxywjsdbjgdyc990wn44qpbb2sjaqaq")))

(define-public crate-solana-notifier-1.10.3 (c (n "solana-notifier") (v "1.10.3") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "14y2zbd5nsk5xxbjvk4cqj1z9qvx6mm5b4svm0hkbq49p8l9nw4h")))

(define-public crate-solana-notifier-1.9.14 (c (n "solana-notifier") (v "1.9.14") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.6") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0klx5n1ffbx9hwqk4abrb5rc8mm9sw337n6pl0dklsi4l2y0cnvn")))

(define-public crate-solana-notifier-1.10.4 (c (n "solana-notifier") (v "1.10.4") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1idd724bbfvikd7nv465dsch3js61rg8y5d7w00nqx23qmhv5hc1")))

(define-public crate-solana-notifier-1.10.5 (c (n "solana-notifier") (v "1.10.5") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "078pz85j7q30cfacy3f4798p3r49d1n4bkfm0p3lxk6iqkds6kqw")))

(define-public crate-solana-notifier-1.10.6 (c (n "solana-notifier") (v "1.10.6") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1mqz1wip6r5hwgh4ghd20f5jkc7f5cy358kihjr3278nv7jczghj")))

(define-public crate-solana-notifier-1.9.15 (c (n "solana-notifier") (v "1.9.15") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.6") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0yc8i4069nfsbkfk8900ywhqj26q1q2qk14vavfylw71i5pjlmx1")))

(define-public crate-solana-notifier-1.10.7 (c (n "solana-notifier") (v "1.10.7") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "050942ksm58lafc44rdxfpbza4shrznqaixc83pg1qg8c3ajgg0i")))

(define-public crate-solana-notifier-1.10.8 (c (n "solana-notifier") (v "1.10.8") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "16ag8hvpddw0f8xh7bwbyy680xnnsaf96iqvvkr03r3kr4jxm0ln")))

(define-public crate-solana-notifier-1.9.16 (c (n "solana-notifier") (v "1.9.16") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.6") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1zlar0611aws8ag5d4hsg32ar883xasn088qgwpzdi9nvjnjyw2z")))

(define-public crate-solana-notifier-1.9.17 (c (n "solana-notifier") (v "1.9.17") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.6") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0xhqrw7jc8f698iknm1ciasngsjcja1kpf139f5rqrz4sd5pzkgq")))

(define-public crate-solana-notifier-1.10.9 (c (n "solana-notifier") (v "1.10.9") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1jiq4nxcbczm4ab81f35s30m5xyqmfkgnczjb5gdkgzpbq1f160j")))

(define-public crate-solana-notifier-1.9.18 (c (n "solana-notifier") (v "1.9.18") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.6") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "03aigw240qxdyzrbwgvfc2rsf44ysgh8vvc9lsz6vwycah76yn9w")))

(define-public crate-solana-notifier-1.10.10 (c (n "solana-notifier") (v "1.10.10") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "09f96z07hjsbxnra1pnfc88r5m4shkbzzfs82lynf3xj3i38kgnb")))

(define-public crate-solana-notifier-1.10.11 (c (n "solana-notifier") (v "1.10.11") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1zvi9m3qql4immm4vlfd2ynsy31kpvar1nwaaa1l2gz33700srih")))

(define-public crate-solana-notifier-1.9.19 (c (n "solana-notifier") (v "1.9.19") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.6") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0rlyw0zajx2kxf9dsvd4j2yds1wj4f21lkdqq434v7xx4fx2p3mi")))

(define-public crate-solana-notifier-1.10.12 (c (n "solana-notifier") (v "1.10.12") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0pr07rw078kyyz3x62j7q3al0a6yryjpljwsk222iccln4xn9wj7")))

(define-public crate-solana-notifier-1.9.20 (c (n "solana-notifier") (v "1.9.20") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.6") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1a0zcfm4bs3c0hynilisaf9kjlzadch923vkpp34hg0a1bxj4b3c")))

(define-public crate-solana-notifier-1.10.13 (c (n "solana-notifier") (v "1.10.13") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0sn99009bvbj66i08rpjjaklplm54hw5spgrzzbfhqjrfh5nh8l6")))

(define-public crate-solana-notifier-1.9.21 (c (n "solana-notifier") (v "1.9.21") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.6") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0n2pq2lq7jbiy86xixnaw205iz0aamvxqkfxrzz3nav9z6257v35")))

(define-public crate-solana-notifier-1.10.14 (c (n "solana-notifier") (v "1.10.14") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0wfqkhvn8xjqs0c41imp8cbns8r25c85ygxvbvnjhkqmmmd7jpnf")))

(define-public crate-solana-notifier-1.9.22 (c (n "solana-notifier") (v "1.9.22") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.6") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1zbapisx2i2hsyy1h4z3k9vzip1mm0sz56h3bbgqlgl5caf837h5")))

(define-public crate-solana-notifier-1.10.15 (c (n "solana-notifier") (v "1.10.15") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0173kq8vzvc0x4yndlyw2777c8l8kk660iajg43y2wm4yzwfd0hl")))

(define-public crate-solana-notifier-1.10.16 (c (n "solana-notifier") (v "1.10.16") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1nfzi7gm13339b6fcg415zc8gjgr2cz6ry7hmx1j43r9kykrnkd1")))

(define-public crate-solana-notifier-1.10.17 (c (n "solana-notifier") (v "1.10.17") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "06nrfhzvagvwqvdj1i0qin7c4yzpc4nxsbn6bzqml8qvcahaa6a6")))

(define-public crate-solana-notifier-1.10.18 (c (n "solana-notifier") (v "1.10.18") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1xdkcx0aspidlhrpwdzlfibdp51gy8wc9fvr7gzsy0ri1xxb2yx5")))

(define-public crate-solana-notifier-1.9.23 (c (n "solana-notifier") (v "1.9.23") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.6") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0fhna9axhvgjcy35w23mxzmy8q3rcy8gxzmf5agsx5b67f14mgzi")))

(define-public crate-solana-notifier-1.10.19 (c (n "solana-notifier") (v "1.10.19") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0z09idvqlbfk7clgq63gh009hpqd4c69ib5nz56l1p3q8d0c0gpk")))

(define-public crate-solana-notifier-1.9.25 (c (n "solana-notifier") (v "1.9.25") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.6") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1yby084f8s7spb4ji8pbwg5d6gcmccqp3f2cs3bbpr4gc19sn9f8")))

(define-public crate-solana-notifier-1.10.20 (c (n "solana-notifier") (v "1.10.20") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1ij672lbf0jn3qh386cdcq38jq955ir6ya49kcca4dymhgfhmrxd")))

(define-public crate-solana-notifier-1.9.26 (c (n "solana-notifier") (v "1.9.26") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.6") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "06vzzdmpvj6fmwr8xqh9ikjm5wlwi2c9bgv0w2bx7nglz0fizx5c")))

(define-public crate-solana-notifier-1.10.21 (c (n "solana-notifier") (v "1.10.21") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0ayiyqsbynsp6hjr0l0fgm5x4awgpfi94gcdrljbxq0igydw0ad3")))

(define-public crate-solana-notifier-1.10.22 (c (n "solana-notifier") (v "1.10.22") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0pf6w60xi8dqlg15k3y3a8r3yrlgkahsncm2i9nsgr51919649gf")))

(define-public crate-solana-notifier-1.9.28 (c (n "solana-notifier") (v "1.9.28") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.6") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0506zmgjqc5zyx98dxy0lipd64h754sm542i5hkwxb28v27x0bzs")))

(define-public crate-solana-notifier-1.10.23 (c (n "solana-notifier") (v "1.10.23") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0fwkrfa636nnx0wbprs7r8wslap81l1ybqrd24ab8zi2l6kmdp6z")))

(define-public crate-solana-notifier-1.10.24 (c (n "solana-notifier") (v "1.10.24") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1zrv6x5s7n9a7hl51d86kai62bz01yxj2x4rhlmy0ybvb6ir5lsf")))

(define-public crate-solana-notifier-1.10.25 (c (n "solana-notifier") (v "1.10.25") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0l3l0yp13zyq9f1z6cvabyh81b98lwq60fvs2hwy8n1w94cviqzi")))

(define-public crate-solana-notifier-1.9.29 (c (n "solana-notifier") (v "1.9.29") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.6") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "06ma3zp16fldzq7dwk37r72d1mjzzvlakfbpz42k18hqypdcbj5d")))

(define-public crate-solana-notifier-1.10.26 (c (n "solana-notifier") (v "1.10.26") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1zkdb57dwp454h5j9dd88rv6ysnbp2kp0qp2gsygiq49ayh1qc4m")))

(define-public crate-solana-notifier-1.11.0 (c (n "solana-notifier") (v "1.11.0") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "00bsa4hdlq5qddcdb4yp9w8gz8sv167ybfqr8lllznbdf4x3qyd9")))

(define-public crate-solana-notifier-1.10.27 (c (n "solana-notifier") (v "1.10.27") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0di2vq6ihzdzcismcg7gqcz4wl48h8g3b7chi963a8s1gngxr697")))

(define-public crate-solana-notifier-1.11.1 (c (n "solana-notifier") (v "1.11.1") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0iszdqjxcqyxil0gz4zaxciq5q3by5ydby0k5xcaq4f0ak4gl8h2")))

(define-public crate-solana-notifier-1.10.28 (c (n "solana-notifier") (v "1.10.28") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1d2whzgvkb6yvb58k6klh4dr37mgnsj6g4wbsf4nl8n90rn4n4l9")))

(define-public crate-solana-notifier-1.10.29 (c (n "solana-notifier") (v "1.10.29") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (f (quote ("blocking" "brotli" "deflate" "gzip" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0lfhv1j7f50025flmlp1kzddhl1m3wy62k0cca3ljsg4fa12k8gr")))

(define-public crate-solana-notifier-1.10.30 (c (n "solana-notifier") (v "1.10.30") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (f (quote ("blocking" "brotli" "deflate" "gzip" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0fi0cabspqzigk2kjcpgfigzkdn6c3idxcvs8cd3y7nc0fvxq11v")))

(define-public crate-solana-notifier-1.11.2 (c (n "solana-notifier") (v "1.11.2") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (f (quote ("blocking" "brotli" "deflate" "gzip" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1xv7f208bgpaqkvq688p3aqlh8v78yvswh61llpjh216hmsbl0l0")))

(define-public crate-solana-notifier-1.10.31 (c (n "solana-notifier") (v "1.10.31") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (f (quote ("blocking" "brotli" "deflate" "gzip" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "17jxvvm2bjki6asxv037m57yxhbdp9qx3dwql2yhvkxlnv6n8hlh")))

(define-public crate-solana-notifier-1.11.3 (c (n "solana-notifier") (v "1.11.3") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (f (quote ("blocking" "brotli" "deflate" "gzip" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1rw1ls34xgmzcqczrc6nl41hxprgmrcw23zrj0wic8s0dvgnwfaw")))

(define-public crate-solana-notifier-1.10.32 (c (n "solana-notifier") (v "1.10.32") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (f (quote ("blocking" "brotli" "deflate" "gzip" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1avzxx5fx2mb0qj2xy25abh2b694g2xw7sd1hyxp33va4bb8smw3")))

(define-public crate-solana-notifier-1.11.4 (c (n "solana-notifier") (v "1.11.4") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (f (quote ("blocking" "brotli" "deflate" "gzip" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0skcfxg294bji1ixyq10f5yd05b9rih1aad7b32p69zqy1wfd1gh")))

(define-public crate-solana-notifier-1.10.33 (c (n "solana-notifier") (v "1.10.33") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (f (quote ("blocking" "brotli" "deflate" "gzip" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0z5mdzs7h6d91yxq06i3cmx734fy7g3csgzy3anl4q6bm3232fn7")))

(define-public crate-solana-notifier-1.10.34 (c (n "solana-notifier") (v "1.10.34") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (f (quote ("blocking" "brotli" "deflate" "gzip" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0xmgd67kbdxmpbqb6l1vmlky5lwddf784wa0189zj07kd20xjyyh")))

(define-public crate-solana-notifier-1.11.5 (c (n "solana-notifier") (v "1.11.5") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (f (quote ("blocking" "brotli" "deflate" "gzip" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "056xl55a6pggps223j43jyi2axq2v380xpp9n3f9ld0cwb3k01nd")))

(define-public crate-solana-notifier-1.10.35 (c (n "solana-notifier") (v "1.10.35") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (f (quote ("blocking" "brotli" "deflate" "gzip" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0wsdmr3l0rkqbcl1vhmg5b3k70a23bhq1vgh78adxj159f6a2qa1")))

(define-public crate-solana-notifier-1.11.6 (c (n "solana-notifier") (v "1.11.6") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (f (quote ("blocking" "brotli" "deflate" "gzip" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "05fbnsqkyxnr3yvdizv1wg6d747izgha1i9ppmzy9322lcd4i1fr")))

(define-public crate-solana-notifier-1.11.7 (c (n "solana-notifier") (v "1.11.7") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (f (quote ("blocking" "brotli" "deflate" "gzip" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0ivlr7vapfq7pjwi8ms3rrcd0l47cwm4sn3w42csgrgbj9zwcmwf")))

(define-public crate-solana-notifier-1.11.8 (c (n "solana-notifier") (v "1.11.8") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (f (quote ("blocking" "brotli" "deflate" "gzip" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1mycsgprn05jmkbbapzaf00cy6gmh487wl1yy2ssxpkp7fclgi7x")))

(define-public crate-solana-notifier-1.11.10 (c (n "solana-notifier") (v "1.11.10") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (f (quote ("blocking" "brotli" "deflate" "gzip" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "07lx5i3zh9n4jdblgrz5kd6jiv861dm95rnzr0na8kx9kq18bx5w")))

(define-public crate-solana-notifier-1.10.38 (c (n "solana-notifier") (v "1.10.38") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (f (quote ("blocking" "brotli" "deflate" "gzip" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1pqd3llscyz1s5krjhna6snmlsw80630qkfjpjmybmkvii5szkc2")))

(define-public crate-solana-notifier-1.13.0 (c (n "solana-notifier") (v "1.13.0") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (f (quote ("blocking" "brotli" "deflate" "gzip" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0sfiz5qyxp4nc56dwv4jff2nhnsyar9b07gjp6jjp6hn7z30dhmc")))

(define-public crate-solana-notifier-1.14.0 (c (n "solana-notifier") (v "1.14.0") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (f (quote ("blocking" "brotli" "deflate" "gzip" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "00kni4bfg97aqhxxflwhzlizvwy8mk3r0klb62af2nan7hzq6f7i")))

(define-public crate-solana-notifier-1.14.1 (c (n "solana-notifier") (v "1.14.1") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (f (quote ("blocking" "brotli" "deflate" "gzip" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "00yxfj1k919qp0pvzr276vbyjvvhkkwmsj15v5vn5rk2dq322q72")))

(define-public crate-solana-notifier-1.10.39 (c (n "solana-notifier") (v "1.10.39") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (f (quote ("blocking" "brotli" "deflate" "gzip" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1s6jvq6wqlyplrp6dj5z7cfqyykpgsaq9n0nnfzppsgq2j9718iq")))

(define-public crate-solana-notifier-1.14.2 (c (n "solana-notifier") (v "1.14.2") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (f (quote ("blocking" "brotli" "deflate" "gzip" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0whazxvw6836fygr31b2rkynp31ipffvh8d5mpbrncyf9jgb8645")))

(define-public crate-solana-notifier-1.13.1 (c (n "solana-notifier") (v "1.13.1") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (f (quote ("blocking" "brotli" "deflate" "gzip" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0i2dxhxg84pc38cl3mcdd6adc9sahnislfvyzs61ss6a85kyc98i")))

(define-public crate-solana-notifier-1.14.3 (c (n "solana-notifier") (v "1.14.3") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (f (quote ("blocking" "brotli" "deflate" "gzip" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1qh3wxr6a6xqqa5mcdvidcsf75m86f980kfzqqfnhip5y958y6qb")))

(define-public crate-solana-notifier-1.10.40 (c (n "solana-notifier") (v "1.10.40") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (f (quote ("blocking" "brotli" "deflate" "gzip" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1ca2k9js0qiwpgdb9jlcvlpkml0j79xa3m8rlldiyzd49zy4b8vk")))

(define-public crate-solana-notifier-1.14.4 (c (n "solana-notifier") (v "1.14.4") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (f (quote ("blocking" "brotli" "deflate" "gzip" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "19pkydzi84d6ggj4az9ylha2gsnjya5aihd1daiqsg8rm5g3gnmn")))

(define-public crate-solana-notifier-1.13.2 (c (n "solana-notifier") (v "1.13.2") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (f (quote ("blocking" "brotli" "deflate" "gzip" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "14jszr4p03vll7axsbmc1b595kk12axhbkllj2kvbajf3jslxmn7")))

(define-public crate-solana-notifier-1.14.5 (c (n "solana-notifier") (v "1.14.5") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (f (quote ("blocking" "brotli" "deflate" "gzip" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1mxny9mzj85aghlk7kldk2zja0nszpc3cvw3q7hh5gcczs29xg5h")))

(define-public crate-solana-notifier-1.10.41 (c (n "solana-notifier") (v "1.10.41") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (f (quote ("blocking" "brotli" "deflate" "gzip" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0dwxa50l6nf54kbvzip3rhj6z4q3hz5hjj8jwl303xv5nqcvl37y")))

(define-public crate-solana-notifier-1.13.3 (c (n "solana-notifier") (v "1.13.3") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (f (quote ("blocking" "brotli" "deflate" "gzip" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0x2x5cacb7q46hnavfwkdigag91nf28n2d60qnmzan167ic966g3")))

(define-public crate-solana-notifier-1.13.4 (c (n "solana-notifier") (v "1.13.4") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (f (quote ("blocking" "brotli" "deflate" "gzip" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1g4wn5m3jag1sdzdff4rvalsl71q9j5z26ydxjqx6jj4xy89b9sl")))

(define-public crate-solana-notifier-1.14.6 (c (n "solana-notifier") (v "1.14.6") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (f (quote ("blocking" "brotli" "deflate" "gzip" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0ibcwx45sbvsq85bgq85v9gjb7v6xlzvsasrh6r3vqqlpjdhyksy")))

(define-public crate-solana-notifier-1.14.7 (c (n "solana-notifier") (v "1.14.7") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (f (quote ("blocking" "brotli" "deflate" "gzip" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1zfkv7f3ajvgg1s4cf8kx097akb92s98mhmm51gysyzjp0v6nh02")))

(define-public crate-solana-notifier-1.13.5 (c (n "solana-notifier") (v "1.13.5") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (f (quote ("blocking" "brotli" "deflate" "gzip" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0clydwf31y5ww599kavdspnznx2ig368wdij90svg56h225vayi6")))

(define-public crate-solana-notifier-1.14.8 (c (n "solana-notifier") (v "1.14.8") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (f (quote ("blocking" "brotli" "deflate" "gzip" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1n15xa4qwbrk9kj595gv74w8arhbv8v0fn4cinx289r9na4f1fsf")))

(define-public crate-solana-notifier-1.14.9 (c (n "solana-notifier") (v "1.14.9") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (f (quote ("blocking" "brotli" "deflate" "gzip" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0daa6kgbwqgmqf0yqbfhkxlywd41044mjrjz81vs7jmbywjn7a3x")))

(define-public crate-solana-notifier-1.14.10 (c (n "solana-notifier") (v "1.14.10") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (f (quote ("blocking" "brotli" "deflate" "gzip" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1gmrgsrg1z47sxnb8yh2vwj11x1ln0f6bcj30z0g1j0f1kmc2al7")))

(define-public crate-solana-notifier-1.14.11 (c (n "solana-notifier") (v "1.14.11") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (f (quote ("blocking" "brotli" "deflate" "gzip" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1iy5j0lq88fwrbwaf5kfgf5990r2ixmhnd998janazfay75c71xf")))

(define-public crate-solana-notifier-1.14.12 (c (n "solana-notifier") (v "1.14.12") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (f (quote ("blocking" "brotli" "deflate" "gzip" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "094qc77i3kfms0bq1ima950y4dsgaf56ix6sadzsw76kq2ixq2h3")))

(define-public crate-solana-notifier-1.13.6 (c (n "solana-notifier") (v "1.13.6") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (f (quote ("blocking" "brotli" "deflate" "gzip" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0pbnwcmllr8wp3sp9cfdi17jaw9gnfrhmz476nfcs432zsrwsprx")))

(define-public crate-solana-notifier-1.14.13 (c (n "solana-notifier") (v "1.14.13") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (f (quote ("blocking" "brotli" "deflate" "gzip" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1h5lspccp289zwp9rimjkbapb4hq2vc8n1jix9nwr4lf13b8b18h")))

(define-public crate-solana-notifier-1.15.0 (c (n "solana-notifier") (v "1.15.0") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.12") (f (quote ("blocking" "brotli" "deflate" "gzip" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.15.0") (d #t) (k 0)))) (h "1mmy53y9cchh78ja3wx45yk49s14zpxkzwm4qlcf02i2vwvqna5q") (y #t)))

(define-public crate-solana-notifier-1.14.14 (c (n "solana-notifier") (v "1.14.14") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (f (quote ("blocking" "brotli" "deflate" "gzip" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0klg8d4af37zv9vyiamzlx1vvambfnvcg84lv0hfvharg0vb2fwm")))

(define-public crate-solana-notifier-1.14.15 (c (n "solana-notifier") (v "1.14.15") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (f (quote ("blocking" "brotli" "deflate" "gzip" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0nbn78ww56rkzb6rvwpg8wdjc4kvv39wfhzfqw1wa67fz6pydd5w")))

(define-public crate-solana-notifier-1.15.1 (c (n "solana-notifier") (v "1.15.1") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.12") (f (quote ("blocking" "brotli" "deflate" "gzip" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.15.1") (d #t) (k 0)))) (h "0qz7smy8j8nd770wdqs5568ggsqiywcq4dsxibaf6anqs9kmz52b") (y #t)))

(define-public crate-solana-notifier-1.15.2 (c (n "solana-notifier") (v "1.15.2") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.12") (f (quote ("blocking" "brotli" "deflate" "gzip" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.15.2") (d #t) (k 0)))) (h "10ybn19vvn8ai9j3ckk9r187yy7s4jdj0k29k44kyhg62pi6rj14") (y #t)))

(define-public crate-solana-notifier-1.14.16 (c (n "solana-notifier") (v "1.14.16") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (f (quote ("blocking" "brotli" "deflate" "gzip" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1jgn810y2mpkayafvcd9rn6r20snkrvvgablxpap7yl56f6ngi93")))

(define-public crate-solana-notifier-1.14.17 (c (n "solana-notifier") (v "1.14.17") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (f (quote ("blocking" "brotli" "deflate" "gzip" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1ciiflf1byg226rjhapn3dzazf8s5p1xc5dkb10mvnaiklm523px")))

(define-public crate-solana-notifier-1.13.7 (c (n "solana-notifier") (v "1.13.7") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (f (quote ("blocking" "brotli" "deflate" "gzip" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0yil9qsp137hdpjh8c4j7vmkzjqq55y9grpq2ig9zrjkdrglyg3b")))

(define-public crate-solana-notifier-1.14.18 (c (n "solana-notifier") (v "1.14.18") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (f (quote ("blocking" "brotli" "deflate" "gzip" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "02g3jld6164c6dnj4g7169n2nqpj341dfx4644w9ysas8ri167pp")))

(define-public crate-solana-notifier-1.16.0 (c (n "solana-notifier") (v "1.16.0") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.17") (f (quote ("blocking" "brotli" "deflate" "gzip" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.16.0") (d #t) (k 0)))) (h "1i4rda9dlmd0whlf025gxnpb552vpv7sjbl8byippz66bg2pxydg")))

(define-public crate-solana-notifier-1.16.1 (c (n "solana-notifier") (v "1.16.1") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.17") (f (quote ("blocking" "brotli" "deflate" "gzip" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.16.1") (d #t) (k 0)))) (h "15pxwkwxzvff5pnnbhydqj8i76f085b60fyzqcn9w8327qda5ckl")))

(define-public crate-solana-notifier-1.14.19 (c (n "solana-notifier") (v "1.14.19") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (f (quote ("blocking" "brotli" "deflate" "gzip" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0a9h9vkgpcv6cj4af0j91krxkr4hfrjfrikn71x5a8ga07y910b1")))

(define-public crate-solana-notifier-1.16.2 (c (n "solana-notifier") (v "1.16.2") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.17") (f (quote ("blocking" "brotli" "deflate" "gzip" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.16.2") (d #t) (k 0)))) (h "0fdz2xvqpy8i347jkwddwz250559gv4k56a3xyfw03xy58b646ch")))

(define-public crate-solana-notifier-1.16.3 (c (n "solana-notifier") (v "1.16.3") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.17") (f (quote ("blocking" "brotli" "deflate" "gzip" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.16.3") (d #t) (k 0)))) (h "06bkdv8hbqy8ch0kwmgpwy7lbwjfp3zl39m9hzwn65112fycnvjq")))

(define-public crate-solana-notifier-1.14.20 (c (n "solana-notifier") (v "1.14.20") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (f (quote ("blocking" "brotli" "deflate" "gzip" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0r5bw21jj818g24mq2dhq7kplp37jxghlhmprgsypnahq5hjzpdy")))

(define-public crate-solana-notifier-1.16.4 (c (n "solana-notifier") (v "1.16.4") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.17") (f (quote ("blocking" "brotli" "deflate" "gzip" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.16.4") (d #t) (k 0)))) (h "1kyg7jbn2c7jjmzdb7x5fa8cwinaxpiddh4ypjhcr6sb3k99n9j6")))

(define-public crate-solana-notifier-1.16.5 (c (n "solana-notifier") (v "1.16.5") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.17") (f (quote ("blocking" "brotli" "deflate" "gzip" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.16.5") (d #t) (k 0)))) (h "15b3868swkvaqha4vxkz1bdggb6ixdiqdpq7y66kflr1cw67rxyl")))

(define-public crate-solana-notifier-1.14.21 (c (n "solana-notifier") (v "1.14.21") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (f (quote ("blocking" "brotli" "deflate" "gzip" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "051qk2vnry36gks6p3jbdw95w6qy5zr42sghacrfqd4chwablsjb")))

(define-public crate-solana-notifier-1.14.22 (c (n "solana-notifier") (v "1.14.22") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (f (quote ("blocking" "brotli" "deflate" "gzip" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "05h4178xqdv9pqh9ifphvj6wjy3q12nnin4044hq0acmwrf3kaa7")))

(define-public crate-solana-notifier-1.16.6 (c (n "solana-notifier") (v "1.16.6") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.17") (f (quote ("blocking" "brotli" "deflate" "gzip" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.16.6") (d #t) (k 0)))) (h "0hc4whwa3jp7mpyrisis1lqfydhw821j5cbsc7ra5b3acb2ivxmg")))

(define-public crate-solana-notifier-1.16.7 (c (n "solana-notifier") (v "1.16.7") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.17") (f (quote ("blocking" "brotli" "deflate" "gzip" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.16.7") (d #t) (k 0)))) (h "1kivv9gzkmcb8svsp1716sncxbvfyxjh8iarsr3095i9b7wcldiw")))

(define-public crate-solana-notifier-1.14.23 (c (n "solana-notifier") (v "1.14.23") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (f (quote ("blocking" "brotli" "deflate" "gzip" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1xq2d416vbc2qm8mpg5xhdgpv5ip1rn0xy3k272mh58h6q7rrb3k")))

(define-public crate-solana-notifier-1.16.8 (c (n "solana-notifier") (v "1.16.8") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.17") (f (quote ("blocking" "brotli" "deflate" "gzip" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.16.8") (d #t) (k 0)))) (h "19iwlrbqpkqrqy3a61hydsxaqafy2l84qc3fnxd5y1fvp7nxj4lp")))

(define-public crate-solana-notifier-1.14.24 (c (n "solana-notifier") (v "1.14.24") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (f (quote ("blocking" "brotli" "deflate" "gzip" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0ji4z0196230lwpysf2kca8i70dp84yjgcnfx3awrvi6jwsf1fg6")))

(define-public crate-solana-notifier-1.16.9 (c (n "solana-notifier") (v "1.16.9") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.17") (f (quote ("blocking" "brotli" "deflate" "gzip" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.16.9") (d #t) (k 0)))) (h "0y1v3izjimjf0mp93iagv8h285gjx5ffm8qca7nll6gjqmf157mc")))

(define-public crate-solana-notifier-1.16.10 (c (n "solana-notifier") (v "1.16.10") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.17") (f (quote ("blocking" "brotli" "deflate" "gzip" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.16.10") (d #t) (k 0)))) (h "123lj5jglmf7fdfqk7ijsd53dpb7blk5xz1bbn6ndi1di0yjqwpw")))

(define-public crate-solana-notifier-1.14.25 (c (n "solana-notifier") (v "1.14.25") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "reqwest") (r "=0.11.11") (f (quote ("blocking" "brotli" "deflate" "gzip" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "07dcqsgj299zv0q3lqz693k1dfdb0g9h6j424m9vsxjxlf3jzsww")))

(define-public crate-solana-notifier-1.16.11 (c (n "solana-notifier") (v "1.16.11") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.17") (f (quote ("blocking" "brotli" "deflate" "gzip" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.16.11") (d #t) (k 0)))) (h "0qz5lxybwnkyg9r3awlwwz4r8r1b7k2qzxcmm0x26sd96la44p8r")))

(define-public crate-solana-notifier-1.14.26 (c (n "solana-notifier") (v "1.14.26") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "reqwest") (r "=0.11.11") (f (quote ("blocking" "brotli" "deflate" "gzip" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1mjpa9kjpp9bsgj7z3aadp9anvlcidr5qncm3c0xyzy909ihxaj3")))

(define-public crate-solana-notifier-1.16.12 (c (n "solana-notifier") (v "1.16.12") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.17") (f (quote ("blocking" "brotli" "deflate" "gzip" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.16.12") (d #t) (k 0)))) (h "0kjrcq2shc175w9m3s0hlnqjygd53n1jzacp8vdvxawk553svivm")))

(define-public crate-solana-notifier-1.14.27 (c (n "solana-notifier") (v "1.14.27") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "reqwest") (r "=0.11.11") (f (quote ("blocking" "brotli" "deflate" "gzip" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "15y01qm0332mdnzhvs0kvh38rbkcq6m0j2x92wmlwzh26slnbxba")))

(define-public crate-solana-notifier-1.16.13 (c (n "solana-notifier") (v "1.16.13") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.17") (f (quote ("blocking" "brotli" "deflate" "gzip" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.16.13") (d #t) (k 0)))) (h "0z33z3bzg4nnrjiyb561hgci1ma4mqjrd6by3w7gb3i69yz61r4m")))

(define-public crate-solana-notifier-1.16.14 (c (n "solana-notifier") (v "1.16.14") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.17") (f (quote ("blocking" "brotli" "deflate" "gzip" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.16.14") (d #t) (k 0)))) (h "0p7n3zrs2i3s1ff4zksqrk3bksri1pwpgvpxgwkyq0bj0mjcgkmp")))

(define-public crate-solana-notifier-1.14.28 (c (n "solana-notifier") (v "1.14.28") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "reqwest") (r "=0.11.11") (f (quote ("blocking" "brotli" "deflate" "gzip" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1rxmsmq1s4ix3l65mn7wkad00qsbggipc7506v4rwhjy6mggazpr")))

(define-public crate-solana-notifier-1.14.29 (c (n "solana-notifier") (v "1.14.29") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "reqwest") (r "=0.11.11") (f (quote ("blocking" "brotli" "deflate" "gzip" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "08mjfmzdnkb53irk0gsc9q7c1kwvy35myqiv7yvzil0h5rqvr4w4")))

(define-public crate-solana-notifier-1.16.15 (c (n "solana-notifier") (v "1.16.15") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.17") (f (quote ("blocking" "brotli" "deflate" "gzip" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.16.15") (d #t) (k 0)))) (h "1g7sfdx3xxg2ga2bmlvpza4zhszs8dwhy3ibwkbz2v2rn04ghn61")))

(define-public crate-solana-notifier-1.17.0 (c (n "solana-notifier") (v "1.17.0") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (f (quote ("blocking" "brotli" "deflate" "gzip" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.0") (d #t) (k 0)))) (h "0lxs3v09wqh87v7mi1dwkq7kvs99f76acbbjaiqii3h1jjj6j1p4")))

(define-public crate-solana-notifier-1.16.16 (c (n "solana-notifier") (v "1.16.16") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.17") (f (quote ("blocking" "brotli" "deflate" "gzip" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.16.16") (d #t) (k 0)))) (h "0l11kc3dp6k9qaaifwdd40b7k06g19z6jgvcgnsm3k0g5nxnr2qi")))

(define-public crate-solana-notifier-1.17.1 (c (n "solana-notifier") (v "1.17.1") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (f (quote ("blocking" "brotli" "deflate" "gzip" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.1") (d #t) (k 0)))) (h "0yhlr65visr6ldaa6ihd4lldpk1hm3frfsj15mvi0dm6xmvslyll")))

(define-public crate-solana-notifier-1.16.17 (c (n "solana-notifier") (v "1.16.17") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.17") (f (quote ("blocking" "brotli" "deflate" "gzip" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.16.17") (d #t) (k 0)))) (h "1hxf8x5n89xasbnncnx1glg8145q5ig5ss52nh69vryx519fwpfl")))

(define-public crate-solana-notifier-1.17.2 (c (n "solana-notifier") (v "1.17.2") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (f (quote ("blocking" "brotli" "deflate" "gzip" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.2") (d #t) (k 0)))) (h "190wy4dfa3vhd2syrvd73gbk75vp7cdzk3splhycg36ncbb75iwi")))

(define-public crate-solana-notifier-1.16.18 (c (n "solana-notifier") (v "1.16.18") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.17") (f (quote ("blocking" "brotli" "deflate" "gzip" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.16.18") (d #t) (k 0)))) (h "07xbvrl0km9wwp512ym5wq911cq1qpv3xsf2xqjdhrh1mb2kjb2v")))

(define-public crate-solana-notifier-1.17.3 (c (n "solana-notifier") (v "1.17.3") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (f (quote ("blocking" "brotli" "deflate" "gzip" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.3") (d #t) (k 0)))) (h "0nbbvyxp17hnbk7qv23l4j5bd4vw8rnjpgzvz4r61127fxwgwpr8")))

(define-public crate-solana-notifier-1.17.4 (c (n "solana-notifier") (v "1.17.4") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (f (quote ("blocking" "brotli" "deflate" "gzip" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.4") (d #t) (k 0)))) (h "1xrpyrqiilxqjhdsbqi0jqfsl0r4i2gixwrq6idivwi3n6pxbkvx")))

(define-public crate-solana-notifier-1.16.19 (c (n "solana-notifier") (v "1.16.19") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.17") (f (quote ("blocking" "brotli" "deflate" "gzip" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.16.19") (d #t) (k 0)))) (h "0nbnk9cdyikb02236rvscz6v7lm1q9wx58q76ifmnb0cknbkjkxv")))

(define-public crate-solana-notifier-1.17.5 (c (n "solana-notifier") (v "1.17.5") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (f (quote ("blocking" "brotli" "deflate" "gzip" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.5") (d #t) (k 0)))) (h "1bdqzqvngizp8x73lawzig9g39dgaj35l9mp3z8ia1vg6zy0b6q6")))

(define-public crate-solana-notifier-1.17.6 (c (n "solana-notifier") (v "1.17.6") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (f (quote ("blocking" "brotli" "deflate" "gzip" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.6") (d #t) (k 0)))) (h "1afsixxw7yss0cpj7k6c1ki9nai4if38ma5hj51iybsijrmpm85v")))

(define-public crate-solana-notifier-1.16.20 (c (n "solana-notifier") (v "1.16.20") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.17") (f (quote ("blocking" "brotli" "deflate" "gzip" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.16.20") (d #t) (k 0)))) (h "1rwbfx2932bivf88ahcp59iasc8a4qyrik0z4ngb91wkzp60frzz")))

(define-public crate-solana-notifier-1.17.7 (c (n "solana-notifier") (v "1.17.7") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (f (quote ("blocking" "brotli" "deflate" "gzip" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.7") (d #t) (k 0)))) (h "0fi6riwkpzk16fqrf5hdn4mxljb8jvlzr8bhp6xmv7ag6iq4kn5m")))

(define-public crate-solana-notifier-1.16.21 (c (n "solana-notifier") (v "1.16.21") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.17") (f (quote ("blocking" "brotli" "deflate" "gzip" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.16.21") (d #t) (k 0)))) (h "0rzwm2dvyqcr8pgdrwmiqhikjy7094kpjq7xdnfr1n6jy3iv37b9")))

(define-public crate-solana-notifier-1.17.8 (c (n "solana-notifier") (v "1.17.8") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (f (quote ("blocking" "brotli" "deflate" "gzip" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.8") (d #t) (k 0)))) (h "0sdcqqvpkvqsrdlbgdldf58ijzjr7i71v2kmb0ay057bhc2i013p")))

(define-public crate-solana-notifier-1.16.22 (c (n "solana-notifier") (v "1.16.22") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.17") (f (quote ("blocking" "brotli" "deflate" "gzip" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.16.22") (d #t) (k 0)))) (h "18dlhni8fp8l8zhz0yxivy6gy6sqadhyzxp49kwaivxaf70p2qxs")))

(define-public crate-solana-notifier-1.16.23 (c (n "solana-notifier") (v "1.16.23") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.17") (f (quote ("blocking" "brotli" "deflate" "gzip" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.16.23") (d #t) (k 0)))) (h "1cparzs181llhdmmsjdr6g1nygv6g37yp3haczcijai01zb4lm6h")))

(define-public crate-solana-notifier-1.17.9 (c (n "solana-notifier") (v "1.17.9") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (f (quote ("blocking" "brotli" "deflate" "gzip" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.9") (d #t) (k 0)))) (h "0bz6hzdfcf7fmra7pir9ixz6ky1xmklfw5a39sb396qkqkrwaqf1")))

(define-public crate-solana-notifier-1.17.10 (c (n "solana-notifier") (v "1.17.10") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (f (quote ("blocking" "brotli" "deflate" "gzip" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.10") (d #t) (k 0)))) (h "0mkz73p9qndws4sip600pxisi2qhz6h3d48qqkkch6h9ncac6v84")))

(define-public crate-solana-notifier-1.17.11 (c (n "solana-notifier") (v "1.17.11") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (f (quote ("blocking" "brotli" "deflate" "gzip" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.11") (d #t) (k 0)))) (h "1fvcxllss2sg6jdv611pv4v6kym80dx9c1460y0y0sgiv7fssr6x")))

(define-public crate-solana-notifier-1.17.12 (c (n "solana-notifier") (v "1.17.12") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (f (quote ("blocking" "brotli" "deflate" "gzip" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.12") (d #t) (k 0)))) (h "01b25svjds649lp8ja3bkh5z023sx1wxvp41r1pmmakp51jy69y9")))

(define-public crate-solana-notifier-1.16.24 (c (n "solana-notifier") (v "1.16.24") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.17") (f (quote ("blocking" "brotli" "deflate" "gzip" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.16.24") (d #t) (k 0)))) (h "07z8v3nrv1pj53hqyb6ivd8gqp7j49xlc1h1xs4dn95n2cnzjbi7")))

(define-public crate-solana-notifier-1.17.13 (c (n "solana-notifier") (v "1.17.13") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (f (quote ("blocking" "brotli" "deflate" "gzip" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.13") (d #t) (k 0)))) (h "16w7j30gdfs6g28kjghabisadk7877p8mz209lxd0km5a0m3z94v")))

(define-public crate-solana-notifier-1.17.14 (c (n "solana-notifier") (v "1.17.14") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (f (quote ("blocking" "brotli" "deflate" "gzip" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.14") (d #t) (k 0)))) (h "08sgprwg0d1nqvxhh9wmgjsrq1x96h9xg40wlspd0szwyivfkjhn")))

(define-public crate-solana-notifier-1.17.15 (c (n "solana-notifier") (v "1.17.15") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (f (quote ("blocking" "brotli" "deflate" "gzip" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.15") (d #t) (k 0)))) (h "1248h2g1idj34l3va113dfkbmbaiwglkg8pc11k5yhdp9hq2lp8c")))

(define-public crate-solana-notifier-1.16.25 (c (n "solana-notifier") (v "1.16.25") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.17") (f (quote ("blocking" "brotli" "deflate" "gzip" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.16.25") (d #t) (k 0)))) (h "0272r9ii1f7k6pm5v9g942n1xkv2hakn8rcvxpzara1ha3pdrgv8")))

(define-public crate-solana-notifier-1.16.27 (c (n "solana-notifier") (v "1.16.27") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.17") (f (quote ("blocking" "brotli" "deflate" "gzip" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.16.27") (d #t) (k 0)))) (h "19j0gpiq1w64852p31v8vw6wyp8q3yqak4kd0aq0zfydyqq6drbc")))

(define-public crate-solana-notifier-1.17.16 (c (n "solana-notifier") (v "1.17.16") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (f (quote ("blocking" "brotli" "deflate" "gzip" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.16") (d #t) (k 0)))) (h "1wmf590xjswia66il036p91irkmkpxlzsf59r2whbkicdmpwnbil")))

(define-public crate-solana-notifier-1.17.17 (c (n "solana-notifier") (v "1.17.17") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (f (quote ("blocking" "brotli" "deflate" "gzip" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.17") (d #t) (k 0)))) (h "0c4596v2w7mz7kvg16qixllqsbbf8sd3gmajaxrd5vflillxx7pq")))

(define-public crate-solana-notifier-1.17.18 (c (n "solana-notifier") (v "1.17.18") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (f (quote ("blocking" "brotli" "deflate" "gzip" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.18") (d #t) (k 0)))) (h "0bbycwb40370mg9wphc9swyq1b63ifqv5mrv7a76y1gpq4a2yqyy")))

(define-public crate-solana-notifier-1.18.0 (c (n "solana-notifier") (v "1.18.0") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.23") (f (quote ("blocking" "brotli" "deflate" "gzip" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.18.0") (d #t) (k 0)))) (h "0848jz5mh8s4z5yhbnndvmwjkblgk59mhc5ryckmfyz89g2vqb59")))

(define-public crate-solana-notifier-1.18.1 (c (n "solana-notifier") (v "1.18.1") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.23") (f (quote ("blocking" "brotli" "deflate" "gzip" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.18.1") (d #t) (k 0)))) (h "1c1m71w3mrzh0n9fv0yd78yshlxpi860zpwr720brdyzwj3lq19i")))

(define-public crate-solana-notifier-1.17.20 (c (n "solana-notifier") (v "1.17.20") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (f (quote ("blocking" "brotli" "deflate" "gzip" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.20") (d #t) (k 0)))) (h "0gdhfn68pn3a244938kg7m8dr0rmq882ygx6axgi35gfjvj9zvn6")))

(define-public crate-solana-notifier-1.17.22 (c (n "solana-notifier") (v "1.17.22") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (f (quote ("blocking" "brotli" "deflate" "gzip" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.22") (d #t) (k 0)))) (h "0kazgl8if97giaygsr4jya15pgjyp4hr8rk4l9siki7c706iwmm1")))

(define-public crate-solana-notifier-1.18.2 (c (n "solana-notifier") (v "1.18.2") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.23") (f (quote ("blocking" "brotli" "deflate" "gzip" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.18.2") (d #t) (k 0)))) (h "1zl1vni5vjmwi9h2b6057fgnk4sa5za9kms30di928f2m1wzzdcg")))

(define-public crate-solana-notifier-1.17.23 (c (n "solana-notifier") (v "1.17.23") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (f (quote ("blocking" "brotli" "deflate" "gzip" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.23") (d #t) (k 0)))) (h "1vj6792mrxan8g7qn3aws4z5jrmyd25z1xw0mj182lfyvw7drzl9")))

(define-public crate-solana-notifier-1.18.3 (c (n "solana-notifier") (v "1.18.3") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.23") (f (quote ("blocking" "brotli" "deflate" "gzip" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.18.3") (d #t) (k 0)))) (h "0hw9a5ql0zmr1hj92ifp6gbwspg6pg728q8cg2c93lwdw3layl8s")))

(define-public crate-solana-notifier-1.18.4 (c (n "solana-notifier") (v "1.18.4") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.23") (f (quote ("blocking" "brotli" "deflate" "gzip" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.18.4") (d #t) (k 0)))) (h "14shd405jq1jg14wj99za74rrvdy23b9zhpjc4g8rf07mvffy9jn")))

(define-public crate-solana-notifier-1.17.24 (c (n "solana-notifier") (v "1.17.24") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (f (quote ("blocking" "brotli" "deflate" "gzip" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.24") (d #t) (k 0)))) (h "0vghhlhl98vmmsxq3b2f4v5q953c53fbmkczvsbz9kydrh128shg")))

(define-public crate-solana-notifier-1.17.25 (c (n "solana-notifier") (v "1.17.25") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (f (quote ("blocking" "brotli" "deflate" "gzip" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.25") (d #t) (k 0)))) (h "0ys9bbhs62agvbyg810cxf09w72riff650lcwp47a40hsm9gwxl2")))

(define-public crate-solana-notifier-1.18.5 (c (n "solana-notifier") (v "1.18.5") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.23") (f (quote ("blocking" "brotli" "deflate" "gzip" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.18.5") (d #t) (k 0)))) (h "1fqd1pxwck7fs2nw3fi00k7y7vh82m4pyfwqsxkac7x57gjabgax")))

(define-public crate-solana-notifier-1.17.26 (c (n "solana-notifier") (v "1.17.26") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (f (quote ("blocking" "brotli" "deflate" "gzip" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.26") (d #t) (k 0)))) (h "1mg52qgjx3kpf2qrhf65hzkh52l27ksaag3f05r9n60ppf8xmr8q")))

(define-public crate-solana-notifier-1.18.6 (c (n "solana-notifier") (v "1.18.6") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.23") (f (quote ("blocking" "brotli" "deflate" "gzip" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.18.6") (d #t) (k 0)))) (h "16iyrkqzinn6bh2k4vwk50fdwjvvin5vlsvb20nq0yn0cbvdf267")))

(define-public crate-solana-notifier-1.17.27 (c (n "solana-notifier") (v "1.17.27") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (f (quote ("blocking" "brotli" "deflate" "gzip" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.27") (d #t) (k 0)))) (h "1imyx5kpzgywkk2ibqpl355x8afawmbqml2r1nm9cg7c6pdhq1dc")))

(define-public crate-solana-notifier-1.18.7 (c (n "solana-notifier") (v "1.18.7") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.23") (f (quote ("blocking" "brotli" "deflate" "gzip" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.18.7") (d #t) (k 0)))) (h "15137hk7z8pszhidjhicwpvpddwp7k49vacd6slg20iqgcy1knw6")))

(define-public crate-solana-notifier-1.18.8 (c (n "solana-notifier") (v "1.18.8") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.23") (f (quote ("blocking" "brotli" "deflate" "gzip" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.18.8") (d #t) (k 0)))) (h "18krrbzyalqk94ldmw9dngzl652i72af4f7y6did4g1rwqs5h4w3")))

(define-public crate-solana-notifier-1.17.28 (c (n "solana-notifier") (v "1.17.28") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (f (quote ("blocking" "brotli" "deflate" "gzip" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.28") (d #t) (k 0)))) (h "0b2scwyjh9r50vbvqz6177wjrj1c8g88fysi79p24i63jdm27pz9")))

(define-public crate-solana-notifier-1.18.9 (c (n "solana-notifier") (v "1.18.9") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.23") (f (quote ("blocking" "brotli" "deflate" "gzip" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.18.9") (d #t) (k 0)))) (h "015xps8x6npagqbl3klq2rlwbbl1jk7n85012rdgxhvz4xd9lmwa")))

(define-public crate-solana-notifier-1.17.29 (c (n "solana-notifier") (v "1.17.29") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (f (quote ("blocking" "brotli" "deflate" "gzip" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.29") (d #t) (k 0)))) (h "1df6mxcfsb7kx6zq8h7lvi9k0rgn5ps0fz538g3p07wwy2k2ccxc")))

(define-public crate-solana-notifier-1.17.30 (c (n "solana-notifier") (v "1.17.30") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (f (quote ("blocking" "brotli" "deflate" "gzip" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.30") (d #t) (k 0)))) (h "1vm15997j3c3p3xjsp19qav8ikq2nwpyjij566xxj74ihvapn14s")))

(define-public crate-solana-notifier-1.18.10 (c (n "solana-notifier") (v "1.18.10") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.23") (f (quote ("blocking" "brotli" "deflate" "gzip" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.18.10") (d #t) (k 0)))) (h "1299zsgi249x0rs3kw12ils79llzajh847jbwhj6n59zy8q44n9a")))

(define-public crate-solana-notifier-1.18.11 (c (n "solana-notifier") (v "1.18.11") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.23") (f (quote ("blocking" "brotli" "deflate" "gzip" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.18.11") (d #t) (k 0)))) (h "1kqxnlqi6c9qdhy2pia8pnnl0cafr9y38d6shacnlq5frh4m2c4c")))

(define-public crate-solana-notifier-1.17.31 (c (n "solana-notifier") (v "1.17.31") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (f (quote ("blocking" "brotli" "deflate" "gzip" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.31") (d #t) (k 0)))) (h "0ysfh3bd11jmg9yr5r1qpb00prz1hknxnsahh8i6nvq768y8w1v7")))

(define-public crate-solana-notifier-1.18.12 (c (n "solana-notifier") (v "1.18.12") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.23") (f (quote ("blocking" "brotli" "deflate" "gzip" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.18.12") (d #t) (k 0)))) (h "1wf88rnn68ba02mg1mbhi26rzal4nq19hg80ns0nfshjvz1s9wwl")))

(define-public crate-solana-notifier-1.17.32 (c (n "solana-notifier") (v "1.17.32") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (f (quote ("blocking" "brotli" "deflate" "gzip" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.32") (d #t) (k 0)))) (h "0hnldzpx1cay1a3jb81r676gkclvdxlaq0gv26l2kkmjan4r3zr9")))

(define-public crate-solana-notifier-1.17.33 (c (n "solana-notifier") (v "1.17.33") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (f (quote ("blocking" "brotli" "deflate" "gzip" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.33") (d #t) (k 0)))) (h "0cr1wkcwrzbwqs616a2zqkxffwjdx7ayf3vi3h1y3i9c1jav1rbz")))

(define-public crate-solana-notifier-1.18.13 (c (n "solana-notifier") (v "1.18.13") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.23") (f (quote ("blocking" "brotli" "deflate" "gzip" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.18.13") (d #t) (k 0)))) (h "098yg852jgzdc319bpa8lfx8pak10r8fh6r6097gxvpd01rbghbm")))

(define-public crate-solana-notifier-1.18.14 (c (n "solana-notifier") (v "1.18.14") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.23") (f (quote ("blocking" "brotli" "deflate" "gzip" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.18.14") (d #t) (k 0)))) (h "0fdipk7qwalis8dpajbbawl9755q9q35s6mvxi8qr4dmmqz0wkl5")))

(define-public crate-solana-notifier-1.17.34 (c (n "solana-notifier") (v "1.17.34") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (f (quote ("blocking" "brotli" "deflate" "gzip" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.34") (d #t) (k 0)))) (h "0xllczb5mb10r8rpi1zb991dgbjknk1fhxcm0nr1flmn8yg6hhf5")))

