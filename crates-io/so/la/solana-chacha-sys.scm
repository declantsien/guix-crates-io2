(define-module (crates-io so la solana-chacha-sys) #:use-module (crates-io))

(define-public crate-solana-chacha-sys-0.16.0 (c (n "solana-chacha-sys") (v "0.16.0") (d (list (d (n "cc") (r "^1.0.37") (d #t) (k 1)))) (h "1n7pssghkmayg85vp5gwgyvyp27ipa1hrp2h3dsif888jr9rbxcq")))

(define-public crate-solana-chacha-sys-0.16.1 (c (n "solana-chacha-sys") (v "0.16.1") (d (list (d (n "cc") (r "^1.0.37") (d #t) (k 1)))) (h "0ka0dq08zdfnxdvk1jjhggknb44iwj4gbsa1jki1q173iy846fzw")))

(define-public crate-solana-chacha-sys-0.16.2 (c (n "solana-chacha-sys") (v "0.16.2") (d (list (d (n "cc") (r "^1.0.37") (d #t) (k 1)))) (h "076ffzlrrca8lvr6qqk4cn325dd8kxkb0x2n1g26h1dpqnq9yyb2")))

(define-public crate-solana-chacha-sys-0.16.3 (c (n "solana-chacha-sys") (v "0.16.3") (d (list (d (n "cc") (r "^1.0.37") (d #t) (k 1)))) (h "0jrzkmzy2d37bsjivrcahnkvfvb8yy9ya24jzb8qb2kx4lpqcxh7")))

(define-public crate-solana-chacha-sys-0.16.4 (c (n "solana-chacha-sys") (v "0.16.4") (d (list (d (n "cc") (r "^1.0.37") (d #t) (k 1)))) (h "0lx1zq8radd715knrm4hk6zmlr38r5fshamnhv47phk62nb3mx60")))

(define-public crate-solana-chacha-sys-0.16.5 (c (n "solana-chacha-sys") (v "0.16.5") (d (list (d (n "cc") (r "^1.0.37") (d #t) (k 1)))) (h "0bqf8x9lws4naqbad778ylwrifbm158flpp05jbaa3xh03nmly1x")))

(define-public crate-solana-chacha-sys-0.16.6 (c (n "solana-chacha-sys") (v "0.16.6") (d (list (d (n "cc") (r "^1.0.37") (d #t) (k 1)))) (h "1lfa6qz8w10jpgcmks7170wcr4vdr9pszbi4ibgmkk4xpj3lb35m")))

(define-public crate-solana-chacha-sys-0.17.0 (c (n "solana-chacha-sys") (v "0.17.0") (d (list (d (n "cc") (r "^1.0.38") (d #t) (k 1)))) (h "0080pacy4m6yhi8js2pabwk3s3mfcyyviapzqvxg1g5g63r606fl")))

(define-public crate-solana-chacha-sys-0.17.1 (c (n "solana-chacha-sys") (v "0.17.1") (d (list (d (n "cc") (r "^1.0.38") (d #t) (k 1)))) (h "0zd6qwldhzkk36pfb5mikxhar37vkx6srxa8hk3ddgiln406mb51")))

(define-public crate-solana-chacha-sys-0.18.0-pre0 (c (n "solana-chacha-sys") (v "0.18.0-pre0") (d (list (d (n "cc") (r "^1.0.38") (d #t) (k 1)))) (h "0saq498ymrac6hvij0pdy9l6ba6vxkv2rc1g73zcrzis05hraia0")))

(define-public crate-solana-chacha-sys-0.18.0-pre1 (c (n "solana-chacha-sys") (v "0.18.0-pre1") (d (list (d (n "cc") (r "^1.0.40") (d #t) (k 1)))) (h "0f34miya1ziv79jxzx0y2wjm77xib4cndwry3g285adh9f5h9c8a")))

(define-public crate-solana-chacha-sys-0.17.2 (c (n "solana-chacha-sys") (v "0.17.2") (d (list (d (n "cc") (r "^1.0.38") (d #t) (k 1)))) (h "00lp561rbq5hsj5x18kh1dxy2dzb0v2zr36jlmcj8crxvxvvsz59")))

(define-public crate-solana-chacha-sys-0.18.0-pre2 (c (n "solana-chacha-sys") (v "0.18.0-pre2") (d (list (d (n "cc") (r "^1.0.40") (d #t) (k 1)))) (h "0x733l5m2ky83br0qrqw8rbi1qw2cg4hxcxb0brixxmpgfn7wa8j")))

(define-public crate-solana-chacha-sys-0.18.0 (c (n "solana-chacha-sys") (v "0.18.0") (d (list (d (n "cc") (r "^1.0.40") (d #t) (k 1)))) (h "0vra7k1iqvkgyvjpc8skhijg6f1pviwgllxcr5j15waqpvrqbg0c")))

(define-public crate-solana-chacha-sys-0.18.1 (c (n "solana-chacha-sys") (v "0.18.1") (d (list (d (n "cc") (r "^1.0.40") (d #t) (k 1)))) (h "0n7vy02pnqsayh4x2j3yqb6vji7jb8iwlg978bgj7mra3shbqriz")))

(define-public crate-solana-chacha-sys-0.19.1 (c (n "solana-chacha-sys") (v "0.19.1") (d (list (d (n "cc") (r "^1.0.45") (d #t) (k 1)))) (h "0fpfvpwrgvz9h3im697cy2457gvlb92q7v55l0a3im7vmdqqck41")))

(define-public crate-solana-chacha-sys-0.20.1 (c (n "solana-chacha-sys") (v "0.20.1") (d (list (d (n "cc") (r "^1.0.46") (d #t) (k 1)))) (h "0s65dl0khasydqj5pmi9aymrvrw6samq29i49igwvs74napzf08b")))

(define-public crate-solana-chacha-sys-0.20.2 (c (n "solana-chacha-sys") (v "0.20.2") (d (list (d (n "cc") (r "^1.0.46") (d #t) (k 1)))) (h "01gbvhbvgy4xizxmig932fynx6hcvhywpfaldppz4msg2905q47b")))

(define-public crate-solana-chacha-sys-0.20.3 (c (n "solana-chacha-sys") (v "0.20.3") (d (list (d (n "cc") (r "^1.0.46") (d #t) (k 1)))) (h "0n5v7q9jbh0pasc8csp1ysicajv7wk782fmasaf1qy41iqr5f0jh")))

(define-public crate-solana-chacha-sys-0.20.4 (c (n "solana-chacha-sys") (v "0.20.4") (d (list (d (n "cc") (r "^1.0.46") (d #t) (k 1)))) (h "0fmw5ydh8qj2vas8j3ywwwjanc4wn9nh0ab5fmfy52lqipxmpm9q")))

(define-public crate-solana-chacha-sys-0.20.5 (c (n "solana-chacha-sys") (v "0.20.5") (d (list (d (n "cc") (r "^1.0.46") (d #t) (k 1)))) (h "0lcfg2jxnrjvjll00k1pn446n32y6y1q63prqyxc9wwlvrxa5hw6")))

(define-public crate-solana-chacha-sys-0.21.0 (c (n "solana-chacha-sys") (v "0.21.0") (d (list (d (n "cc") (r "^1.0.47") (d #t) (k 1)))) (h "00dxmmmkghzzd8d1p74v6v0c5z3df6sjxsh1cdmli1jx94rxp2in")))

(define-public crate-solana-chacha-sys-0.21.1 (c (n "solana-chacha-sys") (v "0.21.1") (d (list (d (n "cc") (r "^1.0.47") (d #t) (k 1)))) (h "1x1gm7jb6imc0p08bwcibzism47ydvqxzfp2b9h3w9n5xxh04yg4")))

(define-public crate-solana-chacha-sys-0.21.2 (c (n "solana-chacha-sys") (v "0.21.2") (d (list (d (n "cc") (r "^1.0.47") (d #t) (k 1)))) (h "1qb1d1wcy1l4smgbnxc1z47bl15zvdjs676r9393k3d57my6jh6f")))

(define-public crate-solana-chacha-sys-0.21.3 (c (n "solana-chacha-sys") (v "0.21.3") (d (list (d (n "cc") (r "^1.0.47") (d #t) (k 1)))) (h "1kbqbbj0s6mdpn8c4h5fj58wa946lx5ppkrbanhk55p5valr4p4f")))

(define-public crate-solana-chacha-sys-0.21.4 (c (n "solana-chacha-sys") (v "0.21.4") (d (list (d (n "cc") (r "^1.0.47") (d #t) (k 1)))) (h "0zrxs2nxhl8kl8fnkkf56lgfbn38vvr5xwf4kw6ljpddmf21rl3s")))

(define-public crate-solana-chacha-sys-0.21.5 (c (n "solana-chacha-sys") (v "0.21.5") (d (list (d (n "cc") (r "^1.0.47") (d #t) (k 1)))) (h "00fx70r0vm42nw6rxif2f1anq8bpak3ir3c0gwqhifx66qahxq3z")))

(define-public crate-solana-chacha-sys-0.22.0 (c (n "solana-chacha-sys") (v "0.22.0") (d (list (d (n "cc") (r "^1.0.48") (d #t) (k 1)))) (h "1anq1sf5gy29hq583yikpfz3ijlgxd5rjmr6inabrvif6m165xb5")))

(define-public crate-solana-chacha-sys-0.21.6 (c (n "solana-chacha-sys") (v "0.21.6") (d (list (d (n "cc") (r "^1.0.47") (d #t) (k 1)))) (h "1f8kl5ysb70v34bqlgf94pq0rdkl83sh526g65s1pmk9214maa86")))

(define-public crate-solana-chacha-sys-0.22.1 (c (n "solana-chacha-sys") (v "0.22.1") (d (list (d (n "cc") (r "^1.0.48") (d #t) (k 1)))) (h "1gij5mzaikpizvvdnq8jbw9x5dn06g5wp85yq1ll93a9l99awpvm")))

(define-public crate-solana-chacha-sys-0.22.2 (c (n "solana-chacha-sys") (v "0.22.2") (d (list (d (n "cc") (r "^1.0.48") (d #t) (k 1)))) (h "1v3h9pxannss0lrjc0425x7y0mq4b6ch61qwcra6y5imrsjhbjvk")))

(define-public crate-solana-chacha-sys-0.21.7 (c (n "solana-chacha-sys") (v "0.21.7") (d (list (d (n "cc") (r "^1.0.47") (d #t) (k 1)))) (h "04dpv20j3lam4f4cfmqv7pmy692348w1f9698iqflaharjzwy560")))

(define-public crate-solana-chacha-sys-0.22.3 (c (n "solana-chacha-sys") (v "0.22.3") (d (list (d (n "cc") (r "^1.0.48") (d #t) (k 1)))) (h "0k6b53p87yhpmgrv26qpq4bc1x4k8i99k3hiqqgg9f5nmam950gl")))

(define-public crate-solana-chacha-sys-0.22.4 (c (n "solana-chacha-sys") (v "0.22.4") (d (list (d (n "cc") (r "^1.0.48") (d #t) (k 1)))) (h "1p9gbz2msb45f0izvgv9dq47h2q8qygilirib0d349nhf0613ic8")))

(define-public crate-solana-chacha-sys-0.23.0 (c (n "solana-chacha-sys") (v "0.23.0") (d (list (d (n "cc") (r "^1.0.49") (d #t) (k 1)))) (h "14nf2g9wyx8sshrljsj7k5vdsalhlq32bkixhbyjw2jgyhx2iqw7")))

(define-public crate-solana-chacha-sys-0.22.5 (c (n "solana-chacha-sys") (v "0.22.5") (d (list (d (n "cc") (r "^1.0.48") (d #t) (k 1)))) (h "1hiy7d66bjqsrqwjxm791m2mr1gl3sqzdmdhzn4m4h3gr8b04n3a")))

(define-public crate-solana-chacha-sys-0.22.6 (c (n "solana-chacha-sys") (v "0.22.6") (d (list (d (n "cc") (r "^1.0.48") (d #t) (k 1)))) (h "188kq4ffly7p19m7aqdbvg0hd9k8dyrniahv3crx0hfl10cyfz4w")))

(define-public crate-solana-chacha-sys-0.23.1 (c (n "solana-chacha-sys") (v "0.23.1") (d (list (d (n "cc") (r "^1.0.49") (d #t) (k 1)))) (h "0wly97i77nkfpams1h48j8w097nv0wz5sz1712w3s8hwysg5inky")))

(define-public crate-solana-chacha-sys-0.23.2 (c (n "solana-chacha-sys") (v "0.23.2") (d (list (d (n "cc") (r "^1.0.49") (d #t) (k 1)))) (h "0hb75g46wshw1ph78bn31r9vfrky8ap12nms6m619qps86516bxp")))

(define-public crate-solana-chacha-sys-0.22.7 (c (n "solana-chacha-sys") (v "0.22.7") (d (list (d (n "cc") (r "^1.0.48") (d #t) (k 1)))) (h "1fyp7izkw453gy4q5x80dvslcryiy8rqkx8p2pqdsnjzp9gfr2pc")))

(define-public crate-solana-chacha-sys-0.23.3 (c (n "solana-chacha-sys") (v "0.23.3") (d (list (d (n "cc") (r "^1.0.49") (d #t) (k 1)))) (h "1yhdrx08gb89xlcg4mr2whdks3zvbvkgn05rxgbcadprwzml2cl2")))

(define-public crate-solana-chacha-sys-0.23.4 (c (n "solana-chacha-sys") (v "0.23.4") (d (list (d (n "cc") (r "^1.0.49") (d #t) (k 1)))) (h "1h8nh5rks2jp0nbrk14snn970dwd91rc8i09gr7j04hrsnkwlr6v")))

(define-public crate-solana-chacha-sys-0.22.8 (c (n "solana-chacha-sys") (v "0.22.8") (d (list (d (n "cc") (r "^1.0.48") (d #t) (k 1)))) (h "0dfdbpnwl24qwz1sfaqhklpm2azixqjns2i6wfnh8s882qrikzp1")))

(define-public crate-solana-chacha-sys-0.23.5 (c (n "solana-chacha-sys") (v "0.23.5") (d (list (d (n "cc") (r "^1.0.49") (d #t) (k 1)))) (h "0jpgknq9m98jj63rkbw01favp1q8nl917pp6p9c3mv82hs7rd7qn")))

(define-public crate-solana-chacha-sys-0.22.9 (c (n "solana-chacha-sys") (v "0.22.9") (d (list (d (n "cc") (r "^1.0.48") (d #t) (k 1)))) (h "010dfjqmaf14s1xlrb94mz0mvy3rqf7bh3admni5q0cvdg8ly8b0")))

(define-public crate-solana-chacha-sys-0.23.6 (c (n "solana-chacha-sys") (v "0.23.6") (d (list (d (n "cc") (r "^1.0.49") (d #t) (k 1)))) (h "0aci936rgapj6xfmf8f97hifl2ncfialv56mrv98xd76gb910jpg")))

(define-public crate-solana-chacha-sys-1.0.0 (c (n "solana-chacha-sys") (v "1.0.0") (d (list (d (n "cc") (r "^1.0.49") (d #t) (k 1)))) (h "0ri4l9jwi17kwwyqb9bkgfn0mjvcjvi23zlgv991nn1wzpc9wllp")))

(define-public crate-solana-chacha-sys-0.23.7 (c (n "solana-chacha-sys") (v "0.23.7") (d (list (d (n "cc") (r "^1.0.49") (d #t) (k 1)))) (h "1wvwn6v3dapvwivi4g8sp66npvfjd2lq3pisx6jaq9d4n5l5qzck")))

(define-public crate-solana-chacha-sys-0.23.8 (c (n "solana-chacha-sys") (v "0.23.8") (d (list (d (n "cc") (r "^1.0.49") (d #t) (k 1)))) (h "1m9vc96faih91n34i6w24zl9yx08xbxhygl9qkpbj87zawjqx3km")))

(define-public crate-solana-chacha-sys-1.0.1 (c (n "solana-chacha-sys") (v "1.0.1") (d (list (d (n "cc") (r "^1.0.49") (d #t) (k 1)))) (h "0r5ivzz2x4iav2rz2xl647gzb6yy9kmpq7p9ia0q9p4kh1zzwx42")))

(define-public crate-solana-chacha-sys-1.0.2 (c (n "solana-chacha-sys") (v "1.0.2") (d (list (d (n "cc") (r "^1.0.49") (d #t) (k 1)))) (h "0wfppqmcw31zvxgw5nkyfhlqvhpn9f6bwiz0ayr753vjdq2ac875")))

(define-public crate-solana-chacha-sys-1.0.3 (c (n "solana-chacha-sys") (v "1.0.3") (d (list (d (n "cc") (r "^1.0.49") (d #t) (k 1)))) (h "0ksyx98xvgbj5gz5b12ny561jlfgd9jihryhjyn56niwaxda3q2j")))

(define-public crate-solana-chacha-sys-1.0.4 (c (n "solana-chacha-sys") (v "1.0.4") (d (list (d (n "cc") (r "^1.0.49") (d #t) (k 1)))) (h "09iys65p0ps00awkg0ymf6s76h5zhk3ifxnvjrp6sbnj63adwxvi")))

(define-public crate-solana-chacha-sys-1.0.5 (c (n "solana-chacha-sys") (v "1.0.5") (d (list (d (n "cc") (r "^1.0.49") (d #t) (k 1)))) (h "16b16wlf0ifc73r2k5yxzh2qi5gl3nm742r37rw7jqrr0w7q08li")))

(define-public crate-solana-chacha-sys-1.0.6 (c (n "solana-chacha-sys") (v "1.0.6") (d (list (d (n "cc") (r "^1.0.49") (d #t) (k 1)))) (h "0mbxy1vky9bnh772gwsy14wwa6x67plfqizc77sbd0n321dfavja")))

(define-public crate-solana-chacha-sys-1.0.7 (c (n "solana-chacha-sys") (v "1.0.7") (d (list (d (n "cc") (r "^1.0.49") (d #t) (k 1)))) (h "1ycgnrcj2yr4h7gfyf1a666r8pgj864jkr9xa1rrn712nb4gdx7n")))

(define-public crate-solana-chacha-sys-1.0.8 (c (n "solana-chacha-sys") (v "1.0.8") (d (list (d (n "cc") (r "^1.0.49") (d #t) (k 1)))) (h "0ccs9wrli8kyha5ls215w858z4y5z4v05llwh27mdr3mxppd7ix0")))

(define-public crate-solana-chacha-sys-1.0.9 (c (n "solana-chacha-sys") (v "1.0.9") (d (list (d (n "cc") (r "^1.0.49") (d #t) (k 1)))) (h "1f0h8qfq2xwk2xhsy27jdl5vaia2va9dfwjnpfw13rv1j24wkzpw")))

(define-public crate-solana-chacha-sys-1.0.10 (c (n "solana-chacha-sys") (v "1.0.10") (d (list (d (n "cc") (r "^1.0.49") (d #t) (k 1)))) (h "1dnla28vwrip318w8fsac6p4ncb5iwq8fsfqrs4jh0l9g8zn14x4")))

(define-public crate-solana-chacha-sys-1.0.11 (c (n "solana-chacha-sys") (v "1.0.11") (d (list (d (n "cc") (r "^1.0.49") (d #t) (k 1)))) (h "1xxrwz3jb9fpfwzc6ba8p840l78xpvf4fl64wqmqrl5mssh16bim")))

(define-public crate-solana-chacha-sys-1.0.12 (c (n "solana-chacha-sys") (v "1.0.12") (d (list (d (n "cc") (r "^1.0.49") (d #t) (k 1)))) (h "0afddp5ibk7g1mkfpw6hk1816hxir3gly2605pm2wjbjc2kvc1f6")))

(define-public crate-solana-chacha-sys-1.1.0 (c (n "solana-chacha-sys") (v "1.1.0") (d (list (d (n "cc") (r "^1.0.49") (d #t) (k 1)))) (h "0w8w317q7xw5b9c53r193b56gzq68aib3irsxd7a2nxw7a3d7mdi")))

(define-public crate-solana-chacha-sys-1.1.1 (c (n "solana-chacha-sys") (v "1.1.1") (d (list (d (n "cc") (r "^1.0.49") (d #t) (k 1)))) (h "1nzwp9rjyxm86xrkyhbqlpj4y8gzn43zb5qfg0ky0fbiv87lla62")))

(define-public crate-solana-chacha-sys-1.0.13 (c (n "solana-chacha-sys") (v "1.0.13") (d (list (d (n "cc") (r "^1.0.49") (d #t) (k 1)))) (h "0271790ifjwmrbixxd3xmqn4m93pjmw3dc4dpraf3vg7jj48zhws")))

(define-public crate-solana-chacha-sys-1.1.2 (c (n "solana-chacha-sys") (v "1.1.2") (d (list (d (n "cc") (r "^1.0.49") (d #t) (k 1)))) (h "19c45hn87s8xygqmcn3jv6jkhw6y0zfr4mpr0ad9h2rzvdsbcsc1")))

(define-public crate-solana-chacha-sys-1.0.14 (c (n "solana-chacha-sys") (v "1.0.14") (d (list (d (n "cc") (r "^1.0.49") (d #t) (k 1)))) (h "1m0bchahm6w7wy26rs9vf21v35bgwd4bsi5pdf6zf1j4pzzvqn8z")))

(define-public crate-solana-chacha-sys-1.0.15 (c (n "solana-chacha-sys") (v "1.0.15") (d (list (d (n "cc") (r "^1.0.49") (d #t) (k 1)))) (h "091m91ych5lfi3f27j463gj0r62r2l4m2w8ax53dnmdzdcjpmz70")))

(define-public crate-solana-chacha-sys-1.0.16 (c (n "solana-chacha-sys") (v "1.0.16") (d (list (d (n "cc") (r "^1.0.49") (d #t) (k 1)))) (h "0kmx57aibm6khiy8hhfi0lmsyhbkx48h22ridsra9r4viz1cnbdy")))

(define-public crate-solana-chacha-sys-1.1.3 (c (n "solana-chacha-sys") (v "1.1.3") (d (list (d (n "cc") (r "^1.0.49") (d #t) (k 1)))) (h "09mdqpzqjlas3r2b4d94ccwbqc8h4480wkdqy7pza632vix62y6g")))

(define-public crate-solana-chacha-sys-1.0.17 (c (n "solana-chacha-sys") (v "1.0.17") (d (list (d (n "cc") (r "^1.0.49") (d #t) (k 1)))) (h "0g1cv7wfmrmlfmid5jg7klm82v7isqcwbnsgizg5lc9lynjxhd2w")))

(define-public crate-solana-chacha-sys-1.0.18 (c (n "solana-chacha-sys") (v "1.0.18") (d (list (d (n "cc") (r "^1.0.49") (d #t) (k 1)))) (h "1zj9ij1h35lampbnbfdjh9c2mcm2klzrb0zxxqzcfq1f18b6m5js")))

(define-public crate-solana-chacha-sys-1.1.4 (c (n "solana-chacha-sys") (v "1.1.4") (d (list (d (n "cc") (r "^1.0.49") (d #t) (k 1)))) (h "12lhfgflayan9jnz5xnawxqbmzx7gxjmx0bipaid92z93yyjgnaz")))

(define-public crate-solana-chacha-sys-1.1.5 (c (n "solana-chacha-sys") (v "1.1.5") (d (list (d (n "cc") (r "^1.0.49") (d #t) (k 1)))) (h "1vgmd1r8frj629c5wkj16z7zdmph2f92s2w5riycmnyxgqvgwc58")))

(define-public crate-solana-chacha-sys-1.1.6 (c (n "solana-chacha-sys") (v "1.1.6") (d (list (d (n "cc") (r "^1.0.49") (d #t) (k 1)))) (h "03wxphggw49863fxwivvfm11rl7jf1ff9vlplgf2y5av9qli12c9")))

(define-public crate-solana-chacha-sys-1.0.19 (c (n "solana-chacha-sys") (v "1.0.19") (d (list (d (n "cc") (r "^1.0.49") (d #t) (k 1)))) (h "15x8cafa4bv7q72rf5224l73ji3k3i3ss5diq05qv51v2v30yb3d")))

(define-public crate-solana-chacha-sys-1.1.7 (c (n "solana-chacha-sys") (v "1.1.7") (d (list (d (n "cc") (r "^1.0.49") (d #t) (k 1)))) (h "1b78rjcyxfp05690pff86wmbbj3zkn1ccynaffzj6mqv6dch6cl1")))

(define-public crate-solana-chacha-sys-1.0.20 (c (n "solana-chacha-sys") (v "1.0.20") (d (list (d (n "cc") (r "^1.0.49") (d #t) (k 1)))) (h "1x02fa504vl16vjgqal75dz7461vk8mlnh9103r7jw4gp14362n5")))

(define-public crate-solana-chacha-sys-1.0.21 (c (n "solana-chacha-sys") (v "1.0.21") (d (list (d (n "cc") (r "^1.0.49") (d #t) (k 1)))) (h "1jb692m9lshn6741h46pklskqsk3zvqw6472ba3sfhxn35mxvwdv")))

(define-public crate-solana-chacha-sys-1.1.8 (c (n "solana-chacha-sys") (v "1.1.8") (d (list (d (n "cc") (r "^1.0.49") (d #t) (k 1)))) (h "1n54a9qn6dyxp7sw6lklqxxai8f9b6dlsd86xkaijdm93mi5zjwv")))

(define-public crate-solana-chacha-sys-1.1.9 (c (n "solana-chacha-sys") (v "1.1.9") (d (list (d (n "cc") (r "^1.0.49") (d #t) (k 1)))) (h "0cphvy522ml0n1n0hm0gzxxczlp7r5dhrf590mnp2m87g4miqwyy")))

(define-public crate-solana-chacha-sys-1.1.10 (c (n "solana-chacha-sys") (v "1.1.10") (d (list (d (n "cc") (r "^1.0.49") (d #t) (k 1)))) (h "09f3wh2r9kk8jc8v936jz6g7arxn15kd93f4441im3saclgvnyn6")))

(define-public crate-solana-chacha-sys-1.0.22 (c (n "solana-chacha-sys") (v "1.0.22") (d (list (d (n "cc") (r "^1.0.49") (d #t) (k 1)))) (h "1pha7gkn7p90rin7j5mfafcj9mjk9xlfxzmw28vc4zwhbc0iqham")))

(define-public crate-solana-chacha-sys-1.1.11 (c (n "solana-chacha-sys") (v "1.1.11") (d (list (d (n "cc") (r "^1.0.49") (d #t) (k 1)))) (h "1wh36a9bmmvd97v1p3n602d272p3dpys6hcsrix9figvc8391fzr")))

(define-public crate-solana-chacha-sys-1.1.12 (c (n "solana-chacha-sys") (v "1.1.12") (d (list (d (n "cc") (r "^1.0.49") (d #t) (k 1)))) (h "1a9gp0yd8qkrn8v75flwzb8911i9hxqr20p4601nvpz7c1hy9slx")))

(define-public crate-solana-chacha-sys-1.1.13 (c (n "solana-chacha-sys") (v "1.1.13") (d (list (d (n "cc") (r "^1.0.49") (d #t) (k 1)))) (h "1yk5sxzbi6lgnhc6xwklg16zrvq01h01shqsw5bb89hmy43jhpgq")))

(define-public crate-solana-chacha-sys-1.0.23 (c (n "solana-chacha-sys") (v "1.0.23") (d (list (d (n "cc") (r "^1.0.49") (d #t) (k 1)))) (h "104fpzxihd9wz3kac7ff57haczap2z71wpj2r6nv43yk7vqpap22")))

(define-public crate-solana-chacha-sys-1.0.24 (c (n "solana-chacha-sys") (v "1.0.24") (d (list (d (n "cc") (r "^1.0.49") (d #t) (k 1)))) (h "0q57vacwmjf9vds7xpdgrvs7hv1ssypifjsr95khwx2xlsp9xczq")))

(define-public crate-solana-chacha-sys-1.1.15 (c (n "solana-chacha-sys") (v "1.1.15") (d (list (d (n "cc") (r "^1.0.49") (d #t) (k 1)))) (h "1fdr1ca2252p5z0y48gdkzlyg6q3fdy6x0yvrppgxyjax0rfmv3s")))

(define-public crate-solana-chacha-sys-1.1.17 (c (n "solana-chacha-sys") (v "1.1.17") (d (list (d (n "cc") (r "^1.0.49") (d #t) (k 1)))) (h "0nq20nrk2i5ln5ar8r1zlq4fx70yis36v65y0cgvs22yklp3j59b")))

(define-public crate-solana-chacha-sys-1.1.18 (c (n "solana-chacha-sys") (v "1.1.18") (d (list (d (n "cc") (r "^1.0.49") (d #t) (k 1)))) (h "1af78fhvnisz1kdba10s31g942zil469hfcsijy6q7c4s0pkmrsd")))

(define-public crate-solana-chacha-sys-1.1.19 (c (n "solana-chacha-sys") (v "1.1.19") (d (list (d (n "cc") (r "^1.0.49") (d #t) (k 1)))) (h "0y6v4smr1yc0f31idkpk6cbr61czqx3fih40xicg47mzsmkw9lv5")))

(define-public crate-solana-chacha-sys-1.1.20 (c (n "solana-chacha-sys") (v "1.1.20") (d (list (d (n "cc") (r "^1.0.49") (d #t) (k 1)))) (h "15cpzgg1zjbbssvyzcgbbndrdr5cclpz8sicms9piq3yr572vlm7")))

(define-public crate-solana-chacha-sys-1.1.23 (c (n "solana-chacha-sys") (v "1.1.23") (d (list (d (n "cc") (r "^1.0.49") (d #t) (k 1)))) (h "0nisggdkrkadwgf4iy6k71q14m68vrkyap54gbnaigclpaxh691v")))

