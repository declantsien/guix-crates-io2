(define-module (crates-io so la solana_libra_nibble) #:use-module (crates-io))

(define-public crate-solana_libra_nibble-0.0.1-sol3 (c (n "solana_libra_nibble") (v "0.0.1-sol3") (d (list (d (n "proptest") (r "^0.9.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.101") (f (quote ("derive"))) (d #t) (k 0)))) (h "1qby6y96dz60dkjkpm7bsfbyha1pbchavbid0qql3cdk34w3kvww")))

(define-public crate-solana_libra_nibble-0.0.1-sol4 (c (n "solana_libra_nibble") (v "0.0.1-sol4") (d (list (d (n "proptest") (r "^0.9.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.101") (f (quote ("derive"))) (d #t) (k 0)))) (h "0m56l3jxr8vqpx38kpnxyv24m5v6jx9i03qqpn3j5fvqip33g6ab")))

(define-public crate-solana_libra_nibble-0.0.1-sol5 (c (n "solana_libra_nibble") (v "0.0.1-sol5") (d (list (d (n "proptest") (r "^0.9.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.101") (f (quote ("derive"))) (d #t) (k 0)))) (h "1j60m6rys6qx8pydm61g2mxaih1dgk835s9vl8cac2550s50px7q")))

