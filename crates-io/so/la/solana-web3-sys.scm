(define-module (crates-io so la solana-web3-sys) #:use-module (crates-io))

(define-public crate-solana-web3-sys-0.0.0 (c (n "solana-web3-sys") (v "0.0.0") (h "1pngkmil63g0c04s0b7b3pbrsj2ira9zfpyymjrx87a2i3kywdzn")))

(define-public crate-solana-web3-sys-0.1.0 (c (n "solana-web3-sys") (v "0.1.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "js-sys") (r "^0.3.61") (d #t) (k 0)) (d (n "solana-program") (r "^1.15.1") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.15.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.84") (d #t) (k 0)) (d (n "wasm-bindgen-futures") (r "^0.4.34") (d #t) (k 0)) (d (n "workflow-log") (r "^0.3.12") (d #t) (k 0)))) (h "0r8kn9pxmbijz76kfiy351jwi9ynf18v07nyfvalikcph6i75pwc") (f (quote (("init") ("default" "init"))))))

(define-public crate-solana-web3-sys-0.1.1 (c (n "solana-web3-sys") (v "0.1.1") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "js-sys") (r "^0.3.61") (d #t) (k 0)) (d (n "solana-program") (r "^1.15.1") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.15.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.84") (d #t) (k 0)) (d (n "wasm-bindgen-futures") (r "^0.4.34") (d #t) (k 0)) (d (n "workflow-log") (r "^0.3.12") (d #t) (k 0)))) (h "02qx5xk04s4905kvbz11qz9qsfkvcc57yf9zfrvqswyfrfqvfqrg") (f (quote (("init") ("default" "init"))))))

