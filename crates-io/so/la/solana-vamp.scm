(define-module (crates-io so la solana-vamp) #:use-module (crates-io))

(define-public crate-solana-vamp-0.1.0 (c (n "solana-vamp") (v "0.1.0") (d (list (d (n "solana-client") (r "=1.15.2") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.15.2") (d #t) (k 0)) (d (n "zerocopy") (r "=0.3.0") (d #t) (k 0)))) (h "07gb389ikjca6h7f1d6dxw52x8rmcid2ivmxgszci9w1vs1zz39j")))

(define-public crate-solana-vamp-0.1.1 (c (n "solana-vamp") (v "0.1.1") (d (list (d (n "solana-client") (r "=1.15.2") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.15.2") (d #t) (k 0)) (d (n "zerocopy") (r "=0.3.0") (d #t) (k 0)))) (h "0k4z5743kz7rrgrlzwzbk0gpyr654prc8nnb3bw3l684ni7nf3m2")))

