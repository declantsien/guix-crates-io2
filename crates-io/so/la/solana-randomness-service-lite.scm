(define-module (crates-io so la solana-randomness-service-lite) #:use-module (crates-io))

(define-public crate-solana-randomness-service-lite-1.0.0 (c (n "solana-randomness-service-lite") (v "1.0.0") (d (list (d (n "borsh") (r "^0.10") (d #t) (k 0)) (d (n "solana-program") (r ">=1.14") (d #t) (k 0)))) (h "1ym712f0dn4lng1v9wivvjznim9j8dnk0qyv06h0613nr51hza9f") (f (quote (("default"))))))

(define-public crate-solana-randomness-service-lite-1.0.1 (c (n "solana-randomness-service-lite") (v "1.0.1") (d (list (d (n "borsh") (r "^0.10") (d #t) (k 0)) (d (n "solana-program") (r ">=1.9.13") (d #t) (k 0)))) (h "0b386aqjgwv85cyahy732ma7pvmdpr6cci5lk9rapg0gq62fq82v") (f (quote (("default"))))))

