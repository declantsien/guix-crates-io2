(define-module (crates-io so la solana-account-balance) #:use-module (crates-io))

(define-public crate-solana-account-balance-0.1.0 (c (n "solana-account-balance") (v "0.1.0") (d (list (d (n "solana-client") (r "^1.10.8") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.10.8") (d #t) (k 0)))) (h "0hwl3kch26crbdbyahf5s429bw4rjnps913lxbkb5rpfw4qvzwkh")))

