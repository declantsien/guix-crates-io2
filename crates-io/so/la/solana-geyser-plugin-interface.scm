(define-module (crates-io so la solana-geyser-plugin-interface) #:use-module (crates-io))

(define-public crate-solana-geyser-plugin-interface-1.9.13 (c (n "solana-geyser-plugin-interface") (v "1.9.13") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.9.13") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.9.13") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "09kqgjxsks8zj2igz8hjp0xgd5zlmsv0d7c9550ly96arzyai4qq")))

(define-public crate-solana-geyser-plugin-interface-1.10.3 (c (n "solana-geyser-plugin-interface") (v "1.10.3") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.10.3") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.10.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0d913q0pbgf1am82dchh2pgjbbxzsbw3gi0mxx5854qdvjk0zjpx")))

(define-public crate-solana-geyser-plugin-interface-1.9.14 (c (n "solana-geyser-plugin-interface") (v "1.9.14") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.9.14") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.9.14") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1x7l8mdb1vgdc7gj52100zy7drpph9d175dqncyigwgii7k3zwm0")))

(define-public crate-solana-geyser-plugin-interface-1.10.4 (c (n "solana-geyser-plugin-interface") (v "1.10.4") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.10.4") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.10.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1iw75fvlra1b60rvkpwdg0v0j8yr25b05mbv6y6kdi4ykqhmsvrs")))

(define-public crate-solana-geyser-plugin-interface-1.10.5 (c (n "solana-geyser-plugin-interface") (v "1.10.5") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.10.5") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.10.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1a1mq0wzsc3278vrj1ykzbig1n6bg6fiix823b8hby5b79znpxvq")))

(define-public crate-solana-geyser-plugin-interface-1.10.6 (c (n "solana-geyser-plugin-interface") (v "1.10.6") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.10.6") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.10.6") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1ckppm8rxxb8gz4whr43wwv2n6yzr8p0v3xwbph97gp9g1vlkala")))

(define-public crate-solana-geyser-plugin-interface-1.9.15 (c (n "solana-geyser-plugin-interface") (v "1.9.15") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.9.15") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.9.15") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1ki7j0rayqwi41xq31pj94d820fvy5wkgkw5v8z5xfk5l8hyfayi")))

(define-public crate-solana-geyser-plugin-interface-1.10.7 (c (n "solana-geyser-plugin-interface") (v "1.10.7") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.10.7") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.10.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1h39kj7s7h3vixpg9al1hfjmz9ljfqrnw6bjvnxrqfzschzwj47j")))

(define-public crate-solana-geyser-plugin-interface-1.10.8 (c (n "solana-geyser-plugin-interface") (v "1.10.8") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.10.8") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.10.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1c7qvr6zm006kwl71i4kbx0d09kkxv14rxvhk868r2xpcbsl468q")))

(define-public crate-solana-geyser-plugin-interface-1.9.16 (c (n "solana-geyser-plugin-interface") (v "1.9.16") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.9.16") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.9.16") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0yy6rkg84s6sf7bp5ambbi2vrpf32xrcpj7iannkwcwb6fhac458")))

(define-public crate-solana-geyser-plugin-interface-1.9.17 (c (n "solana-geyser-plugin-interface") (v "1.9.17") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.9.17") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.9.17") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1wj7gjiqmpff193ny7k07idb26hbh3zcjz43f3bbc1zsvn4js50a")))

(define-public crate-solana-geyser-plugin-interface-1.10.9 (c (n "solana-geyser-plugin-interface") (v "1.10.9") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.10.9") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.10.9") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "13j5fhzg1bl9dhi9s6smqx7pyykqdcirg4vcc7s5qizyyd4vcm4y")))

(define-public crate-solana-geyser-plugin-interface-1.9.18 (c (n "solana-geyser-plugin-interface") (v "1.9.18") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.9.18") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.9.18") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1m61xpf5x3xf032k7iqcwv0z6skq2njzzn5yavw9qhwiq653vj4n")))

(define-public crate-solana-geyser-plugin-interface-1.10.10 (c (n "solana-geyser-plugin-interface") (v "1.10.10") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.10.10") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.10.10") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0sy26xvzl97r44w8adbwybmh9zgkb4904yxmx3dc4ja5xh939d6q")))

(define-public crate-solana-geyser-plugin-interface-1.10.11 (c (n "solana-geyser-plugin-interface") (v "1.10.11") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.10.11") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.10.11") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1id3rxk2fv9qf07p0ni2pcsb3xd5rmps9szhn7qhv22lzgglm90q")))

(define-public crate-solana-geyser-plugin-interface-1.9.19 (c (n "solana-geyser-plugin-interface") (v "1.9.19") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.9.19") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.9.19") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0njhb80n7w39c7920rkl6ycla7c4g9k19f8icgl7dp4vl45hyidp")))

(define-public crate-solana-geyser-plugin-interface-1.10.12 (c (n "solana-geyser-plugin-interface") (v "1.10.12") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.10.12") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.10.12") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0k8dppd93h46x12vz8wgs12y3g20154rglilwcm814q50j2z9r8p")))

(define-public crate-solana-geyser-plugin-interface-1.9.20 (c (n "solana-geyser-plugin-interface") (v "1.9.20") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.9.20") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.9.20") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0yfja3qpwc9vdjrbr8gyalx3zbqwb25bh97qhcykr3vjbnjbcs3c")))

(define-public crate-solana-geyser-plugin-interface-1.10.13 (c (n "solana-geyser-plugin-interface") (v "1.10.13") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.10.13") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.10.13") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0n5j04f9hvh8f1pqrrikwjzx63wajwb9czb6rv6bvljv52f2y9vr")))

(define-public crate-solana-geyser-plugin-interface-1.9.21 (c (n "solana-geyser-plugin-interface") (v "1.9.21") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.9.21") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.9.21") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1gncm36qbdvqr4iqvwd2zx9vvnlyfdysi9lanm75n9ap1jnjiqhd")))

(define-public crate-solana-geyser-plugin-interface-1.10.14 (c (n "solana-geyser-plugin-interface") (v "1.10.14") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.10.14") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.10.14") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "177yj9ahsnwz8ak5dig1wr6sswjmjm8pilg7lqyv9z1a7882bb5l")))

(define-public crate-solana-geyser-plugin-interface-1.9.22 (c (n "solana-geyser-plugin-interface") (v "1.9.22") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.9.22") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.9.22") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "09661i2mb71qq0lcngbk49vmwdmzg4yb3kzkgz7cnvyqpjfh1xs2")))

(define-public crate-solana-geyser-plugin-interface-1.10.15 (c (n "solana-geyser-plugin-interface") (v "1.10.15") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.10.15") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.10.15") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1ss5hr8hpyib1gnz9my4l13nddnwhwaykxxaka1hq1fjaqaa6652")))

(define-public crate-solana-geyser-plugin-interface-1.10.16 (c (n "solana-geyser-plugin-interface") (v "1.10.16") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.10.16") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.10.16") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1zyk08ryljl2ia74lgmrrgraak40symq2c5zah9i8r80pgfm3g70")))

(define-public crate-solana-geyser-plugin-interface-1.10.17 (c (n "solana-geyser-plugin-interface") (v "1.10.17") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.10.17") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.10.17") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0ibklnsf3v6ra6mfnf63i9jc2qy3bnmlyki28z1dn7py1bh7jvv0")))

(define-public crate-solana-geyser-plugin-interface-1.10.18 (c (n "solana-geyser-plugin-interface") (v "1.10.18") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.10.18") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.10.18") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "05s59mngma9vry07mn908fb5h7qrz72v0ghcqnkjfja2l1ldjffh")))

(define-public crate-solana-geyser-plugin-interface-1.9.23 (c (n "solana-geyser-plugin-interface") (v "1.9.23") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.9.23") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.9.23") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0v4axlk8jspk7bpp5n77l9psd7lrvxh406k9m9m2nsrxycmdflbb")))

(define-public crate-solana-geyser-plugin-interface-1.10.19 (c (n "solana-geyser-plugin-interface") (v "1.10.19") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.10.19") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.10.19") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0vnn27dffjc4r60v1pgq6yq0dabr8zxn4imjd5f8p36lzd39y5cg")))

(define-public crate-solana-geyser-plugin-interface-1.9.24 (c (n "solana-geyser-plugin-interface") (v "1.9.24") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.9.24") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.9.24") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0p0gak9422kibs7mcgbzfd8m4q3fm3l297l5pwn9an9n1kixjq4k")))

(define-public crate-solana-geyser-plugin-interface-1.9.25 (c (n "solana-geyser-plugin-interface") (v "1.9.25") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.9.25") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.9.25") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1v076gqszlzsiy4v0xzhvmkmfaham9f297l1gp6870zrj54ic8m6")))

(define-public crate-solana-geyser-plugin-interface-1.10.20 (c (n "solana-geyser-plugin-interface") (v "1.10.20") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.10.20") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.10.20") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0mspr4vfb416rlj1q9rhfr85812b1hqx21zcyilfv2vs2ggvb5d2")))

(define-public crate-solana-geyser-plugin-interface-1.9.26 (c (n "solana-geyser-plugin-interface") (v "1.9.26") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.9.26") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.9.26") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0kih2lk9rfjk53ys4shd7aylb66mw9mdf9p960qsw3bg9vyjis36")))

(define-public crate-solana-geyser-plugin-interface-1.10.21 (c (n "solana-geyser-plugin-interface") (v "1.10.21") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.10.21") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.10.21") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1cg51r3ns97f9ckpa3bqyc0ilypvyg6k0l0hdrzbqymzhxnr5nc4")))

(define-public crate-solana-geyser-plugin-interface-1.9.28 (c (n "solana-geyser-plugin-interface") (v "1.9.28") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.9.28") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.9.28") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "03iw08pcki8bl6r5azkjbjhs3pj3lxviwh98r4nxb490ri3w8nb7")))

(define-public crate-solana-geyser-plugin-interface-1.10.23 (c (n "solana-geyser-plugin-interface") (v "1.10.23") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.10.23") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.10.23") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1gs5vs93ly39pb43ff3nls0v7pw9vcqdaq4zzahscsm4zb3yg9r4")))

(define-public crate-solana-geyser-plugin-interface-1.10.24 (c (n "solana-geyser-plugin-interface") (v "1.10.24") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.10.24") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.10.24") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1ca7k9k5w49dfazdv4qx6ajsack6i36skn48kck97ilc09k4aglp")))

(define-public crate-solana-geyser-plugin-interface-1.10.25 (c (n "solana-geyser-plugin-interface") (v "1.10.25") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.10.25") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.10.25") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0sh6cf2nh4i0dxsinq5igw2zx6yggspcd3ldmj45yybxb8vslm4a")))

(define-public crate-solana-geyser-plugin-interface-1.9.29 (c (n "solana-geyser-plugin-interface") (v "1.9.29") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.9.29") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.9.29") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1blqff58zsgskqg1b6802xb15hl1ljr1ynaf0rn0a9k1ydwazzzz")))

(define-public crate-solana-geyser-plugin-interface-1.10.26 (c (n "solana-geyser-plugin-interface") (v "1.10.26") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.10.26") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.10.26") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1dg7gf188hh93kc8x4smai85irihgnqag2qq7701frq7p4g4n5dc")))

(define-public crate-solana-geyser-plugin-interface-1.11.0 (c (n "solana-geyser-plugin-interface") (v "1.11.0") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.11.0") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.11.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "075x8vjn3fy0y1yf9wfkf0dhxcp7gx0y1clbj1pypybrc6ds7fr2")))

(define-public crate-solana-geyser-plugin-interface-1.10.27 (c (n "solana-geyser-plugin-interface") (v "1.10.27") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.10.27") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.10.27") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "15mm4z8rx74b5cpn2q1g4xzlg73li4m3z6gxz55iacbas4wms2yy")))

(define-public crate-solana-geyser-plugin-interface-1.11.1 (c (n "solana-geyser-plugin-interface") (v "1.11.1") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.11.1") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.11.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0bkn26w8lnv2swdkx4zv5fxs4gx9wr083771ia5jq74d2dwy698v")))

(define-public crate-solana-geyser-plugin-interface-1.10.28 (c (n "solana-geyser-plugin-interface") (v "1.10.28") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.10.28") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.10.28") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0dwwaqa53nlxw01mqyvb3nhz7vdvrgfh6h78b4s7bphrq4zryvz5")))

(define-public crate-solana-geyser-plugin-interface-1.10.29 (c (n "solana-geyser-plugin-interface") (v "1.10.29") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.10.29") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.10.29") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1li3v6s9kcldndrbkqzl3n95v5zli2ylikgyfmga6vcspfcb72r6")))

(define-public crate-solana-geyser-plugin-interface-1.10.30 (c (n "solana-geyser-plugin-interface") (v "1.10.30") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.10.30") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.10.30") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "05z7q585lmxwcbifhfnr840nlvfifvgwjv6r1x4v96xfwzjfqacx")))

(define-public crate-solana-geyser-plugin-interface-1.11.2 (c (n "solana-geyser-plugin-interface") (v "1.11.2") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.11.2") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.11.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "12wmiqh458cgzqm83qsakshaippqdrzz60awjvj5440gxcb2xjj9")))

(define-public crate-solana-geyser-plugin-interface-1.10.31 (c (n "solana-geyser-plugin-interface") (v "1.10.31") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.10.31") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.10.31") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1q1j035zjibzw1xmyszjdbaf6kz9n4gy6zvv7i98li6gpq4ifsqn")))

(define-public crate-solana-geyser-plugin-interface-1.11.3 (c (n "solana-geyser-plugin-interface") (v "1.11.3") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.11.3") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.11.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "175l35j1df0l2yh4ddmb9hpgc22m5h090650qmh6m4h4vsqki58q")))

(define-public crate-solana-geyser-plugin-interface-1.10.32 (c (n "solana-geyser-plugin-interface") (v "1.10.32") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.10.32") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.10.32") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0yhj84jbs4wasgd6pakv2y227xg478g03sp78fgjdnfd4ffi1wal")))

(define-public crate-solana-geyser-plugin-interface-1.11.4 (c (n "solana-geyser-plugin-interface") (v "1.11.4") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.11.4") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.11.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1d2kqcqfy76x7wpg2558x21mh5h3j1fcn275mzl14smxxm06c8gq")))

(define-public crate-solana-geyser-plugin-interface-1.10.33 (c (n "solana-geyser-plugin-interface") (v "1.10.33") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.10.33") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.10.33") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0cxywy1vn38r351l3vxj4yp4dq9i3x5dyvjz2lm9h7gm793y08x4")))

(define-public crate-solana-geyser-plugin-interface-1.10.34 (c (n "solana-geyser-plugin-interface") (v "1.10.34") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.10.34") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.10.34") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0pgyafz3kslqf2l0pd5xhrxfzpafl1pyvk4br7p6s58750va7jmf")))

(define-public crate-solana-geyser-plugin-interface-1.11.5 (c (n "solana-geyser-plugin-interface") (v "1.11.5") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.11.5") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.11.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "03liwdhd4lxn4dcrfkq8qw46w2c4p1m6jjzplmb3794wyd0mfvkn")))

(define-public crate-solana-geyser-plugin-interface-1.10.35 (c (n "solana-geyser-plugin-interface") (v "1.10.35") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.10.35") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.10.35") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "07nwghnarxihpkmfyk1dvdxf2rib8k6b60z63ipw2rp2z2f3k66c")))

(define-public crate-solana-geyser-plugin-interface-1.11.6 (c (n "solana-geyser-plugin-interface") (v "1.11.6") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.11.6") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.11.6") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "025lvhybipk638mr8vnh7mrjn9z6f7v3m8yjlzq288aa0820syzq")))

(define-public crate-solana-geyser-plugin-interface-1.11.7 (c (n "solana-geyser-plugin-interface") (v "1.11.7") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.11.7") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.11.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "08q5i6g5792rjgcn051yr01w57gym41j3b3mw3klnndxa2x2c94d")))

(define-public crate-solana-geyser-plugin-interface-1.11.8 (c (n "solana-geyser-plugin-interface") (v "1.11.8") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.11.8") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.11.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "076096fgljac59xpifvh1rxb36ybwkvf72rgc8fy5ary4jb69bh6")))

(define-public crate-solana-geyser-plugin-interface-1.11.10 (c (n "solana-geyser-plugin-interface") (v "1.11.10") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.11.10") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.11.10") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1dghiyasvbm9ms6z8iapgzg9sq9brkwdjykqzsmy494f1zvw98hn")))

(define-public crate-solana-geyser-plugin-interface-1.10.38 (c (n "solana-geyser-plugin-interface") (v "1.10.38") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.10.38") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.10.38") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1zn2dwnkwfqc8sf7i4kclrmqaa0a1sw9y5sblxqb03hr86d3cb4a")))

(define-public crate-solana-geyser-plugin-interface-1.13.0 (c (n "solana-geyser-plugin-interface") (v "1.13.0") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.13.0") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.13.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0mfikx9swp1grki1kvwf2rwhz63mcs0kspm3n1hnfwc6hzzrwb51")))

(define-public crate-solana-geyser-plugin-interface-1.14.0 (c (n "solana-geyser-plugin-interface") (v "1.14.0") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.14.0") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.14.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1kxgdaj2d13bl4hyi2hf93gqkxishm61jfnrzh9sdd6z557mfh4f")))

(define-public crate-solana-geyser-plugin-interface-1.14.1 (c (n "solana-geyser-plugin-interface") (v "1.14.1") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.14.1") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.14.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0vcwvy23p0l88dqqgmkjdkdp0fbw67069q86nma53wlhp5wwq7yv")))

(define-public crate-solana-geyser-plugin-interface-1.10.39 (c (n "solana-geyser-plugin-interface") (v "1.10.39") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.10.39") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.10.39") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1c5jy7kfnsq9nkdb7bdxr12v0lp818iwwprabi2w3d8il3m3p3rc")))

(define-public crate-solana-geyser-plugin-interface-1.14.2 (c (n "solana-geyser-plugin-interface") (v "1.14.2") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.14.2") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.14.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0g1acw31rhwkcrdfwfvk7bgs7g9vw628dl2xxlm9f742afpm1idr")))

(define-public crate-solana-geyser-plugin-interface-1.13.1 (c (n "solana-geyser-plugin-interface") (v "1.13.1") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.13.1") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.13.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1izn6k21v2kn1zj0q9kwdg4fb4ynn0bap2xdk9xykffihvf9al0z")))

(define-public crate-solana-geyser-plugin-interface-1.14.3 (c (n "solana-geyser-plugin-interface") (v "1.14.3") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.14.3") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.14.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0l7q0fy00zx7hma7yg4vb20xcgfsfzqryygmmmj1aaahzmz4cyyj")))

(define-public crate-solana-geyser-plugin-interface-1.10.40 (c (n "solana-geyser-plugin-interface") (v "1.10.40") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.10.40") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.10.40") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "02syawnahfpkp2b8pdnb7qawnlq12asjyqdqb0slrm1fcgabv9s5")))

(define-public crate-solana-geyser-plugin-interface-1.14.4 (c (n "solana-geyser-plugin-interface") (v "1.14.4") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.14.4") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.14.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1ki4fxxan78f02agmpbf1dy7q7dpm1322939l9d93dncb68h70rm")))

(define-public crate-solana-geyser-plugin-interface-1.13.2 (c (n "solana-geyser-plugin-interface") (v "1.13.2") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.13.2") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.13.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "07ndzjfg1cr2ams2kx34k0q8kmhcj1zkwamqwf96pdf3rfxj59y2")))

(define-public crate-solana-geyser-plugin-interface-1.14.5 (c (n "solana-geyser-plugin-interface") (v "1.14.5") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.14.5") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.14.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0jn80vgp4wpaqklq3kq9gn1cipxi8xapm6gd88irsnwkhgi8sxsm")))

(define-public crate-solana-geyser-plugin-interface-1.13.3 (c (n "solana-geyser-plugin-interface") (v "1.13.3") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.13.3") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.13.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0scarwm3fdkc31whrm9lk3jhar99h695xiflqz20jy93v9xzzbqc")))

(define-public crate-solana-geyser-plugin-interface-1.10.41 (c (n "solana-geyser-plugin-interface") (v "1.10.41") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.10.41") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.10.41") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1spjhfnb186yyqqfjy3i7sc11g99qfmkzgsl33xrcclpwg3k4y74")))

(define-public crate-solana-geyser-plugin-interface-1.13.4 (c (n "solana-geyser-plugin-interface") (v "1.13.4") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.13.4") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.13.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0iij9r5xbqczrj9lbja8sj25agd16zls1sv9w3vgqbv63kycd5kl")))

(define-public crate-solana-geyser-plugin-interface-1.14.6 (c (n "solana-geyser-plugin-interface") (v "1.14.6") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.14.6") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.14.6") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1ard3cx4m69gpg1qr1khd37g808gmm0qxhrx2sn9fddgmbhciraf")))

(define-public crate-solana-geyser-plugin-interface-1.14.7 (c (n "solana-geyser-plugin-interface") (v "1.14.7") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.14.7") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.14.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "16zz5v3pvr7lk2ndf122nhx521qhdlkr7c004mfsys2lsy63qrrj")))

(define-public crate-solana-geyser-plugin-interface-1.13.5 (c (n "solana-geyser-plugin-interface") (v "1.13.5") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.13.5") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.13.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "10awb6sb4fa8njhm1mmcbk057yl40zfbpksah3x9naf86c5jbi33")))

(define-public crate-solana-geyser-plugin-interface-1.14.8 (c (n "solana-geyser-plugin-interface") (v "1.14.8") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.14.8") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.14.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1bkqx7xd4nc6bwr5zzdzcbg9l3ks9kcdz8qk9jzh88brpj9zicvx")))

(define-public crate-solana-geyser-plugin-interface-1.14.9 (c (n "solana-geyser-plugin-interface") (v "1.14.9") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.14.9") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.14.9") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0hmc1s914kwn2jlbysv88qmvng1fs341f923963hhgy17sspxpfv")))

(define-public crate-solana-geyser-plugin-interface-1.14.10 (c (n "solana-geyser-plugin-interface") (v "1.14.10") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.14.10") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.14.10") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "11z78nfdhlgpbblp0bnz69aajw9vdhliarqrj0if4dirgavfr2j3")))

(define-public crate-solana-geyser-plugin-interface-1.14.11 (c (n "solana-geyser-plugin-interface") (v "1.14.11") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.14.11") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.14.11") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "13qvajlpnb0qpb3zjxaafi6j9d10rrvh2v4ai6dh9lr1g5a21ib3")))

(define-public crate-solana-geyser-plugin-interface-1.14.12 (c (n "solana-geyser-plugin-interface") (v "1.14.12") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.14.12") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.14.12") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0hr6acvf8sqycclw5bv4dp4c5ybvm1arqlvmmz4v5s04zvl54ps9")))

(define-public crate-solana-geyser-plugin-interface-1.13.6 (c (n "solana-geyser-plugin-interface") (v "1.13.6") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.13.6") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.13.6") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1c1v8sl4ylrdc9jqppcisjamsfpb244mc3mdmky73czpkwl46p31")))

(define-public crate-solana-geyser-plugin-interface-1.14.13 (c (n "solana-geyser-plugin-interface") (v "1.14.13") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.14.13") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.14.13") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0lb7khf4gkdg5ysbv8vwjbi1vgfn723y7ym79shdlk83c9mj3xwr")))

(define-public crate-solana-geyser-plugin-interface-1.15.0 (c (n "solana-geyser-plugin-interface") (v "1.15.0") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.15.0") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.15.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1lszmn7y1k0pp7fgkd32hvxc7m1d0vwh321dmwig2z3q9a67b7fk") (y #t)))

(define-public crate-solana-geyser-plugin-interface-1.14.14 (c (n "solana-geyser-plugin-interface") (v "1.14.14") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.14.14") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.14.14") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1rgazpiqvjksnf85kmv1p33q75lcrz1v6w8gpk4vb9ks9jwzahmr")))

(define-public crate-solana-geyser-plugin-interface-1.14.15 (c (n "solana-geyser-plugin-interface") (v "1.14.15") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.14.15") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.14.15") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1vnjrq1jjsr58ll1kxm0sd9nfrk2a3lqlqlmi5br5abfsa5ip72x")))

(define-public crate-solana-geyser-plugin-interface-1.15.1 (c (n "solana-geyser-plugin-interface") (v "1.15.1") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.15.1") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.15.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1ajmh7kwlpds1xx59bpqck8rjdgf8w7zc48bwzyafib4jh0k80jj") (y #t)))

(define-public crate-solana-geyser-plugin-interface-1.15.2 (c (n "solana-geyser-plugin-interface") (v "1.15.2") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.15.2") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.15.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "01vgn522fippfsj9cpbdlx9rkjp2qwqxb51rk0wykhd86aicffvi") (y #t)))

(define-public crate-solana-geyser-plugin-interface-1.14.16 (c (n "solana-geyser-plugin-interface") (v "1.14.16") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.14.16") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.14.16") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0aki1vha9cphis2xdyjnv4zhpnfflc41c164bbnc70q3i6aw6rls")))

(define-public crate-solana-geyser-plugin-interface-1.14.17 (c (n "solana-geyser-plugin-interface") (v "1.14.17") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.14.17") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.14.17") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0x9pf8nbi62ia0y675x8q21cmmjpva8m5hby0f2nxc57s5q8acx6")))

(define-public crate-solana-geyser-plugin-interface-1.13.7 (c (n "solana-geyser-plugin-interface") (v "1.13.7") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.13.7") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.13.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "13m073bjgnh4iynw2j50q9m5hz4j4hdhnhshz76fb3h7bv3m5vl6")))

(define-public crate-solana-geyser-plugin-interface-1.14.18 (c (n "solana-geyser-plugin-interface") (v "1.14.18") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.14.18") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.14.18") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0vycwg85acy2ahjl26wrva0a8pry7pq54gxi49v07dpscfvdd6ak")))

(define-public crate-solana-geyser-plugin-interface-1.16.0 (c (n "solana-geyser-plugin-interface") (v "1.16.0") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.16.0") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.16.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0r4pyggbnrl6ndmgiif2flx0pqzqapggn8rk6y2hj6agih9kysdm")))

(define-public crate-solana-geyser-plugin-interface-1.16.1 (c (n "solana-geyser-plugin-interface") (v "1.16.1") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.16.1") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.16.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0amkqx4jcvg17li8zilnm7jbcjsyd6n5f8ilq2xvwh4rw3swm2ss")))

(define-public crate-solana-geyser-plugin-interface-1.14.19 (c (n "solana-geyser-plugin-interface") (v "1.14.19") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.14.19") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.14.19") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0nxcb9zsph3788hlc4sz7cyxxyjgiy48684vsjnlk6rvz8l5yxhh")))

(define-public crate-solana-geyser-plugin-interface-1.16.2 (c (n "solana-geyser-plugin-interface") (v "1.16.2") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.16.2") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.16.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0s7cjb4d9qxl9ml53ncbid1fg7vmsl1km8xanscxlmgf8x98pwn3")))

(define-public crate-solana-geyser-plugin-interface-1.16.3 (c (n "solana-geyser-plugin-interface") (v "1.16.3") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.16.3") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.16.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0yyxjy8x2n7vggqkivwvf190jkbnqrd1zh94885s5y7fk9fa2qp6")))

(define-public crate-solana-geyser-plugin-interface-1.14.20 (c (n "solana-geyser-plugin-interface") (v "1.14.20") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.14.20") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.14.20") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0pirflg35gyqd9qr55ris97p09lnrjadc1xnx4hssavxznrkdlqp")))

(define-public crate-solana-geyser-plugin-interface-1.16.4 (c (n "solana-geyser-plugin-interface") (v "1.16.4") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.16.4") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.16.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "1saswlcvrvj0mrbcn4br83i39ibk0d17m38mv0znprx659m3n86p")))

(define-public crate-solana-geyser-plugin-interface-1.16.5 (c (n "solana-geyser-plugin-interface") (v "1.16.5") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.16.5") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.16.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "17yhcb4kmc7gvn0hrd5rgli455vsh4w67xiqk8f84vmfb7l474zj")))

(define-public crate-solana-geyser-plugin-interface-1.14.21 (c (n "solana-geyser-plugin-interface") (v "1.14.21") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.14.21") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.14.21") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0dpjm682i0rxcm1rcd7y77rhhwqgn2f14h08ivrhbg5fadi7np4p")))

(define-public crate-solana-geyser-plugin-interface-1.14.22 (c (n "solana-geyser-plugin-interface") (v "1.14.22") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.14.22") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.14.22") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0sl4d87gn73jnymyagjsbrskijzby2vshkqniikmhjqcvfzsncs9")))

(define-public crate-solana-geyser-plugin-interface-1.16.6 (c (n "solana-geyser-plugin-interface") (v "1.16.6") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.16.6") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.16.6") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "1hc5l7p2z34gl4pj2n1vj35zmd0clvnhnv6x2y4b34gbaak51120")))

(define-public crate-solana-geyser-plugin-interface-1.16.7 (c (n "solana-geyser-plugin-interface") (v "1.16.7") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.16.7") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.16.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "1p3gazrxnip67cm0zmqkpy4bgb29iwcyzbjpqs5vk0kwygbs6qxa")))

(define-public crate-solana-geyser-plugin-interface-1.14.23 (c (n "solana-geyser-plugin-interface") (v "1.14.23") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.14.23") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.14.23") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1n7i6cxwzbwdacd50br4g01gka1ysvg2xfjkdajn4i1r37i09hz7")))

(define-public crate-solana-geyser-plugin-interface-1.16.8 (c (n "solana-geyser-plugin-interface") (v "1.16.8") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.16.8") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.16.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "1hv40nfp0g3bvbaj11rx22222vkpzb2y5pwlgahrfrn72wlxzi3n")))

(define-public crate-solana-geyser-plugin-interface-1.14.24 (c (n "solana-geyser-plugin-interface") (v "1.14.24") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.14.24") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.14.24") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0z6n945kn70d77agvpn3py27dmzlak6yw9jn7cschv9sx9wpc32j")))

(define-public crate-solana-geyser-plugin-interface-1.16.9 (c (n "solana-geyser-plugin-interface") (v "1.16.9") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.16.9") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.16.9") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "1bfkbvccmn4vmsdbh2i6p2r313yy0nd7576kpvy64jfa8hjp88zh")))

(define-public crate-solana-geyser-plugin-interface-1.16.10 (c (n "solana-geyser-plugin-interface") (v "1.16.10") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.16.10") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.16.10") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "11zk4jczx21jh45k1vd8zk7mq9i9hbsc1jz6jzs1dc126gmsvhfh")))

(define-public crate-solana-geyser-plugin-interface-1.14.25 (c (n "solana-geyser-plugin-interface") (v "1.14.25") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.14.25") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.14.25") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0gv2qbgnzrbbjngqz09k8jv7yrhf8qknrr4gcy28s24sn2msl2j1")))

(define-public crate-solana-geyser-plugin-interface-1.16.11 (c (n "solana-geyser-plugin-interface") (v "1.16.11") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.16.11") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.16.11") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0mshgivg7j3hd0gg7w1wk2y9afgzsxd92hqwfd0cxp4j1mdssm5h")))

(define-public crate-solana-geyser-plugin-interface-1.14.26 (c (n "solana-geyser-plugin-interface") (v "1.14.26") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.14.26") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.14.26") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0g1nwgf3jcm1alisn9dil5q159fnkpgbdzqbdyf1vkmr7p843ys6")))

(define-public crate-solana-geyser-plugin-interface-1.16.12 (c (n "solana-geyser-plugin-interface") (v "1.16.12") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.16.12") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.16.12") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0q664r0i9m2ygip4h90jcvxp84b752l8ipp3k8z64ps2901v7a55")))

(define-public crate-solana-geyser-plugin-interface-1.14.27 (c (n "solana-geyser-plugin-interface") (v "1.14.27") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.14.27") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.14.27") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0rckjnvzzk913fxf3fmpzc7ipyz59c0lkc8k0194a44z6rxlj1pv")))

(define-public crate-solana-geyser-plugin-interface-1.16.13 (c (n "solana-geyser-plugin-interface") (v "1.16.13") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.16.13") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.16.13") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "18r3vsjif1j97qbzvjhc44ajab1g1rhzv3ipi0bxr4gqy637d7f0")))

(define-public crate-solana-geyser-plugin-interface-1.16.14 (c (n "solana-geyser-plugin-interface") (v "1.16.14") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.16.14") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.16.14") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0rprbqsbldcqhgv6p7aj8vzaj8nz0i89ysnbsq2nn2rkw46sv34h")))

(define-public crate-solana-geyser-plugin-interface-1.14.28 (c (n "solana-geyser-plugin-interface") (v "1.14.28") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.14.28") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.14.28") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0kcppz98j96aqcb9wypmcqw19l9smwgiy9gkgacvjldyhk9hn6k5")))

(define-public crate-solana-geyser-plugin-interface-1.14.29 (c (n "solana-geyser-plugin-interface") (v "1.14.29") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.14.29") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.14.29") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0dlszwm8314my4003wpsj71ymvn1hl92c3rgfyhl1mxm2x0xdgnb")))

(define-public crate-solana-geyser-plugin-interface-1.16.15 (c (n "solana-geyser-plugin-interface") (v "1.16.15") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.16.15") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.16.15") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "05qa542dmyh7dl87ds1y7fhvfplf1vyrnic51kvwr4s1rdcbg396")))

(define-public crate-solana-geyser-plugin-interface-1.17.0 (c (n "solana-geyser-plugin-interface") (v "1.17.0") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.0") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.17.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "0wms1m96hi69d6gca23kvny5kx77zinqiw6cwkjrayy9g5lj4sfm")))

(define-public crate-solana-geyser-plugin-interface-1.16.16 (c (n "solana-geyser-plugin-interface") (v "1.16.16") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.16.16") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.16.16") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "1l8iy1lvwa0w9sw2xyy3g77qkqgf1dbcvkkjg1vi6hcp87vp48si")))

(define-public crate-solana-geyser-plugin-interface-1.17.1 (c (n "solana-geyser-plugin-interface") (v "1.17.1") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.1") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.17.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "1lgzx683zyg84zm303kprrzwghw74mzzqxm01nv25fzgvqai8r44")))

(define-public crate-solana-geyser-plugin-interface-1.16.17 (c (n "solana-geyser-plugin-interface") (v "1.16.17") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.16.17") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.16.17") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "1iaxy2wwfzj4iw9n57sfxcjf7g4wljj1xqhynz36b0ya6i5vb940")))

(define-public crate-solana-geyser-plugin-interface-1.17.2 (c (n "solana-geyser-plugin-interface") (v "1.17.2") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.2") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.17.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "0ggx5mrxf5nryw6gsg98rmjl91jxjj459zmav28x0r8sipg9x0yv")))

(define-public crate-solana-geyser-plugin-interface-1.16.18 (c (n "solana-geyser-plugin-interface") (v "1.16.18") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.16.18") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.16.18") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "1zqhvg3p7dqq0x52b7dix66q7dwx98wd3vkz2il36hmxbbi7zn2d")))

(define-public crate-solana-geyser-plugin-interface-1.17.3 (c (n "solana-geyser-plugin-interface") (v "1.17.3") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.3") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.17.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "0rj26riv85y3pvq13afnin8y34bajwczgw0wjpjj7gfxv2n6m9fa")))

(define-public crate-solana-geyser-plugin-interface-1.17.4 (c (n "solana-geyser-plugin-interface") (v "1.17.4") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.4") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.17.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "0i29y7bhwys62dv7ymbbwg6m6m5dpndhjiw2prvb5n4k6y1wn8c6")))

(define-public crate-solana-geyser-plugin-interface-1.16.19 (c (n "solana-geyser-plugin-interface") (v "1.16.19") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.16.19") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.16.19") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0fah0li7mwbbf8x3xdfgpra46jq6qf9mn833c1m0fh1xsl7arz68")))

(define-public crate-solana-geyser-plugin-interface-1.17.5 (c (n "solana-geyser-plugin-interface") (v "1.17.5") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.5") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.17.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "02jw59gs2740lwk3gjr2vf6kwmdg6wny2gcbv576l1a15gmsc59y")))

(define-public crate-solana-geyser-plugin-interface-1.17.6 (c (n "solana-geyser-plugin-interface") (v "1.17.6") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.6") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.17.6") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "1widx22qz8kn000nf1591b77dqjwq237x1gkb8cx1hcj0w7pc2yl")))

(define-public crate-solana-geyser-plugin-interface-1.16.20 (c (n "solana-geyser-plugin-interface") (v "1.16.20") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.16.20") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.16.20") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "01lbzrrmi3r2fmxljabvahgr5nssz1jamrirm0zjvvs858fky5sw")))

(define-public crate-solana-geyser-plugin-interface-1.17.7 (c (n "solana-geyser-plugin-interface") (v "1.17.7") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.7") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.17.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "1w6fvr9jv468xa7mn3yz50xck2bfb0j19ikmz0kcc47632ssp9ri")))

(define-public crate-solana-geyser-plugin-interface-1.16.21 (c (n "solana-geyser-plugin-interface") (v "1.16.21") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.16.21") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.16.21") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "1rr2l32fd4pqc9w5m4h4h2ipn2mxg9s7yfw12mj6yrr249vwcbif")))

(define-public crate-solana-geyser-plugin-interface-1.17.8 (c (n "solana-geyser-plugin-interface") (v "1.17.8") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.8") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.17.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "07cmfffy3q5izwc4n6nv6nlic8g6j991v6xwjpj1fr2bgiqfhk0f")))

(define-public crate-solana-geyser-plugin-interface-1.16.22 (c (n "solana-geyser-plugin-interface") (v "1.16.22") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.16.22") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.16.22") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "19n67ks17zm5slg5kry6x1ab66a298lc2rx9kqngjhdqb95mw1ip")))

(define-public crate-solana-geyser-plugin-interface-1.16.23 (c (n "solana-geyser-plugin-interface") (v "1.16.23") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.16.23") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.16.23") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "16fjqxyhslqap4p2llwd4c07gsq6fdqp2c997r2nfc303b2gdw3w")))

(define-public crate-solana-geyser-plugin-interface-1.17.9 (c (n "solana-geyser-plugin-interface") (v "1.17.9") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.9") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.17.9") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "03z0lfgnxn4wzg4sxfnpwniz38cqg12jln0gpcxjywr9g7q7l1hd")))

(define-public crate-solana-geyser-plugin-interface-1.17.10 (c (n "solana-geyser-plugin-interface") (v "1.17.10") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.10") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.17.10") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "0y27nd1b1pdx7gn65b5ixf0zs4disdmkkb17ypnxkhvn072picc9")))

(define-public crate-solana-geyser-plugin-interface-1.17.11 (c (n "solana-geyser-plugin-interface") (v "1.17.11") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.11") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.17.11") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "0zdv04ijjkmcyxy0s49a1w6fxap7y8k5fg70abi44ffrb09lgcwh")))

(define-public crate-solana-geyser-plugin-interface-1.17.12 (c (n "solana-geyser-plugin-interface") (v "1.17.12") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.12") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.17.12") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "1wgl2czdi7qbzhlxcm8cxj9x7l55z6vw40bs19g7vvnk92yl8j1n")))

(define-public crate-solana-geyser-plugin-interface-1.16.24 (c (n "solana-geyser-plugin-interface") (v "1.16.24") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.16.24") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.16.24") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "19ijfjki05mzkj9fh9jnd6fcb0zb0vabk1k6vhk541gvbdd2nhbv")))

(define-public crate-solana-geyser-plugin-interface-1.17.13 (c (n "solana-geyser-plugin-interface") (v "1.17.13") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.13") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.17.13") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "1abm0gwvwzzxv1xrk93mj2px53mg8p6g3spnh97h4lmm5ba339cc")))

(define-public crate-solana-geyser-plugin-interface-1.17.14 (c (n "solana-geyser-plugin-interface") (v "1.17.14") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.14") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.17.14") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "0qm2kyh49qbdhxfj5h9knzaqi2xd5y61am6nqi32bb1yd84hm4cd")))

(define-public crate-solana-geyser-plugin-interface-1.17.15 (c (n "solana-geyser-plugin-interface") (v "1.17.15") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.15") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.17.15") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "16hf36p5fmddp66x4nrl58l406j022bvg4l5gj5p8sssqzg69pnp")))

(define-public crate-solana-geyser-plugin-interface-1.16.25 (c (n "solana-geyser-plugin-interface") (v "1.16.25") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.16.25") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.16.25") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0xildgwqlh82djlmwzva8kpfzj7z5a7nb6ml0q6x5aq6w0c8hc2p")))

(define-public crate-solana-geyser-plugin-interface-1.16.27 (c (n "solana-geyser-plugin-interface") (v "1.16.27") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.16.27") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.16.27") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0nhq4jkhdnnc8y9v0ailfs1r9gzbv0ikwizjp7dak2ziw8zm39g5")))

(define-public crate-solana-geyser-plugin-interface-1.17.16 (c (n "solana-geyser-plugin-interface") (v "1.17.16") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.16") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.17.16") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "0flk82lc6hdjzza1svbi6p2mdzn1r0wn3xpkffl0zdjjw59q9l2l")))

(define-public crate-solana-geyser-plugin-interface-1.17.17 (c (n "solana-geyser-plugin-interface") (v "1.17.17") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.17") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.17.17") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "07i7gs4mk42qnb3frbm7iw3zgc2lrwhxv0jqy79qnx3yjb8flyhy")))

(define-public crate-solana-geyser-plugin-interface-1.17.18 (c (n "solana-geyser-plugin-interface") (v "1.17.18") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.18") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.17.18") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "17hp88sp5yfcd76l5fqlpy49xg0kh2am01gq3gz1c63ai29y93rn")))

(define-public crate-solana-geyser-plugin-interface-1.18.0 (c (n "solana-geyser-plugin-interface") (v "1.18.0") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.18.0") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.18.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "0q67zgsasqknd9g1jccmnjd588xm32w09i98n0ql547hhlm6ncbw")))

(define-public crate-solana-geyser-plugin-interface-1.18.1 (c (n "solana-geyser-plugin-interface") (v "1.18.1") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.18.1") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.18.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "1hqjm0arqqyd8rs21hqkl52d16fvqbgzd5mvlqj8y9dnrl1sb5z9")))

(define-public crate-solana-geyser-plugin-interface-1.17.20 (c (n "solana-geyser-plugin-interface") (v "1.17.20") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.20") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.17.20") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "0j882mzmpavgryh8nfxh2ipkwngkny0ah4zxqb16db755dnib928")))

(define-public crate-solana-geyser-plugin-interface-1.17.22 (c (n "solana-geyser-plugin-interface") (v "1.17.22") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.22") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.17.22") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "0qlrvri8ywpn4b1xg9l484izi09yqsvx2mw9ayklrvc3sfklnp34")))

(define-public crate-solana-geyser-plugin-interface-1.18.2 (c (n "solana-geyser-plugin-interface") (v "1.18.2") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.18.2") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.18.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "0iaypk8n3ln7cfx55aabk84ii9qv7jp2d069i737nm6xakrjhrx3")))

(define-public crate-solana-geyser-plugin-interface-1.17.23 (c (n "solana-geyser-plugin-interface") (v "1.17.23") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.23") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.17.23") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "157n28g46zvkpxjdli8yr20sf4af91xc60xgd5bmhwxq2vrj1qc0")))

(define-public crate-solana-geyser-plugin-interface-1.18.3 (c (n "solana-geyser-plugin-interface") (v "1.18.3") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.18.3") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.18.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "17i2sshfx18vdhqa9qg1lyzq93zxqcxkwa5gaxq1844wf4ymxz5b")))

(define-public crate-solana-geyser-plugin-interface-1.18.4 (c (n "solana-geyser-plugin-interface") (v "1.18.4") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.18.4") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.18.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "1k6zsd956vrj6ywvacbzc0wr1wywfg9axqbycnrqkk06bjls27w6")))

(define-public crate-solana-geyser-plugin-interface-1.17.24 (c (n "solana-geyser-plugin-interface") (v "1.17.24") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.24") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.17.24") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "1g81l6ww1yz52a5n02qkai593bnpn8fxgfgzxdaxrjjwvnqjrflw")))

(define-public crate-solana-geyser-plugin-interface-1.18.5 (c (n "solana-geyser-plugin-interface") (v "1.18.5") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.18.5") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.18.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "06qsifmzakgwglpw58dwq4bnxyljq0fnfmq86f14f7fwycfgx22c")))

(define-public crate-solana-geyser-plugin-interface-1.17.25 (c (n "solana-geyser-plugin-interface") (v "1.17.25") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.25") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.17.25") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "0zpp7pd11vf9wr0vlj80r9xc8s2h0wm9rzwj5pwznlswv2039nvs")))

(define-public crate-solana-geyser-plugin-interface-1.17.26 (c (n "solana-geyser-plugin-interface") (v "1.17.26") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.26") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.17.26") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "1p637gxv81vnvs5bxjqa4j0zy0l7dj4a05ilgmknp6y9phrm686r")))

(define-public crate-solana-geyser-plugin-interface-1.18.6 (c (n "solana-geyser-plugin-interface") (v "1.18.6") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.18.6") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.18.6") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "19p09kwyycnmqx4mhl9s83wyix9zdbyjjxkvrq92v0wz7zqss04i")))

(define-public crate-solana-geyser-plugin-interface-1.17.27 (c (n "solana-geyser-plugin-interface") (v "1.17.27") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.27") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.17.27") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "11hkzhq78iwr6zgyfhny0pxzg7k6p0wdj6did2san0pnywjhaids")))

(define-public crate-solana-geyser-plugin-interface-1.18.7 (c (n "solana-geyser-plugin-interface") (v "1.18.7") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.18.7") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.18.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "0pkhga9rvhsn5v7y51jnvgs2v7yr616lbgd5kk54hswd3qhgsqyx")))

(define-public crate-solana-geyser-plugin-interface-1.18.8 (c (n "solana-geyser-plugin-interface") (v "1.18.8") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.18.8") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.18.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "0040gb671ygiw4xbdzhcvq4lz0lg1ih0k9p5jvlq931l8kc5m9kr")))

(define-public crate-solana-geyser-plugin-interface-1.17.28 (c (n "solana-geyser-plugin-interface") (v "1.17.28") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.28") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.17.28") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "1rc91ddbjn4gl7h4bkassmz78lxdn45vjn4n04znv720r5fl4czn")))

(define-public crate-solana-geyser-plugin-interface-1.17.29 (c (n "solana-geyser-plugin-interface") (v "1.17.29") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.29") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.17.29") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "062iglsd70x3clh6rl83za8y8gfk9xn9nswn1ipv3px9z26b146y")))

(define-public crate-solana-geyser-plugin-interface-1.18.9 (c (n "solana-geyser-plugin-interface") (v "1.18.9") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.18.9") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.18.9") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "01y7pgfq0ibd09b0ja05rn3wfpi645zkpxnc668am10ym7xby358")))

(define-public crate-solana-geyser-plugin-interface-1.17.30 (c (n "solana-geyser-plugin-interface") (v "1.17.30") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.30") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.17.30") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "1k9ji69vr8s8lv74af2a8jdxzdvyblpdlwgh0yzin7gc2q3zy63d")))

(define-public crate-solana-geyser-plugin-interface-1.18.10 (c (n "solana-geyser-plugin-interface") (v "1.18.10") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.18.10") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.18.10") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "1dfyq4743hs7bsm8598cmrc1zi0zhfg3pxkgqyakrdbp772i8x4l")))

(define-public crate-solana-geyser-plugin-interface-1.18.11 (c (n "solana-geyser-plugin-interface") (v "1.18.11") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.18.11") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.18.11") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "0w0vala89ih25ppg1dd6j2njr32dv07jhfrxalc5x5m015bldm7l")))

(define-public crate-solana-geyser-plugin-interface-1.17.31 (c (n "solana-geyser-plugin-interface") (v "1.17.31") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.31") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.17.31") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "0vy4xyvfqm9lx4n0b9fij5wmqa1s4jigd49b3zn9lknmrybrj4na")))

(define-public crate-solana-geyser-plugin-interface-1.18.12 (c (n "solana-geyser-plugin-interface") (v "1.18.12") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.18.12") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.18.12") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "1pgii56v5nycp78sjn7hiq59xq1makrqa03scgyfa2vby0202cgz")))

(define-public crate-solana-geyser-plugin-interface-1.17.32 (c (n "solana-geyser-plugin-interface") (v "1.17.32") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.32") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.17.32") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "0yscr7gwkcs8hndkqpcsddjnqfxg1fx7r0s1cgmi4ky2nmhljvib")))

(define-public crate-solana-geyser-plugin-interface-1.17.33 (c (n "solana-geyser-plugin-interface") (v "1.17.33") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.33") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.17.33") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "13aamr8p699g3v1m5q61i2yb09sf89na38z408yd4svir8l53dgn")))

(define-public crate-solana-geyser-plugin-interface-1.18.13 (c (n "solana-geyser-plugin-interface") (v "1.18.13") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.18.13") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.18.13") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "0cwhxrzhyl4rqafqfn31gvq5la1yhlr2wkailqp9k2ric7bw3jds")))

(define-public crate-solana-geyser-plugin-interface-1.18.14 (c (n "solana-geyser-plugin-interface") (v "1.18.14") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.18.14") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.18.14") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "0rfka60d8zgkd8b5kn52g4djzvlan4wi8sfhgcg3j3ha5d378qxc")))

(define-public crate-solana-geyser-plugin-interface-1.17.34 (c (n "solana-geyser-plugin-interface") (v "1.17.34") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.34") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.17.34") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "022jdq1gwk2ad8c9ch0prk8knkiym3plxhqy8k5cgp3jcq9plkqm")))

