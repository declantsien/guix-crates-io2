(define-module (crates-io so la solaredge_modbus_chargehq_exporter) #:use-module (crates-io))

(define-public crate-solaredge_modbus_chargehq_exporter-0.1.0 (c (n "solaredge_modbus_chargehq_exporter") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "clap") (r "^4.2.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11.16") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.27.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-modbus") (r "^0.7.1") (f (quote ("tcp"))) (k 0)))) (h "07mjyf2kar0y8ykhjzz3hq03i4d7lddqs1amxaw51ibplkk6bwkg")))

