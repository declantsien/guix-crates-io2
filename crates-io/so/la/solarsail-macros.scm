(define-module (crates-io so la solarsail-macros) #:use-module (crates-io))

(define-public crate-solarsail-macros-0.1.0 (c (n "solarsail-macros") (v "0.1.0") (d (list (d (n "http") (r "^0.2") (d #t) (k 2)) (d (n "proc-macro-error") (r "^1") (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "02jxamn9pw4cf8xfgb3h6ix223jxzpyf2jxa2j2n5gbi8lxb5rrp")))

(define-public crate-solarsail-macros-0.1.1 (c (n "solarsail-macros") (v "0.1.1") (d (list (d (n "http") (r "^0.2") (d #t) (k 2)) (d (n "proc-macro-error") (r "^1") (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "06jrnd7ib266d8ygp3b8ajry3jcgl0405mklhgvc54sq79l9rv1s")))

(define-public crate-solarsail-macros-0.2.0 (c (n "solarsail-macros") (v "0.2.0") (d (list (d (n "http") (r "^0.2") (d #t) (k 2)) (d (n "proc-macro-error") (r "^1") (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0ahv9b6x2c25glrqng50lcxih45rdygd0a09g8fs1f0vcrmr7rck")))

(define-public crate-solarsail-macros-0.4.0 (c (n "solarsail-macros") (v "0.4.0") (d (list (d (n "http") (r "^1.0") (d #t) (k 2)) (d (n "proc-macro-error") (r "^1") (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "16vms141g1pf1vk3sc6d8118yral1ax4pqpgjq63ymp188ky5dc0")))

