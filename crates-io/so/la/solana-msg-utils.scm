(define-module (crates-io so la solana-msg-utils) #:use-module (crates-io))

(define-public crate-solana-msg-utils-0.1.0 (c (n "solana-msg-utils") (v "0.1.0") (d (list (d (n "solana-program") (r ">=1.8") (d #t) (k 0)) (d (n "tulip-arrform") (r "^0.1.1") (d #t) (k 0)))) (h "05w91y03lanx536v8mnwmm3p7m6swz8l1ca3dx89470rg75s3j6b")))

(define-public crate-solana-msg-utils-0.1.1 (c (n "solana-msg-utils") (v "0.1.1") (d (list (d (n "solana-program") (r ">=1.8") (d #t) (k 0)) (d (n "tulip-arrform") (r "^0.1.1") (d #t) (k 0)))) (h "11f6zk9xvakkg3w2m2b8mba1qr1jfqh4pw6388s9afaxfm4l0j60")))

