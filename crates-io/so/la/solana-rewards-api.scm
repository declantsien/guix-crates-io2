(define-module (crates-io so la solana-rewards-api) #:use-module (crates-io))

(define-public crate-solana-rewards-api-0.12.0 (c (n "solana-rewards-api") (v "0.12.0") (d (list (d (n "bincode") (r "^1.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.89") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.89") (d #t) (k 0)) (d (n "solana-sdk") (r "^0.12.0") (d #t) (k 0)) (d (n "solana-vote-api") (r "^0.12.0") (d #t) (k 0)))) (h "02k97ddpdggy4lqrc58sbq75jwxnbsl9d949xdz27qkmsral0l8y")))

