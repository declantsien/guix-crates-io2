(define-module (crates-io so la solana-readonly-account) #:use-module (crates-io))

(define-public crate-solana-readonly-account-1.0.0 (c (n "solana-readonly-account") (v "1.0.0") (d (list (d (n "derive_more") (r ">=0.99") (f (quote ("deref" "deref_mut" "as_ref" "as_mut" "from" "into"))) (o #t) (k 0)) (d (n "solana-program") (r "^1") (d #t) (k 0)) (d (n "solana-sdk") (r "^1") (o #t) (d #t) (k 0)) (d (n "spl-token-2022") (r "^0.7") (d #t) (k 2)))) (h "0nnhhxqfgbacvlkflf5ix05ii3xz5d95k6gi33z9ric4kmkjxd6r") (f (quote (("default")))) (s 2) (e (quote (("solana-sdk" "dep:solana-sdk" "dep:derive_more"))))))

(define-public crate-solana-readonly-account-1.1.0 (c (n "solana-readonly-account") (v "1.1.0") (d (list (d (n "derive_more") (r ">=0.99") (f (quote ("deref" "deref_mut" "as_ref" "as_mut" "from" "into"))) (o #t) (k 0)) (d (n "solana-program") (r "^1") (d #t) (k 0)) (d (n "solana-sdk") (r "^1") (o #t) (d #t) (k 0)) (d (n "spl-token-2022") (r "^0.7") (d #t) (k 2)))) (h "0k0wkrb063wvhaq4c4bpvzmbzg6f29bq908wpsh5lqlhn1zrrkx8") (f (quote (("default")))) (s 2) (e (quote (("solana-sdk" "dep:solana-sdk" "dep:derive_more"))))))

