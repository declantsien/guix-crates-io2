(define-module (crates-io so la solana_bank) #:use-module (crates-io))

(define-public crate-solana_bank-0.1.0 (c (n "solana_bank") (v "0.1.0") (d (list (d (n "arrayref") (r "^0.3.6") (d #t) (k 0)) (d (n "blob") (r "^0.3.0") (d #t) (k 0)) (d (n "borsh") (r "^0.9.1") (d #t) (k 0)) (d (n "borsh-derive") (r "^0.9.1") (d #t) (k 0)) (d (n "solana-program") (r "^1.7.11") (d #t) (k 0)) (d (n "solana-program-test") (r "=1.8.0") (d #t) (k 2)) (d (n "solana-sdk") (r "=1.8.0") (d #t) (k 2)))) (h "1sqvmaqdmg1faxsj8vgmljdfa4g3ymjws6nln3zjpx7plmfkmkxj") (f (quote (("no-entrypoint"))))))

