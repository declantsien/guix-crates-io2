(define-module (crates-io so la solana-maths) #:use-module (crates-io))

(define-public crate-solana-maths-0.1.0 (c (n "solana-maths") (v "0.1.0") (d (list (d (n "arrayref") (r "^0.3.6") (d #t) (k 0)) (d (n "num-derive") (r "^0.3.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "solana-program") (r "^1.8.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "uint") (r "^0.9.1") (d #t) (k 0)))) (h "19lxsxfpnfwpi6n6n2rnyhl1pdmy56g2imzp4sq615bb7ydrj8rg")))

(define-public crate-solana-maths-0.1.1 (c (n "solana-maths") (v "0.1.1") (d (list (d (n "arrayref") (r "^0.3.6") (d #t) (k 0)) (d (n "num-derive") (r "^0.3.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "solana-program") (r "^1.8.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "uint") (r "^0.9.1") (d #t) (k 0)))) (h "05g7ivcr7m46w42basz3d6fc6j9zm8bvjr335926f4dxs1p1jf58")))

(define-public crate-solana-maths-0.1.2 (c (n "solana-maths") (v "0.1.2") (d (list (d (n "arrayref") (r "^0.3.6") (d #t) (k 0)) (d (n "num-derive") (r "^0.3.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "solana-program") (r "^1.8.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "uint") (r "^0.8") (d #t) (k 0)))) (h "19sn8majlfrws12x2kiaxjcdzn1qz2rpi86d2ja63q22gyh5cnhs")))

