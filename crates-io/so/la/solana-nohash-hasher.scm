(define-module (crates-io so la solana-nohash-hasher) #:use-module (crates-io))

(define-public crate-solana-nohash-hasher-0.2.0 (c (n "solana-nohash-hasher") (v "0.2.0") (h "0y5i7vz3sqas7v3zamh05xcipsplsgaii4hnc4kmdpvf0m5ac4kg") (f (quote (("std") ("default" "std"))))))

(define-public crate-solana-nohash-hasher-0.2.1 (c (n "solana-nohash-hasher") (v "0.2.1") (h "07n2znf3mqwxiwl3yxqfwz9ys72iy7h5zc3si9y1g28fsqg772lb") (f (quote (("std") ("default" "std"))))))

