(define-module (crates-io so la solana_libra_failure_ext) #:use-module (crates-io))

(define-public crate-solana_libra_failure_ext-0.0.0-sol7 (c (n "solana_libra_failure_ext") (v "0.0.0-sol7") (d (list (d (n "failure") (r "^0.1.3") (d #t) (k 0)) (d (n "failure_macros") (r "^0.0.0-sol7") (d #t) (k 0) (p "solana_libra_failure_macros")))) (h "0ivai56lc41sxq148vg7lrfs42ypg8p1plk4aw4zxppw4grv3bf1") (y #t)))

(define-public crate-solana_libra_failure_ext-0.0.0-sol8 (c (n "solana_libra_failure_ext") (v "0.0.0-sol8") (d (list (d (n "failure") (r "^0.1.3") (d #t) (k 0)) (d (n "failure_macros") (r "^0.0.0-sol8") (d #t) (k 0) (p "solana_libra_failure_macros")))) (h "1rvlcr1drjwvqqls4k3bc55hj8jqlqysy0k3cqnbkszj38ps6p5w") (y #t)))

(define-public crate-solana_libra_failure_ext-0.0.0-sol9 (c (n "solana_libra_failure_ext") (v "0.0.0-sol9") (d (list (d (n "failure") (r "^0.1.3") (d #t) (k 0)) (d (n "failure_macros") (r "^0.0.0-sol9") (d #t) (k 0) (p "solana_libra_failure_macros")))) (h "1p722kc92247dgp6cpflyinv16863cdhj3ngji4brwaf22sw7v5p") (y #t)))

(define-public crate-solana_libra_failure_ext-0.0.0-sol10 (c (n "solana_libra_failure_ext") (v "0.0.0-sol10") (d (list (d (n "failure") (r "^0.1.3") (d #t) (k 0)) (d (n "failure_macros") (r "^0.0.0-sol10") (d #t) (k 0) (p "solana_libra_failure_macros")))) (h "09k9bfsnwlai2bq8b2c2pwchm8lx4p3sim7x8qyyb3yk67ks9wbk") (y #t)))

(define-public crate-solana_libra_failure_ext-0.0.0-sol11 (c (n "solana_libra_failure_ext") (v "0.0.0-sol11") (d (list (d (n "failure") (r "^0.1.3") (d #t) (k 0)) (d (n "failure_macros") (r "^0.0.0-sol11") (d #t) (k 0) (p "solana_libra_failure_macros")))) (h "1xi8qbcl7m083r7rd9wfph3p9x8zzcs2h4pix343az5pyc858fxw") (y #t)))

(define-public crate-solana_libra_failure_ext-0.0.0-sol12 (c (n "solana_libra_failure_ext") (v "0.0.0-sol12") (d (list (d (n "failure") (r "^0.1.3") (d #t) (k 0)) (d (n "failure_macros") (r "^0.0.0-sol12") (d #t) (k 0) (p "solana_libra_failure_macros")))) (h "0janwhlgzg6cky80vf98c2n4521xc708sa74c37hbp4fhpszzj8q") (y #t)))

(define-public crate-solana_libra_failure_ext-0.0.0-sol13 (c (n "solana_libra_failure_ext") (v "0.0.0-sol13") (d (list (d (n "failure") (r "^0.1.3") (d #t) (k 0)) (d (n "failure_macros") (r "^0.0.0-sol13") (d #t) (k 0) (p "solana_libra_failure_macros")))) (h "0sf0ll343gnjiq6ybh4i856kczq7n566y3djwh4kp7xhv8cr6pbb") (y #t)))

(define-public crate-solana_libra_failure_ext-0.0.0-sol14 (c (n "solana_libra_failure_ext") (v "0.0.0-sol14") (d (list (d (n "failure") (r "^0.1.3") (d #t) (k 0)) (d (n "failure_macros") (r "^0.0.0-sol14") (d #t) (k 0) (p "solana_libra_failure_macros")))) (h "0s9did07rw4vqqi763704rx4yb51bvcszjm2jclcqsf8mdbvzyd0") (y #t)))

(define-public crate-solana_libra_failure_ext-0.0.0-sol15 (c (n "solana_libra_failure_ext") (v "0.0.0-sol15") (d (list (d (n "failure") (r "^0.1.3") (d #t) (k 0)) (d (n "failure_macros") (r "^0.0.0-sol15") (d #t) (k 0) (p "solana_libra_failure_macros")))) (h "02hbbhiiazmhfpccsx5gk29cvxa2l5n0387j9waynx69gw6gaipp")))

(define-public crate-solana_libra_failure_ext-0.0.0 (c (n "solana_libra_failure_ext") (v "0.0.0") (d (list (d (n "failure") (r "^0.1.3") (d #t) (k 0)) (d (n "failure_macros") (r "^0.0.0") (d #t) (k 0) (p "solana_libra_failure_macros")))) (h "1md96a5bgzw5ymv9wq91f6l8jfgp3p3ydf28w2pyjmll18p46rvw")))

(define-public crate-solana_libra_failure_ext-0.0.1-sol3 (c (n "solana_libra_failure_ext") (v "0.0.1-sol3") (d (list (d (n "failure") (r "^0.1.3") (d #t) (k 0)) (d (n "solana_libra_failure_macros") (r "^0.0.1-sol3") (d #t) (k 0)))) (h "190h8sxsdfr6vay1dadx9xvbn5d297w387c8k81580ircvyan1dz")))

(define-public crate-solana_libra_failure_ext-0.0.1-sol4 (c (n "solana_libra_failure_ext") (v "0.0.1-sol4") (d (list (d (n "failure") (r "^0.1.3") (d #t) (k 0)) (d (n "solana_libra_failure_macros") (r "^0.0.1-sol4") (d #t) (k 0)))) (h "0zyjmg8brx0bnxg7277v00wb1skplmqnng4d18gwj6ksi7bsvwfl")))

(define-public crate-solana_libra_failure_ext-0.0.1-sol5 (c (n "solana_libra_failure_ext") (v "0.0.1-sol5") (d (list (d (n "failure") (r "^0.1.3") (d #t) (k 0)) (d (n "solana_libra_failure_macros") (r "^0.0.1-sol5") (d #t) (k 0)))) (h "04c5nlji4ils6p7jqlgmr86m0354c4miixizny6camhlwlq1cnw5")))

