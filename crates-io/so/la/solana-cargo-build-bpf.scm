(define-module (crates-io so la solana-cargo-build-bpf) #:use-module (crates-io))

(define-public crate-solana-cargo-build-bpf-1.4.3 (c (n "solana-cargo-build-bpf") (v "1.4.3") (d (list (d (n "cargo_metadata") (r "^0.12.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)))) (h "07azd97b5nxgzn4xfsdlk27l441ss1dmggr6msyyql0d4rqc6kvz") (f (quote (("program")))) (y #t)))

(define-public crate-solana-cargo-build-bpf-1.4.4 (c (n "solana-cargo-build-bpf") (v "1.4.4") (d (list (d (n "cargo_metadata") (r "^0.12.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)))) (h "0hcrqspzh4il4smar2cn4i80dlcd2h4dqhs585cg0h1qynxh5nw0") (f (quote (("program")))) (y #t)))

(define-public crate-solana-cargo-build-bpf-1.4.5 (c (n "solana-cargo-build-bpf") (v "1.4.5") (d (list (d (n "cargo_metadata") (r "^0.12.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)))) (h "110m9wr9gcf436b8290dknwrcd58b56iha71kc0k9zbrn46626gf") (f (quote (("program")))) (y #t)))

(define-public crate-solana-cargo-build-bpf-1.4.6 (c (n "solana-cargo-build-bpf") (v "1.4.6") (d (list (d (n "cargo_metadata") (r "^0.12.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)))) (h "03nla1m9aybkvp5677x80ssfyl0bjpp6lymla07048fwjwmw00i9") (f (quote (("program")))) (y #t)))

(define-public crate-solana-cargo-build-bpf-1.4.7 (c (n "solana-cargo-build-bpf") (v "1.4.7") (d (list (d (n "cargo_metadata") (r "^0.12.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)))) (h "0yqpw270lppqpq03969x7qr41jvih4nb85fvq96y61adys9919pw") (f (quote (("program")))) (y #t)))

(define-public crate-solana-cargo-build-bpf-1.4.8 (c (n "solana-cargo-build-bpf") (v "1.4.8") (d (list (d (n "cargo_metadata") (r "^0.12.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)))) (h "1dvl776cmkczdydx42svncq04k537qkvyzhmssnqhib1afdryh1r") (f (quote (("program")))) (y #t)))

(define-public crate-solana-cargo-build-bpf-1.4.9 (c (n "solana-cargo-build-bpf") (v "1.4.9") (d (list (d (n "cargo_metadata") (r "^0.12.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)))) (h "0lmmi11xvfl9059j82sq7q0pdmvp59k2wia7vwswpjwr2fsxclfh") (f (quote (("program")))) (y #t)))

