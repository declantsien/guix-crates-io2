(define-module (crates-io so la solaredge-reqwest) #:use-module (crates-io))

(define-public crate-solaredge-reqwest-0.1.0 (c (n "solaredge-reqwest") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "solaredge") (r "^0.1") (d #t) (k 0)) (d (n "url") (r "^2.0") (d #t) (k 0)))) (h "0mflk4vrk715fvdvrlxn6rjca1mvq8p1xlk7ppvxg2vi2v17nskz")))

(define-public crate-solaredge-reqwest-0.1.1 (c (n "solaredge-reqwest") (v "0.1.1") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "solaredge") (r "^0.1") (d #t) (k 0)) (d (n "url") (r "^2.0") (d #t) (k 0)))) (h "1k6l8qq54imvyvrz4vjvnyf0c7cbyg78h56ggvm8mci20n9m3ygj")))

(define-public crate-solaredge-reqwest-0.1.2 (c (n "solaredge-reqwest") (v "0.1.2") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "solaredge") (r "^0.1") (d #t) (k 0)) (d (n "url") (r "^2.0") (d #t) (k 0)))) (h "04vkc9gzm8i0cjf93clqpiyxys5p20gjclc3dfy1a4hs8kczsdjj")))

(define-public crate-solaredge-reqwest-0.1.3 (c (n "solaredge-reqwest") (v "0.1.3") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "solaredge") (r "^0.1") (d #t) (k 0)) (d (n "url") (r "^2.0") (d #t) (k 0)))) (h "18hhar4kswf31wrw257pnxffzm03b0x2795pjf2c4bw4jgsbkrgd")))

(define-public crate-solaredge-reqwest-0.2.0 (c (n "solaredge-reqwest") (v "0.2.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "solaredge") (r "^0.2") (d #t) (k 0)) (d (n "url") (r "^2.0") (d #t) (k 0)))) (h "1cya2j05s6xdkm7p4cf716dynadl71zdx2vxyibapqmg05b1m0s9")))

(define-public crate-solaredge-reqwest-0.2.1 (c (n "solaredge-reqwest") (v "0.2.1") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "solaredge") (r "^0.2") (d #t) (k 0)) (d (n "url") (r "^2.0") (d #t) (k 0)))) (h "1r1g7pbgnlm75019gz710y7vs84cybmy8345bmivlamvlgkx6l9a")))

