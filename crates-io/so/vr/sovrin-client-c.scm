(define-module (crates-io so vr sovrin-client-c) #:use-module (crates-io))

(define-public crate-sovrin-client-c-0.1.0-1 (c (n "sovrin-client-c") (v "0.1.0-1") (d (list (d (n "libc") (r "^0.2.21") (d #t) (k 0)) (d (n "sovrin-client") (r "^0.1.0-15") (d #t) (k 0)))) (h "1dy6shan3gvmwm5fz11sapfd2zb167c6vfkfjfs6pl9b9vnybpr9")))

