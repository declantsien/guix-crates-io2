(define-module (crates-io so vr sovrin-rust-client-lib) #:use-module (crates-io))

(define-public crate-sovrin-rust-client-lib-0.1.0 (c (n "sovrin-rust-client-lib") (v "0.1.0") (d (list (d (n "zmq") (r "^0.8.1") (d #t) (k 0)))) (h "0kfpfjyf1kgfasbgxwx11p3b90s9pwypqwzwz0gcr7ldigh1m5nz")))

(define-public crate-sovrin-rust-client-lib-0.1.0-suffix1 (c (n "sovrin-rust-client-lib") (v "0.1.0-suffix1") (d (list (d (n "zmq") (r "^0.8.1") (d #t) (k 0)))) (h "0ayif52yhdq1n1ixpix7spbv6h6883gq5a8942z6air6gvh4lad2")))

(define-public crate-sovrin-rust-client-lib-0.1.0-devel-92 (c (n "sovrin-rust-client-lib") (v "0.1.0-devel-92") (d (list (d (n "zmq") (r "^0.8.1") (d #t) (k 0)))) (h "0l8wngdld20q9jnmyrnhyvrbjqk02nyb4hz3lvz2vqs4q4i1yp71")))

