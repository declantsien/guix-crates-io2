(define-module (crates-io so mm somm_proto_build) #:use-module (crates-io))

(define-public crate-somm_proto_build-0.1.0 (c (n "somm_proto_build") (v "0.1.0") (d (list (d (n "prost") (r "^0.7") (d #t) (k 0)) (d (n "prost-build") (r "^0.7") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)) (d (n "tonic") (r "^0.4") (d #t) (k 0)) (d (n "tonic-build") (r "^0.4") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "1dkfdviby5whd7ky98iq9a7sv7hq29s45vn0qdl9p9aicwjhd4q1")))

