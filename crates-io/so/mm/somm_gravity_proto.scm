(define-module (crates-io so mm somm_gravity_proto) #:use-module (crates-io))

(define-public crate-somm_gravity_proto-0.1.0 (c (n "somm_gravity_proto") (v "0.1.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "cosmos-sdk-proto") (r "^0.6.0") (d #t) (k 0)) (d (n "prost") (r "^0.7") (d #t) (k 0)) (d (n "prost-types") (r "^0.7") (d #t) (k 0)) (d (n "tonic") (r "^0.4") (d #t) (k 0)))) (h "06rashwhjv367cb07fkvxvdk8sj3g62sgr3p2m3dd2if77d8d3k4")))

(define-public crate-somm_gravity_proto-0.1.1 (c (n "somm_gravity_proto") (v "0.1.1") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "cosmos-sdk-proto") (r "^0.6.1") (d #t) (k 0)) (d (n "prost") (r "^0.7") (d #t) (k 0)) (d (n "prost-types") (r "^0.7") (d #t) (k 0)) (d (n "tonic") (r "^0.4") (d #t) (k 0)))) (h "1xi8xs4sfqkjqfvsq1nmrdmrdp4rdzjs6f56spfbswifnp7blgz5")))

(define-public crate-somm_gravity_proto-0.1.2 (c (n "somm_gravity_proto") (v "0.1.2") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "cosmos-sdk-proto") (r "^0.5.0") (d #t) (k 0)) (d (n "prost") (r "^0.7") (d #t) (k 0)) (d (n "prost-types") (r "^0.7") (d #t) (k 0)) (d (n "tonic") (r "^0.4") (d #t) (k 0)))) (h "139mgm30lf7qxklhmc3r4hfkq97j0nxiqsv1y80439fwwvq1h2pb")))

(define-public crate-somm_gravity_proto-0.1.3 (c (n "somm_gravity_proto") (v "0.1.3") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "cosmos-sdk-proto") (r "^0.6.3") (d #t) (k 0)) (d (n "prost") (r "^0.7") (d #t) (k 0)) (d (n "prost-types") (r "^0.7") (d #t) (k 0)) (d (n "tonic") (r "^0.4") (d #t) (k 0)))) (h "12whd891v3xvl3vr3c7d80q047dcacxrjrf4ip95i6hnlfrxkd56")))

