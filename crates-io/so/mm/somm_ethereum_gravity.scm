(define-module (crates-io so mm somm_ethereum_gravity) #:use-module (crates-io))

(define-public crate-somm_ethereum_gravity-0.1.0 (c (n "somm_ethereum_gravity") (v "0.1.0") (d (list (d (n "clarity") (r "^0.4.11") (d #t) (k 0)) (d (n "deep_space") (r "^2.4.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num256") (r "^0.3") (d #t) (k 0)) (d (n "sha3") (r "^0.9") (d #t) (k 0)) (d (n "somm_gravity_utils") (r "^0.1.0") (d #t) (k 0)) (d (n "web30") (r "^0.14.4") (d #t) (k 0)))) (h "1mgsngp3pjfx8729y0fb93pd64r74m759n6frwkx879g9247h7mv")))

