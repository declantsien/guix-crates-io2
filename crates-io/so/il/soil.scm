(define-module (crates-io so il soil) #:use-module (crates-io))

(define-public crate-soil-0.1.0 (c (n "soil") (v "0.1.0") (h "0y7igkx4jzrl5qkxkd9sy6sd9b3xar0yxqbwg6cfv81c1fgl19mp")))

(define-public crate-soil-0.1.1 (c (n "soil") (v "0.1.1") (h "1pamnikdcn0ha4137xvzyswd82kx9mfmyr7r4f54wlk8wfyimv86")))

(define-public crate-soil-0.1.2 (c (n "soil") (v "0.1.2") (h "0mvgc69sb5s9903l8zawvr67zkpkxdalc4zagjls3kwnahnc21z1")))

(define-public crate-soil-0.1.3 (c (n "soil") (v "0.1.3") (h "0yvlwr30yfgacj69w7q0si4w73yrp5fjlshgf74671yk4vw6qbxw")))

