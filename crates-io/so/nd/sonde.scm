(define-module (crates-io so nd sonde) #:use-module (crates-io))

(define-public crate-sonde-0.1.0 (c (n "sonde") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)) (d (n "nom") (r "^6.1") (d #t) (k 0)) (d (n "tempfile") (r "^3.2") (d #t) (k 0)))) (h "1hqn8vjdjvppabydsv2nbl0yc5fdjz38ww3lkw7vwpc7mgpgrcdq")))

(define-public crate-sonde-0.1.1 (c (n "sonde") (v "0.1.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)) (d (n "nom") (r "^6.1") (d #t) (k 0)) (d (n "tempfile") (r "^3.2") (d #t) (k 0)))) (h "1pvg7na8xlx252lic3xvzdd6x46qjnyr3rndgi3rf35db966mgx8")))

