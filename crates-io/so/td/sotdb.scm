(define-module (crates-io so td sotdb) #:use-module (crates-io))

(define-public crate-sotdb-0.1.0 (c (n "sotdb") (v "0.1.0") (h "14lkg0k8vf145y300q557cqkqd2w1rk0wf9571k0kkw57achn3qj") (y #t)))

(define-public crate-sotdb-0.1.1 (c (n "sotdb") (v "0.1.1") (h "0czpnx30cs8d486xj359zv53l5iaw1k75fa0c9idc045yh12c7x6") (y #t)))

(define-public crate-sotdb-0.1.2 (c (n "sotdb") (v "0.1.2") (h "1b3bivvwhy5fv5ldc60dbanw5mh9mm0l01jjsyzj2708n65afjii") (y #t)))

(define-public crate-sotdb-0.1.3 (c (n "sotdb") (v "0.1.3") (h "0swimb2ncq9f78iqxpq24ii9vmvgqz15rzzllh1rnads0xb2dzag") (y #t)))

(define-public crate-sotdb-0.1.4 (c (n "sotdb") (v "0.1.4") (h "0z01y2hxbf9vrfp0ck5kxi4srsnsnrpjndfdpy4q6q4x1qvnglkp")))

(define-public crate-sotdb-0.1.5 (c (n "sotdb") (v "0.1.5") (h "14d00l2j3r1psp586rqx3aayywb27p0rbb9y7xgqksdrh8jvnm4l")))

