(define-module (crates-io so a_ soa_derive) #:use-module (crates-io))

(define-public crate-soa_derive-0.1.0 (c (n "soa_derive") (v "0.1.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.10") (d #t) (k 0)))) (h "1g51d52bxilpvzlamv8jc69hahd1pxr88cn1x5mpb0dvn6d64vka")))

(define-public crate-soa_derive-0.1.1 (c (n "soa_derive") (v "0.1.1") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.10") (d #t) (k 0)))) (h "038dmgpdaa7gxck1zh2x2w1546rl880amysba375w2h54yf8d3g8")))

(define-public crate-soa_derive-0.2.0 (c (n "soa_derive") (v "0.2.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.10") (d #t) (k 0)))) (h "0dxarsqjyxvrrd4wadx4zkv2zdy36cc8fjdnvz2wn2gbahzd6rsr")))

(define-public crate-soa_derive-0.3.0 (c (n "soa_derive") (v "0.3.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.10") (d #t) (k 0)))) (h "1iqfdh3xx0a7ajr1qcnpihkpwic86vpxik1apv7dv3x5zaa909pr")))

(define-public crate-soa_derive-0.3.1 (c (n "soa_derive") (v "0.3.1") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.10") (d #t) (k 0)))) (h "0vbwyfxfbvcgglx191q0nxg65g3n435r4cx5a2vz8qhck7dxpq70")))

(define-public crate-soa_derive-0.4.0 (c (n "soa_derive") (v "0.4.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "case") (r "^0.1") (d #t) (k 0)) (d (n "permutohedron") (r "^0.2") (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.10") (d #t) (k 0)))) (h "1x3bqp5b9ffbxixi2y4pyhhrff016l3ccm6mvj02wz5zhi9qwk7d")))

(define-public crate-soa_derive-0.4.1 (c (n "soa_derive") (v "0.4.1") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "case") (r "^0.1") (d #t) (k 0)) (d (n "permutohedron") (r "^0.2") (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.10") (d #t) (k 0)))) (h "133l3x78aq0pcc00s13acsi4dx471kz4xr9y0ybqdpf79axk86g9")))

(define-public crate-soa_derive-0.5.0 (c (n "soa_derive") (v "0.5.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "soa_derive_internal") (r "^0.5") (d #t) (k 0)))) (h "0l2vpbx9l2ra5mqs62vijaj5g2xdhb6cpihnhwcn2krz341dyf2j")))

(define-public crate-soa_derive-0.6.0 (c (n "soa_derive") (v "0.6.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "soa_derive_internal") (r "^0.5") (d #t) (k 0)))) (h "0wcwryyynrifnpxn0wx6kcsb1dclhn6ivfwdzimrcdkx7bskqn3j")))

(define-public crate-soa_derive-0.6.1 (c (n "soa_derive") (v "0.6.1") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "soa_derive_internal") (r "^0.5") (d #t) (k 0)))) (h "040yl4bq1x4dzmsjyq3j5w65f81dnsakad45kfvfj3pmxf7s1204")))

(define-public crate-soa_derive-0.6.2 (c (n "soa_derive") (v "0.6.2") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "soa_derive_internal") (r "^0.5") (d #t) (k 0)))) (h "1dnmv0qhdglkap7bcvr5pfy7k4v01i7cksr7vzmk6n29ddk1alvn")))

(define-public crate-soa_derive-0.7.0 (c (n "soa_derive") (v "0.7.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "soa_derive_internal") (r "^0.6") (d #t) (k 0)))) (h "0l87xpp5lpiknn9wq9shvfff1xl5ldzibvbzhbd3lf0amypkh3cr")))

(define-public crate-soa_derive-0.8.0 (c (n "soa_derive") (v "0.8.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "soa_derive_internal") (r "^0.8") (d #t) (k 0)))) (h "1xlcjhbchf8mjhwfjpnv444anrwxs7d035kfj5qnp0z9v8b0n8lz")))

(define-public crate-soa_derive-0.8.1 (c (n "soa_derive") (v "0.8.1") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "soa_derive_internal") (r "^0.9") (d #t) (k 0)))) (h "0gmzai3kpr8bh6bv1m4c3w4440vhd7xqgs4wh0smc5ishxlsgapb")))

(define-public crate-soa_derive-0.10.0 (c (n "soa_derive") (v "0.10.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "soa_derive_internal") (r "^0.10") (d #t) (k 0)))) (h "1xshqh1bzyby9plzfkidz98a6mpf2a43fx9w0jsapci918zjcc4m")))

(define-public crate-soa_derive-0.11.0 (c (n "soa_derive") (v "0.11.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "soa_derive_internal") (r "^0.11") (d #t) (k 0)))) (h "1s0cipsk7wls1pc9g68wg0w624xr4im7wg69q0rc64hicdrx6vc0")))

(define-public crate-soa_derive-0.12.0 (c (n "soa_derive") (v "0.12.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "soa_derive_internal") (r "^0.12") (d #t) (k 0)))) (h "08lkwgxyjr14sadcxs60fgi1l62dd2gv245ahq1rw6naaj1qjyrj")))

(define-public crate-soa_derive-0.13.0 (c (n "soa_derive") (v "0.13.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "permutation") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "soa_derive_internal") (r "^0.13") (d #t) (k 0)))) (h "0mxs57jf220rmwq7aa4n65ydbqrkwv6arpklhsm6nzaybfg1aqpy") (r "1.63")))

