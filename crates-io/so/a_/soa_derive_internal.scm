(define-module (crates-io so a_ soa_derive_internal) #:use-module (crates-io))

(define-public crate-soa_derive_internal-0.5.0 (c (n "soa_derive_internal") (v "0.5.0") (d (list (d (n "case") (r "^0.1") (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.10") (d #t) (k 0)))) (h "1kixnln97pkdphig85cpniijpl7ivxpx545ixsyi3qa8hbswyq3z")))

(define-public crate-soa_derive_internal-0.5.1 (c (n "soa_derive_internal") (v "0.5.1") (d (list (d (n "quote") (r "^0.5") (d #t) (k 0)) (d (n "syn") (r "^0.13") (d #t) (k 0)))) (h "1dqfvksq8npqsbap52cccvbyg0n05ri1kv14lavaqhnys8b4lqql")))

(define-public crate-soa_derive_internal-0.5.2 (c (n "soa_derive_internal") (v "0.5.2") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.14") (d #t) (k 0)))) (h "1vnjma3dkksjbygvx9jhpmqv2j83plrkphs36rvl7v8yqhw0ssa6")))

(define-public crate-soa_derive_internal-0.6.0 (c (n "soa_derive_internal") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.14") (d #t) (k 0)))) (h "1v0dv1x3ns7qijh63yxnqma4hrfqgji39qz8wjfs8bgbmcxp6973")))

(define-public crate-soa_derive_internal-0.6.1 (c (n "soa_derive_internal") (v "0.6.1") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.14") (d #t) (k 0)))) (h "1h1f9hq45hani7wmgahqvnxcc32b9jqhabl0r7f6h26w0wmwkqmm")))

(define-public crate-soa_derive_internal-0.6.2 (c (n "soa_derive_internal") (v "0.6.2") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "1y8jlp3pk7lkdanrlgzvfznv53pm12cy3clwmb3yr4gzb0g6gabb")))

(define-public crate-soa_derive_internal-0.6.3 (c (n "soa_derive_internal") (v "0.6.3") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "04yw75c43lm48rlgc3wmgjfyznmirn0xv7jrbj5nna3hk3hk3yqc")))

(define-public crate-soa_derive_internal-0.8.0 (c (n "soa_derive_internal") (v "0.8.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "002b41q6n6ifbbkpdkl62ciyr74i42g07pcg21bh169mx2m4xlbq")))

(define-public crate-soa_derive_internal-0.9.0 (c (n "soa_derive_internal") (v "0.9.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0pnkm8c047s56inhj91mbahfbqysk8g3cg04yc4c90q83lkrspyd")))

(define-public crate-soa_derive_internal-0.9.1 (c (n "soa_derive_internal") (v "0.9.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1s8vcd0p240cvyayg6g6wnr8yxhw5wrvn842g7667hkck7jph5ij")))

(define-public crate-soa_derive_internal-0.9.2 (c (n "soa_derive_internal") (v "0.9.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1mrvb0yz4a1jd114hjw8s1xsjzvpjvmx8b6a28qggrkpjnm0kvwy") (y #t)))

(define-public crate-soa_derive_internal-0.10.0 (c (n "soa_derive_internal") (v "0.10.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "03x7mrdhqzakhy5802ylazyf3yyjwag9sv0znd5iamhnw129mp02")))

(define-public crate-soa_derive_internal-0.10.1 (c (n "soa_derive_internal") (v "0.10.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1damgwwamd0y1h52rckg8zmjnvlpsm4s2bgq8syrb7rcsdmq0ix5")))

(define-public crate-soa_derive_internal-0.11.0 (c (n "soa_derive_internal") (v "0.11.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "01qfljh232gcy3j7rh6rl964jmsl8bs2706nghk6wfsbhhc1liay")))

(define-public crate-soa_derive_internal-0.11.1 (c (n "soa_derive_internal") (v "0.11.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1zr786ywisay9z1g8x3g13xmnkvds8cwsh79g66np3zr6lq9q15q") (y #t)))

(define-public crate-soa_derive_internal-0.12.0 (c (n "soa_derive_internal") (v "0.12.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1a9man7qqyxnqbj9055pvr91d9ajd0x4dyjlcwyp6mjiscysmjvv")))

(define-public crate-soa_derive_internal-0.13.0 (c (n "soa_derive_internal") (v "0.13.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("derive" "extra-traits"))) (d #t) (k 0)))) (h "1ffvaa2d49w59dipsniw2kfmgwzarxdjfyjlskbjxabisadfyr0g") (r "1.63")))

(define-public crate-soa_derive_internal-0.13.1 (c (n "soa_derive_internal") (v "0.13.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("derive" "extra-traits"))) (d #t) (k 0)))) (h "1p2jhkx893mv4af50qg33g8pshlwg3sr9xz4yb85ancq7g2gcpdd") (r "1.63")))

(define-public crate-soa_derive_internal-0.13.2 (c (n "soa_derive_internal") (v "0.13.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("derive" "extra-traits"))) (d #t) (k 0)))) (h "0y1nlc9fka3f8j5z0lrv4w5nq53smn7asnsmf557014f2my9pwm5") (r "1.63")))

