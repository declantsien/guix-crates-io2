(define-module (crates-io so lc solc-select) #:use-module (crates-io))

(define-public crate-solc-select-0.1.0 (c (n "solc-select") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.3") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sys-info") (r "^0.9.0") (d #t) (k 0)))) (h "12ilxakmikh56cqkjic0fpzl0i0d39wfm45vrx3q0rc4sjwwgbm0")))

(define-public crate-solc-select-0.2.0 (c (n "solc-select") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.3") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sys-info") (r "^0.9.0") (d #t) (k 0)))) (h "06gv9icq9858i2vipq3n9fvvz67x8mfzyh7fpws81ksw10wxj5ly")))

(define-public crate-solc-select-0.4.0 (c (n "solc-select") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^4.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sys-info") (r "^0.9.1") (d #t) (k 0)) (d (n "ureq") (r "^2.6.2") (f (quote ("json"))) (d #t) (k 0)))) (h "0vks3cwd4lpg8ncjrvy11m4a802dz1ncqqmfn7qgcavi7wd35a28")))

