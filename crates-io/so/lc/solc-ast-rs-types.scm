(define-module (crates-io so lc solc-ast-rs-types) #:use-module (crates-io))

(define-public crate-solc-ast-rs-types-0.1.0 (c (n "solc-ast-rs-types") (v "0.1.0") (d (list (d (n "regress") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1j57jfdzaknnwil69mvnvyvlbyhdf1zzi5nhl5fz4raww4pi3hja") (f (quote (("visit-mut") ("visit") ("default" "visit" "visit-mut"))))))

(define-public crate-solc-ast-rs-types-0.1.1 (c (n "solc-ast-rs-types") (v "0.1.1") (d (list (d (n "regress") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1yp2h0iwccm3yhwqjssrpbnwfbiqww0mr1xc2gl4308lnqgqdnb8") (f (quote (("visit") ("default" "visit"))))))

(define-public crate-solc-ast-rs-types-0.1.2 (c (n "solc-ast-rs-types") (v "0.1.2") (d (list (d (n "regress") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "12p17j191xa46qmrf4r7y9br5z53984wvi1jxva3sigpnly4x7ch") (f (quote (("visit") ("default" "visit"))))))

(define-public crate-solc-ast-rs-types-0.1.3 (c (n "solc-ast-rs-types") (v "0.1.3") (d (list (d (n "regress") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0giqndq3rw4lxr95ndx3hkw88h839ndcfyg7j1sjc1k8gs7xj5gw") (f (quote (("visit") ("default" "visit"))))))

(define-public crate-solc-ast-rs-types-0.1.4 (c (n "solc-ast-rs-types") (v "0.1.4") (d (list (d (n "regress") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0j0ra3vl73sszp38f1iyvn62lgavbc9dl5asd489i0f225kk5q0y") (f (quote (("visit") ("default" "visit"))))))

(define-public crate-solc-ast-rs-types-0.1.5 (c (n "solc-ast-rs-types") (v "0.1.5") (d (list (d (n "regress") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1mj30jzw1272cn270fls9n81aqzsj001n69kz2w71v9qvp5p7spl") (f (quote (("visit") ("default" "visit"))))))

(define-public crate-solc-ast-rs-types-0.1.6 (c (n "solc-ast-rs-types") (v "0.1.6") (d (list (d (n "regress") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "03l77z42sq8wqfk7xi7irirmmzi31s3y8k6m6smz01sj4jv2lwkm") (f (quote (("visit") ("default" "visit"))))))

