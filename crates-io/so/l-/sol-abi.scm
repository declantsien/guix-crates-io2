(define-module (crates-io so l- sol-abi) #:use-module (crates-io))

(define-public crate-sol-abi-0.0.0 (c (n "sol-abi") (v "0.0.0") (h "1dcysnrf6nzqz0apgi12zryfmlqg0spswcv2cn2dday31lrk3ma3")))

(define-public crate-sol-abi-0.0.1 (c (n "sol-abi") (v "0.0.1") (d (list (d (n "quote") (r "^1.0.33") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (o #t) (k 0)) (d (n "syn") (r "^2.0.41") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "15awkcbcih3c81lf828j27n2g9ymarj0qscpkjwyyzxj9h1hcncn") (f (quote (("std" "serde/std") ("default" "serde" "syn")))) (s 2) (e (quote (("syn" "dep:syn" "quote" "std"))))))

