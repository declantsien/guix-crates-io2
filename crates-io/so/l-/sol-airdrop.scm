(define-module (crates-io so l- sol-airdrop) #:use-module (crates-io))

(define-public crate-sol-airdrop-0.2.0 (c (n "sol-airdrop") (v "0.2.0") (d (list (d (n "anchor-lang") (r "^0.26.0") (d #t) (k 0)) (d (n "anchor-spl") (r "^0.26.0") (d #t) (k 0)) (d (n "solana-program") (r "^1.13.5") (d #t) (k 0)))) (h "1jikvc5pcgmaxz5hsz1js75fc6zgn8x5pbd6g1y88fhcjfan4h49") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

