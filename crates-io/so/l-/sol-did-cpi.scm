(define-module (crates-io so l- sol-did-cpi) #:use-module (crates-io))

(define-public crate-sol-did-cpi-0.0.1 (c (n "sol-did-cpi") (v "0.0.1") (d (list (d (n "anchor-gen") (r "^0.3.1") (d #t) (k 0)) (d (n "anchor-lang") (r ">=0.20") (d #t) (k 0)))) (h "0sx9risdhq1pi58410wmg6k7y9w5fmz3v284128d8kh2my4l258i") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default" "cpi") ("cpi" "no-entrypoint"))))))

