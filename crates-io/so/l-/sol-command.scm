(define-module (crates-io so l- sol-command) #:use-module (crates-io))

(define-public crate-sol-command-0.0.1 (c (n "sol-command") (v "0.0.1") (d (list (d (n "any-object-storage") (r "^0.1.0") (d #t) (k 0)) (d (n "bip39") (r "^2.0.0") (d #t) (k 0)) (d (n "lettre") (r "^0.11.6") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.18.1") (d #t) (k 0)))) (h "1gn5g58gqj14wck278ak3gx3kmy44mcspxy0x2mf2hgn061amy7q")))

