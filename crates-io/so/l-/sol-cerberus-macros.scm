(define-module (crates-io so l- sol-cerberus-macros) #:use-module (crates-io))

(define-public crate-sol-cerberus-macros-0.1.0 (c (n "sol-cerberus-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("full"))) (d #t) (k 0)))) (h "1vsmvkqnimphiy6rriq7sq9f4lprx7054zlaly86sn1h57gbd93z")))

(define-public crate-sol-cerberus-macros-0.1.1 (c (n "sol-cerberus-macros") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("full"))) (d #t) (k 0)))) (h "09kyzhjpvngfjncyd7akf4ydppr7y7japhk90crsddcw54zyrw6g")))

(define-public crate-sol-cerberus-macros-0.1.2 (c (n "sol-cerberus-macros") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("full"))) (d #t) (k 0)))) (h "0j2gsa8zalavvk42qvh1snfgw7rj57zafajjs5awl7ir5fysc110")))

(define-public crate-sol-cerberus-macros-0.1.5 (c (n "sol-cerberus-macros") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("full"))) (d #t) (k 0)))) (h "09krc28lc5xzjlpkaqvn4y0fdq743m68q0nspby8v9balhvb398j")))

(define-public crate-sol-cerberus-macros-0.1.7 (c (n "sol-cerberus-macros") (v "0.1.7") (d (list (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.22") (f (quote ("full"))) (d #t) (k 0)))) (h "1hcxr5406s744g3rbvzbl8qf0r37b69q13z8q1sdmxn1d41xqwnw")))

(define-public crate-sol-cerberus-macros-0.1.8 (c (n "sol-cerberus-macros") (v "0.1.8") (d (list (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.22") (f (quote ("full"))) (d #t) (k 0)))) (h "0im5aa1a8g5kyskkzpp1bgfjpgn7hqix4jpksq170r2ac6db321z")))

(define-public crate-sol-cerberus-macros-0.1.9 (c (n "sol-cerberus-macros") (v "0.1.9") (d (list (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.22") (f (quote ("full"))) (d #t) (k 0)))) (h "0caw10cjsfmisd53qk4lbb7xjx5cp31n1zqyd8qklrw5gyhab73y")))

(define-public crate-sol-cerberus-macros-0.1.10 (c (n "sol-cerberus-macros") (v "0.1.10") (d (list (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.22") (f (quote ("full"))) (d #t) (k 0)))) (h "0nkpc7lj3lv30b7pv5zwkbvv8x3wavhd1vrmhlszp83k93rzjava")))

