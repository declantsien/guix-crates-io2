(define-module (crates-io so l- sol-cli) #:use-module (crates-io))

(define-public crate-sol-cli-0.1.0 (c (n "sol-cli") (v "0.1.0") (d (list (d (n "bip39") (r "^2.0.0") (d #t) (k 0)) (d (n "sdkman-cli-native") (r "^0.5.1") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.18.1") (d #t) (k 0)))) (h "1anmm3k07d9g2kwcl83vcj39ivhha9b71x6dbgx1579jy35smh9s")))

(define-public crate-sol-cli-1.0.0 (c (n "sol-cli") (v "1.0.0") (d (list (d (n "any-object-storage") (r "^0.1.0") (d #t) (k 0)) (d (n "bip39") (r "^2.0.0") (d #t) (k 0)) (d (n "lettre") (r "^0.11.6") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.18.1") (d #t) (k 0)))) (h "1nksmrk948hjgv936ab71dabr4zvi9ax0ggp1d8gwnysq2xyqzvm")))

