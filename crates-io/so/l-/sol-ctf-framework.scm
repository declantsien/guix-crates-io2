(define-module (crates-io so l- sol-ctf-framework) #:use-module (crates-io))

(define-public crate-sol-ctf-framework-0.1.0 (c (n "sol-ctf-framework") (v "0.1.0") (d (list (d (n "anchor-client") (r "^0.23.0") (d #t) (k 2)) (d (n "poc-framework-osec") (r "^0.1.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "sha2") (r "^0.10.2") (d #t) (k 0)) (d (n "solana-program") (r "^1.8.14") (d #t) (k 2)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)) (d (n "threadpool") (r "^1.8.1") (d #t) (k 2)))) (h "0pf30hyz92fz6jp9i5wc3sqqy5cwnv01mi0r00sg83f7w94xc87h")))

