(define-module (crates-io so li solidity-metadata) #:use-module (crates-io))

(define-public crate-solidity-metadata-1.0.0 (c (n "solidity-metadata") (v "1.0.0") (d (list (d (n "blockscout-display-bytes") (r "^1.0") (d #t) (k 0)) (d (n "minicbor") (r "^0.18") (f (quote ("std"))) (d #t) (k 0)) (d (n "semver") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1wxzj4gcv7z797xpzcvk7w3l4b2940hhr8fvk1sci01g5isyxw0q")))

