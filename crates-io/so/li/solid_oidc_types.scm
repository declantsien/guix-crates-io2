(define-module (crates-io so li solid_oidc_types) #:use-module (crates-io))

(define-public crate-solid_oidc_types-0.1.0 (c (n "solid_oidc_types") (v "0.1.0") (d (list (d (n "dpop") (r "^0.1.1") (d #t) (k 0)) (d (n "gdp_rs") (r "^0.1.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "http_uri") (r "^1.0.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "picky") (r "^7.0.0-rc.8") (f (quote ("jose"))) (k 0)) (d (n "serde") (r "^1.0.167") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.100") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.43") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "webid") (r "^0.1.0") (f (quote ("serde"))) (d #t) (k 0)))) (h "03r5dv2xr6hdp2z02fqhdq5ms2wlbjdy887k54ly9f71iicpxcza")))

