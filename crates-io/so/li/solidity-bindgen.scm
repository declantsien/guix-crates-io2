(define-module (crates-io so li solidity-bindgen) #:use-module (crates-io))

(define-public crate-solidity-bindgen-0.1.0 (c (n "solidity-bindgen") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.50") (d #t) (k 0)) (d (n "ethabi") (r "^14.0.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.5") (d #t) (k 0)) (d (n "secp256k1") (r "^0.20.3") (d #t) (k 0)) (d (n "sodiumoxide") (r "^0.2.5") (d #t) (k 0)) (d (n "solidity-bindgen-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "web3") (r "^0.16.0") (d #t) (k 0)) (d (n "zeroize") (r "^1.1.0") (d #t) (k 0)))) (h "0ai71k1wcwa911qnh1cyl831r93xjpd09vf83srsww7mc4dbmvfj")))

