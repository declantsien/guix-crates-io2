(define-module (crates-io so li solitaire-core) #:use-module (crates-io))

(define-public crate-solitaire-core-0.0.1 (c (n "solitaire-core") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0.23") (d #t) (k 0)) (d (n "bincode") (r "^1.2.0") (d #t) (k 0)) (d (n "built") (r "^0.3.2") (d #t) (k 1)) (d (n "derive_more") (r "^0.99.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.102") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)) (d (n "uuid") (r "^0.8.1") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "05if4jspxfkhpvivha6djyldnpxi7r14k71m5ssasfxpg5pz4f06")))

