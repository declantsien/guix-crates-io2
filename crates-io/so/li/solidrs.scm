(define-module (crates-io so li solidrs) #:use-module (crates-io))

(define-public crate-solidrs-0.1.0 (c (n "solidrs") (v "0.1.0") (d (list (d (n "strum") (r "^0.26.1") (f (quote ("derive"))) (d #t) (k 0)))) (h "02p1h4m23fcyy0z5a4kr7ballb4hi28ny864s1mrkd4qhkjy3wc1")))

(define-public crate-solidrs-0.1.1 (c (n "solidrs") (v "0.1.1") (d (list (d (n "strum") (r "^0.26.1") (f (quote ("derive"))) (d #t) (k 0)))) (h "089mr2b851hnh4fhh38r6x3zx0did6y1wkrfkkh21b94x0vhkw9r")))

(define-public crate-solidrs-0.2.0 (c (n "solidrs") (v "0.2.0") (d (list (d (n "strum") (r "^0.26.1") (f (quote ("derive"))) (d #t) (k 0)))) (h "07zpvr56ay6qd8gdm25mpd5jpdibn9vhcsl3w9b3jcv03ridd31g")))

(define-public crate-solidrs-0.3.0 (c (n "solidrs") (v "0.3.0") (d (list (d (n "strum") (r "^0.26.1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0fyrv02cz72fjrndjwwgypsxhgqymwbx5s6j3dfrcfn5c1hxni96")))

(define-public crate-solidrs-0.3.1 (c (n "solidrs") (v "0.3.1") (h "06bc1lhg67w5pv46vcqiwyn7vl2xr8agxr1q5g1x6rpiggbws4a4")))

(define-public crate-solidrs-0.4.0 (c (n "solidrs") (v "0.4.0") (h "05c27m9h8wrxdv8x63lamibidiak65v3badch2wxc76zqjvznv24")))

