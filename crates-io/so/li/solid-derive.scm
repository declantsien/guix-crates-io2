(define-module (crates-io so li solid-derive) #:use-module (crates-io))

(define-public crate-solid-derive-0.1.0 (c (n "solid-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.8") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.16") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1dlx32dikyjiyjg6vv1bjvhqk0d9p45dfk5v71aj44nwli6s4218")))

(define-public crate-solid-derive-0.1.1 (c (n "solid-derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.8") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.16") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0cmgql78wlsh8pmlgzx8ss51x3vnncdq5x2hdvki619sv86z3wg3")))

(define-public crate-solid-derive-0.1.2 (c (n "solid-derive") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.8") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.16") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1zq43280ma0hvlhdgaxvgxadgcml1768ad4jsb0kpzdwgsrp8s2h")))

(define-public crate-solid-derive-0.1.3 (c (n "solid-derive") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0.8") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.16") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1dbc5q3b3xzrc57pii32plzgcibni46yr289iwxsbbas5m8ypgah")))

