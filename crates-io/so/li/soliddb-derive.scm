(define-module (crates-io so li soliddb-derive) #:use-module (crates-io))

(define-public crate-soliddb-derive-0.1.0 (c (n "soliddb-derive") (v "0.1.0") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0f5dil8gbs8cw330q6nrnrlz6xi1pcqrqqynwy8wjrj70iwynmlf")))

