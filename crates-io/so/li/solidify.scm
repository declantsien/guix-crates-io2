(define-module (crates-io so li solidify) #:use-module (crates-io))

(define-public crate-solidify-1.0.0 (c (n "solidify") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "edit-distance") (r "^2.1") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)))) (h "18qf30fpqnqjl3vcf2xhls4jy4d1x5gh34g223vmm4nn442zglzx")))

