(define-module (crates-io so li solitaire-assets) #:use-module (crates-io))

(define-public crate-solitaire-assets-0.0.1 (c (n "solitaire-assets") (v "0.0.1") (d (list (d (n "rust-embed") (r "^5.1.0") (f (quote ("interpolate-folder-path"))) (d #t) (k 0)))) (h "0zxm9ivfbsgvjd2bq8vpb7zdjz441gxgagv37p7bgzpy11g4agbk")))

