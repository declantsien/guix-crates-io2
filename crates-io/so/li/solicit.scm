(define-module (crates-io so li solicit) #:use-module (crates-io))

(define-public crate-solicit-0.0.1 (c (n "solicit") (v "0.0.1") (d (list (d (n "hpack") (r "*") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "openssl") (r "*") (f (quote ("tlsv1_2" "npn"))) (d #t) (k 0)))) (h "1jzjg9lwm6qlpam1fbfiyf722741d3lm0bz842lg87qng87c0ipg")))

(define-public crate-solicit-0.1.0 (c (n "solicit") (v "0.1.0") (d (list (d (n "hpack") (r "*") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "openssl") (r "*") (f (quote ("tlsv1_2" "npn"))) (d #t) (k 0)))) (h "14gaajbwaidlvp2i4hqayw076xx004am6jwq7smm8sn6dn374aq4") (f (quote (("live_tests"))))))

(define-public crate-solicit-0.2.0 (c (n "solicit") (v "0.2.0") (d (list (d (n "hpack") (r "*") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "openssl") (r "*") (f (quote ("tlsv1_2" "npn"))) (d #t) (k 0)))) (h "0c9b70f3416sgfank6a9fssfic9nj6irqnlxx71kq8vwh1spbzzp") (f (quote (("live_tests"))))))

(define-public crate-solicit-0.3.0 (c (n "solicit") (v "0.3.0") (d (list (d (n "hpack") (r "*") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "openssl") (r "*") (f (quote ("tlsv1_2" "npn"))) (o #t) (d #t) (k 0)))) (h "08cdb10mls4m7qgl1hfx47jwcq3faxc1gf0ccdddrcipy6ywn042") (f (quote (("tls" "openssl/tlsv1_2" "openssl/npn") ("live_tests"))))))

(define-public crate-solicit-0.4.0 (c (n "solicit") (v "0.4.0") (d (list (d (n "hpack") (r "*") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "openssl") (r "*") (f (quote ("tlsv1_2" "npn"))) (o #t) (d #t) (k 0)))) (h "0g7idgncidaxif9hwcs58j62bg6ybc5njmq6hxr6phfq1bivnnzs") (f (quote (("tls" "openssl/tlsv1_2" "openssl/npn") ("live_tests"))))))

(define-public crate-solicit-0.4.1 (c (n "solicit") (v "0.4.1") (d (list (d (n "hpack") (r "*") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "openssl") (r "*") (f (quote ("tlsv1_2" "npn"))) (o #t) (d #t) (k 0)))) (h "150y2cglnz80hyyzy3i44fz1zlyq04hr77zj64389p7z9ss9hnrf") (f (quote (("tls" "openssl" "openssl/tlsv1_2" "openssl/npn") ("live_tests"))))))

(define-public crate-solicit-0.4.2 (c (n "solicit") (v "0.4.2") (d (list (d (n "hpack") (r "*") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "openssl") (r "*") (f (quote ("tlsv1_2" "npn"))) (o #t) (d #t) (k 0)))) (h "0acwm26y7wizgr08bkjrk8rn9k204kahrd7kj9iwbn6ipw0czazc") (f (quote (("tls" "openssl" "openssl/tlsv1_2" "openssl/npn") ("live_tests"))))))

(define-public crate-solicit-0.4.3 (c (n "solicit") (v "0.4.3") (d (list (d (n "hpack") (r "*") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "openssl") (r "*") (f (quote ("tlsv1_2" "npn"))) (o #t) (d #t) (k 0)))) (h "0fcbmcw1jjfbvc5qd0rwbp1zl08im4rzwmrrvv20j6bqyx7mdz6s") (f (quote (("tls" "openssl" "openssl/tlsv1_2" "openssl/npn") ("live_tests"))))))

(define-public crate-solicit-0.4.4 (c (n "solicit") (v "0.4.4") (d (list (d (n "hpack") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "openssl") (r "*") (f (quote ("tlsv1_2" "npn"))) (o #t) (d #t) (k 0)))) (h "1ckmvnpqrsfw2x9y7dip5sa8ivzfz98b4ch7hkbqhia2r6x848qp") (f (quote (("tls" "openssl" "openssl/tlsv1_2" "openssl/npn") ("live_tests"))))))

