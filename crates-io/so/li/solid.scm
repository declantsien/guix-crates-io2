(define-module (crates-io so li solid) #:use-module (crates-io))

(define-public crate-solid-0.1.0 (c (n "solid") (v "0.1.0") (d (list (d (n "solid-core") (r "^0.1.0") (k 0)) (d (n "solid-derive") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "1s1knw8csqmbd56gzargpi8kr867lcn48rzkmagg0wbhqknnjgi5") (f (quote (("serde" "solid-core/derive") ("nightly" "solid-core/nightly") ("derive" "solid-derive") ("default" "serde" "derive") ("bigint" "solid-core/bigint"))))))

(define-public crate-solid-0.1.1 (c (n "solid") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.104") (d #t) (k 0)) (d (n "solid-core") (r "^0.1.0") (k 0)) (d (n "solid-derive") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "06qwdln4dx82m74bpzxdzdbp70xwc39f96l9mhrsvsxhpllq3nyq") (f (quote (("nightly" "solid-core/nightly") ("deser" "solid-core/derive") ("derive" "solid-derive") ("default" "deser" "derive") ("bigint" "solid-core/bigint"))))))

(define-public crate-solid-0.1.2 (c (n "solid") (v "0.1.2") (d (list (d (n "serde") (r "^1.0.104") (d #t) (k 0)) (d (n "solid-core") (r "^0.1.0") (k 0)) (d (n "solid-derive") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "1s6qawf8b7imr0da3yl4j61f2lsf67msnnx3jhs74ifm0cz5xw03") (f (quote (("nightly" "solid-core/nightly") ("deser" "solid-core/derive") ("derive" "solid-derive") ("default" "deser" "derive") ("bigint" "solid-core/bigint"))))))

(define-public crate-solid-0.1.3 (c (n "solid") (v "0.1.3") (d (list (d (n "serde") (r "^1.0.104") (d #t) (k 0)) (d (n "solid-core") (r "^0.1.0") (k 0)) (d (n "solid-derive") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "0qrgki42iy63sk127rq2wl3wz5a2n4mmdrpwq9w8l0v1z82a8jv7") (f (quote (("nightly" "solid-core/nightly") ("deser" "solid-core/derive") ("derive" "solid-derive") ("default" "deser" "derive") ("bigint" "solid-core/bigint"))))))

(define-public crate-solid-0.1.4 (c (n "solid") (v "0.1.4") (d (list (d (n "serde") (r "^1.0.104") (d #t) (k 0)) (d (n "solid-core") (r "^0.1.0") (k 0)) (d (n "solid-derive") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "0vzgwx2n6hcl8hg9li4fazc3q89p5ppf6f5iczmhr03lz2wwsn3r") (f (quote (("nightly" "solid-core/nightly") ("ethereum_types" "solid-core/eth_types") ("deser" "solid-core/derive") ("derive" "solid-derive") ("default" "deser" "derive") ("bigint" "solid-core/bigint"))))))

(define-public crate-solid-0.1.5 (c (n "solid") (v "0.1.5") (d (list (d (n "serde") (r "^1.0.104") (d #t) (k 0)) (d (n "solid-core") (r "^0.1.0") (k 0)) (d (n "solid-derive") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "1pd34xdj5c7kdpx2brw4ikxmfvc4567i83wnylc5sv55zb6ah0ig") (f (quote (("nightly" "solid-core/nightly") ("ethereum_types" "solid-core/eth_types") ("deser" "solid-core/derive") ("derive" "solid-derive") ("default" "deser" "derive") ("bigint" "solid-core/bigint"))))))

