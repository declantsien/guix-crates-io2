(define-module (crates-io so li solidity-bindgen-macros) #:use-module (crates-io))

(define-public crate-solidity-bindgen-macros-0.1.0 (c (n "solidity-bindgen-macros") (v "0.1.0") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "ethabi") (r "^14.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.10") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.106") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.51") (d #t) (k 0)) (d (n "syn") (r "^1.0.18") (d #t) (k 0)))) (h "1h3rg06k22fywsacxg1zifv9g3ma4hc08b9y3ck6vc8ipnn58cvz")))

