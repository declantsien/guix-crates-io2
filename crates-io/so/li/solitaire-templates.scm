(define-module (crates-io so li solitaire-templates) #:use-module (crates-io))

(define-public crate-solitaire-templates-0.0.1 (c (n "solitaire-templates") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0.23") (d #t) (k 0)) (d (n "maud") (r "^0.21.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.11.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.102") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solitaire-core") (r "^0.0.1") (d #t) (k 0)) (d (n "solitaire-service") (r "^0.0.1") (d #t) (k 0)) (d (n "uuid") (r "^0.8.1") (f (quote ("serde" "v4" "wasm-bindgen"))) (d #t) (k 0)))) (h "10xzp4f5dcz3pi4q08qx6x3vnyv4frv1lmmxn7ghmh15h4c95a0l")))

