(define-module (crates-io so li soliterm) #:use-module (crates-io))

(define-public crate-soliterm-0.1.0 (c (n "soliterm") (v "0.1.0") (d (list (d (n "actix") (r "^0.13.0") (d #t) (k 0)) (d (n "soliterm-game") (r "^0.1.0") (d #t) (k 0)) (d (n "soliterm-model") (r "^0.1.0") (d #t) (k 0)) (d (n "soliterm-ui") (r "^0.1.0") (d #t) (k 0)))) (h "0s30nq4jlsp03n5x38rfyjhphqgzkwjsai3d2dh5xcxfp4sg98df")))

