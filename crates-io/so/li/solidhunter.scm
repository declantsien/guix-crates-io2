(define-module (crates-io so li solidhunter) #:use-module (crates-io))

(define-public crate-solidhunter-0.0.2 (c (n "solidhunter") (v "0.0.2") (d (list (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "serde") (r "^1.0.149") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.89") (d #t) (k 0)) (d (n "solidhunter-lib") (r "^0.0.2") (d #t) (k 0)))) (h "05m2zbb3hyddayrnp33ndx5srjk2zqa7qa1ckpz5yx5bcjj3rv56")))

(define-public crate-solidhunter-0.1.1 (c (n "solidhunter") (v "0.1.1") (d (list (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "serde") (r "^1.0.149") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.89") (d #t) (k 0)) (d (n "solidhunter-lib") (r "^0.1.0") (d #t) (k 0)))) (h "1zprqz1pgn59rl7iivd0bkd33jyslpvh2finhf9lpz26yn354hf8")))

(define-public crate-solidhunter-0.2.1 (c (n "solidhunter") (v "0.2.1") (d (list (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "serde") (r "^1.0.149") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.89") (d #t) (k 0)) (d (n "solidhunter-lib") (r "^0.2.0") (d #t) (k 0)))) (h "0kyysby3zrj1pzx1ak7v8lcbrbi13nhh1y9q40lnkr6ypb655ki6")))

