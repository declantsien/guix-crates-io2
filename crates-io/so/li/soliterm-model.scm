(define-module (crates-io so li soliterm-model) #:use-module (crates-io))

(define-public crate-soliterm-model-0.1.0 (c (n "soliterm-model") (v "0.1.0") (d (list (d (n "assert2") (r "^0.3.6") (d #t) (k 2)) (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "enum-map") (r "^2.4.1") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "mockall") (r "^0.11.2") (d #t) (k 2)) (d (n "permutations") (r "^0.1.1") (f (quote ("random"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rstest") (r "^0.15.0") (d #t) (k 2)) (d (n "seq-macro") (r "^0.3.1") (d #t) (k 0)) (d (n "velcro") (r "^0.5.3") (d #t) (k 0)))) (h "05ww2bsq703x1mi8z1kk57207ydl50b7a91s02mfpqafjjii3mv9")))

