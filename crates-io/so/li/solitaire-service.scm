(define-module (crates-io so li solitaire-service) #:use-module (crates-io))

(define-public crate-solitaire-service-0.0.1 (c (n "solitaire-service") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0.23") (d #t) (k 0)) (d (n "chrono") (r "^0.4.9") (d #t) (k 0)) (d (n "serde") (r "^1.0.102") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)) (d (n "slog") (r "^2.5.2") (f (quote ("max_level_trace" "release_max_level_trace"))) (d #t) (k 0)) (d (n "solitaire-core") (r "^0.0.1") (d #t) (k 0)) (d (n "uuid") (r "^0.8.1") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "0s4j88dsvwha8xdi607vxfqjj66abzmfvhnaiyb9wkr595k63bqc")))

