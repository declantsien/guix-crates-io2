(define-module (crates-io so ur sourcenav) #:use-module (crates-io))

(define-public crate-sourcenav-0.1.0 (c (n "sourcenav") (v "0.1.0") (d (list (d (n "aabb-quadtree") (r "^0.2.0") (d #t) (k 0)) (d (n "bitbuffer") (r "^0.7.1") (d #t) (k 0)) (d (n "doc-comment") (r "^0.3.3") (d #t) (k 2)) (d (n "err-derive") (r "^0.2.4") (d #t) (k 0)) (d (n "euclid") (r "^0.19") (d #t) (k 0)))) (h "1zqd2cwqmm64j9ghya71s23m0z3zai0n9cbh1m7drcwfw3awhyc2")))

(define-public crate-sourcenav-0.1.1 (c (n "sourcenav") (v "0.1.1") (d (list (d (n "aabb-quadtree") (r "^0.2.0") (d #t) (k 0)) (d (n "bitbuffer") (r "^0.7.1") (d #t) (k 0)) (d (n "doc-comment") (r "^0.3.3") (d #t) (k 2)) (d (n "err-derive") (r "^0.2.4") (d #t) (k 0)) (d (n "euclid") (r "^0.19") (d #t) (k 0)))) (h "0pljkxnxnm3zi5hhv753y8n8k7smg4d5bm8l7645ihlpppjczc1k")))

(define-public crate-sourcenav-0.2.0 (c (n "sourcenav") (v "0.2.0") (d (list (d (n "aabb-quadtree") (r "^0.2.0") (d #t) (k 0)) (d (n "bitbuffer") (r "^0.7.1") (d #t) (k 0)) (d (n "doc-comment") (r "^0.3.3") (d #t) (k 2)) (d (n "err-derive") (r "^0.2.4") (d #t) (k 0)) (d (n "euclid") (r "^0.19") (d #t) (k 0)))) (h "1yp16pjbyv9j5mn2h5xy5b9dpvlq9bd2423y39jqi8457pckvy4j")))

