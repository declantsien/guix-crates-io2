(define-module (crates-io so ur source-span) #:use-module (crates-io))

(define-public crate-source-span-1.0.0 (c (n "source-span") (v "1.0.0") (d (list (d (n "terminal-escapes") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "utf8-decode") (r "^1.0.0") (d #t) (k 2)))) (h "1srczk7mk89vynk9jrkm0q5nm2wm2p8algjlaz8hapwrkw6xg4wc") (f (quote (("default" "colors") ("colors" "terminal-escapes"))))))

(define-public crate-source-span-1.1.0 (c (n "source-span") (v "1.1.0") (d (list (d (n "termion") (r "^1.5.3") (o #t) (d #t) (k 0)) (d (n "utf8-decode") (r "^1.0.0") (d #t) (k 2)))) (h "1p22yx427nhi62z4xvn92cga00rz8qp0pw6ywxcmrrjkdz0v9fhw") (f (quote (("default" "colors") ("colors" "termion"))))))

(define-public crate-source-span-1.2.0 (c (n "source-span") (v "1.2.0") (d (list (d (n "termion") (r "^1.5.3") (o #t) (d #t) (k 0)) (d (n "utf8-decode") (r "^1.0.0") (d #t) (k 2)))) (h "07hbkxp53kyshahl5xd5jyycqsi4ncjhqakap9qcm32gnx1acacb") (f (quote (("default" "colors") ("colors" "termion"))))))

(define-public crate-source-span-1.3.0 (c (n "source-span") (v "1.3.0") (d (list (d (n "termion") (r "^1.5.3") (o #t) (d #t) (k 0)) (d (n "utf8-decode") (r "^1.0.0") (d #t) (k 2)))) (h "07y6bcwx1fill9f6c16dn2s9albinpyj2xpkc7a923rnmfp225mq") (f (quote (("default" "colors") ("colors" "termion"))))))

(define-public crate-source-span-1.4.0 (c (n "source-span") (v "1.4.0") (d (list (d (n "termion") (r "^1.5.3") (o #t) (d #t) (k 0)) (d (n "utf8-decode") (r "^1.0.0") (d #t) (k 2)))) (h "07sawxq1g8mz75d0whzb835289nlagcggfxf7cipiz8a14anymrr") (f (quote (("default" "colors") ("colors" "termion"))))))

(define-public crate-source-span-2.0.0 (c (n "source-span") (v "2.0.0") (d (list (d (n "termion") (r "^1.5") (o #t) (d #t) (k 0)) (d (n "utf8-decode") (r "^1.0") (d #t) (k 2)))) (h "01fibr8bi72cvfirq1ssl45pa24fyfcplrnw47lm6nqzv4x17kci") (f (quote (("default" "colors") ("colors" "termion"))))))

(define-public crate-source-span-2.0.1 (c (n "source-span") (v "2.0.1") (d (list (d (n "termion") (r "^1.5") (o #t) (d #t) (k 0)) (d (n "utf8-decode") (r "^1.0") (d #t) (k 2)))) (h "0j1jdw5b0f7s4j1pm4f5r334g9q09qp23p0vy8l31zf732061292") (f (quote (("default" "colors") ("colors" "termion"))))))

(define-public crate-source-span-2.1.0 (c (n "source-span") (v "2.1.0") (d (list (d (n "termion") (r "^1.5") (o #t) (d #t) (k 0)) (d (n "utf8-decode") (r "^1.0") (d #t) (k 2)))) (h "02wz16df0264hjwznknzx5m4c5xlcqjr643srlv3fq28163pz2li") (f (quote (("default" "colors") ("colors" "termion"))))))

(define-public crate-source-span-2.1.1 (c (n "source-span") (v "2.1.1") (d (list (d (n "termion") (r "^1.5") (o #t) (d #t) (k 0)) (d (n "utf8-decode") (r "^1.0") (d #t) (k 2)))) (h "0f7g477j1y0hg3382svwnrrv6azh93nlzza8gad0s7np6csj7plz") (f (quote (("default" "colors") ("colors" "termion"))))))

(define-public crate-source-span-2.2.0 (c (n "source-span") (v "2.2.0") (d (list (d (n "termion") (r "^1.5") (o #t) (d #t) (k 0)) (d (n "utf8-decode") (r "^1.0") (d #t) (k 2)))) (h "0935hyfg7a4cx82hrsszd0kcxjgch18x6nzmyxkrx1a80dxnsd98") (f (quote (("default" "colors") ("colors" "termion"))))))

(define-public crate-source-span-2.3.0 (c (n "source-span") (v "2.3.0") (d (list (d (n "termion") (r "^1.5") (o #t) (d #t) (k 0)) (d (n "utf8-decode") (r "^1.0") (d #t) (k 2)))) (h "1d2fskkx9g39hxj6278mr9c6z6vm31p4c7x39pn1106lyf5wwh1w") (f (quote (("default" "colors") ("colors" "termion"))))))

(define-public crate-source-span-2.2.1 (c (n "source-span") (v "2.2.1") (d (list (d (n "termion") (r "^1.5") (o #t) (d #t) (k 0)) (d (n "utf8-decode") (r "^1.0") (d #t) (k 2)))) (h "1lg9s5hf5w9nnp1j22msddzmc0pj60hy9baik9y4h7bl4g1r4dsy") (f (quote (("default" "colors") ("colors" "termion"))))))

(define-public crate-source-span-2.4.0 (c (n "source-span") (v "2.4.0") (d (list (d (n "termion") (r "^1.5") (o #t) (d #t) (k 0)) (d (n "utf8-decode") (r "^1.0") (d #t) (k 2)))) (h "1yj91b3fsckxbrwvhn5z66219dhd3nnqh1v4ymn9qlr6z12rrl68") (f (quote (("default" "colors") ("colors" "termion"))))))

(define-public crate-source-span-2.5.0 (c (n "source-span") (v "2.5.0") (d (list (d (n "termion") (r "^1.5") (o #t) (d #t) (k 0)) (d (n "utf8-decode") (r "^1.0") (d #t) (k 2)))) (h "0jiz4brxdksr3psrrfxs5xg0vvja9kc5f1pv2lvgv59fk2jr7caj") (f (quote (("default" "colors") ("colors" "termion"))))))

(define-public crate-source-span-2.5.1 (c (n "source-span") (v "2.5.1") (d (list (d (n "termion") (r "^1.5") (o #t) (d #t) (k 0)) (d (n "utf8-decode") (r "^1.0") (d #t) (k 2)))) (h "0sskfnxn30l8va73ignhbszgag4zq6f52wxcmilbd0myk5dj6fj0") (f (quote (("default" "colors") ("colors" "termion"))))))

(define-public crate-source-span-2.6.0 (c (n "source-span") (v "2.6.0") (d (list (d (n "termion") (r "^1.5") (o #t) (d #t) (k 0)) (d (n "utf8-decode") (r "^1.0") (d #t) (k 2)))) (h "0dgfgzpj0m5pjcjl108czgfr6q6zlic638jhzdn0yyh26i7xdc5p") (f (quote (("default" "colors") ("colors" "termion"))))))

(define-public crate-source-span-2.7.0 (c (n "source-span") (v "2.7.0") (d (list (d (n "termion") (r "^1.5") (o #t) (d #t) (k 0)) (d (n "utf8-decode") (r "^1.0") (d #t) (k 2)))) (h "0g5a5kaciqh7p3c7z40ihpb0vq69j4icr4r13xrfiqhsga5ypbx3") (f (quote (("default" "colors") ("colors" "termion"))))))

