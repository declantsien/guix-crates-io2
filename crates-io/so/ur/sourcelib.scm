(define-module (crates-io so ur sourcelib) #:use-module (crates-io))

(define-public crate-sourcelib-0.1.0 (c (n "sourcelib") (v "0.1.0") (h "18ijzdhl1f17bw1plf5asics63dxbv0pr9ca5rnnd7cb3578y5vs") (y #t)))

(define-public crate-sourcelib-0.1.1 (c (n "sourcelib") (v "0.1.1") (h "1p1hnj095sdchrn6hl3kmisx73i7szb7s3gx846w2nzlg3x5vm5k") (y #t)))

