(define-module (crates-io so ur source-map-mappings) #:use-module (crates-io))

(define-public crate-source-map-mappings-0.1.0 (c (n "source-map-mappings") (v "0.1.0") (d (list (d (n "quickcheck") (r "^0.5.0") (d #t) (k 2)) (d (n "vlq") (r "^0.3") (d #t) (k 0)))) (h "1g4shbcgr4fcad5151fgxpg9pgyk1f121mpihvqvjbi3yllil9fd")))

(define-public crate-source-map-mappings-0.2.0 (c (n "source-map-mappings") (v "0.2.0") (d (list (d (n "quickcheck") (r "^0.5.0") (d #t) (k 2)) (d (n "vlq") (r "^0.3") (d #t) (k 0)))) (h "0sjmsqd18rlipgbmmxh8nkaraa5c4qjdxwrvfrysp87nv2m5x5rs")))

(define-public crate-source-map-mappings-0.3.0 (c (n "source-map-mappings") (v "0.3.0") (d (list (d (n "quickcheck") (r "^0.5.0") (d #t) (k 2)) (d (n "rand") (r "^0.4.1") (d #t) (k 0)) (d (n "vlq") (r "^0.5") (d #t) (k 0)))) (h "165xq8ph2p5c1b0h7vvmp4qngj6707kq0zhx5lird6zfncbhxz4v")))

(define-public crate-source-map-mappings-0.4.0 (c (n "source-map-mappings") (v "0.4.0") (d (list (d (n "quickcheck") (r "^0.5.0") (d #t) (k 2)) (d (n "rand") (r "^0.4.1") (d #t) (k 0)) (d (n "vlq") (r "^0.5.1") (d #t) (k 0)))) (h "18v74g3p2yva2g4dy2gf04030jllsgx1b3xfahjm5lw50sqsw39a")))

(define-public crate-source-map-mappings-0.5.0 (c (n "source-map-mappings") (v "0.5.0") (d (list (d (n "quickcheck") (r "^0.5.0") (d #t) (k 2)) (d (n "rand") (r "^0.4.1") (d #t) (k 0)) (d (n "vlq") (r "^0.5.1") (d #t) (k 0)))) (h "1ylbpxvr2fwxijm4lw594yv1aw4kd21j9pf560z8wqqzi6kbzfl9")))

