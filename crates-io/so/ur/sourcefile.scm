(define-module (crates-io so ur sourcefile) #:use-module (crates-io))

(define-public crate-sourcefile-0.1.0 (c (n "sourcefile") (v "0.1.0") (d (list (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "13rr757jdir8vjrnslp891kb0h9b8im7cypikc6pclysbdm1fig7")))

(define-public crate-sourcefile-0.1.1 (c (n "sourcefile") (v "0.1.1") (d (list (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1mlha414agijpgspy87lzsdhs86r9c9vbyrh2jz4gws1hp1j2747")))

(define-public crate-sourcefile-0.1.2 (c (n "sourcefile") (v "0.1.2") (d (list (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "035zrdmkjjvp3bdw2cyh1i8wi3g4949qa7614qs8wfbwnw5wimcc")))

(define-public crate-sourcefile-0.1.3 (c (n "sourcefile") (v "0.1.3") (d (list (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "0z558qq62bf75qaqkwndxzac0d41phsb5jkba3ydgv8iwcgkf000")))

(define-public crate-sourcefile-0.1.4 (c (n "sourcefile") (v "0.1.4") (d (list (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1lwa6973zs4bgj29my7agfjgk4nw9hp6j7dfnr13nid85fw7rxsb")))

(define-public crate-sourcefile-0.2.0 (c (n "sourcefile") (v "0.2.0") (d (list (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "0dmjdqbkshyklx227vzn18ckcpav0pi9k08xi08fgjykcqmhi9mr")))

(define-public crate-sourcefile-0.2.1 (c (n "sourcefile") (v "0.2.1") (d (list (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "11p9cvkx3k8rvrxkzxaf68h9w85n6v0w68mjdhninfrl435f0kk5")))

