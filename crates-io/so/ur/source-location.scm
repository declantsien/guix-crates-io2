(define-module (crates-io so ur source-location) #:use-module (crates-io))

(define-public crate-source-location-0.2.0 (c (n "source-location") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (d #t) (k 0)))) (h "00z6l8h7qkvpkkl5zi9i92l9f6515283x8gfyrmxi93chmyrp80s")))

(define-public crate-source-location-0.3.0 (c (n "source-location") (v "0.3.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (d #t) (k 0)))) (h "00zxc1mwgzvwyc4988qa87z654ra807czqagy542l0p81cxnsfai")))

