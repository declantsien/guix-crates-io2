(define-module (crates-io so ur source) #:use-module (crates-io))

(define-public crate-source-0.1.0 (c (n "source") (v "0.1.0") (d (list (d (n "utf8-chars") (r "^0.1") (d #t) (k 0)))) (h "1s9xd52pw5d0dmn50lq6ad66nxa5fhir600zd0g5x7b000a1avhw")))

(define-public crate-source-0.2.0 (c (n "source") (v "0.2.0") (d (list (d (n "utf8-chars") (r "^0.1") (d #t) (k 0)))) (h "0byk4nl7hyh9r2wkbs0kidmyj9sn5fsz6wdv0a15gjj12si7iih6")))

(define-public crate-source-0.2.1 (c (n "source") (v "0.2.1") (d (list (d (n "utf8-chars") (r "^0.1") (d #t) (k 0)))) (h "07yy6mpkd76aa8fcd7cr8v4p2c0f388dlbpy41yrhxs3dybw3sj5")))

