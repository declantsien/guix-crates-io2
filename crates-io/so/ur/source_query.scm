(define-module (crates-io so ur source_query) #:use-module (crates-io))

(define-public crate-source_query-0.1.0 (c (n "source_query") (v "0.1.0") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)))) (h "1d7c9qgibashg8sn68vhl19mkmlvzdiffm3kig54d5yi82zkldy5")))

(define-public crate-source_query-0.1.1 (c (n "source_query") (v "0.1.1") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)))) (h "1aaar6kdp77783wvgzv0h54rpsdw0pjcnj8fj59lh6cqjsj8zzcn")))

(define-public crate-source_query-0.1.2 (c (n "source_query") (v "0.1.2") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)))) (h "1fm5hy1zxw8jbrrn76xi31m30zgnfwb0giw9gndqrfvw9piwjl3v")))

(define-public crate-source_query-0.1.3 (c (n "source_query") (v "0.1.3") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)))) (h "0n9rjwhb802nyw9hjr1rdk5abypd6b07cv0347bx9gammgfz63sw")))

