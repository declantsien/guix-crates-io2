(define-module (crates-io so ur sourceannot) #:use-module (crates-io))

(define-public crate-sourceannot-0.1.0 (c (n "sourceannot") (v "0.1.0") (d (list (d (n "indoc") (r "^2.0.5") (d #t) (k 2)) (d (n "unicode-width") (r "^0.1.11") (d #t) (k 0)))) (h "1d297zpb57bwjsx52fspaz4zcryfhm9knl2d4yhnb03nw6kfdbwg") (r "1.74")))

(define-public crate-sourceannot-0.1.1 (c (n "sourceannot") (v "0.1.1") (d (list (d (n "indoc") (r "^2.0.5") (d #t) (k 2)) (d (n "unicode-width") (r "^0.1.11") (d #t) (k 0)))) (h "0rl12802k7wdnchygkp0kz7w1sq0218620qyjdazmjp1b7jhn1nd") (r "1.74")))

(define-public crate-sourceannot-0.2.0 (c (n "sourceannot") (v "0.2.0") (d (list (d (n "indoc") (r "^2.0.5") (d #t) (k 2)) (d (n "unicode-width") (r "^0.1.11") (d #t) (k 0)))) (h "148x810p0kc7zh4p50zi2qp0789l6bgg6qhlc5gjpn0rg8mv8sl0") (r "1.74")))

