(define-module (crates-io so ur source-cache) #:use-module (crates-io))

(define-public crate-source-cache-0.0.0 (c (n "source-cache") (v "0.0.0") (d (list (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "url") (r "^2.5.0") (d #t) (k 0)))) (h "0a6y5l7yb59aaczmsd8mpy1vp5rxvxj1f8nycailnd1m0jk68rp0") (f (quote (("default"))))))

(define-public crate-source-cache-0.1.0 (c (n "source-cache") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "url") (r "^2.5.0") (d #t) (k 0)))) (h "03sn6w1xw6656sc23ls98rj6vmkhhsmjp3fvb2raqvm7gzy5p0vm") (f (quote (("default"))))))

(define-public crate-source-cache-0.2.0 (c (n "source-cache") (v "0.2.0") (d (list (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "url") (r "^2.5.0") (d #t) (k 0)))) (h "1a1nd38886r9yjwm16zp6x2hngi50raainlvcy77dhrdcrmnyqj0") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde" "url/serde"))))))

(define-public crate-source-cache-0.2.1 (c (n "source-cache") (v "0.2.1") (d (list (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "url") (r "^2.5.0") (d #t) (k 0)))) (h "0dagcz4r09fvsbniil2afy9bxdy1811gbjhnw1vdlpp2y8r2qm70") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde" "url/serde"))))))

(define-public crate-source-cache-0.2.2 (c (n "source-cache") (v "0.2.2") (d (list (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "url") (r "^2.5.0") (d #t) (k 0)))) (h "0qlswvampa4836asfrk4q44b78y1l3v440syh6m0bb37idyv85hy") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde" "url/serde"))))))

(define-public crate-source-cache-0.2.3 (c (n "source-cache") (v "0.2.3") (d (list (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "url") (r "^2.5.0") (d #t) (k 0)))) (h "08ya6iy1kczy2kwpavv7s2wg9jqzxnfcrmh2i14a5g12x8m59bhb") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde" "url/serde"))))))

