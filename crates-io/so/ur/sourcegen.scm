(define-module (crates-io so ur sourcegen) #:use-module (crates-io))

(define-public crate-sourcegen-0.1.0 (c (n "sourcegen") (v "0.1.0") (h "022b4z5vym6ads6iwyj8cqvl0ls6k91k7ksgwlxqz2xlcr2qr6pv")))

(define-public crate-sourcegen-0.2.0 (c (n "sourcegen") (v "0.2.0") (h "0gppak8arb7ks2l1ij8463f5bbcbf44igrifrylc7a3b0isxglld")))

(define-public crate-sourcegen-0.3.0 (c (n "sourcegen") (v "0.3.0") (h "0b2bc22xm88lhiwbb2alkacvyfimqc9c5zhpjavn5sbyi0dd54fi")))

