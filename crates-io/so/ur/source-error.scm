(define-module (crates-io so ur source-error) #:use-module (crates-io))

(define-public crate-source-error-0.0.0 (c (n "source-error") (v "0.0.0") (d (list (d (n "colored") (r "^1.8") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)))) (h "1q676bd9q0a0cy67c6dl89icdwhw63qsbh8fg90wksr5mp0x7xw3")))

(define-public crate-source-error-0.1.0 (c (n "source-error") (v "0.1.0") (d (list (d (n "colored") (r "^1.8") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)))) (h "1lvq49z9lv29sxj1jr39sm2m5ixzxa1wvi05vdn45dc14dx530gi")))

