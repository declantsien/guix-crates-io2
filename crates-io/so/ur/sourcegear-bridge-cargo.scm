(define-module (crates-io so ur sourcegear-bridge-cargo) #:use-module (crates-io))

(define-public crate-sourcegear-bridge-cargo-0.5.0 (c (n "sourcegear-bridge-cargo") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "clap") (r "^4.2.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1zh9jv52696cpyqhiyf0nw0rd6w927n2s80vb5p37pil13ggk3v3")))

