(define-module (crates-io so ur sour) #:use-module (crates-io))

(define-public crate-sour-0.1.0 (c (n "sour") (v "0.1.0") (d (list (d (n "clap") (r "^4.1.1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0vzchx3hy4waq7dgq7gb8zybj7b05njyshn68bnpz27y85hig8il")))

(define-public crate-sour-0.2.0 (c (n "sour") (v "0.2.0") (d (list (d (n "clap") (r "^4.1.1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1v705q2qm3876v7rnyfjimsk6lkvjyxn1500bwd3cfca34103k2d")))

(define-public crate-sour-0.2.1 (c (n "sour") (v "0.2.1") (d (list (d (n "clap") (r "^4.1.1") (f (quote ("derive"))) (d #t) (k 0)))) (h "02zjpjwd5xn6psvw3wbqfrczvjsdnxnqvkrghy19lw4rawwh7fva")))

(define-public crate-sour-0.2.2 (c (n "sour") (v "0.2.2") (d (list (d (n "clap") (r "^4.1.1") (f (quote ("derive"))) (d #t) (k 0)))) (h "06araginr5xr00pwh6phxn1pv08krph35ki3wavvsc2hznix6l55")))

(define-public crate-sour-0.2.3 (c (n "sour") (v "0.2.3") (d (list (d (n "clap") (r "^4.1.1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1851ij3gi7q0d1kby5009vb0cwkvm1bbalfcb9mxmfcafdr0x472")))

(define-public crate-sour-0.2.4 (c (n "sour") (v "0.2.4") (d (list (d (n "clap") (r "^4.1.1") (f (quote ("derive"))) (d #t) (k 0)))) (h "129k9y89k9rdv1chlyd2j6is5nwan3y9w21ynjb27llkwpia3f19")))

(define-public crate-sour-0.2.5 (c (n "sour") (v "0.2.5") (d (list (d (n "clap") (r "^4.1.1") (f (quote ("derive"))) (d #t) (k 0)))) (h "01kkg1zsa4jzcyanr0g2h60622j14wna5qr7195d9mq5cy6gvja1")))

