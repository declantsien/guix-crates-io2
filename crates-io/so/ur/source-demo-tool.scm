(define-module (crates-io so ur source-demo-tool) #:use-module (crates-io))

(define-public crate-source-demo-tool-0.2.0 (c (n "source-demo-tool") (v "0.2.0") (d (list (d (n "async-channel") (r "^1.8.0") (d #t) (k 0)) (d (n "buf_redux") (r "^0.8.4") (d #t) (k 0)) (d (n "source-demo-tool-impl-proc-macros") (r "^0.2.0") (d #t) (k 0)))) (h "12jgi45y15sr33j39l3qp5k9gcnjm9zx33nc0fhrfhg692mkhl59")))

(define-public crate-source-demo-tool-0.2.1 (c (n "source-demo-tool") (v "0.2.1") (d (list (d (n "async-channel") (r "^1.8.0") (d #t) (k 0)) (d (n "buf_redux") (r "^0.8.4") (d #t) (k 0)) (d (n "source-demo-tool-impl-proc-macros") (r "^0.2.0") (d #t) (k 0)))) (h "0nddi6nbpmwl7fg07mx6as275p7z7lak2l0m5s62dfxja7d7j5fy")))

(define-public crate-source-demo-tool-0.2.2 (c (n "source-demo-tool") (v "0.2.2") (d (list (d (n "async-channel") (r "^1.8.0") (d #t) (k 0)) (d (n "buf_redux") (r "^0.8.4") (d #t) (k 0)) (d (n "source-demo-tool-impl-proc-macros") (r "^0.2.0") (d #t) (k 0)))) (h "0pwl18hi7p2srspm2w4w0a6j11xm7il40zmskg25w8bykzrxmvr4")))

(define-public crate-source-demo-tool-0.3.0 (c (n "source-demo-tool") (v "0.3.0") (d (list (d (n "async-channel") (r "^1.8.0") (d #t) (k 0)) (d (n "buf_redux") (r "^0.8.4") (d #t) (k 0)) (d (n "source-demo-tool-impl-proc-macros") (r "^0.3.0") (d #t) (k 0)))) (h "16wsgnni0kwbi1y823w105zdpg3x5mnpadj5b312gnh4z89qg3sh")))

(define-public crate-source-demo-tool-0.4.0 (c (n "source-demo-tool") (v "0.4.0") (d (list (d (n "async-channel") (r "^1.8.0") (d #t) (k 0)) (d (n "buf_redux") (r "^0.8.4") (d #t) (k 0)) (d (n "source-demo-tool-impl-proc-macros") (r "^0.3.0") (d #t) (k 0)))) (h "1g7vi66pyvrcxbzkpfywrrnaasd56d3x2975n3ac5cbrj0ff7d6b") (y #t)))

(define-public crate-source-demo-tool-0.4.1 (c (n "source-demo-tool") (v "0.4.1") (d (list (d (n "async-channel") (r "^1.8.0") (d #t) (k 0)) (d (n "buf_redux") (r "^0.8.4") (d #t) (k 0)) (d (n "source-demo-tool-impl-proc-macros") (r "^0.3.0") (d #t) (k 0)))) (h "1dmjndf609s6dmcr2kfb90xvxsj0jzf0z738x0a0q44qfdc1b8bi")))

(define-public crate-source-demo-tool-0.4.2 (c (n "source-demo-tool") (v "0.4.2") (d (list (d (n "async-channel") (r "^1.8.0") (d #t) (k 0)) (d (n "buf_redux") (r "^0.8.4") (d #t) (k 0)) (d (n "source-demo-tool-impl-proc-macros") (r "^0.3.0") (d #t) (k 0)))) (h "1cg7kwghhyh38amjva8j3s17av84mja8x708x7jm9lvq0016ffxl")))

(define-public crate-source-demo-tool-0.5.0 (c (n "source-demo-tool") (v "0.5.0") (d (list (d (n "async-channel") (r "^1.8.0") (d #t) (k 0)) (d (n "buf_redux") (r "^0.8.4") (d #t) (k 0)) (d (n "source-demo-tool-impl-proc-macros") (r "^0.3.0") (d #t) (k 0)))) (h "1i55519mjpf0m74nsic6j4vgml7vmd5x593wqapggn9al67d7l15")))

(define-public crate-source-demo-tool-0.6.0 (c (n "source-demo-tool") (v "0.6.0") (d (list (d (n "async-channel") (r "^1.8.0") (d #t) (k 0)) (d (n "buf_redux") (r "^0.8.4") (d #t) (k 0)) (d (n "source-demo-tool-impl-proc-macros") (r "^0.3.0") (d #t) (k 0)))) (h "15bv6rvwfpv8vzfb2jb7pszq74dnrmd7w7alj06khxl1pwcgnri1")))

(define-public crate-source-demo-tool-0.6.1 (c (n "source-demo-tool") (v "0.6.1") (d (list (d (n "async-channel") (r "^1.8.0") (d #t) (k 0)) (d (n "buf_redux") (r "^0.8.4") (d #t) (k 0)) (d (n "source-demo-tool-impl-proc-macros") (r "^0.3.1") (d #t) (k 0)))) (h "0k19rrayn21nfdc76kjh4m3gnbqij17v2k9kxs07h20qpflgwvr1")))

(define-public crate-source-demo-tool-0.6.2 (c (n "source-demo-tool") (v "0.6.2") (d (list (d (n "async-channel") (r "^1.8.0") (d #t) (k 0)) (d (n "buf_redux") (r "^0.8.4") (d #t) (k 0)) (d (n "source-demo-tool-impl-proc-macros") (r "^0.3.2") (d #t) (k 0)))) (h "1pk5gygzj1mxk8m5sk46vckgcz57q15fkv4k567nwpf9kga8pq5w")))

(define-public crate-source-demo-tool-0.6.3 (c (n "source-demo-tool") (v "0.6.3") (d (list (d (n "async-channel") (r "^1.8.0") (d #t) (k 0)) (d (n "buf_redux") (r "^0.8.4") (d #t) (k 0)) (d (n "source-demo-tool-impl-proc-macros") (r "^0.3.2") (d #t) (k 0)))) (h "07lnl79rfzrj6pfgy4mqh5n8v5pv2107394a35kqkwc3r354ia6v")))

(define-public crate-source-demo-tool-0.7.0 (c (n "source-demo-tool") (v "0.7.0") (d (list (d (n "async-channel") (r "^1.8.0") (d #t) (k 0)) (d (n "buf_redux") (r "^0.8.4") (d #t) (k 0)) (d (n "source-demo-tool-impl-proc-macros") (r "^0.3.2") (d #t) (k 0)))) (h "0b4kp5jkn0si5f1cvfidj3ayx0rlqw2za27cjb3yh2sr5ijs8xph") (y #t)))

(define-public crate-source-demo-tool-0.7.1 (c (n "source-demo-tool") (v "0.7.1") (d (list (d (n "async-channel") (r "^1.8.0") (d #t) (k 0)) (d (n "buf_redux") (r "^0.8.4") (d #t) (k 0)) (d (n "source-demo-tool-impl-proc-macros") (r "^0.3.3") (d #t) (k 0)))) (h "1256is0ih87l3np8dypbrxmkvr0894cpyk5jpyyxbyjq3pni8dhi")))

(define-public crate-source-demo-tool-0.7.2 (c (n "source-demo-tool") (v "0.7.2") (d (list (d (n "async-channel") (r "^1.8.0") (d #t) (k 0)) (d (n "buf_redux") (r "^0.8.4") (d #t) (k 0)) (d (n "source-demo-tool-impl-proc-macros") (r "^0.3.3") (d #t) (k 0)))) (h "1y7fz4wckwxcbgd8lq08345l36y2s6p4wkamjarb7c65kjlfcfqh")))

(define-public crate-source-demo-tool-0.7.3 (c (n "source-demo-tool") (v "0.7.3") (d (list (d (n "async-channel") (r "^1.8.0") (d #t) (k 0)) (d (n "buf_redux") (r "^0.8.4") (d #t) (k 0)) (d (n "source-demo-tool-impl-proc-macros") (r "^0.3.3") (d #t) (k 0)))) (h "0g7rkizf2h6zpk5qvyfj4x5brlk7jy1zljjmrfvihdzm6v47hydd")))

(define-public crate-source-demo-tool-0.8.0 (c (n "source-demo-tool") (v "0.8.0") (d (list (d (n "async-channel") (r "^1.8.0") (d #t) (k 0)) (d (n "buf_redux") (r "^0.8.4") (d #t) (k 0)) (d (n "source-demo-tool-impl-proc-macros") (r "^0.3.3") (d #t) (k 0)))) (h "13s4fzrj9q4ivy7bv7hp75mhfphj1r8fp67525yb3xala13k1xiz")))

(define-public crate-source-demo-tool-0.9.0 (c (n "source-demo-tool") (v "0.9.0") (d (list (d (n "async-channel") (r "^1.8.0") (d #t) (k 0)) (d (n "buf_redux") (r "^0.8.4") (d #t) (k 0)) (d (n "source-demo-tool-impl-proc-macros") (r "^0.4.0") (d #t) (k 0)))) (h "1s9fyx7csgvl4zgfqnbn2fnfgymwkj1bs0hqwiaqkv9j9j7l5kcx")))

(define-public crate-source-demo-tool-0.9.1 (c (n "source-demo-tool") (v "0.9.1") (d (list (d (n "async-channel") (r "^1.8.0") (d #t) (k 0)) (d (n "buf_redux") (r "^0.8.4") (d #t) (k 0)) (d (n "source-demo-tool-impl-proc-macros") (r "^0.4.1") (d #t) (k 0)))) (h "0id6vi4xvgy2w4akc0kkwvfj5skfmmlc7mcr3hdrjgvxwqq34y5a")))

(define-public crate-source-demo-tool-0.9.2 (c (n "source-demo-tool") (v "0.9.2") (d (list (d (n "async-channel") (r "^1.8.0") (d #t) (k 0)) (d (n "buf_redux") (r "^0.8.4") (d #t) (k 0)) (d (n "source-demo-tool-impl-proc-macros") (r "^0.4.1") (d #t) (k 0)))) (h "1163607n3pnahn0lghimywmpm33wcnqbjb1a4wslf7vqwn9aqsiv")))

