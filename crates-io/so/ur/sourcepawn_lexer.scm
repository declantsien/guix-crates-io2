(define-module (crates-io so ur sourcepawn_lexer) #:use-module (crates-io))

(define-public crate-sourcepawn_lexer-0.1.0 (c (n "sourcepawn_lexer") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "logos") (r "^0.12.1") (d #t) (k 0)) (d (n "lsp-types") (r "^0.94.0") (d #t) (k 0)) (d (n "minreq") (r "^2.7.0") (f (quote ("https"))) (d #t) (k 2)) (d (n "regex") (r "^1.7.3") (d #t) (k 0)))) (h "02jni6zjpcrya22lhy16f137dh6k6cr9j4md565j0629p4yinmg4")))

(define-public crate-sourcepawn_lexer-0.1.1 (c (n "sourcepawn_lexer") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "logos") (r "^0.12.1") (d #t) (k 0)) (d (n "lsp-types") (r "^0.94.0") (d #t) (k 0)) (d (n "minreq") (r "^2.7.0") (f (quote ("https"))) (d #t) (k 2)) (d (n "regex") (r "^1.7.3") (d #t) (k 0)))) (h "098y7wl5pb5mxab9q4wbhvinirh7rlwf1vb995mwhp6rxm37pxqg")))

(define-public crate-sourcepawn_lexer-0.1.2 (c (n "sourcepawn_lexer") (v "0.1.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "logos") (r "^0.12.1") (d #t) (k 0)) (d (n "lsp-types") (r "^0.94.0") (d #t) (k 0)) (d (n "minreq") (r "^2.7.0") (f (quote ("https"))) (d #t) (k 2)) (d (n "regex") (r "^1.7.3") (d #t) (k 0)))) (h "0j12nfr2xblla1yjgrwclkpa8m11kh23h71l5m6iqdmdh5afblyw")))

(define-public crate-sourcepawn_lexer-0.1.3 (c (n "sourcepawn_lexer") (v "0.1.3") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "logos") (r "^0.12.1") (d #t) (k 0)) (d (n "lsp-types") (r "^0.94.0") (d #t) (k 0)) (d (n "minreq") (r "^2.7.0") (f (quote ("https"))) (d #t) (k 2)) (d (n "regex") (r "^1.7.3") (d #t) (k 0)))) (h "0n4myy4frr9cpzh669wprj69c342f7smj2jmdnzshpp34sacd8y3")))

(define-public crate-sourcepawn_lexer-0.2.0 (c (n "sourcepawn_lexer") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "logos") (r "^0.12.1") (d #t) (k 0)) (d (n "lsp-types") (r "^0.94.0") (d #t) (k 0)) (d (n "minreq") (r "^2.7.0") (f (quote ("https"))) (d #t) (k 2)) (d (n "regex") (r "^1.7.3") (d #t) (k 0)) (d (n "smol_str") (r "^0.2.1") (d #t) (k 0)))) (h "0hx1dqc7q3plpmb4bhl6q7h04jldp6ps8skprz6fw0wl25q7hp9k")))

