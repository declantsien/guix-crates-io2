(define-module (crates-io so ur source-map-mappings-wasm-api) #:use-module (crates-io))

(define-public crate-source-map-mappings-wasm-api-0.1.0 (c (n "source-map-mappings-wasm-api") (v "0.1.0") (d (list (d (n "source-map-mappings") (r "^0.1.0") (d #t) (k 0)))) (h "0j7hzfbfkkqs3n6ra4dyi99v4birymfs8fldigkj8pa0g11pv0q2")))

(define-public crate-source-map-mappings-wasm-api-0.2.0 (c (n "source-map-mappings-wasm-api") (v "0.2.0") (d (list (d (n "source-map-mappings") (r "^0.2.0") (d #t) (k 0)))) (h "0f27k5icfiqm0jsgghppk74s0imqz0w74v6cr4c6l7lr63amzrs2")))

(define-public crate-source-map-mappings-wasm-api-0.3.0 (c (n "source-map-mappings-wasm-api") (v "0.3.0") (d (list (d (n "source-map-mappings") (r "^0.3.0") (d #t) (k 0)))) (h "124n9kaax0c3k5sdbi8sgcl232b09kl2f6rcr7b3kkpjwazwq7jm") (f (quote (("profiling"))))))

(define-public crate-source-map-mappings-wasm-api-0.4.0 (c (n "source-map-mappings-wasm-api") (v "0.4.0") (d (list (d (n "source-map-mappings") (r "^0.4.0") (d #t) (k 0)))) (h "0fk4cn5a95kfd0zckpw6fihrg9xyl2s4yysn9wdj0sggsygy7c53") (f (quote (("profiling"))))))

(define-public crate-source-map-mappings-wasm-api-0.5.0 (c (n "source-map-mappings-wasm-api") (v "0.5.0") (d (list (d (n "source-map-mappings") (r "^0.5.0") (d #t) (k 0)))) (h "17l56jy32cz01yvv0l2d5x7330g12v2hdn6m7lc6n5dyn82cvhq7") (f (quote (("profiling"))))))

