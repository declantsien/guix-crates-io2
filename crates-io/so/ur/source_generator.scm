(define-module (crates-io so ur source_generator) #:use-module (crates-io))

(define-public crate-source_generator-0.0.1 (c (n "source_generator") (v "0.0.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "linked_hash_set") (r "^0.1.4") (d #t) (k 0)))) (h "1zfzx5ab0nwcq8dz2zma0s9pzqpn08p6wncmn36k7r6rymqk21xx")))

(define-public crate-source_generator-0.0.2 (c (n "source_generator") (v "0.0.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "linked_hash_set") (r "^0.1.4") (d #t) (k 0)))) (h "0sqy57ah68vsrh02i8xi5iqxm1ix27v78pgky54mafxf15482b5v")))

(define-public crate-source_generator-0.0.3 (c (n "source_generator") (v "0.0.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "linked-hash-map") (r "^0.5.6") (d #t) (k 0)) (d (n "linked_hash_set") (r "^0.1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde-json-schema") (r "^0.1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1b6jmfhmppr1yjirhhxgmbqndwyrv9d1n8mj1hkbcddmb21k69da")))

