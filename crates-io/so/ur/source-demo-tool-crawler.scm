(define-module (crates-io so ur source-demo-tool-crawler) #:use-module (crates-io))

(define-public crate-source-demo-tool-crawler-0.2.0 (c (n "source-demo-tool-crawler") (v "0.2.0") (d (list (d (n "eframe") (r "^0.20.1") (d #t) (k 0)) (d (n "egui_extras") (r "^0.20.0") (d #t) (k 0)) (d (n "rfd") (r "^0.10.0") (d #t) (k 0)) (d (n "source-demo-tool") (r "^0.2.1") (d #t) (k 0)))) (h "0sm8icln7svx1fg8z4qi5hzrlc1xywff11r06649j9z7frywx364")))

(define-public crate-source-demo-tool-crawler-0.2.1 (c (n "source-demo-tool-crawler") (v "0.2.1") (d (list (d (n "eframe") (r "^0.20.1") (d #t) (k 0)) (d (n "egui_extras") (r "^0.20.0") (d #t) (k 0)) (d (n "rfd") (r "^0.10.0") (d #t) (k 0)) (d (n "source-demo-tool") (r "^0.2.2") (d #t) (k 0)))) (h "0h1vhrw4z28llxd2xlf58smvi43gdiq0czvdsii14cky6ax0lmck")))

(define-public crate-source-demo-tool-crawler-0.3.0 (c (n "source-demo-tool-crawler") (v "0.3.0") (d (list (d (n "eframe") (r "^0.20.1") (d #t) (k 0)) (d (n "egui_extras") (r "^0.20.0") (d #t) (k 0)) (d (n "rfd") (r "^0.10.0") (d #t) (k 0)) (d (n "source-demo-tool") (r "^0.3.0") (d #t) (k 0)))) (h "16d2zpsjn7wsml5cwf0r1apnhahpim2m6nc348x8x06fx5ifibra")))

(define-public crate-source-demo-tool-crawler-0.4.0 (c (n "source-demo-tool-crawler") (v "0.4.0") (d (list (d (n "eframe") (r "^0.20.1") (d #t) (k 0)) (d (n "egui_extras") (r "^0.20.0") (d #t) (k 0)) (d (n "rfd") (r "^0.10.0") (d #t) (k 0)) (d (n "source-demo-tool") (r "^0.4.1") (d #t) (k 0)))) (h "1c9yjs3gqdc62i3qw6vdicf6ac122d26vsbx9dkg5ng0b9i1av0z")))

(define-public crate-source-demo-tool-crawler-0.4.1 (c (n "source-demo-tool-crawler") (v "0.4.1") (d (list (d (n "eframe") (r "^0.20.1") (d #t) (k 0)) (d (n "egui_extras") (r "^0.20.0") (d #t) (k 0)) (d (n "rfd") (r "^0.10.0") (d #t) (k 0)) (d (n "source-demo-tool") (r "^0.4.1") (d #t) (k 0)))) (h "0pnrjc8zdb17lymd9wj9lbqyn381a3mhvf123akwk5gq6bqy1jh7")))

(define-public crate-source-demo-tool-crawler-0.4.2 (c (n "source-demo-tool-crawler") (v "0.4.2") (d (list (d (n "eframe") (r "^0.20.1") (d #t) (k 0)) (d (n "egui_extras") (r "^0.20.0") (d #t) (k 0)) (d (n "rfd") (r "^0.11.0") (d #t) (k 0)) (d (n "source-demo-tool") (r "^0.4.1") (d #t) (k 0)))) (h "0v5g0bigpgvhy4ycm14xf8lsnj1lh4bkmwj0kcw1la0f0h4960yp")))

(define-public crate-source-demo-tool-crawler-0.5.0 (c (n "source-demo-tool-crawler") (v "0.5.0") (d (list (d (n "eframe") (r "^0.20.1") (d #t) (k 0)) (d (n "egui_extras") (r "^0.20.0") (d #t) (k 0)) (d (n "rfd") (r "^0.11.0") (d #t) (k 0)) (d (n "source-demo-tool") (r "^0.4.2") (d #t) (k 0)))) (h "09bds7zpina9ayha8901l6nbraf6s1y8cnc01nx69mx9vpadghyl")))

(define-public crate-source-demo-tool-crawler-0.5.1 (c (n "source-demo-tool-crawler") (v "0.5.1") (d (list (d (n "eframe") (r "^0.20.1") (d #t) (k 0)) (d (n "egui_extras") (r "^0.20.0") (d #t) (k 0)) (d (n "rfd") (r "^0.11.0") (d #t) (k 0)) (d (n "source-demo-tool") (r "^0.5.0") (d #t) (k 0)))) (h "08rf7bxax7y0vnnqf2l3z4nwa4ypkpz9m1w74kjy2qnkxgfk7829")))

(define-public crate-source-demo-tool-crawler-0.5.2 (c (n "source-demo-tool-crawler") (v "0.5.2") (d (list (d (n "eframe") (r "^0.20.1") (d #t) (k 0)) (d (n "egui_extras") (r "^0.20.0") (d #t) (k 0)) (d (n "rfd") (r "^0.11.0") (d #t) (k 0)) (d (n "source-demo-tool") (r "^0.7.1") (d #t) (k 0)))) (h "127zf8b5mxldyazgs2p81byss6kzpw4m38snaqpiqdyi0pbl9iji")))

(define-public crate-source-demo-tool-crawler-0.6.0 (c (n "source-demo-tool-crawler") (v "0.6.0") (d (list (d (n "eframe") (r "^0.20.1") (d #t) (k 0)) (d (n "egui_extras") (r "^0.20.0") (d #t) (k 0)) (d (n "rfd") (r "^0.11.0") (d #t) (k 0)) (d (n "source-demo-tool") (r "^0.7.3") (d #t) (k 0)))) (h "0bfj966a97haihaz2sdvnz428hyqml9n0h6j27in29022lrmh5b6")))

(define-public crate-source-demo-tool-crawler-0.7.0 (c (n "source-demo-tool-crawler") (v "0.7.0") (d (list (d (n "eframe") (r "^0.20.1") (d #t) (k 0)) (d (n "egui_extras") (r "^0.20.0") (d #t) (k 0)) (d (n "rfd") (r "^0.11.0") (d #t) (k 0)) (d (n "source-demo-tool") (r "^0.7.3") (d #t) (k 0)))) (h "1hyq1z9j2pav4d1yw8srqgwpkxczrqjm8psw7l3hki1fjs0l0lzb")))

(define-public crate-source-demo-tool-crawler-0.8.0 (c (n "source-demo-tool-crawler") (v "0.8.0") (d (list (d (n "eframe") (r "^0.21.0") (d #t) (k 0)) (d (n "egui_extras") (r "^0.21.0") (d #t) (k 0)) (d (n "rfd") (r "^0.11.0") (d #t) (k 0)) (d (n "source-demo-tool") (r "^0.9.0") (d #t) (k 0)))) (h "05h3pps1v3vr7rvxaaxkmwj3i6mnxkz04mhyqw1zw4aff0ib36nr")))

(define-public crate-source-demo-tool-crawler-0.8.1 (c (n "source-demo-tool-crawler") (v "0.8.1") (d (list (d (n "eframe") (r "^0.21.0") (d #t) (k 0)) (d (n "egui_extras") (r "^0.21.0") (d #t) (k 0)) (d (n "rfd") (r "^0.11.0") (d #t) (k 0)) (d (n "source-demo-tool") (r "^0.9.1") (d #t) (k 0)))) (h "103srsc8v3qyrwmlfiq4vr43h0c426qakf4vv1dwi6zk0xfi5305") (y #t)))

(define-public crate-source-demo-tool-crawler-0.8.2 (c (n "source-demo-tool-crawler") (v "0.8.2") (d (list (d (n "eframe") (r "^0.21.0") (d #t) (k 0)) (d (n "egui_extras") (r "^0.21.0") (d #t) (k 0)) (d (n "rfd") (r "^0.11.0") (d #t) (k 0)) (d (n "source-demo-tool") (r "^0.9.1") (d #t) (k 0)))) (h "14g8b4sjrkm5xxhiwfn75kp17116rl5sk2r7csmmcvw98xjc2ffs")))

