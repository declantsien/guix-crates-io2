(define-module (crates-io so ur source-demo-tool-impl-proc-macros) #:use-module (crates-io))

(define-public crate-source-demo-tool-impl-proc-macros-0.2.0 (c (n "source-demo-tool-impl-proc-macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.49") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0nls40pwcvy9j2jq53y9vpd5a4bf5q7f5000iscwnwrqfi808rrm")))

(define-public crate-source-demo-tool-impl-proc-macros-0.3.0 (c (n "source-demo-tool-impl-proc-macros") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0.49") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "136jx5fx6j75812jhlvbq6117wj84lbqx1iqmh3gx07ir1wyd0h0")))

(define-public crate-source-demo-tool-impl-proc-macros-0.3.1 (c (n "source-demo-tool-impl-proc-macros") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^1.0.49") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "11y9z0qd7y8drifilxjans9p1jbdib3crz2gaxz8mgnq6p6pxqgs")))

(define-public crate-source-demo-tool-impl-proc-macros-0.3.2 (c (n "source-demo-tool-impl-proc-macros") (v "0.3.2") (d (list (d (n "proc-macro2") (r "^1.0.49") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "17vqqyqwy1b797l0hlhiwsivpb86rdfh4124mxhh5rv0bbl7fd5y")))

(define-public crate-source-demo-tool-impl-proc-macros-0.3.3 (c (n "source-demo-tool-impl-proc-macros") (v "0.3.3") (d (list (d (n "proc-macro2") (r "^1.0.49") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0nj6xprqwxp9lssi9q93ch22jnr3dq5j1darpkf1m3xw1bq1gb6r")))

(define-public crate-source-demo-tool-impl-proc-macros-0.3.4 (c (n "source-demo-tool-impl-proc-macros") (v "0.3.4") (d (list (d (n "proc-macro2") (r "^1.0.49") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "08dmqc2k941qj7kkgxal2ps7qp47lxc2b93r2vsg6vcbxfq6qzdw")))

(define-public crate-source-demo-tool-impl-proc-macros-0.4.0 (c (n "source-demo-tool-impl-proc-macros") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0.49") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0cikxdj28swz48q66rdd009wza2scshbsrz194s3i8mcsb4pw256")))

(define-public crate-source-demo-tool-impl-proc-macros-0.4.1 (c (n "source-demo-tool-impl-proc-macros") (v "0.4.1") (d (list (d (n "proc-macro2") (r "^1.0.49") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1xxl3yxb7si25v5d9glzal31xa9q5abq9sy6nziq2y8na7w0v2k6")))

(define-public crate-source-demo-tool-impl-proc-macros-0.4.2 (c (n "source-demo-tool-impl-proc-macros") (v "0.4.2") (d (list (d (n "proc-macro2") (r "^1.0.49") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0yc96q8l8zq09vrppgzaa8prdjf317r75mhlqpxgadwmkvq9gwad")))

