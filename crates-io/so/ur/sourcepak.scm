(define-module (crates-io so ur sourcepak) #:use-module (crates-io))

(define-public crate-sourcepak-0.1.0 (c (n "sourcepak") (v "0.1.0") (d (list (d (n "crc") (r "^3.0.1") (d #t) (k 0)) (d (n "filebuffer") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "lzham-alpha-sys") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (o #t) (d #t) (k 0)))) (h "1isxj9zx388ipdhqhpklzrnjmrifnv4dp5f6kdmma6m7h64hcx9c") (f (quote (("rpak") ("respawn" "revpk" "rpak")))) (s 2) (e (quote (("revpk" "dep:lzham-alpha-sys" "dep:once_cell") ("mem-map" "dep:filebuffer"))))))

(define-public crate-sourcepak-0.1.1 (c (n "sourcepak") (v "0.1.1") (d (list (d (n "crc") (r "^3.0.1") (d #t) (k 0)) (d (n "filebuffer") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "lzham-alpha-sys") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (o #t) (d #t) (k 0)))) (h "0xxslahkkx1pczxpm665mlmi0746br8ld965s1yj5rbhws2d010c") (f (quote (("rpak") ("respawn" "revpk" "rpak")))) (s 2) (e (quote (("revpk" "dep:lzham-alpha-sys" "dep:once_cell") ("mem-map" "dep:filebuffer"))))))

(define-public crate-sourcepak-0.1.2 (c (n "sourcepak") (v "0.1.2") (d (list (d (n "crc") (r "^3.0.1") (d #t) (k 0)) (d (n "filebuffer") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "lzham-alpha-sys") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (o #t) (d #t) (k 0)))) (h "1h84j0l2pk5bv5pp26b4szv40c33dla5vna5b7mawx2ww66lvh9r") (f (quote (("rpak") ("respawn" "revpk" "rpak")))) (s 2) (e (quote (("revpk" "dep:lzham-alpha-sys" "dep:once_cell") ("mem-map" "dep:filebuffer"))))))

