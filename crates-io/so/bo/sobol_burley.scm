(define-module (crates-io so bo sobol_burley) #:use-module (crates-io))

(define-public crate-sobol_burley-0.1.0 (c (n "sobol_burley") (v "0.1.0") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 2)))) (h "0rf9yjp00lk5jqknh19bpz882dyzhqqvq6h3sixww7hfxws7cccj")))

(define-public crate-sobol_burley-0.2.0 (c (n "sobol_burley") (v "0.2.0") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0r5802mf9dkgrpikqknn6b8x45wn0kzxjd13gsc7lcvxj8wymlxr")))

(define-public crate-sobol_burley-0.3.0 (c (n "sobol_burley") (v "0.3.0") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0l6mad55hvia4mr0ffk2qn6mc3g8s5i48ailabhv3wdi165m5qr6")))

(define-public crate-sobol_burley-0.3.1 (c (n "sobol_burley") (v "0.3.1") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0pc0xmk3j38pg0ap4gzj8j9jp2x7l6wfqqqljiif4w7gdip922r7") (f (quote (("simd") ("default" "simd"))))))

(define-public crate-sobol_burley-0.4.0 (c (n "sobol_burley") (v "0.4.0") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "185n1i4sm0winq72rhwrjpwsw4rpk0dbazlf3cpkfvl9pcmv6h9l") (f (quote (("simd") ("default" "simd"))))))

(define-public crate-sobol_burley-0.5.0 (c (n "sobol_burley") (v "0.5.0") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1lg1mx9jxiaxyipvginmk9l3p0gp4rxdwghmfy1hgi4p3np7rwq9") (f (quote (("simd") ("default" "simd"))))))

