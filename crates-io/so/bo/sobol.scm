(define-module (crates-io so bo sobol) #:use-module (crates-io))

(define-public crate-sobol-0.1.0 (c (n "sobol") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "libflate") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1nkn05g7apbdc054pxh847nydrllcwldhnazkjqdclwhbipwi4qr")))

(define-public crate-sobol-0.1.1 (c (n "sobol") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "libflate") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1zj4ck0w9qqvikvs7d1j6wwz3cx24zalazyif2wp2v4i2h0a2an5")))

(define-public crate-sobol-1.0.0 (c (n "sobol") (v "1.0.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "libflate") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0x76l2xqcw17c5bfp3pfj6yj2ayn684risbxbrijryya5hsmzzgl")))

(define-public crate-sobol-1.0.1 (c (n "sobol") (v "1.0.1") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "libflate") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "09vm1mb7gygn8bmcc948z3izdmvijrms72cfnppasx2izflshr8r")))

(define-public crate-sobol-1.0.2 (c (n "sobol") (v "1.0.2") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "libflate") (r "^1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "037jxmka5apfzyf10qqnvgx4srzhqapgi9qrmf663qnnhqc2d2bc")))

