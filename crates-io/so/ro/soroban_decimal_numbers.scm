(define-module (crates-io so ro soroban_decimal_numbers) #:use-module (crates-io))

(define-public crate-soroban_decimal_numbers-0.1.0 (c (n "soroban_decimal_numbers") (v "0.1.0") (h "1s8q88dkg6h50n0jaiksbdkiqxf2rpyza4czbzpwjl4vmgz1vnrg")))

(define-public crate-soroban_decimal_numbers-0.1.1 (c (n "soroban_decimal_numbers") (v "0.1.1") (h "1h12qhcaklqjl1gd9jl79ja44gn3g119z6m5img4cmiy2s8qpqnz")))

(define-public crate-soroban_decimal_numbers-0.1.2 (c (n "soroban_decimal_numbers") (v "0.1.2") (h "0l4xi40x9cph0rgasm1vmwrw9whqs4qwk1fq170k223y8w9dc577")))

(define-public crate-soroban_decimal_numbers-0.1.3 (c (n "soroban_decimal_numbers") (v "0.1.3") (h "0ysxa58xb9fhvpplppz62l4b4xdvca4myp12dr6hcwpacd77zm65")))

(define-public crate-soroban_decimal_numbers-0.1.4 (c (n "soroban_decimal_numbers") (v "0.1.4") (h "1l0bjbxhh472c0afv957hw99pgl47shdrvbkn5if1457i93v4ymw")))

(define-public crate-soroban_decimal_numbers-0.1.5 (c (n "soroban_decimal_numbers") (v "0.1.5") (h "06h31qpg5nhwyq9kjfqrzm251v36ajdm41aga8rb33ijr73h0cjz")))

