(define-module (crates-io so ro soroban-builtin-sdk-macros) #:use-module (crates-io))

(define-public crate-soroban-builtin-sdk-macros-0.0.1 (c (n "soroban-builtin-sdk-macros") (v "0.0.1") (h "0agch6r9gs2ljlkjqlfyyya9ab9pfiqa8q5pzc76g4gy09004lk5")))

(define-public crate-soroban-builtin-sdk-macros-20.0.0 (c (n "soroban-builtin-sdk-macros") (v "20.0.0") (d (list (d (n "itertools") (r "=0.11.0") (d #t) (k 0)) (d (n "proc-macro2") (r "=1.0.69") (d #t) (k 0)) (d (n "quote") (r "=1.0.33") (d #t) (k 0)) (d (n "syn") (r "=2.0.39") (f (quote ("full"))) (d #t) (k 0)))) (h "09q55yqqawba7rq0pwps0aqdn23rdyf5gszyagvli1r6n3b8f95l") (r "1.74")))

(define-public crate-soroban-builtin-sdk-macros-20.0.1 (c (n "soroban-builtin-sdk-macros") (v "20.0.1") (d (list (d (n "itertools") (r "=0.11.0") (d #t) (k 0)) (d (n "proc-macro2") (r "=1.0.69") (d #t) (k 0)) (d (n "quote") (r "=1.0.33") (d #t) (k 0)) (d (n "syn") (r "=2.0.39") (f (quote ("full"))) (d #t) (k 0)))) (h "0bqzkkaz4p7rnpncdv6lwid6hbfxkk2z59xchsdy2m0iasa5qd00") (r "1.74")))

(define-public crate-soroban-builtin-sdk-macros-20.0.2 (c (n "soroban-builtin-sdk-macros") (v "20.0.2") (d (list (d (n "itertools") (r "=0.11.0") (d #t) (k 0)) (d (n "proc-macro2") (r "=1.0.69") (d #t) (k 0)) (d (n "quote") (r "=1.0.33") (d #t) (k 0)) (d (n "syn") (r "=2.0.39") (f (quote ("full"))) (d #t) (k 0)))) (h "138cmk99mj4gk4s9i8xz42llk7yqlz6bfxnwjn9l4ipmn9a4y351") (r "1.74")))

(define-public crate-soroban-builtin-sdk-macros-20.1.0 (c (n "soroban-builtin-sdk-macros") (v "20.1.0") (d (list (d (n "itertools") (r "=0.11.0") (d #t) (k 0)) (d (n "proc-macro2") (r "=1.0.69") (d #t) (k 0)) (d (n "quote") (r "=1.0.33") (d #t) (k 0)) (d (n "syn") (r "=2.0.39") (f (quote ("full"))) (d #t) (k 0)))) (h "0b6vylf8z913ywjbaxhfxyv7x2n0dkg4vmbpkxdlvdvbiiqx191s") (r "1.74")))

(define-public crate-soroban-builtin-sdk-macros-20.1.1 (c (n "soroban-builtin-sdk-macros") (v "20.1.1") (d (list (d (n "itertools") (r "=0.11.0") (d #t) (k 0)) (d (n "proc-macro2") (r "=1.0.69") (d #t) (k 0)) (d (n "quote") (r "=1.0.33") (d #t) (k 0)) (d (n "syn") (r "=2.0.39") (f (quote ("full"))) (d #t) (k 0)))) (h "0ym1v3ccy1g3iqs11dj38wzpsmfhbmz9mmyymg8a2gqad5hm6yr1") (r "1.74.0")))

(define-public crate-soroban-builtin-sdk-macros-20.2.0 (c (n "soroban-builtin-sdk-macros") (v "20.2.0") (d (list (d (n "itertools") (r "=0.11.0") (d #t) (k 0)) (d (n "proc-macro2") (r "=1.0.69") (d #t) (k 0)) (d (n "quote") (r "=1.0.33") (d #t) (k 0)) (d (n "syn") (r "=2.0.39") (f (quote ("full"))) (d #t) (k 0)))) (h "0vgd1c33b5wgslfanxqnlmr00a91gf4b2irky8xxha497rbmdx94") (r "1.74.0")))

(define-public crate-soroban-builtin-sdk-macros-20.2.1 (c (n "soroban-builtin-sdk-macros") (v "20.2.1") (d (list (d (n "itertools") (r "=0.11.0") (d #t) (k 0)) (d (n "proc-macro2") (r "=1.0.69") (d #t) (k 0)) (d (n "quote") (r "=1.0.33") (d #t) (k 0)) (d (n "syn") (r "=2.0.39") (f (quote ("full"))) (d #t) (k 0)))) (h "0l9xhfpy2k8aw7z36rp5ngadxvcyh5crvmr5jk3gkjkziazvz7ri") (r "1.74.0")))

(define-public crate-soroban-builtin-sdk-macros-20.2.2 (c (n "soroban-builtin-sdk-macros") (v "20.2.2") (d (list (d (n "itertools") (r "=0.11.0") (d #t) (k 0)) (d (n "proc-macro2") (r "=1.0.69") (d #t) (k 0)) (d (n "quote") (r "=1.0.33") (d #t) (k 0)) (d (n "syn") (r "=2.0.39") (f (quote ("full"))) (d #t) (k 0)))) (h "1chlay6l967iv79ff5k6ll6lzp2sf0rhxra1wikl588q44nk1vsm") (r "1.74.0")))

(define-public crate-soroban-builtin-sdk-macros-20.3.0 (c (n "soroban-builtin-sdk-macros") (v "20.3.0") (d (list (d (n "itertools") (r "=0.11.0") (d #t) (k 0)) (d (n "proc-macro2") (r "=1.0.69") (d #t) (k 0)) (d (n "quote") (r "=1.0.33") (d #t) (k 0)) (d (n "syn") (r "=2.0.39") (f (quote ("full"))) (d #t) (k 0)))) (h "0049amhfhgma8pkx855zlms0n8d69p8xgh2ffrls4g3zh5p2rhvw") (r "1.74.0")))

(define-public crate-soroban-builtin-sdk-macros-21.0.0 (c (n "soroban-builtin-sdk-macros") (v "21.0.0") (d (list (d (n "itertools") (r "=0.11.0") (d #t) (k 0)) (d (n "proc-macro2") (r "=1.0.69") (d #t) (k 0)) (d (n "quote") (r "=1.0.33") (d #t) (k 0)) (d (n "syn") (r "=2.0.39") (f (quote ("full"))) (d #t) (k 0)))) (h "012ii3rn54v8x00zbf948k7mmdqsbp9cl2c69xqc1rzb4hp3xnd8") (r "1.74.0")))

(define-public crate-soroban-builtin-sdk-macros-21.0.1 (c (n "soroban-builtin-sdk-macros") (v "21.0.1") (d (list (d (n "itertools") (r "=0.11.0") (d #t) (k 0)) (d (n "proc-macro2") (r "=1.0.69") (d #t) (k 0)) (d (n "quote") (r "=1.0.33") (d #t) (k 0)) (d (n "syn") (r "=2.0.39") (f (quote ("full"))) (d #t) (k 0)))) (h "12zlbhbyxb2pf8iwxxq5h3x4svbraqiblq7zn2c2m6hwnn0k8ng1") (r "1.74.0")))

(define-public crate-soroban-builtin-sdk-macros-21.0.2 (c (n "soroban-builtin-sdk-macros") (v "21.0.2") (d (list (d (n "itertools") (r "=0.11.0") (d #t) (k 0)) (d (n "proc-macro2") (r "=1.0.69") (d #t) (k 0)) (d (n "quote") (r "=1.0.33") (d #t) (k 0)) (d (n "syn") (r "=2.0.39") (f (quote ("full"))) (d #t) (k 0)))) (h "0i6krmmkv370gsvmgpj6myzfnhfzkw1206p51d5x308an8f44yn9") (r "1.74.0")))

