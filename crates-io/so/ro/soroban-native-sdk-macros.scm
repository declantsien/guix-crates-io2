(define-module (crates-io so ro soroban-native-sdk-macros) #:use-module (crates-io))

(define-public crate-soroban-native-sdk-macros-0.0.4 (c (n "soroban-native-sdk-macros") (v "0.0.4") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0h3nw43sl3dx070azr33mghkrd45v7rzhpii9py61jp9wsbf45fj") (y #t) (r "1.63")))

(define-public crate-soroban-native-sdk-macros-0.0.5 (c (n "soroban-native-sdk-macros") (v "0.0.5") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1h4wn691nq0f843iq36mj19n35l19z6m3liknq3knpc4xl1m5sii") (y #t) (r "1.63")))

(define-public crate-soroban-native-sdk-macros-0.0.6 (c (n "soroban-native-sdk-macros") (v "0.0.6") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0xf38h5kkkkivi3cwp88gyjillmrxxshwq7pnyckapi2hsg0v8s7") (y #t) (r "1.64")))

(define-public crate-soroban-native-sdk-macros-0.0.7 (c (n "soroban-native-sdk-macros") (v "0.0.7") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0q50w1adpcag9iv4iljyn5v46x4s8w9zpbnpn6nr1bbdydq6klr1") (y #t) (r "1.64")))

(define-public crate-soroban-native-sdk-macros-0.0.8 (c (n "soroban-native-sdk-macros") (v "0.0.8") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0n72yars3ra38id0zj9fv841g542k1x5ly80fgjvh751g727dm59") (y #t) (r "1.64")))

(define-public crate-soroban-native-sdk-macros-0.0.9 (c (n "soroban-native-sdk-macros") (v "0.0.9") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0r7yibys0w3cy9c7pblkp4ccb7sw07zgfrbvzi4s1y5pcz64qjpx") (y #t) (r "1.64")))

(define-public crate-soroban-native-sdk-macros-0.0.10 (c (n "soroban-native-sdk-macros") (v "0.0.10") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "11w7k61caxlpjpm24rjz95rdjr1bcwm896im1m3mycpfiv0cbx35") (y #t) (r "1.65")))

(define-public crate-soroban-native-sdk-macros-0.0.11 (c (n "soroban-native-sdk-macros") (v "0.0.11") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0l7a4hg5s6dmi122v230db6wb0zka2yh1cfs4sx4pfzlv9cj755i") (y #t) (r "1.65")))

(define-public crate-soroban-native-sdk-macros-0.0.12 (c (n "soroban-native-sdk-macros") (v "0.0.12") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0gpqd0bihb7zv1w374d1qfih8gj5d5sx78ncxcrcqa101q5jnki2") (y #t) (r "1.66")))

(define-public crate-soroban-native-sdk-macros-0.0.13 (c (n "soroban-native-sdk-macros") (v "0.0.13") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "149npw9sx1cbb597jjs03ifzaqps5v42vbmbfdjc19iv9833yjaf") (y #t) (r "1.67")))

(define-public crate-soroban-native-sdk-macros-0.0.14 (c (n "soroban-native-sdk-macros") (v "0.0.14") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1jsi772bsb6iqn3469lylh8p3ch7vi499msd1xqrvq3njciwhvld") (y #t) (r "1.67")))

(define-public crate-soroban-native-sdk-macros-0.0.15 (c (n "soroban-native-sdk-macros") (v "0.0.15") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1x9chspmbsw8n0vgkm91fk1j31dpgzr15dzinrfkcvf8c8mjnprb") (y #t) (r "1.68")))

(define-public crate-soroban-native-sdk-macros-0.0.16 (c (n "soroban-native-sdk-macros") (v "0.0.16") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "06pcgxsbrmsw8pff529285g0crr6rm0yfgzk9cmin38aq6932amm") (y #t) (r "1.69")))

(define-public crate-soroban-native-sdk-macros-0.0.17 (c (n "soroban-native-sdk-macros") (v "0.0.17") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0fcn6c4cmvdiv3l3mzqrgpa1z4nfsy73hrwbpg69fs0gdd2ip2bd") (y #t) (r "1.70")))

(define-public crate-soroban-native-sdk-macros-20.0.0-rc1 (c (n "soroban-native-sdk-macros") (v "20.0.0-rc1") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "18j7x0dinj7p6mrzmyxv656p9slgf3rrlh2bk773s7rmq266as6y") (r "1.71")))

(define-public crate-soroban-native-sdk-macros-20.0.0-rc2 (c (n "soroban-native-sdk-macros") (v "20.0.0-rc2") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "120ykjz3nqnzs9mgj07d8c5s4x8lb783a1zsqrxk824kbipgk37y") (r "1.71")))

