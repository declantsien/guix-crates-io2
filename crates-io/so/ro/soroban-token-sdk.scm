(define-module (crates-io so ro soroban-token-sdk) #:use-module (crates-io))

(define-public crate-soroban-token-sdk-0.0.1 (c (n "soroban-token-sdk") (v "0.0.1") (h "1hx2xcccygpd84b3rs2x2gkgs7f6ini236l16pkkjvrc2ja45fkw") (y #t)))

(define-public crate-soroban-token-sdk-0.8.0 (c (n "soroban-token-sdk") (v "0.8.0") (d (list (d (n "soroban-sdk") (r "^0.8.0") (d #t) (k 0)))) (h "0lwyim1rl8wp3bxb40228w3rjx152gf7ma3mn6cdaawda9w3xpac") (y #t) (r "1.69")))

(define-public crate-soroban-token-sdk-0.8.1 (c (n "soroban-token-sdk") (v "0.8.1") (d (list (d (n "soroban-sdk") (r "^0.8.1") (d #t) (k 0)))) (h "0j9r6spfgjvwl95l14klr6gqrdcwqrnp44624mvq4dg4jl40h204") (y #t) (r "1.69")))

(define-public crate-soroban-token-sdk-0.8.2 (c (n "soroban-token-sdk") (v "0.8.2") (d (list (d (n "soroban-sdk") (r "^0.8.2") (d #t) (k 0)))) (h "1fb90f9xsaj8m8m1zfqr8mhlqawfidwm4vjpqxbkl3qdsg6n76lk") (y #t) (r "1.69")))

(define-public crate-soroban-token-sdk-0.8.3 (c (n "soroban-token-sdk") (v "0.8.3") (d (list (d (n "soroban-sdk") (r "^0.8.3") (d #t) (k 0)))) (h "0vbpz2wav1x39d6dby1il5lsjlmv4w63lh0sgk7jqbaxhl2q1cbh") (y #t) (r "1.69")))

(define-public crate-soroban-token-sdk-0.8.4 (c (n "soroban-token-sdk") (v "0.8.4") (d (list (d (n "soroban-sdk") (r "^0.8.4") (d #t) (k 0)))) (h "04pm9s4jvm690w88gbmw76426nxhwbsbqxlbknqkkz12xqqpvbj9") (y #t) (r "1.69")))

(define-public crate-soroban-token-sdk-0.8.5 (c (n "soroban-token-sdk") (v "0.8.5") (d (list (d (n "soroban-sdk") (r "^0.8.5") (d #t) (k 0)))) (h "19zmmmxnfyx8qinfrm70dc0mglbdp4jj1cvngfh2s5idwc09b6km") (y #t) (r "1.69")))

(define-public crate-soroban-token-sdk-0.8.6 (c (n "soroban-token-sdk") (v "0.8.6") (d (list (d (n "soroban-sdk") (r "^0.8.6") (d #t) (k 0)))) (h "13930bwwfl7c9n8qws9npaxvls9922nl2gg8pk872mqy8864ff6f") (y #t) (r "1.69")))

(define-public crate-soroban-token-sdk-0.8.7 (c (n "soroban-token-sdk") (v "0.8.7") (d (list (d (n "soroban-sdk") (r "^0.8.7") (d #t) (k 0)))) (h "1l1h4l972pw4l4zq236vif9hb9r6n0iw5l6xsv5jqhq0p7i54fj4") (y #t) (r "1.69")))

(define-public crate-soroban-token-sdk-0.9.0 (c (n "soroban-token-sdk") (v "0.9.0") (d (list (d (n "soroban-sdk") (r "^0.9.0") (d #t) (k 0)))) (h "1kig2p87qry221yjk27h50gfib713rw5gc96b0mivfhm0pi4hdj2") (y #t) (r "1.70")))

(define-public crate-soroban-token-sdk-0.9.1 (c (n "soroban-token-sdk") (v "0.9.1") (d (list (d (n "soroban-sdk") (r "^0.9.1") (d #t) (k 0)))) (h "0fd2clzbi7mhdg2rmx44dq6bgwx0g2nah81z5grz9kqrba5lp6wi") (y #t) (r "1.70")))

(define-public crate-soroban-token-sdk-0.9.2 (c (n "soroban-token-sdk") (v "0.9.2") (d (list (d (n "soroban-sdk") (r "^0.9.2") (d #t) (k 0)))) (h "0igg5n2d7d9ncpzad3fwhr53gjijksrd6qqn3m3rn2jby7vvryiq") (y #t) (r "1.70")))

(define-public crate-soroban-token-sdk-20.0.0-rc1 (c (n "soroban-token-sdk") (v "20.0.0-rc1") (d (list (d (n "soroban-sdk") (r "^20.0.0-rc1") (d #t) (k 0)))) (h "10imcwh7yb86frmhxdlqgp0c7f94cw3hh0100n4v1gzyp6cig99q") (r "1.72")))

(define-public crate-soroban-token-sdk-20.0.0-rc2 (c (n "soroban-token-sdk") (v "20.0.0-rc2") (d (list (d (n "soroban-sdk") (r "^20.0.0-rc2") (d #t) (k 0)))) (h "1vk6yfg2ia9dv0l2jxc1bdg09wjd5sf8zjk3h6jlvb4p44nii53j") (r "1.72")))

(define-public crate-soroban-token-sdk-20.0.0-rc2.2 (c (n "soroban-token-sdk") (v "20.0.0-rc2.2") (d (list (d (n "soroban-sdk") (r "^20.0.0-rc2.2") (d #t) (k 0)))) (h "1d5h6qcx9rrrd0vk5d60vn5k4jwkpgh09yp3iiw2p2vfsvw9ss21") (r "1.72")))

(define-public crate-soroban-token-sdk-20.0.0 (c (n "soroban-token-sdk") (v "20.0.0") (d (list (d (n "soroban-sdk") (r "^20.0.0") (d #t) (k 0)))) (h "19b59h703cijsh09rkz7vhfxj41d39xcc8ci9l5386774had0xcn") (r "1.74")))

(define-public crate-soroban-token-sdk-20.0.1 (c (n "soroban-token-sdk") (v "20.0.1") (d (list (d (n "soroban-sdk") (r "^20.0.1") (d #t) (k 0)))) (h "1pq5rvp5xssrpixrxzm06r0fx4jrrz5ayi8mdwsrd25w8i7qf01l") (r "1.74")))

(define-public crate-soroban-token-sdk-20.0.2 (c (n "soroban-token-sdk") (v "20.0.2") (d (list (d (n "soroban-sdk") (r "^20.0.2") (d #t) (k 0)))) (h "136mdnvwsv680h95519y4d9yl3lhlgvnprjy9rnpyjdlb0z1ziw1") (r "1.74")))

(define-public crate-soroban-token-sdk-20.0.3 (c (n "soroban-token-sdk") (v "20.0.3") (d (list (d (n "soroban-sdk") (r "^20.0.3") (d #t) (k 0)))) (h "02x7m1kz8bxjyp26p9hbsp68a774560l86vdfd4i6cx3lph4bxr3") (r "1.74")))

(define-public crate-soroban-token-sdk-20.1.0 (c (n "soroban-token-sdk") (v "20.1.0") (d (list (d (n "soroban-sdk") (r "^20.1.0") (d #t) (k 0)))) (h "1hlcbbckajqpangp8329k4qnm10kkyq66hfhmf55qjjfpwg05ayy") (r "1.74")))

(define-public crate-soroban-token-sdk-20.2.0 (c (n "soroban-token-sdk") (v "20.2.0") (d (list (d (n "soroban-sdk") (r "^20.2.0") (d #t) (k 0)))) (h "00fcmgxs01bf4r6n834c51h622bzmw6bhggbxcd6izvhxa14r8xj") (r "1.74.0")))

(define-public crate-soroban-token-sdk-20.3.0 (c (n "soroban-token-sdk") (v "20.3.0") (d (list (d (n "soroban-sdk") (r "^20.3.0") (d #t) (k 0)))) (h "102b3yhrs2rv2xg7rqf14ajwp7j34gfg30lmr48f4l8ya37hay7j") (r "1.74.0")))

(define-public crate-soroban-token-sdk-20.3.1 (c (n "soroban-token-sdk") (v "20.3.1") (d (list (d (n "soroban-sdk") (r "^20.3.1") (d #t) (k 0)))) (h "1vw2rky4gb9ypwb68xrmm7pd38czz2lwvqyicnzjnbq9gs9d4yw6") (r "1.74.0")))

(define-public crate-soroban-token-sdk-20.3.2 (c (n "soroban-token-sdk") (v "20.3.2") (d (list (d (n "soroban-sdk") (r "^20.3.2") (d #t) (k 0)))) (h "1zivc1hpqfgw30xpdjy8knj1ffjrwq0q3v4738c3v03160q1i28k") (r "1.74.0")))

(define-public crate-soroban-token-sdk-20.4.0 (c (n "soroban-token-sdk") (v "20.4.0") (d (list (d (n "soroban-sdk") (r "^20.4.0") (d #t) (k 0)))) (h "170qi3dqmyn6naz638n5ylbfsc13q7p1iwr8z65s23z9zxha83qq") (r "1.74.0")))

(define-public crate-soroban-token-sdk-20.5.0 (c (n "soroban-token-sdk") (v "20.5.0") (d (list (d (n "soroban-sdk") (r "^20.5.0") (d #t) (k 0)))) (h "03vq9k3bwjd0pdlcsb3vz3f82k1bf6ivn04jjfvnfpjx5spd13pf") (r "1.74.0")))

(define-public crate-soroban-token-sdk-21.0.0 (c (n "soroban-token-sdk") (v "21.0.0") (d (list (d (n "soroban-sdk") (r "^21.0.0") (d #t) (k 0)))) (h "1pwz99wzaix3mcw838d8v99xgkdkc2zd562znkhns7mmqm46gm7r") (r "1.74.0")))

(define-public crate-soroban-token-sdk-21.0.1-preview.1 (c (n "soroban-token-sdk") (v "21.0.1-preview.1") (d (list (d (n "soroban-sdk") (r "^21.0.1-preview.1") (d #t) (k 0)))) (h "03xmrzjjkqdwvv88b92fci077hb4a8vvi0l4l4xlxmky8pi1zb59") (r "1.74.0")))

(define-public crate-soroban-token-sdk-21.0.1-preview.2 (c (n "soroban-token-sdk") (v "21.0.1-preview.2") (d (list (d (n "soroban-sdk") (r "^21.0.1-preview.2") (d #t) (k 0)))) (h "02nmiw9m47c6nqagxapw2c715n6k3dqh3ymngpq9022inshvxvab") (r "1.74.0")))

(define-public crate-soroban-token-sdk-21.0.1-preview.3 (c (n "soroban-token-sdk") (v "21.0.1-preview.3") (d (list (d (n "soroban-sdk") (r "^21.0.1-preview.3") (d #t) (k 0)))) (h "0zh5wm2gm2p73gbi8mklfhhl8v7kws9rbni50b5mhl6hi5m14zqn") (r "1.74.0")))

