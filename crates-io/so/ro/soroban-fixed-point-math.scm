(define-module (crates-io so ro soroban-fixed-point-math) #:use-module (crates-io))

(define-public crate-soroban-fixed-point-math-1.0.0 (c (n "soroban-fixed-point-math") (v "1.0.0") (d (list (d (n "soroban-sdk") (r "^20.0.0") (d #t) (k 0)))) (h "03qy32rn5zax98ls0q249a8rdzv3yzf68y5aj5snxpprv815j3i3") (r "1.74")))

(define-public crate-soroban-fixed-point-math-1.1.0 (c (n "soroban-fixed-point-math") (v "1.1.0") (d (list (d (n "soroban-sdk") (r "^20.5.0") (d #t) (k 0)))) (h "0z9v3960lcg0fpii4qvd7a7hsa816vn958xnb6ip3yfdi2hbmfbx") (r "1.74")))

