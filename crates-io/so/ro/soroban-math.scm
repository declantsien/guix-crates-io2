(define-module (crates-io so ro soroban-math) #:use-module (crates-io))

(define-public crate-soroban-math-0.1.0 (c (n "soroban-math") (v "0.1.0") (h "1zkglagk97xjhnvjnb6a8mwnx8m29pizmyadw5pi8rwzy3k5kz5w")))

(define-public crate-soroban-math-0.1.1 (c (n "soroban-math") (v "0.1.1") (h "1j2wzpw9prvidy97xxyw4caw6dpjz63hd9b65hz3msqm9r7bzknb")))

(define-public crate-soroban-math-0.1.2 (c (n "soroban-math") (v "0.1.2") (h "0z43i5rwyh9n6xljb1rnnbpxnii6sj67jd5ii8042iw9ikl9na3z")))

(define-public crate-soroban-math-0.1.3 (c (n "soroban-math") (v "0.1.3") (d (list (d (n "soroban-sdk") (r "^20.3.1") (d #t) (k 0)))) (h "1w0x1c7cgbqxkhply5vgi8sq1iy9s1f594xn2acpz02m5r3w94g2")))

(define-public crate-soroban-math-0.1.4 (c (n "soroban-math") (v "0.1.4") (d (list (d (n "soroban-sdk") (r "^20.3.1") (d #t) (k 0)))) (h "1mfll03g6zg2chqvhr27zm04lpvpnaixv8nih9cvxfxws1x3n4sd")))

(define-public crate-soroban-math-0.1.5 (c (n "soroban-math") (v "0.1.5") (d (list (d (n "soroban-sdk") (r "^20.3.1") (d #t) (k 0)))) (h "1ic4v79cnv3lxc6p8nfcpkc4gzl77xa71chxwc02r4qgwazdd43h")))

(define-public crate-soroban-math-0.1.6 (c (n "soroban-math") (v "0.1.6") (d (list (d (n "soroban-sdk") (r "^20.3.1") (d #t) (k 0)))) (h "1lv3gi6s3mlg3yv6xrdr2w8xbzklk70a43mvw6x5d32bb86g71aa")))

(define-public crate-soroban-math-0.1.7 (c (n "soroban-math") (v "0.1.7") (d (list (d (n "soroban-sdk") (r "^20.3.1") (d #t) (k 0)))) (h "1l226ybl2n4ry2082jx2s9rmwa2sjh6c6sy0cp9lqbmkfnc7ykyk")))

(define-public crate-soroban-math-0.1.8 (c (n "soroban-math") (v "0.1.8") (d (list (d (n "soroban-sdk") (r "^20.3.1") (d #t) (k 0)))) (h "0an255a45f1s6ywlhvzwk93ga4wky8m7yqbbd3dhzh6m9wkbf44p")))

(define-public crate-soroban-math-0.1.9 (c (n "soroban-math") (v "0.1.9") (d (list (d (n "soroban-sdk") (r "^20.3.1") (d #t) (k 0)))) (h "0x4kaymvqqsarsdvx7kn2pxf05cxa10hksswa7zlvr5bxn0jy8fi")))

(define-public crate-soroban-math-0.2.0 (c (n "soroban-math") (v "0.2.0") (d (list (d (n "soroban-sdk") (r "^20.3.1") (d #t) (k 0)))) (h "1w1gq1y8smzfndz4023yicwb2fwb0ab1p4nid0x4wcr3qhabc14c")))

(define-public crate-soroban-math-0.2.1 (c (n "soroban-math") (v "0.2.1") (d (list (d (n "soroban-sdk") (r "^20.3.1") (d #t) (k 0)))) (h "0kfidjhfls4cf3r6xaflgq796f7xpnrzl07drgmc8k03w3y0jlxq")))

(define-public crate-soroban-math-0.2.2 (c (n "soroban-math") (v "0.2.2") (d (list (d (n "soroban-sdk") (r "^20.3.1") (d #t) (k 0)))) (h "18h3rn4c1bpig25nxhlawwwhiqanr7d2nwa3rdpa7r0wrzfif1m9")))

