(define-module (crates-io so ro soroban-token-contract) #:use-module (crates-io))

(define-public crate-soroban-token-contract-0.0.1 (c (n "soroban-token-contract") (v "0.0.1") (h "0yrswbvzgq6akn2v7an8m3w2fbl9kpn32dk6p6hy0km8mw7iwdzj") (y #t)))

(define-public crate-soroban-token-contract-0.0.2 (c (n "soroban-token-contract") (v "0.0.2") (d (list (d (n "ed25519-dalek") (r "^1.0.1") (o #t) (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "soroban-sdk") (r "^0.0.3") (d #t) (k 0)))) (h "15hhgkcj9q8j76gns13006w0ls0d48y8mh0y7yl1i5klmjxxg53h") (f (quote (("export") ("default" "export")))) (y #t) (s 2) (e (quote (("testutils" "soroban-sdk/testutils" "dep:ed25519-dalek"))))))

