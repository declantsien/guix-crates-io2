(define-module (crates-io so ro soroban-wasmi-validation) #:use-module (crates-io))

(define-public crate-soroban-wasmi-validation-0.4.1 (c (n "soroban-wasmi-validation") (v "0.4.1") (d (list (d (n "assert_matches") (r "^1.1") (d #t) (k 2)) (d (n "parity-wasm") (r "^0.42.0") (k 0)))) (h "0jrpijsxda152lw1x5ddcpyskjw5rizyfbf038blqksbf2gzx0w9") (f (quote (("std" "parity-wasm/std") ("default" "std")))) (y #t)))

