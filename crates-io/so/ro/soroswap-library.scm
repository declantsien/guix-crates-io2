(define-module (crates-io so ro soroswap-library) #:use-module (crates-io))

(define-public crate-soroswap-library-0.0.1 (c (n "soroswap-library") (v "0.0.1") (d (list (d (n "num-integer") (r "^0.1.45") (f (quote ("i128"))) (k 0)) (d (n "soroban-sdk") (r "^20.0.0-rc2") (d #t) (k 0)) (d (n "soroban-sdk") (r "^20.0.0-rc2") (f (quote ("testutils"))) (d #t) (k 2)))) (h "1i3pr3sppdxqmrl86b36m3mlyfhhwmranbx05a2rx8zd5i4nrsxr") (r "1.73")))

(define-public crate-soroswap-library-0.0.2 (c (n "soroswap-library") (v "0.0.2") (d (list (d (n "num-integer") (r "^0.1.45") (f (quote ("i128"))) (k 0)) (d (n "soroban-sdk") (r "^20.0.0-rc2") (d #t) (k 0)) (d (n "soroban-sdk") (r "^20.0.0-rc2") (f (quote ("testutils"))) (d #t) (k 2)))) (h "181h32knjvvhz81pr0k79m7cg8xiiw7fpcn1xwwj7r3fg7xpikjs") (r "1.73")))

(define-public crate-soroswap-library-0.0.3 (c (n "soroswap-library") (v "0.0.3") (d (list (d (n "num-integer") (r "^0.1.45") (f (quote ("i128"))) (k 0)) (d (n "soroban-sdk") (r "^20.0.0-rc2") (d #t) (k 0)) (d (n "soroban-sdk") (r "^20.0.0-rc2") (f (quote ("testutils"))) (d #t) (k 2)))) (h "0adk9n8ar3lxzxsmcapmc6g7g4z19ywni3ydf9rkbn1nqblm46y9") (r "1.73")))

(define-public crate-soroswap-library-0.0.4 (c (n "soroswap-library") (v "0.0.4") (d (list (d (n "num-integer") (r "^0.1.45") (f (quote ("i128"))) (k 0)) (d (n "soroban-sdk") (r "^20.0.0-rc2") (d #t) (k 0)) (d (n "soroban-sdk") (r "^20.0.0-rc2") (f (quote ("testutils"))) (d #t) (k 2)))) (h "00h2r4vh3vcx39w64x3ahzdyj3h9jrhdd9m5wc9nnszxhdvklj5r") (r "1.73")))

(define-public crate-soroswap-library-0.0.5 (c (n "soroswap-library") (v "0.0.5") (d (list (d (n "num-integer") (r "^0.1.45") (f (quote ("i128"))) (k 0)) (d (n "soroban-sdk") (r "^20.0.0-rc2") (d #t) (k 0)) (d (n "soroban-sdk") (r "^20.0.0-rc2") (f (quote ("testutils"))) (d #t) (k 2)))) (h "1db4ycmfzwrfxwa5mwqbnam6ylnd4xjnks7yzynpc0921chqi7ra") (r "1.73")))

(define-public crate-soroswap-library-0.0.6 (c (n "soroswap-library") (v "0.0.6") (d (list (d (n "num-integer") (r "^0.1.45") (f (quote ("i128"))) (k 0)) (d (n "soroban-sdk") (r "^20.0.0-rc2") (d #t) (k 0)) (d (n "soroban-sdk") (r "^20.0.0-rc2") (f (quote ("testutils"))) (d #t) (k 2)))) (h "09zf43cyhhffv6gvw1yihzf52mp8hi471g1v35jcssvl6axzdqqc") (r "1.73")))

(define-public crate-soroswap-library-0.1.0 (c (n "soroswap-library") (v "0.1.0") (d (list (d (n "num-integer") (r "^0.1.45") (f (quote ("i128"))) (k 0)) (d (n "soroban-sdk") (r "^20.0.0-rc2") (d #t) (k 0)) (d (n "soroban-sdk") (r "^20.0.0-rc2") (f (quote ("testutils"))) (d #t) (k 2)))) (h "05hsdcb4hgcy8zirwa3j4cwvcp4419z8nh5k3grryhrv21cb6sdc") (r "1.73")))

(define-public crate-soroswap-library-0.1.1 (c (n "soroswap-library") (v "0.1.1") (d (list (d (n "num-integer") (r "^0.1.45") (f (quote ("i128"))) (k 0)) (d (n "soroban-sdk") (r "^20.0.0-rc2") (d #t) (k 0)) (d (n "soroban-sdk") (r "^20.0.0-rc2") (f (quote ("testutils"))) (d #t) (k 2)))) (h "151pp311qxyk3ll3x8cpxl9mjnx1wmb8z4n2l20xniyrm0jbpkl4") (r "1.73")))

(define-public crate-soroswap-library-0.2.1 (c (n "soroswap-library") (v "0.2.1") (d (list (d (n "num-integer") (r "^0.1.45") (f (quote ("i128"))) (k 0)) (d (n "soroban-sdk") (r "^20.0.0-rc2") (d #t) (k 0)) (d (n "soroban-sdk") (r "^20.0.0-rc2") (f (quote ("testutils"))) (d #t) (k 2)))) (h "0y832kxw58qfzdag5g62f0pqnbf5fhiwzfnhhsn5ay36x47a4nar") (r "1.73")))

(define-public crate-soroswap-library-0.2.2 (c (n "soroswap-library") (v "0.2.2") (d (list (d (n "num-integer") (r "^0.1.45") (f (quote ("i128"))) (k 0)) (d (n "soroban-sdk") (r "^20.0.0-rc2") (d #t) (k 0)) (d (n "soroban-sdk") (r "^20.0.0-rc2") (f (quote ("testutils"))) (d #t) (k 2)))) (h "01ym62padw96bcrxc5vvbcc83z6r4j167hbf1cfs76ki8712fvr7") (r "1.73")))

(define-public crate-soroswap-library-0.2.3 (c (n "soroswap-library") (v "0.2.3") (d (list (d (n "num-integer") (r "^0.1.45") (f (quote ("i128"))) (k 0)) (d (n "soroban-sdk") (r "^20.0.0-rc2") (d #t) (k 0)) (d (n "soroban-sdk") (r "^20.0.0-rc2") (f (quote ("testutils"))) (d #t) (k 2)))) (h "0fr9r79bhd86yd9rq3p093wrb002k4cqzv1k29c6bq77jsyw4c16") (r "1.73")))

(define-public crate-soroswap-library-0.2.4 (c (n "soroswap-library") (v "0.2.4") (d (list (d (n "num-integer") (r "^0.1.45") (f (quote ("i128"))) (k 0)) (d (n "soroban-sdk") (r "^20.0.0-rc2") (d #t) (k 0)) (d (n "soroban-sdk") (r "^20.0.0-rc2") (f (quote ("testutils"))) (d #t) (k 2)))) (h "0pnlsg0519h9055kpg4951yd7hsixks3cp70l40xm7d1y0bsjdfz") (r "1.73")))

(define-public crate-soroswap-library-0.3.0 (c (n "soroswap-library") (v "0.3.0") (d (list (d (n "num-integer") (r "^0.1.45") (f (quote ("i128"))) (k 0)) (d (n "soroban-sdk") (r "^20.2.0") (d #t) (k 0)) (d (n "soroban-sdk") (r "^20.2.0") (f (quote ("testutils"))) (d #t) (k 2)))) (h "1v2d9y8nnghk9ajymlzi776di73filhw1084qyxjp3wzk5c22d96") (r "1.73")))

