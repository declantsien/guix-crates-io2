(define-module (crates-io so ro soroban-env-guest) #:use-module (crates-io))

(define-public crate-soroban-env-guest-0.0.1 (c (n "soroban-env-guest") (v "0.0.1") (h "1dgxmpma95ps8wnb3k3v86xypgs2xrdxmfa6zbxwr1hmx7ax2j4d") (y #t)))

(define-public crate-soroban-env-guest-0.0.2 (c (n "soroban-env-guest") (v "0.0.2") (d (list (d (n "soroban-env-common") (r "^0.0.2") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "0xbzamaw4qdr4bd38bi7p7bjhr472kivmap7zsm3c6pq9ziyigds") (y #t)))

(define-public crate-soroban-env-guest-0.0.3 (c (n "soroban-env-guest") (v "0.0.3") (d (list (d (n "soroban-env-common") (r "^0.0.3") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "1l64zi97lvywhdk3cn96q9j38kd1r8kr81hd1ppsa0myg5f96fmj") (y #t)))

(define-public crate-soroban-env-guest-0.0.4 (c (n "soroban-env-guest") (v "0.0.4") (d (list (d (n "soroban-env-common") (r "^0.0.4") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "0dn8mjlgwa33210hf7iafjyhpamf0w7kakdm81rp4kpd5n19is7f") (y #t) (r "1.63")))

(define-public crate-soroban-env-guest-0.0.5 (c (n "soroban-env-guest") (v "0.0.5") (d (list (d (n "soroban-env-common") (r "^0.0.5") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "1pyqzzgcxvf21mh3iq1fb555p7jppxa1gpmwi0kwq9b4jj68r6hr") (y #t) (r "1.63")))

(define-public crate-soroban-env-guest-0.0.6 (c (n "soroban-env-guest") (v "0.0.6") (d (list (d (n "soroban-env-common") (r "^0.0.6") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "06rkyiqc08aw82y392xzwi1z6f04i9058a0960yi4ld87h5bcdnj") (y #t) (r "1.64")))

(define-public crate-soroban-env-guest-0.0.7 (c (n "soroban-env-guest") (v "0.0.7") (d (list (d (n "soroban-env-common") (r "^0.0.7") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "1vc7n3fk4crxn8gcwwh3fjzyj1z5gbb8fkzbwv04412zv22yw8d9") (y #t) (r "1.64")))

(define-public crate-soroban-env-guest-0.0.8 (c (n "soroban-env-guest") (v "0.0.8") (d (list (d (n "soroban-env-common") (r "^0.0.8") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "1hrm421lx4i9r1v5y156nmgx5w8dyk37x8j1xzdlkif7qfr0s5s9") (y #t) (r "1.64")))

(define-public crate-soroban-env-guest-0.0.9 (c (n "soroban-env-guest") (v "0.0.9") (d (list (d (n "soroban-env-common") (r "^0.0.9") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "0p5racbj43n3l5jniqva8q1l6wh6kk2rr5hvwac3ghsjr8lf85g3") (y #t) (r "1.64")))

(define-public crate-soroban-env-guest-0.0.10 (c (n "soroban-env-guest") (v "0.0.10") (d (list (d (n "soroban-env-common") (r "^0.0.10") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "1gg1925dc18lbzdvk97n2aj3h3lj401shjkgz6qp4br2drh2yns8") (y #t) (r "1.65")))

(define-public crate-soroban-env-guest-0.0.11 (c (n "soroban-env-guest") (v "0.0.11") (d (list (d (n "soroban-env-common") (r "^0.0.11") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "1kgwwxchf2kjpbprdp5b8nwn82bk593fwaxrsi9a16bb53r7hdlj") (y #t) (r "1.65")))

(define-public crate-soroban-env-guest-0.0.12 (c (n "soroban-env-guest") (v "0.0.12") (d (list (d (n "soroban-env-common") (r "^0.0.12") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "19h0g0m5c2akvi414nfj2ikncckw66vcx2dagdnw1clp3cmmvyx6") (y #t) (r "1.66")))

(define-public crate-soroban-env-guest-0.0.13 (c (n "soroban-env-guest") (v "0.0.13") (d (list (d (n "soroban-env-common") (r "^0.0.13") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "1b9lm4idyh18hwzllr247336sj9szq96r7dcpna1xqnpcafqxb83") (f (quote (("testutils" "soroban-env-common/testutils")))) (y #t) (r "1.67")))

(define-public crate-soroban-env-guest-0.0.14 (c (n "soroban-env-guest") (v "0.0.14") (d (list (d (n "soroban-env-common") (r "^0.0.14") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "0w49k4myd7lzzfz990pz4xb65qskkhl9zpzsyck1kgynksgnnrfk") (f (quote (("testutils" "soroban-env-common/testutils")))) (y #t) (r "1.67")))

(define-public crate-soroban-env-guest-0.0.15 (c (n "soroban-env-guest") (v "0.0.15") (d (list (d (n "soroban-env-common") (r "^0.0.15") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "13i90hz7gnl9d97f9vrfbpi56n16k6z6m8p8pdna1hj5mywddvrg") (f (quote (("testutils" "soroban-env-common/testutils")))) (y #t) (r "1.68")))

(define-public crate-soroban-env-guest-0.0.16 (c (n "soroban-env-guest") (v "0.0.16") (d (list (d (n "soroban-env-common") (r "^0.0.16") (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "00dkw5hx1qv8x8rrrjcz77gkgbwg5nd4ga6srqdbhiz3cqkp2iah") (f (quote (("testutils" "soroban-env-common/testutils")))) (y #t) (r "1.69")))

(define-public crate-soroban-env-guest-0.0.17 (c (n "soroban-env-guest") (v "0.0.17") (d (list (d (n "soroban-env-common") (r "^0.0.17") (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "1vcg72kxmgvkg8bamx7d1dyj756hy9nqpjp6g7z8qxvl9z7cvg4g") (f (quote (("testutils" "soroban-env-common/testutils")))) (y #t) (r "1.70")))

(define-public crate-soroban-env-guest-20.0.0-rc1 (c (n "soroban-env-guest") (v "20.0.0-rc1") (d (list (d (n "soroban-env-common") (r "^20.0.0-rc1") (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "14w1kh2mi5nm1g8af6nw50b3n79hgf3399rkldmxylffqm1vjlsk") (f (quote (("testutils" "soroban-env-common/testutils")))) (r "1.71")))

(define-public crate-soroban-env-guest-20.0.0-rc2 (c (n "soroban-env-guest") (v "20.0.0-rc2") (d (list (d (n "soroban-env-common") (r "^20.0.0-rc2") (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "0s17dx3xp9zi0lydckkkagh30q41ll9230i0k7zqfx3lpcz1n7xl") (f (quote (("testutils" "soroban-env-common/testutils")))) (r "1.71")))

(define-public crate-soroban-env-guest-20.0.0 (c (n "soroban-env-guest") (v "20.0.0") (d (list (d (n "soroban-env-common") (r "=20.0.0") (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "18vrhir8x08g0s39x453c41gzpwk7y74m0miqj241bszxwwafc9z") (f (quote (("testutils" "soroban-env-common/testutils") ("next" "soroban-env-common/next")))) (r "1.74")))

(define-public crate-soroban-env-guest-20.0.1 (c (n "soroban-env-guest") (v "20.0.1") (d (list (d (n "soroban-env-common") (r "=20.0.1") (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "1yfmzmz4qc1ljbki8qwaapv1ma7594zpi9f3a0aw7c3jdsvhlpwl") (f (quote (("testutils" "soroban-env-common/testutils") ("next" "soroban-env-common/next")))) (r "1.74")))

(define-public crate-soroban-env-guest-20.0.2 (c (n "soroban-env-guest") (v "20.0.2") (d (list (d (n "soroban-env-common") (r "=20.0.2") (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "14qa6g47979pzhffi68lbvnqr2p87y1hk1rgfbc7xpr71gjmddq6") (f (quote (("testutils" "soroban-env-common/testutils") ("next" "soroban-env-common/next")))) (r "1.74")))

(define-public crate-soroban-env-guest-20.1.0 (c (n "soroban-env-guest") (v "20.1.0") (d (list (d (n "soroban-env-common") (r "=20.1.0") (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "1ji8l0d79sljb2yw3i1ipg1ndf95ccdra6s7nhakar83c69i3w8g") (f (quote (("testutils" "soroban-env-common/testutils") ("next" "soroban-env-common/next")))) (r "1.74")))

(define-public crate-soroban-env-guest-20.1.1 (c (n "soroban-env-guest") (v "20.1.1") (d (list (d (n "soroban-env-common") (r "=20.1.1") (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "18sj97mnkjapw7b5gxmj33xgp8rmv5j8x7vz1nyfqg52nnj6na6y") (f (quote (("testutils" "soroban-env-common/testutils") ("next" "soroban-env-common/next")))) (r "1.74.0")))

(define-public crate-soroban-env-guest-20.2.0 (c (n "soroban-env-guest") (v "20.2.0") (d (list (d (n "soroban-env-common") (r "=20.2.0") (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "1x12716zf2g4prszplfbyr3x04vkx9gr161dikwi1qgzf5j5qzmx") (f (quote (("testutils" "soroban-env-common/testutils") ("next" "soroban-env-common/next")))) (r "1.74.0")))

(define-public crate-soroban-env-guest-20.2.1 (c (n "soroban-env-guest") (v "20.2.1") (d (list (d (n "soroban-env-common") (r "=20.2.1") (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "00djp3q3h01x7cb0jchw82rn215li5z8jgxz999q5m1m15ap6398") (f (quote (("testutils" "soroban-env-common/testutils") ("next" "soroban-env-common/next")))) (r "1.74.0")))

(define-public crate-soroban-env-guest-20.2.2 (c (n "soroban-env-guest") (v "20.2.2") (d (list (d (n "soroban-env-common") (r "=20.2.2") (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "0l338qljl72v5qpl0ygs70y0ccqwxm5f5k7fsphmzc4cc2rqg5cl") (f (quote (("testutils" "soroban-env-common/testutils") ("next" "soroban-env-common/next")))) (r "1.74.0")))

(define-public crate-soroban-env-guest-20.3.0 (c (n "soroban-env-guest") (v "20.3.0") (d (list (d (n "soroban-env-common") (r "=20.3.0") (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "0m3lkcaqpdfxfabw1xzqdpha1q3wz129nsx9fvlc3g2yplmcl8ji") (f (quote (("testutils" "soroban-env-common/testutils") ("next" "soroban-env-common/next")))) (r "1.74.0")))

(define-public crate-soroban-env-guest-21.0.0 (c (n "soroban-env-guest") (v "21.0.0") (d (list (d (n "soroban-env-common") (r "=21.0.0") (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "1hh6hnnnxxjp6x65l12lhbbbgaky4jin2dm5m7bzp3rh9ah4ncgp") (f (quote (("testutils" "soroban-env-common/testutils") ("next" "soroban-env-common/next")))) (r "1.74.0")))

(define-public crate-soroban-env-guest-21.0.1 (c (n "soroban-env-guest") (v "21.0.1") (d (list (d (n "soroban-env-common") (r "=21.0.1") (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "1n0956xpcnjgizcgi2h2m93kksmklkxkx1mpzx97wim8sxh9pwwl") (f (quote (("testutils" "soroban-env-common/testutils") ("next" "soroban-env-common/next")))) (r "1.74.0")))

(define-public crate-soroban-env-guest-21.0.2 (c (n "soroban-env-guest") (v "21.0.2") (d (list (d (n "soroban-env-common") (r "=21.0.2") (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "1ydalpljsg8z38bs5nsgfalg7i9g36wa5zmcp0l6ah82ar17l5x6") (f (quote (("testutils" "soroban-env-common/testutils") ("next" "soroban-env-common/next")))) (r "1.74.0")))

