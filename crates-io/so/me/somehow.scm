(define-module (crates-io so me somehow) #:use-module (crates-io))

(define-public crate-somehow-0.1.0 (c (n "somehow") (v "0.1.0") (h "10dh9fnaymlgjh78jhyfd0nwf1smm80m1x3jhvymnl1vmhpgkf6y")))

(define-public crate-somehow-0.1.1 (c (n "somehow") (v "0.1.1") (h "19hyrrxg2bz8jzpfqpis6rj68q2p84zcgc7dspbjcr1kbkl2wksp")))

(define-public crate-somehow-0.1.2 (c (n "somehow") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)))) (h "16zid73v0pxfi7vpb1fsjpkh8wjdmw11m7l02393x2ljg8rh8zsw")))

(define-public crate-somehow-0.1.3 (c (n "somehow") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)))) (h "16ngk9lbd35gphkhnnfsgx1mign2545w8fgh2jn3bj81i78vrj01")))

(define-public crate-somehow-0.1.4 (c (n "somehow") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "sudo") (r "^0.6") (d #t) (k 0)))) (h "07cxxs13i1p7ls3l4s3ixhdvbc9wr0xzm8q07ynayv4nm5ks7b0r")))

(define-public crate-somehow-0.1.5 (c (n "somehow") (v "0.1.5") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "sudo") (r "^0.6") (d #t) (k 0)))) (h "00bsy6wg2kiklfr8z5dw4x2jwk1nnhdzr0l174failbk89w4n793")))

