(define-module (crates-io so me some_blockchain) #:use-module (crates-io))

(define-public crate-some_blockchain-0.1.0 (c (n "some_blockchain") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "sha2") (r "^0.9.2") (d #t) (k 0)))) (h "1ffdmlanb4dkn3lw5jjv3y2s1q1fij1ckqzx26pbs2nqzidd9v2x")))

