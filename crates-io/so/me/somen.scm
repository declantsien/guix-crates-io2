(define-module (crates-io so me somen) #:use-module (crates-io))

(define-public crate-somen-0.1.0 (c (n "somen") (v "0.1.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "futures-core") (r "^0.3") (k 0)) (d (n "futures-io") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2") (d #t) (k 0)))) (h "03pfphhgn5s2w5n9gz52qbp4hbi7b8m6v721gjhspzil1q5hzivf") (f (quote (("std" "alloc" "futures-core/std" "futures-io") ("nightly") ("default" "std") ("alloc" "futures-core/alloc"))))))

(define-public crate-somen-0.2.0 (c (n "somen") (v "0.2.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "futures-core") (r "^0.3") (k 0)) (d (n "futures-io") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2") (d #t) (k 0)))) (h "1l04lm78lf7qgci6zimsrfzgpx5mg16379lx6kbk0xw6s6qamcj5") (f (quote (("std" "alloc" "futures-core/std" "futures-io") ("nightly") ("default" "std") ("alloc" "futures-core/alloc"))))))

(define-public crate-somen-0.3.0-beta (c (n "somen") (v "0.3.0-beta") (d (list (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "futures-core") (r "^0.3") (k 0)) (d (n "futures-io") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2") (d #t) (k 0)))) (h "18cpi4dnnav27m2xkvibfp4yfr74wcgif7xglgfi3y06qmibalb8") (f (quote (("std" "alloc" "futures-core/std" "futures-io") ("nightly") ("default" "std") ("alloc" "futures-core/alloc"))))))

(define-public crate-somen-0.3.0 (c (n "somen") (v "0.3.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "futures-core") (r "^0.3") (k 0)) (d (n "futures-io") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2") (d #t) (k 0)))) (h "1fnykm8rq4x0g728i7xfi5p5vi2smh8lnwvwdghzacfqj9l36b96") (f (quote (("std" "alloc" "futures-core/std" "futures-io") ("nightly") ("default" "std") ("alloc" "futures-core/alloc"))))))

(define-public crate-somen-0.3.1 (c (n "somen") (v "0.3.1") (d (list (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "futures-core") (r "^0.3") (k 0)) (d (n "futures-io") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2") (d #t) (k 0)))) (h "1m0n1zr3s0jqd32ipfm35lhls95c19xbcljxbixq3dsi16l531ls") (f (quote (("std" "alloc" "futures-core/std" "futures-io") ("nightly") ("default" "std") ("alloc" "futures-core/alloc"))))))

