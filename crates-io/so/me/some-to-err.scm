(define-module (crates-io so me some-to-err) #:use-module (crates-io))

(define-public crate-some-to-err-0.0.1 (c (n "some-to-err") (v "0.0.1") (h "12kbi68lb0qjyw7anwx2dqsad1s4dg274k78wmkd6ivbghycl6rc")))

(define-public crate-some-to-err-0.0.2 (c (n "some-to-err") (v "0.0.2") (h "0d229rw43svpi2smc7xaxy37kx953nq32pgc4cf8yrgnw3hppg2i")))

(define-public crate-some-to-err-0.1.0 (c (n "some-to-err") (v "0.1.0") (h "1qcq0ppxykyz32vnwkqq2fh2kv6hxgzi1gzvq6mrrzb9k9diqvw9")))

(define-public crate-some-to-err-0.1.1 (c (n "some-to-err") (v "0.1.1") (h "143d90bzfz7q65rq2qhh31hqvm57822n9zhmsn8s777xj7b7nisf")))

(define-public crate-some-to-err-0.2.0 (c (n "some-to-err") (v "0.2.0") (h "0lrzkbs969vg3ypwj4q4a4cvq4snk94xkz2wqc9nb0kb6b18g81d")))

(define-public crate-some-to-err-0.2.1 (c (n "some-to-err") (v "0.2.1") (h "1b9rgsgjh2hblsl612ksx63vzrq881szhppjw0gnwz4k3rhgv5cd")))

