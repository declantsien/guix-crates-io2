(define-module (crates-io so me somewithimage) #:use-module (crates-io))

(define-public crate-SomeWithImage-0.1.0 (c (n "SomeWithImage") (v "0.1.0") (d (list (d (n "image") (r "^0.24.5") (d #t) (k 0)))) (h "0mnhb1m2a40ahb8hbb26k0rvmk6ivyh07ybkk8m02di6460wzd1q")))

(define-public crate-SomeWithImage-0.1.1 (c (n "SomeWithImage") (v "0.1.1") (d (list (d (n "image") (r "^0.24.5") (d #t) (k 0)))) (h "19080k5x3a9s9raq3h01ay0dbj797a89hzxnmcbzbz7y4ryihf05")))

(define-public crate-SomeWithImage-0.1.2 (c (n "SomeWithImage") (v "0.1.2") (d (list (d (n "image") (r "^0.24.5") (d #t) (k 0)))) (h "0kdphyb6dhv0ij7z8x4rb7fw2kciqz0f4kjj20yal01530h340sd")))

(define-public crate-SomeWithImage-0.1.3 (c (n "SomeWithImage") (v "0.1.3") (d (list (d (n "image") (r "^0.24.5") (d #t) (k 0)))) (h "0baa21z4yf34gwa8q82437b4x5y263ajqjwc7c519s88jvp8h9gg")))

(define-public crate-SomeWithImage-0.1.4 (c (n "SomeWithImage") (v "0.1.4") (d (list (d (n "image") (r "^0.24.5") (d #t) (k 0)))) (h "0kc99zi9s7hi3693qs0af2py3mcmd99i22717q8b5cmd8yvvs508")))

(define-public crate-SomeWithImage-0.1.5 (c (n "SomeWithImage") (v "0.1.5") (d (list (d (n "image") (r "^0.24.5") (d #t) (k 0)))) (h "12pmb9jk3qyf55wv6ky3i4s7jgbzbqaws3dn1205qydmspan6fck")))

(define-public crate-SomeWithImage-0.1.6 (c (n "SomeWithImage") (v "0.1.6") (d (list (d (n "image") (r "^0.24.5") (d #t) (k 0)))) (h "1cxk6sp42ik3myy4vkvx9hlbn3mvm1bmgr2p77z3zgnmydx132vl")))

(define-public crate-SomeWithImage-0.1.7 (c (n "SomeWithImage") (v "0.1.7") (d (list (d (n "image") (r "^0.24.5") (d #t) (k 0)))) (h "0zg847nlvkl0hlnn00xlqrcfxcwsv7bqqfd845ybyz5qcx16r9gk")))

(define-public crate-SomeWithImage-0.1.8 (c (n "SomeWithImage") (v "0.1.8") (d (list (d (n "image") (r "^0.24.5") (d #t) (k 0)))) (h "1dbyk0pcxc73wazz91s82hajpr7s7ihnzx5yd8ma12c190aqs4wz")))

