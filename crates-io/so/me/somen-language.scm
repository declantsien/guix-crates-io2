(define-module (crates-io so me somen-language) #:use-module (crates-io))

(define-public crate-somen-language-0.1.0 (c (n "somen-language") (v "0.1.0") (d (list (d (n "compute-float") (r "^0.1.0") (d #t) (k 0)) (d (n "futures-executor") (r "^0.3") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (k 0)) (d (n "somen") (r "^0.3.0") (k 0)))) (h "0qrk27zqfiy3ww744cpvaji6m16z6s64rvj3dzl3cd0rbla9ym9p") (f (quote (("std" "alloc" "somen/std" "num-traits/std") ("nightly") ("libm" "num-traits/libm") ("default" "std") ("alloc" "somen/alloc"))))))

