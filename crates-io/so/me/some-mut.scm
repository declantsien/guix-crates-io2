(define-module (crates-io so me some-mut) #:use-module (crates-io))

(define-public crate-some-mut-0.1.0 (c (n "some-mut") (v "0.1.0") (h "1xy9fd2kfp1rjg8wax7xaz42vfwwdas0qpx30hwk4v78gmhd9rjx")))

(define-public crate-some-mut-0.1.1 (c (n "some-mut") (v "0.1.1") (h "1nf21sk9acli5xih2rd9azvgmyrvhn55fhx4bks1czn8f49hldp7")))

(define-public crate-some-mut-0.1.2 (c (n "some-mut") (v "0.1.2") (h "18pk840xcgdnw8d5cirr1w59hyrj613wn1nmkcpj45kfrn9q9iv9")))

