(define-module (crates-io so me someip_parse) #:use-module (crates-io))

(define-public crate-someip_parse-0.1.0 (c (n "someip_parse") (v "0.1.0") (d (list (d (n "assert_matches") (r "^1.3.0") (d #t) (k 2)) (d (n "byteorder") (r "^1.2.3") (d #t) (k 0)) (d (n "clap") (r "^2.32.0") (d #t) (k 2)) (d (n "etherparse") (r "^0.5.0") (d #t) (k 2)) (d (n "proptest") (r "^0.8.3") (d #t) (k 2)) (d (n "rpcap") (r "^0.3.0") (d #t) (k 2)) (d (n "time") (r "^0.1.40") (d #t) (k 2)))) (h "0srwiibrc4lmczmaxrjcmcfz6h6r45zx335s74savmmiql7yfmhr")))

(define-public crate-someip_parse-0.2.0 (c (n "someip_parse") (v "0.2.0") (d (list (d (n "assert_matches") (r "^1.3.0") (d #t) (k 2)) (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "clap") (r "^2.32.0") (d #t) (k 2)) (d (n "etherparse") (r "^0.8.0") (d #t) (k 2)) (d (n "proptest") (r "^0.9.1") (d #t) (k 2)) (d (n "rpcap") (r "^0.3.0") (d #t) (k 2)) (d (n "time") (r "^0.1.42") (d #t) (k 2)))) (h "18m8r4wz3fw5qg00vkml44g6xs46qdgi194mycncbag7ndwmpmzd")))

(define-public crate-someip_parse-0.3.0 (c (n "someip_parse") (v "0.3.0") (d (list (d (n "assert_matches") (r "^1.5.0") (d #t) (k 2)) (d (n "clap") (r "^2.33.3") (d #t) (k 2)) (d (n "etherparse") (r "^0.9.0") (d #t) (k 2)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "rpcap") (r "^0.3.0") (d #t) (k 2)) (d (n "time") (r "^0.3.1") (d #t) (k 2)))) (h "1qip3yk21q9vpivrvzfr3z4afgp3lnp31wwhjbd36j190kms36x0")))

(define-public crate-someip_parse-0.3.1 (c (n "someip_parse") (v "0.3.1") (d (list (d (n "assert_matches") (r "^1.5.0") (d #t) (k 2)) (d (n "clap") (r "^2.33.3") (d #t) (k 2)) (d (n "etherparse") (r "^0.9.0") (d #t) (k 2)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "rpcap") (r "^0.3.0") (d #t) (k 2)) (d (n "time") (r "^0.3.1") (d #t) (k 2)))) (h "0gdv5r7md6vndm3ykrra3ipd1m4jmqzvp0j8s387j80zsx94mxbp")))

(define-public crate-someip_parse-0.4.0 (c (n "someip_parse") (v "0.4.0") (d (list (d (n "assert_matches") (r "^1.5.0") (d #t) (k 2)) (d (n "clap") (r "^2.33.3") (d #t) (k 2)) (d (n "etherparse") (r "^0.9.0") (d #t) (k 2)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "rpcap") (r "^0.3.0") (d #t) (k 2)) (d (n "time") (r "^0.3.3") (d #t) (k 2)))) (h "0ip7ywcdxmgp75bmcvi0250mvv05fbjsark0vq93qja1xz67iiq5")))

(define-public crate-someip_parse-0.5.0 (c (n "someip_parse") (v "0.5.0") (d (list (d (n "assert_matches") (r "^1.5.0") (d #t) (k 2)) (d (n "etherparse") (r "^0.13.0") (d #t) (k 2)) (d (n "proptest") (r "^1.3.1") (d #t) (k 2)) (d (n "rpcap") (r "^1.0.0") (d #t) (k 2)))) (h "1ghfwjyrxj98p8cy0drwkccaashyxkxcdbdji6c6mk770d5kaqls") (r "1.60")))

(define-public crate-someip_parse-0.6.0 (c (n "someip_parse") (v "0.6.0") (d (list (d (n "assert_matches") (r "^1.5.0") (d #t) (k 2)) (d (n "etherparse") (r "^0.13.0") (d #t) (k 2)) (d (n "proptest") (r "^1.3.1") (d #t) (k 2)) (d (n "rpcap") (r "^1.0.0") (d #t) (k 2)))) (h "0jc2jb7p0cd1x2qbyqaqkm1icag63cgy1jdvhiwkzycpvcxkkqsw") (r "1.60")))

(define-public crate-someip_parse-0.6.1 (c (n "someip_parse") (v "0.6.1") (d (list (d (n "assert_matches") (r "^1.5.0") (d #t) (k 2)) (d (n "etherparse") (r "^0.13.0") (d #t) (k 2)) (d (n "proptest") (r "^1.3.1") (d #t) (k 2)) (d (n "rpcap") (r "^1.0.0") (d #t) (k 2)))) (h "1i8bi5kjj5y3bxbq3qglaxvqjnnlqr1mi9ffsdnc908xdvkgjcsv") (r "1.60")))

