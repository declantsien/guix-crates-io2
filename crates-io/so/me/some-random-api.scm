(define-module (crates-io so me some-random-api) #:use-module (crates-io))

(define-public crate-some-random-api-0.1.0 (c (n "some-random-api") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1agz7m0qywgqghpv4pgp2qfkpsf6n7s7fldhl24d0x5qzimxjr1f")))

(define-public crate-some-random-api-0.2.0 (c (n "some-random-api") (v "0.2.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0k8mx263psj1y6cakriaa9rh1n6xz06r905ss8c93ag2ldzgq51r")))

(define-public crate-some-random-api-0.2.1 (c (n "some-random-api") (v "0.2.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0l7cs23qknf2zbv70inkmxhavd4115l87wwj814lz58cwsm9azhj")))

(define-public crate-some-random-api-0.3.0 (c (n "some-random-api") (v "0.3.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "serde_repr") (r "^0.1") (d #t) (k 0)))) (h "0fyq6zb5mpxsb8izqiwclmshr8hxbb86j12b3il5pp7ny6mx7c4j")))

