(define-module (crates-io so me somefiletools) #:use-module (crates-io))

(define-public crate-somefiletools-0.0.1 (c (n "somefiletools") (v "0.0.1") (h "09h736niix2b89ixzw1ramflw1w3832sy30cgivy3wqasc4fy290")))

(define-public crate-somefiletools-0.0.2 (c (n "somefiletools") (v "0.0.2") (h "15q5v54f0xay3vgf1nm9x5pli7f538yyxgq6s2k6qnmhprq8nmiy")))

