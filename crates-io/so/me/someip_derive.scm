(define-module (crates-io so me someip_derive) #:use-module (crates-io))

(define-public crate-someip_derive-0.1.0 (c (n "someip_derive") (v "0.1.0") (d (list (d (n "derive-syn-parse") (r "^0.1.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0frrk3rzxl0dbc87hz27ql3k3qdsiizhkg3xa5ixrv5ks8zw84jd")))

