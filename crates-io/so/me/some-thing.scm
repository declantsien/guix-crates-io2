(define-module (crates-io so me some-thing) #:use-module (crates-io))

(define-public crate-some-thing-0.1.0 (c (n "some-thing") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "clap") (r "=3.0.0-beta.2") (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "dialoguer") (r "^0.6") (d #t) (k 0)) (d (n "indicatif") (r "^0.15") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.8") (k 0)) (d (n "rusqlite") (r "^0.24") (f (quote ("bundled"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "url") (r "^2.1") (d #t) (k 0)))) (h "04h0jhzijjh651bjqdb5ky64ibp3pzf8z5diky3np8sdj3yvl77y")))

