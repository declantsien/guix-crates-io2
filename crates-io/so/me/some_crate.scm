(define-module (crates-io so me some_crate) #:use-module (crates-io))

(define-public crate-some_crate-0.1.0 (c (n "some_crate") (v "0.1.0") (h "0hwz05zsdk698kawdnp6g35jyn1c0z58dlh5rn37s3c5sy5nc1mm")))

(define-public crate-some_crate-0.1.1 (c (n "some_crate") (v "0.1.1") (h "0x1wq32rb6d8i19khvxys1qj6szicysqa2b4kh12wb438p3w0dwh")))

