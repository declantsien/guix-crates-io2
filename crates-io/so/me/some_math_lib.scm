(define-module (crates-io so me some_math_lib) #:use-module (crates-io))

(define-public crate-some_math_lib-0.1.0 (c (n "some_math_lib") (v "0.1.0") (h "0qx9j0sxl6pgpv31sdgs377zr2kc7laxax2hidh638a2ja6s9mam")))

(define-public crate-some_math_lib-0.1.1 (c (n "some_math_lib") (v "0.1.1") (h "0vya6jbirqif20w5flxh89dngcnf5s9cd6v35f0lilpfgkp5ri51")))

(define-public crate-some_math_lib-0.1.2 (c (n "some_math_lib") (v "0.1.2") (h "0lrcsham56q1y1xk8r5nkjr0ccpx8jf82g22dnhgahfidrx8riga")))

