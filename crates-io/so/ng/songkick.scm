(define-module (crates-io so ng songkick) #:use-module (crates-io))

(define-public crate-songkick-0.1.0 (c (n "songkick") (v "0.1.0") (d (list (d (n "hyper") (r "^0.9.10") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.6.6") (d #t) (k 2)) (d (n "serde") (r "^0.8.19") (d #t) (k 0)) (d (n "serde_json") (r "^0.8.4") (d #t) (k 0)) (d (n "shrust") (r "^0.0.4") (d #t) (k 2)) (d (n "url") (r "^1.2.1") (d #t) (k 0)))) (h "1rxyrrbj27gyri9jvfj8hkblxwr7662kmnyqaijhlds723lv7dfw")))

