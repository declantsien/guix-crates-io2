(define-module (crates-io so ng songww-harfbuzz-rs) #:use-module (crates-io))

(define-public crate-songww-harfbuzz-rs-0.1.0 (c (n "songww-harfbuzz-rs") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "harfbuzz-sys") (r "^0.1.0") (k 0) (p "songww-harfbuzz-sys")) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1rbkcyzphk8fdnq4fa27lg0i5q6qidbq2085gqzn3v8928rwdhpn") (f (quote (("vendored" "harfbuzz-sys/vendored") ("default" "vendored") ("bindgen" "harfbuzz-sys/bindgen"))))))

