(define-module (crates-io so fi sofiza) #:use-module (crates-io))

(define-public crate-sofiza-0.0.0 (c (n "sofiza") (v "0.0.0") (h "059fjhh0cmfwrjiiga79ircq9pa2rs1nbv3j3y8y49jb3vgiimw2")))

(define-public crate-sofiza-0.1.0 (c (n "sofiza") (v "0.1.0") (d (list (d (n "logos") (r "^0.11.4") (d #t) (k 0)) (d (n "never") (r "^0.1.0") (d #t) (k 0)) (d (n "path-slash") (r "^0.1.3") (d #t) (k 0)) (d (n "phf") (r "^0.8") (f (quote ("macros"))) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "shellexpand") (r "^1.1.1") (d #t) (k 0)))) (h "0aca6idk63nyg3ifsd09mc1n8w8dbkbbww1n4013l6cpl6a8g9k8")))

(define-public crate-sofiza-0.1.1 (c (n "sofiza") (v "0.1.1") (d (list (d (n "logos") (r "^0.11.4") (d #t) (k 0)) (d (n "never") (r "^0.1.0") (d #t) (k 0)) (d (n "phf") (r "^0.8") (f (quote ("macros"))) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "shellexpand") (r "^1.1.1") (d #t) (k 0)))) (h "0rn0sz2xawg1b3dw68zxksamq6a4j7257fndlawq3a9dsmhq4fd4")))

(define-public crate-sofiza-0.1.2 (c (n "sofiza") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "logos") (r "^0.11.4") (d #t) (k 0)) (d (n "never") (r "^0.1.0") (d #t) (k 0)) (d (n "phf") (r "^0.8") (f (quote ("macros"))) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "shellexpand") (r "^1.1.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "082v58m350n8fxg7s8wmcfapl3yshxj19g2y1plqalj446f37c94")))

(define-public crate-sofiza-0.1.3 (c (n "sofiza") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "logos") (r "^0.11") (d #t) (k 0)) (d (n "never") (r "^0.1.0") (d #t) (k 0)) (d (n "phf") (r "^0.8") (f (quote ("macros"))) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1dwrkmh6c8s7ndrp05jv6j1fbj65gy23clisqns6zffqkgdwh3s0")))

(define-public crate-sofiza-0.2.0 (c (n "sofiza") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "logos") (r "^0.12") (d #t) (k 0)) (d (n "never") (r "^0.1") (d #t) (k 0)) (d (n "phf") (r "^0.8") (f (quote ("macros"))) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "08zwi5mly6y2yv2xfayfl9ciax2nn8vcsjb2d0hjl7h40334wkcx")))

(define-public crate-sofiza-0.2.1 (c (n "sofiza") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "logos") (r "^0.12") (d #t) (k 0)) (d (n "never") (r "^0.1") (d #t) (k 0)) (d (n "phf") (r "^0.8") (f (quote ("macros"))) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1fqf5iw2bwvc9lwzr2dlp75bhy4h3pic94jqx15wn4wn8kh00dv7")))

(define-public crate-sofiza-0.2.2 (c (n "sofiza") (v "0.2.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "logos") (r "^0.12") (d #t) (k 0)) (d (n "never") (r "^0.1") (d #t) (k 0)) (d (n "phf") (r "^0.8") (f (quote ("macros"))) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0fa2naq942xsmqzx9qpy2jy5l7gv89rzgr4gk00pljxg507p1qmi")))

(define-public crate-sofiza-0.2.3 (c (n "sofiza") (v "0.2.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "logos") (r "^0.12") (d #t) (k 0)) (d (n "never") (r "^0.1") (d #t) (k 0)) (d (n "phf") (r "^0.10") (f (quote ("macros"))) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0v9fn393l46a2h6zxnbix46wyms5bxpa49npjbiwfjhrza3arzk1")))

(define-public crate-sofiza-0.3.0 (c (n "sofiza") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "logos") (r "^0.12.1") (d #t) (k 0)) (d (n "never") (r "^0.1") (d #t) (k 0)) (d (n "phf") (r "^0.11.1") (f (quote ("macros"))) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "04cx4i5i533x19dw7370akyqblnvks9js4km2qx25ysp390pvacz")))

(define-public crate-sofiza-0.3.1 (c (n "sofiza") (v "0.3.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "logos") (r "^0.12.1") (d #t) (k 0)) (d (n "never") (r "^0.1") (d #t) (k 0)) (d (n "phf") (r "^0.11.1") (f (quote ("macros"))) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0chl8ym1hhvvgccfrcj0s76570ab4lp0hfhjaxgmj3ikk5yiigib")))

