(define-module (crates-io so fi sofia-sip) #:use-module (crates-io))

(define-public crate-sofia-sip-0.0.1 (c (n "sofia-sip") (v "0.0.1") (d (list (d (n "bindgen") (r "^0.58.1") (d #t) (k 1)) (d (n "errno") (r "^0.2.7") (d #t) (k 0)) (d (n "metadeps") (r "^1.1.2") (d #t) (k 1)) (d (n "optargs") (r "^0.1.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.19") (d #t) (k 1)))) (h "10kqbhmlrc53i9zidn9xikv1nv44330qwd7w23hwjj3vsksmbn2a") (y #t) (l "sofia-sip-ua")))

(define-public crate-sofia-sip-0.1.0 (c (n "sofia-sip") (v "0.1.0") (d (list (d (n "adorn") (r "^0.4.0") (d #t) (k 2)) (d (n "bindgen") (r "^0.58.1") (d #t) (k 1)) (d (n "errno") (r "^0.2.7") (d #t) (k 0)) (d (n "metadeps") (r "^1.1.2") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.19") (d #t) (k 1)) (d (n "serial_test") (r "^0.5.1") (d #t) (k 2)))) (h "1gj7pdgqlmhv93m1h5ijfaapr9v7jprzxyfbj8mq83appcyn3lp3") (l "sofia-sip-ua")))

