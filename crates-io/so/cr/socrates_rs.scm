(define-module (crates-io so cr socrates_rs) #:use-module (crates-io))

(define-public crate-socrates_rs-1.0.0 (c (n "socrates_rs") (v "1.0.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0gblhsv66m4ijx8byh4chwf82jfdp85nnx4qa7y92in83dfp8m6g")))

