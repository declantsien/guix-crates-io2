(define-module (crates-io so uk soukoban) #:use-module (crates-io))

(define-public crate-soukoban-0.1.0 (c (n "soukoban") (v "0.1.0") (d (list (d (n "bitflags") (r "^2.4") (d #t) (k 0)) (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "indoc") (r "^2.0") (d #t) (k 2)) (d (n "itertools") (r "^0.12") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0bnqpcgv0gxphsqrqby3wv01g5748n2bk9cl0xf0q155q8xzgcvh")))

