(define-module (crates-io so nt sontonio-iam) #:use-module (crates-io))

(define-public crate-sontonio-iam-0.1.0 (c (n "sontonio-iam") (v "0.1.0") (d (list (d (n "axum") (r "^0.7.3") (d #t) (k 0)) (d (n "rusty-hook") (r "^0.11.2") (d #t) (k 2)) (d (n "tokio") (r "^1.35.1") (f (quote ("rt-multi-thread"))) (d #t) (k 0)) (d (n "tower") (r "^0.4.13") (d #t) (k 2)))) (h "1kljmpwqkd2b0cqd7nw97myf2zxm02bwk1v3lr33hckkpmlbfc36") (y #t)))

(define-public crate-sontonio-iam-0.1.1 (c (n "sontonio-iam") (v "0.1.1") (d (list (d (n "axum") (r "^0.7.3") (d #t) (k 0)) (d (n "rusty-hook") (r "^0.11.2") (d #t) (k 2)) (d (n "tokio") (r "^1.35.1") (f (quote ("rt-multi-thread"))) (d #t) (k 0)) (d (n "tower") (r "^0.4.13") (d #t) (k 2)))) (h "1l26j4lfxslcyi7jx0ybhcw2mb38kl9ab332mvyriijxs80mikqc") (y #t)))

(define-public crate-sontonio-iam-0.1.2 (c (n "sontonio-iam") (v "0.1.2") (d (list (d (n "axum") (r "^0.7.3") (d #t) (k 0)) (d (n "rusty-hook") (r "^0.11.2") (d #t) (k 2)) (d (n "tokio") (r "^1.35.1") (f (quote ("rt-multi-thread"))) (d #t) (k 0)) (d (n "tower") (r "^0.4.13") (d #t) (k 2)))) (h "03swm7vwyap08za96cyr0xh7isvybjlw28d9rp1ira1qb89cfar5") (y #t)))

(define-public crate-sontonio-iam-0.1.3 (c (n "sontonio-iam") (v "0.1.3") (d (list (d (n "axum") (r "^0.7.3") (d #t) (k 0)) (d (n "rusty-hook") (r "^0.11.2") (d #t) (k 2)) (d (n "tokio") (r "^1.35.1") (f (quote ("rt-multi-thread"))) (d #t) (k 0)) (d (n "tower") (r "^0.4.13") (d #t) (k 2)))) (h "1bn1h4qfnlp822c1yq6jin767vdg8imqxwbrgmx8f41c4vfz1pi7") (y #t)))

(define-public crate-sontonio-iam-0.1.4 (c (n "sontonio-iam") (v "0.1.4") (d (list (d (n "axum") (r "^0.7.3") (d #t) (k 0)) (d (n "rusty-hook") (r "^0.11.2") (d #t) (k 2)) (d (n "tokio") (r "^1.35.1") (f (quote ("rt-multi-thread"))) (d #t) (k 0)) (d (n "tower") (r "^0.4.13") (d #t) (k 2)))) (h "163lib62pfcp17266k26jihjpg2i6sjyz9lybmn6vipqqm64z1dy") (y #t)))

(define-public crate-sontonio-iam-0.1.5 (c (n "sontonio-iam") (v "0.1.5") (d (list (d (n "axum") (r "^0.7.3") (d #t) (k 0)) (d (n "rusty-hook") (r "^0.11.2") (d #t) (k 2)) (d (n "tokio") (r "^1.35.1") (f (quote ("rt-multi-thread"))) (d #t) (k 0)) (d (n "tower") (r "^0.4.13") (d #t) (k 2)))) (h "0bb9s1x2135l8ilqv25nk3i9qj8jd3k1wkq0yzn05j3xb1rn5whm") (y #t)))

(define-public crate-sontonio-iam-0.1.6 (c (n "sontonio-iam") (v "0.1.6") (d (list (d (n "axum") (r "^0.7.3") (d #t) (k 0)) (d (n "rusty-hook") (r "^0.11.2") (d #t) (k 2)) (d (n "tokio") (r "^1.35.1") (f (quote ("rt-multi-thread"))) (d #t) (k 0)) (d (n "tower") (r "^0.4.13") (d #t) (k 2)))) (h "0f6azxcl4xxihl4ywprqj1kj66vrb371ihj2n4922dy0hlkrsb13") (y #t)))

(define-public crate-sontonio-iam-0.1.7 (c (n "sontonio-iam") (v "0.1.7") (d (list (d (n "axum") (r "^0.7.3") (d #t) (k 0)) (d (n "rusty-hook") (r "^0.11.2") (d #t) (k 2)) (d (n "tokio") (r "^1.35.1") (f (quote ("rt-multi-thread"))) (d #t) (k 0)) (d (n "tower") (r "^0.4.13") (d #t) (k 2)))) (h "1941p6acc2p0ilz2jzbr108l0njnkry77dcjgrp04zk0aswqg7lp") (y #t)))

(define-public crate-sontonio-iam-0.1.8 (c (n "sontonio-iam") (v "0.1.8") (d (list (d (n "axum") (r "^0.7.3") (d #t) (k 0)) (d (n "rusty-hook") (r "^0.11.2") (d #t) (k 2)) (d (n "tokio") (r "^1.35.1") (f (quote ("rt-multi-thread"))) (d #t) (k 0)) (d (n "tower") (r "^0.4.13") (d #t) (k 2)))) (h "1yanvwqx64p54bwxjbs8531sxidxnhd3kbj2nfpawfmvyplrahrc") (y #t)))

(define-public crate-sontonio-iam-0.1.11 (c (n "sontonio-iam") (v "0.1.11") (d (list (d (n "axum") (r "^0.7.3") (d #t) (k 0)) (d (n "rusty-hook") (r "^0.11.2") (d #t) (k 2)) (d (n "tokio") (r "^1.35.1") (f (quote ("rt-multi-thread"))) (d #t) (k 0)) (d (n "tower") (r "^0.4.13") (d #t) (k 2)))) (h "1ckwswnf4s4nqaxgxirgmydh1k7p5f3y6v3xwa55fj4aqbnc1y16") (y #t)))

(define-public crate-sontonio-iam-0.1.18-alpha.2 (c (n "sontonio-iam") (v "0.1.18-alpha.2") (d (list (d (n "axum") (r "^0.7.3") (d #t) (k 0)) (d (n "rusty-hook") (r "^0.11.2") (d #t) (k 2)) (d (n "tokio") (r "^1.35.1") (f (quote ("rt-multi-thread"))) (d #t) (k 0)) (d (n "tower") (r "^0.4.13") (d #t) (k 2)))) (h "05xvljncmz4s5qxkim21706z0b4nmxmhjn8yz85ifffjyxz10kj2") (y #t)))

