(define-module (crates-io so ar soar) #:use-module (crates-io))

(define-public crate-soar-0.1.0 (c (n "soar") (v "0.1.0") (d (list (d (n "actix") (r "^0.7.6") (d #t) (k 0)) (d (n "env_logger") (r "^0.5.13") (d #t) (k 0)) (d (n "futures") (r "^0.1.25") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "query_interface") (r "^0.3.5") (d #t) (k 0)))) (h "0mkdd9xmikbni4l0m5dhidrs24n8zzrrdlfifxal4glxjrjv8bri")))

