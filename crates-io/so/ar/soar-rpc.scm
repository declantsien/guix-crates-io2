(define-module (crates-io so ar soar-rpc) #:use-module (crates-io))

(define-public crate-soar-rpc-0.1.0 (c (n "soar-rpc") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "ureq") (r "^2.4.0") (d #t) (k 0)))) (h "19c6rsqdj2mh3flkpvgrqlswkyfkbzs5r1z4fr9k048bvailgf6f")))

(define-public crate-soar-rpc-0.1.1 (c (n "soar-rpc") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "ureq") (r "^2.4.0") (d #t) (k 0)))) (h "0xmmnaq5j2c6a2xfm5rb7804isi1vfhp8s2rdwvn8l0sajm6c07y")))

(define-public crate-soar-rpc-0.1.2 (c (n "soar-rpc") (v "0.1.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "ureq") (r "^2.4.0") (d #t) (k 0)))) (h "16c58yf1jissqjg6v9hijsmz4x19vvrrvf59abml4ms16g6i4vwq")))

(define-public crate-soar-rpc-0.1.3 (c (n "soar-rpc") (v "0.1.3") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "ureq") (r "^2.4.0") (d #t) (k 0)))) (h "0cp8icaqdpxx1fqwhjci18j0ika5sypgzsw62n6pnvjsy0mzb72a")))

(define-public crate-soar-rpc-0.1.4 (c (n "soar-rpc") (v "0.1.4") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "ureq") (r "^2.4.0") (d #t) (k 0)))) (h "0c5z2zn8vl6plh150a37iw9kqfhxihrdinfkbcdi8favwwy1l7c6")))

