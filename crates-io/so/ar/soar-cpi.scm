(define-module (crates-io so ar soar-cpi) #:use-module (crates-io))

(define-public crate-soar-cpi-0.1.0 (c (n "soar-cpi") (v "0.1.0") (d (list (d (n "anchor-lang") (r "^0.26.0") (d #t) (k 0)) (d (n "clockwork-anchor-gen") (r "^0.3.2") (d #t) (k 0)))) (h "1lzny6hl5j412yrsn81gkzjy1y7h4dfxh7p95f9w7rplzwjfn4bs") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default" "cpi") ("cpi" "no-entrypoint"))))))

(define-public crate-soar-cpi-0.1.1 (c (n "soar-cpi") (v "0.1.1") (d (list (d (n "anchor-lang") (r "^0.28.0") (d #t) (k 0)) (d (n "clockwork-anchor-gen") (r "^0.3.2") (d #t) (k 0)))) (h "1cxrs03fb5wh0z6cb07bmj274nww2kc43b6g5xdsv09ncyra8mli") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default" "cpi") ("cpi" "no-entrypoint"))))))

(define-public crate-soar-cpi-0.1.2 (c (n "soar-cpi") (v "0.1.2") (d (list (d (n "anchor-lang") (r "^0.28.0") (d #t) (k 0)) (d (n "clockwork-anchor-gen") (r "^0.3.2") (d #t) (k 0)))) (h "06v59bmlmgyvm54wsi3q1lww623zwcrw4s1k017rip2x33s000cf") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default" "cpi") ("cpi" "no-entrypoint"))))))

(define-public crate-soar-cpi-0.1.3 (c (n "soar-cpi") (v "0.1.3") (d (list (d (n "anchor-lang") (r "^0.29.0") (d #t) (k 0)) (d (n "clockwork-anchor-gen") (r "^0.3.2") (d #t) (k 0)))) (h "1ml52wrzc1ww6xfis373cwaxnx26hxhcy288wnkmaxk7zc8gh3sm") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default" "cpi") ("cpi" "no-entrypoint"))))))

(define-public crate-soar-cpi-0.1.4 (c (n "soar-cpi") (v "0.1.4") (d (list (d (n "anchor-lang") (r "^0.29.0") (d #t) (k 0)) (d (n "clockwork-anchor-gen") (r "^0.3.2") (d #t) (k 0)))) (h "0b41j7dw8x441bw9zxiaq8x5qgrjynbzw61jwbwrbfry64ljf821") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default" "cpi") ("cpi" "no-entrypoint"))))))

