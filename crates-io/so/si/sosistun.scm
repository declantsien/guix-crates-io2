(define-module (crates-io so si sosistun) #:use-module (crates-io))

(define-public crate-sosistun-1.0.0 (c (n "sosistun") (v "1.0.0") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rand_core") (r "^0.6.3") (d #t) (k 0)) (d (n "smol") (r "^1.2.5") (d #t) (k 0)) (d (n "sosistab") (r "^0.4.16") (d #t) (k 0)) (d (n "x25519-dalek") (r "^1.1.1") (f (quote ("serde"))) (d #t) (k 0)))) (h "090vvs7cgzrlfck29y63grig7gcry6z2shfqg8jd6sivncw25sd6") (y #t)))

(define-public crate-sosistun-1.0.1 (c (n "sosistun") (v "1.0.1") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rand_core") (r "^0.6.3") (d #t) (k 0)) (d (n "smol") (r "^1.2.5") (d #t) (k 0)) (d (n "sosistab") (r "^0.4.16") (d #t) (k 0)) (d (n "x25519-dalek") (r "^1.1.1") (f (quote ("serde"))) (d #t) (k 0)))) (h "1vi53jfyjlxsx54iafd9m8q1da75rn0x0rvhlyky6dcvswys4f6c") (y #t)))

(define-public crate-sosistun-1.0.2 (c (n "sosistun") (v "1.0.2") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rand_core") (r "^0.6.3") (d #t) (k 0)) (d (n "smol") (r "^1.2.5") (d #t) (k 0)) (d (n "sosistab") (r "^0.4.16") (d #t) (k 0)) (d (n "x25519-dalek") (r "^1.1.1") (f (quote ("serde"))) (d #t) (k 0)))) (h "1qcfb9h4zhshj9vvr33wg9xp56x4p5qf5r83nyjpd1agyd1w8yjc") (y #t)))

(define-public crate-sosistun-1.0.3 (c (n "sosistun") (v "1.0.3") (d (list (d (n "dirs") (r "^3.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rand_core") (r "^0.6.3") (d #t) (k 0)) (d (n "smol") (r "^1.2.5") (d #t) (k 0)) (d (n "sosistab") (r "^0.4.16") (d #t) (k 0)) (d (n "x25519-dalek") (r "^1.1.1") (f (quote ("serde"))) (d #t) (k 0)))) (h "1mhbiyh37raxj1ywakvixmc3clly6sb1zayr6iqi6lk1jnq1c9f6") (y #t)))

(define-public crate-sosistun-2.0.0 (c (n "sosistun") (v "2.0.0") (d (list (d (n "async-recursion") (r "^0.2") (d #t) (k 0)) (d (n "dirs") (r "^3.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rand_core") (r "^0.6.3") (d #t) (k 0)) (d (n "smol") (r "^1.2.5") (d #t) (k 0)) (d (n "sosistab") (r "^0.4.16") (d #t) (k 0)) (d (n "x25519-dalek") (r "^1.1.1") (f (quote ("serde"))) (d #t) (k 0)))) (h "1v6y3rjxh7lxhkzga3a7kn99g0adn5rm6z604fjvcja5f03524dr") (y #t)))

(define-public crate-sosistun-2.0.1 (c (n "sosistun") (v "2.0.1") (d (list (d (n "async-recursion") (r "^0.2") (d #t) (k 0)) (d (n "dirs") (r "^3.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rand_core") (r "^0.6.3") (d #t) (k 0)) (d (n "smol") (r "^1.2.5") (d #t) (k 0)) (d (n "sosistab") (r "^0.4.16") (d #t) (k 0)) (d (n "x25519-dalek") (r "^1.1.1") (f (quote ("serde"))) (d #t) (k 0)))) (h "1lignfxirz9sjl2q2883qb9jkpd7wg3cli5qc66v1yj19csicydp") (y #t)))

(define-public crate-sosistun-2.0.2 (c (n "sosistun") (v "2.0.2") (d (list (d (n "async-recursion") (r "^0.2") (d #t) (k 0)) (d (n "dirs") (r "^3.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rand_core") (r "^0.6.3") (d #t) (k 0)) (d (n "smol") (r "^1.2.5") (d #t) (k 0)) (d (n "sosistab") (r "^0.4.20") (d #t) (k 0)) (d (n "x25519-dalek") (r "^1.1.1") (f (quote ("serde"))) (d #t) (k 0)))) (h "002pyb88rh0siv0ncslasr3d9rwyrd0s770vzppypw6cybk5p0nd") (y #t)))

