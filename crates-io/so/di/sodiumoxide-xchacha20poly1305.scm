(define-module (crates-io so di sodiumoxide-xchacha20poly1305) #:use-module (crates-io))

(define-public crate-sodiumoxide-xchacha20poly1305-0.1.0 (c (n "sodiumoxide-xchacha20poly1305") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.41") (k 0)) (d (n "libsodium-sys") (r "^0.1") (d #t) (k 0)) (d (n "rmp-serde") (r "^0.13.7") (d #t) (k 2)) (d (n "rustc-serialize") (r "^0.3.24") (d #t) (k 2)) (d (n "serde") (r "^1.0.59") (o #t) (k 0)) (d (n "serde") (r "^1.0.59") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.17") (d #t) (k 2)))) (h "1bijgj5ms1snnp4b39ypmzfxgk2qmn75c43pscmsyphqv2xc77dh") (f (quote (("std") ("default" "serde" "std") ("benchmarks")))) (y #t)))

