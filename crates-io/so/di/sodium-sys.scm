(define-module (crates-io so di sodium-sys) #:use-module (crates-io))

(define-public crate-sodium-sys-0.0.1 (c (n "sodium-sys") (v "0.0.1") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "pkg-config") (r "*") (d #t) (k 1)) (d (n "regex") (r "*") (d #t) (k 2)))) (h "12sfr6vnmc6m0p7jwzgam63kf63igiss0kqsdgsjgrxpdjabhfp0") (f (quote (("latest"))))))

(define-public crate-sodium-sys-0.0.2 (c (n "sodium-sys") (v "0.0.2") (d (list (d (n "libc") (r "~0.1.12") (d #t) (k 0)) (d (n "pkg-config") (r "~0.3.6") (d #t) (k 1)) (d (n "regex") (r "*") (d #t) (k 2)))) (h "111wz41glclynifzgwg6s0dqxhxl0dw49mn715z289dmwcj0sqhw") (f (quote (("pwhash_bench") ("mlock_tests") ("latest"))))))

(define-public crate-sodium-sys-0.0.3 (c (n "sodium-sys") (v "0.0.3") (d (list (d (n "libc") (r "~0.2.2") (d #t) (k 0)) (d (n "pkg-config") (r "~0.3.6") (d #t) (k 1)) (d (n "regex") (r "~0.1.41") (d #t) (k 2)))) (h "0a7f4dd6z3syap5h0pkpjng0zg7ncfyb3dwabi8gzc24p68r69cz") (f (quote (("pwhash_bench") ("mlock_tests") ("latest"))))))

(define-public crate-sodium-sys-0.0.4 (c (n "sodium-sys") (v "0.0.4") (d (list (d (n "libc") (r "~0.2.2") (d #t) (k 0)) (d (n "pkg-config") (r "~0.3.6") (d #t) (k 1)) (d (n "regex") (r "~0.1.41") (d #t) (k 2)))) (h "1rmyvadyrg8amsazkwbpd95dgplp7gz2gisr2bigzw4vvlp12vzr") (f (quote (("pwhash_bench") ("mlock_tests") ("latest"))))))

