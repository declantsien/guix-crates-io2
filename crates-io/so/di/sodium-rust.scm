(define-module (crates-io so di sodium-rust) #:use-module (crates-io))

(define-public crate-sodium-rust-1.0.0 (c (n "sodium-rust") (v "1.0.0") (h "170sr1w7xk3byja9fln4flzl7s99pgcb3ars97abp26554828387")))

(define-public crate-sodium-rust-1.0.1 (c (n "sodium-rust") (v "1.0.1") (h "1p7lx85y44hnm1pl7a726y21qhszilrzismnq7d5kjd8sjh0xd33")))

(define-public crate-sodium-rust-2.0.0 (c (n "sodium-rust") (v "2.0.0") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 2)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "06pbr43gqs22cjcjnfvqxhq16q40i29cj9i3fd9mvd70jb1d9plk")))

(define-public crate-sodium-rust-2.0.1 (c (n "sodium-rust") (v "2.0.1") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 2)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "17rki28ansmixg0p6b6ns7jf7r0w65b70wi2ncqagdp5w9kymgb3")))

(define-public crate-sodium-rust-2.0.2 (c (n "sodium-rust") (v "2.0.2") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 2)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "0j2rgxjlqy60lahvclljwqwp9x08lsgw4xmwrs7816jgh7an3n3j")))

(define-public crate-sodium-rust-2.1.0 (c (n "sodium-rust") (v "2.1.0") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 2)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "1091jh8g500hck2f2k9b6vpwa7534zvmsvxq7i0fyhdq25b5cg1g")))

(define-public crate-sodium-rust-2.1.1 (c (n "sodium-rust") (v "2.1.1") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 2)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "10plx989vr871kby2vr6fzzznhz1yn3vvgwx5iqw6jsaiw58y1hi")))

(define-public crate-sodium-rust-2.1.2 (c (n "sodium-rust") (v "2.1.2") (d (list (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 2)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "1wjd7s9yvmb3clyfcs1ldpc9dsqh1h0cgk2d08lrzvifdhbal63r")))

