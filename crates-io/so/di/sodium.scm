(define-module (crates-io so di sodium) #:use-module (crates-io))

(define-public crate-sodium-0.1.0 (c (n "sodium") (v "0.1.0") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 2)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)))) (h "0bpzmq64adykcziq405dh9v38q1mbh1y4g8fxd8b80mljiggpj57")))

(define-public crate-sodium-0.1.1 (c (n "sodium") (v "0.1.1") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 2)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)))) (h "08ak7nfh91a935liwsir7w44jqd1md6gq18ybd6nvm9m66430zwq")))

(define-public crate-sodium-0.1.2 (c (n "sodium") (v "0.1.2") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 2)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)))) (h "1lpiwpwfb2iy3v97fcf8hbr0579prpjr6v54dks5cx5dqiq22wg4")))

(define-public crate-sodium-0.1.3 (c (n "sodium") (v "0.1.3") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 2)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)))) (h "1my06s9xcvds4f3h8n2lni2w252y07f8v316q70x4l3an80xdd31")))

