(define-module (crates-io so rr sorry-im-off-today) #:use-module (crates-io))

(define-public crate-sorry-im-off-today-1.0.0 (c (n "sorry-im-off-today") (v "1.0.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "ical") (r "0.5.*") (f (quote ("ical"))) (k 0)) (d (n "regex") (r "^1.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "slack_api") (r "^0.21") (d #t) (k 0)))) (h "0mcbc4d9yq2mpwrmlr1cllq889fjpp72xsy0x3h8zcjqrbd2fvr1")))

(define-public crate-sorry-im-off-today-1.0.1 (c (n "sorry-im-off-today") (v "1.0.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "ical") (r "0.5.*") (f (quote ("ical"))) (k 0)) (d (n "regex") (r "^1.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "slack_api") (r "^0.21") (d #t) (k 0)))) (h "0r08k6djy5g8bb1w0p5asqkfdyz5p8pb24nlyvzxrwnxgrixrbdz")))

(define-public crate-sorry-im-off-today-1.0.2 (c (n "sorry-im-off-today") (v "1.0.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "ical") (r "0.6.*") (f (quote ("ical"))) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "slack_api") (r "^0.22") (d #t) (k 0)))) (h "1nwilfxvlcxnqwzgkz7s8b8chyd3hnbnlq8vlhaa8ng2q4s6zq4l")))

(define-public crate-sorry-im-off-today-1.0.3 (c (n "sorry-im-off-today") (v "1.0.3") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "ical") (r "^0.7.0") (f (quote ("ical"))) (k 0)) (d (n "regex") (r "^1.5.6") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.10") (f (quote ("rustls-tls" "trust-dns" "blocking"))) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "slack_api") (r "^0.23.1") (f (quote ("reqwest_blocking" "with_rustls"))) (k 0)))) (h "00fsc247yvhdyswxky750svx4c75zi40k5gdqjx38srpf8mcggg8")))

