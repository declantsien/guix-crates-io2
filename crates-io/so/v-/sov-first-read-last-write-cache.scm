(define-module (crates-io so v- sov-first-read-last-write-cache) #:use-module (crates-io))

(define-public crate-sov-first-read-last-write-cache-0.1.0 (c (n "sov-first-read-last-write-cache") (v "0.1.0") (d (list (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "02cpmymr8ccsjd4mnmr5kr6xj0piiip3xijkck1qcxmhfsdg4jzb") (r "1.66")))

(define-public crate-sov-first-read-last-write-cache-0.2.0 (c (n "sov-first-read-last-write-cache") (v "0.2.0") (d (list (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "13bf77hhjcrc8k5shpmlxc46rwqix6l5bwkbn33j0zz7d3vpmgnc") (r "1.66")))

(define-public crate-sov-first-read-last-write-cache-0.3.0 (c (n "sov-first-read-last-write-cache") (v "0.3.0") (d (list (d (n "proptest") (r "^1.3.1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "1mikffk7rqqm2bbvlp7nk6ar24zrz403vymq6rcdvkd4mp7911vh")))

