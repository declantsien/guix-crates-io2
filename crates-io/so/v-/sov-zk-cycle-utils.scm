(define-module (crates-io so v- sov-zk-cycle-utils) #:use-module (crates-io))

(define-public crate-sov-zk-cycle-utils-0.2.0 (c (n "sov-zk-cycle-utils") (v "0.2.0") (d (list (d (n "risc0-zkvm") (r "^0.16") (f (quote ("std"))) (k 0)) (d (n "risc0-zkvm-platform") (r "^0.16") (d #t) (k 0)))) (h "0vx693j95w1z83fqgxdgvyib5sp1sbiyz7s1ww0ypxgl7cbh4m99") (r "1.66")))

(define-public crate-sov-zk-cycle-utils-0.3.0 (c (n "sov-zk-cycle-utils") (v "0.3.0") (d (list (d (n "risc0-zkvm") (r "^0.18") (f (quote ("std"))) (k 0)) (d (n "risc0-zkvm-platform") (r "^0.18") (d #t) (k 0)))) (h "1d9s0awaca7v52wh3qx6b91xkkp4k0n7157if96lm1978n29zfg9") (f (quote (("native" "risc0-zkvm/prove") ("default"))))))

