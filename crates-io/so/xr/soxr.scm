(define-module (crates-io so xr soxr) #:use-module (crates-io))

(define-public crate-soxr-0.6.0 (c (n "soxr") (v "0.6.0") (d (list (d (n "bitflags") (r "^2") (d #t) (k 0)) (d (n "bytemuck") (r "^1.14") (f (quote ("derive" "must_cast" "min_const_generics"))) (d #t) (k 0)) (d (n "libsoxr-sys") (r "^0.1") (d #t) (k 0)))) (h "0yf9vanx25macrnfblwxkd4bxay4i7zmawi1qf4svnh2av36pjnk")))

