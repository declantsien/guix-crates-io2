(define-module (crates-io so ci social_tournament) #:use-module (crates-io))

(define-public crate-social_tournament-0.1.0 (c (n "social_tournament") (v "0.1.0") (d (list (d (n "round_robin_tournament") (r "^0.3.0") (d #t) (k 0)))) (h "0hbw6l6gl71457ic50sy2370q6kra0wwscihcfgbzafhsjj2b9nm")))

(define-public crate-social_tournament-0.2.0 (c (n "social_tournament") (v "0.2.0") (d (list (d (n "round_robin_tournament") (r "^0.3.0") (d #t) (k 0)))) (h "0lamwv8ajxicqnsa9zz6pmgzd36vi8x92jrcnaidqg1hks1j7wd3")))

(define-public crate-social_tournament-0.3.0 (c (n "social_tournament") (v "0.3.0") (d (list (d (n "round_robin_tournament") (r "^0.3.0") (d #t) (k 0)))) (h "09sh0fjaw4h71fcd4n6bjlx7vr4hd4gb1ddr7g4s59phva7ll35h")))

(define-public crate-social_tournament-0.3.1 (c (n "social_tournament") (v "0.3.1") (d (list (d (n "round_robin_tournament") (r "^0.3.0") (d #t) (k 0)))) (h "0j186xqa85gxsvdnirpmpgba7g9fj8g92rmd7j32cc14rm4c73j3")))

(define-public crate-social_tournament-0.3.2 (c (n "social_tournament") (v "0.3.2") (d (list (d (n "round_robin_tournament") (r "^0.3.0") (d #t) (k 0)))) (h "1hhxlbf3l8hsgjvhk5v43q45py5kjz1776yrsrnj00y81n2r5jg8")))

(define-public crate-social_tournament-0.4.0 (c (n "social_tournament") (v "0.4.0") (d (list (d (n "round_robin_tournament") (r "^0.3.0") (d #t) (k 0)))) (h "1zyclb290hlqi7mwjaldw11y3xgcch8zsjsm8kimgi5bx45zq4x3")))

(define-public crate-social_tournament-0.4.1 (c (n "social_tournament") (v "0.4.1") (d (list (d (n "round_robin_tournament") (r "^0.3.0") (d #t) (k 0)))) (h "0m9bxcapxrp9fi4w3mq3p2bm5gdnb1y2i1dk9d2g2klng40mpylf")))

(define-public crate-social_tournament-0.4.2 (c (n "social_tournament") (v "0.4.2") (d (list (d (n "round_robin_tournament") (r "^0.3.0") (d #t) (k 0)))) (h "17g4p9q56x851dihn2dzj2fa3lls2h9x0fhca7d1daca94cw3aai")))

(define-public crate-social_tournament-0.5.0 (c (n "social_tournament") (v "0.5.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "printpdf") (r "^0.4.1") (d #t) (k 0)) (d (n "round_robin_tournament") (r "^0.3.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.29") (d #t) (k 0)))) (h "0z0lwvxjgk9rf4q2k0d24bj55xj7c7nyys2vcfycm963k2c6bpn3")))

(define-public crate-social_tournament-0.5.1 (c (n "social_tournament") (v "0.5.1") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "printpdf") (r "^0.4.1") (d #t) (k 0)) (d (n "round_robin_tournament") (r "^0.3.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.29") (d #t) (k 0)))) (h "1fp4066jzpm6jdlpxpvyrabpygkmvmvpqcpw1dr92fsfk53q847s")))

(define-public crate-social_tournament-0.5.2 (c (n "social_tournament") (v "0.5.2") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "printpdf") (r "^0.4.1") (d #t) (k 0)) (d (n "round_robin_tournament") (r "^0.3.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.29") (d #t) (k 0)))) (h "0ykg8dmirmkrhjjj89wy3r23fkn54252gdi1gfc0icxgg04aq1dp")))

