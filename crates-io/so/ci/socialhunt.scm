(define-module (crates-io so ci socialhunt) #:use-module (crates-io))

(define-public crate-socialhunt-0.1.2 (c (n "socialhunt") (v "0.1.2") (d (list (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1v408nczhkaddsrjvamxqakmrdb5g4cqpd8sdaji0ssiv4z4fhn9")))

