(define-module (crates-io so ku soku) #:use-module (crates-io))

(define-public crate-soku-0.1.0 (c (n "soku") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "1pjan1fnkllq5wlka1lyw20i8dxjznh4bq7672z50z9dgb7a62nn")))

(define-public crate-soku-0.1.1 (c (n "soku") (v "0.1.1") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "futures") (r "^0.3.27") (d #t) (k 2)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "056piwxkw9j5i9wwh6dvy8jyii3fwyiznb7xw686i098rz58ds5m")))

