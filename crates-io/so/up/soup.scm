(define-module (crates-io so up soup) #:use-module (crates-io))

(define-public crate-soup-0.1.0 (c (n "soup") (v "0.1.0") (d (list (d (n "failure") (r "^0.1.3") (d #t) (k 0)) (d (n "html5ever") (r "^0.22.3") (d #t) (k 0)))) (h "03sbyyj7p68g223w7k6ah900w7b0fkvgvqi8342qdzj215qyrbvv")))

(define-public crate-soup-0.1.1 (c (n "soup") (v "0.1.1") (d (list (d (n "failure") (r "^0.1.3") (d #t) (k 0)) (d (n "html5ever") (r "^0.22.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.4") (d #t) (k 2)))) (h "1yhfr324i2cmxiryram8yq8m0mbc4zdjpm410zg3bp8l4jq49mdg")))

(define-public crate-soup-0.2.0 (c (n "soup") (v "0.2.0") (d (list (d (n "failure") (r "^0.1.3") (d #t) (k 0)) (d (n "html5ever") (r "^0.22.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.4") (d #t) (k 2)))) (h "0kpm9s8z72r79r6sgrfchkx9ahj4zawi2hsjs1aisnjhk7djs8gh")))

(define-public crate-soup-0.2.1 (c (n "soup") (v "0.2.1") (d (list (d (n "failure") (r "^0.1.3") (d #t) (k 0)) (d (n "html5ever") (r "^0.22.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.4") (d #t) (k 2)))) (h "0pahpigrqi2ly95n9p8k4m9yd2gpszgbb9nhx65rg9l8j01zs3p1")))

(define-public crate-soup-0.3.0 (c (n "soup") (v "0.3.0") (d (list (d (n "html5ever") (r "^0.22.3") (d #t) (k 0)) (d (n "regex") (r "^1.0.5") (o #t) (d #t) (k 0)) (d (n "reqwest") (r "^0.9.4") (d #t) (k 2)))) (h "1904xqiahpaivgxv9v2jma5qanh6qiic0xn8mg3bsnch6v9727m0") (f (quote (("default" "regex"))))))

(define-public crate-soup-0.4.1 (c (n "soup") (v "0.4.1") (d (list (d (n "html5ever") (r "= 0.22.3") (d #t) (k 0)) (d (n "regex") (r "^1.0.5") (o #t) (d #t) (k 0)) (d (n "reqwest") (r "^0.9.4") (d #t) (k 2)))) (h "0cb00nqymi9xybliaf1gnanmzyia1xhq9vcqjl05fjk5g036psqn") (f (quote (("default" "regex"))))))

(define-public crate-soup-0.5.0 (c (n "soup") (v "0.5.0") (d (list (d (n "html5ever") (r "^0.22") (d #t) (k 0)) (d (n "regex") (r "^1.0.5") (o #t) (d #t) (k 0)) (d (n "reqwest") (r "^0.9.4") (d #t) (k 2)))) (h "17qkdxrqfx9304gipix2nwxyn8wv4g6sz3g1zz45brpd2z0vhhpf") (f (quote (("default" "regex"))))))

(define-public crate-soup-0.5.1 (c (n "soup") (v "0.5.1") (d (list (d (n "html5ever") (r "^0.22") (d #t) (k 0)) (d (n "regex") (r "^1.0.5") (o #t) (d #t) (k 0)) (d (n "reqwest") (r "^0.9.4") (d #t) (k 2)))) (h "057ykkp2zv6b30ck9wskky3hij8g6qsgbsh4mlbpljrza8hmsnma") (f (quote (("default" "regex"))))))

