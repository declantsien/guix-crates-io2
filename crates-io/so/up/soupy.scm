(define-module (crates-io so up soupy) #:use-module (crates-io))

(define-public crate-soupy-0.1.0 (c (n "soupy") (v "0.1.0") (d (list (d (n "nom") (r "^7.1") (d #t) (k 0)) (d (n "regex") (r "^1.9") (o #t) (d #t) (k 0)))) (h "0kyxcj2yk5h2m1672fksld9wxbgr5d19fr590gci7hsa1jbnj7pw") (f (quote (("default" "regex")))) (s 2) (e (quote (("regex" "dep:regex"))))))

(define-public crate-soupy-0.1.1 (c (n "soupy") (v "0.1.1") (d (list (d (n "nom") (r "^7.1") (d #t) (k 0)) (d (n "regex") (r "^1.9") (o #t) (d #t) (k 0)))) (h "0msqn48nsnxwpyik6imfh2p3cx40yfljr9p8i670gw4n5yd48hyn") (f (quote (("default" "regex")))) (s 2) (e (quote (("regex" "dep:regex"))))))

