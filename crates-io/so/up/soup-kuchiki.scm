(define-module (crates-io so up soup-kuchiki) #:use-module (crates-io))

(define-public crate-soup-kuchiki-0.5.0 (c (n "soup-kuchiki") (v "0.5.0") (d (list (d (n "html5ever") (r "^0.25") (d #t) (k 0)) (d (n "kuchiki") (r "^0.8.0") (d #t) (k 0)) (d (n "regex") (r "^1.0.5") (o #t) (d #t) (k 0)) (d (n "reqwest") (r "^0.9.4") (d #t) (k 2)))) (h "0ihxsjkkd85s1crgrqrd39lvdq1qnlq9aabmf90wr94cyqpawlyf") (f (quote (("default" "regex"))))))

