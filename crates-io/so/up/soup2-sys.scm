(define-module (crates-io so up soup2-sys) #:use-module (crates-io))

(define-public crate-soup2-sys-0.1.0 (c (n "soup2-sys") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "gio-sys") (r "^0.14") (d #t) (k 0)) (d (n "glib-sys") (r "^0.14") (d #t) (k 0)) (d (n "gobject-sys") (r "^0.14") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "shell-words") (r "^1.0.0") (d #t) (k 2)) (d (n "system-deps") (r "^5") (d #t) (k 1)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "02n17q5279rif27xlanxbqw1v3ly26xl5xz5cdql39x9xmsnc1cz") (f (quote (("v2_72" "v2_70") ("v2_70" "v2_68") ("v2_68" "v2_66") ("v2_66" "v2_62") ("v2_62" "v2_60") ("v2_60" "v2_58") ("v2_58" "v2_56") ("v2_56" "v2_54") ("v2_54" "v2_52") ("v2_52" "v2_50") ("v2_50" "v2_48") ("v2_48" "v2_46") ("v2_46" "v2_44") ("v2_44" "v2_42") ("v2_42" "v2_40") ("v2_40" "v2_38") ("v2_38" "v2_36") ("v2_36" "v2_34") ("v2_34" "v2_32") ("v2_32" "v2_30") ("v2_30" "v2_28") ("v2_28" "v2_26_3") ("v2_26_3" "v2_26") ("v2_26" "v2_24") ("v2_24") ("dox")))) (l "soup")))

(define-public crate-soup2-sys-0.2.0 (c (n "soup2-sys") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "gio-sys") (r "^0.15") (d #t) (k 0)) (d (n "glib-sys") (r "^0.15") (d #t) (k 0)) (d (n "gobject-sys") (r "^0.15") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "shell-words") (r "^1.0.0") (d #t) (k 2)) (d (n "system-deps") (r "^5") (d #t) (k 1)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1ky9dmgir8v0mk0f9i5lajvcnq3czaklf5l70bw1gjrz20kz97h0") (f (quote (("v2_72" "v2_70") ("v2_70" "v2_68") ("v2_68" "v2_66") ("v2_66" "v2_62") ("v2_62" "v2_60") ("v2_60" "v2_58") ("v2_58" "v2_56") ("v2_56" "v2_54") ("v2_54" "v2_52") ("v2_52" "v2_50") ("v2_50" "v2_48") ("v2_48" "v2_46") ("v2_46" "v2_44") ("v2_44" "v2_42") ("v2_42" "v2_40") ("v2_40" "v2_38") ("v2_38" "v2_36") ("v2_36" "v2_34") ("v2_34" "v2_32") ("v2_32" "v2_30") ("v2_30" "v2_28") ("v2_28" "v2_26_3") ("v2_26_3" "v2_26") ("v2_26" "v2_24") ("v2_24") ("dox")))) (l "soup")))

