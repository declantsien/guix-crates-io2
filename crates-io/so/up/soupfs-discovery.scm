(define-module (crates-io so up soupfs-discovery) #:use-module (crates-io))

(define-public crate-soupfs-discovery-0.0.0 (c (n "soupfs-discovery") (v "0.0.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rouille") (r "^3.6.1") (d #t) (k 0)))) (h "0qhbiqy43hdwrvvzkmy77ab3i0pr2jnl2hqdxnnb2hwdanz5nvcv") (r "1.60.0")))

(define-public crate-soupfs-discovery-0.0.1 (c (n "soupfs-discovery") (v "0.0.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "multihash") (r "^0.18.0") (f (quote ("serde-codec"))) (d #t) (k 0)) (d (n "rouille") (r "^3.6.1") (d #t) (k 0)))) (h "1wcqnaywyifhaq44qmkrc3l27xfawniypqzhb4nmzvfv8751bbjq") (r "1.60.0")))

