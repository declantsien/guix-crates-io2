(define-module (crates-io so up souper-ir) #:use-module (crates-io))

(define-public crate-souper-ir-0.1.0 (c (n "souper-ir") (v "0.1.0") (h "0z612dksvbaqajb8y1r44g8l70a1iqxgn5ivya01dmv15bdfd25j") (f (quote (("stringify") ("parse"))))))

(define-public crate-souper-ir-1.0.0 (c (n "souper-ir") (v "1.0.0") (d (list (d (n "id-arena") (r "^2.2.1") (d #t) (k 0)))) (h "0gx889c99mbgw3xvy29yi0lfk5wsd1vb99h6vz6blv56v2yw4g0n") (f (quote (("stringify") ("parse"))))))

(define-public crate-souper-ir-2.0.0 (c (n "souper-ir") (v "2.0.0") (d (list (d (n "id-arena") (r "^2.2.1") (d #t) (k 0)))) (h "0n0mdckdx4gz6bfkbp1d65nkbgfs2820x40s01alfnl4fvaawzhw") (f (quote (("stringify") ("parse"))))))

(define-public crate-souper-ir-2.1.0 (c (n "souper-ir") (v "2.1.0") (d (list (d (n "id-arena") (r "^2.2.1") (d #t) (k 0)))) (h "0i60q84w5k3rd0j3zhsdc5xasrd4wrkamyrs01rik3lq6g71h355") (f (quote (("stringify") ("parse"))))))

