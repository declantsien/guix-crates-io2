(define-module (crates-io so lw solwizard) #:use-module (crates-io))

(define-public crate-solwizard-0.1.0 (c (n "solwizard") (v "0.1.0") (d (list (d (n "assert_cmd") (r "^2.0.8") (d #t) (k 2)) (d (n "clap") (r "^4.0.15") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fancy") (r "^0.3.1") (d #t) (k 0)) (d (n "handlebars") (r "^4.3.6") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)))) (h "14jk6s3vkn0jlxd88ds0ql014mp1yjdzp0rcc65zidgfqv4liy9x")))

