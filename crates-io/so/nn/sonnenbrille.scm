(define-module (crates-io so nn sonnenbrille) #:use-module (crates-io))

(define-public crate-sonnenbrille-0.1.0 (c (n "sonnenbrille") (v "0.1.0") (d (list (d (n "rand") (r "0.*") (d #t) (k 2)))) (h "1ps1zxna44ydsd8hb3rxxr8bzzcaq5y8qimk9i6gimz85khrvsk9") (y #t)))

(define-public crate-sonnenbrille-0.1.1 (c (n "sonnenbrille") (v "0.1.1") (d (list (d (n "rand") (r "0.*") (d #t) (k 2)))) (h "0m4zx9mj5175idimh1f4ri9752s9p74km7kaka0b3ir8qbxnh772")))

(define-public crate-sonnenbrille-0.1.2 (c (n "sonnenbrille") (v "0.1.2") (d (list (d (n "rand") (r "0.*") (d #t) (k 2)))) (h "0dnhzbcd97aasx0argj48dy8gxnjhj02vhjcx72ljn4vz0b0ny54")))

