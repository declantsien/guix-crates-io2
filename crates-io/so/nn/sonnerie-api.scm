(define-module (crates-io so nn sonnerie-api) #:use-module (crates-io))

(define-public crate-sonnerie-api-0.1.0 (c (n "sonnerie-api") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "escape_string") (r "^0.1.0") (d #t) (k 0)))) (h "1hy5569ka6si6p3k0fld444iihnxwbq3hphqwjnzn7yfzyazgx34")))

(define-public crate-sonnerie-api-0.1.1 (c (n "sonnerie-api") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "escape_string") (r "^0.1.0") (d #t) (k 0)))) (h "0zikndpcslja03m4493rqcvghj5gv062wxm0vi4djp8lv8g5f737")))

(define-public crate-sonnerie-api-0.2.0 (c (n "sonnerie-api") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "escape_string") (r "^0.1.0") (d #t) (k 0)))) (h "0c0nxyb47r5ka6d098aknjwpl8i7vpwcj48m9x9vpbigpclwk8az")))

(define-public crate-sonnerie-api-0.2.1 (c (n "sonnerie-api") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "escape_string") (r "^0.1.0") (d #t) (k 0)))) (h "1srnjl1wdllwc3c49glcd9wynn8k04p7a4j78vk5xqmgv7rcl2vc")))

(define-public crate-sonnerie-api-0.2.2 (c (n "sonnerie-api") (v "0.2.2") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "escape_string") (r "^0.1.0") (d #t) (k 0)))) (h "0jfkgm14qk529x3lq93m0hzsk24khlssxzqs67k4wcs3sqgvvdcb")))

(define-public crate-sonnerie-api-0.2.3 (c (n "sonnerie-api") (v "0.2.3") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "escape_string") (r "^0.1.0") (d #t) (k 0)))) (h "0dg7421i0a3fkmxglyvkm8jyclazflf9f03bgilfzsdr3lsk8hjq")))

(define-public crate-sonnerie-api-0.3.0 (c (n "sonnerie-api") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "escape_string") (r "^0.1.0") (d #t) (k 0)))) (h "1p9d9zin7gfpj3hj96xryygnq7m7ca1hdq8pwswa627bix0whgg7")))

(define-public crate-sonnerie-api-0.4.0 (c (n "sonnerie-api") (v "0.4.0") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "escape_string") (r "^0.1.0") (d #t) (k 0)) (d (n "linestream") (r "^0.1.0") (d #t) (k 0)))) (h "1bz4cl2cnd71r7285nri3wqrz9aj8vvzzlans1ysbzwzm12kaclf")))

(define-public crate-sonnerie-api-0.4.1 (c (n "sonnerie-api") (v "0.4.1") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "escape_string") (r "^0.1.0") (d #t) (k 0)) (d (n "linestream") (r "^0.1.0") (d #t) (k 0)))) (h "1n9487dr08hfacqfiw63dky5k6dvpv6i4kf67w8m2jl38dxnns3h")))

