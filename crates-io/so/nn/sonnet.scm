(define-module (crates-io so nn sonnet) #:use-module (crates-io))

(define-public crate-sonnet-0.1.0 (c (n "sonnet") (v "0.1.0") (h "0k76dwxa01r4sf9hhnnjjlirg1whvbjfmq60aas1rnhp0bi4drl7")))

(define-public crate-sonnet-0.1.1 (c (n "sonnet") (v "0.1.1") (h "0xcwaab0myfrm4qd0znvb6m0hjwmfnay1ri2h10f4qmi0q33d0z7")))

