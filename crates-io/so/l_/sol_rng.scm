(define-module (crates-io so l_ sol_rng) #:use-module (crates-io))

(define-public crate-sol_rng-0.1.0 (c (n "sol_rng") (v "0.1.0") (d (list (d (n "anchor-lang") (r "^0.18.2") (d #t) (k 0)) (d (n "anchor-spl") (r "^0.18.2") (d #t) (k 0)) (d (n "spl-token") (r "^3.1.1") (f (quote ("no-entrypoint"))) (d #t) (k 0)))) (h "1lmqmycymvg48yrh1660h3kihyilkm9xlbxlgjmjihq9f2qidlhl") (f (quote (("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-sol_rng-0.1.1 (c (n "sol_rng") (v "0.1.1") (d (list (d (n "anchor-lang") (r "^0.18.2") (d #t) (k 0)) (d (n "anchor-spl") (r "^0.18.2") (d #t) (k 0)) (d (n "spl-token") (r "^3.1.1") (f (quote ("no-entrypoint"))) (d #t) (k 0)))) (h "08nvl28y12hr630pir17axy72s3x4bls55lwr3n2r3z70c5f1y2a") (f (quote (("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

