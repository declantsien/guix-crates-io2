(define-module (crates-io so l_ sol_prng) #:use-module (crates-io))

(define-public crate-sol_prng-1.0.0 (c (n "sol_prng") (v "1.0.0") (h "035w5zgzfkvbhmw3ir6xs8d9xdavr4n3yz08hn16595wmxhp17f9") (y #t) (r "1.62.0")))

(define-public crate-sol_prng-1.1.0 (c (n "sol_prng") (v "1.1.0") (h "06pipnfc4628wfaik5ms2hf5j9yks5n40lszywbcm4fms7cp04h9") (r "1.62.0")))

(define-public crate-sol_prng-1.2.0 (c (n "sol_prng") (v "1.2.0") (h "0c8jw3rs9jy4mf2bs38zm925h9x3psqqsdbqii0cp67lbzin323i") (r "1.62.0")))

