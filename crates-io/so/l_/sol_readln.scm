(define-module (crates-io so l_ sol_readln) #:use-module (crates-io))

(define-public crate-sol_readln-1.0.0 (c (n "sol_readln") (v "1.0.0") (h "0wwdrjlg9ffqpbn7knm76k8hk7mhv90ri59q57alpksjw2pshj33") (y #t) (r "1.62.0")))

(define-public crate-sol_readln-1.0.1 (c (n "sol_readln") (v "1.0.1") (h "0av73f3a9fmsirkzxw53hndmg2gqsz76g7q4a8p6gfqsjlf1i8s1") (r "1.62.0")))

