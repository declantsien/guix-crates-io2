(define-module (crates-io so ld solders-transaction-confirmation-status) #:use-module (crates-io))

(define-public crate-solders-transaction-confirmation-status-0.18.0 (c (n "solders-transaction-confirmation-status") (v "0.18.0") (d (list (d (n "pyo3") (r "^0.18.0") (f (quote ("macros"))) (k 0)) (d (n "serde") (r "^1.0.136") (d #t) (k 0)) (d (n "solana-transaction-status") (r "^1.16.0") (d #t) (k 0)) (d (n "solders-macros") (r "=0.18.0") (d #t) (k 0)))) (h "1034ixcy0a2wbzjg3rj75ibvrrfihpmhsdkm1xgdwvsxvdcvikxl")))

(define-public crate-solders-transaction-confirmation-status-0.18.1 (c (n "solders-transaction-confirmation-status") (v "0.18.1") (d (list (d (n "pyo3") (r "^0.18.0") (f (quote ("macros"))) (k 0)) (d (n "serde") (r "^1.0.136") (d #t) (k 0)) (d (n "solana-transaction-status") (r "^1.16.0") (d #t) (k 0)) (d (n "solders-macros") (r "=0.18.1") (d #t) (k 0)))) (h "0srp4094hhh5xjyqajlmp2f1p1g90kzs1li8bc7lbp89a6aav4gh")))

(define-public crate-solders-transaction-confirmation-status-0.19.0 (c (n "solders-transaction-confirmation-status") (v "0.19.0") (d (list (d (n "pyo3") (r "^0.19.2") (f (quote ("macros"))) (k 0)) (d (n "serde") (r "^1.0.188") (d #t) (k 0)) (d (n "solana-transaction-status") (r "^1.17.12") (d #t) (k 0)) (d (n "solders-macros") (r "=0.19.0") (d #t) (k 0)))) (h "0yivfsz2ng596scjm5p08sckmaknvrf3jav9p1qmky84c2zdicnv")))

(define-public crate-solders-transaction-confirmation-status-0.21.0 (c (n "solders-transaction-confirmation-status") (v "0.21.0") (d (list (d (n "pyo3") (r "^0.20.2") (f (quote ("macros"))) (k 0)) (d (n "serde") (r "^1.0.188") (d #t) (k 0)) (d (n "solana-transaction-status") (r "^1.18.1") (d #t) (k 0)) (d (n "solders-macros") (r "=0.21.0") (d #t) (k 0)))) (h "0i84xck70f524pp754lmxjbh298wk09ribcn98bm5v9i9s4s6s0d")))

