(define-module (crates-io so ld solders-macros) #:use-module (crates-io))

(define-public crate-solders-macros-0.1.0 (c (n "solders-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.39") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.96") (f (quote ("full"))) (d #t) (k 0)))) (h "1apygwx4ki9ck6r5cs33ifcymrrbcif207qfyjnsybr65pyaprlq")))

(define-public crate-solders-macros-0.2.0 (c (n "solders-macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.39") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.96") (f (quote ("full"))) (d #t) (k 0)))) (h "0r77z795hfwyy7a45hkgcvkdmd5qhwvfx7hvanjhcmqrq0l0w38f")))

(define-public crate-solders-macros-0.3.0 (c (n "solders-macros") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0.39") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.96") (f (quote ("full"))) (d #t) (k 0)))) (h "1hag6mx0mf6lg20317sdss1nr0h9rg256sjvv9zaz3hvxb9aw74k")))

(define-public crate-solders-macros-0.4.0 (c (n "solders-macros") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0.39") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.96") (f (quote ("full"))) (d #t) (k 0)))) (h "0fyql13z2qpqaa2ksbmlm6wg39ivd3k3brx6aradzx8wkpsw2zk6")))

(define-public crate-solders-macros-0.4.1 (c (n "solders-macros") (v "0.4.1") (d (list (d (n "proc-macro2") (r "^1.0.39") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.96") (f (quote ("full"))) (d #t) (k 0)))) (h "0hkgiqlypq2m5gfazd9cgzq41x4glnxmiiw717gz6r01d16j2k2p")))

(define-public crate-solders-macros-0.4.2 (c (n "solders-macros") (v "0.4.2") (d (list (d (n "proc-macro2") (r "^1.0.39") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.96") (f (quote ("full"))) (d #t) (k 0)))) (h "1s8v4c8prcb7l91y8y66d9fh5xvw1n1m8jrc0m6i2fzs5kcc9g6g")))

(define-public crate-solders-macros-0.4.3 (c (n "solders-macros") (v "0.4.3") (d (list (d (n "proc-macro2") (r "^1.0.39") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.96") (f (quote ("full"))) (d #t) (k 0)))) (h "010q0gi8bk194zx8k2kpv977j306qjw7jyxzsqsh1hj4nkvnhsqj")))

(define-public crate-solders-macros-0.5.0 (c (n "solders-macros") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0.39") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.96") (f (quote ("full"))) (d #t) (k 0)))) (h "1j0ksdx062amd7ryfhxpzpck6qzhx4biyvk6gd009d12xydn8jqd")))

(define-public crate-solders-macros-0.6.0 (c (n "solders-macros") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^1.0.39") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.96") (f (quote ("full"))) (d #t) (k 0)))) (h "1xlw0v4d68a6lbbbm4z4ywh3wnj3gawhmr7kxw3wdmvnsmzl0id8")))

(define-public crate-solders-macros-0.6.1 (c (n "solders-macros") (v "0.6.1") (d (list (d (n "proc-macro2") (r "^1.0.39") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.96") (f (quote ("full"))) (d #t) (k 0)))) (h "19g0xhg5w95bcdd942yrj7lhkmlp0d09pngi814625kybdfg4ypl")))

(define-public crate-solders-macros-0.7.0 (c (n "solders-macros") (v "0.7.0") (d (list (d (n "proc-macro2") (r "^1.0.39") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.96") (f (quote ("full"))) (d #t) (k 0)))) (h "18gr0lklg6vcdz6z8z6vwvd05xzjg63d5cxiim9insxrrhpm5ql4")))

(define-public crate-solders-macros-0.11.0 (c (n "solders-macros") (v "0.11.0") (d (list (d (n "proc-macro2") (r "^1.0.39") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.96") (f (quote ("full"))) (d #t) (k 0)))) (h "0mv14gz4z9amzffcly5vqphiyiff4vzlk4c1gbvjjm7ysdqdvx1h")))

(define-public crate-solders-macros-0.12.0 (c (n "solders-macros") (v "0.12.0") (d (list (d (n "proc-macro2") (r "^1.0.39") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.96") (f (quote ("full"))) (d #t) (k 0)))) (h "0ds6j7slh6f5dbvfxfbckkjnxnlwfjnijjm32nfgv0l5zgg2ixvr")))

(define-public crate-solders-macros-0.13.0 (c (n "solders-macros") (v "0.13.0") (d (list (d (n "proc-macro2") (r "^1.0.39") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.96") (f (quote ("full"))) (d #t) (k 0)))) (h "1yb4lbdxsxs8y7d6pxx7cgr74gb1b349yxhgwpigbl6ldj5bp8ni")))

(define-public crate-solders-macros-0.14.0 (c (n "solders-macros") (v "0.14.0") (d (list (d (n "proc-macro2") (r "^1.0.39") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.96") (f (quote ("full"))) (d #t) (k 0)))) (h "1rf4mdlcx854qm2c34vnp2613z6gicknkyvrwhlwjbd7an4kn7kp")))

(define-public crate-solders-macros-0.14.1 (c (n "solders-macros") (v "0.14.1") (d (list (d (n "proc-macro2") (r "^1.0.39") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.96") (f (quote ("full"))) (d #t) (k 0)))) (h "1rl4d14z11yq5d8bm0bx9sn311vxd0sipwr4171m3ncs675n3bym")))

(define-public crate-solders-macros-0.14.2 (c (n "solders-macros") (v "0.14.2") (d (list (d (n "proc-macro2") (r "^1.0.39") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.96") (f (quote ("full"))) (d #t) (k 0)))) (h "1p3acwpw8y1802f9dgjmg8ksl1cll5fgy5x7sdhjxnc24zzyb3j0")))

(define-public crate-solders-macros-0.14.3 (c (n "solders-macros") (v "0.14.3") (d (list (d (n "proc-macro2") (r "^1.0.39") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.96") (f (quote ("full"))) (d #t) (k 0)))) (h "00lr6q4a23x77vk2qxdcndclqzsywfcac5iwi1nn4rr70bicwikc")))

(define-public crate-solders-macros-0.14.4 (c (n "solders-macros") (v "0.14.4") (d (list (d (n "proc-macro2") (r "^1.0.39") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.96") (f (quote ("full"))) (d #t) (k 0)))) (h "11f5clfds55svv56awf8q0446wdnpiyc354ksh99zhlzc4kzkhgk")))

(define-public crate-solders-macros-0.15.0 (c (n "solders-macros") (v "0.15.0") (d (list (d (n "proc-macro2") (r "^1.0.39") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.96") (f (quote ("full"))) (d #t) (k 0)))) (h "1g7ym2mfdcgzcm8bc9fylfr47j83gdzdsrmv9a93ly3l5nrg8n7w")))

(define-public crate-solders-macros-0.15.1 (c (n "solders-macros") (v "0.15.1") (d (list (d (n "proc-macro2") (r "^1.0.39") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.96") (f (quote ("full"))) (d #t) (k 0)))) (h "1xn956945kimqn3fddhrrlsx20bzni8vw4sj2s9k99ahlyslln17")))

(define-public crate-solders-macros-0.16.0 (c (n "solders-macros") (v "0.16.0") (d (list (d (n "proc-macro2") (r "^1.0.39") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.96") (f (quote ("full"))) (d #t) (k 0)))) (h "0lw2gsnmjh34zaw6h8h2n37rcyf1x468f5nyi2gacnwvc2daz7ax")))

(define-public crate-solders-macros-0.17.0 (c (n "solders-macros") (v "0.17.0") (d (list (d (n "proc-macro2") (r "^1.0.39") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.96") (f (quote ("full"))) (d #t) (k 0)))) (h "0f5qigil2p07hmn24xyqs79xrarahl4q54lzkkg77w8w0n7d1bqx")))

(define-public crate-solders-macros-0.18.0 (c (n "solders-macros") (v "0.18.0") (d (list (d (n "proc-macro2") (r "^1.0.39") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.96") (f (quote ("full"))) (d #t) (k 0)))) (h "1diynipr7xi45qsd3584b9kj4n0wpb6cv0ysf5b8mk9middfrwnw")))

(define-public crate-solders-macros-0.18.1 (c (n "solders-macros") (v "0.18.1") (d (list (d (n "proc-macro2") (r "^1.0.39") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.96") (f (quote ("full"))) (d #t) (k 0)))) (h "1mh26g6as6nzq5ygd7cqqmn5i1xwrfkw90zqk4ljmf4p25hbs2aq")))

(define-public crate-solders-macros-0.19.0 (c (n "solders-macros") (v "0.19.0") (d (list (d (n "proc-macro2") (r "^1.0.39") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.96") (f (quote ("full"))) (d #t) (k 0)))) (h "0agsvahhjfn94w4ls0ddba9ikdz9bw1h37ma5wj49zy7vpyr3gwk")))

(define-public crate-solders-macros-0.20.0 (c (n "solders-macros") (v "0.20.0") (d (list (d (n "proc-macro2") (r "^1.0.39") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.96") (f (quote ("full"))) (d #t) (k 0)))) (h "0ii21y0k02v45zyfbc78jz1vbdhq0cq8a774qyyvdzycfd47j3iq")))

(define-public crate-solders-macros-0.21.0 (c (n "solders-macros") (v "0.21.0") (d (list (d (n "proc-macro2") (r "^1.0.39") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.96") (f (quote ("full"))) (d #t) (k 0)))) (h "1skgzqhwshg7x7fyvsnb5j1igavk775lvmyzr1fz3spd0zwc864g")))

