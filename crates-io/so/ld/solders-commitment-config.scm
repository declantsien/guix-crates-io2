(define-module (crates-io so ld solders-commitment-config) #:use-module (crates-io))

(define-public crate-solders-commitment-config-0.16.0 (c (n "solders-commitment-config") (v "0.16.0") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "pyo3") (r "^0.18.0") (f (quote ("macros"))) (k 0)) (d (n "serde") (r "^1.0.136") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.14.17") (d #t) (k 0)) (d (n "solders-traits") (r "=0.16.0") (d #t) (k 0)))) (h "1786gwwch86z78lbcy5awp4z5b7vwm04fz9rdxvmmw25psy0snzp")))

(define-public crate-solders-commitment-config-0.17.0 (c (n "solders-commitment-config") (v "0.17.0") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "pyo3") (r "^0.18.0") (f (quote ("macros"))) (k 0)) (d (n "serde") (r "^1.0.136") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.14.17") (d #t) (k 0)) (d (n "solders-traits") (r "=0.17.0") (d #t) (k 0)))) (h "0z5a6axq6y7c8y1jwn4ll03aak64sxby0ybckhj4khvrwpq5bgiq")))

(define-public crate-solders-commitment-config-0.18.0 (c (n "solders-commitment-config") (v "0.18.0") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "pyo3") (r "^0.18.0") (f (quote ("macros"))) (k 0)) (d (n "serde") (r "^1.0.136") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.16.0") (d #t) (k 0)) (d (n "solders-traits") (r "=0.18.0") (d #t) (k 0)))) (h "1x2bh34a0d779kv4fj4d5wshrcfxarjjkdc2i3911z8n7jqqmn8y")))

(define-public crate-solders-commitment-config-0.18.1 (c (n "solders-commitment-config") (v "0.18.1") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "pyo3") (r "^0.18.0") (f (quote ("macros"))) (k 0)) (d (n "serde") (r "^1.0.136") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.16.0") (d #t) (k 0)) (d (n "solders-traits") (r "=0.18.1") (d #t) (k 0)))) (h "1v6hh0i8k12xk183x2fwag85q61y9zj5wiy02qf7xwl46hj53b13")))

(define-public crate-solders-commitment-config-0.19.0 (c (n "solders-commitment-config") (v "0.19.0") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "pyo3") (r "^0.19.2") (f (quote ("macros"))) (k 0)) (d (n "serde") (r "^1.0.188") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.17.12") (d #t) (k 0)) (d (n "solders-traits") (r "=0.19.0") (d #t) (k 0)))) (h "0hnl05ljkrlms1w3nf4kh0jlkw81vzns93sapp6vsm37rmdqaka3")))

(define-public crate-solders-commitment-config-0.20.0 (c (n "solders-commitment-config") (v "0.20.0") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "pyo3") (r "^0.19.2") (f (quote ("macros"))) (k 0)) (d (n "serde") (r "^1.0.188") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.18.1") (d #t) (k 0)) (d (n "solders-traits") (r "=0.20.0") (d #t) (k 0)))) (h "0x9fkl5db9whfrwlnm9rjmwcygh7mdl6x1dak6jl3r0xxk1imzjz")))

(define-public crate-solders-commitment-config-0.21.0 (c (n "solders-commitment-config") (v "0.21.0") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "pyo3") (r "^0.20.2") (f (quote ("macros"))) (k 0)) (d (n "serde") (r "^1.0.188") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.18.1") (d #t) (k 0)) (d (n "solders-traits") (r "=0.21.0") (d #t) (k 0)))) (h "1313rbjfzjczylvdyrsd1lpx2c4f132z5003scryyv0mim4ikj18")))

