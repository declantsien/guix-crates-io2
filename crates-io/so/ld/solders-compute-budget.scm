(define-module (crates-io so ld solders-compute-budget) #:use-module (crates-io))

(define-public crate-solders-compute-budget-0.17.0 (c (n "solders-compute-budget") (v "0.17.0") (d (list (d (n "pyo3") (r "^0.18.0") (f (quote ("macros"))) (k 0)) (d (n "solana-sdk") (r "^1.14.17") (d #t) (k 0)) (d (n "solders-instruction") (r "=0.17.0") (d #t) (k 0)) (d (n "solders-pubkey") (r "=0.17.0") (d #t) (k 0)))) (h "17s7znpgyqnzkxla1p7myaxa3597smg414i53vi6ysd1dk3gzn4c")))

(define-public crate-solders-compute-budget-0.18.0 (c (n "solders-compute-budget") (v "0.18.0") (d (list (d (n "pyo3") (r "^0.18.0") (f (quote ("macros"))) (k 0)) (d (n "solana-sdk") (r "^1.16.0") (d #t) (k 0)) (d (n "solders-instruction") (r "=0.18.0") (d #t) (k 0)) (d (n "solders-pubkey") (r "=0.18.0") (d #t) (k 0)))) (h "14w5f9xxdgyl3am2v14pbip8lcbcxr85q97cn5p8n0xdgykgx89l")))

(define-public crate-solders-compute-budget-0.18.1 (c (n "solders-compute-budget") (v "0.18.1") (d (list (d (n "pyo3") (r "^0.18.0") (f (quote ("macros"))) (k 0)) (d (n "solana-sdk") (r "^1.16.0") (d #t) (k 0)) (d (n "solders-instruction") (r "=0.18.1") (d #t) (k 0)) (d (n "solders-pubkey") (r "=0.18.1") (d #t) (k 0)))) (h "0baq0sk1r5xprs357gas4c4rkj5a5jpzkcsp8niprvpwslybv2i4")))

(define-public crate-solders-compute-budget-0.19.0 (c (n "solders-compute-budget") (v "0.19.0") (d (list (d (n "pyo3") (r "^0.19.2") (f (quote ("macros"))) (k 0)) (d (n "solana-sdk") (r "^1.17.12") (d #t) (k 0)) (d (n "solders-instruction") (r "=0.19.0") (d #t) (k 0)) (d (n "solders-pubkey") (r "=0.19.0") (d #t) (k 0)))) (h "0vmqqgpx36l54g87hykyb3lhj14bc82gh4bqin2xwqiw9ad3icky")))

(define-public crate-solders-compute-budget-0.20.0 (c (n "solders-compute-budget") (v "0.20.0") (d (list (d (n "pyo3") (r "^0.19.2") (f (quote ("macros"))) (k 0)) (d (n "solana-sdk") (r "^1.18.1") (d #t) (k 0)) (d (n "solders-instruction") (r "=0.20.0") (d #t) (k 0)) (d (n "solders-pubkey") (r "=0.20.0") (d #t) (k 0)))) (h "1dq3xkc7fzjhpp37h5a13jn4lh15ffw9ljbymp3zh2br4ydgg631")))

(define-public crate-solders-compute-budget-0.21.0 (c (n "solders-compute-budget") (v "0.21.0") (d (list (d (n "pyo3") (r "^0.20.2") (f (quote ("macros"))) (k 0)) (d (n "solana-sdk") (r "^1.18.1") (d #t) (k 0)) (d (n "solders-instruction") (r "=0.21.0") (d #t) (k 0)) (d (n "solders-pubkey") (r "=0.21.0") (d #t) (k 0)))) (h "1dx1dgh5pd948rbhbrc28z36zswa61wy8wpy2jfmj393snn4dwki")))

