(define-module (crates-io so ld solder) #:use-module (crates-io))

(define-public crate-solder-0.1.0 (c (n "solder") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "0p1j55gs1ycrbn2hzw0g4cx845av954jrfj4zalzdwaqk2nxb4zv")))

(define-public crate-solder-0.1.1 (c (n "solder") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "1zs4dklz02irnkgp7k984zlhw06ia1r8c0ilmaycbwaml0xblzw1")))

(define-public crate-solder-0.1.2 (c (n "solder") (v "0.1.2") (d (list (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "0lcc8qyi8isnk1d6i2zpn22zx4dwym5k87j4zdannb8vv6drb4d8")))

(define-public crate-solder-0.1.3 (c (n "solder") (v "0.1.3") (d (list (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "19a729jr0dk2kgd2cfqhq9c2580fhl04gy29qwdknl0rl47g9fkx")))

(define-public crate-solder-0.1.4 (c (n "solder") (v "0.1.4") (d (list (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "0dpsyx9xyjz29w3cyw1jpa48l4kmnd3bddf9xmfpysk5ql52ahif")))

(define-public crate-solder-0.1.5 (c (n "solder") (v "0.1.5") (d (list (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "1ihbivvw0ahl4ibymnacwxmxjkhgb9z345hzg5yn4g7wmgyww4lr")))

(define-public crate-solder-0.1.6 (c (n "solder") (v "0.1.6") (d (list (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "1mmlizbriz6h559qf4fx1vx7m0lkdadna5ckyrpb6yal3x96x3jh")))

