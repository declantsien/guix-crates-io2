(define-module (crates-io so ld solders-traits-core) #:use-module (crates-io))

(define-public crate-solders-traits-core-0.15.0 (c (n "solders-traits-core") (v "0.15.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "pyo3") (r "^0.18.0") (k 0)) (d (n "serde") (r "^1.0.136") (d #t) (k 0)) (d (n "serde_cbor") (r "^0.11.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 0)))) (h "14z027pmc8lh66cxc9ajq5xm76jhpg7yi4v8mxlpbgc7lyf2qbm1")))

(define-public crate-solders-traits-core-0.15.1 (c (n "solders-traits-core") (v "0.15.1") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "pyo3") (r "^0.18.0") (k 0)) (d (n "serde") (r "^1.0.136") (d #t) (k 0)) (d (n "serde_cbor") (r "^0.11.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 0)))) (h "0qqzx6lgg0b4b0985w10y54z2q87a99rla5maskjv8bkldp3gd68")))

(define-public crate-solders-traits-core-0.16.0 (c (n "solders-traits-core") (v "0.16.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "pyo3") (r "^0.18.0") (k 0)) (d (n "serde") (r "^1.0.136") (d #t) (k 0)) (d (n "serde_cbor") (r "^0.11.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 0)))) (h "00hgv86rcshw9ninzw55q66n3fvlasn0ah3clwj0fv8hzk43ighs")))

(define-public crate-solders-traits-core-0.17.0 (c (n "solders-traits-core") (v "0.17.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "pyo3") (r "^0.18.0") (k 0)) (d (n "serde") (r "^1.0.136") (d #t) (k 0)) (d (n "serde_cbor") (r "^0.11.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 0)))) (h "126n56qqrdm1d3qfls9iyvxdaypjhl1s0f7jzyyf92dqwp7pswjp")))

(define-public crate-solders-traits-core-0.18.0 (c (n "solders-traits-core") (v "0.18.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "pyo3") (r "^0.18.0") (k 0)) (d (n "serde") (r "^1.0.136") (d #t) (k 0)) (d (n "serde_cbor") (r "^0.11.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 0)))) (h "1j0syjsqvyz5jh4pr175py6gi4pdba8y9ds42r7n6zml24146738")))

(define-public crate-solders-traits-core-0.18.1 (c (n "solders-traits-core") (v "0.18.1") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "pyo3") (r "^0.18.0") (k 0)) (d (n "serde") (r "^1.0.136") (d #t) (k 0)) (d (n "serde_cbor") (r "^0.11.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 0)))) (h "0afy1mh6kk96v91dw1jxxksri8lslx2829dym2hzwzc68wc8xc59")))

(define-public crate-solders-traits-core-0.19.0 (c (n "solders-traits-core") (v "0.19.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "pyo3") (r "^0.19.2") (k 0)) (d (n "serde") (r "^1.0.188") (d #t) (k 0)) (d (n "serde_cbor") (r "^0.11.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.106") (d #t) (k 0)))) (h "1my5zbw97x5m59w0nv8559gp840x7x6gx4qg3h4wl10h9v0dyzra")))

(define-public crate-solders-traits-core-0.20.0 (c (n "solders-traits-core") (v "0.20.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "pyo3") (r "^0.19.2") (k 0)) (d (n "serde") (r "^1.0.188") (d #t) (k 0)) (d (n "serde_cbor") (r "^0.11.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.106") (d #t) (k 0)))) (h "0pp0qd7nfjjxsiabcaywz01gg60pwspdcmnmm1np8ag93qg3ipqs")))

(define-public crate-solders-traits-core-0.21.0 (c (n "solders-traits-core") (v "0.21.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "pyo3") (r "^0.20.2") (k 0)) (d (n "serde") (r "^1.0.188") (d #t) (k 0)) (d (n "serde_cbor") (r "^0.11.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.106") (d #t) (k 0)))) (h "0py0pqxk6vm27bkyjfg0hg2jvn2lfdys0h7bak3sqmh3ha4p6b8a")))

