(define-module (crates-io so lt solts) #:use-module (crates-io))

(define-public crate-solts-0.1.0 (c (n "solts") (v "0.1.0") (d (list (d (n "colored") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)))) (h "07nmqmklnisajdsgxwv73wwvvyxfkbhj7zgql30zfi490rfcybzs")))

(define-public crate-solts-0.1.1 (c (n "solts") (v "0.1.1") (d (list (d (n "colored") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)))) (h "1g6x9jj61jkvm8rqaznbippbsby4d3p10fv0pcpyq84apj3j2fjn")))

