(define-module (crates-io so lt soltrack) #:use-module (crates-io))

(define-public crate-soltrack-1.0.0 (c (n "soltrack") (v "1.0.0") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "solana-client") (r "^1.8.5") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.8.5") (d #t) (k 0)))) (h "1npy9x1239x9dzlw9zw148za6zd6l1ik7l370bnsgk6f5n4qq0hm")))

