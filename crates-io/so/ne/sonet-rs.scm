(define-module (crates-io so ne sonet-rs) #:use-module (crates-io))

(define-public crate-sonet-rs-1.0.0-beta (c (n "sonet-rs") (v "1.0.0-beta") (d (list (d (n "futures") (r "^0.3.19") (d #t) (k 0)) (d (n "tokio") (r "^1.13.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1mnqi9dpfkncpa706m5d3jqr1z1jrdjgvscv88ihvlfsgf23cmmb")))

(define-public crate-sonet-rs-1.0.0-RC (c (n "sonet-rs") (v "1.0.0-RC") (d (list (d (n "futures") (r "^0.3.19") (d #t) (k 0)) (d (n "tokio") (r "^1.13.0") (f (quote ("full"))) (d #t) (k 0)))) (h "10c4ldcilqr2ic90jfnyqqiwy8d6irbqxwr4ig5r9appr8p6lxl4")))

(define-public crate-sonet-rs-1.0.0 (c (n "sonet-rs") (v "1.0.0") (d (list (d (n "futures") (r "^0.3.19") (d #t) (k 0)) (d (n "tokio") (r "^1.13.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0r9ks6z2prrp7zcf54b0npndvdvmaikklni13b54d1xf6j8js7gc")))

