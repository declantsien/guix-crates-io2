(define-module (crates-io so no sonogram) #:use-module (crates-io))

(define-public crate-sonogram-0.2.0 (c (n "sonogram") (v "0.2.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "hound") (r "^3.4.0") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (d #t) (k 0)) (d (n "png") (r "^0.14.0") (d #t) (k 0)))) (h "1nkzz472r0zl9zfjahdpn8vp41jj73l5q17bxwg8b53yigdcsn14")))

(define-public crate-sonogram-0.2.1 (c (n "sonogram") (v "0.2.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "hound") (r "^3.4.0") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (d #t) (k 0)) (d (n "png") (r "^0.14.0") (d #t) (k 0)))) (h "0bbzl07cxjgx38m29mkz95v45pr57k90z7plyl0pb413n6j5hiqa")))

(define-public crate-sonogram-0.2.2 (c (n "sonogram") (v "0.2.2") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "hound") (r "^3.4.0") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (d #t) (k 0)) (d (n "png") (r "^0.14.0") (d #t) (k 0)))) (h "1vrvrgkyqbcyw45q64f77pixmiq9610mdz7cjvak0nn9lmz2fn74")))

(define-public crate-sonogram-0.3.0 (c (n "sonogram") (v "0.3.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "hound") (r "^3.4.0") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (d #t) (k 0)) (d (n "png") (r "^0.14.0") (d #t) (k 0)))) (h "0vql12zcxkb8b0s5z2hsva0vgysz1mp6lir718zmigblbkbmyld7")))

(define-public crate-sonogram-0.4.0 (c (n "sonogram") (v "0.4.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "hound") (r "^3.4.0") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (d #t) (k 0)) (d (n "png") (r "^0.14.0") (d #t) (k 0)))) (h "1y5xa304j9mraap3bbga1ljh5w2ga6fmm16zc968zv6nh82lp93b")))

(define-public crate-sonogram-0.4.1 (c (n "sonogram") (v "0.4.1") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "hound") (r "^3.4") (d #t) (k 0)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "png") (r "^0.14") (d #t) (k 0)))) (h "0n6wp42mb0fq0vcr2d63pq41gv9kf4674cxli0khsqnbnk5cf843")))

(define-public crate-sonogram-0.4.2 (c (n "sonogram") (v "0.4.2") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "hound") (r "^3.4") (d #t) (k 0)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "png") (r "^0.14") (d #t) (k 0)))) (h "145qi6qv6zligsl5kqb9y9ccciyxcg3425zhydykglnyiv2h879j")))

(define-public crate-sonogram-0.4.3 (c (n "sonogram") (v "0.4.3") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "hound") (r "^3.4") (d #t) (k 0)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "png") (r "^0.14") (d #t) (k 0)))) (h "0nhc2jhcrd85w17ick1knzcdvgydnj8wpgnskyjcf0azhx4b5d8l")))

(define-public crate-sonogram-0.4.5 (c (n "sonogram") (v "0.4.5") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "hound") (r "^3.4") (d #t) (k 0)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "png") (r "^0.14") (d #t) (k 0)))) (h "0ik11mb092bvaxcf1rw2val969pz42v6gb4ig7klk8b86x60b5ji")))

(define-public crate-sonogram-0.5.0 (c (n "sonogram") (v "0.5.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "hound") (r "^3.4") (d #t) (k 0)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "png") (r "^0.14") (d #t) (k 0)))) (h "0g9m5lxskipwnamf6p62nmsdmw9ncjxx9b5lxl2jax50qy56lhq9")))

(define-public crate-sonogram-0.6.0 (c (n "sonogram") (v "0.6.0") (d (list (d (n "clap") (r "^3.0.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "hound") (r "^3.4") (d #t) (k 0)) (d (n "png") (r "^0.14") (d #t) (k 0)) (d (n "resize") (r "^0.7.2") (d #t) (k 0)) (d (n "rgb") (r "^0.8.25") (d #t) (k 0)) (d (n "rustfft") (r "^6.0") (d #t) (k 0)))) (h "0b7nq862841qfj0mw3h4apk4f1szqarvwsm88dqms2bcq0yy87s4")))

(define-public crate-sonogram-0.6.1 (c (n "sonogram") (v "0.6.1") (d (list (d (n "clap") (r "^3.0.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "hound") (r "^3.4") (d #t) (k 0)) (d (n "png") (r "^0.14") (d #t) (k 0)) (d (n "resize") (r "^0.7.2") (d #t) (k 0)) (d (n "rgb") (r "^0.8.25") (d #t) (k 0)) (d (n "rustfft") (r "^6.0") (d #t) (k 0)))) (h "0rn5kflq7h4fj9vripkj2himkwz9n8yshi6k9v7jx8v29fg8p8c9")))

(define-public crate-sonogram-0.6.2 (c (n "sonogram") (v "0.6.2") (d (list (d (n "clap") (r "^3.0.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "hound") (r "^3.4") (d #t) (k 0)) (d (n "png") (r "^0.14") (d #t) (k 0)) (d (n "resize") (r "^0.7.2") (d #t) (k 0)) (d (n "rgb") (r "^0.8.25") (d #t) (k 0)) (d (n "rustfft") (r "^6.0") (d #t) (k 0)))) (h "1aaazlp3w2n512wxbcsfc9585i4ck66j3bgfh29cb4h04zdg6vv3")))

(define-public crate-sonogram-0.7.0 (c (n "sonogram") (v "0.7.0") (d (list (d (n "clap") (r "^3.0.14") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "hound") (r "^3.4") (o #t) (d #t) (k 0)) (d (n "png") (r "^0.14") (o #t) (d #t) (k 0)) (d (n "resize") (r "^0.7.2") (d #t) (k 0)) (d (n "rgb") (r "^0.8.25") (d #t) (k 0)) (d (n "rustfft") (r "^6.0") (d #t) (k 0)))) (h "03a6gg8vmgazzzwfwivz3rlvr2vprns0qivpiixzfqixziafkrgr") (f (quote (("default" "hound" "png") ("build-binary" "clap"))))))

(define-public crate-sonogram-0.7.1 (c (n "sonogram") (v "0.7.1") (d (list (d (n "clap") (r "^3.0.14") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "hound") (r "^3.4") (o #t) (d #t) (k 0)) (d (n "png") (r "^0.14") (o #t) (d #t) (k 0)) (d (n "resize") (r "^0.7.2") (d #t) (k 0)) (d (n "rgb") (r "^0.8.25") (d #t) (k 0)) (d (n "rustfft") (r "^6.0") (d #t) (k 0)))) (h "17wvhwqdfw19jxp6zm6ss962vzh5q9n4fd5l1n4k8wyavwb9bnxn") (f (quote (("default" "hound" "png") ("build-binary" "clap"))))))

