(define-module (crates-io so no sonos) #:use-module (crates-io))

(define-public crate-sonos-0.1.0 (c (n "sonos") (v "0.1.0") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)) (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.8") (d #t) (k 0)) (d (n "ssdp") (r "^0.6") (d #t) (k 0)) (d (n "xmltree") (r "^0.6") (d #t) (k 0)))) (h "19icc2kjb8yzlmaasddb805wf34359al7s0dh37j5bddrnnmfw72")))

(define-public crate-sonos-0.1.1 (c (n "sonos") (v "0.1.1") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)) (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.8") (d #t) (k 0)) (d (n "ssdp") (r "^0.6") (d #t) (k 0)) (d (n "xmltree") (r "^0.6") (d #t) (k 0)))) (h "1ln3mllscj7dv7y7l326jnpplsjm5b7zcfi1akp0zgvmnn86c1qm")))

(define-public crate-sonos-0.1.2 (c (n "sonos") (v "0.1.2") (d (list (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.8") (d #t) (k 0)) (d (n "ssdp") (r "^0.6") (d #t) (k 0)) (d (n "xmltree") (r "^0.6") (d #t) (k 0)))) (h "0bpam6q7zwzvjz6wr9iq5ssqj7q1wrw2n692hvgghv3sbbx8fxmb")))

(define-public crate-sonos-0.1.3 (c (n "sonos") (v "0.1.3") (d (list (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.8") (d #t) (k 0)) (d (n "ssdp") (r "^0.7") (d #t) (k 0)) (d (n "xmltree") (r "^0.6") (d #t) (k 0)))) (h "11kn0i6lbb985pdb7v29z6y7lwj0v9sqszghrcrw7x2fs2wcb83j")))

(define-public crate-sonos-0.1.4 (c (n "sonos") (v "0.1.4") (d (list (d (n "error-chain") (r "^0.12") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "ssdp") (r "^0.7") (d #t) (k 0)) (d (n "xmltree") (r "^0.10") (d #t) (k 0)))) (h "1q2r3z5p5pi3hxrcm3xlpv3fickhhsa2nn87ijvkmrs0ch5dpr5h")))

