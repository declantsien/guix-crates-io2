(define-module (crates-io so ra sora) #:use-module (crates-io))

(define-public crate-sora-0.0.1 (c (n "sora") (v "0.0.1") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "insta") (r "^1.39") (d #t) (k 2)) (d (n "memchr") (r "^2.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "simd-json") (r "^0.13") (f (quote ("runtime-detection" "swar-number-parsing"))) (k 0)) (d (n "simd-json-derive") (r "^0.13") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0dy42q73dq45y8zrq5kgjqwzy2yxihfhx2wzqklxbxfw6kwj4lqw") (f (quote (("index-map") ("extension") ("builder"))))))

(define-public crate-sora-0.0.2 (c (n "sora") (v "0.0.2") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "insta") (r "^1.39") (d #t) (k 2)) (d (n "memchr") (r "^2.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "simd-json") (r "^0.13") (f (quote ("runtime-detection" "swar-number-parsing"))) (k 0)) (d (n "simd-json-derive") (r "^0.13") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "08pfvzhmdy4g71pwfm66qzdfmf3rcxv9xvdfg0av7ks63qy614ak") (f (quote (("index-map") ("extension") ("builder"))))))

