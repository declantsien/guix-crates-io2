(define-module (crates-io so rb sorbus) #:use-module (crates-io))

(define-public crate-sorbus-0.5.0 (c (n "sorbus") (v "0.5.0") (h "0dlcbdqn9qf8kmc3n7308fql0avwxf7ll9snaz4pzb0a4mk7hjg1")))

(define-public crate-sorbus-0.7.0 (c (n "sorbus") (v "0.7.0") (h "1mj43l47vdn16yvz1frffhqqr4za6fmnn2i7mbh108jm4x177k44")))

(define-public crate-sorbus-0.8.0 (c (n "sorbus") (v "0.8.0") (h "0hhv7fj136vi514c30qiy2r8gbr3hshpm3dqfdwibjsxgxvzcnkw")))

(define-public crate-sorbus-0.8.1 (c (n "sorbus") (v "0.8.1") (d (list (d (n "identity-hash") (r "^0.1.0") (d #t) (k 0)))) (h "17hyrisvgpczm1ffmzlsvmpysnig9ldv6ah5vmgf1k7dr99gf5dy")))

(define-public crate-sorbus-0.9.0 (c (n "sorbus") (v "0.9.0") (d (list (d (n "identity-hash") (r "^0.1.0") (d #t) (k 0)))) (h "13iz8a9lfi9dvfpbxplb7vgiv235brhafgbqdb93pbl17w042hp3")))

(define-public crate-sorbus-0.10.0 (c (n "sorbus") (v "0.10.0") (d (list (d (n "identity-hash") (r "^0.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0w09hc0si0b4q7ncdhwxhd5msnq8bqrjims5njv6ab1l2l5sddy4")))

(define-public crate-sorbus-0.11.0 (c (n "sorbus") (v "0.11.0") (d (list (d (n "identity-hash") (r "^0.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "05hd0zq79ynmxv7k3jpr29apmrqd4gxmynwbj65wqbrgvcsr9xqy")))

(define-public crate-sorbus-0.12.0 (c (n "sorbus") (v "0.12.0") (d (list (d (n "identity-hash") (r "^0.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1y79vr3cqsiim6siqfq8ij8dz3w2xcdwlz5dpkzjwigaph12k8nl")))

(define-public crate-sorbus-0.12.1 (c (n "sorbus") (v "0.12.1") (d (list (d (n "identity-hash") (r "^0.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1lhrzk71clp709l62n680gb0g5y78b7bliyw5imf1w76rhbywbs9")))

