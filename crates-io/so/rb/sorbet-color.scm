(define-module (crates-io so rb sorbet-color) #:use-module (crates-io))

(define-public crate-sorbet-color-0.0.1 (c (n "sorbet-color") (v "0.0.1") (d (list (d (n "once_cell") (r "^1.9") (d #t) (k 2)))) (h "0i592m2dhavssbzb3llf9v0ivajgvks9r3a7aqnlf0ddkc0bbzzi")))

(define-public crate-sorbet-color-0.0.2 (c (n "sorbet-color") (v "0.0.2") (d (list (d (n "once_cell") (r "^1.9") (d #t) (k 2)))) (h "0zhmihq2bl80m69mz8rvgqwh59bwsgv5ck29vjs276v3gx9a3g3i")))

(define-public crate-sorbet-color-0.1.0 (c (n "sorbet-color") (v "0.1.0") (d (list (d (n "once_cell") (r "^1.9") (d #t) (k 2)) (d (n "strum") (r "^0.24") (f (quote ("derive"))) (d #t) (k 0)) (d (n "test-case") (r "^2") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "wgpu") (r "^0.12") (o #t) (d #t) (k 0)))) (h "05xqpg0rdjqmjpqhb089mfp2m7vn7hly37w8a6ck87z6zcg18h2j")))

(define-public crate-sorbet-color-0.1.1 (c (n "sorbet-color") (v "0.1.1") (d (list (d (n "once_cell") (r "^1.9") (d #t) (k 2)) (d (n "strum") (r "^0.24") (f (quote ("derive"))) (d #t) (k 0)) (d (n "test-case") (r "^2") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "wgpu") (r "^0.12") (o #t) (d #t) (k 0)))) (h "1q2178z9mrn9bf3zfakkcxxpw5m91ylp3mdij8a47y1cg37wsg4w")))

(define-public crate-sorbet-color-0.2.0 (c (n "sorbet-color") (v "0.2.0") (d (list (d (n "once_cell") (r "^1.9") (d #t) (k 2)) (d (n "strum") (r "^0.24") (f (quote ("derive"))) (d #t) (k 0)) (d (n "test-case") (r "^2") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "wgpu") (r "^0.12") (o #t) (d #t) (k 0)))) (h "0fdf1prf1cgihjhvxfivw0axc9ydlzmhysdnghr5js3bh6ncilzj")))

(define-public crate-sorbet-color-0.3.0 (c (n "sorbet-color") (v "0.3.0") (d (list (d (n "once_cell") (r "^1.9") (d #t) (k 2)) (d (n "strum") (r "^0.24") (f (quote ("derive"))) (d #t) (k 0)) (d (n "test-case") (r "^2") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "wgpu") (r "^0.12") (o #t) (d #t) (k 0)))) (h "18460iwa5mapy6rjr6b9pdbzcm4lxnp2lpw2503ak7wkyzg6xqqr")))

(define-public crate-sorbet-color-0.3.1 (c (n "sorbet-color") (v "0.3.1") (d (list (d (n "once_cell") (r "^1.10") (d #t) (k 2)) (d (n "strum") (r "^0.24") (f (quote ("derive"))) (d #t) (k 0)) (d (n "test-case") (r "^2") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "wgpu") (r "^0.12") (o #t) (d #t) (k 0)))) (h "1yb39h7yacz7amdc7870z1wgvvvsavca69mg1nxyngdclg5jr9c4")))

