(define-module (crates-io so ph sophus_lie) #:use-module (crates-io))

(define-public crate-sophus_lie-0.4.0 (c (n "sophus_lie") (v "0.4.0") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 0)) (d (n "assertables") (r "^7.0.1") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32") (f (quote ("rand"))) (d #t) (k 0)) (d (n "sophus_calculus") (r "^0.4.0") (d #t) (k 0)) (d (n "sophus_tensor") (r "^0.4.0") (d #t) (k 0)))) (h "0cz2iyrfsv9lklvfyhwwsx0cbq5rba5csbjz0kbybm5mn7sfp4nz")))

(define-public crate-sophus_lie-0.5.0 (c (n "sophus_lie") (v "0.5.0") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 0)) (d (n "assertables") (r "^7.0.1") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32") (f (quote ("rand"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "sophus_core") (r "^0.5.0") (d #t) (k 0)))) (h "0gl3sfr2sm575bc5j4r39zay3fpk16kmibzy4fycdwk5mwbqfdqd")))

(define-public crate-sophus_lie-0.6.0 (c (n "sophus_lie") (v "0.6.0") (d (list (d (n "approx") (r "^0.5") (d #t) (k 0)) (d (n "assertables") (r "^7.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32") (f (quote ("rand"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "sophus_core") (r "^0.6.0") (d #t) (k 0)))) (h "04xp48dkyv3vc9gbrila5dnqg71zhvxsm3hrvhphfayxszb1h2gh")))

