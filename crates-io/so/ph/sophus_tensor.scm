(define-module (crates-io so ph sophus_tensor) #:use-module (crates-io))

(define-public crate-sophus_tensor-0.4.0 (c (n "sophus_tensor") (v "0.4.0") (d (list (d (n "assertables") (r "^7.0.1") (d #t) (k 0)) (d (n "concat-arrays") (r "^0.1.2") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32") (f (quote ("rand"))) (d #t) (k 0)) (d (n "ndarray") (r "^0.15.4") (f (quote ("approx-0_5"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "simba") (r "^0.8.1") (d #t) (k 0)) (d (n "typenum") (r "^1.17.0") (f (quote ("const-generics"))) (d #t) (k 0)))) (h "1i5mjjdvqr3nmah7mkn43zx3363il86gz9vc1fpma03kwiyg68zz")))

