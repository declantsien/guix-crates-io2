(define-module (crates-io so ph sophus_calculus) #:use-module (crates-io))

(define-public crate-sophus_calculus-0.4.0 (c (n "sophus_calculus") (v "0.4.0") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 0)) (d (n "assertables") (r "^7.0.1") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32") (f (quote ("rand"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "simba") (r "^0.8.1") (d #t) (k 0)) (d (n "sophus_tensor") (r "^0.4.0") (d #t) (k 0)))) (h "0k3c062k301q9cixyyacjfg831j8wa2b5jw77d8wsl2wxqbsxk0x")))

