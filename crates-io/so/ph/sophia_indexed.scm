(define-module (crates-io so ph sophia_indexed) #:use-module (crates-io))

(define-public crate-sophia_indexed-0.7.0 (c (n "sophia_indexed") (v "0.7.0") (d (list (d (n "sophia_api") (r "^0.7.0") (d #t) (k 0)) (d (n "sophia_term") (r "^0.7.0") (d #t) (k 0)))) (h "1rnzfzirzbfzw1bkklfjjl56w7inih8lg3ymiq13ji8j997gwhzv")))

(define-public crate-sophia_indexed-0.7.1 (c (n "sophia_indexed") (v "0.7.1") (d (list (d (n "sophia_api") (r "^0.7.1") (d #t) (k 0)) (d (n "sophia_term") (r "^0.7.1") (d #t) (k 0)))) (h "0pw06yvcb3dbidmpr66417h81nm7qs291mwxvxfx8w422yhn4gyh")))

(define-public crate-sophia_indexed-0.7.2 (c (n "sophia_indexed") (v "0.7.2") (d (list (d (n "sophia_api") (r "^0.7.2") (d #t) (k 0)) (d (n "sophia_term") (r "^0.7.2") (d #t) (k 0)))) (h "1kjyn0idb20xdh4spg1pnjxqfsx622530vagzqi6mqzm7z6mlf7q")))

