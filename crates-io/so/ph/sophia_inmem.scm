(define-module (crates-io so ph sophia_inmem) #:use-module (crates-io))

(define-public crate-sophia_inmem-0.7.0 (c (n "sophia_inmem") (v "0.7.0") (d (list (d (n "sophia_api") (r "^0.7.0") (d #t) (k 0)) (d (n "sophia_api") (r "^0.7.0") (f (quote ("test_macro"))) (d #t) (k 2)) (d (n "sophia_indexed") (r "^0.7.0") (d #t) (k 0)) (d (n "sophia_term") (r "^0.7.0") (d #t) (k 0)))) (h "1g4gsyy5ckhklbslfxsfxkmjjmasnjbqy2z3hwwn4adpcpsjr8si")))

(define-public crate-sophia_inmem-0.7.1 (c (n "sophia_inmem") (v "0.7.1") (d (list (d (n "sophia_api") (r "^0.7.1") (d #t) (k 0)) (d (n "sophia_api") (r "^0.7.1") (f (quote ("test_macro"))) (d #t) (k 2)) (d (n "sophia_indexed") (r "^0.7.1") (d #t) (k 0)) (d (n "sophia_term") (r "^0.7.1") (d #t) (k 0)))) (h "1n2sg75zjmr4868nh27lpq0f47yh4xv4g8lcyan4xd2r2paqzjg9")))

(define-public crate-sophia_inmem-0.7.2 (c (n "sophia_inmem") (v "0.7.2") (d (list (d (n "sophia_api") (r "^0.7.2") (d #t) (k 0)) (d (n "sophia_api") (r "^0.7.2") (f (quote ("test_macro"))) (d #t) (k 2)) (d (n "sophia_indexed") (r "^0.7.2") (d #t) (k 0)) (d (n "sophia_term") (r "^0.7.2") (d #t) (k 0)))) (h "0b24lfjjk5y0g3dr368kkdm1q7w8ry3mjdm2ix6kr0ib6f391w18")))

(define-public crate-sophia_inmem-0.8.0-alpha.0 (c (n "sophia_inmem") (v "0.8.0-alpha.0") (d (list (d (n "sophia_api") (r "^0.8.0-alpha.0") (d #t) (k 0)) (d (n "sophia_api") (r "^0.8.0-alpha.0") (f (quote ("test_macro"))) (d #t) (k 2)))) (h "1y77746qa8i1hxn2piwvbhi268vdh98khs0q8jzm58d14wcqiq4a") (f (quote (("all_tests"))))))

(define-public crate-sophia_inmem-0.8.0-alpha.1 (c (n "sophia_inmem") (v "0.8.0-alpha.1") (d (list (d (n "sophia_api") (r "^0.8.0-alpha.1") (d #t) (k 0)) (d (n "sophia_api") (r "^0.8.0-alpha.1") (f (quote ("test_macro"))) (d #t) (k 2)))) (h "17ndcg989lh00bd46a92pppin522mswlcy600ah8wbj8vwvbwzj5") (f (quote (("all_tests"))))))

(define-public crate-sophia_inmem-0.8.0-alpha.2 (c (n "sophia_inmem") (v "0.8.0-alpha.2") (d (list (d (n "sophia_api") (r "^0.8.0-alpha.2") (d #t) (k 0)) (d (n "sophia_api") (r "^0.8.0-alpha.2") (f (quote ("test_macro"))) (d #t) (k 2)) (d (n "thiserror") (r "^1.0.32") (d #t) (k 0)))) (h "1wkwyy85z8pw5lzkvxw1cd4cr0xivqh4ss148yd46rfmhf6aycpx") (f (quote (("all_tests"))))))

(define-public crate-sophia_inmem-0.8.0-alpha.3 (c (n "sophia_inmem") (v "0.8.0-alpha.3") (d (list (d (n "sophia_api") (r "^0.8.0-alpha.3") (d #t) (k 0)) (d (n "sophia_api") (r "^0.8.0-alpha.3") (f (quote ("test_macro"))) (d #t) (k 2)) (d (n "thiserror") (r "^1.0.32") (d #t) (k 0)))) (h "1zg54m861lrkch003vd9h4jvilp1061203gmwhjbjdjwm9wpw9x5") (f (quote (("all_tests"))))))

(define-public crate-sophia_inmem-0.8.0 (c (n "sophia_inmem") (v "0.8.0") (d (list (d (n "sophia_api") (r "^0.8.0") (d #t) (k 0)) (d (n "sophia_api") (r "^0.8.0") (f (quote ("test_macro"))) (d #t) (k 2)) (d (n "thiserror") (r "^1.0.32") (d #t) (k 0)))) (h "130yighyqr28jn87h0baz4n8xasx3mg9jxx9fdd5vgcbi5mq6gxs") (f (quote (("all_tests"))))))

