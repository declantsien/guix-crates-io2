(define-module (crates-io so ph sophus_pyo3) #:use-module (crates-io))

(define-public crate-sophus_pyo3-0.4.0 (c (n "sophus_pyo3") (v "0.4.0") (d (list (d (n "nalgebra") (r "^0.32") (f (quote ("rand"))) (d #t) (k 0)) (d (n "numpy") (r "^0.20") (d #t) (k 0)) (d (n "pyo3") (r "^0.20.0") (f (quote ("abi3-py38" "multiple-pymethods"))) (d #t) (k 0)) (d (n "sophus_calculus") (r "^0.4.0") (d #t) (k 0)) (d (n "sophus_lie") (r "^0.4.0") (d #t) (k 0)) (d (n "sophus_tensor") (r "^0.4.0") (d #t) (k 0)))) (h "1d07xizimv9dnk23m6z9izg942dzkdhimh2vjjjky2f33a8knxzd")))

(define-public crate-sophus_pyo3-0.5.0 (c (n "sophus_pyo3") (v "0.5.0") (d (list (d (n "nalgebra") (r "^0.32") (f (quote ("rand"))) (d #t) (k 0)) (d (n "numpy") (r "^0.21") (d #t) (k 0)) (d (n "pyo3") (r "^0.21.0") (f (quote ("abi3-py38"))) (d #t) (k 0)) (d (n "sophus_core") (r "^0.5.0") (d #t) (k 0)) (d (n "sophus_lie") (r "^0.5.0") (d #t) (k 0)))) (h "0x4kr0jdiibn0sxzy71b7zapscl3grk97gbdwvcly0jvmx797jzw")))

(define-public crate-sophus_pyo3-0.6.0 (c (n "sophus_pyo3") (v "0.6.0") (d (list (d (n "nalgebra") (r "^0.32") (f (quote ("rand"))) (d #t) (k 0)) (d (n "numpy") (r "^0.21") (d #t) (k 0)) (d (n "pyo3") (r "^0.21.2") (f (quote ("abi3-py38"))) (d #t) (k 0)) (d (n "sophus_core") (r "^0.6.0") (d #t) (k 0)) (d (n "sophus_lie") (r "^0.6.0") (d #t) (k 0)))) (h "140bg0x630y65rbshw3jf689lalplimp38xbh2b951hx4h1nrs91")))

