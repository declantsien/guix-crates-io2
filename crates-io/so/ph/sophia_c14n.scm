(define-module (crates-io so ph sophia_c14n) #:use-module (crates-io))

(define-public crate-sophia_c14n-0.8.0-alpha.0 (c (n "sophia_c14n") (v "0.8.0-alpha.0") (d (list (d (n "hmac-sha256") (r "^1.1.6") (d #t) (k 0)) (d (n "sophia_api") (r "^0.8.0-alpha.0") (d #t) (k 0)) (d (n "sophia_iri") (r "^0.8.0-alpha.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.32") (d #t) (k 0)))) (h "18xq8gry7ib7951bzw9khsjb8f1z8aqggy16zxhwjs7v1cbs2i7q")))

(define-public crate-sophia_c14n-0.8.0-alpha.1 (c (n "sophia_c14n") (v "0.8.0-alpha.1") (d (list (d (n "hmac-sha256") (r "^1.1.6") (d #t) (k 0)) (d (n "sophia_api") (r "^0.8.0-alpha.1") (d #t) (k 0)) (d (n "sophia_iri") (r "^0.8.0-alpha.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.32") (d #t) (k 0)))) (h "12jc3izq0549p397anra0gzgr0cf4i6mxfg36w5yrwgrdwsv6jnd")))

(define-public crate-sophia_c14n-0.8.0-alpha.2 (c (n "sophia_c14n") (v "0.8.0-alpha.2") (d (list (d (n "sha2") (r "^0.10.7") (d #t) (k 0)) (d (n "sophia_api") (r "^0.8.0-alpha.2") (d #t) (k 0)) (d (n "sophia_iri") (r "^0.8.0-alpha.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.32") (d #t) (k 0)))) (h "1lnfag3s97kqq77b1n65awfjz182ha2a4kl08bqy64s9rjhhs5x9")))

(define-public crate-sophia_c14n-0.8.0-alpha.3 (c (n "sophia_c14n") (v "0.8.0-alpha.3") (d (list (d (n "sha2") (r "^0.10.7") (d #t) (k 0)) (d (n "sophia_api") (r "^0.8.0-alpha.3") (d #t) (k 0)) (d (n "sophia_iri") (r "^0.8.0-alpha.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.32") (d #t) (k 0)))) (h "16k11iypndci58ihz0cjm9kvjbarwgmiyyn6za19k6nnbpj03y6i")))

(define-public crate-sophia_c14n-0.8.0 (c (n "sophia_c14n") (v "0.8.0") (d (list (d (n "sha2") (r "^0.10.7") (d #t) (k 0)) (d (n "sophia_api") (r "^0.8.0") (d #t) (k 0)) (d (n "sophia_iri") (r "^0.8.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.32") (d #t) (k 0)))) (h "07badn247gqpb9nc4pba1gjgkl9anhj45xgiyg4rz1q4a7vfpr04")))

