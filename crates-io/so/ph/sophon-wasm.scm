(define-module (crates-io so ph sophon-wasm) #:use-module (crates-io))

(define-public crate-sophon-wasm-0.19.0 (c (n "sophon-wasm") (v "0.19.0") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.4") (d #t) (k 0)))) (h "1cdkr7j8i4k4fjw1sgxadh88q466ynajrhdkp4dppdlm88fb6i5d")))

(define-public crate-sophon-wasm-0.15.3 (c (n "sophon-wasm") (v "0.15.3") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.4") (d #t) (k 0)))) (h "0rpawk5jz9m91y2llakycnk0ag7y99h17mc5nfr5vprvvm0dg3b9")))

(define-public crate-sophon-wasm-0.18.0 (c (n "sophon-wasm") (v "0.18.0") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.4") (d #t) (k 0)))) (h "1iipz94ah0gqcfkdc80p03vfvchl0l694ippjq01d485fb5mqdx6")))

(define-public crate-sophon-wasm-0.15.30 (c (n "sophon-wasm") (v "0.15.30") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.4") (d #t) (k 0)))) (h "0b97n5rc5c3nciszsw93jr1ghhnxw05v9d9v95kqqppfznar3vkx")))

(define-public crate-sophon-wasm-0.15.31 (c (n "sophon-wasm") (v "0.15.31") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.4") (d #t) (k 0)))) (h "1lsq4vgr8ryzj7rv9acxvm0yrq18pbjw89svscnvv48b8c53qvvm")))

(define-public crate-sophon-wasm-0.18.1 (c (n "sophon-wasm") (v "0.18.1") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.4") (d #t) (k 0)))) (h "1hqj9cbjhwly9hs73ylvi2rbffaq9j4dh2fnrkqs34ykasydy3p4")))

