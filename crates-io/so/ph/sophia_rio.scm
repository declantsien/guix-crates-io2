(define-module (crates-io so ph sophia_rio) #:use-module (crates-io))

(define-public crate-sophia_rio-0.7.0 (c (n "sophia_rio") (v "0.7.0") (d (list (d (n "rio_api") (r "^0.6.0") (f (quote ("generalized"))) (d #t) (k 0)) (d (n "sophia_api") (r "^0.7.0") (d #t) (k 0)))) (h "057h95mwh4dx58p6jw6l1i9wq1ar2bcx7r5cwzmvbd8kn9nxs7qv")))

(define-public crate-sophia_rio-0.7.1 (c (n "sophia_rio") (v "0.7.1") (d (list (d (n "rio_api") (r "^0.6.1") (f (quote ("generalized"))) (d #t) (k 0)) (d (n "sophia_api") (r "^0.7.1") (d #t) (k 0)))) (h "0lngiymx6b72h8jw1vvcmvdsynnw3s9395lij96cilr7sh7ail7j")))

(define-public crate-sophia_rio-0.7.2 (c (n "sophia_rio") (v "0.7.2") (d (list (d (n "rio_api") (r "^0.6.1") (f (quote ("generalized"))) (d #t) (k 0)) (d (n "sophia_api") (r "^0.7.2") (d #t) (k 0)))) (h "04930i6p8wi80yj7rjfyajilyw6jyqswwcf5w08rsq8j53rx5p7v")))

(define-public crate-sophia_rio-0.8.0-alpha.0 (c (n "sophia_rio") (v "0.8.0-alpha.0") (d (list (d (n "rio_api") (r "^0.8") (f (quote ("generalized"))) (d #t) (k 0)) (d (n "sophia_api") (r "^0.8.0-alpha.0") (d #t) (k 0)) (d (n "sophia_iri") (r "^0.8.0-alpha.0") (d #t) (k 0)))) (h "114fhxvqy04bmbpf2sja7qs4bqrjc8sd1j6pnjfiwkjd3q0li72i")))

(define-public crate-sophia_rio-0.8.0-alpha.1 (c (n "sophia_rio") (v "0.8.0-alpha.1") (d (list (d (n "rio_api") (r "^0.8") (f (quote ("generalized"))) (d #t) (k 0)) (d (n "sophia_api") (r "^0.8.0-alpha.1") (d #t) (k 0)) (d (n "sophia_iri") (r "^0.8.0-alpha.1") (d #t) (k 0)))) (h "06dri293jvp90rialxvfa3zw995j4madg0pq6anji8z6v4313062")))

(define-public crate-sophia_rio-0.8.0-alpha.2 (c (n "sophia_rio") (v "0.8.0-alpha.2") (d (list (d (n "rio_api") (r "^0.8") (f (quote ("generalized"))) (d #t) (k 0)) (d (n "sophia_api") (r "^0.8.0-alpha.2") (d #t) (k 0)) (d (n "sophia_iri") (r "^0.8.0-alpha.2") (d #t) (k 0)))) (h "1c67jhygll415xy9y423862s7zqi7vixdniblfwn9dzkplmkzd5f")))

(define-public crate-sophia_rio-0.8.0-alpha.3 (c (n "sophia_rio") (v "0.8.0-alpha.3") (d (list (d (n "rio_api") (r "^0.8") (f (quote ("generalized"))) (d #t) (k 0)) (d (n "sophia_api") (r "^0.8.0-alpha.3") (d #t) (k 0)) (d (n "sophia_iri") (r "^0.8.0-alpha.3") (d #t) (k 0)))) (h "12d50y1zqxkgfprnm5ibhm93b9djai1xz800xshdvr0v8lc4pv48")))

(define-public crate-sophia_rio-0.8.0 (c (n "sophia_rio") (v "0.8.0") (d (list (d (n "rio_api") (r "^0.8") (f (quote ("generalized"))) (d #t) (k 0)) (d (n "sophia_api") (r "^0.8.0") (d #t) (k 0)) (d (n "sophia_iri") (r "^0.8.0") (d #t) (k 0)))) (h "1n6l2gvdzfa2i4rl2ly8g2sgpc84wnf5zy70vcly7cx2dh40gqck")))

