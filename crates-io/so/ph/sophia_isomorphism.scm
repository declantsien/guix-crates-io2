(define-module (crates-io so ph sophia_isomorphism) #:use-module (crates-io))

(define-public crate-sophia_isomorphism-0.8.0-alpha.0 (c (n "sophia_isomorphism") (v "0.8.0-alpha.0") (d (list (d (n "sophia_api") (r "^0.8.0-alpha.0") (d #t) (k 0)) (d (n "sophia_iri") (r "^0.8.0-alpha.0") (d #t) (k 0)))) (h "03sp5rndqsr9s1zqi1j33n6cya6935iy0abxi7iq3j8in3qszvxw")))

(define-public crate-sophia_isomorphism-0.8.0-alpha.1 (c (n "sophia_isomorphism") (v "0.8.0-alpha.1") (d (list (d (n "sophia_api") (r "^0.8.0-alpha.1") (d #t) (k 0)) (d (n "sophia_iri") (r "^0.8.0-alpha.1") (d #t) (k 0)))) (h "1fl4vxrfxi2znfm6c3jypk0vy5dx2ipgnffx2flialybiffcdj7r")))

(define-public crate-sophia_isomorphism-0.8.0-alpha.2 (c (n "sophia_isomorphism") (v "0.8.0-alpha.2") (d (list (d (n "sophia_api") (r "^0.8.0-alpha.2") (d #t) (k 0)) (d (n "sophia_iri") (r "^0.8.0-alpha.2") (d #t) (k 0)))) (h "0sdh19933qk5hl1hc4wfa9g86cknnvg1hgwxw97b90p6jbvph3sf")))

(define-public crate-sophia_isomorphism-0.8.0-alpha.3 (c (n "sophia_isomorphism") (v "0.8.0-alpha.3") (d (list (d (n "sophia_api") (r "^0.8.0-alpha.3") (d #t) (k 0)) (d (n "sophia_iri") (r "^0.8.0-alpha.3") (d #t) (k 0)))) (h "140z3brkyx5wysdzbsn9fhl0nlnn0lj44s37b8vgyd12l3bp98zm")))

(define-public crate-sophia_isomorphism-0.8.0 (c (n "sophia_isomorphism") (v "0.8.0") (d (list (d (n "sophia_api") (r "^0.8.0") (d #t) (k 0)) (d (n "sophia_iri") (r "^0.8.0") (d #t) (k 0)))) (h "0pka5x5m3jlydwqmq7kb7yc8la9nl2811ich8pcksvbmf5n8gczj")))

