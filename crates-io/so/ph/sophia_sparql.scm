(define-module (crates-io so ph sophia_sparql) #:use-module (crates-io))

(define-public crate-sophia_sparql-0.1.0-alpha.0 (c (n "sophia_sparql") (v "0.1.0-alpha.0") (d (list (d (n "oxiri") (r "^0.2.2") (d #t) (k 0)) (d (n "sophia") (r "^0.8.0-alpha.3") (d #t) (k 0)) (d (n "spargebra") (r "^0.2.8") (f (quote ("rdf-star"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)))) (h "02v5a9sj6bkrfqakp526ih1ix7nq1irj7achivzclkl6rb5q7cyn")))

