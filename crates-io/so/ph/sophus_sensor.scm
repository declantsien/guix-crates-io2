(define-module (crates-io so ph sophus_sensor) #:use-module (crates-io))

(define-public crate-sophus_sensor-0.4.0 (c (n "sophus_sensor") (v "0.4.0") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 0)) (d (n "assertables") (r "^7.0.1") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32") (f (quote ("rand"))) (d #t) (k 0)) (d (n "ndarray") (r "^0.15.4") (f (quote ("approx-0_5"))) (d #t) (k 0)) (d (n "sophus_calculus") (r "^0.4.0") (d #t) (k 0)) (d (n "sophus_image") (r "^0.4.0") (d #t) (k 0)))) (h "167asajir6nsfnvq59xn257qzr61b8cdwyj44wlc6k4xs6wky0y6")))

(define-public crate-sophus_sensor-0.5.0 (c (n "sophus_sensor") (v "0.5.0") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 0)) (d (n "assertables") (r "^7.0.1") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32") (f (quote ("rand"))) (d #t) (k 0)) (d (n "ndarray") (r "^0.15.4") (f (quote ("approx-0_5"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "sophus_core") (r "^0.5.0") (d #t) (k 0)) (d (n "sophus_image") (r "^0.5.0") (d #t) (k 0)))) (h "05rc3iyqgrp4v9l2sqnjsbqrc8vlyf9x2szmnxx5ihqdyzdyawni")))

(define-public crate-sophus_sensor-0.6.0 (c (n "sophus_sensor") (v "0.6.0") (d (list (d (n "approx") (r "^0.5") (d #t) (k 0)) (d (n "assertables") (r "^7.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32") (f (quote ("rand"))) (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (f (quote ("approx-0_5"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "sophus_core") (r "^0.6.0") (d #t) (k 0)) (d (n "sophus_image") (r "^0.6.0") (d #t) (k 0)))) (h "1kd65siz3x6bn5xrlcv5cc8gmmpvj6kz6ks5x6wilsyjkmm8a2rl")))

