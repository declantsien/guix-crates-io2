(define-module (crates-io so ph sophia_resource) #:use-module (crates-io))

(define-public crate-sophia_resource-0.8.0-alpha.1 (c (n "sophia_resource") (v "0.8.0-alpha.1") (d (list (d (n "sophia_api") (r "^0.8.0-alpha.1") (d #t) (k 0)) (d (n "sophia_iri") (r "^0.8.0-alpha.1") (d #t) (k 0)) (d (n "sophia_turtle") (r "^0.8.0-alpha.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.32") (d #t) (k 0)))) (h "0vr2dzcci0kv8msab80waa9910c29prmclmy64aa0cgw16jlk3w9")))

(define-public crate-sophia_resource-0.8.0-alpha.2 (c (n "sophia_resource") (v "0.8.0-alpha.2") (d (list (d (n "sophia_api") (r "^0.8.0-alpha.2") (d #t) (k 0)) (d (n "sophia_iri") (r "^0.8.0-alpha.2") (d #t) (k 0)) (d (n "sophia_turtle") (r "^0.8.0-alpha.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.32") (d #t) (k 0)))) (h "07085cq8cwkchg25z7kfszs2z12rp49h3faanh4nhq21ys94m51d")))

(define-public crate-sophia_resource-0.8.0-alpha.3 (c (n "sophia_resource") (v "0.8.0-alpha.3") (d (list (d (n "sophia_api") (r "^0.8.0-alpha.3") (d #t) (k 0)) (d (n "sophia_iri") (r "^0.8.0-alpha.3") (d #t) (k 0)) (d (n "sophia_turtle") (r "^0.8.0-alpha.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.32") (d #t) (k 0)))) (h "1zl7kvl9vflbrwhs48q53x3nk39aiqa7i71ayb7bi3dpqfqfpbrn")))

(define-public crate-sophia_resource-0.8.0 (c (n "sophia_resource") (v "0.8.0") (d (list (d (n "futures-util") (r "^0.3.28") (o #t) (d #t) (k 0)) (d (n "sophia_api") (r "^0.8.0") (d #t) (k 0)) (d (n "sophia_iri") (r "^0.8.0") (d #t) (k 0)) (d (n "sophia_jsonld") (r "^0.8.0") (o #t) (d #t) (k 0)) (d (n "sophia_turtle") (r "^0.8.0") (d #t) (k 0)) (d (n "sophia_xml") (r "^0.8.0") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.32") (d #t) (k 0)))) (h "1qkci08gscvwy16l2s7m7x4vsk5hv94ni5znmhz1ykhllk9m6wl0") (f (quote (("xml" "sophia_xml") ("jsonld" "sophia_jsonld" "futures-util") ("http_client") ("file_url"))))))

