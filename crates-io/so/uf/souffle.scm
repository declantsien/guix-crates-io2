(define-module (crates-io so uf souffle) #:use-module (crates-io))

(define-public crate-souffle-0.1.0 (c (n "souffle") (v "0.1.0") (h "0vkgxc05ijvycq3ls5891hkpzhky45k7zxw68zmx7hjqqbdmavv7") (y #t)))

(define-public crate-souffle-0.0.1 (c (n "souffle") (v "0.0.1") (h "053v6i0r5pv813rsq8azyrvqgv4ixbxiw2ic1kr9khddwny2panl")))

