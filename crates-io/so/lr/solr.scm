(define-module (crates-io so lr solr) #:use-module (crates-io))

(define-public crate-solr-0.1.0 (c (n "solr") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.3.4") (d #t) (k 0)) (d (n "errno") (r "^0.1.8") (d #t) (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.19") (d #t) (k 0)) (d (n "semver") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^0.8.0") (d #t) (k 0)))) (h "102rxxhxml8ckwsa89c27h22prahi29c1661pn6krz1bv7garkj2")))

