(define-module (crates-io so lr solrand) #:use-module (crates-io))

(define-public crate-solrand-0.1.3 (c (n "solrand") (v "0.1.3") (d (list (d (n "anchor-lang") (r "^0.18.2") (d #t) (k 0)))) (h "0pdb26br7bx4k1nkyahdl9z2zjr5fzhkdlk7wsxc1ikmy4k7ki1g") (f (quote (("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-solrand-0.1.5 (c (n "solrand") (v "0.1.5") (d (list (d (n "anchor-lang") (r "^0.24.2") (d #t) (k 0)))) (h "0jjyn2j56phjh4xa2lfy7di3lqqgi2nipah357iwdnhs771m04kx") (f (quote (("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

