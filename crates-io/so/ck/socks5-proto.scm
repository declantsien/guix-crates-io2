(define-module (crates-io so ck socks5-proto) #:use-module (crates-io))

(define-public crate-socks5-proto-0.1.0 (c (n "socks5-proto") (v "0.1.0") (d (list (d (n "bytes") (r "^1.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.17") (f (quote ("io-util"))) (d #t) (k 0)))) (h "0dzpfqxmhpgsrpqzkv5alxnxab4vk58bcsj9jfnlval91p78yh8q") (y #t)))

(define-public crate-socks5-proto-0.2.0 (c (n "socks5-proto") (v "0.2.0") (d (list (d (n "bytes") (r "^1.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.18") (f (quote ("io-util"))) (d #t) (k 0)))) (h "12s76z9k7vlvdhs0p95p2csfv6m346gwq8f70sqxj0rr34fzw77n") (y #t)))

(define-public crate-socks5-proto-0.3.0 (c (n "socks5-proto") (v "0.3.0") (d (list (d (n "bytes") (r "^1.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.19") (f (quote ("io-util"))) (d #t) (k 0)))) (h "16az1gmf0isr328z9q362p28ni6pcir6crbhkxcp9j5rlky2d0y6") (y #t)))

(define-public crate-socks5-proto-0.3.1 (c (n "socks5-proto") (v "0.3.1") (d (list (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "bytes") (r "^1.1") (d #t) (k 0)) (d (n "tokio") (r "^1.19") (f (quote ("io-util"))) (d #t) (k 0)))) (h "0l4415hq39276j03dj7480fxyiavxy76bda2j0vd2r0wmpi5xsk0") (y #t)))

(define-public crate-socks5-proto-0.3.2 (c (n "socks5-proto") (v "0.3.2") (d (list (d (n "byteorder") (r "1.4.*") (d #t) (k 0)) (d (n "bytes") (r "1.2.*") (d #t) (k 0)) (d (n "tokio") (r "1.20.*") (f (quote ("io-util"))) (d #t) (k 0)))) (h "02a52k5g3j30l0mdp9i15g2xsvrv4cmhxsa3lmshfg46jwyf2sp5") (y #t)))

(define-public crate-socks5-proto-0.3.3 (c (n "socks5-proto") (v "0.3.3") (d (list (d (n "byteorder") (r "^1.4.3") (f (quote ("std"))) (k 0)) (d (n "bytes") (r "^1.3.0") (f (quote ("std"))) (k 0)) (d (n "tokio") (r "^1.24.2") (f (quote ("io-util"))) (k 0)))) (h "1zsk42xyba7gp8s0ypqgfkqn5411b8237s0sscc28ppxpzdwpldq")))

(define-public crate-socks5-proto-0.4.0 (c (n "socks5-proto") (v "0.4.0") (d (list (d (n "bytes") (r "^1.4.0") (f (quote ("std"))) (k 0)) (d (n "thiserror") (r "^1.0.43") (k 0)) (d (n "tokio") (r "^1.29.1") (f (quote ("io-util"))) (k 0)))) (h "0hq5li3xmhrdlvlsnc804gqya65rwywdlw219jmd02va0ki1cwjw")))

(define-public crate-socks5-proto-0.4.1 (c (n "socks5-proto") (v "0.4.1") (d (list (d (n "bytes") (r "^1.6.0") (f (quote ("std"))) (k 0)) (d (n "thiserror") (r "^1.0.58") (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("io-util"))) (k 0)))) (h "1i7828md66m7cy9gj21nq1l50ccgpracaszl5qvmxqkj8qf4749x")))

