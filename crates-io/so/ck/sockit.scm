(define-module (crates-io so ck sockit) #:use-module (crates-io))

(define-public crate-sockit-0.1.0 (c (n "sockit") (v "0.1.0") (h "0ypaggv8v4n4q0g2wkgw51f7wpndygl0rzfyb98jl4qk9c9r2a2d")))

(define-public crate-sockit-0.2.0 (c (n "sockit") (v "0.2.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "bytes") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.156") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("full"))) (d #t) (k 2)))) (h "0ykx0qpbpdmwicdfv7y8f9w01gnw24rz12byi477mb67ap6dvk32")))

(define-public crate-sockit-0.2.1 (c (n "sockit") (v "0.2.1") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "bytes") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.156") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("full"))) (d #t) (k 2)))) (h "1ybh8da01is5bh7amixsqn1bqdbklh3z14f7syr03n4j1aj9iwfb")))

