(define-module (crates-io so ck sock5s) #:use-module (crates-io))

(define-public crate-sock5s-0.2.0 (c (n "sock5s") (v "0.2.0") (d (list (d (n "async-trait") (r "^0.1.51") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "libc") (r "^0.2.98") (d #t) (k 0)) (d (n "tokio") (r "^1.9.0") (f (quote ("io-util" "macros" "rt-multi-thread" "net"))) (d #t) (k 0)) (d (n "tokio-stream") (r "^0.1.7") (d #t) (k 0)))) (h "0025m9iv7mr021mwp1bg2gbl2k8sz6qsjj164rmk640q6pf317jk")))

(define-public crate-sock5s-0.2.1 (c (n "sock5s") (v "0.2.1") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "libc") (r "^0.2.99") (d #t) (k 0)) (d (n "socket2") (r "^0.4.1") (d #t) (k 0)) (d (n "tokio") (r "^1.9.0") (f (quote ("io-util" "macros" "rt-multi-thread" "net"))) (d #t) (k 0)) (d (n "tokio-stream") (r "^0.1.7") (d #t) (k 0)))) (h "0lnj321q975k0d82lp33bm6vcv2mcpvcs355kx94za8bssybwlvj")))

