(define-module (crates-io so ck socks5-client) #:use-module (crates-io))

(define-public crate-socks5-client-0.1.0 (c (n "socks5-client") (v "0.1.0") (d (list (d (n "amplify") (r "^4.0.0-beta.4") (d #t) (k 0)) (d (n "cypheraddr") (r "^0.1.0") (d #t) (k 0)))) (h "0wi0yjr2y24656yxm04y70rnimsharq9jmw3yb5lkxspc8vnf3yq") (r "1.65")))

(define-public crate-socks5-client-0.2.0 (c (n "socks5-client") (v "0.2.0") (d (list (d (n "amplify") (r "^4.0.0-beta.17") (d #t) (k 0)) (d (n "cypheraddr") (r "^0.2.0") (d #t) (k 0)))) (h "0y8vxiaqbjx5jcz2lqxag8x3fqdyn9rcbi5yxkmkd56gaxnik4a0") (r "1.65")))

(define-public crate-socks5-client-0.3.0 (c (n "socks5-client") (v "0.3.0") (d (list (d (n "amplify") (r "^4.0.0") (d #t) (k 0)) (d (n "cypheraddr") (r "^0.3.0") (d #t) (k 0)))) (h "0pzsyd71fpqz5bx6d0bkx2r97d5871y4grwh97hvcv5j8m2pzhn0") (r "1.66")))

(define-public crate-socks5-client-0.4.0 (c (n "socks5-client") (v "0.4.0") (d (list (d (n "amplify") (r "^4.6.0") (d #t) (k 0)) (d (n "cypheraddr") (r "^0.3.0") (d #t) (k 0)))) (h "0k7hvyjywa4rb41w6hr2yvn3jdmygqgwmv8pc8jixxbnxsxc82xj") (f (quote (("tor" "cypheraddr/tor") ("nym" "cypheraddr/nym") ("i2p" "cypheraddr/i2p") ("dns" "cypheraddr/dns")))) (r "1.69")))

(define-public crate-socks5-client-0.4.1 (c (n "socks5-client") (v "0.4.1") (d (list (d (n "amplify") (r "^4.6.0") (d #t) (k 0)) (d (n "cypheraddr") (r "^0.4.0") (d #t) (k 0)))) (h "0k1jpbpc0wqqqykz27706vj6rmsqqr66l01kss15vmmizbvdrizz") (f (quote (("tor" "cypheraddr/tor") ("nym" "cypheraddr/nym") ("i2p" "cypheraddr/i2p") ("dns" "cypheraddr/dns")))) (r "1.69")))

