(define-module (crates-io so ck socket_io_server) #:use-module (crates-io))

(define-public crate-socket_io_server-0.1.0 (c (n "socket_io_server") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.40") (d #t) (k 0)) (d (n "engine_io_server") (r "^0.1.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)) (d (n "warp") (r "^0.2") (f (quote ("websocket"))) (d #t) (k 0)))) (h "1dyia0jd2v0rabl0zfbisfpzq0i2pqaf847wr3pf3xs7gkp8l7fl")))

