(define-module (crates-io so ck socketlabz) #:use-module (crates-io))

(define-public crate-socketlabz-0.1.4 (c (n "socketlabz") (v "0.1.4") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.70") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.70") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.24") (d #t) (k 0)))) (h "1yja2x6nmfv8rcf7gz66927ys78q6bq8vkwi0rydsqsxpblpwn1y")))

