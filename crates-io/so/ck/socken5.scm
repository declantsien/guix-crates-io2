(define-module (crates-io so ck socken5) #:use-module (crates-io))

(define-public crate-socken5-0.1.0 (c (n "socken5") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.51") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "tokio") (r "^1.14.0") (f (quote ("net" "io-util"))) (d #t) (k 0)))) (h "1i07yq82dq1byqxawsrrkkfpp3glryj526fcycnkyaryz8c5nl7d")))

