(define-module (crates-io so ck socketlabs) #:use-module (crates-io))

(define-public crate-socketlabs-0.1.0 (c (n "socketlabs") (v "0.1.0") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.8.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.70") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.70") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.24") (d #t) (k 0)))) (h "0pzdc43c7jfx9qbdqvh47x60jy47dqic0wxk5k01vsmz2g0wscm2")))

(define-public crate-socketlabs-0.1.1 (c (n "socketlabs") (v "0.1.1") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.8.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.70") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.70") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.24") (d #t) (k 0)))) (h "0f762bhjhad21vdl0l2d7nd7kvyn75r5bwk30ddbkk3ym0xi46c9")))

(define-public crate-socketlabs-0.1.2 (c (n "socketlabs") (v "0.1.2") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.8.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.70") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.70") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.24") (d #t) (k 0)))) (h "11wm7dr590561hxx1xc26i9fp38dmipcwvlls3698jgk62mg1kr1")))

(define-public crate-socketlabs-0.1.3 (c (n "socketlabs") (v "0.1.3") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.8.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.70") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.70") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.24") (d #t) (k 0)))) (h "05m5mkq9gwinkahllxc7mf67jd3fcqs5a6677riv92b7pcwazzrr")))

(define-public crate-socketlabs-0.2.1 (c (n "socketlabs") (v "0.2.1") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.18") (d #t) (k 0)) (d (n "serde") (r "^1.0.93") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.93") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)))) (h "0vx539rvs7qgbkh7n8ajfg92dn0d8npw4541g2jsq43m7541j0fz")))

