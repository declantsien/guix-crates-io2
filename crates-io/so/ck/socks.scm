(define-module (crates-io so ck socks) #:use-module (crates-io))

(define-public crate-socks-0.1.0 (c (n "socks") (v "0.1.0") (d (list (d (n "byteorder") (r "^0.4") (d #t) (k 0)))) (h "1mac92l3y78qqgib7lvj7j5f757nmdysjjll6gbf285g0c71jvkp")))

(define-public crate-socks-0.2.0 (c (n "socks") (v "0.2.0") (d (list (d (n "byteorder") (r "^0.4") (d #t) (k 0)))) (h "02w9q875dvv849d8xx2lq2nyrcpxnvq0hgyx32q80iry5fwpb46r")))

(define-public crate-socks-0.2.1 (c (n "socks") (v "0.2.1") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "net2") (r "^0.2.21") (d #t) (k 0)))) (h "1j6fwk14sbadlyip6zkwf40lzccylxvrq3d45yrn8zvf27prj06v")))

(define-public crate-socks-0.2.2 (c (n "socks") (v "0.2.2") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)))) (h "1x5kdawnpfbp1iwsprc1khixvfj388bn6j3wk4d82ia09x0r1w8c")))

(define-public crate-socks-0.2.3 (c (n "socks") (v "0.2.3") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)))) (h "168jamig6g5rjrp86snbr5c287w66frfcz0qhqq793z8dzhxj2cb")))

(define-public crate-socks-0.3.0 (c (n "socks") (v "0.3.0") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)))) (h "11339jlshxg9q9glapbg2msmahyrs8sc5yjbm0rza3av3issrdks")))

(define-public crate-socks-0.3.1 (c (n "socks") (v "0.3.1") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "ws2_32-sys") (r "^0.1") (d #t) (t "cfg(windows)") (k 0)))) (h "0470dazzibnr4v4my0b12ccg87yghbmmr7s2kicx4rrqs83gwdgm")))

(define-public crate-socks-0.3.2 (c (n "socks") (v "0.3.2") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.2.8") (d #t) (t "cfg(windows)") (k 0)) (d (n "ws2_32-sys") (r "^0.2.1") (d #t) (t "cfg(windows)") (k 0)))) (h "1hnbw4c4j7dn9n3bd1v7ddkdzlxlzkfw3z29da1nxlj6jgx4r9p6")))

(define-public crate-socks-0.3.3 (c (n "socks") (v "0.3.3") (d (list (d (n "byteorder") (r ">=1.0.0, <2.0.0") (d #t) (k 0)) (d (n "libc") (r ">=0.2.0, <0.3.0") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r ">=0.2.8, <0.3.0") (d #t) (t "cfg(windows)") (k 0)) (d (n "ws2_32-sys") (r ">=0.2.1, <0.3.0") (d #t) (t "cfg(windows)") (k 0)))) (h "1bk1gz4vajimkwlww8klms47a05hxzpnfkqs40a2ipzs6mv6ry1h")))

(define-public crate-socks-0.3.4 (c (n "socks") (v "0.3.4") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("winsock2"))) (d #t) (t "cfg(windows)") (k 0)))) (h "12ymihhib0zybm6n4mrvh39hj1dm0ya8mqnqdly63079kayxphzh")))

