(define-module (crates-io so ck socket-server-mocker) #:use-module (crates-io))

(define-public crate-socket-server-mocker-0.0.1 (c (n "socket-server-mocker") (v "0.0.1") (h "052na1cdpw8h98n5229jpr49gv2jwhh3dsg7p6dg2xmvg6hlsdb1")))

(define-public crate-socket-server-mocker-0.0.2 (c (n "socket-server-mocker") (v "0.0.2") (d (list (d (n "postgres") (r "^0.19.4") (d #t) (k 2)) (d (n "reqwest") (r "^0.11.12") (f (quote ("blocking"))) (d #t) (k 2)))) (h "0bzmz6bjqnss1za60l9q3yjrrkfbk07wvjfi3106nziadapazibg")))

(define-public crate-socket-server-mocker-0.0.3 (c (n "socket-server-mocker") (v "0.0.3") (d (list (d (n "lettre") (r "^0.10.1") (d #t) (k 2)) (d (n "postgres") (r "^0.19.4") (d #t) (k 2)) (d (n "reqwest") (r "^0.11.12") (f (quote ("blocking"))) (d #t) (k 2)) (d (n "trust-dns-client") (r "^0.22.0") (d #t) (k 2)))) (h "0qi4ll3f3ww6xgzyml9rhjf4n37g22wlhjakgk51mqanriixf8xj")))

(define-public crate-socket-server-mocker-0.0.4 (c (n "socket-server-mocker") (v "0.0.4") (d (list (d (n "lettre") (r "^0.10.1") (d #t) (k 2)) (d (n "postgres") (r "^0.19.4") (d #t) (k 2)) (d (n "reqwest") (r "^0.11.12") (f (quote ("blocking"))) (d #t) (k 2)) (d (n "trust-dns-client") (r "^0.22.0") (d #t) (k 2)))) (h "0plg3l7kkjccll7pv3qq153m0sg8ld4nlg6v60fr6fhqr25d447s")))

