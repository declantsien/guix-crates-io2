(define-module (crates-io so ck socks5-forwarder) #:use-module (crates-io))

(define-public crate-socks5-forwarder-0.1.0 (c (n "socks5-forwarder") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "env_logger") (r "^0.8") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("net" "rt" "rt-multi-thread" "macros"))) (d #t) (k 0)) (d (n "tokio-socks") (r "^0.5") (d #t) (k 0)) (d (n "tokio-stream") (r "^0.1") (f (quote ("net"))) (d #t) (k 0)))) (h "1rznm7cc29bxggrjb0kmd5ivlraas8z8j7cqvzj8xagbrlyy6p1s")))

