(define-module (crates-io so ck socket-notify) #:use-module (crates-io))

(define-public crate-socket-notify-0.1.0 (c (n "socket-notify") (v "0.1.0") (d (list (d (n "error-chain") (r "^0.11.0") (d #t) (k 0)) (d (n "notify-rust") (r "^3") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "1j5ma354sv7vi4ripgw9p9s01i6mlsrzb1dqmqgqzj8dd525i5gx")))

(define-public crate-socket-notify-0.1.1 (c (n "socket-notify") (v "0.1.1") (d (list (d (n "error-chain") (r "^0.11.0") (d #t) (k 0)) (d (n "notify-rust") (r "^3") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "16gl8nmj4nyv1vxfa46p7p7y7hl3ynkyp2rz2jiv199cpjmysgna")))

