(define-module (crates-io so ck socketstat) #:use-module (crates-io))

(define-public crate-socketstat-0.1.0 (c (n "socketstat") (v "0.1.0") (d (list (d (n "nix") (r "^0.15") (d #t) (t "cfg(unix)") (k 0)) (d (n "palaver") (r "^0.2") (d #t) (k 0)))) (h "0i0vnpzhfpr5da4g6ips7ak3qsgna9asibhs4kpbs5ypsqpnn3zi")))

