(define-module (crates-io so ck socket2-ext) #:use-module (crates-io))

(define-public crate-socket2-ext-0.1.0 (c (n "socket2-ext") (v "0.1.0") (d (list (d (n "ipconfig") (r "^0.3") (d #t) (t "cfg(windows)") (k 0)) (d (n "libc") (r "^0.2.150") (d #t) (t "cfg(unix)") (k 0)) (d (n "socket2") (r "^0.5.6") (f (quote ("all"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (k 0)))) (h "14xyka20dbixc61gil25n950jgrlvkh3zpbximdb2770b9bbd0n9") (f (quote (("win_static_ifname") ("log") ("default"))))))

(define-public crate-socket2-ext-0.1.1 (c (n "socket2-ext") (v "0.1.1") (d (list (d (n "ipconfig") (r "^0.3") (d #t) (t "cfg(windows)") (k 0)) (d (n "libc") (r "^0.2.150") (d #t) (t "cfg(unix)") (k 0)) (d (n "socket2") (r "^0.5.6") (f (quote ("all"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (k 0)))) (h "19nfq3vvn6qqbsfa9albh9az3irzjzjy9xx8x6l1x6923zvv2dy3") (f (quote (("log") ("default"))))))

