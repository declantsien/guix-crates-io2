(define-module (crates-io so ck socket-pktinfo) #:use-module (crates-io))

(define-public crate-socket-pktinfo-0.1.0 (c (n "socket-pktinfo") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.153") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "network-interface") (r "^1.1.1") (d #t) (k 2)) (d (n "socket2") (r "^0.4") (f (quote ("all"))) (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("mswsock"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1ad8nydibp91v58p5ra1y247dzl1cvf0m0zadjinp9pjw671hn3h") (r "1.61.0")))

(define-public crate-socket-pktinfo-0.2.0 (c (n "socket-pktinfo") (v "0.2.0") (d (list (d (n "libc") (r "^0.2.153") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "network-interface") (r "^1.1.1") (d #t) (k 2)) (d (n "socket2") (r "^0.5.5") (f (quote ("all"))) (d #t) (k 0)) (d (n "windows-sys") (r "^0.48") (f (quote ("Win32_Foundation" "Win32_Networking_WinSock" "Win32_System_IO"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0skr4cfb9wjd6kzw7wnrakfhdrvgwaww7hcnxf3nb191av4ic1dy") (r "1.63.0")))

(define-public crate-socket-pktinfo-0.2.1 (c (n "socket-pktinfo") (v "0.2.1") (d (list (d (n "libc") (r "^0.2.153") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "network-interface") (r "^1.1.1") (d #t) (k 2)) (d (n "socket2") (r "^0.5.5") (f (quote ("all"))) (d #t) (k 0)) (d (n "windows-sys") (r "^0.48") (f (quote ("Win32_Foundation" "Win32_Networking_WinSock" "Win32_System_IO"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1w57rikc7fipg8n4smc1vh636n9g0pfh5sg7d778gx7cf0rf71q8") (r "1.63.0")))

