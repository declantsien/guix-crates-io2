(define-module (crates-io so ck socket) #:use-module (crates-io))

(define-public crate-socket-0.0.1 (c (n "socket") (v "0.0.1") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "1graz2kc5fh978cya9ra3gxz53p6hlwv905yrlsnvbnis396gszs")))

(define-public crate-socket-0.0.2 (c (n "socket") (v "0.0.2") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "0lk2zl0xrhh4pwb5k70axb7zfx0g0bk0d13p1f8d1xf4m6mb4bsw")))

(define-public crate-socket-0.0.3 (c (n "socket") (v "0.0.3") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "1fddaaf9bnd1q217yrhk203rq0gzy752gkj9yflkfvmhj2d06i58")))

(define-public crate-socket-0.0.4 (c (n "socket") (v "0.0.4") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "1fw55r8pa9xl8agfzq2pw4d30ssn26qrdbj72x24mzifjv5miils")))

(define-public crate-socket-0.0.5 (c (n "socket") (v "0.0.5") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "01wgic126ll8cvb5khc57w8x7cvzsi6hks8l2w0b4xs1251a04ps")))

(define-public crate-socket-0.0.6 (c (n "socket") (v "0.0.6") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "14035zzl2paz18yhfydj49asl4g0lznr0by2ki7gcy1b9j2brwwk")))

(define-public crate-socket-0.0.7 (c (n "socket") (v "0.0.7") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)))) (h "0lxkzqdlc7fba9qdmmxw0am22h4wf7fwkgc3xq1x15248bbwbw9n")))

