(define-module (crates-io so ck sockets) #:use-module (crates-io))

(define-public crate-sockets-0.1.0 (c (n "sockets") (v "0.1.0") (h "1x8vj1f3scglnhvggjgxbkk5gvmwzmpadg5xikp1v2h058k6biqc")))

(define-public crate-sockets-0.1.1 (c (n "sockets") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (o #t) (d #t) (k 0)) (d (n "socket2") (r "^0.4.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "tokio") (r "^1.24.2") (f (quote ("net" "time" "sync" "rt" "macros"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "0d81h5ww3rwa0gfy98ya79gd8i0ma68j7zx5ydcligy523j16k42") (f (quote (("default" "json")))) (s 2) (e (quote (("serde" "dep:serde") ("json" "dep:serde_json" "serde"))))))

(define-public crate-sockets-0.1.2 (c (n "sockets") (v "0.1.2") (d (list (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (o #t) (d #t) (k 0)) (d (n "socket2") (r "^0.4.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "tokio") (r "^1.24.2") (f (quote ("net" "time" "sync" "rt" "macros"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "0gix2wa2cbw3rr3m3a43gg88872xhpl2h40s44xdhlj8360ar6yh") (f (quote (("default" "json")))) (s 2) (e (quote (("serde" "dep:serde") ("json" "dep:serde_json" "serde"))))))

