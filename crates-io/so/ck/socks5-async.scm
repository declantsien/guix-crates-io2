(define-module (crates-io so ck socks5-async) #:use-module (crates-io))

(define-public crate-socks5-async-0.1.0 (c (n "socks5-async") (v "0.1.0") (d (list (d (n "futures") (r "^0.3.5") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "17gra67lb2xavalgwfgr8wh61348x71b2bfppqh225r34lg8n33w")))

(define-public crate-socks5-async-0.1.1 (c (n "socks5-async") (v "0.1.1") (d (list (d (n "futures") (r "^0.3.5") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "1wl0hqf9ib2d7msa9m8r3n5wgjns1vp81k5zw4nli6hxxq1wp0zg")))

(define-public crate-socks5-async-0.1.2 (c (n "socks5-async") (v "0.1.2") (d (list (d (n "futures") (r "^0.3.5") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "0h2ipg1jsdggvmz2lc0fic30z1mhxdlxyv6326ikzs4nghyfgxvs")))

(define-public crate-socks5-async-0.1.3 (c (n "socks5-async") (v "0.1.3") (d (list (d (n "futures") (r "^0.3.5") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "05vqi2x3k28k4f4q5rfqklpabd4dvl57f9yirnkb6l0sfhqla08k")))

(define-public crate-socks5-async-0.1.4 (c (n "socks5-async") (v "0.1.4") (d (list (d (n "futures") (r "^0.3.5") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "0miahdid71d189zcmgg4pwzckgshrvpyjjkdz6867ng354wwlm62")))

