(define-module (crates-io so ck socket-io) #:use-module (crates-io))

(define-public crate-socket-io-0.1.0 (c (n "socket-io") (v "0.1.0") (d (list (d (n "engine-io") (r "^0.1.1") (d #t) (k 0)) (d (n "iron") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^0.8.0") (d #t) (k 0)) (d (n "serde_json") (r "^0.8.0") (d #t) (k 0)))) (h "0cc2sc41p25bp5qc9b42gg8h4pg4n2dz10m6kbgycf68x9f250z5")))

(define-public crate-socket-io-0.1.1 (c (n "socket-io") (v "0.1.1") (d (list (d (n "engine-io") (r "^0.1.1") (d #t) (k 0)) (d (n "iron") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^0.8.0") (d #t) (k 0)) (d (n "serde_json") (r "^0.8.0") (d #t) (k 0)))) (h "0gayi48gb4j3yfc5z849qs3b9k19ig5i0v1g686807d3c8k1hdng")))

