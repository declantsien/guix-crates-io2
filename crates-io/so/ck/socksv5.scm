(define-module (crates-io so ck socksv5) #:use-module (crates-io))

(define-public crate-socksv5-0.1.0 (c (n "socksv5") (v "0.1.0") (h "1nblz3j9r2m3vylmbs5j15sq9pmn7jh66zrmrxvcg2z3x05nj3sq")))

(define-public crate-socksv5-0.2.0 (c (n "socksv5") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "futures") (r "^0.3.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.19") (d #t) (k 0)))) (h "1y2bdk0kcfkjpvm5i6jxzj24icn61m5vklkkaz6q72v9ddql8qzb")))

(define-public crate-socksv5-0.3.0 (c (n "socksv5") (v "0.3.0") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "futures") (r "^0.3.8") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)) (d (n "tokio_compat") (r "^1.0.2") (f (quote ("macros" "io-util"))) (o #t) (d #t) (k 0) (p "tokio")))) (h "0cdzidsdha08g7slk2xxn79ikqa6ih9d06f04xfhkys16av2d3n3") (f (quote (("tokio" "tokio_compat") ("default" "futures"))))))

(define-public crate-socksv5-0.3.1 (c (n "socksv5") (v "0.3.1") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "futures") (r "^0.3.8") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)) (d (n "tokio_compat") (r "^1.0.2") (f (quote ("macros" "io-util"))) (o #t) (d #t) (k 0) (p "tokio")))) (h "0lcaxggnmw1a5x4mqc3r2dnljgsv4afzk6qq4r9fmvi78wzhvk5a") (f (quote (("tokio" "tokio_compat") ("default" "futures"))))))

