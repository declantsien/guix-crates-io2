(define-module (crates-io so ck socketio-rust-emitter) #:use-module (crates-io))

(define-public crate-socketio-rust-emitter-0.1.0 (c (n "socketio-rust-emitter") (v "0.1.0") (d (list (d (n "redis") (r "^0.10.0") (d #t) (k 0)) (d (n "rmp") (r "^0.8.7") (d #t) (k 0)) (d (n "rmp-serde") (r "^0.13.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.91") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.91") (d #t) (k 0)))) (h "1qi5yqjnziif4rawaklxmycavixv1ggipqim0ihyslizsvffn1cm")))

(define-public crate-socketio-rust-emitter-0.1.2 (c (n "socketio-rust-emitter") (v "0.1.2") (d (list (d (n "redis") (r "^0.10.0") (d #t) (k 0)) (d (n "rmp") (r "^0.8.7") (d #t) (k 0)) (d (n "rmp-serde") (r "^0.13.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.91") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.91") (d #t) (k 0)))) (h "0pdpmmggnvfpn7xm9jnhiwj03yji2cqg5kfwc3ls94ahm3z962jq")))

