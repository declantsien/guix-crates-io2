(define-module (crates-io so ck socket_addr_macros) #:use-module (crates-io))

(define-public crate-socket_addr_macros-1.0.0 (c (n "socket_addr_macros") (v "1.0.0") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)))) (h "1xz8azkxbkgzk82d0gq7llji7njrkl4629qbb5s3ip402fckhmgc") (y #t)))

(define-public crate-socket_addr_macros-1.0.1 (c (n "socket_addr_macros") (v "1.0.1") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)))) (h "0j2d0f7y26v4l1yxn1912r79j21f0vkzgvdh61my01arsg2c6abi")))

