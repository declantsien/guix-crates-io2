(define-module (crates-io so ck socket_selector) #:use-module (crates-io))

(define-public crate-socket_selector-0.1.0 (c (n "socket_selector") (v "0.1.0") (d (list (d (n "mio") (r "^0.8.10") (f (quote ("os-poll" "net"))) (d #t) (k 0)))) (h "1vqirvydk7mwynzcnhr4q2ir28d2mlf9mrq4cxjwi94b4wa0iwgb")))

(define-public crate-socket_selector-0.1.1 (c (n "socket_selector") (v "0.1.1") (d (list (d (n "mio") (r "^0.8.10") (f (quote ("os-poll" "net"))) (d #t) (k 0)))) (h "0saj1h3la3hz5rrby5a8j1rmgg5r1vg403v0p3driiwbv3smr3c8")))

(define-public crate-socket_selector-0.1.2 (c (n "socket_selector") (v "0.1.2") (d (list (d (n "mio") (r "^0.8.10") (f (quote ("os-poll" "net"))) (d #t) (k 0)))) (h "0caxrf3w0mkyi66mwc7hfjj075p6gl71cxljn7fqw3xczhx9a3r0")))

(define-public crate-socket_selector-0.1.3 (c (n "socket_selector") (v "0.1.3") (d (list (d (n "mio") (r "^0.8.10") (f (quote ("os-poll" "net"))) (d #t) (k 0)))) (h "1hrbjb788rgl9b2vfflhh4riyzbnfj2w174m0dkmh5qhlml1h2d3")))

