(define-module (crates-io so ck socks5-protocol) #:use-module (crates-io))

(define-public crate-socks5-protocol-0.1.0 (c (n "socks5-protocol") (v "0.1.0") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("io-util"))) (d #t) (k 0)))) (h "0cpyn6sccx4q7jk8clf2lbhsiwpijs16j3z1yyrfv0zpxqqa9dsp") (f (quote (("doc" "tokio/rt-multi-thread" "tokio/macros") ("default"))))))

(define-public crate-socks5-protocol-0.1.1 (c (n "socks5-protocol") (v "0.1.1") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("io-util"))) (d #t) (k 0)))) (h "1gv4bc3616m6jni6lgxhn5d1kk861icj5nz8n6gxk3q8ckja8f22") (f (quote (("doc" "tokio/rt-multi-thread" "tokio/macros") ("default"))))))

(define-public crate-socks5-protocol-0.2.0 (c (n "socks5-protocol") (v "0.2.0") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("io-util"))) (d #t) (k 0)))) (h "1k5vfd6bg0j15fla9nliqdb0r8b53w3s27xz5bi47ncarrab2alb") (f (quote (("doc" "tokio/rt-multi-thread" "tokio/macros") ("default"))))))

(define-public crate-socks5-protocol-0.3.0 (c (n "socks5-protocol") (v "0.3.0") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("io-util"))) (d #t) (k 0)))) (h "10sf30is561rwmx37z8gs50wq915hz7vhbis5y0v5j2p1lhjjcm0") (f (quote (("doc" "tokio/rt-multi-thread" "tokio/macros") ("default"))))))

(define-public crate-socks5-protocol-0.3.1 (c (n "socks5-protocol") (v "0.3.1") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("io-util"))) (d #t) (k 0)))) (h "0b8i3n1sprjq3mab647g9946mn8iy9gda0l8wy6b111qjvwgbjfj") (f (quote (("sync") ("doc" "tokio/rt-multi-thread" "tokio/macros") ("default" "sync"))))))

(define-public crate-socks5-protocol-0.3.2 (c (n "socks5-protocol") (v "0.3.2") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("io-util"))) (d #t) (k 0)))) (h "1k6x2k3d5rwjn4i1nf20vlp9v2v35npi79zxnha11mafm7zhn0g5") (f (quote (("sync") ("doc" "tokio/rt-multi-thread" "tokio/macros") ("default" "sync"))))))

(define-public crate-socks5-protocol-0.3.3 (c (n "socks5-protocol") (v "0.3.3") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("io-util"))) (d #t) (k 0)))) (h "0259qy50yafpnkzz9pxcgwp05dd3m2mxq3mfldfvg1rhbj534cyd") (f (quote (("sync") ("doc" "tokio/rt-multi-thread" "tokio/macros") ("default" "sync"))))))

(define-public crate-socks5-protocol-0.3.4 (c (n "socks5-protocol") (v "0.3.4") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("io-util"))) (d #t) (k 0)))) (h "041vq5dap2yfsj6dch4csjm2g3qfz5fir5lsl6ckq712gggbj5l3") (f (quote (("sync") ("doc" "tokio/rt-multi-thread" "tokio/macros") ("default" "sync"))))))

(define-public crate-socks5-protocol-0.3.5 (c (n "socks5-protocol") (v "0.3.5") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("io-util"))) (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 2)))) (h "05wiv79kfy0564f12ank9h6vpgml6hr6as4nqqd0mgaq71g95d14") (f (quote (("sync") ("doc" "tokio/rt-multi-thread" "tokio/macros") ("default" "sync"))))))

