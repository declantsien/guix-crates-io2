(define-module (crates-io so ck socktee) #:use-module (crates-io))

(define-public crate-socktee-0.1.0 (c (n "socktee") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "dgrambuf") (r "^0.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "nix") (r "^0.23.1") (d #t) (k 0)) (d (n "problem") (r "^5.3.0") (d #t) (k 0)) (d (n "stderrlog") (r "^0.5.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.2") (d #t) (k 0)) (d (n "tai64") (r "^4.0.0") (d #t) (k 0)))) (h "1nx8zf91029qpnqn6x0xxmjvxs6pyjsjykdlvk9xr6w8v0wb5xz0")))

