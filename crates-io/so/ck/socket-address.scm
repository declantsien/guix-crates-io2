(define-module (crates-io so ck socket-address) #:use-module (crates-io))

(define-public crate-socket-address-0.1.0 (c (n "socket-address") (v "0.1.0") (d (list (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)))) (h "0s9mw1alk8y486n5qsmwznvkcmcq3slxqli8qhbzxjwaqynfh2g0")))

(define-public crate-socket-address-0.2.0 (c (n "socket-address") (v "0.2.0") (d (list (d (n "http") (r "^0.2.9") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)))) (h "040gx660qjca0nv1knq4vhfbraw9sz88rplxmrsmfph573ggn3f4")))

