(define-module (crates-io so ck socks5-protocol-async) #:use-module (crates-io))

(define-public crate-socks5-protocol-async-0.2.2 (c (n "socks5-protocol-async") (v "0.2.2") (d (list (d (n "enum-primitive-derive") (r "^0.1") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "futures-io") (r "^0.3") (d #t) (k 0)) (d (n "futures-test") (r "^0.3") (d #t) (k 2)) (d (n "futures-util") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "19lyjxnhxdxgggb6409wq6w3c1yhhjqa5kb3ivmh2yyhqkr813dm")))

(define-public crate-socks5-protocol-async-0.2.3 (c (n "socks5-protocol-async") (v "0.2.3") (d (list (d (n "enum-primitive-derive") (r "^0.1") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "futures-io") (r "^0.3") (d #t) (k 0)) (d (n "futures-test") (r "^0.3") (d #t) (k 2)) (d (n "futures-util") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0wahxldd8rd0hz0ci7rp7ayfbrvr6n1v8n5fdp6l9p1kynyq9v0l")))

