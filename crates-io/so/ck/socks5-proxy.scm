(define-module (crates-io so ck socks5-proxy) #:use-module (crates-io))

(define-public crate-socks5-proxy-0.1.0 (c (n "socks5-proxy") (v "0.1.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0gqy24776aa3rpang0mxabd82yvq425ajgi1kghqln1si6hkfdj2")))

(define-public crate-socks5-proxy-0.1.1 (c (n "socks5-proxy") (v "0.1.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0f6yv5yqayishwx2xizkpbnvm0psjpsmb2nvdqr0l8wy4iikbv0k")))

