(define-module (crates-io so ck socksv5_future) #:use-module (crates-io))

(define-public crate-socksv5_future-0.1.0 (c (n "socksv5_future") (v "0.1.0") (h "12y0wqrasgdp1057qf88cifsnf9s4ca0fi7z1ymvd2qyxk4anny1")))

(define-public crate-socksv5_future-0.1.1 (c (n "socksv5_future") (v "0.1.1") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)) (d (n "tokio-timer") (r "^0.1") (d #t) (k 0)))) (h "0vvskwwrk0ylvynqb63i5hnwg3s31k9vl2k83ppymzvsfvjjhndi")))

(define-public crate-socksv5_future-0.1.2 (c (n "socksv5_future") (v "0.1.2") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)) (d (n "tokio-timer") (r "^0.1") (d #t) (k 0)))) (h "1wn1wlqyfz3zkchp6bn6485qljjmp3av1y75li8lg6k3mjh1h86p")))

(define-public crate-socksv5_future-0.1.3 (c (n "socksv5_future") (v "0.1.3") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)) (d (n "tokio-timer") (r "^0.1") (d #t) (k 0)))) (h "1vd9z8fzp9b1scikbf825wqfz2h7r2rmkia1pqnjframr8048nyh")))

(define-public crate-socksv5_future-0.2.1 (c (n "socksv5_future") (v "0.2.1") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)) (d (n "tokio-timer") (r "^0.1") (d #t) (k 0)))) (h "1n3dc6wsykfb4jyj0xiqw8idapwz6flba7r6hb6fighwr33n078j")))

(define-public crate-socksv5_future-0.2.2 (c (n "socksv5_future") (v "0.2.2") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)) (d (n "tokio-timer") (r "^0.1") (d #t) (k 0)))) (h "1afsbivmfdkzi6pdpzlmwran3vlldi896vhja2rvnzl5achwf1jv")))

(define-public crate-socksv5_future-0.2.3 (c (n "socksv5_future") (v "0.2.3") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)) (d (n "tokio-timer") (r "^0.1") (d #t) (k 0)))) (h "0dj2waf5giiz6hrgavad7i38fz7nflcd47zs2jc5wzlys01xw78a")))

(define-public crate-socksv5_future-0.2.5 (c (n "socksv5_future") (v "0.2.5") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)) (d (n "tokio-timer") (r "^0.1") (d #t) (k 0)))) (h "15q0qwb72gl8c6a5d2ajr0ziqap3f4wxyw610mgxqp6yb7v2l532")))

