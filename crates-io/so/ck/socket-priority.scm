(define-module (crates-io so ck socket-priority) #:use-module (crates-io))

(define-public crate-socket-priority-0.1.0 (c (n "socket-priority") (v "0.1.0") (d (list (d (n "clippy") (r "~0") (o #t) (d #t) (k 0)) (d (n "libc") (r "~0.2") (d #t) (k 0)))) (h "1rcj4z3g5sv8cl8fpz2g1nmqyky7j179gjysmm9kqa1rs93jhfy9")))

(define-public crate-socket-priority-0.1.1 (c (n "socket-priority") (v "0.1.1") (d (list (d (n "clippy") (r "~0") (o #t) (d #t) (k 0)) (d (n "libc") (r "~0.2") (d #t) (k 0)))) (h "1qr2rh1hsgyxgr4rw6vhbrg1wjdnx6f508xvf00iph0dld47p19i")))

(define-public crate-socket-priority-0.1.2 (c (n "socket-priority") (v "0.1.2") (d (list (d (n "clippy") (r "~0") (o #t) (d #t) (k 0)) (d (n "libc") (r "~0.2") (d #t) (k 0)))) (h "0iwqfkd1w51yjbpvsqrpah1vj43bjz08kzr0l83i1rwrkyiwlvaa")))

(define-public crate-socket-priority-0.1.3 (c (n "socket-priority") (v "0.1.3") (d (list (d (n "clippy") (r "~0") (o #t) (d #t) (k 0)) (d (n "libc") (r "~0.2") (d #t) (k 0)))) (h "10a3zyj2vms93b23id94f88kqwsi8hm15083iq9km1rimdx1b34w")))

(define-public crate-socket-priority-0.1.4 (c (n "socket-priority") (v "0.1.4") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1fp7siglnd8n7sv0i5gbjawhvn9d0bw6zyb2m15h08kblbc07rwz")))

(define-public crate-socket-priority-0.1.5 (c (n "socket-priority") (v "0.1.5") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "187ixkg5l95mnmvxmbnp0h0kxw9ql52vfq9bwnjfci2m0q5qyvjn")))

