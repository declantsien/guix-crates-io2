(define-module (crates-io so ck socketcan2) #:use-module (crates-io))

(define-public crate-socketcan2-0.1.0 (c (n "socketcan2") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "libc") (r "^0.2.137") (d #t) (k 0)))) (h "1kxfmbd8l8d9dgdbzqwjzhwk2maj0g1ks77zq2z2knpkyxc2rw5s") (y #t)))

(define-public crate-socketcan2-0.1.1 (c (n "socketcan2") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "libc") (r "^0.2.137") (d #t) (k 0)))) (h "1yginf3sm8xc0fb9sanbpvxbx5mmirbfbbg4jhm13533f4bgiqaq") (y #t)))

(define-public crate-socketcan2-0.1.2 (c (n "socketcan2") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "libc") (r "^0.2.137") (d #t) (k 0)))) (h "122da9h1a7qvvw3z4yv94mkp8b7hx08fbi5kdvcskdq01h63i81l") (y #t)))

(define-public crate-socketcan2-0.1.3 (c (n "socketcan2") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "libc") (r "^0.2.137") (d #t) (k 0)))) (h "1x4jvyay3llcnzqcly2x04dcsd27la3xpyvld9l4c452nfr0alki") (y #t)))

(define-public crate-socketcan2-0.1.4 (c (n "socketcan2") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "libc") (r "^0.2.137") (d #t) (k 0)))) (h "1piikxkq5s05p5j4ghi6w6q26lya1w188lmpcf2mzfia12kfimx3") (y #t)))

(define-public crate-socketcan2-0.1.5 (c (n "socketcan2") (v "0.1.5") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "libc") (r "^0.2.137") (d #t) (k 0)))) (h "18qbrivaa60xggdzrviayp7nqvkgcflx85ymy2cwvdc0y6lvn5vs")))

