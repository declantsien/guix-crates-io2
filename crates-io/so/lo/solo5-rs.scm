(define-module (crates-io so lo solo5-rs) #:use-module (crates-io))

(define-public crate-solo5-rs-0.1.0 (c (n "solo5-rs") (v "0.1.0") (d (list (d (n "linked_list_allocator") (r "^0.10.5") (d #t) (k 0)) (d (n "smoltcp") (r "^0.10.0") (f (quote ("alloc" "medium-ethernet" "proto-ipv4" "socket-tcp" "proto-ipv4"))) (k 0)) (d (n "solo5-rs-macros") (r "^0.1.0") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "solo5-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "thiserror-no-std") (r "^2.0.2") (d #t) (k 0)))) (h "1fddjhpjc447dch7fkpg84yddmjh0ln2qblymrn0qwx81lshg47q") (f (quote (("tlsf_dump") ("net"))))))

