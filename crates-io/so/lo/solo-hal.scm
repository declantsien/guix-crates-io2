(define-module (crates-io so lo solo-hal) #:use-module (crates-io))

(define-public crate-solo-hal-0.0.0 (c (n "solo-hal") (v "0.0.0") (d (list (d (n "cortex-m") (r "^0.5.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.3") (d #t) (k 0)) (d (n "cortex-m-semihosting") (r "^0.3.1") (d #t) (k 0)) (d (n "panic-halt") (r "^0.2.0") (d #t) (k 0)))) (h "1379fcv8iynh47z5jxzwvdvqm2c94k3mdnpv2yrdrqq5iqirwnar")))

