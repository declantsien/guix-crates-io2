(define-module (crates-io so lo solomka-sdk-macro) #:use-module (crates-io))

(define-public crate-solomka-sdk-macro-1.16.0 (c (n "solomka-sdk-macro") (v "1.16.0") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.12") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0kv478fw5gdjr2158sn69mc1myfvph851l013vwz50b6bgdcc10m") (y #t)))

(define-public crate-solomka-sdk-macro-1.14.20 (c (n "solomka-sdk-macro") (v "1.14.20") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1dn89x778hhmm0znl572jnf0x7p0l6q4w8r9lbg6vg2d5vw4wd9x")))

(define-public crate-solomka-sdk-macro-1.14.12 (c (n "solomka-sdk-macro") (v "1.14.12") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "09nvpdypfjnjhix7xqhf0bzs5sp927yd74l8iiqzvfqc51l74v75")))

