(define-module (crates-io so lo solo-bsc) #:use-module (crates-io))

(define-public crate-solo-bsc-0.0.3 (c (n "solo-bsc") (v "0.0.3") (d (list (d (n "cortex-m") (r "^0.5.8") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.7") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.2") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "nb") (r "^0.1.1") (d #t) (k 2)) (d (n "panic-halt") (r "^0.2.0") (d #t) (k 2)) (d (n "ssd1306") (r "^0.2.4") (d #t) (k 2)) (d (n "stm32l4xx-hal") (r "^0.3.6") (f (quote ("rt" "stm32l4x2"))) (k 0)))) (h "0fs9rlib9zivww04qsk9nncg57c9vd2pmqxigjq5rhdf3n1jn2d1")))

