(define-module (crates-io so lo soloud-sys) #:use-module (crates-io))

(define-public crate-soloud-sys-0.1.0 (c (n "soloud-sys") (v "0.1.0") (d (list (d (n "cmake") (r "^0.1.44") (d #t) (k 1)) (d (n "libc") (r "^0.2.74") (d #t) (k 0)))) (h "1y1m5c6y2yibzwj1jqgrkxibhbjpvlm9fa7gblhf0jrl5alslkpv") (f (quote (("use-ninja")))) (y #t)))

(define-public crate-soloud-sys-0.1.1 (c (n "soloud-sys") (v "0.1.1") (d (list (d (n "cmake") (r "^0.1.44") (d #t) (k 1)) (d (n "libc") (r "^0.2.74") (d #t) (k 0)))) (h "0dxa5smvj8lbw13dh7xhlswxx4q2ldd47cnzrdxmwzd3cvapqags") (f (quote (("use-ninja")))) (y #t)))

(define-public crate-soloud-sys-0.1.2 (c (n "soloud-sys") (v "0.1.2") (d (list (d (n "cmake") (r "^0.1.44") (d #t) (k 1)) (d (n "libc") (r "^0.2.74") (d #t) (k 0)))) (h "19a7p2iima1n5r6lsc6sk1p453kgilzawwnm67yyfcmjml8lyiy2") (f (quote (("use-ninja")))) (y #t)))

(define-public crate-soloud-sys-0.1.3 (c (n "soloud-sys") (v "0.1.3") (d (list (d (n "cmake") (r "^0.1.44") (d #t) (k 1)) (d (n "libc") (r "^0.2.74") (d #t) (k 0)))) (h "176h7hi3nr1x93lkgjgykbh55xvr81v7xkrapcay3n6fk2wng2yn") (f (quote (("use-ninja"))))))

(define-public crate-soloud-sys-0.1.4 (c (n "soloud-sys") (v "0.1.4") (d (list (d (n "cmake") (r "^0.1.44") (d #t) (k 1)) (d (n "libc") (r "^0.2.74") (d #t) (k 0)))) (h "1dzw7363xgk0afylmb9xivnxfnmc3w2fpx57clvwkrnk8ynsk8iv") (f (quote (("use-ninja"))))))

(define-public crate-soloud-sys-0.1.5 (c (n "soloud-sys") (v "0.1.5") (d (list (d (n "cmake") (r "^0.1.44") (d #t) (k 1)) (d (n "libc") (r "^0.2.74") (d #t) (k 0)))) (h "1q5skcp2lh86q321m5w2qyqdqds3rpxckbjjwm3a2vgcr7idwcs5") (f (quote (("use-ninja"))))))

(define-public crate-soloud-sys-0.1.6 (c (n "soloud-sys") (v "0.1.6") (d (list (d (n "cmake") (r "^0.1.44") (d #t) (k 1)) (d (n "libc") (r "^0.2.74") (d #t) (k 0)))) (h "0wdd84cays5p6qicdbjwi5fcdnjsjr6q29m9yp98yjh7kg1h6235") (f (quote (("use-ninja"))))))

(define-public crate-soloud-sys-0.1.7 (c (n "soloud-sys") (v "0.1.7") (d (list (d (n "cmake") (r "^0.1.44") (d #t) (k 1)) (d (n "libc") (r "^0.2.74") (d #t) (k 0)))) (h "0fvxlpfnc0n29rvlgsv220fyzy7mr5k5g5yfamhj1p97zjmrg5gz") (f (quote (("use-ninja"))))))

(define-public crate-soloud-sys-0.1.8 (c (n "soloud-sys") (v "0.1.8") (d (list (d (n "cmake") (r "^0.1.44") (d #t) (k 1)) (d (n "libc") (r "^0.2.74") (d #t) (k 0)))) (h "1rv80527py14cqjgg5jr9rp1wr6n9214lf2w37f53xvc4lbza6zb") (f (quote (("use-ninja"))))))

(define-public crate-soloud-sys-0.1.9 (c (n "soloud-sys") (v "0.1.9") (d (list (d (n "cmake") (r "^0.1.44") (d #t) (k 1)) (d (n "libc") (r "^0.2.77") (d #t) (k 0)))) (h "1gz6q1fblpjwplrzvvh09crnvjgrp2phm6k91ixrsxy2n9m8x3bv") (f (quote (("use-ninja"))))))

(define-public crate-soloud-sys-0.2.0 (c (n "soloud-sys") (v "0.2.0") (d (list (d (n "cmake") (r "^0.1.44") (d #t) (k 1)) (d (n "libc") (r "^0.2.77") (d #t) (k 0)))) (h "1r3bcz57g6kijnwn2s5yx4myq35ax2kx71nk1r2nin8yc8lnasw0") (f (quote (("use-ninja"))))))

(define-public crate-soloud-sys-0.2.1 (c (n "soloud-sys") (v "0.2.1") (d (list (d (n "cmake") (r "^0.1.44") (d #t) (k 1)) (d (n "libc") (r "^0.2.77") (d #t) (k 0)))) (h "01sibqg6vajlwnqikz6q28gaq8w9gjhbckqyi2iwzw0jm1l8mrq2") (f (quote (("use-ninja"))))))

(define-public crate-soloud-sys-0.2.2 (c (n "soloud-sys") (v "0.2.2") (d (list (d (n "cmake") (r ">=0.1.45, <0.2.0") (d #t) (k 1)) (d (n "libc") (r ">=0.2.77, <0.3.0") (d #t) (k 0)))) (h "05qj1a4ji7ixy8rxrfyvyqgnj12ywdj0jws6b8h4799kx4jywmzq") (f (quote (("use-ninja"))))))

(define-public crate-soloud-sys-0.2.3 (c (n "soloud-sys") (v "0.2.3") (d (list (d (n "cmake") (r "^0.1.45") (d #t) (k 1)) (d (n "libc") (r "^0.2.77") (d #t) (k 0)))) (h "1zl4m8lq49ihk6miqc5nqzikxwdbpms83ghx0kl4x4fdzmf2fqgd") (f (quote (("use-ninja"))))))

(define-public crate-soloud-sys-0.2.4 (c (n "soloud-sys") (v "0.2.4") (d (list (d (n "cmake") (r "^0.1.45") (d #t) (k 1)) (d (n "libc") (r "^0.2.81") (d #t) (k 0)))) (h "0g8zdsq5wc2gci54yk95yi9i8npwp3h8bskmsr4khcsymjfcbd85") (f (quote (("use-ninja"))))))

(define-public crate-soloud-sys-0.3.0 (c (n "soloud-sys") (v "0.3.0") (d (list (d (n "cmake") (r "^0.1.45") (d #t) (k 1)) (d (n "libc") (r "^0.2.82") (d #t) (k 0)))) (h "1apba9a23j8qjdry8zfndr5acvsh9qccrwhdm9nzb1hzlgazl323") (f (quote (("xaudio2") ("winmm") ("wasapi") ("use-ninja") ("sdl2-static") ("sdl2") ("portaudio") ("oss") ("opensles") ("openal") ("null") ("nosound") ("miniaudio") ("jack") ("default" "miniaudio") ("coreaudio") ("alsa")))) (y #t)))

(define-public crate-soloud-sys-0.3.1 (c (n "soloud-sys") (v "0.3.1") (d (list (d (n "cmake") (r "^0.1.45") (d #t) (k 1)) (d (n "libc") (r "^0.2.82") (d #t) (k 0)))) (h "1nwwpjqmayvvis57w63ml2lss29s3m1qc5c2syaxddrnjym09w0p") (f (quote (("xaudio2") ("winmm") ("wasapi") ("use-ninja") ("sdl2-static") ("sdl2") ("portaudio") ("oss") ("opensles") ("openal") ("null") ("nosound") ("miniaudio") ("jack") ("default" "miniaudio") ("coreaudio") ("alsa"))))))

(define-public crate-soloud-sys-0.3.2 (c (n "soloud-sys") (v "0.3.2") (d (list (d (n "cmake") (r "^0.1.45") (d #t) (k 1)) (d (n "libc") (r "^0.2.82") (d #t) (k 0)))) (h "1bj574xhz58vdi08n3f6i4czyv6yjc5p9rpqbkg36kj6fhd2m09c") (f (quote (("xaudio2") ("winmm") ("wasapi") ("use-ninja") ("sdl2-static") ("sdl2") ("portaudio") ("oss") ("opensles") ("openal") ("null") ("nosound") ("miniaudio") ("jack") ("coreaudio") ("alsa"))))))

(define-public crate-soloud-sys-0.3.4 (c (n "soloud-sys") (v "0.3.4") (d (list (d (n "cmake") (r "^0.1.45") (d #t) (k 1)) (d (n "libc") (r "^0.2.82") (d #t) (k 0)))) (h "12gcjvkww0gin0bni4jr5b0v92077asam7qr8nhk3kig1l1vwd81") (f (quote (("xaudio2") ("winmm") ("wasapi") ("use-ninja") ("sdl2-static") ("sdl2") ("portaudio") ("oss") ("opensles") ("openal") ("null") ("nosound") ("miniaudio") ("jack") ("coreaudio") ("alsa"))))))

(define-public crate-soloud-sys-0.4.0 (c (n "soloud-sys") (v "0.4.0") (d (list (d (n "cmake") (r "^0.1.45") (d #t) (k 1)) (d (n "libc") (r "^0.2.94") (d #t) (k 0)))) (h "1qfdrjcvln7p5sglh3yfzwaly8523jl5j64v9945f9ms88zwi9hz") (f (quote (("xaudio2") ("winmm") ("wasapi") ("use-ninja") ("sdl2-static") ("sdl2") ("portaudio") ("oss") ("opensles") ("openal") ("null") ("nosound") ("miniaudio") ("jack") ("coreaudio") ("alsa"))))))

(define-public crate-soloud-sys-0.4.4 (c (n "soloud-sys") (v "0.4.4") (d (list (d (n "cmake") (r "^0.1.45") (d #t) (k 1)))) (h "1s0npz7m9zsg0pw0yyp5cjmd8h5mn524k6ygg7r0qgwgwlgfdva4") (f (quote (("xaudio2") ("winmm") ("wasapi") ("use-ninja") ("sdl2-static") ("sdl2") ("portaudio") ("oss") ("opensles") ("openal") ("null") ("nosound") ("miniaudio") ("jack") ("coreaudio") ("alsa"))))))

(define-public crate-soloud-sys-1.0.0 (c (n "soloud-sys") (v "1.0.0") (d (list (d (n "cmake") (r "^0.1.45") (d #t) (k 1)))) (h "107l5wzaaqr58q95f8ysj8jwplml3drd7h2mp6w52hriaxd2c8lf") (f (quote (("xaudio2") ("winmm") ("wasapi") ("use-ninja") ("sdl2-static") ("sdl2") ("portaudio") ("oss") ("opensles") ("openal") ("null") ("nosound") ("miniaudio") ("jack") ("coreaudio") ("alsa"))))))

(define-public crate-soloud-sys-1.0.1 (c (n "soloud-sys") (v "1.0.1") (d (list (d (n "cmake") (r "^0.1.45") (d #t) (k 1)))) (h "060dq70bla9hf13wmf6lzysm4myq2jxr4hk57q3wiq26ms7pdcn3") (f (quote (("xaudio2") ("winmm") ("wasapi") ("use-ninja") ("sdl2-static") ("sdl2") ("portaudio") ("oss") ("opensles") ("openal") ("null") ("nosound") ("miniaudio") ("jack") ("coreaudio") ("alsa"))))))

(define-public crate-soloud-sys-1.0.2 (c (n "soloud-sys") (v "1.0.2") (d (list (d (n "cmake") (r "^0.1.45") (d #t) (k 1)))) (h "09rp6c4asnjvnl6fd5ivzi26r7zs1p2f9s689fvp2l717hpqcyjh") (f (quote (("xaudio2") ("winmm") ("wasapi") ("use-ninja") ("sdl2-static") ("sdl2") ("portaudio") ("oss") ("opensles") ("openal") ("null") ("nosound") ("miniaudio") ("jack") ("coreaudio") ("alsa")))) (l "soloud")))

(define-public crate-soloud-sys-1.0.3 (c (n "soloud-sys") (v "1.0.3") (d (list (d (n "cmake") (r "^0.1.45") (d #t) (k 1)))) (h "1wbl36dhim6fwc3i51v8hk9bm6kpa63i93cymwc8xiyzj9ry1qf5") (f (quote (("xaudio2") ("winmm") ("wasapi") ("use-ninja") ("sdl2-static") ("sdl2") ("portaudio") ("oss") ("opensles") ("openal") ("null") ("nosound") ("miniaudio") ("jack") ("coreaudio") ("alsa")))) (l "soloud")))

(define-public crate-soloud-sys-1.0.4 (c (n "soloud-sys") (v "1.0.4") (d (list (d (n "cmake") (r "^0.1.45") (d #t) (k 1)))) (h "1qdmiq442xg9fn4wda0c26hnwmcp8f59s194rlmnal538j3dx12h") (f (quote (("xaudio2") ("winmm") ("wasapi") ("use-ninja") ("sdl2-static") ("sdl2") ("portaudio") ("oss") ("opensles") ("openal") ("null") ("nosound") ("miniaudio") ("jack") ("coreaudio") ("alsa")))) (l "soloud")))

(define-public crate-soloud-sys-1.0.5 (c (n "soloud-sys") (v "1.0.5") (d (list (d (n "cmake") (r "^0.1.45") (d #t) (k 1)))) (h "0hbv3x3613mc578p6bd8kad7lv8jwaypbs8kg9zqks0apj1zqbzh") (f (quote (("xaudio2") ("winmm") ("wasapi") ("use-ninja") ("sdl2-static") ("sdl2") ("portaudio") ("oss") ("opensles") ("openal") ("null") ("nosound") ("miniaudio") ("jack") ("coreaudio") ("alsa")))) (l "soloud")))

