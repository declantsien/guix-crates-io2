(define-module (crates-io so lo solo-bsp) #:use-module (crates-io))

(define-public crate-solo-bsp-0.0.0 (c (n "solo-bsp") (v "0.0.0") (d (list (d (n "cortex-m") (r "^0.5.8") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.7") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.2") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "nb") (r "^0.1.1") (d #t) (k 2)) (d (n "panic-halt") (r "^0.2.0") (d #t) (k 2)) (d (n "ssd1306") (r "^0.2.2") (d #t) (k 2)) (d (n "stm32l4xx-hal") (r "^0.3.4") (f (quote ("rt" "stm32l4x2"))) (k 0)))) (h "1dd58pps2iwq72wwvafvfh1y75cf76zalk72ab05ahg54ssh0bfs") (y #t)))

(define-public crate-solo-bsp-0.0.1 (c (n "solo-bsp") (v "0.0.1") (d (list (d (n "cortex-m") (r "^0.5.8") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.7") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.2") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "nb") (r "^0.1.1") (d #t) (k 2)) (d (n "panic-halt") (r "^0.2.0") (d #t) (k 2)) (d (n "ssd1306") (r "^0.2.2") (d #t) (k 2)) (d (n "stm32l4xx-hal") (r "^0.3.4") (f (quote ("rt" "stm32l4x2"))) (k 0)))) (h "0jn974h8gav17nrji3h332g5d444wnl1i90qsjd66mc94yx29cp9") (y #t)))

(define-public crate-solo-bsp-0.0.2 (c (n "solo-bsp") (v "0.0.2") (d (list (d (n "cortex-m") (r "^0.5.8") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.7") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.2") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "nb") (r "^0.1.1") (d #t) (k 2)) (d (n "panic-halt") (r "^0.2.0") (d #t) (k 2)) (d (n "ssd1306") (r "^0.2.2") (d #t) (k 2)) (d (n "stm32l4xx-hal") (r "^0.3.4") (f (quote ("rt" "stm32l4x2"))) (k 0)))) (h "1w6qd3b1x5qmygl71i1y5yq1rixlkfxfk566hjqsjwmcpfy2db3w") (y #t)))

(define-public crate-solo-bsp-0.0.3 (c (n "solo-bsp") (v "0.0.3") (h "1g2nj2hbx0rfbd95h9vbb6d7v9pgyy6wky81a68hr7y1r5s8s3hr")))

