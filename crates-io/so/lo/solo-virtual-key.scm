(define-module (crates-io so lo solo-virtual-key) #:use-module (crates-io))

(define-public crate-solo-virtual-key-0.1.0 (c (n "solo-virtual-key") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("process" "io-util" "rt"))) (d #t) (k 0)))) (h "0cim25kq2cn9i6zswpf4kl4dwaxcl1vncgsa15jwq0zrvyhixpnk")))

