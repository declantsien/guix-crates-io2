(define-module (crates-io so lo solo5-rs-macros) #:use-module (crates-io))

(define-public crate-solo5-rs-macros-0.1.0 (c (n "solo5-rs-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("full"))) (d #t) (k 0)))) (h "1p638i0xwl4f4irr14n29pi48h768px3ky3fk7n7rzd81spmxdfy") (f (quote (("alloc"))))))

