(define-module (crates-io so da sodalite) #:use-module (crates-io))

(define-public crate-sodalite-0.1.0 (c (n "sodalite") (v "0.1.0") (h "14j22fd58vzfzpa17nhkar4ya01kqdfdziiswcxkdmy1m83g1mzv")))

(define-public crate-sodalite-0.1.1 (c (n "sodalite") (v "0.1.1") (d (list (d (n "rand") (r "0.*") (d #t) (k 0)) (d (n "tweetnacl") (r "0.*") (d #t) (k 2)))) (h "0hpxyii3m9zjbb5if9r5lvrkms0p893pqh0iqrgzjx42wcyja1zn")))

(define-public crate-sodalite-0.1.2 (c (n "sodalite") (v "0.1.2") (d (list (d (n "rand") (r "0.*") (d #t) (k 0)) (d (n "tweetnacl") (r "0.*") (d #t) (k 2)))) (h "1ghl2wxrsm9ydygmwyx9521nivgxvg6y145ihw9id5wkw9pxds32")))

(define-public crate-sodalite-0.1.3 (c (n "sodalite") (v "0.1.3") (d (list (d (n "rand") (r "0.*") (d #t) (k 0)) (d (n "tweetnacl") (r "0.*") (d #t) (k 2)))) (h "1v0sckqs6yxq8rbifbc1j9j173fi4x524qi2x5s1l5d93bfv8mad")))

(define-public crate-sodalite-0.1.4 (c (n "sodalite") (v "0.1.4") (d (list (d (n "rand") (r "0.*") (d #t) (k 0)) (d (n "tweetnacl") (r "0.*") (d #t) (k 2)))) (h "1ry0w1hvzhdbwipdv07q6dfc6v8vcnrzk7wv8m0vwjda1nsb4baw")))

(define-public crate-sodalite-0.1.5 (c (n "sodalite") (v "0.1.5") (d (list (d (n "rand") (r "0.*") (d #t) (k 0)) (d (n "tweetnacl") (r "0.*") (d #t) (k 2)))) (h "1y21r00cyf1bcvnl6gs1c98qiyckrfng1ikv1lfkv2rplg7caks4")))

(define-public crate-sodalite-0.2.0 (c (n "sodalite") (v "0.2.0") (d (list (d (n "rand") (r "0.*") (d #t) (k 0)) (d (n "tweetnacl") (r "0.*") (d #t) (k 2)))) (h "0zk9mflfk6ijzr4sl2jxanf6dzi08wbpa71j4bybqpm18qdmqhbs")))

(define-public crate-sodalite-0.2.1 (c (n "sodalite") (v "0.2.1") (d (list (d (n "rand") (r "0.*") (d #t) (k 0)) (d (n "tweetnacl") (r "0.*") (d #t) (k 2)))) (h "1084nng04k33r46zrcp1iv2s8djrb3pm2akkplj191hmb861j0p7")))

(define-public crate-sodalite-0.2.2 (c (n "sodalite") (v "0.2.2") (d (list (d (n "index-fixed") (r "0.0.*") (d #t) (k 0)) (d (n "rand") (r "0.*") (d #t) (k 0)) (d (n "tweetnacl") (r "0.*") (d #t) (k 2)))) (h "07sb4k95pwy68620pv5nma69gcrz95a8cs2zyv0fjicaiwksnvkp")))

(define-public crate-sodalite-0.1.6 (c (n "sodalite") (v "0.1.6") (d (list (d (n "rand") (r "0.*") (d #t) (k 0)) (d (n "tweetnacl") (r "0.*") (d #t) (k 2)))) (h "0a3w49xzr9j9z3kf1gf1rc4drvc3rayzn59j0q72y1rha4rds1mi")))

(define-public crate-sodalite-0.2.3 (c (n "sodalite") (v "0.2.3") (d (list (d (n "index-fixed") (r "0.0.*") (d #t) (k 0)) (d (n "rand") (r "0.*") (d #t) (k 0)) (d (n "tweetnacl") (r "0.*") (d #t) (k 2)))) (h "0mabkr4gfr4qcqcm6jwgdp8s2l929i0j79d7scdhc12qjflcwnba")))

(define-public crate-sodalite-0.2.4 (c (n "sodalite") (v "0.2.4") (d (list (d (n "index-fixed") (r "0.0.*") (d #t) (k 0)) (d (n "rand") (r "0.*") (o #t) (d #t) (k 0)) (d (n "rand") (r "0.*") (d #t) (k 2)) (d (n "tweetnacl") (r "0.*") (d #t) (k 2)))) (h "06agnxr0vfgckhyi77arzmhwydp801wkl383jcglhx6qz3c5vygg")))

(define-public crate-sodalite-0.2.5 (c (n "sodalite") (v "0.2.5") (d (list (d (n "index-fixed") (r "0.0.*") (d #t) (k 0)) (d (n "rand") (r "0.*") (o #t) (d #t) (k 0)) (d (n "rand") (r "0.*") (d #t) (k 2)) (d (n "tweetnacl") (r "0.*") (d #t) (k 2)))) (h "1la00vszgmc2ssdwjjr5ig8z5ysvz1iw4skzf19f21zammnj456j") (f (quote (("docs" "rand"))))))

(define-public crate-sodalite-0.2.6 (c (n "sodalite") (v "0.2.6") (d (list (d (n "index-fixed") (r "0.0.*") (d #t) (k 0)) (d (n "rand") (r "0.*") (o #t) (d #t) (k 0)) (d (n "rand") (r "0.*") (d #t) (k 2)) (d (n "tweetnacl") (r "0.*") (d #t) (k 2)))) (h "1470m17hd7lj9hgqj1hscvw7q9a03n8jwrkyahdx8idi7mbwmxkp") (f (quote (("docs" "rand"))))))

(define-public crate-sodalite-0.3.0 (c (n "sodalite") (v "0.3.0") (d (list (d (n "index-fixed") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "tweetnacl") (r "^0.2") (d #t) (k 2)))) (h "0776wgh9hd53hrfzgs749fcaaz623s89f7av4yn7naa0qy230r37") (f (quote (("docs" "rand") ("bench" "rand"))))))

(define-public crate-sodalite-0.4.0 (c (n "sodalite") (v "0.4.0") (d (list (d (n "index-fixed") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "tweetnacl") (r "^0.4") (d #t) (k 2)))) (h "1q40qarzshmal3d9n332s111lyl41brvgk4c56x8pi8mklslly21") (f (quote (("docs" "rand") ("bench" "rand"))))))

