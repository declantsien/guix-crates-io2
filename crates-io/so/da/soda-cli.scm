(define-module (crates-io so da soda-cli) #:use-module (crates-io))

(define-public crate-soda-cli-0.0.1 (c (n "soda-cli") (v "0.0.1") (d (list (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.93") (d #t) (k 0)) (d (n "soda_sol") (r "^0.0.1") (d #t) (k 0)))) (h "18amkdnz6s4am9dij1j1z6s7c0z1z5bvb1gy22ah0xd8kgbqzxjv")))

(define-public crate-soda-cli-0.0.2 (c (n "soda-cli") (v "0.0.2") (d (list (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.93") (d #t) (k 0)) (d (n "soda_sol") (r "^0.0.3") (d #t) (k 0)))) (h "0bqcl8z11c0d26j6rcc2hivgq92vwcl9xwaz440a3wnh3kcjbfn7")))

(define-public crate-soda-cli-0.0.3 (c (n "soda-cli") (v "0.0.3") (d (list (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.93") (d #t) (k 0)) (d (n "soda_sol") (r "^0.0.3") (d #t) (k 0)))) (h "1if71q8bx43lnb5d4av5fvanz0dv9giddi9prfqwxali753rm402")))

(define-public crate-soda-cli-0.0.4 (c (n "soda-cli") (v "0.0.4") (d (list (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.93") (d #t) (k 0)) (d (n "soda_sol") (r "^0.0.4") (d #t) (k 0)))) (h "1lz63h3in5qca89s1h2glh0qh3r58r29n5kwcv605y4w9z7hwl8s")))

(define-public crate-soda-cli-0.0.5 (c (n "soda-cli") (v "0.0.5") (d (list (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.93") (d #t) (k 0)) (d (n "soda_sol") (r "^0.0.5") (d #t) (k 0)))) (h "036x2vxq3q2ih8b3d23hkdc20hs16lffp986y6kwrvrzm2i8j4rc")))

(define-public crate-soda-cli-0.1.0 (c (n "soda-cli") (v "0.1.0") (d (list (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.93") (d #t) (k 0)) (d (n "soda_sol") (r "^0.0.5") (d #t) (k 0)))) (h "110wvalvzvfxxg7lldibrl24zdwk8kdjb9rpa5pk7p4xmh2f1hzr")))

(define-public crate-soda-cli-0.1.1 (c (n "soda-cli") (v "0.1.1") (d (list (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.93") (d #t) (k 0)) (d (n "soda_sol") (r "^0.0.6") (d #t) (k 0)))) (h "14a5q5rb2liqpdhhvzs744pvidk86673gz2dfqg69wlf23xh0q5k")))

