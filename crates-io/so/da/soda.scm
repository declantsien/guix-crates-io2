(define-module (crates-io so da soda) #:use-module (crates-io))

(define-public crate-soda-0.1.0 (c (n "soda") (v "0.1.0") (h "1cfhkcas139c2wksk30jx8sfh949kc6z6il2xi89g2zfjm7irk6w")))

(define-public crate-soda-0.1.1 (c (n "soda") (v "0.1.1") (d (list (d (n "cursive") (r "^0.7") (f (quote ("termion-backend"))) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "0bs2ws092ak14pd1y0majqra1x6jgwhs6bzcag9aswn4y1nhcp2m")))

