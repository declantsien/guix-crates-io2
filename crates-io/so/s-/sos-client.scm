(define-module (crates-io so s- sos-client) #:use-module (crates-io))

(define-public crate-sos-client-0.1.1 (c (n "sos-client") (v "0.1.1") (d (list (d (n "bincode") (r "^1.3") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1m0p21l011z8h8jad0aq8agm2v0gyqv5k3452yqjh4h65sgch3z4") (f (quote (("default" "cache") ("cache" "bincode" "serde"))))))

(define-public crate-sos-client-0.1.2 (c (n "sos-client") (v "0.1.2") (d (list (d (n "bincode") (r "^1.3") (o #t) (d #t) (k 0)) (d (n "glob") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "itertools") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0c9whn70nk9x44sb3w8rh65j4q6jhz8msg730llm738rm8ccdy04") (f (quote (("look-ahead" "glob" "itertools") ("default" "cache") ("cache" "bincode" "serde"))))))

(define-public crate-sos-client-0.1.3 (c (n "sos-client") (v "0.1.3") (d (list (d (n "bincode") (r "^1.3") (o #t) (d #t) (k 0)) (d (n "glob") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "itertools") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0xh6j98pwm6937wblp55qa75ci78y4lymwl02k9cs2hi7cbywvil") (f (quote (("look-ahead" "glob" "itertools") ("default" "cache") ("cache" "bincode" "serde"))))))

