(define-module (crates-io so lf solfmt) #:use-module (crates-io))

(define-public crate-solfmt-0.1.0 (c (n "solfmt") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)))) (h "0874n1i7r26rx7fj06flw3wsg48dw7x5jh72mdbm6wbmbii7jxka")))

(define-public crate-solfmt-0.2.0 (c (n "solfmt") (v "0.2.0") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)))) (h "1mgy9g9nsxmviadd29cvac9m6misa4y1wikzvhblwj9gklbk7gxa")))

(define-public crate-solfmt-0.2.1 (c (n "solfmt") (v "0.2.1") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)))) (h "0rh84sg16agnlha04rgh5ia52a00r2i0lag8fdz3yqh44lp8zj8p")))

(define-public crate-solfmt-0.3.0 (c (n "solfmt") (v "0.3.0") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)))) (h "0swjfrd9lvv0j9bjfh13zbvcm3a3555qwgqnbv1lhzqpa8jlqwda")))

(define-public crate-solfmt-0.3.1 (c (n "solfmt") (v "0.3.1") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)))) (h "1lqyc6c8m8ak0f6lyrza2xwypldixx0lmdqb5z7ijqfic38k3dx0")))

