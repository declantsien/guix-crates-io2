(define-module (crates-io so lf solf) #:use-module (crates-io))

(define-public crate-Solf-0.1.0 (c (n "Solf") (v "0.1.0") (h "1qa6srinywzk84i38zm3c4gqa57acz6nvz1b1d7kk0r9a8cm63f5")))

(define-public crate-Solf-0.0.0 (c (n "Solf") (v "0.0.0") (h "0kjfk8kj7j4lr1f8r5ng9kfvyvfja41757kzqlc3d2h6d1d0a2pv")))

