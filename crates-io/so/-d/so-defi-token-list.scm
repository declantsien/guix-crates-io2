(define-module (crates-io so -d so-defi-token-list) #:use-module (crates-io))

(define-public crate-so-defi-token-list-0.1.0 (c (n "so-defi-token-list") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "tokio") (r "^1.8.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 0)))) (h "01qki002by3s29shaswhg1cslsb8xvpyb9iz66dsz80x2vm75vb8")))

(define-public crate-so-defi-token-list-0.1.1 (c (n "so-defi-token-list") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "tokio") (r "^1.8.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 0)))) (h "1y7z2lc64bs8a6m4w4fj5agiq0q9b3fb3gcd8nsz6hjymdd7g64l")))

