(define-module (crates-io so -d so-defi-utils) #:use-module (crates-io))

(define-public crate-so-defi-utils-0.1.0 (c (n "so-defi-utils") (v "0.1.0") (d (list (d (n "bytemuck") (r "^1.7.2") (d #t) (k 0)) (d (n "solana-program") (r "^1.9") (d #t) (k 0)))) (h "0ak4byjf1jf2wlh1agccqpbzr3i5ww58f1b18j6ppaa1v8wc7gxz")))

(define-public crate-so-defi-utils-0.1.1 (c (n "so-defi-utils") (v "0.1.1") (d (list (d (n "bytemuck") (r "^1.7.2") (d #t) (k 0)) (d (n "solana-client") (r "^1.9") (d #t) (k 2)) (d (n "solana-program") (r "^1.9") (d #t) (k 0)))) (h "0cc7fdhqfyd2q7hspk03qvghg3rl5kyjnkaa15b724am920grccx")))

(define-public crate-so-defi-utils-0.1.2 (c (n "so-defi-utils") (v "0.1.2") (d (list (d (n "bytemuck") (r "^1.7.2") (d #t) (k 0)) (d (n "solana-client") (r "^1.9") (d #t) (k 2)) (d (n "solana-program") (r "^1.9") (d #t) (k 0)))) (h "00d75qqq10978bq8gmn8iqcgsq83kmapd9zfcqavzqs42n469bfk") (f (quote (("experimental") ("default" "experimental"))))))

(define-public crate-so-defi-utils-0.1.3 (c (n "so-defi-utils") (v "0.1.3") (d (list (d (n "bytemuck") (r "^1.7.2") (d #t) (k 0)) (d (n "solana-client") (r ">=1.7") (d #t) (k 2)) (d (n "solana-program") (r ">=1.7") (d #t) (k 0)))) (h "0bd8zrpf68rff0xig0gsfgk0bgr8r6y2mh6m1hg5za244i9nyyvs") (f (quote (("experimental") ("default" "experimental"))))))

(define-public crate-so-defi-utils-0.1.4 (c (n "so-defi-utils") (v "0.1.4") (d (list (d (n "bytemuck") (r "^1.7.2") (d #t) (k 0)) (d (n "solana-client") (r ">=1.7") (d #t) (k 2)) (d (n "solana-program") (r ">=1.7") (d #t) (k 0)))) (h "0310ypq6r9dqh58plp8s4r3sn2jkcmpwa2crp6ry1rzlv16205cj") (f (quote (("experimental") ("default" "experimental"))))))

(define-public crate-so-defi-utils-0.1.5 (c (n "so-defi-utils") (v "0.1.5") (d (list (d (n "bytemuck") (r "^1.7.2") (d #t) (k 0)) (d (n "solana-client") (r ">=1.7") (d #t) (k 2)) (d (n "solana-program") (r ">=1.7") (d #t) (k 0)))) (h "1nrgr7l6rc71xmfdmsncigr2z3rzkghl8bs9pl16n5j494qsyk1g") (f (quote (("experimental") ("default" "experimental"))))))

(define-public crate-so-defi-utils-0.1.6 (c (n "so-defi-utils") (v "0.1.6") (d (list (d (n "bytemuck") (r "^1.7.2") (d #t) (k 0)) (d (n "solana-client") (r ">=1.6") (d #t) (k 2)) (d (n "solana-program") (r ">=1.6") (d #t) (k 0)))) (h "1mqc6m3g08fl581wybm548mc3ak8nzdvxz9vk26l4pzk15hqx08n") (f (quote (("experimental") ("default" "experimental"))))))

