(define-module (crates-io so -d so-defi-accounts) #:use-module (crates-io))

(define-public crate-so-defi-accounts-0.1.0 (c (n "so-defi-accounts") (v "0.1.0") (d (list (d (n "borsh") (r "^0.9.1") (d #t) (k 0)) (d (n "borsh-derive") (r "^0.9.1") (d #t) (k 0)) (d (n "solana-client") (r "^1.9") (d #t) (k 2)) (d (n "solana-program") (r "^1.9") (d #t) (k 0)) (d (n "static-pubkey") (r "^1.0.2") (d #t) (k 0)))) (h "0w2bry864dbqm9hffippayc70lcdww2p0n1xj1rwynn4gknq4hhk")))

(define-public crate-so-defi-accounts-0.1.4 (c (n "so-defi-accounts") (v "0.1.4") (d (list (d (n "borsh") (r "^0.9.1") (d #t) (k 0)) (d (n "borsh-derive") (r "^0.9.1") (d #t) (k 0)) (d (n "solana-client") (r "^1.9") (d #t) (k 2)) (d (n "solana-program") (r "^1.9") (d #t) (k 0)) (d (n "static-pubkey") (r "^1.0.2") (d #t) (k 0)))) (h "0d6if880fk23v1as4gv8bc7krpzhaz39lkccs5776rkgh0r18ixz")))

