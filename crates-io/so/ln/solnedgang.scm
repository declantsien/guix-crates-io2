(define-module (crates-io so ln solnedgang) #:use-module (crates-io))

(define-public crate-solnedgang-0.0.1 (c (n "solnedgang") (v "0.0.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "0mci70jwqjijl7fj41m78l0091mikkv59dncz72iailwmzmv5vak")))

(define-public crate-solnedgang-0.0.2 (c (n "solnedgang") (v "0.0.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "1k5161y9zxvm8zpk0a2v02g8lvf1cfm8mp23czjxmgmlr2rmyb1x")))

(define-public crate-solnedgang-0.0.3 (c (n "solnedgang") (v "0.0.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "1y6w7gibb4kx41bz3415vffa73pc212p026brrawkh0gskz3vxqc")))

(define-public crate-solnedgang-0.0.4 (c (n "solnedgang") (v "0.0.4") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "11r9r4xlg4xa2f7ra7dwwvlyyv8gvpmksz7s3mdzavh3dx4md3qa")))

