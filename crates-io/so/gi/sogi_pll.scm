(define-module (crates-io so gi sogi_pll) #:use-module (crates-io))

(define-public crate-sogi_pll-0.1.0 (c (n "sogi_pll") (v "0.1.0") (d (list (d (n "pid") (r "^4.0.0") (d #t) (k 0)))) (h "11ih8akfrplr5395dzzw50n7rqhvlh90dapi24pq5szr4caz473j")))

(define-public crate-sogi_pll-0.2.0 (c (n "sogi_pll") (v "0.2.0") (d (list (d (n "micromath") (r "^2.1.0") (d #t) (k 0)))) (h "0qnbjr4k9x5d72hifmwgf97v0grm54vg3fyfnglgdvwxlckqgzii")))

(define-public crate-sogi_pll-0.2.1 (c (n "sogi_pll") (v "0.2.1") (d (list (d (n "micromath") (r "^2.1.0") (d #t) (k 0)) (d (n "plotters") (r "^0.3.5") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1p3ab58hgy7l45x6zjppkjfnsa0sa0idi33zsd3kiz3wwrvcz1li")))

