(define-module (crates-io so ls solstack) #:use-module (crates-io))

(define-public crate-solstack-0.1.0 (c (n "solstack") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1miimc0gnnffmdnvmyjsdyfpm3g7gsypgbq6idk7xvz3if9s5zz6")))

(define-public crate-solstack-0.2.0 (c (n "solstack") (v "0.2.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "16gbpqigwlrvd5knyb0j8lk3gr70qccmcc30r7vzfg05b5mvs4s1")))

(define-public crate-solstack-0.3.0 (c (n "solstack") (v "0.3.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0q4bygz11zdh3dl84kavfcxj6xi30nr015h814vni8igwxvkcjhg") (r "1.56.1")))

(define-public crate-solstack-0.3.1 (c (n "solstack") (v "0.3.1") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1va43cldmhiljxi14s6xbvi28i5ynv58q8ngz2lzq275vfwdmvgb") (r "1.56.1")))

(define-public crate-solstack-0.3.2 (c (n "solstack") (v "0.3.2") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "09y1105vlwa2g261rpxhp0qcgac27hw137zkl0cq4p2fr8jiwpq5") (r "1.56.1")))

