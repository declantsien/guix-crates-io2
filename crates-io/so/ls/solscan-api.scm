(define-module (crates-io so ls solscan-api) #:use-module (crates-io))

(define-public crate-solscan-api-0.1.0 (c (n "solscan-api") (v "0.1.0") (d (list (d (n "assert-json-diff") (r "^2.0.2") (d #t) (k 2)) (d (n "httpmock") (r "^0.6") (d #t) (k 2)) (d (n "json") (r "^0.12.4") (d #t) (k 2)) (d (n "reqwest") (r "^0.11.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.139") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)) (d (n "tokio") (r "^1.18.2") (f (quote ("full"))) (d #t) (k 2)))) (h "1w7zl9xchy9mc883cbig1hnc6fgs86dh4wlmhcn1avdhp8pp9a2h")))

(define-public crate-solscan-api-0.1.1 (c (n "solscan-api") (v "0.1.1") (d (list (d (n "assert-json-diff") (r "^2.0.2") (d #t) (k 2)) (d (n "httpmock") (r "^0.6") (d #t) (k 2)) (d (n "json") (r "^0.12.4") (d #t) (k 2)) (d (n "reqwest") (r "^0.11.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.139") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)) (d (n "tokio") (r "^1.18.2") (f (quote ("full"))) (d #t) (k 2)))) (h "0xbqnpq6gis0ic2z3svjpyq3427j1cf0h2rq5ffawxslb6fhprgh")))

(define-public crate-solscan-api-0.1.2 (c (n "solscan-api") (v "0.1.2") (d (list (d (n "assert-json-diff") (r "^2.0.2") (d #t) (k 2)) (d (n "httpmock") (r "^0.6") (d #t) (k 2)) (d (n "json") (r "^0.12.4") (d #t) (k 2)) (d (n "reqwest") (r "^0.11.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.139") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)) (d (n "tokio") (r "^1.18.2") (f (quote ("full"))) (d #t) (k 2)))) (h "19qbqghyjfpmwpc28wws1gb8nl2qscipqdr3hwjygwh6vbc1baxd")))

