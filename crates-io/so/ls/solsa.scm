(define-module (crates-io so ls solsa) #:use-module (crates-io))

(define-public crate-solsa-0.1.0 (c (n "solsa") (v "0.1.0") (d (list (d (n "clap") (r "^2.31") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tera") (r "^0.11") (d #t) (k 0)))) (h "163vwi21fqrng1p9mf5z24l7dnbxdg9c9g2wzhd1b991nh8mlxgs")))

(define-public crate-solsa-0.1.1 (c (n "solsa") (v "0.1.1") (d (list (d (n "clap") (r "^2.31") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tera") (r "^0.11") (d #t) (k 0)))) (h "01hb29ck2z2kpbnl8fjprllsj3097maglsl9mjrlqfdf94l365xl")))

(define-public crate-solsa-0.1.2 (c (n "solsa") (v "0.1.2") (d (list (d (n "clap") (r "^2.31") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tera") (r "^0.11") (d #t) (k 0)))) (h "1dsvrfsq5cipvjhn2ral3sjy1sphn82dnzv1i232z66053ddl583")))

(define-public crate-solsa-0.1.3 (c (n "solsa") (v "0.1.3") (d (list (d (n "clap") (r "^2.31") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tera") (r "^0.11") (d #t) (k 0)))) (h "0gmiji83fmyb2dpc8xilqfqc8y5hsb7dn6j10iya8lyvf621blxm")))

(define-public crate-solsa-0.1.4 (c (n "solsa") (v "0.1.4") (d (list (d (n "clap") (r "^2.31") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tera") (r "^0.11") (d #t) (k 0)))) (h "1sv3vbfm9i765rsgdpcn8zy2dskkrqds1z50rcd7z7w6z72hhb6h")))

(define-public crate-solsa-0.1.5 (c (n "solsa") (v "0.1.5") (d (list (d (n "clap") (r "^2.31") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tera") (r "^0.11") (d #t) (k 0)))) (h "0sz9qg36ff2gwb31y5zra3isadf6r1n4kf6myywgc5fb002r1x3b")))

(define-public crate-solsa-0.1.6 (c (n "solsa") (v "0.1.6") (d (list (d (n "clap") (r "^2.31") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tera") (r "^0.11") (d #t) (k 0)))) (h "1l14n1vppj7dilnl7q9jks5pmnjayhmmx6m42l31w2sfxh0h4k7p")))

(define-public crate-solsa-0.1.7 (c (n "solsa") (v "0.1.7") (d (list (d (n "clap") (r "^2.31") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tera") (r "^0.11") (d #t) (k 0)))) (h "1y2z8sl3klf8582c780cj89qqdkamf2w3yi36pvpcia02winyg5z")))

