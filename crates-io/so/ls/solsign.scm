(define-module (crates-io so ls solsign) #:use-module (crates-io))

(define-public crate-solsign-0.1.0 (c (n "solsign") (v "0.1.0") (d (list (d (n "base64") (r "=0.13.0") (d #t) (k 0)) (d (n "bs58") (r "=0.4.0") (d #t) (k 0)) (d (n "derivation-path") (r "=0.2.0") (d #t) (k 0)) (d (n "ed25519-dalek") (r "=1.0.1") (d #t) (k 0)) (d (n "ed25519-dalek-bip32") (r "=0.2.0") (d #t) (k 0)) (d (n "hmac") (r "=0.12.1") (d #t) (k 0)) (d (n "pbkdf2") (r "=0.11.0") (d #t) (k 0)) (d (n "rpassword") (r "=6.0.1") (d #t) (k 0)) (d (n "sha2") (r "=0.10.5") (d #t) (k 0)))) (h "0i40bj53h8rylr6qgfjd4702017wx16188v1wa9xw2n61bg9xgvk")))

(define-public crate-solsign-0.1.1 (c (n "solsign") (v "0.1.1") (d (list (d (n "base64") (r "=0.13.0") (d #t) (k 0)) (d (n "bs58") (r "=0.4.0") (d #t) (k 0)) (d (n "derivation-path") (r "=0.2.0") (d #t) (k 0)) (d (n "ed25519-dalek") (r "=1.0.1") (d #t) (k 0)) (d (n "ed25519-dalek-bip32") (r "=0.2.0") (d #t) (k 0)) (d (n "hmac") (r "=0.12.1") (d #t) (k 0)) (d (n "pbkdf2") (r "=0.11.0") (d #t) (k 0)) (d (n "rpassword") (r "=6.0.1") (d #t) (k 0)) (d (n "sha2") (r "=0.10.5") (d #t) (k 0)))) (h "15z417bvqm873vjnr9q13ddria0k50pd4k3z7hnq5ndqd1j7i309")))

