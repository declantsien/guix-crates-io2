(define-module (crates-io so ls solstice_glyph) #:use-module (crates-io))

(define-public crate-solstice_glyph-0.1.0 (c (n "solstice_glyph") (v "0.1.0") (d (list (d (n "bytemuck") (r "^1.4") (d #t) (k 0)) (d (n "env_logger") (r "^0.8") (d #t) (k 2)) (d (n "glutin") (r "^0.25") (d #t) (k 2)) (d (n "glyph_brush") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "solstice") (r "^0.1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1s9z0ldicmyp21cxyslyhsm0dwpvf1p4wnfnxis4q314vck67zw6")))

(define-public crate-solstice_glyph-0.1.1 (c (n "solstice_glyph") (v "0.1.1") (d (list (d (n "bytemuck") (r "^1.7") (d #t) (k 0)) (d (n "env_logger") (r "^0.8") (d #t) (k 2)) (d (n "glutin") (r "^0.25") (d #t) (k 2)) (d (n "glyph_brush") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "solstice") (r "^0.1") (f (quote ("derive"))) (d #t) (k 0)))) (h "04qvy92x8xprf08mz0x429wf4zjxb4yknw7irfk6yz414rsrnwws")))

(define-public crate-solstice_glyph-0.1.2 (c (n "solstice_glyph") (v "0.1.2") (d (list (d (n "bytemuck") (r "^1.7") (d #t) (k 0)) (d (n "env_logger") (r "^0.8") (d #t) (k 2)) (d (n "glutin") (r "^0.25") (d #t) (k 2)) (d (n "glyph_brush") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "solstice") (r "^0.1") (f (quote ("derive"))) (d #t) (k 0)))) (h "01p3dq8a4kg9wdqfw7al6h0zwwg92fjshbji78cqisgqykicn6q0")))

(define-public crate-solstice_glyph-0.1.3 (c (n "solstice_glyph") (v "0.1.3") (d (list (d (n "bytemuck") (r "^1.7") (d #t) (k 0)) (d (n "env_logger") (r "^0.8") (d #t) (k 2)) (d (n "glutin") (r "^0.25") (d #t) (k 2)) (d (n "glyph_brush") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "solstice") (r "^0.1") (f (quote ("derive"))) (d #t) (k 0)))) (h "132iqs8j5k7kinrdxdn4ygmvh87n89ydd7484j0mqz3h8gkva6x3")))

