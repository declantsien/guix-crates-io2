(define-module (crates-io so ny sonyflake) #:use-module (crates-io))

(define-public crate-sonyflake-0.1.0 (c (n "sonyflake") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "pnet") (r "^0.27") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "17ziqh3yxg7gyjks6hgjskx0fg5s3k4cx72ij7kjjswp28386j2n")))

(define-public crate-sonyflake-0.1.1 (c (n "sonyflake") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "pnet") (r "^0.27") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1hb2k7vm9yklx9cy3jywp9bhs05mndnpbglv9r5n42kyjz1np9lc")))

(define-public crate-sonyflake-0.1.2 (c (n "sonyflake") (v "0.1.2") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "pnet") (r "^0.28") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "10g7j5w3w8c5rk7nf3kr809dsspp52a1akp81my9jw5midpxh0j1")))

(define-public crate-sonyflake-0.2.0 (c (n "sonyflake") (v "0.2.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "pnet") (r "^0.33") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1c5vihk1wqh2g6qg06fhh5rashy06ilwimg1q6ib3a8bscvpg26x")))

