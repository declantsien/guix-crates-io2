(define-module (crates-io so ap soapy) #:use-module (crates-io))

(define-public crate-soapy-0.1.0 (c (n "soapy") (v "0.1.0") (d (list (d (n "soapy-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "soapy-shared") (r "^0.1.0") (d #t) (k 0)))) (h "1fbhs7dxgl150rb0i94lhfi04i78n76shxjz03r6in9a1a47i8m0")))

(define-public crate-soapy-0.2.0 (c (n "soapy") (v "0.2.0") (d (list (d (n "soapy-derive") (r "^0.2.0") (d #t) (k 0)) (d (n "soapy-shared") (r "^0.2.0") (d #t) (k 0)))) (h "1szp9q45ykqna4b678kkncbqidzivrvzakg11y37z5jm7ygm11pi")))

(define-public crate-soapy-0.2.1 (c (n "soapy") (v "0.2.1") (d (list (d (n "soapy-derive") (r "^0.2.0") (d #t) (k 0)) (d (n "soapy-shared") (r "^0.2.0") (d #t) (k 0)))) (h "0g9jz4r01lnmvzpcsyyrhgyj0y9z5h2ajq8bxb3d5v51y286nkim")))

(define-public crate-soapy-0.2.2 (c (n "soapy") (v "0.2.2") (d (list (d (n "soapy-derive") (r "^0.2.0") (d #t) (k 0)) (d (n "soapy-shared") (r "^0.2.0") (d #t) (k 0)))) (h "1z86ncjfc0fgb9537mbf0lkc912sgy65s3dn998nygr2b53dynv4")))

(define-public crate-soapy-0.2.3 (c (n "soapy") (v "0.2.3") (d (list (d (n "soapy-derive") (r "^0.2.0") (d #t) (k 0)))) (h "0s1kj76j7jia9dhvackld6d1plm5ffcsnv2m1qm220g1qpb33dxm")))

(define-public crate-soapy-0.2.4 (c (n "soapy") (v "0.2.4") (d (list (d (n "soapy-derive") (r "^0.2.3") (d #t) (k 0)))) (h "00ppfsmxgbvkb026rbf36pwjprh6y9s2kmbcd8dbxic8mpsr0hvc")))

(define-public crate-soapy-0.2.5 (c (n "soapy") (v "0.2.5") (d (list (d (n "soapy-derive") (r "^0.2.3") (d #t) (k 0)))) (h "04vxyl63lib4xav6qn3g1lld6cxzigxy7nlxz9smka4jvl3nx9rp")))

(define-public crate-soapy-0.2.6 (c (n "soapy") (v "0.2.6") (d (list (d (n "soapy-derive") (r "^0.2.6") (d #t) (k 0)))) (h "1kv5k71p90xycn3g49mwnj1ycpzhiwl52i21cpg14w541r8gffy0")))

(define-public crate-soapy-0.2.7 (c (n "soapy") (v "0.2.7") (d (list (d (n "soapy-derive") (r "^0.2.6") (d #t) (k 0)))) (h "1pczhw025z3rilgg46hrja7ayxnzd30qrdcbdjx2917gk0vbvimb")))

(define-public crate-soapy-0.2.8 (c (n "soapy") (v "0.2.8") (d (list (d (n "soapy-derive") (r "^0.2.6") (d #t) (k 0)))) (h "0nj0nr58dc9yywgpjipj0rdi8m7k60xj93ms6v9x42sxx99d473p")))

(define-public crate-soapy-0.2.9 (c (n "soapy") (v "0.2.9") (d (list (d (n "soapy-derive") (r "^0.2.8") (d #t) (k 0)))) (h "01qpm3lqhs9dk99k0kzk0fgs2d5kng683582d93jp4vn33bl54xm")))

