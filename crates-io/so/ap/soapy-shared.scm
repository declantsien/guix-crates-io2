(define-module (crates-io so ap soapy-shared) #:use-module (crates-io))

(define-public crate-soapy-shared-0.1.0 (c (n "soapy-shared") (v "0.1.0") (h "0aivkbapjjn1k49hbx0p5mhrjgph3v4a79j8g0391b9qsv1v71zk")))

(define-public crate-soapy-shared-0.2.0 (c (n "soapy-shared") (v "0.2.0") (h "1s0ynfblbghk3rv072j3a5ga8r648z2fnbjfmzba7qbxig96n4sa")))

