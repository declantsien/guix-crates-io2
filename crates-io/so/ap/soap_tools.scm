(define-module (crates-io so ap soap_tools) #:use-module (crates-io))

(define-public crate-soap_tools-0.0.1 (c (n "soap_tools") (v "0.0.1") (d (list (d (n "redis") (r "^0.23.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)))) (h "0fqkqzjy8shs625liwdjwbfsinml2sn20hqqlsi05c3hvh5xj7f1")))

(define-public crate-soap_tools-0.0.2 (c (n "soap_tools") (v "0.0.2") (d (list (d (n "jsonwebtoken") (r "^8.3.0") (d #t) (k 0)) (d (n "redis") (r "^0.23.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)))) (h "1f5npi7ykilx6hph938rrmlaxdc1c5nvpwyd0p2j1gbqcnz4vl8v")))

(define-public crate-soap_tools-0.0.3 (c (n "soap_tools") (v "0.0.3") (d (list (d (n "jsonwebtoken") (r "^8.3.0") (d #t) (k 0)) (d (n "redis") (r "^0.23.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1vls0ghl5phbfvnji6xc809i2p0sh739frrigbydnw22abjq2ai3")))

(define-public crate-soap_tools-0.0.4 (c (n "soap_tools") (v "0.0.4") (d (list (d (n "jsonwebtoken") (r "^8.3.0") (d #t) (k 0)) (d (n "redis") (r "^0.23.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "0hk3jklm8jsd3fmigpik4dbv3xjp3h2v533c0b607615h8yyi335")))

(define-public crate-soap_tools-0.0.5 (c (n "soap_tools") (v "0.0.5") (d (list (d (n "jsonwebtoken") (r "^8.3.0") (d #t) (k 0)) (d (n "redis") (r "^0.23.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "0an4313pnyd8bffvk43lmkl32phpylbj9diq1yj6kbvnnkw2jr0m")))

(define-public crate-soap_tools-0.0.6 (c (n "soap_tools") (v "0.0.6") (d (list (d (n "actix-web") (r "^4.3.1") (d #t) (k 0)) (d (n "jsonwebtoken") (r "^8.3.0") (d #t) (k 0)) (d (n "redis") (r "^0.23.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1fm3bp5cxfj5fcjzwvrw7gyl2y9vs504j3mhkaxhjgq3a73023zl")))

(define-public crate-soap_tools-0.0.7 (c (n "soap_tools") (v "0.0.7") (d (list (d (n "actix-web") (r "^4.3.1") (d #t) (k 0)) (d (n "jsonwebtoken") (r "^8.3.0") (d #t) (k 0)) (d (n "redis") (r "^0.23.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "0qm91m88pll4897jb544xdgp96wh2sghmc2il1cc7sh2c4kychw9")))

(define-public crate-soap_tools-0.0.8 (c (n "soap_tools") (v "0.0.8") (d (list (d (n "actix-web") (r "^4.3.1") (d #t) (k 0)) (d (n "jsonwebtoken") (r "^8.3.0") (d #t) (k 0)) (d (n "redis") (r "^0.23.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "01g359fcpijlhrz8yfb6bmnxzfqq27cvxygwqs4kaqq356m25r81")))

(define-public crate-soap_tools-0.0.9 (c (n "soap_tools") (v "0.0.9") (d (list (d (n "actix-web") (r "^4.3.1") (d #t) (k 0)) (d (n "jsonwebtoken") (r "^8.3.0") (d #t) (k 0)) (d (n "redis") (r "^0.23.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1yiirvg4f3v5k1n0jrzm9a2k0pccjcy47pif1hlz1nxsxgx093c7")))

(define-public crate-soap_tools-0.0.10 (c (n "soap_tools") (v "0.0.10") (d (list (d (n "actix-web") (r "^4.3.1") (d #t) (k 0)) (d (n "jsonwebtoken") (r "^8.3.0") (d #t) (k 0)) (d (n "redis") (r "^0.23.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1p067g6cys3j2p83f3n1pl4nq4s31xbbjgdzxh31z3k6qj1rjk70")))

(define-public crate-soap_tools-0.0.11 (c (n "soap_tools") (v "0.0.11") (d (list (d (n "actix-web") (r "^4.3.1") (d #t) (k 0)) (d (n "jsonwebtoken") (r "^8.3.0") (d #t) (k 0)) (d (n "redis") (r "^0.23.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "18hs1lxrnaz72j796i3lxfk228az361nhy4g945jn3bz04ckankb")))

(define-public crate-soap_tools-0.0.13 (c (n "soap_tools") (v "0.0.13") (d (list (d (n "actix-web") (r "^4.3.1") (d #t) (k 0)) (d (n "jsonwebtoken") (r "^8.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1j6qgimiqbrg79qm1v8v5c29lvandsjy47yda9ivrsv5fyvpj5dn")))

(define-public crate-soap_tools-0.0.14 (c (n "soap_tools") (v "0.0.14") (d (list (d (n "actix-web") (r "^4.3.1") (d #t) (k 0)) (d (n "jsonwebtoken") (r "^8.3.0") (d #t) (k 0)) (d (n "mongodb") (r "^2.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "0yz0pj4l5bzi3pbh3q95scr73327y4gw6hz9gp4bpr0wl3z1m82h") (y #t)))

