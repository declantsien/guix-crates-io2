(define-module (crates-io so ap soapy-derive) #:use-module (crates-io))

(define-public crate-soapy-derive-0.1.0 (c (n "soapy-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1hsb4r0skjl01x7q3y24p20gjrl3d1xrrhr8gknvr8hs1fvb9wgj")))

(define-public crate-soapy-derive-0.2.0 (c (n "soapy-derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "09i1a1s43igkjjpamkqjhxkq5cfkx84i9ngsisq6l9dbyr50f10w")))

(define-public crate-soapy-derive-0.2.3 (c (n "soapy-derive") (v "0.2.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1n491jby2m7r0hpgl9sqx82lnhgs9042bvnky9m1nyk5afhg5vai")))

(define-public crate-soapy-derive-0.2.6 (c (n "soapy-derive") (v "0.2.6") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1j8l88ighb37pm89bfvak0c4zk8dgwrk9a32i0x275wgsddim4ap")))

(define-public crate-soapy-derive-0.2.7 (c (n "soapy-derive") (v "0.2.7") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0x2fxsvkpfwn57jmqms8p7k68jfyjir7gnwnxsv49zm637yysls0")))

(define-public crate-soapy-derive-0.2.8 (c (n "soapy-derive") (v "0.2.8") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0h8vr1blskq7vrwa5ryqgz3ngamcqx0v92rbldkzyagnkmwd9ga2")))

