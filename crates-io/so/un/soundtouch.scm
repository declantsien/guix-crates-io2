(define-module (crates-io so un soundtouch) #:use-module (crates-io))

(define-public crate-soundtouch-0.1.0 (c (n "soundtouch") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "hound") (r "^3.5") (d #t) (k 2)) (d (n "libc") (r "^0.2") (k 0)) (d (n "soundtouch-ffi") (r "^0.1.1") (d #t) (k 0)))) (h "06acw1zc66z0bbgk4amij9zl6l92pxkyq4g0llc9dqq8kvbivbhi")))

(define-public crate-soundtouch-0.2.0 (c (n "soundtouch") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "hound") (r "^3.5") (d #t) (k 2)) (d (n "libc") (r "^0.2") (k 0)) (d (n "soundtouch-ffi") (r "^0.1.1") (d #t) (k 0)))) (h "0panrzwdqj4lnv1akjyri38had90vrxazywk3yw7yzi54wl8p11n") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-soundtouch-0.3.0 (c (n "soundtouch") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "hound") (r "^3.5") (d #t) (k 2)) (d (n "libc") (r "^0.2") (k 0)) (d (n "soundtouch-ffi") (r "^0.2.0") (d #t) (k 0)))) (h "1jvh13yp7s257m7pvmxf7k7v6zlibqlfgd6gk59fik3c04rggsvw") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-soundtouch-0.4.0 (c (n "soundtouch") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "hound") (r "^3.5") (d #t) (k 2)) (d (n "libc") (r "^0.2") (k 0)) (d (n "soundtouch-ffi") (r "^0.2.0") (d #t) (k 0)))) (h "1pby6lg7zp9d8cxwr81ahhp8lh375gjvl3d8bhlw5qqgwdkjyrhd") (f (quote (("default" "alloc") ("alloc"))))))

