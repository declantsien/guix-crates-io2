(define-module (crates-io so un soundtouch-ffi) #:use-module (crates-io))

(define-public crate-soundtouch-ffi-0.1.0 (c (n "soundtouch-ffi") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libc") (r "^0.2") (k 0)))) (h "0bdxp1h585dkjh2dsg1pdh9hy6161x3pp2ai0caykim1d7n364nf") (l "SoundTouch")))

(define-public crate-soundtouch-ffi-0.1.1 (c (n "soundtouch-ffi") (v "0.1.1") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libc") (r "^0.2") (k 0)))) (h "1jw0vr75dkrbcqp9jiz1rca41qyhhimxdbd6qdfihldz8434bnwg") (l "SoundTouch")))

(define-public crate-soundtouch-ffi-0.1.2 (c (n "soundtouch-ffi") (v "0.1.2") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libc") (r "^0.2") (k 0)))) (h "09kyfvza43wv1qax7naj3fxjg3yypqr13hw5ndb3fcznbmq89sbl") (l "SoundTouch")))

(define-public crate-soundtouch-ffi-0.2.0 (c (n "soundtouch-ffi") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.68") (d #t) (k 1)) (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libc") (r "^0.2") (k 0)))) (h "0367iz2cb3wy9n2fn0aw69sdcy2iklakwypmmjzjf3jp3h6wwq7x") (l "SoundTouch")))

