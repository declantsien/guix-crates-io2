(define-module (crates-io so un soundpad-xml) #:use-module (crates-io))

(define-public crate-soundpad-xml-0.1.0 (c (n "soundpad-xml") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.6.0") (d #t) (k 0)) (d (n "time") (r "^0.3.17") (f (quote ("formatting"))) (d #t) (k 0)))) (h "1vpy1dcbmah7abn0c0dyddac7wmj7gbzy061r4wydnl40f92008x")))

(define-public crate-soundpad-xml-0.1.1 (c (n "soundpad-xml") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.6.0") (d #t) (k 0)) (d (n "time") (r "^0.3.17") (f (quote ("formatting"))) (d #t) (k 0)))) (h "1pdpr26yjfy34yi70v8cnihim7p55mzggn035la5n4vdg04yjg4y")))

