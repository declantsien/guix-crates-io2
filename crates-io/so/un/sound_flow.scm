(define-module (crates-io so un sound_flow) #:use-module (crates-io))

(define-public crate-sound_flow-0.1.0 (c (n "sound_flow") (v "0.1.0") (d (list (d (n "enum_delegate") (r "^0.2.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.6.2") (d #t) (k 0)))) (h "0fq2qfr74yn53kkzd1gxkpwaf1dkicyw8kgajw1q020xxzdm3lg2")))

(define-public crate-sound_flow-0.2.0 (c (n "sound_flow") (v "0.2.0") (d (list (d (n "enum_delegate") (r "^0.2.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.6.2") (d #t) (k 0)))) (h "000kz8qr25mrjpn3i4ra71czjwmc7bg5d36if8cdx2k29j64l5ha")))

(define-public crate-sound_flow-0.3.0 (c (n "sound_flow") (v "0.3.0") (d (list (d (n "enum_delegate") (r "^0.2.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.6.2") (d #t) (k 0)))) (h "0x6r947ciaivgk19fkdx1h713yykfc7k1qdg7bax6mcls0jdz8in")))

