(define-module (crates-io so un soundfont) #:use-module (crates-io))

(define-public crate-soundfont-0.0.0 (c (n "soundfont") (v "0.0.0") (d (list (d (n "riff") (r "^1.0.1") (d #t) (k 0)))) (h "1gh5p79nl7jfwkjwag48gr400wn9d3vxn0972xqs4yb5x1fbkkvg")))

(define-public crate-soundfont-0.0.1 (c (n "soundfont") (v "0.0.1") (d (list (d (n "riff") (r "^1.0.1") (d #t) (k 0)))) (h "0idsgy8jqc7q358vhksrbvakmxmzpi5hrgbjj5vawhs2ksxzcac0")))

(define-public crate-soundfont-0.0.2 (c (n "soundfont") (v "0.0.2") (d (list (d (n "riff") (r "^1.0.1") (d #t) (k 0)))) (h "1fln8zv6jmc3cb9929w4jl6v6ml8bnbkklgc3rzgdbzdpb66dj8z")))

(define-public crate-soundfont-0.0.3 (c (n "soundfont") (v "0.0.3") (d (list (d (n "riff") (r "^1.0.1") (d #t) (k 0)))) (h "1kizhps881ykpzl29jqhjpsb4gl1dzgv2gzksk2nsf71kgpc9ww8")))

