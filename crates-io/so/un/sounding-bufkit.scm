(define-module (crates-io so un sounding-bufkit) #:use-module (crates-io))

(define-public crate-sounding-bufkit-0.0.1 (c (n "sounding-bufkit") (v "0.0.1") (d (list (d (n "chrono") (r "^0.3") (d #t) (k 0)) (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "sounding-base") (r "^0.0.1") (d #t) (k 0)))) (h "0hirqskhlqcdqk3lcsyrv41xyvy9gcawvscg5qbwiqxqqsbxkd9b")))

(define-public crate-sounding-bufkit-0.0.2 (c (n "sounding-bufkit") (v "0.0.2") (d (list (d (n "chrono") (r "^0.3") (d #t) (k 0)) (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "sounding-base") (r "^0.0.3") (d #t) (k 0)))) (h "0z7dmc13slp4rs5v994qcs6bz20a5m1cgksj782yd4diwrsaj1p7")))

(define-public crate-sounding-bufkit-0.0.3 (c (n "sounding-bufkit") (v "0.0.3") (d (list (d (n "chrono") (r "^0.3") (d #t) (k 0)) (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "sounding-base") (r "^0.0.4") (d #t) (k 0)))) (h "11nbg2gb2xh9zwd07nw9kpx6my45s0ywna4vb7ykzdzj8wmxf1y5")))

(define-public crate-sounding-bufkit-0.0.4 (c (n "sounding-bufkit") (v "0.0.4") (d (list (d (n "chrono") (r "^0.3") (d #t) (k 0)) (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "sounding-base") (r "^0.0.5") (d #t) (k 0)))) (h "1kn2n3i33hwgh8s8g0d0qs2if4sbvmv6mp8wfyip57y1cbyrxdk6")))

(define-public crate-sounding-bufkit-0.0.5 (c (n "sounding-bufkit") (v "0.0.5") (d (list (d (n "chrono") (r "^0.3") (d #t) (k 0)) (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "sounding-base") (r "^0.0.7") (d #t) (k 0)))) (h "1pb6vri3mfa25i7d8sn53ci09cwlm3zmc1lz2wanmqa8ggrb6700")))

(define-public crate-sounding-bufkit-0.0.6 (c (n "sounding-bufkit") (v "0.0.6") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "sounding-base") (r "^0.0.8") (d #t) (k 0)))) (h "02ci5zprvcfaksgvsid43i6d0kqymmzi3llsrpnwdrnix0xlacza")))

(define-public crate-sounding-bufkit-0.0.7 (c (n "sounding-bufkit") (v "0.0.7") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "sounding-base") (r "^0.0.9") (d #t) (k 0)))) (h "192ja8hx5fwmbyjr7bnpxrm1nps12h0yz21picg3byxp7lax2gjd")))

(define-public crate-sounding-bufkit-0.0.8 (c (n "sounding-bufkit") (v "0.0.8") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "sounding-base") (r "^0.0.10") (d #t) (k 0)))) (h "1hy1hlrf75gkmmspbm6xbm0j067w2wrzyc3y8g73ykgdd6ycn766")))

(define-public crate-sounding-bufkit-0.0.9 (c (n "sounding-bufkit") (v "0.0.9") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "sounding-base") (r "^0.0.11") (d #t) (k 0)))) (h "14k60kbp9ingf49f6xdiv2cnri0spn58pj0kj309fhjfbhmfcsnx")))

(define-public crate-sounding-bufkit-0.0.10 (c (n "sounding-bufkit") (v "0.0.10") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "sounding-base") (r "^0.0.12") (d #t) (k 0)))) (h "1mzdfqf1fdm266gd3ikpgyn1z4ixjxvr9swqhgi9f4dy8zcwpq30")))

(define-public crate-sounding-bufkit-0.0.11 (c (n "sounding-bufkit") (v "0.0.11") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "sounding-base") (r "^0.0.15") (d #t) (k 0)) (d (n "sounding-validate") (r "^0.0.2") (d #t) (k 0)))) (h "1ls77x49ahr1fh663dw5d5fscmzj671zsfb25afrcb1bqxnq9fd5")))

(define-public crate-sounding-bufkit-0.1.0 (c (n "sounding-bufkit") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "sounding-base") (r "^0.1.0") (d #t) (k 0)) (d (n "sounding-validate") (r "^0.1.0") (d #t) (k 0)))) (h "1gnv1gvvj3rvcqwqx5s82qwa1icrc7ymf62a8iypd6rzrmh7mdfb")))

(define-public crate-sounding-bufkit-0.2.0 (c (n "sounding-bufkit") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "sounding-base") (r "^0.2.0") (d #t) (k 0)) (d (n "sounding-validate") (r "^0.2.0") (d #t) (k 0)))) (h "18hi61ickyr2zwgyi76swsnplvy7kw691nsja0r4mnw7p8hki394")))

(define-public crate-sounding-bufkit-0.2.1 (c (n "sounding-bufkit") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "sounding-base") (r "^0.2.0") (d #t) (k 0)) (d (n "sounding-validate") (r "^0.2.0") (d #t) (k 0)))) (h "0g55q3y2s1r1h1llggbil23wlsiy64fdips5ymc45k6hhb8ib7sk")))

(define-public crate-sounding-bufkit-0.3.0 (c (n "sounding-bufkit") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "sounding-base") (r "^0.3.0") (d #t) (k 0)) (d (n "sounding-validate") (r "^0.3.0") (d #t) (k 0)))) (h "0bivxsy5qsc4d1652fyqfhrqqgy9nh9cribpr57r8wv4yxis979m")))

(define-public crate-sounding-bufkit-0.7.0 (c (n "sounding-bufkit") (v "0.7.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "optional") (r "^0.4.2") (d #t) (k 0)) (d (n "sounding-analysis") (r "^0.7") (d #t) (k 0)) (d (n "sounding-base") (r "^0.7") (d #t) (k 0)))) (h "1b167iss194mzpnrksgqidk2z33hkx4x81pj7v7gq7kg50595m8k")))

(define-public crate-sounding-bufkit-0.8.0 (c (n "sounding-bufkit") (v "0.8.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "optional") (r "^0.4.2") (d #t) (k 0)) (d (n "sounding-analysis") (r "^0.8") (d #t) (k 0)) (d (n "sounding-base") (r "^0.7") (d #t) (k 0)))) (h "1ca7xn28fniyfklhww9fplax8x75bprz52hhmhld7qddlwgykw62")))

(define-public crate-sounding-bufkit-0.8.1 (c (n "sounding-bufkit") (v "0.8.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "optional") (r "^0.4.2") (d #t) (k 0)) (d (n "sounding-analysis") (r "^0.8") (d #t) (k 0)) (d (n "sounding-base") (r "^0.7") (d #t) (k 0)))) (h "0siw74c3m414i66w6cmhypx6w0rv06bz3dhfcvwk2i26wc4jmc7j")))

(define-public crate-sounding-bufkit-0.8.2 (c (n "sounding-bufkit") (v "0.8.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "metfor") (r "^0.3.2") (d #t) (k 0)) (d (n "optional") (r "^0.4.2") (d #t) (k 0)) (d (n "sounding-analysis") (r "^0.8") (d #t) (k 0)) (d (n "sounding-base") (r "^0.7") (d #t) (k 0)))) (h "0inm8jzasxpicl9wf7wm5r8b15dxwpgii00hfyxd8jlngssgawr3")))

(define-public crate-sounding-bufkit-0.9.0 (c (n "sounding-bufkit") (v "0.9.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "metfor") (r "^0.4.0") (d #t) (k 0)) (d (n "optional") (r "^0.4.3") (d #t) (k 0)) (d (n "sounding-analysis") (r "^0.9") (d #t) (k 0)) (d (n "sounding-base") (r "^0.8") (d #t) (k 0)))) (h "04v4ynynjvh3iw0apc6xhnsa64nmrnki3s8a41gkx3k0n3ndyykf")))

(define-public crate-sounding-bufkit-0.9.1 (c (n "sounding-bufkit") (v "0.9.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "metfor") (r "^0.4.0") (d #t) (k 0)) (d (n "optional") (r "^0.5.0") (d #t) (k 0)) (d (n "sounding-analysis") (r "^0.9.3") (d #t) (k 0)) (d (n "sounding-base") (r "^0.8.1") (d #t) (k 0)))) (h "022ihw53b3x0p4rrv5iaycrzchdmgfywil7pgphb5ngwk5kcp6yc")))

(define-public crate-sounding-bufkit-0.10.0 (c (n "sounding-bufkit") (v "0.10.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "metfor") (r "^0.7.0") (d #t) (k 0)) (d (n "optional") (r "^0.5.0") (d #t) (k 0)) (d (n "sounding-analysis") (r "^0.10.0") (d #t) (k 0)) (d (n "sounding-base") (r "^0.10.0") (d #t) (k 0)))) (h "0201qg40fhjn309g4lwxf5cswgbz7g08j1vlwawn03in1s4lysfm")))

(define-public crate-sounding-bufkit-0.11.0 (c (n "sounding-bufkit") (v "0.11.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "metfor") (r "^0.7.0") (d #t) (k 0)) (d (n "optional") (r "^0.5.0") (d #t) (k 0)) (d (n "sounding-analysis") (r "^0.11.0") (d #t) (k 0)) (d (n "sounding-base") (r "^0.11.0") (d #t) (k 0)))) (h "1zp99qhj729i9gqc775d0i3nc1202c3vlwmm7dlcghjma8pvhrby")))

(define-public crate-sounding-bufkit-0.12.0 (c (n "sounding-bufkit") (v "0.12.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "metfor") (r "^0.7.0") (d #t) (k 0)) (d (n "optional") (r "^0.5.0") (d #t) (k 0)) (d (n "sounding-analysis") (r "^0.12.0") (d #t) (k 0)))) (h "1aca5gkcjnxc8adr709s7y6w7gpdrvqrh1cpwwpmdcp2pw2gvb4b")))

(define-public crate-sounding-bufkit-0.12.1 (c (n "sounding-bufkit") (v "0.12.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "metfor") (r "^0.7.0") (d #t) (k 0)) (d (n "optional") (r "^0.5.0") (d #t) (k 0)) (d (n "sounding-analysis") (r "^0.12.0") (d #t) (k 0)))) (h "001x0ly2knn5b2yyk0v4fh9yzc1hl2jiyrpgr5qlq16bvh1gvw53")))

(define-public crate-sounding-bufkit-0.12.2 (c (n "sounding-bufkit") (v "0.12.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "metfor") (r "^0.7.0") (d #t) (k 0)) (d (n "optional") (r "^0.5.0") (d #t) (k 0)) (d (n "sounding-analysis") (r "^0.12.0") (d #t) (k 0)))) (h "0i8frayc7rlxvmy6djfy06xls59jwzg3bckf44jcvzrfhw18zjlm")))

(define-public crate-sounding-bufkit-0.13.0 (c (n "sounding-bufkit") (v "0.13.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "metfor") (r "^0.7.0") (d #t) (k 0)) (d (n "optional") (r "^0.5.0") (d #t) (k 0)) (d (n "sounding-analysis") (r "^0.13.0") (d #t) (k 0)))) (h "1glbpbbs4kd5amq2rdkfbdfhycpjrp0ahyrzyyvnjlbmq7dqgcf1")))

(define-public crate-sounding-bufkit-0.13.1 (c (n "sounding-bufkit") (v "0.13.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "metfor") (r "^0.7.4") (d #t) (k 0)) (d (n "optional") (r "^0.5.0") (d #t) (k 0)) (d (n "sounding-analysis") (r "^0.13.1") (d #t) (k 0)))) (h "0jkw67jwlx8120gk9lhgazxjprpvzzrxhlch8ix48dk9ma13mk36")))

(define-public crate-sounding-bufkit-0.14.0 (c (n "sounding-bufkit") (v "0.14.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "metfor") (r "^0.7.4") (d #t) (k 0)) (d (n "optional") (r "^0.5.0") (d #t) (k 0)) (d (n "sounding-analysis") (r "^0.14.0") (d #t) (k 0)))) (h "1h4z613xlhdm7lzy7p4f0if5hbmiqal8dpxi2xsk1ll6c52v6h3a")))

(define-public crate-sounding-bufkit-0.14.1 (c (n "sounding-bufkit") (v "0.14.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "metfor") (r "^0.7.4") (d #t) (k 0)) (d (n "optional") (r "^0.5.0") (d #t) (k 0)) (d (n "sounding-analysis") (r "^0.14.0") (d #t) (k 0)))) (h "0xdknrdllyjijnpms7yy2b8sr6rb4852064idnlnjx2rhcn63vy7")))

(define-public crate-sounding-bufkit-0.14.2 (c (n "sounding-bufkit") (v "0.14.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "metfor") (r "^0.7.4") (d #t) (k 0)) (d (n "optional") (r "^0.5.0") (d #t) (k 0)) (d (n "sounding-analysis") (r "^0.15.0") (d #t) (k 0)))) (h "0xkrirdk5cc44lbb11wvqx689i4dw492zs3gg5mw58m7j31ljjqh")))

(define-public crate-sounding-bufkit-0.15.0 (c (n "sounding-bufkit") (v "0.15.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "metfor") (r "^0.8.1") (d #t) (k 0)) (d (n "optional") (r "^0.5.0") (d #t) (k 0)) (d (n "sounding-analysis") (r "^0.16") (d #t) (k 0)))) (h "1q5rs0imj6yf08awrny7m7p3hnc57r9vsjb0qrjiaxy7mfzf7nqf")))

(define-public crate-sounding-bufkit-0.16.0 (c (n "sounding-bufkit") (v "0.16.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "metfor") (r "^0.9.0") (d #t) (k 0)) (d (n "optional") (r "^0.5.0") (d #t) (k 0)) (d (n "sounding-analysis") (r "^0.17") (d #t) (k 0)))) (h "1isv7vc42n0d5173bkw8ppk7spxg80jj1hc8bidngvjksxphh0lk")))

