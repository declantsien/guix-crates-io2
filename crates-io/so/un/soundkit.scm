(define-module (crates-io so un soundkit) #:use-module (crates-io))

(define-public crate-soundkit-0.9.0 (c (n "soundkit") (v "0.9.0") (d (list (d (n "opus") (r "^0.3.0") (d #t) (k 0)) (d (n "uuid") (r "^1.3.0") (f (quote ("v4" "fast-rng" "macro-diagnostics"))) (d #t) (k 0)))) (h "09davk2pl6andjqd8s6j2vdpspp4jk4fd340cgmar1advb3bw3r4")))

(define-public crate-soundkit-0.9.1 (c (n "soundkit") (v "0.9.1") (d (list (d (n "opus") (r "^0.3.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1697dkm9dbd24as3nmncw5ys8hjv6h88slnrx51p64bv1gdwal29")))

(define-public crate-soundkit-0.9.2 (c (n "soundkit") (v "0.9.2") (d (list (d (n "opus") (r "^0.3.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0iicy19fixyarn4vvp9xhg8czs71vb9rih3rmdzdsmd4nl3py8a8")))

(define-public crate-soundkit-0.9.3 (c (n "soundkit") (v "0.9.3") (d (list (d (n "opus") (r "^0.3.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1qy4p4al52iad18gjhzzphghpl20rzcaa4qixs9m5yas3jdf6dpw")))

