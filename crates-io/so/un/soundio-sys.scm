(define-module (crates-io so un soundio-sys) #:use-module (crates-io))

(define-public crate-soundio-sys-0.1.0 (c (n "soundio-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0fmizlhxwhj8gc42irfq3pzv7r0vxiwj0175bf2nhf6638fnnnkh") (y #t)))

(define-public crate-soundio-sys-0.1.1 (c (n "soundio-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "15yi9hf32p6i89sxjjxqxvv16ixnbcy4m76bv2wc9lkb8841ba3v") (y #t)))

(define-public crate-soundio-sys-0.1.2 (c (n "soundio-sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0yv84jqgsngwm61n162ywp9v9af92nn34wz4ijqdf05vvz52sr1a") (y #t)))

(define-public crate-soundio-sys-0.1.3 (c (n "soundio-sys") (v "0.1.3") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "07zf8jnldrqablq47m9mg151b148k3lqcknyrh9jl5lvgvmw17iy")))

