(define-module (crates-io so un soundchange-english) #:use-module (crates-io))

(define-public crate-soundchange-english-0.0.1 (c (n "soundchange-english") (v "0.0.1") (d (list (d (n "soundchange") (r "^0.0.1") (d #t) (k 0)))) (h "13s5gbkpp52zclrw466i928kjfhziql4v66hvbs5n6i7ca16kbaj")))

(define-public crate-soundchange-english-0.0.2 (c (n "soundchange-english") (v "0.0.2") (d (list (d (n "soundchange") (r "^0.0.3") (d #t) (k 0)))) (h "1vjsgw6c15z301ab98jk52snqra2pwywigp84xcs3nv2a2byzwvy")))

(define-public crate-soundchange-english-0.0.4 (c (n "soundchange-english") (v "0.0.4") (d (list (d (n "soundchange") (r "^0.0.4") (d #t) (k 0)))) (h "05d6wzanbhgznq85kl25p04d0iss5l4ckvahsd2nccc71azb6c7d")))

(define-public crate-soundchange-english-0.0.5 (c (n "soundchange-english") (v "0.0.5") (d (list (d (n "soundchange") (r "^0.0.5") (d #t) (k 0)))) (h "0lfvgbaccx95waznlhil61r1z1kygwjrdl93p89r4b56nq14pb7y")))

(define-public crate-soundchange-english-0.0.6 (c (n "soundchange-english") (v "0.0.6") (d (list (d (n "log") (r "^0.2.2") (d #t) (k 0)) (d (n "soundchange") (r "^0.0.6") (d #t) (k 0)))) (h "1l2jny9p4hw5dw28p3llb4bvis1fg7ybg444r1szjn89fna3y26d")))

(define-public crate-soundchange-english-0.0.7 (c (n "soundchange-english") (v "0.0.7") (d (list (d (n "log") (r "^0.2.4") (d #t) (k 0)) (d (n "soundchange") (r "^0.0.7") (d #t) (k 0)))) (h "1p2sjmh6d0lqnrap5qj3j8m0kr8c7n4m8il2a7bpdmiajdxbxki6")))

(define-public crate-soundchange-english-0.0.8 (c (n "soundchange-english") (v "0.0.8") (d (list (d (n "log") (r "^0.2.5") (d #t) (k 0)) (d (n "soundchange") (r "^0.0.8") (d #t) (k 0)))) (h "0w6w4i8vk81q5pjdqk1dfj7nnfkj38fqj9by696akdp9gpmxllls")))

