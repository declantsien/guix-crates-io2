(define-module (crates-io so un soundchange) #:use-module (crates-io))

(define-public crate-soundchange-0.0.1 (c (n "soundchange") (v "0.0.1") (h "1fz77chxqd3hh1fzpnb04pdgrz40rdvf22i7i6r2phpm1s3kzgql")))

(define-public crate-soundchange-0.0.3 (c (n "soundchange") (v "0.0.3") (h "12whbc9cg7zyl2xjgqhkd1706knzsnyapvgmlghkxq9bw67acddb")))

(define-public crate-soundchange-0.0.4 (c (n "soundchange") (v "0.0.4") (h "1yabd7jibxbb912j02mj39q0gcjnsjzyksljzhmz0zhg3wrvvfzw")))

(define-public crate-soundchange-0.0.5 (c (n "soundchange") (v "0.0.5") (h "1zgajdzlim4822hbdbwl5nbcadazyszs5cbx9p40rdh1gdvdy5jy")))

(define-public crate-soundchange-0.0.6 (c (n "soundchange") (v "0.0.6") (d (list (d (n "log") (r "^0.2.2") (d #t) (k 2)))) (h "04b9prp3xd57q0d9a2ifbad96kdi28y95rr88n17yzkfhc9q02ym")))

(define-public crate-soundchange-0.0.7 (c (n "soundchange") (v "0.0.7") (d (list (d (n "log") (r "^0.2.4") (d #t) (k 2)))) (h "0932sajm1hqnm6v2j6xcsa6005fmn4kpfwvl08j9fyrw74wb6yi6")))

(define-public crate-soundchange-0.0.8 (c (n "soundchange") (v "0.0.8") (d (list (d (n "log") (r "^0.2.5") (d #t) (k 2)))) (h "0v84wrmlxmzf7hcj8wqsdxl2hx3lijq4ja46rsmadkwnfgb57c9z")))

