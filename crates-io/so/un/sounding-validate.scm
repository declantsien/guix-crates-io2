(define-module (crates-io so un sounding-validate) #:use-module (crates-io))

(define-public crate-sounding-validate-0.0.2 (c (n "sounding-validate") (v "0.0.2") (d (list (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "sounding-base") (r "^0.0.15") (d #t) (k 0)))) (h "0jbjjryph32xsd0pns4w9qnqashnhfw1awfqc4qfnjapr9cfza11")))

(define-public crate-sounding-validate-0.1.0 (c (n "sounding-validate") (v "0.1.0") (d (list (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "sounding-base") (r "^0.1.0") (d #t) (k 0)))) (h "07gr9haiv3axgjpbckyxsbfqlmajp807cl73k9d865jh27l8p7qp")))

(define-public crate-sounding-validate-0.2.0 (c (n "sounding-validate") (v "0.2.0") (d (list (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "sounding-base") (r "^0.2.0") (d #t) (k 0)))) (h "0mlpwy684jz1fcx5b8llww9prxsayysvdnha0iaf3gmwk1v3i2ij")))

(define-public crate-sounding-validate-0.3.0 (c (n "sounding-validate") (v "0.3.0") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.1") (d #t) (k 0)) (d (n "sounding-base") (r "^0.3") (d #t) (k 0)))) (h "1wfs1nd1w8mg9sjxrjfia774hj3i2pyy4aiqa6yjxglkz3q6skb4")))

(define-public crate-sounding-validate-0.4.0 (c (n "sounding-validate") (v "0.4.0") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.1") (d #t) (k 0)) (d (n "sounding-base") (r "^0.4") (d #t) (k 0)))) (h "10qs2zrfgvjy6wcbjm45s77m8wx0wgx1ffhzy8hh29fg8adqqzax")))

(define-public crate-sounding-validate-0.5.0 (c (n "sounding-validate") (v "0.5.0") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.1") (d #t) (k 0)) (d (n "sounding-base") (r "^0.5.1") (d #t) (k 0)))) (h "0sxcgxqjfpj8phk4mpjbda5353pf71dq0rzihywxdz3a4wwdzrgi")))

(define-public crate-sounding-validate-0.6.0 (c (n "sounding-validate") (v "0.6.0") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.1") (d #t) (k 0)) (d (n "sounding-base") (r "^0.6") (d #t) (k 0)))) (h "0vvllryn3f7x7srliwa7qng7y2kq5137sns9frisb12waf6y5rgc")))

(define-public crate-sounding-validate-0.7.0 (c (n "sounding-validate") (v "0.7.0") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.1") (d #t) (k 0)) (d (n "optional") (r "^0.4.1") (d #t) (k 0)) (d (n "sounding-base") (r "^0.7") (d #t) (k 0)))) (h "1w0walhvx6rg8052sgadf8h242barx503v51s7axvfz4nf29px3d")))

(define-public crate-sounding-validate-0.7.1 (c (n "sounding-validate") (v "0.7.1") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.1") (d #t) (k 0)) (d (n "optional") (r "^0.4.1") (d #t) (k 0)) (d (n "sounding-base") (r "^0.7") (d #t) (k 0)))) (h "0phyy0fmjq9csgl2lblvx7y7g73cix17vvdbwh5q2d5k3nx7n1p4")))

(define-public crate-sounding-validate-0.8.0 (c (n "sounding-validate") (v "0.8.0") (d (list (d (n "optional") (r "^0.4.3") (d #t) (k 0)) (d (n "sounding-base") (r "^0.8") (d #t) (k 0)))) (h "0hdrvpzppds3vlyjswc7k33xclxd8hvpi4rb71x4jcjlf41zzwlb")))

(define-public crate-sounding-validate-0.8.1 (c (n "sounding-validate") (v "0.8.1") (d (list (d (n "optional") (r "^0.5.0") (d #t) (k 0)) (d (n "sounding-base") (r "^0.8") (d #t) (k 0)))) (h "122jwhri83ipvxjisa83i5mw3zw8wf8av5q9ffjpi7jzjpk4di9b")))

(define-public crate-sounding-validate-0.9.0 (c (n "sounding-validate") (v "0.9.0") (d (list (d (n "metfor") (r "^0.5.0") (d #t) (k 0)) (d (n "optional") (r "^0.5.0") (d #t) (k 0)) (d (n "sounding-base") (r "^0.9.1") (d #t) (k 0)))) (h "03qcmv0p766yw7fwxmiv1jk11knfqb3bdhmr6ymqw7msr955pz04")))

(define-public crate-sounding-validate-0.9.1 (c (n "sounding-validate") (v "0.9.1") (d (list (d (n "metfor") (r "^0.6.0") (d #t) (k 0)) (d (n "optional") (r "^0.5.0") (d #t) (k 0)) (d (n "sounding-base") (r "^0.9.2") (d #t) (k 0)))) (h "1r120qzm87z8x73b9gcq86p2r045pl0qsg17z8nbwlqxq4qxq81z")))

(define-public crate-sounding-validate-0.10.0 (c (n "sounding-validate") (v "0.10.0") (d (list (d (n "metfor") (r "^0.7.0") (d #t) (k 0)) (d (n "optional") (r "^0.5.0") (d #t) (k 0)) (d (n "sounding-base") (r "^0.10.0") (d #t) (k 0)))) (h "1zgnr4mvmp4a8jm6x6jiqsmq9nigrcl4ickbzy5ck15yajl800r2")))

(define-public crate-sounding-validate-0.10.1 (c (n "sounding-validate") (v "0.10.1") (d (list (d (n "metfor") (r "^0.7.0") (d #t) (k 0)) (d (n "optional") (r "^0.5.0") (d #t) (k 0)) (d (n "sounding-base") (r "^0.10.0") (d #t) (k 0)))) (h "19vjjnssr2jsadvnm1iasf46rzfg6iq9z5y05i67yzlbi5pfml74")))

