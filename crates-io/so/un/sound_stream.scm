(define-module (crates-io so un sound_stream) #:use-module (crates-io))

(define-public crate-sound_stream-0.2.0 (c (n "sound_stream") (v "0.2.0") (d (list (d (n "portaudio") (r "*") (d #t) (k 0)) (d (n "sample") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)))) (h "0lqw48fz4gbnzvxiiw5jg9c0ms5nb4sgpavaf4jv61pmr1clda90")))

(define-public crate-sound_stream-0.3.0 (c (n "sound_stream") (v "0.3.0") (d (list (d (n "portaudio") (r "*") (d #t) (k 0)) (d (n "sample") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)))) (h "04irg101dgg3cf8fc5dprkfcqhh7vfkhx071a14kr8201fkjcg20")))

(define-public crate-sound_stream-0.3.1 (c (n "sound_stream") (v "0.3.1") (d (list (d (n "portaudio") (r "*") (d #t) (k 0)) (d (n "sample") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)))) (h "0nkqf76pf11y9kdrk4cgg2hzyqhsndab8544lvkqmgz0xnxxpsv6")))

(define-public crate-sound_stream-0.3.2 (c (n "sound_stream") (v "0.3.2") (d (list (d (n "num") (r "*") (d #t) (k 0)) (d (n "portaudio") (r "*") (d #t) (k 0)) (d (n "sample") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)))) (h "1yx5icwxj17j4y0np1dhp4d29kfmrix18jrvd7r9k0b1n9hnas2s")))

(define-public crate-sound_stream-0.3.3 (c (n "sound_stream") (v "0.3.3") (d (list (d (n "num") (r "*") (d #t) (k 0)) (d (n "portaudio") (r "*") (d #t) (k 0)) (d (n "sample") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)))) (h "03q5r80c7iil3r5p1ylz1lgyidx7gyhr22d0zql973vyhwkgvs2f")))

(define-public crate-sound_stream-0.3.4 (c (n "sound_stream") (v "0.3.4") (d (list (d (n "num") (r "*") (d #t) (k 0)) (d (n "portaudio") (r "*") (d #t) (k 0)) (d (n "sample") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)))) (h "020mn7s4wqb9cc0nrzipbamwxbaycb5wa7gs6w8kr052zj8ch602")))

(define-public crate-sound_stream-0.3.5 (c (n "sound_stream") (v "0.3.5") (d (list (d (n "num") (r "*") (d #t) (k 0)) (d (n "portaudio") (r "*") (d #t) (k 0)) (d (n "sample") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)))) (h "1jb6ki7wypllapyz11a5qvqgqnzbyj3c6lpy0xfmmvghn6svsya2")))

(define-public crate-sound_stream-0.4.0 (c (n "sound_stream") (v "0.4.0") (d (list (d (n "num") (r "*") (d #t) (k 0)) (d (n "portaudio") (r "*") (d #t) (k 0)) (d (n "sample") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)))) (h "11laj7dv4a2qwqizbl9nkgcnmc946w32llryily4dza5fh7ilwhr")))

(define-public crate-sound_stream-0.4.1 (c (n "sound_stream") (v "0.4.1") (d (list (d (n "num") (r "*") (d #t) (k 0)) (d (n "portaudio") (r "*") (d #t) (k 0)) (d (n "sample") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)))) (h "0dq7p4g39fbhlspggg4fbl1757zfrrsqhabjhmc1wjv9042z4iig")))

(define-public crate-sound_stream-0.4.2 (c (n "sound_stream") (v "0.4.2") (d (list (d (n "num") (r "*") (k 0)) (d (n "portaudio") (r "*") (d #t) (k 0)) (d (n "sample") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)))) (h "0yi1qich82m632brpsvwqvd25g2j5s2ivm56a9nhh2g176z0101l")))

(define-public crate-sound_stream-0.4.3 (c (n "sound_stream") (v "0.4.3") (d (list (d (n "num") (r "*") (k 0)) (d (n "portaudio") (r "*") (d #t) (k 0)) (d (n "sample") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)))) (h "1kb4g62d1ci8m2jv270cf7ls42pcf2p909xapx6qs1dj1sda3c90")))

(define-public crate-sound_stream-0.4.4 (c (n "sound_stream") (v "0.4.4") (d (list (d (n "num") (r "*") (k 0)) (d (n "portaudio") (r "*") (d #t) (k 0)) (d (n "sample") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)))) (h "0ak5nz41dcnqr20iddk0aial7ibl42capc3mi92fjc1d9zfqp6pg")))

(define-public crate-sound_stream-0.5.0 (c (n "sound_stream") (v "0.5.0") (d (list (d (n "num") (r "^0.1.27") (k 0)) (d (n "portaudio") (r "^0.4.19") (d #t) (k 0)) (d (n "sample") (r "^0.2.0") (d #t) (k 0)) (d (n "time") (r "^0.1.32") (d #t) (k 0)))) (h "1flv280n9g3rvgp8788iqx82vla3h3ah52zxhpaynv00b8wn1l90")))

(define-public crate-sound_stream-0.6.0 (c (n "sound_stream") (v "0.6.0") (d (list (d (n "num") (r "^0.1.27") (k 0)) (d (n "portaudio") (r "^0.5.1") (d #t) (k 0)) (d (n "sample") (r "^0.2.0") (d #t) (k 0)) (d (n "time") (r "^0.1.32") (d #t) (k 0)))) (h "0v25q87bk6hnk79l0mp7qpjjv1p7z42n76mbwpmqxlfp68zm1mb6")))

