(define-module (crates-io so un soundcloud) #:use-module (crates-io))

(define-public crate-soundcloud-0.1.0 (c (n "soundcloud") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.3.3") (d #t) (k 0)) (d (n "hyper") (r "^0.9.4") (d #t) (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "serde") (r "^0.7.0") (d #t) (k 0)) (d (n "serde_json") (r "^0.7.0") (d #t) (k 0)) (d (n "serde_macros") (r "^0.7.2") (d #t) (k 0)) (d (n "url") (r "^1.0.0") (d #t) (k 0)))) (h "0s7xqfjavlddlvwdhhgmz6sf53dydmlxp3cb8ji1kyfw919yps72")))

(define-public crate-soundcloud-0.3.0 (c (n "soundcloud") (v "0.3.0") (d (list (d (n "env_logger") (r "^0.3.3") (d #t) (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^1.7") (d #t) (k 0)))) (h "092jd0f5v8dggr2gy2zp4w1mvmi20pd0vhz62mf8y8696zk5j3iv")))

(define-public crate-soundcloud-0.4.0 (c (n "soundcloud") (v "0.4.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("json" "stream"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros" "rt-threaded" "fs"))) (d #t) (k 2)) (d (n "tokio-util") (r "~0.3.0") (f (quote ("compat"))) (d #t) (k 2)) (d (n "url") (r "^2.1") (d #t) (k 0)))) (h "1cmffcxy77bhhgilxmpf1mab60i1hqb5cf5sr45pamcf29c73nz9")))

