(define-module (crates-io so un soundex-rs) #:use-module (crates-io))

(define-public crate-soundex-rs-0.1.0 (c (n "soundex-rs") (v "0.1.0") (h "04dqy5956biqxdix1gwcx3qqr3w94i7iqgqx9d1y6km8jlgp2f8z") (f (quote (("full") ("default"))))))

(define-public crate-soundex-rs-0.1.1 (c (n "soundex-rs") (v "0.1.1") (h "1v8hlkv8p66mflnk3gnpx60xkvajc1amyngml4cn751f2gnhrpvm") (f (quote (("full") ("default"))))))

(define-public crate-soundex-rs-0.1.2 (c (n "soundex-rs") (v "0.1.2") (h "0vd4v6m0gg6bk4hpgdn36ilvk9xbsgk1qab4zsbfdy38rzd1wrbg") (f (quote (("full") ("default"))))))

(define-public crate-soundex-rs-0.1.3 (c (n "soundex-rs") (v "0.1.3") (h "15a3kpd9ifnj1027nr11225zvyq7k54md7iq4079q7fasrk0y6d1") (f (quote (("full") ("default"))))))

(define-public crate-soundex-rs-0.1.4 (c (n "soundex-rs") (v "0.1.4") (h "0sq5hldfm3zbgdrpvrs8qpvbfjkf77hp62rj9pk9alyw350swxci") (f (quote (("full") ("default"))))))

(define-public crate-soundex-rs-0.1.5 (c (n "soundex-rs") (v "0.1.5") (h "1h870gph54x1l3bf2lb2yzsdfjpxjxichh6qfzrs5lfa6lmkiiiz") (f (quote (("full") ("default"))))))

(define-public crate-soundex-rs-0.1.6 (c (n "soundex-rs") (v "0.1.6") (h "01kwg3hqv5ny29n60m2makvqsg9wnzjy8mimb236r0zmqdkn8grf") (f (quote (("full") ("default"))))))

(define-public crate-soundex-rs-0.1.7 (c (n "soundex-rs") (v "0.1.7") (h "0rxd59j3aqpghpdd7qpwnyz12d4qs66zg1xp7zqa7mdway9nf25w") (f (quote (("full") ("default"))))))

(define-public crate-soundex-rs-0.1.8 (c (n "soundex-rs") (v "0.1.8") (h "1x8p7nhl3y5k92460i5qg5wh6splpmlqmz70m3l95xibx02gjzhw") (f (quote (("full") ("default"))))))

