(define-module (crates-io so un soundex) #:use-module (crates-io))

(define-public crate-soundex-0.1.0 (c (n "soundex") (v "0.1.0") (h "0r336c4z34wq75xmqlmzr3mcskk21pqqvhn5mjy67hp4iwmhk108")))

(define-public crate-soundex-0.2.0 (c (n "soundex") (v "0.2.0") (d (list (d (n "itertools") (r "^0.6.0") (d #t) (k 0)))) (h "1313zi87bd4kjbygkgwvg4x23vizcja6qiagkb3w8w7a0s6qbcjc")))

