(define-module (crates-io so un sounding-base) #:use-module (crates-io))

(define-public crate-sounding-base-0.0.1 (c (n "sounding-base") (v "0.0.1") (d (list (d (n "chrono") (r "^0.3") (d #t) (k 0)) (d (n "error-chain") (r "^0.10") (d #t) (k 0)))) (h "14b6rgvmf7wba8jaqwmvxmr34bzdy7p8ybsca09k27nl77mdnnlh")))

(define-public crate-sounding-base-0.0.2 (c (n "sounding-base") (v "0.0.2") (d (list (d (n "chrono") (r "^0.3") (d #t) (k 0)) (d (n "error-chain") (r "^0.10") (d #t) (k 0)))) (h "0j4isy2p11zhh5szw4565krm8pnyqhprmcs1zzgjai1z68cp0msq")))

(define-public crate-sounding-base-0.0.3 (c (n "sounding-base") (v "0.0.3") (d (list (d (n "chrono") (r "^0.3") (d #t) (k 0)) (d (n "error-chain") (r "^0.10") (d #t) (k 0)))) (h "1952wxr8w7nr4jck8j6nwvjm31hlmvpr33nkai8dzcbavi36facb")))

(define-public crate-sounding-base-0.0.4 (c (n "sounding-base") (v "0.0.4") (d (list (d (n "chrono") (r "^0.3") (d #t) (k 0)) (d (n "error-chain") (r "^0.10") (d #t) (k 0)))) (h "0n24k5dij69daiab225ba48i8k4srcwhrl01fj3rcpb6wbx5c5sb")))

(define-public crate-sounding-base-0.0.5 (c (n "sounding-base") (v "0.0.5") (d (list (d (n "chrono") (r "^0.3") (d #t) (k 0)) (d (n "error-chain") (r "^0.11") (d #t) (k 0)))) (h "07gdwrcbm6p74pla32k9f5wgnkwlshfa0ia9p5bg1r5kricr7vw0")))

(define-public crate-sounding-base-0.0.6 (c (n "sounding-base") (v "0.0.6") (d (list (d (n "chrono") (r "^0.3") (d #t) (k 0)) (d (n "error-chain") (r "^0.11") (d #t) (k 0)))) (h "08v1nkkk6zirszi610bpfgqmhmsib6g2hryyvi0crl9fb1dk0fdl")))

(define-public crate-sounding-base-0.0.7 (c (n "sounding-base") (v "0.0.7") (d (list (d (n "chrono") (r "^0.3") (d #t) (k 0)) (d (n "error-chain") (r "^0.11") (d #t) (k 0)))) (h "06q6mrvw4w4izk3bbyzl37r0zaw8zqp41vhp2simya4m7mwmm8zx")))

(define-public crate-sounding-base-0.0.8 (c (n "sounding-base") (v "0.0.8") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "error-chain") (r "^0.11") (d #t) (k 0)))) (h "18pv00n3dlm9rj6dhw3z8qcqm0b2kpdgr6qlqhimbxhcdmpak6l5")))

(define-public crate-sounding-base-0.0.9 (c (n "sounding-base") (v "0.0.9") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "error-chain") (r "^0.11") (d #t) (k 0)))) (h "1zrp8v258lcffw9pk3h1vlyg2blk6lj7ls6phv0v01ibidp3lwwl")))

(define-public crate-sounding-base-0.0.10 (c (n "sounding-base") (v "0.0.10") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "error-chain") (r "^0.11") (d #t) (k 0)))) (h "0mszswzsvg9rw38mc1xf36hxg71gfrc1zmfv46iasy7cpjjisck4")))

(define-public crate-sounding-base-0.0.11 (c (n "sounding-base") (v "0.0.11") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "error-chain") (r "^0.11") (d #t) (k 0)))) (h "1pn689f6dlvk6771gddj0vx5l6k1axj85sv2pw6adi2x9d39hkmy")))

(define-public crate-sounding-base-0.0.12 (c (n "sounding-base") (v "0.0.12") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "error-chain") (r "^0.11") (d #t) (k 0)))) (h "1ca1kinkpc6vg6wrv6ds5yg7x37p59pd1d2xfmfjf430rqwm94nb")))

(define-public crate-sounding-base-0.0.13 (c (n "sounding-base") (v "0.0.13") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "error-chain") (r "^0.11") (d #t) (k 0)))) (h "1b307q10z6w35cayj7s7xdswh4jg5c4lwwp81z91yhlspapkx8nl")))

(define-public crate-sounding-base-0.0.14 (c (n "sounding-base") (v "0.0.14") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "1rk3jbfpkm5djv354nwx34v23khg6m7s4rkkql8w8n07va55qka8")))

(define-public crate-sounding-base-0.0.15 (c (n "sounding-base") (v "0.0.15") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "1hrn8xffbqxpr1jc1sqqqmh9q4sf9rckrlxqqr49zqz95s2s1b4g")))

(define-public crate-sounding-base-0.1.0 (c (n "sounding-base") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "088cbn88x7gn0a0v1jmpql87dcixkjdhp4k954cbx4g1v50dj00g")))

(define-public crate-sounding-base-0.2.0 (c (n "sounding-base") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "0nyy1j9vzdpm2c51spydyz1gzw5bnlpdrvz7q7r9kig2vr4x8s01")))

(define-public crate-sounding-base-0.3.0 (c (n "sounding-base") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "metfor") (r "^0.2.0") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.0") (d #t) (k 0)))) (h "02i6qnl2k5gm6s32h9rpq0l42q99whx85qprni9qnr5dk7az5ywb")))

(define-public crate-sounding-base-0.4.0 (c (n "sounding-base") (v "0.4.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "metfor") (r "^0.2.0") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.0") (d #t) (k 0)))) (h "170qwl11hv00aamy7zyk1b2gz8ng59bfxizq67rqnka15h2fq95q")))

(define-public crate-sounding-base-0.5.0 (c (n "sounding-base") (v "0.5.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "metfor") (r "^0.2.0") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.0") (d #t) (k 0)))) (h "1kn8avpz7is8h3dx8zzsggpwlvmw6wiky88v9y8nh2cjqq9qrvl7")))

(define-public crate-sounding-base-0.5.1 (c (n "sounding-base") (v "0.5.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "metfor") (r "^0.2.0") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.0") (d #t) (k 0)))) (h "0xwi33ql07qwlzck7cms03qq9z418k7w8bslrc0c84xnp4fdcvxk")))

(define-public crate-sounding-base-0.5.2 (c (n "sounding-base") (v "0.5.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "metfor") (r "^0.2.0") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.0") (d #t) (k 0)))) (h "1js8lw6k4p34p3a32rfqj6fnk9b1fjha7ynxvqfr2l4ma1acp6hx")))

(define-public crate-sounding-base-0.6.0 (c (n "sounding-base") (v "0.6.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "metfor") (r "^0.2") (d #t) (k 0)) (d (n "smallvec") (r "^0.6") (d #t) (k 0)))) (h "1jspapzm7w3qq66bs1m8bvz6247l7wrgs06qfy42djjq0hy98vj3")))

(define-public crate-sounding-base-0.7.0 (c (n "sounding-base") (v "0.7.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "metfor") (r "^0.2") (d #t) (k 0)) (d (n "optional") (r "^0.4.1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6") (d #t) (k 0)))) (h "0i0f2hfsgzdi1gwdjwxx1gp4xlh4h3hrzlc33hxrskr9xgq0yqan")))

(define-public crate-sounding-base-0.7.1 (c (n "sounding-base") (v "0.7.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "metfor") (r "^0.3") (d #t) (k 0)) (d (n "optional") (r "^0.4.1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6") (d #t) (k 0)))) (h "1q6gzwxkim3d68x929s51q55x0f8h8q2wmck1z0l01i70bqbrlzb")))

(define-public crate-sounding-base-0.8.0 (c (n "sounding-base") (v "0.8.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "metfor") (r "^0.4") (d #t) (k 0)) (d (n "optional") (r "^0.4.3") (d #t) (k 0)))) (h "1shgh4lpak0v3qnvmaqdhi9fxygwsw41awbzsng1xwc0p8ffnh48")))

(define-public crate-sounding-base-0.8.1 (c (n "sounding-base") (v "0.8.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "metfor") (r "^0.4") (d #t) (k 0)) (d (n "optional") (r "^0.5.0") (d #t) (k 0)))) (h "06g9111rv7apyda8zis279igy068icma4cjwhbv3lp4wiz6kp593")))

(define-public crate-sounding-base-0.9.0 (c (n "sounding-base") (v "0.9.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "metfor") (r "^0.5") (f (quote ("use_optional"))) (d #t) (k 0)) (d (n "optional") (r "^0.5.0") (d #t) (k 0)))) (h "051k0ypdp2bsi2ap164h4dkp6c3crpvmqnj4f9czl4p1prfrkd7h")))

(define-public crate-sounding-base-0.9.1 (c (n "sounding-base") (v "0.9.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "metfor") (r "^0.5") (f (quote ("use_optional"))) (d #t) (k 0)) (d (n "optional") (r "^0.5.0") (d #t) (k 0)))) (h "08wi9ylwn86djmpbb0xksajnl72y0qajb00fqx2g88ypaqn9j2ld")))

(define-public crate-sounding-base-0.9.2 (c (n "sounding-base") (v "0.9.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "metfor") (r "^0.6") (f (quote ("use_optional"))) (d #t) (k 0)) (d (n "optional") (r "^0.5.0") (d #t) (k 0)))) (h "0gaanjq5xzp7c5hhaa1llwixpdacp5aja0bwd2hx6i70n6mw773p")))

(define-public crate-sounding-base-0.10.0 (c (n "sounding-base") (v "0.10.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "metfor") (r "^0.7") (f (quote ("use_optional"))) (d #t) (k 0)) (d (n "optional") (r "^0.5.0") (d #t) (k 0)))) (h "07kg7hnja9xmj5nfp2r1abcvjzj9sz2q4dd47qf4lcs04by7w894")))

(define-public crate-sounding-base-0.11.0 (c (n "sounding-base") (v "0.11.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "metfor") (r "^0.7") (f (quote ("use_optional"))) (d #t) (k 0)) (d (n "optional") (r "^0.5.0") (d #t) (k 0)))) (h "0qd4j7jw70682wxldfiph56midfvh6m9lwvhfkg5sl3qrkpz8ip8")))

(define-public crate-sounding-base-0.11.1 (c (n "sounding-base") (v "0.11.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "metfor") (r "^0.7") (f (quote ("use_optional"))) (d #t) (k 0)) (d (n "optional") (r "^0.5.0") (d #t) (k 0)))) (h "0911qgfdpc0mb0fp1pisdgaq4w4785q0gxglfw3vn6n1shk4iwp0")))

