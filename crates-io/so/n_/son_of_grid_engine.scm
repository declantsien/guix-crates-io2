(define-module (crates-io so n_ son_of_grid_engine) #:use-module (crates-io))

(define-public crate-son_of_grid_engine-0.0.1 (c (n "son_of_grid_engine") (v "0.0.1") (d (list (d (n "num_cpus") (r "^1.7.0") (d #t) (k 0)))) (h "0iwhn1m4fg1s3sidk6rxzwdrmih83a2n65vflvr5bhbc06dwwba0")))

(define-public crate-son_of_grid_engine-0.1.0 (c (n "son_of_grid_engine") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num_cpus") (r "^1.7") (d #t) (k 0)) (d (n "threadpool") (r "^1.7") (d #t) (k 0)))) (h "00wc05s56bq6p7awlmh8svfdljw5zkrxfinz6b5agh339fdk4s3f")))

(define-public crate-son_of_grid_engine-0.2.0 (c (n "son_of_grid_engine") (v "0.2.0") (d (list (d (n "file") (r "^1.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num_cpus") (r "^1.7") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "threadpool") (r "^1.7") (d #t) (k 0)))) (h "0p8fs36m4j60n5gk0hz12jr6s7062w9jxnjh8852cb0xlhwymdly")))

