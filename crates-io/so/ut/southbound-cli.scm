(define-module (crates-io so ut southbound-cli) #:use-module (crates-io))

(define-public crate-southbound-cli-0.0.1 (c (n "southbound-cli") (v "0.0.1") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "dirs") (r "^3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3") (d #t) (k 0)))) (h "15lyjgjgf8j2y40c983hrcqwz3mggz8gkz3vpi2blm0x4nscrar6")))

