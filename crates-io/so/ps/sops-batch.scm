(define-module (crates-io so ps sops-batch) #:use-module (crates-io))

(define-public crate-sops-batch-0.4.0 (c (n "sops-batch") (v "0.4.0") (d (list (d (n "clap") (r "^3.2.23") (f (quote ("derive" "env" "unicode"))) (d #t) (k 0)) (d (n "clap_complete_command") (r "^0.3.4") (d #t) (k 0)) (d (n "exitcode") (r "^1.1.2") (d #t) (k 0)) (d (n "self_update") (r "^0.35.0") (f (quote ("rustls"))) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.2") (d #t) (k 0)) (d (n "which") (r "^4.2.5") (d #t) (k 0)))) (h "0v3igxqn9jlk6c4h1069rks8rga2alq7030h4p5j1gksdw1rn9dp")))

