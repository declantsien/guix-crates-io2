(define-module (crates-io so ta sotassl-sys-extras) #:use-module (crates-io))

(define-public crate-sotassl-sys-extras-0.7.14 (c (n "sotassl-sys-extras") (v "0.7.14") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "openssl-sys") (r "^0.7.14") (d #t) (k 0)))) (h "1bahl48snx0qc7w6v4fmcni6pi5r91z2cl01c5cbx8k9hh1lnsvp") (f (quote (("ecdh_auto")))) (y #t)))

(define-public crate-sotassl-sys-extras-0.7.1400 (c (n "sotassl-sys-extras") (v "0.7.1400") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "sotassl-sys") (r "^0.7.1400") (d #t) (k 0)))) (h "12ikmniiigk0jymfxwq9lxqj8my6l6vn18fxhka0vaxq5i29fy3s") (f (quote (("ecdh_auto")))) (y #t)))

