(define-module (crates-io so nc soncai) #:use-module (crates-io))

(define-public crate-soncai-0.0.1 (c (n "soncai") (v "0.0.1") (d (list (d (n "async-trait") (r "^0.1.42") (d #t) (k 0)) (d (n "futures") (r "^0.3.8") (f (quote ("compat" "thread-pool"))) (d #t) (k 0)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)) (d (n "tokio") (r "^1.0.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1nslrig3zyrqgraza73is1lcsvifn7icm0cb4f34a9fz29bc8prf")))

