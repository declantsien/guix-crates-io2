(define-module (crates-io so lv solvent) #:use-module (crates-io))

(define-public crate-solvent-0.1.0 (c (n "solvent") (v "0.1.0") (d (list (d (n "log") (r "*") (d #t) (k 0)))) (h "1bk7wxfpj2d99lpbxp48bxxbwpzi2a10sx3fycdky8vlmzscw179")))

(define-public crate-solvent-0.2.0 (c (n "solvent") (v "0.2.0") (d (list (d (n "log") (r "*") (d #t) (k 0)))) (h "0w3w5i0gxndx54pvzwd20xbyc80i327p4y71hcmbfpwis5hk96cm")))

(define-public crate-solvent-0.5.0 (c (n "solvent") (v "0.5.0") (h "1m02wwffszwn6z18wyzj7c09ar1rcxavgy6bi23m01b26kwk6ww8")))

(define-public crate-solvent-0.5.1 (c (n "solvent") (v "0.5.1") (h "0kas29zk4vqnvgw83d8cd7iiax32yr6ij9vs41whfhxa6aqva3ik")))

(define-public crate-solvent-0.5.2 (c (n "solvent") (v "0.5.2") (d (list (d (n "log") (r "*") (d #t) (k 0)))) (h "054qs61n46qz3100714jx5h35bvq93v9kj9ycg0h27b1xcwfc73l")))

(define-public crate-solvent-0.5.3 (c (n "solvent") (v "0.5.3") (d (list (d (n "log") (r "*") (d #t) (k 0)))) (h "0w3f2wyfkf4c1rzpwcxw9w3g1aapja2p5dqa9xhikhiycjvpvdss")))

(define-public crate-solvent-0.5.4 (c (n "solvent") (v "0.5.4") (d (list (d (n "log") (r "*") (d #t) (k 0)))) (h "04h200dml9rnq0wmfncjgp7pj7fcmlbbiig4qs1akjj4h92zgbwv")))

(define-public crate-solvent-0.5.5 (c (n "solvent") (v "0.5.5") (d (list (d (n "log") (r "*") (d #t) (k 0)))) (h "1m56jbx2mj1rj93bnk1azqa1bvzhlsxcga2x1rjkm1sk0fxrqcv6")))

(define-public crate-solvent-0.5.6 (c (n "solvent") (v "0.5.6") (d (list (d (n "log") (r "*") (d #t) (k 0)))) (h "0a4xzl7gbif4sk8dh1s87pys6rh51xgk2qr89ncn5xvvkfr8m851")))

(define-public crate-solvent-0.5.7 (c (n "solvent") (v "0.5.7") (d (list (d (n "log") (r "*") (d #t) (k 0)))) (h "1mx33s83x17fvh86rvhywqqvhwjl0vz23jmpm1j11cg7x99qdlzl")))

(define-public crate-solvent-0.5.8 (c (n "solvent") (v "0.5.8") (d (list (d (n "log") (r "*") (d #t) (k 0)))) (h "1p5pzpv9580x6iwxwg0k4j4dbqss334q0jiyknnqy71i2j17kz5y")))

(define-public crate-solvent-0.6.0 (c (n "solvent") (v "0.6.0") (d (list (d (n "log") (r "*") (d #t) (k 0)))) (h "0kwr7j6l6m50l9ngwnzyqz5jhxbyks1r14g1wg8743nv06ljqx7z")))

(define-public crate-solvent-0.7.0 (c (n "solvent") (v "0.7.0") (d (list (d (n "log") (r "*") (d #t) (k 0)))) (h "1kbjvk0rkmji75d1k2skxmalmlxvnjfvdkivzwhksrzpf4dhimfk")))

(define-public crate-solvent-0.7.1 (c (n "solvent") (v "0.7.1") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "0d9mylc2j0idbcrpqbi6a9h8gnp5iv2r557hzw0h71h2v7i9d566")))

(define-public crate-solvent-0.8.0 (c (n "solvent") (v "0.8.0") (h "06bxid269krwh97qfhy39bsc771f63h456v7x5fjq3q8gni8dv97")))

(define-public crate-solvent-0.8.1 (c (n "solvent") (v "0.8.1") (h "0vrayw5a1lgzpnfn5abkrapn5lm99aqnb4vs0xc3fdr7f4camwl1")))

(define-public crate-solvent-0.8.2 (c (n "solvent") (v "0.8.2") (h "0xi719gfn7l9an6wmgr9xzh5yqm1wvw9r3z1ia6qfvnypa1l3bc9")))

(define-public crate-solvent-0.8.3 (c (n "solvent") (v "0.8.3") (d (list (d (n "indexmap") (r "^1.7.0") (o #t) (d #t) (k 0)))) (h "05hz8w2yrshywwxkr4h1p2223v3ph8y7cxzrljq9xwj6wnc0398l") (f (quote (("deterministic" "indexmap"))))))

