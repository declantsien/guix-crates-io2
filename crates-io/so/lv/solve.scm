(define-module (crates-io so lv solve) #:use-module (crates-io))

(define-public crate-solve-0.0.1 (c (n "solve") (v "0.0.1") (d (list (d (n "clap") (r "^3.0.0-beta.4") (f (quote ("std" "derive" "color" "suggestions"))) (k 0)) (d (n "rustyline") (r "^9.0.0") (d #t) (k 0)) (d (n "solve_lib") (r "^0.0.1") (d #t) (k 0)))) (h "09id50rp0kj3sdz83a7jg2500dv932qkj9rqsk00nsf9m1wb4vj8")))

