(define-module (crates-io so lv solver) #:use-module (crates-io))

(define-public crate-solver-0.0.1 (c (n "solver") (v "0.0.1") (h "0jisbafjvgxcgf7yy37zyv3vb5dqsrz9gikcq2yzf5r5ia40ckb8") (y #t)))

(define-public crate-solver-0.0.0 (c (n "solver") (v "0.0.0") (h "0260bg4az4c787ra4b1bz0izwvp16indlvcz7w1pd04wqbc7mi9m") (y #t)))

(define-public crate-solver-0.0.2 (c (n "solver") (v "0.0.2") (h "1whbxc1z82ijly4959jnfx0ljhi44vfb7qsav9ingrhcbmyx646c") (y #t)))

(define-public crate-solver-0.0.3-alpha.0 (c (n "solver") (v "0.0.3-alpha.0") (h "0szqx0a0fqlwbwq851h6gijg2j305kp9r0pwi6y3wc521cdc9lc9")))

(define-public crate-solver-0.0.3-alpha.1 (c (n "solver") (v "0.0.3-alpha.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "04flvg9cg7x4zrvnxd483dysxmsvvjdcvimpvlpg21xjc1hdlf1k")))

(define-public crate-solver-0.0.3-alpha.2 (c (n "solver") (v "0.0.3-alpha.2") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "01z6aid2ah4n299ib8q0i0r18ij8zdjdqs7lf5q68136447h9h4f")))

