(define-module (crates-io so lv solved) #:use-module (crates-io))

(define-public crate-solved-0.1.0 (c (n "solved") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "cached") (r "^0.23.0") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "html2text") (r "^0.2.1") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "lru") (r "^0.6.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.2") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.124") (f (quote ("derive"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "1l1bvrnf55yygh2yprmakn79flwwi3288b6iz8f6p47kal7wbsnk") (y #t)))

