(define-module (crates-io so lv solve_string) #:use-module (crates-io))

(define-public crate-solve_string-0.1.0 (c (n "solve_string") (v "0.1.0") (h "1bw8h2j1k9j7p30cgwnzq5hpci9fmgisb0ig3l1dfl9qqhl5aazx")))

(define-public crate-solve_string-0.1.1 (c (n "solve_string") (v "0.1.1") (h "19nwm0ya6irzvjb3rjgg9bzq1gsj4an629whwzyxvc365axjr6js")))

