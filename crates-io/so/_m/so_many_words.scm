(define-module (crates-io so _m so_many_words) #:use-module (crates-io))

(define-public crate-so_many_words-0.0.1 (c (n "so_many_words") (v "0.0.1") (h "1rbv5v0z9499c1v7rf24z2cl38h1nd1s24qxki1shwnk574xv1p7") (y #t)))

(define-public crate-so_many_words-0.0.2 (c (n "so_many_words") (v "0.0.2") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "radix_trie") (r "^0.2.0") (d #t) (k 0)))) (h "05bjjx94vjd4jiqr993hshpga2v3v1yyxgf93rvjwvwm72q5vncl") (y #t)))

(define-public crate-so_many_words-0.0.3 (c (n "so_many_words") (v "0.0.3") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "radix_trie") (r "^0.2.0") (d #t) (k 0)))) (h "0ix4hhwrw2sayg07wa3lp1cqq691331zrjras54z7f9jdavfww58") (y #t)))

(define-public crate-so_many_words-0.0.4 (c (n "so_many_words") (v "0.0.4") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "radix_trie") (r "^0.2.0") (d #t) (k 0)))) (h "19ysyc22d0rxmwj44sfm5fk1c4iyrbpm81lz4ib9rjdvldvmjm22") (y #t)))

(define-public crate-so_many_words-0.0.5 (c (n "so_many_words") (v "0.0.5") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "radix_trie") (r "^0.2.0") (d #t) (k 0)))) (h "1dlwk1n9j7x8zkb8dl66yshpfhiap0lqh61xbsvpfsqf2impshvv") (y #t)))

(define-public crate-so_many_words-0.0.6 (c (n "so_many_words") (v "0.0.6") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "radix_trie") (r "^0.2.0") (d #t) (k 0)))) (h "0h4gy0pi7ayx42j44cnzx2ri7ncq3gdgq1bns580swvw1c6h5nml") (y #t)))

(define-public crate-so_many_words-0.0.7 (c (n "so_many_words") (v "0.0.7") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "radix_trie") (r "^0.2.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "01h2xjswpg4sr9i57mhx82lkqmmc7pki8x8r9k74jvp727sjcsvh") (y #t)))

(define-public crate-so_many_words-0.0.8 (c (n "so_many_words") (v "0.0.8") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "radix_trie") (r "^0.2.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1fr4j5zfcc7vn8vbxgwr5v7n03mfzjlc00kzwsh4bns8kjxcnpnz") (y #t)))

(define-public crate-so_many_words-0.1.0 (c (n "so_many_words") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "radix_trie") (r "^0.2.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1clyn098nsyps83bczy5mmpni0wcyirs5rzzq4x4rgqqpbra6ghr") (y #t)))

(define-public crate-so_many_words-0.1.1 (c (n "so_many_words") (v "0.1.1") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "radix_trie") (r "^0.2.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1n558796ig7lvgxbqgmz9m0mvw41pjnxwjd7a0jyfjmb2843lnh8") (y #t)))

(define-public crate-so_many_words-0.1.2 (c (n "so_many_words") (v "0.1.2") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "radix_trie") (r "^0.2.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0vxgb46gxdy1pfr6hivjdkjgrlrj7m8pln2nf686dkbbd3mqk3pw") (y #t)))

(define-public crate-so_many_words-0.1.3 (c (n "so_many_words") (v "0.1.3") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "radix_trie") (r "^0.2.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "01psna67z4ksf91panz3ij27ngla6vgar2n73rdammprxg1hmlzx") (y #t)))

(define-public crate-so_many_words-0.1.5 (c (n "so_many_words") (v "0.1.5") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "radix_trie") (r "^0.2.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "rust-stemmers") (r "^1.2") (d #t) (k 0)))) (h "0m9ydrxcm0b39n8b0i69nxmnpjyn8a3lz7m63pvdjwvj80ym2jci") (y #t)))

(define-public crate-so_many_words-0.1.6 (c (n "so_many_words") (v "0.1.6") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "radix_trie") (r "^0.2.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "rust-stemmers") (r "^1.2") (d #t) (k 0)))) (h "16miwfb0kk18bi42xiqd5aqmaw2x11d85cmn9500w5156arqh9kb") (y #t)))

(define-public crate-so_many_words-0.1.8 (c (n "so_many_words") (v "0.1.8") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "radix_trie") (r "^0.2.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "rust-stemmers") (r "^1.2") (d #t) (k 0)) (d (n "whatlang") (r "^0.9.0") (d #t) (k 0)))) (h "01bzmmb5qyqlic1vjd3hpv13sc48212lwhskimf6r5dxx4xkqcns") (y #t)))

(define-public crate-so_many_words-0.1.9 (c (n "so_many_words") (v "0.1.9") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "eudex") (r "^0.1.1") (d #t) (k 0)) (d (n "radix_trie") (r "^0.2.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "rust-stemmers") (r "^1.2") (d #t) (k 0)) (d (n "whatlang") (r "^0.9.0") (d #t) (k 0)))) (h "0q6vzlni6h6nvx7z95qn84wirc3sc7kzi2r6grj0bs41qld9s5j3") (y #t)))

(define-public crate-so_many_words-0.1.10 (c (n "so_many_words") (v "0.1.10") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "eudex") (r "^0.1.1") (d #t) (k 0)) (d (n "fst") (r "^0.4") (d #t) (k 0)) (d (n "radix_trie") (r "^0.2.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "rust-stemmers") (r "^1.2") (d #t) (k 0)) (d (n "whatlang") (r "^0.9.0") (d #t) (k 0)))) (h "0r29jqx9bwl5p84aak015syxfa9bmgy15njbfpx320p24xqqqpzx") (y #t)))

(define-public crate-so_many_words-0.1.11 (c (n "so_many_words") (v "0.1.11") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "eudex") (r "^0.1.1") (d #t) (k 0)) (d (n "fst") (r "^0.4") (f (quote ("levenshtein"))) (d #t) (k 0)) (d (n "memmap") (r "^0.7") (d #t) (k 0)) (d (n "radix_trie") (r "^0.2.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "rust-stemmers") (r "^1.2") (d #t) (k 0)) (d (n "whatlang") (r "^0.9.0") (d #t) (k 0)))) (h "1k52c9v0j1h3x753q4crhvir2fbml8i3swxk4z5j0sahijy3bbpv") (y #t)))

(define-public crate-so_many_words-0.1.12 (c (n "so_many_words") (v "0.1.12") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "eudex") (r "^0.1.1") (d #t) (k 0)) (d (n "fst") (r "^0.4") (f (quote ("levenshtein"))) (d #t) (k 0)) (d (n "memmap") (r "^0.7") (d #t) (k 0)) (d (n "radix_trie") (r "^0.2.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "rust-stemmers") (r "^1.2") (d #t) (k 0)) (d (n "whatlang") (r "^0.9.0") (d #t) (k 0)))) (h "02j2q91k2jkl6zmc0pmz2anp81p7y0yccfsq4mqrysp1sywq3lsy") (y #t)))

