(define-module (crates-io so ko sokoban-rs) #:use-module (crates-io))

(define-public crate-sokoban-rs-1.0.0 (c (n "sokoban-rs") (v "1.0.0") (d (list (d (n "bitflags") (r "^0.3.2") (d #t) (k 0)) (d (n "clap") (r "^1.3.0") (d #t) (k 0)) (d (n "sdl2") (r "^0.8.0") (d #t) (k 0)) (d (n "sdl2_image") (r "^0.2.5") (d #t) (k 0)) (d (n "sdl2_ttf") (r "^0.6.1") (d #t) (k 0)) (d (n "xml-rs") (r "^0.1.25") (d #t) (k 0)))) (h "129dlvxy3qhqykw3rfgzc5kjv14q23l8hxcpfkls4qkydm2rwahk")))

(define-public crate-sokoban-rs-1.0.1 (c (n "sokoban-rs") (v "1.0.1") (d (list (d (n "bitflags") (r "^0.3.2") (d #t) (k 0)) (d (n "clap") (r "^1.3.0") (d #t) (k 0)) (d (n "sdl2") (r "^0.8.0") (d #t) (k 0)) (d (n "sdl2_image") (r "^0.2.5") (d #t) (k 0)) (d (n "sdl2_ttf") (r "^0.6.1") (d #t) (k 0)) (d (n "xml-rs") (r "^0.1.25") (d #t) (k 0)))) (h "1iq5ylxipz59lqf7wq9xs7xfaa2j1bm0lbd6kl1lc99p85z3jpv0")))

(define-public crate-sokoban-rs-1.0.2 (c (n "sokoban-rs") (v "1.0.2") (d (list (d (n "bitflags") (r "^0.3.2") (d #t) (k 0)) (d (n "clap") (r "*") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "sdl2") (r "^0.8.0") (d #t) (k 0)) (d (n "sdl2_image") (r "^0.2.5") (d #t) (k 0)) (d (n "sdl2_ttf") (r "^0.6.1") (d #t) (k 0)) (d (n "xml-rs") (r "^0.1.25") (d #t) (k 0)))) (h "07d25pi3gdp3aiyvz885pz7vmxrz3fzvrs3rcr9zx5rak46q7yyq")))

(define-public crate-sokoban-rs-1.0.3 (c (n "sokoban-rs") (v "1.0.3") (d (list (d (n "bitflags") (r "^0.3.3") (d #t) (k 0)) (d (n "clap") (r "*") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "sdl2") (r "^0.12.0") (d #t) (k 0)) (d (n "sdl2_image") (r "^0.6.0") (d #t) (k 0)) (d (n "sdl2_ttf") (r "^0.10.0") (d #t) (k 0)) (d (n "xml-rs") (r "^0.2.2") (d #t) (k 0)))) (h "181g4p8hn5i8cv668rba7z58zfhh2xg9a6wqzwp2lfrbqhyfgggr")))

(define-public crate-sokoban-rs-1.0.4 (c (n "sokoban-rs") (v "1.0.4") (d (list (d (n "bitflags") (r "^0.3.3") (d #t) (k 0)) (d (n "clap") (r "^1.5.3") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "sdl2") (r "^0.12.0") (d #t) (k 0)) (d (n "sdl2_image") (r "^0.6.0") (d #t) (k 0)) (d (n "sdl2_ttf") (r "^0.10.0") (d #t) (k 0)) (d (n "xml-rs") (r "^0.2.2") (d #t) (k 0)))) (h "04w3mkcbvhsb6dbig4b4h7dzqa43p1p3ccav9rzpx5p9c9bclql7")))

(define-public crate-sokoban-rs-1.1.0 (c (n "sokoban-rs") (v "1.1.0") (d (list (d (n "bitflags") (r "^0.3.3") (d #t) (k 0)) (d (n "clap") (r "^1.5.3") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "sdl2") (r "^0.16.0") (d #t) (k 0)) (d (n "sdl2_image") (r "^0.16.0") (d #t) (k 0)) (d (n "sdl2_ttf") (r "^0.16.0") (d #t) (k 0)) (d (n "xml-rs") (r "^0.2.2") (d #t) (k 0)))) (h "1szm3g4g3rwch7b912asv25lim4r5bmlsj5yppq52hk41gnps5ir")))

(define-public crate-sokoban-rs-1.2.0 (c (n "sokoban-rs") (v "1.2.0") (d (list (d (n "bitflags") (r "^1.0.3") (d #t) (k 0)) (d (n "clap") (r "^2.31.2") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "sdl2") (r "^0.31") (f (quote ("image" "ttf"))) (k 0)) (d (n "xml-rs") (r "^0.7.0") (d #t) (k 0)))) (h "17lxwzpgdxczhvfp88bw9jfxfw736g9gcfkwma5ysir27sxvs1yw")))

(define-public crate-sokoban-rs-1.2.1 (c (n "sokoban-rs") (v "1.2.1") (d (list (d (n "bitflags") (r "^1.0.3") (d #t) (k 0)) (d (n "clap") (r "^2.31.2") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "sdl2") (r "^0.31") (f (quote ("image" "ttf"))) (k 0)) (d (n "xml-rs") (r "^0.7.0") (d #t) (k 0)))) (h "0570j81d53sci0a8593a1p40vds5zfr4v2zkgxsmi4fz85am1rlz")))

(define-public crate-sokoban-rs-1.2.2 (c (n "sokoban-rs") (v "1.2.2") (d (list (d (n "bitflags") (r "^1.1.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "sdl2") (r "^0.32.2") (f (quote ("image" "ttf"))) (k 0)) (d (n "xml-rs") (r "^0.8.0") (d #t) (k 0)))) (h "0yf2d8p2hxh3xin18ycmff6hl4rpgj817964xbfcaljw2wahvd01")))

