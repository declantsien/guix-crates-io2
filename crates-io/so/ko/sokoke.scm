(define-module (crates-io so ko sokoke) #:use-module (crates-io))

(define-public crate-sokoke-0.0.1 (c (n "sokoke") (v "0.0.1") (h "0crpp7jk1yf6iwjnzyma6q3dfnkl5s382c7janr06v7rfpfb47fs")))

(define-public crate-sokoke-0.0.2 (c (n "sokoke") (v "0.0.2") (d (list (d (n "tokio") (r "^1.33.0") (f (quote ("rt-multi-thread" "time" "net"))) (d #t) (k 0)))) (h "1mfr652rdvqbj36ll5yzypgdin60j00m92vkwa3w8x1jzwy7kra0")))

