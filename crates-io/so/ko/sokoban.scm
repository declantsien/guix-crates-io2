(define-module (crates-io so ko sokoban) #:use-module (crates-io))

(define-public crate-sokoban-0.1.0 (c (n "sokoban") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "arbitrary") (r "^1") (f (quote ("derive_arbitrary"))) (o #t) (d #t) (k 0)) (d (n "libfuzzer-sys") (r "^0.4.2") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "00w4wb6gcadpssxsszf8crjk72264bx4kmqwgvlijni5hxwbszad") (f (quote (("fuzzing" "arbitrary"))))))

(define-public crate-sokoban-0.1.1 (c (n "sokoban") (v "0.1.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "arbitrary") (r "^1") (f (quote ("derive_arbitrary"))) (o #t) (d #t) (k 0)) (d (n "libfuzzer-sys") (r "^0.4.2") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0sgsrwww9yl0i5mz64pgsvvlbc2w4l146jf48dx65kq2p5wrn8fc") (f (quote (("fuzzing" "arbitrary"))))))

(define-public crate-sokoban-0.1.2 (c (n "sokoban") (v "0.1.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "arbitrary") (r "^1") (f (quote ("derive_arbitrary"))) (o #t) (d #t) (k 0)) (d (n "libfuzzer-sys") (r "^0.4.2") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0p2pn9lk7jxnyk2ibmzazvc6nxzymy6nr8njwzm2v60f7svglp32") (f (quote (("fuzzing" "arbitrary"))))))

(define-public crate-sokoban-0.2.0 (c (n "sokoban") (v "0.2.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "arbitrary") (r "^1") (f (quote ("derive_arbitrary"))) (o #t) (d #t) (k 0)) (d (n "libfuzzer-sys") (r "^0.4.2") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1g7vi39sdnkrldl6kd4vj15kqpq50s3vn5d804gf0s87j51s2mng") (f (quote (("fuzzing" "arbitrary"))))))

(define-public crate-sokoban-0.2.1 (c (n "sokoban") (v "0.2.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "arbitrary") (r "^1") (f (quote ("derive_arbitrary"))) (o #t) (d #t) (k 0)) (d (n "libfuzzer-sys") (r "^0.4.2") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "149h8959xslb6j3x2srjj0i7ay6b3canfm638s269gcvrk3rh5xp") (f (quote (("fuzzing" "arbitrary")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-sokoban-0.2.2 (c (n "sokoban") (v "0.2.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "arbitrary") (r "^1") (f (quote ("derive_arbitrary"))) (o #t) (d #t) (k 0)) (d (n "libfuzzer-sys") (r "^0.4.2") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "16av2z56a93zh667f9xsa27ypv0f5syp3dq5gvlps55b0n59qrj0") (f (quote (("fuzzing" "arbitrary")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-sokoban-0.2.3 (c (n "sokoban") (v "0.2.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "arbitrary") (r "^1") (f (quote ("derive_arbitrary"))) (o #t) (d #t) (k 0)) (d (n "libfuzzer-sys") (r "^0.4.2") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1hbdljmz0fw2dh8a55pnkm40jc1b1jrp7s3q105h2bvha1p84nhs") (f (quote (("fuzzing" "arbitrary")))) (s 2) (e (quote (("serde" "dep:serde"))))))

