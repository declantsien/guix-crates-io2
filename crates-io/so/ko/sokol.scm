(define-module (crates-io so ko sokol) #:use-module (crates-io))

(define-public crate-sokol-0.1.0 (c (n "sokol") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "cc") (r "^1.0.25") (d #t) (k 1)))) (h "031njj1aai9y3xkradd005dnjx7qwqyna2y07zgfws90dd47gl2x")))

(define-public crate-sokol-0.2.0 (c (n "sokol") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "cc") (r "^1.0.25") (d #t) (k 1)))) (h "1svhjfq33fvdjwsizdsnjl6rml50mjba3fiz12l44vsvvwapx8hl")))

(define-public crate-sokol-0.3.0 (c (n "sokol") (v "0.3.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "18whmkfch0clqqgncc7nc5djig9nqn3l0np1pwkzqwhpw3gfp5a9")))

