(define-module (crates-io so us sous) #:use-module (crates-io))

(define-public crate-sous-0.1.0 (c (n "sous") (v "0.1.0") (d (list (d (n "clap") (r "^4.0.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.13") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "0nl6wxyfh429pqa6hbw8z3w71sq7372pf06fi8d9nxpc7yzhnvrw")))

(define-public crate-sous-0.2.0 (c (n "sous") (v "0.2.0") (d (list (d (n "clap") (r "^4.0.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.13") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "0bww7x3wc4nawqjc7xl4fy57gb6h2ninbkrp4qm1363pwx3saq8m")))

(define-public crate-sous-0.3.0 (c (n "sous") (v "0.3.0") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)) (d (n "tera") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "06w6i94j0wgf47kdpvjr7rmxi6qcgsjvk0fn3m2vwkss4kfr65ir")))

