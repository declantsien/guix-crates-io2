(define-module (crates-io so ba sobani-tracker) #:use-module (crates-io))

(define-public crate-sobani-tracker-0.1.0 (c (n "sobani-tracker") (v "0.1.0") (d (list (d (n "async-std") (r "^1") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "sha2") (r "^0.8") (d #t) (k 0)))) (h "16lcsi24mfizbk6vbmbwrz83hflw5ajmhjcj0sh32d1ydd6f4g5s")))

