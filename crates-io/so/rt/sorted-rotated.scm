(define-module (crates-io so rt sorted-rotated) #:use-module (crates-io))

(define-public crate-sorted-rotated-0.1.0 (c (n "sorted-rotated") (v "0.1.0") (h "1vi3aar6lnjc7q2prv12mr0g6gm4lk4wr9bqpk9qnyv5prffz811")))

(define-public crate-sorted-rotated-0.1.1 (c (n "sorted-rotated") (v "0.1.1") (h "0ja2lsdjlycws7g9wrc6wq5mf590hyj0k8x083ypzwcmdn8dhq8i")))

