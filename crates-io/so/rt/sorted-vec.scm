(define-module (crates-io so rt sorted-vec) #:use-module (crates-io))

(define-public crate-sorted-vec-0.3.1 (c (n "sorted-vec") (v "0.3.1") (d (list (d (n "serde") (r "1.*") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0i4mk9wb2rd0var0iplk45n5d0psc1m07qrdd0vvvl2d0d5qy2sn")))

(define-public crate-sorted-vec-0.3.2 (c (n "sorted-vec") (v "0.3.2") (d (list (d (n "serde") (r "1.*") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1qlkmqiazba05ll23b0545r665icsys84w06vvwk52g3ampnqrdf")))

(define-public crate-sorted-vec-0.3.3 (c (n "sorted-vec") (v "0.3.3") (d (list (d (n "serde") (r "1.*") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1ndsihd6bc2nmvffj9a6klkrjvg7cp89kis057v0p67d603h3nkl")))

(define-public crate-sorted-vec-0.3.4 (c (n "sorted-vec") (v "0.3.4") (d (list (d (n "serde") (r "1.*") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "13xw7g89mk18ccl5iqkvv0iia8h1s1dy8amzdpj3bi8gpm23avfc")))

(define-public crate-sorted-vec-0.3.5 (c (n "sorted-vec") (v "0.3.5") (d (list (d (n "serde") (r "1.*") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1zkykgvg9bfmfzz2x1zksgmxs1zhnfranr9vq5v4ikqqn5vi0lsf")))

(define-public crate-sorted-vec-0.3.6 (c (n "sorted-vec") (v "0.3.6") (d (list (d (n "serde") (r "1.*") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1gzzjn0vyc17jmb8z6ppvn4hwf0irq1lg9g6yhh9726phwx791bf")))

(define-public crate-sorted-vec-0.3.7 (c (n "sorted-vec") (v "0.3.7") (d (list (d (n "serde") (r "1.*") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1lrlfy5rz006f4x40pfg63x504zzsjsbyz1zr8v5zm4srmcbxkfk")))

(define-public crate-sorted-vec-0.4.0 (c (n "sorted-vec") (v "0.4.0") (d (list (d (n "serde") (r "1.*") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "185nrws78zmw2bazrrdqw386l5yjiy36d62xvafg03106rinvksm")))

(define-public crate-sorted-vec-0.4.1 (c (n "sorted-vec") (v "0.4.1") (d (list (d (n "serde") (r "1.*") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1pkl33n0f9229v71mya9bbv17ln3b892gzyl83c5vrj4qvyz5arn")))

(define-public crate-sorted-vec-0.5.0 (c (n "sorted-vec") (v "0.5.0") (d (list (d (n "serde") (r "1.*") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1gl71qsw2wsdzildrvf9y7sbjdxa59y0s02lz2zmyjz5db89ml5h")))

(define-public crate-sorted-vec-0.5.1 (c (n "sorted-vec") (v "0.5.1") (d (list (d (n "serde") (r "1.*") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1i1l0x83yy2xrxghs63wrd65lhhssgldx3z3sdr41asg50qn1c7i")))

(define-public crate-sorted-vec-0.5.2 (c (n "sorted-vec") (v "0.5.2") (d (list (d (n "serde") (r "1.*") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0cy2rkzh9ignnqs0v7xzaa81qm7405w9rq7bz6b93vg42rrlc9n0")))

(define-public crate-sorted-vec-0.6.2 (c (n "sorted-vec") (v "0.6.2") (d (list (d (n "serde") (r "1.*") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "1.*") (d #t) (k 2)))) (h "1wl5xxzqbb8g5lgn3hk1jql810sv4gw7w9ywpyyw13fb28knhg30") (f (quote (("serde-transparent" "serde"))))))

(define-public crate-sorted-vec-0.7.0 (c (n "sorted-vec") (v "0.7.0") (d (list (d (n "serde") (r "1.*") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "1.*") (d #t) (k 2)))) (h "1an2z3sm8df7saj6f9q4b49ipckgngiz2fi398fhqnhngnhy2422") (f (quote (("serde-nontransparent" "serde"))))))

(define-public crate-sorted-vec-0.8.0 (c (n "sorted-vec") (v "0.8.0") (d (list (d (n "serde") (r "1.*") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "1.*") (d #t) (k 2)))) (h "0rnk120vdyg4lr81mbs28xjqd6b7xxakfbshkp8a8xs1q8vxp3ns") (f (quote (("serde-nontransparent" "serde"))))))

(define-public crate-sorted-vec-0.8.1 (c (n "sorted-vec") (v "0.8.1") (d (list (d (n "serde") (r "1.*") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "1.*") (d #t) (k 2)))) (h "02h36w5znm8vsimjyakc00a2qlgiwahvdpj9p4czbygah7657f1f") (f (quote (("serde-nontransparent" "serde"))))))

(define-public crate-sorted-vec-0.8.2 (c (n "sorted-vec") (v "0.8.2") (d (list (d (n "serde") (r "1.*") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "1.*") (d #t) (k 2)))) (h "1a1rlnj9kf2w7pb0r9whn7pwi3798x0lp2r2dy33h1k60y17n17h") (f (quote (("serde-nontransparent" "serde"))))))

(define-public crate-sorted-vec-0.8.3 (c (n "sorted-vec") (v "0.8.3") (d (list (d (n "serde") (r "1.*") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "1.*") (d #t) (k 2)))) (h "0n3l8v05b2d7yd1arwylx0a6qb4vncps3k7abvfsslbg1fplqwy6") (f (quote (("serde-nontransparent" "serde"))))))

