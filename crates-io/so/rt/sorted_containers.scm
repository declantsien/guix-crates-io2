(define-module (crates-io so rt sorted_containers) #:use-module (crates-io))

(define-public crate-sorted_containers-0.1.0 (c (n "sorted_containers") (v "0.1.0") (h "1s3rcmamnxp0nadz3q71syhv8q8hskqmpypbhl4xxc8h3bic876h") (f (quote (("default"))))))

(define-public crate-sorted_containers-0.1.1 (c (n "sorted_containers") (v "0.1.1") (d (list (d (n "rand") (r "^0.4.2") (d #t) (k 0)))) (h "0q9ih76a3gc4js21ndnl88sqg0p3nmn8wa0qyr1jngs3yw9bhk17") (f (quote (("default"))))))

