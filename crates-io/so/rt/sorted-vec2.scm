(define-module (crates-io so rt sorted-vec2) #:use-module (crates-io))

(define-public crate-sorted-vec2-0.1.0 (c (n "sorted-vec2") (v "0.1.0") (d (list (d (n "is_sorted") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "1.*") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "1.*") (d #t) (k 2)))) (h "1sfr9s6qzdl9iphj442zb5g99kf5pv3w1krrira2bhbyz5v9547a") (f (quote (("serde-nontransparent" "serde") ("default" "serde")))) (s 2) (e (quote (("serde" "dep:serde" "dep:is_sorted"))))))

