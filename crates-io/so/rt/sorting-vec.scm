(define-module (crates-io so rt sorting-vec) #:use-module (crates-io))

(define-public crate-sorting-vec-0.1.0 (c (n "sorting-vec") (v "0.1.0") (h "13jnlr1ffn15ppjmmk8y0wdn643jr8bln7i1a4pavqfjsn8mgly6")))

(define-public crate-sorting-vec-0.1.1 (c (n "sorting-vec") (v "0.1.1") (h "0h9m7ndybijralxi9y4ai8q8b50134b1vn98q4j5w0xf2pw9y2jl")))

