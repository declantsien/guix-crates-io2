(define-module (crates-io so rt sort-visual) #:use-module (crates-io))

(define-public crate-sort-visual-0.0.0 (c (n "sort-visual") (v "0.0.0") (d (list (d (n "image") (r "^0.24.5") (d #t) (k 0)) (d (n "sort-steps") (r "^0.2.2") (d #t) (k 0)))) (h "16c8lly51dnkk5f7ijzsp2gm8z34fyk2qcjridrhy71ijrv7krfj") (f (quote (("default"))))))

