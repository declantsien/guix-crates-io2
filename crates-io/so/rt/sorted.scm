(define-module (crates-io so rt sorted) #:use-module (crates-io))

(define-public crate-sorted-0.1.0 (c (n "sorted") (v "0.1.0") (h "1zdqivzdn67ka58i283yi0pd76h0n99b03pyyr3yrw63b4907k9a") (f (quote (("unstable") ("default"))))))

(define-public crate-sorted-0.2.0 (c (n "sorted") (v "0.2.0") (h "1lkgcj054z6dq5rjv7cldz279p3vmbsqqkl1gn7smmif4avqhhjl") (f (quote (("unstable") ("default"))))))

