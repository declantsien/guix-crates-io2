(define-module (crates-io so rt sortedlist-rs) #:use-module (crates-io))

(define-public crate-sortedlist-rs-0.1.0 (c (n "sortedlist-rs") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "14s0zlpr1ilqq81h2v2v19qx394pzyjgzakncvf4cz3j2gfxs0ck") (y #t)))

(define-public crate-sortedlist-rs-0.1.1 (c (n "sortedlist-rs") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0jrip195acr0729lmlh9abh5mgwy2bv6kslfa89h6qgpkgbn6g5b") (y #t)))

(define-public crate-sortedlist-rs-0.1.2 (c (n "sortedlist-rs") (v "0.1.2") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0nmjms6h8rlnym8jscl575a6rc5sg08mhm6xnxfrpl2325nkl6i4") (y #t)))

(define-public crate-sortedlist-rs-0.2.0 (c (n "sortedlist-rs") (v "0.2.0") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0kf8lqppnnvyxjx3yp816diirsvqv8l1lvxvjbg4l9iq7cq3g3i1")))

(define-public crate-sortedlist-rs-0.1.3 (c (n "sortedlist-rs") (v "0.1.3") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0dfb6rapm7ld06lzywc8pr4lzkf7i537yzyh2ffb81awip2zsb95") (y #t)))

(define-public crate-sortedlist-rs-0.2.1 (c (n "sortedlist-rs") (v "0.2.1") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 2)))) (h "1972gqzymlnqk9aaz8p2yp4n5zilfcrqviw6pg8cx30r5c0qmgcl")))

(define-public crate-sortedlist-rs-0.2.2 (c (n "sortedlist-rs") (v "0.2.2") (d (list (d (n "criterion") (r "^0.4.0") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 2)))) (h "1238rs2sjmw9xh2c5pqp4659pvvamz4zz0hjp8gsb3jwpch2fpv4")))

