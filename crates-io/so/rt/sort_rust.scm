(define-module (crates-io so rt sort_rust) #:use-module (crates-io))

(define-public crate-sort_rust-0.1.0 (c (n "sort_rust") (v "0.1.0") (h "1pbawwghpvs2jgdfwy70sy2rs9bmj6g0d7py2kv4z6fim2xc4ax9")))

(define-public crate-sort_rust-0.2.0 (c (n "sort_rust") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "04iivlaldpkfdba4fw1w3kpqjn0axrhrw7lr233s18rpklvpfgzh")))

