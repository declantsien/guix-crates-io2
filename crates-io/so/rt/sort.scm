(define-module (crates-io so rt sort) #:use-module (crates-io))

(define-public crate-sort-0.0.3 (c (n "sort") (v "0.0.3") (h "1wkjl8xv8afr1lqkb8prs0rnbm1hplf7czrzdm7j4c309d3ca3dz")))

(define-public crate-sort-0.0.1 (c (n "sort") (v "0.0.1") (h "09ipzfcrvr2fkvihr50yrfq3px1wvxr65wbb8qml6i7694fnvcw5")))

(define-public crate-sort-0.0.4 (c (n "sort") (v "0.0.4") (h "094r2aamsiwzywminybnifr7zvpg1x6f0h3v5m913nspp7vl141j")))

(define-public crate-sort-0.1.0 (c (n "sort") (v "0.1.0") (h "081vxj9mmf2v5czzr3ahggiad7fci2hxlyh9yvjxqqblpmddsx0m")))

(define-public crate-sort-0.2.0 (c (n "sort") (v "0.2.0") (h "0lrk465s3g9c1csvmw55gjsn26973vdpck6r9rw05ixhijbpxay4")))

(define-public crate-sort-0.3.0 (c (n "sort") (v "0.3.0") (h "0rhhsf1ym3ca8gkzqsny5l5vxbi7fj4fz2xayxn20h90qxzdghxn")))

(define-public crate-sort-0.4.0 (c (n "sort") (v "0.4.0") (h "02h1nx3bws289knq820a9lcm3whzmj72nlqp0b8im37kf5kqy0jc")))

(define-public crate-sort-0.5.0 (c (n "sort") (v "0.5.0") (h "0b7safy8gfbnja4q3yv0gd36s2lc6igm7nbwbcdlz8bkhdsrwnyw")))

(define-public crate-sort-0.6.0 (c (n "sort") (v "0.6.0") (h "0xln0pdgrrs8qhxkrl0213zjypg6gnxq6k0bzv0pysknklzsxqyf")))

(define-public crate-sort-0.7.0 (c (n "sort") (v "0.7.0") (h "1sfx7f8gw074rs64i0s2z7vncdhky823yzvn471arkb6rls2n2jl")))

(define-public crate-sort-0.8.0 (c (n "sort") (v "0.8.0") (h "1m3n9rgk0jinw83xwlg1fx9iajx742bq1vrqzdl8vqr1hxycr9mp")))

(define-public crate-sort-0.8.1 (c (n "sort") (v "0.8.1") (h "1d32xxm4wf121wc2civ9klbwzk38p4hy35y0fbg2rvqihnklhz6c")))

(define-public crate-sort-0.8.2 (c (n "sort") (v "0.8.2") (h "0ii3mv1rgmnvjha1qk291y8vd8akdrmw4935b49mf1ryiwaq4qy3")))

(define-public crate-sort-0.8.3 (c (n "sort") (v "0.8.3") (h "1cdf9cmlilza0q4dmfjkd1f75vs98h26d81s907id1pl8yp9pcwg")))

(define-public crate-sort-0.8.4 (c (n "sort") (v "0.8.4") (h "0k10cawc8bswrzf6dg74npkamc8vpirykvhh5plbd87775qzqbdp")))

(define-public crate-sort-0.8.5 (c (n "sort") (v "0.8.5") (h "1idh3iak2y2xdy8fj76qk875gcprmw62mhcxcr2x5fmw9hwkddvr")))

