(define-module (crates-io so rt sorted-list) #:use-module (crates-io))

(define-public crate-sorted-list-0.1.0 (c (n "sorted-list") (v "0.1.0") (h "19ixw8mff51804r6ai5lbxbyp1yw007v0c3bm8bjlssghnivg9s0") (f (quote (("nightly") ("default"))))))

(define-public crate-sorted-list-0.2.0 (c (n "sorted-list") (v "0.2.0") (h "1bblyx2h7f9j0n9ab5vragicrsf6przc1wp2ianybpf727l733i5") (f (quote (("nightly") ("default"))))))

