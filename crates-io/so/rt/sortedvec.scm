(define-module (crates-io so rt sortedvec) #:use-module (crates-io))

(define-public crate-sortedvec-0.1.0 (c (n "sortedvec") (v "0.1.0") (h "0c84ca8nlkafa3ax2llrcsjll6105sz7mzlxkyyf4kd931b757y6")))

(define-public crate-sortedvec-0.2.0 (c (n "sortedvec") (v "0.2.0") (h "06wpwb7bph3hf6vi6qqxflwywsm18zl276ksi7bpf4ji56g2kv1r")))

(define-public crate-sortedvec-0.3.0 (c (n "sortedvec") (v "0.3.0") (h "17j6qxmxi1cb896i0x0d7nkahqj2m94b1rwr0fp8i8pkxjmz9gkd")))

(define-public crate-sortedvec-0.3.1 (c (n "sortedvec") (v "0.3.1") (h "1rpdkvlpfci2d6vpw0v0xb55l67fk57whvakia2vzrrnwdsyjlms")))

(define-public crate-sortedvec-0.3.2 (c (n "sortedvec") (v "0.3.2") (h "0hn4sgzdhbdw9bv8xr6d7im2d1dx9nrvhlbc0p4hagb7ymmrkz9k")))

(define-public crate-sortedvec-0.4.0 (c (n "sortedvec") (v "0.4.0") (h "0335bnq844lq07ks34pf9i6n8f5ldz3hp58knrnpg4qdlmy72y53")))

(define-public crate-sortedvec-0.4.1 (c (n "sortedvec") (v "0.4.1") (h "0p197zal84j6b22nl3ll2kzrs7kijjan8ma3sh0dy1gxw74pb714")))

(define-public crate-sortedvec-0.5.0 (c (n "sortedvec") (v "0.5.0") (d (list (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.9") (d #t) (k 2)) (d (n "rand") (r "^0.7") (f (quote ("small_rng"))) (d #t) (k 2)))) (h "0nagzj9kfj9wnbfj3q0nxnmql36qmmbihyhnqa3555l7p9347vsv")))

