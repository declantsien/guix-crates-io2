(define-module (crates-io so rt sort_by_derive) #:use-module (crates-io))

(define-public crate-sort_by_derive-0.1.0 (c (n "sort_by_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "rust-format") (r "^0.3.4") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (d #t) (k 0)))) (h "0l4n09493q2gk8v85p8s5gn7i358g93iwf7577ykd1lp0akmkjvn")))

(define-public crate-sort_by_derive-0.1.1 (c (n "sort_by_derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "rust-format") (r "^0.3.4") (d #t) (k 2)) (d (n "syn") (r "^1.0.99") (d #t) (k 0)))) (h "0wwc2qpylm97b50bwq0vy9blr72zydm6lr4iyqr0nj668j8ks17x")))

(define-public crate-sort_by_derive-0.1.2 (c (n "sort_by_derive") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "rust-format") (r "^0.3.4") (d #t) (k 2)) (d (n "syn") (r "^1.0.99") (f (quote ("full"))) (d #t) (k 0)))) (h "0s76gcxvwj4n6az2bcmrkvd2y2mrj3f3ah5hlqf77b5kw179m9ri")))

(define-public crate-sort_by_derive-0.1.3 (c (n "sort_by_derive") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "rust-format") (r "^0.3.4") (d #t) (k 2)) (d (n "syn") (r "^1.0.99") (f (quote ("full"))) (d #t) (k 0)))) (h "1mzh69m31bapb92qw94bh9inaalld4bimpyrnyilwf6dr9vxmdxp")))

(define-public crate-sort_by_derive-0.1.4 (c (n "sort_by_derive") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "rust-format") (r "^0.3.4") (d #t) (k 2)) (d (n "syn") (r "^1.0.99") (f (quote ("full"))) (d #t) (k 0)))) (h "08fn6nqaqvdws14pl53qq6hf5n2y5xgc6185c81ah80idyb7l0dr")))

(define-public crate-sort_by_derive-0.1.5 (c (n "sort_by_derive") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "rust-format") (r "^0.3.4") (d #t) (k 2)) (d (n "syn") (r "^1.0.99") (f (quote ("full"))) (d #t) (k 0)))) (h "158fqywc4bvmlkjy0xcsb6km0cqfj3xql9ivl18sw3x2gxmlbkq3")))

(define-public crate-sort_by_derive-0.1.6 (c (n "sort_by_derive") (v "0.1.6") (d (list (d (n "either") (r "^1.8.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "rust-format") (r "^0.3.4") (d #t) (k 2)) (d (n "syn") (r "^1.0.99") (f (quote ("full"))) (d #t) (k 0)))) (h "1cagwgnp1dziylli58hvihjhrw8xv3zrim66vmz6jkc8nz8v54yg")))

(define-public crate-sort_by_derive-0.1.7 (c (n "sort_by_derive") (v "0.1.7") (d (list (d (n "either") (r "^1.8.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "rust-format") (r "^0.3.4") (d #t) (k 2)) (d (n "syn") (r "^1.0.99") (f (quote ("full"))) (d #t) (k 0)))) (h "1p3frpszlcs1xj92a8zqqvm95k0bjgybzwn3mwgsgdpjr535q958")))

(define-public crate-sort_by_derive-0.1.8 (c (n "sort_by_derive") (v "0.1.8") (d (list (d (n "either") (r "^1.8.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "rust-format") (r "^0.3.4") (d #t) (k 2)) (d (n "syn") (r "^1.0.99") (f (quote ("full"))) (d #t) (k 0)))) (h "028p1jhri20rpj0kz5lqcmhpx34hkjlzmx9x0lf2mcynjpvzp57r")))

(define-public crate-sort_by_derive-0.1.9 (c (n "sort_by_derive") (v "0.1.9") (d (list (d (n "either") (r "^1.8.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "rust-format") (r "^0.3.4") (d #t) (k 2)) (d (n "syn") (r "^1.0.99") (f (quote ("full"))) (d #t) (k 0)))) (h "0ncymxccvd30rzf48lzwzyj89nzq24dwdhda3k3f663gkc9ll3ax")))

(define-public crate-sort_by_derive-0.1.10 (c (n "sort_by_derive") (v "0.1.10") (d (list (d (n "either") (r "^1.8.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "rust-format") (r "^0.3.4") (d #t) (k 2)) (d (n "syn") (r "^1.0.99") (f (quote ("full"))) (d #t) (k 0)))) (h "0gg9mmnj2ci3dr2r3jcw9bgyyayd4jz50v0yn85dglpldfk8kzps")))

(define-public crate-sort_by_derive-0.1.11 (c (n "sort_by_derive") (v "0.1.11") (d (list (d (n "either") (r "^1.8.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "rust-format") (r "^0.3.4") (d #t) (k 2)) (d (n "syn") (r "^1.0.99") (f (quote ("full"))) (d #t) (k 0)))) (h "1yil7bh6v24cznybz04v00pqcindw6yz12vh5c1fx345g00chh12")))

(define-public crate-sort_by_derive-0.1.12 (c (n "sort_by_derive") (v "0.1.12") (d (list (d (n "either") (r "^1.8.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "rust-format") (r "^0.3.4") (d #t) (k 2)) (d (n "syn") (r "^1.0.99") (f (quote ("full"))) (d #t) (k 0)))) (h "1nnz1wg3a9bifpzfkr3h765nyxyzs2k38xqq7740pr936z9zylci")))

(define-public crate-sort_by_derive-0.1.13 (c (n "sort_by_derive") (v "0.1.13") (d (list (d (n "either") (r "^1.8.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "rust-format") (r "^0.3.4") (d #t) (k 2)) (d (n "syn") (r "^1.0.99") (f (quote ("full"))) (d #t) (k 0)))) (h "0dg4zb9facz0w5gvvq760645bcwiwnqajcc4v1y2sdhqaiwxzsi1")))

(define-public crate-sort_by_derive-0.1.14 (c (n "sort_by_derive") (v "0.1.14") (d (list (d (n "either") (r "^1.8.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "rust-format") (r "^0.3.4") (d #t) (k 2)) (d (n "syn") (r "^1.0.99") (f (quote ("full"))) (d #t) (k 0)))) (h "07xh9n7ns0bqg5g8ndg5alqaiqdrf45qrzpcrl0qwi9bcjj8vs3v")))

(define-public crate-sort_by_derive-0.1.15 (c (n "sort_by_derive") (v "0.1.15") (d (list (d (n "either") (r "^1.8.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "rust-format") (r "^0.3.4") (d #t) (k 2)) (d (n "syn") (r "^1.0.99") (f (quote ("full"))) (d #t) (k 0)))) (h "1ldzr9l7f7m93xqx5a5gjnrhrvay4nigg9yn0nswgsvx9sp0i9x4")))

(define-public crate-sort_by_derive-0.1.16 (c (n "sort_by_derive") (v "0.1.16") (d (list (d (n "either") (r "^1.8.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "rust-format") (r "^0.3.4") (d #t) (k 2)) (d (n "syn") (r "^2.0.28") (f (quote ("full"))) (d #t) (k 0)))) (h "139r7p9jfqicck7vkh8hyxb0y1q06641f0bqnzm8xqx68fg57v4y")))

(define-public crate-sort_by_derive-0.1.17 (c (n "sort_by_derive") (v "0.1.17") (d (list (d (n "either") (r "^1.4.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.52") (d #t) (k 0)) (d (n "quote") (r "^1.0.25") (d #t) (k 0)) (d (n "rust-format") (r ">=0.2, <1") (d #t) (k 2)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1z60cdkwa5fzw2civ0z7jvbif4smn2xqhq53qy8vhnhw7i9vq0yg")))

