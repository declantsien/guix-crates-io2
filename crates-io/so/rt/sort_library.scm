(define-module (crates-io so rt sort_library) #:use-module (crates-io))

(define-public crate-sort_library-0.1.0 (c (n "sort_library") (v "0.1.0") (h "08g5azjsy7403r3awdw76qa0i7sicqp7j9cn4yqrgm71v3xmwq86")))

(define-public crate-sort_library-0.1.1 (c (n "sort_library") (v "0.1.1") (h "1dj78x64w8cfcn0rfpp827gxfzpg13ha91clzr5kgsyym03n6905")))

