(define-module (crates-io so rt sort-path-length) #:use-module (crates-io))

(define-public crate-sort-path-length-0.1.0 (c (n "sort-path-length") (v "0.1.0") (h "0n5avgrwzyhc763rmi781nc58g5n7hnqshzahy26fspzlixzahr7")))

(define-public crate-sort-path-length-0.1.1 (c (n "sort-path-length") (v "0.1.1") (h "0gc4mi2n11aj4liypb7qxh2bnvqzgibqjaby8s3a630c9b20i2qh")))

(define-public crate-sort-path-length-0.2.0 (c (n "sort-path-length") (v "0.2.0") (h "0cxlmsil7pfjaj2c912cgf3nqn1g0jj9xk28fbihxn815dx9s7bg")))

(define-public crate-sort-path-length-0.2.1 (c (n "sort-path-length") (v "0.2.1") (h "0aqjd2hf50ympvy3hpvvk3nzhi1260xla180d8p3lhdjmj6b6zza")))

(define-public crate-sort-path-length-0.3.0 (c (n "sort-path-length") (v "0.3.0") (d (list (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)))) (h "1xcay22i8wkjxp2imj7v53jp0990qzd9qixcgwy69c553q3i91dd")))

(define-public crate-sort-path-length-0.3.1 (c (n "sort-path-length") (v "0.3.1") (d (list (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)))) (h "0z4hix1f6brwxai3dki8znb4by9b302ric6akvzbbgp6wjwb9py3")))

