(define-module (crates-io so rt sortrs) #:use-module (crates-io))

(define-public crate-sortrs-0.0.1 (c (n "sortrs") (v "0.0.1") (h "0fzbx3bjplr70d3bbvwp0s980ains46wlmrssvmlfhsjpys6v3bb")))

(define-public crate-sortrs-0.0.2 (c (n "sortrs") (v "0.0.2") (h "1ibkvz61b8ybya4968djnxhkbgdwiz3srjcncgjp4q245v2693n3")))

(define-public crate-sortrs-0.0.3 (c (n "sortrs") (v "0.0.3") (h "10qqffa40sa0ivr3anq2cyi4r0v25fyy1aakc5nzl989kfpq4466")))

(define-public crate-sortrs-0.0.4 (c (n "sortrs") (v "0.0.4") (d (list (d (n "rand") (r "^0.3.7") (d #t) (k 0)))) (h "1fp37yhlx528kzbhddlha4x53ds22fwnf38q17b5v035sw3xq2fg")))

(define-public crate-sortrs-0.0.5 (c (n "sortrs") (v "0.0.5") (d (list (d (n "num") (r "^0.1.28") (d #t) (k 0)) (d (n "rand") (r "^0.3.12") (d #t) (k 2)))) (h "06dlfv9m7sfr365dzks45yw6mi0hff7b8acrap2hla0q58qr3dhg")))

