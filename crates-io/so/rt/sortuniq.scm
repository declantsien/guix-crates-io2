(define-module (crates-io so rt sortuniq) #:use-module (crates-io))

(define-public crate-sortuniq-0.1.0 (c (n "sortuniq") (v "0.1.0") (d (list (d (n "clap") (r "^2") (k 0)) (d (n "lru") (r "^0.1") (d #t) (k 0)))) (h "1dablkay4vl06cr6qkm5ifdz37jxmj7qp5p8p2kffl1282ac4q2q")))

(define-public crate-sortuniq-0.2.0 (c (n "sortuniq") (v "0.2.0") (d (list (d (n "clap") (r "^4") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "lru") (r "^0.9") (d #t) (k 0)))) (h "1b1slga13w7lkxywki85m9n2hbmjbzj3aifjhvrii8hfgy0yaqjm")))

