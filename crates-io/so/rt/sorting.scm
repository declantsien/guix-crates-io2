(define-module (crates-io so rt sorting) #:use-module (crates-io))

(define-public crate-sorting-1.0.0 (c (n "sorting") (v "1.0.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0i8rqhahhjkc80kbkylhfqykbzfzikd3kc3n51mvzl6isqg1fwy6")))

(define-public crate-sorting-1.1.0 (c (n "sorting") (v "1.1.0") (d (list (d (n "rand") (r "^0.5") (d #t) (k 0)))) (h "07v3371n5g9a5k0wj3nqpd474ckdwkg3wvyjzzvxp2agzw1fn89k")))

(define-public crate-sorting-1.2.0 (c (n "sorting") (v "1.2.0") (d (list (d (n "rand") (r "^0.5") (d #t) (k 0)))) (h "1hz2iw0blxszx4vkkzjr18wwnyqbjgdhkwwf14nlw5grjcw38xm3")))

