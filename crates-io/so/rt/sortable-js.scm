(define-module (crates-io so rt sortable-js) #:use-module (crates-io))

(define-public crate-sortable-js-0.1.0+js.1.15.0 (c (n "sortable-js") (v "0.1.0+js.1.15.0") (d (list (d (n "js-sys") (r "^0.3.60") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.83") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.60") (f (quote ("Element" "HtmlElement"))) (d #t) (k 0)))) (h "1lf92v5cxvjjz0vlp136749zsfk7dychhm8gh2djyl7v77ad6yli")))

(define-public crate-sortable-js-0.1.1+js.1.15.0 (c (n "sortable-js") (v "0.1.1+js.1.15.0") (d (list (d (n "js-sys") (r "^0.3.60") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.83") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.60") (f (quote ("Element" "HtmlElement"))) (d #t) (k 0)))) (h "0w292v56ifm19k8yr3m897rychhp8a8hpv5yzybsy5sn5j4ghk0x")))

(define-public crate-sortable-js-0.1.2+js.1.15.0 (c (n "sortable-js") (v "0.1.2+js.1.15.0") (d (list (d (n "js-sys") (r "^0.3.60") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.83") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.60") (f (quote ("Element" "HtmlElement"))) (d #t) (k 0)))) (h "105wxsnvpn6wbhhpqpz3a00q9hdlzjigmxkya4l89x85cvd3sgfz")))

(define-public crate-sortable-js-0.1.3+js.1.15.0 (c (n "sortable-js") (v "0.1.3+js.1.15.0") (d (list (d (n "js-sys") (r "^0.3.60") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.83") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.60") (f (quote ("DomRect" "Element" "HtmlElement"))) (d #t) (k 0)))) (h "00xqa2qw0myw7nwv52ycm6gi5gjn6chrgfbsv2jaqi5w161hlnds")))

(define-public crate-sortable-js-0.1.4+js.1.15.0.patched.1 (c (n "sortable-js") (v "0.1.4+js.1.15.0.patched.1") (d (list (d (n "js-sys") (r "^0.3.60") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.83") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.60") (f (quote ("DomRect" "Element" "HtmlElement"))) (d #t) (k 0)))) (h "1zd9k54br5mh4apsnhc95hn2bs49n75y8nasf51ncjyxpzlyyq6z")))

(define-public crate-sortable-js-0.1.5+js.1.15.0.patched.1 (c (n "sortable-js") (v "0.1.5+js.1.15.0.patched.1") (d (list (d (n "js-sys") (r "^0.3.60") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.83") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.60") (f (quote ("DomRect" "Element" "HtmlElement"))) (d #t) (k 0)))) (h "1idlzkjbvinymgm4hq5x6720lx7mphidrh4slnb6240yhgpl0b5r")))

(define-public crate-sortable-js-0.1.6+js.1.15.2.patched.1 (c (n "sortable-js") (v "0.1.6+js.1.15.2.patched.1") (d (list (d (n "js-sys") (r "^0.3.60") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.83") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.60") (f (quote ("DomRect" "Element" "HtmlElement"))) (d #t) (k 0)))) (h "05y6s8rn4wnjllvka2hp768dy5d7ywrvsg9q345c332c7i1nd4z4")))

