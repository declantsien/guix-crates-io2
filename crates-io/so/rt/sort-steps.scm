(define-module (crates-io so rt sort-steps) #:use-module (crates-io))

(define-public crate-sort-steps-0.0.0 (c (n "sort-steps") (v "0.0.0") (d (list (d (n "gen-iter") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (f (quote ("small_rng"))) (d #t) (k 0)))) (h "0cs4qhyzz6sdn6kndfd8rphmj3c2lyb6p5vrnlw19z39a44w0jy1") (f (quote (("default"))))))

(define-public crate-sort-steps-0.1.0 (c (n "sort-steps") (v "0.1.0") (d (list (d (n "gen-iter") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (f (quote ("small_rng"))) (d #t) (k 0)))) (h "0byfg23p4ck5p9bs7m5ypmqmkxcj73pihg0nvp8ia3ss0z24nkbc") (f (quote (("default"))))))

(define-public crate-sort-steps-0.2.0 (c (n "sort-steps") (v "0.2.0") (d (list (d (n "gen-iter") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (f (quote ("small_rng"))) (d #t) (k 0)))) (h "067bv9q6k86mlz06qyvllbh3r5dg6sk6v2abdciv10y9y1k7zin5") (f (quote (("default"))))))

(define-public crate-sort-steps-0.2.1 (c (n "sort-steps") (v "0.2.1") (d (list (d (n "gen-iter") (r "^0.3.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (f (quote ("small_rng"))) (d #t) (k 0)))) (h "0k6hxrhxwmjz2zhnp81wzpi1x9j7z73c4kh80bj0y3vrgsfvjm36") (f (quote (("default"))))))

(define-public crate-sort-steps-0.2.2 (c (n "sort-steps") (v "0.2.2") (d (list (d (n "gen-iter") (r "^0.3.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (f (quote ("small_rng"))) (d #t) (k 0)))) (h "1lvd4pj4ibd9zr6mz4g1vp0diqnii32dfy410lvbj7lnx67kbgin") (f (quote (("default"))))))

(define-public crate-sort-steps-0.2.3 (c (n "sort-steps") (v "0.2.3") (d (list (d (n "rand") (r "^0.8.5") (f (quote ("small_rng"))) (d #t) (k 0)))) (h "1b09f8c905p0cn1i9jqns9pwpif40i0pajqrffj4fx3cf1gz3g23") (f (quote (("default"))))))

(define-public crate-sort-steps-0.2.4 (c (n "sort-steps") (v "0.2.4") (d (list (d (n "rand") (r "^0.8.5") (f (quote ("small_rng"))) (d #t) (k 0)))) (h "09pqd6mnrx860qj6waz1dfrjjr5kg3i0mcrgprn6vjiw1fqagik0") (f (quote (("default"))))))

