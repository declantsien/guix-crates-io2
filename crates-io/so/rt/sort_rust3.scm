(define-module (crates-io so rt sort_rust3) #:use-module (crates-io))

(define-public crate-sort_rust3-0.1.0 (c (n "sort_rust3") (v "0.1.0") (d (list (d (n "rust_keien") (r "^0.1.0") (d #t) (k 0)) (d (n "rust_wbc") (r "^0.1.0") (d #t) (k 0)) (d (n "rust_wek") (r "^0.1.0") (d #t) (k 0)) (d (n "sort_rust") (r "^0.2.0") (d #t) (k 0)))) (h "0py5sbb4bcixm8n0cmbrncjpkzdgnsyjgfrlf4phw3niqklda0pv")))

