(define-module (crates-io so rt sorter) #:use-module (crates-io))

(define-public crate-sorter-0.2.0 (c (n "sorter") (v "0.2.0") (d (list (d (n "rand") (r "*") (d #t) (k 0)))) (h "1w7hvgg816lyk1b99r1rsc3d0xpg72rc9xaa73hgr8ay98jdvi45")))

(define-public crate-sorter-0.3.0 (c (n "sorter") (v "0.3.0") (d (list (d (n "rand") (r "*") (d #t) (k 0)))) (h "0zkrx080j97r95qxj37wgqiwj7aby4lwl6bqzxlzi22agx2hxlhi")))

(define-public crate-sorter-0.4.0 (c (n "sorter") (v "0.4.0") (d (list (d (n "rand") (r "*") (d #t) (k 0)))) (h "1ykilmk5nvh4y8n324gq7cxq1yx3hvjic4dqnm3qjp9wdk9qmi5v")))

