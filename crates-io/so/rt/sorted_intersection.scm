(define-module (crates-io so rt sorted_intersection) #:use-module (crates-io))

(define-public crate-sorted_intersection-0.1.0 (c (n "sorted_intersection") (v "0.1.0") (h "0i2acg2llwq5jd7qys4rvkb8w60g7hwn4vdibkvyv1qj2i17w4bd")))

(define-public crate-sorted_intersection-1.0.0 (c (n "sorted_intersection") (v "1.0.0") (h "0kqapml3mgqgf2jgypyms39f97p8s4wwaj7s6chdvxj58dm9kyld")))

(define-public crate-sorted_intersection-1.0.1 (c (n "sorted_intersection") (v "1.0.1") (h "10g6dchclk1bg420c7jszby9sbgvbzf7jraz4gdg1vl7y462cd09")))

(define-public crate-sorted_intersection-1.0.2 (c (n "sorted_intersection") (v "1.0.2") (h "1h9y9scqrnpja204qg9mpp738q6amxwhwcmg0sf1iil0fm5nd1k2")))

(define-public crate-sorted_intersection-1.1.0 (c (n "sorted_intersection") (v "1.1.0") (h "07znyc3cq19112lsrjp55sjcwp6g6c1nic0rlkfjq4w7qspz7svv")))

(define-public crate-sorted_intersection-1.2.0 (c (n "sorted_intersection") (v "1.2.0") (h "12x2cmldv4b61kwh9zwzhx8b5lqnl7jzbcf5x0jwabcgr378si23")))

