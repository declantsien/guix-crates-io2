(define-module (crates-io so rt sorted_array) #:use-module (crates-io))

(define-public crate-sorted_array-0.1.0 (c (n "sorted_array") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "slice_search") (r "^0.1.0") (d #t) (k 0)))) (h "0zbd1qk76im6lak096wdnyhbk5jl7lifpbjm1zqzrkikji0vmgqw") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-sorted_array-0.1.1 (c (n "sorted_array") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "slice_search") (r "^0.1.1") (d #t) (k 0)))) (h "1dc7rxfq71bi5cxxxrvq9mqzksqgwqlx2v1h2jqnfqpd2jly5q24") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-sorted_array-0.1.2 (c (n "sorted_array") (v "0.1.2") (d (list (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "slice_search") (r "^0.1") (d #t) (k 0)))) (h "1m4a24x4whv4q22cvjw97a49qyi7f4wsbs4zbxxcipqws845iml9") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-sorted_array-0.1.3 (c (n "sorted_array") (v "0.1.3") (d (list (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "slice_search") (r "^0.1.2") (d #t) (k 0)))) (h "1ckw1lilyp5iskx308z50yydcb8ldah6blb4x7g1kv0mqgmzn1rz") (s 2) (e (quote (("serde" "dep:serde"))))))

