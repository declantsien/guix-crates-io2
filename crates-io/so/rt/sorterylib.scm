(define-module (crates-io so rt sorterylib) #:use-module (crates-io))

(define-public crate-sorterylib-0.1.0 (c (n "sorterylib") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "05892bzqiwdg3c1ym7mrdjiz7mkxi6sjg9hdisaj6sdc5hz8ghin")))

(define-public crate-sorterylib-0.1.1 (c (n "sorterylib") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "1dym5sias6id6kv40215yibly96d9i7hb4935211gccyf4mkfhal")))

(define-public crate-sorterylib-0.2.2 (c (n "sorterylib") (v "0.2.2") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "filetime") (r "^0.2.15") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "1cibqmgwqpb6siqyp8f26h0qbhr1bha0p59xvvkniywi4i2n1m9l")))

(define-public crate-sorterylib-0.3.2 (c (n "sorterylib") (v "0.3.2") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "filetime") (r "^0.2.15") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "0fmjbl2s0iyizfyb82n02pm13is89sn2sgm1cy6g9bhba2dwzn44")))

(define-public crate-sorterylib-0.3.3 (c (n "sorterylib") (v "0.3.3") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "filetime") (r "^0.2.15") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "15ybk2gl4k148k315rh765bh3pwpg0syl5ylhasxiq8x1760c8qx")))

