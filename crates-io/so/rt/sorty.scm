(define-module (crates-io so rt sorty) #:use-module (crates-io))

(define-public crate-sorty-0.1.0 (c (n "sorty") (v "0.1.0") (h "0c50wnn59ivipny5sy4zjjmmbxplkqg4sn71p55fpjsxm4fw07d0")))

(define-public crate-sorty-0.1.1 (c (n "sorty") (v "0.1.1") (h "0k76q089l4rspja9biy7cagxhdh8iv2j88dxvm2630qnq0vm22ak")))

(define-public crate-sorty-0.1.2 (c (n "sorty") (v "0.1.2") (h "11hai4cly8hxgjvrv6j811z50jayjbn3k65cp8r2yji6ys6rcycp")))

