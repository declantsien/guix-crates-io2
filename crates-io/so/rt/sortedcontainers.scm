(define-module (crates-io so rt sortedcontainers) #:use-module (crates-io))

(define-public crate-sortedcontainers-0.0.1 (c (n "sortedcontainers") (v "0.0.1") (d (list (d (n "more-asserts") (r "^0.2.2") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1800af84bkagzx2aq98r30djsjis2zspn3hlx7r2ldv6sgkfxpwp") (y #t)))

(define-public crate-sortedcontainers-0.1.0-alpha.0 (c (n "sortedcontainers") (v "0.1.0-alpha.0") (d (list (d (n "more-asserts") (r "^0.2.2") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0z8r2a3abb5khbczzl1gxkkc788hdk546x5ijygb1zhmhxvhww9c") (y #t)))

(define-public crate-sortedcontainers-0.1.0-alpha.2 (c (n "sortedcontainers") (v "0.1.0-alpha.2") (d (list (d (n "more-asserts") (r "^0.2.2") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "00bw5xiz24fmxs247w9837wfi3i6qmwhisd412pgnrczkmlmgbj9") (y #t)))

(define-public crate-sortedcontainers-0.1.0 (c (n "sortedcontainers") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "more-asserts") (r "^0.2.2") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0dvq19m786x00xlpvk02r5kwazkpw6pw2ffka66lzaamqvj2rgw8")))

(define-public crate-sortedcontainers-0.1.1 (c (n "sortedcontainers") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "more-asserts") (r "^0.2.2") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "16h2zs67wj5v5zgcv2dfp81kdm1i9wdsy9w8ljcxkpf688p9xjza")))

(define-public crate-sortedcontainers-0.2.0 (c (n "sortedcontainers") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "more-asserts") (r "^0.2.2") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "02yfnmhg9kl3zw7bkan62w0hqja7lxvq3mhli3h9q2d4y9yb6pdj")))

(define-public crate-sortedcontainers-0.2.1 (c (n "sortedcontainers") (v "0.2.1") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "more-asserts") (r "^0.2.2") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0hs9si4jy71qjhcv7vf6m42c2jbgbjpzv4kkk7ysdfy5jgrwdgml")))

(define-public crate-sortedcontainers-0.3.0 (c (n "sortedcontainers") (v "0.3.0") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "more-asserts") (r "^0.2.2") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "10pn48v6aaxs6m6dcg9p3n9xavshvcik6cr1xwafbkykd01aldhm")))

