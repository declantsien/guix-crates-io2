(define-module (crates-io so rt sorted-insert) #:use-module (crates-io))

(define-public crate-sorted-insert-0.1.0 (c (n "sorted-insert") (v "0.1.0") (h "0xzk143vcav5axvll2h55dsiwlh187h8la5j715f3db92h65y205") (f (quote (("std") ("nightly") ("default" "std"))))))

(define-public crate-sorted-insert-0.2.0 (c (n "sorted-insert") (v "0.2.0") (h "16rrzk23sdx54z36jz8hxkai888m0jj40bkfwjwcnzhcjsfmbsi3") (f (quote (("std") ("nightly") ("default" "std"))))))

(define-public crate-sorted-insert-0.2.1 (c (n "sorted-insert") (v "0.2.1") (h "1rd3irjqwi9v148k4884rsv5cb71hbr0rh2afl5pl9kr1djbpar5") (f (quote (("std") ("nightly") ("default" "std"))))))

(define-public crate-sorted-insert-0.2.2 (c (n "sorted-insert") (v "0.2.2") (h "1qzhb9jq8l2cmngrpkph4fwdpr4pnsv86a3g8x6zs0xa11nzq7bg") (f (quote (("std") ("default" "std"))))))

(define-public crate-sorted-insert-0.2.3 (c (n "sorted-insert") (v "0.2.3") (h "0c20ijjpgn0jq30bpy7x3n4dms405by1dmxlagiqlwjasw1j5mjx") (f (quote (("std") ("default" "std"))))))

