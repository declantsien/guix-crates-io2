(define-module (crates-io so rt sorted-collections) #:use-module (crates-io))

(define-public crate-sorted-collections-0.0.1 (c (n "sorted-collections") (v "0.0.1") (h "1c97y84cycy0nysxg6x1dwpq0qyc48s0zdrn3pfa5b4graxcy1i2")))

(define-public crate-sorted-collections-0.0.2 (c (n "sorted-collections") (v "0.0.2") (h "145x94kz7zypq7z9gckmhqsklqvax9jp89wsic9qsz7wws0mz12y")))

(define-public crate-sorted-collections-0.0.3 (c (n "sorted-collections") (v "0.0.3") (h "0gip5pzib9588gayd0c7vb51g837zni43kzd0ps25nc78mvw2fha")))

(define-public crate-sorted-collections-0.0.4 (c (n "sorted-collections") (v "0.0.4") (h "17m01jn1pmy6lmf94hqjc1mb7x6vcgb30djlg94br4j5yig5l70m")))

(define-public crate-sorted-collections-0.0.5 (c (n "sorted-collections") (v "0.0.5") (h "0v9i7gd6gfaiqaiajilb5bvfkha355m8v6nx7x2zazp5bq302762")))

(define-public crate-sorted-collections-0.0.6 (c (n "sorted-collections") (v "0.0.6") (h "15bgf1a92aclg054jmzvarm1bi3vhlapcinq3y3j5zwp3nnm4rw8")))

(define-public crate-sorted-collections-0.0.7 (c (n "sorted-collections") (v "0.0.7") (h "1vs7iaihw2fs3bj9q1fbk1w5c8sk7afvyjm92vswymnm9dmgrnbn")))

(define-public crate-sorted-collections-0.0.8 (c (n "sorted-collections") (v "0.0.8") (h "0n1a2k06k2dgh2gxdixg9iw6an1l01066631fx96k2hvgz9ccc0s")))

