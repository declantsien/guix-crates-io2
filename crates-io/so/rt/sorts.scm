(define-module (crates-io so rt sorts) #:use-module (crates-io))

(define-public crate-sorts-0.1.0 (c (n "sorts") (v "0.1.0") (h "0sya7h1m7sl6ljxi0jgc8r0blbijvz3lv9apmvzs3wyrpxisdnfx")))

(define-public crate-sorts-0.2.0 (c (n "sorts") (v "0.2.0") (d (list (d (n "rand") (r "^0.5") (d #t) (k 2)))) (h "1zn4iwn6y9yy485dr7x0kslazzidchcy35gp8mdnj3ljcpdxfvp7")))

(define-public crate-sorts-0.3.0 (c (n "sorts") (v "0.3.0") (d (list (d (n "rand") (r "^0.5") (d #t) (k 2)))) (h "13sh807ysrnjyiq0ad56i5w8c25860xdif4d2ayzgjpfig29wrgy")))

(define-public crate-sorts-0.4.0 (c (n "sorts") (v "0.4.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 2)))) (h "08yxnpm85g38b65x192qzw7vnyr23vjgjn0pyym4g3gwh8slvzgn")))

(define-public crate-sorts-0.5.0 (c (n "sorts") (v "0.5.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 2)))) (h "0hwphf7zgnar5y6xm8bmw4a9zbm0jnmd54gh0idw98xi9lsnfjdw")))

(define-public crate-sorts-0.6.0 (c (n "sorts") (v "0.6.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 2)))) (h "1x32b75hfy10c3w1ngd00gf6ljc66nprwkpihnp3pafnr75michw")))

(define-public crate-sorts-0.6.1 (c (n "sorts") (v "0.6.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "1f5g7njgyg1wsashpi73rv6h2sagc3p8sc2qfv39ddvs0i311sxa")))

