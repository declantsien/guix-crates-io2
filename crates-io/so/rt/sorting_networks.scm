(define-module (crates-io so rt sorting_networks) #:use-module (crates-io))

(define-public crate-sorting_networks-0.1.0 (c (n "sorting_networks") (v "0.1.0") (d (list (d (n "quote") (r "^0.4.2") (d #t) (k 1)) (d (n "syn") (r "^0.12.13") (d #t) (k 1)))) (h "1diwsamgwnxv76pzvdx2pjzdki52jg0qdx1syv60z158dn5gadax") (f (quote (("std") ("default" "std"))))))

