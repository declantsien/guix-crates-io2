(define-module (crates-io so rt sorted_vector_map) #:use-module (crates-io))

(define-public crate-sorted_vector_map-0.1.0 (c (n "sorted_vector_map") (v "0.1.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9") (d #t) (k 0)))) (h "0ljxfl7dwzrwcn7gb0hcl26nc90p9q3wvc3nrw55j5skpjh4wkl3")))

(define-public crate-sorted_vector_map-0.2.0 (c (n "sorted_vector_map") (v "0.2.0") (d (list (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0") (d #t) (k 0)))) (h "0s0k8rs0fhjsg9isg429fpymardm19xpzzllabjdpaxbq947c5nr")))

