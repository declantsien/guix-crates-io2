(define-module (crates-io so rt sort_zh) #:use-module (crates-io))

(define-public crate-sort_zh-0.1.0 (c (n "sort_zh") (v "0.1.0") (d (list (d (n "chinese-number") (r "^0.6.5") (d #t) (k 0)) (d (n "rust_icu_ucol") (r "^3.0.0") (d #t) (k 0)))) (h "1d45fgp8nnrdm7jjkg4pmpsn4g46sgxijpkg2zzn9hi2w10fp1z2")))

(define-public crate-sort_zh-0.1.1 (c (n "sort_zh") (v "0.1.1") (d (list (d (n "chinese-number") (r "^0.6.5") (d #t) (k 0)) (d (n "rust_icu_ucol") (r "^3.0.0") (d #t) (k 0)))) (h "1dm29w3g85jswayypyka0k4j0yz1y3z68ck82xipxr43m19w9w8l")))

