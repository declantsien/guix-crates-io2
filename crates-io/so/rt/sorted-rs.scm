(define-module (crates-io so rt sorted-rs) #:use-module (crates-io))

(define-public crate-sorted-rs-0.1.0 (c (n "sorted-rs") (v "0.1.0") (d (list (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "num") (r "^0.4") (k 0)))) (h "1fxq4sdim3il8hmlz65sl4ichr7as74zq00a19202zpyc9r2s2fy") (f (quote (("use-sse") ("use-avx2") ("std" "num/std") ("default" "use-sse" "std"))))))

