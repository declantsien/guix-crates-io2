(define-module (crates-io so rt sortby) #:use-module (crates-io))

(define-public crate-sortby-0.1.0 (c (n "sortby") (v "0.1.0") (h "0ca92kyaziycs87fiavwk4lkhgxhf5zvba5fgwyn7xjjxwm5cv8a")))

(define-public crate-sortby-0.1.1 (c (n "sortby") (v "0.1.1") (h "0faxga2y715dgbdyvpnwbgzn8r7b0xz2xrgqckhjsp8pns4q8v70")))

(define-public crate-sortby-0.1.2 (c (n "sortby") (v "0.1.2") (h "1vxrdd8i45b0887wfy1j8yab8rpzsdpp4g1p2n6s8719fl4bza4i")))

(define-public crate-sortby-0.1.3 (c (n "sortby") (v "0.1.3") (h "0chbqy0bsvnc23znsrd58m6yj2aa4n4r4biraa85y5rmn45nq28v")))

