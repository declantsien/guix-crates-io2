(define-module (crates-io so rt sortnet) #:use-module (crates-io))

(define-public crate-sortnet-0.1.0 (c (n "sortnet") (v "0.1.0") (d (list (d (n "arrayref") (r "^0.3") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "itertools") (r "^0.8") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "static_assertions") (r "^1") (d #t) (k 0)))) (h "1bgkig1pfbcmqmfrribvylzni1k34cphfz7ncykh9f6p8288k4ly")))

