(define-module (crates-io so rt sort_algorithms) #:use-module (crates-io))

(define-public crate-sort_algorithms-0.1.1 (c (n "sort_algorithms") (v "0.1.1") (h "142jkiawz7mz4ga3vbkmalnma3ixjdq12996mmgkn4xl1d22dyf8")))

(define-public crate-sort_algorithms-0.1.2 (c (n "sort_algorithms") (v "0.1.2") (h "0q57f17lfypz8n42aq737yalflvq0l3s3lbl9pnr150zbqlfvmlx")))

(define-public crate-sort_algorithms-0.1.3 (c (n "sort_algorithms") (v "0.1.3") (h "1119b6q4bg5zh6qdi4x7d5qsziccfrirjyra4gdv63mr999wd23s")))

(define-public crate-sort_algorithms-0.2.0 (c (n "sort_algorithms") (v "0.2.0") (h "18cwc16iyznrp9cq35c3539hcjavg3crqfq360xy33myjym2k14g")))

(define-public crate-sort_algorithms-0.2.5 (c (n "sort_algorithms") (v "0.2.5") (h "17jjv0xf5m0vjk3fg936903hq1p01k2p5kr477flrg838p0fqv2w")))

(define-public crate-sort_algorithms-0.2.6 (c (n "sort_algorithms") (v "0.2.6") (h "00037slcdyahjdzz6019gaknw8dhicknvzm80zjbsfa7j9n1mi9r")))

(define-public crate-sort_algorithms-0.2.7 (c (n "sort_algorithms") (v "0.2.7") (h "1sfdhmli40195ksi2yngd59yijbchrk7zc0z8axnvbrszw78178c")))

(define-public crate-sort_algorithms-0.2.8 (c (n "sort_algorithms") (v "0.2.8") (h "1fx1qg341prqyqgl840bc3d5n14df8n16qhr708iidgbhzxpadxf")))

(define-public crate-sort_algorithms-0.2.9 (c (n "sort_algorithms") (v "0.2.9") (h "0xn99n6mmfc26k4w6v549xlc211zdpq0nwinlkbxq45jnwwq2hhp")))

(define-public crate-sort_algorithms-0.3.0 (c (n "sort_algorithms") (v "0.3.0") (h "0xmxmnyalcgk0325n1cr0k2lzrcd55pscppdxwi16ydp4vfzza93")))

(define-public crate-sort_algorithms-0.3.1 (c (n "sort_algorithms") (v "0.3.1") (h "0aifk1lsaz5xya8k8wf641819qmim07dc3w62xq0f86987vi0kaj")))

