(define-module (crates-io so rt sort_algos) #:use-module (crates-io))

(define-public crate-sort_algos-0.1.0 (c (n "sort_algos") (v "0.1.0") (h "0hpmk2q2whq4c7g2ic99sni472lys8nqkb6ifnsmf1c556nv5x6q") (y #t)))

(define-public crate-sort_algos-0.1.1 (c (n "sort_algos") (v "0.1.1") (h "110bp3v3k3hsjmliapvxw7m7wvyjzggkrr5mq0w9z2bhbylllgd4") (y #t)))

(define-public crate-sort_algos-0.1.2 (c (n "sort_algos") (v "0.1.2") (h "1rjx62r1f5n72x44w2ah559c45d6jnfb8asan0kw0acd588njvlz")))

