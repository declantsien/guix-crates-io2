(define-module (crates-io so rt sort_str_to_sql) #:use-module (crates-io))

(define-public crate-sort_str_to_sql-0.0.1 (c (n "sort_str_to_sql") (v "0.0.1") (h "1cm9frnpc1z07ppgq4wbf3przp4ynyjzlckcr4r83zwcrwg80whc")))

(define-public crate-sort_str_to_sql-0.0.2 (c (n "sort_str_to_sql") (v "0.0.2") (h "03wn4y0igrbijjn44mss4b3lb1lyj61ygjvdk7grnwmhyqlym74v")))

(define-public crate-sort_str_to_sql-0.0.3 (c (n "sort_str_to_sql") (v "0.0.3") (d (list (d (n "regex") (r "^0.1.3") (d #t) (k 0)))) (h "0nnxcsksnawi4ch8ypx3sx1aimnsp6wbxjijnfihjsbli8z4x8wm")))

(define-public crate-sort_str_to_sql-0.0.4 (c (n "sort_str_to_sql") (v "0.0.4") (d (list (d (n "regex") (r "^0.1.10") (d #t) (k 0)) (d (n "regex_macros") (r "^0.1.5") (d #t) (k 0)))) (h "0r9r8klsyv0clw01swywg7i2wbza4f69axb2sp67xp057x4jdgcv")))

(define-public crate-sort_str_to_sql-0.0.5 (c (n "sort_str_to_sql") (v "0.0.5") (d (list (d (n "regex") (r "^0.1.10") (d #t) (k 0)) (d (n "regex_macros") (r "^0.1.5") (d #t) (k 0)))) (h "127m6kjdrx77n7fsl5zn53zlvnc4nmyyzqhpcz3xd5nplgh0gh9a")))

(define-public crate-sort_str_to_sql-1.0.0 (c (n "sort_str_to_sql") (v "1.0.0") (d (list (d (n "regex") (r "^0.1.27") (d #t) (k 0)))) (h "1jxxvkajm5pc6261y2i2mb6zjs7h13jhbk8mx483lrnd6bh4r5a0")))

