(define-module (crates-io so rt sorting_explorer) #:use-module (crates-io))

(define-public crate-sorting_explorer-0.1.0 (c (n "sorting_explorer") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8.0") (d #t) (k 2)))) (h "17bncnzxg7gcjjgjh4h2hmwla92aq1pqga4gyvrrk04gq85whhym")))

