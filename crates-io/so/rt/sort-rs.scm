(define-module (crates-io so rt sort-rs) #:use-module (crates-io))

(define-public crate-sort-rs-0.1.0 (c (n "sort-rs") (v "0.1.0") (h "0qbq2c4na88lshpwwaj6b4nc5xlqya0a742z1gzmvvg1gxwqz9q2")))

(define-public crate-sort-rs-0.1.1 (c (n "sort-rs") (v "0.1.1") (h "1kl8ls6mwd59izirnjbxlqgrkfq60ll91ycslpclji0p5ylaawqh")))

(define-public crate-sort-rs-0.1.2 (c (n "sort-rs") (v "0.1.2") (h "0ilgg0wam22n7xkq6cxrsl4c5j5zrll0vip6pjqz3gfkkchibyk3")))

