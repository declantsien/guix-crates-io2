(define-module (crates-io so rt sorted-iter) #:use-module (crates-io))

(define-public crate-sorted-iter-0.1.0 (c (n "sorted-iter") (v "0.1.0") (d (list (d (n "quickcheck") (r "^0.9.0") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.8.0") (d #t) (k 2)))) (h "0rrgc0w8027m5w7w592qq1f93fq8r49cy7iah6g9cpsjdrp4j3f6")))

(define-public crate-sorted-iter-0.1.1 (c (n "sorted-iter") (v "0.1.1") (d (list (d (n "maplit") (r "^1.0.2") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9.0") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.8.0") (d #t) (k 2)))) (h "0gp9vfr8h1pg41l50r7f3dv9av4cmq496059d1hcf81lsv316ka2")))

(define-public crate-sorted-iter-0.1.2 (c (n "sorted-iter") (v "0.1.2") (d (list (d (n "maplit") (r "^1.0.2") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9.0") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.8.0") (d #t) (k 2)))) (h "16ifamnlmfyvrj7afqy4mchvccrx0rri93fmlf6nkra8xvrghrh8")))

(define-public crate-sorted-iter-0.1.3 (c (n "sorted-iter") (v "0.1.3") (d (list (d (n "maplit") (r "^1.0.2") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9.0") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.8.0") (d #t) (k 2)))) (h "152jh2ghf028hcmlnqj26gixj1cw550n5cpm5l1lj02rnr3g9rw3")))

(define-public crate-sorted-iter-0.1.4 (c (n "sorted-iter") (v "0.1.4") (d (list (d (n "maplit") (r "^1.0.2") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9.0") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.8.0") (d #t) (k 2)))) (h "1fq1ln0j3b7r9ka5vayvqzs05hi6hq6k2rxz9xr4r4c8s8qvacmg")))

(define-public crate-sorted-iter-0.1.5 (c (n "sorted-iter") (v "0.1.5") (d (list (d (n "maplit") (r "^1.0.2") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9.0") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.8.0") (d #t) (k 2)))) (h "0s8rbjynn1zdykjnn0izkcbsz8hcsvgsmjz0kvva8bml9bn6nrsy")))

(define-public crate-sorted-iter-0.1.6 (c (n "sorted-iter") (v "0.1.6") (d (list (d (n "maplit") (r "^1.0.2") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9.0") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.8.0") (d #t) (k 2)))) (h "17i23jlsl31mqvc5wn69x2i4f96nkccqc0bbylv9nnpr8wzl5184")))

(define-public crate-sorted-iter-0.1.7 (c (n "sorted-iter") (v "0.1.7") (d (list (d (n "maplit") (r "^1.0") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.8") (d #t) (k 2)))) (h "12rcihj6jiklfqzvzrbb7vs5sky3ilq3crh08wx821gxsmg17sfb")))

(define-public crate-sorted-iter-0.1.8 (c (n "sorted-iter") (v "0.1.8") (d (list (d (n "maplit") (r "^1.0") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.8") (d #t) (k 2)))) (h "1pjknlxnjf62wclfqrvlp3a4i2xljiyzn2jrx6si7frl7w2l24wz")))

(define-public crate-sorted-iter-0.1.9 (c (n "sorted-iter") (v "0.1.9") (d (list (d (n "maplit") (r "^1.0") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.8") (d #t) (k 2)))) (h "1z21jx3wh5zbn8nm69a3rchad16x4ahjghprhwdys7wk0z5a5jxr")))

(define-public crate-sorted-iter-0.1.10 (c (n "sorted-iter") (v "0.1.10") (d (list (d (n "maplit") (r "^1.0") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.8") (d #t) (k 2)))) (h "02dmg39pzaz8i43r91zi8kyymd1g1a04vvh4dps8slmpfysz2qf3")))

(define-public crate-sorted-iter-0.1.11 (c (n "sorted-iter") (v "0.1.11") (d (list (d (n "maplit") (r "^1.0") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.8") (d #t) (k 2)))) (h "1xxiw29lid5hpwny4mbcwcps9apcjbxb69sv1zkdlb690zf5gsxw")))

