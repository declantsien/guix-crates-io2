(define-module (crates-io so rt sorted-channel) #:use-module (crates-io))

(define-public crate-sorted-channel-0.1.0 (c (n "sorted-channel") (v "0.1.0") (d (list (d (n "parking_lot") (r "^0.12") (d #t) (k 0)))) (h "180m5s2djvzblvq5v95ccq1zsgkljrn2xpp04vgygb48km0s8as5")))

(define-public crate-sorted-channel-0.1.1 (c (n "sorted-channel") (v "0.1.1") (d (list (d (n "parking_lot") (r "^0.12") (d #t) (k 0)))) (h "1f29n26vls28k63grk907cva67agbfjp9jzlwxl6jqf270kcfids")))

