(define-module (crates-io so rt sort-it) #:use-module (crates-io))

(define-public crate-sort-it-0.1.0 (c (n "sort-it") (v "0.1.0") (h "1z3gmbmzbycvsw8pd21jrvn1psz7h88s7b9lmwwp26qclpda4ill")))

(define-public crate-sort-it-0.1.1 (c (n "sort-it") (v "0.1.1") (h "1k1hsmi8haxlzd6zmsqpmq0x974mrf78ybbycddrb52r9pfl9d3h")))

(define-public crate-sort-it-0.1.2 (c (n "sort-it") (v "0.1.2") (h "0k89cgw70ri8p7ir7k0v54jccb0ja8l03cj18i3i23nsanqcsm9c")))

(define-public crate-sort-it-0.1.3 (c (n "sort-it") (v "0.1.3") (h "1n1y14xjqfi9msm84dwaxb6ylybjli7ycyglq7m95szfqcz6j1zk")))

(define-public crate-sort-it-0.1.4 (c (n "sort-it") (v "0.1.4") (h "1zy8623hmb3i0ymwdpc6hmjxcna8wjmh9acg7ikapxxyffd9pk9q")))

(define-public crate-sort-it-0.2.0 (c (n "sort-it") (v "0.2.0") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "18i1lypnzrdmbxz5ds5qf0zz64kj7bzcf7w71hz8b9mgfi09fm05")))

(define-public crate-sort-it-0.2.1 (c (n "sort-it") (v "0.2.1") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "1h0vjn62nmhg0l9i6wxl409hdd4szpiispfpbjacmxiwjikck9p1")))

(define-public crate-sort-it-0.2.2 (c (n "sort-it") (v "0.2.2") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "1i9pyi32l1zgbhbghnr79bq20f2ck1kwq3di367g2rpcswc5w06h")))

