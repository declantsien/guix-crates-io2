(define-module (crates-io so rt sortr) #:use-module (crates-io))

(define-public crate-sortr-0.1.0 (c (n "sortr") (v "0.1.0") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0bpkybk0ksimqwwgzqaxs9gnvsab5pcn8yb1wd81kriy5fk8x34b")))

(define-public crate-sortr-0.1.1 (c (n "sortr") (v "0.1.1") (d (list (d (n "cfg-if") (r "^0.1.10") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (o #t) (d #t) (k 0)))) (h "0jx9chjsp7ynya3h6i1q2a4vy87y1wvzn9mychcf51g76fdgk2r9") (f (quote (("merge") ("insertion") ("heap") ("default" "bogo" "bubble" "heap" "insertion" "merge") ("bubble") ("bogo" "rand"))))))

(define-public crate-sortr-0.1.2 (c (n "sortr") (v "0.1.2") (d (list (d (n "cfg-if") (r "^0.1.10") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (o #t) (d #t) (k 0)))) (h "10s3kj8hb9w849yh8fi2gl263dzki3dh1ngv1f8n9w74qwkh6ig1") (f (quote (("merge") ("insertion") ("heap") ("default" "bogo" "bubble" "heap" "insertion" "merge") ("bubble") ("bogo" "rand"))))))

