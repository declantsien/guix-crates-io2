(define-module (crates-io so rt sorth) #:use-module (crates-io))

(define-public crate-sorth-0.1.0 (c (n "sorth") (v "0.1.0") (h "0zvibsa80l8vzp4lsdm0jmbvx6ncdgkcrg2k24dg5nc0ai2qp9la")))

(define-public crate-sorth-0.1.1 (c (n "sorth") (v "0.1.1") (h "0vkfnmigji4pm9f54ffs6x5drzvrc63x09n6dd3s4q0rwynqmqzj")))

(define-public crate-sorth-0.2.0 (c (n "sorth") (v "0.2.0") (h "003qc2ig0aq3zr8fkrr035jbrg90jblzgpbib8vc0fz2yl2f2lwd")))

(define-public crate-sorth-0.2.1 (c (n "sorth") (v "0.2.1") (h "0ik2xsck8k7ssfy2j22amwsa28vrw0zspi80j9j7lasyclrl33ap")))

(define-public crate-sorth-0.2.2 (c (n "sorth") (v "0.2.2") (h "0vwya7xijlbavs3wqznlmr14imj21k8npqv11q0zkkwc6yiknznm")))

