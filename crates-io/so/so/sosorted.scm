(define-module (crates-io so so sosorted) #:use-module (crates-io))

(define-public crate-sosorted-0.1.0 (c (n "sosorted") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (f (quote ("small_rng"))) (d #t) (k 2)))) (h "1iancpag2zfcygx2ssvk51a38lgl0yg1gwkww2fm085q34yyprmw")))

