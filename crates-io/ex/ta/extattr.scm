(define-module (crates-io ex ta extattr) #:use-module (crates-io))

(define-public crate-extattr-0.1.0 (c (n "extattr") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "errno") (r "^0.2.8") (d #t) (k 0)) (d (n "libc") (r "^0.2.137") (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)))) (h "17axyklg4kvzv54545ygbz3514fdppwspd1bvdyc7a2aya893dl8")))

(define-public crate-extattr-0.1.1 (c (n "extattr") (v "0.1.1") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "errno") (r "^0.2.8") (d #t) (k 0)) (d (n "libc") (r "^0.2.137") (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)))) (h "12n3hm2q9bgj3d1kka34yxl4rha3d5h0fc5b0whjvi5b0chkkmbc")))

(define-public crate-extattr-1.0.0 (c (n "extattr") (v "1.0.0") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "errno") (r "^0.2.8") (d #t) (k 0)) (d (n "libc") (r "^0.2.138") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)))) (h "1p0m538xczfps6vf15az9wc69xmx85cm7z6sb9wipzqpg2kzhn8v")))

