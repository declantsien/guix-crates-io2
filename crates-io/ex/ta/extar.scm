(define-module (crates-io ex ta extar) #:use-module (crates-io))

(define-public crate-extar-0.1.0 (c (n "extar") (v "0.1.0") (h "1029xsh1jl2sb3q2zfgx3wwr312ar8n0fsvmxxnbn31gi1ypkxnc")))

(define-public crate-extar-0.1.1 (c (n "extar") (v "0.1.1") (h "1vpb4y980bj2znmzddj0bw4cg4b8lmsq8s8dba04hzy7kjfwf0xx")))

