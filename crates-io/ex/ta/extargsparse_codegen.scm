(define-module (crates-io ex ta extargsparse_codegen) #:use-module (crates-io))

(define-public crate-extargsparse_codegen-0.1.0 (c (n "extargsparse_codegen") (v "0.1.0") (d (list (d (n "bytes") (r "^0.5.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "log4rs") (r "^0.8.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.32") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "018cnhy9vyy6b021my2sn23mj4245kv4xqjjxx7nvxc25jr6ihws")))

(define-public crate-extargsparse_codegen-0.1.2 (c (n "extargsparse_codegen") (v "0.1.2") (d (list (d (n "bytes") (r "^0.5.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.32") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "1l8sv78wp4jxy71chs1wg2awy3m94ad0vhbx3knxw457xq1pa9a6") (r "1.59.0")))

(define-public crate-extargsparse_codegen-0.1.4 (c (n "extargsparse_codegen") (v "0.1.4") (d (list (d (n "bytes") (r "^0.5.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.32") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "1ibn6sg7bxbwg7g53jbchyxy6bx9z8glq3dg3nmwry3cwz1wi0bs") (r "1.59.0")))

