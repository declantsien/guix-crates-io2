(define-module (crates-io ex ta extabs) #:use-module (crates-io))

(define-public crate-extabs-0.1.0 (c (n "extabs") (v "0.1.0") (h "0ha3b44dckc66jb8ylpc45yncqf1ffm90h9j8d8vgmq6ny59dln8")))

(define-public crate-extabs-1.0.0 (c (n "extabs") (v "1.0.0") (h "0l0cik1kx8q13j1kc85vz8ac96pj2ly91sn853827mj4vnf0p2ry")))

(define-public crate-extabs-1.0.1 (c (n "extabs") (v "1.0.1") (h "1p25naa20n181wvbd6cdmdr58w5c6nz75zc7jhy7c3x8ya3djgnx")))

(define-public crate-extabs-1.0.2 (c (n "extabs") (v "1.0.2") (h "1lp1lycdkxs1025nhn7rnb5jqvv02v2pcd11ifmqgjjpjqrqikmz")))

(define-public crate-extabs-1.0.3 (c (n "extabs") (v "1.0.3") (h "1vfkc8wnx8sbhy3hxrj06ccjsnb7g7xv61zb4fickck1wzi2gxzh")))

