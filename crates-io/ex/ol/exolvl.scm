(define-module (crates-io ex ol exolvl) #:use-module (crates-io))

(define-public crate-exolvl-0.5.0 (c (n "exolvl") (v "0.5.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "leb128") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0qdifrqwk18k8zhlvq8d6whvm0v6lirmhp7m4j30a9gz7a0wk3dy") (s 2) (e (quote (("serde" "dep:serde" "chrono/serde"))))))

(define-public crate-exolvl-0.6.0 (c (n "exolvl") (v "0.6.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "leb128") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0jarpcwrdpywkdwx9kiprdzxg1swjwwvbj3jc8kicbc60ff194zs") (s 2) (e (quote (("serde" "dep:serde" "chrono/serde"))))))

