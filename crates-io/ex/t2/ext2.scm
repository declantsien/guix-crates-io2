(define-module (crates-io ex t2 ext2) #:use-module (crates-io))

(define-public crate-ext2-0.0.1-draft (c (n "ext2") (v "0.0.1-draft") (h "1c3zacgszm6xwmd2wslnz57x0vhljmsjndg520ghwyryvvfnsccz") (y #t)))

(define-public crate-ext2-0.0.2-alpha (c (n "ext2") (v "0.0.2-alpha") (d (list (d (n "bit_field") (r "^0.10.2") (d #t) (k 0)) (d (n "bitflags") (r "^2.3.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.146") (d #t) (k 0)) (d (n "nix") (r "^0.26.2") (f (quote ("fs"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1aj3xcm9z2j2hs013ip1sxldayxn2rkfd88f5vxi9f3437nmvpkn") (f (quote (("unstable")))) (y #t)))

(define-public crate-ext2-0.0.3-alpha (c (n "ext2") (v "0.0.3-alpha") (d (list (d (n "bit_field") (r "^0.10.2") (d #t) (k 0)) (d (n "bitflags") (r "^2.3.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.146") (d #t) (k 0)) (d (n "nix") (r "^0.26.2") (f (quote ("fs"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "08m5il54c147zq90400nqzxrql3vrrh1dnq6x5aybgwvd44zvs6a") (f (quote (("unstable")))) (y #t)))

(define-public crate-ext2-0.0.4-beta (c (n "ext2") (v "0.0.4-beta") (d (list (d (n "bit_field") (r "^0.10.2") (d #t) (k 0)) (d (n "bitflags") (r "^2.3.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.146") (d #t) (k 0)) (d (n "nix") (r "^0.26.2") (f (quote ("fs"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0ri68mk1mh4rbsd0xvvl74bmh15j2b274427rrf1lx67jybdnqmv") (f (quote (("unstable")))) (y #t)))

(define-public crate-ext2-0.1.0 (c (n "ext2") (v "0.1.0") (d (list (d (n "bit_field") (r "^0.10.2") (d #t) (k 0)) (d (n "bitflags") (r "^2.3.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.146") (d #t) (k 0)) (d (n "nix") (r "^0.26.2") (f (quote ("fs"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0zmpa7a5byxfwqvabx5nlwlnqy96281q92f7ias76hqlgdmxb5nl") (f (quote (("unstable"))))))

(define-public crate-ext2-0.1.1 (c (n "ext2") (v "0.1.1") (d (list (d (n "bit_field") (r "^0.10.2") (d #t) (k 0)) (d (n "bitflags") (r "^2.3.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.146") (d #t) (k 0)) (d (n "nix") (r "^0.26.2") (f (quote ("fs"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "14xryb0ca7q3ry2qskyy8a37l3v6vz4rihsxp8ywqjvhs8s5kxza") (f (quote (("unstable"))))))

