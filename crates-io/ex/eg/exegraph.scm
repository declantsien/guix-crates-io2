(define-module (crates-io ex eg exegraph) #:use-module (crates-io))

(define-public crate-exegraph-0.1.0 (c (n "exegraph") (v "0.1.0") (h "1brij95531hn4ygb90zwl25m7mdyljywgb1ab572j20yg07qdnf4")))

(define-public crate-exegraph-0.2.0 (c (n "exegraph") (v "0.2.0") (h "146qj6r4dfm56pnr6my1zvnwlynlgahjaq1n2q48s47r2xfrcbsg")))

(define-public crate-exegraph-0.3.0 (c (n "exegraph") (v "0.3.0") (h "0s93kfzvq5kdqybaf81valmg7pmi9hx20a668da3zrb7drgw5bxl")))

(define-public crate-exegraph-0.3.1 (c (n "exegraph") (v "0.3.1") (h "19pgv0zw9fq2iwq0xand2rj6agqnlcxy9nxgls6944xz25rs40ga")))

