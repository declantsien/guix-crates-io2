(define-module (crates-io ex t- ext-trait) #:use-module (crates-io))

(define-public crate-ext-trait-0.1.0-rc1 (c (n "ext-trait") (v "0.1.0-rc1") (d (list (d (n "ext-trait-proc_macros") (r "^0.1.0-rc1") (d #t) (k 0)))) (h "05zrhw08b8nyndi2yw10a4gbp7g8qlkrk18x8p7vhrq8pfnllgvp") (f (quote (("ui-tests" "better-docs") ("better-docs"))))))

(define-public crate-ext-trait-0.1.0-rc2 (c (n "ext-trait") (v "0.1.0-rc2") (d (list (d (n "ext-trait-proc_macros") (r "^0.1.0-rc2") (d #t) (k 0)))) (h "1mmm7ibpiwm9ygn8b1y7qx1sxnkrdd59g0j3qds1ai2kj63ic8ki") (f (quote (("ui-tests" "better-docs") ("better-docs"))))))

(define-public crate-ext-trait-1.0.0 (c (n "ext-trait") (v "1.0.0") (d (list (d (n "ext-trait-proc_macros") (r "^1.0.0") (d #t) (k 0)))) (h "0rm7frllvx7r0qvji2i3rhcawmfny7cz8xnl680l86c5002yn3hi") (f (quote (("ui-tests" "better-docs") ("better-docs"))))))

(define-public crate-ext-trait-1.0.1 (c (n "ext-trait") (v "1.0.1") (d (list (d (n "ext-trait-proc_macros") (r "^1.0.1") (d #t) (k 0)))) (h "1r8hld1n5ld2dghc918sm7b662786m1033mn5xqn6ybp38fdywnp") (f (quote (("ui-tests" "better-docs") ("better-docs"))))))

