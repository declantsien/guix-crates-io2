(define-module (crates-io ex t- ext-mgr) #:use-module (crates-io))

(define-public crate-ext-mgr-0.1.0 (c (n "ext-mgr") (v "0.1.0") (d (list (d (n "bytes") (r "^1.4.0") (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "libloading") (r "^0.7.4") (d #t) (k 0)))) (h "0y7lvlbdyrgf070qfyjl4bxpzvxsyzj7yfd0nl71biibnib9m21l") (y #t)))

(define-public crate-ext-mgr-0.1.1 (c (n "ext-mgr") (v "0.1.1") (d (list (d (n "bytes") (r "^1.4.0") (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "libloading") (r "^0.7.4") (d #t) (k 0)))) (h "0c0wb30m43a5jnz06jwgkklfqa8k8ryh8q1xab43bi2w6jdhk8fk") (y #t)))

(define-public crate-ext-mgr-0.1.2 (c (n "ext-mgr") (v "0.1.2") (d (list (d (n "bytes") (r "^1.4.0") (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "libloading") (r "^0.7.4") (d #t) (k 0)))) (h "0f16f1i0rblqnwxx5640gsjn9w4nr9iblk6lh7failwhvi2m2ma0") (y #t)))

(define-public crate-ext-mgr-0.1.3 (c (n "ext-mgr") (v "0.1.3") (d (list (d (n "bytes") (r "^1.4.0") (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "libloading") (r "^0.7.4") (d #t) (k 0)))) (h "1f6nw5hyds0pm269njbh2x5mqv4ib5111w9r6dv67ygg8w5ylvai")))

