(define-module (crates-io ex t- ext-trait-proc_macros) #:use-module (crates-io))

(define-public crate-ext-trait-proc_macros-0.1.0-rc1 (c (n "ext-trait-proc_macros") (v "0.1.0-rc1") (d (list (d (n "proc-macro2") (r "^1.0.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.7") (f (quote ("full"))) (d #t) (k 0)))) (h "0n07in7j6ay3pwffsyqsxrvmxlsg7x26gq72w57yblc6hwy4fmhp")))

(define-public crate-ext-trait-proc_macros-0.1.0-rc2 (c (n "ext-trait-proc_macros") (v "0.1.0-rc2") (d (list (d (n "proc-macro2") (r "^1.0.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.7") (f (quote ("full"))) (d #t) (k 0)))) (h "0k6n6l75cwxc80ab0jr9d1ahqs4vp88q6gz555bqk2rhai5023lz")))

(define-public crate-ext-trait-proc_macros-1.0.0 (c (n "ext-trait-proc_macros") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1.0.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.7") (f (quote ("full"))) (d #t) (k 0)))) (h "14fdvaichxkfqghygspdyasx3dbibrka2frf2sqyxnnarswwy6hr")))

(define-public crate-ext-trait-proc_macros-1.0.1 (c (n "ext-trait-proc_macros") (v "1.0.1") (d (list (d (n "proc-macro2") (r "^1.0.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.7") (f (quote ("full"))) (d #t) (k 0)))) (h "16k3ml8pz79v9klcjwvh6lqlrbymi9072wwzmajnmwpaa90r7dqs")))

