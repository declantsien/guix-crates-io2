(define-module (crates-io ex t- ext-timeout) #:use-module (crates-io))

(define-public crate-ext-timeout-0.1.0 (c (n "ext-timeout") (v "0.1.0") (d (list (d (n "ext-common") (r "^0.1.0") (d #t) (k 0)))) (h "08qhaycpa34d23pmdczrqrd1hfpghzig20gmhs0vmcrs642wghgf")))

(define-public crate-ext-timeout-0.1.1 (c (n "ext-timeout") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ext-common") (r "^0.1.1") (d #t) (k 0)) (d (n "humantime") (r "^2.1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.18.2") (f (quote ("rt" "process" "macros" "rt-multi-thread" "sync" "time"))) (d #t) (k 0)))) (h "0d1arip0fbws4qx7k0prryclpnvph7jmph8wagqarv9kq9arrv4n")))

