(define-module (crates-io ex t- ext-common) #:use-module (crates-io))

(define-public crate-ext-common-0.1.0 (c (n "ext-common") (v "0.1.0") (h "00hmlmjpiq4zygfak886z9405kqcvz6mixfk52v9rlqsqjmh7ah6")))

(define-public crate-ext-common-0.1.1 (c (n "ext-common") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "clap") (r "^3.1.18") (f (quote ("cargo" "env" "wrap_help"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.34") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.11") (f (quote ("fmt"))) (d #t) (k 0)))) (h "1wp1hskp25gdv89dfhbglfsjfsnq5wgsl21l2r1jjdf2q3y2rwi6")))

