(define-module (crates-io ex t- ext-string) #:use-module (crates-io))

(define-public crate-ext-string-0.1.0 (c (n "ext-string") (v "0.1.0") (h "0nl4a6pa3rpi3h9qnf5a1nssxnfhwpqn1s1fcxy2ljsg79xvg7nw") (y #t)))

(define-public crate-ext-string-0.1.1 (c (n "ext-string") (v "0.1.1") (h "10a52d1i93q3zr1pb4lp3jh8ggvhpk1v524cgrwc6jp9ylxcm4gb") (y #t)))

(define-public crate-ext-string-0.1.2 (c (n "ext-string") (v "0.1.2") (h "10s2kcf22pm6mnkns3ys4wxrm3ggjhrh1vvp833lpna0mx0ccqd6") (y #t)))

(define-public crate-ext-string-0.1.3 (c (n "ext-string") (v "0.1.3") (h "0vi8pfi1x5f9qkvlvchpg28spiqgf13ws9v44ynf9aw71lmaabd4")))

(define-public crate-ext-string-0.1.5 (c (n "ext-string") (v "0.1.5") (d (list (d (n "unicode-segmentation") (r "^1.6.0") (d #t) (k 0)))) (h "0s6mqrd2cwi8qpa5gyx2c6xgqn3fwxzlq900mj4sk24qnqf5bkni")))

