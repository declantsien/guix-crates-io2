(define-module (crates-io ex os exosphere-core) #:use-module (crates-io))

(define-public crate-exosphere-core-0.0.1 (c (n "exosphere-core") (v "0.0.1") (d (list (d (n "rmp-serde") (r "^1.1.0") (d #t) (k 0)) (d (n "rmpv") (r "^1.0.0") (f (quote ("with-serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "027kh7h796mbrp9k7i4k9jmyvmgnwa82gnmjdhgmx9z9m0ygxa59")))

(define-public crate-exosphere-core-0.0.2 (c (n "exosphere-core") (v "0.0.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "rmp-serde") (r "^1.1.0") (d #t) (k 0)) (d (n "rmpv") (r "^1.0.0") (f (quote ("with-serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0skj2rq4jm14b5dvbh0jzc9j7bdmyx0mnypg10pj64jd0gh5a93y")))

