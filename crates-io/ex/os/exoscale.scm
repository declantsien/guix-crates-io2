(define-module (crates-io ex os exoscale) #:use-module (crates-io))

(define-public crate-exoscale-2.0.0 (c (n "exoscale") (v "2.0.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "0rja15qibd4l76jfq986wv42891cpyivphfz66x5nx06vadhi8sw")))

(define-public crate-exoscale-2.1.0 (c (n "exoscale") (v "2.1.0") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "hmac") (r "^0.12") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("net" "macros"))) (d #t) (k 2)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "0kbpijc5xvlmkal1f8l4pnrlch4bjnks9wxswghbvwnj212g45ll")))

