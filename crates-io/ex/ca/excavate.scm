(define-module (crates-io ex ca excavate) #:use-module (crates-io))

(define-public crate-excavate-0.2.0 (c (n "excavate") (v "0.2.0") (d (list (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "nom") (r "^7.1.2") (d #t) (k 0)))) (h "02i530ym8ywldw6cznpvjgdlm28i2bawxl9kn1kk27v25advk28c")))

(define-public crate-excavate-0.3.0 (c (n "excavate") (v "0.3.0") (d (list (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "nom") (r "^7.1.2") (d #t) (k 0)))) (h "00wl37rwd2ydhqpyymh837ibpsnhn8fr162z2ik6cb35jakf0vkd")))

