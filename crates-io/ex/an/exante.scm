(define-module (crates-io ex an exante) #:use-module (crates-io))

(define-public crate-exante-0.1.0 (c (n "exante") (v "0.1.0") (d (list (d (n "base64") (r "^0.21.5") (d #t) (k 0)) (d (n "derive-new") (r "^0.5.9") (d #t) (k 0)) (d (n "http") (r "^0.2.9") (d #t) (k 0)) (d (n "rustify") (r "^0.5.3") (d #t) (k 0)) (d (n "rustify_derive") (r "^0.5.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.190") (d #t) (k 0)) (d (n "tokio") (r "^1.33.0") (f (quote ("rt" "macros"))) (d #t) (k 2)))) (h "114m1d2sqaaal6xsgfnw8z18s5hh8w0m6xaizwqsz3x5jg6g7xpy")))

(define-public crate-exante-0.2.0 (c (n "exante") (v "0.2.0") (d (list (d (n "base64") (r "^0.21.5") (d #t) (k 0)) (d (n "derive-new") (r "^0.6.0") (d #t) (k 0)) (d (n "http") (r "^0.2.9") (d #t) (k 0)) (d (n "rustify") (r "^0.5.3") (d #t) (k 0)) (d (n "rustify_derive") (r "^0.5.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.190") (d #t) (k 0)) (d (n "tokio") (r "^1.33.0") (f (quote ("rt" "macros"))) (d #t) (k 2)))) (h "0l8gcf6pp1ghaqjrq8h8j24kb9d90xjd0dzwisfkkidfsjsdn88l")))

