(define-module (crates-io ex em exemplar_proc_macro) #:use-module (crates-io))

(define-public crate-exemplar_proc_macro-0.0.0 (c (n "exemplar_proc_macro") (v "0.0.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.68") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full"))) (d #t) (k 0)))) (h "1442zwkz2m6272yx5f0s5378gi5rlsf0sxsb1qfh5kbl7x4kq1g5")))

(define-public crate-exemplar_proc_macro-0.5.0 (c (n "exemplar_proc_macro") (v "0.5.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.68") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (d #t) (k 0)))) (h "0xhdidsn4fnjb5s0pmwmfsfrxcwwngdbhp3mprxq1lgrqzb7vljb")))

(define-public crate-exemplar_proc_macro-0.5.1 (c (n "exemplar_proc_macro") (v "0.5.1") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.68") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (d #t) (k 0)))) (h "13cqzk5f5givz73a5hd9d4kx5pk8x5dxgvklfq55hy20zjm1y3kv")))

(define-public crate-exemplar_proc_macro-0.5.2 (c (n "exemplar_proc_macro") (v "0.5.2") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.68") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (d #t) (k 0)))) (h "1scz4qvbjb4f4zvhls73l9n3y7fiz5h0i9zxh3gwrilhj3276lhh")))

(define-public crate-exemplar_proc_macro-0.5.4 (c (n "exemplar_proc_macro") (v "0.5.4") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.68") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (d #t) (k 0)))) (h "1yr0zz8lsil12cyrh5v08r5pb5jpgr94wq30610bfn46177s7nhi")))

(define-public crate-exemplar_proc_macro-0.5.5 (c (n "exemplar_proc_macro") (v "0.5.5") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.68") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (d #t) (k 0)))) (h "1a6aljy2p0ydhz31nkip7cw56nfkkvy6mnc1zxm1sfx104x0sagc")))

(define-public crate-exemplar_proc_macro-0.5.6 (c (n "exemplar_proc_macro") (v "0.5.6") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.68") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (d #t) (k 0)))) (h "1idz6847f6jvnqsyizyswsd95rflfxbnvg029cjpb1001hd15hrb")))

(define-public crate-exemplar_proc_macro-0.5.7 (c (n "exemplar_proc_macro") (v "0.5.7") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.68") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (d #t) (k 0)))) (h "0a78l0qj23480hmk66a1d9ij075604n26kghahpm4r160w1cmgwx")))

(define-public crate-exemplar_proc_macro-0.6.1 (c (n "exemplar_proc_macro") (v "0.6.1") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.68") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (d #t) (k 0)))) (h "1kkysvy108i9f4lriwqhwxsldj96igi6nlsd269n8sqvxpd515qa")))

(define-public crate-exemplar_proc_macro-0.6.3 (c (n "exemplar_proc_macro") (v "0.6.3") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.68") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (d #t) (k 0)))) (h "0hnj7wfpwfk5d6pkwdws22yf4viqx74m96ba30af4wnp0mny6bn9")))

(define-public crate-exemplar_proc_macro-0.7.0 (c (n "exemplar_proc_macro") (v "0.7.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.68") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (d #t) (k 0)))) (h "1x7kf59j757g8sjf07nryz3cnvdjckhp2f803dna3wx2c1v0ssvb")))

(define-public crate-exemplar_proc_macro-0.7.1 (c (n "exemplar_proc_macro") (v "0.7.1") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.68") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (d #t) (k 0)))) (h "0rfip64cj62lzpkxm4sxkv6jss82wla4j4y67ydimhmislzg9b48")))

