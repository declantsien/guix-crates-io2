(define-module (crates-io ex em exempi2) #:use-module (crates-io))

(define-public crate-exempi2-0.1.0 (c (n "exempi2") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "exempi-sys") (r "^2.5.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "03sik1szbsa81r2s3hq4vj6ay1gr0cvj5ckvsnli6ql68fn63nmp")))

(define-public crate-exempi2-0.1.1 (c (n "exempi2") (v "0.1.1") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "exempi-sys") (r "^2.5.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0zda2rr8fwgb1ll96k4274df4aqflj7vfv7w2k3klpx4wwkmbsqx")))

(define-public crate-exempi2-0.1.2 (c (n "exempi2") (v "0.1.2") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "exempi-sys") (r "^2.5.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1inixdy1g0pln57ylminh7xdbr2cv480q5563g6l3xpnbfd7c3bn")))

(define-public crate-exempi2-0.1.3 (c (n "exempi2") (v "0.1.3") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "exempi-sys") (r "^2.5.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "15hzyg1yc5pwzl4rvv9x4srj944c81z593cs5igxxcfw0ffj56ld")))

(define-public crate-exempi2-0.1.4 (c (n "exempi2") (v "0.1.4") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "exempi-sys") (r "^2.5.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "050mvdxi5l46dppiy5kx3ixpk8z8k4aypmsa7b1b95f7w9mlv0bx")))

(define-public crate-exempi2-0.2.0 (c (n "exempi2") (v "0.2.0") (d (list (d (n "bitflags") (r "^2.3.3") (d #t) (k 0)) (d (n "exempi-sys") (r "^2.5.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "18cg1zgrzkvxnwh1x84gx4rkrn8nlfygzswz8fh70zs1rxr5azbp")))

