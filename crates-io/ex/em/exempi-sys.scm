(define-module (crates-io ex em exempi-sys) #:use-module (crates-io))

(define-public crate-exempi-sys-2.3.0 (c (n "exempi-sys") (v "2.3.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "11cnqsbqmxgxvkdzp0n1l8fymalzpwwlscl8c7gx3zaajavn3qj0")))

(define-public crate-exempi-sys-2.4.0 (c (n "exempi-sys") (v "2.4.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1prxpm21b69y6mrx3hqx1fyzhhwi2nqd4wvkybc0d4ijjfjib4qg")))

(define-public crate-exempi-sys-2.4.1 (c (n "exempi-sys") (v "2.4.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1d2n9vk1axqvjb49m8zs9id8jcjm4ag52w3i55qaz3jiz4989vgs")))

(define-public crate-exempi-sys-2.5.0 (c (n "exempi-sys") (v "2.5.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0dvfd6zab8sarl91brzigay37v9ckgnwiz71fjhg45b1hcbx6bh9")))

(define-public crate-exempi-sys-2.5.1 (c (n "exempi-sys") (v "2.5.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0kzw4hfl6hfh13v69zxgjf33jb6pkwldmy7wrngzz9pchxr6vdpy")))

(define-public crate-exempi-sys-2.5.2 (c (n "exempi-sys") (v "2.5.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1rfp93z98kf1mqxddjad974fddd5h6nb3q1d8fr5pkc7cx1xakgb")))

(define-public crate-exempi-sys-2.5.3 (c (n "exempi-sys") (v "2.5.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1azx5ihc9ipi916kxy5bacncdlihc71mm36w77x8ss0d3yckblvm")))

