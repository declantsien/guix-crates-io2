(define-module (crates-io ex em exemplify) #:use-module (crates-io))

(define-public crate-exemplify-0.1.0 (c (n "exemplify") (v "0.1.0") (d (list (d (n "clap") (r "^3.0.0-beta.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "exemplify-lib") (r "^0.1.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.5") (d #t) (k 0)) (d (n "tokio") (r "^0.2.22") (f (quote ("macros"))) (d #t) (k 0)))) (h "1xsj7dmqiyy65scfmcdxlkzkr5v352rqjp602rk9qqfql598jvqf")))

(define-public crate-exemplify-0.1.1 (c (n "exemplify") (v "0.1.1") (d (list (d (n "clap") (r "^3.0.0-beta.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "exemplify-lib") (r "^0.1.1") (d #t) (k 0)) (d (n "futures") (r "^0.3.5") (d #t) (k 0)) (d (n "tokio") (r "^0.2.22") (f (quote ("macros"))) (d #t) (k 0)))) (h "0nxcqs75p7qh78qliy8bsr4wq5s26vw1hi5d1nd8xjyrygbf9jcj")))

(define-public crate-exemplify-0.1.2 (c (n "exemplify") (v "0.1.2") (d (list (d (n "clap") (r "^3.0.0-beta.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "exemplify-lib") (r "^0.1.1") (d #t) (k 0)) (d (n "futures") (r "^0.3.5") (d #t) (k 0)) (d (n "tokio") (r "^0.2.22") (f (quote ("macros"))) (d #t) (k 0)))) (h "1kxaan6kjxkggvivqcyjp3c380j0pm4kmlp7lllp0v7mmkiy012q")))

(define-public crate-exemplify-0.1.3 (c (n "exemplify") (v "0.1.3") (d (list (d (n "clap") (r "^3.0.0-beta.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "exemplify-lib") (r "^0.1.3") (d #t) (k 0)) (d (n "futures") (r "^0.3.5") (d #t) (k 0)) (d (n "tokio") (r "^0.2.22") (f (quote ("macros"))) (d #t) (k 0)))) (h "1csnx25ynv48gsjiq6cv0kljgsynp3c74czh7bgvsiv32rj4qbvb")))

(define-public crate-exemplify-0.1.4 (c (n "exemplify") (v "0.1.4") (d (list (d (n "clap") (r "^3.0.0-beta.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "exemplify-lib") (r "^0.1.3") (d #t) (k 0)) (d (n "futures") (r "^0.3.5") (d #t) (k 0)) (d (n "tokio") (r "^0.2.22") (f (quote ("macros"))) (d #t) (k 0)))) (h "004cpmhl17xn4rw93d565gcxsi0f6nxjk8gkdja0n9dyr1sc3q36")))

(define-public crate-exemplify-0.1.5 (c (n "exemplify") (v "0.1.5") (d (list (d (n "clap") (r "^3.0.0-beta.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "exemplify-lib") (r "^0.1.5") (d #t) (k 0)) (d (n "futures") (r "^0.3.5") (d #t) (k 0)) (d (n "tokio") (r "^0.2.22") (f (quote ("macros"))) (d #t) (k 0)))) (h "1213y82j9bm71sjajmnbgswk6yrgz5s1a5r0ly8621h0k3xy2li1")))

(define-public crate-exemplify-0.1.6 (c (n "exemplify") (v "0.1.6") (d (list (d (n "clap") (r "^3.0.0-beta.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "exemplify-lib") (r "^0.1.6") (d #t) (k 0)) (d (n "futures") (r "^0.3.5") (d #t) (k 0)) (d (n "tokio") (r "^0.2.22") (f (quote ("macros"))) (d #t) (k 0)))) (h "1l8ahff55zbcbbv62gll5nw5pxm77g7nmgsh5wsc2wbalbr3xs7d")))

