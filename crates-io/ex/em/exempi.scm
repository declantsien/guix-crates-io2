(define-module (crates-io ex em exempi) #:use-module (crates-io))

(define-public crate-exempi-2.3.0 (c (n "exempi") (v "2.3.0") (d (list (d (n "bitflags") (r "^0.5.0") (d #t) (k 0)) (d (n "exempi-sys") (r "^2.3.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "19hgiihxcrqr2zkcp1133sf2v3shkvr9lm7pmw2r0xvr10p9kn7l")))

(define-public crate-exempi-2.4.0 (c (n "exempi") (v "2.4.0") (d (list (d (n "bitflags") (r "^0.5.0") (d #t) (k 0)) (d (n "exempi-sys") (r "^2.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "16adb51ch901m5iji7n534661bn1d33g47jir70bif3v8mdl0fa6")))

(define-public crate-exempi-2.4.1 (c (n "exempi") (v "2.4.1") (d (list (d (n "bitflags") (r "^0.5.0") (d #t) (k 0)) (d (n "exempi-sys") (r "^2.4.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1mgakcamr65pp8dd6b1dbkpch1cfm3r4jfp651mxvhs6fw5qysq5")))

(define-public crate-exempi-2.4.2 (c (n "exempi") (v "2.4.2") (d (list (d (n "bitflags") (r "^0.5.0") (d #t) (k 0)) (d (n "exempi-sys") (r "^2.4.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1cipzpxk10959h3pvzpmdqfgyvf280wv6j4k6fh30lvi6xr1fkrb")))

(define-public crate-exempi-2.4.3 (c (n "exempi") (v "2.4.3") (d (list (d (n "bitflags") (r "^0.5.0") (d #t) (k 0)) (d (n "exempi-sys") (r "^2.4.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "16lggbmsblg9fclv4rn759whdjv08p9228wcdmzcqcjchcnhanr6")))

(define-public crate-exempi-2.4.4 (c (n "exempi") (v "2.4.4") (d (list (d (n "bitflags") (r "^0.5.0") (d #t) (k 0)) (d (n "exempi-sys") (r "^2.4.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1ydqj4r3nvlkmxl55yviighwbr4aap6jy813rr3syl2c7821v6id")))

(define-public crate-exempi-2.5.0 (c (n "exempi") (v "2.5.0") (d (list (d (n "bitflags") (r "^0.5.0") (d #t) (k 0)) (d (n "exempi-sys") (r "^2.5.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0hra9idqkid27js4w85rscyv5qbxnjpkp536lb7k4zah0ilp7b0f")))

(define-public crate-exempi-2.5.1 (c (n "exempi") (v "2.5.1") (d (list (d (n "bitflags") (r "^0.5.0") (d #t) (k 0)) (d (n "exempi-sys") (r "^2.5.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "16pkfypw0y7pslj5i8h1il99xfb6l33nf3vx4d1xx4xak1rds7b1")))

