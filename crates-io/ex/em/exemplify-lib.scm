(define-module (crates-io ex em exemplify-lib) #:use-module (crates-io))

(define-public crate-exemplify-lib-0.1.0 (c (n "exemplify-lib") (v "0.1.0") (d (list (d (n "futures") (r "^0.3.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "stringreader") (r "^0.1.1") (d #t) (k 2)) (d (n "tokio") (r "^0.2.22") (f (quote ("rt-threaded" "macros"))) (d #t) (k 2)))) (h "1zwd7q99blc4b76m0xsrdaff84bya6acm03f624fziq4fw64lv57")))

(define-public crate-exemplify-lib-0.1.1 (c (n "exemplify-lib") (v "0.1.1") (d (list (d (n "futures") (r "^0.3.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "stringreader") (r "^0.1.1") (d #t) (k 2)) (d (n "tokio") (r "^0.2.22") (f (quote ("rt-threaded" "macros"))) (d #t) (k 2)))) (h "04a83ar9mzbz30picy4fd5mgd8833qdcja15s63s9vi04fp3c2al")))

(define-public crate-exemplify-lib-0.1.2 (c (n "exemplify-lib") (v "0.1.2") (d (list (d (n "futures") (r "^0.3.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "stringreader") (r "^0.1.1") (d #t) (k 2)) (d (n "tokio") (r "^0.2.22") (f (quote ("rt-threaded" "macros"))) (d #t) (k 2)))) (h "0mvz9l50v28zncaf2bxl7p6ps72yjsh4xphqbv1zsw7a9dpwqgak")))

(define-public crate-exemplify-lib-0.1.3 (c (n "exemplify-lib") (v "0.1.3") (d (list (d (n "futures") (r "^0.3.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "stringreader") (r "^0.1.1") (d #t) (k 2)) (d (n "tokio") (r "^0.2.22") (f (quote ("rt-threaded" "macros"))) (d #t) (k 2)))) (h "0y3vlnk4034cwkg8mbhnbcjrz3jm78v0a04ad4lp2ghxk41m6h64")))

(define-public crate-exemplify-lib-0.1.4 (c (n "exemplify-lib") (v "0.1.4") (d (list (d (n "futures") (r "^0.3.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "stringreader") (r "^0.1.1") (d #t) (k 2)) (d (n "tokio") (r "^0.2.22") (f (quote ("rt-threaded" "macros"))) (d #t) (k 2)))) (h "1d1r2bjg76i95f4nskhsdknrnqmnvp0mc65d0xniwxb60jar1ym6")))

(define-public crate-exemplify-lib-0.1.5 (c (n "exemplify-lib") (v "0.1.5") (d (list (d (n "futures") (r "^0.3.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "stringreader") (r "^0.1.1") (d #t) (k 2)) (d (n "tokio") (r "^0.2.22") (f (quote ("rt-threaded" "macros"))) (d #t) (k 2)))) (h "0hhz8v8gfrmjrs184ayiv3jnr07jcp3afdzbd3fn1r38hihh9vs0")))

(define-public crate-exemplify-lib-0.1.6 (c (n "exemplify-lib") (v "0.1.6") (d (list (d (n "futures") (r "^0.3.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "stringreader") (r "^0.1.1") (d #t) (k 2)) (d (n "tokio") (r "^0.2.22") (f (quote ("rt-threaded" "macros"))) (d #t) (k 2)))) (h "181nzk6hyxnq58a4mgcx310wp4hwyq98j1nhi2hy0zah23jmxnma")))

