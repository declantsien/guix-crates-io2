(define-module (crates-io ex em exemd) #:use-module (crates-io))

(define-public crate-exemd-0.1.0 (c (n "exemd") (v "0.1.0") (d (list (d (n "clap") (r "^3.0.0-beta.1") (d #t) (k 0)) (d (n "colored") (r "^1.8.0") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.7") (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 0)))) (h "0cqywgkfkqkazx27y1xf6wxn0ka82zgv93c894jnglyw4nm8h181")))

