(define-module (crates-io ex te extensor) #:use-module (crates-io))

(define-public crate-extensor-0.1.0 (c (n "extensor") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.11.3") (d #t) (k 2)) (d (n "extensor-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 2)))) (h "0scz5r2fqzp00gak8b8jmfcmrdspa0j518wzr9vh1iskxsgpwai3")))

(define-public crate-extensor-0.1.1 (c (n "extensor") (v "0.1.1") (d (list (d (n "env_logger") (r "^0.11.3") (d #t) (k 2)) (d (n "extensor-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 2)))) (h "1p7pqsnbsmfa5jcgx2r2d42vf9fbypp3yq8llaikv5wyf0c4mpp7")))

