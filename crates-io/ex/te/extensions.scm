(define-module (crates-io ex te extensions) #:use-module (crates-io))

(define-public crate-extensions-0.1.0 (c (n "extensions") (v "0.1.0") (d (list (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)))) (h "1a6b3bq2bpn9izpr1lwwir6jgcrllikymp3d1k866m795fs4yja1")))

(define-public crate-extensions-0.2.0 (c (n "extensions") (v "0.2.0") (d (list (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)))) (h "1q722lvy1984fj48m4m48x93xb6wh4744vd60f2483865fyp13r5")))

