(define-module (crates-io ex te extended-primitives) #:use-module (crates-io))

(define-public crate-extended-primitives-0.2.0 (c (n "extended-primitives") (v "0.2.0") (d (list (d (n "hex") (r "^0.3.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1") (d #t) (k 2)))) (h "1lin3kygx1yzfvvqabiv992vxbk0isqfpcwpj5bj60qlfl8d9p7h") (f (quote (("serialization" "serde") ("rng" "rand"))))))

(define-public crate-extended-primitives-0.3.0 (c (n "extended-primitives") (v "0.3.0") (d (list (d (n "encodings") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1") (d #t) (k 2)))) (h "1ff6h6y5pckzgjxd1z056vx13bah6cmrd342s5s0phdrm683vj86") (f (quote (("serialization" "serde") ("rng" "rand"))))))

(define-public crate-extended-primitives-0.3.1 (c (n "extended-primitives") (v "0.3.1") (d (list (d (n "encodings") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1") (d #t) (k 2)))) (h "1dy68zivpq14hdqxjlbp8wfavr1f29di3w093yh33z0dlmgzfmyy") (f (quote (("serialization" "serde") ("rng" "rand"))))))

(define-public crate-extended-primitives-0.3.2 (c (n "extended-primitives") (v "0.3.2") (d (list (d (n "encodings") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1") (d #t) (k 2)))) (h "14l46mclcmf97mcxrm603y25pavny4fcy4ygbh6fni34nx3cx7ww") (f (quote (("serialization" "serde") ("rng" "rand"))))))

(define-public crate-extended-primitives-0.3.3 (c (n "extended-primitives") (v "0.3.3") (d (list (d (n "encodings") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1") (d #t) (k 2)))) (h "1r1m07rvaqs5bvgb59dhwpszf6cjmqd8xnrjgia06l82sz7s4d94") (f (quote (("serialization" "serde") ("rng" "rand"))))))

(define-public crate-extended-primitives-0.3.4 (c (n "extended-primitives") (v "0.3.4") (d (list (d (n "encodings") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1") (d #t) (k 2)))) (h "15f9d4vksgwcnj89b91lqwp5lilipj1h6nwnvk0dp1003p0qzg46") (f (quote (("serialization" "serde") ("rng" "rand"))))))

(define-public crate-extended-primitives-0.3.5 (c (n "extended-primitives") (v "0.3.5") (d (list (d (n "encodings") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1") (d #t) (k 2)))) (h "1k3zjx67d38ml8x64g8qqrrpp4vjbaan8kijnww8pmcnnbj4ggra") (f (quote (("serialization" "serde") ("rng" "rand"))))))

(define-public crate-extended-primitives-0.3.6 (c (n "extended-primitives") (v "0.3.6") (d (list (d (n "encodings") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1") (d #t) (k 2)))) (h "0gpa7zj8k1indmzdvxz7f4y46yz12fl0c533kvzyxlvsabw6prvh") (f (quote (("serialization" "serde") ("rng" "rand"))))))

(define-public crate-extended-primitives-0.3.7 (c (n "extended-primitives") (v "0.3.7") (d (list (d (n "encodings") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0n9y99halzcgk0kibga9hj40wvy33wg1cikbmv64ixrd2qypp9wr") (f (quote (("serialization" "serde") ("rng" "rand"))))))

(define-public crate-extended-primitives-0.3.8 (c (n "extended-primitives") (v "0.3.8") (d (list (d (n "encodings") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0qhr9lz2h2qdnxnj8by4di366d5vprxcqkvgzbxl028wmyz27bam") (f (quote (("serialization" "serde") ("rng" "rand"))))))

