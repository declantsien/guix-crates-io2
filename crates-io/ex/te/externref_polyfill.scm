(define-module (crates-io ex te externref_polyfill) #:use-module (crates-io))

(define-public crate-externref_polyfill-0.0.0 (c (n "externref_polyfill") (v "0.0.0") (h "1xzwn1swk23xmck9pxc4klgrfnc6cj6s7nmdxiicv6l1jmf2h7b3")))

(define-public crate-externref_polyfill-0.0.1 (c (n "externref_polyfill") (v "0.0.1") (h "19kmf3p8j5iqdxnnb9lg00d15dha7gayjw2030d8r4av80xpgmxr")))

(define-public crate-externref_polyfill-0.0.2 (c (n "externref_polyfill") (v "0.0.2") (h "064qkf801g2fjr6ly5kd6a9xkjnbi7vxnh3jyffj6vmql1inz2aj")))

(define-public crate-externref_polyfill-0.1.0 (c (n "externref_polyfill") (v "0.1.0") (h "0dh4h68b6jq6b1mmrjsbbqd1nvlahn4vwc61kfjbavq2888hlq5x")))

