(define-module (crates-io ex te extendable_vm) #:use-module (crates-io))

(define-public crate-extendable_vm-0.1.0 (c (n "extendable_vm") (v "0.1.0") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 0)) (d (n "scanrs") (r "^0.2.1") (d #t) (k 0)))) (h "1z1s9mzxgk9igdiaf9pj3lwadl9v4lsq11ydgnxpya7vmxammb6m")))

(define-public crate-extendable_vm-0.1.1-releaseAttempt (c (n "extendable_vm") (v "0.1.1-releaseAttempt") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 0)) (d (n "scanrs") (r "^0.2.1") (d #t) (k 0)))) (h "0jgvif2ivbj589kan7z6h9aykik5c8lf8xvbrvz8916s4xz5wyyc") (y #t)))

(define-public crate-extendable_vm-0.1.1-releaseAttempt2 (c (n "extendable_vm") (v "0.1.1-releaseAttempt2") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 0)) (d (n "scanrs") (r "^0.2.1") (d #t) (k 0)))) (h "09xgldkghg0q0848qakw6kwqd9wsrmxxc5nix4shpmhzhi2jc49q") (y #t)))

(define-public crate-extendable_vm-0.1.1-releaseAttempt3 (c (n "extendable_vm") (v "0.1.1-releaseAttempt3") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 0)) (d (n "scanrs") (r "^0.2.1") (d #t) (k 0)))) (h "0lsap7gfwia9y700f6dkl3c362hg2inrj0ziyrbzgscqmfab4qxy") (y #t)))

(define-public crate-extendable_vm-0.1.1-releaseAttempt4 (c (n "extendable_vm") (v "0.1.1-releaseAttempt4") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 0)))) (h "1bs4gni4zf9j4z77p5zyfaznsz878n4fiwnacrws6mf3v2cj1ng9")))

(define-public crate-extendable_vm-0.1.1-releaseAttempt5 (c (n "extendable_vm") (v "0.1.1-releaseAttempt5") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 0)))) (h "1avm15avgjcnr5ppfrsnsp710zqah24cch1i64fzpy7p760bjpss")))

(define-public crate-extendable_vm-0.4.0 (c (n "extendable_vm") (v "0.4.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 0)))) (h "0z7wyc6sdi3idvm7bv9aj3r60rg6cvi62y0f395f3p22l52yp1fn")))

