(define-module (crates-io ex te extel) #:use-module (crates-io))

(define-public crate-extel-0.1.0 (c (n "extel") (v "0.1.0") (d (list (d (n "extel_parameterized") (r "^0.1.0") (d #t) (k 0)))) (h "10nczprnjj3f83rwdsfwa724i2115ba84zln02lipwiyhf6g02ri") (f (quote (("parameterized"))))))

(define-public crate-extel-0.1.1 (c (n "extel") (v "0.1.1") (d (list (d (n "extel_parameterized") (r "^0.1.0") (d #t) (k 0)))) (h "0hh0rbjvgf7r6zxk7fk12zawhh3yfhmvq65xcxkablkld2shbgrd") (f (quote (("parameterized"))))))

(define-public crate-extel-0.1.2 (c (n "extel") (v "0.1.2") (d (list (d (n "extel_parameterized") (r "^0.1.0") (d #t) (k 0)))) (h "0jscl8z9pja6abs97m7h8h4w943n3sha7vz48ciadaa5n2h46h6g") (f (quote (("parameterized"))))))

(define-public crate-extel-0.1.3 (c (n "extel") (v "0.1.3") (d (list (d (n "extel_parameterized") (r "^0.1.0") (d #t) (k 0)))) (h "0yhk14l3g9axnrw8jiq1jdfbl10rw6g34zvzchlf24vhr74ih7z6") (f (quote (("parameterized"))))))

(define-public crate-extel-0.2.0 (c (n "extel") (v "0.2.0") (d (list (d (n "extel_parameterized") (r "^0.1.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "175bkqkq322h56ghm9lg1pxdi9713vdpaivwd5n97749n675j8hz") (f (quote (("parameterized"))))))

(define-public crate-extel-0.2.1 (c (n "extel") (v "0.2.1") (d (list (d (n "extel_parameterized") (r "^0.1.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "1ixi3kdrv78ldx01w9z9108fq62hxz468pvcf20x5icyzi7zrmwr") (f (quote (("parameterized"))))))

(define-public crate-extel-0.2.2 (c (n "extel") (v "0.2.2") (d (list (d (n "extel_parameterized") (r "^0.1.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "0g6s4xjlkvala5wrhd4lnqgg4q94kslkfv0ll4dq4l3b68j74n5d") (f (quote (("parameterized"))))))

