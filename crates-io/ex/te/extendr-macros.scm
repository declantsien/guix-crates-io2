(define-module (crates-io ex te extendr-macros) #:use-module (crates-io))

(define-public crate-extendr-macros-0.1.0 (c (n "extendr-macros") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.6") (d #t) (k 0)) (d (n "syn") (r "^1.0.22") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1w973yrx2hxh5hf70yj9iv8qh8dzr3zmf3w5vp16620d727c8v21")))

(define-public crate-extendr-macros-0.1.1 (c (n "extendr-macros") (v "0.1.1") (d (list (d (n "quote") (r "^1.0.6") (d #t) (k 0)) (d (n "syn") (r "^1.0.22") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "19a5mhmdmq6k9s9azmsqjhyrf9yb0g114r3plixkvvsq9sr2fwyz")))

(define-public crate-extendr-macros-0.1.2 (c (n "extendr-macros") (v "0.1.2") (d (list (d (n "quote") (r "^1.0.6") (d #t) (k 0)) (d (n "syn") (r "^1.0.22") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "034j264qzxks64bcj346v3jp0405g44zrd7pd3b64k90y9c2x7pc")))

(define-public crate-extendr-macros-0.1.3 (c (n "extendr-macros") (v "0.1.3") (d (list (d (n "quote") (r "^1.0.6") (d #t) (k 0)) (d (n "syn") (r "^1.0.22") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0dr1c8l4r8c4ym33qpnh4v1gd5d5ghj9q6y1z312wl5mgsl1p94l")))

(define-public crate-extendr-macros-0.1.7 (c (n "extendr-macros") (v "0.1.7") (d (list (d (n "quote") (r ">=1.0.6, <2.0.0") (d #t) (k 0)) (d (n "syn") (r ">=1.0.22, <2.0.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1nrfw5969xbvm77vqzk2xvbi36lq3q3adq4c1c6ydbgfri7n87zh")))

(define-public crate-extendr-macros-0.1.8 (c (n "extendr-macros") (v "0.1.8") (d (list (d (n "quote") (r ">=1.0.6, <2.0.0") (d #t) (k 0)) (d (n "syn") (r ">=1.0.22, <2.0.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0mnvdi8cky8w6smby1vza6sxadcfqrcgnixkq4lw0bn8z7pyy5vv")))

(define-public crate-extendr-macros-0.1.9 (c (n "extendr-macros") (v "0.1.9") (d (list (d (n "quote") (r ">=1.0.6, <2.0.0") (d #t) (k 0)) (d (n "syn") (r ">=1.0.22, <2.0.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1g15xrf113zx25y6pyl1hrxik75bl7c0hqxn7w6if28y9afr4pbl")))

(define-public crate-extendr-macros-0.1.10 (c (n "extendr-macros") (v "0.1.10") (d (list (d (n "quote") (r ">=1.0.6, <2.0.0") (d #t) (k 0)) (d (n "syn") (r ">=1.0.22, <2.0.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "15js8n2wnn2xr6gx38nx5q7yy8qhrifnw9pj8icy7p8wbrgd7aax")))

(define-public crate-extendr-macros-0.2.0 (c (n "extendr-macros") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.6") (d #t) (k 0)) (d (n "syn") (r "^1.0.22") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "14wla6b2llrj38ygwbgl6ksa4wq34gjzic1s023gwc634316pkw2")))

(define-public crate-extendr-macros-0.3.0 (c (n "extendr-macros") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.6") (d #t) (k 0)) (d (n "syn") (r "^1.0.22") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "19kjjc23m3qn9wasmv8ij2i9hagv9nhd6l22kf8p0xc8af25j8kb")))

(define-public crate-extendr-macros-0.3.1 (c (n "extendr-macros") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.6") (d #t) (k 0)) (d (n "syn") (r "^1.0.22") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "037a46154nsp4516abxfsjrdplxryv1d9aw6iwbd1m9zf25d7mch")))

(define-public crate-extendr-macros-0.4.0 (c (n "extendr-macros") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.6") (d #t) (k 0)) (d (n "syn") (r "^1.0.22") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0rvckqdg669sz4f1fka6bz5mrvbz2d484dv3h6z0k0nly14higq9")))

(define-public crate-extendr-macros-0.6.0 (c (n "extendr-macros") (v "0.6.0") (d (list (d (n "libR-sys") (r "^0.6.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.6") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "115vdi0c552vz48f34h3nqs4jaxz0sarfyq9lfzy9b8ccmqkk0rk")))

