(define-module (crates-io ex te extension-trait) #:use-module (crates-io))

(define-public crate-extension-trait-0.1.0 (c (n "extension-trait") (v "0.1.0") (h "0zwr78z472s6qy8khqj6klfsr2cali7sx2bn0zjbap8dz4mfhi4g")))

(define-public crate-extension-trait-0.1.1 (c (n "extension-trait") (v "0.1.1") (h "0g2c66zyx50bjfd0ykb5fj613zrzwbm7qkhz4akyc4qhfnx6v6ll")))

(define-public crate-extension-trait-0.1.2 (c (n "extension-trait") (v "0.1.2") (h "0sl2kpni9grid1fav2v08yaspvr803lxk45clljlpalhk0mrk0vg")))

(define-public crate-extension-trait-0.1.3 (c (n "extension-trait") (v "0.1.3") (h "04b684flfqaqj0f68a02280lc2kryaq1qg6dqxcsb2glhdn5whrq")))

(define-public crate-extension-trait-0.2.0 (c (n "extension-trait") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.7") (f (quote ("full"))) (d #t) (k 0)))) (h "0x4f69i2lz0iz9kffa0nr413fqk0n4f5jjlymw2xra3gaabzzpdd")))

(define-public crate-extension-trait-0.2.1 (c (n "extension-trait") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.7") (f (quote ("full"))) (d #t) (k 0)))) (h "0pfq3iaq8z0cms04i8z48wn6c336mxp554s4w0dwf0dfrk3wbsfk")))

(define-public crate-extension-trait-0.2.2 (c (n "extension-trait") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.7") (f (quote ("full"))) (d #t) (k 0)))) (h "1aqnc4pw3qznf1gababm2nagbfgssy1plsqcb5xbn77xgg67n37n")))

(define-public crate-extension-trait-0.2.3 (c (n "extension-trait") (v "0.2.3") (d (list (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.7") (f (quote ("full"))) (d #t) (k 0)))) (h "01k1n11132578qmrirrh6hqykvcfbhb2wjzhmniw8g3f3hrgha2j")))

(define-public crate-extension-trait-1.0.0 (c (n "extension-trait") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.7") (f (quote ("full"))) (d #t) (k 0)))) (h "04fnxbihaa1pyly1qz5m4764smvyd29j4cfmxrr25gnkdjn7q3r8")))

(define-public crate-extension-trait-1.0.1 (c (n "extension-trait") (v "1.0.1") (d (list (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.7") (f (quote ("full"))) (d #t) (k 0)))) (h "0x2ddigqs2x2jjgbcg9p5bndvg5gn25gi6jjl3m4cd8qwf7hcaai")))

(define-public crate-extension-trait-1.0.2 (c (n "extension-trait") (v "1.0.2") (d (list (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^2.0.3") (f (quote ("full"))) (d #t) (k 0)))) (h "16sgff81x7xws8sf1hsi3hj3vq6102hc8v32g866hbfjknsz2rfx")))

