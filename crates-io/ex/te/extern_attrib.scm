(define-module (crates-io ex te extern_attrib) #:use-module (crates-io))

(define-public crate-extern_attrib-0.1.0 (c (n "extern_attrib") (v "0.1.0") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (f (quote ("full" "parsing" "printing"))) (d #t) (k 0)))) (h "0iyiq4d87pkbn5b21qbna21c7fkp53ayzajxjxr082x1c2jzdqfv") (y #t)))

(define-public crate-extern_attrib-0.2.0 (c (n "extern_attrib") (v "0.2.0") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (f (quote ("full" "parsing" "printing"))) (d #t) (k 0)))) (h "1b9n48ilrygi9jpljsnah04s8660j1qv8abbhdmjvhhjkcq8anrl") (y #t)))

