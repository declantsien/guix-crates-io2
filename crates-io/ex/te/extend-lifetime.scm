(define-module (crates-io ex te extend-lifetime) #:use-module (crates-io))

(define-public crate-extend-lifetime-0.1.0 (c (n "extend-lifetime") (v "0.1.0") (h "1jx5m5acb8hf0igyvg4xgvqijjmynqhyxr2dsw0h1pjpgpn179py")))

(define-public crate-extend-lifetime-0.2.0 (c (n "extend-lifetime") (v "0.2.0") (h "1fnxbnvg8y60221n5i0br22n9ip25wpvn0cny23wh2ll25mnljdl")))

