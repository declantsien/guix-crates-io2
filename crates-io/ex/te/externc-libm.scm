(define-module (crates-io ex te externc-libm) #:use-module (crates-io))

(define-public crate-externc-libm-0.1.0 (c (n "externc-libm") (v "0.1.0") (d (list (d (n "libm") (r "^0.2.1") (d #t) (k 0)))) (h "08xix6rjnh9qi2n786jl5z03s6rg1xcggpnzxcnfaci6k3j5qbd9") (f (quote (("libm-unstable" "libm/unstable"))))))

