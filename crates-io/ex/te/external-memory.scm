(define-module (crates-io ex te external-memory) #:use-module (crates-io))

(define-public crate-external-memory-0.0.1 (c (n "external-memory") (v "0.0.1") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "drm-fourcc") (r "^2.1.1") (f (quote ("serde"))) (d #t) (k 0)))) (h "0fcxvii3hpy4zlfld913na0qmzrb5n31did8fqkl455hjb9fipz4")))

