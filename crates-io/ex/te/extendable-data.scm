(define-module (crates-io ex te extendable-data) #:use-module (crates-io))

(define-public crate-extendable-data-0.1.0 (c (n "extendable-data") (v "0.1.0") (d (list (d (n "logos") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (d #t) (k 0)))) (h "16lrnl40p278cwh4ngr8jqbgpjrnghhwpjpcqrxykai215yg3hkz")))

(define-public crate-extendable-data-0.1.2 (c (n "extendable-data") (v "0.1.2") (d (list (d (n "extendable-data-helpers") (r "^0.1.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (d #t) (k 0)))) (h "1whlp3sl0si3ryazvf1njaljfw9va0rb9ipkyxf07f6a0drw6nv4")))

(define-public crate-extendable-data-0.1.3 (c (n "extendable-data") (v "0.1.3") (d (list (d (n "assert-tokenstreams-eq") (r "^0.1.0") (d #t) (k 2)) (d (n "extendable-data-helpers") (r "^0.1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (f (quote ("extra-traits"))) (d #t) (k 2)))) (h "1af42k0b9a8n10b74p9mcfkyianz98l6f6kwqk8pf8qjllvdgb4h")))

(define-public crate-extendable-data-0.1.4 (c (n "extendable-data") (v "0.1.4") (d (list (d (n "assert-tokenstreams-eq") (r "^0.1.0") (d #t) (k 2)) (d (n "extendable-data-helpers") (r "^0.1.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (f (quote ("extra-traits"))) (d #t) (k 2)))) (h "035mv031bpx6qnk09k0bxav3zy4anlc3g910yrvkdv7y2x1wf51g")))

(define-public crate-extendable-data-0.1.5 (c (n "extendable-data") (v "0.1.5") (d (list (d (n "assert-tokenstreams-eq") (r "^0.1.0") (d #t) (k 2)) (d (n "extendable-data-helpers") (r "^0.1.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (f (quote ("extra-traits"))) (d #t) (k 2)))) (h "1k50frnfz366s919b6d9kbi1fzpf0kcqv9mx0kihn56hwcvfh456")))

