(define-module (crates-io ex te external-memory-tools) #:use-module (crates-io))

(define-public crate-external-memory-tools-0.1.0 (c (n "external-memory-tools") (v "0.1.0") (h "1wvg2vd7l5gkp8dlzm4xcbbdg0v76jk54sk697b7qfgbykgr2v8r") (f (quote (("std") ("default" "std"))))))

(define-public crate-external-memory-tools-0.1.1 (c (n "external-memory-tools") (v "0.1.1") (h "0m28v5xghwsb4zx63zc7f05wvrpinpjdimyzqh1jwhg5pykjn5yg") (f (quote (("std") ("default" "std"))))))

