(define-module (crates-io ex te extemp) #:use-module (crates-io))

(define-public crate-extemp-0.1.0 (c (n "extemp") (v "0.1.0") (d (list (d (n "bit-set") (r "^0.4.0") (d #t) (k 0)) (d (n "docopt") (r "^0.6.80") (d #t) (k 0)) (d (n "libc") (r "^0.2.11") (d #t) (k 0)) (d (n "multimap") (r "^0.3.0") (d #t) (k 0)) (d (n "regex") (r "^0.1.71") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.19") (d #t) (k 0)) (d (n "rusty-cheddar") (r "^0.3.0") (d #t) (k 1)))) (h "1dnjg99f9cyri61c5dawrcrcr4qgv44dq94m1q8hplar3b38kkc3")))

(define-public crate-extemp-0.1.1 (c (n "extemp") (v "0.1.1") (d (list (d (n "bit-set") (r "^0.4.0") (d #t) (k 0)) (d (n "docopt") (r "^0.6.80") (d #t) (k 0)) (d (n "libc") (r "^0.2.11") (d #t) (k 0)) (d (n "multimap") (r "^0.3.0") (d #t) (k 0)) (d (n "regex") (r "^0.1.71") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.19") (d #t) (k 0)) (d (n "rusty-cheddar") (r "^0.3.0") (d #t) (k 1)))) (h "0yljslnh0wbjrmyfw8hhxn7wy28yh23r4nfv178kiy2n2j5s0467")))

