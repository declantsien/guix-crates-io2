(define-module (crates-io ex te extendable-enums) #:use-module (crates-io))

(define-public crate-extendable-enums-0.1.0 (c (n "extendable-enums") (v "0.1.0") (d (list (d (n "extendable-enums-helpers") (r "^0.1.0") (d #t) (k 0)) (d (n "logos") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (d #t) (k 0)))) (h "087s2hmiip3va0j93a0mlrckfyx9bw9fj48c2dxnllng2akyll1g")))

(define-public crate-extendable-enums-0.1.1 (c (n "extendable-enums") (v "0.1.1") (d (list (d (n "extendable-enums-helpers") (r "^0.1.1") (d #t) (k 0)) (d (n "logos") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (d #t) (k 0)))) (h "0v67w21yxn497w6iz6rxpn9nafws922qp7dr0bhham33b6db6w13")))

(define-public crate-extendable-enums-0.1.2 (c (n "extendable-enums") (v "0.1.2") (d (list (d (n "logos") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (d #t) (k 0)))) (h "1hiq0i428k8y42zqgrba1pg1ibxr8ilvvmmkr6nicf2n8wa6046d")))

(define-public crate-extendable-enums-0.1.3 (c (n "extendable-enums") (v "0.1.3") (d (list (d (n "logos") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (d #t) (k 0)))) (h "052nsvaxcfi79skrsx5gd786nnmf0asl613w8p4bim4qxvvl9jih")))

