(define-module (crates-io ex te externref-cli) #:use-module (crates-io))

(define-public crate-externref-cli-0.2.0 (c (n "externref-cli") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "clap") (r "^4.0.32") (f (quote ("derive" "wrap_help"))) (d #t) (k 0)) (d (n "externref") (r "^0.2.0") (f (quote ("processor"))) (k 0)) (d (n "term-transcript") (r "^0.3.0-beta.2") (f (quote ("portable-pty"))) (d #t) (k 2)) (d (n "test-casing") (r "^0.1.0") (d #t) (k 2)) (d (n "tracing-subscriber") (r "^0.3.16") (f (quote ("env-filter"))) (o #t) (d #t) (k 0)))) (h "1cibszn63gbfzhd87rcz8xczgdalnzw8zsacrammsn3rkszfzpjs") (f (quote (("tracing" "tracing-subscriber" "externref/tracing") ("default" "tracing")))) (r "1.66")))

