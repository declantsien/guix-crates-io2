(define-module (crates-io ex te extended-rational) #:use-module (crates-io))

(define-public crate-extended-rational-1.0.0 (c (n "extended-rational") (v "1.0.0") (h "0j7smi9avqa4ipfr02y10ni50hf4g6qjrmnvv43zwlh9z1wahycm")))

(define-public crate-extended-rational-1.0.1 (c (n "extended-rational") (v "1.0.1") (h "065mibyz81my5xyviv42i1cmm8kqr72jldv3sphfb49fkrpsaab1")))

(define-public crate-extended-rational-1.0.2 (c (n "extended-rational") (v "1.0.2") (h "0149ly7zdw2nsrmvxcr1s96zmrncxjfx9kpnbsgc6h1mwbcknqgf")))

(define-public crate-extended-rational-1.0.3 (c (n "extended-rational") (v "1.0.3") (h "1q52nwyc42344mvj2h3qr4h0liccbn9x1l0mdnr8zi1p1pqz0nqi")))

(define-public crate-extended-rational-1.0.4 (c (n "extended-rational") (v "1.0.4") (h "0ghk0y7z7341rx41ppm6vab4mbm5jps7n5jcyl4407bnagyy7ja0")))

(define-public crate-extended-rational-1.1.0 (c (n "extended-rational") (v "1.1.0") (h "1r7fqf1xww6aq1ifbfrzl2812ayx47z6bs4bx650gq752jcv4zfk")))

(define-public crate-extended-rational-1.1.1 (c (n "extended-rational") (v "1.1.1") (h "0ywksp318ylq30xw6wkc7055kdyg9za8sic2zqsd30vc2db32m5s")))

(define-public crate-extended-rational-1.1.2 (c (n "extended-rational") (v "1.1.2") (h "1yaadfvbjfmj2qca81l4y26p0hmj0pqrz523i1a371j92l1hvjij")))

(define-public crate-extended-rational-1.2.0 (c (n "extended-rational") (v "1.2.0") (h "024vn1xflzy43d037g97yj040cpq7f6vqrr6nv38frqvw7dbmda2")))

(define-public crate-extended-rational-1.2.1 (c (n "extended-rational") (v "1.2.1") (h "1f6kigbc0r0k786746m4n6y2kbnzfj7rln8a8i04cn7zqdhdcchk")))

(define-public crate-extended-rational-1.3.0 (c (n "extended-rational") (v "1.3.0") (h "1hvcqmr2r769ynbdnqvzdahw7bi9rfycip4rcfdcilbfgp7v3ww3")))

(define-public crate-extended-rational-1.3.1 (c (n "extended-rational") (v "1.3.1") (h "1ywcz10mfflnglidp2rsiw5fnid8gc9q9n504imjxj0ns7p5gy4w")))

(define-public crate-extended-rational-1.3.2 (c (n "extended-rational") (v "1.3.2") (h "0djxxzbnsyrxpsf9jxnic8kmb04l1sh5cj7kwz3pbn8czr3b0ygc")))

(define-public crate-extended-rational-1.3.3 (c (n "extended-rational") (v "1.3.3") (d (list (d (n "bit_manager") (r "^0.5.0") (o #t) (d #t) (k 0)) (d (n "bit_manager_derive") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "12zanm1fc7y6azg3y9lsbn6ifl69vz9jn2q3va8hxrc9f6bqlb53") (f (quote (("default") ("bit_manager_enabled" "bit_manager" "bit_manager_derive"))))))

