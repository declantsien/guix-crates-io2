(define-module (crates-io ex te external_sort) #:use-module (crates-io))

(define-public crate-external_sort-0.0.1 (c (n "external_sort") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.5") (d #t) (k 0)))) (h "0ysrk20ylii8hkrvwhzn7k5kc5yfh7yjgzmr2s13w9j4j4jhx0cn")))

(define-public crate-external_sort-0.0.2 (c (n "external_sort") (v "0.0.2") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.5") (d #t) (k 0)))) (h "03ym7y97fyvaxxskqj6iq5yb0qyc90q4lq77px3ly7s5pphxqa4i")))

(define-public crate-external_sort-0.0.3 (c (n "external_sort") (v "0.0.3") (d (list (d (n "rand") (r "^0.5.1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.5") (d #t) (k 0)))) (h "1iiifzlc8d4h1k1gkq3pkjhlgj0lqqgk0msjclzprkwqghcf0shx")))

(define-public crate-external_sort-0.1.0 (c (n "external_sort") (v "0.1.0") (d (list (d (n "rand") (r "^0.5.1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.5") (d #t) (k 0)))) (h "178n9jprfhhk111crmcmdkp9vik9hgn7b7662a2mswag19j35ldj")))

(define-public crate-external_sort-0.1.1 (c (n "external_sort") (v "0.1.1") (d (list (d (n "rand") (r "^0.5.1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.5") (d #t) (k 0)))) (h "01xxlpyw0n1ai7y33f2qgr14w594vdva05547nlky4k9601xq8yp")))

(define-public crate-external_sort-0.1.2 (c (n "external_sort") (v "0.1.2") (d (list (d (n "rand") (r "^0.5.1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.5") (d #t) (k 0)))) (h "0xfcgk0l7hvskr4y90p9y81xnqp4q4mqy7qb5l14dsdz5va0r30k")))

