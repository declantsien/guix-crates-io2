(define-module (crates-io ex te external_set) #:use-module (crates-io))

(define-public crate-external_set-0.1.0 (c (n "external_set") (v "0.1.0") (h "1j79sdnczsj9bvp42gmvqbb5svnwz64pzc744252bilxx5kapq5i")))

(define-public crate-external_set-0.1.1 (c (n "external_set") (v "0.1.1") (h "07kvdidjbja50if7037dcij5rhcdqay7b0r0jdmm2300n4i44nvn")))

