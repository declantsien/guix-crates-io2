(define-module (crates-io ex te extern_executor) #:use-module (crates-io))

(define-public crate-extern_executor-0.1.0 (c (n "extern_executor") (v "0.1.0") (d (list (d (n "cbindgen") (r "^0.14") (o #t) (d #t) (k 1)) (d (n "futures-task") (r "^0.3") (f (quote ("alloc"))) (o #t) (k 0)) (d (n "spin") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "woke") (r "^0.0") (o #t) (d #t) (k 0)))) (h "1yw9zyjjlgrs04y81svq8ff8pw38b9g1p69iyin88bsand9xw06d") (f (quote (("std" "futures-task") ("no_std" "spin" "woke") ("default" "std"))))))

(define-public crate-extern_executor-0.1.2 (c (n "extern_executor") (v "0.1.2") (d (list (d (n "cbindgen") (r "^0.14") (o #t) (d #t) (k 1)) (d (n "dart-sys") (r "^2.0") (o #t) (d #t) (k 0)) (d (n "futures-task") (r "^0.3") (f (quote ("alloc"))) (o #t) (k 0)) (d (n "pkg-config") (r "^0.3") (o #t) (d #t) (k 1)) (d (n "spin") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "woke") (r "^0.0") (o #t) (d #t) (k 0)))) (h "0gwnmxkbxsgkgp2p1gkffyl2774276v4i7anmc7lc4zsqzh4mbhh") (f (quote (("uv" "driver" "pkg-config") ("std" "futures-task") ("no_std" "spin" "woke") ("driver") ("default" "std") ("dart" "dart-sys" "driver"))))))

