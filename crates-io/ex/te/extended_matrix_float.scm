(define-module (crates-io ex te extended_matrix_float) #:use-module (crates-io))

(define-public crate-extended_matrix_float-0.1.0 (c (n "extended_matrix_float") (v "0.1.0") (h "1n421h7lyavnf3fyzky0j2wg2zymdbhqbdkqbsvlp950n414654l") (y #t)))

(define-public crate-extended_matrix_float-0.1.1 (c (n "extended_matrix_float") (v "0.1.1") (h "0vhncg1akh05zcz9i9zm7vz7kv3qdv487dp03fr533birx18lcl0") (y #t)))

(define-public crate-extended_matrix_float-1.0.0 (c (n "extended_matrix_float") (v "1.0.0") (h "0k7l7msj72ck3gw1hw0kkh54myqxqnnxwdwgdpsqsq2rhlm3kr4p")))

