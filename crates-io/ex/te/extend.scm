(define-module (crates-io ex te extend) #:use-module (crates-io))

(define-public crate-extend-0.0.1 (c (n "extend") (v "0.0.1") (d (list (d (n "proc-macro-error") (r "^0.3.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.7") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.17") (d #t) (k 0)))) (h "08ph55q0xzzka30y9a04s666v8l7qw95mgsrzllhlgz4zzypnlyp")))

(define-public crate-extend-0.0.2 (c (n "extend") (v "0.0.2") (d (list (d (n "proc-macro-error") (r "^0.3.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.7") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.17") (d #t) (k 2)))) (h "0k2qwkg90y4hn9xn38qk1g04gs1zgzkm57rmf72jpyyfph0ig417")))

(define-public crate-extend-0.1.0 (c (n "extend") (v "0.1.0") (d (list (d (n "proc-macro-error") (r "^0.3.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.7") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.17") (d #t) (k 2)))) (h "1a3h7dcvmd3dbyvfrjq9z5yh1psly85mwsdas581728acipqcadz")))

(define-public crate-extend-0.1.1 (c (n "extend") (v "0.1.1") (d (list (d (n "proc-macro-error") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.7") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.17") (d #t) (k 2)))) (h "1z18gxmlrcjx2qhxqf20ymq0py4qwzkiahd161i6q3jbcs9v77gy")))

(define-public crate-extend-0.1.2 (c (n "extend") (v "0.1.2") (d (list (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.17") (d #t) (k 2)))) (h "01azyniinxrwng13hkj450gplp1ajslbqzksjg4dk6655sks6zgl")))

(define-public crate-extend-0.2.0 (c (n "extend") (v "0.2.0") (d (list (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits" "visit"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.17") (d #t) (k 2)))) (h "10s3f1gw1skzai0khwc20qg35y0qqzwf6jz602rsxyxw1i9jpriw")))

(define-public crate-extend-0.2.1 (c (n "extend") (v "0.2.1") (d (list (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits" "visit"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.17") (d #t) (k 2)))) (h "0q0c0509y4h2q9pc7mz4cknsccxqfj4j9v3q9ry00lxxzwi034l2")))

(define-public crate-extend-0.3.0 (c (n "extend") (v "0.3.0") (d (list (d (n "async-trait") (r "^0.1.40") (d #t) (k 2)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits" "visit"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.17") (d #t) (k 2)))) (h "1rfmz990ggd7k0vjajm9yldzlr684x6b15an264vz72sd7f5yqqc")))

(define-public crate-extend-1.0.0 (c (n "extend") (v "1.0.0") (d (list (d (n "async-trait") (r "^0.1.40") (d #t) (k 2)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits" "visit"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.17") (d #t) (k 2)))) (h "0gnps24pzyvb4gqlvvwspgnsdphn4ybabhj69hp7v49c38cd37fr")))

(define-public crate-extend-1.0.1 (c (n "extend") (v "1.0.1") (d (list (d (n "async-trait") (r "^0.1.40") (d #t) (k 2)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits" "visit"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.17") (d #t) (k 2)))) (h "04bxi0i4w5z1b1cwizr0dn7annh7a32psw48hzqb439g6f7amzmj")))

(define-public crate-extend-1.1.0 (c (n "extend") (v "1.1.0") (d (list (d (n "async-trait") (r "^0.1.40") (d #t) (k 2)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits" "visit"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.17") (d #t) (k 2)))) (h "00h4f3sd6rhmgyrwhl44al6023ndyw0nqzammq66smv7x81n5ha7")))

(define-public crate-extend-1.1.1 (c (n "extend") (v "1.1.1") (d (list (d (n "async-trait") (r "^0.1.40") (d #t) (k 2)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits" "visit"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.17") (d #t) (k 2)))) (h "1ccbq0wxzk9c4nycm2gyp24qxp5nz6kxd907q0ypbv546clrxj7m")))

(define-public crate-extend-1.1.2 (c (n "extend") (v "1.1.2") (d (list (d (n "async-trait") (r "^0.1.40") (d #t) (k 2)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits" "visit"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.17") (d #t) (k 2)))) (h "041n3d5h53i23zi6wi85byrf1am4r0g8fv8zy6mfnvm7hziicljw")))

(define-public crate-extend-1.1.3 (c (n "extend") (v "1.1.3") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits" "visit"))) (d #t) (k 0)) (d (n "async-trait") (r "^0.1.40") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.17") (d #t) (k 2)))) (h "0nqv20asi409crcq707skcszpj349i3i8c2y08a1sl6cpzjq8hck")))

(define-public crate-extend-1.2.0 (c (n "extend") (v "1.2.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits" "visit"))) (d #t) (k 0)) (d (n "async-trait") (r "^0.1.40") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.17") (d #t) (k 2)))) (h "003dnm6vdfx5ja0j6p7ifabgf9zdjyps0y1c7pvvyq4x3wpns6ii")))

