(define-module (crates-io ex te extended_pulldown) #:use-module (crates-io))

(define-public crate-extended_pulldown-0.1.0 (c (n "extended_pulldown") (v "0.1.0") (d (list (d (n "aho-corasick") (r "^0.7") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1wrcjpac5qx8p6z5xsqa3vpiiwz6vz5yx51ddsz42jw81496xd97")))

