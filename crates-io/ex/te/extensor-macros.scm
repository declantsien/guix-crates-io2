(define-module (crates-io ex te extensor-macros) #:use-module (crates-io))

(define-public crate-extensor-macros-0.1.0 (c (n "extensor-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.81") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.60") (f (quote ("full"))) (d #t) (k 0)))) (h "11d0bxs12jx7gi57if5fc4qwj62wblb7274wlxq5802bwx1f8bym")))

