(define-module (crates-io ex te extendable-data-helpers) #:use-module (crates-io))

(define-public crate-extendable-data-helpers-0.1.1 (c (n "extendable-data-helpers") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (d #t) (k 0)))) (h "1miq91zqv351sfl38dn91p4xj9znpl7jh0h0qdkggvxnx9ylc921")))

(define-public crate-extendable-data-helpers-0.1.2 (c (n "extendable-data-helpers") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (d #t) (k 0)))) (h "1ld51d331c3rmaqvdbsr5qp56m1blw8siq228bvsr90jdiznj6bg")))

(define-public crate-extendable-data-helpers-0.1.3 (c (n "extendable-data-helpers") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (d #t) (k 0)))) (h "1c014nznlsvqsbl50zbc2w6lwxwhmf31cvwagva5z27w0a4qg1s9")))

(define-public crate-extendable-data-helpers-0.1.4 (c (n "extendable-data-helpers") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (d #t) (k 0)))) (h "0marzdwvf2wigvlajx7ca3rvisgz2x4b4r9cfgz6lhj6k1xx8087")))

(define-public crate-extendable-data-helpers-0.1.5 (c (n "extendable-data-helpers") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (d #t) (k 0)))) (h "0npd4mmjkzi2xfkfnqjj3g3q4ahcnv2mdlvyfvc5g70359bjz5gv")))

