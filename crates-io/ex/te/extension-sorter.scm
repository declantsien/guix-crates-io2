(define-module (crates-io ex te extension-sorter) #:use-module (crates-io))

(define-public crate-extension-sorter-0.1.0 (c (n "extension-sorter") (v "0.1.0") (h "0kv79pvcj0pwnnim1x1bxxlkhq4q6ndgl1q6d3wwj58ldwpk8vxs")))

(define-public crate-extension-sorter-0.1.1 (c (n "extension-sorter") (v "0.1.1") (h "1qrdn8l64dkk01iamd1xfhb9jf3w8jzfnkxcq8j6cbr37ir65fkg")))

