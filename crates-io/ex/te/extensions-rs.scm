(define-module (crates-io ex te extensions-rs) #:use-module (crates-io))

(define-public crate-extensions-rs-0.1.0 (c (n "extensions-rs") (v "0.1.0") (d (list (d (n "enum-iterator") (r "^2.0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (d #t) (k 0)))) (h "00wq2cwc5q73ysnrk572xvd2xd4slvlsv926n7xarcd21vvzqlal")))

(define-public crate-extensions-rs-0.2.0 (c (n "extensions-rs") (v "0.2.0") (d (list (d (n "enum-iterator") (r "^2.0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("macros" "rt"))) (d #t) (k 0)))) (h "1hg7m2fvqx82byhdzla3lkh8sdlyflqhs1frqwynr9zr9f2cg4mf")))

(define-public crate-extensions-rs-0.2.1 (c (n "extensions-rs") (v "0.2.1") (d (list (d (n "enum-iterator") (r "^2.0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("macros" "rt"))) (d #t) (k 0)) (d (n "tokio-test") (r "^0.4.3") (d #t) (k 2)))) (h "0w15ixrvvwchd2n7h69wbpqyjvm4jsylf8qhgsaslvi71s3zy2mb")))

