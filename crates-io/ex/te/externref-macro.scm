(define-module (crates-io ex te externref-macro) #:use-module (crates-io))

(define-public crate-externref-macro-0.1.0 (c (n "externref-macro") (v "0.1.0") (d (list (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "version-sync") (r "^0.9.4") (d #t) (k 2)))) (h "1gdcpd5rknnsczznwif5cypv1g5mfszlmxynpffgnypy9k3c08l0") (r "1.60")))

(define-public crate-externref-macro-0.2.0 (c (n "externref-macro") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits"))) (d #t) (k 2)) (d (n "trybuild") (r "^1.0.71") (d #t) (k 2)) (d (n "version-sync") (r "^0.9.4") (d #t) (k 2)))) (h "06zz3qyq2376znqyslidlf5azmfyc7hb34lfa7ga8m3ls0jp643p") (r "1.66")))

