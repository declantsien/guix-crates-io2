(define-module (crates-io ex te extends-rs) #:use-module (crates-io))

(define-public crate-extends-rs-0.1.0 (c (n "extends-rs") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.14") (d #t) (k 0)) (d (n "substring") (r "^1.4.5") (d #t) (k 0)) (d (n "syn") (r "^1.0.84") (f (quote ("full" "fold" "printing"))) (d #t) (k 0)))) (h "1wja3zw5wn9dmx0ph3a9bqkjasdk4jd8kfdvg0vvrg183377mkyh") (y #t)))

(define-public crate-extends-rs-0.1.1 (c (n "extends-rs") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.14") (d #t) (k 0)) (d (n "substring") (r "^1.4.5") (d #t) (k 0)) (d (n "syn") (r "^1.0.84") (f (quote ("full" "fold" "printing"))) (d #t) (k 0)))) (h "0r793vn2fwwf5yjv4qg6lblrmqzg8s9fw64kgmjplgmjrkqg39np") (y #t)))

(define-public crate-extends-rs-0.1.2 (c (n "extends-rs") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.14") (d #t) (k 0)) (d (n "substring") (r "^1.4.5") (d #t) (k 0)) (d (n "syn") (r "^1.0.84") (f (quote ("full" "fold" "printing"))) (d #t) (k 0)))) (h "037m2bdibnkb9ykjsgwgbd7zh6rfqs0cpf68jwqfg1fah391azv8") (y #t)))

(define-public crate-extends-rs-0.1.3 (c (n "extends-rs") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.14") (d #t) (k 0)) (d (n "substring") (r "^1.4.5") (d #t) (k 0)) (d (n "syn") (r "^1.0.84") (f (quote ("full" "fold" "printing"))) (d #t) (k 0)))) (h "12k8vrz3qqb6kwc7qnh7npgj6kf3qjfndwn433f9bp21kcm5mmrk") (y #t)))

(define-public crate-extends-rs-0.1.4 (c (n "extends-rs") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.14") (d #t) (k 0)) (d (n "substring") (r "^1.4.5") (d #t) (k 0)) (d (n "syn") (r "^1.0.84") (f (quote ("full" "fold" "printing"))) (d #t) (k 0)))) (h "0jn94lc7smxwkn6z229wgz8ig2j45lxdd0qs5fxj7z3p0b2z8961")))

(define-public crate-extends-rs-0.1.5 (c (n "extends-rs") (v "0.1.5") (d (list (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.14") (d #t) (k 0)) (d (n "substring") (r "^1.4.5") (d #t) (k 0)) (d (n "syn") (r "^1.0.84") (f (quote ("full" "fold" "printing"))) (d #t) (k 0)))) (h "0yl1m9dznwi51j6vfpbagvyspf35wbwxlmyyrbpsij6249mxrbai")))

(define-public crate-extends-rs-0.1.6 (c (n "extends-rs") (v "0.1.6") (d (list (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.14") (d #t) (k 0)) (d (n "substring") (r "^1.4.5") (d #t) (k 0)) (d (n "syn") (r "^1.0.84") (f (quote ("full" "fold" "printing"))) (d #t) (k 0)))) (h "1pi8l6l9661qb9yhfxagcpc45vbxpnwpiw56d5zjmy31g09absdg")))

