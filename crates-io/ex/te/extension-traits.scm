(define-module (crates-io ex te extension-traits) #:use-module (crates-io))

(define-public crate-extension-traits-1.0.0 (c (n "extension-traits") (v "1.0.0") (d (list (d (n "ext-trait") (r "^1.0.0") (d #t) (k 0)))) (h "02k1i4b3ws88mlmkc27avg1pyzd3kv0lqq8mcaqadg6nfkrrwn9f") (f (quote (("better-docs"))))))

(define-public crate-extension-traits-1.0.1 (c (n "extension-traits") (v "1.0.1") (d (list (d (n "ext-trait") (r "^1.0.1") (d #t) (k 0)))) (h "0dwm124vfsbsim6zav738d39d6mn3jm8771jm2gxy7k2jnlfb5m2") (f (quote (("better-docs"))))))

