(define-module (crates-io ex te extendhash) #:use-module (crates-io))

(define-public crate-extendhash-0.1.0 (c (n "extendhash") (v "0.1.0") (h "0wvfqmxxm8avscgxchwk39cgr30nqlhhld0vkw7fpfs7pcz2qgjj")))

(define-public crate-extendhash-0.1.1 (c (n "extendhash") (v "0.1.1") (h "1ifj2fa0wg308k1msd2lj23x38izsszb6dsdkr4ka21svqsb5326")))

(define-public crate-extendhash-0.2.0 (c (n "extendhash") (v "0.2.0") (h "1avlbkr3m5lmz9apydnw1sc8df0s160m5i75zw0xmcfjwxi9yrip")))

(define-public crate-extendhash-0.2.1 (c (n "extendhash") (v "0.2.1") (h "1rs06ws5jaj7hpkar901vxpg5114dakwbdsaq2z8armyqrkl5dz4")))

(define-public crate-extendhash-0.2.2 (c (n "extendhash") (v "0.2.2") (h "1rkm4hxf1n3ywpmns3i5xnq7jmc4sp0zw05h67y4phq5yn8yas3c")))

(define-public crate-extendhash-0.2.3 (c (n "extendhash") (v "0.2.3") (h "09280iimjl4ms4v9abn4lsffkw1saf4xx8adw0kfcamjlz5j738q")))

(define-public crate-extendhash-0.2.4 (c (n "extendhash") (v "0.2.4") (h "046dmy11ca89b6112m6i6j40fdp36a5hc9zlm3h6vrdkwhddzjcn")))

(define-public crate-extendhash-0.2.5 (c (n "extendhash") (v "0.2.5") (h "0rh5pq706vd969kypdqnn1w9in0rllc6h2s0hd4wvbc4r8vjgv4v")))

(define-public crate-extendhash-0.2.6 (c (n "extendhash") (v "0.2.6") (h "0khxh5al2xww1p55n1x8a50gw334rfxkb3wdix2k04ibzx89xaxb")))

(define-public crate-extendhash-1.0.0 (c (n "extendhash") (v "1.0.0") (h "1scxnn7i363q3kmhxrsgrc584z53wv4pfm9m1rh59gw8hmxz7mj8")))

(define-public crate-extendhash-1.0.1 (c (n "extendhash") (v "1.0.1") (h "1ak0d8g8zayaznhv2xf37cchvp9wnak86gajfrcwmcmysbyrqs3y")))

(define-public crate-extendhash-1.0.2 (c (n "extendhash") (v "1.0.2") (h "0lilfsisz8ivh3z50j0qlh5y1r5q67hcxizr7i70fwb6cic8p5g8")))

(define-public crate-extendhash-1.0.3 (c (n "extendhash") (v "1.0.3") (h "1bms39ga2byx35y9ikns82kf5gb9jfadnk4rv26jzx72y8m5p6wy")))

(define-public crate-extendhash-1.0.4 (c (n "extendhash") (v "1.0.4") (h "0q89q447zbr6r73mn6x5gjxh990hzbhf35q4i9p43wldaih10mc3")))

(define-public crate-extendhash-1.0.5 (c (n "extendhash") (v "1.0.5") (h "1bvqzklllbrbvknm5hl0avi6cc1pxvq82j9bsa1yvkd6zaf649as")))

(define-public crate-extendhash-1.0.6 (c (n "extendhash") (v "1.0.6") (h "1kbx55yxlklpm0srl0pq75i9chrl4jdj7qznfxkg8yb3nmmmwsrk")))

(define-public crate-extendhash-1.0.7 (c (n "extendhash") (v "1.0.7") (h "0j3p7np89pl56qln41j4yn95mkmpm6fkkfc5z7mjfj9ww9958csr")))

(define-public crate-extendhash-1.0.8 (c (n "extendhash") (v "1.0.8") (h "1qd51fivdsmawwvvshbi7wgih02npm46v3hvx7x4jqd2k18zl880")))

(define-public crate-extendhash-1.0.9 (c (n "extendhash") (v "1.0.9") (h "0va29wgzcyjhk4nshzybm8c6yx4qjw4qlnyi2c01mqaslzymhhgh")))

(define-public crate-extendhash-1.0.10 (c (n "extendhash") (v "1.0.10") (h "1rjhyh1gjvfm0636c0mc51g1pxbw76jx1h1q99j4m6806bbw84lq")))

