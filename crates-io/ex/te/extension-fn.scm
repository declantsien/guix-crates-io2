(define-module (crates-io ex te extension-fn) #:use-module (crates-io))

(define-public crate-extension-fn-1.0.0 (c (n "extension-fn") (v "1.0.0") (d (list (d (n "async-trait") (r "^0.1.68") (d #t) (k 2)) (d (n "case") (r "^1.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.13") (f (quote ("full"))) (d #t) (k 0)))) (h "0j1zsn5ldbdpz4ckrfm9vhbgnjdpf922vxg1fi7z46xr7kb62kb2")))

(define-public crate-extension-fn-1.1.0 (c (n "extension-fn") (v "1.1.0") (d (list (d (n "async-trait") (r "^0.1.68") (d #t) (k 2)) (d (n "case") (r "^1.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "public") (r "^0.2.2") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.13") (f (quote ("full"))) (d #t) (k 0)))) (h "1036v7w0h5bg6qxdph18dmb67vjvpscbsyxsgap9fqgbmcdz0x61")))

(define-public crate-extension-fn-1.1.1 (c (n "extension-fn") (v "1.1.1") (d (list (d (n "async-trait") (r "^0.1.68") (d #t) (k 2)) (d (n "case") (r "^1.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "public") (r "^0.2.2") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.13") (f (quote ("full"))) (d #t) (k 0)))) (h "11yy4pj3n4ns6xmlawgk96i78dli96h9za2sbnd6k6fmsnpa5p82")))

(define-public crate-extension-fn-1.2.0 (c (n "extension-fn") (v "1.2.0") (d (list (d (n "async-trait") (r "^0.1.68") (d #t) (k 2)) (d (n "case") (r "^1.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "public") (r "^0.2.2") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.13") (f (quote ("full"))) (d #t) (k 0)))) (h "184lw9dv4smxpfld1bzzmm13vmi038pr4v1gsb7sdnzli2bvrark")))

