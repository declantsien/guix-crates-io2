(define-module (crates-io ex te extendr-engine) #:use-module (crates-io))

(define-public crate-extendr-engine-0.2.0 (c (n "extendr-engine") (v "0.2.0") (d (list (d (n "libR-sys") (r "^0.2.0") (d #t) (k 0)))) (h "0fkibssdv6h5hhsmkm9212an7byxd1w5ip676qfdli4xclmksqjb") (f (quote (("tests-all" "libR-sys/use-bindgen") ("default"))))))

(define-public crate-extendr-engine-0.3.0 (c (n "extendr-engine") (v "0.3.0") (d (list (d (n "libR-sys") (r "^0.2.2") (d #t) (k 0)))) (h "00x9clw2nmbg19lrm75hfnjjzq8nq9fhlnmgq1lqk0pdyq67lbyh") (f (quote (("tests-all" "libR-sys/use-bindgen") ("default"))))))

(define-public crate-extendr-engine-0.3.1 (c (n "extendr-engine") (v "0.3.1") (d (list (d (n "libR-sys") (r "^0.3.0") (d #t) (k 0)))) (h "1x4y6il5r6zpnd2mvagqa962nhfc4yhwynaw49qcz8spr8q8a7sv") (f (quote (("tests-all" "libR-sys/use-bindgen") ("default"))))))

(define-public crate-extendr-engine-0.4.0 (c (n "extendr-engine") (v "0.4.0") (d (list (d (n "libR-sys") (r "^0.4.0") (d #t) (k 0)))) (h "0p8mz27z62n5jdiwcvq9kznrafqxmxd7xg9bp4h1ifcbwfidb642") (f (quote (("tests-all" "libR-sys/use-bindgen") ("default"))))))

(define-public crate-extendr-engine-0.6.0 (c (n "extendr-engine") (v "0.6.0") (d (list (d (n "ctor") (r "^0.2.4") (d #t) (k 0)) (d (n "libR-sys") (r "^0.6.0") (d #t) (k 0)))) (h "0saxwh6nawmcgmi9lis26v65j0ssyg2f842mwiq6miyx4py41116") (f (quote (("tests-all" "libR-sys/use-bindgen") ("default"))))))

