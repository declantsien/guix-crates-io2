(define-module (crates-io ex te extendable-enums-helpers) #:use-module (crates-io))

(define-public crate-extendable-enums-helpers-0.1.0 (c (n "extendable-enums-helpers") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (d #t) (k 0)))) (h "0b58hr9qwsngz092g9h47lf1bb3bscki350h7mxasjajk1r8wfqf")))

(define-public crate-extendable-enums-helpers-0.1.1 (c (n "extendable-enums-helpers") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (d #t) (k 0)))) (h "10hx9m61s90qnp2cnayp03bvkmrylqi134wsj9x6n26rlnhfdp48")))

(define-public crate-extendable-enums-helpers-0.1.3 (c (n "extendable-enums-helpers") (v "0.1.3") (d (list (d (n "extendable-data-helpers") (r "^0.1.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)))) (h "1k3ylwaj7by7ya1vs9d44b1m1drgrlzlrzg4adcih2j545ymvlcx")))

