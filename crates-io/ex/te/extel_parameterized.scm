(define-module (crates-io ex te extel_parameterized) #:use-module (crates-io))

(define-public crate-extel_parameterized-0.1.0 (c (n "extel_parameterized") (v "0.1.0") (h "1vnx7ck7z11c2qzi6iby32rxp3ig160qmivi176z9pjs3dhljnsy")))

(define-public crate-extel_parameterized-0.1.1 (c (n "extel_parameterized") (v "0.1.1") (h "0vkja9lf4ranfa8qlm5pkprdgbccs2f69llvqssgnsbczdp81xb3")))

