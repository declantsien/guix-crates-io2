(define-module (crates-io ex te externalities) #:use-module (crates-io))

(define-public crate-externalities-0.0.0 (c (n "externalities") (v "0.0.0") (h "1pba695ccnp3524acrkrrqglg18g7l14w5wiin5plfy1zgi6n2yz") (y #t)))

(define-public crate-externalities-0.8.1 (c (n "externalities") (v "0.8.1") (d (list (d (n "codec") (r "^2.0.1") (k 0) (p "tetsy-scale-codec")) (d (n "environ") (r "^1.1.2") (k 0)) (d (n "tetcore-std") (r "^2.0.0") (k 0)) (d (n "tetcore-storage") (r "^2.0.0") (k 0)))) (h "1nc6b7gklyyy3fh50kcnr980b4ddchl93pc7isyn98zhjbshs1dj") (f (quote (("std" "codec/std" "environ/std" "tetcore-std/std" "tetcore-storage/std") ("default" "std"))))))

(define-public crate-externalities-0.8.2 (c (n "externalities") (v "0.8.2") (d (list (d (n "codec") (r "^2.0.1") (k 0) (p "tetsy-scale-codec")) (d (n "environ") (r "^1.1.2") (k 0)) (d (n "tetcore-std") (r "^2.0.2") (k 0)) (d (n "tetcore-storage") (r "^2.0.2") (k 0)))) (h "1xvqjqdwg65k83vq5cnjrw8f0l3jplf4qbys7sghzngjhnlxc4m8") (f (quote (("std" "codec/std" "environ/std" "tetcore-std/std" "tetcore-storage/std") ("default" "std"))))))

(define-public crate-externalities-2.1.2 (c (n "externalities") (v "2.1.2") (d (list (d (n "codec") (r "^2.0.1") (k 0) (p "tetsy-scale-codec")) (d (n "environ") (r "^1.1.3") (k 0)) (d (n "tetcore-std") (r "^2.1.2") (k 0)) (d (n "tetcore-storage") (r "^2.1.2") (k 0)))) (h "1dqwins43f310hc13xxga5zlkz46jdga9ifm4sklk6y81fdwjzlq") (f (quote (("std" "codec/std" "environ/std" "tetcore-std/std" "tetcore-storage/std") ("default" "std"))))))

