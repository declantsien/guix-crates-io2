(define-module (crates-io ex te external_mixin_umbrella) #:use-module (crates-io))

(define-public crate-external_mixin_umbrella-0.0.1 (c (n "external_mixin_umbrella") (v "0.0.1") (h "1wdjd0pz357f14rk4pzcaa5mr89lw1g1s7fg0fv8qjlrv144bslm") (f (quote (("default") ("compile_error"))))))

(define-public crate-external_mixin_umbrella-0.0.2 (c (n "external_mixin_umbrella") (v "0.0.2") (d (list (d (n "external_mixin") (r "^0") (d #t) (k 2)) (d (n "rust_mixin") (r "^0") (d #t) (k 2)))) (h "0pn9z1aidn1zzkapczrk5vn8jcljbq5c4amckid3m9rr3w8ifbgf") (f (quote (("default") ("compile_error"))))))

