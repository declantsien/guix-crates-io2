(define-module (crates-io ex te extended_matrix) #:use-module (crates-io))

(define-public crate-extended_matrix-0.2.0 (c (n "extended_matrix") (v "0.2.0") (h "0mb9k2vp156jgw4zz27jr35fcxng8na4iaaw6238xp7xkqcz6qmr") (y #t)))

(define-public crate-extended_matrix-0.2.1 (c (n "extended_matrix") (v "0.2.1") (h "1aiqzm89l3xdkib2svaxn66qhshg1szpr7yxdpigil1zbidc79hm") (y #t)))

(define-public crate-extended_matrix-0.2.2 (c (n "extended_matrix") (v "0.2.2") (h "1vhc0q16rs2dds4kcvcrcs9j92xzm2pqc9dq9h1wrrcig0irfmg1") (y #t)))

(define-public crate-extended_matrix-0.2.3 (c (n "extended_matrix") (v "0.2.3") (h "06xjh2wja487x6f4sf39kddv04j9h8v6509bxv64jnxgm4w02v0c") (y #t)))

(define-public crate-extended_matrix-0.2.4 (c (n "extended_matrix") (v "0.2.4") (h "0b55ynfjq5sj3z73qdsj0vfb6af1m61wzlpnmbafs0x5m8dhxhi2") (y #t)))

(define-public crate-extended_matrix-0.2.5 (c (n "extended_matrix") (v "0.2.5") (h "11lpm6h6i8cn8zh38a5k8y8sj44xdh3cgd5xkgw1i9kw8hrwsq4k") (y #t)))

(define-public crate-extended_matrix-0.2.6 (c (n "extended_matrix") (v "0.2.6") (h "077i9ff8kmhc7fiksc8sh0i2lpwv7rjiivfs1nk056v9dv96rfgs") (y #t)))

(define-public crate-extended_matrix-0.2.7 (c (n "extended_matrix") (v "0.2.7") (h "1dcgp2c2lz1c1kk3382l0hj2085nc2gs5x3r5wqr7zvmd9cy7qva") (y #t)))

(define-public crate-extended_matrix-0.2.8 (c (n "extended_matrix") (v "0.2.8") (h "0rb60jv01a4pf5xw5q19nnqrfjjv6djddf4wjhn2szssjgscyiks") (y #t)))

(define-public crate-extended_matrix-0.2.9 (c (n "extended_matrix") (v "0.2.9") (h "0cqr8i4agxgzcygbkc20xdbnd5cm23yh598zcsm33f78hqbvfgf7") (y #t)))

(define-public crate-extended_matrix-0.2.10 (c (n "extended_matrix") (v "0.2.10") (h "0mdq1drk9n5n9h9a0vf9a1pmrmwnbwajdxj1wkihiyl7ngrlarxy") (y #t)))

(define-public crate-extended_matrix-0.2.11 (c (n "extended_matrix") (v "0.2.11") (h "0blkhab37h8yvblds41wg3pbfcq2dpnz09722cvwgi2d159ndcn1") (y #t)))

(define-public crate-extended_matrix-0.2.12 (c (n "extended_matrix") (v "0.2.12") (h "15y07x1jzy4z1qmxymw5w63c7qx3lb1m7w2g0z7dxd2zm17ww4y2")))

(define-public crate-extended_matrix-0.2.13 (c (n "extended_matrix") (v "0.2.13") (h "1993simkpyn19l9sd8vpkcf06dx3zf6mg6hqy4n3g6d16jf137p2") (y #t)))

(define-public crate-extended_matrix-0.3.0 (c (n "extended_matrix") (v "0.3.0") (h "19078111l6j0sjdjv8n1rx07411ji0qz4cvxprrgm4b9l0x6g3gf") (y #t)))

(define-public crate-extended_matrix-0.3.1 (c (n "extended_matrix") (v "0.3.1") (h "0dwpn7jjcv5jh7lvgkl20v6554djqvdjdbj8z24ing2pxdmkl4f6") (y #t)))

(define-public crate-extended_matrix-0.3.2 (c (n "extended_matrix") (v "0.3.2") (h "0qkvcr8bx9kdwljlhd4xm9hihyp298p3b4b8yl7gqmjbkr0h6bq1")))

(define-public crate-extended_matrix-0.3.3 (c (n "extended_matrix") (v "0.3.3") (d (list (d (n "colsol") (r "^0.1.0") (d #t) (k 0)))) (h "1brg4m5kjlg3a7aiv3mwnvm0p8ngw95qi8dnb2n0p8y6c5klz1h7") (y #t)))

(define-public crate-extended_matrix-0.3.4 (c (n "extended_matrix") (v "0.3.4") (d (list (d (n "colsol") (r "^0.1.1") (d #t) (k 0)))) (h "0xrsyd1cz1r124fhjy9bn0igskmkvghymcbdcnjcjyrnm3dp678z") (y #t)))

(define-public crate-extended_matrix-0.3.5 (c (n "extended_matrix") (v "0.3.5") (d (list (d (n "colsol") (r "^0.1.2") (d #t) (k 0)))) (h "1k1nmj0xjrgcgwvil3982nx9z1qc9mfiqbf50gz5pf504vhksq8h") (y #t)))

(define-public crate-extended_matrix-0.3.6 (c (n "extended_matrix") (v "0.3.6") (d (list (d (n "colsol") (r "^0.1.3") (d #t) (k 0)))) (h "08p3al7szn8faxyxwax2d8187wmd9iyjf4b4bpfpw4pimx5vg2qy")))

(define-public crate-extended_matrix-0.3.7 (c (n "extended_matrix") (v "0.3.7") (d (list (d (n "colsol") (r "^0.1.4") (d #t) (k 0)))) (h "1v9ikffv3ydxwhh0c4fcibd11kqs8flfznldmca76kcipgg6wgkq")))

(define-public crate-extended_matrix-0.3.8 (c (n "extended_matrix") (v "0.3.8") (d (list (d (n "colsol") (r "^0.1.4") (d #t) (k 0)))) (h "04iy3q5fsvss6nmzcqwhz5wk8pxan280n6bg2fwchr7ji5qdizl2") (y #t)))

(define-public crate-extended_matrix-0.3.9 (c (n "extended_matrix") (v "0.3.9") (d (list (d (n "colsol") (r "^0.1.4") (d #t) (k 0)))) (h "0jhxh5gym1war02nksbrka75m3y67j48311vx9rn5dpg0gijxmcv")))

(define-public crate-extended_matrix-0.3.10 (c (n "extended_matrix") (v "0.3.10") (d (list (d (n "colsol") (r "^0.1.4") (d #t) (k 0)) (d (n "extended_matrix_float") (r "^0.1.0") (d #t) (k 0)))) (h "1rxzb3lvhd5krp2jr5aa68ybmyp1p2kcrwkmrbhh2v4dy9sv5r1m") (y #t)))

(define-public crate-extended_matrix-0.3.11 (c (n "extended_matrix") (v "0.3.11") (d (list (d (n "colsol") (r "^0.1.4") (d #t) (k 0)) (d (n "extended_matrix_float") (r "^0.1.0") (d #t) (k 0)))) (h "0f1a3774d0q7cl9hdjr3k9d5j9s63p82giksbrs3gq8i9k8szahv")))

(define-public crate-extended_matrix-0.3.12 (c (n "extended_matrix") (v "0.3.12") (d (list (d (n "colsol") (r "^0.1.4") (d #t) (k 0)) (d (n "extended_matrix_float") (r "^0.1.0") (d #t) (k 0)))) (h "1iz57c4w0fas7xzrnlymy0zmgyli8ldp5l87ld8q8nx8gf4sfzvl")))

(define-public crate-extended_matrix-0.3.13 (c (n "extended_matrix") (v "0.3.13") (d (list (d (n "colsol") (r "^0.1.4") (d #t) (k 0)) (d (n "extended_matrix_float") (r "^0.1.0") (d #t) (k 0)))) (h "1ly8rfgbz3a8w4l1qs5f0cwqb0k7lnlncx7jkszha8g5drdm013k")))

(define-public crate-extended_matrix-0.3.14 (c (n "extended_matrix") (v "0.3.14") (d (list (d (n "colsol") (r "^0.1.4") (d #t) (k 0)) (d (n "extended_matrix_float") (r "^0.1.0") (d #t) (k 0)))) (h "1697ppnd7d21zkgp2d3byfqghvcky6pygypd6qcar0d1lrp665w7")))

(define-public crate-extended_matrix-0.3.15 (c (n "extended_matrix") (v "0.3.15") (d (list (d (n "colsol") (r "^0.1.4") (d #t) (k 0)) (d (n "extended_matrix_float") (r "^0.1.0") (d #t) (k 0)))) (h "110mx5wa4sw3pgcnqdlxp6p7qhvz1pcvp6lrgrkchqmlmcdvdcs9")))

(define-public crate-extended_matrix-0.3.16 (c (n "extended_matrix") (v "0.3.16") (d (list (d (n "colsol") (r "^0.1.4") (d #t) (k 0)) (d (n "extended_matrix_float") (r "^0.1.0") (d #t) (k 0)))) (h "154ybb62zlmq9b97sfj047hkjgl26sm7h6zbmp1b33h2vzgsy1ym")))

(define-public crate-extended_matrix-0.3.17 (c (n "extended_matrix") (v "0.3.17") (d (list (d (n "colsol") (r "^0.1.4") (d #t) (k 0)) (d (n "extended_matrix_float") (r "^0.1.0") (d #t) (k 0)))) (h "17pwgc53jkx9s61fww1lwyg0q94jmmyjz97qvikr6fazpq6wd33i")))

(define-public crate-extended_matrix-0.3.18 (c (n "extended_matrix") (v "0.3.18") (d (list (d (n "colsol") (r "^0.1.4") (d #t) (k 0)) (d (n "extended_matrix_float") (r "^0.1.0") (d #t) (k 0)))) (h "116gp6qq6cgby467bdaww8xmy0l138v20i3v3zwb1mcdmxikdppc")))

(define-public crate-extended_matrix-0.9.0 (c (n "extended_matrix") (v "0.9.0") (d (list (d (n "extended_matrix_float") (r "^1.0.0") (d #t) (k 0)))) (h "1phdcz4bg9zs21x1w1nfrd3yhjypcvsqcvkqq8pqf1x1hcf48rsd")))

(define-public crate-extended_matrix-0.9.1 (c (n "extended_matrix") (v "0.9.1") (d (list (d (n "extended_matrix_float") (r "^1.0.0") (d #t) (k 0)))) (h "0zk0z7ln6g0231h8vspi538rcrzlwy7l6rb2nnwf7jb7pxsyg3nm")))

(define-public crate-extended_matrix-0.9.2 (c (n "extended_matrix") (v "0.9.2") (d (list (d (n "extended_matrix_float") (r "^1.0.0") (d #t) (k 0)))) (h "0ypa679p468r9kzc8dh4qkc3r9bax6vj1l93ldij6y8x3b53mfxm")))

