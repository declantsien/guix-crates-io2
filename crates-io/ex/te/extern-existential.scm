(define-module (crates-io ex te extern-existential) #:use-module (crates-io))

(define-public crate-extern-existential-0.1.0 (c (n "extern-existential") (v "0.1.0") (h "1jlwi8y0ws7yfryrbxsxh73z3fvprj6pkv71x3pirqw4ga76a452")))

(define-public crate-extern-existential-0.1.1 (c (n "extern-existential") (v "0.1.1") (d (list (d (n "core") (r "^1.0.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-core")))) (h "11wcmxrdqarh067ifj2ih94b9fqivwcbphix4wjb0hgy4m3ia00a") (f (quote (("rustc-dep-of-std" "core")))) (y #t)))

(define-public crate-extern-existential-0.1.2 (c (n "extern-existential") (v "0.1.2") (d (list (d (n "compiler_builtins") (r "^0.1.2") (o #t) (d #t) (k 0)) (d (n "core") (r "^1.0.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-core")))) (h "18x6iaxk2dk6hshphx0frjjv9bni7xlxpgzxa7hwf0jxszd0ff8z") (f (quote (("rustc-dep-of-std" "core" "compiler_builtins"))))))

