(define-module (crates-io ex te extended-tea) #:use-module (crates-io))

(define-public crate-extended-tea-0.0.0 (c (n "extended-tea") (v "0.0.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)))) (h "13zj403gm0fvc2mxq8aiv9v6j2rrh2j2mm4ccpczz2w002b74is5")))

(define-public crate-extended-tea-0.1.0 (c (n "extended-tea") (v "0.1.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)))) (h "135y96y0h3ll5rbc53xsm85icbvjgxgspra18vm9n5q6693m1bjg")))

(define-public crate-extended-tea-0.1.1 (c (n "extended-tea") (v "0.1.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)))) (h "046l0363n8y7qyj2l56sp08ycmb3znfxi1kcg8n5wrzkgwnzrws6")))

