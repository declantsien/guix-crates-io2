(define-module (crates-io ex rn exrng) #:use-module (crates-io))

(define-public crate-exrng-0.1.0 (c (n "exrng") (v "0.1.0") (d (list (d (n "rand_core") (r "^0.5.1") (k 0)))) (h "0yiimz3drlln76ckx8rl5707yjq2yv37ss8jjxkln91inbvvqbk7")))

(define-public crate-exrng-0.1.1 (c (n "exrng") (v "0.1.1") (d (list (d (n "rand_core") (r "^0.5.1") (k 0)))) (h "02afi7a5dxpbqgy12y7zh843216lcskqpx4n836kaflhgk1aaicd")))

(define-public crate-exrng-0.1.2 (c (n "exrng") (v "0.1.2") (d (list (d (n "rand_core") (r "^0.4.2") (k 0)))) (h "00lzw4kw5mdfdj8qq2yf6spnm5l36pa105wl91sjx80z1ggmjw4k")))

