(define-module (crates-io ex ac exact-reader) #:use-module (crates-io))

(define-public crate-exact-reader-0.1.0 (c (n "exact-reader") (v "0.1.0") (h "0rpbhh5psssninc9wik9sxskf60nmbp7nffi824v8aq6k6ajypga") (y #t)))

(define-public crate-exact-reader-0.1.1 (c (n "exact-reader") (v "0.1.1") (h "0n4g5yply0r3vljfprmpbqh1z1yyhx8nwci4g80x592fzi71jkir") (y #t)))

(define-public crate-exact-reader-0.1.2 (c (n "exact-reader") (v "0.1.2") (h "0ns4zgsjjkfiam2f8jhx0jp2cn12bbnc7bqr7m4yphm6q2rvv2lj")))

(define-public crate-exact-reader-0.1.3 (c (n "exact-reader") (v "0.1.3") (h "15nx1lcng2vacfrp918yqm103y4gs80wc30mcz6pi98i5c5pazgs")))

(define-public crate-exact-reader-0.1.4 (c (n "exact-reader") (v "0.1.4") (h "0hvgi4xhnijg94s7nklkyjmykhxan49wbb0x7b56w5a3hwy4mc38")))

