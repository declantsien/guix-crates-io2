(define-module (crates-io ex ac exacttime) #:use-module (crates-io))

(define-public crate-exacttime-0.1.0 (c (n "exacttime") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "scraper") (r "^0.12") (d #t) (k 0)))) (h "0yvk27ybx4vqgj3pnklk7jxrcbh65r1pjx4cckf6rx98ajbx43kb")))

(define-public crate-exacttime-0.1.1 (c (n "exacttime") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "scraper") (r "^0.12") (d #t) (k 0)))) (h "06iwc4q1qg6250y2pscza7k20hrdrchjbcni11rg9gqg6fi1fyg4")))

