(define-module (crates-io ex ac exact-covers) #:use-module (crates-io))

(define-public crate-exact-covers-0.1.0 (c (n "exact-covers") (v "0.1.0") (h "17xhjbjdaqdjlp60w20a9pv0fdjjsvq5n0ypy9l8pnhp8frcpixb")))

(define-public crate-exact-covers-0.1.1 (c (n "exact-covers") (v "0.1.1") (h "0wj6zs25ijxdhzyliddw29cs794j8kk9fja6n7q4hh8pwwlp60zr")))

