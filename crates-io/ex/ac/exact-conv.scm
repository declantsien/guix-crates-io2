(define-module (crates-io ex ac exact-conv) #:use-module (crates-io))

(define-public crate-exact-conv-0.1.0 (c (n "exact-conv") (v "0.1.0") (h "1jyjnbswxp1syncdl9ixs4y80iy3ymm8ymw1xgk52k749yjfjwq2")))

(define-public crate-exact-conv-0.2.0 (c (n "exact-conv") (v "0.2.0") (h "1zmr4qxrd7yzifvrry69653780hkkqfxzaww756mf8sqbyfcqg9j")))

(define-public crate-exact-conv-0.2.1 (c (n "exact-conv") (v "0.2.1") (h "18y1yvsssrgdcd1fvbcfsp721cdk6mc3djx9ksl0gi5inxrsygcd")))

