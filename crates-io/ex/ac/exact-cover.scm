(define-module (crates-io ex ac exact-cover) #:use-module (crates-io))

(define-public crate-exact-cover-0.1.0 (c (n "exact-cover") (v "0.1.0") (d (list (d (n "futures") (r "^0.3.13") (d #t) (k 0)) (d (n "indexmap") (r "^1.6.2") (d #t) (k 0)))) (h "18qdl42qrn9lff3hkh88nb8vg51qci7f3p3qpjr5pjp0835h1dr8")))

(define-public crate-exact-cover-0.1.1 (c (n "exact-cover") (v "0.1.1") (d (list (d (n "futures") (r "^0.3.13") (d #t) (k 0)) (d (n "indexmap") (r "^1.6.2") (d #t) (k 0)))) (h "0dzdhb9jc15xg6nxxpy4kn7sfhl0a2wav2qgp8vd8y1hi3assl4g")))

