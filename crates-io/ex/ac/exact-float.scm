(define-module (crates-io ex ac exact-float) #:use-module (crates-io))

(define-public crate-exact-float-0.1.0 (c (n "exact-float") (v "0.1.0") (d (list (d (n "num") (r "^0.1.32") (d #t) (k 0)))) (h "173v2nwxwcrlir01gszmihax78jf01bc8prhhngmsizsx7nsar6n")))

(define-public crate-exact-float-0.1.1 (c (n "exact-float") (v "0.1.1") (d (list (d (n "num") (r "^0.1.32") (d #t) (k 0)))) (h "11lyv24y9xj45kf18iccnk54jnbbdkis7a871cfdhx11cbszwzg6")))

