(define-module (crates-io ex ec executor) #:use-module (crates-io))

(define-public crate-executor-0.0.1 (c (n "executor") (v "0.0.1") (d (list (d (n "once_cell") (r "^1.2.0") (d #t) (k 0)) (d (n "woke") (r "^0.0.1") (d #t) (k 0)))) (h "07mwbcr6hxpk253562zvqyjpfxmlkwq04lz8z5nl4iyj533nr181")))

(define-public crate-executor-0.0.2 (c (n "executor") (v "0.0.2") (d (list (d (n "once_cell") (r "^1.2.0") (d #t) (k 0)) (d (n "woke") (r "^0.0.1") (d #t) (k 0)))) (h "0yx7p70wc5dcinivfggh18j70clbvyi9q12ild9izlk7n526v02b")))

(define-public crate-executor-0.0.3 (c (n "executor") (v "0.0.3") (d (list (d (n "once_cell") (r "^1.2.0") (d #t) (k 0)) (d (n "spin") (r "^0.5.2") (d #t) (k 0)) (d (n "woke") (r "^0.0.2") (d #t) (k 0)))) (h "11lszf01vxhwg0rz1a8ib11f3dl8dva7asfsrm8jlxn2m5hfbx1m")))

(define-public crate-executor-0.0.4 (c (n "executor") (v "0.0.4") (d (list (d (n "once_cell") (r "^1.2.0") (d #t) (k 0)) (d (n "spin") (r "^0.5.2") (d #t) (k 0)) (d (n "woke") (r "^0.0.2") (d #t) (k 0)))) (h "00c0pdarfpg8xhz8x9v6cmsvx5950fsb2ad0j4zfl49npsyngh32")))

(define-public crate-executor-0.0.5 (c (n "executor") (v "0.0.5") (d (list (d (n "lazy_static") (r "^1.4.0") (f (quote ("spin_no_std"))) (d #t) (k 0)) (d (n "spin") (r "^0.5") (d #t) (k 0)) (d (n "woke") (r "^0.0.2") (d #t) (k 0)))) (h "1nmmn3mm3pgh1drki547d6iwiw85in0b9qkjafhvbs8n0anxpmrg")))

(define-public crate-executor-0.0.6 (c (n "executor") (v "0.0.6") (d (list (d (n "lazy_static") (r "^1.4.0") (f (quote ("spin_no_std"))) (d #t) (k 0)) (d (n "smallvec") (r "^1.0.0") (d #t) (k 0)) (d (n "spin") (r "^0.5") (d #t) (k 0)) (d (n "woke") (r "^0.0.2") (d #t) (k 0)))) (h "04n48fmdrpz9akic2h7zfsbjbbsj6p59f43ixb42disxx7jklyvx")))

(define-public crate-executor-0.0.7 (c (n "executor") (v "0.0.7") (d (list (d (n "lazy_static") (r "^1.4.0") (f (quote ("spin_no_std"))) (d #t) (k 0)) (d (n "smallvec") (r "^1.0.0") (d #t) (k 0)) (d (n "spin") (r "^0.5") (d #t) (k 0)) (d (n "woke") (r "^0.0.2") (d #t) (k 0)))) (h "1h2h4d085s0v9r34n1rbx26218wx15935hbl0315crl73b5qpxf8")))

(define-public crate-executor-0.1.0 (c (n "executor") (v "0.1.0") (d (list (d (n "globals") (r "^0.1.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (f (quote ("spin_no_std"))) (d #t) (k 0)) (d (n "smallvec") (r "^1.0.0") (d #t) (k 0)) (d (n "spin") (r "^0.5") (d #t) (k 0)) (d (n "woke") (r "^0.0.2") (d #t) (k 0)))) (h "0b1hp70mvvcsrpflbza7j6b7w440d2kk7fc58zawznsx39yq6lxy")))

(define-public crate-executor-0.1.2 (c (n "executor") (v "0.1.2") (d (list (d (n "globals") (r "^0.1.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (f (quote ("spin_no_std"))) (d #t) (k 0)) (d (n "smallvec") (r "^1.0.0") (d #t) (k 0)) (d (n "spin") (r "^0.5") (d #t) (k 0)) (d (n "woke") (r "^0.0.2") (d #t) (k 0)))) (h "0j4mdxlwjgl5xr3gf8crrpqk05xzvn4i425p9rn0z1z749lzfb23")))

(define-public crate-executor-0.1.3 (c (n "executor") (v "0.1.3") (d (list (d (n "globals") (r "^0.1.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (f (quote ("spin_no_std"))) (d #t) (k 0)) (d (n "smallvec") (r "^1.0.0") (d #t) (k 0)) (d (n "spin") (r "^0.5") (d #t) (k 0)) (d (n "woke") (r "^0.0.2") (d #t) (k 0)))) (h "0fpi6xlybq7pi43gpylcrn4yrvl0fknxll4r5wb5w6622v5d52n4")))

(define-public crate-executor-0.1.4 (c (n "executor") (v "0.1.4") (d (list (d (n "globals") (r "^1.0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (f (quote ("spin_no_std"))) (d #t) (k 0)) (d (n "smallvec") (r "^1.0.0") (d #t) (k 0)) (d (n "spin") (r "^0.5") (d #t) (k 0)) (d (n "woke") (r "^0.0.2") (d #t) (k 0)))) (h "1n5ds5zlg93fqwi7rd8zx0m56s3ldcklyn43s2kpngn2v3fpcn8s")))

(define-public crate-executor-0.2.0 (c (n "executor") (v "0.2.0") (d (list (d (n "globals") (r "^1.0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (f (quote ("spin_no_std"))) (d #t) (k 0)) (d (n "smallvec") (r "^1.0.0") (d #t) (k 0)) (d (n "spin") (r "^0.5") (d #t) (k 0)) (d (n "woke") (r "^0.0.2") (d #t) (k 0)))) (h "1l7g9dhzks54wdfxqvr7g926cz3n1ijg0z57bjf5l2s3sacc5adn")))

(define-public crate-executor-0.3.0 (c (n "executor") (v "0.3.0") (d (list (d (n "lazy_static") (r "^1.4.0") (f (quote ("spin_no_std"))) (d #t) (k 0)) (d (n "smallvec") (r "^1.0.0") (d #t) (k 0)) (d (n "spin") (r "^0.5") (d #t) (k 0)) (d (n "woke") (r "^0.0.2") (d #t) (k 0)))) (h "0q98j528g5nzfcap66lxlscbc0vddpnhlf7x0sn2d4y0viyh24q6")))

(define-public crate-executor-0.4.0 (c (n "executor") (v "0.4.0") (d (list (d (n "lazy_static") (r "^1.4.0") (f (quote ("spin_no_std"))) (d #t) (k 0)) (d (n "smallvec") (r "^1.0.0") (d #t) (k 0)) (d (n "spin") (r "^0.5") (d #t) (k 0)) (d (n "woke") (r "^0.0.2") (d #t) (k 0)))) (h "06iz32va5dlxb277v7qx7k07qlw5i1fia17grrh4gqrv6yq05894")))

(define-public crate-executor-0.5.1 (c (n "executor") (v "0.5.1") (d (list (d (n "lazy_static") (r "^1.4.0") (f (quote ("spin_no_std"))) (d #t) (k 0)) (d (n "smallvec") (r "^1.0.0") (d #t) (k 0)) (d (n "spin") (r "^0.5") (d #t) (k 0)) (d (n "woke") (r "^0.0.2") (d #t) (k 0)))) (h "0y171a9lxzx75kyyc6bl1wf0fv59l0prbrlv6rfmgqip0a9m0dr8")))

(define-public crate-executor-0.5.2 (c (n "executor") (v "0.5.2") (d (list (d (n "lazy_static") (r "^1.4.0") (f (quote ("spin_no_std"))) (d #t) (k 0)) (d (n "spin") (r "^0.5") (d #t) (k 0)) (d (n "woke") (r "^0.0.2") (d #t) (k 0)))) (h "1c40ba672xv1fiscsys9zv0akxikyvmzkcb2g97a9vglrqkg121a")))

(define-public crate-executor-0.5.3 (c (n "executor") (v "0.5.3") (d (list (d (n "lazy_static") (r "^1.4.0") (f (quote ("spin_no_std"))) (d #t) (k 0)) (d (n "spin") (r "^0.5") (d #t) (k 0)) (d (n "woke") (r "^0.0.2") (d #t) (k 0)))) (h "0m4lsl34wccbx4h9qq5zkmiksb66byjcqsy2ml9sj5z5cxjri1rn")))

(define-public crate-executor-0.6.0 (c (n "executor") (v "0.6.0") (d (list (d (n "executor-macros") (r "^0.0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (f (quote ("spin_no_std"))) (d #t) (k 0)) (d (n "spin") (r "^0.5.2") (d #t) (k 0)) (d (n "woke") (r "^0.0.2") (d #t) (k 0)))) (h "1kawm3y9li6ifn4m07a516d0l626glbnapi1v1kci9q1mq5qs9q6")))

(define-public crate-executor-0.6.1 (c (n "executor") (v "0.6.1") (d (list (d (n "executor-macros") (r "^0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (f (quote ("spin_no_std"))) (d #t) (k 0)) (d (n "spin") (r "^0.5.2") (d #t) (k 0)) (d (n "woke") (r "^0.0.2") (d #t) (k 0)))) (h "0s97k7a28ih3dw73yk9l3dlqjjbxnjlim39rydra38s5cp8w9818")))

(define-public crate-executor-0.7.0 (c (n "executor") (v "0.7.0") (d (list (d (n "executor-macros") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (f (quote ("spin_no_std"))) (d #t) (k 0)) (d (n "spin") (r "^0.5.2") (d #t) (k 0)) (d (n "woke") (r "^0.0.2") (d #t) (k 0)))) (h "14b1nm2cq6qxnvj21hli64xzqazzx99970j5vk1c556rvfn8dhsz")))

(define-public crate-executor-0.8.0 (c (n "executor") (v "0.8.0") (d (list (d (n "async-std") (r "^1") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (f (quote ("spin_no_std"))) (d #t) (k 0)) (d (n "spin") (r "^0.5.2") (d #t) (k 0)) (d (n "woke") (r "^0.0.2") (d #t) (k 0)))) (h "0axmma9246awr4yqhisxqfhzbcz8p8vpyvgl6rxhy0rhm4wp75jp")))

(define-public crate-executor-0.8.2 (c (n "executor") (v "0.8.2") (d (list (d (n "async-std") (r "^1") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (f (quote ("spin_no_std"))) (d #t) (k 0)) (d (n "spin") (r "^0.5.2") (d #t) (k 0)) (d (n "woke") (r "^0.0.2") (d #t) (k 0)))) (h "12g5dy0rip5lsrpfcll47dqa1p9dz6l95pdcmmlfxkjy69ap1f80")))

(define-public crate-executor-0.8.3 (c (n "executor") (v "0.8.3") (d (list (d (n "async-std") (r "^1") (d #t) (k 2)) (d (n "spin") (r "^0.5.2") (d #t) (k 0)) (d (n "woke") (r "^0.0.3") (d #t) (k 0)))) (h "1p1yr161jki5mrkvyy4117ivpbn1gib1qd7cdwapzcvq2rmkflyl") (r "1.63")))

(define-public crate-executor-0.8.4 (c (n "executor") (v "0.8.4") (d (list (d (n "async-std") (r "^1") (d #t) (k 2)) (d (n "spin") (r "^0") (d #t) (k 0)) (d (n "woke") (r "^0") (d #t) (k 0)))) (h "0nv8y8zdwfapi1xwl2506xj8hfpbnkxkkzf9x9zf3jgv234fzykv") (r "1.63")))

