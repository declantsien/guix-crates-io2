(define-module (crates-io ex ec execsnoop) #:use-module (crates-io))

(define-public crate-execsnoop-0.2.0 (c (n "execsnoop") (v "0.2.0") (d (list (d (n "atoi") (r "^2.0.0") (d #t) (k 0)) (d (n "bstr") (r "^1.4.0") (d #t) (k 0)) (d (n "bytelines") (r "^2.4.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "1wkmzn8fy1xwyqwdwwi5npz4387zjws87mk4k1pzr8nv3c6bayqy")))

