(define-module (crates-io ex ec executable-finder) #:use-module (crates-io))

(define-public crate-executable-finder-0.1.0 (c (n "executable-finder") (v "0.1.0") (h "1ih7cbn3whaprbwzma91hnpmx7fb790bd208yy04qmpmsaxcrv58")))

(define-public crate-executable-finder-0.1.1 (c (n "executable-finder") (v "0.1.1") (h "0a5590bgcw2lr7k2mkq644zgj0d2wq5fbm41d7hb035vaknzqfb5")))

(define-public crate-executable-finder-0.1.2 (c (n "executable-finder") (v "0.1.2") (h "1plyzb8ffhljh9j61w4ma1xsav9q8mrhmgs12qz0m3j6f3yyph5r")))

(define-public crate-executable-finder-0.1.3 (c (n "executable-finder") (v "0.1.3") (h "01r8bmkzabgrjhs0l24wfg065br0vabia6362jr3xwirsm7prkyw")))

(define-public crate-executable-finder-0.2.0 (c (n "executable-finder") (v "0.2.0") (d (list (d (n "rayon") (r "^1.5.3") (o #t) (d #t) (k 0)))) (h "0napwkfa9q33zmjih9f665lrrq3bd4y8yb8cxfllqr4mwhv41rj8") (f (quote (("default"))))))

(define-public crate-executable-finder-0.2.1 (c (n "executable-finder") (v "0.2.1") (d (list (d (n "rayon") (r "^1.5.3") (o #t) (d #t) (k 0)))) (h "0fqgbddz9jqa1f4ni51d0l18qrdzb3wr8by22ylrrbskx11bg5qk") (f (quote (("default"))))))

(define-public crate-executable-finder-0.2.2 (c (n "executable-finder") (v "0.2.2") (d (list (d (n "rayon") (r "^1.5.3") (o #t) (d #t) (k 0)))) (h "0my41np9ps4ljvv1a3zykp77mb2fchkb2fq7jk7sv8h3lapj6xdj") (f (quote (("default"))))))

(define-public crate-executable-finder-0.3.0 (c (n "executable-finder") (v "0.3.0") (d (list (d (n "rayon") (r "^1.5.3") (o #t) (d #t) (k 0)))) (h "1n8ll2wa0yjkqw8369m9z71z1jdqn1myhk6h22gbwrzn85kkp525") (f (quote (("default"))))))

