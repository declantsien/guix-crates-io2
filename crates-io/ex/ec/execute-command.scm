(define-module (crates-io ex ec execute-command) #:use-module (crates-io))

(define-public crate-execute-command-0.1.0 (c (n "execute-command") (v "0.1.0") (h "0ka8g5xgqx648vhqz4f8i31ibfg8mlxm037yah6vnqcwd79rbmbz") (y #t)))

(define-public crate-execute-command-0.0.0 (c (n "execute-command") (v "0.0.0") (h "19pzrh1rhf25vgkkmffx0w45qxks26d9mhdj1g4ygxr2ik1dzxaz") (y #t)))

(define-public crate-execute-command-0.2.0 (c (n "execute-command") (v "0.2.0") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("user-hooks"))) (k 2)) (d (n "shell-words") (r "^1.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0958pqviww76qmfhr09bxhr6mas4q4l0kxmz10rp3511y9m3m9zq")))

(define-public crate-execute-command-0.3.0 (c (n "execute-command") (v "0.3.0") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("user-hooks"))) (k 2)) (d (n "shell-words") (r "^1.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0ljz3br2r44cm29cfj3mycqdwhc608kynfgrrh2qj4py11kx07fp")))

