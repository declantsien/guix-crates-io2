(define-module (crates-io ex ec exec-target) #:use-module (crates-io))

(define-public crate-exec-target-0.2.3 (c (n "exec-target") (v "0.2.3") (h "0nknw6p1wcywi162q8k73c1z363n1j1b0k7zal26cl3046nx4gnp")))

(define-public crate-exec-target-0.2.4 (c (n "exec-target") (v "0.2.4") (h "02fb5mg3i5wcqnalaj66v6f50r5svxr2jcfq3z6iqbzc7lj5a9w2")))

(define-public crate-exec-target-0.2.5 (c (n "exec-target") (v "0.2.5") (h "0a8bdcsa2q3hp2137c1d5kn812j5pygq58db7i1hgr13hqg6w17a")))

(define-public crate-exec-target-0.2.6 (c (n "exec-target") (v "0.2.6") (h "1xfnp8a91snn5s1yq22mvd402kcjwl1ci59hkbj3lhvq87g7hfgf")))

(define-public crate-exec-target-0.2.7 (c (n "exec-target") (v "0.2.7") (h "0wyzwkpjhgwi0nlavxsnd91mfmr81ir9kj7gayakskcxkcf09j26")))

(define-public crate-exec-target-0.2.8 (c (n "exec-target") (v "0.2.8") (h "1jkjn6rkg795f1acjc6wy0741cxfca1hn6krkhypcgyik7maksgg") (r "1.56.0")))

(define-public crate-exec-target-0.2.9 (c (n "exec-target") (v "0.2.9") (h "0d7cid04bi3pzlk6q97jwhfirzfvqxbdsg1vyxd0wr51yrqlrlz0") (r "1.56.0")))

