(define-module (crates-io ex ec exec) #:use-module (crates-io))

(define-public crate-exec-0.1.0 (c (n "exec") (v "0.1.0") (d (list (d (n "errno") (r "^0.1.6") (d #t) (k 0)) (d (n "libc") (r "^0.2.8") (d #t) (k 0)))) (h "1ld7q0qd1ys7vv79sfxbiiqxri8x1ln9rm1w8ya14ygv7zp690b1")))

(define-public crate-exec-0.1.1 (c (n "exec") (v "0.1.1") (d (list (d (n "errno") (r "^0.1.6") (d #t) (k 0)) (d (n "libc") (r "^0.2.8") (d #t) (k 0)))) (h "1a6a34cn456b1snjgid7b4v639jpbw0jkqnkx6x2c3x1xmgd4ay9") (f (quote (("unstable"))))))

(define-public crate-exec-0.2.0 (c (n "exec") (v "0.2.0") (d (list (d (n "errno") (r "^0.1.6") (d #t) (k 0)) (d (n "libc") (r "^0.2.8") (d #t) (k 0)))) (h "05x7c46afrxs409wd3k9ipm19qvxqig9qmpdkbiwarvzgiyx9kfq") (f (quote (("unstable"))))))

(define-public crate-exec-0.3.0 (c (n "exec") (v "0.3.0") (d (list (d (n "errno") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.8") (d #t) (k 0)))) (h "1whillpa8vhkpzar38jzlp5v1bzjswdr0ia180f102y272xd0rbm") (f (quote (("unstable"))))))

(define-public crate-exec-0.3.1 (c (n "exec") (v "0.3.1") (d (list (d (n "errno") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.8") (d #t) (k 0)))) (h "0596cw844vw1aw5saawg13av25mywhfqx195q2zp325sihr70sw8") (f (quote (("unstable"))))))

