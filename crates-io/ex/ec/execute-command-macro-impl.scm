(define-module (crates-io ex ec execute-command-macro-impl) #:use-module (crates-io))

(define-public crate-execute-command-macro-impl-0.1.0 (c (n "execute-command-macro-impl") (v "0.1.0") (d (list (d (n "execute-command-tokens") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1b5wsrsh571bzxp985bnzrxbw1bqy9k4p91cf73h8ig845mcaia3") (y #t)))

(define-public crate-execute-command-macro-impl-0.1.1 (c (n "execute-command-macro-impl") (v "0.1.1") (d (list (d (n "execute-command-tokens") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "07ylnw3kdwzq8c1ksw17i5rq463454rkv1a1qxmh4f09gxx1rvy7")))

(define-public crate-execute-command-macro-impl-0.1.2 (c (n "execute-command-macro-impl") (v "0.1.2") (d (list (d (n "execute-command-tokens") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "04yvhl4yl8cl68n0k9qnskjyjk8304r5yvp0fwzl8v5hwhsjgd4m")))

(define-public crate-execute-command-macro-impl-0.1.3 (c (n "execute-command-macro-impl") (v "0.1.3") (d (list (d (n "execute-command-tokens") (r "^0.1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0lajpvvbjf5fm4qzsfzz8lxi8hd80drc9nyrbxzlw3g447yv4rbq")))

(define-public crate-execute-command-macro-impl-0.1.4 (c (n "execute-command-macro-impl") (v "0.1.4") (d (list (d (n "execute-command-tokens") (r "^0.1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "07xq7acqc4aif8lkxnn1fs5ccp6mb024gmh21qa4ljv9cigall0x")))

(define-public crate-execute-command-macro-impl-0.1.5 (c (n "execute-command-macro-impl") (v "0.1.5") (d (list (d (n "execute-command-tokens") (r "^0.1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0vs9xixxkkcwd55i6rrrdp4wqbzvi576z4bq997ywbh5l2hia3ak")))

(define-public crate-execute-command-macro-impl-0.1.6 (c (n "execute-command-macro-impl") (v "0.1.6") (d (list (d (n "execute-command-tokens") (r "^0.1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0hj1xa3srzb9bds1kdly9si19p1dr44845jb1pzmwlp00hwyx2yc")))

(define-public crate-execute-command-macro-impl-0.1.7 (c (n "execute-command-macro-impl") (v "0.1.7") (d (list (d (n "execute-command-tokens") (r "^0.1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1f6cgfiqwrcr6siicwi5vzc8zwmz14gd0ydkz10msjl9rm5k0jmg")))

(define-public crate-execute-command-macro-impl-0.1.8 (c (n "execute-command-macro-impl") (v "0.1.8") (d (list (d (n "execute-command-tokens") (r "^0.1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "06x9kiqrdg4agcnx3xz9iy64kq2pdhznycnscnkfszymkjygc2ai")))

(define-public crate-execute-command-macro-impl-0.1.9 (c (n "execute-command-macro-impl") (v "0.1.9") (d (list (d (n "execute-command-tokens") (r "^0.1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "18yrag6y6rdzqg72ibkqmkifyjk8dwv8xm288s2hffxb3mfsbaam") (r "1.56")))

(define-public crate-execute-command-macro-impl-0.1.10 (c (n "execute-command-macro-impl") (v "0.1.10") (d (list (d (n "execute-command-tokens") (r "^0.1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0jygv1jhfkqglascjk7yq3m2kdgm1yd3y9kikjmhbl0s0imd936f") (r "1.65")))

