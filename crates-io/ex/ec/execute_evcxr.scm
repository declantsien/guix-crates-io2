(define-module (crates-io ex ec execute_evcxr) #:use-module (crates-io))

(define-public crate-execute_evcxr-0.1.0 (c (n "execute_evcxr") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.39") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full" "extra-traits" "parsing" "printing"))) (d #t) (k 0)))) (h "00c6sv9n5s2hzb7rzl8m422sam97gyj5ir13lrkhnnsw80104h7g") (r "1.56.1")))

(define-public crate-execute_evcxr-0.1.1 (c (n "execute_evcxr") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.39") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full" "extra-traits" "parsing" "printing"))) (d #t) (k 0)))) (h "1y6dhwp3nmmasdg6a1ij3x2vzfiv7qmxsws3dlk9c26imd49d6rq") (r "1.56.1")))

