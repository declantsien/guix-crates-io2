(define-module (crates-io ex ec execution-context) #:use-module (crates-io))

(define-public crate-execution-context-0.1.0 (c (n "execution-context") (v "0.1.0") (d (list (d (n "im") (r "^10.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.1") (d #t) (k 0)))) (h "1q0hjhi2a093xgivavnsnjk5m7j5zmdqqz0184pflxb7v2pamdni")))

