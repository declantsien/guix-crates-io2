(define-module (crates-io ex ec exec_time) #:use-module (crates-io))

(define-public crate-exec_time-0.1.0 (c (n "exec_time") (v "0.1.0") (d (list (d (n "darling") (r "^0.9.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15.42") (f (quote ("full"))) (d #t) (k 0)))) (h "1g3yrwdlyj9zpbrf6jmc40lp7mm5fgisc4r1l43zym9w9fcqpsk0")))

(define-public crate-exec_time-0.1.1 (c (n "exec_time") (v "0.1.1") (d (list (d (n "darling") (r "^0.9.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15.42") (f (quote ("full"))) (d #t) (k 0)))) (h "1bazm0mq8h7iswp216hdawdnw5i5npa3r7hi4cp4vj720yp0gd06")))

(define-public crate-exec_time-0.1.2 (c (n "exec_time") (v "0.1.2") (d (list (d (n "darling") (r "^0.9.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15.42") (f (quote ("full"))) (d #t) (k 0)))) (h "08jz6b4a3gm0h4n34yk7plsd0wv00mkx2pvkgffxpng9v8md8kvc")))

(define-public crate-exec_time-0.1.3 (c (n "exec_time") (v "0.1.3") (d (list (d (n "darling") (r "^0.9.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15.42") (f (quote ("full"))) (d #t) (k 0)))) (h "10a6l8dzlz3rjp0zgn333xaxssnk1ljs0279nilk41nm0jpnh4lg")))

(define-public crate-exec_time-0.1.4 (c (n "exec_time") (v "0.1.4") (d (list (d (n "darling") (r "^0.9.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15.42") (f (quote ("full"))) (d #t) (k 0)))) (h "0xyl0y0nfvwrdqgrc2sqkv7awgqnmlslizqd60zcnaa4nvhvaxf0")))

