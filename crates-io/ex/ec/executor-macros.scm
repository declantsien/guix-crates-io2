(define-module (crates-io ex ec executor-macros) #:use-module (crates-io))

(define-public crate-executor-macros-0.0.1 (c (n "executor-macros") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1.0.7") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.3") (f (quote ("full"))) (d #t) (k 0)))) (h "099wmi1i1mhybmcp69zbbsxpzn7y640lkqz0ra10h6hmcvbvmvdi")))

(define-public crate-executor-macros-0.0.2 (c (n "executor-macros") (v "0.0.2") (d (list (d (n "proc-macro2") (r "^1.0.7") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.3") (f (quote ("full"))) (d #t) (k 0)))) (h "0z309czdhzpv7chfvcq4yfg8grn67952c0vh41pfcakayyx8djxf")))

(define-public crate-executor-macros-0.1.0 (c (n "executor-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.7") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.3") (f (quote ("full"))) (d #t) (k 0)))) (h "1lsd7dmmgmhzgdyc504s0q8y5li61c6nnbn74vrjnyakpl0gi5g2")))

(define-public crate-executor-macros-0.1.1 (c (n "executor-macros") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.7") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.3") (f (quote ("full"))) (d #t) (k 0)))) (h "00f46r17ski2hq6dcjp45lwm0bmlppdrq2gml6p5b2zvcsk2qw3n")))

