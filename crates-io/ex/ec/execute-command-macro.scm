(define-module (crates-io ex ec execute-command-macro) #:use-module (crates-io))

(define-public crate-execute-command-macro-0.1.0 (c (n "execute-command-macro") (v "0.1.0") (d (list (d (n "execute-command-macro-impl") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)))) (h "0jpqsyb528x4d3kdjp0bis3b6pq3kmpprjwvm6jckphqihzjdwgr")))

(define-public crate-execute-command-macro-0.1.1 (c (n "execute-command-macro") (v "0.1.1") (d (list (d (n "execute-command-macro-impl") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)))) (h "1zwy6xs55dawmzjr5jc9paj6fznws48w3dmw78rf4byssnmm6hrb")))

(define-public crate-execute-command-macro-0.1.2 (c (n "execute-command-macro") (v "0.1.2") (d (list (d (n "execute-command-macro-impl") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)))) (h "19clpq2s9j2kcwlpcrnhby7g1wysmcqxmz2m7rai5mhnf8h407l7")))

(define-public crate-execute-command-macro-0.1.3 (c (n "execute-command-macro") (v "0.1.3") (d (list (d (n "execute-command-macro-impl") (r "^0.1") (d #t) (k 0)))) (h "0qbpsyqhjhyzvzlqm0jwmsd50rh89551dj2skccvsdmhr05aybq0")))

(define-public crate-execute-command-macro-0.1.4 (c (n "execute-command-macro") (v "0.1.4") (d (list (d (n "execute-command-macro-impl") (r "^0.1") (d #t) (k 0)))) (h "05dzh9q28a894bqp05skb3mhn5hnr8ckc6qiz79x3kg5chp0pp15")))

(define-public crate-execute-command-macro-0.1.5 (c (n "execute-command-macro") (v "0.1.5") (d (list (d (n "execute-command-macro-impl") (r "^0.1") (d #t) (k 0)))) (h "0hy1yciwyw59ikr8vxri32c9k1j6xifipmdm3gpwj11ws762a4xb")))

(define-public crate-execute-command-macro-0.1.6 (c (n "execute-command-macro") (v "0.1.6") (d (list (d (n "execute-command-macro-impl") (r "^0.1") (d #t) (k 0)))) (h "1ak9rx7wdwbp0dxnpxsmkic4zbfhp1d7hk6czi8qgi7sa40qhwak")))

(define-public crate-execute-command-macro-0.1.7 (c (n "execute-command-macro") (v "0.1.7") (d (list (d (n "execute-command-macro-impl") (r "^0.1") (d #t) (k 0)))) (h "1ssyzr4dm2axy2mp7dvz4srx056qh6q60cwivqnh5rnzxcsyslvb")))

(define-public crate-execute-command-macro-0.1.8 (c (n "execute-command-macro") (v "0.1.8") (d (list (d (n "execute-command-macro-impl") (r "^0.1") (d #t) (k 0)))) (h "0mrim9nnjg69lchhvhn2nlsfghbi6sd8rhzl8dki0dgp1idcdyx5")))

(define-public crate-execute-command-macro-0.1.9 (c (n "execute-command-macro") (v "0.1.9") (d (list (d (n "execute-command-macro-impl") (r "^0.1") (d #t) (k 0)))) (h "1g88vmsm3nf0xdq266m0dwdg8k564svyrwsgvh8yjr3mahywbplh") (r "1.65")))

