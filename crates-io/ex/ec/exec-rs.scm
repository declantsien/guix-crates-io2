(define-module (crates-io ex ec exec-rs) #:use-module (crates-io))

(define-public crate-exec-rs-0.1.0 (c (n "exec-rs") (v "0.1.0") (d (list (d (n "flurry") (r "^0.3.1") (o #t) (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.1") (o #t) (d #t) (k 0)))) (h "0a38fz48mivih4fxws1kjzabaa61s2wkxcz8aq6n6ldhliq15y45") (f (quote (("sync" "flurry" "parking_lot") ("default" "sync"))))))

(define-public crate-exec-rs-0.1.1 (c (n "exec-rs") (v "0.1.1") (d (list (d (n "flurry") (r "^0.3.1") (o #t) (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.1") (o #t) (d #t) (k 0)))) (h "0j9pa2hzbyxra8a280jj1673xp2ll910j6al6203w4vyi3yc6ka6") (f (quote (("sync" "flurry" "parking_lot") ("default" "sync"))))))

(define-public crate-exec-rs-0.1.2 (c (n "exec-rs") (v "0.1.2") (d (list (d (n "flurry") (r "^0.3.1") (o #t) (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.1") (o #t) (d #t) (k 0)))) (h "07zam1ivl4b6xbfjfqwbapsyx1qyi1zc0ddnq7d6kwzdj0ybx0gn") (f (quote (("sync" "flurry" "parking_lot") ("default" "sync"))))))

