(define-module (crates-io ex ec executor-trait) #:use-module (crates-io))

(define-public crate-executor-trait-0.0.0 (c (n "executor-trait") (v "0.0.0") (h "1k6vfh7g8bcklngpn8jfxj6bk0qa60kp28k0yf0gh4p03b9fbr99")))

(define-public crate-executor-trait-0.1.0 (c (n "executor-trait") (v "0.1.0") (d (list (d (n "async-trait") (r ">=0.1.42, <0.2.0") (d #t) (k 0)))) (h "1k4kjzi6qn18dcyn29shq0cvrdibwjgg9xld8pnza60yq41f8gx8")))

(define-public crate-executor-trait-0.2.0 (c (n "executor-trait") (v "0.2.0") (d (list (d (n "async-trait") (r ">=0.1.42, <0.2.0") (d #t) (k 0)))) (h "0y9r1gn5lpvvbm4rzhsiv6f6pilwj82gjx90msir57g62nhfld60")))

(define-public crate-executor-trait-0.3.0 (c (n "executor-trait") (v "0.3.0") (d (list (d (n "async-trait") (r ">=0.1.42, <0.2.0") (d #t) (k 0)))) (h "0gwp1afz9ngy83viqnfx6w517p519vysbdmv4794x14vfmj2086l")))

(define-public crate-executor-trait-0.3.1 (c (n "executor-trait") (v "0.3.1") (d (list (d (n "async-trait") (r ">=0.1.42, <0.2.0") (d #t) (k 0)))) (h "1gz6vdxppg1zkn5wrsvhh3b73cxap72ydnbfq81fc0san9kd5zkr")))

(define-public crate-executor-trait-0.4.0 (c (n "executor-trait") (v "0.4.0") (d (list (d (n "async-trait") (r ">=0.1.42, <0.2.0") (d #t) (k 0)))) (h "0s3c27pmk61pjbrbpjiq5wn9nwk6cnvin0441vnkb1w0gd8dh3km")))

(define-public crate-executor-trait-0.5.0 (c (n "executor-trait") (v "0.5.0") (d (list (d (n "async-trait") (r ">=0.1.42, <0.2.0") (d #t) (k 0)))) (h "0xxbagjk45d0ny2jjfq5w82vvfhg1zxf5bqic1h3jgdzbjsxikcm")))

(define-public crate-executor-trait-0.6.0 (c (n "executor-trait") (v "0.6.0") (d (list (d (n "async-trait") (r ">=0.1.42, <0.2.0") (d #t) (k 0)))) (h "0425wh7cs8vkz54a78z6qkxkqg02ki1h7chdsmc542f4f1swj0v3")))

(define-public crate-executor-trait-0.6.1 (c (n "executor-trait") (v "0.6.1") (d (list (d (n "async-trait") (r ">=0.1.42, <0.2.0") (d #t) (k 0)))) (h "1dnjnrz395dibav3n9yzkjs3wnz0r6jds8p00n68sziss21qqb9b")))

(define-public crate-executor-trait-0.7.0 (c (n "executor-trait") (v "0.7.0") (d (list (d (n "async-trait") (r ">=0.1.42, <0.2.0") (d #t) (k 0)))) (h "0gphnbdyw82sf2kka7mhwsmd1pr74c3fclmnny3sf3s29z3vlygl")))

(define-public crate-executor-trait-0.8.0 (c (n "executor-trait") (v "0.8.0") (d (list (d (n "async-trait") (r "^0.1.42") (d #t) (k 0)))) (h "1ihgass1xshi9vs6pxr05qr198amg367xhhqplr7pn7n4q2n8shq")))

(define-public crate-executor-trait-0.9.0 (c (n "executor-trait") (v "0.9.0") (d (list (d (n "async-trait") (r "^0.1.42") (d #t) (k 0)))) (h "1b6crz02swksqy5y28anj0dbg52nzfsnvmbrv236r66p0g40y7ws")))

(define-public crate-executor-trait-1.0.0 (c (n "executor-trait") (v "1.0.0") (d (list (d (n "async-trait") (r "^0.1.42") (d #t) (k 0)))) (h "0vny3fysgix3rkl244k9x2kiqyzjsi3rk4xbm25mbrls4k07dvfy") (y #t)))

(define-public crate-executor-trait-1.0.1 (c (n "executor-trait") (v "1.0.1") (d (list (d (n "async-trait") (r "^0.1.42") (d #t) (k 0)))) (h "02vawdcshalcg2xbzx4divn41dnibvana8fbj6h5s4icc8j2naps")))

(define-public crate-executor-trait-1.0.2 (c (n "executor-trait") (v "1.0.2") (d (list (d (n "async-trait") (r "^0.1.42") (d #t) (k 0)))) (h "0i8iv1cal38qdg6x544bq6aarws9dlrbkq4wyzcssjhpvhzrshb0")))

(define-public crate-executor-trait-1.0.3 (c (n "executor-trait") (v "1.0.3") (d (list (d (n "async-trait") (r "^0.1.42") (d #t) (k 0)))) (h "1b00byz0flwhpzyipc2ra6qcc7aj4xkm31lb7zv4ngsrkldfk5iv")))

(define-public crate-executor-trait-2.0.0 (c (n "executor-trait") (v "2.0.0") (d (list (d (n "async-trait") (r "^0.1.42") (d #t) (k 0)))) (h "16pwipnrhwb8c1rwrskng6ih0fp5bk7nyhv9ba9im11qzv62jqby") (y #t)))

(define-public crate-executor-trait-2.0.1 (c (n "executor-trait") (v "2.0.1") (d (list (d (n "async-trait") (r "^0.1.42") (d #t) (k 0)))) (h "15rhqyd3q75ib9xraz3p8v7wfbf0d7qh74xvjpnhidz2x34v71bs")))

(define-public crate-executor-trait-2.0.2 (c (n "executor-trait") (v "2.0.2") (d (list (d (n "async-trait") (r "^0.1.42") (d #t) (k 0)))) (h "1dwcn9m8ms2bwbpfdvy87w18n7yf8d8yh2j8wrkwfwadsmnwv77n")))

(define-public crate-executor-trait-2.1.0 (c (n "executor-trait") (v "2.1.0") (d (list (d (n "async-trait") (r "^0.1.42") (d #t) (k 0)))) (h "023bs22kpcx2q407vi5f0wpybxajv8bv2sbaxivpfai18gfm440s") (r "1.56.0")))

