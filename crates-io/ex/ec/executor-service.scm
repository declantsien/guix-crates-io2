(define-module (crates-io ex ec executor-service) #:use-module (crates-io))

(define-public crate-executor-service-0.1.0 (c (n "executor-service") (v "0.1.0") (h "1kkh6ib7xa5c2yany4dwwi9kkxvgy4swmfsnipdp0575dlmyllim") (y #t)))

(define-public crate-executor-service-0.1.1 (c (n "executor-service") (v "0.1.1") (h "0afvyq7rnbha0lw7s1p6hrx11prh4ffifda5m3v4v0lhq3gl5gdn") (y #t)))

(define-public crate-executor-service-0.1.2 (c (n "executor-service") (v "0.1.2") (h "1zmq5bkm6l1msq4gv20d9nszvws3pjfzb01dp8kam63sprw41kxv") (y #t)))

(define-public crate-executor-service-0.1.3 (c (n "executor-service") (v "0.1.3") (d (list (d (n "log") (r "^0") (d #t) (k 0)) (d (n "ctor") (r "^0") (d #t) (k 2)) (d (n "env_logger") (r "^0") (d #t) (k 2)))) (h "047nnq1qi2cnaigazyhkiwjax41hn6jy8rkg3nifb6kgal910f3m") (y #t)))

(define-public crate-executor-service-0.2.1 (c (n "executor-service") (v "0.2.1") (d (list (d (n "log") (r "^0") (d #t) (k 0)) (d (n "ctor") (r "^0") (d #t) (k 2)) (d (n "env_logger") (r "^0") (d #t) (k 2)))) (h "0aal30sjryml8m7dzq41naw8v1gdhb6my7bc7c658hch884adpiv")))

(define-public crate-executor-service-0.2.2 (c (n "executor-service") (v "0.2.2") (d (list (d (n "log") (r "^0") (d #t) (k 0)) (d (n "ctor") (r "^0") (d #t) (k 2)) (d (n "env_logger") (r "^0") (d #t) (k 2)))) (h "1rqm3n9arhadcyagq5kg0jr4gd72mlfh6sccy4m3ri1rdns1j5kn")))

