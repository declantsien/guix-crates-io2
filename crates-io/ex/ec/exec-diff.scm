(define-module (crates-io ex ec exec-diff) #:use-module (crates-io))

(define-public crate-exec-diff-0.0.0 (c (n "exec-diff") (v "0.0.0") (d (list (d (n "command-extra") (r "^1.0.0") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "pipe-trait") (r "^0.4.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.5.0") (d #t) (k 0)))) (h "0b2l92zk4gjrvkndify5w17ixwghmki3q1v17g39rgvrmnqm326s")))

(define-public crate-exec-diff-0.1.0 (c (n "exec-diff") (v "0.1.0") (d (list (d (n "command-extra") (r "^1.0.0") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "pipe-trait") (r "^0.4.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.5.0") (d #t) (k 0)))) (h "0n4lb7kf62w39f6830nvrmlj129is1w438hqw8q6zhihdg69q036")))

(define-public crate-exec-diff-0.1.1 (c (n "exec-diff") (v "0.1.1") (d (list (d (n "command-extra") (r "^1.0.0") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "pipe-trait") (r "^0.4.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.5.0") (d #t) (k 0)))) (h "0f35skm5bcpnnv68843m0f2123ilfa0kad4yacbf59hlk9lal13j")))

