(define-module (crates-io ex ec executable_memory) #:use-module (crates-io))

(define-public crate-executable_memory-0.1.0 (c (n "executable_memory") (v "0.1.0") (d (list (d (n "kernel32-sys") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.2") (d #t) (k 0)))) (h "0afdni7pxfxracsjh83n186sqbrqv7bq0v6pmqw5am7931jl4b3s")))

(define-public crate-executable_memory-0.1.2 (c (n "executable_memory") (v "0.1.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("memoryapi" "winnt" "errhandlingapi"))) (d #t) (k 0)))) (h "13va7p5ipzzy1lrwzw1kqlbnwjr5qj5rxrhf2vq105h4vf7hx72p")))

