(define-module (crates-io ex ec execute-command-tokens) #:use-module (crates-io))

(define-public crate-execute-command-tokens-0.1.0 (c (n "execute-command-tokens") (v "0.1.0") (h "08clsv3z53lfw6wlyvhk6izxr069q2w6x6ivppkbh16ynri28sny")))

(define-public crate-execute-command-tokens-0.1.1 (c (n "execute-command-tokens") (v "0.1.1") (h "0ridv370133mwz21v7545m32k9dfxpphs5wpi0dx5jkbvqv89fwp")))

(define-public crate-execute-command-tokens-0.1.2 (c (n "execute-command-tokens") (v "0.1.2") (h "1vk5qsx767rm2sh0m518h8mnywzp8hvg4rh2ay5ci7x3xqdvvwiy")))

(define-public crate-execute-command-tokens-0.1.3 (c (n "execute-command-tokens") (v "0.1.3") (h "04yzxciy31vs2hrgs986d3ys1bflv308gi6vqpbinj1zdlwaid9r")))

(define-public crate-execute-command-tokens-0.1.4 (c (n "execute-command-tokens") (v "0.1.4") (h "1kkyc52w3ic5plzfbn1jwjfbdqbl1b54l02z4sm0igk02bmq45g3")))

(define-public crate-execute-command-tokens-0.1.5 (c (n "execute-command-tokens") (v "0.1.5") (h "0dfk9pzjznn2vwmrdr2hyh9b16dwvx4g7y0zz5xa5nd2jlil9qdm")))

(define-public crate-execute-command-tokens-0.1.6 (c (n "execute-command-tokens") (v "0.1.6") (h "0njfh6pw9lqp5m29h0sc8v9baaz8p70fk9sawdqq9v3h3i4nk9cb")))

(define-public crate-execute-command-tokens-0.1.7 (c (n "execute-command-tokens") (v "0.1.7") (h "1bdwzw0q983xx4d0wmmyzwhcn0qpl8xcl826cx27z5xynqg35p39") (r "1.65")))

