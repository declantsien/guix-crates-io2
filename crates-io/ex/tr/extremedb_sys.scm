(define-module (crates-io ex tr extremedb_sys) #:use-module (crates-io))

(define-public crate-extremedb_sys-0.1.0 (c (n "extremedb_sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.54") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 2)) (d (n "quote") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 2)))) (h "0lblasb92hgc9znyn8gd2d37mcm1asgql1hgfzpabxqii3hwa0n5") (f (quote (("sql" "sequences") ("sequences") ("rsql" "sql"))))))

(define-public crate-extremedb_sys-0.1.1 (c (n "extremedb_sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.54") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 2)) (d (n "quote") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 2)))) (h "00s3ifyybbrfdszsgw8kc3mpxx57y20b98sxyfssijih0094wqy1") (f (quote (("sql" "sequences") ("sequences") ("rsql" "sql"))))))

(define-public crate-extremedb_sys-0.1.2 (c (n "extremedb_sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.54") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 2)) (d (n "quote") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 2)))) (h "0m5spgbvw003r98yknpk9xy163h2qhvlxdm84sih0802yv1ipbc5") (f (quote (("sql" "sequences") ("sequences") ("rsql" "sql"))))))

