(define-module (crates-io ex tr extra-safe) #:use-module (crates-io))

(define-public crate-extra-safe-0.1.0 (c (n "extra-safe") (v "0.1.0") (d (list (d (n "hybrid-array") (r "^0.1.0") (d #t) (k 0)) (d (n "typenum") (r "^1.16.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.77") (f (quote ("diff"))) (d #t) (k 2)))) (h "1yirdhm8446kn30kv001cwkps32xvrhfg69dwf9j6bamxxi8dlcg")))

(define-public crate-extra-safe-0.1.1 (c (n "extra-safe") (v "0.1.1") (d (list (d (n "hybrid-array") (r "^0.1.0") (d #t) (k 0)) (d (n "typenum") (r "^1.16.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.77") (f (quote ("diff"))) (d #t) (k 2)))) (h "1wy6g7nn1mlncizj8dmc0xs2rpmls2c5czp0syk4w0fgp9qg4yj7")))

