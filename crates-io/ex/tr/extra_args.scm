(define-module (crates-io ex tr extra_args) #:use-module (crates-io))

(define-public crate-extra_args-0.1.0 (c (n "extra_args") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.76") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "08imnp6kmzvlfz35yyy79s2y7gava8dg9z451x9iyx0w9ca7f5lz") (f (quote (("enable"))))))

(define-public crate-extra_args-0.1.1 (c (n "extra_args") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.76") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "0z2c2ywjn8h974vv3jzpvxsid02d455vi22mhh92284jgdh0h4f8") (f (quote (("enable"))))))

(define-public crate-extra_args-0.1.2 (c (n "extra_args") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.76") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1jxmb49g7x9qy5l0sbb6gvnm1k626csrwhk6dqrqx4952q6w7mzd") (f (quote (("enable"))))))

(define-public crate-extra_args-0.1.3 (c (n "extra_args") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.76") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1q3jn79f44ji119ybdn8yil66f5vff5zqvw9mz2ygdipwln6nzk3") (f (quote (("enable"))))))

(define-public crate-extra_args-0.1.4 (c (n "extra_args") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.76") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "0dqqglwqpf870dyvxcp0mnbafn95yipc05iqdcbdk9c66vlqrmva") (f (quote (("enable"))))))

(define-public crate-extra_args-0.1.5 (c (n "extra_args") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.76") (f (quote ("full" "fold" "extra-traits"))) (d #t) (k 0)))) (h "17ivvjhddyk9rbvrz2a934ld0fvqmgl9qvi8z7rwak3cx8caddf9") (f (quote (("enable"))))))

(define-public crate-extra_args-0.1.6 (c (n "extra_args") (v "0.1.6") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.76") (f (quote ("full" "fold" "extra-traits"))) (d #t) (k 0)))) (h "10mll0w1vvbi7kd83gdx21fcahqa5v1sygmfwl58vqlw0ajwmcy6") (f (quote (("enable"))))))

(define-public crate-extra_args-0.1.61 (c (n "extra_args") (v "0.1.61") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.76") (f (quote ("full" "fold" "extra-traits"))) (d #t) (k 0)))) (h "06r0vvhi3jr8jdr0ka0nyi9mfdbgzs2jv2hqghqn7qd6dpg2nbdx") (f (quote (("enable"))))))

(define-public crate-extra_args-0.1.62 (c (n "extra_args") (v "0.1.62") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.76") (f (quote ("full" "fold" "extra-traits"))) (d #t) (k 0)))) (h "1vcphjk0qsgx4f1sq3j69jdlfjf8gm8pzq59ywrn4k26y02zlks8") (f (quote (("enable"))))))

(define-public crate-extra_args-0.1.63 (c (n "extra_args") (v "0.1.63") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.76") (f (quote ("full" "fold" "extra-traits"))) (d #t) (k 0)))) (h "1fyq9p6wjkdr4mja6iq0hvabywvq1932h54na2cj68mdky0hal2c") (f (quote (("enable"))))))

(define-public crate-extra_args-0.1.64 (c (n "extra_args") (v "0.1.64") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.76") (f (quote ("full" "fold" "extra-traits"))) (d #t) (k 0)))) (h "0661k0wshb31sl0hw0ik29y22rp3ycgkfpcb72fqa7sly5cgbjw5") (f (quote (("enable"))))))

(define-public crate-extra_args-0.1.65 (c (n "extra_args") (v "0.1.65") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.76") (f (quote ("full" "fold" "extra-traits"))) (d #t) (k 0)))) (h "1d2pfw692lnal4rphw2wc24gjflv6k8kfcspppk8pni7yy1chl3b") (f (quote (("enable"))))))

(define-public crate-extra_args-0.1.66 (c (n "extra_args") (v "0.1.66") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.76") (f (quote ("full" "fold" "extra-traits"))) (d #t) (k 0)))) (h "1y4pll752xmbli1fh3rg0dy63s3zc5w6vmanns9z83lip0iwsv3l") (f (quote (("enable"))))))

(define-public crate-extra_args-0.1.67 (c (n "extra_args") (v "0.1.67") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.76") (f (quote ("full" "fold" "extra-traits"))) (d #t) (k 0)))) (h "13zaf9921h8mywaihqnqwk9hv6fzpfzn5b9vlwsbcz2bf9khd2d0") (f (quote (("enable"))))))

(define-public crate-extra_args-0.1.68 (c (n "extra_args") (v "0.1.68") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.76") (f (quote ("full" "fold" "extra-traits"))) (d #t) (k 0)))) (h "1jndziy7224511a61vzp93wmzkw59bfrfdfza5001s9f73sx9av3") (f (quote (("enable"))))))

(define-public crate-extra_args-0.1.69 (c (n "extra_args") (v "0.1.69") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.76") (f (quote ("full" "fold" "extra-traits"))) (d #t) (k 0)))) (h "01gcrni3gyiyp9kj3zqcjcydi6xh4mwfsliik06byk2hnbz53869") (f (quote (("enable"))))))

(define-public crate-extra_args-0.1.7 (c (n "extra_args") (v "0.1.7") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.76") (f (quote ("full" "fold" "extra-traits"))) (d #t) (k 0)))) (h "0p94bkyn8kha851822i6k06m5ml5pakcbg6qm41yhc9bqzz1mzd7") (f (quote (("enable"))))))

(define-public crate-extra_args-0.1.71 (c (n "extra_args") (v "0.1.71") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.76") (f (quote ("full" "fold" "extra-traits"))) (d #t) (k 0)))) (h "1hm590jcr12ar8d3q4r3dbfc8bxdf3w28zc2kdmldhc4wh3jwl8y") (f (quote (("enable"))))))

(define-public crate-extra_args-0.1.72 (c (n "extra_args") (v "0.1.72") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.76") (f (quote ("full" "fold" "extra-traits"))) (d #t) (k 0)))) (h "046k679v7xjjqv0xh6iaqymmjgy1p8k81138y5v479n7bpmm3cqi") (f (quote (("enable"))))))

