(define-module (crates-io ex tr extract_rust_hdl_interface) #:use-module (crates-io))

(define-public crate-extract_rust_hdl_interface-0.1.0 (c (n "extract_rust_hdl_interface") (v "0.1.0") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "sv-parser") (r "^0.13.0") (d #t) (k 0)))) (h "1xvm4vldqi7yz08xk7s8c8b5gg2hvy6p7wpkq8l24i3ri1dxj2yl")))

(define-public crate-extract_rust_hdl_interface-0.2.0 (c (n "extract_rust_hdl_interface") (v "0.2.0") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "sv-parser") (r "^0.13.1") (d #t) (k 0)))) (h "023b86wgaw97q4gfn4zl6ndh5zzxrqp2h6dm3i2i7b5y36a78nwl")))

