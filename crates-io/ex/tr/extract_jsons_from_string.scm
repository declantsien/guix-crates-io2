(define-module (crates-io ex tr extract_jsons_from_string) #:use-module (crates-io))

(define-public crate-extract_jsons_from_string-0.1.0 (c (n "extract_jsons_from_string") (v "0.1.0") (h "029asjvw3wyhq4jcqb24jw4li1nc5xdzzkq1gv509ksgmj6p65fc")))

(define-public crate-extract_jsons_from_string-0.1.1 (c (n "extract_jsons_from_string") (v "0.1.1") (h "08d6nkdq73v8gxqb87mr7200g2rsbl00vzjdr7lkh2x41h0l728p")))

(define-public crate-extract_jsons_from_string-0.1.2 (c (n "extract_jsons_from_string") (v "0.1.2") (h "09p9f9s6dfj0p12cp0g0r68d77lw1h80sgcy72361xvanvib1chw")))

(define-public crate-extract_jsons_from_string-0.1.3 (c (n "extract_jsons_from_string") (v "0.1.3") (h "0xdcf5kamp4kl5s6hyj4fnlc8hvpfbjw0bw2a4b4jajbyjph3k8x")))

