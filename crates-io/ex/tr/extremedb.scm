(define-module (crates-io ex tr extremedb) #:use-module (crates-io))

(define-public crate-extremedb-0.1.0 (c (n "extremedb") (v "0.1.0") (d (list (d (n "extremedb_sys") (r "^0.1.0") (d #t) (k 0)))) (h "085g19k592431qjqwscd8k7mlwxw8ygr7dkkqah1wiaq1ipv21di") (f (quote (("sql" "extremedb_sys/sql") ("sequences" "extremedb_sys/sequences") ("rsql" "sql" "extremedb_sys/rsql"))))))

(define-public crate-extremedb-0.1.1 (c (n "extremedb") (v "0.1.1") (d (list (d (n "extremedb_sys") (r "^0.1.1") (d #t) (k 0)))) (h "02nlxfv570m3mmgwvpd0awgfa5rc1yh6vzpdi8nxdmzccsajnf7v") (f (quote (("sql" "extremedb_sys/sql") ("sequences" "extremedb_sys/sequences") ("rsql" "sql" "extremedb_sys/rsql"))))))

(define-public crate-extremedb-0.1.2 (c (n "extremedb") (v "0.1.2") (d (list (d (n "extremedb_sys") (r "^0.1.1") (d #t) (k 0)))) (h "19sbr01ld863ir3dz8ky9j3dblkr3bxn7a7mpyqfhm45grynppd3") (f (quote (("sql" "extremedb_sys/sql") ("sequences" "extremedb_sys/sequences") ("rsql" "sql" "extremedb_sys/rsql"))))))

