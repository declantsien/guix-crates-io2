(define-module (crates-io ex tr extra_pointers) #:use-module (crates-io))

(define-public crate-extra_pointers-0.1.0 (c (n "extra_pointers") (v "0.1.0") (h "12i3s992j4xxwz1k7qyyv1f5yq86fqxvgh27phbkj0ira41wblbg")))

(define-public crate-extra_pointers-0.1.1 (c (n "extra_pointers") (v "0.1.1") (h "04qv1avpsv8kvmg48ayzwvz6d2h91l55ral3hsk9jcfvrna152rg")))

(define-public crate-extra_pointers-0.1.2 (c (n "extra_pointers") (v "0.1.2") (h "0bfg4wwnaijdp93vjr81pjpk163c80iigjn625v282i92j8irw49")))

(define-public crate-extra_pointers-0.1.3 (c (n "extra_pointers") (v "0.1.3") (h "0kz9zzs4lal77sh5illw6zpi6268p9gmd2j3nmn1gi6b3kk0xd24")))

(define-public crate-extra_pointers-0.1.4 (c (n "extra_pointers") (v "0.1.4") (h "0zssrfxqrwhfy4fr3qwn2c9drfmqdx91r06ibm2ig36qp2idv3wd")))

