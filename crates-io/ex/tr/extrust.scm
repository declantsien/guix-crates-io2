(define-module (crates-io ex tr extrust) #:use-module (crates-io))

(define-public crate-extrust-0.1.0 (c (n "extrust") (v "0.1.0") (h "03ia9m4a3pdxa0wp9pkxzbgksayyns9avii180abprsbwv3fv68x")))

(define-public crate-extrust-0.2.0 (c (n "extrust") (v "0.2.0") (h "1d7jabnxqky0mkibm4hk1nwk3j7lhpyfhh3wj8rilpwwpn382p4j")))

(define-public crate-extrust-0.3.0 (c (n "extrust") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.53") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0scivh08g907pdy7f879vqpv8yl4kw4s10l2zyc1li79896iz53w")))

(define-public crate-extrust-0.4.0 (c (n "extrust") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.53") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "072a6wnhx21i94mkyhm5bv53h83d0ihvlzvavrw4s046b7k33rw4")))

(define-public crate-extrust-0.5.0 (c (n "extrust") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0.53") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0q44nd5qfqx1py901qq9p98crfd367yyn4ql2j5v6bx9fx1pfi3y")))

(define-public crate-extrust-0.6.0 (c (n "extrust") (v "0.6.0") (d (list (d (n "anyhow") (r "^1.0.53") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1y0gm8bz9ih87dhcbjnihcsbn1yxw1sxxmdmqblc2k9x447m78sf")))

(define-public crate-extrust-0.6.1 (c (n "extrust") (v "0.6.1") (d (list (d (n "anyhow") (r "^1.0.53") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0bg86gz9alb45g6pxf1vnsipixwr02db18v3awq508njf814cr7m")))

(define-public crate-extrust-0.6.2 (c (n "extrust") (v "0.6.2") (d (list (d (n "anyhow") (r "^1.0.53") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0bbqk1jsjmmba9d1sx8px4p30kgh0gxdnfh0gyp7r7s5iaihg63l")))

(define-public crate-extrust-0.6.3 (c (n "extrust") (v "0.6.3") (d (list (d (n "anyhow") (r "^1.0.53") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "11qs44jyr85vgz1sl9w3p3xlz6qvdh8dqppdsl78mpp352waky5f")))

(define-public crate-extrust-0.7.0 (c (n "extrust") (v "0.7.0") (d (list (d (n "anyhow") (r "^1.0.53") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1jz2a0f412kp4fh0xzvq891cj6s9pnxb3gqsi35zq4vjvvfi57yc")))

(define-public crate-extrust-0.8.0 (c (n "extrust") (v "0.8.0") (d (list (d (n "anyhow") (r "^1.0.53") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1x094kwzxiyysvsnb52y2npvmi1fby897mjpg6zkwqinwrdd364z")))

(define-public crate-extrust-0.9.0 (c (n "extrust") (v "0.9.0") (d (list (d (n "anyhow") (r "^1.0.53") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0n3f3k7hd3x6mma79mydhry4dvk3hsga6adky3p0mvx3p16zyb3f")))

(define-public crate-extrust-0.9.1 (c (n "extrust") (v "0.9.1") (d (list (d (n "anyhow") (r "^1.0.53") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0xvzwgjclca7zr7vw8288kwv8vkkpkljv3czf6v2qcyb8i3g7fr6")))

(define-public crate-extrust-0.10.0 (c (n "extrust") (v "0.10.0") (d (list (d (n "anyhow") (r "^1.0.53") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0q208h3h9irp7r5zdhmajncv28f022i2niazkl2mwm8sgbvlmkwq")))

(define-public crate-extrust-0.10.1 (c (n "extrust") (v "0.10.1") (d (list (d (n "anyhow") (r "^1.0.53") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1hc8rpyhcnpxdxpqs8sz3z1rwy9r8ymkma10g968q2yjz4j6imbj")))

