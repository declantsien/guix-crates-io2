(define-module (crates-io ex tr extra-default) #:use-module (crates-io))

(define-public crate-extra-default-0.1.0 (c (n "extra-default") (v "0.1.0") (d (list (d (n "clippy") (r "^0.0") (d #t) (k 2)))) (h "04jbk9bz5q89fqjjylxn50sxz74l2dvfwm08fbfgf9wz8ksh6y6v")))

(define-public crate-extra-default-0.2.0 (c (n "extra-default") (v "0.2.0") (h "0mldjb93x45r7sjsf4lv6kjg5s42ppnva5kca52ip8hbjbhk856a") (f (quote (("std" "collections") ("default" "std") ("collections" "alloc") ("alloc"))))))

