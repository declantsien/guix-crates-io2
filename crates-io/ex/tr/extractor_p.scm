(define-module (crates-io ex tr extractor_p) #:use-module (crates-io))

(define-public crate-extractor_p-0.1.0 (c (n "extractor_p") (v "0.1.0") (h "1fwkbkjrzry2j0j4knfvlr4rgz8n5acs9jx6cibq8mzm0p908sja")))

(define-public crate-extractor_p-0.1.1 (c (n "extractor_p") (v "0.1.1") (h "0jmypyjjyy250rbqqzjkvs0l2bvhk7h3rx3p1m5s2l4ympa47zid")))

(define-public crate-extractor_p-0.1.2 (c (n "extractor_p") (v "0.1.2") (h "1mz9357aks5qf7zy4js25isrsiymcvnsfnzwbqb629s0c3cqsxpr")))

(define-public crate-extractor_p-0.1.3 (c (n "extractor_p") (v "0.1.3") (h "1xsa0y8rhgzsdlfb758aqmn85crxql978jmjb1lslaycc9cbapdv")))

