(define-module (crates-io ex tr extract-repo-url) #:use-module (crates-io))

(define-public crate-extract-repo-url-1.0.0 (c (n "extract-repo-url") (v "1.0.0") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "clipboard") (r "^0.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1") (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 0)))) (h "094mzxmix9l0pz5ndc4pja7dvpymgpy6yn27yrnagkrjnkp2d62p")))

(define-public crate-extract-repo-url-1.0.1 (c (n "extract-repo-url") (v "1.0.1") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "clipboard") (r "^0.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1") (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 0)))) (h "0hx3ywa74pwh4xx35hvvmg26aj2migwhci48gc9sv5jf9laswqrr")))

