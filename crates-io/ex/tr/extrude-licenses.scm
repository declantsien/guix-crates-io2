(define-module (crates-io ex tr extrude-licenses) #:use-module (crates-io))

(define-public crate-extrude-licenses-1.0.0 (c (n "extrude-licenses") (v "1.0.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "serde") (r "^1.0.116") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.116") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.58") (d #t) (k 0)))) (h "1d67c2rjjrd46s6sjz368w5lxgnnqjzzz6vcvw0c7kxdlkds7nhh")))

