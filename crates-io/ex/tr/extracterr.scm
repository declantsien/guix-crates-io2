(define-module (crates-io ex tr extracterr) #:use-module (crates-io))

(define-public crate-extracterr-0.1.0 (c (n "extracterr") (v "0.1.0") (d (list (d (n "backtrace") (r "^0.3.48") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.18") (d #t) (k 2)))) (h "0ggbjaiy23d7zj0z5mj9z2q43pqijpq5qw2ia38rn4d6b8b9bwc3")))

(define-public crate-extracterr-0.1.1 (c (n "extracterr") (v "0.1.1") (d (list (d (n "backtrace") (r "^0.3.48") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.18") (d #t) (k 2)))) (h "1mnxpzjh96mq8vyv2zgrr155nbhk09zfmx4nfk2rn79qvz8b9cc0")))

