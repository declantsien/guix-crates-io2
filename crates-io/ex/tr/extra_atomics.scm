(define-module (crates-io ex tr extra_atomics) #:use-module (crates-io))

(define-public crate-extra_atomics-0.1.0 (c (n "extra_atomics") (v "0.1.0") (h "1z3zvl811c6im9lkpw4lrhb0fsdg34ldyk0ina9iy5h3pgch3m71")))

(define-public crate-extra_atomics-0.1.1 (c (n "extra_atomics") (v "0.1.1") (h "19gakw744v34x22ry2340apv1l33sh0bp1v5jqbd7bc8mhqqh0nb")))

