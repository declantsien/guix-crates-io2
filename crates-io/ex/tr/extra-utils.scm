(define-module (crates-io ex tr extra-utils) #:use-module (crates-io))

(define-public crate-extra-utils-0.1.0 (c (n "extra-utils") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)))) (h "10pb3mzcfl426vl9rjf6q0mi0g0srlvsnfdzm84vk7m58glji68f")))

(define-public crate-extra-utils-0.2.0 (c (n "extra-utils") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)))) (h "12x50la19f4w5sd4dwpn6j7fp7wdvjj0yaqmji0apgwww7x53a3s")))

(define-public crate-extra-utils-0.3.0 (c (n "extra-utils") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)))) (h "1j8pgks9lg7s4f2f8dlp889zfv67blbgxhdnaxlcacjn3dmd6c7f")))

(define-public crate-extra-utils-0.4.0 (c (n "extra-utils") (v "0.4.0") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)))) (h "1s7alsx75z5rccjc1idp286zvpza0qxgkzazhns3p0l1c4n1mlik")))

(define-public crate-extra-utils-0.4.1 (c (n "extra-utils") (v "0.4.1") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)))) (h "1xbiiisswvz8x60vhrwaf0m7nc7n826wfkahqwfwagivvnbfjdyx")))

(define-public crate-extra-utils-0.5.0 (c (n "extra-utils") (v "0.5.0") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "futures") (r "^0.1.28") (d #t) (k 0)) (d (n "hyper") (r "^0.12.33") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0.1") (d #t) (k 0)))) (h "01a2b918r3b03la8n10jv5r5gd6ll7sk9pl30i62ampw57i78hrf")))

(define-public crate-extra-utils-0.5.1 (c (n "extra-utils") (v "0.5.1") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "futures") (r "^0.1.28") (d #t) (k 0)) (d (n "hyper") (r "^0.12.33") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0.1") (d #t) (k 0)))) (h "16mcpg8bxw9bcfrqxjr3g5mlqm91360103k33qrba67shfmchyiw")))

(define-public crate-extra-utils-0.5.2 (c (n "extra-utils") (v "0.5.2") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "futures") (r "^0.1.28") (d #t) (k 0)) (d (n "hyper") (r "^0.12.33") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0.1") (d #t) (k 0)))) (h "1amk4lzyv7b2dszzq99kypijz2fwgf6mzxanz8m73fblxi13xhqw")))

(define-public crate-extra-utils-1.0.0 (c (n "extra-utils") (v "1.0.0") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "futures") (r "^0.1.28") (d #t) (k 0)) (d (n "hyper") (r "^0.12.33") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0.1") (d #t) (k 0)))) (h "0l6ky6kp4ri27mj0wqpyib0zjgr0566p7jy713vkn44hqc8m50jh")))

