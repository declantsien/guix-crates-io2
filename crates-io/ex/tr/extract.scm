(define-module (crates-io ex tr extract) #:use-module (crates-io))

(define-public crate-extract-0.1.0 (c (n "extract") (v "0.1.0") (d (list (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)) (d (n "regex") (r "^0.2.1") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.5") (d #t) (k 2)))) (h "0zb8j22iaq413a3q21f1n9q5hy0rqz6ychnqagvz06p03sjyxi07")))

(define-public crate-extract-0.1.1 (c (n "extract") (v "0.1.1") (d (list (d (n "clap") (r "^2.20.5") (d #t) (k 0)) (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)) (d (n "regex") (r "^0.2.1") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.5") (d #t) (k 2)))) (h "09grwdndhivhz3f37haj1759jy0z47xblzjaqc2gr4pfykf5fz7z")))

