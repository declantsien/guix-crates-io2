(define-module (crates-io ex tr extract_map) #:use-module (crates-io))

(define-public crate-extract_map-0.1.0 (c (n "extract_map") (v "0.1.0") (d (list (d (n "gat-lending-iterator") (r "^0.1.5") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1f8jagn6rwq7yar6j7fcc5qn3myim8z6mr5sb9xbhrpngl6559iy") (s 2) (e (quote (("serde" "dep:serde") ("iter_mut" "dep:gat-lending-iterator"))))))

(define-public crate-extract_map-0.1.1 (c (n "extract_map") (v "0.1.1") (d (list (d (n "gat-lending-iterator") (r "^0.1.5") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.115") (d #t) (k 2)))) (h "14hapkmlv6p4rwx9nscgnynd1jxal4nm65ipr0wcz1jrv3jh1zz0") (s 2) (e (quote (("serde" "dep:serde") ("iter_mut" "dep:gat-lending-iterator")))) (r "1.70")))

(define-public crate-extract_map-0.1.2 (c (n "extract_map") (v "0.1.2") (d (list (d (n "gat-lending-iterator") (r "^0.1.5") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.115") (d #t) (k 2)))) (h "0s695n5yzs7wwyvaphkkscr8lfv7h94xsczg49a9qa37nnd51xxb") (s 2) (e (quote (("serde" "dep:serde") ("iter_mut" "dep:gat-lending-iterator")))) (r "1.70")))

