(define-module (crates-io ex tr extract-variant-internal) #:use-module (crates-io))

(define-public crate-extract-variant-internal-0.1.0 (c (n "extract-variant-internal") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit"))) (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits" "visit"))) (d #t) (k 2)))) (h "16b4vklnnqx9rvh0c9xwdbnywgjg5dmcvfn0dx925vgqgjmsk71x")))

(define-public crate-extract-variant-internal-0.2.0 (c (n "extract-variant-internal") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit"))) (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits" "visit"))) (d #t) (k 2)))) (h "1vpdr25kxvz63gmss70i2vs1lxxwab79wgagp2q9f9rxpmfqmpfr")))

