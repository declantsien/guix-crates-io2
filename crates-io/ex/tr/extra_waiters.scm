(define-module (crates-io ex tr extra_waiters) #:use-module (crates-io))

(define-public crate-extra_waiters-1.0.0 (c (n "extra_waiters") (v "1.0.0") (d (list (d (n "parking_lot_core") (r "^0.8.5") (d #t) (k 0)))) (h "0p38x9nrcxc4z0n0y7k7f2ls1k0r1gq605w3vk53ln73vh222308")))

(define-public crate-extra_waiters-1.0.1 (c (n "extra_waiters") (v "1.0.1") (d (list (d (n "parking_lot_core") (r "^0.8.5") (d #t) (k 0)))) (h "1j0qr60daqn20bs3cv59fmrr74idg3chkvv7ajlrkqa50x81kf3y")))

