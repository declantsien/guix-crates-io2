(define-module (crates-io ex tr extractors) #:use-module (crates-io))

(define-public crate-extractors-0.1.0 (c (n "extractors") (v "0.1.0") (h "10rb3xgv7c1fzbn1lspl6m0gpxskls6a88hsn70g7bb7r1mflkf8")))

(define-public crate-extractors-0.2.0 (c (n "extractors") (v "0.2.0") (h "02z3fy5h42za35dj7qvghimz59rn5rpl7rmiw1apjihy20jwmxfv")))

