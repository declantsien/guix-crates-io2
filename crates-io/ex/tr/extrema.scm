(define-module (crates-io ex tr extrema) #:use-module (crates-io))

(define-public crate-extrema-0.1.0 (c (n "extrema") (v "0.1.0") (h "1q0pjwl4ajznv5i2pyblr8nl2dwkbagc8aayl740d9skhjcf0rya")))

(define-public crate-extrema-0.1.1 (c (n "extrema") (v "0.1.1") (h "0v6i032k5j4mm37iqmzh48jzbajpyhvf7sfmvaw1dhlhqb26swb9")))

