(define-module (crates-io ex tr extract-variant) #:use-module (crates-io))

(define-public crate-extract-variant-0.1.0 (c (n "extract-variant") (v "0.1.0") (d (list (d (n "extract-variant-internal") (r "^0.1.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (f (quote ("diff"))) (d #t) (k 2)))) (h "1fgvcm8ab3n0h8i5svjra96z6g2qv4j1wcp7hvbj5r11bws7l7b0") (y #t)))

(define-public crate-extract-variant-0.2.0 (c (n "extract-variant") (v "0.2.0") (d (list (d (n "extract-variant-internal") (r "^0.2.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (f (quote ("diff"))) (d #t) (k 2)))) (h "0cq5pkzkjjipk35rl67lxvzaamcnzrkrp8f8a48lvr54nblydm36") (y #t)))

(define-public crate-extract-variant-0.2.1 (c (n "extract-variant") (v "0.2.1") (d (list (d (n "extract-variant-internal") (r "^0.2.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (f (quote ("diff"))) (d #t) (k 2)))) (h "0110dymjklllvfa6a57ccgjlfqi0viz01l38kp9qw6kliwdiqmqf") (y #t)))

(define-public crate-extract-variant-0.2.2 (c (n "extract-variant") (v "0.2.2") (d (list (d (n "extract-variant-internal") (r "^0.2.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (f (quote ("diff"))) (d #t) (k 2)))) (h "035q7gm98jzjmpb7paiwzvq7hvvngrg4yz7asgij2xp70c52y9cl")))

(define-public crate-extract-variant-0.3.0 (c (n "extract-variant") (v "0.3.0") (d (list (d (n "extract-variant-internal") (r "^0.2.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (f (quote ("diff"))) (d #t) (k 2)))) (h "0m497b9rhvgyhlwj9pakvg6bd20ihggc5pf3q5cb2lc8gl06d959")))

(define-public crate-extract-variant-1.0.0 (c (n "extract-variant") (v "1.0.0") (d (list (d (n "extract-variant-internal") (r "^0.2.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (f (quote ("diff"))) (d #t) (k 2)))) (h "0l62bcjbfv8dgxp7hkih4lg8v6h18wvkbzav9zxv9mh28ikbd1ks")))

