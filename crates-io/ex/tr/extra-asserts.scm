(define-module (crates-io ex tr extra-asserts) #:use-module (crates-io))

(define-public crate-extra-asserts-0.1.0 (c (n "extra-asserts") (v "0.1.0") (h "0mdyia0p0bg9ikv2ap2lykg8psaj2srsc0d9qmsvlvgqxhzqdpa2")))

(define-public crate-extra-asserts-0.1.1 (c (n "extra-asserts") (v "0.1.1") (h "0mv59id24004c9cfs5gks83vjv8w0q01rhndirwkqfkbpqpyz9ia")))

