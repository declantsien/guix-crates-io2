(define-module (crates-io ex tr extreme) #:use-module (crates-io))

(define-public crate-extreme-0.0.0 (c (n "extreme") (v "0.0.0") (h "0l21h2dz91cmjcj69vi18yrgbjkx1s2npfg565bqpx82wxshviij")))

(define-public crate-extreme-1.0.0 (c (n "extreme") (v "1.0.0") (h "1d5r8kvzwb7x8s3idq2jxqs4sx2pf6vhs14fmxq71017r8zd3myr")))

(define-public crate-extreme-1.0.1 (c (n "extreme") (v "1.0.1") (h "0la48cw71ia7xlqbpffd87n9dbd7lbhdl5l20fyg3n3majz7ac4f")))

(define-public crate-extreme-6.6.6 (c (n "extreme") (v "6.6.6") (h "0d7q8qjv1lr992xfn5qq4m47qqqgq6j7k6g6qynw7mywazg6pjhm")))

(define-public crate-extreme-666.666.666 (c (n "extreme") (v "666.666.666") (h "1h2ph5ghzk3hka982l8csfiyydjdibnr24r57qvwxks30q5hrf7w")))

(define-public crate-extreme-666.666.6666 (c (n "extreme") (v "666.666.6666") (h "0sd1jwdrx7ir6v7bwr7a1s6zhi52vg9fmy794mpl4m4rddzmclnl")))

(define-public crate-extreme-666.666.66666 (c (n "extreme") (v "666.666.66666") (h "17mj8fg177mpjl4jmgpprm2dngz47i3k2ikk98yd1gm78sl78ka4")))

(define-public crate-extreme-666.666.666666 (c (n "extreme") (v "666.666.666666") (h "1yrig1ky9a6xpcl5nqnkk9wyis7k7wjzb8bbr4n810apfhq5nq0n")))

