(define-module (crates-io ex tr extract-frontmatter) #:use-module (crates-io))

(define-public crate-extract-frontmatter-0.6.2 (c (n "extract-frontmatter") (v "0.6.2") (d (list (d (n "unicode-segmentation") (r "^1.6.0") (d #t) (k 0)))) (h "1mqq5ac10wlwdyrqb736nhqgkhn0s1a1z89kwyb48drf862l73ji")))

(define-public crate-extract-frontmatter-1.0.0 (c (n "extract-frontmatter") (v "1.0.0") (d (list (d (n "unicode-segmentation") (r "^1.6.0") (d #t) (k 0)))) (h "1mf4g9ppchcqygd9j3h86n6xk938gi48cwlhizrs7mvf2szn7v4y")))

(define-public crate-extract-frontmatter-1.0.1 (c (n "extract-frontmatter") (v "1.0.1") (d (list (d (n "unicode-segmentation") (r "^1.6.0") (d #t) (k 0)))) (h "0p9zkz6i0i1xsg8j869dm2jwcddag215xw926yf0p2dj963q5c6n")))

(define-public crate-extract-frontmatter-1.0.2 (c (n "extract-frontmatter") (v "1.0.2") (d (list (d (n "unicode-segmentation") (r "^1.6.0") (d #t) (k 0)))) (h "0srb2nhzqpk1ny2clgddqf68qrn7p37pfwjkicpdhghj4gkvl952")))

(define-public crate-extract-frontmatter-1.0.3 (c (n "extract-frontmatter") (v "1.0.3") (h "1pmw2xjf53bdnscgm5sivx5fj3f896swqcmp9608ccxxysb1jp7c")))

(define-public crate-extract-frontmatter-2.0.0 (c (n "extract-frontmatter") (v "2.0.0") (h "1zas3mbfis6nvd3fwhl1skjfaq885n9pvvxh8k3d1i4igdmdxncm")))

(define-public crate-extract-frontmatter-2.0.3 (c (n "extract-frontmatter") (v "2.0.3") (d (list (d (n "version-sync") (r "^0.9.1") (d #t) (k 2)))) (h "0k786hhiww9ddwqyjrdm9mbsbcqfy8papj7dkajdzf8yzwdbz65b")))

(define-public crate-extract-frontmatter-2.1.0 (c (n "extract-frontmatter") (v "2.1.0") (h "0mlknfhl9wxxsndnixfvqija3jaha1j6grdyr5inczaz2ancdd1h")))

(define-public crate-extract-frontmatter-3.0.0 (c (n "extract-frontmatter") (v "3.0.0") (h "0whigc7kq7gigv7xy4f9p68h7qqj4bfdw9gb216llrg1waa3kb11")))

(define-public crate-extract-frontmatter-3.0.1 (c (n "extract-frontmatter") (v "3.0.1") (h "0vkw1gk0sxb4dfs2qsfyf61ddnsys620ids8n28aycv0k6fm2dld")))

(define-public crate-extract-frontmatter-3.0.2 (c (n "extract-frontmatter") (v "3.0.2") (h "1npyxrbvafc6y3lx4v7sz0rg80izgdz16zzhwgk6438vdh5ya0w0")))

(define-public crate-extract-frontmatter-4.0.0 (c (n "extract-frontmatter") (v "4.0.0") (h "0n6k2lpaw813psai96gvkgjjv2jycf1khdfh6wyr2zxch7rpkpx8")))

(define-public crate-extract-frontmatter-4.1.0 (c (n "extract-frontmatter") (v "4.1.0") (h "1bh3d9p1b6nkc0rfmic8lslk2rrmcmhp8w2w0p9lg0aj4jsqiva6")))

(define-public crate-extract-frontmatter-4.1.1 (c (n "extract-frontmatter") (v "4.1.1") (h "1cpfqsx3lsbx17b9gppyjbmny66d2d01nj7093igs36xiyyj6v22") (r "1.58.1")))

