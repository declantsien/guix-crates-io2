(define-module (crates-io ex tr extract-words) #:use-module (crates-io))

(define-public crate-extract-words-0.1.0 (c (n "extract-words") (v "0.1.0") (d (list (d (n "rusty-hook") (r "^0.11") (d #t) (k 2)))) (h "0vg6038wychy6b28gdgf42k0vdh46m5144kqyh0hbpz8nhvw0jzw")))

(define-public crate-extract-words-0.2.0 (c (n "extract-words") (v "0.2.0") (d (list (d (n "rusty-hook") (r "^0.11") (d #t) (k 2)))) (h "0flj7k4j0yccx69ka4xvnw2iwfsiy5g3giglfcm4l8lck9mrqwxh")))

