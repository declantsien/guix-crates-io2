(define-module (crates-io ex cl exclude_from_backups) #:use-module (crates-io))

(define-public crate-exclude_from_backups-0.1.0 (c (n "exclude_from_backups") (v "0.1.0") (d (list (d (n "objc") (r "^0.2.2") (d #t) (t "cfg(unix)") (k 0)))) (h "1kiivvvd435ij29fjd664y7d6nbsc2ql4s2pq3fcz6whmrqji3zg")))

(define-public crate-exclude_from_backups-0.1.1 (c (n "exclude_from_backups") (v "0.1.1") (d (list (d (n "objc") (r "^0.2.2") (d #t) (t "cfg(unix)") (k 0)))) (h "05s0hp2pmif0aiwlyk3hdvcj7pwjqcps3q02dz6svnkp8fqnb42h")))

(define-public crate-exclude_from_backups-0.2.0 (c (n "exclude_from_backups") (v "0.2.0") (d (list (d (n "core-foundation") (r "= 0.4.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "core-foundation-sys") (r "= 0.4.2") (d #t) (t "cfg(unix)") (k 0)))) (h "0fc1jcjrzbm1gjw6dsms4v00370nmjrm1k8c3l1dl4lx5ng96y1d")))

(define-public crate-exclude_from_backups-0.2.1 (c (n "exclude_from_backups") (v "0.2.1") (d (list (d (n "core-foundation") (r "^0.4.3") (f (quote ("mac_os_10_8_features"))) (d #t) (t "cfg(unix)") (k 0)))) (h "1k90rfn725fv7nxpvghck4z65dnzpv1i19ym3yb9cp1vnpyr7yps")))

(define-public crate-exclude_from_backups-1.0.0 (c (n "exclude_from_backups") (v "1.0.0") (d (list (d (n "core-foundation") (r "^0.6") (f (quote ("mac_os_10_8_features"))) (d #t) (t "cfg(target_os = \"macos\")") (k 0)))) (h "1s47apfs679ddphkx3q2b72pfj2ffnjkjghhykkx4y6apz94l63k")))

(define-public crate-exclude_from_backups-1.0.1 (c (n "exclude_from_backups") (v "1.0.1") (d (list (d (n "core-foundation") (r "^0.7.0") (f (quote ("mac_os_10_8_features"))) (d #t) (t "cfg(target_os = \"macos\")") (k 0)))) (h "1j4syc72wcxsvbnccc8l1hhqw5f28zbjh1gifbrmzw191fqss5qb")))

(define-public crate-exclude_from_backups-1.0.2 (c (n "exclude_from_backups") (v "1.0.2") (d (list (d (n "core-foundation") (r "^0.9.1") (f (quote ("mac_os_10_8_features"))) (d #t) (t "cfg(target_os = \"macos\")") (k 0)))) (h "0ykf5gapz0q97crxr8i5zfd2fg2rb4af206ba3mycnkggwi7pq2w")))

