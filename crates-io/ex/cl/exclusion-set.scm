(define-module (crates-io ex cl exclusion-set) #:use-module (crates-io))

(define-public crate-exclusion-set-0.1.0 (c (n "exclusion-set") (v "0.1.0") (d (list (d (n "loom") (r "^0.5.6") (d #t) (t "cfg(loom)") (k 0)))) (h "13nhwgsmyn54pwh2142jjv5f9qwmrfbgm81npr1xp71ja9z8vj6f") (f (quote (("std") ("default"))))))

(define-public crate-exclusion-set-0.1.1 (c (n "exclusion-set") (v "0.1.1") (d (list (d (n "loom") (r "^0.5.6") (d #t) (t "cfg(loom)") (k 0)))) (h "1q5b4xq3wd1050ly5cdwkpx7iwsnyyxs3a7pkm23fh4bvnzykvdc") (f (quote (("std") ("default"))))))

(define-public crate-exclusion-set-0.1.2 (c (n "exclusion-set") (v "0.1.2") (d (list (d (n "loom") (r "^0.5.6") (d #t) (t "cfg(loom)") (k 0)))) (h "1l3j1fyy48pmzc3k4nrzjkcxchk9ar0nl4cd963ma96piv20hdww") (f (quote (("std") ("default"))))))

