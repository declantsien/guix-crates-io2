(define-module (crates-io ex cl exclusion-merkle-cbt) #:use-module (crates-io))

(define-public crate-exclusion-merkle-cbt-0.1.0 (c (n "exclusion-merkle-cbt") (v "0.1.0") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "merkle-cbt") (r "^0.3.0") (d #t) (k 0)))) (h "08bwd4ysdcwgznxg4ppwz9aa54s9s4vfnmw9bigwswqd28k47n0p") (f (quote (("std") ("default" "std"))))))

(define-public crate-exclusion-merkle-cbt-0.2.0 (c (n "exclusion-merkle-cbt") (v "0.2.0") (d (list (d (n "blake2b-rs") (r "^0.1") (d #t) (k 2)) (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "merkle-cbt") (r "^0.3.0") (d #t) (k 0)))) (h "0wr8dn8ihdcqgiymbgrh1ahdjrc4fk626kalfjk91war542vqcxh") (f (quote (("std") ("default" "std"))))))

(define-public crate-exclusion-merkle-cbt-0.2.1 (c (n "exclusion-merkle-cbt") (v "0.2.1") (d (list (d (n "blake2b-rs") (r "^0.1") (d #t) (k 2)) (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "merkle-cbt") (r "^0.3.0") (d #t) (k 0)))) (h "03d93c62pkqh0ngynh3zhwhxnjyy7ajqds30f03xi9j31zj48wbm") (f (quote (("std") ("default" "std"))))))

(define-public crate-exclusion-merkle-cbt-0.3.1 (c (n "exclusion-merkle-cbt") (v "0.3.1") (d (list (d (n "blake2b-rs") (r "^0.1") (d #t) (k 2)) (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "merkle-cbt") (r "^0.3.0") (d #t) (k 0)))) (h "1w3kgc1v4ljb9d6fkfp99j7axn9vf1k66ym78kw8l8ry92mfqzqy") (f (quote (("std") ("default" "std"))))))

(define-public crate-exclusion-merkle-cbt-0.3.2 (c (n "exclusion-merkle-cbt") (v "0.3.2") (d (list (d (n "blake2b-rs") (r "^0.1") (d #t) (k 2)) (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "merkle-cbt") (r "^0.3.0") (d #t) (k 0)))) (h "1dzhjkrl8g6cqkqh0sjc639vs908jhf4idyi489jf9ygipa98j9d") (f (quote (("std") ("default" "std"))))))

(define-public crate-exclusion-merkle-cbt-0.3.3 (c (n "exclusion-merkle-cbt") (v "0.3.3") (d (list (d (n "blake2b-rs") (r "^0.1") (d #t) (k 2)) (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "merkle-cbt") (r "^0.3.0") (d #t) (k 0)))) (h "0ar9i8rxq3586fkiiqf21ddpjixqihq4d6k77a4yld4c830vz58a") (f (quote (("std") ("default" "std"))))))

