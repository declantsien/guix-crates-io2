(define-module (crates-io ex t_ ext_format) #:use-module (crates-io))

(define-public crate-ext_format-0.1.0 (c (n "ext_format") (v "0.1.0") (d (list (d (n "litrs") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.32") (d #t) (k 0)))) (h "0zhj4msmb6x4m016lsry2gd7gc270f1l879ic07zk1vc5qndv7na")))

(define-public crate-ext_format-0.1.1 (c (n "ext_format") (v "0.1.1") (d (list (d (n "litrs") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.32") (d #t) (k 0)))) (h "13xygwnrrc7pc0v390lalvzjp4l7wn415w0hzcc7pw4wn5j3xjig")))

