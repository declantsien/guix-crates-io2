(define-module (crates-io ex t_ ext_count) #:use-module (crates-io))

(define-public crate-ext_count-1.0.0 (c (n "ext_count") (v "1.0.0") (d (list (d (n "clap") (r "^4.4.6") (d #t) (k 0)) (d (n "incr_stats") (r "^1.0.1") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.10.0") (d #t) (k 0)) (d (n "tree_magic") (r "^0.2.3") (d #t) (k 0)))) (h "1fziz6msjv9rga4yqkjd3lj40p0h11nws6jmg5783b5ans3r4qal")))

(define-public crate-ext_count-1.0.1 (c (n "ext_count") (v "1.0.1") (d (list (d (n "clap") (r "^4.4.6") (d #t) (k 0)) (d (n "incr_stats") (r "^1.0.1") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.10.0") (d #t) (k 0)) (d (n "tree_magic") (r "^0.2.3") (d #t) (k 0)))) (h "0v4ch1yg28502mvrb66dzii18q5wijr0p02b0a8kkjqfmwmd8xp4")))

