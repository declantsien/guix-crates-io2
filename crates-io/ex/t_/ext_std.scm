(define-module (crates-io ex t_ ext_std) #:use-module (crates-io))

(define-public crate-ext_std-0.1.0 (c (n "ext_std") (v "0.1.0") (h "17jad3r4j5fc3q96ab4ldsi6nxwgddxhpnpzr8lx84awwq2gr9cz")))

(define-public crate-ext_std-0.2.0 (c (n "ext_std") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.53") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1flsd5zdcn7bn1v670hkkw9z4hp86v4fzfqyy0b5vbfl8v0mvsl5")))

(define-public crate-ext_std-0.3.0 (c (n "ext_std") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.53") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0wly38iwwab5afskfsb7y4hs3q3b670wsvnssf56zj8b4i1np0xj")))

(define-public crate-ext_std-0.4.0 (c (n "ext_std") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.53") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0fyazvfwxgbq8a800iq4yhc0dl1y4jwbvs0c8mpgk8k0z9dcm49x")))

