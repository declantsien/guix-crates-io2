(define-module (crates-io ex il exile) #:use-module (crates-io))

(define-public crate-exile-0.0.0 (c (n "exile") (v "0.0.0") (d (list (d (n "cargo-readme") (r "^3.2.0") (d #t) (k 1)) (d (n "serde_json") (r "^1.0.48") (d #t) (k 2)) (d (n "serde_plain") (r "^0.3.0") (d #t) (k 2)) (d (n "xdoc") (r "^0.0.0") (d #t) (k 0)) (d (n "xtest") (r "^0.0.0") (d #t) (k 1)) (d (n "xtest") (r "^0.0.0") (d #t) (k 2)))) (h "0vs00z94wmsixm9djsr7npbk532pwcz82799cby08yk87z3x24v4")))

(define-public crate-exile-0.0.1 (c (n "exile") (v "0.0.1") (d (list (d (n "cargo-readme") (r "^3.2.0") (d #t) (k 1)) (d (n "serde_json") (r "^1.0.48") (d #t) (k 2)) (d (n "serde_plain") (r "^0.3.0") (d #t) (k 2)) (d (n "xdoc") (r "^0.0.1") (d #t) (k 0)) (d (n "xtest") (r "^0.0.1") (d #t) (k 1)) (d (n "xtest") (r "^0.0.1") (d #t) (k 2)))) (h "0w92n3i20iggf7pmf3zplcxp8434jxic642zibg6d59cpkwlnkhk")))

(define-public crate-exile-0.0.2 (c (n "exile") (v "0.0.2") (d (list (d (n "cargo-readme") (r "^3.2.0") (d #t) (k 1)))) (h "04dhplxdx5bbacz693ybzqyywk0pxjrmnb5n7v2cv3bkkvwdzbba")))

(define-public crate-exile-0.0.3 (c (n "exile") (v "0.0.3") (d (list (d (n "cargo-readme") (r ">=3.2.0, <4.0.0") (d #t) (k 1)))) (h "1gf0sckyx89ipgih5mbzszfhvsiyv8qpmcqlxlgg6s9dhdxxpw08")))

(define-public crate-exile-0.0.4 (c (n "exile") (v "0.0.4") (d (list (d (n "cargo-readme") (r "^3.2.0") (d #t) (k 1)))) (h "0fzrxvyvlqrjki1f1c4vr3gkc199lb75fwgnrmkjsrc6kpi6hjmv") (f (quote (("doctype_wip") ("default"))))))

(define-public crate-exile-0.0.5 (c (n "exile") (v "0.0.5") (d (list (d (n "cargo-readme") (r "^3.2.0") (d #t) (k 1)))) (h "0l2c4z5h3i2wpvw8d0wc8j16mg4mq1j5m50kv6c1yrqb1vpdjpnh") (f (quote (("doctype_wip") ("default"))))))

(define-public crate-exile-0.0.6 (c (n "exile") (v "0.0.6") (d (list (d (n "cargo-readme") (r "^3.2.0") (d #t) (k 1)))) (h "107xhl21jb42v5bsyzw73mlnpkv6sapv9bdjqldsgay9qr69kcnj") (f (quote (("doctype_wip") ("default"))))))

(define-public crate-exile-0.0.7 (c (n "exile") (v "0.0.7") (d (list (d (n "cargo-readme") (r "^3.2.0") (d #t) (k 1)))) (h "1446lpyr0vvqcli6pysc6gqsspa22pbg0mgbk0kwc2xrf0dpzcpl") (f (quote (("doctype_wip") ("default"))))))

(define-public crate-exile-0.0.8 (c (n "exile") (v "0.0.8") (d (list (d (n "cargo-readme") (r "^3.2.0") (d #t) (k 1)))) (h "1n7cd2q6m4d26s75i0a1v8ac63yslx5pny7pg7ck1c3vj19hci0j") (f (quote (("doctype_wip") ("default"))))))

(define-public crate-exile-0.0.9 (c (n "exile") (v "0.0.9") (d (list (d (n "cargo-readme") (r "^3.2.0") (d #t) (k 1)))) (h "1nqj9m82iix80dbaamx6ilp6ry3z0cs0bj7js619abpmlbj3y75d") (f (quote (("doctype_wip") ("default"))))))

