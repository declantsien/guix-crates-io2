(define-module (crates-io ex c- exc-make) #:use-module (crates-io))

(define-public crate-exc-make-0.7.0-alpha.0 (c (n "exc-make") (v "0.7.0-alpha.0") (d (list (d (n "exc-service") (r "^0.7.0-alpha.0") (k 0)) (d (n "exc-types") (r "^0.7.0-alpha.0") (k 0)) (d (n "futures") (r "^0.3.25") (d #t) (k 0)) (d (n "tower-make") (r "^0.3.0") (d #t) (k 0)))) (h "0lc7qf8wsr4mbncs0ycbxpkl2x243191c1cw6c6gf3rv6wvbnzsc") (r "1.70.0")))

(define-public crate-exc-make-0.7.0-alpha.1 (c (n "exc-make") (v "0.7.0-alpha.1") (d (list (d (n "exc-service") (r "^0.7.0-alpha.1") (f (quote ("send"))) (k 0)) (d (n "exc-types") (r "^0.7.0-alpha.1") (k 0)) (d (n "futures") (r "^0.3.25") (d #t) (k 0)) (d (n "tower-make") (r "^0.3.0") (d #t) (k 0)) (d (n "tower-service") (r "^0.3.2") (d #t) (k 0)))) (h "15044rg30zhlrbjz4q5pkiqkfsj45bq4fv7qc0w64dypi3rkggcj") (r "1.70.0")))

(define-public crate-exc-make-0.7.0-beta.0 (c (n "exc-make") (v "0.7.0-beta.0") (d (list (d (n "exc-service") (r "^0.7.0-beta.0") (f (quote ("send"))) (k 0)) (d (n "exc-types") (r "^0.7.0-beta.0") (k 0)) (d (n "futures") (r "^0.3.25") (d #t) (k 0)) (d (n "tower-make") (r "^0.3.0") (d #t) (k 0)) (d (n "tower-service") (r "^0.3.2") (d #t) (k 0)))) (h "11q8v9vq90vs4fzgj6gqbczwjzlw6n38xihw174vdnpib1hbq0c1") (r "1.70.0")))

(define-public crate-exc-make-0.7.0 (c (n "exc-make") (v "0.7.0") (d (list (d (n "exc-service") (r "^0.7.0") (f (quote ("send"))) (k 0)) (d (n "exc-types") (r "^0.7.0") (k 0)) (d (n "futures") (r "^0.3.25") (d #t) (k 0)) (d (n "tower-make") (r "^0.3.0") (d #t) (k 0)) (d (n "tower-service") (r "^0.3.2") (d #t) (k 0)))) (h "192llk54j3i8vaw7y8m5vssdfxl2m6sanm7z61wanjaa7z0in6lh") (r "1.70.0")))

(define-public crate-exc-make-0.7.1 (c (n "exc-make") (v "0.7.1") (d (list (d (n "exc-service") (r "^0.7.1") (f (quote ("send"))) (k 0)) (d (n "exc-types") (r "^0.7.1") (k 0)) (d (n "futures") (r "^0.3.25") (d #t) (k 0)) (d (n "tower-make") (r "^0.3.0") (d #t) (k 0)) (d (n "tower-service") (r "^0.3.2") (d #t) (k 0)))) (h "1kxbig9c6jx3ysaf6cfyi4l0zrxz7whgcy7p6lwvhpq5gpblrcga") (r "1.70.0")))

(define-public crate-exc-make-0.7.2-beta.0 (c (n "exc-make") (v "0.7.2-beta.0") (d (list (d (n "exc-service") (r "^0.7.2-beta.0") (f (quote ("send"))) (k 0)) (d (n "exc-types") (r "^0.7.2-beta.0") (k 0)) (d (n "futures") (r "^0.3.25") (d #t) (k 0)) (d (n "tower-make") (r "^0.3.0") (d #t) (k 0)) (d (n "tower-service") (r "^0.3.2") (d #t) (k 0)))) (h "1ibmmdi5lf1m7dyly5vas6a54ih9z38wnkl42yrq295xrfwbdbkd") (r "1.70.0")))

(define-public crate-exc-make-0.7.2-beta.1 (c (n "exc-make") (v "0.7.2-beta.1") (d (list (d (n "exc-service") (r "^0.7.2-beta.1") (f (quote ("send"))) (k 0)) (d (n "exc-types") (r "^0.7.2-beta.1") (k 0)) (d (n "futures") (r "^0.3.25") (d #t) (k 0)) (d (n "tower-make") (r "^0.3.0") (d #t) (k 0)) (d (n "tower-service") (r "^0.3.2") (d #t) (k 0)))) (h "09sf1wfqzhlkibfcrxk7xblv7rn48pyz9w4sslhahd6slidqzi42") (r "1.70.0")))

(define-public crate-exc-make-0.7.2-beta.2 (c (n "exc-make") (v "0.7.2-beta.2") (d (list (d (n "exc-service") (r "^0.7.2-beta.2") (f (quote ("send"))) (k 0)) (d (n "exc-types") (r "^0.7.2-beta.2") (k 0)) (d (n "futures") (r "^0.3.25") (d #t) (k 0)) (d (n "tower-make") (r "^0.3.0") (d #t) (k 0)) (d (n "tower-service") (r "^0.3.2") (d #t) (k 0)))) (h "10426qfwzsd6c2f4jizpzwjxqph7mjxbhs8lqfpij6if1hbmrw9d") (r "1.70.0")))

(define-public crate-exc-make-0.7.2 (c (n "exc-make") (v "0.7.2") (d (list (d (n "exc-service") (r "^0.7.2") (f (quote ("send"))) (k 0)) (d (n "exc-types") (r "^0.7.2") (k 0)) (d (n "futures") (r "^0.3.25") (d #t) (k 0)) (d (n "tower-make") (r "^0.3.0") (d #t) (k 0)) (d (n "tower-service") (r "^0.3.2") (d #t) (k 0)))) (h "1j3808lzxl11088hp2s3lp4fhhw2c1mwmbfpw2spdgbyw04x1nlc") (r "1.70.0")))

(define-public crate-exc-make-0.7.3-beta.0 (c (n "exc-make") (v "0.7.3-beta.0") (d (list (d (n "exc-service") (r "^0.7.3-beta.0") (f (quote ("send"))) (k 0)) (d (n "exc-types") (r "^0.7.3-beta.0") (k 0)) (d (n "futures") (r "^0.3.25") (d #t) (k 0)) (d (n "tower-make") (r "^0.3.0") (d #t) (k 0)) (d (n "tower-service") (r "^0.3.2") (d #t) (k 0)))) (h "0b750c40zhqzc2j8ssw1k0v78lrgjl68rljb3ld9gmx4ivi4y56s") (r "1.70.0")))

(define-public crate-exc-make-0.7.3 (c (n "exc-make") (v "0.7.3") (d (list (d (n "exc-service") (r "^0.7.3") (f (quote ("send"))) (k 0)) (d (n "exc-types") (r "^0.7.3") (k 0)) (d (n "futures") (r "^0.3.25") (d #t) (k 0)) (d (n "tower-make") (r "^0.3.0") (d #t) (k 0)) (d (n "tower-service") (r "^0.3.2") (d #t) (k 0)))) (h "07aib2lba2m0pkmaqqh9z0n9y5pxnfb20r9xr6s2r4qp12n6h5aj") (r "1.70.0")))

