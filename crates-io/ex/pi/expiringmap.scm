(define-module (crates-io ex pi expiringmap) #:use-module (crates-io))

(define-public crate-expiringmap-0.1.0 (c (n "expiringmap") (v "0.1.0") (d (list (d (n "ahash") (r "^0.8") (d #t) (k 0)))) (h "019hz02a0piv033vg9rm2w4pa5g9pxbb5wnryqdr1crl3vk9qawc")))

(define-public crate-expiringmap-0.1.1 (c (n "expiringmap") (v "0.1.1") (d (list (d (n "ahash") (r "^0.8") (d #t) (k 0)))) (h "0dw7h2sdzxaxk6q8f41jirn8284gs8prz4bk6257rr37pbd1lczx")))

