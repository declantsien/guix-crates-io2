(define-module (crates-io ex pi expire) #:use-module (crates-io))

(define-public crate-expire-0.1.0 (c (n "expire") (v "0.1.0") (h "0qzqaig6vwr1pzrrfhk1l370nbcx9r8czskcaykgs3jn0s08qs2q")))

(define-public crate-expire-0.1.1 (c (n "expire") (v "0.1.1") (h "0bh249m6vxh2v9qh8sp82x589f6s1sbc8vaj5gf54hck5llbjin4")))

(define-public crate-expire-0.1.2 (c (n "expire") (v "0.1.2") (h "0570i4yqmfmzizzbdzhyhldw2q7jxbdi6jhycg6pxn5vrnfl1smm")))

