(define-module (crates-io ex pi expire_map) #:use-module (crates-io))

(define-public crate-expire_map-0.0.1 (c (n "expire_map") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "array-macro") (r "^2.1.5") (d #t) (k 0)) (d (n "async-std") (r "^1.12.0") (f (quote ("unstable"))) (d #t) (k 2)) (d (n "criterion") (r "^0.3.6") (d #t) (k 2)) (d (n "dashmap") (r "^5.3.4") (d #t) (k 0)))) (h "03bizz261yzvb881q9prg3vid8q55kggbxifldk7mysacldahl7b") (f (quote (("retry") ("default" "retry"))))))

(define-public crate-expire_map-0.0.3 (c (n "expire_map") (v "0.0.3") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "array-macro") (r "^2.1.5") (d #t) (k 0)) (d (n "async-std") (r "^1.12.0") (f (quote ("unstable"))) (d #t) (k 2)) (d (n "criterion") (r "^0.3.6") (d #t) (k 2)) (d (n "dashmap") (r "^5.3.4") (d #t) (k 0)))) (h "00kv7vz3a6sjnq5fja477dl2x46agq9w80k41xhhqxn485chj3xd") (f (quote (("retry") ("default" "retry"))))))

(define-public crate-expire_map-0.0.5 (c (n "expire_map") (v "0.0.5") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "array-macro") (r "^2.1.5") (d #t) (k 0)) (d (n "async-std") (r "^1.12.0") (f (quote ("unstable"))) (d #t) (k 2)) (d (n "criterion") (r "^0.3.6") (d #t) (k 2)) (d (n "dashmap") (r "^5.3.4") (d #t) (k 0)))) (h "0c71afai5wxfxkampkr1vcjkxazmgycm8a0n57m2zd3pdp6hjp9m") (f (quote (("retry") ("default" "retry"))))))

(define-public crate-expire_map-0.0.7 (c (n "expire_map") (v "0.0.7") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "array-macro") (r "^2.1.5") (d #t) (k 0)) (d (n "async-std") (r "^1.12.0") (f (quote ("unstable"))) (d #t) (k 2)) (d (n "dashmap") (r "^5.3.4") (d #t) (k 0)))) (h "1vjgnv84wp766cnnj3zfjxxk6i6mz55xavxd4d0ygiydk43i3md3") (f (quote (("retry") ("default" "retry"))))))

(define-public crate-expire_map-0.0.8 (c (n "expire_map") (v "0.0.8") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "array-macro") (r "^2.1.5") (d #t) (k 0)) (d (n "async-std") (r "^1.12.0") (f (quote ("unstable"))) (d #t) (k 2)) (d (n "dashmap") (r "^5.3.4") (d #t) (k 0)))) (h "1v57b7zgvj2ykqb5lin8vfxd1qqrxmidd1y7qwsipzfd2i1k86zg") (f (quote (("retry") ("default" "retry"))))))

(define-public crate-expire_map-0.0.9 (c (n "expire_map") (v "0.0.9") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "array-macro") (r "^2.1.5") (d #t) (k 0)) (d (n "async-std") (r "^1.12.0") (f (quote ("unstable"))) (d #t) (k 2)) (d (n "dashmap") (r "^5.3.4") (d #t) (k 0)))) (h "09146kyqqna026dx074lfgv0hrb90jc8nzl18mbkzgyxb58sxaaz") (f (quote (("retry") ("default" "retry"))))))

(define-public crate-expire_map-0.0.10 (c (n "expire_map") (v "0.0.10") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "array-macro") (r "^2.1.5") (d #t) (k 0)) (d (n "async-std") (r "^1.12.0") (f (quote ("unstable"))) (d #t) (k 2)) (d (n "dashmap") (r "^5.3.4") (d #t) (k 0)))) (h "0k40lrq59lcvaj4331jshv74bnck0b9s1d6lqliryp2dwryil2ih") (f (quote (("retry") ("default" "retry"))))))

(define-public crate-expire_map-0.0.11 (c (n "expire_map") (v "0.0.11") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "array-macro") (r "^2.1.5") (d #t) (k 0)) (d (n "async-std") (r "^1.12.0") (f (quote ("unstable"))) (d #t) (k 2)) (d (n "dashmap") (r "^5.3.4") (d #t) (k 0)))) (h "148k8vdr3y7f8g7j3mcvx3x21lkv9c1c1r1ax3kl9p8l4bisjp3a") (f (quote (("retry") ("default" "retry"))))))

(define-public crate-expire_map-0.0.12 (c (n "expire_map") (v "0.0.12") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "array-macro") (r "^2.1.5") (d #t) (k 0)) (d (n "async-std") (r "^1.12.0") (f (quote ("unstable"))) (d #t) (k 2)) (d (n "dashmap") (r "^5.3.4") (d #t) (k 0)))) (h "1xm884x0748h44kwrmhz4prp26kxr8xgdfg5nscp160bby1l20fc") (f (quote (("retry") ("default" "retry"))))))

(define-public crate-expire_map-0.0.13 (c (n "expire_map") (v "0.0.13") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "array-macro") (r "^2.1.5") (d #t) (k 0)) (d (n "async-std") (r "^1.12.0") (f (quote ("unstable"))) (d #t) (k 2)) (d (n "dashmap") (r "^5.3.4") (d #t) (k 0)))) (h "0k2808g1x0d6vzi7wa0gq3qbvzqhlryapl0r6n17d1fyjlhlypcb") (f (quote (("retry") ("default" "retry"))))))

(define-public crate-expire_map-0.0.14 (c (n "expire_map") (v "0.0.14") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "array-macro") (r "^2.1.5") (d #t) (k 0)) (d (n "async-std") (r "^1.12.0") (f (quote ("unstable"))) (d #t) (k 2)) (d (n "dashmap") (r "^5.3.4") (d #t) (k 0)))) (h "0bq3ah06cvwmnjmincia796r6ps1igmx4csrhkl8gfh41rmfn5z8") (f (quote (("retry") ("default" "retry"))))))

(define-public crate-expire_map-0.0.15 (c (n "expire_map") (v "0.0.15") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "array-macro") (r "^2.1.5") (d #t) (k 0)) (d (n "async-std") (r "^1.12.0") (f (quote ("unstable"))) (d #t) (k 2)) (d (n "dashmap") (r "^5.3.4") (d #t) (k 0)))) (h "1jbm4083xh1krkyx7p2p5prfnrgcr9qydc5xrgakdgdln9n4ngkg") (f (quote (("retry") ("default" "retry"))))))

(define-public crate-expire_map-0.0.17 (c (n "expire_map") (v "0.0.17") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "array-macro") (r "^2.1.5") (d #t) (k 0)) (d (n "async-std") (r "^1.12.0") (f (quote ("unstable"))) (d #t) (k 2)) (d (n "dashmap") (r "^5.3.4") (d #t) (k 0)))) (h "0ickg98nbmlxf6ks6a6riy987v79as9z52qcq74d5hh87769vd27") (f (quote (("retry") ("default" "retry"))))))

(define-public crate-expire_map-0.0.18 (c (n "expire_map") (v "0.0.18") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "array-macro") (r "^2.1.5") (d #t) (k 0)) (d (n "async-std") (r "^1.12.0") (f (quote ("unstable"))) (d #t) (k 2)) (d (n "dashmap") (r "^5.3.4") (d #t) (k 0)))) (h "1g2fnnsbmjx8h6cplvvrvqksizv927z2gq56z4j4rv57lf58l89j") (f (quote (("retry") ("default" "retry"))))))

(define-public crate-expire_map-0.0.19 (c (n "expire_map") (v "0.0.19") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "array-macro") (r "^2.1.5") (d #t) (k 0)) (d (n "async-std") (r "^1.12.0") (f (quote ("unstable"))) (d #t) (k 2)) (d (n "dashmap") (r "^5.3.4") (d #t) (k 0)))) (h "19n1kp481vnr5grhqvrkrajrikpccvnzkdf44shp6idi2pr12ymb") (f (quote (("retry") ("default" "retry"))))))

(define-public crate-expire_map-0.0.20 (c (n "expire_map") (v "0.0.20") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "array-macro") (r "^2.1.5") (d #t) (k 0)) (d (n "async-std") (r "^1.12.0") (f (quote ("unstable"))) (d #t) (k 2)) (d (n "dashmap") (r "^5.3.4") (d #t) (k 0)))) (h "1ag1pk1d7wjhvv5251zywsdmhi2phsp3jg9jky6izbyqikfwhdzz") (f (quote (("retry") ("default" "retry"))))))

(define-public crate-expire_map-0.0.21 (c (n "expire_map") (v "0.0.21") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "array-macro") (r "^2.1.5") (d #t) (k 0)) (d (n "async-std") (r "^1.12.0") (f (quote ("unstable"))) (d #t) (k 2)) (d (n "dashmap") (r "^5.3.4") (d #t) (k 0)))) (h "09ss4sj08ksy84kfv4piyb3fb639nmjbral3j4raiqbkjyr4igz9") (f (quote (("retry") ("default" "retry"))))))

(define-public crate-expire_map-0.0.22 (c (n "expire_map") (v "0.0.22") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "array-macro") (r "^2.1.5") (d #t) (k 0)) (d (n "async-std") (r "^1.12.0") (f (quote ("unstable"))) (d #t) (k 2)) (d (n "dashmap") (r "^5.3.4") (d #t) (k 0)))) (h "0r5g13h52q5m54vk3l8liqjj3g0f2jisk655ilx850zq1k4ycljz") (f (quote (("retry") ("default" "retry"))))))

(define-public crate-expire_map-0.0.24 (c (n "expire_map") (v "0.0.24") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "array-macro") (r "^2.1.5") (d #t) (k 0)) (d (n "async-std") (r "^1.12.0") (f (quote ("unstable"))) (d #t) (k 2)) (d (n "dashmap") (r "^5.3.4") (d #t) (k 0)))) (h "1vv3w445nx4k0nhxdhcw6a2z3xzf0v5zm2p2z2msbqfs1i7lgm04") (f (quote (("retry") ("default" "retry"))))))

(define-public crate-expire_map-0.0.25 (c (n "expire_map") (v "0.0.25") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "array-macro") (r "^2.1.5") (d #t) (k 0)) (d (n "async-std") (r "^1.12.0") (f (quote ("unstable"))) (d #t) (k 2)) (d (n "dashmap") (r "^5.3.4") (d #t) (k 0)))) (h "18imfpyw8zpgldyga2rxhv0hwwzbarzfbdl3762lv7f984l41dw3") (f (quote (("retry") ("default" "retry"))))))

(define-public crate-expire_map-0.0.26 (c (n "expire_map") (v "0.0.26") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "array-macro") (r "^2.1.5") (d #t) (k 0)) (d (n "async-std") (r "^1.12.0") (f (quote ("unstable"))) (d #t) (k 2)) (d (n "dashmap") (r "^5.3.4") (d #t) (k 0)))) (h "04wp5glkshz1k35cgwmbl0qjg2dmi9bn8j986z5ilgyyqbcb82il") (f (quote (("retry") ("default" "retry"))))))

(define-public crate-expire_map-0.0.27 (c (n "expire_map") (v "0.0.27") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "array-macro") (r "^2.1.5") (d #t) (k 0)) (d (n "async-std") (r "^1.12.0") (f (quote ("unstable"))) (d #t) (k 2)) (d (n "dashmap") (r "^5.3.4") (d #t) (k 0)))) (h "1b4iqr2nygmjpl48nf498is01bq011ik35fq0xbf5l4v4cqyiqjm") (f (quote (("retry") ("default" "retry"))))))

(define-public crate-expire_map-0.0.28 (c (n "expire_map") (v "0.0.28") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "array-macro") (r "^2.1.5") (d #t) (k 0)) (d (n "async-std") (r "^1.12.0") (f (quote ("unstable"))) (d #t) (k 2)) (d (n "dashmap") (r "^5.3.4") (d #t) (k 0)))) (h "10kmzjwqjkb4bc0nzdj7vm3jz1dplxkylm1valg211ngnm5jpxmp") (f (quote (("retry") ("default" "retry"))))))

(define-public crate-expire_map-0.0.29 (c (n "expire_map") (v "0.0.29") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "array-macro") (r "^2.1.5") (d #t) (k 0)) (d (n "async-std") (r "^1.12.0") (f (quote ("unstable"))) (d #t) (k 2)) (d (n "dashmap") (r "^5.3.4") (d #t) (k 0)))) (h "02gag1ndh7ispqmjv7kilzf9pvjwb2cji1hrxbqdrs8ymi8h0n0v") (f (quote (("retry") ("default" "retry"))))))

(define-public crate-expire_map-0.0.30 (c (n "expire_map") (v "0.0.30") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "array-macro") (r "^2.1.5") (d #t) (k 0)) (d (n "async-std") (r "^1.12.0") (f (quote ("unstable"))) (d #t) (k 2)) (d (n "dashmap") (r "^5.3.4") (d #t) (k 0)))) (h "06q8w2xww32cm9h13l1f5lg3awzl2nqrn1gjpjbm0hfikxnkwaaq") (f (quote (("retry") ("default" "retry"))))))

(define-public crate-expire_map-0.0.31 (c (n "expire_map") (v "0.0.31") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "array-macro") (r "^2.1.5") (d #t) (k 0)) (d (n "async-std") (r "^1.12.0") (f (quote ("unstable"))) (d #t) (k 2)) (d (n "dashmap") (r "^5.3.4") (d #t) (k 0)))) (h "1p4wk3gpbrssj4ax6n8lyjkwcvghxr6rspyiacwpvx8pfhyaq972") (f (quote (("retry") ("default" "retry"))))))

(define-public crate-expire_map-0.0.32 (c (n "expire_map") (v "0.0.32") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "array-macro") (r "^2.1.5") (d #t) (k 0)) (d (n "async-std") (r "^1.12.0") (f (quote ("unstable"))) (d #t) (k 2)) (d (n "dashmap") (r "^5.3.4") (d #t) (k 0)))) (h "0v87l5689akf08h82qbfr4ayvm39lrp3rbx1xswvxbznxrshrjmd") (f (quote (("retry") ("default" "retry"))))))

(define-public crate-expire_map-0.0.33 (c (n "expire_map") (v "0.0.33") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "array-macro") (r "^2.1.5") (d #t) (k 0)) (d (n "async-std") (r "^1.12.0") (f (quote ("unstable"))) (d #t) (k 2)) (d (n "dashmap") (r "^5.3.4") (d #t) (k 0)))) (h "1hxzdvv4n4njil54ah5ixh37nhhqi33pz098934268lc5vlijmm9") (f (quote (("retry") ("default" "retry"))))))

(define-public crate-expire_map-0.0.34 (c (n "expire_map") (v "0.0.34") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "array-macro") (r "^2.1.5") (d #t) (k 0)) (d (n "async-std") (r "^1.12.0") (f (quote ("unstable"))) (d #t) (k 2)) (d (n "dashmap") (r "^5.3.4") (d #t) (k 0)))) (h "01qyi3cvnn7arn7jqw5w310hmz116rfn2wrh4fmd7xb7g9ivlxaq") (f (quote (("retry") ("default" "retry"))))))

