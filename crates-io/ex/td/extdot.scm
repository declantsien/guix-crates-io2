(define-module (crates-io ex td extdot) #:use-module (crates-io))

(define-public crate-extdot-0.1.0 (c (n "extdot") (v "0.1.0") (d (list (d (n "extdot-impl") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)))) (h "12ybcd5vbidqnakxrcwb2bx64qpl2i7zh3i8a82zzbbfj28dp7rv")))

(define-public crate-extdot-0.2.0 (c (n "extdot") (v "0.2.0") (d (list (d (n "extdot-impl") (r "^0.2.0") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "15m4ljip2qcsrwqbwjp9wka75fmcjxf9s3v0cnhwxwlk3jmhady0")))

