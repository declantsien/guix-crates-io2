(define-module (crates-io ex td extd) #:use-module (crates-io))

(define-public crate-extd-0.1.0 (c (n "extd") (v "0.1.0") (d (list (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "095nl83qivsygqd0yblxs82ck7y8n5vqvjqp2nbwlfc1rspf4dld")))

(define-public crate-extd-0.1.1 (c (n "extd") (v "0.1.1") (d (list (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "06zqbbj1s0714209jaazxm3v7w67wx8663rnnyplxca3m1hmdddj")))

(define-public crate-extd-0.1.2 (c (n "extd") (v "0.1.2") (d (list (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0rm6rvng5mfncjviwlx587gc05a82fn46rrrxminfqhmzqjvvvr7")))

(define-public crate-extd-0.1.4 (c (n "extd") (v "0.1.4") (d (list (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1x6alncrgvp8njd2i6m0jc6kd41zy6if07v7709xz6zh1ln9pjbp")))

