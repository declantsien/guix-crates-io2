(define-module (crates-io ex td extdivga) #:use-module (crates-io))

(define-public crate-extdivga-0.1.0 (c (n "extdivga") (v "0.1.0") (d (list (d (n "faer-cholesky") (r "^0.11.0") (d #t) (k 0)) (d (n "faer-core") (r "^0.11.0") (d #t) (k 0)) (d (n "faer-lu") (r "^0.11.0") (d #t) (k 0)) (d (n "faer-qr") (r "^0.11.0") (d #t) (k 0)) (d (n "faer-svd") (r "^0.11.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0pcz3whq2fvkbfpfyhw9l663rn3bx0a84myby2whffgdmalwq2wx")))

