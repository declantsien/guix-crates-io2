(define-module (crates-io ex td extdot-impl) #:use-module (crates-io))

(define-public crate-extdot-impl-0.1.0 (c (n "extdot-impl") (v "0.1.0") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)))) (h "1bya8xvx23y6kq88b0b7v0xiznmvbp60dv7n8a7vy3b8dqq5c2cd")))

(define-public crate-extdot-impl-0.2.0 (c (n "extdot-impl") (v "0.2.0") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)))) (h "1mh8y7zq7nl4k1rsv3zis8cs9gizqlbrismk0bi5rxxa192yjavr")))

