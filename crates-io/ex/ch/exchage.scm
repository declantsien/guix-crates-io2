(define-module (crates-io ex ch exchage) #:use-module (crates-io))

(define-public crate-exchage-0.1.0 (c (n "exchage") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.9.20") (d #t) (k 0)))) (h "0ia7a2kjqlbw597fjngzjh77axv2h6xw6bgm9yb2371640926q18")))

(define-public crate-exchage-0.1.1 (c (n "exchage") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.9.20") (d #t) (k 0)))) (h "14k4aa6vv06ai3p8ndkgyfvgbh7fcjm9vpj4x1zf8yic4arbw70z")))

