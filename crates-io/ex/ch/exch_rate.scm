(define-module (crates-io ex ch exch_rate) #:use-module (crates-io))

(define-public crate-exch_rate-0.1.0 (c (n "exch_rate") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11.18") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.167") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.100") (d #t) (k 0)) (d (n "tokio") (r "^1.29.1") (f (quote ("rt" "full"))) (d #t) (k 0)))) (h "1fk20i26s8mb5apx4i5fyrbclxlv7s2rk81id7lfrfdlw03v0985")))

