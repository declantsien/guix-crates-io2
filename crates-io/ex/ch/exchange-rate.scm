(define-module (crates-io ex ch exchange-rate) #:use-module (crates-io))

(define-public crate-exchange-rate-0.1.0 (c (n "exchange-rate") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "floyd-warshall-alg") (r "^0.1.2") (d #t) (k 0)) (d (n "indexmap") (r "^1.0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "safe-graph") (r "^0.1.4") (d #t) (k 0)))) (h "0zxcc2dnkprq76zqprym6z2wsr14whi4qzjv6qj39gvmf4dj9xl8")))

(define-public crate-exchange-rate-0.1.1 (c (n "exchange-rate") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "floyd-warshall-alg") (r "^0.1.2") (d #t) (k 0)) (d (n "indexmap") (r "^1.0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "safe-graph") (r "^0.1.4") (d #t) (k 0)))) (h "0fyy2s3xiyqisn63s1bvkim2wca3xgci43d6hixmv5dd2r90fvzg")))

(define-public crate-exchange-rate-0.1.3 (c (n "exchange-rate") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "floyd-warshall-alg") (r "^0.1.2") (d #t) (k 0)) (d (n "indexmap") (r "^1.0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "safe-graph") (r "^0.1.4") (d #t) (k 0)))) (h "1cbw5y25nak26h0cyn77ia3pqfq5z54wljqpcgy0w1mh7ajnzccb")))

