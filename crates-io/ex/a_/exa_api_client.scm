(define-module (crates-io ex a_ exa_api_client) #:use-module (crates-io))

(define-public crate-exa_api_client-0.1.0 (c (n "exa_api_client") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "16b4580n3xsa0r30w9xj124y6pm97z6kb8h87m3ybkg1pziha0wj") (y #t)))

(define-public crate-exa_api_client-0.1.1 (c (n "exa_api_client") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "054200qpzp2l3f161k7jfwq31sg7l6vlbc4l5b7gwdy5q9srr9zn") (y #t)))

(define-public crate-exa_api_client-0.1.2 (c (n "exa_api_client") (v "0.1.2") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1aayrp71q43y6y7v8j6sf6jcjb4gqndqlwa86vzvh3892lbvzcv0")))

(define-public crate-exa_api_client-0.1.3 (c (n "exa_api_client") (v "0.1.3") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0rsnx546wx9j95lwjy3v2by0j05bk6ddfm0r7kppmqn56bfjnzq8")))

(define-public crate-exa_api_client-0.1.4 (c (n "exa_api_client") (v "0.1.4") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1iyg5cbpanp5kdr9vb4cqk4b90bn8dgflhsc9nq88qafad15rpcb")))

