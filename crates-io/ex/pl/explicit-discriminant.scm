(define-module (crates-io ex pl explicit-discriminant) #:use-module (crates-io))

(define-public crate-explicit-discriminant-0.1.0 (c (n "explicit-discriminant") (v "0.1.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.82") (d #t) (k 2)))) (h "1fg4kz1mknsfd06f7wi9ji8pyy23pcfrpvqzn8mlf94kf4khx1y5")))

(define-public crate-explicit-discriminant-0.1.2 (c (n "explicit-discriminant") (v "0.1.2") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.82") (d #t) (k 2)))) (h "042l0w5zsj7a0yqsq6l6dqf4w7z6qj0d96gfyf4svibias2asb0m")))

(define-public crate-explicit-discriminant-0.1.3 (c (n "explicit-discriminant") (v "0.1.3") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.82") (d #t) (k 2)))) (h "0haf6rc33ncziz5jix4rd2i6dj7zdcgb6mz40z3xhjcrnjjr9873")))

(define-public crate-explicit-discriminant-0.1.4 (c (n "explicit-discriminant") (v "0.1.4") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.82") (d #t) (k 2)))) (h "00bmx08gcbwv0hvxzk45qvjkli083lxfv4jasylcnz21fxi26v41")))

