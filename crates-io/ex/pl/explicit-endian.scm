(define-module (crates-io ex pl explicit-endian) #:use-module (crates-io))

(define-public crate-explicit-endian-0.1.0 (c (n "explicit-endian") (v "0.1.0") (h "0khpxzwfd8ifbz5w0zk69k8n2a58a3qh3k4256jdrf2fzlbw7rqw")))

(define-public crate-explicit-endian-0.1.1 (c (n "explicit-endian") (v "0.1.1") (h "1gdbn7pq7hyzw6vnk6b81zww2qbmkifx7wwphxh7kjxnmgkq9wm3")))

(define-public crate-explicit-endian-0.1.2 (c (n "explicit-endian") (v "0.1.2") (h "0l9wfyabpyysgj0ajimqp419n94ylh11vhr0dh0wkb3r1hm7h6w2")))

(define-public crate-explicit-endian-0.1.3 (c (n "explicit-endian") (v "0.1.3") (h "0ji7j6qi7sp1m2qr8pkymfyxggzhv1cv4fim3vx7g0gyqiqdhna9")))

(define-public crate-explicit-endian-0.1.4 (c (n "explicit-endian") (v "0.1.4") (h "056n1myvgc3nli2d7x8pdbxbm13z4zr2vybn0zyavp86qmwzwi9h")))

(define-public crate-explicit-endian-0.1.5 (c (n "explicit-endian") (v "0.1.5") (h "13dawbl4nb1mxs50zzdlv5frzha8d4vcxi745813nrsm22mhhf6r")))

