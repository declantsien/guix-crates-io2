(define-module (crates-io ex pl explode) #:use-module (crates-io))

(define-public crate-explode-0.1.0 (c (n "explode") (v "0.1.0") (d (list (d (n "arraydeque") (r "^0.4") (d #t) (k 0)))) (h "05rmyclhprqsfq5kv6ys6ih6vqg4wycnc8vc37nh5mn1zxn9ymyr")))

(define-public crate-explode-0.1.1 (c (n "explode") (v "0.1.1") (d (list (d (n "arraydeque") (r "^0.4") (d #t) (k 0)))) (h "0bmks7jiq5qxlv2g6fijnl3xpc2ib1wwgd037vxa816qcagmxvqg")))

(define-public crate-explode-0.1.2 (c (n "explode") (v "0.1.2") (d (list (d (n "arraydeque") (r "^0.4") (d #t) (k 0)))) (h "1b15h2kw3dl691fk5z5rhab6khiqjyyh81l0w31sd8mpl785ssa6")))

