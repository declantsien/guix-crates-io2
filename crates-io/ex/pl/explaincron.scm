(define-module (crates-io ex pl explaincron) #:use-module (crates-io))

(define-public crate-explaincron-0.1.0 (c (n "explaincron") (v "0.1.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "fastrand") (r "^1.8") (d #t) (k 0)))) (h "0lf60mgadz7l0r3r97l7izk8h99s7fhndvlfvpq8kh6bp5qk6y1s")))

(define-public crate-explaincron-0.2.0 (c (n "explaincron") (v "0.2.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "fastrand") (r "^1.8") (d #t) (k 0)) (d (n "time") (r "^0.3") (f (quote ("local-offset" "macros"))) (d #t) (k 0)))) (h "01i20rav2wh5s0iahwsrn7smskqsc24346bir0glqxwqw67w9y89")))

