(define-module (crates-io ex e_ exe_iza) #:use-module (crates-io))

(define-public crate-exe_iza-0.1.0 (c (n "exe_iza") (v "0.1.0") (d (list (d (n "clap") (r "= 2.33") (d #t) (k 0)) (d (n "futures") (r "= 0.1.28") (d #t) (k 0)) (d (n "kotonoha") (r "= 0.2.0") (d #t) (k 0)))) (h "049iavri8shhvkh8h65mf3nkm98anq53pm6yf4d30pgr66aijg48")))

