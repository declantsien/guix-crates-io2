(define-module (crates-io ex e_ exe_tools) #:use-module (crates-io))

(define-public crate-exe_tools-0.1.0 (c (n "exe_tools") (v "0.1.0") (d (list (d (n "test_tools") (r "~0.8.0") (d #t) (k 2)))) (h "1r8w3ki7kzh9bisk0kfxlmjv3z1855ndmvsfyxdabbcgsj5hvd3k") (f (quote (("use_alloc") ("no_std") ("full" "enabled") ("enabled") ("default" "enabled"))))))

