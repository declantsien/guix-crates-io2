(define-module (crates-io ex ti extism-manifest) #:use-module (crates-io))

(define-public crate-extism-manifest-0.0.1-alpha (c (n "extism-manifest") (v "0.0.1-alpha") (d (list (d (n "base64") (r "^0.20.0-alpha") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1qj136vasf9a55fnf67i9my926krsiis3f7z26p8246dphswwya0")))

(define-public crate-extism-manifest-0.0.1-rc.2 (c (n "extism-manifest") (v "0.0.1-rc.2") (d (list (d (n "base64") (r "^0.20.0-alpha") (d #t) (k 0)) (d (n "schemars") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1jm8w8gzlcjjbcbixvz43yhfifibwz920f5f5sjqi47wdcbbl6ns") (f (quote (("json_schema" "schemars"))))))

(define-public crate-extism-manifest-0.0.1-rc.3 (c (n "extism-manifest") (v "0.0.1-rc.3") (d (list (d (n "base64") (r "^0.20.0-alpha") (d #t) (k 0)) (d (n "schemars") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "0l0ylxj8xdak3hbxb191md96zagk56abwp0fyrnwkmgw0fywndaw") (f (quote (("json_schema" "schemars"))))))

(define-public crate-extism-manifest-0.0.1-rc.4 (c (n "extism-manifest") (v "0.0.1-rc.4") (d (list (d (n "base64") (r "^0.20.0-alpha") (d #t) (k 0)) (d (n "schemars") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "130fyydh53zn8p15nrfcp6yarg9zs32mbgq5v89nxdxn08n7midk") (f (quote (("json_schema" "schemars"))))))

(define-public crate-extism-manifest-0.0.1-rc.5 (c (n "extism-manifest") (v "0.0.1-rc.5") (d (list (d (n "base64") (r "^0.20.0-alpha") (d #t) (k 0)) (d (n "schemars") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "0pjrpkvkjyi7fsbsh751g1j86hm2pdhqvghj04h7mf1v1pnsf8b9") (f (quote (("json_schema" "schemars"))))))

(define-public crate-extism-manifest-0.0.1-rc.6 (c (n "extism-manifest") (v "0.0.1-rc.6") (d (list (d (n "base64") (r "^0.20.0-alpha") (d #t) (k 0)) (d (n "schemars") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "0spbm0riqgli9p73b4fbxnpq1s2c5yqj0pjninzlai0mlfj411ls") (f (quote (("json_schema" "schemars"))))))

(define-public crate-extism-manifest-0.0.1 (c (n "extism-manifest") (v "0.0.1") (d (list (d (n "base64") (r "^0.20.0-alpha") (d #t) (k 0)) (d (n "schemars") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "0labi6ib136c7xn70hjckci1xkm1ddikww60bgjfka2ih7gfjhz9") (f (quote (("json_schema" "schemars"))))))

(define-public crate-extism-manifest-0.1.0 (c (n "extism-manifest") (v "0.1.0") (d (list (d (n "base64") (r "^0.20.0-alpha") (d #t) (k 0)) (d (n "schemars") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "0psibrij80k3wnp0lvcyn0qjdysxp73qmrzzvxvai6a7akyypbkg") (f (quote (("json_schema" "schemars"))))))

(define-public crate-extism-manifest-0.2.0 (c (n "extism-manifest") (v "0.2.0") (d (list (d (n "base64") (r "^0.21.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "0mrsvcfd0p5127vfr70wr8hnl04pm7skixv71hvklfl2nnqh1qgr") (f (quote (("json_schema" "schemars"))))))

(define-public crate-extism-manifest-0.3.0 (c (n "extism-manifest") (v "0.3.0") (d (list (d (n "base64") (r "^0.21.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1ld793di38fy2g7wfgpl7bq6zvga6v5fnknzjp6dkams8lb1ww4j") (f (quote (("json_schema" "schemars"))))))

(define-public crate-extism-manifest-0.5.0 (c (n "extism-manifest") (v "0.5.0") (d (list (d (n "base64") (r "^0.21.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1rxw0b8p0dxzbd6z223rmg1rfqm26v51mhgxbxqk15i8xh0fdc12") (f (quote (("json_schema" "schemars"))))))

(define-public crate-extism-manifest-1.0.0-alpha.0 (c (n "extism-manifest") (v "1.0.0-alpha.0") (d (list (d (n "base64") (r "^0.21.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0pm8z86hqn1ckj5np4hcp30qfpgmh2vs95c742jnmnahy48vlrbq") (f (quote (("json_schema" "schemars"))))))

(define-public crate-extism-manifest-1.0.0-rc2 (c (n "extism-manifest") (v "1.0.0-rc2") (d (list (d (n "base64") (r "~0.21") (d #t) (k 0)) (d (n "schemars") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "01vl0164a7i7c7kpc61hr45sfxf8j7x0nch8r0pc50srlbv5mj9f") (f (quote (("json_schema" "schemars"))))))

(define-public crate-extism-manifest-1.0.0-rc3 (c (n "extism-manifest") (v "1.0.0-rc3") (d (list (d (n "base64") (r "~0.21") (d #t) (k 0)) (d (n "schemars") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0wy3x7dzjbvkmwv9sjkqhhxzsjk4ghbz4619pidwj1xg61dgplng") (f (quote (("json_schema" "schemars"))))))

(define-public crate-extism-manifest-1.0.0-rc5 (c (n "extism-manifest") (v "1.0.0-rc5") (d (list (d (n "base64") (r "~0.21") (d #t) (k 0)) (d (n "schemars") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1x1zzpkgc2sskvch7rwbmh7bvn8alpjbhac59dkqbh1ps1p2scks") (f (quote (("json_schema" "schemars"))))))

(define-public crate-extism-manifest-1.0.0-rc6 (c (n "extism-manifest") (v "1.0.0-rc6") (d (list (d (n "base64") (r "~0.21") (d #t) (k 0)) (d (n "schemars") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0ba8w1zpya8n0cx81vjbdn9x9xl5kc489hpsjf815qsikabh38qy") (f (quote (("json_schema" "schemars"))))))

(define-public crate-extism-manifest-1.0.0-rc7 (c (n "extism-manifest") (v "1.0.0-rc7") (d (list (d (n "base64") (r "~0.21") (d #t) (k 0)) (d (n "schemars") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "06nlyklp32afq0j8mynd0wxk5pgy6nhnm1va1b7miy0f04pdxib5") (f (quote (("json_schema" "schemars"))))))

(define-public crate-extism-manifest-1.0.0 (c (n "extism-manifest") (v "1.0.0") (d (list (d (n "base64") (r "~0.21") (d #t) (k 0)) (d (n "schemars") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1k7szhb1lq0bp4aqrj8wajq0xkj1xxb901i5zxnnwrgxy1fayxpv") (f (quote (("json_schema" "schemars"))))))

(define-public crate-extism-manifest-1.0.1 (c (n "extism-manifest") (v "1.0.1") (d (list (d (n "base64") (r "~0.21") (d #t) (k 0)) (d (n "schemars") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "09f2zb2mm3w0dxfz1k5h5l46dhyj1l7djjyd2my6m1dnszadzifv") (f (quote (("json_schema" "schemars"))))))

(define-public crate-extism-manifest-1.0.2 (c (n "extism-manifest") (v "1.0.2") (d (list (d (n "base64") (r "~0.21") (d #t) (k 0)) (d (n "schemars") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1xi4qzyixiccd0bmczqszja325yg876mr7wn7bhdi1dli17slg3i") (f (quote (("json_schema" "schemars"))))))

(define-public crate-extism-manifest-1.0.3 (c (n "extism-manifest") (v "1.0.3") (d (list (d (n "base64") (r "~0.21") (d #t) (k 0)) (d (n "schemars") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "16g1r6wr2jg7i10mrp7v4ia2lnrfjb26fnpkhjqiks3mgdydqxz9") (f (quote (("json_schema" "schemars"))))))

(define-public crate-extism-manifest-1.1.0 (c (n "extism-manifest") (v "1.1.0") (d (list (d (n "base64") (r "~0.21") (d #t) (k 0)) (d (n "schemars") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "01lxaqbb7y4a4cw8amk7zbbgmwkb4b0nm9n1wrs0hgk3q1nz92m0") (f (quote (("json_schema" "schemars"))))))

(define-public crate-extism-manifest-1.2.0 (c (n "extism-manifest") (v "1.2.0") (d (list (d (n "base64") (r "~0.22") (d #t) (k 0)) (d (n "schemars") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0gaw956pp6bhk2p07x1a6dj8580i6ja8rd93ir0p4sywjmkd3iq5") (f (quote (("json_schema" "schemars"))))))

(define-public crate-extism-manifest-1.3.0 (c (n "extism-manifest") (v "1.3.0") (d (list (d (n "base64") (r "~0.22") (d #t) (k 0)) (d (n "schemars") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0j15ylvsdy6vppxfs8f4alsl24x0gl19wlp2rp43mk0351p8xnlp") (f (quote (("json_schema" "schemars"))))))

