(define-module (crates-io ex ti extism-pdk-derive) #:use-module (crates-io))

(define-public crate-extism-pdk-derive-0.0.1-rc.1 (c (n "extism-pdk-derive") (v "0.0.1-rc.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1dzwj71awsy074rsq9dq4f00gfg9k0b8bfa28hbiwla7d4x0x2ch")))

(define-public crate-extism-pdk-derive-0.1.0 (c (n "extism-pdk-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0hvdqsy7grrc6ywlgz6zwzwrlc63kxvnzwq4184c21f2ighgg7k5")))

(define-public crate-extism-pdk-derive-0.1.1 (c (n "extism-pdk-derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "16jcswwph3ryg2m9mgzwr7mbrdi86kxqn8bpmzwav4dwbd32cwxb")))

(define-public crate-extism-pdk-derive-0.2.0 (c (n "extism-pdk-derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "00kwmy2q6az1smkg18kbz6nwpldz42y4cwhrsr9fv8fk0sxl830q")))

(define-public crate-extism-pdk-derive-0.3.0 (c (n "extism-pdk-derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1z4ydkhmcilcb575pylhw3pzxy8c4v8d9gv6v4dv1jlyy5air1v9")))

(define-public crate-extism-pdk-derive-0.3.1 (c (n "extism-pdk-derive") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1fnhg6h66p3nlpzc1lnb3vff1sc0i1bz3yp0a7h4sc7p61ij3gnj")))

(define-public crate-extism-pdk-derive-1.0.0-beta.0 (c (n "extism-pdk-derive") (v "1.0.0-beta.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0l5k7yny6x2zhbb83883wgk803mbbyldbf2frvqk6d9hd7jc1ck3")))

(define-public crate-extism-pdk-derive-1.0.0-rc1 (c (n "extism-pdk-derive") (v "1.0.0-rc1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1zfcmb4wbsvx3qahppkkvwivdnw6xk0hiin82g2fy00rgzavhh2n")))

(define-public crate-extism-pdk-derive-1.0.0 (c (n "extism-pdk-derive") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1ap9048jfkl5qx1gaayr51x04xzg9j2k01824qb09sad84wr59v1")))

(define-public crate-extism-pdk-derive-1.1.0 (c (n "extism-pdk-derive") (v "1.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0jqdnpfza1c746q2i4lpgaryyw0nkk44k8zjbvys081p0b19affq")))

(define-public crate-extism-pdk-derive-1.2.0 (c (n "extism-pdk-derive") (v "1.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0zji079fn6061z1a428xv30xihbps3x47m4ss78rhl19407ln0is")))

