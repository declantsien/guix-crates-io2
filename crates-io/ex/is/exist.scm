(define-module (crates-io ex is exist) #:use-module (crates-io))

(define-public crate-exist-0.1.0 (c (n "exist") (v "0.1.0") (d (list (d (n "bincode") (r "^1.0.1") (d #t) (k 0)) (d (n "blake2") (r "^0.8.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.2.7") (f (quote ("i128"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_cbor") (r "^0.9.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1nk3xxzs9yiwz97888fwzkwd3cmskd6xph599bfan2gvg4wmwcfa")))

(define-public crate-exist-0.1.1 (c (n "exist") (v "0.1.1") (d (list (d (n "bincode") (r "^1.0.1") (d #t) (k 0)) (d (n "blake2") (r "^0.8.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.2.7") (f (quote ("i128"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_cbor") (r "^0.9.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1cb895rix1qpxhxk75fnh0cxhv3lrp5d93hkfldpf4ima31jnql5")))

(define-public crate-exist-0.1.2 (c (n "exist") (v "0.1.2") (d (list (d (n "bincode") (r "^1.0.1") (d #t) (k 0)) (d (n "blake2") (r "^0.8.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.2.7") (f (quote ("i128"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_cbor") (r "^0.9.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1z3s4h39vi5l6pn1lgq927z64lvcxalcvnhlrq39mh4dfsbwg1xq")))

