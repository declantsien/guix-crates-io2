(define-module (crates-io ex is existential) #:use-module (crates-io))

(define-public crate-existential-0.1.0 (c (n "existential") (v "0.1.0") (h "0yblmynjh2vx283qqglph66y7wryhyr2zz0dgxz5sysjchliqqjp") (y #t)))

(define-public crate-existential-0.1.1 (c (n "existential") (v "0.1.1") (h "1vwqh5xz4j6xkwf0rb1vsnxkcwi5hzd278n5cjc5hdqh6clglbix")))

