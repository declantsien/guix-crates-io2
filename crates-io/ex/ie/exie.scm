(define-module (crates-io ex ie exie) #:use-module (crates-io))

(define-public crate-exie-0.1.0 (c (n "exie") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "console") (r "^0.9") (d #t) (k 0)) (d (n "dialoguer") (r "^0.5") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)))) (h "08yyh9zrqwlycprlcwskyj7n3b76ayw2v3bdfbbbzi83cprlimvx")))

