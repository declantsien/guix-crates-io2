(define-module (crates-io ex oq exoquant) #:use-module (crates-io))

(define-public crate-exoquant-0.1.0 (c (n "exoquant") (v "0.1.0") (d (list (d (n "lodepng") (r "^0.8.1") (d #t) (k 2)) (d (n "rand") (r "^0.3.14") (o #t) (d #t) (k 0)))) (h "13lli6vdswg0w3l3w8bal64sm815bxr0rnn1swhka0d301cp0n5j") (f (quote (("random-sample" "rand"))))))

(define-public crate-exoquant-0.2.0 (c (n "exoquant") (v "0.2.0") (d (list (d (n "lodepng") (r "^0.8.1") (d #t) (k 2)) (d (n "rand") (r "^0.3.14") (o #t) (d #t) (k 0)))) (h "1ap4wm1f2jcqjnjvibgjrxqn279jlm6sajmlcdd4bdw3vvnawj2h") (f (quote (("random-sample" "rand"))))))

