(define-module (crates-io ex lo exlog) #:use-module (crates-io))

(define-public crate-exlog-0.0.1 (c (n "exlog") (v "0.0.1") (h "0n2hmdb9zgx1mg96gbpsjrz7g90yq003kqspihmryghfz13xy6b8") (y #t)))

(define-public crate-exlog-0.0.2 (c (n "exlog") (v "0.0.2") (h "14svywdbiqz6l6qjh6mvaj63mq6czjr7nxqkj1ky3qzqk2vgwg1p") (y #t)))

(define-public crate-exlog-0.1.1 (c (n "exlog") (v "0.1.1") (h "0f6x75zqgwf4gniyds1vakly6myfck7mf4fzkmhd9ca886p3yzp8") (y #t)))

(define-public crate-exlog-0.1.2 (c (n "exlog") (v "0.1.2") (h "1cfw6gb047g6hnzsl2rkd2wpygjbr76vzsmrmrk5yxspgg75fvbc") (y #t)))

(define-public crate-exlog-0.1.3 (c (n "exlog") (v "0.1.3") (h "0ah80d5a0mn6prq69baja0bi1agwkvkddsh0alxqz864ynxlrigm") (y #t)))

(define-public crate-exlog-0.1.4 (c (n "exlog") (v "0.1.4") (h "0l5kif5c6dkx3sxbh6jfg1ylixixpzlcl53s788iyg6mrk8ysbl0")))

