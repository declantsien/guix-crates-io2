(define-module (crates-io ex pr exprz-core) #:use-module (crates-io))

(define-public crate-exprz-core-0.0.0 (c (n "exprz-core") (v "0.0.0") (h "0h9g0jvrhdn3nj26qiz1nkgvbsz9wch1ycyx0a0jy2mxfk6iix9l")))

(define-public crate-exprz-core-0.0.1 (c (n "exprz-core") (v "0.0.1") (h "1y45479dpchgb8zc124hnk4np6mj78xr46nlh4m0vigbwwjr3kr5")))

(define-public crate-exprz-core-0.0.2 (c (n "exprz-core") (v "0.0.2") (h "04fh2qhc2cphng81jllyfw8waibi38vyspm56zcj0rda0zhcq1dx")))

(define-public crate-exprz-core-0.0.3 (c (n "exprz-core") (v "0.0.3") (h "1q3irrlgwkmh87sdriiqyvp0iz8i9znkdbrwfphsi2is5xw19ds0")))

(define-public crate-exprz-core-0.0.4 (c (n "exprz-core") (v "0.0.4") (h "061isap143hz1adhsa5w1wn4hfp5911i0y82n71zqnd73w4bd779")))

(define-public crate-exprz-core-0.0.5 (c (n "exprz-core") (v "0.0.5") (h "17fd46d6ly1hhz9ryx2v1l6f92sjq1mqdxfd4fm955qdn8bx3ji7")))

(define-public crate-exprz-core-0.0.6 (c (n "exprz-core") (v "0.0.6") (h "0lapiwrhpdxfws9m5w071npwsvwxx4n1140phdvd8bp883i1f0ym")))

