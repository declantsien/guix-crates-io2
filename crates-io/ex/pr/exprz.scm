(define-module (crates-io ex pr exprz) #:use-module (crates-io))

(define-public crate-exprz-0.0.0 (c (n "exprz") (v "0.0.0") (d (list (d (n "exprz-core") (r "^0.0.0") (d #t) (k 0)))) (h "0xwi83yil0w8gbw0i7fjbmfbbc1jakqz7nyswhl3fdh8y2lzlc7k")))

(define-public crate-exprz-0.0.1 (c (n "exprz") (v "0.0.1") (d (list (d (n "exprz-core") (r "^0.0.1") (d #t) (k 0)))) (h "0hbww77xpm3g9r6rm83qlfhlb2d13ppwk4d5vvizdimgbb3h1nqv")))

(define-public crate-exprz-0.0.2 (c (n "exprz") (v "0.0.2") (d (list (d (n "exprz-core") (r "^0.0.2") (d #t) (k 0)))) (h "173xqia2y0myc6dp194p6v9jla6p8rmjx6y3dwjj4f2d26nrfmyh")))

(define-public crate-exprz-0.0.3 (c (n "exprz") (v "0.0.3") (d (list (d (n "exprz-core") (r "^0.0.3") (d #t) (k 0)))) (h "1wrk9zdv1d711dpg987sf0vjrrx1ghnzn33qhx00jyfj1gyvnzgj")))

(define-public crate-exprz-0.0.4 (c (n "exprz") (v "0.0.4") (d (list (d (n "exprz-core") (r "^0.0.4") (d #t) (k 0)))) (h "0j4d0a0f0j56ljsas87gb2w9l72apcxfqwqag132ay3ndil31h0y")))

(define-public crate-exprz-0.0.5 (c (n "exprz") (v "0.0.5") (h "0z5xw69v4i8kgvhf55lzm052s2837y6vwhnsns6z1ajxijmc917i") (f (quote (("std") ("default"))))))

(define-public crate-exprz-0.0.6 (c (n "exprz") (v "0.0.6") (h "18zqzz3syipjlp8y80rvyq8vly4yv6cd1phf9146q8flj8f6nxgv") (f (quote (("stable" "alloc") ("shape") ("pattern") ("parse") ("experimental" "shape" "pattern" "buffered") ("default") ("buffered" "alloc") ("alloc") ("all" "stable" "experimental"))))))

(define-public crate-exprz-0.0.7 (c (n "exprz") (v "0.0.7") (h "0xfax7y5km9a26q9l7nkhjgw5qpr27m7492p206zcdxmmv0ch8h7") (f (quote (("stable" "alloc") ("shape") ("pattern") ("parse") ("experimental" "shape" "pattern" "buffered") ("default") ("buffered" "alloc") ("alloc") ("all" "stable" "experimental"))))))

(define-public crate-exprz-0.0.8 (c (n "exprz") (v "0.0.8") (h "006brnr84nypm3ksp67mxlx80r2msfiid38f6a6xsbga3l5cx0ma") (f (quote (("stable" "alloc") ("shape") ("pattern") ("parse") ("experimental" "shape" "pattern" "buffered") ("default") ("buffered" "alloc") ("alloc") ("all" "stable" "experimental"))))))

(define-public crate-exprz-0.0.9 (c (n "exprz") (v "0.0.9") (h "1783kqd1cz3cr85bbg7q9r6jf9nxdlsw6hzqcyic3vgsx7ihsl4g") (f (quote (("stable" "alloc") ("shape") ("pattern") ("parse") ("experimental" "shape" "pattern" "buffered") ("default") ("buffered" "alloc") ("alloc") ("all" "stable" "experimental"))))))

(define-public crate-exprz-0.0.10 (c (n "exprz") (v "0.0.10") (h "18z1qab8lwm1gi7fb8ps5a7gb6yzrz5ywdyrfp3xss9rv54k28sf") (f (quote (("stable" "alloc") ("shape") ("pattern") ("parse") ("experimental" "shape" "pattern" "buffered") ("default") ("buffered" "alloc") ("alloc") ("all" "stable" "experimental"))))))

(define-public crate-exprz-0.0.11 (c (n "exprz") (v "0.0.11") (h "0grvlxpwlnk9c23rijf6c67937cakpz3slhjnm3p04w0h4fcpvb4") (f (quote (("stable" "alloc") ("shape") ("pattern") ("parse") ("experimental" "shape" "pattern" "buffered") ("default") ("buffered" "alloc") ("alloc") ("all" "stable" "experimental"))))))

(define-public crate-exprz-0.0.12 (c (n "exprz") (v "0.0.12") (h "0j5iwmxb4abl38hp8crrs3li5xrgqmrl8kzxzxrr1s8gjlvk0nla") (f (quote (("stable" "alloc") ("shape") ("pattern") ("parse") ("experimental" "shape" "pattern" "buffered") ("default") ("buffered" "alloc") ("alloc") ("all" "stable" "experimental"))))))

(define-public crate-exprz-0.0.13 (c (n "exprz") (v "0.0.13") (h "0dp5bn39afcsi7ssncf3qihyxx1626nsbnc9icaszkn7a91hb73g") (f (quote (("stable" "alloc") ("shape") ("pattern") ("parse") ("experimental" "shape" "pattern" "buffered") ("default") ("buffered" "alloc") ("alloc") ("all" "stable" "experimental"))))))

(define-public crate-exprz-0.0.14 (c (n "exprz") (v "0.0.14") (h "1cd91hb5d4a2n3zicrd8m0kqf4amx0w0fssm647az2i039xk1xa5") (f (quote (("stable" "alloc") ("shape") ("pattern") ("parse") ("multi") ("experimental" "buffered" "multi" "parse" "pattern" "shape") ("default") ("buffered" "alloc") ("alloc") ("all" "stable" "experimental"))))))

(define-public crate-exprz-0.0.15 (c (n "exprz") (v "0.0.15") (d (list (d (n "rayon") (r "^1.5.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (o #t) (d #t) (k 0)))) (h "1233vm1925fglf591pl3jhlm9m7slqc2azyjd5h62yq2bd7wc6g7") (f (quote (("stable" "alloc") ("shape") ("pattern") ("parse") ("no-panic") ("multi") ("experimental" "buffered" "multi" "no-panic" "parse" "pattern" "rayon" "serde" "shape") ("default") ("buffered" "alloc") ("alloc") ("all" "stable" "experimental"))))))

