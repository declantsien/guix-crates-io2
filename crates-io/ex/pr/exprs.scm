(define-module (crates-io ex pr exprs) #:use-module (crates-io))

(define-public crate-exprs-0.1.0 (c (n "exprs") (v "0.1.0") (h "1lnai8zfpas76z7j4mjmpjl4mn9m6nhgp9cwk0yx4v6p2ym88xrg")))

(define-public crate-exprs-0.1.1 (c (n "exprs") (v "0.1.1") (h "0ybwww8wz9gjzqgxqbz47d4gz6dq83w3s3ynpd4iaq2brz449h6s")))

