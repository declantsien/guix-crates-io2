(define-module (crates-io ex pr exprtk_sys) #:use-module (crates-io))

(define-public crate-exprtk_sys-0.0.1 (c (n "exprtk_sys") (v "0.0.1") (d (list (d (n "gcc") (r "^0.3.54") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1fm3wgfxsaq4l7bzsrhk6si6xin0j6fwij9pbi1zpgv3cn2g8lgp") (f (quote (("superscalar_unroll") ("sc_andor") ("rtl_vecops") ("rtl_io_file") ("return_statement") ("enhanced_features") ("default") ("debug") ("comments") ("caseinsensitivity") ("break_continue"))))))

(define-public crate-exprtk_sys-0.0.2 (c (n "exprtk_sys") (v "0.0.2") (d (list (d (n "gcc") (r "^0.3.54") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0ipklnp2cysa6mdy0bm5kpm9b73ds41x4z2sj7rg7m45az7qs012") (f (quote (("superscalar_unroll") ("sc_andor") ("rtl_vecops") ("rtl_io_file") ("return_statement") ("enhanced_features") ("default") ("debug") ("comments") ("caseinsensitivity") ("break_continue"))))))

(define-public crate-exprtk_sys-0.0.3 (c (n "exprtk_sys") (v "0.0.3") (d (list (d (n "gcc") (r "^0.3.54") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "00zbxwla74mddh3m3j8d5zvms6lmgrd1k1mxg2c4air7y9is18j7") (f (quote (("superscalar_unroll") ("sc_andor") ("rtl_vecops") ("rtl_io_file") ("return_statement") ("enhanced_features") ("default") ("debug") ("comments") ("caseinsensitivity") ("break_continue"))))))

(define-public crate-exprtk_sys-0.0.4 (c (n "exprtk_sys") (v "0.0.4") (d (list (d (n "gcc") (r "^0.3.54") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "09lkydn91gcx8lfyjfdsfz2lh0m2vh1djkvjqrzpx07p85spkabq") (f (quote (("superscalar_unroll") ("sc_andor") ("rtl_vecops") ("rtl_io_file") ("return_statement") ("enhanced_features") ("default") ("debug") ("comments") ("caseinsensitivity") ("break_continue"))))))

(define-public crate-exprtk_sys-0.0.5 (c (n "exprtk_sys") (v "0.0.5") (d (list (d (n "gcc") (r "^0.3.54") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "13kjq3qbwbif291pijm9g3i0gh2xbygd0lg658igndqhqn2164pp") (f (quote (("superscalar_unroll") ("sc_andor") ("rtl_vecops") ("rtl_io_file") ("return_statement") ("enhanced_features") ("default") ("debug") ("comments") ("caseinsensitivity") ("break_continue"))))))

(define-public crate-exprtk_sys-0.0.6 (c (n "exprtk_sys") (v "0.0.6") (d (list (d (n "gcc") (r "^0.3.54") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1cvzxnfs6r6p6f4wwl124fkpjzfpq6cbsgi2l0rxvn5alwdvs853") (f (quote (("superscalar_unroll") ("sc_andor") ("rtl_vecops") ("rtl_io_file") ("return_statement") ("enhanced_features") ("default") ("debug") ("comments") ("caseinsensitivity") ("break_continue"))))))

(define-public crate-exprtk_sys-0.0.7 (c (n "exprtk_sys") (v "0.0.7") (d (list (d (n "gcc") (r "^0.3.54") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0zq40pwln235dzhpy6whxfl052dvg3n03nvdipk7bfw28js1nlz0") (f (quote (("superscalar_unroll") ("sc_andor") ("rtl_vecops") ("rtl_io_file") ("return_statement") ("enhanced_features") ("default") ("debug") ("comments") ("caseinsensitivity") ("break_continue"))))))

(define-public crate-exprtk_sys-0.1.0 (c (n "exprtk_sys") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1ifh2vb67hcf2xikaxw4rl0b98hnp51nr12szxxi5w2kgsdjnzsx") (f (quote (("superscalar_unroll") ("sc_andor") ("rtl_vecops") ("rtl_io_file") ("return_statement") ("enhanced_features") ("default") ("debug") ("comments") ("caseinsensitivity") ("break_continue"))))))

