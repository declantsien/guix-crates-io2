(define-module (crates-io ex pr expr-child) #:use-module (crates-io))

(define-public crate-expr-child-0.1.0 (c (n "expr-child") (v "0.1.0") (h "0fsi1zsix995kg59rca42xvvj25y1sy3gijra3w4y21kg4f9cnaj")))

(define-public crate-expr-child-0.1.1 (c (n "expr-child") (v "0.1.1") (d (list (d (n "expr-parent") (r "0.1.*") (d #t) (k 0)))) (h "146diyk8jjnjyrs58jd2mb90dfx6hjz431mlqz2lj9sl58v6pm77")))

