(define-module (crates-io ex pr expry) #:use-module (crates-io))

(define-public crate-expry-0.1.0 (c (n "expry") (v "0.1.0") (d (list (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1") (d #t) (k 2)))) (h "011kn1m3z8ymqvip7ja2dfps305sxn72r926zv861fa3wa0mpx2n") (f (quote (("std") ("mini") ("default" "std") ("bin"))))))

(define-public crate-expry-0.1.1 (c (n "expry") (v "0.1.1") (d (list (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1") (d #t) (k 2)))) (h "1rr5m6q7z7jwk9ik5k29h5rp7y1c8lf5gf8cwd67jvaswhx6hp8z") (f (quote (("std") ("mini") ("default" "std") ("bin"))))))

(define-public crate-expry-0.1.2 (c (n "expry") (v "0.1.2") (d (list (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1") (d #t) (k 2)))) (h "0gkzxivx0rwp5a50s6drl2y17ganj2ljr4y68f8cys0kns4l5qkp") (f (quote (("std") ("mini") ("default" "std") ("bin"))))))

(define-public crate-expry-0.2.0 (c (n "expry") (v "0.2.0") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1") (d #t) (k 2)))) (h "0iv4xf8jx2l9pajk0pn7mhvnydwsrh5swrqck19233i2r9d2sq2s") (f (quote (("std") ("power") ("mini") ("default" "std" "power") ("bin")))) (y #t)))

(define-public crate-expry-0.2.1 (c (n "expry") (v "0.2.1") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1") (d #t) (k 2)))) (h "03ibwm0zad77y6mp53kk2qrmbk3qjvf8rr26412930iwl9dgcm14") (f (quote (("std") ("power") ("mini") ("default" "std" "power") ("bin"))))))

(define-public crate-expry-0.2.2 (c (n "expry") (v "0.2.2") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1") (d #t) (k 2)))) (h "0p2ljk9ylarzph298nxf09kww2zgqgjd63y9ixwbcsfiimavqv4g") (f (quote (("std") ("power") ("mini") ("default" "std" "power") ("bin"))))))

(define-public crate-expry-0.3.0 (c (n "expry") (v "0.3.0") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1") (d #t) (k 2)))) (h "1xv2i3lf0xiia3i8hbyzz3v6q7vknwmqsx0q7lh3y3l1igz34m7p") (f (quote (("std") ("power") ("mini") ("default" "std" "power") ("bin"))))))

(define-public crate-expry-0.4.0 (c (n "expry") (v "0.4.0") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1") (d #t) (k 2)))) (h "15sh5bs5bw1kzi3s6abxfpz6v5i47ajyl91h0wjr6x6wv9sc23dd") (f (quote (("std") ("power") ("mini") ("default" "std" "power") ("bin"))))))

