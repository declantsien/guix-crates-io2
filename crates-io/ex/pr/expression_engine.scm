(define-module (crates-io ex pr expression_engine) #:use-module (crates-io))

(define-public crate-expression_engine-0.1.0 (c (n "expression_engine") (v "0.1.0") (d (list (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.26.1") (d #t) (k 0)))) (h "15pf71by8yssbgafvclwa2q8krz60ix4xkxs363ncv53fm533144") (y #t)))

(define-public crate-expression_engine-0.2.0 (c (n "expression_engine") (v "0.2.0") (d (list (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.26.1") (d #t) (k 0)))) (h "0nfs11vsjcxarhh23m82n9614wm85y2vs8a2i582vcb8b2n1yrr4") (y #t)))

(define-public crate-expression_engine-0.3.0 (c (n "expression_engine") (v "0.3.0") (d (list (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.26.1") (d #t) (k 0)))) (h "17sy82gr79hnpaxx05nhl8wlk10i0dxpjjrr4693h6jcrj3gwy33")))

(define-public crate-expression_engine-0.4.0 (c (n "expression_engine") (v "0.4.0") (d (list (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.26.1") (d #t) (k 0)))) (h "1l8yvlpfxw7vzzg7qh6iv1ygqqdqxmb01kh3q3rxkknagr6y8rzk")))

(define-public crate-expression_engine-0.5.0 (c (n "expression_engine") (v "0.5.0") (d (list (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.26.1") (d #t) (k 0)))) (h "1yabcvj5sybw0d32nqmflq80x665s2a64bginqxs9brj20lj42c1")))

(define-public crate-expression_engine-0.5.1 (c (n "expression_engine") (v "0.5.1") (d (list (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.26.1") (d #t) (k 0)))) (h "1f86fni3g5g6nyq35xqdmzdbnnivajkidp600ljjybr3adwc7qdc")))

(define-public crate-expression_engine-0.5.2 (c (n "expression_engine") (v "0.5.2") (d (list (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.26.1") (d #t) (k 0)))) (h "181wxlbbgicmjaq8vhd0ij9cykxjzx5gbaxddqraq1ckysnzb4sr")))

(define-public crate-expression_engine-0.5.3 (c (n "expression_engine") (v "0.5.3") (d (list (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.26.1") (d #t) (k 0)))) (h "1hyiag7223zkq3v0yazkw6mxmir58gvmp7lcmkfiqf2mkkdlisyw")))

(define-public crate-expression_engine-0.6.0 (c (n "expression_engine") (v "0.6.0") (d (list (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)) (d (n "rust_decimal") (r "^1.31.0") (d #t) (k 0)))) (h "11sj21ys58r3ys9f1mnncjp2mw934g68m941wvj1gac4v012jrkh")))

(define-public crate-expression_engine-0.7.0 (c (n "expression_engine") (v "0.7.0") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)) (d (n "rust_decimal") (r "^1.31.0") (d #t) (k 0)))) (h "0214y6za7b7syc2j7gzd8f355hwhpb82yc9yb9kyv9y6n8rvby3v")))

