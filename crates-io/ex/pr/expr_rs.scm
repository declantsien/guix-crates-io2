(define-module (crates-io ex pr expr_rs) #:use-module (crates-io))

(define-public crate-expr_rs-0.0.0 (c (n "expr_rs") (v "0.0.0") (d (list (d (n "chumsky") (r "^0.8") (d #t) (k 0)) (d (n "pyo3") (r "^0.16.4") (f (quote ("extension-module"))) (o #t) (d #t) (k 0)) (d (n "rust_decimal") (r "^1.23") (f (quote ("maths"))) (d #t) (k 0)))) (h "001bvylck14c548bdavshq24b1iavc25lcl7ixp7hb5iliwy57d9") (s 2) (e (quote (("python" "dep:pyo3"))))))

