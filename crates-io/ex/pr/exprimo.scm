(define-module (crates-io ex pr exprimo) #:use-module (crates-io))

(define-public crate-exprimo-0.1.0 (c (n "exprimo") (v "0.1.0") (d (list (d (n "rslint_parser") (r "~0.3") (d #t) (k 0)) (d (n "scribe-rust") (r "~0.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "~1") (d #t) (k 0)) (d (n "serde_json") (r "~1") (d #t) (k 0)))) (h "0yc19whgmqhars6jahgwwhpiqrww1n1kbkmqjp4hnh1izgfcl3n6") (f (quote (("logging" "scribe-rust") ("default"))))))

(define-public crate-exprimo-0.1.1 (c (n "exprimo") (v "0.1.1") (d (list (d (n "rslint_parser") (r "~0.3") (d #t) (k 0)) (d (n "scribe-rust") (r "~0.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "~1") (d #t) (k 0)) (d (n "serde_json") (r "~1") (d #t) (k 0)))) (h "0wdr5rai7w7l6bj97chwy4dkpya91ilxr0f324p2cwfwm68ryk4h") (f (quote (("logging" "scribe-rust") ("default"))))))

(define-public crate-exprimo-0.1.2 (c (n "exprimo") (v "0.1.2") (d (list (d (n "rslint_parser") (r "~0.3") (d #t) (k 0)) (d (n "scribe-rust") (r "~0.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "~1") (d #t) (k 0)) (d (n "serde_json") (r "~1") (d #t) (k 0)))) (h "1wlaq1rwsvyp7njky2jz5qw2wgr6vfisxq3zq3j09varjh8p0s7n") (f (quote (("logging" "scribe-rust") ("default"))))))

(define-public crate-exprimo-0.1.3 (c (n "exprimo") (v "0.1.3") (d (list (d (n "rslint_parser") (r "~0.3") (d #t) (k 0)) (d (n "scribe-rust") (r "~0.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "~1") (d #t) (k 0)) (d (n "serde_json") (r "~1") (d #t) (k 0)))) (h "10krchmk6vg7a31lgqr5598295vfgahj5mh1zrl2cv16h285x2b8") (f (quote (("logging" "scribe-rust") ("default"))))))

(define-public crate-exprimo-0.1.4 (c (n "exprimo") (v "0.1.4") (d (list (d (n "rslint_parser") (r "~0.3") (d #t) (k 0)) (d (n "scribe-rust") (r "~0.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "~1") (d #t) (k 0)) (d (n "serde_json") (r "~1") (d #t) (k 0)))) (h "0b9achzd8i984lrvhkblh8irdzg1h9c975pr6drlrmxlamggfjx5") (f (quote (("logging" "scribe-rust") ("default"))))))

(define-public crate-exprimo-0.1.5 (c (n "exprimo") (v "0.1.5") (d (list (d (n "rslint_parser") (r "~0.3") (d #t) (k 0)) (d (n "scribe-rust") (r "~0.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "~1") (d #t) (k 0)) (d (n "serde_json") (r "~1") (d #t) (k 0)))) (h "178mr65f06dsd1l93fjdlk8knchhsjnn9rkw2winlw0fjj827gi8") (f (quote (("logging" "scribe-rust") ("default"))))))

(define-public crate-exprimo-0.1.6 (c (n "exprimo") (v "0.1.6") (d (list (d (n "rslint_parser") (r "~0.3") (d #t) (k 0)) (d (n "scribe-rust") (r "~0.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "~1") (d #t) (k 0)) (d (n "serde_json") (r "~1") (d #t) (k 0)))) (h "080i6p5bvlxq4byiqvpbs1i0gdkkazbkd9knlcr25kyk29blhwfj") (f (quote (("logging" "scribe-rust") ("default"))))))

(define-public crate-exprimo-0.1.7 (c (n "exprimo") (v "0.1.7") (d (list (d (n "rslint_parser") (r "~0.3") (d #t) (k 0)) (d (n "scribe-rust") (r "~0.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "~1") (d #t) (k 0)) (d (n "serde_json") (r "~1") (d #t) (k 0)))) (h "0n36nqrdzf3rxdh3ydcny9kg305mdmlqlpii9dkds42709c3yi5b") (f (quote (("logging" "scribe-rust") ("default"))))))

(define-public crate-exprimo-0.1.8 (c (n "exprimo") (v "0.1.8") (d (list (d (n "rslint_parser") (r "~0.3") (d #t) (k 0)) (d (n "scribe-rust") (r "~0.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "~1") (d #t) (k 0)) (d (n "serde_json") (r "~1") (d #t) (k 0)))) (h "1n8lqf345j2965lywwqr19vli812zhcrxlbqmvbwi3xccf8fbz9i") (f (quote (("logging" "scribe-rust") ("default"))))))

(define-public crate-exprimo-0.1.9 (c (n "exprimo") (v "0.1.9") (d (list (d (n "rslint_parser") (r "~0.3") (d #t) (k 0)) (d (n "scribe-rust") (r "~0.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "~1") (d #t) (k 0)) (d (n "serde_json") (r "~1") (d #t) (k 0)))) (h "0difigs8aabfx3kypjl2q3qpk4jzkd7y534k85fiv9lvbp3spmf5") (f (quote (("logging" "scribe-rust") ("default"))))))

(define-public crate-exprimo-0.1.10 (c (n "exprimo") (v "0.1.10") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "rslint_parser") (r "~0.3") (d #t) (k 0)) (d (n "scribe-rust") (r "~0.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "~1") (d #t) (k 0)) (d (n "serde_json") (r "~1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "15fiwqw0b3mjfxcq0a39j29a2da8siyrkxz8jxav10jdsinjqwhp") (f (quote (("logging" "scribe-rust") ("default"))))))

(define-public crate-exprimo-0.1.11 (c (n "exprimo") (v "0.1.11") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "rslint_parser") (r "~0.3") (d #t) (k 0)) (d (n "scribe-rust") (r "~0.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "~1") (d #t) (k 0)) (d (n "serde_json") (r "~1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0svgf38nnblfrpaqa973mb8dsg05iv9xrk9ka4xa2nf0vjamdvwc") (f (quote (("logging" "scribe-rust") ("default"))))))

(define-public crate-exprimo-0.1.13 (c (n "exprimo") (v "0.1.13") (d (list (d (n "anyhow") (r "~1") (d #t) (k 0)) (d (n "rslint_parser") (r "~0.3") (d #t) (k 0)) (d (n "scribe-rust") (r "~0.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "~1") (d #t) (k 0)) (d (n "serde_json") (r "~1") (d #t) (k 0)) (d (n "thiserror") (r "~1") (d #t) (k 0)))) (h "05hcn6a46vnkd0x9ac1b3g8nbk88fl1gc1v8xlalhq1ppwvbl4y9") (f (quote (("logging" "scribe-rust") ("default"))))))

(define-public crate-exprimo-0.1.14 (c (n "exprimo") (v "0.1.14") (d (list (d (n "anyhow") (r "~1") (d #t) (k 0)) (d (n "rslint_parser") (r "~0.3") (d #t) (k 0)) (d (n "scribe-rust") (r "~0.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "~1") (d #t) (k 0)) (d (n "serde_json") (r "~1") (d #t) (k 0)) (d (n "thiserror") (r "~1") (d #t) (k 0)))) (h "0yhidr64pyqqn27fx6pm4glqzvjdv1hns2h1mfvkhw4ssb9sid1r") (f (quote (("logging" "scribe-rust") ("default"))))))

(define-public crate-exprimo-0.1.15 (c (n "exprimo") (v "0.1.15") (d (list (d (n "anyhow") (r "~1") (d #t) (k 0)) (d (n "rslint_parser") (r "~0.3") (d #t) (k 0)) (d (n "scribe-rust") (r "~0.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "~1") (d #t) (k 0)) (d (n "serde_json") (r "~1") (d #t) (k 0)) (d (n "thiserror") (r "~1") (d #t) (k 0)))) (h "1373hdfvka0n5dkhf0lzf7rd0ww728ns0wi3bg19sn0dn6bkqpx9") (f (quote (("logging" "scribe-rust") ("default"))))))

