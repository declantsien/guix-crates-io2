(define-module (crates-io ex pr expression_format_impl) #:use-module (crates-io))

(define-public crate-expression_format_impl-1.0.0 (c (n "expression_format_impl") (v "1.0.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0s4w5dbjn8q9fs4nwlyfwbq92rf84a19nh20rw382vjylkfk8cq3")))

(define-public crate-expression_format_impl-1.0.1 (c (n "expression_format_impl") (v "1.0.1") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0r6whvay17g98ga9mk2hgm8vgighki1w8vyv23l84pa1ihgvsa1s")))

(define-public crate-expression_format_impl-1.1.0 (c (n "expression_format_impl") (v "1.1.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0s8cfgpvxbnplwmvp8mggkm0g7cdrc620hyhk8y7mz92lvvvjx8s")))

(define-public crate-expression_format_impl-1.1.1 (c (n "expression_format_impl") (v "1.1.1") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1p7b366lqdnasvq1blx99n41w4k93msmvqwx0pzfbl45pb80q1fi")))

