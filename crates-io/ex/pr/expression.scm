(define-module (crates-io ex pr expression) #:use-module (crates-io))

(define-public crate-expression-0.1.0 (c (n "expression") (v "0.1.0") (d (list (d (n "asexp") (r "^0.1.0") (d #t) (k 0)))) (h "0iyapw9paa944xabjx15cbhzmbdmbl9ijr889b5jwmx8n64v199l")))

(define-public crate-expression-0.2.0 (c (n "expression") (v "0.2.0") (d (list (d (n "asexp") (r "^0.1.0") (d #t) (k 0)))) (h "1laxp6sy184r4da3p266ff9gxpxadvllqyybgz4209lh4s46h8hj")))

(define-public crate-expression-0.3.0 (c (n "expression") (v "0.3.0") (d (list (d (n "asexp") (r "^0.1.0") (d #t) (k 0)))) (h "0ih9j9gsdwm54xi9kp5vxp2sy1c3m1amrhlwyygwhj6z1rmd1s2c")))

(define-public crate-expression-0.3.1 (c (n "expression") (v "0.3.1") (d (list (d (n "asexp") (r "^0.1.0") (d #t) (k 0)))) (h "0bx62ggs8by6nvxa6gfrbrkm1mpvbhjnrip88knd68c6iqva7a0k")))

(define-public crate-expression-0.3.2 (c (n "expression") (v "0.3.2") (d (list (d (n "asexp") (r "^0.2.0") (d #t) (k 0)))) (h "11lnznx4n3zg87hg8g0p3drwrxqii4909gjr91aszi9jxcbphs3j")))

(define-public crate-expression-0.3.3 (c (n "expression") (v "0.3.3") (d (list (d (n "asexp") (r "^0.3.1") (d #t) (k 0)))) (h "07j2gldzmfn4l9fyhnqvmz9j4adzjwgk5cb97znpsq4g2jlmfhyn")))

(define-public crate-expression-0.3.4 (c (n "expression") (v "0.3.4") (d (list (d (n "asexp") (r "^0.3.2") (d #t) (k 0)))) (h "1cxyd915z6ckc4n1sh3m14pv7p48szfdnxh1wgyrf42n9brvi3zs")))

