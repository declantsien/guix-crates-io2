(define-module (crates-io ex pr expression-num) #:use-module (crates-io))

(define-public crate-expression-num-0.1.0 (c (n "expression-num") (v "0.1.0") (d (list (d (n "asexp") (r "^0.1.0") (d #t) (k 0)) (d (n "expression") (r "^0.3.0") (d #t) (k 0)))) (h "13zsqjliygwlyhly0jy4ncmwsnx73i78q63pjss47h1za8zifja4")))

(define-public crate-expression-num-0.2.0 (c (n "expression-num") (v "0.2.0") (d (list (d (n "asexp") (r "^0.1.0") (d #t) (k 0)) (d (n "expression") (r "^0.3.0") (d #t) (k 0)))) (h "1zs6g6nvsqnqkc293hjpliw249k3c3c4jax6sfgy6hrjl1nx1hgb")))

(define-public crate-expression-num-0.2.1 (c (n "expression-num") (v "0.2.1") (d (list (d (n "asexp") (r "^0.2.0") (d #t) (k 0)) (d (n "expression") (r "^0.3.0") (d #t) (k 0)))) (h "03fdk7x3y0nk3dcgv25pv0dpi0wqdj3kkyji14vx2w939ixmb015")))

(define-public crate-expression-num-0.2.2 (c (n "expression-num") (v "0.2.2") (d (list (d (n "asexp") (r "^0.3.1") (d #t) (k 0)) (d (n "expression") (r "^0.3.3") (d #t) (k 0)))) (h "07x5a2mvffwa68p36akaq20gnb66pzcigh3lrvzsp05hm0p66ihx")))

(define-public crate-expression-num-0.2.3 (c (n "expression-num") (v "0.2.3") (d (list (d (n "asexp") (r "^0.3") (d #t) (k 0)) (d (n "expression") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0053qza9444qhin972898q4c6cf5fdc0jcldba9drvb5bkng0v11")))

