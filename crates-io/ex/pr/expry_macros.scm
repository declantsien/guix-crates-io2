(define-module (crates-io ex pr expry_macros) #:use-module (crates-io))

(define-public crate-expry_macros-0.1.0 (c (n "expry_macros") (v "0.1.0") (d (list (d (n "expry") (r "^0.1.0") (d #t) (k 0)) (d (n "litrs") (r "^0.2.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.39") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)))) (h "0khq74hcfpy41m3zwkplh487fdmqjhp9wdgwv5jr7s4bprfgihgh")))

(define-public crate-expry_macros-0.1.1 (c (n "expry_macros") (v "0.1.1") (d (list (d (n "expry") (r "^0.1") (d #t) (k 0)) (d (n "litrs") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "1m22wipzyxbvd3wwxyf3bnr8pdj1ipyrx7qqy4pnfcs4697r6w6d")))

(define-public crate-expry_macros-0.1.2 (c (n "expry_macros") (v "0.1.2") (d (list (d (n "expry") (r "^0.1") (d #t) (k 0)) (d (n "litrs") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "03v2ndzyd4yjq5v9b3s3kk214zn61dn27rslg0arn80wyxgpyy6q")))

(define-public crate-expry_macros-0.2.0 (c (n "expry_macros") (v "0.2.0") (d (list (d (n "expry") (r "^0.2") (d #t) (k 0)) (d (n "litrs") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "1d6lhylb4iz0yv4ilqg0fpzg512xr4clwzrnnhhwzz1a10i0gsnb") (y #t)))

(define-public crate-expry_macros-0.2.1 (c (n "expry_macros") (v "0.2.1") (d (list (d (n "expry") (r "^0.2") (d #t) (k 0)) (d (n "litrs") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "0xfzpm302p618dqrjgx5qqhk03mm7i7pkg1ab82mdip8i151c71h")))

(define-public crate-expry_macros-0.3.0 (c (n "expry_macros") (v "0.3.0") (d (list (d (n "expry") (r "^0.3") (d #t) (k 0)) (d (n "litrs") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "0flvb7slkzgggcwah5bh4j25cahvc0js6q9478s69wck3j3x4v09")))

(define-public crate-expry_macros-0.4.0 (c (n "expry_macros") (v "0.4.0") (d (list (d (n "expry") (r "^0.4") (d #t) (k 0)) (d (n "litrs") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "0nq4myrc4vy4il200pkidpphw46pq0s15an8sny2m3vb3xr4f3fk")))

