(define-module (crates-io ex pr expr_tools) #:use-module (crates-io))

(define-public crate-expr_tools-0.1.0 (c (n "expr_tools") (v "0.1.0") (d (list (d (n "evalexpr") (r "^6") (d #t) (k 0)) (d (n "statrs") (r "^0.15") (d #t) (k 0)) (d (n "string_utils") (r "^0.1") (d #t) (k 0)) (d (n "vector_utils") (r "^0.1") (d #t) (k 0)))) (h "1rd8vi3a8fb6lip11cmr2ccbxa35amn9md03n666zx2bri59iacx")))

(define-public crate-expr_tools-0.1.1 (c (n "expr_tools") (v "0.1.1") (d (list (d (n "evalexpr") (r "^6") (d #t) (k 0)) (d (n "statrs") (r "^0.15") (d #t) (k 0)) (d (n "string_utils") (r "^0.1") (d #t) (k 0)) (d (n "vector_utils") (r "^0.1") (d #t) (k 0)))) (h "0zlsnhlpmsh53byb0yvlpnryrcc5bxsjbak083b763br3nfm06gv")))

(define-public crate-expr_tools-0.1.2 (c (n "expr_tools") (v "0.1.2") (d (list (d (n "evalexpr") (r "^6") (d #t) (k 0)) (d (n "statrs") (r "^0.15") (d #t) (k 0)) (d (n "string_utils") (r "^0.1") (d #t) (k 0)) (d (n "vector_utils") (r "^0.1") (d #t) (k 0)))) (h "1nglw2y1xiz4k04vylzqq0h61az0gqd4kpd9xmkakn4a8vbj9kfy")))

(define-public crate-expr_tools-0.1.3 (c (n "expr_tools") (v "0.1.3") (d (list (d (n "evalexpr") (r "^7") (d #t) (k 0)) (d (n "statrs") (r "^0.15") (d #t) (k 0)) (d (n "string_utils") (r "^0.1") (d #t) (k 0)) (d (n "vector_utils") (r "^0.1") (d #t) (k 0)))) (h "1ga2wkmbcb19gyvqvwm51b82ghz7nvh7w1rg6xjdhzcv1333i9i6")))

