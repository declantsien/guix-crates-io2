(define-module (crates-io ex pr expresso) #:use-module (crates-io))

(define-public crate-expresso-0.1.0 (c (n "expresso") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "0glm25bym7yjmzria1c7mlj8kknvimxkr3faxnq2rhihfnsgzdpv")))

(define-public crate-expresso-0.1.1 (c (n "expresso") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "1h6npra7qszllp2ravhiq5770qqplsxfz1yxgksh1fmdkjpy8aan")))

(define-public crate-expresso-0.1.2 (c (n "expresso") (v "0.1.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "1g83dj9v5yk120nb9837fpwx9wrbs4df4m0jk7wcnd21d6kfbhid")))

(define-public crate-expresso-0.1.3 (c (n "expresso") (v "0.1.3") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "1gnpys5brqm31lma2fwif5jfmqwp8sjv3ddnjkvlrnlkh1kgward")))

(define-public crate-expresso-0.1.4 (c (n "expresso") (v "0.1.4") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "0n4vnxc1df1fx3pnv56p8j5c1m2hvvki3wl8zr7jpw3vxfiwxkcw")))

(define-public crate-expresso-0.2.0 (c (n "expresso") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "1bhn4jz2kkcq7lpnnqcryf8p44rbj827r9qsqvb6222a0ssiv8gh")))

(define-public crate-expresso-0.2.1 (c (n "expresso") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "0wlh23nia7k4gv1qlbanp7pqk4h1fcyvi812wp96b4nig90v39x8")))

(define-public crate-expresso-0.2.2 (c (n "expresso") (v "0.2.2") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "0b5zw8j0sl4xxm7nyaiz6h0di641j760knvf64arfvizn448c2lw")))

(define-public crate-expresso-0.2.3 (c (n "expresso") (v "0.2.3") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "094x6pnkvppa5m63spglgxnzw2px81v04clqn2acsypkj9xjaj8k")))

(define-public crate-expresso-0.2.4 (c (n "expresso") (v "0.2.4") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "0ml1fmzvcdwcnz4fhqv11pna5x70ljsxnjml987qp9qf3yvcafxb")))

(define-public crate-expresso-0.2.5 (c (n "expresso") (v "0.2.5") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "14p3kcaxl6j2fb1y64dxp8aah2f3mvh2bmyd7n2712zn0pwb5rxx")))

