(define-module (crates-io ex pr expression-closed01) #:use-module (crates-io))

(define-public crate-expression-closed01-0.1.0 (c (n "expression-closed01") (v "0.1.0") (d (list (d (n "asexp") (r "^0.1.0") (d #t) (k 0)) (d (n "closed01") (r "^0.1.0") (d #t) (k 0)) (d (n "expression") (r "^0.3.0") (d #t) (k 0)))) (h "143n20jra416ix6srxccig4lvjzclyn83cgbx1fhc32xrg90767n")))

(define-public crate-expression-closed01-0.2.0 (c (n "expression-closed01") (v "0.2.0") (d (list (d (n "asexp") (r "^0.1.0") (d #t) (k 0)) (d (n "closed01") (r "^0.3.0") (d #t) (k 0)) (d (n "expression") (r "^0.3.0") (d #t) (k 0)))) (h "00q1v1yfcsbv73cd7671grbpyy64xw9g28ghizxsc0x8g1g06ifl")))

(define-public crate-expression-closed01-0.2.1 (c (n "expression-closed01") (v "0.2.1") (d (list (d (n "asexp") (r "^0.2.0") (d #t) (k 0)) (d (n "closed01") (r "^0.3.0") (d #t) (k 0)) (d (n "expression") (r "^0.3.0") (d #t) (k 0)))) (h "08ck6dngydxixbd84d0g6yc2qi9cq36g0qqry4qdxz234vd5v8rz")))

