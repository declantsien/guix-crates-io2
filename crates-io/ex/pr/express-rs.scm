(define-module (crates-io ex pr express-rs) #:use-module (crates-io))

(define-public crate-express-rs-0.1.0 (c (n "express-rs") (v "0.1.0") (h "02652nmgqi8qvqiwn747ab6qdr865bc2hgifb327fy875nwllvzi") (y #t)))

(define-public crate-express-rs-0.0.1 (c (n "express-rs") (v "0.0.1") (h "1dlz0a81ix0ipryd8knpwmji4x2bmhjl160znl7n1biw047f96gf")))

(define-public crate-express-rs-0.0.2 (c (n "express-rs") (v "0.0.2") (h "1f57dgz23bxa22y9b3css41daa5c05l6jkv44y5paav94i4j9lip")))

(define-public crate-express-rs-0.0.3 (c (n "express-rs") (v "0.0.3") (h "1913ws1pz21h5slqfyyhbid6n74ywipscjgh0spfch1mi6sh0kpv")))

(define-public crate-express-rs-0.0.4 (c (n "express-rs") (v "0.0.4") (h "0z12yf9y81yibsm37pyr0sd8l2nfykgmgn60s3257j1ransbksds")))

(define-public crate-express-rs-0.0.5 (c (n "express-rs") (v "0.0.5") (h "0ghvx8dk1masz1washmjjlxc7gn9bg101q2s6f124a3zf8nspglj")))

