(define-module (crates-io ex pr expression_format) #:use-module (crates-io))

(define-public crate-expression_format-1.0.0 (c (n "expression_format") (v "1.0.0") (d (list (d (n "expression_format_impl") (r "^1") (d #t) (k 0)))) (h "0zycgj76kydz3vmjrx5x9xpccpnm64601cwyffxxgzn5xyp1gs20")))

(define-public crate-expression_format-1.0.1 (c (n "expression_format") (v "1.0.1") (d (list (d (n "expression_format_impl") (r "^1") (d #t) (k 0)))) (h "1rh6hvd9855p781l7a20hjkwr54byck4jhqfv0s9iarafww0vhaz")))

(define-public crate-expression_format-1.0.2 (c (n "expression_format") (v "1.0.2") (d (list (d (n "expression_format_impl") (r "^1") (d #t) (k 0)))) (h "0f9bhbsqy4vvvjmwnnsii1dix5awbdgf9l2sb5zrff6a9f6l2ap8")))

(define-public crate-expression_format-1.1.0 (c (n "expression_format") (v "1.1.0") (d (list (d (n "expression_format_impl") (r "^1") (d #t) (k 0)))) (h "0a2qvz1nmvcyjj2smkwp7zms7sba6dswpsww98x1zd5l5zhm5c5s")))

(define-public crate-expression_format-1.1.1 (c (n "expression_format") (v "1.1.1") (d (list (d (n "expression_format_impl") (r "^1") (d #t) (k 0)))) (h "1msbk7gvp64xrj7gsvw3m79za7gdawvpkyz31s9i72nkiq7i4ab7")))

(define-public crate-expression_format-1.1.2 (c (n "expression_format") (v "1.1.2") (d (list (d (n "expression_format_impl") (r "^1") (d #t) (k 0)))) (h "0g09dxrfp0676k7a1p8lpbiiyhzshn35gj1sp6cmbx4bk0sj6850")))

(define-public crate-expression_format-1.1.3 (c (n "expression_format") (v "1.1.3") (d (list (d (n "expression_format_impl") (r "^1.1.1") (d #t) (k 0)))) (h "00n3y4z1a8dncas9aqk24khbjjzwnjhy9vrgv35w8d3cz3d2x3fi")))

