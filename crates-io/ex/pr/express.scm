(define-module (crates-io ex pr express) #:use-module (crates-io))

(define-public crate-express-0.0.1 (c (n "express") (v "0.0.1") (h "1iqnimysyylngp9dzl4fyb25m03aa363lp3i2l8ns0syiyd8xnjh")))

(define-public crate-express-0.0.2 (c (n "express") (v "0.0.2") (h "0p7zgzkidygwmbbj41ba0wzzrx0b4la2y85cclgnhhz4qs95ww9d")))

