(define-module (crates-io ex ce excel-kit) #:use-module (crates-io))

(define-public crate-excel-kit-0.1.0 (c (n "excel-kit") (v "0.1.0") (d (list (d (n "quick-xml") (r "^0.30.0") (d #t) (k 0)))) (h "0yj8ag6hx9vx3n7mbi1kig1bv3mrschx9swka5l153zqd7gy11cj") (y #t) (r "1.64.0")))

(define-public crate-excel-kit-0.1.1 (c (n "excel-kit") (v "0.1.1") (d (list (d (n "quick-xml") (r "^0.30.0") (d #t) (k 0)))) (h "10q3hc9g1libd08cdw6b9wxh36qqa3sag47sxd50f61ld2637i0q") (r "1.64.0")))

(define-public crate-excel-kit-0.1.2 (c (n "excel-kit") (v "0.1.2") (d (list (d (n "quick-xml") (r "^0.30.0") (d #t) (k 0)))) (h "16vwdzwd88gw9j08829i7w7imslxzy4q8801yvsngprvva8zx6b7") (r "1.64.0")))

