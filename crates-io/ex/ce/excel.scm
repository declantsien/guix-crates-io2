(define-module (crates-io ex ce excel) #:use-module (crates-io))

(define-public crate-excel-0.0.1 (c (n "excel") (v "0.0.1") (d (list (d (n "actix-multipart") (r "^0.1.1") (d #t) (k 0)) (d (n "actix-web") (r "^1.0.0-rc") (d #t) (k 0)) (d (n "calamine") (r "^0.15.4") (d #t) (k 0)) (d (n "concat-string") (r "^1.0.1") (d #t) (k 0)) (d (n "futures") (r "^0.1.27") (d #t) (k 0)) (d (n "uuid") (r "^0.7.4") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "05ji7g760jmj8clyw59v2zr8w9fdfw26v0ma2xc61ijc50afcaxz")))

