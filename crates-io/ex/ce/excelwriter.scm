(define-module (crates-io ex ce excelwriter) #:use-module (crates-io))

(define-public crate-excelwriter-0.1.0 (c (n "excelwriter") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)) (d (n "regex") (r "^1.5.6") (d #t) (k 0)) (d (n "regex") (r "^1.5.6") (d #t) (k 2)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)) (d (n "zip") (r "^0.6.2") (d #t) (k 0)))) (h "05fpshad290lsadn8f1hg30b4m20hb7skhv67db8sfzv1dvp1pfc")))

