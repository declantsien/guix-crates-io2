(define-module (crates-io ex ce except) #:use-module (crates-io))

(define-public crate-except-0.1.0 (c (n "except") (v "0.1.0") (h "0lspqkl85h67rj4s7s04wgl33v0gx1gjlixwf5xhywzmhl2r8l85")))

(define-public crate-except-0.2.0 (c (n "except") (v "0.2.0") (h "0339dyc1i49a1m2b30mmhanvmjqy0sp9z5rfss90y29ydrkm8fsi") (r "1.68")))

(define-public crate-except-0.3.0 (c (n "except") (v "0.3.0") (h "0lsjrljk9kr166x95l2dchcd643bqa31a7bgmz6inmn9y3ngj249") (r "1.68")))

