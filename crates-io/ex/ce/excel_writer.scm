(define-module (crates-io ex ce excel_writer) #:use-module (crates-io))

(define-public crate-excel_writer-0.1.0 (c (n "excel_writer") (v "0.1.0") (d (list (d (n "excel_column_id") (r "^0.1") (f (quote ("with-serde"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tera") (r "^0.11") (d #t) (k 0)) (d (n "zip") (r "^0.5") (f (quote ("deflate"))) (k 0)))) (h "0pinc9s5cm3xl1faddwmp4laxczg44nxk4wy9pb956g3lg41zj9a")))

(define-public crate-excel_writer-0.2.0 (c (n "excel_writer") (v "0.2.0") (d (list (d (n "excel_column_id") (r "^0.1") (f (quote ("with-serde"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tera") (r "^1.0.0-beta.14") (d #t) (k 0)) (d (n "zip") (r "^0.5") (f (quote ("deflate"))) (k 0)))) (h "1962l6s9lys6062caas4aaxa2mh6nccsim14bhc602fdnzb5z8q6")))

(define-public crate-excel_writer-0.2.1 (c (n "excel_writer") (v "0.2.1") (d (list (d (n "excel_column_id") (r "^0.1") (f (quote ("with-serde"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tera") (r "^1.2") (d #t) (k 0)) (d (n "zip") (r "^0.5") (f (quote ("deflate"))) (k 0)))) (h "1y33il9jam4yczicjxglagmdcyyfwzvijwkmrbam7r18ih3s5da5")))

(define-public crate-excel_writer-0.2.2 (c (n "excel_writer") (v "0.2.2") (d (list (d (n "excel_column_id") (r "^0.1") (f (quote ("with-serde"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tera") (r "^1.2") (d #t) (k 0)) (d (n "zip") (r "^0.5") (f (quote ("deflate"))) (k 0)))) (h "0g9jxrg3gx8kif2x5r7l3dqqzl51g8lnpabj8s0vrf9bi0cl31rk")))

