(define-module (crates-io ex am exam) #:use-module (crates-io))

(define-public crate-exam-0.0.1 (c (n "exam") (v "0.0.1") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0k3axz95ilay7a9adf1921h3d9fr0gswf9l2fbrjqg0wamg9rm7l")))

(define-public crate-exam-0.0.2 (c (n "exam") (v "0.0.2") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0c16gjrkj9wfmk29fjrvgliwsgx4f9g76balcxkzr7xp59l0jsm5")))

(define-public crate-exam-0.0.3 (c (n "exam") (v "0.0.3") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1j2lav27r1yysip7wcdmiqi2cjks192gfg7bnrr9mz4y7la2b9rb")))

