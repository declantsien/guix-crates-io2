(define-module (crates-io ex am example-hello-world-api) #:use-module (crates-io))

(define-public crate-example-hello-world-api-0.1.0 (c (n "example-hello-world-api") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)) (d (n "example-hello-world-types") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.29.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "warp") (r "^0.3.5") (d #t) (k 0)))) (h "0fp27hlid1i9595g3r3qdnayryay6rxr0g9147nq09hr4vyqnp1w") (y #t)))

(define-public crate-example-hello-world-api-0.1.2 (c (n "example-hello-world-api") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)) (d (n "example-hello-world-types") (r "^0.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.29.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "warp") (r "^0.3.5") (d #t) (k 0)))) (h "0hy3kj17p7x7c9sad4fm9j85lg3jvaz0m2s2x71n6mv0w1pimlqm") (y #t)))

