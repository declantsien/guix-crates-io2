(define-module (crates-io ex am example_crate_with_settings) #:use-module (crates-io))

(define-public crate-example_crate_with_settings-0.0.2 (c (n "example_crate_with_settings") (v "0.0.2") (d (list (d (n "crate-settings") (r "^0.0.2") (d #t) (k 0)))) (h "0yr4q16lcvqrmhqgc31ykkgdw3amsby5x3l1n8lr60vfby9dmq61")))

(define-public crate-example_crate_with_settings-0.0.3 (c (n "example_crate_with_settings") (v "0.0.3") (d (list (d (n "crate-settings") (r "^0.0.3") (d #t) (k 0)))) (h "066112gl2sr14r8xlfypml40nfd89dmw3pdvmbjd8ld6jxc37paz")))

(define-public crate-example_crate_with_settings-0.0.4 (c (n "example_crate_with_settings") (v "0.0.4") (d (list (d (n "crate-settings") (r "^0.0.4") (d #t) (k 0)))) (h "0ywwg02k1l76r9yk1zvd1hir3gk5dcimbpbclmjgmng55lq8d7jx")))

(define-public crate-example_crate_with_settings-0.0.5 (c (n "example_crate_with_settings") (v "0.0.5") (d (list (d (n "crate-settings") (r "^0.0.5") (d #t) (k 0)))) (h "0c90jkc7pq92fa11rn4km19b58qd0qmsbrl2qmn8dvj7wkk2xd5z")))

(define-public crate-example_crate_with_settings-0.0.6 (c (n "example_crate_with_settings") (v "0.0.6") (d (list (d (n "crate-settings") (r "^0") (d #t) (k 0)))) (h "1v5ln2x6ad1sa8gys4crzgc4hamrykfmcyav01r2zql7xa104418")))

(define-public crate-example_crate_with_settings-0.0.7 (c (n "example_crate_with_settings") (v "0.0.7") (d (list (d (n "crate-settings") (r "^0") (d #t) (k 0)))) (h "0jfwsxlvmbpxfbv0l4apq0vi74n7jgzs2v77gq6iikrmmyx2ki9l")))

