(define-module (crates-io ex am example-hello-world-types) #:use-module (crates-io))

(define-public crate-example-hello-world-types-0.1.0 (c (n "example-hello-world-types") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.104") (d #t) (k 0)))) (h "1mi5sbkqw2lwxv9cxr0b6x13qxijqc6kd31w51afpbc5c7asrg7y") (y #t)))

(define-public crate-example-hello-world-types-0.1.2 (c (n "example-hello-world-types") (v "0.1.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.104") (d #t) (k 0)))) (h "1fx0xflk5dwchikkqcd9a1mnjby1lcj7gjwikbl2d44s70zrqh6g") (y #t)))

