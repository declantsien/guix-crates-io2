(define-module (crates-io ex am example) #:use-module (crates-io))

(define-public crate-example-0.1.0 (c (n "example") (v "0.1.0") (h "1pkk8i9kvxp4piwm5v4nwyxpqbk2yzd8qsljmvk6p4rnsw5yqrqs")))

(define-public crate-example-0.2.0 (c (n "example") (v "0.2.0") (h "1g21arqah62pf0ah01vhv3z4d1jw477hp1g767b17xprf07j0x2l")))

(define-public crate-example-1.0.0 (c (n "example") (v "1.0.0") (h "0rigkkigai20pydbygs8n06li20h2fl6h3g22jsnvzsy5m968k1c")))

(define-public crate-example-1.0.1 (c (n "example") (v "1.0.1") (h "1h79cipp88s18xzsrcvrg75d14ngxjygfs2bjwa97l9a4b6vhgpb")))

(define-public crate-example-1.0.2 (c (n "example") (v "1.0.2") (h "1g4s9ypkqjyp72rj4gs9i7nkljmcnjx552jrkc8iy157l7b9a4hh")))

(define-public crate-example-1.1.0 (c (n "example") (v "1.1.0") (h "0zi7d1x5k5k320x31zi72bglhfa076bcsgwg9b9dcz3i97i26shb")))

(define-public crate-example-1.1.1 (c (n "example") (v "1.1.1") (h "1z1435c51zdmqxbh14jyxncqsfyp2xpnhy0cfy9ak5rxc69qhq0l") (y #t)))

