(define-module (crates-io ex am example_api) #:use-module (crates-io))

(define-public crate-example_api-0.1.0 (c (n "example_api") (v "0.1.0") (h "1zbq5rmdas46qaw0jssfdkqag740pcz9jx1s30h1cwhmam19ryfh")))

(define-public crate-example_api-0.1.1 (c (n "example_api") (v "0.1.1") (h "1g0lgj1vglxrd5azfxwpxkhfmf7hjg6zbcnxlxz2hiiwjl71vhrz")))

(define-public crate-example_api-0.2.0 (c (n "example_api") (v "0.2.0") (h "1j4bq3pvm097d9bkq4m3hgllwidb9rilnxaf6hbi5qg523ds9i37")))

(define-public crate-example_api-0.3.0 (c (n "example_api") (v "0.3.0") (h "0mi1pnspkiqhhxqx2c589m4wmlr3gvbdzr03vvbx9qc7s6flfcw9")))

(define-public crate-example_api-0.2.1 (c (n "example_api") (v "0.2.1") (h "1zxahr0x7spbrzl3hl3422m99bf6bz4cn8nxcf6z3kfm9kifpf9c") (f (quote (("feature"))))))

