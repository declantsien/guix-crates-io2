(define-module (crates-io ex tp extprim_literals) #:use-module (crates-io))

(define-public crate-extprim_literals-0.2.0 (c (n "extprim_literals") (v "0.2.0") (d (list (d (n "extprim") (r "^0.2.0") (d #t) (k 0)))) (h "0n54vdcqlsnjhbb2g6nfz4nb7k0p6xcyws8kq32ns04d0fkpnmgx")))

(define-public crate-extprim_literals-0.2.1 (c (n "extprim_literals") (v "0.2.1") (d (list (d (n "extprim") (r "~0.2") (d #t) (k 0)))) (h "0jjcilp64m9mcs12kf8x4fagcyf5f7mfw2060w60g5k3iwkw8gv8")))

(define-public crate-extprim_literals-0.2.2 (c (n "extprim_literals") (v "0.2.2") (d (list (d (n "extprim") (r "~0.2") (d #t) (k 0)))) (h "00xh2sia5603wpivlhfrhkzhkh91w4gjmg7y7yg5xw7l7m09lby6")))

(define-public crate-extprim_literals-1.0.0-rc.1 (c (n "extprim_literals") (v "1.0.0-rc.1") (d (list (d (n "extprim") (r "^1.0.0-rc.1") (d #t) (k 0)) (d (n "rustc_version") (r "^0.1.7") (d #t) (k 1)) (d (n "syntex") (r "^0.31.0") (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.31.0") (d #t) (k 0)))) (h "1pi4vsccp32c2v2rx156dmlpj1kqkc9x1wl753f9v9kcw8wsk2wj") (f (quote (("doc_only")))) (y #t)))

(define-public crate-extprim_literals-1.0.0 (c (n "extprim_literals") (v "1.0.0") (d (list (d (n "extprim") (r "^1.0.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.1.7") (d #t) (k 1)) (d (n "syntex") (r "^0.31.0") (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.31.0") (d #t) (k 0)))) (h "168685vrcrbahzv308pnpq36vs6b2lgfhpmpqnwz4bmbd78r8fm0") (f (quote (("doc_only"))))))

(define-public crate-extprim_literals-1.0.1 (c (n "extprim_literals") (v "1.0.1") (d (list (d (n "extprim") (r "^1.0.1") (d #t) (k 0)) (d (n "rustc_version") (r "^0.1.7") (d #t) (k 1)) (d (n "syntex") (r "^0.33.0") (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.33.0") (d #t) (k 0)))) (h "0a6xri12lh3byvfnz5xw4ap37ncmfpkpfq6w1g5gjlsvjzb4myph") (f (quote (("doc_only"))))))

(define-public crate-extprim_literals-1.1.0 (c (n "extprim_literals") (v "1.1.0") (d (list (d (n "extprim") (r "^1.1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.1.7") (d #t) (k 1)) (d (n "syntex") (r "^0.36.0") (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.36.0") (d #t) (k 0)))) (h "01635iix6igrxzp87q8mb0ychrs3jvk1kknfq01hjb3c4hkwim49") (f (quote (("doc_only"))))))

(define-public crate-extprim_literals-1.2.0 (c (n "extprim_literals") (v "1.2.0") (d (list (d (n "extprim") (r "^1.2.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.1.7") (d #t) (k 1)) (d (n "syntex") (r "^0.48.0") (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.48.0") (d #t) (k 0)))) (h "138wkybhrzpmdg0hd9mr2qlwwn392lzsad508nfk54gpi78xh9wm") (f (quote (("doc_only"))))))

(define-public crate-extprim_literals-2.0.0 (c (n "extprim_literals") (v "2.0.0") (d (list (d (n "extprim_literals_macros") (r "^2.0.0") (d #t) (k 0)) (d (n "procedural-masquerade") (r "^0.1.1") (d #t) (k 0)))) (h "0i6pix7zsij38sd8y8dx50jwg65qay0nbns8606x71ziiy6a73bd") (f (quote (("doc_only"))))))

(define-public crate-extprim_literals-2.0.1 (c (n "extprim_literals") (v "2.0.1") (d (list (d (n "extprim") (r "^1.2") (d #t) (k 2)) (d (n "extprim_literals_macros") (r "^2.0") (d #t) (k 0)) (d (n "procedural-masquerade") (r "^0.1") (d #t) (k 0)))) (h "1c322bh2hx6ab92gag47q8irsk9fi2k2fwj6s0ajl7m8pinahvq7") (y #t)))

(define-public crate-extprim_literals-2.0.2 (c (n "extprim_literals") (v "2.0.2") (d (list (d (n "extprim") (r "^1.3") (d #t) (k 2)) (d (n "extprim_literals_macros") (r "^2.0") (d #t) (k 0)) (d (n "procedural-masquerade") (r "^0.1") (d #t) (k 0)))) (h "0xjdjx23m3jfm3bi78dnl60dcv08mymrvxpdavcpy8g2crwnh3a7")))

(define-public crate-extprim_literals-2.0.3 (c (n "extprim_literals") (v "2.0.3") (d (list (d (n "extprim") (r "^1") (d #t) (k 2)) (d (n "extprim_literals_macros") (r "^2.0") (d #t) (k 0)) (d (n "procedural-masquerade") (r "^0.1") (d #t) (k 0)))) (h "09zsy9hzbjvnrzz9zd25xjijlbzp3m36cqc5q3znnqzvxjp02q46")))

