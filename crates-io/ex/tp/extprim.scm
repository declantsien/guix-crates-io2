(define-module (crates-io ex tp extprim) #:use-module (crates-io))

(define-public crate-extprim-0.2.0 (c (n "extprim") (v "0.2.0") (h "0hj0g1b89h3gp57aacf0ma2y8268yvxyflj10ki9qiivim7g5xig")))

(define-public crate-extprim-0.2.1 (c (n "extprim") (v "0.2.1") (d (list (d (n "extprim_literals") (r "~0.2") (d #t) (k 2)))) (h "05c81q4iylndgxk7j65zmwbn4692mm64sf9bdjbp9j6yalg38asl")))

(define-public crate-extprim-0.2.2 (c (n "extprim") (v "0.2.2") (d (list (d (n "extprim_literals") (r "~0.2") (d #t) (k 2)))) (h "0ia52w0fykvq9ffj0idb7l1l40mblisb6ziw2igfzx27qc4579iy")))

(define-public crate-extprim-0.2.3 (c (n "extprim") (v "0.2.3") (d (list (d (n "extprim_literals") (r "~0.2") (d #t) (k 2)))) (h "0ad9504w2gjy3ma3598h436l3k9zz28swxzvsx87j3a898nkjrap")))

(define-public crate-extprim-0.3.0 (c (n "extprim") (v "0.3.0") (h "1zhzcy3sc7l36bd48xc64q9znlw53qncybpv7lbrajw3xmx1bv6q")))

(define-public crate-extprim-0.4.1 (c (n "extprim") (v "0.4.1") (d (list (d (n "lazy_static") (r "*") (d #t) (k 0)))) (h "0ha66bqz918nwqyv6lvqa6fhaa32q02bg8fc2w2g42f982dhr4k0")))

(define-public crate-extprim-0.4.2 (c (n "extprim") (v "0.4.2") (d (list (d (n "lazy_static") (r "*") (d #t) (k 0)))) (h "1779vnya6r0vfrqni4idq26rg1y3z2w5ay98rn8g5jz2qwkl4gk2")))

(define-public crate-extprim-0.4.3 (c (n "extprim") (v "0.4.3") (d (list (d (n "lazy_static") (r "*") (d #t) (k 0)))) (h "1wllpp64sx7mqdyl7lgiql6c7w22y60r0lwcj3dmrnwqjlfdb8f6")))

(define-public crate-extprim-0.5.0 (c (n "extprim") (v "0.5.0") (d (list (d (n "lazy_static") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 0)))) (h "0cb9rj0h2bxzs5vxjqc5c8h4wxv7w13za172753xc3mqjzkkb9m0")))

(define-public crate-extprim-0.5.1 (c (n "extprim") (v "0.5.1") (d (list (d (n "lazy_static") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 0)))) (h "1i95bls7hxrls8fy8qh0a375sh9rj1fp6z2hl7dqw4frwzbkvgz5")))

(define-public crate-extprim-1.0.0-rc.1 (c (n "extprim") (v "1.0.0-rc.1") (d (list (d (n "lazy_static") (r "^0.2.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.1.32") (d #t) (k 0)) (d (n "rand") (r "^0.3.14") (d #t) (k 0)) (d (n "rustc_version") (r "^0.1.7") (d #t) (k 1)))) (h "1nfykccam6q2j1w2l32d5c49gwbi2phpvlplfffzkhdy48xpb0cr") (y #t)))

(define-public crate-extprim-1.0.0 (c (n "extprim") (v "1.0.0") (d (list (d (n "lazy_static") (r "^0.2.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.1.32") (d #t) (k 0)) (d (n "rand") (r "^0.3.14") (d #t) (k 0)) (d (n "rustc_version") (r "^0.1.7") (d #t) (k 1)))) (h "07nw1x3hvf08ngd1bw7krishzkl5i3a6yg94b922z8id8fyi2qd6")))

(define-public crate-extprim-1.0.1 (c (n "extprim") (v "1.0.1") (d (list (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.1.32") (d #t) (k 0)) (d (n "rand") (r "^0.3.14") (d #t) (k 0)) (d (n "rustc_version") (r "^0.1.7") (d #t) (k 1)))) (h "1wf27i7q6mnigf3pxrrz4wcwhciybhbfyq51irpjrligpsgfksrz")))

(define-public crate-extprim-1.1.0 (c (n "extprim") (v "1.1.0") (d (list (d (n "num-traits") (r "^0.1.32") (d #t) (k 0)) (d (n "rand") (r "^0.3.14") (o #t) (d #t) (k 0)) (d (n "rustc_version") (r "^0.1.7") (d #t) (k 1)))) (h "0xcvgvdg8klkm5s4qykqqs0fz6c5bncv9mb8x6gbksjix5h3lniq") (f (quote (("use-std" "rand") ("default" "use-std"))))))

(define-public crate-extprim-1.1.1 (c (n "extprim") (v "1.1.1") (d (list (d (n "num-traits") (r "^0.1.33") (d #t) (k 0)) (d (n "rand") (r "^0.3.14") (o #t) (d #t) (k 0)) (d (n "rustc_version") (r "^0.1.7") (d #t) (k 1)))) (h "1f122mhivgpq2h9shz26n3n0343a02jbxqmmap84qpgb87mkhrp1") (f (quote (("use-std" "rand") ("default" "use-std"))))))

(define-public crate-extprim-1.2.0 (c (n "extprim") (v "1.2.0") (d (list (d (n "num-traits") (r "^0.1.33") (d #t) (k 0)) (d (n "rand") (r "^0.3.14") (o #t) (d #t) (k 0)) (d (n "rustc_version") (r "^0.1.7") (d #t) (k 1)))) (h "1bfgi4gznz2hqvhm1hzn8bc3yf97wdrpdsp0fhaih8n0p8akrbyc") (f (quote (("use-std" "rand") ("default" "use-std"))))))

(define-public crate-extprim-1.2.1 (c (n "extprim") (v "1.2.1") (d (list (d (n "num-traits") (r "^0.1.36") (d #t) (k 0)) (d (n "rand") (r "^0.3.15") (o #t) (d #t) (k 0)) (d (n "rustc_version") (r "^0.1.7") (d #t) (k 1)))) (h "0f3wmhlrgsikihjssya1hsmb8n866m6di1j3rxgfdxbmwgi51smk") (f (quote (("use-std" "rand") ("default" "use-std"))))))

(define-public crate-extprim-1.2.2 (c (n "extprim") (v "1.2.2") (d (list (d (n "extprim_literals") (r "^2.0.0") (d #t) (k 2)) (d (n "num-traits") (r "^0.1.37") (d #t) (k 0)) (d (n "rand") (r "^0.3.15") (o #t) (d #t) (k 0)) (d (n "rustc_version") (r "^0.2.0") (d #t) (k 1)))) (h "049rm4vcnggh30gfxmhk60cymzilsyn6mkjb49mbbdj372zjvyfr") (f (quote (("use-std" "rand") ("default" "use-std"))))))

(define-public crate-extprim-1.3.0 (c (n "extprim") (v "1.3.0") (d (list (d (n "extprim_literals") (r "^2.0") (d #t) (k 2)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "rustc_version") (r "^0.2.0") (d #t) (k 1)))) (h "0kcdbkak4z0j8dhas57hv6306cjbwiv1yqz0l25wgdi3rdy0wckj") (f (quote (("use-std") ("default" "use-std" "rand"))))))

(define-public crate-extprim-1.4.0 (c (n "extprim") (v "1.4.0") (d (list (d (n "extprim_literals") (r "^2.0") (d #t) (k 2)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "rustc_version") (r "^0.2.0") (d #t) (k 1)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1iaiv4pfc0nnh5pwp52m4ji99ylk4wdavhvcc5f27sv8vfbrgrqx") (f (quote (("use-std") ("default" "use-std" "rand" "serde"))))))

(define-public crate-extprim-1.5.0 (c (n "extprim") (v "1.5.0") (d (list (d (n "extprim_literals") (r "^2.0") (d #t) (k 2)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "rustc_version") (r "^0.2.0") (d #t) (k 1)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "169yqhxpn8px45cv131in9ah0zjwynhdg485rk767653pjdnc63l") (f (quote (("use-std") ("default" "use-std" "rand" "serde"))))))

(define-public crate-extprim-1.5.1 (c (n "extprim") (v "1.5.1") (d (list (d (n "extprim_literals") (r "^2.0") (d #t) (k 2)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "rustc_version") (r "^0.2.0") (d #t) (k 1)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1ffryfhdf23rdi5w1kiv5ikb6f7m1fc60jlyf9bmr2m44kmvc2gb") (f (quote (("use-std") ("default" "use-std" "rand" "serde"))))))

(define-public crate-extprim-1.6.0 (c (n "extprim") (v "1.6.0") (d (list (d (n "extprim_literals") (r "^2.0") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "semver") (r "^0.9") (d #t) (k 1)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "01jjmbfigf2lpg5nfykkiv087xgzbcjlg7p2jy0glriz5daw4jq5") (f (quote (("use-std") ("default" "use-std" "rand" "serde"))))))

(define-public crate-extprim-1.7.0 (c (n "extprim") (v "1.7.0") (d (list (d (n "extprim_literals") (r "^2.0") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "semver") (r "^0.9") (d #t) (k 1)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1cagbgayqfmdw6zmci14ivs8cqdzn8k3jk9ymlyhnxj9qz81pfng") (f (quote (("use-std") ("default" "use-std" "rand" "serde"))))))

(define-public crate-extprim-1.7.1 (c (n "extprim") (v "1.7.1") (d (list (d (n "extprim_literals") (r "^2.0") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "semver") (r "^0.9") (d #t) (k 1)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_derive") (r ">=1.0.0, <=1.0.98") (d #t) (k 2)))) (h "18k69pxlvag5zqyp0nrrvfwpfnf8nmsv7mv0nhwj8d8wj5y3a6ib") (f (quote (("use-std") ("default" "use-std" "rand" "serde"))))))

