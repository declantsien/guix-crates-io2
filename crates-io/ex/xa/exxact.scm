(define-module (crates-io ex xa exxact) #:use-module (crates-io))

(define-public crate-exxact-0.1.0 (c (n "exxact") (v "0.1.0") (d (list (d (n "appendlist") (r "^1.4.0") (d #t) (k 0)) (d (n "fraction") (r "^0.14.0") (d #t) (k 0)) (d (n "lyn") (r "^0.1.0") (d #t) (k 0)) (d (n "num") (r "^0.4.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)) (d (n "ordered-float") (r "^4.2.0") (d #t) (k 0)) (d (n "prime_factorization") (r "^1.0.4") (d #t) (k 0)) (d (n "proptest") (r "^1.4.0") (d #t) (k 0)))) (h "0y9bdfvnnpqz5nxkb63ajvsxlhy1ab0vh9mgyl54v1s4d313cgln")))

