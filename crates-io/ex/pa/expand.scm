(define-module (crates-io ex pa expand) #:use-module (crates-io))

(define-public crate-expand-0.1.0 (c (n "expand") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "syn") (r "^1.0.46") (f (quote ("proc-macro" "parsing"))) (k 0)))) (h "0ca6vang0z7rmzhpwjylg8c7k1liv7i45amc9hzyhzyd9jjzxacs")))

(define-public crate-expand-0.1.1 (c (n "expand") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "syn") (r "^1.0.47") (f (quote ("proc-macro" "parsing"))) (k 0)))) (h "16sldafcyj1k13b1m0f1dqcg5j776lfgir02pbmsxbx3cj09rk85")))

(define-public crate-expand-0.1.2 (c (n "expand") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "syn") (r "^1.0.48") (f (quote ("proc-macro" "parsing"))) (k 0)))) (h "07izgfdni02xa0rix7vdnbfb1w0cdbr14z8jy4y5zlaqscm85z53")))

(define-public crate-expand-0.1.3 (c (n "expand") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.48") (f (quote ("proc-macro" "parsing"))) (k 0)))) (h "0dz16pghihw18vm39mgb5lwydpx52416kvy5frrzp8xv9lbjmgpy")))

(define-public crate-expand-0.2.0 (c (n "expand") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.48") (f (quote ("proc-macro" "parsing"))) (k 0)))) (h "1z0rd46lg2pmw7r40ycwm0c8cdhi52dk6vw114lj79zv40dx7mhc")))

(define-public crate-expand-0.2.1 (c (n "expand") (v "0.2.1") (d (list (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("parsing" "proc-macro"))) (k 0)))) (h "1g9y4nh2p0w38by3fgg05y5f1h00xkp7i4iyzf1n96vg0gfmpgw9")))

(define-public crate-expand-0.3.0 (c (n "expand") (v "0.3.0") (d (list (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.0") (f (quote ("parsing" "proc-macro"))) (k 0)))) (h "173zl3yc9dqq0g0z28sv3js95h74bf6nd78cjcyv56svkbskn1rx")))

