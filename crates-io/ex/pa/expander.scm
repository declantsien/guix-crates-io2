(define-module (crates-io ex pa expander) #:use-module (crates-io))

(define-public crate-expander-0.0.1 (c (n "expander") (v "0.0.1") (d (list (d (n "fs-err") (r "^2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)))) (h "1zvi65pzhrq1abnm7b5xaf0fbvs37bka92p8jyd15rv78ah0p2hv")))

(define-public crate-expander-0.0.2 (c (n "expander") (v "0.0.2") (d (list (d (n "fs-err") (r "^2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)))) (h "1rnmbn5q16a25l8wmidyh5s1x0jxyxvg7vxj8ry6w9vrhka51p3i")))

(define-public crate-expander-0.0.3 (c (n "expander") (v "0.0.3") (d (list (d (n "blake3") (r "^1.3") (d #t) (k 0)) (d (n "fs-err") (r "^2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)))) (h "1cmbkcifgcs6p30f8l28pdq1w5kc8yn9zdqjr1cwmh23swxqy4h3")))

(define-public crate-expander-0.0.4 (c (n "expander") (v "0.0.4") (d (list (d (n "blake3") (r "^1.3") (d #t) (k 0)) (d (n "fs-err") (r "^2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)))) (h "10aq0wj97gyghzpss710zb70dyjhq7iajkpzdybmyp2mbikw0657")))

(define-public crate-expander-0.0.5 (c (n "expander") (v "0.0.5") (d (list (d (n "blake2") (r "^0.10") (d #t) (k 0)) (d (n "fs-err") (r "^2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (o #t) (k 0)))) (h "1ajsnrmqnan85i0d1vvwf16zrrd4sxdiazm0xnvf8f4fkv1j37rh") (f (quote (("syndicate" "syn") ("default" "syndicate"))))))

(define-public crate-expander-0.0.6 (c (n "expander") (v "0.0.6") (d (list (d (n "blake2") (r "^0.10") (d #t) (k 0)) (d (n "fs-err") (r "^2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (o #t) (k 0)))) (h "1diapm121xym5px0k9mbdgrfwgwipqpx66iij0b3sg7iblm1hx1p") (f (quote (("syndicate" "syn") ("default" "syndicate"))))))

(define-public crate-expander-1.0.0-alpha.0 (c (n "expander") (v "1.0.0-alpha.0") (d (list (d (n "blake2") (r "^0.10") (d #t) (k 0)) (d (n "fs-err") (r "^2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (o #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits" "full"))) (d #t) (k 2)))) (h "08lr6lwasr4yv1c62pw2r29w3fy8127873zppjxdm9310dc2rg6p") (f (quote (("syndicate" "syn") ("default" "syndicate"))))))

(define-public crate-expander-1.0.0 (c (n "expander") (v "1.0.0") (d (list (d (n "blake2") (r "^0.10") (d #t) (k 0)) (d (n "fs-err") (r "^2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (o #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits" "full"))) (d #t) (k 2)))) (h "117d2qpn7i9l1m21pwm0sccg85c9mwb7m6ac5vwm71bja28k8q7k") (f (quote (("syndicate" "syn") ("default" "syndicate"))))))

(define-public crate-expander-2.0.0 (c (n "expander") (v "2.0.0") (d (list (d (n "blake2") (r "^0.10") (d #t) (k 0)) (d (n "fs-err") (r "^2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (o #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits" "full"))) (d #t) (k 2)))) (h "1xxhgyvs41vypqxrqpj6c6bcddcrqbv5wl8hdj392645rx4sg1jz") (f (quote (("syndicate" "syn") ("default" "syndicate"))))))

(define-public crate-expander-2.1.0 (c (n "expander") (v "2.1.0") (d (list (d (n "blake2") (r "^0.10") (d #t) (k 0)) (d (n "fs-err") (r "^2") (d #t) (k 0)) (d (n "prettier-please") (r "^0.2") (o #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (o #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits" "parsing" "full"))) (d #t) (k 2)))) (h "0kcdi583h1lpq7x2r2dmv170x9851jk4x5j759cz2dji0c13rs00") (f (quote (("syndicate" "syn") ("pretty" "prettier-please" "syn/parsing" "syn/full") ("default" "syndicate" "pretty"))))))

