(define-module (crates-io ex pa expand_any_lifetime) #:use-module (crates-io))

(define-public crate-expand_any_lifetime-0.1.0 (c (n "expand_any_lifetime") (v "0.1.0") (h "1bh8wrbdib41hpmnhy189gba5p51kjj2m14rx34a7fw9vvlpm1an")))

(define-public crate-expand_any_lifetime-0.1.1 (c (n "expand_any_lifetime") (v "0.1.1") (h "1ri546kb29qzb65zip4r2wzd156sakw3fya3766wbb8k6sjhj0ad") (r "1.54")))

(define-public crate-expand_any_lifetime-0.1.2 (c (n "expand_any_lifetime") (v "0.1.2") (h "14i5xajyk50swb55hiavq4f0kv7zfd0l09azc94lwf61l0p5zlh0") (r "1.54")))

