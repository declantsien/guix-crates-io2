(define-module (crates-io ex pa expanding_slice_rb) #:use-module (crates-io))

(define-public crate-expanding_slice_rb-0.1.0 (c (n "expanding_slice_rb") (v "0.1.0") (d (list (d (n "slice_ring_buf") (r "^0.2") (d #t) (k 0)))) (h "12pmml2qklgqin86ysicb8kv89830yaz38xvr935cqs41dsgxrj4")))

(define-public crate-expanding_slice_rb-0.1.1 (c (n "expanding_slice_rb") (v "0.1.1") (d (list (d (n "slice_ring_buf") (r "^0.2") (d #t) (k 0)))) (h "1vlcj6ljspq6ivwr8xh8gvz49ggwrf44z9chzfcygi2yq105670i")))

(define-public crate-expanding_slice_rb-0.1.2 (c (n "expanding_slice_rb") (v "0.1.2") (d (list (d (n "slice_ring_buf") (r "^0.2") (d #t) (k 0)))) (h "13r5fb6nb98mqlbds6qklsrp7rc8iw50aa3bmi60zlpqc6fdss6p")))

(define-public crate-expanding_slice_rb-0.1.3 (c (n "expanding_slice_rb") (v "0.1.3") (d (list (d (n "slice_ring_buf") (r "^0.2") (d #t) (k 0)))) (h "0ygylw0b716q389khgpwgkip5xxp3727gy3lj9wn11spsym5ham1")))

