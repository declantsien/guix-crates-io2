(define-module (crates-io ex pa expanduser) #:use-module (crates-io))

(define-public crate-expanduser-1.0.0 (c (n "expanduser") (v "1.0.0") (d (list (d (n "lazy_static") (r "^1.0.1") (d #t) (k 0)) (d (n "pwd") (r "^1.3.0") (d #t) (k 0)))) (h "1gkpc7942dyqa4jmrfb96r74abwr9ajs54z97pkq5r2r5zi256p8")))

(define-public crate-expanduser-1.1.0 (c (n "expanduser") (v "1.1.0") (d (list (d (n "dirs") (r "^1.0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.1") (d #t) (k 0)) (d (n "pwd") (r "^1.3.0") (d #t) (k 0)))) (h "13m2g4khgadi52afdb4zvx7r6dfgfar4j3hs2x1jyinnpkjf3nlc")))

(define-public crate-expanduser-1.2.0 (c (n "expanduser") (v "1.2.0") (d (list (d (n "dirs") (r "^1.0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.1") (d #t) (k 0)) (d (n "pwd") (r "^1.3.0") (d #t) (k 0)))) (h "0cy83f7f0k6d5bk2f582xw9bya6v857p6dlxpdbpjpd0bapx641q")))

(define-public crate-expanduser-1.2.1 (c (n "expanduser") (v "1.2.1") (d (list (d (n "dirs") (r "^1.0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.1") (d #t) (k 0)) (d (n "pwd") (r "^1.3.0") (d #t) (k 0)))) (h "15481l2pg6bxcvrkplgvidl18ap61i5k2r8j6zck7850vp4cissi")))

(define-public crate-expanduser-1.2.2 (c (n "expanduser") (v "1.2.2") (d (list (d (n "dirs") (r "^1.0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.1") (d #t) (k 0)) (d (n "pwd") (r "^1.3.0") (d #t) (k 0)))) (h "1h0blhxw7p8h37ly6flp7bb6gs2xdspxkvibdimxnmys6n9bgq0l")))

