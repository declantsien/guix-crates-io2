(define-module (crates-io ex pa expat-sys) #:use-module (crates-io))

(define-public crate-expat-sys-2.1.0-again (c (n "expat-sys") (v "2.1.0-again") (d (list (d (n "make-cmd") (r "^0.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "07zkjs8fihwkb1cqckf0ca3xy08akijd102wgs843y0926bn2dih") (y #t)))

(define-public crate-expat-sys-2.1.1-really.0 (c (n "expat-sys") (v "2.1.1-really.0") (d (list (d (n "make-cmd") (r "^0.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1cazycgsaqnikvndv98vd0jjxxh9vc46f7rbnylmvjvmgwjsw012") (y #t)))

(define-public crate-expat-sys-2.1.1 (c (n "expat-sys") (v "2.1.1") (d (list (d (n "make-cmd") (r "^0.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0f2hxjaxvvb29ndazhxx61lsab0k83drgyhq8rn4idgc9q136h85") (y #t)))

(define-public crate-expat-sys-2.1.2 (c (n "expat-sys") (v "2.1.2") (d (list (d (n "make-cmd") (r "^0.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0gijyhfrp4wh0czrs9lg8wxziafgp6zgs2kg2wgmghclhn1nzksc")))

(define-public crate-expat-sys-2.1.3 (c (n "expat-sys") (v "2.1.3") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "13nknkzyvwfz4cl0yj4hcjr1mkr9g462yb4cxd08q0b3fphrmmp7")))

(define-public crate-expat-sys-2.1.4 (c (n "expat-sys") (v "2.1.4") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1745q2hqgp01h9pz6vg3m2w4r7kv4iipqd4p3nwjhbd0m38nrwyf")))

(define-public crate-expat-sys-2.1.5 (c (n "expat-sys") (v "2.1.5") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1cfmismhjw7zxxi9pgzzb9pl5acxxllq1nr3n14qa27jfawwqw64")))

(define-public crate-expat-sys-2.1.6 (c (n "expat-sys") (v "2.1.6") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1yj5pqynds776ay8wg9mhi3hvna4fv7vf244yr1864r0i5r1k3v5") (l "expat")))

