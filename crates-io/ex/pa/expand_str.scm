(define-module (crates-io ex pa expand_str) #:use-module (crates-io))

(define-public crate-expand_str-0.1.0 (c (n "expand_str") (v "0.1.0") (h "17c9p67v5mmivara34azyq97fqq9n16sw43s4228aml2klbkamac") (f (quote (("env") ("default" "env"))))))

(define-public crate-expand_str-0.1.1 (c (n "expand_str") (v "0.1.1") (h "1r5032gdcwifksw2xrnls1v0pgx71n38qfacw9jwlks5pngvrgzp") (f (quote (("env") ("default" "env"))))))

