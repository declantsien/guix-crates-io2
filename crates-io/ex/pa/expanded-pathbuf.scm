(define-module (crates-io ex pa expanded-pathbuf) #:use-module (crates-io))

(define-public crate-expanded-pathbuf-0.1.0 (c (n "expanded-pathbuf") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "directories") (r "^4") (d #t) (k 2)) (d (n "shellexpand") (r "^2") (d #t) (k 0)))) (h "18sjzsmfm5dqqdipfw8l22d80acrz1nwcaaih7yi3gm0g8i95m4d")))

(define-public crate-expanded-pathbuf-0.1.1 (c (n "expanded-pathbuf") (v "0.1.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "directories") (r "^4") (d #t) (k 2)) (d (n "shellexpand") (r "^2") (d #t) (k 0)))) (h "0p5mf13bjnvvk1f2sv82kdapaars3sl0w11zpxbh3nbdhn4wg2za")))

(define-public crate-expanded-pathbuf-0.1.2 (c (n "expanded-pathbuf") (v "0.1.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "shellexpand") (r "^3") (d #t) (k 0)) (d (n "directories") (r "^5") (d #t) (k 2)))) (h "03plk4bn16k9h1i97qn25vl7xgrxfgfncy5yq31g00dp9mzym1sy")))

