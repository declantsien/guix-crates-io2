(define-module (crates-io ex pa expand_array) #:use-module (crates-io))

(define-public crate-expand_array-0.1.0 (c (n "expand_array") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.82") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.61") (f (quote ("full"))) (d #t) (k 0)))) (h "18i2ixkx0pk7h6gj784aljixv0q3y6vfkpm8v4ksbgv84ifjp0y0")))

(define-public crate-expand_array-0.2.0 (c (n "expand_array") (v "0.2.0") (h "05i7dq7g42wpf0dl7jpn4vcv3cbjksg3h0m6vicnqv4r4qcxh9bw")))

