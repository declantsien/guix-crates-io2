(define-module (crates-io ex pa expansion) #:use-module (crates-io))

(define-public crate-expansion-0.0.0 (c (n "expansion") (v "0.0.0") (h "1505lmz1czl9s8bss75dxq82ypgcg9hw9haf89pvgwbmkna7dkhw")))

(define-public crate-expansion-0.0.1 (c (n "expansion") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0yzhibbjs00rs0x7l1pwprkibcz8kf2niy0s5bwvz0hhfdzq9jfx")))

(define-public crate-expansion-0.0.2 (c (n "expansion") (v "0.0.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0z48mk0x3x5jk42r4h7wxkp8zzf1p54npnvk1ymj33dya6afzs8h")))

(define-public crate-expansion-0.0.3 (c (n "expansion") (v "0.0.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0wm2z4a42jy9gqmw4vmx9b2k4nhzwfjhka0sbp6lrjfbwqk1mbp1")))

(define-public crate-expansion-0.0.4 (c (n "expansion") (v "0.0.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "05nj1asx24lwm7nbpshj1r38vypdg789kqdr7sdn6rpwf9kj0zfl")))

