(define-module (crates-io ex it exit-no-std) #:use-module (crates-io))

(define-public crate-exit-no-std-0.1.0 (c (n "exit-no-std") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.126") (t "cfg(not(windows))") (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("processthreadsapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0pm33kx931ch8ij09vg9j0ivv9ljjja0ipnm2xfxnndj9v3qna9x") (r "1.59")))

(define-public crate-exit-no-std-0.1.1 (c (n "exit-no-std") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.126") (t "cfg(all(not(dos), not(windows)))") (k 0)) (d (n "pc-ints") (r "^0.0.2") (d #t) (t "cfg(dos)") (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("processthreadsapi"))) (d #t) (t "cfg(all(not(dos), windows))") (k 0)))) (h "074n3z26a84p7w4pp8bjl4za0rg4ql3lfmpy4gpvzb091kzq3pgp") (r "1.59")))

(define-public crate-exit-no-std-0.1.2 (c (n "exit-no-std") (v "0.1.2") (d (list (d (n "libc") (r "^0.2.126") (t "cfg(all(not(target_os = \"dos\"), not(windows)))") (k 0)) (d (n "pc-ints") (r "^0.1.0") (d #t) (t "cfg(target_os = \"dos\")") (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("processthreadsapi"))) (d #t) (t "cfg(all(not(target_os = \"dos\"), windows))") (k 0)))) (h "1jbajnzjkn2l4hhm8nzib4sfxx4ghqar5n7hk42fpjs83pbx97gk") (r "1.59")))

(define-public crate-exit-no-std-0.1.3 (c (n "exit-no-std") (v "0.1.3") (d (list (d (n "libc") (r "^0.2.126") (t "cfg(all(not(target_os = \"dos\"), not(windows)))") (k 0)) (d (n "pc-ints") (r "^0.1.4") (d #t) (t "cfg(target_os = \"dos\")") (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("processthreadsapi"))) (d #t) (t "cfg(all(not(target_os = \"dos\"), windows))") (k 0)))) (h "168x2rfs88kcj37ani0qa99y78znsrssv25a2qcqrizscz68qq2a") (r "1.59")))

(define-public crate-exit-no-std-0.1.4 (c (n "exit-no-std") (v "0.1.4") (d (list (d (n "libc") (r "^0.2.126") (t "cfg(all(not(target_os = \"dos\"), not(windows)))") (k 0)) (d (n "pc-ints") (r "^0.2.0") (d #t) (t "cfg(target_os = \"dos\")") (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("processthreadsapi"))) (d #t) (t "cfg(all(not(target_os = \"dos\"), windows))") (k 0)))) (h "12fiwk93x5psmqfzvf5wfpk1g07gy0gra1s7b86z3adxzky6yq1a") (r "1.59")))

(define-public crate-exit-no-std-0.2.0 (c (n "exit-no-std") (v "0.2.0") (d (list (d (n "libc") (r "^0.2.126") (t "cfg(all(not(target_os = \"dos\"), not(windows)))") (k 0)) (d (n "pc-ints") (r "^0.3.0") (d #t) (t "cfg(target_os = \"dos\")") (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("processthreadsapi"))) (d #t) (t "cfg(all(not(target_os = \"dos\"), windows))") (k 0)))) (h "074b50znhsr7ycky1g9vak31bp2awwapxvr5n3rnjjr9rsh7bvvx")))

