(define-module (crates-io ex it exit-stack) #:use-module (crates-io))

(define-public crate-exit-stack-0.1.0 (c (n "exit-stack") (v "0.1.0") (d (list (d (n "pin-utils") (r "^0.1") (d #t) (k 2)) (d (n "static-alloc") (r "^0.2.1") (d #t) (k 0)))) (h "1wq8iwk4s186w3292jrgvcmvyxfd5id07wa1bivgihxxcw03sdqz")))

