(define-module (crates-io ex it exitcode) #:use-module (crates-io))

(define-public crate-exitcode-1.0.0 (c (n "exitcode") (v "1.0.0") (h "150a8y6jm5a1i6qvgl844905wc8nx8fvbkam9xdlg900d8hj1f8b")))

(define-public crate-exitcode-1.0.1 (c (n "exitcode") (v "1.0.1") (h "0rk57jdq7rnbcrl5qlymkz57mq2znd3rwfzkwkr7xl6p276bza4w")))

(define-public crate-exitcode-1.1.0 (c (n "exitcode") (v "1.1.0") (h "0i4d1m0l1xaaylma1s9zmnzdk85599qagr766yaqf833v5hadaml")))

(define-public crate-exitcode-1.1.1 (c (n "exitcode") (v "1.1.1") (h "0c2ivadsgykdx5v170qch747vqhhnizvwzry7q2qpzimyasqfmdr")))

(define-public crate-exitcode-1.1.2 (c (n "exitcode") (v "1.1.2") (h "14x1pgwx86x1gfc5zqgj04akr9pzg14w75d9cblc49vhnij3g1fy")))

