(define-module (crates-io ex it exit) #:use-module (crates-io))

(define-public crate-exit-0.1.0 (c (n "exit") (v "0.1.0") (h "1cf1sn7pccbpr10n5zys5f7qnknafz48g5gplhbvy3r8g1ma7dmz")))

(define-public crate-exit-0.2.0 (c (n "exit") (v "0.2.0") (h "0bgh8p8lr2y4xgmb6259lqv3y3qqzgv9z5bfvl4xa3yj4ibzyb63")))

