(define-module (crates-io ex it exitfailure) #:use-module (crates-io))

(define-public crate-exitfailure-0.1.0 (c (n "exitfailure") (v "0.1.0") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)))) (h "0n1r9g8kx6vm7cwxqa9s77sdv0i6whgdvmfnb1j6jx0q3qryfixx")))

(define-public crate-exitfailure-0.2.0 (c (n "exitfailure") (v "0.2.0") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)))) (h "0kc3ih4zd8vp2ihi8jb8c9jq5v3zblbkjlsgdrsblxmxy19h7ldr")))

(define-public crate-exitfailure-0.2.1 (c (n "exitfailure") (v "0.2.1") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)))) (h "0lhx4fm2wqxn5z9a6pq27q7njwgrrv1y7anp2x4902nlmah2yjpp")))

(define-public crate-exitfailure-0.2.2 (c (n "exitfailure") (v "0.2.2") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)))) (h "0gspz756qvqrp4d8xp9fhx6i50hfcrd4kqdpv4xqnhfa9l9q2lm4")))

(define-public crate-exitfailure-0.2.3 (c (n "exitfailure") (v "0.2.3") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)))) (h "08amp3zyh7g0mscj0a4adjv0ns025vgkl0w7mpvn52d6w935kcpv")))

(define-public crate-exitfailure-0.3.0 (c (n "exitfailure") (v "0.3.0") (d (list (d (n "assert_cli") (r "^0.5") (d #t) (k 2)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)))) (h "0gg2w38g5gslpsdi6jcy9v51zpnpawpqh2m234zjiyiybjyxxzh5")))

(define-public crate-exitfailure-0.4.0 (c (n "exitfailure") (v "0.4.0") (d (list (d (n "assert_cli") (r "^0.6") (d #t) (k 2)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)))) (h "13n9g0idgqwmq2zbrrnvddrgiz29a59kbkqnicrxw14akvzag9lz")))

(define-public crate-exitfailure-0.4.1 (c (n "exitfailure") (v "0.4.1") (d (list (d (n "assert_cli") (r "^0.6") (d #t) (k 2)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)))) (h "0iy6skfkybsgq3xfpj6rayj922svbhrpw1nblb5igfbp5higzs44")))

(define-public crate-exitfailure-0.5.0 (c (n "exitfailure") (v "0.5.0") (d (list (d (n "assert_cmd") (r "^0.6.0") (d #t) (k 2)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "predicates") (r "^0.5.0") (d #t) (k 2)))) (h "1nlk4q6vmg9ybpwq2i5qs60asrq6qkv3cln4jrsgd0cs3brhymzc")))

(define-public crate-exitfailure-0.5.1 (c (n "exitfailure") (v "0.5.1") (d (list (d (n "assert_cmd") (r "^0.9.0") (d #t) (k 2)) (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "predicates") (r "^0.9.0") (d #t) (k 2)))) (h "0585wix3b3pjjj90fkqj9x4ar46d24x82k8rdin3czzk5a1vvx9g")))

