(define-module (crates-io ex it exit-future) #:use-module (crates-io))

(define-public crate-exit-future-0.1.0 (c (n "exit-future") (v "0.1.0") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.4") (d #t) (k 0)))) (h "1dwxz93lz0idvw1bjxnzscdnyca29yxdlvyxykx3inc31wbfqc3c")))

(define-public crate-exit-future-0.1.1 (c (n "exit-future") (v "0.1.1") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.4") (d #t) (k 0)))) (h "0lp4hlpfjfjj7sd61l5695wy6x6y45cs5zpw314k1yhqkll19c9y")))

(define-public crate-exit-future-0.1.2 (c (n "exit-future") (v "0.1.2") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.4") (d #t) (k 0)))) (h "0v9i5z03x9ffwrbjwhgrdj8y5xn9kkhyqb91nwniii38xxnbb9ws")))

(define-public crate-exit-future-0.1.3 (c (n "exit-future") (v "0.1.3") (d (list (d (n "futures") (r "^0.1.17") (d #t) (k 0)) (d (n "parking_lot") (r "^0.6") (d #t) (k 0)))) (h "0766aq6s541kfzh8ixj9fy2rad748dsj6zc6pcmsk0csx449nmc7")))

(define-public crate-exit-future-0.1.4 (c (n "exit-future") (v "0.1.4") (d (list (d (n "futures") (r "^0.1.25") (d #t) (k 0)) (d (n "parking_lot") (r "^0.7.1") (d #t) (k 0)))) (h "0j4gbq08da3cjy1zzh83sgcjnfjx3n7yqd3z1rkirqrq3r23y0fq")))

(define-public crate-exit-future-0.2.0 (c (n "exit-future") (v "0.2.0") (d (list (d (n "futures") (r "^0.3.1") (d #t) (k 0)))) (h "1i88800r9kc3xf4319dilzflkwvhvmplsiljapqk6knn6cc2ygz4")))

