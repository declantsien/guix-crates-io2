(define-module (crates-io ex _e ex_em_ell) #:use-module (crates-io))

(define-public crate-ex_em_ell-0.1.0 (c (n "ex_em_ell") (v "0.1.0") (d (list (d (n "ex_em_ell_derive") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "insta") (r "^1.34.0") (f (quote ("glob"))) (d #t) (k 2)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.19") (d #t) (k 0)))) (h "1lb3yzjiakqcwxw6nrkxn69x1cq6zs5drz2z8pbmddkclc0xbla4") (f (quote (("derive" "ex_em_ell_derive") ("default" "derive"))))))

(define-public crate-ex_em_ell-0.2.0 (c (n "ex_em_ell") (v "0.2.0") (d (list (d (n "ex_em_ell_derive") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "insta") (r "^1.36.1") (f (quote ("glob"))) (d #t) (k 2)) (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.19") (d #t) (k 0)))) (h "064c15p7hbf8ia2z83zndzkwk2q49lw7h1wnfdf06874l73gfpj4") (f (quote (("derive" "ex_em_ell_derive") ("default" "derive"))))))

(define-public crate-ex_em_ell-0.3.0 (c (n "ex_em_ell") (v "0.3.0") (d (list (d (n "ex_em_ell_derive") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "insta") (r "^1.36.1") (f (quote ("glob"))) (d #t) (k 2)) (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.19") (d #t) (k 0)))) (h "0hpnhirc5g9dwxdfsckid9a3ycrsy1kp4mn1jqsqfbw6ldb4pxvy") (f (quote (("derive" "ex_em_ell_derive") ("default" "derive"))))))

