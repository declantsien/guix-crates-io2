(define-module (crates-io ex pe expectest) #:use-module (crates-io))

(define-public crate-expectest-0.1.0 (c (n "expectest") (v "0.1.0") (h "0g0040k3f6lk0zj3i7abvxrjxdpii6z0cdb13wfdcr9gg9h3d98x")))

(define-public crate-expectest-0.1.1 (c (n "expectest") (v "0.1.1") (h "0asplxhhvjskx3c4jprgjcgjqjcd2ss09q2w03cg44qghbzfdxwr")))

(define-public crate-expectest-0.1.2 (c (n "expectest") (v "0.1.2") (h "1pl2swb4nrn1gsmsm06mqkpn0jm0cjx1jjk77s1hji1xzhpyq648")))

(define-public crate-expectest-0.1.3 (c (n "expectest") (v "0.1.3") (h "15i63srfcm23bxl7dsiw4asbgkxm5fisj4p4xdk9qvw74sc7nsak")))

(define-public crate-expectest-0.1.4 (c (n "expectest") (v "0.1.4") (d (list (d (n "num") (r "*") (d #t) (k 0)))) (h "159gkq13ka7ssj77n2dlva8xx2kpb8khg8pig8hjaqqvjhchvi6n")))

(define-public crate-expectest-0.1.5 (c (n "expectest") (v "0.1.5") (d (list (d (n "num") (r "*") (d #t) (k 0)))) (h "19k9mpngspkrdx3qlrgmzzqq3wb69dn9wxy87cn6ky362qdy944c")))

(define-public crate-expectest-0.1.6 (c (n "expectest") (v "0.1.6") (d (list (d (n "num") (r "*") (d #t) (k 0)))) (h "0kylxzsa7q9mpjnsm0sjqxrkcfwhk9w43pbz89lkzisal9cnqzr9")))

(define-public crate-expectest-0.1.7 (c (n "expectest") (v "0.1.7") (d (list (d (n "num") (r "*") (d #t) (k 0)))) (h "1cgp00zdadm08n2xzfnwgnzvkfz07xl9yrjk5cjkgzqgij6rmql9")))

(define-public crate-expectest-0.1.8 (c (n "expectest") (v "0.1.8") (d (list (d (n "num") (r "0.1.*") (d #t) (k 0)))) (h "072kwpq08w68ab4yiyy1vx56ajz2bngss84d65x0yywwmmdzxyba")))

(define-public crate-expectest-0.2.0 (c (n "expectest") (v "0.2.0") (d (list (d (n "num") (r "0.1.*") (d #t) (k 0)))) (h "0haqqcb6g4fmcj0hrzhnf7s1933csl3ymgrlvv92yv02czjdf86z")))

(define-public crate-expectest-0.2.1 (c (n "expectest") (v "0.2.1") (d (list (d (n "num") (r "0.1.*") (d #t) (k 0)))) (h "165lrn3rsrjj0apkh2l3zzrlirq6axsxyhn2c73fc4930xnmgwxk")))

(define-public crate-expectest-0.2.2 (c (n "expectest") (v "0.2.2") (d (list (d (n "num") (r "~0.1.25") (k 0)))) (h "13qyh295vh2slqd9zcj3gi4izqfr3a1r24p6836gg6imwdmdvk57")))

(define-public crate-expectest-0.2.3 (c (n "expectest") (v "0.2.3") (d (list (d (n "num") (r "~0.1.25") (k 0)))) (h "0aalnxvb4acg36frmi7nrpxh4150p2wpriw93qcyz174ghgrgxra")))

(define-public crate-expectest-0.2.4 (c (n "expectest") (v "0.2.4") (d (list (d (n "num") (r "~0.1.27") (k 0)))) (h "1agr72bff9sizxcd4c5xf668g7cs3zqbxk90n71fhiy55a9palb9")))

(define-public crate-expectest-0.2.5 (c (n "expectest") (v "0.2.5") (d (list (d (n "num") (r "~0.1.27") (k 0)))) (h "0bnc037gn7n042v2qwacy3768zjsza6nvpdd4n2i6c0zcdlpi3ip")))

(define-public crate-expectest-0.2.6 (c (n "expectest") (v "0.2.6") (d (list (d (n "num") (r "~0.1.27") (k 0)))) (h "050gf23ilzzxxx088m0dy9mzzxrb90bsbgvyfl676mnyvnmy2a9r")))

(define-public crate-expectest-0.3.0 (c (n "expectest") (v "0.3.0") (d (list (d (n "num") (r "~0.1.27") (k 0)))) (h "0sdnrw46fizhwrcd9q462lm24k0qmby2sk97i63698r79v4jsp9b")))

(define-public crate-expectest-0.3.1 (c (n "expectest") (v "0.3.1") (d (list (d (n "num") (r "~0.1.27") (k 0)))) (h "18z01s5h8iwgykmlxlzcrdxpmsggi3q5hy93cd8didl67razqlh4")))

(define-public crate-expectest-0.4.0 (c (n "expectest") (v "0.4.0") (d (list (d (n "num") (r "^0.1.28") (k 0)))) (h "153y88h120jgd0r85b902gjbzkkhxyhir1i8sr8pg0a6jsz9604s")))

(define-public crate-expectest-0.4.1 (c (n "expectest") (v "0.4.1") (d (list (d (n "num") (r "^0.1.28") (k 0)))) (h "1s3kcmfk1cy0k1j7q3xih1ayrphzy9hrnzwl3icwn2lcjh5wr7qw")))

(define-public crate-expectest-0.5.1 (c (n "expectest") (v "0.5.1") (d (list (d (n "num-traits") (r "^0.1.32") (d #t) (k 0)) (d (n "skeptic") (r "^0.4.0") (d #t) (k 1)) (d (n "skeptic") (r "^0.4.0") (d #t) (k 2)))) (h "0w1brkmqn041sj6lwwxxximjd4c8pj9sapp3ldjgd4d7p5h9bn2h")))

(define-public crate-expectest-0.6.0 (c (n "expectest") (v "0.6.0") (d (list (d (n "num-traits") (r "^0.1.32") (d #t) (k 0)) (d (n "skeptic") (r "^0.5.0") (d #t) (k 1)) (d (n "skeptic") (r "^0.5.0") (d #t) (k 2)))) (h "1gzry69zg7frjqj2wmj0amaxij0lnpvs5wmwzb3zs80swrwa6ppy") (f (quote (("nightly"))))))

(define-public crate-expectest-0.7.0 (c (n "expectest") (v "0.7.0") (d (list (d (n "num-traits") (r "^0.1.36") (d #t) (k 0)) (d (n "skeptic") (r "^0.6.1") (d #t) (k 1)) (d (n "skeptic") (r "^0.6.1") (d #t) (k 2)))) (h "01yjy61rc5594hb0wlkyy4j6fphwy1s0w9p29xm3w88h02ix7azp") (f (quote (("nightly"))))))

(define-public crate-expectest-0.8.0 (c (n "expectest") (v "0.8.0") (d (list (d (n "num-traits") (r "^0.1.40") (d #t) (k 0)))) (h "0hmpdkc2cj5gps6ypjdzl38k75ym75k313nhrv063cksmwg0viyx") (f (quote (("nightly"))))))

(define-public crate-expectest-0.9.1 (c (n "expectest") (v "0.9.1") (d (list (d (n "num-traits") (r "^0.1.40") (d #t) (k 0)))) (h "0m8dskip5rsk62s2f2mh2fy9xvb8mms21c9fcshk2kzvlywwgfr1") (f (quote (("nightly"))))))

(define-public crate-expectest-0.9.2 (c (n "expectest") (v "0.9.2") (d (list (d (n "num-traits") (r "^0.1.40") (d #t) (k 0)))) (h "0f24q2a53x7sfmmrqjbwbk7pahzwkpd829fcr023kb7q5xnd6z4g") (f (quote (("nightly"))))))

(define-public crate-expectest-0.10.0 (c (n "expectest") (v "0.10.0") (d (list (d (n "num-traits") (r "^0.1.40") (d #t) (k 0)))) (h "0vwai8z7yakyl653wn9nbddipzadvy9pqvf75ds3kwwmsjxfq5pk") (f (quote (("nightly"))))))

(define-public crate-expectest-0.11.0 (c (n "expectest") (v "0.11.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "00dv47irmsyq7brzjhz4xns3p722gm98zp39h9hq2mrzd5marpgq") (f (quote (("nightly"))))))

(define-public crate-expectest-0.12.0 (c (n "expectest") (v "0.12.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0y290ak3q5l8l8ajg00mqx1lx9f1pagk6ckmplzibf5ach5pr0bq") (f (quote (("nightly"))))))

