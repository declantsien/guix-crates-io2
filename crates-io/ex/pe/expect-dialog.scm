(define-module (crates-io ex pe expect-dialog) #:use-module (crates-io))

(define-public crate-expect-dialog-1.0.0 (c (n "expect-dialog") (v "1.0.0") (d (list (d (n "native-dialog") (r "^0.6.4") (d #t) (k 0)))) (h "050k89zypglzg50dvjj91bwzjphp1qjjqs9cyb553k4wgzb0bl4j")))

(define-public crate-expect-dialog-1.0.1 (c (n "expect-dialog") (v "1.0.1") (d (list (d (n "native-dialog") (r "^0.6.4") (d #t) (k 0)))) (h "174dxfwpax0h32bc7vn471cq246im32nkgh96l7p9ibwd11fkzrc")))

