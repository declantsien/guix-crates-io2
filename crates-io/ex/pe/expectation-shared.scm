(define-module (crates-io ex pe expectation-shared) #:use-module (crates-io))

(define-public crate-expectation-shared-0.1.0 (c (n "expectation-shared") (v "0.1.0") (d (list (d (n "serde") (r "1.*.*") (d #t) (k 0)) (d (n "serde_derive") (r "1.*.*") (d #t) (k 0)) (d (n "walkdir") (r "^2.2.0") (d #t) (k 0)))) (h "0dfpjdn66hjibrg6wa7l559sq4nlzv54gv3k97rv4gqgdiwv7wvz")))

