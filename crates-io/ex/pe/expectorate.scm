(define-module (crates-io ex pe expectorate) #:use-module (crates-io))

(define-public crate-expectorate-1.0.0 (c (n "expectorate") (v "1.0.0") (d (list (d (n "difference") (r "^2.0.0") (d #t) (k 0)) (d (n "newline-converter") (r "^0.1.0") (d #t) (k 0)))) (h "056804cv67078j9dafgfflsk5xwk3hz9vp5fc68mav6nfr4fn9ar")))

(define-public crate-expectorate-1.0.1 (c (n "expectorate") (v "1.0.1") (d (list (d (n "difference") (r "^2.0.0") (d #t) (k 0)) (d (n "newline-converter") (r "^0.1.0") (d #t) (k 0)))) (h "0z9pgfi780qfhb4kfbbs9nf91f07nf4m2f2zsxzz6bnpdpwgpyrs")))

(define-public crate-expectorate-1.0.2 (c (n "expectorate") (v "1.0.2") (d (list (d (n "difference") (r "^2.0.0") (d #t) (k 0)) (d (n "newline-converter") (r "^0.1.0") (d #t) (k 0)))) (h "130x1fcd3xxjr080jkdhak0mn57qsah3x2clpamvq8vpw69vj573")))

(define-public crate-expectorate-2.0.0 (c (n "expectorate") (v "2.0.0") (d (list (d (n "difference") (r "^2.0.0") (d #t) (k 0)) (d (n "newline-converter") (r "^0.1.0") (d #t) (k 0)))) (h "0l1spf29hz2z36dcfq926s1y07lhsh581sbi1jaxy0hnydpk2qmq") (y #t)))

(define-public crate-expectorate-2.0.1 (c (n "expectorate") (v "2.0.1") (d (list (d (n "difference") (r "^2.0.0") (d #t) (k 0)) (d (n "newline-converter") (r "^0.1.0") (d #t) (k 0)))) (h "0bg1s0w72grgb4rbbj99by6w4jpbbvdcal7dlxr0bb0gg31gbafn") (y #t)))

(define-public crate-expectorate-1.0.3 (c (n "expectorate") (v "1.0.3") (d (list (d (n "difference") (r "^2.0.0") (d #t) (k 0)) (d (n "newline-converter") (r "^0.1.0") (d #t) (k 0)))) (h "1l7l0i16673ymj2pl7zgbib23viv0xbvwxk6m3y84si14gms3iqb")))

(define-public crate-expectorate-1.0.4 (c (n "expectorate") (v "1.0.4") (d (list (d (n "difference") (r "^2.0.0") (d #t) (k 0)) (d (n "newline-converter") (r "^0.2.0") (d #t) (k 0)))) (h "0zp2bqm3avjrl6pb4vww8wwhavaxbgvnqjxbypndngd1m0g60kc0")))

(define-public crate-expectorate-1.0.5 (c (n "expectorate") (v "1.0.5") (d (list (d (n "console") (r "^0.15.0") (d #t) (k 0)) (d (n "newline-converter") (r "^0.2.0") (d #t) (k 0)) (d (n "similar") (r "^2.1.0") (d #t) (k 0)))) (h "17l3rvlm8809ssv4zbc3yif7173vmm1ndyrr38rg0k5mg1qlb6sx")))

(define-public crate-expectorate-1.0.6 (c (n "expectorate") (v "1.0.6") (d (list (d (n "console") (r "^0.15.3") (d #t) (k 0)) (d (n "newline-converter") (r "^0.2.2") (d #t) (k 0)) (d (n "similar") (r "^2.2.1") (d #t) (k 0)))) (h "12r9q6v61k0pyv993ifyvh24v0rvwb1g3g8968caiq069cs8793l")))

(define-public crate-expectorate-1.0.7 (c (n "expectorate") (v "1.0.7") (d (list (d (n "console") (r "^0.15.5") (d #t) (k 0)) (d (n "newline-converter") (r "^0.3.0") (d #t) (k 0)) (d (n "similar") (r "^2.2.1") (d #t) (k 0)))) (h "1485rck8ii03j8xq4bv8qhg7rlm57ypxay3gsqsshf3hsnibc2ki")))

(define-public crate-expectorate-1.1.0 (c (n "expectorate") (v "1.1.0") (d (list (d (n "console") (r "^0.15.7") (d #t) (k 0)) (d (n "newline-converter") (r "^0.3.0") (d #t) (k 0)) (d (n "predicates") (r "^3.0.4") (o #t) (d #t) (k 0)) (d (n "similar") (r "^2.2.1") (d #t) (k 0)))) (h "0pypl45889q7psak1j74sir147y3178prwvmwxx798nzbfr1jvyy") (s 2) (e (quote (("predicates" "dep:predicates"))))))

