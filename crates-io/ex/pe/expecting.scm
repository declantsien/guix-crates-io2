(define-module (crates-io ex pe expecting) #:use-module (crates-io))

(define-public crate-expecting-0.1.0-alpha.0 (c (n "expecting") (v "0.1.0-alpha.0") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)))) (h "1r2q0hfm99is0f50kj3xhcv4gms8s6r7z8gp6lzjhw9ci29azz7d")))

(define-public crate-expecting-0.1.0 (c (n "expecting") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)))) (h "06fz8ss33np4rhgksgi5hgx2h8mq7bqbib9ddpqij4vyz88ih2z3")))

(define-public crate-expecting-0.1.1 (c (n "expecting") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)))) (h "0xbf21hi7kbx290vi0da5fax6w1q4jraxwhjyixg34irx9a0j6f6")))

(define-public crate-expecting-0.1.2 (c (n "expecting") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)))) (h "15lh5rjn1w2hp0nsypwnq7pkv28zi2bp57836jgrpgybr3jspp2j")))

(define-public crate-expecting-0.2.0 (c (n "expecting") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)))) (h "102w58rly084j7g0lyi17hanlhpkqp7b3jlg9ggbh16qalvmcfbd")))

(define-public crate-expecting-0.3.0 (c (n "expecting") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)))) (h "1q95fna6jp5fxfmj276x90085yip47mwrzn5yp4k5yvr71difwxz")))

(define-public crate-expecting-0.4.0 (c (n "expecting") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)))) (h "03lx2sgffcgh1ajsx83rlsswvg70nwj3bcw3y8f9mvmfzbb4whrc")))

(define-public crate-expecting-0.5.0 (c (n "expecting") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)))) (h "13d1rvziwrv5xwj1gqgg2i22ndkk4h7kiin2ihqy7fangwz8r72p")))

(define-public crate-expecting-0.6.0 (c (n "expecting") (v "0.6.0") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)))) (h "09cqpacf03injraq1jzhhirs4c38pn13gpdq67scl1p6k92fyrjj")))

