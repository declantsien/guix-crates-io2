(define-module (crates-io ex pe experiment-cargo-prerelease-semver) #:use-module (crates-io))

(define-public crate-experiment-cargo-prerelease-semver-1.0.0-beta (c (n "experiment-cargo-prerelease-semver") (v "1.0.0-beta") (h "03nfk2qfk7g74pl3v442akd8g612r2rsdhzmqqz5m8bdj8898xsf")))

(define-public crate-experiment-cargo-prerelease-semver-1.0.0 (c (n "experiment-cargo-prerelease-semver") (v "1.0.0") (h "13r6makl44iaj7k5k1a5gy2j3gqwbnf739dkyha2nv6jr8w69fvq")))

