(define-module (crates-io ex pe experimental-test) #:use-module (crates-io))

(define-public crate-experimental-test-0.1.0 (c (n "experimental-test") (v "0.1.0") (h "0lrm1r6jz8w5wsp4kjmb7wfxpyqcjcs8nrqml0ms4lnda5ix07hj")))

(define-public crate-experimental-test-0.1.1 (c (n "experimental-test") (v "0.1.1") (h "088hfh6pw1cgxyndpl3h120iwfgpfq0nz645sjmkba6is6d74msx")))

(define-public crate-experimental-test-0.1.2 (c (n "experimental-test") (v "0.1.2") (h "11w7mrq3z5bkwb7rkfd4hjjh5cv6v61v324vzkf0iwdrlw9k00pa")))

(define-public crate-experimental-test-0.1.3 (c (n "experimental-test") (v "0.1.3") (h "0p4g9c0p6hd82qk5nibpbr7lvdqgyvz82w43q8v96f7fqn51in2y")))

(define-public crate-experimental-test-0.1.4 (c (n "experimental-test") (v "0.1.4") (h "0p989917qz3cd4lb6a58mjvm3bmx1ssxkvd4ib7lq387padiw9y0")))

(define-public crate-experimental-test-0.1.5 (c (n "experimental-test") (v "0.1.5") (h "1zfz08yf4d5f2nfmn9fnf839346hv0r1hj8fha88s65r3drjv4yy")))

(define-public crate-experimental-test-0.1.6 (c (n "experimental-test") (v "0.1.6") (h "18byqb2g9pvyl9nhh0w1psg7bjggxk3q46gqlfxvfqmmns1ik6w8")))

(define-public crate-experimental-test-0.1.7 (c (n "experimental-test") (v "0.1.7") (h "142by5ka0rhbnx9sws53410ji5f4d5g12fv18dlxc84lhws8x71s")))

