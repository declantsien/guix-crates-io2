(define-module (crates-io ex pe experi-codec-derive) #:use-module (crates-io))

(define-public crate-experi-codec-derive-3.1.0 (c (n "experi-codec-derive") (v "3.1.0") (d (list (d (n "proc-macro-crate") (r "^0.1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15.26") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "1xh11wx4chc9fyphw9zr87pgz1d2lvfg30p921fx0pcckrwlf445") (f (quote (("std") ("default" "std")))) (y #t)))

