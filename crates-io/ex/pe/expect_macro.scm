(define-module (crates-io ex pe expect_macro) #:use-module (crates-io))

(define-public crate-expect_macro-0.1.0 (c (n "expect_macro") (v "0.1.0") (h "0wgqnggblj11b62b7x7sf602107ipr4vmd4wfcmh8nix3pwiilhq")))

(define-public crate-expect_macro-0.1.1 (c (n "expect_macro") (v "0.1.1") (h "028y7x68m5lkyfswhywkdqr98q1hlqd4w7j85x36wnhjx7vb7882")))

(define-public crate-expect_macro-0.2.0 (c (n "expect_macro") (v "0.2.0") (h "04c1lg61xlr4s0d6jna0kzm51g7jvx0qlwn867xsk9pf803la03i")))

(define-public crate-expect_macro-0.2.1 (c (n "expect_macro") (v "0.2.1") (h "04r3zcq1grpql6232q8md0kwjil23l6gi5vj30xzq8f8f5lkcxri")))

