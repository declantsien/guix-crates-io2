(define-module (crates-io ex pe experiment-codec-derive) #:use-module (crates-io))

(define-public crate-experiment-codec-derive-3.1.0 (c (n "experiment-codec-derive") (v "3.1.0") (d (list (d (n "proc-macro-crate") (r "^0.1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15.26") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "1wv5gypqlqlpf4cydhnysy9d8k6mlryk9d9d29qn8hdvfhnn11r2") (f (quote (("std") ("default" "std")))) (y #t)))

