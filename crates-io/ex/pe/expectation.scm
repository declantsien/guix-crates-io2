(define-module (crates-io ex pe expectation) #:use-module (crates-io))

(define-public crate-expectation-0.1.0 (c (n "expectation") (v "0.1.0") (d (list (d (n "walkdir") (r "^2.2.0") (d #t) (k 0)))) (h "0yyaw8dqhd82qa8rh179j4gb4n76jajfh750qpynai5gqjvl4y41")))

(define-public crate-expectation-0.1.1 (c (n "expectation") (v "0.1.1") (d (list (d (n "diff") (r "0.1.*") (o #t) (d #t) (k 0)) (d (n "expectation-shared") (r "0.1.*") (d #t) (k 0)) (d (n "image") (r "0.19.*") (o #t) (d #t) (k 0)) (d (n "serde") (r "1.*.*") (d #t) (k 0)) (d (n "serde_json") (r "1.*.*") (d #t) (k 0)))) (h "1213v3kdch3ip959n3bpwbhsmis3ywjrs45gxqwhi92dnryxwbpv") (f (quote (("text" "diff") ("default" "text" "image"))))))

