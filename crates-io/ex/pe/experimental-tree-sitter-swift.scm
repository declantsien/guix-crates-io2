(define-module (crates-io ex pe experimental-tree-sitter-swift) #:use-module (crates-io))

(define-public crate-experimental-tree-sitter-swift-0.0.1 (c (n "experimental-tree-sitter-swift") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.0") (d #t) (k 0)))) (h "0c8pd6hjbrjvxaaghqbv1915br8bhn7qgz7wfbbfyx0slb3g1ygv")))

(define-public crate-experimental-tree-sitter-swift-0.0.2 (c (n "experimental-tree-sitter-swift") (v "0.0.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.0") (d #t) (k 0)))) (h "1kbkpw8dpy0liwmqyp7f024axs28m4dds3pg44bn74s6scrljifn")))

(define-public crate-experimental-tree-sitter-swift-0.0.3 (c (n "experimental-tree-sitter-swift") (v "0.0.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.0") (d #t) (k 0)))) (h "0nsyr41qsi35hcw9rxd1nq348rnzh3p01dvf6z2f94xh65chfbjh")))

