(define-module (crates-io ex pe experi-codec) #:use-module (crates-io))

(define-public crate-experi-codec-3.2.0 (c (n "experi-codec") (v "3.2.0") (d (list (d (n "arrayvec") (r "^0.4") (k 0)) (d (n "experi-codec-derive") (r "^3.1") (o #t) (k 0)) (d (n "experi-codec-derive") (r "^3.1") (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0959safad9qqxy6fg6g3q6la8srgawvnzb5cs6yvshiv1p41ci22") (f (quote (("std" "serde") ("full") ("derive" "experi-codec-derive") ("default" "std")))) (y #t)))

