(define-module (crates-io ex pe expectation_plugin) #:use-module (crates-io))

(define-public crate-expectation_plugin-0.1.0 (c (n "expectation_plugin") (v "0.1.0") (h "1bwj4hhmd17bv9d7fh9xfal90hwv402n3b9qz0vc7rkzfdi61d6f")))

(define-public crate-expectation_plugin-0.1.1 (c (n "expectation_plugin") (v "0.1.1") (d (list (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "syn") (r "^0.14.9") (f (quote ("full"))) (d #t) (k 0)))) (h "1sav272dx4bdjyskwvbr0x78zji5z6h6bbfgnd5r138sghcyj1zk")))

(define-public crate-expectation_plugin-0.1.2 (c (n "expectation_plugin") (v "0.1.2") (d (list (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "syn") (r "^0.14.9") (f (quote ("full"))) (d #t) (k 0)))) (h "1fflf39lwx27fffkx5w24prj9i3knc8v91da3mj02vimpbbkg412")))

