(define-module (crates-io ex pe expect-exit) #:use-module (crates-io))

(define-public crate-expect-exit-0.1.0 (c (n "expect-exit") (v "0.1.0") (h "1y9kbc68qbvs8gkf98fiy57mvbw9g6lrlkfbdxm7a1nm9zk6j1h8")))

(define-public crate-expect-exit-0.2.0 (c (n "expect-exit") (v "0.2.0") (h "18phmxwhr2i8v4b3w3vlq6cmwd57ybzg4cqmp2w5nqq9x83rz1h3")))

(define-public crate-expect-exit-0.3.0 (c (n "expect-exit") (v "0.3.0") (h "0zgfm5sa507girrbiak874yxwa89h1rj4i6lkf1gnz4xikn0618q")))

(define-public crate-expect-exit-0.3.1 (c (n "expect-exit") (v "0.3.1") (h "0975k55r3p86iks9z9wzn92ipn4p2bn22wcm8xz78v4ys51jzwp7")))

(define-public crate-expect-exit-0.4.0 (c (n "expect-exit") (v "0.4.0") (h "19vh8bad2l031cyvl4ivf8k016nbh4z0a2by55r88chc0q5c0a47")))

(define-public crate-expect-exit-0.4.1 (c (n "expect-exit") (v "0.4.1") (h "1yrvk227d7jw0jl42wbwsv5v5f70nwwb7hg9j3vxbv7zn2yfvzmn")))

(define-public crate-expect-exit-0.4.2 (c (n "expect-exit") (v "0.4.2") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 2)))) (h "19fk9ap2mb04fkh67y2ydxih6w22b22bm3fr51hywfscravwiibs")))

(define-public crate-expect-exit-0.4.3 (c (n "expect-exit") (v "0.4.3") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 2)))) (h "1wi7mdfmbvlc8c106fg7r1yv2kxncaq6iljcba59v6r42f9vq7x8")))

(define-public crate-expect-exit-0.5.0 (c (n "expect-exit") (v "0.5.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 2)))) (h "0fhi9j34jnnzvkf0za0chmkh34l316kic4r57vf100wwyf1ibd8m")))

(define-public crate-expect-exit-0.5.1 (c (n "expect-exit") (v "0.5.1") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 2)))) (h "160fvxda4r3r24jh5qawqin4zm83sa03k3wf8a3hv5cass6ymmj8")))

(define-public crate-expect-exit-0.5.2 (c (n "expect-exit") (v "0.5.2") (d (list (d (n "once_cell") (r "^1.1.15") (d #t) (k 2)))) (h "1pyr3b8kkkf1bza6zgywsbzm2kzcbwlw8i5c4xyckmmbr7bka6n5")))

