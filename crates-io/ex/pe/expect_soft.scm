(define-module (crates-io ex pe expect_soft) #:use-module (crates-io))

(define-public crate-expect_soft-0.1.0 (c (n "expect_soft") (v "0.1.0") (h "0spa3z9qf3wm400r9w17zrdgbz2hb20iklq9r9cdjwx6pg9vy1bd")))

(define-public crate-expect_soft-0.1.1 (c (n "expect_soft") (v "0.1.1") (h "15qc4m0d05wq105nv7wrx4msdavcnkcqim32lrbggkvxfvp7jk84")))

