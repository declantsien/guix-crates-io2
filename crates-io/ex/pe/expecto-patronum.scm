(define-module (crates-io ex pe expecto-patronum) #:use-module (crates-io))

(define-public crate-expecto-patronum-0.1.0 (c (n "expecto-patronum") (v "0.1.0") (d (list (d (n "fastmurmur3") (r "^0.1.2") (d #t) (k 0)))) (h "0zzx6vkk6b5vrz9361i4q40fx0wgv8fysda4x1rs85wrizlph6dz") (f (quote (("std") ("default" "std"))))))

