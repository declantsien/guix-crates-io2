(define-module (crates-io ex pe experimental-reactive) #:use-module (crates-io))

(define-public crate-experimental-reactive-0.0.1 (c (n "experimental-reactive") (v "0.0.1") (h "0wsb6g64q58150g33k4d182bi3plx5cigkn40zgyi4jhs3k3vg7q")))

(define-public crate-experimental-reactive-0.0.2 (c (n "experimental-reactive") (v "0.0.2") (h "02nwb5vz3c5d491yh4kb5vcqb1fbh1jr43xsxgraxsy53nnjzxni")))

(define-public crate-experimental-reactive-0.0.3 (c (n "experimental-reactive") (v "0.0.3") (h "04sk60kh35nxxmgwad2jjpm3m3rx75k47p2il4ynkhw7abncqwwx")))

