(define-module (crates-io ex pe expect_macro_derive) #:use-module (crates-io))

(define-public crate-expect_macro_derive-0.0.0 (c (n "expect_macro_derive") (v "0.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (f (quote ("full"))) (d #t) (k 0)))) (h "021iyqj0wcr50wm82kmaz94v8339issw65dxh3gmjm4pn183a3b3")))

