(define-module (crates-io ex pe expect-tests) #:use-module (crates-io))

(define-public crate-expect-tests-0.1.0 (c (n "expect-tests") (v "0.1.0") (d (list (d (n "dissimilar") (r "^1") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)))) (h "1bbzhidpsq3wiqxkxv18r018j0yyilf28cw3j3c4waxgzhn89v0r")))

(define-public crate-expect-tests-0.1.1 (c (n "expect-tests") (v "0.1.1") (d (list (d (n "dissimilar") (r "^1") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)))) (h "14wwwbcd487bcfaqlrqbw89fg4cx52p9mnxdcp1q3cynaii66y5r")))

(define-public crate-expect-tests-0.1.2 (c (n "expect-tests") (v "0.1.2") (d (list (d (n "dissimilar") (r "^1") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "prettyplease") (r "^0.2.15") (o #t) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^2.0") (o #t) (d #t) (k 0)))) (h "01gjchql4y56gkvmablsc6aji0qh7vrl684pvv6fbkpd5bm5qjhn") (f (quote (("default" "expect-tokens")))) (s 2) (e (quote (("expect-tokens" "dep:syn" "dep:proc-macro2" "dep:prettyplease"))))))

