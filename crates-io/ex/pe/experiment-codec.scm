(define-module (crates-io ex pe experiment-codec) #:use-module (crates-io))

(define-public crate-experiment-codec-3.2.0 (c (n "experiment-codec") (v "3.2.0") (d (list (d (n "arrayvec") (r "^0.4") (k 0)) (d (n "experiment-codec-derive") (r "^3.1") (o #t) (k 0)) (d (n "experiment-codec-derive") (r "^3.1") (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0fzkm07hb5pfh07w8pcx8xa74c7gc5p7lk58gdailpibybvca0vg") (f (quote (("std" "serde") ("full") ("derive" "experiment-codec-derive") ("default" "std")))) (y #t)))

