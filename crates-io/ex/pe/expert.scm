(define-module (crates-io ex pe expert) #:use-module (crates-io))

(define-public crate-expert-0.0.1 (c (n "expert") (v "0.0.1") (d (list (d (n "failure") (r "^0.1.0") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "float-cmp") (r "^0.2") (d #t) (k 0)) (d (n "interpolate_idents") (r "^0.1.8") (d #t) (k 0)) (d (n "itertools") (r "^0.6") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "ordered-float") (r "^0.5") (d #t) (k 0)) (d (n "parking_lot") (r "^0.4") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "string-interner") (r "^0.6") (d #t) (k 0)))) (h "08iv9l2zl0fw7l572wrx1gayl5f6lnnnri1im33j6wfhrb7qbn9c")))

