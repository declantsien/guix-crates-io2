(define-module (crates-io ex pe expect-test) #:use-module (crates-io))

(define-public crate-expect-test-0.0.1 (c (n "expect-test") (v "0.0.1") (d (list (d (n "difference") (r "^2") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)))) (h "0n4fwyi6z6lnxc9i6gwh2aq15dpk9lxfn4zbi50nkg6zwwi5507m")))

(define-public crate-expect-test-0.1.0 (c (n "expect-test") (v "0.1.0") (d (list (d (n "difference") (r "^2") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)))) (h "1f5kjbpmdjpxi4a71jhzv6m3dg87r2ld2291axk2i6513rs87qx3")))

(define-public crate-expect-test-1.0.0 (c (n "expect-test") (v "1.0.0") (d (list (d (n "difference") (r "^2") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)))) (h "07fv1blmxrhj5k7ph2cxgf82c3isix111llryqfyd3cq2amv4g1s")))

(define-public crate-expect-test-1.0.1 (c (n "expect-test") (v "1.0.1") (d (list (d (n "difference") (r "^2") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)))) (h "0bhg5c1db4f6ndqn25rgxlbvi0q1nr7gvb1dqmlqfkhdm8z6zfff")))

(define-public crate-expect-test-1.0.2 (c (n "expect-test") (v "1.0.2") (d (list (d (n "difference") (r "^2") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)))) (h "0gpbjbnd3h5xpzb1jsvq50dj66kqxl0zq7wm83g7ka3zf968vdza")))

(define-public crate-expect-test-1.1.0 (c (n "expect-test") (v "1.1.0") (d (list (d (n "dissimilar") (r "^1") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)))) (h "0cq651b3dcrw94bl03krxnvllr8kqx6vskqal0n8ydrsmdx4f013")))

(define-public crate-expect-test-1.2.0-pre.1 (c (n "expect-test") (v "1.2.0-pre.1") (d (list (d (n "dissimilar") (r "^1") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)))) (h "15riwd2i621wkzgv3cshkf6vqn4vmlq39zlbhx3qfr39pij1cbrs")))

(define-public crate-expect-test-1.2.1 (c (n "expect-test") (v "1.2.1") (d (list (d (n "dissimilar") (r "^1") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)))) (h "1iwnx8d07nsdyhlkw8brj7j3iw6rcq2msz3rlx3y2fmgyj5mxz0g")))

(define-public crate-expect-test-1.2.2 (c (n "expect-test") (v "1.2.2") (d (list (d (n "dissimilar") (r "^1") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)))) (h "05nv365xd5fqydmzbsvzqz0148a1vbxp2p0r8a3ivafdvhl6ngky")))

(define-public crate-expect-test-1.3.0 (c (n "expect-test") (v "1.3.0") (d (list (d (n "dissimilar") (r "^1") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)))) (h "0wd2f9f8hh14xfyj9ddhlcrpk3cwbwwss6l4jlgj9qylvk4rbvfw")))

(define-public crate-expect-test-1.4.0 (c (n "expect-test") (v "1.4.0") (d (list (d (n "dissimilar") (r "^1") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)))) (h "1lsd6vhy4f7wggdh1k60winn28424gj2iq9gqyvnx0ldlfn62ihx")))

(define-public crate-expect-test-1.4.1 (c (n "expect-test") (v "1.4.1") (d (list (d (n "dissimilar") (r "^1") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)))) (h "1lzqx5hqh1g4llzqby4z1d18xmrjjx63c5l0na7ycf6mmpzfmn9h")))

(define-public crate-expect-test-1.5.0 (c (n "expect-test") (v "1.5.0") (d (list (d (n "dissimilar") (r "^1") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)))) (h "1q55nrkgzg345905aqbsdrwlq4sk0gjn4z5bdph1an1kc6jy02wy") (r "1.60.0")))

