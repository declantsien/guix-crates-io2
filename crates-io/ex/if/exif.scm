(define-module (crates-io ex if exif) #:use-module (crates-io))

(define-public crate-exif-0.0.1 (c (n "exif") (v "0.0.1") (d (list (d (n "exif-sys") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0vkiv02lwh7lz8kvwil1igl1574cwzbzwvmqk7g9ny44297d18rk")))

