(define-module (crates-io ex if exif-sys) #:use-module (crates-io))

(define-public crate-exif-sys-0.1.0 (c (n "exif-sys") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.8") (d #t) (k 1)))) (h "1kq8rg93vmfhnawv0mxabl0gyl3mzszkw0prs0sdhscq0zp4pd5f")))

