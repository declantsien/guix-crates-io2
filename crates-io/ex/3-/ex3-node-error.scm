(define-module (crates-io ex #{3-}# ex3-node-error) #:use-module (crates-io))

(define-public crate-ex3-node-error-0.13.0 (c (n "ex3-node-error") (v "0.13.0") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.17") (f (quote ("display"))) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "03kbhyrqrbl779d4x9ibai9gzzp4glpr7ijypjfgbw1phh1m7hzi")))

(define-public crate-ex3-node-error-0.13.2 (c (n "ex3-node-error") (v "0.13.2") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.17") (f (quote ("display"))) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1cnk1l7h4iz5930r4hidhdrwmf2sf1ns0asdjn4n1f154326xxsq")))

(define-public crate-ex3-node-error-0.14.0 (c (n "ex3-node-error") (v "0.14.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.17") (f (quote ("display"))) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0frd7k5rcwsnkvz7bcs754420qrwyd1myl2rai2swxx5yx5w4vxh")))

(define-public crate-ex3-node-error-0.14.1 (c (n "ex3-node-error") (v "0.14.1") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1289y0ajs2307iddxi635zdlmgh03ny41kz0b4hfl9945c4kj42k")))

