(define-module (crates-io ex #{3-}# ex3-serde) #:use-module (crates-io))

(define-public crate-ex3-serde-0.1.0 (c (n "ex3-serde") (v "0.1.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 0)) (d (n "serde_cbor") (r "^0.11.2") (d #t) (k 0)))) (h "0ri2rwyz5jdqbhiz8hpvng2r1qrhvn6as22kjyhcl1xb4yfxnfx4") (y #t)))

(define-public crate-ex3-serde-0.2.0 (c (n "ex3-serde") (v "0.2.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 0)) (d (n "serde_cbor") (r "^0.11.2") (d #t) (k 0)))) (h "0s9xzdnj9mki1f5n02mcbx2lyvmycml2bl0yry1lxsmy80p41y6i") (y #t)))

(define-public crate-ex3-serde-0.13.0 (c (n "ex3-serde") (v "0.13.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 0)) (d (n "serde_cbor") (r "^0.11.2") (d #t) (k 0)))) (h "1g72rchvpn7g62j254002dllkj9a03vpr4rdlrhxjgi1pvq98vgw") (y #t)))

(define-public crate-ex3-serde-0.13.2 (c (n "ex3-serde") (v "0.13.2") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 0)) (d (n "serde_cbor") (r "^0.11.2") (d #t) (k 0)))) (h "0kp0840c2ryhgxb4nfyv9bwciyk8kiryjdcbvahzn4f798sbbi2s") (y #t)))

(define-public crate-ex3-serde-0.14.0 (c (n "ex3-serde") (v "0.14.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "ciborium") (r "^0.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 0)))) (h "1mw4gh67m34hmxchhnpm8hc3jingpigwx9nb8glsz8nrmsnlxf55")))

(define-public crate-ex3-serde-0.15.0 (c (n "ex3-serde") (v "0.15.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "ciborium") (r "^0.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 0)))) (h "18ybg9mrcbq5h85r9rgww5wmcvhaw1nc7jw6kf07b4wrryagq764")))

