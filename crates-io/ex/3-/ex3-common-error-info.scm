(define-module (crates-io ex #{3-}# ex3-common-error-info) #:use-module (crates-io))

(define-public crate-ex3-common-error-info-0.2.0 (c (n "ex3-common-error-info") (v "0.2.0") (d (list (d (n "candid") (r "^0.8.3") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "14jz49dfvh61kqwxp4rqscpgsids1m5annldnw45y92iy26p6w14") (f (quote (("canister" "candid")))) (y #t)))

(define-public crate-ex3-common-error-info-0.13.0 (c (n "ex3-common-error-info") (v "0.13.0") (d (list (d (n "candid") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1s4jp6lcvmcsl9zki22y8c5kpda9bg890w7zwabzsppbwvpzj0h2") (f (quote (("canister" "candid")))) (y #t)))

(define-public crate-ex3-common-error-info-0.13.2 (c (n "ex3-common-error-info") (v "0.13.2") (d (list (d (n "candid") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1qs6sr8jmh0nr5938v167h6cimklfpq5brcdidff1cqikdh6s3y8") (f (quote (("canister" "candid")))) (y #t)))

(define-public crate-ex3-common-error-info-0.14.0 (c (n "ex3-common-error-info") (v "0.14.0") (d (list (d (n "candid") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0mxmnfa3d0imz5vmzy7nxf6km34p2175r6dzk6gphbfq413ksy2k") (f (quote (("canister" "candid")))) (y #t)))

(define-public crate-ex3-common-error-info-0.14.1 (c (n "ex3-common-error-info") (v "0.14.1") (d (list (d (n "candid") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1x831pb7hqsvv6b76qrrfp7qhvwmh95fb2370lgp90q7alasjafj") (f (quote (("canister" "candid")))) (y #t)))

(define-public crate-ex3-common-error-info-0.14.2 (c (n "ex3-common-error-info") (v "0.14.2") (d (list (d (n "candid") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0kf4h7sacy76h0l4d7nfhjn7i0khms5j12k4rcz0s8cp38k4lna7") (f (quote (("canister" "candid"))))))

(define-public crate-ex3-common-error-info-0.15.0 (c (n "ex3-common-error-info") (v "0.15.0") (d (list (d (n "candid") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "18sjxrj0xnqc3j0p087fc69ckzr1h7g130jwdiw67fggv62lwxi4") (f (quote (("canister" "candid"))))))

(define-public crate-ex3-common-error-info-0.15.1 (c (n "ex3-common-error-info") (v "0.15.1") (d (list (d (n "candid") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1a8c9lqh4wpyknlyg7qwi7z5sip6qllvykjjzb5na2jz73aj3clc") (f (quote (("canister" "candid"))))))

(define-public crate-ex3-common-error-info-0.15.2 (c (n "ex3-common-error-info") (v "0.15.2") (d (list (d (n "candid") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0azdf919696d73jr3qr7p3n7kx6k1jipvl6286zbc8j5za1gm841") (f (quote (("canister" "candid"))))))

