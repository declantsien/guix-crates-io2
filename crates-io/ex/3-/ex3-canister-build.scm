(define-module (crates-io ex #{3-}# ex3-canister-build) #:use-module (crates-io))

(define-public crate-ex3-canister-build-0.1.0 (c (n "ex3-canister-build") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "build-data") (r "^0.1.3") (d #t) (k 0)) (d (n "env-file-reader") (r "^0.3.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)))) (h "0vhvp71m23vk0hn05wilgnh6iaxwpxnbvvv4h7n9vc2fl4d6w9ib") (y #t)))

(define-public crate-ex3-canister-build-0.2.0 (c (n "ex3-canister-build") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "build-data") (r "^0.1.3") (d #t) (k 0)) (d (n "env-file-reader") (r "^0.3.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)))) (h "1alnagc9a28cnr3s8n5a4hfy9yrqll3zinkxnmfxm9aplmr9890m") (y #t)))

(define-public crate-ex3-canister-build-0.3.0 (c (n "ex3-canister-build") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "build-data") (r "^0.1.3") (d #t) (k 0)) (d (n "env-file-reader") (r "^0.3.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)))) (h "04yh5dvisl1yz0gn5na0gajygx5r9jzhb1pp987m9xqfqhydrxh8") (y #t)))

(define-public crate-ex3-canister-build-0.4.0 (c (n "ex3-canister-build") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "build-data") (r "^0.1.3") (d #t) (k 0)) (d (n "env-file-reader") (r "^0.3.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)))) (h "05dqdqjrrxdnvv9bbi87y96v6c3haw042z961mp12v1xjl60dky4") (y #t)))

(define-public crate-ex3-canister-build-0.10.0 (c (n "ex3-canister-build") (v "0.10.0") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "build-data") (r "^0.1.3") (d #t) (k 0)) (d (n "env-file-reader") (r "^0.3.0") (d #t) (k 0)))) (h "1q3shp8dq9hwq396pi72lfa3zw8klf2x5k7jg83zsvfkg9p354sx") (y #t)))

(define-public crate-ex3-canister-build-0.11.0 (c (n "ex3-canister-build") (v "0.11.0") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "build-data") (r "^0.1.3") (d #t) (k 0)) (d (n "env-file-reader") (r "^0.3.0") (d #t) (k 0)))) (h "1lr586njlm2fhzq5acc7lxffg72cyxr65plslla7l54n9gspyji2") (y #t)))

(define-public crate-ex3-canister-build-0.12.0 (c (n "ex3-canister-build") (v "0.12.0") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "build-data") (r "^0.1.3") (d #t) (k 0)) (d (n "env-file-reader") (r "^0.3.0") (d #t) (k 0)))) (h "02f9rfx9pmf5n0b6isj5cxxv749hhqc0yirn38y4xw861cgimr82") (y #t)))

(define-public crate-ex3-canister-build-0.14.0 (c (n "ex3-canister-build") (v "0.14.0") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "build-data") (r "^0.1.3") (d #t) (k 0)) (d (n "env-file-reader") (r "^0.3.0") (d #t) (k 0)))) (h "1qczpm99gywhpbm28kn3jrkfx1z2mnsb1d2ysv08h5ipyry4as2r") (y #t)))

(define-public crate-ex3-canister-build-0.15.0 (c (n "ex3-canister-build") (v "0.15.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "build-data") (r "^0.1.4") (d #t) (k 0)) (d (n "env-file-reader") (r "^0.3.0") (d #t) (k 0)))) (h "03ydnh99rdppy3padzdbpndki0zr45vhfqyi6r0wqlrxbsnbnawh") (y #t)))

(define-public crate-ex3-canister-build-0.15.1 (c (n "ex3-canister-build") (v "0.15.1") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "build-data") (r "^0.1.4") (d #t) (k 0)) (d (n "env-file-reader") (r "^0.3.0") (d #t) (k 0)))) (h "1c10a3niwc56wcnncfn8gdlfb7zgyq983hl5ghvax4cp465xfzlf") (y #t)))

(define-public crate-ex3-canister-build-0.15.4 (c (n "ex3-canister-build") (v "0.15.4") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "build-data") (r "^0.1.4") (d #t) (k 0)) (d (n "env-file-reader") (r "^0.3.0") (d #t) (k 0)))) (h "09lqwl5ib54y8ihkrrs5r0nq6vyps48msi313vlm5aywkylivx9j") (y #t)))

(define-public crate-ex3-canister-build-0.15.5 (c (n "ex3-canister-build") (v "0.15.5") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "build-data") (r "^0.1.4") (d #t) (k 0)) (d (n "env-file-reader") (r "^0.3.0") (d #t) (k 0)))) (h "1pzy5kjcvxn46fqgc376shmknypky5dyc3sv391q8nwga7map2di") (y #t)))

(define-public crate-ex3-canister-build-0.15.6 (c (n "ex3-canister-build") (v "0.15.6") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "build-data") (r "^0.1.4") (d #t) (k 0)) (d (n "env-file-reader") (r "^0.3.0") (d #t) (k 0)))) (h "1xbq0142b4f8a11nciqq5jyhr4gx47krid1ids7q5kwcbi08d5kk") (y #t)))

(define-public crate-ex3-canister-build-0.15.7 (c (n "ex3-canister-build") (v "0.15.7") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "build-data") (r "^0.1.4") (d #t) (k 0)) (d (n "env-file-reader") (r "^0.3.0") (d #t) (k 0)))) (h "0fjgm8kp8gd4ca86wvfxxgk4x1f4382a1aa3y56gl0z7gra0dizi") (y #t)))

(define-public crate-ex3-canister-build-0.15.8 (c (n "ex3-canister-build") (v "0.15.8") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "build-data") (r "^0.1.4") (d #t) (k 0)) (d (n "env-file-reader") (r "^0.3.0") (d #t) (k 0)))) (h "0ljh3ns37hrxf9lmqambs6gqxijf1c3am68h4qfggv1i5d10kp7k") (y #t)))

(define-public crate-ex3-canister-build-0.16.0 (c (n "ex3-canister-build") (v "0.16.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "build-data") (r "^0.1.4") (d #t) (k 0)) (d (n "env-file-reader") (r "^0.3.0") (d #t) (k 0)))) (h "0a7r245mc45f1jsr0c5hbf3nc8lncr76xv299iy2zdxwyjzjdy3r") (y #t)))

(define-public crate-ex3-canister-build-0.16.1 (c (n "ex3-canister-build") (v "0.16.1") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "build-data") (r "^0.1.4") (d #t) (k 0)) (d (n "env-file-reader") (r "^0.3.0") (d #t) (k 0)))) (h "1l26sawvrdjn0ww4qmppkhxhmy7d2licrbwlv3bby7kdijmiq2xp")))

(define-public crate-ex3-canister-build-0.17.0 (c (n "ex3-canister-build") (v "0.17.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "build-data") (r "^0.1.4") (d #t) (k 0)) (d (n "env-file-reader") (r "^0.3.0") (d #t) (k 0)))) (h "1cq6m58810j9683vzfpx2zw0b154wg3bvn2v3j38z0900c5apdpn")))

(define-public crate-ex3-canister-build-0.17.2 (c (n "ex3-canister-build") (v "0.17.2") (d (list (d (n "env-file-reader") (r "^0.3.0") (d #t) (k 0)))) (h "1cblzxi9v8ahczgldbrjicr1rfa0n1kwl1yrn22cd8m5b1byng8c")))

