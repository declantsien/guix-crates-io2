(define-module (crates-io ex #{3-}# ex3-canister-error) #:use-module (crates-io))

(define-public crate-ex3-canister-error-0.1.0 (c (n "ex3-canister-error") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.0") (f (quote ("display"))) (k 0)) (d (n "ex3-common-error-info") (r "^0.2.0") (f (quote ("canister"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0vfk32sjfszlrsa05pdnajgnwav4mdm398bm55fs3sgb2d1yafbk") (y #t)))

(define-public crate-ex3-canister-error-0.3.0 (c (n "ex3-canister-error") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.0") (f (quote ("display"))) (k 0)) (d (n "ex3-common-error-info") (r "^0.2.0") (f (quote ("canister"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1sr25jyvdi3la82hx696kcji406s2s0jbp38m11xwr4vvjzcy2kj") (y #t)))

(define-public crate-ex3-canister-error-0.4.0 (c (n "ex3-canister-error") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.0") (f (quote ("display"))) (k 0)) (d (n "ex3-common-error-info") (r "^0.2.0") (f (quote ("canister"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "17sksb52lrcz43mamgl2ixdp103ibyhzfdws4527mi98mii0asls") (y #t)))

(define-public crate-ex3-canister-error-0.15.0 (c (n "ex3-canister-error") (v "0.15.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.17") (f (quote ("display"))) (k 0)) (d (n "ex3-common-error-info") (r "^0.13.0") (f (quote ("canister"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1kdxq3fi406c4a4jmcrwir6gxvaljbm06490mpxkczxbi7dqk92l") (y #t)))

(define-public crate-ex3-canister-error-0.15.4 (c (n "ex3-canister-error") (v "0.15.4") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.17") (f (quote ("display"))) (k 0)) (d (n "ex3-common-error-info") (r "^0.13.0") (f (quote ("canister"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0nnkfdbrv0mdysqpd0rgwy2c97qkgilkgjm9wgbh4zyk1cm62jlc") (y #t)))

(define-public crate-ex3-canister-error-0.15.5 (c (n "ex3-canister-error") (v "0.15.5") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.17") (f (quote ("display"))) (k 0)) (d (n "ex3-common-error-info") (r "^0.13.0") (f (quote ("canister"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1zclykaaa79hsdmr4zv5ds2ncb5gawqgdnj1canqx0ypmssyn2g2") (y #t)))

(define-public crate-ex3-canister-error-0.15.8 (c (n "ex3-canister-error") (v "0.15.8") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.17") (f (quote ("display"))) (k 0)) (d (n "ex3-common-error-info") (r "^0.13.0") (f (quote ("canister"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "13jhxpwj4i36lb1kbjkxngyl76fj3mwcq2sbdfvzbf9v2h1bkfy7") (y #t)))

(define-public crate-ex3-canister-error-0.16.0 (c (n "ex3-canister-error") (v "0.16.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.17") (f (quote ("display"))) (k 0)) (d (n "ex3-common-error-info") (r "^0.13.0") (f (quote ("canister"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1fzak8lrzxj7qm4wav240ql4gg4qdvnnm18hajcb7ynvls3y8ggg") (y #t)))

(define-public crate-ex3-canister-error-0.16.1 (c (n "ex3-canister-error") (v "0.16.1") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.17") (f (quote ("display"))) (k 0)) (d (n "ex3-common-error-info") (r "^0.14") (f (quote ("canister"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0j68xqpc6ikjmh9kf59gz9y1w3jfzqpr5ar0pxdr298ps8fakhqv")))

(define-public crate-ex3-canister-error-0.17.0 (c (n "ex3-canister-error") (v "0.17.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.17") (f (quote ("display"))) (k 0)) (d (n "ex3-common-error-info") (r "^0.15") (f (quote ("canister"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "15sh11jlsagi2yimflv47mkxxlsx3mcq7n1gjygjr6wlsah0g5x8")))

(define-public crate-ex3-canister-error-0.17.1 (c (n "ex3-canister-error") (v "0.17.1") (d (list (d (n "anyhow") (r "^1.0.80") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.17") (f (quote ("display"))) (k 0)) (d (n "ex3-common-error-info") (r "^0.15") (f (quote ("canister"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1vnlpvsf3a32kyzw1pyqnki09w6sxmp74sn8g12hs15prwl61gay")))

