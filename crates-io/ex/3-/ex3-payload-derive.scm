(define-module (crates-io ex #{3-}# ex3-payload-derive) #:use-module (crates-io))

(define-public crate-ex3-payload-derive-0.14.0 (c (n "ex3-payload-derive") (v "0.14.0") (d (list (d (n "proc-macro2") (r "^1.0.58") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1hnldisq5vxq9hfwhw5s1cmnip0y9l3ax5p7xa3s79v2ya63pnd0")))

(define-public crate-ex3-payload-derive-0.14.1 (c (n "ex3-payload-derive") (v "0.14.1") (d (list (d (n "proc-macro2") (r "^1.0.58") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0q2a6f11sp1y2sygavsjsj588ds7xbf70g5bjz3cgpvjnjd8jcbm")))

(define-public crate-ex3-payload-derive-0.14.2 (c (n "ex3-payload-derive") (v "0.14.2") (d (list (d (n "proc-macro2") (r "^1.0.58") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0z2mc7jfv0d0ckys6gbspdgv2w9gqj4j34a7s4igq5cz6579a2sz")))

(define-public crate-ex3-payload-derive-0.14.3 (c (n "ex3-payload-derive") (v "0.14.3") (d (list (d (n "insta") (r "^1.29.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1wkz2cwazh9ni7h12l9mrjgqx68mwx452cc2nlz5igdzqcpikzxz")))

(define-public crate-ex3-payload-derive-0.15.0 (c (n "ex3-payload-derive") (v "0.15.0") (d (list (d (n "insta") (r "^1.29.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0mr2ha9ssrqxg1yraqfhr659xvx8qv1s6nf9rcagx0wsm3qsz7sh")))

