(define-module (crates-io ex #{3-}# ex3-spawn-wrapper) #:use-module (crates-io))

(define-public crate-ex3-spawn-wrapper-0.16.0 (c (n "ex3-spawn-wrapper") (v "0.16.0") (h "0gpkfa9zplc3ag6z22awkj5f07x12349iy2ya9pijvz87jsakbxf")))

(define-public crate-ex3-spawn-wrapper-0.17.0 (c (n "ex3-spawn-wrapper") (v "0.17.0") (h "1c89iis7wnj9889hq35zz4c83ypzkinjjclvhsgbxj059m9lzkjq")))

