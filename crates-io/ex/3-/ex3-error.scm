(define-module (crates-io ex #{3-}# ex3-error) #:use-module (crates-io))

(define-public crate-ex3-error-0.2.0 (c (n "ex3-error") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.0") (f (quote ("display"))) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "017zzcwxmqh02ncdck6fdjdnb83lxd9a0s6665r32ikfxnmcj6wh") (y #t)))

