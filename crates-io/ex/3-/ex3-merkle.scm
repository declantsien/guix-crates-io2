(define-module (crates-io ex #{3-}# ex3-merkle) #:use-module (crates-io))

(define-public crate-ex3-merkle-0.15.0 (c (n "ex3-merkle") (v "0.15.0") (d (list (d (n "rayon") (r "^1.5.1") (d #t) (k 2)) (d (n "sha2") (r "^0.10") (k 0)))) (h "1n9cc656nkcbnb331y20bkf7vj7w9lkkv6v0vdy0ypjz9jrwdbi2") (f (quote (("std" "sha2/std") ("default" "std"))))))

