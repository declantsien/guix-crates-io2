(define-module (crates-io ex #{3-}# ex3-timestamp) #:use-module (crates-io))

(define-public crate-ex3-timestamp-0.1.0 (c (n "ex3-timestamp") (v "0.1.0") (d (list (d (n "candid") (r "^0.8.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1wx9q2nvx8wcmzdbxclv89824cgf76bmzprxqjwvglw59ppi9xjh") (f (quote (("canister")))) (y #t)))

(define-public crate-ex3-timestamp-0.2.0 (c (n "ex3-timestamp") (v "0.2.0") (d (list (d (n "candid") (r "^0.8.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0zq8vxqvkg3salix6ml350hq3iq7v344cbakcwmcv4dfml7nwc41") (f (quote (("canister")))) (y #t)))

(define-public crate-ex3-timestamp-0.3.0 (c (n "ex3-timestamp") (v "0.3.0") (d (list (d (n "candid") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1805ah0lfvry5xx3kqcix6ilpwfv767asqri8g4lddf4s8xmvnhg") (f (quote (("canister")))) (y #t)))

(define-public crate-ex3-timestamp-0.13.0 (c (n "ex3-timestamp") (v "0.13.0") (d (list (d (n "candid") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1g6c719abqq7mgzmfmjxawxbh70q4879k292zr1lcivb6qnc73v1") (f (quote (("canister")))) (y #t)))

(define-public crate-ex3-timestamp-0.13.2 (c (n "ex3-timestamp") (v "0.13.2") (d (list (d (n "candid") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1h9s5yj0cj92magl9lxfmgmzwipbzsqwazmn3akq7njl4xjg0xwn") (f (quote (("canister")))) (y #t)))

(define-public crate-ex3-timestamp-0.14.0 (c (n "ex3-timestamp") (v "0.14.0") (d (list (d (n "candid") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0d54dskzsb9s459jhgq17xsm2xm8ppgwhcl1xhldmky1q22wqa2z") (f (quote (("canister")))) (y #t)))

(define-public crate-ex3-timestamp-0.14.1 (c (n "ex3-timestamp") (v "0.14.1") (d (list (d (n "candid") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1vcicz18lk8fnldhwrm8f6cyx25lh08bnn5h263b2b477mz4y6hd") (f (quote (("canister")))) (y #t)))

(define-public crate-ex3-timestamp-0.14.2 (c (n "ex3-timestamp") (v "0.14.2") (d (list (d (n "candid") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "15lvfgvb5s3c45mxvwqpiszzs5gnprwz0sfhj80rk6yrh2a1kgqc") (f (quote (("canister"))))))

(define-public crate-ex3-timestamp-0.14.3 (c (n "ex3-timestamp") (v "0.14.3") (d (list (d (n "candid") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1z9mwr3907x5h0052f588wl5sp00iv6hk38gsj83rvgmragc75cp") (f (quote (("canister"))))))

(define-public crate-ex3-timestamp-0.15.0 (c (n "ex3-timestamp") (v "0.15.0") (d (list (d (n "candid") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1g05zx7j8xb0yh1ghi3749dzf89yvsw30gvgvzm0l76v7v5rj36d") (f (quote (("canister"))))))

(define-public crate-ex3-timestamp-0.15.1 (c (n "ex3-timestamp") (v "0.15.1") (d (list (d (n "candid") (r "^0.9") (d #t) (k 0)) (d (n "ex3-ic-stable-structures") (r "^0.6.0-beta.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1miq45kd235p8xmrjj7g3ifmmlbsgw7j1y05sbf9rpfwr1wk6ax8") (f (quote (("canister"))))))

(define-public crate-ex3-timestamp-0.15.2 (c (n "ex3-timestamp") (v "0.15.2") (d (list (d (n "candid") (r "^0.9") (d #t) (k 0)) (d (n "ic-stable-structures") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "063sdvcvzl0ay5w6l2a4v17vgw02hvh0nflx2jc8dqc003921wa8") (f (quote (("canister"))))))

(define-public crate-ex3-timestamp-0.15.3 (c (n "ex3-timestamp") (v "0.15.3") (d (list (d (n "candid") (r "^0.9") (d #t) (k 0)) (d (n "ic-stable-structures") (r "^0.6.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1m257amlsvx6x9jyq1a3p47iqynqasfv15bxvvjq774s5cqnyppw") (f (quote (("canister"))))))

(define-public crate-ex3-timestamp-0.15.4 (c (n "ex3-timestamp") (v "0.15.4") (d (list (d (n "candid") (r "^0.10") (d #t) (k 0)) (d (n "ic-stable-structures") (r "^0.6.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "038wnr5f42wrgghlk1z5zds1wvcjh5mbakpp8g9j3dc1ih82v6bk") (f (quote (("canister"))))))

