(define-module (crates-io ex tf extfmt) #:use-module (crates-io))

(define-public crate-extfmt-0.1.0 (c (n "extfmt") (v "0.1.0") (h "04c6l3x6sd093203n733z9hfs2w2cyss618wvsw2jggh5j96qb2x")))

(define-public crate-extfmt-0.1.1 (c (n "extfmt") (v "0.1.1") (h "0pckp6p1gj2c631xd7zhj22pfq40rahdgy9v62k4w7xbcqsfb3x4")))

