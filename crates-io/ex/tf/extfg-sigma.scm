(define-module (crates-io ex tf extfg-sigma) #:use-module (crates-io))

(define-public crate-extfg-sigma-0.1.0 (c (n "extfg-sigma") (v "0.1.0") (d (list (d (n "bytes") (r "^0.5.5") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0fhy12dfh9q266lda388mzw53ibsj4zh9kyzgvfncbjgkrgxxnyg")))

(define-public crate-extfg-sigma-0.1.1 (c (n "extfg-sigma") (v "0.1.1") (d (list (d (n "bytes") (r "^0.5.5") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1icaf11mzxxd60zxkqr7lnqsh6nvg5vaj80alf18qnlxd53ivvwp")))

(define-public crate-extfg-sigma-0.1.2 (c (n "extfg-sigma") (v "0.1.2") (d (list (d (n "bytes") (r "^0.5.5") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1plzqbqnriaf2n2zir32k1bpwhq5g1380svvg37vz04yjsj37b63")))

(define-public crate-extfg-sigma-0.1.3 (c (n "extfg-sigma") (v "0.1.3") (d (list (d (n "bytes") (r "^0.5.5") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1d4ck81c33cvdz8j6k8rh00b1cvmj27874kxkx7kyhbn0fpc2hvq")))

(define-public crate-extfg-sigma-0.1.4 (c (n "extfg-sigma") (v "0.1.4") (d (list (d (n "bytes") (r ">=0.5.5, <0.6.0") (d #t) (k 0)) (d (n "rand") (r ">=0.7.0, <0.8.0") (d #t) (k 0)) (d (n "serde") (r ">=1.0.0, <2.0.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r ">=1.0.0, <2.0.0") (d #t) (k 0)))) (h "1bv5bky2biz3aks4gv1kniwah1mklplik69swh73hzk17spdmgz3")))

(define-public crate-extfg-sigma-0.1.5 (c (n "extfg-sigma") (v "0.1.5") (d (list (d (n "bytes") (r "^0.5.5") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0589jh36vsg4pg4rqrmkq9mpwcvy9g6m9g5qhyn8g63apgnysv0l")))

(define-public crate-extfg-sigma-0.1.6 (c (n "extfg-sigma") (v "0.1.6") (d (list (d (n "bytes") (r "^0.5.5") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1y9r6ks73l1xdijv8wks40lw47bw4vzmy5mzgx7b1ldb1ih4kyw4")))

(define-public crate-extfg-sigma-0.1.7 (c (n "extfg-sigma") (v "0.1.7") (d (list (d (n "bytes") (r "^0.5.5") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0lkcik3x3c170vfl4qbibblci7qvx4c1w6yqy84fs7fkpjc4flgg")))

(define-public crate-extfg-sigma-0.1.8 (c (n "extfg-sigma") (v "0.1.8") (d (list (d (n "bytes") (r "^0.5.5") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "16703541002a5k16b7fn6fxfxxfjsflbs3j3dqs487vbc86ahxh8")))

(define-public crate-extfg-sigma-0.1.9 (c (n "extfg-sigma") (v "0.1.9") (d (list (d (n "bytes") (r "^0.5.5") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0k2nqrbf98mgxxx0pg6pmjhdlqh54l98v2azwkcc1pijc3hpkss2")))

(define-public crate-extfg-sigma-0.2.0 (c (n "extfg-sigma") (v "0.2.0") (d (list (d (n "bytes") (r "^1.0.1") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "1hjjlzhsn15xhgw3s38bv855k73vnvs8ihpyv7nf875xqaczd570")))

(define-public crate-extfg-sigma-0.2.1 (c (n "extfg-sigma") (v "0.2.1") (d (list (d (n "bytes") (r "^1.0.1") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "0z4v396vp2hjf3b201wj1lqzwf2s5z9isgar0fw36mzwkkfx1q5j")))

(define-public crate-extfg-sigma-0.2.2 (c (n "extfg-sigma") (v "0.2.2") (d (list (d (n "bytes") (r "^1.0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "040nz8ivkh2zpvjhhzqcra7d9pch1649y7zdpyjyd43q77gxhsmg")))

(define-public crate-extfg-sigma-0.3.0 (c (n "extfg-sigma") (v "0.3.0") (d (list (d (n "bytes") (r "^1.0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)) (d (n "tokio-util") (r "^0.6.9") (f (quote ("codec"))) (o #t) (k 0)))) (h "0gfkyz60ycakp9805fhziihqbd209cd0d8dn6yy2j7bqpl58q3jm") (f (quote (("default") ("codec" "tokio-util"))))))

(define-public crate-extfg-sigma-0.3.1 (c (n "extfg-sigma") (v "0.3.1") (d (list (d (n "bytes") (r "^1.0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)) (d (n "tokio-util") (r "^0.6.9") (f (quote ("codec"))) (o #t) (k 0)))) (h "08j51b46jpdwv2sy4g88g8cnn2va6p1lxs3f7hv89ky3v584dg7i") (f (quote (("default") ("codec" "tokio-util"))))))

(define-public crate-extfg-sigma-0.3.2 (c (n "extfg-sigma") (v "0.3.2") (d (list (d (n "bytes") (r "^1.0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)) (d (n "tokio-util") (r "^0.6.9") (f (quote ("codec"))) (o #t) (k 0)))) (h "0gqri9ly2kiinx006ds17h8sxiy0i9sq1yq6jxjc3wm2pmvq9dr0") (f (quote (("default") ("codec" "tokio-util"))))))

(define-public crate-extfg-sigma-0.3.3 (c (n "extfg-sigma") (v "0.3.3") (d (list (d (n "bytes") (r "^1.0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)) (d (n "tokio-util") (r "^0.6.9") (f (quote ("codec"))) (o #t) (k 0)))) (h "0liildchkazbq2i3znag1xjwyz4i8vr715pa5p9i1qx8bmp4ayhv") (f (quote (("default") ("codec" "tokio-util"))))))

(define-public crate-extfg-sigma-0.3.4 (c (n "extfg-sigma") (v "0.3.4") (d (list (d (n "bytes") (r "^1.0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)) (d (n "tokio-util") (r "^0.7.3") (f (quote ("codec"))) (o #t) (k 0)))) (h "079v6j3lsj4wi7xzqyl0bdlf12gqv3nl7110pjv9d31jxbyzb7jm") (f (quote (("default") ("codec" "tokio-util"))))))

(define-public crate-extfg-sigma-0.3.5 (c (n "extfg-sigma") (v "0.3.5") (d (list (d (n "bytes") (r "^1.0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)) (d (n "tokio-util") (r "^0.7.3") (f (quote ("codec"))) (o #t) (k 0)))) (h "172jh9sb2c62hnpa6bpz7wzj2fxrlszn0vxx67jz33xym4rkga4z") (f (quote (("default") ("codec" "tokio-util"))))))

(define-public crate-extfg-sigma-0.3.6 (c (n "extfg-sigma") (v "0.3.6") (d (list (d (n "bytes") (r "^1.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)) (d (n "tokio-util") (r "^0.7.3") (f (quote ("codec"))) (o #t) (k 0)))) (h "0kj4hs399ylns43abbbv0bxw48ivgjsxgivwyrzm23fphkqsvka2") (f (quote (("default") ("codec" "tokio-util"))))))

