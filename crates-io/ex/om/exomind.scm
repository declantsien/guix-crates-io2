(define-module (crates-io ex om exomind) #:use-module (crates-io))

(define-public crate-exomind-0.1.4 (c (n "exomind") (v "0.1.4") (d (list (d (n "exomind-protos") (r "^0.1.4") (d #t) (k 0)))) (h "0dpmb6kmfwxqgknbkrf6zv4bplr6ljiag495c62r4xg6wjax78s7")))

(define-public crate-exomind-0.1.5 (c (n "exomind") (v "0.1.5") (d (list (d (n "exomind-protos") (r "^0.1.5") (d #t) (k 0)))) (h "19zy7c0837zmgj3jiglw8wfy0mj99zh18zwngzhl599iqfrzar99")))

(define-public crate-exomind-0.1.6 (c (n "exomind") (v "0.1.6") (d (list (d (n "exomind-protos") (r "^0.1.6") (d #t) (k 0)))) (h "0k3bf6735rxvavdyxz6r33vkazwvj2d4bnb2jd6zn3v6px6xz9wq")))

(define-public crate-exomind-0.1.7 (c (n "exomind") (v "0.1.7") (d (list (d (n "exomind-protos") (r "^0.1.7") (d #t) (k 0)))) (h "0j5kgdq5lmkz0knc7518w27ns308qybfncch6vkzakkqd8zl36n8")))

(define-public crate-exomind-0.1.8 (c (n "exomind") (v "0.1.8") (d (list (d (n "exomind-protos") (r "^0.1.8") (d #t) (k 0)))) (h "1rc8f0w1kz4bs7kgjqcdw57hkm51swqsz00q2hrl034kcjy3kgy3")))

(define-public crate-exomind-0.1.9 (c (n "exomind") (v "0.1.9") (d (list (d (n "exomind-protos") (r "^0.1.9") (d #t) (k 0)))) (h "0nhn6d4mg53jbin3rzlh15lxqy6l56ck7b6i7xi1krwaiqhlan3d")))

(define-public crate-exomind-0.1.11 (c (n "exomind") (v "0.1.11") (d (list (d (n "exomind-protos") (r "^0.1.11") (d #t) (k 0)))) (h "11m8k167cwvjzax6ni4vlwp5mhxisz1xfpv8hw47vn6wdzypffym")))

(define-public crate-exomind-0.1.12 (c (n "exomind") (v "0.1.12") (d (list (d (n "exomind-protos") (r "^0.1.12") (d #t) (k 0)))) (h "0azfg8g8m4bd6lcfhgikrqvimr4nx0lzlmj1as4p5np7d7hbmz74")))

(define-public crate-exomind-0.1.13 (c (n "exomind") (v "0.1.13") (d (list (d (n "exomind-protos") (r "^0.1.13") (d #t) (k 0)))) (h "0xj706k7r67akh97wab8z89h827gbvjlwz9hi543557n8wzri15q")))

(define-public crate-exomind-0.1.14 (c (n "exomind") (v "0.1.14") (d (list (d (n "exomind-protos") (r "^0.1.14") (d #t) (k 0)))) (h "14bczsil8zh34nly7bphqxn0f8rzmnf3yv493w47vy4g6nprhysz")))

(define-public crate-exomind-0.1.15 (c (n "exomind") (v "0.1.15") (d (list (d (n "exomind-protos") (r "^0.1.15") (d #t) (k 0)))) (h "1562dzj1zq4ib3h9zfjpfdgr4qxdfz5x84bnv50bqjhhrgw3sqq1")))

