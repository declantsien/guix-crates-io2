(define-module (crates-io ex om exomind-protos) #:use-module (crates-io))

(define-public crate-exomind-protos-0.1.4 (c (n "exomind-protos") (v "0.1.4") (d (list (d (n "exocore") (r "^0.1.8") (f (quote ("protos"))) (k 0)) (d (n "prost") (r "^0.7") (d #t) (k 0)) (d (n "prost-build") (r "^0.7") (d #t) (k 1)) (d (n "prost-types") (r "^0.7") (d #t) (k 0)))) (h "02qwci62ppq5vp8j4n4q8lma49qbdpah7rpr236zlfkgdddivn42")))

(define-public crate-exomind-protos-0.1.5 (c (n "exomind-protos") (v "0.1.5") (d (list (d (n "exocore") (r "^0.1.10") (f (quote ("protos"))) (k 0)) (d (n "prost") (r "^0.7") (d #t) (k 0)) (d (n "prost-build") (r "^0.7") (d #t) (k 1)) (d (n "prost-types") (r "^0.7") (d #t) (k 0)))) (h "1cpqv4vnd09q7s5jgcxv12ky75s3bwispl73ab9i8dw1g31z13fv")))

(define-public crate-exomind-protos-0.1.6 (c (n "exomind-protos") (v "0.1.6") (d (list (d (n "exocore") (r "^0.1.11") (f (quote ("protos"))) (k 0)) (d (n "prost") (r "^0.7") (d #t) (k 0)) (d (n "prost-build") (r "^0.7") (d #t) (k 1)) (d (n "prost-types") (r "^0.7") (d #t) (k 0)))) (h "0hijl8vr7115lc97w510blbbq4arpmab3w2y5rs4h2yjhkwxd8vj")))

(define-public crate-exomind-protos-0.1.7 (c (n "exomind-protos") (v "0.1.7") (d (list (d (n "exocore") (r "^0.1.12") (f (quote ("protos"))) (k 0)) (d (n "prost") (r "^0.7") (d #t) (k 0)) (d (n "prost-build") (r "^0.7") (d #t) (k 1)) (d (n "prost-types") (r "^0.7") (d #t) (k 0)))) (h "0b92qf7w03f58n6rmmdpxhdvfc9pl4dyn3pmkldidw04x6kb27kw")))

(define-public crate-exomind-protos-0.1.8 (c (n "exomind-protos") (v "0.1.8") (d (list (d (n "exocore") (r "^0.1.13") (f (quote ("protos"))) (k 0)) (d (n "prost") (r "^0.7") (d #t) (k 0)) (d (n "prost-build") (r "^0.7") (d #t) (k 1)) (d (n "prost-types") (r "^0.7") (d #t) (k 0)))) (h "0gd536yfmbh2zmvzvraimbg19hil0wwc1rbrpd1md068z4jm1fn4")))

(define-public crate-exomind-protos-0.1.9 (c (n "exomind-protos") (v "0.1.9") (d (list (d (n "exocore") (r "^0.1.13") (f (quote ("protos"))) (k 0)) (d (n "prost") (r "^0.7") (d #t) (k 0)) (d (n "prost-build") (r "^0.7") (d #t) (k 1)) (d (n "prost-types") (r "^0.7") (d #t) (k 0)))) (h "0cpl564qzlkf560lkxsfngwqm5ai1mfkg4m0mwbkwl6gc48ga918")))

(define-public crate-exomind-protos-0.1.11 (c (n "exomind-protos") (v "0.1.11") (d (list (d (n "exocore") (r "^0.1.15") (f (quote ("protos"))) (k 0)) (d (n "prost") (r "^0.9") (d #t) (k 0)) (d (n "prost-build") (r "^0.9") (d #t) (k 1)) (d (n "prost-types") (r "^0.9") (d #t) (k 0)))) (h "06h0blnramv6bp09dysxa2d9izq1ngj13v0jvxcxjqkw0s7wzkyp")))

(define-public crate-exomind-protos-0.1.12 (c (n "exomind-protos") (v "0.1.12") (d (list (d (n "exocore") (r "^0.1.16") (f (quote ("protos"))) (k 0)) (d (n "prost") (r "^0.9") (d #t) (k 0)) (d (n "prost-build") (r "^0.9") (d #t) (k 1)) (d (n "prost-types") (r "^0.9") (d #t) (k 0)))) (h "1ycyzm34qgk55i9alax5w96sd5hya70rglxq4pbl3ps2q1ipxjd4")))

(define-public crate-exomind-protos-0.1.13 (c (n "exomind-protos") (v "0.1.13") (d (list (d (n "exocore") (r "^0.1.19") (f (quote ("protos"))) (k 0)) (d (n "prost") (r "^0.9") (d #t) (k 0)) (d (n "prost-build") (r "^0.9") (d #t) (k 1)) (d (n "prost-types") (r "^0.9") (d #t) (k 0)))) (h "1xjwq1mzxg0nzbjf2ai12nnh510vla45x8s4kw6slkq3wy5f6gam")))

(define-public crate-exomind-protos-0.1.14 (c (n "exomind-protos") (v "0.1.14") (d (list (d (n "exocore") (r "^0.1.21") (f (quote ("protos"))) (k 0)) (d (n "prost") (r "^0.10.3") (d #t) (k 0)) (d (n "prost-build") (r "^0.10.3") (d #t) (k 1)) (d (n "prost-types") (r "^0.10.1") (d #t) (k 0)))) (h "113qn4appz62nw344ympcni73y499zmnvsmbizhfnn56zxnbmgf8")))

(define-public crate-exomind-protos-0.1.15 (c (n "exomind-protos") (v "0.1.15") (d (list (d (n "exocore") (r "^0.1.24") (f (quote ("protos"))) (k 0)) (d (n "prost") (r "^0.11.0") (d #t) (k 0)) (d (n "prost-build") (r "^0.11.1") (d #t) (k 1)) (d (n "prost-types") (r "^0.11.1") (d #t) (k 0)))) (h "0kllv623pgb7x1s2318mhxzkhkqg96gxynw45xacj7q4fnxa5jax")))

