(define-module (crates-io ex om exomind-app) #:use-module (crates-io))

(define-public crate-exomind-app-0.1.4 (c (n "exomind-app") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "exocore") (r "^0.1.8") (f (quote ("apps-sdk"))) (k 0)) (d (n "exomind-protos") (r "^0.1.4") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1fcagb4ggzqrig0f02qi0pcz1prz15i0mbb5bj6r6b06s4ik7fs4")))

(define-public crate-exomind-app-0.1.5 (c (n "exomind-app") (v "0.1.5") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "exocore") (r "^0.1.10") (f (quote ("apps-sdk"))) (k 0)) (d (n "exomind-protos") (r "^0.1.5") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0ls5di623izz95khh98bcw0xn5mjv4vm1lrnv3vfapq3kvz8w31w")))

(define-public crate-exomind-app-0.1.6 (c (n "exomind-app") (v "0.1.6") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "exocore") (r "^0.1.11") (f (quote ("apps-sdk"))) (k 0)) (d (n "exomind-protos") (r "^0.1.6") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "05zfhibbz54yqdq2jkma1gpp0agbcaya2bskxh2g0nmy8rza1mls")))

(define-public crate-exomind-app-0.1.7 (c (n "exomind-app") (v "0.1.7") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "exocore") (r "^0.1.12") (f (quote ("apps-sdk"))) (k 0)) (d (n "exomind-protos") (r "^0.1.7") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "13k64sbyy1cbazjfk2v9aq18784dg05k8ijvg908qydvr8r9frcj")))

(define-public crate-exomind-app-0.1.8 (c (n "exomind-app") (v "0.1.8") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "exocore") (r "^0.1.13") (f (quote ("apps-sdk"))) (k 0)) (d (n "exomind-protos") (r "^0.1.8") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1yqszhvmryzlmjvwjdzhkzqclgrkxr8vjxacvifgpjl14ann12zx")))

(define-public crate-exomind-app-0.1.9 (c (n "exomind-app") (v "0.1.9") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "exocore") (r "^0.1.13") (f (quote ("apps-sdk"))) (k 0)) (d (n "exomind-protos") (r "^0.1.9") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1glg3j8ngfdqan8zkqd25p9wxv64jd9mgc2s6y2l2n0sdi7l39y3")))

(define-public crate-exomind-app-0.1.11 (c (n "exomind-app") (v "0.1.11") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "exocore") (r "^0.1.15") (f (quote ("apps-sdk"))) (k 0)) (d (n "exomind-protos") (r "^0.1.11") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "16j5jqfagygh5i9j74qqdcyhpwapqfsf3na9kziggl8ga21j2igc")))

(define-public crate-exomind-app-0.1.12 (c (n "exomind-app") (v "0.1.12") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "exocore") (r "^0.1.16") (f (quote ("apps-sdk"))) (k 0)) (d (n "exomind-protos") (r "^0.1.12") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0h84aw4xsyavhbf27cssdp521sy65ias2pakgzra666pb1m6iv70")))

(define-public crate-exomind-app-0.1.13 (c (n "exomind-app") (v "0.1.13") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "exocore") (r "^0.1.20") (f (quote ("apps-sdk"))) (k 0)) (d (n "exomind-protos") (r "^0.1.13") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1hpslr8ymhn5rs57l2fr0lm0kwqcl3n8614787533zbfgv8fhw1k")))

(define-public crate-exomind-app-0.1.14 (c (n "exomind-app") (v "0.1.14") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "exocore") (r "^0.1.21") (f (quote ("apps-sdk"))) (k 0)) (d (n "exomind-protos") (r "^0.1.14") (d #t) (k 0)) (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "186p10r6yy47ncjyryy1xgmfdlpv409kxmj7rxjcllrqp55c7rqa")))

(define-public crate-exomind-app-0.1.15 (c (n "exomind-app") (v "0.1.15") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "exocore") (r "^0.1.24") (f (quote ("apps-sdk"))) (k 0)) (d (n "exomind-protos") (r "^0.1.15") (d #t) (k 0)) (d (n "futures") (r "^0.3.24") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "1mikavyy19yx6fpnp3miy7g6l59x6c34ja4wrgdcp9d60ax8ar6d")))

