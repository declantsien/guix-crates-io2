(define-module (crates-io ex _m ex_merge_sort) #:use-module (crates-io))

(define-public crate-ex_merge_sort-0.1.0 (c (n "ex_merge_sort") (v "0.1.0") (d (list (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "1hbxh0p50gj97qy458l5alz76s365y8bsbmgkslqpnzdympki5cq")))

(define-public crate-ex_merge_sort-0.1.1 (c (n "ex_merge_sort") (v "0.1.1") (d (list (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "0irh5jm3v4nszgnq2g3p5q2dm3a0llpy18mnp5kv5fsf3h4jpg51")))

(define-public crate-ex_merge_sort-0.2.0 (c (n "ex_merge_sort") (v "0.2.0") (d (list (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "134zg64yhq6cs0l8qq2walypd220cm46r1s93wmkvba2kxhgb3ry")))

(define-public crate-ex_merge_sort-0.2.1 (c (n "ex_merge_sort") (v "0.2.1") (d (list (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "1n5w3bkncmlbp8ima4pi0ki3z6sm43w48imlj8kvbjpi2z80pyk4")))

(define-public crate-ex_merge_sort-0.3.0 (c (n "ex_merge_sort") (v "0.3.0") (d (list (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "1b2fw9fvlqmvh4s01d5cd5pirsw2n8wvn186wqkkqgnahcxw1msz")))

(define-public crate-ex_merge_sort-0.3.1 (c (n "ex_merge_sort") (v "0.3.1") (d (list (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "1svqwkrjbazyza5ly7pm1rsvqvf6raplk6pkim2qzw9hcnmlz2kb")))

(define-public crate-ex_merge_sort-0.3.2 (c (n "ex_merge_sort") (v "0.3.2") (d (list (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "0rd6jk2mrqx1b5ngx7shd4wllhhrknw95dap2s7svd1iqsr6xr8w")))

(define-public crate-ex_merge_sort-0.4.0 (c (n "ex_merge_sort") (v "0.4.0") (d (list (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "0c2q04vz8hx1ag3vyg4xckz34w5x2lcw36hc4sky18sm0bbax8bc")))

(define-public crate-ex_merge_sort-0.5.0 (c (n "ex_merge_sort") (v "0.5.0") (d (list (d (n "memoize") (r "^0.1.9") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "0zfvfm6aq7b8k9xy1w7lq9qlla9mdbgmsn681mkbsxhid45xhzcs") (y #t)))

