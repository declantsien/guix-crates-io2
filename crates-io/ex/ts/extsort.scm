(define-module (crates-io ex ts extsort) #:use-module (crates-io))

(define-public crate-extsort-0.1.0 (c (n "extsort") (v "0.1.0") (h "0ikl7v7fgps0v7vikihssdwqz9q7md65wm2c63fji2nammgpdz2v")))

(define-public crate-extsort-0.1.1 (c (n "extsort") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.2.7") (d #t) (k 2)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)))) (h "1gr304bfqn5anl4475bkdk940iab0ihwayl1pd5z6vysd5lrmncb")))

(define-public crate-extsort-0.1.2 (c (n "extsort") (v "0.1.2") (d (list (d (n "byteorder") (r "^1.2.7") (d #t) (k 2)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)))) (h "1f1qyffl1wbjpkyami2j7apszaha2wl6hqd81njsxvgi5zdkim65")))

(define-public crate-extsort-0.1.3 (c (n "extsort") (v "0.1.3") (d (list (d (n "byteorder") (r "^1.2.7") (d #t) (k 2)) (d (n "skeptic") (r "^0.13") (d #t) (k 1)) (d (n "skeptic") (r "^0.13") (d #t) (k 2)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)))) (h "0d4p4m7i6v78jvpwzvs4n91birj9lnigkill8h4cvc2dh318fwgz")))

(define-public crate-extsort-0.2.0 (c (n "extsort") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.2.7") (d #t) (k 2)) (d (n "skeptic") (r "^0.13") (d #t) (k 1)) (d (n "skeptic") (r "^0.13") (d #t) (k 2)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)))) (h "0408b11spccm49v2w3pl5ddy7kxcaz3k873xg9lqgh21a0q3kmvm")))

(define-public crate-extsort-0.3.0 (c (n "extsort") (v "0.3.0") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 2)) (d (n "skeptic") (r "^0.13") (d #t) (k 1)) (d (n "skeptic") (r "^0.13") (d #t) (k 2)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)))) (h "12vg8zp4rshsrxxprfi730mz35vl3p122410s6kj3c89l82nx13q")))

(define-public crate-extsort-0.4.0 (c (n "extsort") (v "0.4.0") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rayon") (r "^1.5") (d #t) (k 0)) (d (n "skeptic") (r "^0.13") (d #t) (k 1)) (d (n "skeptic") (r "^0.13") (d #t) (k 2)) (d (n "tempfile") (r "^3.1") (d #t) (k 0)))) (h "0qcpqxpsrx7awlr42a9aixpa3wwkfw7j85178612890c07nvlc6g")))

(define-public crate-extsort-0.4.1 (c (n "extsort") (v "0.4.1") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rayon") (r "^1.5") (d #t) (k 0)) (d (n "skeptic") (r "^0.13") (d #t) (k 1)) (d (n "skeptic") (r "^0.13") (d #t) (k 2)) (d (n "tempfile") (r "^3.1") (d #t) (k 0)))) (h "0lcbh8py1dfjcw6v996hsw96s4380ls8zgcja5c1ipqri1frzs15") (y #t)))

(define-public crate-extsort-0.4.2 (c (n "extsort") (v "0.4.2") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rayon") (r "^1.5") (d #t) (k 0)) (d (n "skeptic") (r "^0.13") (d #t) (k 1)) (d (n "skeptic") (r "^0.13") (d #t) (k 2)) (d (n "tempfile") (r "^3.1") (d #t) (k 0)))) (h "17qcxhcxshw2f8dzyqnbzfr9ij1mijmpx1gna5mcxid3pipvpigz")))

(define-public crate-extsort-0.5.0 (c (n "extsort") (v "0.5.0") (d (list (d (n "byteorder") (r "^1.5") (d #t) (k 2)) (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rayon") (r "^1.8") (d #t) (k 0)) (d (n "skeptic") (r "^0.13") (d #t) (k 1)) (d (n "skeptic") (r "^0.13") (d #t) (k 2)) (d (n "tempfile") (r "^3.10") (d #t) (k 0)))) (h "0ahv2snp9jgllgwb9m04i1q129psb052fm3fna8wj37d86lywnmm")))

