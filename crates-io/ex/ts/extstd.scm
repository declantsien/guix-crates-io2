(define-module (crates-io ex ts extstd) #:use-module (crates-io))

(define-public crate-extstd-0.1.0 (c (n "extstd") (v "0.1.0") (h "1kkhb88z2k9pjisxyvzdxp0qc0b9kmg1pa6rf3ikwiy9d0bia1mf")))

(define-public crate-extstd-0.2.0 (c (n "extstd") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.53") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0r1183qymfkxy81sflq6jagn00y587iz0kv07jzrjlsmd81amhqa")))

(define-public crate-extstd-0.3.0 (c (n "extstd") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.53") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1gpqa2q16j7952xaaqnarj90imagixd4v6gzvdscnn8ivyh4b7zc")))

(define-public crate-extstd-0.4.0 (c (n "extstd") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.53") (d #t) (k 0)) (d (n "crossbeam") (r "^0.8.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "147a3r0kyv0qa4gxi77nirl4y2f3f8ychsva6b5q3lpdvhas7727")))

(define-public crate-extstd-0.5.0 (c (n "extstd") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0.53") (d #t) (k 0)) (d (n "crossbeam") (r "^0.8.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1khz086ajyv1fwbcs92i6b2v87pf180jmi9m2mj5bzjhkzvwwfjy")))

(define-public crate-extstd-0.5.1 (c (n "extstd") (v "0.5.1") (d (list (d (n "anyhow") (r "^1.0.53") (d #t) (k 0)) (d (n "crossbeam") (r "^0.8.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0i5gyzb7bm8fc7j8gsgfshg67sxp4j0n1d5j8kkd17qwqh3vk6ag")))

