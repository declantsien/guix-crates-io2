(define-module (crates-io ex ts extsort-iter) #:use-module (crates-io))

(define-public crate-extsort-iter-0.1.0 (c (n "extsort-iter") (v "0.1.0") (d (list (d (n "rayon") (r "^1") (o #t) (d #t) (k 0)))) (h "1x1p68q5xqvsz11k7q7frmd9wqp1d2kb6ppa6jhqmmqdyw74ljq1") (f (quote (("parallel_sort" "rayon"))))))

(define-public crate-extsort-iter-0.2.0 (c (n "extsort-iter") (v "0.2.0") (d (list (d (n "num_cpus") (r "^1") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rayon") (r "^1") (o #t) (d #t) (k 0)))) (h "1palkxjm1a402563dfz5zajf1yw35gjyh0nr0f4mrc8g3wkc5kll") (f (quote (("parallel_sort" "rayon"))))))

(define-public crate-extsort-iter-0.2.1 (c (n "extsort-iter") (v "0.2.1") (d (list (d (n "num_cpus") (r "^1") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rayon") (r "^1") (o #t) (d #t) (k 0)))) (h "0m6s46li6lqxy2z3fahydbjfr0hc6fbc6ankdvj80ccf14hikwq3") (f (quote (("parallel_sort" "rayon"))))))

(define-public crate-extsort-iter-0.3.0 (c (n "extsort-iter") (v "0.3.0") (d (list (d (n "lz4_flex") (r "^0.11") (o #t) (d #t) (k 0)) (d (n "num_cpus") (r "^1") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rayon") (r "^1") (o #t) (d #t) (k 0)))) (h "0srmphxz6vpd149fs7razn0fp612s7iamci8629g75ylr5jlk7cb") (f (quote (("default") ("compression")))) (s 2) (e (quote (("parallel_sort" "dep:rayon") ("compression_lz4_flex" "compression" "dep:lz4_flex"))))))

(define-public crate-extsort-iter-0.3.1 (c (n "extsort-iter") (v "0.3.1") (d (list (d (n "lz4_flex") (r "^0.11") (o #t) (d #t) (k 0)) (d (n "num_cpus") (r "^1") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rayon") (r "^1") (o #t) (d #t) (k 0)))) (h "0i2x66m2cjmfmqg5nzckyfkyq1jwa21wq9nhmyhdkgpzjxbnjfh9") (f (quote (("default") ("compression")))) (s 2) (e (quote (("parallel_sort" "dep:rayon") ("compression_lz4_flex" "compression" "dep:lz4_flex"))))))

