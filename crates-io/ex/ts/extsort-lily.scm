(define-module (crates-io ex ts extsort-lily) #:use-module (crates-io))

(define-public crate-extsort-lily-0.1.0 (c (n "extsort-lily") (v "0.1.0") (d (list (d (n "rand") (r "^0.4") (d #t) (k 2)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)))) (h "1d7g6x9aw1dmz2hi1p2i9j9n18dya8z1sfz1frv5i1fk0bf0fiz6")))

