(define-module (crates-io ex er exercism_prep_tests) #:use-module (crates-io))

(define-public crate-exercism_prep_tests-0.6.0 (c (n "exercism_prep_tests") (v "0.6.0") (d (list (d (n "clioptions") (r "^0.1.0") (d #t) (k 0)) (d (n "envmnt") (r "^0.10.4") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)))) (h "0rn7gn8h8cl6m90wnkgxg9m472wbnd2ps1zwk0gn89d9gjm3c6as") (y #t)))

(define-public crate-exercism_prep_tests-0.6.1 (c (n "exercism_prep_tests") (v "0.6.1") (d (list (d (n "clioptions") (r "^0.1.0") (d #t) (k 0)) (d (n "envmnt") (r "^0.10.4") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)))) (h "1xj707kp18yp3yscdk0mmwjywiz9bbbyxhr9g0vqbgw00sdqpa5v")))

