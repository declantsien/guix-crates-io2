(define-module (crates-io ex er exercism) #:use-module (crates-io))

(define-public crate-exercism-0.1.0 (c (n "exercism") (v "0.1.0") (h "0501vq9dmg8lkg0zhf80vfmbi47av1m5ybdqf0k1p0d8fl4m9jc9") (r "1.70.0")))

(define-public crate-exercism-0.1.1 (c (n "exercism") (v "0.1.1") (h "19kcdjhzhna5j18lxdsdwsykli29chj297g2qi6dhpvlyj8d72yq") (r "1.70.0")))

(define-public crate-exercism-0.1.2 (c (n "exercism") (v "0.1.2") (h "1pjqynhkbmrf0lsj2hv577rkdpbbivhfqczw341a5mh4pk5bvr25") (r "1.70.0")))

(define-public crate-exercism-0.1.3 (c (n "exercism") (v "0.1.3") (h "01x295806hwd99n27hylmfipfjgg0zjld9lyzaq68ajmdl40hzmq") (r "1.70.0")))

(define-public crate-exercism-0.1.4 (c (n "exercism") (v "0.1.4") (h "0i3r2b4cvccgqydb684xjb2sky0ar0axwf5f8ryq8fh6y7j25848") (r "1.70.0")))

(define-public crate-exercism-0.1.5 (c (n "exercism") (v "0.1.5") (d (list (d (n "itertools") (r "^0.11.0") (d #t) (k 0)))) (h "1lzl323z70r2izybzr3aj9q9irra9qd770kaz4swv02zxlmv47nc") (r "1.70.0")))

(define-public crate-exercism-0.1.6 (c (n "exercism") (v "0.1.6") (d (list (d (n "itertools") (r "^0.11.0") (d #t) (k 0)))) (h "17wmdd9xx5grd49rwavpvsyzyfa9lrqya3qgzs2a3nfnwy1gj6ij") (r "1.70.0")))

(define-public crate-exercism-0.1.7 (c (n "exercism") (v "0.1.7") (d (list (d (n "itertools") (r "^0.11.0") (d #t) (k 0)))) (h "12jkp5if75kna5g6dzhq5i7gdmzk7hl12cpmjj9b8wzlcj26hidi") (r "1.70.0")))

(define-public crate-exercism-0.1.9 (c (n "exercism") (v "0.1.9") (d (list (d (n "itertools") (r "^0.11.0") (d #t) (k 0)))) (h "0qv5wy62vrb3axzfhh3yawk1z5hn4b1d9wjn525b45jvr7a1w839") (r "1.70.0")))

