(define-module (crates-io ex ex exex) #:use-module (crates-io))

(define-public crate-exex-0.0.1 (c (n "exex") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0.12") (d #t) (k 0)) (d (n "clap") (r "^4.4.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.1") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.7") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "predicates") (r "^3.0.4") (d #t) (k 0)))) (h "1mazbid5vsi90qqrj1dcds6ma9szf5c153dy56p7dbd4bfm4nb6w") (y #t)))

