(define-module (crates-io ex ar exar-server) #:use-module (crates-io))

(define-public crate-exar-server-0.1.0 (c (n "exar-server") (v "0.1.0") (d (list (d (n "exar") (r "^0.1") (d #t) (k 0)) (d (n "exar-net") (r "^0.1") (d #t) (k 0)) (d (n "exar-testkit") (r "^0.1") (d #t) (k 2)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "serde") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "serde_macros") (r "^0.7") (o #t) (d #t) (k 0)))) (h "0hjhsxw5wnw68p0fpbc8qqab1l77hjngylijh4irvgbnmjgnssdj") (f (quote (("serde-serialization" "serde" "serde_macros") ("rustc-serialization" "rustc-serialize"))))))

