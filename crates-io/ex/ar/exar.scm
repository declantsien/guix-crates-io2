(define-module (crates-io ex ar exar) #:use-module (crates-io))

(define-public crate-exar-0.1.0 (c (n "exar") (v "0.1.0") (d (list (d (n "exar-testkit") (r "^0.1") (d #t) (k 2)) (d (n "indexed-line-reader") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "serde") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^0.7") (d #t) (k 2)) (d (n "serde_macros") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1l687ggc8gfjl4zafv0gm0067ybdcd8hadxj79429dsp3fqlmjjl") (f (quote (("serde-serialization" "serde" "serde_macros") ("rustc-serialization" "rustc-serialize"))))))

