(define-module (crates-io ex ar exar-db) #:use-module (crates-io))

(define-public crate-exar-db-0.1.0 (c (n "exar-db") (v "0.1.0") (d (list (d (n "clap") (r "^2.1") (d #t) (k 0)) (d (n "exar") (r "^0.1") (f (quote ("rustc-serialization"))) (d #t) (k 0)) (d (n "exar-server") (r "^0.1") (f (quote ("rustc-serialization"))) (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "log4rs") (r "^0.4") (f (quote ("toml"))) (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "toml-config") (r "^0.4") (d #t) (k 0)))) (h "0963a0npvd3dgfnp8fih66xri6awbg6dl7l06g9iakm7gdds1as6")))

