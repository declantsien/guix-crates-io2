(define-module (crates-io ex ar exar-client) #:use-module (crates-io))

(define-public crate-exar-client-0.1.0 (c (n "exar-client") (v "0.1.0") (d (list (d (n "exar") (r "^0.1") (d #t) (k 0)) (d (n "exar-net") (r "^0.1") (d #t) (k 0)) (d (n "exar-testkit") (r "^0.1") (d #t) (k 2)) (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "0a4cy195ygshfcam66xb5f1a9rhkacibck87fcndzh9296sn6zpf")))

