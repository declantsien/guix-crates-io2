(define-module (crates-io ex ot exotic_macro) #:use-module (crates-io))

(define-public crate-exotic_macro-0.1.0 (c (n "exotic_macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "ron") (r "^0.6.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (d #t) (k 0)))) (h "05swfdwwj3849hqq4a5vxnqifm9xh0rglfands6147wajx73qzxz")))

