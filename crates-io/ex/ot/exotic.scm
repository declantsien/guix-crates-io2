(define-module (crates-io ex ot exotic) #:use-module (crates-io))

(define-public crate-exotic-0.1.0 (c (n "exotic") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "crossterm") (r "^0.19.0") (d #t) (k 0)) (d (n "num") (r "^0.3.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "rayon") (r "^1.5.0") (d #t) (k 0)) (d (n "realfft") (r "^1.1.0") (d #t) (k 0)) (d (n "rustfft") (r "^5.0.1") (d #t) (k 0)))) (h "16shb9q3jh821a9aiyddhs0h2fvmz3zc5gpva1qzj24yfb8xzn95") (y #t)))

(define-public crate-exotic-0.1.1 (c (n "exotic") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "crossterm") (r "^0.19.0") (d #t) (k 0)) (d (n "num") (r "^0.3.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "rayon") (r "^1.5.0") (d #t) (k 0)) (d (n "realfft") (r "^1.1.0") (d #t) (k 0)) (d (n "rustfft") (r "^5.0.1") (d #t) (k 0)))) (h "1gg0hc12gvv0r80vr0hsx9carmw52gsjf5s7zj1pmdqcvlfgcggq") (y #t)))

(define-public crate-exotic-0.1.2 (c (n "exotic") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "crossterm") (r "^0.19.0") (d #t) (k 0)) (d (n "num") (r "^0.3.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "rayon") (r "^1.5.0") (d #t) (k 0)) (d (n "realfft") (r "^1.1.0") (d #t) (k 0)) (d (n "rustfft") (r "^5.0.1") (d #t) (k 0)))) (h "0mb777x1z03ymnjz9phn9b5i3fr9ib9zmxzniakbr5bpysvsg55d") (y #t)))

(define-public crate-exotic-0.1.3 (c (n "exotic") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "crossterm") (r "^0.19.0") (d #t) (k 0)) (d (n "num") (r "^0.3.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "rayon") (r "^1.5.0") (d #t) (k 0)) (d (n "realfft") (r "^1.1.0") (d #t) (k 0)) (d (n "rustfft") (r "^5.0.1") (d #t) (k 0)))) (h "1fjmbpcj8idbpk702078s2giybrsb01hxb429s9v3f98g60mhc7n")))

