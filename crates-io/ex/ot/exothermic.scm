(define-module (crates-io ex ot exothermic) #:use-module (crates-io))

(define-public crate-exothermic-0.1.0 (c (n "exothermic") (v "0.1.0") (d (list (d (n "sdl2") (r "^0.30") (f (quote ("ttf" "image" "gfx" "mixer"))) (k 0)) (d (n "xml-rs") (r "^0.6.0") (d #t) (k 0)))) (h "16y16v3dxgxis98c42rqf9ivp2dmb47chqyr0k6yqc52hi1g91nk")))

