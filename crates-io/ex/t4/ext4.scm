(define-module (crates-io ex t4 ext4) #:use-module (crates-io))

(define-public crate-ext4-0.1.0 (c (n "ext4") (v "0.1.0") (d (list (d (n "bitflags") (r "^0.9.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)))) (h "05ggy4p36q1jbmwx9c0xiwsr3rrs2hxbirqrkrav03n6h01yv4ny")))

(define-public crate-ext4-0.1.1 (c (n "ext4") (v "0.1.1") (d (list (d (n "bitflags") (r "^0.9.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)))) (h "0cva66rw1p4gfb0nmvy2gz41kqc2jj4bfnghyl7jmv5fkhfgb7vp")))

(define-public crate-ext4-0.2.0 (c (n "ext4") (v "0.2.0") (d (list (d (n "bitflags") (r "^0.9.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)))) (h "15p73ij8yn6ra5mjkg3gi9y6s47xd3zgq7dnbfvdlnnz0rmi4la4")))

(define-public crate-ext4-0.2.1 (c (n "ext4") (v "0.2.1") (d (list (d (n "bitflags") (r "^0.9.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)))) (h "0rxx4p5cnq0wax6m4ra9x74jwhq3h96a5h21938y3dqz1r1igspf")))

(define-public crate-ext4-0.2.2 (c (n "ext4") (v "0.2.2") (d (list (d (n "bitflags") (r "^0.9.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)))) (h "1fp1hha8q0czsaccg8laah63kwn2kl8qz0wcmzbcp7z7k9wyd2dn")))

(define-public crate-ext4-0.3.0 (c (n "ext4") (v "0.3.0") (d (list (d (n "bitflags") (r "^0.9.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "crc") (r "^1.0.0") (d #t) (k 0)) (d (n "error-chain") (r "^0.10.0") (k 0)))) (h "118zcgh94mb5ps2q73jblyca229bzziw668phnp4bysa5xzzj1bh")))

(define-public crate-ext4-0.4.0 (c (n "ext4") (v "0.4.0") (d (list (d (n "bitflags") (r "^0.9.1") (d #t) (k 0)) (d (n "bootsector") (r "^0.1.0") (d #t) (k 2)) (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "crc") (r "^1.0.0") (d #t) (k 0)) (d (n "error-chain") (r "^0.10.0") (k 0)))) (h "0i74j3i249nsbwj0xpjq2zqar5kiv5m2ksgh4avq9rhva0xcfgcv")))

(define-public crate-ext4-0.4.1 (c (n "ext4") (v "0.4.1") (d (list (d (n "bitflags") (r "^0.9.1") (d #t) (k 0)) (d (n "bootsector") (r "^0.1.0") (d #t) (k 2)) (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "crc") (r "^1.0.0") (d #t) (k 0)) (d (n "error-chain") (r "^0.10.0") (k 0)))) (h "07vyw9l7msn8sskzml4nyvfpsrn5j89yasj6q4f8p27fp6ghnm48")))

(define-public crate-ext4-0.5.0 (c (n "ext4") (v "0.5.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "bootsector") (r "^0.1") (d #t) (k 2)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "crc") (r "^1") (d #t) (k 0)) (d (n "error-chain") (r "^0.11") (k 0)))) (h "1cdzvy3ba9fb2acy63fixjqfdnnywzqmcpkyzpznxmcha43b48lr")))

(define-public crate-ext4-0.6.0 (c (n "ext4") (v "0.6.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "bootsector") (r "^0.1") (d #t) (k 2)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "cast") (r "^0.2") (d #t) (k 0)) (d (n "crc") (r "^1") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)))) (h "0vqq580dfwwy06maixx92gydn7fyri7am1x619z84zz69hcj5jvz")))

(define-public crate-ext4-0.7.0 (c (n "ext4") (v "0.7.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "bootsector") (r "^0.1") (d #t) (k 2)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "crc") (r "^1") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)))) (h "0mnrq8jq0hf4jsjvrb3ha4vp3b9bzr1776h3gw5yl2wwrkprkrjz")))

(define-public crate-ext4-0.8.0 (c (n "ext4") (v "0.8.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "bootsector") (r "^0.1") (d #t) (k 2)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "crc") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1biw5k997qlnw9vj0flaykbnz23iy3kfilppyaz1nrvv2z4wwg2w")))

(define-public crate-ext4-0.9.0 (c (n "ext4") (v "0.9.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "bootsector") (r "^0.1") (d #t) (k 2)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "crc") (r "^1") (d #t) (k 0)) (d (n "positioned-io") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1k11zycnji2nda6dvzr3xpzmv6jlsl4ggam11nnjnalhx59n49k0")))

