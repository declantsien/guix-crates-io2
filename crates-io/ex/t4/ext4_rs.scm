(define-module (crates-io ex t4 ext4_rs) #:use-module (crates-io))

(define-public crate-ext4_rs-0.1.0 (c (n "ext4_rs") (v "0.1.0") (d (list (d (n "bitflags") (r "^2.2.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.5.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1bjv377ml620wlaa10562h6awry0f40np9p5ycw6wljmaa1bi5qb") (y #t)))

(define-public crate-ext4_rs-0.1.1 (c (n "ext4_rs") (v "0.1.1") (d (list (d (n "bitflags") (r "^2.2.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.5.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0x81fh7ir92i2iqdg5lipfj6rscxr6yhb5jrbpxlml8vw85mm8hv") (y #t)))

(define-public crate-ext4_rs-1.0.0 (c (n "ext4_rs") (v "1.0.0") (d (list (d (n "bitflags") (r "^2.2.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.5.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0yrd4r53isk4cikwmaibvi19dd8l1m1wlkv3jcfc61k014vs2iv7")))

