(define-module (crates-io ex t4 ext4fs) #:use-module (crates-io))

(define-public crate-ext4fs-0.1.0 (c (n "ext4fs") (v "0.1.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "byteorder") (r "^1.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.194") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-big-array") (r "^0.5.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "0khs2h6rr192bi38wkj8b8ra1svj3h2dmdq0r05xx29z57h49gpj")))

