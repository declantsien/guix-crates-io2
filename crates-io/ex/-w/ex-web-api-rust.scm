(define-module (crates-io ex -w ex-web-api-rust) #:use-module (crates-io))

(define-public crate-ex-web-api-rust-0.1.0 (c (n "ex-web-api-rust") (v "0.1.0") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "rocket") (r "^0.4.4") (d #t) (k 0)) (d (n "rocket_contrib") (r "^0.4.4") (f (quote ("serve" "json"))) (k 0)) (d (n "simple_logger") (r "^1.6.0") (d #t) (k 0)))) (h "1mrwgfn07l7fm0f9856h49v8dim7bn2hcdrynb1vl67y1v32kqf5")))

(define-public crate-ex-web-api-rust-0.1.1 (c (n "ex-web-api-rust") (v "0.1.1") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "rocket") (r "^0.4.4") (d #t) (k 0)) (d (n "rocket_contrib") (r "^0.4.4") (f (quote ("serve" "json"))) (k 0)) (d (n "simple_logger") (r "^1.6.0") (d #t) (k 0)))) (h "0qc0qmv97rcpzlaas0ld4vsghc5ixdhrd10kcz4igz911kwncavl")))

