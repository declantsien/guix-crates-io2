(define-module (crates-io ex p3 exp3) #:use-module (crates-io))

(define-public crate-exp3-0.1.0 (c (n "exp3") (v "0.1.0") (d (list (d (n "remain") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1abbwx17i4xvvl3g4h7dkzm7kq66144nb75bf3sd02666fwp763h")))

(define-public crate-exp3-0.2.0 (c (n "exp3") (v "0.2.0") (d (list (d (n "remain") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "0yvfxiqxw92paa7ixdkan9jiv0dq6ndyhry7s0239q8sbl669liy")))

