(define-module (crates-io ex ds exdsdevs) #:use-module (crates-io))

(define-public crate-exdsdevs-0.1.2 (c (n "exdsdevs") (v "0.1.2") (d (list (d (n "rand") (r "^0.8.4") (f (quote ("std_rng"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("preserve_order" "arbitrary_precision"))) (d #t) (k 0)) (d (n "threadpool") (r "^1.0") (d #t) (k 0)))) (h "190xl0nzm63mvvd7k7qzw0v3yh47h8bjzv7xmgqfq444shl4vw0r") (f (quote (("default")))) (r "1.56")))

(define-public crate-exdsdevs-0.1.3 (c (n "exdsdevs") (v "0.1.3") (d (list (d (n "rand") (r "^0.8.4") (f (quote ("std_rng"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("preserve_order" "arbitrary_precision"))) (d #t) (k 0)) (d (n "threadpool") (r "^1.0") (d #t) (k 0)))) (h "163w6r9mvzxz91a04gkq6a7qhyip4cjzm17f6nxxcidpgwbbivmz") (f (quote (("default")))) (r "1.56")))

