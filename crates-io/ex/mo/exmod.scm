(define-module (crates-io ex mo exmod) #:use-module (crates-io))

(define-public crate-exmod-0.1.0 (c (n "exmod") (v "0.1.0") (d (list (d (n "cc") (r "^1") (k 1)) (d (n "cpal") (r "^0.13") (k 2)))) (h "17h0bbwqwziq2r11xkbc4j9ybs7zfyjxcchzb8qggcvivzjwh3hs") (f (quote (("strings") ("ramping") ("linear_interpolation") ("defensive") ("default" "linear_interpolation" "ramping" "strings"))))))

(define-public crate-exmod-0.1.1 (c (n "exmod") (v "0.1.1") (d (list (d (n "cc") (r "^1") (k 1)) (d (n "cpal") (r "^0.13") (k 2)))) (h "1mi59dcxq6jxwjbd82apgzwh7x1z01xp78lxmylrqr9apga5hjp8") (f (quote (("strings") ("ramping") ("linear_interpolation") ("defensive") ("default" "linear_interpolation" "ramping" "strings"))))))

(define-public crate-exmod-0.1.2 (c (n "exmod") (v "0.1.2") (d (list (d (n "cc") (r "^1") (k 1)) (d (n "cpal") (r "^0.13") (k 2)))) (h "1jjpqsdmym0affzfpgwsr4fknln08wpz499pgvv2w58a8flzvs0j") (f (quote (("strings") ("ramping") ("linear_interpolation") ("defensive") ("default" "linear_interpolation" "ramping" "strings"))))))

(define-public crate-exmod-0.1.3 (c (n "exmod") (v "0.1.3") (d (list (d (n "cc") (r "^1") (k 1)) (d (n "cpal") (r "^0.13") (k 2)))) (h "1p9x37540bxxn440aa7x4ws0nj3kgk87dmch392cjydvbc1pak7y") (f (quote (("strings") ("ramping") ("linear_interpolation") ("defensive") ("default" "linear_interpolation" "ramping" "strings"))))))

(define-public crate-exmod-0.1.4 (c (n "exmod") (v "0.1.4") (d (list (d (n "cc") (r "^1") (k 1)) (d (n "cpal") (r "^0.13") (k 2)))) (h "1j2l5vnfcw81cpsbxbgarrzk0h2hkg43cs7zd6izpw5jwjkw1a9v") (f (quote (("strings") ("ramping") ("linear_interpolation") ("defensive") ("default" "linear_interpolation" "ramping" "strings"))))))

(define-public crate-exmod-0.1.5 (c (n "exmod") (v "0.1.5") (d (list (d (n "cc") (r "^1") (k 1)) (d (n "cpal") (r "^0.13") (k 2)))) (h "0lw69i739kr8z537w5py7f0rbyrx7xny1mwyspw6jzynsxvy3s4w") (f (quote (("strings") ("ramping") ("linear_interpolation") ("defensive") ("default" "linear_interpolation" "ramping" "strings"))))))

(define-public crate-exmod-0.1.6 (c (n "exmod") (v "0.1.6") (d (list (d (n "cc") (r "^1") (k 1)) (d (n "cpal") (r "^0.13") (k 2)))) (h "1vsxggjk9ihyabzg7nl0rqy3dh760jxfd62z7bd0p315ibism22g") (f (quote (("strings") ("ramping") ("linear_interpolation") ("defensive") ("default" "linear_interpolation" "ramping" "strings"))))))

(define-public crate-exmod-0.1.7 (c (n "exmod") (v "0.1.7") (d (list (d (n "cc") (r "^1") (k 1)) (d (n "cpal") (r "^0.13") (k 2)))) (h "0arvvdarkv117grqmz0sbw4sa175bxz1gb7zpa76xarfy06m7gn4") (f (quote (("strings") ("ramping") ("linear_interpolation") ("defensive") ("default" "linear_interpolation" "ramping" "strings"))))))

(define-public crate-exmod-0.1.8 (c (n "exmod") (v "0.1.8") (d (list (d (n "cc") (r "^1") (k 1)) (d (n "cpal") (r "^0.13") (k 2)))) (h "1h1b3xmf7m8bi2g5482y1akmbf0nnrb0svspgl05x8lvjwzy943g") (f (quote (("strings") ("ramping") ("linear_interpolation") ("defensive") ("default" "linear_interpolation" "ramping" "strings")))) (y #t)))

(define-public crate-exmod-0.1.9 (c (n "exmod") (v "0.1.9") (d (list (d (n "cc") (r "^1") (k 1)) (d (n "cpal") (r "^0.13") (k 2)))) (h "0igvqhj1kzr9k7l6w8fzdwmrkgs487rvb2sbfjp1ldq1jl4kr8cy") (f (quote (("strings") ("ramping") ("linear_interpolation") ("defensive") ("default" "linear_interpolation" "ramping" "strings")))) (y #t)))

(define-public crate-exmod-0.1.10 (c (n "exmod") (v "0.1.10") (d (list (d (n "cc") (r "^1") (k 1)) (d (n "cpal") (r "^0.13") (k 2)))) (h "0yrif3mn5vgd7af12p4jakq615vsmln82lsikzj00zxy19ac8hzm") (f (quote (("strings") ("ramping") ("linear_interpolation") ("defensive") ("default" "linear_interpolation" "ramping" "strings"))))))

(define-public crate-exmod-0.2.0 (c (n "exmod") (v "0.2.0") (d (list (d (n "cc") (r "^1") (k 1)) (d (n "cpal") (r "^0.13") (k 2)))) (h "04c1ki43jh4xiviwnxay8bcxc1g37cf4nrpljnp3qi2780j0k9bm") (f (quote (("strings") ("ramping") ("linear_interpolation") ("defensive") ("default" "linear_interpolation" "ramping" "strings"))))))

(define-public crate-exmod-0.2.1 (c (n "exmod") (v "0.2.1") (d (list (d (n "cc") (r "^1") (k 1)) (d (n "cpal") (r "^0.13") (k 2)))) (h "03rya05v32zywvdjph2b4axrr3cy6f19nbrx3qnl335q1m4nzbix") (f (quote (("strings") ("ramping") ("linear_interpolation") ("defensive") ("default" "linear_interpolation" "ramping" "strings"))))))

(define-public crate-exmod-0.3.0 (c (n "exmod") (v "0.3.0") (d (list (d (n "cc") (r "^1") (k 1)) (d (n "cpal") (r "^0.13") (k 2)) (d (n "sdl2") (r "^0.35") (k 2)))) (h "0rpj71vdd8qzpph8g5j2i3hg0d02gknmzyx894kqq6bb8gflsm42") (f (quote (("strings") ("ramping") ("linear_interpolation") ("defensive") ("default" "linear_interpolation" "ramping" "strings"))))))

(define-public crate-exmod-0.3.1 (c (n "exmod") (v "0.3.1") (d (list (d (n "cc") (r "^1") (k 1)) (d (n "cpal") (r "^0.13") (k 2)) (d (n "sdl2") (r "^0.35") (k 2)))) (h "1lrm3yjp2w35yv4dgyy20r9k509m3mdirh7l1f544hpqay5l5hnd") (f (quote (("strings") ("ramping") ("linear_interpolation") ("defensive") ("default" "linear_interpolation" "ramping" "strings"))))))

(define-public crate-exmod-0.3.2 (c (n "exmod") (v "0.3.2") (d (list (d (n "cc") (r "^1") (k 1)) (d (n "cpal") (r "^0.13") (k 2)) (d (n "sdl2") (r "^0.35") (k 2)))) (h "0phnjnmx2k2arjw246vf39b6vp9h9s33l8cdnizlz7cds2mgmx6x") (f (quote (("strings") ("ramping") ("linear_interpolation") ("defensive") ("default" "linear_interpolation" "ramping" "strings"))))))

(define-public crate-exmod-0.3.3 (c (n "exmod") (v "0.3.3") (d (list (d (n "cc") (r "^1") (k 1)) (d (n "cpal") (r "^0.13") (k 2)) (d (n "sdl2") (r "^0.35") (k 2)))) (h "1azsf5447m8isc1vgrd019gs3ff24arwaw5lxqr925ai4yjrmx0v") (f (quote (("strings") ("ramping") ("linear_interpolation") ("defensive") ("default" "linear_interpolation" "ramping" "strings"))))))

