(define-module (crates-io ex ha exhaustive) #:use-module (crates-io))

(define-public crate-exhaustive-0.1.0 (c (n "exhaustive") (v "0.1.0") (d (list (d (n "exhaustive_macros") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "0f3gx85v04i4mccqc3hxk2iqd2d22va2gmjjvrs6dibxawybrn2w") (f (quote (("macros" "exhaustive_macros") ("default" "macros"))))))

(define-public crate-exhaustive-0.1.1 (c (n "exhaustive") (v "0.1.1") (d (list (d (n "exhaustive_macros") (r "^0.1.1") (o #t) (d #t) (k 0)))) (h "0afjkrqlvaazzpwpmlzvc3hf32gm40bs0khfxqamam27pkd2m36c") (f (quote (("macros" "exhaustive_macros") ("default" "macros"))))))

(define-public crate-exhaustive-0.2.0 (c (n "exhaustive") (v "0.2.0") (d (list (d (n "exhaustive_macros") (r "=0.1.1") (o #t) (d #t) (k 0)))) (h "1psdb5b1y1fzv20klcmkbwxa3fzvcffl702a9kagc40mci07s0x3") (f (quote (("macros" "exhaustive_macros") ("default" "macros"))))))

(define-public crate-exhaustive-0.2.1 (c (n "exhaustive") (v "0.2.1") (d (list (d (n "exhaustive_macros") (r "=0.2.1") (o #t) (d #t) (k 0)))) (h "1d3bqgm8akbkpr574zk74z1jjp264vw2m78mdk51xqn50a5r2zf1") (f (quote (("macros" "exhaustive_macros") ("default" "macros"))))))

