(define-module (crates-io ex ha exhaustive-map-macros) #:use-module (crates-io))

(define-public crate-exhaustive-map-macros-0.1.0 (c (n "exhaustive-map-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.58") (d #t) (k 0)))) (h "079nnz1qlzfqfa1vav4adlw6yja8lr6p181qjrb6balx8wp22l36")))

(define-public crate-exhaustive-map-macros-0.1.1 (c (n "exhaustive-map-macros") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.58") (d #t) (k 0)))) (h "14xaj3bh448rvb8qqg4rhmwa3q9ngc2d84ynd1caddx63agg1v26")))

(define-public crate-exhaustive-map-macros-0.2.0 (c (n "exhaustive-map-macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.58") (d #t) (k 0)))) (h "0ims6nwg3mm9ynifc8drwlr44phr12cc5vlcyqa6fzhvnpzzapk8")))

(define-public crate-exhaustive-map-macros-0.2.1 (c (n "exhaustive-map-macros") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.58") (d #t) (k 0)))) (h "0lnvfnvqndqrbqxvjmsjy1cvifkvqx30p68lfh927g527whzs0c0")))

