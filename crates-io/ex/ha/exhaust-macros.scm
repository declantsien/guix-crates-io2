(define-module (crates-io ex ha exhaust-macros) #:use-module (crates-io))

(define-public crate-exhaust-macros-0.1.0 (c (n "exhaust-macros") (v "0.1.0") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "04g891g05pk7i39yvawvmi7f4acxa776im7fh2nh24iras47hgyx")))

(define-public crate-exhaust-macros-0.1.1 (c (n "exhaust-macros") (v "0.1.1") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.13") (f (quote ("full"))) (d #t) (k 0)))) (h "1qasml36ngqn5pc4m41w19936kx37yik1ib5i9mxwxcvhsyzizjy")))

