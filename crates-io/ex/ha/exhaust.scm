(define-module (crates-io ex ha exhaust) #:use-module (crates-io))

(define-public crate-exhaust-0.1.0 (c (n "exhaust") (v "0.1.0") (d (list (d (n "exhaust-macros") (r "^0.1.0") (d #t) (k 0)))) (h "0v3hiskqjdrgd4s0km8727sx7fbpjs4hw5n6wbprssd694shmvfi") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-exhaust-0.1.1 (c (n "exhaust") (v "0.1.1") (d (list (d (n "exhaust-macros") (r "^0.1.1") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (f (quote ("use_alloc"))) (o #t) (k 0)) (d (n "paste") (r "^1.0.5") (d #t) (k 0)))) (h "1d62pfwngc0zn1s7isdf3znhq8c7sjwjl0afhsmnn8377n2g245s") (f (quote (("std" "alloc") ("default" "std") ("alloc" "itertools"))))))

