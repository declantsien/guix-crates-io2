(define-module (crates-io ex ha exhaustive_macros) #:use-module (crates-io))

(define-public crate-exhaustive_macros-0.1.0 (c (n "exhaustive_macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "16bdpx2wlrnbda7xllbfvr96rc0c8ql9dmvin8k10jhx05fpkhxf")))

(define-public crate-exhaustive_macros-0.1.1 (c (n "exhaustive_macros") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "005a8921y943y1hqdgvpkglg7b1k7pyqc5pk6yymrbcfhakfdamd")))

(define-public crate-exhaustive_macros-0.2.0 (c (n "exhaustive_macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0nhiac1ijfl8z31dk4b5m6np4iwdjx8n2cs3504xzkj97ca1m6xg")))

(define-public crate-exhaustive_macros-0.2.1 (c (n "exhaustive_macros") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0ivsk2lm9pq1g6a8pkp0i4pply99447rj753a23frww93c321ijs")))

