(define-module (crates-io ex ha exhaustive-map) #:use-module (crates-io))

(define-public crate-exhaustive-map-0.1.1 (c (n "exhaustive-map") (v "0.1.1") (d (list (d (n "exhaustive-map-macros") (r "^0.1.0") (d #t) (k 0)))) (h "1rqdygf1i1l9wkb3nxmbiblqv2ra7bild49mr3jac41ghl0l894v")))

(define-public crate-exhaustive-map-0.2.0 (c (n "exhaustive-map") (v "0.2.0") (d (list (d (n "exhaustive-map-macros") (r "^0.2.0") (d #t) (k 0)))) (h "0airl25460nlld2amyxcmhywxzm9is8y2m4wwrygxf2z4g25jcf8")))

(define-public crate-exhaustive-map-0.2.1 (c (n "exhaustive-map") (v "0.2.1") (d (list (d (n "exhaustive-map-macros") (r "^0.2.1") (d #t) (k 0)))) (h "1v1g40wda6ibc5c99v0pv0bh26skghgpvcv61fd7q6z7fhrxbixm")))

