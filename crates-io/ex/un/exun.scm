(define-module (crates-io ex un exun) #:use-module (crates-io))

(define-public crate-exun-0.1.0 (c (n "exun") (v "0.1.0") (h "19ch9hmdjn9snzw5vk9pvl6m1vngn60vrhlcba1jcw21qqa74vj6") (f (quote (("std" "alloc") ("default" "std") ("alloc")))) (r "1.41.1")))

(define-public crate-exun-0.2.0 (c (n "exun") (v "0.2.0") (h "1gvh5lakwy65igv0daqpvqyai1xnqllw15dbd23w9phj72pawhpk") (f (quote (("std" "alloc") ("default" "std") ("alloc")))) (r "1.41.1")))

