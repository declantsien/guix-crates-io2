(define-module (crates-io ex on exonum_profiler) #:use-module (crates-io))

(define-public crate-exonum_profiler-0.1.0 (c (n "exonum_profiler") (v "0.1.0") (d (list (d (n "ctrlc") (r "^3.0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)) (d (n "thread-id") (r "^2.0.0") (d #t) (k 0)))) (h "1fhhlcg2qqqrd3pfrwg2p4qvmyzl59bi4maq7xg1l8bhssbkpq0s") (f (quote (("nomock"))))))

(define-public crate-exonum_profiler-0.1.1 (c (n "exonum_profiler") (v "0.1.1") (d (list (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)))) (h "0v1yvp3jhx4fhiqaa6bm4zynxwiglkjx7m5k3ql6bmwaivmbhnc8") (f (quote (("nomock"))))))

(define-public crate-exonum_profiler-0.1.2 (c (n "exonum_profiler") (v "0.1.2") (d (list (d (n "ctrlc") (r "^3.0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)))) (h "1qk3963sf0wjla73fy91sgda9nf2kv5k4nyvwrf3fssr394lqw3s") (f (quote (("nomock"))))))

