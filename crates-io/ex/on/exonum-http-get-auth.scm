(define-module (crates-io ex on exonum-http-get-auth) #:use-module (crates-io))

(define-public crate-exonum-http-get-auth-0.1.0 (c (n "exonum-http-get-auth") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "exonum") (r "^0.3.0") (d #t) (k 0)) (d (n "iron") (r "^0.5.1") (d #t) (k 0)) (d (n "mount") (r "^0.3.0") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0m8xzz2p9hqyj49l3kr8dvzigjq5yvfrynz920crrxhsw084wkhq")))

