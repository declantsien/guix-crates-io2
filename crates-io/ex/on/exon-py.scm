(define-module (crates-io ex on exon-py) #:use-module (crates-io))

(define-public crate-exon-py-0.11.0 (c (n "exon-py") (v "0.11.0") (d (list (d (n "arrow") (r "^50.0") (f (quote ("pyarrow"))) (d #t) (k 0)) (d (n "datafusion") (r "^36.0") (f (quote ("parquet"))) (d #t) (k 0)) (d (n "exon") (r "^0.11") (f (quote ("all"))) (d #t) (k 0)) (d (n "pyo3") (r "^0.20") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1sq6g476h12972x3r3hcddvv0996qq03kqxxfq9iqmjssy50y84i")))

