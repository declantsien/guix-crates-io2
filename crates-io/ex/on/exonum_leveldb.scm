(define-module (crates-io ex on exonum_leveldb) #:use-module (crates-io))

(define-public crate-exonum_leveldb-0.9.0 (c (n "exonum_leveldb") (v "0.9.0") (d (list (d (n "leveldb-sys") (r "^2.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "01y6ninql3gn08zq3jpx5j5fjn5ybrhjicdmr222x7rjqypddrq0")))

(define-public crate-exonum_leveldb-0.9.1 (c (n "exonum_leveldb") (v "0.9.1") (d (list (d (n "leveldb-sys") (r "^2.0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.26") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.5") (d #t) (k 2)))) (h "15c78cwz1zkrp8a48zd3z1xmfbyg1mxpr4zqvzlf4pkms63dwxia")))

