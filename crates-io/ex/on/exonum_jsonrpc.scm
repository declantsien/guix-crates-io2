(define-module (crates-io ex on exonum_jsonrpc) #:use-module (crates-io))

(define-public crate-exonum_jsonrpc-0.2.0 (c (n "exonum_jsonrpc") (v "0.2.0") (d (list (d (n "hyper") (r "^0.10.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "176204wkcv0d30i80djhkbdlr9smmz0k6n1sghhcr3x7jaxgngiq")))

(define-public crate-exonum_jsonrpc-0.3.0 (c (n "exonum_jsonrpc") (v "0.3.0") (d (list (d (n "reqwest") (r "^0.8.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.0") (d #t) (k 0)))) (h "0svjyvvap92947kddbahpgfgxh9j0iia3lm464qf6l18jy90hm2k")))

(define-public crate-exonum_jsonrpc-0.4.0 (c (n "exonum_jsonrpc") (v "0.4.0") (d (list (d (n "display_derive") (r "^0.0.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.8.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.0") (d #t) (k 0)))) (h "0ifc079i8bxfmvj14vp2ylnzvbq8kplgafih77fzw8m97791d8jb")))

(define-public crate-exonum_jsonrpc-0.5.0 (c (n "exonum_jsonrpc") (v "0.5.0") (d (list (d (n "display_derive") (r "^0.0") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "14096m2ryd7j47hx1wly2gzdrl0bqnqacfwvw5ijyn8aqmczyg7q")))

(define-public crate-exonum_jsonrpc-0.5.1 (c (n "exonum_jsonrpc") (v "0.5.1") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "reqwest") (r "= 0.9.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0p9nwg7g3wsixiib45az4ppj9w1sfwz3dbn3k3blfzrf03ww2sjx")))

