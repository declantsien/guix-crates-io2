(define-module (crates-io ex on exonum_rocksdb) #:use-module (crates-io))

(define-public crate-exonum_rocksdb-0.6.1 (c (n "exonum_rocksdb") (v "0.6.1") (d (list (d (n "exonum_librocksdb-sys") (r "^0.4.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.5") (d #t) (k 0)))) (h "182drp1wp78zdy2c89818lw95syav8zg9vszyay2zr67p7hsay03") (f (quote (("valgrind") ("default"))))))

(define-public crate-exonum_rocksdb-0.7.0 (c (n "exonum_rocksdb") (v "0.7.0") (d (list (d (n "exonum_librocksdb-sys") (r "^0.5.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.5") (d #t) (k 0)))) (h "0d14y47m9f1zh8hm6q09cjdqhb79k0qx784c251s9q5am3izh24p") (f (quote (("valgrind") ("default"))))))

(define-public crate-exonum_rocksdb-0.7.1 (c (n "exonum_rocksdb") (v "0.7.1") (d (list (d (n "exonum_librocksdb-sys") (r "^0.5.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.5") (d #t) (k 0)))) (h "0lnxc8c73r4kwg8b445z7gq1apg55mf0y1m8h43a0s0ah5wcsnmq") (f (quote (("valgrind") ("default"))))))

(define-public crate-exonum_rocksdb-0.7.2 (c (n "exonum_rocksdb") (v "0.7.2") (d (list (d (n "exonum_librocksdb-sys") (r "^0.5.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.5") (d #t) (k 0)))) (h "1lkm8psmxsbh0qr0bxd6ilbfqik6kprcc1970ilw88gfs2yc738n") (f (quote (("valgrind") ("default"))))))

(define-public crate-exonum_rocksdb-0.7.4 (c (n "exonum_rocksdb") (v "0.7.4") (d (list (d (n "exonum_librocksdb-sys") (r "^0.5.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.5") (d #t) (k 0)))) (h "1b8z271ps4v5pdabsssjaipy4b7kac96vxfhdanq2czificqhchj") (f (quote (("valgrind") ("default"))))))

(define-public crate-exonum_rocksdb-0.7.5 (c (n "exonum_rocksdb") (v "0.7.5") (d (list (d (n "exonum_librocksdb-sys") (r "^0.5.5") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)))) (h "1mqj7k7zl1b7a2wyfi0szyngn5np59m4b267s9kg65xl8fxckqsf") (f (quote (("valgrind") ("default"))))))

(define-public crate-exonum_rocksdb-0.7.6 (c (n "exonum_rocksdb") (v "0.7.6") (d (list (d (n "exonum_librocksdb-sys") (r "^0.5.5") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)))) (h "14yvdnni6bc1xc1x2caw9xmk53im97mrnn1dwzgzfjcfw9k8rn6b") (f (quote (("valgrind") ("default"))))))

