(define-module (crates-io ex on exonum-build) #:use-module (crates-io))

(define-public crate-exonum-build-0.10.0 (c (n "exonum-build") (v "0.10.0") (d (list (d (n "protoc-rust") (r "^2.2.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.2.7") (d #t) (k 0)))) (h "16m021c6rjgwh797nr17zc7wmp9xsg7q8slccljfyjxa5mddyiwk")))

(define-public crate-exonum-build-0.11.0 (c (n "exonum-build") (v "0.11.0") (d (list (d (n "protoc-rust") (r "^2.2.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.2.7") (d #t) (k 0)))) (h "191byqbfrvm6pjjwham7mzx32h65dfnf9bi6lasj9f37ri3a75xn")))

(define-public crate-exonum-build-0.12.0 (c (n "exonum-build") (v "0.12.0") (d (list (d (n "protoc-rust") (r "^2.2.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.2.7") (d #t) (k 0)))) (h "1yi15125ilnnq3fgiiz4rpla5n4qp9byqc73p2c0a8kydm7745k9")))

(define-public crate-exonum-build-0.13.0-rc.1 (c (n "exonum-build") (v "0.13.0-rc.1") (d (list (d (n "proc-macro2") (r "^1.0.4") (d #t) (k 0)) (d (n "protoc-rust") (r "^2.8.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (d #t) (k 0)) (d (n "walkdir") (r "^2.2.9") (d #t) (k 0)))) (h "1ln7q9jqjg65r8yyza0qgsn4rnkma4ad13k4fpykrs0nbgzvn4sr")))

(define-public crate-exonum-build-0.13.0-rc.2 (c (n "exonum-build") (v "0.13.0-rc.2") (d (list (d (n "proc-macro2") (r "^1.0.4") (d #t) (k 0)) (d (n "protoc-rust") (r "^2.8.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (d #t) (k 0)) (d (n "walkdir") (r "^2.2.9") (d #t) (k 0)))) (h "1cv1lz37zhiraa3pb0x0smnk9w6r36ckxq9krwv1carhkbkxw2r2")))

(define-public crate-exonum-build-1.0.0-rc.1 (c (n "exonum-build") (v "1.0.0-rc.1") (d (list (d (n "proc-macro2") (r "^1.0.4") (d #t) (k 0)) (d (n "protoc-rust") (r "^2.8.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (d #t) (k 0)) (d (n "walkdir") (r "^2.2.9") (d #t) (k 0)))) (h "0775mkvk12v6zjhsysn6cqy5miprcbrkvin6fa8kgi0xgak9sk27")))

(define-public crate-exonum-build-1.0.0-rc.2 (c (n "exonum-build") (v "1.0.0-rc.2") (d (list (d (n "proc-macro2") (r "^1.0.4") (d #t) (k 0)) (d (n "protoc-rust") (r "^2.8.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (d #t) (k 0)) (d (n "walkdir") (r "^2.2.9") (d #t) (k 0)))) (h "0k3lwpixz82xvnv82wcf122qnizd2a7760kak59a8f4akij7v5gz")))

(define-public crate-exonum-build-1.0.0-rc.3 (c (n "exonum-build") (v "1.0.0-rc.3") (d (list (d (n "proc-macro2") (r "^1.0.4") (d #t) (k 0)) (d (n "protoc-rust") (r "^2.8.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (d #t) (k 0)) (d (n "walkdir") (r "^2.2.9") (d #t) (k 0)))) (h "0dr2jgzkgb5gq0qy5lcvn88432hhnqi9gkc771lkzh0z1bkwmkmv")))

(define-public crate-exonum-build-1.0.0 (c (n "exonum-build") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1.0.4") (d #t) (k 0)) (d (n "protoc-rust") (r "^2.8.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (d #t) (k 0)) (d (n "walkdir") (r "^2.2.9") (d #t) (k 0)))) (h "1z6l9f8sw0b8xidh9f4sb74v0a31dixsb4c3bkf8lqkjnsrqw17x")))

(define-public crate-exonum-build-1.0.1 (c (n "exonum-build") (v "1.0.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "protoc-rust") (r "^2.14.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "05w75vihhbc6yzi54y73cx26g08fq6zlw7jh4203ypkn1isl7ha0")))

