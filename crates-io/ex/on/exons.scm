(define-module (crates-io ex on exons) #:use-module (crates-io))

(define-public crate-exons-0.1.1 (c (n "exons") (v "0.1.1") (d (list (d (n "io_utils") (r "^0.2.4") (d #t) (k 0)) (d (n "string_utils") (r "^0.1.1") (d #t) (k 0)) (d (n "vector_utils") (r "^0.1.3") (d #t) (k 0)))) (h "0afc6qml4sk7qnpkhzx98a2s9sk9vc4sjsr5al2x1l7hpapjd3ly")))

(define-public crate-exons-0.1.2 (c (n "exons") (v "0.1.2") (d (list (d (n "io_utils") (r "^0.2") (d #t) (k 0)) (d (n "string_utils") (r "^0.1.1") (d #t) (k 0)) (d (n "vector_utils") (r "^0.1.3") (d #t) (k 0)))) (h "0668zpbp6v1vxfl35ylm83a9x4yavhj4clypqaymgqby57xs4v7x")))

(define-public crate-exons-0.1.3 (c (n "exons") (v "0.1.3") (d (list (d (n "io_utils") (r "^0.2") (d #t) (k 0)) (d (n "string_utils") (r "^0.1") (d #t) (k 0)) (d (n "vector_utils") (r "^0.1.3") (d #t) (k 0)))) (h "09xgbcwy22zzd1ql7bssfzyxpydz6z2y265hvwc23pc7q86qw6s4")))

(define-public crate-exons-0.1.4 (c (n "exons") (v "0.1.4") (d (list (d (n "io_utils") (r "^0.2") (d #t) (k 0)) (d (n "string_utils") (r "^0.1") (d #t) (k 0)) (d (n "vector_utils") (r "^0.1") (d #t) (k 0)))) (h "0f7q12c7h4gxd5q8rx7z09myyi9sbxjl4yl0h4zwfghz3k7hv24c")))

(define-public crate-exons-0.1.5 (c (n "exons") (v "0.1.5") (d (list (d (n "io_utils") (r "^0.3") (d #t) (k 0)) (d (n "string_utils") (r "^0.1") (d #t) (k 0)) (d (n "vector_utils") (r "^0.1") (d #t) (k 0)))) (h "0s00wf1abvw0kz2sqmf082jws226ffp8bmqf0r1krs77vbjfdbcq")))

