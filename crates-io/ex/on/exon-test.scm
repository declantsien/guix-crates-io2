(define-module (crates-io ex on exon-test) #:use-module (crates-io))

(define-public crate-exon-test-0.3.4-beta.7 (c (n "exon-test") (v "0.3.4-beta.7") (d (list (d (n "datafusion") (r "^31") (f (quote ("compression"))) (d #t) (k 0)) (d (n "object_store") (r "^0.7.1") (d #t) (k 0)))) (h "06sn2c9phs0rldygrnjgba02asd7wj7x95av9hc5chpqv8h7w9kl")))

(define-public crate-exon-test-0.3.4-beta.8 (c (n "exon-test") (v "0.3.4-beta.8") (d (list (d (n "datafusion") (r "^31") (f (quote ("compression"))) (d #t) (k 0)) (d (n "object_store") (r "^0.7.1") (d #t) (k 0)))) (h "15p7wi37d82cgr8av33cklb047na06086cdad00gwqndldh5spgk")))

(define-public crate-exon-test-0.3.4-beta.9 (c (n "exon-test") (v "0.3.4-beta.9") (d (list (d (n "datafusion") (r "^31") (f (quote ("compression"))) (d #t) (k 0)) (d (n "object_store") (r "^0.7.1") (d #t) (k 0)))) (h "11zz2ddydb886pgrdlwbqsz0fi8pp5m5wfj322a37lv9qa2lv51q")))

