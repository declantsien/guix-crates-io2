(define-module (crates-io ex on exonum-derive) #:use-module (crates-io))

(define-public crate-exonum-derive-0.10.0 (c (n "exonum-derive") (v "0.10.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "15jzl1mgr2yyp5qr20q3js5yyh17ib72bsgw4k4wbzb8yclh6wa2")))

(define-public crate-exonum-derive-0.11.0 (c (n "exonum-derive") (v "0.11.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "07f8lnmsxi11by0xb8w6x28z3104l74hj0z3y8bz7hk048q4h8rf")))

(define-public crate-exonum-derive-0.12.0 (c (n "exonum-derive") (v "0.12.0") (d (list (d (n "exonum-merkledb") (r "^0.12.0") (d #t) (k 2)) (d (n "failure") (r "^0.1.5") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "protobuf") (r "^2.8.0") (d #t) (k 2)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1xlwsgj1q84gzib5qgsq0xmcs1jgfvgv899hi1jdynp8qc0j852i")))

(define-public crate-exonum-derive-0.13.0-rc.1 (c (n "exonum-derive") (v "0.13.0-rc.1") (d (list (d (n "darling") (r "^0.10.0") (d #t) (k 0)) (d (n "heck") (r "^0.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "semver") (r "^0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "04bb8nlhhy4a34q0rfaajbrbc1kdjfj0kkb24i3r37wa9s7fn0j2")))

(define-public crate-exonum-derive-0.13.0-rc.2 (c (n "exonum-derive") (v "0.13.0-rc.2") (d (list (d (n "darling") (r "^0.10.0") (d #t) (k 0)) (d (n "heck") (r "^0.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "semver") (r "^0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0113hygf0wv2rv2j4ay7y3lkjc5j7cz8nkbs07r0q4j377cnxk6n")))

(define-public crate-exonum-derive-1.0.0-rc.1 (c (n "exonum-derive") (v "1.0.0-rc.1") (d (list (d (n "darling") (r "^0.10.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "semver") (r "^0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1ws3q8car8f1ly7r3gycjicy5pcbk04sc76p1h1jlnj6xa3l7cbj")))

(define-public crate-exonum-derive-1.0.0-rc.2 (c (n "exonum-derive") (v "1.0.0-rc.2") (d (list (d (n "darling") (r "^0.10.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "semver") (r "^0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "14w3ni4yzhg0fcdzj51fg3b2v0a4fmibc2m3wqbpg0q26wkcr17j")))

(define-public crate-exonum-derive-1.0.0-rc.3 (c (n "exonum-derive") (v "1.0.0-rc.3") (d (list (d (n "darling") (r "^0.10.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "semver") (r "^0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0vzkvp85ppl7c6k29ib252zdnxjf5av5nk6lxfn19avw7cb899km")))

(define-public crate-exonum-derive-1.0.0 (c (n "exonum-derive") (v "1.0.0") (d (list (d (n "darling") (r "^0.10.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "semver") (r "^0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1cyy28pg8jj8nfgjg3y1imfw6d3lqpyw07p4xkp2b00x9ahsqqyz")))

