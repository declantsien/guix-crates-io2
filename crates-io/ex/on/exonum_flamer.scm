(define-module (crates-io ex on exonum_flamer) #:use-module (crates-io))

(define-public crate-exonum_flamer-0.1.4 (c (n "exonum_flamer") (v "0.1.4") (h "1p8ids49pxx2zkla58zbba6rl713w0sg31inczcyqhh46d1i1sby")))

(define-public crate-exonum_flamer-0.1.5 (c (n "exonum_flamer") (v "0.1.5") (h "1pq5kxlxnx2nd1vr2r0qg4i5z20vlzwcp9b6vspm21mac04adrn5")))

(define-public crate-exonum_flamer-0.1.6 (c (n "exonum_flamer") (v "0.1.6") (h "1p6hyzc03g868py06dyjk1ylbkvdmfhv1qwa9s96lq28jiyjczah")))

