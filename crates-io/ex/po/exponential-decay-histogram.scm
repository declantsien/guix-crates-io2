(define-module (crates-io ex po exponential-decay-histogram) #:use-module (crates-io))

(define-public crate-exponential-decay-histogram-0.1.0 (c (n "exponential-decay-histogram") (v "0.1.0") (d (list (d (n "ordered-float") (r "^0.5.0") (d #t) (k 0)) (d (n "rand") (r "^0.3.15") (d #t) (k 0)))) (h "0is3sgqvvx7vkfrxkvripyb6kybcsypqp2gqc2mii1804zfid0xp")))

(define-public crate-exponential-decay-histogram-0.1.1 (c (n "exponential-decay-histogram") (v "0.1.1") (d (list (d (n "ordered-float") (r "^0.5.0") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "1rrjdv5c3z4ak2ymsmhp26pqds5krkd3s76nnjk5g8qb7665qfzy")))

(define-public crate-exponential-decay-histogram-0.1.2 (c (n "exponential-decay-histogram") (v "0.1.2") (d (list (d (n "ordered-float") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)))) (h "17zcv06a9fxrsbvhrk2f1hh78ck445zy32wy142gnk9bb1lcngv2")))

(define-public crate-exponential-decay-histogram-0.1.3 (c (n "exponential-decay-histogram") (v "0.1.3") (d (list (d (n "ordered-float") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)))) (h "1f519gsyjzb6v0bbixxyh5i1qlca771jjxayvmyys87y8p9n1lg6")))

(define-public crate-exponential-decay-histogram-0.1.4 (c (n "exponential-decay-histogram") (v "0.1.4") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "ordered-float") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)))) (h "0hvk4wj6fgg7drl80kk1b9nxs0qwnkkasrsclf9jd5dj99ql95bb")))

(define-public crate-exponential-decay-histogram-0.1.5 (c (n "exponential-decay-histogram") (v "0.1.5") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "ordered-float") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.7") (f (quote ("small_rng"))) (d #t) (k 0)))) (h "0973wp21sw6cw82vcq0cnrs7hhzppdm0jvyzqvrp5mmnmj3m1afy")))

(define-public crate-exponential-decay-histogram-0.1.6 (c (n "exponential-decay-histogram") (v "0.1.6") (d (list (d (n "criterion") (r ">=0.3.0, <0.4.0") (d #t) (k 2)) (d (n "ordered-float") (r ">=2.0.0, <3.0.0") (d #t) (k 0)) (d (n "rand") (r ">=0.7.0, <0.8.0") (f (quote ("small_rng"))) (d #t) (k 0)))) (h "1i7916mw1ay2fr70rw7cpj00s73vcyy1hcckad5pr6mnwi978viw")))

(define-public crate-exponential-decay-histogram-0.1.7 (c (n "exponential-decay-histogram") (v "0.1.7") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "ordered-float") (r "^2.0") (d #t) (k 0)) (d (n "rand") (r "^0.7") (f (quote ("small_rng"))) (d #t) (k 0)))) (h "02x4rj436nnkf415gdspv1480644vqyrkv3qcmnzg95ga43w2ac1")))

(define-public crate-exponential-decay-histogram-0.1.8 (c (n "exponential-decay-histogram") (v "0.1.8") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "ordered-float") (r "^2.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 0)))) (h "1d4rly0xh3hs4pc4x2ykynrf61pr6c8q8kq0gbc179gkjgcrgqhl")))

(define-public crate-exponential-decay-histogram-0.1.9 (c (n "exponential-decay-histogram") (v "0.1.9") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "ordered-float") (r "^2.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 0)))) (h "1w1ynda5182xhn4mblnmb31x1vv410xgq205qdrql5ayhc3bd1mk")))

(define-public crate-exponential-decay-histogram-0.1.10 (c (n "exponential-decay-histogram") (v "0.1.10") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "ordered-float") (r "^3.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 0)))) (h "0dcr5q1xnkykhc2hbrp5yncz5ijv70wsa3hwd66brcf0ch8drnam")))

(define-public crate-exponential-decay-histogram-0.1.11 (c (n "exponential-decay-histogram") (v "0.1.11") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "ordered-float") (r "^4.1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 0)))) (h "1wxg43d3lirxds52haq0s9vnnvmfmgv1h02qkv4ag400x83wzqy6")))

