(define-module (crates-io ex po expose) #:use-module (crates-io))

(define-public crate-expose-0.1.0 (c (n "expose") (v "0.1.0") (d (list (d (n "cbindgen") (r "^0.8.3") (d #t) (k 0)) (d (n "libc") (r "^0.2.51") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "0802amqnr34csddxk969jid6i7iisg9m08f96ps5jp2vzp96x717")))

(define-public crate-expose-0.1.1 (c (n "expose") (v "0.1.1") (d (list (d (n "cbindgen") (r "^0.8.3") (d #t) (k 0)) (d (n "libc") (r "^0.2.51") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "1gp85dmr82jl72lzsf8mkibndaykbcwwz3mmnnw3k092saz8iv9a")))

