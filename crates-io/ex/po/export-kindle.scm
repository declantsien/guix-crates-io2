(define-module (crates-io ex po export-kindle) #:use-module (crates-io))

(define-public crate-export-kindle-0.1.4 (c (n "export-kindle") (v "0.1.4") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "quick-xml") (r "^0.28") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)))) (h "0a87zp5bcakdlf4hm2v5fy8qv06n9prrhmr0klalbd6wixh3b85v")))

(define-public crate-export-kindle-0.1.5 (c (n "export-kindle") (v "0.1.5") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "quick-xml") (r "^0.28") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)))) (h "0k8i6zv9ch9mxrsv9caca6fdvcy8xcpplrzy0j7nr2xi4kk1i1qc")))

