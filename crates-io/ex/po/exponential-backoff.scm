(define-module (crates-io ex po exponential-backoff) #:use-module (crates-io))

(define-public crate-exponential-backoff-0.1.0 (c (n "exponential-backoff") (v "0.1.0") (d (list (d (n "rand") (r "^0.5.3") (d #t) (k 0)))) (h "0g57s35l03f0n100abj7z2iljd16yyddhn7z9dhpn9kllvzwh6hp")))

(define-public crate-exponential-backoff-0.2.0 (c (n "exponential-backoff") (v "0.2.0") (d (list (d (n "rand") (r "^0.5.3") (d #t) (k 0)))) (h "1dc1x31056rxs7mh91yf7prsskcjh7p190gj9w7br961r02mph8q")))

(define-public crate-exponential-backoff-0.2.1 (c (n "exponential-backoff") (v "0.2.1") (d (list (d (n "rand") (r "^0.5.3") (d #t) (k 0)))) (h "0i3h6f410djmphqv6incix29gkh5imrsihxr6xsg8hxi01hm4mr5")))

(define-public crate-exponential-backoff-0.2.2 (c (n "exponential-backoff") (v "0.2.2") (d (list (d (n "rand") (r "^0.5.3") (d #t) (k 0)))) (h "10agh8kiwgk66s16rmigmschblhwi1kii3gviprr7vzd6dr7qczg")))

(define-public crate-exponential-backoff-1.0.0 (c (n "exponential-backoff") (v "1.0.0") (d (list (d (n "rand") (r "^0.5.3") (d #t) (k 0)))) (h "15cqdd5hjqrr73jb51k8dxm4mhc9szdxk5kbr2l56ylf94ngxq9s")))

(define-public crate-exponential-backoff-1.1.0 (c (n "exponential-backoff") (v "1.1.0") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "126sm20w096i4ba5v1zf7mzcpdjcbzsw3yf58b21fwrb40dmq745")))

(define-public crate-exponential-backoff-1.2.0 (c (n "exponential-backoff") (v "1.2.0") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1k9jz6h94d5h5ck88jwm3syp5729vqrb0akad2sy9vihv63qvxs7")))

