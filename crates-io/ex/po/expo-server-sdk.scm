(define-module (crates-io ex po expo-server-sdk) #:use-module (crates-io))

(define-public crate-expo-server-sdk-0.1.0 (c (n "expo-server-sdk") (v "0.1.0") (d (list (d (n "clap") (r "^2.31") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "0jb79z9cmmnpsacy58rq2dac6yjwsf55wqasdy5sp3xvgz7ja50i")))

