(define-module (crates-io ex po export-ffxiv-macros) #:use-module (crates-io))

(define-public crate-export-ffxiv-macros-0.1.0 (c (n "export-ffxiv-macros") (v "0.1.0") (d (list (d (n "directories") (r "^3.0.2") (d #t) (k 0)) (d (n "libxivdat") (r "^0.2.0") (f (quote ("macro"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "1mxfanpw4p2k7sd3awa6vmjnkaqwvpb9ml7fk0l1xwzk00znwhfx")))

