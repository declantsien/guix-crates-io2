(define-module (crates-io ex po export_cstr) #:use-module (crates-io))

(define-public crate-export_cstr-0.0.1 (c (n "export_cstr") (v "0.0.1") (h "0n75mf74lq5kk9d7lrqli14h2s2wiyad18nxx3wq5g4xfxlxjzl8")))

(define-public crate-export_cstr-0.0.2 (c (n "export_cstr") (v "0.0.2") (h "0j31lh68xkrw4p8dlg14iz8fhxpp1r0g4g962m8a9shdq4swq14a")))

(define-public crate-export_cstr-0.0.3 (c (n "export_cstr") (v "0.0.3") (h "1dvfbpp4grb4gzmf57rzmvl4ghx1dxahi78zl68v3x8fk97fgdy4")))

(define-public crate-export_cstr-0.0.4 (c (n "export_cstr") (v "0.0.4") (h "1xnc4mnmxxmzkgrnzd92k2z1r6rnfa4c5xsf3qv9jxhsjxkvd8f0")))

