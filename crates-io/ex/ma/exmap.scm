(define-module (crates-io ex ma exmap) #:use-module (crates-io))

(define-public crate-exmap-0.0.1 (c (n "exmap") (v "0.0.1") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)) (d (n "rustix") (r "^0.36.5") (f (quote ("mm" "fs"))) (d #t) (k 0)) (d (n "sc") (r "^0.2.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "1zizr196897a29fwyyb11w733pbvk19d8kbg2gd2mlard625wv6n")))

