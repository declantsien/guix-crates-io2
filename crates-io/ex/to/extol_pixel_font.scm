(define-module (crates-io ex to extol_pixel_font) #:use-module (crates-io))

(define-public crate-extol_pixel_font-0.3.1 (c (n "extol_pixel_font") (v "0.3.1") (d (list (d (n "bevy") (r "^0.13") (f (quote ("bevy_asset" "bevy_render" "bevy_sprite"))) (k 0)) (d (n "bevy") (r "^0.13") (f (quote ("bevy_asset" "bevy_render" "bevy_sprite" "bevy_core_pipeline" "multi-threaded" "png" "x11" "wayland"))) (k 2)) (d (n "bevy_asset_loader") (r "^0.20.0") (f (quote ("2d"))) (d #t) (k 2)))) (h "09b472c0m4i6hpff5siwi12ybc4hpr38c44qsn4j3nflzjhskpw4")))

