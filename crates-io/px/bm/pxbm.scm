(define-module (crates-io px bm pxbm) #:use-module (crates-io))

(define-public crate-pxbm-0.1.0 (c (n "pxbm") (v "0.1.0") (d (list (d (n "clap") (r "^4.2.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "parse-display") (r "^0.8.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)))) (h "0573lvjyr3xy8pg1hws4vjc109mhgmiw98g9vzdmd8wpjznq19a0")))

