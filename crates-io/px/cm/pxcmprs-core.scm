(define-module (crates-io px cm pxcmprs-core) #:use-module (crates-io))

(define-public crate-pxcmprs-core-0.1.0 (c (n "pxcmprs-core") (v "0.1.0") (d (list (d (n "gif") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "image") (r "^0.23") (o #t) (d #t) (k 0)) (d (n "mime") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "webp") (r "^0.1") (o #t) (d #t) (k 0)))) (h "034307rkbm5ijyaands023jydllky9hxyrvf19527pnzkw7w6w9f") (f (quote (("pipeline" "image" "webp" "gif") ("default" "pipeline"))))))

(define-public crate-pxcmprs-core-0.1.1 (c (n "pxcmprs-core") (v "0.1.1") (d (list (d (n "gif") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "image") (r "^0.23") (o #t) (d #t) (k 0)) (d (n "mime") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "webp") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0lr5fr49djf9qk7vwdrjpvkm9khdhlfnkr9j09iglb6vzvpixyn7") (f (quote (("pipeline" "image" "webp" "gif") ("default" "pipeline"))))))

