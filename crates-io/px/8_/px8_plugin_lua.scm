(define-module (crates-io px #{8_}# px8_plugin_lua) #:use-module (crates-io))

(define-public crate-px8_plugin_lua-0.0.11 (c (n "px8_plugin_lua") (v "0.0.11") (d (list (d (n "bitflags") (r "^0.1") (d #t) (k 0)) (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2.13") (d #t) (k 0)))) (h "1vwbv3l7syqiihmm2maymzbg1cr33c9wmizhg28a34vz6nfvf459")))

