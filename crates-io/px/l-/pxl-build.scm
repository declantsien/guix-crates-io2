(define-module (crates-io px l- pxl-build) #:use-module (crates-io))

(define-public crate-pxl-build-0.0.9 (c (n "pxl-build") (v "0.0.9") (d (list (d (n "image") (r "^0.19.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.1") (d #t) (k 0)) (d (n "palette") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.3.8") (d #t) (k 0)) (d (n "pxl") (r "^0.0.9") (d #t) (k 2)) (d (n "quote") (r "^0.5.2") (d #t) (k 0)) (d (n "regex") (r "^1.0.1") (d #t) (k 0)) (d (n "syn") (r "^0.13.10") (d #t) (k 0)) (d (n "tempfile") (r "^3.0.2") (d #t) (k 2)))) (h "1mhcba0wh744xmgvvbp895yrj2mmjagpi9n0fpm1isv0rfann9h7")))

