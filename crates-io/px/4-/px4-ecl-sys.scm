(define-module (crates-io px #{4-}# px4-ecl-sys) #:use-module (crates-io))

(define-public crate-px4-ecl-sys-0.1.0 (c (n "px4-ecl-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.51") (d #t) (k 1)) (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "cpp-vwrap-gen") (r "^0.1") (d #t) (k 1)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0") (d #t) (k 1)))) (h "1fzck84xcb85wm1y7p6qh6i3w3l3l2vkqf2gp41w091ypwp78n9r") (l "px4-ecl")))

