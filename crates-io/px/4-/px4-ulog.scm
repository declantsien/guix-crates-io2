(define-module (crates-io px #{4-}# px4-ulog) #:use-module (crates-io))

(define-public crate-px4-ulog-0.1.0 (c (n "px4-ulog") (v "0.1.0") (h "0820g166wzmp0racqr0mi08zcssxg3mrc6fa92halg5q1i4vmf92")))

(define-public crate-px4-ulog-0.1.1 (c (n "px4-ulog") (v "0.1.1") (h "00wkddm0f1sly7h71l4534yh25v1c7bfs0dzp39va2dxld0g5mjg")))

