(define-module (crates-io px #{4-}# px4-ecl) #:use-module (crates-io))

(define-public crate-px4-ecl-0.1.0 (c (n "px4-ecl") (v "0.1.0") (d (list (d (n "nalgebra") (r "^0.18") (d #t) (k 0)) (d (n "px4-ecl-sys") (r "^0.1.0") (d #t) (k 0)))) (h "0iwcjjirad1b27md7fa6nnfhf1qz3kyzwmsyrai9l1h8pbxbgn1k")))

