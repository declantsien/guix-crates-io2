(define-module (crates-io px so pxsort) #:use-module (crates-io))

(define-public crate-pxsort-0.1.0 (c (n "pxsort") (v "0.1.0") (d (list (d (n "image") (r "^0.21.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2.15") (d #t) (k 0)))) (h "1p53nip3ad23i50v4jxvvhf5lxg7cxjbyggsajgbfms7h1h5lldq")))

(define-public crate-pxsort-0.2.0 (c (n "pxsort") (v "0.2.0") (d (list (d (n "image") (r "^0.21.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2.15") (d #t) (k 0)))) (h "0g4jdp26rvfzz316lbg1df94l1rr1r7rxnl5wynlwpg71ybsadpn")))

(define-public crate-pxsort-0.2.1 (c (n "pxsort") (v "0.2.1") (d (list (d (n "image") (r "^0.21.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2.15") (d #t) (k 0)))) (h "17m9xmzr3c5n48ic4kn83i02h011wx57gdbg6f1bw71y2la5vgfv")))

(define-public crate-pxsort-0.3.0 (c (n "pxsort") (v "0.3.0") (d (list (d (n "image") (r "^0.21.0") (d #t) (k 0)) (d (n "indicatif") (r "^0.11.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2.15") (d #t) (k 0)))) (h "01kycyc6d3glh5njwjkzjj8vva99vzyz4jmk1fhzadz4ylk6xcg5")))

(define-public crate-pxsort-0.3.1 (c (n "pxsort") (v "0.3.1") (d (list (d (n "image") (r "^0.21.0") (d #t) (k 0)) (d (n "indicatif") (r "^0.11.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2.15") (d #t) (k 0)))) (h "0di98ivfk72cplwalxpal5d37jpppw0vvzwzb1cwq7cx85s4q8r1")))

(define-public crate-pxsort-0.3.2 (c (n "pxsort") (v "0.3.2") (d (list (d (n "image") (r "^0.21.0") (d #t) (k 0)) (d (n "indicatif") (r "^0.11.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2.15") (d #t) (k 0)))) (h "05kxhvvi31paf53qy6x25ad7kay90674jxyf3gigc5sllkajnajg")))

(define-public crate-pxsort-0.3.3 (c (n "pxsort") (v "0.3.3") (d (list (d (n "image") (r "^0.21.0") (d #t) (k 0)) (d (n "indicatif") (r "^0.11.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2.15") (d #t) (k 0)))) (h "1q8ddhpwqac23gwnl72s3z87hqimzbg2znk5kq7yxnlha6f545p0")))

(define-public crate-pxsort-0.4.0 (c (n "pxsort") (v "0.4.0") (d (list (d (n "image") (r "^0.21.0") (d #t) (k 0)) (d (n "indicatif") (r "^0.11.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2.15") (d #t) (k 0)))) (h "0s6xy6ch07nzgrhs9zkp00hcqp7bfcsmpgz9i9qllaljf1vcri6z")))

(define-public crate-pxsort-0.4.1 (c (n "pxsort") (v "0.4.1") (d (list (d (n "image") (r "^0.21.0") (d #t) (k 0)) (d (n "indicatif") (r "^0.11.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2.15") (d #t) (k 0)))) (h "1akhxmcvr2avgd1zzg6fyin4995c9l8f6gzxm361si2rgw2nx6fq")))

(define-public crate-pxsort-0.4.2 (c (n "pxsort") (v "0.4.2") (d (list (d (n "image") (r "^0.21.0") (d #t) (k 0)) (d (n "indicatif") (r "^0.11.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2.15") (d #t) (k 0)))) (h "0xliaiws1nf62w5jylka7hpahwcsnpk9r3p3i7hqhmkvnslbkmly")))

(define-public crate-pxsort-0.5.0 (c (n "pxsort") (v "0.5.0") (d (list (d (n "image") (r "^0.21.0") (d #t) (k 0)) (d (n "indicatif") (r "^0.11.0") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "structopt") (r "^0.2.15") (d #t) (k 0)) (d (n "strum") (r "^0.15.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.15.0") (d #t) (k 0)))) (h "118n5z2b5gp9369zk530fd3469ngk94acnw7gwaqwmwl2s6cvndk")))

