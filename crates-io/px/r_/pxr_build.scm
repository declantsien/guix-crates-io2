(define-module (crates-io px r_ pxr_build) #:use-module (crates-io))

(define-public crate-pxr_build-0.1.0 (c (n "pxr_build") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.10") (d #t) (k 0)) (d (n "which") (r "^5.0") (d #t) (k 0)))) (h "1c1q1avkpyrs65driz0x8gicx4jfnqja76lgvnhcw6zsaxlwwvhx") (r "1.70")))

(define-public crate-pxr_build-0.1.1 (c (n "pxr_build") (v "0.1.1") (d (list (d (n "regex") (r "^1.10") (d #t) (k 0)) (d (n "which") (r "^5.0") (d #t) (k 0)))) (h "08xl1zp5qk8ckn7nwaymmndpasfzxlhc0aj7j78d810dd0kck0zj") (r "1.70")))

(define-public crate-pxr_build-0.1.2 (c (n "pxr_build") (v "0.1.2") (d (list (d (n "regex") (r "^1.10") (d #t) (k 0)) (d (n "which") (r "^5.0") (d #t) (k 0)))) (h "18lvy8kpck26a10yyb5idsl43kpq3hr0ihi2pszy66mflfj4lcib") (r "1.70")))

(define-public crate-pxr_build-0.2.0 (c (n "pxr_build") (v "0.2.0") (d (list (d (n "regex") (r "^1.10") (d #t) (k 0)) (d (n "which") (r "^6.0") (d #t) (k 0)))) (h "0vvsmxz2anx102p4y8pa5v40xnpqpyc3qm6fl5wyzwqfgv78h21m") (r "1.70")))

