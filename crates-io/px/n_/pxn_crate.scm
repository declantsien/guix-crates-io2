(define-module (crates-io px n_ pxn_crate) #:use-module (crates-io))

(define-public crate-pxn_crate-0.1.1 (c (n "pxn_crate") (v "0.1.1") (d (list (d (n "chrono") (r "0.*") (d #t) (k 0)) (d (n "env_logger") (r "0.*") (d #t) (k 0)) (d (n "log") (r "0.*") (d #t) (k 0)) (d (n "log-panics") (r "2.*") (d #t) (k 0)) (d (n "tempfile") (r "3.*") (d #t) (k 0)))) (h "0p3iy3b6nghf7j1wwi1vv4gg6rcddx8wlqyr2a1sarl0rq4w8wig")))

(define-public crate-pxn_crate-0.1.2 (c (n "pxn_crate") (v "0.1.2") (d (list (d (n "chrono") (r "0.*") (d #t) (k 0)) (d (n "env_logger") (r "0.*") (d #t) (k 0)) (d (n "log") (r "0.*") (d #t) (k 0)) (d (n "log-panics") (r "2.*") (d #t) (k 0)) (d (n "tempfile") (r "3.*") (d #t) (k 0)))) (h "1jbnb3ly1wirmbsqp1m2f0pssccs8pkivi7pv1v2wv3fyra30d4a")))

(define-public crate-pxn_crate-0.1.3 (c (n "pxn_crate") (v "0.1.3") (d (list (d (n "chrono") (r "0.*") (d #t) (k 0)) (d (n "env_logger") (r "0.*") (d #t) (k 0)) (d (n "log") (r "0.*") (d #t) (k 0)) (d (n "log-panics") (r "2.*") (d #t) (k 0)) (d (n "tempfile") (r "3.*") (d #t) (k 0)))) (h "1zm5vpvyyb5df8q6pxs5wc1qkzqm6ydxa5z7hl0kxmkv036yvbb2")))

(define-public crate-pxn_crate-0.1.4 (c (n "pxn_crate") (v "0.1.4") (d (list (d (n "chrono") (r "0.*") (d #t) (k 0)) (d (n "env_logger") (r "0.*") (d #t) (k 0)) (d (n "log") (r "0.*") (d #t) (k 0)) (d (n "log-panics") (r "2.*") (d #t) (k 0)) (d (n "tempfile") (r "3.*") (d #t) (k 0)))) (h "19ngs7vvgyhc57fw1dqlq56hj29jipxma817hciibry4w216pl7b")))

(define-public crate-pxn_crate-0.1.0 (c (n "pxn_crate") (v "0.1.0") (d (list (d (n "chrono") (r "0.*") (d #t) (k 0)) (d (n "env_logger") (r "0.*") (d #t) (k 0)) (d (n "log") (r "0.*") (d #t) (k 0)) (d (n "log-panics") (r "2.*") (d #t) (k 0)) (d (n "tempfile") (r "3.*") (d #t) (k 0)))) (h "0v137x5jrxmn3z642mx15pbmhnmfhkcav7asq7alspawn3s3ljin")))

(define-public crate-pxn_crate-0.1.5 (c (n "pxn_crate") (v "0.1.5") (d (list (d (n "chrono") (r "0.*") (d #t) (k 0)) (d (n "env_logger") (r "0.*") (d #t) (k 0)) (d (n "log") (r "0.*") (d #t) (k 0)) (d (n "log-panics") (r "2.*") (d #t) (k 0)) (d (n "tempfile") (r "3.*") (d #t) (k 0)))) (h "0m8564xhv68knvy3zqc0x8n6jibhpz515r4srvhs3y3lvg131i20")))

