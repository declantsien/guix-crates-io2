(define-module (crates-io px to pxtone) #:use-module (crates-io))

(define-public crate-pxtone-0.1.0 (c (n "pxtone") (v "0.1.0") (d (list (d (n "cpal") (r "^0.13") (d #t) (k 2)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "pxtone-sys") (r "^0.1") (d #t) (k 0)))) (h "1pcdd8ymrkilwhr7i4hizrgcfcsigwcya8k8s642s6v3a6vqfdw3")))

