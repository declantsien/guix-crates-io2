(define-module (crates-io px re pxrem) #:use-module (crates-io))

(define-public crate-pxrem-0.1.0 (c (n "pxrem") (v "0.1.0") (d (list (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "structopt") (r "^0.2.16") (d #t) (k 0)))) (h "12i8dbq6i07fwmibs1glvshz1k4pl1ad112885a2ibxm5v4rnmcl")))

(define-public crate-pxrem-0.2.0 (c (n "pxrem") (v "0.2.0") (d (list (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "structopt") (r "^0.2.16") (d #t) (k 0)))) (h "0wny65l0bjvwzab7rkrhmiqnd4bxvd1721ysxq6l53rr9dagkdgm")))

(define-public crate-pxrem-0.2.1 (c (n "pxrem") (v "0.2.1") (d (list (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "structopt") (r "^0.2.16") (d #t) (k 0)))) (h "02pizhyal3si1wg0czw99snpmxb30yg45yv302bpm4spdwpkdrby")))

(define-public crate-pxrem-0.2.2 (c (n "pxrem") (v "0.2.2") (d (list (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "structopt") (r "^0.2.16") (d #t) (k 0)))) (h "0fy5cvmhnqy1d7xsbwlsy23rk8yhcnn100c9z14cg44q2269z02b")))

(define-public crate-pxrem-0.3.2 (c (n "pxrem") (v "0.3.2") (d (list (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "structopt") (r "^0.2.16") (d #t) (k 0)))) (h "18a4yz9flr135i1pn11b9zkwdd9pyp8ifb5himhjqz3w0bvpg2mh")))

(define-public crate-pxrem-0.3.3 (c (n "pxrem") (v "0.3.3") (d (list (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "structopt") (r "^0.2.16") (d #t) (k 0)))) (h "0x98az03l0yifnrmszjnzm64h4cjphdgjz5wc7218pmm7g3lxa15")))

