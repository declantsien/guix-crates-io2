(define-module (crates-io px p- pxp-parser) #:use-module (crates-io))

(define-public crate-pxp-parser-0.0.0-b4 (c (n "pxp-parser") (v "0.0.0-b4") (d (list (d (n "ariadne") (r "^0.1.5") (d #t) (k 0)) (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.149") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.89") (d #t) (k 0)))) (h "0wi29zzlzgzz02ylya9qcjnm8p530nimrz0nk8n7hqgigii8f1z8")))

