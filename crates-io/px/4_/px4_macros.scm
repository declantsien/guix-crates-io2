(define-module (crates-io px #{4_}# px4_macros) #:use-module (crates-io))

(define-public crate-px4_macros-0.1.0 (c (n "px4_macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "0zyimcfyqb2196xckqw7dnkyw75s1jjs763vjsdal4cicqha4cph")))

(define-public crate-px4_macros-0.2.1 (c (n "px4_macros") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "0khx0q50xidd3n726imimsxp9xq8mi8s7sbiaim6p1y5505kj007")))

(define-public crate-px4_macros-0.2.4 (c (n "px4_macros") (v "0.2.4") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "02yv5snijndcs1fdm1rz4w9x42jggw82fmnj2blnk0iybr2byz70")))

