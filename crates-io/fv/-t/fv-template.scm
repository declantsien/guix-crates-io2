(define-module (crates-io fv -t fv-template) #:use-module (crates-io))

(define-public crate-fv-template-0.0.0 (c (n "fv-template") (v "0.0.0") (h "1hkngl0qh29zsvffxv97ncvvwl8xpqvk67h6826nz9ysc4j7k18a")))

(define-public crate-fv-template-0.1.0 (c (n "fv-template") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "visit-mut"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1szs97p05w6gvlc2zsqsmpvr422x6ki1pk753cdirrfrjdkpx7m0")))

(define-public crate-fv-template-0.2.0 (c (n "fv-template") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "visit-mut"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0iszrgdfdi0qrcs1h4z7sphp156nkxhjd2n0rj640hr84p22yk81")))

(define-public crate-fv-template-0.2.1 (c (n "fv-template") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "visit-mut"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0bqkjasd2iqgdan9990mp6cngi5965qy4yfibbwnbh750hgkv4gf")))

(define-public crate-fv-template-0.2.2 (c (n "fv-template") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "visit-mut"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0sx7k846i0xyp3jfj2rj9g9b1vxi78zrlbmz07xf3ajpccxb4h97")))

(define-public crate-fv-template-0.3.0 (c (n "fv-template") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "visit-mut"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1v9fj6icn62s1lg7f4a2j2v4hrqsllhghra716gqgqmlkapi3a3q")))

(define-public crate-fv-template-0.4.0 (c (n "fv-template") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "02k7l2d83a839k5v61kyn536aqb2k26j1l4vq7drxrzjyvliyxva")))

