(define-module (crates-io fv oi fvoid) #:use-module (crates-io))

(define-public crate-fvoid-0.1.0 (c (n "fvoid") (v "0.1.0") (h "03bg3bp85zypclrshaaj8fr3v9avfg8fp7agj590gk20lby0yrx6")))

(define-public crate-fvoid-0.2.4 (c (n "fvoid") (v "0.2.4") (h "1wvbv2vbsgrqx4jpxwwvrd3r1y7gyv3a4f488l4as1s9bwy4ns29")))

