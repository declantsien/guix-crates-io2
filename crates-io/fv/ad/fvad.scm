(define-module (crates-io fv ad fvad) #:use-module (crates-io))

(define-public crate-fvad-0.1.0 (c (n "fvad") (v "0.1.0") (d (list (d (n "libfvad-sys") (r "^1.0.0") (d #t) (k 0)))) (h "0sgnssy9h4m2z1wmg4zvihkz423hcy2xyihpak53421fzw1x6paa") (y #t)))

(define-public crate-fvad-0.1.1 (c (n "fvad") (v "0.1.1") (d (list (d (n "libfvad-sys") (r "^1.0.0") (d #t) (k 0)))) (h "04jq2j7r1v5z6phqjrjh2im829pmmpy5d2rrd7zv9acmmkp2xmn3")))

(define-public crate-fvad-0.1.2 (c (n "fvad") (v "0.1.2") (d (list (d (n "libfvad-sys") (r "^1.0.0") (d #t) (k 0)))) (h "16j8isj6cshpcxbk0m60x1pgafcksc6121sb4yy8srnxan965fh6")))

(define-public crate-fvad-0.1.3 (c (n "fvad") (v "0.1.3") (d (list (d (n "libfvad-sys") (r "^1.0.0") (d #t) (k 0)))) (h "0fklqi67vs50i599l5j6xq6pdf2cwh55lmsnjj6rdniihz7h93lb")))

