(define-module (crates-io fv m- fvm-2d) #:use-module (crates-io))

(define-public crate-fvm-2d-0.1.0 (c (n "fvm-2d") (v "0.1.0") (d (list (d (n "plotpy") (r "^0.6.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "voronator") (r "^0.2.1") (d #t) (k 0)))) (h "0jcl8jph39iarq80b8iwibk6f74nsh74lskyhk74ahzr7szw4d3c")))

