(define-module (crates-io fv m- fvm-mock) #:use-module (crates-io))

(define-public crate-fvm-mock-1.0.0 (c (n "fvm-mock") (v "1.0.0") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "fvm-std") (r "^1.0.0") (d #t) (k 0)) (d (n "scale") (r "^3.1") (f (quote ("derive"))) (k 0) (p "parity-scale-codec")) (d (n "scale-info") (r "^2.1") (f (quote ("derive" "serde" "decode"))) (k 0)) (d (n "sha3") (r "^0.10.2") (d #t) (k 0)) (d (n "fvm-abi") (r "^1.0.0") (d #t) (k 2)) (d (n "fvm-std") (r "^1.0.0") (f (quote ("advance"))) (d #t) (k 2)))) (h "1v66cjqpbjc6rky2hqr99kp05cd82g7wclw61vd5jr2i2gvy1q6p")))

