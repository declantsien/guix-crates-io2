(define-module (crates-io fv m- fvm-abi) #:use-module (crates-io))

(define-public crate-fvm-abi-1.0.0 (c (n "fvm-abi") (v "1.0.0") (d (list (d (n "scale-info") (r "^2.1") (f (quote ("derive" "serde" "decode"))) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "alloc"))) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (d #t) (k 0)))) (h "181xzidvjzz24qnyzvciicpsg591ii79m2yxk3jjfgjl97ikjv87")))

