(define-module (crates-io fv m- fvm-std) #:use-module (crates-io))

(define-public crate-fvm-std-1.0.0 (c (n "fvm-std") (v "1.0.0") (d (list (d (n "borsh") (r "^0.10.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "fixed-hash") (r "^0.8.0") (k 0)) (d (n "scale") (r "^3.1") (f (quote ("derive"))) (k 0) (p "parity-scale-codec")) (d (n "scale-info") (r "^2.1") (f (quote ("derive" "serde" "decode"))) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "sha2") (r "^0.10.6") (o #t) (d #t) (k 0)))) (h "1wzv8v498kyh9j8icbrrk1bw6hldnxp3r7zk1c3xizfrjc3hzfmd") (f (quote (("advance"))))))

