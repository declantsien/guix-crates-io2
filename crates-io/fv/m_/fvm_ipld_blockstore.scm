(define-module (crates-io fv m_ fvm_ipld_blockstore) #:use-module (crates-io))

(define-public crate-fvm_ipld_blockstore-0.1.0 (c (n "fvm_ipld_blockstore") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.51") (d #t) (k 0)) (d (n "cid") (r "^0.8.2") (f (quote ("serde-codec" "std"))) (k 0)) (d (n "multihash") (r "^0.16.1") (f (quote ("blake2b" "multihash-impl"))) (k 2)))) (h "0y15607axzvvw5wp7zxlgzxmj50ll9r5q4ff6m2zbyy2ivjym18r") (f (quote (("default"))))))

(define-public crate-fvm_ipld_blockstore-0.1.1 (c (n "fvm_ipld_blockstore") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.51") (d #t) (k 0)) (d (n "cid") (r "^0.8.2") (f (quote ("serde-codec" "std"))) (k 0)) (d (n "multihash") (r "^0.16.1") (f (quote ("multihash-impl"))) (k 0)))) (h "1xrdcz61fq13gsy4fn0zz5kzi5fppyd6iyd30mkpymwrc6lkk0k8") (f (quote (("default"))))))

(define-public crate-fvm_ipld_blockstore-0.1.2 (c (n "fvm_ipld_blockstore") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.51") (d #t) (k 0)) (d (n "cid") (r "^0.8.5") (f (quote ("serde-codec" "std"))) (k 0)) (d (n "multihash") (r "^0.16.1") (f (quote ("multihash-impl"))) (k 0)))) (h "1g8mwm2gd67zjbhnn6qmxbv38fac9p9h4n3mm7hl72dmw9dwgs7y") (f (quote (("default"))))))

(define-public crate-fvm_ipld_blockstore-0.2.0 (c (n "fvm_ipld_blockstore") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "cid") (r "^0.10.1") (f (quote ("serde-codec" "std"))) (k 0)) (d (n "multihash") (r "^0.18.1") (f (quote ("multihash-impl"))) (k 0)))) (h "0874zzi3v1v6426lnj0g88b2qjpi8v7y2ql4vrl9m7svj7v54zs1") (f (quote (("default"))))))

(define-public crate-fvm_ipld_blockstore-0.2.1 (c (n "fvm_ipld_blockstore") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "cid") (r "^0.10.1") (f (quote ("serde-codec" "std"))) (k 0)) (d (n "multihash") (r "^0.18.1") (f (quote ("multihash-impl"))) (k 0)))) (h "1zfvmdz57wmn01r32da1cydv2bifqfkalfqmg89wqphg89bvjr6h") (f (quote (("default"))))))

