(define-module (crates-io zv t_ zvt_builder) #:use-module (crates-io))

(define-public crate-zvt_builder-0.1.0 (c (n "zvt_builder") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.43") (d #t) (k 0)) (d (n "yore") (r "^1.0.2") (d #t) (k 0)) (d (n "zvt_derive") (r "^0.1.0") (d #t) (k 0)))) (h "17q5r4p8xq6pxfic8r9vgk5h8j5sc3wrvsqlw0njyhs2hi8lwid4")))

