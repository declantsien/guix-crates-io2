(define-module (crates-io zv t_ zvt_derive) #:use-module (crates-io))

(define-public crate-zvt_derive-0.1.0 (c (n "zvt_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.58") (d #t) (k 0)) (d (n "quote") (r "^1.0.27") (d #t) (k 0)) (d (n "syn") (r "^2.0.16") (d #t) (k 0)))) (h "08zh3z948fh4gaq4742by1pvd05klm0bflniyknky639f7qjkdna")))

