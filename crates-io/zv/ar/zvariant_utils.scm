(define-module (crates-io zv ar zvariant_utils) #:use-module (crates-io))

(define-public crate-zvariant_utils-1.0.0 (c (n "zvariant_utils") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "12qaci07qpcbimrfwdw535kxswiqqvqr4v1vgahjilf4vf9jkcjk") (r "1.60")))

(define-public crate-zvariant_utils-1.0.1 (c (n "zvariant_utils") (v "1.0.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "00625h3240rixvfhq6yhws1d4bwf3vrf74v8s69b97aq27cg0d3j") (r "1.60")))

(define-public crate-zvariant_utils-1.1.0 (c (n "zvariant_utils") (v "1.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0wl17k3jd0z66grkc3nfrrd24l35pjhy5zkk312i5k4kl4bdpgh0") (r "1.75")))

(define-public crate-zvariant_utils-1.1.1 (c (n "zvariant_utils") (v "1.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0nkxvv1g9vrajjncgpsnb0q9cvgirdw9vk4p9wyd336npn8p5ykm") (r "1.75")))

(define-public crate-zvariant_utils-2.0.0 (c (n "zvariant_utils") (v "2.0.0") (d (list (d (n "proc-macro2") (r "^1.0.81") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.64") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "11l7vwdkkd56hhaqf4n3xlr830mswh4piap7mpcjphpghyq2s97w") (r "1.75")))

