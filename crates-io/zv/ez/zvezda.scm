(define-module (crates-io zv ez zvezda) #:use-module (crates-io))

(define-public crate-zvezda-0.1.1 (c (n "zvezda") (v "0.1.1") (h "0q48sblnrinqngs4d334ckngndp8qnrnzl29l93lffix0bc8b44y")))

(define-public crate-zvezda-0.1.2 (c (n "zvezda") (v "0.1.2") (h "0qjr14w6pfsj5aq710g5hxwrgzsr58gq21ngpgh44jcn2hlkgphq")))

(define-public crate-zvezda-0.1.3 (c (n "zvezda") (v "0.1.3") (h "03q6aa6dhfvwx4yv34bjvxymyv7ymwsl2hhb91p4p7l6sim30mj3")))

(define-public crate-zvezda-0.1.4 (c (n "zvezda") (v "0.1.4") (h "0pfdm82yiigwqzma10yp915va5x3qyh6cqmnk1lyp9ih6x8pi1yk")))

