(define-module (crates-io ny ar nyar-collection) #:use-module (crates-io))

(define-public crate-nyar-collection-0.0.0 (c (n "nyar-collection") (v "0.0.0") (d (list (d (n "im") (r "^15.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "shredder") (r "^0.2.0") (d #t) (k 0)))) (h "1vzjz4x759v7hrjhr21v0f5j0ql3anzq30c2j78h3zb3w97b8pw5") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-nyar-collection-0.0.1 (c (n "nyar-collection") (v "0.0.1") (d (list (d (n "im") (r "^15.1.0") (d #t) (k 0)) (d (n "nyar-error") (r "0.1.*") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "shredder") (r "^0.2.0") (d #t) (k 0)))) (h "12jh8p8jw7ng15dhmhys2lj15qk41vz4zqyxc7nrrql2csvl4pfk") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

