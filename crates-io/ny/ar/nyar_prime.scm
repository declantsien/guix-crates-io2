(define-module (crates-io ny ar nyar_prime) #:use-module (crates-io))

(define-public crate-nyar_prime-0.0.1 (c (n "nyar_prime") (v "0.0.1") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "num-bigint") (r "^0.1.39") (d #t) (k 0)) (d (n "primal") (r "^0.2.3") (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 0)))) (h "1dv66cxd297j2fy4x40q9rfjq7dcmlvbfw3j9vh60a6cjzbc81ps")))

(define-public crate-nyar_prime-0.0.2 (c (n "nyar_prime") (v "0.0.2") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "num-bigint") (r "^0.1.39") (d #t) (k 0)) (d (n "primal") (r "^0.2.3") (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 0)))) (h "1yhydwa0i74vmjrxhhlvh9s7qbvfbl7pq3z9lxd6dl791qx18936")))

(define-public crate-nyar_prime-0.0.3 (c (n "nyar_prime") (v "0.0.3") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "num-bigint") (r "^0.1.39") (d #t) (k 0)) (d (n "primal") (r "^0.2.3") (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 0)))) (h "0r9vg0ljmwll0zzkfjnpv5pnyl1snr66f00cl72dvz56jgjaszs1")))

(define-public crate-nyar_prime-0.1.0 (c (n "nyar_prime") (v "0.1.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "primal") (r "^0.2.3") (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 0)))) (h "1c62b6rv66bqz23awa1vraa9x6ahjvda612r91gz5c9fklyyf8vl")))

