(define-module (crates-io ny ar nyar-runtime) #:use-module (crates-io))

(define-public crate-nyar-runtime-0.0.1 (c (n "nyar-runtime") (v "0.0.1") (d (list (d (n "indexmap") (r "^1.6") (d #t) (k 0)))) (h "0272qqmkqdhfsasf6pgjqvhpv3q53p3icwl2gj8j2mpb3l6kg443")))

(define-public crate-nyar-runtime-0.0.2 (c (n "nyar-runtime") (v "0.0.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "wasmtime") (r "^18.0.1") (f (quote ("component-model" "async"))) (d #t) (k 0)) (d (n "wasmtime-wasi") (r "^18.0.1") (f (quote ("tokio"))) (d #t) (k 0)))) (h "1yr6cylii45kcj34y4iaalccavnc3vwqsv5yv8wmdcd94qbkwadr")))

