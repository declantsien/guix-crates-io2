(define-module (crates-io ny ar nyar-macro) #:use-module (crates-io))

(define-public crate-nyar-macro-0.1.0 (c (n "nyar-macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full" "parsing" "printing" "proc-macro" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.63") (d #t) (k 2)))) (h "1iri35n4yhxg5iahdljzm1n1125wlg05xr7ggkm56mhwcrj0g33z") (f (quote (("default"))))))

(define-public crate-nyar-macro-0.1.1 (c (n "nyar-macro") (v "0.1.1") (d (list (d (n "syn") (r "^1.0.98") (f (quote ("full" "parsing" "printing" "proc-macro" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.63") (d #t) (k 2)))) (h "0mcs33w8jkwqzapbzvpgc1p3y2xim0wqcr54q8x4515y5rq24k0s") (f (quote (("default"))))))

