(define-module (crates-io ny ar nyar_valkyrie) #:use-module (crates-io))

(define-public crate-nyar_valkyrie-0.0.1 (c (n "nyar_valkyrie") (v "0.0.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "nyar_ast") (r "^0.0.1") (d #t) (k 0)) (d (n "pest") (r "^2.1.0") (d #t) (k 0)))) (h "0sj6az4rydkapxbqgbxiqpdkz268pzm07q2dxndq774jpw5wl329")))

(define-public crate-nyar_valkyrie-0.0.2 (c (n "nyar_valkyrie") (v "0.0.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "nyar_ast") (r "^0.0.1") (d #t) (k 0)) (d (n "pest") (r "^2.1.0") (d #t) (k 0)))) (h "0w41sqcwx4py6vsal3hfnj0h9y4liv1imllxyzz6fyff8i0qpxkk")))

(define-public crate-nyar_valkyrie-0.0.3 (c (n "nyar_valkyrie") (v "0.0.3") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "nyar_ast") (r "^0.0.2") (d #t) (k 0)) (d (n "pest") (r "^2.1.0") (d #t) (k 0)))) (h "0d7i2ifdkxdzblqb0llvhn50qv1gqpwr5lk44pn9gpfhi5bxjd0f")))

(define-public crate-nyar_valkyrie-0.0.4 (c (n "nyar_valkyrie") (v "0.0.4") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "nyar_ast") (r "^0.0.3") (d #t) (k 0)) (d (n "pest") (r "^2.1.0") (d #t) (k 0)))) (h "0zl370dyx2sljx7cp90pzrm2xv9wr7dvpsksrdi748y2d6mingqa")))

(define-public crate-nyar_valkyrie-0.0.5 (c (n "nyar_valkyrie") (v "0.0.5") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "nyar_ast") (r "^0.0.5") (d #t) (k 0)) (d (n "pest") (r "^2.1.0") (d #t) (k 0)))) (h "09q3z7vzxgy207z05zqldlbfwra8f0h2sv6l5vrjq43ldyq3g5j0")))

(define-public crate-nyar_valkyrie-0.0.6 (c (n "nyar_valkyrie") (v "0.0.6") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "nyar_ast") (r "^0.0.6") (d #t) (k 0)) (d (n "pest") (r "^2.1.0") (d #t) (k 0)))) (h "0qfj8ngbjhv35p1giwjg7m52f13d4idyfng1rk43k0i8bnvfbnpa")))

(define-public crate-nyar_valkyrie-0.0.7 (c (n "nyar_valkyrie") (v "0.0.7") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "nyar_ast") (r "^0.0.7") (d #t) (k 0)) (d (n "pest") (r "^2.1.0") (d #t) (k 0)))) (h "08w41q1y558n2ifgls2iaivx5gd7vc4db07msp4bqyzikv108yp8")))

(define-public crate-nyar_valkyrie-0.0.8 (c (n "nyar_valkyrie") (v "0.0.8") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "nyar_ast") (r "^0.0.8") (d #t) (k 0)) (d (n "pest") (r "^2.1.0") (d #t) (k 0)))) (h "0jnxf5hrvyc9l2ia4085hi5qj3v60rwka5brrzbb3555rrka15ms")))

