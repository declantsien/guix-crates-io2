(define-module (crates-io ny ar nyar-wasm) #:use-module (crates-io))

(define-public crate-nyar-wasm-0.0.0 (c (n "nyar-wasm") (v "0.0.0") (d (list (d (n "indexmap") (r "^2.1.0") (d #t) (k 0)) (d (n "wasm-encoder") (r "^0.38.1") (d #t) (k 0)) (d (n "wasmprinter") (r "^0.2.75") (d #t) (k 0)) (d (n "wasmtime") (r "^15.0.1") (f (quote ("component-model"))) (d #t) (k 0)))) (h "1j4vg2rap37i5i00axqs8j3qfkzlm4xfbrg5rdp7a27873bzlwfw") (f (quote (("default"))))))

(define-public crate-nyar-wasm-0.0.1 (c (n "nyar-wasm") (v "0.0.1") (d (list (d (n "indexmap") (r "^2.1.0") (d #t) (k 0)) (d (n "nyar-error") (r "0.1.*") (d #t) (k 0)) (d (n "nyar-hir") (r "0.2.*") (d #t) (k 0)) (d (n "wast") (r "^69.0.1") (d #t) (k 0)))) (h "1zj1g5igc8cs2v7bcswxkqnx1b4fygsxz0kyh2k5cgw5h3h6pc3a") (f (quote (("default"))))))

(define-public crate-nyar-wasm-0.0.2 (c (n "nyar-wasm") (v "0.0.2") (d (list (d (n "indexmap") (r "^2.1.0") (d #t) (k 0)) (d (n "nyar-error") (r "0.1.*") (d #t) (k 0)) (d (n "wast") (r "^69.0.1") (d #t) (k 0)))) (h "0z2b3di9jrq08vig7x8gsjckficpn7nsl24gfw266xj533magajh") (f (quote (("default"))))))

(define-public crate-nyar-wasm-0.0.3 (c (n "nyar-wasm") (v "0.0.3") (d (list (d (n "indexmap") (r "^2.1.0") (d #t) (k 0)) (d (n "nyar-error") (r "0.1.*") (d #t) (k 0)) (d (n "wast") (r "^69.0.1") (d #t) (k 0)))) (h "11vd5l5nxz88ync4ssq1cqfxw42ivl86jj5p3qgzy5ppbrzs46ld") (f (quote (("default"))))))

(define-public crate-nyar-wasm-0.0.4 (c (n "nyar-wasm") (v "0.0.4") (d (list (d (n "indexmap") (r "^2.2.3") (d #t) (k 0)) (d (n "nyar-error") (r "0.1.*") (d #t) (k 0)) (d (n "wast") (r "^200.0.0") (d #t) (k 0)))) (h "1hv8w101l44s3xi0gfd2b7dp715kwc2ksnyswy3xkngfzdnyw4hn") (f (quote (("default"))))))

(define-public crate-nyar-wasm-0.0.5 (c (n "nyar-wasm") (v "0.0.5") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "dependent_sort") (r "^0.2.0") (d #t) (k 0)) (d (n "indexmap") (r "^2.2.3") (d #t) (k 0)) (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "nyar-error") (r "0.1.*") (d #t) (k 0)) (d (n "semver") (r "^1.0.22") (d #t) (k 0)))) (h "0wh5nim3yywsmfg1glnrmp1lskhq7l5c6qbm6llwwha5fqn4i6ia") (f (quote (("default"))))))

(define-public crate-nyar-wasm-0.0.6 (c (n "nyar-wasm") (v "0.0.6") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "dependent_sort") (r "^0.2.0") (d #t) (k 0)) (d (n "indexmap") (r "^2.2.5") (d #t) (k 0)) (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "nyar-error") (r "0.1.*") (d #t) (k 0)) (d (n "semver") (r "^1.0.22") (d #t) (k 0)) (d (n "string-pool") (r "^0.2.1") (d #t) (k 0)) (d (n "wast") (r "^201.0.0") (d #t) (k 0)))) (h "1r0gxd8d1xypb85lgrbv2ji4slky1xkj6c7w0c1ks8ian5mkxn4r") (f (quote (("default"))))))

(define-public crate-nyar-wasm-0.0.7 (c (n "nyar-wasm") (v "0.0.7") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "dependent_sort") (r "^0.2.0") (d #t) (k 0)) (d (n "indexmap") (r "^2.2.3") (d #t) (k 0)) (d (n "itertools") (r "^0.13.0") (d #t) (k 0)) (d (n "nyar-error") (r "0.1.*") (d #t) (k 0)) (d (n "semver") (r "^1.0.22") (d #t) (k 0)) (d (n "string-pool") (r "^0.2.1") (d #t) (k 0)) (d (n "wast") (r "^208.0.1") (d #t) (k 0)))) (h "18fg22vfijzw6s05r4mbgr76p8cs5nx5n29rgq95sby59i9b14gk") (f (quote (("default"))))))

