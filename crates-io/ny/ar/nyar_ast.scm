(define-module (crates-io ny ar nyar_ast) #:use-module (crates-io))

(define-public crate-nyar_ast-0.0.1 (c (n "nyar_ast") (v "0.0.1") (d (list (d (n "serde") (r "^1.0.104") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.104") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.45") (d #t) (k 0)))) (h "0bhgf9rf3gkggvar58gl8r8ajmkyk3jg06w68g11cx6kk6ln0nmd")))

(define-public crate-nyar_ast-0.0.2 (c (n "nyar_ast") (v "0.0.2") (d (list (d (n "serde") (r "^1.0.104") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.104") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.45") (d #t) (k 0)))) (h "0iy4gdadx07ykbyyy9f42ld9lqah58dzkrk4x1cvhjmf3mhxwaqw")))

(define-public crate-nyar_ast-0.0.3 (c (n "nyar_ast") (v "0.0.3") (d (list (d (n "serde") (r "^1.0.104") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.104") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.45") (d #t) (k 0)))) (h "1qavk7s39f09l43ywi60rlx1xgwbaba4pzdph4c8wjc4mnwafr36")))

(define-public crate-nyar_ast-0.0.5 (c (n "nyar_ast") (v "0.0.5") (d (list (d (n "num") (r "^0.2.0") (f (quote ("serde" "std"))) (k 0)) (d (n "serde") (r "^1.0.104") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.104") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.45") (d #t) (k 0)))) (h "0gvmi60fqhmy420gxkqvkkqgamvjd52y2dqhn55pdqz14q9a32g9")))

(define-public crate-nyar_ast-0.0.6 (c (n "nyar_ast") (v "0.0.6") (d (list (d (n "num") (r "^0.2.1") (f (quote ("serde" "std"))) (k 0)) (d (n "serde") (r "^1.0.104") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.104") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.45") (d #t) (k 0)))) (h "0y1v6hbvh7qs3a5n1j11g0jk90wc7jrcp9wsc5k069qfg7msgz9a")))

(define-public crate-nyar_ast-0.0.7 (c (n "nyar_ast") (v "0.0.7") (d (list (d (n "num") (r "^0.2.1") (f (quote ("serde" "std"))) (k 0)) (d (n "serde") (r "^1.0.104") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.104") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.45") (d #t) (k 0)))) (h "02zvcgwlh5qyxs3khhvy1vajfl2ybmhlyl7vjf7rwkhah04m79kw")))

(define-public crate-nyar_ast-0.0.8 (c (n "nyar_ast") (v "0.0.8") (d (list (d (n "num") (r "^0.2.1") (f (quote ("serde" "std"))) (k 0)) (d (n "serde") (r "^1.0.104") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.104") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.45") (d #t) (k 0)))) (h "16jgl5mp89yi4wqn11f7qpd9bjnbfifqlbg6k6xj0872pnsakqjy")))

(define-public crate-nyar_ast-0.1.0 (c (n "nyar_ast") (v "0.1.0") (d (list (d (n "num") (r "^0.2.1") (f (quote ("serde" "std"))) (k 0)) (d (n "serde") (r "^1.0.104") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.104") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.45") (d #t) (k 0)))) (h "03ghl8wi1zsa26yq6wyp84j9hw0yi72bd3vpdqw08ychx8z3y0ri")))

