(define-module (crates-io ny ar nyar-hir) #:use-module (crates-io))

(define-public crate-nyar-hir-0.1.1 (c (n "nyar-hir") (v "0.1.1") (d (list (d (n "lsp-types") (r "^0.89") (d #t) (k 0)))) (h "1xc31hn3ng76z0cl9nws3qy63pf4fak5mhdngkd6zjakj18lngcd") (f (quote (("default"))))))

(define-public crate-nyar-hir-0.2.0 (c (n "nyar-hir") (v "0.2.0") (d (list (d (n "rkyv") (r "^0.7.39") (d #t) (k 0)))) (h "0xd3amxk46dyqrm2qzkd610crw2hn5gwwcafh5hrqqb1aidcx8x0") (f (quote (("default"))))))

(define-public crate-nyar-hir-0.2.1 (c (n "nyar-hir") (v "0.2.1") (d (list (d (n "nyar-error") (r "^0.1.2") (d #t) (k 0)) (d (n "pretty") (r "^0.11.3") (f (quote ("termcolor"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.143") (f (quote ("derive"))) (d #t) (k 0)))) (h "1z5lgry3rbwqpiqpy6zc6han7846ld1p0xjj3zy9kbrjx8j2hwv1") (f (quote (("default"))))))

(define-public crate-nyar-hir-0.2.2 (c (n "nyar-hir") (v "0.2.2") (d (list (d (n "nyar-error") (r "^0.1.4") (d #t) (k 0)) (d (n "pretty") (r "^0.11.3") (f (quote ("termcolor"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)))) (h "14c2vxhl120zg1d73g4ss2l5vwm8csr4fg2dlq10rd1hmx4xh9w5") (f (quote (("default"))))))

(define-public crate-nyar-hir-0.2.3 (c (n "nyar-hir") (v "0.2.3") (d (list (d (n "indexmap") (r "^2.1.0") (d #t) (k 0)) (d (n "nyar-error") (r "0.1.*") (d #t) (k 0)) (d (n "wasmtime") (r "^15.0.1") (o #t) (d #t) (k 0)) (d (n "wasmtime-runtime") (r "^15.0.1") (o #t) (d #t) (k 0)))) (h "1wwnc498z425sp3l318n0ddnqc287akqaa9m8s2slqpvw7jwi3da") (f (quote (("wasm" "wasmtime" "wasmtime-runtime") ("default"))))))

(define-public crate-nyar-hir-0.2.4 (c (n "nyar-hir") (v "0.2.4") (d (list (d (n "indexmap") (r "^2.1.0") (d #t) (k 0)) (d (n "nyar-error") (r "0.1.*") (d #t) (k 0)))) (h "0prym2pwd5zzfkdvw5q4k0hfbbr6i0rhi0m4lhaqgkr9smp97inc") (f (quote (("default"))))))

