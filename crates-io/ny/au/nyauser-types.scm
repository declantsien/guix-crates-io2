(define-module (crates-io ny au nyauser-types) #:use-module (crates-io))

(define-public crate-nyauser-types-0.1.1 (c (n "nyauser-types") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "indexmap") (r "^1.9") (f (quote ("serde"))) (d #t) (k 0)) (d (n "regex") (r "^1.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "155sgab51hcwydmsyzfs4c3zn6a66l4iy6i8fj7255hlwrcli9a3")))

(define-public crate-nyauser-types-0.1.2 (c (n "nyauser-types") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "indexmap") (r "^1.9") (f (quote ("serde"))) (d #t) (k 0)) (d (n "regex") (r "^1.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1q0im1gmfhlijf76k09jjfrfgac2wj4hi17bqbbq9kpx50ya362s")))

