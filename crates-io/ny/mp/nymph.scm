(define-module (crates-io ny mp nymph) #:use-module (crates-io))

(define-public crate-nymph-0.1.0 (c (n "nymph") (v "0.1.0") (d (list (d (n "ndarray") (r "^0.13.0") (d #t) (k 0)) (d (n "ndarray-linalg") (r "^0.12") (d #t) (k 0)) (d (n "ordered-float") (r "^1") (d #t) (k 0)) (d (n "rayon") (r "1.2.*") (d #t) (k 0)))) (h "00imchmbza76qh22bngfzyl6i5n71f6wiyazb736623wcz29c2fg")))

(define-public crate-nymph-0.1.1 (c (n "nymph") (v "0.1.1") (d (list (d (n "approx") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "0.4.*") (d #t) (k 0)) (d (n "ndarray") (r "^0.13.0") (f (quote ("rayon"))) (d #t) (k 0)) (d (n "ndarray-linalg") (r "^0.12") (d #t) (k 0)) (d (n "openblas-src") (r "^0.7") (d #t) (k 0)) (d (n "ordered-float") (r "^1") (d #t) (k 0)) (d (n "rand") (r "0.6.*") (d #t) (k 0)) (d (n "rayon") (r "1.2.*") (d #t) (k 0)))) (h "06ay2zkndxyx97hs8s7m16ikrsv131rmc6f3wvhdkz28l73v6hdy")))

(define-public crate-nymph-0.1.2 (c (n "nymph") (v "0.1.2") (d (list (d (n "approx") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "ndarray") (r "^0.13.0") (f (quote ("rayon"))) (d #t) (k 0)) (d (n "ndarray-linalg") (r "^0.12") (d #t) (k 0)) (d (n "openblas-src") (r "^0.7") (d #t) (k 0)) (d (n "ordered-float") (r "^1") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "rayon") (r "1.2.*") (d #t) (k 0)))) (h "0lvlzcdbmj569h4sjb1dksn6p87gxa9v2nyx66rnhj6kff6am9p2")))

