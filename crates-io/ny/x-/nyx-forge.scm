(define-module (crates-io ny x- nyx-forge) #:use-module (crates-io))

(define-public crate-nyx-forge-0.1.0 (c (n "nyx-forge") (v "0.1.0") (d (list (d (n "libloading") (r "^0.8.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4.0") (d #t) (k 1)))) (h "07k2bm9g2xbdplk13y9slpac55rwnl05slh0yx0233vzg47blaq9") (y #t)))

