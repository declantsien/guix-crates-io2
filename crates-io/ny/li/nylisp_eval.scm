(define-module (crates-io ny li nylisp_eval) #:use-module (crates-io))

(define-public crate-nylisp_eval-0.1.0 (c (n "nylisp_eval") (v "0.1.0") (h "0pvafbs9cy1l2as9pgwrm68wrvkvwpps9c9wsgyqmjyb3bppm5bp")))

(define-public crate-nylisp_eval-0.2.0 (c (n "nylisp_eval") (v "0.2.0") (d (list (d (n "rand") (r "^0.7.3") (f (quote ("wasm-bindgen"))) (d #t) (k 0)))) (h "18qhykwwx4g1pb0pxnlbsfqb1xg97dicxgh461587f1lkp5r5msl")))

(define-public crate-nylisp_eval-0.2.1 (c (n "nylisp_eval") (v "0.2.1") (d (list (d (n "rand") (r "^0.7.3") (f (quote ("wasm-bindgen"))) (d #t) (k 0)))) (h "1zs8pka3w5dr9wd3pq8dc744a9w07byb20ys88jf13hklc0m6inv")))

(define-public crate-nylisp_eval-0.2.2 (c (n "nylisp_eval") (v "0.2.2") (d (list (d (n "rand") (r "^0.7.3") (f (quote ("wasm-bindgen"))) (d #t) (k 0)))) (h "04k09h97gfwdwpg28if3cd5m32wipdgjyy0jhcwg5wzf9xizfhpn")))

(define-public crate-nylisp_eval-0.2.3 (c (n "nylisp_eval") (v "0.2.3") (d (list (d (n "rand") (r "^0.7.3") (f (quote ("wasm-bindgen"))) (d #t) (k 0)))) (h "1y1zqx9mayabbsg15ccg9y5fab3dk2g8lixpaiijh2khr76b8cxh")))

(define-public crate-nylisp_eval-0.2.4 (c (n "nylisp_eval") (v "0.2.4") (d (list (d (n "rand") (r "^0.7.3") (f (quote ("wasm-bindgen"))) (d #t) (k 0)))) (h "08gr41bkrrayxvjdq31i0g23v1ayj8hih59al99a7xcdf4wfrinq")))

(define-public crate-nylisp_eval-0.2.5 (c (n "nylisp_eval") (v "0.2.5") (d (list (d (n "rand") (r "^0.7.3") (f (quote ("wasm-bindgen"))) (d #t) (k 0)))) (h "0269qakhzm0dasif4vfwnqbsaagab3wd662lmyh4invvx0lcl7lf")))

(define-public crate-nylisp_eval-0.2.6 (c (n "nylisp_eval") (v "0.2.6") (d (list (d (n "rand") (r "^0.7.3") (f (quote ("wasm-bindgen"))) (d #t) (k 0)))) (h "155y2c4m69aqis0q1zccc3wjljkrlf8wxdsbmmw29wvwaz1phih1")))

