(define-module (crates-io ny th nyth) #:use-module (crates-io))

(define-public crate-nyth-0.0.12 (c (n "nyth") (v "0.0.12") (d (list (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "handlebars") (r "^5.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)) (d (n "zip-extract") (r "^0.1.2") (d #t) (k 0)))) (h "0y77dmgnc2185v8m4d4vl77hs24ikzq2f7yk39yjnbnavw7lrw0y")))

(define-public crate-nyth-0.0.13 (c (n "nyth") (v "0.0.13") (d (list (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "handlebars") (r "^5.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)) (d (n "zip-extract") (r "^0.1.2") (d #t) (k 0)))) (h "1rj0yy2wdyzixn91rv6703d19dg59qsvp87hy225pg85iz2smpcv")))

(define-public crate-nyth-0.0.14 (c (n "nyth") (v "0.0.14") (d (list (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "handlebars") (r "^5.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)) (d (n "zip-extract") (r "^0.1.2") (d #t) (k 0)))) (h "1mf0xs3l4k0fsad0d958768dckvmxz609r7kz72fdw5ca3m30fp1")))

(define-public crate-nyth-0.0.15 (c (n "nyth") (v "0.0.15") (d (list (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "handlebars") (r "^5.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)) (d (n "zip-extract") (r "^0.1.2") (d #t) (k 0)))) (h "0j2w6dfn5k8z4aq2iz12a70yhpf8r7j1jxwmh16vk6gw4i07w0qb")))

(define-public crate-nyth-0.0.16 (c (n "nyth") (v "0.0.16") (d (list (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "handlebars") (r "^5.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)) (d (n "zip-extract") (r "^0.1.2") (d #t) (k 0)))) (h "1qb1js31s81dd7d0av939p7yhw2d65w58w0s9hy16v9kzxrzz0fq")))

(define-public crate-nyth-0.0.18 (c (n "nyth") (v "0.0.18") (d (list (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "handlebars") (r "^5.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)) (d (n "zip-extract") (r "^0.1.2") (d #t) (k 0)))) (h "144jpbpn0v5d74xbwm4z3n973qkglfl8wd8h3x3amz68igl4qsvb")))

(define-public crate-nyth-0.0.19 (c (n "nyth") (v "0.0.19") (d (list (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "handlebars") (r "^5.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)) (d (n "zip-extract") (r "^0.1.2") (d #t) (k 0)))) (h "0zsipwcyp8dqb0hli1g0xdvrrs7gyw8adj95w3r2mihisrfda47l")))

