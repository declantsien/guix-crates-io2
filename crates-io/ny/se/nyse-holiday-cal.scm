(define-module (crates-io ny se nyse-holiday-cal) #:use-module (crates-io))

(define-public crate-nyse-holiday-cal-0.1.0 (c (n "nyse-holiday-cal") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)))) (h "0qkidf0hnrhx9iy2fgj0bz9n4a3y9p0wcfl95nyjs6qyvq3wcy6k")))

(define-public crate-nyse-holiday-cal-0.2.0 (c (n "nyse-holiday-cal") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "time") (r "^0.3.20") (o #t) (d #t) (k 0)))) (h "1nlmgp7s8j5phql37bpgj47b4xaqx4phkrm4yqblb83lb2wpy0r6")))

(define-public crate-nyse-holiday-cal-0.2.1 (c (n "nyse-holiday-cal") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "time") (r "^0.3.20") (o #t) (d #t) (k 0)))) (h "1wzciia44951766l0ar8l2qqzcrys5qils5x61g90ws222m1z60j")))

