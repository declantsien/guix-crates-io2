(define-module (crates-io ny du nydus-error) #:use-module (crates-io))

(define-public crate-nydus-error-0.1.0 (c (n "nydus-error") (v "0.1.0") (d (list (d (n "backtrace") (r "^0.3") (d #t) (k 0)) (d (n "httpdate") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "serde") (r ">=1.0.27") (f (quote ("serde_derive" "rc"))) (d #t) (k 0)) (d (n "serde_json") (r ">=1.0.9") (d #t) (k 0)))) (h "0i58v6r6lfp9n6qybmdbl05ycazbg20ybwsgb5j4xsbhls8z6xac")))

(define-public crate-nydus-error-0.2.0 (c (n "nydus-error") (v "0.2.0") (d (list (d (n "backtrace") (r "^0.3") (d #t) (k 0)) (d (n "httpdate") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r ">=1.0.27") (f (quote ("serde_derive" "rc"))) (d #t) (k 0)) (d (n "serde_json") (r ">=1.0.9") (d #t) (k 0)))) (h "0237870wljidj4dk74igaw30hhqnh424hda40g5cgb18w272zzbr")))

(define-public crate-nydus-error-0.2.1 (c (n "nydus-error") (v "0.2.1") (d (list (d (n "backtrace") (r "^0.3") (d #t) (k 0)) (d (n "httpdate") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.110") (f (quote ("serde_derive" "rc"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.53") (d #t) (k 0)))) (h "0r2myxcf9sl9b73dq6pk7a01sqmcjn5q848i4ckzq7bpmgl70b2r")))

(define-public crate-nydus-error-0.2.2 (c (n "nydus-error") (v "0.2.2") (d (list (d (n "backtrace") (r "^0.3") (d #t) (k 0)) (d (n "httpdate") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.110") (f (quote ("serde_derive" "rc"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.53") (d #t) (k 0)))) (h "04vj9ygy67z92v9inh3gxaxy7n2qxmxp93pi266k51l24svhz5lh")))

(define-public crate-nydus-error-0.2.3 (c (n "nydus-error") (v "0.2.3") (d (list (d (n "backtrace") (r "^0.3") (d #t) (k 0)) (d (n "httpdate") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.110") (f (quote ("serde_derive" "rc"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.53") (d #t) (k 0)))) (h "0090x6m0zclpkwqr6k8nmgxv4wac553i0syaxzdpg4sqs7pw2bmf")))

