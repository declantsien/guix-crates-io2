(define-module (crates-io ny aa nyaa-rsearch) #:use-module (crates-io))

(define-public crate-nyaa-rsearch-0.1.0 (c (n "nyaa-rsearch") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "scraper") (r "^0.17.1") (d #t) (k 0)))) (h "0r3m92fqi6ww3hhfrsds6z896hxj57k61dyzr6a2ggkyva9bdhk5")))

(define-public crate-nyaa-rsearch-0.1.1 (c (n "nyaa-rsearch") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "scraper") (r "^0.17.1") (d #t) (k 0)))) (h "00qivsx51l4zm5b617q5grdx77lz1cyqyg694h0kvd54viq8hix2")))

(define-public crate-nyaa-rsearch-0.1.2 (c (n "nyaa-rsearch") (v "0.1.2") (d (list (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "scraper") (r "^0.17.1") (d #t) (k 0)))) (h "0ah3jqw1sm70mlwhykqlghakwyw1lhqg83wyphwphijr8sn57iqy")))

(define-public crate-nyaa-rsearch-0.1.3 (c (n "nyaa-rsearch") (v "0.1.3") (d (list (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "scraper") (r "^0.17.1") (d #t) (k 0)) (d (n "tokio") (r "^1.34.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0zqxz9n8rhdx2fvq1c5c1g9bp7jjbm4zb8ijsg5ywmbh1pq2z8cw")))

(define-public crate-nyaa-rsearch-0.1.4 (c (n "nyaa-rsearch") (v "0.1.4") (d (list (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "scraper") (r "^0.17.1") (d #t) (k 0)) (d (n "tokio") (r "^1.34.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0ym01r24hy6db0xg4cr1nznhgm2c6ji2dgd556ywpwgscawnfgav")))

(define-public crate-nyaa-rsearch-0.1.5 (c (n "nyaa-rsearch") (v "0.1.5") (d (list (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "scraper") (r "^0.17.1") (d #t) (k 0)) (d (n "tokio") (r "^1.34.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1rkysm6d2mps3ins47nfgx60naf6fc91g3ayvs5a22n6vqa3vh62")))

