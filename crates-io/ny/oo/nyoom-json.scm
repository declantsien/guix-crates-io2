(define-module (crates-io ny oo nyoom-json) #:use-module (crates-io))

(define-public crate-nyoom-json-0.1.0 (c (n "nyoom-json") (v "0.1.0") (d (list (d (n "itoa") (r "^1.0.6") (d #t) (k 0)) (d (n "ryu") (r "^1.0.13") (d #t) (k 0)) (d (n "sealed") (r "^0.5.0") (d #t) (k 0)))) (h "0kg5cjpz6vk4halfndzhgjgqzc6s3qmi1pz32mxdqawdpx4nyx0x") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-nyoom-json-0.1.1 (c (n "nyoom-json") (v "0.1.1") (d (list (d (n "itoa") (r "^1.0.6") (d #t) (k 0)) (d (n "ryu") (r "^1.0.13") (d #t) (k 0)) (d (n "sealed") (r "^0.5.0") (d #t) (k 0)))) (h "06hsrbq2v17ppkzrb3a84kk8xsxqa6kbbx75krln17dallcy647k") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-nyoom-json-0.2.0 (c (n "nyoom-json") (v "0.2.0") (d (list (d (n "itoa") (r "^1.0.6") (d #t) (k 0)) (d (n "ryu") (r "^1.0.13") (d #t) (k 0)) (d (n "sealed") (r "^0.5.0") (d #t) (k 0)))) (h "1n6qrvnql93jw76vangsaqk0gk8ami5qh0ajwdpsvcjsmpsrs8f4") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-nyoom-json-0.3.0 (c (n "nyoom-json") (v "0.3.0") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "itoa") (r "^1.0.6") (d #t) (k 0)) (d (n "ryu") (r "^1.0.13") (d #t) (k 0)) (d (n "sealed") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "strum") (r "^0.24") (f (quote ("derive"))) (d #t) (k 2)))) (h "0vm5kryn11czi995gh83znpbfd6w94z29cbpl97ixy6bxl9rjb1a") (f (quote (("default" "alloc") ("alloc"))))))

