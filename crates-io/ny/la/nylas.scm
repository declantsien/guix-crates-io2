(define-module (crates-io ny la nylas) #:use-module (crates-io))

(define-public crate-nylas-0.0.1 (c (n "nylas") (v "0.0.1") (d (list (d (n "url") (r "^2.4.1") (d #t) (k 0)))) (h "13bq1vbpfr1916qxgq93ax40nk9zgj3f0zbkxjagm45mpcid3jks")))

(define-public crate-nylas-0.0.2 (c (n "nylas") (v "0.0.2") (d (list (d (n "reqwest") (r "^0.11.22") (f (quote ("json"))) (d #t) (k 0)) (d (n "tokio") (r "^1.33.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "url") (r "^2.4.1") (d #t) (k 0)))) (h "07mhgv67iq85sfh6jjsiabl3b9hk78f29fjhn5mya2qknchf0fjl")))

(define-public crate-nylas-0.0.3 (c (n "nylas") (v "0.0.3") (d (list (d (n "reqwest") (r "^0.11.22") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.189") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "tokio") (r "^1.33.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "url") (r "^2.4.1") (d #t) (k 0)))) (h "0gl85qq5dghfpk1ww7i7ckkviqd9bz2lp382s8xshi02r95ysah0")))

(define-public crate-nylas-0.0.4 (c (n "nylas") (v "0.0.4") (d (list (d (n "reqwest") (r "^0.11.22") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.189") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "tokio") (r "^1.33.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "url") (r "^2.4.1") (d #t) (k 0)))) (h "0nddm0170v9b2vilc2wq54yrp1g46abvd4giis1ig6fi8sfix6ig")))

(define-public crate-nylas-0.0.5 (c (n "nylas") (v "0.0.5") (d (list (d (n "reqwest") (r "^0.11.22") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.189") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "tokio") (r "^1.33.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "url") (r "^2.4.1") (d #t) (k 0)))) (h "1x61dmsfdxmdvzn9np62s4cb6g9y1ys65gg866j780qazbmpfi38")))

(define-public crate-nylas-0.0.6 (c (n "nylas") (v "0.0.6") (d (list (d (n "reqwest") (r "^0.11.22") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.189") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "tokio") (r "^1.33.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "url") (r "^2.4.1") (d #t) (k 0)))) (h "1nziq7x60jcf31j0p61217bq46dflb85kl3hrnvf64agkxp3xj9y")))

(define-public crate-nylas-0.0.7 (c (n "nylas") (v "0.0.7") (d (list (d (n "reqwest") (r "^0.11.22") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.189") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "tokio") (r "^1.33.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "url") (r "^2.4.1") (d #t) (k 0)))) (h "1yyz3c7qr91ybrvb1dvafb1npwqmm1dr4a4ngbypkl9jc1g9hpwd")))

(define-public crate-nylas-0.0.8 (c (n "nylas") (v "0.0.8") (d (list (d (n "base64") (r "^0.21.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.22") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.189") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "url") (r "^2.4.1") (d #t) (k 0)))) (h "1prvfnk1z26b3rqr0h4ka7y36a7157vaxk8ycbysyvii5l64q5gz")))

