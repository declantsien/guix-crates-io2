(define-module (crates-io ny sa nysa) #:use-module (crates-io))

(define-public crate-nysa-0.1.0 (c (n "nysa") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (o #t) (d #t) (k 0)))) (h "17v357bwqag8iqz5vp495y7q5m914hbgmmkdgzd1w154yxzpd6n5") (f (quote (("global" "lazy_static") ("default" "global"))))))

(define-public crate-nysa-0.1.1 (c (n "nysa") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4.0") (o #t) (d #t) (k 0)))) (h "01ch7djxax1k8gqvsg8a9w6jd96dx22sw1adydkzp2wd5pa36dzn") (f (quote (("global" "lazy_static") ("default" "global"))))))

(define-public crate-nysa-0.1.2 (c (n "nysa") (v "0.1.2") (d (list (d (n "lazy_static") (r "^1.4.0") (o #t) (d #t) (k 0)))) (h "1ji1jk74m3n97pzr6gys9ffh9xkrgdyjfwdbx72fsfalwsyrzgyf") (f (quote (("global" "lazy_static") ("default" "global"))))))

(define-public crate-nysa-0.2.0 (c (n "nysa") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.4.0") (o #t) (d #t) (k 0)))) (h "17h92qkp2jgkkn98pvlvp0gzrsg83n0b3cl6z34gmjxi2dc7zlvn") (f (quote (("global" "lazy_static") ("default" "global"))))))

(define-public crate-nysa-0.2.1 (c (n "nysa") (v "0.2.1") (d (list (d (n "lazy_static") (r "^1.4.0") (o #t) (d #t) (k 0)))) (h "102cw00s1s48n6y63i6fhzycvy0100sd28q6aynz8bkicih76sll") (f (quote (("global" "lazy_static") ("default" "global"))))))

(define-public crate-nysa-0.2.2 (c (n "nysa") (v "0.2.2") (d (list (d (n "lazy_static") (r "^1.4.0") (o #t) (d #t) (k 0)))) (h "0g0jgxmlcjg379g1fxpz1gwfm2i8ymbzpagvcw14rk21j8lgg6dj") (f (quote (("global" "lazy_static") ("default" "global"))))))

