(define-module (crates-io ny an nyantrack-common) #:use-module (crates-io))

(define-public crate-nyantrack-common-0.1.0 (c (n "nyantrack-common") (v "0.1.0") (d (list (d (n "language-tags") (r "^0.3.2") (f (quote ("serde"))) (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1968nc31505f78lm35v7md11bq9nmcv5b4z0v8mjx5nn2132g0x6")))

