(define-module (crates-io ny an nyan) #:use-module (crates-io))

(define-public crate-nyan-0.0.1 (c (n "nyan") (v "0.0.1") (h "1d5n45xb6jfvjw8fzi7xsr8i4y3rkfd32iywivvazmsyn7pakxli") (y #t)))

(define-public crate-nyan-0.1.0 (c (n "nyan") (v "0.1.0") (d (list (d (n "rodio") (r "^0.10.0") (f (quote ("wav"))) (k 0)))) (h "1mhlj2gg2c6i45kdl6d4l749q4l92yfsig0cw2kj516rdaf6k8va") (y #t)))

(define-public crate-nyan-0.1.1 (c (n "nyan") (v "0.1.1") (d (list (d (n "rodio") (r "^0.10.0") (f (quote ("wav"))) (k 0)))) (h "1ai034s0cks904n0k8walxqf3gkbi19j6pnaybmkvxk10cby2mf0") (y #t)))

(define-public crate-nyan-0.1.2 (c (n "nyan") (v "0.1.2") (d (list (d (n "rodio") (r "^0.10.0") (f (quote ("wav"))) (k 0)))) (h "141agc2jaxj0qsy7l68hazhnrlnnq61pf4xpaxwbrgmp6yc2vmpj") (y #t)))

