(define-module (crates-io ny m- nym-sphinx-types) #:use-module (crates-io))

(define-public crate-nym-sphinx-types-0.1.0 (c (n "nym-sphinx-types") (v "0.1.0") (d (list (d (n "sphinx-packet") (r "^0.1.0") (d #t) (k 0)))) (h "0afhjk3pshryqsaxsavnrw4xb2yp1ixacv7g51pan9l93xz9cji8")))

(define-public crate-nym-sphinx-types-0.2.0 (c (n "nym-sphinx-types") (v "0.2.0") (d (list (d (n "sphinx-packet") (r "^0.1.0") (d #t) (k 0)))) (h "1k9sz80hvd2d15bj4ikfi7ybpyl3xlrl2m6s32hk07n01kjmmyj3")))

