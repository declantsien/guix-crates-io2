(define-module (crates-io ny m- nym-contracts-common) #:use-module (crates-io))

(define-public crate-nym-contracts-common-0.1.0 (c (n "nym-contracts-common") (v "0.1.0") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.0.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.0") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "19z2bibciigb4jn4d7986k92qvd6a38mwjp898p2904ry11raxdp")))

(define-public crate-nym-contracts-common-0.2.0 (c (n "nym-contracts-common") (v "0.2.0") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.0.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.0") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "14kpdw6agf76lbh05n9lbxqw43whjd9p1g37ynx0vwd306qlxq8i")))

(define-public crate-nym-contracts-common-0.3.0 (c (n "nym-contracts-common") (v "0.3.0") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.0.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.0") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0nvw95xaidbib7hlgyajkyhwg2xb9vip7p7a61wlchvi5c50faql")))

(define-public crate-nym-contracts-common-0.4.0 (c (n "nym-contracts-common") (v "0.4.0") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "cosmwasm-std") (r "=1.0.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.0") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0qv01525f2c89p1xdwl34lfambklyhfcbmyg87z101fpbpg46w0z")))

(define-public crate-nym-contracts-common-0.5.0 (c (n "nym-contracts-common") (v "0.5.0") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "cosmwasm-std") (r "=1.0.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.0") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0syhqbyzbv56jyq5nv8icgrdnl51r1jqsnbxmfagska0znx7lbx9")))

(define-public crate-nym-contracts-common-0.6.0 (c (n "nym-contracts-common") (v "0.6.0") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "cosmwasm-std") (r "=1.2.5") (d #t) (k 0)) (d (n "schemars") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.0") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1f8vd1d63f75wnxwrcyg9jllzr4m6wgr1bhqza4gyj8z07vajn6i")))

