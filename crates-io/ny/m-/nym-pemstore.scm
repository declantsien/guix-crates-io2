(define-module (crates-io ny m- nym-pemstore) #:use-module (crates-io))

(define-public crate-nym-pemstore-0.1.0 (c (n "nym-pemstore") (v "0.1.0") (d (list (d (n "pem") (r "^0.8") (d #t) (k 0)))) (h "0ycrjq1vldnbjbnz3lqyylzph7n8g40yqvzcij8q4c7nxp0agji4")))

(define-public crate-nym-pemstore-0.2.0 (c (n "nym-pemstore") (v "0.2.0") (d (list (d (n "pem") (r "^0.8") (d #t) (k 0)))) (h "0fvhqm2fipng43y0jjb0ik2zyxzg48m2gmr9vv0i8pgrjckwapmc")))

(define-public crate-nym-pemstore-0.3.0 (c (n "nym-pemstore") (v "0.3.0") (d (list (d (n "pem") (r "^0.8") (d #t) (k 0)))) (h "0bhbz2khp2acgpyi47z5pm06w306m6sfijzf4c82nvvaj3cn30df")))

