(define-module (crates-io ny st nyst) #:use-module (crates-io))

(define-public crate-nyst-0.1.0 (c (n "nyst") (v "0.1.0") (h "1vgqkkp71zzvzqzknyvbzpcx7vky2kws3ny480z8bppkhd77awjd") (y #t)))

(define-public crate-nyst-0.1.1 (c (n "nyst") (v "0.1.1") (d (list (d (n "regex") (r "^1.4.1") (d #t) (k 0)) (d (n "ron") (r "^0.6.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (d #t) (k 0)))) (h "0in24ncljcd8494zrnanvc253rqaj2i6qbmlkav21nnmspbccs72") (y #t)))

(define-public crate-nyst-0.1.2 (c (n "nyst") (v "0.1.2") (d (list (d (n "regex") (r "^1.4.1") (d #t) (k 0)) (d (n "ron") (r "^0.6.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (d #t) (k 0)))) (h "0zdwv8lm7798vbv0rrc6l7ibfiy81g89jb9880qyr7glpq3rrrg3") (y #t)))

(define-public crate-nyst-0.1.3 (c (n "nyst") (v "0.1.3") (h "1d9q8pllsrqpfxi7d8191jbhg6pp0gnjar1yh1par7d92rgbb4da")))

(define-public crate-nyst-0.1.4 (c (n "nyst") (v "0.1.4") (h "1phbrv89khs5il0q07avplsasxyd05mik32flj5g1w35a40hz9f4")))

(define-public crate-nyst-0.2.0 (c (n "nyst") (v "0.2.0") (h "0jms8g6z6vpbbfhlfhid3v8bly8z22x2g5mpmjsajpx2yfm3rhkx")))

(define-public crate-nyst-0.3.0 (c (n "nyst") (v "0.3.0") (h "05lzmxmh2ahipfwgbj9hpcm4asma8dn2n6cj185pz0lclbbl4mf8")))

(define-public crate-nyst-0.4.0 (c (n "nyst") (v "0.4.0") (h "0yb8phvfbswkss0ahnwcbkp61v109v6zqgp0b22qf1qfvfwzcfjh")))

(define-public crate-nyst-0.4.1 (c (n "nyst") (v "0.4.1") (h "0gd5zz07ank4g7yy6x0igq2lk3z2bc999y8z9i2yjlc4hmiyp0rm")))

(define-public crate-nyst-0.4.2 (c (n "nyst") (v "0.4.2") (h "0w7z0mds00x2c87mkiyqd77kkyskalxxp9r1l6igcyv4shhr5y2m")))

(define-public crate-nyst-0.5.0 (c (n "nyst") (v "0.5.0") (h "1zn6pmf5k9mkn3j3zh3b5afxp018m9fpv77422nzlib9bf5nhha4")))

(define-public crate-nyst-0.5.1 (c (n "nyst") (v "0.5.1") (h "0yqmmviasy8lcj4fba4c5vfhydzz9xndcdhc6k01gipmhbiqb2pv")))

(define-public crate-nyst-0.5.2 (c (n "nyst") (v "0.5.2") (h "0zjlfm0f22idma001vliv5z2c0rzc9dq9fyz939k2xgyvhk5w9ac")))

