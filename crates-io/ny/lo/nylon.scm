(define-module (crates-io ny lo nylon) #:use-module (crates-io))

(define-public crate-nylon-0.1.0 (c (n "nylon") (v "0.1.0") (d (list (d (n "core_affinity") (r "^0.5") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(any(target_os = \"linux\", target_os = \"macos\"))") (k 0)) (d (n "winapi") (r "^0.3") (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "0kjs2s39nvp8zz9xbjxvbg8znnrs508kpjqw77iq3a1zh3nm4iy1")))

