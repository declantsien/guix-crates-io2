(define-module (crates-io z- ba z-base-32) #:use-module (crates-io))

(define-public crate-z-base-32-0.1.0 (c (n "z-base-32") (v "0.1.0") (d (list (d (n "pyo3") (r "^0.14.2") (f (quote ("extension-module"))) (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^1") (d #t) (k 2)))) (h "0lxc6q9j83ah4b9sgfwc4df1cxg2fa95vfdrha5bmhrvq0fgdrj9") (f (quote (("python" "pyo3"))))))

(define-public crate-z-base-32-0.1.1 (c (n "z-base-32") (v "0.1.1") (d (list (d (n "pyo3") (r "^0.14.2") (f (quote ("extension-module"))) (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^1") (d #t) (k 2)))) (h "0d3kdgm0k1vwp8wgs8bncxs25m6q2qn14wrgmxs83a6x1jpyhl9n") (f (quote (("python" "pyo3"))))))

(define-public crate-z-base-32-0.1.2 (c (n "z-base-32") (v "0.1.2") (d (list (d (n "pyo3") (r "^0.14.2") (f (quote ("extension-module"))) (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^1") (d #t) (k 2)))) (h "11sy4h4l2fk2r8r50l1dyfl8kdqaakr1fj1j580bgyvx18jpr2cf") (f (quote (("python" "pyo3"))))))

(define-public crate-z-base-32-0.1.3 (c (n "z-base-32") (v "0.1.3") (d (list (d (n "pyo3") (r "^0.20.0") (f (quote ("extension-module"))) (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^1") (d #t) (k 2)))) (h "14hzhw3dg2kw4bmh6i54mg3chy3w5mq4f85x2n7gh2ip2f3dk840") (f (quote (("python" "pyo3"))))))

(define-public crate-z-base-32-0.1.4 (c (n "z-base-32") (v "0.1.4") (d (list (d (n "anyhow") (r "^1") (o #t) (d #t) (k 0)) (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "pyo3") (r "^0.20.2") (f (quote ("extension-module"))) (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^1") (d #t) (k 2)))) (h "0fqk028mzvgnx9phqzvhzrx3fbzv4r736cm3x3hid136g157pgr1") (s 2) (e (quote (("python" "dep:pyo3") ("cli" "dep:clap" "dep:anyhow"))))))

