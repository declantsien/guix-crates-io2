(define-module (crates-io z- bu z-buffer-game) #:use-module (crates-io))

(define-public crate-z-buffer-game-0.1.0 (c (n "z-buffer-game") (v "0.1.0") (d (list (d (n "bluenoisers") (r "^1.1.1") (d #t) (k 0)) (d (n "dirs") (r "^1.0.4") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "shred") (r "^0.7.1") (d #t) (k 0)) (d (n "shred-derive") (r "^0.5.1") (d #t) (k 0)) (d (n "specs") (r "^0.14.3") (d #t) (k 0)) (d (n "specs-derive") (r "^0.4.0") (d #t) (k 0)) (d (n "stderrlog") (r "^0.4.1") (d #t) (k 0)) (d (n "tcod") (r "^0.13") (d #t) (k 0)))) (h "05w1bsfc1pzrk4bcwpbywrgk23jbszv8giyry47ff8nxfgk9mrbs")))

