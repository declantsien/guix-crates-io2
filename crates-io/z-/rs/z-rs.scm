(define-module (crates-io z- rs z-rs) #:use-module (crates-io))

(define-public crate-z-rs-0.1.0 (c (n "z-rs") (v "0.1.0") (d (list (d (n "argh") (r "^0.1.3") (d #t) (k 0)) (d (n "bstr") (r "^0.2.13") (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_cbor") (r "^0.11.1") (d #t) (k 0)))) (h "1bkdfnr1s5ayw47pkga4scaaiphydlhi45dfc4z2vikisgy0r7v1")))

