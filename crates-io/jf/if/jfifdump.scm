(define-module (crates-io jf if jfifdump) #:use-module (crates-io))

(define-public crate-jfifdump-0.1.0 (c (n "jfifdump") (v "0.1.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "json") (r "^0.12") (d #t) (k 0)))) (h "104mbkxs9vg738a2n4ij13i6k2ssa40c8smcjms473y2m7ahbh56")))

(define-public crate-jfifdump-0.1.1 (c (n "jfifdump") (v "0.1.1") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "json") (r "^0.12") (d #t) (k 0)))) (h "14vl5yxrhrsfcfmi8nszyyh01nsr0pi3l5h70vwsnndfbfqy4cvb")))

(define-public crate-jfifdump-0.2.0 (c (n "jfifdump") (v "0.2.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "json") (r "^0.12") (d #t) (k 0)))) (h "1jlkiq01jhjb3h9bwm48i75zaql7w766dn1a8val2l70gvmwhg6d")))

(define-public crate-jfifdump-0.3.0 (c (n "jfifdump") (v "0.3.0") (d (list (d (n "clap") (r "^3.1") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "json") (r "^0.12") (d #t) (k 0)))) (h "114s68sk8g5mfd92fdwjkpagjqnsh7lr9f9r115xqd2qdrr1wfkr")))

(define-public crate-jfifdump-0.3.1 (c (n "jfifdump") (v "0.3.1") (d (list (d (n "json") (r "^0.12") (d #t) (k 0)))) (h "0icsz1fjn0q8wczjb5gq0pr0ps7a623w5kaiv01bv4dxhxmcsfw6")))

(define-public crate-jfifdump-0.3.2 (c (n "jfifdump") (v "0.3.2") (d (list (d (n "json") (r "^0.12") (d #t) (k 0)))) (h "08xwi9jf5wkq5kmc44km4ihlkd3raw1khvbnmzz6ly32rinm9y64")))

(define-public crate-jfifdump-0.4.0 (c (n "jfifdump") (v "0.4.0") (d (list (d (n "json") (r "^0.12") (d #t) (k 0)))) (h "09h44bf7sxibxqy1c7aq7b434skibssc9js4bxnyckbjyalywmib")))

(define-public crate-jfifdump-0.5.0 (c (n "jfifdump") (v "0.5.0") (d (list (d (n "jzon") (r "^0.12") (o #t) (d #t) (k 0)))) (h "187mg86xq6drfy8xjf9wz9nxxrpsl52573w54l33cxl19ngv7z2j") (f (quote (("json" "jzon") ("default"))))))

(define-public crate-jfifdump-0.5.1 (c (n "jfifdump") (v "0.5.1") (d (list (d (n "jzon") (r "^0.12") (o #t) (d #t) (k 0)))) (h "019cn4q9vflzll2rj3vkv1vi4qb98lfz2wm2b0z23yx87v44frij") (f (quote (("json" "jzon") ("default"))))))

