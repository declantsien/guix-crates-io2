(define-module (crates-io jf if jfifdump-cli) #:use-module (crates-io))

(define-public crate-jfifdump-cli-0.3.1 (c (n "jfifdump-cli") (v "0.3.1") (d (list (d (n "clap") (r "^3.2") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "jfifdump") (r "^0.3.1") (d #t) (k 0)))) (h "1qy2v81sfd914daggr4zxf6x2darsmn574h1yxjxspvpwxjkvkl8")))

(define-public crate-jfifdump-cli-0.3.2 (c (n "jfifdump-cli") (v "0.3.2") (d (list (d (n "clap") (r "^4.1") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "jfifdump") (r "^0.3.2") (d #t) (k 0)))) (h "037l46x7h06j8q80nik17624i97rwp99l48x77z2as3s0i6wm7ci")))

(define-public crate-jfifdump-cli-0.4.0 (c (n "jfifdump-cli") (v "0.4.0") (d (list (d (n "clap") (r "^4.3") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "jfifdump") (r "^0.4.0") (d #t) (k 0)))) (h "06psl4h86vgykysz19kydfi61fibzb79bn2mdarkbm7pmxfc992y")))

(define-public crate-jfifdump-cli-0.5.0 (c (n "jfifdump-cli") (v "0.5.0") (d (list (d (n "clap") (r "^4.3") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "jfifdump") (r "^0.5.0") (f (quote ("json"))) (d #t) (k 0)))) (h "11psfkvmaj458dxxqsjr65gc3lk2x5zyiqlj1s40s699i3ia4xqx")))

(define-public crate-jfifdump-cli-0.5.1 (c (n "jfifdump-cli") (v "0.5.1") (d (list (d (n "clap") (r "^4.3") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "jfifdump") (r "^0.5.0") (f (quote ("json"))) (d #t) (k 0)))) (h "0myq6my4df5bb0mk2h57gi8xj9d4pksich788h30lz7cjg0cz394")))

