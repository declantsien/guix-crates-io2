(define-module (crates-io jf tp jftp) #:use-module (crates-io))

(define-public crate-jftp-0.1.0 (c (n "jftp") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1z5alxy7cwa1y5421amizd6ykdkcyq1frsglvl8r7njywcbzb4xi")))

