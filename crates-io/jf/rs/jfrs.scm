(define-module (crates-io jf rs jfrs) #:use-module (crates-io))

(define-public crate-jfrs-0.1.0 (c (n "jfrs") (v "0.1.0") (d (list (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)))) (h "1wd877a35kp5v0yd2qrl5zbkirc87s30vspiljf7v58p6mvqj7yh")))

(define-public crate-jfrs-0.1.1 (c (n "jfrs") (v "0.1.1") (d (list (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)))) (h "06frccamwsykhyjvykmc6csq9nhcpb9a29zdszz85xqc7pnvas04")))

(define-public crate-jfrs-0.1.2 (c (n "jfrs") (v "0.1.2") (d (list (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)))) (h "1l7735aks2j956rxis03jcr3l2gpmldp6hifsikcjcv1haxlz3ni")))

(define-public crate-jfrs-0.1.3 (c (n "jfrs") (v "0.1.3") (d (list (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)))) (h "0h5ds1wvh8ji4ry8ljrniic5bcv73sjdgld1dfnbsl6slix2km2y")))

(define-public crate-jfrs-0.1.4 (c (n "jfrs") (v "0.1.4") (d (list (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)))) (h "14q6ywdpahwljzwm8zfxra2frf5wa3lrdq1h77a7jnra55if1fai")))

(define-public crate-jfrs-0.2.0 (c (n "jfrs") (v "0.2.0") (d (list (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)))) (h "1xgji7zag1mkrbwv3xi193p3cm41s8if8mcayy3v1a3mf4yw24pz") (f (quote (("cstring"))))))

(define-public crate-jfrs-0.2.1 (c (n "jfrs") (v "0.2.1") (d (list (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)))) (h "0qrmbarz6hlm2zbcr2gkvvzsap9bhm06fz6y2cxmnxxzgnh116jr") (f (quote (("cstring"))))))

(define-public crate-jfrs-0.2.2 (c (n "jfrs") (v "0.2.2") (d (list (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)))) (h "105r2xd1fmcyp4xky6ccbk2f39s613q34jninn388bch7mlw8wb9") (f (quote (("cstring"))))))

(define-public crate-jfrs-0.2.3 (c (n "jfrs") (v "0.2.3") (d (list (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)))) (h "025bj8bw8pvj5dpgdpyxb7mw40lzb8n854zgdf166ihls0mpc6p8") (f (quote (("cstring"))))))

(define-public crate-jfrs-0.2.4 (c (n "jfrs") (v "0.2.4") (d (list (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)))) (h "1bhyccqqbv2vgj97f43dlw54j1mvhicsh9zkncyzcgvm3lv70gsm") (f (quote (("cstring"))))))

(define-public crate-jfrs-0.2.5 (c (n "jfrs") (v "0.2.5") (d (list (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)))) (h "0mr434ajp0ygk92vixhqlsdj5zs01wpf8m9wd64c7flfkwry17fm") (f (quote (("cstring"))))))

