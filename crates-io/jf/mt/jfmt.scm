(define-module (crates-io jf mt jfmt) #:use-module (crates-io))

(define-public crate-jfmt-1.0.0 (c (n "jfmt") (v "1.0.0") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.89") (d #t) (k 0)) (d (n "serde-transcode") (r "^1.1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)))) (h "0i83q78rs189vc005bkchp9vvah4pkmd68s2fn6gc21pqp0jprgv")))

(define-public crate-jfmt-1.0.1 (c (n "jfmt") (v "1.0.1") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.89") (d #t) (k 0)) (d (n "serde-transcode") (r "^1.1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)))) (h "1zkkf5wjhlp5nw0x5s39zwh4gsfm0ghdlw3aswhmwra07wc9cqcp")))

(define-public crate-jfmt-1.2.0 (c (n "jfmt") (v "1.2.0") (d (list (d (n "clap") (r "^3.0.14") (f (quote ("cargo" "derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (d #t) (k 0)) (d (n "serde-transcode") (r "^1.1.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.78") (d #t) (k 0)))) (h "1wiwdynn7sz0sj195fckrvv1w88ac75m1nmz9l9m2ylsbrs1hhdj")))

(define-public crate-jfmt-1.2.1 (c (n "jfmt") (v "1.2.1") (d (list (d (n "clap") (r "^3.0.14") (f (quote ("cargo" "derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (d #t) (k 0)) (d (n "serde-transcode") (r "^1.1.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.78") (d #t) (k 0)))) (h "1xlnpl7kakbd0zklpnd1i0hl12mjqrifwlzfbf76f0sfbqlm2qfl")))

