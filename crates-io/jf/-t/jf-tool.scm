(define-module (crates-io jf -t jf-tool) #:use-module (crates-io))

(define-public crate-jf-tool-0.1.0 (c (n "jf-tool") (v "0.1.0") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "os_info") (r "^3.8.2") (d #t) (k 0)))) (h "1h9z1935s15m1bnpgxr0jki34wjb4pys3kbvspwq9zbk131s91pw")))

