(define-module (crates-io yn ab ynab-reimbursements) #:use-module (crates-io))

(define-public crate-ynab-reimbursements-0.0.1 (c (n "ynab-reimbursements") (v "0.0.1") (d (list (d (n "cursive") (r "^0.12") (d #t) (k 0)) (d (n "cursive_table_view") (r "^0.10") (d #t) (k 0)) (d (n "directories") (r "^2.0") (d #t) (k 0)) (d (n "snafu") (r "^0.4") (d #t) (k 0)) (d (n "ynab-api") (r "^1.0") (d #t) (k 0)))) (h "1mpa53p8ysd2h331in1rs1xh8b5mhm51b4w3fzhkr433ak2a1p6a")))

(define-public crate-ynab-reimbursements-0.0.2 (c (n "ynab-reimbursements") (v "0.0.2") (d (list (d (n "cursive") (r "^0.12") (d #t) (k 0)) (d (n "cursive_table_view") (r "^0.10") (d #t) (k 0)) (d (n "directories") (r "^2.0") (d #t) (k 0)) (d (n "snafu") (r "^0.4") (d #t) (k 0)) (d (n "ynab-api") (r "^2") (d #t) (k 0)))) (h "1maiz8320lnhlysn8c0vpg8vfiyhynihcy0j251d70nlf7c9ix1q")))

(define-public crate-ynab-reimbursements-0.0.3 (c (n "ynab-reimbursements") (v "0.0.3") (d (list (d (n "cursive") (r "^0.12") (d #t) (k 0)) (d (n "cursive_table_view") (r "^0.10") (d #t) (k 0)) (d (n "directories") (r "^2.0") (d #t) (k 0)) (d (n "snafu") (r "^0.4") (d #t) (k 0)) (d (n "ynab-api") (r "^3") (d #t) (k 0)))) (h "0m6zi1jnhlk76508mb79kf0fw0m5yr2v609x13q0r1ayfdbi4v2h")))

