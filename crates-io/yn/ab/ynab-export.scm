(define-module (crates-io yn ab ynab-export) #:use-module (crates-io))

(define-public crate-ynab-export-0.0.1 (c (n "ynab-export") (v "0.0.1") (d (list (d (n "directories") (r "^2") (d #t) (k 0)) (d (n "ynab-api") (r "^1") (d #t) (k 0)))) (h "1igl75lffgc62b1h9shp9zs6iijs5g6a0y6fnr7i4imb6kiq1f7c")))

(define-public crate-ynab-export-0.0.2 (c (n "ynab-export") (v "0.0.2") (d (list (d (n "directories") (r "^2") (d #t) (k 0)) (d (n "ynab-api") (r "^1") (d #t) (k 0)))) (h "189ianpggz0gv2wjhl8adr084g35fja9scn919112wkprc8glk46")))

(define-public crate-ynab-export-0.0.3 (c (n "ynab-export") (v "0.0.3") (d (list (d (n "directories") (r "^2") (d #t) (k 0)) (d (n "ynab-api") (r "^2") (d #t) (k 0)))) (h "0vgnjhm1a75cb8j6h3wph7dzv2cw35b1dmj0ms75i5wxqkf3pfm5")))

(define-public crate-ynab-export-0.0.4 (c (n "ynab-export") (v "0.0.4") (d (list (d (n "directories") (r "^2") (d #t) (k 0)) (d (n "ynab-api") (r "^3") (d #t) (k 0)))) (h "0a6c6i1i2j98z8kjr2gdsd7yg8j4kfrk8g5qa5gz0kivj2ik2w1q")))

