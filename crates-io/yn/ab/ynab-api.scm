(define-module (crates-io yn ab ynab-api) #:use-module (crates-io))

(define-public crate-ynab-api-1.0.0 (c (n "ynab-api") (v "1.0.0") (d (list (d (n "reqwest") (r "~0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^1.5") (d #t) (k 0)))) (h "0fq54ji4d6lf0r6kvg1mrf9djlgb34ijl7jg7aff0blmhvbq2i80")))

(define-public crate-ynab-api-2.0.0 (c (n "ynab-api") (v "2.0.0") (d (list (d (n "reqwest") (r "~0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^1.5") (d #t) (k 0)))) (h "1a1b7isqq4qijhai1rq19lb148x3bpdy2h8dzl4j4dvjx4qrf972")))

(define-public crate-ynab-api-3.0.0 (c (n "ynab-api") (v "3.0.0") (d (list (d (n "reqwest") (r "~0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^1.5") (d #t) (k 0)))) (h "0i69dz3vcx8k4bk9ayz82m2f0c4w7ngas7z4znn75b1brc9fmdcp")))

