(define-module (crates-io td ig tdigest) #:use-module (crates-io))

(define-public crate-tdigest-0.1.0 (c (n "tdigest") (v "0.1.0") (d (list (d (n "ordered-float") (r "^1.0") (d #t) (k 0)))) (h "1mzvkcblhzq30dfq7npc4dp5nsidkdvjzzlbapbdbl7fk1rlm2sq")))

(define-public crate-tdigest-0.1.1 (c (n "tdigest") (v "0.1.1") (d (list (d (n "ordered-float") (r "^1.0") (d #t) (k 0)))) (h "0km1r9hj5zyih3p24k4lzcq1r6k88blv66xi2pfr90k3cwbsh1v2")))

(define-public crate-tdigest-0.2.0 (c (n "tdigest") (v "0.2.0") (d (list (d (n "ordered-float") (r "^1.0") (d #t) (k 0)))) (h "1znz1a4kk441kvkjphvbbj97qyh1id6l8jivx43v6k6aybi7ma1z")))

(define-public crate-tdigest-0.2.1 (c (n "tdigest") (v "0.2.1") (d (list (d (n "ordered-float") (r "^1.0") (d #t) (k 0)))) (h "0v71923knncbd5qbqwb7b8zmsf2c2rf6mra2gccv2jhf69in6i9z")))

(define-public crate-tdigest-0.2.2 (c (n "tdigest") (v "0.2.2") (d (list (d (n "ordered-float") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (k 0) (p "serde")))) (h "0b3s39pzp205qa6zwdzazi4l6zp6yifc030rpg139y0xc8f6qxwv") (f (quote (("use_serde" "serde" "serde/derive" "ordered-float/serde"))))))

(define-public crate-tdigest-0.2.3 (c (n "tdigest") (v "0.2.3") (d (list (d (n "ordered-float") (r "^2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (k 0) (p "serde")))) (h "1j7kifbpa5jgl30yql1pknr0bax8dq3b8vflqzhg1k7b11d24pf4") (f (quote (("use_serde" "serde" "serde/derive" "ordered-float/serde"))))))

