(define-module (crates-io td ig tdigest-rs) #:use-module (crates-io))

(define-public crate-tdigest-rs-0.2.4 (c (n "tdigest-rs") (v "0.2.4") (d (list (d (n "ordered-float") (r "^2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (k 0) (p "serde")))) (h "1djqw8irwfb4jv48r1qp8z9rh11kpv5h2xalxd334wv98kmwb2xf") (f (quote (("use_serde" "serde" "serde/derive" "ordered-float/serde"))))))

(define-public crate-tdigest-rs-0.2.5 (c (n "tdigest-rs") (v "0.2.5") (d (list (d (n "ordered-float") (r "^2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (k 0) (p "serde")))) (h "0lgycyv168dkybg79l91197mzba74kbqcjr2b1c3bfdr9mbmi84r") (f (quote (("use_serde" "serde" "serde/derive" "ordered-float/serde"))))))

