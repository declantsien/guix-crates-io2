(define-module (crates-io td x- tdx-guest) #:use-module (crates-io))

(define-public crate-tdx-guest-0.1.0 (c (n "tdx-guest") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand_core") (r "^0.6.4") (f (quote ("getrandom"))) (d #t) (k 0)) (d (n "raw-cpuid") (r "^10") (d #t) (k 0)) (d (n "x86_64") (r "^0.14.10") (d #t) (k 0)))) (h "1xl7ss1vwv0dfh9l8lk5r5lglmqz3c229rg9m5w0ccl1gfdxjnv7")))

(define-public crate-tdx-guest-0.1.1 (c (n "tdx-guest") (v "0.1.1") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "iced-x86") (r "^1.21.0") (f (quote ("no_std" "decoder" "gas"))) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "raw-cpuid") (r "^10") (d #t) (k 0)) (d (n "x86_64") (r "^0.14.10") (d #t) (k 0)))) (h "0qhk8hvmf4ji9apkbckxg4b24lalmwj6b88a6dy6c0x9i6f6pipy")))

