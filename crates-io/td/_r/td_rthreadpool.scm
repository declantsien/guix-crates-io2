(define-module (crates-io td _r td_rthreadpool) #:use-module (crates-io))

(define-public crate-td_rthreadpool-0.1.0 (c (n "td_rthreadpool") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.1") (d #t) (k 0)))) (h "0spjbjwhsj3iwhjbxq9byvm9pdyrfx2xajlyvc6pnqlr9m5p2fn8")))

(define-public crate-td_rthreadpool-0.1.1 (c (n "td_rthreadpool") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.1") (d #t) (k 0)))) (h "1vpvlp8nak92yva6r1kafj5dnyig111zl58yl5qplz9alaf3l235")))

(define-public crate-td_rthreadpool-0.1.2 (c (n "td_rthreadpool") (v "0.1.2") (d (list (d (n "libc") (r "^0.2.1") (d #t) (k 0)))) (h "19h0jkzkmhf1pwszvcjd0l23g3g6ha5hyhcjsnvfhvbfj6ahrk25")))

(define-public crate-td_rthreadpool-0.1.3 (c (n "td_rthreadpool") (v "0.1.3") (d (list (d (n "libc") (r "^0.2.1") (d #t) (k 0)))) (h "1k6v9rgy55vdwlhg9v6l4pba4zvmh5kch92d4q9k77bfz2pi3cpb")))

(define-public crate-td_rthreadpool-0.1.4 (c (n "td_rthreadpool") (v "0.1.4") (d (list (d (n "libc") (r "^0.2.1") (d #t) (k 0)))) (h "19khzxc10himi0iiabxxgicvxaz3rzh488m0acj10wzcgvp61g2d")))

