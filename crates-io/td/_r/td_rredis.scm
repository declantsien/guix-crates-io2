(define-module (crates-io td _r td_rredis) #:use-module (crates-io))

(define-public crate-td_rredis-0.1.0 (c (n "td_rredis") (v "0.1.0") (d (list (d (n "sha1") (r "^0.1.1") (d #t) (k 0)) (d (n "url") (r "^0.5.4") (d #t) (k 0)))) (h "15np8n6hx2cjc4ppk4ngqb27ij67j2xps8zpsa0jwh60vgcfyjma")))

(define-public crate-td_rredis-0.1.1 (c (n "td_rredis") (v "0.1.1") (d (list (d (n "sha1") (r "^0.1.1") (d #t) (k 0)) (d (n "url") (r "^0.5.4") (d #t) (k 0)))) (h "094k7rfm117wkviph5na3wxr4g1c0ffk41w2d2l1p96885l8jxc0")))

(define-public crate-td_rredis-0.1.2 (c (n "td_rredis") (v "0.1.2") (d (list (d (n "sha1") (r "^0.1.1") (d #t) (k 0)) (d (n "url") (r "^0.5.4") (d #t) (k 0)))) (h "13j8fqckjdpxzdxrlllza6mp4sc7iininklznwmpifyr1ydxhhv6")))

(define-public crate-td_rredis-0.1.3 (c (n "td_rredis") (v "0.1.3") (d (list (d (n "sha1") (r "^0.1.1") (d #t) (k 0)) (d (n "url") (r "^0.5.4") (d #t) (k 0)))) (h "0szm0n23wqj41z970wwnmq7ibb627ryvyvf39w9fni8cpwc89x0w")))

