(define-module (crates-io td es tdesktop_theme) #:use-module (crates-io))

(define-public crate-tdesktop_theme-0.1.0 (c (n "tdesktop_theme") (v "0.1.0") (d (list (d (n "indexmap") (r "^1.0.1") (d #t) (k 0)) (d (n "zip") (r "^0.4.2") (d #t) (k 0)))) (h "106nqlj4r67z45n68pymfv6y9q5xv0y0rbaisrh3wlkfdmm4p6j4")))

(define-public crate-tdesktop_theme-0.1.1 (c (n "tdesktop_theme") (v "0.1.1") (d (list (d (n "indexmap") (r "^1.0.1") (d #t) (k 0)) (d (n "zip") (r "^0.4.2") (d #t) (k 0)))) (h "1k752z26j83nqhy7n0mqrg6xayz44vpzwhkb1jy9n2gd5qyilndp")))

(define-public crate-tdesktop_theme-0.1.2 (c (n "tdesktop_theme") (v "0.1.2") (d (list (d (n "indexmap") (r "^1.0.1") (d #t) (k 0)) (d (n "zip") (r "^0.4.2") (d #t) (k 0)))) (h "16wv1bynbv9mggx1zvb2vgfc2k82vmmvfl7crlmzbs8sfli9nm4v")))

(define-public crate-tdesktop_theme-0.2.0 (c (n "tdesktop_theme") (v "0.2.0") (d (list (d (n "indexmap") (r "^1.0.1") (d #t) (k 0)) (d (n "zip") (r "^0.4.2") (d #t) (k 0)))) (h "12dr6xm04d7hi7fayc894g4iw6fkrc6qfhzlmi3ixlkx40rhsxpy")))

(define-public crate-tdesktop_theme-0.3.0 (c (n "tdesktop_theme") (v "0.3.0") (d (list (d (n "indexmap") (r "^1.0.1") (d #t) (k 0)) (d (n "zip") (r "^0.4.2") (d #t) (k 0)))) (h "0k3zlzdd4v43h94si4mbvf10w4z4c15w7rhl5jkw1qy3kfr7m4rs")))

(define-public crate-tdesktop_theme-0.3.1 (c (n "tdesktop_theme") (v "0.3.1") (d (list (d (n "indexmap") (r "^1.0.1") (d #t) (k 0)) (d (n "zip") (r "^0.4.2") (d #t) (k 0)))) (h "1gsb4vmfkkdc0c5a5dbz644a143wshqn290lpdyzh2h3razqbq43")))

(define-public crate-tdesktop_theme-0.3.2 (c (n "tdesktop_theme") (v "0.3.2") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "zip") (r "^0.5") (f (quote ("deflate"))) (k 0)))) (h "1pvffnggpdl8m2nng79l2h92zcawxxv9qxyl5rlbvxkw75mpzh75")))

