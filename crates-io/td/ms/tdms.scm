(define-module (crates-io td ms tdms) #:use-module (crates-io))

(define-public crate-tdms-0.1.0 (c (n "tdms") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 0)) (d (n "extended") (r "^0.1.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "indexmap") (r "^1.8.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0s4b6p7d8ma6z0ymrqcy2c8b455fi40k7g98f321dn6sw1pyyg66")))

(define-public crate-tdms-0.1.1 (c (n "tdms") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 0)) (d (n "extended") (r "^0.1.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "indexmap") (r "^1.8.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0ppxqbdx1l7nh986r04f0p2bll7h8hs8gv6la89j07vxzxcq18ll")))

(define-public crate-tdms-0.1.2 (c (n "tdms") (v "0.1.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 0)) (d (n "extended") (r "^0.1.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "indexmap") (r "^1.8.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0zk2q0zp5zcwyarad5xgv0lv65gi40avmffvkcfi50bh6a9gy94n")))

(define-public crate-tdms-0.1.3 (c (n "tdms") (v "0.1.3") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "indexmap") (r "^1.8.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0himv4rmfd807jrqpnsglkdwjxrdmg3zz3m8hs1aagydndia5xb7")))

(define-public crate-tdms-0.1.4 (c (n "tdms") (v "0.1.4") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "indexmap") (r "^1.8.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "156ya1lrzj0kz636xl8ff46rwmla25ib87zb2cl12hlqdd20l60a")))

(define-public crate-tdms-0.1.5 (c (n "tdms") (v "0.1.5") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "indexmap") (r "^1.8.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0jj17h6vy25j3mrsipys6h8xwy1bw15y4n8zrgyvr121rl8q2z3n")))

(define-public crate-tdms-0.1.6 (c (n "tdms") (v "0.1.6") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "indexmap") (r "^1.8.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0qnhmhxg48chv9p5xs32k38hfr35ifixmzk6hs5b1f09pcvvw43q")))

(define-public crate-tdms-0.1.7 (c (n "tdms") (v "0.1.7") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "extended") (r "^0.1.0") (d #t) (k 0)) (d (n "fixed") (r "^1.15.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "indexmap") (r "^1.8.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "00nv5pqkb7j1350ah6fxpskm9myrr8xv49i5vggyl8a0adzxys8v")))

(define-public crate-tdms-0.1.8 (c (n "tdms") (v "0.1.8") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "extended") (r "^0.1.0") (d #t) (k 0)) (d (n "fixed") (r "^1.15.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "indexmap") (r "^1.8.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "13yvqwhqm4j9j09f7ywqrd3r0q5brzdw2kidldpyvi3wyhm63gbr")))

(define-public crate-tdms-0.1.9 (c (n "tdms") (v "0.1.9") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "extended") (r "^0.1.0") (d #t) (k 0)) (d (n "fixed") (r "^1.15.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "indexmap") (r "^1.8.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "14xjnm5z8j9pxqsl7v08ild89pm17p5lrm37rkb9sq4n8bwg1x0q")))

(define-public crate-tdms-0.2.0 (c (n "tdms") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "extended") (r "^0.1.0") (d #t) (k 0)) (d (n "fixed") (r "^1.15.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "indexmap") (r "^1.8.2") (d #t) (k 0)) (d (n "pprof") (r "^0.10.0") (f (quote ("flamegraph" "criterion"))) (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1f829lk5a80knq23js8a9fsm5dflkwlmlw9036gq6lizpnsgqniz")))

(define-public crate-tdms-0.2.1 (c (n "tdms") (v "0.2.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "extended") (r "^0.1.0") (d #t) (k 0)) (d (n "fixed") (r "^1.15.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "indexmap") (r "^1.8.2") (d #t) (k 0)) (d (n "pprof") (r "^0.10.0") (f (quote ("flamegraph" "criterion"))) (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "024vxx504b03a9pl57dr7dv22vgwlkbngrav1nij3h96c4g3xc6n")))

(define-public crate-tdms-0.2.3 (c (n "tdms") (v "0.2.3") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "extended") (r "^0.1.0") (d #t) (k 0)) (d (n "fixed") (r "^1.15.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "indexmap") (r "^1.8.2") (d #t) (k 0)) (d (n "pprof") (r "^0.10.0") (f (quote ("flamegraph" "criterion" "protobuf-codec"))) (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1w1bhrakqxmx9li6gz4v749508450rjphjfcazvqijpfivn127yd")))

(define-public crate-tdms-0.2.4 (c (n "tdms") (v "0.2.4") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "extended") (r "^0.1.0") (d #t) (k 0)) (d (n "fixed") (r "^1.15.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "indexmap") (r "^1.8.2") (d #t) (k 0)) (d (n "pprof") (r "^0.10.0") (f (quote ("flamegraph" "criterion" "protobuf-codec"))) (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "15l3li87rvlrd2an21mhyb2vh2zw544wqm9fq1izki6xshnwn1p1")))

(define-public crate-tdms-0.2.5 (c (n "tdms") (v "0.2.5") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "extended") (r "^0.1.0") (d #t) (k 0)) (d (n "fixed") (r "^1.15.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "indexmap") (r "^1.8.2") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "pprof") (r "^0.10.0") (f (quote ("flamegraph" "criterion" "protobuf-codec"))) (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "10jkg904gswxnd8pbbb1fj877v87vkwhjj4s81krhh58y92prvys")))

(define-public crate-tdms-0.2.6 (c (n "tdms") (v "0.2.6") (d (list (d (n "extended") (r "^0.1.0") (d #t) (k 0)) (d (n "fixed") (r "^1.15.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "indexmap") (r "^1.8.2") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "pprof") (r "^0.10.0") (f (quote ("flamegraph" "criterion" "protobuf-codec"))) (d #t) (k 2)))) (h "1nra3fb5s3imhx2bh4pzm5wz4p8gsvd80z57hd2wfqjxjn2f3qm7")))

(define-public crate-tdms-0.2.7 (c (n "tdms") (v "0.2.7") (d (list (d (n "extended") (r "^0.1.0") (d #t) (k 0)) (d (n "fixed") (r "^1.15.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "indexmap") (r "^1.8.2") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "pprof") (r "^0.10.0") (f (quote ("flamegraph" "criterion" "protobuf-codec"))) (d #t) (k 2)))) (h "0lajlj227yjaiqapdb76j9jwglaain5b0y5hcj3rc559dsa7qk5w")))

(define-public crate-tdms-0.3.0 (c (n "tdms") (v "0.3.0") (d (list (d (n "extended") (r "^0.1.0") (d #t) (k 0)) (d (n "fixed") (r "^1.15.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "indexmap") (r "^1.8.2") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "pprof") (r "^0.10.0") (f (quote ("flamegraph" "criterion" "protobuf-codec"))) (d #t) (k 2)))) (h "1dah3waqdai2gq95w3iljlca9i3v8cpbla3qqln2j0lcikcjddr9")))

