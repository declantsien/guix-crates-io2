(define-module (crates-io td f- tdf-derive) #:use-module (crates-io))

(define-public crate-tdf-derive-0.0.0 (c (n "tdf-derive") (v "0.0.0") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "09bdifzzgnrlkcyxyijvflhhwmckg2sx8hpfpx0bf0w6rclznabg")))

(define-public crate-tdf-derive-0.1.0 (c (n "tdf-derive") (v "0.1.0") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0k5h58l0ihpkxbm73226r88h2k7f8y18g0n1vbdj55r8rasf44pk")))

(define-public crate-tdf-derive-0.1.1 (c (n "tdf-derive") (v "0.1.1") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0wxb5c969jqaw3p8h37qls5hc4iga1j1jz95x9xvg335b64csmfd")))

(define-public crate-tdf-derive-0.2.0 (c (n "tdf-derive") (v "0.2.0") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1hbyb1vca1xrpnj5b3a148gi689szh5cpzyy44hln1h9fjdcp7i3")))

