(define-module (crates-io td s- tds-meter) #:use-module (crates-io))

(define-public crate-tds-meter-0.0.0 (c (n "tds-meter") (v "0.0.0") (h "0fkmmfb96zsrp95cgirahl9liv6zrrkn9vpf0r711jz1zzxyvab7")))

(define-public crate-tds-meter-0.1.0 (c (n "tds-meter") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2.7") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "nb") (r "1.0.*") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (k 0)))) (h "0263qnbxfgkjhwan6rwnws48vccmxzz4dy1aj69g3fmnn324sn5b")))

