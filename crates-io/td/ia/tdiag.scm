(define-module (crates-io td ia tdiag) #:use-module (crates-io))

(define-public crate-tdiag-0.1.0 (c (n "tdiag") (v "0.1.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "differential-dataflow") (r "^0.9") (d #t) (k 0)) (d (n "tdiag-connect") (r "^0.1") (d #t) (k 0)) (d (n "timely") (r "^0.9") (d #t) (k 0)))) (h "11gsrmynzy76sf8m2kas8g9i96q11bpdi6bsxllfg52ig61ga7ql")))

(define-public crate-tdiag-0.2.0 (c (n "tdiag") (v "0.2.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "differential-dataflow") (r "^0.10") (d #t) (k 0)) (d (n "tdiag-connect") (r "^0.2") (d #t) (k 0)) (d (n "timely") (r "^0.10") (d #t) (k 0)))) (h "1dn4g9ckdbidgzyjbzgdv5dmczq8c4avcrv27qgv8g9ihq8y59qx")))

