(define-module (crates-io td n_ tdn_permission) #:use-module (crates-io))

(define-public crate-tdn_permission-0.2.0 (c (n "tdn_permission") (v "0.2.0") (d (list (d (n "async-channel") (r "^1.4") (d #t) (k 0)) (d (n "postcard") (r "^0.5") (f (quote ("alloc"))) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "smol") (r "^1") (d #t) (k 2)) (d (n "tdn") (r "^0.2") (d #t) (k 2)) (d (n "tdn_types") (r "^0.2") (d #t) (k 0)))) (h "0ay84whdzphdjb4n54srjam3dw08gz2lyn25csx39cxh5sjd4z2m")))

