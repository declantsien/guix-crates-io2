(define-module (crates-io td n_ tdn_storage) #:use-module (crates-io))

(define-public crate-tdn_storage-0.1.0 (c (n "tdn_storage") (v "0.1.0") (d (list (d (n "async-fs") (r "^1.3") (d #t) (k 0)) (d (n "postcard") (r "^0.5") (f (quote ("alloc"))) (k 0)) (d (n "serde") (r "^1.0") (k 0)) (d (n "sled") (r "^0.34") (o #t) (d #t) (k 0)) (d (n "smol") (r "^1") (d #t) (k 2)) (d (n "tdn_types") (r "^0.2") (d #t) (k 0)))) (h "1a8x2ll2injmdikwsqn4lclrfb5ayw9xm4gvcgk34nm4z24jdw1p") (f (quote (("local" "sled") ("distributed") ("default" "local") ("decentralized"))))))

