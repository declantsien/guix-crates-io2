(define-module (crates-io td b_ tdb_cli) #:use-module (crates-io))

(define-public crate-tdb_cli-0.5.0 (c (n "tdb_cli") (v "0.5.0") (d (list (d (n "bufstream") (r "^0.1.4") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.104") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.46") (d #t) (k 0)) (d (n "tdb_core") (r "^0.5.0") (d #t) (k 0)))) (h "0b8ah47h26wlyn6h7y9145vzi0za9pnsw4filf3cx0sf0jgfy0ls")))

(define-public crate-tdb_cli-0.5.1 (c (n "tdb_cli") (v "0.5.1") (d (list (d (n "bufstream") (r "^0.1.4") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.104") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.46") (d #t) (k 0)) (d (n "tdb_core") (r "^0.5.0") (d #t) (k 0)))) (h "0mv7vra5j7k2w106snvll14h1b3kwg2sfpzjgkw560s33d3hwwac")))

