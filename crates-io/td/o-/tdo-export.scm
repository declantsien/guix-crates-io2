(define-module (crates-io td o- tdo-export) #:use-module (crates-io))

(define-public crate-tdo-export-0.1.0 (c (n "tdo-export") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tdo-core") (r "^0.1.0") (d #t) (k 0)))) (h "1iq6cfknmycismv5y6z188l99gjcjhh3p72ypq7w1h8ni2yy3m8l")))

(define-public crate-tdo-export-0.1.1 (c (n "tdo-export") (v "0.1.1") (d (list (d (n "colored") (r "^1.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tdo-core") (r "^0.1.0") (d #t) (k 0)))) (h "1sdl950znxmslll9z9h6rxd1c4028z3siyfnd2lpkjgzh5s6q27k")))

(define-public crate-tdo-export-0.1.2 (c (n "tdo-export") (v "0.1.2") (d (list (d (n "colored") (r "^1.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tdo-core") (r "^0.1.1") (d #t) (k 0)))) (h "0m3ka9yb5azjdv0mzydznpk9z11z4vll8xwjk4k1v88chpafysv7")))

(define-public crate-tdo-export-0.2.0 (c (n "tdo-export") (v "0.2.0") (d (list (d (n "colored") (r "^1.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.6.6") (d #t) (k 0)) (d (n "reqwest") (r "^0.5.1") (d #t) (k 0)) (d (n "tdo-core") (r "^0.2.0") (d #t) (k 0)))) (h "0s2fy71p6pwskzn6mlfd6rcv6sgzlc1khnsfd9yc8dfq9gfmj655")))

(define-public crate-tdo-export-0.2.1 (c (n "tdo-export") (v "0.2.1") (d (list (d (n "colored") (r "^1.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.6") (d #t) (k 0)) (d (n "reqwest") (r "^0.5") (d #t) (k 0)) (d (n "tdo-core") (r "^0.2") (d #t) (k 0)))) (h "07qwvq4r2rhiw027saxkk6aakd64qf7phqrgzwdm86qgkqbf87wb")))

