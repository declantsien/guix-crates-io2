(define-module (crates-io td o- tdo-core) #:use-module (crates-io))

(define-public crate-tdo-core-0.1.0 (c (n "tdo-core") (v "0.1.0") (d (list (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)) (d (n "json") (r "^0.11.6") (d #t) (k 0)) (d (n "serde") (r "^0.9.5") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9.5") (d #t) (k 0)) (d (n "serde_json") (r "^0.9.4") (d #t) (k 0)))) (h "16lmgm28irndky6abvx1iqy2cp92w61jlj2rsi77ysgqdg7wkhjk")))

(define-public crate-tdo-core-0.1.1 (c (n "tdo-core") (v "0.1.1") (d (list (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)) (d (n "json") (r "^0.11.6") (d #t) (k 0)) (d (n "serde") (r "^0.9.5") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9.5") (d #t) (k 0)) (d (n "serde_json") (r "^0.9.4") (d #t) (k 0)))) (h "12fva6c6hc4nhwx06wkg6jppr1v08jgwvm27f0rp4nn8fna3qxjv")))

(define-public crate-tdo-core-0.2.0 (c (n "tdo-core") (v "0.2.0") (d (list (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)) (d (n "json") (r "^0.11.6") (d #t) (k 0)) (d (n "serde") (r "^0.9.5") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9.5") (d #t) (k 0)) (d (n "serde_json") (r "^0.9.4") (d #t) (k 0)))) (h "110by5n72mxb0m7livfdg7hwnmy58jch1x59x25sry11p14y9srd")))

(define-public crate-tdo-core-0.2.1 (c (n "tdo-core") (v "0.2.1") (d (list (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)) (d (n "json") (r "^0.11.6") (d #t) (k 0)) (d (n "serde") (r "^0.9.5") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9.5") (d #t) (k 0)) (d (n "serde_json") (r "^0.9.4") (d #t) (k 0)))) (h "0792l7gd8ril97hj4h73n3ihfqyks58c6jc4zcghjknh8las5qcy")))

