(define-module (crates-io td _c td_clua_ext) #:use-module (crates-io))

(define-public crate-td_clua_ext-0.1.0 (c (n "td_clua_ext") (v "0.1.0") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "td_rlua") (r "^0.1.0") (d #t) (k 0)))) (h "0qspr443yq1xyb5wh7wbhlbpy1gh7rich3bpwpnglslps52fka84")))

