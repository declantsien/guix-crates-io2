(define-module (crates-io td _c td_clua) #:use-module (crates-io))

(define-public crate-td_clua-0.1.0 (c (n "td_clua") (v "0.1.0") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2.1") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0yy3wiww4sz7rag6sl3pfn4x5gc9vhh28q1zspd2qr6rcg9wfyxj")))

(define-public crate-td_clua-0.1.2 (c (n "td_clua") (v "0.1.2") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2.1") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1i78gxrnb8nf6gyaq0aazf6n727lrnwvlnj45db4gbg5bmliffax")))

(define-public crate-td_clua-0.1.3 (c (n "td_clua") (v "0.1.3") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2.1") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "13glspc063r9w4w6dzkdva7lj127pszki2b7yy4j5dwppbp8ppdc") (l "lua")))

