(define-module (crates-io td ut tdutils) #:use-module (crates-io))

(define-public crate-tdutils-0.1.0 (c (n "tdutils") (v "0.1.0") (h "0k70rlf3zb25izv8471824mf9mrjcwp8h8yzywzpa73l846lq9pn")))

(define-public crate-tdutils-0.1.1 (c (n "tdutils") (v "0.1.1") (h "0vix6v3q48a7xzi2zncnf82nkmdhx321ynz6w16dhdxkrlccgqf5")))

