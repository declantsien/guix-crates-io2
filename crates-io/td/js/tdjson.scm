(define-module (crates-io td js tdjson) #:use-module (crates-io))

(define-public crate-tdjson-0.1.2 (c (n "tdjson") (v "0.1.2") (d (list (d (n "tdjson-sys") (r "^0.1") (d #t) (k 0)))) (h "11hwb05lbxdkhldhbd7sfb4svh7pql7hyj5b1qf51ffq63ymsyqg")))

(define-public crate-tdjson-0.2.0 (c (n "tdjson") (v "0.2.0") (d (list (d (n "tdjson-sys") (r "^0.1.3") (d #t) (k 0)))) (h "06lw4il6q0mb747ypkmjff0sj2nzasax5andyv6vadiqrgsvbn7r")))

(define-public crate-tdjson-0.2.1 (c (n "tdjson") (v "0.2.1") (d (list (d (n "tdjson-sys") (r "^0.1.4") (d #t) (k 0)))) (h "09h98hp4h0aj7f4dp3lsn4hlmyk30fp65azia0wm8a5gs3cvbxg7")))

(define-public crate-tdjson-0.2.2 (c (n "tdjson") (v "0.2.2") (d (list (d (n "tdjson-sys") (r "^0.1.5") (d #t) (k 0)))) (h "0qcxbncg08rbbr2jvqxg9lxyrkyijdssmm6zkk53qdf6i3nqzdhq")))

