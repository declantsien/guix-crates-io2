(define-module (crates-io td js tdjson-copy) #:use-module (crates-io))

(define-public crate-tdjson-copy-0.2.2 (c (n "tdjson-copy") (v "0.2.2") (d (list (d (n "tdjson-sys-copy") (r "^0.1.0") (d #t) (k 0)))) (h "1kxhvgxa7h8fra6x9983s4xsdky6vqyv1c9xnhim1g7v9268cxy6") (y #t)))

(define-public crate-tdjson-copy-0.1.0 (c (n "tdjson-copy") (v "0.1.0") (d (list (d (n "tdjson-sys-copy") (r "^0.1.0") (d #t) (k 0)))) (h "1jqkbxihk0h762sprmvfz287faccqv164vijfk20nqiizgh4bky3")))

(define-public crate-tdjson-copy-0.1.1 (c (n "tdjson-copy") (v "0.1.1") (d (list (d (n "tdjson-sys-copy") (r "^0.1.0") (d #t) (k 0)))) (h "0msp53gj3k413ff9nfvmlpm6rgd9nz8q2n883xaps5q96nkd248a")))

