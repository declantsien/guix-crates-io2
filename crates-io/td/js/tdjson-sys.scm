(define-module (crates-io td js tdjson-sys) #:use-module (crates-io))

(define-public crate-tdjson-sys-0.1.0 (c (n "tdjson-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.26.3") (d #t) (k 1)))) (h "16k1sawajgcrq9wacnxn3kszy4hzklfk14jyr56692psxf96isk5")))

(define-public crate-tdjson-sys-0.1.1 (c (n "tdjson-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.26.3") (d #t) (k 1)))) (h "0p4dgnh87i806isilbjh82cpmwlqadzmw7n8r9g8wnlach89kkd7")))

(define-public crate-tdjson-sys-0.1.2 (c (n "tdjson-sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.26.3") (d #t) (k 1)))) (h "0bj6a42airb3qqpakjygdyvnql8pl9zb0vqq51v3dacjr1cd090z")))

(define-public crate-tdjson-sys-0.1.3 (c (n "tdjson-sys") (v "0.1.3") (d (list (d (n "bindgen") (r "^0.26.3") (d #t) (k 1)))) (h "1n7j7c66am8zj2p9f1lr6ka1hknbhnnzjw6wjjwyvlg77m2j2n00")))

(define-public crate-tdjson-sys-0.1.4 (c (n "tdjson-sys") (v "0.1.4") (d (list (d (n "bindgen") (r "^0.26.3") (d #t) (k 1)))) (h "0idg0k2nhl3vrgnv26qzs868r51kxxz9z24aaci4dsink8mdi862")))

(define-public crate-tdjson-sys-0.1.5 (c (n "tdjson-sys") (v "0.1.5") (d (list (d (n "bindgen") (r "^0.26.3") (d #t) (k 1)))) (h "072nbsh27gghynshrbxc0qkrr0jxx0ndhzmfgq099m5qxf5pxi5j")))

