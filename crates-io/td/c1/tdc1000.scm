(define-module (crates-io td c1 tdc1000) #:use-module (crates-io))

(define-public crate-tdc1000-0.1.0 (c (n "tdc1000") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2.5") (d #t) (k 0)))) (h "061czvaszakg3g1pgvzxg0n0q40c01p4m1qxgbq4d4hzgbh914iv") (y #t)))

(define-public crate-tdc1000-0.1.1 (c (n "tdc1000") (v "0.1.1") (d (list (d (n "embedded-hal") (r "^0.2.5") (d #t) (k 0)))) (h "1aviz4419c1liyqpmh2f2rpsac8bnb419jswsmbnz5gg3w5p3qz7")))

(define-public crate-tdc1000-0.1.2 (c (n "tdc1000") (v "0.1.2") (d (list (d (n "embedded-hal") (r "^0.2.5") (d #t) (k 0)))) (h "02lnnvmww8v3dlzmwx8122h3xhljq2raqk9y1bdv17mmiy8w94z5")))

