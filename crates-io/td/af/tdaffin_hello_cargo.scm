(define-module (crates-io td af tdaffin_hello_cargo) #:use-module (crates-io))

(define-public crate-tdaffin_hello_cargo-0.1.0 (c (n "tdaffin_hello_cargo") (v "0.1.0") (h "135snnkk603zcxbw17advs11v8f1mnzf5ai0p46q4dz46skck1vb")))

(define-public crate-tdaffin_hello_cargo-0.1.1 (c (n "tdaffin_hello_cargo") (v "0.1.1") (h "0l7svsf9d8nphjkj5afdak1cvlfbak6dqbq6zryc2k08qhmjrrk4")))

