(define-module (crates-io td f_ tdf_utils) #:use-module (crates-io))

(define-public crate-tdf_utils-0.1.3 (c (n "tdf_utils") (v "0.1.3") (d (list (d (n "json") (r "^0.12") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "04h8y2xd4r6sfkip806lmxpwrv65lhfzyv4na3axfxdzwa7csp9g")))

(define-public crate-tdf_utils-0.1.4 (c (n "tdf_utils") (v "0.1.4") (d (list (d (n "json") (r "^0.12") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0kcdx5igbvymy21jq0m07fjb3j3hkkkkpkwpf56nnb7wv31kam61")))

(define-public crate-tdf_utils-0.1.5 (c (n "tdf_utils") (v "0.1.5") (d (list (d (n "json") (r "^0.12") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "17xxsl4r48wlq00q1rsbv55gqwy2v8ym2hyj92qgxc2xh693lvnw")))

(define-public crate-tdf_utils-0.1.8 (c (n "tdf_utils") (v "0.1.8") (d (list (d (n "json") (r "^0.12") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "09b2hi1ajf3wfhmjc2zh173ardzl2b081hgaimkd56in9mqy4pz7")))

(define-public crate-tdf_utils-0.2.0 (c (n "tdf_utils") (v "0.2.0") (d (list (d (n "json") (r "^0.12") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0qb8lar5fvqb4bx9i3jcd3nf0s480dwsa704xhw429a51h00z8kc")))

(define-public crate-tdf_utils-0.2.2 (c (n "tdf_utils") (v "0.2.2") (d (list (d (n "json") (r "^0.12") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "11izgpayc2x175gf39n0mnwchvr3ik62jwwg4mylhqcjhbiivh67")))

(define-public crate-tdf_utils-0.2.3 (c (n "tdf_utils") (v "0.2.3") (d (list (d (n "json") (r "^0.12") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1340fa5psg2ld3y2jam3zkyrg7smbbcwp79720b4nx9gs6hardmf")))

(define-public crate-tdf_utils-0.2.4 (c (n "tdf_utils") (v "0.2.4") (d (list (d (n "json") (r "^0.12") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1n0plxcx9cqs0d7d9jqfr8axcvrs3vrp8hbk9qm262l0788b81zv")))

