(define-module (crates-io td lg tdlg) #:use-module (crates-io))

(define-public crate-tdlg-0.1.0 (c (n "tdlg") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rand_pcg") (r "^0.3.1") (d #t) (k 0)) (d (n "rand_seeder") (r "^0.2.2") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "0b9ks4vadvvk0k7kl89q6h4jjalqmq6axpsif8cahp21iijxgs3f")))

(define-public crate-tdlg-0.1.1 (c (n "tdlg") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rand_pcg") (r "^0.3.1") (d #t) (k 0)) (d (n "rand_seeder") (r "^0.2.2") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "04i7ixv8g9f8f3fv3hqc2aj2n0xz26w391ss10dbya7hm74g81wx")))

(define-public crate-tdlg-0.1.2 (c (n "tdlg") (v "0.1.2") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rand_pcg") (r "^0.3.1") (d #t) (k 0)) (d (n "rand_seeder") (r "^0.2.2") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "1blmvw13whh16yc49qyk3igmy118d82cdz0sril0yhwnm56ddrzd")))

(define-public crate-tdlg-2.0.0 (c (n "tdlg") (v "2.0.0") (d (list (d (n "pathfinding") (r "^4.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rand_pcg") (r "^0.3") (d #t) (k 0)) (d (n "rand_seeder") (r "^0.2") (d #t) (k 0)) (d (n "walkdir") (r "^2.3") (d #t) (k 0)))) (h "184cp46fif5q6nil81mcyznaxr0crpb4yx4l98rvg5v77g1mz70n")))

(define-public crate-tdlg-2.0.1 (c (n "tdlg") (v "2.0.1") (d (list (d (n "pathfinding") (r "^4.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rand_pcg") (r "^0.3") (d #t) (k 0)) (d (n "rand_seeder") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "walkdir") (r "^2.3") (d #t) (k 0)))) (h "0r8h7kc57iyvkkdj39701q3d2c2ghkyplj9x09ga7gjgmh3fb2sp")))

(define-public crate-tdlg-3.0.0 (c (n "tdlg") (v "3.0.0") (d (list (d (n "pathfinding") (r "^4.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rand_pcg") (r "^0.3") (d #t) (k 0)) (d (n "rand_seeder") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "walkdir") (r "^2.3") (d #t) (k 0)))) (h "02a8jnm7zx6zvrxg804sha0v0vbcqzp5y44nw9bja9rmlf77yz6h")))

