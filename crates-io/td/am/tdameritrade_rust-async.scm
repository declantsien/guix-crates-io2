(define-module (crates-io td am tdameritrade_rust-async) #:use-module (crates-io))

(define-public crate-tdameritrade_rust-async-0.1.0 (c (n "tdameritrade_rust-async") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.22") (d #t) (k 0)) (d (n "derive_builder") (r "^0.11.2") (d #t) (k 0)) (d (n "itertools") (r "^0.10.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thirtyfour") (r "^0.31.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "urlencoding") (r "^2.1.2") (d #t) (k 0)))) (h "13w3q49c42h3bnb7qhlynam69s0yjn2237hkky2v4pvmxzshr7nj") (y #t)))

