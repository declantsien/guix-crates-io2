(define-module (crates-io td x_ tdx_attest) #:use-module (crates-io))

(define-public crate-tdx_attest-0.1.0 (c (n "tdx_attest") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "nix") (r "^0.26.2") (d #t) (k 0)))) (h "0w8dfwh2hv7bvf13b5abiv6f6y6wrfsnx0ichd1kl196g3qv3ms1") (y #t)))

(define-public crate-tdx_attest-0.1.1 (c (n "tdx_attest") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "nix") (r "^0.26.2") (d #t) (k 0)))) (h "114bwjf24ncngch33rs305cxxnpjzzgswdqp3rbfcjvmhfgrnlcv")))

