(define-module (crates-io td -s td-shim-interface) #:use-module (crates-io))

(define-public crate-td-shim-interface-0.1.0 (c (n "td-shim-interface") (v "0.1.0") (d (list (d (n "log") (r "^0.4.13") (d #t) (k 0)) (d (n "r-efi") (r "^3.2.0") (d #t) (k 0)) (d (n "scroll") (r "^0.10") (f (quote ("derive"))) (k 0)) (d (n "zerocopy") (r "^0.7.31") (f (quote ("derive"))) (d #t) (k 0)))) (h "18643lk7463v4vmc19azmzxif86ia9rfvjbkdan55hx4i5nxzkgh")))

(define-public crate-td-shim-interface-0.1.1 (c (n "td-shim-interface") (v "0.1.1") (d (list (d (n "log") (r "^0.4.13") (d #t) (k 0)) (d (n "r-efi") (r "^3.2.0") (d #t) (k 0)) (d (n "scroll") (r "^0.10") (f (quote ("derive"))) (k 0)) (d (n "zerocopy") (r "^0.7.31") (f (quote ("derive"))) (d #t) (k 0)))) (h "0n140kaw10ln8x4001a0l4dn9qhapa0rwkdwb3mmi4xsv7zfih46")))

