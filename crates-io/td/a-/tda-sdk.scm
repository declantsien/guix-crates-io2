(define-module (crates-io td a- tda-sdk) #:use-module (crates-io))

(define-public crate-tda-sdk-0.1.0 (c (n "tda-sdk") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "dotenv") (r "^0.15") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "ureq") (r "^1.1") (f (quote ("json"))) (d #t) (k 0)))) (h "1ccnqi9p3y6qmkabns51dmsr9mqvgmkh91jdzacsigw73cwxz9ka")))

(define-public crate-tda-sdk-0.1.1 (c (n "tda-sdk") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "dotenv") (r "^0.15") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "ureq") (r "^1.1") (f (quote ("json"))) (d #t) (k 0)))) (h "0g9vhd8qj45hgrgawzw6sni74iiwjq30sz3znn7a68w7lrqb3gbm")))

(define-public crate-tda-sdk-0.1.2 (c (n "tda-sdk") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "dotenv") (r "^0.15") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "ureq") (r "^1.1") (f (quote ("json"))) (d #t) (k 0)))) (h "0m0crdf40s5s4j4sarm65gl7nnds4j1f1jplzmvx1kii23zf5m6k")))

(define-public crate-tda-sdk-0.1.3 (c (n "tda-sdk") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "dotenv") (r "^0.15") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "ureq") (r "^1.1") (f (quote ("json"))) (d #t) (k 0)))) (h "1fym2s9nyd90bi6jhacazis8w7jk74whlsh0ii5a2h39z04dwcrj")))

(define-public crate-tda-sdk-0.1.4 (c (n "tda-sdk") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "dotenv") (r "^0.15") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "ureq") (r "^1.1") (f (quote ("json"))) (d #t) (k 0)))) (h "0n463qsgjbx3lpqyghf4xj3mf0pppq4v3xfxzk90fsa8y4n2kgrv")))

(define-public crate-tda-sdk-0.1.5 (c (n "tda-sdk") (v "0.1.5") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "dotenv") (r "^0.15") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "ureq") (r "^1.1") (f (quote ("json"))) (d #t) (k 0)))) (h "1yrim4c4bq5kgpvhcvajbkz4bgr31m00kqwh7gqvi7ak8fcfv2v6")))

