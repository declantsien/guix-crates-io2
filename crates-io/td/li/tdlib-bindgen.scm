(define-module (crates-io td li tdlib-bindgen) #:use-module (crates-io))

(define-public crate-tdlib-bindgen-0.2.0 (c (n "tdlib-bindgen") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.53.2") (d #t) (k 1)) (d (n "tdlib-sys") (r "^0.1.0") (d #t) (k 0)))) (h "1rspglvbpx7l0iq6iwxgrzqv7mq1wagl3kifjzqr0y1jwn077ymn") (f (quote (("bundled_deps" "tdlib-sys/bundled_deps"))))))

(define-public crate-tdlib-bindgen-0.3.0 (c (n "tdlib-bindgen") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.53.2") (d #t) (k 1)) (d (n "tdlib-sys") (r "^0.2.0") (d #t) (k 0)))) (h "1fvgqns33kskmm22wk9bm9gylfvykipihfcappmrf914jhpnrh2z") (f (quote (("bundled_deps" "tdlib-sys/bundled_deps"))))))

