(define-module (crates-io td li tdlib-futures-copy) #:use-module (crates-io))

(define-public crate-tdlib-futures-copy-0.1.0 (c (n "tdlib-futures-copy") (v "0.1.0") (d (list (d (n "blocking") (r "^0.4.6") (d #t) (k 0)) (d (n "dotenv") (r "^0.13.0") (d #t) (k 2)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 2)) (d (n "futures") (r "^0.3.5") (d #t) (k 0)) (d (n "log") (r "^0.4.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-aux") (r "^0.5.3") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.78") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.27") (d #t) (k 0)) (d (n "tdjson-copy") (r "^0.1.1") (d #t) (k 0)) (d (n "tdlib-types") (r "^0.1.0") (d #t) (k 0)))) (h "0xvz6jwg6qhmwf1ak43kxzbjjg05hiclkp16bqip4g9lng0yaqil") (y #t)))

(define-public crate-tdlib-futures-copy-0.1.1 (c (n "tdlib-futures-copy") (v "0.1.1") (d (list (d (n "blocking") (r "^0.4.6") (d #t) (k 0)) (d (n "dotenv") (r "^0.13.0") (d #t) (k 2)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 2)) (d (n "futures") (r "^0.3.5") (d #t) (k 0)) (d (n "log") (r "^0.4.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-aux") (r "^0.5.3") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.78") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.27") (d #t) (k 0)) (d (n "tdjson-copy") (r "^0.1.1") (d #t) (k 0)) (d (n "tdlib-types") (r "^0.1.0") (d #t) (k 0)))) (h "0lvz7gfqvy0a9x837rjp0mlwjbpl2iwq6mbx1n7cllwngz6syfmb") (y #t)))

(define-public crate-tdlib-futures-copy-0.1.2 (c (n "tdlib-futures-copy") (v "0.1.2") (d (list (d (n "blocking") (r "^0.4.6") (d #t) (k 0)) (d (n "dotenv") (r "^0.13.0") (d #t) (k 2)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 2)) (d (n "futures") (r "^0.3.5") (d #t) (k 0)) (d (n "log") (r "^0.4.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-aux") (r "^0.5.3") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.78") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.27") (d #t) (k 0)) (d (n "tdjson-copy") (r "^0.1.1") (d #t) (k 0)) (d (n "tdlib-types") (r "^0.1.0") (d #t) (k 0)))) (h "1l5mh04fhmcwd9l33cy4f5f2a5d9mvmk75gv4m0rlr72690r7ki3") (y #t)))

(define-public crate-tdlib-futures-copy-0.1.3 (c (n "tdlib-futures-copy") (v "0.1.3") (d (list (d (n "blocking") (r "^0.4.6") (d #t) (k 0)) (d (n "dotenv") (r "^0.13.0") (d #t) (k 2)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 2)) (d (n "futures") (r "^0.3.5") (d #t) (k 0)) (d (n "log") (r "^0.4.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-aux") (r "^0.5.3") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.78") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.27") (d #t) (k 0)) (d (n "tdjson-copy") (r "^0.1.1") (d #t) (k 0)) (d (n "tdlib-types") (r "^0.1.0") (d #t) (k 0)))) (h "1bqas99vrfck9pa9jj4dwy8k2srzamlb0d94ahc9sqr9qfvxv54d")))

