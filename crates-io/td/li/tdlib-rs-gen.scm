(define-module (crates-io td li tdlib-rs-gen) #:use-module (crates-io))

(define-public crate-tdlib-rs-gen-1.0.0 (c (n "tdlib-rs-gen") (v "1.0.0") (d (list (d (n "tdlib-rs-parser") (r "^1.0.0") (d #t) (k 0)))) (h "19h9gm45ksni6qy9y75al4w9jvk6iaslj4zh6wpzimjzhpj3v87r")))

(define-public crate-tdlib-rs-gen-1.0.1 (c (n "tdlib-rs-gen") (v "1.0.1") (d (list (d (n "tdlib-rs-parser") (r "^1.0.1") (d #t) (k 0)))) (h "10l33zs57aa08dgp00rr1g1pdhv68ca8bivx30dpgqgag3r0nwzp")))

(define-public crate-tdlib-rs-gen-1.0.2 (c (n "tdlib-rs-gen") (v "1.0.2") (d (list (d (n "tdlib-rs-parser") (r "^1.0.2") (d #t) (k 0)))) (h "1xpg37maspw5a4mcnsf26svv3i6s1l79469bin0m4af3gz3i00wy")))

