(define-module (crates-io td li tdlib-tl-gen) #:use-module (crates-io))

(define-public crate-tdlib-tl-gen-0.1.0 (c (n "tdlib-tl-gen") (v "0.1.0") (d (list (d (n "tdlib-tl-parser") (r "^0.1") (d #t) (k 0)))) (h "1zl7wcckfva0qp954s2qj56ggdm1y8zb0sw4g11ipammg6117hch")))

(define-public crate-tdlib-tl-gen-0.2.0 (c (n "tdlib-tl-gen") (v "0.2.0") (d (list (d (n "tdlib-tl-parser") (r "^0.1") (d #t) (k 0)))) (h "11q60ljmd27d25k3rcqxr6wjqrd18lvdb4xrmmbkd3lr60f3hw65")))

(define-public crate-tdlib-tl-gen-0.3.0 (c (n "tdlib-tl-gen") (v "0.3.0") (d (list (d (n "tdlib-tl-parser") (r "^0.2") (d #t) (k 0)))) (h "15pf65y1jq9w8sw9fij2g4vgljfn5n5vbmj5k3pj2p0mas539miy")))

(define-public crate-tdlib-tl-gen-0.3.1 (c (n "tdlib-tl-gen") (v "0.3.1") (d (list (d (n "tdlib-tl-parser") (r "^0.2") (d #t) (k 0)))) (h "0i7cw0rngdgphfgnysdf7n8d45rs4fn06s7rbrg3yr0kl0vyrdjr")))

(define-public crate-tdlib-tl-gen-0.3.2 (c (n "tdlib-tl-gen") (v "0.3.2") (d (list (d (n "tdlib-tl-parser") (r "^0.2") (d #t) (k 0)))) (h "1h8v8nk2hz1br0y5bwqv37p19dd6w244mxqyvy38hkwfij970adf")))

(define-public crate-tdlib-tl-gen-0.4.0 (c (n "tdlib-tl-gen") (v "0.4.0") (d (list (d (n "tdlib-tl-parser") (r "^0.2") (d #t) (k 0)))) (h "0iqbncqmm7k93lmmyvph6kn3w43f50djp7cw103sdx2ddq3ixap3")))

(define-public crate-tdlib-tl-gen-0.5.0 (c (n "tdlib-tl-gen") (v "0.5.0") (d (list (d (n "tdlib-tl-parser") (r "^0.2") (d #t) (k 0)))) (h "1aqz2by9whdbqy8a5768vr28s9jkzzb4hyicw7dfcazzdwsar4q4")))

