(define-module (crates-io td li tdlib-rs-parser) #:use-module (crates-io))

(define-public crate-tdlib-rs-parser-1.0.0 (c (n "tdlib-rs-parser") (v "1.0.0") (h "1f4n51v6ml5q3zwgv5cl5nlcl8sq3fq9ldj56wsmjrwfv6i67ngi")))

(define-public crate-tdlib-rs-parser-1.0.1 (c (n "tdlib-rs-parser") (v "1.0.1") (h "0vly0gn6c98y7r6nkymkz7w3pzrnxkj7wprdaxz9k21rhp75zp18")))

(define-public crate-tdlib-rs-parser-1.0.2 (c (n "tdlib-rs-parser") (v "1.0.2") (h "18lpv3ylkg40amc7vmq046wxyclh9v960yz2k6m4vv5b3xpbdafr")))

