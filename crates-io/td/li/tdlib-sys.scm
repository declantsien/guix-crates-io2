(define-module (crates-io td li tdlib-sys) #:use-module (crates-io))

(define-public crate-tdlib-sys-0.1.0 (c (n "tdlib-sys") (v "0.1.0") (d (list (d (n "cmake") (r "^0.1.42") (d #t) (k 1)) (d (n "libz-sys") (r "^1.0.25") (d #t) (k 0)) (d (n "openssl-sys") (r "^0.9.55") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 1)))) (h "1sd8q5i7rz0b8r1igh567ww7bc4lni36qnv2612c855d736z86yb") (f (quote (("vendored_openssl" "openssl-sys/vendored") ("static_zlib" "libz-sys/static") ("bundled_deps" "static_zlib" "vendored_openssl")))) (l "tdjson")))

(define-public crate-tdlib-sys-0.2.0 (c (n "tdlib-sys") (v "0.2.0") (d (list (d (n "cmake") (r "^0.1.42") (d #t) (k 1)) (d (n "libz-sys") (r "^1.0.25") (d #t) (k 0)) (d (n "openssl-sys") (r "^0.9.55") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 1)))) (h "1jxha6xbpas0q3d5l5gi0pxvphndi1shgr60n1arpkw7rs9dwr45") (f (quote (("vendored_openssl" "openssl-sys/vendored") ("static_zlib" "libz-sys/static") ("bundled_deps" "static_zlib" "vendored_openssl")))) (l "tdjson")))

