(define-module (crates-io td li tdlib-types) #:use-module (crates-io))

(define-public crate-tdlib-types-0.1.0 (c (n "tdlib-types") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.78") (d #t) (k 0)) (d (n "serde-aux") (r "^0.5.3") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.78") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.27") (d #t) (k 0)) (d (n "tl-codegen") (r "^0.1.0") (d #t) (k 1)))) (h "0l7vyyhxnwvlkmmfpdnvq06py6w69j9i4hixcmyb1195lrlr6rvm")))

