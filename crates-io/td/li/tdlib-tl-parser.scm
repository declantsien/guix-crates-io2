(define-module (crates-io td li tdlib-tl-parser) #:use-module (crates-io))

(define-public crate-tdlib-tl-parser-0.1.0 (c (n "tdlib-tl-parser") (v "0.1.0") (h "1s3sib36c8c9ndlg4lclgn0sjl39rmi3x2za1brnzxxxk7b9nb32")))

(define-public crate-tdlib-tl-parser-0.2.0 (c (n "tdlib-tl-parser") (v "0.2.0") (h "1hdy81jml4z1yqhfs25yz7g3lfp1wxcdr266dnglcazd2m9nncij")))

