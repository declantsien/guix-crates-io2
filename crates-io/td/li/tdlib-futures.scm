(define-module (crates-io td li tdlib-futures) #:use-module (crates-io))

(define-public crate-tdlib-futures-0.1.0 (c (n "tdlib-futures") (v "0.1.0") (d (list (d (n "dotenv") (r "^0.13.0") (d #t) (k 2)) (d (n "futures") (r "^0.1.24") (d #t) (k 0)) (d (n "log") (r "^0.4.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.78") (d #t) (k 0)) (d (n "serde-aux") (r "^0.5.3") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.78") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.27") (d #t) (k 0)) (d (n "tdjson") (r "^0.2") (d #t) (k 0)) (d (n "tdlib-types") (r "^0.1.0") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1.17") (d #t) (k 0)))) (h "1csm0g5a9kq020fix85hj281c53bzrg5l67amckpvihb0avlspbn")))

