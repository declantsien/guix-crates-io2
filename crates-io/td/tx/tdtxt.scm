(define-module (crates-io td tx tdtxt) #:use-module (crates-io))

(define-public crate-tdtxt-0.1.0 (c (n "tdtxt") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)))) (h "0w7nr40g04qwifl0hlga16mlaxp7n483g3ccc36n6vb38xi7n0dl")))

(define-public crate-tdtxt-0.2.0 (c (n "tdtxt") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.19") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "pretty_assertions") (r "^0.7.2") (d #t) (k 2)) (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.67") (d #t) (k 2)) (d (n "todo-txt") (r "^2.1.0") (d #t) (k 2)) (d (n "todotxt") (r "^0.3.0") (d #t) (k 2)))) (h "1jwv1y7v1i4959sp65zfxbhd0872s1614f77qsvlglb7r2i1dxky") (f (quote (("default") ("all" "chrono" "serde"))))))

(define-public crate-tdtxt-0.3.0 (c (n "tdtxt") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4.19") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "pretty_assertions") (r "^0.7.2") (d #t) (k 2)) (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.67") (d #t) (k 2)) (d (n "todo-txt") (r "^2.1.0") (d #t) (k 2)) (d (n "todotxt") (r "^0.3.0") (d #t) (k 2)))) (h "05kvkj654smg2nx57ip3rsv3pmb75463k7jrvd7y72amsz0kzqkg") (f (quote (("default") ("all" "chrono" "serde"))))))

