(define-module (crates-io td ne tdnet) #:use-module (crates-io))

(define-public crate-tdnet-0.1.0 (c (n "tdnet") (v "0.1.0") (h "1rzlqy3kwz9zkc5z6r32c0s2zms2hj0z592iq3n7chf12fr85k1h")))

(define-public crate-tdnet-0.1.2 (c (n "tdnet") (v "0.1.2") (d (list (d (n "actix-files") (r "^0.2") (d #t) (k 0)) (d (n "actix-rt") (r "^1.0") (d #t) (k 0)) (d (n "actix-web") (r "^2.0") (d #t) (k 0)))) (h "00vnb1lxnn2yk49dh6gyghzwyg7xdyqb7h5dmf2m2az01wd64bb9")))

