(define-module (crates-io td yn tdyne-peer-id-registry) #:use-module (crates-io))

(define-public crate-tdyne-peer-id-registry-0.1.0 (c (n "tdyne-peer-id-registry") (v "0.1.0") (d (list (d (n "phf") (r "^0.11") (d #t) (k 0)) (d (n "phf") (r "^0.11") (k 1)) (d (n "phf_codegen") (r "^0.11") (d #t) (k 1)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "tdyne-peer-id") (r "^1") (d #t) (k 0)) (d (n "test-case") (r "^3") (d #t) (k 2)))) (h "0q57mkalssydc4p4gmyicb3f34kvvh2319n6h2h4fh59mqd3i10p") (y #t)))

(define-public crate-tdyne-peer-id-registry-0.1.1 (c (n "tdyne-peer-id-registry") (v "0.1.1") (d (list (d (n "phf") (r "^0.11") (d #t) (k 0)) (d (n "phf") (r "^0.11") (k 1)) (d (n "phf_codegen") (r "^0.11") (d #t) (k 1)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "tdyne-peer-id") (r "^1") (d #t) (k 0)) (d (n "test-case") (r "^3") (d #t) (k 2)))) (h "09cchq723qwshp6rxg705zghjcr9vrc9phs7x2zfi070av9v48qr")))

