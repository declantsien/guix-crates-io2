(define-module (crates-io td yn tdyne-peer-id) #:use-module (crates-io))

(define-public crate-tdyne-peer-id-1.0.0 (c (n "tdyne-peer-id") (v "1.0.0") (d (list (d (n "pretty_assertions") (r "^1") (d #t) (k 2)))) (h "1i4v8l4ydgcslkf42wf3441lha61lwk4x4ikmz7isad5h50sbqlp") (y #t)))

(define-public crate-tdyne-peer-id-1.0.1 (c (n "tdyne-peer-id") (v "1.0.1") (d (list (d (n "pretty_assertions") (r "^1") (d #t) (k 2)))) (h "1ipp23i3h4c534g74slkicqrizaz46vw1v2kfl2f8qjsg8w115vv") (y #t)))

(define-public crate-tdyne-peer-id-1.0.2 (c (n "tdyne-peer-id") (v "1.0.2") (d (list (d (n "pretty_assertions") (r "^1") (d #t) (k 2)))) (h "0ghc6ws1bgj7zxfrq7a28zxsm2kp7xqg9m7r91b08dgqlrdjipkd")))

