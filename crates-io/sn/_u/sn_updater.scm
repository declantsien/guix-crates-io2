(define-module (crates-io sn _u sn_updater) #:use-module (crates-io))

(define-public crate-sn_updater-0.1.0 (c (n "sn_updater") (v "0.1.0") (d (list (d (n "self_update") (r "^0.32") (f (quote ("rustls" "archive-tar" "compression-flate2"))) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "0hs5ry0xnwl9i6gc7xk1prdhdqsnxda3wlfna9djhwxyiicanpkk")))

(define-public crate-sn_updater-0.2.0 (c (n "sn_updater") (v "0.2.0") (d (list (d (n "self_update") (r "^0.32") (f (quote ("rustls" "archive-tar" "compression-flate2"))) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "0yymyrwnznyfq3ssc6nasbjy9w7ngbp5fsx0x34qxclvzggmhhf2")))

