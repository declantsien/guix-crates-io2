(define-module (crates-io sn ow snowchains_proc_macros) #:use-module (crates-io))

(define-public crate-snowchains_proc_macros-0.1.0 (c (n "snowchains_proc_macros") (v "0.1.0") (d (list (d (n "combine") (r "^3.6.6") (d #t) (k 0)) (d (n "if_chain") (r "^0.1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.27") (d #t) (k 0)) (d (n "quote") (r "^0.6.11") (d #t) (k 0)) (d (n "syn") (r "^0.15.26") (f (quote ("full"))) (d #t) (k 0)))) (h "1m3d563bg44jg8xsn2cnkwzspa44idchvymvj4f8kvx6r0bz4gy5")))

(define-public crate-snowchains_proc_macros-0.2.0 (c (n "snowchains_proc_macros") (v "0.2.0") (d (list (d (n "heck") (r "^0.3.1") (d #t) (k 0)) (d (n "if_chain") (r "^1.0.0") (d #t) (k 0)) (d (n "itertools") (r "^0.8.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.8") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.14") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "18frmfq6mr1lrq9vrds5bcf1yvpg6n0m80ci5r99431di2y12z2b")))

(define-public crate-snowchains_proc_macros-0.2.1 (c (n "snowchains_proc_macros") (v "0.2.1") (d (list (d (n "heck") (r "^0.3.1") (d #t) (k 0)) (d (n "if_chain") (r "^1.0.0") (d #t) (k 0)) (d (n "itertools") (r "^0.8.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.8") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.14") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0qk574chxp62lfiry3zf6f8axdsfa9wccrnwqfnfd6bbjn2cvpac")))

