(define-module (crates-io sn ow snowcat_macros) #:use-module (crates-io))

(define-public crate-snowcat_macros-1.0.0 (c (n "snowcat_macros") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("clone-impls" "full" "parsing" "printing" "proc-macro"))) (k 0)))) (h "1v8f15dgaz270gdc1hvs6xlc7ahz3df6jb2mcjh70rhh1bhq5mz0")))

