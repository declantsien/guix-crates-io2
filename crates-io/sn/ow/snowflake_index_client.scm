(define-module (crates-io sn ow snowflake_index_client) #:use-module (crates-io))

(define-public crate-snowflake_index_client-0.2.0 (c (n "snowflake_index_client") (v "0.2.0") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "miette") (r "^5.5.0") (d #t) (k 0)) (d (n "octocrab") (r "^0.18.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.13") (f (quote ("json" "gzip"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "1p0rbn3rh1za9day7ka4dc8nx1a3yvamwh19by3d09hbvs97zfib")))

