(define-module (crates-io sn ow snowflaked) #:use-module (crates-io))

(define-public crate-snowflaked-0.1.0 (c (n "snowflaked") (v "0.1.0") (h "0yjahbvnjn8x4kn9pcklavrr6awbvsrh1hrn2a33h4mmna94c1az") (f (quote (("sync") ("default"))))))

(define-public crate-snowflaked-0.1.1 (c (n "snowflaked") (v "0.1.1") (h "1aa5npcv1zhg9zmrgf1jq06yiy6yfjlh4rghdrkfdi5477rzgg1m") (f (quote (("sync") ("default"))))))

(define-public crate-snowflaked-0.1.2 (c (n "snowflaked") (v "0.1.2") (h "0sisfwqbsg4sf38yswgq37knjcbdaiz4ashs8m5c1gxqpwx734fp") (f (quote (("sync") ("default"))))))

(define-public crate-snowflaked-0.1.3 (c (n "snowflaked") (v "0.1.3") (d (list (d (n "num_cpus") (r "^1.13.1") (d #t) (k 2)))) (h "0cj9480k3l0m86zh9fqd6fiyx8gdzlgv937q41r8m8nn9in17k8m") (f (quote (("sync") ("default"))))))

(define-public crate-snowflaked-0.1.4 (c (n "snowflaked") (v "0.1.4") (d (list (d (n "num_cpus") (r "^1.13.1") (d #t) (k 2)))) (h "0ffp14vb45xysq6md46jzwgvp9qxl6mvf224wj6dh9gh0gc7scw8") (f (quote (("sync") ("default"))))))

(define-public crate-snowflaked-0.1.5 (c (n "snowflaked") (v "0.1.5") (h "02wdiq8h4kc5k9rgyda6bg8rps00h8bqhp1i6frdf7kdymxrsr91") (f (quote (("sync") ("default"))))))

(define-public crate-snowflaked-0.1.6 (c (n "snowflaked") (v "0.1.6") (h "1hnk34fq4b6n93y0ygy6lrqs33ayf08czpn6xwl59v55njzasrns") (f (quote (("sync") ("default"))))))

(define-public crate-snowflaked-0.1.7 (c (n "snowflaked") (v "0.1.7") (h "1crfh2aw9ynzabs73xl132kvr6zc4r6wp9szzlj60y9hfzc5gd4k") (f (quote (("sync") ("default"))))))

(define-public crate-snowflaked-0.1.8 (c (n "snowflaked") (v "0.1.8") (h "0vds6cwmxapa5xvbw9km86qzwpg5b3anm5947ksiffr3jh9wdv4n") (f (quote (("sync") ("default"))))))

(define-public crate-snowflaked-1.0.0 (c (n "snowflaked") (v "1.0.0") (h "0pxcm7bmxy1d79vr8bb4gs1r87zgfm7bl1bqk7q8qda5gv4qw6m3") (f (quote (("sync") ("default"))))))

(define-public crate-snowflaked-1.0.1 (c (n "snowflaked") (v "1.0.1") (h "0lwcgn8bhq5pd2jffqp311la51qc2v6grnsyzmwzcal8lmkzf2ch") (f (quote (("sync") ("default"))))))

(define-public crate-snowflaked-1.0.2 (c (n "snowflaked") (v "1.0.2") (d (list (d (n "loom") (r "^0.5") (d #t) (t "cfg(loom)") (k 0)))) (h "0c692bskgah78byv9plbqhl0jjqw00azq0gcy160rbfh61yiycwd") (f (quote (("sync") ("default"))))))

(define-public crate-snowflaked-1.0.3 (c (n "snowflaked") (v "1.0.3") (d (list (d (n "loom") (r "^0.5") (d #t) (t "cfg(loom)") (k 0)))) (h "0l7q6aqf4pfjylaycskzlp3v9v50195v4f908nz9jhs59hn4d39r") (f (quote (("sync") ("default"))))))

