(define-module (crates-io sn ow snowflake) #:use-module (crates-io))

(define-public crate-snowflake-0.5.0 (c (n "snowflake") (v "0.5.0") (d (list (d (n "time") (r "*") (d #t) (k 2)))) (h "01rfw2g7q0y7qssrdmi3x6496nb313030k2wscgkqzlxjiaa9ydn")))

(define-public crate-snowflake-0.5.1 (c (n "snowflake") (v "0.5.1") (d (list (d (n "rand") (r "*") (d #t) (k 2)) (d (n "time") (r "*") (d #t) (k 2)) (d (n "uuid") (r "*") (d #t) (k 2)))) (h "007av51i0p2xnsjqa95bvbq07a8sskzlzsni32lcbcczknwkwvfb")))

(define-public crate-snowflake-0.5.2 (c (n "snowflake") (v "0.5.2") (d (list (d (n "rand") (r "*") (d #t) (k 2)) (d (n "threadpool") (r "*") (d #t) (k 2)) (d (n "time") (r "*") (d #t) (k 2)) (d (n "uuid") (r "*") (d #t) (k 2)))) (h "1gb9hs48qrqwb6ds0hl74ww67x43c30abl6zcfiilp5gb39965i2")))

(define-public crate-snowflake-0.6.0 (c (n "snowflake") (v "0.6.0") (d (list (d (n "rand") (r "*") (d #t) (k 2)) (d (n "threadpool") (r "*") (d #t) (k 2)) (d (n "time") (r "*") (d #t) (k 2)) (d (n "uuid") (r "*") (d #t) (k 2)))) (h "11yxlxk63aj46wqw2ndzg8p8y1aw23sz07wyx4qm8vs6mm0xwd3j")))

(define-public crate-snowflake-1.0.0 (c (n "snowflake") (v "1.0.0") (d (list (d (n "rand") (r "*") (d #t) (k 2)) (d (n "threadpool") (r "*") (d #t) (k 2)) (d (n "time") (r "*") (d #t) (k 2)) (d (n "uuid") (r "*") (d #t) (k 2)))) (h "1q0x2y9ax0xq65v1z48303a3dl15q0hw7l92w9ldpzrj199sac48")))

(define-public crate-snowflake-1.1.0 (c (n "snowflake") (v "1.1.0") (d (list (d (n "rand") (r "*") (d #t) (k 2)) (d (n "threadpool") (r "*") (d #t) (k 2)) (d (n "time") (r "*") (d #t) (k 2)) (d (n "uuid") (r "*") (d #t) (k 2)))) (h "153442v0vgmasxzhd6rdwzfaahhcyk0brcmdgij6wwxs17wxd5v6")))

(define-public crate-snowflake-1.2.0 (c (n "snowflake") (v "1.2.0") (d (list (d (n "clippy") (r "^0") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "threadpool") (r "^0.2") (d #t) (k 2)) (d (n "time") (r "^0.1") (d #t) (k 2)) (d (n "uuid") (r "^0.1") (d #t) (k 2)))) (h "17l04bdcg1c2ikdxkgn9v1hf5cm2r0dii12fsjif9ss3iih6bb98") (f (quote (("default"))))))

(define-public crate-snowflake-1.3.0 (c (n "snowflake") (v "1.3.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "threadpool") (r "^1") (d #t) (k 2)) (d (n "time") (r "^0.1") (d #t) (k 2)) (d (n "uuid") (r "^0.5") (f (quote ("v4"))) (d #t) (k 2)))) (h "1wadr7bxdxbmkbqkqsvzan6q1h3mxqpxningi3ss3v9jaav7n817") (f (quote (("serde_support" "serde" "serde_derive") ("default"))))))

