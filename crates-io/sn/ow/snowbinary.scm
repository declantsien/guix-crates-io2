(define-module (crates-io sn ow snowbinary) #:use-module (crates-io))

(define-public crate-snowbinary-0.0.0 (c (n "snowbinary") (v "0.0.0") (h "1s6vs23kvwarc6iv8zv7g22wvdjiz571rwf0875v83w52r8r22fj") (y #t)))

(define-public crate-snowbinary-0.1.0 (c (n "snowbinary") (v "0.1.0") (d (list (d (n "seahash") (r "^4.1.0") (o #t) (d #t) (k 0)))) (h "0rfmjpgijxvgf73w40xpfp6q1c7pjxaxrx4n9h3d0zkppax7k7mh") (f (quote (("v_hash" "seahash") ("default")))) (y #t)))

(define-public crate-snowbinary-0.2.0 (c (n "snowbinary") (v "0.2.0") (d (list (d (n "ahash") (r "^0.7.6") (o #t) (d #t) (k 0)))) (h "13fw2mdbj2a0iqw0w7caj8p7q71ml2vqdw0msajs70qa2nk3hafz") (f (quote (("v_hash" "ahash") ("default")))) (y #t)))

(define-public crate-snowbinary-0.3.0 (c (n "snowbinary") (v "0.3.0") (d (list (d (n "blake3") (r "^1.3.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "1jn57879bg73cgg71xv0fdn8jxzrfmhc3hmngg6gi8cs7syjrb7k") (f (quote (("default"))))))

(define-public crate-snowbinary-0.3.1 (c (n "snowbinary") (v "0.3.1") (d (list (d (n "blake3") (r "^1.5.0") (d #t) (k 0)))) (h "0n933gcpbqgikxs0my8333vh6pj0p772mif77735cin1kxrzwx97") (f (quote (("default")))) (r "1.66")))

(define-public crate-snowbinary-0.4.0 (c (n "snowbinary") (v "0.4.0") (d (list (d (n "blake3") (r "^1.5.1") (d #t) (k 0)))) (h "0wzi7rvk9h1j0ql106f81c1gwvhbarl2x56qigwv4y9l03ics2ns") (f (quote (("default")))) (r "1.66")))

