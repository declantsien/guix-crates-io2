(define-module (crates-io sn ow snowflake-rust) #:use-module (crates-io))

(define-public crate-snowflake-rust-0.1.0 (c (n "snowflake-rust") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.10") (d #t) (k 0)))) (h "1jdsfk9fldgs0xp49xh9dm1yj4wpdm1cr0lr5f7rqpbhj7yx90hx")))

(define-public crate-snowflake-rust-0.2.0 (c (n "snowflake-rust") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.10") (d #t) (k 0)))) (h "1ah72c1l2252p7jxs7gbrmqr1h23dcjlj6c7kb4ggmndpvjr5m6w")))

(define-public crate-snowflake-rust-0.3.0 (c (n "snowflake-rust") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4.10") (d #t) (k 0)))) (h "01404wvfh55prw8p2xm6x4z18rbc2b047c0ywjrgx2arfyra22mc")))

(define-public crate-snowflake-rust-0.4.0 (c (n "snowflake-rust") (v "0.4.0") (d (list (d (n "chrono") (r "^0.4.10") (d #t) (k 0)))) (h "1dqk5q9q8mvl7qiqrbxy2viskdiwajm2ki813zjawf94qmmryyiv")))

(define-public crate-snowflake-rust-0.5.0 (c (n "snowflake-rust") (v "0.5.0") (d (list (d (n "chrono") (r "^0.4.10") (d #t) (k 0)))) (h "0c9igrqd7chwyvpn51rvmzxwa79pbbsyp1n8faz78azxddrcy4dz")))

(define-public crate-snowflake-rust-0.5.1 (c (n "snowflake-rust") (v "0.5.1") (d (list (d (n "chrono") (r "^0.4.10") (d #t) (k 0)))) (h "1ys7pcr7i8lr3fjn8wkj9y0s3wcs7bcwynv119l134a51fnmxbdi")))

(define-public crate-snowflake-rust-0.5.2 (c (n "snowflake-rust") (v "0.5.2") (d (list (d (n "chrono") (r "^0.4.10") (d #t) (k 0)))) (h "11m1g907vgc62vqhv97d25ipyxcrwxjivv3y37izn35ah587s650")))

(define-public crate-snowflake-rust-0.5.3 (c (n "snowflake-rust") (v "0.5.3") (d (list (d (n "chrono") (r "^0.4.10") (d #t) (k 0)))) (h "0hsf78m7nqsq3l5blfc2z7ys80d7x85klwszinkmqw9r9wcxw2ym")))

(define-public crate-snowflake-rust-0.5.4 (c (n "snowflake-rust") (v "0.5.4") (d (list (d (n "chrono") (r "^0.4.10") (d #t) (k 0)))) (h "1h1r8swjx4lwplavj10anxy4p6x9w2p5h4vrk7v5likvpla7l1ys")))

(define-public crate-snowflake-rust-0.5.5 (c (n "snowflake-rust") (v "0.5.5") (d (list (d (n "chrono") (r "^0.4.10") (d #t) (k 0)))) (h "1dnvj59p316xfz7bq11vccwsk4m7s28bnkjrnnaqa21vq3cxh00p")))

(define-public crate-snowflake-rust-0.5.6 (c (n "snowflake-rust") (v "0.5.6") (d (list (d (n "chrono") (r "^0.4.10") (d #t) (k 0)))) (h "0adbdvlnlwh5yd7zcdwcqzlwdi1nb30alp5w5wimw2bdkh2afp4q")))

