(define-module (crates-io sn ow snowbridge-rococo-common) #:use-module (crates-io))

(define-public crate-snowbridge-rococo-common-0.0.0 (c (n "snowbridge-rococo-common") (v "0.0.0") (h "0m4xl580d61p0ajnx0mik7z5p4wiv17p01xi1lglw2q99b8v5blh")))

(define-public crate-snowbridge-rococo-common-0.1.0 (c (n "snowbridge-rococo-common") (v "0.1.0") (d (list (d (n "frame-support") (r "^28.0.0") (k 0)) (d (n "log") (r "^0.4.20") (k 0)) (d (n "xcm") (r "^7.0.0") (k 0) (p "staging-xcm")))) (h "194n08ki833va4l6368zvrgxg4zp20h3333qk83l08gcggxxvj1x") (f (quote (("std" "frame-support/std" "log/std" "xcm/std") ("runtime-benchmarks" "frame-support/runtime-benchmarks") ("default" "std"))))))

