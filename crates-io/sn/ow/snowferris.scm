(define-module (crates-io sn ow snowferris) #:use-module (crates-io))

(define-public crate-snowferris-0.1.0 (c (n "snowferris") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.1") (d #t) (k 0)))) (h "1d29f22gnarlnhvjlrsciwkgj0sspvfg321wfb236c5v9jid71fs") (y #t)))

(define-public crate-snowferris-0.2.1 (c (n "snowferris") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.1") (d #t) (k 0)))) (h "00vh3k6ba0cd2zda5vxknryj4mvf8wh05p45iwn16i5w88pb13jp")))

(define-public crate-snowferris-1.0.0 (c (n "snowferris") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 0)))) (h "1jmcjzzzi3pncppsc4ddkkdp8m9i31ix66jb4g91dgl7q7djvxys") (f (quote (("xid") ("snowflake") ("sno") ("sandflake") ("rngs") ("nanoid" "rngs") ("macros") ("ksuid") ("full" "snowflake" "sno" "sandflake" "ksuid" "xid" "nanoid" "macros"))))))

(define-public crate-snowferris-1.0.1 (c (n "snowferris") (v "1.0.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 0)))) (h "0rj4ja1pd1mcj8sm4bhff08xpnnmcmkjslzh8y7jlscrcdav8zs0") (f (quote (("xid") ("snowflake") ("sno") ("sandflake") ("rngs") ("nanoid" "rngs") ("macros") ("ksuid") ("full" "snowflake" "sno" "sandflake" "ksuid" "xid" "nanoid" "macros"))))))

(define-public crate-snowferris-1.0.2 (c (n "snowferris") (v "1.0.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 0)))) (h "1n66j32a35jn6dx54d6z1adq5l9vfkishkj1gfmj8q5kh4z6jb0s") (f (quote (("xid") ("snowflake") ("sno") ("sandflake") ("rngs") ("nanoid" "rngs") ("macros") ("ksuid") ("full" "snowflake" "sno" "sandflake" "ksuid" "xid" "nanoid" "macros"))))))

