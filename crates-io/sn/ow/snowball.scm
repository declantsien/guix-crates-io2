(define-module (crates-io sn ow snowball) #:use-module (crates-io))

(define-public crate-snowball-0.1.0 (c (n "snowball") (v "0.1.0") (d (list (d (n "crossbeam-channel") (r "^0.4.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "08w8dnygrllsj5y26rjs83ix0cr1d3ha9i98c4qba4pn7x6cp5x3")))

(define-public crate-snowball-0.1.1 (c (n "snowball") (v "0.1.1") (d (list (d (n "crossbeam-channel") (r "^0.4.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "01bdsyw445wiy2wdd7s9xbp0pr41ph82npy3pc34vp1bczx2cm0f")))

(define-public crate-snowball-0.1.2 (c (n "snowball") (v "0.1.2") (d (list (d (n "crossbeam-channel") (r "^0.4.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0ckzs4ivh31xlrsrn73s09im26nfnlb1kbnykk9px877qfsix28q")))

(define-public crate-snowball-0.1.3 (c (n "snowball") (v "0.1.3") (d (list (d (n "crossbeam-channel") (r "^0.4.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "179cdcfmfkxiclz6kbvp7krflrzqafywz8zd0f7x24vx32g6b76b")))

