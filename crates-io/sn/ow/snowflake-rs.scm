(define-module (crates-io sn ow snowflake-rs) #:use-module (crates-io))

(define-public crate-snowflake-rs-0.1.0 (c (n "snowflake-rs") (v "0.1.0") (d (list (d (n "time") (r "^0.1.40") (d #t) (k 0)))) (h "0h9lr65z2676fl3nwbda9yz4hnb9lyznay7085w66zpzm5av1xnc")))

(define-public crate-snowflake-rs-0.1.1 (c (n "snowflake-rs") (v "0.1.1") (d (list (d (n "time") (r "^0.1.40") (d #t) (k 0)))) (h "0pqk00dh8kjrv0iz428i156vhv4chb1rw7kkchwhv7yi7ns7x39c")))

