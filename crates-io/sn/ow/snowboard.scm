(define-module (crates-io sn ow snowboard) #:use-module (crates-io))

(define-public crate-snowboard-0.1.0 (c (n "snowboard") (v "0.1.0") (h "1d679fdkss3z9wyaygrh342a7yi2fdfdj6nvvy31rs2psxnnzkpl")))

(define-public crate-snowboard-0.2.0 (c (n "snowboard") (v "0.2.0") (h "09yg15jzibsc4302kw4s45w93grzah4fkzrjd1liy4s5cf143rfv")))

(define-public crate-snowboard-0.2.1 (c (n "snowboard") (v "0.2.1") (h "0yz1xr78lm4yriwj6vwb44y8cm7wqfkw77vvxaidvdz9v6c47yna")))

(define-public crate-snowboard-0.2.2 (c (n "snowboard") (v "0.2.2") (h "0l10wa8ag9wfjpv9nh1h3cala1b7qfhb2p5n6wmk9xpga6k5w0zl")))

(define-public crate-snowboard-0.3.0 (c (n "snowboard") (v "0.3.0") (h "1w81q2gp9151dva3xkhccbj9cnvly566d9k7vdh5xg1jqa08nypm")))

(define-public crate-snowboard-0.3.1 (c (n "snowboard") (v "0.3.1") (h "03vq7wzbc8vb271qj2mnh0n3rij6x0nr287nj22mgvx2ffi94hnz")))

(define-public crate-snowboard-0.3.2 (c (n "snowboard") (v "0.3.2") (h "0jj33lhjrc0b910fn45x3rfidyfi95dcjrfv4pnx3gsbjj76349y")))

(define-public crate-snowboard-0.4.0 (c (n "snowboard") (v "0.4.0") (h "0g0r5ffqcrvxj4cjs5bz4cvpphmwdwk7f6cr2z1sp9iybcsyy66h")))

(define-public crate-snowboard-0.4.1 (c (n "snowboard") (v "0.4.1") (d (list (d (n "async-std") (r "^1.12.0") (o #t) (d #t) (k 0)))) (h "13h5snhr5rrwp1y9wpdgpvqqn99nl9hskkis4wyhrgly0b3b6pl9") (f (quote (("default") ("async" "async-std"))))))

(define-public crate-snowboard-0.4.2 (c (n "snowboard") (v "0.4.2") (d (list (d (n "async-std") (r "^1.12.0") (o #t) (d #t) (k 0)))) (h "1fq3z9w0yd6ai8lg20k2cslwix2pr5qp6xssbcv7mh2l3lrphvff") (f (quote (("default") ("async" "async-std"))))))

(define-public crate-snowboard-0.4.3 (c (n "snowboard") (v "0.4.3") (d (list (d (n "async-std") (r "^1.12.0") (o #t) (d #t) (k 0)))) (h "1blfirs5f31adjqmip6l7fy5gms7i08ymff0xc087bxdlyldh9vy") (f (quote (("default") ("async" "async-std"))))))

(define-public crate-snowboard-0.4.4 (c (n "snowboard") (v "0.4.4") (d (list (d (n "async-std") (r "^1.12.0") (o #t) (d #t) (k 0)))) (h "0nvyq4yjdcq76swsgdjzn6c631sd3l397wj39hwwry996m1pj7b0") (f (quote (("default") ("async" "async-std"))))))

(define-public crate-snowboard-0.4.5 (c (n "snowboard") (v "0.4.5") (d (list (d (n "async-std") (r "^1.12.0") (o #t) (d #t) (k 0)))) (h "0ywbxxaf9fx242ik2ddgz8c5yvfxs9ahv86qnrbnyddsa1zgxcsv") (f (quote (("default") ("async" "async-std"))))))

(define-public crate-snowboard-0.4.6 (c (n "snowboard") (v "0.4.6") (d (list (d (n "async-std") (r "^1.12.0") (o #t) (d #t) (k 0)))) (h "0w6qdz9zasgvca7igjlmy5wrqlx7m44wdpw15ijp8xg52h1gw3lx") (f (quote (("default") ("async" "async-std"))))))

(define-public crate-snowboard-0.5.0 (c (n "snowboard") (v "0.5.0") (d (list (d (n "async-std") (r "^1.12.0") (o #t) (d #t) (k 0)) (d (n "native-tls") (r "^0.2.11") (o #t) (d #t) (k 0)))) (h "1b7in9k9vfq9nbkj5qxd2lv4bgb0s4x0srxrcy3mck7n8gndy9gs") (f (quote (("tls" "native-tls") ("full" "async" "tls") ("default") ("async" "async-std"))))))

(define-public crate-snowboard-0.5.1 (c (n "snowboard") (v "0.5.1") (d (list (d (n "async-std") (r "^1.12.0") (o #t) (d #t) (k 0)) (d (n "native-tls") (r "^0.2.11") (o #t) (d #t) (k 0)))) (h "1qawmk8waj2mb01nfcv7g5gl4ri99fn2x3mgfasmbx8hjcpafrbq") (f (quote (("tls" "native-tls") ("full" "async" "tls") ("default") ("async" "async-std"))))))

(define-public crate-snowboard-0.5.2 (c (n "snowboard") (v "0.5.2") (d (list (d (n "async-std") (r "^1.12.0") (o #t) (d #t) (k 0)) (d (n "native-tls") (r "^0.2.11") (o #t) (d #t) (k 0)))) (h "088dkgxq0fdiz3nrjqyz343h6szhcikh2j3hz505wqwcsq6kch5v") (f (quote (("tls" "native-tls") ("full" "async" "tls") ("default") ("async" "async-std"))))))

(define-public crate-snowboard-0.5.3 (c (n "snowboard") (v "0.5.3") (d (list (d (n "async-std") (r "^1.12.0") (o #t) (d #t) (k 0)) (d (n "native-tls") (r "^0.2.11") (o #t) (d #t) (k 0)))) (h "052jrxsq32c4sd5wcmy39b08qbw8gpl4va91bp27mmzkhhpn6zv9") (f (quote (("tls" "native-tls") ("full" "async" "tls") ("default") ("async" "async-std"))))))

(define-public crate-snowboard-0.5.4 (c (n "snowboard") (v "0.5.4") (d (list (d (n "async-std") (r "^1.12.0") (o #t) (d #t) (k 0)) (d (n "native-tls") (r "^0.2.11") (o #t) (d #t) (k 0)))) (h "09in24c4f78jmwwdq7c7xxi8d73asngms4mxr3bmnch8ca64v7f7") (f (quote (("tls" "native-tls") ("full" "async" "tls") ("default") ("async" "async-std"))))))

(define-public crate-snowboard-0.5.5 (c (n "snowboard") (v "0.5.5") (d (list (d (n "async-std") (r "^1.12.0") (o #t) (d #t) (k 0)) (d (n "native-tls") (r "^0.2.11") (o #t) (d #t) (k 0)))) (h "0fnkn98m7pki33wb6bvkks6hzp4wnrknaxv40szibdb3j6wfmbf1") (f (quote (("tls" "native-tls") ("full" "async" "tls") ("default") ("async" "async-std")))) (r "1.60.0")))

(define-public crate-snowboard-0.5.5-fix (c (n "snowboard") (v "0.5.5-fix") (d (list (d (n "async-std") (r "^1.12.0") (o #t) (d #t) (k 0)) (d (n "native-tls") (r "^0.2.11") (o #t) (d #t) (k 0)))) (h "1wi1qrf99anc0c0jrp0rixf550bz310p24yi740ig4s0hkc09yv0") (f (quote (("tls" "native-tls") ("full" "async" "tls") ("default") ("async" "async-std")))) (r "1.60.0")))

(define-public crate-snowboard-0.6.0 (c (n "snowboard") (v "0.6.0") (d (list (d (n "async-std") (r "^1.12.0") (o #t) (d #t) (k 0)) (d (n "native-tls") (r "^0.2.11") (o #t) (d #t) (k 0)))) (h "1g3lprdx547gj2bcz0lcg59z96siafy4s7q4p8srn745rx3adaai") (f (quote (("tls" "native-tls") ("full" "async" "tls") ("default") ("async" "async-std")))) (r "1.60.0")))

(define-public crate-snowboard-0.6.1 (c (n "snowboard") (v "0.6.1") (d (list (d (n "async-std") (r "^1.12.0") (o #t) (d #t) (k 0)) (d (n "native-tls") (r "^0.2.11") (o #t) (d #t) (k 0)))) (h "0a84f5dvkkm2r4nhiqpwz1crhk5y9m7s382jxqhfg09harq51swl") (f (quote (("tls" "native-tls") ("full" "async" "tls") ("default") ("async" "async-std")))) (r "1.60.0")))

(define-public crate-snowboard-0.6.2 (c (n "snowboard") (v "0.6.2") (d (list (d (n "async-std") (r "^1.12.0") (o #t) (d #t) (k 0)) (d (n "native-tls") (r "^0.2.11") (o #t) (d #t) (k 0)))) (h "0h67zbcw8dxa00hcyp2cvih5v7qw5vsni1by5b6iysabnq862a4n") (f (quote (("tls" "native-tls") ("full" "async" "tls") ("default") ("async" "async-std")))) (r "1.60.0")))

(define-public crate-snowboard-0.6.3 (c (n "snowboard") (v "0.6.3") (d (list (d (n "async-std") (r "^1.12.0") (o #t) (d #t) (k 0)) (d (n "native-tls") (r "^0.2.11") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (o #t) (d #t) (k 0)))) (h "1xkjb5h2jpi293ym7d4k8pf0xvip413q49fcba2dhp0ph6mw3f19") (f (quote (("tls" "native-tls") ("json" "serde_json" "serde") ("full" "async" "tls") ("default") ("async" "async-std")))) (r "1.60.0")))

(define-public crate-snowboard-0.7.0 (c (n "snowboard") (v "0.7.0") (d (list (d (n "async-std") (r "^1.12.0") (o #t) (d #t) (k 0)) (d (n "base64") (r "^0.21.5") (o #t) (d #t) (k 0)) (d (n "native-tls") (r "^0.2.11") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (o #t) (d #t) (k 0)) (d (n "sha1") (r "^0.10.6") (o #t) (d #t) (k 0)) (d (n "tungstenite") (r "^0.20.1") (o #t) (d #t) (k 0)))) (h "120ddz270ywc4dbvwag2z4wwnirbpwc6kklh88qlmps5av2bbvmi") (f (quote (("websocket" "tungstenite" "base64" "sha1") ("tls" "native-tls") ("json" "serde_json" "serde") ("full" "async" "tls") ("default") ("async" "async-std")))) (r "1.60.0")))

(define-public crate-snowboard-0.7.2 (c (n "snowboard") (v "0.7.2") (d (list (d (n "async-std") (r "^1.12.0") (o #t) (d #t) (k 0)) (d (n "base64") (r "^0.21.5") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "native-tls") (r "^0.2.11") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (o #t) (d #t) (k 0)) (d (n "sha1") (r "^0.10.6") (o #t) (d #t) (k 0)) (d (n "tungstenite") (r "^0.20.1") (o #t) (d #t) (k 0)))) (h "0p7jphiii83j92zqjl04143phcbv2amlkqkjxfsp6i63pdj8m6l2") (f (quote (("websocket" "tungstenite" "base64" "sha1") ("tls" "native-tls") ("json" "serde_json" "serde") ("full" "async" "tls" "json" "websocket") ("default") ("async" "async-std")))) (r "1.60.0")))

(define-public crate-snowboard-0.7.3 (c (n "snowboard") (v "0.7.3") (d (list (d (n "async-std") (r "^1.12.0") (o #t) (d #t) (k 0)) (d (n "base64") (r "^0.21.5") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "native-tls") (r "^0.2.11") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (o #t) (d #t) (k 0)) (d (n "sha1") (r "^0.10.6") (o #t) (d #t) (k 0)) (d (n "tungstenite") (r "^0.20.1") (o #t) (d #t) (k 0)))) (h "0vk3nm261jind26aj0g37ypi64pa39srqj31cjvwwb1mszj5pyad") (f (quote (("websocket" "tungstenite" "base64" "sha1") ("tls" "native-tls") ("json" "serde_json" "serde") ("full" "async" "tls" "json" "websocket") ("default") ("async" "async-std")))) (r "1.60.0")))

(define-public crate-snowboard-1.0.0 (c (n "snowboard") (v "1.0.0") (d (list (d (n "async-std") (r "^1.12.0") (o #t) (d #t) (k 0)) (d (n "base64") (r "^0.21.5") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "native-tls") (r "^0.2.11") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (o #t) (d #t) (k 0)) (d (n "sha1") (r "^0.10.6") (o #t) (d #t) (k 0)) (d (n "tungstenite") (r "^0.20.1") (o #t) (d #t) (k 0)))) (h "00znpd37id82zwygwawpbngscgaqlhxcgnlk19m876ix7d6r6dla") (f (quote (("websocket" "tungstenite" "base64" "sha1") ("tls" "native-tls") ("json" "serde_json" "serde") ("full" "async" "tls" "json" "websocket") ("default") ("async" "async-std")))) (r "1.60.0")))

(define-public crate-snowboard-1.0.1 (c (n "snowboard") (v "1.0.1") (d (list (d (n "async-std") (r "^1.12.0") (o #t) (d #t) (k 0)) (d (n "base64") (r "^0.21.5") (o #t) (d #t) (k 0)) (d (n "chrono") (r "^0.4.31") (f (quote ("clock"))) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "native-tls") (r "^0.2.11") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (o #t) (d #t) (k 0)) (d (n "sha1") (r "^0.10.6") (o #t) (d #t) (k 0)) (d (n "tungstenite") (r "^0.20.1") (o #t) (d #t) (k 0)))) (h "0xfd6jmabswkmwf7mf308y28qwynmyyccvysh0lq4jw0l7539acl") (f (quote (("websocket" "tungstenite" "base64" "sha1") ("tls" "native-tls") ("json" "serde_json" "serde") ("full" "async" "tls" "json" "websocket") ("default") ("async" "async-std")))) (r "1.60.0")))

(define-public crate-snowboard-1.0.2 (c (n "snowboard") (v "1.0.2") (d (list (d (n "async-std") (r "^1.12.0") (o #t) (d #t) (k 0)) (d (n "base64") (r "^0.21.5") (o #t) (d #t) (k 0)) (d (n "chrono") (r "^0.4.31") (f (quote ("clock"))) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "native-tls") (r "^0.2.11") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (o #t) (d #t) (k 0)) (d (n "sha1") (r "^0.10.6") (o #t) (d #t) (k 0)) (d (n "tungstenite") (r "^0.21.0") (o #t) (d #t) (k 0)))) (h "1k4gxiaddvdq377hn61rj0myk00phrnnsd84yz9pcywxdp7fir1z") (f (quote (("websocket" "tungstenite" "base64" "sha1") ("tls" "native-tls") ("json" "serde_json" "serde") ("full" "async" "tls" "json" "websocket") ("default") ("async" "async-std")))) (r "1.60.0")))

(define-public crate-snowboard-1.0.3 (c (n "snowboard") (v "1.0.3") (d (list (d (n "async-std") (r "^1.12.0") (o #t) (d #t) (k 0)) (d (n "base64") (r "^0.21.5") (o #t) (d #t) (k 0)) (d (n "chrono") (r "^0.4.31") (f (quote ("clock"))) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "native-tls") (r "^0.2.11") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (o #t) (d #t) (k 0)) (d (n "sha1") (r "^0.10.6") (o #t) (d #t) (k 0)) (d (n "tungstenite") (r "^0.21.0") (o #t) (d #t) (k 0)))) (h "1bnw8jzsdidzsxpmjcdn1m6jm57qv5zmyf5pfj6fbl87ai9cxwjj") (f (quote (("websocket" "tungstenite" "base64" "sha1") ("tls" "native-tls") ("json" "serde_json" "serde") ("full" "async" "tls" "json" "websocket") ("default") ("async" "async-std")))) (r "1.60.0")))

