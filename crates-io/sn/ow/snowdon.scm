(define-module (crates-io sn ow snowdon) #:use-module (crates-io))

(define-public crate-snowdon-0.1.0 (c (n "snowdon") (v "0.1.0") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "loom") (r "^0.5") (d #t) (t "cfg(loom)") (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1na619zayjy32kap0zda0in3j62dzsmkf1vpf6x22lcccmb2i70g") (f (quote (("lock-free") ("default" "blocking") ("blocking")))) (r "1.56.1")))

(define-public crate-snowdon-0.2.0 (c (n "snowdon") (v "0.2.0") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "loom") (r "^0.5") (d #t) (t "cfg(loom)") (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1ipbzl7vj3cqqjhv1ka6ilwlbszb64j5d896xdlclzlx9izfc4gd") (f (quote (("lock-free") ("default" "lock-free") ("blocking")))) (r "1.56.1")))

