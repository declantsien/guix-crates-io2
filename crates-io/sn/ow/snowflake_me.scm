(define-module (crates-io sn ow snowflake_me) #:use-module (crates-io))

(define-public crate-snowflake_me-0.1.5 (c (n "snowflake_me") (v "0.1.5") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "chrono") (r "^0.4.22") (d #t) (k 0)) (d (n "pnet_datalink") (r "^0.31.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "1hqw72q61d1wn7jl9bzrfd1876v9qv83x4dxfxx6ca0w559b4g4z")))

(define-public crate-snowflake_me-0.1.6 (c (n "snowflake_me") (v "0.1.6") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "chrono") (r "^0.4.22") (d #t) (k 0)) (d (n "pnet_datalink") (r "^0.31.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "1y4kmnaviashga88qnxyzdldcsy62lqky8nb7bwzhq1djrhgr1q0")))

(define-public crate-snowflake_me-0.1.7 (c (n "snowflake_me") (v "0.1.7") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "chrono") (r "^0.4.22") (d #t) (k 0)) (d (n "pnet_datalink") (r "^0.31.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)) (d (n "time") (r "^0.3.15") (d #t) (k 0)))) (h "0nwcc28qdkgd7dqf7fj2dn96ia52ck80lhdncdpyl5bv23hyfcff")))

(define-public crate-snowflake_me-0.1.8 (c (n "snowflake_me") (v "0.1.8") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "chrono") (r "^0.4.22") (d #t) (k 0)) (d (n "pnet_datalink") (r "^0.31.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)) (d (n "time") (r "^0.3.15") (d #t) (k 0)))) (h "15gj5z9l6wc4isfqdmny7h7vjn4f8pxay0npsvmq5p87mrgyh7w5")))

(define-public crate-snowflake_me-0.1.9 (c (n "snowflake_me") (v "0.1.9") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "pnet_datalink") (r "^0.33.0") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1pyl8wk7msd7l2qp3vbf6mv92qadqhscfi34dy7m1zlx7xxjf3gw") (f (quote (("default"))))))

(define-public crate-snowflake_me-0.1.10 (c (n "snowflake_me") (v "0.1.10") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "pnet") (r "^0.34") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0n3pbii17jwlaqryxfwxwmvl80gdvrinbkgms03hj9gs8kl28pnk")))

