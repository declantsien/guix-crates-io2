(define-module (crates-io sn ow snowgoggles) #:use-module (crates-io))

(define-public crate-snowgoggles-0.1.0 (c (n "snowgoggles") (v "0.1.0") (h "1p9ccrc2qrbdjfrw32cd3gy8r9idwi1xcbmh2bjyxk6y62prb9ds") (y #t)))

(define-public crate-snowgoggles-0.1.1 (c (n "snowgoggles") (v "0.1.1") (h "0q11c3q7nlzbmm90p5bqi84zcx7yb6d5bksbk9xk9zpj5anrg8my") (y #t)))

