(define-module (crates-io sn ow snowflaker) #:use-module (crates-io))

(define-public crate-snowflaker-0.1.0 (c (n "snowflaker") (v "0.1.0") (d (list (d (n "chronounit") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "05ijsjwf00nb56cr9y3inrmvhs2s1jk2mapsagnp6swr2516wkqr")))

(define-public crate-snowflaker-0.2.0 (c (n "snowflaker") (v "0.2.0") (d (list (d (n "chronounit") (r "^0.2") (d #t) (k 0)) (d (n "ifcfg") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "07n6lzpmda1sdhhk18p2csmkqml5s95awdxkplyl4spv6zi2ip5w") (f (quote (("dynamic"))))))

(define-public crate-snowflaker-0.2.1 (c (n "snowflaker") (v "0.2.1") (d (list (d (n "chronounit") (r "^0.2") (d #t) (k 0)) (d (n "ifcfg") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "13z52f7frsjmnmxd8q7swnk6yz31s0y2xnkm0hiimbsgm8jmn28a") (f (quote (("dynamic"))))))

(define-public crate-snowflaker-0.2.2 (c (n "snowflaker") (v "0.2.2") (d (list (d (n "chronounit") (r "^0.2") (d #t) (k 0)) (d (n "ifcfg") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "0k4sw4i7hp64wnrm0ik7gzmgiq4ba587xlp87b7q0jahp64wxj5v") (f (quote (("dynamic"))))))

(define-public crate-snowflaker-0.3.0 (c (n "snowflaker") (v "0.3.0") (d (list (d (n "chronounit") (r "^0.2") (d #t) (k 0)) (d (n "ifcfg") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "13iidm9gpbcfaw5jh25wkqwdia2ba9fink4jrzbvb38jd3szbwch") (f (quote (("dynamic"))))))

(define-public crate-snowflaker-0.3.1 (c (n "snowflaker") (v "0.3.1") (d (list (d (n "chronounit") (r "^0.2") (d #t) (k 0)) (d (n "ifcfg") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "0c9x4i1ykggxl44y7bh5wkqdc1iriqkckpriwxqgcr5ic756vqgy") (f (quote (("dynamic"))))))

(define-public crate-snowflaker-0.3.2 (c (n "snowflaker") (v "0.3.2") (d (list (d (n "chronounit") (r "^0.2") (d #t) (k 0)) (d (n "ifcfg") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "1cv6s0xjwaxwycyvvb2djxx4k8fxwf09j9lknlycam5y3v953qdw") (f (quote (("dynamic"))))))

(define-public crate-snowflaker-0.3.3 (c (n "snowflaker") (v "0.3.3") (d (list (d (n "chronounit") (r "^0.2") (d #t) (k 0)) (d (n "ifcfg") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "06idy9bvvf7wlyfmwhq6f33vbimvigwg3q52yvjq24savd0s3y5r") (f (quote (("dynamic"))))))

(define-public crate-snowflaker-0.3.4 (c (n "snowflaker") (v "0.3.4") (d (list (d (n "chronounit") (r "^0.2") (d #t) (k 0)) (d (n "ifcfg") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "17hn6yib9i546wqarmz98rapbap954f3bipijklqy8gmv9gaicww") (f (quote (("dynamic"))))))

(define-public crate-snowflaker-0.3.5 (c (n "snowflaker") (v "0.3.5") (d (list (d (n "chronounit") (r "^0.2") (d #t) (k 0)) (d (n "ifcfg") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "0rnp6mp9qakxagvb4mxal8rhnf63024xl16y4w226c17xhsgi1by") (f (quote (("dynamic"))))))

