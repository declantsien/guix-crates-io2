(define-module (crates-io sn ow snowflake-deserializer) #:use-module (crates-io))

(define-public crate-snowflake-deserializer-0.1.0 (c (n "snowflake-deserializer") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)))) (h "11q8pzdmwdlmpn6ajzng2p13frv9ksrvva6hgxddpj5gfbz24gzf")))

(define-public crate-snowflake-deserializer-0.1.2 (c (n "snowflake-deserializer") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)))) (h "0fbspmln1q610vv4v2h13qp7gd56pij9dx8jh22rfp72m49ji2bc")))

(define-public crate-snowflake-deserializer-0.2.0 (c (n "snowflake-deserializer") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.27.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)))) (h "13r8al34c7dd21b0dnr628w2y2cfc6qg24wjq0mpicrn5vf9nw4g")))

