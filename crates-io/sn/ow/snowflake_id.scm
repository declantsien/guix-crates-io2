(define-module (crates-io sn ow snowflake_id) #:use-module (crates-io))

(define-public crate-snowflake_id-0.1.0 (c (n "snowflake_id") (v "0.1.0") (h "1r1mbry1221ni22s83b9gjd4p46qwh9jpmvii5lgh4h93pf8ys34") (y #t)))

(define-public crate-snowflake_id-1.0.0 (c (n "snowflake_id") (v "1.0.0") (h "1mpp15jfzq7pja006ny0jnfqxpaanq759cbfjisgjkd3xjh59309")))

(define-public crate-snowflake_id-1.0.1 (c (n "snowflake_id") (v "1.0.1") (h "0n02pnparly8d9ynhawglh4rsyzpr52vigqsz3qzz3myp1cql7dx")))

