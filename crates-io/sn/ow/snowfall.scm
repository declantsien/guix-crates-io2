(define-module (crates-io sn ow snowfall) #:use-module (crates-io))

(define-public crate-snowfall-0.1.0 (c (n "snowfall") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0dgnf8mm5rfq8p5ir18ikhi4v6cb4jwv0ignasxfkl3hpnninc1b")))

