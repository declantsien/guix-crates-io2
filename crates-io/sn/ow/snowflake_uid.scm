(define-module (crates-io sn ow snowflake_uid) #:use-module (crates-io))

(define-public crate-snowflake_uid-0.1.0 (c (n "snowflake_uid") (v "0.1.0") (h "1ay5bf6vd7r63j1i8jjapawpkrjicvawgg8x66ypczljqkbfpf5r")))

(define-public crate-snowflake_uid-0.1.1 (c (n "snowflake_uid") (v "0.1.1") (h "0lq8i845iv1zgy27jl6c6wf23lfdm5gzfil02srbf63kfz4hnzhn")))

