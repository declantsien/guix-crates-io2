(define-module (crates-io sn ow snowflake-multi-threaded) #:use-module (crates-io))

(define-public crate-snowflake-multi-threaded-0.1.0 (c (n "snowflake-multi-threaded") (v "0.1.0") (h "1d3hfvf8jqr85nvql6i50mh7jmqxqi4l3s1i7yqj665qhz6l4666") (y #t)))

(define-public crate-snowflake-multi-threaded-0.1.1 (c (n "snowflake-multi-threaded") (v "0.1.1") (d (list (d (n "time") (r "^0.1.39") (d #t) (k 0)))) (h "0dn5zap4rfh2zqyky2cng4zfcm2367l51r82g3p3hz6s7xdfrw1w") (y #t)))

(define-public crate-snowflake-multi-threaded-0.1.2 (c (n "snowflake-multi-threaded") (v "0.1.2") (d (list (d (n "time") (r "^0.1.39") (d #t) (k 0)))) (h "1xpiwzm890fcz9di95i64647gjfylwpr70h2ypwa6572nfz9hsc1") (y #t)))

(define-public crate-snowflake-multi-threaded-0.1.3 (c (n "snowflake-multi-threaded") (v "0.1.3") (d (list (d (n "time") (r "^0.1.39") (d #t) (k 0)))) (h "170y189zhrdcl72lvp3h8bci5dca0w0vh6idzabq8zxkmfgc2nd6")))

(define-public crate-snowflake-multi-threaded-0.1.4 (c (n "snowflake-multi-threaded") (v "0.1.4") (d (list (d (n "time") (r "^0.1.39") (d #t) (k 0)))) (h "1b78acjy3z6fiyzp1xsyk7zfm54m6b2xjla5ljrzwlka77yyz4xq")))

