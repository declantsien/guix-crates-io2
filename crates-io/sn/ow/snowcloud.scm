(define-module (crates-io sn ow snowcloud) #:use-module (crates-io))

(define-public crate-snowcloud-0.1.0 (c (n "snowcloud") (v "0.1.0") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)))) (h "1syqmvg162v0b6lsklmhysrkn8hkwai3axhbybrng7lqldifqnql") (r "1.65.0")))

(define-public crate-snowcloud-0.1.1 (c (n "snowcloud") (v "0.1.1") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)))) (h "04fbqfsa4y8i5r2a283ggk2xzw3vlf2x10143rwl5wrmj22h62mz") (r "1.65.0")))

(define-public crate-snowcloud-0.2.0 (c (n "snowcloud") (v "0.2.0") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "01laj5dr7xxsk2k92llynal1967nxkkairniz8661n6xyprr9vxq") (s 2) (e (quote (("serde" "dep:serde")))) (r "1.65.0")))

(define-public crate-snowcloud-0.3.0 (c (n "snowcloud") (v "0.3.0") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "0lx3h7li9jk8fv2s3lgrkk9jkfkpg3wa0z238ssk62qvp2lqwq15") (s 2) (e (quote (("serde" "dep:serde")))) (r "1.65.0")))

(define-public crate-snowcloud-0.4.0 (c (n "snowcloud") (v "0.4.0") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "0m5mr6wkrg1c7yv7a10n3g9h0yrizdj4l2n43a93j78z94kqvwii") (s 2) (e (quote (("serde" "dep:serde")))) (r "1.65.0")))

