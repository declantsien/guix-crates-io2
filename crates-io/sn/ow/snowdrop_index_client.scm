(define-module (crates-io sn ow snowdrop_index_client) #:use-module (crates-io))

(define-public crate-snowdrop_index_client-0.2.0 (c (n "snowdrop_index_client") (v "0.2.0") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "miette") (r "^5.5.0") (d #t) (k 0)) (d (n "octocrab") (r "^0.18.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.13") (f (quote ("json" "gzip"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "1d1p3vvd76gy9h3234gmzf4wglycsh5q4qn8pzgzm94p4z7yzf0v")))

(define-public crate-snowdrop_index_client-0.4.0 (c (n "snowdrop_index_client") (v "0.4.0") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "miette") (r "^5.5.0") (d #t) (k 0)) (d (n "octocrab") (r "^0.18.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.13") (f (quote ("json" "gzip"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "1qarmxsab2bp004k45g9ry1ph6g3rq5brwxakvmmy73anpxq712d")))

