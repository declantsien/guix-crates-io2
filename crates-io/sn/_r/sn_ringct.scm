(define-module (crates-io sn _r sn_ringct) #:use-module (crates-io))

(define-public crate-sn_ringct-0.1.0 (c (n "sn_ringct") (v "0.1.0") (d (list (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1.0.130") (o #t) (d #t) (k 0)) (d (n "sn_bulletproofs") (r "^0.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tiny-keccak") (r "^2.0") (f (quote ("sha3"))) (d #t) (k 0)))) (h "0svxyw2xr1q0v2y9zl03f8yfnsc7gcndfm1qfr5f785wsm5ncp8x")))

