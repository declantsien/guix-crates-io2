(define-module (crates-io sn tp sntpc) #:use-module (crates-io))

(define-public crate-sntpc-0.1.0 (c (n "sntpc") (v "0.1.0") (h "0kshi9v0rjj4p6lrw8c6rv7dqaawbf8gqbnizpsh8w2ihkg3wj4c")))

(define-public crate-sntpc-0.1.1 (c (n "sntpc") (v "0.1.1") (h "1kqajlr93p7kcyd9xhp3k882d6qdxbyvmp9g2ai7i8qdk6kfr5dk")))

(define-public crate-sntpc-0.1.2 (c (n "sntpc") (v "0.1.2") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "simple_logger") (r "^1.3") (d #t) (k 2)))) (h "08zpi0588dbgcdcw5fcv8lcwi7qm6400n64z9c4a3mlcqjswp1mz")))

(define-public crate-sntpc-0.2.0 (c (n "sntpc") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "simple_logger") (r "^1.4") (d #t) (k 0)))) (h "0gjghwcd6ca8ysy7rnjzgfg9sz3p01146zyg3xclgaky44cjpr21")))

(define-public crate-sntpc-0.3.0 (c (n "sntpc") (v "0.3.0") (d (list (d (n "chrono") (r "~0.4") (o #t) (k 0)) (d (n "clap") (r "^2.33") (o #t) (k 0)) (d (n "log") (r "~0.4") (o #t) (d #t) (k 0)) (d (n "no-std-net") (r "~0.5") (d #t) (k 0)) (d (n "simple_logger") (r "~1.13") (d #t) (k 2)) (d (n "smoltcp") (r "~0.7") (f (quote ("phy-tap_interface" "socket-udp" "proto-ipv4"))) (k 2)))) (h "194my445s2ln5825yd8nmxh5k06y3dlpw3akkv6vqrnzr4k1rndk") (f (quote (("utils" "std" "chrono/clock") ("std") ("default" "std"))))))

(define-public crate-sntpc-0.3.1 (c (n "sntpc") (v "0.3.1") (d (list (d (n "chrono") (r "~0.4") (o #t) (k 0)) (d (n "clap") (r "^2.33") (k 2)) (d (n "log") (r "~0.4") (o #t) (d #t) (k 0)) (d (n "no-std-net") (r "~0.5") (d #t) (k 0)) (d (n "simple_logger") (r "~1.13") (d #t) (k 2)) (d (n "smoltcp") (r "~0.7") (f (quote ("phy-tap_interface" "socket-udp" "proto-ipv4"))) (k 2)))) (h "1wsjrz9c8l3n95y6kjf28cws0cxip0178kh5c7y0vh3lw46yznad") (f (quote (("utils" "std" "chrono/clock") ("std") ("default" "std"))))))

(define-public crate-sntpc-0.3.2 (c (n "sntpc") (v "0.3.2") (d (list (d (n "chrono") (r "~0.4") (o #t) (k 0)) (d (n "clap") (r "^2.33") (k 2)) (d (n "log") (r "~0.4") (o #t) (d #t) (k 0)) (d (n "no-std-net") (r "~0.5") (d #t) (k 0)) (d (n "simple_logger") (r "~1.13") (d #t) (k 2)) (d (n "smoltcp") (r "~0.7") (f (quote ("phy-tap_interface" "socket-udp" "proto-ipv4"))) (k 2)))) (h "06wq327y066qw5y7qpwm05h4nq8azlf4vallgz14n6qg4afbv6zq") (f (quote (("utils" "std" "chrono/clock") ("std") ("default" "std"))))))

(define-public crate-sntpc-0.3.3 (c (n "sntpc") (v "0.3.3") (d (list (d (n "chrono") (r "~0.4") (o #t) (k 0)) (d (n "clap") (r "^2.33") (k 2)) (d (n "log") (r "~0.4") (o #t) (d #t) (k 0)) (d (n "no-std-net") (r "~0.6") (d #t) (k 0)) (d (n "simple_logger") (r "~1.13") (d #t) (k 2)) (d (n "smoltcp") (r "~0.7") (f (quote ("phy-tap_interface" "socket-udp" "proto-ipv4"))) (k 2)))) (h "185lvr2wlsfp0md5yzr75gjp52r0y5ssqva8yx6xdh74m54icvdm") (f (quote (("utils" "std" "chrono/clock") ("std") ("default" "std"))))))

(define-public crate-sntpc-0.3.4 (c (n "sntpc") (v "0.3.4") (d (list (d (n "chrono") (r "~0.4") (o #t) (k 0)) (d (n "clap") (r "^2.33") (k 2)) (d (n "log") (r "~0.4") (o #t) (d #t) (k 0)) (d (n "no-std-net") (r "~0.6") (d #t) (k 0)) (d (n "simple_logger") (r "~1.13") (d #t) (k 2)) (d (n "smoltcp") (r "~0.9") (f (quote ("phy-tuntap_interface" "socket-udp" "proto-ipv4"))) (k 2)))) (h "1v2rxrgpnrxcv030v9crv21s6djrhpx4yd0k0i4h3rj5i6hg0yj8") (f (quote (("utils" "std" "chrono/clock") ("std") ("default" "std"))))))

(define-public crate-sntpc-0.3.5 (c (n "sntpc") (v "0.3.5") (d (list (d (n "chrono") (r "~0.4") (o #t) (k 0)) (d (n "clap") (r "^2.33") (k 2)) (d (n "log") (r "~0.4") (o #t) (d #t) (k 0)) (d (n "no-std-net") (r "~0.6") (d #t) (k 0)) (d (n "simple_logger") (r "~1.13") (d #t) (k 2)) (d (n "smoltcp") (r "~0.9") (f (quote ("phy-tuntap_interface" "socket-udp" "proto-ipv4"))) (k 2)))) (h "1qw885x5jayjpw3k5lrs9pnhf65kb7b6y9div4v8dmfjha6x0464") (f (quote (("utils" "std" "chrono/clock") ("std") ("default" "std"))))))

(define-public crate-sntpc-0.3.6 (c (n "sntpc") (v "0.3.6") (d (list (d (n "chrono") (r "~0.4") (o #t) (k 0)) (d (n "clap") (r "^2.33") (k 2)) (d (n "log") (r "~0.4") (o #t) (d #t) (k 0)) (d (n "no-std-net") (r "~0.6") (d #t) (k 0)) (d (n "simple_logger") (r "~1.13") (d #t) (k 2)) (d (n "smoltcp") (r "~0.9") (f (quote ("phy-tuntap_interface" "socket-udp" "proto-ipv4"))) (k 2)))) (h "1b131x41njmxwd3lj0i6cva3fd6cmgafqm9rydhp4bn4isnnrqbz") (f (quote (("utils" "std" "chrono/clock") ("std") ("default" "std"))))))

(define-public crate-sntpc-0.3.7 (c (n "sntpc") (v "0.3.7") (d (list (d (n "async-trait") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "chrono") (r "~0.4") (o #t) (k 0)) (d (n "clap") (r "^2.33") (k 2)) (d (n "log") (r "~0.4") (o #t) (d #t) (k 0)) (d (n "no-std-net") (r "~0.6") (d #t) (k 0)) (d (n "simple_logger") (r "~1.13") (d #t) (k 2)) (d (n "smoltcp") (r "~0.9") (f (quote ("phy-tuntap_interface" "socket-udp" "proto-ipv4"))) (k 2)) (d (n "tokio") (r "^1") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "09356ilpvf37lpd2gnfkf0ahn8iiwk4frph2qwa8lyrxz0w9z6k2") (f (quote (("utils" "std" "chrono/clock") ("std") ("default" "std") ("async_tokio" "std" "async" "tokio" "async-trait") ("async"))))))

