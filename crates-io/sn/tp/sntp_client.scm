(define-module (crates-io sn tp sntp_client) #:use-module (crates-io))

(define-public crate-sntp_client-1.2.0 (c (n "sntp_client") (v "1.2.0") (d (list (d (n "byteorder") (r "^0.5.1") (d #t) (k 0)) (d (n "clap") (r "^2.2.5") (f (quote ("unstable"))) (d #t) (k 0)) (d (n "time") (r "^0.1.35") (d #t) (k 0)))) (h "0bdi9vhs11hnkmxvvcis3vwdahiisp0m8jv7f853yaq3d7rmwl84")))

