(define-module (crates-io sn tp sntp_request) #:use-module (crates-io))

(define-public crate-sntp_request-1.0.0 (c (n "sntp_request") (v "1.0.0") (d (list (d (n "chrono") (r "^0.4.10") (d #t) (k 2)))) (h "06k6ds1z6zr8a5y5mf4nlpv4a32pf3a2y8g6hnpy6vnr7nqxml0v")))

(define-public crate-sntp_request-1.0.2 (c (n "sntp_request") (v "1.0.2") (d (list (d (n "chrono") (r "^0.4.10") (d #t) (k 2)))) (h "1xl1hw2xv8b7v7bd90a0c6ppg3jcsvbx1cqp0wyds0w6d0qz8v43")))

(define-public crate-sntp_request-1.0.3 (c (n "sntp_request") (v "1.0.3") (d (list (d (n "chrono") (r "^0.4.10") (d #t) (k 2)))) (h "1882awqww3gawbsw9m97cbzj5pyz27hk5342ad9ss59k6ywq429v")))

(define-public crate-sntp_request-1.0.4 (c (n "sntp_request") (v "1.0.4") (d (list (d (n "chrono") (r "^0.4.10") (d #t) (k 2)))) (h "1mgc54fl7cylv7zx9yjsk7azwyxf58y1p5683vxc24g2fpr5fygw")))

(define-public crate-sntp_request-1.1.0 (c (n "sntp_request") (v "1.1.0") (d (list (d (n "chrono") (r "^0.4.10") (d #t) (k 2)))) (h "1amihprjxpyv7bagl27ydsg7j4hfqan1c53p8xan91ndwyk2zcrr")))

(define-public crate-sntp_request-1.1.1 (c (n "sntp_request") (v "1.1.1") (d (list (d (n "chrono") (r "^0.4.10") (d #t) (k 2)))) (h "1jmn892kr3j497s3hvngfwdqfl70wkz5bwmc1gj1myx86rbgk1rb")))

(define-public crate-sntp_request-2.0.0 (c (n "sntp_request") (v "2.0.0") (d (list (d (n "chrono") (r "^0.4.11") (d #t) (k 2)))) (h "1bvjlixjhdmxpip5d9dw1s9bb5mnjxnmlyfl8xhkzmcf17xd11wl")))

(define-public crate-sntp_request-2.0.1 (c (n "sntp_request") (v "2.0.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)))) (h "1061srqkxkk9dgsnm7cscshk8h05j6p76c19js6m8dcbs95jckvy")))

