(define-module (crates-io sn tp sntp) #:use-module (crates-io))

(define-public crate-sntp-0.1.0 (c (n "sntp") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.3.4") (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 2)) (d (n "log") (r "^0.4.8") (o #t) (k 0)) (d (n "smoltcp") (r "^0.6.0") (f (quote ("proto-ipv4" "socket-udp"))) (k 0)))) (h "0rydbnad5rgdxl6mgk5i37djw7s5km4s9x4ypjciykcwq2mkh9g4") (f (quote (("tap" "log" "smoltcp/ethernet" "smoltcp/phy-tap_interface") ("default"))))))

(define-public crate-sntp-0.1.1 (c (n "sntp") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.3.4") (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 2)) (d (n "log") (r "^0.4.8") (o #t) (k 0)) (d (n "smoltcp") (r "^0.6.0") (f (quote ("proto-ipv4" "socket-udp"))) (k 0)))) (h "0g8azc664ra9b12sg0kabv0877hrylk0c0g020hxga3d50jg29is") (f (quote (("tap" "log" "smoltcp/ethernet" "smoltcp/phy-tap_interface") ("default"))))))

(define-public crate-sntp-0.1.2 (c (n "sntp") (v "0.1.2") (d (list (d (n "byteorder") (r "^1.3.4") (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 2)) (d (n "log") (r "^0.4.8") (o #t) (k 0)) (d (n "smoltcp") (r "^0.6.0") (f (quote ("proto-ipv4" "socket-udp"))) (k 0)))) (h "0yizn5hdswawcnzjynsvi2h7x46rmwrkl543f3mj31h5chi0icnr") (f (quote (("tap" "log" "smoltcp/ethernet" "smoltcp/phy-tap_interface") ("default"))))))

