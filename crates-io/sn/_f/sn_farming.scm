(define-module (crates-io sn _f sn_farming) #:use-module (crates-io))

(define-public crate-sn_farming-0.1.0 (c (n "sn_farming") (v "0.1.0") (d (list (d (n "bincode") (r "=1.2.1") (d #t) (k 0)) (d (n "crdts") (r "^4.1.0") (d #t) (k 0)) (d (n "rand") (r "~0.7.3") (d #t) (k 0)) (d (n "rayon") (r "^1.3.1") (d #t) (k 0)) (d (n "serde") (r "~1.0.97") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sn_data_types") (r "~0.11.7") (d #t) (k 0)) (d (n "threshold_crypto") (r "~0.4.0") (d #t) (k 0)))) (h "0lfynhrrfcqqffslyf94dd0i7k827c537qssnxzwbgyi8gfswm5v")))

