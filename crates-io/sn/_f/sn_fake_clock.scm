(define-module (crates-io sn _f sn_fake_clock) #:use-module (crates-io))

(define-public crate-sn_fake_clock-0.4.0 (c (n "sn_fake_clock") (v "0.4.0") (h "1aqnvs4z5dby4mw4nwm2brv9fy2q8lkqk08ihsi8n1m586avhfwv")))

(define-public crate-sn_fake_clock-0.4.1 (c (n "sn_fake_clock") (v "0.4.1") (h "0pjymyp19mg3dx95p5i5373vkvzi5q765axfxlcc517f96nc3sin")))

(define-public crate-sn_fake_clock-0.4.2 (c (n "sn_fake_clock") (v "0.4.2") (h "1m73kwyi5avixpw2hwh14a0xr4sf1l9ayjw0hkkk1875kswlpnrn")))

(define-public crate-sn_fake_clock-0.4.3 (c (n "sn_fake_clock") (v "0.4.3") (h "0h51a427w6zf2af3lg5mdww84s9a90593qs6q45nmaxzzgin1kmd")))

(define-public crate-sn_fake_clock-0.4.4 (c (n "sn_fake_clock") (v "0.4.4") (h "0bkvr4s0fkx8r0sm7admgcinyh7x7qqqdbnzrd52kwcdkkyjwh3n")))

(define-public crate-sn_fake_clock-0.4.5 (c (n "sn_fake_clock") (v "0.4.5") (h "0wn8spq9x8v3mv9b4xjz5lanz39ia6kiwjwz6hmw7fl7sfrpqp4z")))

(define-public crate-sn_fake_clock-0.4.6 (c (n "sn_fake_clock") (v "0.4.6") (h "0377y4axsbxhmmq5rzkfajlg8mpw6asagwmvzcmy5n8h9h6jryma")))

(define-public crate-sn_fake_clock-0.4.7 (c (n "sn_fake_clock") (v "0.4.7") (h "1vchp2x7ci9lzdwfzjiypl9bljdka7s264f5fyyiiymdffbl01cr")))

(define-public crate-sn_fake_clock-0.4.8 (c (n "sn_fake_clock") (v "0.4.8") (h "0bc5r2zm2i0zyzkwypzcw8a3zyfyvkbp1vpq899iwf5d6zm75wfr")))

(define-public crate-sn_fake_clock-0.4.9 (c (n "sn_fake_clock") (v "0.4.9") (h "03hhmli6h84ascvr5d2faydl9dzpg95jdcww3df6n7k0xm0ks39c")))

(define-public crate-sn_fake_clock-0.4.10 (c (n "sn_fake_clock") (v "0.4.10") (h "0g44djfpsf1sz539h3ljqa0wk67bj2bg012ksnlsfic1lpmdy3ya")))

(define-public crate-sn_fake_clock-0.4.11 (c (n "sn_fake_clock") (v "0.4.11") (h "0cb50wq8zj6blrh2kjx1yn8vm0ywcpx7hzjpq01z2djp6bd2pln3")))

(define-public crate-sn_fake_clock-0.4.12 (c (n "sn_fake_clock") (v "0.4.12") (h "1c0v8h0vgqaag32k4f0i9gpa1vn6b63hds1qsap43arh3fgljcfn")))

(define-public crate-sn_fake_clock-0.4.13 (c (n "sn_fake_clock") (v "0.4.13") (h "0i8ksjv951rjmy4wpvbs0g5g2384csczq1p59rjjf43vl851dwf5")))

(define-public crate-sn_fake_clock-0.4.14 (c (n "sn_fake_clock") (v "0.4.14") (h "10ybf5bf255alcc1sw4yjgz9ryvk8i409ynjm7rxazgx3idpsh62")))

