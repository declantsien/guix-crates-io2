(define-module (crates-io sn ai snail_nn) #:use-module (crates-io))

(define-public crate-snail_nn-0.1.0 (c (n "snail_nn") (v "0.1.0") (d (list (d (n "eframe") (r "^0.22.0") (d #t) (k 2)) (d (n "egui") (r "^0.22.0") (d #t) (k 2)) (d (n "egui_extras") (r "^0.22.0") (f (quote ("image"))) (d #t) (k 2)) (d (n "image") (r "^0.24.6") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)))) (h "0rhng7ly0aybb0c7pg0xmz0xh5qky1ynykhjdg4v59y60w1cm2hd")))

