(define-module (crates-io sn ai snail-countdown) #:use-module (crates-io))

(define-public crate-snail-countdown-1.0.2 (c (n "snail-countdown") (v "1.0.2") (d (list (d (n "clap") (r "^3.0.0-beta.1") (d #t) (k 0)) (d (n "notify-rust") (r "^4") (d #t) (k 0)))) (h "10n7r8qzn7gw5ppj4f5nn53vdqi6ip4s1mkvm3y3nkdlzp1c249i")))

(define-public crate-snail-countdown-1.1.0 (c (n "snail-countdown") (v "1.1.0") (d (list (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "notify-rust") (r "^4") (d #t) (k 0)))) (h "00qrbyfyc74ckrsvn7wvr80pdx13f6q259xibwq3gvra4nsq3klf")))

