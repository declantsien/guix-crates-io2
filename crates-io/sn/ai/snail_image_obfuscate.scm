(define-module (crates-io sn ai snail_image_obfuscate) #:use-module (crates-io))

(define-public crate-snail_image_obfuscate-1.0.0 (c (n "snail_image_obfuscate") (v "1.0.0") (d (list (d (n "clap") (r "^3.2.8") (d #t) (k 0)) (d (n "image") (r "^0.24.2") (d #t) (k 0)) (d (n "klask") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 0)))) (h "1cf95y75hd3nmsd3zlrla1f0y1y2bxpil6hppjfyl5xfyj4p3znk")))

(define-public crate-snail_image_obfuscate-1.0.1 (c (n "snail_image_obfuscate") (v "1.0.1") (d (list (d (n "clap") (r "^3.2") (d #t) (k 0)) (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "klask") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 0)))) (h "14nkqxfrzqr2hv80zl9c1i88ivy8ngr5088dx8frxziz863k40vs")))

