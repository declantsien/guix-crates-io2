(define-module (crates-io sn ai snail_image_confuse) #:use-module (crates-io))

(define-public crate-snail_image_confuse-0.1.0 (c (n "snail_image_confuse") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 0)))) (h "18fk296bv9g47nhcvnqz3adnlfm5pawb9d53j0zsg0gnha0kvy5s")))

