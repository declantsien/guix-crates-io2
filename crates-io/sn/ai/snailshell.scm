(define-module (crates-io sn ai snailshell) #:use-module (crates-io))

(define-public crate-snailshell-0.1.0 (c (n "snailshell") (v "0.1.0") (d (list (d (n "clap") (r "^3.2.15") (f (quote ("derive"))) (d #t) (k 0)))) (h "09qnx3a8arq7jh480pymhkrm1scbm98l99l53qav64y2hhy454qj")))

(define-public crate-snailshell-0.1.1 (c (n "snailshell") (v "0.1.1") (d (list (d (n "clap") (r "^3.2.15") (f (quote ("derive"))) (d #t) (k 0)))) (h "1v0ky6bcxf4j0a6hik7hzbsk662y769pfl0ra8i8phbz3vknfbg9")))

(define-public crate-snailshell-0.2.0 (c (n "snailshell") (v "0.2.0") (d (list (d (n "clap") (r "^3.2.15") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.24.0") (d #t) (k 2)))) (h "0r6vm5yc3hb91yn8wxrddjj4kazyy43gawc1c2m6l4dmjydrkwh7")))

(define-public crate-snailshell-0.2.1 (c (n "snailshell") (v "0.2.1") (d (list (d (n "clap") (r "^3.2.15") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.24.0") (d #t) (k 2)))) (h "0xapkcllzz1vdqnpv8xs2rlvzm35c9dmyjg01vaqyd2fpsskc0w5")))

(define-public crate-snailshell-0.2.2 (c (n "snailshell") (v "0.2.2") (d (list (d (n "clap") (r "^3.2.15") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.24.0") (d #t) (k 2)) (d (n "unicode-segmentation") (r "^1.9.0") (d #t) (k 0)))) (h "1812ji9a4wddm7bmxn0p1pmj70y3j3402w7vmqsnss6wy1p6iymw")))

(define-public crate-snailshell-0.3.0 (c (n "snailshell") (v "0.3.0") (d (list (d (n "clap") (r "^3.2.15") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.24.0") (d #t) (k 2)) (d (n "once_cell") (r "^1.13.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.9.0") (d #t) (k 0)))) (h "1kxkb0218gjny1xak2nn4k5irr73v0gd9876mkx2lr9y1kphnpna")))

(define-public crate-snailshell-0.3.1 (c (n "snailshell") (v "0.3.1") (d (list (d (n "clap") (r "^3.2.15") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.24.0") (d #t) (k 2)) (d (n "once_cell") (r "^1.13.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.9.0") (d #t) (k 0)))) (h "0sm9fb7xlrxgq7k0024l5iwpgrmphp1wibbsb3lkn2p8hw12cs8m")))

