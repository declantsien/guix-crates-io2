(define-module (crates-io sn ai snail_sort) #:use-module (crates-io))

(define-public crate-snail_sort-1.0.0 (c (n "snail_sort") (v "1.0.0") (h "0qkhnyn6m1w9z049xjzy186x3q5gk8z891h95pkk9awl4cvqch5q")))

(define-public crate-snail_sort-1.0.1 (c (n "snail_sort") (v "1.0.1") (h "0j7dmg43lg3c1kj6ijpg2y6r20i3h98nbxq91s6ccnkm4y3apjma")))

