(define-module (crates-io sn ai snailcrypt) #:use-module (crates-io))

(define-public crate-snailcrypt-0.1.0 (c (n "snailcrypt") (v "0.1.0") (d (list (d (n "base64") (r "^0.13.1") (d #t) (k 0)) (d (n "cbindgen") (r "^0.24.3") (d #t) (k 1)) (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "curl") (r "^0.4.44") (d #t) (k 0)) (d (n "openssl") (r "^0.10.42") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "1v12f9lf24x8y7w6zdql5lp0b2glg906ifj55cb3547ldn762g3f")))

(define-public crate-snailcrypt-0.2.0 (c (n "snailcrypt") (v "0.2.0") (d (list (d (n "base64") (r "^0.13.1") (d #t) (k 0)) (d (n "cbindgen") (r "^0.24.3") (d #t) (k 1)) (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "curl") (r "^0.4.44") (d #t) (k 0)) (d (n "openssl") (r "^0.10.42") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "19pgd17bgq1g3b7zmd0xr1rhspc31dm1xcnjiyhgz6hqvahivj10")))

(define-public crate-snailcrypt-0.3.0 (c (n "snailcrypt") (v "0.3.0") (d (list (d (n "base64") (r "^0.13.1") (d #t) (k 0)) (d (n "cbindgen") (r "^0.24.3") (d #t) (k 1)) (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "curl") (r "^0.4.44") (d #t) (k 0)) (d (n "openssl") (r "^0.10.42") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "18nsv5mw7hzv9nrh95d0dqfg0xnv6797wnz1zw4za8871wj2g514")))

