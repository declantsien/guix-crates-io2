(define-module (crates-io sn ai snailquote) #:use-module (crates-io))

(define-public crate-snailquote-0.1.0 (c (n "snailquote") (v "0.1.0") (d (list (d (n "quickcheck") (r "^0.6") (d #t) (k 2)) (d (n "unicode_categories") (r "^0.1.1") (d #t) (k 0)))) (h "00qyz2rhzkhxb7h61yj267nfld1wa1pxjlawf38ywb1mlwxh5s5b")))

(define-public crate-snailquote-0.1.1 (c (n "snailquote") (v "0.1.1") (d (list (d (n "quickcheck") (r "^0.6") (d #t) (k 2)) (d (n "unicode_categories") (r "^0.1.1") (d #t) (k 0)))) (h "1i6yav9ln76926ijqqc1cm7a9bpnkvagjg35m2w4y8s3lfa2hgpw")))

(define-public crate-snailquote-0.2.0 (c (n "snailquote") (v "0.2.0") (d (list (d (n "quickcheck") (r "^0.6") (d #t) (k 2)) (d (n "unicode_categories") (r "^0.1.1") (d #t) (k 0)))) (h "0kpglzhjxygjah857cgjzdj5881l0irn68l2zn3z6p1sdv1hxhbp")))

(define-public crate-snailquote-0.3.0 (c (n "snailquote") (v "0.3.0") (d (list (d (n "quickcheck") (r "^0.6") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "unicode_categories") (r "^0.1.1") (d #t) (k 0)))) (h "04687yzvgk3ja0ch7k96w3v36fzvqd9ci2l5k12ijligh2fp4jzk")))

(define-public crate-snailquote-0.3.1 (c (n "snailquote") (v "0.3.1") (d (list (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.9") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "unicode_categories") (r "^0.1.1") (d #t) (k 0)))) (h "11lgz0q7gxgpx8895y304i7j1qa6z44ijw8s9005iwd7pm4sjqpc") (f (quote (("unsafe_tests"))))))

