(define-module (crates-io sn #{0i}# sn0int-signal) #:use-module (crates-io))

(define-public crate-sn0int-signal-0.1.0 (c (n "sn0int-signal") (v "0.1.0") (d (list (d (n "actix") (r "^0.9.0") (d #t) (k 0)) (d (n "actix-rt") (r "^1.1.1") (d #t) (k 0)) (d (n "actix-web") (r "^2.0.0") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "serde") (r "^1.0.111") (d #t) (k 0)) (d (n "structopt") (r "^0.3.14") (d #t) (k 0)) (d (n "tokio") (r "^0.2.21") (f (quote ("process"))) (d #t) (k 0)))) (h "0i7jdwy75xw5m5ri5a6aa1gq7src742gmas0fq183g6gvifjxfzm")))

