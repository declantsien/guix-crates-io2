(define-module (crates-io sn #{0i}# sn0int-common) #:use-module (crates-io))

(define-public crate-sn0int-common-0.1.0 (c (n "sn0int-common") (v "0.1.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "nom") (r "^4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1qmvpcn4j8f37cglkf44gla6sf9r6rnfahaf36garidj7daybmg2")))

(define-public crate-sn0int-common-0.2.0 (c (n "sn0int-common") (v "0.2.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "nom") (r "^4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1x6k6vibb7swgfgrss9qncmxim276clsnlxxb9ivl311r2vz3xr6")))

(define-public crate-sn0int-common-0.3.0 (c (n "sn0int-common") (v "0.3.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "nom") (r "^4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1p5rq022sf6x990lhg66iljic65jica7g3kxrbvqjydx9b8v0gcw")))

(define-public crate-sn0int-common-0.4.0 (c (n "sn0int-common") (v "0.4.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "nom") (r "^4.0") (d #t) (k 0)) (d (n "rocket_failure") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "023bvbfgxgixdipmvrrhvcyc5g7k6gc979pk1vrillr4ky594pbg")))

(define-public crate-sn0int-common-0.5.0 (c (n "sn0int-common") (v "0.5.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "nom") (r "^4.0") (d #t) (k 0)) (d (n "rocket_failure") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0s4x277r3nw0ivzqmjd4nf1dr81k3fwgva2iq3z0y274dj1sjyh0")))

(define-public crate-sn0int-common-0.6.0 (c (n "sn0int-common") (v "0.6.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "nom") (r "^4.0") (d #t) (k 0)) (d (n "rocket_failure") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1my2ry6bz6m384jalsqg463yippidwacrcdrxz60i35ip5dlddga")))

(define-public crate-sn0int-common-0.7.0 (c (n "sn0int-common") (v "0.7.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "nom") (r "^4.0") (d #t) (k 0)) (d (n "rocket_failure") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0lwq0yhcfb0b8zsk7dl44vn7g1q23q3jslqrkf4cxvpjm31d3g0h")))

(define-public crate-sn0int-common-0.8.0 (c (n "sn0int-common") (v "0.8.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "nom") (r "^5.0") (d #t) (k 0)) (d (n "rocket_failure_errors") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "14p8h7sl4fkpn5rxacasx5k2a4mxc28jzfxrirjq0bl23b2kmqmn")))

(define-public crate-sn0int-common-0.9.0 (c (n "sn0int-common") (v "0.9.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "nom") (r "^5.0") (d #t) (k 0)) (d (n "rocket_failure_errors") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0p53aqm72x1p7lgbm9nnr8b6yw9f6dnmjb198blgggvzrj0pqpmq")))

(define-public crate-sn0int-common-0.10.0 (c (n "sn0int-common") (v "0.10.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "nom") (r "^5.0") (d #t) (k 0)) (d (n "rocket_failure_errors") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "16wb6f2dl3acbwsi3yyi74w6h4snqy9pg4y9k4dfczfn20p1hlxk")))

(define-public crate-sn0int-common-0.11.0 (c (n "sn0int-common") (v "0.11.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "nom") (r "^5.0") (d #t) (k 0)) (d (n "rocket_failure_errors") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0ymkb01pcr954m4kz51fbkkf2mascsrcbwgz1vbcm1p92iq2w9s4")))

(define-public crate-sn0int-common-0.12.0 (c (n "sn0int-common") (v "0.12.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "nom") (r "^6.0") (d #t) (k 0)) (d (n "rocket_failure_errors") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "15p788gcdbl92pxizgd8wvxrw7bk3zp5jm6dqgizpkzajhkd7kc6")))

(define-public crate-sn0int-common-0.13.0 (c (n "sn0int-common") (v "0.13.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "nom") (r "^7.0") (d #t) (k 0)) (d (n "rocket_failure_errors") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "083cd0qhg8k50vjl0fq9ymqkx7vnklcqvcsm9rcq75fb89fi7056")))

(define-public crate-sn0int-common-0.14.0 (c (n "sn0int-common") (v "0.14.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^4.3.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "nom") (r "^7.0") (d #t) (k 0)) (d (n "rocket_failure_errors") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "02nl3h67vm7ws09vfqjijn1cypzhn550cm85jp1nhaskz05rzkzi")))

