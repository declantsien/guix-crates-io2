(define-module (crates-io sn ma snmalloc-sys) #:use-module (crates-io))

(define-public crate-snmalloc-sys-0.1.0 (c (n "snmalloc-sys") (v "0.1.0") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (k 0)))) (h "1jlqnspxxpn547rpqymi8livkfyhgr64sgmfs8mqn0vj8bj9n6w1") (f (quote (("debug") ("cache-friendly") ("1mib"))))))

(define-public crate-snmalloc-sys-0.1.1 (c (n "snmalloc-sys") (v "0.1.1") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (k 0)))) (h "0wjjyxc6pd53kxyhwd64zbp00v36r3cj3fvh5i3zzk5gy0vm7axx") (f (quote (("debug") ("cache-friendly") ("1mib"))))))

(define-public crate-snmalloc-sys-0.1.2 (c (n "snmalloc-sys") (v "0.1.2") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (k 0)))) (h "15ndjjc6rcnccwziawn3vawfygsj669ag9v01bxhr06nrmiilp3r") (f (quote (("debug") ("cache-friendly") ("1mib"))))))

(define-public crate-snmalloc-sys-0.2.0 (c (n "snmalloc-sys") (v "0.2.0") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (k 0)))) (h "1hwlmppbb8brvlaiw70ncah8j5llz39pwzk2fqh2kyn36kdwy3pa") (f (quote (("debug") ("cache-friendly") ("1mib"))))))

(define-public crate-snmalloc-sys-0.2.1 (c (n "snmalloc-sys") (v "0.2.1") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (k 0)))) (h "1nv799ymd8zfhjqjawxs2xlpv2wi5pcjv25j2291nimi2rd0x5bi") (f (quote (("debug") ("cache-friendly") ("1mib"))))))

(define-public crate-snmalloc-sys-0.2.2 (c (n "snmalloc-sys") (v "0.2.2") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (k 0)))) (h "1gvgz0s4hfx9r4rmk218hmzphf0r6l9ixgyjd50riwc9grm2vxad") (f (quote (("debug") ("cache-friendly") ("1mib"))))))

(define-public crate-snmalloc-sys-0.2.3 (c (n "snmalloc-sys") (v "0.2.3") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (k 0)))) (h "0g6yxc35rpvxgxlg8jal7wv91asp9ic9m63q7pzlj7mw6akh4r4h") (f (quote (("debug") ("cache-friendly") ("1mib"))))))

(define-public crate-snmalloc-sys-0.2.4 (c (n "snmalloc-sys") (v "0.2.4") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (k 0)))) (h "17x63l20vqwcmcn3fgzy27fj4yy6wcxdwgk7ald9mg8qq8nkz8i4") (f (quote (("debug") ("cache-friendly") ("1mib"))))))

(define-public crate-snmalloc-sys-0.2.5 (c (n "snmalloc-sys") (v "0.2.5") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (k 0)))) (h "021zhspqh3kgrhrn7f1wnccyrzmpf6gsg0l4ii75hgwd5xiz9ix7") (f (quote (("debug") ("cache-friendly") ("1mib"))))))

(define-public crate-snmalloc-sys-0.2.6 (c (n "snmalloc-sys") (v "0.2.6") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (k 0)))) (h "0zz79dwfq5yvkw8ikfxip8pa1frfs44p1jp4vp1jklwdjy97pky2") (f (quote (("debug") ("cache-friendly") ("1mib"))))))

(define-public crate-snmalloc-sys-0.2.7 (c (n "snmalloc-sys") (v "0.2.7") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (k 0)))) (h "1chp2bv7iln24kw9pb2x8r9f2fnd057nnzil1rgmr1fgyhni7jps") (f (quote (("debug") ("cache-friendly") ("1mib"))))))

(define-public crate-snmalloc-sys-0.2.8 (c (n "snmalloc-sys") (v "0.2.8") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (k 0)))) (h "1y23zykaxrqh2mxzv2swwws2ygaiqw8r5mlizkypcj79m70i71iw") (f (quote (("debug") ("cache-friendly") ("1mib"))))))

(define-public crate-snmalloc-sys-0.2.9 (c (n "snmalloc-sys") (v "0.2.9") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (k 0)))) (h "0dnqgqh6z9b5glnfsdi94m4wfi7ma78yczy6hgrlwyz1jnmwpzvb") (f (quote (("debug") ("cache-friendly") ("1mib"))))))

(define-public crate-snmalloc-sys-0.2.10 (c (n "snmalloc-sys") (v "0.2.10") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (k 0)))) (h "0byhb09nlfdyg34jx8scj5crzxckn8d2rn13akmps0kvvc07a0dd") (f (quote (("debug") ("cache-friendly") ("1mib"))))))

(define-public crate-snmalloc-sys-0.2.11 (c (n "snmalloc-sys") (v "0.2.11") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (k 0)))) (h "1w581p9br01yc5m40ir56k3lbk0x1qahdrr4453j42xywrx4w8f0") (f (quote (("debug") ("cache-friendly") ("android-shared-stl") ("android-lld") ("1mib"))))))

(define-public crate-snmalloc-sys-0.2.12 (c (n "snmalloc-sys") (v "0.2.12") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (k 0)))) (h "0dq5mi1k9v438gspril9m32nfs5i5l8x23lj2qlsfw688rmkn339") (f (quote (("debug") ("cache-friendly") ("android-shared-stl") ("android-lld") ("1mib"))))))

(define-public crate-snmalloc-sys-0.2.13 (c (n "snmalloc-sys") (v "0.2.13") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (k 0)))) (h "0ssba6fikzzf174mqp8jdvbykbs6zqqgia79v0bc9w2m9mwr2im3") (f (quote (("debug") ("cache-friendly") ("android-shared-stl") ("android-lld") ("1mib"))))))

(define-public crate-snmalloc-sys-0.2.14 (c (n "snmalloc-sys") (v "0.2.14") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (k 0)))) (h "0pi52f5nygacbkjaqvlmfmna9g87fsnwzxhacsla8im67h9dn760") (f (quote (("native-cpu") ("debug") ("cache-friendly") ("android-shared-stl") ("android-lld") ("1mib"))))))

(define-public crate-snmalloc-sys-0.2.15 (c (n "snmalloc-sys") (v "0.2.15") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (k 0)))) (h "12y736633qf0hsl7szy92jrivcb8sbiqa9a6c58a1zkc23za20mz") (f (quote (("native-cpu") ("debug") ("cache-friendly") ("android-shared-stl") ("android-lld") ("1mib"))))))

(define-public crate-snmalloc-sys-0.2.16 (c (n "snmalloc-sys") (v "0.2.16") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (k 0)))) (h "06wzcisyh165ym3z6aisb99d7s3ndl1dhdrv2268wc5xp6jy5n40") (f (quote (("native-cpu") ("debug") ("cache-friendly") ("android-shared-stl") ("android-lld") ("1mib"))))))

(define-public crate-snmalloc-sys-0.2.17 (c (n "snmalloc-sys") (v "0.2.17") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (k 0)))) (h "1bkxnysjwpf4xrjcvx94x355j7lz9yydxnmmvyj8sy9dqr642px8") (f (quote (("stats") ("qemu") ("native-cpu") ("default" "1mib") ("debug") ("cache-friendly") ("android-shared-stl") ("android-lld") ("1mib") ("16mib"))))))

(define-public crate-snmalloc-sys-0.2.18 (c (n "snmalloc-sys") (v "0.2.18") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (k 0)))) (h "0n8zqmjzl2zwfl1w6ykhj2dcbbpgvmgv1142cq2y8jrrqzl8yzws") (f (quote (("stats") ("qemu") ("native-cpu") ("default" "1mib") ("debug") ("cache-friendly") ("android-shared-stl") ("android-lld") ("1mib") ("16mib"))))))

(define-public crate-snmalloc-sys-0.2.19 (c (n "snmalloc-sys") (v "0.2.19") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (k 0)))) (h "1k6dn919kpxxbryf0ay9s5qx1nk28gh8a568qiaf3har6nfrnzji") (f (quote (("stats") ("qemu") ("native-cpu") ("default" "1mib") ("debug") ("cache-friendly") ("android-shared-stl") ("android-lld") ("1mib") ("16mib"))))))

(define-public crate-snmalloc-sys-0.2.20 (c (n "snmalloc-sys") (v "0.2.20") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (k 0)))) (h "1a266r19dzgyssyv7418ich8nk8arp0kkgs5kg4f81d6blswmb84") (f (quote (("stats") ("qemu") ("native-cpu") ("default" "1mib") ("debug") ("cache-friendly") ("android-shared-stl") ("android-lld") ("1mib") ("16mib"))))))

(define-public crate-snmalloc-sys-0.2.21 (c (n "snmalloc-sys") (v "0.2.21") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (k 0)))) (h "1apskgiczr6y07ib6in9p4mbbkpbxshv7cm8p13srmfa9s5pikxd") (f (quote (("stats") ("qemu") ("native-cpu") ("default" "1mib") ("debug") ("cache-friendly") ("android-shared-stl") ("android-lld") ("1mib") ("16mib"))))))

(define-public crate-snmalloc-sys-0.2.22 (c (n "snmalloc-sys") (v "0.2.22") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (k 0)))) (h "1pk48qcrknz5vhwh3w8jnwj6gj96z0pzp2s4frmcx4s464maf24y") (f (quote (("stats") ("qemu") ("native-cpu") ("default" "1mib") ("debug") ("cache-friendly") ("android-shared-stl") ("android-lld") ("1mib") ("16mib"))))))

(define-public crate-snmalloc-sys-0.2.23 (c (n "snmalloc-sys") (v "0.2.23") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (k 0)))) (h "10fb04azk5rgfciy4z5af6c9i8nb2aax91cnh2nlnkp0s5c87kcg") (f (quote (("stats") ("qemu") ("native-cpu") ("default" "1mib") ("debug") ("cache-friendly") ("android-shared-stl") ("android-lld") ("1mib") ("16mib"))))))

(define-public crate-snmalloc-sys-0.2.24 (c (n "snmalloc-sys") (v "0.2.24") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (k 0)))) (h "03wdrx9xhj8f7dipwqycz59f63rix9smmypfcdr1zanp09hrmq4a") (f (quote (("stats") ("qemu") ("native-cpu") ("default" "1mib") ("debug") ("cache-friendly") ("android-shared-stl") ("android-lld") ("1mib") ("16mib"))))))

(define-public crate-snmalloc-sys-0.2.25 (c (n "snmalloc-sys") (v "0.2.25") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (k 0)))) (h "0d9lv3pld1c09ls01ii7y4s08nyhj4v1y010dxdgzbdz8ax9x27z") (f (quote (("stats") ("qemu") ("native-cpu") ("default" "1mib") ("debug") ("cache-friendly") ("android-shared-stl") ("android-lld") ("1mib") ("16mib"))))))

(define-public crate-snmalloc-sys-0.2.26 (c (n "snmalloc-sys") (v "0.2.26") (d (list (d (n "cc") (r "^1.0") (o #t) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (k 0)))) (h "00mrp4sxwl56s1ymvv06zfffx9zag7rpj4vbnjv41c9nrhj97ibk") (f (quote (("stats") ("qemu") ("native-cpu") ("local_dynamic_tls") ("default" "1mib" "build_cmake") ("debug") ("cache-friendly") ("build_cmake" "cmake") ("build_cc" "cc") ("android-shared-stl") ("android-lld") ("1mib") ("16mib"))))))

(define-public crate-snmalloc-sys-0.2.27 (c (n "snmalloc-sys") (v "0.2.27") (d (list (d (n "cc") (r "^1.0") (o #t) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (o #t) (d #t) (k 1)))) (h "1j6bc4lx263hr3f59a5n15a70jb9m84zcs29vyk089xb4lx8264m") (f (quote (("win8compat") ("usecxx20") ("stats") ("qemu") ("native-cpu") ("local_dynamic_tls") ("default" "1mib" "build_cmake") ("debug") ("cache-friendly") ("build_cmake" "cmake") ("build_cc" "cc") ("android-shared-stl") ("android-lld") ("1mib") ("16mib"))))))

(define-public crate-snmalloc-sys-0.2.28 (c (n "snmalloc-sys") (v "0.2.28") (d (list (d (n "cc") (r "^1.0") (o #t) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (o #t) (d #t) (k 1)))) (h "1da0irbxc5rwxm41nh59pa6rr7qagvydxpld0pp6nxgyspkyd9rm") (f (quote (("win8compat") ("usecxx20") ("stats") ("qemu") ("native-cpu") ("local_dynamic_tls") ("default" "1mib" "build_cmake") ("debug") ("cache-friendly") ("build_cmake" "cmake") ("build_cc" "cc") ("android-shared-stl") ("android-lld") ("1mib") ("16mib"))))))

(define-public crate-snmalloc-sys-0.3.0-beta.1+f1be609 (c (n "snmalloc-sys") (v "0.3.0-beta.1+f1be609") (d (list (d (n "cc") (r "^1.0") (o #t) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (o #t) (d #t) (k 1)))) (h "1mvkg2drbd5q4gzl6jsd71fv9dpy9p8vpqwx6ri6xbz954ixv6z1") (f (quote (("win8compat") ("usecxx17") ("stats") ("qemu") ("native-cpu") ("local_dynamic_tls") ("default" "build_cmake") ("debug") ("check") ("build_cmake" "cmake") ("build_cc" "cc") ("android-lld"))))))

(define-public crate-snmalloc-sys-0.3.0 (c (n "snmalloc-sys") (v "0.3.0") (d (list (d (n "cc") (r "^1.0") (o #t) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (o #t) (d #t) (k 1)))) (h "1ri1vj65yzsyqmx1nz0j8l2x85146g19gbhipzxbncxbc5v5f014") (f (quote (("win8compat") ("usecxx17") ("qemu") ("native-cpu") ("local_dynamic_tls") ("default" "build_cmake") ("debug") ("check") ("build_cmake" "cmake") ("build_cc" "cc") ("android-lld"))))))

(define-public crate-snmalloc-sys-0.3.1 (c (n "snmalloc-sys") (v "0.3.1") (d (list (d (n "cc") (r "^1.0") (o #t) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (o #t) (d #t) (k 1)))) (h "05v91lbwc6lhi47x4gzzmqsy7q6f1vvncmcc0dkja04rqcq9inf0") (f (quote (("win8compat") ("usecxx17") ("qemu") ("native-cpu") ("local_dynamic_tls") ("default" "build_cmake") ("debug") ("check") ("build_cmake" "cmake") ("build_cc" "cc") ("android-lld"))))))

(define-public crate-snmalloc-sys-0.3.2 (c (n "snmalloc-sys") (v "0.3.2") (d (list (d (n "cc") (r "^1.0") (o #t) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (o #t) (d #t) (k 1)))) (h "0g48i2hb39wfl6y7mr6phncnjch3ph5z0sliwdxppxbdzzllb2bx") (f (quote (("win8compat") ("usecxx17") ("qemu") ("native-cpu") ("local_dynamic_tls") ("default" "build_cmake") ("debug") ("check") ("build_cmake" "cmake") ("build_cc" "cc") ("android-lld"))))))

(define-public crate-snmalloc-sys-0.3.3 (c (n "snmalloc-sys") (v "0.3.3") (d (list (d (n "cc") (r "^1.0") (o #t) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (o #t) (d #t) (k 1)))) (h "1msx49m304rg2v1d7ki8h4i1607833m7gnzxkbs9zamcbngnqxc0") (f (quote (("win8compat") ("usecxx17") ("qemu") ("native-cpu") ("local_dynamic_tls") ("default" "build_cmake") ("debug") ("check") ("build_cmake" "cmake") ("build_cc" "cc") ("android-lld"))))))

(define-public crate-snmalloc-sys-0.3.4 (c (n "snmalloc-sys") (v "0.3.4") (d (list (d (n "cc") (r "^1.0") (o #t) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (o #t) (d #t) (k 1)))) (h "0hzi949c0304n7fphb498c0yqijqz9ilwlynwjpi3grfcrzkzblw") (f (quote (("win8compat") ("usecxx17") ("qemu") ("native-cpu") ("local_dynamic_tls") ("default" "build_cmake") ("debug") ("check") ("build_cmake" "cmake") ("build_cc" "cc") ("android-lld"))))))

(define-public crate-snmalloc-sys-0.3.5 (c (n "snmalloc-sys") (v "0.3.5") (d (list (d (n "cc") (r "^1.0") (o #t) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (o #t) (d #t) (k 1)))) (h "1yp846c4h5z0007y8s4ymzvm86hbx1l5mgf3nsx1caml2cqbhq87") (f (quote (("win8compat") ("usecxx17") ("qemu") ("notls") ("native-cpu") ("lto") ("local_dynamic_tls") ("default" "build_cmake") ("debug") ("check") ("build_cmake" "cmake") ("build_cc" "cc") ("android-lld"))))))

