(define-module (crates-io sn ip sniper_node) #:use-module (crates-io))

(define-public crate-sniper_node-0.0.1 (c (n "sniper_node") (v "0.0.1") (d (list (d (n "random-string") (r "^1.0.0") (d #t) (k 0)) (d (n "relative-path") (r "^1.7.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.151") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.16") (d #t) (k 0)))) (h "0xxmp19ghyjx3mf1lxgdxpwrgwi9wdn2nbcq28dw3qps06k2ql68") (y #t)))

