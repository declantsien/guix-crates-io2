(define-module (crates-io sn ip snip-services) #:use-module (crates-io))

(define-public crate-snip-services-0.1.0 (c (n "snip-services") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "base64") (r "^0.13") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11.5") (d #t) (k 0)) (d (n "wasmbus-rpc") (r "^0.14") (d #t) (k 0)) (d (n "weld-codegen") (r "^0.7") (d #t) (k 1)))) (h "05nsmjqd2ng287njn1p9zh8aas6s7nfcjm4n0p86r7vjp1l3s9z3")))

