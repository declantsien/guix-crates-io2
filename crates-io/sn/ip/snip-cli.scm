(define-module (crates-io sn ip snip-cli) #:use-module (crates-io))

(define-public crate-snip-cli-0.1.1 (c (n "snip-cli") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.11") (f (quote ("cargo" "derive" "string"))) (d #t) (k 0)) (d (n "dirs-next") (r "^2.0.0") (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (f (quote ("clap"))) (d #t) (k 0)) (d (n "prettytable") (r "^0.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "tempfile") (r "^3.8.1") (d #t) (k 0)) (d (n "tokio") (r "^1.35.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1dbibh3ix0hh6hlxjma4zd405fxvz1p2vvw9rhcczqdwh5wgg2a1")))

