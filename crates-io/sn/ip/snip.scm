(define-module (crates-io sn ip snip) #:use-module (crates-io))

(define-public crate-snip-0.1.0 (c (n "snip") (v "0.1.0") (d (list (d (n "cli-clipboard") (r "^0.2") (d #t) (k 0)) (d (n "dirs") (r "^3.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "14wpaa1lg3ysczsrr8b26pg1vy9v2gifz2dq25k56jl9zzr7wrpk")))

(define-public crate-snip-0.1.1 (c (n "snip") (v "0.1.1") (d (list (d (n "cli-clipboard") (r "^0.2") (d #t) (k 0)) (d (n "dirs") (r "^3.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1ymv8ya79m2l9g85z4yqmfhf6mhy2nj2v4226q3aq62zs73i07j8")))

(define-public crate-snip-0.1.2 (c (n "snip") (v "0.1.2") (d (list (d (n "cli-clipboard") (r "^0.2") (d #t) (k 0)) (d (n "dirs") (r "^3.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "191prwb8dn3v8krjmpnj17b6djnq6yfkwfg56dqi6y2714aad5xa")))

(define-public crate-snip-0.2.0 (c (n "snip") (v "0.2.0") (d (list (d (n "cli-clipboard") (r "^0.2") (d #t) (k 0)) (d (n "dirs") (r "^3.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)) (d (n "webbrowser") (r "^0.5.5") (d #t) (k 0)))) (h "11ivczzc3xkgarxplaab0lsrsivddixvrbgsfh7kh784qnj937yi")))

(define-public crate-snip-0.2.1 (c (n "snip") (v "0.2.1") (d (list (d (n "cli-clipboard") (r "^0.2") (d #t) (k 0)) (d (n "dirs") (r "^3.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)) (d (n "webbrowser") (r "^0.5.5") (d #t) (k 0)))) (h "01gwn0smjn6b7y8q5xk39ks3apm3icx6sv05ap8x1rhsb3lrdcfb")))

(define-public crate-snip-0.3.0 (c (n "snip") (v "0.3.0") (d (list (d (n "cli-clipboard") (r "^0.2") (d #t) (k 0)) (d (n "dirs") (r "^3.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)) (d (n "webbrowser") (r "^0.5.5") (d #t) (k 0)) (d (n "zip") (r "^0.5") (d #t) (k 0)))) (h "03zsrlxsv7d0a2qrc04lw86mc6jrh0fd8swgbzlza21rswas0bm7")))

(define-public crate-snip-0.3.1 (c (n "snip") (v "0.3.1") (d (list (d (n "cli-clipboard") (r "^0.2") (d #t) (k 0)) (d (n "dirs") (r "^3.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)) (d (n "webbrowser") (r "^0.5.5") (d #t) (k 0)) (d (n "zip") (r "^0.5") (d #t) (k 0)))) (h "11akhnxf2y2pfb3ln9svjlyyngzr0d2i6gsfm43gwzyp2bd1nkxc")))

(define-public crate-snip-0.3.2 (c (n "snip") (v "0.3.2") (d (list (d (n "cli-clipboard") (r "^0.2") (d #t) (k 0)) (d (n "dirs") (r "^3.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)) (d (n "webbrowser") (r "^0.5.5") (d #t) (k 0)) (d (n "zip") (r "^0.5") (d #t) (k 0)))) (h "18dfxn7vplr8nwm7rmrb4gh769zyaslf1psj4qv79j4jc95hmlcq")))

