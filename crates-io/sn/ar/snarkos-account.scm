(define-module (crates-io sn ar snarkos-account) #:use-module (crates-io))

(define-public crate-snarkos-account-2.1.0 (c (n "snarkos-account") (v "2.1.0") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (k 0)) (d (n "snarkvm") (r "=0.12.1") (f (quote ("circuit" "console" "rocks" "console"))) (d #t) (k 0)))) (h "121fwnmxy8vmkxmrynw786pjxrd4gp5z8divsv6nyqdmggasf31b")))

(define-public crate-snarkos-account-2.1.1 (c (n "snarkos-account") (v "2.1.1") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (k 0)) (d (n "snarkvm") (r "=0.12.2") (f (quote ("circuit" "console" "rocks" "console"))) (d #t) (k 0)))) (h "0brslzl5jsgi5nqlh0rj9gsfj6smr63135d3azwxkzmdvd5nk7xz")))

(define-public crate-snarkos-account-2.1.2 (c (n "snarkos-account") (v "2.1.2") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (k 0)) (d (n "snarkvm") (r "=0.12.4") (f (quote ("circuit" "console" "rocks" "console"))) (d #t) (k 0)))) (h "1b4kz2vix7kwmgh1j9rnqqbc2w8g7l9i0m3axz9wshd0g96j7a8h")))

(define-public crate-snarkos-account-2.1.3 (c (n "snarkos-account") (v "2.1.3") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (k 0)) (d (n "snarkvm") (r "=0.12.5") (f (quote ("circuit" "console" "rocks" "console"))) (d #t) (k 0)))) (h "1l2p4n55ig0bjp205zj3077pbyjs9fzjn744m10irjzayln9z1d1")))

(define-public crate-snarkos-account-2.1.4 (c (n "snarkos-account") (v "2.1.4") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (k 0)) (d (n "snarkvm") (r "=0.12.6") (f (quote ("circuit" "console" "rocks" "console"))) (d #t) (k 0)))) (h "0gcz45kg56hvzixi4ii7dwkg4nv0s4h47l4lj97pk4gqgl6x7pnl")))

(define-public crate-snarkos-account-2.1.5 (c (n "snarkos-account") (v "2.1.5") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (k 0)) (d (n "snarkvm") (r "=0.14.5") (f (quote ("circuit" "console" "rocks" "console"))) (d #t) (k 0)))) (h "0imgl50n299sqpwp6qnvm9449g534j381pnl905sfzsr4nyy5k2q")))

(define-public crate-snarkos-account-2.1.6 (c (n "snarkos-account") (v "2.1.6") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (k 0)) (d (n "snarkvm") (r "=0.14.6") (f (quote ("circuit" "console" "rocks" "console"))) (d #t) (k 0)))) (h "03v0qj3p24lz2b2i3cdzd2dbws2w79v1csissa3xqam11hl3dh1j")))

(define-public crate-snarkos-account-2.1.7 (c (n "snarkos-account") (v "2.1.7") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (k 0)) (d (n "snarkvm") (r "=0.14.6") (f (quote ("circuit" "console" "rocks" "console"))) (d #t) (k 0)))) (h "0iwyirmja57r9p3nj0im0ybhanvg0fw4f20ir7fvgj5k6v78hpil")))

(define-public crate-snarkos-account-2.2.0 (c (n "snarkos-account") (v "2.2.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (k 0)) (d (n "snarkvm") (r "=0.16.1") (f (quote ("circuit" "console" "rocks" "console"))) (d #t) (k 0)))) (h "0c5s6qg7bfdrz9sbpvqk43nmyxd3widl743r3z7n5jfhqngi2bab")))

(define-public crate-snarkos-account-2.2.1 (c (n "snarkos-account") (v "2.2.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (k 0)) (d (n "snarkvm") (r "=0.16.1") (f (quote ("circuit" "console" "rocks" "console"))) (d #t) (k 0)))) (h "0hgvb7hd517d7xcjxyh840npd634zv3va32cc8faqldfwcdpk5sr")))

(define-public crate-snarkos-account-2.2.2 (c (n "snarkos-account") (v "2.2.2") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (k 0)) (d (n "snarkvm") (r "=0.16.6") (f (quote ("circuit" "console" "rocks" "console"))) (d #t) (k 0)))) (h "1f7spxf21lgsdydah9kzfd5111ici0i1nf2mn9a8fxxv37cl5gdf")))

(define-public crate-snarkos-account-2.2.3 (c (n "snarkos-account") (v "2.2.3") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (k 0)) (d (n "snarkvm") (r "=0.16.7") (f (quote ("circuit" "console" "rocks" "console"))) (d #t) (k 0)))) (h "1qi1bs2gd9j9ryl4gz9rc1v08r93z3idm0f211294sdgjzyyhs53")))

(define-public crate-snarkos-account-2.2.4 (c (n "snarkos-account") (v "2.2.4") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (k 0)) (d (n "snarkvm") (r "=0.16.8") (f (quote ("circuit" "console" "rocks" "console"))) (d #t) (k 0)))) (h "1s5gqhdnp9c5cw745i98dcj7q6mk40zlxy4xzwhnanah6zrbdl4k")))

(define-public crate-snarkos-account-2.2.5 (c (n "snarkos-account") (v "2.2.5") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (k 0)) (d (n "snarkvm") (r "=0.16.11") (f (quote ("circuit" "console" "rocks" "console"))) (d #t) (k 0)))) (h "0p5lar2nnk8gg0vll2vviypiq6x75g9g03bfskkbldl2dcvajmyg")))

(define-public crate-snarkos-account-2.2.6 (c (n "snarkos-account") (v "2.2.6") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (k 0)) (d (n "snarkvm") (r "=0.16.13") (f (quote ("circuit" "console" "rocks" "console"))) (d #t) (k 0)))) (h "1j5ispvswwdba3qnig1mnxgsl44hllf85j5kmv047zdkvabrb6lx")))

(define-public crate-snarkos-account-2.2.7 (c (n "snarkos-account") (v "2.2.7") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (k 0)) (d (n "snarkvm") (r "=0.16.15") (f (quote ("circuit" "console" "rocks" "console"))) (d #t) (k 0)))) (h "0l82xw7hnq1agf1x764saip6asgcmqyrmmx1w3iwy23is58p61g0")))

