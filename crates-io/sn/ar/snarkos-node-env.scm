(define-module (crates-io sn ar snarkos-node-env) #:use-module (crates-io))

(define-public crate-snarkos-node-env-2.1.0 (c (n "snarkos-node-env") (v "2.1.0") (d (list (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0zdkzvrhhrn4fs1p6w0smmgi2jpvyh4jndlkcbziv6jzmd2864n0")))

(define-public crate-snarkos-node-env-2.1.1 (c (n "snarkos-node-env") (v "2.1.1") (d (list (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "07cns1jlhs2h1g3kx5rmjczzyjarfjf34ij591jwnhlchb0f27dl")))

(define-public crate-snarkos-node-env-2.1.2 (c (n "snarkos-node-env") (v "2.1.2") (d (list (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1gzvbygz8zn5bcyddvf8mf3g82ijb0rba965jmh434mf4w3cba30")))

(define-public crate-snarkos-node-env-2.1.3 (c (n "snarkos-node-env") (v "2.1.3") (d (list (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0nlhyibqvlq4k758ha2gcj6n3f0b80wb0rkybpdmwfdbvgzvsib6")))

(define-public crate-snarkos-node-env-2.1.4 (c (n "snarkos-node-env") (v "2.1.4") (d (list (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1bwckp6n6x4na91cykz53s80xvf3ay46nam0gn6ch0vcnjgii731")))

(define-public crate-snarkos-node-env-2.1.5 (c (n "snarkos-node-env") (v "2.1.5") (d (list (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0lmg364a80r1a4j4pmyx6lsc5s9h9556703h4zka6cpnzv55j0jx")))

(define-public crate-snarkos-node-env-2.1.6 (c (n "snarkos-node-env") (v "2.1.6") (d (list (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1a6fsan7ggrdyr5b03vbhgazd5s5vyhkg414ml6mg8x44c7ni42w")))

(define-public crate-snarkos-node-env-2.1.7 (c (n "snarkos-node-env") (v "2.1.7") (d (list (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0hn7w1m6f5wdbfhp08s7zzl3cm3c0msp9rab05ckl2g7b31rxay2")))

