(define-module (crates-io sn ar snarkos-derives) #:use-module (crates-io))

(define-public crate-snarkos-derives-1.0.0 (c (n "snarkos-derives") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1b08d2balsr2j039k5m2zmr1357aw1h9mqdlldszc6ps91ddprlw") (y #t)))

(define-public crate-snarkos-derives-1.1.0 (c (n "snarkos-derives") (v "1.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1sskvbhc5fmc6jzyccsnmb4i2hl4w79cjkmpdwds4kjszqc08ks1") (y #t)))

(define-public crate-snarkos-derives-1.1.1 (c (n "snarkos-derives") (v "1.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "04f1im8m1kdb4xa003r5x3kdi16i2rlasfh7x06mwzv2ypxkzcb4") (y #t)))

(define-public crate-snarkos-derives-1.1.2 (c (n "snarkos-derives") (v "1.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "03g8r7gw51hs8c6dy2xq66av4bivqq28rmwvm6ljhhp0pvcp9p0m") (y #t)))

(define-public crate-snarkos-derives-1.1.3 (c (n "snarkos-derives") (v "1.1.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1zg4vpw8lhl8bq1vwfdbzvkfcvg4wf34v2l417x2qs8sah82s91r") (y #t)))

(define-public crate-snarkos-derives-1.1.4 (c (n "snarkos-derives") (v "1.1.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "038j2f6dnp9yki49hybi49g11jimg5s3hs8w73ji6g7pz4swxxm8") (y #t)))

