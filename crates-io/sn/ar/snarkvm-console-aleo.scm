(define-module (crates-io sn ar snarkvm-console-aleo) #:use-module (crates-io))

(define-public crate-snarkvm-console-aleo-99.99.99 (c (n "snarkvm-console-aleo") (v "99.99.99") (h "1wbwxiclak79kmw4a0j1hgpvqnk990g66f7i947xcdcdjxcljh1y")))

(define-public crate-snarkvm-console-aleo-99.99.999 (c (n "snarkvm-console-aleo") (v "99.99.999") (h "0513zwgpb8ni1rx9vkg01fs8f0gwxhyfyayivycmkkvvjgdy3393")))

