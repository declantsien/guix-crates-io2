(define-module (crates-io sn ar snarkvm-metrics) #:use-module (crates-io))

(define-public crate-snarkvm-metrics-0.16.12 (c (n "snarkvm-metrics") (v "0.16.12") (d (list (d (n "metrics") (r "^0.21") (d #t) (k 0)) (d (n "metrics-exporter-prometheus") (r "^0.12") (d #t) (k 0)))) (h "0sf3mcfm0d3gvs97nza7d9gxm7kmbzwggy5jr6s1bf62yfl9gacn")))

(define-public crate-snarkvm-metrics-0.16.13 (c (n "snarkvm-metrics") (v "0.16.13") (d (list (d (n "metrics") (r "^0.21") (d #t) (k 0)) (d (n "metrics-exporter-prometheus") (r "^0.12") (d #t) (k 0)))) (h "0wrv38hxw8y88m3x31vyz22x81923zc8p0lj3vbkw41yqg8xxhq2")))

(define-public crate-snarkvm-metrics-0.16.14 (c (n "snarkvm-metrics") (v "0.16.14") (d (list (d (n "metrics") (r "^0.21") (d #t) (k 0)) (d (n "metrics-exporter-prometheus") (r "^0.12") (d #t) (k 0)))) (h "1rfn1ifgawjwh5bj63lpps5fdk5bpfp5137vw363m3jilyy4jp3p")))

(define-public crate-snarkvm-metrics-0.16.15 (c (n "snarkvm-metrics") (v "0.16.15") (d (list (d (n "metrics") (r "^0.21") (d #t) (k 0)) (d (n "metrics-exporter-prometheus") (r "^0.12") (d #t) (k 0)))) (h "17kixd5cw4ria87ay4a5mg74vj3ikwlafpkgnzhd6g5mr8d93758")))

(define-public crate-snarkvm-metrics-0.16.16 (c (n "snarkvm-metrics") (v "0.16.16") (d (list (d (n "metrics") (r "^0.21") (d #t) (k 0)) (d (n "metrics-exporter-prometheus") (r "^0.12") (d #t) (k 0)))) (h "0fw9h0qcxwmn5ifjb3bb8amrnlhcr7cwp6ihkh30qqfkpdz6hv1j")))

(define-public crate-snarkvm-metrics-0.16.17 (c (n "snarkvm-metrics") (v "0.16.17") (d (list (d (n "metrics") (r "^0.22") (d #t) (k 0)) (d (n "metrics-exporter-prometheus") (r "^0.13") (d #t) (k 0)))) (h "17ga30y2sakbns1wadd119k4v2w39jhv4vd3ajdb1wmbb2xq8l8i")))

(define-public crate-snarkvm-metrics-0.16.18 (c (n "snarkvm-metrics") (v "0.16.18") (d (list (d (n "metrics") (r "^0.22") (d #t) (k 0)) (d (n "metrics-exporter-prometheus") (r "^0.13") (d #t) (k 0)))) (h "1bhmhxndp0y4s4yh0f8lg70fvxiy3j73qk8g5vsy49xran40izi3")))

(define-public crate-snarkvm-metrics-0.16.19 (c (n "snarkvm-metrics") (v "0.16.19") (d (list (d (n "metrics") (r "^0.22") (d #t) (k 0)) (d (n "metrics-exporter-prometheus") (r "^0.13") (d #t) (k 0)))) (h "0v6miyr0mdafgd5440609cd1ad9zqd9j0j12c9b6qkfxb4h0ch7f")))

