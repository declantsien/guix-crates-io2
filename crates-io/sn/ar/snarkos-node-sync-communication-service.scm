(define-module (crates-io sn ar snarkos-node-sync-communication-service) #:use-module (crates-io))

(define-public crate-snarkos-node-sync-communication-service-2.2.0 (c (n "snarkos-node-sync-communication-service") (v "2.2.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^1.28") (f (quote ("sync"))) (d #t) (k 0)))) (h "1h0q724sid033m6wbs66l62ykgg1g2nazpdkpcn0f16phj3wz3xp")))

(define-public crate-snarkos-node-sync-communication-service-2.2.1 (c (n "snarkos-node-sync-communication-service") (v "2.2.1") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^1.28") (f (quote ("sync"))) (d #t) (k 0)))) (h "1dmhrk0g0df5fklhywcaqck1ihbdkwjnybf98waywvph8ljvlkl8")))

(define-public crate-snarkos-node-sync-communication-service-2.2.2 (c (n "snarkos-node-sync-communication-service") (v "2.2.2") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^1.28") (f (quote ("sync"))) (d #t) (k 0)))) (h "0a4409v4gd9l0l8wnddiahimlk0kv65338fw2aici1nj63lxw1ip")))

(define-public crate-snarkos-node-sync-communication-service-2.2.3 (c (n "snarkos-node-sync-communication-service") (v "2.2.3") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^1.28") (f (quote ("sync"))) (d #t) (k 0)))) (h "0qva8vbzhfpskb7zrxdijx65q3831nbi3aw2hw0p9xqbi9sw069w")))

(define-public crate-snarkos-node-sync-communication-service-2.2.4 (c (n "snarkos-node-sync-communication-service") (v "2.2.4") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^1.28") (f (quote ("sync"))) (d #t) (k 0)))) (h "13qanr9zxgwb4zxzqvdx7bq3z714rzalqqifyqg8fncs3wnq28jn")))

(define-public crate-snarkos-node-sync-communication-service-2.2.5 (c (n "snarkos-node-sync-communication-service") (v "2.2.5") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^1.28") (f (quote ("sync"))) (d #t) (k 0)))) (h "0k5gb00h5phy3ajkw8cfni8x821gvp068as8x6g98q4kjryy68nw")))

(define-public crate-snarkos-node-sync-communication-service-2.2.6 (c (n "snarkos-node-sync-communication-service") (v "2.2.6") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^1.28") (f (quote ("sync"))) (d #t) (k 0)))) (h "1qlgb5czywsrxyaqq3mb1mfyviwxnc97fq4hkwyzy92cc5msk763")))

(define-public crate-snarkos-node-sync-communication-service-2.2.7 (c (n "snarkos-node-sync-communication-service") (v "2.2.7") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^1.28") (f (quote ("sync"))) (d #t) (k 0)))) (h "0maw59b7rb92dp161pnnaxpwwksq2pdi3c308v1hyiwiw5ycdfbg")))

