(define-module (crates-io sn ar snark-tool) #:use-module (crates-io))

(define-public crate-snark-tool-0.4.0 (c (n "snark-tool") (v "0.4.0") (d (list (d (n "bit-vec") (r "^0.6") (d #t) (k 0)) (d (n "cadical") (r "^0.1.13") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13") (d #t) (k 0)) (d (n "petgraph") (r "^0.5.1") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "structopt") (r "^0.2.10") (d #t) (k 0)) (d (n "varisat") (r "^0.2.2") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.4") (d #t) (k 0)))) (h "1x9mdpl2sfmd5glpvbd2d5llg583fisqq6nm4nm0dhbiawkmbsgb")))

