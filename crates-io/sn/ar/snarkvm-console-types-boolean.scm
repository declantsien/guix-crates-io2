(define-module (crates-io sn ar snarkvm-console-types-boolean) #:use-module (crates-io))

(define-public crate-snarkvm-console-types-boolean-0.9.0 (c (n "snarkvm-console-types-boolean") (v "0.9.0") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "snarkvm-console-network-environment") (r "^0.9.0") (d #t) (k 0)))) (h "0b4rmgzzjlz6hwhfhw9pb837b8x4ljpf1mcylqh5yqiri1nywhcw")))

(define-public crate-snarkvm-console-types-boolean-0.9.1 (c (n "snarkvm-console-types-boolean") (v "0.9.1") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "snarkvm-console-network-environment") (r "^0.9.1") (d #t) (k 0)))) (h "0vqqpai2r2m14aymchmx3l6hdqmqzzp7zaldvmjaw1w2vb5qfrj8")))

(define-public crate-snarkvm-console-types-boolean-0.9.2 (c (n "snarkvm-console-types-boolean") (v "0.9.2") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "snarkvm-console-network-environment") (r "^0.9.2") (d #t) (k 0)))) (h "0vds2fa437mw7lhd98zvz02zdgq99p8gdv30n5zw2dwn314s65xa")))

(define-public crate-snarkvm-console-types-boolean-0.9.3 (c (n "snarkvm-console-types-boolean") (v "0.9.3") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "snarkvm-console-network-environment") (r "^0.9.3") (d #t) (k 0)))) (h "1p607ha3b7yv09fav3amry9qx9v0qyybfxbj0rmw198jhxzdnwdi")))

(define-public crate-snarkvm-console-types-boolean-0.9.4 (c (n "snarkvm-console-types-boolean") (v "0.9.4") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "snarkvm-console-network-environment") (r "^0.9.4") (d #t) (k 0)))) (h "01gs7c40zy5ab8jldpc60076hr3qwdb7hhvsfnlargi5512qn8wk")))

(define-public crate-snarkvm-console-types-boolean-0.9.5 (c (n "snarkvm-console-types-boolean") (v "0.9.5") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "snarkvm-console-network-environment") (r "^0.9.5") (d #t) (k 0)))) (h "1b411p447qkvy5nnc5x1ay4xqay0f6vaz5clhgm4q5cwvgxqhyr3")))

(define-public crate-snarkvm-console-types-boolean-0.9.6 (c (n "snarkvm-console-types-boolean") (v "0.9.6") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "snarkvm-console-network-environment") (r "^0.9.6") (d #t) (k 0)))) (h "0k35idxfag90fp193fr78kph0spxxv8mvagg8qn0n41w9f2nwzcm")))

(define-public crate-snarkvm-console-types-boolean-0.9.7 (c (n "snarkvm-console-types-boolean") (v "0.9.7") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "snarkvm-console-network-environment") (r "^0.9.7") (d #t) (k 0)))) (h "11p5rk1sryp0r8ix5dyrfqyz0p1wf6il3gkasxk8adj4h2zgag0w")))

(define-public crate-snarkvm-console-types-boolean-0.9.8 (c (n "snarkvm-console-types-boolean") (v "0.9.8") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "snarkvm-console-network-environment") (r "^0.9.8") (d #t) (k 0)))) (h "185wsdnzy7an2m7b75z9yk854kz19hc1rzv5jr70zns57pni8qip")))

(define-public crate-snarkvm-console-types-boolean-0.9.9 (c (n "snarkvm-console-types-boolean") (v "0.9.9") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "snarkvm-console-network-environment") (r "^0.9.9") (d #t) (k 0)))) (h "0wsy383x2iz8hybr51c4zqm1h63igscxcg9dpxby9jaaj7i5d1mk")))

(define-public crate-snarkvm-console-types-boolean-0.9.10 (c (n "snarkvm-console-types-boolean") (v "0.9.10") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "snarkvm-console-network-environment") (r "^0.9.10") (d #t) (k 0)))) (h "1vvhisnpd30ax9i4pxmkil0shlnvr5jkq4sr70jmjn5idqplsqml")))

(define-public crate-snarkvm-console-types-boolean-0.9.11 (c (n "snarkvm-console-types-boolean") (v "0.9.11") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "snarkvm-console-network-environment") (r "^0.9.11") (d #t) (k 0)))) (h "1ldk0n26fgaxkg7rzi3s6ngbrcavbpxg3jpkpmicw49qvp04f1mw")))

(define-public crate-snarkvm-console-types-boolean-0.9.12 (c (n "snarkvm-console-types-boolean") (v "0.9.12") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (f (quote ("preserve_order"))) (d #t) (k 2)) (d (n "snarkvm-console-network-environment") (r "^0.9.12") (d #t) (k 0)))) (h "1m43xs8w6nc17fn5sk46knapba1jls6kfm1gipdpqzrgxi5sk2sn")))

(define-public crate-snarkvm-console-types-boolean-0.9.13 (c (n "snarkvm-console-types-boolean") (v "0.9.13") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (f (quote ("preserve_order"))) (d #t) (k 2)) (d (n "snarkvm-console-network-environment") (r "^0.9.13") (d #t) (k 0)))) (h "1xqk46ai89nbscp21jfsd5vyayl0i3qsz77kbnpg5wvi7vgi595y")))

(define-public crate-snarkvm-console-types-boolean-0.9.14 (c (n "snarkvm-console-types-boolean") (v "0.9.14") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (f (quote ("preserve_order"))) (d #t) (k 2)) (d (n "snarkvm-console-network-environment") (r "^0.9.14") (d #t) (k 0)))) (h "1q96ia97ck068zgs627zpx2qm5isf2kcfk2p555lflhwxcip0fyk")))

(define-public crate-snarkvm-console-types-boolean-0.9.15 (c (n "snarkvm-console-types-boolean") (v "0.9.15") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (f (quote ("preserve_order"))) (d #t) (k 2)) (d (n "snarkvm-console-network-environment") (r "^0.9.15") (d #t) (k 0)))) (h "1gghm6zgs0r2bc58dkcn9gi2aikd88v6sbx1889wxsfcn5axcc6s")))

(define-public crate-snarkvm-console-types-boolean-0.9.16 (c (n "snarkvm-console-types-boolean") (v "0.9.16") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (f (quote ("preserve_order"))) (d #t) (k 2)) (d (n "snarkvm-console-network-environment") (r "^0.9.16") (d #t) (k 0)))) (h "11b56ikqm438ankncrima8xqfgxnr1vlxpxp8xd2lswzrgsj47zk")))

(define-public crate-snarkvm-console-types-boolean-0.10.0 (c (n "snarkvm-console-types-boolean") (v "0.10.0") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (f (quote ("preserve_order"))) (d #t) (k 2)) (d (n "snarkvm-console-network-environment") (r "^0.10.0") (d #t) (k 0)))) (h "00yjibcdw01lcml1km116f5kbi6j5i79d44n4hd362f1y4dg99h1")))

(define-public crate-snarkvm-console-types-boolean-0.10.1 (c (n "snarkvm-console-types-boolean") (v "0.10.1") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (f (quote ("preserve_order"))) (d #t) (k 2)) (d (n "snarkvm-console-network-environment") (r "^0.10.1") (d #t) (k 0)))) (h "03vgjq2b87nycng0g697b2xpcz787sf2ksvyjhf3msbyia17pbc1")))

(define-public crate-snarkvm-console-types-boolean-0.10.2 (c (n "snarkvm-console-types-boolean") (v "0.10.2") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (f (quote ("preserve_order"))) (d #t) (k 2)) (d (n "snarkvm-console-network-environment") (r "^0.10.2") (d #t) (k 0)))) (h "0z4b4wsmicx84mh4c9y65mag9mgpwk731hpcaiv75zyb8am66jxj")))

(define-public crate-snarkvm-console-types-boolean-0.10.3 (c (n "snarkvm-console-types-boolean") (v "0.10.3") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (f (quote ("preserve_order"))) (d #t) (k 2)) (d (n "snarkvm-console-network-environment") (r "^0.10.3") (d #t) (k 0)))) (h "07qmap5wrlf6zyfdk6yknqpv1n78mpcbjjb3k86azblynx3hfr43")))

(define-public crate-snarkvm-console-types-boolean-0.11.0 (c (n "snarkvm-console-types-boolean") (v "0.11.0") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (f (quote ("preserve_order"))) (d #t) (k 2)) (d (n "snarkvm-console-network-environment") (r "^0.11.0") (d #t) (k 0)))) (h "1rr0hc5djnmy5pwapylz5a6nhxbna698djzfd57yx5d1xa5yf3ia")))

(define-public crate-snarkvm-console-types-boolean-0.11.1 (c (n "snarkvm-console-types-boolean") (v "0.11.1") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (f (quote ("preserve_order"))) (d #t) (k 2)) (d (n "snarkvm-console-network-environment") (r "^0.11.1") (d #t) (k 0)))) (h "073fas890vkqrm9zvdmcv89dkl39b26ykrmf0s2qizcdqnvxvb6s")))

(define-public crate-snarkvm-console-types-boolean-0.11.2 (c (n "snarkvm-console-types-boolean") (v "0.11.2") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (f (quote ("preserve_order"))) (d #t) (k 2)) (d (n "snarkvm-console-network-environment") (r "^0.11.2") (d #t) (k 0)))) (h "05dl7zlsijdab5k5mnykmh8s4ykr1zj77zi8wwx1r24iyr22mqi3")))

(define-public crate-snarkvm-console-types-boolean-0.11.3 (c (n "snarkvm-console-types-boolean") (v "0.11.3") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (f (quote ("preserve_order"))) (d #t) (k 2)) (d (n "snarkvm-console-network-environment") (r "=0.11.3") (d #t) (k 0)))) (h "1jqhkv2yr9pn8k3m2jh0w2kcaf7jdgzag26pj2sq2q6qac75l92i")))

(define-public crate-snarkvm-console-types-boolean-0.11.4 (c (n "snarkvm-console-types-boolean") (v "0.11.4") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (f (quote ("preserve_order"))) (d #t) (k 2)) (d (n "snarkvm-console-network-environment") (r "=0.11.4") (d #t) (k 0)))) (h "0l49iw84vipn39y0z6sil7p26g2ann7kjn0xzr4qpx3wr2g1xayv")))

(define-public crate-snarkvm-console-types-boolean-0.11.5 (c (n "snarkvm-console-types-boolean") (v "0.11.5") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (f (quote ("preserve_order"))) (d #t) (k 2)) (d (n "snarkvm-console-network-environment") (r "=0.11.5") (d #t) (k 0)))) (h "0q8p24rfrx3mk286fbcd7ikijqhkkn77rs8rcclzh40kr4adzykb")))

(define-public crate-snarkvm-console-types-boolean-0.11.6 (c (n "snarkvm-console-types-boolean") (v "0.11.6") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (f (quote ("preserve_order"))) (d #t) (k 2)) (d (n "snarkvm-console-network-environment") (r "=0.11.6") (d #t) (k 0)))) (h "1lb1x8wy0yqchj3vc0sk8jrsfs8ssaw5ym0snly5bv7fmsg61r11")))

(define-public crate-snarkvm-console-types-boolean-0.11.7 (c (n "snarkvm-console-types-boolean") (v "0.11.7") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (f (quote ("preserve_order"))) (d #t) (k 2)) (d (n "snarkvm-console-network-environment") (r "=0.11.7") (d #t) (k 0)))) (h "0i8bn049icafnd7x9dfyf8mrd193al4zdmanryzqrnr15ivk8dmi")))

(define-public crate-snarkvm-console-types-boolean-0.12.0 (c (n "snarkvm-console-types-boolean") (v "0.12.0") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (f (quote ("preserve_order"))) (d #t) (k 2)) (d (n "snarkvm-console-network-environment") (r "=0.12.0") (d #t) (k 0)))) (h "0y340k3scg8627qwwzggkcklzkqvdws4dqb2bi8b04gdnfqymn1k")))

(define-public crate-snarkvm-console-types-boolean-0.12.1 (c (n "snarkvm-console-types-boolean") (v "0.12.1") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (f (quote ("preserve_order"))) (d #t) (k 2)) (d (n "snarkvm-console-network-environment") (r "=0.12.1") (d #t) (k 0)))) (h "08wi5zm1c5xdszr21c8ac5zq590snds5349hsj574ca2djrb0vid")))

(define-public crate-snarkvm-console-types-boolean-0.12.2 (c (n "snarkvm-console-types-boolean") (v "0.12.2") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (f (quote ("preserve_order"))) (d #t) (k 2)) (d (n "snarkvm-console-network-environment") (r "=0.12.2") (d #t) (k 0)))) (h "0hsmvzgywrblbwwycgsavyp4p2v77fn90kxdrb02a3ly7hxb3bxd")))

(define-public crate-snarkvm-console-types-boolean-0.12.3 (c (n "snarkvm-console-types-boolean") (v "0.12.3") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (f (quote ("preserve_order"))) (d #t) (k 2)) (d (n "snarkvm-console-network-environment") (r "=0.12.3") (d #t) (k 0)))) (h "11i9cgwlnfv9bdlxisqpl6rnl85sq0h8dph6abdm0mdazg0pqn35")))

(define-public crate-snarkvm-console-types-boolean-0.12.4 (c (n "snarkvm-console-types-boolean") (v "0.12.4") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (f (quote ("preserve_order"))) (d #t) (k 2)) (d (n "snarkvm-console-network-environment") (r "=0.12.4") (d #t) (k 0)))) (h "095c9ibh0xclylvk291s7jahq8b2hj65vsxhnn124hkq6qjqrgmr")))

(define-public crate-snarkvm-console-types-boolean-0.12.5 (c (n "snarkvm-console-types-boolean") (v "0.12.5") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (f (quote ("preserve_order"))) (d #t) (k 2)) (d (n "snarkvm-console-network-environment") (r "=0.12.5") (d #t) (k 0)))) (h "07wr6aqqkjzvcvbsggln5mw038siw7dh7kcbpdl3j8bj8id2qsbs")))

(define-public crate-snarkvm-console-types-boolean-0.12.6 (c (n "snarkvm-console-types-boolean") (v "0.12.6") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (f (quote ("preserve_order"))) (d #t) (k 2)) (d (n "snarkvm-console-network-environment") (r "=0.12.6") (d #t) (k 0)))) (h "0qxqg4phlx4ja557c6vwf1vgfmiqwx8a07nc104az3qpianpdfya")))

(define-public crate-snarkvm-console-types-boolean-0.13.0 (c (n "snarkvm-console-types-boolean") (v "0.13.0") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (f (quote ("preserve_order"))) (d #t) (k 2)) (d (n "snarkvm-console-network-environment") (r "=0.13.0") (d #t) (k 0)))) (h "0f761lsp99zdw4ybvvzw77s29sjfx612m0yw9xrjhypxchg0kx6z")))

(define-public crate-snarkvm-console-types-boolean-0.14.0 (c (n "snarkvm-console-types-boolean") (v "0.14.0") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (f (quote ("preserve_order"))) (d #t) (k 2)) (d (n "snarkvm-console-network-environment") (r "=0.14.0") (d #t) (k 0)))) (h "07r1bjr9d8ii2fg7b50d7991v42i8br83ml2p0h0d7cyi9pwdwd7")))

(define-public crate-snarkvm-console-types-boolean-0.14.1 (c (n "snarkvm-console-types-boolean") (v "0.14.1") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (f (quote ("preserve_order"))) (d #t) (k 2)) (d (n "snarkvm-console-network-environment") (r "=0.14.1") (d #t) (k 0)))) (h "1n71bn07fs5713gyqw754sz37n8cz3sb4q1qz25vv5igwrvcxaa2")))

(define-public crate-snarkvm-console-types-boolean-0.14.2 (c (n "snarkvm-console-types-boolean") (v "0.14.2") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (f (quote ("preserve_order"))) (d #t) (k 2)) (d (n "snarkvm-console-network-environment") (r "=0.14.2") (d #t) (k 0)))) (h "0fybz0an7c4x40x2amd9f05ssigwxq05d522w93lsrl3iwx5yyba")))

(define-public crate-snarkvm-console-types-boolean-0.14.3 (c (n "snarkvm-console-types-boolean") (v "0.14.3") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (f (quote ("preserve_order"))) (d #t) (k 2)) (d (n "snarkvm-console-network-environment") (r "=0.14.3") (d #t) (k 0)))) (h "07zlzdkwrk9fdygwzpq1rr4b1jf6xpmrafdvqb884yyk1fgljcq4")))

(define-public crate-snarkvm-console-types-boolean-0.14.4 (c (n "snarkvm-console-types-boolean") (v "0.14.4") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (f (quote ("preserve_order"))) (d #t) (k 2)) (d (n "snarkvm-console-network-environment") (r "=0.14.4") (d #t) (k 0)))) (h "0l25kr1y6sh5fr9nbzwcmjsas8zzdvqn4h0n4s55kxych3dlzzsh")))

(define-public crate-snarkvm-console-types-boolean-0.14.5 (c (n "snarkvm-console-types-boolean") (v "0.14.5") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (f (quote ("preserve_order"))) (d #t) (k 2)) (d (n "snarkvm-console-network-environment") (r "=0.14.5") (d #t) (k 0)))) (h "0qlswh875s8ipm0wnq14nrknx2lxxnjza6ja2wfh7wawv180x7ac")))

(define-public crate-snarkvm-console-types-boolean-0.14.6 (c (n "snarkvm-console-types-boolean") (v "0.14.6") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (f (quote ("preserve_order"))) (d #t) (k 2)) (d (n "snarkvm-console-network-environment") (r "=0.14.6") (d #t) (k 0)))) (h "0i41gqp5s9893rgb8hm8gx3czyhgfs69hgc7vark8rl33xwxrcap")))

(define-public crate-snarkvm-console-types-boolean-0.15.0 (c (n "snarkvm-console-types-boolean") (v "0.15.0") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (f (quote ("preserve_order"))) (d #t) (k 2)) (d (n "snarkvm-console-network-environment") (r "=0.15.0") (d #t) (k 0)))) (h "14xz37qv4lqmnfpsmhp4a4295wal7mqdz7mbf6ac437xd633l9mg")))

(define-public crate-snarkvm-console-types-boolean-0.15.1 (c (n "snarkvm-console-types-boolean") (v "0.15.1") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (f (quote ("preserve_order"))) (d #t) (k 2)) (d (n "snarkvm-console-network-environment") (r "=0.15.1") (d #t) (k 0)))) (h "158v41gvm69g1qnq77yf5ia4zhj4wz00mwp0x6178xa4cvh8p380")))

(define-public crate-snarkvm-console-types-boolean-0.15.2 (c (n "snarkvm-console-types-boolean") (v "0.15.2") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (f (quote ("preserve_order"))) (d #t) (k 2)) (d (n "snarkvm-console-network-environment") (r "=0.15.2") (d #t) (k 0)))) (h "10c0vlyb19jf565x1k6gr9rcvyp806z9i0mibympan2nvnffl3lb")))

(define-public crate-snarkvm-console-types-boolean-0.15.3 (c (n "snarkvm-console-types-boolean") (v "0.15.3") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (f (quote ("preserve_order"))) (d #t) (k 2)) (d (n "snarkvm-console-network-environment") (r "=0.15.3") (d #t) (k 0)))) (h "001vi6wljai89zna76p2v2qwhcimw6k1v1xmdd0kix0w3c0gmhwv")))

(define-public crate-snarkvm-console-types-boolean-0.15.4 (c (n "snarkvm-console-types-boolean") (v "0.15.4") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (f (quote ("preserve_order"))) (d #t) (k 2)) (d (n "snarkvm-console-network-environment") (r "=0.15.4") (d #t) (k 0)))) (h "0q3ficjq99bykfdmb611imsi10gavh9jrgxhiwwyn9dm7fyg2i1g")))

(define-public crate-snarkvm-console-types-boolean-0.16.0 (c (n "snarkvm-console-types-boolean") (v "0.16.0") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (f (quote ("preserve_order"))) (d #t) (k 2)) (d (n "snarkvm-console-network-environment") (r "=0.16.0") (d #t) (k 0)))) (h "0vdiqfdxi5gfw2swvbkjrk48hj4w3nyhngnmbb09wnih5qjf2zzw")))

(define-public crate-snarkvm-console-types-boolean-0.16.1 (c (n "snarkvm-console-types-boolean") (v "0.16.1") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (f (quote ("preserve_order"))) (d #t) (k 2)) (d (n "snarkvm-console-network-environment") (r "=0.16.1") (d #t) (k 0)))) (h "1swph2kz2g23mncyniha8vbdpx2gj80ld72jc52whjxvv0d8bhm0")))

(define-public crate-snarkvm-console-types-boolean-0.16.2 (c (n "snarkvm-console-types-boolean") (v "0.16.2") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (f (quote ("preserve_order"))) (d #t) (k 2)) (d (n "snarkvm-console-network-environment") (r "=0.16.2") (d #t) (k 0)))) (h "01l2774pdbzyazb1grkcl2jx1zx5c1gf1nr70x10l4lz1610jffh")))

(define-public crate-snarkvm-console-types-boolean-0.16.3 (c (n "snarkvm-console-types-boolean") (v "0.16.3") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (f (quote ("preserve_order"))) (d #t) (k 2)) (d (n "snarkvm-console-network-environment") (r "=0.16.3") (d #t) (k 0)))) (h "0rsaiflzxjh86rg1f0g4p4rds1yh9d42nsd6qgm6vgl6fl7ikyym")))

(define-public crate-snarkvm-console-types-boolean-0.16.4 (c (n "snarkvm-console-types-boolean") (v "0.16.4") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (f (quote ("preserve_order"))) (d #t) (k 2)) (d (n "snarkvm-console-network-environment") (r "=0.16.4") (d #t) (k 0)))) (h "07sz0xns4b11c07s7iwxr30466fjm7bf7xlg2k7fxc81vgiisvfh")))

(define-public crate-snarkvm-console-types-boolean-0.16.5 (c (n "snarkvm-console-types-boolean") (v "0.16.5") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (f (quote ("preserve_order"))) (d #t) (k 2)) (d (n "snarkvm-console-network-environment") (r "=0.16.5") (d #t) (k 0)))) (h "01hyxfd1wvbi87lpw6cp9prkfiifyhy9b0g1yrdc1xbccx50wiah")))

(define-public crate-snarkvm-console-types-boolean-0.16.6 (c (n "snarkvm-console-types-boolean") (v "0.16.6") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (f (quote ("preserve_order"))) (d #t) (k 2)) (d (n "snarkvm-console-network-environment") (r "=0.16.6") (d #t) (k 0)))) (h "0d4vgq8k1m6z2dzsiij692gq1pm5r4rgssz2xdyd4mc6r75h5s0r")))

(define-public crate-snarkvm-console-types-boolean-0.16.7 (c (n "snarkvm-console-types-boolean") (v "0.16.7") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (f (quote ("preserve_order"))) (d #t) (k 2)) (d (n "snarkvm-console-network-environment") (r "=0.16.7") (d #t) (k 0)))) (h "172n3cyiz8n169sdcy42bnka21ci0yj4i4d75fwh65zb8m0r0189")))

(define-public crate-snarkvm-console-types-boolean-0.16.8 (c (n "snarkvm-console-types-boolean") (v "0.16.8") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (f (quote ("preserve_order"))) (d #t) (k 2)) (d (n "snarkvm-console-network-environment") (r "=0.16.8") (d #t) (k 0)))) (h "1sd21drwwryamqnfgx5dz1801k1c15jrhjxanqbab6c77cjdcrsr")))

(define-public crate-snarkvm-console-types-boolean-0.16.9 (c (n "snarkvm-console-types-boolean") (v "0.16.9") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (f (quote ("preserve_order"))) (d #t) (k 2)) (d (n "snarkvm-console-network-environment") (r "=0.16.9") (d #t) (k 0)))) (h "1lxqr2wyig03vv86p9fb2x7hr498kjc8y47h0hc4z61v57y7r0j3")))

(define-public crate-snarkvm-console-types-boolean-0.16.10 (c (n "snarkvm-console-types-boolean") (v "0.16.10") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (f (quote ("preserve_order"))) (d #t) (k 2)) (d (n "snarkvm-console-network-environment") (r "=0.16.10") (d #t) (k 0)))) (h "0gj5iasbgb1fgyyvm0117w6nnlbgv51k2g0idclhfxdkywsq4kmr")))

(define-public crate-snarkvm-console-types-boolean-0.16.11 (c (n "snarkvm-console-types-boolean") (v "0.16.11") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (f (quote ("preserve_order"))) (d #t) (k 2)) (d (n "snarkvm-console-network-environment") (r "=0.16.11") (d #t) (k 0)))) (h "17abq2b9nz9nfc57xa5z4xfvisks60160425kdc13dk1sdqxxwa6")))

(define-public crate-snarkvm-console-types-boolean-0.16.12 (c (n "snarkvm-console-types-boolean") (v "0.16.12") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (f (quote ("preserve_order"))) (d #t) (k 2)) (d (n "snarkvm-console-network-environment") (r "=0.16.12") (d #t) (k 0)))) (h "0grk5inq4i9pbjb3yk4zjzaq1mlxkazamz0h0fg6cclnpcnwwmr9")))

(define-public crate-snarkvm-console-types-boolean-0.16.13 (c (n "snarkvm-console-types-boolean") (v "0.16.13") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (f (quote ("preserve_order"))) (d #t) (k 2)) (d (n "snarkvm-console-network-environment") (r "=0.16.13") (d #t) (k 0)))) (h "1lmcx58yanw0x08j4g2ywbk1wixkvk0rdmw5c0wfgzmdvzpl0jmp")))

(define-public crate-snarkvm-console-types-boolean-0.16.14 (c (n "snarkvm-console-types-boolean") (v "0.16.14") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (f (quote ("preserve_order"))) (d #t) (k 2)) (d (n "snarkvm-console-network-environment") (r "=0.16.14") (d #t) (k 0)))) (h "0ngifi5r96ih9arf1gkwmasij9pa3vgm6b63562i8kjkpvnr0zxp")))

(define-public crate-snarkvm-console-types-boolean-0.16.15 (c (n "snarkvm-console-types-boolean") (v "0.16.15") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (f (quote ("preserve_order"))) (d #t) (k 2)) (d (n "snarkvm-console-network-environment") (r "=0.16.15") (d #t) (k 0)))) (h "080fb4ipxacjpgv4hrhv9w4mlz89zvja2hcjxdr7ikqv1adsbb33")))

(define-public crate-snarkvm-console-types-boolean-0.16.16 (c (n "snarkvm-console-types-boolean") (v "0.16.16") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (f (quote ("preserve_order"))) (d #t) (k 2)) (d (n "snarkvm-console-network-environment") (r "=0.16.16") (d #t) (k 0)))) (h "1cqybabwa5x4c2rdih4mrvs0p7dh194d546s31aks4l9iq6r6syz")))

(define-public crate-snarkvm-console-types-boolean-0.16.17 (c (n "snarkvm-console-types-boolean") (v "0.16.17") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (f (quote ("preserve_order"))) (d #t) (k 2)) (d (n "snarkvm-console-network-environment") (r "=0.16.17") (d #t) (k 0)))) (h "1zyd0gpybk3krc1a2g1lib5rbjlw5k2is88diazbbj7dd0m5m4qf")))

(define-public crate-snarkvm-console-types-boolean-0.16.18 (c (n "snarkvm-console-types-boolean") (v "0.16.18") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (f (quote ("preserve_order"))) (d #t) (k 2)) (d (n "snarkvm-console-network-environment") (r "=0.16.18") (d #t) (k 0)))) (h "1nslmllnryh3qrlww760x65238nwv3mqxxg0iqdpzg6xc3ivdvcs")))

(define-public crate-snarkvm-console-types-boolean-0.16.19 (c (n "snarkvm-console-types-boolean") (v "0.16.19") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (f (quote ("preserve_order"))) (d #t) (k 2)) (d (n "snarkvm-console-network-environment") (r "=0.16.19") (d #t) (k 0)))) (h "1lw16qclgi5wh0nj499k1x4vb40b2p984qjfbflrk2rp9yjm6id1")))

