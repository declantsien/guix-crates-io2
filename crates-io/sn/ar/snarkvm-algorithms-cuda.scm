(define-module (crates-io sn ar snarkvm-algorithms-cuda) #:use-module (crates-io))

(define-public crate-snarkvm-algorithms-cuda-0.9.11 (c (n "snarkvm-algorithms-cuda") (v "0.9.11") (d (list (d (n "blst") (r "^0.3.9") (d #t) (k 0)) (d (n "cc") (r "^1.0.70") (d #t) (k 1)) (d (n "sppark") (r "^0.1.3") (d #t) (k 0)) (d (n "which") (r "^4.0") (d #t) (k 1)))) (h "109fprclnw1lnk8p64136jd7hcqr6fxh8psg7y6w8r651wlvbvwr") (f (quote (("quiet") ("portable" "blst/portable") ("default"))))))

(define-public crate-snarkvm-algorithms-cuda-0.9.12 (c (n "snarkvm-algorithms-cuda") (v "0.9.12") (d (list (d (n "blst") (r "^0.3.9") (d #t) (k 0)) (d (n "cc") (r "^1.0.79") (d #t) (k 1)) (d (n "sppark") (r "^0.1.3") (d #t) (k 0)) (d (n "which") (r "^4.4") (d #t) (k 1)))) (h "0cbcd317fnqvph6i9f1l1jw1xcgn4fi6vyjkr0c47jqw62lj4ahc") (f (quote (("quiet") ("portable" "blst/portable") ("default"))))))

(define-public crate-snarkvm-algorithms-cuda-0.9.13 (c (n "snarkvm-algorithms-cuda") (v "0.9.13") (d (list (d (n "blst") (r "^0.3.9") (d #t) (k 0)) (d (n "cc") (r "^1.0.79") (d #t) (k 1)) (d (n "sppark") (r "^0.1.3") (d #t) (k 0)) (d (n "which") (r "^4.4") (d #t) (k 1)))) (h "0bpkgmx0qmw9ql9rwavaxy2zd9v8g9m36rbk609m25q1gncxh400") (f (quote (("quiet") ("portable" "blst/portable") ("default"))))))

(define-public crate-snarkvm-algorithms-cuda-0.9.14 (c (n "snarkvm-algorithms-cuda") (v "0.9.14") (d (list (d (n "blst") (r "^0.3.9") (d #t) (k 0)) (d (n "cc") (r "^1.0.79") (d #t) (k 1)) (d (n "sppark") (r "^0.1.3") (d #t) (k 0)) (d (n "which") (r "^4.4") (d #t) (k 1)))) (h "1j2mcnw3p27b9xj5sn5l12qrdyc5fvyvs9i01vbwc682ra2bgz4k") (f (quote (("quiet") ("portable" "blst/portable") ("default"))))))

(define-public crate-snarkvm-algorithms-cuda-0.9.15 (c (n "snarkvm-algorithms-cuda") (v "0.9.15") (d (list (d (n "blst") (r "^0.3.9") (d #t) (k 0)) (d (n "cc") (r "^1.0.79") (d #t) (k 1)) (d (n "sppark") (r "^0.1.3") (d #t) (k 0)) (d (n "which") (r "^4.4") (d #t) (k 1)))) (h "1mqsljxm9vpl0n1hyzb65cx690447wdg5hiqmlwyg1xqrp8kc3dd") (f (quote (("quiet") ("portable" "blst/portable") ("default"))))))

(define-public crate-snarkvm-algorithms-cuda-0.9.16 (c (n "snarkvm-algorithms-cuda") (v "0.9.16") (d (list (d (n "blst") (r "^0.3.9") (d #t) (k 0)) (d (n "cc") (r "^1.0.79") (d #t) (k 1)) (d (n "sppark") (r "^0.1.3") (d #t) (k 0)) (d (n "which") (r "^4.4") (d #t) (k 1)))) (h "09bdhia87l23jjvczkljzwj25755s9yys6srqnb20gwrp74vyb3z") (f (quote (("quiet") ("portable" "blst/portable") ("default"))))))

(define-public crate-snarkvm-algorithms-cuda-0.10.0 (c (n "snarkvm-algorithms-cuda") (v "0.10.0") (d (list (d (n "blst") (r "^0.3.9") (d #t) (k 0)) (d (n "cc") (r "^1.0.79") (d #t) (k 1)) (d (n "sppark") (r "^0.1.3") (d #t) (k 0)) (d (n "which") (r "^4.4") (d #t) (k 1)))) (h "08yb13w3yaxx25977rkz21gphq7xhcmz9i00qdhqdfvifm00mp5d") (f (quote (("quiet") ("portable" "blst/portable") ("default"))))))

(define-public crate-snarkvm-algorithms-cuda-0.10.1 (c (n "snarkvm-algorithms-cuda") (v "0.10.1") (d (list (d (n "blst") (r "^0.3.9") (d #t) (k 0)) (d (n "cc") (r "^1.0.79") (d #t) (k 1)) (d (n "sppark") (r "^0.1.3") (d #t) (k 0)) (d (n "which") (r "^4.4") (d #t) (k 1)))) (h "0zaw5vm0fcvg2j0m33mw3z6p2z2hpk9230njfxpqf41s38fz87dz") (f (quote (("quiet") ("portable" "blst/portable") ("default"))))))

(define-public crate-snarkvm-algorithms-cuda-0.10.2 (c (n "snarkvm-algorithms-cuda") (v "0.10.2") (d (list (d (n "blst") (r "^0.3.9") (d #t) (k 0)) (d (n "cc") (r "^1.0.79") (d #t) (k 1)) (d (n "sppark") (r "^0.1.3") (d #t) (k 0)) (d (n "which") (r "^4.4") (d #t) (k 1)))) (h "0kv0s5yq0i425cwgwx5mcixnksfk6nzb6hgl04xrb624flx9dnb3") (f (quote (("quiet") ("portable" "blst/portable") ("default"))))))

(define-public crate-snarkvm-algorithms-cuda-0.10.3 (c (n "snarkvm-algorithms-cuda") (v "0.10.3") (d (list (d (n "blst") (r "^0.3.9") (d #t) (k 0)) (d (n "cc") (r "^1.0.79") (d #t) (k 1)) (d (n "sppark") (r "^0.1.3") (d #t) (k 0)) (d (n "which") (r "^4.4") (d #t) (k 1)))) (h "0f3n9jzmdd2i3kif90akawlbxdq9d8pa3vpp6ch1m1knk0cnl94q") (f (quote (("quiet") ("portable" "blst/portable") ("default"))))))

(define-public crate-snarkvm-algorithms-cuda-0.11.0 (c (n "snarkvm-algorithms-cuda") (v "0.11.0") (d (list (d (n "blst") (r "^0.3.9") (d #t) (k 0)) (d (n "cc") (r "^1.0.79") (d #t) (k 1)) (d (n "sppark") (r "^0.1.3") (d #t) (k 0)) (d (n "which") (r "^4.4") (d #t) (k 1)))) (h "1g8827d34by32w0vllrl7dpcz8hbxwwdk8620f433fnmr1rmkyp7") (f (quote (("quiet") ("portable" "blst/portable") ("default"))))))

(define-public crate-snarkvm-algorithms-cuda-0.11.1 (c (n "snarkvm-algorithms-cuda") (v "0.11.1") (d (list (d (n "blst") (r "^0.3.9") (d #t) (k 0)) (d (n "cc") (r "^1.0.79") (d #t) (k 1)) (d (n "sppark") (r "^0.1.3") (d #t) (k 0)) (d (n "which") (r "^4.4") (d #t) (k 1)))) (h "0h872bgiv10kavjqpsabi3rn78x25kn0ib1m5l1fdgdi119plrn8") (f (quote (("quiet") ("portable" "blst/portable") ("default"))))))

(define-public crate-snarkvm-algorithms-cuda-0.11.2 (c (n "snarkvm-algorithms-cuda") (v "0.11.2") (d (list (d (n "blst") (r "^0.3.9") (d #t) (k 0)) (d (n "cc") (r "^1.0.79") (d #t) (k 1)) (d (n "sppark") (r "^0.1.3") (d #t) (k 0)) (d (n "which") (r "^4.4") (d #t) (k 1)))) (h "1qivl7z6lcyjj5x3v5g8slqzq9g5rvb5s8508asv7bm4jj057bzz") (f (quote (("quiet") ("portable" "blst/portable") ("default"))))))

(define-public crate-snarkvm-algorithms-cuda-0.11.3 (c (n "snarkvm-algorithms-cuda") (v "0.11.3") (d (list (d (n "blst") (r "^0.3.9") (d #t) (k 0)) (d (n "cc") (r "^1.0.79") (d #t) (k 1)) (d (n "sppark") (r "^0.1.3") (d #t) (k 0)) (d (n "which") (r "^4.4") (d #t) (k 1)))) (h "1zdgwc24cmp0iwj94rwkim3jl4gr37sgrrdnx15p1zaxw3j3vmni") (f (quote (("quiet") ("portable" "blst/portable") ("default"))))))

(define-public crate-snarkvm-algorithms-cuda-0.11.4 (c (n "snarkvm-algorithms-cuda") (v "0.11.4") (d (list (d (n "blst") (r "^0.3.9") (d #t) (k 0)) (d (n "cc") (r "^1.0.79") (d #t) (k 1)) (d (n "sppark") (r "^0.1.3") (d #t) (k 0)) (d (n "which") (r "^4.4") (d #t) (k 1)))) (h "1sz1jn169fjrnv06r180kmjkr29dany4pp2iz2jdg568bbkj3p55") (f (quote (("quiet") ("portable" "blst/portable") ("default"))))))

(define-public crate-snarkvm-algorithms-cuda-0.11.5 (c (n "snarkvm-algorithms-cuda") (v "0.11.5") (d (list (d (n "blst") (r "^0.3.9") (d #t) (k 0)) (d (n "cc") (r "^1.0.79") (d #t) (k 1)) (d (n "sppark") (r "^0.1.3") (d #t) (k 0)) (d (n "which") (r "^4.4") (d #t) (k 1)))) (h "1fjgdhsbi2d257z7snx9si16znkjdpghjhb8qlb47kyy3zf14v0h") (f (quote (("quiet") ("portable" "blst/portable") ("default"))))))

(define-public crate-snarkvm-algorithms-cuda-0.11.6 (c (n "snarkvm-algorithms-cuda") (v "0.11.6") (d (list (d (n "blst") (r "^0.3.9") (d #t) (k 0)) (d (n "cc") (r "^1.0.79") (d #t) (k 1)) (d (n "sppark") (r "^0.1.3") (d #t) (k 0)) (d (n "which") (r "^4.4") (d #t) (k 1)))) (h "0syliix2fil6gxcd7bg3138fs686vl4ganwr5hp1ikvzrqdjdvzm") (f (quote (("quiet") ("portable" "blst/portable") ("default"))))))

(define-public crate-snarkvm-algorithms-cuda-0.11.7 (c (n "snarkvm-algorithms-cuda") (v "0.11.7") (d (list (d (n "blst") (r "^0.3.9") (d #t) (k 0)) (d (n "cc") (r "^1.0.79") (d #t) (k 1)) (d (n "sppark") (r "^0.1.3") (d #t) (k 0)) (d (n "which") (r "^4.4") (d #t) (k 1)))) (h "0da15pgzv0630q8hrmwbrnhraswj3z3k5pnhb6gdjmp6sdmmjbp9") (f (quote (("quiet") ("portable" "blst/portable") ("default"))))))

(define-public crate-snarkvm-algorithms-cuda-0.12.0 (c (n "snarkvm-algorithms-cuda") (v "0.12.0") (d (list (d (n "blst") (r "^0.3.9") (d #t) (k 0)) (d (n "cc") (r "^1.0.79") (d #t) (k 1)) (d (n "sppark") (r "^0.1.3") (d #t) (k 0)) (d (n "which") (r "^4.4") (d #t) (k 1)))) (h "0bscgks49pai7bl1ys6q5x4y10c5b4bw7z4jx3h41z73w8xa0c2v") (f (quote (("quiet") ("portable" "blst/portable") ("default"))))))

(define-public crate-snarkvm-algorithms-cuda-0.12.1 (c (n "snarkvm-algorithms-cuda") (v "0.12.1") (d (list (d (n "blst") (r "^0.3.9") (d #t) (k 0)) (d (n "cc") (r "^1.0.79") (d #t) (k 1)) (d (n "sppark") (r "^0.1.3") (d #t) (k 0)) (d (n "which") (r "^4.4") (d #t) (k 1)))) (h "19jf5rwjic2xqxlpfzaw1mp5d3mlm1y41gm03p5yv4x4faa2hr0d") (f (quote (("quiet") ("portable" "blst/portable") ("default"))))))

(define-public crate-snarkvm-algorithms-cuda-0.12.2 (c (n "snarkvm-algorithms-cuda") (v "0.12.2") (d (list (d (n "blst") (r "^0.3.9") (d #t) (k 0)) (d (n "cc") (r "^1.0.79") (d #t) (k 1)) (d (n "sppark") (r "^0.1.3") (d #t) (k 0)) (d (n "which") (r "^4.4") (d #t) (k 1)))) (h "0ki8ibpm0i809sijbzalgjrwy80c7c265j7q50mhyf5rr1c3yayx") (f (quote (("quiet") ("portable" "blst/portable") ("default"))))))

(define-public crate-snarkvm-algorithms-cuda-0.12.3 (c (n "snarkvm-algorithms-cuda") (v "0.12.3") (d (list (d (n "blst") (r "^0.3.9") (d #t) (k 0)) (d (n "cc") (r "^1.0.79") (d #t) (k 1)) (d (n "sppark") (r "^0.1.3") (d #t) (k 0)) (d (n "which") (r "^4.4") (d #t) (k 1)))) (h "0m6wz11px4s94jfmcwhinx5ah27x5crwjbilzqi49vvr37ql2f4x") (f (quote (("quiet") ("portable" "blst/portable") ("default"))))))

(define-public crate-snarkvm-algorithms-cuda-0.12.4 (c (n "snarkvm-algorithms-cuda") (v "0.12.4") (d (list (d (n "blst") (r "^0.3.9") (d #t) (k 0)) (d (n "cc") (r "^1.0.79") (d #t) (k 1)) (d (n "sppark") (r "^0.1.3") (d #t) (k 0)) (d (n "which") (r "^4.4") (d #t) (k 1)))) (h "09iq3a10z71kz6xx3qfw8mw214n1b34av0an6xpg1m2plb6j4aq6") (f (quote (("quiet") ("portable" "blst/portable") ("default"))))))

(define-public crate-snarkvm-algorithms-cuda-0.12.5 (c (n "snarkvm-algorithms-cuda") (v "0.12.5") (d (list (d (n "blst") (r "^0.3.9") (d #t) (k 0)) (d (n "cc") (r "^1.0.79") (d #t) (k 1)) (d (n "sppark") (r "^0.1.3") (d #t) (k 0)) (d (n "which") (r "^4.4") (d #t) (k 1)))) (h "1bx9mplpmsk0mj6zanr80a9k37mrp9im5zj7bmaaap27dm617a57") (f (quote (("quiet") ("portable" "blst/portable") ("default"))))))

(define-public crate-snarkvm-algorithms-cuda-0.12.6 (c (n "snarkvm-algorithms-cuda") (v "0.12.6") (d (list (d (n "blst") (r "^0.3.9") (d #t) (k 0)) (d (n "cc") (r "^1.0.79") (d #t) (k 1)) (d (n "sppark") (r "^0.1.3") (d #t) (k 0)) (d (n "which") (r "^4.4") (d #t) (k 1)))) (h "0zf4smbq0amfd52i9z2rax9fji3qw9qmd0d3kiipz43wi1svi2wi") (f (quote (("quiet") ("portable" "blst/portable") ("default"))))))

(define-public crate-snarkvm-algorithms-cuda-0.13.0 (c (n "snarkvm-algorithms-cuda") (v "0.13.0") (d (list (d (n "blst") (r "^0.3.9") (d #t) (k 0)) (d (n "cc") (r "^1.0.79") (d #t) (k 1)) (d (n "sppark") (r "^0.1.3") (d #t) (k 0)) (d (n "which") (r "^4.4") (d #t) (k 1)))) (h "1znqrxzvg77f9ski5ajhbqrz160i645ajxqy0wsz2fbzn1sxbpsk") (f (quote (("quiet") ("portable" "blst/portable") ("default"))))))

(define-public crate-snarkvm-algorithms-cuda-0.14.0 (c (n "snarkvm-algorithms-cuda") (v "0.14.0") (d (list (d (n "blst") (r "^0.3.9") (d #t) (k 0)) (d (n "cc") (r "^1.0.79") (d #t) (k 1)) (d (n "sppark") (r "^0.1.3") (d #t) (k 0)) (d (n "which") (r "^4.4") (d #t) (k 1)))) (h "13fcal49hsr01sf10ln6afzxmwzj8gqpsnxp9dkxhcyyzrq12irb") (f (quote (("quiet") ("portable" "blst/portable") ("default"))))))

(define-public crate-snarkvm-algorithms-cuda-0.14.1 (c (n "snarkvm-algorithms-cuda") (v "0.14.1") (d (list (d (n "blst") (r "^0.3.9") (d #t) (k 0)) (d (n "cc") (r "^1.0.79") (d #t) (k 1)) (d (n "sppark") (r "^0.1.3") (d #t) (k 0)) (d (n "which") (r "^4.4") (d #t) (k 1)))) (h "1czdwwbww2hpx6xryhczki0f2isrni043i2wgm0fb2rzqdphk1a9") (f (quote (("quiet") ("portable" "blst/portable") ("default"))))))

(define-public crate-snarkvm-algorithms-cuda-0.14.2 (c (n "snarkvm-algorithms-cuda") (v "0.14.2") (d (list (d (n "blst") (r "^0.3.9") (d #t) (k 0)) (d (n "cc") (r "^1.0.79") (d #t) (k 1)) (d (n "sppark") (r "^0.1.3") (d #t) (k 0)) (d (n "which") (r "^4.4") (d #t) (k 1)))) (h "0mrk6b7pm50skg1wfyn5fnkr23382pcbl09dy9afb1ibqvn4sflm") (f (quote (("quiet") ("portable" "blst/portable") ("default"))))))

(define-public crate-snarkvm-algorithms-cuda-0.14.3 (c (n "snarkvm-algorithms-cuda") (v "0.14.3") (d (list (d (n "blst") (r "^0.3.9") (d #t) (k 0)) (d (n "cc") (r "^1.0.79") (d #t) (k 1)) (d (n "sppark") (r "^0.1.3") (d #t) (k 0)) (d (n "which") (r "^4.4") (d #t) (k 1)))) (h "0955dwc7pawhpjn1m3h2gqv176qk8xv27q28h107vwgq846fnnmg") (f (quote (("quiet") ("portable" "blst/portable") ("default"))))))

(define-public crate-snarkvm-algorithms-cuda-0.14.4 (c (n "snarkvm-algorithms-cuda") (v "0.14.4") (d (list (d (n "blst") (r "^0.3.9") (d #t) (k 0)) (d (n "cc") (r "^1.0.79") (d #t) (k 1)) (d (n "sppark") (r "^0.1.3") (d #t) (k 0)) (d (n "which") (r "^4.4") (d #t) (k 1)))) (h "0na6hzg1hidzhf9cnm57d6ln9m4xxnaxzir1j0z57sycbcyzqh6a") (f (quote (("quiet") ("portable" "blst/portable") ("default"))))))

(define-public crate-snarkvm-algorithms-cuda-0.14.5 (c (n "snarkvm-algorithms-cuda") (v "0.14.5") (d (list (d (n "blst") (r "^0.3.9") (d #t) (k 0)) (d (n "cc") (r "^1.0.79") (d #t) (k 1)) (d (n "sppark") (r "^0.1.3") (d #t) (k 0)) (d (n "which") (r "^4.4") (d #t) (k 1)))) (h "0kd0mnvz0mn4iap03chbdrvv7jwci56jdfyibs18lai9wav8d1ha") (f (quote (("quiet") ("portable" "blst/portable") ("default"))))))

(define-public crate-snarkvm-algorithms-cuda-0.14.6 (c (n "snarkvm-algorithms-cuda") (v "0.14.6") (d (list (d (n "blst") (r "^0.3.9") (d #t) (k 0)) (d (n "cc") (r "^1.0.79") (d #t) (k 1)) (d (n "sppark") (r "^0.1.3") (d #t) (k 0)) (d (n "which") (r "^4.4") (d #t) (k 1)))) (h "1hfljbs2p5h4ks55zlpvd4ia25pkbi52f08mrimnqayx6xja1s7j") (f (quote (("quiet") ("portable" "blst/portable") ("default"))))))

(define-public crate-snarkvm-algorithms-cuda-0.15.0 (c (n "snarkvm-algorithms-cuda") (v "0.15.0") (d (list (d (n "blst") (r "^0.3.11") (d #t) (k 0)) (d (n "cc") (r "^1.0.83") (d #t) (k 1)) (d (n "sppark") (r "^0.1.5") (d #t) (k 0)) (d (n "which") (r "^4.4") (d #t) (k 1)))) (h "0xwq413vvlqa40q4wq3qwcp75psj6qw02k7dy6ccphy0hknwq47x") (f (quote (("quiet") ("portable" "blst/portable") ("default"))))))

(define-public crate-snarkvm-algorithms-cuda-0.15.1 (c (n "snarkvm-algorithms-cuda") (v "0.15.1") (d (list (d (n "blst") (r "^0.3.11") (d #t) (k 0)) (d (n "cc") (r "^1.0.83") (d #t) (k 1)) (d (n "sppark") (r "^0.1.5") (d #t) (k 0)) (d (n "which") (r "^4.4") (d #t) (k 1)))) (h "1gl1inzhjjfwdl0cbhbjq7152j7vcpf8hk7kk98vpn86hcmr28m1") (f (quote (("quiet") ("portable" "blst/portable") ("default"))))))

(define-public crate-snarkvm-algorithms-cuda-0.15.2 (c (n "snarkvm-algorithms-cuda") (v "0.15.2") (d (list (d (n "blst") (r "^0.3.11") (d #t) (k 0)) (d (n "cc") (r "^1.0.83") (d #t) (k 1)) (d (n "sppark") (r "^0.1.5") (d #t) (k 0)) (d (n "which") (r "^4.4") (d #t) (k 1)))) (h "0id7rk3nxws4nrql2wxz9yc22j0jvariizzwj26ai8pja9d58pv7") (f (quote (("quiet") ("portable" "blst/portable") ("default"))))))

(define-public crate-snarkvm-algorithms-cuda-0.15.3 (c (n "snarkvm-algorithms-cuda") (v "0.15.3") (d (list (d (n "blst") (r "^0.3.11") (d #t) (k 0)) (d (n "cc") (r "^1.0.83") (d #t) (k 1)) (d (n "sppark") (r "^0.1.5") (d #t) (k 0)) (d (n "which") (r "^4.4") (d #t) (k 1)))) (h "0v7fbh09r8ykawwdg7h96jcbdkn034wp0i0zg3kxbnrh0r7avsll") (f (quote (("quiet") ("portable" "blst/portable") ("default"))))))

(define-public crate-snarkvm-algorithms-cuda-0.15.4 (c (n "snarkvm-algorithms-cuda") (v "0.15.4") (d (list (d (n "blst") (r "^0.3.11") (d #t) (k 0)) (d (n "cc") (r "^1.0.83") (d #t) (k 1)) (d (n "sppark") (r "^0.1.5") (d #t) (k 0)) (d (n "which") (r "^4.4") (d #t) (k 1)))) (h "0w2rhvjgcqbfcjc9q3fhr18azw59kza45gm3gk03aygcv17i82sd") (f (quote (("quiet") ("portable" "blst/portable") ("default"))))))

(define-public crate-snarkvm-algorithms-cuda-0.16.0 (c (n "snarkvm-algorithms-cuda") (v "0.16.0") (d (list (d (n "blst") (r "^0.3.11") (d #t) (k 0)) (d (n "cc") (r "^1.0.83") (d #t) (k 1)) (d (n "sppark") (r "^0.1.5") (d #t) (k 0)) (d (n "which") (r "^4.4") (d #t) (k 1)))) (h "1c2y521kcvd4vlwa1rrhm4j6j0q3ky330xp9bakrhbvp78s9c185") (f (quote (("quiet") ("portable" "blst/portable") ("default"))))))

(define-public crate-snarkvm-algorithms-cuda-0.16.1 (c (n "snarkvm-algorithms-cuda") (v "0.16.1") (d (list (d (n "blst") (r "^0.3.11") (d #t) (k 0)) (d (n "cc") (r "^1.0.83") (d #t) (k 1)) (d (n "sppark") (r "^0.1.5") (d #t) (k 0)) (d (n "which") (r "^4.4") (d #t) (k 1)))) (h "0ypyc9icrp9v2hy625lly2k5qapkva1vfgaw1w0ns12xpcl5bb25") (f (quote (("quiet") ("portable" "blst/portable") ("default"))))))

(define-public crate-snarkvm-algorithms-cuda-0.16.2 (c (n "snarkvm-algorithms-cuda") (v "0.16.2") (d (list (d (n "blst") (r "^0.3.11") (d #t) (k 0)) (d (n "cc") (r "^1.0.83") (d #t) (k 1)) (d (n "sppark") (r "^0.1.5") (d #t) (k 0)) (d (n "which") (r "^4.4") (d #t) (k 1)))) (h "0z705djydjwscfk5hmj585h27z98gw3r9giwgnffqsdjdsv8qfgp") (f (quote (("quiet") ("portable" "blst/portable") ("default"))))))

(define-public crate-snarkvm-algorithms-cuda-0.16.3 (c (n "snarkvm-algorithms-cuda") (v "0.16.3") (d (list (d (n "blst") (r "^0.3.11") (d #t) (k 0)) (d (n "cc") (r "^1.0.83") (d #t) (k 1)) (d (n "sppark") (r "^0.1.5") (d #t) (k 0)) (d (n "which") (r "^4.4") (d #t) (k 1)))) (h "1pzpqj50qbwapr9kk7c8v3g2npqxi4ddmpyg1a25mrpsjjizcfk0") (f (quote (("quiet") ("portable" "blst/portable") ("default"))))))

(define-public crate-snarkvm-algorithms-cuda-0.16.4 (c (n "snarkvm-algorithms-cuda") (v "0.16.4") (d (list (d (n "blst") (r "^0.3.11") (d #t) (k 0)) (d (n "cc") (r "^1.0.83") (d #t) (k 1)) (d (n "sppark") (r "^0.1.5") (d #t) (k 0)) (d (n "which") (r "^4.4") (d #t) (k 1)))) (h "16qnhflwy4l38cgikh444rhxzijg6z7psnkkw5h0v1kwl5p7b89x") (f (quote (("quiet") ("portable" "blst/portable") ("default"))))))

(define-public crate-snarkvm-algorithms-cuda-0.16.5 (c (n "snarkvm-algorithms-cuda") (v "0.16.5") (d (list (d (n "blst") (r "^0.3.11") (d #t) (k 0)) (d (n "cc") (r "^1.0.83") (d #t) (k 1)) (d (n "sppark") (r "^0.1.5") (d #t) (k 0)) (d (n "which") (r "^4.4") (d #t) (k 1)))) (h "0fhqkzm5fa5bh8x74fx7hm8jr3xrn1wbwyha8c926n2jcnng42g5") (f (quote (("quiet") ("portable" "blst/portable") ("default"))))))

(define-public crate-snarkvm-algorithms-cuda-0.16.6 (c (n "snarkvm-algorithms-cuda") (v "0.16.6") (d (list (d (n "blst") (r "^0.3.11") (d #t) (k 0)) (d (n "cc") (r "^1.0.83") (d #t) (k 1)) (d (n "sppark") (r "^0.1.5") (d #t) (k 0)) (d (n "which") (r "^4.4") (d #t) (k 1)))) (h "037ihn3cs8gpj6n45hwimx10v13y32rygdkvm2hkgywz36xgvs4w") (f (quote (("quiet") ("portable" "blst/portable") ("default"))))))

(define-public crate-snarkvm-algorithms-cuda-0.16.7 (c (n "snarkvm-algorithms-cuda") (v "0.16.7") (d (list (d (n "blst") (r "^0.3.11") (d #t) (k 0)) (d (n "cc") (r "^1.0.83") (d #t) (k 1)) (d (n "sppark") (r "^0.1.5") (d #t) (k 0)) (d (n "which") (r "^4.4") (d #t) (k 1)))) (h "10w9rkzyz2c3p5jrahc1kcblyw6wa84b4d7vgvmkajyd0pg153hm") (f (quote (("quiet") ("portable" "blst/portable") ("default"))))))

(define-public crate-snarkvm-algorithms-cuda-0.16.8 (c (n "snarkvm-algorithms-cuda") (v "0.16.8") (d (list (d (n "blst") (r "^0.3.11") (d #t) (k 0)) (d (n "cc") (r "^1.0.83") (d #t) (k 1)) (d (n "sppark") (r "^0.1.5") (d #t) (k 0)) (d (n "which") (r "^4.4") (d #t) (k 1)))) (h "0gxak381n4l14jhj26q7w4zh3msiyk6i7i6szz5q2xlh9p04cwwi") (f (quote (("quiet") ("portable" "blst/portable") ("default"))))))

(define-public crate-snarkvm-algorithms-cuda-0.16.9 (c (n "snarkvm-algorithms-cuda") (v "0.16.9") (d (list (d (n "blst") (r "^0.3.11") (d #t) (k 0)) (d (n "cc") (r "^1.0.83") (d #t) (k 1)) (d (n "sppark") (r "^0.1.5") (d #t) (k 0)) (d (n "which") (r "^4.4") (d #t) (k 1)))) (h "0afw2dvbix2vmvdd9pbxkxp77a4vff7zbhgzpidjwdincq7na388") (f (quote (("quiet") ("portable" "blst/portable") ("default"))))))

(define-public crate-snarkvm-algorithms-cuda-0.16.10 (c (n "snarkvm-algorithms-cuda") (v "0.16.10") (d (list (d (n "blst") (r "^0.3.11") (d #t) (k 0)) (d (n "cc") (r "^1.0.83") (d #t) (k 1)) (d (n "sppark") (r "^0.1.5") (d #t) (k 0)) (d (n "which") (r "^4.4") (d #t) (k 1)))) (h "1yyjdj4k5bwlnndxvw74z59k6ds0ssg4x4w0l188kjv3zrgg4rl9") (f (quote (("quiet") ("portable" "blst/portable") ("default"))))))

(define-public crate-snarkvm-algorithms-cuda-0.16.11 (c (n "snarkvm-algorithms-cuda") (v "0.16.11") (d (list (d (n "blst") (r "^0.3.11") (d #t) (k 0)) (d (n "cc") (r "^1.0.83") (d #t) (k 1)) (d (n "sppark") (r "^0.1.5") (d #t) (k 0)) (d (n "which") (r "^4.4") (d #t) (k 1)))) (h "12pr0872hnlpp7g78av5g16shxqsgjqrlp5b2lvsdmfjf4fvrnly") (f (quote (("quiet") ("portable" "blst/portable") ("default"))))))

(define-public crate-snarkvm-algorithms-cuda-0.16.12 (c (n "snarkvm-algorithms-cuda") (v "0.16.12") (d (list (d (n "blst") (r "^0.3.11") (d #t) (k 0)) (d (n "cc") (r "^1.0.83") (d #t) (k 1)) (d (n "sppark") (r "^0.1.5") (d #t) (k 0)) (d (n "which") (r "^4.4") (d #t) (k 1)))) (h "1bj4i7glvhicj5rlxndkam9b795ynnisgw9c64g0l3rc5x5wh2ww") (f (quote (("quiet") ("portable" "blst/portable") ("default"))))))

(define-public crate-snarkvm-algorithms-cuda-0.16.13 (c (n "snarkvm-algorithms-cuda") (v "0.16.13") (d (list (d (n "blst") (r "^0.3.11") (d #t) (k 0)) (d (n "cc") (r "^1.0.83") (d #t) (k 1)) (d (n "sppark") (r "^0.1.5") (d #t) (k 0)) (d (n "which") (r "^4.4") (d #t) (k 1)))) (h "1kf3vp17knrq61vvix4464zx33alln8jwjhsmqz5fsyd2clsxwra") (f (quote (("quiet") ("portable" "blst/portable") ("default"))))))

(define-public crate-snarkvm-algorithms-cuda-0.16.14 (c (n "snarkvm-algorithms-cuda") (v "0.16.14") (d (list (d (n "blst") (r "^0.3.11") (d #t) (k 0)) (d (n "cc") (r "^1.0.83") (d #t) (k 1)) (d (n "sppark") (r "^0.1.5") (d #t) (k 0)) (d (n "which") (r "^4.4") (d #t) (k 1)))) (h "09cwb903n6dd2i0izvwxv8pyagrjmrpqyzplmjzinbwllizl3qap") (f (quote (("quiet") ("portable" "blst/portable") ("default"))))))

(define-public crate-snarkvm-algorithms-cuda-0.16.15 (c (n "snarkvm-algorithms-cuda") (v "0.16.15") (d (list (d (n "blst") (r "^0.3.11") (d #t) (k 0)) (d (n "cc") (r "^1.0.83") (d #t) (k 1)) (d (n "sppark") (r "^0.1.5") (d #t) (k 0)) (d (n "which") (r "^4.4") (d #t) (k 1)))) (h "12dfx4h56942jdv2xfg15bc8i20w01cnkfjgcmvy2zy1bbv3ps99") (f (quote (("quiet") ("portable" "blst/portable") ("default"))))))

(define-public crate-snarkvm-algorithms-cuda-0.16.16 (c (n "snarkvm-algorithms-cuda") (v "0.16.16") (d (list (d (n "blst") (r "^0.3.11") (d #t) (k 0)) (d (n "cc") (r "^1.0.83") (d #t) (k 1)) (d (n "sppark") (r "^0.1.5") (d #t) (k 0)) (d (n "which") (r "^4.4") (d #t) (k 1)))) (h "0qipmavgnp27mfihzj9k0cbqrvyjfxna4j4f6zs20y1i10h984vm") (f (quote (("quiet") ("portable" "blst/portable") ("default"))))))

(define-public crate-snarkvm-algorithms-cuda-0.16.17 (c (n "snarkvm-algorithms-cuda") (v "0.16.17") (d (list (d (n "blst") (r "^0.3.11") (d #t) (k 0)) (d (n "cc") (r "^1.0.83") (d #t) (k 1)) (d (n "sppark") (r "^0.1.5") (d #t) (k 0)) (d (n "which") (r "^4.4") (d #t) (k 1)))) (h "1ilrcsplbbm3k85ixy114sdg28f42dz1rlx39p305s2lgdrw1hsd") (f (quote (("quiet") ("portable" "blst/portable") ("default"))))))

(define-public crate-snarkvm-algorithms-cuda-0.16.18 (c (n "snarkvm-algorithms-cuda") (v "0.16.18") (d (list (d (n "blst") (r "^0.3.11") (d #t) (k 0)) (d (n "cc") (r "^1.0.83") (d #t) (k 1)) (d (n "sppark") (r "^0.1.5") (d #t) (k 0)) (d (n "which") (r "^4.4") (d #t) (k 1)))) (h "147bjwrci2442a68lv0fr43a1dqkrmj2ds418zs8fgs5g2qd5xc2") (f (quote (("quiet") ("portable" "blst/portable") ("default"))))))

(define-public crate-snarkvm-algorithms-cuda-0.16.19 (c (n "snarkvm-algorithms-cuda") (v "0.16.19") (d (list (d (n "blst") (r "^0.3.11") (d #t) (k 0)) (d (n "cc") (r "^1.0.83") (d #t) (k 1)) (d (n "sppark") (r "^0.1.5") (d #t) (k 0)) (d (n "which") (r "^4.4") (d #t) (k 1)))) (h "0y4fhb4lqwqd3hy2p37y95jfncr5jfgwxhf35bisjlnvpf1shh0h") (f (quote (("quiet") ("portable" "blst/portable") ("default"))))))

