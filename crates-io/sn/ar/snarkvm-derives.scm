(define-module (crates-io sn ar snarkvm-derives) #:use-module (crates-io))

(define-public crate-snarkvm-derives-0.0.2 (c (n "snarkvm-derives") (v "0.0.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "167c5s1g4fppz8m1m9115yb6r2s9sd543yqr70cfzg2b7fmyqkmp")))

(define-public crate-snarkvm-derives-0.0.3 (c (n "snarkvm-derives") (v "0.0.3") (d (list (d (n "proc-macro-crate") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0xynvn81f86m3707h4pmr4cvd98kzsgpw9gq79m3bg21gq2ggjhw")))

(define-public crate-snarkvm-derives-0.0.4 (c (n "snarkvm-derives") (v "0.0.4") (d (list (d (n "proc-macro-crate") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "08rxh83scqk1g6m18g31yhv0flp8027s43gj2zpfa8rxwwk09gwi")))

(define-public crate-snarkvm-derives-0.0.5 (c (n "snarkvm-derives") (v "0.0.5") (d (list (d (n "proc-macro-crate") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1c87zgqmdnv0shnq59v22mpcczqcjk9dc5g1phimx9j31lbw9i94")))

(define-public crate-snarkvm-derives-0.0.6 (c (n "snarkvm-derives") (v "0.0.6") (d (list (d (n "proc-macro-crate") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "01ywxf3rm0n7r7z7hwipff6iqi5k3mdpzkyvk9a70n3l2vw3jcqv")))

(define-public crate-snarkvm-derives-0.2.0 (c (n "snarkvm-derives") (v "0.2.0") (d (list (d (n "proc-macro-crate") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0gq442mh2q0bxdqvq3c8mxyrq546rvbw2bp3pricmkmz4hj5l1gm")))

(define-public crate-snarkvm-derives-0.2.1 (c (n "snarkvm-derives") (v "0.2.1") (d (list (d (n "proc-macro-crate") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0f301yl8ykw2b55da8rxr1hmwa743dlim9y6vd557whzc1ksjava")))

(define-public crate-snarkvm-derives-0.2.2 (c (n "snarkvm-derives") (v "0.2.2") (d (list (d (n "proc-macro-crate") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "09j3s1hq3hkm7c39qv0s2ybxjpbmzs45xifnqk3v88r0dsdq5ja6")))

(define-public crate-snarkvm-derives-0.2.3 (c (n "snarkvm-derives") (v "0.2.3") (d (list (d (n "proc-macro-crate") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "07rxb87686i3zrvsdldksc165c10a4942nyw0pf220fwp7k6y8cn")))

(define-public crate-snarkvm-derives-0.3.1 (c (n "snarkvm-derives") (v "0.3.1") (d (list (d (n "proc-macro-crate") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "10q3214nx93mk59bqhhz0fqjchgk9jn0prz854d570frpj1svcyi")))

(define-public crate-snarkvm-derives-0.3.2 (c (n "snarkvm-derives") (v "0.3.2") (d (list (d (n "proc-macro-crate") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1q9qbfqrc3hzq6j1khvr4nqshxb3bkkcqa48kq96b4p7im46fy4r")))

(define-public crate-snarkvm-derives-0.4.0 (c (n "snarkvm-derives") (v "0.4.0") (d (list (d (n "proc-macro-crate") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0g6qcqvmssjy34hjdih8m8n9kvvcgyq57hmqx3642xbvlp1ak6kn")))

(define-public crate-snarkvm-derives-0.5.0 (c (n "snarkvm-derives") (v "0.5.0") (d (list (d (n "proc-macro-crate") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "18n5k3s81wcvpsvqxb57hxai1asva03hm9ywvn475sg2yw2z7qcf")))

(define-public crate-snarkvm-derives-0.5.1 (c (n "snarkvm-derives") (v "0.5.1") (d (list (d (n "proc-macro-crate") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "13j4vrfsqf3l6clkd08mj30yq5fffil87fdbqyc51pp3yhzfsj2v")))

(define-public crate-snarkvm-derives-0.5.2 (c (n "snarkvm-derives") (v "0.5.2") (d (list (d (n "proc-macro-crate") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "041cpz0hmh63iaql54ihq48lp1p87bwvh5dm3sjscmhzrssnsplh")))

(define-public crate-snarkvm-derives-0.5.3 (c (n "snarkvm-derives") (v "0.5.3") (d (list (d (n "proc-macro-crate") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "06iqs0kais1079dim6jnp6n83qmh7iq30zglkj1ilv8ljqp3zmnm")))

(define-public crate-snarkvm-derives-0.5.4 (c (n "snarkvm-derives") (v "0.5.4") (d (list (d (n "proc-macro-crate") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "04m6z88hqfkj7kdifbm1frywq6dlpqv2h59ri4b1dxh3x0x02z58")))

(define-public crate-snarkvm-derives-0.5.5 (c (n "snarkvm-derives") (v "0.5.5") (d (list (d (n "proc-macro-crate") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0xj9r05b2si42lrxybpnq6789bbl2ivwcpp8rf77kkdwspsq56pv")))

(define-public crate-snarkvm-derives-0.6.0 (c (n "snarkvm-derives") (v "0.6.0") (d (list (d (n "proc-macro-crate") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0rk0wbjklkzaw9wgn11sy2l221kzvsqzlb2cg8k3qcl1g866m225")))

(define-public crate-snarkvm-derives-0.7.0 (c (n "snarkvm-derives") (v "0.7.0") (d (list (d (n "proc-macro-crate") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "04yspj3lcn99xj91vg5zh80c8kb7jzm8m1xng985a3vvj518jiy4")))

(define-public crate-snarkvm-derives-0.7.1 (c (n "snarkvm-derives") (v "0.7.1") (d (list (d (n "proc-macro-crate") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1g4i3ighpn097gwbh4177wqs9fs5rp40i0nq61fhqr9bx9hmag2w")))

(define-public crate-snarkvm-derives-0.7.2 (c (n "snarkvm-derives") (v "0.7.2") (d (list (d (n "proc-macro-crate") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "063pxp7rfjxr042d57h6ir9nkq7pha1sgp2bxjx6knjyvzlkz1pl")))

(define-public crate-snarkvm-derives-0.7.3 (c (n "snarkvm-derives") (v "0.7.3") (d (list (d (n "proc-macro-crate") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0ixa5w9i1688hihiw1kvjf8b3l49hkagksrvk30kp9177ba1hxwn")))

(define-public crate-snarkvm-derives-0.7.4 (c (n "snarkvm-derives") (v "0.7.4") (d (list (d (n "proc-macro-crate") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0xjisqn0f2bkc9v627x248npjc738m1gwvai4z32b37c0pqd5kzz")))

(define-public crate-snarkvm-derives-0.7.5 (c (n "snarkvm-derives") (v "0.7.5") (d (list (d (n "proc-macro-crate") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0acv6vp0m6ypbzvsgzlpvpy0sf1xyap6af6f940i6jd1pmd4sbyl")))

(define-public crate-snarkvm-derives-0.7.6 (c (n "snarkvm-derives") (v "0.7.6") (d (list (d (n "proc-macro-crate") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1iaxh393agd0y1g46n1dad2vzjpv8wyk3q39n3qc7k8id84nkgiw")))

(define-public crate-snarkvm-derives-0.7.7 (c (n "snarkvm-derives") (v "0.7.7") (d (list (d (n "proc-macro-crate") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1w4z2qfmv550jwggqx3yk2gb56nc7z1436nk2a9jfvxhz4s937zc")))

(define-public crate-snarkvm-derives-0.7.8 (c (n "snarkvm-derives") (v "0.7.8") (d (list (d (n "proc-macro-crate") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0n61zxrxcm313dd62mgcc6425yf6f776d4mb36k6c0cz43j3m5hf")))

(define-public crate-snarkvm-derives-0.7.9 (c (n "snarkvm-derives") (v "0.7.9") (d (list (d (n "proc-macro-crate") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0yrcdjql30b99y1lhccxnal72farmvd9mrczqz2xh1a3cf44i19y")))

(define-public crate-snarkvm-derives-0.8.0 (c (n "snarkvm-derives") (v "0.8.0") (d (list (d (n "proc-macro-crate") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1migdwkiwx79cvqdpi0cjcs4i243zk9wbd3cnqbajnbsgll70lxc")))

