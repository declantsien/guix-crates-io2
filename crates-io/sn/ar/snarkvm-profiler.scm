(define-module (crates-io sn ar snarkvm-profiler) #:use-module (crates-io))

(define-public crate-snarkvm-profiler-0.0.2 (c (n "snarkvm-profiler") (v "0.0.2") (d (list (d (n "colored") (r "^2") (o #t) (d #t) (k 0)))) (h "0nkrdal4rcd9858m2ax4g3i3ydq14i0daw30cxpf17h4vpmcb7pd") (f (quote (("print-trace" "colored"))))))

(define-public crate-snarkvm-profiler-0.0.3 (c (n "snarkvm-profiler") (v "0.0.3") (d (list (d (n "colored") (r "^2") (o #t) (d #t) (k 0)))) (h "14avwqh6q06n13i0j5g7y5q33y7ix9hri9n4y27b2rjfy1yw7yfm") (f (quote (("print-trace" "colored"))))))

(define-public crate-snarkvm-profiler-0.0.4 (c (n "snarkvm-profiler") (v "0.0.4") (d (list (d (n "colored") (r "^2") (o #t) (d #t) (k 0)))) (h "18f0hg2gfna497k27aqpy8yy1ifx5y31h4j8549fnd1crhccrldc") (f (quote (("print-trace" "colored"))))))

(define-public crate-snarkvm-profiler-0.0.5 (c (n "snarkvm-profiler") (v "0.0.5") (d (list (d (n "colored") (r "^2") (o #t) (d #t) (k 0)))) (h "0asycrynraixm9hjv833hykxzl6nqwxpkn07hrc575nbcjwykd9z") (f (quote (("print-trace" "colored"))))))

(define-public crate-snarkvm-profiler-0.0.6 (c (n "snarkvm-profiler") (v "0.0.6") (d (list (d (n "colored") (r "^2") (o #t) (d #t) (k 0)))) (h "04l376yiwhj4np722f7lg3aissmvl61hvmwh2bqjd18rsw023kip") (f (quote (("print-trace" "colored"))))))

(define-public crate-snarkvm-profiler-0.2.0 (c (n "snarkvm-profiler") (v "0.2.0") (d (list (d (n "colored") (r "^2") (o #t) (d #t) (k 0)))) (h "0c35cy2sx6c7azqym6gs2r3ky4ql1lkfl4zx0dzyihxzi8zx8byg") (f (quote (("print-trace" "colored"))))))

(define-public crate-snarkvm-profiler-0.2.1 (c (n "snarkvm-profiler") (v "0.2.1") (d (list (d (n "colored") (r "^2") (o #t) (d #t) (k 0)))) (h "1x95m6jk5ni5ldnb0688favs9a84gb8632zhh3rg4cbsydxdad3q") (f (quote (("print-trace" "colored"))))))

(define-public crate-snarkvm-profiler-0.2.2 (c (n "snarkvm-profiler") (v "0.2.2") (d (list (d (n "colored") (r "^2") (o #t) (d #t) (k 0)))) (h "0yrx5avckh1izns0d0l63y0mw46ll61dxr06ldg9zxr53k00limj") (f (quote (("print-trace" "colored"))))))

(define-public crate-snarkvm-profiler-0.2.3 (c (n "snarkvm-profiler") (v "0.2.3") (d (list (d (n "colored") (r "^2") (o #t) (d #t) (k 0)))) (h "0bkkjl2spiy5vrpzs9c8hq9ig8va9dv3zikv0p54hb1sllz6mpya") (f (quote (("print-trace" "colored"))))))

(define-public crate-snarkvm-profiler-0.3.1 (c (n "snarkvm-profiler") (v "0.3.1") (d (list (d (n "colored") (r "^2") (o #t) (d #t) (k 0)))) (h "1n9vvpfxnddbf0vpgx1k18hjcx7cslpxixj34gda8flh2pi7wzw7") (f (quote (("print-trace" "colored"))))))

(define-public crate-snarkvm-profiler-0.3.2 (c (n "snarkvm-profiler") (v "0.3.2") (d (list (d (n "colored") (r "^2") (o #t) (d #t) (k 0)))) (h "0krnfplrcjk41l4j8mjv93i7sqdsdsmx21mcw00zlyl6s951vx7s") (f (quote (("print-trace" "colored"))))))

(define-public crate-snarkvm-profiler-0.4.0 (c (n "snarkvm-profiler") (v "0.4.0") (d (list (d (n "colored") (r "^2") (o #t) (d #t) (k 0)))) (h "010n1p0sxv9nlmiri5abppyf4y183h3jyqqignrsw527g7sfilsg") (f (quote (("print-trace" "colored"))))))

(define-public crate-snarkvm-profiler-0.5.0 (c (n "snarkvm-profiler") (v "0.5.0") (d (list (d (n "colored") (r "^2") (o #t) (d #t) (k 0)))) (h "02fy7bcf5walh7az5amh2irfm3cdc7cwa48rnwiidhs38m5fjgmz") (f (quote (("print-trace" "colored"))))))

(define-public crate-snarkvm-profiler-0.5.1 (c (n "snarkvm-profiler") (v "0.5.1") (d (list (d (n "colored") (r "^2") (o #t) (d #t) (k 0)))) (h "0s3qk51mb8mkaicbpkr8pa1kd54r2k8axlw0mmr8x0m2hg5zjq2j") (f (quote (("print-trace" "colored"))))))

(define-public crate-snarkvm-profiler-0.5.2 (c (n "snarkvm-profiler") (v "0.5.2") (d (list (d (n "colored") (r "^2") (o #t) (d #t) (k 0)))) (h "00i1ksr4hxvgimaqgby2jvcb2kriashqmidzwlmfn5pash0p4b58") (f (quote (("print-trace" "colored"))))))

(define-public crate-snarkvm-profiler-0.5.3 (c (n "snarkvm-profiler") (v "0.5.3") (d (list (d (n "colored") (r "^2") (o #t) (d #t) (k 0)))) (h "0fxi6a8ynmwa6sdzidbx3rzrznadmiwpp7hjafa972ygwmbix3ih") (f (quote (("print-trace" "colored"))))))

(define-public crate-snarkvm-profiler-0.5.4 (c (n "snarkvm-profiler") (v "0.5.4") (d (list (d (n "colored") (r "^2") (o #t) (d #t) (k 0)))) (h "1l4ahvmx7a4bb7vfsmdi467syzn1kchmpnrr3lkl6nm6hf848i6f") (f (quote (("print-trace" "colored"))))))

(define-public crate-snarkvm-profiler-0.5.5 (c (n "snarkvm-profiler") (v "0.5.5") (d (list (d (n "colored") (r "^2") (o #t) (d #t) (k 0)))) (h "07v6379k1hznipp97b954gzj2j4s4jbhscavjjfs2v0mszbl5nsz") (f (quote (("print-trace" "colored"))))))

(define-public crate-snarkvm-profiler-0.6.0 (c (n "snarkvm-profiler") (v "0.6.0") (d (list (d (n "colored") (r "^2") (o #t) (d #t) (k 0)))) (h "04qmxj1ygkm14g2nqijnirb3zzxxi7x6kg43v5n5fr8bcc9jm2d8") (f (quote (("print-trace" "colored"))))))

(define-public crate-snarkvm-profiler-0.7.0 (c (n "snarkvm-profiler") (v "0.7.0") (d (list (d (n "colored") (r "^2") (o #t) (d #t) (k 0)))) (h "0rc055nysiv28sc03ja2qvanf93b2dnqpakr46ljib8pj0ilywvs") (f (quote (("print-trace" "colored"))))))

(define-public crate-snarkvm-profiler-0.7.1 (c (n "snarkvm-profiler") (v "0.7.1") (d (list (d (n "colored") (r "^2") (o #t) (d #t) (k 0)))) (h "0bwfy7ivcv5xhkywsibml1x65pn0qc1cwfjhcf5ksy7pa62sq7wp") (f (quote (("print-trace" "colored"))))))

(define-public crate-snarkvm-profiler-0.7.2 (c (n "snarkvm-profiler") (v "0.7.2") (d (list (d (n "colored") (r "^2") (o #t) (d #t) (k 0)))) (h "1dwxrdl89y9zq1vs6gl3rvkqmmq2vdjmm05dpm4manrix835pxfr") (f (quote (("print-trace" "colored"))))))

(define-public crate-snarkvm-profiler-0.7.3 (c (n "snarkvm-profiler") (v "0.7.3") (d (list (d (n "colored") (r "^2") (o #t) (d #t) (k 0)))) (h "1f2pysrv12nc0m6xn9fqww4vdk1qydrk5hrqlx569ib22bm89r60") (f (quote (("print-trace" "colored"))))))

(define-public crate-snarkvm-profiler-0.7.4 (c (n "snarkvm-profiler") (v "0.7.4") (d (list (d (n "colored") (r "^2") (o #t) (d #t) (k 0)))) (h "051ndnc0kwh7ii9cvkarb7zyrn3glcz5ivl36smc7y1dbxkqyhia") (f (quote (("print-trace" "colored"))))))

(define-public crate-snarkvm-profiler-0.7.5 (c (n "snarkvm-profiler") (v "0.7.5") (d (list (d (n "colored") (r "^2") (o #t) (d #t) (k 0)))) (h "1yj5f8cvxsqc5fm8qbyq4025ll7lngyxxaapp1416sipy13abgrf") (f (quote (("print-trace" "colored"))))))

(define-public crate-snarkvm-profiler-0.7.6 (c (n "snarkvm-profiler") (v "0.7.6") (d (list (d (n "colored") (r "^2") (o #t) (d #t) (k 0)))) (h "189wsinr733z63kv4p48vhpi7igqf6pvpg91ax31a461ymkhq1hs") (f (quote (("print-trace" "colored"))))))

(define-public crate-snarkvm-profiler-0.7.7 (c (n "snarkvm-profiler") (v "0.7.7") (d (list (d (n "colored") (r "^2") (o #t) (d #t) (k 0)))) (h "0qqcn5g19162yxkvbir3kjs0alfc2wapk24q3nw7x0ni72paqw5w") (f (quote (("print-trace" "colored"))))))

(define-public crate-snarkvm-profiler-0.7.8 (c (n "snarkvm-profiler") (v "0.7.8") (d (list (d (n "colored") (r "^2") (o #t) (d #t) (k 0)))) (h "1d0l0vn17f71xr1pz3ax1028dp3nx3sc1nw48ivpgnsrkkbjijr9") (f (quote (("print-trace" "colored"))))))

(define-public crate-snarkvm-profiler-0.7.9 (c (n "snarkvm-profiler") (v "0.7.9") (d (list (d (n "colored") (r "^2") (o #t) (d #t) (k 0)))) (h "0w9xydzmcyb2c2dcy8li8k3w8qv2gvhsfbsq707rxqdr8g1hrn7p") (f (quote (("print-trace" "colored"))))))

(define-public crate-snarkvm-profiler-0.8.0 (c (n "snarkvm-profiler") (v "0.8.0") (d (list (d (n "colored") (r "^2") (o #t) (d #t) (k 0)))) (h "1hn6g2asfqnn95k8wgpdy3ghmbldwc5vmilzhm1v029v7mdy2rz7") (f (quote (("print-trace" "colored"))))))

