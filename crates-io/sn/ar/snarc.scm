(define-module (crates-io sn ar snarc) #:use-module (crates-io))

(define-public crate-snarc-0.1.0 (c (n "snarc") (v "0.1.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "scopeguard") (r "^1") (d #t) (k 0)) (d (n "static_assertions") (r "^1") (d #t) (k 2)) (d (n "thread_local") (r "^1") (o #t) (d #t) (k 0)))) (h "0z6y348plr8cggxpfnq5c089hc5720xlikmh74r99xnnln3964w6") (f (quote (("thread-local" "thread_local") ("default"))))))

