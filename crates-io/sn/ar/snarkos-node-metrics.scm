(define-module (crates-io sn ar snarkos-node-metrics) #:use-module (crates-io))

(define-public crate-snarkos-node-metrics-2.1.0 (c (n "snarkos-node-metrics") (v "2.1.0") (d (list (d (n "metrics") (r "^0.21") (d #t) (k 0)) (d (n "metrics-exporter-prometheus") (r "^0.12") (d #t) (k 0)) (d (n "tokio") (r "^1.28") (f (quote ("rt"))) (d #t) (k 0)))) (h "0y1bm4qbgs8q5hk8xjsx8i1sfgic1gsxbgnsjlvx8ig72jvibiw4")))

(define-public crate-snarkos-node-metrics-2.1.1 (c (n "snarkos-node-metrics") (v "2.1.1") (d (list (d (n "metrics") (r "^0.21") (d #t) (k 0)) (d (n "metrics-exporter-prometheus") (r "^0.12") (d #t) (k 0)) (d (n "tokio") (r "^1.28") (f (quote ("rt"))) (d #t) (k 0)))) (h "140yq7y7nvs0n2r3ms8f64ssj5cqr4vqpzqi86245sqcrx251ir2")))

(define-public crate-snarkos-node-metrics-2.1.2 (c (n "snarkos-node-metrics") (v "2.1.2") (d (list (d (n "metrics") (r "^0.21") (d #t) (k 0)) (d (n "metrics-exporter-prometheus") (r "^0.12") (d #t) (k 0)) (d (n "tokio") (r "^1.28") (f (quote ("rt"))) (d #t) (k 0)))) (h "0ylfmijgz59gbp6jn72jdgd348xpyl3yqw55j38spw24sav8x5mj")))

(define-public crate-snarkos-node-metrics-2.1.3 (c (n "snarkos-node-metrics") (v "2.1.3") (d (list (d (n "metrics") (r "^0.21") (d #t) (k 0)) (d (n "metrics-exporter-prometheus") (r "^0.12") (d #t) (k 0)) (d (n "tokio") (r "^1.28") (f (quote ("rt"))) (d #t) (k 0)))) (h "01lzjcfgxi2ndrqm588yghyzqzwpmcgpz4kg2ijqgz71lzbqszrb")))

(define-public crate-snarkos-node-metrics-2.1.4 (c (n "snarkos-node-metrics") (v "2.1.4") (d (list (d (n "metrics") (r "^0.21") (d #t) (k 0)) (d (n "metrics-exporter-prometheus") (r "^0.12") (d #t) (k 0)) (d (n "tokio") (r "^1.28") (f (quote ("rt"))) (d #t) (k 0)))) (h "0b6iibphr6ksw4h0csijk5409iff01qm8542mq5ysnjqxv3pcn9i")))

(define-public crate-snarkos-node-metrics-2.1.5 (c (n "snarkos-node-metrics") (v "2.1.5") (d (list (d (n "metrics") (r "^0.21") (d #t) (k 0)) (d (n "metrics-exporter-prometheus") (r "^0.12") (d #t) (k 0)) (d (n "tokio") (r "^1.28") (f (quote ("rt"))) (d #t) (k 0)))) (h "1dz6zmjmasn4v5ilcy6j4k973im67bqh2nwk67sng0a9ns52xg58")))

(define-public crate-snarkos-node-metrics-2.1.6 (c (n "snarkos-node-metrics") (v "2.1.6") (d (list (d (n "metrics") (r "^0.21") (d #t) (k 0)) (d (n "metrics-exporter-prometheus") (r "^0.12") (d #t) (k 0)) (d (n "tokio") (r "^1.28") (f (quote ("rt"))) (d #t) (k 0)))) (h "1gfp9h27cc0snfhhbvbrl0nvxkmfmwkmjd32jarbdqvjyx37sgjn")))

(define-public crate-snarkos-node-metrics-2.1.7 (c (n "snarkos-node-metrics") (v "2.1.7") (d (list (d (n "metrics") (r "^0.21") (d #t) (k 0)) (d (n "metrics-exporter-prometheus") (r "^0.12") (d #t) (k 0)) (d (n "tokio") (r "^1.28") (f (quote ("rt"))) (d #t) (k 0)))) (h "0c05xf44zf8yiajy95g6l56rkxix1qpxk6ani25zzn1k0lp024gv")))

(define-public crate-snarkos-node-metrics-2.2.0 (c (n "snarkos-node-metrics") (v "2.2.0") (d (list (d (n "metrics") (r "^0.21") (d #t) (k 0)) (d (n "metrics-exporter-prometheus") (r "^0.12") (d #t) (k 0)) (d (n "tokio") (r "^1.28") (f (quote ("rt"))) (d #t) (k 0)))) (h "0cg714xdc31qljdmpaivpj5i9v75fza9r94996bb8gf8yga2d8ps")))

(define-public crate-snarkos-node-metrics-2.2.1 (c (n "snarkos-node-metrics") (v "2.2.1") (d (list (d (n "metrics") (r "^0.21") (d #t) (k 0)) (d (n "metrics-exporter-prometheus") (r "^0.12") (d #t) (k 0)) (d (n "tokio") (r "^1.28") (f (quote ("rt"))) (d #t) (k 0)))) (h "0jkjk751wla0g3hmxk1rhxh4lvk77a2vckj5hc4lfsh88h7343v0")))

(define-public crate-snarkos-node-metrics-2.2.2 (c (n "snarkos-node-metrics") (v "2.2.2") (d (list (d (n "metrics") (r "^0.21") (d #t) (k 0)) (d (n "metrics-exporter-prometheus") (r "^0.12") (d #t) (k 0)) (d (n "tokio") (r "^1.28") (f (quote ("rt"))) (d #t) (k 0)))) (h "10ia5k111ar7zj15xcjrcp5irbpcdnhl17g02masr7ckbrvvx3bv")))

(define-public crate-snarkos-node-metrics-2.2.3 (c (n "snarkos-node-metrics") (v "2.2.3") (d (list (d (n "metrics") (r "^0.21") (d #t) (k 0)) (d (n "metrics-exporter-prometheus") (r "^0.12") (d #t) (k 0)) (d (n "tokio") (r "^1.28") (f (quote ("rt"))) (d #t) (k 0)))) (h "1v236m3w9aclf9ppa5zxm1ainsf74wki9q99mzlfjzh14njql22n")))

(define-public crate-snarkos-node-metrics-2.2.4 (c (n "snarkos-node-metrics") (v "2.2.4") (d (list (d (n "metrics") (r "^0.21") (d #t) (k 0)) (d (n "metrics-exporter-prometheus") (r "^0.12") (d #t) (k 0)) (d (n "tokio") (r "^1.28") (f (quote ("rt"))) (d #t) (k 0)))) (h "06srymw3d5fr22mzs6aa9xhbc6bcxf7pw9hjvp8374l7lm806i3s")))

(define-public crate-snarkos-node-metrics-2.2.5 (c (n "snarkos-node-metrics") (v "2.2.5") (d (list (d (n "metrics") (r "^0.21") (d #t) (k 0)) (d (n "metrics-exporter-prometheus") (r "^0.12") (d #t) (k 0)) (d (n "tokio") (r "^1.28") (f (quote ("rt"))) (d #t) (k 0)))) (h "1kns2cigcxdd6hjssl3yfb9n9kcnz2i4vjd91bd5jyd8fs1ixniz")))

(define-public crate-snarkos-node-metrics-2.2.6 (c (n "snarkos-node-metrics") (v "2.2.6") (d (list (d (n "metrics") (r "^0.21") (d #t) (k 0)) (d (n "metrics-exporter-prometheus") (r "^0.12") (d #t) (k 0)) (d (n "tokio") (r "^1.28") (f (quote ("rt"))) (d #t) (k 0)))) (h "101qwyd54qk58jbnrwpkfsrk58cp9pj1clzrvlmv7zkjfvd0nmli")))

(define-public crate-snarkos-node-metrics-2.2.7 (c (n "snarkos-node-metrics") (v "2.2.7") (d (list (d (n "metrics") (r "^0.21") (d #t) (k 0)) (d (n "metrics-exporter-prometheus") (r "^0.12") (d #t) (k 0)) (d (n "snarkvm") (r "=0.16.15") (f (quote ("circuit" "console" "rocks" "metrics"))) (d #t) (k 0)) (d (n "tokio") (r "^1.28") (f (quote ("rt"))) (d #t) (k 0)))) (h "04b1d3a71x3h2gkq25mdqls2xcgmr0mi4ad7jv3ria6yn81gr38z") (f (quote (("metrics" "snarkvm/metrics"))))))

