(define-module (crates-io sn ar snarkos-profiler) #:use-module (crates-io))

(define-public crate-snarkos-profiler-1.0.0 (c (n "snarkos-profiler") (v "1.0.0") (d (list (d (n "colored") (r "^2") (o #t) (d #t) (k 0)))) (h "1dhdm9z8lc2iw4j9w06qap5c4xp1hlgcdxxmsl8mha11h7yzbw3w") (f (quote (("print-trace" "colored"))))))

(define-public crate-snarkos-profiler-1.1.0 (c (n "snarkos-profiler") (v "1.1.0") (d (list (d (n "colored") (r "^2") (o #t) (d #t) (k 0)))) (h "1dsfpwwlshv9ac93x2i22iip4zks8yabcwf3idffck58773173rw") (f (quote (("print-trace" "colored"))))))

(define-public crate-snarkos-profiler-1.1.1 (c (n "snarkos-profiler") (v "1.1.1") (d (list (d (n "colored") (r "^2") (o #t) (d #t) (k 0)))) (h "0iriz83ds7b39rhprj8x17bd174ldssqlhjv1gzmdmi94r9sb20p") (f (quote (("print-trace" "colored"))))))

(define-public crate-snarkos-profiler-1.1.2 (c (n "snarkos-profiler") (v "1.1.2") (d (list (d (n "colored") (r "^2") (o #t) (d #t) (k 0)))) (h "0ms0fzsp1drjqz14pbxpggyn3ns28ixwbrnmdxycnh9abg6fyq2b") (f (quote (("print-trace" "colored"))))))

(define-public crate-snarkos-profiler-1.1.3 (c (n "snarkos-profiler") (v "1.1.3") (d (list (d (n "colored") (r "^2") (o #t) (d #t) (k 0)))) (h "1jp34hjpjyyrmzba5m4h7l9jsddmqik4jx7f2vd1qg1w9krvmmbm") (f (quote (("print-trace" "colored"))))))

(define-public crate-snarkos-profiler-1.1.4 (c (n "snarkos-profiler") (v "1.1.4") (d (list (d (n "colored") (r "^2") (o #t) (d #t) (k 0)))) (h "0aza8a1ar2ds1b11bgh3gv3d3ywiqv7ix5z36bija135f69rqlfh") (f (quote (("print-trace" "colored"))))))

(define-public crate-snarkos-profiler-1.2.0 (c (n "snarkos-profiler") (v "1.2.0") (d (list (d (n "colored") (r "^2") (o #t) (d #t) (k 0)))) (h "0gx0sddihb20m23xk0bpdc7ckq8nf8c1j1sxqch75rhdid5b809h") (f (quote (("print-trace" "colored"))))))

(define-public crate-snarkos-profiler-1.3.0 (c (n "snarkos-profiler") (v "1.3.0") (d (list (d (n "colored") (r "^2") (o #t) (d #t) (k 0)))) (h "1c2hw4xnc3rz9r2d41ap7901qbdad9hxql7szp62axpslvkjzc41") (f (quote (("print-trace" "colored"))))))

(define-public crate-snarkos-profiler-1.3.1 (c (n "snarkos-profiler") (v "1.3.1") (d (list (d (n "colored") (r "^2") (o #t) (d #t) (k 0)))) (h "0w3m2ixx2c3w7z51011xqzjqjxzhslccv7z5ll1ibm1sk22k4cw9") (f (quote (("print-trace" "colored"))))))

(define-public crate-snarkos-profiler-1.3.2 (c (n "snarkos-profiler") (v "1.3.2") (d (list (d (n "colored") (r "^2") (o #t) (d #t) (k 0)))) (h "0cijy8ykkaqgjkpwawk3imcmlp8fq5n2c8alpifli4xsxygmf2lk") (f (quote (("print-trace" "colored"))))))

(define-public crate-snarkos-profiler-1.3.3 (c (n "snarkos-profiler") (v "1.3.3") (d (list (d (n "colored") (r "^2") (o #t) (d #t) (k 0)))) (h "1szcp5l50vfv2xcj52xf43xqbraxcl9iygzk87lhxmdmmv2an1bc") (f (quote (("print-trace" "colored"))))))

(define-public crate-snarkos-profiler-1.3.4 (c (n "snarkos-profiler") (v "1.3.4") (d (list (d (n "colored") (r "^2") (o #t) (d #t) (k 0)))) (h "1dzmjll98q05g8w112k2bd4b6a5dvf5migvaajdzizl2wlilsn9f") (f (quote (("print-trace" "colored"))))))

(define-public crate-snarkos-profiler-1.3.5 (c (n "snarkos-profiler") (v "1.3.5") (d (list (d (n "colored") (r "^2") (o #t) (d #t) (k 0)))) (h "07cjch5agjkl0zsivvj4kgfa8q2a0l7zs6x4qzh0q55nlf690ca3") (f (quote (("print-trace" "colored"))))))

(define-public crate-snarkos-profiler-1.3.6 (c (n "snarkos-profiler") (v "1.3.6") (d (list (d (n "colored") (r "^2") (o #t) (d #t) (k 0)))) (h "0cq5g28vxq9240j06k475xb9pjq8l1dfhqy5xf6h4lmfvsip2yw0") (f (quote (("print-trace" "colored"))))))

(define-public crate-snarkos-profiler-1.3.7 (c (n "snarkos-profiler") (v "1.3.7") (d (list (d (n "colored") (r "^2") (o #t) (d #t) (k 0)))) (h "1szd63y431alp2gk2lgz1kwbbgfd1wqm9qa8sjggck4f6d0x2k3j") (f (quote (("print-trace" "colored"))))))

(define-public crate-snarkos-profiler-1.3.8 (c (n "snarkos-profiler") (v "1.3.8") (d (list (d (n "colored") (r "^2") (o #t) (d #t) (k 0)))) (h "1lfhwifz62haw9lr2g42kr71gh0zwgy1nnk6b4i3dm41wq9bf359") (f (quote (("print-trace" "colored"))))))

(define-public crate-snarkos-profiler-1.3.9 (c (n "snarkos-profiler") (v "1.3.9") (d (list (d (n "colored") (r "^2") (o #t) (d #t) (k 0)))) (h "0k05kpgnyglskpn6j284jwdnh9784ix1fy8s1p1x9x4ncynlar8k") (f (quote (("print-trace" "colored"))))))

(define-public crate-snarkos-profiler-1.3.10 (c (n "snarkos-profiler") (v "1.3.10") (d (list (d (n "colored") (r "^2") (o #t) (d #t) (k 0)))) (h "1r4hfn7cicl22pbmaz041fnhgda9n40wc5k14d0zl95w1xfrwa4v") (f (quote (("print-trace" "colored"))))))

(define-public crate-snarkos-profiler-1.3.11 (c (n "snarkos-profiler") (v "1.3.11") (d (list (d (n "colored") (r "^2") (o #t) (d #t) (k 0)))) (h "12ggcxqrlf9i5h78c9ndf7vg32g2y804b5l50wvzljd0kqixfba4") (f (quote (("print-trace" "colored"))))))

(define-public crate-snarkos-profiler-1.3.12 (c (n "snarkos-profiler") (v "1.3.12") (d (list (d (n "colored") (r "^2") (o #t) (d #t) (k 0)))) (h "0262wx1a0fawaxlvll9amjz7nax044ax462x6kirdfrz5s187xkl") (f (quote (("print-trace" "colored")))) (y #t)))

(define-public crate-snarkos-profiler-1.3.13 (c (n "snarkos-profiler") (v "1.3.13") (d (list (d (n "colored") (r "^2") (o #t) (d #t) (k 0)))) (h "03isrfnf1xbqym9fcpqhw4gjipl9yzdjfjs0cfqr8yzib6hpmiji") (f (quote (("print-trace" "colored"))))))

(define-public crate-snarkos-profiler-1.3.14 (c (n "snarkos-profiler") (v "1.3.14") (d (list (d (n "colored") (r "^2") (o #t) (d #t) (k 0)))) (h "1b02xk8jnd0gn28msdimqdryr2diwsjsh5ly0iyavhs22f792d1i") (f (quote (("print-trace" "colored"))))))

(define-public crate-snarkos-profiler-1.3.15 (c (n "snarkos-profiler") (v "1.3.15") (d (list (d (n "colored") (r "^2") (o #t) (d #t) (k 0)))) (h "16ln7ljdsf1iy5ldadvczy81n50jz81dm3rfs0msvlfrh6viavm0") (f (quote (("print-trace" "colored"))))))

(define-public crate-snarkos-profiler-1.3.16 (c (n "snarkos-profiler") (v "1.3.16") (d (list (d (n "colored") (r "^2") (o #t) (d #t) (k 0)))) (h "0r9x52zbap9x7vv7x7s7xswc95l6779avc3i6pn0qzg0xbk7r0va") (f (quote (("print-trace" "colored"))))))

(define-public crate-snarkos-profiler-1.3.17 (c (n "snarkos-profiler") (v "1.3.17") (d (list (d (n "colored") (r "^2") (o #t) (d #t) (k 0)))) (h "0v6g192h1jhxkqn2w8pq7limwj9v8irxmzrpgbx2ybm644h456jl") (f (quote (("print-trace" "colored"))))))

