(define-module (crates-io sn _b sn_bls_ckd) #:use-module (crates-io))

(define-public crate-sn_bls_ckd-0.2.1 (c (n "sn_bls_ckd") (v "0.2.1") (d (list (d (n "curv") (r "^0.10.1") (f (quote ("num-bigint"))) (k 0) (p "sn_curv")) (d (n "hex") (r "^0.3.1") (d #t) (k 0)) (d (n "hkdf") (r "^0.12.3") (d #t) (k 0)) (d (n "sha2") (r "^0.10.6") (d #t) (k 0)))) (h "1hnqqdcxr8msmj8s94xd4rybb3jycijb18dgrza8v32w1mdr1hcc")))

