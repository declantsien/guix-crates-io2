(define-module (crates-io sn _b sn_build_info) #:use-module (crates-io))

(define-public crate-sn_build_info-0.1.0 (c (n "sn_build_info") (v "0.1.0") (h "1cmza2gvxa736cd613pm8gbs3j5m6kgqnib764hg5k7lnwh8w4ia")))

(define-public crate-sn_build_info-0.1.1 (c (n "sn_build_info") (v "0.1.1") (h "0isjbik7wzb620afg9hgp0dwkdmb4phfpsc1b2r0zx3jjvdkmr9v")))

(define-public crate-sn_build_info-0.1.2 (c (n "sn_build_info") (v "0.1.2") (d (list (d (n "vergen") (r "^8.0.0") (f (quote ("build" "git" "gitcl"))) (d #t) (k 1)))) (h "0d2xm86hm8lvbz6mxx2qz851ddyaxqannbnjdlr2hdfd6qsk9x59")))

(define-public crate-sn_build_info-0.1.3 (c (n "sn_build_info") (v "0.1.3") (d (list (d (n "vergen") (r "^8.0.0") (f (quote ("build" "git" "gitcl"))) (d #t) (k 1)))) (h "1sbb6i47azim8fhbvyspp88f0mdi51dkb9y4a8m32ypy8k7bs0zw")))

(define-public crate-sn_build_info-0.1.4 (c (n "sn_build_info") (v "0.1.4") (d (list (d (n "vergen") (r "^8.0.0") (f (quote ("build" "git" "gitcl"))) (d #t) (k 1)))) (h "1xclvshamaqgzpnk8sndf94pr19a41f3i0k7jdykc2x72d2b77m8")))

(define-public crate-sn_build_info-0.1.5 (c (n "sn_build_info") (v "0.1.5") (d (list (d (n "vergen") (r "^8.0.0") (f (quote ("build" "git" "gitcl"))) (d #t) (k 1)))) (h "1rfaxzi42xa54kwj274056rbq9x88w9wi353kdxhsg1xrmv39z80")))

(define-public crate-sn_build_info-0.1.6-alpha.0 (c (n "sn_build_info") (v "0.1.6-alpha.0") (d (list (d (n "vergen") (r "^8.0.0") (f (quote ("build" "git" "gitcl"))) (d #t) (k 1)))) (h "0snfkynlvfqlmkb9rp416dvvdkgyfg5n6p3sk8d27raamnin01xs")))

(define-public crate-sn_build_info-0.1.6 (c (n "sn_build_info") (v "0.1.6") (d (list (d (n "vergen") (r "^8.0.0") (f (quote ("build" "git" "gitcl"))) (d #t) (k 1)))) (h "08qsvk0538d4a7mk2dihrnzllql8261imhv7awmi3s3mswfizc80")))

(define-public crate-sn_build_info-0.1.7-alpha.0 (c (n "sn_build_info") (v "0.1.7-alpha.0") (d (list (d (n "vergen") (r "^8.0.0") (f (quote ("build" "git" "gitcl"))) (d #t) (k 1)))) (h "0wdigidf0z6n3bdkyx17fdabql69yqngvya5bda4cnqbp3x5kxyr")))

(define-public crate-sn_build_info-0.1.7-alpha.1 (c (n "sn_build_info") (v "0.1.7-alpha.1") (d (list (d (n "vergen") (r "^8.0.0") (f (quote ("build" "git" "gitcl"))) (d #t) (k 1)))) (h "0pg8yn258sx7mz43rrmmpvgd5z0hmvx78andybn83285a79cqmdn")))

(define-public crate-sn_build_info-0.1.7 (c (n "sn_build_info") (v "0.1.7") (d (list (d (n "vergen") (r "^8.0.0") (f (quote ("build" "git" "gitcl"))) (d #t) (k 1)))) (h "0vcfnb0vrs9sqcy9rn44784cz8l49iij3s9dksjd1zf6ih4km2hv")))

