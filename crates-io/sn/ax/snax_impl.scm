(define-module (crates-io sn ax snax_impl) #:use-module (crates-io))

(define-public crate-snax_impl-0.1.0 (c (n "snax_impl") (v "0.1.0") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.11") (d #t) (k 0)) (d (n "snax_syntax") (r "^0.1.0") (d #t) (k 0)))) (h "1rnmh1bq8xpgz76pxbh998vp5x8jz0f5wpbyzgq8w6q6r5v8j7dl") (y #t)))

