(define-module (crates-io sn ed snedfile) #:use-module (crates-io))

(define-public crate-snedfile-0.1.0 (c (n "snedfile") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tcp-test") (r "^0.1") (d #t) (k 2)) (d (n "tempfile") (r "^3.1") (d #t) (k 2)))) (h "1d496syfqzd06qw1xszi9y3f854dzwyf88ydyddjgrr6jmjxjjcb") (f (quote (("large-files") ("ios-sendfile") ("fallback-bufreader") ("fallback-buf") ("default" "fallback-bufreader" "ios-sendfile"))))))

