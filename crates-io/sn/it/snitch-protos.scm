(define-module (crates-io sn it snitch-protos) #:use-module (crates-io))

(define-public crate-snitch-protos-0.0.4 (c (n "snitch-protos") (v "0.0.4") (d (list (d (n "protobuf") (r "^3.2.0") (d #t) (k 0)))) (h "1wnqzkyash5g01j7lia7pmfzhwr4v740sf2n92dl05f2afr8idlh")))

(define-public crate-snitch-protos-0.0.5 (c (n "snitch-protos") (v "0.0.5") (d (list (d (n "protobuf") (r "^3.2.0") (d #t) (k 0)))) (h "0h34hm30i8dbzp3dwvp41n16w6ws3kj7w680g0nmjxrhsp9wf4xg")))

(define-public crate-snitch-protos-0.0.6 (c (n "snitch-protos") (v "0.0.6") (d (list (d (n "protobuf") (r "^3.2.0") (d #t) (k 0)))) (h "13fxw42qwblmqwdbwad7g2xzs7kazzyy0fsrdcpnwfyii985gihp")))

(define-public crate-snitch-protos-0.0.7 (c (n "snitch-protos") (v "0.0.7") (d (list (d (n "protobuf") (r "^3.2.0") (d #t) (k 0)))) (h "1d2np57nkm0k2v64gwhn32zigvn2hmcalhmcg8j29qh3gigrnm2q")))

(define-public crate-snitch-protos-0.0.8 (c (n "snitch-protos") (v "0.0.8") (d (list (d (n "protobuf") (r "^3.2.0") (d #t) (k 0)))) (h "04y0mg8fa1zhdmzgnral2zwifdf4zqw2x5gjijfsvj86laq2sswa")))

(define-public crate-snitch-protos-0.0.9 (c (n "snitch-protos") (v "0.0.9") (d (list (d (n "protobuf") (r "^3.2.0") (d #t) (k 0)))) (h "1bck1lkf5k6v4zmb87lvcc6sa07ywi127wrlf0bcpyy0905arbj7")))

(define-public crate-snitch-protos-0.0.12 (c (n "snitch-protos") (v "0.0.12") (d (list (d (n "protobuf") (r "^3.2.0") (d #t) (k 0)))) (h "049fq5lnwwar9vb00h13m7bpjs69fdvashh36n9cm0a4ni6v77ng")))

(define-public crate-snitch-protos-0.0.13 (c (n "snitch-protos") (v "0.0.13") (d (list (d (n "protobuf") (r "^3.2.0") (d #t) (k 0)))) (h "0wnzikp2ylm5qm54zxqgn92zz97fn10vgcq6rwqs59l9khj99gd9")))

(define-public crate-snitch-protos-0.0.14 (c (n "snitch-protos") (v "0.0.14") (d (list (d (n "protobuf") (r "^3.2.0") (d #t) (k 0)))) (h "1ddqq8mixlf2x7dv5d8i2rnwh132lwqxzmsdlazxw7anch1byzf3")))

(define-public crate-snitch-protos-0.0.15 (c (n "snitch-protos") (v "0.0.15") (d (list (d (n "protobuf") (r "^3.2.0") (d #t) (k 0)))) (h "1ypgwd7qx8cy41mdpd7314lrzsv0bv4ccjai00zbf0ln39ba9z62")))

(define-public crate-snitch-protos-0.0.16 (c (n "snitch-protos") (v "0.0.16") (d (list (d (n "protobuf") (r "^3.2.0") (d #t) (k 0)))) (h "1a4fa5fv6cy6vb101n76rlz467j8zqr7h56bgb60j9ya7j7vghr2")))

(define-public crate-snitch-protos-0.0.17 (c (n "snitch-protos") (v "0.0.17") (d (list (d (n "protobuf") (r "^3.2.0") (d #t) (k 0)))) (h "1r6v1dfrvvli2hd2vhyz765giv3c0y8p2b38g8z8gxma2iggh6vb")))

(define-public crate-snitch-protos-0.0.18 (c (n "snitch-protos") (v "0.0.18") (d (list (d (n "protobuf") (r "^3.2.0") (d #t) (k 0)))) (h "14id8g3is1vi2g5g38cjdfn9f30zhixzcfdp73pz01g003640gxi")))

(define-public crate-snitch-protos-0.0.19 (c (n "snitch-protos") (v "0.0.19") (d (list (d (n "protobuf") (r "^3.2.0") (d #t) (k 0)))) (h "0djbisxcavymwjkvg46hsxp4s41mp5sfyyl4i2zdfpss5sgyjgwi")))

(define-public crate-snitch-protos-0.0.20 (c (n "snitch-protos") (v "0.0.20") (d (list (d (n "protobuf") (r "^3.2.0") (d #t) (k 0)))) (h "0m7z55ii4pgj778aar9ql8hwfigyvria6iqmig56is79ljbxzxap")))

(define-public crate-snitch-protos-0.0.21 (c (n "snitch-protos") (v "0.0.21") (d (list (d (n "protobuf") (r "^3.2.0") (d #t) (k 0)))) (h "0i8l7yblqni1h40098g0acrdmjljy7rg0lrg1i2ka264wp9ajv6y")))

(define-public crate-snitch-protos-0.0.22 (c (n "snitch-protos") (v "0.0.22") (d (list (d (n "protobuf") (r "^3.2.0") (d #t) (k 0)))) (h "1wpmg9xpb6258hknr3w085694yrzfck8rxx864hyi837v6c98kl6")))

(define-public crate-snitch-protos-0.0.23 (c (n "snitch-protos") (v "0.0.23") (d (list (d (n "protobuf") (r "^3.2.0") (d #t) (k 0)))) (h "09li6pbpr671jmxsk8nzqdmbhxn1pf6m5rs42k14jsgh80gb2yna")))

(define-public crate-snitch-protos-0.0.24 (c (n "snitch-protos") (v "0.0.24") (d (list (d (n "protobuf") (r "^3.2.0") (d #t) (k 0)))) (h "11xgl2a5649cy9hr45b6yh736gh28l6jwmmsp4d3r9i8iajnsiki")))

(define-public crate-snitch-protos-0.0.25 (c (n "snitch-protos") (v "0.0.25") (d (list (d (n "protobuf") (r "^3.2.0") (d #t) (k 0)))) (h "0hsxiwv1xz06q72p2a9lpr8vxhkj0mbh0db98fdz5vfmdz66mh9v")))

(define-public crate-snitch-protos-0.0.26 (c (n "snitch-protos") (v "0.0.26") (d (list (d (n "protobuf") (r "^3.2.0") (d #t) (k 0)))) (h "1y1vgmnkaj33vr9blr7c0725qxzpjr8bdzf3si2y4cxh65jkvhgh")))

(define-public crate-snitch-protos-0.0.27 (c (n "snitch-protos") (v "0.0.27") (d (list (d (n "protobuf") (r "^3.2.0") (d #t) (k 0)))) (h "0k84773znpgd5y3f5apa9a8q5fk8sp2bfgby4andg2c3vyh5946y")))

(define-public crate-snitch-protos-0.0.28 (c (n "snitch-protos") (v "0.0.28") (d (list (d (n "protobuf") (r "^3.2.0") (d #t) (k 0)))) (h "13l31zlrydsanpccpz4rjaq461jy0a18ifzfdj443s9hvndcvn1p")))

(define-public crate-snitch-protos-0.0.29 (c (n "snitch-protos") (v "0.0.29") (d (list (d (n "protobuf") (r "^3.2.0") (d #t) (k 0)))) (h "0f290ngj16a4y5czjh6bciivwl58h3g5yd89vr8jn15apqw4iivl")))

(define-public crate-snitch-protos-0.0.30 (c (n "snitch-protos") (v "0.0.30") (d (list (d (n "protobuf") (r "^3.2.0") (d #t) (k 0)))) (h "0v3lgqn7zdvkd0mv0yz459cgz47ddrnj7n2nbv3syf9ql7fn5vy4")))

(define-public crate-snitch-protos-0.0.31 (c (n "snitch-protos") (v "0.0.31") (d (list (d (n "protobuf") (r "^3.2.0") (d #t) (k 0)))) (h "1vv1n1bgy1h3jbv7f6wssigrwa5l9m9fambvbdj14ms666cla96p")))

(define-public crate-snitch-protos-0.0.32 (c (n "snitch-protos") (v "0.0.32") (d (list (d (n "protobuf") (r "^3.2.0") (d #t) (k 0)))) (h "0snflb2qpnr2d7n8027m1lnb0j9x4hqw8d2k773m722r4srysbfl")))

(define-public crate-snitch-protos-0.0.33 (c (n "snitch-protos") (v "0.0.33") (d (list (d (n "protobuf") (r "^3.2.0") (d #t) (k 0)))) (h "0f1dqji99nksh77x07g1ky9vncdskvyy844n67p88iaqzwhighdp")))

(define-public crate-snitch-protos-0.0.34 (c (n "snitch-protos") (v "0.0.34") (d (list (d (n "protobuf") (r "^3.2.0") (d #t) (k 0)))) (h "0jx4wnflxpjdf7cj8d7m179v19y3wzm8038ydq1b1izjv0r8iyl4")))

(define-public crate-snitch-protos-0.0.35 (c (n "snitch-protos") (v "0.0.35") (d (list (d (n "protobuf") (r "^3.2.0") (d #t) (k 0)))) (h "0rbnf04pfim55jrd1lwr6jjhwvxvsx4199gq8irg9r9bk7gsjvdw")))

(define-public crate-snitch-protos-0.0.36 (c (n "snitch-protos") (v "0.0.36") (d (list (d (n "protobuf") (r "^3.2.0") (d #t) (k 0)))) (h "1i8r8qqhb0k7949a6a1gvlprcc8a1hmi2ldvkmhxpr2brrcn6y94")))

(define-public crate-snitch-protos-0.0.37 (c (n "snitch-protos") (v "0.0.37") (d (list (d (n "protobuf") (r "^3.2.0") (d #t) (k 0)))) (h "15dp8i3b55x6qyi1bf6kvv37nc7m6d3b593f51njaccxmsrg76qc")))

(define-public crate-snitch-protos-0.0.38 (c (n "snitch-protos") (v "0.0.38") (d (list (d (n "protobuf") (r "^3.2.0") (d #t) (k 0)))) (h "09zs3lm6klccbzga7s5imkg6l23wpyndjvapm5pnrazpmab0il7i")))

(define-public crate-snitch-protos-0.0.39 (c (n "snitch-protos") (v "0.0.39") (d (list (d (n "protobuf") (r "^3.2.0") (d #t) (k 0)))) (h "1kp9lkljw6ygwkvy0fz80bwjfy8b2ii6ryf3sy6p26apwgckhila")))

(define-public crate-snitch-protos-0.0.40 (c (n "snitch-protos") (v "0.0.40") (d (list (d (n "protobuf") (r "^3.2.0") (d #t) (k 0)))) (h "0q6zclaglz4082cfisxcl1ddvnqq3yhkk5gwv00530h4di9x78vx")))

(define-public crate-snitch-protos-0.0.41 (c (n "snitch-protos") (v "0.0.41") (d (list (d (n "protobuf") (r "^3.2.0") (d #t) (k 0)))) (h "07dk7a24d8nbjpgpwm59g8v6klk2fqfq6hfzjv80m2bhnx9hbvp0")))

(define-public crate-snitch-protos-0.0.42 (c (n "snitch-protos") (v "0.0.42") (d (list (d (n "protobuf") (r "^3.2.0") (d #t) (k 0)))) (h "033wjm4xmdam8piw989k9frd7brxv9fb636xk883ivrr8jvl687l")))

(define-public crate-snitch-protos-0.0.43 (c (n "snitch-protos") (v "0.0.43") (d (list (d (n "protobuf") (r "^3.2.0") (d #t) (k 0)))) (h "1q03pd67wf9gihdcibh2s1kl1hlcy9hspqzfha2198c4m8dx50vw")))

(define-public crate-snitch-protos-0.0.44 (c (n "snitch-protos") (v "0.0.44") (d (list (d (n "protobuf") (r "^3.2.0") (d #t) (k 0)))) (h "0l862zbn0zl71xfylb0frj8qjd0p28a40j4ykgg3a7gmd0qcfnfk")))

(define-public crate-snitch-protos-0.0.45 (c (n "snitch-protos") (v "0.0.45") (d (list (d (n "protobuf") (r "^3.2.0") (d #t) (k 0)))) (h "15x21mhh6g2sibqlz0fn5v1sh7sxfqj2z28a4aq1kx290nl0a2pm")))

(define-public crate-snitch-protos-0.0.46 (c (n "snitch-protos") (v "0.0.46") (d (list (d (n "protobuf") (r "^3.2.0") (d #t) (k 0)))) (h "0gxa5y40sf5fb1m2xv41xqj5h73c3rijx5f8ykqrysmk67ci28lh")))

(define-public crate-snitch-protos-0.0.47 (c (n "snitch-protos") (v "0.0.47") (d (list (d (n "protobuf") (r "^3.2.0") (d #t) (k 0)))) (h "1zha2gcwj14fw2hf6bjx1racd8gmah89bmmgzzh52k3mvqymhfnr")))

(define-public crate-snitch-protos-0.0.48 (c (n "snitch-protos") (v "0.0.48") (d (list (d (n "protobuf") (r "^3.2.0") (d #t) (k 0)))) (h "0zrgx5clapllggxv91awzi644vdi9pqjfmirnvs5ljx9r77q1lz7")))

(define-public crate-snitch-protos-0.0.49 (c (n "snitch-protos") (v "0.0.49") (d (list (d (n "protobuf") (r "^3.2.0") (d #t) (k 0)))) (h "0z2x59g5s19a481wh9hjjqzrr1ki2bz5jcy2b2z5dx4bdmxfdg86")))

(define-public crate-snitch-protos-0.0.50 (c (n "snitch-protos") (v "0.0.50") (d (list (d (n "protobuf") (r "^3.2.0") (d #t) (k 0)))) (h "188z6s1044hrv59rg2zhspfa6k89w5rq6yrsw47knf5fhxki8fvc")))

(define-public crate-snitch-protos-0.0.51 (c (n "snitch-protos") (v "0.0.51") (d (list (d (n "protobuf") (r "^3.2.0") (d #t) (k 0)))) (h "1bi51qiinsk66gnnizic9dp5crh2z8jz5s43hj4pkw2ya1p2yn8s")))

(define-public crate-snitch-protos-0.0.52 (c (n "snitch-protos") (v "0.0.52") (d (list (d (n "protobuf") (r "^3.2.0") (d #t) (k 0)))) (h "1iqr0rl49a319d97sp20l5vmb4c1bd8xkhhcjrsw56hqjnj1rj4z")))

(define-public crate-snitch-protos-0.0.53 (c (n "snitch-protos") (v "0.0.53") (d (list (d (n "protobuf") (r "^3.2.0") (d #t) (k 0)))) (h "1kjfc0mwzi3px6v8rl7jkwhlg9hi9lgibzzhbsr2qlxpmw298r4w")))

(define-public crate-snitch-protos-0.0.54 (c (n "snitch-protos") (v "0.0.54") (d (list (d (n "protobuf") (r "^3.2.0") (d #t) (k 0)))) (h "0kh3z7qxipd12548025k7xcjsvir9b4fg9w9np30fvjs65ssgb9g")))

(define-public crate-snitch-protos-0.0.55 (c (n "snitch-protos") (v "0.0.55") (d (list (d (n "protobuf") (r "^3.2.0") (d #t) (k 0)))) (h "1j2fzbvffkyn44b7585d80rzhh058r2cqsrbf7bvy2cz5fav8njz")))

(define-public crate-snitch-protos-0.0.56 (c (n "snitch-protos") (v "0.0.56") (d (list (d (n "protobuf") (r "^3.2.0") (d #t) (k 0)))) (h "0jjal0x4c1w9jp0q4zk336l7abqsl0dxzw2wqg3rjzbj7qjd4q0l")))

(define-public crate-snitch-protos-0.0.57 (c (n "snitch-protos") (v "0.0.57") (d (list (d (n "protobuf") (r "^3.2.0") (d #t) (k 0)))) (h "1v433y5h64lfg6sgy20w57nqx2hcfy4yqs701mx54p07i4lxcr4v")))

(define-public crate-snitch-protos-0.0.58 (c (n "snitch-protos") (v "0.0.58") (d (list (d (n "protobuf") (r "^3.2.0") (d #t) (k 0)))) (h "0lbwgw3401wq2s4h1abpk7qii7amvi22069vmp5fdhl8bh8hhs5r")))

(define-public crate-snitch-protos-0.0.59 (c (n "snitch-protos") (v "0.0.59") (d (list (d (n "protobuf") (r "^3.2.0") (d #t) (k 0)))) (h "0hjq01az58z4am705jwsab38vfa59794pr437f89r5f9s85wp9x5")))

(define-public crate-snitch-protos-0.0.60 (c (n "snitch-protos") (v "0.0.60") (d (list (d (n "protobuf") (r "^3.2.0") (d #t) (k 0)))) (h "02gzjk8q51rs6gcqipjcha8h3llrzqhfmai42493x9lij7kdgwdl")))

(define-public crate-snitch-protos-0.0.61 (c (n "snitch-protos") (v "0.0.61") (d (list (d (n "protobuf") (r "^3.2.0") (d #t) (k 0)))) (h "0x259f16xy5i7b3frkv3z2kj28b33ik4imac9ybl7zwbzljvi4kx")))

(define-public crate-snitch-protos-0.0.62 (c (n "snitch-protos") (v "0.0.62") (d (list (d (n "protobuf") (r "^3.2.0") (d #t) (k 0)))) (h "0695rf32b6nv0pyr40j2z32l8b0kgbcn5n0g7lhw3c6l00zfnazd")))

(define-public crate-snitch-protos-0.0.63 (c (n "snitch-protos") (v "0.0.63") (d (list (d (n "protobuf") (r "^3.2.0") (d #t) (k 0)))) (h "1vplvdm19li23vyl9cag748srzs44pmkrqpjm2q4y20idy83xbh8")))

(define-public crate-snitch-protos-0.0.64 (c (n "snitch-protos") (v "0.0.64") (d (list (d (n "protobuf") (r "^3.2.0") (d #t) (k 0)))) (h "0gabyfbvkgb9pb0hws3ips5qdpbi68hffr10ciwdmi596w1si1w0")))

(define-public crate-snitch-protos-0.0.65 (c (n "snitch-protos") (v "0.0.65") (d (list (d (n "protobuf") (r "^3.2.0") (d #t) (k 0)))) (h "10a4m5jm55czf40mmyc9mh04392na78lnhkiy4vh9cr5a59g8yhm")))

(define-public crate-snitch-protos-0.0.66 (c (n "snitch-protos") (v "0.0.66") (d (list (d (n "protobuf") (r "^3.2.0") (d #t) (k 0)))) (h "1v6886prf9j4kcrxgxk2j953n2cklxbndjy37g14l2lqnysq792q")))

(define-public crate-snitch-protos-0.0.67 (c (n "snitch-protos") (v "0.0.67") (d (list (d (n "protobuf") (r "^3.2.0") (d #t) (k 0)))) (h "17ygpksxn6lp3n14vqadfyfhkp93dq70slrf80lb2zlmhpx5dakd")))

(define-public crate-snitch-protos-0.0.68 (c (n "snitch-protos") (v "0.0.68") (d (list (d (n "protobuf") (r "^3.2.0") (d #t) (k 0)))) (h "10d84ypcn2a7nhrm8j627jqajsishs5awlsrdq8bddwn0040ckr9")))

(define-public crate-snitch-protos-0.0.69 (c (n "snitch-protos") (v "0.0.69") (d (list (d (n "protobuf") (r "^3.2.0") (d #t) (k 0)))) (h "1m3lhabvzwr8ayp8wl75mbi8kr5hz5cajj9daza0n8w3pssclw8j")))

(define-public crate-snitch-protos-0.0.70 (c (n "snitch-protos") (v "0.0.70") (d (list (d (n "protobuf") (r "^3.2.0") (d #t) (k 0)))) (h "1bgk1ykn2wqrz9ag8jrdvilb69kk0f3m051il730vrsihynqiicm")))

(define-public crate-snitch-protos-0.0.71 (c (n "snitch-protos") (v "0.0.71") (d (list (d (n "protobuf") (r "^3.2.0") (d #t) (k 0)))) (h "1c80ny8ym85ywgpvhh2s924pg6gha6rx3j1swjf3ziwwkb1y39d3")))

(define-public crate-snitch-protos-0.0.72 (c (n "snitch-protos") (v "0.0.72") (d (list (d (n "protobuf") (r "^3.2.0") (d #t) (k 0)))) (h "1y63fkc27avsn5cjlpk2c5vbbq4da8rz2kxp7s0blykjx94z1hz4")))

(define-public crate-snitch-protos-0.0.73 (c (n "snitch-protos") (v "0.0.73") (d (list (d (n "protobuf") (r "^3.2.0") (d #t) (k 0)))) (h "0xi4n545vxn2pkfffwiw438jj323vy6v0lbk7lks3557admhvp88")))

(define-public crate-snitch-protos-0.0.74 (c (n "snitch-protos") (v "0.0.74") (d (list (d (n "protobuf") (r "^3.2.0") (d #t) (k 0)))) (h "0di3bn8vfy1w6w2672c3b4rcaqvnm84219lcflfdrzl8ppx9afa7")))

(define-public crate-snitch-protos-0.0.75 (c (n "snitch-protos") (v "0.0.75") (d (list (d (n "protobuf") (r "^3.2.0") (d #t) (k 0)))) (h "08g8jqs57jbb4wci8xy1nvqkss410jwapwl4v506szfan2c0skj2")))

(define-public crate-snitch-protos-0.0.76 (c (n "snitch-protos") (v "0.0.76") (d (list (d (n "protobuf") (r "^3.2.0") (d #t) (k 0)))) (h "0zqjhhvchi53dirb790r53as1zh6pndwh8k3j1507a5j6dh7c714")))

(define-public crate-snitch-protos-0.0.77 (c (n "snitch-protos") (v "0.0.77") (d (list (d (n "protobuf") (r "^3.2.0") (d #t) (k 0)))) (h "00kmy1ln03x4v4md1gqxfy6ss6v1ih517kwmxmdp2mz6fp9lyv60")))

(define-public crate-snitch-protos-0.0.78 (c (n "snitch-protos") (v "0.0.78") (d (list (d (n "protobuf") (r "^3.2.0") (d #t) (k 0)))) (h "167aazrxlrazj63mldd8m5sp7il9n6xgfxlbndp4650p1z8qpbkk")))

(define-public crate-snitch-protos-0.0.79 (c (n "snitch-protos") (v "0.0.79") (d (list (d (n "protobuf") (r "^3.2.0") (d #t) (k 0)))) (h "0mjyglydwgcry1x4x4rqdqspdd1snzh05zzxf8npilwxa1hyqqkl")))

(define-public crate-snitch-protos-0.0.80 (c (n "snitch-protos") (v "0.0.80") (d (list (d (n "protobuf") (r "^3.2.0") (d #t) (k 0)))) (h "0r8j8i4m8a8ihchg67dwk5mic4qa51wg0pj0hdvjc9dr4y40cp6v")))

(define-public crate-snitch-protos-0.0.81 (c (n "snitch-protos") (v "0.0.81") (d (list (d (n "protobuf") (r "^3.2.0") (d #t) (k 0)))) (h "0gqssya8rm19kxfvjbsjg5x5p4d117pw98jd90bc6ra7ymqx9p0b")))

(define-public crate-snitch-protos-0.0.82 (c (n "snitch-protos") (v "0.0.82") (d (list (d (n "protobuf") (r "^3.2.0") (d #t) (k 0)))) (h "1iv35pr28lmpspabfnb49sf9s3mzyq0aa32vwwbvbbdp7nz292nd")))

(define-public crate-snitch-protos-0.0.83 (c (n "snitch-protos") (v "0.0.83") (d (list (d (n "protobuf") (r "^3.2.0") (d #t) (k 0)))) (h "01p468vpcnmyd32cjm4hrlipxns9axyxshyffr9xgqcncynfhryr")))

(define-public crate-snitch-protos-0.0.84 (c (n "snitch-protos") (v "0.0.84") (d (list (d (n "protobuf") (r "^3.2.0") (d #t) (k 0)))) (h "1r226lia8zcrg90hcsml8mypdb8xq0ghm1lch81k8ghqx436d18p")))

(define-public crate-snitch-protos-0.0.85 (c (n "snitch-protos") (v "0.0.85") (d (list (d (n "protobuf") (r "^3.2.0") (d #t) (k 0)))) (h "1pi58qz9wbbamd9494qkbk88wdzb029nr7czbr4w5jv4rzwpjp1z")))

(define-public crate-snitch-protos-0.0.86 (c (n "snitch-protos") (v "0.0.86") (d (list (d (n "protobuf") (r "^3.2.0") (d #t) (k 0)))) (h "1qa3wcgzid98p4jg5ar7xz56pzpjbzsgv6bnsp1wz1v1dzhhjd9a")))

(define-public crate-snitch-protos-0.0.87 (c (n "snitch-protos") (v "0.0.87") (d (list (d (n "protobuf") (r "^3.2.0") (d #t) (k 0)))) (h "1qnrfwm1pfakpav2w7699yf85gaf4c68na4ckhrbpn6ipyvy7v2a")))

(define-public crate-snitch-protos-0.0.88 (c (n "snitch-protos") (v "0.0.88") (d (list (d (n "protobuf") (r "^3.2.0") (d #t) (k 0)))) (h "0qf85k1q4cfjbj6xcx7w8ivwrpwcbm42ca77qp9m3jbpk09ic5l8")))

(define-public crate-snitch-protos-0.0.89 (c (n "snitch-protos") (v "0.0.89") (d (list (d (n "protobuf") (r "^3.2.0") (d #t) (k 0)))) (h "1pnh812a402nkmvqc395rcyjxfnf74cii6kfg51l943kigvjny23")))

(define-public crate-snitch-protos-0.0.90 (c (n "snitch-protos") (v "0.0.90") (d (list (d (n "protobuf") (r "^3.2.0") (d #t) (k 0)))) (h "1m9kh9hhff86zpr24r3jjlcsl3jng1q67d14ff16waayz4rwgpyf")))

(define-public crate-snitch-protos-0.0.91 (c (n "snitch-protos") (v "0.0.91") (d (list (d (n "protobuf") (r "^3.2.0") (d #t) (k 0)))) (h "146vxaws4hjnbba8crzkg1z1fqwbsjkma2czp6xbpwspx8h66d1j")))

(define-public crate-snitch-protos-0.0.92 (c (n "snitch-protos") (v "0.0.92") (d (list (d (n "protobuf") (r "^3.2.0") (d #t) (k 0)))) (h "07q4wcnwg2xc1ngilpp95j6l93nazn880gl84hf10dsz30h6n4iv")))

(define-public crate-snitch-protos-0.0.93 (c (n "snitch-protos") (v "0.0.93") (d (list (d (n "protobuf") (r "^3.2.0") (d #t) (k 0)))) (h "02vxnms7l2fdavd38pk02hbf39515sy0d7l2q52qwkf2sd6hiysb")))

(define-public crate-snitch-protos-0.0.94 (c (n "snitch-protos") (v "0.0.94") (d (list (d (n "protobuf") (r "^3.2.0") (d #t) (k 0)))) (h "0lj913p6jz366085qyykj49vdasg8hnfh3vj6206gwy2x25nqr70")))

(define-public crate-snitch-protos-0.0.95 (c (n "snitch-protos") (v "0.0.95") (d (list (d (n "protobuf") (r "^3.2.0") (d #t) (k 0)))) (h "0pdf732r39lr4ijpsz3d5wblmnv86dffg6xb7xb2xyx5nbpkr9ac")))

(define-public crate-snitch-protos-0.0.96 (c (n "snitch-protos") (v "0.0.96") (d (list (d (n "protobuf") (r "^3.2.0") (d #t) (k 0)))) (h "16kkrb8n9ic35csw2l35gjs6li8m1kxlczb2s0k43iz2mccfl532")))

(define-public crate-snitch-protos-0.0.97 (c (n "snitch-protos") (v "0.0.97") (d (list (d (n "protobuf") (r "^3.2.0") (d #t) (k 0)))) (h "0awkz7l34gzw2k5affwy7d598dwvbkp67rl7y0z5da3m99cj7g3v")))

(define-public crate-snitch-protos-0.0.98 (c (n "snitch-protos") (v "0.0.98") (d (list (d (n "protobuf") (r "^3.2.0") (d #t) (k 0)))) (h "0p754njvv5w6qbbqxzaydxirqinydibc0273cdjscwrkf03lhv27")))

(define-public crate-snitch-protos-0.0.99 (c (n "snitch-protos") (v "0.0.99") (d (list (d (n "protobuf") (r "^3.2.0") (d #t) (k 0)))) (h "04cm0598vqfdgxwab1vnn12vsgq5ccd28099v1ldfl221di9fpmn")))

(define-public crate-snitch-protos-0.0.100 (c (n "snitch-protos") (v "0.0.100") (d (list (d (n "protobuf") (r "^3.2.0") (d #t) (k 0)))) (h "12drv55xj978swdipg0b7aqx3jxph5w4g9q0fs5k909bnfz1faw0")))

(define-public crate-snitch-protos-0.0.101 (c (n "snitch-protos") (v "0.0.101") (d (list (d (n "protobuf") (r "^3.2.0") (d #t) (k 0)))) (h "0sz9vynv91p371vas8myj4v0k8sza6mqyw3ix3726fyjxccwddv1")))

(define-public crate-snitch-protos-0.0.105 (c (n "snitch-protos") (v "0.0.105") (d (list (d (n "protobuf") (r "^3.2.0") (d #t) (k 0)))) (h "19wyhb8lk353m5zzlfp8mb8j1g2iqcykcvbk7f4a89sabxss2mcn")))

(define-public crate-snitch-protos-0.0.106 (c (n "snitch-protos") (v "0.0.106") (d (list (d (n "protobuf") (r "^3.2.0") (d #t) (k 0)))) (h "13iwhq51awrmmvl0zjv27mfn04s0288dgzkzm6199fsr01wqxpsl")))

(define-public crate-snitch-protos-0.0.107 (c (n "snitch-protos") (v "0.0.107") (d (list (d (n "protobuf") (r "^3.2.0") (d #t) (k 0)))) (h "0yf7hzw2xrb9aslrarjp366mz52k9n9zpgk0h77yf91vxawl8yrr")))

(define-public crate-snitch-protos-0.0.108 (c (n "snitch-protos") (v "0.0.108") (d (list (d (n "protobuf") (r "^3.2.0") (d #t) (k 0)))) (h "10l70n1n6l2bkhls1pjyvlj7xijivm0xcbaqg5cdrnfqpmidg7jk")))

(define-public crate-snitch-protos-0.0.109 (c (n "snitch-protos") (v "0.0.109") (d (list (d (n "protobuf") (r "^3.2.0") (d #t) (k 0)))) (h "0ips7sfg73d1q6h4kmycks8qnbls3bx0a2mwgkbfv6f3w2g9s7pr")))

(define-public crate-snitch-protos-0.0.110 (c (n "snitch-protos") (v "0.0.110") (d (list (d (n "protobuf") (r "^3.2.0") (d #t) (k 0)))) (h "1mdnh5zw7dd39j1gdphdh3hqw80gc9hlp1q4h1qmgnm40qb3hqrj")))

(define-public crate-snitch-protos-0.0.111 (c (n "snitch-protos") (v "0.0.111") (d (list (d (n "protobuf") (r "^3.2.0") (d #t) (k 0)))) (h "13lmcv40wjqszfxmmq18p1ld6v0zj4q11mvvcrxr32i5bznzwqk2")))

(define-public crate-snitch-protos-0.0.112 (c (n "snitch-protos") (v "0.0.112") (d (list (d (n "protobuf") (r "^3.2.0") (d #t) (k 0)))) (h "00frldxi09nr2p5sf32yv1sd7f0pml8q9j0zjq318b4ahxbf0gwq")))

(define-public crate-snitch-protos-0.0.113 (c (n "snitch-protos") (v "0.0.113") (d (list (d (n "protobuf") (r "^3.2.0") (d #t) (k 0)))) (h "1vyvy6gvz34ialqzzidj5h0zfmk7x4srspzkmqgfspqx2sfs2zk6")))

(define-public crate-snitch-protos-0.0.114 (c (n "snitch-protos") (v "0.0.114") (d (list (d (n "protobuf") (r "^3.2.0") (d #t) (k 0)))) (h "0m1si3lilriqs85wn16iwll9d0rac5hglxmmg5i6cxjcdd8zbbz6")))

