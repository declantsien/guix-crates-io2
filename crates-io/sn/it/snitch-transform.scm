(define-module (crates-io sn it snitch-transform) #:use-module (crates-io))

(define-public crate-snitch-transform-0.0.1 (c (n "snitch-transform") (v "0.0.1") (d (list (d (n "sha256") (r "^1.1.4") (d #t) (k 0)) (d (n "snitch-gjson") (r "^0.8.1-with-set") (d #t) (k 0)) (d (n "snitch-protos") (r "^0.0.9") (d #t) (k 0)))) (h "07q6w7vn7pnm9cmcawm864ppxwav181vkxwry50dsznkbvi3nyj9")))

(define-public crate-snitch-transform-0.0.2 (c (n "snitch-transform") (v "0.0.2") (d (list (d (n "sha256") (r "^1.1.4") (d #t) (k 0)) (d (n "snitch-gjson") (r "^0.8.1-with-set") (d #t) (k 0)) (d (n "snitch-protos") (r "^0.0.9") (d #t) (k 0)))) (h "0hpsr03jldqrqf1r0ij0cjasz9bc543s8rqccyh0jqp89m8crs37")))

(define-public crate-snitch-transform-0.0.3 (c (n "snitch-transform") (v "0.0.3") (d (list (d (n "sha256") (r "^1.1.4") (d #t) (k 0)) (d (n "snitch-gjson") (r "^0.8.1-with-set") (d #t) (k 0)) (d (n "snitch-protos") (r "^0.0.9") (d #t) (k 0)))) (h "02wb3zgdagp24cvsyki5b941ijb3nqrv2j0vszhndp9i2mj2mcd2")))

(define-public crate-snitch-transform-0.0.4 (c (n "snitch-transform") (v "0.0.4") (d (list (d (n "sha256") (r "^1.1.4") (d #t) (k 0)) (d (n "snitch-gjson") (r "^0.8.1-with-set") (d #t) (k 0)) (d (n "snitch-protos") (r "^0.0.12") (d #t) (k 0)))) (h "00qy59z0mvdaawwl45xaxibcfwv1vfcvmmmpfrva321hah36l8sh")))

(define-public crate-snitch-transform-0.0.5 (c (n "snitch-transform") (v "0.0.5") (d (list (d (n "sha256") (r "^1.1.4") (d #t) (k 0)) (d (n "snitch-gjson") (r "^0.8.1-with-set") (d #t) (k 0)) (d (n "snitch-protos") (r "^0.0.12") (d #t) (k 0)))) (h "0600xaw7i95r8mmv9hd9mf3yf1g0cr53w681kaa4lbjdiab74pkb")))

(define-public crate-snitch-transform-0.0.6 (c (n "snitch-transform") (v "0.0.6") (d (list (d (n "sha256") (r "^1.1.4") (d #t) (k 0)) (d (n "snitch-gjson") (r "^0.8.1-with-set") (d #t) (k 0)))) (h "1zlw0fqd4gbvasq27ymzgaijsv5l1117d99s782rnck2fskx33z9")))

