(define-module (crates-io sn zi snzip) #:use-module (crates-io))

(define-public crate-snzip-0.1.0 (c (n "snzip") (v "0.1.0") (d (list (d (n "byteorder") (r "*") (d #t) (k 0)) (d (n "crc") (r "^0.3.1") (d #t) (k 0)) (d (n "docopt") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "06gl2i0y6iig75rf0s58y0vhyb3hygz8yb244hsypa2chz64y57h") (y #t)))

