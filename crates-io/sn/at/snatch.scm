(define-module (crates-io sn at snatch) #:use-module (crates-io))

(define-public crate-snatch-0.1.2 (c (n "snatch") (v "0.1.2") (d (list (d (n "ansi_term") (r "^0.9.0") (d #t) (k 0)) (d (n "clap") (r "^2.20.0") (d #t) (k 0)) (d (n "hyper") (r "^0.9") (d #t) (k 0)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)) (d (n "pbr") (r "^1.0.0-alpha.2") (d #t) (k 0)))) (h "1f888fv1dqz5g1jpyrwnwj0gkp0lyqxpk4cjj9sq41ybjh92k6fk")))

(define-public crate-snatch-0.1.3 (c (n "snatch") (v "0.1.3") (d (list (d (n "ansi_term") (r "^0.9.0") (d #t) (k 0)) (d (n "clap") (r "^2.20.0") (d #t) (k 0)) (d (n "hyper") (r "^0.9") (d #t) (k 0)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)) (d (n "pbr") (r "^1.0.0") (d #t) (k 0)))) (h "0nsqlpcvcbbcmjap0xwv968kzi8c7b7s4lzr5ckicw27ci8l2kl5")))

