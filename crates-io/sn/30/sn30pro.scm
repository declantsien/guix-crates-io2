(define-module (crates-io sn #{30}# sn30pro) #:use-module (crates-io))

(define-public crate-sn30pro-0.1.0 (c (n "sn30pro") (v "0.1.0") (d (list (d (n "bytes") (r "^1.1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.16.1") (f (quote ("fs" "io-util"))) (d #t) (k 0)))) (h "0xag3wwdpnalcp66rqm6hyhdylp5a14963vx0nycrsp55b5fqqvp")))

(define-public crate-sn30pro-0.2.0 (c (n "sn30pro") (v "0.2.0") (d (list (d (n "bytes") (r "^1.1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.16.1") (f (quote ("fs" "io-util"))) (d #t) (k 0)))) (h "0wl4jban3gycjbn6m51dgg2x9v8l7bvhhv08v1kaac0jjmdidf36")))

