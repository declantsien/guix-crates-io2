(define-module (crates-io sn oo snoo) #:use-module (crates-io))

(define-public crate-snoo-0.0.1 (c (n "snoo") (v "0.0.1") (d (list (d (n "chrono") (r "^0.4.11") (f (quote ("serde"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.10.4") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2.21") (f (quote ("macros"))) (d #t) (k 0)))) (h "0b6sy0vv30wjf6nqdkimn5p6ihjnvvh2m54nh7bn3b20qc050aif")))

(define-public crate-snoo-0.0.2 (c (n "snoo") (v "0.0.2") (d (list (d (n "chrono") (r "^0.4.11") (f (quote ("serde"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.10.4") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2.21") (f (quote ("macros"))) (d #t) (k 0)))) (h "1k7fvnpx2ap3qq8yz2hb4ck0daiagjs5al1s4v6skaa7zjcbkxf3")))

(define-public crate-snoo-0.0.3 (c (n "snoo") (v "0.0.3") (d (list (d (n "chrono") (r "^0.4.11") (f (quote ("serde"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.10.4") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2.21") (f (quote ("macros"))) (d #t) (k 0)))) (h "06dmy54i6b0rxgd48vniprg0ax4x4y535j42pprmbqh4prml7j5l")))

(define-public crate-snoo-0.0.4 (c (n "snoo") (v "0.0.4") (d (list (d (n "chrono") (r "^0.4.11") (f (quote ("serde"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "log") (r "^0.4.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.4") (f (quote ("json" "cookies"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2.21") (f (quote ("macros" "time"))) (d #t) (k 0)))) (h "1bj246hc709llrnmf53a0il89jkjw8vfbv7a6j7cd2ndg2kmq61h")))

