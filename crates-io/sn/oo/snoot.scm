(define-module (crates-io sn oo snoot) #:use-module (crates-io))

(define-public crate-snoot-0.1.0 (c (n "snoot") (v "0.1.0") (d (list (d (n "itertools") (r "^0.5.9") (d #t) (k 0)) (d (n "regex") (r "^0.2.1") (d #t) (k 0)) (d (n "tendril") (r "^0.2.3") (d #t) (k 0)))) (h "1y7fzp475v4w7g0rzf3psbb86zzrlj669v2l3a51wxjkqmgp533h")))

(define-public crate-snoot-0.1.1 (c (n "snoot") (v "0.1.1") (d (list (d (n "itertools") (r "^0.5.9") (d #t) (k 0)) (d (n "regex") (r "^0.2.1") (d #t) (k 0)) (d (n "serde_json") (r "0.9.*") (d #t) (k 0)) (d (n "tendril") (r "^0.2.3") (d #t) (k 0)))) (h "0mbxp29w648pqyzlagr26pa129azyqb21d2gfgb4nxi0n854qjil")))

(define-public crate-snoot-0.1.2 (c (n "snoot") (v "0.1.2") (d (list (d (n "itertools") (r "^0.5.9") (d #t) (k 0)) (d (n "regex") (r "^0.2.1") (d #t) (k 0)) (d (n "serde") (r "1.*.*") (d #t) (k 0)) (d (n "serde_derive") (r "1.*.*") (d #t) (k 0)) (d (n "serde_json") (r "0.9.*") (d #t) (k 0)) (d (n "tendril") (r "^0.2.3") (d #t) (k 0)))) (h "00la4dxcfgszfgbzxy6y3llfbxid72qggv91ica9n0vcb7cqk17q")))

