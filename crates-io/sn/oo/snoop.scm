(define-module (crates-io sn oo snoop) #:use-module (crates-io))

(define-public crate-snoop-1.0.0 (c (n "snoop") (v "1.0.0") (h "1xw27k3rhs8hy1fbvmlm8cxmx2wfqpxblhyn3ip1hgh5rca94va6") (f (quote (("write") ("read") ("parse") ("full" "read" "write") ("default" "parse")))) (y #t)))

(define-public crate-snoop-1.0.1 (c (n "snoop") (v "1.0.1") (h "1hd0xa7dklycdgcch3i408wbfsdh22vqgziank55ky0gz2xzp34j") (f (quote (("write") ("read") ("parse") ("full" "read" "write") ("default" "parse"))))))

