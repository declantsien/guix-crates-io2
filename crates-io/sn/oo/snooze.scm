(define-module (crates-io sn oo snooze) #:use-module (crates-io))

(define-public crate-snooze-0.1.0 (c (n "snooze") (v "0.1.0") (d (list (d (n "pin-utils") (r "^0.1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.3.2") (f (quote ("sync" "rt" "time" "macros"))) (d #t) (k 0)))) (h "10sba2ljs8b3ncflljnfw2rk2w2fn737isl0x1s255c7l34n11cj")))

