(define-module (crates-io sn oo snooze-rs) #:use-module (crates-io))

(define-public crate-snooze-rs-0.0.1 (c (n "snooze-rs") (v "0.0.1") (h "1nij6b1d24bhrj4ad2fdj6dd56yl557xc9sb8nqhd9mmf580fbl7")))

(define-public crate-snooze-rs-0.0.2 (c (n "snooze-rs") (v "0.0.2") (h "005662rnljadjm3sxskm26akjc10pslqkw2cvg3jbgxg5x8h5wwk")))

(define-public crate-snooze-rs-0.0.3 (c (n "snooze-rs") (v "0.0.3") (d (list (d (n "libc") (r "^0.1.8") (d #t) (k 0)) (d (n "time") (r "^0.1.25") (d #t) (k 0)))) (h "14k22vwmsgk6r8j4ywaa1a85ly94sgkf5zc0vmi9k4w6bv687cdf")))

