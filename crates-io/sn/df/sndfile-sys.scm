(define-module (crates-io sn df sndfile-sys) #:use-module (crates-io))

(define-public crate-sndfile-sys-0.1.0 (c (n "sndfile-sys") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "18yj9phv0qbkkawg5csrlzkd3fd15c198rynqyy92ki3f2b8fl8x")))

(define-public crate-sndfile-sys-0.1.1 (c (n "sndfile-sys") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1a3rib76ndj4m03hpc23k2lxqpbsryi7ss8n8nm5nmj4chmp50ym")))

(define-public crate-sndfile-sys-0.1.2 (c (n "sndfile-sys") (v "0.1.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "vcpkg") (r "^0.2.6") (d #t) (k 1)))) (h "176pk3jq9fmwv2mb8a0vv9xh3n1jnhb3wfqk9xkkw7js3grrwq6w") (l "sndfile")))

(define-public crate-sndfile-sys-0.2.0 (c (n "sndfile-sys") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "vcpkg") (r "^0.2.6") (d #t) (k 1)))) (h "0d71i5gk2r606ml0pk14iaxhksisrk2hgziz7skiivz8fwnyp5jb") (l "sndfile")))

(define-public crate-sndfile-sys-0.2.1 (c (n "sndfile-sys") (v "0.2.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "vcpkg") (r "^0.2.6") (d #t) (k 1)))) (h "1qv2k2dm9in5fm08z5m96dz9d6r3zkhn20fdw6kbx082yfazb9bs") (l "sndfile")))

(define-public crate-sndfile-sys-0.2.2 (c (n "sndfile-sys") (v "0.2.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.18") (d #t) (k 1)) (d (n "vcpkg") (r "^0.2.6") (d #t) (k 1)))) (h "0nzdc27j4sb3l57djn8xqdlsy0jmdsj8z00wlvlkr0rs6vkd5g1v") (l "sndfile")))

(define-public crate-sndfile-sys-0.3.1 (c (n "sndfile-sys") (v "0.3.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.18") (d #t) (k 1)) (d (n "vcpkg") (r "^0.2.6") (d #t) (k 1)))) (h "1wb6lznifdhr1pvmcn4bs3yiv7nhkw38b7n9xavrk6bsm94xk6dj") (l "sndfile")))

