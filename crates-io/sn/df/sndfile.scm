(define-module (crates-io sn df sndfile) #:use-module (crates-io))

(define-public crate-sndfile-0.0.0 (c (n "sndfile") (v "0.0.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "ndarray") (r "^0.13") (o #t) (d #t) (k 0)) (d (n "sndfile-sys") (r "^0.2") (d #t) (k 0)))) (h "13ig8vbx52hx45yvwl0gcmlkbq21zzl1y5a9rp96aybpd3ymskiy") (f (quote (("ndarray_features" "ndarray") ("default"))))))

(define-public crate-sndfile-0.0.1 (c (n "sndfile") (v "0.0.1") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "ndarray") (r "^0.13") (o #t) (d #t) (k 0)) (d (n "sndfile-sys") (r "^0.2") (d #t) (k 0)))) (h "1q2ja0ld3ikqzppw5nrby5sfx2nqyxczhjbma9vda4m4lhsp2x90") (f (quote (("ndarray_features" "ndarray") ("default"))))))

(define-public crate-sndfile-0.0.2 (c (n "sndfile") (v "0.0.2") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "ndarray") (r "^0.13") (o #t) (d #t) (k 0)) (d (n "sndfile-sys") (r "^0.2") (d #t) (k 0)))) (h "05ncpyrc786zp78l52ma6b62llprhvvzflmd9d5gbq4jmh2sx0gp") (f (quote (("ndarray_features" "ndarray") ("default"))))))

(define-public crate-sndfile-0.0.3 (c (n "sndfile") (v "0.0.3") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "ndarray") (r "^0.13") (o #t) (d #t) (k 0)) (d (n "sndfile-sys") (r "^0.2") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1gkf4s4dw780qvhmyfr0bkhl04wz2akfjfkqckf72mn80f56anix") (f (quote (("ndarray_features" "ndarray") ("default"))))))

(define-public crate-sndfile-0.0.4 (c (n "sndfile") (v "0.0.4") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "ndarray") (r "^0.13") (o #t) (d #t) (k 0)) (d (n "sndfile-sys") (r "^0.2") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "0vc002hjshq89lq0s6pbgvzry5x7ds8n59rm506bmb340i8w61qa") (f (quote (("ndarray_features" "ndarray") ("default"))))))

(define-public crate-sndfile-0.1.0 (c (n "sndfile") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "ndarray") (r "^0.13") (o #t) (d #t) (k 0)) (d (n "sndfile-sys") (r "^0.2") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1gsqlz4fd0h78s69pll0pbfr53d1x84bkwvb4c3lph48kyq2s857") (f (quote (("ndarray_features" "ndarray") ("default"))))))

(define-public crate-sndfile-0.1.1 (c (n "sndfile") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "ndarray") (r "^0.13") (o #t) (d #t) (k 0)) (d (n "sndfile-sys") (r "^0.2") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1w8g53bmsyg31rvwxz518vybyv6xmwwigal2n65ardcdrrk45bz5") (f (quote (("ndarray_features" "ndarray") ("default"))))))

