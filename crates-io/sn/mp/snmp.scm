(define-module (crates-io sn mp snmp) #:use-module (crates-io))

(define-public crate-snmp-0.1.0 (c (n "snmp") (v "0.1.0") (h "094mw6nnhdlnrzbw42v0m1i3cngqkx3cs79fg6zncbi4kksp1iks") (f (quote (("private-tests"))))))

(define-public crate-snmp-0.1.1 (c (n "snmp") (v "0.1.1") (h "1pl4svdncy275jq51qklf8dp1ys2mc8rhnnc29a8qzdm8r7han4l") (f (quote (("private-tests"))))))

(define-public crate-snmp-0.2.0 (c (n "snmp") (v "0.2.0") (h "18lvvqx06fvrvykn75n8g3pmk6b19l6iwv2gqysy2qp9k45cn0d9")))

(define-public crate-snmp-0.2.1 (c (n "snmp") (v "0.2.1") (h "02a608p9i61h3n6nqm8v8smhhhcm92b7gl3pajnn1sylk0yqzin9")))

(define-public crate-snmp-0.2.2 (c (n "snmp") (v "0.2.2") (h "007wazrzca7y2vw668ymgwb20dnqrjs0q72l3q4qgi5595a5fapz")))

