(define-module (crates-io sn mp snmp-parser) #:use-module (crates-io))

(define-public crate-snmp-parser-0.1.0 (c (n "snmp-parser") (v "0.1.0") (d (list (d (n "der-parser") (r "^0.5.0") (d #t) (k 0)) (d (n "nom") (r "^3.0") (f (quote ("verbose-errors"))) (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.5.0") (d #t) (k 2)) (d (n "rusticata-macros") (r "^0.4.0") (d #t) (k 0)))) (h "1bnb5fflqlb9kz2p5nixid7qj18viym2biq38j0yjwqqw1v0ys5q")))

(define-public crate-snmp-parser-0.2.0 (c (n "snmp-parser") (v "0.2.0") (d (list (d (n "der-parser") (r "^1.0") (d #t) (k 0)) (d (n "nom") (r "^4.0") (f (quote ("verbose-errors"))) (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.5.0") (d #t) (k 2)) (d (n "rusticata-macros") (r "^1.0") (d #t) (k 0)))) (h "10yrpd6p29hdwxmh7xcypy8w8b285wy0ys6rnrc1snjzp1mldjzh")))

(define-public crate-snmp-parser-0.1.1 (c (n "snmp-parser") (v "0.1.1") (d (list (d (n "der-parser") (r "^0.5.0") (d #t) (k 0)) (d (n "nom") (r "^3.0") (f (quote ("verbose-errors"))) (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.5.0") (d #t) (k 2)) (d (n "rusticata-macros") (r "^0.4.0") (d #t) (k 0)))) (h "1zjhzh7jy11rbzbkjvw8hwaqg1rz46ksfc502l49jm83sfjz5dqv")))

(define-public crate-snmp-parser-0.3.0 (c (n "snmp-parser") (v "0.3.0") (d (list (d (n "der-parser") (r "^1.0") (d #t) (k 0)) (d (n "nom") (r "^4.0") (f (quote ("verbose-errors"))) (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.5.0") (d #t) (k 2)) (d (n "rusticata-macros") (r "^1.0") (d #t) (k 0)))) (h "0pckjvpwii7qs13837k1id0mayl1wcj4kmmjqzvxv4zwhkay8ixk")))

(define-public crate-snmp-parser-0.4.0 (c (n "snmp-parser") (v "0.4.0") (d (list (d (n "der-parser") (r "^2.0") (d #t) (k 0)) (d (n "nom") (r "^4.0") (f (quote ("verbose-errors"))) (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.5.0") (d #t) (k 2)) (d (n "rusticata-macros") (r "^1.0") (d #t) (k 0)))) (h "1w684509yrhpgf93nqms605j8x8inb2przligqycrnn4b6mmq5pc")))

(define-public crate-snmp-parser-0.4.1 (c (n "snmp-parser") (v "0.4.1") (d (list (d (n "der-parser") (r "^2.1") (d #t) (k 0)) (d (n "nom") (r "^4.0") (f (quote ("verbose-errors"))) (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.5.0") (d #t) (k 2)) (d (n "rusticata-macros") (r "^1.0") (d #t) (k 0)))) (h "0p5ygchxqd4h4nq54j56k28krdzn9hphkpvhjmsrwk3nf21dmrii")))

(define-public crate-snmp-parser-0.5.0 (c (n "snmp-parser") (v "0.5.0") (d (list (d (n "der-parser") (r "^3.0") (d #t) (k 0)) (d (n "nom") (r "^5.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6.0") (d #t) (k 2)) (d (n "rusticata-macros") (r "^2.0.2") (d #t) (k 0)))) (h "1ygaz23yj5lijvhp4i97s82cajk4kiqdwhplviin939qrk2mrhqz")))

(define-public crate-snmp-parser-0.5.1 (c (n "snmp-parser") (v "0.5.1") (d (list (d (n "der-parser") (r "^3.0") (d #t) (k 0)) (d (n "hex-literal") (r "^0.2") (d #t) (k 2)) (d (n "nom") (r "^5.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6.0") (d #t) (k 2)) (d (n "rusticata-macros") (r "^2.0.2") (d #t) (k 0)))) (h "15n680r4pwcxi88vdmrjyhyxfd9684w4h22cpx19wrvzicsy7ji6")))

(define-public crate-snmp-parser-0.5.2 (c (n "snmp-parser") (v "0.5.2") (d (list (d (n "der-parser") (r "^3.0.2") (d #t) (k 0)) (d (n "hex-literal") (r "^0.2") (d #t) (k 2)) (d (n "nom") (r "^5.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6.0") (d #t) (k 2)) (d (n "rusticata-macros") (r "^2.0.2") (d #t) (k 0)))) (h "1g9fw37hxz1bmrfwxm7v0njgvsbqjy3i0144is65w407ky84csi0")))

(define-public crate-snmp-parser-0.6.0 (c (n "snmp-parser") (v "0.6.0") (d (list (d (n "der-parser") (r "^4.0.0") (d #t) (k 0)) (d (n "hex-literal") (r "^0.2") (d #t) (k 2)) (d (n "nom") (r "^5.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6.0") (d #t) (k 2)) (d (n "rusticata-macros") (r "^2.0.2") (d #t) (k 0)))) (h "01bar3c1bafc9kn86y79dnfrhjl6hp7sx1fry2i2xv54bljn446f")))

(define-public crate-snmp-parser-0.7.0 (c (n "snmp-parser") (v "0.7.0") (d (list (d (n "der-parser") (r "^5.0") (d #t) (k 0)) (d (n "hex-literal") (r "^0.3") (d #t) (k 2)) (d (n "nom") (r "^6.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6.0") (d #t) (k 2)) (d (n "rusticata-macros") (r "^3.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1grhrli13nir7786shr8hc2x71kfqj7via7hb9g30984fzalyxf7")))

(define-public crate-snmp-parser-0.8.0 (c (n "snmp-parser") (v "0.8.0") (d (list (d (n "der-parser") (r "^6.0.0") (d #t) (k 0)) (d (n "hex-literal") (r "^0.3") (d #t) (k 2)) (d (n "nom") (r "^7.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.7") (d #t) (k 2)) (d (n "rusticata-macros") (r "^4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0yj0m03slhpj6y7j1ml92cyfvqm8wajbxjnvlapy2vs72q10nzwz")))

(define-public crate-snmp-parser-0.9.0 (c (n "snmp-parser") (v "0.9.0") (d (list (d (n "asn1-rs") (r "^0.5") (d #t) (k 0)) (d (n "hex-literal") (r "^0.3") (d #t) (k 2)) (d (n "nom") (r "^7.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.0") (d #t) (k 2)) (d (n "rusticata-macros") (r "^4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1cr4m80ripi85w5lzhvbsnmzwcgv5qk35k77b516yqs2cynjcfkp")))

(define-public crate-snmp-parser-0.10.0 (c (n "snmp-parser") (v "0.10.0") (d (list (d (n "asn1-rs") (r "^0.6") (d #t) (k 0)) (d (n "hex-literal") (r "^0.4") (d #t) (k 2)) (d (n "nom") (r "^7.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.0") (d #t) (k 2)) (d (n "rusticata-macros") (r "^4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "081cdvx3p0avq6bwkgf6z5mfmvy741xllyrvck91w513v851i4qg")))

