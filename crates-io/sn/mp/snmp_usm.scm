(define-module (crates-io sn mp snmp_usm) #:use-module (crates-io))

(define-public crate-snmp_usm-0.1.0 (c (n "snmp_usm") (v "0.1.0") (d (list (d (n "aes") (r "^0.4.0") (d #t) (k 0)) (d (n "block-modes") (r "^0.5.0") (d #t) (k 0)) (d (n "cfb-mode") (r "^0.4.0") (d #t) (k 0)) (d (n "des") (r "^0.4.0") (d #t) (k 0)) (d (n "hmac") (r "^0.8.1") (d #t) (k 0)) (d (n "md-5") (r "^0.9.1") (d #t) (k 0)) (d (n "sha-1") (r "^0.9.1") (d #t) (k 0)) (d (n "yasna") (r "^0.3.2") (d #t) (k 0)))) (h "1hrzfv84h0i5n3pns3sjmkqq5z041n7q8g2cgkw2z0h1fwzdv3m9")))

(define-public crate-snmp_usm-0.2.0 (c (n "snmp_usm") (v "0.2.0") (d (list (d (n "aes") (r "^0.4.0") (d #t) (k 0)) (d (n "block-modes") (r "^0.5.0") (d #t) (k 0)) (d (n "cfb-mode") (r "^0.4.0") (d #t) (k 0)) (d (n "des") (r "^0.4.0") (d #t) (k 0)) (d (n "hmac") (r "^0.8.1") (d #t) (k 0)) (d (n "md-5") (r "^0.9.1") (d #t) (k 0)) (d (n "sha-1") (r "^0.9.1") (d #t) (k 0)) (d (n "yasna") (r "^0.3.2") (d #t) (k 0)))) (h "0b9h81m0qc79z3jrib6qsdyaqjgjmlmalg01si8s8lr01lxlg2z7")))

(define-public crate-snmp_usm-0.2.1 (c (n "snmp_usm") (v "0.2.1") (d (list (d (n "aes") (r "^0.8.2") (d #t) (k 0)) (d (n "block-modes") (r "^0.5.0") (d #t) (k 0)) (d (n "cfb-mode") (r "^0.8.2") (d #t) (k 0)) (d (n "des") (r "^0.4.0") (d #t) (k 0)) (d (n "hmac") (r "^0.8.1") (d #t) (k 0)) (d (n "md-5") (r "^0.9.1") (d #t) (k 0)) (d (n "sha-1") (r "^0.9.1") (d #t) (k 0)) (d (n "yasna") (r "^0.3.2") (d #t) (k 0)))) (h "0i2izz5ndc3smxj5ph9x5r9xr5lkh1sdyprjr1vjg7b3fnv33l41")))

