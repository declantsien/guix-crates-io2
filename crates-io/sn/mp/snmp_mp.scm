(define-module (crates-io sn mp snmp_mp) #:use-module (crates-io))

(define-public crate-snmp_mp-0.1.0 (c (n "snmp_mp") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "yasna") (r "^0.3.2") (d #t) (k 0)))) (h "0fa5ib8cijr9z0km6qqys1f6chd9qsfjdg3nbjb2y4qs0m7hmgj0")))

