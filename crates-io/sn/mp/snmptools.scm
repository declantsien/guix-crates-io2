(define-module (crates-io sn mp snmptools) #:use-module (crates-io))

(define-public crate-snmptools-0.0.1 (c (n "snmptools") (v "0.0.1") (d (list (d (n "der-parser") (r "^6.0.0") (f (quote ("bigint"))) (d #t) (k 0)) (d (n "netsnmp-sys") (r "^0.1.1") (d #t) (k 0)))) (h "18i4bhw90j0fnli04y37hfbi4g8v4ihq2lkz61kr5swxrbg89nfs")))

(define-public crate-snmptools-0.0.2 (c (n "snmptools") (v "0.0.2") (d (list (d (n "der-parser") (r "^6.0.0") (f (quote ("bigint"))) (d #t) (k 0)) (d (n "netsnmp-sys") (r "^0.1.1") (d #t) (k 0)))) (h "01q2vw92wg2l4rzbx2k6s5nwcykcs2k1s72994r4jbbq4c38bbwh")))

(define-public crate-snmptools-0.0.3 (c (n "snmptools") (v "0.0.3") (d (list (d (n "der-parser") (r "^6.0.0") (f (quote ("bigint"))) (d #t) (k 0)) (d (n "netsnmp-sys") (r "^0.1.1") (d #t) (k 0)))) (h "0m7c9wla3m7nxch1viv19wbm3cv8rlrbmr11r4z9l33pr0q4dxb8")))

(define-public crate-snmptools-0.0.4 (c (n "snmptools") (v "0.0.4") (d (list (d (n "der-parser") (r "^6.0.0") (f (quote ("bigint"))) (d #t) (k 0)) (d (n "netsnmp-sys") (r "^0.1.1") (d #t) (k 0)))) (h "0x3zchhskd5lav9cmcss0cj9v71cyfpng4ps1qsjbq2rfwsi2dmh")))

(define-public crate-snmptools-0.0.5 (c (n "snmptools") (v "0.0.5") (d (list (d (n "der-parser") (r "^6.0.0") (f (quote ("bigint"))) (d #t) (k 0)) (d (n "netsnmp-sys") (r "^0.1.1") (d #t) (k 0)))) (h "1yxjxs5xcfq40fcaik40wlkwc0b8dlmdqh8amfz86mi9z62xj8af")))

(define-public crate-snmptools-0.0.6 (c (n "snmptools") (v "0.0.6") (d (list (d (n "der-parser") (r "^6.0.0") (f (quote ("bigint"))) (d #t) (k 0)) (d (n "netsnmp-sys") (r "^0.1.1") (d #t) (k 0)))) (h "1lfmhl97056gminz0szr747cj25qf0wy8fm6zhixr1abciqd72ki")))

(define-public crate-snmptools-0.0.7 (c (n "snmptools") (v "0.0.7") (d (list (d (n "der-parser") (r "^6.0.0") (f (quote ("bigint"))) (d #t) (k 0)) (d (n "libloading") (r "^0.7.3") (o #t) (d #t) (k 0)) (d (n "netsnmp-sys") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "once_cell") (r "^1.12.0") (o #t) (d #t) (k 0)))) (h "0agvamv55rn6b5ppddvs1c27qi9rm1s24glb4ina86gx22hg6gc3") (f (quote (("static" "netsnmp-sys") ("dynamic" "libloading" "once_cell"))))))

(define-public crate-snmptools-0.0.8 (c (n "snmptools") (v "0.0.8") (d (list (d (n "der-parser") (r "^6.0.0") (f (quote ("bigint"))) (d #t) (k 0)) (d (n "libloading") (r "^0.7.3") (o #t) (d #t) (k 0)) (d (n "netsnmp-sys") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "once_cell") (r "^1.12.0") (o #t) (d #t) (k 0)))) (h "17s8b7ikplbvk3li5m1sf953mdxhzlaysp0xm2xzhhgyxgywcbw8") (f (quote (("static" "netsnmp-sys") ("dynamic" "libloading" "once_cell"))))))

(define-public crate-snmptools-0.0.9 (c (n "snmptools") (v "0.0.9") (d (list (d (n "der-parser") (r "^6.0.0") (f (quote ("bigint"))) (d #t) (k 0)) (d (n "libloading") (r "^0.7.3") (o #t) (d #t) (k 0)) (d (n "netsnmp-sys-nocrypto") (r "^0.1.2") (o #t) (d #t) (k 0)) (d (n "once_cell") (r "^1.12.0") (o #t) (d #t) (k 0)))) (h "19pvv59dijd0h3a1h1d22wsf7pp5h91dqwd4k1y7skzmqw5sn62p") (f (quote (("static" "netsnmp-sys-nocrypto") ("dynamic" "libloading" "once_cell"))))))

(define-public crate-snmptools-0.0.10 (c (n "snmptools") (v "0.0.10") (d (list (d (n "der-parser") (r "^6.0.0") (f (quote ("bigint"))) (d #t) (k 0)) (d (n "libloading") (r "^0.7.3") (o #t) (d #t) (k 0)) (d (n "netsnmp-sys-nocrypto") (r "^0.1.3") (o #t) (d #t) (k 0)) (d (n "once_cell") (r "^1.12.0") (o #t) (d #t) (k 0)))) (h "1y8lsfbacpg7np4igbcryfi46pb6lpjnww3qaaqkn6rl7r2ms751") (f (quote (("static" "netsnmp-sys-nocrypto") ("dynamic" "libloading" "once_cell"))))))

(define-public crate-snmptools-0.0.11 (c (n "snmptools") (v "0.0.11") (d (list (d (n "der-parser") (r "^6.0.0") (f (quote ("bigint"))) (d #t) (k 0)) (d (n "libloading") (r "^0.7.3") (o #t) (d #t) (k 0)) (d (n "netsnmp-sys-nocrypto") (r "^0.1.3") (o #t) (d #t) (k 0)) (d (n "once_cell") (r "^1.12.0") (o #t) (d #t) (k 0)))) (h "0bhxxrk2iwxbwnfhv7ml4407dzrzv7a0611m80g5z53aszky96y1") (f (quote (("static" "netsnmp-sys-nocrypto") ("dynamic" "libloading" "once_cell"))))))

