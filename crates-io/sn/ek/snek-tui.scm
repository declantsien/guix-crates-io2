(define-module (crates-io sn ek snek-tui) #:use-module (crates-io))

(define-public crate-snek-tui-0.1.0 (c (n "snek-tui") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.26.1") (d #t) (k 0)) (d (n "tui") (r "^0.19.0") (d #t) (k 0)))) (h "0k9db3c31bngxlka2gj2r1l4ivhd0wirmg0i7zdj9dgg73y52k2b")))

