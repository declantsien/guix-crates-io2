(define-module (crates-io sn ek snek) #:use-module (crates-io))

(define-public crate-snek-0.1.0 (c (n "snek") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.7") (d #t) (k 0)))) (h "14rzyll9yh7nhs8pqgr06sgwwpsm379fzc5yazxbcxv05k7s4mq2")))

(define-public crate-snek-0.1.1 (c (n "snek") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.7") (d #t) (k 0)))) (h "182dzwzk0p2q778nxzsfqxskji0dv3pmxh1gf4z39hancsqbwss9")))

(define-public crate-snek-0.1.2 (c (n "snek") (v "0.1.2") (d (list (d (n "libc") (r "^0.2.7") (d #t) (k 0)))) (h "0i2jjpsd0723jvviwc2dys3yfdfw1aqblcvr83f8xlqmxr71v7fv")))

(define-public crate-snek-0.1.3 (c (n "snek") (v "0.1.3") (d (list (d (n "libc") (r "^0.2.7") (d #t) (k 0)))) (h "1zb73mq28pdvnmsi0np5iwdqzyc7xvsr2gpxgc9ha4y3r33j53g6")))

(define-public crate-snek-0.2.0 (c (n "snek") (v "0.2.0") (d (list (d (n "libc") (r "^0.2.7") (d #t) (k 0)))) (h "03ymk6y4224syhg7kfdx33c104xp7w5n7xz3mw1vs3a6kfzxrnbz")))

(define-public crate-snek-0.3.0 (c (n "snek") (v "0.3.0") (d (list (d (n "kernel32-sys") (r "^0.2.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.7") (d #t) (k 0)) (d (n "winapi") (r "^0.2.5") (d #t) (k 0)))) (h "1al112c2a80fs44d3hgi9w8gpr6qbbkaydrlwpdx82r3nzvmcfy1")))

