(define-module (crates-io sn ek snekgame) #:use-module (crates-io))

(define-public crate-snekgame-0.1.0 (c (n "snekgame") (v "0.1.0") (d (list (d (n "clap") (r "^4.5.1") (f (quote ("derive" "help" "usage" "error-context"))) (d #t) (k 0)) (d (n "clap_complete") (r "^4.5.1") (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "eyre") (r "^0.6.12") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)))) (h "0f8kkc4rbif11bmgz53mnxk1nrm4s5yzcckpbrn1m9ia143w8wz2")))

(define-public crate-snekgame-0.1.1 (c (n "snekgame") (v "0.1.1") (d (list (d (n "clap") (r "^4.5.1") (f (quote ("derive" "help" "usage" "error-context"))) (d #t) (k 0)) (d (n "clap_complete") (r "^4.5.1") (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "eyre") (r "^0.6.12") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)))) (h "0dnqgbndxpxm7gxjq3kw0dnl1wlnzndrawiczfbvwna610sjcsk7")))

(define-public crate-snekgame-0.1.2 (c (n "snekgame") (v "0.1.2") (d (list (d (n "clap") (r "^4.5.1") (f (quote ("derive" "help" "usage" "error-context"))) (d #t) (k 0)) (d (n "clap_complete") (r "^4.5.1") (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "eyre") (r "^0.6.12") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)))) (h "1cswkwpg6gkd4xmlpsb2gdg22v32ys3v4ngal0ki974sh2f0av5a")))

