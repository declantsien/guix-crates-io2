(define-module (crates-io sn ek sneks) #:use-module (crates-io))

(define-public crate-sneks-0.1.0 (c (n "sneks") (v "0.1.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (f (quote ("full"))) (d #t) (k 0)))) (h "0mhd2jril12q5b9j91aw72np4bacgm1gqs347h09izs8p0hvs0vg")))

(define-public crate-sneks-0.1.1 (c (n "sneks") (v "0.1.1") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (f (quote ("full"))) (d #t) (k 0)))) (h "1hlcfk4fkiicwyi3h6dh72bdwyssg4iw1bim7h07rn72zc3y24c1")))

(define-public crate-sneks-0.1.2 (c (n "sneks") (v "0.1.2") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (f (quote ("full"))) (d #t) (k 0)))) (h "03c18gfpi3jyhcj4a7s9g8ilyb5rwi0j4dlx4zxlkl9p76asjm0p")))

