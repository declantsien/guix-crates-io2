(define-module (crates-io sn bc snbclabelprint) #:use-module (crates-io))

(define-public crate-snbclabelprint-0.1.0 (c (n "snbclabelprint") (v "0.1.0") (d (list (d (n "encoding") (r "^0.2.33") (d #t) (k 0)) (d (n "libc") (r "^0.2.135") (d #t) (k 0)) (d (n "libloading") (r "^0.7.3") (d #t) (k 0)))) (h "135b9avgm7c654wr4a569aim3i9df1f8h2yp890vrjbfk33jgxr8")))

