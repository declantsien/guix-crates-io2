(define-module (crates-io sn ap snappy-cpp) #:use-module (crates-io))

(define-public crate-snappy-cpp-0.1.0 (c (n "snappy-cpp") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0sq5hbl09p6bn75ggfk1anwfsq6dh4b237z8l1b1i45pz23hqwbd")))

(define-public crate-snappy-cpp-0.1.1 (c (n "snappy-cpp") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0sxga9z8sg4gp6ghyz1p8r3c6flzjw57dlqdxavhvcjr1iwcfki3")))

