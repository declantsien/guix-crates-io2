(define-module (crates-io sn ap snappy-sys) #:use-module (crates-io))

(define-public crate-snappy-sys-0.1.0 (c (n "snappy-sys") (v "0.1.0") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.7") (d #t) (k 1)))) (h "01x3k6ybljpczkvknddynfgkin9qrsi3mnr4p6q4hm88xx9k11wz")))

