(define-module (crates-io sn ap snap) #:use-module (crates-io))

(define-public crate-snap-0.1.0 (c (n "snap") (v "0.1.0") (d (list (d (n "byteorder") (r "^0.5.3") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "quickcheck") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "snappy-cpp") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0dbsl6da4w4rv6a3svycsx0l8257irdbgxklkfw0qk0adb3plkp5") (f (quote (("cpp" "snappy-cpp"))))))

(define-public crate-snap-0.1.1 (c (n "snap") (v "0.1.1") (d (list (d (n "byteorder") (r "^0.5.3") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "quickcheck") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "snappy-cpp") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0ilxmh7vkw2av326c8vp1y9q1jm4cvl258z9nn94k030kl0r2dw5") (f (quote (("cpp" "snappy-cpp"))))))

(define-public crate-snap-0.1.2 (c (n "snap") (v "0.1.2") (d (list (d (n "byteorder") (r "^0.5.3") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "quickcheck") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "snappy-cpp") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0kvvv9jd5by7b5g2wlz7lh1lk10qy639iydl4hrnp7ayxknpp1rp") (f (quote (("cpp" "snappy-cpp"))))))

(define-public crate-snap-0.2.0 (c (n "snap") (v "0.2.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "quickcheck") (r "^0.4") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "snappy-cpp") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1127n3abgmbjdsiwqspsxc863ixcxp3xr66nw6v4czw4wgrjssq2") (f (quote (("cpp" "snappy-cpp"))))))

(define-public crate-snap-0.2.1 (c (n "snap") (v "0.2.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "quickcheck") (r "^0.4") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "snappy-cpp") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0cdnsz2xmcqb0hz2zycc016hid91lmh9d8w43l8m2n9fw3lpij95") (f (quote (("cpp" "snappy-cpp"))))))

(define-public crate-snap-0.2.2 (c (n "snap") (v "0.2.2") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "quickcheck") (r "^0.4") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "snappy-cpp") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1qlzkbf9bx7vv80msh44drf32fd0sv67kyjzfcxqfgw2ydfzlqjl") (f (quote (("cpp" "snappy-cpp"))))))

(define-public crate-snap-0.2.3 (c (n "snap") (v "0.2.3") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "quickcheck") (r "^0.4") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "snappy-cpp") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0h51qkdfqmpkzc0gjwgxkvy4xvx6ns0xccpixna74isxdxhl1yi4") (f (quote (("cpp" "snappy-cpp"))))))

(define-public crate-snap-0.2.4 (c (n "snap") (v "0.2.4") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.6") (d #t) (k 2)) (d (n "rand") (r "^0.4") (d #t) (k 2)) (d (n "snappy-cpp") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0wr5p3kh7khpdff2l5zl2skm6p4fbn7g2w10x550hibhixsm2i0p") (f (quote (("cpp" "snappy-cpp"))))))

(define-public crate-snap-0.2.5 (c (n "snap") (v "0.2.5") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.7") (k 2)) (d (n "rand") (r "^0.5") (d #t) (k 2)) (d (n "snappy-cpp") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0hvzvvywzw654y2r3j4jya5af8j0nf2vydfjp1w8pba47pb9gmlm") (f (quote (("cpp" "snappy-cpp"))))))

(define-public crate-snap-1.0.0 (c (n "snap") (v "1.0.0") (d (list (d (n "doc-comment") (r "^0.3.1") (d #t) (k 2)))) (h "0xl3g86brk6blbfhqxhiadia49n2v51knjj7q4f5mcvpp05rpyzp")))

(define-public crate-snap-1.0.1 (c (n "snap") (v "1.0.1") (d (list (d (n "doc-comment") (r "^0.3.1") (d #t) (k 2)))) (h "0vkxgdfg75g0sj3y5vini8j7mkmyy6jz14qb606f9h7bgbvwhwys")))

(define-public crate-snap-1.0.2 (c (n "snap") (v "1.0.2") (d (list (d (n "doc-comment") (r "^0.3.1") (d #t) (k 2)))) (h "125v7nd87lfgahf17yyy82070cy759kqf66fzhm4zp487ahz5izc")))

(define-public crate-snap-1.0.3 (c (n "snap") (v "1.0.3") (d (list (d (n "doc-comment") (r "^0.3.1") (d #t) (k 2)))) (h "1d0psg7hdkgqz2032q0sr564bm91nz1wkd6qdh6p31mzhip31lwq")))

(define-public crate-snap-1.0.4 (c (n "snap") (v "1.0.4") (d (list (d (n "doc-comment") (r "^0.3.1") (d #t) (k 2)))) (h "16mmcks5v8srixpla117ckraqckg3wwavl3csn0093rrl5v58wnw")))

(define-public crate-snap-1.0.5 (c (n "snap") (v "1.0.5") (d (list (d (n "doc-comment") (r "^0.3.1") (d #t) (k 2)))) (h "0l8llidvm7dlwfw2ql6hk4b4byl9677fppwgl7i2wglqs6a60ia5")))

(define-public crate-snap-1.1.0 (c (n "snap") (v "1.1.0") (d (list (d (n "doc-comment") (r "^0.3.1") (d #t) (k 2)))) (h "0c882cs4wbyi34nw8njpxa729gyi6sj71h8rj4ykbdvyxyv0m7sy")))

(define-public crate-snap-1.1.1 (c (n "snap") (v "1.1.1") (d (list (d (n "doc-comment") (r "^0.3.1") (d #t) (k 2)))) (h "0fxw80m831l76a5zxcwmz2aq7mcwc1pp345pnljl4cv1kbxnfsqv")))

