(define-module (crates-io sn ap snapbox-macros) #:use-module (crates-io))

(define-public crate-snapbox-macros-0.2.0 (c (n "snapbox-macros") (v "0.2.0") (h "0j3x7z6qxacdriw4sxl29v6xhzdx51y6nymrnxcqyb1k8g1hlqfh") (f (quote (("default") ("debug"))))))

(define-public crate-snapbox-macros-0.2.1 (c (n "snapbox-macros") (v "0.2.1") (h "0c79lnjcs9yp62y665swv5y5y6088qc256bfr3s7xcnb0izfl7f0") (f (quote (("default") ("debug"))))))

(define-public crate-snapbox-macros-0.3.0 (c (n "snapbox-macros") (v "0.3.0") (h "1wp81hx08vj598ksnigcw3c6j259kwj0lq00p8649yjci5pkw9ca") (f (quote (("default") ("debug")))) (r "1.56.1")))

(define-public crate-snapbox-macros-0.3.1 (c (n "snapbox-macros") (v "0.3.1") (h "0dkk7b5l9g8q7fswqj0686jqafkdl8apv1ay8r275cry430napj8") (f (quote (("default") ("debug")))) (r "1.60.0")))

(define-public crate-snapbox-macros-0.3.2 (c (n "snapbox-macros") (v "0.3.2") (d (list (d (n "anstyle-stream") (r "^0.2.1") (o #t) (d #t) (k 0)))) (h "1xb4q7qmbq989wz2cwh7h9qxyypg7kpbgxmmyv7sb46nzhjvh86f") (f (quote (("default") ("debug")))) (s 2) (e (quote (("color" "dep:anstyle-stream")))) (r "1.64.0")))

(define-public crate-snapbox-macros-0.3.3 (c (n "snapbox-macros") (v "0.3.3") (d (list (d (n "anstream") (r "^0.2.1") (o #t) (d #t) (k 0)))) (h "03w6rn8y6barxzlw7id6vzpj62mz2c05sm0g0ssirvc8fdk0rr7q") (f (quote (("default") ("debug")))) (s 2) (e (quote (("color" "dep:anstream")))) (r "1.64.0")))

(define-public crate-snapbox-macros-0.3.4 (c (n "snapbox-macros") (v "0.3.4") (d (list (d (n "anstream") (r "^0.3.0") (o #t) (d #t) (k 0)))) (h "0cfaq1vwy90g3csmmzm773kk0i91422r3432x4myishfkzghkbza") (f (quote (("default") ("debug")))) (s 2) (e (quote (("color" "dep:anstream")))) (r "1.64.0")))

(define-public crate-snapbox-macros-0.3.5 (c (n "snapbox-macros") (v "0.3.5") (d (list (d (n "anstream") (r "^0.5.0") (o #t) (d #t) (k 0)))) (h "09g37v54ffw999wvnhz8fsym8yc4nws52a281s6sjzdq27cgzx4m") (f (quote (("default") ("debug")))) (s 2) (e (quote (("color" "dep:anstream")))) (r "1.70.0")))

(define-public crate-snapbox-macros-0.3.6 (c (n "snapbox-macros") (v "0.3.6") (d (list (d (n "anstream") (r "^0.6.0") (o #t) (d #t) (k 0)))) (h "13mj53zkgz85f80pk3g3nixkwhqdjhzbxf926gfnlscazyx5j5gd") (f (quote (("default") ("debug")))) (s 2) (e (quote (("color" "dep:anstream")))) (r "1.70.0")))

(define-public crate-snapbox-macros-0.3.7 (c (n "snapbox-macros") (v "0.3.7") (d (list (d (n "anstream") (r "^0.6.7") (o #t) (d #t) (k 0)))) (h "0rxn80n3y6h7cp75zd3g3akpw3whh31fhvv9zy20k55dk82xxk3q") (f (quote (("default") ("debug")))) (s 2) (e (quote (("color" "dep:anstream")))) (r "1.70.0")))

(define-public crate-snapbox-macros-0.3.8 (c (n "snapbox-macros") (v "0.3.8") (d (list (d (n "anstream") (r "^0.6.7") (o #t) (d #t) (k 0)))) (h "0h6yy5h83y17bghi1lq9pw3knbqba1rwns20flian5axn0wbii71") (f (quote (("default") ("debug")))) (s 2) (e (quote (("color" "dep:anstream")))) (r "1.70.0")))

(define-public crate-snapbox-macros-0.3.9 (c (n "snapbox-macros") (v "0.3.9") (d (list (d (n "anstream") (r "^0.6.7") (o #t) (d #t) (k 0)))) (h "0ka2av6cg4b5nxzi19c226jrcc909f9ir02ljb03ci3if93c3x5i") (f (quote (("default") ("debug")))) (s 2) (e (quote (("color" "dep:anstream")))) (r "1.65")))

