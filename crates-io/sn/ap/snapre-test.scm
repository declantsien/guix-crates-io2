(define-module (crates-io sn ap snapre-test) #:use-module (crates-io))

(define-public crate-snapre-test-0.1.0 (c (n "snapre-test") (v "0.1.0") (h "0fm18rvixqxm1lbf0vbn0ppgph1bsvlf4dmn0bdlj8gw976swknf") (y #t)))

(define-public crate-snapre-test-0.1.1 (c (n "snapre-test") (v "0.1.1") (h "1gvvkrn91yhh074psqyw093rp78zrlhw6a4356ycd5qfm95mn08j") (y #t)))

