(define-module (crates-io sn ap snap7-sys) #:use-module (crates-io))

(define-public crate-snap7-sys-0.1.0 (c (n "snap7-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.37") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "00d9dr3rb96yllw97zszs5mxfpiqqn26nm2wihfs16nvz9ap2xp1")))

(define-public crate-snap7-sys-0.1.1 (c (n "snap7-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.37") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0vd81m45fnzg13ik53rlzpi7b7zh8lvigvgibmhipz66nv666fx5")))

(define-public crate-snap7-sys-0.1.2 (c (n "snap7-sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.49.2") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1v38ww5lbr0vn230gr1fnapqjbyqi1chdxwi0ycqy8wpiibsrd12")))

(define-public crate-snap7-sys-0.1.3 (c (n "snap7-sys") (v "0.1.3") (d (list (d (n "bindgen") (r "^0.49.2") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "17hfr4d04w2yc4pjxl1p85ymsjcryacz9k94qadnbgrd1alddk9h")))

(define-public crate-snap7-sys-0.1.4 (c (n "snap7-sys") (v "0.1.4") (d (list (d (n "bindgen") (r "^0.51.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1nvjxacb4rh3immhcdxgh883rk4dj2ijp6shq7dhakrg8js5agin")))

(define-public crate-snap7-sys-0.1.5 (c (n "snap7-sys") (v "0.1.5") (d (list (d (n "bindgen") (r "^0.69") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libloading") (r "^0.8") (d #t) (k 0)))) (h "1ny3q18r0lxg59z7pkyl3jzy5z9rmddb7hh7mxvp4q9q1fnc1zh5")))

