(define-module (crates-io sn ap snapdown) #:use-module (crates-io))

(define-public crate-snapdown-1.0.0 (c (n "snapdown") (v "1.0.0") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 0)) (d (n "datatest-stable") (r "^0.1.3") (d #t) (k 0)) (d (n "similar-asserts") (r "^1.4.2") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)))) (h "0pvnfbw9nc9p231778zblqxsmh3hr5q7shyyg2jyb9bwvc6ccdfp") (f (quote (("default"))))))

