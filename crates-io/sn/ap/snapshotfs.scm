(define-module (crates-io sn ap snapshotfs) #:use-module (crates-io))

(define-public crate-snapshotfs-0.1.0 (c (n "snapshotfs") (v "0.1.0") (d (list (d (n "clap") (r "^4.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "fuser") (r "^0.12") (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "1xnx36aa8l7mk2bshk25ci9ri123xfnjyczx1b670vap9c1wcswc")))

(define-public crate-snapshotfs-0.1.1 (c (n "snapshotfs") (v "0.1.1") (d (list (d (n "clap") (r "^4.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "fuser") (r "^0.12") (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "1q208hxhmd5ils9i2i98dayzc196zb9yahasvbkkmhnaxr2a2khv")))

(define-public crate-snapshotfs-0.2.0 (c (n "snapshotfs") (v "0.2.0") (d (list (d (n "clap") (r "^4.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "fuser") (r "^0.12") (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "05ms9a20bhc53dzp1qvw3m0bd6m1xvl54pk573xqgq180a8gjwqj")))

(define-public crate-snapshotfs-0.3.0 (c (n "snapshotfs") (v "0.3.0") (d (list (d (n "clap") (r "^4.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "fuser") (r "^0.12") (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0v2jskqgc4rzj26k845nng83wrn770gfrcblj1ynrh22xafc7njy")))

(define-public crate-snapshotfs-0.4.0 (c (n "snapshotfs") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^4.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "daemonize") (r "^0.5") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "fuser") (r "^0.12") (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0mi66ihlcv8cgl4kv280nm7s5ll9k2v4sn4swyfax4q2v9r7nrzm")))

