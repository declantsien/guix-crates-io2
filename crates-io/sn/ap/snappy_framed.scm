(define-module (crates-io sn ap snappy_framed) #:use-module (crates-io))

(define-public crate-snappy_framed-0.1.0 (c (n "snappy_framed") (v "0.1.0") (d (list (d (n "crc") (r "*") (d #t) (k 0)) (d (n "dribble") (r "*") (d #t) (k 2)) (d (n "log") (r "*") (d #t) (k 0)) (d (n "snappy") (r "*") (d #t) (k 0)))) (h "11yjpqqjfpfgrxk00mdilyjxr49crwalkww7n2niizrq2mr9p28z") (f (quote (("unstable"))))))

