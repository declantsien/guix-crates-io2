(define-module (crates-io sn ap snaptest) #:use-module (crates-io))

(define-public crate-snaptest-0.1.0 (c (n "snaptest") (v "0.1.0") (d (list (d (n "bincode") (r "^1.0.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.33") (d #t) (k 0)) (d (n "skittles") (r "^0.1.0") (d #t) (k 0)) (d (n "trail") (r "^0.1.0") (d #t) (k 0)))) (h "1c6r7fm9nb7fnxsj6lxypnki18lm57b7776dbfy6wki3rb8nkz05")))

(define-public crate-snaptest-0.2.0 (c (n "snaptest") (v "0.2.0") (d (list (d (n "bincode") (r "^1.0.0") (d #t) (k 0)) (d (n "diff") (r "^0.1.11") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.33") (d #t) (k 0)) (d (n "skittles") (r "^0.1.0") (d #t) (k 0)) (d (n "trail") (r "^0.1.0") (d #t) (k 0)))) (h "0vjmc06737131781zc06scgzxfzcrpbi7ppqs9zhdrfk3snb2xjq")))

