(define-module (crates-io sn ap snaplog) #:use-module (crates-io))

(define-public crate-snaplog-0.1.0 (c (n "snaplog") (v "0.1.0") (h "0cc6cbqmqg7024r6c36iz7svknwd6zp2gvxwg0labndqwnc6dj0w")))

(define-public crate-snaplog-0.2.0 (c (n "snaplog") (v "0.2.0") (h "1i97cgslii49si1ji16gkgqqxfcz6nqldbpin0qyl9pb8j815xgc")))

(define-public crate-snaplog-0.2.1 (c (n "snaplog") (v "0.2.1") (h "1r6n0c5rl50rnnfyqlirxi4b63wbsgddnk8kiql2k6chiv9v7cwi")))

(define-public crate-snaplog-0.3.0 (c (n "snaplog") (v "0.3.0") (h "1m78jkdv86psnvvj7s75pz2c9s38zrx2izan7zs547xbhj008gv0")))

(define-public crate-snaplog-0.3.1 (c (n "snaplog") (v "0.3.1") (h "0kcbmvizip4b7ymwr7nsc84mi3psl1nz1qx54b5z6l30m6lwjcqk")))

(define-public crate-snaplog-0.3.2 (c (n "snaplog") (v "0.3.2") (h "0m3bpln2gqgvpkrmrklak7vmsv4671ml1hnnm6y1zk379aij0zc7")))

(define-public crate-snaplog-0.4.0 (c (n "snaplog") (v "0.4.0") (h "014frzc6jhfx8j52vlkcxgqpz4spga2yr04c0wd2hw68hm4hf7z4")))

