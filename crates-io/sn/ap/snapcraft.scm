(define-module (crates-io sn ap snapcraft) #:use-module (crates-io))

(define-public crate-snapcraft-0.0.1 (c (n "snapcraft") (v "0.0.1") (h "1dmn6xfmx159c20yjcl4jix2j7afmb7qr551mys6qz8pqhqi5mpw") (y #t) (r "1.58")))

(define-public crate-snapcraft-0.0.2 (c (n "snapcraft") (v "0.0.2") (h "0s090d551d4mdapf2m8ah8i2qa13xgb2phd0ipz7m133fglf32xj") (y #t) (r "1.58")))

(define-public crate-snapcraft-0.0.3 (c (n "snapcraft") (v "0.0.3") (h "1qyniv9132g0njwk9fw0mgkq7vcsyagnxd2a22vg8c628imwzvnc") (y #t) (r "1.58")))

(define-public crate-snapcraft-0.0.4 (c (n "snapcraft") (v "0.0.4") (h "0j80c9lr0qqqrhg0wkjkqn0dj3kr3ac2db5daz3bqvfafj4qpqig") (y #t) (r "1.58")))

(define-public crate-snapcraft-0.0.5 (c (n "snapcraft") (v "0.0.5") (h "1qpdsdb70xkcyjry3qnbi2jmac89d88dcxg9xahin552irha91md") (y #t) (r "1.58")))

(define-public crate-snapcraft-0.0.6 (c (n "snapcraft") (v "0.0.6") (h "1zca98q3zgwy3kcx2719rac9pw95349n4i2g0mjwg6qgll8izbsj") (y #t) (r "1.58")))

(define-public crate-snapcraft-0.0.7 (c (n "snapcraft") (v "0.0.7") (h "14z7mj08y8q1ibln2zfz4s74h413xiswcy4w8x180gy8gjzswydz") (y #t) (r "1.58")))

(define-public crate-snapcraft-0.1.0 (c (n "snapcraft") (v "0.1.0") (h "1wdp982s9zg3dgkihidih5mc3pzld6jfp0yv5xc95px5mq7l3rf7") (r "1.58")))

(define-public crate-snapcraft-0.2.0 (c (n "snapcraft") (v "0.2.0") (d (list (d (n "serial_test") (r "^1") (d #t) (k 2)))) (h "1ygix54wqi53781bwp9q6b1xbvrip6vc2f2pxs8wyj12j2y5h9aw") (r "1.58")))

(define-public crate-snapcraft-0.3.0 (c (n "snapcraft") (v "0.3.0") (d (list (d (n "serial_test") (r "^1") (d #t) (k 2)))) (h "01fip5giy0bzcn6djw90rjzj32mfvs7v6bfbc7i7qmivr9i3kw46") (r "1.58")))

(define-public crate-snapcraft-0.3.1 (c (n "snapcraft") (v "0.3.1") (d (list (d (n "serial_test") (r "^3") (d #t) (k 2)))) (h "1qv7w3yp6mgf3y6zm20b287v82fck2s8w4ydp5csrxdspmp3n5br") (r "1.58")))

(define-public crate-snapcraft-0.3.2 (c (n "snapcraft") (v "0.3.2") (d (list (d (n "serial_test") (r "^3") (d #t) (k 2)))) (h "0il6ssiyqal4rq2kiv0vbfc8lsglmxbkhpqysbcxxmynq04cpsdf") (r "1.58")))

(define-public crate-snapcraft-0.3.3 (c (n "snapcraft") (v "0.3.3") (d (list (d (n "serial_test") (r "^3") (d #t) (k 2)))) (h "0bj59bizr1jplbqjjlihjdi568qlndznshc2f7a5ijf8f50hbprz") (r "1.58")))

(define-public crate-snapcraft-0.3.4 (c (n "snapcraft") (v "0.3.4") (d (list (d (n "serial_test") (r "^3") (d #t) (k 2)))) (h "0944bgdvafi723w9jn1g2zim79ryblzhlnmbd4lhvk3wk7x11ywd") (r "1.58")))

(define-public crate-snapcraft-0.3.5 (c (n "snapcraft") (v "0.3.5") (d (list (d (n "serial_test") (r "^3") (d #t) (k 2)))) (h "0ayz85442c8kwxx20k7nk9cy3hixlhzzi2hj3bvv9xrklzy4nl95") (r "1.58")))

(define-public crate-snapcraft-0.4.0 (c (n "snapcraft") (v "0.4.0") (d (list (d (n "serial_test") (r "^3") (d #t) (k 2)))) (h "1hrfxghx0k99i3p0x3xwwc3snjaj2fv47hkg08pa0kkxaz7q26nc") (r "1.58")))

