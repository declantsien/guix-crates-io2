(define-module (crates-io sn ap snappy_sc) #:use-module (crates-io))

(define-public crate-snappy_sc-0.1.2 (c (n "snappy_sc") (v "0.1.2") (d (list (d (n "image") (r "^0.23") (d #t) (k 0)) (d (n "metal") (r "^0.24.0") (d #t) (t "cfg(macos)") (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)) (d (n "webp") (r "^0.2.2") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("minwindef" "windef" "wingdi" "winuser"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0cwlvx9m988rsg51blg7c97a0nwhvx1s129qmx8a0w8mwsf946c8")))

