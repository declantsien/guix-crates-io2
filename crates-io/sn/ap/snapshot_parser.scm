(define-module (crates-io sn ap snapshot_parser) #:use-module (crates-io))

(define-public crate-snapshot_parser-0.1.0 (c (n "snapshot_parser") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.133") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.74") (d #t) (k 0)))) (h "0gg0l7s4ln7hsawd85pgmqzv8n0yhd6mz18bx26j6hc2k20f2h1i")))

