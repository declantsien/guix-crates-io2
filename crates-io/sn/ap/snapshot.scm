(define-module (crates-io sn ap snapshot) #:use-module (crates-io))

(define-public crate-snapshot-0.1.0 (c (n "snapshot") (v "0.1.0") (d (list (d (n "pretty_assertions") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.7") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.8") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.2") (d #t) (k 0)) (d (n "snapshot-proc-macro") (r "^0.1") (d #t) (k 0)))) (h "06cn2xfnj4wfn8dgak7bq8a5y24h7kqdk019md5rmwa60l77sw17")))

