(define-module (crates-io sn ap snappy) #:use-module (crates-io))

(define-public crate-snappy-0.2.0 (c (n "snappy") (v "0.2.0") (d (list (d (n "libc") (r "^0.1.7") (d #t) (k 0)))) (h "0dbwk60andsrf2adarnaynz5sac68d75m1ki59xyddji8q7jdkx6")))

(define-public crate-snappy-0.3.0 (c (n "snappy") (v "0.3.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0bwal9yiidsnd233h6irh00phm681ilwimb4ysnl9699qf2hrmqp")))

(define-public crate-snappy-0.4.0 (c (n "snappy") (v "0.4.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "snappy-sys") (r "^0.1") (d #t) (k 0)))) (h "1cav336iz7zmi1l8sk8y2lfafawnzshiavg40xm17jrgkv9v2w5m")))

