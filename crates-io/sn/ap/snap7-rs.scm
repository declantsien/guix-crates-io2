(define-module (crates-io sn ap snap7-rs) #:use-module (crates-io))

(define-public crate-snap7-rs-1.142.0 (c (n "snap7-rs") (v "1.142.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "cc") (r "^1") (d #t) (k 1)))) (h "1vaa36r347lhhcq8f1mg9yx1p17l3ryjxhlc4z1w8bl32w7fdkc0")))

(define-public crate-snap7-rs-1.142.1 (c (n "snap7-rs") (v "1.142.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "cc") (r "^1") (d #t) (k 1)))) (h "0y2v064m8scgw51n5icizm3gvn336gprllaxq1zam1c94b2spj5a")))

