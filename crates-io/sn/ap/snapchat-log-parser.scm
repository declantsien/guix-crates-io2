(define-module (crates-io sn ap snapchat-log-parser) #:use-module (crates-io))

(define-public crate-snapchat-log-parser-0.1.2 (c (n "snapchat-log-parser") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.132") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.73") (d #t) (k 0)))) (h "01mrzq10w4vs1anpsadsbngys7n4f57yq6fvm64vnnv08s1jz1xb")))

(define-public crate-snapchat-log-parser-0.2.0 (c (n "snapchat-log-parser") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.132") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.73") (d #t) (k 0)) (d (n "serde_with") (r "^1.11.0") (d #t) (k 0)))) (h "0pgkgfcfc820wss3600jq0399zin9r9nhd7vfzxhxlrymjdawr10")))

(define-public crate-snapchat-log-parser-0.2.1 (c (n "snapchat-log-parser") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.132") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.73") (d #t) (k 0)) (d (n "serde_with") (r "^1.11.0") (d #t) (k 0)))) (h "0y122bp1c9ss7cpvrig5pg75xggybdmc4xlvj64q8hgnwbs0bkdk")))

(define-public crate-snapchat-log-parser-0.3.0 (c (n "snapchat-log-parser") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.132") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.73") (d #t) (k 0)) (d (n "serde_with") (r "^1.11.0") (d #t) (k 0)))) (h "1lhjf8vfp8spn6i7n2bpz5ynxl1asnvknxwwqarqrip00srn9068")))

(define-public crate-snapchat-log-parser-0.4.0 (c (n "snapchat-log-parser") (v "0.4.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.132") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.73") (d #t) (k 0)) (d (n "serde_with") (r "^1.11.0") (d #t) (k 0)))) (h "0mjgasy4gvvrncb2wpa5rsprjraia262yjv4hsc9vicj5q7nj4q0")))

