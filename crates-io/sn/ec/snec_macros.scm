(define-module (crates-io sn ec snec_macros) #:use-module (crates-io))

(define-public crate-snec_macros-0.1.0 (c (n "snec_macros") (v "0.1.0") (d (list (d (n "fehler") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.4") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1b62qn651qxhb700f975sjahpmk3h655x778fafj4ayjvxg8ni4n")))

(define-public crate-snec_macros-1.0.0 (c (n "snec_macros") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1cfpwkh6ddm340am4q01dz2ksn2qsdjfmnywdnpkhag4c51sxmif")))

