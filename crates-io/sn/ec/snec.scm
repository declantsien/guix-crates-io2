(define-module (crates-io sn ec snec) #:use-module (crates-io))

(define-public crate-snec-0.1.0 (c (n "snec") (v "0.1.0") (d (list (d (n "snec_macros") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0ncjbji4l81rky43n62mhnkjicijpfm666zwq55cqaxid008l5aq") (f (quote (("std") ("macros" "snec_macros") ("default" "std" "macros"))))))

(define-public crate-snec-1.0.0 (c (n "snec") (v "1.0.0") (d (list (d (n "snec_macros") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1h7lblvf479pn8riz7inn2085cizm5kzwj89b5nkjwy5limv71xs") (f (quote (("std") ("macros" "snec_macros") ("default" "std" "macros"))))))

