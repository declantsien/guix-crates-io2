(define-module (crates-io sn uf snuffles) #:use-module (crates-io))

(define-public crate-snuffles-0.1.0 (c (n "snuffles") (v "0.1.0") (d (list (d (n "cgmath") (r "^0.18") (d #t) (k 0)) (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "pollster") (r "^0.2") (d #t) (k 0)) (d (n "wgpu") (r "^0.12") (d #t) (k 0)) (d (n "winit") (r "^0.26") (d #t) (k 0)))) (h "0cajw8ivwwjysdx8s7fcf7ykb6hy3l0g8dsp8iqzq61s4r8s6cbs")))

