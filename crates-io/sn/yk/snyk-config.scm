(define-module (crates-io sn yk snyk-config) #:use-module (crates-io))

(define-public crate-snyk-config-0.1.0 (c (n "snyk-config") (v "0.1.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1ap4bwl680ra4f54iiivgyr3qy4favik5bysi2mjc3k6mqz4ywvi")))

(define-public crate-snyk-config-0.2.0 (c (n "snyk-config") (v "0.2.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "10h2ms4ibx46nw1six3c5g0vylfz74708kq2z39ich3hjsp501fm")))

