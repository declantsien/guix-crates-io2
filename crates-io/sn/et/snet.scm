(define-module (crates-io sn et snet) #:use-module (crates-io))

(define-public crate-snet-0.1.0 (c (n "snet") (v "0.1.0") (d (list (d (n "bus") (r "^2.0.0") (d #t) (k 0)) (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "serialport") (r "^3.0.0") (d #t) (k 0)))) (h "08lpw2ixmz1hawl5icf63aiij4qhxvsnz7v5gz95iw3zhvw8lp3n")))

(define-public crate-snet-0.1.1 (c (n "snet") (v "0.1.1") (d (list (d (n "bus") (r "^2.0.0") (d #t) (k 0)) (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "serialport") (r "^3.0.0") (d #t) (k 0)))) (h "1gd7y6gcrls38yzavn40zra5ljmicdlx3160vqx8sv550v0xrbih")))

(define-public crate-snet-0.2.0 (c (n "snet") (v "0.2.0") (d (list (d (n "bus") (r "^2.0.0") (d #t) (k 0)) (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serialport") (r "^3.0.0") (d #t) (k 0)))) (h "082ifink46jjmzw6dx1lzqci1k3dj6wmri1gn0bkvrjx8gwgrg1j")))

