(define-module (crates-io sn dc sndcld) #:use-module (crates-io))

(define-public crate-sndcld-0.1.0 (c (n "sndcld") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.6") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.56") (d #t) (k 0)))) (h "1fylkypc8kkj0jfxmgcgknzy54wnmn3gqppywaixzx5hgpphxrz3")))

(define-public crate-sndcld-0.2.0 (c (n "sndcld") (v "0.2.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.6") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.56") (d #t) (k 0)))) (h "1wjn3v87f4kc8hcs8brnadlbw91wahcy8qhfg24rmirrfzxnpdv4")))

(define-public crate-sndcld-0.2.1 (c (n "sndcld") (v "0.2.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.6") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.56") (d #t) (k 0)))) (h "0mknvs41386254n3hh65hab1yr096s4kwybr0a974yxi1xz80d01")))

