(define-module (crates-io sn af snafu-derive) #:use-module (crates-io))

(define-public crate-snafu-derive-0.1.0 (c (n "snafu-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4.26") (d #t) (k 0)) (d (n "quote") (r "^0.6.11") (d #t) (k 0)) (d (n "syn") (r "^0.15.26") (f (quote ("full"))) (d #t) (k 0)))) (h "0y5xi2wz5x3ljybyncy3r6gjml9h19bjjrng9dlqq7pnsynq6b5k")))

(define-public crate-snafu-derive-0.1.1 (c (n "snafu-derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^0.4.26") (d #t) (k 0)) (d (n "quote") (r "^0.6.11") (d #t) (k 0)) (d (n "syn") (r "^0.15.26") (f (quote ("full"))) (d #t) (k 0)))) (h "0kgb6pnzrhnkkz4ap4fxg7ixvirmgdkk59rqyqqkb3z57k6n5hzx")))

(define-public crate-snafu-derive-0.1.2 (c (n "snafu-derive") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^0.4.26") (d #t) (k 0)) (d (n "quote") (r "^0.6.11") (d #t) (k 0)) (d (n "syn") (r "^0.15.26") (f (quote ("full"))) (d #t) (k 0)))) (h "166pqz240ha97w2k3wbwcw8kdj2cgipkdbp0yik312gc31kdix0x") (f (quote (("unstable_display_attribute") ("rust_1_30"))))))

(define-public crate-snafu-derive-0.1.3 (c (n "snafu-derive") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15.19") (f (quote ("full"))) (d #t) (k 0)))) (h "0jdrg93s11axmpj3d7qbj3z0xpv1cl858m824vgbknmcx8kk8ibj") (f (quote (("unstable_display_attribute") ("rust_1_30") ("backtraces"))))))

(define-public crate-snafu-derive-0.1.4 (c (n "snafu-derive") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15.19") (f (quote ("full"))) (d #t) (k 0)))) (h "0ddibz6pjy6pf2sdqj586zkjrn4ipma30bcqf5sfbf8vy1n78b0c") (f (quote (("unstable_display_attribute") ("rust_1_30") ("backtraces"))))))

(define-public crate-snafu-derive-0.1.5 (c (n "snafu-derive") (v "0.1.5") (d (list (d (n "compiletest_rs") (r "^0.3.18") (f (quote ("stable" "tmp"))) (d #t) (k 2)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15.19") (f (quote ("full"))) (d #t) (k 0)))) (h "114yii8l6x0as5pfin82hcvyviqj8ivwlnzciw93sn4j2pds1mj7") (f (quote (("unstable_display_attribute") ("rust_1_30") ("backtraces"))))))

(define-public crate-snafu-derive-0.1.6 (c (n "snafu-derive") (v "0.1.6") (d (list (d (n "compiletest_rs") (r "^0.3.18") (f (quote ("stable" "tmp"))) (d #t) (k 2)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15.19") (f (quote ("full"))) (d #t) (k 0)))) (h "0d2phvpjriwb80rdzf18blm0zjdfc72f6pil6519ra9hd7pi42n8") (f (quote (("unstable_display_attribute") ("rust_1_30") ("backtraces"))))))

(define-public crate-snafu-derive-0.1.7 (c (n "snafu-derive") (v "0.1.7") (d (list (d (n "compiletest_rs") (r "^0.3.18") (f (quote ("stable" "tmp"))) (d #t) (k 2)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15.19") (f (quote ("full"))) (d #t) (k 0)))) (h "0rf786ijrgkgy83c6if3x0scwk1h4arzk8v9mqd9i49wvjk2rwbb") (f (quote (("unstable_display_attribute") ("rust_1_30") ("backtraces"))))))

(define-public crate-snafu-derive-0.1.8 (c (n "snafu-derive") (v "0.1.8") (d (list (d (n "compiletest_rs") (r "^0.3.18") (f (quote ("stable" "tmp"))) (d #t) (k 2)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15.19") (f (quote ("full"))) (d #t) (k 0)))) (h "1nqssafh72mpaqr8p20nb6z7mbxfwxra21rr9q6jz5b6gcddxn6i") (f (quote (("unstable_display_attribute") ("rust_1_30") ("backtraces"))))))

(define-public crate-snafu-derive-0.1.9 (c (n "snafu-derive") (v "0.1.9") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15.19") (f (quote ("full"))) (d #t) (k 0)))) (h "17b4bxslxjjk8735b5jr2bd5gwb3cxcyzrhfjmgvmm5miz32fyaz") (f (quote (("unstable_display_attribute") ("rust_1_30") ("backtraces"))))))

(define-public crate-snafu-derive-0.2.0 (c (n "snafu-derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15.19") (f (quote ("full"))) (d #t) (k 0)))) (h "1h0rzcw461lrg5nsq6fjjkx9h8n3qq78za7vkskkrb58cr4c7m13") (f (quote (("rust_1_30") ("backtraces"))))))

(define-public crate-snafu-derive-0.2.1 (c (n "snafu-derive") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15.19") (f (quote ("full"))) (d #t) (k 0)))) (h "07pk7hwkj19jr66qvph6f4kdmmav10n9qa3ma1wrb3gvkylny4f6") (f (quote (("rust_1_30") ("backtraces"))))))

(define-public crate-snafu-derive-0.2.2 (c (n "snafu-derive") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15.19") (f (quote ("full"))) (d #t) (k 0)))) (h "0bc04cznnisavlyw8wjlb9p1gaw9v0fsfc7syc0ah00ixv1x0c7m") (f (quote (("rust_1_30") ("backtraces"))))))

(define-public crate-snafu-derive-0.2.3 (c (n "snafu-derive") (v "0.2.3") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15.19") (f (quote ("full"))) (d #t) (k 0)))) (h "021v37kppqywfcsqnfkjpn3vx7x79igh54pph2br9yii5c66b5mz") (f (quote (("rust_1_30") ("backtraces"))))))

(define-public crate-snafu-derive-0.3.0 (c (n "snafu-derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15.19") (f (quote ("full"))) (d #t) (k 0)))) (h "0yvyp30w6ksdg6n2hh8ys0xd1f4alhnwwzvx3wnz28gg82dya9sx") (f (quote (("rust_1_30") ("backtraces"))))))

(define-public crate-snafu-derive-0.3.1 (c (n "snafu-derive") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15.19") (f (quote ("full"))) (d #t) (k 0)))) (h "17bv82qzyvis7k524k2ciqp3702paawd8q91zk4ghf9k4f3n7c28") (f (quote (("rust_1_30") ("backtraces"))))))

(define-public crate-snafu-derive-0.4.0 (c (n "snafu-derive") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15.19") (f (quote ("full"))) (d #t) (k 0)))) (h "0hviivjyk7fwdbpivg9ygm80y7p4idnkchkia5bqybrvm6dk2c8s") (f (quote (("rust_1_30") ("backtraces"))))))

(define-public crate-snafu-derive-0.4.1 (c (n "snafu-derive") (v "0.4.1") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15.19") (f (quote ("full"))) (d #t) (k 0)))) (h "1vllp2h2qb88br7l7zmp49585i0jcimv4gvfp85xg8id0jq8y5vq") (f (quote (("rust_1_30") ("backtraces"))))))

(define-public crate-snafu-derive-0.4.2 (c (n "snafu-derive") (v "0.4.2") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15.19") (f (quote ("full"))) (d #t) (k 0)))) (h "1663pjfvrpyhhwx22ir07q1k4sa4jan1p2x2s6xxsj9i9549cin0") (f (quote (("rust_1_30") ("backtraces"))))))

(define-public crate-snafu-derive-0.4.3 (c (n "snafu-derive") (v "0.4.3") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15.19") (f (quote ("full"))) (d #t) (k 0)))) (h "14wriqjh1ppdshjyv39frph79b52jp4y0a8phqqbaplp3mi9ngv1") (f (quote (("rust_1_30") ("backtraces"))))))

(define-public crate-snafu-derive-0.4.4 (c (n "snafu-derive") (v "0.4.4") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15.19") (f (quote ("full"))) (d #t) (k 0)))) (h "0spcmg8jvn330fkr1fxr6licl83nijzyi5i61rcql90ca3psll5z") (f (quote (("rust_1_30") ("backtraces"))))))

(define-public crate-snafu-derive-0.5.0 (c (n "snafu-derive") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15.19") (f (quote ("full"))) (d #t) (k 0)))) (h "1816bgy739n83mk7xyzmhkidpmvsg8g72rxlhdwgcwg472yr8kk2") (f (quote (("rust_1_30") ("backtraces"))))))

(define-public crate-snafu-derive-0.6.0 (c (n "snafu-derive") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1h6142828lidhkahs6svwab1q8ypd95pk3nskmy4axq5if636pjc") (f (quote (("unstable-backtraces-impl-std"))))))

(define-public crate-snafu-derive-0.6.1 (c (n "snafu-derive") (v "0.6.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "105f9r9gb3847jnrknnw4imypqs32kjj33fcf9gby6nkmz8v3ln6") (f (quote (("unstable-backtraces-impl-std"))))))

(define-public crate-snafu-derive-0.6.2 (c (n "snafu-derive") (v "0.6.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1qphv741zjd234fy7jr7qvj09fq9lzdqz77x09s2jgrjw2i5vixx") (f (quote (("unstable-backtraces-impl-std"))))))

(define-public crate-snafu-derive-0.6.3 (c (n "snafu-derive") (v "0.6.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0amw2l3w8jl0vf5haq9ydzfxgncxadr4q0x5znz9kynbaqcc9ldm") (f (quote (("unstable-backtraces-impl-std"))))))

(define-public crate-snafu-derive-0.6.4 (c (n "snafu-derive") (v "0.6.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "13dz3qii72ilvacwlpy4fsxdavdf3amdbl8i03zdb4anj13iz1mi") (f (quote (("unstable-backtraces-impl-std"))))))

(define-public crate-snafu-derive-0.6.5 (c (n "snafu-derive") (v "0.6.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "118mg2jl1asy7hv35gbp3pdvcwjrx2qm9qsydbcs2ki8dzz7bb5b") (f (quote (("unstable-backtraces-impl-std"))))))

(define-public crate-snafu-derive-0.6.6 (c (n "snafu-derive") (v "0.6.6") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1qcrkn42fqgzgvl75sk8kxcqa0hvsd30rz9jpjqax1ks9al2phqf") (f (quote (("unstable-backtraces-impl-std"))))))

(define-public crate-snafu-derive-0.6.7 (c (n "snafu-derive") (v "0.6.7") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "17myvp3l5d3i9y0ja2m3ljdl8h3bb3sy8p0nz7l1kj3zalbfl1yi") (f (quote (("unstable-backtraces-impl-std"))))))

(define-public crate-snafu-derive-0.6.8 (c (n "snafu-derive") (v "0.6.8") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1sa86qgjakxlgdv4i67yl2xmis9441lahxkh1zgsj101fbazgy7b") (f (quote (("unstable-backtraces-impl-std"))))))

(define-public crate-snafu-derive-0.6.9 (c (n "snafu-derive") (v "0.6.9") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1rfkf10q3sqj12jv9bxfiwvksh3z0q393641clz2z7x86a3l8wvh") (f (quote (("unstable-backtraces-impl-std"))))))

(define-public crate-snafu-derive-0.6.10 (c (n "snafu-derive") (v "0.6.10") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0nri7ma06g5kimpcdcm8359a55nmps5f3kcngy0j6bin7jhfy20m") (f (quote (("unstable-backtraces-impl-std"))))))

(define-public crate-snafu-derive-0.7.0-beta.0 (c (n "snafu-derive") (v "0.7.0-beta.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1z1v8p706fbf2ykb4rflvdjx56gqi62h9pd8wvp53yk3vhpsri9x") (f (quote (("unstable-backtraces-impl-std"))))))

(define-public crate-snafu-derive-0.7.0-beta.1 (c (n "snafu-derive") (v "0.7.0-beta.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1whk36lvzmm8nn25apikwyi2hyb1iq975v86iavas9pf5lrx3hnh") (f (quote (("unstable-backtraces-impl-std") ("rust_1_46"))))))

(define-public crate-snafu-derive-0.7.0-beta.2 (c (n "snafu-derive") (v "0.7.0-beta.2") (d (list (d (n "heck") (r "^0.3.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0iwjbjyldnk1a5i0nxzh7pjbgczy1qiiswv5aqwcqp3ns2jx9kfn") (f (quote (("unstable-backtraces-impl-std") ("rust_1_46"))))))

(define-public crate-snafu-derive-0.7.0 (c (n "snafu-derive") (v "0.7.0") (d (list (d (n "heck") (r "^0.3.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0x9shcbvbpl9fryrgzsqy1qngwr8hmiljmdwrb2igwcycsqfjzvs") (f (quote (("unstable-backtraces-impl-std") ("rust_1_46"))))))

(define-public crate-snafu-derive-0.7.1 (c (n "snafu-derive") (v "0.7.1") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1r95zrm2i6wh2zsddmzmchh6xa2nin38qj72sg7903a4jznjc2s1") (f (quote (("unstable-backtraces-impl-std") ("rust_1_46"))))))

(define-public crate-snafu-derive-0.7.2 (c (n "snafu-derive") (v "0.7.2") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0x2f153p479vhmz032ry77ff6rr70c38xcs5lfxfl55hn3ljj9bi") (f (quote (("unstable-provider-api") ("unstable-backtraces-impl-std") ("rust_1_61") ("rust_1_46") ("rust_1_39"))))))

(define-public crate-snafu-derive-0.7.3 (c (n "snafu-derive") (v "0.7.3") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0l6qzswgmc1z1r6isqb33x311d2ry73yvfmxm43bpbmbpgg9rrym") (f (quote (("unstable-provider-api") ("unstable-backtraces-impl-std") ("rust_1_61") ("rust_1_46") ("rust_1_39"))))))

(define-public crate-snafu-derive-0.7.4 (c (n "snafu-derive") (v "0.7.4") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1cl1vyxkwb0nbp223ryfxkzrzsrh3xydcqkgi8nnzhj5aaz3nns7") (f (quote (("unstable-provider-api") ("unstable-backtraces-impl-std") ("rust_1_61") ("rust_1_46") ("rust_1_39"))))))

(define-public crate-snafu-derive-0.7.5 (c (n "snafu-derive") (v "0.7.5") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1gzy9rzggs090zf7hfvgp4lm1glrmg9qzh796686jnq7bxk7j04r") (f (quote (("unstable-provider-api") ("unstable-backtraces-impl-std") ("rust_1_61") ("rust_1_46") ("rust_1_39"))))))

(define-public crate-snafu-derive-0.8.0 (c (n "snafu-derive") (v "0.8.0") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.52") (d #t) (k 0)) (d (n "quote") (r "^1.0.25") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1837y976zh0cn137srwfmdiwz59raj5xs7gnsqaszc9n2jbl8308") (f (quote (("unstable-provider-api") ("rust_1_61")))) (r "1.56")))

(define-public crate-snafu-derive-0.8.1 (c (n "snafu-derive") (v "0.8.1") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.52") (d #t) (k 0)) (d (n "quote") (r "^1.0.25") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0x2jidlb00pjbavkgqzkx5iq8bjmh4rnrfpwjc4h2dpc4s118la6") (f (quote (("unstable-provider-api") ("rust_1_61")))) (r "1.56")))

(define-public crate-snafu-derive-0.8.2 (c (n "snafu-derive") (v "0.8.2") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.52") (d #t) (k 0)) (d (n "quote") (r "^1.0.25") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0pyr3rdipfsr74hlk14q7bssls0qssr6rg0727xw5f5zvq8rkcdl") (f (quote (("unstable-provider-api") ("rust_1_61")))) (r "1.56")))

(define-public crate-snafu-derive-0.8.3 (c (n "snafu-derive") (v "0.8.3") (d (list (d (n "heck") (r ">=0.4, <0.6") (k 0)) (d (n "proc-macro2") (r "^1.0.52") (d #t) (k 0)) (d (n "quote") (r "^1.0.25") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1dbzdwcrvsrfwlkzjcpp2y5wijyavng47836h9yx206sd6k14j0s") (f (quote (("unstable-provider-api") ("rust_1_61")))) (r "1.56")))

