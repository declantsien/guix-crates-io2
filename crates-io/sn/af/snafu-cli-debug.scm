(define-module (crates-io sn af snafu-cli-debug) #:use-module (crates-io))

(define-public crate-snafu-cli-debug-0.1.0 (c (n "snafu-cli-debug") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4.30") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "snafu") (r "^0.4.1") (d #t) (k 2)) (d (n "syn") (r "^0.15.39") (d #t) (k 0)))) (h "16zbsi9qv37z8d5fa49rm3xsr0qrnmc04wgnfcmx08g0w2wa113z")))

(define-public crate-snafu-cli-debug-0.1.1 (c (n "snafu-cli-debug") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^0.4.30") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "snafu") (r "^0.5") (d #t) (k 2)) (d (n "syn") (r "^0.15.39") (d #t) (k 0)))) (h "132dmrz908aiwy073lqmm0vmhm2y0s1gvp34qngmd3mpnq3jw91l")))

