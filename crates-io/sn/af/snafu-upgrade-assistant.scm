(define-module (crates-io sn af snafu-upgrade-assistant) #:use-module (crates-io))

(define-public crate-snafu-upgrade-assistant-1.0.0 (c (n "snafu-upgrade-assistant") (v "1.0.0") (d (list (d (n "argh") (r "^0.1.4") (k 0)) (d (n "once_cell") (r "^1.8.0") (f (quote ("std"))) (k 0)) (d (n "regex") (r "^1.5.4") (f (quote ("std" "unicode-perl"))) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1.0.64") (f (quote ("std"))) (k 0)))) (h "0a7q54rpgz6cj4d5ab6zy3n167gm6khbzavx662fnz9n5761jkqa")))

