(define-module (crates-io sn af snafu-numbers) #:use-module (crates-io))

(define-public crate-snafu-numbers-0.1.0 (c (n "snafu-numbers") (v "0.1.0") (h "0iw836s8d0jziyk4j915kzvjnmc5cl5h8889ppgnhhdshb5afcp4")))

(define-public crate-snafu-numbers-0.1.1 (c (n "snafu-numbers") (v "0.1.1") (h "0ggpawd84xfjssm0jwqg5a8ggn8aciilhnmkrfahfhap39cj83i2")))

