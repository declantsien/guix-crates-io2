(define-module (crates-io sn ak snakeoil) #:use-module (crates-io))

(define-public crate-snakeoil-0.1.0 (c (n "snakeoil") (v "0.1.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.8") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "pyc") (r "^0.1.0") (d #t) (k 0)) (d (n "snakeoil-wasm") (r "^0.1.0") (d #t) (k 0)) (d (n "wasm-gen") (r "^0.1.3") (d #t) (k 0)))) (h "14n0h0ma6sw06fm12w9hcg7zy0cws4inf7bbbxkvq7wh9l52ivjj")))

