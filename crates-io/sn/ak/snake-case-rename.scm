(define-module (crates-io sn ak snake-case-rename) #:use-module (crates-io))

(define-public crate-snake-case-rename-0.0.1 (c (n "snake-case-rename") (v "0.0.1") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.3") (d #t) (k 0)))) (h "1bbxalyd2ra5hrbqvk6hjkarnd5yw4q3wpml6qh274h3hgm1i7hs")))

