(define-module (crates-io sn ak snake-tui-rs) #:use-module (crates-io))

(define-public crate-snake-tui-rs-0.1.0 (c (n "snake-tui-rs") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.23.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "04d3iy2vwr2ywggsfwc1clqa4vxvmkcn6qf5jr16cgs4vdp654ad")))

(define-public crate-snake-tui-rs-0.1.1 (c (n "snake-tui-rs") (v "0.1.1") (d (list (d (n "crossterm") (r "^0.23.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1pr467rz8f098xc0hfsbc9is030vv2y3g2izx8fqqg138xcryh2w")))

