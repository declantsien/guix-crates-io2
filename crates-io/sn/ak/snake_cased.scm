(define-module (crates-io sn ak snake_cased) #:use-module (crates-io))

(define-public crate-snake_cased-0.1.0 (c (n "snake_cased") (v "0.1.0") (d (list (d (n "snake_cased_derive") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "099d8vdnm3lry4dng4j0p720xxpjfxh12qf8a82v1mrzw6hwb1j5") (f (quote (("derive" "snake_cased_derive"))))))

