(define-module (crates-io sn ak snakeoil-wasm) #:use-module (crates-io))

(define-public crate-snakeoil-wasm-0.1.0 (c (n "snakeoil-wasm") (v "0.1.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "python_object") (r "^0.1.0") (d #t) (k 0)) (d (n "wasm-gen") (r "^0.1.3") (d #t) (k 0)) (d (n "wasmi") (r "^0.5.1") (d #t) (k 0)))) (h "0b2lpwq11i80n5ahyf6j8ln942fymc66gyszhy18624ca1ckwafr")))

