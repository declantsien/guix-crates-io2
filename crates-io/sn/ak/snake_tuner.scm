(define-module (crates-io sn ak snake_tuner) #:use-module (crates-io))

(define-public crate-snake_tuner-0.1.0 (c (n "snake_tuner") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "13r06rgy6zy6zlb63h9dhny5lgwqzmhcga6j4bmqzjz5wig9d91n")))

(define-public crate-snake_tuner-0.2.0 (c (n "snake_tuner") (v "0.2.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "18m2aq5qcx729k5cccqgv1ipi41xg1905dg72gaw9zx09bc7fx85")))

(define-public crate-snake_tuner-0.2.1 (c (n "snake_tuner") (v "0.2.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "11s4dnzmygkd80n973fjszks46q6kfha35sskssg4sfd97adhj2k")))

(define-public crate-snake_tuner-0.2.2 (c (n "snake_tuner") (v "0.2.2") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0yyi7myf2a1zxyd54jcwij57nwic8k0pxvjxz2533nzd6b5iln2z")))

(define-public crate-snake_tuner-0.3.0 (c (n "snake_tuner") (v "0.3.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1x00jcryl684wqp2m6hwm85av96kmy3ff71man5hnzbs2x082dq1")))

(define-public crate-snake_tuner-0.4.0 (c (n "snake_tuner") (v "0.4.0") (d (list (d (n "nalgebra") (r "^0.32.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "14cmwmw6rh87d1h8181cfsh6i9sgxf1ak5c0l5220gfx1hd1fw79")))

(define-public crate-snake_tuner-0.4.1 (c (n "snake_tuner") (v "0.4.1") (d (list (d (n "nalgebra") (r "^0.32.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "006xbqdyqj1a5skhyqrwmjmwaa1wcf34f0cga31rhjywy9riwk84")))

(define-public crate-snake_tuner-0.4.2 (c (n "snake_tuner") (v "0.4.2") (d (list (d (n "nalgebra") (r "^0.32.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1vpcgbjnzzzvv0bgp0bw6n1645xgsfmhpwkqa0x7mnrxdg1pwn8b")))

(define-public crate-snake_tuner-0.5.0 (c (n "snake_tuner") (v "0.5.0") (d (list (d (n "nalgebra") (r "^0.32.2") (f (quote ("rand"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0lhyvms3kvlyi0nlfr99xldkykp1f6nnsx9w6m98mv08kwgagxqf")))

(define-public crate-snake_tuner-0.5.1 (c (n "snake_tuner") (v "0.5.1") (d (list (d (n "nalgebra") (r "^0.32.2") (f (quote ("rand"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1gmpz3v57r1563887m25s0bkf528dn8nq77mapczilqiq8gnab6v")))

(define-public crate-snake_tuner-0.5.2 (c (n "snake_tuner") (v "0.5.2") (d (list (d (n "nalgebra") (r "^0.32.2") (f (quote ("rand"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "09711sak827pb5lwq2jy3r0ghlwzyv07v55h5kq5damvsrn6b52c")))

(define-public crate-snake_tuner-0.5.3 (c (n "snake_tuner") (v "0.5.3") (d (list (d (n "nalgebra") (r "^0.32.2") (f (quote ("rand"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "112xpc2a6s8m5zy3cwwi8x31d2ssl83wqr6vi1gw68xjzsa2887p")))

