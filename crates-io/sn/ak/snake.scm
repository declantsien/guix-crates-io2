(define-module (crates-io sn ak snake) #:use-module (crates-io))

(define-public crate-snake-0.1.0 (c (n "snake") (v "0.1.0") (d (list (d (n "pancurses") (r "^0.16.1") (d #t) (k 0)))) (h "0llk99c86y4lml338dkdpi7lvvg27zcsjggdrw6y1yi4rsn037r4")))

(define-public crate-snake-0.2.0 (c (n "snake") (v "0.2.0") (d (list (d (n "pancurses") (r "^0.16.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 0)))) (h "0warhh3943kshgs5blwvx1n4dwidqddpq6sp9h64pci2jx0hj7i8")))

(define-public crate-snake-0.3.0 (c (n "snake") (v "0.3.0") (d (list (d (n "pancurses") (r "^0.16.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 0)))) (h "1b7shkqw58n96d838cz3y8my16jil833d4k92cm0c4w1ccmamh23")))

(define-public crate-snake-0.3.1 (c (n "snake") (v "0.3.1") (d (list (d (n "pancurses") (r "^0.16.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 0)))) (h "0rf89xzy199sb7nn28s04li7my2p2asf4nd5vd4a4fq8bmdycd2n")))

