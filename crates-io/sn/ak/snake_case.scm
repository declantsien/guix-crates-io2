(define-module (crates-io sn ak snake_case) #:use-module (crates-io))

(define-public crate-snake_case-0.1.0 (c (n "snake_case") (v "0.1.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "02wjqjrgycsyjjplxqylf1sq9qhybdrppc1kmwxmjln2nin2cq0q")))

(define-public crate-snake_case-0.2.0 (c (n "snake_case") (v "0.2.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "110wy54qk4px9q68mahjzqsbfw8hq8ymkbdska8gjhb2rdrn1ril") (f (quote (("default"))))))

(define-public crate-snake_case-0.2.1 (c (n "snake_case") (v "0.2.1") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "10lraq7zrs1jbrjkik2mifdifgzjwgx07pq08l695gdsmh02y3x1") (f (quote (("default"))))))

(define-public crate-snake_case-0.3.0 (c (n "snake_case") (v "0.3.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "02mjmzlif46in2ppjpalw7gbzkqi76k455fnygsca601lldnsz4c") (f (quote (("default"))))))

(define-public crate-snake_case-0.3.1 (c (n "snake_case") (v "0.3.1") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1v4f132rk9wxiw8hb3kgnixirzr8kbfhg2lgsf4b85vbg02a0jfn") (f (quote (("default"))))))

