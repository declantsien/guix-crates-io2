(define-module (crates-io sn ak snake-rs) #:use-module (crates-io))

(define-public crate-snake-rs-0.1.0 (c (n "snake-rs") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "indexmap") (r "^1.3.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 0)) (d (n "sfml") (r "^0.15.1") (o #t) (d #t) (k 0)))) (h "1hwb49i7ahq0bqx8y8wc8xcvkqn4n605dkm4ij766qk0ji5yfiyl") (f (quote (("display" "sfml"))))))

(define-public crate-snake-rs-0.1.1 (c (n "snake-rs") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "indexmap") (r "^1.3.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 0)) (d (n "sfml") (r "^0.15.1") (o #t) (d #t) (k 0)))) (h "1v3qf6l02pw2d85r8lxa4skhv33q458jsspnlvw4m2nphy7fw1n7") (f (quote (("display" "sfml"))))))

(define-public crate-snake-rs-0.1.2 (c (n "snake-rs") (v "0.1.2") (d (list (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "indexmap") (r "^1.3.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 0)) (d (n "sfml") (r "^0.16.0") (o #t) (d #t) (k 0)))) (h "1ci9bk11c1lrnwxj1n4glq6vrzzk7d52b139kz4384miilpl6916") (f (quote (("display" "sfml"))))))

