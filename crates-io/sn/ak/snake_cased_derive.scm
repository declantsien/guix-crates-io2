(define-module (crates-io sn ak snake_cased_derive) #:use-module (crates-io))

(define-public crate-snake_cased_derive-0.1.0 (c (n "snake_cased_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1n8ca0qjdl8n1vv03h6jym6mlvq725y7y75dsvcwnacbk37bclk2") (y #t)))

(define-public crate-snake_cased_derive-0.1.1 (c (n "snake_cased_derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0iwwfwcjsjc4k8a9pfq9an252h3r67hg3khk565kl4nxhha9r19w")))

