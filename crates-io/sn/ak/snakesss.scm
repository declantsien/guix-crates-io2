(define-module (crates-io sn ak snakesss) #:use-module (crates-io))

(define-public crate-snakesss-0.1.0 (c (n "snakesss") (v "0.1.0") (d (list (d (n "freetype-rs") (r "^0.29.0") (d #t) (k 0)) (d (n "piston") (r "^0.53.0") (d #t) (k 0)) (d (n "piston2d-graphics") (r "^0.42.0") (d #t) (k 0)) (d (n "piston2d-opengl_graphics") (r "^0.81.0") (d #t) (k 0)) (d (n "pistoncore-glutin_window") (r "^0.69.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "0qwis1xcb617kli6szv35q1lrh821l90k7l21gnci82fnkyazgb6")))

(define-public crate-snakesss-0.1.1 (c (n "snakesss") (v "0.1.1") (d (list (d (n "freetype-rs") (r "^0.29.0") (d #t) (k 0)) (d (n "piston") (r "^0.53.0") (d #t) (k 0)) (d (n "piston2d-graphics") (r "^0.42.0") (d #t) (k 0)) (d (n "piston2d-opengl_graphics") (r "^0.81.0") (d #t) (k 0)) (d (n "pistoncore-glutin_window") (r "^0.69.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "1h44h2i02ssfrl5w3iyj99kbh0s680c1jsfjcrd1j07dppq4wk07")))

(define-public crate-snakesss-0.1.2 (c (n "snakesss") (v "0.1.2") (d (list (d (n "freetype-rs") (r "^0.29.0") (d #t) (k 0)) (d (n "piston") (r "^0.53.0") (d #t) (k 0)) (d (n "piston2d-graphics") (r "^0.42.0") (d #t) (k 0)) (d (n "piston2d-opengl_graphics") (r "^0.81.0") (d #t) (k 0)) (d (n "pistoncore-glutin_window") (r "^0.69.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "011ck2mv3cmf1msmgsi1qb34w3yl3snhw16qp9avxcr5ayhillhg")))

