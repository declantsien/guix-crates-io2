(define-module (crates-io sn ak snake_helper) #:use-module (crates-io))

(define-public crate-snake_helper-0.1.0 (c (n "snake_helper") (v "0.1.0") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 2)) (d (n "miette") (r "^5.5.0") (d #t) (k 2)) (d (n "unwrap_or") (r "^1.0.0") (d #t) (k 0)))) (h "1lbcsszdf5j6xj529acdaqi844abdagg0qrwdr16p307ffhjz05a")))

