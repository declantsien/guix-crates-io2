(define-module (crates-io sn ak snakers) #:use-module (crates-io))

(define-public crate-snakers-0.1.0 (c (n "snakers") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.26.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "tui") (r "^0.20.1") (d #t) (k 0) (p "ratatui")))) (h "050xmjbfc71g1zh1j1lc46jpki21xl7bh3xc2rpbr5yy8xfhniyf")))

