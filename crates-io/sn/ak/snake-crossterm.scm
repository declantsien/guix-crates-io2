(define-module (crates-io sn ak snake-crossterm) #:use-module (crates-io))

(define-public crate-snake-crossterm-1.0.0 (c (n "snake-crossterm") (v "1.0.0") (d (list (d (n "crossterm") (r "^0.23.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1br46qnnsnlm7jsw54qj10xig092i22ji25kp32m71h4khls5xfq")))

(define-public crate-snake-crossterm-1.1.0 (c (n "snake-crossterm") (v "1.1.0") (d (list (d (n "crossterm") (r "^0.23.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1f2n2ga2w5913fdxj73abyn6dr66kg7hv1wbnhibs9ykm36hbcn9")))

