(define-module (crates-io sn es snes_address) #:use-module (crates-io))

(define-public crate-snes_address-0.1.0 (c (n "snes_address") (v "0.1.0") (d (list (d (n "thiserror") (r "1.*") (d #t) (k 0)))) (h "1vx8xrnaday6l998c8srv0ijrf167kw6k8gnh9w5q2n0m0s8h6vg")))

(define-public crate-snes_address-0.1.1 (c (n "snes_address") (v "0.1.1") (d (list (d (n "thiserror") (r "1.*") (d #t) (k 0)))) (h "004nb48r0mcvqfagqyw9p1n2drf7ppk5v07axm9cbf68n8jmvv0m")))

