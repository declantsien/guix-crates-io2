(define-module (crates-io sn es snes-apu) #:use-module (crates-io))

(define-public crate-snes-apu-0.1.0 (c (n "snes-apu") (v "0.1.0") (d (list (d (n "emu") (r "^0.1.0") (d #t) (k 0)))) (h "0yik51xl62ba3calir77dvpvvd77l4q5ws78l7f6db4m65pp87lm")))

(define-public crate-snes-apu-0.1.1 (c (n "snes-apu") (v "0.1.1") (d (list (d (n "emu") (r "^0.1.0") (d #t) (k 0)))) (h "13lmrw12srhbbfhxf93h72cak8b2fvd106imhc46xmggjvvxmfrj")))

(define-public crate-snes-apu-0.1.2 (c (n "snes-apu") (v "0.1.2") (d (list (d (n "emu") (r "^0.1.0") (d #t) (k 0)))) (h "1c7ypg337fljnm20v08h46l3yfd96xfn54754jlb21ya4kmp6dp5")))

(define-public crate-snes-apu-0.1.3 (c (n "snes-apu") (v "0.1.3") (d (list (d (n "emu") (r "^0.1.0") (d #t) (k 0)) (d (n "spc") (r "^0.1.0") (d #t) (k 0)))) (h "00biv1fzxkv9zrxc91xm4kay736g8pcmjwsz6yp5yj9gm1fgxb27")))

(define-public crate-snes-apu-0.1.4 (c (n "snes-apu") (v "0.1.4") (d (list (d (n "emu") (r "^0.1.0") (d #t) (k 0)) (d (n "spc") (r "^0.1.0") (d #t) (k 0)))) (h "07lkjg2ah6h34llgd6xmvygiafz15ybf0mi6ljl475p61mi7i3f0")))

(define-public crate-snes-apu-0.1.5 (c (n "snes-apu") (v "0.1.5") (d (list (d (n "emu") (r "^0.1.0") (d #t) (k 0)) (d (n "spc") (r "^0.1.0") (d #t) (k 0)))) (h "1hb4j9kdg6a274wf7rq3lcvrwgl2h4hi6vksj79p8qm7nhdmvfm5")))

(define-public crate-snes-apu-0.1.6 (c (n "snes-apu") (v "0.1.6") (d (list (d (n "emu") (r "^0.1.1") (d #t) (k 0)) (d (n "spc") (r "^0.1.0") (d #t) (k 0)))) (h "0b1likckiy6vnd636q5mkpwzgkci3bqmyn3f8cbpywlircxz2nl3")))

(define-public crate-snes-apu-0.1.7 (c (n "snes-apu") (v "0.1.7") (d (list (d (n "emu") (r "^0.1.1") (d #t) (k 0)) (d (n "spc") (r "^0.1.0") (d #t) (k 0)))) (h "1mycma2mkm6vacp4q5wk75li98iyrsfi43lqxq59022kp0id5mig")))

(define-public crate-snes-apu-0.1.8 (c (n "snes-apu") (v "0.1.8") (d (list (d (n "emu") (r "^0.1.1") (d #t) (k 0)) (d (n "spc") (r "^0.1.0") (d #t) (k 0)))) (h "0b4z8ym1571rgh213ghrw8am3vmgivms9qbrd3mcm6vzj90y3i8m")))

(define-public crate-snes-apu-0.1.9 (c (n "snes-apu") (v "0.1.9") (d (list (d (n "emu") (r "^0.1.1") (d #t) (k 0)) (d (n "spc") (r "^0.1.0") (d #t) (k 0)))) (h "0n01lm428fk4i6mrk2321a26gfdfzmf3bbz9ghbap0n1y28q5dn3")))

(define-public crate-snes-apu-0.1.10 (c (n "snes-apu") (v "0.1.10") (d (list (d (n "emu") (r "^0.1.1") (d #t) (k 0)) (d (n "spc") (r "^0.1.0") (d #t) (k 0)))) (h "101aqccq2lvqgi9h36b5hbmhkxri5frr7kjbwxfzwcz6fnx2j8p9")))

(define-public crate-snes-apu-0.1.11 (c (n "snes-apu") (v "0.1.11") (d (list (d (n "emu") (r "^0.1.2") (d #t) (k 2)) (d (n "spc") (r "^0.1.0") (d #t) (k 0)))) (h "02zsm7dklldi5zp5x7lcz76amvmbm1m74f5p5nh00ii44nqnvi14")))

(define-public crate-snes-apu-0.1.12 (c (n "snes-apu") (v "0.1.12") (d (list (d (n "emu") (r "^0.1.3") (d #t) (k 2)) (d (n "spc") (r "^0.1.0") (d #t) (k 0)))) (h "1jgn8riryc2dsfx42zwffl1iarm7m876j1cp5b23qqk9sxqw53a8")))

