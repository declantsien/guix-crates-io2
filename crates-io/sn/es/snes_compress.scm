(define-module (crates-io sn es snes_compress) #:use-module (crates-io))

(define-public crate-snes_compress-0.1.0 (c (n "snes_compress") (v "0.1.0") (d (list (d (n "thiserror") (r "1.*") (d #t) (k 0)))) (h "0ng590lvvvzc9nbrmp187hljljibb502vz1dy01m34f6kv9458mx")))

(define-public crate-snes_compress-0.1.1 (c (n "snes_compress") (v "0.1.1") (d (list (d (n "thiserror") (r "1.*") (d #t) (k 0)))) (h "1k66zfxnhlziidkjly3q4c96a1zn2fmbjzflhpawpgq2l3w7l4ky")))

(define-public crate-snes_compress-0.1.2 (c (n "snes_compress") (v "0.1.2") (d (list (d (n "thiserror") (r "1.*") (d #t) (k 0)))) (h "0q7si43kgvjf6hdgnk312ivw4q1d6n9d5s1hggj3qvgcz12a4dji")))

(define-public crate-snes_compress-0.1.3 (c (n "snes_compress") (v "0.1.3") (d (list (d (n "thiserror") (r "1.*") (d #t) (k 0)))) (h "1jwlahkzn44wh3hxzps653avkcq4vkpd2fzdj5pxmvyirgs991lk")))

(define-public crate-snes_compress-0.1.4 (c (n "snes_compress") (v "0.1.4") (d (list (d (n "thiserror") (r "1.*") (d #t) (k 0)))) (h "1iwdmcmz6615chjx55drlbwlrcsc2086dymriyg3nx4fl2v9zj51")))

