(define-module (crates-io sn s- sns-push-notifications) #:use-module (crates-io))

(define-public crate-sns-push-notifications-0.1.0 (c (n "sns-push-notifications") (v "0.1.0") (d (list (d (n "rusoto_core") (r "^0.36.0") (d #t) (k 0)) (d (n "rusoto_sns") (r "^0.36.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.87") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.87") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.38") (d #t) (k 0)))) (h "0n2s7yvwpj489g3ahpfsp9mzhx9nry6lnfsindlqjdc0ylm3a8dv")))

(define-public crate-sns-push-notifications-0.1.1 (c (n "sns-push-notifications") (v "0.1.1") (d (list (d (n "rusoto_core") (r "^0.36.0") (d #t) (k 0)) (d (n "rusoto_sns") (r "^0.36.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.87") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.87") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.38") (d #t) (k 0)))) (h "04dmcjrrfb0jm7y81xgj7pln5ssq5sx3i615gk79rrfsm86zsh37")))

