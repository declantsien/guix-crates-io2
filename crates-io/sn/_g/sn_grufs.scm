(define-module (crates-io sn _g sn_grufs) #:use-module (crates-io))

(define-public crate-sn_grufs-1.0.0 (c (n "sn_grufs") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "curl") (r "^0.4") (d #t) (k 0)) (d (n "directories") (r "^3.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.8") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "sled") (r "^0.34") (d #t) (k 0)) (d (n "tokio") (r "^1.6") (f (quote ("full"))) (d #t) (k 0)))) (h "0knnb36496skk2ma874hn8523888dkq1hzmlvjzf6lm8x3syqfr9")))

