(define-module (crates-io sn or snor) #:use-module (crates-io))

(define-public crate-snor-0.1.0 (c (n "snor") (v "0.1.0") (h "0azjnwipczl5hzrznjwb3k6n1kb7610dywshbsvr0n0id9nmms5s")))

(define-public crate-snor-0.1.1 (c (n "snor") (v "0.1.1") (h "12d58il0m2bj4bz1ch0dn4dm4sv136gafvnqf1jw6jn9vdn80a60")))

