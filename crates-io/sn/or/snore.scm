(define-module (crates-io sn or snore) #:use-module (crates-io))

(define-public crate-snore-0.1.0 (c (n "snore") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)))) (h "0vvhz8q5f9b1n7xkhqxgfl56x2gjmsdr7vxkns4dbb9ld7rilfy5")))

(define-public crate-snore-0.1.1 (c (n "snore") (v "0.1.1") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)))) (h "14wrshyfhy5x2sjkcas04ncwq0pjhnbshmf0xf7v9hsrh87p3rc7")))

