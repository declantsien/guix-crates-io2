(define-module (crates-io sn dj sndjvu_format) #:use-module (crates-io))

(define-public crate-sndjvu_format-0.0.0 (c (n "sndjvu_format") (v "0.0.0") (h "1nmfmdfkx84qz6h1pqb6jpsgfnfr3xhvvv8r2zf5gsjz5h8wwyd3") (y #t)))

(define-public crate-sndjvu_format-0.1.0 (c (n "sndjvu_format") (v "0.1.0") (h "1gmqwilz16216n7qw04caj3fmx1ykfkxk3z522lc68nsvgb7fixg") (f (quote (("std") ("backtrace" "std")))) (r "1.65.0")))

(define-public crate-sndjvu_format-0.1.1 (c (n "sndjvu_format") (v "0.1.1") (h "0mm8369vzgp6mhk7hp9lpz1wvpgi0r979zxzfni0s70dghsp81hh") (f (quote (("std") ("backtrace" "std")))) (r "1.65.0")))

(define-public crate-sndjvu_format-0.1.2 (c (n "sndjvu_format") (v "0.1.2") (h "13pglxwn3da7618hhgingd5zh8zrigxm4lsz2wxfbzg0fdpiixm2") (f (quote (("std") ("backtrace" "std")))) (r "1.65.0")))

(define-public crate-sndjvu_format-0.2.0 (c (n "sndjvu_format") (v "0.2.0") (h "06vwb7xdwmhybkhyzyx2mvcjaxr5jd9ngi6hq64pi546n8ybyjca") (f (quote (("std") ("backtrace" "std")))) (r "1.65.0")))

