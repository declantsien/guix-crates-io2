(define-module (crates-io le as least) #:use-module (crates-io))

(define-public crate-least-0.1.0 (c (n "least") (v "0.1.0") (h "1ql1s5ymz9iypzd8i61li3apjslsj5yslm0bxr6m4r6isajhvd33") (y #t)))

(define-public crate-least-0.2.0 (c (n "least") (v "0.2.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "crossterm") (r "^0.19.0") (d #t) (k 0)))) (h "0k1dv2b4k90pl0lmfc7726lymmv5li5f5fm0bs1w3lgh3iqarvgr")))

