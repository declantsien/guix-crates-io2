(define-module (crates-io le ad lead_lang_interpreter) #:use-module (crates-io))

(define-public crate-lead_lang_interpreter-0.0.1 (c (n "lead_lang_interpreter") (v "0.0.1") (d (list (d (n "chalk_rs") (r "^1.0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "17510qvrhaj4gf19ggrn8khlr4qiwqvi42ph0b90a9ksc84vynaa")))

(define-public crate-lead_lang_interpreter-0.0.2 (c (n "lead_lang_interpreter") (v "0.0.2") (d (list (d (n "chalk_rs") (r "^1.0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0jp73bwpz6dlpgk1ny89c9dz7y32iz918igc0fbzc8krrs8069jr")))

(define-public crate-lead_lang_interpreter-0.0.3 (c (n "lead_lang_interpreter") (v "0.0.3") (d (list (d (n "chalk_rs") (r "^1.0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "02lbwd6pqy27x61hbjrwnf8r7vhldx4dm72mcknl3l89gjq9b1qa")))

(define-public crate-lead_lang_interpreter-0.0.4 (c (n "lead_lang_interpreter") (v "0.0.4") (d (list (d (n "chalk_rs") (r "^1.0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1ayyzzhfigm4zs3p9pf7ym42ihbjdi3vmjizsfz8bbsqflwcb80s")))

(define-public crate-lead_lang_interpreter-0.0.5 (c (n "lead_lang_interpreter") (v "0.0.5") (d (list (d (n "chalk_rs") (r "^1.0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0lxb30mfq5l36ahxy9nn89ymcfqxx2iarhiq9snqb03wxvx99xd5")))

(define-public crate-lead_lang_interpreter-0.0.6 (c (n "lead_lang_interpreter") (v "0.0.6") (d (list (d (n "chalk_rs") (r "^1.0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "10cyh6hvgs5s0pqs885dvipwn7j3jp5x1q2s9dcbhxn3k8z3q95m")))

(define-public crate-lead_lang_interpreter-0.0.7 (c (n "lead_lang_interpreter") (v "0.0.7") (d (list (d (n "chalk_rs") (r "^1.0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "13v18542rfncsj4plzidxx0s9h58fwp9qspj9vkdnzqvyywx1z9w")))

(define-public crate-lead_lang_interpreter-0.0.8 (c (n "lead_lang_interpreter") (v "0.0.8") (d (list (d (n "chalk_rs") (r "^1.0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "01fx1m73akgvcgsa2247h5qkcwgbnijcl0msx3im51sl018djava")))

(define-public crate-lead_lang_interpreter-0.0.9 (c (n "lead_lang_interpreter") (v "0.0.9") (d (list (d (n "chalk_rs") (r "^1.0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1sq8xljkgjf228q47zljybxfnqpvcv7j5rps6yga3cg3aqcw03lj")))

