(define-module (crates-io le ad lead_lang_std) #:use-module (crates-io))

(define-public crate-lead_lang_std-0.0.1 (c (n "lead_lang_std") (v "0.0.1") (d (list (d (n "lead_lang_interpreter") (r "^0.0.1") (d #t) (k 0)))) (h "0482i3zljqfjm73nlsmmdppfb6chh2n7xig2vp6qr49cicwpvax6") (y #t)))

(define-public crate-lead_lang_std-0.0.2 (c (n "lead_lang_std") (v "0.0.2") (d (list (d (n "lead_lang_interpreter") (r "^0.0.1") (d #t) (k 0)))) (h "1xfckii9sv1wmzj33k8mqd1rg9j0dp9nq7z2r2l600r9pln0pyb8") (y #t)))

