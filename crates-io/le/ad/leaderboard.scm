(define-module (crates-io le ad leaderboard) #:use-module (crates-io))

(define-public crate-leaderboard-0.1.0 (c (n "leaderboard") (v "0.1.0") (h "11bbhpm56gzlm9zzz21yhylcr042kq78iwfp18pqxjlln2hig951")))

(define-public crate-leaderboard-0.1.1 (c (n "leaderboard") (v "0.1.1") (h "1k59ilr282s2amavm0f2v4pjq1ahq685kfbaflb2ii60h5q82nrw")))

(define-public crate-leaderboard-0.1.2 (c (n "leaderboard") (v "0.1.2") (h "1rkbhgfnsyz3wpjkg7m7rym8y1xdf950xmazjxm10d86kvjfgrj4")))

