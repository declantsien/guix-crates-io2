(define-module (crates-io le nr lenra-app) #:use-module (crates-io))

(define-public crate-lenra-app-1.0.0-beta.1 (c (n "lenra-app") (v "1.0.0-beta.1") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "ureq") (r "^2.5.0") (f (quote ("json"))) (d #t) (k 0)))) (h "0fyk1bvld26hsjrjii4xrwly45f1ffh31f9fbqcg9vf13h28q6x6")))

(define-public crate-lenra-app-1.0.0-beta.2 (c (n "lenra-app") (v "1.0.0-beta.2") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "ureq") (r "^2.5.0") (f (quote ("json"))) (d #t) (k 0)))) (h "1ymd3lqgpsjrkxyqf18agvabg68771pqnjxldf10kvav08jg1nw4")))

(define-public crate-lenra-app-1.0.0-beta.3 (c (n "lenra-app") (v "1.0.0-beta.3") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "regress") (r "^0.7.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "ureq") (r "^2.5.0") (f (quote ("json"))) (d #t) (k 0)))) (h "1dl8vsmrl98nz1v4bdnzai3af63nigdlnj5m0zh2b5fcksf171nh")))

(define-public crate-lenra-app-1.0.0 (c (n "lenra-app") (v "1.0.0") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "regress") (r "^0.7.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "ureq") (r "^2.5.0") (f (quote ("json"))) (d #t) (k 0)))) (h "1q1c8y6wj1gxp20iv1llj06gps2q6445mi68vqpfh4ax83mw2459")))

(define-public crate-lenra-app-1.0.1 (c (n "lenra-app") (v "1.0.1") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "regress") (r "^0.7.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "ureq") (r "^2.5.0") (f (quote ("json"))) (d #t) (k 0)))) (h "1kn8f729g9bvnr0ffm4dakkalcgmf54a8q0b42ps61qyd8djih10")))

