(define-module (crates-io le mg lemgine) #:use-module (crates-io))

(define-public crate-lemgine-0.1.0 (c (n "lemgine") (v "0.1.0") (d (list (d (n "glium") (r "^0.30.2") (d #t) (k 0)))) (h "1am34hinkgpgywz3778j6ji28db38n58arl3h36m43y0ysy4br8h")))

(define-public crate-lemgine-0.1.1 (c (n "lemgine") (v "0.1.1") (d (list (d (n "glium") (r "^0.30.2") (d #t) (k 0)))) (h "07sl9c8fcmwjsmikyir9k65zm77rizv47cir9afg54av3zfm18rc")))

(define-public crate-lemgine-0.1.2 (c (n "lemgine") (v "0.1.2") (d (list (d (n "glium") (r "^0.30.2") (d #t) (k 0)))) (h "0489776r52h08jnc42krcig2nb1fs3xzhz2xa8d8nq4vj40ljk6n")))

(define-public crate-lemgine-0.1.3 (c (n "lemgine") (v "0.1.3") (d (list (d (n "glium") (r "^0.30.2") (d #t) (k 0)))) (h "103ckrylahi2kglqaip6jpbcnnhn7hlm5d8pdkwykykp2li3fxd1")))

(define-public crate-lemgine-0.2.0 (c (n "lemgine") (v "0.2.0") (d (list (d (n "glium") (r "^0.30.2") (d #t) (k 0)))) (h "0ndg9gjw10bjwb4xhx377mqwa0idzp2jf1d0k74bxjsdyyjpv0v2")))

(define-public crate-lemgine-0.2.1 (c (n "lemgine") (v "0.2.1") (d (list (d (n "glium") (r "^0.30.2") (d #t) (k 0)) (d (n "winit_input_helper") (r "^0.10.0") (d #t) (k 0)))) (h "01vdk4wkwvm5qkzxr6bzamj65kzap37qaid2ix4bmdnbv26qsair")))

