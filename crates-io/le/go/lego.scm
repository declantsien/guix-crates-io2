(define-module (crates-io le go lego) #:use-module (crates-io))

(define-public crate-lego-0.0.1 (c (n "lego") (v "0.0.1") (h "0222yjd7pnds1phsql1q7fdbqdaqzq2vs4bph862y6zz7m18p996")))

(define-public crate-lego-0.0.2 (c (n "lego") (v "0.0.2") (h "14m7q2hb98a7i7rw97rv67pkl0lkj2s9ksbvlysd4g6fx45vwwv9")))

