(define-module (crates-io le ft left-right) #:use-module (crates-io))

(define-public crate-left-right-0.1.0 (c (n "left-right") (v "0.1.0") (h "04f1jhkn46w4k3zqpvcqvrjygxp2isrwvg99aadivspz3f16ykc4")))

(define-public crate-left-right-0.9.0 (c (n "left-right") (v "0.9.0") (d (list (d (n "slab") (r "^0.4") (d #t) (k 0)))) (h "1p5w4nyvpdcxs3kp1prqz3vg95jmz1lkvmgmvh5zih464g6zg1da")))

(define-public crate-left-right-0.9.1 (c (n "left-right") (v "0.9.1") (d (list (d (n "slab") (r "^0.4") (d #t) (k 0)))) (h "0y1l5d77mlpa40lxd9wjlcvrplwnjv97mz6bh7l7w1m19m13nb1h")))

(define-public crate-left-right-0.9.2 (c (n "left-right") (v "0.9.2") (d (list (d (n "slab") (r "^0.4") (d #t) (k 0)))) (h "09i2grg1n2a0ly034av2zwicgh9d1nhcr0s1fdk7n37y325fmk0w")))

(define-public crate-left-right-0.10.0 (c (n "left-right") (v "0.10.0") (d (list (d (n "slab") (r "^0.4") (d #t) (k 0)))) (h "0452kz7k561vd6l1n18fbr09b7amca3n9cpbchcbhhh40xqm6slm")))

(define-public crate-left-right-0.11.0 (c (n "left-right") (v "0.11.0") (d (list (d (n "slab") (r "^0.4") (d #t) (k 0)))) (h "1cq0fgrwc8svkf1nkcb414xds6si3g1bwbsy27qzavcmmy4zgbyj")))

(define-public crate-left-right-0.11.2 (c (n "left-right") (v "0.11.2") (d (list (d (n "loom") (r "^0.4.0") (d #t) (t "cfg(loom)") (k 0)) (d (n "slab") (r "^0.4") (d #t) (k 0)))) (h "1szrc2s5j1h7sfflxljygq82nlr80f32jm1b8khp46ki5047h7hs")))

(define-public crate-left-right-0.11.3 (c (n "left-right") (v "0.11.3") (d (list (d (n "loom") (r "^0.4.0") (d #t) (t "cfg(loom)") (k 0)) (d (n "slab") (r "^0.4") (d #t) (k 0)))) (h "0sxxnj7aqkcwknalnsbvqc21v275phxcjk76iw15p0bgvmmmjkbz")))

(define-public crate-left-right-0.11.4 (c (n "left-right") (v "0.11.4") (d (list (d (n "loom") (r "^0.4.0") (d #t) (t "cfg(loom)") (k 0)) (d (n "slab") (r "^0.4") (d #t) (k 0)))) (h "00xjcvwzp4nsgq859v2jkip88jx18vfwr5d09xvik1y65dyw6cr0")))

(define-public crate-left-right-0.11.5 (c (n "left-right") (v "0.11.5") (d (list (d (n "loom") (r "^0.5.6") (d #t) (t "cfg(loom)") (k 0)) (d (n "slab") (r "^0.4.1") (d #t) (k 0)))) (h "1rir0k7m50103chvp260np0ln0f2dym3j82nhij74avimprxvgya")))

