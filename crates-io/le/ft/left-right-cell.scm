(define-module (crates-io le ft left-right-cell) #:use-module (crates-io))

(define-public crate-left-right-cell-0.1.0 (c (n "left-right-cell") (v "0.1.0") (d (list (d (n "left-right") (r "^0.11.5") (d #t) (k 0)))) (h "1zjqyrs67i466jaaj8d4wclh3jrvdan09ia21n6y3hhd4zg1q71i")))

(define-public crate-left-right-cell-0.1.1 (c (n "left-right-cell") (v "0.1.1") (d (list (d (n "left-right") (r "^0.11.5") (d #t) (k 0)))) (h "0z7lqva1nk483zvkc4kb0mrq6yjcr8zpwfim8p7h3zc0r2p8wlz8")))

(define-public crate-left-right-cell-0.1.2 (c (n "left-right-cell") (v "0.1.2") (d (list (d (n "left-right") (r "^0.11.5") (d #t) (k 0)))) (h "178z77z49hf20mls5hqc01sj29zbdcs7i4bd1jl25bjgranx5zww")))

(define-public crate-left-right-cell-0.1.3 (c (n "left-right-cell") (v "0.1.3") (d (list (d (n "left-right") (r "^0.11.5") (d #t) (k 0)))) (h "016cxh4giq41jqa7fvfbwnhfzyjxxnywsyv3xm1mhjs8iv3y443q")))

