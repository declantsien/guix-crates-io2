(define-module (crates-io le ft leftpad-rs) #:use-module (crates-io))

(define-public crate-leftpad-rs-1.0.0 (c (n "leftpad-rs") (v "1.0.0") (d (list (d (n "rstest") (r "^0.11.0") (d #t) (k 2)))) (h "0iqzj50z8bwhjzx1ji5b3mg31w7bpm4wx336nw0wdmif8q6xgm4n")))

(define-public crate-leftpad-rs-1.0.1 (c (n "leftpad-rs") (v "1.0.1") (d (list (d (n "rstest") (r "^0.11.0") (d #t) (k 2)))) (h "05s7fyhyj2j5ncizvyl8kb6bqj1vrgs58mpqhvb663v4p6clqirg")))

(define-public crate-leftpad-rs-1.1.0 (c (n "leftpad-rs") (v "1.1.0") (d (list (d (n "rstest") (r "^0.11.0") (d #t) (k 2)))) (h "0jvg9hz0vrdwf1chgwzz06v9g4jg9w779lgd1i35digbn1jzcnrs")))

(define-public crate-leftpad-rs-1.2.0 (c (n "leftpad-rs") (v "1.2.0") (d (list (d (n "rstest") (r "^0.11.0") (d #t) (k 2)))) (h "1cmjv65c6pprf3fkhzvm36c270m7vrs28g6v4i35fllm2xbxsn4l")))

