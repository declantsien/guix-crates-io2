(define-module (crates-io le ft leftpad-str) #:use-module (crates-io))

(define-public crate-leftpad-str-6.6.6 (c (n "leftpad-str") (v "6.6.6") (h "0h9klfb60nx5yq61d39mphs0gip67kp2n2zq0kadsnkxbi9psl7w")))

(define-public crate-leftpad-str-9.9.9 (c (n "leftpad-str") (v "9.9.9") (h "064lzhcrm2wqm895b34x5dh1dgw0265lsbvmyaxjxplbpg964dm3")))

