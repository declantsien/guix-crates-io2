(define-module (crates-io le ft leftpad) #:use-module (crates-io))

(define-public crate-leftpad-0.1.0 (c (n "leftpad") (v "0.1.0") (h "0w04dsk1wnvh7ay6rgyb9lazahqjcxanw8zpqz5ffz3fjaxsw1g1")))

(define-public crate-leftpad-0.2.0 (c (n "leftpad") (v "0.2.0") (h "1yb0jq162gbvwbwh5r2329z41j028wd6cdm6pgd9avh1136czyc8")))

