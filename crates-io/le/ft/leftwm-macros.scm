(define-module (crates-io le ft leftwm-macros) #:use-module (crates-io))

(define-public crate-leftwm-macros-0.5.0 (c (n "leftwm-macros") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.17") (f (quote ("full" "parsing" "fold"))) (d #t) (k 0)))) (h "00bilkvl94q3rf93n3rv3vibsfnzr5qz7sym1c1aghi0nzygr7ry") (r "1.70.0")))

