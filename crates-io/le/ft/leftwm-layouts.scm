(define-module (crates-io le ft leftwm-layouts) #:use-module (crates-io))

(define-public crate-leftwm-layouts-0.0.1 (c (n "leftwm-layouts") (v "0.0.1") (h "1x2if5p9k22q25539gkj6zwmj3bh1glg8d3xqmkqvs0hjq22xb7w")))

(define-public crate-leftwm-layouts-0.0.2 (c (n "leftwm-layouts") (v "0.0.2") (h "07ml2r8j92i5v1n1ma0pn88dadbczg89z24knfb3pg3w83kw1vr3")))

(define-public crate-leftwm-layouts-0.0.3 (c (n "leftwm-layouts") (v "0.0.3") (h "1n9arlyfmnhhylx6zqr4n92fsz50d8x7h9dkwbqqgd4lnq222j3a")))

(define-public crate-leftwm-layouts-0.0.4 (c (n "leftwm-layouts") (v "0.0.4") (h "1gv907k2g2vxv82wnzjl31vsfhs0khnlvh8pc6mzpsywfls0smfh")))

(define-public crate-leftwm-layouts-0.0.5 (c (n "leftwm-layouts") (v "0.0.5") (d (list (d (n "ron") (r "^0.7") (d #t) (k 0)) (d (n "ron") (r "^0.7") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0cn8shgpgs4bz0x3dwjsqdfngwawhpwqcanhyqjmyvcydbjayzzi")))

(define-public crate-leftwm-layouts-0.0.6 (c (n "leftwm-layouts") (v "0.0.6") (d (list (d (n "ron") (r "^0.7") (d #t) (k 0)) (d (n "ron") (r "^0.7") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1jcc5fknixxq1y3i2pmn884vhnd45aw52z2dgq811lpmg6625r2r")))

(define-public crate-leftwm-layouts-0.0.7 (c (n "leftwm-layouts") (v "0.0.7") (d (list (d (n "ron") (r "^0.7") (d #t) (k 0)) (d (n "ron") (r "^0.7") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1588scpgg2b7ynsam5dpn76gz13wm9928vi6zix154rvzfjqxy9b")))

(define-public crate-leftwm-layouts-0.0.8 (c (n "leftwm-layouts") (v "0.0.8") (d (list (d (n "ron") (r "^0.7") (d #t) (k 0)) (d (n "ron") (r "^0.7") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1jf8f02mpz2jkn2z5fkmxyfh0phjilzr3n1gylvpv82pahyhgw2k")))

(define-public crate-leftwm-layouts-0.0.9 (c (n "leftwm-layouts") (v "0.0.9") (d (list (d (n "ron") (r "^0.7") (d #t) (k 0)) (d (n "ron") (r "^0.7") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0jrlnxqkxcl742spcb0881in98v7ix7vmdr7j14g2g6l376n23g0")))

(define-public crate-leftwm-layouts-0.1.0 (c (n "leftwm-layouts") (v "0.1.0") (d (list (d (n "ron") (r "^0.7") (d #t) (k 0)) (d (n "ron") (r "^0.7") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0yqaf8b2ybsw4kr1y8p84gv04xvhpxps6idn0qalzil87yy62jaf")))

(define-public crate-leftwm-layouts-0.2.0 (c (n "leftwm-layouts") (v "0.2.0") (h "1qr1ivcfxgfn67nnqi03a2xzh1yvnygcjcjf5a6047sxysff5q3k")))

(define-public crate-leftwm-layouts-0.3.0 (c (n "leftwm-layouts") (v "0.3.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1jg2a3j91cm4w79s9jrq52ljhj32hgic14by3pkgjia66pjnpg7k")))

(define-public crate-leftwm-layouts-0.3.1 (c (n "leftwm-layouts") (v "0.3.1") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "14m8lfhnapddp10jx3dhqk57i6fspyzjw221c6621fpfqd3nxq3m")))

(define-public crate-leftwm-layouts-0.3.2 (c (n "leftwm-layouts") (v "0.3.2") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1rjf9fzp10kdm0yszih5zdvkmba2539laksrzs9kc1r9cybdcc8y")))

(define-public crate-leftwm-layouts-0.3.3 (c (n "leftwm-layouts") (v "0.3.3") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "04258fsqjhcrjxv90fbrdaq0bv3iwz1i0sny1d826mhcn4gh9l3j")))

(define-public crate-leftwm-layouts-0.4.0 (c (n "leftwm-layouts") (v "0.4.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0bpvgj9agnfgq1a3hvq5x1sbx7wf46wdw99574ldmgy344k2mapv")))

(define-public crate-leftwm-layouts-0.5.0 (c (n "leftwm-layouts") (v "0.5.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1zbnsn9f7i4mrnwak4j8ymxjjkq1g3z7hhzsxiq5687ixxnnh3wv")))

(define-public crate-leftwm-layouts-0.6.0 (c (n "leftwm-layouts") (v "0.6.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1sv8v9c5p4wr15naax2ngcdpvqglibylp9scw8x40z23cipcwsr4")))

(define-public crate-leftwm-layouts-0.6.1 (c (n "leftwm-layouts") (v "0.6.1") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1443j28arlczc2f9yl337895sn41vnr0m2vci7x58sqqji5pdmjw")))

(define-public crate-leftwm-layouts-0.7.0 (c (n "leftwm-layouts") (v "0.7.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0l8wq7gly48vc2qrh7nkcj5d22z4sbf8xw1i7blgw9ifv94s4bnf")))

(define-public crate-leftwm-layouts-0.7.1 (c (n "leftwm-layouts") (v "0.7.1") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "08cf7759g86908v9wjzi2c7cjsv1jdc5z0rszp7iah4czl81phfq")))

(define-public crate-leftwm-layouts-0.8.0 (c (n "leftwm-layouts") (v "0.8.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0xdcc37m744q0i6kxd82fh8ckcg2gc2ck9hxzlvpx8bkc1803aa4")))

(define-public crate-leftwm-layouts-0.8.1 (c (n "leftwm-layouts") (v "0.8.1") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "10j4jimbqh23i8rv1h37smz7q4zqqikpgqzysmnf4g7zkd8icxwd")))

(define-public crate-leftwm-layouts-0.8.2 (c (n "leftwm-layouts") (v "0.8.2") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0v3kq6sqifbi4gbcp5q5q0r3v72v5hwwfixm32z1bzmmdda0zn3l")))

(define-public crate-leftwm-layouts-0.8.3 (c (n "leftwm-layouts") (v "0.8.3") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0biv10hsxghcn8q84lz7n1203vpp7g5dns93a11xfikf14sjb2k9")))

(define-public crate-leftwm-layouts-0.8.4 (c (n "leftwm-layouts") (v "0.8.4") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0wyhnqfqdwwppx53iym4r3kdcvxial4sjmywlimigw2r7y6lplks")))

(define-public crate-leftwm-layouts-0.9.0 (c (n "leftwm-layouts") (v "0.9.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0h42ina1xy0r8hv7w5s5xjwyy8lgnpk88xfcyx78axv8plfc0cl2")))

(define-public crate-leftwm-layouts-0.9.1 (c (n "leftwm-layouts") (v "0.9.1") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1lyzmvjvsx0lkdcib1q5iycaac1k27mn5s2mc7vf2a8nhpiwa1jx")))

