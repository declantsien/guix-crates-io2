(define-module (crates-io le ft left-pad) #:use-module (crates-io))

(define-public crate-left-pad-1.0.0 (c (n "left-pad") (v "1.0.0") (h "02900mf45qlq36vhl1d9qbypx5fjz2g98scjxky70nlryzznjw02")))

(define-public crate-left-pad-1.0.1 (c (n "left-pad") (v "1.0.1") (h "0az6hvvvnwihgxf8qd0l7wqcr8nycr5c6nazii2l7hgcic8my8wp")))

