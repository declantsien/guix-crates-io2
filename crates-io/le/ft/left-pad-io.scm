(define-module (crates-io le ft left-pad-io) #:use-module (crates-io))

(define-public crate-left-pad-io-0.0.1 (c (n "left-pad-io") (v "0.0.1") (d (list (d (n "hyper") (r "^0.8.0") (d #t) (k 0)) (d (n "quick-error") (r "^1.0.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.18") (d #t) (k 0)))) (h "0b0sm869mjcjsls2bgibfz5l6pvz0x0cjih7vx8n3waix79wdp76")))

(define-public crate-left-pad-io-0.0.2 (c (n "left-pad-io") (v "0.0.2") (d (list (d (n "hyper") (r "^0.9.10") (d #t) (k 0)) (d (n "quick-error") (r "^1.0.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.18") (d #t) (k 0)))) (h "1n5wniqzyk9v1wn7jb4dn2gq7f8jpmls48bp88pj0zpav0c470y7")))

