(define-module (crates-io le mo lemon-agent) #:use-module (crates-io))

(define-public crate-lemon-agent-0.0.0 (c (n "lemon-agent") (v "0.0.0") (h "08kqw4h7gyaibpcr8svnhx47dn1qjnfsf5iw7qjhh0pfg6kwgjcs")))

(define-public crate-lemon-agent-0.0.1 (c (n "lemon-agent") (v "0.0.1") (h "1qrnqp4vwi2srgm8xpk4cdbq5sjs7g99cs2w92jghlh65kmm90ik")))

