(define-module (crates-io le mo lemonmath) #:use-module (crates-io))

(define-public crate-lemonmath-0.1.0 (c (n "lemonmath") (v "0.1.0") (h "0sr7chdxzdk38x03f58drd1d1xyzrbc8cv2sj16w01r8y0z6yx9h")))

(define-public crate-lemonmath-0.1.1 (c (n "lemonmath") (v "0.1.1") (h "1jh15iw4s6bal3rpbb4ms6sv1k8sbvyghfdsgal6b3zx9scl067l")))

(define-public crate-lemonmath-0.1.2 (c (n "lemonmath") (v "0.1.2") (h "1065nrvs1c0n5hrd4pchpk4gsbg443j67xkx84k5jmmzyv0hgx6r")))

(define-public crate-lemonmath-0.1.3 (c (n "lemonmath") (v "0.1.3") (h "0d02y0sfwcdbg311zkf8zh6xwbhs6b9bagpxd0halp9gxamr5d9q")))

(define-public crate-lemonmath-0.1.4 (c (n "lemonmath") (v "0.1.4") (h "0isfnjp1043xlr76ajz9wysrbxwplqf4cphcsbwzz0bl67z3x607")))

