(define-module (crates-io le mo lemon-memory) #:use-module (crates-io))

(define-public crate-lemon-memory-0.0.0 (c (n "lemon-memory") (v "0.0.0") (h "1d6hjkbxkr1c79m8aqnx84b2jwviyykg7lr074c58gm0ij8z89j2")))

(define-public crate-lemon-memory-0.0.1 (c (n "lemon-memory") (v "0.0.1") (h "1z0pk9bpxlv9p8cwc3a1w1ms1pfhgqzxsn1msfimvccrs454x99d")))

