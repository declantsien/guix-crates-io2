(define-module (crates-io le mo lemonade) #:use-module (crates-io))

(define-public crate-lemonade-0.0.0 (c (n "lemonade") (v "0.0.0") (h "0vfiblr9hhd38i1lxp4c7nv4i9jil5bsamgk4x768yd1016r8fia")))

(define-public crate-lemonade-0.0.1 (c (n "lemonade") (v "0.0.1") (h "08vy4zcjbq5znwng0sr03klfpzxylbyiyaf6vzrfrr0qc1vil6l3")))

