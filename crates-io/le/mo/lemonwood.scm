(define-module (crates-io le mo lemonwood) #:use-module (crates-io))

(define-public crate-lemonwood-0.1.0 (c (n "lemonwood") (v "0.1.0") (d (list (d (n "function_name") (r "^0.3.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (f (quote ("max_level_trace" "release_max_level_warn"))) (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 0)) (d (n "volmark") (r "^0.1.0") (d #t) (k 0)))) (h "0cjrm8xcwa9w2f2l1c5sm56yn1mf1g78flr2n14n7y80sjxa9wck")))

