(define-module (crates-io le mo lemon-tree) #:use-module (crates-io))

(define-public crate-lemon-tree-0.1.0 (c (n "lemon-tree") (v "0.1.0") (d (list (d (n "lemon-mint") (r "^0.1") (d #t) (k 0)) (d (n "lemon-tree-derive") (r "^0.1") (d #t) (k 0)))) (h "0xf31a9w185px93icsw89n5b27mwkh4ksv7d04sqg0w6q09h07l0") (f (quote (("dump-lemon-grammar" "lemon-tree-derive/dump-lemon-grammar") ("dump-grammar" "lemon-tree-derive/dump-grammar"))))))

(define-public crate-lemon-tree-0.1.1 (c (n "lemon-tree") (v "0.1.1") (d (list (d (n "lemon-mint") (r "^0.1") (d #t) (k 0)) (d (n "lemon-tree-derive") (r "^0.1") (d #t) (k 0)))) (h "123jyinw270fn23wnpzx7h5zgjcyyv4p8p14sd1w8vab7vqc8blj") (f (quote (("dump-lemon-grammar" "lemon-tree-derive/dump-lemon-grammar") ("dump-grammar" "lemon-tree-derive/dump-grammar"))))))

(define-public crate-lemon-tree-0.1.2 (c (n "lemon-tree") (v "0.1.2") (d (list (d (n "lemon-mint") (r "^0.1") (d #t) (k 0)) (d (n "lemon-tree-derive") (r "^0.1") (d #t) (k 0)))) (h "14v7sxd0rw7xikn1ljfsnd2d08v5h19w0m78ry40cmrhgj4nvili") (f (quote (("dump-lemon-grammar" "lemon-tree-derive/dump-lemon-grammar") ("dump-grammar" "lemon-tree-derive/dump-grammar"))))))

(define-public crate-lemon-tree-0.1.3 (c (n "lemon-tree") (v "0.1.3") (d (list (d (n "lemon-mint") (r "^0.1") (d #t) (k 0)) (d (n "lemon-tree-derive") (r "^0.1") (d #t) (k 0)))) (h "07wjj0qs16g30j777r4zyp6msjwzzjwql73v2cy4qpwg50k5c7z5") (f (quote (("dump-lemon-grammar" "lemon-tree-derive/dump-lemon-grammar") ("dump-grammar" "lemon-tree-derive/dump-grammar"))))))

(define-public crate-lemon-tree-0.1.4 (c (n "lemon-tree") (v "0.1.4") (d (list (d (n "lemon-tree-derive") (r "^0.1") (d #t) (k 0)))) (h "0blb7w5x0323d14aysdldzmlys088xqy13ji9v0x14x020li23qg") (f (quote (("dump-lemon-grammar" "lemon-tree-derive/dump-lemon-grammar") ("dump-grammar" "lemon-tree-derive/dump-grammar"))))))

(define-public crate-lemon-tree-0.1.5 (c (n "lemon-tree") (v "0.1.5") (d (list (d (n "lemon-tree-derive") (r "^1.0") (d #t) (k 0)))) (h "01cm5mmzydjs9fsxhqcrn46sqcdfwidw6frrsls1y1vrzmnj6lpx") (f (quote (("dump-lemon-grammar" "lemon-tree-derive/dump-lemon-grammar") ("dump-grammar" "lemon-tree-derive/dump-grammar"))))))

(define-public crate-lemon-tree-0.1.6 (c (n "lemon-tree") (v "0.1.6") (d (list (d (n "lemon-tree-derive") (r "^1.0") (d #t) (k 0)))) (h "0y6lg8cyvc89js79k8gq4jyhgza5fydsqkzvkzhsjrfjqmygrbin") (f (quote (("dump-lemon-grammar" "lemon-tree-derive/dump-lemon-grammar") ("dump-grammar" "lemon-tree-derive/dump-grammar"))))))

(define-public crate-lemon-tree-0.1.7 (c (n "lemon-tree") (v "0.1.7") (d (list (d (n "lemon-tree-derive") (r "^1.0") (d #t) (k 0)))) (h "0qkrqgi3wyk2c2ipn10fi1g9524s018jsas26pd7cdabhqi2myn2") (f (quote (("dump-lemon-grammar" "lemon-tree-derive/dump-lemon-grammar") ("dump-grammar" "lemon-tree-derive/dump-grammar") ("debug-parser-to-file" "lemon-tree-derive/debug-parser-to-file"))))))

