(define-module (crates-io le x- lex-lib) #:use-module (crates-io))

(define-public crate-lex-lib-0.1.0 (c (n "lex-lib") (v "0.1.0") (h "0bbpkvzxmq9l9m0jfsqwi07d90j3jjh5wzknz7ab8lrn9klc42yz")))

(define-public crate-lex-lib-0.2.0 (c (n "lex-lib") (v "0.2.0") (h "1xhgskcn8yhpbklyp2hqvcsvj6w937ngkg8zph611sw32hqcnw2g")))

(define-public crate-lex-lib-0.3.0 (c (n "lex-lib") (v "0.3.0") (h "0h59np2ixaihkb0xrpxy2wbw6jlxj8vk0yjx1j9sl4yibjcdjskh")))

(define-public crate-lex-lib-0.3.1 (c (n "lex-lib") (v "0.3.1") (h "0gchgs50n76c2casd62px3vxpg4mkjbdg3xr1w47ywg6lqgy58sw")))

(define-public crate-lex-lib-0.3.2 (c (n "lex-lib") (v "0.3.2") (h "022r92n05bp7j7f1gvv2lqs1s434x84xnx6h972yldgkigz6ax7c")))

