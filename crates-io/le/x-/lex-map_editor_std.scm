(define-module (crates-io le x- lex-map_editor_std) #:use-module (crates-io))

(define-public crate-lex-map_editor_std-0.1.0 (c (n "lex-map_editor_std") (v "0.1.0") (h "1iyk84rd8jb6gysdq88ipbcgggl3bkbkg5z7h2a2r9kxamr4b5yc")))

(define-public crate-lex-map_editor_std-0.1.1 (c (n "lex-map_editor_std") (v "0.1.1") (h "0a60mjshm0hpqyzrfvyhpigvvl43814n0lisan0vjbgkalcnnjfg")))

(define-public crate-lex-map_editor_std-0.2.0 (c (n "lex-map_editor_std") (v "0.2.0") (h "1vfn75z7pkzqkl3hvq5ck6acrk7hc2c9s7rv2l56hfiabkvlx1cj")))

