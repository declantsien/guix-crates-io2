(define-module (crates-io le -r le-robert) #:use-module (crates-io))

(define-public crate-le-robert-0.1.0 (c (n "le-robert") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "scraper") (r "^0.16.0") (d #t) (k 0)))) (h "1my2sv02r9awpma8kawhcappbmb94x834hhpdywhya8nl4pz72hi")))

