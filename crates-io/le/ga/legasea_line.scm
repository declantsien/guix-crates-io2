(define-module (crates-io le ga legasea_line) #:use-module (crates-io))

(define-public crate-legasea_line-0.1.0 (c (n "legasea_line") (v "0.1.0") (d (list (d (n "euclid") (r "^0.22.1") (d #t) (k 0)))) (h "1amykjylfyw9c3dqga7q63mvdqnpw96zmjwkh549inar0ny9myyh")))

(define-public crate-legasea_line-0.2.0 (c (n "legasea_line") (v "0.2.0") (d (list (d (n "approx") (r "^0.3.2") (d #t) (k 2)) (d (n "mint") (r "^0.5.5") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.12") (d #t) (k 0)))) (h "1myhmclw9dw3ar184b9mfw1dgsghfxj4j0d3pi9m9h3y2rni5ayn")))

(define-public crate-legasea_line-0.2.1 (c (n "legasea_line") (v "0.2.1") (d (list (d (n "approx") (r "^0.4.0") (d #t) (k 2)) (d (n "mint") (r "^0.5.6") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "1ilxb5ic1zlfl67jm4sma0h5jf8dcgnnhagwk9zj8ca5w9kj0h27")))

