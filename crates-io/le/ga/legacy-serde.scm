(define-module (crates-io le ga legacy-serde) #:use-module (crates-io))

(define-public crate-legacy-serde-0.6.0 (c (n "legacy-serde") (v "0.6.0") (d (list (d (n "serde") (r "^0.6") (k 0)))) (h "1x2h5vdwb016qll1hgjhjhb8qycmad7cs5rqh0xcw7dp3qfzny1p") (f (quote (("num-rational" "serde/num-rational") ("num-impls" "serde/num-impls") ("num-complex" "serde/num-complex") ("num-bigint" "serde/num-bigint") ("nightly-testing" "serde/nightly-testing") ("nightly" "serde/nightly"))))))

(define-public crate-legacy-serde-0.7.0 (c (n "legacy-serde") (v "0.7.0") (d (list (d (n "serde") (r "^0.7") (k 0)))) (h "014amjrl2k67l0yhcmnw4s653lp9zkmmsjqdzfcs9xnx9jfqw8ci") (f (quote (("std" "serde/std") ("nightly-testing" "serde/nightly-testing") ("nightly" "serde/nightly") ("default" "std") ("collections" "serde/collections") ("alloc" "serde/alloc"))))))

(define-public crate-legacy-serde-0.8.0 (c (n "legacy-serde") (v "0.8.0") (d (list (d (n "serde") (r "^0.8") (k 0)))) (h "0vy33ixsgr2dspih9pzg3qgq80z38w5da23p34dlxn67hzmpq06y") (f (quote (("unstable-testing" "serde/unstable-testing") ("unstable" "serde/unstable") ("std" "serde/std") ("default" "std") ("collections" "serde/collections") ("alloc" "serde/alloc"))))))

(define-public crate-legacy-serde-0.9.0 (c (n "legacy-serde") (v "0.9.0") (d (list (d (n "serde") (r "^0.9") (k 0)))) (h "14m7g9f1fjck67178bazfjp2ms3dii4lzcmaihm7byi8kflmkbmy") (f (quote (("unstable-testing" "serde/unstable-testing") ("unstable" "serde/unstable") ("std" "serde/std") ("playground" "serde/playground") ("derive" "serde/derive") ("default" "std") ("collections" "serde/collections") ("alloc" "serde/alloc"))))))

