(define-module (crates-io le ga legal_chess) #:use-module (crates-io))

(define-public crate-legal_chess-0.1.0 (c (n "legal_chess") (v "0.1.0") (h "0wvryj5lbya0976pp2xc5svzs5jihn09dmy4ycvkbbwm2j86c7c6")))

(define-public crate-legal_chess-0.1.1 (c (n "legal_chess") (v "0.1.1") (h "1mi2rajry5aj1b3hrhm5mqygs1z2n8g34ng3ykzb5k1s1ns6y8m1")))

(define-public crate-legal_chess-0.1.2 (c (n "legal_chess") (v "0.1.2") (h "1lp480lvwz8xvq0ys2abgg6jzfpb904wqjnazf5a9xyqznr4gqml")))

(define-public crate-legal_chess-0.1.3 (c (n "legal_chess") (v "0.1.3") (h "1q1fvzacyy8qlhr7h0mpzgxzpz234kgbgs5dblsyn4sbc9fbrryx")))

(define-public crate-legal_chess-0.1.4 (c (n "legal_chess") (v "0.1.4") (h "1b8b555ws6r59xfwhgjbppyfqb2g0p212ja9hkq9vmvj6bw77rr3")))

(define-public crate-legal_chess-0.2.0 (c (n "legal_chess") (v "0.2.0") (h "1fxlr695z4jw2xmfzh2d0580x4rr8i2qrz4pw9pqvwihmxs24a9z")))

(define-public crate-legal_chess-0.2.1 (c (n "legal_chess") (v "0.2.1") (h "1wb03zwsgix46zqf70pffhpyqpk9pm9dw4iigyv691dd5vcphgj4")))

(define-public crate-legal_chess-0.3.0 (c (n "legal_chess") (v "0.3.0") (h "0j5d9196yx13fbr5xjcq841dmc4lm37kpadfvz8cy1x827g8gw0y")))

(define-public crate-legal_chess-0.3.1 (c (n "legal_chess") (v "0.3.1") (h "1kcn9rh9rihrnd61n1vwp8615bqkf0mb1k5mmmzb2y2i4p5nwjag")))

(define-public crate-legal_chess-0.4.0 (c (n "legal_chess") (v "0.4.0") (h "03j0rnj27pcc823p7x9p09mgk2cbignlqa1sr32ad0asc7frdbia")))

(define-public crate-legal_chess-0.4.1 (c (n "legal_chess") (v "0.4.1") (h "0n4sl1ji4yialbbxb5ig5ba3p1p562xyzr4qwbwkdckxiyk3wh99")))

(define-public crate-legal_chess-0.4.2 (c (n "legal_chess") (v "0.4.2") (h "1af0c7841qvj40acc1jkahbd72ahwlwk4a61pi8gr3g3x20qgdsh")))

(define-public crate-legal_chess-0.4.3 (c (n "legal_chess") (v "0.4.3") (h "17kqn3zlai908gd6i6cm9qyykbmzlg39qvmmaarvjg9bwx1bcmcb")))

(define-public crate-legal_chess-0.4.4 (c (n "legal_chess") (v "0.4.4") (h "07inq359ywwvdbnnahm2c6rbmykcaskjkdv7hzby6rf6arz23ax9")))

(define-public crate-legal_chess-0.4.5 (c (n "legal_chess") (v "0.4.5") (h "1xmniyf3c8z9dbd9jwn3a758d07fik11a0x8zlfwzp5h2k4vbqd2")))

(define-public crate-legal_chess-0.4.6 (c (n "legal_chess") (v "0.4.6") (h "1qjbql288sp1i7ksndy89hd88h8hza6p7jz932rwlmmr379glqq6")))

(define-public crate-legal_chess-0.4.7 (c (n "legal_chess") (v "0.4.7") (h "1nibzal34dgk02ckh6aaxpsaa11fxqks7zz3j3fyl1nx6nw413nv")))

(define-public crate-legal_chess-0.4.8 (c (n "legal_chess") (v "0.4.8") (h "05d5818kydia1v1m6zpj9bzmjnc795azdgnvsxb8jfksgl751ww9")))

(define-public crate-legal_chess-0.4.9 (c (n "legal_chess") (v "0.4.9") (h "1vckmf5b10vkc5q4k1ibdx2s9vs7gdv9ijfjysnk38lw1fr1lgr4")))

(define-public crate-legal_chess-0.4.10 (c (n "legal_chess") (v "0.4.10") (h "1yyl4z4xaw9n9fdavkmz37v4y5xlc67kks7c7g1zwp9k9rx2012n")))

(define-public crate-legal_chess-0.4.11 (c (n "legal_chess") (v "0.4.11") (h "0nj7wcvmiy6gbixsp8rzaqms40ajhys8p6ms7d2l2kvlbag23v12")))

(define-public crate-legal_chess-0.4.12 (c (n "legal_chess") (v "0.4.12") (h "1cm1r57lay1d1qw07hmalf5w1k025jxcsf5ab6bh3c19y5j9fyjq")))

(define-public crate-legal_chess-0.4.13 (c (n "legal_chess") (v "0.4.13") (h "0fwlh2fzhkji51ywfy3kzp5kwns0himmsnwnkcjha7q2lws6lmzk")))

(define-public crate-legal_chess-0.4.14 (c (n "legal_chess") (v "0.4.14") (h "0jxq4h4sx2fkjf2abqsyj5dfy9rv2c81cqlbm2cpxiq74211shc1")))

(define-public crate-legal_chess-0.4.15 (c (n "legal_chess") (v "0.4.15") (h "1pnmyi3cq2ypx9vlb4lqqa0v10kkm0rc60371s5zxkncf6avk9f1")))

(define-public crate-legal_chess-0.4.16 (c (n "legal_chess") (v "0.4.16") (h "1a962nrfma2mhcx4bf735lp2p94p55qis3spjy8sck6lgr4mpq7j")))

(define-public crate-legal_chess-0.4.17 (c (n "legal_chess") (v "0.4.17") (h "1kab868imdbmf0xwdpcyvxlmd2jlwm68dwn0fzqflpda51cfsbmg")))

(define-public crate-legal_chess-0.4.18 (c (n "legal_chess") (v "0.4.18") (h "0jf0v0yrwzj8jvnha57w4qvw4xcp22i0m0hpr1wfhcqi2yhsqy9s")))

(define-public crate-legal_chess-0.4.19 (c (n "legal_chess") (v "0.4.19") (h "0wi9fmjm882a4x5mjvw9nizq0fbr65mkficklnlq8aiyy2vqma25")))

(define-public crate-legal_chess-0.4.20 (c (n "legal_chess") (v "0.4.20") (h "0lwvs7frn6pf7j0ygbyakja7wpxqfv6y25yq9608baxxhp6zs05x")))

(define-public crate-legal_chess-0.4.21 (c (n "legal_chess") (v "0.4.21") (h "1blvdxd579ka6k194m2bj2sc8hgc34kwnlr3vsasyg7cajlsqc0f")))

(define-public crate-legal_chess-0.4.22 (c (n "legal_chess") (v "0.4.22") (h "1kppxpbp8zv2l718s1bhsygn6mdwibl2j5gfxncizb04dnhl0avg")))

(define-public crate-legal_chess-0.4.23 (c (n "legal_chess") (v "0.4.23") (h "0cb5kxfh7n6dddby72awbx9g7mlrjz44pd289q7bz058cbnk6y16")))

(define-public crate-legal_chess-0.4.24 (c (n "legal_chess") (v "0.4.24") (h "07n53q10j9y0nq04p3pyp5xc5q6zmdsqzxbh5c8kbmdy7755dyrq")))

(define-public crate-legal_chess-0.4.25 (c (n "legal_chess") (v "0.4.25") (h "0w2cpg2bl4qk3rw1q5r8ajxkpwb2476zp7y5l48q2illi4lxwxir")))

(define-public crate-legal_chess-0.4.26 (c (n "legal_chess") (v "0.4.26") (h "0i75j4ya8ayn0sknqvrrvdb8akgbhpcrs1xafh30k962gwjhk4a4")))

(define-public crate-legal_chess-0.4.27 (c (n "legal_chess") (v "0.4.27") (h "0v738qdlvilmr4343pvsxj6di2kma6crzw81hjzv70saj7akphjg")))

(define-public crate-legal_chess-0.4.28 (c (n "legal_chess") (v "0.4.28") (h "1xigrgq65gp032h0yrss49yxcfnhvf3r5zfbj8wsdgxwsj0cxvgv")))

(define-public crate-legal_chess-0.4.29 (c (n "legal_chess") (v "0.4.29") (h "0rlk91aimfpxp28z8c6wx4yvvjs8d6v3irbva7lnz26yiw7rhb1i")))

(define-public crate-legal_chess-0.4.30 (c (n "legal_chess") (v "0.4.30") (h "1w60yfzc79paxjgspwsrfy5r46sk21z0f4ygd4gy4k34aap9aj01")))

(define-public crate-legal_chess-0.4.31 (c (n "legal_chess") (v "0.4.31") (h "0992xq0frpiy63c6rzx5jcscxfhs1sw68xr6lci9dj6skdcrwr83")))

(define-public crate-legal_chess-0.4.32 (c (n "legal_chess") (v "0.4.32") (h "1a39xx237vq9hh39w138gnjhycprdvzinw4rj85ix4drj7lnn8hy")))

(define-public crate-legal_chess-0.4.33 (c (n "legal_chess") (v "0.4.33") (h "00yn513ab6rj24kknbnskd5hx2i4klcmws8v331fi3i5flkj7fl7")))

(define-public crate-legal_chess-0.4.34 (c (n "legal_chess") (v "0.4.34") (h "10ybq806kwn0y9rzwpdq1vj5b9g2a1nys2zkk5fvs660dmp8bdi5")))

(define-public crate-legal_chess-0.4.35 (c (n "legal_chess") (v "0.4.35") (h "144laj8s7amzaylr11wc8rs6ck6n1w3d2g5rxk92vg9px1mlldgh")))

(define-public crate-legal_chess-0.4.36 (c (n "legal_chess") (v "0.4.36") (h "0hk5q53fjz138dlh5vymbvb42s2v9izhf9mjalfi3lzhna7m0rm8")))

(define-public crate-legal_chess-0.4.37 (c (n "legal_chess") (v "0.4.37") (h "0ab39bl0fwhvxpnnmdcb356j0y0mbd57kkjpmrfhdwlqxwpic91q")))

(define-public crate-legal_chess-0.4.38 (c (n "legal_chess") (v "0.4.38") (h "0xscqcjwmxs4ws4syq5nsi2dl982jz5gq8ch0js7611bimhx2jc3")))

(define-public crate-legal_chess-0.4.39 (c (n "legal_chess") (v "0.4.39") (h "1dj8hkiws0hp5kvfqd4q418jbd1gz2969n490x1pl1qls850qcxm")))

(define-public crate-legal_chess-0.4.40 (c (n "legal_chess") (v "0.4.40") (h "0lkddsh67pl4srz2s7jrl0d0v66i58gfp9aq0iqv1xy3vg7yy7nb")))

(define-public crate-legal_chess-0.4.41 (c (n "legal_chess") (v "0.4.41") (h "1nj9jx1z7v16l9j7b8y5h29qn5vi51dhnan51wg4nn559dwvl6my")))

(define-public crate-legal_chess-0.4.42 (c (n "legal_chess") (v "0.4.42") (h "0v4csx6q3brcmbr7bkcnbqwandzr3qj0v917aqgnr3jwlb417971")))

