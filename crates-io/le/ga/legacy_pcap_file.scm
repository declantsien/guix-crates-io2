(define-module (crates-io le ga legacy_pcap_file) #:use-module (crates-io))

(define-public crate-legacy_pcap_file-0.1.0 (c (n "legacy_pcap_file") (v "0.1.0") (d (list (d (n "hex") (r "^0.4") (d #t) (k 2)) (d (n "parameterized") (r "^1") (d #t) (k 2)) (d (n "tempfile") (r "^3.3") (d #t) (k 2)))) (h "1gqmn7jgx6cy9128a0fwvmnqwnzmip09gr2wyh3pbyij1y5rnmck")))

