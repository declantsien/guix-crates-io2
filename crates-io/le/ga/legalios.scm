(define-module (crates-io le ga legalios) #:use-module (crates-io))

(define-public crate-legalios-0.0.7 (c (n "legalios") (v "0.0.7") (d (list (d (n "rust_decimal") (r "^1.16") (d #t) (k 0)) (d (n "rust_decimal_macros") (r "^1.16") (d #t) (k 0)))) (h "14islswjnaiahzz983vy1l9y5zkls3whjydjslxf800r1vr0mpb7")))

(define-public crate-legalios-0.0.8 (c (n "legalios") (v "0.0.8") (d (list (d (n "rust_decimal") (r "^1.16") (d #t) (k 0)) (d (n "rust_decimal_macros") (r "^1.16") (d #t) (k 0)))) (h "0zjvvjqpdg4whlac6zgrw4fiayfphvfclvc8xb32ixzhmliz71zs")))

(define-public crate-legalios-0.0.9 (c (n "legalios") (v "0.0.9") (d (list (d (n "rust_decimal") (r "^1.16") (d #t) (k 0)) (d (n "rust_decimal_macros") (r "^1.16") (d #t) (k 0)))) (h "1jzkyxjnpl413gai2h6miyh39f0pg5h5xjxpil2f08cnffxkhv9i")))

(define-public crate-legalios-0.0.10 (c (n "legalios") (v "0.0.10") (d (list (d (n "rust_decimal") (r "^1.16") (d #t) (k 0)) (d (n "rust_decimal_macros") (r "^1.16") (d #t) (k 0)))) (h "1hr25ynaj7rnqxglkklkyvfpq6kz10jk1a1hrf5ykpwvz6cnsa43")))

(define-public crate-legalios-0.0.11 (c (n "legalios") (v "0.0.11") (d (list (d (n "rust_decimal") (r "^1.16") (d #t) (k 0)) (d (n "rust_decimal_macros") (r "^1.16") (d #t) (k 0)))) (h "094vk3ax8ad0i1wd4nr31zchcb90r4h582pq4kdzsbgn2bdv8s19")))

(define-public crate-legalios-0.0.12 (c (n "legalios") (v "0.0.12") (d (list (d (n "rust_decimal") (r "^1.16") (d #t) (k 0)) (d (n "rust_decimal_macros") (r "^1.16") (d #t) (k 0)))) (h "07zif4csda186s0f1cy4rmn3z17dxskl0vnp0ngfiv91x63nwwj5")))

(define-public crate-legalios-0.22.2 (c (n "legalios") (v "0.22.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.16") (d #t) (k 0)) (d (n "rust_decimal_macros") (r "^1.16") (d #t) (k 0)))) (h "1p03m6xv20hj53vj4q7v3h6inlqnv1hin4m35lhd1s4919c2c0q3")))

(define-public crate-legalios-0.22.13 (c (n "legalios") (v "0.22.13") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.16") (d #t) (k 0)) (d (n "rust_decimal_macros") (r "^1.16") (d #t) (k 0)))) (h "0cwlgxx45lxhvwb230g79m5b8rqm2gdsd4dh07xmmd0hwly6l7qh")))

