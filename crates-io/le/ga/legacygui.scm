(define-module (crates-io le ga legacygui) #:use-module (crates-io))

(define-public crate-legacygui-0.1.0 (c (n "legacygui") (v "0.1.0") (d (list (d (n "crossbeam-channel") (r "^0.5.6") (d #t) (k 0)) (d (n "diskit") (r "^0.1.1") (d #t) (k 0)) (d (n "eframe") (r "^0.19.0") (d #t) (k 0)) (d (n "egui") (r "^0.19.0") (d #t) (k 0)) (d (n "either") (r "^1.8.0") (d #t) (k 0)) (d (n "legacylisten") (r "^0.2.0") (d #t) (k 0)) (d (n "legacytranslate") (r "^0.2.0") (d #t) (k 0)))) (h "1nvlgkdq9wg8ys7sc7ikwmb5pjzv67pa5gykvpnmy7f0lni4jijz")))

(define-public crate-legacygui-0.1.1 (c (n "legacygui") (v "0.1.1") (d (list (d (n "crossbeam-channel") (r "^0.5.6") (d #t) (k 0)) (d (n "diskit") (r "^0.1.2") (d #t) (k 0)) (d (n "eframe") (r "^0.21.3") (d #t) (k 0)) (d (n "egui") (r "^0.21.0") (d #t) (k 0)) (d (n "either") (r "^1.8.1") (d #t) (k 0)) (d (n "legacylisten") (r "^0.2.1") (d #t) (k 0)) (d (n "legacytranslate") (r "^0.2.1") (d #t) (k 0)))) (h "03ncmmgsw8d0jndbx3wvajdabfrx80bspjbdazbwp3h46c620g01")))

(define-public crate-legacygui-0.1.2 (c (n "legacygui") (v "0.1.2") (d (list (d (n "crossbeam-channel") (r "^0.5.8") (d #t) (k 0)) (d (n "diskit") (r "^0.1.3") (d #t) (k 0)) (d (n "eframe") (r "^0.23.0") (d #t) (k 0)) (d (n "egui") (r "^0.23.0") (d #t) (k 0)) (d (n "either") (r "^1.9.0") (d #t) (k 0)) (d (n "legacylisten") (r "^0.3.2") (d #t) (k 0)) (d (n "legacytranslate") (r "^0.3.1") (d #t) (k 0)))) (h "02lajysxab6xfy4h6cm8i9i3p4a5s36bk4zjfsqm96p287c57mid")))

