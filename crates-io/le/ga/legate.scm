(define-module (crates-io le ga legate) #:use-module (crates-io))

(define-public crate-legate-0.0.0 (c (n "legate") (v "0.0.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "base64") (r "^0.21") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "hyper") (r "^0.14") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "16yl55gfaxrcivis92924hrdqbslslhfx22gl4zwcq88ipnihr7b") (r "1.69")))

