(define-module (crates-io le op leopard-codec) #:use-module (crates-io))

(define-public crate-leopard-codec-0.1.0 (c (n "leopard-codec") (v "0.1.0") (d (list (d (n "bytes") (r "^1.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "proptest") (r "^1.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "test-strategy") (r "^0.3") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "078s3ffq4afbyag1d9jhr4gnvcqqfr2y05d9gmfqh8xx2k2dnn7f")))

