(define-module (crates-io le da leda) #:use-module (crates-io))

(define-public crate-leda-0.1.0 (c (n "leda") (v "0.1.0") (d (list (d (n "openssl") (r "^0.10") (f (quote ("vendored"))) (d #t) (k 0)) (d (n "pyo3") (r "^0.15.1") (f (quote ("extension-module"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "143l3zrpg6mq2mnrpb5a8dcshm1c4w90mwfmgxf9dc5z9bpv4b5j") (f (quote (("py_bindings" "pyo3"))))))

(define-public crate-leda-0.2.0 (c (n "leda") (v "0.2.0") (d (list (d (n "rustls") (r "^0.20.6") (f (quote ("dangerous_configuration"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "1myv9b7p0mma9lacp5zzbxb3lk3gxv7yrn922m9yh52p7jv84jv6")))

(define-public crate-leda-0.3.0 (c (n "leda") (v "0.3.0") (d (list (d (n "rustls") (r "^0.20.6") (f (quote ("dangerous_configuration"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "16iygl02f9hh983b61i1bhknq2a4s2mrlbh5qll6f8qgwvx3x310")))

(define-public crate-leda-0.4.0 (c (n "leda") (v "0.4.0") (d (list (d (n "async-rustls") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "async-std") (r "^1.12.0") (o #t) (d #t) (k 0)) (d (n "rustls") (r "^0.20.6") (f (quote ("dangerous_configuration"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "089vraby2x6286jap9wib3psiv5l8fl83qzayrpsgb8vcj1kjanz") (s 2) (e (quote (("async" "dep:async-rustls" "dep:async-std"))))))

(define-public crate-leda-0.4.1 (c (n "leda") (v "0.4.1") (d (list (d (n "async-rustls") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "async-std") (r "^1.12.0") (o #t) (d #t) (k 0)) (d (n "rustls") (r "^0.20.6") (f (quote ("dangerous_configuration"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "18mgqmwzyfb8r777842m10b0dk44hvkbiwhyhpq507r9fmj9hzp8") (s 2) (e (quote (("async" "dep:async-rustls" "dep:async-std"))))))

(define-public crate-leda-0.4.2 (c (n "leda") (v "0.4.2") (d (list (d (n "async-rustls") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "async-std") (r "^1.12.0") (o #t) (d #t) (k 0)) (d (n "rustls") (r "^0.20.6") (f (quote ("dangerous_configuration"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "1hwym0jmgh7k946rykq7xv3wpwhfpprwf38f22pzlzw18rd5p907") (s 2) (e (quote (("async" "dep:async-rustls" "dep:async-std"))))))

(define-public crate-leda-0.5.0 (c (n "leda") (v "0.5.0") (d (list (d (n "async-rustls") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "async-std") (r "^1.12.0") (o #t) (d #t) (k 0)) (d (n "rustls") (r "^0.20.6") (f (quote ("dangerous_configuration"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "109ym6b698mi1p0xpkg7rlzvnfya90mxw7r5ckrs7lr0y3s5wqs2") (s 2) (e (quote (("async" "dep:async-rustls" "dep:async-std"))))))

