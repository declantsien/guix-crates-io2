(define-module (crates-io le ct lecturn) #:use-module (crates-io))

(define-public crate-lecturn-0.1.0 (c (n "lecturn") (v "0.1.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "termion") (r "^1") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1") (d #t) (k 0)))) (h "096a39hibgpp84l5pd1hq2mrc495af296h1qrlm1xvrjwhbng8h3")))

(define-public crate-lecturn-0.1.2 (c (n "lecturn") (v "0.1.2") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "termion") (r "^1") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1") (d #t) (k 0)))) (h "02a2kh62cl0na7r5dnvgn8xjzkzclddimd3pp9rd926icx1kjfq9")))

(define-public crate-lecturn-0.1.4 (c (n "lecturn") (v "0.1.4") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "termion") (r "^1") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1") (d #t) (k 0)))) (h "1x87nd9cm6550mp3n0jnf5mwrsfl3hz4li8v3msjcgz2ji1qvl02")))

(define-public crate-lecturn-0.1.5 (c (n "lecturn") (v "0.1.5") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "termion") (r "^1") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1") (d #t) (k 0)))) (h "0nyynskpwnvhl38rb8qp5wmn5yjksb1xy6dvwh4wna9w97yvnj6c")))

(define-public crate-lecturn-0.1.6 (c (n "lecturn") (v "0.1.6") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "termion") (r "^1") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1") (d #t) (k 0)))) (h "0flxkjrnxhwc3qilly8vyxs2xaairs72gy0m3azi2xzcciw2mhis")))

(define-public crate-lecturn-0.1.7 (c (n "lecturn") (v "0.1.7") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "termion") (r "^1") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1") (d #t) (k 0)))) (h "0ghb3v9ia3hpdbgih7kdjciiyg550yf6fa99shbcia7dhzh1z0z6")))

(define-public crate-lecturn-0.1.8 (c (n "lecturn") (v "0.1.8") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "termion") (r "^1") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1") (d #t) (k 0)))) (h "0lhwbnvq5xylz0sjgm0l3vi74lmg3rk72b2nkg1xm2mw2b54wk8a")))

(define-public crate-lecturn-0.1.9 (c (n "lecturn") (v "0.1.9") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "termion") (r "^1") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1") (d #t) (k 0)))) (h "1idnda3m255ny3gmma2jbrw6j4xyapfyh6zjbkpd8n2z10q78qyv")))

