(define-module (crates-io le ct lector-tts) #:use-module (crates-io))

(define-public crate-lector-tts-0.1.0 (c (n "lector-tts") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.77") (d #t) (k 0)) (d (n "cocoa") (r "^0.25.0") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "cocoa-foundation") (r "^0.1") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "objc") (r "^0.2") (f (quote ("exception"))) (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "tts") (r "^0.25.6") (d #t) (k 0)))) (h "1nvbb392jpwvdsy3zah3466c22nlflmn4p95im3af613q672vdw3")))

