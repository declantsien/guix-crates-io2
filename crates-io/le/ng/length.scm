(define-module (crates-io le ng length) #:use-module (crates-io))

(define-public crate-length-0.1.0 (c (n "length") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "190pk5sibfjrbj2vj3233ghvfiw45f2lzvjl2q3ddn7686af75d4")))

(define-public crate-length-0.2.0 (c (n "length") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "05p6c5ac1xaa4hlyfldvnzy8di7n1qlzn4jmc3vb1p8lns1ajprv")))

(define-public crate-length-0.2.1 (c (n "length") (v "0.2.1") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "regex") (r "^1.6") (d #t) (k 0)))) (h "00rv0dqyq60zpji7raqbkppxw8crxrp7a379pqh5m6pp4aajpf7w")))

(define-public crate-length-0.2.2 (c (n "length") (v "0.2.2") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "regex") (r "^1.7") (d #t) (k 0)))) (h "0pz8zwzxn2j77avff2gkfq74qqnz0cwxsvs546ynh5gcradwxsmi")))

