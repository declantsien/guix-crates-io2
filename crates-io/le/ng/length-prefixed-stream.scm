(define-module (crates-io le ng length-prefixed-stream) #:use-module (crates-io))

(define-public crate-length-prefixed-stream-1.0.0 (c (n "length-prefixed-stream") (v "1.0.0") (d (list (d (n "async-std") (r "^1.9.0") (d #t) (k 0)) (d (n "desert") (r "^2.0.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.12") (d #t) (k 0)) (d (n "futures-core") (r "^0.3.12") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2.4") (d #t) (k 0)))) (h "17mzv0c78hksla41201fps75l6d6nmdm2r7l0bk249l3wk2qmacs")))

