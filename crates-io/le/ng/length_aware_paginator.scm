(define-module (crates-io le ng length_aware_paginator) #:use-module (crates-io))

(define-public crate-length_aware_paginator-0.1.0 (c (n "length_aware_paginator") (v "0.1.0") (d (list (d (n "diesel") (r "^1.4.5") (f (quote ("postgres"))) (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1ha930fjyk80ilhrlw7kz0fl32ifhx1150cqm0qy9j8wc27igial")))

(define-public crate-length_aware_paginator-0.3.0 (c (n "length_aware_paginator") (v "0.3.0") (d (list (d (n "diesel") (r "^1.4.5") (f (quote ("postgres"))) (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0x5mpwr400f6a92r0gs6kvy0i3jirvn8sx60ig1v0kf4x3zyvawg")))

(define-public crate-length_aware_paginator-1.0.0 (c (n "length_aware_paginator") (v "1.0.0") (d (list (d (n "diesel") (r "^2.0.0-rc.0") (f (quote ("postgres"))) (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1249wk7b4sl2vdvcq6wwhlb8013zc2v4dp8lw175wrmpr32ii5xz")))

