(define-module (crates-io le xx lexx) #:use-module (crates-io))

(define-public crate-lexx-0.1.0 (c (n "lexx") (v "0.1.0") (d (list (d (n "syntex_errors") (r "^0.46.0") (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.46.0") (d #t) (k 0)))) (h "1zzh2dd2cz1dll2kv6r367md4fcgrpcqlmjgz676mipy43lh0wm5")))

