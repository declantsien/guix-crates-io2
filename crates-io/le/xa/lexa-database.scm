(define-module (crates-io le xa lexa-database) #:use-module (crates-io))

(define-public crate-lexa-database-0.1.0 (c (n "lexa-database") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "sqlx") (r "^0.7") (d #t) (k 0)) (d (n "sqlx") (r "^0.7") (f (quote ("runtime-tokio"))) (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.32") (f (quote ("macros" "test-util"))) (d #t) (k 2)))) (h "1v3r0gkrb8r8gq0fh4k88k12jcxhj8b7pa86fan24wc86gyyakbz") (f (quote (("postgres-sgbd" "sqlx/postgres"))))))

(define-public crate-lexa-database-0.1.1 (c (n "lexa-database") (v "0.1.1") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "sqlx") (r "^0.7") (d #t) (k 0)) (d (n "sqlx") (r "^0.7") (f (quote ("runtime-tokio"))) (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.32") (f (quote ("macros" "test-util"))) (d #t) (k 2)))) (h "1xzyq4vls81ik7vs5f4pxfz9xinh75dkgimqk2flw0i8pnd7whyh") (f (quote (("postgres-sgbd" "sqlx/postgres"))))))

(define-public crate-lexa-database-0.1.2 (c (n "lexa-database") (v "0.1.2") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "sqlx") (r "^0.7") (d #t) (k 0)) (d (n "sqlx") (r "^0.7") (f (quote ("runtime-tokio"))) (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.32") (f (quote ("macros" "test-util"))) (d #t) (k 2)))) (h "0c4jl14ryhr1l2zlnjxvynx9r3ras5sji29vqx6s299fna03h3g2") (f (quote (("postgres-sgbd" "sqlx/postgres"))))))

(define-public crate-lexa-database-0.1.3 (c (n "lexa-database") (v "0.1.3") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "sqlx") (r "^0.7") (f (quote ("runtime-tokio"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.32") (f (quote ("macros" "test-util"))) (d #t) (k 2)))) (h "1p8p0n7pkw2i8l9bznhcg96mgpf9i3vszfwwck6cfcp4l3wh4z5m") (f (quote (("postgres-sgbd" "sqlx/postgres"))))))

(define-public crate-lexa-database-0.1.4 (c (n "lexa-database") (v "0.1.4") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "sqlx") (r "^0.7") (f (quote ("runtime-tokio"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.32") (f (quote ("macros" "test-util"))) (d #t) (k 2)))) (h "1kzppki3aakx9bwjzjx67czn07q1w9a6fq0hsr6w5hj4cvfcqnri") (f (quote (("postgres-sgbd" "sqlx/postgres"))))))

