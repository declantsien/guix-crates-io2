(define-module (crates-io le xa lexa-framework-macro) #:use-module (crates-io))

(define-public crate-lexa-framework-macro-0.1.3 (c (n "lexa-framework-macro") (v "0.1.3") (d (list (d (n "diagnostics") (r "^0.10") (k 0) (p "proc-macro2-diagnostics")) (d (n "lexa-syn") (r "^0.1") (d #t) (k 0)) (d (n "rstml") (r "^0.11") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^1.4") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "0n5ah395hszbk2q22whf59nzgkz8avcq3qb1rir45rz3rpdwcfbw")))

(define-public crate-lexa-framework-macro-0.1.4 (c (n "lexa-framework-macro") (v "0.1.4") (d (list (d (n "diagnostics") (r "^0.10") (k 0) (p "proc-macro2-diagnostics")) (d (n "lexa-syn") (r "^0.1") (d #t) (k 0)) (d (n "rstml") (r "^0.11") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^1.4") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "1lswl123i7d18v6r1hk08ixyha7bypqh3cfhigqrgazkpswcpdbw")))

(define-public crate-lexa-framework-macro-0.2.5 (c (n "lexa-framework-macro") (v "0.2.5") (d (list (d (n "diagnostics") (r "^0.10") (k 0) (p "proc-macro2-diagnostics")) (d (n "lexa-syn") (r "^0.1") (d #t) (k 0)) (d (n "rstml") (r "^0.11") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^1.4") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "1a30skpl231fxsnijmk7k76hphskxy0hs6vjp9632fy62v01nscl")))

