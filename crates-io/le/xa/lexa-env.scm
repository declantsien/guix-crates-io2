(define-module (crates-io le xa lexa-env) #:use-module (crates-io))

(define-public crate-lexa-env-2.0.0 (c (n "lexa-env") (v "2.0.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-env") (r "^0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0nxjyx38wff23w86sxqym4niaagrzif53vp3m3jrciz5bdxpdbyf") (y #t)))

(define-public crate-lexa-env-0.1.0 (c (n "lexa-env") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-env") (r "^0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0xa1sw8k9c1v3l2bacjvj7bfkx41wbil47vy8bzrvzwpgjla7l9p")))

(define-public crate-lexa-env-0.1.1 (c (n "lexa-env") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-env") (r "^0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "00092hzxq7r77fg3223v1v7h1ljsya00ab01cgwwb4lcqakxf526")))

(define-public crate-lexa-env-0.1.2 (c (n "lexa-env") (v "0.1.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-env") (r "^0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1alza1qk1yqq178fwcypmgkx7wmadlz32qxb5vr1y4m7931q80ss")))

