(define-module (crates-io le xa lexactivator) #:use-module (crates-io))

(define-public crate-lexactivator-3.0.0 (c (n "lexactivator") (v "3.0.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1di9n8wsvchpgyhar1zm0fn13kfk5b8z6fia6scykkzc65icjk9g")))

(define-public crate-lexactivator-3.21.0 (c (n "lexactivator") (v "3.21.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "100pwi3swdh3cgf3g8518724lapqbry2smjxgp1dmcpcypvi09va")))

(define-public crate-lexactivator-3.21.1 (c (n "lexactivator") (v "3.21.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0dplzi5vraw965dhvwn0qnwc9hid2ficcgc181fkcqlkrsbgrr7q")))

(define-public crate-lexactivator-3.21.2 (c (n "lexactivator") (v "3.21.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "18j4dnsi0lsysv9hl5yhh7hj5rkxh5q05bzmzpb64xay2sq36rqr")))

(define-public crate-lexactivator-3.21.3 (c (n "lexactivator") (v "3.21.3") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 1)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0vmsnx0l4rsvi29fh7w24nzz9dlgibls3aaxbkawxl90xgh59xg5")))

(define-public crate-lexactivator-3.21.4 (c (n "lexactivator") (v "3.21.4") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 1)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0q13z7yg9j9v6b1z9dsxnsz1m8ggqywg8a58njl19qpk8cwaj61b")))

(define-public crate-lexactivator-3.22.0 (c (n "lexactivator") (v "3.22.0") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 1)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1l25zl3ap6vcpgfn39c4icnx4y7y7kv3h60h18zb6asmmzwgh6vl")))

(define-public crate-lexactivator-3.22.1 (c (n "lexactivator") (v "3.22.1") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 1)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1vwz9zrp2asyfmw577g2fbirhw826jp47gq20q37jnkzddc4ys13")))

(define-public crate-lexactivator-3.22.2 (c (n "lexactivator") (v "3.22.2") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 1)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "06j10vhfxvrgpiylrgicf704v34rdf9rifvq5liy04wy9iw1zd7z")))

