(define-module (crates-io le xa lexa-prompt) #:use-module (crates-io))

(define-public crate-lexa-prompt-0.1.0 (c (n "lexa-prompt") (v "0.1.0") (d (list (d (n "inquire") (r "^0.6") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1cxwxdqmysplaj98113la7mi74ad4jx7d8xf3x3hasn63v089i0h")))

(define-public crate-lexa-prompt-0.1.1 (c (n "lexa-prompt") (v "0.1.1") (d (list (d (n "inquire") (r "^0.6") (d #t) (k 0)) (d (n "lexa-prompt-macro") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1gc64jdrgj0kwcdrgix0v8i50gfhg5y2207v2iyv1xjgr7g8sqqg")))

(define-public crate-lexa-prompt-0.1.2 (c (n "lexa-prompt") (v "0.1.2") (d (list (d (n "inquire") (r "^0.6") (d #t) (k 0)) (d (n "lexa-prompt-macro") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "19153gj6ncs2fq3rx1h873lkyp5yjn2qb7ak49shskch7gjpwk7s")))

(define-public crate-lexa-prompt-0.1.3 (c (n "lexa-prompt") (v "0.1.3") (d (list (d (n "inquire") (r "^0.6") (d #t) (k 0)) (d (n "lexa-prompt-macro") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0aaak8yk5jaykwp24406bz9fchm2mi5fbpqp25iak9cnrwv5gcg0") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-lexa-prompt-0.1.4 (c (n "lexa-prompt") (v "0.1.4") (d (list (d (n "inquire") (r "^0.6") (d #t) (k 0)) (d (n "lexa-prompt-macro") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "03lbxlh2krgl9zm2k98r8gsk6pjjvrhyc8f6pjjbmda3kwcm94v5") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-lexa-prompt-0.1.5 (c (n "lexa-prompt") (v "0.1.5") (d (list (d (n "inquire") (r "^0.7") (d #t) (k 0)) (d (n "lexa-prompt-macro") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "08iq2nm4rd66cnqchyk1y6254ain2ppjsgzl9fdjm23l4887gvmc") (s 2) (e (quote (("serde" "dep:serde"))))))

