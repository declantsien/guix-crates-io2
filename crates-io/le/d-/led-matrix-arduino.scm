(define-module (crates-io le d- led-matrix-arduino) #:use-module (crates-io))

(define-public crate-led-matrix-arduino-0.1.0 (c (n "led-matrix-arduino") (v "0.1.0") (h "1cq1nin4h46ij5q1ac439r25sb48ks5xrabzlimcbhx3bf6pk5hi")))

(define-public crate-led-matrix-arduino-0.1.1 (c (n "led-matrix-arduino") (v "0.1.1") (h "1win6yhj5ywnymkpb1z23ihw1k7y58pwx87c2z5r96h87wbwpdp7")))

(define-public crate-led-matrix-arduino-0.1.2 (c (n "led-matrix-arduino") (v "0.1.2") (h "08ifiia76is866bvrvcd9i61y14l9qhvllhabjv9qdpgf15i26nk")))

(define-public crate-led-matrix-arduino-0.1.3 (c (n "led-matrix-arduino") (v "0.1.3") (h "0a31prrl90sf9jacv2as2z81ba0v8c2f8wc0jb27bj4lmikjlwbg")))

