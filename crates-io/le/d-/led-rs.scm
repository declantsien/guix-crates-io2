(define-module (crates-io le d- led-rs) #:use-module (crates-io))

(define-public crate-led-rs-0.1.0 (c (n "led-rs") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1p61lcddr09nndrg7zah9aaalr754kx41qcxck6yr4nl3za44zc8")))

