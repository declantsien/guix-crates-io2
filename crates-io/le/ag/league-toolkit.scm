(define-module (crates-io le ag league-toolkit) #:use-module (crates-io))

(define-public crate-league-toolkit-0.1.0 (c (n "league-toolkit") (v "0.1.0") (d (list (d (n "getset") (r "^0.1.1") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0w5p9rn0zyn0jxnkbcixdxmislki1gmw7kzg1pac1dj9cc3bq8h9")))

(define-public crate-league-toolkit-0.1.1 (c (n "league-toolkit") (v "0.1.1") (d (list (d (n "getset") (r "^0.1.1") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0a21a47zksndj8is9mxg1nmmpsmwn0q2pj3cw7vr8r35qv7dcdm8")))

