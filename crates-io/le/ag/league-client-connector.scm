(define-module (crates-io le ag league-client-connector) #:use-module (crates-io))

(define-public crate-league-client-connector-0.1.0 (c (n "league-client-connector") (v "0.1.0") (d (list (d (n "base64") (r "^0.12") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "snafu") (r "^0.6") (d #t) (k 0)))) (h "0zjc651d2mg3spfihqkf7ji512b24b89jwz0lfr42g48hhcpf3y3")))

(define-public crate-league-client-connector-0.1.1 (c (n "league-client-connector") (v "0.1.1") (d (list (d (n "base64") (r "^0.12") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "snafu") (r "^0.6") (d #t) (k 0)))) (h "027igy5bqfd30sa8ryxgg5aikxmk1mv1qnqbglfqbf7nk9w4lb18")))

(define-public crate-league-client-connector-0.1.2 (c (n "league-client-connector") (v "0.1.2") (d (list (d (n "base64") (r "^0.12") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "snafu") (r "^0.6") (d #t) (k 0)))) (h "1arfhp1q8kj4ay9xc7jbh4sg4by5zr24sf2n57kzxnh1spwqab5y")))

