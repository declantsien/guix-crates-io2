(define-module (crates-io le ag league-of-crates) #:use-module (crates-io))

(define-public crate-league-of-crates-0.1.0 (c (n "league-of-crates") (v "0.1.0") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.178") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.104") (d #t) (k 0)))) (h "07zk6zri2pcjl8cghkbxsmgjvvbav0jnpk8a32mwp4b5qnfqc6wh")))

(define-public crate-league-of-crates-0.1.1 (c (n "league-of-crates") (v "0.1.1") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.178") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.104") (d #t) (k 0)))) (h "1fck30zyizii2qd6mxlw081xix54ijgcznbahlq9ap5j23y3zjxf")))

(define-public crate-league-of-crates-0.2.0 (c (n "league-of-crates") (v "0.2.0") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.178") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.104") (f (quote ("unbounded_depth"))) (d #t) (k 0)))) (h "1qld9cr0cjlf30ia4kpbc3q70h0lvg77234a2rr90alg0k1narxd")))

(define-public crate-league-of-crates-0.3.0 (c (n "league-of-crates") (v "0.3.0") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (f (quote ("json" "serde_json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.178") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.104") (d #t) (k 0)) (d (n "tokio") (r "^1.31.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0gdlm5zqzca5fj0xlcv91y3ipljl9mmavqxk3m6kdwmwb5hv2mqq")))

(define-public crate-league-of-crates-0.3.1 (c (n "league-of-crates") (v "0.3.1") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (f (quote ("json" "serde_json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.178") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.104") (d #t) (k 0)) (d (n "tokio") (r "^1.31.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0lwvibhn5fpvx4zmj51x9k97qpd5j374pvafpknmj8bq84k6r0qc")))

(define-public crate-league-of-crates-0.3.2 (c (n "league-of-crates") (v "0.3.2") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (f (quote ("json" "serde_json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.178") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.104") (d #t) (k 0)) (d (n "tokio") (r "^1.31.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0m8sn6ydy5f5gz6js87czr61g0r9s574q70mj47va3hfr6wvc439")))

(define-public crate-league-of-crates-0.3.3 (c (n "league-of-crates") (v "0.3.3") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (f (quote ("json" "serde_json"))) (d #t) (k 0)) (d (n "serde") (r "=1.0.171") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.104") (d #t) (k 0)) (d (n "tokio") (r "^1.31.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0gs22ypa9zai78p4qd9p7d2f6pmxylv7y61gxqdgfdxf44dl9vsy")))

(define-public crate-league-of-crates-0.3.4 (c (n "league-of-crates") (v "0.3.4") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (f (quote ("json" "serde_json"))) (d #t) (k 0)) (d (n "serde") (r "=1.0.171") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.104") (d #t) (k 0)) (d (n "tokio") (r "^1.31.0") (f (quote ("full"))) (d #t) (k 0)))) (h "03jj5vsykqqnswh3llihhhc5ck1gdqy3mp46yjx7qdkmb951ards")))

