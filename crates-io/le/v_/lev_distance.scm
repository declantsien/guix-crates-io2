(define-module (crates-io le v_ lev_distance) #:use-module (crates-io))

(define-public crate-lev_distance-0.1.0 (c (n "lev_distance") (v "0.1.0") (h "0nw2rqbqcda7ps11c235wjallqwsdhkcg939bf9iqqpmxj4ls8sd")))

(define-public crate-lev_distance-0.1.1 (c (n "lev_distance") (v "0.1.1") (h "0pk26fp1fcjyg2ml8g5ma1jj2gvgnmmri4md8y3bqdjr46yx3nbj")))

