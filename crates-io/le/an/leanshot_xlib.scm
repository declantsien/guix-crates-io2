(define-module (crates-io le an leanshot_xlib) #:use-module (crates-io))

(define-public crate-leanshot_xlib-0.1.0 (c (n "leanshot_xlib") (v "0.1.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "x11") (r "^2.18") (f (quote ("xlib"))) (d #t) (k 0)))) (h "0vmb9f66z2rar6vkrrfl5h7wmrx3d700lwpj05zc17pm8aja9ib4")))

(define-public crate-leanshot_xlib-0.1.1 (c (n "leanshot_xlib") (v "0.1.1") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "x11") (r "^2.18") (f (quote ("xlib"))) (d #t) (k 0)))) (h "04qgg7b44265rsd4vrldaxyngn0q18rq2dpnmk3lfdsj54j6gmvl")))

