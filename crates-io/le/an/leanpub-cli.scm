(define-module (crates-io le an leanpub-cli) #:use-module (crates-io))

(define-public crate-leanpub-cli-0.1.0 (c (n "leanpub-cli") (v "0.1.0") (d (list (d (n "error-chain") (r "^0.12.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.89") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)) (d (n "structopt") (r "^0.2.14") (d #t) (k 0)))) (h "0mycchfzcxwvhybqpb9cf8c3qk2ck00asc9cm9c23iq29sb0515g")))

(define-public crate-leanpub-cli-0.1.1 (c (n "leanpub-cli") (v "0.1.1") (d (list (d (n "error-chain") (r "^0.12.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.89") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)) (d (n "structopt") (r "^0.2.14") (d #t) (k 0)))) (h "1b7zb3hs9xlkn15r4s7c4qnp8p8vww99w35zgxa54x80kqfbwv3x")))

