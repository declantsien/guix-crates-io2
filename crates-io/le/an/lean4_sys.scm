(define-module (crates-io le an lean4_sys) #:use-module (crates-io))

(define-public crate-lean4_sys-0.1.0 (c (n "lean4_sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.65.1") (f (quote ("experimental" "runtime"))) (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "03sm7da72adrf43kfg4vkln0qx8ca25scdnxpdmh7baq3krrmqig") (y #t)))

