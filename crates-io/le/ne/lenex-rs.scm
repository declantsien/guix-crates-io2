(define-module (crates-io le ne lenex-rs) #:use-module (crates-io))

(define-public crate-lenex-rs-0.0.1 (c (n "lenex-rs") (v "0.0.1") (d (list (d (n "chrono") (r "^0.4.23") (f (quote ("serde"))) (d #t) (k 0)) (d (n "chrono-tz") (r "^0.8.1") (d #t) (k 0)) (d (n "fast-xml") (r "^0.23.1") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "zip") (r "^0.6.3") (d #t) (k 0)))) (h "0l9p7wmqj0ckz85wssd7661bh4yk7zsgqrvjap258qk0wjkdzjjd") (y #t)))

(define-public crate-lenex-rs-0.0.2 (c (n "lenex-rs") (v "0.0.2") (d (list (d (n "chrono") (r "^0.4.23") (f (quote ("serde"))) (d #t) (k 0)) (d (n "chrono-tz") (r "^0.8.1") (d #t) (k 0)) (d (n "fast-xml") (r "^0.23.1") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "zip") (r "^0.6.3") (d #t) (k 0)))) (h "1bp3z022gnb6s7i50bhqnwkhhvkyhvq7w0pcclnydmn3cl6jm6f0") (y #t)))

(define-public crate-lenex-rs-0.0.3-beta1 (c (n "lenex-rs") (v "0.0.3-beta1") (d (list (d (n "chrono") (r "^0.4.23") (f (quote ("serde"))) (d #t) (k 0)) (d (n "chrono-tz") (r "^0.8.1") (d #t) (k 0)) (d (n "derive_builder") (r "^0.12.0") (d #t) (k 0)) (d (n "fast-xml") (r "^0.23.1") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "zip") (r "^0.6.3") (d #t) (k 0)))) (h "0d26wsr9vqsxv1brspq4mccw4f20xa5vw9gvacbydha3671k8bzz") (y #t)))

(define-public crate-lenex-rs-0.0.3 (c (n "lenex-rs") (v "0.0.3") (d (list (d (n "chrono") (r "^0.4.23") (f (quote ("serde"))) (d #t) (k 0)) (d (n "chrono-tz") (r "^0.8.1") (d #t) (k 0)) (d (n "derive_builder") (r "^0.12.0") (d #t) (k 0)) (d (n "fast-xml") (r "^0.23.1") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "zip") (r "^0.6.3") (d #t) (k 0)))) (h "0cv8sc52w0bc6l9bls5kh2brsb6v6s0qyvw60ibn9w2mk53485nn")))

(define-public crate-lenex-rs-0.1.0 (c (n "lenex-rs") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.23") (f (quote ("serde"))) (d #t) (k 0)) (d (n "chrono-tz") (r "^0.8.1") (d #t) (k 0)) (d (n "fast-xml") (r "^0.23.1") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "zip") (r "^0.6.3") (d #t) (k 0)))) (h "1mcaxvcaagjbz505y0pyk1cdm429qkfmvdx5xfspb4ckbx0bbkqw")))

