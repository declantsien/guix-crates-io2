(define-module (crates-io le wp lewp) #:use-module (crates-io))

(define-public crate-lewp-0.0.0 (c (n "lewp") (v "0.0.0") (h "144s03dqxb84i2zfh5khsw9amyi8jaiakngqdq777cajbiz1x6kx")))

(define-public crate-lewp-0.2.0 (c (n "lewp") (v "0.2.0") (d (list (d (n "charsets") (r "^0.2.0") (d #t) (k 0)) (d (n "html5ever") (r "^0.25.1") (d #t) (k 0)) (d (n "langtag") (r "^0.2.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "markup5ever_rcdom") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "05j08jkndd4a7p24hnljnmizy3xrdyyglcdzh679dbif887md1xw") (f (quote (("submodules") ("full" "submodules"))))))

(define-public crate-lewp-0.3.0 (c (n "lewp") (v "0.3.0") (d (list (d (n "charsets") (r "^0.2.0") (d #t) (k 0)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 2)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "html5ever") (r "^0.25.1") (d #t) (k 0)) (d (n "langtag") (r "^0.2.0") (d #t) (k 0)) (d (n "lewp-css") (r "^0.1.22") (d #t) (k 0)) (d (n "lewp-html") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "markup5ever_rcdom") (r "^0.1.0") (d #t) (k 0)) (d (n "pathdiff") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "selectors") (r "^0.23.1") (d #t) (k 0) (p "lewp-selectors")) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "192rmbqbgcl13bx0svqd91bgwr4wg5g8sk1nk5ipxrasbgwki8dz")))

