(define-module (crates-io le wp lewp-html) #:use-module (crates-io))

(define-public crate-lewp-html-0.1.0 (c (n "lewp-html") (v "0.1.0") (d (list (d (n "charsets") (r "^0.2") (d #t) (k 0)) (d (n "html5ever") (r "^0.25") (d #t) (k 0)) (d (n "langtag") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rcdom") (r "^0.1") (d #t) (k 0) (p "markup5ever_rcdom")))) (h "1jmwgpvnjiicr98847cnnxjcynqi1xwylba8j9677fpmilhsykkr")))

(define-public crate-lewp-html-0.2.0 (c (n "lewp-html") (v "0.2.0") (d (list (d (n "charsets") (r "^0.2") (d #t) (k 0)) (d (n "html5ever") (r "^0.25") (d #t) (k 0)) (d (n "langtag") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rcdom") (r "^0.1") (d #t) (k 0) (p "markup5ever_rcdom")))) (h "13i5z80pk5al5lpw3sy9b5hp5wanrnd0b7v61p779lymrfmcnyf8")))

