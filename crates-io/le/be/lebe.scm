(define-module (crates-io le be lebe) #:use-module (crates-io))

(define-public crate-lebe-0.5.0 (c (n "lebe") (v "0.5.0") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "byteorder") (r "^1.3.2") (d #t) (k 2)))) (h "1anr45bm029xyaarmlan3sn9z1bk4l2vz176sfq0swr5jnz1swax")))

(define-public crate-lebe-0.5.1 (c (n "lebe") (v "0.5.1") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "byteorder") (r "^1.3.2") (d #t) (k 2)))) (h "1zr6g3d35h349j0dsx6722lrjws00x2d8z0sy5p9wxdhimlivzby")))

(define-public crate-lebe-0.5.2 (c (n "lebe") (v "0.5.2") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 2)))) (h "1j2l6chx19qpa5gqcw434j83gyskq3g2cnffrbl3842ymlmpq203")))

