(define-module (crates-io le o- leo-linter) #:use-module (crates-io))

(define-public crate-leo-linter-1.0.2 (c (n "leo-linter") (v "1.0.2") (h "0mny823i8v471a4y66qn20zpbvmkqsn2rvsadaywsm718jzp8iqw")))

(define-public crate-leo-linter-1.0.3 (c (n "leo-linter") (v "1.0.3") (h "044bdhkhj7p6a4f5kqggvdnymkjnsmnl06pwkcwh65cbnjmh926p")))

(define-public crate-leo-linter-1.0.4 (c (n "leo-linter") (v "1.0.4") (h "17lw8z88wr6skjcl094k09qml3wmlcwgz8frz9hv8i30ymn59pwy")))

(define-public crate-leo-linter-1.0.5 (c (n "leo-linter") (v "1.0.5") (h "114fjv5ilfa2gg4pgrv4ksx3y0amp24ib0qj2y86dm472j2mwxzp")))

(define-public crate-leo-linter-1.0.6 (c (n "leo-linter") (v "1.0.6") (h "02m7j06hgb7vm5qq7h4hqfxncax1iwx22krsrbqzbxwnms1x87j5")))

(define-public crate-leo-linter-1.0.7 (c (n "leo-linter") (v "1.0.7") (h "01p8rich7g86vsdd63yrc86vlk5hfgaacchi8xkcyvj44nijrwgs")))

(define-public crate-leo-linter-1.0.8 (c (n "leo-linter") (v "1.0.8") (h "1iylq0ps56j87jf0d27cfgn1aq6zavjjcshmjc2pvacaf35fdybh")))

(define-public crate-leo-linter-1.1.8 (c (n "leo-linter") (v "1.1.8") (h "0q4k2637x4w7liwa1mp7ax01ivncv8lb74bqkg2s1pcmynbhrbg2")))

(define-public crate-leo-linter-1.2.0 (c (n "leo-linter") (v "1.2.0") (h "0idvmj0d5v73jk70p8h32w2704zadrqxyw292f7vmj6978p20mv8")))

(define-public crate-leo-linter-1.2.1 (c (n "leo-linter") (v "1.2.1") (h "0zr4h3kf44a6jk4iq30lykczyp7aw6dzkprg9f8js888yxf02i06")))

(define-public crate-leo-linter-1.2.2 (c (n "leo-linter") (v "1.2.2") (h "0hc0mvgmh2925igrl0jymsb7cmy741nk858ka5wfzn6mqsfpa7sa")))

(define-public crate-leo-linter-1.2.3 (c (n "leo-linter") (v "1.2.3") (h "1mbwrwmj0g11qapy67f2g9jgfc1cszjhpl1f3d7hic0g0qdhhp77")))

(define-public crate-leo-linter-1.3.0 (c (n "leo-linter") (v "1.3.0") (h "11ifd0ab5h21bxqg58zarwpna8anmvfwyml5ma24777iv7zqls7z")))

(define-public crate-leo-linter-1.4.0 (c (n "leo-linter") (v "1.4.0") (h "1481h2qafvfdhyy0m7qpy7f0qgbz9ywkslgzgz4jvrpnby3hn3qd")))

(define-public crate-leo-linter-1.5.0 (c (n "leo-linter") (v "1.5.0") (h "1h99qn1q27r9lcbdk5dd7lazp376ya569n2swkgqzxaxkdm3gb12")))

(define-public crate-leo-linter-1.5.1 (c (n "leo-linter") (v "1.5.1") (h "1q600j1ddw7hpjp9hsm4mc60zza22wcrrijcimkf3wrz3p0rm08g")))

(define-public crate-leo-linter-1.5.2 (c (n "leo-linter") (v "1.5.2") (h "00c215rgwpbg7aawa48l9wmp30hwwkpv7rxndqg4fh08bah890hx")))

(define-public crate-leo-linter-1.5.3 (c (n "leo-linter") (v "1.5.3") (h "14mr8bxv1w2cy2fp9kx2bfr8w6p2iyv96kq7xr951q58q3hifkj4")))

