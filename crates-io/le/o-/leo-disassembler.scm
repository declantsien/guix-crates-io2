(define-module (crates-io le o- leo-disassembler) #:use-module (crates-io))

(define-public crate-leo-disassembler-1.11.0 (c (n "leo-disassembler") (v "1.11.0") (d (list (d (n "leo-ast") (r "=1.11.0") (d #t) (k 0)) (d (n "leo-errors") (r "^1.11.0") (d #t) (k 0)) (d (n "leo-span") (r "^1.11.0") (d #t) (k 0)) (d (n "snarkvm") (r "^0.16.19") (d #t) (k 0)))) (h "0ypi8m4x1gdkzphnjnl35yhl7fdckyy132j5398wxh83jmnbsba3") (r "1.69")))

(define-public crate-leo-disassembler-1.12.0 (c (n "leo-disassembler") (v "1.12.0") (d (list (d (n "leo-ast") (r "=1.12.0") (d #t) (k 0)) (d (n "leo-errors") (r "^1.12.0") (d #t) (k 0)) (d (n "leo-span") (r "^1.12.0") (d #t) (k 0)) (d (n "snarkvm") (r "^0.16.19") (d #t) (k 0)))) (h "0dl3z63v5vi334a6x3yla8m7k6pxb9m2xqrswfxkdgy7jyny02n4") (r "1.69")))

