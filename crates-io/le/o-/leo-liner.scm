(define-module (crates-io le o- leo-liner) #:use-module (crates-io))

(define-public crate-leo-liner-1.0.0 (c (n "leo-liner") (v "1.0.0") (h "1vypm2hr9qs395a4xywmfwvmcms15dvzbv7xjhwzkc21q0dvf437") (y #t)))

(define-public crate-leo-liner-1.0.1 (c (n "leo-liner") (v "1.0.1") (h "1p5winna3ss5n61ir51pa27m2hay0s5qidg0216rm3wbhgr7l2qp") (y #t)))

