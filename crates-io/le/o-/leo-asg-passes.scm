(define-module (crates-io le o- leo-asg-passes) #:use-module (crates-io))

(define-public crate-leo-asg-passes-1.3.0 (c (n "leo-asg-passes") (v "1.3.0") (d (list (d (n "leo-asg") (r "^1.3.0") (d #t) (k 0)))) (h "0dj14hzcga6yisjw8ih4l5clxwbwj3d94655g2nvfvzw00yvk4f5")))

(define-public crate-leo-asg-passes-1.4.0 (c (n "leo-asg-passes") (v "1.4.0") (d (list (d (n "leo-asg") (r "^1.4.0") (d #t) (k 0)))) (h "09pk7jin3rfzs78s1m45zabwqg0hw318gdj0c3c2p8hqcdw7d4fs")))

(define-public crate-leo-asg-passes-1.5.0 (c (n "leo-asg-passes") (v "1.5.0") (d (list (d (n "leo-asg") (r "^1.5.0") (d #t) (k 0)))) (h "02v9kkx87mkfgnj6h6y1rh4h4gssa9wi314ahixal32ya00djlim")))

(define-public crate-leo-asg-passes-1.5.1 (c (n "leo-asg-passes") (v "1.5.1") (d (list (d (n "leo-asg") (r "^1.5.1") (d #t) (k 0)))) (h "1n1182y1p9364jns0is8qinyi58zlvlpqz0r7wpvdzk6can0izjv")))

(define-public crate-leo-asg-passes-1.5.2 (c (n "leo-asg-passes") (v "1.5.2") (d (list (d (n "leo-asg") (r "^1.5.2") (d #t) (k 0)))) (h "1ssrb8dj7ya7ajis9xz7d9fnvx7wk7gh6spppl9gpn72bhacdyaw")))

(define-public crate-leo-asg-passes-1.5.3 (c (n "leo-asg-passes") (v "1.5.3") (d (list (d (n "leo-asg") (r "^1.5.3") (d #t) (k 0)))) (h "1lqmi406gvcvbkgk5p5rblds00jwax0ixzf169z7lv18nw7rcxhb")))

