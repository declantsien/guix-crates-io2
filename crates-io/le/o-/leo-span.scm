(define-module (crates-io le o- leo-span) #:use-module (crates-io))

(define-public crate-leo-span-1.6.0 (c (n "leo-span") (v "1.6.0") (d (list (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "indexmap") (r "^1.9") (f (quote ("serde"))) (d #t) (k 0)) (d (n "scoped-tls") (r "^1.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "03s11sx9aq6s1pz6sscbcnw0gh1fa46k9sxc3dnhqrrrxaz4mv3z") (r "1.63")))

(define-public crate-leo-span-1.6.1 (c (n "leo-span") (v "1.6.1") (d (list (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "indexmap") (r "^1.9") (f (quote ("serde"))) (d #t) (k 0)) (d (n "scoped-tls") (r "^1.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.148") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "1prds1vbxag0bl1j44r08bqmhwb4fhmpdyqldr6ykfxifvjdv0v1") (r "1.65")))

(define-public crate-leo-span-1.6.2 (c (n "leo-span") (v "1.6.2") (d (list (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "indexmap") (r "^1.9") (f (quote ("serde"))) (d #t) (k 0)) (d (n "scoped-tls") (r "^1.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.148") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "0p4bv11ap62g12xfmx70rk5smrh2s4an2lyi6a03n1sxy8g68lcx") (r "1.65")))

(define-public crate-leo-span-1.6.3 (c (n "leo-span") (v "1.6.3") (d (list (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "indexmap") (r "^1.9") (f (quote ("serde"))) (d #t) (k 0)) (d (n "scoped-tls") (r "^1.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "0821yzmvm40gag83nrhabj35wf4xci3damk6crglmqdnbz9myj3g") (r "1.65")))

(define-public crate-leo-span-1.7.0 (c (n "leo-span") (v "1.7.0") (d (list (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "indexmap") (r "^1.9") (f (quote ("serde"))) (d #t) (k 0)) (d (n "scoped-tls") (r "^1.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "1c3n1grpzl7yvp6dr99xvi7isy6vdr7jdp8yy5iq3rgfl21w7fwc") (r "1.65")))

(define-public crate-leo-span-1.7.1 (c (n "leo-span") (v "1.7.1") (d (list (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "indexmap") (r "^1.9") (f (quote ("serde"))) (d #t) (k 0)) (d (n "scoped-tls") (r "^1.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "1l83vm7lg3zd39b7dqhl9knddxav48kg7i9w8kwkkyzczbrlrzff") (r "1.69")))

(define-public crate-leo-span-1.7.2 (c (n "leo-span") (v "1.7.2") (d (list (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "indexmap") (r "^1.9") (f (quote ("serde"))) (d #t) (k 0)) (d (n "scoped-tls") (r "^1.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "1m7diwwb9xmlvr6h92r2wbw6y4vks0iprqdyjym26dk4msg1b71f") (r "1.69")))

(define-public crate-leo-span-1.7.3 (c (n "leo-span") (v "1.7.3") (d (list (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "indexmap") (r "^1.9") (f (quote ("serde"))) (d #t) (k 0)) (d (n "scoped-tls") (r "^1.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "11k145kfgyn6bkmc2icf9vsjhpim73kjblbq1d419bkja3bwgczn") (r "1.69")))

(define-public crate-leo-span-1.8.0 (c (n "leo-span") (v "1.8.0") (d (list (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "indexmap") (r "^1.9") (f (quote ("serde"))) (d #t) (k 0)) (d (n "scoped-tls") (r "^1.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "1il650sb2xmp9p2lb3hzb18xq87ph8sk207mmhpyy5wqccccpasx") (r "1.69")))

(define-public crate-leo-span-1.8.1 (c (n "leo-span") (v "1.8.1") (d (list (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "indexmap") (r "^1.9") (f (quote ("serde"))) (d #t) (k 0)) (d (n "scoped-tls") (r "^1.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "0xw6s2yj7v4mhlcnk73wg2cqx64l7rwfsp0255wkaskg8fmddnsi") (r "1.69")))

(define-public crate-leo-span-1.8.2 (c (n "leo-span") (v "1.8.2") (d (list (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "indexmap") (r "^1.9") (f (quote ("serde"))) (d #t) (k 0)) (d (n "scoped-tls") (r "^1.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "14m8hh66v1rxmcfbgkbm93jwxic4p7009cmqllfn1vjc6dlqn8z1") (r "1.69")))

(define-public crate-leo-span-1.8.3 (c (n "leo-span") (v "1.8.3") (d (list (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "indexmap") (r "^1.9") (f (quote ("serde"))) (d #t) (k 0)) (d (n "scoped-tls") (r "^1.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.166") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "1ahjgk4ndwigmxjj0wh5887aiq7rm1k8g5gsplmpmfp9n21hlj01") (r "1.69")))

(define-public crate-leo-span-1.9.0 (c (n "leo-span") (v "1.9.0") (d (list (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "indexmap") (r "^1.9") (f (quote ("serde"))) (d #t) (k 0)) (d (n "scoped-tls") (r "^1.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.171") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "0d6vp8714vwzkzm722cw31czzh9f13ks2j0vr8rhrpn2qh98984w") (r "1.69")))

(define-public crate-leo-span-1.9.1 (c (n "leo-span") (v "1.9.1") (d (list (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "indexmap") (r "^1.9") (f (quote ("serde"))) (d #t) (k 0)) (d (n "scoped-tls") (r "^1.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.174") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "1bb0r8r0j7qib41jislsrwc0q7v568j64pgg9nglyvmi7q7xk8bz") (r "1.69")))

(define-public crate-leo-span-1.9.2 (c (n "leo-span") (v "1.9.2") (d (list (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "indexmap") (r "^1.9") (f (quote ("serde"))) (d #t) (k 0)) (d (n "scoped-tls") (r "^1.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.183") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "155293ybyhm39m1ssf07d46if3632zgh0s8jf6mgdp0ghyb0basf") (r "1.69")))

(define-public crate-leo-span-1.9.3 (c (n "leo-span") (v "1.9.3") (d (list (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "indexmap") (r "^1.9") (f (quote ("serde"))) (d #t) (k 0)) (d (n "scoped-tls") (r "^1.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.183") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "1zaj3ax10vmzc5f2n67xwhcn37iaxmm5hxmypc6fq25i8rpi5hx8") (r "1.69")))

(define-public crate-leo-span-1.9.4 (c (n "leo-span") (v "1.9.4") (d (list (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "indexmap") (r "^1.9") (f (quote ("serde"))) (d #t) (k 0)) (d (n "scoped-tls") (r "^1.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.183") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "1r29avb2w0xmia9aq21r236q27qz0x56xdyr288zd61cj3nj35b2") (y #t) (r "1.69")))

(define-public crate-leo-span-1.10.0 (c (n "leo-span") (v "1.10.0") (d (list (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "indexmap") (r "^1.9") (f (quote ("serde"))) (d #t) (k 0)) (d (n "scoped-tls") (r "^1.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.189") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "0wp2d0j5y4cc0pfbf2kz8hn7lrfllpz3n72snq3sxn59p96gsz03") (r "1.69")))

(define-public crate-leo-span-1.11.0 (c (n "leo-span") (v "1.11.0") (d (list (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "indexmap") (r "^1.9") (f (quote ("serde"))) (d #t) (k 0)) (d (n "scoped-tls") (r "^1.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "02bpac5r0la92770nwx9limsv6iwg2jdzhzz8xs2xfl7a4v9f21y") (r "1.69")))

(define-public crate-leo-span-1.12.0 (c (n "leo-span") (v "1.12.0") (d (list (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "indexmap") (r "^1.9") (f (quote ("serde"))) (d #t) (k 0)) (d (n "scoped-tls") (r "^1.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.201") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "07l1f5pz1dzmw491yd25cvh5b13dcip0kmkwyrv5822jx8dridj2") (r "1.69")))

