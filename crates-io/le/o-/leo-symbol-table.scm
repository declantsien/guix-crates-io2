(define-module (crates-io le o- leo-symbol-table) #:use-module (crates-io))

(define-public crate-leo-symbol-table-1.0.5 (c (n "leo-symbol-table") (v "1.0.5") (d (list (d (n "leo-ast") (r "^1.0.5") (d #t) (k 0)) (d (n "leo-core") (r "^1.0.5") (d #t) (k 0)) (d (n "leo-grammar") (r "^1.0.5") (d #t) (k 0)) (d (n "leo-imports") (r "^1.0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "035j54llvkm58ifs8kh14ad08shsva0s421j2h34f520vqbjlm9w") (y #t)))

(define-public crate-leo-symbol-table-1.0.6 (c (n "leo-symbol-table") (v "1.0.6") (d (list (d (n "leo-ast") (r "^1.0.6") (d #t) (k 0)) (d (n "leo-core") (r "^1.0.6") (d #t) (k 0)) (d (n "leo-grammar") (r "^1.0.6") (d #t) (k 0)) (d (n "leo-imports") (r "^1.0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "03hq9psbzym7ij83j3kwzv1pza7jwbgw16kcbzz7jbbv60qbp17z") (y #t)))

(define-public crate-leo-symbol-table-1.0.7 (c (n "leo-symbol-table") (v "1.0.7") (d (list (d (n "indexmap") (r "^1.6.0") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "leo-ast") (r "^1.0.7") (d #t) (k 0)) (d (n "leo-core") (r "^1.0.7") (d #t) (k 0)) (d (n "leo-grammar") (r "^1.0.7") (d #t) (k 0)) (d (n "leo-imports") (r "^1.0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "08livim29nnly4fm8kw79lik73iz70bhmpvl0c08wjry95qfjvvw") (y #t)))

(define-public crate-leo-symbol-table-1.0.8 (c (n "leo-symbol-table") (v "1.0.8") (d (list (d (n "indexmap") (r "^1.6.1") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "leo-ast") (r "^1.0.8") (d #t) (k 0)) (d (n "leo-core") (r "^1.0.8") (d #t) (k 0)) (d (n "leo-grammar") (r "^1.0.8") (d #t) (k 0)) (d (n "leo-imports") (r "^1.0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "19qckvx6ywsplsaqyxwmpkc06cnnxcz19fizwbnlfmcprprhk6n0") (y #t)))

