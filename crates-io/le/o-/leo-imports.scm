(define-module (crates-io le o- leo-imports) #:use-module (crates-io))

(define-public crate-leo-imports-1.0.5 (c (n "leo-imports") (v "1.0.5") (d (list (d (n "leo-ast") (r "^1.0.5") (d #t) (k 0)) (d (n "leo-grammar") (r "^1.0.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "05c496g7zkb8q99p75pyxqyggmjcg307mwj6qlyl7hz0nrdkzvjs")))

(define-public crate-leo-imports-1.0.6 (c (n "leo-imports") (v "1.0.6") (d (list (d (n "leo-ast") (r "^1.0.6") (d #t) (k 0)) (d (n "leo-grammar") (r "^1.0.6") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1ha2x3ajwcxx8iz6mx0zzs2sjrc8hfkiq3vpdxff30kpqr41l28h")))

(define-public crate-leo-imports-1.0.7 (c (n "leo-imports") (v "1.0.7") (d (list (d (n "indexmap") (r "^1.6.0") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "leo-ast") (r "^1.0.7") (d #t) (k 0)) (d (n "leo-grammar") (r "^1.0.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1j0djvbv2ixim17llsmwcf25l55gvwjbpd7girxg8lcj0b3faqbc")))

(define-public crate-leo-imports-1.0.8 (c (n "leo-imports") (v "1.0.8") (d (list (d (n "indexmap") (r "^1.6.1") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "leo-ast") (r "^1.0.8") (d #t) (k 0)) (d (n "leo-grammar") (r "^1.0.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0kpfin8lsfyga99n4js2qwx9wn7y2i4lfg5gb86qmm78jq18a8r8")))

(define-public crate-leo-imports-1.1.8 (c (n "leo-imports") (v "1.1.8") (d (list (d (n "indexmap") (r "^1.6.1") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "leo-asg") (r "^1.1.7") (d #t) (k 0)) (d (n "leo-ast") (r "^1.1.8") (d #t) (k 0)) (d (n "leo-grammar") (r "^1.1.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "01fm3c6xmcigjqxanbjj1qrbxk4c5z14wdi0hd872p1c6lvz4x6j")))

(define-public crate-leo-imports-1.2.0 (c (n "leo-imports") (v "1.2.0") (d (list (d (n "indexmap") (r "^1.6.1") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "leo-asg") (r "^1.2.0") (d #t) (k 0)) (d (n "leo-ast") (r "^1.2.0") (d #t) (k 0)) (d (n "leo-grammar") (r "^1.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1jgs8grpspzx7wsvina4r4h8sjy626h3y1d6cvcsp11hhsfcv0a1")))

(define-public crate-leo-imports-1.2.2 (c (n "leo-imports") (v "1.2.2") (d (list (d (n "indexmap") (r "^1.6.1") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "leo-asg") (r "^1.2.2") (d #t) (k 0)) (d (n "leo-ast") (r "^1.2.2") (d #t) (k 0)) (d (n "leo-grammar") (r "^1.2.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0gcsviyn9kvnspfg0ml9kw9myay2mjpjhfva1ijzzzhbib56xaia")))

(define-public crate-leo-imports-1.2.3 (c (n "leo-imports") (v "1.2.3") (d (list (d (n "indexmap") (r "^1.6.1") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "leo-asg") (r "^1.2.3") (d #t) (k 0)) (d (n "leo-ast") (r "^1.2.3") (d #t) (k 0)) (d (n "leo-grammar") (r "^1.2.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0x767aa63spwjxkm6zml1pnymxddqj81hjpv8k49xhdi418y0n0f")))

(define-public crate-leo-imports-1.3.0 (c (n "leo-imports") (v "1.3.0") (d (list (d (n "indexmap") (r "^1.6.2") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "leo-asg") (r "^1.3.0") (d #t) (k 0)) (d (n "leo-ast") (r "^1.3.0") (d #t) (k 0)) (d (n "leo-parser") (r "^1.3.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "027j8ni1ca9q70axj6xr47i4a5817jn2sc1df64w7w6vj2dd7hsb")))

(define-public crate-leo-imports-1.4.0 (c (n "leo-imports") (v "1.4.0") (d (list (d (n "indexmap") (r "^1.6.2") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "leo-asg") (r "^1.4.0") (d #t) (k 0)) (d (n "leo-ast") (r "^1.4.0") (d #t) (k 0)) (d (n "leo-parser") (r "^1.4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1ydm28v9id049vh9gy7yjj9f4600xx5rnnp14zmh3gx7fjhc5iq4")))

(define-public crate-leo-imports-1.5.0 (c (n "leo-imports") (v "1.5.0") (d (list (d (n "indexmap") (r "^1.6.2") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "leo-asg") (r "^1.5.0") (d #t) (k 0)) (d (n "leo-ast") (r "^1.5.0") (d #t) (k 0)) (d (n "leo-parser") (r "^1.5.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1c7vngqphxxnr2s7m4zmvy02rv8z2nyrfdzywfp95hi3860xssic")))

(define-public crate-leo-imports-1.5.1 (c (n "leo-imports") (v "1.5.1") (d (list (d (n "indexmap") (r "^1.6.2") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "leo-asg") (r "^1.5.1") (d #t) (k 0)) (d (n "leo-ast") (r "^1.5.1") (d #t) (k 0)) (d (n "leo-parser") (r "^1.5.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0mlza409skp76azq9sqfnd8z2kbw3vcmrrjfzlkx1rk95rpca46z")))

(define-public crate-leo-imports-1.5.2 (c (n "leo-imports") (v "1.5.2") (d (list (d (n "indexmap") (r "^1.6.2") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "leo-asg") (r "^1.5.2") (d #t) (k 0)) (d (n "leo-ast") (r "^1.5.2") (d #t) (k 0)) (d (n "leo-parser") (r "^1.5.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1slhb4a4l1w7ka0xlywwqsvyzyjv4443ckn0zx77jvm6q1h7nbwi")))

(define-public crate-leo-imports-1.5.3 (c (n "leo-imports") (v "1.5.3") (d (list (d (n "indexmap") (r "^1.7.0") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "leo-asg") (r "^1.5.3") (d #t) (k 0)) (d (n "leo-ast") (r "^1.5.3") (d #t) (k 0)) (d (n "leo-parser") (r "^1.5.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "09r2h8r4zvwc5djv4pngzllxbg5wrd6azzg599w2hqwy393p451i")))

