(define-module (crates-io le nt lentrait) #:use-module (crates-io))

(define-public crate-lentrait-0.1.0 (c (n "lentrait") (v "0.1.0") (d (list (d (n "len-trait") (r "^0.6") (d #t) (k 0)) (d (n "mako") (r "^0.1.0") (d #t) (k 0)) (d (n "pyo3") (r "^0.13.2") (f (quote ("extension-module" "abi3-py36"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "0wmjz227g8pb3s6kn4ib4nvp2pva75yg6fbrkgf3cdaqqyaw4kk6")))

(define-public crate-lentrait-0.2.0 (c (n "lentrait") (v "0.2.0") (d (list (d (n "len-trait") (r "^0.6") (d #t) (k 0)) (d (n "mako") (r "^0.1.0") (d #t) (k 0)) (d (n "pyo3") (r "^0.13.2") (f (quote ("extension-module" "abi3-py36"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "1k8d3qx9jsy1qxc52igqywsdf5ycqp2l78242h1kdavls5bjvkid")))

(define-public crate-lentrait-0.2.1 (c (n "lentrait") (v "0.2.1") (d (list (d (n "len-trait") (r "^0.6") (d #t) (k 0)) (d (n "mako") (r "^0.2.0") (d #t) (k 0)) (d (n "pyo3") (r "^0.13.2") (f (quote ("extension-module" "abi3-py36"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "1qsjb5l1sa1ndpwki5zj9cx5p7lwzzf01kkv9y9iz1mf4ib9fc8n")))

(define-public crate-lentrait-0.2.2 (c (n "lentrait") (v "0.2.2") (d (list (d (n "pyo3") (r "^0.13.2") (f (quote ("extension-module" "abi3-py36"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "0mvvg0jrabbh8hjgw25z610vl019z719gl1g4gdvcfa68wmwc28z")))

(define-public crate-lentrait-0.2.3 (c (n "lentrait") (v "0.2.3") (d (list (d (n "pyo3") (r "^0.13.2") (f (quote ("extension-module" "abi3-py36"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (o #t) (d #t) (k 0)))) (h "000bcqiw7dzpvza5y4h5cy1whiqpl01xi25xcy506vn0m17ixb6g") (f (quote (("serde_crates" "serde_json" "serde") ("default"))))))

