(define-module (crates-io le dm ledmatrix_widgets) #:use-module (crates-io))

(define-public crate-ledmatrix_widgets-0.1.0 (c (n "ledmatrix_widgets") (v "0.1.0") (d (list (d (n "battery") (r "^0.7.8") (d #t) (k 0)) (d (n "serialport") (r "^4.3.0") (d #t) (k 0)) (d (n "sysinfo") (r "^0.30.9") (d #t) (k 0)))) (h "08balllqzwf6pmla21165c08lf2r6pfgsspx586m5pn63j1w0bs7")))

