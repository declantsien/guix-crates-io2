(define-module (crates-io le xe lexer-generator) #:use-module (crates-io))

(define-public crate-lexer-generator-0.1.0 (c (n "lexer-generator") (v "0.1.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("alloc"))) (k 0)))) (h "0p9xj4pcapvrwg553afqsmwxfrhyq1vdnh7bfaivz3g598wzq16v")))

(define-public crate-lexer-generator-0.1.1 (c (n "lexer-generator") (v "0.1.1") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("alloc"))) (k 0)))) (h "0pyspz24wah1b8kslw2qsyhb28z5jq6yzmpwlvdl209zjfqj2r94")))

(define-public crate-lexer-generator-0.1.2 (c (n "lexer-generator") (v "0.1.2") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("alloc"))) (k 0)))) (h "1f40ivvx45ngs46a7vp1sn7jd0i2n1kazi7x8vjzxg2h682kpiyn")))

(define-public crate-lexer-generator-0.1.3 (c (n "lexer-generator") (v "0.1.3") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("alloc"))) (k 0)))) (h "063fylydqr380gvpnr1z1cxpfz06xxrhnfjxwqpprl3h6sayr5p9")))

(define-public crate-lexer-generator-0.1.4 (c (n "lexer-generator") (v "0.1.4") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("alloc"))) (k 0)))) (h "0rmgcvqf8cw4af1shpii4i6khi2h74zahymfh65163ba8n3ksdp8")))

