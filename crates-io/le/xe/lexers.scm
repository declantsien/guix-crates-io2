(define-module (crates-io le xe lexers) #:use-module (crates-io))

(define-public crate-lexers-0.0.3 (c (n "lexers") (v "0.0.3") (h "0gmivkdaslgdkd46c5ic740la8bwq25bl128003vc0yzfqjlzb2h")))

(define-public crate-lexers-0.0.4 (c (n "lexers") (v "0.0.4") (h "0jca8arncyi5f3yll62iv0md80mj09zx6hp8nxaghpsna7fn89dv")))

(define-public crate-lexers-0.0.5 (c (n "lexers") (v "0.0.5") (h "1djrlp2rp2lr64cbrc815nfv1c3aqwb43irm28fr164mivcxsq9l")))

(define-public crate-lexers-0.0.6 (c (n "lexers") (v "0.0.6") (h "0wvdl39z1gp6if3azbazgg3qgh7pw9ygjn1dbig0a8n7xwnhrq1r")))

(define-public crate-lexers-0.0.7 (c (n "lexers") (v "0.0.7") (h "051b1k2cgam42a1j38pfpx2zwq10d37q8f44f2485pcq827bi22g")))

(define-public crate-lexers-0.0.8 (c (n "lexers") (v "0.0.8") (h "1bdwfp01nif53dn6hn30g1ka0g1r5y48iggpmrpr06vnary5f3y4")))

(define-public crate-lexers-0.1.0 (c (n "lexers") (v "0.1.0") (h "0mnz6whmgnv992zcgw4b994n1dz1y8z0c2vd1kph6d1fp3v427mg") (y #t)))

(define-public crate-lexers-0.1.1 (c (n "lexers") (v "0.1.1") (h "1axdn3sf7g3zk776ka5pgw56qly6x3zgbxsk24njfi3h5mc4v7ld") (y #t)))

(define-public crate-lexers-0.1.2 (c (n "lexers") (v "0.1.2") (h "0szgfyizffjabhd1il3xyv4khb30hjc8gvb331myyxzli63i51rv")))

(define-public crate-lexers-0.1.3 (c (n "lexers") (v "0.1.3") (h "1syxbxxzsnf233b2925h0k9y4nbsfc7dq05k7905pwnrzkavfgcs")))

(define-public crate-lexers-0.1.4 (c (n "lexers") (v "0.1.4") (h "1yla78rqbwnim9fpbvzglsw21qkjm4z5hi9aac9mxzz8v6srppl2")))

