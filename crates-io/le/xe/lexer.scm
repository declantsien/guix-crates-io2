(define-module (crates-io le xe lexer) #:use-module (crates-io))

(define-public crate-lexer-0.1.0 (c (n "lexer") (v "0.1.0") (h "132qb5yid78ixw7gqfvla9nlp7gflwkbq2gjfcwmk5syivac6bxn")))

(define-public crate-lexer-0.1.1 (c (n "lexer") (v "0.1.1") (h "1disg87jiyglm2jmd09sap87qxwlvz4922i1a77sz3nij5shlb2x")))

(define-public crate-lexer-0.1.2 (c (n "lexer") (v "0.1.2") (d (list (d (n "serde") (r "^1.0") (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0k9szgrsdaf0d0xpccn19lxmmpxl5rhbxybchanfymla3d802389") (f (quote (("use_std") ("default" "use_std"))))))

(define-public crate-lexer-0.1.3 (c (n "lexer") (v "0.1.3") (d (list (d (n "serde") (r "^1.0") (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1lpdicawggqw55lqracv8dsrry3xzhp4v3nz1dqirhksyy3f4sa5") (f (quote (("use_std") ("default" "use_std"))))))

(define-public crate-lexer-0.1.4 (c (n "lexer") (v "0.1.4") (d (list (d (n "serde") (r "^1.0") (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1kg1h04n7xc122589wpy7inmflvsrzpy33fkbcibj1dwll4llrap") (f (quote (("use_std") ("default" "use_std"))))))

(define-public crate-lexer-0.1.5 (c (n "lexer") (v "0.1.5") (d (list (d (n "serde") (r "^1.0") (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1bc03kycqk14pckrlhnk669sqj753b2wfh8zk95vaqf8l8yg7phz") (f (quote (("use_std") ("default" "use_std"))))))

(define-public crate-lexer-0.1.6 (c (n "lexer") (v "0.1.6") (d (list (d (n "chars_input") (r "^0.2") (k 0)) (d (n "serde") (r "^1.0") (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1z9r316ynymgpmyplghjzqdpmc6hpxbxjv43z58hjbd5712fwis0") (f (quote (("std" "chars_input/std") ("default" "std"))))))

(define-public crate-lexer-0.1.7 (c (n "lexer") (v "0.1.7") (d (list (d (n "chars_input") (r "^0.2") (k 0)) (d (n "serde") (r "^1.0") (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0bpfipkdabn6whp63n6f012xgac8dwjghxzfn13dxpgagxzr6jdd") (f (quote (("std" "chars_input/std") ("default" "std"))))))

(define-public crate-lexer-0.1.8 (c (n "lexer") (v "0.1.8") (d (list (d (n "chars_input") (r "^0.2") (k 0)) (d (n "serde") (r "^1.0") (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0n5cpkgd76v27n9c4pr80sxvf7ws7aa3sha8aw92fy5bkrhnsijj") (f (quote (("std" "chars_input/std") ("default" "std"))))))

(define-public crate-lexer-0.1.9 (c (n "lexer") (v "0.1.9") (d (list (d (n "chars_input") (r "^0.2") (k 0)) (d (n "serde") (r "^1.0") (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1i31k3z20ygi3bcs3l39hmh44b3k8vq14hr223vl6kaaaqinz03q") (f (quote (("std" "chars_input/std") ("default" "std"))))))

(define-public crate-lexer-0.1.10 (c (n "lexer") (v "0.1.10") (d (list (d (n "peek-nth") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "00mcal434xvzfyh0a6f8m7hvia5irk72a1k8xikh4hlcfwy0099s")))

(define-public crate-lexer-0.1.11 (c (n "lexer") (v "0.1.11") (d (list (d (n "peek-nth") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1s2dllw5cyp2hn6xw5andhwx1hj40i130dmmyy1f25dimbvnr2bz")))

(define-public crate-lexer-0.1.12 (c (n "lexer") (v "0.1.12") (d (list (d (n "peek-nth") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0g9y9pw1fyfad32g3q6xlgvfr7kcvfs9d3mpbm60w0hrib8q2za2")))

(define-public crate-lexer-0.1.13 (c (n "lexer") (v "0.1.13") (d (list (d (n "peek-nth") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1b2pgid7n21y7vk6df43ap3ijbp8jpcnnm453mhiqb02hkawlnn8")))

(define-public crate-lexer-0.1.15 (c (n "lexer") (v "0.1.15") (d (list (d (n "peek-nth") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "159abp6anb4bp561sn3s362n1qw1zi92y0sfadldb3p6bbprhlxi")))

(define-public crate-lexer-0.1.16 (c (n "lexer") (v "0.1.16") (d (list (d (n "peek-nth") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1aa44zv7wnaw027pzky6v3ir68gzpg5n1r373gzak61ziymvc0dq")))

(define-public crate-lexer-0.1.18 (c (n "lexer") (v "0.1.18") (d (list (d (n "peek-nth") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1yvphbqi3n0ffraij889fljc2c39myd1a3v01rkvnsmrj51x90jp")))

