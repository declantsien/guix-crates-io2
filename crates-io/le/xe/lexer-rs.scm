(define-module (crates-io le xe lexer-rs) #:use-module (crates-io))

(define-public crate-lexer-rs-0.0.2 (c (n "lexer-rs") (v "0.0.2") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)))) (h "0mrv24fp10mckxl5qwy77a3sx7pjl2mc23ipmscb8mvvbljlkv77")))

(define-public crate-lexer-rs-0.0.3 (c (n "lexer-rs") (v "0.0.3") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)))) (h "0njw622xxr6wsjq80rja2jhbk5r5mpk4csp91hg3grcrpyfx18wp")))

(define-public crate-lexer-rs-0.0.4 (c (n "lexer-rs") (v "0.0.4") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)))) (h "1cs8pxx0wy1dxnal7cp0rmvm83gi04c2bqhd7xckcrmvyn15lc2a")))

