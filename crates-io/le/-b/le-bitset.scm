(define-module (crates-io le -b le-bitset) #:use-module (crates-io))

(define-public crate-le-bitset-0.1.0 (c (n "le-bitset") (v "0.1.0") (h "0v7xmq94qhnciag3icrw8f68y1lcpkrwd4nqk7mxjcb1hsw7z2hc")))

(define-public crate-le-bitset-0.1.1 (c (n "le-bitset") (v "0.1.1") (h "1kn7lrr7l01l2cm6z8dr361lxvjhlipm4sz626rmy0ql0h23xwcj")))

(define-public crate-le-bitset-0.1.2 (c (n "le-bitset") (v "0.1.2") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "044acisqrbgmjn0ra3f1xqvxc967qfd949nssjcr3fcgjc4sxsh5") (s 2) (e (quote (("serde" "dep:serde" "serde_derive"))))))

(define-public crate-le-bitset-0.1.3 (c (n "le-bitset") (v "0.1.3") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0fw17k2xpzkz2wfdhf365vlf4j7jp79h5l9ggn63w6rca99yb86b") (s 2) (e (quote (("serde" "dep:serde" "serde_derive"))))))

