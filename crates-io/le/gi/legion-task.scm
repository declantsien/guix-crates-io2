(define-module (crates-io le gi legion-task) #:use-module (crates-io))

(define-public crate-legion-task-0.1.0 (c (n "legion-task") (v "0.1.0") (d (list (d (n "itertools") (r "^0.8.2") (d #t) (k 0)) (d (n "legion") (r "^0.2.4") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "simple_logger") (r "^1.6.0") (d #t) (k 2)))) (h "04lx9dy3mrpv8pm8mmz9ndbr83yd37ck9dq8hs0qra17hmxi4a03")))

