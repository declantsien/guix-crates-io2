(define-module (crates-io le gi legion_codegen) #:use-module (crates-io))

(define-public crate-legion_codegen-0.3.0 (c (n "legion_codegen") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0zrhrn6fnh1f2acz4zms1y9nlrc023s18gzqhxmswvk51q35hkn2")))

(define-public crate-legion_codegen-0.4.0 (c (n "legion_codegen") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "06xfhwaqdmd5qwng5ghnd0ycnd9ilxgxjd8304i2ayqx6rxavmca")))

