(define-module (crates-io le gi legit) #:use-module (crates-io))

(define-public crate-legit-0.2.2 (c (n "legit") (v "0.2.2") (d (list (d (n "argparse") (r "^0.2.2") (d #t) (k 0)) (d (n "chrono") (r "^0") (d #t) (k 0)) (d (n "git2") (r "^0.9.1") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)) (d (n "time") (r "^0.1.42") (d #t) (k 0)))) (h "08aq2jxid905k9h5bmp27h1lk9ddjzz6bs931i0xczaj0bhiv4v2") (r "1.61.0")))

(define-public crate-legit-0.2.1 (c (n "legit") (v "0.2.1") (d (list (d (n "argparse") (r "^0.2.2") (d #t) (k 0)) (d (n "chrono") (r "^0") (d #t) (k 0)) (d (n "git2") (r "^0.9.1") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)) (d (n "time") (r "^0.1.42") (d #t) (k 0)))) (h "1cklhiky8bxii65r6qla094cjqgwax2qgja94fi0k5na6qs8f428") (r "1.61.0")))

(define-public crate-legit-0.2.4 (c (n "legit") (v "0.2.4") (d (list (d (n "argparse") (r "^0.2.2") (d #t) (k 0)) (d (n "chrono") (r "^0") (d #t) (k 0)) (d (n "git2") (r "^0.9.1") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)) (d (n "time") (r "^0.1.42") (d #t) (k 0)))) (h "02hp74a7c0sdgb66lzv2s76yzq3mncy498bbl7x3sgc30p8lgfss") (r "1.61.0")))

