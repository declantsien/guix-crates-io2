(define-module (crates-io le mu lemurs-8080) #:use-module (crates-io))

(define-public crate-lemurs-8080-0.3.0 (c (n "lemurs-8080") (v "0.3.0") (d (list (d (n "disclose") (r "^0") (d #t) (k 0)))) (h "0797j2ic31pwymlb9sjyxwnr4f8c5xgc07yhkv339flr5z266pdm") (f (quote (("std") ("open") ("default" "std"))))))

(define-public crate-lemurs-8080-0.4.0 (c (n "lemurs-8080") (v "0.4.0") (d (list (d (n "cc") (r ">=1.0.0") (d #t) (k 1)) (d (n "cruppers") (r ">=0.4") (o #t) (k 0)) (d (n "disclose") (r "^0") (d #t) (k 0)))) (h "1990la05rp2ifi79zrpvj6wn4zkb3r51nqa70q4642ai6z6dab7h") (f (quote (("std") ("open") ("default" "std") ("cpp_panic" "cruppers/exception" "_cpp") ("cpp_alloc" "cruppers/memory" "_cpp") ("cpp" "cpp_panic" "cpp_alloc") ("_cpp"))))))

(define-public crate-lemurs-8080-0.4.6 (c (n "lemurs-8080") (v "0.4.6") (d (list (d (n "cc") (r ">=1.0.0") (d #t) (k 1)) (d (n "cruppers") (r ">=0.4") (o #t) (k 0)) (d (n "disclose") (r "^0") (d #t) (k 0)))) (h "0gdw2a20441pfmygn6qv21j7l5k3a3bp16i5ac5wnr3a96ijbcb5") (f (quote (("std") ("open") ("default" "std") ("cpp_panic" "cruppers/exception" "_cpp") ("cpp_alloc" "cruppers/memory" "_cpp") ("cpp" "cpp_panic" "cpp_alloc") ("_cpp"))))))

(define-public crate-lemurs-8080-0.4.7 (c (n "lemurs-8080") (v "0.4.7") (d (list (d (n "cc") (r ">=1.0.0") (d #t) (k 1)) (d (n "cruppers") (r ">=0.4") (o #t) (k 0)) (d (n "disclose") (r "^0") (d #t) (k 0)))) (h "1fp3531z9q4hdk471i3vzzqgyq1zxghrj7irzrww3dgp5lpj37vb") (f (quote (("std") ("open") ("default" "std") ("cpp_panic" "cruppers/exception" "_cpp") ("cpp_alloc" "cruppers/memory" "_cpp") ("cpp" "cpp_panic" "cpp_alloc") ("_cpp"))))))

(define-public crate-lemurs-8080-0.4.8 (c (n "lemurs-8080") (v "0.4.8") (d (list (d (n "cc") (r ">=1.0.0") (d #t) (k 1)) (d (n "cruppers") (r ">=0.4") (o #t) (k 0)) (d (n "disclose") (r "^0") (d #t) (k 0)))) (h "1pxlimgbg15lipbxvlz8sw4qp6cm24pfghc2h3lijsp71m6livjz") (f (quote (("std") ("open") ("default" "std") ("cpp_panic" "cruppers/exception" "_cpp") ("cpp_alloc" "cruppers/memory" "_cpp") ("cpp" "cpp_panic" "cpp_alloc") ("_cpp"))))))

(define-public crate-lemurs-8080-0.5.0 (c (n "lemurs-8080") (v "0.5.0") (d (list (d (n "cc") (r ">=1.0.0") (d #t) (k 1)) (d (n "cruppers") (r ">=0.4") (o #t) (k 0)) (d (n "disclose") (r "^0") (d #t) (k 0)))) (h "0sj25kfhr34yj0bhvwzvyn5m4q8mb0pyaj0ncsbfq9cf3wkf8vaq") (f (quote (("std") ("open") ("default" "std") ("cpp_panic" "cruppers/exception" "_cpp") ("cpp_alloc" "cruppers/memory" "_cpp") ("cpp" "cpp_panic" "cpp_alloc") ("_cpp"))))))

(define-public crate-lemurs-8080-0.5.1 (c (n "lemurs-8080") (v "0.5.1") (d (list (d (n "cc") (r ">=1.0.0") (d #t) (k 1)) (d (n "cruppers") (r ">=0.4") (o #t) (k 0)) (d (n "disclose") (r "^0") (d #t) (k 0)))) (h "16m8mq6f36niar4a0aw50ksfhnxxr06hpfs38dspvw0jhmgag0pl") (f (quote (("std") ("open") ("default" "std") ("cpp_panic" "cruppers/exception" "_cpp") ("cpp_alloc" "cruppers/memory" "_cpp") ("cpp" "cpp_panic" "cpp_alloc") ("_cpp"))))))

(define-public crate-lemurs-8080-0.5.2 (c (n "lemurs-8080") (v "0.5.2") (d (list (d (n "cc") (r ">=1.0.0") (d #t) (k 1)) (d (n "cruppers") (r ">=0.4") (o #t) (k 0)) (d (n "disclose") (r "^0") (d #t) (k 0)))) (h "1646jfg78ldk2a0pgbd78xm8g7408nljz42pln0g35a742shyqbh") (f (quote (("std") ("open") ("default" "std") ("cpp_panic" "cruppers/exception" "_cpp") ("cpp_alloc" "cruppers/memory" "_cpp") ("cpp" "cpp_panic" "cpp_alloc") ("_cpp"))))))

