(define-module (crates-io le dg ledger-zondax-generic) #:use-module (crates-io))

(define-public crate-ledger-zondax-generic-0.4.0 (c (n "ledger-zondax-generic") (v "0.4.0") (d (list (d (n "ledger-apdu") (r "^0.4.0") (d #t) (k 0)) (d (n "ledger-transport") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "13j7xks4d11v7xhidfdjnsi5vz93rcrc3prq54jdwh0635j2pcda")))

(define-public crate-ledger-zondax-generic-0.7.0 (c (n "ledger-zondax-generic") (v "0.7.0") (d (list (d (n "ledger-apdu") (r "^0.7.0") (d #t) (k 0)) (d (n "ledger-transport") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "008ninwv1vfh5khzvlv7h6zw7phmzaw2gwml1d3lp2dagny89yp1")))

(define-public crate-ledger-zondax-generic-0.8.0 (c (n "ledger-zondax-generic") (v "0.8.0") (d (list (d (n "ledger-apdu") (r "^0.8.0") (d #t) (k 0)) (d (n "ledger-transport") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "15kzcd8rzi49v9bjxzpm09l7ym09pwcbl6l8p66vb9yrrcl0a2li")))

(define-public crate-ledger-zondax-generic-0.9.0 (c (n "ledger-zondax-generic") (v "0.9.0") (d (list (d (n "async-trait") (r "^0.1.53") (d #t) (k 0)) (d (n "ledger-transport") (r "^0.9.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "01k1wvxjq7a118q9lzdiry4frvp6lhj6n7pdzvs33dr5wdd7nlya")))

(define-public crate-ledger-zondax-generic-0.9.1 (c (n "ledger-zondax-generic") (v "0.9.1") (d (list (d (n "async-trait") (r "^0.1.53") (d #t) (k 0)) (d (n "ledger-transport") (r "^0.9.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0c38ca9kl0a821sgsdabfhf4aysl4qw9ldv4zlv675m7fgaa8alh")))

(define-public crate-ledger-zondax-generic-0.10.0 (c (n "ledger-zondax-generic") (v "0.10.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "ledger-transport") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "11wj3w9qrfznqb1c71f7zcf5wkxs4692d3anpj2qxi5rxa26q0q2")))

(define-public crate-ledger-zondax-generic-0.11.0 (c (n "ledger-zondax-generic") (v "0.11.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "ledger-transport") (r "^0.11.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0pn6075fzmdwirdw9i9mkd4ynzdrlk23y0jy6zy547xsz6cxkxlb")))

