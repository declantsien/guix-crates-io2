(define-module (crates-io le dg ledger-cli) #:use-module (crates-io))

(define-public crate-ledger-cli-0.1.0 (c (n "ledger-cli") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "clap") (r "^4.2.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "humantime") (r "^2.1.0") (d #t) (k 0)) (d (n "ledger-lib") (r "^0.1.0") (f (quote ("clap"))) (d #t) (k 0)) (d (n "ledger-proto") (r "^0.1.0") (d #t) (k 0)) (d (n "time") (r "^0.3.21") (f (quote ("macros"))) (d #t) (k 0)) (d (n "tokio") (r "^1.27.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.16") (f (quote ("env-filter"))) (d #t) (k 0)))) (h "0pxiycfsji6k9qi2b75pmrd9b62v53zc7drj8wpikzqjjqk409vp")))

