(define-module (crates-io le dg ledger-polkadot) #:use-module (crates-io))

(define-public crate-ledger-polkadot-0.1.0 (c (n "ledger-polkadot") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "ed25519-dalek") (r "^1.0.0-pre.1") (d #t) (k 2)) (d (n "hex") (r "^0.4.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "ledger") (r "^0.2.5") (d #t) (k 0)) (d (n "matches") (r "^0.1.8") (d #t) (k 0)) (d (n "quick-error") (r "^1.2.2") (d #t) (k 0)) (d (n "sha2") (r "^0.8.0") (d #t) (k 2)))) (h "060gi4y918psa3bh685x48qcq5sl8qihxcfd7p9qc1rh5ww7iyy5")))

(define-public crate-ledger-polkadot-0.2.0 (c (n "ledger-polkadot") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "ed25519-dalek") (r "^1.0.0-pre.2") (d #t) (k 2)) (d (n "hex") (r "^0.4.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "ledger") (r "^0.2.5") (d #t) (k 0)) (d (n "matches") (r "^0.1.8") (d #t) (k 0)) (d (n "quick-error") (r "^1.2.2") (d #t) (k 0)) (d (n "sha2") (r "^0.8.0") (d #t) (k 2)))) (h "0j2fvpjl89xgw16p3mmzc7wgq9dc4nsn9szi8rc5m9566j22fpjp")))

