(define-module (crates-io le dg ledger_secure_sdk_sys) #:use-module (crates-io))

(define-public crate-ledger_secure_sdk_sys-1.0.0 (c (n "ledger_secure_sdk_sys") (v "1.0.0") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.73") (d #t) (k 1)))) (h "0ichyg29g95465bxhix788qlfzl6nkw9mrjmphzsi3mnigzml87x") (f (quote (("lib_bagl") ("ccid"))))))

(define-public crate-ledger_secure_sdk_sys-1.0.1 (c (n "ledger_secure_sdk_sys") (v "1.0.1") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.73") (d #t) (k 1)))) (h "0bsx0h5j6rb8llrh64h9qqh716lc0wyxlfbqa08lvlzii88c0qdw") (f (quote (("lib_bagl") ("ccid"))))))

(define-public crate-ledger_secure_sdk_sys-1.0.2 (c (n "ledger_secure_sdk_sys") (v "1.0.2") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.73") (d #t) (k 1)))) (h "1yjs1scm0q1nq4bqx8xsq8c5jzk6wbh8ccgq73qm2898qddz5ai2") (f (quote (("lib_bagl") ("ccid"))))))

(define-public crate-ledger_secure_sdk_sys-1.0.3 (c (n "ledger_secure_sdk_sys") (v "1.0.3") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.73") (d #t) (k 1)))) (h "0rh2l020hn132c7a2drf39vydvkv6xcy6if57z0rb8yqv704y990") (f (quote (("lib_bagl") ("ccid"))))))

(define-public crate-ledger_secure_sdk_sys-1.1.0 (c (n "ledger_secure_sdk_sys") (v "1.1.0") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.73") (d #t) (k 1)))) (h "1swxcd0zhfclzjr3v6l29icf9y0mbjgfflqw205cc304766jnkls") (f (quote (("lib_bagl") ("ccid"))))))

(define-public crate-ledger_secure_sdk_sys-1.2.0 (c (n "ledger_secure_sdk_sys") (v "1.2.0") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.73") (d #t) (k 1)))) (h "071q04yafprqzhrfxbvca79i1a8a3qfk956692pypfjpmpfh3qk0") (f (quote (("lib_bagl") ("ccid"))))))

(define-public crate-ledger_secure_sdk_sys-1.3.0 (c (n "ledger_secure_sdk_sys") (v "1.3.0") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.73") (d #t) (k 1)) (d (n "glob") (r "^0.3.1") (d #t) (k 1)))) (h "10lk9hx7ihqj1nzc056iq15ljp5mqpljk1fl91dgm091g99v48f4")))

