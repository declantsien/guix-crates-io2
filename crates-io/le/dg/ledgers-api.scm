(define-module (crates-io le dg ledgers-api) #:use-module (crates-io))

(define-public crate-ledgers-api-0.1.0 (c (n "ledgers-api") (v "0.1.0") (d (list (d (n "prost") (r "^0.6") (d #t) (k 0)) (d (n "tonic") (r "^0.2.1") (d #t) (k 0)) (d (n "tonic-build") (r "^0.2") (d #t) (k 1)))) (h "0h087ckw8mw9fhhfhmmdy91shz34y3zj7znw3wbjkv1fbdszk940")))

