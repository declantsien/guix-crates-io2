(define-module (crates-io le dg ledger-api) #:use-module (crates-io))

(define-public crate-ledger-api-0.1.1 (c (n "ledger-api") (v "0.1.1") (d (list (d (n "futures") (r "^0.3.5") (d #t) (k 0)) (d (n "prost") (r "^0.6") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)) (d (n "tonic") (r "^0.2.1") (d #t) (k 0)) (d (n "tonic-build") (r "^0.2") (d #t) (k 1)))) (h "03z1xwn3w499lqf1r78pnjpy670gj1yfvw2wa1hnp6mkxna6243f")))

