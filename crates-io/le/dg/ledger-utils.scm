(define-module (crates-io le dg ledger-utils) #:use-module (crates-io))

(define-public crate-ledger-utils-0.1.0 (c (n "ledger-utils") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "ledger-parser") (r "^4") (d #t) (k 0)) (d (n "rust_decimal") (r "^1") (d #t) (k 0)))) (h "0vlmics4k92wmw07rglxqzs9nh3m9xskxjl3ir92hv0mp65s7nn7")))

(define-public crate-ledger-utils-0.2.0 (c (n "ledger-utils") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "ledger-parser") (r "^5") (d #t) (k 0)) (d (n "rust_decimal") (r "^1") (d #t) (k 0)))) (h "0kzrj3g0vz87pyaha5fwcywfh2vfq0abki7ak2vj0pma0vv9kyl2")))

(define-public crate-ledger-utils-0.3.0 (c (n "ledger-utils") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "ledger-parser") (r "^5.1") (d #t) (k 0)) (d (n "rust_decimal") (r "^1") (d #t) (k 0)))) (h "0ajznw2wznqsjag1aj0klwf0g6kr3dd5w3w3ldjj1y4mvva8xh4f")))

(define-public crate-ledger-utils-0.4.0 (c (n "ledger-utils") (v "0.4.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "ledger-parser") (r "^5.1") (d #t) (k 0)) (d (n "rust_decimal") (r "^1") (d #t) (k 0)))) (h "1zd1241nlzwjy9lhwr8dymd8kj17za56403rwa19ns81yr0nwj2w")))

(define-public crate-ledger-utils-0.4.1 (c (n "ledger-utils") (v "0.4.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "ledger-parser") (r "^5.1") (d #t) (k 0)) (d (n "rust_decimal") (r "^1") (d #t) (k 0)))) (h "1xlq35bhwpmrgzk3c830ak57k5sa94v7bhnlrmmj4w8xs8lf2hxx")))

(define-public crate-ledger-utils-0.5.0 (c (n "ledger-utils") (v "0.5.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "ledger-parser") (r "^6") (d #t) (k 0)) (d (n "rust_decimal") (r "^1") (d #t) (k 0)))) (h "0b1cwxcgf461shfdnhckszxfizpsfgzw28qs01h7ickmv6w0ix9v")))

(define-public crate-ledger-utils-0.6.0 (c (n "ledger-utils") (v "0.6.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "ledger-parser") (r "^6") (d #t) (k 0)) (d (n "rust_decimal") (r "^1") (d #t) (k 0)))) (h "0r7v0g78xkmsqc24d9cblxc7ncz5kkg1v1gh9lf8iap8lkwqc9y1")))

