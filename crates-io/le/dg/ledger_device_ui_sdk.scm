(define-module (crates-io le dg ledger_device_ui_sdk) #:use-module (crates-io))

(define-public crate-ledger_device_ui_sdk-1.0.0 (c (n "ledger_device_ui_sdk") (v "1.0.0") (d (list (d (n "include_gif") (r "^1.0.0") (d #t) (k 0)) (d (n "ledger_secure_sdk_sys") (r "^1.0.0") (d #t) (k 0)))) (h "1ikz1iazphxxg3wjxpyzh2fhmpsgbllp8zsjb1k5l1v27y7zk46n") (f (quote (("speculos"))))))

(define-public crate-ledger_device_ui_sdk-1.1.0 (c (n "ledger_device_ui_sdk") (v "1.1.0") (d (list (d (n "include_gif") (r "^1.0.0") (d #t) (k 0)) (d (n "ledger_device_sdk") (r "^1.0.0") (d #t) (k 0)) (d (n "ledger_secure_sdk_sys") (r "^1.0.0") (d #t) (k 0)) (d (n "numtoa") (r "^0.2.4") (d #t) (k 0)))) (h "042kfbf1g8zhxxdrpjdnpi705hpjw6nkl5d7g8cyp78rmz9ys7sl") (f (quote (("speculos"))))))

(define-public crate-ledger_device_ui_sdk-1.1.1 (c (n "ledger_device_ui_sdk") (v "1.1.1") (d (list (d (n "include_gif") (r "^1.0.0") (d #t) (k 0)) (d (n "ledger_device_sdk") (r "^1.0.1") (d #t) (k 0)) (d (n "ledger_secure_sdk_sys") (r "^1.0.1") (d #t) (k 0)) (d (n "numtoa") (r "^0.2.4") (d #t) (k 0)))) (h "19cnrva1nwl2vsi73y451vmm988shzg6dxhx6f1r7a6kwnb23bxz") (f (quote (("speculos"))))))

