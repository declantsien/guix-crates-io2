(define-module (crates-io le dg ledger-cosmos-rs) #:use-module (crates-io))

(define-public crate-ledger-cosmos-rs-0.0.1 (c (n "ledger-cosmos-rs") (v "0.0.1") (d (list (d (n "ledger") (r "^0.0.4") (d #t) (k 0)))) (h "09p5x967l95h4y557ixxdqbijdz5gacncdgzr712mwlbl10wl2k1") (y #t)))

(define-public crate-ledger-cosmos-rs-0.0.2 (c (n "ledger-cosmos-rs") (v "0.0.2") (d (list (d (n "byteorder") (r "^1.2.4") (d #t) (k 0)) (d (n "ledger") (r "^0.0.6") (d #t) (k 0)) (d (n "matches") (r "^0.1.8") (d #t) (k 0)) (d (n "quick-error") (r "^1.2.2") (d #t) (k 0)))) (h "03zm58sdrb4yd7im12vip1bikabiz61gnqmw941cg9xfq4gvszyp") (y #t)))

(define-public crate-ledger-cosmos-rs-0.0.3 (c (n "ledger-cosmos-rs") (v "0.0.3") (d (list (d (n "byteorder") (r "^1.2.4") (d #t) (k 0)) (d (n "ledger") (r "^0.0.7") (d #t) (k 0)) (d (n "matches") (r "^0.1.8") (d #t) (k 0)) (d (n "quick-error") (r "^1.2.2") (d #t) (k 0)))) (h "1b61g40k83wzlv1ia1y8d39cvh1prcpp4jkghyph4yg5d0f438ml") (y #t)))

(define-public crate-ledger-cosmos-rs-0.0.4 (c (n "ledger-cosmos-rs") (v "0.0.4") (d (list (d (n "byteorder") (r "^1.2.4") (d #t) (k 0)) (d (n "ledger") (r "^0.0.7") (d #t) (k 0)) (d (n "matches") (r "^0.1.8") (d #t) (k 0)) (d (n "quick-error") (r "^1.2.2") (d #t) (k 0)))) (h "0bbbwpcwagl0mygbyfyr3dx6fflhz962y7f043qvmfhhhszda216") (y #t)))

(define-public crate-ledger-cosmos-rs-0.0.5 (c (n "ledger-cosmos-rs") (v "0.0.5") (d (list (d (n "byteorder") (r "^1.2.4") (d #t) (k 0)) (d (n "ledger") (r "^0.0.8") (d #t) (k 0)) (d (n "matches") (r "^0.1.8") (d #t) (k 0)) (d (n "quick-error") (r "^1.2.2") (d #t) (k 0)))) (h "0ldnai76cpi15v4kh9bzwbid8p29ri8dwq8s7jra1h18s2ibaxam") (y #t)))

(define-public crate-ledger-cosmos-rs-0.0.6 (c (n "ledger-cosmos-rs") (v "0.0.6") (d (list (d (n "byteorder") (r "^1.2.4") (d #t) (k 0)) (d (n "ledger") (r "^0.0.8") (d #t) (k 0)) (d (n "matches") (r "^0.1.8") (d #t) (k 0)) (d (n "quick-error") (r "^1.2.2") (d #t) (k 0)))) (h "1y0hi088lis8mj3l8hnccnhksqc07klbn6wzvaarrkfqsx9135r3") (y #t)))

