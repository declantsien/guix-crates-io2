(define-module (crates-io le dg ledger-parser) #:use-module (crates-io))

(define-public crate-ledger-parser-1.0.0 (c (n "ledger-parser") (v "1.0.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^4") (d #t) (k 0)) (d (n "rust_decimal") (r "^0.10") (d #t) (k 0)))) (h "0rdx254kk84wmrda42lkyc7y2xb8qfbgaqhz3wrdvjilzwglhlm1")))

(define-public crate-ledger-parser-1.0.1 (c (n "ledger-parser") (v "1.0.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^4") (d #t) (k 0)) (d (n "rust_decimal") (r "^0.10") (d #t) (k 0)))) (h "0ai17vhjx7fvkfhcav5an93i6xjlmh505jjxc83g96rgh9pfmg0h")))

(define-public crate-ledger-parser-2.0.0 (c (n "ledger-parser") (v "2.0.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^4") (d #t) (k 0)) (d (n "rust_decimal") (r "^0.10") (d #t) (k 0)))) (h "0nz9l52kdd60arbjpbjlsn4hxfy4rsm60dfl8pj4r8143yycn57i")))

(define-public crate-ledger-parser-2.1.0 (c (n "ledger-parser") (v "2.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^4") (d #t) (k 0)) (d (n "rust_decimal") (r "^0.10") (d #t) (k 0)))) (h "0m4q7hgfqwf3qfv2nbvh9jh3lad0wmjgx3n272d86d32p28hp4py")))

(define-public crate-ledger-parser-2.2.0 (c (n "ledger-parser") (v "2.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^4") (d #t) (k 0)) (d (n "rust_decimal") (r "^0.11") (d #t) (k 0)))) (h "1vr60988jizsydixl7hjpg1djll2wp3qxdsf3lz2za4m0rwpckvp")))

(define-public crate-ledger-parser-3.0.0 (c (n "ledger-parser") (v "3.0.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^4") (d #t) (k 0)) (d (n "rust_decimal") (r "^1") (d #t) (k 0)))) (h "1fs16iax6261kfrlfz8mkhh5srvicy9g2mz9s7j3waacmxb6szz0")))

(define-public crate-ledger-parser-3.1.0 (c (n "ledger-parser") (v "3.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^4") (d #t) (k 0)) (d (n "rust_decimal") (r "^1") (d #t) (k 0)))) (h "08nxbz3ma1kr4crpark1l958f30q3c6rkwax5ad4jvh840zyr1jp")))

(define-public crate-ledger-parser-4.0.0 (c (n "ledger-parser") (v "4.0.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^4") (d #t) (k 0)) (d (n "rust_decimal") (r "^1") (d #t) (k 0)))) (h "1kkllbigw693lnxcyr4c565cm5marapyx4746bl64rmblqy5m14k")))

(define-public crate-ledger-parser-4.1.0 (c (n "ledger-parser") (v "4.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "rust_decimal") (r "^1") (d #t) (k 0)))) (h "08sz7v6palgxhflz5bj4q1323n2ghyqyk6pcp9mjqkik45r3id13")))

(define-public crate-ledger-parser-5.0.0 (c (n "ledger-parser") (v "5.0.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "rust_decimal") (r "^1") (d #t) (k 0)))) (h "1fhjkn88x0ripgbykpc4w0ncl0zw4n5ahxzwnp7agacd81p2f001")))

(define-public crate-ledger-parser-5.1.0 (c (n "ledger-parser") (v "5.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "rust_decimal") (r "^1") (d #t) (k 0)))) (h "0rj5s0fd0v5lf4ywaa3ch49n1hggg00vm549wywcjqn7vk3y3l17")))

(define-public crate-ledger-parser-5.1.1 (c (n "ledger-parser") (v "5.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "rust_decimal") (r "^1") (d #t) (k 0)))) (h "0qbihk2x7ind848729jxppsx1lb9zqps9g0bkkymsk8s7plbdk7d")))

(define-public crate-ledger-parser-6.0.0 (c (n "ledger-parser") (v "6.0.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "ordered-float") (r "^4") (d #t) (k 0)) (d (n "rust_decimal") (r "^1") (d #t) (k 0)))) (h "13nzcnkq4837n2rnpl2rjag7j8qn3bk6lffx65w9hsvsyzr1x32m")))

