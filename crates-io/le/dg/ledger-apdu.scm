(define-module (crates-io le dg ledger-apdu) #:use-module (crates-io))

(define-public crate-ledger-apdu-0.4.0 (c (n "ledger-apdu") (v "0.4.0") (h "0p7ncnqfsbd8h8ahy3d7jirg1lydw8lnllc98jndk78r8r7a9yci")))

(define-public crate-ledger-apdu-0.7.0 (c (n "ledger-apdu") (v "0.7.0") (h "1cnksacwa9mxvvw26dnz14f3j53zzm4d1r82nbf5hdl632c7d1d4")))

(define-public crate-ledger-apdu-0.8.0 (c (n "ledger-apdu") (v "0.8.0") (h "0cyww79swr1srlliwza54h7sn5a5l1j1zfyd2a1p893jjxvhblvz")))

(define-public crate-ledger-apdu-0.9.0 (c (n "ledger-apdu") (v "0.9.0") (d (list (d (n "arrayref") (r "^0.3.6") (d #t) (k 0)) (d (n "no-std-compat") (r "^0.4.1") (d #t) (k 0)) (d (n "snafu") (r "^0.7.0") (f (quote ("rust_1_46"))) (k 0)))) (h "06ak32hm261rj2dxx05bvflqqg5gr134i6wcq6p3nlvnhq49zd0b") (f (quote (("std" "snafu/std" "no-std-compat/std") ("default" "std"))))))

(define-public crate-ledger-apdu-0.10.0 (c (n "ledger-apdu") (v "0.10.0") (d (list (d (n "arrayref") (r "^0.3.6") (d #t) (k 0)) (d (n "no-std-compat") (r "^0.4.1") (d #t) (k 0)) (d (n "snafu") (r "^0.7") (f (quote ("rust_1_46"))) (k 0)))) (h "145zmkr0xr46g8gfkwgx53yk10jb7ii5xvfdxyjympwpq435hhzy") (f (quote (("std" "snafu/std" "no-std-compat/std") ("default" "std"))))))

(define-public crate-ledger-apdu-0.11.0 (c (n "ledger-apdu") (v "0.11.0") (d (list (d (n "arrayref") (r "^0.3") (d #t) (k 0)) (d (n "no-std-compat") (r "^0.4") (d #t) (k 0)) (d (n "snafu") (r "^0.8") (k 0)))) (h "0s91hz35xlklw4wm04js7kzn1a4zih3ygsxj39km54kwv4lgs7y2") (f (quote (("std" "snafu/std" "no-std-compat/std") ("default" "std"))))))

