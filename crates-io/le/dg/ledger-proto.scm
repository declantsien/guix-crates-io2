(define-module (crates-io le dg ledger-proto) #:use-module (crates-io))

(define-public crate-ledger-proto-0.1.0 (c (n "ledger-proto") (v "0.1.0") (d (list (d (n "bitflags") (r "^2.1.0") (k 0)) (d (n "displaydoc") (r "^0.2.3") (k 0)) (d (n "encdec") (r "^0.9.0") (k 0)) (d (n "thiserror") (r "^1.0.40") (o #t) (d #t) (k 0)))) (h "1smdrcd6kaf9lw0xs7zz3kfkaiz0f50xhim0szp2rmyydqllja2j") (f (quote (("default" "std") ("alloc")))) (s 2) (e (quote (("std" "dep:thiserror" "alloc"))))))

