(define-module (crates-io le dg ledgerdb) #:use-module (crates-io))

(define-public crate-ledgerdb-1.0.0 (c (n "ledgerdb") (v "1.0.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^3.1.18") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1a59dgganbvlj91l22q6xi9rajc6qazbj166lysdkz4lpacsraah")))

(define-public crate-ledgerdb-1.1.0 (c (n "ledgerdb") (v "1.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^3.1.18") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "08878ml9sbj3jamp21zj81cfw3p99adi7m9iq01w29gwvzz0bcj1")))

