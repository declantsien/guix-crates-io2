(define-module (crates-io le dg ledger-kv) #:use-module (crates-io))

(define-public crate-ledger-kv-0.1.0 (c (n "ledger-kv") (v "0.1.0") (d (list (d (n "ahash") (r "^0.8.6") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "borsh") (r "^1.1.1") (d #t) (k 0)) (d (n "borsh-derive") (r "^1.1.1") (d #t) (k 0)) (d (n "clap") (r "^4.4.7") (d #t) (k 0)) (d (n "fs-err") (r "^2.9.0") (d #t) (k 0)) (d (n "indexmap") (r "^2.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "memmap2") (r "^0.9.0") (d #t) (t "cfg(target_arch = \"x86_64\")") (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)) (d (n "tempfile") (r "^3.8.0") (d #t) (k 2)))) (h "04p6mx50hg3zmvjnxcj7rdp2n04gkh76zxvbmafm3d4l79rdkz1h")))

