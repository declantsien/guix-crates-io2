(define-module (crates-io le dg ledger-cosmos) #:use-module (crates-io))

(define-public crate-ledger-cosmos-0.0.6 (c (n "ledger-cosmos") (v "0.0.6") (d (list (d (n "byteorder") (r "^1.2.4") (d #t) (k 0)) (d (n "ledger") (r "^0.0.8") (d #t) (k 0)) (d (n "matches") (r "^0.1.8") (d #t) (k 0)) (d (n "quick-error") (r "^1.2.2") (d #t) (k 0)))) (h "005ps9jx1m46xva1m3wd0l343xjfjcsw0ydj5w387lmackxly2dj")))

(define-public crate-ledger-cosmos-0.0.8 (c (n "ledger-cosmos") (v "0.0.8") (d (list (d (n "byteorder") (r "^1.2.4") (d #t) (k 0)) (d (n "ledger") (r "^0.0.9") (d #t) (k 0)) (d (n "matches") (r "^0.1.8") (d #t) (k 0)) (d (n "quick-error") (r "^1.2.2") (d #t) (k 0)))) (h "16w608dq9kl3wdga81bday3gfzmxkzgix889bpjjgjzibpk0n82w")))

(define-public crate-ledger-cosmos-0.0.9 (c (n "ledger-cosmos") (v "0.0.9") (d (list (d (n "byteorder") (r "^1.2.4") (d #t) (k 0)) (d (n "ledger") (r "^0.0.9") (d #t) (k 0)) (d (n "matches") (r "^0.1.8") (d #t) (k 0)) (d (n "quick-error") (r "^1.2.2") (d #t) (k 0)))) (h "0f71xjszmvk9g7iy6g7afkfq6fq647q8dlgxpjyn01887mv162p1")))

(define-public crate-ledger-cosmos-0.1.0 (c (n "ledger-cosmos") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.2.4") (d #t) (k 0)) (d (n "ledger") (r "^0.0.9") (d #t) (k 0)) (d (n "matches") (r "^0.1.8") (d #t) (k 0)) (d (n "quick-error") (r "^1.2.2") (d #t) (k 0)))) (h "1rcxprr0ffms1aa4vjxjf435r8cmxf0n5jbzwlhwggkylvlmy5n9")))

(define-public crate-ledger-cosmos-0.1.1 (c (n "ledger-cosmos") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.2.4") (d #t) (k 0)) (d (n "ed25519-dalek") (r "^0.8.1") (d #t) (k 2)) (d (n "ledger") (r "^0.0.9") (d #t) (k 0)) (d (n "matches") (r "^0.1.8") (d #t) (k 0)) (d (n "quick-error") (r "^1.2.2") (d #t) (k 0)) (d (n "sha2") (r "^0.7.0") (d #t) (k 2)))) (h "1d5q1914wscp4kgza9kyh2rmy8i9g9lzv4s2y1pnmyniapy3plxn")))

(define-public crate-ledger-cosmos-0.2.0 (c (n "ledger-cosmos") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.2.4") (d #t) (k 0)) (d (n "ed25519-dalek") (r "^0.8.1") (d #t) (k 2)) (d (n "ledger") (r "^0.0.9") (d #t) (k 0)) (d (n "matches") (r "^0.1.8") (d #t) (k 0)) (d (n "quick-error") (r "^1.2.2") (d #t) (k 0)) (d (n "sha2") (r "^0.7.0") (d #t) (k 2)))) (h "1caz0d9vqs7b5gznrgjf18i766awkf0yaxhalma0p4h9y84r2g4k")))

(define-public crate-ledger-cosmos-0.2.1 (c (n "ledger-cosmos") (v "0.2.1") (d (list (d (n "byteorder") (r "^1.2.4") (d #t) (k 0)) (d (n "ed25519-dalek") (r "^0.8.1") (d #t) (k 2)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "ledger") (r "^0.1.0") (d #t) (k 0)) (d (n "matches") (r "^0.1.8") (d #t) (k 0)) (d (n "quick-error") (r "^1.2.2") (d #t) (k 0)) (d (n "sha2") (r "^0.7.0") (d #t) (k 2)))) (h "0g64v07llxg6px7as97l0fhgvj37jmm48fhyixdxr1zrkmj822zd")))

