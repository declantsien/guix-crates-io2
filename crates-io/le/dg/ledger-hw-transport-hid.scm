(define-module (crates-io le dg ledger-hw-transport-hid) #:use-module (crates-io))

(define-public crate-ledger-hw-transport-hid-0.0.1 (c (n "ledger-hw-transport-hid") (v "0.0.1") (d (list (d (n "async-trait") (r "^0.1.24") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "hidapi") (r "^1.2.0") (f (quote ("linux-static-hidraw"))) (k 0)) (d (n "ledger-hw") (r "^0.0.1") (d #t) (k 0)) (d (n "ledger-hw-transport") (r "^0.0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.50") (d #t) (k 0)) (d (n "nix") (r "^0.13.0") (d #t) (k 0)))) (h "1qwjqjkmzsnz7f1pk0w9jv4ppcz9pny17vqmlv37vgrj5bl7ypda")))

