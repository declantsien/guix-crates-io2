(define-module (crates-io le dg ledge) #:use-module (crates-io))

(define-public crate-ledge-0.1.0 (c (n "ledge") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "dirs") (r "^1.0") (d #t) (k 0)) (d (n "dotenv") (r "^0.13") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)) (d (n "url") (r "^1.7") (d #t) (k 0)) (d (n "url_serde") (r "^0.2") (d #t) (k 0)) (d (n "uuid") (r "^0.7") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "06g6rsrblmxiyimkls30h3w1mv1rfkli1rfjz9ffs43nw4xcrcdw")))

