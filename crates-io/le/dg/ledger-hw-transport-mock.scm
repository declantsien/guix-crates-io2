(define-module (crates-io le dg ledger-hw-transport-mock) #:use-module (crates-io))

(define-public crate-ledger-hw-transport-mock-0.0.1 (c (n "ledger-hw-transport-mock") (v "0.0.1") (d (list (d (n "async-trait") (r "^0.1.24") (d #t) (k 0)) (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "ledger-hw-transport") (r "^0.0.1") (d #t) (k 0)) (d (n "regex") (r "^1.3.6") (d #t) (k 0)))) (h "14xz7dpfm84yqwjirbqlb2byb4d4f938693rngmy0zb2yazlnkps")))

