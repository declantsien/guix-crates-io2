(define-module (crates-io le dg ledger-hw-transport) #:use-module (crates-io))

(define-public crate-ledger-hw-transport-0.0.1 (c (n "ledger-hw-transport") (v "0.0.1") (d (list (d (n "async-trait") (r "^0.1.24") (d #t) (k 0)) (d (n "futures-await-test") (r "^0.3.0") (d #t) (k 2)) (d (n "ledger-hw") (r "^0.0.1") (d #t) (k 0)))) (h "10bb3h2znih46cpmqxhhnzqq772443q9jrif7glmq37y9wbqa349")))

