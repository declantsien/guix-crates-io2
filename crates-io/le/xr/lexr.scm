(define-module (crates-io le xr lexr) #:use-module (crates-io))

(define-public crate-lexr-0.1.0 (c (n "lexr") (v "0.1.0") (d (list (d (n "concat-idents") (r "^1.1.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "0skmvhrs0y8vkf9sj1y6divp7wnmrvjwj9c3r8g3rkrgqxjgvrw5")))

