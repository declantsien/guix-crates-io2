(define-module (crates-io le xp lexpr-macros) #:use-module (crates-io))

(define-public crate-lexpr-macros-0.1.0 (c (n "lexpr-macros") (v "0.1.0") (d (list (d (n "proc-macro-hack") (r "^0.5.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)))) (h "1wf6nnavig80h8hmpgsjlw9zqa3l74lc9sw8d2q30wqpmkmwywq3")))

(define-public crate-lexpr-macros-0.1.1 (c (n "lexpr-macros") (v "0.1.1") (d (list (d (n "proc-macro-hack") (r "^0.5.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)))) (h "0x62m42p96v5b486qs0f2zpz2vlfq749mgsp332vmb7kpd9yq8qm")))

(define-public crate-lexpr-macros-0.1.2 (c (n "lexpr-macros") (v "0.1.2") (d (list (d (n "proc-macro-hack") (r "^0.5.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)))) (h "08apxk05l2gq7aykvxh880mgmmdsf6p97qax6my4mrlkmpwdnqj0")))

(define-public crate-lexpr-macros-0.2.0 (c (n "lexpr-macros") (v "0.2.0") (d (list (d (n "proc-macro-hack") (r "^0.5.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)))) (h "079134mi2kwbzprar5jvcyh1w6s57825almb21whx269xxzpdk8x")))

(define-public crate-lexpr-macros-0.2.1 (c (n "lexpr-macros") (v "0.2.1") (d (list (d (n "proc-macro-hack") (r "^0.5.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "0d8wpcvs1g2xqddw3rddms8iragp0n92a6460s6hvh0risrpyqnd")))

(define-public crate-lexpr-macros-0.2.2 (c (n "lexpr-macros") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "1frm3zi3dhlrgbprfhynvmsga52jhv2bby50q651mj45p65wpd9n")))

