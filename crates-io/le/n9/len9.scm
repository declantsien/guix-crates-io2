(define-module (crates-io le n9 len9) #:use-module (crates-io))

(define-public crate-len9-0.3.3 (c (n "len9") (v "0.3.3") (d (list (d (n "clap") (r "^2.33.3") (o #t) (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "0wxn4p8gvry7i6cdqw0cn0km4csc56xrrms4jy935yi3ghqsbyil") (f (quote (("application" "clap")))) (y #t)))

(define-public crate-len9-0.3.4 (c (n "len9") (v "0.3.4") (d (list (d (n "clap") (r "^2.33.3") (o #t) (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "0riqvmwd7if6vnwrgnsr36c23jkpyvky5j574898j654nbvzvm1i") (f (quote (("application" "clap")))) (y #t)))

