(define-module (crates-io le kn leknet) #:use-module (crates-io))

(define-public crate-leknet-0.1.0 (c (n "leknet") (v "0.1.0") (d (list (d (n "bevy_app") (r "^0.10.1") (d #t) (k 0)) (d (n "bevy_ecs") (r "^0.10.1") (d #t) (k 0)) (d (n "bevy_quinnet") (r "^0.4.0") (d #t) (k 0)) (d (n "bevy_reflect") (r "^0.10.1") (d #t) (k 0)) (d (n "bimap") (r "^0.6.3") (d #t) (k 0)) (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "port_scanner") (r "^0.1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (f (quote ("derive"))) (d #t) (k 0)))) (h "1fq4a2j7cxh6n9ngb6v20yjqnnxizfg5mwlj2cil2s0wh41q8n33")))

