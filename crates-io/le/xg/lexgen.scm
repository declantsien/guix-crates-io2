(define-module (crates-io le xg lexgen) #:use-module (crates-io))

(define-public crate-lexgen-0.1.0 (c (n "lexgen") (v "0.1.0") (d (list (d (n "fxhash") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "19hswsschqirympdj7qy5hdf8nwqndkhyizgy7dn7x3vm6pm036z")))

(define-public crate-lexgen-0.2.0 (c (n "lexgen") (v "0.2.0") (d (list (d (n "fxhash") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0j6ay9kfxxaaqg1gd9w6bx741rfnv9d3c0ccki7jpf7grk9rn412")))

(define-public crate-lexgen-0.2.1 (c (n "lexgen") (v "0.2.1") (d (list (d (n "fxhash") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "17s9gi74dc9cfs5wc6l36k2sq2hgiiqzvqv8yc3m513yyhjddpds")))

(define-public crate-lexgen-0.2.2 (c (n "lexgen") (v "0.2.2") (d (list (d (n "fxhash") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1xymf4fh3ijlmllf95bz3aldj1yx38w3j9xilg8sa7zixmz8rrvq")))

(define-public crate-lexgen-0.3.0 (c (n "lexgen") (v "0.3.0") (d (list (d (n "fxhash") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0y0k2pgzwqw54zvp1f2i4g81c2r55ishn08xqv0q15hjf45mf42j")))

(define-public crate-lexgen-0.4.0 (c (n "lexgen") (v "0.4.0") (d (list (d (n "fxhash") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1h4dj8v68wa7h0hhlnlxgg1h6h6jirck4jrqnmx73r8p78852530")))

(define-public crate-lexgen-0.5.0 (c (n "lexgen") (v "0.5.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "fxhash") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1z9n0iiyvj3j7yapl2x8s1129im1k9mxsk94vv23nygnlwvdhqrf")))

(define-public crate-lexgen-0.6.0 (c (n "lexgen") (v "0.6.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "fxhash") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1ks8rj55q6wvkaa9q6di0p9js7m58125yvmp0j4yzi69b4z5q3a3")))

(define-public crate-lexgen-0.7.0 (c (n "lexgen") (v "0.7.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "fxhash") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "03z9n8srh12r6p1c4fm9arby6z3j028nqnl44za7pdqbdbdisxgr")))

(define-public crate-lexgen-0.8.0 (c (n "lexgen") (v "0.8.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "fxhash") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0q7f759iv7lxqwxsnjrr5fiqrpv3my4gzf3m6lg7nqwvvq6r5zdq")))

(define-public crate-lexgen-0.8.1 (c (n "lexgen") (v "0.8.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "fxhash") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0lj6rzhmw7bqqy9k9pv48n6xizz2vgrf8mrybpr739i6p2mdvjfq")))

(define-public crate-lexgen-0.9.0 (c (n "lexgen") (v "0.9.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "fxhash") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0ai2qmkm7s0vw5snmsyw8rcqj48m7aaiyakmvp6lsrgv4sizaf1w")))

(define-public crate-lexgen-0.10.0 (c (n "lexgen") (v "0.10.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "fxhash") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1f5adj88ld1ss2a6fviq3fyazvp2xhmh41hf5iqm5xv4sfmm5zf8")))

(define-public crate-lexgen-0.11.0 (c (n "lexgen") (v "0.11.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "fxhash") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "048m93q9czdk34aihpkwsm7l9hnjra4af1d041w81vcc14v9xhn8")))

(define-public crate-lexgen-0.12.0 (c (n "lexgen") (v "0.12.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "fxhash") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "fold" "full" "visit"))) (d #t) (k 0)))) (h "192mbhn15a0nywvy7vszkhsqwjalwypavxzzrz7qkqb3ya4jgh85")))

(define-public crate-lexgen-0.13.0 (c (n "lexgen") (v "0.13.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "fxhash") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "fold" "full" "visit"))) (d #t) (k 0)))) (h "0sb88qli310j800sahgaqfdj7dam1h3nj2g7y3id7sij77a9cf57")))

(define-public crate-lexgen-0.14.0 (c (n "lexgen") (v "0.14.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "fxhash") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "fold" "full" "visit"))) (d #t) (k 0)))) (h "0fkhkxid9j0zghh19gcn6a942ndzk67apinkkkss80bjdn6iwbb0")))

(define-public crate-lexgen-0.15.0 (c (n "lexgen") (v "0.15.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.30") (f (quote ("extra-traits" "fold" "full" "visit"))) (d #t) (k 0)))) (h "147lgpg2f7c99w3xgzbcpphzgap7b25lrqwihj5xchcnxx776s8s")))

