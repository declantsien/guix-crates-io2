(define-module (crates-io le xg lexgen_util) #:use-module (crates-io))

(define-public crate-lexgen_util-0.8.0 (c (n "lexgen_util") (v "0.8.0") (d (list (d (n "unicode-width") (r "^0.1.9") (d #t) (k 0)))) (h "0056r9iz7nlxn328j7z6azgh3lcvfyrr1264zjp5rcm55kyg6y96")))

(define-public crate-lexgen_util-0.8.1 (c (n "lexgen_util") (v "0.8.1") (d (list (d (n "unicode-width") (r "^0.1.9") (d #t) (k 0)))) (h "1jw10r6bx0baawbm5w53gfy5m8vqzz63ms4wwpfr7dd88agjw1gb")))

(define-public crate-lexgen_util-0.9.0 (c (n "lexgen_util") (v "0.9.0") (d (list (d (n "unicode-width") (r "^0.1.9") (d #t) (k 0)))) (h "10ajcnp5wjjk725dp2lyfdq23dqxdncwy9m4kw9rwgzbap81r66g")))

(define-public crate-lexgen_util-0.10.0 (c (n "lexgen_util") (v "0.10.0") (d (list (d (n "unicode-width") (r "^0.1.9") (d #t) (k 0)))) (h "1zva56cx4ld1ashd1in39lylfkq3n5w7lipj175iwg0m6z138wka")))

(define-public crate-lexgen_util-0.11.0 (c (n "lexgen_util") (v "0.11.0") (d (list (d (n "unicode-width") (r "^0.1.9") (d #t) (k 0)))) (h "0iwfhf8qdilwi989wjxgc4rf8ydj2rpb4dxx7w84i0wncq0h5sqq")))

(define-public crate-lexgen_util-0.12.0 (c (n "lexgen_util") (v "0.12.0") (d (list (d (n "unicode-width") (r "^0.1.9") (d #t) (k 0)))) (h "1w58inqny15m5i8bf63a131yc7ifis3147rvhi061wdhy5knlwjf")))

(define-public crate-lexgen_util-0.13.0 (c (n "lexgen_util") (v "0.13.0") (d (list (d (n "unicode-width") (r "^0.1.10") (d #t) (k 0)))) (h "0y4qfn271cnmaz76pyfki470igfj0mnxwk0nfd7f0nvkf7qvv8cb")))

(define-public crate-lexgen_util-0.14.0 (c (n "lexgen_util") (v "0.14.0") (d (list (d (n "unicode-width") (r "^0.1.10") (d #t) (k 0)))) (h "0cv10fbkl260fgaxbwjqv7656vfmxx6gswvy6j4j6vigskma9qdw")))

(define-public crate-lexgen_util-0.15.0 (c (n "lexgen_util") (v "0.15.0") (d (list (d (n "unicode-width") (r "^0.1.10") (d #t) (k 0)))) (h "1k0hfxj41gdaf4k2s76xah18r9bscx8sfyzcgjvg6iy0pk601bsb")))

