(define-module (crates-io le ts lets-encrypt-warp) #:use-module (crates-io))

(define-public crate-lets-encrypt-warp-0.0.1 (c (n "lets-encrypt-warp") (v "0.0.1") (d (list (d (n "acme-client") (r "^0.5.3") (d #t) (k 0)) (d (n "warp") (r "^0.1.16") (f (quote ("tls"))) (d #t) (k 0)))) (h "0dh2j59qkbm1q51xyvv08hvn3qnlq20sivd43p9788bfjzx96w57")))

(define-public crate-lets-encrypt-warp-0.0.2 (c (n "lets-encrypt-warp") (v "0.0.2") (d (list (d (n "acme-client") (r "^0.5.3") (d #t) (k 0)) (d (n "warp") (r "^0.1.16") (f (quote ("tls"))) (d #t) (k 0)))) (h "0gp0p0w2vv1q80gajzj12nqllw28g1sq6gg60rhwwvflkvbmd70f")))

(define-public crate-lets-encrypt-warp-0.0.3 (c (n "lets-encrypt-warp") (v "0.0.3") (d (list (d (n "acme-client") (r "^0.5.3") (d #t) (k 0)) (d (n "warp") (r "^0.1.16") (f (quote ("tls"))) (d #t) (k 0)))) (h "17pa9166l89yv1f95d7a39kxcdhhn0sdgwzagb2vldnyny7s7mk1")))

(define-public crate-lets-encrypt-warp-0.0.4 (c (n "lets-encrypt-warp") (v "0.0.4") (d (list (d (n "acme-client") (r "^0.5.3") (d #t) (k 0)) (d (n "warp") (r "^0.1.16") (f (quote ("tls"))) (d #t) (k 0)))) (h "09hcbgkbdhy4qxmvk0vrb5sijxs4wg2jq1hfgj5cgj1csd8dicv0")))

(define-public crate-lets-encrypt-warp-0.0.5 (c (n "lets-encrypt-warp") (v "0.0.5") (d (list (d (n "acme-client") (r "^0.5.3") (d #t) (k 0)) (d (n "futures") (r "^0.1.28") (d #t) (k 0)) (d (n "warp") (r "^0.1.16") (f (quote ("tls"))) (d #t) (k 0)) (d (n "x509-parser") (r "^0.5.1") (d #t) (k 0)))) (h "0f1vfjgjrnzjmp5fdlqbml8rlw97gc6v587zafz5szf2zgxaa5zn")))

(define-public crate-lets-encrypt-warp-0.0.6 (c (n "lets-encrypt-warp") (v "0.0.6") (d (list (d (n "acme-client") (r "^0.5.3") (d #t) (k 0)) (d (n "futures") (r "^0.1.28") (d #t) (k 0)) (d (n "tokio") (r "^0.1.22") (d #t) (k 0)) (d (n "warp") (r "^0.1.16") (f (quote ("tls"))) (d #t) (k 0)) (d (n "x509-parser") (r "^0.5.1") (d #t) (k 0)))) (h "193r1xfa2d9q7n32yb61z014ihkf5rd2jvjgcchgd6kwqz4sdc1h")))

(define-public crate-lets-encrypt-warp-0.0.7 (c (n "lets-encrypt-warp") (v "0.0.7") (d (list (d (n "acme-client") (r "^0.5.3") (d #t) (k 0)) (d (n "futures") (r "^0.1.28") (d #t) (k 0)) (d (n "tokio") (r "^0.1.22") (d #t) (k 0)) (d (n "warp") (r "^0.1.16") (f (quote ("tls"))) (d #t) (k 0)) (d (n "x509-parser") (r "^0.5.1") (d #t) (k 0)))) (h "012j6yjfvgg4xikqghjar3f3208sv5irz7kj2z37x79ljpzkna70")))

(define-public crate-lets-encrypt-warp-0.1.0 (c (n "lets-encrypt-warp") (v "0.1.0") (d (list (d (n "acme-lib") (r "^0.5.0") (d #t) (k 0)) (d (n "futures") (r "^0.1.28") (d #t) (k 0)) (d (n "tokio") (r "^0.1.22") (d #t) (k 0)) (d (n "warp") (r "^0.1.16") (f (quote ("tls"))) (d #t) (k 0)) (d (n "x509-parser") (r "^0.5.1") (d #t) (k 0)))) (h "1lqgbqwpxvx9a0lp2r7f7d2arki2ggcss33l8qjvylw9fy956bqq")))

(define-public crate-lets-encrypt-warp-0.2.0 (c (n "lets-encrypt-warp") (v "0.2.0") (d (list (d (n "acme-lib") (r "^0.5.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "tokio") (r "^0.2.9") (d #t) (k 0)) (d (n "warp") (r "^0.2.0") (f (quote ("tls"))) (d #t) (k 0)) (d (n "x509-parser") (r "^0.6.0") (d #t) (k 0)))) (h "1yrdlb34ws98znpl99rkac2k2196n547kxw4nv9rlwz3ygkfbvh0")))

(define-public crate-lets-encrypt-warp-0.2.1 (c (n "lets-encrypt-warp") (v "0.2.1") (d (list (d (n "acme-lib") (r "^0.8.2") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "openssl-sys") (r "^0.9.77") (f (quote ("vendored"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2.9") (d #t) (k 0)) (d (n "warp") (r "^0.2.0") (f (quote ("tls"))) (d #t) (k 0)) (d (n "x509-parser") (r "^0.6.0") (d #t) (k 0)))) (h "1fz4w496l12vdbz5nc3w7779a3q536kkhc9i1fkavvpfxwfvvz5y")))

(define-public crate-lets-encrypt-warp-0.3.0 (c (n "lets-encrypt-warp") (v "0.3.0") (d (list (d (n "acme-lib") (r "^0.8.2") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "openssl-sys") (r "^0.9.77") (f (quote ("vendored"))) (d #t) (k 0)) (d (n "tokio") (r "^1.22.0") (f (quote ("time" "rt"))) (d #t) (k 0)) (d (n "warp") (r "^0.3.3") (f (quote ("tls"))) (d #t) (k 0)) (d (n "x509-parser") (r "^0.6.0") (d #t) (k 0)))) (h "081sg1zd1nws659kzv9zvhvdpirdrqrrgi06zcz0iwn6klzd4dkc")))

