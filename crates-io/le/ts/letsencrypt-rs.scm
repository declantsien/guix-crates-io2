(define-module (crates-io le ts letsencrypt-rs) #:use-module (crates-io))

(define-public crate-letsencrypt-rs-0.0.0 (c (n "letsencrypt-rs") (v "0.0.0") (h "0zhgdp3c819rpkjfnryhp6bj5xav286hwnqn488gkzx5ifhd03ch")))

(define-public crate-letsencrypt-rs-0.1.0 (c (n "letsencrypt-rs") (v "0.1.0") (d (list (d (n "acme-client") (r "^0.1") (d #t) (k 0)) (d (n "clap") (r "^2.10.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.3") (d #t) (k 0)))) (h "0nzrhhv8bd993v2p4v84f1gh71yglklck6h6yymdags9w0x9yzi9")))

(define-public crate-letsencrypt-rs-0.1.1 (c (n "letsencrypt-rs") (v "0.1.1") (d (list (d (n "acme-client") (r "^0.1") (d #t) (k 0)) (d (n "clap") (r "^2.10.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.3") (d #t) (k 0)))) (h "1xxrx89sx4hwvl1jwnpfh75r0incjddl15s90a1gnlrvd9dz6rqc")))

(define-public crate-letsencrypt-rs-0.1.2 (c (n "letsencrypt-rs") (v "0.1.2") (d (list (d (n "acme-client") (r "^0.1.2") (d #t) (k 0)) (d (n "clap") (r "^2.10.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.3") (d #t) (k 0)))) (h "1rdfpy3rpm3r9syrbgd00bnrs9pl4h42h8cv93rz9zkj5pbr513y")))

(define-public crate-letsencrypt-rs-0.1.3 (c (n "letsencrypt-rs") (v "0.1.3") (d (list (d (n "acme-client") (r "^0.2.0") (d #t) (k 0)) (d (n "clap") (r "^2.10.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.3") (d #t) (k 0)))) (h "0czipdhjzb2zxzr40l6wc1cc3ihqk1m63cyf797nbxdr198lwbsi")))

(define-public crate-letsencrypt-rs-0.1.4 (c (n "letsencrypt-rs") (v "0.1.4") (d (list (d (n "acme-client") (r "^0.3") (d #t) (k 0)) (d (n "clap") (r "^2.10") (d #t) (k 0)) (d (n "env_logger") (r "^0.3") (d #t) (k 0)))) (h "1sp1xamywm7sg85il79021cdabrmhjqvjdabcqyi1imvw82dnxzi")))

(define-public crate-letsencrypt-rs-0.2.0 (c (n "letsencrypt-rs") (v "0.2.0") (d (list (d (n "acme-client") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "env_logger") (r "^0.4") (d #t) (k 0)))) (h "0qmk39vm3qg2grh5v60snxzd92hraa9ams5cdw5r6mwnzchbw547")))

(define-public crate-letsencrypt-rs-0.2.1 (c (n "letsencrypt-rs") (v "0.2.1") (d (list (d (n "acme-client") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "env_logger") (r "^0.4") (d #t) (k 0)))) (h "1lp51arp3yp32yzspqqawknac358dmwpp85grrcfss1f7xz35r8x")))

(define-public crate-letsencrypt-rs-0.2.2 (c (n "letsencrypt-rs") (v "0.2.2") (d (list (d (n "acme-client") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "env_logger") (r "^0.4") (d #t) (k 0)))) (h "0wbkw6r4gsy3qyr5vizdw2q3j72scs1ymgq6r4n1kn2d2w77w2iv")))

(define-public crate-letsencrypt-rs-0.3.0 (c (n "letsencrypt-rs") (v "0.3.0") (h "1xx2i5pbrvl59y35aa55zr40zri6xh8dmjpjj0iiin4q8s2jqbyz")))

