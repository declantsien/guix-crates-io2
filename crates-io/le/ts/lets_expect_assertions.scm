(define-module (crates-io le ts lets_expect_assertions) #:use-module (crates-io))

(define-public crate-lets_expect_assertions-0.1.0 (c (n "lets_expect_assertions") (v "0.1.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "lets_expect_core") (r "^0") (d #t) (k 0)))) (h "1lndpysv81gkl0gillazhqag81vms14qiz3nih3k1kgny9524c99")))

(define-public crate-lets_expect_assertions-0.1.1 (c (n "lets_expect_assertions") (v "0.1.1") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "lets_expect_core") (r "^0") (d #t) (k 0)))) (h "00wdrq9wylr0m99n90af68wbiv4cgy43jf35idqgqz1a6wd5bznw") (f (quote (("tokio" "lets_expect_core/tokio")))) (r "1.64")))

(define-public crate-lets_expect_assertions-0.2.0 (c (n "lets_expect_assertions") (v "0.2.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "lets_expect_core") (r "=0.2.0") (d #t) (k 0)))) (h "1bz32mvilbp9aq4gi4wddbcchhh646ynsmzjfzz7ligf7ddra2pq") (f (quote (("tokio" "lets_expect_core/tokio")))) (r "1.64")))

(define-public crate-lets_expect_assertions-0.2.1 (c (n "lets_expect_assertions") (v "0.2.1") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "lets_expect_core") (r "=0.2.1") (d #t) (k 0)))) (h "1bwfhasfyfa89wsdsac1hph88rsj208wsw4lgpcf28kybqdwy5i8") (f (quote (("tokio" "lets_expect_core/tokio")))) (r "1.64")))

(define-public crate-lets_expect_assertions-0.3.0 (c (n "lets_expect_assertions") (v "0.3.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "lets_expect_core") (r "=0.3.0") (d #t) (k 0)))) (h "065w4z5fiyf4m24y5v586aay76fjalp1sqn93fzcgm8kjhcda1l9") (f (quote (("tokio" "lets_expect_core/tokio")))) (r "1.64")))

(define-public crate-lets_expect_assertions-0.3.1 (c (n "lets_expect_assertions") (v "0.3.1") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "lets_expect_core") (r "=0.3.1") (d #t) (k 0)))) (h "16vssjqv6nwcszan88v2p0p6l5jiql6wgq9wk2nxr6h409q6cwa7") (f (quote (("tokio" "lets_expect_core/tokio")))) (r "1.64")))

(define-public crate-lets_expect_assertions-0.4.0 (c (n "lets_expect_assertions") (v "0.4.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "lets_expect_core") (r "=0.4.0") (d #t) (k 0)))) (h "10m6y409i1payr60zx6bcycjk9l51v04adqmp7z2lbhbgghc2xx1") (f (quote (("tokio" "lets_expect_core/tokio")))) (r "1.64")))

(define-public crate-lets_expect_assertions-0.5.0 (c (n "lets_expect_assertions") (v "0.5.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "lets_expect_core") (r "=0.5.0") (d #t) (k 0)))) (h "1sw84rps4pj6d2f9abz680xnc1ak065672ra5ay1nv32zv6ym3qs") (f (quote (("tokio" "lets_expect_core/tokio")))) (r "1.64")))

(define-public crate-lets_expect_assertions-0.5.1 (c (n "lets_expect_assertions") (v "0.5.1") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "lets_expect_core") (r "=0.5.1") (d #t) (k 0)))) (h "1sdzd9fxg60a7k53b2cf37jz74d0giajwxrfisa4dm17wvhn5jq2") (f (quote (("tokio" "lets_expect_core/tokio")))) (r "1.64")))

