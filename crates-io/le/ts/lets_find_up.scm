(define-module (crates-io le ts lets_find_up) #:use-module (crates-io))

(define-public crate-lets_find_up-0.0.1 (c (n "lets_find_up") (v "0.0.1") (d (list (d (n "path") (r "^0.0.8") (d #t) (k 0) (p "nodejs_path")))) (h "0zfp1fxd6r95l9jxa821n60abwyr83zc0hclvkg9ar62fn644f2m")))

(define-public crate-lets_find_up-0.0.2 (c (n "lets_find_up") (v "0.0.2") (d (list (d (n "path") (r "^0.0.8") (d #t) (k 0) (p "nodejs_path")))) (h "11zplknnfci620dlgbklccnj9i4rzvc8v5sryhgnqpl90mdbq4qj")))

(define-public crate-lets_find_up-0.0.3 (c (n "lets_find_up") (v "0.0.3") (d (list (d (n "sugar_path") (r "^0.0.11") (d #t) (k 2)))) (h "1gdgyprlm6l2lxnrr3yf2p3c4wvidh4vrj3c90jy00231gxi86mr")))

