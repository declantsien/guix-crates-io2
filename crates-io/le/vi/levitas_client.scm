(define-module (crates-io le vi levitas_client) #:use-module (crates-io))

(define-public crate-levitas_client-1.0.0 (c (n "levitas_client") (v "1.0.0") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "16l9lp60iqls82f54m2jj4vqhdsprzgdcy8aza2f0vdmjpgnmgzq")))

(define-public crate-levitas_client-1.0.1 (c (n "levitas_client") (v "1.0.1") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "16372nxjzmd504gh6c5ga97v5g3kqavd3h4anbryx4xysbqlgxsy")))

(define-public crate-levitas_client-1.0.2 (c (n "levitas_client") (v "1.0.2") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "04gzirmqqycpw9gaxclm32y55f67d24s4fd7iavk6dbdgcs20rsv")))

(define-public crate-levitas_client-1.0.3 (c (n "levitas_client") (v "1.0.3") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0dwpkwzwn3xdr5gfafcz9wxla4aikz6c79rlz4d8knvrm22pzn1g")))

(define-public crate-levitas_client-1.1.0 (c (n "levitas_client") (v "1.1.0") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1bgl1b9gx9rk3l00ab996m7483ikb1msg3ns7h8v14y0mlkl3bbb")))

(define-public crate-levitas_client-2.0.0 (c (n "levitas_client") (v "2.0.0") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0fsbfcrwj7i1l9jz252vlqdylwdjy4q4g1mqca1xm02wl947xl8k")))

