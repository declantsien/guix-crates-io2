(define-module (crates-io le vi levin) #:use-module (crates-io))

(define-public crate-levin-0.1.0 (c (n "levin") (v "0.1.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 0)) (d (n "serde_epee") (r "^0.2") (d #t) (k 0)))) (h "04mc1fj1jfy2sffw2pmsy6dslzs7gsyp5af5dpyr2q7ri1c6807v")))

