(define-module (crates-io le ak leak) #:use-module (crates-io))

(define-public crate-leak-0.1.0 (c (n "leak") (v "0.1.0") (h "0qy17vqa1irfi6vlhxpnncfwqhnyhdjs04jr3dg66m0vwbjppw26")))

(define-public crate-leak-0.1.1 (c (n "leak") (v "0.1.1") (h "0v37q84va9njrvzhkqdv73825wljnxkli4pf8g3g6ax9pdmynyz2")))

(define-public crate-leak-0.1.2 (c (n "leak") (v "0.1.2") (h "0wqcb8jlacif35j535gsqywx09dbmqcj5l57vw42jkqmy40hw45x")))

