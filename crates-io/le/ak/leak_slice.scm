(define-module (crates-io le ak leak_slice) #:use-module (crates-io))

(define-public crate-leak_slice-0.1.0 (c (n "leak_slice") (v "0.1.0") (h "178k3fkrbgkxdd0k3ipr4z1qhm1mjd9w7f1f02fn34jyd7cjn2vs")))

(define-public crate-leak_slice-0.1.1 (c (n "leak_slice") (v "0.1.1") (h "19qr8113nra3pbgaqrvawnb0n1w6ibl54bmjlzhdlxyw7i6c9lw9")))

(define-public crate-leak_slice-0.2.0 (c (n "leak_slice") (v "0.2.0") (h "09pd79s4ndjvnhcirwbs2yvxjvfj7kfnsc71jiir0hgvm5ykiwzc")))

