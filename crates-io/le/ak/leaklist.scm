(define-module (crates-io le ak leaklist) #:use-module (crates-io))

(define-public crate-leaklist-0.1.0 (c (n "leaklist") (v "0.1.0") (h "0ami44wxp3nsqcya8770f4v82ii5if2lfx3yyr9524rk9bwa7bkx")))

(define-public crate-leaklist-0.2.0 (c (n "leaklist") (v "0.2.0") (h "1yzla43jm8xfwcsvlsv0fqfram3r2jjzcsapr6mhqz85gcs4ja6r")))

(define-public crate-leaklist-0.2.1 (c (n "leaklist") (v "0.2.1") (h "03vibmga4b6jyf4vlgva2ipj64dag8ak772wll38drm81q4ljm8p")))

(define-public crate-leaklist-0.2.2 (c (n "leaklist") (v "0.2.2") (h "0rkn1qbcqfhg682fx9nlc47qdnd42z3nqr2mz323q5pkbim2y5zp")))

(define-public crate-leaklist-0.2.3 (c (n "leaklist") (v "0.2.3") (h "0k4mrl9pkviwl43q6j1ijk9c7p11jzzi4zhxng95w1ajpflq2hgv")))

(define-public crate-leaklist-0.2.4 (c (n "leaklist") (v "0.2.4") (h "08sgslpra2g186kcn44k8yhfyvj2n29s0pjsl4i70c9v8jbjiwrr")))

