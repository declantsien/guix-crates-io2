(define-module (crates-io le ak leaky-bucket-lite) #:use-module (crates-io))

(define-public crate-leaky-bucket-lite-0.1.0 (c (n "leaky-bucket-lite") (v "0.1.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("time" "sync"))) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "0khijck7bkjf3dj1b02risffdrgx0diiyhk6k2yhycnx2qa6g641")))

(define-public crate-leaky-bucket-lite-0.1.1 (c (n "leaky-bucket-lite") (v "0.1.1") (d (list (d (n "tokio") (r "^1") (f (quote ("time" "sync"))) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1k1nv7rxf85zycyhz0iyf43ncp9a720kvchhz3k2i1sy71fj0k8i")))

(define-public crate-leaky-bucket-lite-0.1.2 (c (n "leaky-bucket-lite") (v "0.1.2") (d (list (d (n "tokio") (r "^1") (f (quote ("time" "sync"))) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1r3wnlzbk4hqrpvcf491fgikci51mxp7vmrkjfhfk77kjlrg1m0n")))

(define-public crate-leaky-bucket-lite-0.1.3 (c (n "leaky-bucket-lite") (v "0.1.3") (d (list (d (n "tokio") (r "^1") (f (quote ("time" "sync"))) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1s8b5vqllz4b0m74vvmm8flmyix6nkms05pm0wprggi2gsfza0bb")))

(define-public crate-leaky-bucket-lite-0.1.4 (c (n "leaky-bucket-lite") (v "0.1.4") (d (list (d (n "tokio") (r "^1") (f (quote ("time" "sync"))) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "0q4d81lwyifnqjmpjcv0f2cja6j2wfb6xy53l5z0jrxrc1kipl78")))

(define-public crate-leaky-bucket-lite-0.2.0 (c (n "leaky-bucket-lite") (v "0.2.0") (d (list (d (n "tokio") (r "^1") (f (quote ("time" "sync"))) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread" "macros"))) (k 2)))) (h "1gkm41lqicmn91v1ca84bdfaq0x97bmk88l3d1pd4yb77wndh94y")))

(define-public crate-leaky-bucket-lite-0.2.1 (c (n "leaky-bucket-lite") (v "0.2.1") (d (list (d (n "tokio") (r "^1") (f (quote ("time" "sync"))) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread" "macros"))) (k 2)))) (h "0sgsnv3gy86z71xpzy6h3bi7i047q8vr0cva2hpnv6yrka22dh8j")))

(define-public crate-leaky-bucket-lite-0.2.2 (c (n "leaky-bucket-lite") (v "0.2.2") (d (list (d (n "tokio") (r "^1") (f (quote ("time" "sync"))) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread" "macros"))) (k 2)))) (h "05mdgcsiasggd416w028l6v2daih1pay8vr7mp4qsli8i5191g2c")))

(define-public crate-leaky-bucket-lite-0.3.0 (c (n "leaky-bucket-lite") (v "0.3.0") (d (list (d (n "tokio") (r "^1") (f (quote ("time" "sync"))) (o #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread" "macros"))) (k 2)))) (h "0vlzmnlrywana6w9i3c33j4vrb09s9xrdl81a9wzpb7biwpbbxg9") (f (quote (("sync") ("default" "tokio"))))))

(define-public crate-leaky-bucket-lite-0.3.1 (c (n "leaky-bucket-lite") (v "0.3.1") (d (list (d (n "parking_lot") (r "^0.11") (o #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("time" "sync"))) (o #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread" "macros"))) (k 2)))) (h "0x9mhxh5dsc0dv7scj1dp42g7wwhbb16wx4m0bc72rppsdc1djlr") (f (quote (("sync-threadsafe") ("sync") ("parking-lot" "parking_lot") ("default" "tokio"))))))

(define-public crate-leaky-bucket-lite-0.3.2 (c (n "leaky-bucket-lite") (v "0.3.2") (d (list (d (n "parking_lot") (r "^0.11") (o #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("time" "sync"))) (o #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread" "macros"))) (k 2)))) (h "04kxqwxlvka0k1mby88ca2x0kp5hvgv7qp1ybjxbf7zawn0dsn35") (f (quote (("sync-threadsafe") ("sync") ("parking-lot" "parking_lot") ("default" "tokio"))))))

(define-public crate-leaky-bucket-lite-0.4.0 (c (n "leaky-bucket-lite") (v "0.4.0") (d (list (d (n "parking_lot") (r "^0.11") (o #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("time" "sync"))) (o #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread" "macros"))) (k 2)))) (h "0rcavcn0p3nciy36y0pf8y4m41p8lhgg46g72g50y6dh2lkplhi7") (f (quote (("sync-threadsafe") ("sync") ("default" "tokio"))))))

(define-public crate-leaky-bucket-lite-0.5.0 (c (n "leaky-bucket-lite") (v "0.5.0") (d (list (d (n "parking_lot") (r "^0.11") (o #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("time" "sync"))) (o #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread" "macros"))) (k 2)))) (h "1gfzjzpapi4a7clfkvkddva07jagjls1vw60h3k4qj8004qxhf3f") (f (quote (("sync-threadsafe") ("sync") ("default" "tokio"))))))

(define-public crate-leaky-bucket-lite-0.5.1 (c (n "leaky-bucket-lite") (v "0.5.1") (d (list (d (n "parking_lot") (r "^0.11") (o #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("time" "sync"))) (o #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread" "macros"))) (k 2)))) (h "0w97fpjn71dmx0aa6x5fdcna5nkplyzjn2zybdw3px5cby6rhx2p") (f (quote (("sync-threadsafe") ("sync") ("default" "tokio"))))))

(define-public crate-leaky-bucket-lite-0.5.2 (c (n "leaky-bucket-lite") (v "0.5.2") (d (list (d (n "parking_lot") (r "^0.12") (o #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("time" "sync"))) (o #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread" "macros"))) (k 2)))) (h "0aajpyc2qjc6v5nxz13j4z62s2wjgx5z36mj9824i9r1vlvwf48l") (f (quote (("sync-threadsafe") ("sync") ("default" "tokio"))))))

