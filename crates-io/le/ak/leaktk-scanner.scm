(define-module (crates-io le ak leaktk-scanner) #:use-module (crates-io))

(define-public crate-leaktk-scanner-0.0.0 (c (n "leaktk-scanner") (v "0.0.0") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "log") (r "^0.4.17") (f (quote ("std" "serde"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "ring") (r "^0.16") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "time") (r "^0.3.17") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "uuid") (r "^1.2.2") (f (quote ("v4" "fast-rng"))) (d #t) (k 0)))) (h "1dadisf3j2n4qzndkfdm5wdkdaz7k2nd8jbn6587nqwpcn8pan5p")))

