(define-module (crates-io le ak leak-detect-allocator) #:use-module (crates-io))

(define-public crate-leak-detect-allocator-0.1.0 (c (n "leak-detect-allocator") (v "0.1.0") (d (list (d (n "backtrace") (r "^0.3.46") (f (quote ("libunwind" "libbacktrace" "dladdr" "dbghelp"))) (k 0)) (d (n "heapless") (r "^0.5.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.3.1") (d #t) (k 0)) (d (n "spin") (r "^0.5.2") (d #t) (k 0)))) (h "1nmmh5w2xf624mv80xqwp21pldrpyvcpydlpbdvn7fg0rfm7ic6h")))

(define-public crate-leak-detect-allocator-0.1.2 (c (n "leak-detect-allocator") (v "0.1.2") (d (list (d (n "backtrace") (r "^0.3.46") (f (quote ("libunwind" "libbacktrace" "dladdr" "dbghelp"))) (k 0)) (d (n "heapless") (r "^0.5.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.3.1") (d #t) (k 0)) (d (n "spin") (r "^0.5.2") (d #t) (k 0)))) (h "1f2a0zyzakp1pgprh9d94nxzkc1wkp41ngl9qzq4svs7gznjbavx")))

(define-public crate-leak-detect-allocator-0.1.3 (c (n "leak-detect-allocator") (v "0.1.3") (d (list (d (n "backtrace") (r "^0.3.46") (f (quote ("libunwind" "libbacktrace" "dladdr" "dbghelp"))) (k 0)) (d (n "heapless") (r "^0.5.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.3.1") (d #t) (k 0)) (d (n "spin") (r "^0.5.2") (d #t) (k 0)))) (h "064k3zb4mpqizyd9ss3zdkvlqm75sm28465pn97mc8kqhdc9ksc3")))

