(define-module (crates-io le ak leaky-cow) #:use-module (crates-io))

(define-public crate-leaky-cow-0.1.0 (c (n "leaky-cow") (v "0.1.0") (d (list (d (n "leak") (r "^0.1.1") (d #t) (k 0)))) (h "1g6cqp9jn8v4cyfhqv5xwpkmvgax812gknp4zmic787kadllh6xr")))

(define-public crate-leaky-cow-0.1.1 (c (n "leaky-cow") (v "0.1.1") (d (list (d (n "leak") (r "^0.1.1") (d #t) (k 0)))) (h "1z16b4gyfm7gydf4vpm7x6kwiiszcfx0ca5gm0jd67r48ifj5a20")))

