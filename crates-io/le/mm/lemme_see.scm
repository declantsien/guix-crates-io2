(define-module (crates-io le mm lemme_see) #:use-module (crates-io))

(define-public crate-lemme_see-0.1.0 (c (n "lemme_see") (v "0.1.0") (d (list (d (n "dns-lookup") (r "^1.0.8") (d #t) (k 0)))) (h "1qf7ahj1akhflwswz2lc7vnl6vdsbxb488hhjn64ha6hvv0d4c4k") (y #t)))

(define-public crate-lemme_see-0.1.10 (c (n "lemme_see") (v "0.1.10") (d (list (d (n "dns-lookup") (r "^1.0.8") (d #t) (k 0)))) (h "122qg4hqmfn1jvm2sjvf0c9vszphp8wpivcc8kpd3sisql2a1fyb") (y #t)))

(define-public crate-lemme_see-0.1.11 (c (n "lemme_see") (v "0.1.11") (d (list (d (n "dns-lookup") (r "^1.0.8") (d #t) (k 0)))) (h "1d4fwwf2dm0sigjqfdj5v6609c4fph4d60k4771jpj2irprpbhms") (y #t)))

(define-public crate-lemme_see-0.1.12 (c (n "lemme_see") (v "0.1.12") (d (list (d (n "dns-lookup") (r "^1.0.8") (d #t) (k 0)))) (h "12j3897wggnawb1n5ijyqijsr2y124vs2lcaqxsp1qyax72gyf57")))

(define-public crate-lemme_see-0.1.2 (c (n "lemme_see") (v "0.1.2") (d (list (d (n "dns-lookup") (r "^1.0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.133") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.74") (d #t) (k 0)) (d (n "tokio") (r "^1.15.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-test") (r "^0.4.2") (d #t) (k 0)))) (h "0az8xj67ywr6wd8a631pwq5am6v4gbm1gf9mlryajdkm2zjfk47s") (y #t)))

