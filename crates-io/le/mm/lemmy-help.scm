(define-module (crates-io le mm lemmy-help) #:use-module (crates-io))

(define-public crate-lemmy-help-0.0.0 (c (n "lemmy-help") (v "0.0.0") (d (list (d (n "chumsky") (r "^0.8.0") (k 0)) (d (n "tabled") (r "^0.6.1") (k 0)))) (h "1pmxglsdrv2vr96yh9dymd6vafl670xw2ydassk5wx9ifklhm16m")))

(define-public crate-lemmy-help-0.0.1 (c (n "lemmy-help") (v "0.0.1") (d (list (d (n "chumsky") (r "^0.8.0") (k 0)) (d (n "clap") (r "^3.1.10") (f (quote ("std" "derive" "cargo"))) (o #t) (k 0)) (d (n "tabled") (r "^0.6.1") (k 0)))) (h "1zrzwblwpjnbingwvg6r3hf2872vgj1x8a4c3igvkz115vaq45vs") (s 2) (e (quote (("cli" "dep:clap")))) (r "1.60.0")))

(define-public crate-lemmy-help-0.0.2 (c (n "lemmy-help") (v "0.0.2") (d (list (d (n "chumsky") (r "^0.8.0") (k 0)) (d (n "clap") (r "^3.1.10") (f (quote ("std" "derive" "cargo"))) (o #t) (k 0)) (d (n "tabled") (r "^0.6.1") (k 0)))) (h "053mjxwinn1lxsicllfdxp1y2hz3zqd9nnv44bk2klan6bhnyj0p") (s 2) (e (quote (("cli" "dep:clap")))) (r "1.60.0")))

(define-public crate-lemmy-help-0.0.3 (c (n "lemmy-help") (v "0.0.3") (d (list (d (n "chumsky") (r "^0.8.0") (k 0)) (d (n "lexopt") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "tabular") (r "^0.2.0") (k 0)) (d (n "textwrap") (r "^0.15.0") (k 0)))) (h "0jbd19dcfn94imz9i59afxd6z48rgmxkak37b946ri5x2j8kzyr9") (s 2) (e (quote (("cli" "dep:lexopt")))) (r "1.60.0")))

(define-public crate-lemmy-help-0.0.4 (c (n "lemmy-help") (v "0.0.4") (d (list (d (n "chumsky") (r "^0.8.0") (k 0)) (d (n "lexopt") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "tabular") (r "^0.2.0") (k 0)) (d (n "textwrap") (r "^0.15.0") (k 0)))) (h "0yhnckd3syh2dn8yhkpjjk4x9y82gq4q14vv1iqlii7h0dqj0wf9") (s 2) (e (quote (("cli" "dep:lexopt")))) (r "1.60.0")))

(define-public crate-lemmy-help-0.5.0 (c (n "lemmy-help") (v "0.5.0") (d (list (d (n "chumsky") (r "^0.8.0") (k 0)) (d (n "comfy-table") (r "^6.0.0") (k 0)) (d (n "lexopt") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "textwrap") (r "^0.15.0") (k 0)))) (h "19hrpbhpjm4mkjzmx5n9szgcp85y97j5gf2jkd42y2jdmwznl9vl") (s 2) (e (quote (("cli" "dep:lexopt")))) (r "1.60.0")))

(define-public crate-lemmy-help-0.6.0 (c (n "lemmy-help") (v "0.6.0") (d (list (d (n "chumsky") (r "^0.8.0") (k 0)) (d (n "comfy-table") (r "^6.0.0") (k 0)) (d (n "lexopt") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "textwrap") (r "^0.15.0") (k 0)))) (h "0yndsfmh2r3pdf67kqq0kxya1xs9ppq4n4z9ba62z95yh5b6s6ip") (s 2) (e (quote (("cli" "dep:lexopt")))) (r "1.63.0")))

(define-public crate-lemmy-help-0.7.0 (c (n "lemmy-help") (v "0.7.0") (d (list (d (n "chumsky") (r "^0.8.0") (k 0)) (d (n "comfy-table") (r "^6.1.0") (o #t) (k 0)) (d (n "lexopt") (r "^0.2.1") (o #t) (k 0)) (d (n "textwrap") (r "^0.15.1") (o #t) (k 0)))) (h "18ag92fv4v2pyn3lppv6z8gfcs3vsrm5ciwmj3a7pwhn685cmm5z") (s 2) (e (quote (("vimdoc" "dep:textwrap" "dep:comfy-table") ("cli" "vimdoc" "dep:lexopt")))) (r "1.63.0")))

(define-public crate-lemmy-help-0.8.0 (c (n "lemmy-help") (v "0.8.0") (d (list (d (n "chumsky") (r "^0.8.0") (k 0)) (d (n "comfy-table") (r "^6.1.0") (o #t) (k 0)) (d (n "lexopt") (r "^0.2.1") (o #t) (k 0)) (d (n "textwrap") (r "^0.15.1") (o #t) (k 0)))) (h "0yin8jfpl3d0ir3hhc7i92qy7cx3ylxwyfrrj5qv167zl69fw233") (s 2) (e (quote (("vimdoc" "dep:textwrap" "dep:comfy-table") ("cli" "vimdoc" "dep:lexopt")))) (r "1.63.0")))

(define-public crate-lemmy-help-0.8.1 (c (n "lemmy-help") (v "0.8.1") (d (list (d (n "chumsky") (r "^0.8.0") (k 0)) (d (n "comfy-table") (r "^6.1.0") (o #t) (k 0)) (d (n "lexopt") (r "^0.2.1") (o #t) (k 0)) (d (n "textwrap") (r "^0.15.1") (o #t) (k 0)))) (h "0i2d03xwr0cp2csryivryh74qmnk47ax0g52d8p41nlc0rwg998r") (s 2) (e (quote (("vimdoc" "dep:textwrap" "dep:comfy-table") ("cli" "vimdoc" "dep:lexopt")))) (r "1.63.0")))

(define-public crate-lemmy-help-0.9.0 (c (n "lemmy-help") (v "0.9.0") (d (list (d (n "chumsky") (r "^0.8.0") (k 0)) (d (n "comfy-table") (r "^6.1.0") (o #t) (k 0)) (d (n "lexopt") (r "^0.2.1") (o #t) (k 0)) (d (n "textwrap") (r "^0.15.1") (o #t) (k 0)))) (h "0p6rqpx63ynn25fpf2b5y07lyrh4c84f0g252ivqvjf7svizw5bj") (s 2) (e (quote (("vimdoc" "dep:textwrap" "dep:comfy-table") ("cli" "vimdoc" "dep:lexopt")))) (r "1.63.0")))

(define-public crate-lemmy-help-0.10.0 (c (n "lemmy-help") (v "0.10.0") (d (list (d (n "chumsky") (r "^0.8.0") (k 0)) (d (n "comfy-table") (r "^6.1.2") (o #t) (k 0)) (d (n "lexopt") (r "^0.2.1") (o #t) (k 0)) (d (n "textwrap") (r "^0.16.0") (o #t) (k 0)))) (h "1zbwxsvrf98jm6vcmk6i7267bwjhhkqfky3bjhfif91n51spa6ap") (s 2) (e (quote (("vimdoc" "dep:textwrap" "dep:comfy-table") ("cli" "vimdoc" "dep:lexopt")))) (r "1.65.0")))

(define-public crate-lemmy-help-0.11.0 (c (n "lemmy-help") (v "0.11.0") (d (list (d (n "chumsky") (r "^0.8.0") (k 0)) (d (n "comfy-table") (r "^6.1.2") (o #t) (k 0)) (d (n "lexopt") (r "^0.2.1") (o #t) (k 0)) (d (n "textwrap") (r "^0.16.0") (o #t) (k 0)))) (h "08i79w34jrhly5lv73w4jz5s7h3ypf3c41s1abqpiqw91w6xynz2") (s 2) (e (quote (("vimdoc" "dep:textwrap" "dep:comfy-table") ("cli" "vimdoc" "dep:lexopt")))) (r "1.65.0")))

