(define-module (crates-io le mm lemmy_apub_lib_derive) #:use-module (crates-io))

(define-public crate-lemmy_apub_lib_derive-0.11.3-rc.5 (c (n "lemmy_apub_lib_derive") (v "0.11.3-rc.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (f (quote ("diff"))) (d #t) (k 2)))) (h "1bnxvavvhkfgh91jdarcrwbxbwb1gcrh4df8ssqayy2pdnb68mhc")))

(define-public crate-lemmy_apub_lib_derive-0.11.4-alpha.0 (c (n "lemmy_apub_lib_derive") (v "0.11.4-alpha.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (f (quote ("diff"))) (d #t) (k 2)))) (h "0nwnim5wrfcc8hb4dsxinxs01bbrj00q0yhgmqr026zpr1b60mwn")))

(define-public crate-lemmy_apub_lib_derive-0.11.4-rc.28 (c (n "lemmy_apub_lib_derive") (v "0.11.4-rc.28") (d (list (d (n "proc-macro2") (r "^1.0.28") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.74") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.45") (f (quote ("diff"))) (d #t) (k 2)))) (h "019vr05b0401r8f3w0csc5ykvflj1my7yv3bpc7lj399adrf22w2")))

(define-public crate-lemmy_apub_lib_derive-0.12.0-rc.1 (c (n "lemmy_apub_lib_derive") (v "0.12.0-rc.1") (d (list (d (n "proc-macro2") (r "^1.0.28") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.74") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.45") (f (quote ("diff"))) (d #t) (k 2)))) (h "1js17z3n6kqqbrn2wrrrz786sw2qp700bz347vdygm6lvk2481yv")))

(define-public crate-lemmy_apub_lib_derive-0.12.0-rc.2 (c (n "lemmy_apub_lib_derive") (v "0.12.0-rc.2") (d (list (d (n "proc-macro2") (r "^1.0.28") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.75") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.45") (f (quote ("diff"))) (d #t) (k 2)))) (h "0h1cc8zkf00k16kmf63sksy7z53qaqzs202dmgndmwk8p1s3wwy8")))

(define-public crate-lemmy_apub_lib_derive-0.12.0 (c (n "lemmy_apub_lib_derive") (v "0.12.0") (d (list (d (n "proc-macro2") (r "^1.0.28") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.75") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.45") (f (quote ("diff"))) (d #t) (k 2)))) (h "11m6q2mqn2s5cabxw5xgrzdjhxz0mdw4h1vadklj6crcplh2szbq")))

(define-public crate-lemmy_apub_lib_derive-0.12.1-rc.1 (c (n "lemmy_apub_lib_derive") (v "0.12.1-rc.1") (d (list (d (n "proc-macro2") (r "^1.0.28") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.75") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.45") (f (quote ("diff"))) (d #t) (k 2)))) (h "020mazjf0f6i6krnkbb836635pf7jpmf5xl2gljf4py4wn1awmnv")))

(define-public crate-lemmy_apub_lib_derive-0.12.1 (c (n "lemmy_apub_lib_derive") (v "0.12.1") (d (list (d (n "proc-macro2") (r "^1.0.28") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.75") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.45") (f (quote ("diff"))) (d #t) (k 2)))) (h "0prn2ljmy0q6mhjnx7bq6cwhkhn1d41jnbh39ln7cv87sy62qigi")))

(define-public crate-lemmy_apub_lib_derive-0.12.2-rc.1 (c (n "lemmy_apub_lib_derive") (v "0.12.2-rc.1") (d (list (d (n "proc-macro2") (r "^1.0.28") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.75") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.45") (f (quote ("diff"))) (d #t) (k 2)))) (h "0n6qhfc56vahgq592wbms7dqh0b3b2abii4yihk0pfr841fyc8a6")))

(define-public crate-lemmy_apub_lib_derive-0.12.2 (c (n "lemmy_apub_lib_derive") (v "0.12.2") (d (list (d (n "proc-macro2") (r "^1.0.28") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.75") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.45") (f (quote ("diff"))) (d #t) (k 2)))) (h "16svmf0wg65bdzas4sc04w86c6qkwcb5v11zbhl80wm2sqj1gm0d")))

(define-public crate-lemmy_apub_lib_derive-0.13.0-rc.1 (c (n "lemmy_apub_lib_derive") (v "0.13.0-rc.1") (d (list (d (n "proc-macro2") (r "^1.0.28") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.75") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.45") (f (quote ("diff"))) (d #t) (k 2)))) (h "1g29awr5l29lv2hc12kj8392pf16jwwzhndiqc3wcsrjb3gc9d0j")))

(define-public crate-lemmy_apub_lib_derive-0.13.0 (c (n "lemmy_apub_lib_derive") (v "0.13.0") (d (list (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.45") (f (quote ("diff"))) (d #t) (k 2)))) (h "1pzhpq3yglq6nnarg6nqjy9xbrcggvqbsqmlilpgm9xy1j47iyz4")))

(define-public crate-lemmy_apub_lib_derive-0.13.1 (c (n "lemmy_apub_lib_derive") (v "0.13.1") (d (list (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.45") (f (quote ("diff"))) (d #t) (k 2)))) (h "0whd0j53a3v7jrl542kfdp0bcr5lp0m6v8mkhsb1cg4inl6p87w9")))

(define-public crate-lemmy_apub_lib_derive-0.13.3 (c (n "lemmy_apub_lib_derive") (v "0.13.3") (d (list (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.45") (f (quote ("diff"))) (d #t) (k 2)))) (h "1a53dc1skxbnwm3w8amg9hnd8d40x0yib33zhq3pxaznf3jxsxmr")))

(define-public crate-lemmy_apub_lib_derive-0.13.5-rc.6 (c (n "lemmy_apub_lib_derive") (v "0.13.5-rc.6") (d (list (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.45") (f (quote ("diff"))) (d #t) (k 2)))) (h "0whfy3f32das2cjmkchl40l23a7kq8mgg3c48i990qcvi50l7b0m")))

(define-public crate-lemmy_apub_lib_derive-0.13.5-rc.7 (c (n "lemmy_apub_lib_derive") (v "0.13.5-rc.7") (d (list (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.45") (f (quote ("diff"))) (d #t) (k 2)))) (h "1xb5g29y19xylfabp31229l3nnimq2sph1bxnsdf7p6h8zz086n5")))

(define-public crate-lemmy_apub_lib_derive-0.13.5 (c (n "lemmy_apub_lib_derive") (v "0.13.5") (d (list (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.45") (f (quote ("diff"))) (d #t) (k 2)))) (h "0z0rrqwkckv8miicdcqwmm369z1wj79skfxr4vdwk53axvgmi1cc")))

(define-public crate-lemmy_apub_lib_derive-0.14.0-rc.1 (c (n "lemmy_apub_lib_derive") (v "0.14.0-rc.1") (d (list (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.45") (f (quote ("diff"))) (d #t) (k 2)))) (h "03vxwwlj440kkp360jaagwdwl16yhy5aynj5w89j66ip7vw2p79v")))

(define-public crate-lemmy_apub_lib_derive-0.14.0-rc.2 (c (n "lemmy_apub_lib_derive") (v "0.14.0-rc.2") (d (list (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.45") (f (quote ("diff"))) (d #t) (k 2)))) (h "1gx372hdlzv5qvszrfj1wdb14mkjp3z3dii2wrmkl59xkd1xj2nd")))

(define-public crate-lemmy_apub_lib_derive-0.14.0 (c (n "lemmy_apub_lib_derive") (v "0.14.0") (d (list (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.45") (f (quote ("diff"))) (d #t) (k 2)))) (h "0dl57dz0ykp5pgp66zq14q796fvcn9bbxgp7sg0spq7n306bjqb0")))

(define-public crate-lemmy_apub_lib_derive-0.14.1 (c (n "lemmy_apub_lib_derive") (v "0.14.1") (d (list (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.45") (f (quote ("diff"))) (d #t) (k 2)))) (h "19v4mf92azvgkqs5nwj8jbrm3599xpd49s595albkxsb4xpl25qs")))

(define-public crate-lemmy_apub_lib_derive-0.14.2-rc.1 (c (n "lemmy_apub_lib_derive") (v "0.14.2-rc.1") (d (list (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.45") (f (quote ("diff"))) (d #t) (k 2)))) (h "09jji7xn24zycyy029nxmw92x732gpd6yi7z7qid269vc725ax0k")))

(define-public crate-lemmy_apub_lib_derive-0.14.2 (c (n "lemmy_apub_lib_derive") (v "0.14.2") (d (list (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.45") (f (quote ("diff"))) (d #t) (k 2)))) (h "0zcsph4mpayq47p5qvjyfwmfbva1smnz69368dhz7mmsz3wk960i")))

(define-public crate-lemmy_apub_lib_derive-0.14.3 (c (n "lemmy_apub_lib_derive") (v "0.14.3") (d (list (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.45") (f (quote ("diff"))) (d #t) (k 2)))) (h "1zsm3f3lsjmcynh15a8nzz7gg5w4qjv05digpwflymzljapr1k1f")))

(define-public crate-lemmy_apub_lib_derive-0.14.4-rc.3 (c (n "lemmy_apub_lib_derive") (v "0.14.4-rc.3") (d (list (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.45") (f (quote ("diff"))) (d #t) (k 2)))) (h "02ckl6a774kb14whl3i9p3fir6dawqcc26xj5cq4ag20s7g5dwxn")))

(define-public crate-lemmy_apub_lib_derive-0.14.4-rc.4 (c (n "lemmy_apub_lib_derive") (v "0.14.4-rc.4") (d (list (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.45") (f (quote ("diff"))) (d #t) (k 2)))) (h "1qipyb8hqyyfdnf6q0caw63spsdqqib74gbshyj3pb901bzqx9rr")))

(define-public crate-lemmy_apub_lib_derive-0.14.5-rc.1 (c (n "lemmy_apub_lib_derive") (v "0.14.5-rc.1") (d (list (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.45") (f (quote ("diff"))) (d #t) (k 2)))) (h "09jivm1w19b0r9ca22mvnyl2x16k75hp1jbvrv48cg5qkrysbzh7")))

(define-public crate-lemmy_apub_lib_derive-0.14.5-rc.2 (c (n "lemmy_apub_lib_derive") (v "0.14.5-rc.2") (d (list (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.45") (f (quote ("diff"))) (d #t) (k 2)))) (h "0v9fgsjvrrl1jf7m2xjhmak0zajlbjgp39hnqasyc3qw5g021nq7")))

(define-public crate-lemmy_apub_lib_derive-0.14.5 (c (n "lemmy_apub_lib_derive") (v "0.14.5") (d (list (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.45") (f (quote ("diff"))) (d #t) (k 2)))) (h "0lhzml5wvshnp0nq6jcy4iy4vd52k7vz0c8akpqa1b9rcy029530")))

(define-public crate-lemmy_apub_lib_derive-0.15.0-rc.3 (c (n "lemmy_apub_lib_derive") (v "0.15.0-rc.3") (d (list (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.45") (f (quote ("diff"))) (d #t) (k 2)))) (h "07n1d4gwmkf53ndsangn0n5j8lvnwwpljfc1px7z21h7jvzkycir")))

(define-public crate-lemmy_apub_lib_derive-0.15.0-rc.4 (c (n "lemmy_apub_lib_derive") (v "0.15.0-rc.4") (d (list (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.45") (f (quote ("diff"))) (d #t) (k 2)))) (h "12f02hvbvhmsp0m07gcm2274b3jqz4jdja48n5by4vyhnxwznm1i")))

(define-public crate-lemmy_apub_lib_derive-0.14.6-rc.1 (c (n "lemmy_apub_lib_derive") (v "0.14.6-rc.1") (d (list (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.45") (f (quote ("diff"))) (d #t) (k 2)))) (h "0wnb9bjdzfw462349m5cx1k4ijff2jxdwkg0ckascxvdlcjqkfnc")))

(define-public crate-lemmy_apub_lib_derive-0.15.0-rc.6 (c (n "lemmy_apub_lib_derive") (v "0.15.0-rc.6") (d (list (d (n "proc-macro2") (r "^1.0.33") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.82") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.53") (f (quote ("diff"))) (d #t) (k 2)))) (h "1ydx2a4w2ng62xbbz2q5m98pi0zk8n9v6acm8yci4iknr9fp9as0")))

(define-public crate-lemmy_apub_lib_derive-0.14.6-rc.2 (c (n "lemmy_apub_lib_derive") (v "0.14.6-rc.2") (d (list (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.45") (f (quote ("diff"))) (d #t) (k 2)))) (h "15fghbvdj92g0847n5kawzliplrcnyjdxvkdy6p6l39817qhjlfy")))

(define-public crate-lemmy_apub_lib_derive-0.15.0-rc.7 (c (n "lemmy_apub_lib_derive") (v "0.15.0-rc.7") (d (list (d (n "proc-macro2") (r "^1.0.33") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.82") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.53") (f (quote ("diff"))) (d #t) (k 2)))) (h "1s3w3zsw0cl5l31sqyrxir202j38hnxsadf25jrlkvafjw5v06ln")))

(define-public crate-lemmy_apub_lib_derive-0.15.0 (c (n "lemmy_apub_lib_derive") (v "0.15.0") (d (list (d (n "proc-macro2") (r "^1.0.33") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.82") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.53") (f (quote ("diff"))) (d #t) (k 2)))) (h "15gqyx835mk9fl18grx8man016h555zvfdnv3a0rqwvlf1xnvpnk")))

(define-public crate-lemmy_apub_lib_derive-0.15.1 (c (n "lemmy_apub_lib_derive") (v "0.15.1") (d (list (d (n "proc-macro2") (r "^1.0.33") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.82") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.53") (f (quote ("diff"))) (d #t) (k 2)))) (h "1blbldvjprifr8yy7h4r61bf27sf8zb1nhgrb8wgdc98fws0cvnf")))

(define-public crate-lemmy_apub_lib_derive-0.15.2-rc.1 (c (n "lemmy_apub_lib_derive") (v "0.15.2-rc.1") (d (list (d (n "proc-macro2") (r "^1.0.33") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.82") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.53") (f (quote ("diff"))) (d #t) (k 2)))) (h "10kgvh90cmgl4h3wb89ckiicgsc3vzaaz56vkvw9b0rgqzj2fggh")))

(define-public crate-lemmy_apub_lib_derive-0.15.2 (c (n "lemmy_apub_lib_derive") (v "0.15.2") (d (list (d (n "proc-macro2") (r "^1.0.33") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.82") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.53") (f (quote ("diff"))) (d #t) (k 2)))) (h "1pyjxb8gap6nzm41q71bmkajj698a95sr3z5hgiwf4slwh639wnz")))

(define-public crate-lemmy_apub_lib_derive-0.15.4-rc.1 (c (n "lemmy_apub_lib_derive") (v "0.15.4-rc.1") (d (list (d (n "proc-macro2") (r "^1.0.33") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.82") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.53") (f (quote ("diff"))) (d #t) (k 2)))) (h "0l0x4mhrchwj5c44684s5a50ifb098hx2y6iwy395i1sdsgb9h08")))

(define-public crate-lemmy_apub_lib_derive-0.15.4 (c (n "lemmy_apub_lib_derive") (v "0.15.4") (d (list (d (n "proc-macro2") (r "^1.0.33") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.82") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.53") (f (quote ("diff"))) (d #t) (k 2)))) (h "0bd9kfcvsr5xd3xs1sxvbqpc95x9rshwl51vpf7fq66zy0hdmig0")))

(define-public crate-lemmy_apub_lib_derive-0.16.0-rc.1 (c (n "lemmy_apub_lib_derive") (v "0.16.0-rc.1") (d (list (d (n "proc-macro2") (r "^1.0.33") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.82") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.53") (f (quote ("diff"))) (d #t) (k 2)))) (h "16pi0nrjknr2j2mf28xnykcdy6p73l68q9811q37pvpq6cci7cb6")))

(define-public crate-lemmy_apub_lib_derive-0.16.0-rc.2 (c (n "lemmy_apub_lib_derive") (v "0.16.0-rc.2") (d (list (d (n "proc-macro2") (r "^1.0.33") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.82") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.53") (f (quote ("diff"))) (d #t) (k 2)))) (h "0vy97dbz1k8sa61hpxc346347zgppf9rd9d4x1bbsk4s7p20wsfr")))

(define-public crate-lemmy_apub_lib_derive-0.16.0-rc.3 (c (n "lemmy_apub_lib_derive") (v "0.16.0-rc.3") (d (list (d (n "proc-macro2") (r "^1.0.33") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.82") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.53") (f (quote ("diff"))) (d #t) (k 2)))) (h "063v4hqs1bkrvrpg0c1rcsh9wabkflsmg225kja500yfhvl37v42")))

(define-public crate-lemmy_apub_lib_derive-0.16.0-rc.4 (c (n "lemmy_apub_lib_derive") (v "0.16.0-rc.4") (d (list (d (n "proc-macro2") (r "^1.0.33") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.82") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.53") (f (quote ("diff"))) (d #t) (k 2)))) (h "0v7i4f55yg3m0ib548yabx47bjn88mibf7z48xj8kv2h40gvjp22")))

(define-public crate-lemmy_apub_lib_derive-0.16.0 (c (n "lemmy_apub_lib_derive") (v "0.16.0") (d (list (d (n "proc-macro2") (r "^1.0.33") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.82") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.53") (f (quote ("diff"))) (d #t) (k 2)))) (h "18m6fl783psljfq57qvvrl6qpn76i71xb2vww7m4zls74pam0dzq")))

(define-public crate-lemmy_apub_lib_derive-0.16.1-rc.1 (c (n "lemmy_apub_lib_derive") (v "0.16.1-rc.1") (d (list (d (n "proc-macro2") (r "^1.0.33") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.82") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.53") (f (quote ("diff"))) (d #t) (k 2)))) (h "10y79xqpcscpq4pmv6ix7i2smnfbw3bg0xwxy6s8ab3gfy081z74")))

(define-public crate-lemmy_apub_lib_derive-0.16.1 (c (n "lemmy_apub_lib_derive") (v "0.16.1") (d (list (d (n "proc-macro2") (r "^1.0.33") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.82") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.53") (f (quote ("diff"))) (d #t) (k 2)))) (h "1sc826s01rchs46chmlab5ib9f9plchcb1pvphfx51h2xyxhrfj8")))

(define-public crate-lemmy_apub_lib_derive-0.16.2-rc.1 (c (n "lemmy_apub_lib_derive") (v "0.16.2-rc.1") (d (list (d (n "proc-macro2") (r "^1.0.33") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.82") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.53") (f (quote ("diff"))) (d #t) (k 2)))) (h "0rxd35fa5x39gn5wrx7vc7cxv6973qh6ln77s43zqwq3c3xvd7ra")))

(define-public crate-lemmy_apub_lib_derive-0.16.2-rc.3 (c (n "lemmy_apub_lib_derive") (v "0.16.2-rc.3") (d (list (d (n "proc-macro2") (r "^1.0.33") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.82") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.53") (f (quote ("diff"))) (d #t) (k 2)))) (h "1zcjy598ncs2wc0vp7bvchgbvn6z37ialfakab6bdq1lpfbh86s2")))

(define-public crate-lemmy_apub_lib_derive-0.16.2 (c (n "lemmy_apub_lib_derive") (v "0.16.2") (d (list (d (n "proc-macro2") (r "^1.0.33") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.82") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.53") (f (quote ("diff"))) (d #t) (k 2)))) (h "0cadrvzhsgj72v4alwyfzgqb1lhjqlf9w36lvw4nvyl69ifh7sbs")))

(define-public crate-lemmy_apub_lib_derive-0.16.3-rc.1 (c (n "lemmy_apub_lib_derive") (v "0.16.3-rc.1") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.17") (d #t) (k 0)) (d (n "syn") (r "^1.0.90") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.57") (f (quote ("diff"))) (d #t) (k 2)))) (h "03p2xnkbac1z94pdwgv8hr1pvvl9xrx29xrm1ds961gn2r7rkybd")))

(define-public crate-lemmy_apub_lib_derive-0.16.3 (c (n "lemmy_apub_lib_derive") (v "0.16.3") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.17") (d #t) (k 0)) (d (n "syn") (r "^1.0.90") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.57") (f (quote ("diff"))) (d #t) (k 2)))) (h "1vx87s4rmax7arxkp7mj00yp0m5z9qpfmckcvzz317i0k0gcq5h4")))

(define-public crate-lemmy_apub_lib_derive-0.16.4-rc.10 (c (n "lemmy_apub_lib_derive") (v "0.16.4-rc.10") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.17") (d #t) (k 0)) (d (n "syn") (r "^1.0.90") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.57") (f (quote ("diff"))) (d #t) (k 2)))) (h "0q423nh7319zll0fpaajrfn6lpq9znsd7xiknsxp9a1mgzi5fvs3")))

(define-public crate-lemmy_apub_lib_derive-0.16.4-rc.11 (c (n "lemmy_apub_lib_derive") (v "0.16.4-rc.11") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.17") (d #t) (k 0)) (d (n "syn") (r "^1.0.90") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.57") (f (quote ("diff"))) (d #t) (k 2)))) (h "0ff0a9knpk0wf583r7l8mwl9dl6q3am1xrv6yls2h4lsb9r029vr")))

(define-public crate-lemmy_apub_lib_derive-0.16.4 (c (n "lemmy_apub_lib_derive") (v "0.16.4") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.17") (d #t) (k 0)) (d (n "syn") (r "^1.0.90") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.57") (f (quote ("diff"))) (d #t) (k 2)))) (h "1cf5vxcvb4swjwvfmk7vbsh264qr47nqv9y5vlqkyq6n58nb2z5c")))

(define-public crate-lemmy_apub_lib_derive-0.16.6-rc.1 (c (n "lemmy_apub_lib_derive") (v "0.16.6-rc.1") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.17") (d #t) (k 0)) (d (n "syn") (r "^1.0.90") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.57") (f (quote ("diff"))) (d #t) (k 2)))) (h "13pbdf1h1hyqzd2s0797rap6hgpgij01q4wwkfrrlq3fyhpjlbdz")))

(define-public crate-lemmy_apub_lib_derive-0.16.6-rc.3 (c (n "lemmy_apub_lib_derive") (v "0.16.6-rc.3") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.17") (d #t) (k 0)) (d (n "syn") (r "^1.0.90") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.57") (f (quote ("diff"))) (d #t) (k 2)))) (h "113jzyisffkfciynrf69fdjx58kx70spq20mkrmy71izj7zkk92k")))

(define-public crate-lemmy_apub_lib_derive-0.16.6 (c (n "lemmy_apub_lib_derive") (v "0.16.6") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.17") (d #t) (k 0)) (d (n "syn") (r "^1.0.90") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.57") (f (quote ("diff"))) (d #t) (k 2)))) (h "0v486lx83n10xcq2mvpxb96bnsr1pjk503yp5azs4z0b011a34z7")))

(define-public crate-lemmy_apub_lib_derive-0.16.7-rc.2 (c (n "lemmy_apub_lib_derive") (v "0.16.7-rc.2") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.17") (d #t) (k 0)) (d (n "syn") (r "^1.0.90") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.57") (f (quote ("diff"))) (d #t) (k 2)))) (h "11sq6k59bwvl0bivg6h5jjq0d8q78fpis0nyxi2mvknwlbbq9my9")))

(define-public crate-lemmy_apub_lib_derive-0.16.7-rc.3 (c (n "lemmy_apub_lib_derive") (v "0.16.7-rc.3") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.17") (d #t) (k 0)) (d (n "syn") (r "^1.0.90") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.57") (f (quote ("diff"))) (d #t) (k 2)))) (h "04llkbw8x4r3w3hz90svdk53p9khxy3v287i20gcg7p303pyxzpb")))

(define-public crate-lemmy_apub_lib_derive-0.16.7 (c (n "lemmy_apub_lib_derive") (v "0.16.7") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.17") (d #t) (k 0)) (d (n "syn") (r "^1.0.90") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.57") (f (quote ("diff"))) (d #t) (k 2)))) (h "072g3fq0f18q5hq7kc9inca2xlih4v5ip448mbflaml7y0zlyp69")))

