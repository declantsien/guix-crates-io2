(define-module (crates-io le xo lexorank) #:use-module (crates-io))

(define-public crate-lexorank-0.1.0 (c (n "lexorank") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "substring") (r "^1.4") (d #t) (k 0)))) (h "16yjvvyn6hvb09mn9fnjb7znjsg74kmnngfq338g2x9rdcbplg01")))

(define-public crate-lexorank-2.0.0 (c (n "lexorank") (v "2.0.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "substring") (r "^1.4") (d #t) (k 0)))) (h "0ri1pjaz0bysq5lb2gx3ynw1g3fk712zwyrywrv160mmca0q8x63")))

