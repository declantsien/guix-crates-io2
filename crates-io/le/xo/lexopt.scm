(define-module (crates-io le xo lexopt) #:use-module (crates-io))

(define-public crate-lexopt-0.1.0 (c (n "lexopt") (v "0.1.0") (h "0lpm871glk0hzra2r70mfngfj5ahhm8bvrh2yqgk3jg88dc9npbc")))

(define-public crate-lexopt-0.2.0 (c (n "lexopt") (v "0.2.0") (h "0w1adb1qwp9rkznq37qvwh01ckxpwcywb6ki9mlw03filyp16xfy")))

(define-public crate-lexopt-0.2.1 (c (n "lexopt") (v "0.2.1") (h "1s7k6hra9p9xilhl852qi1a7c2gis59qf4yl1ca5pbxf5bkfk3j7")))

(define-public crate-lexopt-0.3.0 (c (n "lexopt") (v "0.3.0") (h "00dlvik2ygw8z101vf3bfndcvxhp92v25sbzz6bdiwvxgxhlpzxs")))

