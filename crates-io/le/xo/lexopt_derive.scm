(define-module (crates-io le xo lexopt_derive) #:use-module (crates-io))

(define-public crate-lexopt_derive-0.0.1-alpha (c (n "lexopt_derive") (v "0.0.1-alpha") (d (list (d (n "lexopt") (r "^0.3.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (f (quote ("derive" "parsing" "proc-macro"))) (k 0)))) (h "0vylzz9wx0h7mna8c1hi6dzhi87s7qv30nbhc7wnlgad7jl1fy3s")))

