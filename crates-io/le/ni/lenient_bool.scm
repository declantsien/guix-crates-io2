(define-module (crates-io le ni lenient_bool) #:use-module (crates-io))

(define-public crate-lenient_bool-0.1.0 (c (n "lenient_bool") (v "0.1.0") (h "1nzvk54ddya5j0mf8g8mg25bhck6icwlv66q8b5ijax1fq4nssp7")))

(define-public crate-lenient_bool-0.1.1 (c (n "lenient_bool") (v "0.1.1") (h "0kcmzfw2v2ybvv94b2axf3vysanj5rgby6f2a5q2fvg9cbaaxvjp")))

