(define-module (crates-io le ni lenient_semver_version_builder) #:use-module (crates-io))

(define-public crate-lenient_semver_version_builder-0.4.0 (c (n "lenient_semver_version_builder") (v "0.4.0") (d (list (d (n "semver") (r "^1") (o #t) (d #t) (k 0)) (d (n "semver") (r "^1") (d #t) (k 2)) (d (n "semver010") (r "^0.10") (o #t) (d #t) (k 0) (p "semver")) (d (n "semver011") (r "^0.11") (o #t) (d #t) (k 0) (p "semver")) (d (n "test-case") (r "^1") (d #t) (k 2)))) (h "0s7bqw11f4p36i6chhplf4m3yyizgnv834ldpy0l40mycgvw8psq") (f (quote (("default" "semver"))))))

(define-public crate-lenient_semver_version_builder-0.4.1 (c (n "lenient_semver_version_builder") (v "0.4.1") (d (list (d (n "semver") (r "^1") (o #t) (d #t) (k 0)) (d (n "semver") (r "^1") (d #t) (k 2)) (d (n "semver010") (r "^0.10") (o #t) (d #t) (k 0) (p "semver")) (d (n "semver011") (r "^0.11") (o #t) (d #t) (k 0) (p "semver")) (d (n "test-case") (r "^1") (d #t) (k 2)))) (h "0b7pn7nfg25xrsijssji5qn9cfgqdz5g6zvh7sayl13dcqkmyrsp") (f (quote (("default" "semver"))))))

(define-public crate-lenient_semver_version_builder-0.4.2 (c (n "lenient_semver_version_builder") (v "0.4.2") (d (list (d (n "semver") (r "^1") (o #t) (d #t) (k 0)) (d (n "semver") (r "^1") (d #t) (k 2)) (d (n "semver010") (r "^0.10") (o #t) (d #t) (k 0) (p "semver")) (d (n "semver011") (r "^0.11") (o #t) (d #t) (k 0) (p "semver")) (d (n "test-case") (r "^1") (d #t) (k 2)))) (h "05rd538cgbs6h99ddwibcj59qj9h0bklhwamjmpr8nzp97zzhjch") (f (quote (("default" "semver"))))))

