(define-module (crates-io le ni lenia_ca) #:use-module (crates-io))

(define-public crate-lenia_ca-0.1.0 (c (n "lenia_ca") (v "0.1.0") (d (list (d (n "ndarray") (r "^0.15.6") (f (quote ("rayon"))) (d #t) (k 0)) (d (n "num-complex") (r "^0.4.2") (d #t) (k 0)) (d (n "png") (r "^0.17.7") (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)) (d (n "rustfft") (r "^6.1.0") (d #t) (k 0)))) (h "1ggjh22h03apgb3fgpk3x10h463x620gbv5kd1s5njizb96wf19q")))

(define-public crate-lenia_ca-0.1.1 (c (n "lenia_ca") (v "0.1.1") (d (list (d (n "ndarray") (r "^0.15.6") (f (quote ("rayon"))) (d #t) (k 0)) (d (n "num-complex") (r "^0.4.2") (d #t) (k 0)) (d (n "png") (r "^0.17.7") (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)) (d (n "rustfft") (r "^6.1.0") (d #t) (k 0)))) (h "15ia6akckcz02hyhlryzfn6kl7h0yhp450qwbshyh8h1kx9yy3qb")))

