(define-module (crates-io le nd lender-derive) #:use-module (crates-io))

(define-public crate-lender-derive-0.1.0 (c (n "lender-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.43") (f (quote ("full"))) (d #t) (k 0)))) (h "069fx5lhbsxpqplcv0q8bkabrq5cdkg7m1blj9yn9bxiksm93dsc")))

(define-public crate-lender-derive-0.1.1 (c (n "lender-derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.43") (f (quote ("full"))) (d #t) (k 0)))) (h "1lfdxjp7wlb57rj7a4vabinb53b1q3j96hqss4alb0h3pjj500vm")))

(define-public crate-lender-derive-0.1.2 (c (n "lender-derive") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.43") (f (quote ("full"))) (d #t) (k 0)))) (h "09hxjc1bpzrs8vfzl06dblh8s1680kc4wswzbyd6c8nvk86ayvsm")))

