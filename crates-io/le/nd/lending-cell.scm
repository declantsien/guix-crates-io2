(define-module (crates-io le nd lending-cell) #:use-module (crates-io))

(define-public crate-lending-cell-0.0.1 (c (n "lending-cell") (v "0.0.1") (h "0rapmdifq1vswwbnxk05l677k78jhdvwsjzmiqcidnwak4rmp6in")))

(define-public crate-lending-cell-0.1.0 (c (n "lending-cell") (v "0.1.0") (h "02idp04nyrjjz2g4f8vinfrfg49jkr7dmw4jhykf41x1hy6k6nr8")))

(define-public crate-lending-cell-0.1.1 (c (n "lending-cell") (v "0.1.1") (h "13g8pdjj51181hcqv8wjajwvzx53jgd6x17i06gga4af10zrq90p")))

