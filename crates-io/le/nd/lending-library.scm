(define-module (crates-io le nd lending-library) #:use-module (crates-io))

(define-public crate-lending-library-0.1.0 (c (n "lending-library") (v "0.1.0") (h "1834azs6rc4yw4g4wbxmw5frm6p49h4vagrv6p712w5kkafd5mf8")))

(define-public crate-lending-library-0.1.1 (c (n "lending-library") (v "0.1.1") (h "1yj5rj70vsldbi7vq0277d5xv9g2zs900sscnc05rjkifijchd30")))

(define-public crate-lending-library-0.1.2 (c (n "lending-library") (v "0.1.2") (h "0kyz8bpgrr54507xvpxzxrlqb09sdai0hfkvpjwsr466zh8wnlq6")))

(define-public crate-lending-library-0.1.3 (c (n "lending-library") (v "0.1.3") (h "0vcr5nkkirgagrmbarygha2kyx7cxhfrgmgdwgm58x3irl5n8cr8")))

(define-public crate-lending-library-0.1.4 (c (n "lending-library") (v "0.1.4") (h "1q8b2z0n50i9x9qj0i9aycd7kkij0q68y5a9rg32kndq4abllaan")))

(define-public crate-lending-library-0.2.0 (c (n "lending-library") (v "0.2.0") (h "018z6s7lh032c97zmqhqd4hvqa116vvwlrz817fz2mhi0jr47kqv")))

