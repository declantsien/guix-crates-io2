(define-module (crates-io le nd lending-iterator-proc_macros) #:use-module (crates-io))

(define-public crate-lending-iterator-proc_macros-0.1.0-rc1 (c (n "lending-iterator-proc_macros") (v "0.1.0-rc1") (d (list (d (n "proc-macro2") (r "^1.0.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.7") (f (quote ("visit-mut"))) (d #t) (k 0)))) (h "0384h8g648y4ghkz2gh8gs4lin8s2yicd6jygjvk4y2slzb62433")))

(define-public crate-lending-iterator-proc_macros-0.1.0 (c (n "lending-iterator-proc_macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.7") (f (quote ("visit-mut"))) (d #t) (k 0)))) (h "05qljf759d2113n317yw633szkk0k851yh6gf9xm69yriwpf42wf")))

(define-public crate-lending-iterator-proc_macros-0.1.1-rc1 (c (n "lending-iterator-proc_macros") (v "0.1.1-rc1") (d (list (d (n "proc-macro2") (r "^1.0.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.7") (f (quote ("visit-mut"))) (d #t) (k 0)))) (h "1mxgzql85gdk2mzl0l8hfdkbd0h4l99z822diljd49c4jm8fmgsn")))

(define-public crate-lending-iterator-proc_macros-0.1.1 (c (n "lending-iterator-proc_macros") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.7") (f (quote ("visit-mut"))) (d #t) (k 0)))) (h "0kxg3xj5vjdfxjb2snhyyy8b1zwrb14mn6vmfmgcxd0byzny2nc4")))

(define-public crate-lending-iterator-proc_macros-0.1.2 (c (n "lending-iterator-proc_macros") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.7") (f (quote ("visit-mut"))) (d #t) (k 0)))) (h "1vzyfdbdmf1rx2f2abg3cyl0g1338v42hrfazlviqk30bl7w0ppb")))

(define-public crate-lending-iterator-proc_macros-0.1.3-rc1 (c (n "lending-iterator-proc_macros") (v "0.1.3-rc1") (d (list (d (n "proc-macro2") (r "^1.0.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.7") (f (quote ("visit-mut"))) (d #t) (k 0)))) (h "0yy61h2aksqbhib7wgcb517za0xwa4r4q0irhn8d8x3hpyjwi59b")))

(define-public crate-lending-iterator-proc_macros-0.1.3 (c (n "lending-iterator-proc_macros") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.7") (f (quote ("visit-mut"))) (d #t) (k 0)))) (h "09ff6v83w6w9d3bc9dx3lqb7wgfb531fkrassqy7i8xkisb8g4yj")))

(define-public crate-lending-iterator-proc_macros-0.1.4 (c (n "lending-iterator-proc_macros") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.7") (f (quote ("visit-mut"))) (d #t) (k 0)))) (h "1j19l53mrrx2v7ia93ir56r2mirfkysvaq2kw3amdjd9pk98bv87")))

(define-public crate-lending-iterator-proc_macros-0.1.5 (c (n "lending-iterator-proc_macros") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^1.0.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.7") (f (quote ("visit-mut"))) (d #t) (k 0)))) (h "03nhw8dcds5ay53a5g4d2p7mvlm1hbhxm9jfy9bk3gnhnqihlmvj")))

(define-public crate-lending-iterator-proc_macros-0.1.6-rc1 (c (n "lending-iterator-proc_macros") (v "0.1.6-rc1") (d (list (d (n "proc-macro2") (r "^1.0.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.7") (f (quote ("visit-mut"))) (d #t) (k 0)))) (h "1rak2p6l6i4clwraizhwm8qmy27pxvr9s3smv4qfjcl2sjygxrad")))

(define-public crate-lending-iterator-proc_macros-0.1.6-rc2 (c (n "lending-iterator-proc_macros") (v "0.1.6-rc2") (d (list (d (n "proc-macro2") (r "^1.0.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.7") (f (quote ("visit-mut"))) (d #t) (k 0)))) (h "0h2lgl5b4siv4xwx90zhp2hypbj0x1jr6qqkqyskd0slc0b5sdmg")))

(define-public crate-lending-iterator-proc_macros-0.1.6 (c (n "lending-iterator-proc_macros") (v "0.1.6") (d (list (d (n "proc-macro2") (r "^1.0.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.7") (f (quote ("visit-mut"))) (d #t) (k 0)))) (h "0ylmx9xnfd7lwvpz10fj2aynxs68k2664xgg07nrvm3ysr839ars")))

(define-public crate-lending-iterator-proc_macros-0.1.7-rc1 (c (n "lending-iterator-proc_macros") (v "0.1.7-rc1") (d (list (d (n "proc-macro2") (r "^1.0.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.7") (f (quote ("visit-mut"))) (d #t) (k 0)))) (h "0izvpv8rivga7li70sz1larrhpg6cs8wnnx742qa2b454c2g73fm")))

(define-public crate-lending-iterator-proc_macros-0.1.7 (c (n "lending-iterator-proc_macros") (v "0.1.7") (d (list (d (n "proc-macro2") (r "^1.0.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.7") (f (quote ("visit-mut"))) (d #t) (k 0)))) (h "078vsmr8lrp96sq8y4hihhzahv38zhbismhni9xykcfyq38msi5m")))

