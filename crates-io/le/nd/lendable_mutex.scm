(define-module (crates-io le nd lendable_mutex) #:use-module (crates-io))

(define-public crate-lendable_mutex-0.1.0 (c (n "lendable_mutex") (v "0.1.0") (d (list (d (n "defer") (r "^0.1") (d #t) (k 0)) (d (n "lock_api") (r "^0.4") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (o #t) (d #t) (k 0)))) (h "1ys5x5q9gx9j9a165dmqf5lp2qf9znf7zccl6lacmy33dw1i0zc4") (f (quote (("nightly" "parking_lot/nightly") ("default" "parking_lot"))))))

(define-public crate-lendable_mutex-0.2.0 (c (n "lendable_mutex") (v "0.2.0") (d (list (d (n "defer") (r "^0.1") (d #t) (k 0)) (d (n "lock_api") (r "^0.4") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (o #t) (d #t) (k 0)))) (h "0840csphwcjs8h9yrmqm54hfwmi08j98lic3shpmjy8vc99an5zj") (f (quote (("nightly" "parking_lot/nightly") ("default" "parking_lot"))))))

(define-public crate-lendable_mutex-0.3.0 (c (n "lendable_mutex") (v "0.3.0") (d (list (d (n "defer") (r "^0.1") (d #t) (k 0)) (d (n "lock_api") (r "^0.4") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (o #t) (d #t) (k 0)))) (h "1hn0n74l2syx22qp633zr2ygbxcz05asdqxfn7w2lwf5wyhjvq5d") (f (quote (("nightly" "parking_lot/nightly") ("default" "parking_lot"))))))

(define-public crate-lendable_mutex-0.3.1 (c (n "lendable_mutex") (v "0.3.1") (d (list (d (n "defer") (r "^0.1") (d #t) (k 0)) (d (n "lock_api") (r "^0.4") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (o #t) (d #t) (k 0)))) (h "1lszr5gwmlb26c5gf9kwack78my2g2habvy6y3k3bwqrgs3q8dd2") (f (quote (("nightly" "parking_lot/nightly") ("default" "parking_lot"))))))

(define-public crate-lendable_mutex-0.3.2 (c (n "lendable_mutex") (v "0.3.2") (d (list (d (n "defer") (r "^0.1") (d #t) (k 0)) (d (n "lock_api") (r "^0.4") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (o #t) (d #t) (k 0)))) (h "134id1fb0vgdqh76k2w9cqivzq4ii0dw0fnpxzxymhjz9152mh4p") (f (quote (("nightly" "parking_lot/nightly") ("default" "parking_lot"))))))

