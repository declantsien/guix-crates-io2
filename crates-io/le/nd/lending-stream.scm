(define-module (crates-io le nd lending-stream) #:use-module (crates-io))

(define-public crate-lending-stream-1.0.0 (c (n "lending-stream") (v "1.0.0") (d (list (d (n "futures-core") (r "^0.3.28") (d #t) (k 0)) (d (n "futures-lite") (r "^1.13.0") (d #t) (k 2)) (d (n "pin-project") (r "^1.1.3") (d #t) (k 0)))) (h "0hmc9f3hy2qb5lxdwx5h8az4nyxh9hzwz5pqiq83qwdsfpnv5ybd")))

