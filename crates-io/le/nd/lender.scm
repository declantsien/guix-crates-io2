(define-module (crates-io le nd lender) #:use-module (crates-io))

(define-public crate-lender-0.0.0 (c (n "lender") (v "0.0.0") (h "0g2kwa8g5dl7hkgg7nngpc1wmxqjqxgfdhf1ipcb86xs86g95vkj") (y #t)))

(define-public crate-lender-0.0.1 (c (n "lender") (v "0.0.1") (h "0i3xjqk2hqx086wlcdxa25w7fvkzbyjfg85fbs1w27ld44z0hxf7") (y #t)))

(define-public crate-lender-0.0.2 (c (n "lender") (v "0.0.2") (h "0ywvp18g92phn99c41dc9x90qaddh3rwb9zwzfwqmbbgv3p7fgcy") (y #t)))

(define-public crate-lender-0.1.0 (c (n "lender") (v "0.1.0") (h "14cx87rymdkd060kk1k4bm26mv6n1a34wqcf2ma3k08jhbjs7rw7") (y #t)))

(define-public crate-lender-0.1.1 (c (n "lender") (v "0.1.1") (h "1d2xhrmlkcvqvxqmq6dsfc3szlrwff6wj43in1r4yy11d3pq7ddn") (y #t)))

(define-public crate-lender-0.1.2 (c (n "lender") (v "0.1.2") (h "091yc5byk8k3p86mw7acr5ifi6ssbkz4aqjgj126932nr3lvwawy") (y #t)))

(define-public crate-lender-0.1.3 (c (n "lender") (v "0.1.3") (h "1g719fsy25j17vbkdnffv9mgbw60zkcqmq0wkq9klybfzv87c4a0") (y #t)))

(define-public crate-lender-0.1.5 (c (n "lender") (v "0.1.5") (h "1dl69mpxg19jcww957fdx3ry48xyn98ghya40f58xq35d56ylp85") (y #t)))

(define-public crate-lender-0.1.6 (c (n "lender") (v "0.1.6") (h "1ywvr5jaxzhg4by8angycc4r6nq946pfry5aqnd9c6wn6scbi67r")))

(define-public crate-lender-0.1.7 (c (n "lender") (v "0.1.7") (h "1x4sim49agx7cwlklc2a0aixgxjvkq5zlvfbf2w89wpqqswidrfc")))

(define-public crate-lender-0.2.0 (c (n "lender") (v "0.2.0") (h "0k0y5lpga63xblz3wh745qyllmz1s8k5mdz09hz94kmliapdvjr4")))

(define-public crate-lender-0.2.1 (c (n "lender") (v "0.2.1") (h "1bq2ks7nyd2ws90pn4if5462sgifc5y9sqjhx9gbhqpy7vg92j5z")))

(define-public crate-lender-0.2.2 (c (n "lender") (v "0.2.2") (h "1k6363y9p4h1y7ak9nkdsqp8vab3cnibi3hy7yamd61qvnp3ijwf")))

(define-public crate-lender-0.2.3 (c (n "lender") (v "0.2.3") (d (list (d (n "lender-derive") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "1rqf0pxr919imrl1yi59ys806hxx9b02y2sg10xvdyx6c5vhg3lp") (f (quote (("derive" "lender-derive") ("default" "derive"))))))

(define-public crate-lender-0.2.4 (c (n "lender") (v "0.2.4") (d (list (d (n "lender-derive") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "11vwwqmmaxa9hwa1xn520d1saq6jk7dw0i1mfd80bnmh6chxrjlx") (f (quote (("derive" "lender-derive") ("default" "derive"))))))

(define-public crate-lender-0.2.5 (c (n "lender") (v "0.2.5") (d (list (d (n "lender-derive") (r "^0.1.1") (o #t) (d #t) (k 0)))) (h "1zl0xljxgkdh4g05ipilwvq14j6gsiakdqxyc9wnnsnx0dzq9jg9") (f (quote (("derive" "lender-derive") ("default" "derive"))))))

(define-public crate-lender-0.2.6 (c (n "lender") (v "0.2.6") (d (list (d (n "lender-derive") (r "^0.1.1") (o #t) (d #t) (k 0)))) (h "1bnkgl6pwga5as9c4gn1vmx46b4lzfq3gc1anx4znvpb0wxnz892") (f (quote (("derive" "lender-derive") ("default" "derive"))))))

(define-public crate-lender-0.2.7 (c (n "lender") (v "0.2.7") (d (list (d (n "lender-derive") (r "^0.1.1") (o #t) (d #t) (k 0)))) (h "17s1rifqxsrwfalkd840i6bsl20pmb1c1s66alwlc4crlwh8jcx5") (f (quote (("derive" "lender-derive") ("default" "derive"))))))

(define-public crate-lender-0.2.8 (c (n "lender") (v "0.2.8") (d (list (d (n "lender-derive") (r "^0.1.1") (o #t) (d #t) (k 0)))) (h "0jhx4rwrxrzr60da804vrfimpni5a7hckc8n8lkfxgf6jjbi0n1x") (f (quote (("derive" "lender-derive") ("default" "derive"))))))

(define-public crate-lender-0.2.9 (c (n "lender") (v "0.2.9") (d (list (d (n "lender-derive") (r "^0.1.1") (o #t) (d #t) (k 0)))) (h "1q5jk9ai5kd98zd5vlfsdaq1vs22cn7rwgnr8wsh1nm5agvzgc9r") (f (quote (("derive" "lender-derive") ("default" "derive"))))))

