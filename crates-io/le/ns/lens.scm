(define-module (crates-io le ns lens) #:use-module (crates-io))

(define-public crate-lens-0.1.0 (c (n "lens") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "ratatui") (r "^0.25.0") (d #t) (k 0)) (d (n "tui-textarea") (r "^0.4.0") (d #t) (k 0)))) (h "1f931dwg0abdksix9ljpbq7xw1hkn0f5g2hbca9hxly76mlimc61")))

