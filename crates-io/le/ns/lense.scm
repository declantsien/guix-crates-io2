(define-module (crates-io le ns lense) #:use-module (crates-io))

(define-public crate-lense-0.1.0 (c (n "lense") (v "0.1.0") (h "0y0p74q797l6fv8nwnfjvnss6zkky7c07sl8hypsy3dfr7i111gr") (y #t)))

(define-public crate-lense-0.1.1 (c (n "lense") (v "0.1.1") (h "1ni9xvw8f33la0y7nvy8z1nnzk77yd8fpq47na9izfw1676n9vj2") (y #t)))

(define-public crate-lense-0.1.2 (c (n "lense") (v "0.1.2") (h "1m9i39gf5hs0fjkwp2av50m7s71kzjvjvn8p74p1c4h88y5i07ym") (y #t)))

