(define-module (crates-io le ns lens-rs_derive) #:use-module (crates-io))

(define-public crate-lens-rs_derive-0.0.5 (c (n "lens-rs_derive") (v "0.0.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1cf3fpywrfsmim54n57y8dyvsdxvbpii0z2yahqddz5a7ncbf0g7") (y #t)))

(define-public crate-lens-rs_derive-0.0.6 (c (n "lens-rs_derive") (v "0.0.6") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "16vh6na251bkn14zsnkxb0xzc776hzkwb4b8pi0a81zjpxwgpw38") (y #t)))

(define-public crate-lens-rs_derive-0.0.7 (c (n "lens-rs_derive") (v "0.0.7") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0z6cy8bvwvjwx5l0qbqwps97j35m5nc8k1dfvm1p104npm35j8pl")))

(define-public crate-lens-rs_derive-0.0.8 (c (n "lens-rs_derive") (v "0.0.8") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "03y1mkfa0nx5yvp3wj4lhjdbvwliyv999cvgnc662j4qkq424icw")))

(define-public crate-lens-rs_derive-0.0.88 (c (n "lens-rs_derive") (v "0.0.88") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0j5sdd2l46iq57l79gviajwhw3xjzg5yqla0kj9ixnxcicyfnbmb")))

(define-public crate-lens-rs_derive-0.0.9 (c (n "lens-rs_derive") (v "0.0.9") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full" "visit"))) (d #t) (k 0)))) (h "16l8iap9gw8kf06kqzcb8kcrm8wr136lbwjm7j85hqjc3p93gzcc") (y #t)))

(define-public crate-lens-rs_derive-0.1.0 (c (n "lens-rs_derive") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full" "visit"))) (d #t) (k 0)))) (h "0irfpvmnbzhc812phdpymljaha56rgr84fgb1gv4zkmhzgs48jwd")))

(define-public crate-lens-rs_derive-0.1.1 (c (n "lens-rs_derive") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full" "visit"))) (d #t) (k 0)))) (h "1g8arh4n8q4sc7j4ja6qjbdzpplyz4knjqvc0xjfrcicjpillhff")))

(define-public crate-lens-rs_derive-0.1.11 (c (n "lens-rs_derive") (v "0.1.11") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full" "visit"))) (d #t) (k 0)))) (h "002l0zb986v64yhzpn2pycnjd51a50wa4wk5jy9k8n0k50vhjfhg")))

(define-public crate-lens-rs_derive-0.1.2 (c (n "lens-rs_derive") (v "0.1.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full" "visit"))) (d #t) (k 0)))) (h "0hj88k5inkk96aj178h40myfidw3bpfldmw306nz7llizvr5i9ng") (y #t)))

(define-public crate-lens-rs_derive-0.1.20 (c (n "lens-rs_derive") (v "0.1.20") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full" "visit"))) (d #t) (k 0)))) (h "12yz1jp1bcnwpb82iq7sm95bs131mjhqg062iq6qc96f563m6fgp")))

(define-public crate-lens-rs_derive-0.2.0 (c (n "lens-rs_derive") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full" "visit" "printing"))) (d #t) (k 0)))) (h "02bgd9ivm15riq8iryyrmb0855h2qig9sz4lns5v0wr6yfr9zz9q")))

(define-public crate-lens-rs_derive-0.2.1 (c (n "lens-rs_derive") (v "0.2.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full" "visit" "printing"))) (d #t) (k 0)))) (h "1wiclridzds822j50vgwm1f6absysc4q7p4x62jmbyr07wva1d2i")))

(define-public crate-lens-rs_derive-0.2.2 (c (n "lens-rs_derive") (v "0.2.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full" "visit" "printing"))) (d #t) (k 0)))) (h "0cqcsirjppp41682ibn71gypfls43iysxiafzbsdi13wl7iwfgah")))

(define-public crate-lens-rs_derive-0.3.0 (c (n "lens-rs_derive") (v "0.3.0") (d (list (d (n "lens-rs_generator") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full" "visit" "printing"))) (d #t) (k 0)))) (h "0lsvgg2hd8ya8qx2kwhr6g0rbl648gz2v3jfz9c1s0xv5ss6w270") (f (quote (("structx"))))))

(define-public crate-lens-rs_derive-0.3.1 (c (n "lens-rs_derive") (v "0.3.1") (d (list (d (n "lens-rs_generator") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full" "visit" "printing"))) (d #t) (k 0)))) (h "06z57mqqqnpcaj1gg8rp5vaij3p66gmlbr58kvd9958yhfpk93sl") (f (quote (("structx"))))))

(define-public crate-lens-rs_derive-0.3.2 (c (n "lens-rs_derive") (v "0.3.2") (d (list (d (n "lens-rs_generator") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full" "visit" "printing"))) (d #t) (k 0)))) (h "0jnwbbwhhvnm3p7mdnrixm679fjawnwq2cjqnx0jnjks4jjrv731") (f (quote (("structx"))))))

