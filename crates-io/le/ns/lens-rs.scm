(define-module (crates-io le ns lens-rs) #:use-module (crates-io))

(define-public crate-lens-rs-0.0.8 (c (n "lens-rs") (v "0.0.8") (d (list (d (n "lens-rs_derive") (r "^0.0.5") (d #t) (k 0)))) (h "14xpmk3vjwavf4ziikfd71wd5x9yiihqn312pcgpbs0jkn8nadf7")))

(define-public crate-lens-rs-0.0.9 (c (n "lens-rs") (v "0.0.9") (d (list (d (n "lens-rs_derive") (r "^0.0.5") (d #t) (k 0)))) (h "0l544gfqlwy6y05jap2jdcgyxgbag2d3n3snsiv8lal7alvyvnng")))

(define-public crate-lens-rs-0.0.99 (c (n "lens-rs") (v "0.0.99") (d (list (d (n "lens-rs_derive") (r "^0.0.5") (d #t) (k 0)))) (h "19fxldxb8f7cdfipvklzwfyg2i7x6f8ph3fgn8az5xsr57wpwnb7")))

(define-public crate-lens-rs-0.1.0 (c (n "lens-rs") (v "0.1.0") (h "0lsba7ghmim7nn3s49yakhphp0p00r673mpy6xp7nybxikc47css")))

(define-public crate-lens-rs-0.1.1 (c (n "lens-rs") (v "0.1.1") (d (list (d (n "inwelling") (r "^0.3") (d #t) (k 1)) (d (n "lens-rs_derive") (r "^0.1.0") (d #t) (k 0)))) (h "00wg94dk7ywqx3ddg1h5cx8s444rb6cdnc9jkfzhid03m5m5lrjf")))

(define-public crate-lens-rs-0.1.2 (c (n "lens-rs") (v "0.1.2") (d (list (d (n "inwelling") (r "^0.3") (d #t) (k 1)) (d (n "lens-rs_derive") (r "^0.1") (d #t) (k 0)))) (h "13b6wk8d61mqd0fdp2ig7vpv2q1smlzzxzv6l0674jw78nxjafx4")))

(define-public crate-lens-rs-0.1.21 (c (n "lens-rs") (v "0.1.21") (d (list (d (n "inwelling") (r "^0.3") (d #t) (k 1)) (d (n "lens-rs_derive") (r "^0.1") (d #t) (k 0)))) (h "1c3n2sjy1d5d455175fp9c7l8p1xcfip9v8ik1n9a7v8pjwnj2ym")))

(define-public crate-lens-rs-0.1.22 (c (n "lens-rs") (v "0.1.22") (d (list (d (n "inwelling") (r "^0.3") (d #t) (k 1)) (d (n "lens-rs_derive") (r "^0.1") (d #t) (k 0)))) (h "0g1q0pm2lzpnwqqan88ii352wrzlj4z7hpzj4xv45qcsj3dvxiq7")))

(define-public crate-lens-rs-0.1.3 (c (n "lens-rs") (v "0.1.3") (d (list (d (n "inwelling") (r "^0.3") (d #t) (k 1)) (d (n "lens-rs_derive") (r "^0.1") (d #t) (k 0)))) (h "16c3nnsgsri7dham0487fbj4rx674pzclscjghwsairlyjs6gj75") (y #t)))

(define-public crate-lens-rs-0.1.30 (c (n "lens-rs") (v "0.1.30") (d (list (d (n "inwelling") (r "^0.3") (d #t) (k 1)) (d (n "lens-rs_derive") (r "^0.1") (d #t) (k 0)))) (h "0z0hj49pba41k3fkxavib2ba9djxd4n382j82v9xisa2fpbl5d31")))

(define-public crate-lens-rs-0.2.0 (c (n "lens-rs") (v "0.2.0") (d (list (d (n "inwelling") (r "^0.3") (d #t) (k 1)) (d (n "lens-rs_derive") (r "^0.2") (d #t) (k 0)))) (h "0pzwwlg6j71h1d6glrlksjpw60dmb08fj22wxlzjfvkg58rcm74i")))

(define-public crate-lens-rs-0.2.1 (c (n "lens-rs") (v "0.2.1") (d (list (d (n "inwelling") (r "^0.3") (d #t) (k 1)) (d (n "lens-rs_derive") (r "^0.2") (d #t) (k 0)))) (h "0fifyyq8m55zbia1hsb8nifgf6pdjcgd938m0x2fnshgvyyjvlf5")))

(define-public crate-lens-rs-0.2.2 (c (n "lens-rs") (v "0.2.2") (d (list (d (n "inwelling") (r "^0.3") (d #t) (k 1)) (d (n "lens-rs_derive") (r "^0.2") (d #t) (k 0)))) (h "093j4chiih846vx69vhmwknc8ny9qqyi0d3ayxknn3a59scj4rnf")))

(define-public crate-lens-rs-0.3.0 (c (n "lens-rs") (v "0.3.0") (d (list (d (n "lens-rs_derive") (r "^0.3") (d #t) (k 0)) (d (n "lens-rs_generator") (r "^0.1") (d #t) (k 0)))) (h "1rb69hyj7yl9fsy5wk1rjp5ic5h0f2b1djzns9rnflnmi59pzmf2") (f (quote (("structx" "lens-rs_generator/structx"))))))

(define-public crate-lens-rs-0.3.1 (c (n "lens-rs") (v "0.3.1") (d (list (d (n "lens-rs_derive") (r "^0.3") (d #t) (k 0)) (d (n "lens-rs_generator") (r "^0.1") (d #t) (k 0)))) (h "0sqz384cf739kirf9rd8q75h6zzm4jxdyc1g5y2w0ky7576cy34j") (f (quote (("structx" "lens-rs_generator/structx"))))))

(define-public crate-lens-rs-0.3.2 (c (n "lens-rs") (v "0.3.2") (d (list (d (n "lens-rs_derive") (r "^0.3") (d #t) (k 0)) (d (n "lens-rs_generator") (r "^0.1") (d #t) (k 0)))) (h "1lgmjm1yyd39jj1ga22iyr4cwz1scvy59rc0ky4fdwxh3hx6vmvv") (f (quote (("structx" "lens-rs_generator/structx"))))))

