(define-module (crates-io le ns lens_sdk) #:use-module (crates-io))

(define-public crate-lens_sdk-0.5.0 (c (n "lens_sdk") (v "0.5.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 0)))) (h "1946k09lcxb93nr6lsj3zh4d1m2w38i4rhna7xiayvprjy4n88vk")))

