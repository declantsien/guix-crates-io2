(define-module (crates-io le mn lemna-winit) #:use-module (crates-io))

(define-public crate-lemna-winit-0.2.0 (c (n "lemna-winit") (v "0.2.0") (d (list (d (n "lemna") (r "^0.2") (d #t) (k 0)) (d (n "lemna-macros") (r "^0.2") (d #t) (k 2)) (d (n "raw-window-handle") (r "^0.5") (d #t) (k 0)) (d (n "simplelog") (r "^0.7") (d #t) (k 2)) (d (n "ttf-noto-sans") (r "^0.1") (d #t) (k 2)) (d (n "winit") (r "^0.28") (d #t) (k 0)))) (h "1lj7h7p099i83c1wbnmhghxw73bfnyg8j5gsw1ryyqlr09rfnjbz") (f (quote (("instrumented" "lemna/instrumented"))))))

(define-public crate-lemna-winit-0.3.0 (c (n "lemna-winit") (v "0.3.0") (d (list (d (n "lemna") (r "^0.3") (d #t) (k 0)) (d (n "lemna-macros") (r "^0.3") (d #t) (k 2)) (d (n "raw-window-handle") (r "^0.5") (d #t) (k 0)) (d (n "simplelog") (r "^0.7") (d #t) (k 2)) (d (n "ttf-noto-sans") (r "^0.1") (d #t) (k 2)) (d (n "winit") (r "^0.28") (d #t) (k 0)))) (h "0wjx4id7g4ra3jfhpq215azf6bm5rhx3wv8d9jgmyabl8v0swg0z") (f (quote (("instrumented" "lemna/instrumented"))))))

(define-public crate-lemna-winit-0.4.0 (c (n "lemna-winit") (v "0.4.0") (d (list (d (n "lemna") (r "^0.4") (d #t) (k 0)) (d (n "lemna-macros") (r "^0.4") (d #t) (k 2)) (d (n "raw-window-handle") (r "^0.5") (d #t) (k 0)) (d (n "simplelog") (r "^0.7") (d #t) (k 2)) (d (n "ttf-noto-sans") (r "^0.1") (d #t) (k 2)) (d (n "winit") (r "^0.28") (d #t) (k 0)))) (h "1f32algaw1ysm3pr0hlsfynj36lp2cy0lf311r32a13xicdfyw1k") (f (quote (("instrumented" "lemna/instrumented"))))))

