(define-module (crates-io le mn lemna-macros) #:use-module (crates-io))

(define-public crate-lemna-macros-0.1.0 (c (n "lemna-macros") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1h6cdm905gnj3idiwi6p9zs36kf3q4pnfw8mf1m9r027d35y5fdw")))

(define-public crate-lemna-macros-0.2.0 (c (n "lemna-macros") (v "0.2.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0vjbkcqkmcpzf6jg73v8hzqndl9y9lhkrw402p7bvqaf1kh14rgx")))

(define-public crate-lemna-macros-0.3.0 (c (n "lemna-macros") (v "0.3.0") (d (list (d (n "global_counter") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1p2rwm6s0b914hxkvj895rnf6xjni95a4ipgmmjvj7vlbll9hwzf")))

(define-public crate-lemna-macros-0.4.0 (c (n "lemna-macros") (v "0.4.0") (d (list (d (n "global_counter") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "07l0sysx4nk0j2dgcy2m2d66nq84qr9b88x54h17ccrki7f9qyv6")))

