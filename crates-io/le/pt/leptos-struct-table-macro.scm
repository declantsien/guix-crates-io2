(define-module (crates-io le pt leptos-struct-table-macro) #:use-module (crates-io))

(define-public crate-leptos-struct-table-macro-0.1.0 (c (n "leptos-struct-table-macro") (v "0.1.0") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "05r1nagfvpi4fxc53p61x0ivdv0fqnzpr43yhndc69a5a82gdw0d")))

(define-public crate-leptos-struct-table-macro-0.1.1 (c (n "leptos-struct-table-macro") (v "0.1.1") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "09zlflxpx4grzrirvp4x5j5fmzcjxrvsh45si63jlvlmsmv9sh2y")))

(define-public crate-leptos-struct-table-macro-0.1.2 (c (n "leptos-struct-table-macro") (v "0.1.2") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1w9h9gv77w1m1200a38mcvpyn4qjfnzv397d7mps51nac51ja1cq")))

(define-public crate-leptos-struct-table-macro-0.1.3 (c (n "leptos-struct-table-macro") (v "0.1.3") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "12zqli9rqjrqw10r8brwqm5ah4in9d4r1jhfnwljnlxgki751v2k")))

(define-public crate-leptos-struct-table-macro-0.1.4 (c (n "leptos-struct-table-macro") (v "0.1.4") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1avsirxvyk81izqj16znrkn6caw82rdchjf5x0hv1nvj5d3acak5")))

(define-public crate-leptos-struct-table-macro-0.1.6 (c (n "leptos-struct-table-macro") (v "0.1.6") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "13ji15973c2vf7na0n01jp5hl2976sqdpgrq24caddnfi1c6mp5c")))

(define-public crate-leptos-struct-table-macro-0.2.0 (c (n "leptos-struct-table-macro") (v "0.2.0") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "08wad05ih99yj8nscn3i21limj47dp7g1gqxgjyql0akwq3rp4zk")))

(define-public crate-leptos-struct-table-macro-0.3.0 (c (n "leptos-struct-table-macro") (v "0.3.0") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "03j85zv0v5b64q5nki3kslc4xmsf9bb4d5f8a9cp6vzy9yj5yaix")))

(define-public crate-leptos-struct-table-macro-0.3.1 (c (n "leptos-struct-table-macro") (v "0.3.1") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1wdj56kjl59sibjwzz4aslsg65kz54dvjmrj3pckc8yvq6rf7dmp")))

(define-public crate-leptos-struct-table-macro-0.3.2 (c (n "leptos-struct-table-macro") (v "0.3.2") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0shj36jn4349c85n8h6m7w1i8zdvm3ql7qngcmcw32ljzyiwqkjl")))

(define-public crate-leptos-struct-table-macro-0.3.3 (c (n "leptos-struct-table-macro") (v "0.3.3") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0a7pzfcw92rj9z8px341j1ypk1xy3zaby9kzwax2rvfkkbppxja5")))

(define-public crate-leptos-struct-table-macro-0.4.0 (c (n "leptos-struct-table-macro") (v "0.4.0") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1gsjrjmx21rrysb1nh239607mngdskxllnrhgy6dv072yljk11hc")))

(define-public crate-leptos-struct-table-macro-0.4.1 (c (n "leptos-struct-table-macro") (v "0.4.1") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0b7j06sxj1ppn8hvcazfk4c0rwc3ybcca5i4qc16n452xn6wa3lz")))

(define-public crate-leptos-struct-table-macro-0.4.2 (c (n "leptos-struct-table-macro") (v "0.4.2") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1m2245m5m0id4rbmln3c8j17kh0mlwvdzz7aays2zcv3vqfqp5l6")))

(define-public crate-leptos-struct-table-macro-0.4.3 (c (n "leptos-struct-table-macro") (v "0.4.3") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "05x0fih9w1225rvvkw6j0zpp5350fnpmq114b87ysg2a5r6h79v8") (y #t)))

(define-public crate-leptos-struct-table-macro-0.5.0 (c (n "leptos-struct-table-macro") (v "0.5.0") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0ihcrcb28sxplvn4px2bma3ziphdf6ri1j373fv52iiry5515rvj") (y #t)))

(define-public crate-leptos-struct-table-macro-0.5.1 (c (n "leptos-struct-table-macro") (v "0.5.1") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "04q5vqyfz1syf9a9zxji8i10j8d9rqd3pxs5jwwp204jg5mgfd9r")))

(define-public crate-leptos-struct-table-macro-0.6.0 (c (n "leptos-struct-table-macro") (v "0.6.0") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0kizxidnnjvxgf9wkgzdfcllx6rms0bgniydykncyak9d8w7af13") (y #t)))

(define-public crate-leptos-struct-table-macro-0.6.1 (c (n "leptos-struct-table-macro") (v "0.6.1") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1356jv7z8m6fji1z7da2lwinsr4b59hlvqvih291fqwxa3jsqml4")))

(define-public crate-leptos-struct-table-macro-0.7.0 (c (n "leptos-struct-table-macro") (v "0.7.0") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0zvwk1igd0nwnpnly46318y5rcqj9mzh61vyd03bgd29vkgj8yk9")))

(define-public crate-leptos-struct-table-macro-0.8.0 (c (n "leptos-struct-table-macro") (v "0.8.0") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1kcr3lpgz7q8xxk9rn431s013vsv8rhzdc3h9qrbcsrhcbpb9084")))

(define-public crate-leptos-struct-table-macro-0.8.1 (c (n "leptos-struct-table-macro") (v "0.8.1") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1hcyvdp49qwszxa5mcfq2psgm42m4z1xyi17x52n8y76cph9cj92")))

(define-public crate-leptos-struct-table-macro-0.9.0 (c (n "leptos-struct-table-macro") (v "0.9.0") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "135mpp4r6p4sxnr9ba0v9fz7i8kzjn8jjv6rkwsfxwl9frjv99z7")))

(define-public crate-leptos-struct-table-macro-0.9.1 (c (n "leptos-struct-table-macro") (v "0.9.1") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0iysdmp3r5ix19f88pv06chm0zqny4lcrbs2b67h62bkh9wx95dk")))

(define-public crate-leptos-struct-table-macro-0.10.0 (c (n "leptos-struct-table-macro") (v "0.10.0") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0a1147x971s1sd50pc0r1xqbjk0jzn0iwwsl9m1wd0a1c5wykfpd")))

(define-public crate-leptos-struct-table-macro-0.11.0 (c (n "leptos-struct-table-macro") (v "0.11.0") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0gz1kmx7cp98nzgfgxizp2h9ahab3fyk28pabq6nnacag82c2k1p")))

