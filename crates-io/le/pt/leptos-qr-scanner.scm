(define-module (crates-io le pt leptos-qr-scanner) #:use-module (crates-io))

(define-public crate-leptos-qr-scanner-0.1.0 (c (n "leptos-qr-scanner") (v "0.1.0") (d (list (d (n "js-sys") (r "^0.3.64") (d #t) (k 0)) (d (n "leptos") (r "^0.6.5") (f (quote ("csr"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.64") (f (quote ("HtmlVideoElement"))) (d #t) (k 0)))) (h "140szkir46z22mhqbw6w01pb0w9v3nlxan006qhrrdsa3isagf2c")))

(define-public crate-leptos-qr-scanner-0.1.1 (c (n "leptos-qr-scanner") (v "0.1.1") (d (list (d (n "js-sys") (r "^0.3.64") (d #t) (k 0)) (d (n "leptos") (r "^0.6.5") (f (quote ("csr"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.64") (f (quote ("HtmlVideoElement"))) (d #t) (k 0)))) (h "0p36x45y7dpplrnr229w7qadx4ggi074kqwxr36f1c7mc5d471w0")))

