(define-module (crates-io le pt leptos_sweetalert) #:use-module (crates-io))

(define-public crate-leptos_sweetalert-1.0.0 (c (n "leptos_sweetalert") (v "1.0.0") (d (list (d (n "leptos") (r "^0.6.11") (f (quote ("csr"))) (d #t) (k 0)))) (h "1x9yk7sl2v4vl3sk6dq00czyrv4iyb8a8dv7vx6yzi2aib2mcyng")))

(define-public crate-leptos_sweetalert-1.0.1 (c (n "leptos_sweetalert") (v "1.0.1") (d (list (d (n "leptos") (r "^0.6.11") (f (quote ("csr"))) (d #t) (k 0)))) (h "1mhb2rg25kpmfhka8346023a01bq00y6nhssd1psri9li6khm2ma")))

(define-public crate-leptos_sweetalert-1.0.2 (c (n "leptos_sweetalert") (v "1.0.2") (d (list (d (n "leptos") (r "^0.6.11") (f (quote ("csr"))) (d #t) (k 0)))) (h "10z30mgy7976d6j55z013vhls9qdpam25s6gx4s1cw1qy1b8xffg")))

