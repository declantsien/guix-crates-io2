(define-module (crates-io le pt leptos_hotkeys) #:use-module (crates-io))

(define-public crate-leptos_hotkeys-0.1.0 (c (n "leptos_hotkeys") (v "0.1.0") (d (list (d (n "leptos") (r "^0.5.4") (f (quote ("csr" "nightly"))) (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.89") (d #t) (k 0)))) (h "0y0ppnagvliyiprywww86n3dbqmdwngiplg9gw9rn38kyclb44y5")))

(define-public crate-leptos_hotkeys-0.1.1 (c (n "leptos_hotkeys") (v "0.1.1") (d (list (d (n "leptos") (r "^0.5.4") (f (quote ("csr" "nightly"))) (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.89") (d #t) (k 0)))) (h "1zymi85gs33r3fl0p8a816fl7r17467nv9kw2yk39riqr1gwvii9")))

(define-public crate-leptos_hotkeys-0.1.2 (c (n "leptos_hotkeys") (v "0.1.2") (d (list (d (n "leptos") (r "^0.6.5") (f (quote ("csr" "nightly"))) (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.89") (d #t) (k 0)))) (h "0g6ixrbx90124lsbljmacq08mpfyv2a1c7j54jpcdzpk0iba02hi")))

(define-public crate-leptos_hotkeys-0.1.3 (c (n "leptos_hotkeys") (v "0.1.3") (d (list (d (n "leptos") (r "^0.6.5") (f (quote ("csr" "nightly"))) (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.89") (d #t) (k 0)))) (h "04dppj3d54ac887mrjx529jj6q9w0ahaqfq2cyfb879pxqf3ix2g")))

(define-public crate-leptos_hotkeys-0.1.4 (c (n "leptos_hotkeys") (v "0.1.4") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "leptos") (r "^0.6.5") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.89") (o #t) (d #t) (k 0)) (d (n "web-sys") (r "^0.3.66") (o #t) (d #t) (k 0)))) (h "1gmkmc03mbf8s2k622k57mm3k0nk50l5hq3qizg1wpnxjizbjrjp") (f (quote (("ssr")))) (s 2) (e (quote (("hydrate" "dep:web-sys" "dep:wasm-bindgen") ("csr" "dep:web-sys" "dep:wasm-bindgen"))))))

(define-public crate-leptos_hotkeys-0.1.5 (c (n "leptos_hotkeys") (v "0.1.5") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "leptos") (r "^0.6.5") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.89") (o #t) (d #t) (k 0)) (d (n "web-sys") (r "^0.3.66") (o #t) (d #t) (k 0)))) (h "0j6qaflqwafh9c7hbjq464i6kv0gzzn6qr3c1hx7s3hx4zqfp6kn") (f (quote (("ssr")))) (s 2) (e (quote (("hydrate" "dep:web-sys" "dep:wasm-bindgen") ("csr" "dep:web-sys" "dep:wasm-bindgen"))))))

(define-public crate-leptos_hotkeys-0.2.0-alpha.1 (c (n "leptos_hotkeys") (v "0.2.0-alpha.1") (d (list (d (n "leptos") (r "^0.6") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (d #t) (k 0)))) (h "0s6m92jsr67psi00rhnh6x6kl3cprjxx3blfm5r3592vyzi3wpc3") (f (quote (("ssr")))) (s 2) (e (quote (("debug" "dep:log"))))))

(define-public crate-leptos_hotkeys-0.2.0 (c (n "leptos_hotkeys") (v "0.2.0") (d (list (d (n "leptos") (r "^0.6") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (d #t) (k 0)))) (h "1936hlj93lqxjw13wl2kq3rnj7mr5wcm7hbmvfiy1lj8ph21jnj4") (f (quote (("ssr")))) (s 2) (e (quote (("debug" "dep:log"))))))

(define-public crate-leptos_hotkeys-0.2.1 (c (n "leptos_hotkeys") (v "0.2.1") (d (list (d (n "leptos") (r "^0.6") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (d #t) (k 0)))) (h "1fcryvll6gcw6yc1q0rahhbpwn24mwhg5mdyl2x4jp7j65hc7l59") (f (quote (("ssr")))) (s 2) (e (quote (("debug" "dep:log"))))))

