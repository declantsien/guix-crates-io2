(define-module (crates-io le pt leptos-font-icons) #:use-module (crates-io))

(define-public crate-leptos-font-icons-0.1.0 (c (n "leptos-font-icons") (v "0.1.0") (d (list (d (n "leptos") (r "^0.6.5") (f (quote ("csr" "nightly"))) (d #t) (k 0)))) (h "1wg7dyl2m3wcz5xkayp1mjrq2lgd2qnvg96c8rcbh1sc5bn6yayv")))

(define-public crate-leptos-font-icons-0.1.1 (c (n "leptos-font-icons") (v "0.1.1") (d (list (d (n "leptos") (r "^0.6.5") (f (quote ("csr" "nightly"))) (d #t) (k 0)))) (h "1qfi6hpx47i37m1gr0fsyixhxmkyjyhb258qnjv9pnbayfxkj7w6")))

