(define-module (crates-io le pt leptos-signals) #:use-module (crates-io))

(define-public crate-leptos-signals-0.0.1 (c (n "leptos-signals") (v "0.0.1") (d (list (d (n "leptos") (r "^0.0") (d #t) (k 0)))) (h "0s1dxwszr8z6qp45ryr72h2vw81w91g43lr1vpjql5z4pbwqsdxb") (f (quote (("stable" "leptos/stable"))))))

(define-public crate-leptos-signals-0.0.2 (c (n "leptos-signals") (v "0.0.2") (d (list (d (n "leptos_reactive") (r "^0.0") (k 0)))) (h "0gjk9ksslz9zhn80qrzx6qm7iyf2mkzaws0gxmqs8cvll06risq1") (f (quote (("stable" "leptos_reactive/stable"))))))

(define-public crate-leptos-signals-0.0.3 (c (n "leptos-signals") (v "0.0.3") (d (list (d (n "leptos_reactive") (r "^0.1") (k 0)))) (h "0aq59x3qqbzj5v9gd5vsc5smwdxyki627gxjv2pbh1ldc7m3fvrk") (f (quote (("stable" "leptos_reactive/stable"))))))

