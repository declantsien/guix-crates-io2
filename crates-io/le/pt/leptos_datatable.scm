(define-module (crates-io le pt leptos_datatable) #:use-module (crates-io))

(define-public crate-leptos_datatable-0.1.0 (c (n "leptos_datatable") (v "0.1.0") (d (list (d (n "leptos") (r "^0.6.9") (f (quote ("csr" "nightly"))) (d #t) (k 0)))) (h "00k6fz5drmrzxi7sf275fl5ankk0gjw1skp1npv233530myiq7is") (y #t)))

(define-public crate-leptos_datatable-0.1.0-alpha (c (n "leptos_datatable") (v "0.1.0-alpha") (d (list (d (n "leptos") (r "^0.6.9") (f (quote ("csr" "nightly"))) (d #t) (k 0)))) (h "00rmif5z825dsdz4s07nh48hcligm1km0s40jrv77nrgi73xf4lz")))

(define-public crate-leptos_datatable-0.1.1-alpha (c (n "leptos_datatable") (v "0.1.1-alpha") (d (list (d (n "leptos") (r "^0.6.9") (f (quote ("csr" "nightly"))) (d #t) (k 0)))) (h "1apwm96lhkbwc6z4fkcx4mbfhiwlh9zc0m7ynk2bdihv0gsjc1yr")))

