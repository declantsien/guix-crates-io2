(define-module (crates-io le pt leptonic-theme) #:use-module (crates-io))

(define-public crate-leptonic-theme-0.0.1 (c (n "leptonic-theme") (v "0.0.1") (d (list (d (n "include_dir") (r "^0.7") (d #t) (k 0)))) (h "07f2ds6ns9jj4k61xjadr5dcdwsqlf559lgklgnrkdcsfkzw1hn3") (r "1.56")))

(define-public crate-leptonic-theme-0.1.0 (c (n "leptonic-theme") (v "0.1.0") (d (list (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)) (d (n "indoc") (r "^2.0.3") (d #t) (k 0)))) (h "1r01d9wxs6lf5mh45qd99gcivcm62bi1323n08cgb9kka68bngaj") (r "1.60")))

(define-public crate-leptonic-theme-0.2.0 (c (n "leptonic-theme") (v "0.2.0") (d (list (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)) (d (n "indoc") (r "^2.0.3") (d #t) (k 0)))) (h "07ck9i2ac5i0j996zfkg758flx3jc3h2ypkigrpvvaxd6jk6s4d0") (r "1.60")))

(define-public crate-leptonic-theme-0.3.0-beta (c (n "leptonic-theme") (v "0.3.0-beta") (d (list (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)) (d (n "indoc") (r "^2.0.3") (d #t) (k 0)))) (h "1cxl2h2kfdcap832zqjjj0dq4p982qasqk707lwb0pxy4w9d85qj") (r "1.60")))

(define-public crate-leptonic-theme-0.3.0 (c (n "leptonic-theme") (v "0.3.0") (d (list (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)) (d (n "indoc") (r "^2.0.3") (d #t) (k 0)))) (h "0xpyclj4y276wiqksqsmmv1czi5yrvc6i4lcj58q45c3x6lpdnlq") (r "1.60")))

(define-public crate-leptonic-theme-0.4.0 (c (n "leptonic-theme") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)) (d (n "indoc") (r "^2.0.4") (d #t) (k 0)))) (h "1xajnjbxdysfiy2qqh8pjaig8gsbci9n3rwsrh1wk3mizfb2yvgf") (r "1.60")))

(define-public crate-leptonic-theme-0.5.0 (c (n "leptonic-theme") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)) (d (n "indoc") (r "^2.0.4") (d #t) (k 0)))) (h "0kvx8wcdj860dld1i0w24bpnpzl6xflrz56q7kv2pcibdkdhv15k") (r "1.60")))

