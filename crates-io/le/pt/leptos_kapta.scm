(define-module (crates-io le pt leptos_kapta) #:use-module (crates-io))

(define-public crate-leptos_kapta-0.0.2 (c (n "leptos_kapta") (v "0.0.2") (d (list (d (n "kapta") (r "^0.0.2") (d #t) (k 0)) (d (n "leptos") (r "^0.5") (f (quote ("csr"))) (d #t) (k 0)) (d (n "leptos-use") (r "^0.8") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "wasm-logger") (r "^0.2.0") (d #t) (k 0)))) (h "09k7znxz4n1bl0h5ycrnnih5qa3iw91c34g15dplzbbi6jx9hsb7")))

(define-public crate-leptos_kapta-0.0.3 (c (n "leptos_kapta") (v "0.0.3") (d (list (d (n "kapta") (r "^0.0.3") (d #t) (k 0)) (d (n "leptos") (r "^0.5") (f (quote ("csr"))) (d #t) (k 0)) (d (n "leptos-use") (r "^0.8") (d #t) (k 0)))) (h "1lx5xsxf6d50zd5hai0hmp68yvqgbv85gk137syxxpc0kcdb40dq")))

(define-public crate-leptos_kapta-0.0.4 (c (n "leptos_kapta") (v "0.0.4") (d (list (d (n "geojson") (r "^0.24.1") (d #t) (k 0)) (d (n "kapta") (r "^0.0.4") (d #t) (k 0)) (d (n "leptos") (r "^0.5.2") (f (quote ("csr"))) (d #t) (k 0)) (d (n "leptos-use") (r "^0.8") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)))) (h "1w0q985pid6alpb1b4n73icanjdb0rd8508zrzck5davq9c4w8xk")))

