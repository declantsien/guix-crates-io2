(define-module (crates-io le pt leptos_devtools) #:use-module (crates-io))

(define-public crate-leptos_devtools-0.0.0 (c (n "leptos_devtools") (v "0.0.0") (h "0sh8b4xipqq7d7amg2q3q79bkxlh5q75srlhgvrmrwnjgz8wsrpa") (y #t)))

(define-public crate-leptos_devtools-0.0.1 (c (n "leptos_devtools") (v "0.0.1") (d (list (d (n "leptos_devtools_extension_api") (r "^0.0.1") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.17") (f (quote ("smallvec" "fmt" "std"))) (k 0)))) (h "0wr8w4i8838qrsh7k05rih8z4p5ap5ymmlc9rixh4vi4dsc1jbrk")))

