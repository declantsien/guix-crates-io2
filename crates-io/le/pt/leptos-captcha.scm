(define-module (crates-io le pt leptos-captcha) #:use-module (crates-io))

(define-public crate-leptos-captcha-0.1.0 (c (n "leptos-captcha") (v "0.1.0") (d (list (d (n "leptos") (r "^0.5.4") (d #t) (k 0)) (d (n "spow") (r "^0.2.0") (d #t) (k 0)))) (h "0sp1kwa34ccpy998yixgyg7wbsdhsnzj46kxp622xw7cxss1ryha") (r "1.70.0")))

(define-public crate-leptos-captcha-0.2.0 (c (n "leptos-captcha") (v "0.2.0") (d (list (d (n "leptos") (r "^0.6.5") (d #t) (k 0)) (d (n "spow") (r "^0.2.0") (d #t) (k 0)))) (h "0nqb00bi3z1w1gn21myj4cdr626d9sgpl8mz1nhfcs1rxk4k7lqs") (r "1.75.0")))

