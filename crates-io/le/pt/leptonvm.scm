(define-module (crates-io le pt leptonvm) #:use-module (crates-io))

(define-public crate-leptonvm-0.1.0 (c (n "leptonvm") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "peg") (r "^0.8.0") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "serde_with") (r "^1.13.0") (d #t) (k 0)))) (h "1kankclpwwyz0wrnw9qn1pqyxcwa26f7ih80yhdyf8kjm8753yra")))

