(define-module (crates-io le pt leptonica-plumbing) #:use-module (crates-io))

(define-public crate-leptonica-plumbing-0.1.0 (c (n "leptonica-plumbing") (v "0.1.0") (d (list (d (n "leptonica-sys") (r "^0.3.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "07z5fmhy3axg4mj24y3qi74yhk82lk235xhn0gynlpgw3pb1x32x")))

(define-public crate-leptonica-plumbing-0.2.0 (c (n "leptonica-plumbing") (v "0.2.0") (d (list (d (n "leptonica-sys") (r "^0.3.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1qca02ijyspn5b1mm5p4rlq497qmc4fd14dfqi76vyfkhmcqs5rg")))

(define-public crate-leptonica-plumbing-0.3.0 (c (n "leptonica-plumbing") (v "0.3.0") (d (list (d (n "leptonica-sys") (r "^0.3.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0aqyd5a667265ijal1pcxin6wxma0a065wi0jzphaqgw2r2scqan")))

(define-public crate-leptonica-plumbing-0.4.0 (c (n "leptonica-plumbing") (v "0.4.0") (d (list (d (n "leptonica-sys") (r "^0.3.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1vc7d8q47b19chkjz546g8g214sawsxvgfk0i603qlpz0af9m74d")))

(define-public crate-leptonica-plumbing-0.4.1 (c (n "leptonica-plumbing") (v "0.4.1") (d (list (d (n "leptonica-sys") (r "~0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1hqrqn4mvfis1zjwwjrafdxq67gfg3zplwvsphmq4r8bjpkbxb8k")))

(define-public crate-leptonica-plumbing-0.5.0 (c (n "leptonica-plumbing") (v "0.5.0") (d (list (d (n "leptonica-sys") (r "~0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0binvrc9nhlnv3hh5m45cjaivjmyxj0zz79ipzyiqzv0d9nl64xn")))

(define-public crate-leptonica-plumbing-0.6.0 (c (n "leptonica-plumbing") (v "0.6.0") (d (list (d (n "leptonica-sys") (r "~0.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "15pvzmyhaxj8x88kzfgk78w5ql5cdksqkjcx7sqbwz7gn3624m6s")))

(define-public crate-leptonica-plumbing-1.0.0 (c (n "leptonica-plumbing") (v "1.0.0") (d (list (d (n "leptonica-sys") (r "~0.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1a84cxxjla4zkzrychwznxwzg087c0gc61m9ba6yk9gdp2a2y7pk")))

(define-public crate-leptonica-plumbing-1.0.1 (c (n "leptonica-plumbing") (v "1.0.1") (d (list (d (n "leptonica-sys") (r "~0.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1zhssh38s32mr2rhxjvh7gziyw7hrn6nc5crvdnvnm8pvqd79qc7")))

(define-public crate-leptonica-plumbing-1.1.0 (c (n "leptonica-plumbing") (v "1.1.0") (d (list (d (n "leptonica-sys") (r "~0.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0cfprrpp63ka6skx57139wwirz1cfspcdjpsyaw86pvgxdn3sw91")))

(define-public crate-leptonica-plumbing-1.2.0 (c (n "leptonica-plumbing") (v "1.2.0") (d (list (d (n "leptonica-sys") (r "~0.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1rgxa0692iv1131zif5rr1pyvagsqz7br9rnsx5gbdi0jq9b4az5")))

(define-public crate-leptonica-plumbing-1.3.0 (c (n "leptonica-plumbing") (v "1.3.0") (d (list (d (n "leptonica-sys") (r "~0.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0v30szx51jgcy685h84mydrkfdxwr9n9lbnc5a6pngylfygg36v4")))

(define-public crate-leptonica-plumbing-1.4.0 (c (n "leptonica-plumbing") (v "1.4.0") (d (list (d (n "leptonica-sys") (r "~0.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0hkg2lqlwav6jdf5j5sjajx8pka7dwr3y8wd2lwhs2bg7p278ync")))

