(define-module (crates-io le pt leptos-tiptap-build) #:use-module (crates-io))

(define-public crate-leptos-tiptap-build-0.1.0 (c (n "leptos-tiptap-build") (v "0.1.0") (d (list (d (n "phf") (r "^0.11") (d #t) (k 0)))) (h "1c6v1fkhqb2wd2k64crclg196mrydj4z09p0j38cfnklpy961smx") (r "1.70")))

(define-public crate-leptos-tiptap-build-0.1.1 (c (n "leptos-tiptap-build") (v "0.1.1") (d (list (d (n "phf") (r "^0.11") (d #t) (k 0)))) (h "08qswl8fxg8xhiwc0kxqi9hcpq8w4pmzp584mrfjwwsywnr3szfw") (r "1.70")))

(define-public crate-leptos-tiptap-build-0.2.0 (c (n "leptos-tiptap-build") (v "0.2.0") (d (list (d (n "phf") (r "^0.11") (d #t) (k 0)))) (h "0z1i1w26r7yd1np0crbkpybwwm4b3b53fslnjd5qihfrh76qzidn") (r "1.70")))

(define-public crate-leptos-tiptap-build-0.2.1 (c (n "leptos-tiptap-build") (v "0.2.1") (d (list (d (n "phf") (r "^0.11") (d #t) (k 0)))) (h "07sy6hslq88q9ybw76bk7snq7via30v3mkp8hn46pvjwbsvnnyqk") (r "1.60")))

(define-public crate-leptos-tiptap-build-0.2.2 (c (n "leptos-tiptap-build") (v "0.2.2") (d (list (d (n "phf") (r "^0.11.2") (d #t) (k 0)))) (h "0imx3q2d808hpv38ib11jhncv3wyd5ph1vkf1fxqshimwsp51yg9") (r "1.60.0")))

(define-public crate-leptos-tiptap-build-0.2.3 (c (n "leptos-tiptap-build") (v "0.2.3") (d (list (d (n "phf") (r "^0.11.2") (d #t) (k 0)))) (h "0rc0dj319sb4pxmszxvd3h4khz8cc0m9mzbwkb8gw3gfj0h3cpv9") (r "1.60.0")))

(define-public crate-leptos-tiptap-build-0.2.4 (c (n "leptos-tiptap-build") (v "0.2.4") (d (list (d (n "phf") (r "^0.11.2") (d #t) (k 0)))) (h "0zrqlpwkrmx9nm61wsbb82ydkx3mjq4fmn0szv3p782sacnm4ws6") (r "1.60.0")))

(define-public crate-leptos-tiptap-build-0.2.5 (c (n "leptos-tiptap-build") (v "0.2.5") (d (list (d (n "phf") (r "^0.11.2") (d #t) (k 0)))) (h "0qgk4ha5976kdb5fmi153sbfrwcfhm7hsiv6yjaj4p7x75rpl10r") (r "1.60.0")))

(define-public crate-leptos-tiptap-build-0.2.6 (c (n "leptos-tiptap-build") (v "0.2.6") (h "0i5rp9h1dc9ja037wvqqa0ygcw9kkdggz7g9dplynq0zqgx4yd1l") (r "1.60.0")))

