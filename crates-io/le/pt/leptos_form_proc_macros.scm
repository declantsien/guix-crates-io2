(define-module (crates-io le pt leptos_form_proc_macros) #:use-module (crates-io))

(define-public crate-leptos_form_proc_macros-0.1.0 (c (n "leptos_form_proc_macros") (v "0.1.0") (d (list (d (n "leptos_form_proc_macros_core") (r "^0.1.0") (d #t) (k 0)))) (h "1xfr5had3gxxz26flraqwp6dvnsl16v3mk1psw479zai002fl9ln") (r "1.75.0")))

(define-public crate-leptos_form_proc_macros-0.1.1 (c (n "leptos_form_proc_macros") (v "0.1.1") (d (list (d (n "leptos_form_proc_macros_core") (r "^0.1.0") (d #t) (k 0)))) (h "0rbqhkzn2lymafvcrmscblsw0xy4j0iin29pxfps540zn78nkw8s") (r "1.75.0")))

(define-public crate-leptos_form_proc_macros-0.1.2 (c (n "leptos_form_proc_macros") (v "0.1.2") (d (list (d (n "leptos_form_proc_macros_core") (r "^0.1.0") (d #t) (k 0)))) (h "1bxfchclk3f6lpr7slwj95kcyrqk8l5vxrqi89jjmry1m6k53xzl") (r "1.75.0")))

(define-public crate-leptos_form_proc_macros-0.1.3 (c (n "leptos_form_proc_macros") (v "0.1.3") (d (list (d (n "leptos_form_proc_macros_core") (r "^0.1.0") (d #t) (k 0)))) (h "095qifna8dhgsrhiiwgqbcndrs7rymndgdxmwvz3c8gg7gfhx2fs") (r "1.75.0")))

(define-public crate-leptos_form_proc_macros-0.1.4 (c (n "leptos_form_proc_macros") (v "0.1.4") (d (list (d (n "leptos_form_proc_macros_core") (r "^0.1.0") (d #t) (k 0)))) (h "15bfgvg4mpsnm0rlifga6nblha007mc8g1bg2cw6ss1bplp9gd2i") (r "1.75.0")))

(define-public crate-leptos_form_proc_macros-0.1.5 (c (n "leptos_form_proc_macros") (v "0.1.5") (d (list (d (n "leptos_form_proc_macros_core") (r "^0.1.5") (d #t) (k 0)))) (h "1vlh8avn9liaglbjl9hjpz4qkf47fdbwayw4364jh365i7ncqbvg") (r "1.75.0")))

(define-public crate-leptos_form_proc_macros-0.1.6 (c (n "leptos_form_proc_macros") (v "0.1.6") (d (list (d (n "leptos_form_proc_macros_core") (r "^0.1.6") (d #t) (k 0)))) (h "13s1fsr70fvj3w2a25qbk3fgjb2jliparzs111y1vaqxii9f6hc4") (r "1.75.0")))

(define-public crate-leptos_form_proc_macros-0.1.7 (c (n "leptos_form_proc_macros") (v "0.1.7") (d (list (d (n "leptos_form_proc_macros_core") (r "^0.1.7") (d #t) (k 0)))) (h "0yz1c72pw9khqv6yc71cw91vxdcbcx50hx9yhgq05s4jmcpk3pds") (r "1.75.0")))

(define-public crate-leptos_form_proc_macros-0.1.8 (c (n "leptos_form_proc_macros") (v "0.1.8") (d (list (d (n "leptos_form_proc_macros_core") (r "^0.1.7") (d #t) (k 0)))) (h "0vpgm8fq10n68lqvnf7fzrrcd16b0rdj1snciv2jp6k3fr71vxb6") (r "1.75.0")))

(define-public crate-leptos_form_proc_macros-0.2.0-beta-1 (c (n "leptos_form_proc_macros") (v "0.2.0-beta-1") (d (list (d (n "leptos_form_proc_macros_core") (r "=0.2.0-beta-1") (d #t) (k 0)))) (h "1768sr0dykfhlc7mf2z13lhzjfixxp76q9x2x75id5c8ikfmfd0l") (r "1.75.0")))

(define-public crate-leptos_form_proc_macros-0.2.0-beta-2 (c (n "leptos_form_proc_macros") (v "0.2.0-beta-2") (d (list (d (n "leptos_form_proc_macros_core") (r "=0.2.0-beta-2") (d #t) (k 0)))) (h "11z99hhkpf6xjgk5l9jkwzqdrap8r93b8zki3qqb0rvgxa6hbw00") (r "1.75.0")))

(define-public crate-leptos_form_proc_macros-0.2.0-rc1 (c (n "leptos_form_proc_macros") (v "0.2.0-rc1") (d (list (d (n "leptos_form_proc_macros_core") (r "=0.2.0-rc1") (d #t) (k 0)))) (h "0ga41kb08lg1lvm0f8qg2fn2wcx8cil2a149vs2ng8ndp5v70hcd") (r "1.75.0")))

