(define-module (crates-io le pt leptos_test) #:use-module (crates-io))

(define-public crate-leptos_test-0.1.0 (c (n "leptos_test") (v "0.1.0") (d (list (d (n "dom_testing_library") (r "^0.1.0") (d #t) (k 0)) (d (n "leptos") (r "^0.6.0-beta") (f (quote ("csr"))) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.9") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.0") (d #t) (k 2)) (d (n "web-sys") (r "^0.3.0") (f (quote ("Element" "HtmlElement"))) (d #t) (k 0)))) (h "0hszlacn79ilbz1mdz7pc1w0xvbmwkx32wrf252f2qfrhx2chpx5")))

