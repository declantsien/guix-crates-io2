(define-module (crates-io le pt leptoskit) #:use-module (crates-io))

(define-public crate-leptoskit-0.0.0 (c (n "leptoskit") (v "0.0.0") (d (list (d (n "leptos") (r "^0.1") (f (quote ("serde" "stable"))) (d #t) (k 0)))) (h "0gk14mzjnqcklyr9mvqd1y4xlv103xkcamlxp0lg9r9ahim3hff1") (r "1.67.0")))

