(define-module (crates-io le pt leptess) #:use-module (crates-io))

(define-public crate-leptess-0.1.0 (c (n "leptess") (v "0.1.0") (d (list (d (n "bindgen") (r "0.49.*") (d #t) (k 1)) (d (n "libc") (r "0.*") (d #t) (k 0)))) (h "0926dhfz03yg644v56am8105bcavrjb3cirsdjis6ybm8yqlcrl9")))

(define-public crate-leptess-0.2.0 (c (n "leptess") (v "0.2.0") (d (list (d (n "bindgen") (r "0.49.*") (d #t) (k 1)) (d (n "libc") (r "0.*") (d #t) (k 0)))) (h "15rak5rj3qzd6adbhnxq2z64bjga331cap8ks5hdz1qs5zbgp07n")))

(define-public crate-leptess-0.2.1 (c (n "leptess") (v "0.2.1") (d (list (d (n "bindgen") (r "0.49.*") (d #t) (k 1)) (d (n "libc") (r "0.*") (d #t) (k 0)))) (h "0wq6qslx4jr8x29kf59g08rvp26dhhn8x18wkyz0ds59glzfi19l")))

(define-public crate-leptess-0.3.0 (c (n "leptess") (v "0.3.0") (d (list (d (n "bindgen") (r "0.49.*") (d #t) (k 1)) (d (n "libc") (r "0.*") (d #t) (k 0)))) (h "0188hdz4gdq7vjp4760rl8k4z7k928v9485dzyrllcqgamfl4qs7")))

(define-public crate-leptess-0.4.0 (c (n "leptess") (v "0.4.0") (d (list (d (n "bindgen") (r "0.49.*") (d #t) (k 1)) (d (n "libc") (r "0.*") (d #t) (k 0)))) (h "0q0mbh8ljjzmvphyy0r059k7gxq7r6r4z0blwqqpszdrld9ii3h6")))

(define-public crate-leptess-0.4.1 (c (n "leptess") (v "0.4.1") (d (list (d (n "bindgen") (r "0.49.*") (d #t) (k 1)) (d (n "libc") (r "0.*") (d #t) (k 0)))) (h "1pz1kavk3lva74s74caly97y5dj25s362blnfifazff1mbgw05y1")))

(define-public crate-leptess-0.5.0 (c (n "leptess") (v "0.5.0") (d (list (d (n "bindgen") (r "0.49.*") (d #t) (k 1)) (d (n "libc") (r "0.*") (d #t) (k 0)))) (h "0rvb6zagiplf2sa7yb0x107552f2pqc3l6kkcsw84yzyz5432ymm")))

(define-public crate-leptess-0.6.0 (c (n "leptess") (v "0.6.0") (d (list (d (n "bindgen") (r "0.49.*") (d #t) (k 1)) (d (n "libc") (r "0.*") (d #t) (k 0)))) (h "13vnvxy8kmymdir9jhwspiba8y350267mmzilr736ka1k0spxnzh")))

(define-public crate-leptess-0.6.1 (c (n "leptess") (v "0.6.1") (d (list (d (n "bindgen") (r "0.49.*") (d #t) (k 1)) (d (n "libc") (r "0.*") (d #t) (k 0)))) (h "052sr8vxpl84gp5lg1y1myprl4i6jd0a8r82b6mk25hrkv00fzbv")))

(define-public crate-leptess-0.7.0 (c (n "leptess") (v "0.7.0") (d (list (d (n "leptonica-sys") (r "^0.2.0") (d #t) (k 0)) (d (n "tesseract-sys") (r "^0.3.0") (d #t) (k 0)))) (h "0ai6p6z3d5gawv67yq2vmvhcnlkzidza8bs2lwcg5mn2jfwryl5n")))

(define-public crate-leptess-0.7.1 (c (n "leptess") (v "0.7.1") (d (list (d (n "leptonica-sys") (r "^0.2.0") (d #t) (k 0)) (d (n "tesseract-sys") (r "^0.3.0") (d #t) (k 0)))) (h "1sbyxb3a0mydhxjg9nl1jc21wx15sqf8snyld8abkbqqinyz82jb")))

(define-public crate-leptess-0.7.2 (c (n "leptess") (v "0.7.2") (d (list (d (n "leptonica-sys") (r "^0.2.0") (d #t) (k 0)) (d (n "tesseract-sys") (r "^0.3.0") (d #t) (k 0)))) (h "1bpzrima032l39995fm7pn383m2x0w1g1bxw4csw2qgrjn5jssbq")))

(define-public crate-leptess-0.7.3 (c (n "leptess") (v "0.7.3") (d (list (d (n "leptonica-sys") (r "^0.2.0") (d #t) (k 0)) (d (n "tesseract-sys") (r "^0.3.0") (d #t) (k 0)))) (h "07qdxvj801j6073ph6i1m9hn2nc58wx8q8qdhzhcbxd7vgpmbi4a")))

(define-public crate-leptess-0.7.4 (c (n "leptess") (v "0.7.4") (d (list (d (n "leptonica-sys") (r "^0.2.0") (d #t) (k 0)) (d (n "tesseract-sys") (r "^0.3.0") (d #t) (k 0)))) (h "06s2lxiw53bzrikhdbpq18jq4m4h33ykqk7iazgdmh3fbixgk8yv")))

(define-public crate-leptess-0.8.0 (c (n "leptess") (v "0.8.0") (d (list (d (n "leptonica-sys") (r "^0.2.0") (d #t) (k 0)) (d (n "tesseract-sys") (r "^0.4.0") (d #t) (k 0)))) (h "10vckwvhfd7m7h9n227r31pj93q7vdac6vgihsjiscbyxb1c8hzc")))

(define-public crate-leptess-0.8.1 (c (n "leptess") (v "0.8.1") (d (list (d (n "leptonica-sys") (r "^0.2.0") (d #t) (k 0)) (d (n "tesseract-sys") (r "^0.4.1") (d #t) (k 0)))) (h "0ls9w355wxdsqdkfl0qjd5viqp5km9dbyip6gp1zs0b3x7p9vbxc")))

(define-public crate-leptess-0.8.2 (c (n "leptess") (v "0.8.2") (d (list (d (n "leptonica-sys") (r "^0.3.1") (d #t) (k 0)) (d (n "tesseract-sys") (r "^0.5.2") (d #t) (k 0)))) (h "1slgd13ldhkvcqwilqhj6m88a0xannzmbwqdalr817h7i79lwgpv")))

(define-public crate-leptess-0.8.3 (c (n "leptess") (v "0.8.3") (d (list (d (n "leptonica-sys") (r "^0.3.1") (d #t) (k 0)) (d (n "tesseract-sys") (r "^0.5.2") (d #t) (k 0)))) (h "0794g86gpsazpndxmk61zvjz05xl0l70nx9kkmf33nzsblg6yv72")))

(define-public crate-leptess-0.9.0 (c (n "leptess") (v "0.9.0") (d (list (d (n "leptonica-sys") (r "^0.3.2") (d #t) (k 0)) (d (n "tesseract-sys") (r "^0.5.2") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "15c3jskp8dmpxja6ibajlxvm503ih8badc5h2ifhbsxd6568vrrj")))

(define-public crate-leptess-0.10.0 (c (n "leptess") (v "0.10.0") (d (list (d (n "leptonica-sys") (r "^0.3.2") (d #t) (k 0)) (d (n "tesseract-sys") (r "^0.5.2") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0nnk7ndqnip66gl7hhsgkx5x431ghpjdm280nwia264d02xcp59q")))

(define-public crate-leptess-0.11.0 (c (n "leptess") (v "0.11.0") (d (list (d (n "leptonica-sys") (r "^0.3.2") (d #t) (k 0)) (d (n "regex") (r "^1.4.3") (d #t) (k 2)) (d (n "tesseract-sys") (r "^0.5.2") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1jx14jyyvs4xhdsdapnfay9ia3m8znr3blpj7jbjnlpj7r518vq1")))

(define-public crate-leptess-0.12.0 (c (n "leptess") (v "0.12.0") (d (list (d (n "regex") (r "^1.4.3") (d #t) (k 2)) (d (n "tesseract-plumbing") (r "^0.5.1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1vjfkj3jdmir5y09p1m9amzsa7cpwrjk6pwkm4clx7qigg7dxx0r")))

(define-public crate-leptess-0.13.0 (c (n "leptess") (v "0.13.0") (d (list (d (n "regex") (r "^1.4.3") (d #t) (k 2)) (d (n "tesseract-plumbing") (r "^0.5.1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "14nak42g02b1r5kb017flgd1dq313hn0kbqgz69vh5knrqfpjy2s")))

(define-public crate-leptess-0.13.1 (c (n "leptess") (v "0.13.1") (d (list (d (n "regex") (r "^1.4.3") (d #t) (k 2)) (d (n "tesseract-plumbing") (r "^0.5.2") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1jmfk04pd9j12y5y1fjs54rf9w8rzh6w5b74ci6cl7a80x2n2x0m")))

(define-public crate-leptess-0.13.2 (c (n "leptess") (v "0.13.2") (d (list (d (n "regex") (r "^1.4.3") (d #t) (k 2)) (d (n "tesseract-plumbing") (r "~0.6") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "14a0ggdjy7bpcy9vlik84wyxj4qgjbdcjlw31hjjk09zds3247vp")))

(define-public crate-leptess-0.13.4 (c (n "leptess") (v "0.13.4") (d (list (d (n "image") (r "^0.24.2") (d #t) (k 2)) (d (n "regex") (r "^1.4.3") (d #t) (k 2)) (d (n "tesseract-plumbing") (r "~0.7") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1sqhha22dc5sp3a0xpghv41rcrlp9acg3l4l868lns24jkbp3wfd")))

(define-public crate-leptess-0.14.0 (c (n "leptess") (v "0.14.0") (d (list (d (n "tesseract-plumbing") (r "~0.8") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "image") (r "^0.24.2") (d #t) (k 2)) (d (n "regex") (r "^1.4.3") (d #t) (k 2)))) (h "04vmk24h8xck3c0kcffkmapin0bpiip05l52vmkyc2r7sgin92df")))

