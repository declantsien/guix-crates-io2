(define-module (crates-io le pt leptos_workers_macro) #:use-module (crates-io))

(define-public crate-leptos_workers_macro-0.1.0 (c (n "leptos_workers_macro") (v "0.1.0") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0k8d3r6s4fmpb50rmn4bcnh8k8x1fgr54h7r6xx6f8ik09zvnxvn")))

(define-public crate-leptos_workers_macro-0.2.0 (c (n "leptos_workers_macro") (v "0.2.0") (d (list (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "indoc") (r "^2.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1av1yvhp8a8687x4l34lf6na0h8di62046bxaid3yb0xa46afr71") (f (quote (("ssr"))))))

(define-public crate-leptos_workers_macro-0.2.1 (c (n "leptos_workers_macro") (v "0.2.1") (d (list (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "indoc") (r "^2.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0mj4k0vkbj8ivd5iln1p7zx5p12dszjcvi2c7hy11r1a1vphgaj7") (f (quote (("ssr"))))))

