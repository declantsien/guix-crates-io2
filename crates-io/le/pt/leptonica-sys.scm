(define-module (crates-io le pt leptonica-sys) #:use-module (crates-io))

(define-public crate-leptonica-sys-0.1.0 (c (n "leptonica-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.49.2") (d #t) (k 1)))) (h "0j50s6dcirq2yp5sm1f0d03jkvlp3faplhx8z5jp22lz7dd0xwvx") (l "lept")))

(define-public crate-leptonica-sys-0.2.0 (c (n "leptonica-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.49.2") (d #t) (k 1)))) (h "1jrvnkpzil3m5hhbpgahcahf39cb24672h3dmj3wbw9i4sfazfpc") (l "lept")))

(define-public crate-leptonica-sys-0.2.1 (c (n "leptonica-sys") (v "0.2.1") (d (list (d (n "bindgen") (r "^0.50") (d #t) (k 1)))) (h "08mba4lnb83ygydq51scb3xhpr5wxphscjnsvdxdvddfn5rvm83l") (l "lept")))

(define-public crate-leptonica-sys-0.3.0 (c (n "leptonica-sys") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.52") (d #t) (k 1)))) (h "1j2pr5vc8ik4zadswxlak8rka1c73b0h2vaan12qsawx0hsfsjb7") (l "lept")))

(define-public crate-leptonica-sys-0.3.1 (c (n "leptonica-sys") (v "0.3.1") (d (list (d (n "bindgen") (r "^0.52") (d #t) (k 1)) (d (n "vcpkg") (r "^0.2.8") (d #t) (t "cfg(windows)") (k 1)))) (h "1r1kaqb6gibswqxpcdmwkw7f3p3rmg8abffbn174gng6y7fmi5w9") (l "lept")))

(define-public crate-leptonica-sys-0.3.2 (c (n "leptonica-sys") (v "0.3.2") (d (list (d (n "bindgen") (r "^0.54") (d #t) (k 1)) (d (n "vcpkg") (r "^0.2.8") (d #t) (t "cfg(windows)") (k 1)))) (h "1yjhjbl0kqmws69ngb0afcz2mfll5cbyadnrwi8hdxfar0yg22j3") (l "lept")))

(define-public crate-leptonica-sys-0.3.3 (c (n "leptonica-sys") (v "0.3.3") (d (list (d (n "bindgen") (r "^0.55") (d #t) (k 1)) (d (n "vcpkg") (r "^0.2.8") (d #t) (t "cfg(windows)") (k 1)))) (h "0g5h55wg3amhy7a3s09bbxkv84xrzvrv1xgbai4y2iyj4zalm4dl") (l "lept")))

(define-public crate-leptonica-sys-0.3.4 (c (n "leptonica-sys") (v "0.3.4") (d (list (d (n "bindgen") (r "^0.56") (d #t) (k 1)) (d (n "vcpkg") (r "^0.2.8") (d #t) (t "cfg(windows)") (k 1)))) (h "0pddy438yg3rp3jw3kihrl5awc4n63p5anlqglkn9m5j7ms922bq") (l "lept")))

(define-public crate-leptonica-sys-0.3.5 (c (n "leptonica-sys") (v "0.3.5") (d (list (d (n "bindgen") (r "^0.56") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.19") (d #t) (t "cfg(target_os = \"macos\")") (k 1)) (d (n "vcpkg") (r "^0.2.8") (d #t) (t "cfg(windows)") (k 1)))) (h "0kaq3s3fxkcbriw5rrbbbivyalb26d3h1dpabmw76ji9ii2kli0c") (l "lept")))

(define-public crate-leptonica-sys-0.4.0 (c (n "leptonica-sys") (v "0.4.0") (d (list (d (n "bindgen") (r "^0.56") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.19") (d #t) (t "cfg(any(target_os = \"macos\", target_os = \"linux\"))") (k 1)) (d (n "vcpkg") (r "^0.2.8") (d #t) (t "cfg(windows)") (k 1)))) (h "1lwhh5b75zzm051y982h2b7zn1d0n6c873fy18q6cx49slmimcrb") (l "lept")))

(define-public crate-leptonica-sys-0.4.1 (c (n "leptonica-sys") (v "0.4.1") (d (list (d (n "bindgen") (r "^0.56") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.19") (d #t) (t "cfg(any(target_os = \"macos\", target_os = \"linux\"))") (k 1)) (d (n "vcpkg") (r "^0.2.8") (d #t) (t "cfg(windows)") (k 1)))) (h "0dff44yac8sza4ld7wj29za1ssjxhrixfdmdvr252fa1jinyv3s1") (l "lept")))

(define-public crate-leptonica-sys-0.4.2 (c (n "leptonica-sys") (v "0.4.2") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.25") (d #t) (t "cfg(any(target_os = \"macos\", target_os = \"linux\"))") (k 1)) (d (n "vcpkg") (r "^0.2.15") (d #t) (t "cfg(windows)") (k 1)))) (h "1zzdi7bx63rca2hwqwg9v98923676kklim0qxyfq4281gm5ni5bf") (l "lept")))

(define-public crate-leptonica-sys-0.4.3 (c (n "leptonica-sys") (v "0.4.3") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.25") (d #t) (t "cfg(any(target_os = \"macos\", target_os = \"linux\"))") (k 1)) (d (n "vcpkg") (r "^0.2.15") (d #t) (t "cfg(windows)") (k 1)))) (h "1yg4wb170z01yd2adz2ring2qfgdqdgpwzz6x7zkvx1kyysh69c0") (l "lept")))

(define-public crate-leptonica-sys-0.4.4 (c (n "leptonica-sys") (v "0.4.4") (d (list (d (n "bindgen") (r "^0.64.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.25") (d #t) (t "cfg(any(target_os = \"macos\", target_os = \"linux\"))") (k 1)) (d (n "vcpkg") (r "^0.2.15") (d #t) (t "cfg(windows)") (k 1)))) (h "0bisc4db5hpxlkx7r78dllpd079kj1qzms614drhsppigycr46l1") (l "lept")))

(define-public crate-leptonica-sys-0.4.5 (c (n "leptonica-sys") (v "0.4.5") (d (list (d (n "bindgen") (r "^0.64.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.25") (d #t) (t "cfg(any(target_os = \"macos\", target_os = \"linux\"))") (k 1)) (d (n "vcpkg") (r "^0.2.15") (d #t) (t "cfg(windows)") (k 1)))) (h "0r62yqwp811y5cbg5b84pvyiblbm8ba9b9mzc9p6yqk5dw2lgp89") (l "lept")))

(define-public crate-leptonica-sys-0.4.6 (c (n "leptonica-sys") (v "0.4.6") (d (list (d (n "bindgen") (r "^0.64.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.25") (d #t) (t "cfg(any(target_os = \"macos\", target_os = \"linux\"))") (k 1)) (d (n "vcpkg") (r "^0.2.15") (d #t) (t "cfg(windows)") (k 1)))) (h "09mk3w2w9zci3j5v51vqg99mf4baib59knzq509424h15zfg3wzg") (l "lept")))

(define-public crate-leptonica-sys-0.4.7 (c (n "leptonica-sys") (v "0.4.7") (d (list (d (n "bindgen") (r "^0.64.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.25") (d #t) (t "cfg(any(target_os=\"macos\", target_os=\"linux\", target_os=\"freebsd\"))") (k 1)) (d (n "vcpkg") (r "^0.2.15") (d #t) (t "cfg(windows)") (k 1)))) (h "1zsbszk42f5y8p69ps25v9r3m0pfwq69hsjrsmi3sjcdzbassnik") (l "lept")))

