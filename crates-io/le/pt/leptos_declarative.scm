(define-module (crates-io le pt leptos_declarative) #:use-module (crates-io))

(define-public crate-leptos_declarative-0.1.0 (c (n "leptos_declarative") (v "0.1.0") (d (list (d (n "leptos") (r "^0.2") (d #t) (k 0)))) (h "10wh259b648mpgs5p5rhm2l0gnvxmr9n10w0j0d0n2r09gyydw04")))

(define-public crate-leptos_declarative-0.2.0 (c (n "leptos_declarative") (v "0.2.0") (d (list (d (n "leptos") (r "^0.3") (d #t) (k 0)))) (h "02bg5zyiirligqniaxdphqv65l6470kmzfngkcar7a0yc1w9lm83")))

(define-public crate-leptos_declarative-0.2.1 (c (n "leptos_declarative") (v "0.2.1") (d (list (d (n "leptos") (r "^0.3") (d #t) (k 0)))) (h "184jvpcag5ns4hvakmp0y8qh6acjd4205symlm8yrh555l5l5cpg")))

(define-public crate-leptos_declarative-0.3.0 (c (n "leptos_declarative") (v "0.3.0") (d (list (d (n "leptos") (r "^0.4") (d #t) (k 0)))) (h "0lcipi0ahaf1pp9bvg0pbcjshhbcvkasra5npk8rfbd51f6spnds")))

