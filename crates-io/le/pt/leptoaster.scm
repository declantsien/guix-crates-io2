(define-module (crates-io le pt leptoaster) #:use-module (crates-io))

(define-public crate-leptoaster-0.1.0 (c (n "leptoaster") (v "0.1.0") (d (list (d (n "gloo-timers") (r "^0.3.0") (f (quote ("futures"))) (d #t) (k 0)) (d (n "leptos") (r "^0.6.5") (f (quote ("csr" "nightly"))) (d #t) (k 0)))) (h "17xy8f3nx74knjbzsrxffk4492xspjvliq0nzyh9dbwhv3q0l6sb")))

(define-public crate-leptoaster-0.1.1 (c (n "leptoaster") (v "0.1.1") (d (list (d (n "gloo-timers") (r "^0.3.0") (f (quote ("futures"))) (d #t) (k 0)) (d (n "leptos") (r "^0.6.5") (f (quote ("csr" "nightly"))) (d #t) (k 0)))) (h "0lp2m0jxxbw9r1b443gms1w5wgpc3v5hbc2yb4an13h4dsf1n1rv")))

(define-public crate-leptoaster-0.1.2 (c (n "leptoaster") (v "0.1.2") (d (list (d (n "gloo-timers") (r "^0.3.0") (f (quote ("futures"))) (d #t) (k 0)) (d (n "leptos") (r "^0.6.5") (f (quote ("csr" "nightly"))) (d #t) (k 0)))) (h "04agvfz9dxvn7hvh4zwdndg66hqv134glzk4l4018x4cqisw2izv")))

(define-public crate-leptoaster-0.1.3 (c (n "leptoaster") (v "0.1.3") (d (list (d (n "gloo-timers") (r "^0.3.0") (f (quote ("futures"))) (d #t) (k 0)) (d (n "leptos") (r "^0.6.5") (f (quote ("csr" "nightly"))) (d #t) (k 0)))) (h "0vfny2az6x8axv13w8zjqx5vq2kld081599svmcg3v59dwd0wnbv")))

(define-public crate-leptoaster-0.1.4 (c (n "leptoaster") (v "0.1.4") (d (list (d (n "gloo-timers") (r "^0.3.0") (f (quote ("futures"))) (d #t) (k 0)) (d (n "leptos") (r "^0.6.5") (f (quote ("csr" "nightly"))) (d #t) (k 0)))) (h "078fijfwjd1xqmqz12c3cyi1fnic821lgix44b2xi72n2xbxhfma")))

(define-public crate-leptoaster-0.1.5 (c (n "leptoaster") (v "0.1.5") (d (list (d (n "gloo-timers") (r "^0.3.0") (f (quote ("futures"))) (d #t) (k 0)) (d (n "leptos") (r "^0.6.5") (f (quote ("csr" "nightly"))) (d #t) (k 0)))) (h "1qqxgssw06xfamacc2367kl3jbbwdzjn3cx9bzxlhm3r6glvbybr")))

(define-public crate-leptoaster-0.1.6 (c (n "leptoaster") (v "0.1.6") (d (list (d (n "gloo-timers") (r "^0.3.0") (f (quote ("futures"))) (d #t) (k 0)) (d (n "leptos") (r "^0.6.5") (f (quote ("csr" "nightly"))) (d #t) (k 0)))) (h "1dps1k1721qwlblqkmr76zsfn6zay5fkn6377y686awgw769y23x")))

(define-public crate-leptoaster-0.1.7 (c (n "leptoaster") (v "0.1.7") (d (list (d (n "gloo-timers") (r "^0.3.0") (f (quote ("futures"))) (d #t) (k 0)) (d (n "leptos") (r "^0.6.5") (f (quote ("csr" "nightly"))) (d #t) (k 0)))) (h "14sv44lfldjjxnms74clxg77mkmcb70z12r5dqwbg11rr7dkwr2m")))

(define-public crate-leptoaster-0.1.8 (c (n "leptoaster") (v "0.1.8") (d (list (d (n "gloo-timers") (r "^0.3.0") (f (quote ("futures"))) (d #t) (k 0)) (d (n "leptos") (r "^0.6.5") (f (quote ("csr" "nightly"))) (d #t) (k 0)))) (h "1rdc4nfc2ji8zlnk9gwgnsvk1bimbwjamxs74wvl1a5nafpxvmfq")))

