(define-module (crates-io le pt leptos_aria_button) #:use-module (crates-io))

(define-public crate-leptos_aria_button-0.0.0 (c (n "leptos_aria_button") (v "0.0.0") (d (list (d (n "leptos") (r "^0.1") (f (quote ("stable"))) (d #t) (k 0)) (d (n "leptos_aria_interactions") (r "^0.0.0") (d #t) (k 0)) (d (n "leptos_aria_utils") (r "^0.0.0") (d #t) (k 0)))) (h "1blxr1k9lvgz71l9xlzpl3jw856ndlp74v2dn3aay3031y4f5v6k") (r "1.67.0")))

