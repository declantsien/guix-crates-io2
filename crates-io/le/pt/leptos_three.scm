(define-module (crates-io le pt leptos_three) #:use-module (crates-io))

(define-public crate-leptos_three-0.0.0 (c (n "leptos_three") (v "0.0.0") (d (list (d (n "leptos") (r "^0.4.5") (f (quote ("csr"))) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.87") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.64") (d #t) (k 0)))) (h "0k21gwfgq5zc988gvm0kkssf2p1mbiblz4m36yvqlabrq4my31vb")))

