(define-module (crates-io le pt leptos_aria_interactions) #:use-module (crates-io))

(define-public crate-leptos_aria_interactions-0.0.0 (c (n "leptos_aria_interactions") (v "0.0.0") (d (list (d (n "leptos") (r "^0.1") (f (quote ("stable"))) (d #t) (k 0)) (d (n "leptos_aria_utils") (r "^0.0.0") (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("Navigator" "CssStyleDeclaration"))) (d #t) (k 0)))) (h "06m87hz7xi3m1j6w258kypq8lbli6mz5wf11pbii86vb4qvpjf0f") (r "1.67.0")))

