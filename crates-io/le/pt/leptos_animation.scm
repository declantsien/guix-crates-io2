(define-module (crates-io le pt leptos_animation) #:use-module (crates-io))

(define-public crate-leptos_animation-0.1.0 (c (n "leptos_animation") (v "0.1.0") (d (list (d (n "instant") (r "^0.1.12") (f (quote ("wasm-bindgen"))) (d #t) (k 0)) (d (n "leptos") (r "^0.3.1") (d #t) (k 0)))) (h "02r4a2mzypxvh851zfxbdql2k5fzlf84dkln8acdxsyyjm0jk3nh")))

(define-public crate-leptos_animation-0.2.0 (c (n "leptos_animation") (v "0.2.0") (d (list (d (n "instant") (r "^0.1.12") (f (quote ("wasm-bindgen"))) (d #t) (k 0)) (d (n "leptos") (r "^0.4.3") (d #t) (k 0)))) (h "0cmxz3x42sdi5ljkyd1lsb8yi5sc8xazw7ycn8yl73nbbhy1p69n")))

(define-public crate-leptos_animation-0.3.0 (c (n "leptos_animation") (v "0.3.0") (d (list (d (n "instant") (r "^0.1.12") (f (quote ("wasm-bindgen"))) (d #t) (k 0)) (d (n "leptos") (r "^0.5.1") (d #t) (k 0)))) (h "18r9m7qhdzi75f741hj3zrzf2c48vnyrldj5h7zfwf7iqs2n28ni")))

(define-public crate-leptos_animation-0.4.0 (c (n "leptos_animation") (v "0.4.0") (d (list (d (n "instant") (r "^0.1.12") (f (quote ("wasm-bindgen"))) (d #t) (k 0)) (d (n "leptos") (r "^0.5.1") (d #t) (k 0)))) (h "0knqm205pvphm14i0qz407hixgsjk81cszj3q1v5pkm0x8mvlzp8")))

(define-public crate-leptos_animation-0.5.0 (c (n "leptos_animation") (v "0.5.0") (d (list (d (n "instant") (r "^0.1") (f (quote ("wasm-bindgen"))) (d #t) (k 0)) (d (n "leptos") (r "^0.6") (f (quote ("nightly"))) (d #t) (k 0)))) (h "11vl7d7xwhkqqj47w597l2v4phzbpn27chf0z0hyvwyj7q2gr5hf")))

(define-public crate-leptos_animation-0.5.1 (c (n "leptos_animation") (v "0.5.1") (d (list (d (n "instant") (r "^0.1") (f (quote ("wasm-bindgen"))) (d #t) (k 0)) (d (n "leptos") (r "^0.6") (f (quote ("nightly"))) (d #t) (k 0)))) (h "18qgwnph2na69x82c3w7rvxrhh6f0hwlykjwqcfvsw7l5h9bfw8p")))

(define-public crate-leptos_animation-0.5.2 (c (n "leptos_animation") (v "0.5.2") (d (list (d (n "instant") (r "^0.1") (f (quote ("wasm-bindgen"))) (d #t) (k 0)) (d (n "leptos") (r "^0.6") (d #t) (k 0)))) (h "1p8kp1phwkkpiivs9iv5ay806ips7b7z4s17jh758krrimy4lh33")))

