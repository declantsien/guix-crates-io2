(define-module (crates-io le pt leptrino-force-torque-sensor) #:use-module (crates-io))

(define-public crate-leptrino-force-torque-sensor-0.1.0 (c (n "leptrino-force-torque-sensor") (v "0.1.0") (d (list (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "pair_macro") (r "^0.1.4") (d #t) (k 0)) (d (n "serialport") (r "^4.0.0") (d #t) (k 0)))) (h "11vhn4c3kcgh6z7gqxqbnk6g3d71ibnwl3l2b4jyanl5b6cifq0n")))

