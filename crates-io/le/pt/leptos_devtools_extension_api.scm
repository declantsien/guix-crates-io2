(define-module (crates-io le pt leptos_devtools_extension_api) #:use-module (crates-io))

(define-public crate-leptos_devtools_extension_api-0.0.1 (c (n "leptos_devtools_extension_api") (v "0.0.1") (d (list (d (n "js-sys") (r "^0.3.64") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-wasm-bindgen") (r "^0.6.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 2)) (d (n "wasm-bindgen") (r "^0.2.87") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.64") (f (quote ("console" "Window" "MessageEvent"))) (d #t) (k 0)))) (h "1c8c4r5mqqxnp3502y9v1aqr84hyakzr2dj1kz7cirhh0y1f5blx")))

