(define-module (crates-io le pt lepton) #:use-module (crates-io))

(define-public crate-lepton-0.1.0 (c (n "lepton") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "19ilnbjxkg4mws4wfj5d1876xq6gmjr15gh0qscwfjbl57ghq99d")))

