(define-module (crates-io le pt leptos-chartistry) #:use-module (crates-io))

(define-public crate-leptos-chartistry-0.1.0 (c (n "leptos-chartistry") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "leptos") (r "^0.6") (f (quote ("csr"))) (d #t) (k 0)) (d (n "leptos-use") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("DomRectReadOnly"))) (d #t) (k 0)))) (h "037j6zfakq6kzsb0snkzp6lq6bscpvx38cz8sqlw95s7sla5w3pa")))

(define-public crate-leptos-chartistry-0.1.1 (c (n "leptos-chartistry") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "leptos") (r "^0.6") (f (quote ("csr"))) (d #t) (k 0)) (d (n "leptos-use") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("DomRectReadOnly"))) (d #t) (k 0)))) (h "07f7z8dgmqfiivfbknjilnsdhng6j2smal0jz2lbxpdaqq1lj2xk")))

(define-public crate-leptos-chartistry-0.1.2 (c (n "leptos-chartistry") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "leptos") (r "^0.6") (d #t) (k 0)) (d (n "leptos-use") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("DomRectReadOnly"))) (d #t) (k 0)))) (h "0bfc5d1ads6zmfb4x40nwgdbijkqhydqrz7qcia2shjl55wjpnja")))

(define-public crate-leptos-chartistry-0.1.3 (c (n "leptos-chartistry") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "leptos") (r "^0.6") (d #t) (k 0)) (d (n "leptos-use") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("DomRectReadOnly"))) (d #t) (k 0)))) (h "1yya3x5k37mi8awyymxqq9a6anrabip6a57x8cadp56pdpbfg8rz")))

(define-public crate-leptos-chartistry-0.1.4 (c (n "leptos-chartistry") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "leptos") (r "^0.6") (d #t) (k 0)) (d (n "leptos-use") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("DomRectReadOnly"))) (d #t) (k 0)))) (h "17brhsfx2rfwx5xj2jv7yljn8cf8azdaircnicl957ydg8r1086y")))

(define-public crate-leptos-chartistry-0.1.5 (c (n "leptos-chartistry") (v "0.1.5") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "leptos") (r "^0.6") (d #t) (k 0)) (d (n "leptos-use") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("DomRectReadOnly"))) (d #t) (k 0)))) (h "1kghm0sw8i3zp49mg0xlhsw1022v7lj661k5c8s8maqnw3zcxnb8")))

