(define-module (crates-io le pt leptos_transition_flip) #:use-module (crates-io))

(define-public crate-leptos_transition_flip-0.1.0 (c (n "leptos_transition_flip") (v "0.1.0") (d (list (d (n "leptos") (r "^0.5.2") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.64") (f (quote ("Element" "DomRect"))) (d #t) (k 0)))) (h "03vfg21kwmjs622hqz76bs7gj2n19mw983mpkx2hv5yiiyp9bb7w")))

