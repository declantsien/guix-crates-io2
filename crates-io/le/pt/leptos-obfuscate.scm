(define-module (crates-io le pt leptos-obfuscate) #:use-module (crates-io))

(define-public crate-leptos-obfuscate-0.1.0 (c (n "leptos-obfuscate") (v "0.1.0") (d (list (d (n "leptos") (r "^0.5") (d #t) (k 0)))) (h "12f3yv4dl8dkjcsayih6vl9w14paxpvl9406b12p5zv267pgy3c4") (r "1.70.0")))

(define-public crate-leptos-obfuscate-0.1.1 (c (n "leptos-obfuscate") (v "0.1.1") (d (list (d (n "leptos") (r "^0.5") (d #t) (k 0)))) (h "1m6zf85w019ql87qyf4219g7zc4di4mbh7jdp0l9bxw5apjv1rvd") (r "1.70.0")))

(define-public crate-leptos-obfuscate-0.2.0 (c (n "leptos-obfuscate") (v "0.2.0") (d (list (d (n "leptos") (r "^0.6.5") (d #t) (k 0)))) (h "18js55qhswxhmk4cwjjzs7c9xl8kb1m155bvcrdicdj2ny0ngaxy") (r "1.75.0")))

