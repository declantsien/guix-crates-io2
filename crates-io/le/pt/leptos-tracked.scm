(define-module (crates-io le pt leptos-tracked) #:use-module (crates-io))

(define-public crate-leptos-tracked-0.1.0 (c (n "leptos-tracked") (v "0.1.0") (d (list (d (n "leptos_reactive") (r "^0.2.1") (d #t) (k 0)))) (h "0j54r2gw7k4rpbvxxdkdd6xwxaci08jy2fhh5d4zrsgcpzbzq866")))

(define-public crate-leptos-tracked-0.1.1 (c (n "leptos-tracked") (v "0.1.1") (d (list (d (n "leptos_reactive") (r "^0.2.1") (d #t) (k 0)))) (h "0hnl584fhd5v71lnj422g0vjybzfj5caw1lhzn58bd9f86ajxkd8")))

(define-public crate-leptos-tracked-0.1.2 (c (n "leptos-tracked") (v "0.1.2") (d (list (d (n "leptos_reactive") (r "^0.2.1") (d #t) (k 0)))) (h "0fwgxsrar5an4nfcihqq7ghdgjdpjcr91ydcv15kxwr8iyl9xp16")))

(define-public crate-leptos-tracked-0.1.3 (c (n "leptos-tracked") (v "0.1.3") (d (list (d (n "leptos_reactive") (r "^0.2.1") (d #t) (k 0)))) (h "0kawxqxd2mkm1zqgfwsjsjsf106m0af5h6ydcdaawg01ypcinsxf")))

(define-public crate-leptos-tracked-0.1.4 (c (n "leptos-tracked") (v "0.1.4") (d (list (d (n "leptos_reactive") (r "^0.2.1") (d #t) (k 0)))) (h "140dyvgxj8h3i3qg5jf8zhv5j902p8nb0snxkss4n1d0f8ksizca")))

