(define-module (crates-io le pt leptos-mview-core) #:use-module (crates-io))

(define-public crate-leptos-mview-core-0.1.0 (c (n "leptos-mview-core") (v "0.1.0") (d (list (d (n "proc-macro-error") (r "^1") (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "12x4iqjx7vaamsyrdg925166ycvp2idxrw2szhgz632wx2wivcww")))

(define-public crate-leptos-mview-core-0.2.0 (c (n "leptos-mview-core") (v "0.2.0") (d (list (d (n "proc-macro-error") (r "^1") (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0jd3d0zdbswls433jw7aq0xw03vakrzy387bw0bj4csgy1la1nmm")))

(define-public crate-leptos-mview-core-0.2.1 (c (n "leptos-mview-core") (v "0.2.1") (d (list (d (n "proc-macro-error") (r "^1") (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0sayhxj3wyyl1zqgaf964hfc722b10xvy4ipzg4lpwl82qdhgk3k")))

(define-public crate-leptos-mview-core-0.2.2 (c (n "leptos-mview-core") (v "0.2.2") (d (list (d (n "proc-macro-error") (r "^1") (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1slhv34amfcn0vspgl27b7sgvz90ccp9zb2vyfh6lpi8a9finsx7")))

(define-public crate-leptos-mview-core-0.2.3 (c (n "leptos-mview-core") (v "0.2.3") (d (list (d (n "proc-macro-error") (r "^1") (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1ci440y1l2bps33bvdcmsw4aaxj6n3fx8ckhabicby7npy52wdxg")))

(define-public crate-leptos-mview-core-0.3.0 (c (n "leptos-mview-core") (v "0.3.0") (d (list (d (n "proc-macro-error") (r "^1") (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0j89y1kgnnndlpjcrxc9r1nhh570k57miqd3pqwiacpxjhc89g65")))

(define-public crate-leptos-mview-core-0.3.1 (c (n "leptos-mview-core") (v "0.3.1") (d (list (d (n "proc-macro-error") (r "^1") (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0fvv7sacsd2ylzl44bxnn91gdxf55qxwbxg9wxwggf1g39wwwnav")))

