(define-module (crates-io le pt leptos_darkmode) #:use-module (crates-io))

(define-public crate-leptos_darkmode-0.1.0 (c (n "leptos_darkmode") (v "0.1.0") (d (list (d (n "leptos") (r "^0.5") (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("MediaQueryList" "Storage"))) (k 0)))) (h "04dkfbz08wqy8w70wcpajqnwknnd99x0h87k33nh78s74vqnzn8a")))

(define-public crate-leptos_darkmode-0.1.1 (c (n "leptos_darkmode") (v "0.1.1") (d (list (d (n "leptos") (r "^0.5") (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("MediaQueryList" "Storage"))) (k 0)))) (h "0r6r9qlh5hdv3jq77cca8r1nmwy1rv8s78ar7hwlbcm7ilximhrz")))

(define-public crate-leptos_darkmode-0.1.2 (c (n "leptos_darkmode") (v "0.1.2") (d (list (d (n "leptos") (r "^0.5") (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("MediaQueryList" "Storage"))) (k 0)))) (h "10sjrlxnpfdjbv6q33i6bxzfw7a452p9hihli6vxx92a6m0ji9v9")))

(define-public crate-leptos_darkmode-0.1.3 (c (n "leptos_darkmode") (v "0.1.3") (d (list (d (n "leptos") (r "^0.5") (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("MediaQueryList" "Storage"))) (k 0)))) (h "1s4lix0cqds1fvj9n2i4xbicpw0kw6hl2l9vhhh97v47g1iyy9h8")))

(define-public crate-leptos_darkmode-0.1.4 (c (n "leptos_darkmode") (v "0.1.4") (d (list (d (n "leptos") (r "^0.5") (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("MediaQueryList" "Storage"))) (k 0)))) (h "0f5ycfzpqx9jk53bx4rqrl52wi1lcz8b84p9qi3rjmdnjasyx2p5")))

(define-public crate-leptos_darkmode-0.1.5 (c (n "leptos_darkmode") (v "0.1.5") (d (list (d (n "leptos") (r "^0.5") (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("MediaQueryList" "Storage"))) (k 0)))) (h "1yhdy58czrjb40qr5xapmzjk96r00xyj970ljvvgzcjk7f28a577")))

(define-public crate-leptos_darkmode-0.1.6 (c (n "leptos_darkmode") (v "0.1.6") (d (list (d (n "leptos") (r "^0.5") (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("MediaQueryList" "Storage"))) (k 0)))) (h "0jcrllksn7cnb08snyqma4sqn5bxyrkz9dmsp8jiv4yq2yd5kf14")))

(define-public crate-leptos_darkmode-0.2.0 (c (n "leptos_darkmode") (v "0.2.0") (d (list (d (n "leptos") (r "^0.6") (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("MediaQueryList" "Storage"))) (k 0)))) (h "0lv2m5i9abjwaqxgihfv5aw1wr0dnwncr04nfc5dl4mkary7rv1m")))

