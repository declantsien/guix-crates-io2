(define-module (crates-io le pt leptos_tea_macros) #:use-module (crates-io))

(define-public crate-leptos_tea_macros-0.1.0 (c (n "leptos_tea_macros") (v "0.1.0") (d (list (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1kp18n63g3ak3285kkfb5dn2jagidj11sdzs5dmiqvfhrc7byidw")))

(define-public crate-leptos_tea_macros-0.1.1 (c (n "leptos_tea_macros") (v "0.1.1") (d (list (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "00i4padcr6xj0r2f1pyg9j637a806pighrym22agwd75pvg8091b")))

(define-public crate-leptos_tea_macros-0.2.0 (c (n "leptos_tea_macros") (v "0.2.0") (d (list (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0qv69m80b1jlzanmii0lpjzg458v0703mc2bhprqb5ifz2kigk4z")))

(define-public crate-leptos_tea_macros-0.2.1 (c (n "leptos_tea_macros") (v "0.2.1") (d (list (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1cys4vkxrc01h408hr9d2dn511r04pl9c9j9c53xcgbqp7ac8hj6")))

(define-public crate-leptos_tea_macros-0.2.2 (c (n "leptos_tea_macros") (v "0.2.2") (d (list (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1qc250cgj8x29b32ydxflw156m637i8jzf480jp1p0ajscd5kl4b") (y #t)))

(define-public crate-leptos_tea_macros-0.2.3 (c (n "leptos_tea_macros") (v "0.2.3") (d (list (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0p20djx3sn5210lkj3p8yvalrp5g70iby4xlwf7p958q0civw1xy")))

(define-public crate-leptos_tea_macros-0.3.0 (c (n "leptos_tea_macros") (v "0.3.0") (d (list (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "17l8k4fbbalc4scvh86dcxpxkbm8sxw12qp9r2vlb42932rbpjw4")))

(define-public crate-leptos_tea_macros-0.4.0 (c (n "leptos_tea_macros") (v "0.4.0") (d (list (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "06fms8j9qg0r7s22ifjl370094d6jgma68viyiv0ras9076rzhhi")))

(define-public crate-leptos_tea_macros-0.5.0 (c (n "leptos_tea_macros") (v "0.5.0") (d (list (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1jyzrdg0gy85xyqphb74bnxm58dv9i1rji1fbblkqn6rnfc0abq1") (y #t)))

(define-public crate-leptos_tea_macros-0.5.1 (c (n "leptos_tea_macros") (v "0.5.1") (d (list (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1c0r043nl1kagmh3xz27qgmi729nzf83n8n250amdk14bvjgp4a6") (y #t)))

(define-public crate-leptos_tea_macros-0.5.2 (c (n "leptos_tea_macros") (v "0.5.2") (d (list (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "04njqd6rgx2lg2gs375ncak3vcy1p82zskwnyl8l1q7fgylrar19")))

(define-public crate-leptos_tea_macros-0.6.0-beta.1 (c (n "leptos_tea_macros") (v "0.6.0-beta.1") (d (list (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1jffvkskxw7qkp4hv8frq25ywmray3b34cpv0ipg6s0vyyhnpz3a")))

(define-public crate-leptos_tea_macros-0.6.0-rc1 (c (n "leptos_tea_macros") (v "0.6.0-rc1") (d (list (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0qqjj4aj08mvrj9ax9himj3vbg43y4745lw7bj0p7va9dxxld3ga")))

