(define-module (crates-io le pt leptos-mview-macro) #:use-module (crates-io))

(define-public crate-leptos-mview-macro-0.1.0 (c (n "leptos-mview-macro") (v "0.1.0") (d (list (d (n "leptos-mview-core") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)))) (h "0yx5wfy1hxi8sq367mzkfx834l0pwqvh5c3kx4svn115zgbms4hr")))

(define-public crate-leptos-mview-macro-0.2.0 (c (n "leptos-mview-macro") (v "0.2.0") (d (list (d (n "leptos-mview-core") (r "^0.2.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)))) (h "0y5fg2pl2q3612j9ddr52gp56k3wx5xb0lqajvnq13rh9kkjf06a")))

(define-public crate-leptos-mview-macro-0.2.1 (c (n "leptos-mview-macro") (v "0.2.1") (d (list (d (n "leptos-mview-core") (r "^0.2.1") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)))) (h "0i53d29h19jjrc5iqarm9n7xqq7rgx249j7nv8m841sbarm768jw")))

(define-public crate-leptos-mview-macro-0.2.2 (c (n "leptos-mview-macro") (v "0.2.2") (d (list (d (n "leptos-mview-core") (r "^0.2.2") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)))) (h "17r4kkb30naivq9m0n3aqa47l9j4mn9159ldnrxb3q7mzgc6ga5w")))

(define-public crate-leptos-mview-macro-0.2.3 (c (n "leptos-mview-macro") (v "0.2.3") (d (list (d (n "leptos-mview-core") (r "^0.2.3") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)))) (h "1xmph5kk3h62bm1cfv3b1i3lcyvb3jmw8sskvnq0g5qiqzhpb6l2")))

(define-public crate-leptos-mview-macro-0.3.0 (c (n "leptos-mview-macro") (v "0.3.0") (d (list (d (n "leptos-mview-core") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)))) (h "09jal8a8q839sf1chyp6lmx2pj6v2zx627lf4viin2pv4ly0fym3")))

(define-public crate-leptos-mview-macro-0.3.1 (c (n "leptos-mview-macro") (v "0.3.1") (d (list (d (n "leptos-mview-core") (r "^0.3.1") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)))) (h "172ix4yh8rwbm6h5y2s4jvkjh9k513gid7qs5kmvwz5039ymb5yc")))

