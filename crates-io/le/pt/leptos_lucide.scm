(define-module (crates-io le pt leptos_lucide) #:use-module (crates-io))

(define-public crate-leptos_lucide-0.1.0 (c (n "leptos_lucide") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 1)) (d (n "leptos") (r "^0.6.9") (d #t) (k 0)) (d (n "leptos") (r "^0.6.9") (d #t) (k 1)) (d (n "leptos_dom") (r "^0.6.9") (d #t) (k 0)) (d (n "leptos_dom") (r "^0.6.9") (d #t) (k 1)) (d (n "wasm-bindgen-test") (r "^0.3.42") (d #t) (k 2)))) (h "09m4q18cc9ck116lpvvxym9cb0lzs7n5d09d1ifx5y5wpap2x359")))

(define-public crate-leptos_lucide-0.1.1 (c (n "leptos_lucide") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 1)) (d (n "leptos") (r "^0.6.9") (d #t) (k 0)) (d (n "leptos") (r "^0.6.9") (d #t) (k 1)) (d (n "leptos_dom") (r "^0.6.9") (d #t) (k 0)) (d (n "leptos_dom") (r "^0.6.9") (d #t) (k 1)) (d (n "wasm-bindgen-test") (r "^0.3.42") (d #t) (k 2)))) (h "1x0cqpa9hi0swv32flv0xajglb12dfi366nn95j8arxrs2bcbnmc")))

