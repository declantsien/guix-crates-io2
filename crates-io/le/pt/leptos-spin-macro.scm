(define-module (crates-io le pt leptos-spin-macro) #:use-module (crates-io))

(define-public crate-leptos-spin-macro-0.1.0 (c (n "leptos-spin-macro") (v "0.1.0") (d (list (d (n "http") (r "^1.0.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "server_fn_macro") (r "^0.6.3") (d #t) (k 0)) (d (n "spin-sdk") (r "^2.1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (d #t) (k 0)))) (h "098lkk48pvws05qxcazky4dl2rnim0icvp6j5hs0dr0z29zg71kw") (f (quote (("ssr" "server_fn_macro/ssr") ("nightly" "server_fn_macro/nightly") ("axum" "server_fn_macro/axum") ("actix" "server_fn_macro/actix"))))))

(define-public crate-leptos-spin-macro-0.2.0 (c (n "leptos-spin-macro") (v "0.2.0") (d (list (d (n "http") (r "^1.0.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "server_fn_macro") (r "^0.6.3") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (d #t) (k 0)))) (h "1bvw6nn7yqisf1r4hmd91pkvj585md38i36awcb8243ik1xs5zl7") (f (quote (("ssr" "server_fn_macro/ssr") ("nightly" "server_fn_macro/nightly") ("axum" "server_fn_macro/axum") ("actix" "server_fn_macro/actix"))))))

