(define-module (crates-io le pt leptosfmt-pretty-printer) #:use-module (crates-io))

(define-public crate-leptosfmt-pretty-printer-0.1.2 (c (n "leptosfmt-pretty-printer") (v "0.1.2") (h "1m36jd0vv08b6qsdm0ffxin9xcaa40z6qm14ail0m4p74myvmw3g")))

(define-public crate-leptosfmt-pretty-printer-0.1.3 (c (n "leptosfmt-pretty-printer") (v "0.1.3") (h "0fnb8pdxfp8f98xlk32zs4fqk1cin72lvs6yjs05m19nf9g5fpsa")))

(define-public crate-leptosfmt-pretty-printer-0.1.4 (c (n "leptosfmt-pretty-printer") (v "0.1.4") (h "1s58cwkirsclhvxvsymmllyyg6h7ls670wn1zjndss5d1489hvcn")))

(define-public crate-leptosfmt-pretty-printer-0.1.5 (c (n "leptosfmt-pretty-printer") (v "0.1.5") (h "157wy29izvyri0259fvh6r1lpzqymcd0n139kvz1w4fw20j15n7d")))

(define-public crate-leptosfmt-pretty-printer-0.1.6 (c (n "leptosfmt-pretty-printer") (v "0.1.6") (h "0hdhsl1ylk9g08zlka8cp8lf561y4hd3n340pqv6wy54r9dhsmxw")))

(define-public crate-leptosfmt-pretty-printer-0.1.7 (c (n "leptosfmt-pretty-printer") (v "0.1.7") (h "0iyn9irx3ymxyamij0wa32bn69knqw03v6bj21ax3ylkxl050ix0")))

(define-public crate-leptosfmt-pretty-printer-0.1.8 (c (n "leptosfmt-pretty-printer") (v "0.1.8") (h "1aqizvndlfmkh5f6164plv7qs57ja9vl4xvf1mjl5whd92ycyx5y")))

(define-public crate-leptosfmt-pretty-printer-0.1.18 (c (n "leptosfmt-pretty-printer") (v "0.1.18") (h "1z1iindds6gp70hpml6ql9n0xb6a2sk56382j27a4s269fi25lfr")))

