(define-module (crates-io le pt leptos_macabre) #:use-module (crates-io))

(define-public crate-leptos_macabre-0.1.0 (c (n "leptos_macabre") (v "0.1.0") (d (list (d (n "indoc") (r "^2.0.4") (d #t) (k 2)) (d (n "leptos_dom") (r "^0.5.4") (f (quote ("ssr" "experimental-islands"))) (d #t) (k 0)))) (h "1a34hzqw0c43kc4hjihhcqgzhk22r18444sknzqpr34yylxpnwmk")))

