(define-module (crates-io le pt leptos-remix-icon) #:use-module (crates-io))

(define-public crate-leptos-remix-icon-1.0.0 (c (n "leptos-remix-icon") (v "1.0.0") (d (list (d (n "leptos") (r "^0.6.11") (d #t) (k 0)))) (h "1821p6lh8lv9g96wl3mpxyipzlz0sphgq8x0xlkksr55j2zxh9mk")))

(define-public crate-leptos-remix-icon-1.0.1 (c (n "leptos-remix-icon") (v "1.0.1") (d (list (d (n "leptos") (r "^0.6.11") (d #t) (k 0)))) (h "03ji8sdd10hhnl9hm3hypanhi88vvqs60dwfha99ql54y667vf0c")))

(define-public crate-leptos-remix-icon-1.0.2 (c (n "leptos-remix-icon") (v "1.0.2") (d (list (d (n "leptos") (r "^0.6.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.202") (f (quote ("derive"))) (d #t) (k 0)))) (h "1ciai3ny42fcvhcwyq0pdzf861jm7ynjh7zc9kcyrfi25vam2vd1")))

(define-public crate-leptos-remix-icon-1.0.3 (c (n "leptos-remix-icon") (v "1.0.3") (d (list (d (n "leptos") (r "^0.6.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.202") (f (quote ("derive"))) (d #t) (k 0)))) (h "1s1kr0jyichr58smqx1ir1n93pvvcyv63q4mzpff1yc20arxc63c")))

