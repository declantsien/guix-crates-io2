(define-module (crates-io le ic leicester) #:use-module (crates-io))

(define-public crate-leicester-0.0.0 (c (n "leicester") (v "0.0.0") (d (list (d (n "rust-iptables") (r "^0.0.2") (d #t) (k 0)))) (h "02ygyr1l45q59cbcaar0ymxq14m2c9lqrd9i1w42dlx14fliyn7g")))

(define-public crate-leicester-0.0.1 (c (n "leicester") (v "0.0.1") (d (list (d (n "rust-iptables") (r "^0.0.2") (d #t) (k 0)))) (h "1p8nysqzkj5grqs320qsnz27p4l7xf9h5h4lh0z20dzqndan9qsk")))

