(define-module (crates-io le va levarint64) #:use-module (crates-io))

(define-public crate-levarint64-0.2.0 (c (n "levarint64") (v "0.2.0") (h "1c4frkma23n34d8cjnx17bdprac6qhhi72nrd3nnffiq1irlr16r")))

(define-public crate-levarint64-0.2.2 (c (n "levarint64") (v "0.2.2") (h "1iig6r7idkz9qwx9bmxzlk9iav4c6a0jw88398c6p72qsbcvcszf")))

(define-public crate-levarint64-0.2.4 (c (n "levarint64") (v "0.2.4") (h "034axzl8j5d0y9lc01kyjbh6smwni5814mvzms0vll44gnggs51r")))

(define-public crate-levarint64-0.2.6 (c (n "levarint64") (v "0.2.6") (h "00adnzim1g0ikpc45zhafa4jlvjwvsf1n90ip33lh1mb89ysdk79")))

(define-public crate-levarint64-0.2.8 (c (n "levarint64") (v "0.2.8") (h "13kdznf80jq91pz9xar62bj6s52bbwn7qvyjs1g0b6qgrmag9n6h")))

