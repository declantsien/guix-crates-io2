(define-module (crates-io le af leafers) #:use-module (crates-io))

(define-public crate-leafers-0.1.0 (c (n "leafers") (v "0.1.0") (d (list (d (n "chakracore") (r "^0.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.5.1") (d #t) (k 0)) (d (n "rental") (r "^0.5.0") (d #t) (k 0)))) (h "01l860v4q97i6v1mz5bxq853yb7nq503dkfphy6bf3mx4jb2jhxi") (y #t)))

