(define-module (crates-io le af leaflet) #:use-module (crates-io))

(define-public crate-leaflet-0.0.0 (c (n "leaflet") (v "0.0.0") (d (list (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)))) (h "0lzgmg1vy7bwjr64m0fc3gpy7w2zyr699sx96c0cxp746wai2hfh")))

(define-public crate-leaflet-0.1.0 (c (n "leaflet") (v "0.1.0") (d (list (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)))) (h "1ryv43qpgs28xbxarrp7368rgfmwrrhgm8dq2y9w7c5f8w4y6qlf")))

(define-public crate-leaflet-0.1.1 (c (n "leaflet") (v "0.1.1") (d (list (d (n "js-sys") (r "^0.3") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("Event" "HtmlElement"))) (d #t) (k 0)))) (h "1hwp64zs8m41k6z34dh8q8r3chxxgms77825znl70wglvy3y439n")))

(define-public crate-leaflet-0.1.2 (c (n "leaflet") (v "0.1.2") (d (list (d (n "js-sys") (r "^0.3") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("Event" "HtmlElement"))) (d #t) (k 0)))) (h "1sa2kr04y8xdawv01d281jr7qcsga00mdz4ff6fk22zhwj1907sq")))

(define-public crate-leaflet-0.1.3 (c (n "leaflet") (v "0.1.3") (d (list (d (n "js-sys") (r "^0.3") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("Event" "HtmlElement"))) (d #t) (k 0)))) (h "0syq3fa92igaaj009mzsvv41wgmar45ndvbr8fhbngdn81s2v5cd")))

(define-public crate-leaflet-0.2.0 (c (n "leaflet") (v "0.2.0") (d (list (d (n "js-sys") (r "^0.3") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("Event" "HtmlElement"))) (d #t) (k 0)))) (h "00z80wi5ldqqag3rj987wrnhcagn80b03rj6zr48g19c9qmvmjll")))

(define-public crate-leaflet-0.2.1 (c (n "leaflet") (v "0.2.1") (d (list (d (n "js-sys") (r "^0.3") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("Event" "HtmlElement"))) (d #t) (k 0)))) (h "04195s3g5vj39ys7mbg79ibq4calrha2qzl0mip2ca87fgzacfqj")))

(define-public crate-leaflet-0.2.2 (c (n "leaflet") (v "0.2.2") (d (list (d (n "js-sys") (r "^0.3") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("Event" "HtmlElement"))) (d #t) (k 0)))) (h "042z9v8q1ncjzld40vawnwg9cxf06ngzj7pqky8cncwlch8ylb8r")))

(define-public crate-leaflet-0.2.3 (c (n "leaflet") (v "0.2.3") (d (list (d (n "js-sys") (r "^0.3") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("Event" "HtmlElement"))) (d #t) (k 0)))) (h "1bbs394r86vbq3pxaz772h4mj5rf2dc482v6i01rprjlyihba3wi")))

(define-public crate-leaflet-0.3.0 (c (n "leaflet") (v "0.3.0") (d (list (d (n "js-sys") (r "^0.3") (d #t) (k 0)) (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("Event" "HtmlElement" "MouseEvent"))) (d #t) (k 0)))) (h "1zmfylk5zpcz2b1rhnm06w9qa6x2rwjk7kj0daqq7m2c90fzmra8")))

(define-public crate-leaflet-0.4.0 (c (n "leaflet") (v "0.4.0") (d (list (d (n "js-sys") (r "^0.3") (d #t) (k 0)) (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("Event" "HtmlElement" "MouseEvent"))) (d #t) (k 0)))) (h "1qsbkjd9hrpx3cdj7w67lxx9f1k5vkv5l2nm72n184vrri74zjpi")))

(define-public crate-leaflet-0.4.1 (c (n "leaflet") (v "0.4.1") (d (list (d (n "js-sys") (r "^0.3") (d #t) (k 0)) (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("Event" "HtmlElement" "MouseEvent"))) (d #t) (k 0)))) (h "05qx4wxdmzkn5nbk3v3hnw14xmpk7a3fa4z3s9vr7nkkkvmhfn1n") (r "1.75")))

