(define-module (crates-io le af leafslug_time) #:use-module (crates-io))

(define-public crate-leafslug_time-0.1.0 (c (n "leafslug_time") (v "0.1.0") (d (list (d (n "assert2") (r "^0.3.11") (d #t) (k 2)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)) (d (n "time") (r "^0.3.31") (f (quote ("serde" "local-offset" "serde-well-known"))) (d #t) (k 0)))) (h "0x64dq8klgq323lkakknrdxdk1vg88ihd7m663al68ism81x05c8")))

(define-public crate-leafslug_time-0.4.1 (c (n "leafslug_time") (v "0.4.1") (d (list (d (n "assert2") (r "^0.3.11") (d #t) (k 2)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)) (d (n "time") (r "^0.3.31") (f (quote ("serde" "local-offset" "serde-well-known"))) (d #t) (k 0)))) (h "1rq9kzrqvijrnqikx67cjga7a9mgwcrp18p8sjq062s3bhjpxdwy")))

