(define-module (crates-io le af leafwing_abilities_macros) #:use-module (crates-io))

(define-public crate-leafwing_abilities_macros-0.1.0 (c (n "leafwing_abilities_macros") (v "0.1.0") (d (list (d (n "proc-macro-crate") (r "^1.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0112acsaawa8icdbwf1jmrq0yvwhx5w8y38x4awnpfxgc67k4fhg")))

(define-public crate-leafwing_abilities_macros-0.3.0 (c (n "leafwing_abilities_macros") (v "0.3.0") (d (list (d (n "proc-macro-crate") (r "^1.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "18g0sh28ikn8bpqid665yp8r7zayalffbv29zy1xb8790fgxsnnb")))

