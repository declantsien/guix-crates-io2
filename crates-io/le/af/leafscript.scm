(define-module (crates-io le af leafscript) #:use-module (crates-io))

(define-public crate-leafscript-0.1.0 (c (n "leafscript") (v "0.1.0") (h "1qrrwkh91580drnp00ymiin5p9j2j89mjy1z6c0ajll3qwj09aq2") (y #t)))

(define-public crate-leafscript-0.1.1 (c (n "leafscript") (v "0.1.1") (h "10md0mkaavmkypy5fn5d0q55vpsmmrwncfqq1baxwyaqr3x2s44j")))

