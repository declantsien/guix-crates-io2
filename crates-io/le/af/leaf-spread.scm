(define-module (crates-io le af leaf-spread) #:use-module (crates-io))

(define-public crate-leaf-spread-0.1.0 (c (n "leaf-spread") (v "0.1.0") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)))) (h "11binbi9hh0f2x29c309wlgjs1rljm6pk4h8ban7r91raa74f1h6")))

(define-public crate-leaf-spread-0.1.1 (c (n "leaf-spread") (v "0.1.1") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)))) (h "0phfly2ql2zqk8hckw2b288mppgphai2wa6z7q3j3fapk9pfgfb9")))

(define-public crate-leaf-spread-0.1.2 (c (n "leaf-spread") (v "0.1.2") (h "0p49n99jivljmvbclmlss3j3k7h4yl2zpcss4wskimzk6ym3npn1")))

