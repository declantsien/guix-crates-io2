(define-module (crates-io le af leaflet-velocity-sys) #:use-module (crates-io))

(define-public crate-leaflet-velocity-sys-0.1.0 (c (n "leaflet-velocity-sys") (v "0.1.0") (d (list (d (n "js-sys") (r "^0.3") (d #t) (k 0)) (d (n "leaflet") (r "^0.4") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.89") (d #t) (k 0)))) (h "14l7prirjpbknxys70js44zq52c1qp6h7fh4q972a3n09gxgdl9p")))

