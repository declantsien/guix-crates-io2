(define-module (crates-io le af leafslug_effects) #:use-module (crates-io))

(define-public crate-leafslug_effects-0.1.0 (c (n "leafslug_effects") (v "0.1.0") (d (list (d (n "fs_extra") (r "^1.3.0") (d #t) (k 0)) (d (n "gix-discover") (r "^0.26.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)) (d (n "time") (r "^0.3.31") (f (quote ("serde" "local-offset" "serde-well-known"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "0dfy3my7mbws6kj90cx1gz5mbmbs8l225lmzcv9id3zzan37pprc")))

(define-public crate-leafslug_effects-0.4.1 (c (n "leafslug_effects") (v "0.4.1") (d (list (d (n "fs_extra") (r "^1.3.0") (d #t) (k 0)) (d (n "gix-discover") (r "^0.26.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)) (d (n "time") (r "^0.3.31") (f (quote ("serde" "local-offset" "serde-well-known"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "1si836hjxvraxjl5k3arpv58bqhmarjqvp21klq3m97ddlq944nd")))

