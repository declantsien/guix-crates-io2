(define-module (crates-io le af leafwing_input_manager_macros) #:use-module (crates-io))

(define-public crate-leafwing_input_manager_macros-0.1.0 (c (n "leafwing_input_manager_macros") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1s7yi9yhcqn0akvjlkmh7bc8ajd4l8dcgxds4abbra1ivp4n0vk9")))

(define-public crate-leafwing_input_manager_macros-0.2.0 (c (n "leafwing_input_manager_macros") (v "0.2.0") (d (list (d (n "proc-macro-crate") (r "^1.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1ip8x855zq9hz7a6rf5q5zjcpn1l126p093rcgpf85dib5al5vf1")))

(define-public crate-leafwing_input_manager_macros-0.3.0 (c (n "leafwing_input_manager_macros") (v "0.3.0") (d (list (d (n "proc-macro-crate") (r "^1.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "16sg9gi2yc3afm31hli0s2lvk5jfdm592dz1jcy5j560lj5hc890")))

(define-public crate-leafwing_input_manager_macros-0.4.1 (c (n "leafwing_input_manager_macros") (v "0.4.1") (d (list (d (n "proc-macro-crate") (r "^1.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "10cqa5b87dgg3xlz7avckzbdl7r4606sqwcflh76q330sikbhqzh")))

(define-public crate-leafwing_input_manager_macros-0.5.0 (c (n "leafwing_input_manager_macros") (v "0.5.0") (d (list (d (n "proc-macro-crate") (r "^1.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1k9955miwfrbbazg939gi0pcfcvh182y02qmaianqcvgwaxpd1nk")))

(define-public crate-leafwing_input_manager_macros-0.6.0 (c (n "leafwing_input_manager_macros") (v "0.6.0") (d (list (d (n "proc-macro-crate") (r "^1.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1f8n89ddff651298b3802nm482pb6139mi2h1p3fj820ck5n91nr")))

(define-public crate-leafwing_input_manager_macros-0.7.0 (c (n "leafwing_input_manager_macros") (v "0.7.0") (d (list (d (n "proc-macro-crate") (r "^1.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1pm7aciv8g9z0qsq1pjqvca7hixqk3ms3hvwaik6l3jznajd1hsy")))

(define-public crate-leafwing_input_manager_macros-0.9.0 (c (n "leafwing_input_manager_macros") (v "0.9.0") (d (list (d (n "proc-macro-crate") (r "^1.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0jh6j150csdpq6yk8hca4zam1c0v5wyhbskyr65sgw4yrjzzmbhi")))

(define-public crate-leafwing_input_manager_macros-0.11.0 (c (n "leafwing_input_manager_macros") (v "0.11.0") (d (list (d (n "proc-macro-crate") (r "^2.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1gz5h4mpfzwc82gqf28zlkbyp2274d52sxqbbl0y81snsjcaxs6d")))

(define-public crate-leafwing_input_manager_macros-0.12.0 (c (n "leafwing_input_manager_macros") (v "0.12.0") (d (list (d (n "proc-macro-crate") (r "^3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "15m3psqz42n3fqfdn1vhszlbpim2q4x6fdn98g07v09b0g9z5z47")))

(define-public crate-leafwing_input_manager_macros-0.13.0 (c (n "leafwing_input_manager_macros") (v "0.13.0") (d (list (d (n "proc-macro-crate") (r "^3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "15nnv2f0yl7fhpjhxmhcslgx0sj733d4imkngrcwrnbi0x7mw1jx")))

