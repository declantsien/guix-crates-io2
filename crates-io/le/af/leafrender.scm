(define-module (crates-io le af leafrender) #:use-module (crates-io))

(define-public crate-leafrender-0.1.0 (c (n "leafrender") (v "0.1.0") (d (list (d (n "egl") (r "^0.2.7") (o #t) (d #t) (k 0)) (d (n "evdev") (r "^0.10.1") (o #t) (d #t) (k 0)) (d (n "gl") (r "^0.12.0") (o #t) (d #t) (k 0)) (d (n "glutin") (r "^0.21.0") (o #t) (d #t) (k 0)) (d (n "image") (r "^0.21") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "opengles") (r "^0.1.2") (o #t) (d #t) (k 0)) (d (n "rusttype") (r "^0.7.6") (d #t) (k 0)) (d (n "videocore") (r "^0.1.3") (o #t) (d #t) (k 0)))) (h "1kqaflxr4dsp92qa6hym78pkq5pkvb0j9r8j4c9vggz5hgkfz1ah") (f (quote (("raspberry_pi" "egl" "opengles" "videocore" "evdev") ("desktop_gl" "glutin" "gl") ("default" "desktop_gl"))))))

