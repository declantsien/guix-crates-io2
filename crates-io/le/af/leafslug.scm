(define-module (crates-io le af leafslug) #:use-module (crates-io))

(define-public crate-leafslug-0.1.0 (c (n "leafslug") (v "0.1.0") (h "1pb9a7vbgqcfm5rnjyg7inl5ww5vg2ns6qi87fiplhndj3wgmgwm")))

(define-public crate-leafslug-0.1.1 (c (n "leafslug") (v "0.1.1") (h "12hl2d31wbq6lg8zviyxjgp90j7wqa0rv9gb535k0f43j9swi0fk")))

(define-public crate-leafslug-0.1.2 (c (n "leafslug") (v "0.1.2") (h "01x75378ngd6v7w6fqmnwvrpx16ga1krblpb5xjdqrsqc6ahg6hw")))

(define-public crate-leafslug-0.1.3 (c (n "leafslug") (v "0.1.3") (h "1sg8j51ladfilrncz6nkwxw7r13j76nspihrikfgf4npg127zkx2")))

(define-public crate-leafslug-0.1.4 (c (n "leafslug") (v "0.1.4") (h "08q487dbl2zmm70y1i22mw8k86akyxghqbmy1x0xqja9g5japzsq")))

(define-public crate-leafslug-0.1.5 (c (n "leafslug") (v "0.1.5") (d (list (d (n "serde") (r "^1.0.181") (f (quote ("derive"))) (d #t) (k 0)))) (h "1bl6qg46fhqmwrhy3f8rncymf4prnc8qygjfhv4mil8baqw93dzs")))

(define-public crate-leafslug-0.2.0 (c (n "leafslug") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.72") (d #t) (k 0)) (d (n "axum") (r "^0.6.20") (f (quote ("http2"))) (d #t) (k 0)) (d (n "figment") (r "^0.10.10") (f (quote ("env" "toml"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (f (quote ("rustls-tls" "json"))) (k 2)) (d (n "serde") (r "^1.0.183") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 0)) (d (n "sqlx") (r "^0.7.1") (f (quote ("runtime-tokio-rustls" "macros" "postgres" "time"))) (k 0)) (d (n "sqlx") (r "^0.7.1") (f (quote ("runtime-tokio-rustls" "macros" "postgres" "time" "migrate"))) (k 2)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)) (d (n "time") (r "^0.3.25") (f (quote ("local-offset"))) (d #t) (k 0)) (d (n "tokio") (r "^1.29.1") (f (quote ("rt-multi-thread" "test-util" "macros"))) (d #t) (k 0)) (d (n "tokio-macros") (r "^2.1.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-bunyan-formatter") (r "^0.3.9") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.17") (d #t) (k 0)))) (h "0x01cmqv3yf7rcq5mkpnr0hv3sm0mc8k85nq7iak1a8wx7hwg1lb") (f (quote (("local"))))))

(define-public crate-leafslug-0.3.0 (c (n "leafslug") (v "0.3.0") (d (list (d (n "assert2") (r "^0.3.11") (d #t) (k 2)) (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap_complete_command") (r "^0.5.1") (f (quote ("carapace"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (d #t) (k 0)) (d (n "comfy-table") (r "^7.1.0") (d #t) (k 0)) (d (n "dialoguer") (r "^0.11.0") (f (quote ("fuzzy-select"))) (d #t) (k 0)) (d (n "figment") (r "^0.10.11") (f (quote ("env" "json"))) (d #t) (k 0)) (d (n "fs_extra") (r "^1.3.0") (d #t) (k 0)) (d (n "gix-discover") (r "^0.26.0") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)) (d (n "serde") (r "^1.0.189") (f (quote ("serde_derive" "rc"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "tempfile") (r "^3.8.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)) (d (n "time") (r "^0.3.30") (f (quote ("serde" "local-offset" "serde-human-readable"))) (d #t) (k 0)) (d (n "tokio") (r "^1.34.0") (f (quote ("macros" "rt-multi-thread" "process"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.18") (d #t) (k 0)))) (h "0kba1jqd7fvxphg6fcs1ddg30w32dy230rw1bjalkx7r34vsw568") (f (quote (("default" "serde")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-leafslug-0.4.2 (c (n "leafslug") (v "0.4.2") (h "0fnj89q2qrcxmvakl47gf3a82zvday4qnjxilvzy07z1sadf1p91")))

