(define-module (crates-io le af leaf-create-react) #:use-module (crates-io))

(define-public crate-leaf-create-react-0.1.0 (c (n "leaf-create-react") (v "0.1.0") (d (list (d (n "rust-embed") (r "^8.3.0") (d #t) (k 0)))) (h "1wy240ws54djwpq55223b5jf56zniwzyrydp3p0ikdkgy9rlng3z")))

(define-public crate-leaf-create-react-0.1.1 (c (n "leaf-create-react") (v "0.1.1") (d (list (d (n "rust-embed") (r "^8.3.0") (d #t) (k 0)))) (h "1vf6vcyjx75mg9wx1i0ym9grik129052h885fiws1i2n8kzy6xz5")))

(define-public crate-leaf-create-react-0.1.2 (c (n "leaf-create-react") (v "0.1.2") (d (list (d (n "rust-embed") (r "^8.3.0") (d #t) (k 0)))) (h "10h53rsbzparp8jcw44wn9rgnpih0knbl85s5gqcw7x6h047xcbn")))

(define-public crate-leaf-create-react-0.1.3 (c (n "leaf-create-react") (v "0.1.3") (d (list (d (n "rust-embed") (r "^8.3.0") (d #t) (k 0)))) (h "13h04hkj06v67q3vl7mgzx19sinhjvc2njkdn67f9hn1pssyqn8r")))

