(define-module (crates-io le th lethean-api) #:use-module (crates-io))

(define-public crate-lethean-api-1.0.0 (c (n "lethean-api") (v "1.0.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "0fjrrcdxysv72i71xi17r40imivgxbbnmbs69xnjll9ai4092zaa") (y #t)))

(define-public crate-lethean-api-1.0.1 (c (n "lethean-api") (v "1.0.1") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "1kdpfwf54x26nh2gx8xj2pify8q9a8p7q8h5x4hnv99amv67a545") (y #t)))

(define-public crate-lethean-api-1.1.1 (c (n "lethean-api") (v "1.1.1") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "13djkv8glkl2i5m6m78kdjpvh7zwfxsc08v0xa4mna63h0lvhc06") (y #t)))

