(define-module (crates-io le th lethean) #:use-module (crates-io))

(define-public crate-lethean-1.2.1 (c (n "lethean") (v "1.2.1") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "09xa3kapz672sk1c4i37zy1q2zd4x5gv5wf4ccqjdzbc945swnw5")))

(define-public crate-lethean-1.2.2 (c (n "lethean") (v "1.2.2") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "1nv2sqmnh5xkp0mk861q4mkrwf2pdav7yyf55cag026h9j0yl6wf")))

