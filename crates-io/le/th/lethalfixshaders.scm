(define-module (crates-io le th lethalfixshaders) #:use-module (crates-io))

(define-public crate-lethalfixshaders-0.1.0 (c (n "lethalfixshaders") (v "0.1.0") (d (list (d (n "anstyle") (r "^1.0.4") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.11") (f (quote ("derive" "unstable-styles"))) (d #t) (k 0)) (d (n "indoc") (r "^2.0.4") (d #t) (k 2)) (d (n "tokio") (r "^1.35.0") (f (quote ("fs" "macros" "rt" "rt-multi-thread"))) (d #t) (k 0)))) (h "06wmfw6ayg6l2i5xfijzbmzwars8yyjv0im4zin8dfl5qb5cnpi8")))

(define-public crate-lethalfixshaders-0.1.1 (c (n "lethalfixshaders") (v "0.1.1") (d (list (d (n "anstyle") (r "^1.0.4") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.11") (f (quote ("derive" "unstable-styles"))) (d #t) (k 0)) (d (n "indoc") (r "^2.0.4") (d #t) (k 2)) (d (n "tokio") (r "^1.35.0") (f (quote ("fs" "macros" "rt" "rt-multi-thread"))) (d #t) (k 0)))) (h "0yjgndxlgfhiabnlpzh0bgqp38pjphg950pndshw413ga84gv0fi")))

