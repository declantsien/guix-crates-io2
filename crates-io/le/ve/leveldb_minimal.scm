(define-module (crates-io le ve leveldb_minimal) #:use-module (crates-io))

(define-public crate-leveldb_minimal-0.1.0 (c (n "leveldb_minimal") (v "0.1.0") (d (list (d (n "leveldb-sys") (r "^2.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.4") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.4") (d #t) (k 2)))) (h "1c8nbnfdhj5c8slw8yazaa7i33nk4p6zfbkfzj3f9k6kswicpd2k") (f (quote (("default" "leveldb-sys/snappy"))))))

