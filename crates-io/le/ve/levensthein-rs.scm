(define-module (crates-io le ve levensthein-rs) #:use-module (crates-io))

(define-public crate-levensthein-rs-0.1.0 (c (n "levensthein-rs") (v "0.1.0") (d (list (d (n "itertools") (r "^0.8.2") (d #t) (k 0)) (d (n "rayon") (r "^1.3.0") (d #t) (k 0)))) (h "1b90s77hmzag52lagvwzxwq5sdkj5g3j9n5cslmr2hxi3dsmk5zd") (y #t)))

