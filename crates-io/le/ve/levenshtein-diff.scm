(define-module (crates-io le ve levenshtein-diff) #:use-module (crates-io))

(define-public crate-levenshtein-diff-0.1.0 (c (n "levenshtein-diff") (v "0.1.0") (h "0yx3rk5sfka4r34bi9q507036d2jsd8fg9kgfrybawhic0g5j2cj")))

(define-public crate-levenshtein-diff-0.1.1 (c (n "levenshtein-diff") (v "0.1.1") (h "0fz8wi9gc78avh98j27mn9ggb8da64252xj5mwv0dxm7x7gzngdm")))

(define-public crate-levenshtein-diff-0.1.2 (c (n "levenshtein-diff") (v "0.1.2") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "0yc5wgrp3i86q9iq2sna5pi17smz0i9p1jb73vjin1kjh70zp0nf")))

(define-public crate-levenshtein-diff-0.2.0 (c (n "levenshtein-diff") (v "0.2.0") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "17zik3jfncjpsw72jijv9idplwkhx8m576hwsj8ml6ms1n9fpkyy")))

(define-public crate-levenshtein-diff-0.2.1 (c (n "levenshtein-diff") (v "0.2.1") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "0h7gnfd5frapsndwmsf42l73snaflp4srspdw7a61pgraxbkjpsk")))

(define-public crate-levenshtein-diff-0.2.2 (c (n "levenshtein-diff") (v "0.2.2") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "0q74yp8881raffvwawiwh0i3f5fjh48q6yr5bnnhy37sc3g5if24")))

(define-public crate-levenshtein-diff-0.2.3 (c (n "levenshtein-diff") (v "0.2.3") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "0yqlakpwnmci9aaj0a3dvy20r8nlizw3qmzavva0k14j416mipbd")))

(define-public crate-levenshtein-diff-0.2.4 (c (n "levenshtein-diff") (v "0.2.4") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "1krmlk5w8l0zmcwnw26zfqx2y50f4hh58a3fcsdn0421s1fvpwpv")))

