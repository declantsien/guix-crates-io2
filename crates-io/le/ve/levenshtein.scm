(define-module (crates-io le ve levenshtein) #:use-module (crates-io))

(define-public crate-levenshtein-1.0.0 (c (n "levenshtein") (v "1.0.0") (h "1qzfb201d7d596m56czvq7yk8wklmky7hja9q5inifwgbg1vbkyw")))

(define-public crate-levenshtein-1.0.1 (c (n "levenshtein") (v "1.0.1") (h "10a4nr5f49hyjcdyc9vvnqf0wn2gafi4l96sfj5s60ijdq1bdciw")))

(define-public crate-levenshtein-1.0.2 (c (n "levenshtein") (v "1.0.2") (h "1jilr0rvw9fzc3mzz5c69n2gyzz6zd8vvxsz2996g1c68220212s")))

(define-public crate-levenshtein-1.0.3 (c (n "levenshtein") (v "1.0.3") (h "1n52wm4vwbfc2p1kfzb77m1ridq2pdri3h13r1v9522rmjzlhl59")))

(define-public crate-levenshtein-1.0.4 (c (n "levenshtein") (v "1.0.4") (h "15mc12s3isf5z78cp9rnrcdk24q0rkyf4lzb7h1c0r8w2q99q636")))

(define-public crate-levenshtein-1.0.5 (c (n "levenshtein") (v "1.0.5") (h "0q0pa1prsjk3v28xkas5rf30ng9816mxpr2n3xls65dmgawss4yv")))

