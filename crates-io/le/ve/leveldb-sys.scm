(define-module (crates-io le ve leveldb-sys) #:use-module (crates-io))

(define-public crate-leveldb-sys-0.0.1 (c (n "leveldb-sys") (v "0.0.1") (h "0x1b6w2yd4f9xmqb8hnz88xih7vdkpxs0s3a4hps6pba90j5rdvv") (f (quote (("snappy"))))))

(define-public crate-leveldb-sys-0.0.2 (c (n "leveldb-sys") (v "0.0.2") (h "0hsj1l10615401v688c35yzqh6ilnj3wpczj4ksb8jd2bz8rqzx5") (f (quote (("snappy"))))))

(define-public crate-leveldb-sys-0.0.3 (c (n "leveldb-sys") (v "0.0.3") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "1a3wqdx2gzh7pbjhvf8g1vcraxcwhgnjqpmv4h6i9xcj9bbq7f2l") (f (quote (("snappy"))))))

(define-public crate-leveldb-sys-1.0.0 (c (n "leveldb-sys") (v "1.0.0") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "0m0wscqyp46xl837x7c6jynwn8dql3qsdwhy8kzzxs9zkqc65cbv") (f (quote (("snappy"))))))

(define-public crate-leveldb-sys-2.0.0 (c (n "leveldb-sys") (v "2.0.0") (d (list (d (n "libc") (r "^0.2.4") (d #t) (k 0)))) (h "0pvd07z1h9wxpl2q3phf8s7qi31ywb46083nqkmfsd7np8dgl9i9") (f (quote (("snappy"))))))

(define-public crate-leveldb-sys-2.0.1 (c (n "leveldb-sys") (v "2.0.1") (d (list (d (n "libc") (r "0.2.*") (d #t) (k 0)))) (h "06687p705b58wh7m3wa1g8hlkwz9b4cprx7m78n60lpfw6s9fkja") (f (quote (("snappy"))))))

(define-public crate-leveldb-sys-2.0.2 (c (n "leveldb-sys") (v "2.0.2") (d (list (d (n "libc") (r "0.2.*") (d #t) (k 0)))) (h "1v94hp2ikc4q1k4izhg3kn2k16lncn00kaw596d1gdrh7d17iqj4") (f (quote (("snappy")))) (l "leveldb")))

(define-public crate-leveldb-sys-2.0.3 (c (n "leveldb-sys") (v "2.0.3") (d (list (d (n "libc") (r "0.2.*") (d #t) (k 0)))) (h "1yp700jzj7ya8mkis4a8nz7slvncmf1mgxwfm02qvmwggms6gf9s") (f (quote (("snappy")))) (l "leveldb")))

(define-public crate-leveldb-sys-2.0.4 (c (n "leveldb-sys") (v "2.0.4") (d (list (d (n "libc") (r "0.2.*") (d #t) (k 0)))) (h "1pyjmgg2qf15f9p3plb4mdxgfiwadkhmkv8iwibaj1bhls5l9c8n") (f (quote (("snappy")))) (l "leveldb")))

(define-public crate-leveldb-sys-2.0.5 (c (n "leveldb-sys") (v "2.0.5") (d (list (d (n "libc") (r "0.2.*") (d #t) (k 0)))) (h "1lkqcyfhir16ljhjh5yjffkj86nkzwkyvblsjcz2qqbhpcln9x3i") (f (quote (("snappy")))) (l "leveldb")))

(define-public crate-leveldb-sys-2.0.6 (c (n "leveldb-sys") (v "2.0.6") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "0.2.*") (d #t) (k 0)) (d (n "num_cpus") (r "^1.10") (d #t) (k 1)))) (h "00n5xpdrq6pl827n15b16bmnz3qjmsm4ijlbc5vg05mp3chvqjig") (f (quote (("snappy")))) (l "leveldb")))

(define-public crate-leveldb-sys-2.0.7 (c (n "leveldb-sys") (v "2.0.7") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "0.2.*") (d #t) (k 0)) (d (n "num_cpus") (r "^1.10") (d #t) (k 1)))) (h "11rf91z31lfpcvwny8yzx80bkvg7lhrd9swhh7ahb9swg2dlpi3n") (f (quote (("snappy")))) (l "leveldb")))

(define-public crate-leveldb-sys-2.0.8 (c (n "leveldb-sys") (v "2.0.8") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "0.2.*") (d #t) (k 0)) (d (n "num_cpus") (r "^1.10") (d #t) (k 1)))) (h "1js6fbjbng0g1b12wylw7rdszhbim9aa9a90ci2vhb6klddyx2k1") (f (quote (("snappy")))) (l "leveldb")))

(define-public crate-leveldb-sys-2.0.9 (c (n "leveldb-sys") (v "2.0.9") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "ffi-opaque") (r "^2") (d #t) (k 0)) (d (n "libc") (r "0.2.*") (d #t) (k 0)) (d (n "num_cpus") (r "^1.10") (d #t) (k 1)))) (h "1m1ih15gfdy3ndf1wk8wr9ll8qlsns1cf9qswkjkg922096lmncx") (f (quote (("snappy")))) (l "leveldb")))

