(define-module (crates-io le ve leveldb) #:use-module (crates-io))

(define-public crate-leveldb-0.1.0 (c (n "leveldb") (v "0.1.0") (d (list (d (n "db-key") (r "^0.0.1") (d #t) (k 0)))) (h "0ysqdrfx2jn1ri3linxzppdjb18mh43lpml3g1az6nkjbakkq3ic")))

(define-public crate-leveldb-0.2.0 (c (n "leveldb") (v "0.2.0") (d (list (d (n "db-key") (r "^0.0.1") (d #t) (k 0)))) (h "1z1ddr9wyh1kvvcil27v8v6lavhx137jrlwcp2kk6bp2hiq4az5w")))

(define-public crate-leveldb-0.3.0 (c (n "leveldb") (v "0.3.0") (d (list (d (n "db-key") (r "^0.0.1") (d #t) (k 0)))) (h "0ibganswspqcaz70ylvyycka1qlxmcclk24j6bbv69wiv9kq3xav")))

(define-public crate-leveldb-0.3.1 (c (n "leveldb") (v "0.3.1") (d (list (d (n "db-key") (r "^0.0.1") (d #t) (k 0)))) (h "0nrfs4j84lxg2vabygfd686xrs86j6walg381jwg32cdv302lqxr")))

(define-public crate-leveldb-0.3.3 (c (n "leveldb") (v "0.3.3") (d (list (d (n "db-key") (r "^0.0.3") (d #t) (k 0)))) (h "0j5im7nynxg77iv9x5hrbwx4wi8si4lmvyyxizrk9blbis7bmivx")))

(define-public crate-leveldb-0.3.4 (c (n "leveldb") (v "0.3.4") (d (list (d (n "db-key") (r "^0.0.4") (d #t) (k 0)))) (h "0xby1z3r7qy2dxhgk44c55b0sbrfrmjgpy2359jcphj9lbwvzwkn")))

(define-public crate-leveldb-0.3.5 (c (n "leveldb") (v "0.3.5") (d (list (d (n "db-key") (r "^0.0.4") (d #t) (k 0)))) (h "0bl4l4j4w4z8b9f8byw3ph8j3m0bdg7skplzflpcm57yanwwj0z3")))

(define-public crate-leveldb-0.3.6 (c (n "leveldb") (v "0.3.6") (d (list (d (n "db-key") (r "^0.0.4") (d #t) (k 0)))) (h "1sn1mbky120zkq2m2pn41qy70fmy6l0c05idjwamhlbfn6yvcrq6")))

(define-public crate-leveldb-0.3.8 (c (n "leveldb") (v "0.3.8") (d (list (d (n "db-key") (r "^0.0.4") (d #t) (k 0)))) (h "0arwvpd1mblm89nshhakff94jsy5pcvfy2aw246sslipf8asp41d")))

(define-public crate-leveldb-0.3.9 (c (n "leveldb") (v "0.3.9") (d (list (d (n "db-key") (r "^0.0.4") (d #t) (k 0)))) (h "09vddfpkbkz2zv4rpaz631kdhxdpd60y004zgnav695d0wgjih9r")))

(define-public crate-leveldb-0.4.0 (c (n "leveldb") (v "0.4.0") (d (list (d (n "db-key") (r "^0.0.4") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "068kibh7vh2j2br9xpfdn2wlqxbri16616bnbp11z10bsbm8qgkg")))

(define-public crate-leveldb-0.4.1 (c (n "leveldb") (v "0.4.1") (d (list (d (n "db-key") (r "^0.0.4") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "13679hrn3msgwp4fyhdfdjnyi7qnm42gcd6s7fsbz3yg7ar48774")))

(define-public crate-leveldb-0.4.2 (c (n "leveldb") (v "0.4.2") (d (list (d (n "db-key") (r "^0.0.4") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "tempdir") (r "*") (d #t) (k 2)))) (h "0jwfsffkjiwimqnpdg6ysbaq60cnyzqqplrl110r9i8qdkp3p670")))

(define-public crate-leveldb-0.5.0 (c (n "leveldb") (v "0.5.0") (d (list (d (n "db-key") (r "^0.0.4") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "tempdir") (r "*") (d #t) (k 2)))) (h "0nd37ribsxavpfl0fl9w4gwfpz886ya9vq9ly8454gr0adnarac7")))

(define-public crate-leveldb-0.5.1 (c (n "leveldb") (v "0.5.1") (d (list (d (n "db-key") (r "^0.0.4") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "tempdir") (r "*") (d #t) (k 2)))) (h "0n73wb6kldp5pvysvrawg9cn0ppiviq7j4pp4nywhidl6zzb1fjv")))

(define-public crate-leveldb-0.6.0 (c (n "leveldb") (v "0.6.0") (d (list (d (n "db-key") (r "^0.0.5") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "tempdir") (r "*") (d #t) (k 2)))) (h "1fm3y1vrh68xyb7pyxywanzdi2vj0svf2f3zg97f34vma23p05hl")))

(define-public crate-leveldb-0.6.1 (c (n "leveldb") (v "0.6.1") (d (list (d (n "db-key") (r "^0.0.5") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.0") (d #t) (k 2)))) (h "1biqipaljy30sbhb7mc0vjp3100p467dq4pmqvli8rbhc70x9ha4")))

(define-public crate-leveldb-0.7.0 (c (n "leveldb") (v "0.7.0") (d (list (d (n "db-key") (r "^0.0.5") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.0") (d #t) (k 2)))) (h "0jkpqcka20q7s05iddbiky1aa8zpwb57qk4zlrmrvckvzrsdwads")))

(define-public crate-leveldb-0.7.1 (c (n "leveldb") (v "0.7.1") (d (list (d (n "db-key") (r "^0.0.5") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.0") (d #t) (k 2)))) (h "03b9d56d36q0q8h0xk90bhxncdhl6jwl0i0b0nfch30x4lfslsfn")))

(define-public crate-leveldb-0.8.0 (c (n "leveldb") (v "0.8.0") (d (list (d (n "db-key") (r "^0.0.5") (d #t) (k 0)) (d (n "leveldb-sys") (r "^1.0.0") (f (quote ("snappy"))) (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.0") (d #t) (k 2)))) (h "15z0yiwj9cjji3l5hwc458d991nq8vw1z2055479ag35fav4ss0i")))

(define-public crate-leveldb-0.8.3 (c (n "leveldb") (v "0.8.3") (d (list (d (n "db-key") (r "^0.0.5") (d #t) (k 0)) (d (n "leveldb-sys") (r "^2.0.0") (f (quote ("snappy"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.4") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.4") (d #t) (k 2)))) (h "0a425ljrj1lzp20756f1spimf56igfxr0h7qlb202gkj5jksx3cs")))

(define-public crate-leveldb-0.8.4 (c (n "leveldb") (v "0.8.4") (d (list (d (n "db-key") (r "^0.0.5") (d #t) (k 0)) (d (n "leveldb-sys") (r "^2.0.0") (f (quote ("snappy"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.4") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.4") (d #t) (k 2)))) (h "1bfbkwsk643ygf1l4vxvj6zsdircqvixfxs4khwsr0n965ma6f44")))

(define-public crate-leveldb-0.8.5 (c (n "leveldb") (v "0.8.5") (d (list (d (n "db-key") (r "^0.0.5") (d #t) (k 0)) (d (n "leveldb-sys") (r "^2.0.0") (f (quote ("snappy"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.4") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.4") (d #t) (k 2)))) (h "1hlnmqrmrqx23p8ivdypr1ls9216wbpsg2a3ari4gb241j1zm8m5")))

(define-public crate-leveldb-0.8.6 (c (n "leveldb") (v "0.8.6") (d (list (d (n "db-key") (r "^0.0.5") (d #t) (k 0)) (d (n "leveldb-sys") (r "^2.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.4") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.4") (d #t) (k 2)))) (h "0sidk9i9mvshhz35msmhglb4qcyh7zkjbrmyw2kb75jmmam1nr9j") (f (quote (("default" "leveldb-sys/snappy"))))))

