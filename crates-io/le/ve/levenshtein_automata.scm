(define-module (crates-io le ve levenshtein_automata) #:use-module (crates-io))

(define-public crate-levenshtein_automata-0.1.0 (c (n "levenshtein_automata") (v "0.1.0") (d (list (d (n "fst") (r "^0.3") (o #t) (k 0)) (d (n "levenshtein") (r "^1.0") (d #t) (k 2)))) (h "042cw8rlz79lm4ca529zb6ar79z9brf9asa7l3a599d7c5mh3cdi") (f (quote (("fst_automaton" "fst")))) (y #t)))

(define-public crate-levenshtein_automata-0.1.1 (c (n "levenshtein_automata") (v "0.1.1") (d (list (d (n "fst") (r "^0.3") (o #t) (k 0)) (d (n "levenshtein") (r "^1.0") (d #t) (k 2)))) (h "1jp8yb4dyqxidp1id16vpgxdhjd2ars9gi0ain6m8s7lfzw0983k") (f (quote (("fst_automaton" "fst"))))))

(define-public crate-levenshtein_automata-0.2.0 (c (n "levenshtein_automata") (v "0.2.0") (d (list (d (n "fst") (r "^0.4") (o #t) (k 0)) (d (n "levenshtein") (r "^1.0") (d #t) (k 2)))) (h "06lypjvq15ds4cwq0csyjarjag54pyn0blcj994rn16vkhcv8kgl") (f (quote (("fst_automaton" "fst"))))))

(define-public crate-levenshtein_automata-0.2.1 (c (n "levenshtein_automata") (v "0.2.1") (d (list (d (n "fst") (r "^0.4") (o #t) (k 0)) (d (n "levenshtein") (r "^1.0") (d #t) (k 2)))) (h "09dv3rahqgslyv347s5ymwv0krw44d6xpfymz9mz7sa5dsvdwb0c") (f (quote (("fst_automaton" "fst"))))))

