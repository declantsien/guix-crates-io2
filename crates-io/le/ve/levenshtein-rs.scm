(define-module (crates-io le ve levenshtein-rs) #:use-module (crates-io))

(define-public crate-levenshtein-rs-0.1.0 (c (n "levenshtein-rs") (v "0.1.0") (h "0x98w5b55pwnac4gkm4180wz2m6iby965lwy8a6mhjm2cfc2n1ac") (y #t)))

(define-public crate-levenshtein-rs-0.1.1 (c (n "levenshtein-rs") (v "0.1.1") (h "0xmkx5dkkbn8p1wxppij42yi0pg8bh6g7qpx8623pa13387c0x8z") (y #t)))

