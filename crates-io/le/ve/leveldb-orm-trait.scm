(define-module (crates-io le ve leveldb-orm-trait) #:use-module (crates-io))

(define-public crate-leveldb-orm-trait-0.1.0 (c (n "leveldb-orm-trait") (v "0.1.0") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "db-key") (r "^0.0.5") (d #t) (k 0)) (d (n "leveldb") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "13xkqqlnjdvc42x0db23gp3y5hcpjgbzbdrihdaw96l7y71i3mq3") (y #t)))

