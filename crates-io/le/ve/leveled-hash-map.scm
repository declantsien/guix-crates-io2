(define-module (crates-io le ve leveled-hash-map) #:use-module (crates-io))

(define-public crate-leveled-hash-map-1.0.0 (c (n "leveled-hash-map") (v "1.0.0") (h "01gzdd3h5na9k7r4gxsfz6n8fl5awzb8mfcdl3b9nc9cy3mrif6j")))

(define-public crate-leveled-hash-map-1.1.0 (c (n "leveled-hash-map") (v "1.1.0") (h "1yrfsy3bjp8sls1wykz3ik2pn29jrmg7d6v4dr530lspv0252qq1")))

(define-public crate-leveled-hash-map-1.1.1 (c (n "leveled-hash-map") (v "1.1.1") (h "1phjn6dpkn6i8vbzxrmbkzp1f8lp95cxbmz34h2zdq19wvxp66jk")))

(define-public crate-leveled-hash-map-1.1.2 (c (n "leveled-hash-map") (v "1.1.2") (h "0qgdsb5yr7jbcnzz05gx1q2v97pwb6mmcyrkcsa51zdmh31b54fr")))

(define-public crate-leveled-hash-map-1.1.3 (c (n "leveled-hash-map") (v "1.1.3") (h "05yxsn1ry1k2nzdgg1sz94sy2d096viazxmajk2w0v8j8k7amldh")))

(define-public crate-leveled-hash-map-1.1.4 (c (n "leveled-hash-map") (v "1.1.4") (h "0x23745fss2wv8k0m8mi1cavrrcf67b5vmlfylsbcrhgm0gzc3pa")))

(define-public crate-leveled-hash-map-1.1.5 (c (n "leveled-hash-map") (v "1.1.5") (h "066gyl9h84fr1c4xlp65dbbn6b5dg60f27fr3p7yw6pp7l98gm4f")))

(define-public crate-leveled-hash-map-1.1.6 (c (n "leveled-hash-map") (v "1.1.6") (h "05fb74ibsbj7a00risvcmbclyzml181nkr9c1glymnqv9zng53if")))

(define-public crate-leveled-hash-map-1.1.7 (c (n "leveled-hash-map") (v "1.1.7") (h "1zsb504r7ck4j3933j5vhrmv81n57mpxxi8mhamcm05w2as4icji")))

(define-public crate-leveled-hash-map-1.1.8 (c (n "leveled-hash-map") (v "1.1.8") (h "1mbn17jb4jfyw888vqd7wgslxygzs1zcv3f5hfwydy8yng2hs0x3")))

(define-public crate-leveled-hash-map-1.1.10 (c (n "leveled-hash-map") (v "1.1.10") (h "0dpk9cvcmhxv1xxs7vafpc7pv47pp5wix32mygb3ayfrjbcpmnsr")))

(define-public crate-leveled-hash-map-1.1.11 (c (n "leveled-hash-map") (v "1.1.11") (h "0gdqh11r2g5w4rvbzw16dzbzal3xwyglql41x3zvb94iv08pnvq0")))

(define-public crate-leveled-hash-map-1.1.12 (c (n "leveled-hash-map") (v "1.1.12") (h "0g7gvklbslrcm8377s3icqbpwg2pbk0h8gdjpl6wpfndn4bj7hpk")))

(define-public crate-leveled-hash-map-1.1.13 (c (n "leveled-hash-map") (v "1.1.13") (h "1n9k9r687vj1fgy2ikyn5gwzq9lrksmkdv5igy1mgg1vmlrxr1n3")))

(define-public crate-leveled-hash-map-1.1.14 (c (n "leveled-hash-map") (v "1.1.14") (h "1s4zmldn9qji065ycc84na9xxzsgsm4vm59i8ilzyaxdabnl5va9")))

