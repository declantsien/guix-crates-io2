(define-module (crates-io le ve leveldb-rs) #:use-module (crates-io))

(define-public crate-leveldb-rs-0.0.1 (c (n "leveldb-rs") (v "0.0.1") (h "0gscxcfx25zz2anpdbqwpk9pbli4rhlzbyv04wz408hny234dy9y")))

(define-public crate-leveldb-rs-0.0.2 (c (n "leveldb-rs") (v "0.0.2") (h "14wlp48hlc7hfz9p9jq11j0mwlqxc80k8ii07r2vkjps0gvjc8l9")))

(define-public crate-leveldb-rs-0.0.3 (c (n "leveldb-rs") (v "0.0.3") (d (list (d (n "leveldb-sys") (r "^0.0.1") (d #t) (k 0)))) (h "1fhjv0lanh0rivzc7c0qmnvi4q4iszryxl8hmxg9575k8gm0npjq") (f (quote (("snappy" "leveldb-sys/snappy"))))))

(define-public crate-leveldb-rs-0.0.4 (c (n "leveldb-rs") (v "0.0.4") (d (list (d (n "leveldb-sys") (r "^0.0.2") (d #t) (k 0)))) (h "0b760v8pgcwcdcaggzwdk5lmmg1vjy3fsiwba8fa0yx2jc5nnjxx") (f (quote (("snappy" "leveldb-sys/snappy"))))))

(define-public crate-leveldb-rs-0.0.5 (c (n "leveldb-rs") (v "0.0.5") (d (list (d (n "leveldb-sys") (r "^0.0.3") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.0") (d #t) (k 2)))) (h "052xn1sbqlx30qvqi3ya6b35gvk3ikiambsgwz3i2nq57xziv23d") (f (quote (("snappy" "leveldb-sys/snappy"))))))

(define-public crate-leveldb-rs-0.0.6 (c (n "leveldb-rs") (v "0.0.6") (d (list (d (n "leveldb-sys") (r "^2.0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.0") (d #t) (k 2)))) (h "1naqrm6r8127vgakhpgp9jc1az8vijasf3w84fg4wcp51pm2zdy0") (f (quote (("snappy" "leveldb-sys/snappy"))))))

(define-public crate-leveldb-rs-0.0.7 (c (n "leveldb-rs") (v "0.0.7") (d (list (d (n "leveldb-sys") (r "^2.0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.0") (d #t) (k 2)))) (h "18lqwgrzbhgjdz15185z3dirxdyzrvsmj7jgkvxaibhjjphaydlz") (f (quote (("snappy" "leveldb-sys/snappy"))))))

