(define-module (crates-io le ve level) #:use-module (crates-io))

(define-public crate-level-0.1.0 (c (n "level") (v "0.1.0") (d (list (d (n "indicatif") (r "^0.13.0") (d #t) (k 0)) (d (n "termion") (r "^1.5.4") (d #t) (k 0)))) (h "1slg7k59yq4070lx7dk2gmjjc31yhw5i0ww5cf5z6xkagmmijvkl")))

(define-public crate-level-0.2.0 (c (n "level") (v "0.2.0") (d (list (d (n "indicatif") (r "^0.13.0") (d #t) (k 0)) (d (n "termion") (r "^1.5.4") (d #t) (k 0)))) (h "1v18m1b78jnjdqhfjy22va7id45ra7kzpzyqnzmdkgmx2pw7f3al")))

