(define-module (crates-io le et leetcode) #:use-module (crates-io))

(define-public crate-leetcode-0.1.0 (c (n "leetcode") (v "0.1.0") (h "02az6p1nr0r9idiy0c51cmdclpp6jrhjn344b5gfadljr8w2nz41")))

(define-public crate-leetcode-0.1.1 (c (n "leetcode") (v "0.1.1") (h "1is8ki7vdkyv65pj4s9yn8mah32rynz310a3f29y138g3ma03j7g")))

(define-public crate-leetcode-0.1.2 (c (n "leetcode") (v "0.1.2") (h "1351vic28z24fk8q3ad1aim5j8q81fzphzrxnx5n7xy910pvkqnr")))

(define-public crate-leetcode-0.1.3 (c (n "leetcode") (v "0.1.3") (h "1rka8xjgfwaj6hlq6mcr20r5xx9q3dj72ck1hxgy1ipl06q8bz0g")))

(define-public crate-leetcode-0.1.4 (c (n "leetcode") (v "0.1.4") (h "094imi5z79k2yw0x4bb0jkq7prx381bw9pybnr0pz02kkz1w9k0g")))

