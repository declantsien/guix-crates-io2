(define-module (crates-io le et leetcoders) #:use-module (crates-io))

(define-public crate-leetcoders-0.0.0 (c (n "leetcoders") (v "0.0.0") (d (list (d (n "rand") (r "^0.6") (d #t) (k 0)))) (h "0prpc9k0alm8v5jiapvsiwdcvrl31nnk88s87502r18kls1iyx3v")))

(define-public crate-leetcoders-0.0.1 (c (n "leetcoders") (v "0.0.1") (d (list (d (n "rand") (r "^0.6") (d #t) (k 0)))) (h "1b018yng5aqh168vclxp8zibh43afyrg7g57gqwnrv4kixl72b04")))

(define-public crate-leetcoders-0.0.2 (c (n "leetcoders") (v "0.0.2") (d (list (d (n "rand") (r "^0.6") (d #t) (k 0)))) (h "1ksl6176r0p9bcjfc0zlywyslymvf4wzfn2dx73adybhbfz48p4d")))

(define-public crate-leetcoders-0.0.3 (c (n "leetcoders") (v "0.0.3") (d (list (d (n "rand") (r "^0.6") (d #t) (k 0)))) (h "0zrf69ipb2hfhjlz8c8msilzgbw116mm16na777xg4m26y71rji7")))

(define-public crate-leetcoders-0.0.5 (c (n "leetcoders") (v "0.0.5") (d (list (d (n "rand") (r "^0.6") (d #t) (k 0)))) (h "17wm1c9xjdpnl1kf61k925mhvnv6yv4jf8akn1zmvsa5rm73m62d")))

(define-public crate-leetcoders-0.0.20 (c (n "leetcoders") (v "0.0.20") (d (list (d (n "rand") (r "^0.6") (d #t) (k 0)))) (h "15bj9f853w2zjh6gra5nl52bq1r41ndsk8jwygbj4p5giy0dd3zp")))

