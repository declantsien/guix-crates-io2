(define-module (crates-io le et leetcode-test-utils) #:use-module (crates-io))

(define-public crate-leetcode-test-utils-0.0.1 (c (n "leetcode-test-utils") (v "0.0.1") (d (list (d (n "num_cpus") (r "^1.13.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "threadpool") (r "^1.8.1") (d #t) (k 0)))) (h "1rmjhz720q12ynlss80h4sm85n79nljbp0qp4c7l4k0gjxairphh")))

(define-public crate-leetcode-test-utils-0.0.2 (c (n "leetcode-test-utils") (v "0.0.2") (d (list (d (n "num_cpus") (r "^1.13.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "threadpool") (r "^1.8.1") (d #t) (k 0)))) (h "1fn9n2cwbc6hxxspr6j7avhdcw42jxxl3s5f35znh613q7dg7ppk")))

(define-public crate-leetcode-test-utils-0.0.3 (c (n "leetcode-test-utils") (v "0.0.3") (d (list (d (n "num_cpus") (r "^1.13.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "threadpool") (r "^1.8.1") (d #t) (k 0)))) (h "117glyzaq1683p624bjb6yj2fybzsyqi0j0rjadh2n2j7vmw7s05")))

(define-public crate-leetcode-test-utils-0.0.4 (c (n "leetcode-test-utils") (v "0.0.4") (h "0d2l92ryp3vp5jk56jqlkmrcwiyjrhawh5hf52f0lwq2v6z0rs5q")))

(define-public crate-leetcode-test-utils-0.0.5 (c (n "leetcode-test-utils") (v "0.0.5") (h "00az1dn7wzxl5f4j6wb4gx3mfvgj60skbfiwnkkhij1pggh0vr4l")))

(define-public crate-leetcode-test-utils-0.0.6 (c (n "leetcode-test-utils") (v "0.0.6") (h "1cg5hdgrk4nwhnls7syydkxgwjbxqc6316rvjghw5s7nh8ca582h")))

(define-public crate-leetcode-test-utils-0.0.7 (c (n "leetcode-test-utils") (v "0.0.7") (h "11qpaxl01smgqdxv07cp7h457pfm6y41i79zyxxpzsg3khi2vqpl")))

