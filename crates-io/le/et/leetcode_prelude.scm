(define-module (crates-io le et leetcode_prelude) #:use-module (crates-io))

(define-public crate-leetcode_prelude-0.1.0 (c (n "leetcode_prelude") (v "0.1.0") (d (list (d (n "leetcode_test") (r "^0.1") (d #t) (k 0)))) (h "0f2r1z20ccw2ckmp07rgd5ymvqr78z3r1md34pnm760kpf6qf9qg")))

(define-public crate-leetcode_prelude-0.1.1 (c (n "leetcode_prelude") (v "0.1.1") (d (list (d (n "leetcode_test") (r "^0.1") (d #t) (k 0)))) (h "10dq881sc88939rp7f2yvbri5k6i20fd1vn0jwj19a3mj47hmkmz")))

(define-public crate-leetcode_prelude-0.1.2 (c (n "leetcode_prelude") (v "0.1.2") (d (list (d (n "leetcode_test") (r "^0.1") (d #t) (k 0)))) (h "0d99ggqbma760zcq1nah32c5h5m371iy9z1xddr091ha8jivra27")))

(define-public crate-leetcode_prelude-0.2.2 (c (n "leetcode_prelude") (v "0.2.2") (d (list (d (n "leetcode_test") (r "^0.1") (d #t) (k 0)))) (h "0py2yicma98l05xwnrydxim79xjjw17vgr2dz62syl8pl9k8479h")))

(define-public crate-leetcode_prelude-0.2.3 (c (n "leetcode_prelude") (v "0.2.3") (d (list (d (n "leetcode_test") (r "^0.1") (d #t) (k 0)))) (h "08wsibwhnyym3qk755gscw235x10lswll5lkn346ganykz9qhvai")))

