(define-module (crates-io le et leetcode-in-rust) #:use-module (crates-io))

(define-public crate-leetcode-in-rust-0.2.10 (c (n "leetcode-in-rust") (v "0.2.10") (d (list (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.93") (d #t) (k 0)) (d (n "criterion") (r "^0.4") (d #t) (k 2)))) (h "1hzqapckwn92bznck70860vcxaric75cmvpgprjwdnglxpyijljp")))

