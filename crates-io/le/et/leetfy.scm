(define-module (crates-io le et leetfy) #:use-module (crates-io))

(define-public crate-leetfy-0.2.0 (c (n "leetfy") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0.12") (d #t) (k 2)) (d (n "clap") (r "^4.3.23") (f (quote ("derive"))) (d #t) (k 0)) (d (n "predicates") (r "^3.0.3") (d #t) (k 2)))) (h "0h2r3pn15khaxvq5vbjrf8pj1c0bjgzxfzdcqa79rzp7agn4czqi") (y #t)))

(define-public crate-leetfy-0.2.1 (c (n "leetfy") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0.12") (d #t) (k 2)) (d (n "clap") (r "^4.3.23") (f (quote ("derive"))) (d #t) (k 0)) (d (n "predicates") (r "^3.0.3") (d #t) (k 2)))) (h "0v3bgr4ksxdrbnb0q80dnjqwlzicawjb7bfcwnmqnxx4ids2wpjl") (y #t)))

(define-public crate-leetfy-0.2.2 (c (n "leetfy") (v "0.2.2") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0.12") (d #t) (k 2)) (d (n "clap") (r "^4.3.23") (f (quote ("derive"))) (d #t) (k 0)) (d (n "predicates") (r "^3.0.3") (d #t) (k 2)))) (h "19vc2m06jghnwpzd7n2b6g54n9bdmi9anirnxx46j0im2lk2k69x") (y #t)))

(define-public crate-leetfy-0.2.3 (c (n "leetfy") (v "0.2.3") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0.12") (d #t) (k 2)) (d (n "clap") (r "^4.3.23") (f (quote ("derive"))) (d #t) (k 0)) (d (n "predicates") (r "^3.0.3") (d #t) (k 2)))) (h "0jn5p3bcvjcn5dmyv0ppcjvhl4k18c7h5qkf18fnvkxjqpjgfvfz")))

