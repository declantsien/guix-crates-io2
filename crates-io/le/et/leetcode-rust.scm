(define-module (crates-io le et leetcode-rust) #:use-module (crates-io))

(define-public crate-leetcode-rust-0.1.0 (c (n "leetcode-rust") (v "0.1.0") (d (list (d (n "rand") (r "^0.6") (d #t) (k 0)))) (h "0ypvz3hz1wjxqfkrh0md89yazrfmpc9zvfmf3r7rg7a3f9a4bzd1")))

(define-public crate-leetcode-rust-0.1.1 (c (n "leetcode-rust") (v "0.1.1") (h "0a9acip7ysk4339c2j1fx4r787k1hrq16v3w74zp7j625ck7dn1g")))

(define-public crate-leetcode-rust-0.1.2 (c (n "leetcode-rust") (v "0.1.2") (h "15q9d1if151n7yg9x1bpp9dxgi1zl870qirp68hyaqzw3scsg273")))

