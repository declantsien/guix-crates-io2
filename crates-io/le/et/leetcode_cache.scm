(define-module (crates-io le et leetcode_cache) #:use-module (crates-io))

(define-public crate-leetcode_cache-0.1.0 (c (n "leetcode_cache") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "serial_test") (r "^0.6.0") (d #t) (k 2)))) (h "179k8gnl0nk3xpy101xrwg31hkh6w8cpsgkpgcvi5gkxiqdf92gi")))

