(define-module (crates-io le et leetcode-tui-db) #:use-module (crates-io))

(define-public crate-leetcode-tui-db-0.4.0 (c (n "leetcode-tui-db") (v "0.4.0") (d (list (d (n "leetcode-core") (r "^0.4.0") (d #t) (k 0)) (d (n "leetcode-tui-shared") (r "^0.4.0") (d #t) (k 0)) (d (n "native_db") (r "^0") (d #t) (k 0)) (d (n "native_model") (r "^0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1c5vq3dh5zz9mdjpdvbfhpg8yc05v0rzs6gpmv6f3gi72gniz3lf")))

