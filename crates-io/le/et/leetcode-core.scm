(define-module (crates-io le et leetcode-core) #:use-module (crates-io))

(define-public crate-leetcode-core-0.4.0 (c (n "leetcode-core") (v "0.4.0") (d (list (d (n "async-trait") (r "^0") (d #t) (k 0)) (d (n "html2text") (r "^0") (d #t) (k 0)) (d (n "leetcode-tui-config") (r "^0.4.0") (d #t) (k 0)) (d (n "lru") (r "^0") (d #t) (k 0)) (d (n "reqwest") (r "^0") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "serde_with") (r "^3.3.0") (f (quote ("macros"))) (d #t) (k 0)) (d (n "strum") (r "^0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "15im8s0ygjwyxqchjzzjniixaqdpp5pn41dbcvzb6bdmjx47f7kq")))

