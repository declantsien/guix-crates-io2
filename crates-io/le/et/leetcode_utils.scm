(define-module (crates-io le et leetcode_utils) #:use-module (crates-io))

(define-public crate-leetcode_utils-0.1.0 (c (n "leetcode_utils") (v "0.1.0") (h "1ry6ajfi2kv5s11gqxdppfdj5dxgdlzzb3a8j53y5zj61fabalfa")))

(define-public crate-leetcode_utils-0.1.1 (c (n "leetcode_utils") (v "0.1.1") (h "0kavy5j7damrzyn0xdf0k5fzph0z8k25wmx7kskqjs0y4in444ym") (y #t)))

(define-public crate-leetcode_utils-0.1.2 (c (n "leetcode_utils") (v "0.1.2") (h "18vnnw0pakm21lw8l8yyw5i9v9mhcxjy56gcvl9lsrfq82vrbdk9")))

(define-public crate-leetcode_utils-0.1.3 (c (n "leetcode_utils") (v "0.1.3") (h "16c9lljf2h58arljf31si828xxdkdiz99nvjww7730l94h1r3qya")))

(define-public crate-leetcode_utils-0.1.4 (c (n "leetcode_utils") (v "0.1.4") (h "0yxjcfdw9ws3vd9b941mzbmk10p156p42lxam3qga36zfnf1zfhf")))

(define-public crate-leetcode_utils-0.1.5 (c (n "leetcode_utils") (v "0.1.5") (h "0xdm64l6jk01jcqhlimjxg2n682ac2aainbyr3d2jypm8glg0gxd")))

