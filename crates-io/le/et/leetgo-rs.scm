(define-module (crates-io le et leetgo-rs) #:use-module (crates-io))

(define-public crate-leetgo-rs-0.1.0 (c (n "leetgo-rs") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "serde") (r "^1.0.156") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)))) (h "0f7ghhy2f0iilq6d8macbvh68kwisp089ac9a030fja2nz2yz4sc")))

(define-public crate-leetgo-rs-0.2.0 (c (n "leetgo-rs") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "serde") (r "^1.0.156") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)))) (h "1x3v2yl7gh39g7l26a3qlvk0nkkijxbl79ds3b39i9vcwmar8dib")))

(define-public crate-leetgo-rs-0.2.1 (c (n "leetgo-rs") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "serde") (r "^1.0.156") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)))) (h "0qhivwg458vibhl5xkp7hkdglda0aqy9ib1iampn2y45sw9j62rn")))

