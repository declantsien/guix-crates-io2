(define-module (crates-io le et leetup-cache) #:use-module (crates-io))

(define-public crate-leetup-cache-0.1.0 (c (n "leetup-cache") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.55") (d #t) (k 0)))) (h "109vbp4jlk89k7b865i9c997r8agh5jdvpy6m9l7n7fhfch4bw2d")))

(define-public crate-leetup-cache-0.2.0 (c (n "leetup-cache") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.55") (d #t) (k 0)))) (h "1x47wq58959r8afacqxpynxmpq1n4w0zivx78jas2bnfhxbz6hxq")))

