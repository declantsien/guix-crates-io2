(define-module (crates-io le et leet_rs) #:use-module (crates-io))

(define-public crate-leet_rs-0.1.0 (c (n "leet_rs") (v "0.1.0") (h "0wv5c2acdw62lfkf60if3j6bgzvhdg0448d97rakr2ir6xvjbmwz")))

(define-public crate-leet_rs-0.1.1 (c (n "leet_rs") (v "0.1.1") (h "0ayaqsn1z5nqiwdczm2agjq56wb9ggab09dqvwr0yc4i5qsd9zgj")))

(define-public crate-leet_rs-0.1.2 (c (n "leet_rs") (v "0.1.2") (h "1n5wym1waafnlq8hsg5nxi39afrzacrqnbwvxp6cqq25y4nr4dcq")))

(define-public crate-leet_rs-0.1.3 (c (n "leet_rs") (v "0.1.3") (h "050r05fd0f6b408sjl51pfmvhgs70vx0j11f8n26i6sc9vjk5ryz")))

(define-public crate-leet_rs-0.1.4 (c (n "leet_rs") (v "0.1.4") (h "04cz60n72rwc2bxbp7pplak0669zqa3v4kxlzisp1rdlcymdamfq")))

(define-public crate-leet_rs-0.1.5 (c (n "leet_rs") (v "0.1.5") (h "0ry246vf81b5ikcnj4l0n8b1h45larl0mnsypfkg4sxwpkgjz6f6")))

(define-public crate-leet_rs-0.1.6 (c (n "leet_rs") (v "0.1.6") (h "15v4sbg15rqyas3bh2nwmqn2711j30wbk21a29icw2fr5cjvflng")))

(define-public crate-leet_rs-0.1.7 (c (n "leet_rs") (v "0.1.7") (h "1wmn0dw93kcmzfasnccvlj9vzzs0v2q9w4yl4chv6q29m3zyhxqg")))

(define-public crate-leet_rs-0.1.8 (c (n "leet_rs") (v "0.1.8") (h "1969z5ixgzrj8zjmld3060493znvjawx84fdw7iwj52phf6jh8b9")))

(define-public crate-leet_rs-0.1.9 (c (n "leet_rs") (v "0.1.9") (h "0pw84yqri1vhnk8hk5f155d64n7agwfmd9pkzypp2zbgjfjhz0jy")))

(define-public crate-leet_rs-0.2.0 (c (n "leet_rs") (v "0.2.0") (h "0dsr3hjhfdywhycasplzh42n6vka1afwkzr94yz43gi4j8zp4hgn")))

(define-public crate-leet_rs-0.2.1 (c (n "leet_rs") (v "0.2.1") (h "0alxxi96akmr80z2b36hmqbhy0haly67w4m0lzf3qxdq5j2zlni5")))

(define-public crate-leet_rs-0.2.2 (c (n "leet_rs") (v "0.2.2") (h "1c7vc71gj3lrd5hhxjga611679bq5907j1zqq4jh4wi1m9i4blwz")))

