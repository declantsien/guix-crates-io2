(define-module (crates-io le et leetcode-tui-shared) #:use-module (crates-io))

(define-public crate-leetcode-tui-shared-0.4.0 (c (n "leetcode-tui-shared") (v "0.4.0") (d (list (d (n "color-eyre") (r "^0") (d #t) (k 0)) (d (n "crossterm") (r "^0") (f (quote ("event-stream"))) (d #t) (k 0)) (d (n "ratatui") (r "^0.23") (f (quote ("all-widgets"))) (d #t) (k 0)) (d (n "signal-hook") (r "^0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-util") (r "^0") (d #t) (k 0)))) (h "0cxlzr9xgs06vvc0d31c57h8x4mnknp95bd8j8lmgnfpm7ad90v8")))

