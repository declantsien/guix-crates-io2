(define-module (crates-io le et leetcode-solutions) #:use-module (crates-io))

(define-public crate-leetcode-solutions-0.1.0 (c (n "leetcode-solutions") (v "0.1.0") (h "14bdcv300rlfnhhs1pd142l6dr9392h5na51410clhyv8i721pgi")))

(define-public crate-leetcode-solutions-0.1.1 (c (n "leetcode-solutions") (v "0.1.1") (h "05mkqhizymcsa9a73qr8c1ily8kikfn6x0m7d3lsivrr6rnadp03")))

