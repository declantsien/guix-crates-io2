(define-module (crates-io le et leetcode_for_rust) #:use-module (crates-io))

(define-public crate-leetcode_for_rust-0.1.0 (c (n "leetcode_for_rust") (v "0.1.0") (h "009ccqg81k81lb2vy5z3glb9f90ncc1bdjqra2rw622iccavcd1q")))

(define-public crate-leetcode_for_rust-0.1.1 (c (n "leetcode_for_rust") (v "0.1.1") (h "0izmnzshdqg12115cr3jvf5dszi51jsxznv0c0pz2ya37pdx2ikj")))

(define-public crate-leetcode_for_rust-0.1.2 (c (n "leetcode_for_rust") (v "0.1.2") (h "065akjb1mazvd2kxaa8gpf8bmjjj9qh2kf424sphcpx6zmz698pb")))

(define-public crate-leetcode_for_rust-0.1.3 (c (n "leetcode_for_rust") (v "0.1.3") (h "0mjpb8lwwyz36kgq18xhdysyx4wagk4l6w4w0a8s12mhp565dlwg")))

(define-public crate-leetcode_for_rust-0.1.30 (c (n "leetcode_for_rust") (v "0.1.30") (h "1bi1xps0afzjcvwqyc0n0q420q40nzcrkmgp8p745372xjf7jikv")))

(define-public crate-leetcode_for_rust-0.1.31 (c (n "leetcode_for_rust") (v "0.1.31") (h "15x49dq9albxgvy91mnnpcd7j4zgi7a1i76nj2zg829p49dx7jdg")))

(define-public crate-leetcode_for_rust-0.1.32 (c (n "leetcode_for_rust") (v "0.1.32") (h "112ld8pyp57d80jisx5jhhwrf5hpkq5yj417agr1bjyyv09dz23l")))

(define-public crate-leetcode_for_rust-0.1.33 (c (n "leetcode_for_rust") (v "0.1.33") (h "0fpxfgbqssjbls2q5397w2wnx6z616k7vd0shqy47s9vhld6l2bi")))

(define-public crate-leetcode_for_rust-0.1.34 (c (n "leetcode_for_rust") (v "0.1.34") (h "1szryglsnwip2mkriar0wi8dja607gznb49n1rb3ccqciqrqa672")))

(define-public crate-leetcode_for_rust-0.1.35 (c (n "leetcode_for_rust") (v "0.1.35") (h "182cahhbjdvw0p4qlc3hds77gxs44qfg2kw7nxqapv6qw48v5bcq")))

(define-public crate-leetcode_for_rust-0.1.36 (c (n "leetcode_for_rust") (v "0.1.36") (h "0m3xq5ypc9zlzlqdgprj51l3rgl0l4y3kynica9vhckmiba80mbd")))

(define-public crate-leetcode_for_rust-0.1.37 (c (n "leetcode_for_rust") (v "0.1.37") (h "1vpw6p5714a0lk0j7jz9wcn1vgb7jmkvrr8d2cy19y3sa93kl77w")))

