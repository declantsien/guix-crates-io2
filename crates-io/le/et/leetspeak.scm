(define-module (crates-io le et leetspeak) #:use-module (crates-io))

(define-public crate-leetspeak-0.1.0 (c (n "leetspeak") (v "0.1.0") (d (list (d (n "phf") (r "^0.11.1") (f (quote ("macros"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "093p5j40712dai3cv6akvzamx0zcadbrc2f82rlaansxsddb3cww")))

(define-public crate-leetspeak-0.1.1 (c (n "leetspeak") (v "0.1.1") (d (list (d (n "phf") (r "^0.11.1") (f (quote ("macros"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0nzrk0wa4nwcki4qr6zdkcz22248v3nxdfnabx0knxr6672wnw3d")))

(define-public crate-leetspeak-0.1.2 (c (n "leetspeak") (v "0.1.2") (d (list (d (n "phf") (r "^0.11.1") (f (quote ("macros"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "15h4jgp1a21s77kcaqk9qv7x3mh9g7c0k88ihrmwpwakzjcsawxp")))

(define-public crate-leetspeak-0.2.0 (c (n "leetspeak") (v "0.2.0") (d (list (d (n "phf") (r "^0.11.1") (f (quote ("macros"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1sgjr58hg6yhncvv09f8yi62qb3w8kyc8k7npp8a8bs8qhzbfpg7")))

