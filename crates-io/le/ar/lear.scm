(define-module (crates-io le ar lear) #:use-module (crates-io))

(define-public crate-lear-0.1.0 (c (n "lear") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.68") (d #t) (k 0)) (d (n "termion") (r "^1.5.6") (d #t) (k 0)))) (h "14abh0vchcs48aqfn7cdibi23bi9i1hngq8hymi3gfcf9nyizy7y")))

(define-public crate-lear-0.2.0 (c (n "lear") (v "0.2.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.68") (d #t) (k 0)) (d (n "termion") (r "^1.5.6") (d #t) (k 0)))) (h "1jasf3fj2881rh9yrrlxr67vznhzjqnn17q1dhp2jrzh3qaa5fjg")))

