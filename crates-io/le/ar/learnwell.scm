(define-module (crates-io le ar learnwell) #:use-module (crates-io))

(define-public crate-learnwell-0.1.0 (c (n "learnwell") (v "0.1.0") (d (list (d (n "fastrand") (r "^1.8.0") (d #t) (k 0)) (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "show-image") (r "^0.13.1") (d #t) (k 0)))) (h "1lx42lpnfq9lk02vnnyj1fk4jwvyzn0m7z648j3jrz37niiz382a")))

(define-public crate-learnwell-0.2.0 (c (n "learnwell") (v "0.2.0") (d (list (d (n "fastrand") (r "^1.9.0") (d #t) (k 0)) (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "runnt") (r "^0.1.0") (d #t) (k 0)) (d (n "show-image") (r "^0.13.1") (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (f (quote ("strum_macros"))) (d #t) (k 0)))) (h "1q5xhnkl5q7sy5rka0qqa4kf68zjk3rasa666hc2vz8qjhfshjcp")))

(define-public crate-learnwell-0.2.1 (c (n "learnwell") (v "0.2.1") (d (list (d (n "fastrand") (r "^1.9.0") (d #t) (k 0)) (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "runnt") (r "^0.6.0") (d #t) (k 0)) (d (n "show-image") (r "^0.13.1") (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (f (quote ("strum_macros"))) (d #t) (k 0)))) (h "1p8gz81r7bgyzgpnssyglzb7wiwcqp13dw1sxw8d49f64jb15xml")))

