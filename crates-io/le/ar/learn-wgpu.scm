(define-module (crates-io le ar learn-wgpu) #:use-module (crates-io))

(define-public crate-learn-wgpu-0.1.0 (c (n "learn-wgpu") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "pollster") (r "^0.2.5") (d #t) (k 0)) (d (n "wgpu") (r "^0.13.1") (d #t) (k 0)) (d (n "winit") (r "^0.26") (d #t) (k 0)))) (h "00gn5k2n87d9dq556dswva50razky197jl27x7zzvamfj2qkpf8p")))

