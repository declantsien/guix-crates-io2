(define-module (crates-io le ar learn-alg) #:use-module (crates-io))

(define-public crate-learn-alg-0.1.0 (c (n "learn-alg") (v "0.1.0") (h "0jv7djqhg9ndqnxyaz1xi7r77mdglc6i8xyr3fs1091bcylk4421")))

(define-public crate-learn-alg-0.1.1 (c (n "learn-alg") (v "0.1.1") (h "1fkx1jfkylbc4zg2x8i4balyjc5hk6a68l72a1zj05f4sda2d9c2")))

