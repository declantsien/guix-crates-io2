(define-module (crates-io le ar learntk) #:use-module (crates-io))

(define-public crate-learntk-0.1.0 (c (n "learntk") (v "0.1.0") (d (list (d (n "indicatif") (r "^0.17.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "14p8bl5pgpf55k30xycg30yc77wd56rwsgi7gyjxbcli30cigjbf")))

(define-public crate-learntk-0.2.0 (c (n "learntk") (v "0.2.0") (d (list (d (n "indicatif") (r "^0.17.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1az9qbz92ihfn8dbm5crc1fmk10hqq1z4l54hzmfhsvgk1fqv7ik")))

(define-public crate-learntk-0.2.1 (c (n "learntk") (v "0.2.1") (d (list (d (n "indicatif") (r "^0.17.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1z34c867z4yg2v1rdaamb07a1ag5040dx73zl5va572arj9js0c6")))

(define-public crate-learntk-0.3.0 (c (n "learntk") (v "0.3.0") (d (list (d (n "indicatif") (r "^0.17.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "12l4hqici8acaabv1wh38r9x5p7zi5pml4cr0p2jg3z7nnrmlg66")))

