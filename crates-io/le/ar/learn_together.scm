(define-module (crates-io le ar learn_together) #:use-module (crates-io))

(define-public crate-learn_together-0.1.0 (c (n "learn_together") (v "0.1.0") (h "0ba64xj0jivdpw5fyp2jkwbn4g49g5b5n7z1khr3a6mn4jrwv024")))

(define-public crate-learn_together-0.1.1 (c (n "learn_together") (v "0.1.1") (h "0wkf5hqirvhbm9pw0v43q4darfq2mbzxdd3cz1wa7s9y06md6g5c")))

