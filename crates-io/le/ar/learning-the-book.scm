(define-module (crates-io le ar learning-the-book) #:use-module (crates-io))

(define-public crate-learning-the-book-0.1.0 (c (n "learning-the-book") (v "0.1.0") (h "1zimy754bajsm9f8sclxl5msrmi5fjx3rzz9agskrk206kr96pa8")))

(define-public crate-learning-the-book-0.1.1 (c (n "learning-the-book") (v "0.1.1") (h "0ri9qc4m2asbv8rxssm24lx47sf899z8k5595zm6swgxc1vnwjlx")))

