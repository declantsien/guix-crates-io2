(define-module (crates-io le ar learn-opengl) #:use-module (crates-io))

(define-public crate-learn-opengl-0.0.0 (c (n "learn-opengl") (v "0.0.0") (d (list (d (n "beryllium") (r "^0.2.0-alpha.2") (d #t) (k 2)) (d (n "bytemuck") (r "^1") (d #t) (k 0)) (d (n "imagine") (r "^0.0.2") (d #t) (k 2)) (d (n "ogl33") (r "^0.1") (f (quote ("debug_error_checks" "debug_trace_messages"))) (d #t) (k 0)) (d (n "ultraviolet") (r "^0.3") (d #t) (k 2)))) (h "155q1w5b281xk96rxslg4aag0jhwqwr1p7nrxfj4jprj6vmw3lic")))

