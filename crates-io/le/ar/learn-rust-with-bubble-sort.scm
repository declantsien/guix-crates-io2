(define-module (crates-io le ar learn-rust-with-bubble-sort) #:use-module (crates-io))

(define-public crate-learn-rust-with-bubble-sort-0.1.0 (c (n "learn-rust-with-bubble-sort") (v "0.1.0") (h "0mracm41zm4hjw0ygm45yvmandlx77x13qiaym7w624nrcp0a884")))

(define-public crate-learn-rust-with-bubble-sort-0.1.1 (c (n "learn-rust-with-bubble-sort") (v "0.1.1") (h "0r2lfghb0jqqx8w7n4ivblkj766jrsim49v89qvmh7pc3x7y7l00")))

