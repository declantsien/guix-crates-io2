(define-module (crates-io le ar learn-rust-publish) #:use-module (crates-io))

(define-public crate-learn-rust-publish-0.1.0 (c (n "learn-rust-publish") (v "0.1.0") (h "03im3hd8hyg45ajnapjc026z730mrrj3gl08scvldr1vg50vkcbv")))

(define-public crate-learn-rust-publish-0.1.1 (c (n "learn-rust-publish") (v "0.1.1") (h "00k487jbzyqqpbjimk919d876rkfnz47901jmd8rp7wgnf1ddq94")))

