(define-module (crates-io le ar learn_rust) #:use-module (crates-io))

(define-public crate-learn_rust-0.1.0 (c (n "learn_rust") (v "0.1.0") (h "00dd1fslavp66hbsplxw96fp4fyaa8rns2gapg62mdmaskvs92ac")))

(define-public crate-learn_rust-0.1.1 (c (n "learn_rust") (v "0.1.1") (h "1sxhjp6p0p7fcvplszsc15kbry1vl6yg9b7pmvw2yg3j856qd5ih")))

(define-public crate-learn_rust-0.1.2 (c (n "learn_rust") (v "0.1.2") (h "1pyq7v3l56gm0wzgca3qmnnwgqigqkg015s0bin9by8w6cqwm8j8")))

(define-public crate-learn_rust-0.1.3 (c (n "learn_rust") (v "0.1.3") (h "1p28283866g300y6jpyglgkqlq490ngfswyqx1sb9l55rx08zc93")))

