(define-module (crates-io le db ledb-types) #:use-module (crates-io))

(define-public crate-ledb-types-0.2.0 (c (n "ledb-types") (v "0.2.0") (d (list (d (n "bytes") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_cbor") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)))) (h "0xyw78ylbhwnrlaly7p0132i165f7wr4ra93llhhca30lh2baddy") (f (quote (("json" "serde_json") ("default") ("cbor" "serde_cbor"))))))

(define-public crate-ledb-types-0.3.0 (c (n "ledb-types") (v "0.3.0") (d (list (d (n "bytes") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_cbor") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)))) (h "1qfjyyr0ppbnmgpin09hkz6k9k7xba80hyjqvg71giplhs9wkxpj") (f (quote (("json" "serde_json") ("default") ("cbor" "serde_cbor"))))))

(define-public crate-ledb-types-0.4.0 (c (n "ledb-types") (v "0.4.0") (d (list (d (n "bytes") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_cbor") (r "^0.11") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)))) (h "13h45ldlg75gnrdh97wiq2l6fk2jjw8xk62lws25ndjbf8qdxl90") (f (quote (("json" "serde_json") ("default") ("cbor" "serde_cbor"))))))

