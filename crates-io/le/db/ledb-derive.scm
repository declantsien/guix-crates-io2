(define-module (crates-io le db ledb-derive) #:use-module (crates-io))

(define-public crate-ledb-derive-0.2.0 (c (n "ledb-derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("derive"))) (d #t) (k 0)))) (h "0h2h6rsl9nqyr913s7wn3zpmbfr21r45ymkj1rn1pi95r9cnn1g5")))

(define-public crate-ledb-derive-0.2.1 (c (n "ledb-derive") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("derive"))) (d #t) (k 0)))) (h "1nb6jp8cr010lha04izmvykhqgnvf7fplrjn0hv9i057rgys3iaz")))

(define-public crate-ledb-derive-0.3.0 (c (n "ledb-derive") (v "0.3.0") (d (list (d (n "ledb") (r "^0.3") (d #t) (k 2)) (d (n "ledb-types") (r "^0.3") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0vd60sv64yzdp2bbss45bb2mkhgkv7iafq85an4hinsxydfc23l9")))

(define-public crate-ledb-derive-0.4.0 (c (n "ledb-derive") (v "0.4.0") (d (list (d (n "ledb") (r "^0.4") (d #t) (k 2)) (d (n "ledb-types") (r "^0.4") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 2)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1pl4aij9mjz10x9zrmw9xf6blr1r4rchqc64js2g37k0r8nf7qq1")))

