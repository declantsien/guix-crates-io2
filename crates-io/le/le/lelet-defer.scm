(define-module (crates-io le le lelet-defer) #:use-module (crates-io))

(define-public crate-lelet-defer-1.0.0 (c (n "lelet-defer") (v "1.0.0") (h "12ny8w7hlpdpcbhjrsx8mpxk5bpsbf4qv4y3d9p3qqg23v17ws2h")))

(define-public crate-lelet-defer-1.1.0 (c (n "lelet-defer") (v "1.1.0") (h "0d6y9rps6x3q63c9m8lvza90z6fyxzn73nx4b7qhqrjfc6bfsm67")))

