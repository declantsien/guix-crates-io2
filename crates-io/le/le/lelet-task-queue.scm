(define-module (crates-io le le lelet-task-queue) #:use-module (crates-io))

(define-public crate-lelet-task-queue-1.0.0 (c (n "lelet-task-queue") (v "1.0.0") (d (list (d (n "async-task") (r "^4.0.3") (d #t) (k 0)) (d (n "crossbeam-deque") (r "^0.8.0") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.8.1") (d #t) (k 0)) (d (n "fastrand") (r "^1.4.0") (d #t) (k 0)) (d (n "lelet-defer") (r "^1.0.0") (d #t) (k 0)))) (h "0q76w96bn4i7x61ni8df5l3zjcx0k375mhl880ccmfb5vb6h29v8")))

(define-public crate-lelet-task-queue-1.0.1 (c (n "lelet-task-queue") (v "1.0.1") (d (list (d (n "async-task") (r "^4.0.3") (d #t) (k 0)) (d (n "crossbeam-deque") (r "^0.8.0") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.8.1") (d #t) (k 0)) (d (n "fastrand") (r "^1.4.0") (d #t) (k 0)) (d (n "lelet-defer") (r "^1.0.0") (d #t) (k 0)))) (h "114r1mxa7fsky125w20b5kdmzyndhpnj57lcs1n794k84mg6hrjl")))

(define-public crate-lelet-task-queue-1.0.2 (c (n "lelet-task-queue") (v "1.0.2") (d (list (d (n "async-task") (r "^4.0.3") (d #t) (k 0)) (d (n "crossbeam-deque") (r "^0.8.0") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.8.1") (d #t) (k 0)) (d (n "fastrand") (r "^1.4.0") (d #t) (k 0)) (d (n "lelet-defer") (r "^1.0.0") (d #t) (k 0)))) (h "1bsyg85ajb1470w788vv022llxgzmxq74svhkf99728cph0ig2hb") (y #t)))

(define-public crate-lelet-task-queue-1.0.3 (c (n "lelet-task-queue") (v "1.0.3") (d (list (d (n "async-task") (r "^4.0.3") (d #t) (k 0)) (d (n "crossbeam-deque") (r "^0.8.0") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.8.1") (d #t) (k 0)) (d (n "fastrand") (r "^1.4.0") (d #t) (k 0)) (d (n "lelet-defer") (r "^1.0.0") (d #t) (k 0)))) (h "0bsn4fn0fvnxy1i1s74s6wdmww6cgxg5n8w9qhx8mpibs7312dh4")))

