(define-module (crates-io le le lelet-utils) #:use-module (crates-io))

(define-public crate-lelet-utils-0.1.0 (c (n "lelet-utils") (v "0.1.0") (d (list (d (n "crossbeam-utils") (r "^0.7.2") (d #t) (k 0)))) (h "0703bsir7i5nxvzfh27w1w6z1zy5qnx7r379j9d1wv2xg1vyawdk") (y #t)))

(define-public crate-lelet-utils-0.1.1 (c (n "lelet-utils") (v "0.1.1") (d (list (d (n "crossbeam-utils") (r "^0.7.2") (d #t) (k 0)))) (h "0nxnf6j7n4pyxgqq56hzfrjqfvi2f007fmrzk2jsmir686kv7vhf") (y #t)))

(define-public crate-lelet-utils-0.1.2 (c (n "lelet-utils") (v "0.1.2") (d (list (d (n "crossbeam-utils") (r "^0.7.2") (d #t) (k 0)))) (h "1psx56151dx1d1sqqpjbrsagd1zhgg64b9q01wg5q8n383vlh22r") (y #t)))

(define-public crate-lelet-utils-0.1.3 (c (n "lelet-utils") (v "0.1.3") (d (list (d (n "crossbeam-utils") (r "^0.7.2") (d #t) (k 0)))) (h "0qc0y75sczpdlbpkjaa1p3gzwk3861k9a2kq6dm9ypksxv99sy7h") (y #t)))

(define-public crate-lelet-utils-0.1.4 (c (n "lelet-utils") (v "0.1.4") (d (list (d (n "crossbeam-utils") (r "^0.7.2") (d #t) (k 0)))) (h "10hkmh7kgvgzpv693pw813s5jz23hmrdlwdjrih2vi3mwd0swzph") (y #t)))

(define-public crate-lelet-utils-0.1.5 (c (n "lelet-utils") (v "0.1.5") (d (list (d (n "crossbeam-utils") (r "^0.7.2") (d #t) (k 0)))) (h "07aw6zqkv55ljfm6ai3h07wcmjk8rpws84k1mcqg5pakp56qy8zd") (y #t)))

(define-public crate-lelet-utils-0.2.0 (c (n "lelet-utils") (v "0.2.0") (h "1pk24iy59883acshqjm489m8vm9if43c9r589gkpdx5by8glhw5y") (y #t)))

(define-public crate-lelet-utils-0.2.1 (c (n "lelet-utils") (v "0.2.1") (h "1bk3i5xpbaij2iv3lfa1x5ch9vf0cxqzcw7xnx376724g08ix6by") (y #t)))

(define-public crate-lelet-utils-0.2.2 (c (n "lelet-utils") (v "0.2.2") (h "1b71yrll1iyc4dsbg3pd5mddbcl6j4nk2gjn50ffx2bdvnndflbm") (y #t)))

(define-public crate-lelet-utils-0.2.3 (c (n "lelet-utils") (v "0.2.3") (h "1kbnzkdjrykrc197fl647iqvd7ma9c2i9lzpx40k6w27w2drbnm4") (y #t)))

(define-public crate-lelet-utils-0.2.4 (c (n "lelet-utils") (v "0.2.4") (h "0ymfyk21fsqqczkp3k7f87lh7yrxpir5nxd9s5d202sk1zjmj280") (y #t)))

(define-public crate-lelet-utils-0.2.5 (c (n "lelet-utils") (v "0.2.5") (h "0kyy4c06psaap5a3pf10alrz0pv7nwxzi1y7cdcnli5f0ncrb99m") (y #t)))

(define-public crate-lelet-utils-0.3.0 (c (n "lelet-utils") (v "0.3.0") (h "1khaqf0j8qajwfr33809zqq0l8qivl0qx3vqvlhc7sdn1pwbkgmd") (y #t)))

(define-public crate-lelet-utils-0.3.1 (c (n "lelet-utils") (v "0.3.1") (h "0wd4qyspcpv312m3q5ivkgn5pd8mhgqnfwab6wbrws7yj5pvbna0") (y #t)))

(define-public crate-lelet-utils-0.3.2 (c (n "lelet-utils") (v "0.3.2") (h "1kjmc2n7q59hn2qcqyds8naffana820rvs0i169x72gk59hahci1") (y #t)))

(define-public crate-lelet-utils-0.3.3 (c (n "lelet-utils") (v "0.3.3") (h "1m9p271ar4xa56r3vl8dxy73vd45125k9s11cwydfijfzsip2qdv") (y #t)))

(define-public crate-lelet-utils-0.3.4 (c (n "lelet-utils") (v "0.3.4") (h "1g4vkqr1wcm1ka9rj2yr3qf20sz8gddfhc084mqzhyvsn314qy81")))

(define-public crate-lelet-utils-0.3.5 (c (n "lelet-utils") (v "0.3.5") (h "1q1wzshr35bdrkpddjbxliq3x6kdf9k2vib72hgv18l53fvfl3nh")))

(define-public crate-lelet-utils-0.3.6 (c (n "lelet-utils") (v "0.3.6") (h "0ffqsrjqdwki2800ql0k872rv731prbnwq4lww5m2pgaig0wxgkc")))

(define-public crate-lelet-utils-0.4.0 (c (n "lelet-utils") (v "0.4.0") (h "0vpz4hqsnw33rhb5dnjsx1yqhj68b63qgdpgm0hkdmbfddxafz0k")))

