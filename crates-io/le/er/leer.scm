(define-module (crates-io le er leer) #:use-module (crates-io))

(define-public crate-leer-0.1.0 (c (n "leer") (v "0.1.0") (d (list (d (n "leer-macros") (r "=0.0.1") (o #t) (d #t) (k 0)))) (h "0qzqh02wpla9s92bnskhxnxjdjhsfjs674ls82ls2jjzincd6n94") (f (quote (("derive" "leer-macros"))))))

(define-public crate-leer-0.1.1 (c (n "leer") (v "0.1.1") (d (list (d (n "leer-macros") (r "=0.0.1") (o #t) (d #t) (k 0)))) (h "1y392pncdzris6knvdxgx24y4cd4rlpzncaci1gyf3cdqr4zc747") (f (quote (("derive" "leer-macros"))))))

