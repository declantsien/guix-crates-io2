(define-module (crates-io le n_ len_constraints) #:use-module (crates-io))

(define-public crate-len_constraints-0.1.0 (c (n "len_constraints") (v "0.1.0") (h "1v5r215f1qs99p21ih53znh1dbzrhkq7v4r8fl58absi9mha7gml")))

(define-public crate-len_constraints-0.1.1 (c (n "len_constraints") (v "0.1.1") (h "1094avbx03wdvarz8cs22i9chi7kccmvi3bkcyd8qk4s27iphf90")))

(define-public crate-len_constraints-0.1.2 (c (n "len_constraints") (v "0.1.2") (h "1qwaza8bmqwkxiicb8y7mhg4vkxsrw44ln4ajhinmn8x18lk4a05")))

