(define-module (crates-io le sl leslie_lamport) #:use-module (crates-io))

(define-public crate-leslie_lamport-0.1.0 (c (n "leslie_lamport") (v "0.1.0") (d (list (d (n "blake2-rfc") (r "^0.2.18") (d #t) (k 0)) (d (n "getrandom") (r "^0.1.12") (d #t) (k 0)) (d (n "hex") (r "^0.4.0") (d #t) (k 0)))) (h "06wd6s51z72cd5iwiqzqx03civdcnr9vpalf6jacy01fyr7p8y5p")))

(define-public crate-leslie_lamport-0.2.0 (c (n "leslie_lamport") (v "0.2.0") (d (list (d (n "blake2-rfc") (r "^0.2.18") (d #t) (k 0)) (d (n "crypto-hash") (r "^0.3.4") (d #t) (k 0)) (d (n "getrandom") (r "^0.1.14") (d #t) (k 0)) (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "15gjmnsjyqa2cgw4iwr18bl2aahyllm26zj19cjif3h26rh7c512") (y #t)))

(define-public crate-leslie_lamport-0.2.1 (c (n "leslie_lamport") (v "0.2.1") (d (list (d (n "blake2-rfc") (r "^0.2.18") (d #t) (k 0)) (d (n "crypto-hash") (r "^0.3.4") (d #t) (k 0)) (d (n "getrandom") (r "^0.1.14") (d #t) (k 0)) (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1vhx13vvzcl2fi52acihfz36mwa3h6zm3j7jippbcqs3381bgqk2")))

(define-public crate-leslie_lamport-0.3.0 (c (n "leslie_lamport") (v "0.3.0") (d (list (d (n "blake2-rfc") (r "^0.2.18") (d #t) (k 0)) (d (n "crypto-hash") (r "^0.3.4") (d #t) (k 0)) (d (n "getrandom") (r "^0.1.14") (d #t) (k 0)) (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.12") (d #t) (k 0)))) (h "0plrvmzxqs3f7rcy3hr7i0s88dgdfrgjkhz02cyaiakr81a9ix6h")))

(define-public crate-leslie_lamport-0.3.1 (c (n "leslie_lamport") (v "0.3.1") (d (list (d (n "blake2-rfc") (r "^0.2.18") (d #t) (k 0)) (d (n "crypto-hash") (r "^0.3.4") (d #t) (k 0)) (d (n "getrandom") (r "^0.1.16") (d #t) (k 0)) (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.12") (d #t) (k 0)))) (h "0417b5ja73k1r4r1kq1prfsv5y1sncxj0pw9fiyjhka3wmkgzpjw")))

