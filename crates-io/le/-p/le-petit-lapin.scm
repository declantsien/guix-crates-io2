(define-module (crates-io le -p le-petit-lapin) #:use-module (crates-io))

(define-public crate-le-petit-lapin-0.1.0 (c (n "le-petit-lapin") (v "0.1.0") (d (list (d (n "x11") (r "^2.21.0") (d #t) (k 0)) (d (n "xcb") (r "^1.2.0") (f (quote ("xkb" "xinerama" "debug_atom_names"))) (d #t) (k 0)))) (h "0c5i0rkkrrpwgpynf7w8klxjgknvfj0xkldv9kl4fiww1ialgwdm") (l "x11")))

(define-public crate-le-petit-lapin-0.1.1 (c (n "le-petit-lapin") (v "0.1.1") (d (list (d (n "x11") (r "^2.21.0") (d #t) (k 0)) (d (n "xcb") (r "^1.2.0") (f (quote ("xkb" "xinerama" "debug_atom_names"))) (d #t) (k 0)))) (h "052alsp3c6a0gfbcc9118jz4w5kn0hdrwnf9q9k0lihh0jc5f6kb") (l "x11")))

(define-public crate-le-petit-lapin-0.1.2 (c (n "le-petit-lapin") (v "0.1.2") (d (list (d (n "x11") (r "^2.21.0") (d #t) (k 0)) (d (n "xcb") (r "^1.2.0") (f (quote ("xkb" "xinerama" "debug_atom_names"))) (d #t) (k 0)))) (h "1nykr2ixb6gkvlvvrc96c2jyy6ij2vb82c4b8ralcs6z83wwncx7") (l "x11")))

