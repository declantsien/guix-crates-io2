(define-module (crates-io le t_ let_or_return) #:use-module (crates-io))

(define-public crate-let_or_return-0.1.0 (c (n "let_or_return") (v "0.1.0") (d (list (d (n "macrotest") (r "^1.0.9") (d #t) (k 2)))) (h "1wg39vyq8zbdavyklc0w5qvpjpzq3qkj3k1dvi9qg9blgrrah4c4")))

(define-public crate-let_or_return-1.0.0 (c (n "let_or_return") (v "1.0.0") (d (list (d (n "macrotest") (r "^1.0.9") (d #t) (k 2)))) (h "1nwnwwc7w81hqr2kpch2q9bbxq95r862vacn8l74d5n3r6xgvmi1")))

(define-public crate-let_or_return-1.0.1 (c (n "let_or_return") (v "1.0.1") (d (list (d (n "macrotest") (r "^1.0.9") (d #t) (k 2)))) (h "119yqg9brzmlkj8pdxdb0ygvqq3mwgqblrfkn7cy308s7amy2v1n")))

(define-public crate-let_or_return-1.0.2 (c (n "let_or_return") (v "1.0.2") (d (list (d (n "macrotest") (r "^1.0.9") (d #t) (k 2)))) (h "1wl8i3fd0m0r7jlc9mkgq9wixavvck3bawpij440rxswrriq5ax8")))

