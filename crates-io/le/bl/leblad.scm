(define-module (crates-io le bl leblad) #:use-module (crates-io))

(define-public crate-leblad-1.0.0 (c (n "leblad") (v "1.0.0") (d (list (d (n "reqwest") (r "^0.11.23") (f (quote ("json"))) (d #t) (k 1)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 1)) (d (n "tokio") (r "^1.35.1") (f (quote ("full"))) (d #t) (k 1)))) (h "0klhn46jjyixsx39n17czwjzxxcnd475chk2qh3rqhx8d8d79kbf") (y #t)))

(define-public crate-leblad-1.0.1 (c (n "leblad") (v "1.0.1") (d (list (d (n "reqwest") (r "^0.11.23") (f (quote ("json"))) (d #t) (k 1)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 1)) (d (n "tokio") (r "^1.35.1") (f (quote ("full"))) (d #t) (k 1)))) (h "0wfrqpmdy6sw9ds7q3pjy6dl37cdnss2b1g1lwnc8yczakwna5lq")))

(define-public crate-leblad-1.0.2 (c (n "leblad") (v "1.0.2") (d (list (d (n "reqwest") (r "^0.11.23") (f (quote ("json"))) (d #t) (k 1)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 1)) (d (n "tokio") (r "^1.35.1") (f (quote ("full"))) (d #t) (k 1)))) (h "0y3y3qp6vx8f7blnq4xq8h8wr9h11kfm6arvn56b54v0a3cbd62m")))

