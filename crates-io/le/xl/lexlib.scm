(define-module (crates-io le xl lexlib) #:use-module (crates-io))

(define-public crate-lexlib-0.4.0 (c (n "lexlib") (v "0.4.0") (h "140ivwv7179g74318w9gdf9ksxqjrry3rfyamn29cj6x7ibggbww")))

(define-public crate-lexlib-0.5.0 (c (n "lexlib") (v "0.5.0") (d (list (d (n "egui") (r "^0.20.1") (o #t) (d #t) (k 0)))) (h "0rss0bxig3fnap8pb5b3npd4y7bxwx9x172rh5fzsy6k4gqlllmm") (s 2) (e (quote (("egui" "dep:egui"))))))

(define-public crate-lexlib-0.5.1 (c (n "lexlib") (v "0.5.1") (d (list (d (n "egui") (r "^0.20.1") (o #t) (d #t) (k 0)))) (h "0g317azd9y5k7a00pd8p1zdcxmq6zvdc5zrxl0dm46vdrajazjp1") (s 2) (e (quote (("egui" "dep:egui"))))))

(define-public crate-lexlib-0.6.0-dev (c (n "lexlib") (v "0.6.0-dev") (d (list (d (n "egui") (r "^0.20.1") (o #t) (d #t) (k 0)))) (h "1xbd1ssyjyisxyzx2pn4kprrw9jbksg6w9ljv9g44iqyrvhy7mah") (y #t) (s 2) (e (quote (("egui" "dep:egui"))))))

(define-public crate-lexlib-0.7.0 (c (n "lexlib") (v "0.7.0") (d (list (d (n "egui") (r "^0.20.1") (o #t) (d #t) (k 0)))) (h "023g39ixxngr92xlkxajinj0m8010k0qan9jrm4r1i9k97hxq5li") (s 2) (e (quote (("egui" "dep:egui"))))))

(define-public crate-lexlib-0.7.1 (c (n "lexlib") (v "0.7.1") (d (list (d (n "egui") (r "^0.20.1") (o #t) (d #t) (k 0)) (d (n "sdl2") (r "^0.35.2") (o #t) (d #t) (k 0)))) (h "1vm2fkzj7fh2whmgxq78n1qf1slr1hb0w2f74dj2fkrld8z91lnd") (s 2) (e (quote (("sdl2" "dep:sdl2") ("egui" "dep:egui"))))))

(define-public crate-lexlib-0.7.2 (c (n "lexlib") (v "0.7.2") (d (list (d (n "egui") (r "^0.20.1") (o #t) (d #t) (k 0)) (d (n "sdl2") (r "^0.35.2") (o #t) (d #t) (k 0)))) (h "0gcvw8vss8kd06w3ymm8pp1si1vmlbdn2r5by0baq1yxnpzyrjv6") (f (quote (("reprC")))) (s 2) (e (quote (("sdl2" "dep:sdl2") ("egui" "dep:egui"))))))

(define-public crate-lexlib-0.7.3 (c (n "lexlib") (v "0.7.3") (d (list (d (n "egui") (r "^0.20.1") (o #t) (d #t) (k 0)) (d (n "sdl2") (r "^0.35.2") (o #t) (d #t) (k 0)))) (h "0v511bk55383kw87q6jar8czmyzj1s1d01n92n2x1fk7rvsm0cyw") (f (quote (("reprC")))) (s 2) (e (quote (("sdl2" "dep:sdl2") ("egui" "dep:egui"))))))

(define-public crate-lexlib-0.7.4 (c (n "lexlib") (v "0.7.4") (d (list (d (n "egui") (r "^0.21") (o #t) (d #t) (k 0)) (d (n "sdl2") (r "^0.35.2") (o #t) (d #t) (k 0)))) (h "0f82qncnpzs04hi2hdxb6sbl7iiszdbkvxs2ck6liw8l9gs7sfcb") (f (quote (("reprC")))) (s 2) (e (quote (("sdl2" "dep:sdl2") ("egui" "dep:egui"))))))

(define-public crate-lexlib-1.6.0-dev (c (n "lexlib") (v "1.6.0-dev") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)))) (h "197w4gv2zliw5bnp17s4hrlvzmb1ghjxwrdwcsakpfs8h66229zv")))

(define-public crate-lexlib-2.0.0 (c (n "lexlib") (v "2.0.0") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "14q5aph4ywk0k03di4mppg8ffrhlr6di5babzcww9wpc248xxn2f") (f (quote (("libpng") ("experimental"))))))

(define-public crate-lexlib-2.0.1 (c (n "lexlib") (v "2.0.1") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1fdb0430qsr979jlz17mrwx206mhkqlx9nr769akqymw18cws50g") (f (quote (("libpng") ("experimental"))))))

