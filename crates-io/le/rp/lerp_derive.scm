(define-module (crates-io le rp lerp_derive) #:use-module (crates-io))

(define-public crate-lerp_derive-0.4.0 (c (n "lerp_derive") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0w81rs5i85ygsrqi3wzjl3f2ngflys04s45z49i489gj1yvmgk9n")))

