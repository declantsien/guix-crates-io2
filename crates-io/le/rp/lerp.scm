(define-module (crates-io le rp lerp) #:use-module (crates-io))

(define-public crate-lerp-0.1.0 (c (n "lerp") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.1.34") (d #t) (k 0)))) (h "1fdkjbjrbxwrda7gwf6iv3xjr67ccf5iy58z8cd0nh2600k5xdvw")))

(define-public crate-lerp-0.1.1 (c (n "lerp") (v "0.1.1") (d (list (d (n "num-traits") (r "^0.1.34") (d #t) (k 0)))) (h "042m9csmyv21l3n7gfd04lzvnzsv8559y3d32yqk16kcb8mpvpnp")))

(define-public crate-lerp-0.2.0 (c (n "lerp") (v "0.2.0") (d (list (d (n "num-traits") (r "^0.1.34") (d #t) (k 0)))) (h "0saf8pj958v8grknzhiks893gqmdmh12v50d1h5wkq5dvxbxiiza")))

(define-public crate-lerp-0.3.0 (c (n "lerp") (v "0.3.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "01q1c7gngwlbgayklym15x21452afd084rac8wic7750zpjd8kky")))

(define-public crate-lerp-0.4.0 (c (n "lerp") (v "0.4.0") (d (list (d (n "lerp_derive") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0k4yn0kz7pnkdcpm0wilzbw1qv7fzd5h7f4f9811mzwv846wb6mi") (f (quote (("derive" "lerp_derive") ("default"))))))

(define-public crate-lerp-0.5.0 (c (n "lerp") (v "0.5.0") (d (list (d (n "lerp_derive") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0q32f07gngpg81b560dpx65b17kirr58ykxvnsnbrv4k8l16migc") (f (quote (("derive" "lerp_derive") ("default"))))))

