(define-module (crates-io le xi lexi-matic) #:use-module (crates-io))

(define-public crate-lexi-matic-0.1.0 (c (n "lexi-matic") (v "0.1.0") (d (list (d (n "lexi-matic-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "regex-automata") (r "^0.4.4") (f (quote ("perf" "dfa-search"))) (k 0)))) (h "1ifbm59q4f471xv3wf08s0sqa3pm589v08mjs74wm2n7gq7w5m8d")))

