(define-module (crates-io le xi lexis) #:use-module (crates-io))

(define-public crate-lexis-0.1.0 (c (n "lexis") (v "0.1.0") (h "045447ccym75krn0isr03fb3msmx141ynnh3j50mgj01msc4scf1")))

(define-public crate-lexis-0.1.1 (c (n "lexis") (v "0.1.1") (h "0h5bvsn3r2f4wam3nkr5sd0a2fymnf0m2vdsr7l67pgw1bqigbp8")))

(define-public crate-lexis-0.2.0 (c (n "lexis") (v "0.2.0") (h "0cij4s0jp4ba16hp55187k5xb7dg2ps2srl33lzbrpyh0jkfz2pl")))

(define-public crate-lexis-0.2.1 (c (n "lexis") (v "0.2.1") (h "0di7bj5378hy2rzkg7gafk249sb7clglz5b0h9740rsfa7jw57ny")))

(define-public crate-lexis-0.2.2 (c (n "lexis") (v "0.2.2") (h "03k74y2p4fcbz0p40gx3s82bvwxq7a2aqkxridm28ffpwkcpcb3z")))

