(define-module (crates-io le xi lexicon) #:use-module (crates-io))

(define-public crate-lexicon-0.1.0 (c (n "lexicon") (v "0.1.0") (d (list (d (n "bincode") (r "^1.2.1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0ydzf6y2s0al8ipmraq38k006zrg9anmkplmp5ai05r5jrw26gv2")))

(define-public crate-lexicon-0.1.1 (c (n "lexicon") (v "0.1.1") (d (list (d (n "bincode") (r "^1.2.1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1n5z16lsad7gnjj0mabaxfpqd5969caq9c6cdbx85qrswkzzab0g")))

(define-public crate-lexicon-0.1.2 (c (n "lexicon") (v "0.1.2") (d (list (d (n "bincode") (r "^1.2.1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0qkf2fc04vm5a9m0c71icanfr494c59m3yjxzq5pbbzwncdyx9rj")))

