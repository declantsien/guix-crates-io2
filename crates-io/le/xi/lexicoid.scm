(define-module (crates-io le xi lexicoid) #:use-module (crates-io))

(define-public crate-lexicoid-0.1.0 (c (n "lexicoid") (v "0.1.0") (d (list (d (n "data-encoding") (r "^2.3.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "09d8idcich5mks9s8ddpd16f0kal5n007xnh4a3rhn0mdpjbvlrb")))

