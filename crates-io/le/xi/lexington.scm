(define-module (crates-io le xi lexington) #:use-module (crates-io))

(define-public crate-lexington-0.1.0 (c (n "lexington") (v "0.1.0") (h "1kg890zg587fxyhvlmsvdw0wzwn3jbvcpx5wlbvwia0p3nwsb639")))

(define-public crate-lexington-0.2.0 (c (n "lexington") (v "0.2.0") (h "0gr17n5fxq4b0gg7vv84wmy970fzs7393ws3ahz2yjbad2xpl3kg")))

(define-public crate-lexington-0.3.0 (c (n "lexington") (v "0.3.0") (h "0xfv1gpx36s1zb0f6p4vkp6di1xswr4cxwalf8mi78gm0pzza9n7")))

