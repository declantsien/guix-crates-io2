(define-module (crates-io le xi lexiclean-cow) #:use-module (crates-io))

(define-public crate-lexiclean-cow-0.1.0 (c (n "lexiclean-cow") (v "0.1.0") (d (list (d (n "camino") (r "^1.0.7") (o #t) (d #t) (k 0)))) (h "0xhcr5bb8r090ik7hd5ys3x3ym0rjda6vmzzwd1vs6mfjbxdz5iz") (f (quote (("default")))) (s 2) (e (quote (("camino" "dep:camino"))))))

(define-public crate-lexiclean-cow-0.1.1 (c (n "lexiclean-cow") (v "0.1.1") (d (list (d (n "camino") (r "^1.0.7") (o #t) (d #t) (k 0)))) (h "0wha73kl0na1bm9l9hn0nwy9778mf4anvfqz3y6cihn12bnbcygf") (f (quote (("default")))) (s 2) (e (quote (("camino" "dep:camino"))))))

