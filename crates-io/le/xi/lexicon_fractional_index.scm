(define-module (crates-io le xi lexicon_fractional_index) #:use-module (crates-io))

(define-public crate-lexicon_fractional_index-0.0.1-a1 (c (n "lexicon_fractional_index") (v "0.0.1-a1") (h "1dzpl0d2r721g0xqk9b5lpcsd04lyg4238awywqbgvz85pw86zwi")))

(define-public crate-lexicon_fractional_index-0.0.1 (c (n "lexicon_fractional_index") (v "0.0.1") (h "076qq2fz21pcg7jq0j0g2zc2xl2rx2lp72frpyikbgap5zcp3yd6")))

(define-public crate-lexicon_fractional_index-0.0.2 (c (n "lexicon_fractional_index") (v "0.0.2") (h "1f9wpi1j6icnhhc672n28ddw9piwyfg3p77szh1xyx6alpwzcb8v")))

(define-public crate-lexicon_fractional_index-0.0.3 (c (n "lexicon_fractional_index") (v "0.0.3") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "1lv7f3v1lxbh5gvn34jyy92mrv6p03ivpnvvd0xy546n9m9m4k5i")))

(define-public crate-lexicon_fractional_index-0.0.4-a1 (c (n "lexicon_fractional_index") (v "0.0.4-a1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "1f9qgwamhldnfcnxhj0ff7kngbj42zyjfw1941m8ixwzj7h7m0d9")))

