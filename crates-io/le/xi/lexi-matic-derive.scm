(define-module (crates-io le xi lexi-matic-derive) #:use-module (crates-io))

(define-public crate-lexi-matic-derive-0.1.0 (c (n "lexi-matic-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "regex-automata") (r "^0.4.4") (f (quote ("alloc" "dfa" "syntax" "unicode" "perf"))) (k 0)) (d (n "regex-syntax") (r "^0.8.2") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (d #t) (k 0)))) (h "0j055ngl12v7gv3pdgrn8ammwnkq3n8i94rmjnyi0dz252w8wibc")))

