(define-module (crates-io le xi lexical_bool) #:use-module (crates-io))

(define-public crate-lexical_bool-0.1.1 (c (n "lexical_bool") (v "0.1.1") (d (list (d (n "once_cell") (r "^0.2.3") (d #t) (k 0)))) (h "13p4idsiagnxfs23dd0mz0jx6anmp7k5wzdfpl6kzjmafrk2c538")))

(define-public crate-lexical_bool-0.1.2 (c (n "lexical_bool") (v "0.1.2") (d (list (d (n "once_cell") (r "^0.2.3") (d #t) (k 0)))) (h "13cmp1f8h46h1im558j6s1vdwk9y51yv9ghfyla72jhg4marf495")))

(define-public crate-lexical_bool-0.1.3 (c (n "lexical_bool") (v "0.1.3") (d (list (d (n "once_cell") (r "^1.2.0") (d #t) (k 0)))) (h "1f6ndx5p48civ027f4kkx7n6n7bsh72ik4b9px820901s3n97a3m")))

