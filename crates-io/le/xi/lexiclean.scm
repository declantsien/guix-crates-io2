(define-module (crates-io le xi lexiclean) #:use-module (crates-io))

(define-public crate-lexiclean-0.0.0 (c (n "lexiclean") (v "0.0.0") (h "0as68idi00544b7zvjc2jwvg7sayc801r160218hnlpa0vmivc8v")))

(define-public crate-lexiclean-0.0.1 (c (n "lexiclean") (v "0.0.1") (h "1rb8sky7gi75a7xxn6xrfkrbqkp465npm54p5s89ysqhgc0ja4j4")))

