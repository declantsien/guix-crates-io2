(define-module (crates-io le xi lexical-sort) #:use-module (crates-io))

(define-public crate-lexical-sort-0.1.0 (c (n "lexical-sort") (v "0.1.0") (d (list (d (n "deunicode") (r "^1.1.1") (d #t) (k 0)))) (h "0m6lxc9f161nhq8fd58dv22jr1iahdzc4dwwhrpdv1xqjq62dxpz")))

(define-public crate-lexical-sort-0.2.0 (c (n "lexical-sort") (v "0.2.0") (d (list (d (n "alphanumeric-sort") (r "^1.1") (d #t) (k 2)) (d (n "any_ascii") (r "^0.1.5") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0mv9rsfcn2k8vx50i7lfcd8n9xss5d8cfgw2lkp6jkgzc2dnznkr")))

(define-public crate-lexical-sort-0.2.1 (c (n "lexical-sort") (v "0.2.1") (d (list (d (n "alphanumeric-sort") (r "^1.1") (d #t) (k 2)) (d (n "any_ascii") (r "^0.1.5") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "071jc8zz1zaadf77rqkzn2yq5x69rk27zk58v11hq25685mpcqgy")))

(define-public crate-lexical-sort-0.3.0-pre.1 (c (n "lexical-sort") (v "0.3.0-pre.1") (d (list (d (n "alphanumeric-sort") (r "^1.1") (d #t) (k 2)) (d (n "any_ascii") (r "^0.1.5") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0wvg21a86nsc1z2wrsds5iqb4jjrakiraxm90dgnk5688fjscg71") (y #t)))

(define-public crate-lexical-sort-0.3.0 (c (n "lexical-sort") (v "0.3.0") (d (list (d (n "alphanumeric-sort") (r "^1.1") (d #t) (k 2)) (d (n "any_ascii") (r "^0.1.5") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0cnc3rrvayqklzqwavnb6idbb32l83vbh8z27ncfg2mn1ifkx60l")))

(define-public crate-lexical-sort-0.3.1 (c (n "lexical-sort") (v "0.3.1") (d (list (d (n "alphanumeric-sort") (r "^1.1") (d #t) (k 2)) (d (n "any_ascii") (r "^0.1.6") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rust_icu_ucol") (r "^0.3") (d #t) (k 2)) (d (n "rust_icu_ustring") (r "^0.3") (d #t) (k 2)))) (h "0yi0jzlvjaszwl5a49r0a0gcq404rdk5ls2c9npis8qyc68lb7n0") (f (quote (("std") ("default" "std"))))))

