(define-module (crates-io le xi lexical_scanner) #:use-module (crates-io))

(define-public crate-lexical_scanner-0.1.0 (c (n "lexical_scanner") (v "0.1.0") (h "10b6kr3jrkbllwmv7lb8yn8ywlhb66cdi289m19j4fac8chk1y0z")))

(define-public crate-lexical_scanner-0.1.1 (c (n "lexical_scanner") (v "0.1.1") (h "13axhip7da8ljpg3iwwgwipr7m6c93r6mw1dvir540j1sc4r63pf")))

(define-public crate-lexical_scanner-0.1.2 (c (n "lexical_scanner") (v "0.1.2") (h "09v36rns7jp0yhv8rxjijgsi8ggl3v6c52p1vqx52gw4f6097w64")))

(define-public crate-lexical_scanner-0.1.3 (c (n "lexical_scanner") (v "0.1.3") (h "0mhcql1m5kwra71ryfrim9z8qvkd1zv6dn77jg4pkr2ljkydz5lr")))

(define-public crate-lexical_scanner-0.1.4 (c (n "lexical_scanner") (v "0.1.4") (h "0knxg86bvcbid46093bkhz7vqmhjjvwi07i43km3j06rzgg27ysb")))

(define-public crate-lexical_scanner-0.1.5 (c (n "lexical_scanner") (v "0.1.5") (h "17f3vg41xi9hs4ms9dgwaqkpg3lar3hk81ylw5xspas775br1gzp")))

(define-public crate-lexical_scanner-0.1.6 (c (n "lexical_scanner") (v "0.1.6") (h "1gc2r03rjsjw0m3bjg3699pa67wpyqsww3gc57y6xglbvqsbx71j")))

(define-public crate-lexical_scanner-0.1.7 (c (n "lexical_scanner") (v "0.1.7") (h "08kdyjfdngrc4fcb9zpnnv8b0na926s3whxy6iqfyiw7ngj2jsdj")))

(define-public crate-lexical_scanner-0.1.8 (c (n "lexical_scanner") (v "0.1.8") (h "1hfpfvbik9pkaap7w5jcrf63s8x89ibqbp986znywzccvwd13gzn")))

(define-public crate-lexical_scanner-0.1.9 (c (n "lexical_scanner") (v "0.1.9") (h "0dl412v8hyxvj0v9zqgzd54lbfbmk778m3cavlhf7qlgb2hqvy9a")))

(define-public crate-lexical_scanner-0.1.10 (c (n "lexical_scanner") (v "0.1.10") (h "0492k8rya18f59ys4i9if6ch4za39ifwdb6cc7p0p5qs0vqswa2f")))

(define-public crate-lexical_scanner-0.1.11 (c (n "lexical_scanner") (v "0.1.11") (h "0zzkmvlfdvswabjcr0kpdc63xxm7x982kny9pmvmpavz1bw80yba")))

(define-public crate-lexical_scanner-0.1.12 (c (n "lexical_scanner") (v "0.1.12") (h "0nvhsic0bbf8qy35za91hilvscb7zi53qzjdi7ljs3yni9ry4vhc")))

(define-public crate-lexical_scanner-0.1.13 (c (n "lexical_scanner") (v "0.1.13") (h "1vp4yby5swyyfzrw5hrb02crx2miysjsfj3ci7bfxg6yx9klcmg2")))

(define-public crate-lexical_scanner-0.1.14 (c (n "lexical_scanner") (v "0.1.14") (h "005a5zr795qcva6z8l0x14wr4yw9f04pz8mr26vxcr6br63y62hp")))

(define-public crate-lexical_scanner-0.1.15 (c (n "lexical_scanner") (v "0.1.15") (h "0qrfnlhiyvyhld5v8k6nrrka62rcjkyb0c96207dlfa63migp965")))

(define-public crate-lexical_scanner-0.1.16 (c (n "lexical_scanner") (v "0.1.16") (h "1b67zg149skr922w8pbg4bagvjqwsbzcf6imm41ivijikvanxq1n")))

(define-public crate-lexical_scanner-0.1.17 (c (n "lexical_scanner") (v "0.1.17") (h "0qy8h1qc3wl5d092g82k860xw6ahqgpa59w5p62659s7idzvsyng")))

(define-public crate-lexical_scanner-0.1.18 (c (n "lexical_scanner") (v "0.1.18") (h "15ak9c70q9fggqnq85frbj8fa4v2j40n7g6pbm4w1l8d6z1zp3yi")))

