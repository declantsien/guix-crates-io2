(define-module (crates-io le xi lexify) #:use-module (crates-io))

(define-public crate-lexify-0.1.0 (c (n "lexify") (v "0.1.0") (d (list (d (n "chomper") (r "^0.1.5") (d #t) (k 0)))) (h "0vznaqkdjw8li7jnq62ib2hkzh5x4yxg2pwfp4pdpxin5rfrcy51") (y #t)))

(define-public crate-lexify-0.1.1 (c (n "lexify") (v "0.1.1") (d (list (d (n "chomper") (r "^0.1.5") (d #t) (k 0)))) (h "1ljpg642h02v91m1hll2ajhqr7ykangb11grqrgxxii9dq4nzkr1") (y #t)))

(define-public crate-lexify-0.1.2 (c (n "lexify") (v "0.1.2") (d (list (d (n "chomper") (r "^0.1.5") (d #t) (k 0)))) (h "1i3vpvr18ydn7zmrwk6ypd9sqvkv7qvj0lmad8612p24rg5fzcvp") (y #t)))

(define-public crate-lexify-0.1.3 (c (n "lexify") (v "0.1.3") (d (list (d (n "chomper") (r "^0.1.8") (d #t) (k 0)))) (h "0dnzz1407hf6i6m2i0i0qvv4yd4gay0687qy45jzk73iaxs6vydf") (y #t)))

(define-public crate-lexify-0.1.4 (c (n "lexify") (v "0.1.4") (d (list (d (n "chomper") (r "^0.1.8") (d #t) (k 0)))) (h "04pgnj5j04n05mxkyhcyrh92vy17r4xvwbl1lm83h19hv619scdj") (y #t)))

(define-public crate-lexify-0.1.5 (c (n "lexify") (v "0.1.5") (d (list (d (n "chomper") (r "^0.1.8") (d #t) (k 0)))) (h "1k9nkghrqar7q9kdp5pvvwpw9v3vbxf7z4lvf6sk0yk6pxzc8866") (y #t)))

(define-public crate-lexify-0.1.6 (c (n "lexify") (v "0.1.6") (d (list (d (n "chomper") (r "^0.1.8") (d #t) (k 0)))) (h "18nnckq2cdqzwlcb7ldg72bnxz7iig1nrzjad3vhihl75vxf279w") (y #t)))

