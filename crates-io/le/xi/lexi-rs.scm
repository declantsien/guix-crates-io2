(define-module (crates-io le xi lexi-rs) #:use-module (crates-io))

(define-public crate-lexi-rs-0.1.0 (c (n "lexi-rs") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("rt" "rt-multi-thread" "net" "io-util" "macros"))) (d #t) (k 0)))) (h "1062g8r4f3bcqabq8ipid6gp77k0sg52pqhdwngx9szsvqpkh4fz")))

(define-public crate-lexi-rs-0.1.1 (c (n "lexi-rs") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("rt" "rt-multi-thread" "net" "io-util" "macros"))) (d #t) (k 0)))) (h "1jpvxy7321pwnpx2fhnlnxblyy337a5ips4c9p7qcngdnpk81y0d")))

(define-public crate-lexi-rs-0.1.2 (c (n "lexi-rs") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("rt" "rt-multi-thread" "net" "io-util" "macros"))) (d #t) (k 0)))) (h "162ji24kyqv72k786zv5msba2a7x5xriq6vny60jrdgkavrj9qxc")))

(define-public crate-lexi-rs-0.1.3 (c (n "lexi-rs") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "serde") (r "^1.0.190") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("rt" "rt-multi-thread" "net" "io-util" "macros"))) (d #t) (k 0)))) (h "1mvw717shc2h4hfmykayvhhfg0cn78d8apfz1kw6q643x45mn0xi")))

(define-public crate-lexi-rs-0.1.4 (c (n "lexi-rs") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "serde") (r "^1.0.190") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("rt" "rt-multi-thread" "net" "io-util" "macros"))) (d #t) (k 0)))) (h "0vlyznzyqmrfkgb7bp85m669vi1nfxyaagirbxw121y1401z1mw4")))

(define-public crate-lexi-rs-0.1.5 (c (n "lexi-rs") (v "0.1.5") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "serde") (r "^1.0.190") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("rt" "rt-multi-thread" "net" "io-util" "macros"))) (d #t) (k 0)))) (h "1383n7jxqmhs9c7xi1nd1992ajzc5h2gr7j1cgpc6vmiq87nb153")))

(define-public crate-lexi-rs-0.1.6 (c (n "lexi-rs") (v "0.1.6") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "serde") (r "^1.0.190") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("rt" "rt-multi-thread" "net" "io-util" "macros"))) (d #t) (k 0)))) (h "06k27a70yllrlfkj1szkik6w6rnxhbk5by7yr34k1lsr497yh3y4")))

(define-public crate-lexi-rs-0.1.7 (c (n "lexi-rs") (v "0.1.7") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "serde") (r "^1.0.190") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("rt" "rt-multi-thread" "net" "io-util" "macros"))) (d #t) (k 0)))) (h "0465qrgk4nzra7v2acvj5d06d43ynxmbal5wmrl14llv3sfvs6yj")))

(define-public crate-lexi-rs-0.1.8 (c (n "lexi-rs") (v "0.1.8") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "serde") (r "^1.0.190") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("rt" "rt-multi-thread" "net" "io-util" "macros"))) (d #t) (k 0)))) (h "18rx30502fvinli0433pvy1k2zlv18zccr9wv5p0dwak2dqym1sk")))

(define-public crate-lexi-rs-0.1.9 (c (n "lexi-rs") (v "0.1.9") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "serde") (r "^1.0.190") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("rt" "rt-multi-thread" "net" "io-util" "macros"))) (d #t) (k 0)))) (h "0gs0b6dhb39yqgdrfb8cizf09j42zrxznxc6vgr300j11papwv0j")))

