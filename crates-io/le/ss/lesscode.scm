(define-module (crates-io le ss lesscode) #:use-module (crates-io))

(define-public crate-lesscode-0.1.0 (c (n "lesscode") (v "0.1.0") (h "0nmvacm44xlc2dmqapma4jwyhc7mdf8q115ays86syiydwd19g23")))

(define-public crate-lesscode-0.1.1 (c (n "lesscode") (v "0.1.1") (d (list (d (n "mongodb") (r "^2.6.1") (f (quote ("sync"))) (k 0)) (d (n "mysql") (r "^24.0.0") (d #t) (k 0)))) (h "08kzhxwjxxrjl0az36amx9rb4wmh0c3bl6cd2f7vm1xx7abrlz4y")))

(define-public crate-lesscode-0.1.2 (c (n "lesscode") (v "0.1.2") (d (list (d (n "mongodb") (r "^2.6.1") (d #t) (k 0)) (d (n "mysql") (r "^24.0.0") (d #t) (k 0)) (d (n "neo4rs") (r "^0.6.2") (d #t) (k 0)))) (h "1lnm04fl99b6qlk2kfhr635vdiq9mybfs5z34g4kjfnag9bbipaz")))

(define-public crate-lesscode-0.1.3 (c (n "lesscode") (v "0.1.3") (d (list (d (n "mongodb") (r "^2.6.1") (d #t) (k 0)) (d (n "mysql") (r "^24.0.0") (d #t) (k 0)) (d (n "neo4rs") (r "^0.6.2") (d #t) (k 0)) (d (n "postgres") (r "^0.19.7") (d #t) (k 0)) (d (n "redis") (r "^0.23.3") (d #t) (k 0)))) (h "1ia3kh409w4kk2g5p3h6j7cv4q8ywyg5dwra2kh6lhnr6hbw2lx2")))

(define-public crate-lesscode-0.1.4 (c (n "lesscode") (v "0.1.4") (d (list (d (n "mongodb") (r "^2.6.1") (d #t) (k 0)) (d (n "mysql") (r "^24.0.0") (d #t) (k 0)) (d (n "neo4rs") (r "^0.6.2") (d #t) (k 0)) (d (n "postgres") (r "^0.19.7") (d #t) (k 0)) (d (n "redis") (r "^0.23.2") (d #t) (k 0)))) (h "1fdvfv8sdkx853r99190v7fa6xgrpz1ifp82brfnyrg13agjm989")))

