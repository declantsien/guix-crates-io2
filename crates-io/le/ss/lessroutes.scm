(define-module (crates-io le ss lessroutes) #:use-module (crates-io))

(define-public crate-lessroutes-0.1.0 (c (n "lessroutes") (v "0.1.0") (d (list (d (n "clap") (r "~2.33.0") (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "futures-preview") (r "= 0.3.0-alpha.19") (f (quote ("async-await"))) (d #t) (k 0)) (d (n "ipnet") (r "^2.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.0-alpha.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "simplelog") (r "^0.7.4") (d #t) (k 0)) (d (n "tokio") (r "^0.2.0-alpha.6") (d #t) (k 0)))) (h "1pj17llr22ha2nf59y6srlrlgrlap26l2y5bxqg0hkbwi6zf0q67")))

