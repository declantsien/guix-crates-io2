(define-module (crates-io le ss lesson6_publishing_your_crate) #:use-module (crates-io))

(define-public crate-lesson6_publishing_your_crate-0.1.0 (c (n "lesson6_publishing_your_crate") (v "0.1.0") (h "19vg0g0xkbv015mrkw3011r6yyr90jwi28bx72sqpd74my07vmhg")))

(define-public crate-lesson6_publishing_your_crate-0.1.1 (c (n "lesson6_publishing_your_crate") (v "0.1.1") (h "0gmnmphmqsx7biy8pmk8z73k410mb6jqcqvzk7fi9p7sz3gxwp4j")))

