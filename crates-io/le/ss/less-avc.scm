(define-module (crates-io le ss less-avc) #:use-module (crates-io))

(define-public crate-less-avc-0.1.0 (c (n "less-avc") (v "0.1.0") (d (list (d (n "bitvec") (r "^1.0.1") (d #t) (k 0)) (d (n "h264-reader") (r "^0.6.0") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1zbqyqii0z090g9cpbs06d6hiqsjpd3k2118f2wxwdvqkbzdi4ws") (f (quote (("backtrace"))))))

(define-public crate-less-avc-0.1.1 (c (n "less-avc") (v "0.1.1") (d (list (d (n "bitvec") (r "^1.0.1") (d #t) (k 0)) (d (n "h264-reader") (r "^0.6.0") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1qgvkb9srq67lplpa4x7gifzaclqpsas37bbycmpagx7r60gclq6") (f (quote (("backtrace"))))))

(define-public crate-less-avc-0.1.2 (c (n "less-avc") (v "0.1.2") (d (list (d (n "bitvec") (r "^1.0.1") (d #t) (k 0)) (d (n "h264-reader") (r "^0.6.0") (d #t) (k 2)) (d (n "memchr") (r "^2.5.0") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1gzhllfpzaxrbs36xqpdjfg0vizr7ic037vb5yhmw5lixgzdl7w8") (f (quote (("backtrace"))))))

(define-public crate-less-avc-0.1.3 (c (n "less-avc") (v "0.1.3") (d (list (d (n "bitvec") (r "^1.0.1") (d #t) (k 0)) (d (n "h264-reader") (r "^0.6.0") (d #t) (k 2)) (d (n "memchr") (r "^2.5.0") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1ivaykijzi93qp5hdm66jvdm47i33b1d23c1vkvvkkb104phy3s2") (f (quote (("backtrace"))))))

(define-public crate-less-avc-0.1.4 (c (n "less-avc") (v "0.1.4") (d (list (d (n "bitvec") (r "^1.0.1") (f (quote ("alloc"))) (k 0)) (d (n "h264-reader") (r "^0.6.0") (d #t) (k 2)) (d (n "memchr") (r "^2.5.0") (k 0)))) (h "0dwmkj3fhimshn8s2fmk6qxaqqdc3fw36myq3f9sljqjsb78afm9") (f (quote (("std") ("default" "std") ("backtrace"))))))

(define-public crate-less-avc-0.1.5 (c (n "less-avc") (v "0.1.5") (d (list (d (n "bitvec") (r "^1.0.1") (f (quote ("alloc"))) (k 0)) (d (n "h264-reader") (r "^0.7.0") (d #t) (k 2)) (d (n "memchr") (r "^2.5.0") (k 0)))) (h "1gxr3fsgl5ha57dk5iln8hnn8j467l829w30islvx487xs9lxpyp") (f (quote (("std") ("default" "std") ("backtrace"))))))

