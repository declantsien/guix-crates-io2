(define-module (crates-io le ss lessanvil) #:use-module (crates-io))

(define-public crate-lessanvil-0.1.0 (c (n "lessanvil") (v "0.1.0") (d (list (d (n "anstream") (r "^0.2.6") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "clap") (r "^4.1.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "fastanvil") (r "^0.27.0") (d #t) (k 0)) (d (n "fastnbt") (r "^2.3.2") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.3") (f (quote ("rayon"))) (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "owo-colors") (r "^3.5.0") (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (d #t) (k 0)))) (h "0k1r4cc9vylxvckzgqsixpf5xmhbkk28xz1hn7m2j68yy5ffiw90")))

(define-public crate-lessanvil-1.0.0 (c (n "lessanvil") (v "1.0.0") (d (list (d (n "fastanvil") (r "^0.28.0") (k 0)) (d (n "fastnbt") (r "^2.3.2") (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0smfjs5l6slhxzdybfhcd2z59q393shdkls6c5plsmdb90r12mjb")))

(define-public crate-lessanvil-1.0.1 (c (n "lessanvil") (v "1.0.1") (d (list (d (n "fastanvil") (r "^0.28.0") (k 0)) (d (n "fastnbt") (r "^2.3.2") (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "066brkiamwdf38f73q6m5qwx5lmrxv98l09syrvwnyk6wjb0hl6k")))

(define-public crate-lessanvil-1.0.2 (c (n "lessanvil") (v "1.0.2") (d (list (d (n "fastanvil") (r "^0.28.0") (k 0)) (d (n "fastnbt") (r "^2.3.2") (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "1ziicm59gazp055fav6hhla2m0gg9x5ymi814yjh4afy9d08y583")))

