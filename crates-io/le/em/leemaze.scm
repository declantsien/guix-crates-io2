(define-module (crates-io le em leemaze) #:use-module (crates-io))

(define-public crate-leemaze-0.1.0 (c (n "leemaze") (v "0.1.0") (h "0098wqpvxwijg0qjqwvgq7wmm2s4ny2cfmvhzfd9wpngdb4d4llk")))

(define-public crate-leemaze-0.1.1 (c (n "leemaze") (v "0.1.1") (h "03h48sg8qjkhgxilhih2pbk02076mh15sfhgszlb6ni8qlmvs6ml")))

(define-public crate-leemaze-0.1.11 (c (n "leemaze") (v "0.1.11") (h "0098xkmnhzgd3mwhs25bx1fk1fws4l1jc6mz0avbhf1283i3indn")))

