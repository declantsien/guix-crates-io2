(define-module (crates-io le ap leap-seconds) #:use-module (crates-io))

(define-public crate-leap-seconds-0.1.0 (c (n "leap-seconds") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.23") (f (quote ("clock"))) (k 2)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "reqwest") (r "^0.11.13") (f (quote ("blocking"))) (d #t) (k 2)) (d (n "sha1") (r "^0.10.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (k 0)))) (h "1y9yc47frr60g5zbk4a7yajnp2kx0nd51ngwhgzabrl05a0gpd4p")))

