(define-module (crates-io le ap leapfrog) #:use-module (crates-io))

(define-public crate-leapfrog-0.1.0 (c (n "leapfrog") (v "0.1.0") (d (list (d (n "atomic") (r "^0.5.1") (d #t) (k 0)) (d (n "core_affinity") (r "^0.5.10") (d #t) (k 2)) (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "1c2l1n80qifbs2zkfw68dvhnqxfpazp5afqc2acf5jp1nrqw4bw5")))

(define-public crate-leapfrog-0.1.1 (c (n "leapfrog") (v "0.1.1") (d (list (d (n "atomic") (r "^0.5.1") (d #t) (k 0)) (d (n "core_affinity") (r "^0.5.10") (d #t) (k 2)) (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "1jfdbi2by464lmfypj2imgqv8z6w0vg4b71kwil1d86d85nhlgyf")))

(define-public crate-leapfrog-0.1.2 (c (n "leapfrog") (v "0.1.2") (d (list (d (n "atomic") (r "^0.5.1") (d #t) (k 0)) (d (n "core_affinity") (r "^0.5.10") (d #t) (k 2)) (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "1kpnp84dw7g6z25pqj0vcdj6sid58n5jpylz7q6syrji96mjgvya")))

(define-public crate-leapfrog-0.2.0 (c (n "leapfrog") (v "0.2.0") (d (list (d (n "atomic") (r "^0.5.1") (d #t) (k 0)) (d (n "core_affinity") (r "^0.5.10") (d #t) (k 2)) (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "serde_crate") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0) (p "serde")) (d (n "serde_json") (r "^1.0.50") (d #t) (k 2)))) (h "0aqxm9q2pghnbbdlqnsvm1qyn16c5l09yjpv95c8znqv9823rnk7") (f (quote (("serde") ("default"))))))

(define-public crate-leapfrog-0.2.1 (c (n "leapfrog") (v "0.2.1") (d (list (d (n "atomic") (r "^0.5.1") (d #t) (k 0)) (d (n "core_affinity") (r "^0.5.10") (d #t) (k 2)) (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "serde_crate") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0) (p "serde")) (d (n "serde_json") (r "^1.0.50") (d #t) (k 2)))) (h "0qzw1iia186ysjwsln2a3lkqmdwsfk5qp9w9jwy7vmd4z2yfrr0j") (f (quote (("serde") ("default"))))))

(define-public crate-leapfrog-0.2.2 (c (n "leapfrog") (v "0.2.2") (d (list (d (n "atomic") (r "^0.5.1") (d #t) (k 0)) (d (n "core_affinity") (r "^0.5.10") (d #t) (k 2)) (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "serde_crate") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0) (p "serde")) (d (n "serde_json") (r "^1.0.50") (d #t) (k 2)))) (h "04wcwaafxz4x17gzr4dckbcky85f844x6p1wl3b0z2h0cq8w6k1b") (f (quote (("serde") ("default"))))))

(define-public crate-leapfrog-0.3.0 (c (n "leapfrog") (v "0.3.0") (d (list (d (n "allocator-api2") (r "^0.2.15") (o #t) (d #t) (k 0)) (d (n "atomic") (r "^0.5.1") (d #t) (k 0)) (d (n "core_affinity") (r "^0.5.10") (d #t) (k 2)) (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "serde_crate") (r "^1.0.136") (f (quote ("derive"))) (o #t) (d #t) (k 0) (p "serde")) (d (n "serde_json") (r "^1.0.50") (d #t) (k 2)))) (h "1l3n1kds12mnsnq3xxixks089rwa2v0ahdw0hh7lsy417n2k238m") (f (quote (("default")))) (s 2) (e (quote (("stable_alloc" "dep:allocator-api2") ("serde" "dep:serde_crate"))))))

