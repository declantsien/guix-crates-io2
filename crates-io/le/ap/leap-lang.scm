(define-module (crates-io le ap leap-lang) #:use-module (crates-io))

(define-public crate-leap-lang-0.1.0 (c (n "leap-lang") (v "0.1.0") (h "04vq9snwqsp7mnqn1d0pdazcipk3c0m4hm3fyzain1wv7bqkzn5m")))

(define-public crate-leap-lang-0.1.1 (c (n "leap-lang") (v "0.1.1") (h "18skpxl5syfpx92i9x7zsmwss7hfm6qjir6h0d13z64yvg31f61q")))

(define-public crate-leap-lang-0.1.2 (c (n "leap-lang") (v "0.1.2") (h "1jbxr9xpz1fd48qzki2i9499awyqam5j6i38w6nz4h9b2l1apx4f")))

(define-public crate-leap-lang-0.1.3 (c (n "leap-lang") (v "0.1.3") (h "1zifn113xyr918ml5gai0gp5zmjygibiwl1hdyz0xyhqjblg6qdh")))

(define-public crate-leap-lang-0.2.0 (c (n "leap-lang") (v "0.2.0") (h "0pvya7mj6nlfimsrdsjvpa3b725mldi53a2fiafvmpwpgbini4ln")))

(define-public crate-leap-lang-0.3.0 (c (n "leap-lang") (v "0.3.0") (h "1jincn95zn5ics46x0j1xywz0av60l920h19f0xf9hywygscdss1")))

