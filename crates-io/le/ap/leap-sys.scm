(define-module (crates-io le ap leap-sys) #:use-module (crates-io))

(define-public crate-leap-sys-0.1.0 (c (n "leap-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.53.2") (d #t) (k 1)))) (h "1xqp4qvppqkga60dn1rywqjhcyb8vzm4vzklp9s1xw01csr5nfpd")))

(define-public crate-leap-sys-0.1.1 (c (n "leap-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.53.2") (o #t) (d #t) (k 1)))) (h "1nh2hf0dd3gh6xwvd3riniyqk8g7w884gscx0w9gjk75p4k1bbi8") (f (quote (("default") ("buildtime_bindgen" "bindgen"))))))

(define-public crate-leap-sys-0.2.0 (c (n "leap-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.60.1") (o #t) (d #t) (k 1)))) (h "0qi5wlmf32231wayyhn27rbk6kvlg8kgxiw3znvb7d6389skqq13") (f (quote (("default") ("buildtime_bindgen" "bindgen"))))))

