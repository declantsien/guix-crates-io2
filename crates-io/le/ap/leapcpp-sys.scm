(define-module (crates-io le ap leapcpp-sys) #:use-module (crates-io))

(define-public crate-leapcpp-sys-0.1.0 (c (n "leapcpp-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.60.1") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0.71") (d #t) (k 1)))) (h "1fphrl92vxf9dbv9jicnz7z7a7bqq8376irax28lg6g8yzcm23b8") (f (quote (("regen" "bindgen"))))))

