(define-module (crates-io le ap leapcpp) #:use-module (crates-io))

(define-public crate-leapcpp-0.1.0 (c (n "leapcpp") (v "0.1.0") (d (list (d (n "leapcpp-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "macroquad") (r "^0.3.10") (k 2)))) (h "1rbffzi2d07kzykpqnrkd8cacim3hi343ijypfdby2dccwf89lss")))

