(define-module (crates-io le on leonardo-heap) #:use-module (crates-io))

(define-public crate-leonardo-heap-0.1.0 (c (n "leonardo-heap") (v "0.1.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0gl7bgkz2h2kqr24nxy34q47r736ic3853l68r592rc8p9v9i6az")))

(define-public crate-leonardo-heap-0.2.0 (c (n "leonardo-heap") (v "0.2.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "1vrvq8xrzlqkxcpy6rby2vdyg36zgfyngzp9id47mg7wyaayhmk7")))

(define-public crate-leonardo-heap-0.3.0 (c (n "leonardo-heap") (v "0.3.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "1qvnj5j22jpf1fidxa8c7v7613kp84jvzcpar80w1h1pjnnb71zw")))

