(define-module (crates-io le on leon-macros) #:use-module (crates-io))

(define-public crate-leon-macros-1.0.0 (c (n "leon-macros") (v "1.0.0") (d (list (d (n "leon") (r "^2.0.0") (k 0)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("proc-macro" "parsing"))) (k 0)))) (h "0km969c0rxfh3xiki2fbq2iskfvnwg6cwgn28zj4wzcchqwb4wba") (r "1.61.0")))

(define-public crate-leon-macros-1.0.1 (c (n "leon-macros") (v "1.0.1") (d (list (d (n "leon") (r "^3.0.0") (k 0)) (d (n "proc-macro2") (r "^1.0.68") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.43") (f (quote ("proc-macro" "parsing"))) (k 0)))) (h "1an6fzm2r6ml0m57a0lgbcz1v4sr9wm4d7j5b3r55baxvvx19vc5") (r "1.61.0")))

(define-public crate-leon-macros-1.0.2 (c (n "leon-macros") (v "1.0.2") (d (list (d (n "leon") (r "^3.0.0") (k 0)) (d (n "proc-macro2") (r "^1.0.68") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.43") (f (quote ("proc-macro" "parsing"))) (k 0)))) (h "1aj4jnk8v8i8fz1q6k4v99c63x8v3hgx0fh7vvdgxwg18qan5wxc") (r "1.61.0")))

