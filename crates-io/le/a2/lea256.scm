(define-module (crates-io le a2 lea256) #:use-module (crates-io))

(define-public crate-lea256-0.1.0 (c (n "lea256") (v "0.1.0") (h "1vqdk8f2xvq0zkkwcw4hcq7jnh8z8rp8zgd128ps07spxj7g4xd9") (y #t)))

(define-public crate-lea256-0.1.1 (c (n "lea256") (v "0.1.1") (h "1md94gbf4naxxrnskp0cfb7mc9w6inff4xxjhh4rbr38raajf3h7") (y #t)))

(define-public crate-lea256-0.1.2 (c (n "lea256") (v "0.1.2") (h "0hb56z8r7fhjq7b05rp409pzp3n2j2g66vg4cvbfrpdyzp7hs00n") (y #t)))

(define-public crate-lea256-0.2.0 (c (n "lea256") (v "0.2.0") (d (list (d (n "block-cipher-trait") (r "0.6.*") (d #t) (k 0)))) (h "1ihd0rr04rkz10d331xbzk24sby4mdw2wrsvn1l4fhg36z3pcf9l") (y #t)))

(define-public crate-lea256-0.2.1 (c (n "lea256") (v "0.2.1") (d (list (d (n "block-cipher-trait") (r "0.6.*") (d #t) (k 0)))) (h "155ch29xks89qrzwn6lacmib2qs3kk7f7kra4r92b7y7dpi3k6yj") (y #t)))

