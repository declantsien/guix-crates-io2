(define-module (crates-io le bi lebicon) #:use-module (crates-io))

(define-public crate-lebicon-1.0.0 (c (n "lebicon") (v "1.0.0") (d (list (d (n "codicon") (r "^1.0.0") (d #t) (k 0)) (d (n "leb128") (r "^0.2.1") (d #t) (k 2)) (d (n "signrel") (r "^1.0.0") (d #t) (k 0)) (d (n "uabs") (r "^1.0.0") (d #t) (k 0)))) (h "1s1m5v12m68bhxs4j4wy9w8y9gbi8nryfg1wfd911lh2racps2ag")))

(define-public crate-lebicon-2.0.0 (c (n "lebicon") (v "2.0.0") (d (list (d (n "codicon") (r "^1.0.0") (d #t) (k 0)) (d (n "leb128") (r "^0.2.1") (d #t) (k 2)) (d (n "signrel") (r "^1.0.0") (d #t) (k 0)) (d (n "uabs") (r "^1.0.0") (d #t) (k 0)))) (h "1px34xicqzzpc87vh9rrjhwgzdahq6s520d809l5cqysqh6as83z")))

(define-public crate-lebicon-3.0.0 (c (n "lebicon") (v "3.0.0") (d (list (d (n "codicon") (r "^2.0.0") (d #t) (k 0)) (d (n "leb128") (r "^0.2.1") (d #t) (k 2)) (d (n "signrel") (r "^1.0.0") (d #t) (k 0)) (d (n "uabs") (r "^1.0.0") (d #t) (k 0)))) (h "1690sjrgqlw2zsckdmn02x1yyal5mraagl3hpnbmvzmkvw714i51")))

(define-public crate-lebicon-4.0.0 (c (n "lebicon") (v "4.0.0") (d (list (d (n "codicon") (r "^3.0") (d #t) (k 0)) (d (n "leb128") (r "^0.2") (d #t) (k 2)) (d (n "signrel") (r "^2.0") (d #t) (k 0)) (d (n "uabs") (r "^3.0") (d #t) (k 0)))) (h "080ihmyfh2vkasxz7bvy0988wnl64kwg77a8yccp9sari6dbf660")))

