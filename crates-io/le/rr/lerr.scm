(define-module (crates-io le rr lerr) #:use-module (crates-io))

(define-public crate-lerr-0.1.0 (c (n "lerr") (v "0.1.0") (d (list (d (n "unicode-width") (r "^0.1.10") (d #t) (k 0)))) (h "00r9a3n56858g6sf4q2vmxwlj1bi2yg0sn445w6ax2vn9a2damhg")))

(define-public crate-lerr-0.1.1 (c (n "lerr") (v "0.1.1") (d (list (d (n "unicode-width") (r "^0.1.10") (d #t) (k 0)))) (h "17w7vcgp59y3nk9f739b537p9k11gc66p9zpydqdlj3vq05bia8j")))

(define-public crate-lerr-0.1.2 (c (n "lerr") (v "0.1.2") (d (list (d (n "unicode-width") (r "^0.1.10") (d #t) (k 0)))) (h "0pn5nv2pz1vc2f04fxd7ndwc3813b7flkpdkhzhnm1srs3cxrcr8")))

(define-public crate-lerr-0.1.3 (c (n "lerr") (v "0.1.3") (d (list (d (n "anstream") (r "^0.5.0") (k 2)) (d (n "comat") (r "^0.1.0") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.10") (d #t) (k 0)))) (h "0vdladwxwvdzkv0mdjgixknjnpaig7cmcnvdcx9lq8zdl1sigd51")))

(define-public crate-lerr-0.1.4 (c (n "lerr") (v "0.1.4") (d (list (d (n "anstream") (r "^0.5.0") (k 0)) (d (n "comat") (r "^0.1.0") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.10") (d #t) (k 0)))) (h "1c6l8mbdn17y5crl7f37l51881asj9iqg3ghv3vj9zdvragx3w7w")))

(define-public crate-lerr-0.1.5 (c (n "lerr") (v "0.1.5") (d (list (d (n "anstream") (r "^0.5.0") (k 0)) (d (n "comat") (r "^0.1.0") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.10") (d #t) (k 0)))) (h "1d22l22s324vglclrmy563g9cf3x0hnmiwrl5lmy8l01f6cpb2ri")))

