(define-module (crates-io le im leim) #:use-module (crates-io))

(define-public crate-leim-0.1.0 (c (n "leim") (v "0.1.0") (d (list (d (n "async-graphql") (r "^5") (f (quote ("chrono"))) (o #t) (k 0)) (d (n "diesel") (r "^2") (o #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1z126dmw3mbx7sibpbz2wnh7hnsgfdc511vgz4gahm81vximx9kj")))

(define-public crate-leim-0.1.1 (c (n "leim") (v "0.1.1") (d (list (d (n "async-graphql") (r "^5") (f (quote ("chrono"))) (o #t) (k 0)) (d (n "diesel") (r "2.0.*") (o #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "02fdfw1f7iz6zv3a0sqd4i0x4dh9ajrfjjrll3z8ck7f9v6djcfa")))

(define-public crate-leim-0.1.2 (c (n "leim") (v "0.1.2") (d (list (d (n "async-graphql") (r "^5") (f (quote ("chrono"))) (o #t) (k 0)) (d (n "diesel") (r "2.0.*") (o #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0dccmrl3pb0gh5yrqivxk9yw4j5l71s0givp0y1ay6fkxv0bp809")))

(define-public crate-leim-0.2.0 (c (n "leim") (v "0.2.0") (d (list (d (n "async-graphql") (r "^5") (f (quote ("chrono"))) (o #t) (k 0)) (d (n "diesel") (r "^2.1") (o #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_test") (r "^1.0.163") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1r3x95qcz80dpdp197nhws5xc5h5z7izcxbv1qkyqf1r7dyb4vz2")))

