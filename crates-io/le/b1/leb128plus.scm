(define-module (crates-io le b1 leb128plus) #:use-module (crates-io))

(define-public crate-leb128plus-0.2.0 (c (n "leb128plus") (v "0.2.0") (h "03hp859xz4fvbkb60ks5p0125d59sl56sxmdqcphjxfis8hd1dix")))

(define-public crate-leb128plus-0.3.0 (c (n "leb128plus") (v "0.3.0") (h "0zb2kffls7bwy3hcpinfhfqj9gs00rik7hhlj1d7si4f0n613y10")))

(define-public crate-leb128plus-0.4.0 (c (n "leb128plus") (v "0.4.0") (h "0i9ds206l7msf637yknaz8h3dxg2abb0lp57w533kg1vj7pib23z")))

(define-public crate-leb128plus-0.5.0 (c (n "leb128plus") (v "0.5.0") (d (list (d (n "int") (r "^0.1.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.6") (d #t) (k 0)))) (h "04w9z5xrnz505v647gc94n3zssslzajb1kaw2rck44gzwbf8icfc")))

(define-public crate-leb128plus-0.5.1 (c (n "leb128plus") (v "0.5.1") (d (list (d (n "int") (r "^0.2.0") (d #t) (k 0)))) (h "0dp9mj8rkhjm9h4fq49chcvhf4aa80x0irsgqymn7d8n6fr3nvdl")))

(define-public crate-leb128plus-0.6.0 (c (n "leb128plus") (v "0.6.0") (d (list (d (n "int") (r "^0.2.10") (d #t) (k 0)))) (h "1cm298gyjm9d1ay4kz8zh1rpsnc4dq2a3c4zmls9rbckzay7b6qf")))

