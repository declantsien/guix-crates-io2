(define-module (crates-io le b1 leb128fmt) #:use-module (crates-io))

(define-public crate-leb128fmt-0.1.0 (c (n "leb128fmt") (v "0.1.0") (h "1chxm1484a0bly6anh6bd7a99sn355ymlagnwj3yajafnpldkv89") (f (quote (("std") ("default" "std") ("alloc")))) (r "1.56.0")))

