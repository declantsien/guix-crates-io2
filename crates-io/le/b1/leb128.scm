(define-module (crates-io le b1 leb128) #:use-module (crates-io))

(define-public crate-leb128-0.1.0 (c (n "leb128") (v "0.1.0") (d (list (d (n "clippy") (r "^0.0.76") (o #t) (d #t) (k 0)))) (h "0a3dhzhw3dla9i9yabwg60pk8ncfmm6xg17b311601kdqpfl9ah3") (f (quote (("nightly" "clippy"))))))

(define-public crate-leb128-0.2.0 (c (n "leb128") (v "0.2.0") (h "0lh04gbg8lqswp84m2pf62413zix9qfvdxw9acyk4w21xdaykqgs") (f (quote (("nightly"))))))

(define-public crate-leb128-0.2.1 (c (n "leb128") (v "0.2.1") (h "04njpamfckg1sgvcnby8wdr1vz1xs99lyziqkvsv1r476ykj88kr") (f (quote (("nightly"))))))

(define-public crate-leb128-0.2.2 (c (n "leb128") (v "0.2.2") (h "079l6bwilz01zavhnhmczq1lmkq7xkpv4l6ihr66is84fqjl408c") (f (quote (("nightly"))))))

(define-public crate-leb128-0.2.3 (c (n "leb128") (v "0.2.3") (h "1579s5by5ag85nfal5jdcrcxylp3n6z274q4ahgjslq01g95hy36") (f (quote (("nightly"))))))

(define-public crate-leb128-0.2.4 (c (n "leb128") (v "0.2.4") (d (list (d (n "quickcheck") (r "^0.8.0") (d #t) (k 2)))) (h "0aid6qy7qlyqr9rhb3md4s5693b93pdidkfzdw86y3x05dzshxim") (f (quote (("nightly"))))))

(define-public crate-leb128-0.2.5 (c (n "leb128") (v "0.2.5") (d (list (d (n "quickcheck") (r "^0.8.0") (d #t) (k 2)))) (h "0rxxjdn76sjbrb08s4bi7m4x47zg68f71jzgx8ww7j0cnivjckl8") (f (quote (("nightly"))))))

