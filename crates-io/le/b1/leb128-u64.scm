(define-module (crates-io le b1 leb128-u64) #:use-module (crates-io))

(define-public crate-leb128-u64-0.1.0 (c (n "leb128-u64") (v "0.1.0") (d (list (d (n "bytes") (r "^1.4") (k 0)) (d (n "proptest") (r "^1.1") (d #t) (k 2)))) (h "0sjv2p6nkbkjmq32cxzfpc9jzkmmfpysawaps7bw0z5lhrnxvm6n")))

(define-public crate-leb128-u64-0.1.1 (c (n "leb128-u64") (v "0.1.1") (d (list (d (n "bytes") (r "^1.4") (k 0)) (d (n "proptest") (r "^1.1") (d #t) (k 2)))) (h "05576pdywhlav9q37c3qi0gcim5gk7v8ipkqlf5r8fjmqf23g7v5")))

