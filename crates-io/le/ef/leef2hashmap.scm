(define-module (crates-io le ef leef2hashmap) #:use-module (crates-io))

(define-public crate-leef2hashmap-0.1.3 (c (n "leef2hashmap") (v "0.1.3") (h "1jjyl7hx32fpavk12zpbnbr8s5l66b2ndsdcyf18hf12zy09ar9n")))

(define-public crate-leef2hashmap-0.1.4 (c (n "leef2hashmap") (v "0.1.4") (h "0hi43x518bb9si095ib07wwwqz6kahrg7xsc79vra0kn645mszqv")))

