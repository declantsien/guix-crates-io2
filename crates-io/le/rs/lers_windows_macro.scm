(define-module (crates-io le rs lers_windows_macro) #:use-module (crates-io))

(define-public crate-lers_windows_macro-0.0.3 (c (n "lers_windows_macro") (v "0.0.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.58") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "0icr0si2ckfapdyjk9y04ilcxmnj5xdhdkhybzkdfzkpqw9f7a4f")))

