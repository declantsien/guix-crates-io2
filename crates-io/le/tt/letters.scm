(define-module (crates-io le tt letters) #:use-module (crates-io))

(define-public crate-letters-0.0.1 (c (n "letters") (v "0.0.1") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.29.1") (d #t) (k 0)) (d (n "rust_decimal_macros") (r "^1.29.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.162") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uuid") (r "^1.3.2") (d #t) (k 0)) (d (n "xml_serde") (r "^1.2.3") (d #t) (k 0)))) (h "0s4pvi2qv6k85cfww8l7inkbc94l6jh2apf9maibah6s7giq88hy")))

