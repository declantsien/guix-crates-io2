(define-module (crates-io le tt lettre-openssl111_email) #:use-module (crates-io))

(define-public crate-lettre-openssl111_email-0.9.0 (c (n "lettre-openssl111_email") (v "0.9.0") (d (list (d (n "base64") (r "^0.9") (d #t) (k 0)) (d (n "email") (r "^0.0") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "glob") (r "^0.2") (d #t) (k 2)) (d (n "lettre-openssl111") (r "^0.9") (k 0)) (d (n "lettre-openssl111") (r "^0.9") (f (quote ("smtp-transport"))) (d #t) (k 2)) (d (n "mime") (r "^0.3") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "uuid") (r "^0.7") (f (quote ("v4"))) (d #t) (k 0)))) (h "1sb8gbjppds1441fajjc78zigyzc2xnfsw9ggqd64hwpk4x3h230") (y #t)))

