(define-module (crates-io le tt letter-sequence) #:use-module (crates-io))

(define-public crate-letter-sequence-0.1.0 (c (n "letter-sequence") (v "0.1.0") (h "1crb3n2spras0kn1y4i5mkww06hpgy74j3b26xrnjcfqqijmlxlz")))

(define-public crate-letter-sequence-0.2.0 (c (n "letter-sequence") (v "0.2.0") (h "1iw9g72p198va4kjfvlylw5wma7i8b85yrs0nw82djmphr71x012")))

(define-public crate-letter-sequence-0.3.0 (c (n "letter-sequence") (v "0.3.0") (h "0m4pmb4iaa76186rsd2jwwvp6bmgfnzv37k0hca3qvvv3syzz00f")))

(define-public crate-letter-sequence-0.4.0 (c (n "letter-sequence") (v "0.4.0") (h "0kcc683y671hkcy0w53ch1f0ib3v8v8fz8z9pwfp854dsdas3033")))

(define-public crate-letter-sequence-0.5.0 (c (n "letter-sequence") (v "0.5.0") (h "0naixlfhv48zc7nzvqqifxr6638068619l7q6q450m752rc9hvdd")))

(define-public crate-letter-sequence-0.6.0 (c (n "letter-sequence") (v "0.6.0") (d (list (d (n "structopt") (r "^0.3.23") (o #t) (d #t) (k 0)))) (h "1qgagd98x6w25j6z7fpbl1hq8hpr9iqgl7fx52yq5j7ln1g6yrkx") (f (quote (("bin" "structopt"))))))

(define-public crate-letter-sequence-1.0.0 (c (n "letter-sequence") (v "1.0.0") (d (list (d (n "clap") (r "^3.2.5") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "derive_builder") (r "^0.11.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0asxrmqypq4k22d6jdffilaqb5p3fv4cghnpa9rrvcslzanxsg3i") (f (quote (("bin" "clap"))))))

(define-public crate-letter-sequence-1.0.1 (c (n "letter-sequence") (v "1.0.1") (d (list (d (n "clap") (r "^3.2.5") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "derive_builder") (r "^0.11.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1ph53nwcz3aghz0jvgmil60ncyc91dyjfn1n8q6sanri700299bv") (f (quote (("bin" "clap"))))))

(define-public crate-letter-sequence-2.0.0 (c (n "letter-sequence") (v "2.0.0") (d (list (d (n "clap") (r "^3.2.5") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "derive_builder") (r "^0.11.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1waymy7gfb4cnq0ns29fkvm23bc9zhxqa7qbwrx8s1s9zaqbrvdl") (f (quote (("bin" "clap"))))))

(define-public crate-letter-sequence-2.0.1 (c (n "letter-sequence") (v "2.0.1") (d (list (d (n "clap") (r "^3.2.6") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "derive_builder") (r "^0.11.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1ji611lmv6piv3gmzd2zjf926w449sj96xja00sin3y4pi8a49db") (f (quote (("bin" "clap"))))))

(define-public crate-letter-sequence-2.1.0 (c (n "letter-sequence") (v "2.1.0") (d (list (d (n "clap") (r "^3.2.6") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "derive_builder") (r "^0.11.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0pfwfq06g9z1b8zmb55b2fdgpavpmznjjdk95da16wym5vsn6dqy") (f (quote (("bin" "clap"))))))

