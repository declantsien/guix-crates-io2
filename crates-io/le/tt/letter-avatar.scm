(define-module (crates-io le tt letter-avatar) #:use-module (crates-io))

(define-public crate-letter-avatar-0.1.0 (c (n "letter-avatar") (v "0.1.0") (d (list (d (n "cairo-rs") (r "^0.4.1") (f (quote ("png"))) (d #t) (k 0)) (d (n "pango") (r "^0.4.0") (d #t) (k 0)) (d (n "pangocairo") (r "^0.5.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.2.0") (d #t) (k 0)))) (h "0rxm25xawyrkiv1zqljjnnk3bhgw6akbn8xs2l7fw6cf50pc0drb")))

(define-public crate-letter-avatar-0.1.1 (c (n "letter-avatar") (v "0.1.1") (d (list (d (n "cairo-rs") (r "^0.4.1") (f (quote ("png"))) (d #t) (k 0)) (d (n "pango") (r "^0.4.0") (d #t) (k 0)) (d (n "pangocairo") (r "^0.5.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.2.0") (d #t) (k 0)))) (h "1aknkq36wbf5xqaz1klp1ynjplkfn6xmah3kv59x5lv8v3cjc2r3")))

(define-public crate-letter-avatar-1.0.0 (c (n "letter-avatar") (v "1.0.0") (d (list (d (n "cairo-rs") (r "^0.5.0") (f (quote ("png"))) (d #t) (k 0)) (d (n "pango") (r "^0.5.0") (d #t) (k 0)) (d (n "pangocairo") (r "^0.6.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.2.1") (d #t) (k 0)))) (h "0hisndwd8j27qkq00k92rzxqrn4q0v5qcy4j99klvnfr8mn14pfg")))

(define-public crate-letter-avatar-1.0.1 (c (n "letter-avatar") (v "1.0.1") (d (list (d (n "cairo-rs") (r "^0.5.0") (f (quote ("png"))) (d #t) (k 0)) (d (n "pango") (r "^0.5.0") (d #t) (k 0)) (d (n "pangocairo") (r "^0.6.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.2.1") (d #t) (k 0)))) (h "0rwibmsr3xk2s5sdg455qv15iicwxzzxwbd7ijk7sv5x4r5nss5v")))

(define-public crate-letter-avatar-1.0.2 (c (n "letter-avatar") (v "1.0.2") (d (list (d (n "cairo-rs") (r "^0.5.0") (f (quote ("png"))) (d #t) (k 0)) (d (n "pango") (r "^0.5.0") (d #t) (k 0)) (d (n "pangocairo") (r "^0.6.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.2.1") (d #t) (k 0)))) (h "0din2y1ajrzqsjm4d3kc93z9zygwmx87aivwp1zb5fyc5wgq9s1i")))

(define-public crate-letter-avatar-1.1.0 (c (n "letter-avatar") (v "1.1.0") (d (list (d (n "cairo-rs") (r "^0.6.0") (f (quote ("png"))) (d #t) (k 0)) (d (n "pango") (r "^0.6.0") (d #t) (k 0)) (d (n "pangocairo") (r "^0.7.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.2.1") (d #t) (k 0)))) (h "091gnhr2sjxnj0xqr1752rpc34ljm4ww6g496pkc29dmkaqfxxdl")))

(define-public crate-letter-avatar-1.2.0 (c (n "letter-avatar") (v "1.2.0") (d (list (d (n "cairo-rs") (r "^0.7.0") (f (quote ("png"))) (d #t) (k 0)) (d (n "pango") (r "^0.7.0") (d #t) (k 0)) (d (n "pangocairo") (r "^0.8.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.2.1") (d #t) (k 0)))) (h "1lgqkjgmwr0nxpjdm5l5jyk2sqv8rskypxrgnxiic5c0kg42xs8y")))

(define-public crate-letter-avatar-1.2.1 (c (n "letter-avatar") (v "1.2.1") (d (list (d (n "cairo-rs") (r "^0.8.1") (f (quote ("png"))) (d #t) (k 0)) (d (n "pango") (r "^0.8.0") (d #t) (k 0)) (d (n "pangocairo") (r "^0.9.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.6.0") (d #t) (k 0)))) (h "00hnlyckr25h5hvisk90vf88diww6cds140s8cpfhjx4p33kxnm8")))

(define-public crate-letter-avatar-1.3.0 (c (n "letter-avatar") (v "1.3.0") (d (list (d (n "cairo-rs") (r "^0.9.1") (f (quote ("png"))) (d #t) (k 0)) (d (n "pango") (r "^0.9.0") (d #t) (k 0)) (d (n "pangocairo") (r "^0.10.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.6.0") (d #t) (k 0)))) (h "0if2mihpxn4xissv68qciqn5vywy0kl0sx9hcyrhd70s2fyn09k6")))

(define-public crate-letter-avatar-1.4.0 (c (n "letter-avatar") (v "1.4.0") (d (list (d (n "cairo-rs") (r "^0.15.1") (f (quote ("png"))) (d #t) (k 0)) (d (n "pango") (r "^0.15.1") (d #t) (k 0)) (d (n "pangocairo") (r "^0.15.1") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.8.0") (d #t) (k 0)))) (h "04jv5f4ixx6z51r68ngycbndsbz8f4ad1m5pg347z0hpdc3r0fx2")))

