(define-module (crates-io le tt letterbox-lang) #:use-module (crates-io))

(define-public crate-letterbox-lang-0.1.0 (c (n "letterbox-lang") (v "0.1.0") (d (list (d (n "logos") (r "^0.12.1") (d #t) (k 0)) (d (n "regex") (r "^1.8.1") (d #t) (k 0)))) (h "18aay3vk2jcmjfr813rw2wclqs8lbnlnq22j1fklp3lbkqzmzc2w")))

(define-public crate-letterbox-lang-0.2.0 (c (n "letterbox-lang") (v "0.2.0") (d (list (d (n "logos") (r "^0.12.1") (d #t) (k 0)) (d (n "regex") (r "^1.8.1") (d #t) (k 0)))) (h "0hb6cvchhip1klvmynnmhq9q9sv0lwy5zwsb2m0ywqbyvgzn4lyc")))

