(define-module (crates-io le tt letterboxed) #:use-module (crates-io))

(define-public crate-letterboxed-1.0.0 (c (n "letterboxed") (v "1.0.0") (h "0r9374nhphf2324w5vgmnk2zkf13al5wr7z46r6qcbp7b53wphaf")))

(define-public crate-letterboxed-1.1.0 (c (n "letterboxed") (v "1.1.0") (h "1y05m39y5sk9mgqbnfljkalc641ls241xb3g1bigyyvfnpxmjhhh")))

