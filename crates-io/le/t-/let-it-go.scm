(define-module (crates-io le t- let-it-go) #:use-module (crates-io))

(define-public crate-let-it-go-0.1.0 (c (n "let-it-go") (v "0.1.0") (h "1lm6wfdl079g7kz09x4ksz639nkfnaa73nq70pza5s7z32bsiww7") (y #t)))

(define-public crate-let-it-go-0.1.1 (c (n "let-it-go") (v "0.1.1") (h "1z5ipsb845rnirzj900zdw3jzlz89np47205i0wj0fhpmxg43m74") (y #t)))

(define-public crate-let-it-go-0.1.2 (c (n "let-it-go") (v "0.1.2") (h "0zwq6wy2yfxph2w4d89w9mp8gykxin6apbk4s3kiqhr22jkf9271") (y #t)))

