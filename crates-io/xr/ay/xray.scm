(define-module (crates-io xr ay xray) #:use-module (crates-io))

(define-public crate-xray-0.1.0 (c (n "xray") (v "0.1.0") (d (list (d (n "gl") (r "^0.10.0") (o #t) (d #t) (k 0)) (d (n "image") (r "^0.19.0") (d #t) (k 0)))) (h "0jvyc5rfdnlzmg9iffnvrbz7f9pbg01prwbi7qgbfgwzy0b8wfn0") (f (quote (("opengl" "gl") ("default" "gl"))))))

(define-public crate-xray-0.1.1 (c (n "xray") (v "0.1.1") (d (list (d (n "gl") (r "^0.10.0") (o #t) (d #t) (k 0)) (d (n "image") (r "^0.19.0") (d #t) (k 0)))) (h "1wjig48chb4h98ligiqmhr55ak1qbl6rpkzy7rjcp6hsrigfzajf") (f (quote (("opengl" "gl") ("default" "gl"))))))

