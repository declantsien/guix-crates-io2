(define-module (crates-io xr ay xray-lite) #:use-module (crates-io))

(define-public crate-xray-lite-0.0.9 (c (n "xray-lite") (v "0.0.9") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "02kd2c8lkf8g0bam1am3l38scwp6p52izgc7bkndb5xlglyv9ylh")))

