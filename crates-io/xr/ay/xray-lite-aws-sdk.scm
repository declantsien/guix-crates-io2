(define-module (crates-io xr ay xray-lite-aws-sdk) #:use-module (crates-io))

(define-public crate-xray-lite-aws-sdk-0.0.4 (c (n "xray-lite-aws-sdk") (v "0.0.4") (d (list (d (n "aws-config") (r "^1.1.8") (d #t) (k 2)) (d (n "aws-sdk-s3") (r "^1.18.0") (d #t) (k 2)) (d (n "aws-smithy-runtime-api") (r "^1.1") (f (quote ("client"))) (d #t) (k 0)) (d (n "aws-smithy-types") (r "^1.1") (d #t) (k 0)) (d (n "aws-types") (r "^1.1") (d #t) (k 0)) (d (n "xray-lite") (r "^0.0.9") (d #t) (k 0)))) (h "1ci94kgxkxydbb316yids7lwawfj5g555xavcynpygxq68hdv4ds")))

