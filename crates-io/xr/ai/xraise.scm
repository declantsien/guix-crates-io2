(define-module (crates-io xr ai xraise) #:use-module (crates-io))

(define-public crate-xraise-0.1.0 (c (n "xraise") (v "0.1.0") (d (list (d (n "psutil") (r "^1.0.0") (d #t) (k 0)) (d (n "x11") (r "^2.12") (f (quote ("xlib"))) (d #t) (k 0)))) (h "0mgpralmh5rc45k5lz13al6r88190xb0a75iip1f9fcg8qp7chak")))

