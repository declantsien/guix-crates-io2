(define-module (crates-io xr ef xref) #:use-module (crates-io))

(define-public crate-xref-0.1.0 (c (n "xref") (v "0.1.0") (h "072hf3iibsg221gx2xzpc643is06k15xqjp8pqij5211fwnaqqhp")))

(define-public crate-xref-0.1.1 (c (n "xref") (v "0.1.1") (h "0f9f4z387cvgkg8c15vgg406x8yjl7nxqmrg2n3p7s1n8a548zqy")))

