(define-module (crates-io xr c_ xrc_cli) #:use-module (crates-io))

(define-public crate-xrc_cli-1.0.0 (c (n "xrc_cli") (v "1.0.0") (d (list (d (n "clap") (r "^4.0.27") (d #t) (k 0)) (d (n "hmac") (r "^0.12.1") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.2") (d #t) (k 0)) (d (n "path-absolutize") (r "^3.0.14") (d #t) (k 0)) (d (n "sha2") (r "^0.10.6") (d #t) (k 0)) (d (n "xor_cryptor") (r "^1.0.1") (d #t) (k 0)))) (h "1ssc6cihssk8wkhni9wmyxxkjf80zzx20s8k84b3jfs0mcq3g60b") (y #t)))

(define-public crate-xrc_cli-1.0.1 (c (n "xrc_cli") (v "1.0.1") (d (list (d (n "clap") (r "^4.0.27") (d #t) (k 0)) (d (n "hmac") (r "^0.12.1") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.2") (d #t) (k 0)) (d (n "path-absolutize") (r "^3.0.14") (d #t) (k 0)) (d (n "sha2") (r "^0.10.6") (d #t) (k 0)) (d (n "xor_cryptor") (r "^1.0.1") (d #t) (k 0)))) (h "0g6m4ksldkkb1vdhh2fqi0q351nzh759vh7grcwxlm0wa1jmxwjm")))

(define-public crate-xrc_cli-1.0.2 (c (n "xrc_cli") (v "1.0.2") (d (list (d (n "clap") (r "^4.0.27") (d #t) (k 0)) (d (n "hmac") (r "^0.12.1") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.2") (d #t) (k 0)) (d (n "path-absolutize") (r "^3.0.14") (d #t) (k 0)) (d (n "sha2") (r "^0.10.6") (d #t) (k 0)) (d (n "xor_cryptor") (r "^1.0.1") (d #t) (k 0)))) (h "11f0c2ij7y3q27q5qvyi90cplncymk9s4caz66zqn5cpaw6w9mpq")))

(define-public crate-xrc_cli-1.1.0 (c (n "xrc_cli") (v "1.1.0") (d (list (d (n "clap") (r "^4.0.27") (d #t) (k 0)) (d (n "hmac") (r "^0.12.1") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.2") (d #t) (k 0)) (d (n "path-absolutize") (r "^3.0.14") (d #t) (k 0)) (d (n "sha2") (r "^0.10.6") (d #t) (k 0)) (d (n "xor_cryptor") (r "^1.0.2") (d #t) (k 0)))) (h "19km9s5pb0a5drpw6hwxjxs098fs4hsfaiflxpn7h41ci3jk4188")))

(define-public crate-xrc_cli-1.1.1 (c (n "xrc_cli") (v "1.1.1") (d (list (d (n "clap") (r "^4.0.27") (d #t) (k 0)) (d (n "hmac") (r "^0.12.1") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.2") (d #t) (k 0)) (d (n "path-absolutize") (r "^3.0.14") (d #t) (k 0)) (d (n "sha2") (r "^0.10.6") (d #t) (k 0)) (d (n "xor_cryptor") (r "^1.0.2") (d #t) (k 0)))) (h "09lgr32fflgv87h1sab5xda9iij3z6lxyrz5cmn4aqh9sw61073l")))

(define-public crate-xrc_cli-1.2.0 (c (n "xrc_cli") (v "1.2.0") (d (list (d (n "clap") (r "^4.0.27") (d #t) (k 0)) (d (n "hmac") (r "^0.12.1") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.2") (d #t) (k 0)) (d (n "path-absolutize") (r "^3.0.14") (d #t) (k 0)) (d (n "sha2") (r "^0.10.6") (d #t) (k 0)) (d (n "xor_cryptor") (r "^1.1.0") (d #t) (k 0)))) (h "073w4l02prgzscas2y175xaykiw9ak8kxr4h1ichijpiwsr77yxh")))

(define-public crate-xrc_cli-1.2.1 (c (n "xrc_cli") (v "1.2.1") (d (list (d (n "clap") (r "^4.0.27") (d #t) (k 0)) (d (n "hmac") (r "^0.12.1") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.2") (d #t) (k 0)) (d (n "path-absolutize") (r "^3.0.14") (d #t) (k 0)) (d (n "sha2") (r "^0.10.6") (d #t) (k 0)) (d (n "xor_cryptor") (r "^1.2.1") (d #t) (k 0)))) (h "0mrc6l2dzapyf4ld3hh1rv6sc4kfpjg61l6vrwhjld3839gsnbm8")))

(define-public crate-xrc_cli-1.2.2 (c (n "xrc_cli") (v "1.2.2") (d (list (d (n "clap") (r "^4.0.27") (d #t) (k 0)) (d (n "hmac") (r "^0.12.1") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.2") (d #t) (k 0)) (d (n "path-absolutize") (r "^3.0.14") (d #t) (k 0)) (d (n "sha2") (r "^0.10.6") (d #t) (k 0)) (d (n "xor_cryptor") (r "^1.2.2") (d #t) (k 0)))) (h "11v96m5xkbhi0gmc8q4yg1q6zxxsj87d9rsvw61qxcms450qara5")))

