(define-module (crates-io xr eq xreq-cli-utils) #:use-module (crates-io))

(define-public crate-xreq-cli-utils-0.1.0 (c (n "xreq-cli-utils") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.62") (d #t) (k 0)))) (h "11k7maxaqpk3f305gxj03snk15b61iky12i1v5bngrfk3dz5qmhf")))

(define-public crate-xreq-cli-utils-0.2.0 (c (n "xreq-cli-utils") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.62") (d #t) (k 0)) (d (n "xreq-lib") (r "^0.3.0") (d #t) (k 0)))) (h "0wmdjhc6386q4avxr1xfinrx66gjz6qa2h2nr60nkwjb9fnwhdvi")))

(define-public crate-xreq-cli-utils-0.3.0 (c (n "xreq-cli-utils") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.62") (d #t) (k 0)) (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "syntect") (r "^4") (d #t) (k 0)) (d (n "xreq-lib") (r "^0.4.0") (d #t) (k 0)))) (h "0ybhkpffm3rc236q1w8vla0pwyizikighnnwgmlb43zr03sg152i")))

(define-public crate-xreq-cli-utils-0.3.1 (c (n "xreq-cli-utils") (v "0.3.1") (d (list (d (n "anyhow") (r "^1.0.62") (d #t) (k 0)) (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "syntect") (r "^5.0.0") (d #t) (k 0)) (d (n "xreq-lib") (r "^0.4.0") (d #t) (k 0)))) (h "006kaq82a9p4641zx9z66d4mva3q4kgfzj6ps3i90gf5p5av0g45")))

