(define-module (crates-io xr an xrandr) #:use-module (crates-io))

(define-public crate-xrandr-0.1.0 (c (n "xrandr") (v "0.1.0") (d (list (d (n "indexmap") (r "^1.6.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)) (d (n "x11") (r "^2.18.2") (f (quote ("xlib" "xrandr"))) (d #t) (k 0)))) (h "0ly0fa61wxnd04vhy4dlrnwbdng0clvljdyy1ig11i6cxfym7wql")))

(define-public crate-xrandr-0.1.1 (c (n "xrandr") (v "0.1.1") (d (list (d (n "indexmap") (r "^1.6.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.133") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)) (d (n "x11") (r "^2.18.2") (f (quote ("xlib" "xrandr"))) (d #t) (k 0)))) (h "0qkqf0pjl4apdb3cb9h307l4wlmnyyv5hmvpb6cy0krmbyxyryp9") (f (quote (("serialize" "serde" "indexmap/serde-1"))))))

(define-public crate-xrandr-0.2.0 (c (n "xrandr") (v "0.2.0") (d (list (d (n "indexmap") (r "^1.6.2") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "libc") (r "^0.2.146") (d #t) (k 0)) (d (n "serde") (r "^1.0.133") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)) (d (n "time") (r "^0.3.20") (d #t) (k 0)) (d (n "x11") (r "^2.18.2") (f (quote ("xlib" "xrandr"))) (d #t) (k 0)))) (h "1pm8lr841qmz8712mcfbh0rhn2rnw0w2sqzlsbqlg6461k1lx8b7") (f (quote (("serialize" "serde" "indexmap/serde-1"))))))

