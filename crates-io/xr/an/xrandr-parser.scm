(define-module (crates-io xr an xrandr-parser) #:use-module (crates-io))

(define-public crate-xrandr-parser-0.1.0 (c (n "xrandr-parser") (v "0.1.0") (d (list (d (n "derive_setters") (r "^0.1") (d #t) (k 0)))) (h "1i5aa2qpv6pdvyzh39zxmj1llig23pl12nljqbw2gnzwv8jk2hj2") (f (quote (("test"))))))

(define-public crate-xrandr-parser-0.2.0 (c (n "xrandr-parser") (v "0.2.0") (d (list (d (n "derive_setters") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1ja39l5lq19g8bsxifq93wa7wgxa1i9liifrvfg38mav5x0f6z0l") (f (quote (("test"))))))

(define-public crate-xrandr-parser-0.3.0 (c (n "xrandr-parser") (v "0.3.0") (d (list (d (n "derive_setters") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1h9h02aw5h3k1ws02i7s89zsq5aswjcsi0czdgc8prffc6k3px2s") (f (quote (("test"))))))

