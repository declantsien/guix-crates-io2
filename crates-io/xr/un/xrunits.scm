(define-module (crates-io xr un xrunits) #:use-module (crates-io))

(define-public crate-xrunits-0.1.1 (c (n "xrunits") (v "0.1.1") (h "1rwlfjy4bd50a3frsxa426h4zx880y1hhy02l6sjaj9lr2pp8whv")))

(define-public crate-xrunits-0.1.2 (c (n "xrunits") (v "0.1.2") (h "0n0gfgjvwr4fdwfi8v852rn4w87zv2gbkr5v6hjpadi8h1m77g37")))

