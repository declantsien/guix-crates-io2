(define-module (crates-io xr un xrun) #:use-module (crates-io))

(define-public crate-xrun-0.1.0 (c (n "xrun") (v "0.1.0") (d (list (d (n "comfy-table") (r "^7.1.1") (d #t) (k 0)))) (h "0zw2899mmqn1arwylcwmncfnayz17kmqwpcq3jynsslpxc9y105m")))

(define-public crate-xrun-0.2.0 (c (n "xrun") (v "0.2.0") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "comfy-table") (r "^7.1.1") (d #t) (k 0)))) (h "198vbl7646kax76rd83620zp92nhh4viswp0cdg8al4w5sb17zdx")))

(define-public crate-xrun-0.3.0 (c (n "xrun") (v "0.3.0") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "comfy-table") (r "^7.1.1") (d #t) (k 0)))) (h "05yspw32fcwsvj21475wi3d205703qkkxdn1xwpp8injx3wfj606")))

(define-public crate-xrun-0.4.0 (c (n "xrun") (v "0.4.0") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "comfy-table") (r "^7.1.1") (d #t) (k 0)))) (h "0cpnlq11bik35jd17vh5bnassl53zd27s6gfda3hph4z9gnkajy3")))

(define-public crate-xrun-0.5.0 (c (n "xrun") (v "0.5.0") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "comfy-table") (r "^7.1.1") (d #t) (k 0)))) (h "1h6rgdh6wmn5dcr8bb8pzgcxv0j3bgf0ygm41265smhmw274wixg")))

