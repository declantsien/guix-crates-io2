(define-module (crates-io xr pl xrpl) #:use-module (crates-io))

(define-public crate-xrpl-0.1.0 (c (n "xrpl") (v "0.1.0") (h "1pliks6arbfydrw8ngpy2rxm16rph1sy9zid8lrq0mirk75afl4y") (y #t)))

(define-public crate-xrpl-0.1.1 (c (n "xrpl") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.26") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-tungstenite") (r "^0.18") (f (quote ("native-tls"))) (d #t) (k 0)) (d (n "uuid") (r "^1.3.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "1yibcdr1qdigygzxxyp6q4zgjv669g1cy3bbp7gxi8jl76a5r6sh")))

(define-public crate-xrpl-0.1.2 (c (n "xrpl") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_with") (r "^3.0.0") (d #t) (k 0)) (d (n "tokio") (r "^1.26") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-tungstenite") (r "^0.18") (f (quote ("native-tls"))) (d #t) (k 0)) (d (n "tokio-util") (r "^0.7") (d #t) (k 0)) (d (n "uuid") (r "^1.3.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "0amb1671952q2w7v1r722kkcna1jy9fb1vv3xzw79miml40bg371")))

