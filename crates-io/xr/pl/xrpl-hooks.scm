(define-module (crates-io xr pl xrpl-hooks) #:use-module (crates-io))

(define-public crate-xrpl-hooks-0.1.0 (c (n "xrpl-hooks") (v "0.1.0") (h "14dwvmypm96i232g4443zpqnxxlwm5r88qymbdc4l4npfy0mll6v")))

(define-public crate-xrpl-hooks-0.2.0 (c (n "xrpl-hooks") (v "0.2.0") (h "1d1hnhn0dgmw3adccnjyy045gnl6r248l9sz7x3bbnc8g506l66c")))

(define-public crate-xrpl-hooks-0.3.0 (c (n "xrpl-hooks") (v "0.3.0") (h "1ynyrsbhvhgs9ar1n10990drzabg9wbyh2yab5im10z2pd6ycnzw")))

(define-public crate-xrpl-hooks-0.3.1 (c (n "xrpl-hooks") (v "0.3.1") (h "0814s25dkmpnil92gxbgwfx6q4slgpbbwrg4vqcsk35g99cc344n")))

