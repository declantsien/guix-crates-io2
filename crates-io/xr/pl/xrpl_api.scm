(define-module (crates-io xr pl xrpl_api) #:use-module (crates-io))

(define-public crate-xrpl_api-0.1.0 (c (n "xrpl_api") (v "0.1.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "xrpl_types") (r "^0.2") (d #t) (k 0)))) (h "1jfqrwn4qdnfd9g8skj4lqx2myn6175fjv7pxafay1zq9lv2srq0")))

(define-public crate-xrpl_api-0.9.0 (c (n "xrpl_api") (v "0.9.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "xrpl_types") (r "^0.9") (d #t) (k 0)))) (h "0lvqrv1yy84q634397zj8ng23d8jg6jpzyv02837w1xwga7w0yxa")))

(define-public crate-xrpl_api-0.11.0 (c (n "xrpl_api") (v "0.11.0") (d (list (d (n "enumflags2") (r "^0.7.7") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "xrpl_types") (r "^0.11") (d #t) (k 0)))) (h "18xj4984n8mvnp2ip4ngzw7fyk0vpqwcl3lkz1y2smm1bp8kr8yw")))

(define-public crate-xrpl_api-0.12.0 (c (n "xrpl_api") (v "0.12.0") (d (list (d (n "enumflags2") (r "^0.7.7") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "xrpl_types") (r "^0.12") (d #t) (k 0)))) (h "1jkh12sr48v83mflidscyy91mgyi1hnww93b356341hl02ig0vi4")))

(define-public crate-xrpl_api-0.13.0 (c (n "xrpl_api") (v "0.13.0") (d (list (d (n "enumflags2") (r "^0.7.7") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "xrpl_types") (r "^0.13") (d #t) (k 0)))) (h "0zmij1jqp101gj74k9ziaxsarcdia6g47j9ab0qccywvp82ywj49")))

(define-public crate-xrpl_api-0.14.0 (c (n "xrpl_api") (v "0.14.0") (d (list (d (n "assert_matches") (r "^1.5.0") (d #t) (k 2)) (d (n "enumflags2") (r "^0.7.7") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "xrpl_types") (r "^0.14") (d #t) (k 0)))) (h "14abc38ahxzja1jwzb4xvkp5f5qpliqbrd8p3d71jm1hljh4qjv9")))

(define-public crate-xrpl_api-0.15.0 (c (n "xrpl_api") (v "0.15.0") (d (list (d (n "assert_matches") (r "^1.5.0") (d #t) (k 2)) (d (n "enumflags2") (r "^0.7.7") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "xrpl_types") (r "^0.15") (d #t) (k 0)))) (h "0ipjp3zz0mjj99bb3nc88l58kzif5vmd3spp5km6s59l44j6ymy4")))

(define-public crate-xrpl_api-0.16.0 (c (n "xrpl_api") (v "0.16.0") (d (list (d (n "assert_matches") (r "^1.5.0") (d #t) (k 2)) (d (n "enumflags2") (r "^0.7.7") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "xrpl_types") (r "^0.15") (d #t) (k 0)))) (h "1fky9cn89lfp90kz0q1g0z2w87sli7iymh3nk26gms7vyvri3vzk")))

(define-public crate-xrpl_api-0.16.3 (c (n "xrpl_api") (v "0.16.3") (d (list (d (n "assert_matches") (r "^1.5.0") (d #t) (k 2)) (d (n "enumflags2") (r "^0.7.7") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "xrpl_types") (r "^0.16.3") (d #t) (k 0)))) (h "0jw6pvy3zdxc8cxwf8pxng8s8sg8xvfaykcz7na4f8gzbhdh475p")))

(define-public crate-xrpl_api-0.16.4 (c (n "xrpl_api") (v "0.16.4") (d (list (d (n "assert_matches") (r "^1.5.0") (d #t) (k 2)) (d (n "enumflags2") (r "^0.7.7") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "xrpl_types") (r "^0.16.4") (d #t) (k 0)))) (h "09s8209ccbwa6rkzwk27xcpy7lqsbdm44x2d69n8l351k8b2qz47")))

