(define-module (crates-io xr pl xrpl_address_codec) #:use-module (crates-io))

(define-public crate-xrpl_address_codec-0.1.0 (c (n "xrpl_address_codec") (v "0.1.0") (h "01dxsd09syajln1kdx2clhxg1k79viy51bwa0c24p31hylckjlh9")))

(define-public crate-xrpl_address_codec-0.9.0 (c (n "xrpl_address_codec") (v "0.9.0") (h "0r3d797wimnr94hzcd84i9ys6kdmis13fgjp6r6gkrn0pf4rw1ya")))

(define-public crate-xrpl_address_codec-0.11.0 (c (n "xrpl_address_codec") (v "0.11.0") (h "025r1k9v7s1cb6nhzb23x8dkqlnq8kz1gnp4ld74zbwr7rdfva1x")))

(define-public crate-xrpl_address_codec-0.12.0 (c (n "xrpl_address_codec") (v "0.12.0") (h "0qwxah3dlzs2wbyff054vdb3mghmpzvjvnzc9i9xqs7hpg2pw67p")))

(define-public crate-xrpl_address_codec-0.13.0 (c (n "xrpl_address_codec") (v "0.13.0") (h "0fpc5268fvk92hkjjpqvirvvj4yxh6w39inqzpab0v4vnclm89dn")))

(define-public crate-xrpl_address_codec-0.14.0 (c (n "xrpl_address_codec") (v "0.14.0") (h "1fz90v8jfh62s3rpr7yf4rl9b4v88xv0mydb2xsk9319srsam6n3")))

(define-public crate-xrpl_address_codec-0.15.0 (c (n "xrpl_address_codec") (v "0.15.0") (h "0mx1fmrlnl9scw3wmqh41xzhmwa0awid16mcwk3a68nglyjjf2hb")))

(define-public crate-xrpl_address_codec-0.16.0 (c (n "xrpl_address_codec") (v "0.16.0") (h "1by783shyq53flk5039da2hbc8pvcn7xhzmrr4vs1zydjn5q2l68")))

(define-public crate-xrpl_address_codec-0.16.1 (c (n "xrpl_address_codec") (v "0.16.1") (h "1raw8rai1y49x0vibdq29gjf8jphfc47pp8h7bz2rki8f08wkcwg")))

(define-public crate-xrpl_address_codec-0.16.3 (c (n "xrpl_address_codec") (v "0.16.3") (h "0gfxlx0z1r0r6dx93p95v330c5nxsmqg5rhbsiqjvsfslfch7q5i")))

(define-public crate-xrpl_address_codec-0.16.4 (c (n "xrpl_address_codec") (v "0.16.4") (h "1jqp88iaymgr3jw08933h93brd50g3lh41bggsrb4sfbg6mdsx7z")))

