(define-module (crates-io xr #{80}# xr806-pac) #:use-module (crates-io))

(define-public crate-xr806-pac-0.0.1 (c (n "xr806-pac") (v "0.0.1") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1l8q01w4zazr7afbg7997rhpq10bisahxxq80n6i0ispyn7wrrz1") (f (quote (("rt" "cortex-m-rt/device"))))))

