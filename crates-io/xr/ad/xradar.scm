(define-module (crates-io xr ad xradar) #:use-module (crates-io))

(define-public crate-xradar-0.1.0 (c (n "xradar") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "net" "time" "macros" "process" "rt-multi-thread"))) (d #t) (k 0)))) (h "1a15b5is58ggarfrgfc1vpasvkbac5657s1ri7zrdzjz3548jxkw") (r "1.56.0")))

