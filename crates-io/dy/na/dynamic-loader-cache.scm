(define-module (crates-io dy na dynamic-loader-cache) #:use-module (crates-io))

(define-public crate-dynamic-loader-cache-0.1.0 (c (n "dynamic-loader-cache") (v "0.1.0") (d (list (d (n "arrayvec") (r "^0.7") (d #t) (k 0)) (d (n "memmap2") (r "^0.9") (d #t) (k 0)) (d (n "memoffset") (r "^0.9") (d #t) (k 0)) (d (n "nom") (r "^7.1") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0kvi7vx1mmj0dnim574cyppyf04fmrdzm3nx9iph3qvyh8914gb6")))

(define-public crate-dynamic-loader-cache-0.1.1 (c (n "dynamic-loader-cache") (v "0.1.1") (d (list (d (n "arrayvec") (r "^0.7") (d #t) (k 0)) (d (n "assert_matches") (r "^1.5") (d #t) (k 2)) (d (n "memmap2") (r "^0.9") (d #t) (k 0)) (d (n "memoffset") (r "^0.9") (d #t) (k 0)) (d (n "nom") (r "^7.1") (d #t) (k 0)) (d (n "proptest") (r "^1.4") (d #t) (k 2)) (d (n "static_assertions") (r "^1.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1vbsmjfr61n15drj5khml19vms099sh6wh0rdvpwz0zg3d81132v")))

(define-public crate-dynamic-loader-cache-0.1.2 (c (n "dynamic-loader-cache") (v "0.1.2") (d (list (d (n "arrayvec") (r "^0.7") (d #t) (k 0)) (d (n "assert_matches") (r "^1.5") (d #t) (k 2)) (d (n "memmap2") (r "^0.9") (d #t) (k 0)) (d (n "nom") (r "^7.1") (d #t) (k 0)) (d (n "proptest") (r "^1.4") (d #t) (k 2)) (d (n "static_assertions") (r "^1.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0nc2jpn7b448k07fvaans793bzjhp1y1zv74c9ms5smhc40qcv3l")))

(define-public crate-dynamic-loader-cache-0.2.0 (c (n "dynamic-loader-cache") (v "0.2.0") (d (list (d (n "arrayvec") (r "^0.7") (d #t) (k 0)) (d (n "assert_matches") (r "^1.5") (d #t) (k 2)) (d (n "memmap2") (r "^0.9") (d #t) (k 0)) (d (n "nom") (r "^7.1") (d #t) (k 0)) (d (n "proptest") (r "^1.4") (d #t) (k 2)) (d (n "static_assertions") (r "^1.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "08p4fjxbmzhs995h1qchzl05yv3y2ykflni1ssj9vclgnc4rs40y")))

