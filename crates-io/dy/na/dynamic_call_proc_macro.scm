(define-module (crates-io dy na dynamic_call_proc_macro) #:use-module (crates-io))

(define-public crate-dynamic_call_proc_macro-0.1.0 (c (n "dynamic_call_proc_macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0rc6qfrf77x0932bmscfd9d0vfyzg3basp3s2jd9mzbxlcpn3q45")))

(define-public crate-dynamic_call_proc_macro-0.1.1 (c (n "dynamic_call_proc_macro") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0mvbw57g2cypa38vkc44k0615pn2phgkd19bdfxl4h131il5jwvs")))

