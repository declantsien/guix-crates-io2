(define-module (crates-io dy na dynamic-plugin-macros) #:use-module (crates-io))

(define-public crate-dynamic-plugin-macros-0.1.0 (c (n "dynamic-plugin-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.82") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.64") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0ca25wp443zqrd3vi43frpbk7s5hbgmzn9m87w4kbggg0bqyxakz") (f (quote (("host") ("client"))))))

