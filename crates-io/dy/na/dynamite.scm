(define-module (crates-io dy na dynamite) #:use-module (crates-io))

(define-public crate-dynamite-0.0.1 (c (n "dynamite") (v "0.0.1") (d (list (d (n "dlopen") (r "^0.1.8") (d #t) (k 0)) (d (n "dlopen_derive") (r "^0.1.4") (d #t) (k 0)) (d (n "safer-ffi") (r "^0.0.5") (f (quote ("proc_macros"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_cbor") (r "^0.11.1") (d #t) (k 0)))) (h "0y8hm03zxkh543m820qcjxcdgqqi5b2a3zbq14bv5wbq3x04jbs5")))

