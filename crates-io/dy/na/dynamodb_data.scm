(define-module (crates-io dy na dynamodb_data) #:use-module (crates-io))

(define-public crate-dynamodb_data-0.1.0 (c (n "dynamodb_data") (v "0.1.0") (d (list (d (n "rusoto_core") (r "^0.40.0") (d #t) (k 0)) (d (n "rusoto_dynamodb") (r "^0.40.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.99") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)))) (h "17kxd3lrjfakfdbykjwnnh4j4jbw5mrgr7yznvx4sp0lwibqqxc9")))

(define-public crate-dynamodb_data-0.1.1 (c (n "dynamodb_data") (v "0.1.1") (d (list (d (n "rusoto_core") (r "^0.40.0") (d #t) (k 0)) (d (n "rusoto_dynamodb") (r "^0.40.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.99") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)))) (h "0fpba7jn9acqkfc053bl8p3d1qqmfax2lqh4lx6wh44dd4kjbhi4")))

(define-public crate-dynamodb_data-0.1.2 (c (n "dynamodb_data") (v "0.1.2") (d (list (d (n "rusoto_core") (r "^0.40.0") (d #t) (k 0)) (d (n "rusoto_dynamodb") (r "^0.40.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.99") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)))) (h "1bqqlvjn4l31myyvk2d6cpkl2jqn0vlrmh3n71y9b3rzz257jral")))

(define-public crate-dynamodb_data-0.1.3 (c (n "dynamodb_data") (v "0.1.3") (d (list (d (n "rusoto_core") (r "^0.40.0") (d #t) (k 0)) (d (n "rusoto_dynamodb") (r "^0.40.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.99") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)))) (h "01iflq091f4gahr0045kp2shs2rp0p0czw10qdd70l7jqmkiq82y")))

(define-public crate-dynamodb_data-0.1.4 (c (n "dynamodb_data") (v "0.1.4") (d (list (d (n "rusoto_core") (r "^0.40.0") (d #t) (k 0)) (d (n "rusoto_dynamodb") (r "^0.40.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.99") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)))) (h "14a1j809li3h0h5wbzm8n1i5g6ajih1rkhawznq77asgpjh8329g")))

(define-public crate-dynamodb_data-0.1.5 (c (n "dynamodb_data") (v "0.1.5") (d (list (d (n "rusoto_core") (r "^0.40.0") (d #t) (k 0)) (d (n "rusoto_dynamodb") (r "^0.40.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.99") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)) (d (n "uuid") (r "^0.7.4") (f (quote ("serde" "v4"))) (d #t) (k 2)))) (h "1pmzqyp98gpl0qcxhq781yi63x7wbaq1zy7gnfvzqzh2gkwr7vvc")))

(define-public crate-dynamodb_data-0.1.6 (c (n "dynamodb_data") (v "0.1.6") (d (list (d (n "rusoto_core") (r "^0.40.0") (d #t) (k 0)) (d (n "rusoto_dynamodb") (r "^0.40.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.99") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)) (d (n "uuid") (r "^0.7.4") (f (quote ("serde" "v4"))) (d #t) (k 2)))) (h "1gjzj689f91r9di48ak1sqjqxfjwr831qf7c615ahkd454gqygf5")))

(define-public crate-dynamodb_data-0.1.7 (c (n "dynamodb_data") (v "0.1.7") (d (list (d (n "rusoto_core") (r "^0.40.0") (d #t) (k 0)) (d (n "rusoto_dynamodb") (r "^0.40.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.99") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)) (d (n "uuid") (r "^0.7.4") (f (quote ("serde" "v4"))) (d #t) (k 2)))) (h "0hjxpvfmibng26p2j242vqxacic53bic785b4v5ld3y2bikkhslj")))

(define-public crate-dynamodb_data-0.1.8 (c (n "dynamodb_data") (v "0.1.8") (d (list (d (n "rusoto_core") (r "^0.40.0") (d #t) (k 0)) (d (n "rusoto_dynamodb") (r "^0.40.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.99") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)) (d (n "uuid") (r "^0.7.4") (f (quote ("serde" "v4"))) (d #t) (k 2)))) (h "1gr3gps5s4mcw8pcji6qzfahjkxlh25wi0bdzdb2cgv7kh8k4zbl")))

