(define-module (crates-io dy na dynamization) #:use-module (crates-io))

(define-public crate-dynamization-0.1.0 (c (n "dynamization") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "0mbdjfl4za3bymsicfxm6ld6c6zkl6q83h4g3vlwn3lz0lqp6myj") (f (quote (("sorted_vec") ("default")))) (y #t)))

(define-public crate-dynamization-0.2.0 (c (n "dynamization") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "0zpf6gjsvcpfv3hk2imnvm4shrn39rgsj8aj50i7dixbfc0zdjm3") (f (quote (("sorted_vec") ("default"))))))

(define-public crate-dynamization-0.3.0 (c (n "dynamization") (v "0.3.0") (d (list (d (n "criterion") (r ">=0.3.0, <0.4.0") (d #t) (k 2)) (d (n "rand") (r ">=0.7.0, <0.8.0") (d #t) (k 2)))) (h "0nzcc2x1gxgb0inrjqkk8hvyldqbga9nkmaq98x5hlibsv31apf2") (f (quote (("sorted_vec") ("default"))))))

(define-public crate-dynamization-0.4.0 (c (n "dynamization") (v "0.4.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "0zln9fjyhp82wdazh6fxhkqa35ikgqrzbqgzb1qnb0nasawlwd3k") (f (quote (("sorted_vec") ("default"))))))

