(define-module (crates-io dy na dynamic-graphql) #:use-module (crates-io))

(define-public crate-dynamic-graphql-0.1.0 (c (n "dynamic-graphql") (v "0.1.0") (d (list (d (n "async-graphql") (r "^5") (f (quote ("dynamic-schema"))) (d #t) (k 0)) (d (n "dynamic-graphql-derive") (r "^0.1.0") (d #t) (k 0)))) (h "0s8k9446mpz8lk62v1dsb7nj7139m0fhrlx1kpkpzcqj1ik8i2xj")))

(define-public crate-dynamic-graphql-0.1.1 (c (n "dynamic-graphql") (v "0.1.1") (d (list (d (n "async-graphql") (r "^5") (f (quote ("dynamic-schema"))) (d #t) (k 0)) (d (n "dynamic-graphql-derive") (r "^0.1.1") (d #t) (k 0)))) (h "04qi3aln81gz4993n5m70xiwpi56jrjg133q2rwlj42zmqx4vdq0")))

(define-public crate-dynamic-graphql-0.2.0 (c (n "dynamic-graphql") (v "0.2.0") (d (list (d (n "async-graphql") (r "^5.0.5") (f (quote ("dynamic-schema"))) (d #t) (k 0)) (d (n "dynamic-graphql-derive") (r "^0.2.0") (d #t) (k 0)))) (h "1187y7q9gdpsiq9hbpm3c423k9x9rc8dsis03laiddmp1ccb572f")))

(define-public crate-dynamic-graphql-0.3.0 (c (n "dynamic-graphql") (v "0.3.0") (d (list (d (n "async-graphql") (r "^5.0.5") (f (quote ("dynamic-schema"))) (d #t) (k 0)) (d (n "dynamic-graphql-derive") (r "^0.3.0") (d #t) (k 0)))) (h "0gqz0x9d6g885h4f73f0vaprpyg1qlfjqh8jx0v6zrf6wx6a3vmf")))

(define-public crate-dynamic-graphql-0.4.0 (c (n "dynamic-graphql") (v "0.4.0") (d (list (d (n "async-graphql") (r "^5.0.5") (f (quote ("dynamic-schema"))) (d #t) (k 0)) (d (n "dynamic-graphql-derive") (r "^0.4.0") (d #t) (k 0)))) (h "1rm6n5g2vb602pvhnwrwqs8h7gz27vajhysljq7dl4qi1lwgpfy8")))

(define-public crate-dynamic-graphql-0.5.0 (c (n "dynamic-graphql") (v "0.5.0") (d (list (d (n "async-graphql") (r "^5.0.5") (f (quote ("dynamic-schema"))) (d #t) (k 0)) (d (n "dynamic-graphql-derive") (r "^0.4.0") (d #t) (k 0)) (d (n "fnv") (r "^1") (d #t) (k 0)))) (h "058hpi1nnm24jix55mdj28nxjsn2hybaw6az9bfwbjnjnpim5cpy")))

(define-public crate-dynamic-graphql-0.5.1 (c (n "dynamic-graphql") (v "0.5.1") (d (list (d (n "async-graphql") (r "^5.0.5") (f (quote ("dynamic-schema"))) (d #t) (k 0)) (d (n "dynamic-graphql-derive") (r "^0.5.1") (d #t) (k 0)) (d (n "fnv") (r "^1") (d #t) (k 0)))) (h "1838zyjdj4jlicn6f06pwrfk3p45dvmmasy6b1mj0vayi1h5ylb0")))

(define-public crate-dynamic-graphql-0.5.2 (c (n "dynamic-graphql") (v "0.5.2") (d (list (d (n "async-graphql") (r "^5.0.5") (f (quote ("dynamic-schema"))) (d #t) (k 0)) (d (n "dynamic-graphql-derive") (r "^0.5.2") (d #t) (k 0)) (d (n "fnv") (r "^1") (d #t) (k 0)))) (h "1xvkdfwyf2vp8rzwlxp7jngnlwcqdwgrsprx37iknj38d6qn07rg")))

(define-public crate-dynamic-graphql-0.5.3 (c (n "dynamic-graphql") (v "0.5.3") (d (list (d (n "async-graphql") (r "^5.0.5") (f (quote ("dynamic-schema"))) (d #t) (k 0)) (d (n "dynamic-graphql-derive") (r "^0.5.3") (d #t) (k 0)) (d (n "fnv") (r "^1") (d #t) (k 0)))) (h "1ccq5w23qk2cjlay6siyh8dmw2dnsmkrcicqnh7pi5ly71xz6rlc")))

(define-public crate-dynamic-graphql-0.5.4 (c (n "dynamic-graphql") (v "0.5.4") (d (list (d (n "async-graphql") (r "^5.0.5") (f (quote ("dynamic-schema"))) (d #t) (k 0)) (d (n "dynamic-graphql-derive") (r "^0.5.4") (d #t) (k 0)) (d (n "fnv") (r "^1") (d #t) (k 0)))) (h "0dijkazhggw0pirv2q5w1ybzxalxvfax6bbndxspyd1ak8k3wjiq")))

(define-public crate-dynamic-graphql-0.6.0 (c (n "dynamic-graphql") (v "0.6.0") (d (list (d (n "async-graphql") (r "^5.0.5") (f (quote ("dynamic-schema"))) (d #t) (k 0)) (d (n "dynamic-graphql-derive") (r "^0.6.0") (d #t) (k 0)) (d (n "fnv") (r "^1") (d #t) (k 0)))) (h "1rr15rp8n3xi82bh4w2i2hvqb2kn0np54k373ng7n709rfkq6d69")))

(define-public crate-dynamic-graphql-0.6.1 (c (n "dynamic-graphql") (v "0.6.1") (d (list (d (n "async-graphql") (r "^5.0.5") (f (quote ("dynamic-schema"))) (d #t) (k 0)) (d (n "dynamic-graphql-derive") (r "^0.6.1") (d #t) (k 0)) (d (n "fnv") (r "^1") (d #t) (k 0)))) (h "0r5n6xfrqjn5pbvq8znx2lh2cgxp783av5hyf6l4d8p74prmycz1")))

(define-public crate-dynamic-graphql-0.7.0 (c (n "dynamic-graphql") (v "0.7.0") (d (list (d (n "async-graphql") (r "^5.0.5") (f (quote ("dynamic-schema"))) (d #t) (k 0)) (d (n "dynamic-graphql-derive") (r "^0.7.0") (d #t) (k 0)) (d (n "fnv") (r "^1") (d #t) (k 0)))) (h "0afdhk359rpb306hszmfgcc2hmcqw72cindffdnf7kri0q31s8n3")))

(define-public crate-dynamic-graphql-0.7.1 (c (n "dynamic-graphql") (v "0.7.1") (d (list (d (n "async-graphql") (r "^5.0.5") (f (quote ("dynamic-schema"))) (d #t) (k 0)) (d (n "dynamic-graphql-derive") (r "^0.7.1") (d #t) (k 0)) (d (n "fnv") (r "^1") (d #t) (k 0)))) (h "19j2f30595qqda3nl1mniha87k8j6qpjj4d0prqh48iwqr2as22v")))

(define-public crate-dynamic-graphql-0.7.2 (c (n "dynamic-graphql") (v "0.7.2") (d (list (d (n "async-graphql") (r "^5.0.5") (f (quote ("dynamic-schema"))) (d #t) (k 0)) (d (n "dynamic-graphql-derive") (r "^0.7.2") (d #t) (k 0)) (d (n "fnv") (r "^1") (d #t) (k 0)) (d (n "graphql-parser") (r "^0.4") (d #t) (k 2)) (d (n "tokio-test") (r "^0.4") (d #t) (k 2)))) (h "1naw3xafaa3y3zydicwnkc6hg55k4qyxlg16azw9rp8gqshilxpm")))

(define-public crate-dynamic-graphql-0.7.3 (c (n "dynamic-graphql") (v "0.7.3") (d (list (d (n "async-graphql") (r "^5.0.5") (f (quote ("dynamic-schema"))) (d #t) (k 0)) (d (n "dynamic-graphql-derive") (r "^0.7.3") (d #t) (k 0)) (d (n "fnv") (r "^1") (d #t) (k 0)) (d (n "graphql-parser") (r "^0.4") (d #t) (k 2)) (d (n "tokio-test") (r "^0.4") (d #t) (k 2)))) (h "0989p74ifz0j31n0j299w1pv164cx9smmvqrpcan6gmdfiwclf8f")))

(define-public crate-dynamic-graphql-0.8.0 (c (n "dynamic-graphql") (v "0.8.0") (d (list (d (n "async-graphql") (r "^6") (f (quote ("dynamic-schema"))) (d #t) (k 0)) (d (n "dynamic-graphql-derive") (r "^0.8.0") (d #t) (k 0)) (d (n "fnv") (r "^1") (d #t) (k 0)) (d (n "graphql-parser") (r "^0.4") (d #t) (k 2)) (d (n "tokio-test") (r "^0.4") (d #t) (k 2)))) (h "05r9lv5jks8ca68k1h8xbny2gc15wcdvssd65zbr1yyf5cvc591j")))

(define-public crate-dynamic-graphql-0.8.1 (c (n "dynamic-graphql") (v "0.8.1") (d (list (d (n "async-graphql") (r "^6") (f (quote ("dynamic-schema"))) (d #t) (k 0)) (d (n "dynamic-graphql-derive") (r "^0.8.1") (d #t) (k 0)) (d (n "fnv") (r "^1") (d #t) (k 0)) (d (n "graphql-parser") (r "^0.4") (d #t) (k 2)) (d (n "tokio-test") (r "^0.4") (d #t) (k 2)))) (h "0zqv7ba0g35f20ficg5jjmqyh6bsc8v1r35ggwm73cnp8xw83chf")))

