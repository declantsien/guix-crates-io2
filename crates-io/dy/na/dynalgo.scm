(define-module (crates-io dy na dynalgo) #:use-module (crates-io))

(define-public crate-dynalgo-1.0.0 (c (n "dynalgo") (v "1.0.0") (h "1nqbqbdps7whj1z34749bi4p03lm6pg10qbf0aiy27vvyx8x1wis")))

(define-public crate-dynalgo-1.1.0 (c (n "dynalgo") (v "1.1.0") (h "0szlw2jwgdyfa4vcxfw6xymcza6jsjip5yqymi7jnn41a5jb18c2")))

(define-public crate-dynalgo-1.2.0 (c (n "dynalgo") (v "1.2.0") (h "13rymrrjck4wkpzy258d7qyl06w1am72l465zr8jl2awc98imfy7")))

(define-public crate-dynalgo-2.0.0 (c (n "dynalgo") (v "2.0.0") (h "18zxypsa6mydp5yxa9dzfiad8kwkg290rrks2w8ds55nyjrs1q9x")))

(define-public crate-dynalgo-2.0.1 (c (n "dynalgo") (v "2.0.1") (h "0kjir1d3f1n8yh4isxisw0f28xnifxj7yha6h5y3ni1pnjygimfl")))

(define-public crate-dynalgo-3.0.0 (c (n "dynalgo") (v "3.0.0") (h "1i9i4zrhl3w5gb9dfjvycjwyvjqilsdxqv1lilldvjraa7p2vb20")))

(define-public crate-dynalgo-3.0.1 (c (n "dynalgo") (v "3.0.1") (h "139ghzk3glcza0m3bpn0gg3xy2xb4057ma7jf352maz0gwawl2hk")))

(define-public crate-dynalgo-3.1.0 (c (n "dynalgo") (v "3.1.0") (h "0pwcyqjmdp06isycv58pblqmjxszf29nznbl4y5b07fl9pb55gh0")))

(define-public crate-dynalgo-3.1.1 (c (n "dynalgo") (v "3.1.1") (h "0414wy87k4mbjzzjzkwdz7h1x0j9mkqvca15i5kxy9sab8sclp5p")))

(define-public crate-dynalgo-3.1.2 (c (n "dynalgo") (v "3.1.2") (h "09589q9d6f483n3w1gmrhv4fzz6nbr3anycydz31d0lqwpbigzz3")))

