(define-module (crates-io dy na dynamo_mapper) #:use-module (crates-io))

(define-public crate-dynamo_mapper-0.1.0 (c (n "dynamo_mapper") (v "0.1.0") (d (list (d (n "dynamo_mapper_macro") (r "^0.1.0") (d #t) (k 0)) (d (n "rusoto_core") (r "^0.38.0") (d #t) (k 0)) (d (n "rusoto_dynamodb") (r "^0.38.0") (d #t) (k 0)) (d (n "uuid") (r "^0.7.4") (f (quote ("v4"))) (o #t) (d #t) (k 0)))) (h "1lyw3nsdhz37xr59k170gb1yx1nlgqs56kssq6cvp0lqi8i643pn") (f (quote (("default"))))))

