(define-module (crates-io dy na dynamecs-tool) #:use-module (crates-io))

(define-public crate-dynamecs-tool-0.1.0 (c (n "dynamecs-tool") (v "0.1.0") (d (list (d (n "clap") (r "^4.3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dynamecs-analyze") (r "^0.0.1") (d #t) (k 0)) (d (n "escargot") (r "^0.5.7") (d #t) (k 2)) (d (n "insta") (r "^1.29.0") (d #t) (k 2)) (d (n "regex") (r "^1.8.3") (d #t) (k 2)) (d (n "tempfile") (r "^3.5.0") (d #t) (k 2)))) (h "12xschg5s8m7g4s3c2dgv1wlilx6096w70jxqr17yda61mvq40ab")))

(define-public crate-dynamecs-tool-0.2.0 (c (n "dynamecs-tool") (v "0.2.0") (d (list (d (n "clap") (r "^4.3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dynamecs-analyze") (r "^0.0.2") (d #t) (k 0)) (d (n "escargot") (r "^0.5.7") (d #t) (k 2)) (d (n "insta") (r "^1.29.0") (d #t) (k 2)) (d (n "regex") (r "^1.8.3") (d #t) (k 2)) (d (n "tempfile") (r "^3.5.0") (d #t) (k 2)))) (h "0jjh5s4jpy52sv81sk239y1wlq00xq1rl6dzc353l65x0si8sgcf")))

