(define-module (crates-io dy na dynamic-array) #:use-module (crates-io))

(define-public crate-dynamic-array-0.0.0 (c (n "dynamic-array") (v "0.0.0") (h "0kv08yzhj9agryvg4qa41l3d2fmrg2z377yc1djpr92m5wl8h3gv")))

(define-public crate-dynamic-array-0.0.1 (c (n "dynamic-array") (v "0.0.1") (h "0wkir9ismjyak4fmfs78mhr6fs89cwz1xhlmldk0vzmg1m18ac21")))

(define-public crate-dynamic-array-0.1.0 (c (n "dynamic-array") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.117") (o #t) (d #t) (k 0)))) (h "16qmhx8wka2nf0w9qxxbllwavlgbgxs16ykiy1ij7hq5lgfljb5x") (f (quote (("serde-derive" "serde") ("packed"))))))

(define-public crate-dynamic-array-0.1.1 (c (n "dynamic-array") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.117") (o #t) (d #t) (k 0)))) (h "162ic7k4y0ifxlx69z08ldnp5m32gvh256riyzf1al787css0m9h") (f (quote (("serde-derive" "serde") ("packed") ("default-derive"))))))

(define-public crate-dynamic-array-0.1.2 (c (n "dynamic-array") (v "0.1.2") (d (list (d (n "serde") (r "^1.0.117") (o #t) (d #t) (k 0)))) (h "0ivp0yl265bbbrpb7q3rmvdrljvam77kgrsb9vkkshzczwcg80dx") (f (quote (("serde-derive" "serde") ("packed") ("default-derive"))))))

(define-public crate-dynamic-array-0.1.3 (c (n "dynamic-array") (v "0.1.3") (d (list (d (n "serde") (r "^1.0.117") (o #t) (d #t) (k 0)))) (h "0iapjrfnmbmydg2x6f1mpw0brif3iqyjl5z6wh7pl1gmr1dbxa99") (f (quote (("serde-derive" "serde") ("packed") ("default-derive"))))))

(define-public crate-dynamic-array-0.1.4 (c (n "dynamic-array") (v "0.1.4") (d (list (d (n "serde") (r "^1.0.119") (o #t) (d #t) (k 0)))) (h "1icmgq4nayvmhyscgsys73phyvvgg3mw89ry1d0m19qsvkgzaxrc") (f (quote (("serde-derive" "serde") ("packed") ("default-derive"))))))

(define-public crate-dynamic-array-0.1.5 (c (n "dynamic-array") (v "0.1.5") (d (list (d (n "ptr_meta") (r "~0.1.3") (o #t) (k 0)) (d (n "rkyv") (r "^0.7.11") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.119") (o #t) (d #t) (k 0)))) (h "11sgkra0w8d6qss1yizkwrjz6nikkjr1nscdsc28bq1hkmpl5qly") (f (quote (("serde-derive" "serde") ("rkyv-derive" "rkyv" "ptr_meta") ("packed") ("default-derive"))))))

(define-public crate-dynamic-array-0.2.0 (c (n "dynamic-array") (v "0.2.0") (d (list (d (n "ptr_meta") (r "~0.1.3") (o #t) (k 0)) (d (n "rkyv") (r "^0.7.11") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.127") (o #t) (d #t) (k 0)))) (h "0mcaxaadycf1qsjvnbkhwr1dv19r56f50klbkiad09gcj8k2x2ai") (f (quote (("serde-derive" "serde") ("rkyv-derive" "rkyv" "ptr_meta") ("packed") ("default-derive"))))))

(define-public crate-dynamic-array-0.2.1 (c (n "dynamic-array") (v "0.2.1") (d (list (d (n "ptr_meta") (r "~0.1.3") (o #t) (k 0)) (d (n "rkyv") (r "^0.7.11") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.127") (o #t) (d #t) (k 0)))) (h "1jx8k3fpjk3ibm0bwb7z4fkj9svnr0sn4y0a5f65flnb03qsy5vc") (f (quote (("serde-derive" "serde") ("rkyv-derive" "rkyv" "ptr_meta") ("packed") ("default-derive"))))))

(define-public crate-dynamic-array-0.2.2 (c (n "dynamic-array") (v "0.2.2") (d (list (d (n "ptr_meta") (r "~0.1.3") (o #t) (k 0)) (d (n "rkyv") (r "^0.7.11") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.127") (o #t) (d #t) (k 0)))) (h "0pdzv6dg43yj0j69ns3phd01gj6yw1czai36f4y10jmp0ibz56j3") (f (quote (("serde-derive" "serde") ("rkyv-derive" "rkyv" "ptr_meta") ("packed") ("default-derive"))))))

(define-public crate-dynamic-array-0.2.3 (c (n "dynamic-array") (v "0.2.3") (d (list (d (n "ptr_meta") (r "~0.1.3") (o #t) (k 0)) (d (n "rkyv") (r "^0.7.11") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.127") (o #t) (d #t) (k 0)))) (h "1xfsv95gqs8kc5dyl5gvfar33cmjk5ikk6zb13kflblbgr7fqs3v") (f (quote (("serde-derive" "serde") ("rkyv-derive" "rkyv" "ptr_meta") ("packed") ("default-derive"))))))

