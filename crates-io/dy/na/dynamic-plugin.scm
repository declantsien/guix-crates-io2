(define-module (crates-io dy na dynamic-plugin) #:use-module (crates-io))

(define-public crate-dynamic-plugin-0.1.0 (c (n "dynamic-plugin") (v "0.1.0") (d (list (d (n "dynamic-plugin-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.155") (d #t) (k 0)) (d (n "libloading") (r "^0.8.3") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.61") (d #t) (k 0)))) (h "0b29sagj76ah7ggw2cm2lm2h78az77igdvdfibvnjsffzm3knn9a") (f (quote (("host" "dynamic-plugin-macros/host") ("client" "dynamic-plugin-macros/client"))))))

