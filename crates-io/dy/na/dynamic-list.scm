(define-module (crates-io dy na dynamic-list) #:use-module (crates-io))

(define-public crate-dynamic-list-0.1.0 (c (n "dynamic-list") (v "0.1.0") (h "1gv6dqsn843z3nyl0sasqmlgmm5rmzhbi5jszn5v9qnkrah075hp")))

(define-public crate-dynamic-list-0.2.0 (c (n "dynamic-list") (v "0.2.0") (d (list (d (n "typenum") (r "^1.17.0") (f (quote ("const-generics"))) (d #t) (k 0)))) (h "0qgiknwrjz48ylw2sjhi44252r5a80kj6bpsdc9p26zrqj9bvbkk")))

(define-public crate-dynamic-list-0.3.0 (c (n "dynamic-list") (v "0.3.0") (d (list (d (n "typenum") (r "^1.17.0") (f (quote ("const-generics"))) (d #t) (k 0)))) (h "1rhqxrz8kjgszzny32c23194gq2dd47b1b8ncn45r3aspbnib6wh")))

