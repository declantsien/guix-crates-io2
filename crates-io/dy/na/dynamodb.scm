(define-module (crates-io dy na dynamodb) #:use-module (crates-io))

(define-public crate-dynamodb-0.1.0 (c (n "dynamodb") (v "0.1.0") (h "0yjy9l91ipjisvrl2zrb2q1w0r6nzkyyj642id8m5zc46wf86j6s") (y #t)))

(define-public crate-dynamodb-0.0.0-alpha (c (n "dynamodb") (v "0.0.0-alpha") (h "0i6dyh3jma4lvl9cp6njxs48x69j3rhcllwj35idxb3n3cqnbvlv")))

