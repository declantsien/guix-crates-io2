(define-module (crates-io dy na dynamic-proxy) #:use-module (crates-io))

(define-public crate-dynamic-proxy-0.1.0 (c (n "dynamic-proxy") (v "0.1.0") (d (list (d (n "dynamic-proxy-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "dynamic-proxy-types") (r "^0.1.0") (d #t) (k 0)))) (h "0wc40gidz0jjgj6wn7avm1s0rsh21p6cpxjikdpw4hsz2cnr9viy")))

(define-public crate-dynamic-proxy-0.2.0 (c (n "dynamic-proxy") (v "0.2.0") (d (list (d (n "dynamic-proxy-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "dynamic-proxy-types") (r "^0.2.0") (d #t) (k 0)))) (h "1r71dd3sgjfmj66qpc7nvjwkg2rvgiv6v488lrx0l70iyk5bhgyv")))

(define-public crate-dynamic-proxy-0.3.0 (c (n "dynamic-proxy") (v "0.3.0") (d (list (d (n "dynamic-proxy-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "dynamic-proxy-types") (r "^0.3.0") (d #t) (k 0)))) (h "06i368aqihkpk5w7ajd5h7qc9hl1j0dm36a6x9yg1hmqj23pp644")))

(define-public crate-dynamic-proxy-0.4.0 (c (n "dynamic-proxy") (v "0.4.0") (d (list (d (n "dynamic-proxy-macros") (r "^0.2.0") (d #t) (k 0)) (d (n "dynamic-proxy-types") (r "^0.5.0") (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0z6x75hc4a4yjj3v7wvz1i24krka0pik9qhxwr48cb6p37ffk26w")))

