(define-module (crates-io dy na dynamic_graph) #:use-module (crates-io))

(define-public crate-dynamic_graph-0.0.1 (c (n "dynamic_graph") (v "0.0.1") (d (list (d (n "generativity") (r "^1.0.0") (d #t) (k 0)))) (h "1rqh393xfpphk7dg5j7n7s1ixqfh962ykgf7913vyl778ndjysvc")))

(define-public crate-dynamic_graph-0.0.2 (c (n "dynamic_graph") (v "0.0.2") (d (list (d (n "generativity") (r "^1.0.0") (d #t) (k 0)))) (h "1mia3zp29p2y35x83pbb9xxl73xvwixwwmivwfc4lnipqzjrfi87") (y #t)))

(define-public crate-dynamic_graph-0.0.3 (c (n "dynamic_graph") (v "0.0.3") (d (list (d (n "generativity") (r "^1.0.0") (d #t) (k 0)))) (h "027rhpwdhqy2czrfba5gg1m2bcf5bj27cqi0l481p9nh58iaqpsp")))

(define-public crate-dynamic_graph-0.0.4 (c (n "dynamic_graph") (v "0.0.4") (d (list (d (n "generativity") (r "^1.0.0") (d #t) (k 0)))) (h "0xhh9hzk8wmzb0x8cjfb6wzrk3pnb14xgavb27815lzgl8h8jsvn")))

(define-public crate-dynamic_graph-0.1.4 (c (n "dynamic_graph") (v "0.1.4") (d (list (d (n "generativity") (r "^1.0.0") (d #t) (k 0)) (d (n "unsafer") (r "^0.1.1") (d #t) (k 0)))) (h "0iz3l97nm7x5wfchmr63w6vq7kv027dhzhzdyrxmln28b9hyszvc")))

(define-public crate-dynamic_graph-0.1.5 (c (n "dynamic_graph") (v "0.1.5") (d (list (d (n "generativity") (r "^1.0.0") (d #t) (k 0)) (d (n "unsafer") (r "^0.1.1") (d #t) (k 0)))) (h "1x66chg11p8b86ck13kknak8lk55dxn2jvr37d2b3ii0xa8pmkxm")))

