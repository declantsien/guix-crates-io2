(define-module (crates-io dy na dynamo) #:use-module (crates-io))

(define-public crate-dynamo-0.1.0 (c (n "dynamo") (v "0.1.0") (d (list (d (n "piston_meta") (r "^0.25.0") (d #t) (k 0)) (d (n "rand") (r "^0.3.13") (d #t) (k 0)) (d (n "range") (r "^0.3.1") (d #t) (k 0)))) (h "0v8vkq5j2hazpkialm2pzbx2pwsl6s0qr0a5q5shpc8xpl12pn57")))

