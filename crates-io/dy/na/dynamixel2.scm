(define-module (crates-io dy na dynamixel2) #:use-module (crates-io))

(define-public crate-dynamixel2-0.1.0 (c (n "dynamixel2") (v "0.1.0") (d (list (d (n "assert2") (r "^0.2.1") (d #t) (k 2)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 2)) (d (n "log") (r "^0.4.8") (o #t) (d #t) (k 0)) (d (n "serial") (r "^0.4.0") (d #t) (k 2)))) (h "1i48bni7f4laskgr6c6sdf0sivj125i13dzpl21w1yggs8mcgnzm") (y #t)))

(define-public crate-dynamixel2-0.1.1 (c (n "dynamixel2") (v "0.1.1") (d (list (d (n "assert2") (r "^0.2.1") (d #t) (k 2)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 2)) (d (n "log") (r "^0.4.8") (o #t) (d #t) (k 0)) (d (n "serial") (r "^0.4.0") (d #t) (k 2)))) (h "049ixhdfiiz0jjlwmw8ka2p7pj5byswkxd5jz4k3mcai3i4j71al") (y #t)))

(define-public crate-dynamixel2-0.1.2 (c (n "dynamixel2") (v "0.1.2") (d (list (d (n "assert2") (r "^0.2.1") (d #t) (k 2)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 2)) (d (n "log") (r "^0.4.8") (o #t) (d #t) (k 0)) (d (n "serial") (r "^0.4.0") (d #t) (k 2)))) (h "16ksfbxq2iy7ck4pmraprrpg39z5c53rmngrmqdw0lnzgpw21rm5")))

(define-public crate-dynamixel2-0.1.3 (c (n "dynamixel2") (v "0.1.3") (d (list (d (n "assert2") (r "^0.2.1") (d #t) (k 2)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 2)) (d (n "log") (r "^0.4.8") (o #t) (d #t) (k 0)) (d (n "serial") (r "^0.4.0") (d #t) (k 2)))) (h "1l7q6fcr64ydycyzymn87ykrjl8pmqqh9fpsjd44cm5d336fszxf")))

(define-public crate-dynamixel2-0.1.4 (c (n "dynamixel2") (v "0.1.4") (d (list (d (n "assert2") (r "^0.3.3") (d #t) (k 2)) (d (n "env_logger") (r "^0.8.1") (d #t) (k 2)) (d (n "log") (r "^0.4.8") (o #t) (d #t) (k 0)) (d (n "serial") (r "^0.4.0") (d #t) (k 2)))) (h "1apw8685y4r1m1xhzjmjd24v035846lp74n3l4frg0ayflwi4ngf")))

(define-public crate-dynamixel2-0.2.0 (c (n "dynamixel2") (v "0.2.0") (d (list (d (n "assert2") (r "^0.3.3") (d #t) (k 2)) (d (n "env_logger") (r "^0.8.1") (d #t) (k 2)) (d (n "log") (r "^0.4.8") (o #t) (d #t) (k 0)) (d (n "serial") (r "^0.4.0") (d #t) (k 2)))) (h "08fk3vznf5rsivxwx8wmllpqh8424cyh7hy4jmsafli8rlw5gb9w")))

(define-public crate-dynamixel2-0.2.1 (c (n "dynamixel2") (v "0.2.1") (d (list (d (n "assert2") (r "^0.3.3") (d #t) (k 2)) (d (n "env_logger") (r "^0.8.1") (d #t) (k 2)) (d (n "log") (r "^0.4.8") (o #t) (d #t) (k 0)) (d (n "serial") (r "^0.4.0") (d #t) (k 2)))) (h "1qh40z6qvh937zdzjcmyi3zfxqnwr2m586ghc3ssp2p5cjk8q0bz")))

(define-public crate-dynamixel2-0.2.2 (c (n "dynamixel2") (v "0.2.2") (d (list (d (n "assert2") (r "^0.3.3") (d #t) (k 2)) (d (n "env_logger") (r "^0.8.1") (d #t) (k 2)) (d (n "log") (r "^0.4.8") (o #t) (d #t) (k 0)) (d (n "serial") (r "^0.4.0") (d #t) (k 2)))) (h "0cjvhpzpvfyv2w6k5m556zljvvw0ncs1k20zmm6hrfb45f3n8sc5")))

(define-public crate-dynamixel2-0.2.3 (c (n "dynamixel2") (v "0.2.3") (d (list (d (n "assert2") (r "^0.3.3") (d #t) (k 2)) (d (n "env_logger") (r "^0.8.1") (d #t) (k 2)) (d (n "log") (r "^0.4.8") (o #t) (d #t) (k 0)) (d (n "serial") (r "^0.4.0") (d #t) (k 2)))) (h "1sikhpdf4bnspl9r44qh2kk8hzmm6ipsa0ac05q9n0gfwpas6pim")))

(define-public crate-dynamixel2-0.3.0 (c (n "dynamixel2") (v "0.3.0") (d (list (d (n "assert2") (r "^0.3.3") (d #t) (k 2)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 2)) (d (n "log") (r "^0.4.8") (o #t) (d #t) (k 0)) (d (n "serial2") (r "^0.1.3") (d #t) (k 0)))) (h "14fphaslx8dr72di64mzqrqwb07l2m0cnhnw7m76a3jggf37v39c")))

(define-public crate-dynamixel2-0.3.1 (c (n "dynamixel2") (v "0.3.1") (d (list (d (n "assert2") (r "^0.3.3") (d #t) (k 2)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 2)) (d (n "log") (r "^0.4.8") (o #t) (d #t) (k 0)) (d (n "serial2") (r "^0.1.3") (d #t) (k 0)))) (h "17x5r94b1r4nz8zlqjramg8ba1m25ms5gvzvpl5vmh42x0ajb4q1")))

(define-public crate-dynamixel2-0.4.0 (c (n "dynamixel2") (v "0.4.0") (d (list (d (n "assert2") (r "^0.3.3") (d #t) (k 2)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 2)) (d (n "log") (r "^0.4.8") (o #t) (d #t) (k 0)) (d (n "serial2") (r "^0.1.3") (d #t) (k 0)))) (h "1cp06a7bpj9pl71zjd8w01s6nv48dllpcv8b653wz5p9n9a2qmks") (y #t)))

(define-public crate-dynamixel2-0.4.1 (c (n "dynamixel2") (v "0.4.1") (d (list (d (n "assert2") (r "^0.3.3") (d #t) (k 2)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 2)) (d (n "log") (r "^0.4.8") (o #t) (d #t) (k 0)) (d (n "serial2") (r "^0.1.3") (d #t) (k 0)))) (h "1dyzdcdy3sjnrfhi9mpsa0i4k38bvr1d2zplndgvxrbm4jx7v1ja")))

(define-public crate-dynamixel2-0.4.2 (c (n "dynamixel2") (v "0.4.2") (d (list (d (n "assert2") (r "^0.3.3") (d #t) (k 2)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "log") (r "^0.4.8") (o #t) (d #t) (k 0)) (d (n "serial2") (r "^0.1.7") (d #t) (k 0)))) (h "0ycq3rzkgr54sw747zqzp0yrc7r33xb5y33c790nda2r0dzawp51")))

(define-public crate-dynamixel2-0.5.0 (c (n "dynamixel2") (v "0.5.0") (d (list (d (n "assert2") (r "^0.3.3") (d #t) (k 2)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "log") (r "^0.4.8") (o #t) (d #t) (k 0)) (d (n "serial2") (r "^0.2.10") (d #t) (k 0)))) (h "06zljs9g12nknmjsyi7ird63qsmpmy21fhpna2raf5hlhbg6rkh7")))

(define-public crate-dynamixel2-0.5.1 (c (n "dynamixel2") (v "0.5.1") (d (list (d (n "assert2") (r "^0.3.3") (d #t) (k 2)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "log") (r "^0.4.8") (o #t) (d #t) (k 0)) (d (n "serial2") (r "^0.2.10") (d #t) (k 0)))) (h "0qin2swy8adcxamf2mpmxqvxdr87mhf8rdfn4ad83pib0wl6wzkc")))

(define-public crate-dynamixel2-0.6.0 (c (n "dynamixel2") (v "0.6.0") (d (list (d (n "assert2") (r "^0.3.3") (d #t) (k 2)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "log") (r "^0.4.8") (o #t) (d #t) (k 0)) (d (n "serial2") (r "^0.2.10") (d #t) (k 0)))) (h "08cl41509ygzk2glwfrgjqwxrbgmrxzblm7g8vpcqgisav9m62lx")))

(define-public crate-dynamixel2-0.6.1 (c (n "dynamixel2") (v "0.6.1") (d (list (d (n "assert2") (r "^0.3.3") (d #t) (k 2)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "log") (r "^0.4.8") (o #t) (d #t) (k 0)) (d (n "serial2") (r "^0.2.10") (d #t) (k 0)))) (h "0gwck6p4vgvr1bhkrd8l7zbsbpilg08x909pq4pl1c3d9immkvqx")))

(define-public crate-dynamixel2-0.7.0 (c (n "dynamixel2") (v "0.7.0") (d (list (d (n "assert2") (r "^0.3.3") (d #t) (k 2)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "log") (r "^0.4.8") (o #t) (d #t) (k 0)) (d (n "serial2") (r "^0.2.10") (d #t) (k 0)))) (h "1wq3wpjbrcyqc1q41r7nxqhrkh0n0762qhyya76g5mnxr9v9ackd")))

(define-public crate-dynamixel2-0.7.1 (c (n "dynamixel2") (v "0.7.1") (d (list (d (n "assert2") (r "^0.3.3") (d #t) (k 2)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "log") (r "^0.4.8") (o #t) (d #t) (k 0)) (d (n "serial2") (r "^0.2.10") (d #t) (k 0)))) (h "0j11mi2jqpzafx2kjs5vnvx9341hrh02x1f8qwv8i2v71g7f8r8x")))

(define-public crate-dynamixel2-0.7.2 (c (n "dynamixel2") (v "0.7.2") (d (list (d (n "assert2") (r "^0.3.3") (d #t) (k 2)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "log") (r "^0.4.8") (o #t) (d #t) (k 0)) (d (n "serial2") (r "^0.2.10") (d #t) (k 0)))) (h "0wkw5rdidwq4fwvyqpbsmjjk73zcy6y8yzw0lzhndi4ja5dyacfh")))

(define-public crate-dynamixel2-0.8.0 (c (n "dynamixel2") (v "0.8.0") (d (list (d (n "assert2") (r "^0.3.3") (d #t) (k 2)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "log") (r "^0.4.8") (o #t) (d #t) (k 0)) (d (n "serial2") (r "^0.2.10") (d #t) (k 0)))) (h "0gzcvs2isnl196jya98inxhahy7ipjx6viwml433llzbgfdh5krj")))

