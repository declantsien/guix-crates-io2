(define-module (crates-io dy na dynamodb_testcontainer) #:use-module (crates-io))

(define-public crate-dynamodb_testcontainer-0.1.0 (c (n "dynamodb_testcontainer") (v "0.1.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.2") (d #t) (k 2)) (d (n "rusoto_core") (r "^0.35") (d #t) (k 2)) (d (n "rusoto_credential") (r "^0.14") (d #t) (k 2)) (d (n "rusoto_dynamodb") (r "^0.35") (d #t) (k 2)) (d (n "spectral") (r "^0.6") (d #t) (k 2)) (d (n "tc_core") (r "^0.2") (d #t) (k 0)) (d (n "testcontainers") (r "^0.3.1") (d #t) (k 2)))) (h "0j7fbw5ck4dzhzw95qsmi1ck9fsrsm35x1qm3r7lpdl3ai5k1z8c")))

