(define-module (crates-io dy na dynamic-pooling) #:use-module (crates-io))

(define-public crate-dynamic-pooling-1.0.0 (c (n "dynamic-pooling") (v "1.0.0") (d (list (d (n "crossbeam-queue") (r "^0.3.5") (f (quote ("alloc"))) (k 0)))) (h "11q9l9kdbvxny5m2cyvbfysnaap93x7ifjschdxfl2r9wibvkilr") (f (quote (("std" "crossbeam-queue/std") ("default" "std"))))))

