(define-module (crates-io dy na dynamodel) #:use-module (crates-io))

(define-public crate-dynamodel-0.1.0 (c (n "dynamodel") (v "0.1.0") (d (list (d (n "aws-sdk-dynamodb") (r "^1") (d #t) (k 0)) (d (n "dynamodel_derive") (r "^0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1qx5q2kac3z4jdl1c8djbypnzf113nwd5qni3kgyqhw4qxcfznnm")))

(define-public crate-dynamodel-0.2.0 (c (n "dynamodel") (v "0.2.0") (d (list (d (n "aws-sdk-dynamodb") (r "^1") (d #t) (k 0)) (d (n "dynamodel_derive") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1jdkls562licnp82alrm7jndxiiwmkw930bmdnx6wdicx2f1937b")))

(define-public crate-dynamodel-0.3.0 (c (n "dynamodel") (v "0.3.0") (d (list (d (n "aws-sdk-dynamodb") (r "^1") (d #t) (k 0)) (d (n "dynamodel_derive") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1bbxxddcvr9nh7hhm2nsr21wirbaafkf44nl4p5g3mk5ws8bvhin")))

(define-public crate-dynamodel-0.3.1 (c (n "dynamodel") (v "0.3.1") (d (list (d (n "aws-sdk-dynamodb") (r "^1") (d #t) (k 0)) (d (n "dynamodel_derive") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1gpq59v4xvihnkc6x39y6ldx4rxm70vdbhjs9f1azqwk8dmd286d")))

