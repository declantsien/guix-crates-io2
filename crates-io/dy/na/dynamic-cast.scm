(define-module (crates-io dy na dynamic-cast) #:use-module (crates-io))

(define-public crate-dynamic-cast-0.1.0 (c (n "dynamic-cast") (v "0.1.0") (d (list (d (n "arraybox") (r "^0.1.0") (d #t) (k 0)) (d (n "downcast-rs") (r "^1.2.0") (k 0)))) (h "05532b9m0wkbh5vhzp8zg4h9j9886nk7shj0pv87phiddsxhc006")))

(define-public crate-dynamic-cast-0.1.1 (c (n "dynamic-cast") (v "0.1.1") (d (list (d (n "arraybox") (r "^0.1.0") (d #t) (k 0)) (d (n "downcast-rs") (r "^1.2.0") (k 0)))) (h "1kwc6g3hlcp43d080sbn17m3db1ad3hj08vsbjmpskr159i4irx9")))

(define-public crate-dynamic-cast-0.1.2 (c (n "dynamic-cast") (v "0.1.2") (d (list (d (n "arraybox") (r "^0.1.0") (d #t) (k 0)) (d (n "downcast-rs") (r "^1.2.0") (k 0)))) (h "0jg8maxkbws30f13hxaqxb43m8mlq9ir915lahsz4j4ls4j8lqv3")))

(define-public crate-dynamic-cast-0.1.3 (c (n "dynamic-cast") (v "0.1.3") (d (list (d (n "arraybox") (r "^0.1.0") (d #t) (k 0)) (d (n "downcast-rs") (r "^1.2.0") (k 0)))) (h "0snsy1cd74d01c8v81120098yb2mk6p69m6np5imfsf5nlag6ykv")))

