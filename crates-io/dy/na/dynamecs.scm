(define-module (crates-io dy na dynamecs) #:use-module (crates-io))

(define-public crate-dynamecs-0.0.1 (c (n "dynamecs") (v "0.0.1") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 2)) (d (n "cool_asserts") (r "^1.1.1") (d #t) (k 2)) (d (n "erased-serde") (r "^0.3") (d #t) (k 0)) (d (n "eyre") (r "^0.6.5") (d #t) (k 0)) (d (n "once_cell") (r "^1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "065ji8d5wg2w405g6m7gmgmkj7f4l1qnsqvi4w8dsvqww04hqd0z")))

(define-public crate-dynamecs-0.0.2 (c (n "dynamecs") (v "0.0.2") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 2)) (d (n "cool_asserts") (r "^1.1.1") (d #t) (k 2)) (d (n "erased-serde") (r "^0.3") (d #t) (k 0)) (d (n "eyre") (r "^0.6.5") (d #t) (k 0)) (d (n "once_cell") (r "^1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1jjh8x78k0mxalg98g2kqygmw271ikz6y6hh6qlwywn20dy4k6yf")))

(define-public crate-dynamecs-0.0.3 (c (n "dynamecs") (v "0.0.3") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 2)) (d (n "cool_asserts") (r "^1.1.1") (d #t) (k 2)) (d (n "erased-serde") (r "^0.3") (d #t) (k 0)) (d (n "eyre") (r "^0.6.5") (d #t) (k 0)) (d (n "once_cell") (r "^1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0xz3ns8l38akvylmcv0vm968xzjmj25ddp0h1l81aypad41hwm5z")))

(define-public crate-dynamecs-0.0.4 (c (n "dynamecs") (v "0.0.4") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 2)) (d (n "cool_asserts") (r "^1.1.1") (d #t) (k 2)) (d (n "erased-serde") (r "^0.3") (d #t) (k 0)) (d (n "eyre") (r "^0.6.5") (d #t) (k 0)) (d (n "once_cell") (r "^1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0ysy3gl70c6sr9nfr93b7h8n712p0z5fsmrsjrhx3nm74ic97qcd")))

