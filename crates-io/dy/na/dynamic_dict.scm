(define-module (crates-io dy na dynamic_dict) #:use-module (crates-io))

(define-public crate-dynamic_dict-0.1.0 (c (n "dynamic_dict") (v "0.1.0") (h "16grf03nwz0is6809cii6rrxv30vxiygkfw297jw579jndc7wajj")))

(define-public crate-dynamic_dict-0.1.1 (c (n "dynamic_dict") (v "0.1.1") (h "02vczpcfvipj4cnwv4w8sgdj9zvzy3x4mc2xybbmpldj8vf48h0n")))

(define-public crate-dynamic_dict-0.1.2 (c (n "dynamic_dict") (v "0.1.2") (h "0hc7qz7208whsn0mw1agnldayr8x5mgfvbby3p30cn9iyxgcncnx")))

(define-public crate-dynamic_dict-0.1.3 (c (n "dynamic_dict") (v "0.1.3") (h "1kwixsw3y3lgzh7kkpkrwczak5b9aczminc0l54nx6viniafqiqw")))

(define-public crate-dynamic_dict-0.1.4 (c (n "dynamic_dict") (v "0.1.4") (h "1rg2ix0pq34ydrnf2a4s7s746xpd5x21npaji6yx3m2wf820bh92")))

(define-public crate-dynamic_dict-0.1.5 (c (n "dynamic_dict") (v "0.1.5") (h "0vlv6vv0qpsqgaml5xlki63yc4xwmwjrycs2arx4padf1hwzfbir")))

(define-public crate-dynamic_dict-0.1.6 (c (n "dynamic_dict") (v "0.1.6") (h "1xs8axln5aq3l9i25xjxljrcigby2wxn5r1imincxkzvvzz0m9w8")))

(define-public crate-dynamic_dict-0.1.7 (c (n "dynamic_dict") (v "0.1.7") (h "06rlnclq13crgjkg6pnjcnvm85pbg6kj4jdjczdz2f1h9ydsv713")))

(define-public crate-dynamic_dict-0.1.8 (c (n "dynamic_dict") (v "0.1.8") (h "0h800an56wgkj05g29sbs6an8li7b88ly9ma3vdhha7wy37cl3ag")))

(define-public crate-dynamic_dict-0.1.9 (c (n "dynamic_dict") (v "0.1.9") (h "1r7g8jgv1gx71i0xjb7xmdbzmis5skk2bv6dqf4chhvwy1gfi1di")))

(define-public crate-dynamic_dict-0.2.0 (c (n "dynamic_dict") (v "0.2.0") (h "0kh66j23l0mdazqmvz2bniimz01i922mw1zs2glhbybw8l1iqlb4")))

(define-public crate-dynamic_dict-0.2.1 (c (n "dynamic_dict") (v "0.2.1") (h "173m79fy1m48jrhp4hqgqyn1bs8n6gm94gqqd5zi72jb7v98vari")))

(define-public crate-dynamic_dict-0.2.2 (c (n "dynamic_dict") (v "0.2.2") (h "0a3ki5h0rv5v1whik1wi5nkl8rqabp1l409ahk3vyvff06psnybz")))

(define-public crate-dynamic_dict-0.2.3 (c (n "dynamic_dict") (v "0.2.3") (h "0p0dh31x78axmg3kxhzphgwfgprp58sz6j5akpq67iwksadqihzp")))

(define-public crate-dynamic_dict-0.2.4 (c (n "dynamic_dict") (v "0.2.4") (h "0li1la3m6zj6vj10n1kzmqpz4ip37cppw6z8araqkz6r9dfrfc6k")))

