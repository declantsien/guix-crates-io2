(define-module (crates-io dy na dynamic-weighted-index) #:use-module (crates-io))

(define-public crate-dynamic-weighted-index-0.1.0 (c (n "dynamic-weighted-index") (v "0.1.0") (d (list (d (n "assert_float_eq") (r "^1.1") (d #t) (k 2)) (d (n "itertools") (r "^0.10") (d #t) (k 2)) (d (n "pcg_rand") (r "^0.13") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "smallvec") (r "^1.10") (d #t) (k 0)) (d (n "statrs") (r "^0.16") (d #t) (k 2)) (d (n "structopt") (r "^0.3") (d #t) (k 2)))) (h "1il47q8lnp33xrl0h0i6clxdv5i7m3957zkldlc4x5h7fx82agp6")))

