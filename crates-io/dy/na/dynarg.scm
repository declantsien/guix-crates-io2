(define-module (crates-io dy na dynarg) #:use-module (crates-io))

(define-public crate-dynarg-0.1.0 (c (n "dynarg") (v "0.1.0") (d (list (d (n "indexmap") (r "^1.9.2") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.16") (d #t) (k 0)))) (h "03nbmpyb828ij4x7aaicm92zgxf40ff5221sws5dchzmwxnfq2wn")))

(define-public crate-dynarg-0.1.1 (c (n "dynarg") (v "0.1.1") (d (list (d (n "indexmap") (r "^1.9.2") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.16") (d #t) (k 0)))) (h "12yz4p5s1ly0ypa1ai0w3jj3nxxmas1zsc6bk1ib5c55mklaf9vy")))

(define-public crate-dynarg-0.1.2 (c (n "dynarg") (v "0.1.2") (d (list (d (n "indexmap") (r "^1.9.2") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.16") (d #t) (k 0)))) (h "1vzp9iy6fxaskhwx3dwcb7yqnjxick4jni84arz1b27qnmbk3jad")))

(define-public crate-dynarg-0.1.3 (c (n "dynarg") (v "0.1.3") (d (list (d (n "indexmap") (r "^1.9.2") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.16") (d #t) (k 0)))) (h "00xdwb8xi079cnkizwv04anchcwwwq2c8p7p7qxa87lj3qldjhc1")))

(define-public crate-dynarg-1.1.4 (c (n "dynarg") (v "1.1.4") (d (list (d (n "indexmap") (r "^1.9.2") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.16") (d #t) (k 0)))) (h "1mxg1m6m2dwav61qkkq8cq0gan7kg56s4isbd5db49kzpssnmp71")))

(define-public crate-dynarg-1.1.5 (c (n "dynarg") (v "1.1.5") (d (list (d (n "indexmap") (r "^1.9.2") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.16") (d #t) (k 0)))) (h "0nb8yh00bpp3434xim52715pmf5wa50g58sh9358s7kipwh569xb")))

(define-public crate-dynarg-2.0.0 (c (n "dynarg") (v "2.0.0") (d (list (d (n "indexmap") (r "^1.9.2") (d #t) (k 0)) (d (n "snafu") (r "^0.7.4") (d #t) (k 0)))) (h "0kiz0111j885gwixkbhrasl8pg7kfd5z1iy6rzrxwc7pkafwdd2r") (f (quote (("used") ("default" "used"))))))

(define-public crate-dynarg-2.0.1 (c (n "dynarg") (v "2.0.1") (d (list (d (n "indexmap") (r "^1.9.2") (d #t) (k 0)) (d (n "snafu") (r "^0.7.4") (d #t) (k 0)))) (h "1gzmmrg1zxkvs6bigngx1i6lnqfgwnljppvc4i0gq8anp3k77ihh") (f (quote (("used") ("default" "used"))))))

(define-public crate-dynarg-2.0.2 (c (n "dynarg") (v "2.0.2") (d (list (d (n "indexmap") (r "^1.9.2") (d #t) (k 0)) (d (n "snafu") (r "^0.7.4") (d #t) (k 0)))) (h "1h9n1ghmaqjd459f317bfv81qmikwhmrcjzlvsqs78xlj2v1y5vi") (f (quote (("used") ("default" "used"))))))

(define-public crate-dynarg-2.0.3 (c (n "dynarg") (v "2.0.3") (d (list (d (n "indexmap") (r "^1.9.2") (d #t) (k 0)) (d (n "snafu") (r "^0.7.4") (d #t) (k 0)))) (h "0s2pxmdl4rkhxnvrb6m03ygvzvxlhf87zdd8an4qmcjkpas8f4ji") (f (quote (("used") ("default" "used"))))))

(define-public crate-dynarg-2.1.0 (c (n "dynarg") (v "2.1.0") (d (list (d (n "indexmap") (r "^1.9.2") (d #t) (k 0)) (d (n "snafu") (r "^0.7.4") (d #t) (k 0)) (d (n "skeptic") (r "^0.13") (d #t) (k 2)) (d (n "skeptic") (r "^0.13") (d #t) (k 1)))) (h "0y8j6n9zdxq5la71p9ncavwhizfiw2ijgjf0bw22d4wg5rp1h8g6") (f (quote (("used") ("default" "used"))))))

(define-public crate-dynarg-2.1.1 (c (n "dynarg") (v "2.1.1") (d (list (d (n "indexmap") (r "^1.9.2") (d #t) (k 0)) (d (n "snafu") (r "^0.7.4") (d #t) (k 0)) (d (n "skeptic") (r "^0.13") (d #t) (k 2)) (d (n "skeptic") (r "^0.13") (d #t) (k 1)))) (h "136ihiz83z7w8sisimhq7b7bbzhgi530kv4pcc9nkgpd0qqg1y8a") (f (quote (("used") ("default" "used"))))))

(define-public crate-dynarg-2.1.2 (c (n "dynarg") (v "2.1.2") (d (list (d (n "indexmap") (r "^1.9.2") (d #t) (k 0)) (d (n "snafu") (r "^0.7.4") (d #t) (k 0)) (d (n "skeptic") (r "^0.13") (d #t) (k 2)) (d (n "skeptic") (r "^0.13") (d #t) (k 1)))) (h "060v9ia6bs4h7y1xc662il87pwhdmi4a2qaj5wa9y4a439vprgsb") (f (quote (("used") ("default" "used"))))))

(define-public crate-dynarg-2.1.3 (c (n "dynarg") (v "2.1.3") (d (list (d (n "indexmap") (r "^1.9.2") (d #t) (k 0)) (d (n "snafu") (r "^0.7.4") (d #t) (k 0)) (d (n "skeptic") (r "^0.13") (d #t) (k 2)) (d (n "skeptic") (r "^0.13") (d #t) (k 1)))) (h "1d0pnflcpkds04irfk78kbd7s838fw89av9336n8m73hsgf7xk68") (f (quote (("used") ("default" "used"))))))

(define-public crate-dynarg-2.1.4 (c (n "dynarg") (v "2.1.4") (d (list (d (n "indexmap") (r "^1.9.2") (d #t) (k 0)) (d (n "snafu") (r "^0.7.4") (d #t) (k 0)) (d (n "skeptic") (r "^0.13") (d #t) (k 2)) (d (n "skeptic") (r "^0.13") (d #t) (k 1)))) (h "0fxmvr0zrpaff4xai16lvh846246bw1pfkqv85n4hlwv8m5bl7fm") (f (quote (("used") ("default" "used"))))))

