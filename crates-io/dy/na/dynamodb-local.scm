(define-module (crates-io dy na dynamodb-local) #:use-module (crates-io))

(define-public crate-dynamodb-local-0.1.0 (c (n "dynamodb-local") (v "0.1.0") (h "198lv1wqff06ax0am7xavqz4g23mc6vxa4ppimgvngvz7c9w6hz8") (y #t)))

(define-public crate-dynamodb-local-0.0.0-alpha (c (n "dynamodb-local") (v "0.0.0-alpha") (h "0f9yvz3fsv4lj92dm4zw8jg8mgy85n8vbf2c30bys8zba7995c9p")))

