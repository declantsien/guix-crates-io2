(define-module (crates-io dy na dynamodb-mutex-rs) #:use-module (crates-io))

(define-public crate-dynamodb-mutex-rs-0.1.0 (c (n "dynamodb-mutex-rs") (v "0.1.0") (d (list (d (n "chrono") (r "~0.4.19") (d #t) (k 0)) (d (n "futures") (r "~0.3.8") (d #t) (k 2)) (d (n "rusoto_core") (r "~0.45.0") (d #t) (k 0)) (d (n "rusoto_dynamodb") (r "~0.45.0") (d #t) (k 0)) (d (n "thiserror") (r "~1.0.22") (d #t) (k 0)) (d (n "tokio") (r "~0.2") (f (quote ("macros" "rt-core"))) (d #t) (k 2)))) (h "15m91pgh9fjfv7w7kjfyq2i7229i7jy9r1jqcv9gpid49kn38kan") (y #t)))

