(define-module (crates-io dy na dynamic-dispatch) #:use-module (crates-io))

(define-public crate-dynamic-dispatch-0.1.0 (c (n "dynamic-dispatch") (v "0.1.0") (d (list (d (n "dynamic-dispatch-proc-macro") (r "^0.1.0") (d #t) (k 0)))) (h "1spw0cpsrpv3rszbmc86x6a4kwb5cai86h0cb6asc0qpvprp9p37")))

(define-public crate-dynamic-dispatch-0.2.0 (c (n "dynamic-dispatch") (v "0.2.0") (d (list (d (n "dynamic-dispatch-proc-macro") (r "^0.2.0") (d #t) (k 0)))) (h "1bv0wbxfaykbrwf8izkvyb6vs1l3hz5axkdwv5nhlfga2lz45zdz")))

(define-public crate-dynamic-dispatch-0.3.0 (c (n "dynamic-dispatch") (v "0.3.0") (d (list (d (n "dynamic-dispatch-proc-macro") (r "^0.3.0") (d #t) (k 0)))) (h "08lzfxj41i243vf4f4pkjk7l9whzpylxyyal7hmxmz1ll82irwws")))

(define-public crate-dynamic-dispatch-0.4.0 (c (n "dynamic-dispatch") (v "0.4.0") (d (list (d (n "dynamic-dispatch-proc-macro") (r "^0.3.0") (d #t) (k 0)))) (h "18wrrf82vgfr7xnnpfhdqrpzrxjpgx55g6rk41s1b7n3n6341ad2")))

(define-public crate-dynamic-dispatch-0.4.1 (c (n "dynamic-dispatch") (v "0.4.1") (d (list (d (n "dynamic-dispatch-proc-macro") (r "^0.4.0") (d #t) (k 0)))) (h "0xmrymbz7wasm2hqi9nkbwyg0z740n5si4f2vfynfwbkj0yr2fyw")))

(define-public crate-dynamic-dispatch-0.4.2 (c (n "dynamic-dispatch") (v "0.4.2") (d (list (d (n "dynamic-dispatch-proc-macro") (r "^0.4.1") (d #t) (k 0)))) (h "1989v68maabqhk12c2wxzqqqjpvc8gkc00aq9p9yhzhiawqhi0ll")))

(define-public crate-dynamic-dispatch-0.4.3 (c (n "dynamic-dispatch") (v "0.4.3") (d (list (d (n "dynamic-dispatch-proc-macro") (r "^0.4.2") (d #t) (k 0)))) (h "1z4zsyfsa8x0brp98ibdxy65d3jwdiyfxh2yhi1hfbfa6c1hmymk")))

(define-public crate-dynamic-dispatch-0.5.3 (c (n "dynamic-dispatch") (v "0.5.3") (d (list (d (n "dynamic-dispatch-proc-macro") (r "^0.4.2") (d #t) (k 0)))) (h "12lg5sz3w7p6i53vbxxkf8b47kxans196r3mwj0virnbidillva3")))

(define-public crate-dynamic-dispatch-0.5.4 (c (n "dynamic-dispatch") (v "0.5.4") (d (list (d (n "dynamic-dispatch-proc-macro") (r "^0.5.0") (d #t) (k 0)))) (h "03kyr1k2q373ynl8hk18xic0wisflkccws3p5kbjix3vlmpsq8zx")))

