(define-module (crates-io dy na dynamic-html) #:use-module (crates-io))

(define-public crate-dynamic-html-1.0.0 (c (n "dynamic-html") (v "1.0.0") (d (list (d (n "pathdiff") (r "^0.2.1") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (o #t) (d #t) (k 0)) (d (n "wasm-bindgen") (r "=0.2.81") (o #t) (d #t) (k 0)))) (h "1ba92p6ag2llf9l4zjccc3d5by47hffzbv9i34i3l4ilnn0abi12") (f (quote (("default" "wasm")))) (s 2) (e (quote (("wasm" "dep:wasm-bindgen" "dep:serde" "dep:serde_json"))))))

