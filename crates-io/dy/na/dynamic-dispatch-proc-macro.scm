(define-module (crates-io dy na dynamic-dispatch-proc-macro) #:use-module (crates-io))

(define-public crate-dynamic-dispatch-proc-macro-0.1.0 (c (n "dynamic-dispatch-proc-macro") (v "0.1.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.46") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.102") (f (quote ("full"))) (d #t) (k 0)))) (h "1qrp5mvqrwav7fm8q8jbqcyf3h7x0j00p9pywfcc87dilaw5k67z")))

(define-public crate-dynamic-dispatch-proc-macro-0.2.0 (c (n "dynamic-dispatch-proc-macro") (v "0.2.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.46") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.102") (f (quote ("full"))) (d #t) (k 0)))) (h "0ssiw8w4xp6kw9dr3hmhgjpqidxzhh3mxa7fzndqsg7ynvlgp0pl")))

(define-public crate-dynamic-dispatch-proc-macro-0.3.0 (c (n "dynamic-dispatch-proc-macro") (v "0.3.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.46") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.102") (f (quote ("full"))) (d #t) (k 0)))) (h "05rx1hydhx9ihf570fvlpgaw21p9h576d118ldc2400lzpw9f4z9")))

(define-public crate-dynamic-dispatch-proc-macro-0.4.0 (c (n "dynamic-dispatch-proc-macro") (v "0.4.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.46") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.102") (f (quote ("full"))) (d #t) (k 0)))) (h "1n15684n819gdkxy7js2m9010ra0ijhgy541frk7xnz1i1425ydl")))

(define-public crate-dynamic-dispatch-proc-macro-0.4.1 (c (n "dynamic-dispatch-proc-macro") (v "0.4.1") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.46") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.102") (f (quote ("full"))) (d #t) (k 0)))) (h "1h1vj9xpps20s6qg9a1bl6npndvl3ha1i4sh56vwyxgdkalnhkl6")))

(define-public crate-dynamic-dispatch-proc-macro-0.4.2 (c (n "dynamic-dispatch-proc-macro") (v "0.4.2") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "regex") (r "^1.8.1") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "1wcy8bf0gj38rswkl4rwm7c12ngdpd9b0xm06hlmcvwbmyps8y60")))

(define-public crate-dynamic-dispatch-proc-macro-0.5.0 (c (n "dynamic-dispatch-proc-macro") (v "0.5.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "regex") (r "^1.8.1") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "14gbayd9mid75q08lw99gyvg22sglwap9ll3dvnfcm6q6944gb88")))

