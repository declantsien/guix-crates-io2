(define-module (crates-io dy na dynamic-arena) #:use-module (crates-io))

(define-public crate-dynamic-arena-0.1.0 (c (n "dynamic-arena") (v "0.1.0") (d (list (d (n "compiletest_rs") (r "^0.3.1") (d #t) (k 2)))) (h "0rkx0j7cpbjyg0cc2q7h2h61y3b245rvcvi1dlz3fxbkxidwanvv")))

(define-public crate-dynamic-arena-0.1.1 (c (n "dynamic-arena") (v "0.1.1") (d (list (d (n "compiletest_rs") (r "^0.3.1") (d #t) (k 2)))) (h "1bncnlcrbfavqqvfgbda551c91rg9g3vyspidnspz2drjlvz0y0p")))

(define-public crate-dynamic-arena-0.1.2 (c (n "dynamic-arena") (v "0.1.2") (d (list (d (n "compiletest_rs") (r "^0.3.1") (d #t) (k 2)) (d (n "typed-arena") (r "^1.4.1") (d #t) (k 0)))) (h "0lcdjh5ha2xpmfzywsngkbkg97cdp05j9yrkz4ff2gv3phmxbkfa")))

(define-public crate-dynamic-arena-0.1.4 (c (n "dynamic-arena") (v "0.1.4") (d (list (d (n "compiletest_rs") (r "^0.5") (d #t) (k 2)) (d (n "typed-arena") (r "^2") (d #t) (k 0)))) (h "1jcy3yj4y3cdymmbsan9xwwqmwjmxgxqap0qnwnlq0j32ch3hrdl")))

(define-public crate-dynamic-arena-0.1.6 (c (n "dynamic-arena") (v "0.1.6") (d (list (d (n "bumpalo") (r "^3") (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "13mkjy3s8bz75i90w1q2lv98i4c56wamkw4y7zkhmyqs73mjbyqz")))

