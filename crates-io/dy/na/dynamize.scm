(define-module (crates-io dy na dynamize) #:use-module (crates-io))

(define-public crate-dynamize-0.1.0 (c (n "dynamize") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0qc9h8630l1pvapq2m706hhddl6y1imwcilahn98s494ccjn1pwr") (f (quote (("nightly")))) (y #t)))

(define-public crate-dynamize-0.2.0 (c (n "dynamize") (v "0.2.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1ja0nbsqh1grym2jny9xpy6nlxmzgid4g7vqzk97d9v1w3b9s6vz") (f (quote (("nightly")))) (y #t)))

(define-public crate-dynamize-0.3.0 (c (n "dynamize") (v "0.3.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0gyxr47v6saih6s2wwid2p0lfjp56cg322wl6jibd7k5k3vsbz33") (f (quote (("nightly")))) (y #t)))

(define-public crate-dynamize-0.3.1 (c (n "dynamize") (v "0.3.1") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0c39g109mfn1yvdfky92azg40jj52gw80sb1k7wnzj8ybgraxrhj") (f (quote (("nightly")))) (y #t)))

(define-public crate-dynamize-0.3.2 (c (n "dynamize") (v "0.3.2") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0qwq5k12g4y1qjshklf5hhk1g5f4g1j13yzy455aswxjjnlwhl86") (f (quote (("nightly"))))))

(define-public crate-dynamize-0.3.3 (c (n "dynamize") (v "0.3.3") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "04gdbgnr18n60klk85a4laygchz27mf0man6d1kl4w3in8alzan2") (f (quote (("nightly"))))))

(define-public crate-dynamize-0.3.4 (c (n "dynamize") (v "0.3.4") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1iwf4bpskxvnlxylwyjxr7179w1ga4cxay3nbwv1cdil6g0wcvp8") (f (quote (("nightly"))))))

(define-public crate-dynamize-0.3.5 (c (n "dynamize") (v "0.3.5") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1xfl9r2m737xh8idzg83zrzlvkir0hibr4b7250igq6swsnak7in") (f (quote (("nightly"))))))

