(define-module (crates-io dy na dynamodel_derive) #:use-module (crates-io))

(define-public crate-dynamodel_derive-0.1.0 (c (n "dynamodel_derive") (v "0.1.0") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0fd7qrk690lqi175a4hpx7fppas4ar9fpw86a0108bg76jay59j7")))

(define-public crate-dynamodel_derive-0.2.0 (c (n "dynamodel_derive") (v "0.2.0") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "04009m38pwqykysarf1ckwk4shgvnkp770wddvl7xr399f7gqczy")))

(define-public crate-dynamodel_derive-0.3.0 (c (n "dynamodel_derive") (v "0.3.0") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0wgwxc0r2sq1f4g9zlr8q9k2f147nsba0bhi5r1xsa9hclw9mcx9")))

(define-public crate-dynamodel_derive-0.3.1 (c (n "dynamodel_derive") (v "0.3.1") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0aqv7qwy5raa94x2nvy3q13166ygbrvccak5anvf0z7ck5pf1193")))

