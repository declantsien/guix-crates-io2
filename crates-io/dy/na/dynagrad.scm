(define-module (crates-io dy na dynagrad) #:use-module (crates-io))

(define-public crate-dynagrad-0.3.0 (c (n "dynagrad") (v "0.3.0") (d (list (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)))) (h "04wp57w6iq83jfny0lx2g5fcdxx71wn0394zbw15m4dhi1mipmid")))

(define-public crate-dynagrad-0.4.0 (c (n "dynagrad") (v "0.4.0") (d (list (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "ndarray") (r "^0.12.1") (d #t) (k 0)) (d (n "ndarray-rand") (r "^0.9.0") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.6") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 2)))) (h "1faak4w5703jca9azg42fp33xya16l1ind4bb0jr26fsfh8j040z")))

(define-public crate-dynagrad-0.4.1 (c (n "dynagrad") (v "0.4.1") (d (list (d (n "gnuplot") (r "^0.0.32") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "ndarray") (r "^0.12.1") (d #t) (k 0)) (d (n "ndarray-rand") (r "^0.9.0") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.6") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 2)))) (h "1hjaarg4s2lmxdliiij2y3zmhxnqj82jsnq7awnc80a4rvd8n5d7")))

