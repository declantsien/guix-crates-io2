(define-module (crates-io dy na dynamic-proxy-macros) #:use-module (crates-io))

(define-public crate-dynamic-proxy-macros-0.1.0 (c (n "dynamic-proxy-macros") (v "0.1.0") (d (list (d (n "dynamic-proxy-types") (r "^0.1.0") (d #t) (k 0)) (d (n "fern") (r "^0.6.2") (d #t) (k 0)) (d (n "humantime") (r "^2.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.81") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.60") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1rk8xax96gbjli4cnl02g700zfjv8x8qy76irs6wwlhzbyry3n02")))

(define-public crate-dynamic-proxy-macros-0.2.0 (c (n "dynamic-proxy-macros") (v "0.2.0") (d (list (d (n "dynamic-proxy-types") (r "^0.5.0") (d #t) (k 0)) (d (n "fern") (r "^0.6.2") (d #t) (k 0)) (d (n "humantime") (r "^2.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.81") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.60") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1mwb13rrbjgmagvbndzp2y235mganlkv726spxa4mrnr5q0v946k")))

