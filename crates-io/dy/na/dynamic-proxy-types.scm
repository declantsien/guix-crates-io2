(define-module (crates-io dy na dynamic-proxy-types) #:use-module (crates-io))

(define-public crate-dynamic-proxy-types-0.1.0 (c (n "dynamic-proxy-types") (v "0.1.0") (d (list (d (n "syn") (r "^2.0.60") (d #t) (k 0)))) (h "06iyv976grihxhz1j6gdzhjmc0k9irbvn8nw2lfk98mr7p8n3j44")))

(define-public crate-dynamic-proxy-types-0.2.0 (c (n "dynamic-proxy-types") (v "0.2.0") (d (list (d (n "syn") (r "^2.0.60") (d #t) (k 0)))) (h "0sbsjxpj1s8j2ibb052ghmvrk44iljd48zc3fivj7wg641nr7y3b")))

(define-public crate-dynamic-proxy-types-0.3.0 (c (n "dynamic-proxy-types") (v "0.3.0") (d (list (d (n "syn") (r "^2.0.60") (d #t) (k 0)))) (h "1nv322wghyaikir4rzczacv9y4zk13i95w29qddj2xclgwiswmn6")))

(define-public crate-dynamic-proxy-types-0.4.0 (c (n "dynamic-proxy-types") (v "0.4.0") (d (list (d (n "syn") (r "^2.0.60") (d #t) (k 0)))) (h "0jrl13jdjz52sj6ivz7cyc2sb0fzld36xap05rwpav8fvazv6847")))

(define-public crate-dynamic-proxy-types-0.5.0 (c (n "dynamic-proxy-types") (v "0.5.0") (d (list (d (n "syn") (r "^2.0.60") (d #t) (k 0)))) (h "113q3554j4vhaf4ji5wlp0fvlarazi6b70rs2mpc8wfmrxr2yzl2")))

