(define-module (crates-io dy na dynamodb-helper) #:use-module (crates-io))

(define-public crate-dynamodb-helper-0.1.0 (c (n "dynamodb-helper") (v "0.1.0") (d (list (d (n "aws-sdk-dynamodb") (r "^0.17.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.46") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1s2ijrhd47xkvmbkcrcsqfibxn7nr0dd8df3zrsffvfr1ic9pih3")))

(define-public crate-dynamodb-helper-0.1.1 (c (n "dynamodb-helper") (v "0.1.1") (d (list (d (n "aws-sdk-dynamodb") (r "^0.17.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.46") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1w8g2dpasvyr433sdlk4j3fk7wcgh1dzjd2fgd4mdrmgkab8nngp")))

(define-public crate-dynamodb-helper-0.1.2 (c (n "dynamodb-helper") (v "0.1.2") (d (list (d (n "aws-sdk-dynamodb") (r "^0.17.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.46") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "141fq63hm4v5lcvrqblqdny2cvas1p0hzsdflkb16wxg88inq4vc")))

