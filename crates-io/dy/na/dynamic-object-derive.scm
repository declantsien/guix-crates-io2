(define-module (crates-io dy na dynamic-object-derive) #:use-module (crates-io))

(define-public crate-dynamic-object-derive-0.1.0 (c (n "dynamic-object-derive") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("full" "fold"))) (d #t) (k 0)) (d (n "uuid") (r "^1.1.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "055qqhysq27s2cj3fqnprw7b4404nnyk6gn2zrhr9ik1g5xgs7ci")))

(define-public crate-dynamic-object-derive-0.1.1 (c (n "dynamic-object-derive") (v "0.1.1") (d (list (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("full" "fold"))) (d #t) (k 0)) (d (n "uuid") (r "^1.1.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "0gczvn18zkni421sgn8cqv0w779ydigrs0kfcpj2gn7p58lyd633")))

(define-public crate-dynamic-object-derive-0.1.2 (c (n "dynamic-object-derive") (v "0.1.2") (d (list (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("full" "fold"))) (d #t) (k 0)) (d (n "uuid") (r "^1.1.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "0vq23ig56gy9wj2f31fih8i9nigywymvagfqq0mhjx91s1a8q4iz")))

