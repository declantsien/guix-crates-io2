(define-module (crates-io dy na dynamorio-sys) #:use-module (crates-io))

(define-public crate-dynamorio-sys-8.0.0 (c (n "dynamorio-sys") (v "8.0.0") (d (list (d (n "bindgen") (r "^0.53") (d #t) (k 1)) (d (n "cfg-if") (r "0.*") (d #t) (k 1)) (d (n "regex") (r "^1") (d #t) (k 1)))) (h "12h7xkjxlyb5cnhjxhidgd1fc374jj8i8vp5y70w882xi0vy807c") (f (quote (("x") ("wrap") ("util") ("syms") ("reg") ("option") ("mgr") ("gui") ("default") ("covlib") ("containers") ("bbdup"))))))

