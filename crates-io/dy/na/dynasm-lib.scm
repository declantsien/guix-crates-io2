(define-module (crates-io dy na dynasm-lib) #:use-module (crates-io))

(define-public crate-dynasm-lib-0.1.0-alpha (c (n "dynasm-lib") (v "0.1.0-alpha") (d (list (d (n "bitflags") (r "^1.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "0r4g55am190fdymmfzw1hsk22dklig8x5m4i5zmmdvcmxmsm0smd")))

