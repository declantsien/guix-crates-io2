(define-module (crates-io dy na dynamic_poisson_sampling) #:use-module (crates-io))

(define-public crate-dynamic_poisson_sampling-1.0.0 (c (n "dynamic_poisson_sampling") (v "1.0.0") (d (list (d (n "image") (r "^0.24.7") (d #t) (k 2)) (d (n "imageproc") (r "^0.23.0") (f (quote ("display-window"))) (d #t) (k 2)) (d (n "noise") (r "^0.8.2") (f (quote ("images"))) (d #t) (k 2)) (d (n "plotters") (r "^0.3.5") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1wclsyd8478v2i7v273pmmg0z6fb2h6kdr2p28x04441pbigydh3")))

