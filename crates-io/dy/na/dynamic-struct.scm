(define-module (crates-io dy na dynamic-struct) #:use-module (crates-io))

(define-public crate-dynamic-struct-0.1.0 (c (n "dynamic-struct") (v "0.1.0") (d (list (d (n "bae") (r "^0.1.7") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.49") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (d #t) (k 0)))) (h "0w23z6wz69ig5ynm1bn3m6gakz86517nhckgl77aja8wm1wvz329")))

