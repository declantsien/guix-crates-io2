(define-module (crates-io dy na dynamixel) #:use-module (crates-io))

(define-public crate-dynamixel-0.1.0 (c (n "dynamixel") (v "0.1.0") (d (list (d (n "badlog") (r "^1.1") (d #t) (k 2)) (d (n "bit_field") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serialport") (r "^2.0") (o #t) (d #t) (k 0)) (d (n "serialport") (r "^2.0") (d #t) (k 2)))) (h "1zqjfln1rdy0d3ckyxb4pncj6fjypwdb6d9rq1zjbif36zv9j726") (f (quote (("std" "log/std") ("default"))))))

(define-public crate-dynamixel-0.1.1 (c (n "dynamixel") (v "0.1.1") (d (list (d (n "badlog") (r "^1.1") (d #t) (k 2)) (d (n "bit_field") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serialport") (r "^2.1") (o #t) (d #t) (k 0)) (d (n "serialport") (r "^2.1") (d #t) (k 2)))) (h "0vl5kcywp1fhckn3vvsfgm0q4z8hdaghspspghrij3ba0kb9gspp") (f (quote (("std" "log/std") ("default"))))))

