(define-module (crates-io dy na dynamic_call) #:use-module (crates-io))

(define-public crate-dynamic_call-0.1.0 (c (n "dynamic_call") (v "0.1.0") (d (list (d (n "dynamic_call_proc_macro") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0m7ywwpzjrck1m3k1l10706k1qybqm1bjjjgrbbx0pvkl3i6sv0w")))

(define-public crate-dynamic_call-0.1.1 (c (n "dynamic_call") (v "0.1.1") (d (list (d (n "dynamic_call_proc_macro") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1622k8szv398wcz3khi85cmm9p0vb4fqc4ssrvanm679bcxzwn9f")))

