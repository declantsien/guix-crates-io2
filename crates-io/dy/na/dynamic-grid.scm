(define-module (crates-io dy na dynamic-grid) #:use-module (crates-io))

(define-public crate-dynamic-grid-0.1.0 (c (n "dynamic-grid") (v "0.1.0") (d (list (d (n "assert_matches") (r "^1.5.0") (d #t) (k 2)))) (h "13i20p1dqf2a35ciwvh2vb7ixf6qikax8anlrhgcyhvzffd7lwmm")))

(define-public crate-dynamic-grid-0.2.0 (c (n "dynamic-grid") (v "0.2.0") (d (list (d (n "assert_matches") (r "^1.5.0") (d #t) (k 2)))) (h "1532h23wlm54zwq05ffyf1yx29syr7399wrfb5pkjpx8bjzg54lk")))

