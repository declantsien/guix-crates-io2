(define-module (crates-io dy na dynamic-object) #:use-module (crates-io))

(define-public crate-dynamic-object-0.1.0 (c (n "dynamic-object") (v "0.1.0") (d (list (d (n "dynamic-object-derive") (r "^0.1.0") (d #t) (k 0)))) (h "142vwbh5wzr3h32srxjhp0r7g2sarai03cs2lzkhqifwb5fw8a5k")))

(define-public crate-dynamic-object-0.1.1 (c (n "dynamic-object") (v "0.1.1") (d (list (d (n "dynamic-object-derive") (r "^0.1.1") (d #t) (k 0)))) (h "15h2y0chc7z9mi8c6srk7cacphxjvazh7w6w66vmrmcr0ayhps17")))

(define-public crate-dynamic-object-0.1.2 (c (n "dynamic-object") (v "0.1.2") (d (list (d (n "dynamic-object-derive") (r "^0.1.1") (d #t) (k 0)))) (h "13ikm0nrfzmnq92vaanlbsvn3g7phw8cqig37hdldl26y8ynnqw5")))

