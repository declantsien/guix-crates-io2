(define-module (crates-io dy na dynamic-pool) #:use-module (crates-io))

(define-public crate-dynamic-pool-0.1.0 (c (n "dynamic-pool") (v "0.1.0") (d (list (d (n "crossbeam-queue") (r "^0.2") (d #t) (k 0)))) (h "0c4r67bb0j9b4avcx7j3xb2ryqdsrr8nq6c5mzi15gvcycbhwyfh")))

(define-public crate-dynamic-pool-0.1.1 (c (n "dynamic-pool") (v "0.1.1") (d (list (d (n "crossbeam-queue") (r "^0.2") (d #t) (k 0)))) (h "0sxwdhg9c03g7xwniac12cqjg75bcbigwcn2h9272dbjvr8lqx80")))

(define-public crate-dynamic-pool-0.2.0 (c (n "dynamic-pool") (v "0.2.0") (d (list (d (n "crossbeam-queue") (r "^0.2") (d #t) (k 0)))) (h "050h8xxl0rcax5zb17vhpz2p9nyxigwg7fys7a3kxrw7yha5fxas") (y #t)))

(define-public crate-dynamic-pool-0.2.1 (c (n "dynamic-pool") (v "0.2.1") (d (list (d (n "crossbeam-queue") (r "^0.2") (d #t) (k 0)))) (h "1sm63cvashdnysdp7qqzaf9w4q2hvlhb6jfvy66s8m6hl5wy7pl3") (y #t)))

(define-public crate-dynamic-pool-0.2.2 (c (n "dynamic-pool") (v "0.2.2") (d (list (d (n "crossbeam-queue") (r "^0.2") (d #t) (k 0)))) (h "07r1dl0pidg1bnzl3sn8bq85dyjhwh6v2drz7kqkyp1h9klgnx0l")))

