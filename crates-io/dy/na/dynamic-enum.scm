(define-module (crates-io dy na dynamic-enum) #:use-module (crates-io))

(define-public crate-dynamic-enum-0.0.1 (c (n "dynamic-enum") (v "0.0.1") (h "0ycvzlz4dkzw54qiq33sx826sh7liwq64781mgv1p50kqi27f64w") (y #t)))

(define-public crate-dynamic-enum-0.0.2 (c (n "dynamic-enum") (v "0.0.2") (h "0d5nx05l1b0b64xv7wlff56v19m8pgjbn3f9hj55z6sfpya40cn4") (y #t)))

