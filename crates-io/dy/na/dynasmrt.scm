(define-module (crates-io dy na dynasmrt) #:use-module (crates-io))

(define-public crate-dynasmrt-0.0.1 (c (n "dynasmrt") (v "0.0.1") (d (list (d (n "memmap") (r "0.4.*") (d #t) (k 0)))) (h "1im6gpqlpgns5m2f92r1c1dc5cl93rx94na8060nyk829qvggcl1")))

(define-public crate-dynasmrt-0.0.2 (c (n "dynasmrt") (v "0.0.2") (d (list (d (n "memmap") (r "0.4.*") (d #t) (k 0)))) (h "12xm2mcbpqwsp35wq1kpl7m88aj47dicdsifg5dacnrmgizvp3v0")))

(define-public crate-dynasmrt-0.0.3 (c (n "dynasmrt") (v "0.0.3") (d (list (d (n "memmap") (r "0.4.*") (d #t) (k 0)))) (h "1pi61gbbj53i2f90libgb4x7jzq836big4n2v461bv7dmbwanc56")))

(define-public crate-dynasmrt-0.0.4 (c (n "dynasmrt") (v "0.0.4") (d (list (d (n "memmap") (r "0.4.*") (d #t) (k 0)))) (h "0m67gmkf21pa292jgd8ny5r4hrn55x3nsq8snl7m9krvn3f8j7f2")))

(define-public crate-dynasmrt-0.0.5 (c (n "dynasmrt") (v "0.0.5") (d (list (d (n "memmap") (r "0.4.*") (d #t) (k 0)))) (h "0j03xav531m6gc859a1kcw1g244bf2v0layp937qdcrlfh4dsh81")))

(define-public crate-dynasmrt-0.0.6 (c (n "dynasmrt") (v "0.0.6") (d (list (d (n "memmap") (r "0.4.*") (d #t) (k 0)))) (h "0qpgydcg5qp5w6wsiw8d5g3ln3dc2x47y3la2jiqp4q21i5w0ra4")))

(define-public crate-dynasmrt-0.1.0 (c (n "dynasmrt") (v "0.1.0") (d (list (d (n "memmap") (r "0.4.*") (d #t) (k 0)))) (h "0j4m3l1m6mrcspz5qymysdrvvhw0jg916xrivnvyb32hicl69j8g")))

(define-public crate-dynasmrt-0.1.1 (c (n "dynasmrt") (v "0.1.1") (d (list (d (n "memmap") (r "0.4.*") (d #t) (k 0)))) (h "0jhmpp6kycv63kxqzbvbll2qpzcwz5ic2yb3vrk2k0msadm5ypq9")))

(define-public crate-dynasmrt-0.1.2 (c (n "dynasmrt") (v "0.1.2") (d (list (d (n "memmap") (r "^0.5.2") (d #t) (k 0)))) (h "0p7gd7w8f76rgf8hm6zwl47h12jzqyd48xchh6zfxjh8468jhj3h")))

(define-public crate-dynasmrt-0.1.3 (c (n "dynasmrt") (v "0.1.3") (d (list (d (n "memmap") (r "^0.5.2") (d #t) (k 0)))) (h "0zx8mj7rqyrv087nfhmpnm8hss8z3dnpynbvf5qzi1nvrnmmlwj5")))

(define-public crate-dynasmrt-0.1.4 (c (n "dynasmrt") (v "0.1.4") (d (list (d (n "memmap") (r "^0.5.2") (d #t) (k 0)))) (h "1s22mkvfrf8n5nbxd3m05cdnq6kvqkm0g8d363dk6lk53klvlmn9")))

(define-public crate-dynasmrt-0.2.0 (c (n "dynasmrt") (v "0.2.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "memmap") (r "^0.6.2") (d #t) (k 0)) (d (n "take_mut") (r "^0.2.2") (d #t) (k 0)))) (h "0380ifq3icf7d40r6pvid8759g24zgwwagg44i2pn9zdswrm2wmj")))

(define-public crate-dynasmrt-0.2.1 (c (n "dynasmrt") (v "0.2.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "memmap") (r "^0.6.2") (d #t) (k 0)) (d (n "take_mut") (r "^0.2.2") (d #t) (k 0)))) (h "14izf1cw8rcibw42gzhq7h9czza4qa5lz5hfbyw7vdgjca4pw9bn")))

(define-public crate-dynasmrt-0.2.2 (c (n "dynasmrt") (v "0.2.2") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "memmap") (r "^0.6.2") (d #t) (k 0)) (d (n "take_mut") (r "^0.2.2") (d #t) (k 0)))) (h "0qj70f6bl9yichy4pcqds52jbjbbdqgs9bi5vnk6yblsnfywjc4w")))

(define-public crate-dynasmrt-0.2.3 (c (n "dynasmrt") (v "0.2.3") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "memmap") (r "^0.6.2") (d #t) (k 0)) (d (n "take_mut") (r "^0.2.2") (d #t) (k 0)))) (h "0vijdqrqnwi67a63jsgka0690c9h0w4a035d5dgmh31zkq0wkkyf")))

(define-public crate-dynasmrt-0.3.0 (c (n "dynasmrt") (v "0.3.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "memmap") (r "^0.6.2") (d #t) (k 0)) (d (n "take_mut") (r "^0.2.2") (d #t) (k 0)))) (h "0rpcwf97w7917qgmawyl98d95gj70xyrmss7jlh1dahv5smn6x25")))

(define-public crate-dynasmrt-0.3.1 (c (n "dynasmrt") (v "0.3.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "memmap") (r "^0.6.2") (d #t) (k 0)) (d (n "take_mut") (r "^0.2.2") (d #t) (k 0)))) (h "0s0cigw6nw8wg2xs88bsa6qw6jy1y3gnpr7m54l7dxg726i0ii54")))

(define-public crate-dynasmrt-0.4.0 (c (n "dynasmrt") (v "0.4.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "memmap") (r "^0.7") (d #t) (k 0)) (d (n "take_mut") (r "^0.2") (d #t) (k 0)))) (h "0n6af3p8b6ww0l2ckcndm0806dydgd5kvg6p28haphm5zh151z7g")))

(define-public crate-dynasmrt-0.5.0 (c (n "dynasmrt") (v "0.5.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "memmap") (r "^0.7") (d #t) (k 0)))) (h "0b3n30vymb8bs90kyphrx02a45zpv890g9q71xz90nshwg05dj41")))

(define-public crate-dynasmrt-0.5.1 (c (n "dynasmrt") (v "0.5.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "memmap") (r "^0.7") (d #t) (k 0)))) (h "0clr3c0n6sz5v5p3mnb8428cbxh65n1bjbqgk4w5r21r6446n15h")))

(define-public crate-dynasmrt-0.5.2 (c (n "dynasmrt") (v "0.5.2") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "memmap") (r "^0.7") (d #t) (k 0)))) (h "139jr1d43h6c0da8a8kynwgcr0hzjxan2nvvyjy4h6j4njp3lfca")))

(define-public crate-dynasmrt-0.6.0 (c (n "dynasmrt") (v "0.6.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "memmap") (r "^0.7") (d #t) (k 0)))) (h "1np089x8h09vdylh0di2jpbnylb9ynfphpp30ynr83hlw404ikkx")))

(define-public crate-dynasmrt-0.7.0 (c (n "dynasmrt") (v "0.7.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "dynasm") (r "^0.7.0") (d #t) (k 0)) (d (n "memmap") (r "^0.7") (d #t) (k 0)))) (h "0swg3d0y1rzcag983hj4yh34n63ifs7zhzm6kd27wib0fw8kq3d3")))

(define-public crate-dynasmrt-0.7.1 (c (n "dynasmrt") (v "0.7.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "dynasm") (r "=0.7.1") (d #t) (k 0)) (d (n "memmap") (r "^0.7") (d #t) (k 0)))) (h "028y7vhw98lv89qlafkq6kfkx5ppz54wvm0qrd6xn2fyl075bbmc")))

(define-public crate-dynasmrt-1.0.0 (c (n "dynasmrt") (v "1.0.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "dynasm") (r "=1.0.0") (d #t) (k 0)) (d (n "memmap") (r "^0.7") (d #t) (k 0)))) (h "1w9fpfqhka9ygblyxzlzdvr6341w83yz7irx3ixx6h98mvnw7gl5")))

(define-public crate-dynasmrt-1.0.1 (c (n "dynasmrt") (v "1.0.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "dynasm") (r "=1.0.1") (d #t) (k 0)) (d (n "memmap2") (r "^0.2") (d #t) (k 0)))) (h "1abafbbrvp8dcd4vi6za5nj37cp88dv7m99ryg75h8m1bhflvpf1")))

(define-public crate-dynasmrt-1.1.0 (c (n "dynasmrt") (v "1.1.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "dynasm") (r "=1.1.0") (d #t) (k 0)) (d (n "memmap2") (r "^0.2") (d #t) (k 0)))) (h "12gnjily9ix7a9i9l2y3chkvfgx6cndalmd2rj3kirjz40znw9s2")))

(define-public crate-dynasmrt-1.2.0 (c (n "dynasmrt") (v "1.2.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "dynasm") (r "=1.2.0") (d #t) (k 0)) (d (n "memmap2") (r "^0.5") (d #t) (k 0)))) (h "0nfjyd5fdzflyviqdxdxpzcmqqa81xwiqc3wnj4pir3aw78nj362")))

(define-public crate-dynasmrt-1.2.1 (c (n "dynasmrt") (v "1.2.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "dynasm") (r "=1.2.1") (d #t) (k 0)) (d (n "memmap2") (r "^0.5") (d #t) (k 0)))) (h "0qchn2g010f1a4p54niwqgpd46i0g155zyn1zxnqya5djgy8lhhx")))

(define-public crate-dynasmrt-1.2.2 (c (n "dynasmrt") (v "1.2.2") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "dynasm") (r "=1.2.2") (d #t) (k 0)) (d (n "memmap2") (r "^0.5") (d #t) (k 0)))) (h "10xcy1mxw69ia32scs5awnjc70krz85lgdi0w20sb46ks82k5j2m")))

(define-public crate-dynasmrt-1.2.3 (c (n "dynasmrt") (v "1.2.3") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "dynasm") (r "=1.2.3") (d #t) (k 0)) (d (n "memmap2") (r "^0.5") (d #t) (k 0)))) (h "1aa1av01h0l8ms9fk32ydahby77fd3hhv85zsk51fsnp5fjabyv4")))

(define-public crate-dynasmrt-2.0.0 (c (n "dynasmrt") (v "2.0.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "dynasm") (r "=2.0.0") (d #t) (k 0)) (d (n "fnv") (r "^1.0.3") (d #t) (k 0)) (d (n "memmap2") (r "^0.5") (d #t) (k 0)))) (h "0a3agfr2kgxpwa1lwa0glx6kivcc86yzc57nmsb8j1bqlqqwrp7p")))

