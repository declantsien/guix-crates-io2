(define-module (crates-io dy na dynamic-protobuf) #:use-module (crates-io))

(define-public crate-dynamic-protobuf-0.1.0 (c (n "dynamic-protobuf") (v "0.1.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)))) (h "08fl72fsabylhx0g78a8zckfdpw6s8xn7ix2xyf6mk9sl4847pa2")))

(define-public crate-dynamic-protobuf-0.1.1 (c (n "dynamic-protobuf") (v "0.1.1") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)))) (h "0br4qp6vspaqlzxz7xb1zsxdpsrfx7p5qmg57dbrhvlzmz154ayn")))

