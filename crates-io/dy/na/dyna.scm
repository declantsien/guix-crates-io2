(define-module (crates-io dy na dyna) #:use-module (crates-io))

(define-public crate-dyna-0.1.0 (c (n "dyna") (v "0.1.0") (d (list (d (n "rust-ini") (r "^0.17.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "structopt") (r "^0.3.25") (k 0)))) (h "1f5vn9j13ilfygyz3399cfgnmwwq2hrggikxqdsbklr9zpw5791r")))

