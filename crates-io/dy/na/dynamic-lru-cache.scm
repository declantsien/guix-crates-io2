(define-module (crates-io dy na dynamic-lru-cache) #:use-module (crates-io))

(define-public crate-dynamic-lru-cache-0.1.0 (c (n "dynamic-lru-cache") (v "0.1.0") (d (list (d (n "parking_lot") (r "^0.11.2") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "0q1ys58h7i6f3f59kfq3ph7c5cbl93n38f8x8s10z27vk3inp10s")))

(define-public crate-dynamic-lru-cache-0.2.0 (c (n "dynamic-lru-cache") (v "0.2.0") (d (list (d (n "parking_lot") (r "^0.11.2") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "08kdynmdj51q2zjsif09lyhw8rxdy6gffpma4b8qqm0780p1k8jq")))

(define-public crate-dynamic-lru-cache-0.2.1 (c (n "dynamic-lru-cache") (v "0.2.1") (d (list (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "18ypdqx3qg6ag3zrfv7d7dkpq5dvxcprq9kgr8ciy5ny135pj6r1")))

