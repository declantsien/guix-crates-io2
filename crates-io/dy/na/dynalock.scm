(define-module (crates-io dy na dynalock) #:use-module (crates-io))

(define-public crate-dynalock-0.1.0 (c (n "dynalock") (v "0.1.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "maplit") (r "^1.0") (d #t) (k 0)) (d (n "rusoto_core") (r "^0.32") (o #t) (d #t) (k 0)) (d (n "rusoto_dynamodb") (r "^0.32") (o #t) (d #t) (k 0)) (d (n "rusoto_mock") (r "^0.26") (d #t) (k 2)) (d (n "uuid") (r "^0.6") (f (quote ("v4"))) (o #t) (d #t) (k 0)))) (h "13y7wdqx1kjg5ql7hvm1z5r7wwa72p0k8883vv9nf02vzhlhwh94") (f (quote (("dynamodb" "rusoto_core" "rusoto_dynamodb" "uuid") ("default" "dynamodb"))))))

