(define-module (crates-io dy na dynamic-token) #:use-module (crates-io))

(define-public crate-dynamic-token-0.1.0 (c (n "dynamic-token") (v "0.1.0") (d (list (d (n "base-encode") (r "^0.3.1") (d #t) (k 0)) (d (n "base64") (r "^0.22.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "simple-string-patterns") (r "^0.2.2") (d #t) (k 0)) (d (n "utcnow") (r "^0.2.4") (d #t) (k 0)))) (h "0cav9ap4vlhbig8gpg75gjkw4g1ydzxxb6yvar8s32f7fwhr2s30") (y #t)))

(define-public crate-dynamic-token-0.1.1 (c (n "dynamic-token") (v "0.1.1") (d (list (d (n "base-encode") (r "^0.3.1") (d #t) (k 0)) (d (n "base64") (r "^0.22.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "simple-string-patterns") (r "^0.2.2") (d #t) (k 0)) (d (n "utcnow") (r "^0.2.4") (d #t) (k 0)))) (h "0wps2nsimybv9jik0zqhqvh6ghzz65iqlc6dp3a1jv1kv5qydqkj") (y #t)))

(define-public crate-dynamic-token-0.1.2 (c (n "dynamic-token") (v "0.1.2") (d (list (d (n "base-encode") (r "^0.3.1") (d #t) (k 0)) (d (n "base64") (r "^0.22.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "simple-string-patterns") (r "^0.2.2") (d #t) (k 0)) (d (n "utcnow") (r "^0.2.4") (d #t) (k 0)))) (h "1cma8x93f5d097yvzynis6wjvyvjr8cl25mlyi22r28axm151mcv") (y #t)))

(define-public crate-dynamic-token-0.1.3 (c (n "dynamic-token") (v "0.1.3") (d (list (d (n "base-encode") (r "^0.3.1") (d #t) (k 0)) (d (n "base64") (r "^0.22.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "simple-string-patterns") (r "^0.2.2") (d #t) (k 0)) (d (n "utcnow") (r "^0.2.4") (d #t) (k 0)))) (h "0wn3mvds4ixyyl3awj9y8y8i886mkisjsli9zw50j0q1hfq6nkxz") (y #t)))

(define-public crate-dynamic-token-0.1.4 (c (n "dynamic-token") (v "0.1.4") (d (list (d (n "base-encode") (r "^0.3.1") (d #t) (k 0)) (d (n "base64") (r "^0.22.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "simple-string-patterns") (r "^0.2.2") (d #t) (k 0)) (d (n "utcnow") (r "^0.2.4") (d #t) (k 0)))) (h "17lq5zlkf8sg2hg6kx2fqqhxz3y985i5fc0xhfc6gzpyab4in8rr") (y #t)))

(define-public crate-dynamic-token-0.1.5 (c (n "dynamic-token") (v "0.1.5") (d (list (d (n "base-encode") (r "^0.3.1") (d #t) (k 0)) (d (n "base64") (r "^0.22.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "simple-string-patterns") (r "^0.2.2") (d #t) (k 0)) (d (n "utcnow") (r "^0.2.4") (d #t) (k 0)))) (h "1nx73yqamd4qiynv6i3z45kyfm7xk29arwv32fhqv07mrqqvp053") (y #t)))

(define-public crate-dynamic-token-0.1.6 (c (n "dynamic-token") (v "0.1.6") (d (list (d (n "base-encode") (r "^0.3.1") (d #t) (k 0)) (d (n "base64") (r "^0.22.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "simple-string-patterns") (r "^0.2.2") (d #t) (k 0)) (d (n "utcnow") (r "^0.2.4") (d #t) (k 0)))) (h "1n834c9swy0i9i1qad6y1i15dwljnj3llvwini3rpjyql10q4jzi") (y #t)))

(define-public crate-dynamic-token-0.1.7 (c (n "dynamic-token") (v "0.1.7") (d (list (d (n "base-encode") (r "^0.3.1") (d #t) (k 0)) (d (n "base64") (r "^0.22.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "simple-string-patterns") (r "0.2.*") (d #t) (k 0)) (d (n "utcnow") (r "^0.2.4") (d #t) (k 0)))) (h "1xsnrgy8ra4ci91a031fay0h09gv976p4vx4f052mqkczs2bwx46")))

