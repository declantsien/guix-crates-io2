(define-module (crates-io dy ns dynsequence) #:use-module (crates-io))

(define-public crate-dynsequence-0.1.0-alpha.1 (c (n "dynsequence") (v "0.1.0-alpha.1") (h "1b1rcky7hkp1qi52brilbdlgpw778nydh9ww8vim2clsx38y7y27") (f (quote (("unstable") ("default"))))))

(define-public crate-dynsequence-0.1.0-alpha.2 (c (n "dynsequence") (v "0.1.0-alpha.2") (h "0arqj0v2pkkmz3cf9spzhfhgy8h6005i1pwv9j1n40n3ml4rsaly") (f (quote (("unstable") ("default"))))))

(define-public crate-dynsequence-0.1.0-alpha.3 (c (n "dynsequence") (v "0.1.0-alpha.3") (h "18dxlqgigfijjj820ml6d7kahn5j9minjjg4xgk8k1dfwhc0bp1m") (f (quote (("unstable") ("default"))))))

(define-public crate-dynsequence-0.1.0-alpha.4 (c (n "dynsequence") (v "0.1.0-alpha.4") (h "1b0802w3li7cixqqigwmf585bh7jwddf25h18nnhxm6zhgd9y78p") (f (quote (("unstable") ("default"))))))

