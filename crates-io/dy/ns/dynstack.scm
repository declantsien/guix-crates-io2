(define-module (crates-io dy ns dynstack) #:use-module (crates-io))

(define-public crate-dynstack-0.1.0 (c (n "dynstack") (v "0.1.0") (d (list (d (n "criterion") (r "^0.1.2") (d #t) (k 2)))) (h "1723hls0gg887630idwwpycs4ivz9yjqm61bznjj93kn8ax8s07j")))

(define-public crate-dynstack-0.1.1 (c (n "dynstack") (v "0.1.1") (d (list (d (n "criterion") (r "^0.1.2") (d #t) (k 2)))) (h "073mqaz08h55jafd8l3fl77pkd783rghw63s4hm4sf8pknar3lyr")))

(define-public crate-dynstack-0.2.0 (c (n "dynstack") (v "0.2.0") (d (list (d (n "criterion") (r "^0.1.2") (d #t) (k 2)))) (h "1jbqrik6wmr5xd6xj6gkpx0a1fw531swx4kzyjvidhhg8yq6yxzs")))

(define-public crate-dynstack-0.2.1 (c (n "dynstack") (v "0.2.1") (d (list (d (n "criterion") (r "^0.1.2") (d #t) (k 2)))) (h "0c4f2c322n4dmcqsjh9sk551liidjzq2x9b1lzlggnmn8wfanzmg")))

(define-public crate-dynstack-0.3.0 (c (n "dynstack") (v "0.3.0") (d (list (d (n "criterion") (r "^0.1.2") (d #t) (k 2)))) (h "03rd6lxgkk26a8qiax9z3r06qk9769b1gc0f9xis5ybzh8pvmq7i")))

(define-public crate-dynstack-0.4.0 (c (n "dynstack") (v "0.4.0") (d (list (d (n "criterion") (r "^0.1.2") (d #t) (k 2)))) (h "04kd67hbnd84sz60nsl01gd2h43ypf5k52ivw9p431zsblqg0z1h") (f (quote (("std") ("default" "std"))))))

