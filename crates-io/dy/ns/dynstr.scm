(define-module (crates-io dy ns dynstr) #:use-module (crates-io))

(define-public crate-dynstr-0.1.0 (c (n "dynstr") (v "0.1.0") (h "0qb1kgxzqr2akc5hq04m66s5lvymahp83d5v2qaz41hz3sz3rn5a") (y #t)))

(define-public crate-dynstr-0.1.1 (c (n "dynstr") (v "0.1.1") (h "0zkqsj09iwp0fi1li6lig7a88sbfw1ly4k0jgaqyq8riiywcpj0k")))

