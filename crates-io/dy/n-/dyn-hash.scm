(define-module (crates-io dy n- dyn-hash) #:use-module (crates-io))

(define-public crate-dyn-hash-0.1.0 (c (n "dyn-hash") (v "0.1.0") (h "0r1mpnkwgjawchj5a0gzivav39gyawp769kmwmlbv5dr2y6sx5jf") (r "1.56")))

(define-public crate-dyn-hash-0.1.1 (c (n "dyn-hash") (v "0.1.1") (h "1lna4glyq65j01i5hsq8qgldl9xl238gcgs7la9br6hx5br1vgxs") (r "1.56")))

(define-public crate-dyn-hash-0.1.2 (c (n "dyn-hash") (v "0.1.2") (d (list (d (n "rustversion") (r "^1") (d #t) (k 2)) (d (n "trybuild") (r "^1") (f (quote ("diff"))) (d #t) (k 2)))) (h "10l0dnwzzc37r9pvyjgckfnfgd0lf6rz2y7zl06v4cvzdx04jidm") (r "1.56")))

(define-public crate-dyn-hash-0.2.0 (c (n "dyn-hash") (v "0.2.0") (d (list (d (n "rustversion") (r "^1") (d #t) (k 2)) (d (n "trybuild") (r "^1") (f (quote ("diff"))) (d #t) (k 2)))) (h "0n12z51d7kixywij9z6fzr9rhc3rcpajm6pd0pr1xzx8qrhs8l56") (r "1.56")))

