(define-module (crates-io dy n- dyn-slice) #:use-module (crates-io))

(define-public crate-dyn-slice-1.0.0-alpha.1 (c (n "dyn-slice") (v "1.0.0-alpha.1") (h "0jqcjxgbn17z7c1h0hx3j7srmqls7wg2087qc422kihm9v7gqfpx") (f (quote (("std") ("default" "std") ("alloc"))))))

(define-public crate-dyn-slice-1.0.0-alpha.2 (c (n "dyn-slice") (v "1.0.0-alpha.2") (h "16m8yjiy189fllf0aasr81wpwfdvh9g5s2jhp9n2ff68hyq11pyz") (f (quote (("std") ("default" "std") ("alloc"))))))

(define-public crate-dyn-slice-1.0.0 (c (n "dyn-slice") (v "1.0.0") (h "0nvdcpjmykpb9m5cz4m1dw1l0vh7sjh0j8nfkg3czhwvakl7kdw6") (f (quote (("std") ("default" "std" "alloc") ("alloc"))))))

(define-public crate-dyn-slice-2.0.0 (c (n "dyn-slice") (v "2.0.0") (h "15gx6il3h6z09wryfr8zim9s4pvcwvpywxyv4klcf9g4df9jn0dl") (f (quote (("std") ("default" "std" "alloc") ("alloc"))))))

(define-public crate-dyn-slice-3.0.0-alpha.1 (c (n "dyn-slice") (v "3.0.0-alpha.1") (d (list (d (n "trybuild") (r "^1.0.82") (f (quote ("diff"))) (d #t) (k 2)))) (h "0ar1wg6zvqzr6l1ym0sqvjrkcpyxih8ccvllbh9q0z52rf8rmlwf") (f (quote (("std") ("default" "std" "alloc") ("alloc"))))))

(define-public crate-dyn-slice-3.0.0-alpha.2 (c (n "dyn-slice") (v "3.0.0-alpha.2") (d (list (d (n "trybuild") (r "^1.0.82") (f (quote ("diff"))) (d #t) (k 2)))) (h "1905hq8r9iiyhjss0h133zwrfmcvsh4m1wvfbhx3f3bswfj1iszy") (f (quote (("std") ("default" "std" "alloc") ("alloc"))))))

(define-public crate-dyn-slice-3.0.0-alpha.3 (c (n "dyn-slice") (v "3.0.0-alpha.3") (d (list (d (n "trybuild") (r "^1.0.82") (f (quote ("diff"))) (d #t) (k 2)))) (h "1kys34axxgn35il25l7vx108rzvx2zy8q6ydmx5rqvhgy285d1b3") (f (quote (("std") ("default" "std" "alloc") ("alloc"))))))

(define-public crate-dyn-slice-3.0.0 (c (n "dyn-slice") (v "3.0.0") (d (list (d (n "trybuild") (r "^1.0.82") (f (quote ("diff"))) (d #t) (k 2)))) (h "0g0dlkmwjq8f0wl71gsb9ca7lm6mn2fwj7ry4s6gvnav94sfrya3") (f (quote (("std") ("default" "std" "alloc") ("alloc"))))))

(define-public crate-dyn-slice-3.0.1 (c (n "dyn-slice") (v "3.0.1") (d (list (d (n "trybuild") (r "^1.0.82") (f (quote ("diff"))) (d #t) (k 2)))) (h "0af5y2rfc6a78v1rr05l8w2nnanfvg1skwbc81swjkdr6qv20pp0") (f (quote (("std") ("default" "std" "alloc") ("alloc"))))))

(define-public crate-dyn-slice-3.0.2 (c (n "dyn-slice") (v "3.0.2") (d (list (d (n "trybuild") (r "^1.0.82") (f (quote ("diff"))) (d #t) (k 2)))) (h "0l6hyxw28hcv53jg078xmazf8wziwwc28pqy7vh8nlkd3f80w31n") (f (quote (("std") ("default" "std" "alloc") ("alloc"))))))

(define-public crate-dyn-slice-3.1.0 (c (n "dyn-slice") (v "3.1.0") (d (list (d (n "trybuild") (r "^1.0.82") (f (quote ("diff"))) (d #t) (k 2)))) (h "18d7cbfs9rgj8hvkdjkjf7x6g91l1qzgwa1a9fx1bc2g48lz31l3") (f (quote (("std" "alloc") ("default" "std" "alloc") ("alloc"))))))

(define-public crate-dyn-slice-3.1.1 (c (n "dyn-slice") (v "3.1.1") (d (list (d (n "trybuild") (r "^1.0.82") (f (quote ("diff"))) (d #t) (k 2)))) (h "13yjmardnscybci1wa9wx7h3grw1zpz6rbdklzsicp59gjlwvqwa") (f (quote (("std" "alloc") ("default" "std" "alloc") ("alloc"))))))

(define-public crate-dyn-slice-3.2.0-alpha.1 (c (n "dyn-slice") (v "3.2.0-alpha.1") (d (list (d (n "trybuild") (r "^1.0.82") (f (quote ("diff"))) (d #t) (k 2)))) (h "0s5fivbssarfn4py6ssrhjwag5a9c8pwbyayqjk4cq7n1clsq0la") (f (quote (("std" "alloc") ("default" "std" "alloc") ("alloc"))))))

(define-public crate-dyn-slice-3.2.0-alpha.2 (c (n "dyn-slice") (v "3.2.0-alpha.2") (d (list (d (n "trybuild") (r "^1.0.82") (f (quote ("diff"))) (d #t) (k 2)))) (h "1kgdvfdd3zwwcirvai2804njkl15dkwf4cbmwpbdc6891b2zravd") (f (quote (("std" "alloc") ("default" "std" "alloc") ("alloc"))))))

(define-public crate-dyn-slice-3.2.0-alpha.3 (c (n "dyn-slice") (v "3.2.0-alpha.3") (d (list (d (n "dyn-slice-macros") (r "^3.2.0-alpha.3") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.82") (f (quote ("diff"))) (d #t) (k 2)))) (h "08v61m0ksmnm04lgqw10z7b7s8cq2zpbx906xv4c48hml806lygc") (f (quote (("std" "alloc") ("default" "std" "alloc") ("alloc"))))))

(define-public crate-dyn-slice-3.2.0 (c (n "dyn-slice") (v "3.2.0") (d (list (d (n "dyn-slice-macros") (r "^3.2.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.82") (f (quote ("diff"))) (d #t) (k 2)))) (h "0lc4q9dg0bxqb16rz8nfpcxpziz48v4pblqgv97f4z8xlbwd8fbv") (f (quote (("std" "alloc") ("default" "std" "alloc") ("alloc"))))))

(define-public crate-dyn-slice-3.2.1 (c (n "dyn-slice") (v "3.2.1") (d (list (d (n "dyn-slice-macros") (r "^3.2.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.82") (f (quote ("diff"))) (d #t) (k 2)))) (h "1ijjv830im2dvafca5w5yvj7s1crzpbsi6ycj7cwmpzz8aw35q3q") (f (quote (("std" "alloc") ("default" "std" "alloc") ("alloc"))))))

(define-public crate-dyn-slice-3.2.2 (c (n "dyn-slice") (v "3.2.2") (d (list (d (n "dyn-slice-macros") (r "^3.2.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.82") (f (quote ("diff"))) (d #t) (k 2)))) (h "1gx87g8gd9vd38b2zm2iqjbdg2mh91hi3yjp7x0v5c2kvj08iy94") (f (quote (("std" "alloc") ("default" "std" "alloc") ("alloc"))))))

