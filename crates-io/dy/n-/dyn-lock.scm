(define-module (crates-io dy n- dyn-lock) #:use-module (crates-io))

(define-public crate-dyn-lock-0.1.0 (c (n "dyn-lock") (v "0.1.0") (d (list (d (n "blake2b-rs") (r "^0.2") (o #t) (d #t) (k 1)) (d (n "ckb-std") (r "^0.7") (d #t) (k 0)) (d (n "faster-hex") (r "^0.4") (o #t) (d #t) (k 1)) (d (n "includedir") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "includedir_codegen") (r "^0.6") (o #t) (d #t) (k 1)) (d (n "phf") (r "^0.8") (o #t) (d #t) (k 0)))) (h "1anm0r8yz2d75alb6mwc1js0i2mysw193k92z1a7wqlvmhp0idqy") (f (quote (("include_locks" "includedir" "phf" "includedir_codegen" "blake2b-rs" "faster-hex") ("default"))))))

