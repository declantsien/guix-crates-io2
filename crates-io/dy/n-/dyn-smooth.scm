(define-module (crates-io dy n- dyn-smooth) #:use-module (crates-io))

(define-public crate-dyn-smooth-0.1.0 (c (n "dyn-smooth") (v "0.1.0") (h "02yyhvrkfkq8j80l33kdsdx59pwlchxyn64bm9hag75dg5jnajxx") (r "1.56")))

(define-public crate-dyn-smooth-0.2.0 (c (n "dyn-smooth") (v "0.2.0") (h "001p3wc6h4j11jxi5rwpdw5nfaaixr8fflqdgq1qmqzcaiswiyj7") (r "1.56")))

