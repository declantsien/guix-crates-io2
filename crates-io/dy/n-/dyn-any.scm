(define-module (crates-io dy n- dyn-any) #:use-module (crates-io))

(define-public crate-dyn-any-0.1.0 (c (n "dyn-any") (v "0.1.0") (d (list (d (n "dyn-any-derive") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "13wzjvijfyxdm54n33ril7xjc8zcc18kbckl1n837m1mfcswnjax") (f (quote (("derive" "dyn-any-derive")))) (y #t)))

(define-public crate-dyn-any-0.2.0 (c (n "dyn-any") (v "0.2.0") (d (list (d (n "dyn-any-derive") (r "^0.2.0") (o #t) (d #t) (k 0)))) (h "077gsd8dbpa60c6dnwp48idn0jmlfrgq9l4bwxg63l15vg1xcc2a") (f (quote (("derive" "dyn-any-derive")))) (y #t)))

(define-public crate-dyn-any-0.2.1 (c (n "dyn-any") (v "0.2.1") (d (list (d (n "dyn-any-derive") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "glam") (r "^0.22") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)))) (h "0pj6c3m5r62l444z2yw18lq7pblj6svx43pdf9wapv7x0innyz0k") (f (quote (("std" "alloc" "rc") ("rc") ("log-bad-types" "log") ("large-atomics") ("derive" "dyn-any-derive") ("default" "std" "large-atomics") ("alloc")))) (y #t) (s 2) (e (quote (("glam" "dep:glam")))) (r "1.66.0")))

(define-public crate-dyn-any-0.3.0 (c (n "dyn-any") (v "0.3.0") (d (list (d (n "dyn-any-derive") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "glam") (r "^0.22") (o #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)))) (h "054f2r555js2hx6qxjf1qs7p94qhcp3fagp86n9fljyxj8163ag1") (f (quote (("std" "alloc" "rc" "glam/default") ("rc") ("log-bad-types" "log") ("large-atomics") ("derive" "dyn-any-derive") ("default" "std" "large-atomics") ("alloc")))) (y #t) (s 2) (e (quote (("glam" "dep:glam")))) (r "1.66.0")))

(define-public crate-dyn-any-0.3.1 (c (n "dyn-any") (v "0.3.1") (d (list (d (n "dyn-any-derive") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "glam") (r "^0.22") (o #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)))) (h "15gj2a0ppfwlrkx8rz6dym0rzhfdrl31xxhkgdl3yglbhc3yf2ml") (f (quote (("std" "alloc" "rc" "glam/default") ("rc") ("log-bad-types" "log") ("large-atomics") ("derive" "dyn-any-derive") ("default" "std" "large-atomics") ("alloc")))) (s 2) (e (quote (("glam" "dep:glam")))) (r "1.66.0")))

