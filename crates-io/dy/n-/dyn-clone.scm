(define-module (crates-io dy n- dyn-clone) #:use-module (crates-io))

(define-public crate-dyn-clone-1.0.0 (c (n "dyn-clone") (v "1.0.0") (h "0h89bcrfl93djks6c55a5pwd7kwnw3gq3gjlsi63d0b47w21i98d")))

(define-public crate-dyn-clone-1.0.1 (c (n "dyn-clone") (v "1.0.1") (h "0rh8nww2xmkyzc6f7j7mjjqnljz7pb6333n9a63p1km2p5zrrv5k")))

(define-public crate-dyn-clone-1.0.2 (c (n "dyn-clone") (v "1.0.2") (h "10idzzq2sad7dhrfhrhcx7yckzj8il2bzr16204683ryclxdqlsc")))

(define-public crate-dyn-clone-1.0.3 (c (n "dyn-clone") (v "1.0.3") (h "010qyf229kqkx3nz22cjc5kbgwir311grawfr92jj35jl6prcmym")))

(define-public crate-dyn-clone-1.0.4 (c (n "dyn-clone") (v "1.0.4") (h "1kxb16nxiixn558gn82yaq2fl6271jay4rqkrw31lmnprjpjc9pf")))

(define-public crate-dyn-clone-1.0.5 (c (n "dyn-clone") (v "1.0.5") (d (list (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)))) (h "0a6xdx9h74477dy0zxhp8skhqxnhhy56jfypbqzs9mknvhx0zr91") (r "1.36")))

(define-public crate-dyn-clone-1.0.6 (c (n "dyn-clone") (v "1.0.6") (d (list (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)))) (h "0nhs57spvr1hncqqp2rlipz9qs9h1fywrdf9rzdkxg5jiyvhc0hl") (r "1.36")))

(define-public crate-dyn-clone-1.0.7 (c (n "dyn-clone") (v "1.0.7") (d (list (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)))) (h "0b987fhfbf0rp4jrp79s9jg1s9r6shf9kyw5658h0vyhnbqiflj3") (r "1.36")))

(define-public crate-dyn-clone-1.0.8 (c (n "dyn-clone") (v "1.0.8") (d (list (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)))) (h "1j2r9vyl4mf4d9lfgvpajxrdz91xw0c1k6x5wl0xnagvs61aj1wx") (r "1.45")))

(define-public crate-dyn-clone-1.0.9 (c (n "dyn-clone") (v "1.0.9") (d (list (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)))) (h "1cl8dn2qgvlpmhb22llwbg99yhmz86wbf5747645psmfq84zm52g") (r "1.45")))

(define-public crate-dyn-clone-1.0.10 (c (n "dyn-clone") (v "1.0.10") (d (list (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.66") (f (quote ("diff"))) (d #t) (k 2)))) (h "0q6blny7zl0bsnzx710hyq386f5wywhlf7qm71dc36a5zmg71c69") (r "1.45")))

(define-public crate-dyn-clone-1.0.11 (c (n "dyn-clone") (v "1.0.11") (d (list (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.66") (f (quote ("diff"))) (d #t) (k 2)))) (h "0c7wjwkcsk1sxx98pl4azbn0g1xmdff75fqfs0yf8c0j5w0wzc38") (r "1.45")))

(define-public crate-dyn-clone-1.0.12 (c (n "dyn-clone") (v "1.0.12") (d (list (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.66") (f (quote ("diff"))) (d #t) (k 2)))) (h "0wi2s4fx929wzzz889vdbdiwm9qaz48brax9jwg0k4x5xw46akih") (r "1.45")))

(define-public crate-dyn-clone-1.0.13 (c (n "dyn-clone") (v "1.0.13") (d (list (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.66") (f (quote ("diff"))) (d #t) (k 2)))) (h "0mf5av01hi771dqdhssri1fikbxic0pj8m8fvjda1wmqq524gz5v") (r "1.45")))

(define-public crate-dyn-clone-1.0.14 (c (n "dyn-clone") (v "1.0.14") (d (list (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.66") (f (quote ("diff"))) (d #t) (k 2)))) (h "1kgv9cjfdfnwjxk391w08ylkrmv90pqvvdg4dmk3smwsgm0g7li3") (r "1.45")))

(define-public crate-dyn-clone-1.0.15 (c (n "dyn-clone") (v "1.0.15") (d (list (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.66") (f (quote ("diff"))) (d #t) (k 2)))) (h "0f47z79dammnnhazfngv2mvz6m2zj3j99y0q9qs4fds63ns0zll0") (r "1.45")))

(define-public crate-dyn-clone-1.0.16 (c (n "dyn-clone") (v "1.0.16") (d (list (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.66") (f (quote ("diff"))) (d #t) (k 2)))) (h "0pa9kas6a241pbx0q82ipwi4f7m7wwyzkkc725caky24gl4j4nsl") (r "1.45")))

(define-public crate-dyn-clone-1.0.17 (c (n "dyn-clone") (v "1.0.17") (d (list (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.66") (f (quote ("diff"))) (d #t) (k 2)))) (h "09cig7dgg6jnqa10p4233nd8wllbjf4ffsw7wj0m4lwa5w3z0vhd") (r "1.45")))

