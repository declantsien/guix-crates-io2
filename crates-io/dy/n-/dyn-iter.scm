(define-module (crates-io dy n- dyn-iter) #:use-module (crates-io))

(define-public crate-dyn-iter-0.1.0 (c (n "dyn-iter") (v "0.1.0") (h "14izcknnq6avapv112pbz8r4w0lshx6pw5ss2gskx0n6yyypza8r")))

(define-public crate-dyn-iter-0.2.0 (c (n "dyn-iter") (v "0.2.0") (h "0rpadnzvi9fwx4iyzpvd5crncri6zqs0grxy669v04yf7dljc5qk")))

(define-public crate-dyn-iter-1.0.0 (c (n "dyn-iter") (v "1.0.0") (h "16gv1a5cjvg7f05pssk6krdmcch9lcknb78s1gya5cps23k702ks")))

(define-public crate-dyn-iter-1.0.1 (c (n "dyn-iter") (v "1.0.1") (h "109b69c2a57zhzsipkcb8gjjjbs8xi1xdb3b86r0vr5jz7v876g4")))

