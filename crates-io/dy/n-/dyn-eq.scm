(define-module (crates-io dy n- dyn-eq) #:use-module (crates-io))

(define-public crate-dyn-eq-0.1.0 (c (n "dyn-eq") (v "0.1.0") (h "1whx1rxrhfwij6f6c3i24prp44gx1jry7agk8hjlkcrcjbwyqwzm")))

(define-public crate-dyn-eq-0.1.1 (c (n "dyn-eq") (v "0.1.1") (h "1566pyib4my7qj1iv11xb56mh7z45fi7y5xa7gx7377bjnxdnjjg") (f (quote (("default" "alloc") ("alloc")))) (y #t)))

(define-public crate-dyn-eq-0.1.2 (c (n "dyn-eq") (v "0.1.2") (h "0n5cz1rl2scjg07pqczbzmcrq97zx3sgbpn022qjiy30nlqbhp99") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-dyn-eq-0.1.3 (c (n "dyn-eq") (v "0.1.3") (h "122kglyl6y0ijc35sbhlkw554qzrbd548yswdwddwp5g45fh6baw") (f (quote (("default" "alloc") ("alloc"))))))

