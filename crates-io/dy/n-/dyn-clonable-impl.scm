(define-module (crates-io dy n- dyn-clonable-impl) #:use-module (crates-io))

(define-public crate-dyn-clonable-impl-0.9.0 (c (n "dyn-clonable-impl") (v "0.9.0") (d (list (d (n "dyn-clone") (r "^1.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1icrjdqiriiy6abxpsygyxylgxg2gq5j9z876pslqdrwazm413jm")))

