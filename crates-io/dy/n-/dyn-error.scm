(define-module (crates-io dy n- dyn-error) #:use-module (crates-io))

(define-public crate-dyn-error-0.1.0 (c (n "dyn-error") (v "0.1.0") (d (list (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "speculate2") (r "^0.2") (d #t) (k 2)))) (h "0k2ahv72viwzfxcgvpk678kmbijkq46s8b72jgf56qp99hjvzi54")))

(define-public crate-dyn-error-0.1.1 (c (n "dyn-error") (v "0.1.1") (d (list (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "speculate2") (r "^0.2") (d #t) (k 2)))) (h "1zzgc3gc802irpi5pcfrx4m8ry72j0xkd543j9l8s3a0ds1vs4r2")))

(define-public crate-dyn-error-0.2.0 (c (n "dyn-error") (v "0.2.0") (h "1l5cib8701pdbjcsikqmvmbkprwrz8pj1qchnqf8gvavca1yplkx")))

