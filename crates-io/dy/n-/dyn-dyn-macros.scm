(define-module (crates-io dy n- dyn-dyn-macros) #:use-module (crates-io))

(define-public crate-dyn-dyn-macros-0.1.0-alpha.1 (c (n "dyn-dyn-macros") (v "0.1.0-alpha.1") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0z86vymys3ypv9ihlin0vj3myw14c62kfad0m4zc01jnczg9qaxh")))

(define-public crate-dyn-dyn-macros-0.1.0-alpha.2 (c (n "dyn-dyn-macros") (v "0.1.0-alpha.2") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0g9vhxpzv4vf112v08nxd0mji86j40g30f5wq4ldr16cj8s8jp80")))

(define-public crate-dyn-dyn-macros-0.1.0 (c (n "dyn-dyn-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1nk7r1npbil8fbrqkks5x9ba3yz1hi705vawcg74gr7ak4320lbz")))

(define-public crate-dyn-dyn-macros-0.1.1 (c (n "dyn-dyn-macros") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1f5w5ih43j5hjkgiby2ybqi19w3z5yw16d5wh6rmf0ni5148h0fj")))

(define-public crate-dyn-dyn-macros-0.1.2 (c (n "dyn-dyn-macros") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1nqy6rsvpczkdkn7x1dkxm7jr8blj3rzd5p71lxqffnqhp3cmpch")))

