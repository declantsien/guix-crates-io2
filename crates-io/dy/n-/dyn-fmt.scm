(define-module (crates-io dy n- dyn-fmt) #:use-module (crates-io))

(define-public crate-dyn-fmt-0.1.0 (c (n "dyn-fmt") (v "0.1.0") (h "11c2n7iync8dw5idvngplaq7ms2p5pdrjf8iqfib0dg57wv6r9d9")))

(define-public crate-dyn-fmt-0.1.1 (c (n "dyn-fmt") (v "0.1.1") (h "129kj7h1i5n3d0bkb830r7xmzfidh8wfxwb1wyp7g5v86qs0jjhf")))

(define-public crate-dyn-fmt-0.1.2 (c (n "dyn-fmt") (v "0.1.2") (h "1bdch3z5lsikr0cipz9x5hnj58y7s7xdvpwifyw0pj72b4y5872g")))

(define-public crate-dyn-fmt-0.2.0 (c (n "dyn-fmt") (v "0.2.0") (h "1fgz4ckldxik2rq7qribpzf9csx7y192hq8l4d2664bmf1qsyy3i") (f (quote (("std") ("default" "std"))))))

(define-public crate-dyn-fmt-0.2.1 (c (n "dyn-fmt") (v "0.2.1") (h "1gd4wkjizgifcmmdygn1l4xcb57g73f99garv9cbsxydyxk72f6f") (f (quote (("std") ("default" "std"))))))

(define-public crate-dyn-fmt-0.2.2 (c (n "dyn-fmt") (v "0.2.2") (h "11lb01nkh5wf8lpdqwqpdalnalwwpx5hr9in46rc3j6h04dxqmhf") (f (quote (("std") ("default" "std"))))))

(define-public crate-dyn-fmt-0.2.3 (c (n "dyn-fmt") (v "0.2.3") (h "1d6pg007jbmk6c77f8kpf4d125kp9lpxvb5bfllfzi0ladp6xg3m") (f (quote (("std") ("default" "std"))))))

(define-public crate-dyn-fmt-0.2.4 (c (n "dyn-fmt") (v "0.2.4") (h "0han7cbnfjsyf15zbs254552j8l9r4ghjgwlr83iyx8dlli7ikny") (f (quote (("std") ("default" "std")))) (y #t)))

(define-public crate-dyn-fmt-0.2.5 (c (n "dyn-fmt") (v "0.2.5") (h "1mp9cjsph4al20zlxizlj1b83sj02x76hm41y2fl36n9za5260w4") (f (quote (("std") ("default" "std"))))))

(define-public crate-dyn-fmt-0.3.0 (c (n "dyn-fmt") (v "0.3.0") (h "1231qhrlsvdcpj4l76gg2gfrdpzfn72zy3jvlly9sfnpkdn87862") (f (quote (("std") ("default" "std"))))))

(define-public crate-dyn-fmt-0.4.0 (c (n "dyn-fmt") (v "0.4.0") (h "16i0jpfgv94ac1i84k6cjjml6cj02sj82yvfbmck5j5bjbq39vvr") (f (quote (("std") ("default" "std")))) (r "1.71")))

