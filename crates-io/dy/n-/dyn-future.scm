(define-module (crates-io dy n- dyn-future) #:use-module (crates-io))

(define-public crate-dyn-future-1.0.0 (c (n "dyn-future") (v "1.0.0") (h "1i0wfwmpzzbbmiwac72v5k7gskgmamc030x6pay9rpbfiwprxcvb")))

(define-public crate-dyn-future-1.0.1 (c (n "dyn-future") (v "1.0.1") (h "01pqpp6pdajrggw5x9088br5rjwwxbbb3dylaclkzi0zsgx0khp8")))

(define-public crate-dyn-future-2.0.0 (c (n "dyn-future") (v "2.0.0") (d (list (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "futures") (r "^0.3.1") (d #t) (k 2)) (d (n "once_cell") (r "^1.2.0") (d #t) (k 0)) (d (n "rayon") (r "^1.2.1") (d #t) (k 2)) (d (n "spin") (r "^0.5.2") (d #t) (k 0)))) (h "1ykpjsbcq6lfmnl2qfibci5n7r450n4imq796g0nsnnh7ry946pl")))

(define-public crate-dyn-future-2.0.1 (c (n "dyn-future") (v "2.0.1") (d (list (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "futures") (r "^0.3.1") (d #t) (k 2)) (d (n "once_cell") (r "^1.2.0") (d #t) (k 0)) (d (n "rayon") (r "^1.2.1") (d #t) (k 2)) (d (n "spin") (r "^0.5.2") (d #t) (k 0)))) (h "0dl1y9dn73llcj99dxf54ijncqw140rgcy3r8p6ka6q97v7s30rh")))

(define-public crate-dyn-future-2.0.2 (c (n "dyn-future") (v "2.0.2") (d (list (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "futures") (r "^0.3.1") (d #t) (k 2)) (d (n "once_cell") (r "^1.2.0") (d #t) (k 0)) (d (n "rayon") (r "^1.2.1") (d #t) (k 2)) (d (n "spin") (r "^0.5.2") (d #t) (k 0)))) (h "0g4vik4vh1k8h4ch42gxk440j95wpx25im9q9x6903is34iafjk9")))

(define-public crate-dyn-future-2.1.0 (c (n "dyn-future") (v "2.1.0") (d (list (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "futures") (r "^0.3.1") (d #t) (k 2)) (d (n "once_cell") (r "^1.2.0") (d #t) (k 0)) (d (n "rayon") (r "^1.2.1") (d #t) (k 2)) (d (n "spin") (r "^0.5.2") (d #t) (k 0)))) (h "1qx7l6qf24pr7k6ymwydjl30fqmxdiwg80cdjmi40bnfgdx28ng3")))

(define-public crate-dyn-future-2.1.1 (c (n "dyn-future") (v "2.1.1") (d (list (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "futures") (r "^0.3.1") (d #t) (k 2)) (d (n "once_cell") (r "^1.2.0") (d #t) (k 0)) (d (n "rayon") (r "^1.2.1") (d #t) (k 2)) (d (n "spin") (r "^0.5.2") (d #t) (k 0)))) (h "0cg44mf8sb836jzzk328id5clyw728p58hmhr90bsy58f8223wgi")))

(define-public crate-dyn-future-2.1.2 (c (n "dyn-future") (v "2.1.2") (d (list (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "futures") (r "^0.3.1") (d #t) (k 2)) (d (n "once_cell") (r "^1.2.0") (d #t) (k 0)) (d (n "rayon") (r "^1.2.1") (d #t) (k 2)) (d (n "spin") (r "^0.5.2") (d #t) (k 0)))) (h "1423wvqmxx12cq7l0ga7znih6zbzd2l1acvsf7sw91zg7n0g0z0j")))

(define-public crate-dyn-future-2.1.3 (c (n "dyn-future") (v "2.1.3") (d (list (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "futures") (r "^0.3.1") (d #t) (k 2)) (d (n "once_cell") (r "^1.2.0") (d #t) (k 0)) (d (n "rayon") (r "^1.2.1") (d #t) (k 2)) (d (n "spin") (r "^0.5.2") (d #t) (k 0)))) (h "03rby3mxscc3nicr3zlvcj1cpzxzpzj7ww4yhb1x4p8jrhwmjyhz")))

(define-public crate-dyn-future-3.0.0 (c (n "dyn-future") (v "3.0.0") (d (list (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "futures") (r "^0.3.1") (d #t) (k 2)) (d (n "once_cell") (r "^1.2.0") (d #t) (k 0)) (d (n "rayon") (r "^1.2.1") (d #t) (k 2)) (d (n "spin") (r "^0.5.2") (d #t) (k 0)))) (h "1hf4c70w2ih5a6x4rj1q9py2ys5vn3khs97969z4ik7f60qhy411")))

(define-public crate-dyn-future-3.0.1 (c (n "dyn-future") (v "3.0.1") (d (list (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "futures") (r "^0.3.1") (d #t) (k 2)) (d (n "once_cell") (r "^1.2.0") (d #t) (k 0)) (d (n "rayon") (r "^1.2.1") (d #t) (k 2)) (d (n "spin") (r "^0.5.2") (d #t) (k 0)))) (h "097s9vavw1wc8wkhzb0yvx8qfbyk941i9ggqw3izrq28p1vsfa4d")))

(define-public crate-dyn-future-3.0.2 (c (n "dyn-future") (v "3.0.2") (d (list (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "futures") (r "^0.3.1") (d #t) (k 2)) (d (n "once_cell") (r "^1.2.0") (d #t) (k 0)) (d (n "rayon") (r "^1.2.1") (d #t) (k 2)) (d (n "spin") (r "^0.5.2") (d #t) (k 0)))) (h "0msd7xy24yqm4kjz2mal8vrmfhxyswjzpw9akb3zkqnfv3hhr49m")))

(define-public crate-dyn-future-3.0.3 (c (n "dyn-future") (v "3.0.3") (d (list (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "futures") (r "^0.3.1") (d #t) (k 2)) (d (n "once_cell") (r "^1.2.0") (d #t) (k 0)) (d (n "rayon") (r "^1.2.1") (d #t) (k 2)) (d (n "spin") (r "^0.5.2") (d #t) (k 0)))) (h "0fy8ap9r2l5bg0znal1xn73wb013150q931j17sfx2g34k1wsr2r")))

(define-public crate-dyn-future-3.0.4 (c (n "dyn-future") (v "3.0.4") (d (list (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "futures") (r "^0.3.1") (d #t) (k 2)) (d (n "once_cell") (r "^1.2.0") (d #t) (k 0)) (d (n "rayon") (r "^1.2.1") (d #t) (k 2)) (d (n "spin") (r "^0.5.2") (d #t) (k 0)))) (h "0912lbm6dskrwwr0qjxikqfx2p9qnsimv2sc05gkjq9qn7y9472y")))

