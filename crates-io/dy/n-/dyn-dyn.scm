(define-module (crates-io dy n- dyn-dyn) #:use-module (crates-io))

(define-public crate-dyn-dyn-0.1.0-alpha.1 (c (n "dyn-dyn") (v "0.1.0-alpha.1") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "dyn-dyn-macros") (r "^0.1.0-alpha.1") (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.2.0") (k 0)))) (h "1gij0fz31a6wd3awlnxkyzyali7ml67n4i1nga650qhpqf9nn1mw") (f (quote (("std" "alloc" "stable_deref_trait/std") ("dynamic-names") ("default" "std") ("alloc" "stable_deref_trait/alloc"))))))

(define-public crate-dyn-dyn-0.1.0-alpha.2 (c (n "dyn-dyn") (v "0.1.0-alpha.2") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "dyn-dyn-macros") (r "^0.1.0-alpha.2") (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.2.0") (k 0)))) (h "17wivmc2092klsfv0s2wk1yjnicvgdgmp3zhz9vvg71akqksdd6l") (f (quote (("std" "alloc" "stable_deref_trait/std") ("dynamic-names") ("default" "std") ("alloc" "stable_deref_trait/alloc"))))))

(define-public crate-dyn-dyn-0.1.0 (c (n "dyn-dyn") (v "0.1.0") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "dyn-dyn-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.2.0") (k 0)))) (h "06cqbh9bhb47f50v4ij8dgrs5g8wmp01jk1c7ir4cwddr3snl8rl") (f (quote (("std" "alloc" "stable_deref_trait/std") ("dynamic-names") ("default" "std") ("alloc" "stable_deref_trait/alloc"))))))

(define-public crate-dyn-dyn-0.1.1 (c (n "dyn-dyn") (v "0.1.1") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "dyn-dyn-macros") (r "^0.1.1") (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.2.0") (k 0)))) (h "1768xyxkhnlm1x97j5aks8mgyk1mfzmqcya06n6ddy25sbni579b") (f (quote (("std" "alloc" "stable_deref_trait/std") ("dynamic-names") ("default" "std") ("alloc" "stable_deref_trait/alloc"))))))

(define-public crate-dyn-dyn-0.1.2 (c (n "dyn-dyn") (v "0.1.2") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "dyn-dyn-macros") (r "^0.1.2") (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.2.0") (k 0)))) (h "03d0rd3awfz080lx81mbijwmsxbw3x0jvqzszdkhhiblq795jldk") (f (quote (("std" "alloc" "stable_deref_trait/std") ("dynamic-names") ("default" "std") ("alloc" "stable_deref_trait/alloc"))))))

