(define-module (crates-io dy n- dyn-stack) #:use-module (crates-io))

(define-public crate-dyn-stack-0.1.0 (c (n "dyn-stack") (v "0.1.0") (d (list (d (n "reborrow") (r "^0.2.0") (d #t) (k 0)))) (h "1g585kns4h6hs43k2as1wdqav347nfpa7xlh3zpzvnmm2h8a2psk") (f (quote (("std") ("default" "std"))))))

(define-public crate-dyn-stack-0.1.1 (c (n "dyn-stack") (v "0.1.1") (d (list (d (n "reborrow") (r "^0.2.0") (d #t) (k 0)))) (h "0wl33z752vhbr2di8k7wxh6pzl6hhv93yiw27ik6754l2530lg2g") (f (quote (("std") ("default" "std"))))))

(define-public crate-dyn-stack-0.1.2 (c (n "dyn-stack") (v "0.1.2") (d (list (d (n "reborrow") (r "^0.2.0") (d #t) (k 0)))) (h "0is274vpi49d9zk8fdzknl1f4i4asvngj8fdmx4pr0n28f7719s8") (f (quote (("std") ("default" "std"))))))

(define-public crate-dyn-stack-0.1.3 (c (n "dyn-stack") (v "0.1.3") (d (list (d (n "reborrow") (r "^0.2.0") (d #t) (k 0)))) (h "1ayrmr18riiygh3ly6qxgil6wswcavk1nca767582plbym57s84w") (f (quote (("std") ("default" "std"))))))

(define-public crate-dyn-stack-0.1.4 (c (n "dyn-stack") (v "0.1.4") (d (list (d (n "reborrow") (r "^0.2.0") (d #t) (k 0)))) (h "07fjmgqz17gdjzinhqd90mbywbb2zghn41vknmasz9ci1ymys30a") (f (quote (("std") ("nightly") ("default" "std")))) (y #t)))

(define-public crate-dyn-stack-0.2.0 (c (n "dyn-stack") (v "0.2.0") (d (list (d (n "reborrow") (r "^0.2.0") (d #t) (k 0)))) (h "06l92nbsa9n0jznm4lgpd5acn3ld5bh13pzgy3aiqlij476jna9r") (f (quote (("std") ("nightly") ("default" "std"))))))

(define-public crate-dyn-stack-0.2.1 (c (n "dyn-stack") (v "0.2.1") (d (list (d (n "reborrow") (r "^0.2.0") (d #t) (k 0)))) (h "0fdxpx08y8s8qj97j9lgjf3hg5y0qf3qji8zb2y71fzgzh9b4s9s") (f (quote (("std") ("nightly") ("default" "std"))))))

(define-public crate-dyn-stack-0.2.2 (c (n "dyn-stack") (v "0.2.2") (d (list (d (n "reborrow") (r "^0.2.0") (d #t) (k 0)))) (h "00im7ii14h5jqrmxr8nm0rsz2p5wnh15i12hb6f5ixinix7jzhfm") (f (quote (("std") ("nightly") ("default" "std"))))))

(define-public crate-dyn-stack-0.3.0 (c (n "dyn-stack") (v "0.3.0") (d (list (d (n "reborrow") (r "^0.2") (d #t) (k 0)))) (h "0d28w5hqz0zbyfn7idgsdaqhx7rf3817c35db2nymb7z4m1jqb2f") (f (quote (("std") ("nightly") ("default" "std"))))))

(define-public crate-dyn-stack-0.3.1 (c (n "dyn-stack") (v "0.3.1") (d (list (d (n "reborrow") (r "^0.2") (d #t) (k 0)))) (h "1xhl6ply0bnhraqqigkx8xkpnmpd1d8w4fpqlbkf4biia3vmpii9") (f (quote (("std") ("nightly") ("default" "std"))))))

(define-public crate-dyn-stack-0.3.2 (c (n "dyn-stack") (v "0.3.2") (d (list (d (n "reborrow") (r "^0.2") (d #t) (k 0)))) (h "1wnjxnhv0dhp8ddhf1wh1vy7z6751lxmwfkc4i2f2wlggbwlkvnm") (f (quote (("std") ("nightly") ("default" "std"))))))

(define-public crate-dyn-stack-0.4.0 (c (n "dyn-stack") (v "0.4.0") (d (list (d (n "reborrow") (r "^0.2") (d #t) (k 0)))) (h "1x5p3yw374jdrm6lsff2mws7yxb8xlmirm1zab87za4bk2krgwzn") (f (quote (("std") ("nightly") ("default" "std"))))))

(define-public crate-dyn-stack-0.5.0 (c (n "dyn-stack") (v "0.5.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "reborrow") (r "^0.2") (d #t) (k 0)))) (h "07q32pgy9sw56hkyikih83dshbp7bxklpca7m9qnlg80c1rqvq9y") (f (quote (("std") ("nightly") ("default" "std"))))))

(define-public crate-dyn-stack-0.5.1 (c (n "dyn-stack") (v "0.5.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "reborrow") (r "^0.2") (d #t) (k 0)))) (h "1z331yqllgj0r4ygis8g0rca5cvrf9i9ggi5yppk5lglwmykz0d5") (f (quote (("std") ("nightly") ("default" "std"))))))

(define-public crate-dyn-stack-0.5.2 (c (n "dyn-stack") (v "0.5.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "reborrow") (r "^0.3") (d #t) (k 0)))) (h "1m7cfc3d7pr7w9rvawzdxn1nq9f5x39dasrg7sr1r4zxgrx75c2b") (f (quote (("std") ("nightly") ("default" "std"))))))

(define-public crate-dyn-stack-0.6.0 (c (n "dyn-stack") (v "0.6.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "reborrow") (r "^0.3") (d #t) (k 0)))) (h "0qmn6i3x4c32pdmq5g9q10al1bj93pzcramlxganzrlkfm9r4svc") (f (quote (("std") ("nightly") ("default" "std"))))))

(define-public crate-dyn-stack-0.6.1 (c (n "dyn-stack") (v "0.6.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "reborrow") (r "^0.4") (d #t) (k 0)))) (h "1ipmv57dma78q208qxx6fq1g8jyk5rq0fxgmgfdfdr161pcxn8bb") (f (quote (("std") ("nightly") ("default" "std"))))))

(define-public crate-dyn-stack-0.7.0 (c (n "dyn-stack") (v "0.7.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "reborrow") (r "^0.5") (d #t) (k 0)))) (h "1zqa0ipn9318lianvhav693ndq518wp6l7nvij58ky0z4hi8riib") (f (quote (("std") ("nightly") ("default" "std"))))))

(define-public crate-dyn-stack-0.8.0 (c (n "dyn-stack") (v "0.8.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "reborrow") (r "^0.5") (d #t) (k 0)))) (h "0gax31faihr7x1s9zzqaih3x8if55m0nzwvqhkxihyjz41c0k8yw") (f (quote (("std") ("nightly") ("default" "std"))))))

(define-public crate-dyn-stack-0.8.1 (c (n "dyn-stack") (v "0.8.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "reborrow") (r "^0.5") (d #t) (k 0)))) (h "1f7jncvj12w29lfk0y6yg2bi4yxpszsqkfrbyyahnhhg35agbk17") (f (quote (("std") ("nightly") ("default" "std"))))))

(define-public crate-dyn-stack-0.8.2 (c (n "dyn-stack") (v "0.8.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "reborrow") (r "^0.5") (d #t) (k 0)))) (h "0jc4nnir0r8dn79mdcahk45m172r6w0kii9lsjv1icagfww0ag3a") (f (quote (("std") ("nightly") ("default" "std"))))))

(define-public crate-dyn-stack-0.8.3 (c (n "dyn-stack") (v "0.8.3") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "reborrow") (r "^0.5") (d #t) (k 0)))) (h "1jl8nxcg3z7vzyvidkfkw4qk8vbscz2hg68lr3rnl0ppkwcw6qrc") (f (quote (("std") ("nightly") ("default" "std"))))))

(define-public crate-dyn-stack-0.9.0 (c (n "dyn-stack") (v "0.9.0") (d (list (d (n "bytemuck") (r "^1") (d #t) (k 0)) (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "reborrow") (r "^0.5") (d #t) (k 0)))) (h "1g4psiznsh6d218wxff061djvaxrc0dfy8h62c9bqxf1qwwrf9i4") (f (quote (("std") ("nightly") ("default" "std"))))))

(define-public crate-dyn-stack-0.9.1 (c (n "dyn-stack") (v "0.9.1") (d (list (d (n "bytemuck") (r "^1") (d #t) (k 0)) (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "reborrow") (r "^0.5") (d #t) (k 0)))) (h "0vi5gbgipzp0k8q6knp03n9y6pc0fk7q4ys37hfkh8y5pkbzirvz") (f (quote (("std") ("nightly") ("default" "std"))))))

(define-public crate-dyn-stack-0.10.0 (c (n "dyn-stack") (v "0.10.0") (d (list (d (n "bytemuck") (r "^1") (d #t) (k 0)) (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "reborrow") (r "^0.5") (d #t) (k 0)))) (h "12vz0bkj1z36w0dh3dsxwjgxn1fxi1s3iyzqckrk4mlgd2ckgran") (f (quote (("std") ("nightly") ("default" "std"))))))

