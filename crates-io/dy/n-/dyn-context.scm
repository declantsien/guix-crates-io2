(define-module (crates-io dy n- dyn-context) #:use-module (crates-io))

(define-public crate-dyn-context-0.0.1 (c (n "dyn-context") (v "0.0.1") (h "0mfa2bxr40z5xlv5aykhwm4w4194nig24ic7w7qgcwy7rsihj15k") (f (quote (("nightly") ("default" "nightly"))))))

(define-public crate-dyn-context-0.0.2 (c (n "dyn-context") (v "0.0.2") (d (list (d (n "paste") (r "^1.0.0") (d #t) (k 0)))) (h "15b87f61qgfm8lmifw0x8qzmw718bnckj4991qjjnqrfvbv0226f") (f (quote (("nightly") ("default" "nightly"))))))

(define-public crate-dyn-context-0.0.3 (c (n "dyn-context") (v "0.0.3") (d (list (d (n "paste") (r "^1.0.0") (d #t) (k 0)))) (h "09s3d651q51isd6gr4nr4mh4rls1vawidsw35blyf2pfcz087kpl") (f (quote (("nightly") ("default" "nightly"))))))

(define-public crate-dyn-context-0.0.4 (c (n "dyn-context") (v "0.0.4") (d (list (d (n "paste") (r "^1.0.0") (d #t) (k 0)))) (h "0ll1ydn5qvzvfrry0ww2pdxpv3ik5s767qapvv9aphd4848876px") (f (quote (("nightly") ("default" "nightly"))))))

(define-public crate-dyn-context-0.0.5 (c (n "dyn-context") (v "0.0.5") (d (list (d (n "paste") (r "^1.0.0") (d #t) (k 0)))) (h "1r1807y8hydyzp47bn4jmkkbgn8zk59swhcwg4d3w7ajif96x54m") (f (quote (("nightly") ("default" "nightly"))))))

(define-public crate-dyn-context-0.0.6 (c (n "dyn-context") (v "0.0.6") (d (list (d (n "paste") (r "^1.0.0") (d #t) (k 0)))) (h "0m01cnq390wnyhbv9xj8wgm23zbwsc3jblwwr5vhvpw8ydimfzkv") (f (quote (("nightly") ("default" "nightly"))))))

(define-public crate-dyn-context-0.0.7 (c (n "dyn-context") (v "0.0.7") (d (list (d (n "paste") (r "^1.0.0") (d #t) (k 0)))) (h "0k2q2wv2v2wmcbn655sxfl5whkhs1cvrfqa4js8yvfmsf2d4cxcj") (f (quote (("nightly") ("default" "nightly"))))))

(define-public crate-dyn-context-0.0.8 (c (n "dyn-context") (v "0.0.8") (d (list (d (n "paste") (r "^1.0.0") (d #t) (k 0)))) (h "1jzpc2n53jcdfs07b27bgw0xpypavkm0msdvm92wmslnybzwnp9p") (f (quote (("nightly") ("default" "nightly"))))))

(define-public crate-dyn-context-0.1.0 (c (n "dyn-context") (v "0.1.0") (d (list (d (n "paste") (r "^1.0.0") (d #t) (k 0)))) (h "0qsy4j4qyx50v2bcqj3ih8ydxks9mvkrwnb8lays0360w6z11wdd") (f (quote (("nightly") ("default" "nightly"))))))

(define-public crate-dyn-context-0.2.0 (c (n "dyn-context") (v "0.2.0") (d (list (d (n "paste") (r "^1.0.0") (d #t) (k 0)))) (h "15krbr96hc1qyrdc37r80ydznsjpjh8apl5zyxxhpmnzz8744s71") (f (quote (("nightly") ("default" "nightly"))))))

(define-public crate-dyn-context-0.3.0 (c (n "dyn-context") (v "0.3.0") (d (list (d (n "generics") (r "^0.1.0") (d #t) (k 0)) (d (n "macro-attr-2018") (r "^1.0.0") (d #t) (k 2)) (d (n "paste") (r "^1.0.0") (d #t) (k 0)))) (h "05zfp2pjg5qnx6663xmwrx9ssygb9cgzd7i6n0bm8hh41ab6jcri") (f (quote (("nightly") ("default" "nightly"))))))

(define-public crate-dyn-context-0.3.1 (c (n "dyn-context") (v "0.3.1") (d (list (d (n "generics") (r "^0.1.0") (d #t) (k 0)) (d (n "macro-attr-2018") (r "^1.0.0") (d #t) (k 2)) (d (n "paste") (r "^1.0.0") (d #t) (k 0)))) (h "0q22kh55allmc6lbc4smq2abkm3f43mvg55p5pf9h8khysvd31ah") (f (quote (("nightly") ("default" "nightly"))))))

(define-public crate-dyn-context-0.3.2 (c (n "dyn-context") (v "0.3.2") (d (list (d (n "generics") (r "^0.2.4") (d #t) (k 0)) (d (n "macro-attr-2018") (r "^1.1.0") (d #t) (k 2)) (d (n "paste") (r "^1.0.0") (d #t) (k 0)))) (h "065i77wayvy2gxk4djvvw9zj2f3zfmxwdbrnyl8zralxnlblcmqx") (f (quote (("nightly") ("default" "nightly"))))))

(define-public crate-dyn-context-0.4.0 (c (n "dyn-context") (v "0.4.0") (d (list (d (n "generics") (r "^0.2.4") (d #t) (k 0)) (d (n "macro-attr-2018") (r "^1.1.0") (d #t) (k 2)) (d (n "paste") (r "^1.0.0") (d #t) (k 0)))) (h "173iiqmrdd1251vqy8g3mm9d6pcf18yw70ayivpmqh71lblgiz1v") (f (quote (("nightly") ("default" "nightly"))))))

(define-public crate-dyn-context-0.4.1 (c (n "dyn-context") (v "0.4.1") (d (list (d (n "generics") (r "^0.2.4") (d #t) (k 0)) (d (n "macro-attr-2018") (r "^1.1.0") (d #t) (k 2)) (d (n "paste") (r "^1.0.0") (d #t) (k 0)))) (h "188slfd67y65x59m19c0bhc2qbi2kw9877d8ir461fgv98jk0iyl") (f (quote (("nightly") ("default" "nightly"))))))

(define-public crate-dyn-context-0.5.0 (c (n "dyn-context") (v "0.5.0") (d (list (d (n "generics") (r "^0.2.4") (d #t) (k 0)) (d (n "macro-attr-2018") (r "^1.1.0") (d #t) (k 2)) (d (n "paste") (r "^1.0.0") (d #t) (k 0)))) (h "0idrjp9h0brywzi0zkl12r8v4zdn3r3hkvxdy2k529y01fmq1v08") (f (quote (("nightly") ("default" "nightly"))))))

(define-public crate-dyn-context-0.6.0 (c (n "dyn-context") (v "0.6.0") (d (list (d (n "generics") (r "^0.2.4") (d #t) (k 0)) (d (n "macro-attr-2018") (r "^1.1.0") (d #t) (k 2)) (d (n "paste") (r "^1.0.0") (d #t) (k 0)))) (h "1pxq94xmjazjzqyk90mnwc8mymcx38c7j0nryfh05ahrc5q9zc2j") (f (quote (("nightly") ("default" "nightly"))))))

(define-public crate-dyn-context-0.6.1 (c (n "dyn-context") (v "0.6.1") (d (list (d (n "generics") (r "^0.3.1") (d #t) (k 0)) (d (n "macro-attr-2018") (r "^1.1.1") (d #t) (k 2)) (d (n "paste") (r "^1.0.0") (d #t) (k 0)))) (h "1cnpq2srn6np3dmwz1xdlah4xlk4a19pddav2djj6py55karidaw") (f (quote (("nightly") ("default" "nightly"))))))

(define-public crate-dyn-context-0.7.0 (c (n "dyn-context") (v "0.7.0") (d (list (d (n "generics") (r "^0.3.1") (d #t) (k 0)) (d (n "paste") (r "^1.0.0") (d #t) (k 0)))) (h "1d4301v4gjvw4457yba12r59hvklwmj16mmynn3wswzza6milkwb") (f (quote (("nightly") ("default" "nightly"))))))

(define-public crate-dyn-context-0.8.0 (c (n "dyn-context") (v "0.8.0") (d (list (d (n "educe") (r "^0.4.18") (d #t) (k 0)) (d (n "generics") (r "^0.3.1") (d #t) (k 0)) (d (n "paste") (r "^1.0.0") (d #t) (k 0)) (d (n "phantom-type") (r "^0.3.1") (d #t) (k 0)))) (h "1gcdf6yyqfplf2gg68sidc0did9hi2p54d1snzjj3r8drbrbpcld") (f (quote (("nightly") ("default" "nightly"))))))

(define-public crate-dyn-context-0.9.0 (c (n "dyn-context") (v "0.9.0") (d (list (d (n "educe") (r "^0.4.18") (d #t) (k 0)) (d (n "generics") (r "^0.3.1") (d #t) (k 0)) (d (n "paste") (r "^1.0.0") (d #t) (k 0)) (d (n "phantom-type") (r "^0.3.1") (d #t) (k 0)))) (h "19qk5vjr3bxs8jzjqj8773h1l8wjla7nqy3wja45djxak3aqchbg") (f (quote (("nightly") ("default" "nightly"))))))

(define-public crate-dyn-context-0.10.0 (c (n "dyn-context") (v "0.10.0") (d (list (d (n "educe") (r "^0.4.18") (d #t) (k 0)) (d (n "generics") (r "^0.3.1") (d #t) (k 0)) (d (n "paste") (r "^1.0.0") (d #t) (k 0)) (d (n "phantom-type") (r "^0.3.1") (d #t) (k 0)))) (h "172igxdafw6d1cal72biw3073b22dxhp257z0z5j3n3zj65dxf7i") (f (quote (("nightly") ("default" "nightly"))))))

(define-public crate-dyn-context-0.10.1 (c (n "dyn-context") (v "0.10.1") (d (list (d (n "educe") (r "^0.4.18") (d #t) (k 0)) (d (n "generics") (r "^0.3.1") (d #t) (k 0)) (d (n "panicking") (r "^0.0.1") (k 0)) (d (n "paste") (r "^1.0.0") (d #t) (k 0)) (d (n "phantom-type") (r "^0.3.1") (d #t) (k 0)))) (h "0ab6451167iv5xkv0sl987kinb4g4znil1fflz6gcbk0ckqhnm6x") (f (quote (("nightly") ("default" "nightly"))))))

(define-public crate-dyn-context-0.10.2 (c (n "dyn-context") (v "0.10.2") (d (list (d (n "educe") (r "^0.4.18") (d #t) (k 0)) (d (n "generics") (r "^0.3.1") (d #t) (k 0)) (d (n "panicking") (r "^0.0.1") (k 0)) (d (n "paste") (r "^1.0.0") (d #t) (k 0)) (d (n "phantom-type") (r "^0.3.1") (d #t) (k 0)))) (h "0dcbw6js2p24bs9b004w9c8dqbpkqqs3zn3mixgppc2233311mp5") (f (quote (("nightly") ("default" "nightly"))))))

(define-public crate-dyn-context-0.10.3 (c (n "dyn-context") (v "0.10.3") (d (list (d (n "educe") (r "^0.4.18") (d #t) (k 0)) (d (n "generics") (r "^0.3.1") (d #t) (k 0)) (d (n "panicking") (r "^0.0.1") (k 0)) (d (n "paste") (r "^1.0.0") (d #t) (k 0)) (d (n "phantom-type") (r "^0.3.1") (d #t) (k 0)))) (h "0wdbxi4kiz2q3n0rrczyr1mkq986zgqh9899qqfayayljwfm3vzc") (f (quote (("nightly") ("default" "nightly"))))))

(define-public crate-dyn-context-0.10.4 (c (n "dyn-context") (v "0.10.4") (d (list (d (n "educe") (r "^0.4.18") (d #t) (k 0)) (d (n "generics") (r "^0.3.1") (d #t) (k 0)) (d (n "panicking") (r "^0.0.1") (k 0)) (d (n "paste") (r "^1.0.0") (d #t) (k 0)) (d (n "phantom-type") (r "^0.3.1") (d #t) (k 0)))) (h "1v7cx2wvz8lj8h9xjnbi5mfglj1r46gakb47ixhz1qhrvvi0gyzf") (f (quote (("nightly") ("default" "nightly"))))))

(define-public crate-dyn-context-0.10.5 (c (n "dyn-context") (v "0.10.5") (d (list (d (n "educe") (r "^0.4.18") (d #t) (k 0)) (d (n "generics") (r "^0.3.1") (d #t) (k 0)) (d (n "panicking") (r "^0.0.1") (k 0)) (d (n "paste") (r "^1.0.0") (d #t) (k 0)) (d (n "phantom-type") (r "^0.3.1") (d #t) (k 0)))) (h "0bijkm18dxw7jhr9ws1dgc3fp2a7ilnw3f00jny65fswlp6c4a4w") (f (quote (("nightly") ("default" "nightly"))))))

(define-public crate-dyn-context-0.10.6 (c (n "dyn-context") (v "0.10.6") (d (list (d (n "educe") (r "^0.4.18") (d #t) (k 0)) (d (n "generics") (r "^0.3.1") (d #t) (k 0)) (d (n "panicking") (r "^0.0.1") (k 0)) (d (n "paste") (r "^1.0.0") (d #t) (k 0)) (d (n "phantom-type") (r "^0.3.1") (d #t) (k 0)))) (h "1jwl15997amhg8183c35gnbcs65sjaalmf9gq5g07yvcxbhirwyy") (f (quote (("nightly") ("default" "nightly"))))))

(define-public crate-dyn-context-0.10.7 (c (n "dyn-context") (v "0.10.7") (d (list (d (n "educe") (r "^0.4.18") (d #t) (k 0)) (d (n "generics") (r "^0.3.1") (d #t) (k 0)) (d (n "panicking") (r "^0.0.1") (k 0)) (d (n "paste") (r "^1.0.0") (d #t) (k 0)) (d (n "phantom-type") (r "^0.3.1") (d #t) (k 0)))) (h "1j5hwc20yypxzyms919cbhyacd9sgv60pr828nl6hpx7bgcdj6xr") (f (quote (("nightly") ("default" "nightly"))))))

(define-public crate-dyn-context-0.10.8 (c (n "dyn-context") (v "0.10.8") (d (list (d (n "educe") (r "^0.4.18") (d #t) (k 0)) (d (n "generics") (r "^0.3.1") (d #t) (k 0)) (d (n "panicking") (r "^0.0.1") (k 0)) (d (n "paste") (r "^1.0.0") (d #t) (k 0)) (d (n "phantom-type") (r "^0.3.1") (k 0)))) (h "1614qd35kdnqi3vj6xp06ryjj2grzkp9z2jixhab3br6x0w2j8ji") (f (quote (("nightly") ("default" "nightly"))))))

(define-public crate-dyn-context-0.11.0 (c (n "dyn-context") (v "0.11.0") (d (list (d (n "educe") (r "^0.4.18") (d #t) (k 0)) (d (n "generics") (r "^0.4.0") (d #t) (k 0)) (d (n "panicking") (r "^0.0.1") (k 0)) (d (n "paste") (r "^1.0.0") (d #t) (k 0)) (d (n "phantom-type") (r "^0.4.1") (k 0)))) (h "1jpfagc8ggvcrpz511c2giynisg95vciax5jvx7wlfvzccafi19j") (f (quote (("nightly") ("default" "nightly")))) (r "1.60")))

(define-public crate-dyn-context-0.11.1 (c (n "dyn-context") (v "0.11.1") (d (list (d (n "educe") (r "^0.4.18") (d #t) (k 0)) (d (n "generics") (r "^0.4.0") (d #t) (k 0)) (d (n "panicking") (r "^0.0.1") (k 0)) (d (n "paste") (r "^1.0.0") (d #t) (k 0)) (d (n "phantom-type") (r "^0.4.1") (k 0)))) (h "1fizgv8nrwhq0facq01rr79l4czc7x18xpawyawmkcn5mhs2cxic") (f (quote (("nightly") ("default" "nightly")))) (r "1.60")))

(define-public crate-dyn-context-0.12.0 (c (n "dyn-context") (v "0.12.0") (d (list (d (n "educe") (r "^0.4.18") (d #t) (k 0)) (d (n "generics") (r "^0.4.0") (d #t) (k 0)) (d (n "panicking") (r "^0.0.1") (k 0)) (d (n "paste") (r "^1.0.0") (d #t) (k 0)) (d (n "phantom-type") (r "^0.4.1") (k 0)))) (h "0nkyj246nkdpm8lgvgqr0v63ja2p6314wpcwqqw5lnmfrdyf7gl0") (f (quote (("nightly") ("default" "nightly")))) (r "1.60")))

(define-public crate-dyn-context-0.13.0 (c (n "dyn-context") (v "0.13.0") (d (list (d (n "educe") (r "^0.4.18") (d #t) (k 0)) (d (n "either") (r "^1.6.1") (k 0)) (d (n "generics") (r "^0.4.0") (d #t) (k 0)) (d (n "panicking") (r "^0.0.1") (k 0)) (d (n "paste") (r "^1.0.0") (d #t) (k 0)) (d (n "phantom-type") (r "^0.4.1") (k 0)))) (h "04vzg4ad8bnh9r1irnvqca21miszmsqalpiy7ly4xawnlp9nvmw7") (f (quote (("nightly") ("default" "nightly")))) (r "1.60")))

(define-public crate-dyn-context-0.14.0 (c (n "dyn-context") (v "0.14.0") (d (list (d (n "educe") (r "^0.4.18") (d #t) (k 0)) (d (n "generics") (r "^0.4.0") (d #t) (k 0)) (d (n "panicking") (r "^0.0.1") (k 0)) (d (n "paste") (r "^1.0.0") (d #t) (k 0)) (d (n "phantom-type") (r "^0.4.1") (k 0)))) (h "07y622sfnhd80zwkp6wyf0clbqg4gpn0zy0zblw7b6hq9b4qjhif") (f (quote (("nightly") ("default" "nightly")))) (r "1.60")))

(define-public crate-dyn-context-0.14.1 (c (n "dyn-context") (v "0.14.1") (d (list (d (n "educe") (r "^0.4.18") (d #t) (k 0)) (d (n "generics") (r "^0.4.0") (d #t) (k 0)) (d (n "macro-attr-2018") (r "^2.0.1") (d #t) (k 2)) (d (n "panicking") (r "^0.0.1") (k 0)) (d (n "paste") (r "^1.0.0") (d #t) (k 0)) (d (n "phantom-type") (r "^0.4.1") (k 0)))) (h "0sv09s6clrihigqn8xljvm45zszs72lal6z28flnykj3rk7df0m2") (f (quote (("nightly") ("default" "nightly")))) (r "1.60")))

(define-public crate-dyn-context-0.14.2 (c (n "dyn-context") (v "0.14.2") (d (list (d (n "dyn-context-macro") (r "^0.0.2") (d #t) (k 0)) (d (n "educe") (r "^0.4.18") (d #t) (k 0)) (d (n "generics") (r "^0.4.0") (d #t) (k 0)) (d (n "macro-attr-2018") (r "^2.0.1") (d #t) (k 2)) (d (n "panicking") (r "^0.0.1") (k 0)) (d (n "paste") (r "^1.0.0") (d #t) (k 0)) (d (n "phantom-type") (r "^0.4.1") (k 0)))) (h "0zpvfdakkwdgmcxdsgyvc947j97pci5jph83yqni7kari7l1mxl7") (f (quote (("nightly") ("default" "nightly")))) (r "1.60")))

(define-public crate-dyn-context-0.15.0 (c (n "dyn-context") (v "0.15.0") (d (list (d (n "dyn-context-macro") (r "^0.0.3") (d #t) (k 0)) (d (n "educe") (r "^0.4.18") (d #t) (k 0)) (d (n "generics") (r "^0.4.0") (d #t) (k 0)) (d (n "macro-attr-2018") (r "^2.0.1") (d #t) (k 2)) (d (n "panicking") (r "^0.0.1") (k 0)) (d (n "paste") (r "^1.0.0") (d #t) (k 0)) (d (n "phantom-type") (r "^0.4.1") (k 0)))) (h "149hdb4zjhamacjqfzrjgpjfscf7sj6aclqhs8yks6ihph9inhsg") (f (quote (("nightly") ("default" "nightly")))) (r "1.60")))

(define-public crate-dyn-context-0.16.0 (c (n "dyn-context") (v "0.16.0") (d (list (d (n "dyn-context-macro") (r "^0.0.3") (d #t) (k 0)) (d (n "educe") (r "^0.4.18") (d #t) (k 0)) (d (n "generics") (r "^0.4.0") (d #t) (k 0)) (d (n "macro-attr-2018") (r "^2.0.1") (d #t) (k 2)) (d (n "panicking") (r "^0.0.1") (k 0)) (d (n "paste") (r "^1.0.0") (d #t) (k 0)) (d (n "phantom-type") (r "^0.4.1") (k 0)))) (h "0rzspnwiipq7ykhsbdm8pb441alz58k05srva4w6wc31z8qzdky4") (f (quote (("nightly") ("default" "nightly")))) (r "1.60")))

(define-public crate-dyn-context-0.16.1 (c (n "dyn-context") (v "0.16.1") (d (list (d (n "dyn-context-macro") (r "^0.0.3") (d #t) (k 0)) (d (n "educe") (r "^0.4.18") (d #t) (k 0)) (d (n "generics") (r "^0.4.0") (d #t) (k 0)) (d (n "macro-attr-2018") (r "^2.0.1") (d #t) (k 2)) (d (n "panicking") (r "^0.0.2") (k 0)) (d (n "paste") (r "^1.0.0") (d #t) (k 0)) (d (n "phantom-type") (r "^0.4.1") (k 0)))) (h "1i5kqmaiw27nzsm7q34jw2nhsaayrlzfr4rqsiw12dfb891q26ha") (f (quote (("nightly") ("default" "nightly")))) (r "1.60")))

(define-public crate-dyn-context-0.17.0 (c (n "dyn-context") (v "0.17.0") (d (list (d (n "dyn-context-macro") (r "^0.0.4") (d #t) (k 0)) (d (n "educe") (r "^0.4.18") (d #t) (k 0)) (d (n "generics") (r "^0.4.0") (d #t) (k 0)) (d (n "macro-attr-2018") (r "^2.0.1") (d #t) (k 2)) (d (n "panicking") (r "^0.1.0") (k 0)) (d (n "paste") (r "^1.0.0") (d #t) (k 0)) (d (n "phantom-type") (r "^0.4.1") (k 0)))) (h "04jdv43vqf6sc0v2cvxq15z162hzm318qdfbzs0mpbdgnk3c59a7") (f (quote (("nightly") ("default" "nightly")))) (r "1.60")))

(define-public crate-dyn-context-0.17.1 (c (n "dyn-context") (v "0.17.1") (d (list (d (n "dyn-context-macro") (r "^0.0.4") (d #t) (k 0)) (d (n "educe") (r "^0.4.18") (d #t) (k 0)) (d (n "generics") (r "^0.4.0") (d #t) (k 0)) (d (n "indoc") (r "^1.0.6") (d #t) (k 0)) (d (n "macro-attr-2018") (r "^2.0.1") (d #t) (k 2)) (d (n "panicking") (r "^0.1.0") (k 0)) (d (n "paste") (r "^1.0.0") (d #t) (k 0)) (d (n "phantom-type") (r "^0.4.1") (k 0)))) (h "1blmxdq8g095a3y6yacq23kzmpg42q9ii3k44wj0pbcw9cf6pif6") (f (quote (("nightly") ("default" "nightly")))) (r "1.60")))

(define-public crate-dyn-context-0.17.2 (c (n "dyn-context") (v "0.17.2") (d (list (d (n "dyn-context-macro") (r "^0.0.4") (d #t) (k 0)) (d (n "educe") (r "^0.4.18") (d #t) (k 0)) (d (n "generics") (r "^0.4.0") (d #t) (k 0)) (d (n "indoc") (r "^1.0.6") (d #t) (k 0)) (d (n "macro-attr-2018") (r "^2.0.1") (d #t) (k 2)) (d (n "panicking") (r "^0.1.0") (k 0)) (d (n "paste") (r "^1.0.0") (d #t) (k 0)) (d (n "phantom-type") (r "^0.4.1") (k 0)))) (h "0k76yqzwrzf8hqgdjfx312hd614pnr90b4kmfr4qimanjrc2wz89") (f (quote (("nightly") ("default" "nightly")))) (r "1.60")))

(define-public crate-dyn-context-0.17.3 (c (n "dyn-context") (v "0.17.3") (d (list (d (n "dyn-context-macro") (r "^0.0.4") (d #t) (k 0)) (d (n "educe") (r "^0.4.18") (d #t) (k 0)) (d (n "generics") (r "^0.4.0") (d #t) (k 0)) (d (n "indoc") (r "^1.0.6") (d #t) (k 0)) (d (n "macro-attr-2018") (r "^2.0.1") (d #t) (k 2)) (d (n "panicking") (r "^0.1.0") (k 0)) (d (n "paste") (r "^1.0.0") (d #t) (k 0)) (d (n "phantom-type") (r "^0.4.1") (k 0)))) (h "1jnps46rdrfk6fwl6m0mi2h6cvv2r70ncswcimmxkp7vkpw4yblv") (f (quote (("nightly") ("default" "nightly")))) (r "1.60")))

(define-public crate-dyn-context-0.17.4 (c (n "dyn-context") (v "0.17.4") (d (list (d (n "dyn-context-macro") (r "^0.0.5") (d #t) (k 0)) (d (n "educe") (r "^0.4.18") (d #t) (k 0)) (d (n "generics") (r "^0.4.0") (d #t) (k 0)) (d (n "indoc") (r "^1.0.6") (d #t) (k 0)) (d (n "macro-attr-2018") (r "^2.0.1") (d #t) (k 2)) (d (n "panicking") (r "^0.1.0") (k 0)) (d (n "paste") (r "^1.0.0") (d #t) (k 0)) (d (n "phantom-type") (r "^0.4.1") (k 0)))) (h "0c595z4p7aj6mx721xd85p9j4cd3f1w7pfr6nm5ld8gp4j43v1vb") (f (quote (("nightly") ("default" "nightly")))) (r "1.60")))

(define-public crate-dyn-context-0.18.0 (c (n "dyn-context") (v "0.18.0") (d (list (d (n "generics") (r "^0.4.0") (d #t) (k 0)) (d (n "indoc") (r "^1.0.6") (d #t) (k 0)) (d (n "paste") (r "^1.0.0") (d #t) (k 0)))) (h "19isrzk860jxxs7d3fhy35w0bd94j8w96brvi32zc4d2zy7pd9pm") (r "1.60")))

(define-public crate-dyn-context-0.19.0 (c (n "dyn-context") (v "0.19.0") (d (list (d (n "generics") (r "^0.5.1") (d #t) (k 0)) (d (n "indoc") (r "^2.0.4") (d #t) (k 0)) (d (n "paste") (r "^1.0.0") (d #t) (k 0)))) (h "0qy9jj4gph1a7m4f72cpwqabynyg7739nva0ynpnvdw3lrm4di0x") (r "1.71")))

(define-public crate-dyn-context-0.19.1 (c (n "dyn-context") (v "0.19.1") (d (list (d (n "generics") (r "^0.5.1") (d #t) (k 0)) (d (n "indoc") (r "^2.0.4") (d #t) (k 0)) (d (n "paste") (r "^1.0.0") (d #t) (k 0)))) (h "04lnlr2r3fnjzx0kmdxnqyg3ik7nc7f09dkq4cc8w44qfq5sr22y") (r "1.71")))

(define-public crate-dyn-context-0.19.2 (c (n "dyn-context") (v "0.19.2") (d (list (d (n "generics") (r "^0.5.1") (d #t) (k 0)) (d (n "indoc") (r "^2.0.4") (d #t) (k 0)) (d (n "paste") (r "^1.0.0") (d #t) (k 0)))) (h "0rza7fvllhvr9rd1sbd040f5ppbkvs3i985h5w41n6ip4n5n4pv4") (r "1.71")))

(define-public crate-dyn-context-0.19.3 (c (n "dyn-context") (v "0.19.3") (d (list (d (n "generics") (r "^0.5.1") (d #t) (k 0)) (d (n "indoc") (r "^2.0.4") (d #t) (k 0)) (d (n "paste") (r "^1.0.0") (d #t) (k 0)))) (h "011725qh6c0971jxv27ivy42h83sc1rsgq1aqa766c5hdvg7y0v2") (r "1.71")))

(define-public crate-dyn-context-0.19.4 (c (n "dyn-context") (v "0.19.4") (d (list (d (n "generics") (r "^0.5.1") (d #t) (k 0)) (d (n "indoc") (r "^2.0.4") (d #t) (k 0)) (d (n "paste") (r "^1.0.0") (d #t) (k 0)))) (h "1lig4yw82g1mknf7v5l5x23wymgj5aqqarmwcxpy5w74ppvf4zij") (r "1.71")))

(define-public crate-dyn-context-0.19.5 (c (n "dyn-context") (v "0.19.5") (d (list (d (n "generics") (r "^0.5.1") (d #t) (k 0)) (d (n "indoc") (r "^2.0.4") (d #t) (k 0)) (d (n "paste") (r "^1.0.0") (d #t) (k 0)))) (h "14fll4874yf8fd5v83z1ralbhc1xrnvmcpfh0941q4r1fdnvqwqg") (r "1.71")))

