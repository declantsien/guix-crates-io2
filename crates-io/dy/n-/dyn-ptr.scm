(define-module (crates-io dy n- dyn-ptr) #:use-module (crates-io))

(define-public crate-dyn-ptr-0.1.0 (c (n "dyn-ptr") (v "0.1.0") (h "03fgsap54jdbrr9g52gkmqrkn6zffl6cdlvm6r1d6zssn89s3lsc") (f (quote (("any-ptr"))))))

(define-public crate-dyn-ptr-0.2.0 (c (n "dyn-ptr") (v "0.2.0") (d (list (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "042gvbpb3nkwmf4hcgsswm7a989qf1lg3q4pp1n00qkbvj0hyahj") (f (quote (("default" "any-ptr") ("any-ptr"))))))

(define-public crate-dyn-ptr-0.2.1 (c (n "dyn-ptr") (v "0.2.1") (d (list (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0mkadk61lmszmfjxgaymwyr0r02j8abrpjc7nyma6ss6lsi5qi0j") (f (quote (("default" "any-ptr") ("any-ptr"))))))

(define-public crate-dyn-ptr-0.2.2 (c (n "dyn-ptr") (v "0.2.2") (d (list (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1za69pl6p0wnicasrh9ar72v7lc92r1av3xx326lxrmxhp6nx5b6") (f (quote (("default" "any-ptr") ("any-ptr"))))))

(define-public crate-dyn-ptr-0.2.3 (c (n "dyn-ptr") (v "0.2.3") (d (list (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0drcsb0bv09fjlws5hlppmjk6h9lzix7cyjkl573qrmnfmjzbkfa") (f (quote (("default" "any-ptr") ("any-ptr"))))))

