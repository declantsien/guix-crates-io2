(define-module (crates-io dy n- dyn-timeout) #:use-module (crates-io))

(define-public crate-dyn-timeout-0.1.0 (c (n "dyn-timeout") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1.15.0") (f (quote ("full" "macros"))) (d #t) (k 0)))) (h "0xlrgf1h286yg2fz98mqnab0ckjs06qmmgnkbvgmh42zjdnf301l")))

(define-public crate-dyn-timeout-0.2.0 (c (n "dyn-timeout") (v "0.2.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "tokio") (r "^1.15.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1gjnzfiv6qk14m9bxgclza3yb2ab51zqjrmf0nrcp5m3ljy7cp9s")))

(define-public crate-dyn-timeout-0.3.0 (c (n "dyn-timeout") (v "0.3.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "tokio") (r "^1.17") (f (quote ("full"))) (d #t) (k 0)))) (h "0gsz84mv29789yf6xnrk38y478bq3rxw9qgvrccyw0bsiq0j0xrn")))

(define-public crate-dyn-timeout-0.3.1 (c (n "dyn-timeout") (v "0.3.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "tokio") (r "^1.17") (f (quote ("full"))) (d #t) (k 0)))) (h "0cf0s8z9vlnq1miix5wsmii5bm6xry59w73h3brgqfms29xkgk2g")))

(define-public crate-dyn-timeout-0.3.2 (c (n "dyn-timeout") (v "0.3.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "tokio") (r "^1.17") (f (quote ("full"))) (d #t) (k 0)))) (h "177xrdx6xjhs1lqphxnrcqrhrw8820fjjln6nrny110h91p88rzx")))

(define-public crate-dyn-timeout-0.3.3 (c (n "dyn-timeout") (v "0.3.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "tokio") (r "^1.17") (f (quote ("full"))) (d #t) (k 0)))) (h "1mpcadnlj5fipbk4vapnsy4kv3cks5zcxksmrxqyxj9mm0n81vv5")))

