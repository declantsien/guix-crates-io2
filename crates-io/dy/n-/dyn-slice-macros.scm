(define-module (crates-io dy n- dyn-slice-macros) #:use-module (crates-io))

(define-public crate-dyn-slice-macros-3.2.0-alpha.3 (c (n "dyn-slice-macros") (v "3.2.0-alpha.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "17j1j2064150xsfpc3gl69m24vyyfrbc2nzvqw8cgg6ji7p6kg7c")))

(define-public crate-dyn-slice-macros-3.2.0 (c (n "dyn-slice-macros") (v "3.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1qi8k4id7fkkjf8pagh1pm2zz8h7zwdhqc25ynbnpm6m3sqwkryy")))

