(define-module (crates-io dy n- dyn-dictionary) #:use-module (crates-io))

(define-public crate-dyn-dictionary-0.1.0 (c (n "dyn-dictionary") (v "0.1.0") (h "1s7zskml5pr5xqnhb0q2ydz7r9gfp738da2rlbidgvi8zy74rjfc") (y #t)))

(define-public crate-dyn-dictionary-0.1.2 (c (n "dyn-dictionary") (v "0.1.2") (d (list (d (n "serde") (r "^1.0.127") (d #t) (k 0)))) (h "0fhjkcjwpbr092h8nzjwnjcmkac87m122jrxxdl9mxh1s9qdh5r8") (y #t)))

(define-public crate-dyn-dictionary-0.1.3 (c (n "dyn-dictionary") (v "0.1.3") (d (list (d (n "serde") (r "^1.0.127") (d #t) (k 0)))) (h "0s7lvf1x64xwxfiak34cxqrk77psip1pj0033c60l2dllf9vcfj9") (y #t)))

(define-public crate-dyn-dictionary-0.1.4 (c (n "dyn-dictionary") (v "0.1.4") (d (list (d (n "serde") (r "^1.0.127") (d #t) (k 0)))) (h "10d730nxsrcr4kyk8nvdryad63y3cbcfxldv0qhvl9lpx2qhhllh")))

