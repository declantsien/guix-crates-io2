(define-module (crates-io dy li dylib) #:use-module (crates-io))

(define-public crate-dylib-0.0.1 (c (n "dylib") (v "0.0.1") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "1bj7bs0vm31yf9vvfagmdqa333vyhshbypmcqliizmv7qh7hgzyd")))

(define-public crate-dylib-0.0.2 (c (n "dylib") (v "0.0.2") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "04yzr8l5qgzk3kr27pkakla9i7wc8xhqh9rccsdp1s0rq43w7m5q")))

(define-public crate-dylib-0.0.3 (c (n "dylib") (v "0.0.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "07kkci6i50wks3d34m5hlql216j9f8sl7qgfcfri5a8k603i6v7h")))

