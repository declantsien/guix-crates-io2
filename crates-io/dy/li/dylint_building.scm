(define-module (crates-io dy li dylint_building) #:use-module (crates-io))

(define-public crate-dylint_building-0.1.0-pre.0 (c (n "dylint_building") (v "0.1.0-pre.0") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "1cf030sxnxi89vmp5gmdmfxyggmp0bpil07nkzkmkb0q78jgpq0v")))

(define-public crate-dylint_building-0.1.0-pre.1 (c (n "dylint_building") (v "0.1.0-pre.1") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "1i857l6faqmxhf4mh2i142sygx3vw1rd3dmlbjv7w995i8w6am30")))

(define-public crate-dylint_building-0.1.0-pre.2 (c (n "dylint_building") (v "0.1.0-pre.2") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "0cbi9qcl0455qbhdc5ccp6dwd498ij3v4wn0j045p3zkrpg05zm5")))

