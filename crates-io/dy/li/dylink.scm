(define-module (crates-io dy li dylink) #:use-module (crates-io))

(define-public crate-dylink-0.1.0 (c (n "dylink") (v "0.1.0") (d (list (d (n "dylink_macro") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "once_cell") (r "^1.16") (d #t) (k 0)) (d (n "windows-sys") (r "^0.48") (f (quote ("Win32_Foundation" "Win32_System_LibraryLoader"))) (d #t) (t "cfg(windows)") (k 0)))) (h "04fnljzsi7bkw66ksd1h0wwchj01whfhb2rvpa5fr3xsahqm7czv") (y #t)))

(define-public crate-dylink-0.2.0 (c (n "dylink") (v "0.2.0") (d (list (d (n "dylink_macro") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "once_cell") (r "^1.16") (d #t) (k 0)) (d (n "windows-sys") (r "^0.48") (f (quote ("Win32_Foundation" "Win32_System_LibraryLoader"))) (d #t) (t "cfg(windows)") (k 0)))) (h "06z3jyixzjfbjwzjkdkz6zcq51cm5gqwj5pkp9kh7ml2kaysv68v") (y #t)))

(define-public crate-dylink-0.2.1 (c (n "dylink") (v "0.2.1") (d (list (d (n "dylink_macro") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "once_cell") (r "^1.16") (d #t) (k 0)) (d (n "windows-sys") (r "^0.48") (f (quote ("Win32_Foundation" "Win32_System_LibraryLoader"))) (d #t) (t "cfg(windows)") (k 0)))) (h "19jxz8qibc1d66qsn7qqv4zddaf1pw93ljjhmxkpvpkjnh07zf7v") (y #t)))

(define-public crate-dylink-0.3.0 (c (n "dylink") (v "0.3.0") (d (list (d (n "dylink_macro") (r "^0.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "once_cell") (r "^1.16") (d #t) (k 0)) (d (n "windows-sys") (r "^0.48") (f (quote ("Win32_Foundation" "Win32_System_LibraryLoader"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0vj3xyp952vx3rb04aay7a66ks3k3cyi3qfqxvpqdq7zxjd17shn") (y #t)))

(define-public crate-dylink-0.4.0 (c (n "dylink") (v "0.4.0") (d (list (d (n "dylink_macro") (r "^0.5.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (t "cfg(unix)") (k 0)) (d (n "once_cell") (r "^1.16") (d #t) (k 0)) (d (n "windows-sys") (r "^0.48") (f (quote ("Win32_Foundation" "Win32_System_LibraryLoader"))) (t "cfg(windows)") (k 0)))) (h "1ng6d10pc6g4bwi7lasw5rpdbn2wqmdmx8npwb1yn8l70x76qya8") (y #t)))

(define-public crate-dylink-0.5.0 (c (n "dylink") (v "0.5.0") (d (list (d (n "dylink_macro") (r "^0.6") (d #t) (k 0)) (d (n "once_cell") (r "^1.16") (d #t) (k 0)))) (h "1d5i747bz2ra5l8nli64i6s0jw1hyk44qs1y3anbc6il4d2l4l7l") (y #t)))

(define-public crate-dylink-0.5.1 (c (n "dylink") (v "0.5.1") (d (list (d (n "dylink_macro") (r "^0.6") (d #t) (k 0)))) (h "0a9qf6vps51my7baliga73nqihc297gi5r8pnbzbachwgxzy9cry") (y #t)))

(define-public crate-dylink-0.5.2 (c (n "dylink") (v "0.5.2") (d (list (d (n "dylink_macro") (r "^0.6") (d #t) (k 0)))) (h "0650gw8gpqxrdp84w5v80kf1xapppih0xn0x2iapd5z7wwxcf47h") (y #t)))

(define-public crate-dylink-0.6.0 (c (n "dylink") (v "0.6.0") (d (list (d (n "dylink_macro") (r "^0.7") (d #t) (k 0)) (d (n "once_cell") (r "^1.17") (d #t) (k 0)))) (h "1pf7djw12c7gyfbpkxv34138z35a740sbrg7xxik1kz1gyvwilxw") (y #t)))

(define-public crate-dylink-0.6.1 (c (n "dylink") (v "0.6.1") (d (list (d (n "dylink_macro") (r "^0.7") (d #t) (k 0)) (d (n "once_cell") (r "^1.17") (d #t) (k 0)))) (h "0k5dj22yxgcig7vchy9m92il7k9cmv7zla1k5pf8w6nxrk1ki82w") (y #t)))

(define-public crate-dylink-0.7.0 (c (n "dylink") (v "0.7.0") (d (list (d (n "dylink_macro") (r "^0.8") (d #t) (k 0)))) (h "1gavmjrkrbjxdwy6zha11q5x3s56vs579782ysjgrnaihvdmac3g")))

(define-public crate-dylink-0.8.0 (c (n "dylink") (v "0.8.0") (d (list (d (n "dylink_macro") (r "^0.9") (d #t) (k 0)))) (h "11wcnvx8v6j6yi8byhjv82g8bncdh6q3vydpfwyggnw9f0d71dj8") (f (quote (("unstable"))))))

(define-public crate-dylink-0.9.0 (c (n "dylink") (v "0.9.0") (d (list (d (n "dylink_macro") (r "^0.9") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1q1qf5jgy7wj17dmgb8drmlsci0bx88dy7awgsmxic641nwfh455") (f (quote (("unstable"))))))

(define-public crate-dylink-0.9.1 (c (n "dylink") (v "0.9.1") (d (list (d (n "dylink_macro") (r "^0.9") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)))) (h "0926s8jrq7nhcqyasxvmp94y3cq7kawnaps0b8fd9cpr8spks5b4") (f (quote (("unstable"))))))

(define-public crate-dylink-0.10.0 (c (n "dylink") (v "0.10.0") (d (list (d (n "dylink_macro") (r "^0.10") (d #t) (k 0)))) (h "1yacbmih2qc8b321y3kj9vnpxhyvjifwl6jqmws9m903cls9bv2b") (f (quote (("unstable"))))))

