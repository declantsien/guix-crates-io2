(define-module (crates-io dy li dylint_env) #:use-module (crates-io))

(define-public crate-dylint_env-0.1.0-pre.0 (c (n "dylint_env") (v "0.1.0-pre.0") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)))) (h "0c391sx6sl58sdbcbcdb82lfzqcldmnvmh0dbi3shgwjamj0132q")))

(define-public crate-dylint_env-0.1.0-pre.1 (c (n "dylint_env") (v "0.1.0-pre.1") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)))) (h "1fl03sjlfajn8qp6h25qdknjjxdvhj98pcwdfwf529s987p0q61b")))

(define-public crate-dylint_env-0.1.0-pre.2 (c (n "dylint_env") (v "0.1.0-pre.2") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)))) (h "1s7v47lvy964wr2y7ybkab82bbpgbk1d5y2zq5g3sh9q58m92axm")))

