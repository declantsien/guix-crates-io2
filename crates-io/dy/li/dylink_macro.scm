(define-module (crates-io dy li dylink_macro) #:use-module (crates-io))

(define-public crate-dylink_macro-0.1.0 (c (n "dylink_macro") (v "0.1.0") (h "061kz3apw93h9jv4a5aqxlnf96jdpydxwcv78s0s5ip4n1k44j9m") (y #t)))

(define-public crate-dylink_macro-0.2.0 (c (n "dylink_macro") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0sqhphl3ilf5gfjf5lcyxcbhffnm2h44sgxwi2529g9iqrnj4aph") (f (quote (("warnings") ("no_lifetimes")))) (y #t)))

(define-public crate-dylink_macro-0.3.0 (c (n "dylink_macro") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1kb42p9h4126vpk09ma5q5fxmfa8flwqkp8rg1h8fs267csb3z6z") (f (quote (("warnings") ("no_lifetimes")))) (y #t)))

(define-public crate-dylink_macro-0.4.0 (c (n "dylink_macro") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "02qyp98jw51m2gmp772nv1ivg6x6d7zq44y3xxdipgpnyhb37yzp") (f (quote (("warnings") ("no_lifetimes")))) (y #t)))

(define-public crate-dylink_macro-0.5.0 (c (n "dylink_macro") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "parsing" "printing"))) (k 0)))) (h "0qsfx650fr2mp6mg62y7nsy3whc485s9ir3qlhhfvimm6sqf1hc8") (f (quote (("warnings") ("no_lifetimes")))) (y #t)))

(define-public crate-dylink_macro-0.6.0 (c (n "dylink_macro") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "parsing" "printing"))) (k 0)))) (h "045sx58jab52gb1pmyq6avr1d9xhrvrjrh7fn7ridxfg5hfqbsaq") (y #t)))

(define-public crate-dylink_macro-0.6.1 (c (n "dylink_macro") (v "0.6.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "parsing" "printing"))) (k 0)))) (h "06g0y8w5izj2kfiaa5372162gbr87p5xy2fabhskz3d2w8yla9cj") (y #t)))

(define-public crate-dylink_macro-0.6.2 (c (n "dylink_macro") (v "0.6.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "parsing" "printing"))) (k 0)))) (h "1j0rybvfrs3ykvr1m9vs77jj43gb7cr6v0v8ibm4xpq532hhza4h") (y #t)))

(define-public crate-dylink_macro-0.7.0 (c (n "dylink_macro") (v "0.7.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "parsing" "printing"))) (k 0)))) (h "1kh43mvna237lzz8bz9sd4100gx2jjh8f8b0mq8vi0373z2sn9za") (y #t)))

(define-public crate-dylink_macro-0.8.0 (c (n "dylink_macro") (v "0.8.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "parsing" "printing" "clone-impls"))) (k 0)))) (h "04vi58vcg6b70ahczn6nns66nswh7lrg9xfih1yixyxj9vw2a1i7")))

(define-public crate-dylink_macro-0.9.0 (c (n "dylink_macro") (v "0.9.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "parsing" "printing" "clone-impls"))) (k 0)))) (h "1gqgyf1r7rpss65vbi3mn20yz0nmcrvvn1sk9xyja2rj85k2sn55")))

(define-public crate-dylink_macro-0.10.0 (c (n "dylink_macro") (v "0.10.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "parsing" "printing" "clone-impls"))) (k 0)))) (h "1bh0k4n7lmqqic12wn54f6xmkfkf5p9sbvpzag155v0fyv9968g1")))

