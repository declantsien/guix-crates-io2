(define-module (crates-io dy li dylint_examples) #:use-module (crates-io))

(define-public crate-dylint_examples-0.1.0-pre.1 (c (n "dylint_examples") (v "0.1.0-pre.1") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "dylint_building") (r "=0.1.0-pre.1") (d #t) (k 0)) (d (n "dylint_env") (r "=0.1.0-pre.1") (d #t) (k 0)))) (h "05qgj08m00s2gpx5m5y22zfdpy4hlq77d64jskp3bdjb33zsanyb")))

(define-public crate-dylint_examples-0.1.0-pre.2 (c (n "dylint_examples") (v "0.1.0-pre.2") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "dylint_building") (r "=0.1.0-pre.2") (d #t) (k 0)) (d (n "dylint_env") (r "=0.1.0-pre.2") (d #t) (k 0)))) (h "1vqpp6g7q955ynwyr5wvpk9bi2lgd1wg7d2q7zhxzmx25jclg6vz")))

(define-public crate-dylint_examples-0.1.0-pre.3 (c (n "dylint_examples") (v "0.1.0-pre.3") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "dylint_internal") (r "=0.1.0-pre.3") (d #t) (k 0)))) (h "1n6gi0zs9hw6m6g54ycbmwzyanfxwkya0vdz2m6shg6z3ddnv0dn")))

(define-public crate-dylint_examples-0.1.0-pre.4 (c (n "dylint_examples") (v "0.1.0-pre.4") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "dylint_internal") (r "=0.1.0-pre.4") (d #t) (k 0)))) (h "0rnimvlsbdpa17s1lww0hgc0bwmsa9vmk97lms3fx619rfa74l0c")))

(define-public crate-dylint_examples-0.1.0-pre.5 (c (n "dylint_examples") (v "0.1.0-pre.5") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "dylint_internal") (r "=0.1.0-pre.5") (d #t) (k 0)))) (h "1p779jk4gq2lb6c8mp0yrfw1a2dsnhfkcw7iz6xxrbls0fvrig9m")))

(define-public crate-dylint_examples-0.1.0-pre.6 (c (n "dylint_examples") (v "0.1.0-pre.6") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "dylint_internal") (r "=0.1.0-pre.6") (d #t) (k 0)))) (h "1hdjnjrfkd5v2g6bcrzhii6m1f9xjgl69qw8fa4x16fww6sx0wjp")))

(define-public crate-dylint_examples-0.1.0-pre.7 (c (n "dylint_examples") (v "0.1.0-pre.7") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "dylint_internal") (r "=0.1.0-pre.7") (d #t) (k 0)))) (h "00p3ifrkny8flrr7m0601mrzw2b2xg31qh7xj7i4cqsr0m1lgf9p")))

(define-public crate-dylint_examples-0.1.0-pre.8 (c (n "dylint_examples") (v "0.1.0-pre.8") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "dylint_internal") (r "=0.1.0-pre.8") (d #t) (k 0)))) (h "1xdpvkdh70qdkmrwmxhn7d8vcjmaahki70hamyqnjcycf07mbk7j")))

(define-public crate-dylint_examples-0.1.0-pre.9 (c (n "dylint_examples") (v "0.1.0-pre.9") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "dylint_internal") (r "=0.1.0-pre.9") (d #t) (k 0)))) (h "12vlfgzpzb9zphxg8hjf68bhd4zfzxim0a3xd1vixdx1kiqpfzs3")))

(define-public crate-dylint_examples-0.1.0-pre.10 (c (n "dylint_examples") (v "0.1.0-pre.10") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "dylint_internal") (r "=0.1.0-pre.10") (d #t) (k 0)))) (h "04zsxxfp24b2agssagbcsmvw03agnnwfc0pvxld2yqvhb3s8ri2f")))

(define-public crate-dylint_examples-0.1.0-pre.11 (c (n "dylint_examples") (v "0.1.0-pre.11") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "dylint_internal") (r "=0.1.0-pre.11") (d #t) (k 0)))) (h "100mjb7g79gqqd0mwp9hiy89xjb37xy998mm2ag2c5qzp61bpg2k")))

(define-public crate-dylint_examples-0.1.0-pre.12 (c (n "dylint_examples") (v "0.1.0-pre.12") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "dylint_internal") (r "=0.1.0-pre.12") (d #t) (k 0)))) (h "080bwz6l2nh4h971ymms6xlbbmv0ri3msgpp2xbb3lv4al4hikz8")))

(define-public crate-dylint_examples-0.1.0-pre.13 (c (n "dylint_examples") (v "0.1.0-pre.13") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "dylint_internal") (r "=0.1.0-pre.13") (d #t) (k 0)))) (h "12a3anj21sdqg8jxrcq2qjay6wp037lz8n1iwy7b13mq4ikr7lbi")))

(define-public crate-dylint_examples-0.1.0-pre.14 (c (n "dylint_examples") (v "0.1.0-pre.14") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "dylint_internal") (r "=0.1.0-pre.14") (d #t) (k 0)))) (h "1409i7crj2v3420j9n867gjw9i0krywkinmzb4yf8vcs2m41n3np")))

(define-public crate-dylint_examples-0.1.0 (c (n "dylint_examples") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "dylint_internal") (r "=0.1.0") (d #t) (k 0)))) (h "1ss8j218briph189wn2zyas2bgynx84sc839m49wpv38hzfw2j71")))

(define-public crate-dylint_examples-0.1.2 (c (n "dylint_examples") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "dylint_internal") (r "=0.1.2") (d #t) (k 0)))) (h "0c8ib558fy1gkjz9y7r5n70mdiz9vq5169mvh83ssz08skwxzr84")))

(define-public crate-dylint_examples-0.1.3 (c (n "dylint_examples") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "dylint_internal") (r "=0.1.3") (d #t) (k 0)))) (h "1p0sg9wlqzs089zd51rcmnwb5nqcfd1x4x498n5ygwfk7xhqy2j7")))

