(define-module (crates-io dy er dyer-macros) #:use-module (crates-io))

(define-public crate-dyer-macros-0.1.0 (c (n "dyer-macros") (v "0.1.0") (h "0l3c3idj4xwpp5l4rchbsqzdw0lbj9blrxsxprizil99q5dccm69")))

(define-public crate-dyer-macros-0.2.0 (c (n "dyer-macros") (v "0.2.0") (h "1cyh2as2pldv5w7rfl8v3y5vf3cpxkawahm5j5qpndph1b0p0mky")))

