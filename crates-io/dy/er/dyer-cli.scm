(define-module (crates-io dy er dyer-cli) #:use-module (crates-io))

(define-public crate-dyer-cli-0.1.0 (c (n "dyer-cli") (v "0.1.0") (h "05w2rzb451ac3aq0q5nys9a55xk24lbq8x3ds8218vngmlblx6y9")))

(define-public crate-dyer-cli-0.1.1 (c (n "dyer-cli") (v "0.1.1") (h "0vddrjxv9k6bykjpa19prazskc5c3m6bvf8w4fyx7wzizfqylbf7")))

(define-public crate-dyer-cli-0.1.2 (c (n "dyer-cli") (v "0.1.2") (h "183sv0hqz8c8nw33j6gq4bf16dchh91abqz0gx586lvdz790pq94")))

(define-public crate-dyer-cli-0.1.3 (c (n "dyer-cli") (v "0.1.3") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "0vxzlkw4qvai8rbz2aj6ak7rj7ikrx4h3pav4x6rzjbsbrjcqlny")))

(define-public crate-dyer-cli-0.2.0 (c (n "dyer-cli") (v "0.2.0") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "0vk1p1jlk66xsxwvvlchpjw2m75rp793z5mh3zwa67q2ic4bbp4a")))

(define-public crate-dyer-cli-0.3.0 (c (n "dyer-cli") (v "0.3.0") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "0mgc7h7fp4ss8xfj1xv1r7v7jcf3lnsb75i7h86616jg9daicjv2")))

(define-public crate-dyer-cli-0.4.0 (c (n "dyer-cli") (v "0.4.0") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "0ab4hf1sjshfmr9p6c0d17m5swh8afp9bgvid2n4z9mw4vh6grl9")))

(define-public crate-dyer-cli-0.5.0 (c (n "dyer-cli") (v "0.5.0") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "1s3js7ri7cvb4a4l62njpdwrjkpma8pi2qgsck9fpkvbld5b4ycz")))

(define-public crate-dyer-cli-0.6.0 (c (n "dyer-cli") (v "0.6.0") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "1vjwc64z6r6ikv4m32q2vyrcz0rh2jpd3zrdx4ldr11h827v4vpn")))

