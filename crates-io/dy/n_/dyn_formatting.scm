(define-module (crates-io dy n_ dyn_formatting) #:use-module (crates-io))

(define-public crate-dyn_formatting-1.0.0 (c (n "dyn_formatting") (v "1.0.0") (h "0gz93v9zid9z53y0cp2w7zwpyd12c1njb8hi4qsvyk0x0rfn6dvi") (y #t)))

(define-public crate-dyn_formatting-1.0.1 (c (n "dyn_formatting") (v "1.0.1") (h "0nqwilz5k650k4r2ydszg037c5dxv7w9z45s13riih7hnkqwl0hz") (y #t)))

(define-public crate-dyn_formatting-2.0.0 (c (n "dyn_formatting") (v "2.0.0") (h "04cf4zicwj4dqgnnnz9fqwkgwk2gvh3rdfxpd09vaj64cs63f1l6")))

(define-public crate-dyn_formatting-3.0.0 (c (n "dyn_formatting") (v "3.0.0") (h "1b5i4dgprahgk9rjybj4hp5z983cy3dzvw0dlp94z1x3rj7n605j")))

