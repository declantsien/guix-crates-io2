(define-module (crates-io dy n_ dyn_safe) #:use-module (crates-io))

(define-public crate-dyn_safe-0.0.1 (c (n "dyn_safe") (v "0.0.1") (d (list (d (n "proc_macros") (r "^0.0.1") (d #t) (k 0) (p "dyn_safe-proc_macros")))) (h "1xsqyxkyzjawvirdmr5ld9m6k7nzvk7q7lnkwnsmlpsa7l0l7d6l")))

(define-public crate-dyn_safe-0.0.2 (c (n "dyn_safe") (v "0.0.2") (d (list (d (n "proc_macros") (r "^0.0.2") (d #t) (k 0) (p "dyn_safe-proc_macros")))) (h "1xrfb111w7dp291jnkrgyfb0pvhhvm6j55yzrir50j4rn0jnfabz")))

(define-public crate-dyn_safe-0.0.3 (c (n "dyn_safe") (v "0.0.3") (d (list (d (n "proc_macros") (r "^0.0.3") (d #t) (k 0) (p "dyn_safe-proc_macros")))) (h "0fnkkwqal4pf5j1byvc1m2zdbx3qycm5f3xhrzhwh0akh1xf7m82")))

(define-public crate-dyn_safe-0.0.4 (c (n "dyn_safe") (v "0.0.4") (d (list (d (n "proc_macros") (r "^0.0.4") (d #t) (k 0) (p "dyn_safe-proc_macros")))) (h "0dbc68b5xpp9i9an96ardd8wag500731mxdl12rgii2343l18bn6")))

