(define-module (crates-io dy n_ dyn_safe-proc_macros) #:use-module (crates-io))

(define-public crate-dyn_safe-proc_macros-0.0.1 (c (n "dyn_safe-proc_macros") (v "0.0.1") (h "15zz24m57vvznlgra1rvnn60liha7vgr7dbzyfkiv8hbqd492rd5")))

(define-public crate-dyn_safe-proc_macros-0.0.2 (c (n "dyn_safe-proc_macros") (v "0.0.2") (h "19lmbhcvkfmx9bb9200n3c16yxb2xf3w7hvb7aww3xlgnfqxcc8p")))

(define-public crate-dyn_safe-proc_macros-0.0.3 (c (n "dyn_safe-proc_macros") (v "0.0.3") (h "00ywwjydj4m17qiv116almrkfphfy39iicsaz1563f3k5sgcql03")))

(define-public crate-dyn_safe-proc_macros-0.0.4 (c (n "dyn_safe-proc_macros") (v "0.0.4") (h "00hc9ijanpg5h1ljp2cr55ks6wh3qf7ysxlg4hcpbcixrz0xyqrw")))

