(define-module (crates-io dy n_ dyn_struct_derive2) #:use-module (crates-io))

(define-public crate-dyn_struct_derive2-0.1.0 (c (n "dyn_struct_derive2") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.30") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.80") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0pwlilfwdnmnrcdwblqzrfnn9ixbkaxlpfl5cqc3sfcdl9d937gg")))

