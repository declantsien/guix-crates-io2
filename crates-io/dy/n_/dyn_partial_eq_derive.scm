(define-module (crates-io dy n_ dyn_partial_eq_derive) #:use-module (crates-io))

(define-public crate-dyn_partial_eq_derive-0.1.0 (c (n "dyn_partial_eq_derive") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.72") (f (quote ("full"))) (d #t) (k 0)))) (h "10rglyafjriv9wrbpi23s5sfmx5gp5pn52lapfndfd5wamxx399k")))

(define-public crate-dyn_partial_eq_derive-0.1.1 (c (n "dyn_partial_eq_derive") (v "0.1.1") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.72") (f (quote ("full"))) (d #t) (k 0)))) (h "15y7va87gvvwpk069vncxbczs8q2w728la7fqbkhx0dmy6fmxb3z")))

(define-public crate-dyn_partial_eq_derive-0.1.2 (c (n "dyn_partial_eq_derive") (v "0.1.2") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.72") (f (quote ("full"))) (d #t) (k 0)))) (h "1gg8r12y0x8yjlidsvjhmy72n69rsf4ladb2hswgksrm2in7q88f")))

(define-public crate-dyn_partial_eq_derive-0.1.3 (c (n "dyn_partial_eq_derive") (v "0.1.3") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.72") (f (quote ("full"))) (d #t) (k 0)))) (h "0qi1jj3rj099xydf6kqfjzbhw4wbqs6gmjnl51s7pjcb7snlhzyj") (f (quote (("std") ("alloc"))))))

