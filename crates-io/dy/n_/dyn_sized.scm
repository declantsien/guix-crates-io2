(define-module (crates-io dy n_ dyn_sized) #:use-module (crates-io))

(define-public crate-dyn_sized-0.1.0 (c (n "dyn_sized") (v "0.1.0") (d (list (d (n "fn_move") (r "^0.1") (d #t) (k 0)))) (h "190dwj2sl0sr9apmac0xcxzpx86imqz2zf2ahsk0g8xlsiji59rk")))

(define-public crate-dyn_sized-0.2.0 (c (n "dyn_sized") (v "0.2.0") (d (list (d (n "fn_move") (r "^0.1") (d #t) (k 0)))) (h "0gn1v0k9clfkvr5gdi4xjbmy052mafd9hxh0abxx45gb74p9bfv4")))

