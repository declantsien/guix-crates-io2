(define-module (crates-io dy n_ dyn_size_of) #:use-module (crates-io))

(define-public crate-dyn_size_of-0.1.0 (c (n "dyn_size_of") (v "0.1.0") (h "18wjv9z05vr4dpfngw61qrs5zr49l10fri28vm2giysyrxz6jaqz")))

(define-public crate-dyn_size_of-0.1.1 (c (n "dyn_size_of") (v "0.1.1") (h "0jy8113nmhps10khlq0h54xjr4fm7gc1y2jfdvf9clvbqk53g1aq")))

(define-public crate-dyn_size_of-0.2.0 (c (n "dyn_size_of") (v "0.2.0") (h "01qzspyfr848lvf5q4km2vx8skyb4b3qmjdv6c0jpyvzz5myg39b")))

(define-public crate-dyn_size_of-0.2.1 (c (n "dyn_size_of") (v "0.2.1") (h "1mwdm6r3y8ir9kyljsr3pnq6slszz9w8qqcywrq2kqfxcmicymsq")))

(define-public crate-dyn_size_of-0.2.2 (c (n "dyn_size_of") (v "0.2.2") (h "01vdfx9wn1syndjcc8806j2h6p5a3viz3z5gbj5mb1z1nl23aba4")))

(define-public crate-dyn_size_of-0.3.0 (c (n "dyn_size_of") (v "0.3.0") (h "1cf8fwhxqi3rb15vxj1kkajldy491zhi6c2yd5h15ac3ck8bj9wb") (y #t)))

(define-public crate-dyn_size_of-0.4.0 (c (n "dyn_size_of") (v "0.4.0") (h "06gmdi272mizh3d22jypwhnls02b3w8kqslip36f9z33azmqm2rb")))

(define-public crate-dyn_size_of-0.4.1 (c (n "dyn_size_of") (v "0.4.1") (h "1iq1y16y70jakmg33082x1283ybbsn7jdgcv6vhqmwgf577dr2kc")))

(define-public crate-dyn_size_of-0.4.2 (c (n "dyn_size_of") (v "0.4.2") (d (list (d (n "aligned-vec") (r "^0.5") (o #t) (d #t) (k 0)))) (h "0d2iimxcvjipi6jbvcxhcv1p7xx2gdhaznmgijzkbv5i825ggm1k")))

