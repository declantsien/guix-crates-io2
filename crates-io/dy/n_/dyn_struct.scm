(define-module (crates-io dy n_ dyn_struct) #:use-module (crates-io))

(define-public crate-dyn_struct-0.1.0 (c (n "dyn_struct") (v "0.1.0") (d (list (d (n "dyn_struct_derive") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "0607cnxjnxmk089b4li9sj95bxm187i6bvcjdcyg4mkbqinhdpks") (f (quote (("derive" "dyn_struct_derive") ("default" "derive"))))))

(define-public crate-dyn_struct-0.2.0 (c (n "dyn_struct") (v "0.2.0") (d (list (d (n "dyn_struct_derive") (r "^0.2.0") (o #t) (d #t) (k 0)))) (h "0m9vxbmgfx521wvkvh75n240v1jp2w747nyp616pwa36q7b2y674") (f (quote (("derive" "dyn_struct_derive") ("default" "derive"))))))

(define-public crate-dyn_struct-0.3.0 (c (n "dyn_struct") (v "0.3.0") (d (list (d (n "dyn_struct_derive") (r "^0.3.0") (o #t) (d #t) (k 0)))) (h "1bg4y1ywldylwh0yrr8ghl3fy96d3mh13641x2cn6ffdbhk57qgv") (f (quote (("derive" "dyn_struct_derive") ("default" "derive")))) (y #t)))

(define-public crate-dyn_struct-0.3.1 (c (n "dyn_struct") (v "0.3.1") (d (list (d (n "dyn_struct_derive") (r "^0.3.0") (o #t) (d #t) (k 0)))) (h "0z1bn1rib3w4c3g18c1wbm8nbs2biasjfwgykjlh521y5d3d25mj") (f (quote (("derive" "dyn_struct_derive") ("default" "derive")))) (y #t)))

(define-public crate-dyn_struct-0.3.2 (c (n "dyn_struct") (v "0.3.2") (d (list (d (n "dyn_struct_derive") (r "^0.3.0") (o #t) (d #t) (k 0)))) (h "1vcq8xsfydc62syp0l9fxqgcx4py3jb3bzfbxpb45pd4alqvawrk") (f (quote (("derive" "dyn_struct_derive") ("default" "derive"))))))

