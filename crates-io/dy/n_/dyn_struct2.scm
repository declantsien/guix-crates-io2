(define-module (crates-io dy n_ dyn_struct2) #:use-module (crates-io))

(define-public crate-dyn_struct2-0.1.0 (c (n "dyn_struct2") (v "0.1.0") (d (list (d (n "dyn_struct_derive2") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "transmute") (r "^0.1.1") (d #t) (k 0)))) (h "1n2wa4ajsgac56vqcf32c05jcmpdl2znyp9yvgiq4fd9rxp6wiin") (f (quote (("derive" "dyn_struct_derive2"))))))

