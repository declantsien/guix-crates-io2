(define-module (crates-io dy n_ dyn_ord) #:use-module (crates-io))

(define-public crate-dyn_ord-0.1.0 (c (n "dyn_ord") (v "0.1.0") (h "1pbv88q3d6hb8vqn8v551yzmc36ssdz31rph4z8mb4qqah11aac7")))

(define-public crate-dyn_ord-0.2.0 (c (n "dyn_ord") (v "0.2.0") (h "14z1ddi569a5v17s82v5pg0a6y0k487dmih36b79jpf5by1sni3d")))

(define-public crate-dyn_ord-0.2.1 (c (n "dyn_ord") (v "0.2.1") (h "0b1sfjm2cncbmpff61hk5jfj1y4dxy9klwqdmf2l9r6nph0cjm4x")))

