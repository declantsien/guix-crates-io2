(define-module (crates-io dy n_ dyn_struct_derive) #:use-module (crates-io))

(define-public crate-dyn_struct_derive-0.1.0 (c (n "dyn_struct_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.30") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.80") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0skx3qi2xg511mhz761h78n8iccmp49wh2isqb1d7hli217s30ck")))

(define-public crate-dyn_struct_derive-0.2.0 (c (n "dyn_struct_derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.30") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.80") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "11lim8s8bswcq2mwvpi0fg72678qwvm7ly08nc2xksynr6gzj5ln")))

(define-public crate-dyn_struct_derive-0.3.0 (c (n "dyn_struct_derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0.30") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.80") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0rm7kympka0w9kdrf8synfnrxyhbac32v6fy1xs93h9w11kf12w2")))

