(define-module (crates-io dy n_ dyn_object) #:use-module (crates-io))

(define-public crate-dyn_object-0.1.0 (c (n "dyn_object") (v "0.1.0") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)))) (h "1878ybqpnfzwvg8valbvmx42rsipfm74nl4f6yvww2szqk48hzvq") (y #t)))

(define-public crate-dyn_object-0.2.0 (c (n "dyn_object") (v "0.2.0") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)))) (h "0yrsc8fn6q7xgcrmjmaxipfq7dl447v3j1krpc8sx24h6g1fawb6")))

