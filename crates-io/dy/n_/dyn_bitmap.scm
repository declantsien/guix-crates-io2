(define-module (crates-io dy n_ dyn_bitmap) #:use-module (crates-io))

(define-public crate-dyn_bitmap-0.0.1 (c (n "dyn_bitmap") (v "0.0.1") (d (list (d (n "thiserror") (r "^1.0.21") (d #t) (k 0)))) (h "13kk7fah2lzrvjhq5m5jc952l2jv7jig7jvm54kzr6b68bqyaz6q")))

(define-public crate-dyn_bitmap-0.0.2 (c (n "dyn_bitmap") (v "0.0.2") (d (list (d (n "thiserror") (r "^1.0.21") (d #t) (k 0)))) (h "02nmb0m1px38cafqipp4hs2d6ni2c2b52wwhzv0880hzas08fvxg")))

(define-public crate-dyn_bitmap-0.1.0 (c (n "dyn_bitmap") (v "0.1.0") (d (list (d (n "thiserror") (r "^1.0.21") (d #t) (k 0)))) (h "0x3cqkfy18csphwq2vx2456kx609y8i58im9cvy3fa0bwl5nzi6l")))

(define-public crate-dyn_bitmap-0.3.0 (c (n "dyn_bitmap") (v "0.3.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1x6bnk0fammdwbwpp84b1k9irv2xvd7d0yl1kr6igjrlwibp777a") (y #t)))

(define-public crate-dyn_bitmap-0.3.1 (c (n "dyn_bitmap") (v "0.3.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0g79k6i3l76l88i2axm9i1zs02v5z9si89v9xi4p3ql4qg4nrgk2") (y #t)))

(define-public crate-dyn_bitmap-0.3.2 (c (n "dyn_bitmap") (v "0.3.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "07n6frb0627ysw0c76dcfkyjp652fvr5jk3m4jmc3w66ilwkwv8v")))

