(define-module (crates-io dy nc dyncast) #:use-module (crates-io))

(define-public crate-dyncast-0.0.1 (c (n "dyncast") (v "0.0.1") (h "1sib57lhl1xr9wrcih3blngdv6ywj7jkkz4jjjy8dzhnfswkj3n9")))

(define-public crate-dyncast-0.1.0 (c (n "dyncast") (v "0.1.0") (d (list (d (n "dyncast-impl") (r "=0.1.0") (d #t) (k 0)) (d (n "sptr") (r "^0.3.2") (d #t) (k 0)))) (h "1p6f0xc5smqxdkzygyfhmbg1l7fjw98rncfgnzrj4rglpk86cvfz") (r "1.74")))

