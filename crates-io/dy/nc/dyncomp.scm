(define-module (crates-io dy nc dyncomp) #:use-module (crates-io))

(define-public crate-dyncomp-0.1.0 (c (n "dyncomp") (v "0.1.0") (d (list (d (n "clap") (r "^3.0") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "dirs") (r "^4.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.16") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "shlex") (r "^1.1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.3") (d #t) (k 0)))) (h "0a73fy6sw0p74n5rnhff1pimw5xmfph5rbyizs6rxfgfhz75cp68")))

(define-public crate-dyncomp-0.1.1 (c (n "dyncomp") (v "0.1.1") (d (list (d (n "clap") (r "^3.0") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "dirs") (r "^4.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.16") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "shlex") (r "^1.1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.3") (d #t) (k 0)))) (h "1bn3navsad6qkhcmjlbjq2j8ljybhgv387aih2pg0fk3iz0j0070")))

