(define-module (crates-io dy nj dynja) #:use-module (crates-io))

(define-public crate-dynja-0.1.0 (c (n "dynja") (v "0.1.0") (d (list (d (n "askama") (r "^0.12") (d #t) (k 0)) (d (n "dynja_derive") (r "^0.1") (d #t) (k 0)) (d (n "minijinja") (r "^1.0") (f (quote ("loader"))) (d #t) (k 0)))) (h "0b1kbs24s8q1xb0hxg20xxa71fpw9q1xakqi9sawx33wbyzz51b0")))

(define-public crate-dynja-0.2.0 (c (n "dynja") (v "0.2.0") (d (list (d (n "askama") (r "^0.12") (d #t) (k 0)) (d (n "dynja_derive") (r "^0.1") (d #t) (k 0)) (d (n "minijinja") (r "^1.0") (f (quote ("loader"))) (d #t) (k 0)))) (h "0s6j3knq2ab0rvm1hms061f04z969nx1xxdd575vfcvnbjxv7rxr")))

(define-public crate-dynja-0.2.1 (c (n "dynja") (v "0.2.1") (d (list (d (n "askama") (r "^0.12") (d #t) (k 0)) (d (n "dynja_derive") (r "^0.2") (d #t) (k 0)) (d (n "minijinja") (r "^1.0") (f (quote ("loader"))) (d #t) (k 0)))) (h "1sldzga7qdhkh2yja3dma3xrybv7ah18zbfb0a982qg7klgy3zyz")))

(define-public crate-dynja-0.3.0 (c (n "dynja") (v "0.3.0") (d (list (d (n "askama") (r "^0.12") (d #t) (k 0)) (d (n "dynja_derive") (r "^0.3") (d #t) (k 0)) (d (n "minijinja") (r "^1.0") (f (quote ("loader"))) (d #t) (k 0)))) (h "1h6gqksvd0n4h4x4a7djp1fn9c8dwkampgsnmydgj4r08ag0k8lj")))

(define-public crate-dynja-0.3.1 (c (n "dynja") (v "0.3.1") (d (list (d (n "askama") (r "^0.12") (d #t) (k 0)) (d (n "dynja_derive") (r "^0.3") (d #t) (k 0)) (d (n "minijinja") (r "^1.0") (f (quote ("loader"))) (d #t) (k 0)))) (h "0a8pxcn99f1v9im5r7dn23wvy56k2s5q8dwqmf08ccvhxb5qi3xc")))

(define-public crate-dynja-0.4.0 (c (n "dynja") (v "0.4.0") (d (list (d (n "askama") (r "^0.12") (o #t) (d #t) (k 0)) (d (n "dynja_derive") (r "^0.4") (d #t) (k 0)) (d (n "minijinja") (r "^1.0") (f (quote ("loader"))) (d #t) (k 0)))) (h "0jmaqnizb78zy0x25lz5k18p9xk778dym851pk48b1n2hn60d1zk") (f (quote (("default")))) (s 2) (e (quote (("askama_release" "dep:askama"))))))

(define-public crate-dynja-0.4.1 (c (n "dynja") (v "0.4.1") (d (list (d (n "askama") (r "^0.12") (o #t) (d #t) (k 0)) (d (n "dynja_derive") (r "^0.4") (d #t) (k 0)) (d (n "minijinja") (r "^1.0") (f (quote ("loader"))) (d #t) (k 0)))) (h "0nwjcf0yg1y4digbf04p63ghh21hayyqqpm8jf275xz0k3mgbnii") (f (quote (("default")))) (s 2) (e (quote (("askama_release" "dep:askama"))))))

