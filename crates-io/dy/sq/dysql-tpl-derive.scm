(define-module (crates-io dy sq dysql-tpl-derive) #:use-module (crates-io))

(define-public crate-dysql-tpl-derive-0.17.2 (c (n "dysql-tpl-derive") (v "0.17.2") (d (list (d (n "bae") (r "^0.1.7") (d #t) (k 0)) (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "108lrmv0g7cp3n3s5bf0kdwldm2mmsmxmvg8cwv5wsvxdkzhgwly")))

(define-public crate-dysql-tpl-derive-2.0.0 (c (n "dysql-tpl-derive") (v "2.0.0") (d (list (d (n "bae2") (r "^1.0") (d #t) (k 0)) (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "10iasw9hli1h05ai8dwljnagbs712z80xz64g1v23n52iz0sq5kc")))

