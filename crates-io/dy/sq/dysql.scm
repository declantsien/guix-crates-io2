(define-module (crates-io dy sq dysql) #:use-module (crates-io))

(define-public crate-dysql-0.1.0 (c (n "dysql") (v "0.1.0") (d (list (d (n "once_cell") (r "^1.13") (d #t) (k 0)) (d (n "ramhorns") (r "^0.14") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)))) (h "0z79cwgznzm5xg961ck1pw1sl2qr6f6dbjngfmw0zvvzg60bkx7p")))

(define-public crate-dysql-0.1.1 (c (n "dysql") (v "0.1.1") (d (list (d (n "once_cell") (r "^1.13") (d #t) (k 0)) (d (n "ramhorns") (r "^0.14") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)))) (h "1dr83xq5wbyav1fdpcnhgj30lij1qf4shb5j9yr7f25hghsfmi6s")))

(define-public crate-dysql-0.1.2 (c (n "dysql") (v "0.1.2") (d (list (d (n "once_cell") (r "^1.13") (d #t) (k 0)) (d (n "ramhorns") (r "^0.14") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)))) (h "1b7k81c7p7l9yyw6g4issm7721rb928q7ccm5z4h22l1v5sym0yr")))

(define-public crate-dysql-0.2.0 (c (n "dysql") (v "0.2.0") (d (list (d (n "once_cell") (r "^1.13") (d #t) (k 0)) (d (n "ramhorns") (r "^0.14") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)))) (h "046xnmds3lh8yd3fn2hrgrmi0mcybbzxl03k1lxn6iwsbxm77p4b")))

(define-public crate-dysql-0.3.0 (c (n "dysql") (v "0.3.0") (d (list (d (n "once_cell") (r "^1.13") (d #t) (k 0)) (d (n "ramhorns") (r "^0.14") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)))) (h "0i8ra9iahsvqcxf3wgi46bwibmy7yc50zvg24cd83hmnb4gbx8pf")))

(define-public crate-dysql-0.3.1 (c (n "dysql") (v "0.3.1") (d (list (d (n "once_cell") (r "^1.13") (d #t) (k 0)) (d (n "ramhorns") (r "^0.14") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)))) (h "13q8abmgp6ipz5p83q54zn7m25644p5225sczzhanazf7b629bkd")))

(define-public crate-dysql-0.4.0 (c (n "dysql") (v "0.4.0") (d (list (d (n "once_cell") (r "^1.13") (d #t) (k 0)) (d (n "ramhorns") (r "^0.14") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)))) (h "0pn6z2x6j5jqa38f9ka4ky5f6ikzb8xav9g2fpxy0728wk35yxyy")))

(define-public crate-dysql-0.4.1 (c (n "dysql") (v "0.4.1") (d (list (d (n "once_cell") (r "^1.13") (d #t) (k 0)) (d (n "ramhorns") (r "^0.14") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)))) (h "19mdd7gxfibxqqv8njsxmdq80clnm3q8g9l59j7phb0jx84a4sd3")))

(define-public crate-dysql-0.4.2 (c (n "dysql") (v "0.4.2") (d (list (d (n "once_cell") (r "^1.13") (d #t) (k 0)) (d (n "ramhorns") (r "^0.14") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)))) (h "0rjq8mwgkavv5l17gz4hyb58naprm4xwlb34ddckcjdadbxpjba3")))

(define-public crate-dysql-0.4.3 (c (n "dysql") (v "0.4.3") (d (list (d (n "once_cell") (r "^1.13") (d #t) (k 0)) (d (n "ramhorns") (r "^0.14") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)))) (h "0pr18pyy8xf3wryxxlisr72v322cq2ghhz4rlyp2d34cq1f1lq41")))

(define-public crate-dysql-0.4.4 (c (n "dysql") (v "0.4.4") (d (list (d (n "once_cell") (r "^1.13") (d #t) (k 0)) (d (n "ramhorns") (r "^0.14") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)))) (h "1rmsha0v7gs0vg175h0f889rfy2abialc8a52gdp8hmy0fi3g92k")))

(define-public crate-dysql-0.5.0 (c (n "dysql") (v "0.5.0") (d (list (d (n "once_cell") (r "^1.13") (d #t) (k 0)) (d (n "ramhorns") (r "^0.14") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)))) (h "0p0giks0fp7bqvmfcbcdsp19wsnk8d0cbf1vp5r0gjz4yf84i47c")))

(define-public crate-dysql-0.6.0 (c (n "dysql") (v "0.6.0") (d (list (d (n "once_cell") (r "^1.13") (d #t) (k 0)) (d (n "ramhorns") (r "^0.14") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1i7j1rczw26j3l2l3khz8vg77nqb47hh4vlw9madw60ga1svpp4k")))

(define-public crate-dysql-0.7.0 (c (n "dysql") (v "0.7.0") (d (list (d (n "once_cell") (r "^1.13") (d #t) (k 0)) (d (n "ramhorns") (r "^0.14") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0na3ifam3y46nbxzrk2fzvwg6y9b7fw0a6nkd9fv54b8ginslhq4")))

(define-public crate-dysql-0.8.0 (c (n "dysql") (v "0.8.0") (d (list (d (n "once_cell") (r "^1.13") (d #t) (k 0)) (d (n "ramhorns") (r "^0.14") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0hy2yzx9gkq6sbjl1ym1p3vss7n4y3wsykl9k4l36lg52r15wp6v")))

(define-public crate-dysql-0.8.1 (c (n "dysql") (v "0.8.1") (d (list (d (n "once_cell") (r "^1.13") (d #t) (k 0)) (d (n "ramhorns") (r "^0.14") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1apscxv1xy5xh4axsdmfyk9qa3dg7pihmi1sfpkw12rz2n7yga36")))

(define-public crate-dysql-0.8.2 (c (n "dysql") (v "0.8.2") (d (list (d (n "once_cell") (r "^1.13") (d #t) (k 0)) (d (n "ramhorns") (r "^0.14") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1bpsnjyi48zn97ir6xxcvpjh6jhqpc4s7mgbjj8i9jng4497pnda")))

(define-public crate-dysql-0.8.3 (c (n "dysql") (v "0.8.3") (d (list (d (n "once_cell") (r "^1.13") (d #t) (k 0)) (d (n "ramhorns") (r "^0.14") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0vxx5ziwylfkph145pzvsf7rqjzcd22hdgzyj8s47ldck9dawb7x")))

(define-public crate-dysql-0.8.4 (c (n "dysql") (v "0.8.4") (d (list (d (n "once_cell") (r "^1.13") (d #t) (k 0)) (d (n "ramhorns") (r "^0.14") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0c3m82rzhvw4ai1zg4mkhgic3g54banf4l2nsxbb4kabjb9syhj7")))

(define-public crate-dysql-0.8.5 (c (n "dysql") (v "0.8.5") (d (list (d (n "once_cell") (r "^1.13") (d #t) (k 0)) (d (n "ramhorns") (r "^0.14") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0z7nqfagbr70aj1az72xbd31gbkz5z4vih5bq95zjgdyw7m9gk08")))

(define-public crate-dysql-0.8.6 (c (n "dysql") (v "0.8.6") (d (list (d (n "once_cell") (r "^1.13") (d #t) (k 0)) (d (n "ramhorns") (r "^0.14") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1ji3zkzdrnrgh9hc3j6bavaabb6xp8pxgqwfll00c3nqlbrx1ml8")))

(define-public crate-dysql-0.8.7 (c (n "dysql") (v "0.8.7") (d (list (d (n "once_cell") (r "^1.13") (d #t) (k 0)) (d (n "ramhorns") (r "^0.14") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1y8w8rpbi99j4b3vray2d24yh71aqsv7042hzkm475dvc1lcsrjc")))

(define-public crate-dysql-0.8.8 (c (n "dysql") (v "0.8.8") (d (list (d (n "once_cell") (r "^1.13") (d #t) (k 0)) (d (n "ramhorns") (r "^0.14") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0vg4xfdgyyp5rr83411n81dpyrqyjy0kjmcmngw1b76lp6cvj86m")))

(define-public crate-dysql-0.8.9 (c (n "dysql") (v "0.8.9") (d (list (d (n "once_cell") (r "^1.13") (d #t) (k 0)) (d (n "ramhorns") (r "^0.14") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1xvda095y4w2r37zvbsr6icgwwkfpps5lnpv7jf563jxcqsv4rwp")))

(define-public crate-dysql-0.8.10 (c (n "dysql") (v "0.8.10") (d (list (d (n "once_cell") (r "^1.13") (d #t) (k 0)) (d (n "ramhorns") (r "^0.14") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0nqxwmmyg64vx1797lknsbvwwqcg2vnc7r20j0d6937947i5h3hb")))

(define-public crate-dysql-0.8.11 (c (n "dysql") (v "0.8.11") (d (list (d (n "once_cell") (r "^1.13") (d #t) (k 0)) (d (n "ramhorns-ext") (r "^0.14") (f (quote ("chrono" "uuid"))) (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0gp4jnwk6gaajal9dqqfrjpiqrqs1phcl41p6k3mll26hmc5wj7x")))

(define-public crate-dysql-0.8.13 (c (n "dysql") (v "0.8.13") (d (list (d (n "once_cell") (r "^1.13") (d #t) (k 0)) (d (n "ramhorns-ext") (r "^0.16") (f (quote ("chrono" "uuid"))) (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "023cg9fy8cx203a2lyjipvhxk238a8v1fcqb22vyqbxh2vwdvnv4")))

(define-public crate-dysql-0.9.0 (c (n "dysql") (v "0.9.0") (d (list (d (n "once_cell") (r "^1.13") (d #t) (k 0)) (d (n "ramhorns-ext") (r "^0.17") (f (quote ("chrono" "uuid"))) (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "10sj717yczrawkgv40mvmifmz9crnrcq9w9njir88p11rwmazi5f")))

(define-public crate-dysql-0.10.0 (c (n "dysql") (v "0.10.0") (d (list (d (n "dysql-core") (r "^0.10") (d #t) (k 0)) (d (n "dysql-macro") (r "^0.10") (d #t) (k 0)) (d (n "once_cell") (r "^1.13") (d #t) (k 0)) (d (n "ramhorns-ext") (r "^0.17") (f (quote ("chrono" "uuid"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0vj13ffp051ddf7jhqd04n0d6j9v9canjzfc7i8qyg0yw5z0ijmw")))

(define-public crate-dysql-0.11.0 (c (n "dysql") (v "0.11.0") (d (list (d (n "dysql-core") (r "^0.10") (d #t) (k 0)) (d (n "dysql-macro") (r "^0.10") (d #t) (k 0)) (d (n "dysql-tpl") (r "^0.17") (f (quote ("chrono" "uuid"))) (d #t) (k 0)) (d (n "once_cell") (r "^1.13") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0svavbj7cc0b63qas64zr7lx3srxb7qjjjig2rlshgyxgigjavbq")))

(define-public crate-dysql-2.0.0 (c (n "dysql") (v "2.0.0") (d (list (d (n "dysql-core") (r "^2") (d #t) (k 0)) (d (n "dysql-macro") (r "^2") (d #t) (k 0)) (d (n "dysql-tpl") (r "^2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1kgagcn4qh7p1zn6dqryccymrgszwz1gkx5hmm9k7idr72ayansp") (f (quote (("tokio-postgres" "dysql-core/tokio-postgres" "dysql-macro/tokio-postgres") ("sqlx-sqlite" "dysql-core/sqlx-sqlite" "dysql-macro/sqlx") ("sqlx-postgres" "dysql-core/sqlx-postgres" "dysql-macro/sqlx") ("sqlx-mysql" "dysql-core/sqlx-mysql" "dysql-macro/sqlx") ("rbatis-sqlite" "dysql-core/rbatis-sqlite") ("rbatis-pg" "dysql-core/rbatis-pg") ("rbatis-mysql" "dysql-core/rbatis-mysql") ("default"))))))

