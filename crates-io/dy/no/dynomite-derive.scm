(define-module (crates-io dy no dynomite-derive) #:use-module (crates-io))

(define-public crate-dynomite-derive-0.1.0 (c (n "dynomite-derive") (v "0.1.0") (d (list (d (n "dynomite") (r "^0.0.0") (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "rusoto_core") (r "^0") (d #t) (k 2)) (d (n "rusoto_dynamodb") (r "^0") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "195wnmd1805g8mf06mgza9rs7pnl3yp23xibvz0qfai4v6g7hdsj")))

(define-public crate-dynomite-derive-0.1.1 (c (n "dynomite-derive") (v "0.1.1") (d (list (d (n "dynomite") (r "^0.0.0") (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "rusoto_core") (r "^0") (d #t) (k 2)) (d (n "rusoto_dynamodb") (r "^0") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)) (d (n "uuid") (r "^0.6") (f (quote ("v4"))) (d #t) (k 2)))) (h "1jp6nd35wb8m2li8j1kznin4nr0i935na5s6jbc8rfj1w81gapq5")))

(define-public crate-dynomite-derive-0.1.2 (c (n "dynomite-derive") (v "0.1.2") (d (list (d (n "dynomite") (r "^0.1") (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "rusoto_core") (r "^0") (d #t) (k 2)) (d (n "rusoto_dynamodb") (r "^0") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)) (d (n "uuid") (r "^0.6") (f (quote ("v4"))) (d #t) (k 2)))) (h "1ql5ziiihikbgi9s4p7k88sk0a9llfhasf6jdnqp2iqar66p6vx5")))

(define-public crate-dynomite-derive-0.1.3 (c (n "dynomite-derive") (v "0.1.3") (d (list (d (n "dynomite") (r "^0.1") (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "rusoto_core") (r "^0") (d #t) (k 2)) (d (n "rusoto_dynamodb") (r "^0") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)) (d (n "uuid") (r "^0.6") (f (quote ("v4"))) (d #t) (k 2)))) (h "14aiw4pf81zjrkhz22fyy16mx7w3dsa3362i0gk7pk2bvfmbqi2n")))

(define-public crate-dynomite-derive-0.1.4 (c (n "dynomite-derive") (v "0.1.4") (d (list (d (n "dynomite") (r "^0.1.4") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 2)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "rusoto_core") (r "^0.34") (d #t) (k 2)) (d (n "rusoto_dynamodb") (r "^0.34") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 2)) (d (n "uuid") (r "^0.6") (f (quote ("v4"))) (d #t) (k 2)))) (h "0il9l1yl8h9chvclgx92kvqwiskdrx8ajk82acmwl46sli9pabdh")))

(define-public crate-dynomite-derive-0.1.5 (c (n "dynomite-derive") (v "0.1.5") (d (list (d (n "dynomite") (r "^0.1.5") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "rusoto_core") (r "^0.36") (d #t) (k 2)) (d (n "rusoto_dynamodb") (r "^0.36") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 2)) (d (n "uuid") (r "^0.7") (f (quote ("v4"))) (d #t) (k 2)))) (h "0yknd8r8kwl9121f8k7na8q900g3xhh0k4jgr3w5w8a852y2gnqp")))

(define-public crate-dynomite-derive-0.2.0 (c (n "dynomite-derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (d #t) (k 0)))) (h "0y5xs3y89cnsqzhkhj7gq9pbyif716dlbyfvqzas83gcdwmzzp0a")))

(define-public crate-dynomite-derive-0.2.1 (c (n "dynomite-derive") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "06wma2z02zr48sqbm352bx9nw5b9v65xni20wz7h9z00h12dj1a6")))

(define-public crate-dynomite-derive-0.3.0 (c (n "dynomite-derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "0hcj0s43sz8qmyajhldvba5p6i1r999iaadi1121dq6j1x2ax3ah")))

(define-public crate-dynomite-derive-0.4.0 (c (n "dynomite-derive") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "1dggj48i4sqdjxprgph534w2nci7rh4fly3j7d7n6bb02yyx5wz9")))

(define-public crate-dynomite-derive-0.4.1 (c (n "dynomite-derive") (v "0.4.1") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "0fnh5v54fwknij0n16zgzyycx9q7431sh3658h0cy7jgckj8n7ki")))

(define-public crate-dynomite-derive-0.5.0 (c (n "dynomite-derive") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "154s8ac9q1xznld8jdhi896jzzdd86zsh7q82nyxjyvmm27mbd2c")))

(define-public crate-dynomite-derive-0.5.1 (c (n "dynomite-derive") (v "0.5.1") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "0sj31l81wmppaj85x3z0ky0qqfczfxas4ja12m3lm4h0vs8p57j2")))

(define-public crate-dynomite-derive-0.5.2 (c (n "dynomite-derive") (v "0.5.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1qipzwfw2hxra0isqk99g2kpslrjvmyyhpgjypzb7a1q4l37vzmz")))

(define-public crate-dynomite-derive-0.6.0 (c (n "dynomite-derive") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0wv2c53r9q6wr84hz8ilh2f5dfi1vzklgzq8xsgjww1qjcdhpp5x")))

(define-public crate-dynomite-derive-0.7.0 (c (n "dynomite-derive") (v "0.7.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "00bzyg4j917m2m4m1v4y9qp29qi2bbpzn9c6fybglxvvcg51m8hl")))

(define-public crate-dynomite-derive-0.8.0 (c (n "dynomite-derive") (v "0.8.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "19jcp9n0r5s3r4xs6k370dn3m7fyrhbaa16bglwbp2jdkgay0svw")))

(define-public crate-dynomite-derive-0.8.1 (c (n "dynomite-derive") (v "0.8.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "149ysrg7mq3i2015z22yvi0whw1qyf0xyp5pgvypgra091ybj1r3")))

(define-public crate-dynomite-derive-0.8.2 (c (n "dynomite-derive") (v "0.8.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0nv0zmr9s7zx9wba1l00wa024iydyyb365kjpml87i65sa6ldkpn")))

(define-public crate-dynomite-derive-0.9.0 (c (n "dynomite-derive") (v "0.9.0") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "13zvab1307b6xqmywnkgymynjfgrxmq065xn4g13wwv1xzl2k2pr")))

(define-public crate-dynomite-derive-0.10.0 (c (n "dynomite-derive") (v "0.10.0") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "109mh7fk1jin6g9l21sd5lyc7fvpvz7dh10ak6f8sjp2kbvjic7l")))

