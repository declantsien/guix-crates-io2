(define-module (crates-io dy no dynoxide) #:use-module (crates-io))

(define-public crate-dynoxide-0.0.1 (c (n "dynoxide") (v "0.0.1") (d (list (d (n "csv") (r "^1.2.0") (d #t) (k 0)) (d (n "kml") (r "^0.7.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)))) (h "12xj0vy5d6lxmm6n595sys9562qfjbhsghsjf4ckn178s7q04pph")))

