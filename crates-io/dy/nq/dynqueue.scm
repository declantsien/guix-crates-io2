(define-module (crates-io dy nq dynqueue) #:use-module (crates-io))

(define-public crate-dynqueue-0.1.0 (c (n "dynqueue") (v "0.1.0") (d (list (d (n "rayon") (r "^1.3.0") (d #t) (k 0)))) (h "04k8zxjgf22qycmpvxw684bxxnqizyx5jbr0xyyy71cmip5mvsr0")))

(define-public crate-dynqueue-0.1.1 (c (n "dynqueue") (v "0.1.1") (d (list (d (n "rayon") (r "^1.3.0") (d #t) (k 0)))) (h "0pvmd9w7lb6q5083h5dlp3gwl1wwswfcaqpih0zknjrv4rd306gd")))

(define-public crate-dynqueue-0.1.2 (c (n "dynqueue") (v "0.1.2") (d (list (d (n "rayon") (r "^1.3.0") (d #t) (k 0)))) (h "0bblxcq71mh632yr7byq615bq9adsfxfqijmmf9kn5ix4dlxbh3s")))

(define-public crate-dynqueue-0.1.3 (c (n "dynqueue") (v "0.1.3") (d (list (d (n "crossbeam-queue") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "rayon") (r "^1.3.0") (d #t) (k 0)))) (h "1p8lakvsa3wz0v86c7763m2l07dbkhywlq03wqlay2y441jgg0jx")))

(define-public crate-dynqueue-0.2.0 (c (n "dynqueue") (v "0.2.0") (d (list (d (n "crossbeam-queue") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "rayon") (r "^1.3") (d #t) (k 0)))) (h "1ms42va5gd353mgl3m77myvi4d6p2ip4fn4zcq3ylpmnfdgq1mlp")))

(define-public crate-dynqueue-0.2.1 (c (n "dynqueue") (v "0.2.1") (d (list (d (n "crossbeam-queue") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "rayon") (r "^1.3") (d #t) (k 0)))) (h "1ck4ajb57k76cbvsnk31h8707sxf96jrk80b4fc5a29q8712r6ii")))

(define-public crate-dynqueue-0.3.0 (c (n "dynqueue") (v "0.3.0") (d (list (d (n "crossbeam-queue") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "rayon") (r "^1.3") (d #t) (k 0)))) (h "0ndxyspjn36q4fjvlbhd988n45cr2bvc8ycbzkhczgii1iswk6zz")))

