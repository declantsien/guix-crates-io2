(define-module (crates-io dy mo dymod) #:use-module (crates-io))

(define-public crate-dymod-0.1.0 (c (n "dymod") (v "0.1.0") (d (list (d (n "sharedlib") (r "~7.0.0") (d #t) (k 0)))) (h "1rng36n4z1ps3hx43bc8xrmsrziijfgdsf14l8pgci2drmx80kgn")))

(define-public crate-dymod-0.2.0 (c (n "dymod") (v "0.2.0") (d (list (d (n "libloading") (r "~0.5.0") (d #t) (k 0)))) (h "0ra9aj6gkg5bjcq9xnnzxr96pr93ixrvg3kbs96b65s8hii0rmdz") (f (quote (("force-static") ("force-dynamic") ("default" "auto-reload") ("auto-reload"))))))

(define-public crate-dymod-0.3.0 (c (n "dymod") (v "0.3.0") (d (list (d (n "libloading") (r "~0.5.0") (o #t) (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "0k8kqi1v813insbh6ammsy55srbhwl62dfj8ijyh5a2dn66ikdkc") (f (quote (("force-static") ("force-dynamic" "libloading") ("default" "auto-reload" "libloading") ("auto-reload"))))))

(define-public crate-dymod-0.4.0 (c (n "dymod") (v "0.4.0") (d (list (d (n "libloading") (r "^0.5") (o #t) (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "0yffdcxajhrrzd8v2c5ylr8b9cbwycc7bqp6rm52rkpxglmb6d29") (f (quote (("force-static") ("force-dynamic" "libloading") ("default" "auto-reload" "libloading") ("auto-reload"))))))

