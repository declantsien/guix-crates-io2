(define-module (crates-io dy nt dyntest) #:use-module (crates-io))

(define-public crate-dyntest-0.1.0 (c (n "dyntest") (v "0.1.0") (d (list (d (n "globset") (r "^0.4.14") (o #t) (d #t) (k 0)) (d (n "walkdir") (r "^2.5.0") (o #t) (d #t) (k 0)))) (h "0z5924y2c765bgrb4id9z47cgq3k3wld391z5xli7j6bxj2b8gzr") (f (quote (("default" "glob")))) (s 2) (e (quote (("glob" "dep:globset" "dep:walkdir"))))))

(define-public crate-dyntest-0.1.1 (c (n "dyntest") (v "0.1.1") (d (list (d (n "globset") (r "^0.4.14") (o #t) (d #t) (k 0)) (d (n "walkdir") (r "^2.5.0") (o #t) (d #t) (k 0)))) (h "1q9ri20gabs24k3yzn2hznrfhgiv1fy9zn6b47c5jxqcbx594z63") (f (quote (("default" "glob")))) (s 2) (e (quote (("glob" "dep:globset" "dep:walkdir"))))))

(define-public crate-dyntest-0.1.2 (c (n "dyntest") (v "0.1.2") (d (list (d (n "globset") (r "^0.4.14") (o #t) (d #t) (k 0)) (d (n "walkdir") (r "^2.5.0") (o #t) (d #t) (k 0)))) (h "1prncw2jvhlwgmrjzjf6cck6iq6ir1c3rm9yz1xg0z3vc3q335l2") (f (quote (("default" "glob")))) (s 2) (e (quote (("glob" "dep:globset" "dep:walkdir"))))))

(define-public crate-dyntest-0.2.0 (c (n "dyntest") (v "0.2.0") (d (list (d (n "globset") (r "^0.4.14") (o #t) (d #t) (k 0)) (d (n "walkdir") (r "^2.5.0") (o #t) (d #t) (k 0)))) (h "0mv86pg6k1bw3chwl1bbw6hk414fawa0h1844591rypx1kx92q2i") (f (quote (("default" "glob")))) (s 2) (e (quote (("glob" "dep:globset" "dep:walkdir"))))))

