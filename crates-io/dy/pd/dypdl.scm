(define-module (crates-io dy pd dypdl) #:use-module (crates-io))

(define-public crate-dypdl-0.1.0 (c (n "dypdl") (v "0.1.0") (d (list (d (n "approx") (r "^0.5") (d #t) (k 0)) (d (n "fixedbitset") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "ordered-float") (r "^3.0") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1") (d #t) (k 0)))) (h "18awx9cr5axbqzxs12c6a49r9s31fvlmjmyrzvjawl8m8mcbpsm4")))

(define-public crate-dypdl-0.2.0 (c (n "dypdl") (v "0.2.0") (d (list (d (n "approx") (r "^0.5") (d #t) (k 0)) (d (n "fixedbitset") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "ordered-float") (r "^3.0") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1") (d #t) (k 0)))) (h "0l4idqrn7znxki9sfzc09xnwrjiy7q61xai2c9gg7v8a95v2gnvm")))

(define-public crate-dypdl-0.3.0 (c (n "dypdl") (v "0.3.0") (d (list (d (n "approx") (r "^0.5") (d #t) (k 0)) (d (n "fixedbitset") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "ordered-float") (r "^3.0") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1") (d #t) (k 0)))) (h "1zj78mmfwyidivjk4b9l31jymlr0hlx1fpx74kywwjb5b0c6y7ji")))

(define-public crate-dypdl-0.3.1 (c (n "dypdl") (v "0.3.1") (d (list (d (n "approx") (r "^0.5") (d #t) (k 0)) (d (n "fixedbitset") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "ordered-float") (r "^3.0") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1") (d #t) (k 0)))) (h "0y380cl7dnrfan5nndjp0lap0iwziqj5mvxrjqa28j2p45l4w4zj")))

(define-public crate-dypdl-0.3.2 (c (n "dypdl") (v "0.3.2") (d (list (d (n "approx") (r "^0.5") (d #t) (k 0)) (d (n "fixedbitset") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "ordered-float") (r "^3.0") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1") (d #t) (k 0)))) (h "05hrqfkw8js4y952ii8m99i5hl5p9c02z2mn8y24k6sqxkzzddmj")))

(define-public crate-dypdl-0.3.3 (c (n "dypdl") (v "0.3.3") (d (list (d (n "approx") (r "^0.5") (d #t) (k 0)) (d (n "fixedbitset") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "ordered-float") (r "^3.0") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1") (d #t) (k 0)))) (h "05x5hwb4cmjd7anav9khg5dyxmkcbaxbg23mbd6z0z1gd7fj4p37")))

(define-public crate-dypdl-0.3.4 (c (n "dypdl") (v "0.3.4") (d (list (d (n "approx") (r "^0.5") (d #t) (k 0)) (d (n "fixedbitset") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "ordered-float") (r "^3.0") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1") (d #t) (k 0)))) (h "18i5lcxv8wcw9nm7sdmwj1m7icijarcm50qpncnbbgx26i7q4qjl")))

(define-public crate-dypdl-0.4.0 (c (n "dypdl") (v "0.4.0") (d (list (d (n "approx") (r "^0.5") (d #t) (k 0)) (d (n "fixedbitset") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "ordered-float") (r "^3.0") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1") (d #t) (k 0)))) (h "0pvhxmw3nrcfv6r1pz2klc23anvjlqbgav0n8njxxbp5kyr3pzpg") (r "1.64")))

(define-public crate-dypdl-0.4.1 (c (n "dypdl") (v "0.4.1") (d (list (d (n "approx") (r "^0.5") (d #t) (k 0)) (d (n "fixedbitset") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "ordered-float") (r "^3.0") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1") (d #t) (k 0)))) (h "0ikksiqr7i7lw97v8inpa72hsdnhm8c6w3796yb0lnqq79gi0vyr") (r "1.64")))

(define-public crate-dypdl-0.5.0 (c (n "dypdl") (v "0.5.0") (d (list (d (n "approx") (r "^0.5") (d #t) (k 0)) (d (n "fixedbitset") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "ordered-float") (r "^3.7") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1") (d #t) (k 0)))) (h "131srw998lwn8211zqqmjla31f44x80bmdy1x9132s583qxlcnil") (r "1.64")))

(define-public crate-dypdl-0.6.0 (c (n "dypdl") (v "0.6.0") (d (list (d (n "approx") (r "^0.5") (d #t) (k 0)) (d (n "fixedbitset") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "ordered-float") (r "^3.7") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1") (d #t) (k 0)))) (h "0rvsmsnm0wwlbwk6vghbvfv1sir4l4gs64hlbcay500ygfbvki37") (r "1.65")))

(define-public crate-dypdl-0.6.1 (c (n "dypdl") (v "0.6.1") (d (list (d (n "approx") (r "^0.5") (d #t) (k 0)) (d (n "fixedbitset") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "ordered-float") (r "^3.7") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1") (d #t) (k 0)))) (h "06kwpwlrzg3shfzqxvszh6wbcbbry45y2gl0f900am81da73s7jk") (r "1.65")))

(define-public crate-dypdl-0.7.0 (c (n "dypdl") (v "0.7.0") (d (list (d (n "approx") (r "^0.5") (d #t) (k 0)) (d (n "fixedbitset") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "ordered-float") (r "^3.9") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1") (d #t) (k 0)))) (h "0dm7klpqki5z7c9fprzcmzvvi755qkwl3rhqnxl35dd625gz90g2") (r "1.65")))

(define-public crate-dypdl-0.7.1 (c (n "dypdl") (v "0.7.1") (d (list (d (n "approx") (r "^0.5") (d #t) (k 0)) (d (n "fixedbitset") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "ordered-float") (r "^3.9") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1") (d #t) (k 0)))) (h "1a2picid9wzs1wiq8kfis6bmysl26lci4dbbs8scs9mgjyvm2sih") (r "1.65")))

(define-public crate-dypdl-0.7.2 (c (n "dypdl") (v "0.7.2") (d (list (d (n "approx") (r "^0.5") (d #t) (k 0)) (d (n "fixedbitset") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "ordered-float") (r "^3.9") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1") (d #t) (k 0)))) (h "0gqvwzzh7m9mar17ykm4jq0pkvh70gkabz41qlp9chh9hnkvi6jw") (r "1.65")))

(define-public crate-dypdl-0.7.3 (c (n "dypdl") (v "0.7.3") (d (list (d (n "approx") (r "^0.5") (d #t) (k 0)) (d (n "fixedbitset") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "ordered-float") (r "^3.9") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1") (d #t) (k 0)))) (h "149r6k8wr7cnfvvra67j3xsz37vjirpj5xxiv4r8qmfbazgwqq21") (r "1.65")))

