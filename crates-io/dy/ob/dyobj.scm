(define-module (crates-io dy ob dyobj) #:use-module (crates-io))

(define-public crate-dyobj-0.1.2 (c (n "dyobj") (v "0.1.2") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.5") (d #t) (k 2)))) (h "0rsps7rh4q26vk2i8d0fimdkqrwzq4fk9zc19k50br9cp2l5wpb2")))

