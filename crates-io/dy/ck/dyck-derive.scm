(define-module (crates-io dy ck dyck-derive) #:use-module (crates-io))

(define-public crate-dyck-derive-0.1.0 (c (n "dyck-derive") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.58") (d #t) (k 0)))) (h "0vchfz7h3zvbzf3f500mz7kzc5g8w6l3s3y7hiwyn83qd79rrp5w")))

