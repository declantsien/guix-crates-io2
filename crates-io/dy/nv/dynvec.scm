(define-module (crates-io dy nv dynvec) #:use-module (crates-io))

(define-public crate-dynvec-0.1.0 (c (n "dynvec") (v "0.1.0") (h "0j3qq0mnks20yrfx3s9dvxsa59jn66b3qiqbvxqp0hhimmz5habh")))

(define-public crate-dynvec-0.1.1 (c (n "dynvec") (v "0.1.1") (h "0jinb2yspaw5z13gh4f4awbch8x8986s13492zyrckqwqajvhzn8")))

(define-public crate-dynvec-0.1.2 (c (n "dynvec") (v "0.1.2") (h "1yklm6ka9c9w3wcykrxkpxv3ws5cqj3bdx6xm0w8lvkqkjglf0ra")))

(define-public crate-dynvec-0.1.3 (c (n "dynvec") (v "0.1.3") (h "12d5pqxhrf2wapdsml8hlqhyjl3ch4dmfwlj53jkk17piqiyng8v")))

(define-public crate-dynvec-0.1.4 (c (n "dynvec") (v "0.1.4") (h "0k6imzqbx35f4wvywh64l58bn72pfs0avjd00r7blch5nhs1wcd0")))

