(define-module (crates-io dy nh dynhash) #:use-module (crates-io))

(define-public crate-dynhash-0.1.0 (c (n "dynhash") (v "0.1.0") (d (list (d (n "mopa") (r "^0.2.2") (d #t) (k 0)) (d (n "sha2") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "sha3") (r "^0.8") (o #t) (d #t) (k 0)))) (h "17n2fzpm97rrsgab0kzdggawqih60gfcvd9p3nc10qn64cf6vcck") (f (quote (("test" "sha2" "sha3") ("doc" "sha2" "sha3") ("default"))))))

