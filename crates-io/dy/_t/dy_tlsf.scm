(define-module (crates-io dy _t dy_tlsf) #:use-module (crates-io))

(define-public crate-dy_tlsf-0.0.1 (c (n "dy_tlsf") (v "0.0.1") (d (list (d (n "arrayvec") (r "^0.7.0") (d #t) (k 0)) (d (n "spin") (r "^0.9.0") (d #t) (k 0)))) (h "1h2na314c5p3v8n1qdsfash4cr8rl5asjq7hhiys91sii9yjfycy")))

(define-public crate-dy_tlsf-0.0.2 (c (n "dy_tlsf") (v "0.0.2") (d (list (d (n "arrayvec") (r "^0.7.0") (d #t) (k 0)) (d (n "spin") (r "^0.9.0") (d #t) (k 0)))) (h "09y8238aaa57z7yf0a7h7g4crvrlm23j3v1l5n6vpx0irnz39kf0")))

