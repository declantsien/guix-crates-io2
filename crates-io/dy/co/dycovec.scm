(define-module (crates-io dy co dycovec) #:use-module (crates-io))

(define-public crate-dycovec-0.1.0 (c (n "dycovec") (v "0.1.0") (h "15l56li0bg4szmwdp20vy2jna6i775jizy61ckqp802kb4gj2xl5")))

(define-public crate-dycovec-0.1.1 (c (n "dycovec") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "crossbeam") (r "^0.8") (d #t) (k 2)) (d (n "evmap") (r "^10") (d #t) (k 2)) (d (n "evmap-derive") (r "^0.2") (d #t) (k 2)) (d (n "parking_lot") (r "^0.11") (d #t) (k 2)) (d (n "rayon") (r "^1.5") (d #t) (k 2)) (d (n "sharded-slab") (r "^0.1") (d #t) (k 2)))) (h "10bqx1sfjigwd6h0z3mrm3x2mpl9c4cgigabhdz51dy030kmxdz1")))

