(define-module (crates-io dy ne dynec-codegen) #:use-module (crates-io))

(define-public crate-dynec-codegen-0.1.0 (c (n "dynec-codegen") (v "0.1.0") (d (list (d (n "matches2") (r "^1.2.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full"))) (d #t) (k 0)))) (h "1n4ninm5vrmz7p739xldy8m6j0a8l6c18jv3ybzqz0spn9f03msq")))

(define-public crate-dynec-codegen-0.2.0-alpha.1 (c (n "dynec-codegen") (v "0.2.0-alpha.1") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "matches2") (r "^1.2.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full"))) (d #t) (k 0)))) (h "0fhz3bsmyby184c7d2zqdg12jiid891mz0a45qzhcdfj5bzawj17")))

(define-public crate-dynec-codegen-0.2.0-alpha.2 (c (n "dynec-codegen") (v "0.2.0-alpha.2") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "matches2") (r "^1.2.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "0kkakqsaanidaf2ykh9ypqkgdcn5f3pfxcyb2n1v89p89kcfr2j3")))

(define-public crate-dynec-codegen-0.2.0-alpha.3 (c (n "dynec-codegen") (v "0.2.0-alpha.3") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "matches2") (r "^1.2.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "1j90rwbxjsp4b94w6rlv371syk7mx9980zsqnq9qjjdarkm9nhmr")))

(define-public crate-dynec-codegen-0.2.0 (c (n "dynec-codegen") (v "0.2.0") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "matches2") (r "^1.2.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "15qn6qs2svq0mblfw3cmx37rydpdrbya80jv7wmrjcqpis9gmx3v")))

(define-public crate-dynec-codegen-0.2.1 (c (n "dynec-codegen") (v "0.2.1") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "matches2") (r "^1.2.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "1zj8xiyhwc61d25h4bf70qmlybcqz8kwhb0zzcfgl47bcvsslh7j")))

