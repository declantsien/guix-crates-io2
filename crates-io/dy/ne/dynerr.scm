(define-module (crates-io dy ne dynerr) #:use-module (crates-io))

(define-public crate-dynerr-0.1.0 (c (n "dynerr") (v "0.1.0") (h "188why31kcmdbp91q8b8c0qvzhlqkky8yi8pvfyaxgwkdbwndvsm")))

(define-public crate-dynerr-0.1.1 (c (n "dynerr") (v "0.1.1") (h "1jd2b6jgp4wxf754lqsfybg04cgvavggry5d45fvh4v39yn066lh")))

(define-public crate-dynerr-0.1.11 (c (n "dynerr") (v "0.1.11") (h "0hqyn7lvck7qj9qvb8afxlj6qwcwmn5yqzblpcjqafqfvry0ab48")))

(define-public crate-dynerr-0.1.2 (c (n "dynerr") (v "0.1.2") (h "12hdcgpjq2l9rr2chch8lghrwig0m3ibj5r2zlg181q4yy0w8ybl")))

(define-public crate-dynerr-0.1.20 (c (n "dynerr") (v "0.1.20") (h "0klr7pgp589abz96lrsl3zvig1i4i1sk2vwc41h847mfq247dr7w")))

(define-public crate-dynerr-0.1.21 (c (n "dynerr") (v "0.1.21") (h "1qwxwagw8rhw6cd0393x7qrsaf4ivrsznyfsy9kygzjxcv4f52b6")))

(define-public crate-dynerr-0.1.22 (c (n "dynerr") (v "0.1.22") (h "0cd4b3mp9hgi01nyjwrp2vc4xv4n51sjp0m68zym9c1vwy08563f")))

(define-public crate-dynerr-0.1.23 (c (n "dynerr") (v "0.1.23") (h "1l5yv0d8bjsh2kl0xdn1chiazwsqrmwaq84scv61yb96fcyj3xcv")))

(define-public crate-dynerr-0.1.24 (c (n "dynerr") (v "0.1.24") (h "1wxmcqhmfqh88hgd21k13b5k1a78078r3s21rq15z9dsf2k2asq4")))

