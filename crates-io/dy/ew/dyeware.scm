(define-module (crates-io dy ew dyeware) #:use-module (crates-io))

(define-public crate-dyeware-0.0.0 (c (n "dyeware") (v "0.0.0") (d (list (d (n "color-span") (r "^0.2.1") (d #t) (k 0)) (d (n "diagnostic-quick") (r "^0.2.3") (f (quote ("tl"))) (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)) (d (n "tl") (r "^0.7.7") (f (quote ("simd"))) (d #t) (k 0)))) (h "1f5hr4fjbb5jd1wx68h0q6wh15wpjx63lg3c1d517dmp0salwd5a") (f (quote (("highlightjs") ("default" "highlightjs"))))))

