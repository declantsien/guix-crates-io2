(define-module (crates-io dy nf dynforest) #:use-module (crates-io))

(define-public crate-dynforest-0.1.0 (c (n "dynforest") (v "0.1.0") (d (list (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0iwn22l22y8xb0wjfpwrf71nzwbp7wbgfnqjaxjdpwbhc101rsr9")))

(define-public crate-dynforest-0.2.0 (c (n "dynforest") (v "0.2.0") (d (list (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "043gc6ngspjm09qjqzk9917bj2dn64hbxlkqa43b25200p2w1kh7")))

