(define-module (crates-io dy nf dynfractal) #:use-module (crates-io))

(define-public crate-dynfractal-0.1.0 (c (n "dynfractal") (v "0.1.0") (d (list (d (n "rcalc_lib") (r "^1.0.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "text_io") (r "^0.1.10") (d #t) (k 0)))) (h "099k1zmydwq6py5y95596j9i93cvxjh28acmxv3cm0q1y8mhwzgk")))

