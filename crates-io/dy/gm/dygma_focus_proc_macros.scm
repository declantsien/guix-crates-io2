(define-module (crates-io dy gm dygma_focus_proc_macros) #:use-module (crates-io))

(define-public crate-dygma_focus_proc_macros-0.1.0 (c (n "dygma_focus_proc_macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0llsqlap545308hwsrcya6fba07gsgph2vjqz92hbnsswlb3xajs")))

(define-public crate-dygma_focus_proc_macros-0.1.1 (c (n "dygma_focus_proc_macros") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0p026r9gn2zpcg5k01wrwyx1gfm0i1pg6r0ixvqr98dyh1zkv29r")))

(define-public crate-dygma_focus_proc_macros-0.2.0 (c (n "dygma_focus_proc_macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0222knjmp2968n1yn65z47phc8kpw3qdvnnhnap2xgw7bjc20mk9")))

(define-public crate-dygma_focus_proc_macros-0.2.1 (c (n "dygma_focus_proc_macros") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0riqw42nka32mlyljhfjx2cmvr9akhd9x0sb6yiwfdk0mkr16k64")))

(define-public crate-dygma_focus_proc_macros-0.2.2 (c (n "dygma_focus_proc_macros") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1ybky8cqzkh1ls5vaa97rm7h1xcjlclxr7f7s04smjvmx4cgqz80")))

(define-public crate-dygma_focus_proc_macros-0.2.3 (c (n "dygma_focus_proc_macros") (v "0.2.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0v0w25j4mbk6bbv31bc2jxmbq1nfc0jdv3psaammh0y57jpbkj0j")))

(define-public crate-dygma_focus_proc_macros-0.2.4 (c (n "dygma_focus_proc_macros") (v "0.2.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0xknvqpckaggs3x2hykii2w6in8sp34am3bp960w5sd2mddmk1iv")))

(define-public crate-dygma_focus_proc_macros-0.3.0 (c (n "dygma_focus_proc_macros") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1ic9198cfwkng2hl1hwzszabjjwlh3fr06q5hf08qfwbrv4q2brq")))

(define-public crate-dygma_focus_proc_macros-0.3.1 (c (n "dygma_focus_proc_macros") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "16kfi6fly2vbaj9c2dmwyd294z1i6sc7mjdlj4iijb4vs9jlvsj6")))

(define-public crate-dygma_focus_proc_macros-0.3.2 (c (n "dygma_focus_proc_macros") (v "0.3.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "06q8i6116zi3hg4yy6fzic5kwa125nhs8figc382r3lvvv3kaj93")))

(define-public crate-dygma_focus_proc_macros-0.4.0 (c (n "dygma_focus_proc_macros") (v "0.4.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0ggbwnm5fii2pvpgvfixnxibqbc508dw7mqpq090f9ajfx2v48kw")))

(define-public crate-dygma_focus_proc_macros-0.4.1 (c (n "dygma_focus_proc_macros") (v "0.4.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0qk5cljrsmw6bkd29flpknmz99jpin87i5a2i8lvy9s3wgy74090")))

(define-public crate-dygma_focus_proc_macros-0.4.2 (c (n "dygma_focus_proc_macros") (v "0.4.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0nk8fp0nmy3v7i8r53wz3037a70sddlr17qzmdlmqjwklw8k60qa")))

(define-public crate-dygma_focus_proc_macros-0.4.3 (c (n "dygma_focus_proc_macros") (v "0.4.3") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1fyrzj9yjig4qbz6yfbmfbap1x184wpf0xx2r7nf0gdi4b6qpnm9")))

(define-public crate-dygma_focus_proc_macros-0.4.4 (c (n "dygma_focus_proc_macros") (v "0.4.4") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "18vajbxn6hnhn2524p9lv59h2fpmhrg1c3iryi8hw1rp5gqjyn0y")))

(define-public crate-dygma_focus_proc_macros-0.4.5 (c (n "dygma_focus_proc_macros") (v "0.4.5") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "02pdhd8hg4pcs6bfplh51m2hajr7ayawlj7gr6sp5vr268pyznqz")))

(define-public crate-dygma_focus_proc_macros-0.4.6 (c (n "dygma_focus_proc_macros") (v "0.4.6") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0d4gpqms3dy5gv5jllh9zz20chcxcwgb5v148gkglkjbj8gb8ax5")))

(define-public crate-dygma_focus_proc_macros-0.4.7 (c (n "dygma_focus_proc_macros") (v "0.4.7") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1n9i7cykaqy6lsfm8dv2gn0ri7af93pk8anlc0k299ayccprk44g")))

(define-public crate-dygma_focus_proc_macros-0.4.8 (c (n "dygma_focus_proc_macros") (v "0.4.8") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0rnw4xskiy7g5zins3lc4dvn48fxr9aisc8nqpyzvld9hk7jvpqb")))

