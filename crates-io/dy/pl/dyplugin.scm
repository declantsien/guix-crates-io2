(define-module (crates-io dy pl dyplugin) #:use-module (crates-io))

(define-public crate-dyplugin-0.1.0 (c (n "dyplugin") (v "0.1.0") (d (list (d (n "libloading") (r "^0.5.0") (d #t) (k 0)))) (h "0f9pwrb4n0wyrxhdjy3harbhxhwzvmr3vwpfadwiaxkq9kz8rhid")))

(define-public crate-dyplugin-0.2.0 (c (n "dyplugin") (v "0.2.0") (d (list (d (n "libloading") (r "^0.5.0") (d #t) (k 0)))) (h "0w0wy67k28j8k3mi1q644pl3n2xkfdc0ap5l4wl7mipbzbi5qg1d")))

