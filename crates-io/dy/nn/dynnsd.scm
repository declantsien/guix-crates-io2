(define-module (crates-io dy nn dynnsd) #:use-module (crates-io))

(define-public crate-dynnsd-0.2.3 (c (n "dynnsd") (v "0.2.3") (d (list (d (n "base64") (r "^0.11.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.66") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "log4rs") (r "^0.8.3") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "sanename") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.101") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.101") (d #t) (k 0)) (d (n "sha2") (r "^0.8.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.3") (d #t) (k 0)))) (h "0n6ldsk3pqh9rkfybx5kvlsxfxzq2mn2hsky2hv2mml8ax27jx40")))

