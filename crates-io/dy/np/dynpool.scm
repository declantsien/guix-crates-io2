(define-module (crates-io dy np dynpool) #:use-module (crates-io))

(define-public crate-dynpool-0.0.0 (c (n "dynpool") (v "0.0.0") (d (list (d (n "backoff") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 2)))) (h "00d09xcn78wjqy66xla2gvnaadjmmbwb5h3229ijbnfdhrdq65mi") (f (quote (("instrument"))))))

(define-public crate-dynpool-0.0.1 (c (n "dynpool") (v "0.0.1") (d (list (d (n "backoff") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 2)))) (h "19bsgf94llnhvi73iw25m1ffjggj15dccmnlwf7m9v7vj0y7hzch") (f (quote (("instrument"))))))

(define-public crate-dynpool-0.0.2 (c (n "dynpool") (v "0.0.2") (d (list (d (n "backoff") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 2)))) (h "02hvjb9vb0pf1f9fwzh7060wqz37bc1dkm6mb05sj873bk6y8zgm") (f (quote (("instrument"))))))

