(define-module (crates-io dy np dynpick-force-torque-sensor) #:use-module (crates-io))

(define-public crate-dynpick-force-torque-sensor-0.1.0 (c (n "dynpick-force-torque-sensor") (v "0.1.0") (d (list (d (n "easy-ext") (r "^0.2.6") (d #t) (k 0)) (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "pair_macro") (r "^0.1.4") (d #t) (k 0)) (d (n "serialport") (r "^4.0.0") (d #t) (k 0)))) (h "02m16z9sfmzm4h7hzyasd6gm75911c4hq9w80jzmib2gbm62hbdi")))

