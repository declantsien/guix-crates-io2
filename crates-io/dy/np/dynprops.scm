(define-module (crates-io dy np dynprops) #:use-module (crates-io))

(define-public crate-dynprops-0.1.0 (c (n "dynprops") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "static_init") (r "^1.0.1") (d #t) (k 2)))) (h "0dx0q1i25i63n5k50gza1cmd0gjslyrjqdllj8qpbknr5rwkg85j")))

