(define-module (crates-io dy np dynp) #:use-module (crates-io))

(define-public crate-dynp-0.1.10 (c (n "dynp") (v "0.1.10") (d (list (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)))) (h "0r28alh052xnq14gz6fy6mwhpd1hgzch88rnm7ns9prk23lnz2i6")))

(define-public crate-dynp-0.1.11-PullRequest0001.2 (c (n "dynp") (v "0.1.11-PullRequest0001.2") (d (list (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)))) (h "16lb3x02i3y6a072gsxcdkj3vizw7kffyxv7fbm7apkl7r7rd0sa") (y #t)))

(define-public crate-dynp-0.1.11-PullRequest0001.3 (c (n "dynp") (v "0.1.11-PullRequest0001.3") (d (list (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)))) (h "1187n3c1w8liygg6d2xny1h521xq65jddfrxdj2ng46yggmbz71a") (y #t)))

(define-public crate-dynp-0.1.11-PullRequest0001.4 (c (n "dynp") (v "0.1.11-PullRequest0001.4") (d (list (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)))) (h "0zc0aqwl2p9nsca5nqb3d0xd0kz6ci568mb45lk42b6l6j0kzvvr") (y #t)))

(define-public crate-dynp-0.1.11 (c (n "dynp") (v "0.1.11") (d (list (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)))) (h "04m1nbvk9fbicb6d3rzgrndnfvyw7y8qdlaivkba1nb89z8g45s5")))

(define-public crate-dynp-0.1.12 (c (n "dynp") (v "0.1.12") (h "1waivcnh9bf0zcf961g90l0zdzzq7hd0h4lg9jppynca0a2pwcnv")))

