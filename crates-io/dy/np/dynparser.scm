(define-module (crates-io dy np dynparser) #:use-module (crates-io))

(define-public crate-dynparser-0.1.0 (c (n "dynparser") (v "0.1.0") (h "1napxp62gcmn033r9mrqj8fpkika3qgfshxlqb8xnkbf7cp2rmlb")))

(define-public crate-dynparser-0.2.0 (c (n "dynparser") (v "0.2.0") (h "0wqfz46av3xpsnkslkwlj88dblyyjssh62wnm6nzvrg5n231lya6") (y #t)))

(define-public crate-dynparser-0.2.1 (c (n "dynparser") (v "0.2.1") (h "04nb0ilzf8nbc8yrycah3q7qb6z26al5vh27krv29v8mwqajlmxa")))

(define-public crate-dynparser-0.4.0 (c (n "dynparser") (v "0.4.0") (d (list (d (n "idata") (r "^0.1.0") (d #t) (k 0)))) (h "14zqp1634ig233i5j4pyjhgfwmpjcsnbvj8kl0x7ymsvbipz1rmv")))

(define-public crate-dynparser-0.4.1 (c (n "dynparser") (v "0.4.1") (d (list (d (n "idata") (r "^0.1.0") (d #t) (k 0)))) (h "048shy0z7gvjxibc7jrv5knx4va3b836v9iqc14byrpv4vmx354l")))

(define-public crate-dynparser-0.4.2 (c (n "dynparser") (v "0.4.2") (d (list (d (n "idata") (r "^0.1.0") (d #t) (k 0)))) (h "0cq05y033ac9pzxx7sxr5vd92jlnwmal7v2f6dcgkccr31sxhf5b")))

