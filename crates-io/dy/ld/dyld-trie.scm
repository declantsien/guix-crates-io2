(define-module (crates-io dy ld dyld-trie) #:use-module (crates-io))

(define-public crate-dyld-trie-0.1.0 (c (n "dyld-trie") (v "0.1.0") (d (list (d (n "hex-literal") (r "^0.4") (d #t) (k 2)) (d (n "leb128") (r "^0.2") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 2)))) (h "01vx1s60g8jcgp7m25ddan1xabn316g8nbcl42vpzs101ka6xpl2") (f (quote (("std")))) (r "1.65")))

