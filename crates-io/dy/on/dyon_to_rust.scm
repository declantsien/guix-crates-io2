(define-module (crates-io dy on dyon_to_rust) #:use-module (crates-io))

(define-public crate-dyon_to_rust-0.1.0 (c (n "dyon_to_rust") (v "0.1.0") (d (list (d (n "dyon") (r "^0.24.5") (d #t) (k 0)) (d (n "piston_meta") (r "^0.29.0") (d #t) (k 0)))) (h "1k95jz7bm2ywh0r0db2l434p3xh68lsvji6rrg2153rmx6djza0p")))

