(define-module (crates-io dy e- dye-cli) #:use-module (crates-io))

(define-public crate-dye-cli-0.1.2 (c (n "dye-cli") (v "0.1.2") (d (list (d (n "clap") (r "^3.0.0-beta.4") (f (quote ("color" "env" "suggestions"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "06jvk010j14jr3g02d2jyczkz8rf0wkkqvz5vkh2machb4cm97bi")))

(define-public crate-dye-cli-0.1.4 (c (n "dye-cli") (v "0.1.4") (d (list (d (n "clap") (r "^3.0.0-beta.4") (f (quote ("color" "env" "suggestions"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "0ahq9cmkp9jwyzgmakw4r6brb83wwpx819n8q8vmb29m6c0bykkh")))

(define-public crate-dye-cli-0.1.5 (c (n "dye-cli") (v "0.1.5") (d (list (d (n "clap") (r "^3.0.0-beta.4") (f (quote ("color" "env" "suggestions"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "0pq3a4qcs4cv0xw5izlz7qcmx211dq992w7kq7l462a8if0ywlcv")))

(define-public crate-dye-cli-0.1.6 (c (n "dye-cli") (v "0.1.6") (d (list (d (n "clap") (r "^3.0.0-beta.4") (f (quote ("color" "env" "suggestions"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "0nm9pz4fna9h614jblkbgxl54z65zd34w3phlwzwqdm56j1vy47l")))

(define-public crate-dye-cli-0.1.14 (c (n "dye-cli") (v "0.1.14") (d (list (d (n "clap") (r "^3.0.0-beta.4") (f (quote ("color" "env" "suggestions"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "0dsf64p46grkvwxk6xdbv1axvy6ygl4gnzjsfqpl1mpqyvypc6ms")))

(define-public crate-dye-cli-0.1.15 (c (n "dye-cli") (v "0.1.15") (d (list (d (n "clap") (r "^3.0.0-beta.4") (f (quote ("color" "env" "suggestions"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "11jgx09g405a0z82la4g01mjh6kvhbzsrqdm7illlkw5aga20ngy")))

(define-public crate-dye-cli-0.1.16 (c (n "dye-cli") (v "0.1.16") (d (list (d (n "clap") (r "^3.0.0-beta.4") (f (quote ("color" "env" "suggestions"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "1gy4n0hbmg43qhnc5dnkm5v5wa2jb3cc4bw3rn1bwd81869r72mx")))

(define-public crate-dye-cli-0.2.0 (c (n "dye-cli") (v "0.2.0") (d (list (d (n "clap") (r "^3.0.0-beta.4") (f (quote ("color" "env" "suggestions"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "0ys8rzhcvbn7idlllbfhbfp4m5vim3xd016scy8ggbyk1iplp2ps")))

(define-public crate-dye-cli-0.4.0 (c (n "dye-cli") (v "0.4.0") (d (list (d (n "clap") (r "^3.0.0-beta.4") (f (quote ("color" "env" "suggestions"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "1gyrw8bg9cyxlxaz9anphs5b0y42c8bilssld1qjp39w8p9lp4k7")))

(define-public crate-dye-cli-0.4.2 (c (n "dye-cli") (v "0.4.2") (d (list (d (n "clap") (r "=3.0.0-beta.4") (f (quote ("color" "env" "suggestions"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "00yjdqbwhrkpd97nwpqyk3rkj25va33gh2kv2vscc4mq28bh0mic")))

