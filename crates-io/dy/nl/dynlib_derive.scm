(define-module (crates-io dy nl dynlib_derive) #:use-module (crates-io))

(define-public crate-dynlib_derive-0.1.0 (c (n "dynlib_derive") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.29") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "19c2jd635aiwfqvly55nnmrslril2fhjj4icjq607zpjq90l63cr") (y #t)))

