(define-module (crates-io sl ur slur) #:use-module (crates-io))

(define-public crate-slur-0.1.0 (c (n "slur") (v "0.1.0") (d (list (d (n "imgref") (r "^1.9.2") (d #t) (k 0)) (d (n "imgref-iter") (r "~0.4.0") (f (quote ("simd"))) (d #t) (k 0)))) (h "17dk08la6bb59lziyss37irvgazv26flasgljq8rs9d8crg0s85k")))

