(define-module (crates-io sl ur slurmplus) #:use-module (crates-io))

(define-public crate-slurmplus-0.1.1 (c (n "slurmplus") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "itertools") (r "^0.7") (d #t) (k 0)) (d (n "slurm") (r "^0.1.1") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)) (d (n "termcolor") (r "^0.3") (d #t) (k 0)) (d (n "users") (r "^0.6") (d #t) (k 0)))) (h "0ksrxpqj8c8qpsy9fjfqq5hxyi5r0jx43fbbnrbgqa5wcz0y4zwi")))

(define-public crate-slurmplus-0.1.3 (c (n "slurmplus") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "itertools") (r "^0.7") (d #t) (k 0)) (d (n "slurm") (r "^0.1.3") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)) (d (n "termcolor") (r "^0.3") (d #t) (k 0)) (d (n "users") (r "^0.6") (d #t) (k 0)))) (h "0flkgxzns0i339hip43parsj3calwg2iga3qybpdv8db7y9774mp")))

