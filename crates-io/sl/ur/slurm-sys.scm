(define-module (crates-io sl ur slurm-sys) #:use-module (crates-io))

(define-public crate-slurm-sys-0.1.0 (c (n "slurm-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.33") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0a420v2cd3dslk27lqvvlbr6r1nkync9l2qrnqn01jvhxjq8y9qz")))

(define-public crate-slurm-sys-0.1.1 (c (n "slurm-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.35") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0q8d71jyg28i7y24777gq865kq4fqdj6lyvg9jhgzcfpw5s65p04") (l "slurm")))

(define-public crate-slurm-sys-0.1.2 (c (n "slurm-sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.35") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0mcs0gjfpka5agdw62xxlhap85ccm54330qpz891wr2xcm1mdbc3") (l "slurm")))

(define-public crate-slurm-sys-0.1.3 (c (n "slurm-sys") (v "0.1.3") (d (list (d (n "bindgen") (r "^0.35") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0zh51n7y0gz3rgkgc92jmw9nm1gl2wika649sw7vwwf17yz486im") (l "slurm")))

