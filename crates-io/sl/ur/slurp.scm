(define-module (crates-io sl ur slurp) #:use-module (crates-io))

(define-public crate-slurp-1.0.0 (c (n "slurp") (v "1.0.0") (h "13ia5a2psnjy7nkwimf4n828zvbzinhhg8j85dp1ndb9xllbvlny")))

(define-public crate-slurp-1.0.1 (c (n "slurp") (v "1.0.1") (h "017pr9rg00axjjsnvwldr95y7b7515pblwa3bif8qbrv1wd80gk7")))

