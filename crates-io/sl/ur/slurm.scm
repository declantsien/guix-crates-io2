(define-module (crates-io sl ur slurm) #:use-module (crates-io))

(define-public crate-slurm-0.1.0 (c (n "slurm") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2.30") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "slurm-sys") (r "^0.1.0") (d #t) (k 0)))) (h "0xsssm523cwyn6cmj50c71by1a4191dyz9w2x0b0b5rc0z4x74dm")))

(define-public crate-slurm-0.1.1 (c (n "slurm") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2.30") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "itertools") (r "^0.7") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "slurm-sys") (r "^0.1.1") (d #t) (k 0)) (d (n "slurm-sys") (r "^0.1.1") (d #t) (k 1)))) (h "1fh0dz8m70jvn5yvz05y4w083r12aypr1lb2hl0r20yfmif6ifwd")))

(define-public crate-slurm-0.1.3 (c (n "slurm") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2.30") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "itertools") (r "^0.7") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "slurm-sys") (r "^0.1.3") (d #t) (k 0)) (d (n "slurm-sys") (r "^0.1.3") (d #t) (k 1)))) (h "0d9dd21kx77ywxxgp6rmqldj5wm6divhlcl7k2vqkgfgv8jc03kn")))

