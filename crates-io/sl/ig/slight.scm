(define-module (crates-io sl ig slight) #:use-module (crates-io))

(define-public crate-slight-0.1.0 (c (n "slight") (v "0.1.0") (d (list (d (n "argh") (r "^0.1") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.0") (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "1ji8pnsqwaz90qcscw2hqdb461ava4m312xhvrkcbdxl10xkxyrn")))

