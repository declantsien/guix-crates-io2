(define-module (crates-io sl k5 slk581) #:use-module (crates-io))

(define-public crate-slk581-0.0.1 (c (n "slk581") (v "0.0.1") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)))) (h "1vbqmqq1z9y53r3p3r1ddkmz3yv1nv2650pxl9w8dhn7i448n1a6")))

(define-public crate-slk581-0.0.2 (c (n "slk581") (v "0.0.2") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)))) (h "1l0cg1kfyk87m7a0r9cnclw80sxl46wpyjynbbcqzj1xxlcfhyyq")))

