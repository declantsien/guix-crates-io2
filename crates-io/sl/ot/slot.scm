(define-module (crates-io sl ot slot) #:use-module (crates-io))

(define-public crate-slot-0.1.0 (c (n "slot") (v "0.1.0") (h "0lybqgd9dz6ayk8qkvpvasqrx55ipg2x0sivjjzn5r9bxbn98r5x")))

(define-public crate-slot-0.2.0 (c (n "slot") (v "0.2.0") (h "0s0vx9fxvqpsn5r76sr063a8nm0qvpr32554z63qwl2p54j5l1hb")))

(define-public crate-slot-0.2.1 (c (n "slot") (v "0.2.1") (h "0fgbcnh61bzr9v34x8qalvjw9p5wm4k8gc7f2w6hdjvmi8j57kad")))

(define-public crate-slot-0.2.2 (c (n "slot") (v "0.2.2") (h "033i8gy87krk5gq6748kh6cf21kh4clwq8fyymds0zbmigww1zs8")))

(define-public crate-slot-0.2.3 (c (n "slot") (v "0.2.3") (h "1k50lvikh8nj3gp88xy4xbhh9699f0j5k8l6k7a60k6jmd1h6qxl")))

(define-public crate-slot-0.2.4 (c (n "slot") (v "0.2.4") (h "1521jnf1mnsyddc22pkzhq0hknkb6llhwzsnagwi8d1v7xg7m3jj")))

(define-public crate-slot-0.2.5 (c (n "slot") (v "0.2.5") (h "1wix9rklf991sf36amg01hw23i6fnalpzcik1vkjp0rpprrn1zs1")))

