(define-module (crates-io sl ot slot_machine) #:use-module (crates-io))

(define-public crate-slot_machine-0.1.0 (c (n "slot_machine") (v "0.1.0") (d (list (d (n "rand") (r "^0.7.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "01fkp4d7xhqmzkkbp2k36a4cy2212ar5v0cwc8ryf6dw44vbwjpv")))

(define-public crate-slot_machine-0.1.1 (c (n "slot_machine") (v "0.1.1") (d (list (d (n "rand") (r "^0.7.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1ayvv9izipqp9p0c8ll6xpmyhdbyzr30fnhslqzg9p77x6q360lz")))

(define-public crate-slot_machine-0.2.0 (c (n "slot_machine") (v "0.2.0") (d (list (d (n "rand") (r "^0.7.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1cypk6499gyvd8pdxap6nm2mdvq8k9kz1iqgbbmxsnh4b8m6dhdl")))

(define-public crate-slot_machine-0.2.1 (c (n "slot_machine") (v "0.2.1") (d (list (d (n "rand") (r "^0.7.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0975d8kxdl6wxx6dmc61qxv0kmxsyy0sykyyx7ifzm24fqapblnw")))

(define-public crate-slot_machine-0.3.0 (c (n "slot_machine") (v "0.3.0") (d (list (d (n "rand") (r "^0.7.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0vxb76ixc05piwwsqqfiqicrn84fr50wg15h54p8jfl7bvlmb0k9")))

