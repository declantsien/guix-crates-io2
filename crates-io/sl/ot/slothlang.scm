(define-module (crates-io sl ot slothlang) #:use-module (crates-io))

(define-public crate-slothlang-0.1.0 (c (n "slothlang") (v "0.1.0") (d (list (d (n "clap") (r "^3.2.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.5.6") (d #t) (k 0)) (d (n "text_io") (r "^0.1.10") (d #t) (k 0)))) (h "1lzrxymv72l2551riq84gn85h4zmdf0aqbrrsx5h28c242zl9phn")))

(define-public crate-slothlang-0.1.1 (c (n "slothlang") (v "0.1.1") (d (list (d (n "clap") (r "^3.2.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.5.6") (d #t) (k 0)) (d (n "text_io") (r "^0.1.10") (d #t) (k 0)))) (h "13skj9lby7jffnrylhk9cy2zwbws0d1cji69mf9iv4gcf1clh4i4")))

(define-public crate-slothlang-0.6.0 (c (n "slothlang") (v "0.6.0") (d (list (d (n "clap") (r "^3.2.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1.5.6") (d #t) (k 0)) (d (n "sdl2") (r "^0.35.2") (d #t) (k 0)) (d (n "text_io") (r "^0.1.10") (d #t) (k 0)))) (h "1mj939k4xbn5rcivh33sxjslrz66y5isnj1dxrqjdba4a8bk80i8")))

(define-public crate-slothlang-0.9.0 (c (n "slothlang") (v "0.9.0") (d (list (d (n "clap") (r "^3.2.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1.5.6") (d #t) (k 0)) (d (n "sdl2") (r "^0.35.2") (d #t) (k 0)) (d (n "text_io") (r "^0.1.10") (d #t) (k 0)))) (h "1a6qww9f0fzpyands2s8ad4c042708ay7yz1f7mrrgb68bipabgm")))

(define-public crate-slothlang-0.9.1 (c (n "slothlang") (v "0.9.1") (d (list (d (n "clap") (r "^3.2.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1.5.6") (d #t) (k 0)) (d (n "sdl2") (r "^0.35.2") (d #t) (k 0)) (d (n "text_io") (r "^0.1.10") (d #t) (k 0)))) (h "1prllczwp9nbhxn4bljrrfc5njx8cswp50l06r9jfwlad0rkynnz")))

(define-public crate-slothlang-0.9.2 (c (n "slothlang") (v "0.9.2") (d (list (d (n "clap") (r "^3.2.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1.5.6") (d #t) (k 0)) (d (n "sdl2") (r "^0.35.2") (d #t) (k 0)) (d (n "text_io") (r "^0.1.10") (d #t) (k 0)))) (h "1ypzjsvfr0wp5wmanpm4qn3vxc2nq9qmvk3hg9piaddg9a1xxrxn")))

(define-public crate-slothlang-0.9.3 (c (n "slothlang") (v "0.9.3") (d (list (d (n "clap") (r "^3.2.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1.5.6") (d #t) (k 0)) (d (n "sdl2") (r "^0.35.2") (d #t) (k 0)) (d (n "text_io") (r "^0.1.10") (d #t) (k 0)))) (h "09c4gx2nj5c0nfmkbsp4jsciy2rrax7kv9mns44i0y9yalz371ch")))

(define-public crate-slothlang-1.0.0 (c (n "slothlang") (v "1.0.0") (d (list (d (n "clap") (r "^3.2.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1.5.6") (d #t) (k 0)) (d (n "sdl2") (r "^0.35.2") (d #t) (k 0)) (d (n "text_io") (r "^0.1.10") (d #t) (k 0)))) (h "05f571w3wkmiih372i8h0r1a6zdym0347kbx7zv2xb7pmn4n2d1w")))

(define-public crate-slothlang-1.0.1 (c (n "slothlang") (v "1.0.1") (d (list (d (n "clap") (r "^3.2.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1.5.6") (d #t) (k 0)) (d (n "sdl2") (r "^0.35.2") (d #t) (k 0)) (d (n "text_io") (r "^0.1.10") (d #t) (k 0)))) (h "1fwlaa5y6yis7j7l12d1b8fl8prjbb6lnk4nzla1nvbrjjrrwyv5")))

(define-public crate-slothlang-1.0.2 (c (n "slothlang") (v "1.0.2") (d (list (d (n "clap") (r "^3.2.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1.5.6") (d #t) (k 0)) (d (n "sdl2") (r "^0.35.2") (d #t) (k 0)) (d (n "text_io") (r "^0.1.10") (d #t) (k 0)))) (h "0ap8q6m0d8l86aan66s1lx4a4y0k4qk89qn65fbmamn90gv97lhb")))

(define-public crate-slothlang-1.1.0 (c (n "slothlang") (v "1.1.0") (d (list (d (n "clap") (r "^3.2.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1.5.6") (d #t) (k 0)) (d (n "sdl2") (r "^0.35.2") (d #t) (k 0)) (d (n "text_io") (r "^0.1.10") (d #t) (k 0)))) (h "0a7r6mwh2q23nfn14x7rmcnlsli8082n6g39gk9lycngmv9lv0qr")))

(define-public crate-slothlang-1.2.0 (c (n "slothlang") (v "1.2.0") (d (list (d (n "clap") (r "^3.2.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1.5.6") (d #t) (k 0)) (d (n "sdl2") (r "^0.35.2") (d #t) (k 0)) (d (n "text_io") (r "^0.1.10") (d #t) (k 0)))) (h "1i0hwhq3ih2m81yfvrzrz57wbmr415wbf6n8yvk4z8kgbj2l9rhp")))

(define-public crate-slothlang-1.2.1 (c (n "slothlang") (v "1.2.1") (d (list (d (n "clap") (r "^3.2.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1.5.6") (d #t) (k 0)) (d (n "sdl2") (r "^0.35.2") (d #t) (k 0)) (d (n "text_io") (r "^0.1.10") (d #t) (k 0)))) (h "07bbaxr0sxjf7rfphkd5vxk6caq6v7c6z3gd2xsk2xn0p9mzm7ly")))

(define-public crate-slothlang-1.3.0 (c (n "slothlang") (v "1.3.0") (d (list (d (n "clap") (r "^3.2.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1.5.6") (d #t) (k 0)) (d (n "sdl2") (r "^0.35.2") (d #t) (k 0)) (d (n "text_io") (r "^0.1.10") (d #t) (k 0)))) (h "1q7kyyrcx0d49l8acvf359hf2bdsnhyc7ga3yps53cbczalc749f")))

(define-public crate-slothlang-1.4.0 (c (n "slothlang") (v "1.4.0") (d (list (d (n "clap") (r "^3.2.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1.5.6") (d #t) (k 0)) (d (n "sdl2") (r "^0.35.2") (d #t) (k 0)) (d (n "text_io") (r "^0.1.10") (d #t) (k 0)))) (h "01v9qqghld31kz1hsjilwyhraaxlx4zi74c8cxvkx13n7vdwqwai")))

