(define-module (crates-io sl ot slots) #:use-module (crates-io))

(define-public crate-slots-0.1.0 (c (n "slots") (v "0.1.0") (d (list (d (n "generic-array") (r "^0.14.1") (d #t) (k 0)))) (h "0zg811n26fkyr233r9fpxhw0m5bjpsqqlk803dlw0m27a56ml9ww")))

(define-public crate-slots-0.1.1 (c (n "slots") (v "0.1.1") (d (list (d (n "generic-array") (r "^0.14.1") (d #t) (k 0)))) (h "1zdvfmzj21n1x6b5aqkxdqqs5r0cznwdxq0v5fzj0h5yjpa8kgcj")))

(define-public crate-slots-0.2.0 (c (n "slots") (v "0.2.0") (d (list (d (n "generic-array") (r "^0.14.1") (d #t) (k 0)))) (h "1pa321bxp93m11n9zwhdyi2n7f6gg1n1b39clklzplxc02swfavm") (f (quote (("verify_owner") ("default" "verify_owner"))))))

(define-public crate-slots-0.3.0 (c (n "slots") (v "0.3.0") (d (list (d (n "generic-array") (r "^0.14.1") (d #t) (k 0)))) (h "1pk6gfxyxwg9pskc441xw1560vi92q0zv9ljw5gb3rah6cld7763") (f (quote (("runtime_checks") ("default" "runtime_checks"))))))

(define-public crate-slots-0.3.1 (c (n "slots") (v "0.3.1") (d (list (d (n "generic-array") (r "^0.14.1") (d #t) (k 0)))) (h "1garijmq9hfgnxjnflnhzg3c62jdwm9qck7wwi3107pa3b1g4n65") (f (quote (("runtime_checks") ("default" "runtime_checks"))))))

(define-public crate-slots-0.4.0 (c (n "slots") (v "0.4.0") (d (list (d (n "array-init") (r "^2") (d #t) (k 0)))) (h "1q2niipq68g2kklmx3rzlxfg410a0890kn6zyax100bx1vpxhykl") (f (quote (("runtime_checks") ("default" "runtime_checks"))))))

