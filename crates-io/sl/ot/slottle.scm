(define-module (crates-io sl ot slottle) #:use-module (crates-io))

(define-public crate-slottle-0.1.0 (c (n "slottle") (v "0.1.0") (d (list (d (n "rand") (r "^0.7.3") (o #t) (d #t) (k 0)) (d (n "rayon") (r "^1.5.0") (d #t) (k 2)) (d (n "retry") (r "^1.1.0") (o #t) (d #t) (k 0)) (d (n "std-semaphore") (r "^0.1.0") (d #t) (k 0)))) (h "1i2xcxcnj89fbld80h3698h557fhf5rxyln90czrsr04awnrxk1i") (f (quote (("retrying" "retry") ("fuzzy_fns" "rand") ("default" "fuzzy_fns"))))))

(define-public crate-slottle-0.1.1 (c (n "slottle") (v "0.1.1") (d (list (d (n "rand") (r "^0.7.3") (o #t) (d #t) (k 0)) (d (n "rayon") (r "^1.5.0") (d #t) (k 2)) (d (n "retry") (r "^1.1.0") (o #t) (d #t) (k 0)) (d (n "std-semaphore") (r "^0.1.0") (d #t) (k 0)))) (h "0s13008yazyf0phzak2vb0r169viwfnl6lwddmhkfhiwxd7j94cy") (f (quote (("retrying" "retry") ("fuzzy_fns" "rand") ("default" "fuzzy_fns"))))))

(define-public crate-slottle-0.2.0 (c (n "slottle") (v "0.2.0") (d (list (d (n "rand") (r "^0.7.3") (o #t) (d #t) (k 0)) (d (n "rayon") (r "^1.5.0") (d #t) (k 2)) (d (n "retry") (r "^1.1.0") (o #t) (d #t) (k 0)) (d (n "std-semaphore") (r "^0.1.0") (d #t) (k 0)))) (h "10crbryhjmxn15fzcikvjydpwb378j3xa5schjcmajwhmwnw5w5z") (f (quote (("retrying" "retry") ("fuzzy_fns" "rand") ("default"))))))

(define-public crate-slottle-0.3.0 (c (n "slottle") (v "0.3.0") (d (list (d (n "rand") (r "^0.8.0") (d #t) (k 2)) (d (n "rayon") (r "^1.5.0") (d #t) (k 2)) (d (n "std-semaphore") (r "^0.1.0") (d #t) (k 0)))) (h "0nm8h3wdjqy95mbf35ayj2nr3mj05xp7ra507igj5irsax2qnbkp")))

(define-public crate-slottle-0.4.0 (c (n "slottle") (v "0.4.0") (d (list (d (n "rand") (r "^0.8.0") (d #t) (k 2)) (d (n "rayon") (r "^1.5.0") (d #t) (k 2)) (d (n "std-semaphore") (r "^0.1.0") (d #t) (k 0)))) (h "1lgs02gp6nkhrm5s10y87aq7kljy6x8yf2vn8zz2a0im3cj4q1sr")))

(define-public crate-slottle-0.4.1 (c (n "slottle") (v "0.4.1") (d (list (d (n "rand") (r "^0.8.0") (d #t) (k 2)) (d (n "rayon") (r "^1.5.0") (d #t) (k 2)) (d (n "std-semaphore") (r "^0.1.0") (d #t) (k 0)))) (h "0bzyw2dcdcmcfdg90ywpfvvdrgvym439w0jvzk73wvvjdc9d7c9v")))

(define-public crate-slottle-0.4.2 (c (n "slottle") (v "0.4.2") (d (list (d (n "rand") (r "^0.8.0") (d #t) (k 2)) (d (n "rayon") (r "^1.5.0") (d #t) (k 2)) (d (n "std-semaphore") (r "^0.1.0") (d #t) (k 0)))) (h "12gj5d1z1wjv2xi36ms75b9va7i17kkmisqwzz3s0ipwf874qxng")))

