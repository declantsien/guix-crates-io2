(define-module (crates-io sl ot slotgraph) #:use-module (crates-io))

(define-public crate-slotgraph-0.1.0 (c (n "slotgraph") (v "0.1.0") (d (list (d (n "petgraph") (r "^0.6.4") (k 0)) (d (n "rstest") (r "^0.19.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.200") (o #t) (d #t) (k 0)) (d (n "slotmap") (r "^1.0.7") (d #t) (k 0)))) (h "1k46yrfkdgs595s6a2zpr0bdfl3ibyk9am0sfh55yzxhgmi9jk77") (s 2) (e (quote (("serde" "dep:serde" "slotmap/serde"))))))

