(define-module (crates-io sl ot sloth) #:use-module (crates-io))

(define-public crate-sloth-0.1.0 (c (n "sloth") (v "0.1.0") (h "0yqxjjks1j8pdvijaxnjxfcg457qdrj3k0yf794pw3zqkgdl3jzk")))

(define-public crate-sloth-0.1.1 (c (n "sloth") (v "0.1.1") (h "1sqks5ffpb909iwz4lhilrj72sq39m1aclz15xqwlya32prngssn")))

(define-public crate-sloth-0.1.2 (c (n "sloth") (v "0.1.2") (h "039rcs2cj1zwzi2551bcz04nd3n46dv61x1z8b93mcj6gcma2ps1")))

(define-public crate-sloth-0.1.3 (c (n "sloth") (v "0.1.3") (h "1r2brwy2vrdddw66hxkfl7mgi5f6n09p6k1lpn4fs8v6lzfngk3c")))

(define-public crate-sloth-0.2.0 (c (n "sloth") (v "0.2.0") (h "1y303b36m8ljfjrjpsph8picxk6qaa03f36kc06ywj8z2hbh1hx7")))

