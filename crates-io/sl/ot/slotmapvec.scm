(define-module (crates-io sl ot slotmapvec) #:use-module (crates-io))

(define-public crate-slotmapvec-0.1.0 (c (n "slotmapvec") (v "0.1.0") (h "0jfxiww2d7w7ij21d6761cnm78v2pz0qxwnnb9l3q6765ga32f0g")))

(define-public crate-slotmapvec-0.1.1 (c (n "slotmapvec") (v "0.1.1") (h "1nqxxwvhbqmykkmpvamxd2aqqdynkb0df4rljyfifp63qjh6jq4z")))

(define-public crate-slotmapvec-0.1.2 (c (n "slotmapvec") (v "0.1.2") (h "09pjxg7b9fcaash8fimk047xwf9i8nfdxag2xkyd2kvysajwnjjz")))

(define-public crate-slotmapvec-0.1.3 (c (n "slotmapvec") (v "0.1.3") (h "0lxqcqdm1jgc2pll02z518zw49ag0afdpk0qalggyijzz7ikh22s")))

