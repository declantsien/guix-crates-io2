(define-module (crates-io sl ot slot-arena) #:use-module (crates-io))

(define-public crate-slot-arena-0.1.0 (c (n "slot-arena") (v "0.1.0") (h "0fjadi1iw1kypgxwn5c6d5cin8scck0c2hgrmmr7x2hawbr4r2qd")))

(define-public crate-slot-arena-0.1.1 (c (n "slot-arena") (v "0.1.1") (h "193hgcjsaz5gdd0npihjahwcla3zm6zscf50yyr47bjp6p4ds4pq")))

