(define-module (crates-io sl ot slotmap-fork-otter) #:use-module (crates-io))

(define-public crate-slotmap-fork-otter-1.0.2 (c (n "slotmap-fork-otter") (v "1.0.2") (d (list (d (n "fxhash") (r "^0.2.1") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive" "alloc"))) (o #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "version_check") (r "^0.9") (d #t) (k 1)))) (h "0fkcgfaibmp0ph2i8ki49jng01z19f81swnwr48ncqvifls20bgx") (f (quote (("unstable") ("std") ("default" "std"))))))

