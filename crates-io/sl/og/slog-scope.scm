(define-module (crates-io sl og slog-scope) #:use-module (crates-io))

(define-public crate-slog-scope-0.1.0 (c (n "slog-scope") (v "0.1.0") (d (list (d (n "crossbeam") (r "^0.2.9") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)) (d (n "slog") (r "^1") (d #t) (k 0)) (d (n "slog-term") (r "^1") (d #t) (k 2)))) (h "1gjmica67xkg0045dwfc1s9sry3d22ndkcaaf9fyy0s3srp1zcg6")))

(define-public crate-slog-scope-0.2.0 (c (n "slog-scope") (v "0.2.0") (d (list (d (n "crossbeam") (r "^0.2.9") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)) (d (n "slog") (r "^1") (d #t) (k 0)) (d (n "slog-term") (r "^1") (d #t) (k 2)))) (h "0x707sqahcf4iwkyfib9zkxahgybrfqvindq8shlx3iccdkq9qij")))

(define-public crate-slog-scope-0.2.2 (c (n "slog-scope") (v "0.2.2") (d (list (d (n "crossbeam") (r "^0.2.9") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)) (d (n "slog") (r "^1") (d #t) (k 0)) (d (n "slog-term") (r "^1") (d #t) (k 2)))) (h "0r4zhry8i2i5h7bicqh5dxbqfck0wmg2ggrqg1cv8mxkq4znwxaa")))

(define-public crate-slog-scope-0.3.0 (c (n "slog-scope") (v "0.3.0") (d (list (d (n "crossbeam") (r "^0.2.9") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)) (d (n "slog") (r ">= 2.0.0-1, < 2.0.0-2") (d #t) (k 0)) (d (n "slog-term") (r ">= 2.0.0-1, < 2.0.0-2") (d #t) (k 2)))) (h "0jzpxjqciz8nnza0nf9j23wxzrb9isvs7d045sg3zlwplss2chpv") (y #t)))

(define-public crate-slog-scope-2.0.0-1.0 (c (n "slog-scope") (v "2.0.0-1.0") (d (list (d (n "crossbeam") (r "^0.2.9") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)) (d (n "slog") (r ">= 2.0.0-1, < 2.0.0-2") (d #t) (k 0)) (d (n "slog-term") (r ">= 2.0.0-1, < 2.0.0-2") (d #t) (k 2)))) (h "1h0gwpsyvfx4hrmmvlfghvigsa87v2pv35af9n8ljgvk95h1ixjy")))

(define-public crate-slog-scope-2.0.0-2.0 (c (n "slog-scope") (v "2.0.0-2.0") (d (list (d (n "crossbeam") (r "^0.2.9") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)) (d (n "slog") (r "~2.0.0-2") (d #t) (k 0)) (d (n "slog-term") (r "~2.0.0-2") (d #t) (k 2)))) (h "08hfnnl56jjmqy4glgmyzniirnmwkx2h2v0jfqgnvl54f71ilckc")))

(define-public crate-slog-scope-2.0.0-3.0 (c (n "slog-scope") (v "2.0.0-3.0") (d (list (d (n "crossbeam") (r "^0.2.9") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)) (d (n "slog") (r "~2.0.0-3") (d #t) (k 0)) (d (n "slog-async") (r "~2.0.0-3") (d #t) (k 2)) (d (n "slog-term") (r "~2.0.0-3") (d #t) (k 2)))) (h "01ygzvik4yw0n12zfdk84k3hp4dy1a1izzch9mnx1dskcy9xd288")))

(define-public crate-slog-scope-2.0.0-3.1 (c (n "slog-scope") (v "2.0.0-3.1") (d (list (d (n "crossbeam") (r "^0.2.9") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)) (d (n "slog") (r "~2.0.0-3") (d #t) (k 0)) (d (n "slog-async") (r "~2.0.0-3") (d #t) (k 2)) (d (n "slog-term") (r "~2.0.0-3") (d #t) (k 2)))) (h "0chjq9qssffcj6nzx83z6plkwfmppfdjngw78ws8rssc4f158hxc")))

(define-public crate-slog-scope-2.0.0 (c (n "slog-scope") (v "2.0.0") (d (list (d (n "crossbeam") (r "^0.2.9") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)) (d (n "slog") (r "^2") (d #t) (k 0)) (d (n "slog-async") (r "^2") (d #t) (k 2)) (d (n "slog-term") (r "~2.0.0-4") (d #t) (k 2)))) (h "0k2jyx4i9d5pfi3rb4j5pq7xds774a42gglh4yyck1g7nyx9nw9v")))

(define-public crate-slog-scope-3.0.0 (c (n "slog-scope") (v "3.0.0") (d (list (d (n "crossbeam") (r "^0.2.9") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)) (d (n "slog") (r "^2") (d #t) (k 0)) (d (n "slog-async") (r "^2") (d #t) (k 2)) (d (n "slog-term") (r "~2.0.0-4") (d #t) (k 2)))) (h "1yhf1m203njj6l1lbhh0v5bnsx6w829jg9p3xpikbb2pb9mprm7c")))

(define-public crate-slog-scope-4.0.0 (c (n "slog-scope") (v "4.0.0") (d (list (d (n "crossbeam") (r "^0.2.9") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)) (d (n "slog") (r "^2") (d #t) (k 0)) (d (n "slog-async") (r "^2") (d #t) (k 2)) (d (n "slog-term") (r "~2.0.0-4") (d #t) (k 2)))) (h "1ib5v1sz0cihgxmvnz9sram2ax25qsfjhc1r039pw54ch7hkccnx")))

(define-public crate-slog-scope-4.0.1 (c (n "slog-scope") (v "4.0.1") (d (list (d (n "crossbeam") (r "^0.2.9") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)) (d (n "slog") (r "^2") (d #t) (k 0)) (d (n "slog-async") (r "^2") (d #t) (k 2)) (d (n "slog-term") (r "~2.0.0-4") (d #t) (k 2)))) (h "1px9cxghhdn9bmal1kr7jj08bi6pk2vdvvq5cgd24aqf9k4l8cq5")))

(define-public crate-slog-scope-4.1.0 (c (n "slog-scope") (v "4.1.0") (d (list (d (n "crossbeam") (r "^0.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2") (d #t) (k 0)) (d (n "slog") (r "^2") (d #t) (k 0)) (d (n "slog-async") (r "^2") (d #t) (k 2)) (d (n "slog-term") (r "~2.0.0-4") (d #t) (k 2)))) (h "16knya8kddhlqirqmfpdcl6d9hdn5akgh59mh53z3ncmfznr117v") (y #t)))

(define-public crate-slog-scope-4.1.1 (c (n "slog-scope") (v "4.1.1") (d (list (d (n "crossbeam") (r "^0.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2") (d #t) (k 0)) (d (n "slog") (r "^2.4") (d #t) (k 0)) (d (n "slog-async") (r "^2") (d #t) (k 2)) (d (n "slog-term") (r "~2.0.0-4") (d #t) (k 2)))) (h "16jkn74s5s3nlfl0kqf5qi144iqmrmxssb9crxf5j17s4r3lph30")))

(define-public crate-slog-scope-4.1.2 (c (n "slog-scope") (v "4.1.2") (d (list (d (n "arc-swap") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2") (d #t) (k 0)) (d (n "slog") (r "^2.4") (d #t) (k 0)) (d (n "slog-async") (r "^2") (d #t) (k 2)) (d (n "slog-term") (r "^2") (d #t) (k 2)))) (h "1gs6p6pjciy56d9gmzqc58bmjv56phmrghc7xjkmfvnl2iifrlyi")))

(define-public crate-slog-scope-4.2.0 (c (n "slog-scope") (v "4.2.0") (d (list (d (n "arc-swap") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2") (d #t) (k 0)) (d (n "slog") (r "^2.4") (d #t) (k 0)) (d (n "slog-async") (r "^2") (d #t) (k 2)) (d (n "slog-term") (r "^2") (d #t) (k 2)))) (h "085169imcmc8l5vf2w53y3471970z65wxn5h88p58fmp1ag839ak")))

(define-public crate-slog-scope-4.3.0 (c (n "slog-scope") (v "4.3.0") (d (list (d (n "arc-swap") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2") (d #t) (k 0)) (d (n "slog") (r "^2.4") (d #t) (k 0)) (d (n "slog-async") (r "^2") (d #t) (k 2)) (d (n "slog-term") (r "^2") (d #t) (k 2)))) (h "1riyabmd2qz984ff6dn5d540p17px99q6cdfs4vlbbmhv2fwhi3w")))

(define-public crate-slog-scope-4.4.0 (c (n "slog-scope") (v "4.4.0") (d (list (d (n "arc-swap") (r "^1.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2") (d #t) (k 0)) (d (n "slog") (r "^2.4") (d #t) (k 0)) (d (n "slog-async") (r "^2") (d #t) (k 2)) (d (n "slog-term") (r "^2") (d #t) (k 2)))) (h "11n7nd0g3iab8ahcwnxzpmchi4ycgjsq5nj9jn3d4k17qfsa959g")))

