(define-module (crates-io sl og slog-syslog) #:use-module (crates-io))

(define-public crate-slog-syslog-0.6.0 (c (n "slog-syslog") (v "0.6.0") (d (list (d (n "nix") (r "^0.6.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.19") (d #t) (k 0)) (d (n "slog") (r "^0.6.0") (d #t) (k 0)) (d (n "syslog") (r "^3.1.0") (d #t) (k 0)))) (h "01nwgh4cmd3rhphg2qm2d27425rr042fbg1nw682hwr4kvn9zv59")))

(define-public crate-slog-syslog-0.7.0 (c (n "slog-syslog") (v "0.7.0") (d (list (d (n "nix") (r "^0.6.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.19") (d #t) (k 0)) (d (n "slog") (r "^0.7.0") (d #t) (k 0)) (d (n "syslog") (r "^3.1.0") (d #t) (k 0)))) (h "081rr7fwzss8xxbi7c30jiba0nbsrsh0nm36x7qlhmc9bsdil8w9")))

(define-public crate-slog-syslog-1.0.0-alpha (c (n "slog-syslog") (v "1.0.0-alpha") (d (list (d (n "nix") (r "^0.6.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.19") (d #t) (k 0)) (d (n "slog") (r "^1.0.0-alpha") (d #t) (k 0)) (d (n "syslog") (r "^3.1.0") (d #t) (k 0)))) (h "10sx3vhgb25sppfrb733k94f22ln5ghlg55i9fdhyqijzj6r81zm") (y #t)))

(define-public crate-slog-syslog-1.0.0-alpha2 (c (n "slog-syslog") (v "1.0.0-alpha2") (d (list (d (n "nix") (r "^0.6.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.19") (d #t) (k 0)) (d (n "slog") (r "^1.0.0-alpha2") (d #t) (k 0)) (d (n "syslog") (r "^3.1.0") (d #t) (k 0)))) (h "1jf30f4n5p64fa1r8pzlc62b3wqqalwaj7pam20jzwcz0pgda22i") (y #t)))

(define-public crate-slog-syslog-1.0.0-alpha3 (c (n "slog-syslog") (v "1.0.0-alpha3") (d (list (d (n "nix") (r "^0.6.0") (d #t) (k 0)) (d (n "slog") (r "^1.0.0-alpha3") (d #t) (k 0)) (d (n "syslog") (r "^3.1.0") (d #t) (k 0)))) (h "0y1bxbk5zcfg7hl6q2kscji6rcdxyq37z2ksnly0d1f8z6bihls7") (y #t)))

(define-public crate-slog-syslog-1.0.0-alpha6 (c (n "slog-syslog") (v "1.0.0-alpha6") (d (list (d (n "nix") (r "^0.6.0") (d #t) (k 0)) (d (n "slog") (r "^1.0.0-alpha6") (d #t) (k 0)) (d (n "slog-stream") (r "^1.0.0-alpha6") (d #t) (k 0)) (d (n "syslog") (r "^3.1.0") (d #t) (k 0)))) (h "0pqnvila2dzlz4jgbgd9bf894bl87k3afm8c5y645lk6a4480h6d") (y #t)))

(define-public crate-slog-syslog-1.0.0-alpha7 (c (n "slog-syslog") (v "1.0.0-alpha7") (d (list (d (n "nix") (r "^0.6.0") (d #t) (k 0)) (d (n "slog") (r "^1.0.0-alpha7") (d #t) (k 0)) (d (n "slog-stream") (r "^1.0.0-alpha7") (d #t) (k 0)) (d (n "syslog") (r "^3.1.0") (d #t) (k 0)))) (h "0f4gg5my13zfjq4a3b360ajbp33b5fnzsfljd6jnc0wb8130l857") (y #t)))

(define-public crate-slog-syslog-1.0.0-alpha8 (c (n "slog-syslog") (v "1.0.0-alpha8") (d (list (d (n "nix") (r "^0.6.0") (d #t) (k 0)) (d (n "slog") (r "^1") (d #t) (k 0)) (d (n "slog-stream") (r "^1.0.0-alpha8") (d #t) (k 0)) (d (n "syslog") (r "^3.1.0") (d #t) (k 0)))) (h "15vfxlzm83ig2qchjn1jldq117hgd4ak5dw2mi506qsgr34nldf4") (y #t)))

(define-public crate-slog-syslog-1.0.0-alpha9 (c (n "slog-syslog") (v "1.0.0-alpha9") (d (list (d (n "nix") (r "^0.6.0") (d #t) (k 0)) (d (n "slog") (r "^1.1") (d #t) (k 0)) (d (n "slog-stream") (r "^1.1") (d #t) (k 0)) (d (n "syslog") (r "^3.1.0") (d #t) (k 0)))) (h "1lbwkhzd9p5vk1764gjy8ih5lb2xl6skpv3c8sd1qglkc1di2wi9") (y #t)))

(define-public crate-slog-syslog-0.8.0 (c (n "slog-syslog") (v "0.8.0") (d (list (d (n "nix") (r "^0.6.0") (d #t) (k 0)) (d (n "slog") (r "^1.1") (d #t) (k 0)) (d (n "slog-stream") (r "^1.1") (d #t) (k 0)) (d (n "syslog") (r "^3.1.0") (d #t) (k 0)))) (h "0yp9vih6h6526xcrnwsfidydvpdb3d553zj2lnldb5mlz6c68vd5")))

(define-public crate-slog-syslog-0.8.1 (c (n "slog-syslog") (v "0.8.1") (d (list (d (n "nix") (r "^0.6.0") (d #t) (k 0)) (d (n "slog") (r "^1.1") (d #t) (k 0)) (d (n "slog-stream") (r "^1.1") (d #t) (k 0)) (d (n "syslog") (r "^3.1.0") (d #t) (k 0)))) (h "04d6c390pnqb9i0l205d7maw5247kgh1gbv8l4vgz72pirln0l8l")))

(define-public crate-slog-syslog-1.0.0-alpha10 (c (n "slog-syslog") (v "1.0.0-alpha10") (d (list (d (n "nix") (r "^0.6.0") (d #t) (k 0)) (d (n "slog") (r "^1.1") (d #t) (k 0)) (d (n "slog-stream") (r "^1.1") (d #t) (k 0)) (d (n "syslog") (r "^3.1.0") (d #t) (k 0)))) (h "03r6wq7azpq5raszp4im8y7dwx5rzbfj1fkbkjvh6lapsfwd89i7") (y #t)))

(define-public crate-slog-syslog-1.0.0-alpha11 (c (n "slog-syslog") (v "1.0.0-alpha11") (d (list (d (n "nix") (r "^0.6.0") (d #t) (k 0)) (d (n "slog") (r "^1.1") (d #t) (k 0)) (d (n "slog-stream") (r "^1.1") (d #t) (k 0)) (d (n "syslog") (r "^3.1.0") (d #t) (k 0)))) (h "0xn0m2fg0zmgazx3wk6dcy96fjjbvndy58rx514y56g0l1zyxn5d") (y #t)))

(define-public crate-slog-syslog-1.0.0-alpha9.1 (c (n "slog-syslog") (v "1.0.0-alpha9.1") (d (list (d (n "nix") (r "^0.6.0") (d #t) (k 0)) (d (n "slog") (r "^1.1") (d #t) (k 0)) (d (n "slog-stream") (r "^1.1") (d #t) (k 0)) (d (n "syslog") (r "^3.1.0") (d #t) (k 0)))) (h "0a607xl2ncx238k9qiv6mb1ip3l633r5ddka7cy1mrrh7s2mi294") (y #t)))

(define-public crate-slog-syslog-0.8.2 (c (n "slog-syslog") (v "0.8.2") (d (list (d (n "nix") (r "^0.7.0") (d #t) (k 0)) (d (n "slog") (r "^1.1") (d #t) (k 0)) (d (n "slog-stream") (r "^1.1") (d #t) (k 0)) (d (n "syslog") (r "^3.2.0") (d #t) (k 0)))) (h "1vpq6z452g91ci82cmc46s6qxybz8f9pisch56q3hip6329pcwq5")))

(define-public crate-slog-syslog-0.9.0 (c (n "slog-syslog") (v "0.9.0") (d (list (d (n "nix") (r "^0.8.0") (d #t) (k 0)) (d (n "slog") (r "~2.0.0-2") (d #t) (k 0)) (d (n "syslog") (r "^3.2.0") (d #t) (k 0)))) (h "19c3ajcsxsjjjq5p42nwaqhhsxf0xy9kzax9xg0md11js8zfzaaq")))

(define-public crate-slog-syslog-0.10.0 (c (n "slog-syslog") (v "0.10.0") (d (list (d (n "nix") (r "^0.8.0") (d #t) (k 0)) (d (n "slog") (r "~2.0.0-3") (d #t) (k 0)) (d (n "syslog") (r "^3.2.0") (d #t) (k 0)))) (h "0fh3zr1l2ykd8dq63qgsa9xzb4pcjc7rqzgjyjz323rgrbax8xvg")))

(define-public crate-slog-syslog-0.11.0 (c (n "slog-syslog") (v "0.11.0") (d (list (d (n "nix") (r "^0.8.0") (d #t) (k 0)) (d (n "slog") (r "^2") (d #t) (k 0)) (d (n "syslog") (r "^3.2.0") (d #t) (k 0)))) (h "0y0m3wr1gbrhxx13xk40inp7j3gdhs1liyjia4431hzv20g8rd53")))

(define-public crate-slog-syslog-0.12.0 (c (n "slog-syslog") (v "0.12.0") (d (list (d (n "nix") (r "^0.14.0") (d #t) (k 0)) (d (n "slog") (r "^2.1.1") (d #t) (k 0)) (d (n "syslog") (r "^3.3.0") (d #t) (k 0)))) (h "1p89jrc1apx5kj0vsfkirs5nvnc75b3mgqrvmsk51sl8c74br94s")))

(define-public crate-slog-syslog-0.13.0 (c (n "slog-syslog") (v "0.13.0") (d (list (d (n "slog") (r "^2.1.1") (d #t) (k 0)) (d (n "syslog") (r "^5.0") (d #t) (k 0)))) (h "0j55gnk127rim5n7c97nagqd6n19kh3xqh584jwk38s30bsnzfi8")))

