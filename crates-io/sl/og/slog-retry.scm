(define-module (crates-io sl og slog-retry) #:use-module (crates-io))

(define-public crate-slog-retry-0.1.0 (c (n "slog-retry") (v "0.1.0") (d (list (d (n "failure") (r "~0.1.1") (d #t) (k 0)) (d (n "slog") (r "~2.0") (d #t) (k 0)) (d (n "slog-async") (r "~2") (d #t) (k 2)) (d (n "slog-json") (r "~2") (d #t) (k 2)) (d (n "version-sync") (r "~0.5") (d #t) (k 2)))) (h "1xj332a09cf9xi8m30cia1a3kp7k288avrhdnvdp09dhzyxc6vjf")))

(define-public crate-slog-retry-0.1.1 (c (n "slog-retry") (v "0.1.1") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "slog") (r "^2.0") (d #t) (k 0)) (d (n "slog-async") (r "^2.3") (d #t) (k 2)) (d (n "slog-json") (r "^2.2") (d #t) (k 2)) (d (n "version-sync") (r "~0.5") (d #t) (k 2)))) (h "1h9628nzg00l3gishgz44qpz9ksh2ssfywh8v043zp5qyklvxds0")))

