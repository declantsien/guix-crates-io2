(define-module (crates-io sl og slog-logfmt) #:use-module (crates-io))

(define-public crate-slog-logfmt-0.0.1 (c (n "slog-logfmt") (v "0.0.1") (d (list (d (n "slog") (r "^2.5.0") (d #t) (k 0)) (d (n "slog-async") (r "^2.4.0") (d #t) (k 2)))) (h "16zis34gdqvz3i06qa0c496r215yxpwmzg7ps2a7ra13di5mdss8")))

(define-public crate-slog-logfmt-0.1.0 (c (n "slog-logfmt") (v "0.1.0") (d (list (d (n "slog") (r "^2.5.0") (d #t) (k 0)) (d (n "slog-async") (r "^2.4.0") (d #t) (k 2)))) (h "0y32jnhxv0sdblbgsshm4yrb76518iyfi4mldmdkhsqj3mc6vq6q")))

