(define-module (crates-io sl og slog_unwraps) #:use-module (crates-io))

(define-public crate-slog_unwraps-0.1.0 (c (n "slog_unwraps") (v "0.1.0") (d (list (d (n "backtrace") (r "^0.3.14") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 2)) (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)) (d (n "regex") (r "^1.1.2") (d #t) (k 0)) (d (n "slog") (r "^2.4.1") (d #t) (k 0)) (d (n "slog-term") (r "^2.4.0") (d #t) (k 2)))) (h "0a6kzssifkc8q066x0cfzyyc0ra1f8qxi1811ahc2xjzqs2dbdj9")))

(define-public crate-slog_unwraps-0.1.1 (c (n "slog_unwraps") (v "0.1.1") (d (list (d (n "backtrace") (r "^0.3.14") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)) (d (n "regex") (r "^1.1.2") (d #t) (k 0)) (d (n "slog") (r "^2.4.1") (d #t) (k 0)) (d (n "slog-term") (r "^2.4.0") (d #t) (k 2)))) (h "0lsh2q0dgwlqzzcyqk4r2k2a2dplz2m6vjdsy904jr9ya3mczn1p")))

(define-public crate-slog_unwraps-0.1.2 (c (n "slog_unwraps") (v "0.1.2") (d (list (d (n "backtrace") (r "^0.3.14") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)) (d (n "regex") (r "^1.1.2") (d #t) (k 0)) (d (n "slog") (r "^2.4.1") (d #t) (k 0)) (d (n "slog-term") (r "^2.4.0") (d #t) (k 2)))) (h "1mfh0pxs50fs266pj55v2bpm14zkylizm52vr6zb02d61jxf7s1y")))

(define-public crate-slog_unwraps-0.1.3 (c (n "slog_unwraps") (v "0.1.3") (d (list (d (n "backtrace") (r "^0.3.14") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)) (d (n "regex") (r "^1.1.2") (d #t) (k 0)) (d (n "slog") (r "^2.4.1") (d #t) (k 0)) (d (n "slog-term") (r "^2.4.0") (d #t) (k 2)))) (h "1nfm9126ay5m0ippnyl8zmwg6fbl786m7i8k17n2jwj06gr17q15")))

(define-public crate-slog_unwraps-0.1.4 (c (n "slog_unwraps") (v "0.1.4") (d (list (d (n "backtrace") (r "^0.3.14") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)) (d (n "regex") (r "^1.1.2") (d #t) (k 0)) (d (n "slog") (r "^2.4.1") (d #t) (k 0)) (d (n "slog-term") (r "^2.4.0") (d #t) (k 2)))) (h "0pzwap65bf8nmi72av83aj7h3gkp7m380hgax8w9cg0zm87hr44v")))

(define-public crate-slog_unwraps-0.1.5 (c (n "slog_unwraps") (v "0.1.5") (d (list (d (n "backtrace") (r "^0.3.14") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)) (d (n "regex") (r "^1.1.2") (d #t) (k 0)) (d (n "slog") (r "^2.4.1") (d #t) (k 0)) (d (n "slog-term") (r "^2.4.0") (d #t) (k 2)))) (h "0dkn63nrq7rf6d5xdnxdi8a02vjai6a6xg2q1082riw2p8n2sxq9")))

