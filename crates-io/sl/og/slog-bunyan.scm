(define-module (crates-io sl og slog-bunyan) #:use-module (crates-io))

(define-public crate-slog-bunyan-0.6.0 (c (n "slog-bunyan") (v "0.6.0") (d (list (d (n "chrono") (r "^0.2.22") (d #t) (k 0)) (d (n "nix") (r "^0.6.0") (d #t) (k 0)) (d (n "slog") (r "^0.6.0") (d #t) (k 0)) (d (n "slog-json") (r "^0.6.0") (d #t) (k 0)))) (h "0r2iilvh2qsb9pd0nkavnjwibm0r19k1vxfg9afarx1ix9llx347")))

(define-public crate-slog-bunyan-0.7.0 (c (n "slog-bunyan") (v "0.7.0") (d (list (d (n "chrono") (r "^0.2.22") (d #t) (k 0)) (d (n "nix") (r "^0.6.0") (d #t) (k 0)) (d (n "slog") (r "^0.7.0") (d #t) (k 0)) (d (n "slog-json") (r "^0.7.0") (d #t) (k 0)))) (h "0zgxgigjs1jjz0b9jdmv7hgj4pj9hwdgpbq40ajc3q2kgirjjr63")))

(define-public crate-slog-bunyan-1.0.0-alpha (c (n "slog-bunyan") (v "1.0.0-alpha") (d (list (d (n "chrono") (r "^0.2.22") (d #t) (k 0)) (d (n "nix") (r "^0.6.0") (d #t) (k 0)) (d (n "slog") (r "^1.0.0-alpha") (d #t) (k 0)) (d (n "slog-json") (r "^1.0.0-alpha") (d #t) (k 0)))) (h "1p0hmcfsgm7iambxpj4lr3i29p6wqdmsxmv6mjpnz8dvsil9dj1n")))

(define-public crate-slog-bunyan-1.0.0-alpha2 (c (n "slog-bunyan") (v "1.0.0-alpha2") (d (list (d (n "chrono") (r "^0.2.22") (d #t) (k 0)) (d (n "nix") (r "^0.6.0") (d #t) (k 0)) (d (n "slog") (r "^1.0.0-alpha2") (d #t) (k 0)) (d (n "slog-json") (r "^1.0.0-alpha2") (d #t) (k 0)))) (h "0rmgac8gmrl996y0dl5x054lpc0yl5p314dhpgy5dhvy2rqrg3s7")))

(define-public crate-slog-bunyan-1.0.0-alpha3 (c (n "slog-bunyan") (v "1.0.0-alpha3") (d (list (d (n "chrono") (r "^0.2.22") (d #t) (k 0)) (d (n "nix") (r "^0.6.0") (d #t) (k 0)) (d (n "slog") (r "^1.0.0-alpha3") (d #t) (k 0)) (d (n "slog-json") (r "^1.0.0-alpha3") (d #t) (k 0)))) (h "0ksrlwmq22xvxcp82lrp56p3caqis8pfafrfaxa684dgv2m9cn1a")))

(define-public crate-slog-bunyan-1.0.0-alpha6 (c (n "slog-bunyan") (v "1.0.0-alpha6") (d (list (d (n "chrono") (r "^0.2.22") (d #t) (k 0)) (d (n "nix") (r "^0.6.0") (d #t) (k 0)) (d (n "slog") (r "^1.0.0-alpha6") (d #t) (k 0)) (d (n "slog-json") (r "^1.0.0-alpha6") (d #t) (k 0)) (d (n "slog-stream") (r "^1.0.0-alpha6") (d #t) (k 2)))) (h "00lv2g91hb3znb5nq6v0kgraqnqd3k4rvy5qysb1siymq18v4v07")))

(define-public crate-slog-bunyan-1.0.0-alpha7 (c (n "slog-bunyan") (v "1.0.0-alpha7") (d (list (d (n "chrono") (r "^0.2.22") (d #t) (k 0)) (d (n "nix") (r "^0.6.0") (d #t) (k 0)) (d (n "slog") (r "^1.0.0-alpha7") (d #t) (k 0)) (d (n "slog-json") (r "^1.0.0-alpha7") (d #t) (k 0)) (d (n "slog-stream") (r "^1.0.0-alpha7") (d #t) (k 2)))) (h "1a32sba0k6ycfdlzbs722m4s00gcqx3s71b1q15j05ai81vdkfmd")))

(define-public crate-slog-bunyan-1.0.0-alpha8 (c (n "slog-bunyan") (v "1.0.0-alpha8") (d (list (d (n "chrono") (r "^0.2.22") (d #t) (k 0)) (d (n "nix") (r "^0.6.0") (d #t) (k 0)) (d (n "slog") (r "^1") (d #t) (k 0)) (d (n "slog-json") (r "^1.0.0-alpha8") (d #t) (k 0)) (d (n "slog-stream") (r "^1.0.0-alpha8") (d #t) (k 2)))) (h "1b2iq4ahwkhbfgqaia2mnznpmszqcjgc2ma4a19yx0an51i3d0gh")))

(define-public crate-slog-bunyan-1.0.0 (c (n "slog-bunyan") (v "1.0.0") (d (list (d (n "chrono") (r "^0.2.22") (d #t) (k 0)) (d (n "nix") (r "^0.6.0") (d #t) (k 0)) (d (n "slog") (r "^1") (d #t) (k 0)) (d (n "slog-json") (r "^1") (d #t) (k 0)) (d (n "slog-stream") (r "^1") (d #t) (k 2)))) (h "05fwa5g1s9xz52mx52crzkvrihlwvbd84w9gc7n62rwr19z4p92k")))

(define-public crate-slog-bunyan-1.1.0 (c (n "slog-bunyan") (v "1.1.0") (d (list (d (n "chrono") (r "^0.2.22") (d #t) (k 0)) (d (n "nix") (r "^0.6.0") (d #t) (k 0)) (d (n "slog") (r "^1.1") (d #t) (k 0)) (d (n "slog-json") (r "^1") (d #t) (k 0)) (d (n "slog-stream") (r "^1") (d #t) (k 2)))) (h "1wqq7j5ahfg2b5nin5blsyc7y7fh461kwhr4sx2q3bsypivp64bn")))

(define-public crate-slog-bunyan-1.1.1 (c (n "slog-bunyan") (v "1.1.1") (d (list (d (n "chrono") (r "^0.2.22") (d #t) (k 0)) (d (n "nix") (r "^0.6.0") (d #t) (k 0)) (d (n "slog") (r "^1.1") (d #t) (k 0)) (d (n "slog-json") (r "^1") (d #t) (k 0)) (d (n "slog-stream") (r "^1.2") (d #t) (k 2)))) (h "1awidl71li8fmlia4n6y815xr4ncizb4g84pr0k5ix62b5lmv7ic")))

(define-public crate-slog-bunyan-2.0.0-1.0 (c (n "slog-bunyan") (v "2.0.0-1.0") (d (list (d (n "chrono") (r "^0.2.22") (d #t) (k 0)) (d (n "nix") (r "^0.6.0") (d #t) (k 0)) (d (n "slog") (r ">= 2.0.0-1.0, < 2.0.0-2") (d #t) (k 0)) (d (n "slog-json") (r ">= 2.0.0-1.0, < 2.0.0-2") (d #t) (k 0)))) (h "1wd5wrfvi8lqr4n7qx8n4xkmbm8sjc8g3d72jgpmj57mz9varwa2")))

(define-public crate-slog-bunyan-2.0.0-2.0 (c (n "slog-bunyan") (v "2.0.0-2.0") (d (list (d (n "chrono") (r "^0.2.22") (d #t) (k 0)) (d (n "nix") (r "^0.6.0") (d #t) (k 0)) (d (n "slog") (r "~2.0.0-2") (d #t) (k 0)) (d (n "slog-json") (r "~2.0.0-2") (d #t) (k 0)))) (h "1ymljggkm4rzf6vk1q7jnxsj17zbn0198n53g5nq3ggq0bkm8swf")))

(define-public crate-slog-bunyan-2.0.0-3.0 (c (n "slog-bunyan") (v "2.0.0-3.0") (d (list (d (n "chrono") (r "^0.2.22") (d #t) (k 0)) (d (n "nix") (r "^0.6.0") (d #t) (k 0)) (d (n "slog") (r "~2.0.0-3") (d #t) (k 0)) (d (n "slog-json") (r "~2.0.0-3") (d #t) (k 0)))) (h "106mssmigsnbzklwsqi48wbvppskvcv2115jyh7lc599nwxas943")))

(define-public crate-slog-bunyan-2.0.0-4.0 (c (n "slog-bunyan") (v "2.0.0-4.0") (d (list (d (n "chrono") (r "^0.3.0") (d #t) (k 0)) (d (n "nix") (r "^0.8.0") (d #t) (k 0)) (d (n "slog") (r "^2") (d #t) (k 0)) (d (n "slog-json") (r "~2.0.0-4") (d #t) (k 0)))) (h "1gil41wwkbbixmbfc1q6276hrxpw4r35ycpqmxbn02djayq7ndf6")))

(define-public crate-slog-bunyan-2.0.0 (c (n "slog-bunyan") (v "2.0.0") (d (list (d (n "chrono") (r "^0.3.0") (d #t) (k 0)) (d (n "nix") (r "^0.8.0") (d #t) (k 0)) (d (n "slog") (r "^2") (d #t) (k 0)) (d (n "slog-json") (r "^2") (d #t) (k 0)))) (h "1skl89k6dd2i8aqvbp2qdf6pq97gyn35mdagpg28i5hwvg27cxn7")))

(define-public crate-slog-bunyan-2.1.0 (c (n "slog-bunyan") (v "2.1.0") (d (list (d (n "chrono") (r "^0.3.0") (d #t) (k 0)) (d (n "nix") (r "^0.8.0") (d #t) (k 0)) (d (n "slog") (r "^2") (d #t) (k 0)) (d (n "slog-json") (r "^2") (d #t) (k 0)))) (h "0f99avg7qg4apr6l3hqlwx7yxlxz4wlzxh2s0plzx5dbfiy7xmfa")))

(define-public crate-slog-bunyan-2.2.0 (c (n "slog-bunyan") (v "2.2.0") (d (list (d (n "chrono") (r "^0.3.0") (d #t) (k 0)) (d (n "hostname") (r "^0.1.5") (d #t) (k 0)) (d (n "slog") (r "^2") (d #t) (k 0)) (d (n "slog-json") (r "^2") (d #t) (k 0)))) (h "0nia4fv9l87p7rx4fvnacmp6ir6ad57hjgpl7j4529sv95bsgjl3")))

(define-public crate-slog-bunyan-2.3.0 (c (n "slog-bunyan") (v "2.3.0") (d (list (d (n "chrono") (r "^0.4.11") (d #t) (k 0)) (d (n "hostname") (r "^0.3.0") (d #t) (k 0)) (d (n "slog") (r "^2") (d #t) (k 0)) (d (n "slog-json") (r "^2") (d #t) (k 0)))) (h "186xq05x7374i6i2pwcrhgp223lr491h505dys2f8s8hba5mwkwj")))

(define-public crate-slog-bunyan-2.4.0 (c (n "slog-bunyan") (v "2.4.0") (d (list (d (n "hostname") (r "^0.3.0") (d #t) (k 0)) (d (n "slog") (r "^2") (d #t) (k 0)) (d (n "slog-json") (r "^2.6") (d #t) (k 0)) (d (n "time") (r "^0.3.6") (f (quote ("formatting" "local-offset" "macros"))) (d #t) (k 0)))) (h "0zgr19iaqhcvgqcmfwx0ba87z16vxdi0nv6pk17ixhr30hnx63s4") (r "1.53")))

(define-public crate-slog-bunyan-2.5.0 (c (n "slog-bunyan") (v "2.5.0") (d (list (d (n "hostname") (r "^0.3.0") (d #t) (k 0)) (d (n "slog") (r "^2") (d #t) (k 0)) (d (n "slog-json") (r "^2.6") (d #t) (k 0)) (d (n "time") (r "^0.3.15") (f (quote ("formatting" "local-offset" "macros"))) (d #t) (k 0)))) (h "147i84jl8l3fhd2f7diq20m2bvql692bqwhy3x0z1lw9hzkgdanw") (f (quote (("nested-values" "slog-json/nested-values")))) (r "1.59")))

