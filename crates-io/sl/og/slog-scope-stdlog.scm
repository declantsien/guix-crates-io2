(define-module (crates-io sl og slog-scope-stdlog) #:use-module (crates-io))

(define-public crate-slog-scope-stdlog-0.2.0 (c (n "slog-scope-stdlog") (v "0.2.0") (d (list (d (n "crossbeam") (r "^0.2.9") (d #t) (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "slog") (r "^1.1") (d #t) (k 0)) (d (n "slog-scope") (r "^0.2.2") (d #t) (k 0)) (d (n "slog-term") (r "^1.3") (d #t) (k 0)))) (h "0qi0dyy92jnhwy09vg0w0374agxwwd71gwkamwzfsb4lvsm4ab22")))

