(define-module (crates-io sl og slog-json-concur) #:use-module (crates-io))

(define-public crate-slog-json-concur-0.1.0 (c (n "slog-json-concur") (v "0.1.0") (d (list (d (n "erased-serde") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "num_cpus") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "slog") (r "^2.1.1") (d #t) (k 0)) (d (n "slog-json") (r "^2.6.1") (d #t) (k 2)) (d (n "time") (r "^0.3.6") (f (quote ("formatting"))) (d #t) (k 0)))) (h "005957h438g28rwffy3k2xj1rncnk1z5c25hzflrpfmq3qiwfzfy") (f (quote (("nested-values" "erased-serde" "slog/nested-values") ("dynamic-keys" "slog/dynamic-keys") ("default")))) (r "1.53")))

