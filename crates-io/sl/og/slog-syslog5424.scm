(define-module (crates-io sl og slog-syslog5424) #:use-module (crates-io))

(define-public crate-slog-syslog5424-0.1.0 (c (n "slog-syslog5424") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "slog") (r "^2.3") (d #t) (k 0)) (d (n "syslog5424") (r "^0.1") (d #t) (k 0)))) (h "0cjhp7mb782dys8qb9bg0s19rf0ipsl909jh5a6i34pwk6h1x1qs")))

(define-public crate-slog-syslog5424-0.1.1 (c (n "slog-syslog5424") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "slog") (r "^2.3") (d #t) (k 0)) (d (n "syslog5424") (r "^0.1") (d #t) (k 0)))) (h "14irlg1932skci93jfx7qg2gvl5nzi130ffkj83v385nq5h9ncp8")))

