(define-module (crates-io sl og slog-serde) #:use-module (crates-io))

(define-public crate-slog-serde-0.5.0 (c (n "slog-serde") (v "0.5.0") (d (list (d (n "serde") (r "^0.7.10") (d #t) (k 0)) (d (n "slog") (r "^0.5.0") (d #t) (k 0)))) (h "06knimjm9g5q5iy5lkyjiw0vp67mfpn6nbp917k3xfywqdlfgkxl")))

(define-public crate-slog-serde-0.6.0 (c (n "slog-serde") (v "0.6.0") (d (list (d (n "serde") (r "^0.7.10") (d #t) (k 0)) (d (n "slog") (r "^0.6.0") (d #t) (k 0)))) (h "0gsi4l2r2k12m24cnml0jikhwqmha7q7pg1vy5zhzrfwciddjgiv")))

(define-public crate-slog-serde-0.7.0 (c (n "slog-serde") (v "0.7.0") (d (list (d (n "serde") (r "^0.7.10") (d #t) (k 0)) (d (n "slog") (r "^0.7.0") (d #t) (k 0)))) (h "0f2n47qwqmrryxg64y86zi8cv5and3q905vnfn3kg3qpxql2rv64")))

(define-public crate-slog-serde-1.0.0-alpha (c (n "slog-serde") (v "1.0.0-alpha") (d (list (d (n "serde") (r "^0.7.10") (d #t) (k 0)) (d (n "slog") (r "^1.0.0-alpha") (d #t) (k 0)))) (h "0ks117xmw7d4wr8zvbn4gy1hff56sy79l6yhm8963cd0ikfm01aa")))

(define-public crate-slog-serde-1.0.0-alpha2 (c (n "slog-serde") (v "1.0.0-alpha2") (d (list (d (n "serde") (r "^0.7.10") (d #t) (k 0)) (d (n "slog") (r "^1.0.0-alpha2") (d #t) (k 0)))) (h "1zs95dgyrx0qd80irs1ggaqpsxmfnvxc4n1m07n735rn7fsl7ly4")))

(define-public crate-slog-serde-1.0.0-alpha3 (c (n "slog-serde") (v "1.0.0-alpha3") (d (list (d (n "serde") (r "^0.7.10") (d #t) (k 0)) (d (n "slog") (r "^1.0.0-alpha3") (d #t) (k 0)))) (h "16k167bmci0pdgl412w9i9zb0kb488w4h4vcd5g33rzl7lc0dzab")))

(define-public crate-slog-serde-1.0.0-alpha6 (c (n "slog-serde") (v "1.0.0-alpha6") (d (list (d (n "serde") (r "^0.7.10") (d #t) (k 0)) (d (n "slog") (r "^1.0.0-alpha6") (d #t) (k 0)))) (h "1vyd9mn7jm5426n6jwfz6viczlj412cp04ssns7insh6vkp2ajmj")))

(define-public crate-slog-serde-1.0.0-alpha7 (c (n "slog-serde") (v "1.0.0-alpha7") (d (list (d (n "serde") (r "^0.7.10") (d #t) (k 0)) (d (n "slog") (r "^1.0.0-alpha7") (d #t) (k 0)))) (h "1a03cp5wxb6il29x5h62icn7rx4mfkp1njnnccnkwi4zxrkxnjgx")))

(define-public crate-slog-serde-1.0.0-alpha8 (c (n "slog-serde") (v "1.0.0-alpha8") (d (list (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "slog") (r "^1") (d #t) (k 0)))) (h "0simp7hhpqcsywki2d0a6vcyw9w7sz37bv8pnjm4jg3i4kyj9k9x")))

(define-public crate-slog-serde-1.0.0-alpha9 (c (n "slog-serde") (v "1.0.0-alpha9") (d (list (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "slog") (r "^1.3") (d #t) (k 0)))) (h "10raz3dn0gd8108ni0w3sw3kxlwqvbmbq67lqqkzk2yxlwbyxdi4")))

