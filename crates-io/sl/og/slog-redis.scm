(define-module (crates-io sl og slog-redis) #:use-module (crates-io))

(define-public crate-slog-redis-0.1.0 (c (n "slog-redis") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "r2d2") (r "^0.8.3") (d #t) (k 0)) (d (n "r2d2_redis") (r "^0.8.0") (d #t) (k 0)) (d (n "redis") (r "^0.9.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.57") (d #t) (k 0)) (d (n "slog") (r "^2.4.1") (f (quote ("max_level_trace" "release_max_level_info"))) (d #t) (k 0)))) (h "05gkwnxmk6xl0llnvm8v0y7v7m5hjmy64hzga6f1dnmdanznz899")))

(define-public crate-slog-redis-0.1.1 (c (n "slog-redis") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "r2d2") (r "^0.8.9") (d #t) (k 0)) (d (n "r2d2_redis") (r "^0.14.0") (d #t) (k 0)) (d (n "redis") (r "^0.20.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "slog") (r "^2.7.0") (f (quote ("max_level_trace" "release_max_level_info"))) (d #t) (k 0)) (d (n "slog-async") (r "^2.6.0") (d #t) (k 2)))) (h "1yb7sd602cclplj99q6hlqi2fmy7xdzj97ipisixxiqzazd8ag1c")))

(define-public crate-slog-redis-0.1.2 (c (n "slog-redis") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "r2d2") (r "^0.8.9") (d #t) (k 0)) (d (n "r2d2_redis") (r "^0.14.0") (d #t) (k 0)) (d (n "redis") (r "^0.20.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "slog") (r "^2.7.0") (f (quote ("max_level_trace" "release_max_level_info"))) (d #t) (k 0)) (d (n "slog-async") (r "^2.6.0") (d #t) (k 2)))) (h "0s19ldyn2cjs6jcjzmbnx2hdh4q0c3380l481178s88b8357cz2c")))

