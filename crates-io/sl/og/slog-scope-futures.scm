(define-module (crates-io sl og slog-scope-futures) #:use-module (crates-io))

(define-public crate-slog-scope-futures-0.1.0 (c (n "slog-scope-futures") (v "0.1.0") (d (list (d (n "slog") (r "^2.4.1") (d #t) (k 0)) (d (n "slog-scope") (r "^4.1.1") (d #t) (k 0)))) (h "1w0hg4c0lx7k94q0mdhgjslqv54ichxn8p34k2g13ndvg7fg2mld")))

(define-public crate-slog-scope-futures-0.1.1 (c (n "slog-scope-futures") (v "0.1.1") (d (list (d (n "slog") (r "^2.4.1") (d #t) (k 0)) (d (n "slog-scope") (r "^4.1.1") (d #t) (k 0)))) (h "1jlfj4jpvw672wfm78lz7wc5phqnzhr5lbk7jxqdr95djjv5bbd0")))

