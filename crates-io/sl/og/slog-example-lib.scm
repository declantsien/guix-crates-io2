(define-module (crates-io sl og slog-example-lib) #:use-module (crates-io))

(define-public crate-slog-example-lib-0.1.0 (c (n "slog-example-lib") (v "0.1.0") (d (list (d (n "slog") (r "^1.0.0-alpha6") (d #t) (k 0)) (d (n "slog-stdlog") (r "^1.0.0-alpha6") (d #t) (k 0)))) (h "104051qvhc7v7x1fazxv8y95ynkh34axc8bkn4cxqrl1l77dmr93")))

(define-public crate-slog-example-lib-0.2.0 (c (n "slog-example-lib") (v "0.2.0") (d (list (d (n "env_logger") (r "^0.3.5") (d #t) (k 2)) (d (n "slog") (r "^2") (d #t) (k 0)) (d (n "slog-async") (r "^2") (d #t) (k 2)) (d (n "slog-stdlog") (r "^2") (d #t) (k 0)) (d (n "slog-term") (r "^2") (d #t) (k 2)))) (h "1j2pn6ipzn4w32z6jb5ksz80n01kqksfm5363sf9hf421jr2xrmx")))

(define-public crate-slog-example-lib-0.3.0 (c (n "slog-example-lib") (v "0.3.0") (d (list (d (n "env_logger") (r "^0.3.5") (d #t) (k 2)) (d (n "slog") (r "^2") (d #t) (k 0)) (d (n "slog-async") (r "^2") (d #t) (k 2)) (d (n "slog-stdlog") (r "^3") (d #t) (k 0)) (d (n "slog-term") (r "^2") (d #t) (k 2)))) (h "1w53qhl4bkna3sjhmlgf7dl1czscjs82hqqk74pv8rqjgb9g85s0")))

