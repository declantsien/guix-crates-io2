(define-module (crates-io sl og slog-mock-proc-macros) #:use-module (crates-io))

(define-public crate-slog-mock-proc-macros-0.3.0 (c (n "slog-mock-proc-macros") (v "0.3.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "095k77lml07vyimd3f9v93dsfjla19xn02imm9rxf7ybc70kmwrs")))

(define-public crate-slog-mock-proc-macros-0.4.0 (c (n "slog-mock-proc-macros") (v "0.4.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0f6jrfwrvvscl0l70q80yy2facxmq7idq434kqdkhj7p3q2yhg3i")))

