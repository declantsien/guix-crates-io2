(define-module (crates-io sl og slog-json) #:use-module (crates-io))

(define-public crate-slog-json-0.5.0 (c (n "slog-json") (v "0.5.0") (d (list (d (n "serde_json") (r "^0.7.1") (d #t) (k 0)) (d (n "slog") (r "^0.5.0") (d #t) (k 0)) (d (n "slog-serde") (r "^0.5.0") (d #t) (k 0)))) (h "0h0hmnzm6cfn6shk0s5kldz2yv5q393j97jv0gj6839g0a9mb1yz")))

(define-public crate-slog-json-0.6.0 (c (n "slog-json") (v "0.6.0") (d (list (d (n "serde_json") (r "^0.7.1") (d #t) (k 0)) (d (n "slog") (r "^0.6.0") (d #t) (k 0)) (d (n "slog-serde") (r "^0.6.0") (d #t) (k 0)))) (h "11xg9fcxfa93x14j0jzpqlvj58amqs2mclbblacx5by707n1vp53")))

(define-public crate-slog-json-0.7.0 (c (n "slog-json") (v "0.7.0") (d (list (d (n "serde_json") (r "^0.7.1") (d #t) (k 0)) (d (n "slog") (r "^0.7.0") (d #t) (k 0)) (d (n "slog-serde") (r "^0.7.0") (d #t) (k 0)))) (h "09cxrdzj87gzz7d12kyb9g0g8ihh22mn3jjir5dfkp4y17wly3nb")))

(define-public crate-slog-json-1.0.0-alpha (c (n "slog-json") (v "1.0.0-alpha") (d (list (d (n "serde_json") (r "^0.7.1") (d #t) (k 0)) (d (n "slog") (r "^1.0.0-alpha") (d #t) (k 0)) (d (n "slog-serde") (r "^1.0.0-alpha") (d #t) (k 0)))) (h "1p3fb2dr9yqbhyg2x0r5b0znm2q17g8jz4lbgjyd14ddgfi3b6yq")))

(define-public crate-slog-json-1.0.0-alpha2 (c (n "slog-json") (v "1.0.0-alpha2") (d (list (d (n "serde_json") (r "^0.7.1") (d #t) (k 0)) (d (n "slog") (r "^1.0.0-alpha2") (d #t) (k 0)) (d (n "slog-serde") (r "^1.0.0-alpha2") (d #t) (k 0)))) (h "0r3wjj6xl109x50sczmblx3hi74xnssyh0cwan7jxablp30ff8hs")))

(define-public crate-slog-json-1.0.0-alpha3 (c (n "slog-json") (v "1.0.0-alpha3") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "serde_json") (r "^0.7.1") (d #t) (k 0)) (d (n "slog") (r "^1.0.0-alpha3") (d #t) (k 0)) (d (n "slog-serde") (r "^1.0.0-alpha3") (d #t) (k 0)))) (h "02yblyshhlq2pgcvrzf5qm81gkh5i9myvl9azgjhld64m64jn1c9")))

(define-public crate-slog-json-1.0.0-alpha6 (c (n "slog-json") (v "1.0.0-alpha6") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "serde_json") (r "^0.7.1") (d #t) (k 0)) (d (n "slog") (r "^1.0.0-alpha6") (d #t) (k 0)) (d (n "slog-serde") (r "^1.0.0-alpha6") (d #t) (k 0)) (d (n "slog-stream") (r "^1.0.0-alpha6") (d #t) (k 0)))) (h "0whvhr64wni6dr19hsd9ws5hinfhqscgyigcgraykg2kpbwwfng3")))

(define-public crate-slog-json-1.0.0-alpha7 (c (n "slog-json") (v "1.0.0-alpha7") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "serde_json") (r "^0.7.1") (d #t) (k 0)) (d (n "slog") (r "^1.0.0-alpha7") (d #t) (k 0)) (d (n "slog-serde") (r "^1.0.0-alpha7") (d #t) (k 0)) (d (n "slog-stream") (r "^1.0.0-alpha7") (d #t) (k 0)))) (h "0fv3jhj2ccc3mblddrq3747yxb8n7w63i5903cdyfppcwvkadl32")))

(define-public crate-slog-json-1.0.0-alpha8 (c (n "slog-json") (v "1.0.0-alpha8") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "serde_json") (r "^0.8.2") (d #t) (k 0)) (d (n "slog") (r "^1") (d #t) (k 0)) (d (n "slog-serde") (r "^1.0.0-alpha8") (d #t) (k 0)) (d (n "slog-stream") (r "^1.0.0-alpha8") (d #t) (k 0)))) (h "0z874mimbc0z257pdvd9w1mpksa01q2h184jhv4azajy9iiv29sd")))

(define-public crate-slog-json-1.0.0 (c (n "slog-json") (v "1.0.0") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "serde_json") (r "^0.8.2") (d #t) (k 0)) (d (n "slog") (r "^1") (d #t) (k 0)) (d (n "slog-serde") (r "^1.0.0-alpha8") (d #t) (k 0)) (d (n "slog-stream") (r "^1") (d #t) (k 0)))) (h "1gw34303108nnj8wpb9pj1kgslg6y7vqm67p3977cladb3z83pkb")))

(define-public crate-slog-json-1.1.0 (c (n "slog-json") (v "1.1.0") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "serde_json") (r "^0.8.2") (d #t) (k 0)) (d (n "slog") (r "^1.1") (d #t) (k 0)) (d (n "slog-serde") (r "^1.0.0-alpha8") (d #t) (k 0)) (d (n "slog-stream") (r "^1") (d #t) (k 0)))) (h "03kc9v8y0vfnm71a15jrmmax54hqbykxh6ybzy6185njf7nwfb6l")))

(define-public crate-slog-json-1.2.0 (c (n "slog-json") (v "1.2.0") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "serde_json") (r "^0.8.2") (d #t) (k 0)) (d (n "slog") (r "^1.2") (d #t) (k 0)) (d (n "slog-serde") (r "^1.0.0-alpha8") (d #t) (k 0)) (d (n "slog-stream") (r "^1.2") (d #t) (k 0)))) (h "0am5ynd4k0h1jg5lxc3rw8yn7wqyv0h31p2r2wqq1gi1x1g1hlhk")))

(define-public crate-slog-json-1.2.1 (c (n "slog-json") (v "1.2.1") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "serde_json") (r "^0.8.2") (d #t) (k 0)) (d (n "slog") (r "^1.2") (d #t) (k 0)) (d (n "slog-serde") (r "^1.0.0-alpha9") (d #t) (k 0)) (d (n "slog-stream") (r "^1.2") (d #t) (k 0)))) (h "18pzl9yk5mibw460h80an52jd9sw2z935yb5sai9064yy2yz9xpg")))

(define-public crate-slog-json-2.0.0-alpha1 (c (n "slog-json") (v "2.0.0-alpha1") (d (list (d (n "chrono") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (d #t) (k 0)) (d (n "slog") (r "^2.0.0-alpha1") (d #t) (k 0)))) (h "1kml1ccv3njwzagfwfr2z1i7dk1q5pjzin9s7q058zfg3vny2a6a") (y #t)))

(define-public crate-slog-json-2.0.0-alpha2 (c (n "slog-json") (v "2.0.0-alpha2") (d (list (d (n "chrono") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (d #t) (k 0)) (d (n "slog") (r "^2.0.0-alpha2") (d #t) (k 0)))) (h "0zf6jn816aq1g5vk9vz2z5m3qnwj09h3dvhkfxv268wxi9z37300") (y #t)))

(define-public crate-slog-json-2.0.0-1.0 (c (n "slog-json") (v "2.0.0-1.0") (d (list (d (n "chrono") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (d #t) (k 0)) (d (n "slog") (r ">= 2.0.0-1.0, < 2.0.0-2") (d #t) (k 0)))) (h "0sa2x12xhpq39cw3jx0710hssaq9bha3a3vcp8122wndbxgj5xf5")))

(define-public crate-slog-json-2.0.0-2.0 (c (n "slog-json") (v "2.0.0-2.0") (d (list (d (n "chrono") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (d #t) (k 0)) (d (n "slog") (r "~2.0.0-2") (d #t) (k 0)))) (h "0y44bxdp1x4zlh3max58mq2yd1md0gj8m0kzmwvgrphfnff7ag8i")))

(define-public crate-slog-json-2.0.0-3.1 (c (n "slog-json") (v "2.0.0-3.1") (d (list (d (n "chrono") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (d #t) (k 0)) (d (n "slog") (r "~2.0.0-3") (d #t) (k 0)))) (h "022gf0wldf6zbpm3x953xy6lwh7cy8aml262afvjbwqhn5d4nikc")))

(define-public crate-slog-json-2.0.0-4.0 (c (n "slog-json") (v "2.0.0-4.0") (d (list (d (n "chrono") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (d #t) (k 0)) (d (n "slog") (r "^2") (d #t) (k 0)))) (h "05i7g67p4gxqzwisnvgyl26mm564iv00ibsrqvq9vfzmvcjnbvbn")))

(define-public crate-slog-json-2.0.0 (c (n "slog-json") (v "2.0.0") (d (list (d (n "chrono") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "slog") (r "^2") (d #t) (k 0)))) (h "1q0mphda0ssf4fsjxlm6qr0r07mvxwmv2aqw71lxyqlg0rkzg7w9")))

(define-public crate-slog-json-2.0.1 (c (n "slog-json") (v "2.0.1") (d (list (d (n "chrono") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "slog") (r "^2") (d #t) (k 0)))) (h "1qb5bf446diqhw006sppijs0i0cmppvwxc91m11737mrjxqjv9my")))

(define-public crate-slog-json-2.0.2 (c (n "slog-json") (v "2.0.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "slog") (r "^2") (d #t) (k 0)))) (h "1z53ipxgqj5628as4drn2basjw0drpv6w704g4h216178cskjd8f")))

(define-public crate-slog-json-2.2.0 (c (n "slog-json") (v "2.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "erased-serde") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "slog") (r "^2.1.1") (d #t) (k 0)) (d (n "slog-async") (r "^2") (d #t) (k 2)))) (h "1r94wnxm60nz0gwrjka425dp3gsh8dw7jg4k7s5kfffzya6lplfx") (f (quote (("nested-values" "erased-serde" "slog/nested-values") ("dynamic-keys" "slog/dynamic-keys") ("default"))))))

(define-public crate-slog-json-2.3.0 (c (n "slog-json") (v "2.3.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "erased-serde") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "slog") (r "^2.1.1") (d #t) (k 0)) (d (n "slog-async") (r "^2") (d #t) (k 2)))) (h "0yn3c7845gsqgy4sb0gmncq0plnwwsv0x6hdcvpjbwzqy6px5h6x") (f (quote (("nested-values" "erased-serde" "slog/nested-values") ("dynamic-keys" "slog/dynamic-keys") ("default"))))))

(define-public crate-slog-json-2.4.0 (c (n "slog-json") (v "2.4.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "erased-serde") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "slog") (r "^2.1.1") (d #t) (k 0)) (d (n "slog-async") (r "^2") (d #t) (k 2)))) (h "0hyhhp5gh6g6jwk2ry783ik3gdbfcnkard132hvhxs5mnrpvksaj") (f (quote (("nested-values" "erased-serde" "slog/nested-values") ("dynamic-keys" "slog/dynamic-keys") ("default"))))))

(define-public crate-slog-json-2.5.0 (c (n "slog-json") (v "2.5.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("clock"))) (k 0)) (d (n "erased-serde") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "slog") (r "^2.1.1") (d #t) (k 0)) (d (n "slog-async") (r "^2") (d #t) (k 2)))) (h "1s99jf3ffwpwag56hdh6pahql7yijlwabw3vl6fwl3z85japlzqg") (f (quote (("nested-values" "erased-serde" "slog/nested-values") ("dynamic-keys" "slog/dynamic-keys") ("default"))))))

(define-public crate-slog-json-2.6.0 (c (n "slog-json") (v "2.6.0") (d (list (d (n "erased-serde") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "slog") (r "^2.1.1") (d #t) (k 0)) (d (n "slog-async") (r "^2") (d #t) (k 2)) (d (n "time") (r "^0.3.6") (f (quote ("formatting" "local-offset"))) (d #t) (k 0)))) (h "1d7w7pxr4mmhp565i0chrbysfia9m79za78i32ihmx26fg72by3h") (f (quote (("nested-values" "erased-serde" "slog/nested-values") ("dynamic-keys" "slog/dynamic-keys") ("default")))) (r "1.53")))

(define-public crate-slog-json-2.6.1 (c (n "slog-json") (v "2.6.1") (d (list (d (n "erased-serde") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "slog") (r "^2.1.1") (d #t) (k 0)) (d (n "slog-async") (r "^2") (d #t) (k 2)) (d (n "time") (r "^0.3.6") (f (quote ("formatting"))) (d #t) (k 0)))) (h "06gjy1n7ivrcy7rr8dyndmfza24hwbgakw7faawciqzi3bv567iy") (f (quote (("nested-values" "erased-serde" "slog/nested-values") ("dynamic-keys" "slog/dynamic-keys") ("default")))) (r "1.53")))

