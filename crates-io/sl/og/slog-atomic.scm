(define-module (crates-io sl og slog-atomic) #:use-module (crates-io))

(define-public crate-slog-atomic-0.2.0 (c (n "slog-atomic") (v "0.2.0") (d (list (d (n "crossbeam") (r "^0.2.9") (d #t) (k 0)) (d (n "slog") (r "^1.0.0-alpha3") (d #t) (k 0)))) (h "0jq97b0h1m2d13060xy2cnmd7frvbrvn3cpnmkzff24zs8g4f2ly")))

(define-public crate-slog-atomic-0.3.0 (c (n "slog-atomic") (v "0.3.0") (d (list (d (n "crossbeam") (r "^0.2.9") (d #t) (k 0)) (d (n "slog") (r "^1.0.0-alpha6") (d #t) (k 0)))) (h "0n21pydykmbbjp6f4vsz230qvfm3hwj355ln9rcpcbmy46kgv3lw")))

(define-public crate-slog-atomic-0.4.0 (c (n "slog-atomic") (v "0.4.0") (d (list (d (n "crossbeam") (r "^0.2.9") (d #t) (k 0)) (d (n "slog") (r "^1") (d #t) (k 0)))) (h "0wq543mdmrsnnk0h7pvpmj698ydy06ww5vglqr8q24am4hc02fxs")))

(define-public crate-slog-atomic-0.4.1 (c (n "slog-atomic") (v "0.4.1") (d (list (d (n "crossbeam") (r "^0.2.9") (d #t) (k 0)) (d (n "slog") (r "^1") (d #t) (k 0)))) (h "0gzl2dr4m0jvy1hksx9zzl0p0aq7ab50n3vxzyq6k6g6p3rb2wgq")))

(define-public crate-slog-atomic-0.4.3 (c (n "slog-atomic") (v "0.4.3") (d (list (d (n "crossbeam") (r "^0.2.9") (d #t) (k 0)) (d (n "lazy_static") (r "^0.1.16") (d #t) (k 2)) (d (n "nix") (r "^0.6.0") (d #t) (k 2)) (d (n "slog") (r "^1") (d #t) (k 0)) (d (n "slog-json") (r "^1") (d #t) (k 2)) (d (n "slog-stream") (r "^1") (d #t) (k 2)) (d (n "slog-term") (r "^1") (d #t) (k 2)))) (h "0r48fyk25dv0z7mnap48mdb1b03pafx2cq9vapz1hs4dj3ja9xfn")))

(define-public crate-slog-atomic-2.0.0-3.0 (c (n "slog-atomic") (v "2.0.0-3.0") (d (list (d (n "crossbeam") (r "^0.2.9") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.4") (d #t) (k 2)) (d (n "nix") (r "^0.8.0") (d #t) (k 2)) (d (n "slog") (r "~2.0.0-3") (d #t) (k 0)) (d (n "slog-json") (r "~2.0.0-3") (d #t) (k 2)) (d (n "slog-term") (r "~2.0.0-3") (d #t) (k 2)))) (h "1q9ynxmriim9nz7q9qr5gscnnkvglj5h65z0f7ddsw79sm66w944")))

(define-public crate-slog-atomic-2.0.0-3.1 (c (n "slog-atomic") (v "2.0.0-3.1") (d (list (d (n "crossbeam") (r "^0.2.9") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.4") (d #t) (k 2)) (d (n "nix") (r "^0.8.0") (d #t) (k 2)) (d (n "slog") (r "~2.0.0-3") (d #t) (k 0)) (d (n "slog-async") (r "~2.0.0-3") (d #t) (k 2)) (d (n "slog-json") (r "~2.0.0-3") (d #t) (k 2)) (d (n "slog-term") (r "~2.0.0-3") (d #t) (k 2)))) (h "0lb5ph9z1ndml3041562z6q1m72nr36yqx93i1ywbqqcqkayvy7a")))

(define-public crate-slog-atomic-2.0.0 (c (n "slog-atomic") (v "2.0.0") (d (list (d (n "crossbeam") (r "^0.2.9") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.4") (d #t) (k 2)) (d (n "nix") (r "^0.8.0") (d #t) (k 2)) (d (n "slog") (r "^2") (d #t) (k 0)) (d (n "slog-async") (r "^2") (d #t) (k 2)) (d (n "slog-json") (r "^2") (d #t) (k 2)) (d (n "slog-term") (r "^2") (d #t) (k 2)))) (h "1s82zwpwy79fwz9pq212ci8kh1mhczjkcyaks59xaa76aqvx2xvy")))

(define-public crate-slog-atomic-3.0.0 (c (n "slog-atomic") (v "3.0.0") (d (list (d (n "arc-swap") (r "~0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.4") (d #t) (k 2)) (d (n "nix") (r "^0.8.0") (d #t) (k 2)) (d (n "slog") (r "^2") (d #t) (k 0)) (d (n "slog-async") (r "^2") (d #t) (k 2)) (d (n "slog-json") (r "^2") (d #t) (k 2)) (d (n "slog-term") (r "^2") (d #t) (k 2)))) (h "0fjj4aiw1c1msbxw21i4rv3pykhpzzlkksgcmcx5aik355ckg047")))

(define-public crate-slog-atomic-3.1.0 (c (n "slog-atomic") (v "3.1.0") (d (list (d (n "arc-swap") (r "^1.1") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.4") (d #t) (k 2)) (d (n "nix") (r "^0.8.0") (d #t) (k 2)) (d (n "slog") (r "^2") (d #t) (k 0)) (d (n "slog-async") (r "^2") (d #t) (k 2)) (d (n "slog-json") (r "^2") (d #t) (k 2)) (d (n "slog-term") (r "^2") (d #t) (k 2)))) (h "0rmhm6czipnnyg0f86qlv3z2yqx3s6x510zb6f3lbqd9vpr1gdgn")))

