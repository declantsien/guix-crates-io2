(define-module (crates-io sl og slog_mongodb) #:use-module (crates-io))

(define-public crate-slog_mongodb-1.0.0 (c (n "slog_mongodb") (v "1.0.0") (d (list (d (n "bson") (r "^0.14") (f (quote ("u2i"))) (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "mongodb") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "slog") (r "^2.5") (d #t) (k 0)) (d (n "slog-async") (r "^2.5") (d #t) (k 2)))) (h "1j087zinad46r291qg9l0rxlxc78b57y78fc28qcsjj2kv796gsc")))

