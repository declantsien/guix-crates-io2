(define-module (crates-io sl og slog-async) #:use-module (crates-io))

(define-public crate-slog-async-0.2.0-alpha1 (c (n "slog-async") (v "0.2.0-alpha1") (d (list (d (n "slog") (r "^2.0.0-alpha1") (d #t) (k 0)) (d (n "take_mut") (r "^0.1.3") (d #t) (k 0)) (d (n "thread_local") (r "^0.3.2") (d #t) (k 0)))) (h "04v9gbx0swif4fhk3y4gk127zzfpgdjm7r8d4js7j9qmrkn6mgqa") (y #t)))

(define-public crate-slog-async-0.2.0-alpha2 (c (n "slog-async") (v "0.2.0-alpha2") (d (list (d (n "slog") (r "^2.0.0-alpha2") (d #t) (k 0)) (d (n "take_mut") (r "^0.1.3") (d #t) (k 0)) (d (n "thread_local") (r "^0.3.3") (d #t) (k 0)))) (h "1gdvb9mzlim0sg6xf9y6djbvic0gycmapsjdblanxahlraf6ghy2") (y #t)))

(define-public crate-slog-async-2.0.0-1.0 (c (n "slog-async") (v "2.0.0-1.0") (d (list (d (n "slog") (r ">= 2.0.0-1, < 2.0.0-2") (d #t) (k 0)) (d (n "take_mut") (r "^0.1.3") (d #t) (k 0)) (d (n "thread_local") (r "^0.3.3") (d #t) (k 0)))) (h "1iln3xzbz4xvcp2whbzm8hylk276ykshx3wpmraln6bjfc9s4lj4")))

(define-public crate-slog-async-2.0.0-2.0 (c (n "slog-async") (v "2.0.0-2.0") (d (list (d (n "slog") (r "~2.0.0-2") (d #t) (k 0)) (d (n "take_mut") (r "^0.1.3") (d #t) (k 0)) (d (n "thread_local") (r "^0.3.3") (d #t) (k 0)))) (h "07q17cyrmg743ka8fxy5hw8ygznwkcx97h5chbyvwc0qf9saqz2d")))

(define-public crate-slog-async-2.0.0-3.0 (c (n "slog-async") (v "2.0.0-3.0") (d (list (d (n "slog") (r "~2.0.0-3") (d #t) (k 0)) (d (n "take_mut") (r "^0.1.3") (d #t) (k 0)) (d (n "thread_local") (r "^0.3.3") (d #t) (k 0)))) (h "09n534l41ilpjmfw0lnbp9r4wv71ilnd4qw8smz87kqp6yg3kr5y")))

(define-public crate-slog-async-2.0.0 (c (n "slog-async") (v "2.0.0") (d (list (d (n "slog") (r "^2") (d #t) (k 0)) (d (n "take_mut") (r "^0.1.3") (d #t) (k 0)) (d (n "thread_local") (r "^0.3.3") (d #t) (k 0)))) (h "008cgmssyxrqfv80hkld0kicvw326rclplp1w3kbx2awiw2hqyi7")))

(define-public crate-slog-async-2.0.1 (c (n "slog-async") (v "2.0.1") (d (list (d (n "slog") (r "^2") (d #t) (k 0)) (d (n "take_mut") (r "^0.1.3") (d #t) (k 0)) (d (n "thread_local") (r "^0.3.3") (d #t) (k 0)))) (h "0dh9wrg6lr5zvvaksvn2s5s2iz2lwpdpjs6a7pl3dk4cj6rvmgmg")))

(define-public crate-slog-async-2.1.0 (c (n "slog-async") (v "2.1.0") (d (list (d (n "slog") (r "^2") (d #t) (k 0)) (d (n "take_mut") (r "^0.1.3") (d #t) (k 0)) (d (n "thread_local") (r "^0.3.3") (d #t) (k 0)))) (h "1hlfkfrc4hw3pa6rgy58dla42w56i8rnhf1hjmpj0ajmmgpc5l4a")))

(define-public crate-slog-async-2.2.0 (c (n "slog-async") (v "2.2.0") (d (list (d (n "slog") (r "^2.1.1") (d #t) (k 0)) (d (n "take_mut") (r "^0.2.0") (d #t) (k 0)) (d (n "thread_local") (r "^0.3.3") (d #t) (k 0)))) (h "045akqgb1hvghh91mjvzp8ilcyxd3lmjzjppslc4c04bq0q9lcay") (f (quote (("nested-values" "slog/nested-values") ("dynamic-keys" "slog/dynamic-keys") ("default"))))))

(define-public crate-slog-async-2.3.0 (c (n "slog-async") (v "2.3.0") (d (list (d (n "slog") (r "^2.1") (d #t) (k 0)) (d (n "take_mut") (r "^0.2.0") (d #t) (k 0)) (d (n "thread_local") (r "^0.3.3") (d #t) (k 0)))) (h "0bzwkcvwn4d3x6dc8vqywxmcmkxa65gfabv6cv488393ddnd2i75") (f (quote (("nested-values" "slog/nested-values") ("dynamic-keys" "slog/dynamic-keys") ("default"))))))

(define-public crate-slog-async-2.4.0 (c (n "slog-async") (v "2.4.0") (d (list (d (n "crossbeam-channel") (r "^0.3") (d #t) (k 0)) (d (n "slog") (r "^2.1") (d #t) (k 0)) (d (n "take_mut") (r "^0.2.0") (d #t) (k 0)) (d (n "thread_local") (r "^1") (d #t) (k 0)))) (h "1vhk15qaz3j8qb5w41jrzr4wnycd6wylmgjwb768ra0d31dr5jkq") (f (quote (("nested-values" "slog/nested-values") ("dynamic-keys" "slog/dynamic-keys") ("default"))))))

(define-public crate-slog-async-2.5.0 (c (n "slog-async") (v "2.5.0") (d (list (d (n "crossbeam-channel") (r "^0.4") (d #t) (k 0)) (d (n "slog") (r "^2.1") (d #t) (k 0)) (d (n "take_mut") (r "^0.2.0") (d #t) (k 0)) (d (n "thread_local") (r "^1") (d #t) (k 0)))) (h "1fzvx0n2q02bjrcjjylsgdr74d2yp1zc17s9fdkgkqkwwin37csi") (f (quote (("nested-values" "slog/nested-values") ("dynamic-keys" "slog/dynamic-keys") ("default"))))))

(define-public crate-slog-async-2.6.0 (c (n "slog-async") (v "2.6.0") (d (list (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "slog") (r "^2.1") (d #t) (k 0)) (d (n "take_mut") (r "^0.2.0") (d #t) (k 0)) (d (n "thread_local") (r "^1") (d #t) (k 0)))) (h "1p7v0jl82snmk1c7f6ch528ladzyprlk5gzaqkdqa342ky3i6266") (f (quote (("nested-values" "slog/nested-values") ("dynamic-keys" "slog/dynamic-keys") ("default"))))))

(define-public crate-slog-async-2.7.0 (c (n "slog-async") (v "2.7.0") (d (list (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "slog") (r "^2.1") (d #t) (k 0)) (d (n "take_mut") (r "^0.2.0") (d #t) (k 0)) (d (n "thread_local") (r "^1") (d #t) (k 0)))) (h "1zkz15xl2gkhnbn3xd0rvw26sklcbpshz1qj2ijk8ap6aar5jv3n") (f (quote (("nested-values" "slog/nested-values") ("dynamic-keys" "slog/dynamic-keys") ("default"))))))

(define-public crate-slog-async-2.8.0 (c (n "slog-async") (v "2.8.0") (d (list (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "slog") (r "^2.1") (d #t) (k 0)) (d (n "take_mut") (r "^0.2.0") (d #t) (k 0)) (d (n "thread_local") (r "^1") (d #t) (k 0)))) (h "113b17aw7jx7mr68vwfq2yiv6mb4702hz6a0g587jb4ai67h7j3j") (f (quote (("nested-values" "slog/nested-values") ("dynamic-keys" "slog/dynamic-keys") ("default"))))))

