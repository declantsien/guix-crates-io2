(define-module (crates-io sl og slog-extra) #:use-module (crates-io))

(define-public crate-slog-extra-0.1.0 (c (n "slog-extra") (v "0.1.0") (d (list (d (n "slog") (r "^1.1") (d #t) (k 0)) (d (n "thread_local") (r "^0.2.6") (d #t) (k 0)))) (h "1zcvx00hgwyd9fpl6ygszm93gyf12cmphqqp3wbs2jvjrjzqpbs3")))

(define-public crate-slog-extra-0.1.1 (c (n "slog-extra") (v "0.1.1") (d (list (d (n "slog") (r "^1.2.0") (d #t) (k 0)) (d (n "thread_local") (r "^0.2.6") (d #t) (k 0)))) (h "0idl890i7b699l6skvrnpdvapmgg7qg2s1drsymc6kash57n2wgm")))

(define-public crate-slog-extra-0.1.2 (c (n "slog-extra") (v "0.1.2") (d (list (d (n "slog") (r "^1.2.0") (d #t) (k 0)) (d (n "thread_local") (r "^0.3.2") (d #t) (k 0)))) (h "0aj94xyh05jgfym5hxbmx1swkncjlvl0pdlrr970xj8xvps825ai")))

