(define-module (crates-io sl og slog_kmsg) #:use-module (crates-io))

(define-public crate-slog_kmsg-0.1.0 (c (n "slog_kmsg") (v "0.1.0") (d (list (d (n "slog") (r "^2.0") (d #t) (k 0)) (d (n "slog-async") (r "^2.0") (d #t) (k 0)))) (h "0xlhna62qknzpnw10y32vv2nnyhqaxzh30jnkq804vcrvg3dmr02")))

(define-public crate-slog_kmsg-0.1.1 (c (n "slog_kmsg") (v "0.1.1") (d (list (d (n "slog") (r "^2.0") (d #t) (k 0)) (d (n "slog-async") (r "^2.0") (d #t) (k 0)))) (h "0w866c77462bh19wv3b1dvdf5xdf2x9zdhb539h02lkd2dwy0szc")))

(define-public crate-slog_kmsg-0.1.2 (c (n "slog_kmsg") (v "0.1.2") (d (list (d (n "slog") (r "^2.0") (d #t) (k 0)) (d (n "slog-async") (r "^2.0") (d #t) (k 0)))) (h "02ikh26rhwvw71brr3bwfk68hp2n4yw7bxzdm76sq15yq9n5kx1j")))

