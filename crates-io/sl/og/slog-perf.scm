(define-module (crates-io sl og slog-perf) #:use-module (crates-io))

(define-public crate-slog-perf-0.1.0 (c (n "slog-perf") (v "0.1.0") (d (list (d (n "slog") (r "^2") (d #t) (k 0)))) (h "0vpi4rql7rwqf26rm1pj0wmi7wzdi3b20rs8kp89pqwhdr3njn9d")))

(define-public crate-slog-perf-0.2.0 (c (n "slog-perf") (v "0.2.0") (d (list (d (n "slog") (r "^2") (d #t) (k 0)))) (h "04m8hjcf3jc0p6iniw4zkdi21igv8shaanpjdmsb3aagka8zd8nc")))

