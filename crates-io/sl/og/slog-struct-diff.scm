(define-module (crates-io sl og slog-struct-diff) #:use-module (crates-io))

(define-public crate-slog-struct-diff-0.1.0 (c (n "slog-struct-diff") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "slog") (r "^2.0.12") (d #t) (k 0)) (d (n "struct-diff") (r "^0.2") (d #t) (k 0)))) (h "1h3ji8ryqwzmpy2xi6yy38i376gkvax6drvi45dg3rpfz1wf5w7k")))

