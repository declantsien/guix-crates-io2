(define-module (crates-io sl og slog-stream) #:use-module (crates-io))

(define-public crate-slog-stream-1.0.0-alpha6 (c (n "slog-stream") (v "1.0.0-alpha6") (d (list (d (n "slog") (r "^1.0.0-alpha6") (d #t) (k 0)))) (h "0bixf3l091m6v1gzi69az8cr0ssm84i4li00m4rm5kqnfn93sj80")))

(define-public crate-slog-stream-1.0.0-alpha7 (c (n "slog-stream") (v "1.0.0-alpha7") (d (list (d (n "slog") (r "^1.0.0-alpha7") (d #t) (k 0)))) (h "191q26q85z0aj8v6v2l4qjg6q4n3hf6jr8gdpxynbvwwds1fki9g")))

(define-public crate-slog-stream-1.0.0-alpha8 (c (n "slog-stream") (v "1.0.0-alpha8") (d (list (d (n "slog") (r "^1") (d #t) (k 0)) (d (n "thread_local") (r "^0.2.6") (d #t) (k 0)))) (h "0d3jk412ivrzx621fr7h77n29gs1vlvii4a2ban59wfflkxa752m")))

(define-public crate-slog-stream-1.0.0 (c (n "slog-stream") (v "1.0.0") (d (list (d (n "slog") (r "^1") (d #t) (k 0)) (d (n "thread_local") (r "^0.2.6") (d #t) (k 0)))) (h "0vq98qvabcikngx5j8bq9yn3ga6ciw6a71cjf8akbmjfbmlgmjai")))

(define-public crate-slog-stream-1.1.0 (c (n "slog-stream") (v "1.1.0") (d (list (d (n "slog") (r "^1") (d #t) (k 0)) (d (n "thread_local") (r "^0.2.6") (d #t) (k 0)))) (h "08mryd6z6jq2g2354ijvj4h3bg9y2kgix87nybr3bf8kgviq3q9k")))

(define-public crate-slog-stream-1.2.0 (c (n "slog-stream") (v "1.2.0") (d (list (d (n "slog") (r "^1.2") (d #t) (k 0)) (d (n "slog-extra") (r "^0.1.0") (d #t) (k 0)) (d (n "thread_local") (r "^0.2.6") (d #t) (k 0)))) (h "1i9m2s8s03nr5ak6ji61z492gp5n0fx49ky5h97s99w01ghgxw76")))

(define-public crate-slog-stream-1.2.1 (c (n "slog-stream") (v "1.2.1") (d (list (d (n "slog") (r "^1.2") (d #t) (k 0)) (d (n "slog-extra") (r "^0.1.1") (d #t) (k 0)) (d (n "thread_local") (r "^0.3.2") (d #t) (k 0)))) (h "1alxf4z5g81ly9bcb2jd8p8ih7q58sd0a7kpiwrvgp8723vlmb1z")))

