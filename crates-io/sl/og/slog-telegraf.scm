(define-module (crates-io sl og slog-telegraf) #:use-module (crates-io))

(define-public crate-slog-telegraf-0.1.0 (c (n "slog-telegraf") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "slog") (r "^2.5") (d #t) (k 0)) (d (n "slog-async") (r "^2.5") (d #t) (k 2)) (d (n "url") (r "~2.1.1") (d #t) (k 0)))) (h "1hqjwrjg10a28lkr45i4nz351g456pg08fbl7g7jw60wy705b0ri")))

(define-public crate-slog-telegraf-0.2.0 (c (n "slog-telegraf") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "slog") (r "^2.5") (d #t) (k 0)) (d (n "slog-async") (r "^2.5") (d #t) (k 2)) (d (n "url") (r "~2.1.1") (d #t) (k 0)))) (h "1gxa46kjn0lld55yz88cdkdkjfyvrn2nrpwssl8cpwpcgr22f2pf")))

(define-public crate-slog-telegraf-0.2.1 (c (n "slog-telegraf") (v "0.2.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "slog") (r "^2.5") (d #t) (k 0)) (d (n "slog-async") (r "^2.5") (d #t) (k 2)) (d (n "url") (r "~2.1.1") (d #t) (k 0)))) (h "0m5say52drc92w7mzmn9i7kivx01k7fgd042knik6xb9v3x4xl5z")))

(define-public crate-slog-telegraf-0.2.2 (c (n "slog-telegraf") (v "0.2.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "slog") (r "^2.5") (d #t) (k 0)) (d (n "slog-async") (r "^2.5") (d #t) (k 2)) (d (n "url") (r "^2.1") (d #t) (k 0)))) (h "1a5qqmz3n3vfc4dnrwbvd13867ja265n2rmgl0rshn0s2gfwhk9n")))

