(define-module (crates-io sl og slog-mozlog-json) #:use-module (crates-io))

(define-public crate-slog-mozlog-json-0.1.0 (c (n "slog-mozlog-json") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "slog") (r "^2.2") (f (quote ("nested-values"))) (d #t) (k 0)))) (h "183w2g58hi22spkcjdvmkw1fpk14qnh1r4z8clhgbwcnvg2z207l")))

