(define-module (crates-io sl og slog-config) #:use-module (crates-io))

(define-public crate-slog-config-0.1.0 (c (n "slog-config") (v "0.1.0") (d (list (d (n "isatty") (r "^0.1.3") (d #t) (k 0)) (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_derive") (r "^0.8") (d #t) (k 0)) (d (n "slog") (r "^1.4") (d #t) (k 0)) (d (n "slog-json") (r "^1.2.1") (d #t) (k 0)) (d (n "slog-stream") (r "^1.2.0") (d #t) (k 0)) (d (n "slog-term") (r "^1.3.5") (d #t) (k 0)) (d (n "toml") (r "^0.2.1") (f (quote ("serde"))) (k 0)))) (h "07kxdrlwvvk3ncwr0cknk9lkvsx0dg76rpsr03wykkrkbldjb7cn")))

(define-public crate-slog-config-0.3.0 (c (n "slog-config") (v "0.3.0") (d (list (d (n "isatty") (r "^0.1.3") (d #t) (k 0)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 0)) (d (n "slog") (r "~2.0.0-2") (d #t) (k 0)) (d (n "slog-json") (r "~2.0.0-2") (d #t) (k 0)) (d (n "slog-term") (r "~2.0.0-2") (d #t) (k 0)) (d (n "toml") (r "^0.3") (f (quote ("serde"))) (k 0)))) (h "1vzn2zp799m5p7i0mgv2yvbh8fglwbdkb1n7knhrxrqxff1am6lz")))

(define-public crate-slog-config-0.4.0 (c (n "slog-config") (v "0.4.0") (d (list (d (n "isatty") (r "^0.1.3") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "slog") (r "^2") (d #t) (k 0)) (d (n "slog-json") (r "^2") (d #t) (k 0)) (d (n "slog-term") (r "^2") (d #t) (k 0)) (d (n "toml") (r "^0.4") (f (quote ("serde"))) (k 0)))) (h "0zf2pz3si9i3jwaxzzi1gknpb9zi4nbgi5yqj7gkr4d13my9lik2")))

