(define-module (crates-io sl og slog_prometheus) #:use-module (crates-io))

(define-public crate-slog_prometheus-0.1.0 (c (n "slog_prometheus") (v "0.1.0") (d (list (d (n "prometheus") (r "^0.13.0") (d #t) (k 0)) (d (n "prometheus_exporter") (r "^0.8.4") (d #t) (k 2)) (d (n "reqwest") (r "^0.11.10") (f (quote ("blocking"))) (d #t) (k 2)) (d (n "slog") (r "^2.7.0") (d #t) (k 0)) (d (n "slog-async") (r "^2.7.0") (d #t) (k 2)) (d (n "slog-term") (r "^2.9.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "1b4h3cc21bm03lqim17fckpscwjykf34wykpczxv8an1n8001v41")))

