(define-module (crates-io sl og slog-stdlog) #:use-module (crates-io))

(define-public crate-slog-stdlog-0.6.0 (c (n "slog-stdlog") (v "0.6.0") (d (list (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "slog") (r "^0.6.0") (d #t) (k 0)))) (h "0hh3whv8f4w0kcrbk4jdzqgc9ri8hx2gnssn86b1q2492crm2qvp")))

(define-public crate-slog-stdlog-0.7.0 (c (n "slog-stdlog") (v "0.7.0") (d (list (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "slog") (r "^0.7.0") (d #t) (k 0)))) (h "10in6y4mvc21i09wag7d70d2x9nbwl4kf7fzk0phn12kqjlfybjs")))

(define-public crate-slog-stdlog-1.0.0-alpha (c (n "slog-stdlog") (v "1.0.0-alpha") (d (list (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "slog") (r "^1.0.0-alpha") (d #t) (k 0)) (d (n "slog-term") (r "^1.0.0-alpha") (d #t) (k 0)))) (h "1dp5wifj9yyymfwzbrb03kynrd7x018mwl6yqpwq8q8d04kr5qrs")))

(define-public crate-slog-stdlog-1.0.0-alpha2 (c (n "slog-stdlog") (v "1.0.0-alpha2") (d (list (d (n "crossbeam") (r "^0.2.9") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "slog") (r "^1.0.0-alpha") (d #t) (k 0)) (d (n "slog-term") (r "^1.0.0-alpha") (d #t) (k 0)))) (h "0sa36r5rfxb49kg9kbzz70wbgdqzi0iy343r0hjla96fxklpsyv8")))

(define-public crate-slog-stdlog-1.0.0-alpha3 (c (n "slog-stdlog") (v "1.0.0-alpha3") (d (list (d (n "crossbeam") (r "^0.2.9") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "slog") (r "^1.0.0-alpha") (d #t) (k 0)) (d (n "slog-term") (r "^1.0.0-alpha") (d #t) (k 0)))) (h "16zm0hahqasg00adbx7c4bprb50gj4lgxc6mbp5ipdj6ma8mfkfp")))

(define-public crate-slog-stdlog-1.0.0-alpha4 (c (n "slog-stdlog") (v "1.0.0-alpha4") (d (list (d (n "crossbeam") (r "^0.2.9") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "slog") (r "^1.0.0-alpha2") (d #t) (k 0)) (d (n "slog-term") (r "^1.0.0-alpha2") (d #t) (k 0)))) (h "1p66n4xmhgljl1jxakz292z27qdxswbv6j55q89w4i5x93f2x3mn")))

(define-public crate-slog-stdlog-1.0.0-alpha5 (c (n "slog-stdlog") (v "1.0.0-alpha5") (d (list (d (n "crossbeam") (r "^0.2.9") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "slog") (r "^1.0.0-alpha3") (d #t) (k 0)) (d (n "slog-term") (r "^1.0.0-alpha3") (d #t) (k 0)))) (h "0ghp6zn6kk7yxqif9ckm1bxcm8f8rmgvr9k1rhnfc8byx10akxp9")))

(define-public crate-slog-stdlog-1.0.0-alpha6 (c (n "slog-stdlog") (v "1.0.0-alpha6") (d (list (d (n "crossbeam") (r "^0.2.9") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "slog") (r "^1.0.0-alpha6") (d #t) (k 0)) (d (n "slog-term") (r "^1.0.0-alpha6") (d #t) (k 0)))) (h "1l4zspnxlzxp8vg0ngdlfvhk82ss3x0df35rai23ls4337ahld2k")))

(define-public crate-slog-stdlog-1.0.0-alpha7 (c (n "slog-stdlog") (v "1.0.0-alpha7") (d (list (d (n "crossbeam") (r "^0.2.9") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "slog") (r "^1.0.0-alpha7") (d #t) (k 0)) (d (n "slog-term") (r "^1.0.0-alpha7") (d #t) (k 0)))) (h "1wm1yp1lfhb4w09rx4925mfq6687lwbckkyyb5yzr3z00zzxx6yv")))

(define-public crate-slog-stdlog-1.0.0-alpha8 (c (n "slog-stdlog") (v "1.0.0-alpha8") (d (list (d (n "crossbeam") (r "^0.2.9") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "slog") (r "^1") (d #t) (k 0)) (d (n "slog-term") (r "^1.0.0-alpha8") (d #t) (k 0)))) (h "0rb99lbdjwpa64y0viyhn5i8xawlyy8isyr4ndhg978gmiw703m7")))

(define-public crate-slog-stdlog-1.0.0 (c (n "slog-stdlog") (v "1.0.0") (d (list (d (n "crossbeam") (r "^0.2.9") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "slog") (r "^1") (d #t) (k 0)) (d (n "slog-term") (r "^1") (d #t) (k 0)))) (h "0hq0wkx0l1f48gfr2j01wdnji20wjsaaz1sy30xg2z5fzm6704nr")))

(define-public crate-slog-stdlog-1.0.1 (c (n "slog-stdlog") (v "1.0.1") (d (list (d (n "crossbeam") (r "^0.2.9") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "slog") (r "^1") (d #t) (k 0)) (d (n "slog-term") (r "^1") (d #t) (k 0)))) (h "0zrmwngs8pqmmijxhj3wb5nrp3gkfbszvg8p2i7xa7j92c79al4w")))

(define-public crate-slog-stdlog-1.1.0 (c (n "slog-stdlog") (v "1.1.0") (d (list (d (n "crossbeam") (r "^0.2.9") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "slog") (r "^1.1") (d #t) (k 0)) (d (n "slog-term") (r "^1") (d #t) (k 0)))) (h "0z53h30gvsshhh3pvz1771nigdy54ada3q7xvi0spq251ks0ik2n")))

(define-public crate-slog-stdlog-2.0.0-0.1 (c (n "slog-stdlog") (v "2.0.0-0.1") (d (list (d (n "crossbeam") (r "^0.2.9") (d #t) (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "slog") (r ">= 2.0.0-1.0, < 2.0.0-2") (d #t) (k 0)) (d (n "slog-scope") (r "^0.3.0") (d #t) (k 0)) (d (n "slog-term") (r "^1.3") (d #t) (k 0)))) (h "0jgb24ivlmw2akfzvxiad055dx9lcmj6hw6gwb2vvrskwsba834a")))

(define-public crate-slog-stdlog-2.0.0-0.2 (c (n "slog-stdlog") (v "2.0.0-0.2") (d (list (d (n "crossbeam") (r "^0.2.9") (d #t) (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "slog") (r ">= 2.0.0-1.0, < 2.0.0-2") (d #t) (k 0)) (d (n "slog-scope") (r ">= 2.0.0-1.0, < 2.0.0-2") (d #t) (k 0)) (d (n "slog-term") (r ">= 2.0.0-1.0, < 2.0.0-2") (d #t) (k 0)))) (h "0hbqcfnm7fm5cjxpacah47sjva82b1gh3mdjpygsk18vlhd4p4dn")))

(define-public crate-slog-stdlog-2.0.0-1.0 (c (n "slog-stdlog") (v "2.0.0-1.0") (d (list (d (n "crossbeam") (r "^0.2.9") (d #t) (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "slog") (r "~2.0.0-2") (d #t) (k 0)) (d (n "slog-scope") (r "~2.0.0-2") (d #t) (k 0)) (d (n "slog-term") (r "~2.0.0-2") (d #t) (k 0)))) (h "1iflh729sf94cbp0bgaji8c8ah3symp884kdz0azvdkxw2b57dcd")))

(define-public crate-slog-stdlog-2.0.0-3.0 (c (n "slog-stdlog") (v "2.0.0-3.0") (d (list (d (n "crossbeam") (r "^0.2.9") (d #t) (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "slog") (r "~2.0.0-3") (d #t) (k 0)) (d (n "slog-scope") (r "~2.0.0-3") (d #t) (k 0)) (d (n "slog-term") (r "~2.0.0-3") (d #t) (k 0)))) (h "1xn1yx02v0jim7miilc0mpx1hsr02m14yzgh1zd8qdvgmipqrkzc")))

(define-public crate-slog-stdlog-2.0.0-4.0 (c (n "slog-stdlog") (v "2.0.0-4.0") (d (list (d (n "crossbeam") (r "^0.2.9") (d #t) (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "slog") (r "^2") (d #t) (k 0)) (d (n "slog-scope") (r "^2") (d #t) (k 0)) (d (n "slog-term") (r "~2.0.0-4") (d #t) (k 0)))) (h "04b1nxbi4shfai5hk6n1n98sn14vggvsl9mxhr9x70k0pv8dfffa")))

(define-public crate-slog-stdlog-2.0.0 (c (n "slog-stdlog") (v "2.0.0") (d (list (d (n "crossbeam") (r "^0.2.9") (d #t) (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "slog") (r "^2") (d #t) (k 0)) (d (n "slog-scope") (r "^3") (d #t) (k 0)))) (h "1il2i83z509vpvscksjl06may0j7cj2pdhd8nz1b188lmv5bz27m")))

(define-public crate-slog-stdlog-3.0.0 (c (n "slog-stdlog") (v "3.0.0") (d (list (d (n "crossbeam") (r "^0.2.9") (d #t) (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "slog") (r "^2") (d #t) (k 0)) (d (n "slog-scope") (r "^4") (d #t) (k 0)))) (h "062a9j0y7rwilpqj753gcmxpphbwfmrsdp4cwsvx4gj197i0gqy4")))

(define-public crate-slog-stdlog-3.0.1 (c (n "slog-stdlog") (v "3.0.1") (d (list (d (n "crossbeam") (r "^0.2.9") (d #t) (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "slog") (r "^2") (d #t) (k 0)) (d (n "slog-scope") (r "^4") (d #t) (k 0)))) (h "12xlr13axa2ic6j5m9sjfdnrkxpw1zcbn8w4inwr21yqhk00b205")))

(define-public crate-slog-stdlog-3.0.2 (c (n "slog-stdlog") (v "3.0.2") (d (list (d (n "crossbeam") (r "^0.2.9") (d #t) (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "slog") (r "^2") (d #t) (k 0)) (d (n "slog-async") (r "^2") (d #t) (k 2)) (d (n "slog-scope") (r "^4") (d #t) (k 0)) (d (n "slog-term") (r "^2") (d #t) (k 2)))) (h "1a644kg6cjf4skaz83d1hy4dzp28n39i150gciywr5p998jzhhmc")))

(define-public crate-slog-stdlog-3.0.4-pre (c (n "slog-stdlog") (v "3.0.4-pre") (d (list (d (n "crossbeam") (r "^0.2.9") (d #t) (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "slog") (r "^2") (d #t) (k 0)) (d (n "slog-async") (r "^2") (d #t) (k 2)) (d (n "slog-scope") (r "^4") (d #t) (k 0)) (d (n "slog-term") (r "^2") (d #t) (k 2)))) (h "0hhk0xlgmn9z827wb80lm3fn0x0sc8njp9wa7vms6l7diicz8n7h")))

(define-public crate-slog-stdlog-3.0.5 (c (n "slog-stdlog") (v "3.0.5") (d (list (d (n "crossbeam") (r "^0.2.9") (d #t) (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "slog") (r "^2") (d #t) (k 0)) (d (n "slog-async") (r "^2") (d #t) (k 2)) (d (n "slog-scope") (r "^4") (d #t) (k 0)) (d (n "slog-term") (r "^2") (d #t) (k 2)))) (h "00fgkrg27n5lwz3gdccl1jslgbvc40rd2v76xvwkcgqy7mbnki7i")))

(define-public crate-slog-stdlog-4.0.0 (c (n "slog-stdlog") (v "4.0.0") (d (list (d (n "crossbeam") (r "^0.7.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (f (quote ("std"))) (d #t) (k 0)) (d (n "slog") (r "^2") (d #t) (k 0)) (d (n "slog-async") (r "^2") (d #t) (k 2)) (d (n "slog-scope") (r "^4") (d #t) (k 0)) (d (n "slog-term") (r "^2") (d #t) (k 2)))) (h "08sc604j19zxc6kb64ppig2nhf7lgqzsrhrbv2i5srdg7f88fkdy")))

(define-public crate-slog-stdlog-4.1.0 (c (n "slog-stdlog") (v "4.1.0") (d (list (d (n "log") (r "^0.4.11") (f (quote ("std"))) (d #t) (k 0)) (d (n "slog") (r "^2.4") (d #t) (k 0)) (d (n "slog-async") (r "^2") (d #t) (k 2)) (d (n "slog-scope") (r "^4") (d #t) (k 0)) (d (n "slog-term") (r "^2") (d #t) (k 2)))) (h "0nhg9mwaf5y1gs2f7nbz3r9fngq0g3d3qvz66z5lzgxd09rsna42")))

(define-public crate-slog-stdlog-4.1.1 (c (n "slog-stdlog") (v "4.1.1") (d (list (d (n "log") (r "^0.4.11") (f (quote ("std"))) (d #t) (k 0)) (d (n "slog") (r "^2.4") (d #t) (k 0)) (d (n "slog-async") (r "^2") (d #t) (k 2)) (d (n "slog-scope") (r "^4") (d #t) (k 0)) (d (n "slog-term") (r "^2") (d #t) (k 2)))) (h "0gpsf62ckblpc6a70dnhsz677c7s5cz4glpqsf8p5bmvwnnb41k7") (f (quote (("kv_unstable" "log/kv_unstable_std" "slog/dynamic-keys") ("default")))) (r "1.38")))

