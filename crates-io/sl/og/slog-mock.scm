(define-module (crates-io sl og slog-mock) #:use-module (crates-io))

(define-public crate-slog-mock-0.1.1 (c (n "slog-mock") (v "0.1.1") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1w5g2ig7qpmgbvs2c5l2zk45h3ixr9af4vk43mn22csalk3xyaz1") (y #t)))

(define-public crate-slog-mock-0.2.0 (c (n "slog-mock") (v "0.2.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1id9xhxw58vrffm588x09nlzr8agals7api4fgjmam3sp5fcggm9") (y #t)))

(define-public crate-slog-mock-0.3.0 (c (n "slog-mock") (v "0.3.0") (d (list (d (n "slog-mock-proc-macros") (r "^0.3") (d #t) (k 0)))) (h "0ai0npm8mnww2mqzq4c1a67a6073hkk6jzdr6js5khc4i2b5qab4") (y #t)))

(define-public crate-slog-mock-0.4.0 (c (n "slog-mock") (v "0.4.0") (d (list (d (n "slog-mock-proc-macros") (r "^0.4") (d #t) (k 0)))) (h "01jq022c20hix59m38rm2qiy198x8s79chi9apm2h4c8y1qqmi3d") (y #t)))

(define-public crate-slog-mock-0.5.0 (c (n "slog-mock") (v "0.5.0") (d (list (d (n "slog-mock-proc-macros") (r "^0.4") (d #t) (k 0)))) (h "0g72bdcyqwhzs2f5s2lj7hgqxp2x3mrjfyqx7j9cxm3x0j3w8pf0") (y #t)))

