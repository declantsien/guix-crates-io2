(define-module (crates-io sl og slog-html) #:use-module (crates-io))

(define-public crate-slog-html-0.1.0 (c (n "slog-html") (v "0.1.0") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "slog") (r "^1.4") (f (quote ("max_level_trace" "release_max_level_trace"))) (d #t) (k 0)) (d (n "slog-stream") (r "^1.2") (d #t) (k 0)))) (h "10iy1vxzg9h16id0vawf59cv20j756ikkx9qy5j1mz87n6q0kwq8")))

(define-public crate-slog-html-0.1.1 (c (n "slog-html") (v "0.1.1") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "slog") (r "^1.4") (f (quote ("max_level_trace" "release_max_level_trace"))) (d #t) (k 0)) (d (n "slog-stream") (r "^1.2") (d #t) (k 0)))) (h "1y0k4f7s6mm2y6p7s4w1m39042jp1r111lbdzs056dyn2bc5vvbq")))

(define-public crate-slog-html-0.1.2 (c (n "slog-html") (v "0.1.2") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "slog") (r "^1.4") (f (quote ("max_level_trace" "release_max_level_trace"))) (d #t) (k 0)) (d (n "slog-stream") (r "^1.2") (d #t) (k 0)))) (h "0ndxq388qpw4p1vlamnb3793k0x3kgyvdhxkv88avd7nzvlligpi")))

(define-public crate-slog-html-0.1.3 (c (n "slog-html") (v "0.1.3") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "slog") (r "^1.4") (f (quote ("max_level_trace" "release_max_level_trace"))) (d #t) (k 0)) (d (n "slog-stream") (r "^1.2") (d #t) (k 0)))) (h "0a5i5cpw4rcvwzj1v8x94amcnkszbscs8kpgqc147lhq4ksl999z")))

