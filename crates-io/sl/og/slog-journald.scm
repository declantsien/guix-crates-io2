(define-module (crates-io sl og slog-journald) #:use-module (crates-io))

(define-public crate-slog-journald-1.0.0 (c (n "slog-journald") (v "1.0.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libsystemd-sys") (r "^0.0") (d #t) (k 0)) (d (n "slog") (r "^1.1.0") (d #t) (k 0)))) (h "1lcahpvs8xpw131a7hw26kjhmkdxqj0alawma6idqib6771vspjx")))

(define-public crate-slog-journald-2.0.0 (c (n "slog-journald") (v "2.0.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libsystemd-sys") (r "^0.0") (d #t) (k 0)) (d (n "slog") (r "^2.0.4") (d #t) (k 0)))) (h "14mjr728ipzaqgzjb0ahmflazalkzd9ibnk6lny0knk6ifrzy9iv")))

(define-public crate-slog-journald-2.1.0 (c (n "slog-journald") (v "2.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libsystemd-sys") (r "^0.2") (d #t) (k 0)) (d (n "slog") (r "^2.5") (d #t) (k 0)))) (h "00ks0dp69hyk93gjbff5k3mkzlm4886r8ah79yin3vyr2dz0fn8z")))

(define-public crate-slog-journald-2.1.1 (c (n "slog-journald") (v "2.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libsystemd-sys") (r "^0.2") (d #t) (k 0)) (d (n "slog") (r "^2.5") (d #t) (k 0)))) (h "19zb9mxdb984977k3gyjn9vn7hnwd8699nkmx49wkch3ibhg3wxn")))

(define-public crate-slog-journald-2.2.0 (c (n "slog-journald") (v "2.2.0") (d (list (d (n "libsystemd") (r "^0.4.1") (d #t) (k 0)) (d (n "slog") (r "^2.7") (d #t) (k 0)))) (h "175kks8v2p67k777skrkrg2lsph9j5ihxi5skzycil7mqaw4xqc3") (f (quote (("log_error_sources") ("log_errno"))))))

