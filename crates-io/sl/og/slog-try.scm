(define-module (crates-io sl og slog-try) #:use-module (crates-io))

(define-public crate-slog-try-0.1.0 (c (n "slog-try") (v "0.1.0") (d (list (d (n "slog") (r "^2.0.9") (f (quote ("max_level_trace" "release_max_level_trace"))) (d #t) (k 2)) (d (n "slog-term") (r "^2.2.0") (d #t) (k 2)))) (h "0bvmrjql7fmxfg0n0pr2apkl81xh6dlir69nam35fs2hv8vsq8aq")))

(define-public crate-slog-try-0.1.1 (c (n "slog-try") (v "0.1.1") (d (list (d (n "slog") (r "= 2.1.1") (f (quote ("max_level_trace" "release_max_level_trace"))) (d #t) (k 0)) (d (n "slog-term") (r "^2") (d #t) (k 2)))) (h "0y7dnzfkqvga2z9klgrlp7g2jblnp2z9pm0m6sgpwwi83ssy2bj5")))

(define-public crate-slog-try-0.2.0 (c (n "slog-try") (v "0.2.0") (d (list (d (n "slog") (r "^2") (d #t) (k 0)) (d (n "slog") (r "^2") (f (quote ("max_level_trace"))) (d #t) (k 2)) (d (n "slog-term") (r "^2") (d #t) (k 2)))) (h "0a73hf5r0s4p73c5jdsagq7pmry5ib4jxdwk4g9laxrmrkhs32gm")))

(define-public crate-slog-try-1.0.0 (c (n "slog-try") (v "1.0.0") (d (list (d (n "rustversion") (r "^1") (d #t) (k 1)) (d (n "slog") (r "^2") (d #t) (k 0)) (d (n "slog") (r "^2") (f (quote ("max_level_trace"))) (d #t) (k 2)))) (h "0p2hqqdsvhb9ysbjxgwvqk0c13szhwi6zjszis67vdwrs4scdk11")))

(define-public crate-slog-try-1.0.1 (c (n "slog-try") (v "1.0.1") (d (list (d (n "rustversion") (r "^1") (d #t) (k 1)) (d (n "slog") (r "^2") (d #t) (k 0)) (d (n "slog") (r "^2") (f (quote ("max_level_trace"))) (d #t) (k 2)))) (h "0p2gjrlqln4fprbfw654k2rpd92xxh6z9viyviihka0vrkk0dsxi")))

(define-public crate-slog-try-1.0.2 (c (n "slog-try") (v "1.0.2") (d (list (d (n "rustversion") (r "^1") (d #t) (k 1)) (d (n "slog") (r "^2.7.0") (d #t) (k 0)) (d (n "slog") (r "^2.7.0") (f (quote ("max_level_trace"))) (d #t) (k 2)))) (h "16xgcymiwywph1i8qwgllrz6m048dxkvq8m9hq71ivyf6h07zwg2")))

