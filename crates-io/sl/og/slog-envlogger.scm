(define-module (crates-io sl og slog-envlogger) #:use-module (crates-io))

(define-public crate-slog-envlogger-0.1.0 (c (n "slog-envlogger") (v "0.1.0") (d (list (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "slog") (r "^1.0.0-alpha") (d #t) (k 0)) (d (n "slog-stdlog") (r "^1.0.0-alpha") (d #t) (k 0)) (d (n "slog-term") (r "^1.0.0-alpha") (d #t) (k 0)))) (h "106hmn2xxfs8bpigcskxr28ajni3wqaazgva7vjbfz0zx06ky1c5")))

(define-public crate-slog-envlogger-0.1.1 (c (n "slog-envlogger") (v "0.1.1") (d (list (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "slog") (r "^1.0.0-alpha") (d #t) (k 0)) (d (n "slog-stdlog") (r "^1.0.0-alpha2") (d #t) (k 0)) (d (n "slog-term") (r "^1.0.0-alpha") (d #t) (k 0)))) (h "0i31mkvdkmvblpmd039l7air0g1rw25p43jfvdfjfx0iicp69w23")))

(define-public crate-slog-envlogger-0.2.0 (c (n "slog-envlogger") (v "0.2.0") (d (list (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "slog") (r "^1.0.0-alpha") (d #t) (k 0)) (d (n "slog-stdlog") (r "^1.0.0-alpha3") (d #t) (k 0)) (d (n "slog-term") (r "^1.0.0-alpha") (d #t) (k 0)))) (h "0w3545z1fs82psxjvqwnfdfwwja3cw4f53slhcqygfq4bmipljwy")))

(define-public crate-slog-envlogger-0.3.0 (c (n "slog-envlogger") (v "0.3.0") (d (list (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "slog") (r "^1.0.0-alpha2") (d #t) (k 0)) (d (n "slog-stdlog") (r "^1.0.0-alpha4") (d #t) (k 0)) (d (n "slog-term") (r "^1.0.0-alpha2") (d #t) (k 0)))) (h "0mz7kjmqbxapz98fhid09w41klfr5q5k5w5awz8cwi2rgnw4y5pq")))

(define-public crate-slog-envlogger-0.4.0 (c (n "slog-envlogger") (v "0.4.0") (d (list (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "slog") (r "^1.0.0-alpha3") (d #t) (k 0)) (d (n "slog-stdlog") (r "^1.0.0-alpha5") (d #t) (k 0)) (d (n "slog-term") (r "^1.0.0-alpha3") (d #t) (k 0)))) (h "1v3ij6h3pjw413dxf9308nmx8wq966ci42pfl72r44r3q5kjz9vf")))

(define-public crate-slog-envlogger-0.4.1 (c (n "slog-envlogger") (v "0.4.1") (d (list (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "slog") (r "^1.0.0-alpha3") (d #t) (k 0)) (d (n "slog-stdlog") (r "^1.0.0-alpha5") (d #t) (k 0)) (d (n "slog-term") (r "^1.0.0-alpha3") (d #t) (k 0)))) (h "0svb263zrh2kkjw438mljl2qlg7pjz2sfb75xa1awcsz0dwyscfq")))

(define-public crate-slog-envlogger-0.4.2 (c (n "slog-envlogger") (v "0.4.2") (d (list (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "slog") (r "^1.0.0-alpha6") (d #t) (k 0)) (d (n "slog-stdlog") (r "^1.0.0-alpha6") (d #t) (k 0)) (d (n "slog-term") (r "^1.0.0-alpha6") (d #t) (k 0)))) (h "0rz57gfv00pwja99f76675ham2b8shq0njvqyb8fi4d75zkdwh0l")))

(define-public crate-slog-envlogger-0.5.0 (c (n "slog-envlogger") (v "0.5.0") (d (list (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "slog") (r "^1") (d #t) (k 0)) (d (n "slog-stdlog") (r "^1.0.0-alpha8") (d #t) (k 0)) (d (n "slog-term") (r "^1.0.0-alpha8") (d #t) (k 0)))) (h "0y7g5mk6k84mfjky59xr909qafayp7jvqng6j27krhqhnddp3snz")))

(define-public crate-slog-envlogger-2.0.0-1.0 (c (n "slog-envlogger") (v "2.0.0-1.0") (d (list (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "slog") (r ">= 2.0.0-1.0, < 2.0.0-2") (d #t) (k 0)) (d (n "slog-async") (r ">= 2.0.0-1, < 2.0.0-2") (d #t) (k 0)) (d (n "slog-scope") (r ">= 2.0.0-1, < 2.0.0-2") (d #t) (k 0)) (d (n "slog-stdlog") (r ">= 2.0.0-0, < 2.0.0-1") (d #t) (k 0)) (d (n "slog-term") (r ">= 2.0.0-1, < 2.0.0-2") (d #t) (k 0)))) (h "08npb2rjb3ih8r240kf01gx46bibcnv58qc9ablzqxg6sgj4s86g")))

(define-public crate-slog-envlogger-2.0.0-3.0 (c (n "slog-envlogger") (v "2.0.0-3.0") (d (list (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "slog") (r "~2.0.0-3") (d #t) (k 0)) (d (n "slog-async") (r "~2.0.0-3") (d #t) (k 0)) (d (n "slog-scope") (r "~2.0.0-3") (d #t) (k 0)) (d (n "slog-stdlog") (r "~2.0.0-3") (d #t) (k 0)) (d (n "slog-term") (r "~2.0.0-3") (d #t) (k 0)))) (h "073d6ii51k3fzka00jgmwyd1gapdybxq60sbjl6lp3ckqck6vi6m")))

(define-public crate-slog-envlogger-2.0.0 (c (n "slog-envlogger") (v "2.0.0") (d (list (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "slog") (r "^2") (d #t) (k 0)) (d (n "slog-async") (r "^2") (d #t) (k 0)) (d (n "slog-scope") (r "^3") (d #t) (k 0)) (d (n "slog-stdlog") (r "^2") (d #t) (k 0)) (d (n "slog-term") (r "^2") (d #t) (k 0)))) (h "13b03xzhpjwvfd5kz3xy877flila14hya4lcjzzc5qc4l1vkdljm")))

(define-public crate-slog-envlogger-2.1.0 (c (n "slog-envlogger") (v "2.1.0") (d (list (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "slog") (r "^2") (d #t) (k 0)) (d (n "slog-async") (r "^2") (d #t) (k 0)) (d (n "slog-scope") (r "^4") (d #t) (k 0)) (d (n "slog-stdlog") (r "^3") (d #t) (k 0)) (d (n "slog-term") (r "^2") (d #t) (k 0)))) (h "0jrqblsccalfzhk4ji89svmkn0azbpdb6b794j3gasq8h18niipp")))

(define-public crate-slog-envlogger-2.2.0 (c (n "slog-envlogger") (v "2.2.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.2") (o #t) (d #t) (k 0)) (d (n "slog") (r "^2") (d #t) (k 0)) (d (n "slog-async") (r "^2") (d #t) (k 0)) (d (n "slog-async") (r "^2") (d #t) (k 2)) (d (n "slog-scope") (r "^4") (d #t) (k 0)) (d (n "slog-stdlog") (r "^4") (d #t) (k 0)) (d (n "slog-term") (r "^2") (d #t) (k 0)))) (h "1h7m0jnj6kvsn9553fyqvaw3swy3pwpmwamqyhnnkv9zqh5ilslh") (f (quote (("default" "regex"))))))

