(define-module (crates-io sl og slog-kvfilter) #:use-module (crates-io))

(define-public crate-slog-kvfilter-0.4.0 (c (n "slog-kvfilter") (v "0.4.0") (d (list (d (n "slog") (r "^2") (d #t) (k 0)))) (h "0an6c93zy4vnlfas4jic5yqjc4x7k86xr6dv1npq85zsvqlj0qbg")))

(define-public crate-slog-kvfilter-0.4.1 (c (n "slog-kvfilter") (v "0.4.1") (d (list (d (n "slog") (r "^2") (d #t) (k 0)))) (h "16vaski5fzqhjhmb2h09pbph230526p2dz2cpw4g7rpyhw23xwdm")))

(define-public crate-slog-kvfilter-0.5.0-pre (c (n "slog-kvfilter") (v "0.5.0-pre") (d (list (d (n "slog") (r "^2") (d #t) (k 0)))) (h "1njn7nmf5ag4m4k1z67z077nj08jq2aa637d4l69zsx0pfm1x68f")))

(define-public crate-slog-kvfilter-0.5.0 (c (n "slog-kvfilter") (v "0.5.0") (d (list (d (n "slog") (r "^2") (d #t) (k 0)))) (h "0j8qvbff1mbwpwww89v3a2w0kj4s2mrjvgknj5qjr4r8avbn1gb0")))

(define-public crate-slog-kvfilter-0.5.1 (c (n "slog-kvfilter") (v "0.5.1") (d (list (d (n "slog") (r "^2") (d #t) (k 0)))) (h "12bcr37fcffjv9jxqjkkni544zq6pdjfba90aqi6vm1d6rrr9gbm")))

(define-public crate-slog-kvfilter-0.5.2 (c (n "slog-kvfilter") (v "0.5.2") (d (list (d (n "slog") (r "^2") (d #t) (k 0)))) (h "0367lbzc6fw30mda95868pjsn56bqi6nlg02pnikhblzm311nl66")))

(define-public crate-slog-kvfilter-0.6.0 (c (n "slog-kvfilter") (v "0.6.0") (d (list (d (n "slog") (r "^2") (d #t) (k 0)))) (h "0xsgi0lb061y1vh3brh400dcfgvvcvd6ia48xvswxhb3d42q172s")))

(define-public crate-slog-kvfilter-0.6.1 (c (n "slog-kvfilter") (v "0.6.1") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "slog") (r "^2") (d #t) (k 0)))) (h "0mfxc4l0yscqm8qbklvv89nxv3gxyp0vws4rpzx78psqadn46n9m")))

(define-public crate-slog-kvfilter-0.7.0 (c (n "slog-kvfilter") (v "0.7.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "slog") (r "^2") (d #t) (k 0)))) (h "1q3mq6a9aq8iscy9xh97zifxrxa6y10d8p2gkxlxkvk9s7brx4xf")))

