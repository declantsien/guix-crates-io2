(define-module (crates-io sl og slog-unwrap) #:use-module (crates-io))

(define-public crate-slog-unwrap-0.1.0 (c (n "slog-unwrap") (v "0.1.0") (h "1k9yc5vymi9jy9hzqz0rwdff27gsndpjybz1390jjzj1h77d7hr8")))

(define-public crate-slog-unwrap-0.1.1 (c (n "slog-unwrap") (v "0.1.1") (d (list (d (n "slog") (r "^2.5") (d #t) (k 0)))) (h "0n2jcbp6926hfglr7cc40annd71294bwdhakcsa17bqm47x6cwd6")))

(define-public crate-slog-unwrap-0.1.2 (c (n "slog-unwrap") (v "0.1.2") (d (list (d (n "slog") (r "^2.5") (d #t) (k 0)))) (h "1a0y66zbs502d227svgvpaspsaprz80c9wlvla41qv214qm22vvw")))

(define-public crate-slog-unwrap-0.1.3 (c (n "slog-unwrap") (v "0.1.3") (d (list (d (n "slog") (r "^2.5") (d #t) (k 0)))) (h "0fcdkj9nb6bfrkcfra1lp8hfhx639pn2hwhhgd6v6s5s5j2710hz")))

(define-public crate-slog-unwrap-0.1.4 (c (n "slog-unwrap") (v "0.1.4") (d (list (d (n "slog") (r "^2.5") (d #t) (k 0)))) (h "05yp0x4xmhy5skrd0dqfwf0jqb28scdqwl3jhzdplxi26dqk3zaq")))

(define-public crate-slog-unwrap-0.1.5 (c (n "slog-unwrap") (v "0.1.5") (d (list (d (n "slog") (r "^2.5") (d #t) (k 0)))) (h "0hc2dcd7bk913kicmwbqyy9drk50cxkqk4hkxa9snpzdi3zlfjcf") (f (quote (("panic-quiet") ("default" "panic-quiet"))))))

(define-public crate-slog-unwrap-0.8.0 (c (n "slog-unwrap") (v "0.8.0") (d (list (d (n "slog") (r "^2.5") (d #t) (k 0)) (d (n "slog-scope") (r "^4.3") (o #t) (d #t) (k 0)))) (h "02s9kqirpdmgk3jdkm98yd4vwnsvpd2ynql4xd8v24p757vb6m00") (f (quote (("scope" "slog-scope") ("panic-quiet") ("default" "panic-quiet"))))))

(define-public crate-slog-unwrap-0.9.0 (c (n "slog-unwrap") (v "0.9.0") (d (list (d (n "slog") (r "^2.5") (d #t) (k 0)) (d (n "slog-scope") (r "^4.3") (o #t) (d #t) (k 0)))) (h "0d3y57ay4n91mj87p2ssfk3dihz5376p5984jq90znkah55apn37") (f (quote (("scope" "slog-scope") ("panic-quiet") ("default" "panic-quiet"))))))

(define-public crate-slog-unwrap-0.9.1 (c (n "slog-unwrap") (v "0.9.1") (d (list (d (n "slog") (r "^2.5") (d #t) (k 0)) (d (n "slog-scope") (r "^4.3") (o #t) (d #t) (k 0)))) (h "0vg163jv48ri9j2qhx3r80yjbs6ssbh4jlhs16k2lbcjrzliva4a") (f (quote (("scope" "slog-scope") ("panic-quiet") ("default" "panic-quiet"))))))

(define-public crate-slog-unwrap-0.9.2 (c (n "slog-unwrap") (v "0.9.2") (d (list (d (n "slog") (r "^2.5") (d #t) (k 0)) (d (n "slog-scope") (r "^4.3") (o #t) (d #t) (k 0)))) (h "1jidkxqr8drg0jf34h387ii0vjlndnqdf18l73szl8m6k3v591yc") (f (quote (("scope" "slog-scope") ("panic-quiet") ("default" "panic-quiet"))))))

