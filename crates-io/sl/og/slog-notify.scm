(define-module (crates-io sl og slog-notify) #:use-module (crates-io))

(define-public crate-slog-notify-0.1.0 (c (n "slog-notify") (v "0.1.0") (d (list (d (n "notify-rust") (r "^3.2") (d #t) (k 0)) (d (n "slog") (r "^1.5") (d #t) (k 0)) (d (n "slog-term") (r "^1.5") (d #t) (k 0)))) (h "0xh5b3qlnadwkmd39cd7hxf4whcn9nasxii9bfa5x3q8zqf7lans")))

(define-public crate-slog-notify-0.2.0 (c (n "slog-notify") (v "0.2.0") (d (list (d (n "notify-rust") (r "^3.2") (d #t) (k 0)) (d (n "slog") (r "^2") (d #t) (k 0)) (d (n "slog-async") (r "^2") (d #t) (k 2)) (d (n "slog-term") (r "^2") (d #t) (k 0)))) (h "1xq4js3ml0wvh0af871kglycm3w04li7pdl4q1jn4bwb4fdd6068")))

