(define-module (crates-io sl im slim_protocol) #:use-module (crates-io))

(define-public crate-slim_protocol-0.1.0 (c (n "slim_protocol") (v "0.1.0") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "ulid") (r "^1") (d #t) (k 0)))) (h "17brnc6cdxj9nsflhqp82prib5b05yimhrw6szry3g3xzgr6bf4x")))

(define-public crate-slim_protocol-0.2.0 (c (n "slim_protocol") (v "0.2.0") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "ulid") (r "^1") (d #t) (k 0)))) (h "0gr8204s24y14q7yzzl8602qbzji0wy9la4g2ibs4q5048m4yj4v")))

