(define-module (crates-io sl im slime) #:use-module (crates-io))

(define-public crate-slime-0.1.0 (c (n "slime") (v "0.1.0") (d (list (d (n "handlebars") (r "^0.29.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.4") (d #t) (k 0)))) (h "1abmvk87lr6vl7viwn4h57f38s6inr2saknjn72rr76jw0lapgy5")))

(define-public crate-slime-0.2.0 (c (n "slime") (v "0.2.0") (d (list (d (n "handlebars") (r "^0.29.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.4") (d #t) (k 0)))) (h "03q4nf55nh8in5kpxh81v0czx2a48rlrkc36ffcja2cm6m8ay9hs")))

(define-public crate-slime-0.3.0 (c (n "slime") (v "0.3.0") (d (list (d (n "fs_extra") (r "^1.1.0") (d #t) (k 0)) (d (n "handlebars") (r "^0.30.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.9") (d #t) (k 0)) (d (n "toml") (r "^0.4.5") (d #t) (k 0)))) (h "0zlm3q57g8y1gk415qm0w05dlj2i2m59mxl8ssf75dapcy5fd59h")))

(define-public crate-slime-0.4.0 (c (n "slime") (v "0.4.0") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "fs_extra") (r "^1.1.0") (d #t) (k 0)) (d (n "handlebars") (r "^0.32.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.9") (d #t) (k 0)) (d (n "toml") (r "^0.4.5") (d #t) (k 0)))) (h "1a27k27pfbfwwr2grq83x5mkx0fsccfd71kgpmzq3plyszxnlcjd")))

