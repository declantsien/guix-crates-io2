(define-module (crates-io sl im slimmemeter) #:use-module (crates-io))

(define-public crate-slimmemeter-0.2.0 (c (n "slimmemeter") (v "0.2.0") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dsmr5") (r "^0.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "influxdb") (r "^0.5") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rumqttc") (r "^0.18") (f (quote ("url"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serial") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1.22") (f (quote ("full"))) (d #t) (k 0)))) (h "047c558r4aj5j84xdwaqb35bjaypqr3sf4579mk0yh2k686y0whq")))

(define-public crate-slimmemeter-0.2.1 (c (n "slimmemeter") (v "0.2.1") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dsmr5") (r "^0.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rumqttc") (r "^0.18") (f (quote ("url"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serial") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1.22") (f (quote ("full"))) (d #t) (k 0)))) (h "0z4qfcassskxvcjijkcnblndc4is4ln9hp06fbpyf58ngr73bj61")))

