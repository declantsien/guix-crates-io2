(define-module (crates-io sl im slimbox) #:use-module (crates-io))

(define-public crate-slimbox-0.0.1 (c (n "slimbox") (v "0.0.1") (h "00ncc5ai41q6cwwfvcqvf3vd6s3xd71p95y4cvhpng0cralzj401") (f (quote (("unsafe_stable") ("nightly"))))))

(define-public crate-slimbox-0.0.2 (c (n "slimbox") (v "0.0.2") (h "0q69amhrblqizwdsnlj0n9r112ab3jzvs06x1zz730s7z1axzbxp") (f (quote (("unsafe_stable") ("nightly"))))))

(define-public crate-slimbox-0.1.0 (c (n "slimbox") (v "0.1.0") (h "06kbchmrs1wblzlhg9xcjjhrxpyk63phv7jv4jwy8c789xzczpr6") (f (quote (("unsafe_stable") ("nightly"))))))

