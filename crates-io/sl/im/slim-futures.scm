(define-module (crates-io sl im slim-futures) #:use-module (crates-io))

(define-public crate-slim-futures-0.1.0-alpha.0 (c (n "slim-futures") (v "0.1.0-alpha.0") (d (list (d (n "fn-traits") (r "^0.1") (d #t) (k 0)) (d (n "futures-core") (r "^0.3") (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2") (d #t) (k 0)) (d (n "criterion") (r "^0.4") (f (quote ("async_futures" "html_reports"))) (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt"))) (d #t) (k 2)))) (h "1sg3shrwv5b0b2k6ssjipdf00hfzkp8fx0pfbfngw2plmjv8pf12")))

