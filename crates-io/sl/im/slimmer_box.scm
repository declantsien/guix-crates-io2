(define-module (crates-io sl im slimmer_box) #:use-module (crates-io))

(define-public crate-slimmer_box-0.5.0 (c (n "slimmer_box") (v "0.5.0") (d (list (d (n "ptr_meta") (r "^0.2.0") (k 0)) (d (n "rkyv") (r "^0.7.39") (o #t) (d #t) (k 0)))) (h "0ylsk1273pm4f9qg5qsy7d5lav5rn3pg01mvsp7aiy7hl0rlgnhh") (f (quote (("std"))))))

(define-public crate-slimmer_box-0.5.1 (c (n "slimmer_box") (v "0.5.1") (d (list (d (n "ptr_meta") (r "^0.2.0") (k 0)) (d (n "rkyv") (r "^0.7.39") (o #t) (d #t) (k 0)))) (h "07skdismwa2xi16cx32xklj2hbkhbjx2vvyly8872r74nf3g8lrv") (f (quote (("std"))))))

(define-public crate-slimmer_box-0.5.2 (c (n "slimmer_box") (v "0.5.2") (d (list (d (n "ptr_meta") (r "^0.2.0") (k 0)) (d (n "rkyv") (r "^0.7.39") (o #t) (d #t) (k 0)))) (h "0mz0l68fgm9r2qzs1rn9s6k8lqmqcpz4h8ya5ipca0nv12ax0ls1") (f (quote (("std"))))))

(define-public crate-slimmer_box-0.6.0 (c (n "slimmer_box") (v "0.6.0") (d (list (d (n "ptr_meta") (r "^0.2.0") (k 0)) (d (n "rkyv") (r "^0.7.39") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "08sz5y2k3pavqxz42ih4zra3qcf5j7gy5p1xgbv3gmylrjfbf9mr") (f (quote (("std" "ptr_meta/std") ("default" "std"))))))

(define-public crate-slimmer_box-0.6.1 (c (n "slimmer_box") (v "0.6.1") (d (list (d (n "ptr_meta") (r "^0.2.0") (k 0)) (d (n "rkyv") (r "^0.7.39") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "1g978km8bbx2c6znbc3ga434vvzic8m7cmrlwm11q4qnrnrqb530") (f (quote (("std" "ptr_meta/std") ("default" "std"))))))

(define-public crate-slimmer_box-0.6.2 (c (n "slimmer_box") (v "0.6.2") (d (list (d (n "ptr_meta") (r "^0.2.0") (k 0)) (d (n "rkyv") (r "^0.7.39") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "0jvmd16r1jirdjbhppdsa0cqyv76zjdrkq5gvc6x9xqbh22ph7y3") (f (quote (("std" "ptr_meta/std") ("default" "std"))))))

(define-public crate-slimmer_box-0.6.3 (c (n "slimmer_box") (v "0.6.3") (d (list (d (n "ptr_meta") (r "^0.2.0") (k 0)) (d (n "rkyv") (r "^0.7.39") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "0wqrlzd0v67nhp9h696yh4k8p02df91gqd17n4q25n2rsgf40vrw") (f (quote (("std" "ptr_meta/std") ("default" "std"))))))

(define-public crate-slimmer_box-0.6.4 (c (n "slimmer_box") (v "0.6.4") (d (list (d (n "ptr_meta") (r "^0.2.0") (k 0)) (d (n "rkyv") (r "^0.7.39") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "1jf0zpyb37z0ii6h4kanv6hlcjl7px4jv15zdhhy9y9ikbnh28c5") (f (quote (("std" "ptr_meta/std") ("default" "std"))))))

(define-public crate-slimmer_box-0.6.5 (c (n "slimmer_box") (v "0.6.5") (d (list (d (n "ptr_meta") (r "^0.2.0") (k 0)) (d (n "rkyv") (r "^0.7.39") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "1qnhrd15198gk8h18lzjqzpfsf10swqw5xqij34xbm7zay7b7wfd") (f (quote (("std" "ptr_meta/std") ("default" "std"))))))

