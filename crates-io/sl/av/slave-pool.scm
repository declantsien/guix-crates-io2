(define-module (crates-io sl av slave-pool) #:use-module (crates-io))

(define-public crate-slave-pool-0.1.0 (c (n "slave-pool") (v "0.1.0") (d (list (d (n "crossbeam-channel") (r "^0.4") (k 0)) (d (n "parking_lot") (r "^0.10") (k 0)))) (h "1vdiiamx8nialqxbpzajl2254xgf0gwk197h50mq7ab6fmg57jnf") (y #t)))

(define-public crate-slave-pool-0.1.1 (c (n "slave-pool") (v "0.1.1") (d (list (d (n "crossbeam-channel") (r "^0.4") (k 0)) (d (n "parking_lot") (r "^0.10") (k 0)))) (h "0ilmd0l34vd77wr56wbccjscmij92la8kz05rpbqdqp2avrvjxhw") (y #t)))

(define-public crate-slave-pool-0.1.2 (c (n "slave-pool") (v "0.1.2") (d (list (d (n "crossbeam-channel") (r "^0.4") (k 0)))) (h "1f371ishb8bzlkfh1b0pwaxndq1ydimj9c00k3mviabbjszji3is") (y #t)))

(define-public crate-slave-pool-0.2.0 (c (n "slave-pool") (v "0.2.0") (d (list (d (n "crossbeam-channel") (r "^0.4") (k 0)))) (h "1fksbq8lp1dcb3g9ij2mydgfz0xwcnn3jybpdnd92z89n3s9v8ny") (y #t)))

(define-public crate-slave-pool-0.2.1 (c (n "slave-pool") (v "0.2.1") (d (list (d (n "crossbeam-channel") (r "^0.4") (k 0)))) (h "18mcy150k3gvs0kfi13798wdl2j802cy7509lsfvz6np7zjm3plf")))

(define-public crate-slave-pool-0.2.2 (c (n "slave-pool") (v "0.2.2") (d (list (d (n "crossbeam-channel") (r "^0.4") (k 0)))) (h "0jdvl44mc3bx2mrmnbhlz3x35jcfnsfqb0k80jr6p8k6xqd2an2c")))

(define-public crate-slave-pool-0.2.3 (c (n "slave-pool") (v "0.2.3") (d (list (d (n "crossbeam-channel") (r "^0.5") (f (quote ("std"))) (k 0)))) (h "012g1k5jv18i4hl9lnvffkmq555krjyxx6lqyvnfsisdizi0f451")))

