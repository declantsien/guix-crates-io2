(define-module (crates-io sl ca slcan) #:use-module (crates-io))

(define-public crate-slcan-0.1.2 (c (n "slcan") (v "0.1.2") (d (list (d (n "serial") (r "^0.4") (d #t) (k 2)) (d (n "serial-core") (r "^0.4") (d #t) (k 0)))) (h "091480c3cvcs939v044xdm2y6bjjk8iy93hnnyvp1vd3azn05l56")))

(define-public crate-slcan-0.1.3 (c (n "slcan") (v "0.1.3") (d (list (d (n "serial") (r "^0.4") (d #t) (k 2)) (d (n "serial-core") (r "^0.4") (d #t) (k 0)))) (h "0cbpmyk4d5b8yw4fvvn0189piw35fjp5q8swipddsjrwdk5nryxq")))

