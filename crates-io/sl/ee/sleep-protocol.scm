(define-module (crates-io sl ee sleep-protocol) #:use-module (crates-io))

(define-public crate-sleep-protocol-0.1.0 (c (n "sleep-protocol") (v "0.1.0") (d (list (d (n "protobuf") (r "^1.4.3") (d #t) (k 0)) (d (n "protoc-rust") (r "^1.4") (d #t) (k 1)))) (h "1pdhkn53l5di1vaaywgkqkdk8zsg3shh46n00ypydlkapxjixqwh")))

(define-public crate-sleep-protocol-0.1.1 (c (n "sleep-protocol") (v "0.1.1") (d (list (d (n "protobuf") (r "^1.4.3") (d #t) (k 0)) (d (n "protoc-rust") (r "^1.4") (d #t) (k 1)))) (h "0fhw8z68l19j5wq0hkid46km8z198m5b8cmj5gf0h6ryxnv01sa6")))

