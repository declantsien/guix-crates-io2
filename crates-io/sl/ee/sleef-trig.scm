(define-module (crates-io sl ee sleef-trig) #:use-module (crates-io))

(define-public crate-sleef-trig-0.1.0 (c (n "sleef-trig") (v "0.1.0") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "sleef-trig-sys") (r "^0.1.0") (d #t) (k 2)))) (h "09850kg50ilk5kbawn68rarm3wp7p06r5ic4ag6xy1daxilakihs")))

