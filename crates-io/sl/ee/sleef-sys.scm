(define-module (crates-io sl ee sleef-sys) #:use-module (crates-io))

(define-public crate-sleef-sys-0.1.0 (c (n "sleef-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.38") (d #t) (k 1)) (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (k 0)))) (h "1k7mxzaa915vzrqj6ngv8284yv5drighzb34r2rqm7acs0p859gi") (f (quote (("dft") ("default")))) (l "sleef")))

(define-public crate-sleef-sys-0.1.1 (c (n "sleef-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.38") (d #t) (k 1)) (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (k 0)))) (h "1nwkan44sj495vn8wbipp9aq2k0kbx96wr6xagi7hl1ic8v7c78h") (f (quote (("dft") ("default")))) (l "sleef")))

(define-public crate-sleef-sys-0.1.2 (c (n "sleef-sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.46") (d #t) (k 1)) (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "env_logger") (r "^0.6") (d #t) (k 1)) (d (n "libc") (r "^0.2") (k 0)))) (h "1881q2yc17j2m1yvh01447c93ws1mspnrj3k2nbvwbvcm8z81kkv") (f (quote (("dft") ("default")))) (l "sleef")))

