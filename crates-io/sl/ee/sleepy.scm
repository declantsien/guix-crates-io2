(define-module (crates-io sl ee sleepy) #:use-module (crates-io))

(define-public crate-sleepy-1.0.0 (c (n "sleepy") (v "1.0.0") (h "1lsg6pmvk24p7vay2bags3spxw1v7wg0izxycjrmk0079ig7c5vq") (y #t)))

(define-public crate-sleepy-1.0.1 (c (n "sleepy") (v "1.0.1") (h "0lic1r2ggqw13jsg6dxmwb7sgk2kdf9sapykbbnva9w6vcjw117n")))

