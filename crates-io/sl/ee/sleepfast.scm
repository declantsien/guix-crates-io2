(define-module (crates-io sl ee sleepfast) #:use-module (crates-io))

(define-public crate-sleepfast-1.0.0 (c (n "sleepfast") (v "1.0.0") (h "0gvmjd89gn56jy0w14fbmxli5mvjg03q6ly3r53cg6v0qsz4kqvp") (f (quote (("nightly"))))))

(define-public crate-sleepfast-1.1.0 (c (n "sleepfast") (v "1.1.0") (h "1d74xgz2ny3fk130fjzgx5pcasmm6lv41mlhz1d2k0sl49hkhxqq") (f (quote (("nightly"))))))

(define-public crate-sleepfast-1.2.0 (c (n "sleepfast") (v "1.2.0") (h "0qzk08696pvq37226v7q907z2pgkyqy52x4hplq2scx8kaxc4x59") (f (quote (("nightly"))))))

(define-public crate-sleepfast-1.3.0 (c (n "sleepfast") (v "1.3.0") (h "1dbx7gfh91sdv0xjv3smyashbzk4b70hqq7lfbxpg3hfk8lcawrz")))

