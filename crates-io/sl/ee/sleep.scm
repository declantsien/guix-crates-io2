(define-module (crates-io sl ee sleep) #:use-module (crates-io))

(define-public crate-sleep-1.0.0 (c (n "sleep") (v "1.0.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)))) (h "17d5w9jzis5qnpif0gnbbsqa5ny4hga96pb2wc71jvpkybz17qw9")))

(define-public crate-sleep-1.0.1 (c (n "sleep") (v "1.0.1") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)))) (h "0n67ymvjn41xafq8vdvj7ri7lhcbbyf5kk7hzv42qk25bczrgxr8")))

(define-public crate-sleep-1.0.2 (c (n "sleep") (v "1.0.2") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)))) (h "11lyc3cagw3kk241qp76xcbs0rmqaczysm93qf1gn1d90fwh8h95")))

