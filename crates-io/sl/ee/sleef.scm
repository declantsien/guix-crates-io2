(define-module (crates-io sl ee sleef) #:use-module (crates-io))

(define-public crate-sleef-0.1.0 (c (n "sleef") (v "0.1.0") (d (list (d (n "doubled") (r "^0.2.3") (f (quote ("packed_simd"))) (d #t) (k 0)) (d (n "packed_simd") (r "^0.3.8") (f (quote ("into_bits"))) (d #t) (k 0) (p "packed_simd_2")) (d (n "rand") (r "^0.8.5") (f (quote ("simd_support"))) (d #t) (k 2)) (d (n "rug") (r "^1.16") (d #t) (k 2)))) (h "0mviy7gawqijkbz0gfc9jb1p7m12n5wrxkv12zppyw0h8jgbi6d5") (f (quote (("default"))))))

(define-public crate-sleef-0.2.0 (c (n "sleef") (v "0.2.0") (d (list (d (n "doubled") (r "^0.3.0") (f (quote ("simd"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rug") (r "^1.16") (d #t) (k 2)))) (h "0c9bj7ap7299k5p18xsqpbg830xxnszkvp7qwiwjv7hnjgid1x1j") (f (quote (("default"))))))

(define-public crate-sleef-0.3.0 (c (n "sleef") (v "0.3.0") (d (list (d (n "doubled") (r "^0.3.1") (f (quote ("simd"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rug") (r "^1.16") (d #t) (k 2)))) (h "1xzvrb22d6z6lr03b9lw3zvnwc3cxccli2xm4q1fya9hy756sa0w") (f (quote (("default"))))))

(define-public crate-sleef-0.3.1 (c (n "sleef") (v "0.3.1") (d (list (d (n "doubled") (r "^0.3.2") (f (quote ("simd"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rug") (r "^1.16") (d #t) (k 2)))) (h "0cr2g2mr5d9sm4dnd6ldpl8qhsdqrvqch27x4s8gqd61p58qx57v") (f (quote (("default"))))))

(define-public crate-sleef-0.3.2 (c (n "sleef") (v "0.3.2") (d (list (d (n "doubled") (r "^0.3.2") (f (quote ("simd"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rug") (r "^1.16") (d #t) (k 2)))) (h "0zgl9smpnsir199ys76xg2xiv3mfdmk0lrinawyh26cs0j461qcz") (f (quote (("default"))))))

