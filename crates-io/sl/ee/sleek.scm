(define-module (crates-io sl ee sleek) #:use-module (crates-io))

(define-public crate-sleek-0.1.0 (c (n "sleek") (v "0.1.0") (d (list (d (n "clap") (r "^4.2.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "sqlformat") (r "^0.2.1") (d #t) (k 0)))) (h "0v7gllj4g0r253smnjydsqn7j4bnynbbj49znckil2wcszl0c62n") (y #t)))

(define-public crate-sleek-0.1.1 (c (n "sleek") (v "0.1.1") (d (list (d (n "clap") (r "^4.2.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "sqlformat") (r "^0.2.1") (d #t) (k 0)))) (h "00k8njgx71jcncsn7hcvkhg4nns57z0f6wqq328bbqg4cg8zil39")))

(define-public crate-sleek-0.1.2 (c (n "sleek") (v "0.1.2") (d (list (d (n "clap") (r "^4.2.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "sqlformat") (r "^0.2.1") (d #t) (k 0)))) (h "0mz2kgfjhqid0kgzraad86vby1gy04b81fq5ar2kfcrxrgw35gil")))

(define-public crate-sleek-0.2.0 (c (n "sleek") (v "0.2.0") (d (list (d (n "clap") (r "^4.2.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "sqlformat") (r "^0.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "1r5i5lfzhr6wa7zs7mg5c2pgkfbv44gqjbw0hwc29yvi81jzf119")))

(define-public crate-sleek-0.2.1 (c (n "sleek") (v "0.2.1") (d (list (d (n "clap") (r "^4.2.1") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "sqlformat") (r "^0.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0vkaddfra7vky7mmkhpqxn44lzam4r4mdqd81x4xcb66vjsa3qi6")))

(define-public crate-sleek-0.2.2 (c (n "sleek") (v "0.2.2") (d (list (d (n "clap") (r "^4.2.1") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "sqlformat") (r "^0.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0c7lrhqry3rjivfhh2izf87hxnqzhq17f0zj5n54p04vmywa2clz")))

(define-public crate-sleek-0.2.3 (c (n "sleek") (v "0.2.3") (d (list (d (n "clap") (r "^4.2.1") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "sqlformat") (r "^0.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0h92pkwdxfrv1a1r0amcns82fpimdlcjlkyp34d1ndkrfp2ajk38")))

(define-public crate-sleek-0.3.0 (c (n "sleek") (v "0.3.0") (d (list (d (n "clap") (r "^4.2.4") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "sqlformat") (r "^0.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "1ncaaj4ni9nr7j9z8nmc2s7fiz0dz4kphbxnbbx8frqkqgm2ahs5")))

