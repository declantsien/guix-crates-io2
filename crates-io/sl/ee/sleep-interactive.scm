(define-module (crates-io sl ee sleep-interactive) #:use-module (crates-io))

(define-public crate-sleep-interactive-0.1.0 (c (n "sleep-interactive") (v "0.1.0") (d (list (d (n "clap") (r "^4.2.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "indicatif") (r "^0.17.3") (d #t) (k 0)))) (h "0v8ngmaxh0x3fj3v1c7hhd1vkbih1d5xcgpvw93y0dbs9bksc7mh")))

(define-public crate-sleep-interactive-1.0.0 (c (n "sleep-interactive") (v "1.0.0") (d (list (d (n "clap") (r "^4.2.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "indicatif") (r "^0.17.3") (d #t) (k 0)))) (h "1020xfmjdkcv25nl7bmmph4wfdajcv2mbdxlx331ccdhbw3dspvf")))

(define-public crate-sleep-interactive-1.0.1 (c (n "sleep-interactive") (v "1.0.1") (d (list (d (n "clap") (r "^4.2.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "indicatif") (r "^0.17.3") (d #t) (k 0)))) (h "0h6pp5hfrk2cpj0sj1nida1j5mn5lf294kxzxzbkgc8v01qn39qz")))

