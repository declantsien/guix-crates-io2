(define-module (crates-io sl ee sleepycat) #:use-module (crates-io))

(define-public crate-sleepycat-0.1.0 (c (n "sleepycat") (v "0.1.0") (d (list (d (n "clap") (r "~2.33") (d #t) (k 0)))) (h "0sr38bb8cq3fs4fh0sbah9x9w1ravalni80a7iakfmigcbvqwfws")))

(define-public crate-sleepycat-0.1.1 (c (n "sleepycat") (v "0.1.1") (d (list (d (n "clap") (r "~2.33") (d #t) (k 0)))) (h "1fsc44435fmy2v24vbdzlqsbq5zas7myfbmxicxgybidl7dybmk4")))

(define-public crate-sleepycat-0.1.2 (c (n "sleepycat") (v "0.1.2") (d (list (d (n "clap") (r "~2.33") (d #t) (k 0)))) (h "1wk4n592v3d1gs0373kf96vphssjzl7mn4m89vxyc7r646xqlr2l")))

