(define-module (crates-io sl ee sleep-parser) #:use-module (crates-io))

(define-public crate-sleep-parser-0.0.1 (c (n "sleep-parser") (v "0.0.1") (d (list (d (n "nom") (r "^3.2.1") (d #t) (k 0)))) (h "0hmrk84mq0jqxgn7sdjz82z6f27w6fc1y4s949vv74vf17i3m0cs")))

(define-public crate-sleep-parser-0.0.5 (c (n "sleep-parser") (v "0.0.5") (d (list (d (n "byteorder") (r "^1.2.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)))) (h "17i96cdfx9ypcv605d6lyfrspdl0iasnbg9rzr6v4j8lg7ww8mxn")))

(define-public crate-sleep-parser-0.1.0 (c (n "sleep-parser") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.2.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)))) (h "1a1s3yix486171vjzg0gkz33nzpfdjwszycp42sipdkwb6h6vm5l")))

(define-public crate-sleep-parser-0.1.1 (c (n "sleep-parser") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.2.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)))) (h "0cbmwmb0yqw3fm9yayfii0pfwzdr4jp4dj46gfddkq5w3z1nh7r0")))

(define-public crate-sleep-parser-0.2.0 (c (n "sleep-parser") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.2.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)))) (h "1w0lsadamr7a363bqc10by0pi1mgbgzssz8ysqz1nwli7gba3s1h")))

(define-public crate-sleep-parser-0.3.1 (c (n "sleep-parser") (v "0.3.1") (d (list (d (n "byteorder") (r "^1.2.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)))) (h "08phsvfk8svspdswdzzaljq0smv4wgy1kzqp7spxjmp9488kbvrr")))

(define-public crate-sleep-parser-0.4.0 (c (n "sleep-parser") (v "0.4.0") (d (list (d (n "byteorder") (r "^1.2.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)))) (h "07nyyiil5ikmyi5baa0jyhwlv68n9715mg1j21i28vj8lf5arw09")))

(define-public crate-sleep-parser-0.5.0 (c (n "sleep-parser") (v "0.5.0") (d (list (d (n "byteorder") (r "^1.2.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)))) (h "1p8b35ajcyalqlp0j3svm8j9m9c9lmslzngfw7ngx5bdk6acz69z")))

(define-public crate-sleep-parser-0.6.0 (c (n "sleep-parser") (v "0.6.0") (d (list (d (n "byteorder") (r "^1.2.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)))) (h "154l9xg9g2hgixix529sxmxypzyd18p609ghx4f10mq40lrzfdgp")))

(define-public crate-sleep-parser-0.7.0 (c (n "sleep-parser") (v "0.7.0") (d (list (d (n "byteorder") (r "^1.2.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)))) (h "1gzk17wyh1mj0g08pjm2jlkhy8hdb4bs4csmd50j30vg2wx1wa6j")))

(define-public crate-sleep-parser-0.8.0 (c (n "sleep-parser") (v "0.8.0") (d (list (d (n "byteorder") (r "^1.2.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)))) (h "0x57dw0jmraz6xrpsir6v49f8q5ri4rc9fncbqjk9vic7gvl8xxp")))

