(define-module (crates-io sl ee sleek_csv) #:use-module (crates-io))

(define-public crate-sleek_csv-0.1.0 (c (n "sleek_csv") (v "0.1.0") (d (list (d (n "arrayvec") (r "^0.5.1") (d #t) (k 2)) (d (n "csv-core") (r "^0.1.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (d #t) (k 2)))) (h "0711wrd73mnc90ldqjpjiyf2zx541cc98a8jr9a7rmwq7laq39gp")))

