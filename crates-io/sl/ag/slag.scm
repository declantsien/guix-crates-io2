(define-module (crates-io sl ag slag) #:use-module (crates-io))

(define-public crate-slag-0.1.0 (c (n "slag") (v "0.1.0") (d (list (d (n "docopt") (r "^0.6") (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.7.0") (d #t) (k 0)))) (h "12prxxjah9mzl21aap47752lglqccwnsy75n1p3df9vzj6vp4cn9")))

(define-public crate-slag-0.1.1 (c (n "slag") (v "0.1.1") (d (list (d (n "docopt") (r "^0.6") (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.7.0") (d #t) (k 0)))) (h "0zgjhln3ymcnq88xmb6k9fibcl1cac2ldjfb3lmc7l2hdai277gm")))

