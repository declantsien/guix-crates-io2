(define-module (crates-io sl ic slice-copy) #:use-module (crates-io))

(define-public crate-slice-copy-0.1.0 (c (n "slice-copy") (v "0.1.0") (h "0bcsnbqvkk8a8rshnz82jy151w0nk37r80hbdf8k6prrzdrrky4q")))

(define-public crate-slice-copy-0.2.0 (c (n "slice-copy") (v "0.2.0") (h "0lb4hg2bdgkbmgs6y0z0yz2jbkr122mq7srmvybv564479w892cm") (f (quote (("nightly"))))))

(define-public crate-slice-copy-0.3.0 (c (n "slice-copy") (v "0.3.0") (h "0ynh20n6d1fddn7v1lri220dx4bi51fg5i2frwi2vjndc930zq7m")))

