(define-module (crates-io sl ic slice-by-8) #:use-module (crates-io))

(define-public crate-slice-by-8-0.1.0 (c (n "slice-by-8") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "100ij8fj3yps83n956lf11icnpw4v17kg1m1c28q4ga09spy53y8") (y #t)))

(define-public crate-slice-by-8-1.0.0 (c (n "slice-by-8") (v "1.0.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1q2z7zzf6a9v7x32lqm8pwvpf16v9yd1bllh4ak4km82xzvmhksl") (y #t)))

(define-public crate-slice-by-8-1.0.1 (c (n "slice-by-8") (v "1.0.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1jadf4rchr1gc3c8s7sixx42r53hsfyx2r7j9qg8nc1kibi1lyy5") (y #t)))

(define-public crate-slice-by-8-1.0.2 (c (n "slice-by-8") (v "1.0.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0v0giky3ajvw8pfp7pb7x7xnr9ajn3n78c4a1mfklzl0jsy2lv7a") (y #t)))

(define-public crate-slice-by-8-1.0.3 (c (n "slice-by-8") (v "1.0.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1dmsiq6knvk9q8mq1iy1a4yvvzzayg0q4pqr8idih0kndb67kmp2") (y #t)))

(define-public crate-slice-by-8-1.0.4 (c (n "slice-by-8") (v "1.0.4") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1k39kfd9v8px08rln11amh3f5z4kssgjw58ldsm6c5qfdskzjk3m") (y #t)))

(define-public crate-slice-by-8-1.0.5 (c (n "slice-by-8") (v "1.0.5") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1max9ax79cwgqpd5x9rhwy2bf06v0m2qn5yvqxhnn38a1glzb1lm") (y #t)))

(define-public crate-slice-by-8-1.0.6 (c (n "slice-by-8") (v "1.0.6") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "102dgjjpl87fx41llj21jr5cqxpjw661iic9jssp8vfkwg7nlkdg") (y #t)))

