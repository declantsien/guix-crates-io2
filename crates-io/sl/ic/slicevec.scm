(define-module (crates-io sl ic slicevec) #:use-module (crates-io))

(define-public crate-slicevec-0.1.0 (c (n "slicevec") (v "0.1.0") (h "1vx5wdj0sqr5xz53gbqgaxjz1lc6yga16bcf0yrlqvrn8wv7cwqg")))

(define-public crate-slicevec-0.1.1 (c (n "slicevec") (v "0.1.1") (h "1y1dji654k0z7wb1phv14x7qlwjffjvyz9h1yr0h7nh6zb1yqf4q")))

