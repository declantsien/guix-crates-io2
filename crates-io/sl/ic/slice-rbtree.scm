(define-module (crates-io sl ic slice-rbtree) #:use-module (crates-io))

(define-public crate-slice-rbtree-0.1.0-alpha (c (n "slice-rbtree") (v "0.1.0-alpha") (d (list (d (n "borsh") (r "^0.9.1") (d #t) (k 0)) (d (n "bytemuck") (r "^1.7.2") (f (quote ("derive" "min_const_generics"))) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "paste") (r "^1.0") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)))) (h "0hp80qav5sn52ch7lj2c7kqf6fzr0bf0dzwnakad2wd7px9sdydj")))

(define-public crate-slice-rbtree-0.1.0-alpha.1 (c (n "slice-rbtree") (v "0.1.0-alpha.1") (d (list (d (n "borsh") (r "^0.9.1") (f (quote ("const-generics"))) (k 0)) (d (n "bytemuck") (r "^1.7.2") (f (quote ("derive" "min_const_generics"))) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "paste") (r "^1.0") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)))) (h "0fy4bqb9672xmn8j6hh51kvpw1x16bx5n7qf3a2dncvmpcs51klc")))

(define-public crate-slice-rbtree-0.1.0 (c (n "slice-rbtree") (v "0.1.0") (d (list (d (n "arbitrary") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "borsh") (r "^0.9.1") (f (quote ("const-generics"))) (k 0)) (d (n "bytemuck") (r "^1.7.2") (f (quote ("derive" "min_const_generics"))) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "paste") (r "^1.0") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)))) (h "0i2jv4amqrx5pbzvsb01c9rmg9m7jpk3i63fp7zfwbw861q5nn39")))

