(define-module (crates-io sl ic sliced) #:use-module (crates-io))

(define-public crate-sliced-0.2.5 (c (n "sliced") (v "0.2.5") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (f (quote ("small_rng"))) (d #t) (k 2)))) (h "10s8nmd2yi3ajvplmmmkmvd55amx78klvjijr7gr5fns8cahldbm")))

(define-public crate-sliced-0.2.6 (c (n "sliced") (v "0.2.6") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (f (quote ("small_rng"))) (d #t) (k 2)))) (h "1am8709b3a14nfkhf2gzkxhkzc4w72m50x3hgzigk2mk4z2yijb8")))

(define-public crate-sliced-0.2.7 (c (n "sliced") (v "0.2.7") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (f (quote ("small_rng"))) (d #t) (k 2)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 2)))) (h "1bbq0ficix4avdhkkh4kw0cqv8iarha1h4f12adnqvihp8sd5fh5")))

(define-public crate-sliced-0.3.0 (c (n "sliced") (v "0.3.0") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (f (quote ("small_rng"))) (d #t) (k 2)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 2)))) (h "029sjckyajvkrf216mqyikf6gvcvbmrgv5dlvfj4kqvs9avv10y6")))

(define-public crate-sliced-0.3.2 (c (n "sliced") (v "0.3.2") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (f (quote ("small_rng"))) (d #t) (k 2)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 2)))) (h "0z3cm0b82qhzcvyc19nii6g2vfz87crq68bpk1qswwnnr9jkab10")))

(define-public crate-sliced-0.3.3 (c (n "sliced") (v "0.3.3") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (f (quote ("small_rng"))) (d #t) (k 2)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 2)))) (h "061hrf9mydicmn0my5wp59lfa0qgw1yi39ws9vzz2mdv6gj435wg")))

