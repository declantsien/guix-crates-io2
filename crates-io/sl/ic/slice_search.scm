(define-module (crates-io sl ic slice_search) #:use-module (crates-io))

(define-public crate-slice_search-0.1.0 (c (n "slice_search") (v "0.1.0") (h "1g9wmdqb9ivn1zcwmbpqrvw1k110xl6n3i1fn94hhjfr8rwsr91v")))

(define-public crate-slice_search-0.1.1 (c (n "slice_search") (v "0.1.1") (h "0dh4sfqrp4z9zg3is0hhahprzx3df167qlwdjkdxywqfx0gdkclx")))

(define-public crate-slice_search-0.1.2 (c (n "slice_search") (v "0.1.2") (h "0dbx1fkyjyw8a0rdm730a4230frd6yr8kzz2g0bxp3q6acv49p82")))

