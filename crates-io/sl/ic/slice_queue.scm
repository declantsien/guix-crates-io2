(define-module (crates-io sl ic slice_queue) #:use-module (crates-io))

(define-public crate-slice_queue-0.1.0 (c (n "slice_queue") (v "0.1.0") (h "0ih6ifc273cmhfw2sq87p3j8k96qb55n6g7h1pds0iz4aq3ylkfn")))

(define-public crate-slice_queue-0.1.1 (c (n "slice_queue") (v "0.1.1") (h "1aia7mhly5z6hv5h7217qmaynjrajsvg6vvzxqpifakrwhjbsp0a")))

(define-public crate-slice_queue-0.1.2 (c (n "slice_queue") (v "0.1.2") (h "1ckqdijsmpc5yrbh4lrjas3khzw7j3cyz2xqyak0jysyz6mrknxp")))

(define-public crate-slice_queue-0.1.3 (c (n "slice_queue") (v "0.1.3") (h "00m0m4dabwawcmxk8pmpi107nipp8blq5l4kn2sp83fdw2192159")))

(define-public crate-slice_queue-0.1.4 (c (n "slice_queue") (v "0.1.4") (h "06vf57zv74raxfz0l58113g0v4k4fy3n9crix2xff5v59g37kr31") (f (quote (("fast_unsafe_code") ("default"))))))

(define-public crate-slice_queue-0.1.5 (c (n "slice_queue") (v "0.1.5") (h "01p84n56m6rxf73zich8m3rjn2a4l59vig0l40g7jd7ai6wnb8aj") (f (quote (("fast_unsafe_code") ("default"))))))

(define-public crate-slice_queue-0.1.6 (c (n "slice_queue") (v "0.1.6") (h "1wjrycrbiaxp4yr8b7ilynmzk3amnks8acrk1azigccwwr2rq6pa") (f (quote (("fast_unsafe_code") ("deref") ("default" "deref"))))))

(define-public crate-slice_queue-0.1.7 (c (n "slice_queue") (v "0.1.7") (h "1s1wz79wpn60405xiz2n8zhif3wn2g35jl0ync80h65gg5aqi411") (f (quote (("fast_unsafe_code") ("deref") ("default" "deref"))))))

(define-public crate-slice_queue-0.1.8 (c (n "slice_queue") (v "0.1.8") (h "1hi6f73svx7xyy6xpbighclfnr75i655dvq9rkld1ziwdzjzd8b9") (f (quote (("unsafe_fast_code") ("deref") ("default" "deref" "unsafe_fast_code"))))))

(define-public crate-slice_queue-0.1.9 (c (n "slice_queue") (v "0.1.9") (h "02w8lw74j8nfrh3f3c7bwjkmi1lx59am4lshf9v4lpy9k4502zns") (f (quote (("unsafe_fast_code") ("deref") ("default" "deref" "unsafe_fast_code"))))))

(define-public crate-slice_queue-0.1.10 (c (n "slice_queue") (v "0.1.10") (h "10d6b0hmiw0biqhpvryn01ry1mw53949rvaqc30lxj5yscd65pwl") (f (quote (("unsafe_fast_code") ("deref") ("default" "deref" "unsafe_fast_code"))))))

(define-public crate-slice_queue-0.2.0 (c (n "slice_queue") (v "0.2.0") (h "1sggsvb9zzjgabjakzsaiaw8jx1jq3ch088b463lh5bgcfwp0172") (f (quote (("unsafe_fast_code") ("deref") ("default" "deref" "unsafe_fast_code"))))))

(define-public crate-slice_queue-0.2.1 (c (n "slice_queue") (v "0.2.1") (h "0b9v6yyxkgd55sc527jxxpiq0gyn9x19iazqm14xv9zbhnlrj8qr") (f (quote (("unsafe_fast_code") ("deref") ("default" "deref" "unsafe_fast_code"))))))

(define-public crate-slice_queue-0.2.2 (c (n "slice_queue") (v "0.2.2") (h "1mzw882n6zbb87anvr7gnxi6kcdwjfmha5v0n4cv24kmsxf4w408") (f (quote (("unsafe_fast_code") ("deref") ("default" "deref" "unsafe_fast_code"))))))

(define-public crate-slice_queue-0.3.0 (c (n "slice_queue") (v "0.3.0") (h "1jfvi0w28j0xqmi0vwp6rwlrf4rpw8pg9pqidib80lrs1gvbdxgl") (f (quote (("unsafe_fast_code") ("deref") ("default" "deref" "unsafe_fast_code"))))))

(define-public crate-slice_queue-0.3.1 (c (n "slice_queue") (v "0.3.1") (h "1qp8jl494n8rkrqpzr9034jpdqvfp2pv0bdcr8x17nq7sqqa7870") (f (quote (("unsafe_fast_code") ("deref") ("default" "deref" "unsafe_fast_code"))))))

(define-public crate-slice_queue-0.3.2 (c (n "slice_queue") (v "0.3.2") (h "1alh8xqzjs6jm94xx8wr7kab96jxq3yzwki7ip8d421gp2yj3gc6") (f (quote (("unsafe_fast_code") ("deref") ("default" "deref" "unsafe_fast_code"))))))

