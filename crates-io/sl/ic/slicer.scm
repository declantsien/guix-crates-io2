(define-module (crates-io sl ic slicer) #:use-module (crates-io))

(define-public crate-slicer-0.1.0 (c (n "slicer") (v "0.1.0") (h "1cl7gfizzd8cdp1sk72di808g4cf6q7q8d5z0abm8qpjnhpani2j")))

(define-public crate-slicer-0.1.1 (c (n "slicer") (v "0.1.1") (h "0280gb22iaivkrvp0rdqyp3dij1km4i4aylfpmlwj7sk05plzxwr")))

