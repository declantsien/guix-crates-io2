(define-module (crates-io sl ic slicejson) #:use-module (crates-io))

(define-public crate-slicejson-0.1.0 (c (n "slicejson") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "=1.10.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.139") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)) (d (n "simd-json") (r "^0.13.6") (d #t) (k 0)))) (h "0hc53b0kkg2iknp333cwz9w3vr2k8fx7rivrydx2kd5ybc5xr48a")))

(define-public crate-slicejson-0.1.1 (c (n "slicejson") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "=1.10.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.139") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)) (d (n "simd-json") (r "^0.13.6") (d #t) (k 0)))) (h "1n2d6306afr1v4ywg8xscbb2j99spj107mzf574j5m3d1apbsrb4")))

(define-public crate-slicejson-0.1.2 (c (n "slicejson") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "=1.10.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.139") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)) (d (n "simd-json") (r "^0.13.6") (d #t) (k 0)))) (h "04md9cngg36ccaqcy3zy15381w5j4sbcg92z6v31walnqbinvxn3")))

