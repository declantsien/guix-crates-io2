(define-module (crates-io sl ic slicefields) #:use-module (crates-io))

(define-public crate-slicefields-1.0.0 (c (n "slicefields") (v "1.0.0") (d (list (d (n "prettyplease") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "18aiskjrzrnj3dj6w2bkjdifhp3a9ly9cxjx1pavdjb6fhvv0gdf") (s 2) (e (quote (("debug" "dep:syn" "dep:prettyplease"))))))

(define-public crate-slicefields-1.0.1 (c (n "slicefields") (v "1.0.1") (d (list (d (n "prettyplease") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "1dr7r3qvgyin6fhdp2glvmbws50vjy1d410sxqn5bxf7cdrs3rj0") (s 2) (e (quote (("debug" "dep:syn" "dep:prettyplease"))))))

(define-public crate-slicefields-1.0.2 (c (n "slicefields") (v "1.0.2") (d (list (d (n "prettyplease") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "0kzqhasvxpa1fddac9cbs3lv18j63dc5nsb97kgssa9rhbr45dlx") (s 2) (e (quote (("debug" "dep:syn" "dep:prettyplease"))))))

(define-public crate-slicefields-1.0.3 (c (n "slicefields") (v "1.0.3") (d (list (d (n "prettyplease") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "13n5lhplz77b7zwg7aakfjxvpqbiv9dzr0pgcrhs9v6ahyyrbvw6") (f (quote (("unstable")))) (s 2) (e (quote (("debug" "dep:syn" "dep:prettyplease"))))))

(define-public crate-slicefields-1.0.4 (c (n "slicefields") (v "1.0.4") (d (list (d (n "prettyplease") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "1lcmb8kymy47w2cl4izf0r3mnypp1rxpwnr9xbrcbalm2xxvzsj5") (f (quote (("unstable")))) (s 2) (e (quote (("debug" "dep:syn" "dep:prettyplease"))))))

