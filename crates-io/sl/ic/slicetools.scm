(define-module (crates-io sl ic slicetools) #:use-module (crates-io))

(define-public crate-slicetools-0.1.0 (c (n "slicetools") (v "0.1.0") (h "1d66m89ah9wpwsaqnkmqnxdgn79l3rcjjzxb2dvliii57x6rdl3k")))

(define-public crate-slicetools-0.1.1 (c (n "slicetools") (v "0.1.1") (h "1bgp7chzc291s165mhn8cgm5c238580gprzh5xnr8zs6v7b9vfb2")))

(define-public crate-slicetools-0.2.0 (c (n "slicetools") (v "0.2.0") (h "1509gx63adikshwxbr99f9b4l87kkgy3ps5c6h826ili0pyfx59p")))

(define-public crate-slicetools-0.3.0 (c (n "slicetools") (v "0.3.0") (h "0l63mj3k5ghnzh7i2illk3c0q61swhf4mppnymxr6g6jbdyr013d")))

