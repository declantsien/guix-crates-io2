(define-module (crates-io sl ic sliceread) #:use-module (crates-io))

(define-public crate-sliceread-0.1.0 (c (n "sliceread") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3.3") (d #t) (k 2)))) (h "1arwxgwfpr6mj6n9qzvc0q35hcl4mf68l8p4gizzf19a5cqc2nnj") (y #t)))

(define-public crate-sliceread-0.1.1 (c (n "sliceread") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3.3") (d #t) (k 2)))) (h "1rdlhsn389db770djpbc34nh4xdq6vwj8qh0pa45a081sy3c84s6") (y #t)))

(define-public crate-sliceread-0.1.2 (c (n "sliceread") (v "0.1.2") (d (list (d (n "criterion") (r "^0.3.3") (d #t) (k 2)))) (h "1zn55nb0lc6587yak52k9gckyq4q8apck5nlhsv7nx5nabhnwvhq") (y #t)))

(define-public crate-sliceread-0.1.3 (c (n "sliceread") (v "0.1.3") (d (list (d (n "criterion") (r "^0.3.3") (d #t) (k 2)))) (h "1yvyd4ajb0bpbbywymr6a46dl4v94h8svkjig51g3k63hj0vfx8w") (y #t)))

