(define-module (crates-io sl ic slice-of-array) #:use-module (crates-io))

(define-public crate-slice-of-array-0.1.0 (c (n "slice-of-array") (v "0.1.0") (d (list (d (n "version-sync") (r "^0.3") (d #t) (k 2)))) (h "0ikcnjsjkqgh7vhmjr63j6q3a7cgg17rhd9ms5zrygs6cvkk1yk7")))

(define-public crate-slice-of-array-0.1.1 (c (n "slice-of-array") (v "0.1.1") (d (list (d (n "version-sync") (r "^0.3") (d #t) (k 2)))) (h "09arbg8701pxfas4wv50b8ymajbaj9w42lylbncnjs31vzr3qp2h")))

(define-public crate-slice-of-array-0.2.0 (c (n "slice-of-array") (v "0.2.0") (d (list (d (n "version-sync") (r "^0.3") (d #t) (k 2)))) (h "1rcy149jl634iv893wbr9fx9ahl0q9l978m1gvwd433rnlanmr7x")))

(define-public crate-slice-of-array-0.2.1 (c (n "slice-of-array") (v "0.2.1") (d (list (d (n "version-sync") (r "^0.3") (d #t) (k 2)))) (h "0rv3j2hzqkyyfq6yai95b2c680d1w95m16sji13p46h5n4yfkzwg")))

(define-public crate-slice-of-array-0.3.0 (c (n "slice-of-array") (v "0.3.0") (d (list (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "1m9gpk35snmlzvdzsf304s1qmxjjcsrb7rigqdhpb75q0pqdhnna")))

(define-public crate-slice-of-array-0.3.1 (c (n "slice-of-array") (v "0.3.1") (d (list (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "0qqqnyihm8gi9a2dr9xralhyrmrbdf8hiq97czrljj3b4sskaxmg") (f (quote (("std"))))))

(define-public crate-slice-of-array-0.3.2 (c (n "slice-of-array") (v "0.3.2") (d (list (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "116f8m4h89awryqhfvxl4gfpkzybqdl2k20wnbdb0k6bk2xj1wd4") (f (quote (("std"))))))

