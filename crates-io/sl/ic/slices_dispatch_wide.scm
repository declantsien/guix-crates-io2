(define-module (crates-io sl ic slices_dispatch_wide) #:use-module (crates-io))

(define-public crate-slices_dispatch_wide-0.1.0 (c (n "slices_dispatch_wide") (v "0.1.0") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "wide") (r "^0.7.4") (d #t) (k 0)))) (h "0rm5dfzw6nphm4ya83xmvkix96zps3b4jzlkix9gkzz5nfqh8mpw")))

(define-public crate-slices_dispatch_wide-0.1.1 (c (n "slices_dispatch_wide") (v "0.1.1") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "wide") (r "^0.7.4") (d #t) (k 0)))) (h "11kgfffrypnsdkhc8jb8wnl89zv36ds96lqdm6mrf56ml0rqa6fs")))

