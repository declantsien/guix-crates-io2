(define-module (crates-io sl ic slicestring) #:use-module (crates-io))

(define-public crate-slicestring-0.1.0 (c (n "slicestring") (v "0.1.0") (h "0i5nb53d7lfm6vsc87g5pg28idvlj7dcnig0ysyvqxlqjpqrsqd6")))

(define-public crate-slicestring-0.1.1 (c (n "slicestring") (v "0.1.1") (h "1qrcca5dz2bg36hj3n5v3qj42z52865gg84110907rls9xb82kls") (y #t)))

(define-public crate-slicestring-0.1.2 (c (n "slicestring") (v "0.1.2") (h "19f07ff3758pyjfraxggfypciqmczahsc11bqp4s054f8v8b305s") (y #t)))

(define-public crate-slicestring-0.1.3 (c (n "slicestring") (v "0.1.3") (h "106i4c9fmfm5z3v2mi7yml6hkkblc2d0mji024m7rb3jasf9fsv3")))

(define-public crate-slicestring-0.2.0 (c (n "slicestring") (v "0.2.0") (h "1p1s79k72d67qlw97sfkr6fq9mxcjdfnpg7y1b10b7chnh8mvm7g")))

(define-public crate-slicestring-0.2.1 (c (n "slicestring") (v "0.2.1") (h "1x32p79382pbdprrc7k9npp7dv7waqin2w1sjyql023pqvplix8f")))

(define-public crate-slicestring-0.3.0 (c (n "slicestring") (v "0.3.0") (h "1nrd82c6wjbhj3kcsanfwvalaab2cnbkl61a3kxzk5x7a2pf3j8v") (y #t)))

(define-public crate-slicestring-0.3.1 (c (n "slicestring") (v "0.3.1") (h "0v62xdkilw3gh27qgh5whxax8swjfv8viqqzi1bf1alv6sa85gdc") (y #t)))

(define-public crate-slicestring-0.3.2 (c (n "slicestring") (v "0.3.2") (h "0xfzv796m8i98psccxzmp85ix2xz6fk5mg40zyrjbnklxva0lyys")))

(define-public crate-slicestring-0.3.3 (c (n "slicestring") (v "0.3.3") (h "1d27vlccl0b82hj83mqi67ig13vjfdmpw92f176n49mbdgzls8c4")))

