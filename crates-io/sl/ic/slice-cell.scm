(define-module (crates-io sl ic slice-cell) #:use-module (crates-io))

(define-public crate-slice-cell-0.0.1 (c (n "slice-cell") (v "0.0.1") (h "1pmqjnsh7rj5y97ds5xq3lkzdm5rc42x4fv0f7vxplqax3y0kw54") (f (quote (("std" "alloc") ("rc" "alloc") ("default" "std" "assume_cell_layout" "rc") ("assume_cell_layout") ("alloc"))))))

(define-public crate-slice-cell-0.0.2 (c (n "slice-cell") (v "0.0.2") (h "0fyzn2pgvjdcj5jv9a55bfj1vvwm62kricv0by393s0vvf2g665l") (f (quote (("std" "alloc") ("rc" "alloc") ("default" "std" "assume_cell_layout" "rc") ("assume_cell_layout") ("alloc"))))))

(define-public crate-slice-cell-0.0.3 (c (n "slice-cell") (v "0.0.3") (h "0a0xqywkncs1iqcwfd4g5pmvk3dd1pgzzq2rw2baca9ksk55k9xk") (f (quote (("std" "alloc") ("rc" "alloc") ("default" "std" "assume_cell_layout" "rc") ("assume_cell_layout") ("alloc"))))))

(define-public crate-slice-cell-0.0.4 (c (n "slice-cell") (v "0.0.4") (d (list (d (n "tokio") (r "^1.24.1") (o #t) (k 0)))) (h "0z5vjfj31x13khqd92xsslm3yjcimgrrknwyvfhgva8gx8cgfzhz") (f (quote (("tokio_assumptions" "tokio") ("std" "alloc") ("rc" "alloc") ("default" "std" "assume_cell_layout" "rc") ("assume_cell_layout") ("alloc")))) (s 2) (e (quote (("tokio" "dep:tokio" "std"))))))

(define-public crate-slice-cell-0.0.5 (c (n "slice-cell") (v "0.0.5") (d (list (d (n "tokio") (r "^1.24.1") (o #t) (k 0)))) (h "1156ryaskrhjp721051mz4g2vwhx2bs41jqjizg2gx7mxi2p1km5") (f (quote (("tokio_assumptions" "tokio") ("std" "alloc") ("rc" "alloc") ("full" "std" "rc" "tokio") ("default" "std" "rc") ("alloc")))) (s 2) (e (quote (("tokio" "dep:tokio" "std"))))))

