(define-module (crates-io sl ic slice-utils) #:use-module (crates-io))

(define-public crate-slice-utils-1.0.0 (c (n "slice-utils") (v "1.0.0") (h "0cf1i9mn4d8vw5299r3v4jmdhj7w1g1ax93bfand3152g22n7rqi") (f (quote (("std") ("default"))))))

(define-public crate-slice-utils-1.0.1 (c (n "slice-utils") (v "1.0.1") (h "0q5gx8dn4x9zglb0fasfg5j8g3hinwij7c1p5rv6w2f7ffcq5nxr") (f (quote (("std") ("default"))))))

(define-public crate-slice-utils-1.0.2 (c (n "slice-utils") (v "1.0.2") (h "1a9diyxryfaxw7zl8z0if80nxgx7ifdry7sczws2hwgah6wmx163") (f (quote (("std") ("default"))))))

(define-public crate-slice-utils-2.0.0 (c (n "slice-utils") (v "2.0.0") (d (list (d (n "paste") (r "^1.0.14") (d #t) (k 0)))) (h "0y2i384smrh9h0v56r8n3mxdlj10yhf38fd7bgba95c8zwp91y8m") (f (quote (("std") ("default"))))))

(define-public crate-slice-utils-2.0.1 (c (n "slice-utils") (v "2.0.1") (d (list (d (n "paste") (r "^1.0.14") (d #t) (k 0)))) (h "1gwldrfspl8ss96xg1mlygx6080j41ldjb5dk2v5nziliz3lhw4c") (f (quote (("std") ("default"))))))

(define-public crate-slice-utils-2.0.2 (c (n "slice-utils") (v "2.0.2") (d (list (d (n "paste") (r "^1.0.14") (d #t) (k 0)))) (h "1mi2b384cxcbmyrf4c6f1mza1w0ldqbrayq6c530fmraa69yf25z") (f (quote (("std") ("default"))))))

(define-public crate-slice-utils-2.0.3 (c (n "slice-utils") (v "2.0.3") (d (list (d (n "paste") (r "^1.0.14") (d #t) (k 0)))) (h "0p82ia4g60d69ijpjbv4jy5867hvi0r5g01vjff5dihkqwfp9m88") (f (quote (("std") ("default"))))))

(define-public crate-slice-utils-2.1.0 (c (n "slice-utils") (v "2.1.0") (d (list (d (n "paste") (r "^1.0.14") (d #t) (k 0)))) (h "1dr8q3lia6zs2nzc77slkjfk47fmkblm9s626v7zn0zdi2894n8n") (f (quote (("std") ("default"))))))

