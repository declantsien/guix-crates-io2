(define-module (crates-io sl ic slice_adapter) #:use-module (crates-io))

(define-public crate-slice_adapter-0.1.0 (c (n "slice_adapter") (v "0.1.0") (h "0n336fcgvhwxxqczqxip8zlzjld38wbciswyjbjr4pbwbkpgdcql") (y #t)))

(define-public crate-slice_adapter-0.1.1 (c (n "slice_adapter") (v "0.1.1") (h "1iyg8kwdpd5lq45148fi519p3jammm3q94xl3bmyrwripqymkb88")))

