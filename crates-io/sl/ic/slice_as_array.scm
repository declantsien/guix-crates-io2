(define-module (crates-io sl ic slice_as_array) #:use-module (crates-io))

(define-public crate-slice_as_array-1.0.0 (c (n "slice_as_array") (v "1.0.0") (d (list (d (n "compiletest_rs") (r "^0.0.11") (o #t) (d #t) (k 0)))) (h "0p5pc5zyy255q4vkhxxhvx1p698cr5rrmywjcf7nicf947jrdqvp") (f (quote (("use_std") ("default" "use_std") ("compiletest" "compiletest_rs"))))))

(define-public crate-slice_as_array-1.1.0 (c (n "slice_as_array") (v "1.1.0") (d (list (d (n "compiletest_rs") (r "^0.0.11") (o #t) (d #t) (k 0)))) (h "1hz4py2rknxbpdpn7iaj59br58ml91ydjb6wjnmvbvfxb7p67jb4") (f (quote (("use_std") ("default" "use_std") ("compiletest" "compiletest_rs"))))))

