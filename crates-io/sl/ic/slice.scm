(define-module (crates-io sl ic slice) #:use-module (crates-io))

(define-public crate-slice-0.0.1 (c (n "slice") (v "0.0.1") (h "1vhirqxg34qjmjd9pxjqcmshgfhcz7qsvbb1msmld7kcjrlrn0z5")))

(define-public crate-slice-0.0.2 (c (n "slice") (v "0.0.2") (h "1sgadmwjqmswiwv9bmgx8mhssl0zs9rhxlqjq57n82k81hcr4zm0")))

(define-public crate-slice-0.0.3 (c (n "slice") (v "0.0.3") (h "0ii3z451h9swdnbwjsvxfdqlirwx7a5ddplvrs29dx5bnc2q6p4q")))

(define-public crate-slice-0.0.4 (c (n "slice") (v "0.0.4") (h "0dzrck37gk6bzb7ah4crdcnrzhyy20zzc2bybd8mnxd02561g3d4")))

