(define-module (crates-io sl ic slick2) #:use-module (crates-io))

(define-public crate-slick2-0.2.0 (c (n "slick2") (v "0.2.0") (h "10hvrcd0g40zjxpls24mb40ip35iffygqcvfk7cvibx60lcc1vpr")))

(define-public crate-slick2-0.1.0 (c (n "slick2") (v "0.1.0") (d (list (d (n "blake3") (r "^0.3.6") (d #t) (k 0)) (d (n "numext-fixed-uint") (r "^0.1.4") (d #t) (k 0)))) (h "056hilri7b07fgddzw35qsdjzpzpzfjdj08z9xp5vaj9hmwpbk63")))

