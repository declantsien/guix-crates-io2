(define-module (crates-io sl ic slicedvec) #:use-module (crates-io))

(define-public crate-slicedvec-0.1.0 (c (n "slicedvec") (v "0.1.0") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (f (quote ("small_rng"))) (d #t) (k 2)))) (h "1ilk9pzsma3434s2f95vdsj04660q063i6hdgda41l2i0bpvqlc3")))

(define-public crate-slicedvec-0.1.1 (c (n "slicedvec") (v "0.1.1") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (f (quote ("small_rng"))) (d #t) (k 2)))) (h "15l1czcjv2x6mj5bb80ifgbw20h49fjqy6k0lwzlrfc4wg5csi3h")))

(define-public crate-slicedvec-0.2.0 (c (n "slicedvec") (v "0.2.0") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (f (quote ("small_rng"))) (d #t) (k 2)))) (h "0fcw4lypw8537v96nzp4ddisg1lyy5dwhz05bnwg9ryc9jjr18dq")))

(define-public crate-slicedvec-0.2.1 (c (n "slicedvec") (v "0.2.1") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (f (quote ("small_rng"))) (d #t) (k 2)))) (h "0wzfal9r72r6flc6vk37axdvsmh55sc9hkwgyv4zmd2gyj4bpjqv")))

(define-public crate-slicedvec-0.2.2 (c (n "slicedvec") (v "0.2.2") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (f (quote ("small_rng"))) (d #t) (k 2)))) (h "041pa5s3ygmh42c5yc0fxgrvgw78wkbc25ka1420ac87s98iwav2")))

(define-public crate-slicedvec-0.2.3 (c (n "slicedvec") (v "0.2.3") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (f (quote ("small_rng"))) (d #t) (k 2)))) (h "0pc6p3w222hkg4205zmrjq3hiynqbxr7djdpwwkzcv4xn3vj3ydq")))

