(define-module (crates-io sl ic slice_ops) #:use-module (crates-io))

(define-public crate-slice_ops-0.1.0 (c (n "slice_ops") (v "0.1.0") (d (list (d (n "moddef") (r "^0.2.2") (d #t) (k 0)) (d (n "slice_trait") (r "^0.1.0") (d #t) (k 0)))) (h "116aakdjynpp2nf8b4dvgk2lp7g3q8c0h9m3zpc4vzl9czsx7x55")))

(define-public crate-slice_ops-0.1.1 (c (n "slice_ops") (v "0.1.1") (d (list (d (n "moddef") (r "^0.2.2") (d #t) (k 0)) (d (n "slice_trait") (r "^0.1.0") (d #t) (k 0)))) (h "0znsj0shprqfkghsgqiva1cqd34pqndm065glczp5k8dh62bz61m")))

(define-public crate-slice_ops-0.1.2 (c (n "slice_ops") (v "0.1.2") (d (list (d (n "moddef") (r "^0.2.2") (d #t) (k 0)) (d (n "slice_trait") (r "^0.1.0") (d #t) (k 0)))) (h "1llb9r6hy2b88236m8mn6svj7f7m6b3v0qj16jid6z54fd5g02j3")))

(define-public crate-slice_ops-0.1.3 (c (n "slice_ops") (v "0.1.3") (d (list (d (n "moddef") (r "^0.2.2") (d #t) (k 0)) (d (n "slice_trait") (r "^0.1.0") (d #t) (k 0)))) (h "0vz1q1p1jgx3qdpqxw2wylfg754skv2bvm08s88i2mvy0gvi1nif") (f (quote (("std") ("default" "std"))))))

(define-public crate-slice_ops-0.1.4 (c (n "slice_ops") (v "0.1.4") (d (list (d (n "moddef") (r "^0.2.2") (d #t) (k 0)) (d (n "slice_trait") (r "^0.1.0") (d #t) (k 0)))) (h "1yg8fh94br1v696mwvga5w1870b8hmnpl09zrrj9qdx5c0x7s15l") (f (quote (("std") ("default" "std"))))))

(define-public crate-slice_ops-0.1.5 (c (n "slice_ops") (v "0.1.5") (d (list (d (n "moddef") (r "^0.2.2") (d #t) (k 0)) (d (n "slice_trait") (r "^0.1.0") (d #t) (k 0)))) (h "1s8p5wy2xv57kn8p5xxn3mbrzdwh0h9alw0p4w56cbcdab31ffs3") (f (quote (("std") ("default" "std"))))))

(define-public crate-slice_ops-0.1.6 (c (n "slice_ops") (v "0.1.6") (d (list (d (n "moddef") (r "^0.2.2") (d #t) (k 0)) (d (n "slice_trait") (r "^0.1.0") (d #t) (k 0)))) (h "1xm72d7jlxj42gqylg81zwv479h3m13jnsgv2zhwya17apfy8cls") (f (quote (("std") ("default" "std"))))))

(define-public crate-slice_ops-0.1.7 (c (n "slice_ops") (v "0.1.7") (d (list (d (n "moddef") (r "^0.2.2") (d #t) (k 0)) (d (n "slice_trait") (r "^0.1.0") (d #t) (k 0)))) (h "072hfyq3paqdq43n0yz2xkg5sw91v5aaq0mgcap57w3rrwrycqfb") (f (quote (("std") ("default" "std"))))))

(define-public crate-slice_ops-0.1.8 (c (n "slice_ops") (v "0.1.8") (d (list (d (n "moddef") (r "^0.2.2") (d #t) (k 0)) (d (n "slice_trait") (r "^0.1.0") (d #t) (k 0)))) (h "0rikl70c1kvxk1gmycp0mfhpz1n4r1b3kbdgvn0xpg4kcl502f41") (f (quote (("std") ("default" "std"))))))

(define-public crate-slice_ops-0.1.9 (c (n "slice_ops") (v "0.1.9") (d (list (d (n "moddef") (r "^0.2.2") (d #t) (k 0)) (d (n "slice_trait") (r "^0.1.0") (d #t) (k 0)))) (h "1dvw8b24qb0q32kggljvc90pgyqi6p8q2anjxbkl14735laf0xmg") (f (quote (("std") ("default" "std"))))))

(define-public crate-slice_ops-0.1.10 (c (n "slice_ops") (v "0.1.10") (d (list (d (n "moddef") (r "^0.2.2") (d #t) (k 0)) (d (n "slice_trait") (r "^0.1.0") (d #t) (k 0)))) (h "0q0z8iy245bgf23mvggpw14d89zvawdzm8znijx5rsf4xbzmx4x4") (f (quote (("std") ("default" "std"))))))

