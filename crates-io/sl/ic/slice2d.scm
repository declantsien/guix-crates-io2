(define-module (crates-io sl ic slice2d) #:use-module (crates-io))

(define-public crate-slice2d-0.1.0 (c (n "slice2d") (v "0.1.0") (h "12s7p5w4bzngsf5hajs6qidh0akgk0hdim4dws64v11a2z4l77vl")))

(define-public crate-slice2d-0.1.1 (c (n "slice2d") (v "0.1.1") (h "0dyak1cqjd7av51pqy287r7sdvg8dmvgdxsbps6w2fiph7rcwcvp")))

