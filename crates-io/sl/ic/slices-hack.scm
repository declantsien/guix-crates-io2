(define-module (crates-io sl ic slices-hack) #:use-module (crates-io))

(define-public crate-slices-hack-0.1.0 (c (n "slices-hack") (v "0.1.0") (d (list (d (n "faster-hex") (r "~0.3") (d #t) (k 0)) (d (n "proc-macro-hack") (r "~0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "~0.4") (d #t) (k 0)) (d (n "quote") (r "~0.6") (d #t) (k 0)) (d (n "syn") (r "~0.15") (d #t) (k 0)))) (h "0x01z3p5qns2r7i3x05bqnqw51zgl4zna1b1iyd3idq5j72lq3g4")))

(define-public crate-slices-hack-0.1.1 (c (n "slices-hack") (v "0.1.1") (d (list (d (n "faster-hex") (r "~0.4") (d #t) (k 0)) (d (n "proc-macro-hack") (r "~0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "~1.0") (d #t) (k 0)) (d (n "quote") (r "~1.0") (d #t) (k 0)) (d (n "syn") (r "~1.0") (d #t) (k 0)))) (h "07crddhx38qjwcdn7imwl3xs82cxk8hccx6mzx7wiwxas8xwy2ab")))

