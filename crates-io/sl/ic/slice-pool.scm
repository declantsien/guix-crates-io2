(define-module (crates-io sl ic slice-pool) #:use-module (crates-io))

(define-public crate-slice-pool-0.1.0 (c (n "slice-pool") (v "0.1.0") (h "1apfag72kx7sd45yjjkpj4mk7zb3jlqj3nfg27snnc5av8c2irpg")))

(define-public crate-slice-pool-0.2.0 (c (n "slice-pool") (v "0.2.0") (h "0ajp9h9b3lmk2mqx59v453p5ayifal6cw4d87348mkp9k980aa4d")))

(define-public crate-slice-pool-0.3.0 (c (n "slice-pool") (v "0.3.0") (h "0arxq4ssilwbd0mhzmkmh1y1frlyvvpgbqjmfd962fy9hrk1s5p3")))

(define-public crate-slice-pool-0.3.1 (c (n "slice-pool") (v "0.3.1") (h "1hq5xvragihjpx9z7hrlim12vz6qsib55n77d854rkr01rjilpm9")))

(define-public crate-slice-pool-0.3.2 (c (n "slice-pool") (v "0.3.2") (h "18awq9accvbcn7n5kq8ac2dynzdan2r3x3c3xs6az6gybngxgb6i")))

(define-public crate-slice-pool-0.3.3 (c (n "slice-pool") (v "0.3.3") (h "05lm36x8v9ky2a1920zb91x1f4k02cz17pf7mai5wqkc27sr79dr")))

(define-public crate-slice-pool-0.3.4 (c (n "slice-pool") (v "0.3.4") (h "1ajfp0nx6mygsfz5q7a0637l003pnics3hsc7w00k3skjlx9il90")))

(define-public crate-slice-pool-0.4.0 (c (n "slice-pool") (v "0.4.0") (h "0nfc3grabc9ckl2x3irmqn5zy1pzlim504m4n1z982n2ds0ab58x")))

(define-public crate-slice-pool-0.4.1 (c (n "slice-pool") (v "0.4.1") (h "0yifbvph8xmaqf88rjgwq88cj5sz9vmbvja2z0v82fmxy7jwcgvk")))

