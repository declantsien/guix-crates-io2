(define-module (crates-io sl ic slice-group-by) #:use-module (crates-io))

(define-public crate-slice-group-by-0.1.0 (c (n "slice-group-by") (v "0.1.0") (d (list (d (n "rand") (r "^0.5") (d #t) (k 2)) (d (n "sdset") (r "^0.3") (d #t) (k 0)))) (h "0m1xgbfnrlc42s8lb6sx1ghchg7l5688dfhnfzszdq0q4am3xlwi") (f (quote (("nightly"))))))

(define-public crate-slice-group-by-0.2.0 (c (n "slice-group-by") (v "0.2.0") (d (list (d (n "rand") (r "^0.5") (d #t) (k 2)) (d (n "sdset") (r "^0.3") (d #t) (k 0)))) (h "0dika9mgdzin81vrv2l828bkxdzc1kq712kfkj6pfs7mlp7k2ksd") (f (quote (("nightly"))))))

(define-public crate-slice-group-by-0.2.1 (c (n "slice-group-by") (v "0.2.1") (d (list (d (n "rand") (r "^0.5") (d #t) (k 2)) (d (n "sdset") (r "^0.3") (d #t) (k 0)))) (h "1rxq3xl92qr6041pa027a8lgk4a4x2x0jfwdz5v6c5jqjzmd8q75") (f (quote (("nightly"))))))

(define-public crate-slice-group-by-0.2.2 (c (n "slice-group-by") (v "0.2.2") (d (list (d (n "rand") (r "^0.5") (d #t) (k 2)) (d (n "sdset") (r "^0.3") (d #t) (k 0)))) (h "14rzpdinrspbj3yidj1g19gx89qzhwdvz4b72m0z1fak52fqwlrm") (f (quote (("nightly"))))))

(define-public crate-slice-group-by-0.2.3-alpha (c (n "slice-group-by") (v "0.2.3-alpha") (d (list (d (n "rand") (r "^0.5") (d #t) (k 2)))) (h "0f70n6846q7pdkfvzr9fmrkzh5jbj4qc0drq2k27lzvrb82hlff4") (f (quote (("std") ("nightly") ("default" "std"))))))

(define-public crate-slice-group-by-0.2.3 (c (n "slice-group-by") (v "0.2.3") (d (list (d (n "rand") (r "^0.5") (d #t) (k 2)))) (h "08arc72rgcj7d8wm7vyx7iav6znrgv77an7figp31d268rmwn5f1") (f (quote (("std") ("nightly") ("default" "std"))))))

(define-public crate-slice-group-by-0.2.4 (c (n "slice-group-by") (v "0.2.4") (d (list (d (n "rand") (r "^0.5") (d #t) (k 2)))) (h "1c13fz29z3j09bm8dq8n273hz74rd0h4hr9jp5w9nz6j9rkrk584") (f (quote (("std") ("nightly") ("default" "std"))))))

(define-public crate-slice-group-by-0.2.5 (c (n "slice-group-by") (v "0.2.5") (d (list (d (n "rand") (r "^0.6.5") (d #t) (k 2)))) (h "17vd2qx581hw4511is656mwwq8xils64il73pdcjmd9y5cxf23xb") (f (quote (("std") ("nightly") ("default" "std"))))))

(define-public crate-slice-group-by-0.2.6 (c (n "slice-group-by") (v "0.2.6") (d (list (d (n "rand") (r "^0.6.5") (d #t) (k 2)))) (h "1yx765cnd8xz0f2ar7lmkn3bq5s6fisdjbmn18v2ilj6nvq78x0z") (f (quote (("std") ("nightly") ("default" "std"))))))

(define-public crate-slice-group-by-0.2.7 (c (n "slice-group-by") (v "0.2.7") (d (list (d (n "rand") (r "^0.6.5") (d #t) (k 2)))) (h "16q20q5p95198r0cs5aywjxdyg667c7dk6bffxhqv9ld80ll6v9k") (f (quote (("std") ("nightly") ("default" "std")))) (y #t)))

(define-public crate-slice-group-by-0.3.0 (c (n "slice-group-by") (v "0.3.0") (d (list (d (n "rand") (r "^0.6.5") (d #t) (k 2)))) (h "1v52vyzmwr0phb8zm2wpgig0fvsp12sl7qagijiv22lnggc39dh3") (f (quote (("std") ("nightly") ("default" "std"))))))

(define-public crate-slice-group-by-0.3.1 (c (n "slice-group-by") (v "0.3.1") (d (list (d (n "rand") (r "^0.6.5") (d #t) (k 2)))) (h "19vbyyxqvc25fv2dmhlxijlk5sa9j34yb6hyydb9vf89kh36fqc2") (f (quote (("std") ("nightly") ("default" "std"))))))

