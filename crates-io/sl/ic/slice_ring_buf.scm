(define-module (crates-io sl ic slice_ring_buf) #:use-module (crates-io))

(define-public crate-slice_ring_buf-0.1.0 (c (n "slice_ring_buf") (v "0.1.0") (h "0kyasy3yj10118p5v5j5ah7075gdbrils90lfykn65hjim8yy30g")))

(define-public crate-slice_ring_buf-0.1.1 (c (n "slice_ring_buf") (v "0.1.1") (h "10g287wydhkqnwqxv3cgl7g9qmhz936zzss7bh3jiajyzxpi7qwz")))

(define-public crate-slice_ring_buf-0.1.2 (c (n "slice_ring_buf") (v "0.1.2") (h "0l1xk5pcl1nwylf3p57l8grzxkw0g2x2abkvrj61qlx20sjlrnhg")))

(define-public crate-slice_ring_buf-0.1.3 (c (n "slice_ring_buf") (v "0.1.3") (h "0dhkvybg5j70pkf4k1kg1lbrxbdccg4qwrgk09hz87wc038x5nj4")))

(define-public crate-slice_ring_buf-0.2.0 (c (n "slice_ring_buf") (v "0.2.0") (h "1zb6daj2fk49qcz9igazhzzqnny3rv4fvmnjxc4a7d6mi5f1zh2y")))

(define-public crate-slice_ring_buf-0.2.1 (c (n "slice_ring_buf") (v "0.2.1") (h "089fzfm9pqliv55w1i8kqa2arx9vfipqs55419kif0ngryn0n7vv")))

(define-public crate-slice_ring_buf-0.2.2 (c (n "slice_ring_buf") (v "0.2.2") (h "02s6jn5zrkldhd6crsc3x0dgbr5h2flj9chmklf9p5p4zznxzahy")))

(define-public crate-slice_ring_buf-0.2.3 (c (n "slice_ring_buf") (v "0.2.3") (h "1az797j2n5b3k59mwqvqxck4bb9s55x4zm5srzs54a4n092rxfv4")))

(define-public crate-slice_ring_buf-0.2.4 (c (n "slice_ring_buf") (v "0.2.4") (h "1q95cddi99d37qhhqmphw11vryrcpvqgw23yw1d77vlgc3ddji7w")))

(define-public crate-slice_ring_buf-0.2.5 (c (n "slice_ring_buf") (v "0.2.5") (h "05v7yr0lp3cjg5qbvqawn7y2z9n7670h89yiyla79fiqpii9dmf5")))

(define-public crate-slice_ring_buf-0.2.6 (c (n "slice_ring_buf") (v "0.2.6") (h "0nalflxpifrl36gxnhmryd5nx58w6si9yq3ibpdz0slz4w7fmhf8")))

(define-public crate-slice_ring_buf-0.2.7 (c (n "slice_ring_buf") (v "0.2.7") (h "0sg4isp226i0szpxdcdkmq78yn0831rcmx49ka401l0g4lf3di5q")))

