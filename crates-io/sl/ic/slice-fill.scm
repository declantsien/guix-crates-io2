(define-module (crates-io sl ic slice-fill) #:use-module (crates-io))

(define-public crate-slice-fill-1.0.0 (c (n "slice-fill") (v "1.0.0") (d (list (d (n "libc") (r "^0.2.68") (d #t) (k 0)))) (h "1nwjx3dycm35vfj43ziz8qq3c2v8vi2cl4fxi6a0drgai9l17c7h")))

(define-public crate-slice-fill-1.0.1 (c (n "slice-fill") (v "1.0.1") (d (list (d (n "libc") (r "^0.2.68") (d #t) (k 0)))) (h "0n1b5sf1rl3z08yz1f6zzph0pz7aa95s6c98yqrjax6ad58ca0dp")))

