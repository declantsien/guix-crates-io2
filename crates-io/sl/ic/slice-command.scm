(define-module (crates-io sl ic slice-command) #:use-module (crates-io))

(define-public crate-slice-command-0.0.0 (c (n "slice-command") (v "0.0.0") (d (list (d (n "clap") (r "^4.2.3") (f (quote ("derive"))) (d #t) (k 0)))) (h "078jgad52q9s91yhkcan0wbligi6hvb2rkr60xcabqmcmjspxcbj")))

(define-public crate-slice-command-0.1.0 (c (n "slice-command") (v "0.1.0") (d (list (d (n "clap") (r "^4.2.4") (f (quote ("derive"))) (d #t) (k 0)))) (h "1x8k90q3pk9z1fl3z68y51pdgf93666lsf8z25x3g9y8am04s0db")))

(define-public crate-slice-command-0.2.0 (c (n "slice-command") (v "0.2.0") (d (list (d (n "clap") (r "^4.2.7") (f (quote ("derive"))) (d #t) (k 0)))) (h "0hvmd4w88wvwgsnkd7nhi7j6lf7g65jhwh739jqnwj50c812av8m")))

(define-public crate-slice-command-0.2.1 (c (n "slice-command") (v "0.2.1") (d (list (d (n "clap") (r "^4.3.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "14zpw1a02fhlh8ds3p6x74n6wp611jaz69jb3xxl6svagx1xr8hb")))

(define-public crate-slice-command-0.2.2 (c (n "slice-command") (v "0.2.2") (d (list (d (n "clap") (r "^4.3.4") (f (quote ("derive"))) (d #t) (k 0)))) (h "0j5y7p7h4c9znl4g1vf7ihlibjdhk9vq2rcx06kzih4rmq32psgl")))

(define-public crate-slice-command-0.3.0 (c (n "slice-command") (v "0.3.0") (d (list (d (n "bytesize") (r "^1.2.0") (d #t) (k 0)) (d (n "clap") (r "^4.3.8") (f (quote ("derive"))) (d #t) (k 0)))) (h "0xs7jdfw1v2lr7qyr6nihqdlbmyq0jxpkpnwbi4xnh21x4l0qzg2")))

(define-public crate-slice-command-0.3.1 (c (n "slice-command") (v "0.3.1") (d (list (d (n "bytesize") (r "^1.2.0") (d #t) (k 0)) (d (n "clap") (r "^4.3.23") (f (quote ("derive"))) (d #t) (k 0)))) (h "0bb72p0qi3qndza4ga8naygs33kz51gbyyjx468vm42xcn0q0n4f")))

(define-public crate-slice-command-0.4.0 (c (n "slice-command") (v "0.4.0") (d (list (d (n "bytesize") (r "^1.3.0") (d #t) (k 0)) (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)))) (h "1rcgvsnhahkq7qjjh7ibq08zsp4579llswx28vxsbib2q845pzl9")))

(define-public crate-slice-command-0.4.1 (c (n "slice-command") (v "0.4.1") (d (list (d (n "bytesize") (r "^1.3.0") (d #t) (k 0)) (d (n "clap") (r "^4.5.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0lga0hb7zia7il28g8isms7gi8spmcai6lbmaawx0203p7xji30w")))

