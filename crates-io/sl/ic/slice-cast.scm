(define-module (crates-io sl ic slice-cast) #:use-module (crates-io))

(define-public crate-slice-cast-0.1.0 (c (n "slice-cast") (v "0.1.0") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)))) (h "0dwp273p2znqg7wn9xs9i0sw1df9c23mpxfpnqw9p87nrhqkxkx2") (y #t)))

(define-public crate-slice-cast-0.1.1 (c (n "slice-cast") (v "0.1.1") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)))) (h "15mmnf5ygrypg0nd1nd26wwqvg1h1vmpkjijcpnkhwaphkqp6hfx") (y #t)))

(define-public crate-slice-cast-0.1.2 (c (n "slice-cast") (v "0.1.2") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)))) (h "12793chfrzr9n16kna28jjp86z331qd78ysizy026g6adxbr27yf") (y #t)))

