(define-module (crates-io sl ic slice-diff-patch) #:use-module (crates-io))

(define-public crate-slice-diff-patch-0.1.0 (c (n "slice-diff-patch") (v "0.1.0") (d (list (d (n "lcs-diff") (r "^0.1.1") (d #t) (k 0)) (d (n "wu-diff") (r "^0.1.2") (d #t) (k 0)))) (h "15qcl842qi6dixnhbw5mrvi8k99vap4l5b0ravi9il2y2pjsgq9h")))

(define-public crate-slice-diff-patch-0.2.0 (c (n "slice-diff-patch") (v "0.2.0") (d (list (d (n "diff") (r "^0.1.12") (d #t) (k 0)) (d (n "lcs-diff") (r "^0.1.1") (d #t) (k 0)) (d (n "wu-diff") (r "^0.1.2") (d #t) (k 0)))) (h "0b6l8gy6bw9zr9iy7mvhqb4byv1zjs4qqbjvsy9smr40wh549qrn")))

(define-public crate-slice-diff-patch-0.2.1 (c (n "slice-diff-patch") (v "0.2.1") (d (list (d (n "diff") (r "^0.1.12") (d #t) (k 0)) (d (n "lcs-diff") (r "^0.1.1") (d #t) (k 0)) (d (n "wu-diff") (r "^0.1.2") (d #t) (k 0)))) (h "1fbz81z4jjkp8h25fk6acwyr9vrz4a32lfl0fmy3x5nhhd7x4ndl")))

(define-public crate-slice-diff-patch-0.2.2 (c (n "slice-diff-patch") (v "0.2.2") (d (list (d (n "diff") (r "^0.1.12") (d #t) (k 0)) (d (n "lcs-diff") (r "^0.1.1") (d #t) (k 0)) (d (n "wu-diff") (r "^0.1.2") (d #t) (k 0)))) (h "066c2sqxwm6fc8ygkirfidw652ymb3hyzx6wy4ibp49200nv1gqz")))

(define-public crate-slice-diff-patch-0.3.0 (c (n "slice-diff-patch") (v "0.3.0") (d (list (d (n "diff") (r "^0.1.12") (d #t) (k 0)) (d (n "lcs-diff") (r "^0.1.1") (d #t) (k 0)) (d (n "wu-diff") (r "^0.1.2") (d #t) (k 0)))) (h "19f01jhjdanavxqld0ny0j28n0ng91hja88hiigbfy7navhsaijg")))

(define-public crate-slice-diff-patch-1.0.0 (c (n "slice-diff-patch") (v "1.0.0") (d (list (d (n "diff") (r "^0.1.12") (d #t) (k 0)) (d (n "lcs-diff") (r "^0.1.1") (d #t) (k 0)) (d (n "wu-diff") (r "^0.1.2") (d #t) (k 0)))) (h "0jryaf8fqisp6kwa92n0gj21g4d9xgj3rwvsf9nc0qdyq9dzbcsq")))

(define-public crate-slice-diff-patch-1.1.0 (c (n "slice-diff-patch") (v "1.1.0") (d (list (d (n "diff") (r "^0.1.12") (d #t) (k 0)) (d (n "lcs-diff") (r "^0.1.1") (d #t) (k 0)) (d (n "wu-diff") (r "^0.1.2") (d #t) (k 0)))) (h "1a3mblgxmk6ngqbcm8fib4capww71si01pdd1chb5d15gm955yp7")))

(define-public crate-slice-diff-patch-1.1.1 (c (n "slice-diff-patch") (v "1.1.1") (d (list (d (n "diff") (r "^0.1.12") (d #t) (k 0)) (d (n "lcs-diff") (r "^0.1.1") (d #t) (k 0)) (d (n "wu-diff") (r "^0.1.2") (d #t) (k 0)))) (h "1002gks633ncjs9a4lcfmniady2rxpghy6sy9xj6y1ivvahn5f68")))

(define-public crate-slice-diff-patch-1.2.1 (c (n "slice-diff-patch") (v "1.2.1") (d (list (d (n "diff") (r "^0.1.12") (d #t) (k 0)) (d (n "lcs-diff") (r "^0.1.1") (d #t) (k 0)) (d (n "wu-diff") (r "^0.1.2") (d #t) (k 0)))) (h "04glp7ac20m2pb8kycm46cv6vmsajpcyj1zzaa8hp0slfcvvajfw")))

