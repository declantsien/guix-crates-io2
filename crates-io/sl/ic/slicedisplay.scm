(define-module (crates-io sl ic slicedisplay) #:use-module (crates-io))

(define-public crate-slicedisplay-0.1.0 (c (n "slicedisplay") (v "0.1.0") (h "1zig1q0x733qdys0ihhxbpzczjm2vh0f4p5g4dikrfjmbnv982z6")))

(define-public crate-slicedisplay-0.2.0 (c (n "slicedisplay") (v "0.2.0") (h "1rlf7lm32s8xyz3gz12xjrksnnb8b7w5biz4rwpd0azz86iv2c31")))

(define-public crate-slicedisplay-0.2.1 (c (n "slicedisplay") (v "0.2.1") (h "17gb9dmyfa5yag202r31s754kqmk6irqj95ql83gpp5zxinwj52x")))

(define-public crate-slicedisplay-0.2.2 (c (n "slicedisplay") (v "0.2.2") (h "1rshlszv3ba1n0q829qly0vlb28bqraa1i0k0861nh5psgrhic78")))

