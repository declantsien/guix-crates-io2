(define-module (crates-io sl ic slice_n) #:use-module (crates-io))

(define-public crate-slice_n-0.0.1 (c (n "slice_n") (v "0.0.1") (d (list (d (n "maybe-std") (r "^0.1.2") (d #t) (k 0)))) (h "1135xvsmcm0swsv1fk8lp0m7r2bjq5zs7pdmxxap91kzy1qpcd29") (f (quote (("unstable" "maybe-std/unstable") ("std" "maybe-std/std") ("default") ("alloc" "maybe-std/alloc"))))))

(define-public crate-slice_n-0.0.2 (c (n "slice_n") (v "0.0.2") (d (list (d (n "maybe-std") (r "^0.1.2") (d #t) (k 0)))) (h "0dzzqqb85v0qimqcxvdrrdi5y6hf8044r4kysxcjqifp71nh5sm6") (f (quote (("unstable" "maybe-std/unstable") ("std" "maybe-std/std") ("default") ("alloc" "maybe-std/alloc"))))))

