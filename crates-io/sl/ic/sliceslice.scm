(define-module (crates-io sl ic sliceslice) #:use-module (crates-io))

(define-public crate-sliceslice-0.1.0 (c (n "sliceslice") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "memchr") (r "^2.3") (d #t) (k 0)) (d (n "memmem") (r "^0.1") (d #t) (k 2)) (d (n "twoway") (r "^0.2") (d #t) (k 2)))) (h "0bxwpwyzbrc93j93xn0v5s5q9bdf3722yppl3aqsyj8g8gkcrmb7") (y #t)))

(define-public crate-sliceslice-0.2.0 (c (n "sliceslice") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "memchr") (r "^2.3") (d #t) (k 0)) (d (n "memmem") (r "^0.1") (d #t) (k 2)) (d (n "twoway") (r "^0.2") (d #t) (k 2)))) (h "0bxl449xyx5sz597c2bdangl6q3spj32r3v16v2z6jpaz3g71dv1")))

(define-public crate-sliceslice-0.2.1 (c (n "sliceslice") (v "0.2.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "memchr") (r "^2.3") (d #t) (k 0)) (d (n "memmem") (r "^0.1") (d #t) (k 2)) (d (n "twoway") (r "^0.2") (d #t) (k 2)))) (h "1nm6ywl8pa2dp5yjkql9harm8rfzw6vmn8dzqw8z132c5kpj8hqi")))

(define-public crate-sliceslice-0.3.0 (c (n "sliceslice") (v "0.3.0") (d (list (d (n "memchr") (r "^2.3") (d #t) (k 0)) (d (n "seq-macro") (r "^0.2") (d #t) (k 0)))) (h "19zsafb7gfm4nr8fkx8m71piw798231lfx7c0rn22lmjmqn2jbr4")))

(define-public crate-sliceslice-0.3.1 (c (n "sliceslice") (v "0.3.1") (d (list (d (n "memchr") (r "^2.3") (d #t) (k 0)) (d (n "seq-macro") (r "^0.2") (d #t) (k 0)))) (h "173fmw79bzznw3wh6xzbv4yrkfg58psw9xw9ilv5pw8x879552nb")))

(define-public crate-sliceslice-0.4.0 (c (n "sliceslice") (v "0.4.0") (d (list (d (n "cfg-if") (r "^1") (d #t) (k 2)) (d (n "memchr") (r "^2.3") (d #t) (k 0)) (d (n "memmap2") (r "^0.5") (d #t) (k 2)) (d (n "multiversion") (r "^0.6") (k 0)) (d (n "seq-macro") (r "^0.2") (d #t) (k 0)))) (h "02amqzbh8pai42aiwf34j7cdz5ilj7g23cjngb1gs8xan6ifqkn8") (f (quote (("stdsimd"))))))

(define-public crate-sliceslice-0.4.1 (c (n "sliceslice") (v "0.4.1") (d (list (d (n "cfg-if") (r "^1") (d #t) (k 2)) (d (n "memchr") (r "^2.3") (d #t) (k 0)) (d (n "memmap2") (r "^0.5") (d #t) (k 2)) (d (n "multiversion") (r "^0.6") (k 0)) (d (n "seq-macro") (r "^0.2") (d #t) (k 0)))) (h "0h525zfjj3jpc08x3qkm8wawx3annghappxm0xdk12a60dxvddn7") (f (quote (("stdsimd") ("aarch64"))))))

(define-public crate-sliceslice-0.4.2 (c (n "sliceslice") (v "0.4.2") (d (list (d (n "cfg-if") (r "^1") (d #t) (k 2)) (d (n "memchr") (r "^2.3") (d #t) (k 0)) (d (n "memmap2") (r "^0.5") (d #t) (k 2)) (d (n "multiversion") (r "^0.6") (k 0)) (d (n "seq-macro") (r "^0.3") (d #t) (k 0)))) (h "1il76pi1jkvv3bdkq945w86nfsidcp0nwa897f83k19ar5y43ch4") (f (quote (("stdsimd"))))))

