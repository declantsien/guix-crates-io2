(define-module (crates-io sl ic slice-dst) #:use-module (crates-io))

(define-public crate-slice-dst-1.0.0 (c (n "slice-dst") (v "1.0.0") (d (list (d (n "autocfg") (r "^1.0.0") (d #t) (k 1)) (d (n "erasable") (r "^1.0.0") (o #t) (d #t) (k 0)))) (h "1gdy3bnhxq08cklmp2kiy38xphj9v6llgpfcfk0fy11n8ndrp6a2") (f (quote (("default" "erasable")))) (y #t)))

(define-public crate-slice-dst-1.1.0 (c (n "slice-dst") (v "1.1.0") (d (list (d (n "autocfg") (r "^1.0.0") (d #t) (k 1)) (d (n "erasable") (r "^1.0.0") (o #t) (d #t) (k 0)))) (h "15mhclvq01bgbvqldg9qvb1g2qjkh03kn9c9mpvnz9liflaivih5") (f (quote (("default" "erasable")))) (y #t)))

(define-public crate-slice-dst-1.1.1 (c (n "slice-dst") (v "1.1.1") (d (list (d (n "autocfg") (r "^1.0.0") (d #t) (k 1)) (d (n "erasable") (r "^1.1.0") (o #t) (d #t) (k 0)))) (h "0wihk6gj7qppfgdq780fi2l6dp2q5wgjlq1f2538g6s05yajzlhq") (f (quote (("default" "erasable")))) (y #t)))

(define-public crate-slice-dst-1.1.2 (c (n "slice-dst") (v "1.1.2") (d (list (d (n "autocfg") (r "^1.0.0") (d #t) (k 1)) (d (n "erasable") (r "^1.1.0") (o #t) (d #t) (k 0)))) (h "1b2id0r1x622a3hfvwnldi3cn72bg5m3qaagirf7xlcjcgx03wv9") (f (quote (("default" "erasable")))) (y #t)))

(define-public crate-slice-dst-1.2.0 (c (n "slice-dst") (v "1.2.0") (d (list (d (n "autocfg") (r "^1.0.0") (d #t) (k 1)) (d (n "erasable") (r "^1.1.0") (o #t) (d #t) (k 0)))) (h "1v97kc0lmq7wncy54icl2gab9lj4g152fnvb7c701wmall1y4d2r") (f (quote (("default" "erasable"))))))

(define-public crate-slice-dst-1.2.1 (c (n "slice-dst") (v "1.2.1") (d (list (d (n "autocfg") (r "^1.0.0") (d #t) (k 1)) (d (n "erasable") (r "^1.1.0") (o #t) (d #t) (k 0)))) (h "0yrl62yyxpmcy2md77dlhzxaaad386irgrmx8i9cf8vv69n9nv34") (f (quote (("default" "erasable"))))))

(define-public crate-slice-dst-1.3.0 (c (n "slice-dst") (v "1.3.0") (d (list (d (n "autocfg") (r "^1.0.0") (d #t) (k 1)) (d (n "erasable") (r "^1.1.0") (o #t) (d #t) (k 0)))) (h "1narz4xnzzqqjmbfx2gbvma5hnl8m0x9da0qd0fq73i4nscq9jba") (f (quote (("default" "erasable"))))))

(define-public crate-slice-dst-1.4.0 (c (n "slice-dst") (v "1.4.0") (d (list (d (n "autocfg") (r "^1.0.0") (d #t) (k 1)) (d (n "erasable") (r "^1.1.0") (o #t) (d #t) (k 0)))) (h "1my1j5zd1q4a84bq1l13nwwd9k2yjf8s6f5mdnd1syc44rl070dz") (f (quote (("default" "erasable"))))))

(define-public crate-slice-dst-1.4.1 (c (n "slice-dst") (v "1.4.1") (d (list (d (n "autocfg") (r "^1.0.0") (d #t) (k 1)) (d (n "erasable") (r "^1.1.0") (o #t) (d #t) (k 0)))) (h "0dkx3yqrmqf9sba50zs2pwclbxbix0dh4ap2kqha78q4pl159bcz") (f (quote (("default" "erasable"))))))

(define-public crate-slice-dst-1.5.0 (c (n "slice-dst") (v "1.5.0") (d (list (d (n "autocfg") (r "^1.0.0") (d #t) (k 1)) (d (n "erasable") (r "^1.1.0") (o #t) (d #t) (k 0)))) (h "1i5an4a2mz7dqb321dz46hkx19f98jwkvqrmfiym5smzhwlxr1m1") (f (quote (("default" "erasable"))))))

(define-public crate-slice-dst-1.5.1 (c (n "slice-dst") (v "1.5.1") (d (list (d (n "autocfg") (r "^1.0.0") (d #t) (k 1)) (d (n "erasable") (r "^1.1.0") (o #t) (d #t) (k 0)))) (h "04il428xdcfrvlixj3havp2k4x42dbd3wkk5x9y9khnplqhnf6pc") (f (quote (("default" "erasable"))))))

