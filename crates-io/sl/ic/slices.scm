(define-module (crates-io sl ic slices) #:use-module (crates-io))

(define-public crate-slices-0.1.0 (c (n "slices") (v "0.1.0") (d (list (d (n "hack") (r "= 0.1.0") (d #t) (k 0) (p "slices-hack")) (d (n "proc-macro-hack") (r "~0.5") (d #t) (k 0)))) (h "00g5iylg4hwky474mrkcmzb6y28fx6zjpsjf57nql8jg3y9kkw9d")))

(define-public crate-slices-0.1.1 (c (n "slices") (v "0.1.1") (d (list (d (n "hack") (r "= 0.1.1") (d #t) (k 0) (p "slices-hack")) (d (n "proc-macro-hack") (r "~0.5") (d #t) (k 0)))) (h "1brxndlk6l72f92zifh48l6xz2705ypw05lfkgkdag73b1znydkk")))

(define-public crate-slices-0.2.0 (c (n "slices") (v "0.2.0") (d (list (d (n "faster-hex") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (d #t) (k 0)))) (h "1ks9fw8dmsp2r60bp6nr6vk3qbncnh2fvxp972ldr71ni92nw27j")))

