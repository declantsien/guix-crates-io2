(define-module (crates-io sl ic slick) #:use-module (crates-io))

(define-public crate-slick-0.1.0 (c (n "slick") (v "0.1.0") (h "1jcnls0l6ch6zfq56cm0irr2mxs83bhkffp1vbr1f8x1gpvwcdc0")))

(define-public crate-slick-0.1.1 (c (n "slick") (v "0.1.1") (h "0ln6pa6xxhn969xqik70658nvvjnqvl29kqwlbxric7sjr3h63cg")))

(define-public crate-slick-0.1.2 (c (n "slick") (v "0.1.2") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "git2") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0q0i9wszqj7jfvdgbszk8silvlwr4k07vrclp6g6pap95xshdlcz")))

(define-public crate-slick-0.1.3 (c (n "slick") (v "0.1.3") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "git2") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0mxb6nwgq3z7xprnzcssaam17776c8705w5z6s7kpq3g2gfg7jxh")))

(define-public crate-slick-0.1.4 (c (n "slick") (v "0.1.4") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "git2") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1rkm6786gnkaf12gaz12lvjnsa8x4vn15xxydbmff7hk8fmw9kx8")))

(define-public crate-slick-0.1.5 (c (n "slick") (v "0.1.5") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "git2") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1psr56miw3w8c3pscfifanzzsd396pgj8qzvk16yhbm17qrmlhhm")))

(define-public crate-slick-0.1.6 (c (n "slick") (v "0.1.6") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "git2") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0s6mjdb9312nx2jwar12sxn5mc4balx1m4b1z4d6ywmnsgic2n4w")))

(define-public crate-slick-0.1.7 (c (n "slick") (v "0.1.7") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "compound_duration") (r "^1") (d #t) (k 0)) (d (n "git2") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0jj7qycabin6plqn41437dh0fma1djq9p0y41xl9w4b2xr1kayhh")))

(define-public crate-slick-0.1.8 (c (n "slick") (v "0.1.8") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "compound_duration") (r "^1") (d #t) (k 0)) (d (n "git2") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "143zfii6y47cslpjg2axcd2p6d5ig76pzllw86l56b02xswsvvkz")))

(define-public crate-slick-0.1.9 (c (n "slick") (v "0.1.9") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "compound_duration") (r "^1") (d #t) (k 0)) (d (n "git2") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "17adzacqmx20g4filayz7b42y1r83s0pd9nblsl8hs7vn3kk0frk")))

(define-public crate-slick-0.1.10 (c (n "slick") (v "0.1.10") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "compound_duration") (r "^1") (d #t) (k 0)) (d (n "git2") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1pm2gva6msdzj622iwhl6nw0qp7xqsgvswj6njp5i3kn0xzfzrbx")))

(define-public crate-slick-0.1.11 (c (n "slick") (v "0.1.11") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "compound_duration") (r "^1") (d #t) (k 0)) (d (n "git2") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0m5r6hdj4fqlfgny1bnp2sqaz04dnpj0dzyyxzb9x5jszcsapzcv")))

(define-public crate-slick-0.1.12 (c (n "slick") (v "0.1.12") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "compound_duration") (r "^1") (d #t) (k 0)) (d (n "git2") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1l6zp6avjf925ab8ss75bs1kqpad3sll7l19if0z9g8rf1ldndz1")))

(define-public crate-slick-0.2.0 (c (n "slick") (v "0.2.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "compound_duration") (r "^1") (d #t) (k 0)) (d (n "git2") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "users") (r "^0.9") (d #t) (k 0)))) (h "00l2agwaa5szc5b7i9j9lms4314x4wlzkbs6zb17xqn3lfpj2hr5")))

(define-public crate-slick-0.2.1 (c (n "slick") (v "0.2.1") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "compound_duration") (r "^1") (d #t) (k 0)) (d (n "git2") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "users") (r "^0.9") (d #t) (k 0)))) (h "10vn4j32a8d6881zs49kmh4hyfk26r3dq01wi863cw634c8z5v3x")))

(define-public crate-slick-0.3.0 (c (n "slick") (v "0.3.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "compound_duration") (r "^1") (d #t) (k 0)) (d (n "git2") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "users") (r "^0.9") (d #t) (k 0)))) (h "0aj0kn5a09wy94psjkwyff81bhw9cmckmdv91qlxj0d031p97sk2")))

(define-public crate-slick-0.4.0 (c (n "slick") (v "0.4.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "compound_duration") (r "^1") (d #t) (k 0)) (d (n "git2") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "users") (r "^0.9") (d #t) (k 0)))) (h "1qksriicb8j6v0l743qgvq38y4xjpp9lx2iihwh19fbwkwqvw2hj")))

(define-public crate-slick-0.4.1 (c (n "slick") (v "0.4.1") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "compound_duration") (r "^1") (d #t) (k 0)) (d (n "git2") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "users") (r "^0.9") (d #t) (k 0)))) (h "1r80d5xk2awkh8c5lnc5pg0ccksc26ylkgbpfdlynd77349ji272")))

(define-public crate-slick-0.5.0 (c (n "slick") (v "0.5.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "compound_duration") (r "^1") (d #t) (k 0)) (d (n "git2") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "users") (r "^0.9") (d #t) (k 0)))) (h "0lyrrgk2bdaw6fxgfq93n984rhgbq305axrdnmwyjsgvj1dr0r8f")))

(define-public crate-slick-0.6.0 (c (n "slick") (v "0.6.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "compound_duration") (r "^1") (d #t) (k 0)) (d (n "git2") (r "^0.9") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "users") (r "^0.9") (d #t) (k 0)))) (h "021nnm34102agdxq06faknfyfmwskssng2dpyy8blxj2np9mw6pq")))

(define-public crate-slick-0.6.1 (c (n "slick") (v "0.6.1") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "compound_duration") (r "^1") (d #t) (k 0)) (d (n "git2") (r "^0.9") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "users") (r "^0.9") (d #t) (k 0)))) (h "091gzji8ik6gpn9zgrkv28i2rvgfpwdhcg8gbm11a83b57kp50jp")))

(define-public crate-slick-0.6.2 (c (n "slick") (v "0.6.2") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "compound_duration") (r "^1") (d #t) (k 0)) (d (n "git2") (r "^0.9") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "users") (r "^0.9") (d #t) (k 0)))) (h "1y8k8hz0b1rd4cr9m0amskr925ykjc7izy2ndyybcdadq376b75d")))

(define-public crate-slick-0.7.0 (c (n "slick") (v "0.7.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "compound_duration") (r "^1") (d #t) (k 0)) (d (n "git2") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "users") (r "^0.9") (d #t) (k 0)))) (h "1g30aa0c4jh8jzv45swf99pzdgambv5kjws546lqfb6nrkchdxgz")))

(define-public crate-slick-0.7.1 (c (n "slick") (v "0.7.1") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "compound_duration") (r "^1") (d #t) (k 0)) (d (n "git2") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "users") (r "^0.9") (d #t) (k 0)))) (h "0crxbw2qy3zacmzrm67rdcx5lfz8a7gkzjy179jzqsff1nvkz6x8")))

(define-public crate-slick-0.7.2 (c (n "slick") (v "0.7.2") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "compound_duration") (r "^1") (d #t) (k 0)) (d (n "git2") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "users") (r "^0.9") (d #t) (k 0)))) (h "0g4lyif38ilpwcmfplpmx018bg9s1cb49rrad1gnas5lwn56a3qz")))

(define-public crate-slick-0.8.0 (c (n "slick") (v "0.8.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "compound_duration") (r "^1") (d #t) (k 0)) (d (n "git2") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "users") (r "^0.9") (d #t) (k 0)))) (h "0vv4s51q9072vfgmhxvx5v8yc88s0s27dqg8hggc7w83nb1cf619")))

(define-public crate-slick-0.9.0 (c (n "slick") (v "0.9.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "compound_duration") (r "^1") (d #t) (k 0)) (d (n "git2") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "users") (r "^0.9") (d #t) (k 0)))) (h "036xjn76qhicfjxhvk6zigic95nx6rh1flrai2yzim7nzhwvp2m2")))

(define-public crate-slick-0.9.1 (c (n "slick") (v "0.9.1") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "compound_duration") (r "^1") (d #t) (k 0)) (d (n "git2") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "users") (r "^0.9") (d #t) (k 0)))) (h "1nymv1by5v3qiwmrg261nrkl06paq2kf8a7ssglidh7dg4b7p9ag")))

(define-public crate-slick-0.9.2 (c (n "slick") (v "0.9.2") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "compound_duration") (r "^1") (d #t) (k 0)) (d (n "git2") (r "^0.13") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "users") (r "^0.9") (d #t) (k 0)))) (h "18jabf5r4fsz42h2dwp9lh5p3gh4sv61d1461qfnqf0f0bix1zhl")))

(define-public crate-slick-0.9.3 (c (n "slick") (v "0.9.3") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "compound_duration") (r "^1") (d #t) (k 0)) (d (n "git2") (r "^0.13") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "users") (r "^0.11") (d #t) (k 0)))) (h "14kaha0vd7li1jh41hn5j5gdbh5yla6ajb4i06vzj52ns2q75mcx")))

(define-public crate-slick-0.9.4 (c (n "slick") (v "0.9.4") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "compound_duration") (r "^1") (d #t) (k 0)) (d (n "git2") (r "^0.13") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "users") (r "^0.11") (d #t) (k 0)))) (h "09272y6ya5qqfk5q8b34na9rij9d2xd0gb3zvbmmk9kb1k7zxmds")))

(define-public crate-slick-0.9.5 (c (n "slick") (v "0.9.5") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "compound_duration") (r "^1") (d #t) (k 0)) (d (n "git2") (r "^0.13") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "users") (r "^0.11") (d #t) (k 0)))) (h "052kn809dhk3wlp9sh5njj6gvsy1xhhyqqcca67xbba4xikjcqp4")))

(define-public crate-slick-0.10.0 (c (n "slick") (v "0.10.0") (d (list (d (n "clap") (r "^3.1") (d #t) (k 0)) (d (n "compound_duration") (r "^1") (d #t) (k 0)) (d (n "git2") (r "^0.14") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "users") (r "^0.11") (d #t) (k 0)))) (h "0f4ka5vy0ip1fn4n2h0kzrpf861s1fxvssrnb99kzp7y876zs5bz")))

(define-public crate-slick-0.11.0 (c (n "slick") (v "0.11.0") (d (list (d (n "clap") (r "^3.1") (d #t) (k 0)) (d (n "compound_duration") (r "^1") (d #t) (k 0)) (d (n "git2") (r "^0.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "users") (r "^0.11") (d #t) (k 0)))) (h "0kfkp1zf1hq1d9ha8iqw5j8p9s62mipavjlxrc79ikjxnma8d10m")))

(define-public crate-slick-0.11.1 (c (n "slick") (v "0.11.1") (d (list (d (n "clap") (r "^3.1") (d #t) (k 0)) (d (n "compound_duration") (r "^1") (d #t) (k 0)) (d (n "git2") (r "^0.18") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "users") (r "^0.11") (d #t) (k 0)))) (h "1j4c2alp5d0f8z3mgmlp4g8jwd6swsa1ka2rsibrcr3fbimgfqrb")))

(define-public crate-slick-0.11.2 (c (n "slick") (v "0.11.2") (d (list (d (n "clap") (r "^4") (d #t) (k 0)) (d (n "compound_duration") (r "^1") (d #t) (k 0)) (d (n "git2") (r "^0.18") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (f (quote ("vendored"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "users") (r "^0.11") (d #t) (k 0)))) (h "0rhq8gr0aaq9g1c69xahkn2pr7hc7z7917sv4gsrl70v8b7r312l")))

(define-public crate-slick-0.12.0 (c (n "slick") (v "0.12.0") (d (list (d (n "clap") (r "^4") (d #t) (k 0)) (d (n "compound_duration") (r "^1") (d #t) (k 0)) (d (n "git2") (r "^0.18") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (f (quote ("vendored"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "users") (r "^0.11") (d #t) (k 0)))) (h "0nm3r6c36dsrbvg5svpni97ykis2rhnsgpzd43ibc5zbnsfdjw11")))

