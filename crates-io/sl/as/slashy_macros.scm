(define-module (crates-io sl as slashy_macros) #:use-module (crates-io))

(define-public crate-slashy_macros-0.1.0 (c (n "slashy_macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0kzg0kblzk28xzpjrirx2f0r1d8rpidbljcib5l91kyai89nlxw0")))

(define-public crate-slashy_macros-0.1.2 (c (n "slashy_macros") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "04g93av8lvmppwmm2382fq10gw2andkh20a1k8cl5a0l6syv0isy")))

