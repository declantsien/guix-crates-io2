(define-module (crates-io sl as slas) #:use-module (crates-io))

(define-public crate-slas-0.1.0 (c (n "slas") (v "0.1.0") (d (list (d (n "blas-src") (r "^0.8.0") (f (quote ("blis"))) (d #t) (k 0)) (d (n "cblas-sys") (r "^0.1.4") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "0ky8nrjycka0r4bwdha2kvd24b3nhsl7qx0ha54yb9cyrsbpymwk")))

(define-public crate-slas-0.1.1 (c (n "slas") (v "0.1.1") (d (list (d (n "blas-src") (r "^0.8.0") (f (quote ("blis"))) (d #t) (k 0)) (d (n "cblas-sys") (r "^0.1.4") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "113ifgvxvb3a8k49f3d53cgsnda7l78zidndpdglpg62qyxqk8xz")))

(define-public crate-slas-0.2.0 (c (n "slas") (v "0.2.0") (d (list (d (n "blis-src") (r "^0.2.0") (f (quote ("system"))) (o #t) (d #t) (k 0)) (d (n "cblas-sys") (r "^0.1.4") (d #t) (k 0)))) (h "09rclld0jxjbnj9vmf50xzpbxcda6vvi6km34m028jfy1x20r46v") (f (quote (("default" "blis-sys") ("blis-sys" "blis-src"))))))

(define-public crate-slas-0.2.1 (c (n "slas") (v "0.2.1") (d (list (d (n "blis-src") (r "^0.2.0") (f (quote ("system"))) (o #t) (d #t) (k 0)) (d (n "cblas-sys") (r "^0.1.4") (d #t) (k 0)))) (h "0xv7xsh155s8qpxj94qr5l2qrs692fyx0i26fkjb8r4alvahkcrx") (f (quote (("default" "blis-sys") ("blis-sys" "blis-src"))))))

(define-public crate-slas-0.3.0 (c (n "slas") (v "0.3.0") (d (list (d (n "blis-src") (r "^0.2.0") (o #t) (k 0)) (d (n "cblas-sys") (r "^0.1.4") (o #t) (d #t) (k 0)) (d (n "levitate") (r "^0.1.0") (d #t) (k 0)) (d (n "paste") (r "^1.0.6") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.0.0") (d #t) (k 2)))) (h "00i9vj23hvkv78gc2qgmzgmizpacqbpc4ci97200bzwqw561bhkh") (f (quote (("fast-floats" "levitate/fast-floats") ("default" "blis-sys" "fast-floats") ("blis-sys" "blis-src" "blis-src/system" "blas") ("blis-static" "blis-src" "blis-src/static" "blas") ("blas" "cblas-sys"))))))

(define-public crate-slas-0.3.1 (c (n "slas") (v "0.3.1") (d (list (d (n "blis-src") (r "^0.2.0") (o #t) (k 0)) (d (n "cblas-sys") (r "^0.1.4") (o #t) (d #t) (k 0)) (d (n "levitate") (r "^0.1.1") (d #t) (k 0)) (d (n "paste") (r "^1.0.6") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.0.0") (d #t) (k 2)))) (h "1kq5vwa6jm3321kvpnvq5sa43wh0waqdd916vx52rgccm3kvpfi4") (f (quote (("fast-floats" "levitate/fast-floats") ("default" "blis-sys" "fast-floats") ("blis-sys" "blis-src" "blis-src/system" "blas") ("blis-static" "blis-src" "blis-src/static" "blas") ("blas" "cblas-sys"))))))

