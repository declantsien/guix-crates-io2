(define-module (crates-io sl as slash) #:use-module (crates-io))

(define-public crate-slash-0.1.0 (c (n "slash") (v "0.1.0") (h "1gjqw7rsl8n1ky4x9z4ly7xhjcrni0kl99snxxscdls1db03j3f0")))

(define-public crate-slash-0.1.1 (c (n "slash") (v "0.1.1") (h "1r0v4h3q391cc8ql508nki2m3b6yd492js1048zmpgkal180pfkj")))

