(define-module (crates-io sl as slashook-macros) #:use-module (crates-io))

(define-public crate-slashook-macros-0.1.0 (c (n "slashook-macros") (v "0.1.0") (d (list (d (n "devise") (r "^0.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.28") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.75") (f (quote ("full"))) (d #t) (k 0)))) (h "09f5l8fmp2qp9ww0f72fkrq9xjgq6vbxvssz29xa6104iy57xicg")))

(define-public crate-slashook-macros-0.3.0 (c (n "slashook-macros") (v "0.3.0") (d (list (d (n "devise") (r "^0.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("full"))) (d #t) (k 0)))) (h "05vj7wdkkgavcqk3w7y6wi2zp6q16vyb5zmks675gynm0avx58gk")))

