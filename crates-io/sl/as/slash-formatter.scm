(define-module (crates-io sl as slash-formatter) #:use-module (crates-io))

(define-public crate-slash-formatter-1.0.0 (c (n "slash-formatter") (v "1.0.0") (h "1r64pg2rsk9j27jbcbq99qlnw0flmf3naf8g9n2bis18vknv13yn")))

(define-public crate-slash-formatter-1.0.1 (c (n "slash-formatter") (v "1.0.1") (h "0flkbmapd828hi3w8x9355ifmwgvs9jyrgasxmj44zl0rr11hs0f")))

(define-public crate-slash-formatter-2.0.0 (c (n "slash-formatter") (v "2.0.0") (h "1l8cz68s4xq81x9xp9pjb0pclb278mpr1psi8f8j1dwlyp74sc0m")))

(define-public crate-slash-formatter-2.1.0 (c (n "slash-formatter") (v "2.1.0") (h "0kgc93s46lz03l7j2jlnq347idpgk9n56294fbfwhsm6yna3cv68")))

(define-public crate-slash-formatter-2.2.0 (c (n "slash-formatter") (v "2.2.0") (h "1m5b12kk2cjr334lm1gq9hd7bj2ismrc5yycpby29iwk4dv2wzrk")))

(define-public crate-slash-formatter-2.2.1 (c (n "slash-formatter") (v "2.2.1") (h "0h3c00r7miwr30nlcq02c63i5d03y6qyf1mxi8wz5y5z6iy17hpc")))

(define-public crate-slash-formatter-2.2.2 (c (n "slash-formatter") (v "2.2.2") (h "1lpwf5flw359nx994fak4ymf9mafxbrnrqnmay8a5mgvqz4vr4xg")))

(define-public crate-slash-formatter-2.2.3 (c (n "slash-formatter") (v "2.2.3") (h "03h25nll4i5xhyxkjvqldxrsq8bkgysk8s7z68cnf2yqd9rdywsw")))

(define-public crate-slash-formatter-2.2.4 (c (n "slash-formatter") (v "2.2.4") (h "1y2h9dpx5pniaqqin4h3fbjhrhikl4sy1vckqdwgv5m8qwn64il1")))

(define-public crate-slash-formatter-2.2.5 (c (n "slash-formatter") (v "2.2.5") (h "1dr21vyxzlmcys7dskv29w6kh67jqdrk4wiw0ivpk9npl56mj9qk")))

(define-public crate-slash-formatter-2.2.6 (c (n "slash-formatter") (v "2.2.6") (h "0s85nzxwldv2vwlhclvjcbn90a71li1vqgizcxa200p2fs7bjzrg")))

(define-public crate-slash-formatter-3.0.0 (c (n "slash-formatter") (v "3.0.0") (d (list (d (n "concat-with") (r "^0.2.3") (d #t) (k 0)))) (h "027q494d7s11jsdhbkxjyx0kfvxbl158lyg80pbpkgjkjk8y7d49")))

(define-public crate-slash-formatter-3.0.1 (c (n "slash-formatter") (v "3.0.1") (d (list (d (n "concat-with") (r "^0.2.3") (d #t) (k 0)))) (h "0z79b7dg6x3p0gl7hgxal90gkqrf748g5sml92dgf6l0lsvgap4x")))

(define-public crate-slash-formatter-3.0.2 (c (n "slash-formatter") (v "3.0.2") (d (list (d (n "concat-with") (r "^0.2.3") (d #t) (k 0)))) (h "1x0bdg67bwpb4wn8dyj1x05gk0cxcg20kpwi4kncf3a2ahszyqic")))

(define-public crate-slash-formatter-3.0.3 (c (n "slash-formatter") (v "3.0.3") (d (list (d (n "concat-with") (r "^0.2.3") (d #t) (k 0)))) (h "1nay1al2bphr25clzvviy3j9bfy7zhdy0lbz0ihwr57yxgsgh8rv")))

(define-public crate-slash-formatter-3.1.0 (c (n "slash-formatter") (v "3.1.0") (d (list (d (n "concat-with") (r "^0.2.3") (d #t) (k 0)))) (h "1ddqwh3nld1b29gnmqkk4fm47pyfvgyn8cpizf4zsv552ims66dc")))

(define-public crate-slash-formatter-3.1.1 (c (n "slash-formatter") (v "3.1.1") (d (list (d (n "concat-with") (r "^0.2.3") (d #t) (k 0)))) (h "1l2azxcxcbf7cx3sf1n887x86qckvmi4panaxz5i7li5qjvnb82m")))

(define-public crate-slash-formatter-3.1.2 (c (n "slash-formatter") (v "3.1.2") (d (list (d (n "concat-with") (r "^0.2.3") (d #t) (k 0)))) (h "00d256f1afkscd3da5zjyrfqvpr4igck0f48nsklkbh09qvz1rcb")))

(define-public crate-slash-formatter-3.1.3 (c (n "slash-formatter") (v "3.1.3") (d (list (d (n "concat-with") (r "^0.2.3") (d #t) (k 0)))) (h "1yqzm9hm4ffm32imsxmwdwrd01zf31prx4i2zxm4ws6lm2fsfmr3")))

(define-public crate-slash-formatter-3.1.4 (c (n "slash-formatter") (v "3.1.4") (d (list (d (n "concat-with") (r "^0.2.3") (d #t) (k 0)))) (h "1qf4r41kj4v5g1rrp1gn56d244bmi3hrj3ncm7b8rcv4frdi2c3d") (y #t) (r "1.56")))

(define-public crate-slash-formatter-3.1.5 (c (n "slash-formatter") (v "3.1.5") (d (list (d (n "concat-with") (r "^0.2.3") (d #t) (k 0)))) (h "1lcxmpv8pmz4bg7jwfsl5i5gzp8zqj411ilvkihwa6c8nv7rzf9x") (y #t) (r "1.56")))

(define-public crate-slash-formatter-3.1.6 (c (n "slash-formatter") (v "3.1.6") (d (list (d (n "concat-with") (r "^0.2.3") (d #t) (k 0)))) (h "1gzz6ggczm6s9ayz5wja20x0lr0ljmhzfx5k61107msrxglpyxhl") (r "1.56")))

