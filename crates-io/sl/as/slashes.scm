(define-module (crates-io sl as slashes) #:use-module (crates-io))

(define-public crate-slashes-0.1.0 (c (n "slashes") (v "0.1.0") (h "0xh436xqbaqfk9sszarn2lk6db1xn5fwr0vanz8w2b7622faqn06")))

(define-public crate-slashes-0.1.1 (c (n "slashes") (v "0.1.1") (h "07ag56fn49wchskgl28b3x7ylf2zrqkpk3qnjc251v25biza5dw4")))

