(define-module (crates-io sl as slashies) #:use-module (crates-io))

(define-public crate-slashies-0.1.0 (c (n "slashies") (v "0.1.0") (d (list (d (n "serenity") (r "^0.11") (f (quote ("cache" "client" "collector" "gateway" "rustls_backend" "model" "unstable_discord_api"))) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "157j42i835wirklja63rnlgzgsr15vc0x69a7qwnccfv0pnk5gnm")))

(define-public crate-slashies-0.1.1 (c (n "slashies") (v "0.1.1") (d (list (d (n "serenity") (r "^0.11") (f (quote ("cache" "client" "collector" "gateway" "rustls_backend" "model" "unstable_discord_api"))) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0bqgjmafvwlgnvc47jrncqbyjvpns99xcs9pm8qg5yn3d0x6xxcl")))

(define-public crate-slashies-0.1.2 (c (n "slashies") (v "0.1.2") (d (list (d (n "serenity") (r "^0.11") (f (quote ("cache" "client" "collector" "gateway" "rustls_backend" "model" "unstable_discord_api"))) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0gim6pcwmawnsd1wgwykxd3wd5yyalkz1wzyv4hrlqdfkcpsx241")))

(define-public crate-slashies-0.1.3 (c (n "slashies") (v "0.1.3") (d (list (d (n "serenity") (r "^0.11") (f (quote ("cache" "client" "collector" "gateway" "rustls_backend" "model" "unstable_discord_api"))) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0f2vpqd2nkjbmvajrfg6hrggl165lajpfcbii1qw5jf78ginj0f0")))

