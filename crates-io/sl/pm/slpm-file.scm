(define-module (crates-io sl pm slpm-file) #:use-module (crates-io))

(define-public crate-slpm-file-0.1.0 (c (n "slpm-file") (v "0.1.0") (d (list (d (n "aes-gcm") (r "^0.9.4") (d #t) (k 0)) (d (n "argon2") (r "^0.3.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "pad") (r "^0.1.6") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "00gbqyy8b3smwyi6ipwvysznkkmlz34ggvrawc9jrfyc32w8054i")))

(define-public crate-slpm-file-0.1.1 (c (n "slpm-file") (v "0.1.1") (d (list (d (n "aes-gcm") (r "^0.9.4") (d #t) (k 0)) (d (n "argon2") (r "^0.3.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "pad") (r "^0.1.6") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "1f9b9llli9c4w34bcjmwi3a1x99mz8npc8vldnwzv2bzbr58f54w")))

(define-public crate-slpm-file-0.1.2 (c (n "slpm-file") (v "0.1.2") (d (list (d (n "aes-gcm") (r "^0.9.4") (d #t) (k 0)) (d (n "argon2") (r "^0.3.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "pad") (r "^0.1.6") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "00cpxn732177n5qcqvwfp9xzpfyqxr4hicx0yng9zm81cdn9pkws")))

(define-public crate-slpm-file-0.1.3 (c (n "slpm-file") (v "0.1.3") (d (list (d (n "aes-gcm") (r "^0.9.4") (d #t) (k 0)) (d (n "argon2") (r "^0.3.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "pad") (r "^0.1.6") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "0w135a028kk84il2mnk226w7wcnvcgkki68hnn4h9jq96r880ihx")))

