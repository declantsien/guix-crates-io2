(define-module (crates-io sl vs slvs) #:use-module (crates-io))

(define-public crate-slvs-0.0.0 (c (n "slvs") (v "0.0.0") (d (list (d (n "bindgen") (r "^0.64.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.79") (d #t) (k 1)) (d (n "dunce") (r "^1.0.3") (d #t) (k 1)))) (h "09kaf9qsplcryxjdr3z02qcm9cgkjz3kyy2p7r0gi75nggklb1sq")))

(define-public crate-slvs-0.5.0 (c (n "slvs") (v "0.5.0") (d (list (d (n "bindgen") (r "^0.64.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.79") (d #t) (k 1)) (d (n "dunce") (r "^1.0.3") (d #t) (k 1)))) (h "08zzxgi0hp8amgm1vmgic5pvw4h6v96hz1q2v47zs4nak3841gws")))

(define-public crate-slvs-0.5.1 (c (n "slvs") (v "0.5.1") (d (list (d (n "bindgen") (r "^0.64.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.79") (d #t) (k 1)) (d (n "dunce") (r "^1.0.3") (d #t) (k 1)))) (h "079h56i7s3lpv2avglax0y9a4maxjiym5xi3l4khy5xkr14ld7dp")))

(define-public crate-slvs-0.6.0 (c (n "slvs") (v "0.6.0") (d (list (d (n "bindgen") (r "^0.64.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.79") (d #t) (k 1)) (d (n "dunce") (r "^1.0.3") (d #t) (k 1)) (d (n "euclid") (r "^0.22.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1hrs5ap3y34h44i3p0h8rsrq1wycxk5ng5a5s2955m99b5z8q3mn")))

