(define-module (crates-io sl ap slap_vm) #:use-module (crates-io))

(define-public crate-slap_vm-0.1.0 (c (n "slap_vm") (v "0.1.0") (h "1zmqsxv95wk09h3fsqxgqq0qpb9lvqb5xjjg2ifwq9zi9pi8qszm") (y #t)))

(define-public crate-slap_vm-0.1.1 (c (n "slap_vm") (v "0.1.1") (h "15hxhzfc2is737c84nr139z6yj1x3xl0v2cckma2rmv11dyk5bxd") (y #t)))

(define-public crate-slap_vm-0.2.0 (c (n "slap_vm") (v "0.2.0") (h "0fbshahs1x9ch1x036vf9by3b6gxwckaifq34yyy58kk9riv99p0")))

