(define-module (crates-io sl id sliderule) #:use-module (crates-io))

(define-public crate-sliderule-0.1.0 (c (n "sliderule") (v "0.1.0") (d (list (d (n "git2") (r "^0.8") (d #t) (k 2)) (d (n "globwalk") (r "^0.6") (d #t) (k 0)) (d (n "liquid") (r "^0.17") (d #t) (k 0)) (d (n "os_info") (r "^1.1") (d #t) (k 0)) (d (n "uuid") (r "^0.7") (f (quote ("v4"))) (d #t) (k 2)) (d (n "walkdir") (r "^2.2.5") (d #t) (k 0)))) (h "0xqjn5v72yw2habdm8p9jd6bmafpcp373n4dycc9cfkp17cszkr5")))

(define-public crate-sliderule-0.2.0 (c (n "sliderule") (v "0.2.0") (d (list (d (n "git2") (r "^0.8") (d #t) (k 2)) (d (n "globwalk") (r "^0.6") (d #t) (k 0)) (d (n "liquid") (r "^0.17") (d #t) (k 0)) (d (n "os_info") (r "^1.1") (d #t) (k 0)) (d (n "uuid") (r "^0.7") (f (quote ("v4"))) (d #t) (k 2)) (d (n "walkdir") (r "^2.2.5") (d #t) (k 0)))) (h "17k15wfn8w8dpapr5aqhmpkb91drj2cf1mwznvys4n2aa2cwvw3n")))

(define-public crate-sliderule-0.2.1 (c (n "sliderule") (v "0.2.1") (d (list (d (n "git2") (r "^0.8") (d #t) (k 2)) (d (n "globwalk") (r "^0.6") (d #t) (k 0)) (d (n "liquid") (r "^0.17") (d #t) (k 0)) (d (n "os_info") (r "^1.1") (d #t) (k 0)) (d (n "uuid") (r "^0.7") (f (quote ("v4"))) (d #t) (k 2)) (d (n "walkdir") (r "^2.2.5") (d #t) (k 0)))) (h "1bmqxklvs4s6kkz7r6jpgkrp01cf96hnqwcc6yi9hb3na8yp8vmc")))

