(define-module (crates-io sl id slide) #:use-module (crates-io))

(define-public crate-slide-0.0.1 (c (n "slide") (v "0.0.1") (d (list (d (n "annotate-snippets") (r "^0.8.0") (f (quote ("color"))) (d #t) (k 0)) (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "difference") (r "^2.0.0") (d #t) (k 2)) (d (n "libslide") (r "^0.0.1") (d #t) (k 0)) (d (n "termcolor") (r "^1.1.0") (d #t) (k 0)))) (h "0pn0z2kmvksk5m7iq1n5m3296nknmvfshfkg3pb8c9867mmmj25b")))

