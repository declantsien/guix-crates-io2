(define-module (crates-io sl id slid) #:use-module (crates-io))

(define-public crate-slid-0.1.0 (c (n "slid") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.149") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_arrays") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "0nq2shbv10rxvcw03sib1mli9mik44fyl2zfa8i0j9lw15wbch2b") (f (quote (("default" "serde")))) (s 2) (e (quote (("serde" "dep:serde" "serde_arrays"))))))

(define-public crate-slid-0.2.0 (c (n "slid") (v "0.2.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.149") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_arrays") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "08b5bicr4aya24bnzwzbk5srf68l0mmwcgc8qg8bfis0417kvj75") (f (quote (("default" "serde")))) (s 2) (e (quote (("serde" "dep:serde" "serde_arrays"))))))

