(define-module (crates-io sl id sliding_features) #:use-module (crates-io))

(define-public crate-sliding_features-0.4.0 (c (n "sliding_features") (v "0.4.0") (d (list (d (n "dyn-clone") (r "^1.0.3") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "round") (r "^0.1") (d #t) (k 2)) (d (n "rulinalg") (r "^0.4.2") (d #t) (k 0)))) (h "0h19211zr4l94c9fyhjyg8s9r2cpx5ncpp6sjkl0i8sxqzk6dxk4")))

(define-public crate-sliding_features-0.5.0 (c (n "sliding_features") (v "0.5.0") (d (list (d (n "dyn-clone") (r "^1.0.3") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "round") (r "^0.1") (d #t) (k 2)) (d (n "rulinalg") (r "^0.4.2") (d #t) (k 0)))) (h "1a7yxwipdqan6v9xpljzrrzh2iyzky6n1qswhnk4xxr0jy9xr4z3")))

(define-public crate-sliding_features-0.5.1 (c (n "sliding_features") (v "0.5.1") (d (list (d (n "dyn-clone") (r "^1.0.3") (d #t) (k 0)) (d (n "plotters") (r "^0.3.0") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "round") (r "^0.1") (d #t) (k 2)) (d (n "rulinalg") (r "^0.4.2") (d #t) (k 0)) (d (n "time_series_generator") (r "^0.2.0") (d #t) (k 2)))) (h "1fygmvzbrk0pcxd7jwlnhl94ckzb7pkd3czk9nnk98xg187p448d")))

(define-public crate-sliding_features-0.5.2 (c (n "sliding_features") (v "0.5.2") (d (list (d (n "dyn-clone") (r "^1.0.3") (d #t) (k 0)) (d (n "plotters") (r "^0.3.0") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "round") (r "^0.1") (d #t) (k 2)) (d (n "rulinalg") (r "^0.4.2") (d #t) (k 0)) (d (n "time_series_generator") (r "^0.2.0") (d #t) (k 2)))) (h "0xwibp8zs245mi04ry6xl5x7xg37ic4n9n15qdki18cggaa8b2kj")))

(define-public crate-sliding_features-0.5.3 (c (n "sliding_features") (v "0.5.3") (d (list (d (n "dyn-clone") (r "^1.0.3") (d #t) (k 0)) (d (n "plotters") (r "^0.3.0") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "round") (r "^0.1") (d #t) (k 2)) (d (n "rulinalg") (r "^0.4.2") (d #t) (k 0)) (d (n "time_series_generator") (r "^0.2.0") (d #t) (k 2)))) (h "0d4hppc27d0f46pib98l3k6ba0k5v6gi9dybh6a4cl87x90ggqv9")))

(define-public crate-sliding_features-0.5.4 (c (n "sliding_features") (v "0.5.4") (d (list (d (n "dyn-clone") (r "^1.0.3") (d #t) (k 0)) (d (n "plotters") (r "^0.3.0") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "round") (r "^0.1") (d #t) (k 2)) (d (n "rulinalg") (r "^0.4.2") (d #t) (k 0)) (d (n "time_series_generator") (r "^0.2.0") (d #t) (k 2)))) (h "10n4safhiybvbvlhrmx3w7dxvarqla5syhgvmb10zy7f1vxzmw7c")))

(define-public crate-sliding_features-0.5.5 (c (n "sliding_features") (v "0.5.5") (d (list (d (n "dyn-clone") (r "^1.0.3") (d #t) (k 0)) (d (n "plotters") (r "^0.3.0") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "round") (r "^0.1") (d #t) (k 2)) (d (n "rulinalg") (r "^0.4.2") (d #t) (k 0)) (d (n "time_series_generator") (r "^0.2.0") (d #t) (k 2)))) (h "1yg9nnjlvpzjkh50pbqd6b9ii0h2sa26wpw2bv7r7lzk40x5z6rr")))

(define-public crate-sliding_features-0.5.6 (c (n "sliding_features") (v "0.5.6") (d (list (d (n "dyn-clone") (r "^1.0.3") (d #t) (k 0)) (d (n "plotters") (r "^0.3.0") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "round") (r "^0.1") (d #t) (k 2)) (d (n "rulinalg") (r "^0.4.2") (d #t) (k 0)) (d (n "time_series_generator") (r "^0.2.0") (d #t) (k 2)))) (h "0fqcdk718qs31qks4nnj6rxgisqrz9r99di9wm6wfb274y2hwmdy")))

(define-public crate-sliding_features-0.6.0 (c (n "sliding_features") (v "0.6.0") (d (list (d (n "dyn-clone") (r "^1.0.3") (d #t) (k 0)) (d (n "plotters") (r "^0.3.0") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "round") (r "^0.1") (d #t) (k 2)) (d (n "rulinalg") (r "^0.4.2") (d #t) (k 0)) (d (n "time_series_generator") (r "^0.2.0") (d #t) (k 2)))) (h "1alb9sa5xdyqfxn8i8wx6n29h8ig9bfjd4wn9kzkw7d27xn48vzp")))

(define-public crate-sliding_features-0.6.1 (c (n "sliding_features") (v "0.6.1") (d (list (d (n "dyn-clone") (r "^1.0.3") (d #t) (k 0)) (d (n "plotters") (r "^0.3.0") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "round") (r "^0.1") (d #t) (k 2)) (d (n "rulinalg") (r "^0.4.2") (d #t) (k 0)) (d (n "time_series_generator") (r "^0.2.0") (d #t) (k 2)))) (h "07d2z2cbhq9wmhpwnd8q25p56aafgc0vdvndnkx3ch090zmhsyzq")))

(define-public crate-sliding_features-0.7.0 (c (n "sliding_features") (v "0.7.0") (d (list (d (n "dyn-clone") (r "^1.0.3") (d #t) (k 0)) (d (n "plotters") (r "^0.3.0") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "round") (r "^0.1") (d #t) (k 2)) (d (n "rulinalg") (r "^0.4.2") (d #t) (k 0)) (d (n "time_series_generator") (r "^0.2.0") (d #t) (k 2)))) (h "00k6rfnkb0ciccn9ii7v2ccyfmvj643zzjpp96gca3npzdpsx7ad")))

(define-public crate-sliding_features-0.7.1 (c (n "sliding_features") (v "0.7.1") (d (list (d (n "dyn-clone") (r "^1.0.3") (d #t) (k 0)) (d (n "plotters") (r "^0.3.0") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "round") (r "^0.1") (d #t) (k 2)) (d (n "rulinalg") (r "^0.4.2") (d #t) (k 0)) (d (n "time_series_generator") (r "^0.2.0") (d #t) (k 2)))) (h "13f4yaihrl9bx4mp473qzfrh0wjh3g3dgg3kpjgkf78qmkl1qw9l")))

(define-public crate-sliding_features-0.7.2 (c (n "sliding_features") (v "0.7.2") (d (list (d (n "dyn-clone") (r "^1.0.3") (d #t) (k 0)) (d (n "plotters") (r "^0.3.0") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "round") (r "^0.1") (d #t) (k 2)) (d (n "rulinalg") (r "^0.4.2") (d #t) (k 0)) (d (n "time_series_generator") (r "^0.2.0") (d #t) (k 2)))) (h "1i9srxs8c7mzkdcy2ya1596rvkidhs3cfxp6cddz3i9xamjs6pm3")))

(define-public crate-sliding_features-1.0.0 (c (n "sliding_features") (v "1.0.0") (d (list (d (n "dyn-clone") (r "^1.0.3") (d #t) (k 0)) (d (n "plotters") (r "^0.3.0") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "round") (r "^0.1") (d #t) (k 2)) (d (n "rulinalg") (r "^0.4.2") (d #t) (k 0)) (d (n "time_series_generator") (r "^0.2.0") (d #t) (k 2)))) (h "1fcx0c235hrgfl5dk6rh4af6fg1w2icifhw2nr4rd036pgl83iiv")))

(define-public crate-sliding_features-2.0.0 (c (n "sliding_features") (v "2.0.0") (d (list (d (n "dyn-clone") (r "^1.0.3") (d #t) (k 0)) (d (n "plotters") (r "^0.3.0") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "round") (r "^0.1") (d #t) (k 2)) (d (n "rulinalg") (r "^0.4.2") (d #t) (k 0)) (d (n "time_series_generator") (r "^0.2.0") (d #t) (k 2)))) (h "1gbq3vpnbzga4w1l7s0mi9alzcr61yrs5aas7rjj0dmpydxkv78n")))

(define-public crate-sliding_features-2.1.0 (c (n "sliding_features") (v "2.1.0") (d (list (d (n "dyn-clone") (r "^1.0.3") (d #t) (k 0)) (d (n "plotters") (r "^0.3.0") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "round") (r "^0.1") (d #t) (k 2)) (d (n "rulinalg") (r "^0.4.2") (d #t) (k 0)) (d (n "time_series_generator") (r "^0.2.0") (d #t) (k 2)))) (h "1l7gpr2dg22wmanq5plhs5568rhbnawh8ra45ahhgn3arvgb5h71")))

(define-public crate-sliding_features-2.2.0 (c (n "sliding_features") (v "2.2.0") (d (list (d (n "dyn-clone") (r "^1.0.3") (d #t) (k 0)) (d (n "plotters") (r "^0.3.0") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "round") (r "^0.1") (d #t) (k 2)) (d (n "rulinalg") (r "^0.4.2") (d #t) (k 0)) (d (n "time_series_generator") (r "^0.2.0") (d #t) (k 2)))) (h "0czq7516s9m414rc4l036b7i2bh2n1nxiwpqhzag9n4a8ps8vx5i")))

(define-public crate-sliding_features-2.3.0 (c (n "sliding_features") (v "2.3.0") (d (list (d (n "dyn-clone") (r "^1.0.3") (d #t) (k 0)) (d (n "plotters") (r "^0.3.0") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "round") (r "^0.1") (d #t) (k 2)) (d (n "rulinalg") (r "^0.4.2") (d #t) (k 0)) (d (n "time_series_generator") (r "^0.2.0") (d #t) (k 2)))) (h "0kk7c1b0i7dgbn06zah7sqhzxmvg0qx78f0hgxzjjkjfwcy2i9zb")))

(define-public crate-sliding_features-2.4.0 (c (n "sliding_features") (v "2.4.0") (d (list (d (n "dyn-clone") (r "^1.0.3") (d #t) (k 0)) (d (n "plotters") (r "^0.3.0") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "round") (r "^0.1") (d #t) (k 2)) (d (n "rulinalg") (r "^0.4.2") (d #t) (k 0)) (d (n "time_series_generator") (r "^0.2.0") (d #t) (k 2)))) (h "1llkwm9lgnb7w4ksisxj97l6lf9bq78m4yn9rzchkfs39fmjx9dl")))

(define-public crate-sliding_features-2.4.1 (c (n "sliding_features") (v "2.4.1") (d (list (d (n "dyn-clone") (r "^1.0.3") (d #t) (k 0)) (d (n "plotters") (r "^0.3.0") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "round") (r "^0.1") (d #t) (k 2)) (d (n "rulinalg") (r "^0.4.2") (d #t) (k 0)) (d (n "time_series_generator") (r "^0.2.0") (d #t) (k 2)))) (h "14vw2762c1jpmmc66g9mm2wzwqq2s0yr3qx8nxqvfkxv5piwgmw5")))

(define-public crate-sliding_features-2.5.0 (c (n "sliding_features") (v "2.5.0") (d (list (d (n "dyn-clone") (r "^1.0.3") (d #t) (k 0)) (d (n "plotters") (r "^0.3.0") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "round") (r "^0.1") (d #t) (k 2)) (d (n "rulinalg") (r "^0.4.2") (d #t) (k 0)) (d (n "time_series_generator") (r "^0.2.0") (d #t) (k 2)))) (h "1mkcfb046d2n6xpxgc7z0r2jp9dzvqk9wjw3r4iin5fccibdp28c")))

(define-public crate-sliding_features-2.5.1 (c (n "sliding_features") (v "2.5.1") (d (list (d (n "plotters") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "round") (r "^0.1") (d #t) (k 2)) (d (n "time_series_generator") (r "^0.3") (d #t) (k 2)))) (h "0k64g4sn3qqcd0r392cwpvvgmi5xfh8kfj8cbryzfd8pxayq2clx")))

(define-public crate-sliding_features-2.5.2 (c (n "sliding_features") (v "2.5.2") (d (list (d (n "plotters") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "round") (r "^0.1") (d #t) (k 2)) (d (n "time_series_generator") (r "^0.3") (d #t) (k 2)))) (h "0vja4d7zp217k52lj1k1mqcsf6rzq0s4wvlh8z46hqk21h78na8l")))

(define-public crate-sliding_features-2.5.3 (c (n "sliding_features") (v "2.5.3") (d (list (d (n "plotters") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "round") (r "^0.1") (d #t) (k 2)) (d (n "time_series_generator") (r "^0.3") (d #t) (k 2)))) (h "0c6bi2ly1rx6dhj73zglzy2pj268r6qd0nm9annzbgrgkz0kav90")))

