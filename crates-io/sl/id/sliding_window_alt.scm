(define-module (crates-io sl id sliding_window_alt) #:use-module (crates-io))

(define-public crate-sliding_window_alt-0.1.0 (c (n "sliding_window_alt") (v "0.1.0") (h "044y1sj4ix03v0wq7hr875jmlwbwg4i6q8548fmsafaxqkh394mw")))

(define-public crate-sliding_window_alt-0.1.1 (c (n "sliding_window_alt") (v "0.1.1") (d (list (d (n "circular-queue") (r "^0.2.6") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "queues") (r "^1.1.0") (d #t) (k 2)) (d (n "sliding_window") (r "^0.1.2") (d #t) (k 2)))) (h "1hx8vcdhli5g7b5w4di7rxgmchwih5kvwpfsx1a7rxy8km25d7rh")))

(define-public crate-sliding_window_alt-0.1.2 (c (n "sliding_window_alt") (v "0.1.2") (d (list (d (n "circular-queue") (r "^0.2.6") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "queues") (r "^1.1.0") (d #t) (k 2)) (d (n "sliding_window") (r "^0.1.2") (d #t) (k 2)))) (h "10pd4fb6grw6pbja5145g6xgg73gpzlan6xf7h2fpg5a4v4x3y21")))

