(define-module (crates-io sl id sliding_extrema) #:use-module (crates-io))

(define-public crate-sliding_extrema-0.1.0 (c (n "sliding_extrema") (v "0.1.0") (d (list (d (n "rand") (r "^0.4.2") (d #t) (k 2)))) (h "16lwv9grn8qv1nsayjkmwfps2kv98b393ahd4xw3w5phlvammh3b")))

(define-public crate-sliding_extrema-0.1.1 (c (n "sliding_extrema") (v "0.1.1") (d (list (d (n "rand") (r "^0.4.2") (d #t) (k 2)))) (h "04hqw8wnk3s84r0xf5vh34armdh38wy24xh9mfmkyr3d1ynlvvmp")))

(define-public crate-sliding_extrema-0.1.2 (c (n "sliding_extrema") (v "0.1.2") (d (list (d (n "rand") (r "^0.4.2") (d #t) (k 2)))) (h "06ssxhv626amxjjiqfc7il5h7b1wrlwmdhyjf95m9sqiaa1f2vgv")))

(define-public crate-sliding_extrema-0.1.3 (c (n "sliding_extrema") (v "0.1.3") (d (list (d (n "rand") (r "^0.4.2") (d #t) (k 2)))) (h "1zhjqckk82ni0zqp233wq54gyrbpimr00b7cgl91sikkqih2mvx5")))

(define-public crate-sliding_extrema-0.1.4 (c (n "sliding_extrema") (v "0.1.4") (d (list (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "166iiqyizg6m4zmn5fwbmdc494gjqxq8js2hi8r353yiavdc2v4a")))

(define-public crate-sliding_extrema-0.2.0 (c (n "sliding_extrema") (v "0.2.0") (d (list (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1651kiynfybhlbx39gh0ljx7fyk4im0di4x40c4an5zf2xhkc7rp")))

