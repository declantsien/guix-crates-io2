(define-module (crates-io sl id sliding_dft) #:use-module (crates-io))

(define-public crate-sliding_dft-1.0.0 (c (n "sliding_dft") (v "1.0.0") (d (list (d (n "num-complex") (r "^0.4.2") (f (quote ("libm"))) (k 0)) (d (n "num-traits") (r "^0.2.15") (f (quote ("libm"))) (k 0)))) (h "0hqq218hxicq9mc2qxghvnghw11kd2ni325dj0mc59bfcl30i6mn")))

