(define-module (crates-io sl id slidetown) #:use-module (crates-io))

(define-public crate-slidetown-0.1.0 (c (n "slidetown") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.43") (d #t) (k 0)) (d (n "binread") (r "^2.1.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 2)) (d (n "encoding_rs") (r "^0.8.28") (d #t) (k 0)) (d (n "image") (r "^0.23.14") (d #t) (k 2)) (d (n "imageproc") (r "^0.22.0") (d #t) (k 2)) (d (n "inflate") (r "^0.4.5") (d #t) (k 0)) (d (n "quick-xml") (r "^0.22.0") (f (quote ("encoding"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.127") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 2)))) (h "1qv5vp9gsbgc8x62h43gw587xy6f8hbw3v1z24ghwiryq0s9p77i")))

