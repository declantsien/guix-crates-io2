(define-module (crates-io sl id sliding_puzzle) #:use-module (crates-io))

(define-public crate-sliding_puzzle-1.0.0 (c (n "sliding_puzzle") (v "1.0.0") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "bit-set") (r "^0.4.0") (d #t) (k 0)) (d (n "lehmer") (r "^2.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.4.2") (d #t) (k 0)))) (h "1q3738dz8l3b7jgznxhij146d9zhiy85n1k9vqks2sb5jmxips2y")))

(define-public crate-sliding_puzzle-1.0.1 (c (n "sliding_puzzle") (v "1.0.1") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "bit-set") (r "^0.4.0") (d #t) (k 0)) (d (n "lehmer") (r "^2.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.4.2") (d #t) (k 0)))) (h "1n9mnq4i8il54p809dpzvvqp8r5k1vvji4bjp3ag6qx69wcpck14")))

