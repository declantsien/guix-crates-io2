(define-module (crates-io sl id slides) #:use-module (crates-io))

(define-public crate-slides-0.1.0 (c (n "slides") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.0") (d #t) (k 0)) (d (n "stdweb") (r "^0.4.10") (d #t) (k 0)) (d (n "stdweb-derive") (r "^0.5.0") (d #t) (k 0)) (d (n "yew") (r "^0.4.0") (d #t) (k 0)))) (h "04zsaz0vhhi975xzflk8r475446j0awz51nw00jvdd8fmynac0qa")))

(define-public crate-slides-0.1.1 (c (n "slides") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.0") (d #t) (k 0)) (d (n "stdweb") (r "^0.4.10") (d #t) (k 0)) (d (n "stdweb-derive") (r "^0.5.0") (d #t) (k 0)) (d (n "yew") (r "^0.4.0") (d #t) (k 0)))) (h "0d3wipb2y1j3jnc7191vhkgksf4kjvwvhqqsbml3y9amfgcqc46i")))

(define-public crate-slides-0.1.2 (c (n "slides") (v "0.1.2") (d (list (d (n "serde") (r "^1.0.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.0") (d #t) (k 0)) (d (n "stdweb") (r "^0.4.10") (d #t) (k 0)) (d (n "stdweb-derive") (r "^0.5.0") (d #t) (k 0)) (d (n "yew") (r "^0.4.0") (d #t) (k 0)))) (h "0p1si4d13ky23qa4xb25c8bi4880scipnr902y7q8jvlfmlnmjsy")))

(define-public crate-slides-0.1.3 (c (n "slides") (v "0.1.3") (d (list (d (n "serde") (r "^1.0.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.0") (d #t) (k 0)) (d (n "stdweb") (r "^0.4.10") (d #t) (k 0)) (d (n "stdweb-derive") (r "^0.5.0") (d #t) (k 0)) (d (n "yew") (r "^0.4.0") (d #t) (k 0)))) (h "1bg9db4r92475rmh5840g35islxpsfb3cmxh1s3v6msgicz88wv2")))

(define-public crate-slides-0.1.4 (c (n "slides") (v "0.1.4") (d (list (d (n "serde") (r "^1.0.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.0") (d #t) (k 0)) (d (n "stdweb") (r "^0.4.10") (d #t) (k 0)) (d (n "stdweb-derive") (r "^0.5.0") (d #t) (k 0)) (d (n "yew") (r "^0.4.0") (d #t) (k 0)))) (h "0dvjbfc3rh7r8fk65mqx7m62ivnrlv35h5ilh494y1kk4dx4blxj")))

(define-public crate-slides-0.1.5 (c (n "slides") (v "0.1.5") (d (list (d (n "serde") (r "^1.0.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.0") (d #t) (k 0)) (d (n "stdweb") (r "^0.4.10") (d #t) (k 0)) (d (n "stdweb-derive") (r "^0.5.0") (d #t) (k 0)) (d (n "yew") (r "^0.4.0") (d #t) (k 0)))) (h "06gn40zghad9lwmkisx4cr8sbxqa9c9hfdx94bslhvyksghalshd")))

