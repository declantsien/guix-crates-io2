(define-module (crates-io sl id slider) #:use-module (crates-io))

(define-public crate-slider-0.4.0 (c (n "slider") (v "0.4.0") (d (list (d (n "find_folder") (r "^0.3.0") (d #t) (k 0)) (d (n "gfx_device_gl") (r "^0.13.0") (d #t) (k 0)) (d (n "image") (r "^0.12.3") (d #t) (k 0)) (d (n "piston_window") (r "^0.64.0") (d #t) (k 0)) (d (n "rand") (r "^0.3.15") (d #t) (k 0)))) (h "0sh9s9yk9n07kpf36gm8g72cgd387w6dad8fmxks2hs1i22a4b0f")))

