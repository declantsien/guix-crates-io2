(define-module (crates-io sl id sliding_windows) #:use-module (crates-io))

(define-public crate-sliding_windows-1.0.0 (c (n "sliding_windows") (v "1.0.0") (h "17jnym8mrkdvcckw1m9ah3r6rz7f4b14zh68gyb76vzg0xabig4r")))

(define-public crate-sliding_windows-2.0.0 (c (n "sliding_windows") (v "2.0.0") (h "1yg85wd7zbqzf399ky5i6vm792wwlyfc9zbr32sfa0hwazm9bn4i")))

(define-public crate-sliding_windows-2.0.1 (c (n "sliding_windows") (v "2.0.1") (h "0nsbjx64q1gfpmjln93rwrcw06kad46n54bxcb65s1d7vmmrmdhn")))

(define-public crate-sliding_windows-2.0.2 (c (n "sliding_windows") (v "2.0.2") (h "09kykjzg9fiv6lnr8vki18lajqniwzlgnp832rgyliavdlx5r4ky")))

(define-public crate-sliding_windows-2.0.3 (c (n "sliding_windows") (v "2.0.3") (h "1pkn3i66pr62npzfqgwxw9fqh37w86fjmcbgy3dnsm8pbs1w9gpi")))

(define-public crate-sliding_windows-2.0.4 (c (n "sliding_windows") (v "2.0.4") (h "0gqyrmv6b8y5h2f04d2dzrvyx0bxhmyi8yvk8inkzkhclhz9pwhm")))

(define-public crate-sliding_windows-3.0.0 (c (n "sliding_windows") (v "3.0.0") (h "0pjzlp3lj01l757lik0q352vk5mfq5ldrv45mi5qhdff9pbyac58")))

