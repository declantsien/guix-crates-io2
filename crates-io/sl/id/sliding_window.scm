(define-module (crates-io sl id sliding_window) #:use-module (crates-io))

(define-public crate-sliding_window-0.1.0 (c (n "sliding_window") (v "0.1.0") (d (list (d (n "generic-array") (r "^0.14") (d #t) (k 0)))) (h "1jhwsbzd7sa35fx77zy4qfmk5z9s9lfg01msi6bj0gwlyixy6y37")))

(define-public crate-sliding_window-0.1.1 (c (n "sliding_window") (v "0.1.1") (d (list (d (n "generic-array") (r "^0.14") (d #t) (k 0)))) (h "1h62bi2kkp8569i27hnlfnpc4p2gh2gyzr0c7ca5vqin9ncm6vbh")))

(define-public crate-sliding_window-0.1.2 (c (n "sliding_window") (v "0.1.2") (d (list (d (n "generic-array") (r "^0.14") (d #t) (k 0)))) (h "122132i81mn4gl4pjvv2vklgdvm7ddmndjggvgddy5rb6yicxlxd")))

