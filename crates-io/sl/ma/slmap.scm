(define-module (crates-io sl ma slmap) #:use-module (crates-io))

(define-public crate-slmap-0.1.0 (c (n "slmap") (v "0.1.0") (d (list (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "whoami") (r "^1") (d #t) (k 0)))) (h "1ifv9ralgiki9q566l4vks82sbgjwn27f6vjs4w4h0bhb8dpvps9")))

(define-public crate-slmap-0.1.1 (c (n "slmap") (v "0.1.1") (d (list (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "03l2mlrdhf3423a0wc468vscdlf3r2jx3i5q6qld01d2csaiw662")))

(define-public crate-slmap-0.1.2 (c (n "slmap") (v "0.1.2") (d (list (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "05b6bhcy9d61zk3f77dhwqhjnnbi7y7rk06ifza8n2w0z4w4hd3l")))

(define-public crate-slmap-0.2.0 (c (n "slmap") (v "0.2.0") (d (list (d (n "clap") (r "^4.4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "shellexpand") (r "^3.1.0") (d #t) (k 0)) (d (n "toml") (r "^0.7.6") (d #t) (k 0)))) (h "1bblyiqvqy7z6q764rzcrx35nbk9fmrbwmhkjmp772v66f68lmza")))

(define-public crate-slmap-0.2.1 (c (n "slmap") (v "0.2.1") (d (list (d (n "clap") (r "^4.4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "shellexpand") (r "^3.1.0") (d #t) (k 0)) (d (n "toml") (r "^0.7.6") (d #t) (k 0)))) (h "017sbgzrvi11dgi7y4brxya7n030g51d1s5q9m0jl6cvzp62zf9f")))

