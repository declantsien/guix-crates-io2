(define-module (crates-io sl ei sleigh2macro) #:use-module (crates-io))

(define-public crate-sleigh2macro-0.1.0 (c (n "sleigh2macro") (v "0.1.0") (d (list (d (n "litrs") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "sleigh2rust") (r "^0.1") (d #t) (k 0)))) (h "1a1mwy9y9sw28dk62bfhbbv6sbpk98c5yfvragpcj01f4f92n3a6")))

(define-public crate-sleigh2macro-0.1.1 (c (n "sleigh2macro") (v "0.1.1") (d (list (d (n "litrs") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "sleigh2rust") (r "^0.1") (d #t) (k 0)))) (h "0w4ki1hv6mhvcj368z0wvbf1zivqwjyaxsalzadriw8y7bxwwazb")))

(define-public crate-sleigh2macro-0.1.2 (c (n "sleigh2macro") (v "0.1.2") (d (list (d (n "litrs") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "sleigh2rust") (r "^0.1.3") (d #t) (k 0)))) (h "0jcx3a4hdl5r82l5abk3ajs95c7bhmm90xsai464byzzfvx1sswm")))

(define-public crate-sleigh2macro-0.1.3 (c (n "sleigh2macro") (v "0.1.3") (d (list (d (n "litrs") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "sleigh2rust") (r "^0.1.4") (d #t) (k 0)))) (h "0h25vs21mf11pkvifcgmjjd9d7lkmriyy6p34r7mvbzj3jlyhgpf")))

(define-public crate-sleigh2macro-0.1.4 (c (n "sleigh2macro") (v "0.1.4") (d (list (d (n "litrs") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "sleigh2rust") (r "^0.1.5") (d #t) (k 0)))) (h "1yp73120pkxaix7y12qkn1w5yddfmhmpqpcyqhphm5g90wd6cvz7")))

(define-public crate-sleigh2macro-0.1.5 (c (n "sleigh2macro") (v "0.1.5") (d (list (d (n "litrs") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "sleigh2rust") (r "^0.1.6") (d #t) (k 0)))) (h "0d1mjpd4g73r06v236dli1kk95g6j989l4lqginrdar2d2wwx12k")))

