(define-module (crates-io sl ei sleigh2rust) #:use-module (crates-io))

(define-public crate-sleigh2rust-0.1.0 (c (n "sleigh2rust") (v "0.1.0") (d (list (d (n "bitvec") (r "^1") (d #t) (k 0)) (d (n "ethnum") (r "^1") (d #t) (k 0)) (d (n "indexmap") (r "^1.9") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "sleigh-rs") (r "^0.1") (d #t) (k 0)) (d (n "sleigh4rust") (r "^0.1") (d #t) (k 0)))) (h "086bpqyqjpygfk4lajamkb1dzbysrlm7m5mnc661a95n7g6awv0f")))

(define-public crate-sleigh2rust-0.1.1 (c (n "sleigh2rust") (v "0.1.1") (d (list (d (n "bitvec") (r "^1") (d #t) (k 0)) (d (n "ethnum") (r "^1") (d #t) (k 0)) (d (n "indexmap") (r "^1.9") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "sleigh-rs") (r "^0.1") (d #t) (k 0)) (d (n "sleigh4rust") (r "^0.1") (d #t) (k 0)))) (h "05cxmjarj101dy23p34ymwd6mqfzjpdj2pzmpbs75s7i5q0c3z78")))

(define-public crate-sleigh2rust-0.1.2 (c (n "sleigh2rust") (v "0.1.2") (d (list (d (n "bitvec") (r "^1") (d #t) (k 0)) (d (n "ethnum") (r "^1") (d #t) (k 0)) (d (n "indexmap") (r "^1.9") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "sleigh-rs") (r "^0.1") (d #t) (k 0)) (d (n "sleigh4rust") (r "^0.1") (d #t) (k 0)))) (h "015q8kb2bdzdxrriwri3jjny5qgwzdf7702j3zwqjqcdxrm1ny01")))

(define-public crate-sleigh2rust-0.1.3 (c (n "sleigh2rust") (v "0.1.3") (d (list (d (n "bitvec") (r "^1") (d #t) (k 0)) (d (n "ethnum") (r "^1") (d #t) (k 0)) (d (n "indexmap") (r "^1.9") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "sleigh-rs") (r "^0.1.4") (d #t) (k 0)))) (h "1z9mj4pjslzrf2mycimh22nm20p4123yzkr48zyqka2gh5affqwf")))

(define-public crate-sleigh2rust-0.1.4 (c (n "sleigh2rust") (v "0.1.4") (d (list (d (n "bitvec") (r "^1") (d #t) (k 0)) (d (n "ethnum") (r "^1") (d #t) (k 0)) (d (n "indexmap") (r "^1.9") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "sleigh-rs") (r "^0.1.4") (d #t) (k 0)))) (h "0nzs9j9f26yrzkkx29jbxfn61k96282272as8s3gs5i5fzcqn0zz")))

(define-public crate-sleigh2rust-0.1.5 (c (n "sleigh2rust") (v "0.1.5") (d (list (d (n "bitvec") (r "^1") (d #t) (k 0)) (d (n "ethnum") (r "^1") (d #t) (k 0)) (d (n "indexmap") (r "^1.9") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "sleigh-rs") (r "^0.1.4") (d #t) (k 0)))) (h "1p83q4rfzn226l543s2bhavskznv4b2bxd35g055yy1l91g5wifi")))

(define-public crate-sleigh2rust-0.1.6 (c (n "sleigh2rust") (v "0.1.6") (d (list (d (n "bitvec") (r "^1") (d #t) (k 0)) (d (n "ethnum") (r "^1") (d #t) (k 0)) (d (n "indexmap") (r "^1.9") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "sleigh-rs") (r "^0.1.5") (d #t) (k 0)))) (h "0648p2p9q2wmf6kvjly56zxk1shzw9fzcj7i05vgp1z7pynhwhyi")))

