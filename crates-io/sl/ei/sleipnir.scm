(define-module (crates-io sl ei sleipnir) #:use-module (crates-io))

(define-public crate-sleipnir-0.0.1 (c (n "sleipnir") (v "0.0.1") (d (list (d (n "kurbo") (r "^0.10.4") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "skrifa") (r "^0.17.0") (d #t) (k 0)) (d (n "smol_str") (r "^0.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)) (d (n "write-fonts") (r "^0.17.0") (d #t) (k 0)))) (h "1nrvgvp73zzmj2j760xq8hiwspgisq0qfsn072xybdy426shixws")))

(define-public crate-sleipnir-0.1.0 (c (n "sleipnir") (v "0.1.0") (d (list (d (n "kurbo") (r "^0.10.4") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "skrifa") (r "^0.17.0") (d #t) (k 0)) (d (n "smol_str") (r "^0.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)) (d (n "write-fonts") (r "^0.17.0") (d #t) (k 0)))) (h "0r14qkqqml5gmia36hv4r9djs2z42br9wll55p6w5z5gsmibfwng")))

(define-public crate-sleipnir-0.1.1 (c (n "sleipnir") (v "0.1.1") (d (list (d (n "kurbo") (r "^0.10.4") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "skrifa") (r "^0.18.0") (d #t) (k 0)) (d (n "smol_str") (r "^0.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)))) (h "0sxw3f0cmj689x61k5dwng3r883ay54crblfvabx2mp2slx2zzdn")))

(define-public crate-sleipnir-0.1.2 (c (n "sleipnir") (v "0.1.2") (d (list (d (n "kurbo") (r "^0.11.0") (d #t) (k 0)) (d (n "skrifa") (r "^0.18.0") (d #t) (k 0)) (d (n "smol_str") (r "^0.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)))) (h "0v3kbvsf51b1b272hknw6qw0zkfbsk7zxg59hn6nv5kbrvc6fas5")))

(define-public crate-sleipnir-0.1.3 (c (n "sleipnir") (v "0.1.3") (d (list (d (n "kurbo") (r "^0.11.0") (d #t) (k 0)) (d (n "skrifa") (r "^0.18.0") (d #t) (k 0)) (d (n "smol_str") (r "^0.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)))) (h "1csdzqcz7r6f3z9iasfh4jacx7vx0aaf1yz0a4qzvk6agykbn48y")))

(define-public crate-sleipnir-0.1.4 (c (n "sleipnir") (v "0.1.4") (d (list (d (n "kurbo") (r "^0.11.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4") (d #t) (k 2)) (d (n "regex") (r "^1.10.4") (d #t) (k 2)) (d (n "skrifa") (r "^0.19.1") (d #t) (k 0)) (d (n "smol_str") (r "^0.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)))) (h "1pi3ihs6z9ij86g5mlvb2wns5ah4i246j8qq7r6v26x1wfh4v151")))

(define-public crate-sleipnir-0.1.5 (c (n "sleipnir") (v "0.1.5") (d (list (d (n "kurbo") (r "^0.11.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4") (d #t) (k 2)) (d (n "regex") (r "^1.10.4") (d #t) (k 2)) (d (n "skrifa") (r "^0.19.1") (d #t) (k 0)) (d (n "smol_str") (r "^0.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)))) (h "0gm5k76klrb7g3jm5a7ap2m63wpz0vhrw9ig5g9rf92rmmikcj76")))

(define-public crate-sleipnir-0.1.6 (c (n "sleipnir") (v "0.1.6") (d (list (d (n "kurbo") (r "^0.11.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4") (d #t) (k 2)) (d (n "regex") (r "^1.10.4") (d #t) (k 2)) (d (n "skrifa") (r "^0.19.1") (d #t) (k 0)) (d (n "smol_str") (r "^0.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)))) (h "1psbn15hrmp2bv2s7qc0d7kkkl5c5qr2ph2h4k68qgx5pqf1lqbn")))

(define-public crate-sleipnir-0.1.7 (c (n "sleipnir") (v "0.1.7") (d (list (d (n "kurbo") (r "^0.11.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4") (d #t) (k 2)) (d (n "regex") (r "^1.10.4") (d #t) (k 2)) (d (n "skrifa") (r "^0.19.1") (d #t) (k 0)) (d (n "smol_str") (r "^0.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)))) (h "1ia7yjrjvs1r46yv2fbwkk5his1vlqpglwxw15xm0q1hm39ccg2l")))

(define-public crate-sleipnir-0.1.8 (c (n "sleipnir") (v "0.1.8") (d (list (d (n "kurbo") (r "^0.11.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4") (d #t) (k 2)) (d (n "regex") (r "^1.10.4") (d #t) (k 2)) (d (n "skrifa") (r "^0.19.1") (d #t) (k 0)) (d (n "smol_str") (r "^0.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)))) (h "04zx288xqdjra3l313zpfd2i9f357a043jykik2m1cqjr5k1yaws")))

(define-public crate-sleipnir-0.1.9 (c (n "sleipnir") (v "0.1.9") (d (list (d (n "kurbo") (r "^0.11.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4") (d #t) (k 2)) (d (n "regex") (r "^1.10.4") (d #t) (k 2)) (d (n "skrifa") (r "^0.19.1") (d #t) (k 0)) (d (n "smol_str") (r "^0.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)))) (h "0fg53arqn97cr0084ci7wcknrmc6z1ggdh0xf9h49hppihh0nczx")))

