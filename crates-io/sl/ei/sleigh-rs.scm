(define-module (crates-io sl ei sleigh-rs) #:use-module (crates-io))

(define-public crate-sleigh-rs-0.1.0 (c (n "sleigh-rs") (v "0.1.0") (d (list (d (n "indexmap") (r "^1.9") (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "sleigh4rust") (r "^0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "06waz35r12czwa936hfijgv0ki31qrj68cxwwm606scsya4da81z")))

(define-public crate-sleigh-rs-0.1.1 (c (n "sleigh-rs") (v "0.1.1") (d (list (d (n "indexmap") (r "^1.9") (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "sleigh4rust") (r "^0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0gp4jb3870qacrwyb9ld3p00f5sbzigs564ybgpcs5rf4n65jd85")))

(define-public crate-sleigh-rs-0.1.2 (c (n "sleigh-rs") (v "0.1.2") (d (list (d (n "indexmap") (r "^1.9") (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "sleigh4rust") (r "^0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1sgad6al0rgxf8y0kbjdr3yz29frkjrybzf23ij27c9kp9biz3yf")))

(define-public crate-sleigh-rs-0.1.3 (c (n "sleigh-rs") (v "0.1.3") (d (list (d (n "indexmap") (r "^1.9") (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "sleigh4rust") (r "^0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0hfpjhg00vi54zmxc2yvsaqm4zkzi8isqf3jsbkgf0frljz8354q")))

(define-public crate-sleigh-rs-0.1.4 (c (n "sleigh-rs") (v "0.1.4") (d (list (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1ixfha85k88ihfzcbqz6p643qaqrhznlka1mr19gdjrqcbm5cjna")))

(define-public crate-sleigh-rs-0.1.5 (c (n "sleigh-rs") (v "0.1.5") (d (list (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "10kb2li0skckhd144br4zfjlwimpqvlly64iw8b40swld3lvhq7f")))

