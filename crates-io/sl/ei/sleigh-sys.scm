(define-module (crates-io sl ei sleigh-sys) #:use-module (crates-io))

(define-public crate-sleigh-sys-0.1.0 (c (n "sleigh-sys") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1a9qindasb5f9zkq3bl4hkrvimby3gf78drfiq1xmy0jfvzp9k9w")))

