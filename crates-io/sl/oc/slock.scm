(define-module (crates-io sl oc slock) #:use-module (crates-io))

(define-public crate-slock-0.1.0 (c (n "slock") (v "0.1.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 2)))) (h "1ww6nq9rx1x5fqvjj7d1p92c30phpfpvmk8ql54n65zh9fqph0s2")))

(define-public crate-slock-0.1.1 (c (n "slock") (v "0.1.1") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 2)))) (h "0mw0n14jzmmaa5di1dqghq0p533rn1dj74rzdlb71jz4qgg2p67m")))

(define-public crate-slock-0.1.2 (c (n "slock") (v "0.1.2") (d (list (d (n "async-std") (r "^1.6") (d #t) (k 0)) (d (n "futures") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 2)))) (h "0c2d6yb0nnzmv2jfr3xh327hncc8acyx5afqpk3ac38r1bbhn3g6") (f (quote (("default" "futures" "blocking") ("blocking" "futures"))))))

(define-public crate-slock-0.2.0 (c (n "slock") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "tokio") (r "^1.22") (f (quote ("sync" "time" "rt" "macros"))) (d #t) (k 0)))) (h "0jhqqqq191imjk4sidr4nzpbscmbd8lqjfklih25m3igx4fa8riv")))

(define-public crate-slock-0.2.1 (c (n "slock") (v "0.2.1") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "tokio") (r "^1.22") (f (quote ("sync" "time" "rt" "macros"))) (d #t) (k 0)))) (h "1shj7a0ynlqfzsjdp2k7w1gvfhyjj2nzz3wpjx7x7992ccbzv6xp")))

