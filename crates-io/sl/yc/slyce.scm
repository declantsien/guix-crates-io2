(define-module (crates-io sl yc slyce) #:use-module (crates-io))

(define-public crate-slyce-0.1.0 (c (n "slyce") (v "0.1.0") (h "030qc82b9disfagwc4wx133pcd1xkrwax3ifdn4pxw3zy7djwpfv")))

(define-public crate-slyce-0.1.1 (c (n "slyce") (v "0.1.1") (h "11ha3c9dpds67c2nhkd67xg6yvfiazdgpsvyb4hwfv04ji02h8xw")))

(define-public crate-slyce-0.1.2 (c (n "slyce") (v "0.1.2") (h "0aqadlcrhjqzfvcf44fv5yd98grh58lnapk6cwaxf7ahnwzqqaqw")))

(define-public crate-slyce-0.1.3 (c (n "slyce") (v "0.1.3") (h "12k51a49asr5n91kfn0rrvvr93k7v2zdkzhckg8kmyv1f5gw1vcc")))

(define-public crate-slyce-0.1.4 (c (n "slyce") (v "0.1.4") (h "0wbfckz63s6hff7nhb37gng7wx6l8h3sfmja6h9rl9gzjq3jp2i8")))

(define-public crate-slyce-0.1.5 (c (n "slyce") (v "0.1.5") (h "0f01xnc0ciywh520dmj0mr49310s7pkfpjpwv3zrnvxnx7pan2s2")))

(define-public crate-slyce-0.1.6 (c (n "slyce") (v "0.1.6") (h "08fgkiw0904f410rbn575y69ci9yf6sb3nzlb49bsi7234vyww85")))

(define-public crate-slyce-0.2.0 (c (n "slyce") (v "0.2.0") (h "1kasm9m0l5n9gdd0icrgx5jijzbxx30jy4195dixfqf5sqikf2g3")))

(define-public crate-slyce-0.2.1 (c (n "slyce") (v "0.2.1") (h "054s300k317s1b7asf6r4vlk77dajrc306abmrfj2wzzg6i9jznm")))

(define-public crate-slyce-0.3.0 (c (n "slyce") (v "0.3.0") (d (list (d (n "arbitrary") (r "^0.4.7") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0qqf4rlv9qb6lbj0prxl1sars41wpb8hgvywsx8nm3maczbhh35d")))

(define-public crate-slyce-0.3.1 (c (n "slyce") (v "0.3.1") (d (list (d (n "arbitrary") (r "^0.4.7") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0aa792p5k0h37mcsmkzrdk4rcwxxvljaj10x7zciif3xndkiqv84")))

