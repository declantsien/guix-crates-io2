(define-module (crates-io sl ip slippy_map_tilenames) #:use-module (crates-io))

(define-public crate-slippy_map_tilenames-0.1.0 (c (n "slippy_map_tilenames") (v "0.1.0") (h "0kxkhkvas47dmsbf93dg2jymmydxdjz91ki58r13jzz299y5wpmr")))

(define-public crate-slippy_map_tilenames-0.2.0 (c (n "slippy_map_tilenames") (v "0.2.0") (h "1mn1j1wlvgi0w2v2iz98k4cp0navzy103l528jc97wvg4ywggcwj")))

