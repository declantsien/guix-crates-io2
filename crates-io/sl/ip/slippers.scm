(define-module (crates-io sl ip slippers) #:use-module (crates-io))

(define-public crate-slippers-0.1.0 (c (n "slippers") (v "0.1.0") (d (list (d (n "hexlit") (r "^0.5.3") (d #t) (k 2)))) (h "1hwic4lqj736i4ll7d3vpi0cf0d62h09f4x39fz09j9df1nldwd0") (f (quote (("default"))))))

(define-public crate-slippers-0.1.1 (c (n "slippers") (v "0.1.1") (d (list (d (n "hexlit") (r "^0.5.3") (d #t) (k 2)))) (h "0kp28f7lbz0d2cnd85hzsgwaiwmj398akfz1g1q069nsh4cq0yh1") (f (quote (("default"))))))

(define-public crate-slippers-0.1.2 (c (n "slippers") (v "0.1.2") (d (list (d (n "hexlit") (r "^0.5.3") (d #t) (k 2)))) (h "0d14ymixvnay4p02czrrpaahmpxvawf6a5vg99xy41mkz1m6q574") (f (quote (("default"))))))

(define-public crate-slippers-0.1.3 (c (n "slippers") (v "0.1.3") (d (list (d (n "hexlit") (r "^0.5.3") (d #t) (k 2)))) (h "07fzlv1sfgx66mza6gvyasp912ljhm40c3a9yz66vzqs4dacb1vx") (f (quote (("default"))))))

