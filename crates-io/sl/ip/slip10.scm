(define-module (crates-io sl ip slip10) #:use-module (crates-io))

(define-public crate-slip10-0.0.1 (c (n "slip10") (v "0.0.1") (h "1wnmh6gddbmlhpjmx2ybvfwkc8jd8hrrfcggwf13p6dw24npw3xh")))

(define-public crate-slip10-0.1.0 (c (n "slip10") (v "0.1.0") (d (list (d (n "ed25519-dalek") (r "^1.0.0-pre.4") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 2)) (d (n "hmac") (r "^0.9") (d #t) (k 0)) (d (n "sha2") (r "^0.9") (d #t) (k 0)))) (h "1abjm2jggn13245ji6mp3rxg315874gi89p4vhgkrmaqnw3kf0hk")))

(define-public crate-slip10-0.2.0 (c (n "slip10") (v "0.2.0") (d (list (d (n "ed25519-dalek") (r "^1.0.0-pre.4") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 2)) (d (n "hmac") (r "^0.9") (d #t) (k 0)) (d (n "sha2") (r "^0.9") (d #t) (k 0)))) (h "0laxjwh5v7q3mj2m7ybkx4dcj59s1liqdb08vycxxq0rx86wjqnw")))

(define-public crate-slip10-0.3.0 (c (n "slip10") (v "0.3.0") (d (list (d (n "ed25519-dalek") (r "^1.0.0-pre.4") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 2)) (d (n "hmac") (r "^0.9") (d #t) (k 0)) (d (n "sha2") (r "^0.9") (d #t) (k 0)))) (h "18nv5s7630hf940dwhi7ha3clkjk33cfhfvzssx8yik7wvcxihjv")))

(define-public crate-slip10-0.3.1 (c (n "slip10") (v "0.3.1") (d (list (d (n "ed25519-dalek") (r "^1.0.0-pre.4") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 2)) (d (n "hmac") (r "^0.9") (d #t) (k 0)) (d (n "sha2") (r "^0.9") (d #t) (k 0)))) (h "0kqhi5wjar2allg6qxc8vbb4vpizhq730qkd8rih48yfy8x6cssk")))

(define-public crate-slip10-0.4.0 (c (n "slip10") (v "0.4.0") (d (list (d (n "ed25519-dalek") (r "^1.0.0-pre.4") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 2)) (d (n "hmac") (r "^0.9") (d #t) (k 0)) (d (n "sha2") (r "^0.9") (d #t) (k 0)))) (h "1mxqdah2004n94j27kh4m80rghdnwfpr62j02i8bffrx180z0g75")))

(define-public crate-slip10-0.4.1 (c (n "slip10") (v "0.4.1") (d (list (d (n "ed25519-dalek") (r "^1.0.0-pre.4") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 2)) (d (n "hmac") (r "^0.9") (d #t) (k 0)) (d (n "sha2") (r "^0.9") (d #t) (k 0)))) (h "06zjs32fk7rqkpy503gpxrr9vbzahcw92b8zl8f88ijybq2yq033")))

(define-public crate-slip10-0.4.2 (c (n "slip10") (v "0.4.2") (d (list (d (n "ed25519-dalek") (r "^1.0") (f (quote ("u64_backend"))) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 2)) (d (n "hmac") (r "^0.9") (k 0)) (d (n "sha2") (r "^0.9") (k 0)))) (h "1a52jqfp9isnsrc0sws9fzr5ykamx2smkpf8sk8qgybny5g6lq00")))

(define-public crate-slip10-0.4.3 (c (n "slip10") (v "0.4.3") (d (list (d (n "ed25519-dalek") (r "^1.0") (f (quote ("u64_backend"))) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 2)) (d (n "hmac") (r "^0.9") (k 0)) (d (n "sha2") (r "^0.9") (k 0)))) (h "1g567b98jzx50f1mjlx3wscsmwx67m49222qbh8wpc3hdxp4lwi8")))

