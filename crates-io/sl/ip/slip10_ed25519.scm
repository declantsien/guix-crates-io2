(define-module (crates-io sl ip slip10_ed25519) #:use-module (crates-io))

(define-public crate-slip10_ed25519-0.1.0 (c (n "slip10_ed25519") (v "0.1.0") (d (list (d (n "bitcoin_hashes") (r "^0.7.6") (d #t) (k 0)) (d (n "hex") (r "^0.4.2") (d #t) (k 2)) (d (n "hmac-sha512") (r "^0.1.8") (d #t) (k 0)))) (h "1afxxkvj2as98jlms0w2k06k8dx2cvacsqjsa9jclfnpyggb6nxv")))

(define-public crate-slip10_ed25519-0.1.1 (c (n "slip10_ed25519") (v "0.1.1") (d (list (d (n "bitcoin_hashes") (r "^0.7.6") (d #t) (k 0)) (d (n "hex") (r "^0.4.2") (d #t) (k 2)) (d (n "hmac-sha512") (r "^0.1.8") (d #t) (k 0)))) (h "0zdzqf5gfb1iyas11i2nhazb5vmizmvd34zzyhcmq00cpv1f6y5b")))

(define-public crate-slip10_ed25519-0.1.2 (c (n "slip10_ed25519") (v "0.1.2") (d (list (d (n "bitcoin_hashes") (r "^0.7.6") (d #t) (k 0)) (d (n "hex") (r "^0.4.2") (d #t) (k 2)) (d (n "hmac-sha512") (r "^0.1.8") (d #t) (k 0)))) (h "04lqn5yg49csfmf4cvvrrj2cpvxvpvwyvm7wf40hq7mdgyb01wki")))

(define-public crate-slip10_ed25519-0.1.3 (c (n "slip10_ed25519") (v "0.1.3") (d (list (d (n "hex") (r "^0.4.2") (d #t) (k 2)) (d (n "hmac-sha512") (r "^0.1.8") (d #t) (k 0)))) (h "16nwm67ng8vrfpf2iicfg6nkbx54hx70hs916h563y8lpwlgzq2b")))

