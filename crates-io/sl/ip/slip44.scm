(define-module (crates-io sl ip slip44) #:use-module (crates-io))

(define-public crate-slip44-0.1.0 (c (n "slip44") (v "0.1.0") (d (list (d (n "itertools") (r "^0.10.1") (o #t) (d #t) (k 0)) (d (n "reqwest") (r "^0.11.4") (f (quote ("blocking"))) (o #t) (d #t) (k 0)))) (h "15qqhfn8bgmirqri4526rk7f5917i0bnf1py157ii9xvmyyz2s66") (f (quote (("parse-coins" "itertools" "reqwest"))))))

(define-public crate-slip44-0.1.1 (c (n "slip44") (v "0.1.1") (d (list (d (n "itertools") (r "^0.10.1") (o #t) (d #t) (k 0)) (d (n "reqwest") (r "^0.11.4") (f (quote ("blocking"))) (o #t) (d #t) (k 0)))) (h "06xh5k4c82z7skbbvqv5z44v7ab82sg1jmi17ci9ywmz73z3mcmb") (f (quote (("parse-coins" "itertools" "reqwest"))))))

(define-public crate-slip44-0.1.2 (c (n "slip44") (v "0.1.2") (d (list (d (n "itertools") (r "^0.10.1") (o #t) (d #t) (k 0)) (d (n "reqwest") (r "^0.11.4") (f (quote ("blocking"))) (o #t) (d #t) (k 0)))) (h "0xwq9jyj8znblqggrkzwl24hj3bxpdn3l43j5bpmc0irg8lsspdp") (f (quote (("parse-coins" "itertools" "reqwest"))))))

(define-public crate-slip44-0.1.3 (c (n "slip44") (v "0.1.3") (d (list (d (n "itertools") (r "^0.10.1") (o #t) (d #t) (k 0)) (d (n "reqwest") (r "^0.11.4") (f (quote ("blocking"))) (o #t) (d #t) (k 0)))) (h "0mpqlyb1f00ika8sqv3i8lynd509gkd8x08gm899d1nx0z29ls82") (f (quote (("parse-coins" "itertools" "reqwest"))))))

(define-public crate-slip44-0.1.4 (c (n "slip44") (v "0.1.4") (d (list (d (n "itertools") (r "^0.10.1") (o #t) (d #t) (k 0)) (d (n "reqwest") (r "^0.11.4") (f (quote ("blocking"))) (o #t) (d #t) (k 0)))) (h "1mpi34iyqs1hklpzzi9xvq5x78w01hxqfs02v8gj2kpsjxpa0yir") (f (quote (("parse-coins" "itertools" "reqwest"))))))

