(define-module (crates-io sl ip slippy-map-tiles) #:use-module (crates-io))

(define-public crate-slippy-map-tiles-0.1.0 (c (n "slippy-map-tiles") (v "0.1.0") (h "0khilaz7k9i6334p9xns01ncxgr3g35scdzljkq44k3gsx0m4sfh")))

(define-public crate-slippy-map-tiles-0.3.0 (c (n "slippy-map-tiles") (v "0.3.0") (d (list (d (n "regex") (r "^0.1.55") (d #t) (k 0)))) (h "1ylsw27mb5rp1rsbr0549cch0gdlc0klamlzj8v5yxsvvxnhd5qa")))

(define-public crate-slippy-map-tiles-0.4.0 (c (n "slippy-map-tiles") (v "0.4.0") (d (list (d (n "regex") (r "^0.1.55") (d #t) (k 0)))) (h "1kadwycyqh6va5s3q230cwz5p3fph6kg739qy3viaxsvbwr3b04r")))

(define-public crate-slippy-map-tiles-0.5.0 (c (n "slippy-map-tiles") (v "0.5.0") (d (list (d (n "lazy_static") (r "^0.1.15") (d #t) (k 0)) (d (n "regex") (r "^0.1.55") (d #t) (k 0)))) (h "158jnjbrlqybnil1j79yrzisw6nhmi7ml0cvk4nyg2i935h5kk1h")))

(define-public crate-slippy-map-tiles-0.7.0 (c (n "slippy-map-tiles") (v "0.7.0") (d (list (d (n "lazy_static") (r "^0.1.15") (d #t) (k 0)) (d (n "regex") (r "^0.1.55") (d #t) (k 0)))) (h "1pwrzg4lfgcsa5943fdn21g3njagd9mzy0ci4lpyl5wv56jigpay")))

(define-public crate-slippy-map-tiles-0.8.0 (c (n "slippy-map-tiles") (v "0.8.0") (d (list (d (n "lazy_static") (r "^0.1.15") (d #t) (k 0)) (d (n "regex") (r "^0.1.55") (d #t) (k 0)))) (h "0w0lf0jb77lzrbk5lqz0ca1agn11kxx6bpdgr6n2zfc34qdn5ykc")))

(define-public crate-slippy-map-tiles-0.9.0 (c (n "slippy-map-tiles") (v "0.9.0") (d (list (d (n "lazy_static") (r "^0.1.15") (d #t) (k 0)) (d (n "regex") (r "^0.1.55") (d #t) (k 0)))) (h "151pgd8dv4s4f3kdcgz9fbxkyqrymc29fmx3q9yg8hsqgfdwwiwx")))

(define-public crate-slippy-map-tiles-0.10.0 (c (n "slippy-map-tiles") (v "0.10.0") (d (list (d (n "lazy_static") (r "^0.1.15") (d #t) (k 0)) (d (n "regex") (r "^0.1.55") (d #t) (k 0)))) (h "1rl8qm2zjqiyqyik4r35rhapp4xnyg6hsc597fmij7k1c4703mlf")))

(define-public crate-slippy-map-tiles-0.11.0 (c (n "slippy-map-tiles") (v "0.11.0") (d (list (d (n "lazy_static") (r "^0.1.15") (d #t) (k 0)) (d (n "regex") (r "^0.1.55") (d #t) (k 0)))) (h "0szy2jz7wm9fw5i42w6nw0931h8542r1z1ksbyklbbyi2y46rf7b")))

(define-public crate-slippy-map-tiles-0.12.0 (c (n "slippy-map-tiles") (v "0.12.0") (d (list (d (n "lazy_static") (r "^0.1.15") (d #t) (k 0)) (d (n "regex") (r "^0.1.55") (d #t) (k 0)))) (h "1zlf5m8f7va3jawvf3mbpqimpx62pl9vs9mr3ks1gssmyrwhamvd")))

(define-public crate-slippy-map-tiles-0.13.0 (c (n "slippy-map-tiles") (v "0.13.0") (d (list (d (n "lazy_static") (r "^0.1.15") (d #t) (k 0)) (d (n "regex") (r "^0.1.55") (d #t) (k 0)))) (h "01hsn8x3nry82gcr9ggl9gc37n3w6p1c28bibmw3070qds8i4w06")))

(define-public crate-slippy-map-tiles-0.14.0 (c (n "slippy-map-tiles") (v "0.14.0") (d (list (d (n "lazy_static") (r "^0.1.15") (d #t) (k 0)) (d (n "regex") (r "^0.1.55") (d #t) (k 0)))) (h "176sgk9q54nv7rw618f046qfbhyrwsfclx6nybzskf25pnmpzshg")))

(define-public crate-slippy-map-tiles-0.13.1 (c (n "slippy-map-tiles") (v "0.13.1") (d (list (d (n "lazy_static") (r "^0.1.15") (d #t) (k 0)) (d (n "regex") (r "^0.1.55") (d #t) (k 0)))) (h "0xp77bz3nl1lh0dd1v7jrbilmncflhgnc2fdka5b23ldsqarkkap")))

(define-public crate-slippy-map-tiles-0.15.0 (c (n "slippy-map-tiles") (v "0.15.0") (d (list (d (n "lazy_static") (r "^0.1.15") (d #t) (k 0)) (d (n "regex") (r "^0.1.55") (d #t) (k 0)) (d (n "world_image_file") (r "^0.1") (o #t) (d #t) (k 0)))) (h "13syjhh6p84bm078nj7rvf3irla9n1mnry3lhgwlkj6dn6h57my7") (f (quote (("world_file" "world_image_file"))))))

(define-public crate-slippy-map-tiles-0.16.0 (c (n "slippy-map-tiles") (v "0.16.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 0)) (d (n "world_image_file") (r "^0.1") (o #t) (d #t) (k 0)))) (h "16bhy2q9ab9dr3hzxaigr8gns4hwikq198b81vn84n1nws0vp83w") (f (quote (("world_file" "world_image_file"))))))

