(define-module (crates-io sl ip slip) #:use-module (crates-io))

(define-public crate-slip-0.1.0 (c (n "slip") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "slip-imp") (r "^0.1.0") (d #t) (k 0)))) (h "05f0r5pvbxghvpyb27jh42yk9v95ar32w66f49lkvmb9xgjwkl6b") (f (quote (("default") ("allow-no-encryption" "slip-imp/allow-no-encryption"))))))

