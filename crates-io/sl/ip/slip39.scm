(define-module (crates-io sl ip slip39) #:use-module (crates-io))

(define-public crate-slip39-0.1.0 (c (n "slip39") (v "0.1.0") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "hex") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.101") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)) (d (n "sssmc39") (r "^0.0.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.2") (d #t) (k 0)) (d (n "tiny-bip39") (r "^0.6.2") (d #t) (k 0)))) (h "094fq4g5nng8j449igg58zy3cx571d535nhs0g9svx23lxlm7byn")))

(define-public crate-slip39-0.1.1 (c (n "slip39") (v "0.1.1") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "hex") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.101") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)) (d (n "sssmc39") (r "^0.0.2") (d #t) (k 0)) (d (n "structopt") (r "^0.3.2") (d #t) (k 0)) (d (n "tiny-bip39") (r "^0.6.2") (d #t) (k 0)))) (h "01f2jwwqibgwgmqlac0c86yds3a828hgi3dvkxygivyn4fsisz5l")))

