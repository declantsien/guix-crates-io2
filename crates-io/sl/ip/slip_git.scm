(define-module (crates-io sl ip slip_git) #:use-module (crates-io))

(define-public crate-slip_git-0.1.0 (c (n "slip_git") (v "0.1.0") (d (list (d (n "clap") (r "^3.0.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.133") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.133") (d #t) (k 0)) (d (n "shellexpand") (r "^2.1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "url") (r "^2.2.1") (d #t) (k 0)))) (h "1ak9mkry6wkjcdfc41l75kxw1r6h2a17jh3g494zwxjh1sjndg2a")))

(define-public crate-slip_git-0.1.1 (c (n "slip_git") (v "0.1.1") (d (list (d (n "clap") (r "^3.0.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.133") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.133") (d #t) (k 0)) (d (n "shellexpand") (r "^2.1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "url") (r "^2.2.1") (d #t) (k 0)))) (h "00hpmh6qx3ajjynkz4pb5jxmzii55cxm87i5bhyy7gq9w5sz6f46")))

(define-public crate-slip_git-0.1.2 (c (n "slip_git") (v "0.1.2") (d (list (d (n "clap") (r "^3.0.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.133") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.133") (d #t) (k 0)) (d (n "shellexpand") (r "^2.1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "url") (r "^2.2.1") (d #t) (k 0)))) (h "08whfhywy3f1mx356kjd1q8c0rc7b3ghxrfcyhprxwgm9drb0fx6")))

(define-public crate-slip_git-0.1.3 (c (n "slip_git") (v "0.1.3") (d (list (d (n "clap") (r "^3.0.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.24.0") (d #t) (k 0)) (d (n "fuzzy-matcher") (r "^0.3.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.133") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.133") (d #t) (k 0)) (d (n "shellexpand") (r "^2.1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "tui") (r "^0.18.0") (d #t) (k 0)) (d (n "url") (r "^2.2.1") (d #t) (k 0)))) (h "1n2gykxhfadgw1pvi390an3wj6f2y3qgnd5smq98sx3kk3srg8cj")))

