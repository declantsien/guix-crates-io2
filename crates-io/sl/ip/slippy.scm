(define-module (crates-io sl ip slippy) #:use-module (crates-io))

(define-public crate-slippy-0.1.0 (c (n "slippy") (v "0.1.0") (d (list (d (n "assert_cmd") (r "^2.0.8") (d #t) (k 2)) (d (n "axoasset") (r "^0.0.1") (d #t) (k 0)) (d (n "axohtml") (r "^0.4.1") (d #t) (k 0)) (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "console") (r "^0.15.5") (d #t) (k 0)) (d (n "css-minify") (r "^0.3.1") (d #t) (k 0)) (d (n "fs_extra") (r "^1.3.0") (d #t) (k 0)) (d (n "markdown") (r "^1.0.0-alpha.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.39") (d #t) (k 0)))) (h "0vjjjq7lvf1j2phm440y79xj4bik3b0zkccdvncagfqn3nyl9j10")))

