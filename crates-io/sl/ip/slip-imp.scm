(define-module (crates-io sl ip slip-imp) #:use-module (crates-io))

(define-public crate-slip-imp-0.1.0 (c (n "slip-imp") (v "0.1.0") (d (list (d (n "absolution") (r "^0.1") (d #t) (k 0)) (d (n "aes-ctr") (r "^0.3") (d #t) (k 0)) (d (n "base64") (r "^0.12") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "0j7a8h9akqkspnyxz73rgrs21iwzmihl2mhbbqbwfcbf7fvn72bx") (f (quote (("default") ("allow-no-encryption"))))))

