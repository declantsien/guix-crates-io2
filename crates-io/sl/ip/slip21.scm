(define-module (crates-io sl ip slip21) #:use-module (crates-io))

(define-public crate-slip21-0.1.0 (c (n "slip21") (v "0.1.0") (d (list (d (n "bitcoin_hashes") (r "^0.6.0") (d #t) (k 0)) (d (n "hex") (r "^0.3.2") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "06fijbhcrrchsz56vzs9g8166zhvd3hqdbp1iwm7yn2x685wwjxk")))

(define-public crate-slip21-0.2.0 (c (n "slip21") (v "0.2.0") (d (list (d (n "bitcoin_hashes") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1gjcxnz6w2zvg3gw84ks76qmxn8xhqw57pmxhj0jr42d1wd9mzxc")))

(define-public crate-slip21-0.2.1 (c (n "slip21") (v "0.2.1") (d (list (d (n "bitcoin_hashes") (r ">=0.7.0, <0.12.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0syan50fav0vw0d4300rz13zzwhjnm8h44qlqjln4yf6rvrm4vsi")))

