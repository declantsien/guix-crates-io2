(define-module (crates-io sl ip slipstream) #:use-module (crates-io))

(define-public crate-slipstream-0.1.0 (c (n "slipstream") (v "0.1.0") (d (list (d (n "generic-array") (r "~0.14") (d #t) (k 0)) (d (n "multiversion") (r "~0.5") (d #t) (k 2)) (d (n "proptest") (r "~0.9") (d #t) (k 2)) (d (n "rand") (r "~0.7") (d #t) (k 2)) (d (n "typenum") (r "~1") (d #t) (k 0)) (d (n "version-sync") (r "~0.9") (d #t) (k 2)))) (h "0sqwdn25fb7y2lchvfab1qvx81v6m80ngavrm017yf9vvy866klz")))

(define-public crate-slipstream-0.1.1 (c (n "slipstream") (v "0.1.1") (d (list (d (n "generic-array") (r "~0.14") (d #t) (k 0)) (d (n "multiversion") (r "~0.5") (d #t) (k 2)) (d (n "proptest") (r "~0.9") (d #t) (k 2)) (d (n "rand") (r "~0.7") (d #t) (k 2)) (d (n "typenum") (r "~1") (d #t) (k 0)) (d (n "version-sync") (r "~0.9") (d #t) (k 2)))) (h "0n938rvpf3v83gjfgaygv5hyip3dg3nmiakay2f7db40gs4l59yi")))

(define-public crate-slipstream-0.2.0 (c (n "slipstream") (v "0.2.0") (d (list (d (n "criterion") (r "~0.3") (d #t) (k 2)) (d (n "multiversion") (r "~0.6") (d #t) (k 2)) (d (n "proptest") (r "~0.10") (d #t) (k 2)) (d (n "rand") (r "~0.8") (d #t) (k 2)))) (h "010vl7aj5zg5n0l5jcw7wjrdlpy5l63ahg2n4w09zjmw412kpgvi")))

(define-public crate-slipstream-0.2.1 (c (n "slipstream") (v "0.2.1") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "criterion") (r "~0.3") (d #t) (k 2)) (d (n "multiversion") (r "~0.6") (d #t) (k 2)) (d (n "proptest") (r "~0.10") (d #t) (k 2)) (d (n "rand") (r "~0.8") (d #t) (k 2)))) (h "0vd1j49vchhdsx8zijw0krxkv73jssg1xcr2s9v1yvw3d2xrcn3z")))

