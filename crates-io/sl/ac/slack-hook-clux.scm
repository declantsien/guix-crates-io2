(define-module (crates-io sl ac slack-hook-clux) #:use-module (crates-io))

(define-public crate-slack-hook-clux-0.7.1 (c (n "slack-hook-clux") (v "0.7.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "error-chain") (r "~0.11") (d #t) (k 0)) (d (n "hex") (r "^0.2.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "url_serde") (r "^0.2") (d #t) (k 0)))) (h "15n0hxcd6x7rpicldcbxca8s206mqh56rjfwav9l3h7d5j2vkarb") (y #t)))

