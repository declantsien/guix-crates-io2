(define-module (crates-io sl ac slack-api-client) #:use-module (crates-io))

(define-public crate-slack-api-client-0.1.0 (c (n "slack-api-client") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11.21") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("parking_lot" "macros"))) (d #t) (k 0)))) (h "1p0la0zmxf57a2ym2mm1qni7rkc5l0ys1bb16v0sy2ls8qr8xvdb")))

(define-public crate-slack-api-client-0.1.1 (c (n "slack-api-client") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.11.21") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "1v9fdfkmbqhnpiyggaj829b6a6x7r4i8pwm453in6371c7q58i0b")))

(define-public crate-slack-api-client-0.1.2 (c (n "slack-api-client") (v "0.1.2") (d (list (d (n "reqwest") (r "^0.11.21") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "1f42mgw0pq6dp09qq7qh07ka0cbvnzs4vjxxvax7c3z1fkr89jr3")))

(define-public crate-slack-api-client-0.1.3 (c (n "slack-api-client") (v "0.1.3") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0cjbw7wz72lgbxcinpvkn9xjl7ym947h8ddibwqgjs0pzfw841wn")))

(define-public crate-slack-api-client-0.1.4 (c (n "slack-api-client") (v "0.1.4") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0k75hcz0rfxnl30zg5lxvzfik3d7rk30mnf28qkp4n441q182d1y")))

(define-public crate-slack-api-client-0.1.5 (c (n "slack-api-client") (v "0.1.5") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0a0j80p3a4f9kpwy12800pkrskzzam4bvgg093gs4agzw6pcisyw")))

(define-public crate-slack-api-client-0.1.6 (c (n "slack-api-client") (v "0.1.6") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1nll21pig8lp3zbh2smfmhcbs1aizp8pf3bm94lrjcdnyxsya7xq")))

(define-public crate-slack-api-client-0.1.7 (c (n "slack-api-client") (v "0.1.7") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0k2hkd0l1nxqvvik26h0f1jwndqq3b9v7cgjz35jhmnvsxil918k")))

(define-public crate-slack-api-client-0.1.8 (c (n "slack-api-client") (v "0.1.8") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "13snzyk0b02kdq2zmqbj5j4bn64i85yxdaxfqwgnkc4lzqrq2sn5")))

(define-public crate-slack-api-client-0.1.9 (c (n "slack-api-client") (v "0.1.9") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1wqla0wmppgxgnkjil0dm1d65qvyzhahb8wfrwgfw40l91ddkzr7")))

