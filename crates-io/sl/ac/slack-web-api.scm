(define-module (crates-io sl ac slack-web-api) #:use-module (crates-io))

(define-public crate-slack-web-api-0.1.0 (c (n "slack-web-api") (v "0.1.0") (d (list (d (n "hyper") (r "^0.14.26") (f (quote ("http2"))) (d #t) (k 0)) (d (n "hyper-tls") (r "^0.5.0") (d #t) (k 0)) (d (n "multipart") (r "^0.18") (f (quote ("default"))) (d #t) (k 0)) (d (n "serde") (r "~1") (d #t) (k 0)) (d (n "serde_derive") (r "~1") (d #t) (k 0)) (d (n "serde_json") (r "~1") (d #t) (k 0)) (d (n "tokio") (r "^1.19.2") (f (quote ("full"))) (d #t) (k 2)))) (h "0ni9p7ki2rl8b6c1lvikflvm0v43qf17vjadg9adjsayizn2hjb1")))

(define-public crate-slack-web-api-0.1.1 (c (n "slack-web-api") (v "0.1.1") (d (list (d (n "hyper") (r "^0.14") (f (quote ("http2" "stream"))) (d #t) (k 0)) (d (n "hyper-tls") (r "^0.5.0") (d #t) (k 0)) (d (n "mpart-async") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "~1") (d #t) (k 0)) (d (n "serde_derive") (r "~1") (d #t) (k 0)) (d (n "serde_json") (r "~1") (d #t) (k 0)) (d (n "tokio") (r "^1.19.2") (f (quote ("full"))) (d #t) (k 2)))) (h "1hc68xh623gqk02nbdvdzms72ibdfk207n5ac9h6089k0pciaj2m")))

