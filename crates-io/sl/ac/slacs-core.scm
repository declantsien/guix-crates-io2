(define-module (crates-io sl ac slacs-core) #:use-module (crates-io))

(define-public crate-slacs-core-0.0.0 (c (n "slacs-core") (v "0.0.0") (d (list (d (n "docopt") (r "^0.6.82") (d #t) (k 0)) (d (n "gapbuffer") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^0.8.1") (d #t) (k 0)) (d (n "serde_json") (r "^0.8.1") (d #t) (k 0)) (d (n "slog") (r "^0.7.0") (d #t) (k 0)) (d (n "slog-json") (r "^0.7.0") (d #t) (k 0)) (d (n "slog-term") (r "^0.7.0") (d #t) (k 0)) (d (n "xi-rope") (r "^0.1.0") (d #t) (k 0)) (d (n "xi-unicode") (r "^0.0.1") (d #t) (k 0)))) (h "162mhf9s4k9yj2hnjjabhp5brmiwdb4dhpqz6h1j0rlcqra15iwb") (f (quote (("default")))) (y #t)))

