(define-module (crates-io sl ac slackbot) #:use-module (crates-io))

(define-public crate-slackbot-0.1.0 (c (n "slackbot") (v "0.1.0") (d (list (d (n "serde_json") (r "^0.6.0") (d #t) (k 0)) (d (n "slack") (r "^0.8.1") (d #t) (k 0)))) (h "1s7ddky2ybzrxlbc6cn0qyvjb7vdjxlrpr4z7zjhzxvyar1kqp49")))

(define-public crate-slackbot-0.2.0 (c (n "slackbot") (v "0.2.0") (d (list (d (n "serde_json") (r "^0.6.0") (d #t) (k 0)) (d (n "slack") (r "^0.8.2") (d #t) (k 0)))) (h "08nxld07zxbscnh6bpnzr9bi735kgqb5bb5lbdplsv0as773w03p")))

