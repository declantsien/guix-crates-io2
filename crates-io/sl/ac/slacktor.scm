(define-module (crates-io sl ac slacktor) #:use-module (crates-io))

(define-public crate-slacktor-0.1.0 (c (n "slacktor") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rayon") (r "^1.10.0") (d #t) (k 2)) (d (n "slab") (r "^0.4.9") (k 0)))) (h "1aiq2m40dldi1nf1kjk9hls3pvnbxihml31aadv7c78clsi0f0z7") (f (quote (("async"))))))

(define-public crate-slacktor-0.2.0 (c (n "slacktor") (v "0.2.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rayon") (r "^1.10.0") (d #t) (k 2)) (d (n "slab") (r "^0.4.9") (k 0)))) (h "17g1bzknks43w72304x1l00fy1f44yr48ak0474qnkyym2g02lcx") (f (quote (("async")))) (y #t)))

(define-public crate-slacktor-0.2.1 (c (n "slacktor") (v "0.2.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rayon") (r "^1.10.0") (d #t) (k 2)) (d (n "slab") (r "^0.4.9") (k 0)))) (h "0b5n6vp3za7zpivp56vd0mbpfqiw9a2497nahdi5vw96qknc26gh") (f (quote (("async")))) (y #t)))

(define-public crate-slacktor-0.2.2 (c (n "slacktor") (v "0.2.2") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rayon") (r "^1.10.0") (d #t) (k 2)) (d (n "slab") (r "^0.4.9") (k 0)))) (h "1x9srbg7ahx5g1c7yicj0ri0ghw5ql0110bjll3g11ajszq8ijzn") (f (quote (("async"))))))

(define-public crate-slacktor-0.3.0 (c (n "slacktor") (v "0.3.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rayon") (r "^1.10.0") (d #t) (k 2)) (d (n "slab") (r "^0.4.9") (k 0)))) (h "16dxajz5pkj3zjmg10vwh09bm8z9plbm28y5iablqh5n9rvrs145") (f (quote (("async"))))))

