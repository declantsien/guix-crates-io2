(define-module (crates-io sl ac slack-http-verifier) #:use-module (crates-io))

(define-public crate-slack-http-verifier-0.1.0 (c (n "slack-http-verifier") (v "0.1.0") (d (list (d (n "crypto-mac") (r "^0.7") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "hmac") (r "^0.7") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "sha2") (r "^0.8") (d #t) (k 0)))) (h "113vk39iwjbrbq6ynn4iqyvc2s1jni10xxxz5mqb3pmdpvmrvi9i")))

(define-public crate-slack-http-verifier-0.1.1 (c (n "slack-http-verifier") (v "0.1.1") (d (list (d (n "crypto-mac") (r "^0.7") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "hmac") (r "^0.7") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "sha2") (r "^0.8") (d #t) (k 0)))) (h "15s9qwx5p89w6sxcblk4xgqci9km0h1849fpq7pqa60i20i7k700")))

(define-public crate-slack-http-verifier-0.1.2 (c (n "slack-http-verifier") (v "0.1.2") (d (list (d (n "crypto-mac") (r "^0.7") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "hmac") (r "^0.7") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "sha2") (r "^0.8") (d #t) (k 0)))) (h "1y2nwwvzvbqzg8gk201pid1j79b2dq9w9c18gj6cbhcga50qg172")))

