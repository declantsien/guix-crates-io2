(define-module (crates-io sl ac slack) #:use-module (crates-io))

(define-public crate-slack-0.1.0 (c (n "slack") (v "0.1.0") (d (list (d (n "hyper") (r "*") (d #t) (k 0)) (d (n "websocket") (r "*") (d #t) (k 0)))) (h "0zcs3xvahb2cpsmfxj5blk0bampqjdkk70x8vcxrswjhwc199pzr")))

(define-public crate-slack-0.1.1 (c (n "slack") (v "0.1.1") (d (list (d (n "hyper") (r "^0.0.17") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.1.5") (d #t) (k 0)) (d (n "websocket") (r "^0.9.1") (d #t) (k 0)))) (h "0ablbjq4wzzgk6phc6lach4199axf93fj9nshng0km4x3l6h0v61")))

(define-public crate-slack-0.1.2 (c (n "slack") (v "0.1.2") (d (list (d (n "hyper") (r "^0.0.17") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.1.5") (d #t) (k 0)) (d (n "websocket") (r "^0.9.1") (d #t) (k 0)))) (h "1dgxz9m63a80rqlas66ya9kmhwh0pj2vvpcjnwbhvlahhcy0fz9r")))

(define-public crate-slack-0.2.1 (c (n "slack") (v "0.2.1") (d (list (d (n "hyper") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "websocket") (r "^0.9.6") (d #t) (k 0)))) (h "0g46sb5wynr3xa6vjqsw9jfxkgr09rlagllyqnvfb6mlv5qbpdgr")))

(define-public crate-slack-0.2.3 (c (n "slack") (v "0.2.3") (d (list (d (n "hyper") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "websocket") (r "^0.9.6") (d #t) (k 0)))) (h "0i5nifrqjrkmrhy61ml64lqigmy5v04z9bljqvs1n1890znc952q")))

(define-public crate-slack-0.2.4 (c (n "slack") (v "0.2.4") (d (list (d (n "hyper") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "websocket") (r "^0.9.7") (d #t) (k 0)))) (h "1vm0by35cx29r5d7q2khbmz2xjvbpcxp20faslv1vrkag009vjgb")))

(define-public crate-slack-0.2.5 (c (n "slack") (v "0.2.5") (d (list (d (n "hyper") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "websocket") (r "^0.10.0") (d #t) (k 0)))) (h "0qc4l9fvg992z7ychag1zwga0mwk0bhxn4vqi09nnccghlyvdqz2")))

(define-public crate-slack-0.3.1 (c (n "slack") (v "0.3.1") (d (list (d (n "hyper") (r "*") (d #t) (k 0)) (d (n "openssl") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "websocket") (r "^0.10.1") (d #t) (k 0)))) (h "0i19zcwyw7af3vyhjv9n11jzk74qkadm2fp29qqaqhvm1h73jkd6")))

(define-public crate-slack-0.3.2 (c (n "slack") (v "0.3.2") (d (list (d (n "hyper") (r "*") (d #t) (k 0)) (d (n "openssl") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "websocket") (r "^0.10.1") (d #t) (k 0)))) (h "0rabhifpgc9clxxc055ivgw6jafbpbv3xl3xrb6dg55r4vmf5xzj")))

(define-public crate-slack-0.3.3 (c (n "slack") (v "0.3.3") (d (list (d (n "hyper") (r "*") (d #t) (k 0)) (d (n "openssl") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "websocket") (r "^0.10.1") (d #t) (k 0)))) (h "01gacmxf5ffkbdn0b7hrkf4a47bj1l0xz1sc67mqfaf592gllnks")))

(define-public crate-slack-0.3.4 (c (n "slack") (v "0.3.4") (d (list (d (n "hyper") (r "*") (d #t) (k 0)) (d (n "openssl") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "websocket") (r "^0.10.1") (d #t) (k 0)))) (h "1amnb4zspkskvvc521nm5xlknj9145yhsknz6y63cmab5mkrqcm7")))

(define-public crate-slack-0.3.5 (c (n "slack") (v "0.3.5") (d (list (d (n "hyper") (r "*") (d #t) (k 0)) (d (n "openssl") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "websocket") (r "^0.10.1") (d #t) (k 0)))) (h "1s6gwr0zdy70gcyjydmr57pqbx2wwnw0rlprbd8nljnsfnxn6d00")))

(define-public crate-slack-0.4.0 (c (n "slack") (v "0.4.0") (d (list (d (n "hyper") (r "*") (d #t) (k 0)) (d (n "openssl") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "websocket") (r "^0.10.1") (d #t) (k 0)))) (h "0qi19z380qbsvgaz2mdjgrcd0y2ai081j9r2v69jrhp1avl4z07i")))

(define-public crate-slack-0.4.1 (c (n "slack") (v "0.4.1") (d (list (d (n "hyper") (r "*") (d #t) (k 0)) (d (n "openssl") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "websocket") (r "^0.10.1") (d #t) (k 0)))) (h "0npkw39zahlfdvs9cbyn7jvrcx1yrz8ymc3r3khia1m4vshy1jvf")))

(define-public crate-slack-0.5.0 (c (n "slack") (v "0.5.0") (d (list (d (n "hyper") (r "*") (d #t) (k 0)) (d (n "openssl") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "websocket") (r "^0.10.1") (d #t) (k 0)))) (h "1qzjibmd0j4pbnqdz45jd1yb3c5j4m1zgxpqq2mynf901y5byc9k")))

(define-public crate-slack-0.5.2 (c (n "slack") (v "0.5.2") (d (list (d (n "hyper") (r "*") (d #t) (k 0)) (d (n "openssl") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "websocket") (r "^0.11.0") (d #t) (k 0)))) (h "063ii3q3lnggrg13g02fj2qjcmlmgxr6ibj5ax6iz8jjrpwsk3ly")))

(define-public crate-slack-0.6.0 (c (n "slack") (v "0.6.0") (d (list (d (n "hyper") (r "*") (d #t) (k 0)) (d (n "openssl") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "websocket") (r "^0.11.11") (d #t) (k 0)))) (h "0x25jb4gh7nfx1k6ad64cfwclkwfhqybmvk07djm1acahwnw0m5g")))

(define-public crate-slack-0.6.1 (c (n "slack") (v "0.6.1") (d (list (d (n "hyper") (r "*") (d #t) (k 0)) (d (n "openssl") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "websocket") (r "~0.12.1") (d #t) (k 0)))) (h "0rxzy6d9qh3jk46amz2jl9n6a0zyzm7gbmhv44wfx8xj7vji5hlf")))

(define-public crate-slack-0.7.1 (c (n "slack") (v "0.7.1") (d (list (d (n "hyper") (r "~0.6.14") (d #t) (k 0)) (d (n "openssl") (r "~0.6.4") (d #t) (k 0)) (d (n "rustc-serialize") (r "~0.3") (d #t) (k 0)) (d (n "websocket") (r "~0.12.2") (d #t) (k 0)))) (h "1v22w69p3c9wc2i67p6xlxyiilwiw00plx33hyavj2shj0gr5g9p") (y #t)))

(define-public crate-slack-0.7.2 (c (n "slack") (v "0.7.2") (d (list (d (n "hyper") (r "~0.6.14") (d #t) (k 0)) (d (n "openssl") (r "~0.6.4") (d #t) (k 0)) (d (n "rustc-serialize") (r "~0.3") (d #t) (k 0)) (d (n "websocket") (r "~0.12.2") (d #t) (k 0)))) (h "11kh36zj5shhs3mjh7j3hx7f270q38b0c119vfngy16va8a70y4c")))

(define-public crate-slack-0.8.1 (c (n "slack") (v "0.8.1") (d (list (d (n "hyper") (r "~0.6.14") (d #t) (k 0)) (d (n "openssl") (r "~0.6.4") (d #t) (k 0)) (d (n "rustc-serialize") (r "~0.3") (d #t) (k 0)) (d (n "websocket") (r "~0.12.2") (d #t) (k 0)))) (h "0x7hgcp0fwx12f64ln6510ymb4bgy6dinxxqqz46im4zzyfaxjz3") (y #t)))

(define-public crate-slack-0.8.2 (c (n "slack") (v "0.8.2") (d (list (d (n "hyper") (r "~0.6.14") (d #t) (k 0)) (d (n "openssl") (r "~0.6.4") (d #t) (k 0)) (d (n "rustc-serialize") (r "~0.3") (d #t) (k 0)) (d (n "websocket") (r "~0.12.2") (d #t) (k 0)))) (h "0bsmcp3fvnr9jsbvrhi4jxq4cx4y1054aa2dr3g4ywldhhcai1g7")))

(define-public crate-slack-0.8.3 (c (n "slack") (v "0.8.3") (d (list (d (n "hyper") (r "~0.6.14") (d #t) (k 0)) (d (n "openssl") (r "~0.6.4") (d #t) (k 0)) (d (n "rustc-serialize") (r "~0.3") (d #t) (k 0)) (d (n "websocket") (r "~0.12.2") (d #t) (k 0)))) (h "0rj6d3mifi9iyrn8ncrh1d9z2qpz1pr0jg9h0vkm68263w93mr96")))

(define-public crate-slack-0.9.1 (c (n "slack") (v "0.9.1") (d (list (d (n "hyper") (r "~0.6.14") (d #t) (k 0)) (d (n "openssl") (r "^0.6.4") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "url") (r "^0.2") (d #t) (k 0)) (d (n "websocket") (r "~0.12.2") (d #t) (k 0)))) (h "0vrbakw4cap6vyqc9z5zx2m897g5qjiad1xf02qsg8cznfk9c0yi")))

(define-public crate-slack-0.9.2 (c (n "slack") (v "0.9.2") (d (list (d (n "hyper") (r "~0.6.14") (d #t) (k 0)) (d (n "openssl") (r "^0.6.4") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "url") (r "^0.2") (d #t) (k 0)) (d (n "websocket") (r "~0.12.2") (d #t) (k 0)))) (h "0lcslrmvx5l134a5mgsphchsc0faqvqyrx29kchn2rkrl8i382vn")))

(define-public crate-slack-0.10.1 (c (n "slack") (v "0.10.1") (d (list (d (n "hyper") (r "~0.6.14") (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 2)) (d (n "openssl") (r "^0.6.4") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "url") (r "^0.2") (d #t) (k 0)) (d (n "websocket") (r "~0.12.2") (d #t) (k 0)) (d (n "yup-hyper-mock") (r "*") (d #t) (k 2)))) (h "097j26g12q3qdkj72c5jcl44njj1mrr9sf46cfghvmy9ny7c6m8m")))

(define-public crate-slack-0.11.0 (c (n "slack") (v "0.11.0") (d (list (d (n "hyper") (r "~0.6.14") (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 2)) (d (n "openssl") (r "^0.6.4") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "url") (r "^0.2") (d #t) (k 0)) (d (n "websocket") (r "~0.12.2") (d #t) (k 0)) (d (n "yup-hyper-mock") (r "*") (d #t) (k 2)))) (h "0a954qksi9izqxwpqrbxw1hqsxfgiq8qs2vygxaig3rpymrfhl4w")))

(define-public crate-slack-0.12.0 (c (n "slack") (v "0.12.0") (d (list (d (n "hyper") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 2)) (d (n "openssl") (r "^0.7") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "url") (r "^0.5") (d #t) (k 0)) (d (n "websocket") (r "^0.14") (d #t) (k 0)) (d (n "yup-hyper-mock") (r "*") (d #t) (k 2)))) (h "02sfjnzvgv8qx1vim3f5dm7kml0vvxv1f5a901cmg3izpccyax1i")))

(define-public crate-slack-0.12.2 (c (n "slack") (v "0.12.2") (d (list (d (n "hyper") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.3.5") (d #t) (k 2)) (d (n "openssl") (r "^0.7") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "url") (r "^0.5") (d #t) (k 0)) (d (n "websocket") (r "^0.14") (d #t) (k 0)) (d (n "yup-hyper-mock") (r "^1.3.2") (d #t) (k 2)))) (h "18bwp96yy4ng6abb6fwig3lndwla0ww9cs9z9acsna3cwwm7m26c")))

(define-public crate-slack-0.13.0 (c (n "slack") (v "0.13.0") (d (list (d (n "hyper") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.3.5") (d #t) (k 2)) (d (n "openssl") (r "^0.7") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "url") (r "^0.5") (d #t) (k 0)) (d (n "websocket") (r "^0.14") (d #t) (k 0)) (d (n "yup-hyper-mock") (r "^1.3.2") (d #t) (k 2)))) (h "1mhyaw8x0h1cq1xv9irxgd74jwyk6csh1mgjmc62ys3gch9vl3pk")))

(define-public crate-slack-0.14.0 (c (n "slack") (v "0.14.0") (d (list (d (n "hyper") (r "^0.9.5") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.18") (d #t) (k 0)) (d (n "slack_api") (r "^0.15.0") (d #t) (k 0)) (d (n "websocket") (r "^0.17.1") (d #t) (k 0)))) (h "121agnscs93if3wvvlk374w0b7w8li3raim71xm96f6xj6v55q60")))

(define-public crate-slack-0.15.0 (c (n "slack") (v "0.15.0") (d (list (d (n "hyper") (r "^0.9.5") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.18") (d #t) (k 0)) (d (n "slack_api") (r "^0.15.0") (d #t) (k 0)) (d (n "websocket") (r "^0.17.1") (d #t) (k 0)))) (h "18mz7mjbrax0xf26pznxf5895yd9x2zl094193awl0xw9c7a80xh")))

(define-public crate-slack-0.16.0 (c (n "slack") (v "0.16.0") (d (list (d (n "hyper") (r "^0.9.5") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.18") (d #t) (k 0)) (d (n "slack_api") (r "^0.15.0") (d #t) (k 0)) (d (n "websocket") (r "^0.17.1") (d #t) (k 0)))) (h "07kavap3dm9fq6d5p134cn1chc9sb83ijd9vmdna0hlsba4744ik")))

(define-public crate-slack-0.17.0 (c (n "slack") (v "0.17.0") (d (list (d (n "log") (r "^0.3.7") (d #t) (k 0)) (d (n "reqwest") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (d #t) (k 0)) (d (n "slack_api") (r "^0.16.0") (f (quote ("reqwest"))) (d #t) (k 0)) (d (n "tungstenite") (r "^0.2.0") (d #t) (k 0)))) (h "1m62fy4r4qbqqmykxf0xb0bb0755l96v7w8qhfk9bw9ky7i3805h")))

(define-public crate-slack-0.17.1 (c (n "slack") (v "0.17.1") (d (list (d (n "log") (r "^0.3.7") (d #t) (k 0)) (d (n "reqwest") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (d #t) (k 0)) (d (n "slack_api") (r "^0.16.1") (f (quote ("reqwest"))) (d #t) (k 0)) (d (n "tungstenite") (r "^0.2.0") (d #t) (k 0)))) (h "08hsmbpha098vkjqs0g185c9j41zdh8n8mh21gh01lwagk9zgj21")))

(define-public crate-slack-0.18.0 (c (n "slack") (v "0.18.0") (d (list (d (n "log") (r "^0.3.7") (d #t) (k 0)) (d (n "reqwest") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (d #t) (k 0)) (d (n "slack_api") (r "^0.17.0") (f (quote ("reqwest"))) (d #t) (k 0)) (d (n "tungstenite") (r "^0.2.0") (d #t) (k 0)))) (h "0zdpxhchbfxfcpsb51kv1szy8r698njznfmmpy0ibgbf922l2rs4")))

(define-public crate-slack-0.19.0 (c (n "slack") (v "0.19.0") (d (list (d (n "log") (r "^0.3.7") (d #t) (k 0)) (d (n "reqwest") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.0") (d #t) (k 0)) (d (n "slack_api") (r "^0.18.0") (f (quote ("reqwest"))) (d #t) (k 0)) (d (n "tungstenite") (r "^0.2.0") (d #t) (k 0)))) (h "0qdgagmwdrpa33l2d0qfx15x40ffsrj7m2f8x7mza594ws6bny45")))

(define-public crate-slack-0.20.0 (c (n "slack") (v "0.20.0") (d (list (d (n "log") (r "^0.3.7") (d #t) (k 0)) (d (n "reqwest") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.0") (d #t) (k 0)) (d (n "slack_api") (r "^0.19.0") (f (quote ("reqwest"))) (d #t) (k 0)) (d (n "tungstenite") (r "^0.5.3") (d #t) (k 0)))) (h "1li4isxm5sn2zibrr253vjc7h8mhihaghnscaqb27n9jvkkl5279")))

(define-public crate-slack-0.21.0 (c (n "slack") (v "0.21.0") (d (list (d (n "log") (r "^0.3.7") (d #t) (k 0)) (d (n "reqwest") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.0") (d #t) (k 0)) (d (n "slack_api") (r "^0.20.0") (f (quote ("reqwest"))) (d #t) (k 0)) (d (n "tungstenite") (r "^0.5.3") (d #t) (k 0)))) (h "19sa60kknwr7q9dppr2pibymqkq25awnfvy56466mxhkc38493pk")))

(define-public crate-slack-0.22.0 (c (n "slack") (v "0.22.0") (d (list (d (n "log") (r "^0.3.7") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.0") (d #t) (k 0)) (d (n "slack_api") (r "^0.21.0") (f (quote ("reqwest"))) (d #t) (k 0)) (d (n "tungstenite") (r "^0.6") (d #t) (k 0)))) (h "1wq2vkc0d56j0rhh5lyvc7c9q77w4n4i2ffl9y95pjqyrji47fi5")))

(define-public crate-slack-0.23.0 (c (n "slack") (v "0.23.0") (d (list (d (n "log") (r "^0.3.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.0") (d #t) (k 0)) (d (n "slack_api") (r "^0.22.0") (d #t) (k 0)) (d (n "tungstenite") (r "^0.9.2") (d #t) (k 0)) (d (n "url") (r "^2.1.0") (d #t) (k 0)))) (h "1cdac6c91pdk5cl4bxhzgffsyfdj6gzrgrns5wvhr6pnq0zyp790")))

(define-public crate-slack-0.24.0 (c (n "slack") (v "0.24.0") (d (list (d (n "log") (r "^0.3.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.0") (d #t) (k 0)) (d (n "slack_api") (r "^0.23.1") (f (quote ("reqwest_blocking"))) (k 0)) (d (n "tungstenite") (r "^0.9.2") (d #t) (k 0)) (d (n "url") (r "^2.1.0") (d #t) (k 0)))) (h "0hdj5i3kwv026vcdfrxyxi8fqwwvvbj6hz78hghp0s76kd76ngg7")))

(define-public crate-slack-0.25.0 (c (n "slack") (v "0.25.0") (d (list (d (n "log") (r "^0.3.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.0") (d #t) (k 0)) (d (n "slack_api") (r "^0.23.1") (f (quote ("reqwest_blocking"))) (k 0)) (d (n "tungstenite") (r "^0.9.2") (d #t) (k 0)) (d (n "url") (r "^2.1.0") (d #t) (k 0)))) (h "1n6fgdrzry40l6ncv33mmlnhbck4hrp8wh1mfmx90mpbm5n2b3mw") (f (quote (("with_rustls" "slack_api/with_rustls") ("with_native_tls" "slack_api/with_native_tls") ("default" "with_native_tls"))))))

