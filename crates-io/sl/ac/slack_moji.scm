(define-module (crates-io sl ac slack_moji) #:use-module (crates-io))

(define-public crate-slack_moji-0.0.1 (c (n "slack_moji") (v "0.0.1") (d (list (d (n "ansi_term") (r "^0.11.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2.15") (d #t) (k 0)))) (h "0r4ca8zdinybyzz3r8bfl31l3b9qjk8xxx5h94y24r5113wchbwj")))

(define-public crate-slack_moji-0.0.2 (c (n "slack_moji") (v "0.0.2") (d (list (d (n "ansi_term") (r "^0.11.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2.15") (d #t) (k 0)))) (h "1ym0wxljvmh7b57hs2z0g2bc6pi2wm141jcxw1v8sc5j6mn8hivm")))

