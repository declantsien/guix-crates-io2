(define-module (crates-io sl ac slack-flows) #:use-module (crates-io))

(define-public crate-slack-flows-0.1.0 (c (n "slack-flows") (v "0.1.0") (d (list (d (n "http_req_wasi") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "urlencoding") (r "^2") (d #t) (k 0)))) (h "051c12nwkgz1ij0xqydfadwimmzr39wsdwsypimm3cwl5g7cqk62")))

(define-public crate-slack-flows-0.1.1 (c (n "slack-flows") (v "0.1.1") (d (list (d (n "http_req_wasi") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "urlencoding") (r "^2") (d #t) (k 0)))) (h "0kc0q634ks2c62f6dn11dmc2v0irgwxkjbx3yxwbzrllazcb0xi9")))

(define-public crate-slack-flows-0.1.2 (c (n "slack-flows") (v "0.1.2") (d (list (d (n "http_req_wasi") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "urlencoding") (r "^2") (d #t) (k 0)))) (h "0x9pcd991c4djhn5jn8lah3bc8n9y30ss7b3virdx9jryggm49m1")))

(define-public crate-slack-flows-0.1.3 (c (n "slack-flows") (v "0.1.3") (d (list (d (n "http_req_wasi") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "urlencoding") (r "^2") (d #t) (k 0)))) (h "039f50kdclhs6m2c8m0j806gsxxlk2km51qy01zjjdlgn1ijfj0c")))

(define-public crate-slack-flows-0.1.4 (c (n "slack-flows") (v "0.1.4") (d (list (d (n "http_req_wasi") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "urlencoding") (r "^2") (d #t) (k 0)))) (h "1xml30way3m0rlac21l9fcw7iac7ix229hvmxms7gg4x8dyghjk7")))

(define-public crate-slack-flows-0.1.5 (c (n "slack-flows") (v "0.1.5") (d (list (d (n "http_req_wasi") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "urlencoding") (r "^2") (d #t) (k 0)))) (h "0c36fqhjhw8wpqv780qijg28h82abh9b7baqi97lxfpwy9f026g9")))

(define-public crate-slack-flows-0.1.6 (c (n "slack-flows") (v "0.1.6") (d (list (d (n "http_req_wasi") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "urlencoding") (r "^2") (d #t) (k 0)))) (h "1m9422qa80s0i4fi1n0pyvk1lpjzxhl1l6qzf4ji79ndimbvdrb9")))

(define-public crate-slack-flows-0.1.7 (c (n "slack-flows") (v "0.1.7") (d (list (d (n "http_req_wasi") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "urlencoding") (r "^2") (d #t) (k 0)))) (h "1psbf17hlvnwwi02ijbh89pdz091z6211i26pa4j05q6ipwq3fbq")))

(define-public crate-slack-flows-0.1.8 (c (n "slack-flows") (v "0.1.8") (d (list (d (n "http_req_wasi") (r "^0.10") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "urlencoding") (r "^2") (d #t) (k 0)))) (h "1jkhrz306jwmv2k9p5fq02vlp8v484j46z9a9yfj6himdmmlxp2r")))

(define-public crate-slack-flows-0.1.9 (c (n "slack-flows") (v "0.1.9") (d (list (d (n "http_req_wasi") (r "^0.10") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "urlencoding") (r "^2") (d #t) (k 0)))) (h "1ac3k3v1jlvidwgq73w0vss8hdhcnm6lllx7riqacm9hsjz41wb9")))

(define-public crate-slack-flows-0.1.10 (c (n "slack-flows") (v "0.1.10") (d (list (d (n "http_req_wasi") (r "^0.10") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "urlencoding") (r "^2") (d #t) (k 0)))) (h "09k6faxa1sfb8zvms17k0cd5f0kf082r5k4fdw3jhnfg4kj2n69y")))

(define-public crate-slack-flows-0.1.11 (c (n "slack-flows") (v "0.1.11") (d (list (d (n "http_req_wasi") (r "^0.10") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "urlencoding") (r "^2") (d #t) (k 0)))) (h "0gvycb1j0s8556n549mwks7895x4xvnpm6wlvwd1kqrcywv4hf92")))

(define-public crate-slack-flows-0.1.12 (c (n "slack-flows") (v "0.1.12") (d (list (d (n "http_req_wasi") (r "^0.10") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "urlencoding") (r "^2") (d #t) (k 0)))) (h "1q30q875nwmcv2x6ly22kkhv90jyp1j4rnzn7fw9g3nwgbrfdf88")))

(define-public crate-slack-flows-0.1.13 (c (n "slack-flows") (v "0.1.13") (d (list (d (n "http_req_wasi") (r "^0.10") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "urlencoding") (r "^2") (d #t) (k 0)))) (h "1vl06p5mfp0isfrblfdqmcl8l2gly49jsgywrn6dr8sjxq2xhayh")))

(define-public crate-slack-flows-0.1.14 (c (n "slack-flows") (v "0.1.14") (d (list (d (n "http_req_wasi") (r "^0.10") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "urlencoding") (r "^2") (d #t) (k 0)))) (h "0h4jlg7mjv4njnsr1gcqyais8q58hdqyl81pcim5zdxzf6apamsq")))

(define-public crate-slack-flows-0.1.15 (c (n "slack-flows") (v "0.1.15") (d (list (d (n "http_req_wasi") (r "^0.10") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "urlencoding") (r "^2") (d #t) (k 0)))) (h "10iv515d4a4f0jayxpqx6400493mbw5prk5gyvxvkyksjisv74sv")))

(define-public crate-slack-flows-0.1.16 (c (n "slack-flows") (v "0.1.16") (d (list (d (n "http_req_wasi") (r "^0.10") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "urlencoding") (r "^2") (d #t) (k 0)))) (h "18rzdwzl5lhf8raw48jcybvcpk1s9zkq7r3dxp99pybnyf0lk8hq")))

(define-public crate-slack-flows-0.1.17 (c (n "slack-flows") (v "0.1.17") (d (list (d (n "http_req_wasi") (r "^0.10") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "urlencoding") (r "^2") (d #t) (k 0)))) (h "052w5nb8iz06idpz28bqwqbhgmpxfq7vjv6vphpr7hdwdws676wc")))

(define-public crate-slack-flows-0.1.19 (c (n "slack-flows") (v "0.1.19") (d (list (d (n "http_req_wasi") (r "^0.10") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "urlencoding") (r "^2") (d #t) (k 0)))) (h "0yzlqixsj4g9vzsswms66imzx881cg5gngszk1aicgdbyjlbl5jw")))

(define-public crate-slack-flows-0.1.20 (c (n "slack-flows") (v "0.1.20") (d (list (d (n "http_req_wasi") (r "^0.10") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "urlencoding") (r "^2") (d #t) (k 0)))) (h "0gd8jawwxf37y30mjgaivjzx9wmv48fyayjzmh44a1c00p9b4r0m")))

(define-public crate-slack-flows-0.1.21 (c (n "slack-flows") (v "0.1.21") (d (list (d (n "http_req_wasi") (r "^0.10") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "urlencoding") (r "^2") (d #t) (k 0)))) (h "072kbinw9qijhq179pnnhfd1qa9a99m81k314w29nvmw3z9409h6")))

(define-public crate-slack-flows-0.2.0 (c (n "slack-flows") (v "0.2.0") (d (list (d (n "http_req_wasi") (r "^0.10") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "urlencoding") (r "^2") (d #t) (k 0)))) (h "0s7bg31j6i40iykxf71xhxmc9xs98vi2m6j14x4jara7a3c140b5")))

(define-public crate-slack-flows-0.2.1 (c (n "slack-flows") (v "0.2.1") (d (list (d (n "http_req_wasi") (r "^0.10") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "urlencoding") (r "^2") (d #t) (k 0)))) (h "13fxzf8ryg7vmqzhcnjwh2f2nrj3kc0lp96gv7h08mc9kaslks5c")))

(define-public crate-slack-flows-0.2.2 (c (n "slack-flows") (v "0.2.2") (d (list (d (n "http_req_wasi") (r "^0.10") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "urlencoding") (r "^2") (d #t) (k 0)))) (h "12rfjr2s70np62yysf685fv453vmw9pmirkmkdf824r8mkny8hk2")))

(define-public crate-slack-flows-0.2.3 (c (n "slack-flows") (v "0.2.3") (d (list (d (n "http_req_wasi") (r "^0.10") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "urlencoding") (r "^2") (d #t) (k 0)))) (h "0hy6rrkq2q3rsdbxhqynpach1zgkyr7mfxdpaai691zng68w786b")))

(define-public crate-slack-flows-0.2.4 (c (n "slack-flows") (v "0.2.4") (d (list (d (n "http_req_wasi") (r "^0.10") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "urlencoding") (r "^2") (d #t) (k 0)))) (h "054s1d5vmmi3wd8vwsr19jmsb4m2j01mzikvwf8mvsl11xabjcdz")))

(define-public crate-slack-flows-0.2.5 (c (n "slack-flows") (v "0.2.5") (d (list (d (n "flowsnet-platform-sdk") (r "^0.1") (d #t) (k 0)) (d (n "http_req_wasi") (r "^0.10") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "urlencoding") (r "^2") (d #t) (k 0)))) (h "0s0yh61f2zhs7v2prylxfx82b1dv8lfrzkpibaslridama65fa99")))

(define-public crate-slack-flows-0.2.6 (c (n "slack-flows") (v "0.2.6") (d (list (d (n "flowsnet-platform-sdk") (r "^0.1") (d #t) (k 0)) (d (n "http_req_wasi") (r "^0.10") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "urlencoding") (r "^2") (d #t) (k 0)))) (h "0rb77ks4l42iw7l7svacy21m0lalh0zckcwkdij4qrql7ma4vynv")))

(define-public crate-slack-flows-0.3.0 (c (n "slack-flows") (v "0.3.0") (d (list (d (n "flowsnet-platform-sdk") (r "^0.1") (d #t) (k 0)) (d (n "http_req_wasi") (r "^0.10") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "urlencoding") (r "^2") (d #t) (k 0)))) (h "0sfw9n39yg3l7jpjjn181ar51d770zva3cisi1gz5pv6w77xi3rf")))

(define-public crate-slack-flows-0.3.1 (c (n "slack-flows") (v "0.3.1") (d (list (d (n "flowsnet-platform-sdk") (r "^0.1") (d #t) (k 0)) (d (n "http_req_wasi") (r "^0.10") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "urlencoding") (r "^2") (d #t) (k 0)))) (h "1zx93bl93sc3p0jhrqsgi822shpzbbwnsdypky8z6lhp9arlrghw")))

(define-public crate-slack-flows-0.3.2 (c (n "slack-flows") (v "0.3.2") (d (list (d (n "flowsnet-platform-sdk") (r "^0.1") (d #t) (k 0)) (d (n "http_req_wasi") (r "^0.10") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "urlencoding") (r "^2") (d #t) (k 0)))) (h "1znk3d1jyn9855iq3hmnbh2699fi4aci45kwn2hlbw5xjmclnrnj")))

(define-public crate-slack-flows-0.3.3 (c (n "slack-flows") (v "0.3.3") (d (list (d (n "flowsnet-platform-sdk") (r "^0.1") (d #t) (k 0)) (d (n "http_req_wasi") (r "^0.10") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "urlencoding") (r "^2") (d #t) (k 0)))) (h "18s9fd15lsdj0vmbpk93am44nl47amzndd8p3f7d4g0gjnlhmpjd")))

(define-public crate-slack-flows-0.3.4 (c (n "slack-flows") (v "0.3.4") (d (list (d (n "flowsnet-platform-sdk") (r "^0.1") (d #t) (k 0)) (d (n "http_req_wasi") (r "^0.10") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "urlencoding") (r "^2") (d #t) (k 0)))) (h "0p6czbxgsjz89r861vijpw54c92q4zl72g2pf67aisp1wpsrd24c")))

