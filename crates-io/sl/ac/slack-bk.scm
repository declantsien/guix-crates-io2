(define-module (crates-io sl ac slack-bk) #:use-module (crates-io))

(define-public crate-slack-bk-0.1.0 (c (n "slack-bk") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1jql46k9gwlxvmbvj0x4dm1dmg8mwi49xgvh13ki8p4w0ccsqhxg")))

(define-public crate-slack-bk-0.1.1 (c (n "slack-bk") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0hr9ys3riivshp5hgdksxssv9gb4a2xz00d97vs9wzk6n44ax6rj")))

