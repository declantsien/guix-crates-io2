(define-module (crates-io sl ac slack_status) #:use-module (crates-io))

(define-public crate-slack_status-0.1.0 (c (n "slack_status") (v "0.1.0") (d (list (d (n "clap") (r "^4.3.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1rgs130ilksyvz844amlx9il917qg29c2cgsmyl94mrxgpr4vrlz")))

(define-public crate-slack_status-0.1.1 (c (n "slack_status") (v "0.1.1") (d (list (d (n "clap") (r "^4.3.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0ikq7hlkfrncdcs2mchp0y477kwmmq4mvd91xy4s8jlvch65bljs")))

