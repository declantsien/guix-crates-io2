(define-module (crates-io sl ac slack-verify) #:use-module (crates-io))

(define-public crate-slack-verify-0.1.0 (c (n "slack-verify") (v "0.1.0") (d (list (d (n "hex") (r "^0.3.2") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)))) (h "0fiyjym9d0kvcx9vr7v3m2lhwywykisp06m46w28549j5lc9g5p9") (y #t)))

(define-public crate-slack-verify-0.1.1 (c (n "slack-verify") (v "0.1.1") (d (list (d (n "hex") (r "^0.3.2") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)))) (h "0gif7v9rw93mdj94af3qy0mfw6kkb46jkvvpix9dn6phcsd6mdq8")))

