(define-module (crates-io sl ac slack-gc) #:use-module (crates-io))

(define-public crate-slack-gc-1.0.0 (c (n "slack-gc") (v "1.0.0") (d (list (d (n "chrono") (r "^0.4.4") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "fern") (r "^0.5.6") (d #t) (k 0)) (d (n "log") (r "^0.4.3") (d #t) (k 0)) (d (n "pretty-bytes") (r "^0.2.2") (d #t) (k 0)) (d (n "rayon") (r "^1.0.2") (d #t) (k 0)) (d (n "slack_api") (r "^0.20.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2.10") (d #t) (k 0)))) (h "14rpx8qm1adcw7bsaimsx575fgv61nkdarkw7422vd3vw17kwmzh")))

(define-public crate-slack-gc-1.0.1 (c (n "slack-gc") (v "1.0.1") (d (list (d (n "chrono") (r "^0.4.4") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "fern") (r "^0.5.6") (d #t) (k 0)) (d (n "log") (r "^0.4.3") (d #t) (k 0)) (d (n "pretty-bytes") (r "^0.2.2") (d #t) (k 0)) (d (n "rayon") (r "^1.0.2") (d #t) (k 0)) (d (n "slack_api") (r "^0.20.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2.10") (d #t) (k 0)))) (h "16716wj3k9vd432h1rczxnhf1wnwh7pj2sz4w7c039ipphs1a8zp")))

