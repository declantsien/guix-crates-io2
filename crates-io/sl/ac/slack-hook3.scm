(define-module (crates-io sl ac slack-hook3) #:use-module (crates-io))

(define-public crate-slack-hook3-0.11.0 (c (n "slack-hook3") (v "0.11.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2") (f (quote ("serde"))) (d #t) (k 0)))) (h "11h6v5llw4wlr2yzb6ls28mjlzkr8f6fjlhpxxljb6sqjyhrax3m") (f (quote (("rustls-tls" "reqwest/rustls-tls") ("native-tls" "reqwest/native-tls") ("default" "native-tls"))))))

