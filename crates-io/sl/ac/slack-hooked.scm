(define-module (crates-io sl ac slack-hooked) #:use-module (crates-io))

(define-public crate-slack-hooked-0.9.0 (c (n "slack-hooked") (v "0.9.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "error-chain") (r "^0.12") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "url") (r "^2") (f (quote ("serde"))) (d #t) (k 0)))) (h "10djg7bkfqn1zdqnhlh6vk04gp8qj4mp3xgqr669xyp2003i9wjy")))

