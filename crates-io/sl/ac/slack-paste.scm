(define-module (crates-io sl ac slack-paste) #:use-module (crates-io))

(define-public crate-slack-paste-0.1.0 (c (n "slack-paste") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "directories") (r "^3.0.1") (d #t) (k 0)) (d (n "read_input") (r "^0.8.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.116") (f (quote ("derive"))) (d #t) (k 0)) (d (n "slack-morphism") (r "^0.2.8") (d #t) (k 0)) (d (n "slack-morphism-models") (r "^0.2.8") (d #t) (k 0)) (d (n "tokio") (r "^0.2.22") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1vk5mccgrgbr3qlr2c0hwxrs8xg7cwisb9863m9if638rv37nd0q")))

