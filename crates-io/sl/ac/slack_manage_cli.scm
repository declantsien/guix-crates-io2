(define-module (crates-io sl ac slack_manage_cli) #:use-module (crates-io))

(define-public crate-slack_manage_cli-0.0.1 (c (n "slack_manage_cli") (v "0.0.1") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.0") (d #t) (k 0)) (d (n "slack_api") (r "^0.21.0") (d #t) (k 0)))) (h "0j7wdza9pwf71jk63ivkxr5g4al63ydr4jy9anyzg3bz2nlmr93y")))

