(define-module (crates-io sl ac slacktus) #:use-module (crates-io))

(define-public crate-slacktus-0.1.0 (c (n "slacktus") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "~2.32") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.12") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1cd17i2qfc787inm1v7gsn8ga3nzydn62m4jvscj5973kfpbppw9")))

(define-public crate-slacktus-0.1.1 (c (n "slacktus") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "~2.32") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.12") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1x1h431h3x0islldvannmkhy3j138fwc8xyx1817rh3573z61mf9")))

(define-public crate-slacktus-0.1.2 (c (n "slacktus") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "~2.32") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.12") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0k01zly8mfpq3yy9g53r28b4ahiv88hk5r9gbhn6g5xqqrdd3qbm")))

(define-public crate-slacktus-0.1.3 (c (n "slacktus") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "~2.32") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.12") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1r91m0vf14866r88hvlnxllkmaj0vmv5h45xd58ydpddq19pz16v")))

(define-public crate-slacktus-0.1.4 (c (n "slacktus") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "~2.32") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.12") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0nmzd923p9bzwbdynggidhmxim3mx0qh310bsl58n1gm1i4bfxpr")))

(define-public crate-slacktus-0.1.5 (c (n "slacktus") (v "0.1.5") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "~2.32") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.12") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0n35y2p9003dp6z7gx34kybk3javi4m5nh37js1dk0m20xxvgr7a")))

