(define-module (crates-io sl en slender-math) #:use-module (crates-io))

(define-public crate-slender-math-0.1.0 (c (n "slender-math") (v "0.1.0") (d (list (d (n "bytemuck") (r "^1.13") (o #t) (d #t) (k 0)))) (h "0pphz6x1wcpfmanr28769dr5pnnji4vz0iwvrgmh85d0r7w68xzj") (f (quote (("short_names") ("default" "bytemuck" "short_names" "color_fields") ("color_fields"))))))

(define-public crate-slender-math-0.1.1 (c (n "slender-math") (v "0.1.1") (d (list (d (n "bytemuck") (r "^1.13") (o #t) (d #t) (k 0)))) (h "1i3v9b1d7gb9az7fnyv3vb9yiam5r79a0dxn1x7rk7jyhyy1xw6s") (f (quote (("short_names") ("default" "bytemuck" "short_names" "color_fields") ("color_fields"))))))

