(define-module (crates-io sl en slen) #:use-module (crates-io))

(define-public crate-slen-0.1.0 (c (n "slen") (v "0.1.0") (d (list (d (n "array-init") (r "^2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "substring") (r "^1.4") (d #t) (k 0)))) (h "0m40jfc3l35z0jk03xk1j5730wgyvbmwdj0xp060ypdq14p7vcbr")))

(define-public crate-slen-0.1.1 (c (n "slen") (v "0.1.1") (d (list (d (n "array-init") (r "^2.0") (d #t) (k 0)) (d (n "substring") (r "^1.4") (d #t) (k 0)))) (h "1c4bg3y46f0g906xp943xi3514wcaiv7mi477a3r7fy0z3g89rkr")))

