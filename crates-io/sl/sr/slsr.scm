(define-module (crates-io sl sr slsr) #:use-module (crates-io))

(define-public crate-SLSR-0.0.1 (c (n "SLSR") (v "0.0.1") (d (list (d (n "docopt") (r "*") (d #t) (k 0)) (d (n "docopt_macros") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "term") (r "*") (d #t) (k 0)) (d (n "union-find") (r "*") (d #t) (k 0)))) (h "13gsi2qwm97jx6ayl4p9g17j1ca8s9iw3lxrm3jz92n5cx70md89")))

