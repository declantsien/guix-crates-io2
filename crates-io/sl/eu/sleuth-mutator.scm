(define-module (crates-io sl eu sleuth-mutator) #:use-module (crates-io))

(define-public crate-sleuth-mutator-0.1.0 (c (n "sleuth-mutator") (v "0.1.0") (d (list (d (n "proc-macro2") (r ">=1") (d #t) (k 0)) (d (n "quote") (r ">=1") (d #t) (k 0)) (d (n "syn") (r ">=2") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0npbsl2bawx2bswbwmws1ng2i0qlzkdw7b021sg7pz2zkijpwm46")))

(define-public crate-sleuth-mutator-0.1.1 (c (n "sleuth-mutator") (v "0.1.1") (d (list (d (n "proc-macro2") (r ">=1") (d #t) (k 0)) (d (n "quote") (r ">=1") (d #t) (k 0)) (d (n "syn") (r ">=2") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1sapkac636s20qhcjqphyjq1wvy2m50s9xb2s60cncbvs3x231mq")))

(define-public crate-sleuth-mutator-0.1.3 (c (n "sleuth-mutator") (v "0.1.3") (d (list (d (n "proc-macro2") (r ">=1") (d #t) (k 0)) (d (n "quote") (r ">=1") (d #t) (k 0)) (d (n "syn") (r ">=2") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "193mqxjr6x834l9z5qb0dc470qjq1vslvkkrcknmz4zjkll4sj24")))

(define-public crate-sleuth-mutator-0.1.4 (c (n "sleuth-mutator") (v "0.1.4") (d (list (d (n "heck") (r ">=0.4") (d #t) (k 0)) (d (n "proc-macro2") (r ">=1") (d #t) (k 0)) (d (n "quote") (r ">=1") (d #t) (k 0)) (d (n "syn") (r ">=2") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0pd35g1cmhz3ayd1n3964dp985d3g048slxkc171pkkz4cwk0ff0")))

(define-public crate-sleuth-mutator-0.2.0 (c (n "sleuth-mutator") (v "0.2.0") (d (list (d (n "heck") (r ">=0.4") (d #t) (k 0)) (d (n "proc-macro2") (r ">=1") (d #t) (k 0)) (d (n "quote") (r ">=1") (d #t) (k 0)) (d (n "syn") (r ">=2") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "178b0dp63h4f77y6s35acvs8xifz3lp6z99gv18b0p8nipg6i8c9")))

(define-public crate-sleuth-mutator-0.2.1 (c (n "sleuth-mutator") (v "0.2.1") (d (list (d (n "heck") (r ">=0.4") (d #t) (k 0)) (d (n "proc-macro2") (r ">=1") (d #t) (k 0)) (d (n "quote") (r ">=1") (d #t) (k 0)) (d (n "syn") (r ">=2") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0ckq9d831d8dbcjjli27r4d38zj1dh69g71hnkk520j6v1qg8w8f")))

