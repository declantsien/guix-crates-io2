(define-module (crates-io sl eu sleuth) #:use-module (crates-io))

(define-public crate-sleuth-0.1.0 (c (n "sleuth") (v "0.1.0") (d (list (d (n "anyhow") (r ">=1") (d #t) (k 0)) (d (n "colored") (r ">=2") (d #t) (k 0)) (d (n "hashbrown") (r ">=0.13") (f (quote ("inline-more"))) (d #t) (k 0)) (d (n "mutator") (r ">=0.1.0") (d #t) (k 0)) (d (n "prettyplease") (r ">=0.2") (d #t) (k 0)) (d (n "syn") (r ">=2") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0jja6pqbqpihc2l1mzhrp9nnwlhm6d7j56h6baa7sxg9ksg78wzv")))

(define-public crate-sleuth-0.1.1 (c (n "sleuth") (v "0.1.1") (d (list (d (n "anyhow") (r ">=1") (d #t) (k 0)) (d (n "colored") (r ">=2") (d #t) (k 0)) (d (n "hashbrown") (r ">=0.13") (f (quote ("inline-more"))) (d #t) (k 0)) (d (n "prettyplease") (r ">=0.2") (d #t) (k 0)) (d (n "sleuth-mutator") (r ">=0.1.0") (d #t) (k 0)) (d (n "syn") (r ">=2") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "04c6pqs2k40a3srhpgms6bnmys61whsspidk83nj5hdvv22kg0d3")))

(define-public crate-sleuth-0.1.2 (c (n "sleuth") (v "0.1.2") (d (list (d (n "anyhow") (r ">=1") (d #t) (k 0)) (d (n "colored") (r ">=2") (d #t) (k 0)) (d (n "hashbrown") (r ">=0.13") (f (quote ("inline-more"))) (d #t) (k 0)) (d (n "prettyplease") (r ">=0.2") (d #t) (k 0)) (d (n "sleuth-mutator") (r ">=0.1.0") (d #t) (k 0)) (d (n "syn") (r ">=2") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1a4z7r68f5yjc9vpjg75p1j1fcpcx1nzd2phqp8xhn1jm4r7h5wn")))

(define-public crate-sleuth-0.1.3 (c (n "sleuth") (v "0.1.3") (d (list (d (n "anyhow") (r ">=1") (d #t) (k 0)) (d (n "colored") (r ">=2") (d #t) (k 0)) (d (n "hashbrown") (r ">=0.13") (f (quote ("inline-more"))) (d #t) (k 0)) (d (n "prettyplease") (r ">=0.2") (d #t) (k 0)) (d (n "sleuth-mutator") (r ">=0.1.0") (d #t) (k 0)) (d (n "syn") (r ">=2") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "072dzxpgrhnfr1jl79afrdda6fpggb75swsp453a79w6m3fkj7gf")))

(define-public crate-sleuth-0.1.4 (c (n "sleuth") (v "0.1.4") (d (list (d (n "colored") (r ">=2") (d #t) (k 0)) (d (n "prettyplease") (r ">=0.2") (d #t) (k 0)) (d (n "sleuth-mutator") (r ">=0") (d #t) (k 0)) (d (n "syn") (r ">=2") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0vr1n5affyd4l2dkpmq7vq7xl8cg5mramad2b6aqlrddf4ki60xa")))

(define-public crate-sleuth-0.2.0 (c (n "sleuth") (v "0.2.0") (d (list (d (n "colored") (r ">=2") (d #t) (k 0)) (d (n "prettyplease") (r ">=0.2") (d #t) (k 0)) (d (n "sleuth-mutator") (r ">=0") (d #t) (k 0)) (d (n "syn") (r ">=2") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "03z40m5y716ghsh1cxas1p2a49zqivj29qy6nlfladjhgwrsrqca")))

(define-public crate-sleuth-0.2.1 (c (n "sleuth") (v "0.2.1") (d (list (d (n "colored") (r ">=2") (d #t) (k 0)) (d (n "prettyplease") (r ">=0.2") (d #t) (k 0)) (d (n "sleuth-mutator") (r ">=0") (d #t) (k 0)) (d (n "syn") (r ">=2") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "10ibkn009fcwh5fjsj6d0l26k3j3wbhhcjk56xk26gl6zv6smj45")))

