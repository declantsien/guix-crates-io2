(define-module (crates-io sl ug slugify-rs) #:use-module (crates-io))

(define-public crate-slugify-rs-0.0.1 (c (n "slugify-rs") (v "0.0.1") (d (list (d (n "deunicode") (r "^1.3.1") (d #t) (k 0)) (d (n "nanoid") (r "^0.4.0") (d #t) (k 0)))) (h "098ayjgcxpf43kvmkr822ap13nwbnzddy3smg8r0idk7yaf7w6l4") (r "1.56")))

(define-public crate-slugify-rs-0.0.2 (c (n "slugify-rs") (v "0.0.2") (d (list (d (n "deunicode") (r "^1.3.1") (d #t) (k 0)) (d (n "nanoid") (r "^0.4.0") (d #t) (k 0)))) (h "1fihfyyp3sw4hfwwf6y91gq61x1qvjqqv2w4gd6xnabpq0z41anh") (y #t) (r "1.56")))

(define-public crate-slugify-rs-0.0.3 (c (n "slugify-rs") (v "0.0.3") (d (list (d (n "deunicode") (r "^1.3.1") (d #t) (k 0)) (d (n "nanoid") (r "^0.4.0") (d #t) (k 0)))) (h "1wxzpmzaj0k4sql6h4vb70jcwwr37cv3lz16dzg1vnlllxpdp368") (r "1.56")))

