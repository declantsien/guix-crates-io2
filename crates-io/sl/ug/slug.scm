(define-module (crates-io sl ug slug) #:use-module (crates-io))

(define-public crate-slug-0.1.0 (c (n "slug") (v "0.1.0") (d (list (d (n "unidecode") (r "*") (d #t) (k 0)))) (h "1jkr8b4rkk0y3wb21c7q4230j172rdbqvx08z8iawb2z9s8mc5fz")))

(define-public crate-slug-0.1.1 (c (n "slug") (v "0.1.1") (d (list (d (n "unidecode") (r "^0.2") (d #t) (k 0)))) (h "1wz9ldljfb968fkj0dh1gi4gnl3nalk6d8ygky5kq9d1i3l1rbrr")))

(define-public crate-slug-0.1.2 (c (n "slug") (v "0.1.2") (d (list (d (n "unidecode") (r "^0.2") (d #t) (k 0)))) (h "1f4c681r8b4f75gdi9r7bagdyb522hkwjdljbxnbh1yb8d5zzxgn")))

(define-public crate-slug-0.1.3 (c (n "slug") (v "0.1.3") (d (list (d (n "unidecode") (r "^0.3") (d #t) (k 0)))) (h "04cyd2ksa0zqfcydwwn402j8qbgv997q25alc6l3j4p94i6wnyvr")))

(define-public crate-slug-0.1.4 (c (n "slug") (v "0.1.4") (d (list (d (n "deunicode") (r "^0.4.0") (d #t) (k 0)))) (h "0wrk0w7mcmnvpmc27fw8dxkip6f6xgwpfgp7mp56yv2bd8p7dg5k")))

(define-public crate-slug-0.1.5 (c (n "slug") (v "0.1.5") (d (list (d (n "deunicode") (r "^1") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (t "cfg(target_family = \"wasm\")") (k 0)) (d (n "wasm-bindgen-test") (r "^0.3") (d #t) (t "cfg(target_family = \"wasm\")") (k 2)))) (h "1i68hkvpbf04ga5kcssyads2wdy0kyikbqgq0l069nn8r774mn9v")))

