(define-module (crates-io sl ug slugid) #:use-module (crates-io))

(define-public crate-slugid-1.0.0 (c (n "slugid") (v "1.0.0") (d (list (d (n "base64") (r "^0.12.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "ring") (r "^0.16.15") (d #t) (k 0)))) (h "1zcjq4fx0w523py69fs5jm7bs0ln5j84ha6p5mp5kwm019awxzvx")))

(define-public crate-slugid-1.0.1 (c (n "slugid") (v "1.0.1") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "ring") (r "^0.16.15") (d #t) (k 0)))) (h "1ssv5kwfjl6imznwhsyk2pmz5s7gpljvjv4lf0xjnjnlrspak5x7")))

