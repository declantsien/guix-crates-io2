(define-module (crates-io sl ug slugmin) #:use-module (crates-io))

(define-public crate-slugmin-1.0.0 (c (n "slugmin") (v "1.0.0") (d (list (d (n "deunicode") (r "^1") (d #t) (k 0)))) (h "19c4kihhczhkpvw256zw7y5bwmqlpx7wd40pf8p4c5b2cljqaw9c")))

(define-public crate-slugmin-1.0.1 (c (n "slugmin") (v "1.0.1") (d (list (d (n "deunicode") (r "^1") (d #t) (k 0)))) (h "01ifk40hv9mqvgk6w7lamgcbipcz42k790xibg2v881ism5larw5")))

(define-public crate-slugmin-1.0.2 (c (n "slugmin") (v "1.0.2") (d (list (d (n "deunicode") (r "^1") (d #t) (k 0)))) (h "0vsvx0x4xspbhnmqc4wg38v32l83kmscw6rkqz3x2rz1avmbgkyq")))

