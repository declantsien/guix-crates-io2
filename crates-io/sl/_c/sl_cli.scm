(define-module (crates-io sl _c sl_cli) #:use-module (crates-io))

(define-public crate-sl_cli-1.0.0 (c (n "sl_cli") (v "1.0.0") (d (list (d (n "clap") (r "^3.0.0-beta.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.71") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.19") (d #t) (k 0)))) (h "0lva9qrb2mjb4klnini4c4spmh390qi2piywzyjj83i8w6v9pkna")))

