(define-module (crates-io sl y_ sly_static) #:use-module (crates-io))

(define-public crate-sly_static-1.0.0 (c (n "sly_static") (v "1.0.0") (d (list (d (n "linkme") (r "^0.3.23") (d #t) (k 0)) (d (n "sly_static_macros") (r "^1.0.0") (d #t) (k 0)))) (h "06d0g5i9cqjrm9y1cxvzhx0b56hcbal5qghrdha6jgqxrv3868kw")))

(define-public crate-sly_static-1.0.1 (c (n "sly_static") (v "1.0.1") (d (list (d (n "linkme") (r "^0.3.23") (d #t) (k 0)) (d (n "sly_static_macros") (r "^1.0.0") (d #t) (k 0)))) (h "1ih922ma0c15rz06v28b0ksxvb6rnnf9spdnw8fdkaayz3fm691v")))

(define-public crate-sly_static-1.0.2 (c (n "sly_static") (v "1.0.2") (d (list (d (n "linkme") (r "^0.3.23") (d #t) (k 0)) (d (n "sly_static_macros") (r "^1.0.0") (d #t) (k 0)))) (h "17jyfh94kgycjd9lz8z830h500z75hnirf88ixq4frn5dx3z99r8")))

(define-public crate-sly_static-1.0.3 (c (n "sly_static") (v "1.0.3") (d (list (d (n "linkme") (r "^0.3.23") (d #t) (k 0)) (d (n "sly_static_macros") (r "^1.0.0") (d #t) (k 0)))) (h "0a48yx4nz8yn0qb285wnlrf09vhl4yqyk5s6ypfvhcrq10l843bn")))

(define-public crate-sly_static-1.0.4 (c (n "sly_static") (v "1.0.4") (d (list (d (n "linkme") (r "^0.3.23") (d #t) (k 0)) (d (n "sly_static_macros") (r "^1.0.4") (d #t) (k 0)))) (h "0cf2vlgd4jmzhkssrhj7sw5k2kd8gx6y2qhmj7dl8xsykqs7d2lr")))

(define-public crate-sly_static-1.0.5 (c (n "sly_static") (v "1.0.5") (d (list (d (n "linkme") (r "^0.3.23") (d #t) (k 0)) (d (n "sly_static_macros") (r "^1.0.5") (d #t) (k 0)))) (h "177z91n1mgbcm2rc87y1j052mabvwa2vxx55dm935jykz0w19xs1")))

