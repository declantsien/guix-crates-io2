(define-module (crates-io sl y_ sly_static_macros) #:use-module (crates-io))

(define-public crate-sly_static_macros-1.0.0 (c (n "sly_static_macros") (v "1.0.0") (d (list (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.50") (f (quote ("full"))) (d #t) (k 0)))) (h "0si6wcmis5ayc40x3cknipayb114fskqq7f9905jljlrknn842fr")))

(define-public crate-sly_static_macros-1.0.1 (c (n "sly_static_macros") (v "1.0.1") (d (list (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.50") (f (quote ("full"))) (d #t) (k 0)))) (h "084jhh9sfq8211vcna4pgwshrdvsf4fla1v7kbcb3zhfvibad293")))

(define-public crate-sly_static_macros-1.0.2 (c (n "sly_static_macros") (v "1.0.2") (d (list (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.50") (f (quote ("full"))) (d #t) (k 0)))) (h "0y3isf2bnnlqd1vz6k4wwhscxnhwx3wp1n76nh78b2xmm3n2z3vz")))

(define-public crate-sly_static_macros-1.0.3 (c (n "sly_static_macros") (v "1.0.3") (d (list (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.50") (f (quote ("full"))) (d #t) (k 0)))) (h "19hk12i7rf0jnxp4gd8qyclz2sb857fwlm5iaya4wyw7v8na555n")))

(define-public crate-sly_static_macros-1.0.4 (c (n "sly_static_macros") (v "1.0.4") (d (list (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.50") (f (quote ("full"))) (d #t) (k 0)))) (h "1swmmllyb0109svlvhplgp8d96li4p2l5f5gqgj95qcf82bd8196")))

(define-public crate-sly_static_macros-1.0.5 (c (n "sly_static_macros") (v "1.0.5") (d (list (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.50") (f (quote ("full"))) (d #t) (k 0)))) (h "0ngxhkxn06labw09szzj142kz3wsnc9swpmix86nr4qpwwcf7g0g")))

