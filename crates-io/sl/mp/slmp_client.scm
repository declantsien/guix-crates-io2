(define-module (crates-io sl mp slmp_client) #:use-module (crates-io))

(define-public crate-slmp_client-0.1.0 (c (n "slmp_client") (v "0.1.0") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "1s35ax81simq6v58xwbr1xbhc4034fi4zbmihb4yp1f0rygm1mrb")))

(define-public crate-slmp_client-0.1.1 (c (n "slmp_client") (v "0.1.1") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "01wxh7vwhn83rnr138ayw95k7lh7wb8jpyhig7yh184z4v1s9fdk")))

(define-public crate-slmp_client-0.2.0 (c (n "slmp_client") (v "0.2.0") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "1n7g45ig1y5zl5a80jcknzxdnn9rhsvvczkb5887j9wq745pf0yc")))

(define-public crate-slmp_client-0.2.1 (c (n "slmp_client") (v "0.2.1") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "1qb66kqi84qlsimwqs2bl78pidkih892gxd13wd760ivid2ld1fj")))

