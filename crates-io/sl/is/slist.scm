(define-module (crates-io sl is slist) #:use-module (crates-io))

(define-public crate-slist-0.1.0 (c (n "slist") (v "0.1.0") (d (list (d (n "slist-derive") (r "^0.1.0") (d #t) (k 0)))) (h "12w6bf5gd23iissx2rizhdvm7yigbpw2cp9a5lq9kpy64l0a0vcg")))

(define-public crate-slist-0.1.1 (c (n "slist") (v "0.1.1") (d (list (d (n "slist-derive") (r "^0.1.0") (d #t) (k 0)))) (h "0r6rmrnbdvgnvys819l5bx4b4s0fg6f9ankzllgyp9l6bw81f5wm")))

(define-public crate-slist-0.1.2 (c (n "slist") (v "0.1.2") (d (list (d (n "slist-derive") (r "^0.1.0") (d #t) (k 0)))) (h "1j5iwy81q5qbfmj15q9k5nnn8if19cpncsja6xvy6qy1wavx930h")))

