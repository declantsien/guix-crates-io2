(define-module (crates-io sl -u sl-up) #:use-module (crates-io))

(define-public crate-sl-up-0.1.0 (c (n "sl-up") (v "0.1.0") (d (list (d (n "ansi-parser") (r "^0.8.0") (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "enum_dispatch") (r "^0.3.12") (d #t) (k 0)))) (h "1g3i6xd1llijj14hvdk7cr7smashalqdyz83hbjqyd0inrdcfakr")))

(define-public crate-sl-up-0.1.1 (c (n "sl-up") (v "0.1.1") (d (list (d (n "ansi-parser") (r "^0.8.0") (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "enum_dispatch") (r "^0.3.12") (d #t) (k 0)))) (h "1ifz8dsh8vjpyavmw37b5x6p4hnq28lndipg8scccf6ngrh301by")))

