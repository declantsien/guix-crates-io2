(define-module (crates-io sl ow slow_nn) #:use-module (crates-io))

(define-public crate-slow_nn-0.1.0 (c (n "slow_nn") (v "0.1.0") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "1nsshmghczlhm8ks440085q5xl0mzld53g0i51cirvj7mkjl4ap6")))

(define-public crate-slow_nn-0.1.1 (c (n "slow_nn") (v "0.1.1") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0apfixpcv9yv4hk00xf691mb3386yy2llr4h6qbjrk76fcks54hy")))

(define-public crate-slow_nn-0.1.2 (c (n "slow_nn") (v "0.1.2") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.106") (f (quote ("derive"))) (d #t) (k 0)))) (h "0n7gbdkhrccga7rbm09cgsmsjp3x668gqd6vbb3vgnc7a3kfqaq7")))

(define-public crate-slow_nn-0.1.21 (c (n "slow_nn") (v "0.1.21") (d (list (d (n "bincode") (r "^1.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.106") (f (quote ("derive"))) (d #t) (k 0)))) (h "1pawh888g6cjh7i1w1p27j31wginbbrdj8kh3mghnzf2i4i1hrrl")))

(define-public crate-slow_nn-0.1.22 (c (n "slow_nn") (v "0.1.22") (d (list (d (n "bincode") (r "^1.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.106") (f (quote ("derive"))) (d #t) (k 0)))) (h "0lrpxw62ahimgj4yjn1kib01c79wvavws1j07j3lrgqac6l7m8sb")))

