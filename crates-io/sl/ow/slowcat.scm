(define-module (crates-io sl ow slowcat) #:use-module (crates-io))

(define-public crate-slowcat-1.0.0 (c (n "slowcat") (v "1.0.0") (d (list (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.8") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1q8sj62hbgdiy6f1h5jfzzlf90ybw6i7n4lwykjglfcpmyp5r2nl")))

