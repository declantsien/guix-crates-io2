(define-module (crates-io sl ow slow_primes) #:use-module (crates-io))

(define-public crate-slow_primes-0.1.0 (c (n "slow_primes") (v "0.1.0") (d (list (d (n "num") (r ">= 0.0.0") (d #t) (k 0)))) (h "1gnyfxrpnd4a1ynlkvg41v5kn06zc1xkd3p8dxhah5xx7j8nn4ax")))

(define-public crate-slow_primes-0.1.1 (c (n "slow_primes") (v "0.1.1") (d (list (d (n "num") (r ">= 0.0.0") (d #t) (k 0)))) (h "1ihcsb1vmiag2m44phjin6gxflnq612zb9nldvlsc4l3pzd03hph")))

(define-public crate-slow_primes-0.1.2 (c (n "slow_primes") (v "0.1.2") (d (list (d (n "num") (r ">= 0.0.0") (d #t) (k 0)))) (h "0kz12bzyglx30d4w91igvpg4mbawnmhzaasn4qsan3s95aazak61")))

(define-public crate-slow_primes-0.1.3 (c (n "slow_primes") (v "0.1.3") (d (list (d (n "num") (r "~0.0.3") (d #t) (k 0)))) (h "0lxp2p1ga55hlbdjsm8vm5cc3z4qkhpfw4g8x48nadhyy84hnajs")))

(define-public crate-slow_primes-0.1.4 (c (n "slow_primes") (v "0.1.4") (d (list (d (n "num") (r "~0.0.3") (d #t) (k 0)))) (h "11hmxan535m8nw19c3jibirk9p0id8qm2ny1hyj5c25xv9pljb4m")))

(define-public crate-slow_primes-0.1.5 (c (n "slow_primes") (v "0.1.5") (d (list (d (n "num") (r "~0") (d #t) (k 0)))) (h "0r7c4ilr35zz0z1x3qj1dr512nmg5w5d8vpch9rxfm7ns9j3fpk6")))

(define-public crate-slow_primes-0.1.6 (c (n "slow_primes") (v "0.1.6") (d (list (d (n "num") (r "~0") (d #t) (k 0)))) (h "01j2m7vyn9iacfznbdhk9677arqiw1j1fyvijdjb538q27s0bvc6")))

(define-public crate-slow_primes-0.1.7 (c (n "slow_primes") (v "0.1.7") (d (list (d (n "num") (r "~0") (d #t) (k 0)))) (h "0wx4a8idkvmr40h2dgzdihr3vcwmm6cx6cp3ixjzr2ry5d029b0i")))

(define-public crate-slow_primes-0.1.8 (c (n "slow_primes") (v "0.1.8") (d (list (d (n "num") (r "~0") (d #t) (k 0)))) (h "0j78ggdx84kw0zjxg692d8l4hqzn2vpjwbxngikd2lz6w0822q5c")))

(define-public crate-slow_primes-0.1.9 (c (n "slow_primes") (v "0.1.9") (d (list (d (n "num") (r "~0") (d #t) (k 0)))) (h "0pyivj0dkvbq7nixbbfja4ray7hqaahq15ffrwbdydxd79sjzhq0")))

(define-public crate-slow_primes-0.1.10 (c (n "slow_primes") (v "0.1.10") (d (list (d (n "num") (r "~0") (d #t) (k 0)))) (h "17dc3jfxvgm5chm3kcs8nfvix662rymmb8hivdigwllrqhrd7q3s")))

(define-public crate-slow_primes-0.1.11 (c (n "slow_primes") (v "0.1.11") (d (list (d (n "num") (r "~0") (d #t) (k 0)))) (h "0f2khp44lqxmljhcpnpz7ga7bi5v5nv336a2n11yclqdg3fizcqf")))

(define-public crate-slow_primes-0.1.12 (c (n "slow_primes") (v "0.1.12") (d (list (d (n "num") (r "~0") (d #t) (k 0)))) (h "03qwb0wsbq596dxx1mr06yv7bhz0pxfwam6pzxdy7mq5frzgbb2i")))

(define-public crate-slow_primes-0.1.13 (c (n "slow_primes") (v "0.1.13") (d (list (d (n "num") (r "~0") (d #t) (k 0)))) (h "1vjws99xbq4j3g3606yx0ypmc6yjqlmcl1lhl0wc4v8aj0kmc77c") (f (quote (("unstable"))))))

(define-public crate-slow_primes-0.1.14 (c (n "slow_primes") (v "0.1.14") (d (list (d (n "num") (r "~0") (d #t) (k 0)))) (h "0f1r27swrgjrhwdv1s60jwjb18lh5mni0glypbncwvdazg97s9jq") (f (quote (("unstable"))))))

