(define-module (crates-io sl ow slowlock) #:use-module (crates-io))

(define-public crate-slowlock-0.1.0 (c (n "slowlock") (v "0.1.0") (d (list (d (n "aead") (r "^0.4.1") (d #t) (k 0)) (d (n "aes-gcm") (r "^0.9.1") (d #t) (k 2)) (d (n "hex-literal") (r "^0.3.1") (d #t) (k 2)) (d (n "log") (r "^0.4.14") (o #t) (d #t) (k 0)) (d (n "rust-argon2") (r "^0.8.3") (d #t) (k 0)) (d (n "secrecy") (r "^0.7.0") (d #t) (k 0)) (d (n "simple_logger") (r "^1.11.0") (d #t) (k 2)) (d (n "sysinfo") (r "^0.17.3") (d #t) (k 0)))) (h "1bfryzlfm62d61kq59p3g034pk5a1knswhpd5sa4zwaf93agza38") (f (quote (("logging" "log") ("default" "logging"))))))

(define-public crate-slowlock-0.2.0 (c (n "slowlock") (v "0.2.0") (d (list (d (n "aead") (r "^0.4.1") (d #t) (k 0)) (d (n "aes-gcm") (r "^0.9.1") (d #t) (k 2)) (d (n "hex-literal") (r "^0.3.1") (d #t) (k 2)) (d (n "log") (r "^0.4.14") (o #t) (d #t) (k 0)) (d (n "rust-argon2") (r "^0.8.3") (d #t) (k 0)) (d (n "secrecy") (r "^0.7.0") (d #t) (k 0)) (d (n "simple_logger") (r "^1.11.0") (d #t) (k 2)) (d (n "sysinfo") (r "^0.17.3") (d #t) (k 0)))) (h "0a9iy3r4ggv33pq73kprhg5w2vq1wasncxnp4q34hh8m0mwhgw51") (f (quote (("logging" "log") ("default" "logging"))))))

