(define-module (crates-io sl ow slowlorust) #:use-module (crates-io))

(define-public crate-slowlorust-0.1.0 (c (n "slowlorust") (v "0.1.0") (d (list (d (n "clap") (r "^3.0.0-beta.5") (d #t) (k 0)) (d (n "colog") (r "^1.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "rand") (r "^0.5.0") (d #t) (k 0)) (d (n "scoped_threadpool") (r "0.1.*") (d #t) (k 0)))) (h "1rc7lkv64yf0lanj7wh5xl5k1zn5gkn3v1acyvvx8wm3v9s67gzc") (y #t)))

(define-public crate-slowlorust-0.1.1 (c (n "slowlorust") (v "0.1.1") (d (list (d (n "clap") (r "^3.0.0-beta.5") (d #t) (k 0)) (d (n "colog") (r "^1.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "rand") (r "^0.5.0") (d #t) (k 0)) (d (n "scoped_threadpool") (r "0.1.*") (d #t) (k 0)))) (h "147jm5mqbr9647mbscnp1089cc3wxx7h9ragjajy2ya1s4hf4l7m")))

