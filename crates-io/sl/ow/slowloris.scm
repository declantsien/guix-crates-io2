(define-module (crates-io sl ow slowloris) #:use-module (crates-io))

(define-public crate-slowloris-0.1.1 (c (n "slowloris") (v "0.1.1") (d (list (d (n "native-tls") (r "^0.1.4") (d #t) (k 0)) (d (n "rand") (r "^0.3.17") (d #t) (k 0)) (d (n "rayon") (r "^0.8.2") (d #t) (k 0)) (d (n "url") (r "^1.6.0") (d #t) (k 0)))) (h "0nsikgkny8r8nmrbk9nkwp14d2sc61j4qj4nc1yrvmsdrqps80ym")))

(define-public crate-slowloris-0.1.2 (c (n "slowloris") (v "0.1.2") (d (list (d (n "native-tls") (r "^0.1.4") (d #t) (k 0)) (d (n "rand") (r "^0.3.17") (d #t) (k 0)) (d (n "rayon") (r "^0.8.2") (d #t) (k 0)) (d (n "url") (r "^1.6.0") (d #t) (k 0)))) (h "1lyjirlji93fnl8pvb5j4hbxq0z8y4w8nncixpn8r9msj92p369b")))

(define-public crate-slowloris-0.2.0 (c (n "slowloris") (v "0.2.0") (d (list (d (n "clap") (r "^2.27.1") (d #t) (k 0)) (d (n "native-tls") (r "^0.1.4") (d #t) (k 0)) (d (n "rand") (r "^0.3.17") (d #t) (k 0)) (d (n "rayon") (r "^0.8.2") (d #t) (k 0)) (d (n "url") (r "^1.6.0") (d #t) (k 0)))) (h "03m9j2rv2r0yc9cq2rd7qi8bgfblh3s5ylq5hk2pb6vgn2bzb5fp")))

(define-public crate-slowloris-1.0.0 (c (n "slowloris") (v "1.0.0") (d (list (d (n "clap") (r "^2.27.1") (d #t) (k 0)) (d (n "native-tls") (r "^0.1.4") (d #t) (k 0)) (d (n "rand") (r "^0.3.17") (d #t) (k 0)) (d (n "rayon") (r "^0.8.2") (d #t) (k 0)) (d (n "url") (r "^1.6.0") (d #t) (k 0)))) (h "11q108vvr2l32wgy8adpx6l8fl5pflckapspmd36vbgn1n5y8bnl")))

(define-public crate-slowloris-1.1.0 (c (n "slowloris") (v "1.1.0") (d (list (d (n "clap") (r "^2.27.1") (d #t) (k 0)) (d (n "native-tls") (r "^0.1.4") (d #t) (k 0)) (d (n "rand") (r "^0.3.17") (d #t) (k 0)) (d (n "rayon") (r "^0.8.2") (d #t) (k 0)) (d (n "url") (r "^1.6.0") (d #t) (k 0)))) (h "1n5mv98i9028aa739lcp5bmidq1fp8djilk0nnyd0izch8hrnx4z")))

(define-public crate-slowloris-1.2.0 (c (n "slowloris") (v "1.2.0") (d (list (d (n "clap") (r "^2.27.1") (d #t) (k 0)) (d (n "native-tls") (r "^0.1.4") (d #t) (k 0)) (d (n "rand") (r "^0.3.17") (d #t) (k 0)) (d (n "rayon") (r "^0.8.2") (d #t) (k 0)) (d (n "url") (r "^1.6.0") (d #t) (k 0)))) (h "11yrxcpzkqj5d83k9mz65gpa17acip7w64wliqj268072zlxik8p")))

(define-public crate-slowloris-1.2.1 (c (n "slowloris") (v "1.2.1") (d (list (d (n "clap") (r "^3.1.17") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "native-tls") (r "^0.2.10") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rayon") (r "^1.5.2") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "10zhk5py3yh0r7qhbsk9mk3k6nnxwiksdlhzkbplidx8r8bv0q1f")))

