(define-module (crates-io sl ow slowchop_console_derive) #:use-module (crates-io))

(define-public crate-slowchop_console_derive-0.1.0 (c (n "slowchop_console_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.70") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1485z006rm5bmbk3b9vgz1qv5f2j2xs8hmh4xbq0c97kkh51a6xz")))

(define-public crate-slowchop_console_derive-0.1.1 (c (n "slowchop_console_derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.70") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1xgr6m0sqm92690siy76aip5xzzagl45p1122116pwqnhcd6b95q")))

(define-public crate-slowchop_console_derive-0.1.2 (c (n "slowchop_console_derive") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.70") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1371z7ih4bs60zgf49xrc4cpjrf5fngfxpsry7wqj7cxfh5d6lqp")))

(define-public crate-slowchop_console_derive-0.1.3 (c (n "slowchop_console_derive") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0.70") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0z9j6174h70k4jyspvvgvs3fjy8s61mj563kgziv8fg78fsxwkll")))

