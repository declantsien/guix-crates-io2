(define-module (crates-io sl r_ slr_parser) #:use-module (crates-io))

(define-public crate-slr_parser-0.0.14 (c (n "slr_parser") (v "0.0.14") (h "0z664z46nnrz73hfrg632byrgvdmr8g66805j9ysnb16axrkv6b8")))

(define-public crate-slr_parser-0.0.15 (c (n "slr_parser") (v "0.0.15") (h "1kblzj9j4i0cacrj724r4sld8wxfcxnkf02k5xvvawcfilj0dnhh")))

(define-public crate-slr_parser-0.0.16 (c (n "slr_parser") (v "0.0.16") (h "0xh5g76nzrmfs6z8m354d36p88b5xd1xnl1bbp05jwv3f1g6r3zh")))

(define-public crate-slr_parser-0.0.17 (c (n "slr_parser") (v "0.0.17") (h "0akzxlbfr2s3knkv7agm6wcn7qxr66f1j3bsnn012ig65fn5n8xk")))

(define-public crate-slr_parser-0.0.18 (c (n "slr_parser") (v "0.0.18") (d (list (d (n "serde") (r "= 1.0.82") (d #t) (k 0)))) (h "1h6imywy6nwfny0fkf7gd1mvbzk6ybk2lvx350kfki0mcpqj4jhh")))

(define-public crate-slr_parser-0.0.19 (c (n "slr_parser") (v "0.0.19") (d (list (d (n "serde") (r "^1.0.104") (d #t) (k 0)))) (h "02im8c1q6fwq9nfvwfsq9r0slyk8vywyh0xv2k4bmhdm4abfv9xn")))

(define-public crate-slr_parser-0.0.20 (c (n "slr_parser") (v "0.0.20") (d (list (d (n "serde") (r "^1.0.104") (d #t) (k 0)))) (h "0lxlcg0afwpdacxnn2k87mn3j26crjjh2xqnq1vgfdnf7z0cmn55")))

(define-public crate-slr_parser-0.0.21 (c (n "slr_parser") (v "0.0.21") (d (list (d (n "serde") (r "^1.0.104") (d #t) (k 0)))) (h "1ck35xjdycfz0wrzvvifpiy3id31kkcv6hshxnl47xx51k3cig2v")))

(define-public crate-slr_parser-0.0.22 (c (n "slr_parser") (v "0.0.22") (d (list (d (n "serde") (r "^1.0.104") (d #t) (k 0)))) (h "1fsd2aqid45kfdrm8166i0khl3i7rn9c3xhgncwssrbkbcjflj63")))

