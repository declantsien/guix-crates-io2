(define-module (crates-io sl r_ slr_config) #:use-module (crates-io))

(define-public crate-slr_config-0.0.1 (c (n "slr_config") (v "0.0.1") (d (list (d (n "slr_lexer") (r "= 0.0.1") (d #t) (k 0)))) (h "1xc8fbriw14p8bsvm8c83i04azpjxb8sdpb20cfcs2bi0826jpjw")))

(define-public crate-slr_config-0.0.2 (c (n "slr_config") (v "0.0.2") (d (list (d (n "slr_lexer") (r "= 0.0.2") (d #t) (k 0)))) (h "0179f27hq0g18sw4rgy6yzf1nrf14ay3hj207g27bdvxalffz1cx")))

(define-public crate-slr_config-0.0.3 (c (n "slr_config") (v "0.0.3") (d (list (d (n "slr_lexer") (r "= 0.0.3") (d #t) (k 0)))) (h "0sr687id7asl6f0ikjlcr8v9n5n77vp78gn387fhs2ijhf7dnkhp")))

(define-public crate-slr_config-0.0.4 (c (n "slr_config") (v "0.0.4") (d (list (d (n "slr_lexer") (r "= 0.0.4") (d #t) (k 0)))) (h "1xz6wdvsaqpc12w4c8wny5mn3f970s9pdmzaf7k1aqsb1074i8fy")))

(define-public crate-slr_config-0.0.5 (c (n "slr_config") (v "0.0.5") (d (list (d (n "slr_lexer") (r "= 0.0.5") (d #t) (k 0)))) (h "1w68djf39s22y4j71cl95rpmd0dm913g89f0fhxjn45h3n02r0qq")))

(define-public crate-slr_config-0.0.6 (c (n "slr_config") (v "0.0.6") (d (list (d (n "slr_lexer") (r "= 0.0.6") (d #t) (k 0)))) (h "09pihzbiyalh63v1blwp61vgmr83jfjd4v86j2dhd4hqnh31vi5v")))

(define-public crate-slr_config-0.0.7 (c (n "slr_config") (v "0.0.7") (d (list (d (n "slr_lexer") (r "= 0.0.7") (d #t) (k 0)))) (h "1f1haapahm0yb0ljhca17jmah1yz72zmz5iwhsa5l27zhqf8642n")))

(define-public crate-slr_config-0.0.8 (c (n "slr_config") (v "0.0.8") (d (list (d (n "slr_lexer") (r "= 0.0.8") (d #t) (k 0)))) (h "0kyy2zjv7zf9i70ll92b5h7ivjg3bryvqmh35a1wnjb4izivc2vb")))

(define-public crate-slr_config-0.0.9 (c (n "slr_config") (v "0.0.9") (d (list (d (n "slr_lexer") (r "= 0.0.9") (d #t) (k 0)))) (h "0i5kbmz6rkq1ldmnfw2jy8kgvsydspvv5kq4qsm3sanmlahlcr19")))

(define-public crate-slr_config-0.0.10 (c (n "slr_config") (v "0.0.10") (d (list (d (n "slr_lexer") (r "= 0.0.10") (d #t) (k 0)))) (h "0dwrbmm5wisznrg6wyxihwvh6kahzpxp84r0x89h2zhjh5lw6q3v")))

(define-public crate-slr_config-0.0.11 (c (n "slr_config") (v "0.0.11") (d (list (d (n "slr_lexer") (r "= 0.0.11") (d #t) (k 0)))) (h "0a3whbj3x2z9ffskk3cj4cjy0l6cfvqyrhb79j0nzgxga4yqzwsz")))

(define-public crate-slr_config-0.0.12 (c (n "slr_config") (v "0.0.12") (d (list (d (n "slr_lexer") (r "= 0.0.12") (d #t) (k 0)))) (h "0qj9b32wci63237j3qpx67dnzqj41llzr5i992d76bnf80yb4bbx")))

(define-public crate-slr_config-0.0.13 (c (n "slr_config") (v "0.0.13") (d (list (d (n "slr_lexer") (r "= 0.0.13") (d #t) (k 0)))) (h "0qlhghf0g61g8h1p608jprc2fbgsv9lvxyr25r0ppxr99yncmbr9")))

(define-public crate-slr_config-0.0.14 (c (n "slr_config") (v "0.0.14") (d (list (d (n "slr_parser") (r "= 0.0.14") (d #t) (k 0)))) (h "1s6sr69n4zwb4ygdybjkm64kxs7jz9gskfmpl5hwnvdbcrl11m14")))

(define-public crate-slr_config-0.0.15 (c (n "slr_config") (v "0.0.15") (d (list (d (n "slr_parser") (r "= 0.0.15") (d #t) (k 0)))) (h "1y0yqn5y9gfs6sqfc8ymq2akjk17vkv4xnzkyqcswnxny0s3454p")))

(define-public crate-slr_config-0.0.16 (c (n "slr_config") (v "0.0.16") (d (list (d (n "slr_parser") (r "= 0.0.16") (d #t) (k 0)))) (h "110qqj90g9d69wrhms2wx8naicnnj097i01sm42yqr6sm61vrs94")))

(define-public crate-slr_config-0.0.17 (c (n "slr_config") (v "0.0.17") (d (list (d (n "slr_parser") (r "= 0.0.17") (d #t) (k 0)))) (h "0s2f9c4g773ka6kjcfb53zfl6pzyzmp2faw00mfkrk4dvp8md6y3")))

(define-public crate-slr_config-0.0.18 (c (n "slr_config") (v "0.0.18") (d (list (d (n "serde") (r "= 1.0.82") (d #t) (k 0)) (d (n "serde_derive") (r "= 1.0.82") (d #t) (k 0)) (d (n "slr_parser") (r "= 0.0.18") (d #t) (k 0)))) (h "0vvcvznv0l0wwpixqhkb284m4n7k9ammpd9snsl4rbknxzpqa1aj")))

(define-public crate-slr_config-0.0.19 (c (n "slr_config") (v "0.0.19") (d (list (d (n "serde") (r "^1.0.104") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.104") (d #t) (k 0)) (d (n "slr_parser") (r "= 0.0.19") (d #t) (k 0)))) (h "07y3qzm0qdfcgv5j8npmggfgka2zq7ryap1bcl9k0halajp6bsr2")))

(define-public crate-slr_config-0.0.20 (c (n "slr_config") (v "0.0.20") (d (list (d (n "serde") (r "^1.0.104") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.104") (d #t) (k 0)) (d (n "slr_parser") (r "= 0.0.20") (d #t) (k 0)))) (h "15j1ss8p6ci7jgw7vq9473xyl5658qn4qh30vb5r8vrzhg6jg3kx")))

(define-public crate-slr_config-0.0.21 (c (n "slr_config") (v "0.0.21") (d (list (d (n "indexmap") (r "^1.9.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.104") (d #t) (k 0)) (d (n "slr_parser") (r "=0.0.21") (d #t) (k 0)))) (h "0agq54md67x6zqk26xix0fi08d551xih6ri5akflxyx3b711zyyg")))

(define-public crate-slr_config-0.0.22 (c (n "slr_config") (v "0.0.22") (d (list (d (n "indexmap") (r "^1.9.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.104") (d #t) (k 0)) (d (n "slr_parser") (r "=0.0.22") (d #t) (k 0)))) (h "0nknyb72kw70xwbp01fcydmf9zg6087lyqslk6dcn6ir4lz142j5")))

