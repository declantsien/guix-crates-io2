(define-module (crates-io sl r_ slr_lexer) #:use-module (crates-io))

(define-public crate-slr_lexer-0.0.1 (c (n "slr_lexer") (v "0.0.1") (h "0vyq9g509fcrgdwc8zxivbby18zn41k5m0zg5ih9kq962qb3iai6")))

(define-public crate-slr_lexer-0.0.2 (c (n "slr_lexer") (v "0.0.2") (h "0x2264rm5s50sgsx94zhqpfy59137gp4888l9m6w9fh6ixjlh44i")))

(define-public crate-slr_lexer-0.0.3 (c (n "slr_lexer") (v "0.0.3") (h "0khxijh4fv21z6kjr70jqlf9mbx2pzlgn8j7m9r8m65ys6gi3891")))

(define-public crate-slr_lexer-0.0.4 (c (n "slr_lexer") (v "0.0.4") (h "0z9xy2xlcx9z20nmf71wp0ij5p2573xlifdx2xdpcwwd4wpv76qs")))

(define-public crate-slr_lexer-0.0.5 (c (n "slr_lexer") (v "0.0.5") (h "0s9xmg8hw36pmmwfvvf2i7hzv02d8n5w716v47mm04s2ybk0jaq9")))

(define-public crate-slr_lexer-0.0.6 (c (n "slr_lexer") (v "0.0.6") (h "13rndp2md3rr6062harvk859w0dsdirxq3flvz6c0w5mr0byqw3v")))

(define-public crate-slr_lexer-0.0.7 (c (n "slr_lexer") (v "0.0.7") (h "0bn00yzhnirbg6dni3vschf16y7860n3x1p9n88j90r8qh7gsphj")))

(define-public crate-slr_lexer-0.0.8 (c (n "slr_lexer") (v "0.0.8") (h "1aziic1in2ilyb6x7jlcjhl8j19zi77m3a83knjbazlmwlql3bzh")))

(define-public crate-slr_lexer-0.0.9 (c (n "slr_lexer") (v "0.0.9") (h "1ykvbxxk79q3z9rf2sikdkkhjrvcgz93zys98ff5ql2k61yz9g0x")))

(define-public crate-slr_lexer-0.0.10 (c (n "slr_lexer") (v "0.0.10") (h "0rvi0dxqbalkfd5xlkr004njk7p45hfmjjl8xv6hnb7c8zcsqjbi")))

(define-public crate-slr_lexer-0.0.11 (c (n "slr_lexer") (v "0.0.11") (h "0akdcd72xlhgvgr04idrvnsfq04vp4r9sd6sbv5hx9xgs2dn3xg1")))

(define-public crate-slr_lexer-0.0.12 (c (n "slr_lexer") (v "0.0.12") (h "1by289sa9ili3gqkj9zlw4iqkx6fvqin67d8aws1f9sylgpnlixr")))

(define-public crate-slr_lexer-0.0.13 (c (n "slr_lexer") (v "0.0.13") (h "1xkiaps0qcvw4xvhjq34jb14wqk2pd0qkbmnwx6q5h8a6w1s8qrh")))

