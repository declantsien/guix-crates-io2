(define-module (crates-io sl op sloppy) #:use-module (crates-io))

(define-public crate-sloppy-0.1.0 (c (n "sloppy") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sloppy-core") (r "^0.1.0") (f (quote ("lua" "python" "nushell"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.2") (d #t) (k 0)))) (h "05lpm9iq0d5giirvx6hqd12fsp45ykkb333dycxri2pfq1s1hk5i")))

(define-public crate-sloppy-0.1.1 (c (n "sloppy") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sloppy-core") (r "^0.1.1") (f (quote ("lua" "python" "nushell"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.2") (d #t) (k 0)))) (h "06l0chs076nv4vyl0lfapng601visbjasgrm6gaclp8kgfjfiyk9")))

(define-public crate-sloppy-0.1.2 (c (n "sloppy") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sloppy-core") (r "^0.1.2") (f (quote ("lua" "python" "nushell"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.2") (d #t) (k 0)))) (h "1skvdcj40bjvz621ll95hpddg42hipyc3iva8ig3wms9fpg9l6l5")))

