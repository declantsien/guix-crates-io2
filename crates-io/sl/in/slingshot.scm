(define-module (crates-io sl in slingshot) #:use-module (crates-io))

(define-public crate-slingshot-0.1.0 (c (n "slingshot") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)))) (h "1m8vgqzd9lqv7bw54k81fw7bqr1xnc2m57afz8sk7fk7w5g4d2ic") (y #t)))

(define-public crate-slingshot-0.2.0 (c (n "slingshot") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "windows-sys") (r "^0.48") (f (quote ("Win32_Foundation" "Win32_Networking_WinSock" "Win32_System_IO" "Win32_System_Threading"))) (d #t) (t "cfg(windows)") (k 0)))) (h "08rmdww53m1ajhckvx989y8bi885aimk2p6pdl6hwjqzbc8n2jj5") (y #t)))

(define-public crate-slingshot-0.3.0 (c (n "slingshot") (v "0.3.0") (h "1biljr3vwd4m6s99q7fx3wwm1hv7qci7xk15fxaz2894dcnz9466") (y #t)))

