(define-module (crates-io sl in slink) #:use-module (crates-io))

(define-public crate-slink-0.1.0 (c (n "slink") (v "0.1.0") (d (list (d (n "isatty") (r "^0.1") (d #t) (k 0)) (d (n "pathdiff") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.7") (d #t) (k 0)) (d (n "shell-escape") (r "^0.1") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)) (d (n "xdg") (r "^2.1") (d #t) (k 0)))) (h "1p6jj822479vbhjyk7vphyjpvcyxmzm4jgqpxg5g3c0h1kmr0b6n")))

(define-public crate-slink-0.1.1 (c (n "slink") (v "0.1.1") (d (list (d (n "isatty") (r "^0.1") (d #t) (k 0)) (d (n "pathdiff") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.7") (d #t) (k 0)) (d (n "shell-escape") (r "^0.1") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)) (d (n "xdg") (r "^2.1") (d #t) (k 0)))) (h "126yzbaip8bzjriaxkw6hdn0xksz44gplwq1nsnhib04rgkpb84f")))

(define-public crate-slink-0.1.2 (c (n "slink") (v "0.1.2") (d (list (d (n "isatty") (r "^0.1") (d #t) (k 0)) (d (n "pathdiff") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.7") (d #t) (k 0)) (d (n "shell-escape") (r "^0.1") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)) (d (n "xdg") (r "^2.1") (d #t) (k 0)))) (h "0vv0x45can1gsx6526l7g0ccqkp5692i58sc24r4hlzvpvan5c26")))

(define-public crate-slink-0.2.0 (c (n "slink") (v "0.2.0") (d (list (d (n "dirs") (r "^1.0.4") (d #t) (k 0)) (d (n "isatty") (r "^0.1") (d #t) (k 0)) (d (n "pathdiff") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.7") (d #t) (k 0)) (d (n "shell-escape") (r "^0.1") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)) (d (n "xdg") (r "^2.1") (d #t) (k 0)))) (h "0a0557hfy5ngpd6za6xp267q3kswki3h60yf4j8apjha2jffnixw")))

(define-public crate-slink-0.2.1 (c (n "slink") (v "0.2.1") (d (list (d (n "dirs") (r "^1.0.4") (d #t) (k 0)) (d (n "isatty") (r "^0.1") (d #t) (k 0)) (d (n "pathdiff") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.7") (d #t) (k 0)) (d (n "shell-escape") (r "^0.1") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)) (d (n "xdg") (r "^2.1") (d #t) (k 0)))) (h "0yi17cnjdk8qmlyicj220d39fsi3xjwjgqdr9f6il14hxy6nm9wf")))

(define-public crate-slink-0.2.2 (c (n "slink") (v "0.2.2") (d (list (d (n "dirs") (r "^1.0.4") (d #t) (k 0)) (d (n "isatty") (r "^0.1") (d #t) (k 0)) (d (n "pathdiff") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.7") (d #t) (k 0)) (d (n "shell-escape") (r "^0.1") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)) (d (n "xdg") (r "^2.1") (d #t) (k 0)))) (h "0d8c89rilk3mvzsx8jxsfq52ggvmd6nb39cdm3lbijl1wlyv0drj")))

