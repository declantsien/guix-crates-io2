(define-module (crates-io sl in slingr) #:use-module (crates-io))

(define-public crate-slingr-0.1.0 (c (n "slingr") (v "0.1.0") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "colored") (r "^1.6") (d #t) (k 0)) (d (n "termios") (r "^0.3.0") (d #t) (k 0)))) (h "17myyxa87yw849hq0zg4g7mzp9lwkz31nl0zmd37v48fy3h1zlwc")))

