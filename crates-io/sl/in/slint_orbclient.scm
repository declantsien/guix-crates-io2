(define-module (crates-io sl in slint_orbclient) #:use-module (crates-io))

(define-public crate-slint_orbclient-0.1.0 (c (n "slint_orbclient") (v "0.1.0") (d (list (d (n "instant") (r "^0.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.16.0") (d #t) (k 0)) (d (n "orbclient") (r "^0.3") (d #t) (k 0)) (d (n "slint") (r "^0.3.2") (f (quote ("compat-0-3-0" "unsafe-single-threaded" "libm"))) (k 0)))) (h "089zwanp1rywql5inwlfb387gqzy4y9jc3lf7r6nqrdm62k45mwg")))

