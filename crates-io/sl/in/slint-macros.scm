(define-module (crates-io sl in slint-macros) #:use-module (crates-io))

(define-public crate-slint-macros-0.2.0 (c (n "slint-macros") (v "0.2.0") (d (list (d (n "i-slint-compiler") (r "=0.2.0") (f (quote ("proc_macro_span" "rust" "display-diagnostics"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.17") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "spin_on") (r "^0.1") (d #t) (k 0)))) (h "0yj1a3xi2h83nnngrk2inj7r7k6s08njd6lw74cpnimmzzlccl52")))

(define-public crate-slint-macros-0.2.1 (c (n "slint-macros") (v "0.2.1") (d (list (d (n "i-slint-compiler") (r "=0.2.1") (f (quote ("proc_macro_span" "rust" "display-diagnostics"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.17") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "spin_on") (r "^0.1") (d #t) (k 0)))) (h "0jh9msybdh9rc0ws3pcfmc7a03lpm2y2vqgscp11k16q6gfzw2is")))

(define-public crate-slint-macros-0.2.2 (c (n "slint-macros") (v "0.2.2") (d (list (d (n "i-slint-compiler") (r "=0.2.2") (f (quote ("proc_macro_span" "rust" "display-diagnostics"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.17") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "spin_on") (r "^0.1") (d #t) (k 0)))) (h "1gr8dr6fyv3rf5d70z6hki9xp6g1fcb5d12bzix5frng4r351b1z")))

(define-public crate-slint-macros-0.2.3 (c (n "slint-macros") (v "0.2.3") (d (list (d (n "i-slint-compiler") (r "=0.2.3") (f (quote ("proc_macro_span" "rust" "display-diagnostics"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.17") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "spin_on") (r "^0.1") (d #t) (k 0)))) (h "00w480mll2fg5jwqz9m5gxbjcv3v8pfv1ld674snd31vpbhdwbyc")))

(define-public crate-slint-macros-0.2.4 (c (n "slint-macros") (v "0.2.4") (d (list (d (n "i-slint-compiler") (r "=0.2.4") (f (quote ("proc_macro_span" "rust" "display-diagnostics"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.17") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "spin_on") (r "^0.1") (d #t) (k 0)))) (h "1nk6wrvx9iqpvb1ir0b63b3gv4v60jfnyfqifd1y3347praxd6nq")))

(define-public crate-slint-macros-0.2.5 (c (n "slint-macros") (v "0.2.5") (d (list (d (n "i-slint-compiler") (r "=0.2.5") (f (quote ("proc_macro_span" "rust" "display-diagnostics"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.17") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "spin_on") (r "^0.1") (d #t) (k 0)))) (h "1h6f5v7xpr9hi2mfxnf5izsg5ic6il0ipfvrr42bagd15m9bcx8m")))

(define-public crate-slint-macros-0.3.0 (c (n "slint-macros") (v "0.3.0") (d (list (d (n "i-slint-compiler") (r "=0.3.0") (f (quote ("proc_macro_span" "rust" "display-diagnostics"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.17") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "spin_on") (r "^0.1") (d #t) (k 0)))) (h "1wmdrmzc1s2khwiajdpy4rpdjnn982k7lzpgnqd1260q1fn8cxxy")))

(define-public crate-slint-macros-0.3.1 (c (n "slint-macros") (v "0.3.1") (d (list (d (n "i-slint-compiler") (r "=0.3.1") (f (quote ("proc_macro_span" "rust" "display-diagnostics"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.17") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "spin_on") (r "^0.1") (d #t) (k 0)))) (h "139shpifqp0cjkg6ijp7nldfglmailw2gcgffx0ymr05f7j44van")))

(define-public crate-slint-macros-0.3.2 (c (n "slint-macros") (v "0.3.2") (d (list (d (n "i-slint-compiler") (r "=0.3.2") (f (quote ("proc_macro_span" "rust" "display-diagnostics"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.17") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "spin_on") (r "^0.1") (d #t) (k 0)))) (h "0bv9v8iwm4nqk7hgb99wniw4kp2sbqycwffr9vbh81mf8wrmngsy")))

(define-public crate-slint-macros-0.3.3 (c (n "slint-macros") (v "0.3.3") (d (list (d (n "i-slint-compiler") (r "=0.3.3") (f (quote ("proc_macro_span" "rust" "display-diagnostics"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.17") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "spin_on") (r "^0.1") (d #t) (k 0)))) (h "12nq12c55p9vsxfdl6p9qjpgzgnz9nmvcpcjiqkg9wyrj5g6fc51")))

(define-public crate-slint-macros-0.3.4 (c (n "slint-macros") (v "0.3.4") (d (list (d (n "i-slint-compiler") (r "=0.3.4") (f (quote ("proc_macro_span" "rust" "display-diagnostics"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.17") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "spin_on") (r "^0.1") (d #t) (k 0)))) (h "1zy7xqxc22n6cy0gmy5zc37ydbiihr3p75njz5gb46cc43ssx9aj")))

(define-public crate-slint-macros-0.3.5 (c (n "slint-macros") (v "0.3.5") (d (list (d (n "i-slint-compiler") (r "=0.3.5") (f (quote ("proc_macro_span" "rust" "display-diagnostics"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.17") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "spin_on") (r "^0.1") (d #t) (k 0)))) (h "123kamm8fd0z6i52980pppdmnvi018dcn637yw3b8pjh0rc5rgmr")))

(define-public crate-slint-macros-1.0.0 (c (n "slint-macros") (v "1.0.0") (d (list (d (n "i-slint-compiler") (r "=1.0.0") (f (quote ("proc_macro_span" "rust" "display-diagnostics"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.17") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "spin_on") (r "^0.1") (d #t) (k 0)))) (h "0a87rga41gp5s9ki00n7pxv37m6w1y7imn5an0aqyn7qvyk6ja9b")))

(define-public crate-slint-macros-1.0.1 (c (n "slint-macros") (v "1.0.1") (d (list (d (n "i-slint-compiler") (r "=1.0.1") (f (quote ("proc_macro_span" "rust" "display-diagnostics"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.17") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "spin_on") (r "^0.1") (d #t) (k 0)))) (h "1pbs7l2lppcjmxp2yhir0h5rnbj8dscbl8n8aiwzpx8sna4vc0m9")))

(define-public crate-slint-macros-1.0.2 (c (n "slint-macros") (v "1.0.2") (d (list (d (n "i-slint-compiler") (r "=1.0.2") (f (quote ("proc_macro_span" "rust" "display-diagnostics"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.17") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "spin_on") (r "^0.1") (d #t) (k 0)))) (h "1vyhax357jj3nl4ihpsyyka31l031i5c88d4sp4yprf16r3dfw37")))

(define-public crate-slint-macros-1.1.0 (c (n "slint-macros") (v "1.1.0") (d (list (d (n "i-slint-compiler") (r "=1.1.0") (f (quote ("proc_macro_span" "rust" "display-diagnostics"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.17") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "spin_on") (r "^0.1") (d #t) (k 0)))) (h "1wd5b0fb7lx5qzjripimbajscmwhlkbdpdl40pcyw03mypgjnk9z")))

(define-public crate-slint-macros-1.1.1 (c (n "slint-macros") (v "1.1.1") (d (list (d (n "i-slint-compiler") (r "=1.1.1") (f (quote ("proc_macro_span" "rust" "display-diagnostics"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.17") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "spin_on") (r "^0.1") (d #t) (k 0)))) (h "0qqj6nxa2k59l9rrv6nk43g5rh4nx2agvjzya2vv3mrw173xb9q5")))

(define-public crate-slint-macros-1.2.0 (c (n "slint-macros") (v "1.2.0") (d (list (d (n "i-slint-compiler") (r "=1.2.0") (f (quote ("proc_macro_span" "rust" "display-diagnostics"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.17") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "spin_on") (r "^0.1") (d #t) (k 0)))) (h "055byk9f4kpjjk0facf3wjkbr6n1xbjjgzydk1mz5q6np067ykxr")))

(define-public crate-slint-macros-1.2.1 (c (n "slint-macros") (v "1.2.1") (d (list (d (n "i-slint-compiler") (r "=1.2.1") (f (quote ("proc_macro_span" "rust" "display-diagnostics"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.17") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "spin_on") (r "^0.1") (d #t) (k 0)))) (h "0xvmi8sn8ifm25d4csf49lk1b00mam5hvzvzwqi74ma6cg36szcp")))

(define-public crate-slint-macros-1.2.2 (c (n "slint-macros") (v "1.2.2") (d (list (d (n "i-slint-compiler") (r "=1.2.2") (f (quote ("proc_macro_span" "rust" "display-diagnostics"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.17") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "spin_on") (r "^0.1") (d #t) (k 0)))) (h "1768v313hgg62qaw2q6g0y8k4yjg291rs59vfydwcjjpl1g6xy9k")))

(define-public crate-slint-macros-1.3.0 (c (n "slint-macros") (v "1.3.0") (d (list (d (n "i-slint-compiler") (r "=1.3.0") (f (quote ("default" "proc_macro_span" "rust" "display-diagnostics"))) (k 0)) (d (n "proc-macro2") (r "^1.0.17") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "spin_on") (r "^0.1") (d #t) (k 0)))) (h "05apsry34czjclx3hxzy2hnwafk11p3f3z4r6iwnidwy714qn6zf") (f (quote (("default")))) (r "1.70")))

(define-public crate-slint-macros-1.3.1 (c (n "slint-macros") (v "1.3.1") (d (list (d (n "i-slint-compiler") (r "=1.3.1") (f (quote ("default" "proc_macro_span" "rust" "display-diagnostics"))) (k 0)) (d (n "proc-macro2") (r "^1.0.17") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "spin_on") (r "^0.1") (d #t) (k 0)))) (h "1dw5q3hrhc294dkh82358fwr8fv960n2szqp741pvbnfbkrn08m8") (f (quote (("default")))) (r "1.70")))

(define-public crate-slint-macros-1.3.2 (c (n "slint-macros") (v "1.3.2") (d (list (d (n "i-slint-compiler") (r "=1.3.2") (f (quote ("default" "proc_macro_span" "rust" "display-diagnostics"))) (k 0)) (d (n "proc-macro2") (r "^1.0.17") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "spin_on") (r "^0.1") (d #t) (k 0)))) (h "0xl96mmvpcav9vy9gcy5vmk8ad3qczqplc8vbm2g46nm5q1qhvig") (f (quote (("default")))) (r "1.70")))

(define-public crate-slint-macros-1.4.0 (c (n "slint-macros") (v "1.4.0") (d (list (d (n "i-slint-compiler") (r "=1.4.0") (f (quote ("default" "proc_macro_span" "rust" "display-diagnostics"))) (k 0)) (d (n "proc-macro2") (r "^1.0.17") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "spin_on") (r "^0.1") (d #t) (k 0)))) (h "0jm5gl25xzjljlz4b2cf6b83hrjjkbmjv5ivr4fhnsv8xqjccqxd") (f (quote (("default")))) (r "1.70")))

(define-public crate-slint-macros-1.4.1 (c (n "slint-macros") (v "1.4.1") (d (list (d (n "i-slint-compiler") (r "=1.4.1") (f (quote ("default" "proc_macro_span" "rust" "display-diagnostics"))) (k 0)) (d (n "proc-macro2") (r "^1.0.17") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "spin_on") (r "^0.1") (d #t) (k 0)))) (h "018cparhi66hlp53hrg8zrbbjwm9xya06qvkrnlpl01lrhjjljsn") (f (quote (("default")))) (r "1.70")))

(define-public crate-slint-macros-1.5.0 (c (n "slint-macros") (v "1.5.0") (d (list (d (n "i-slint-compiler") (r "=1.5.0") (f (quote ("default" "proc_macro_span" "rust" "display-diagnostics"))) (k 0)) (d (n "proc-macro2") (r "^1.0.17") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "spin_on") (r "^0.1") (d #t) (k 0)))) (h "1zabl6091nixal4hkghgxqf30wzi0npcisk74cb4z6845fhiyv3s") (f (quote (("default")))) (r "1.70")))

(define-public crate-slint-macros-1.5.1 (c (n "slint-macros") (v "1.5.1") (d (list (d (n "i-slint-compiler") (r "=1.5.1") (f (quote ("default" "proc_macro_span" "rust" "display-diagnostics"))) (k 0)) (d (n "proc-macro2") (r "^1.0.17") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "spin_on") (r "^0.1") (d #t) (k 0)))) (h "1hf46dv9maq8hm07gjc7lg574jnj5dqcgvpklk8c4x8mxijzwvah") (f (quote (("default")))) (r "1.70")))

(define-public crate-slint-macros-1.6.0 (c (n "slint-macros") (v "1.6.0") (d (list (d (n "i-slint-compiler") (r "=1.6.0") (f (quote ("default" "proc_macro_span" "rust" "display-diagnostics"))) (k 0)) (d (n "proc-macro2") (r "^1.0.17") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "spin_on") (r "^0.1") (d #t) (k 0)))) (h "0s37bd43axlhj7wl5a15l62rknfcmca3s61jhvfgbn98pp1s7mzw") (f (quote (("default")))) (r "1.73")))

