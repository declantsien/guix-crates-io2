(define-module (crates-io sl in slingshot-term) #:use-module (crates-io))

(define-public crate-slingshot-term-0.2.3 (c (n "slingshot-term") (v "0.2.3") (d (list (d (n "crossterm") (r "^0.26.1") (d #t) (k 0)) (d (n "phf") (r "^0.11") (f (quote ("macros"))) (d #t) (k 0)))) (h "1cs4scjxv4pkh0adiq1jvvnb1rl6na9dy2vyc5n60b6lg32pnhqb")))

