(define-module (crates-io sl in sling) #:use-module (crates-io))

(define-public crate-sling-0.0.0-pre-release0.0 (c (n "sling") (v "0.0.0-pre-release0.0") (h "1zg7z1m8rslyd42sk7fi5sz92alyfqw9dyrs84aq66bjlmp72bk6")))

(define-public crate-sling-0.1.0 (c (n "sling") (v "0.1.0") (d (list (d (n "arbitrary") (r "^1.2.2") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "lockfree") (r "^0.5") (d #t) (k 2)))) (h "10ccc76zscipvkhx08sgn85js233p0akzmn8g2dfmx9jw1xxln98") (f (quote (("nightly") ("default"))))))

(define-public crate-sling-0.1.1 (c (n "sling") (v "0.1.1") (d (list (d (n "arbitrary") (r "^1.2.2") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "lockfree") (r "^0.5") (d #t) (k 2)))) (h "0j92dnhgq2nvypkqybjq2qy5jpgzx40ffp4gp20g68d35jczql09") (f (quote (("nightly") ("default"))))))

(define-public crate-sling-0.1.2 (c (n "sling") (v "0.1.2") (d (list (d (n "arbitrary") (r "^1.2.2") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "lockfree") (r "^0.5") (d #t) (k 2)))) (h "04g3mr5m3qs3q7adk1wn3h8zry7xdbl4x07290arwxk3k5sj6bbs") (f (quote (("nightly") ("default"))))))

(define-public crate-sling-0.1.3 (c (n "sling") (v "0.1.3") (d (list (d (n "arbitrary") (r "^1.2.2") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "lockfree") (r "^0.5") (d #t) (k 2)))) (h "0rhjvxir9d8bx2jrpzjijnq2dkh2blcwvxby82ip1zf5vjxkfmwa") (f (quote (("nightly") ("default"))))))

(define-public crate-sling-0.2.0 (c (n "sling") (v "0.2.0") (d (list (d (n "arbitrary") (r "^1.2.2") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "crossbeam") (r "^0.8") (d #t) (k 2)) (d (n "lockfree") (r "^0.5") (d #t) (k 2)) (d (n "loom") (r "^0.5") (d #t) (k 2)) (d (n "loom") (r "^0.5") (d #t) (t "cfg(loom)") (k 0)))) (h "18g9ym8zwfzv8ljkbs130ivmxqcpizq3ccfiapp7spaf173y0831") (f (quote (("nightly") ("default"))))))

