(define-module (crates-io sl in slint-center-win) #:use-module (crates-io))

(define-public crate-slint-center-win-0.1.2 (c (n "slint-center-win") (v "0.1.2") (d (list (d (n "i-slint-backend-winit") (r "^1") (f (quote ("renderer-femtovg" "renderer-skia" "renderer-software"))) (d #t) (k 0)) (d (n "slint") (r "^1") (d #t) (k 0)) (d (n "winit") (r "^0") (d #t) (k 0)))) (h "04i115asx944smqbsv8agabzyp0anjk4z0qwcf1vfis46jvh1iwr")))

(define-public crate-slint-center-win-0.2.0 (c (n "slint-center-win") (v "0.2.0") (d (list (d (n "i-slint-backend-winit") (r "^1.5") (f (quote ("renderer-femtovg" "renderer-skia" "renderer-software"))) (d #t) (k 0)) (d (n "slint") (r "^1.5") (d #t) (k 0)))) (h "1f40nvlzmynlzr2mmq7k01xvw06wqbbfcvil8w6i7i8lccy0i6hg")))

