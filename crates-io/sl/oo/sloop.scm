(define-module (crates-io sl oo sloop) #:use-module (crates-io))

(define-public crate-sloop-1.0.0 (c (n "sloop") (v "1.0.0") (d (list (d (n "base64") (r "^0.10") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "md5") (r "^0.6") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1") (d #t) (k 0)) (d (n "roxmltree") (r "^0.7") (d #t) (k 2)) (d (n "rss") (r "^1.8") (d #t) (k 0)) (d (n "rusoto_core") (r "^0.41") (d #t) (k 0)) (d (n "rusoto_s3") (r "^0.41") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0jbh9rp4ny8dczbppgjaimbpqrllf9l9hwkaih0c8vw5za5wj7d7")))

