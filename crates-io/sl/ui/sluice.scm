(define-module (crates-io sl ui sluice) #:use-module (crates-io))

(define-public crate-sluice-0.4.0-alpha.1 (c (n "sluice") (v "0.4.0-alpha.1") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "futures-preview") (r "^0.3.0-alpha.16") (d #t) (k 0)))) (h "1rr5an0323g61p771n9mi1rgwsq59b2wf6vyl9pppasd6cpysy1p")))

(define-public crate-sluice-0.4.0-alpha.2 (c (n "sluice") (v "0.4.0-alpha.2") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "futures-preview") (r "^0.3.0-alpha.16") (d #t) (k 0)))) (h "1vhr88wfd4adz0rzarcsfcaqlikxnpyqll99jn4s6g1vlhjm9jzs")))

(define-public crate-sluice-0.4.0 (c (n "sluice") (v "0.4.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "futures-preview") (r "^0.3.0-alpha.17") (d #t) (k 0)))) (h "039imcyc5hc4n0zyxcxdgrah6w8dm9jy4v4y57mklydcm68hcnwa") (f (quote (("nightly"))))))

(define-public crate-sluice-0.4.1 (c (n "sluice") (v "0.4.1") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "futures-channel-preview") (r "^0.3.0-alpha.17") (d #t) (k 0)) (d (n "futures-core-preview") (r "^0.3.0-alpha.17") (d #t) (k 0)) (d (n "futures-io-preview") (r "^0.3.0-alpha.17") (d #t) (k 0)) (d (n "futures-preview") (r "^0.3.0-alpha.17") (d #t) (k 2)))) (h "1dsr9sdc9ifk6ia7jfszj5q73x1bwrn1qagp3152s9kwn71xfw7c") (f (quote (("nightly"))))))

(define-public crate-sluice-0.4.2 (c (n "sluice") (v "0.4.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "futures-channel-preview") (r "^0.3.0-alpha.18") (d #t) (k 0)) (d (n "futures-core-preview") (r "^0.3.0-alpha.18") (d #t) (k 0)) (d (n "futures-io-preview") (r "^0.3.0-alpha.18") (d #t) (k 0)) (d (n "futures-preview") (r "^0.3.0-alpha.18") (d #t) (k 2)))) (h "0c9mbbf9c1180yrld2fnfy0d0wbl4w1a7s3dkv0knx78ngghcz8a") (f (quote (("nightly"))))))

(define-public crate-sluice-0.5.0 (c (n "sluice") (v "0.5.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "futures-channel") (r "^0.3") (d #t) (k 0)) (d (n "futures-core") (r "^0.3") (d #t) (k 0)) (d (n "futures-io") (r "^0.3") (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (d #t) (k 0)))) (h "0hr2c1cc7j3v5p4xq5h5daxjl2j2zggmii1jijnkbrgraf7vy9bw")))

(define-public crate-sluice-0.5.1 (c (n "sluice") (v "0.5.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "futures-channel") (r "^0.3") (d #t) (k 0)) (d (n "futures-core") (r "^0.3") (d #t) (k 0)) (d (n "futures-io") (r "^0.3") (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (k 0)))) (h "0q1356gxwcaqyjvb7zjxy3a2hy7385hw5aqa5mkh4szq28dsaa34")))

(define-public crate-sluice-0.5.2 (c (n "sluice") (v "0.5.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "futures-channel") (r "^0.3") (d #t) (k 0)) (d (n "futures-core") (r "^0.3") (d #t) (k 0)) (d (n "futures-io") (r "^0.3") (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (f (quote ("io"))) (k 0)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.9") (d #t) (k 2)))) (h "1cc93x2kf0fl8icgk2dg665wr2l4g840yx64n9fs24vgniy3plgy")))

(define-public crate-sluice-0.5.3 (c (n "sluice") (v "0.5.3") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "futures-channel") (r "^0.3") (d #t) (k 0)) (d (n "futures-core") (r "^0.3") (d #t) (k 0)) (d (n "futures-io") (r "^0.3") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.9") (d #t) (k 2)))) (h "1w9brs9dqyvr2w7cs5nxkp2wggw2xh76bc4qq0p4yxwfvhgfs94f")))

(define-public crate-sluice-0.5.4 (c (n "sluice") (v "0.5.4") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "futures-channel") (r "^0.3") (d #t) (k 0)) (d (n "futures-core") (r "^0.3") (d #t) (k 0)) (d (n "futures-io") (r "^0.3") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1.0") (d #t) (k 2)))) (h "1x9838pw0x1la1kd67c31ihmaahchh8wcp3plrs38bpzc0x3784g")))

(define-public crate-sluice-0.5.5 (c (n "sluice") (v "0.5.5") (d (list (d (n "async-channel") (r "^1") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "futures-core") (r "^0.3") (d #t) (k 0)) (d (n "futures-io") (r "^0.3") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1.0") (d #t) (k 2)))) (h "1d9ywr5039ibgaby8sc72f8fs5lpp8j5y6p3npya4jplxz000x3d")))

