(define-module (crates-io sl tu sltunnel) #:use-module (crates-io))

(define-public crate-sltunnel-0.1.0 (c (n "sltunnel") (v "0.1.0") (d (list (d (n "tokio") (r "^0.2") (f (quote ("io-util" "macros" "tcp" "rt-core"))) (d #t) (k 0)) (d (n "tokio-rustls") (r "^0.14") (d #t) (k 0)))) (h "0d8k6zp0qgsxgr1lqkvwdra965nb82wgx90y2qrqjcbw6fxji8yf") (y #t)))

