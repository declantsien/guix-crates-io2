(define-module (crates-io sl h- slh-dsa-rs) #:use-module (crates-io))

(define-public crate-slh-dsa-rs-0.1.0 (c (n "slh-dsa-rs") (v "0.1.0") (h "168sdzzbch2x6lhj1flzqbnzzf0z9kxn636q56df58cmk4hwab7b") (y #t) (r "1.73")))

(define-public crate-slh-dsa-rs-0.1.1 (c (n "slh-dsa-rs") (v "0.1.1") (h "06vy6sfbh2k2zf1l7lkvp3dbs2375yz349wfckmr8hc663jknpxy") (r "1.73")))

