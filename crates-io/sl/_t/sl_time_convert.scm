(define-module (crates-io sl _t sl_time_convert) #:use-module (crates-io))

(define-public crate-sl_time_convert-0.1.0 (c (n "sl_time_convert") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "time") (r "^0.1.42") (d #t) (k 0)))) (h "1ngcidwfsm0y1xwdlc81fzin443yhlh6js69wia00y5w9dkd7zhw")))

(define-public crate-sl_time_convert-0.3.2 (c (n "sl_time_convert") (v "0.3.2") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "time") (r "^0.1.42") (d #t) (k 0)))) (h "1m7dnsvjyqj4cklxwhpnwciz7x3f5smqyqgyvcsr1jwgk8rpc0lj")))

(define-public crate-sl_time_convert-0.4.0 (c (n "sl_time_convert") (v "0.4.0") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "time") (r "^0.1.42") (d #t) (k 0)))) (h "09xvgd2j6c677sfzph9y12zgiaknn7086mzsklp48s5krvx56ajq")))

