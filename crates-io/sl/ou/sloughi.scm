(define-module (crates-io sl ou sloughi) #:use-module (crates-io))

(define-public crate-sloughi-0.1.0 (c (n "sloughi") (v "0.1.0") (h "0r8mgd21fgrixfpgsyr4m4kpr95pdp2pzh0knzk4q9x0m80r4npm") (y #t) (r "1.59")))

(define-public crate-sloughi-0.2.0 (c (n "sloughi") (v "0.2.0") (h "0hshirjvmzpdak04c4sh5d8dg339dmwyfxmd00vawn4sfrmpvcyh") (r "1.59")))

(define-public crate-sloughi-0.3.0 (c (n "sloughi") (v "0.3.0") (h "1l5z6c8a2vjbxvrzywjvljx4jsskcpw6vcbprfyhvn546bjgws85")))

