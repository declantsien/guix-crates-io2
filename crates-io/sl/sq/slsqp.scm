(define-module (crates-io sl sq slsqp) #:use-module (crates-io))

(define-public crate-slsqp-0.1.0-alpha.1 (c (n "slsqp") (v "0.1.0-alpha.1") (d (list (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "17m3kix13ngwd18ijkryxxdx5rhk4pfxy45njzyjfk4xhphjn33f")))

(define-public crate-slsqp-0.1.0-alpha.2 (c (n "slsqp") (v "0.1.0-alpha.2") (d (list (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1y89ra8xpls3dy4mr338wwaqqkh823ygv70j421dcc9vwc0gn0xa")))

(define-public crate-slsqp-0.1.0-alpha.3 (c (n "slsqp") (v "0.1.0-alpha.3") (d (list (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1yw4jg8l1w7ia02crbhlnrly0s4cqw9z1nfb7gahg83064ly8mhf")))

(define-public crate-slsqp-0.1.0 (c (n "slsqp") (v "0.1.0") (d (list (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "17qjrg3lzc40j7zxz2rlylay8152klaa7ibkxk6z3v0wh8r2vgc8")))

(define-public crate-slsqp-0.1.1 (c (n "slsqp") (v "0.1.1") (d (list (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0n6ll93a4vzfz44swaw7cbkp50913rnwgsgy22ravx7qyi679l02")))

