(define-module (crates-io sl ud sludge-cicd) #:use-module (crates-io))

(define-public crate-sludge-cicd-0.1.0 (c (n "sludge-cicd") (v "0.1.0") (h "1lh41z2fdidmf21ab0sbyc21j604fiz20m1z9qd0ymh1mqvag72n")))

(define-public crate-sludge-cicd-0.1.1 (c (n "sludge-cicd") (v "0.1.1") (h "0rij0y9i1gwgppcmfwpzjdqp2i897wkxdzkw4qq7r7746yrrb9b2")))

(define-public crate-sludge-cicd-0.1.2 (c (n "sludge-cicd") (v "0.1.2") (h "10di6mgr1ahlhja8sdfcid36ps7bbzhqrav6c89ax4q376j7imc3")))

(define-public crate-sludge-cicd-0.1.3 (c (n "sludge-cicd") (v "0.1.3") (h "13zr421amq00qnkfgj9cgrvp0jkf2r5is0dx89w16jzajcbyd6nx")))

(define-public crate-sludge-cicd-0.1.4 (c (n "sludge-cicd") (v "0.1.4") (h "18h1dj6w1v8m1jjiilg5f33cqcwl0p8zhn2lciyks6nmmxwzb3dr")))

(define-public crate-sludge-cicd-0.1.5 (c (n "sludge-cicd") (v "0.1.5") (h "1va80ybajh2mpchsayz7pk9fklss2wsr5nja3ql93qshlp2hlw5q")))

(define-public crate-sludge-cicd-0.1.6 (c (n "sludge-cicd") (v "0.1.6") (h "1nh5s7bacjf0pn2w2vka1r4hlzc2zf9nyric3br2d84q9ndpkd63")))

(define-public crate-sludge-cicd-0.1.7 (c (n "sludge-cicd") (v "0.1.7") (h "18bxny7gik2zf8yrdfq3vrz5hw4pwz1m5kfd6pnpfl07vrldf9gf")))

(define-public crate-sludge-cicd-0.1.8 (c (n "sludge-cicd") (v "0.1.8") (h "1pxnh22k27fcx9zr4kra0822f8i2a2c1ckdllxkcprccvqv2m45l")))

(define-public crate-sludge-cicd-0.1.9 (c (n "sludge-cicd") (v "0.1.9") (h "0cwb0lxnmm5vk935hqcgjpw1bjv071ki635ciq7cyjnaf6h61rpn")))

(define-public crate-sludge-cicd-0.1.10 (c (n "sludge-cicd") (v "0.1.10") (h "121x0a34i1zv0wf9vjgjsi7w2cg6lr3nkqqk0yapmm76j3pnwfcm")))

(define-public crate-sludge-cicd-0.1.11 (c (n "sludge-cicd") (v "0.1.11") (h "07bf9i5kxlhnyd69ykwlf4nwq4v66ibfg8fhh5618pj5xgzmbl8c")))

(define-public crate-sludge-cicd-0.1.12 (c (n "sludge-cicd") (v "0.1.12") (h "1f56shzgnjmi2r0aq03blmn26kgg8sdcq9ay0z8pi7q80wc7p3n5")))

(define-public crate-sludge-cicd-0.1.13 (c (n "sludge-cicd") (v "0.1.13") (h "1mdkyqgxlpmdjyyn6g4alhk687r2i0a4rr0x8j3d9z93zxb69a2m")))

(define-public crate-sludge-cicd-0.1.14 (c (n "sludge-cicd") (v "0.1.14") (d (list (d (n "expect-test") (r "^1.5.0") (d #t) (k 2)))) (h "1d4fvanxvl70la5kz54xdarpadg60q75rywgq9ckx9x1yniif61s")))

