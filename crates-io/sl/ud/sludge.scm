(define-module (crates-io sl ud sludge) #:use-module (crates-io))

(define-public crate-sludge-0.0.0 (c (n "sludge") (v "0.0.0") (d (list (d (n "extreme") (r "^666") (d #t) (k 0)) (d (n "libc") (r "^0.2.66") (d #t) (k 0)) (d (n "rio") (r "^0.9") (d #t) (k 0)) (d (n "sled") (r "^0.31") (d #t) (k 0)))) (h "1ra47iq6byf1xlc5js84759p31i431nc91q3ppanx8r0gamd6sk2") (f (quote (("no_metrics"))))))

