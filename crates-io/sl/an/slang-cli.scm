(define-module (crates-io sl an slang-cli) #:use-module (crates-io))

(define-public crate-slang-cli-0.2.0 (c (n "slang-cli") (v "0.2.0") (d (list (d (n "clap-verbosity-flag") (r "^0.3.2") (d #t) (k 0)) (d (n "config") (r "^0.11.0") (f (quote ("toml"))) (d #t) (k 0)) (d (n "console") (r "^0.15.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.19") (d #t) (k 0)) (d (n "home") (r "^0.5.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.8") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.132") (f (quote ("derive"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3.25") (d #t) (k 0)) (d (n "tokio") (r "^1.15.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1sj1viiwc9hcrg99dpbpkvav25j6h4jdvwjkzm6118sxd5al2328")))

