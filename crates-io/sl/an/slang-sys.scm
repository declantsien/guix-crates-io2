(define-module (crates-io sl an slang-sys) #:use-module (crates-io))

(define-public crate-slang-sys-0.1.0 (c (n "slang-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.48") (d #t) (k 1)) (d (n "cfg-if") (r "^0.1") (d #t) (k 1)) (d (n "curl") (r "^0.4") (d #t) (k 1)) (d (n "url") (r "^1.7") (d #t) (k 1)) (d (n "zip") (r "^0.5") (d #t) (k 1)))) (h "0c09zn7rhm1iczrcq43p3my2a3ka4sr7afadm2r8g4jliikhplb5") (y #t) (l "slang")))

(define-public crate-slang-sys-0.1.1 (c (n "slang-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.48") (d #t) (k 1)) (d (n "cfg-if") (r "^0.1") (d #t) (k 1)) (d (n "curl") (r "^0.4") (d #t) (k 1)) (d (n "url") (r "^1.7") (d #t) (k 1)) (d (n "zip") (r "^0.5") (d #t) (k 1)))) (h "1bfbv97jam0wz4wqz3m8cm5whdmq0sng0lhinlw17xk5crvqf804") (y #t) (l "slang")))

(define-public crate-slang-sys-0.1.2 (c (n "slang-sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.48") (d #t) (k 1)) (d (n "cfg-if") (r "^0.1") (d #t) (k 1)) (d (n "curl") (r "^0.4") (d #t) (k 1)) (d (n "url") (r "^1.7") (d #t) (k 1)) (d (n "zip") (r "^0.5") (d #t) (k 1)))) (h "10my7bxlk3vlffha1rh35sdxdyswn113ymv3h4bl9biv4wvxankv") (l "slang")))

