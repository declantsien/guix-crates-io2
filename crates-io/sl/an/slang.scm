(define-module (crates-io sl an slang) #:use-module (crates-io))

(define-public crate-slang-0.0.0 (c (n "slang") (v "0.0.0") (d (list (d (n "slang-sys") (r "^0.1") (d #t) (k 0)))) (h "0d284y30bvfcnm4wfz5dy7ljzmiv3j625mddqxr0h6b7q29xads3")))

(define-public crate-slang-0.0.1 (c (n "slang") (v "0.0.1") (d (list (d (n "slang-sys") (r "^0.1") (d #t) (k 0)))) (h "1d782rg6ipb2pv735m4idr3bx2advwgml7lqkracvfr8l5mjdp15")))

(define-public crate-slang-0.0.2 (c (n "slang") (v "0.0.2") (d (list (d (n "slang-sys") (r "^0.1") (d #t) (k 0)))) (h "04kyf1q9a96fqvbnjm9kpjccjq5lnq7k8m29fwj2rvbhbszvxmqq")))

