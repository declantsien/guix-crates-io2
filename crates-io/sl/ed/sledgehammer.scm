(define-module (crates-io sl ed sledgehammer) #:use-module (crates-io))

(define-public crate-sledgehammer-0.0.1 (c (n "sledgehammer") (v "0.0.1") (d (list (d (n "js-sys") (r "^0.3.60") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.83") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.60") (f (quote ("Window" "Document" "Element" "HtmlElement" "HtmlHeadElement"))) (d #t) (k 0)))) (h "1kzxw3ls563q963mn8x1hcqbddbqmdcfsi57hv2s3zi5z8cvnjsg")))

(define-public crate-sledgehammer-0.1.0 (c (n "sledgehammer") (v "0.1.0") (d (list (d (n "js-sys") (r "^0.3.60") (d #t) (k 0)) (d (n "ufmt") (r "^0.2.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.83") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.60") (f (quote ("console" "Window" "Document" "Element" "HtmlElement" "HtmlHeadElement"))) (d #t) (k 0)))) (h "0dbn6wqfmyqy57cvi62j76kxkdlp4g9k4a4lq69rgp6cr5706ixq")))

(define-public crate-sledgehammer-0.2.0 (c (n "sledgehammer") (v "0.2.0") (d (list (d (n "js-sys") (r "^0.3.60") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.83") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.60") (f (quote ("console" "Window" "Document" "Element" "HtmlElement" "HtmlHeadElement"))) (d #t) (k 0)))) (h "1yy7hl3k77fj72j75fb4hnqsalp2r3nzlzg4w58gyris8vr5frbm")))

