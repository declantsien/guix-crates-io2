(define-module (crates-io sl ed sleded) #:use-module (crates-io))

(define-public crate-sleded-0.1.0 (c (n "sleded") (v "0.1.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sled") (r "^0.34.7") (d #t) (k 0)))) (h "0ynl4ihcflgwb373mvvik4a6cgw4i1i3ar22szp04njlgnn2368a")))

(define-public crate-sleded-0.1.1 (c (n "sleded") (v "0.1.1") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sled") (r "^0.34.7") (d #t) (k 0)))) (h "1hd0ww12fr1q8l6hm46cxax91q111a3mvp87xwrv0dyidfhr4z3c")))

(define-public crate-sleded-1.0.0 (c (n "sleded") (v "1.0.0") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sled") (r "^0.34") (d #t) (k 0)))) (h "19khpbghzspdhs63n5j3w40iqm07g2xlvrks89jv4g6a4xp49dir")))

