(define-module (crates-io sl ed sledash-magickwand-bindgen) #:use-module (crates-io))

(define-public crate-sledash-magickwand-bindgen-0.1.0 (c (n "sledash-magickwand-bindgen") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.53.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.17") (d #t) (k 1)))) (h "1yzzp5s19fjwqb48821jwggib87ggrlc1b7sj2s0gqq9inzb4adb")))

