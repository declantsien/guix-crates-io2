(define-module (crates-io sl ed sled-web) #:use-module (crates-io))

(define-public crate-sled-web-0.1.0 (c (n "sled-web") (v "0.1.0") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "http") (r "^0.1") (d #t) (k 0)) (d (n "hyper") (r "^0.12") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "sled-search") (r "^0.1") (d #t) (k 0)))) (h "0rizv2bh8c33z3na39lkjja2s97v4v58rpwhbvgkvym9xwi0gb93")))

(define-public crate-sled-web-0.1.1 (c (n "sled-web") (v "0.1.1") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "http") (r "^0.1") (d #t) (k 0)) (d (n "hyper") (r "^0.12") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "sled-search") (r "^0.1") (d #t) (k 0)))) (h "0zb6hil6n82gd4qnmbind7mhrlj62rknmdw3i3j9n311r9fgb6rk")))

(define-public crate-sled-web-0.2.0 (c (n "sled-web") (v "0.2.0") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "http") (r "^0.1") (d #t) (k 0)) (d (n "hyper") (r "^0.12") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "sled-search") (r "^0.2") (d #t) (k 0)))) (h "01v1g3qyn4bxg5ngll4139ywvhqzks71y7x4lcclp7jysg03jrm0")))

(define-public crate-sled-web-0.2.1 (c (n "sled-web") (v "0.2.1") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "http") (r "^0.1") (d #t) (k 0)) (d (n "hyper") (r "^0.12") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "sled-search") (r "^0.2") (d #t) (k 0)))) (h "0c3k27bhk3h8w7m2vkfywcanc5ppjnd6n4wp8l0bm6xsrbs4bjkp")))

(define-public crate-sled-web-0.3.0 (c (n "sled-web") (v "0.3.0") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "http") (r "^0.1") (d #t) (k 0)) (d (n "hyper") (r "^0.12") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "sled-search") (r "^0.2") (d #t) (k 0)))) (h "0shipby91985p9hjj7m9gvg6q09n6250j28948ndfl0114564iks")))

