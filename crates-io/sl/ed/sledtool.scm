(define-module (crates-io sl ed sledtool) #:use-module (crates-io))

(define-public crate-sledtool-0.1.0 (c (n "sledtool") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.37") (d #t) (k 0)) (d (n "argh") (r "^0.1.4") (d #t) (k 0)) (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "sled") (r "^0.34.6") (d #t) (k 0)))) (h "1l145xhbrm4npvn8qf8zfkhwlz0nffkd3jwzp2krl9iqmri46aa8")))

(define-public crate-sledtool-0.1.1 (c (n "sledtool") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.37") (d #t) (k 0)) (d (n "argh") (r "^0.1.4") (d #t) (k 0)) (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "sled") (r "^0.34.6") (d #t) (k 0)))) (h "1j491bfj8h36gr4xx2glq3gs0p1jw03zjjhd8zxb06mmyffgcy7s") (f (quote (("default" "compression") ("compression" "sled/compression"))))))

(define-public crate-sledtool-0.1.2 (c (n "sledtool") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.37") (d #t) (k 0)) (d (n "argh") (r "^0.1.4") (d #t) (k 0)) (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "sled") (r "^0.34.6") (d #t) (k 0)))) (h "18d0pwcmad7z5cmzch1bc20f86l0birk472j417sjnzwx9ylifr1") (f (quote (("default" "compression") ("compression" "sled/compression"))))))

