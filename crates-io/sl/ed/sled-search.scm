(define-module (crates-io sl ed sled-search) #:use-module (crates-io))

(define-public crate-sled-search-0.1.0 (c (n "sled-search") (v "0.1.0") (d (list (d (n "sled") (r "^0.15") (d #t) (k 0)))) (h "13vr9d0jw5b4bckzcmb3bppvzh9zmxk1bg6jw1gvhdkc63jwbkdh")))

(define-public crate-sled-search-0.2.0 (c (n "sled-search") (v "0.2.0") (d (list (d (n "sled") (r "^0.15") (d #t) (k 0)))) (h "0061r633qlhzxh4cx78xvagvvq1fh26q4hvsp00h2h60g431fh0i")))

