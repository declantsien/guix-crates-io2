(define-module (crates-io sl ed sledash) #:use-module (crates-io))

(define-public crate-sledash-0.1.0 (c (n "sledash") (v "0.1.0") (d (list (d (n "indicatif") (r "^0.14") (d #t) (k 2)) (d (n "regex") (r "^1") (d #t) (k 2)) (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "sledash-magickwand") (r "^0.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 2)))) (h "1rbf8ic597v7yj6dqd2av8dll6mm0zh1xp5k002jcr7i7g0wf6cy")))

