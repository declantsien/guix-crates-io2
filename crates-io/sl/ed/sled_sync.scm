(define-module (crates-io sl ed sled_sync) #:use-module (crates-io))

(define-public crate-sled_sync-0.1.2 (c (n "sled_sync") (v "0.1.2") (d (list (d (n "crossbeam-epoch") (r "^0.6") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)))) (h "0fa4jvfci86b9bvla6wdhbyjgh94a2m2jkhjrlv7clqqws697b6q") (f (quote (("no_inline") ("nightly") ("lock_free_delays"))))))

(define-public crate-sled_sync-0.1.3 (c (n "sled_sync") (v "0.1.3") (d (list (d (n "crossbeam-epoch") (r "^0.6") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)))) (h "0fl7k67jzqr9scqg2wwsrcskym9ddb9m3fi9hyyhva70hqp4dw8c") (f (quote (("no_inline") ("nightly") ("lock_free_delays")))) (y #t)))

(define-public crate-sled_sync-0.1.4 (c (n "sled_sync") (v "0.1.4") (d (list (d (n "crossbeam-epoch") (r "^0.6") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "rand_hc") (r "^0.1") (d #t) (k 0)))) (h "19h0sgmfvpz57j82lj7808im9zvczncf1lawc6d8mhgh3ay6vd6c") (f (quote (("no_inline") ("nightly") ("lock_free_delays"))))))

(define-public crate-sled_sync-0.1.5 (c (n "sled_sync") (v "0.1.5") (d (list (d (n "crossbeam-epoch") (r "^0.6") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "rand_hc") (r "^0.1") (d #t) (k 0)))) (h "09asxfqnakhg8z15m12zq27mdylaz152sygp6kbf1dm0w8nwbynk") (f (quote (("no_inline") ("nightly") ("lock_free_delays"))))))

(define-public crate-sled_sync-0.2.0 (c (n "sled_sync") (v "0.2.0") (d (list (d (n "crossbeam-epoch") (r "^0.6") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "rand_hc") (r "^0.1") (d #t) (k 0)))) (h "1zalavhl8pch8p1gv1al6bfiimjb7pdlm2kdywzjvjmfg85qsj0y") (f (quote (("no_inline") ("nightly") ("lock_free_delays"))))))

(define-public crate-sled_sync-0.2.2 (c (n "sled_sync") (v "0.2.2") (d (list (d (n "crossbeam-epoch") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "rand_hc") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0yl5rigdmcciarmw1aziry4f6dpcrr6s2f2fzjl5kh4mqp77m2fi") (f (quote (("no_inline") ("nightly") ("lock_free_delays" "log" "rand" "rand_hc"))))))

(define-public crate-sled_sync-0.3.0 (c (n "sled_sync") (v "0.3.0") (d (list (d (n "crossbeam-epoch") (r "^0.7") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.6.5") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "rand_hc") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1fy9injnq0a7s4z0wp5gia9qibmy7qvs15p95pmh4hilcfp9niai") (f (quote (("no_inline") ("nightly") ("lock_free_delays" "log" "rand" "rand_hc"))))))

