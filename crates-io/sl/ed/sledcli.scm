(define-module (crates-io sl ed sledcli) #:use-module (crates-io))

(define-public crate-sledcli-0.0.1 (c (n "sledcli") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "hexdump") (r "^0.1") (d #t) (k 0)) (d (n "rustyline") (r "^9.0") (d #t) (k 0)) (d (n "sled") (r "^0.34.7") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (k 0)) (d (n "utf-8") (r "^0.7") (d #t) (k 0)))) (h "1s071phsrl0197gy3qlf313v90scnc9frginqq9i43zmfvdkr666")))

(define-public crate-sledcli-0.0.2 (c (n "sledcli") (v "0.0.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "hexdump") (r "^0.1") (d #t) (k 0)) (d (n "humansize") (r "^1.1") (d #t) (k 0)) (d (n "rustyline") (r "^9.0") (d #t) (k 0)) (d (n "sled") (r "^0.34.7") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (k 0)) (d (n "utf-8") (r "^0.7") (d #t) (k 0)))) (h "1vkf46s4ccyg9lkv86v369170hi3975yfq7q5swfdkla7518yvbg")))

