(define-module (crates-io sl ed sled-native) #:use-module (crates-io))

(define-public crate-sled-native-0.31.0 (c (n "sled-native") (v "0.31.0") (d (list (d (n "libc") (r "^0.2.62") (d #t) (k 0)) (d (n "sled") (r "^0.31.0") (d #t) (k 0)))) (h "1aygd9jlq7ysjswmcm12l8xs4yvrcs8mb2xdya0jinlwhqhcal7f")))

(define-public crate-sled-native-0.34.6 (c (n "sled-native") (v "0.34.6") (d (list (d (n "libc") (r "^0.2.62") (d #t) (k 0)) (d (n "sled") (r "^0.34.6") (d #t) (k 0)))) (h "184a061a5p401ln9p9h9k9hj8pdwvm83905mw7vcz21fkmc021js")))

