(define-module (crates-io sl ed sledis) #:use-module (crates-io))

(define-public crate-sledis-0.0.1 (c (n "sledis") (v "0.0.1") (d (list (d (n "criterion") (r "^0.3.2") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9.2") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.9.1") (d #t) (k 2)) (d (n "sled") (r "^0.31.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.15") (d #t) (k 0)))) (h "1jfq3jrz8x7c09f0vwg0pkbgfib860lzf1wdwqzv5zziqcyv7r6y")))

(define-public crate-sledis-0.0.2 (c (n "sledis") (v "0.0.2") (d (list (d (n "criterion") (r "^0.3.2") (d #t) (k 2)) (d (n "dashmap") (r "^3.11.4") (d #t) (k 0)) (d (n "parking_lot") (r "^0.10.2") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9.2") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.9.1") (d #t) (k 2)) (d (n "sled") (r "^0.31.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.15") (d #t) (k 0)))) (h "17vjv7iw8wi36md626xxmc6p3129wd1x2bmaz0ajnld79ixmymq0")))

