(define-module (crates-io sl ed sled-overlay) #:use-module (crates-io))

(define-public crate-sled-overlay-0.0.1 (c (n "sled-overlay") (v "0.0.1") (d (list (d (n "sled") (r "^0.34.7") (d #t) (k 0)))) (h "1cvjprbf7h03i3xgi9whbiqbg78jql0wlayxkpw7f3mybhmq0qgg")))

(define-public crate-sled-overlay-0.0.2 (c (n "sled-overlay") (v "0.0.2") (d (list (d (n "sled") (r "^0.34.7") (d #t) (k 0)))) (h "0saa56v1nhqfspfry3l3f6pz2c297vxxpkdrg2g5w4wmpcgpk5bh")))

(define-public crate-sled-overlay-0.0.3 (c (n "sled-overlay") (v "0.0.3") (d (list (d (n "sled") (r "^0.34.7") (d #t) (k 0)))) (h "0z7aa96pfw1s2q2qx3c3ig6wsrfvh932y5kdp1ysw3llqlfhsban")))

(define-public crate-sled-overlay-0.0.4 (c (n "sled-overlay") (v "0.0.4") (d (list (d (n "sled") (r "^0.34.7") (d #t) (k 0)))) (h "09pns8kq10kkikw71r9saq2dz7jjzaqpn7lmzhw141s4szd3875r")))

(define-public crate-sled-overlay-0.0.5 (c (n "sled-overlay") (v "0.0.5") (d (list (d (n "sled") (r "^0.34.7") (d #t) (k 0)))) (h "0gr7j07aigilg7cjinadcqbg96chrxl40r94cwra0fpqsay02bmf")))

(define-public crate-sled-overlay-0.0.6 (c (n "sled-overlay") (v "0.0.6") (d (list (d (n "sled") (r "^0.34.7") (d #t) (k 0)))) (h "0nb0bi9w51m5wxws8lic42ya98jnjfx3cqw12p17rsxs41cha0zv")))

(define-public crate-sled-overlay-0.0.7 (c (n "sled-overlay") (v "0.0.7") (d (list (d (n "sled") (r "^0.34.7") (d #t) (k 0)))) (h "1yg22a4dnv5l5jl016flsxqdkjcnv2cwij855wkkvgvkfpp9jrpr")))

(define-public crate-sled-overlay-0.0.8 (c (n "sled-overlay") (v "0.0.8") (d (list (d (n "sled") (r "^0.34.7") (d #t) (k 0)))) (h "019d6m2yps3a7b6x6ymvsm2p906r9js5a2d295d75na0i6h4mr48")))

(define-public crate-sled-overlay-0.0.9 (c (n "sled-overlay") (v "0.0.9") (d (list (d (n "sled") (r "^0.34.7") (d #t) (k 0)))) (h "0zh01w3xq9x1sii2miscdqpvkz8iqkfribqp11hy0rcqxxv8vf5d")))

(define-public crate-sled-overlay-0.1.0 (c (n "sled-overlay") (v "0.1.0") (d (list (d (n "sled") (r "^0.34.7") (d #t) (k 0)))) (h "19mwfjspdmianqvp1ldcgmi5hm8lpb3p84mswaa23v9baplkzcgr")))

(define-public crate-sled-overlay-0.1.1 (c (n "sled-overlay") (v "0.1.1") (d (list (d (n "sled") (r "^0.34.7") (d #t) (k 0)))) (h "1izl7jygxcxxa9yjbvpk9mchwkpwn99arbx1wkqpkcwz2wvd7n5d")))

(define-public crate-sled-overlay-0.1.2 (c (n "sled-overlay") (v "0.1.2") (d (list (d (n "sled") (r "^0.34.7") (d #t) (k 0)))) (h "1xsh791nldimjhmnd9a1nm8l04y3gcxzbkf8k59n8kadyvsm3chw")))

