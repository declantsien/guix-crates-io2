(define-module (crates-io sl ed sled-typed) #:use-module (crates-io))

(define-public crate-sled-typed-0.0.0 (c (n "sled-typed") (v "0.0.0") (d (list (d (n "bincode") (r "^2.0.0-rc.2") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sled") (r "^0.34.7") (f (quote ("compression"))) (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("full"))) (d #t) (k 2)))) (h "0mmans7dlc7c9c5z78azb8hk8b32wl1080mkckh5l35ksv8kgsiz") (f (quote (("default"))))))

