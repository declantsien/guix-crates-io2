(define-module (crates-io sl ed sledgehammer_bindgen_macro) #:use-module (crates-io))

(define-public crate-sledgehammer_bindgen_macro-0.4.0 (c (n "sledgehammer_bindgen_macro") (v "0.4.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("Node" "console"))) (d #t) (k 2)))) (h "0133aayf2363qq20ip1gwy84v7348incpbbw8l42zip57pnrsm84") (f (quote (("web") ("default"))))))

(define-public crate-sledgehammer_bindgen_macro-0.5.0 (c (n "sledgehammer_bindgen_macro") (v "0.5.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("Node" "console"))) (d #t) (k 2)))) (h "0fc1kr6h5lgs0kn4asvwlsx04792w72d1yfs9rlxrlwvag643ndx") (f (quote (("web") ("default"))))))

