(define-module (crates-io sl -r sl-rs) #:use-module (crates-io))

(define-public crate-sl-rs-0.1.0 (c (n "sl-rs") (v "0.1.0") (d (list (d (n "getopts") (r "^0") (d #t) (k 0)) (d (n "ncurses") (r "^5.85") (d #t) (k 0)) (d (n "nix") (r "^0") (d #t) (k 0)))) (h "0wqp8zaqzpw8c2hjahwzzmmq0cc9g5g7n6bmqznh4nxxy3ywq3k8")))

(define-public crate-sl-rs-1.0.0 (c (n "sl-rs") (v "1.0.0") (d (list (d (n "getopts") (r "^0") (d #t) (k 0)) (d (n "ncurses") (r "^5.85") (d #t) (k 0)) (d (n "nix") (r "^0") (d #t) (k 0)))) (h "1lz71qkw1534p0zff49vzp2yvb1wm125axl1d75v1v2wsigd1zss")))

(define-public crate-sl-rs-1.0.1 (c (n "sl-rs") (v "1.0.1") (d (list (d (n "getopts") (r "^0") (d #t) (k 0)) (d (n "ncurses") (r "^5.91") (d #t) (k 0)) (d (n "nix") (r "^0") (d #t) (k 0)))) (h "0fx2iprbxpgm8w0xg6nnrw859l0lrg2i1nj7bf93906l082kz13p")))

(define-public crate-sl-rs-2.0.0 (c (n "sl-rs") (v "2.0.0") (d (list (d (n "crossterm") (r "^0.9.6") (f (quote ("terminal" "cursor" "input" "screen"))) (k 0)) (d (n "getopts") (r "^0") (d #t) (k 0)))) (h "0kkm3xba1ghlpd2nx8yjdmqnfn1kyg8f82fzxznmx3hc1b7i90k9")))

