(define-module (crates-io sl ab slabigator) #:use-module (crates-io))

(define-public crate-slabigator-0.1.0 (c (n "slabigator") (v "0.1.0") (h "0dybzvilz35ij8s41qr7s3vc0pfhvm44kf2vfzxw5fc897sylxa1")))

(define-public crate-slabigator-0.1.1 (c (n "slabigator") (v "0.1.1") (h "0jdgqzriv8j8vzdwr77p3p4z04bz3q4idy9isqwahvq4w1m23gvk")))

(define-public crate-slabigator-0.1.3 (c (n "slabigator") (v "0.1.3") (h "1rvysy7n8ph622cyab3cdk5kjblb9nf02k0c4ri6g9p1lhabmmy7")))

(define-public crate-slabigator-0.1.4 (c (n "slabigator") (v "0.1.4") (h "0pflasv59iwgf30pdab52hbnhbqzcfgs1bk1sp24b474935yzifv")))

(define-public crate-slabigator-0.1.6 (c (n "slabigator") (v "0.1.6") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1yzf4jff4n6l09dgd4zb6ivdr77wdai03l0wab52zz8l0ghzb5zh") (f (quote (("unsafe"))))))

(define-public crate-slabigator-0.2.0 (c (n "slabigator") (v "0.2.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0cf4sf4fprq95qn9dcws2sz1mfky8k6h3iklbll11lkivscv0sd4") (f (quote (("unsafe"))))))

(define-public crate-slabigator-0.1.5 (c (n "slabigator") (v "0.1.5") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0vfgyk82wlbwplxw4j8h46ning8p7sfbv001l9rkgcr4kg4r4akh") (f (quote (("unsafe"))))))

(define-public crate-slabigator-0.9.0 (c (n "slabigator") (v "0.9.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1mpdpb1sjhfvay9ygcayjam27xv46c9gzgr5h4fd8iayns6iinva") (f (quote (("unsafe") ("slot_usize") ("slot_u64") ("slot_u32"))))))

(define-public crate-slabigator-0.9.1 (c (n "slabigator") (v "0.9.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "09hn8rsh96avj1la9a2yrv0iw1qigfzblhbfpqg9czbynmli4l74") (f (quote (("slot_usize") ("slot_u64") ("slot_u32") ("releasefast"))))))

(define-public crate-slabigator-0.9.2 (c (n "slabigator") (v "0.9.2") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1060n2l2qfnrlwwxqcj27fwc91rfq8fwamjk2q75gg5qns9lj4v5") (f (quote (("slot_usize") ("slot_u64") ("slot_u32") ("releasefast"))))))

