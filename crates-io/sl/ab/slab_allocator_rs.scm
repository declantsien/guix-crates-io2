(define-module (crates-io sl ab slab_allocator_rs) #:use-module (crates-io))

(define-public crate-slab_allocator_rs-1.0.0 (c (n "slab_allocator_rs") (v "1.0.0") (d (list (d (n "buddy_system_allocator") (r "^0.8.0") (d #t) (k 0)) (d (n "spin") (r "^0.9.0") (d #t) (k 0)))) (h "1w7a812f2i6sxhk2vy055zvzlkb0cg0lndgq9w5pjy9irs690dap")))

(define-public crate-slab_allocator_rs-1.0.1 (c (n "slab_allocator_rs") (v "1.0.1") (d (list (d (n "buddy_system_allocator") (r "^0.8.0") (d #t) (k 0)) (d (n "spin") (r "^0.9.2") (d #t) (k 0)))) (h "07n2g4mkl4bwix97f0am4l3nb5nlmbicqs79iddv2r5smhhg1vkz")))

(define-public crate-slab_allocator_rs-1.0.2 (c (n "slab_allocator_rs") (v "1.0.2") (d (list (d (n "buddy_system_allocator") (r "^0.9.0") (d #t) (k 0)) (d (n "spin") (r "^0.9.4") (d #t) (k 0)))) (h "0s9nd54w75cqyi8y3xk1alxr514saddbsyqdzfbklimyigpfia1i")))

