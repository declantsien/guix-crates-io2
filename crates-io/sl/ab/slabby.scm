(define-module (crates-io sl ab slabby) #:use-module (crates-io))

(define-public crate-slabby-0.1.0 (c (n "slabby") (v "0.1.0") (h "1ijvv8x7aqhkyvwzlhvr424j2a1ws0lw98z64xaaysf3d476lpcr") (y #t) (r "1.56.1")))

(define-public crate-slabby-0.1.1 (c (n "slabby") (v "0.1.1") (h "0wsfri8h8k1j3bqi79xd6lvgyd8y191izs2c0l5ipcr31fp76633") (y #t) (r "1.56.1")))

(define-public crate-slabby-0.1.2 (c (n "slabby") (v "0.1.2") (h "1l51ja88c3fdw5yppz8kxkbbi8x5l96rszf5jwyr203afix8ryp3") (y #t) (r "1.56.1")))

(define-public crate-slabby-0.2.0 (c (n "slabby") (v "0.2.0") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "1xd23i3hjsl6hd6wyj05yq17p7jj1qk83sy4b47b03ajm3pkwif3") (y #t) (r "1.66")))

(define-public crate-slabby-0.3.0 (c (n "slabby") (v "0.3.0") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "13f9bknxrxi8d0lrvm8wnhzzz9b2d7mish1rdbfbm8r3fg5nl10s") (r "1.66")))

(define-public crate-slabby-0.3.1 (c (n "slabby") (v "0.3.1") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "19jlk1grd60s8ap85ga8ckxhapz1p286abp5fp9vn5szj48w9h3g") (r "1.66")))

