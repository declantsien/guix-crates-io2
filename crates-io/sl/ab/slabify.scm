(define-module (crates-io sl ab slabify) #:use-module (crates-io))

(define-public crate-slabify-0.0.0 (c (n "slabify") (v "0.0.0") (d (list (d (n "generational-arena") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "01z1234axrf7x6blxlimqyphsx9adx56qgmjk05x6h5n3cxhp8q0")))

