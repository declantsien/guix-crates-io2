(define-module (crates-io sl ab slablit) #:use-module (crates-io))

(define-public crate-slablit-0.2.0 (c (n "slablit") (v "0.2.0") (d (list (d (n "slab") (r "^0.4") (d #t) (k 0)))) (h "0icfs74w91rf21z32r5fwqd5dcvzb08dflppjw9c62drzrvkq41g")))

(define-public crate-slablit-0.2.1 (c (n "slablit") (v "0.2.1") (d (list (d (n "slab") (r "^0.4") (d #t) (k 0)))) (h "07ngjqgh7xxga246ijll6vd7bgqnjbl2pib3i8sm003mbwi0470c")))

(define-public crate-slablit-0.3.0 (c (n "slablit") (v "0.3.0") (d (list (d (n "slab") (r "^0.4") (d #t) (k 0)))) (h "0w803hx32jbvpwqfl7vs06qi9mxs6xhavxmjnanh3lpbx63fkr8b")))

