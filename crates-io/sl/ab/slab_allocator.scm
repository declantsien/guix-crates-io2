(define-module (crates-io sl ab slab_allocator) #:use-module (crates-io))

(define-public crate-slab_allocator-0.1.0 (c (n "slab_allocator") (v "0.1.0") (d (list (d (n "spin") (r "^0.4.4") (d #t) (k 0)))) (h "0s0sz8n5skr8iirsc62a91w5n575d83b7v4b2vcpri2kr0slz2ik")))

(define-public crate-slab_allocator-0.2.0 (c (n "slab_allocator") (v "0.2.0") (d (list (d (n "spin") (r "^0.4.6") (d #t) (k 0)))) (h "0l00cgrcv4kls0ngjfcag2hrd6qwqbw1v2jm3akkkbmmpk6g0005")))

(define-public crate-slab_allocator-0.3.0 (c (n "slab_allocator") (v "0.3.0") (d (list (d (n "linked_list_allocator") (r "^0.4.3") (d #t) (k 0)) (d (n "spin") (r "^0.4.6") (d #t) (k 0)))) (h "0l9kv4bhx99pxhsv2nf1q2nm4f8ngyp3rzcbycq09wbr8sqryazh")))

(define-public crate-slab_allocator-0.3.1 (c (n "slab_allocator") (v "0.3.1") (d (list (d (n "linked_list_allocator") (r "^0.5") (d #t) (k 0)) (d (n "spin") (r "^0.4") (d #t) (k 0)))) (h "15qrb3yxx766s5i2hhp6a7bm7pa2ayshwky2pnzq37aqk3mzi8nb")))

(define-public crate-slab_allocator-0.3.2 (c (n "slab_allocator") (v "0.3.2") (d (list (d (n "linked_list_allocator") (r "^0.6.2") (d #t) (k 0)) (d (n "spin") (r "^0.4.8") (d #t) (k 0)))) (h "11147ci4qccmkwc1gyl7lipykzbj7xfyqyx5aa8srlans1dswcx5")))

(define-public crate-slab_allocator-0.3.3 (c (n "slab_allocator") (v "0.3.3") (d (list (d (n "linked_list_allocator") (r "^0.6.3") (d #t) (k 0)) (d (n "spin") (r "^0.4.8") (d #t) (k 0)))) (h "0ka1q0wjnlbbqjkz1w12i6d5cmy63vs5pqa7bam1rv2w693r16nr")))

(define-public crate-slab_allocator-0.3.4 (c (n "slab_allocator") (v "0.3.4") (d (list (d (n "linked_list_allocator") (r "^0.6.3") (d #t) (k 0)) (d (n "spin") (r "^0.4.8") (d #t) (k 0)))) (h "0figgrqyf4j63k617yfyi9blzy6ls2rwmn8kzv59w9xsdbs75fmw") (y #t)))

(define-public crate-slab_allocator-0.3.5 (c (n "slab_allocator") (v "0.3.5") (d (list (d (n "linked_list_allocator") (r "^0.6.3") (d #t) (k 0)) (d (n "spin") (r "^0.4.9") (d #t) (k 0)))) (h "0yrmxpzw2yai4b3d2k890gv4r57a16hsipd0sybvaba995x3mcl4")))

