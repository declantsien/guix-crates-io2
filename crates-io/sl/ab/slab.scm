(define-module (crates-io sl ab slab) #:use-module (crates-io))

(define-public crate-slab-0.1.0 (c (n "slab") (v "0.1.0") (h "0g7v862p48d5xywv965mf508pg0anzz7y9s93cksbv7wnla5x52a")))

(define-public crate-slab-0.1.1 (c (n "slab") (v "0.1.1") (h "0zbj5pps6f46ac47hp813gjzvfjg0gx578cmjbni9cw3y6k0bklr")))

(define-public crate-slab-0.1.2 (c (n "slab") (v "0.1.2") (h "1zqm0g51slgrr21bn0nlljcnqh4j3k436x39wqsdnc4i8cghzinn")))

(define-public crate-slab-0.1.3 (c (n "slab") (v "0.1.3") (h "0vplakybxbpsk3k5pk1bald28j4ppa8vicvwsyzbl6qqqicgs1yq")))

(define-public crate-slab-0.2.0 (c (n "slab") (v "0.2.0") (h "1i69vq1q0v7rbs4k718di3c86y2icak0w6y4s7d2ilr8plsd7gbd")))

(define-public crate-slab-0.3.0 (c (n "slab") (v "0.3.0") (h "08xw8w61zdfn1094qkq1d554vh5wmm9bqdys8gqqxc4sv2pgrd0p")))

(define-public crate-slab-0.4.0 (c (n "slab") (v "0.4.0") (h "179fj4c5lw8wiaxczcpd59fc6npy7nkvqk3lwg3rxxfgkv6z9vzx")))

(define-public crate-slab-0.4.1 (c (n "slab") (v "0.4.1") (h "0b8hsgl5adwfcain6aqbzqlg2vwqml8nr16gqqsppxw6p7b7d5sz")))

(define-public crate-slab-0.4.2 (c (n "slab") (v "0.4.2") (h "1y59xsa27jk84sxzswjk60xcjf8b4fm5960jwpznrrcmasyva4f1")))

(define-public crate-slab-0.4.3 (c (n "slab") (v "0.4.3") (d (list (d (n "serde") (r "^1.0.95") (f (quote ("alloc"))) (o #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_test") (r "^1") (d #t) (k 2)))) (h "09v57dmy9gnfcj3c6gywp7wi09zywxf0ppj07w02hfvy38ysqwzi") (f (quote (("std") ("default" "std"))))))

(define-public crate-slab-0.4.4 (c (n "slab") (v "0.4.4") (d (list (d (n "serde") (r "^1.0.95") (f (quote ("alloc"))) (o #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_test") (r "^1") (d #t) (k 2)))) (h "1465rj4y59qjvzxik0v2m0mp71q50m9xfigxqww7yhsw3hna61y3") (f (quote (("std") ("default" "std"))))))

(define-public crate-slab-0.4.5 (c (n "slab") (v "0.4.5") (d (list (d (n "serde") (r "^1.0.95") (f (quote ("alloc"))) (o #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_test") (r "^1") (d #t) (k 2)))) (h "1ddg01hf8h4bpfm027h0snhb7jfcs1jzi497083y13q13vyr3vwx") (f (quote (("std") ("default" "std"))))))

(define-public crate-slab-0.4.6 (c (n "slab") (v "0.4.6") (d (list (d (n "serde") (r "^1.0.95") (f (quote ("alloc"))) (o #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_test") (r "^1") (d #t) (k 2)))) (h "0cmvcy9ppsh3dz8mi6jljx7bxyknvgpas4aid2ayxk1vjpz3qw7b") (f (quote (("std") ("default" "std")))) (r "1.31")))

(define-public crate-slab-0.4.7 (c (n "slab") (v "0.4.7") (d (list (d (n "autocfg") (r "^1") (d #t) (k 1)) (d (n "rustversion") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1.0.95") (f (quote ("alloc"))) (o #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_test") (r "^1") (d #t) (k 2)))) (h "1vyw3rkdfdfkzfa1mh83s237sll8v5kazfwxma60bq4b59msf526") (f (quote (("std") ("default" "std")))) (r "1.31")))

(define-public crate-slab-0.4.8 (c (n "slab") (v "0.4.8") (d (list (d (n "autocfg") (r "^1") (d #t) (k 1)) (d (n "rustversion") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1.0.95") (f (quote ("alloc"))) (o #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_test") (r "^1") (d #t) (k 2)))) (h "0bgwxig8gkqp6gz8rvrpdj6qwa10karnsxwx7wsj5ay8kcf3aa35") (f (quote (("std") ("default" "std")))) (r "1.31")))

(define-public crate-slab-0.4.9 (c (n "slab") (v "0.4.9") (d (list (d (n "autocfg") (r "^1") (d #t) (k 1)) (d (n "rustversion") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1.0.95") (f (quote ("alloc"))) (o #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_test") (r "^1") (d #t) (k 2)))) (h "0rxvsgir0qw5lkycrqgb1cxsvxzjv9bmx73bk5y42svnzfba94lg") (f (quote (("std") ("default" "std")))) (r "1.31")))

