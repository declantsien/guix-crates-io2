(define-module (crates-io sl ab slabmap) #:use-module (crates-io))

(define-public crate-slabmap-0.1.0 (c (n "slabmap") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "proptest") (r "^0.10") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "slab") (r "^0.4") (d #t) (k 2)))) (h "0yf2lhh3qgsh3mxb9174rqcbp66z0r85gswvplc9qim7rsbka5ds")))

(define-public crate-slabmap-0.1.1 (c (n "slabmap") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "proptest") (r "^0.10") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "slab") (r "^0.4") (d #t) (k 2)))) (h "1pajqx5w139nihv6l87ixd6jwqr03xlrs0hgh73hd2bywl7y69nm")))

(define-public crate-slabmap-0.2.0 (c (n "slabmap") (v "0.2.0") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "proptest") (r "^1.2.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "slab") (r "^0.4.8") (d #t) (k 2)) (d (n "test-strategy") (r "^0.3.1") (d #t) (k 2)))) (h "016ahrfvarhbwiql9lp9g5brvz9smgynngh7byzkpihj042idlnj")))

(define-public crate-slabmap-0.2.1 (c (n "slabmap") (v "0.2.1") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "derive-ex") (r "^0.1.8") (d #t) (k 0)) (d (n "proptest") (r "^1.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "slab") (r "^0.4.9") (d #t) (k 2)) (d (n "test-strategy") (r "^0.3.1") (d #t) (k 2)))) (h "1196wcr1x3a7qqsk1x2w6ar4jzl0jkhhazp0lj0m7vlwf54klnsl")))

