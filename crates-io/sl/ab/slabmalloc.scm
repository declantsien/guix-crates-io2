(define-module (crates-io sl ab slabmalloc) #:use-module (crates-io))

(define-public crate-slabmalloc-0.0.1 (c (n "slabmalloc") (v "0.0.1") (d (list (d (n "x86") (r "*") (d #t) (k 0)))) (h "0nprh0annwg22cb5frs0q94xryp6z521bqfbfmvn3bvri9shir7s")))

(define-public crate-slabmalloc-0.1.0 (c (n "slabmalloc") (v "0.1.0") (d (list (d (n "libc") (r "*") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "x86") (r "*") (d #t) (k 0)))) (h "0irscm2jqawykydxmdb3ssj1hzid73xgkkyhl10jq4pfym5d5p8v")))

(define-public crate-slabmalloc-0.1.1 (c (n "slabmalloc") (v "0.1.1") (d (list (d (n "libc") (r "*") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "x86") (r "*") (d #t) (k 0)))) (h "04fk23pma3bja6hk77394azn54dfb8dm1vzx9saclg78421w129v")))

(define-public crate-slabmalloc-0.2.0 (c (n "slabmalloc") (v "0.2.0") (d (list (d (n "libc") (r "*") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "x86") (r "*") (d #t) (k 0)))) (h "1rizsk1scl3rpvpa94qg54i1hczbznffn91mhlsjlqiv1lqyy7bv")))

(define-public crate-slabmalloc-0.2.1 (c (n "slabmalloc") (v "0.2.1") (d (list (d (n "libc") (r "*") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "06dr0j25g3khhpac2impwfql94xzlcw5nvidaykfsjr4w7n6dlsl")))

(define-public crate-slabmalloc-0.3.0 (c (n "slabmalloc") (v "0.3.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "spin") (r "^0.4.8") (d #t) (k 0)))) (h "02mg906bm6wrk2r0fysvg9b5xjkbvyj85pmfgqgrrbkx8zyb5kpv")))

(define-public crate-slabmalloc-0.3.1 (c (n "slabmalloc") (v "0.3.1") (d (list (d (n "env_logger") (r "^0.5.10") (d #t) (t "cfg(unix)") (k 2)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (t "cfg(unix)") (k 2)) (d (n "spin") (r "^0.4.8") (k 0)))) (h "1ix0xyr4cy6j4xq6ysfzi66ym7cckvz8f7ym3sk6my3sfhr8k4jh") (f (quote (("unstable" "spin/unstable") ("default" "unstable"))))))

(define-public crate-slabmalloc-0.3.2 (c (n "slabmalloc") (v "0.3.2") (d (list (d (n "env_logger") (r "^0.5.10") (d #t) (t "cfg(unix)") (k 2)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (t "cfg(unix)") (k 2)) (d (n "spin") (r "^0.4.8") (k 0)))) (h "112wn2giza888cvh3m6xb5kbsr8pfdhn05j4dx6yhpqagavpl30c") (f (quote (("unstable" "spin/unstable") ("default" "unstable"))))))

(define-public crate-slabmalloc-0.3.3 (c (n "slabmalloc") (v "0.3.3") (d (list (d (n "env_logger") (r "^0.5.10") (d #t) (t "cfg(unix)") (k 2)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (t "cfg(unix)") (k 2)) (d (n "spin") (r "^0.4.8") (k 0)))) (h "1w30pz57nqjz6s662v4apaypg5a78fxprxinvljhjqki5c5qrdbz") (f (quote (("unstable" "spin/unstable") ("default" "unstable"))))))

(define-public crate-slabmalloc-0.3.4 (c (n "slabmalloc") (v "0.3.4") (d (list (d (n "env_logger") (r "^0.5.10") (d #t) (t "cfg(unix)") (k 2)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (t "cfg(unix)") (k 2)) (d (n "spin") (r "^0.5.2") (d #t) (k 0)))) (h "0b2krsi4v0rhlx739d9s19kqvq5aj0mfjdkrxh645ps5wkvwyr85") (f (quote (("unstable") ("default" "unstable"))))))

(define-public crate-slabmalloc-0.4.0 (c (n "slabmalloc") (v "0.4.0") (d (list (d (n "env_logger") (r "^0.7") (d #t) (t "cfg(unix)") (k 2)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (t "cfg(unix)") (k 2)) (d (n "spin") (r "^0.5.2") (d #t) (k 0)))) (h "0jsmr22202qvd9vysvib3i8ds7drl59q33xan2lxligx2jghj194") (f (quote (("unstable") ("default" "unstable"))))))

(define-public crate-slabmalloc-0.5.0 (c (n "slabmalloc") (v "0.5.0") (d (list (d (n "env_logger") (r "^0.7") (d #t) (t "cfg(unix)") (k 2)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (t "cfg(unix)") (k 2)) (d (n "spin") (r "^0.5.2") (d #t) (k 0)))) (h "0g52hhvpqlmv5fwvkk00ifz93a9d9bhcjqhzh4k21y0ih49grg5i") (f (quote (("unstable") ("default" "unstable")))) (y #t)))

(define-public crate-slabmalloc-0.6.0 (c (n "slabmalloc") (v "0.6.0") (d (list (d (n "env_logger") (r "^0.7") (d #t) (t "cfg(unix)") (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (t "cfg(unix)") (k 2)) (d (n "spin") (r "^0.5.2") (d #t) (k 0)))) (h "0fvm80j0cymv94grxw2dv4g6798abn70jzigg76794hfydbq4lc4") (f (quote (("unstable") ("default" "unstable"))))))

(define-public crate-slabmalloc-0.7.0 (c (n "slabmalloc") (v "0.7.0") (d (list (d (n "env_logger") (r "^0.7") (d #t) (t "cfg(unix)") (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (t "cfg(unix)") (k 2)) (d (n "spin") (r "^0.5.2") (d #t) (t "cfg(unix)") (k 2)))) (h "05a1q82icybc7vd5c0cx1jj2pacnhsh04q0a5r18jmblz4bdq7d5") (f (quote (("unstable") ("default" "unstable"))))))

(define-public crate-slabmalloc-0.8.0 (c (n "slabmalloc") (v "0.8.0") (d (list (d (n "env_logger") (r "^0.7") (d #t) (t "cfg(unix)") (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (t "cfg(unix)") (k 2)) (d (n "spin") (r "^0.5.2") (d #t) (t "cfg(unix)") (k 2)))) (h "0969lmkyngx45mi346yhjvxnl3m1zixl247p23dl19d1rfbdikb2") (f (quote (("unstable") ("default" "unstable"))))))

(define-public crate-slabmalloc-0.9.0 (c (n "slabmalloc") (v "0.9.0") (d (list (d (n "env_logger") (r "^0.7") (d #t) (t "cfg(unix)") (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (t "cfg(unix)") (k 2)) (d (n "spin") (r "^0.5.2") (d #t) (t "cfg(unix)") (k 2)))) (h "1pcgvxvswfyxp4h5rsx9lwbyp13q1saj4wqclp1rav0d5x6gjn9a") (f (quote (("unstable") ("default" "unstable"))))))

(define-public crate-slabmalloc-0.10.0 (c (n "slabmalloc") (v "0.10.0") (d (list (d (n "env_logger") (r "^0.8") (d #t) (t "cfg(unix)") (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (t "cfg(unix)") (k 2)) (d (n "spin") (r "^0.9.0") (d #t) (t "cfg(unix)") (k 2)))) (h "1w0h3r6x28zbkv5nsf1h96xzlyv7ayfrrwb7dcs8i0h5ql7wihs7") (f (quote (("unstable") ("default" "unstable"))))))

(define-public crate-slabmalloc-0.11.0 (c (n "slabmalloc") (v "0.11.0") (d (list (d (n "env_logger") (r "^0.9") (d #t) (t "cfg(unix)") (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (t "cfg(unix)") (k 2)) (d (n "spin") (r "^0.9.0") (d #t) (t "cfg(unix)") (k 2)))) (h "1dvkj2cac2yzwnh92yjh3zi1zigb6bqjnvy9jf9a2wwq3g379cjj") (f (quote (("unstable") ("default" "unstable"))))))

