(define-module (crates-io sl ab slab_32) #:use-module (crates-io))

(define-public crate-slab_32-0.1.0 (c (n "slab_32") (v "0.1.0") (h "1y50ivky544b8h38k3aqvy43f20sm4s6y766y3wqbz80yd4q818w")))

(define-public crate-slab_32-0.1.1 (c (n "slab_32") (v "0.1.1") (h "1vdlfhagpf0dcaz7vakr71ajgvqr9h56lcwqjq7m313kjr1bbpby")))

