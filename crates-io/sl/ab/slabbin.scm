(define-module (crates-io sl ab slabbin) #:use-module (crates-io))

(define-public crate-slabbin-1.0.0 (c (n "slabbin") (v "1.0.0") (h "15kddj33f1682v7z4nm39dm6yl1mw0gbnskhjhh8wrdl78szqh0h")))

(define-public crate-slabbin-1.0.1 (c (n "slabbin") (v "1.0.1") (h "0wzj400wzrhxl4va1sh4f82grybwp0kjshw5xlklcqa4c040b0yx")))

