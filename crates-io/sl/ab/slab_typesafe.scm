(define-module (crates-io sl ab slab_typesafe) #:use-module (crates-io))

(define-public crate-slab_typesafe-0.1.0 (c (n "slab_typesafe") (v "0.1.0") (d (list (d (n "slab") (r "^0.4") (d #t) (k 0)))) (h "0dnzyw7i2llf4nwsa9z7419dbklkhv7cczpy0ca0brdywyig106f")))

(define-public crate-slab_typesafe-0.1.1 (c (n "slab_typesafe") (v "0.1.1") (d (list (d (n "slab") (r "^0.4") (d #t) (k 0)))) (h "1lidivzivjh9pskscjwmisq4crfk2pgipg9jhkv27x9d4krnxn5j")))

(define-public crate-slab_typesafe-0.1.2 (c (n "slab_typesafe") (v "0.1.2") (d (list (d (n "slab") (r "^0.4") (d #t) (k 0)))) (h "14ya96qn66ybv69p0klksfldf6yvhhn7y8009vlf412p098v0vav")))

(define-public crate-slab_typesafe-0.1.3 (c (n "slab_typesafe") (v "0.1.3") (d (list (d (n "slab") (r "^0.4") (d #t) (k 0)))) (h "1hf0l3xzf2g2i3fp4rlc1ivcp2f5klaa6iih510xmar64l3a5qan")))

