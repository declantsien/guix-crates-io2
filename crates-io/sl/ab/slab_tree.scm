(define-module (crates-io sl ab slab_tree) #:use-module (crates-io))

(define-public crate-slab_tree-0.1.0 (c (n "slab_tree") (v "0.1.0") (d (list (d (n "snowflake") (r "^1.3.0") (d #t) (k 0)))) (h "0gh66wgywq6n820cilzg24ngjdqkgyjjiw19cj7mqvn96jagczbd")))

(define-public crate-slab_tree-0.1.1 (c (n "slab_tree") (v "0.1.1") (d (list (d (n "snowflake") (r "^1.3.0") (d #t) (k 0)))) (h "03s1abv2vb9q63mvkfby7qg45k4fn4h49bzq1c6s5ap8fls5g1nq")))

(define-public crate-slab_tree-0.1.2 (c (n "slab_tree") (v "0.1.2") (d (list (d (n "snowflake") (r "^1.3.0") (d #t) (k 0)))) (h "07hwmxxgfs9a9glmxq91bizmb1jzy5mq8407l0b12hwhk49yv6vz")))

(define-public crate-slab_tree-0.1.3 (c (n "slab_tree") (v "0.1.3") (d (list (d (n "snowflake") (r "^1.3.0") (d #t) (k 0)))) (h "0pkgkjd04sk551j2lrhvf3wvz3mn3vp2b9x4xsc72gw2fs3pm2ib")))

(define-public crate-slab_tree-0.1.4 (c (n "slab_tree") (v "0.1.4") (d (list (d (n "snowflake") (r "^1.3.0") (d #t) (k 0)))) (h "0sr1kffbrfyw6b357dzsrhbm8rimpmcynl30z3380hcahl3pzq6y")))

(define-public crate-slab_tree-0.2.0 (c (n "slab_tree") (v "0.2.0") (d (list (d (n "snowflake") (r "^1.3.0") (d #t) (k 0)))) (h "1pykrdh3fhy78mvf7l07gbqiyy8rn7sqb0xwcq9zdkajjjx8ajw6")))

(define-public crate-slab_tree-0.3.0 (c (n "slab_tree") (v "0.3.0") (d (list (d (n "snowflake") (r "^1.3.0") (d #t) (k 0)))) (h "049km2f7n5h73c31dghq82k542vlkawxjcdpkkbx9ghdr2d0kxld")))

(define-public crate-slab_tree-0.3.1 (c (n "slab_tree") (v "0.3.1") (d (list (d (n "snowflake") (r "^1.3.0") (d #t) (k 0)))) (h "1zycdib53j2vks42ywvjx806m0mlls5grmj4cikh066981jhr4ra")))

(define-public crate-slab_tree-0.3.2 (c (n "slab_tree") (v "0.3.2") (d (list (d (n "snowflake") (r "^1.3.0") (d #t) (k 0)))) (h "17z389gaywssmq8y5xaz5k469qgz7jbi2jh50fvwmdvpprdb9s7r")))

