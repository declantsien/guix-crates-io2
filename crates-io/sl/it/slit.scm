(define-module (crates-io sl it slit) #:use-module (crates-io))

(define-public crate-slit-0.0.1 (c (n "slit") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 0)) (d (n "jacklog") (r "^0.0.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "15cn0569ic9cmjwdfq0x35q39vhg5lqq99jmm7j9jl4n65bkxwb7")))

(define-public crate-slit-0.0.2 (c (n "slit") (v "0.0.2") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 0)) (d (n "jacklog") (r "^0.0.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "0lhamki6v38189vxk9nqmii3j6i24fbyfxwhx5qhf0rvjwx033pr")))

