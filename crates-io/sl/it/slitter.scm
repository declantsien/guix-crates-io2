(define-module (crates-io sl it slitter) #:use-module (crates-io))

(define-public crate-slitter-0.1.0 (c (n "slitter") (v "0.1.0") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "contracts") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "contracts") (r "^0.6") (d #t) (k 2)) (d (n "disabled_contracts") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "static_assertions") (r "^1.1") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "02n21dc19m84jx9j2g6iqkcv0bs9k47cgy7xb7n5xdmr6nxkv5wa") (f (quote (("test_only_small_constants") ("default" "check_contracts_in_tests" "c_fast_path") ("check_contracts_in_tests") ("check_contracts" "contracts") ("c_fast_path"))))))

