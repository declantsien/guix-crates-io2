(define-module (crates-io sl ib slib) #:use-module (crates-io))

(define-public crate-slib-0.0.0 (c (n "slib") (v "0.0.0") (h "08a7slg6zyrk5a0ibhb671rxv8zr33aw8wg1lrkygz7f9iqxc48n")))

(define-public crate-slib-0.0.1 (c (n "slib") (v "0.0.1") (h "1p9q2wqw7pd6fj4k3lcyl740ffccb2w7w1gafy5jhl3jlj0ynn8r")))

(define-public crate-slib-0.0.2 (c (n "slib") (v "0.0.2") (h "040n4s9kihv5bmcpmqnn70hj4mcsqzl2f8zk2x1qspckvbjbm549")))

(define-public crate-slib-0.0.3 (c (n "slib") (v "0.0.3") (h "1v0hd5rh4c8bc83zddiajkbgaivvdc10qk97z516agv62h51lph2")))

