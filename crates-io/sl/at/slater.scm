(define-module (crates-io sl at slater) #:use-module (crates-io))

(define-public crate-slater-0.1.0 (c (n "slater") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0qvg2n0mai89gcvzlnvk1xjhh3xhmjq6y4b13sfi1d6qsnjb3qwd")))

(define-public crate-slater-0.2.0 (c (n "slater") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1wqxdd55v2vhsdrmdk9vd1rin25nnx9an4vhkr9zpxp6fbw52d6x")))

(define-public crate-slater-0.2.1 (c (n "slater") (v "0.2.1") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "16dxz5la3vmzp936zigizrv9550dm9i5vz89lfvgalqxc5qny8b0")))

(define-public crate-slater-0.2.2 (c (n "slater") (v "0.2.2") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1rgg8p3m1a2xvsvdy8zy587bq30nba846839nap1vg9akx39ymcm")))

(define-public crate-slater-0.2.3 (c (n "slater") (v "0.2.3") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1mn4zwb4p6yxm1v2f4kdpj8dnz5src5i2g53jd9s15nc8gf4lfv2")))

(define-public crate-slater-0.2.4 (c (n "slater") (v "0.2.4") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1wyr6whhb9vrwns5h0gcwd8m25m7fhkyzm4r8dm2sxcy98vjkvvf")))

