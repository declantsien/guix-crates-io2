(define-module (crates-io sl at slate) #:use-module (crates-io))

(define-public crate-slate-1.0.2 (c (n "slate") (v "1.0.2") (d (list (d (n "docopt") (r "^0.6") (d #t) (k 0)) (d (n "exec") (r "^0.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "regex") (r "^0.1.69") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1xc70b2mr7an5l3rfagvxpzspczja30v7sj3r4fp9y5zbpxa26zc")))

(define-public crate-slate-1.1.0 (c (n "slate") (v "1.1.0") (d (list (d (n "docopt") (r "^0.6") (d #t) (k 0)) (d (n "exec") (r "^0.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "regex") (r "^0.1.69") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0swirwydrzxl15hw2czafn3xn84kk9lqw6pqakj3938y4g2mhr9y")))

(define-public crate-slate-1.1.1 (c (n "slate") (v "1.1.1") (d (list (d (n "docopt") (r "^0.6") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "regex") (r "^0.1.69") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1p7dv7nn3b1c96xcl71200i4dhlvyhb9ygnibrwc048w251d1ayk")))

(define-public crate-slate-1.2.0 (c (n "slate") (v "1.2.0") (d (list (d (n "docopt") (r "^0.6") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "regex") (r "^0.1.69") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0ysivgmfh0l9w3y1kjl80i90q07rv2qrj0wrfghfphmkqd554dgc")))

(define-public crate-slate-1.3.0 (c (n "slate") (v "1.3.0") (d (list (d (n "docopt") (r "^0.6") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "regex") (r "^0.1.69") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1y4lffrv0kmyc4r8iklla7j650rl3zhscg5rzbwbnxk08nikxd18")))

(define-public crate-slate-1.4.0 (c (n "slate") (v "1.4.0") (d (list (d (n "docopt") (r "^0.8") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1f3f90m9ifr1madg4xdg9m72ijjjnd9k8vg6maylynzi5sq8fm9p")))

