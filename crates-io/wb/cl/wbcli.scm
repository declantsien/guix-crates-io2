(define-module (crates-io wb cl wbcli) #:use-module (crates-io))

(define-public crate-wbcli-0.1.0 (c (n "wbcli") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "confy") (r "^0.4.0") (d #t) (k 0)) (d (n "dirs") (r "^3.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "log4rs") (r "^1.0.0") (d #t) (k 0)) (d (n "read_input") (r "^0.8.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.2") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1vrpzx8y0w8srqq8p327wlgjl3nqyyf39jpbz6yz87a4xl6z9bzz")))

