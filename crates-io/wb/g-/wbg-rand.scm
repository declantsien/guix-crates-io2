(define-module (crates-io wb g- wbg-rand) #:use-module (crates-io))

(define-public crate-wbg-rand-0.4.0 (c (n "wbg-rand") (v "0.4.0") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)))) (h "0k1blnqrhc8pn6pbqgx191hlddcpmksbldawhgnnzxcin46f21am")))

(define-public crate-wbg-rand-0.4.1 (c (n "wbg-rand") (v "0.4.1") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)))) (h "1yikmkkshr0dg94hvi5g62w2wq4xwbak93l9px818p1di7f2d7ii")))

