(define-module (crates-io wb s- wbs-backup) #:use-module (crates-io))

(define-public crate-wbs-backup-0.1.0 (c (n "wbs-backup") (v "0.1.0") (d (list (d (n "rustc-serialize") (r "^0.3.14") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "01zi1rq2d8jgdih9pwxvdv1pq7vg3mxny5xlf8a57kzg7lvcv2hx")))

(define-public crate-wbs-backup-0.1.1 (c (n "wbs-backup") (v "0.1.1") (d (list (d (n "rustc-serialize") (r "^0.3.14") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1srf1lblxa4kpyaahbn9ga76ghryjlxqiir0xc9lfs75z7zlnbni")))

(define-public crate-wbs-backup-0.1.2 (c (n "wbs-backup") (v "0.1.2") (d (list (d (n "rustc-serialize") (r "^0.3.14") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "11zndwxkvnh5ripvbqjk9z9j57xsfxsxa6dy2s6hxzmba6mk2ibc")))

(define-public crate-wbs-backup-0.1.4 (c (n "wbs-backup") (v "0.1.4") (d (list (d (n "rustc-serialize") (r "^0.3.14") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1vr6708gbh2yhmfs790iins65rcdllzsvh2c5m9yhd3671v40y3k")))

(define-public crate-wbs-backup-0.1.6 (c (n "wbs-backup") (v "0.1.6") (d (list (d (n "rustc-serialize") (r "^0.3.14") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1hw9p2k2idp4czghgxwd672x9axiy3nbv5lrcyp7kz380rfgr9iy")))

(define-public crate-wbs-backup-0.1.8 (c (n "wbs-backup") (v "0.1.8") (d (list (d (n "rustc-serialize") (r "^0.3.14") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "121qn8fg2pd331kdvdhrvivyvnq1cdc8pdllpacng4q86nbgb0cv")))

