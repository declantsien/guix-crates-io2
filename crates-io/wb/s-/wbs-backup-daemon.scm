(define-module (crates-io wb s- wbs-backup-daemon) #:use-module (crates-io))

(define-public crate-wbs-backup-daemon-0.1.10 (c (n "wbs-backup-daemon") (v "0.1.10") (d (list (d (n "rustc-serialize") (r "^0.3.14") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "17kxy8a91wgmscd4ikmr8cl8h4lay5156x65gjmww9mqq2gdrlm7")))

(define-public crate-wbs-backup-daemon-0.1.11 (c (n "wbs-backup-daemon") (v "0.1.11") (d (list (d (n "rustc-serialize") (r "^0.3.14") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0b10i069qhb2zqz51qvhiqqyyxl3wih6mndlbb9q84n1ifbqgv4g")))

(define-public crate-wbs-backup-daemon-0.1.12 (c (n "wbs-backup-daemon") (v "0.1.12") (d (list (d (n "rustc-serialize") (r "^0.3.14") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0gnxb31xfazmwaqpg4sj5sxmjf564pksg5iqhdgmmzsygl7qlr4k")))

(define-public crate-wbs-backup-daemon-0.1.13 (c (n "wbs-backup-daemon") (v "0.1.13") (d (list (d (n "rustc-serialize") (r "^0.3.14") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0daa0agif7nmlcddhhv0rp86aqsjhb457gbkczsj7dr5f0mxlbpm")))

(define-public crate-wbs-backup-daemon-0.1.14 (c (n "wbs-backup-daemon") (v "0.1.14") (d (list (d (n "rustc-serialize") (r "^0.3.14") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "05f9ay3s3f7xgrv08qdx0szjdy59a5s6rjpw1hcs505prjdim8kc")))

(define-public crate-wbs-backup-daemon-1.1.15 (c (n "wbs-backup-daemon") (v "1.1.15") (d (list (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1ijjm6qynxxxyy4zbb1fbd3zbj9hvk34rrj04d72jxyngsqc5y40")))

