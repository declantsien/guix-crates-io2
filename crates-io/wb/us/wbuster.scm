(define-module (crates-io wb us wbuster) #:use-module (crates-io))

(define-public crate-wbuster-0.1.0 (c (n "wbuster") (v "0.1.0") (d (list (d (n "clap") (r "^4") (d #t) (k 0)) (d (n "colored") (r "^1.9.1") (d #t) (k 0)) (d (n "rayon") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1i6zdq3lkqw1bkiyr5rgv0qh9xmdvsax2zqs5660pi2f13x2pjcm") (y #t)))

(define-public crate-wbuster-0.1.1 (c (n "wbuster") (v "0.1.1") (d (list (d (n "clap") (r "^4") (d #t) (k 0)) (d (n "colored") (r "^1.9.1") (d #t) (k 0)) (d (n "rayon") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0ihylqfhkqj9pk0hazs0h4vm8rvzx9a7pm5z66biy0mmgdz241ni")))

(define-public crate-wbuster-0.1.2 (c (n "wbuster") (v "0.1.2") (d (list (d (n "clap") (r "^4") (d #t) (k 0)) (d (n "colored") (r "^1.9.1") (d #t) (k 0)) (d (n "rayon") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0awa3z9q6sdc9vpc56wvydmsiy7mx9p8259qb70msf3qhrxwqfx4")))

(define-public crate-wbuster-0.1.3 (c (n "wbuster") (v "0.1.3") (d (list (d (n "clap") (r "^4") (d #t) (k 0)) (d (n "colored") (r "^1.9.1") (d #t) (k 0)) (d (n "rayon") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1n02zn2qygxypjd4mjsy6b4aihq794ai9vcyh1whdyskw22sp9zb")))

(define-public crate-wbuster-0.1.4 (c (n "wbuster") (v "0.1.4") (d (list (d (n "clap") (r "^4") (d #t) (k 0)) (d (n "colored") (r "^1.9.1") (d #t) (k 0)) (d (n "rayon") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1mlf6l43v7000ajccs1nb65lz1d61bysnzy9y7g8rylfjn7mcjgy")))

