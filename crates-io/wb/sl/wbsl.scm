(define-module (crates-io wb sl wbsl) #:use-module (crates-io))

(define-public crate-wbsl-0.0.0 (c (n "wbsl") (v "0.0.0") (d (list (d (n "aul") (r "^1.3.1") (d #t) (k 0)) (d (n "whdp") (r "^1.1.8") (d #t) (k 0)) (d (n "wjp") (r "^1.0.1") (d #t) (k 0)))) (h "15bhwax58d3c97y804bp6z7dr1spm84rjixk52knqrw8i7zc0mfg") (f (quote (("no-color" "aul/no-color"))))))

(define-public crate-wbsl-0.0.1 (c (n "wbsl") (v "0.0.1") (d (list (d (n "aul") (r "^1.3.1") (d #t) (k 0)) (d (n "whdp") (r "^1.1.9") (d #t) (k 0)) (d (n "wjp") (r "^1.0.2") (d #t) (k 0)))) (h "04i8mvz753ml6x7clbximlccbsg53a18617ill97zbs23wajx88h") (f (quote (("no-color" "aul/no-color"))))))

(define-public crate-wbsl-0.0.2 (c (n "wbsl") (v "0.0.2") (d (list (d (n "aul") (r "^1.3.1") (d #t) (k 0)) (d (n "whdp") (r "^1.1.10") (d #t) (k 0)) (d (n "wjp") (r "^1.0.2") (d #t) (k 0)))) (h "07mbfshdvih3bcbymcavxr9fmhfap2d61pcyn8ibgv27lsb1pz2k") (f (quote (("no-color" "aul/no-color"))))))

(define-public crate-wbsl-0.0.3 (c (n "wbsl") (v "0.0.3") (d (list (d (n "aul") (r "^1.3.1") (d #t) (k 0)) (d (n "wbdl") (r "^1.0.3") (d #t) (k 0)) (d (n "whdp") (r "^1.1.10") (d #t) (k 0)) (d (n "wjp") (r "^1.0.2") (d #t) (k 0)))) (h "0kpnyp965b0pylj4a7qbgn96lsz22niw3xm2z98c5xspqvbhaww4") (f (quote (("no-color" "aul/no-color"))))))

(define-public crate-wbsl-0.0.4 (c (n "wbsl") (v "0.0.4") (d (list (d (n "aul") (r "^1.3.1") (d #t) (k 0)) (d (n "wbdl") (r "^1.1.0") (d #t) (k 0)) (d (n "whdp") (r "^1.1.11") (d #t) (k 0)) (d (n "wjp") (r "^1.1.1") (d #t) (k 0)))) (h "17l4ap8r4ijw7vqi4b44r154bapiz6zryjz4dm7knfy719ylb7h0") (f (quote (("no-color" "aul/no-color"))))))

(define-public crate-wbsl-0.0.5 (c (n "wbsl") (v "0.0.5") (d (list (d (n "aul") (r "^1.3.2") (d #t) (k 0)) (d (n "wbdl") (r "^1.2.0") (d #t) (k 0)) (d (n "whdp") (r "^1.2.0") (d #t) (k 0)) (d (n "wjp") (r "^1.1.3") (d #t) (k 0)))) (h "04g12vgck1x1ihfrpkfhz7szaz8ppb1n4676mylsl5dasikg7zfv") (f (quote (("no-color" "aul/no-color"))))))

(define-public crate-wbsl-0.1.0 (c (n "wbsl") (v "0.1.0") (d (list (d (n "aul") (r "^1.3.2") (d #t) (k 0)) (d (n "wbdl") (r "^1.2.0") (d #t) (k 0)) (d (n "whdp") (r "^1.2.0") (d #t) (k 0)) (d (n "wjp") (r "^1.1.3") (d #t) (k 0)))) (h "067n59lsgfj0y1196nnn14grj7w8ihg4cah432b4djkm6msbqcs2") (f (quote (("no-color" "aul/no-color"))))))

(define-public crate-wbsl-0.1.1 (c (n "wbsl") (v "0.1.1") (d (list (d (n "aul") (r "^1.4.1") (d #t) (k 0)) (d (n "wbdl") (r "^1.2.0") (d #t) (k 0)) (d (n "whdp") (r "^1.2.0") (d #t) (k 0)) (d (n "wjp") (r "^1.1.3") (d #t) (k 0)))) (h "12wkz2a99q4v4w2hvrwd8yzb5rsfv5mx6k9ml569cgz8ynbx17h2") (f (quote (("color" "aul/color"))))))

