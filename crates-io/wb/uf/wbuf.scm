(define-module (crates-io wb uf wbuf) #:use-module (crates-io))

(define-public crate-wbuf-0.1.0 (c (n "wbuf") (v "0.1.0") (h "06szd4268cckgmsxi7v09j06scn1b04dmi0dsyad6cdvrc0pn536")))

(define-public crate-wbuf-0.1.1 (c (n "wbuf") (v "0.1.1") (h "1r1njmyshkdz9f7sd1w83mjb0i2wrsk13sdv86fdkj9vkyglcfwn")))

