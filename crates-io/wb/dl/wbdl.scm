(define-module (crates-io wb dl wbdl) #:use-module (crates-io))

(define-public crate-wbdl-0.0.0 (c (n "wbdl") (v "0.0.0") (h "026v5mj8lp8l2iad3lpapxwdciinz7clpw8kgj01nsasyb6rz4fr")))

(define-public crate-wbdl-1.0.0 (c (n "wbdl") (v "1.0.0") (h "1jxms9vq3v0av8immyyw7arfwdg28cl4vz9zycxf1d9higdnakg8")))

(define-public crate-wbdl-1.0.1 (c (n "wbdl") (v "1.0.1") (h "0jfiwizszg02xisdws6aras4f4s4brxjp0mc3ggxs8xgyis46shi")))

(define-public crate-wbdl-1.0.2 (c (n "wbdl") (v "1.0.2") (h "0fgy3jj4n8k56fi8a6lln926m6bc2mm540v8h5n64wbr7i3m19jy")))

(define-public crate-wbdl-1.0.3 (c (n "wbdl") (v "1.0.3") (h "0j3nqd3c7ksdig1vsg3njd3miyvdsqj30zs75wybn7wf7bziai2y")))

(define-public crate-wbdl-1.0.4 (c (n "wbdl") (v "1.0.4") (h "081g76s19w7yhqsglxn0kzqq00a342qmmmv52vggb9655ggfxl22")))

(define-public crate-wbdl-1.1.0 (c (n "wbdl") (v "1.1.0") (h "1r068w6p2ic9vbbsvcan9dhnp43s8czc8c97rkwvv552d8i2c9y3")))

(define-public crate-wbdl-1.2.0 (c (n "wbdl") (v "1.2.0") (h "0pfsd8g85v7dixlcaad67mfwccvssny6192nxr4zlxzzqfrr6nqx")))

