(define-module (crates-io ir cp ircprobe) #:use-module (crates-io))

(define-public crate-ircprobe-0.1.0 (c (n "ircprobe") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "clap") (r "^4.2.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "rustyline-async") (r "^0.3.1") (d #t) (k 0)) (d (n "tokio") (r "^1.27.0") (f (quote ("rt-multi-thread" "macros" "net"))) (d #t) (k 0)) (d (n "tokio-util") (r "^0.7.7") (f (quote ("codec"))) (d #t) (k 0)))) (h "1zhibnnfr43vq6c6bf0669hv91mqdq7p905vqn3fv1nv0vvfp29h")))

(define-public crate-ircprobe-0.1.1 (c (n "ircprobe") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "clap") (r "^4.2.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "rustyline-async") (r "^0.3.1") (d #t) (k 0)) (d (n "tokio") (r "^1.27.0") (f (quote ("rt-multi-thread" "macros" "net"))) (d #t) (k 0)) (d (n "tokio-util") (r "^0.7.7") (f (quote ("codec"))) (d #t) (k 0)))) (h "14d42f1jpb55m5d83f8a7lyk6fi20c7c15mghn9y3myqgxf0xpnc")))

