(define-module (crates-io ir cp ircparser) #:use-module (crates-io))

(define-public crate-ircparser-0.1.0 (c (n "ircparser") (v "0.1.0") (d (list (d (n "collection_macros") (r "^0.2.0") (d #t) (k 2)))) (h "0wicyh70dgr2279v9dlpx89h0x61vwbvfhggh70mq7s12nkzkydn")))

(define-public crate-ircparser-0.2.0 (c (n "ircparser") (v "0.2.0") (d (list (d (n "collection_macros") (r "^0.2.0") (d #t) (k 2)))) (h "0qywy9j06d6rj6z32xkys8h1yjz3pg77g90m8rdlqg4ymvnd9ykg")))

(define-public crate-ircparser-0.2.1 (c (n "ircparser") (v "0.2.1") (d (list (d (n "collection_macros") (r "^0.2.0") (d #t) (k 2)))) (h "15ajg89rp725xwpvrlk1la2w30f0aqpcn7l99y5na5iw1m8g7dxm")))

