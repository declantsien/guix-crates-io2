(define-module (crates-io ir c3 irc3) #:use-module (crates-io))

(define-public crate-irc3-0.1.0 (c (n "irc3") (v "0.1.0") (d (list (d (n "async-std") (r "^1.5") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)))) (h "08hzln4scxy92flb6fgp1prsxi2i7n63z90333pqmhwjh1apk9pi")))

(define-public crate-irc3-0.1.1 (c (n "irc3") (v "0.1.1") (d (list (d (n "async-std") (r "^1.5") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)))) (h "1ii1nr3s0a1irsv4sis34dq4pvbli314lyf429zmhslhnbay7sgc")))

(define-public crate-irc3-0.2.0 (c (n "irc3") (v "0.2.0") (d (list (d (n "async-std") (r "^1.5") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)))) (h "1i8vp8xwda3541778c64qa5hb3h7isbwkq7mpcm953dvkxmhnvkh")))

(define-public crate-irc3-0.2.1 (c (n "irc3") (v "0.2.1") (d (list (d (n "async-std") (r "^1.5") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)))) (h "0lbphb2ih10pgb3hl6rs54vqjrgkmaqsgflxnplfy2j3x3mkijl7")))

(define-public crate-irc3-0.2.2 (c (n "irc3") (v "0.2.2") (d (list (d (n "async-std") (r "^1.5") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)))) (h "1bif4017ycy84rhnym3mqw6ib9q43djz3rcf9dhshgdpxskgxhqn")))

(define-public crate-irc3-0.3.0 (c (n "irc3") (v "0.3.0") (d (list (d (n "async-std") (r "^1.5") (d #t) (k 0)) (d (n "async-tls") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)))) (h "1ib2xaln9i6gbbjc4adbwf53lza14nf8474l1qf2inb0r8xgqx32") (f (quote (("tls" "async-tls") ("default"))))))

