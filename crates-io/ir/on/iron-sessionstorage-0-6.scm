(define-module (crates-io ir on iron-sessionstorage-0-6) #:use-module (crates-io))

(define-public crate-iron-sessionstorage-0-6-0.6.6 (c (n "iron-sessionstorage-0-6") (v "0.6.6") (d (list (d (n "cookie") (r "^0.5") (f (quote ("secure"))) (d #t) (k 0)) (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "iron") (r "^0.6") (d #t) (k 0)) (d (n "r2d2") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "r2d2_redis") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)) (d (n "redis") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "router") (r "^0.6") (d #t) (k 2)) (d (n "urlencoded") (r "^0.6") (d #t) (k 2)))) (h "0kgxbhjvn9x4b7v9920f3gb2k27bycv963yccpi3v6k7rm5bkhqm") (f (quote (("redis-backend" "redis" "r2d2_redis" "r2d2"))))))

