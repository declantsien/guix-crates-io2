(define-module (crates-io ir on iron-oxide) #:use-module (crates-io))

(define-public crate-iron-oxide-0.1.0 (c (n "iron-oxide") (v "0.1.0") (d (list (d (n "block") (r "^0.1.6") (d #t) (k 0)) (d (n "colog") (r "^1.0.0") (d #t) (k 2)) (d (n "enumflags2") (r "^0.6.4") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "objc") (r "^0.2.7") (d #t) (k 0)) (d (n "png") (r "^0.16.3") (d #t) (k 2)) (d (n "raw-window-handle") (r "^0.3.3") (d #t) (k 0)) (d (n "winit") (r "^0.22.1") (d #t) (k 2)))) (h "12qs4spmg72cw2s0nvjghvwcp43fb1jv0sh733r4n3dc0nyb50my")))

(define-public crate-iron-oxide-0.1.1 (c (n "iron-oxide") (v "0.1.1") (d (list (d (n "block") (r "^0.1.6") (d #t) (k 0)) (d (n "colog") (r "^1.0.0") (d #t) (k 2)) (d (n "enumflags2") (r "^0.6.4") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "objc") (r "^0.2.7") (d #t) (k 0)) (d (n "png") (r "^0.16.3") (d #t) (k 2)) (d (n "raw-window-handle") (r "^0.3.3") (d #t) (k 0)) (d (n "winit") (r "^0.22.1") (d #t) (k 2)))) (h "1w4rkza13xsbpv3y5ky96fgzd145hm1apb5gm9cddh2hnqxkqpgd")))

