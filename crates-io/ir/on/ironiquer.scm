(define-module (crates-io ir on ironiquer) #:use-module (crates-io))

(define-public crate-ironiquer-0.1.0 (c (n "ironiquer") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "0jcx2yxgfvc8pf9y8n816y0wnm4ric3akdbpksysv6z7gbw62sm8")))

(define-public crate-ironiquer-0.1.1 (c (n "ironiquer") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "1yxrj28slal8dkzqcn0kvmwbjhg2bjmlc4x745zjwm8svb85iwin")))

