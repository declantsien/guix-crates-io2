(define-module (crates-io ir on ironmq) #:use-module (crates-io))

(define-public crate-ironmq-0.0.1 (c (n "ironmq") (v "0.0.1") (d (list (d (n "bytes") (r "^0.6.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.7") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "ironmq-codec") (r "^0.0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^0.3") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-util") (r "^0.5") (f (quote ("codec"))) (d #t) (k 0)))) (h "1xxqpzdrvpn7a5ihzs0wy17idbcdbwlspwg6cqs89lj21npab482")))

