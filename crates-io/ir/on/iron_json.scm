(define-module (crates-io ir on iron_json) #:use-module (crates-io))

(define-public crate-iron_json-1.0.0 (c (n "iron_json") (v "1.0.0") (d (list (d (n "bodyparser") (r "0.5.*") (d #t) (k 0)) (d (n "iron") (r "0.5.*") (d #t) (k 0)) (d (n "json") (r "0.11.*") (d #t) (k 0)))) (h "1w0yl3fxf5zyiv300xba5avzyk2rms2wpj4qjarzx727bk7yz1z4")))

