(define-module (crates-io ir on iron_config) #:use-module (crates-io))

(define-public crate-iron_config-0.1.0 (c (n "iron_config") (v "0.1.0") (d (list (d (n "lazy_static") (r "^0.2.2") (d #t) (k 0)) (d (n "toml") (r "^0.2.1") (d #t) (k 0)))) (h "0qz5xh41zgqknzri2x6lifqv2zqpv1lp5mazf0gymjdkjsb1q80v")))

(define-public crate-iron_config-0.1.1 (c (n "iron_config") (v "0.1.1") (d (list (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)) (d (n "toml") (r "^0.4.5") (d #t) (k 0)))) (h "00c11snlgnml524d82v2fww92dxza9y87zg9kmzhkmvyi65siqll")))

