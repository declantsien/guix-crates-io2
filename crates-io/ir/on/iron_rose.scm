(define-module (crates-io ir on iron_rose) #:use-module (crates-io))

(define-public crate-iron_rose-0.1.0 (c (n "iron_rose") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "fasthash") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 2)))) (h "1jwn53884bxsbd2bfwkcgcynj7qrv7lsfxp0mqvffjsyxmx1idgq")))

(define-public crate-iron_rose-0.1.1 (c (n "iron_rose") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "fasthash") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 2)))) (h "0pj4agn69dmpjrd4jnv44dxsyw78avxk0cbl2lxmlxdzasv4qi3r")))

