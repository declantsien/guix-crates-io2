(define-module (crates-io ir on iron-middlefiddle) #:use-module (crates-io))

(define-public crate-iron-middlefiddle-0.1.0 (c (n "iron-middlefiddle") (v "0.1.0") (d (list (d (n "iron") (r "^0.5.1") (d #t) (k 0)))) (h "1ywsv0n60fxfpnsnqsmlkim06ixcqy9pliqlqy45xdj0r6zp0k5b")))

(define-public crate-iron-middlefiddle-0.1.1 (c (n "iron-middlefiddle") (v "0.1.1") (d (list (d (n "iron") (r "^0.5.1") (d #t) (k 0)))) (h "0w2bynwby1qf7g713gikz6f2bl635zrpkq3kx2gg9jxbifkgbi4f")))

