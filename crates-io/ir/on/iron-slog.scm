(define-module (crates-io ir on iron-slog) #:use-module (crates-io))

(define-public crate-iron-slog-0.0.1 (c (n "iron-slog") (v "0.0.1") (d (list (d (n "chrono") (r "^0.3") (d #t) (k 0)) (d (n "iron") (r "^0.5") (d #t) (k 0)) (d (n "router") (r "^0.5.1") (d #t) (k 2)) (d (n "slog") (r "^2.0") (d #t) (k 0)) (d (n "slog-async") (r "^2.0") (d #t) (k 2)) (d (n "slog-term") (r "^2.0") (d #t) (k 2)))) (h "187l8b04a3psm4j29vparlvcxh270nd21bxki58ayyy70r1w637b")))

(define-public crate-iron-slog-0.0.2 (c (n "iron-slog") (v "0.0.2") (d (list (d (n "chrono") (r "^0.4.2") (d #t) (k 0)) (d (n "iron") (r "~0.6") (d #t) (k 0)) (d (n "router") (r "^0.6") (d #t) (k 2)) (d (n "slog") (r "^2.0") (d #t) (k 0)) (d (n "slog-async") (r "^2.0") (d #t) (k 2)) (d (n "slog-term") (r "^2.0") (d #t) (k 2)))) (h "0m8j0hajxcnrwpbgahc6xnk8m27r464awmzl173a8dn9s981v521")))

