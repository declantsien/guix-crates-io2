(define-module (crates-io ir on iron_vhosts) #:use-module (crates-io))

(define-public crate-iron_vhosts-0.1.0 (c (n "iron_vhosts") (v "0.1.0") (d (list (d (n "iron") (r "*") (d #t) (k 0)) (d (n "url") (r "*") (d #t) (k 0)))) (h "0wp6af7610iwpr8yb38ffdv526343zrffm3bh50fhwx2901fh0xz")))

(define-public crate-iron_vhosts-0.2.0 (c (n "iron_vhosts") (v "0.2.0") (d (list (d (n "iron") (r "^0.3.0") (d #t) (k 0)) (d (n "router") (r "^0.1.1") (d #t) (k 2)) (d (n "url") (r "^0.5") (d #t) (k 0)))) (h "1w0104r3w9yclyncprf5gagvskhiky8caina3sc2j7vqn72w90gf")))

(define-public crate-iron_vhosts-0.4.0 (c (n "iron_vhosts") (v "0.4.0") (d (list (d (n "iron") (r "^0.4.0") (d #t) (k 0)) (d (n "router") (r "^0.4.0") (d #t) (k 2)))) (h "0sqqn80yd5fzycr28fgf45hgx9jar3vhwmkv7pxkal21hmmckfv6")))

(define-public crate-iron_vhosts-0.4.1 (c (n "iron_vhosts") (v "0.4.1") (d (list (d (n "iron") (r "^0.4.0") (d #t) (k 0)) (d (n "router") (r "^0.4.0") (d #t) (k 2)))) (h "03nrkywcd7i2fjhgzgww3a7glxsgwj0ncn8j6w6zzjgq6x1sbjp8")))

(define-public crate-iron_vhosts-0.5.0 (c (n "iron_vhosts") (v "0.5.0") (d (list (d (n "iron") (r "^0.5") (d #t) (k 0)) (d (n "router") (r "^0.5") (d #t) (k 2)))) (h "045f0f99rwa4hn4vmcxkkh95xvagfjyswqr0c0hny8yjsbydr6fp")))

