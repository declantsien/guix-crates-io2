(define-module (crates-io ir on iron-login) #:use-module (crates-io))

(define-public crate-iron-login-0.1.0 (c (n "iron-login") (v "0.1.0") (d (list (d (n "cookie") (r "^0.1.21") (f (quote ("secure"))) (d #t) (k 0)) (d (n "iron") (r "^0.2") (d #t) (k 0)) (d (n "oven") (r "^0.2") (d #t) (k 0)))) (h "1briwj06a7nivqv5yyi8v0q159bw4sj1fx2d2knfzn0dy7hi34g8")))

(define-public crate-iron-login-0.1.1 (c (n "iron-login") (v "0.1.1") (d (list (d (n "cookie") (r "^0.1.21") (f (quote ("secure"))) (d #t) (k 0)) (d (n "iron") (r "^0.2") (d #t) (k 0)) (d (n "oven") (r "^0.2") (d #t) (k 0)))) (h "1m350z36k6ivyhv4kkhdhb9059maw9v0li6i73lnkmjk839p9dkp")))

(define-public crate-iron-login-0.1.2 (c (n "iron-login") (v "0.1.2") (d (list (d (n "cookie") (r "^0.1.21") (f (quote ("secure"))) (d #t) (k 0)) (d (n "iron") (r "^0.2") (d #t) (k 0)) (d (n "oven") (r "^0.2") (d #t) (k 0)))) (h "1nx7vhi1cgg8hs30jrch7qa2j5wy68xzqbmpyi0nk0i99py1k3l5")))

(define-public crate-iron-login-0.1.3 (c (n "iron-login") (v "0.1.3") (d (list (d (n "cookie") (r "^0.2") (f (quote ("secure"))) (d #t) (k 0)) (d (n "iron") (r "^0.2") (d #t) (k 0)) (d (n "oven") (r "^0.2") (d #t) (k 0)))) (h "1i9n38zabxpmdk6czpp7rn6cvjpf0xl7r7dcdsswwymiim4qq0hv")))

(define-public crate-iron-login-0.1.4 (c (n "iron-login") (v "0.1.4") (d (list (d (n "cookie") (r "^0.2") (f (quote ("secure"))) (d #t) (k 0)) (d (n "iron") (r "^0.2") (d #t) (k 0)) (d (n "oven") (r "^0.2") (d #t) (k 0)))) (h "0zpswn4f06hy76fx10zw28j5lac190ay78q0dfw8jd2s3blyqyxv")))

(define-public crate-iron-login-0.1.5 (c (n "iron-login") (v "0.1.5") (d (list (d (n "cookie") (r "^0.2") (f (quote ("secure"))) (d #t) (k 0)) (d (n "iron") (r "^0.2") (d #t) (k 0)) (d (n "oven") (r "^0.2") (d #t) (k 0)))) (h "09s49lf34zbldms72r4q3qrm1rw5kpyab99iy4b88ml5yda6d72m")))

(define-public crate-iron-login-0.1.6 (c (n "iron-login") (v "0.1.6") (d (list (d (n "cookie") (r "^0.2") (f (quote ("secure"))) (d #t) (k 0)) (d (n "iron") (r "^0.2") (d #t) (k 0)) (d (n "oven") (r "^0.2") (d #t) (k 0)))) (h "1j6fd2qrqchp7n6sc8z91c1nm9mjig32ih81nmc8f0yz7nmcn8k9")))

(define-public crate-iron-login-0.1.7 (c (n "iron-login") (v "0.1.7") (d (list (d (n "cookie") (r "^0.2") (f (quote ("secure"))) (d #t) (k 0)) (d (n "iron") (r "^0.2") (d #t) (k 0)) (d (n "oven") (r "^0.2") (d #t) (k 0)) (d (n "persistent") (r "^0.0") (d #t) (k 0)))) (h "1zqdlv61zn02d0v4yqbxfc16v2gdr0kw2hq3m93lgi8z9xxcc67z")))

(define-public crate-iron-login-0.1.8 (c (n "iron-login") (v "0.1.8") (d (list (d (n "cookie") (r "^0.2") (f (quote ("secure"))) (d #t) (k 0)) (d (n "iron") (r "^0.2") (d #t) (k 0)) (d (n "oven") (r "^0.2") (d #t) (k 0)) (d (n "persistent") (r "^0.0") (d #t) (k 0)))) (h "1lgnpjz0q81dslg7zlqisvl6247k1n8p525ix35j9ci79knlpjim")))

(define-public crate-iron-login-0.1.9 (c (n "iron-login") (v "0.1.9") (d (list (d (n "cookie") (r "^0.2") (f (quote ("secure"))) (d #t) (k 0)) (d (n "iron") (r "^0.2") (d #t) (k 0)) (d (n "oven") (r "^0.2") (d #t) (k 0)) (d (n "persistent") (r "^0.0") (d #t) (k 0)))) (h "03vp085kkz6szczibm0cdkcnmn554qlr5n9ch0ya1g4a6qly067b")))

(define-public crate-iron-login-0.2.0 (c (n "iron-login") (v "0.2.0") (d (list (d (n "cookie") (r "^0.2") (f (quote ("secure"))) (d #t) (k 0)) (d (n "iron") (r "^0.2") (d #t) (k 0)) (d (n "oven") (r "^0.2") (d #t) (k 0)) (d (n "persistent") (r "^0.0") (d #t) (k 0)))) (h "06df40wvkmwg4s2s9x4i5xpaj3xyy0wi3hydcch1va17hsnj7jy8")))

(define-public crate-iron-login-0.3.0 (c (n "iron-login") (v "0.3.0") (d (list (d (n "cookie") (r "^0.2") (f (quote ("secure"))) (d #t) (k 0)) (d (n "iron") (r "^0.2") (d #t) (k 0)) (d (n "oven") (r "^0.2") (d #t) (k 0)) (d (n "persistent") (r "^0.0") (d #t) (k 0)))) (h "13g6g95206ilqfbw6qki0zyxyqp5s0xkknlcl0y6rz75bwgxbh0g")))

(define-public crate-iron-login-0.3.1 (c (n "iron-login") (v "0.3.1") (d (list (d (n "cookie") (r "^0.2") (f (quote ("secure"))) (d #t) (k 0)) (d (n "iron") (r "^0.2") (d #t) (k 0)) (d (n "oven") (r "= 0.2.16") (d #t) (k 0)) (d (n "persistent") (r "^0.0") (d #t) (k 0)))) (h "1f4csfl3v801h4pzpwn6phk1npdk8md03kxw3z0j0z333xcn58x5")))

(define-public crate-iron-login-0.4.0 (c (n "iron-login") (v "0.4.0") (d (list (d (n "cookie") (r "^0.2") (f (quote ("secure"))) (d #t) (k 0)) (d (n "iron") (r "^0.3") (d #t) (k 0)) (d (n "oven") (r ">= 0.2.17") (d #t) (k 0)) (d (n "persistent") (r "^0.1") (d #t) (k 0)))) (h "1a2yrlqfx6zmwalfshg86mcirwc6ywi52iw48p3vi0via18cf9kj")))

(define-public crate-iron-login-0.4.1 (c (n "iron-login") (v "0.4.1") (d (list (d (n "cookie") (r "^0.2") (f (quote ("secure"))) (d #t) (k 0)) (d (n "iron") (r "^0.3") (d #t) (k 0)) (d (n "oven") (r "^0.3") (d #t) (k 0)) (d (n "persistent") (r "^0.1") (d #t) (k 0)))) (h "07s56lacmm8z0c5vy0zav2rahbf0n0f7bvhc9aijigrnb1pawj5b")))

(define-public crate-iron-login-0.5.0 (c (n "iron-login") (v "0.5.0") (d (list (d (n "cookie") (r "^0.2") (f (quote ("secure"))) (d #t) (k 0)) (d (n "iron") (r "^0.4") (d #t) (k 0)) (d (n "oven") (r "^0.4") (d #t) (k 0)) (d (n "persistent") (r "^0.2") (d #t) (k 0)))) (h "1zmg34s8mrn2x9cmfl1wd85m8gis26d387z3bn0lpnyhmv9q6xk2")))

(define-public crate-iron-login-0.5.1 (c (n "iron-login") (v "0.5.1") (d (list (d (n "cookie") (r "^0.2") (f (quote ("secure"))) (d #t) (k 0)) (d (n "iron") (r "^0.4") (d #t) (k 0)) (d (n "oven") (r "^0.4") (d #t) (k 0)) (d (n "persistent") (r "^0.2") (d #t) (k 0)))) (h "0nwqzc5iy2byf34i87d7r3jbgfxz07d8qsvws594kmglrg2vmwd8")))

