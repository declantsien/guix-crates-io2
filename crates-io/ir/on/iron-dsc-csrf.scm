(define-module (crates-io ir on iron-dsc-csrf) #:use-module (crates-io))

(define-public crate-iron-dsc-csrf-0.1.0 (c (n "iron-dsc-csrf") (v "0.1.0") (d (list (d (n "assert_matches") (r "^1.1.0") (d #t) (k 2)) (d (n "base64") (r "^0.9.0") (d #t) (k 0)) (d (n "clippy") (r "= 0.0.180") (d #t) (k 2)) (d (n "cookie") (r "^0.10.1") (d #t) (k 0)) (d (n "iron") (r "^0.6.0") (d #t) (k 0)) (d (n "iron-test") (r "^0.6.0") (d #t) (k 2)) (d (n "rand") (r "^0.4.2") (d #t) (k 0)) (d (n "subtle") (r "^0.3.0") (d #t) (k 0)))) (h "0592aiakqpfb5b76g29xa3ah9bdmmgg8y92q57n0w58qg71wz0x1")))

