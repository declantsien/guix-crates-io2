(define-module (crates-io ir on ironcladserver) #:use-module (crates-io))

(define-public crate-ironcladserver-0.1.0 (c (n "ironcladserver") (v "0.1.0") (d (list (d (n "async-std") (r "^1.6") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)))) (h "1i53x0jyks3khf9dx1n4s8zsv686iv7wl5xq2w6gwm4icky8k554")))

