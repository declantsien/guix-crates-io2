(define-module (crates-io ir on iron_rusqlite_middleware) #:use-module (crates-io))

(define-public crate-iron_rusqlite_middleware-0.1.1 (c (n "iron_rusqlite_middleware") (v "0.1.1") (d (list (d (n "iron") (r "^0.5.1") (d #t) (k 0)) (d (n "r2d2") (r "^0.7.3") (d #t) (k 0)) (d (n "r2d2_sqlite") (r "^0.2.1") (d #t) (k 0)) (d (n "rusqlite") (r "^0.12.0") (d #t) (k 0)))) (h "0057f9l7zy4rxpynbsmijdry7la97rvnbkqzb3019pxnvblzb79p")))

(define-public crate-iron_rusqlite_middleware-0.1.2 (c (n "iron_rusqlite_middleware") (v "0.1.2") (d (list (d (n "iron") (r "^0.5.1") (d #t) (k 0)) (d (n "r2d2") (r "^0.7.3") (d #t) (k 0)) (d (n "r2d2_sqlite") (r "^0.2.1") (d #t) (k 0)) (d (n "rusqlite") (r "^0.12.0") (d #t) (k 0)))) (h "08fr7f36g81ihwd25wp84vkaq26d5v23sd772h3vzgpws0kagc8y")))

