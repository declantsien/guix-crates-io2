(define-module (crates-io ir on iron-tera) #:use-module (crates-io))

(define-public crate-iron-tera-0.1.0 (c (n "iron-tera") (v "0.1.0") (d (list (d (n "iron") (r "^0.4.0") (d #t) (k 0)) (d (n "router") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_derive") (r "^0.8") (d #t) (k 0)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)) (d (n "tera") (r "^0.5.0") (d #t) (k 0)))) (h "0j5lj4nn08h8f7kzma8kypz28k848bdgbaqbq5z0ffplhsb7z5ds")))

(define-public crate-iron-tera-0.1.1 (c (n "iron-tera") (v "0.1.1") (d (list (d (n "iron") (r "^0.4.0") (d #t) (k 0)) (d (n "router") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_derive") (r "^0.8") (d #t) (k 0)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)) (d (n "tera") (r "^0.5.0") (d #t) (k 0)))) (h "1hkb2xxx0lrbh0m1vh1wddn7mlc6hy6rj1irib4k39j911gcc2yz")))

(define-public crate-iron-tera-0.1.2 (c (n "iron-tera") (v "0.1.2") (d (list (d (n "iron") (r "^0.4.0") (d #t) (k 0)) (d (n "router") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_derive") (r "^0.8") (d #t) (k 0)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)) (d (n "tera") (r "^0.6.1") (d #t) (k 0)))) (h "0jhyzb49akjzwlzbk3w1p6l8gh9is7p6804ybcbd115apci6y436")))

(define-public crate-iron-tera-0.1.3 (c (n "iron-tera") (v "0.1.3") (d (list (d (n "iron") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)) (d (n "tera") (r "^0.6.1") (d #t) (k 0)))) (h "14jh4dhbrgj38j01aisvb801gz43rn5h1s65mvw7qqpqs3jlllj0")))

(define-public crate-iron-tera-0.1.4 (c (n "iron-tera") (v "0.1.4") (d (list (d (n "iron") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)) (d (n "tera") (r "^0.6.1") (d #t) (k 0)))) (h "144083vr4437z30ga35aclmsa3m132wb8gvw3608j7bvqiag8a0n")))

(define-public crate-iron-tera-0.2.0 (c (n "iron-tera") (v "0.2.0") (d (list (d (n "iron") (r "^0.4.0") (d #t) (k 0)) (d (n "router") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (d #t) (k 0)) (d (n "tera") (r "^0.7.0") (d #t) (k 0)))) (h "1lx3insnwyc26hc8n27p2i3fn8s0463f1n1b6ab994rqmdlqki16")))

(define-public crate-iron-tera-0.3.0 (c (n "iron-tera") (v "0.3.0") (d (list (d (n "iron") (r "^0.5.0") (d #t) (k 0)) (d (n "plugin") (r "^0.2.0") (d #t) (k 0)) (d (n "router") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tera") (r "^0.10") (d #t) (k 0)))) (h "1llj614wjk7s77myvj18nrw58ikjjv7idqlw056ak0dslr2fzj0m")))

(define-public crate-iron-tera-0.4.0 (c (n "iron-tera") (v "0.4.0") (d (list (d (n "iron") (r "^0.5.0") (d #t) (k 0)) (d (n "plugin") (r "^0.2.0") (d #t) (k 0)) (d (n "router") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tera") (r "^0.10") (d #t) (k 0)))) (h "1225dn6d9fca4zwm66hc9szr9lf6fn6xdz8cjhhd0l5gbj31k3aq") (f (quote (("unstable") ("default"))))))

(define-public crate-iron-tera-0.5.0 (c (n "iron-tera") (v "0.5.0") (d (list (d (n "iron") (r "^0.6.0") (d #t) (k 0)) (d (n "plugin") (r "^0.2.0") (d #t) (k 0)) (d (n "router") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tera") (r "^0.11") (d #t) (k 0)))) (h "1hvk29ip3if5y9accpi7lhcs663dq5sc0lg4kkwdgi0l4ldlv7w3") (f (quote (("unstable") ("default"))))))

