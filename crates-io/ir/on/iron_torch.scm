(define-module (crates-io ir on iron_torch) #:use-module (crates-io))

(define-public crate-iron_torch-0.1.0 (c (n "iron_torch") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "0dxifd7r1lgg6ygdwqi428gx1fs295784rppdns3k812131jc8b5")))

(define-public crate-iron_torch-0.1.1 (c (n "iron_torch") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "163c53khb30vsaz0ix9qd9i8z7j3qfcmrsiq3jbn3i39r6a22vm3")))

(define-public crate-iron_torch-0.1.2 (c (n "iron_torch") (v "0.1.2") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "1csi2dndwffk62rs5q4759kapdhis3vj4rla1y55asc0mbfdam9g")))

(define-public crate-iron_torch-0.1.3 (c (n "iron_torch") (v "0.1.3") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "0vrhcyj43q977bvj1hs0vjh07i263hh3dkpq580v00xzy3abz45s")))

(define-public crate-iron_torch-0.1.4 (c (n "iron_torch") (v "0.1.4") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "14qwiwzaxh9w65wrp5b60hx1rf20rhwn8fgijf6x4s8f3m9vq521")))

(define-public crate-iron_torch-0.1.5 (c (n "iron_torch") (v "0.1.5") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "06wi1aqy5p5pfsiakdwhi21d5zjalwbfbxqazgzd4zgrawlyf6j4")))

(define-public crate-iron_torch-0.1.6 (c (n "iron_torch") (v "0.1.6") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "0d4nlffh20yrccjxw7q7yfvz49d6mb1vhv5smqb9w7rzb0wldgb4")))

(define-public crate-iron_torch-0.1.7 (c (n "iron_torch") (v "0.1.7") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "0favw5hp46zc240hiir8z1wl5mb9zfaw7yam5a7jmawqg7yb1swy")))

(define-public crate-iron_torch-0.1.8 (c (n "iron_torch") (v "0.1.8") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "1wz83jras0n21q9rimyb3kvvl171gnan2285agqqkl5cwi57gl80")))

(define-public crate-iron_torch-0.1.9 (c (n "iron_torch") (v "0.1.9") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "15j81afn0grd6453dzff2q1q322sj1jzy7689hr1288lkq1hpj43")))

(define-public crate-iron_torch-0.1.10 (c (n "iron_torch") (v "0.1.10") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "0cfbaimp77b149zbc52fq960p45pdgpgzzgin4fz24lzbgb77dkg")))

(define-public crate-iron_torch-0.1.11 (c (n "iron_torch") (v "0.1.11") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "0kdmcg9kghd970agllwdrf4ypyg0cmhqq9jgv2jz867cnzyr212g")))

(define-public crate-iron_torch-0.1.12 (c (n "iron_torch") (v "0.1.12") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "0dffbmsm5zn6dl3in505698l4gj419x9rpi2av4bpgq4b988jbzs")))

(define-public crate-iron_torch-0.1.13 (c (n "iron_torch") (v "0.1.13") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "1vxvjlcl2gfd112id4vw4mm5mzab3c196gpvqm8jais7jwjq4vi3")))

(define-public crate-iron_torch-0.1.14 (c (n "iron_torch") (v "0.1.14") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "1xpbayab76hgbhrn4hha752rmmxhia6r0x36v8wkzr0q2kb45bjk")))

(define-public crate-iron_torch-0.1.15 (c (n "iron_torch") (v "0.1.15") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "1qk60p0q0cjqyh1cp8hqk92sgdj51pqzbxq88g3ck838v4v8apaw")))

(define-public crate-iron_torch-0.1.16 (c (n "iron_torch") (v "0.1.16") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "003ls4p2lmwlzvvqp7c5zvs1ys92cmchwmxjgh0wqfpgjphspmwv")))

(define-public crate-iron_torch-0.1.17 (c (n "iron_torch") (v "0.1.17") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "1m4s3wg250acr2fwglqv1ayj7152gg4qsgsc5jazgbiqj46pmz8d")))

(define-public crate-iron_torch-0.1.18 (c (n "iron_torch") (v "0.1.18") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "19xw5s5gd231ppzqq7idp5izzk5plxjy6vwrnqxml771l3fwfcrv")))

