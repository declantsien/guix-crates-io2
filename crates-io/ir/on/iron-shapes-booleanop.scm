(define-module (crates-io ir on iron-shapes-booleanop) #:use-module (crates-io))

(define-public crate-iron-shapes-booleanop-0.0.2 (c (n "iron-shapes-booleanop") (v "0.0.2") (d (list (d (n "iron-shapes") (r "^0.0.7") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "libreda-splay") (r "^0.1") (d #t) (k 0)) (d (n "num-integer") (r "^0.1") (d #t) (k 0)) (d (n "num-rational") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "10cfkih5r9f6hmw7442b2yrrja47aadcawqp16wxks8j4qzn62kj")))

(define-public crate-iron-shapes-booleanop-0.0.3 (c (n "iron-shapes-booleanop") (v "0.0.3") (d (list (d (n "iron-shapes") (r "^0.0.8") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "libreda-splay") (r "^0.1") (d #t) (k 0)) (d (n "num-integer") (r "^0.1") (d #t) (k 0)) (d (n "num-rational") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "18kzlmgx947x3s54jl44v870p3ky23g7jh3pd1jl3wjy1l2c05ji")))

