(define-module (crates-io ir on iron_requestid) #:use-module (crates-io))

(define-public crate-iron_requestid-0.1.0 (c (n "iron_requestid") (v "0.1.0") (d (list (d (n "iron") (r "^0.5.1") (d #t) (k 0)) (d (n "uuid") (r "^0.4.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "1bnzgk19wisf6brp08pdg9x49nnsicrvvb8vwrvna54dqfqpnnki")))

(define-public crate-iron_requestid-0.1.1 (c (n "iron_requestid") (v "0.1.1") (d (list (d (n "iron") (r "^0.5.1") (d #t) (k 0)) (d (n "uuid") (r "^0.4.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "0cmv1j3l10kjglhnk5mrsmifj1didisgivwd08a477fny6rn84ws")))

