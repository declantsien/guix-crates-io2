(define-module (crates-io ir on ironplc-dsl) #:use-module (crates-io))

(define-public crate-ironplc-dsl-0.1.0 (c (n "ironplc-dsl") (v "0.1.0") (d (list (d (n "time") (r "^0.3.11") (d #t) (k 0)))) (h "1ns81a9ka04vvmj79pjnjanzqx13ddldivchg3zpmmxi6msdwpwv")))

(define-public crate-ironplc-dsl-0.1.1 (c (n "ironplc-dsl") (v "0.1.1") (d (list (d (n "time") (r "^0.3.17") (d #t) (k 0)))) (h "0yb6kch1fmbx8iq8f54s39nv6j1fwh0f7irfbalh5dfkq6khzscj")))

