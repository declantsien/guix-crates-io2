(define-module (crates-io ir on ironfetch) #:use-module (crates-io))

(define-public crate-ironfetch-0.1.0 (c (n "ironfetch") (v "0.1.0") (d (list (d (n "phf") (r "^0.11.1") (f (quote ("macros"))) (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "sysinfo") (r "^0.26.5") (d #t) (k 0)) (d (n "whoami") (r "^1.2.3") (d #t) (k 0)))) (h "0kzkvv30shkng532ydp98j1hn6l08hci846z7yf039s2qjhjz4j1")))

(define-public crate-ironfetch-0.2.0 (c (n "ironfetch") (v "0.2.0") (d (list (d (n "phf") (r "^0.11.1") (f (quote ("macros"))) (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "sysinfo") (r "^0.26.5") (d #t) (k 0)) (d (n "whoami") (r "^1.2.3") (d #t) (k 0)))) (h "1d23w9lh976bmn4zl4ih9xfy22yd0c363bj8d9fd7z40i6c97rmc")))

