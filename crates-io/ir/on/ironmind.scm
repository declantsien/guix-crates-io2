(define-module (crates-io ir on ironmind) #:use-module (crates-io))

(define-public crate-ironmind-0.1.0 (c (n "ironmind") (v "0.1.0") (h "1qjn1wqdjiwsl2vvxmw33112fidhbv7064xa7drzmq1mbhdv5mam")))

(define-public crate-ironmind-0.1.1 (c (n "ironmind") (v "0.1.1") (h "1198vmw1mpvyik6fvwn9h5dsf5w4v9c4rhd2n3w654ppyzxcbizz")))

(define-public crate-ironmind-0.1.2 (c (n "ironmind") (v "0.1.2") (h "065h1ly26lb07zk4r4rnza3x96wc9biyzmwlpj2dciv3q057gpmb")))

(define-public crate-ironmind-0.1.3 (c (n "ironmind") (v "0.1.3") (h "0d4ss9b7fcs4yki1mhpj8xz46q9fgxnir6d1b0kn31dd15ifxvl9")))

(define-public crate-ironmind-0.1.4 (c (n "ironmind") (v "0.1.4") (h "0vzblmlqn3m699y2h5d2n520477xma0djf5cmsclq2npc0klh427")))

(define-public crate-ironmind-0.1.5 (c (n "ironmind") (v "0.1.5") (h "0wmqzhrjh5h4ynbvy0cza9chm284g9w6v6hkphc18bj9mncq6k8q")))

(define-public crate-ironmind-0.1.6 (c (n "ironmind") (v "0.1.6") (h "04byf0nww5m0qbd1yjnz379jnwm632lljr3nnhrc9aflwpk9n164")))

(define-public crate-ironmind-0.1.7 (c (n "ironmind") (v "0.1.7") (h "0w62v12m6iv6w41n9slp4jjk8kjyf8ap4ccfqa4wm0pc69fgs41c")))

(define-public crate-ironmind-0.2.0 (c (n "ironmind") (v "0.2.0") (h "08l2fycl46n0x322aw9w98lsl1m6a6jas5ncn1hwhd3mavyw7g1i")))

(define-public crate-ironmind-0.2.1 (c (n "ironmind") (v "0.2.1") (h "1pg5r3a2dk477xa3vwfsb2pr05cb2l89smqvxszpxcdwpql8qh96")))

(define-public crate-ironmind-0.2.2 (c (n "ironmind") (v "0.2.2") (h "01s83963sank6qlx6myr3rd3s1nfwv758bi4wg4qj70p4ssbalr7")))

(define-public crate-ironmind-0.2.3 (c (n "ironmind") (v "0.2.3") (d (list (d (n "clap") (r "^4.4.10") (f (quote ("derive"))) (d #t) (k 0)))) (h "0qyd35wcfi71mbicdlrkbyslv73kjrfbp0kksjbdym3zsxj47h0x")))

(define-public crate-ironmind-0.2.4 (c (n "ironmind") (v "0.2.4") (d (list (d (n "clap") (r "^4.4.10") (f (quote ("derive"))) (d #t) (k 0)))) (h "1rvk3g8i54xpisx4agdpnpla4q5b7alnaifn22prr6d843y88cj3")))

(define-public crate-ironmind-0.2.5 (c (n "ironmind") (v "0.2.5") (d (list (d (n "clap") (r "^4.4.10") (f (quote ("derive"))) (d #t) (k 0)))) (h "0c5bn411hx5h6dlpjl1d57kc8r0i40yai5kc4360qxhrczq5acrh")))

(define-public crate-ironmind-0.2.6 (c (n "ironmind") (v "0.2.6") (d (list (d (n "clap") (r "^4.4.10") (f (quote ("derive"))) (d #t) (k 0)))) (h "1ripjx8sgmh1lnl6ar51sdaj9ahwx751d72id5m3nn6cdzyw7dgd")))

(define-public crate-ironmind-0.2.7 (c (n "ironmind") (v "0.2.7") (d (list (d (n "clap") (r "^4.4.10") (f (quote ("derive"))) (d #t) (k 0)))) (h "0qbjqzzcz3zd65r9899k1k664imwxgk8cn7nfyx4bkg5bhcp09ms")))

(define-public crate-ironmind-0.2.8 (c (n "ironmind") (v "0.2.8") (d (list (d (n "clap") (r "^4.4.10") (f (quote ("derive"))) (d #t) (k 0)))) (h "1r2ik0pq3gbv3yqbqkx49hq08y8x34s456c0hl95xk60l735zdbi")))

(define-public crate-ironmind-0.3.0 (c (n "ironmind") (v "0.3.0") (d (list (d (n "clap") (r "^4.4.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "cursive") (r "^0.20.0") (d #t) (k 0)))) (h "092hjf0damhgrbjz096jqxl2l8g9y3jgkh48njc18pi7s47vgpic")))

(define-public crate-ironmind-0.3.1 (c (n "ironmind") (v "0.3.1") (d (list (d (n "clap") (r "^4.4.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "cursive") (r "^0.20.0") (d #t) (k 0)))) (h "09djzbn1pk11b7l2k15kw1nm99gfkj9ppc2ifggkwhbr5w1h53z2")))

(define-public crate-ironmind-0.3.2 (c (n "ironmind") (v "0.3.2") (d (list (d (n "clap") (r "^4.4.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "cursive") (r "^0.20.0") (d #t) (k 0)))) (h "06ndqb88b2c5q42909yxxk391hy8qg6rcyqrmgkarzs7ippa8pmz")))

(define-public crate-ironmind-0.3.3 (c (n "ironmind") (v "0.3.3") (d (list (d (n "clap") (r "^4.4.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "cursive") (r "^0.20.0") (d #t) (k 0)))) (h "1lwxj2yqi2fvzp1zj95k4n0dz7g6spc05924n9jmx0laprq2kmqg")))

(define-public crate-ironmind-0.3.4 (c (n "ironmind") (v "0.3.4") (d (list (d (n "clap") (r "^4.4.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "cursive") (r "^0.20.0") (d #t) (k 0)))) (h "16lwdjqzfw4ai107xdg966g8r6qkh6803856l386krjdvq5gikyj")))

(define-public crate-ironmind-0.3.5 (c (n "ironmind") (v "0.3.5") (d (list (d (n "clap") (r "^4.4.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "cursive") (r "^0.20.0") (d #t) (k 0)))) (h "0nncqhnxfb1r8vhag6lm7hlsklh8zs136ripn67mrkdpwslh424h")))

(define-public crate-ironmind-0.3.6 (c (n "ironmind") (v "0.3.6") (d (list (d (n "clap") (r "^4.4.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "cursive") (r "^0.20.0") (d #t) (k 0)))) (h "0kvid8pq734yvraghx5my31smrjlzpvi2cjkcx99pqlf003mbn2i")))

(define-public crate-ironmind-0.3.7 (c (n "ironmind") (v "0.3.7") (d (list (d (n "clap") (r "^4.4.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "cursive") (r "^0.20.0") (d #t) (k 0)))) (h "1x3lsvznck2k6kbf68fjnfndpa3mkbz0lzpvfzqpkkgvgdlf57ip")))

(define-public crate-ironmind-0.3.8 (c (n "ironmind") (v "0.3.8") (d (list (d (n "clap") (r "^4.4.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "cursive") (r "^0.20.0") (d #t) (k 0)))) (h "0kmgyflfb05aapdr83147z3c3v96zqh7k3wgf3g9f8h6gk5cjmhl")))

(define-public crate-ironmind-0.3.9 (c (n "ironmind") (v "0.3.9") (d (list (d (n "clap") (r "^4.4.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "cursive") (r "^0.20.0") (d #t) (k 0)))) (h "0wlfhm61swpjs1bbl3dv2z4kfvl1kfwm2gp9jzk70kmcxql9rli2")))

(define-public crate-ironmind-0.3.10 (c (n "ironmind") (v "0.3.10") (d (list (d (n "clap") (r "^4.4.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "cursive") (r "^0.20.0") (d #t) (k 0)))) (h "0nhahsr6pqp6n1cq0r536rqn2y5ndm0wrnx36m8sv2p3g7svwvb5")))

(define-public crate-ironmind-0.3.11 (c (n "ironmind") (v "0.3.11") (d (list (d (n "clap") (r "^4.4.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "cursive") (r "^0.20.0") (d #t) (k 0)))) (h "19nq9wpl873x9ki5pp3y41zardd5431b6lvsjs8022b662wgnh74")))

(define-public crate-ironmind-0.3.12 (c (n "ironmind") (v "0.3.12") (d (list (d (n "clap") (r "^4.4.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "cursive") (r "^0.20.0") (d #t) (k 0)))) (h "1s10bb2adq7qj17i49psnsk6pp4i0hnfvhjcxplhljmwffr31xw7")))

(define-public crate-ironmind-0.3.13 (c (n "ironmind") (v "0.3.13") (d (list (d (n "clap") (r "^4.4.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "cursive") (r "^0.20.0") (d #t) (k 0)))) (h "1j9fiqgkqk7xi4wh08s43anq6gkns8qs2p52f14f8x3nc6s8yl6a")))

(define-public crate-ironmind-0.3.14 (c (n "ironmind") (v "0.3.14") (d (list (d (n "clap") (r "^4.4.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "cursive") (r "^0.20.0") (d #t) (k 0)))) (h "0fd82kg8ayc1wvcx2xhy8ahl2fgka31nzkyd6zcqwsbik9gsjiyc")))

(define-public crate-ironmind-0.3.15 (c (n "ironmind") (v "0.3.15") (d (list (d (n "clap") (r "^4.4.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "cursive") (r "^0.20.0") (d #t) (k 0)))) (h "0rfjbccgy85xhrvz1vn4i1611clxb490nfazf0lkrjx7bccrazcn")))

(define-public crate-ironmind-0.3.16 (c (n "ironmind") (v "0.3.16") (d (list (d (n "clap") (r "^4.4.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "cursive") (r "^0.20.0") (d #t) (k 0)))) (h "0r1w8h8zmayc0h8xys2lk03ixnqpryxd43ya35fkqkxr47mzi6i4")))

