(define-module (crates-io ir on irondash_jni_context) #:use-module (crates-io))

(define-public crate-irondash_jni_context-0.1.0 (c (n "irondash_jni_context") (v "0.1.0") (d (list (d (n "jni") (r "^0.19") (d #t) (t "cfg(target_os = \"android\")") (k 0)) (d (n "once_cell") (r "^1.16.0") (d #t) (t "cfg(target_os = \"android\")") (k 0)))) (h "19dqarh7alrc77vvchvis96f4rr873mbw4f85041q5qzhcqxnrrg")))

(define-public crate-irondash_jni_context-0.1.1 (c (n "irondash_jni_context") (v "0.1.1") (d (list (d (n "jni") (r "^0.19") (d #t) (t "cfg(target_os = \"android\")") (k 0)) (d (n "once_cell") (r "^1.16.0") (d #t) (t "cfg(target_os = \"android\")") (k 0)))) (h "0c9n5bjxsy938clrc6vmwq86qkdxpg5y8c27py59bmhvmiwqrwi3")))

(define-public crate-irondash_jni_context-0.1.2 (c (n "irondash_jni_context") (v "0.1.2") (d (list (d (n "jni") (r "^0.19") (d #t) (t "cfg(target_os = \"android\")") (k 0)) (d (n "once_cell") (r "^1.16.0") (d #t) (t "cfg(target_os = \"android\")") (k 0)))) (h "0s3dhys5m0yp6g41dd710fhk4smr3209pap1saqw0mwjscwfichb")))

(define-public crate-irondash_jni_context-0.2.0 (c (n "irondash_jni_context") (v "0.2.0") (d (list (d (n "jni") (r "^0.21.1") (d #t) (t "cfg(target_os = \"android\")") (k 0)) (d (n "once_cell") (r "^1.16.0") (d #t) (t "cfg(target_os = \"android\")") (k 0)))) (h "08m4nqq6qnni0fj0691p4az67h4msm3ifcq9rgbxli4d8ac2jl3a")))

