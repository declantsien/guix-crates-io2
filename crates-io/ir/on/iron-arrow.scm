(define-module (crates-io ir on iron-arrow) #:use-module (crates-io))

(define-public crate-iron-arrow-0.1.0 (c (n "iron-arrow") (v "0.1.0") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1fpfph73dq6idc4rmsfkixmgl0mby6smd13hg0hc2w06zss7ikyv")))

