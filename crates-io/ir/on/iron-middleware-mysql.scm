(define-module (crates-io ir on iron-middleware-mysql) #:use-module (crates-io))

(define-public crate-iron-middleware-mysql-0.0.1 (c (n "iron-middleware-mysql") (v "0.0.1") (d (list (d (n "iron") (r "^0.3.0") (d #t) (k 0)) (d (n "mysql") (r "^5.1.0") (d #t) (k 0)))) (h "1hrpmkrxqi82yf9f27bjqrb7q4fnfpiyd46f0pckf4jp2jh3p4ig")))

(define-public crate-iron-middleware-mysql-0.0.2 (c (n "iron-middleware-mysql") (v "0.0.2") (d (list (d (n "iron") (r "^0.3.0") (d #t) (k 0)) (d (n "mysql") (r "^5.1.0") (d #t) (k 0)))) (h "0n4qr1ai0icwacxklac4jh7kcf3jal655cji7qw1d7mi2zz7vx8d")))

