(define-module (crates-io ir on ironbird_jni_context) #:use-module (crates-io))

(define-public crate-ironbird_jni_context-0.1.0 (c (n "ironbird_jni_context") (v "0.1.0") (d (list (d (n "jni") (r "^0.19") (d #t) (t "cfg(target_os = \"android\")") (k 0)) (d (n "once_cell") (r "^1.16.0") (d #t) (t "cfg(target_os = \"android\")") (k 0)))) (h "1gf85rhv2h0rvn7k9hsk835nv0yqb36n5990szm2z8brz55njq5i")))

