(define-module (crates-io ir on ironplc-plc2x) #:use-module (crates-io))

(define-public crate-ironplc-plc2x-0.1.0 (c (n "ironplc-plc2x") (v "0.1.0") (d (list (d (n "ironplc-dsl") (r "^0.1.0") (d #t) (k 0)) (d (n "ironplc-parser") (r "^0.1.0") (d #t) (k 0)) (d (n "time") (r "^0.3.11") (d #t) (k 0)))) (h "073p4ljwfnsxr7m46bxc0c3aiimc62d0pmfgmhqn9lq7zdll2xz4")))

(define-public crate-ironplc-plc2x-0.1.1 (c (n "ironplc-plc2x") (v "0.1.1") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "codespan-reporting") (r "^0.11.1") (d #t) (k 0)) (d (n "ironplc-dsl") (r "^0.1.1") (d #t) (k 0)) (d (n "ironplc-parser") (r "^0.1.1") (d #t) (k 0)) (d (n "petgraph") (r "^0.6.2") (d #t) (k 0)) (d (n "phf") (r "^0.11") (f (quote ("macros"))) (d #t) (k 0)) (d (n "time") (r "^0.3.17") (d #t) (k 0)))) (h "0bdvah4gy67j5hknq6sl9mqzmy8kwg63l60cmdyhg7sfwcs338zx") (f (quote (("trace" "ironplc-parser/trace"))))))

