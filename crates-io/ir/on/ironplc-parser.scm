(define-module (crates-io ir on ironplc-parser) #:use-module (crates-io))

(define-public crate-ironplc-parser-0.1.0 (c (n "ironplc-parser") (v "0.1.0") (d (list (d (n "ironplc-dsl") (r "^0.1.0") (d #t) (k 0)) (d (n "peg") (r "^0.8.0") (f (quote ("trace"))) (d #t) (k 0)) (d (n "time") (r "^0.3.11") (d #t) (k 0)))) (h "17jid6kkflsjpnys4m701fvbjwy8z8v4w48c82ny9ifkqa7jylkm")))

(define-public crate-ironplc-parser-0.1.1 (c (n "ironplc-parser") (v "0.1.1") (d (list (d (n "codespan-reporting") (r "^0.11.1") (d #t) (k 0)) (d (n "ironplc-dsl") (r "^0.1.1") (d #t) (k 0)) (d (n "peg") (r "^0.8.1") (d #t) (k 0)) (d (n "time") (r "^0.3.17") (d #t) (k 0)))) (h "10mq03rn513pd325y6rnsnp8ki52hz8n1zzrs9jrly6bz09wkkwv") (f (quote (("trace" "peg/trace"))))))

