(define-module (crates-io ir on iron-pack) #:use-module (crates-io))

(define-public crate-iron-pack-0.1.0 (c (n "iron-pack") (v "0.1.0") (d (list (d (n "iron") (r "^0.5") (d #t) (k 0)) (d (n "iron-test") (r "^0.5") (d #t) (k 2)) (d (n "libflate") (r "^0.1") (d #t) (k 0)))) (h "0jvfdjkfipj1im5j7k9g7r48ibbqg57wwbya6mkvz05f5hpibqs2")))

(define-public crate-iron-pack-0.2.0 (c (n "iron-pack") (v "0.2.0") (d (list (d (n "brotli") (r "^1.0") (d #t) (k 0)) (d (n "iron") (r "^0.5") (d #t) (k 0)) (d (n "iron-test") (r "^0.5") (d #t) (k 2)) (d (n "libflate") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "0bwpispw8ix8swky8sc70a2cid2q0rb8fgwgrir4411gg5w6iza2") (f (quote (("unstable"))))))

(define-public crate-iron-pack-0.3.0 (c (n "iron-pack") (v "0.3.0") (d (list (d (n "brotli") (r "^1.0") (d #t) (k 0)) (d (n "iron") (r "^0") (d #t) (k 0)) (d (n "iron-test") (r "^0") (d #t) (k 2)) (d (n "libflate") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "100ycdhi5xldyzjjgpg9m5ajc03cy5wqsysgpzmji4wwlrj8j6li") (f (quote (("unstable"))))))

