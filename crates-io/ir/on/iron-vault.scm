(define-module (crates-io ir on iron-vault) #:use-module (crates-io))

(define-public crate-iron-vault-0.1.0 (c (n "iron-vault") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.202") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.117") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1qmyzfrzjq6cj6m3dl5k5d4ay5zkn335iizaq0vviry3ii4ikn0j")))

