(define-module (crates-io ir on ironshell) #:use-module (crates-io))

(define-public crate-IronShell-0.1.0 (c (n "IronShell") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "git2") (r "^0.18.3") (d #t) (k 0)) (d (n "termcolor") (r "^1.4.1") (d #t) (k 0)))) (h "0qb7ydihlha4wc1ajplwvx2bzcx6kmrzhgwpd181yq9c3ag3dsgm")))

(define-public crate-IronShell-0.1.1 (c (n "IronShell") (v "0.1.1") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "git2") (r "^0.18.3") (d #t) (k 0)) (d (n "termcolor") (r "^1.4.1") (d #t) (k 0)))) (h "0gcwz835ayhz8b2gzcri4zibhkmypym8njqfhi8nqqx7g0zxqmbx")))

