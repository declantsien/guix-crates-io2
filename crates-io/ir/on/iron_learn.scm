(define-module (crates-io ir on iron_learn) #:use-module (crates-io))

(define-public crate-iron_learn-0.0.1 (c (n "iron_learn") (v "0.0.1") (h "0cgz5jgx4i7cxqzz0hgyjf95pjs31hcjww0yzway9xl1blz0v2rq") (y #t)))

(define-public crate-iron_learn-0.1.0 (c (n "iron_learn") (v "0.1.0") (h "1a8w67g510c8wnjbbkjxqmwp91wf50fzxfwyrg2n58ng9iviihyj")))

(define-public crate-iron_learn-0.1.1 (c (n "iron_learn") (v "0.1.1") (h "03kz1mal3l7q8jiwhqhi1m3y71ykp0kk9shficjl8g85mzs68zrb")))

(define-public crate-iron_learn-0.2.0 (c (n "iron_learn") (v "0.2.0") (h "0p899padp4g0i1v0s18y95gmnkzw07lbvb70nrj6ngrcvhxwnbkr")))

(define-public crate-iron_learn-0.3.0 (c (n "iron_learn") (v "0.3.0") (h "1mxid14hl5llzdgs6iv176l82570pmjkpjzh8s3jhjvgm52p3dc6")))

(define-public crate-iron_learn-0.3.1 (c (n "iron_learn") (v "0.3.1") (h "0z2fsj3w9dn45z7czn325dm2w2ymvp36wyhv1jn2pkcvixy4i7v4")))

(define-public crate-iron_learn-0.3.2 (c (n "iron_learn") (v "0.3.2") (h "0wnmbk7y5rak52bpakfm1byk8gm522f7sh02yvblb25pbfpfzhkb")))

