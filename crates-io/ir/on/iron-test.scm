(define-module (crates-io ir on iron-test) #:use-module (crates-io))

(define-public crate-iron-test-0.0.1 (c (n "iron-test") (v "0.0.1") (d (list (d (n "hyper") (r "*") (d #t) (k 0)) (d (n "intovec") (r "*") (d #t) (k 0)) (d (n "iron") (r "*") (d #t) (k 0)) (d (n "url") (r "*") (d #t) (k 0)) (d (n "uuid") (r "*") (d #t) (k 0)))) (h "0c2fl9vppbkji35lqmjwyjcr2q9g9i7175rbnjlhdf68gwy3jzbx")))

(define-public crate-iron-test-0.0.2 (c (n "iron-test") (v "0.0.2") (d (list (d (n "hyper") (r "*") (d #t) (k 0)) (d (n "intovec") (r "*") (d #t) (k 0)) (d (n "iron") (r "*") (d #t) (k 0)) (d (n "url") (r "*") (d #t) (k 0)) (d (n "uuid") (r "*") (d #t) (k 0)))) (h "04599m53hijqr8qmad79qhn2x87fx0w1ggdc6mqi6gykdnk2w9pn")))

(define-public crate-iron-test-0.0.3 (c (n "iron-test") (v "0.0.3") (d (list (d (n "hyper") (r "*") (d #t) (k 0)) (d (n "intovec") (r "*") (d #t) (k 0)) (d (n "iron") (r "*") (d #t) (k 0)) (d (n "url") (r "*") (d #t) (k 0)) (d (n "uuid") (r "*") (d #t) (k 0)))) (h "0vgdlb1641dlb684q44ggrvnmdw9l4pmp9gz927vawpsdv4lc0m1")))

(define-public crate-iron-test-0.0.4 (c (n "iron-test") (v "0.0.4") (d (list (d (n "hyper") (r "*") (d #t) (k 0)) (d (n "intovec") (r "*") (d #t) (k 0)) (d (n "iron") (r "*") (d #t) (k 0)) (d (n "url") (r "*") (d #t) (k 0)) (d (n "uuid") (r "*") (d #t) (k 0)))) (h "0016b41fgjg81dblwr5z4y8px5y4c2fbv28fr1d53kxb14zhzrfw")))

(define-public crate-iron-test-0.0.5 (c (n "iron-test") (v "0.0.5") (d (list (d (n "hyper") (r "*") (d #t) (k 0)) (d (n "intovec") (r "*") (d #t) (k 0)) (d (n "iron") (r "*") (d #t) (k 0)) (d (n "url") (r "*") (d #t) (k 0)) (d (n "uuid") (r "*") (d #t) (k 0)))) (h "031fldb9wbkibvyi616r901hfvh3hd3jcqmnaaw9cgy6569s7rx1")))

(define-public crate-iron-test-0.0.6 (c (n "iron-test") (v "0.0.6") (d (list (d (n "hyper") (r "*") (d #t) (k 0)) (d (n "iron") (r "*") (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)) (d (n "url") (r "*") (d #t) (k 0)) (d (n "uuid") (r "*") (d #t) (k 0)))) (h "1pmkz8xlpz1abvdnlx8dwh1x31x4na9dx8na20yzb8x5d4b8hf6g")))

(define-public crate-iron-test-0.0.7 (c (n "iron-test") (v "0.0.7") (d (list (d (n "hyper") (r "*") (d #t) (k 0)) (d (n "iron") (r "*") (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)) (d (n "url") (r "*") (d #t) (k 0)) (d (n "uuid") (r "*") (d #t) (k 0)))) (h "0z0bq7csdrgl7wmxgk745wnyrh234kjh7n5sfw63bglwpx0785ix")))

(define-public crate-iron-test-0.0.8 (c (n "iron-test") (v "0.0.8") (d (list (d (n "hyper") (r "*") (d #t) (k 0)) (d (n "iron") (r "*") (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)) (d (n "url") (r "*") (d #t) (k 0)) (d (n "uuid") (r "*") (d #t) (k 0)))) (h "0r4n8myyc3xh7firsif4v3s9p71wxjqz49ncx3lcjwkm2r930l1m")))

(define-public crate-iron-test-0.0.9 (c (n "iron-test") (v "0.0.9") (d (list (d (n "hyper") (r "*") (d #t) (k 0)) (d (n "iron") (r "*") (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)) (d (n "url") (r "*") (d #t) (k 0)) (d (n "uuid") (r "*") (d #t) (k 0)))) (h "1lqrf35nvrlzsxlzyi72nqg6fv3zycqn84lj95q1ji14644nss5j")))

(define-public crate-iron-test-0.0.10 (c (n "iron-test") (v "0.0.10") (d (list (d (n "hyper") (r "*") (d #t) (k 0)) (d (n "iron") (r "*") (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)) (d (n "url") (r "*") (d #t) (k 0)) (d (n "uuid") (r "*") (d #t) (k 0)))) (h "07w0k2gcfp97vi419x90syp1czl470fll35kyhdd6122i2m4y6qg")))

(define-public crate-iron-test-0.1.0 (c (n "iron-test") (v "0.1.0") (d (list (d (n "hyper") (r "*") (d #t) (k 0)) (d (n "iron") (r "*") (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)) (d (n "url") (r "*") (d #t) (k 0)) (d (n "uuid") (r "*") (d #t) (k 0)))) (h "1fw7pgl1mmgp7s4q9w2bfz1klszzz00ppnvqhwjw7h6hb34ginfd")))

(define-public crate-iron-test-0.1.1 (c (n "iron-test") (v "0.1.1") (d (list (d (n "hyper") (r "^0.7") (d #t) (k 0)) (d (n "iron") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "url") (r "^0.5") (d #t) (k 0)) (d (n "uuid") (r "^0.1") (d #t) (k 0)))) (h "1n3l1kk89qfm428iqc1k0lf0pmsqbbj4hn71qww225gdnnd9ls9g")))

(define-public crate-iron-test-0.2.0 (c (n "iron-test") (v "0.2.0") (d (list (d (n "hyper") (r "^0.7") (d #t) (k 0)) (d (n "iron") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "mime") (r "^0.1") (d #t) (k 2)) (d (n "router") (r "^0.0") (d #t) (k 2)) (d (n "url") (r "^0.5") (d #t) (k 0)) (d (n "urlencoded") (r "^0.2") (d #t) (k 2)) (d (n "uuid") (r "^0.1") (d #t) (k 0)))) (h "1pmxs65w34v7bdpzpry2crk4ipyjxp5rsxgnqks3h09ylz1i9fkg")))

(define-public crate-iron-test-0.3.0 (c (n "iron-test") (v "0.3.0") (d (list (d (n "hyper") (r "^0.8") (d #t) (k 0)) (d (n "iron") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "mime") (r "^0.2") (d #t) (k 2)) (d (n "router") (r "^0.1") (d #t) (k 2)) (d (n "url") (r "^0.5") (d #t) (k 0)) (d (n "urlencoded") (r "^0.3") (d #t) (k 2)) (d (n "uuid") (r "^0.1") (d #t) (k 0)))) (h "08426gbglid3dwsasghbfkqgkg6sdk4pf3ij84nn8vp20irjf8ix")))

(define-public crate-iron-test-0.4.0 (c (n "iron-test") (v "0.4.0") (d (list (d (n "hyper") (r "^0.9") (d #t) (k 0)) (d (n "iron") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "mime") (r "^0.2") (d #t) (k 2)) (d (n "router") (r "^0.2") (d #t) (k 2)) (d (n "url") (r "^1.1") (d #t) (k 0)) (d (n "urlencoded") (r "^0.4") (d #t) (k 2)) (d (n "uuid") (r "^0.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "1arr8j9n3blfz0vx24majpfv4pj4wawk205kdhc8gywfas496qrk")))

(define-public crate-iron-test-0.5.0 (c (n "iron-test") (v "0.5.0") (d (list (d (n "hyper") (r "^0.10") (d #t) (k 0)) (d (n "iron") (r "^0.5") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "mime") (r "^0.2") (d #t) (k 2)) (d (n "router") (r "^0.5") (d #t) (k 2)) (d (n "url") (r "^1.1") (d #t) (k 0)) (d (n "urlencoded") (r "^0.5") (d #t) (k 2)) (d (n "uuid") (r "^0.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "05kabfhkbl1vzjirwdm6qqbdfljjid9vhi2w9fv4y2mwhncm2pc6")))

(define-public crate-iron-test-0.6.0 (c (n "iron-test") (v "0.6.0") (d (list (d (n "hyper") (r "^0.10") (d #t) (k 0)) (d (n "iron") (r "^0.6.0") (d #t) (k 0)) (d (n "log") (r "^0.3.8") (d #t) (k 0)) (d (n "mime") (r "^0.3.5") (d #t) (k 2)) (d (n "router") (r "^0.6.0") (d #t) (k 2)) (d (n "url") (r "^1.6.0") (d #t) (k 0)) (d (n "urlencoded") (r "^0.6.0") (d #t) (k 2)) (d (n "uuid") (r "^0.5.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "1hc3s90rl5vhvvb7hiwkh2a5wxfrgm8ibrq1pwggbczq637lp55i")))

