(define-module (crates-io ir on iron-error-router) #:use-module (crates-io))

(define-public crate-iron-error-router-0.1.0 (c (n "iron-error-router") (v "0.1.0") (d (list (d (n "iron") (r "^0.2") (d #t) (k 0)))) (h "12czb1sgy0j7hvbhxiiscdndri6icrmnl4q8hvpvcslkic6n3iy5")))

(define-public crate-iron-error-router-0.1.1 (c (n "iron-error-router") (v "0.1.1") (d (list (d (n "iron") (r "^0.2") (d #t) (k 0)))) (h "0ymkgagmzcafm22hiwpk5njbmb94wxa7f0wjvf78rfwc5wqrg377")))

(define-public crate-iron-error-router-0.1.2 (c (n "iron-error-router") (v "0.1.2") (d (list (d (n "iron") (r "^0.3") (d #t) (k 0)))) (h "1f1211d4ks7bm7prpss8s0d06rc2gp8mca1ylvxcbsik8qzzi222")))

(define-public crate-iron-error-router-0.1.3 (c (n "iron-error-router") (v "0.1.3") (d (list (d (n "iron") (r "^0.3") (d #t) (k 0)))) (h "07qpdz7djm5b3bm30r5bshblr97lp7pmni3qkgrdkpy14xwfck6z")))

(define-public crate-iron-error-router-0.1.5 (c (n "iron-error-router") (v "0.1.5") (d (list (d (n "iron") (r "^0.4") (d #t) (k 0)))) (h "0f4azbcrvabb8jsnv91pszbvci7nhmgd4zpxqdpg181a3sq677rq") (y #t)))

(define-public crate-iron-error-router-0.2.0 (c (n "iron-error-router") (v "0.2.0") (d (list (d (n "iron") (r "^0.4") (d #t) (k 0)))) (h "07q16g6fr1a5a6zr1dkgsy403c2mhadm7fh204wdyq2gfgn5q4hw")))

(define-public crate-iron-error-router-0.3.0 (c (n "iron-error-router") (v "0.3.0") (d (list (d (n "iron") (r "^0.5") (d #t) (k 0)))) (h "1mjb2vg35vfrp3gf3g3zzhlx31s4b55330vgz0wl913ybf06q33b")))

