(define-module (crates-io ir on iron_planet) #:use-module (crates-io))

(define-public crate-iron_planet-0.1.1 (c (n "iron_planet") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.68") (d #t) (k 0)) (d (n "serde_repr") (r "^0.1.7") (d #t) (k 0)) (d (n "sysinfo") (r "^0.21.1") (d #t) (k 0)) (d (n "ureq") (r "^2.3.0") (f (quote ("json"))) (d #t) (k 0)))) (h "0hilal9d700nj0r1rnavhc0n893h28v3qn9i6kh57ig6kackcgqp")))

(define-public crate-iron_planet-0.2.0 (c (n "iron_planet") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.68") (d #t) (k 0)) (d (n "serde_repr") (r "^0.1.7") (d #t) (k 0)) (d (n "sysinfo") (r "^0.21.1") (d #t) (k 0)) (d (n "ureq") (r "^2.3.0") (f (quote ("json"))) (d #t) (k 0)))) (h "0f0s14ppmns2y40vdzz93xjgcqakvcxmjfaj8j85ivk1ha7qgl47")))

(define-public crate-iron_planet-0.2.1 (c (n "iron_planet") (v "0.2.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.68") (d #t) (k 0)) (d (n "serde_repr") (r "^0.1.7") (d #t) (k 0)) (d (n "sysinfo") (r "^0.21.1") (d #t) (k 0)) (d (n "ureq") (r "^2.3.0") (f (quote ("json"))) (d #t) (k 0)))) (h "0qc589wv1ls5s4ap4wc4xzpxrzzs6d5m37x1x4k06jfwvx96914a")))

(define-public crate-iron_planet-0.2.2 (c (n "iron_planet") (v "0.2.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.68") (d #t) (k 0)) (d (n "serde_repr") (r "^0.1.7") (d #t) (k 0)) (d (n "sysinfo") (r "^0.21.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "ureq") (r "^2.3.0") (f (quote ("json"))) (d #t) (k 0)))) (h "0aabfg2siyk6b59b91lhgjprgmlrcmha1x2c976vf91vdq2qcsca")))

