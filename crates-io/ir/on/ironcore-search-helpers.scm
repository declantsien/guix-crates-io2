(define-module (crates-io ir on ironcore-search-helpers) #:use-module (crates-io))

(define-public crate-ironcore-search-helpers-0.1.0 (c (n "ironcore-search-helpers") (v "0.1.0") (d (list (d (n "itertools") (r "~0.8.2") (d #t) (k 0)) (d (n "lazy_static") (r "~1.4.0") (d #t) (k 0)) (d (n "rand") (r "~0.7.3") (d #t) (k 0)) (d (n "regex") (r "~1.3.4") (d #t) (k 0)) (d (n "sha2") (r "~0.8.1") (d #t) (k 0)) (d (n "unidecode") (r "= 0.3.0") (d #t) (k 0)) (d (n "voca_rs") (r "= 1.9.1") (d #t) (k 0)))) (h "0z1cjs84l4057dhkbj24qvyaabpabwzy4p2mksvwbdnr6s7p33bk")))

(define-public crate-ironcore-search-helpers-0.1.1 (c (n "ironcore-search-helpers") (v "0.1.1") (d (list (d (n "itertools") (r "~0.8.2") (d #t) (k 0)) (d (n "lazy_static") (r "~1.4.0") (d #t) (k 0)) (d (n "rand") (r "~0.7.3") (d #t) (k 0)) (d (n "sha2") (r "~0.8.1") (d #t) (k 0)) (d (n "unicode-segmentation") (r "= 1.6.0") (d #t) (k 0)) (d (n "unidecode") (r "= 0.3.0") (d #t) (k 0)))) (h "19asd945zlkv10cw68325afzk0ijisghxndrc5l1f4ra28pxq243")))

(define-public crate-ironcore-search-helpers-0.1.2 (c (n "ironcore-search-helpers") (v "0.1.2") (d (list (d (n "itertools") (r "~0.8.2") (d #t) (k 0)) (d (n "lazy_static") (r "~1.4.0") (d #t) (k 0)) (d (n "rand") (r "~0.7.3") (d #t) (k 0)) (d (n "sha2") (r "~0.8.1") (d #t) (k 0)) (d (n "unicode-segmentation") (r "= 1.6.0") (d #t) (k 0)) (d (n "unidecode") (r "= 0.3.0") (d #t) (k 0)))) (h "0igd71jlcwmcv7ycz4hq50752sk0mdj0n0sggjmskl0y47vidfxc")))

(define-public crate-ironcore-search-helpers-0.2.0 (c (n "ironcore-search-helpers") (v "0.2.0") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "sha2") (r "^0.9") (d #t) (k 0)) (d (n "unicode-segmentation") (r "=1.8.0") (d #t) (k 0)) (d (n "unidecode") (r "=0.3.0") (d #t) (k 0)))) (h "0hwh5y58vxwdnihmpqsv0wj6m1n7iw6vnrhcycg2xy9axqbz7r2y") (r "1.56.0")))

