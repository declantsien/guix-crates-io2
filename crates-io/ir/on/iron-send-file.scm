(define-module (crates-io ir on iron-send-file) #:use-module (crates-io))

(define-public crate-iron-send-file-0.1.0 (c (n "iron-send-file") (v "0.1.0") (d (list (d (n "conduit-mime-types") (r "^0.7.3") (d #t) (k 0)) (d (n "http-range") (r "^0.1.0") (d #t) (k 0)) (d (n "hyper") (r "^0.9.10") (d #t) (k 0)) (d (n "iron") (r "^0.4.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)))) (h "18sgqlknmkyrc8dvh6qrmcwswcp97r4h6y5rdqy4r3h0s2gmjpzf")))

(define-public crate-iron-send-file-0.1.1 (c (n "iron-send-file") (v "0.1.1") (d (list (d (n "conduit-mime-types") (r "^0.7.3") (d #t) (k 0)) (d (n "http-range") (r "^0.1.1") (d #t) (k 0)) (d (n "hyper") (r "^0.9.10") (d #t) (k 0)) (d (n "iron") (r "^0.4.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)))) (h "0jpwd7zn144xfm5g8h3l80bzdr956ik3a556v34pj67zqfw22gi0")))

