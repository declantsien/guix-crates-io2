(define-module (crates-io ir on iron-params) #:use-module (crates-io))

(define-public crate-iron-params-0.1.0 (c (n "iron-params") (v "0.1.0") (d (list (d (n "iron") (r "^0.4") (d #t) (k 0)) (d (n "url") (r "^1.1.1") (d #t) (k 0)))) (h "0fqply4kr2m063p64cxykc58x0wi6032xfr0cq8ghfpjl5s00afx")))

(define-public crate-iron-params-0.1.1 (c (n "iron-params") (v "0.1.1") (d (list (d (n "iron") (r "^0.4") (d #t) (k 0)) (d (n "url") (r "^1.1.1") (d #t) (k 0)))) (h "0a0wri3dwjvwdjbc59h6g80vm9g6i22iqf84ypr6fg2ycyjmc7s2")))

(define-public crate-iron-params-0.1.2 (c (n "iron-params") (v "0.1.2") (d (list (d (n "iron") (r "^0.4") (d #t) (k 0)) (d (n "url") (r "^1.1.1") (d #t) (k 0)))) (h "0pxmdnvxqd684m39391s80dpg9h9j5l4p0c576nqx4mzbnffdzij")))

(define-public crate-iron-params-0.1.3 (c (n "iron-params") (v "0.1.3") (d (list (d (n "iron") (r "^0.4") (d #t) (k 0)) (d (n "url") (r "^1.1.1") (d #t) (k 0)))) (h "0bw90lw51k3zwgcy15wwc1fs7kqcjrjx37lvwqwvv4svsnwv2v5x")))

