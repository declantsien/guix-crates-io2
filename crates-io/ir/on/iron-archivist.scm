(define-module (crates-io ir on iron-archivist) #:use-module (crates-io))

(define-public crate-iron-archivist-0.1.0-alpha.1 (c (n "iron-archivist") (v "0.1.0-alpha.1") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "iron") (r "^0.6") (d #t) (k 0)) (d (n "mime_guess") (r "^1.8") (d #t) (k 0)) (d (n "mount") (r "^0.4") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.1") (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)) (d (n "url") (r "^1.6") (d #t) (k 0)) (d (n "urlencoded") (r "^0.6") (d #t) (k 0)))) (h "19w69q4fyr0mzgdil9m711y5a7ggw068jfmhz7vv7x0b738ri7wi")))

