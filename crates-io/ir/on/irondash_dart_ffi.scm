(define-module (crates-io ir on irondash_dart_ffi) #:use-module (crates-io))

(define-public crate-irondash_dart_ffi-0.1.0 (c (n "irondash_dart_ffi") (v "0.1.0") (d (list (d (n "once_cell") (r "^1.16.0") (d #t) (k 0)))) (h "0v2r3f8f95gs9va6gigrf4zyyfa578np86l2brwcv9p5lcksw1dd")))

(define-public crate-irondash_dart_ffi-0.2.0 (c (n "irondash_dart_ffi") (v "0.2.0") (d (list (d (n "once_cell") (r "^1.16.0") (d #t) (k 0)))) (h "1p1xkgfi86q0phc4630dc3mj1s0lx9x2yislj7c29l1z86l3xvc8")))

