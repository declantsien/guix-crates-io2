(define-module (crates-io ir on iron-shapes) #:use-module (crates-io))

(define-public crate-iron-shapes-0.0.4 (c (n "iron-shapes") (v "0.0.4") (d (list (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "num-integer") (r "^0.1") (d #t) (k 0)) (d (n "num-rational") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1qbvsya8ax50ss7mqm7vhrnanwvwxfxm7zls6m61dk881gjvq3j7")))

(define-public crate-iron-shapes-0.0.6 (c (n "iron-shapes") (v "0.0.6") (d (list (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "num-integer") (r "^0.1") (d #t) (k 0)) (d (n "num-rational") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "194bfjjxdfzcc6j6f4yh2b3lgi0a9k8f6rnml84mdv7khpa1djkk")))

(define-public crate-iron-shapes-0.0.7 (c (n "iron-shapes") (v "0.0.7") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "num-integer") (r "^0.1") (d #t) (k 0)) (d (n "num-rational") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0xy6gans61314yq7q3v6hgi3sp8m59hyssgyswi4r5s5facphm8d")))

(define-public crate-iron-shapes-0.0.8 (c (n "iron-shapes") (v "0.0.8") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "num-integer") (r "^0.1") (d #t) (k 0)) (d (n "num-rational") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "096p145kkzw0dp1r9z1anc8p20kfzhyhcpgflqm5nkv07ynp1rm6")))

