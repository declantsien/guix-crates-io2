(define-module (crates-io ir on ironbird_engine_context) #:use-module (crates-io))

(define-public crate-ironbird_engine_context-0.1.0 (c (n "ironbird_engine_context") (v "0.1.0") (d (list (d (n "android_logger") (r "^0.11") (d #t) (t "cfg(target_os = \"android\")") (k 0)) (d (n "cocoa") (r "^0.24") (d #t) (t "cfg(any(target_os = \"macos\", target_os = \"ios\"))") (k 0)) (d (n "ironbird_jni_context") (r "^0.1.0") (d #t) (t "cfg(target_os = \"android\")") (k 0)) (d (n "jni") (r "^0.19") (d #t) (t "cfg(target_os = \"android\")") (k 0)) (d (n "log") (r "^0.4") (d #t) (t "cfg(target_os = \"android\")") (k 0)) (d (n "objc") (r "^0.2.7") (d #t) (t "cfg(any(target_os = \"macos\", target_os = \"ios\"))") (k 0)))) (h "1hly0hprr89fgpr1nvk84j68jrn5f6cx1sq459bv1w0wv9rj50s2")))

