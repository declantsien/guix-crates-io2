(define-module (crates-io ir on ironfish_zkp) #:use-module (crates-io))

(define-public crate-ironfish_zkp-0.1.0 (c (n "ironfish_zkp") (v "0.1.0") (d (list (d (n "bellman") (r "^0.13.1") (d #t) (k 0)) (d (n "blake2s_simd") (r "^1.0.0") (d #t) (k 0)) (d (n "bls12_381") (r "^0.7.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "ff") (r "^0.12.0") (d #t) (k 0)) (d (n "group") (r "^0.12.0") (d #t) (k 0)) (d (n "jubjub") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "zcash_primitives") (r "^0.7.0") (d #t) (k 0)) (d (n "zcash_proofs") (r "^0.7.1") (d #t) (k 0)))) (h "1abh4mb3pah8z97c758d8zfkgdaa7wjzdb6fx5rn3x8v20ikmcz2")))

