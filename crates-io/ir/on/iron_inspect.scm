(define-module (crates-io ir on iron_inspect) #:use-module (crates-io))

(define-public crate-iron_inspect-0.1.0 (c (n "iron_inspect") (v "0.1.0") (d (list (d (n "iron") (r "^0.5.1") (d #t) (k 0)))) (h "10sqlbnvfns9mn5kryf57d73g80d7w5n7yb02dlkk3mkp4vk1jfh")))

(define-public crate-iron_inspect-0.1.1 (c (n "iron_inspect") (v "0.1.1") (d (list (d (n "iron") (r ">= 0.5.0, < 0.7.0") (d #t) (k 0)))) (h "070rf07hkrcach2papzfprpxbgx2m7l2bc9dqhkb0rgbdra5vfwp") (y #t)))

(define-public crate-iron_inspect-0.1.2 (c (n "iron_inspect") (v "0.1.2") (d (list (d (n "iron") (r ">= 0.5.0, < 0.6.0") (d #t) (k 0)))) (h "0jmdccbzm57cg0f3wwj49sykz5644xd1y7xqyif40dys3yy9havn")))

(define-public crate-iron_inspect-0.2.0 (c (n "iron_inspect") (v "0.2.0") (d (list (d (n "iron") (r ">= 0.5.0, < 0.7.0") (d #t) (k 0)))) (h "18agr16n4fh3jb1fw5m96wkvy7iwvh7w053jy2bygq9pxzbpjh0q")))

(define-public crate-iron_inspect-0.2.1 (c (n "iron_inspect") (v "0.2.1") (d (list (d (n "iron") (r ">= 0.5.0, < 0.7.0") (d #t) (k 0)))) (h "08cj2acs1hg90157v1hwxrlx94gyn06phh2vvwpbxp5qf3gymgdq")))

