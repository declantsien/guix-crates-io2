(define-module (crates-io ir on ironmq-client) #:use-module (crates-io))

(define-public crate-ironmq-client-0.1.0 (c (n "ironmq-client") (v "0.1.0") (d (list (d (n "bytes") (r "^0.6") (d #t) (k 0)) (d (n "env_logger") (r "^0.7") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "ironmq-codec") (r "^0.0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^0.3") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-util") (r "^0.5") (f (quote ("codec"))) (d #t) (k 0)))) (h "1m6pgmzjg3cm6f9p1b1yfj4fc3ljhhbx6ba6gpnlxknx8rdvdg2d")))

(define-public crate-ironmq-client-0.1.1 (c (n "ironmq-client") (v "0.1.1") (d (list (d (n "bytes") (r "^0.6") (d #t) (k 0)) (d (n "env_logger") (r "^0.7") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "ironmq-codec") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^0.3") (f (quote ("macros" "net" "rt" "rt-multi-thread" "stream" "sync"))) (d #t) (k 0)) (d (n "tokio-util") (r "^0.5") (f (quote ("codec"))) (d #t) (k 0)))) (h "1l6mqnd0ngb703wp3rqxnvr67gzymxxiikknhp32rndh3w6fjm58")))

