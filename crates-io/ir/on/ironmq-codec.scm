(define-module (crates-io ir on ironmq-codec) #:use-module (crates-io))

(define-public crate-ironmq-codec-0.0.1 (c (n "ironmq-codec") (v "0.0.1") (d (list (d (n "bytes") (r "^0.6") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "tokio-util") (r "^0.5") (f (quote ("codec"))) (d #t) (k 0)))) (h "1ibqp9pzmklw0cqxyndi751i807ifza0w1ws2cnvva5l30w3sxvg")))

(define-public crate-ironmq-codec-0.1.0 (c (n "ironmq-codec") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "bytes") (r "^0.6") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "tokio-util") (r "^0.5") (f (quote ("codec"))) (d #t) (k 0)))) (h "186sm6w7skn9xa94f8a158vsbs9ikwf670js58h38rbrgb8282qf")))

(define-public crate-ironmq-codec-0.1.1 (c (n "ironmq-codec") (v "0.1.1") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "bytes") (r "^0.6") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "tokio-util") (r "^0.5") (f (quote ("codec"))) (d #t) (k 0)))) (h "00xpawjxwwahjkky57ym752z218h19lwwq91fdp2hw769az78mw1")))

