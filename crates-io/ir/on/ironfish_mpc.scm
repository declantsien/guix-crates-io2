(define-module (crates-io ir on ironfish_mpc) #:use-module (crates-io))

(define-public crate-ironfish_mpc-1.0.1 (c (n "ironfish_mpc") (v "1.0.1") (d (list (d (n "regex") (r "^0.1.19") (d #t) (k 0)))) (h "0y2rd8n5vmzrspr137zdzxybf6d3l9dyaiwi7ra6a0as75r2idrh")))

(define-public crate-ironfish_mpc-1.1.1 (c (n "ironfish_mpc") (v "1.1.1") (d (list (d (n "regex") (r "^0.1.19") (d #t) (k 0)))) (h "07vapp0dfbp12bhb2z0ns747n116z1vycr2qy8pl56w71wxj8xp8")))

