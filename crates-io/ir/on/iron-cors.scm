(define-module (crates-io ir on iron-cors) #:use-module (crates-io))

(define-public crate-iron-cors-0.1.0 (c (n "iron-cors") (v "0.1.0") (d (list (d (n "iron") (r "^0.4") (d #t) (k 0)) (d (n "iron-test") (r "^0.4.0") (d #t) (k 2)) (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "1mc750rzz7m79ic4iklfk2ck4v16w0rdi64k39fizaic4r85d88s")))

(define-public crate-iron-cors-0.2.0 (c (n "iron-cors") (v "0.2.0") (d (list (d (n "iron") (r "^0.4") (d #t) (k 0)) (d (n "iron-test") (r "^0.4.0") (d #t) (k 2)) (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "0m4pij165if6rbwsgdr6w8vykkkdmi9lkji91n01sq2zb8kwq42d")))

(define-public crate-iron-cors-0.3.0 (c (n "iron-cors") (v "0.3.0") (d (list (d (n "iron") (r "^0.4") (d #t) (k 0)) (d (n "iron-test") (r "^0.4.0") (d #t) (k 2)) (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "1m4ka2a0r2np1c3jfx4svpqniqiw5rlq5xv35hxkiiipkp8sarn2")))

(define-public crate-iron-cors-0.4.0 (c (n "iron-cors") (v "0.4.0") (d (list (d (n "iron") (r "^0.4") (d #t) (k 0)) (d (n "iron-test") (r "^0.4.0") (d #t) (k 2)) (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "0lxab9zwv1h5alk13drv9hjy3gskfyjkr5wlh1cnbs6ikb735fmr")))

(define-public crate-iron-cors-0.5.0 (c (n "iron-cors") (v "0.5.0") (d (list (d (n "iron") (r "^0.5") (d #t) (k 0)) (d (n "iron-test") (r "^0.5.0") (d #t) (k 2)) (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "14c6hz795axkasgd25kqx11crkgxn8jwh4h96qxfbdj8w21w0a7d")))

(define-public crate-iron-cors-0.5.1 (c (n "iron-cors") (v "0.5.1") (d (list (d (n "iron") (r "^0.5") (d #t) (k 0)) (d (n "iron-test") (r "^0.5.0") (d #t) (k 2)) (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "124b4218hdjiv7ax8y05ckxjimjdx9yjpfr3klk8fs38kpy738pq")))

(define-public crate-iron-cors-0.6.0-rc.1 (c (n "iron-cors") (v "0.6.0-rc.1") (d (list (d (n "iron") (r "^0.5") (d #t) (k 0)) (d (n "iron-test") (r "^0.5.0") (d #t) (k 2)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "unicase") (r "^1.4.0") (d #t) (k 2)))) (h "1zcwwc49f42fn8h3x349ddvxqjf44vwfnpsw73ggzy8dhz0jqvkk")))

(define-public crate-iron-cors-0.7.0 (c (n "iron-cors") (v "0.7.0") (d (list (d (n "iron") (r "^0.6") (d #t) (k 0)) (d (n "iron-test") (r "^0.6.0") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicase") (r "^1.4.0") (d #t) (k 2)))) (h "1wcig80wpi0jbfwai7mclyzqv3fbirshpjllqxm1752nfvwxs1c0")))

(define-public crate-iron-cors-0.7.1 (c (n "iron-cors") (v "0.7.1") (d (list (d (n "iron") (r "^0.6") (d #t) (k 0)) (d (n "iron-test") (r "^0.6.0") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicase") (r "^1.4.0") (d #t) (k 2)))) (h "0cz2xiz0m4kal36lmly1nxn6j87kjqrsg6dwpsbx7ybnxqn008r4")))

(define-public crate-iron-cors-0.8.0 (c (n "iron-cors") (v "0.8.0") (d (list (d (n "iron") (r "^0.6") (d #t) (k 0)) (d (n "iron-test") (r "^0.6.0") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicase") (r "^1.4.0") (d #t) (k 2)))) (h "0w4vikw1har4k8n9vx0rxhxkysffy0n80gj87i24xwf7as42pc14")))

