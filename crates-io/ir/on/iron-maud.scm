(define-module (crates-io ir on iron-maud) #:use-module (crates-io))

(define-public crate-iron-maud-0.1.0 (c (n "iron-maud") (v "0.1.0") (d (list (d (n "iron") (r "^0.4.0") (d #t) (k 0)) (d (n "maud") (r "^0.10.0") (d #t) (k 0)))) (h "0sd4gy6j9zdi986rgijf1h4mk3wji5k3fckv98x8g5hbrl16blqq") (y #t)))

