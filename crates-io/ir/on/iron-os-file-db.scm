(define-module (crates-io ir on iron-os-file-db) #:use-module (crates-io))

(define-public crate-iron-os-file-db-0.2.0 (c (n "iron-os-file-db") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("fs"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("fs" "macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1f5j9s6xna1zksf9km7899aiksv1qlyh0i63pcbkql6dnci5rqwi") (f (quote (("async" "tokio"))))))

