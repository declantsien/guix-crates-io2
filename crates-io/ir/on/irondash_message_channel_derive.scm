(define-module (crates-io ir on irondash_message_channel_derive) #:use-module (crates-io))

(define-public crate-irondash_message_channel_derive-0.1.1 (c (n "irondash_message_channel_derive") (v "0.1.1") (d (list (d (n "proc-macro-error") (r "^1.0") (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1dxhikwblz61ls679r1p6l65hqr86sz6v4qc5l4nmsh1lvf41h1w")))

