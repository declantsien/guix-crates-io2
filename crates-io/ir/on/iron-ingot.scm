(define-module (crates-io ir on iron-ingot) #:use-module (crates-io))

(define-public crate-iron-ingot-0.1.0 (c (n "iron-ingot") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1h95ncis611h6ps1s8avbg6rzfzw65xzaqngvv79wi9i6s9lp3gx")))

(define-public crate-iron-ingot-0.2.0 (c (n "iron-ingot") (v "0.2.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0b8cmiqw519sfzhgzm359qlbpln2mdb5qn5cgchbibnn14jqyny2")))

(define-public crate-iron-ingot-0.3.0 (c (n "iron-ingot") (v "0.3.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0zcnii0dp80s0gbffihgwrbfhi1x38161kx929rcczlyzm17d1cv")))

(define-public crate-iron-ingot-0.4.0 (c (n "iron-ingot") (v "0.4.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1dq4b5g75ywga1mcaygkk10vji5mxds6v3g0akrgn140p7dw8mi7")))

(define-public crate-iron-ingot-0.4.1 (c (n "iron-ingot") (v "0.4.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0qvsl54m3czllwrzp8wrsarqndkfip0khbclf2bspz1w4dmlvn2p")))

(define-public crate-iron-ingot-0.5.0 (c (n "iron-ingot") (v "0.5.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1v8kpb9fdighpgslfscz6dl9yahlw1akpqjm1ji31srpz5av371w")))

(define-public crate-iron-ingot-0.6.0 (c (n "iron-ingot") (v "0.6.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0yxs10mmq7fhahpkcbqyvg4hdxl9l10qrz9rli5v84cfk4aqcyrv")))

(define-public crate-iron-ingot-0.7.0 (c (n "iron-ingot") (v "0.7.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0sb1cg8yyrxyfirr4calym15q0h94c6i35czbjqa1wbpia6w843r")))

(define-public crate-iron-ingot-0.7.1 (c (n "iron-ingot") (v "0.7.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "10iqysa6rhm1yrdmr4fkg7853mjfp1d0bmnfyz923p7k3wlv8fzi")))

