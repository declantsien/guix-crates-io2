(define-module (crates-io ir on iron-drain) #:use-module (crates-io))

(define-public crate-iron-drain-0.1.0 (c (n "iron-drain") (v "0.1.0") (d (list (d (n "iron") (r "^0.3.0") (d #t) (k 0)))) (h "0yklhm2hbh2k8bvh42j8bvba2p2pb1ic4fp546as763q44lc1nch")))

(define-public crate-iron-drain-0.1.1 (c (n "iron-drain") (v "0.1.1") (d (list (d (n "iron") (r "^0.3.0") (d #t) (k 0)))) (h "0vyhd6q7i0j1sv3sz8j2jajga1dj7m4sxcnwv1nzi22w5vh08nsh")))

(define-public crate-iron-drain-0.1.2 (c (n "iron-drain") (v "0.1.2") (d (list (d (n "iron") (r "^0.3.0") (d #t) (k 0)))) (h "0lg2kz334bszjgdycgj78dy3bbm8ql5kjpqjsajnj71c5ykxbgf1")))

