(define-module (crates-io ir on iron_dice) #:use-module (crates-io))

(define-public crate-iron_dice-0.1.0 (c (n "iron_dice") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1l2rp2jfdcscn11311sv24s1c402m00r8bcmmvfz2g03q5hq7vzb")))

(define-public crate-iron_dice-0.1.1 (c (n "iron_dice") (v "0.1.1") (d (list (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "00mksv0vhi6wffirivz5gl4p17agh5a4930zm129h3q5psjxaqam")))

(define-public crate-iron_dice-0.1.2 (c (n "iron_dice") (v "0.1.2") (d (list (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0z30asj9j7771snxs2sm82rbvv9v27rm6rlixa6pyglxmb82j614")))

(define-public crate-iron_dice-0.1.3 (c (n "iron_dice") (v "0.1.3") (d (list (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "187dxdhj0bs0k12md8c6wclgrpnhk4ddznbfcwincbig4hjyf9vg")))

