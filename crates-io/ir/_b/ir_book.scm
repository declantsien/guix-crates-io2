(define-module (crates-io ir _b ir_book) #:use-module (crates-io))

(define-public crate-ir_book-0.1.0 (c (n "ir_book") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.62") (d #t) (k 0)) (d (n "clap") (r "^3.2.17") (f (quote ("derive"))) (d #t) (k 0)) (d (n "image") (r "^0.24.3") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.0") (d #t) (k 0)) (d (n "klask") (r "^1.0.0") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "printpdf") (r "^0.5.3") (f (quote ("embedded_images"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "default-tls"))) (d #t) (k 0)))) (h "1dfpxfwicg2klc34flvi751f6cs19hmsygh2g39mkiwnw1qr779j")))

