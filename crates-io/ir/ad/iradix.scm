(define-module (crates-io ir ad iradix) #:use-module (crates-io))

(define-public crate-iradix-0.0.0 (c (n "iradix") (v "0.0.0") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "0h93jw1q7rvh5w6smcgb4wj1mi329jdy2yc4xd2nihswqyi7ik3z") (f (quote (("std") ("default")))) (y #t) (r "1.73")))

(define-public crate-iradix-0.0.1 (c (n "iradix") (v "0.0.1") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "0b7yqmvkaiysyl9l3r7g9bbz8i3bfj797avxm11qy2z8kgnw718s") (f (quote (("default")))) (s 2) (e (quote (("std" "serde?/default")))) (r "1.73")))

