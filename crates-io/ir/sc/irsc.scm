(define-module (crates-io ir sc irsc) #:use-module (crates-io))

(define-public crate-irsc-0.0.1 (c (n "irsc") (v "0.0.1") (h "049fl8rndh4wwcf27qpcyhsdsdwskzd4krg71kzj9042kw84jrsd")))

(define-public crate-irsc-0.0.2 (c (n "irsc") (v "0.0.2") (h "0k4l8l4zrknfdqnvf3ki64xq158bxp397a5n3jw4yww1q6np6jz1")))

(define-public crate-irsc-0.0.3 (c (n "irsc") (v "0.0.3") (h "0lwsimwmr73z7l9yz2h9327cq4ycnc7yf7gqdc48wv1rfh96p25l")))

(define-public crate-irsc-0.1.0 (c (n "irsc") (v "0.1.0") (d (list (d (n "env_logger") (r "*") (d #t) (k 2)) (d (n "log") (r "*") (d #t) (k 0)) (d (n "openssl") (r "*") (o #t) (d #t) (k 0)) (d (n "regex") (r "*") (d #t) (k 0)) (d (n "regex_macros") (r "*") (d #t) (k 0)))) (h "0yiixwxzvrdkq0s09lacmnmc0m0lsflypapbh98lc55jgkvpys3x") (f (quote (("ssl" "openssl"))))))

(define-public crate-irsc-0.1.1 (c (n "irsc") (v "0.1.1") (d (list (d (n "env_logger") (r "*") (d #t) (k 2)) (d (n "log") (r "*") (d #t) (k 0)) (d (n "openssl") (r "*") (o #t) (d #t) (k 0)) (d (n "regex") (r "*") (d #t) (k 0)) (d (n "regex_macros") (r "*") (d #t) (k 0)))) (h "1cvkyiydq27nhsil446gw3cninwhwrdvmdhv2r1ilwwx8pllcv9s") (f (quote (("ssl" "openssl"))))))

(define-public crate-irsc-0.2.0 (c (n "irsc") (v "0.2.0") (d (list (d (n "carboxyl") (r "^0.1") (d #t) (k 0)) (d (n "clippy") (r "*") (o #t) (d #t) (k 0)) (d (n "env_logger") (r "*") (d #t) (k 2)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "openssl") (r "^0.6") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "regex_macros") (r "^0.1") (d #t) (k 0)))) (h "19l83gfdsnnlj844x1hc203lpb5vdccdy8x9l93vc69djamwl499") (f (quote (("lints" "clippy")))) (y #t)))

