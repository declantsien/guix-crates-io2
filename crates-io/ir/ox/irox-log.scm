(define-module (crates-io ir ox irox-log) #:use-module (crates-io))

(define-public crate-irox-log-0.1.0 (c (n "irox-log") (v "0.1.0") (d (list (d (n "irox-time") (r "^0") (d #t) (k 0)) (d (n "irox-tools") (r "^0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "16r7pqp9ry49lkiqcjyipls16738di50gb627ffqdj0ljlphqv1p")))

(define-public crate-irox-log-0.1.1 (c (n "irox-log") (v "0.1.1") (d (list (d (n "irox-time") (r "^0") (d #t) (k 0)) (d (n "irox-tools") (r "^0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0r3inrlfx23wfq58038zgdayj41k52mp875hybz5bzf5d5q5d5yf")))

(define-public crate-irox-log-0.1.2 (c (n "irox-log") (v "0.1.2") (d (list (d (n "irox-time") (r "^0") (d #t) (k 0)) (d (n "irox-tools") (r "^0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1hrvpabd7h800iqrw9rqj89avf5baqp9x56s0mp14lb2kj3rmgcr")))

(define-public crate-irox-log-0.1.3 (c (n "irox-log") (v "0.1.3") (d (list (d (n "irox-time") (r "^0") (d #t) (k 0)) (d (n "irox-tools") (r "^0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0qycs001hkqcxw4rbi8xljachbvx35lrrh55j5lxg97qyvpcjlqz")))

(define-public crate-irox-log-0.2.0 (c (n "irox-log") (v "0.2.0") (d (list (d (n "irox-time") (r "^0") (d #t) (k 0)) (d (n "irox-tools") (r "^0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0wnn449l1ja2x16zwm8pgmphyj4biaca3jjra4bd6cmr7snvp8am")))

