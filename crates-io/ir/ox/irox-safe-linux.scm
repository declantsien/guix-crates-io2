(define-module (crates-io ir ox irox-safe-linux) #:use-module (crates-io))

(define-public crate-irox-safe-linux-0.1.0 (c (n "irox-safe-linux") (v "0.1.0") (d (list (d (n "irox-enums") (r "^0") (d #t) (k 0)))) (h "0jrfywfkf44xx1fzf7w3qlq6i1ldlm39wq4cpqyzsg7ykxm1bngd")))

(define-public crate-irox-safe-linux-0.1.1 (c (n "irox-safe-linux") (v "0.1.1") (d (list (d (n "irox-enums") (r "^0") (d #t) (k 0)))) (h "0di99zs12ninmd5kv9fl5kqmgvvcs3dml978ap7ii02bhkijs5b0")))

