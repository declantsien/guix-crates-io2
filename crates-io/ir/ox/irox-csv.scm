(define-module (crates-io ir ox irox-csv) #:use-module (crates-io))

(define-public crate-irox-csv-0.1.0 (c (n "irox-csv") (v "0.1.0") (h "1dzkr3qvarhjklz69rjyhihcqlli1ya2hkj9w27g0yxa84p097gv")))

(define-public crate-irox-csv-0.2.0 (c (n "irox-csv") (v "0.2.0") (h "12l0h2xyq03kpxxcqrb0d87b6q5rk65ls5fjjh51rfqzs2xsicm9")))

(define-public crate-irox-csv-0.2.1 (c (n "irox-csv") (v "0.2.1") (h "0ygpvyr5wfx716prh8jlkd77pwk97h7g93chiqqnpj9w4zaszpqh")))

(define-public crate-irox-csv-0.3.0 (c (n "irox-csv") (v "0.3.0") (d (list (d (n "irox-tools") (r "^0") (d #t) (k 0)))) (h "1b5wyv015r4hzfgyhg6ifqaspjxb4nm2cdc11zc4n1wlyr7qma84")))

(define-public crate-irox-csv-0.4.0 (c (n "irox-csv") (v "0.4.0") (d (list (d (n "irox-tools") (r "^0") (d #t) (k 0)))) (h "1wdpiawz65wsp8l3fffs29xnv4p1z85xn151hhzdj4qj71w045ya")))

(define-public crate-irox-csv-0.4.1 (c (n "irox-csv") (v "0.4.1") (d (list (d (n "irox-tools") (r "^0") (d #t) (k 0)))) (h "1bpvg7bsgjpgd3mwqrgb3i2giyls1v973dmbx9iklx76iy95r7vw")))

(define-public crate-irox-csv-0.5.0 (c (n "irox-csv") (v "0.5.0") (d (list (d (n "irox-tools") (r "^0") (d #t) (k 0)))) (h "0d527q12m0s5nfgk531l6hprwp0r4b3qak8d90s88n63igd5n9rz")))

(define-public crate-irox-csv-0.5.1 (c (n "irox-csv") (v "0.5.1") (d (list (d (n "irox-tools") (r "^0") (f (quote ("std"))) (d #t) (k 0)))) (h "0ich0070962simq9vmzwbah1ggyw9kvy33rzhy8j4kzmcljrlnzc")))

