(define-module (crates-io ir ox irox-build-rs) #:use-module (crates-io))

(define-public crate-irox-build-rs-0.1.0 (c (n "irox-build-rs") (v "0.1.0") (d (list (d (n "git2") (r "^0.18.1") (f (quote ("vendored-openssl" "vendored-libgit2"))) (o #t) (d #t) (k 0)) (d (n "irox-git-tools") (r "^0") (o #t) (d #t) (k 0)) (d (n "irox-time") (r "^0") (o #t) (d #t) (k 0)))) (h "1zk3h93ci7ngl1ch7jm5xh2s5and46xd7dfhzi5i77d8mv9rq8wq") (s 2) (e (quote (("git" "dep:git2" "dep:irox-git-tools" "dep:irox-time"))))))

(define-public crate-irox-build-rs-0.2.0 (c (n "irox-build-rs") (v "0.2.0") (d (list (d (n "git2") (r "^0.18.1") (f (quote ("vendored-openssl" "vendored-libgit2"))) (o #t) (d #t) (k 0)) (d (n "irox-git-tools") (r "^0") (o #t) (d #t) (k 0)) (d (n "irox-time") (r "^0") (o #t) (d #t) (k 0)))) (h "0cxp649l2443b3v0gh8f98ciyvnjhrghcvgsz77bssibp4wgxgbm") (s 2) (e (quote (("git" "dep:git2" "dep:irox-git-tools" "dep:irox-time"))))))

(define-public crate-irox-build-rs-0.2.1 (c (n "irox-build-rs") (v "0.2.1") (d (list (d (n "git2") (r "^0.18.2") (f (quote ("vendored-openssl" "vendored-libgit2"))) (o #t) (d #t) (k 0)) (d (n "irox-git-tools") (r "^0") (o #t) (d #t) (k 0)) (d (n "irox-time") (r "^0") (o #t) (d #t) (k 0)))) (h "1ydxdvys4p7qlbx76nykmk5plwww7f0wpq243svrgbaphfys2fpc") (s 2) (e (quote (("git" "dep:git2" "dep:irox-git-tools" "dep:irox-time"))))))

