(define-module (crates-io ir ox irox-enums_derive) #:use-module (crates-io))

(define-public crate-irox-enums_derive-0.1.0 (c (n "irox-enums_derive") (v "0.1.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "18qnq5klcpcqlzdb4d2yp3j5kgv6ykx9z3j2xzhiylwpbrcljc61")))

(define-public crate-irox-enums_derive-0.2.0 (c (n "irox-enums_derive") (v "0.2.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "028g0nl65jn6hlzn7wv7x42w01pl98my944ilr9cqmp9dvwjdw0r")))

(define-public crate-irox-enums_derive-0.2.1 (c (n "irox-enums_derive") (v "0.2.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "12c450p1883gcn47kdxycicy6n2imk93hp8sdrdri2sy5j8czxsq")))

(define-public crate-irox-enums_derive-0.2.2 (c (n "irox-enums_derive") (v "0.2.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0dz3qn598bp051j2nfhyg5yfd0m5n9sj3a05959lkijlkhsclzgg")))

