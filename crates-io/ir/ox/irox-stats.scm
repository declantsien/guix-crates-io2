(define-module (crates-io ir ox irox-stats) #:use-module (crates-io))

(define-public crate-irox-stats-0.1.0 (c (n "irox-stats") (v "0.1.0") (h "1wcq8n01aijwrapi0qryl1mb48igvbryywlrqs3vjkpks2mb3p8p")))

(define-public crate-irox-stats-0.2.0 (c (n "irox-stats") (v "0.2.0") (h "0laz38jx445wdibd4vi3yvn8lfgw7ps2advcwlacmv4895sz8axh")))

(define-public crate-irox-stats-0.2.1 (c (n "irox-stats") (v "0.2.1") (h "1zqsiq9gld28vj2l6r0d4ahkg2dxqk4d179amhqr78ag5y12rv4n")))

(define-public crate-irox-stats-0.2.2 (c (n "irox-stats") (v "0.2.2") (h "1x1b5aq205p6yjswzg1d1qr0sss0jjqirmdc4pbj77smgfbm48gp")))

(define-public crate-irox-stats-0.2.3 (c (n "irox-stats") (v "0.2.3") (h "0b0as3amyajkj6rxc0as3rgszbmbhmjvbrzirb0dqnj5y4q69bpm")))

(define-public crate-irox-stats-0.2.4 (c (n "irox-stats") (v "0.2.4") (d (list (d (n "irox-tools") (r "^0") (k 0)) (d (n "irox-units") (r "^0") (d #t) (k 2)))) (h "1j61fy64v4iykk800mxvvq6zjp2drmibzh2qlfs7l9r7rz19xf75")))

(define-public crate-irox-stats-0.2.5 (c (n "irox-stats") (v "0.2.5") (d (list (d (n "irox-tools") (r "^0") (k 0)) (d (n "irox-units") (r "^0") (d #t) (k 2)))) (h "16cq9paxcnvvrlyrppf0nprh9ykl9dpgzpb5spf128x661flbxdz")))

