(define-module (crates-io ir ox irox-networking) #:use-module (crates-io))

(define-public crate-irox-networking-0.1.0 (c (n "irox-networking") (v "0.1.0") (d (list (d (n "log") (r "^0") (d #t) (k 0)) (d (n "serde") (r "^1, <1.0.172") (f (quote ("derive"))) (d #t) (k 0)))) (h "1iy6p3fkmw3h6v6ahs1rj85h40m7kglma35jmnpys3aga2a6bzl7")))

(define-public crate-irox-networking-0.2.0 (c (n "irox-networking") (v "0.2.0") (d (list (d (n "log") (r "^0") (d #t) (k 0)) (d (n "serde") (r "^1, <1.0.172") (f (quote ("derive"))) (d #t) (k 0)))) (h "1v0zb21bhrgda9icqqpn3ihpcyjl8rng2c3cgrsmi4ky7v9lh66r")))

(define-public crate-irox-networking-0.2.1 (c (n "irox-networking") (v "0.2.1") (d (list (d (n "log") (r "=0.4.20") (d #t) (k 0)) (d (n "serde") (r "=1.0.188") (f (quote ("derive"))) (d #t) (k 0)))) (h "0glhhkjzdpahphsi7dnwfly9p569iyiy4zd1qdsn2pkrnfjn9s57")))

(define-public crate-irox-networking-0.3.0 (c (n "irox-networking") (v "0.3.0") (d (list (d (n "irox-tools") (r "^0") (d #t) (k 0)) (d (n "log") (r "=0.4.20") (d #t) (k 0)) (d (n "serde") (r "=1.0.190") (f (quote ("derive"))) (d #t) (k 0)))) (h "09iz25k1r7m6crqyvqy7q6w9w428a7zmfb0f0gblkjxxsrwya4j2")))

(define-public crate-irox-networking-0.3.1 (c (n "irox-networking") (v "0.3.1") (d (list (d (n "irox-tools") (r "^0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)))) (h "097zmsh6nvsx3zn1sg2q82kv08bf53c9wazgik4dx8jgbnjp8giz")))

(define-public crate-irox-networking-0.3.2 (c (n "irox-networking") (v "0.3.2") (d (list (d (n "irox-tools") (r "^0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1jmmjq9xz198dq38blff3vi4ri4p1fyjd3gprgna1k9ia3vscrr2") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-irox-networking-0.4.0 (c (n "irox-networking") (v "0.4.0") (d (list (d (n "irox-tools") (r "^0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.194") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1ilzriq2z50jidn86phm4l2qk0nnz7ij77mv50454wjfbvgc7azp") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-irox-networking-0.4.1 (c (n "irox-networking") (v "0.4.1") (d (list (d (n "irox-enums") (r "^0") (d #t) (k 0)) (d (n "irox-log") (r "^0") (d #t) (k 2)) (d (n "irox-structs") (r "^0") (d #t) (k 0)) (d (n "irox-tools") (r "^0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.194") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.194") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0hi5f58hkxsn4kjch8lq1xp4rmvw6mjpv30dblivz0bpsg1r85yf") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-irox-networking-0.4.2 (c (n "irox-networking") (v "0.4.2") (d (list (d (n "irox-enums") (r "^0") (d #t) (k 0)) (d (n "irox-log") (r "^0") (d #t) (k 2)) (d (n "irox-structs") (r "^0") (d #t) (k 0)) (d (n "irox-tools") (r "^0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1xzp1lilj34z04s3bj2fc6zbdp0kbwvf91c9zk54dfk7py157f49") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-irox-networking-0.4.3 (c (n "irox-networking") (v "0.4.3") (d (list (d (n "irox-enums") (r "^0") (d #t) (k 0)) (d (n "irox-log") (r "^0") (d #t) (k 2)) (d (n "irox-structs") (r "^0") (d #t) (k 0)) (d (n "irox-tools") (r "^0") (f (quote ("std"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.201") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.201") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "01azgff9jl2cnhyl4q0wiqbyx1sfwkxhdg3z3b7liwrqznyqjy2s") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

