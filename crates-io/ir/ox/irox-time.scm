(define-module (crates-io ir ox irox-time) #:use-module (crates-io))

(define-public crate-irox-time-0.1.0 (c (n "irox-time") (v "0.1.0") (d (list (d (n "irox-enums") (r "^0") (d #t) (k 0)) (d (n "irox-tools") (r "^0") (d #t) (k 0)) (d (n "irox-units") (r "^0") (d #t) (k 0)))) (h "1g331x02nmd459dsxf5lma5l8gmmxizx4hx8rhvkk6q759ygw5ij")))

(define-public crate-irox-time-0.2.0 (c (n "irox-time") (v "0.2.0") (d (list (d (n "irox-enums") (r "^0") (d #t) (k 0)) (d (n "irox-tools") (r "^0") (d #t) (k 0)) (d (n "irox-units") (r "^0") (d #t) (k 0)))) (h "133kmbnfx3h3zf232sjy32wn33fnvz1c56rpfbz1iazy7dwfb6nb")))

(define-public crate-irox-time-0.3.0 (c (n "irox-time") (v "0.3.0") (d (list (d (n "irox-enums") (r "^0") (d #t) (k 0)) (d (n "irox-tools") (r "^0") (d #t) (k 0)) (d (n "irox-units") (r "^0") (d #t) (k 0)))) (h "1y9mzxgb0yg6azldvqxzn2rcfj1kw7kbs839c3w1q8ykp7z4qq0a")))

(define-public crate-irox-time-0.3.1 (c (n "irox-time") (v "0.3.1") (d (list (d (n "irox-enums") (r "^0") (d #t) (k 0)) (d (n "irox-tools") (r "^0") (d #t) (k 0)) (d (n "irox-units") (r "^0") (d #t) (k 0)))) (h "16wf9ca5cyg717dynjg5lri4h7qv98yfjdfa6fph5irswjyzdihm")))

(define-public crate-irox-time-0.3.2 (c (n "irox-time") (v "0.3.2") (d (list (d (n "irox-enums") (r "^0") (d #t) (k 0)) (d (n "irox-tools") (r "^0") (d #t) (k 0)) (d (n "irox-units") (r "^0") (d #t) (k 0)))) (h "0bcdyydbpvssg398kli7fw0mm295wckiczwv0rdvqrrmm30qbxw3")))

(define-public crate-irox-time-0.3.3 (c (n "irox-time") (v "0.3.3") (d (list (d (n "irox-enums") (r "^0") (d #t) (k 0)) (d (n "irox-tools") (r "^0") (d #t) (k 0)) (d (n "irox-units") (r "^0") (d #t) (k 0)))) (h "1y006lnhdsxl58l1c9l08877i656fr079zap1gj1ik9hw1fkswwn")))

(define-public crate-irox-time-0.3.4 (c (n "irox-time") (v "0.3.4") (d (list (d (n "irox-enums") (r "^0") (d #t) (k 0)) (d (n "irox-tools") (r "^0") (d #t) (k 0)) (d (n "irox-units") (r "^0") (d #t) (k 0)))) (h "02j25pm7x7hpyb81c3hxgsjr52dd2k4sf5dq9jk4k0qlbb6gzdrz")))

(define-public crate-irox-time-0.3.5 (c (n "irox-time") (v "0.3.5") (d (list (d (n "irox-enums") (r "^0") (d #t) (k 0)) (d (n "irox-tools") (r "^0") (d #t) (k 0)) (d (n "irox-units") (r "^0") (d #t) (k 0)))) (h "13m8iknw8xqznqx682x1p20hz2fr0n8j6203yxwa2w6gydp1jz4w")))

(define-public crate-irox-time-0.4.0 (c (n "irox-time") (v "0.4.0") (d (list (d (n "irox-enums") (r "^0") (d #t) (k 0)) (d (n "irox-tools") (r "^0") (d #t) (k 0)) (d (n "irox-units") (r "^0") (d #t) (k 0)))) (h "18q6g61q9psvrz367jpcqrljqbbw4g2w65fgscg9y139ymavqa5g")))

(define-public crate-irox-time-0.4.1 (c (n "irox-time") (v "0.4.1") (d (list (d (n "irox-enums") (r "^0") (d #t) (k 0)) (d (n "irox-tools") (r "^0") (d #t) (k 0)) (d (n "irox-units") (r "^0") (d #t) (k 0)))) (h "1vd18javna07j8h575rcqvm4vsxz64d7f1cydpa02ppi4wp64f3h")))

(define-public crate-irox-time-0.4.2 (c (n "irox-time") (v "0.4.2") (d (list (d (n "irox-enums") (r "^0") (d #t) (k 0)) (d (n "irox-tools") (r "^0") (k 0)) (d (n "irox-units") (r "^0") (d #t) (k 0)))) (h "1lbnywbsaf7i302k60hwc0kkqwzy7p8yihf5jd5v8ds8saxi1pam") (f (quote (("std") ("default" "std"))))))

