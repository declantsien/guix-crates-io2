(define-module (crates-io ir ox irox-sirf) #:use-module (crates-io))

(define-public crate-irox-sirf-0.1.0 (c (n "irox-sirf") (v "0.1.0") (d (list (d (n "irox-tools") (r "^0.1") (d #t) (k 0)))) (h "1dmmmx7pq3w3kn1g9qy9ghf3mna77zmzp59hd0zb890iac7ic8w2")))

(define-public crate-irox-sirf-0.2.0 (c (n "irox-sirf") (v "0.2.0") (d (list (d (n "irox-structs") (r "^0") (d #t) (k 0)) (d (n "irox-tools") (r "^0") (d #t) (k 0)) (d (n "log") (r "^0") (d #t) (k 0)))) (h "1kmkg2v32q3xz4zrn5h003vzziax00gb692v02gh24kvsd3g2s0p")))

(define-public crate-irox-sirf-0.3.0 (c (n "irox-sirf") (v "0.3.0") (d (list (d (n "irox-structs") (r "^0") (d #t) (k 0)) (d (n "irox-tools") (r "^0") (d #t) (k 0)) (d (n "log") (r "=0.4.20") (d #t) (k 0)))) (h "10pspr6qw8ww17zr776aig5014cf45ryingqzsf57zgi4s6gmil3")))

(define-public crate-irox-sirf-0.3.1 (c (n "irox-sirf") (v "0.3.1") (d (list (d (n "irox-structs") (r "^0") (d #t) (k 0)) (d (n "irox-tools") (r "^0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1gylllxnb41nnfk9z91rr6m50vjaw4gvqr0bai1vrglx02xj0ivp")))

(define-public crate-irox-sirf-0.4.0 (c (n "irox-sirf") (v "0.4.0") (d (list (d (n "irox-structs") (r "^0") (d #t) (k 0)) (d (n "irox-tools") (r "^0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "14p9v68kf5i3rm4vrxj4zh2r8jlrgpajr5f4m5q3was0sj58r16y")))

(define-public crate-irox-sirf-0.4.1 (c (n "irox-sirf") (v "0.4.1") (d (list (d (n "irox-bits") (r "^0") (f (quote ("std"))) (d #t) (k 0)) (d (n "irox-structs") (r "^0") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "irox-tools") (r "^0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "12qpmf1jvgrzxwfg4dg0g02jvrn816lkh9ib1rcyfl1ylw04k8qg")))

