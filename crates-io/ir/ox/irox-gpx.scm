(define-module (crates-io ir ox irox-gpx) #:use-module (crates-io))

(define-public crate-irox-gpx-0.1.0 (c (n "irox-gpx") (v "0.1.0") (d (list (d (n "irox-carto") (r "^0") (d #t) (k 0)) (d (n "irox-tools") (r "^0") (d #t) (k 0)) (d (n "irox-units") (r "^0") (d #t) (k 0)) (d (n "log") (r "=0.4.20") (d #t) (k 0)) (d (n "time") (r "=0.3.29") (f (quote ("formatting" "macros"))) (d #t) (k 0)) (d (n "xml") (r "=0.8.10") (d #t) (k 0)))) (h "0zf5mhgjqhzqh9m0x3x8ia86k346kjnx9afyw8acak1jn7m3753a")))

(define-public crate-irox-gpx-0.2.0 (c (n "irox-gpx") (v "0.2.0") (d (list (d (n "irox-carto") (r "^0") (d #t) (k 0)) (d (n "irox-tools") (r "^0") (d #t) (k 0)) (d (n "irox-units") (r "^0") (d #t) (k 0)) (d (n "log") (r "=0.4.20") (d #t) (k 0)) (d (n "time") (r "=0.3.29") (f (quote ("formatting" "macros"))) (d #t) (k 0)) (d (n "xml") (r "=0.8.10") (d #t) (k 0)))) (h "148n63gs9mcidz48gyddzwl0ivx1c6fd5raaays6cpbfdw99if9g")))

(define-public crate-irox-gpx-0.3.0 (c (n "irox-gpx") (v "0.3.0") (d (list (d (n "irox-carto") (r "^0") (d #t) (k 0)) (d (n "irox-time") (r "^0") (d #t) (k 0)) (d (n "irox-tools") (r "^0") (d #t) (k 0)) (d (n "irox-units") (r "^0") (d #t) (k 0)) (d (n "log") (r "=0.4.20") (d #t) (k 0)) (d (n "xml") (r "=0.8.10") (d #t) (k 0)))) (h "1v0ymkpy8q7v6bbb5zl4par1kp804s7nkvvdm05zn3ycq083mgna")))

(define-public crate-irox-gpx-0.4.0 (c (n "irox-gpx") (v "0.4.0") (d (list (d (n "irox-carto") (r "^0") (d #t) (k 0)) (d (n "irox-time") (r "^0") (d #t) (k 0)) (d (n "irox-units") (r "^0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "xml") (r "^0.8") (d #t) (k 0)))) (h "1b5qrfj094nk02wwhyr8mbx40bfj4wnclpxsz6hgh2dxiwghlsr8")))

(define-public crate-irox-gpx-0.5.0 (c (n "irox-gpx") (v "0.5.0") (d (list (d (n "irox-carto") (r "^0") (d #t) (k 0)) (d (n "irox-time") (r "^0") (d #t) (k 0)) (d (n "irox-units") (r "^0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "xml") (r "^0.8") (d #t) (k 0)))) (h "0z0wjvh0fjh39biyfzx6fb5hrb9102kiw8fwqdycqlr69dlj8qw4")))

