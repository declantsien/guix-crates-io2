(define-module (crates-io ir ox irox-enums) #:use-module (crates-io))

(define-public crate-irox-enums-0.1.0 (c (n "irox-enums") (v "0.1.0") (d (list (d (n "irox-enums_derive") (r "^0") (d #t) (k 0)))) (h "15gkmkmrxzcjkpygvcdmx72z6yd63wzh6zv1asb87vhf0c67yqxm")))

(define-public crate-irox-enums-0.2.0 (c (n "irox-enums") (v "0.2.0") (d (list (d (n "irox-enums_derive") (r "^0") (d #t) (k 0)))) (h "0gvia93w7k4pn6iyfx6si51rcg77p145vby2k69s0a7g0pv0s176")))

(define-public crate-irox-enums-0.2.1 (c (n "irox-enums") (v "0.2.1") (d (list (d (n "irox-enums_derive") (r "^0") (d #t) (k 0)))) (h "04fxj4w0pcrlh2jdzrk4h0z17rbvzq5m2l8cmc22s0xmgk3avjh7")))

(define-public crate-irox-enums-0.2.2 (c (n "irox-enums") (v "0.2.2") (d (list (d (n "irox-enums_derive") (r "^0") (d #t) (k 0)))) (h "0ykbf6prjd6zkr3ddh16hqzvwkr8x123cz86wcwc01v8xkq2x7mc")))

