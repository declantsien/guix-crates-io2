(define-module (crates-io ir ox irox-structs) #:use-module (crates-io))

(define-public crate-irox-structs-0.1.0 (c (n "irox-structs") (v "0.1.0") (d (list (d (n "irox-structs_derive") (r "^0") (d #t) (k 0)) (d (n "irox-tools") (r "^0") (d #t) (k 0)))) (h "1dg3qa1wq8yc0chnnrypc3wsa2l1gfjbmaa55wkxcsbqvxa33wdi")))

(define-public crate-irox-structs-0.2.0 (c (n "irox-structs") (v "0.2.0") (d (list (d (n "irox-structs_derive") (r "^0") (d #t) (k 0)) (d (n "irox-tools") (r "^0") (d #t) (k 0)))) (h "0ib6v9kca4lbf0sh2z1b4q4sms42gim755mvsk717vhlvrwh0ndl")))

(define-public crate-irox-structs-0.3.0 (c (n "irox-structs") (v "0.3.0") (d (list (d (n "irox-structs_derive") (r "^0") (d #t) (k 0)) (d (n "irox-tools") (r "^0") (d #t) (k 0)))) (h "1pl7gh1ri22frj80annxghli5kmvhyd8sqr1501nl3hm3953gyy7")))

(define-public crate-irox-structs-0.3.1 (c (n "irox-structs") (v "0.3.1") (d (list (d (n "irox-structs_derive") (r "^0") (d #t) (k 0)) (d (n "irox-tools") (r "^0") (d #t) (k 0)))) (h "1x39h2fvlgzwrs50kr5836v8ak7jvcwaagwxkc4dmv4f5x0rr1ya")))

(define-public crate-irox-structs-0.4.0 (c (n "irox-structs") (v "0.4.0") (d (list (d (n "irox-structs_derive") (r "^0") (d #t) (k 0)) (d (n "irox-tools") (r "^0") (d #t) (k 0)))) (h "0s9cjg4spdwp7y8pncgzncc89782zcxi24k4qy7647dxk5lbjkr5")))

(define-public crate-irox-structs-0.5.0 (c (n "irox-structs") (v "0.5.0") (d (list (d (n "irox-structs_derive") (r "^0") (d #t) (k 0)) (d (n "irox-tools") (r "^0") (k 0)))) (h "0mnn4hx8v81kly4b4sk3y2jq3zmla4zabg1qfh54zsqpydrqhdyf")))

(define-public crate-irox-structs-0.5.1 (c (n "irox-structs") (v "0.5.1") (d (list (d (n "irox-bits") (r "^0") (d #t) (k 0)) (d (n "irox-bits") (r "^0") (f (quote ("alloc"))) (d #t) (k 2)) (d (n "irox-structs_derive") (r "^0") (d #t) (k 0)))) (h "03wg5jldg0ws3i952isb8a45mxx74bybjqwdcysg1zpn169iivk9") (f (quote (("default") ("alloc" "irox-bits/alloc"))))))

