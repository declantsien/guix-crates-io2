(define-module (crates-io ir ox irox-threading) #:use-module (crates-io))

(define-public crate-irox-threading-0.1.0 (c (n "irox-threading") (v "0.1.0") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "1ccpm3cqyzvayi5r6bhymvc441wbcksq00j0sqmb119w89yzydck")))

(define-public crate-irox-threading-0.2.0 (c (n "irox-threading") (v "0.2.0") (d (list (d (n "irox-log") (r "^0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "num_cpus") (r "^1.16.0") (o #t) (d #t) (k 0)))) (h "08x39196d4lkm1pz99dmm6l1232xp5az3cwq88mqn5hd6ai3qn36") (f (quote (("default"))))))

(define-public crate-irox-threading-0.2.1 (c (n "irox-threading") (v "0.2.1") (d (list (d (n "irox-log") (r "^0") (d #t) (k 2)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "num_cpus") (r "^1.16.0") (o #t) (d #t) (k 0)))) (h "01bn1vcgi8i3g5cvkg73iz76nkk0chbg9nm2mq6dpl96p57i1sz2") (f (quote (("default"))))))

(define-public crate-irox-threading-0.2.2 (c (n "irox-threading") (v "0.2.2") (d (list (d (n "irox-log") (r "^0") (d #t) (k 2)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "num_cpus") (r "^1.16.0") (o #t) (d #t) (k 0)))) (h "1az4ggc6qxqvdwjkypvaxfwciriyhd3h1hvq7dggs14xp5q8670w") (f (quote (("default"))))))

(define-public crate-irox-threading-0.2.3 (c (n "irox-threading") (v "0.2.3") (d (list (d (n "irox-log") (r "^0") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num_cpus") (r "^1.16.0") (o #t) (d #t) (k 0)))) (h "00njpdp2kwqj109w4hp51lb0x1jx1rn1v5fwx9y6wl9xqx90ahjj") (f (quote (("default"))))))

(define-public crate-irox-threading-0.2.4 (c (n "irox-threading") (v "0.2.4") (d (list (d (n "irox-log") (r "^0") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num_cpus") (r "^1.16.0") (o #t) (d #t) (k 0)))) (h "1s723xmhly5rb3wjkzk0j054d7arhgn7bfzdqh6x25arb2ymx5mc") (f (quote (("default"))))))

