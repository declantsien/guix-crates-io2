(define-module (crates-io ir ox irox-unsafe) #:use-module (crates-io))

(define-public crate-irox-unsafe-0.1.0 (c (n "irox-unsafe") (v "0.1.0") (d (list (d (n "irox-safe-windows") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "1bcicaik3rnh847082bf4w582a0ps1r5ml7rq11a06k807v5479s") (f (quote (("default")))) (s 2) (e (quote (("safe-windows" "dep:irox-safe-windows"))))))

(define-public crate-irox-unsafe-0.1.1 (c (n "irox-unsafe") (v "0.1.1") (d (list (d (n "irox-safe-linux") (r "^0") (o #t) (d #t) (k 0)) (d (n "irox-safe-windows") (r "^0.1.1") (o #t) (d #t) (k 0)))) (h "12i0069illj825w9ph3idc0pbyk48v23f28sw3nq7kjqs7x6y3zi") (f (quote (("default")))) (s 2) (e (quote (("safe-windows" "dep:irox-safe-windows") ("safe-linux" "dep:irox-safe-linux"))))))

(define-public crate-irox-unsafe-0.1.2 (c (n "irox-unsafe") (v "0.1.2") (d (list (d (n "irox-safe-linux") (r "^0") (o #t) (d #t) (k 0)) (d (n "irox-safe-windows") (r "^0.1.2") (o #t) (d #t) (k 0)))) (h "08bfcfp1y7fykki2ss6wrajyn6iyspymj9s1zpn05yqwl46piqfc") (f (quote (("default")))) (s 2) (e (quote (("safe-windows" "dep:irox-safe-windows") ("safe-linux" "dep:irox-safe-linux"))))))

