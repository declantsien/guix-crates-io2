(define-module (crates-io ir ox irox-types) #:use-module (crates-io))

(define-public crate-irox-types-0.1.0 (c (n "irox-types") (v "0.1.0") (d (list (d (n "irox-enums") (r "^0") (d #t) (k 0)) (d (n "irox-enums_derive") (r "^0") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits"))) (o #t) (d #t) (k 0)))) (h "1fi81msaawldqpra2lz0r3s15wb81vj4c67v84ck7h7pssd22fim") (f (quote (("default")))) (s 2) (e (quote (("syn" "dep:syn"))))))

(define-public crate-irox-types-0.2.0 (c (n "irox-types") (v "0.2.0") (d (list (d (n "irox-enums") (r "^0") (d #t) (k 0)) (d (n "irox-enums_derive") (r "^0") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits"))) (o #t) (d #t) (k 0)))) (h "05pfi0d3z7v4cl41qhq2hv7zm3shc3bls4nzjgq2p9fcird0r3bd") (f (quote (("default")))) (s 2) (e (quote (("syn" "dep:syn"))))))

(define-public crate-irox-types-0.2.1 (c (n "irox-types") (v "0.2.1") (d (list (d (n "irox-enums") (r "^0") (d #t) (k 0)) (d (n "irox-enums_derive") (r "^0") (d #t) (k 0)) (d (n "syn") (r "=2.0.37") (f (quote ("extra-traits"))) (o #t) (d #t) (k 0)))) (h "1cc7zpr1wvq9vrzzi0k28id54apc31df5qaa8llwzs6bhr7hkx1s") (f (quote (("default")))) (s 2) (e (quote (("syn" "dep:syn"))))))

(define-public crate-irox-types-0.2.2 (c (n "irox-types") (v "0.2.2") (d (list (d (n "irox-enums") (r "^0") (d #t) (k 0)) (d (n "irox-enums_derive") (r "^0") (d #t) (k 0)) (d (n "syn") (r "=2.0.38") (f (quote ("extra-traits"))) (o #t) (d #t) (k 0)))) (h "0p52vqw7andxjyfawfd5h5qpwv2rxxypfsics9bf72pgprmmbcwf") (f (quote (("default")))) (s 2) (e (quote (("syn" "dep:syn"))))))

(define-public crate-irox-types-0.3.0 (c (n "irox-types") (v "0.3.0") (d (list (d (n "irox-enums") (r "^0") (d #t) (k 0)) (d (n "irox-enums_derive") (r "^0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits"))) (o #t) (d #t) (k 0)))) (h "1pl3i89lpxxnxhsmv6mjvrh1qcmakdvi9qx60y49jkl0566na1ah") (f (quote (("default")))) (s 2) (e (quote (("syn" "dep:syn"))))))

(define-public crate-irox-types-0.4.0 (c (n "irox-types") (v "0.4.0") (d (list (d (n "irox-enums") (r "^0") (d #t) (k 0)) (d (n "irox-enums_derive") (r "^0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits"))) (o #t) (d #t) (k 0)))) (h "1bh2x3gj715csbclnzcwdynn5l7nkipnac2zxdbk8vcpicqpzx3i") (f (quote (("default")))) (s 2) (e (quote (("syn" "dep:syn"))))))

(define-public crate-irox-types-0.4.1 (c (n "irox-types") (v "0.4.1") (d (list (d (n "irox-enums") (r "^0") (d #t) (k 0)) (d (n "irox-enums_derive") (r "^0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits"))) (o #t) (d #t) (k 0)))) (h "0gw9qh1c9wd33xzrdzw9wz95hm3agg5br1lqv9ysz1grhwjlif6l") (f (quote (("default")))) (s 2) (e (quote (("syn" "dep:syn"))))))

