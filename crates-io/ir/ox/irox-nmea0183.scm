(define-module (crates-io ir ox irox-nmea0183) #:use-module (crates-io))

(define-public crate-irox-nmea0183-0.1.0 (c (n "irox-nmea0183") (v "0.1.0") (d (list (d (n "irox-carto") (r "^0") (d #t) (k 0)) (d (n "irox-enums") (r "^0") (d #t) (k 0)) (d (n "irox-structs") (r "^0") (d #t) (k 0)) (d (n "irox-tools") (r "^0") (d #t) (k 0)) (d (n "irox-units") (r "^0") (d #t) (k 0)) (d (n "log") (r "^0") (d #t) (k 0)) (d (n "time") (r "^0") (f (quote ("formatting" "macros"))) (d #t) (k 0)))) (h "0fqvpp8xmpfiv6zh1nk66dad3b6408jg42bmazm345bv5lb3vgyf")))

(define-public crate-irox-nmea0183-0.2.0 (c (n "irox-nmea0183") (v "0.2.0") (d (list (d (n "irox-carto") (r "^0") (d #t) (k 0)) (d (n "irox-enums") (r "^0") (d #t) (k 0)) (d (n "irox-structs") (r "^0") (d #t) (k 0)) (d (n "irox-tools") (r "^0") (d #t) (k 0)) (d (n "irox-units") (r "^0") (d #t) (k 0)) (d (n "log") (r "=0.4.20") (d #t) (k 0)) (d (n "time") (r "=0.3.29") (f (quote ("formatting" "macros"))) (d #t) (k 0)))) (h "05aih8y1hfw8dlii71vk5840n8a86rv6zn3d5kvg9cv1ny3jax30")))

(define-public crate-irox-nmea0183-0.3.0 (c (n "irox-nmea0183") (v "0.3.0") (d (list (d (n "irox-carto") (r "^0") (d #t) (k 0)) (d (n "irox-enums") (r "^0") (d #t) (k 0)) (d (n "irox-structs") (r "^0") (d #t) (k 0)) (d (n "irox-time") (r "^0") (d #t) (k 0)) (d (n "irox-tools") (r "^0") (d #t) (k 0)) (d (n "irox-units") (r "^0") (d #t) (k 0)) (d (n "log") (r "=0.4.20") (d #t) (k 0)))) (h "0wy5p3pa6ixjhin7gvqcrdf4pf2dg593acgwfdfkb4121s1c33x5")))

(define-public crate-irox-nmea0183-0.4.0 (c (n "irox-nmea0183") (v "0.4.0") (d (list (d (n "irox-carto") (r "^0") (d #t) (k 0)) (d (n "irox-enums") (r "^0") (d #t) (k 0)) (d (n "irox-time") (r "^0") (d #t) (k 0)) (d (n "irox-tools") (r "^0") (d #t) (k 0)) (d (n "irox-units") (r "^0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0df418rlf656rlm70m32856psmigqvfp0kxq8g7305rrnqzcxi8l")))

(define-public crate-irox-nmea0183-0.5.0 (c (n "irox-nmea0183") (v "0.5.0") (d (list (d (n "irox-carto") (r "^0") (d #t) (k 0)) (d (n "irox-enums") (r "^0") (d #t) (k 0)) (d (n "irox-time") (r "^0") (d #t) (k 0)) (d (n "irox-tools") (r "^0") (d #t) (k 0)) (d (n "irox-units") (r "^0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0wffp0lcqbnzc3nzy68ki2pb367zwdm30c4k9c7mc1kb4kyykjsm")))

(define-public crate-irox-nmea0183-0.5.1 (c (n "irox-nmea0183") (v "0.5.1") (d (list (d (n "irox-bits") (r "^0") (f (quote ("std"))) (d #t) (k 0)) (d (n "irox-carto") (r "^0") (d #t) (k 0)) (d (n "irox-enums") (r "^0") (d #t) (k 0)) (d (n "irox-time") (r "^0") (d #t) (k 0)) (d (n "irox-tools") (r "^0") (f (quote ("std"))) (d #t) (k 0)) (d (n "irox-units") (r "^0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1340gydff5arrwijiiwjn7p5gjmczqz0ca51b63pds7zs22jpa9c")))

