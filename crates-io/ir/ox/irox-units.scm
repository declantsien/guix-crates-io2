(define-module (crates-io ir ox irox-units) #:use-module (crates-io))

(define-public crate-irox-units-0.1.0 (c (n "irox-units") (v "0.1.0") (h "1hq73kbrrnhgnvlqc76g3x043gc6xzn2rdba3jrz0rak7jyavvdc")))

(define-public crate-irox-units-0.2.0 (c (n "irox-units") (v "0.2.0") (h "0by3faqqpi3pdbcfnf9ihq8g11mjf1ib4d19fm462bqaynryc5qv")))

(define-public crate-irox-units-0.3.0 (c (n "irox-units") (v "0.3.0") (h "14lpii9bpqka5b0q4s4rr8vavswj1b8w33xhyjf6w6gnlpi4lk09")))

(define-public crate-irox-units-0.3.1 (c (n "irox-units") (v "0.3.1") (h "01qcx7ay4qdarnjmqfmbsw235rzdg4fjzwas2rvm5qz5ncbm9x6l")))

(define-public crate-irox-units-0.3.2 (c (n "irox-units") (v "0.3.2") (h "1hpjjk97lqirqk5b0ldg965dckf2z39qmmxhyci88w8n6aa4j11h")))

(define-public crate-irox-units-0.3.3 (c (n "irox-units") (v "0.3.3") (h "0yin7jk3nrpz7mgbpgmnbxyqclaxh5awpfml8y6fj5s3z1xy4zii")))

(define-public crate-irox-units-0.3.4 (c (n "irox-units") (v "0.3.4") (h "0yri98ghznw5ifi14rpqdz79z9bbkfg59vxc9fx0gv374mqd2vn3")))

(define-public crate-irox-units-0.3.5 (c (n "irox-units") (v "0.3.5") (d (list (d (n "irox-tools") (r "^0") (k 0)))) (h "0npp7bk0ba3j16rxlb8wlrlqhpcdz4yjzavi8rx7ap1w84k87yqy") (f (quote (("std_errors") ("default"))))))

(define-public crate-irox-units-0.3.6 (c (n "irox-units") (v "0.3.6") (d (list (d (n "irox-tools") (r "^0") (k 0)))) (h "05yjfgdvdld7zrn4h401m3rv0rf2q86ihakp3svpw7mz54rpkaw4") (f (quote (("std_errors") ("default"))))))

(define-public crate-irox-units-0.4.0 (c (n "irox-units") (v "0.4.0") (d (list (d (n "irox-tools") (r "^0") (k 0)))) (h "0f3k2cbsn1wpvyax9jj5g4nka54qp6kw9p4w5k0mjnznvs2g0hnz") (f (quote (("std") ("default"))))))

