(define-module (crates-io ir ox irox-git-tools) #:use-module (crates-io))

(define-public crate-irox-git-tools-0.1.0 (c (n "irox-git-tools") (v "0.1.0") (d (list (d (n "git2") (r "^0.18.1") (f (quote ("vendored-openssl" "vendored-libgit2"))) (d #t) (k 0)) (d (n "irox-tools") (r "^0") (d #t) (k 0)))) (h "0mrk856zgsmzwsc8h11dki8gcnydp5p3p9y46mi0hxsnf45c739x")))

(define-public crate-irox-git-tools-0.2.0 (c (n "irox-git-tools") (v "0.2.0") (d (list (d (n "git2") (r "^0.18.1") (f (quote ("vendored-openssl" "vendored-libgit2"))) (d #t) (k 0)) (d (n "irox-tools") (r "^0") (d #t) (k 0)))) (h "1clyayjg24vy39pvg3bb997pp17nb4gyk6rw6r2sbs2h0kn63nhz")))

