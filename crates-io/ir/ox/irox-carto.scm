(define-module (crates-io ir ox irox-carto) #:use-module (crates-io))

(define-public crate-irox-carto-0.1.0 (c (n "irox-carto") (v "0.1.0") (d (list (d (n "irox-tools") (r "^0.1") (d #t) (k 2)) (d (n "irox-units") (r "^0.1") (d #t) (k 0)))) (h "1n9p9c4n3kigsxhi9shsz3laa9m2i67l66hm70z58yvqqyi6ys74")))

(define-public crate-irox-carto-0.2.0 (c (n "irox-carto") (v "0.2.0") (d (list (d (n "irox-enums") (r "^0") (d #t) (k 0)) (d (n "irox-tools") (r "^0") (d #t) (k 0)) (d (n "irox-units") (r "^0") (d #t) (k 0)) (d (n "log") (r "^0") (d #t) (k 0)) (d (n "windows") (r "^0.51.1") (f (quote ("Devices_Geolocation" "Foundation"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1ibddlksh8d18andbnwxyb05rvdlcjpkb35hlf3r13y4fgl27zk2")))

(define-public crate-irox-carto-0.2.1 (c (n "irox-carto") (v "0.2.1") (d (list (d (n "irox-enums") (r "^0") (d #t) (k 0)) (d (n "irox-tools") (r "^0") (d #t) (k 0)) (d (n "irox-units") (r "^0") (d #t) (k 0)) (d (n "log") (r "=0.4.20") (d #t) (k 0)) (d (n "windows") (r "=0.51.1") (f (quote ("Devices_Geolocation" "Foundation"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0dkvr1brc80fvprj5ks3gw06rkkrsa2fnfpl90xfgrf4h4y22802")))

(define-public crate-irox-carto-0.3.0 (c (n "irox-carto") (v "0.3.0") (d (list (d (n "irox-enums") (r "^0") (d #t) (k 0)) (d (n "irox-tools") (r "^0") (d #t) (k 0)) (d (n "irox-units") (r "^0") (d #t) (k 0)) (d (n "log") (r "=0.4.20") (d #t) (k 0)) (d (n "windows") (r "=0.51.1") (f (quote ("Devices_Geolocation" "Foundation"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0zanw3q8hpvk0511jbrfwwi5gby0bff66ks91zy1359zlmri19rj")))

(define-public crate-irox-carto-0.4.0 (c (n "irox-carto") (v "0.4.0") (d (list (d (n "irox-csv") (r "^0") (d #t) (k 1)) (d (n "irox-enums") (r "^0") (d #t) (k 0)) (d (n "irox-time") (r "^0") (d #t) (k 0)) (d (n "irox-tools") (r "^0") (d #t) (k 0)) (d (n "irox-units") (r "^0") (d #t) (k 0)) (d (n "log") (r "=0.4.20") (d #t) (k 0)) (d (n "windows") (r "=0.51.1") (f (quote ("Devices_Geolocation" "Foundation"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0zpk4k4zxdx9bw8nqda2c4xcg46cyjfs0zdvnyqr4fvh7gj25afd")))

(define-public crate-irox-carto-0.5.0 (c (n "irox-carto") (v "0.5.0") (d (list (d (n "irox-csv") (r "^0") (d #t) (k 1)) (d (n "irox-enums") (r "^0") (d #t) (k 0)) (d (n "irox-time") (r "^0") (d #t) (k 0)) (d (n "irox-tools") (r "^0") (d #t) (k 0)) (d (n "irox-units") (r "^0") (d #t) (k 0)) (d (n "windows") (r "^0.52") (f (quote ("Devices_Geolocation" "Foundation"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1jypfgl5ai0pc0bmr4jq5jy2j40d6057iaxjk9njsk59krb869za")))

(define-public crate-irox-carto-0.6.0 (c (n "irox-carto") (v "0.6.0") (d (list (d (n "irox-csv") (r "^0") (d #t) (k 1)) (d (n "irox-enums") (r "^0") (d #t) (k 0)) (d (n "irox-time") (r "^0") (d #t) (k 0)) (d (n "irox-tools") (r "^0") (d #t) (k 0)) (d (n "irox-units") (r "^0") (d #t) (k 0)) (d (n "windows") (r "^0.52") (f (quote ("Devices_Geolocation" "Foundation"))) (d #t) (t "cfg(windows)") (k 0)))) (h "19i0krgm2ha7myv2jh2milv1yz01j7bwv1v14qcf1h5hgyik4g3d")))

(define-public crate-irox-carto-0.6.1 (c (n "irox-carto") (v "0.6.1") (d (list (d (n "irox-csv") (r "^0") (d #t) (k 1)) (d (n "irox-enums") (r "^0") (d #t) (k 0)) (d (n "irox-time") (r "^0") (d #t) (k 0)) (d (n "irox-tools") (r "^0") (d #t) (k 0)) (d (n "irox-units") (r "^0") (d #t) (k 0)) (d (n "windows") (r "^0.53") (f (quote ("Devices_Geolocation" "Foundation"))) (d #t) (t "cfg(windows)") (k 0)))) (h "11pcmcmp220q6rzcgvn7df7zmv8p262i0a9p9a7s5vxzscv1gjbn")))

