(define-module (crates-io ir ox irox-tools) #:use-module (crates-io))

(define-public crate-irox-tools-0.1.0 (c (n "irox-tools") (v "0.1.0") (h "1f94x8pw0lhvq3x75mh2d3m0cv98ank3zh8n9dp3p0k3lxz1qfva")))

(define-public crate-irox-tools-0.2.0 (c (n "irox-tools") (v "0.2.0") (h "1hj2b8fi5s7pm1bq1na5mdysbakz0fbd6jsk6j02508n8bjfgrpk")))

(define-public crate-irox-tools-0.2.1 (c (n "irox-tools") (v "0.2.1") (h "0ky5641lxvps2ynn52a41i0z372gxxs8fk7xjd9sm7bxbhvxfm43")))

(define-public crate-irox-tools-0.2.2 (c (n "irox-tools") (v "0.2.2") (h "1qa7bbvnblncqqfnjhb96nhq06w4m713cc4iwgwfdqpwxdc3048x")))

(define-public crate-irox-tools-0.3.0 (c (n "irox-tools") (v "0.3.0") (h "1jvcxm09p99grkz1lp91f7zipk38h6nza9w0hllykp0hkxxlp6hc")))

(define-public crate-irox-tools-0.3.1 (c (n "irox-tools") (v "0.3.1") (h "01442ch5vsx4hk0sihwjbz9yjlj5zd50jkzn9188zrcq6p3csyw7")))

(define-public crate-irox-tools-0.3.2 (c (n "irox-tools") (v "0.3.2") (h "0ji7a7s3rkgcnqn0vk47la3xaypq6k1fyxrar4z11baql9pqj7sh")))

(define-public crate-irox-tools-0.4.0 (c (n "irox-tools") (v "0.4.0") (h "01daiqkb86dd8mmcqvzm2gm7nirg432f7d2iwrnh1hlrzynswc3g")))

(define-public crate-irox-tools-0.4.1 (c (n "irox-tools") (v "0.4.1") (h "0wcg7lwdjg25x6majq8pld569951j9zv0zp43ffy62ccalimkapd")))

(define-public crate-irox-tools-0.5.0 (c (n "irox-tools") (v "0.5.0") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)))) (h "019cq6ynnfwlj0bq75f7f05apqzig1i2vpcjaryz88izcg64wj3z") (f (quote (("std") ("default" "std"))))))

(define-public crate-irox-tools-0.5.1 (c (n "irox-tools") (v "0.5.1") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)))) (h "1hhp6pwskgivzbhdwarv2vr918gy2nf2yxmznfj7ajfzkkl34a2p") (f (quote (("std") ("default" "std"))))))

(define-public crate-irox-tools-0.5.2 (c (n "irox-tools") (v "0.5.2") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)))) (h "15nvs2hfivl46rnvf6pyp3kygd39fcsxy67qagh7b4psc6l7rssn") (f (quote (("std") ("default" "std"))))))

(define-public crate-irox-tools-0.5.3 (c (n "irox-tools") (v "0.5.3") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)))) (h "0321hscga7lrr2mrpdb5lks7gysfsxcgx71av1p85dvfhkigq563") (f (quote (("std") ("default" "std"))))))

(define-public crate-irox-tools-0.6.0 (c (n "irox-tools") (v "0.6.0") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)))) (h "1x7vi5ckyrs0cvz0lvx6pd956fxz3mqdbarhj97w1b9hfanzl6wy") (f (quote (("std") ("default" "std"))))))

(define-public crate-irox-tools-0.6.1 (c (n "irox-tools") (v "0.6.1") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)))) (h "1m0wyjf4m4kbpg5hs5axn8ggrhgjv669j72bwfh32wl0whfzayid") (f (quote (("std") ("default" "std"))))))

(define-public crate-irox-tools-0.7.0 (c (n "irox-tools") (v "0.7.0") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)))) (h "1kbc74yhq2n5im9dw7did19wmmqj9im4ym621vnkhdqvhbj64j7x") (f (quote (("std") ("default" "std"))))))

(define-public crate-irox-tools-0.8.0 (c (n "irox-tools") (v "0.8.0") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "irox-bits") (r "^0") (d #t) (k 0)))) (h "1manwxmf2r8wasfwvgfbi4qvb5wzgkrs4zjanch5cdnjmg3gzqz9") (f (quote (("std" "irox-bits/std" "alloc") ("default") ("alloc" "irox-bits/alloc"))))))

(define-public crate-irox-tools-0.8.1 (c (n "irox-tools") (v "0.8.1") (d (list (d (n "core_affinity") (r "^0.8.1") (d #t) (k 2)) (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "irox-bits") (r "^0") (d #t) (k 0)))) (h "1j0771l22jq6qc4nlhz67yj4wr0lxpbdyjp9mk6plhkw71r5pn8s") (f (quote (("std" "irox-bits/std" "alloc") ("default") ("alloc" "irox-bits/alloc"))))))

