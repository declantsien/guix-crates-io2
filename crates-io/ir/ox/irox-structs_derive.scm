(define-module (crates-io ir ox irox-structs_derive) #:use-module (crates-io))

(define-public crate-irox-structs_derive-0.1.0 (c (n "irox-structs_derive") (v "0.1.0") (d (list (d (n "irox-types") (r "^0") (f (quote ("syn"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0763ffrilc2qdj0g8a79qigx0swn4xnjxxf93jw78iry6s899k00")))

(define-public crate-irox-structs_derive-0.2.0 (c (n "irox-structs_derive") (v "0.2.0") (d (list (d (n "irox-types") (r "^0") (f (quote ("syn"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0bvi8lfd6gr486q9mbywkqbvigp020fpd97r93wz66iil8p30646")))

(define-public crate-irox-structs_derive-0.2.1 (c (n "irox-structs_derive") (v "0.2.1") (d (list (d (n "irox-types") (r "^0") (f (quote ("syn"))) (d #t) (k 0)) (d (n "proc-macro2") (r "=1.0.68") (d #t) (k 0)) (d (n "quote") (r "=1.0.33") (d #t) (k 0)) (d (n "syn") (r "=2.0.37") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0y3iac2k9g8qrx091cypljj6kpizjv5saibf6by9mpavs12wjlqr")))

(define-public crate-irox-structs_derive-0.2.2 (c (n "irox-structs_derive") (v "0.2.2") (d (list (d (n "irox-types") (r "^0") (f (quote ("syn"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1jnd3plsyj6xsa0nxga4f0y2f7jhqc67i8halnc3ssj75nran9vs")))

(define-public crate-irox-structs_derive-0.2.3 (c (n "irox-structs_derive") (v "0.2.3") (d (list (d (n "irox-types") (r "^0") (f (quote ("syn"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0x0dk4xvvyhcna3bzkqhh577fayxy3bqibdbyflab7sh7cngrhgv")))

(define-public crate-irox-structs_derive-0.3.0 (c (n "irox-structs_derive") (v "0.3.0") (d (list (d (n "irox-tools") (r "^0") (k 0)) (d (n "irox-types") (r "^0") (f (quote ("syn"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0xqqyam0dxlp063j9jqqlz5br7f6yn0vqi52f8f501wlwr8yh9ik")))

(define-public crate-irox-structs_derive-0.3.1 (c (n "irox-structs_derive") (v "0.3.1") (d (list (d (n "irox-tools") (r "^0") (k 0)) (d (n "irox-types") (r "^0") (f (quote ("syn"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1638ylgdlsnb0ysmk7x0d8ybvpzkyy8xyz594j89l8mdf511fr0m")))

