(define-module (crates-io ir ox irox-bits) #:use-module (crates-io))

(define-public crate-irox-bits-0.1.0 (c (n "irox-bits") (v "0.1.0") (h "0jv1qrwcgah1pm0xcfnrvrcip7pw8hqsmr7p45dixl6g0hmfn3mg") (f (quote (("std" "alloc") ("default") ("alloc"))))))

(define-public crate-irox-bits-0.1.1 (c (n "irox-bits") (v "0.1.1") (h "0y60r3cvldjh2fl7dmrwbj8fx33hxmnmax38b88lawxsx1l8ln6r") (f (quote (("std" "alloc") ("default") ("alloc"))))))

