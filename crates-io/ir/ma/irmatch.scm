(define-module (crates-io ir ma irmatch) #:use-module (crates-io))

(define-public crate-irmatch-0.1.0 (c (n "irmatch") (v "0.1.0") (h "15q4d759x2lbcli7zgy8v10lhwyn1syjdyl7mkqkzhpk5mzpjj1d")))

(define-public crate-irmatch-0.2.0 (c (n "irmatch") (v "0.2.0") (h "18xagsypmnrvfvyxcxv3k3pvxfc7v27l0myi8y158qi9hji0y0y7")))

