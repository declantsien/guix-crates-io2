(define-module (crates-io ir o- iro-cli) #:use-module (crates-io))

(define-public crate-iro-cli-0.2.1 (c (n "iro-cli") (v "0.2.1") (d (list (d (n "colorconv") (r "^0.1.1") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.1") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "0zr2d0xyna4fmmzw5iqnrk9nahjgb07fi6q8c16lq34hl9ay7540")))

