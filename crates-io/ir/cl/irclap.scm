(define-module (crates-io ir cl irclap) #:use-module (crates-io))

(define-public crate-irclap-0.1.0 (c (n "irclap") (v "0.1.0") (d (list (d (n "clap") (r "^2.30") (d #t) (k 0)) (d (n "clap") (r "^2.30") (f (quote ("yaml"))) (d #t) (k 2)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "irc") (r "^0.13") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)))) (h "1m0wl26giqrwpj49xard7mbk9vlvm3319mvi3759gdyk3kz18ilf")))

