(define-module (crates-io ir eg iregexp) #:use-module (crates-io))

(define-public crate-iregexp-0.1.0 (c (n "iregexp") (v "0.1.0") (d (list (d (n "pest") (r "^2.7") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7") (d #t) (k 0)))) (h "1i3nw8bw1d6yjjix1m7m9ra8p52x4s9gxsp2h58d35fyfk1vhwy0")))

(define-public crate-iregexp-0.1.1 (c (n "iregexp") (v "0.1.1") (d (list (d (n "pest") (r "^2.7") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7") (d #t) (k 0)))) (h "0k57wcn2607qx9gngz31sm4034qr9mm8zhb00ljf7ry4k8y55151")))

