(define-module (crates-io ir us irust_api) #:use-module (crates-io))

(define-public crate-irust_api-0.1.0 (c (n "irust_api") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)))) (h "1zgdr4bsawrzpnangai4z9pjp56sbdf02f3jn3vxfv78v3n43ndb")))

(define-public crate-irust_api-0.1.1 (c (n "irust_api") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)))) (h "0ynf349l9vpfhy8f7jwqy08yf7h21af5jn1gqzyb0hxgn9nzfp6i")))

(define-public crate-irust_api-0.1.2 (c (n "irust_api") (v "0.1.2") (d (list (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)))) (h "01s5r5js67q18pw40pg3afjpiv44l1j0x49hlswpg207png3p2f1")))

(define-public crate-irust_api-0.2.0 (c (n "irust_api") (v "0.2.0") (d (list (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)))) (h "0ig9245l5c3dh4dxvcdjsrblgyv9dc6m0576zhnww0igi3vrn8ry")))

(define-public crate-irust_api-0.3.0 (c (n "irust_api") (v "0.3.0") (d (list (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)))) (h "0zw4pbgi7ll2v489cflwfbjsacnpn3jmaa79abcnvsphvh2d83b9")))

(define-public crate-irust_api-0.4.0 (c (n "irust_api") (v "0.4.0") (d (list (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)))) (h "14w288v9hxznf04yy64g44k0g8vs9zn7h16cf180s1xl6zym7qn9")))

(define-public crate-irust_api-0.5.0 (c (n "irust_api") (v "0.5.0") (d (list (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)))) (h "05grx5kxvbc3p3jqjf2mcy6pasna270x3scp76rlnipkw9s9ya5l")))

(define-public crate-irust_api-0.6.0 (c (n "irust_api") (v "0.6.0") (d (list (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)))) (h "0dq7kk46wmbhkhpbmd36nsd9icyl4hxpn2rbpfkzcs1d3vzcbf5a")))

(define-public crate-irust_api-0.7.0 (c (n "irust_api") (v "0.7.0") (d (list (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)))) (h "1j61ckldynsak31abh9rdd2gcrlbj6knqhhal8kwp4zlrzx3fsck")))

(define-public crate-irust_api-0.8.0 (c (n "irust_api") (v "0.8.0") (d (list (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)))) (h "0wiz9hvyh3r2bf3h8nya5hp7px6vvix3aqvngpkif2wfd6j0as0r")))

(define-public crate-irust_api-0.9.0 (c (n "irust_api") (v "0.9.0") (d (list (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)))) (h "19x81x2x51q8a456bxix4wwfxs0i9l3d7zic42zfigwyg31n1yw4")))

(define-public crate-irust_api-0.10.0 (c (n "irust_api") (v "0.10.0") (d (list (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)))) (h "0yzhzq08ilaj9gam4syj70154ng2cqg08cyxdiys2bqnnkwxdgk6")))

(define-public crate-irust_api-0.11.0 (c (n "irust_api") (v "0.11.0") (d (list (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)))) (h "1q0ljkiqq6plfgkja4icx6vsbvz31gnha8s9dcr52kff9mwnjm7z")))

(define-public crate-irust_api-0.12.0 (c (n "irust_api") (v "0.12.0") (d (list (d (n "crossterm") (r "^0.20.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "rscript") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)))) (h "0hwllrqjclic4da306vhbcdynwid8q9banhvzmpw5v9vhd51ih2v")))

(define-public crate-irust_api-0.13.0 (c (n "irust_api") (v "0.13.0") (d (list (d (n "crossterm") (r "^0.20.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "rscript") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)))) (h "08hrhb6k6ag368hrn15xf66x8ddwmh1zij84whf5fsrzq6695zw0")))

(define-public crate-irust_api-0.14.0 (c (n "irust_api") (v "0.14.0") (d (list (d (n "crossterm") (r "^0.20.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "rscript") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)))) (h "1g7270zhjhvcc97a2q9ziw5qcc46qyh88gvaij9x7rchc6vxlscm")))

(define-public crate-irust_api-0.15.0 (c (n "irust_api") (v "0.15.0") (d (list (d (n "crossterm") (r "^0.20.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "rscript") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)))) (h "1289xwpzsiz4v43csazfh5m817c6s2g9lxc1p5q1p8dnifvjsl69")))

(define-public crate-irust_api-0.16.0 (c (n "irust_api") (v "0.16.0") (d (list (d (n "crossterm") (r "^0.20.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "rscript") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)))) (h "0mkimc4bgyi9hh0l1j31319g5zhfh798y6qxnzgliqj6bmr37iqa")))

(define-public crate-irust_api-0.17.0 (c (n "irust_api") (v "0.17.0") (d (list (d (n "crossterm") (r "^0.20.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "rscript") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)))) (h "01044cqkjxvl2ciq18ynd6698idg1l3hbycfgpiplqbn4bzv8ksz")))

(define-public crate-irust_api-0.18.0 (c (n "irust_api") (v "0.18.0") (d (list (d (n "crossterm") (r "^0.20.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "rscript") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)))) (h "0w32f06v6892bcr49llni25nhrxhxikimdgzvwxgc2bq5yqq97i4")))

(define-public crate-irust_api-0.19.0 (c (n "irust_api") (v "0.19.0") (d (list (d (n "crossterm") (r "^0.20.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "rscript") (r "^0.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)))) (h "0hgy7a0fsf8q05v0k7hi6nvgrl4p75x1375apg50x5nci5yj53h1")))

(define-public crate-irust_api-0.19.1 (c (n "irust_api") (v "0.19.1") (d (list (d (n "crossterm") (r "^0.21.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "rscript") (r "^0.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)))) (h "1j58xp9b29wha8w5lc9n4qg4hwq3xh7gbk6himsdhckslv27bjhm")))

(define-public crate-irust_api-0.20.0 (c (n "irust_api") (v "0.20.0") (d (list (d (n "crossterm") (r "^0.20.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "rscript") (r "^0.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)))) (h "0a123f3ms20jzv1iskbsjx2kh08v6qyhm6318hz3rmqm7r0z77qw")))

(define-public crate-irust_api-0.21.0 (c (n "irust_api") (v "0.21.0") (d (list (d (n "crossterm") (r "^0.20.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "rscript") (r "^0.12.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)))) (h "0my4bpyfb9xkr8v9kgvhiwdjyqgqg1fh5ivlhnacgd8rcrv0y9wi")))

(define-public crate-irust_api-0.21.1 (c (n "irust_api") (v "0.21.1") (d (list (d (n "crossterm") (r "^0.22.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "rscript") (r "^0.12.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)))) (h "0q3ma8jv328b1x36zv9hxd93gjy75l1h6lc5qppjj0i8gqb81ij3")))

(define-public crate-irust_api-0.22.0 (c (n "irust_api") (v "0.22.0") (d (list (d (n "crossterm") (r "^0.22.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "rscript") (r "^0.13.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)))) (h "06836fv7148xg6nhxh87xbp843pnv5cilxarkjpsiyhhqdvsl9dn")))

(define-public crate-irust_api-0.23.0 (c (n "irust_api") (v "0.23.0") (d (list (d (n "crossterm") (r "^0.22.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "rscript") (r "^0.14.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)))) (h "1zd0q5qzvrqksbv0jpf7imrjy3nj8r4m6yx0igdnq2il1k1w0gsl")))

(define-public crate-irust_api-0.24.0 (c (n "irust_api") (v "0.24.0") (d (list (d (n "crossterm") (r "^0.22.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "rscript") (r "^0.14.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)))) (h "1d8bp5z5ya9wlhqlayjlb8dszxndrd9wkpv978was8jxw13lbl19")))

(define-public crate-irust_api-0.25.0 (c (n "irust_api") (v "0.25.0") (d (list (d (n "crossterm") (r "^0.22.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "rscript") (r "^0.14.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)))) (h "0b117441msdnsi4j3a7cs42f03g5hm9zd12nglnifkld5bdzn8pb")))

(define-public crate-irust_api-0.26.0 (c (n "irust_api") (v "0.26.0") (d (list (d (n "crossterm") (r "^0.22.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "rscript") (r "^0.14.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)))) (h "1g17hvrf6nyq3bnab4ghvpiy2l94s98v61isrs95sa5ym32azp2p")))

(define-public crate-irust_api-0.27.0 (c (n "irust_api") (v "0.27.0") (d (list (d (n "crossterm") (r "^0.22.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "rscript") (r "^0.14.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)))) (h "13bnd85hr3460as1j2pdc4jrvx4r22kvi1m4ylixp6m8ac58x569")))

(define-public crate-irust_api-0.28.0 (c (n "irust_api") (v "0.28.0") (d (list (d (n "crossterm") (r "^0.22.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "rscript") (r "^0.15.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)))) (h "1yir6yy8xjysywsjgg702dabhgimcjmkd9zgdry8z481srv3q9zd")))

(define-public crate-irust_api-0.29.0 (c (n "irust_api") (v "0.29.0") (d (list (d (n "crossterm") (r "^0.22.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "rscript") (r "^0.15.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)))) (h "0h00hbpw7m3lqjhnfnzsjpn3072kbmrxpp82yyyff5k6d3irh8lh")))

(define-public crate-irust_api-0.30.0 (c (n "irust_api") (v "0.30.0") (d (list (d (n "crossterm") (r "^0.22.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "rscript") (r "^0.17.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)))) (h "0n2zbgmvqkhjkj0q1h2fzq6ww8bsd6aj5mpwsfcq6gz4vz10l72v")))

(define-public crate-irust_api-0.30.1 (c (n "irust_api") (v "0.30.1") (d (list (d (n "crossterm") (r "^0.23.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "rscript") (r "^0.17.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)))) (h "12xc8g4q8p0nd5f9m17xfkhhcj5zak51fvrkd4ck5wknqng2yfsf")))

(define-public crate-irust_api-0.30.2 (c (n "irust_api") (v "0.30.2") (d (list (d (n "crossterm") (r "^0.24.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "rscript") (r "^0.17.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)))) (h "18ll90w2vqhz61rd7z5p11vrk4k6bskysxjr61f6521600ra3sxd")))

(define-public crate-irust_api-0.31.0 (c (n "irust_api") (v "0.31.0") (d (list (d (n "crossterm") (r "^0.25.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "rscript") (r "^0.17.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)))) (h "1177z8wc47c14pww40wryrlqcnh3a6nbzn397l2vf2mg69ph6kjf")))

(define-public crate-irust_api-0.31.1 (c (n "irust_api") (v "0.31.1") (d (list (d (n "crossterm") (r "^0.25.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "rscript") (r "^0.17.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)))) (h "178x5cwi0g1g9k8qi27bqi359bdj1v4n2bba9s0wgd8zcy20xw1m")))

(define-public crate-irust_api-0.31.2 (c (n "irust_api") (v "0.31.2") (d (list (d (n "crossterm") (r "^0.25.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "rscript") (r "^0.17.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)))) (h "1a9p4anyvf8qngzdfjabr48cphjg1f3i55iznak5ii80nj1rxlh7")))

(define-public crate-irust_api-0.31.3 (c (n "irust_api") (v "0.31.3") (d (list (d (n "crossterm") (r "^0.26.0") (f (quote ("serde" "use-dev-tty"))) (d #t) (k 0)) (d (n "rscript") (r "^0.17.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)))) (h "02fg8blsqpxksafqlhhvadjd678jc0i3qg3vii51ql6jcmx8hcyc")))

(define-public crate-irust_api-0.31.4 (c (n "irust_api") (v "0.31.4") (d (list (d (n "crossterm") (r "^0.26.1") (f (quote ("serde" "use-dev-tty"))) (d #t) (k 0)) (d (n "rscript") (r "^0.17.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)))) (h "0bbfvxhvay69bnyncnl7x7i05k3f6qfm4y9hbny0h5dfwg02g7i8")))

(define-public crate-irust_api-0.31.5 (c (n "irust_api") (v "0.31.5") (d (list (d (n "crossterm") (r "^0.26.1") (f (quote ("serde" "use-dev-tty"))) (d #t) (k 0)) (d (n "rscript") (r "^0.17.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)))) (h "0njif7qn5l5jfd4z35d49c7r0yds7ziqapi3jzw1rahvf5jjy3dr")))

(define-public crate-irust_api-0.31.6 (c (n "irust_api") (v "0.31.6") (d (list (d (n "crossterm") (r "^0.27.0") (f (quote ("serde" "use-dev-tty"))) (d #t) (k 0)) (d (n "rscript") (r "^0.17.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)))) (h "11bas7q2nafcpf0xb37z1h0phcmhycjqxpninvxvb1fka1snrxwp")))

(define-public crate-irust_api-0.31.7 (c (n "irust_api") (v "0.31.7") (d (list (d (n "crossterm") (r "^0.27.0") (f (quote ("serde" "use-dev-tty"))) (d #t) (k 0)) (d (n "rscript") (r "^0.17.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)))) (h "12mypch0p9vj854hix22yphp6i0r9vw4gk2x3pigznz53vnjk0yc")))

(define-public crate-irust_api-0.31.8 (c (n "irust_api") (v "0.31.8") (d (list (d (n "crossterm") (r "^0.27.0") (f (quote ("serde" "use-dev-tty"))) (d #t) (k 0)) (d (n "rscript") (r "^0.17.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)))) (h "1w932v6qvqihi0w05r4s9s2ng2nzdis71ldc8f30bb7953a21zgz")))

