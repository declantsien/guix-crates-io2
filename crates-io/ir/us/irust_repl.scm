(define-module (crates-io ir us irust_repl) #:use-module (crates-io))

(define-public crate-irust_repl-0.1.0 (c (n "irust_repl") (v "0.1.0") (d (list (d (n "once_cell") (r "^1.7.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0jch6mi1r7npxqxyiigas8vqi00lmymz71y9i2s4ak5sicsa9qb8")))

(define-public crate-irust_repl-0.1.1 (c (n "irust_repl") (v "0.1.1") (d (list (d (n "once_cell") (r "^1.7.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "100p6kz1d5r10mfv4faj80kvcl4zsz6x8gk88p3yjpgnvymbyg44")))

(define-public crate-irust_repl-0.1.2 (c (n "irust_repl") (v "0.1.2") (d (list (d (n "once_cell") (r "^1.7.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "099rmy0g9dmwmvy96j52cgjlx8n1cv51g1vhps01ax669i12syqr")))

(define-public crate-irust_repl-0.2.0 (c (n "irust_repl") (v "0.2.0") (d (list (d (n "once_cell") (r "^1.7.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "08hxjqjlfd9mzc5f49gglgk8r7np76zm1xd6dvym2zxln4b3ix8j")))

(define-public crate-irust_repl-0.2.1 (c (n "irust_repl") (v "0.2.1") (d (list (d (n "once_cell") (r "^1.7.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1b76c84k4mlc7i5r69s1vwnr3r549im93p7af9llg77899r4bchj")))

(define-public crate-irust_repl-0.3.0 (c (n "irust_repl") (v "0.3.0") (d (list (d (n "once_cell") (r "^1.7.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1szss5qcbnf35hagqgf8klqq3n4l1r3hv0qdwk6qr6mcn2j8dy5a")))

(define-public crate-irust_repl-0.4.0 (c (n "irust_repl") (v "0.4.0") (d (list (d (n "once_cell") (r "^1.7.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0cgmqwkbznv3namv57zchra6bpqxpc8843id8fpf70hbwv2b46nz")))

(define-public crate-irust_repl-0.5.0 (c (n "irust_repl") (v "0.5.0") (d (list (d (n "once_cell") (r "^1.7.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1v13vic5ay428ifxlya4n6ip30zvb0h9nskil3qsl3382csrsa6j")))

(define-public crate-irust_repl-0.6.0 (c (n "irust_repl") (v "0.6.0") (d (list (d (n "once_cell") (r "^1.7.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "08ny9np6lg7zyjpkli1fd7h4vwa220d8pwkikzvkq638nrzisyi6")))

(define-public crate-irust_repl-0.7.0 (c (n "irust_repl") (v "0.7.0") (d (list (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "072hn4wnbghidrlc35igxf8ww8z285aypnqxjjgnah1nqkx486nw")))

(define-public crate-irust_repl-0.7.1 (c (n "irust_repl") (v "0.7.1") (d (list (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "06r1a5j34ra9jgxb6ll0vj2bd5v7mqnhyqkyahfy99c18hm5xvp7")))

(define-public crate-irust_repl-0.8.0 (c (n "irust_repl") (v "0.8.0") (d (list (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "17fmfcg031jdq9m049m53g87fgfysxy5m5x8j70rc85wjjj609s8")))

(define-public crate-irust_repl-0.9.0 (c (n "irust_repl") (v "0.9.0") (d (list (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0j00kl93s3qj3sbbv124ldh51fkps9inh4sfyhxnxbw61jf261xx")))

(define-public crate-irust_repl-0.10.0 (c (n "irust_repl") (v "0.10.0") (d (list (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1ri5a0z3ds7w36hqa3yr6cl2101cw1gsgaqzxf3kyl7vg4lhj6rs")))

(define-public crate-irust_repl-0.11.0 (c (n "irust_repl") (v "0.11.0") (d (list (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1zsa77wj6k2kiv76hcrn05gjqvxq05lh8qcwxgf7hf4aw5i88bvy")))

(define-public crate-irust_repl-0.12.0 (c (n "irust_repl") (v "0.12.0") (d (list (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0l1ixahff5m1hy3xrfmilr24pipl2ig8fc9n0h64p9xc7dbyp8ks")))

(define-public crate-irust_repl-0.13.0 (c (n "irust_repl") (v "0.13.0") (d (list (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0cagvff0kqsf8550wfsk720cr8pcny4i0fxqkywnx9c0ww1s5dz8")))

(define-public crate-irust_repl-0.14.0 (c (n "irust_repl") (v "0.14.0") (d (list (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "05scvx56xj7fdcskyxn1ad63i1gwf28hfy9wfqc6bhzj7ps6qqwv")))

(define-public crate-irust_repl-0.15.0 (c (n "irust_repl") (v "0.15.0") (d (list (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1cjn256pv7n71barr8qahhalm1vs5qq39pqkliqf5psfj2435dzf")))

(define-public crate-irust_repl-0.16.0 (c (n "irust_repl") (v "0.16.0") (d (list (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "120a36n1h3i6dnam2gnciz4057bihbj7avmimw3x3fmp27zr6k5l")))

(define-public crate-irust_repl-0.17.0 (c (n "irust_repl") (v "0.17.0") (d (list (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0bnjns9j2zs02b53sg3y80bzv0jf2r3r2k7fplpmhxvyd1dwwc3r")))

(define-public crate-irust_repl-0.18.0 (c (n "irust_repl") (v "0.18.0") (d (list (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "08zxgyagj0iiimw7dc7kad4mm1dfa9fzi9rgj4zzlwqp2683dsda")))

(define-public crate-irust_repl-0.19.0 (c (n "irust_repl") (v "0.19.0") (d (list (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0axzxbj91nz84fm26sjrxkdk0qix91r3gbk7j52a8sdrhasj35pi")))

(define-public crate-irust_repl-0.19.1 (c (n "irust_repl") (v "0.19.1") (d (list (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0p1ypqys44x2mmvdxqm74mzf6bzpv5ps759vqwplak8hppx68jb7")))

(define-public crate-irust_repl-0.19.2 (c (n "irust_repl") (v "0.19.2") (d (list (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1jh26pbjb2y54l5mby187sb20waw57xqdw1z6wk2i64c5w7fqy7r")))

(define-public crate-irust_repl-0.19.3 (c (n "irust_repl") (v "0.19.3") (d (list (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "044zq09b0gfdxywm1dd8pql095f7c3qn5hzxnvcn1x2sdcl9pgdw")))

(define-public crate-irust_repl-0.19.4 (c (n "irust_repl") (v "0.19.4") (d (list (d (n "once_cell") (r "^1.15.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0bg00npdj1c09dynqxlgkg78w6gi4c560b3ljyi5dignxff88agl")))

(define-public crate-irust_repl-0.20.0 (c (n "irust_repl") (v "0.20.0") (d (list (d (n "once_cell") (r "^1.16.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1gfrxcrs353g9dm411m1jix5vvh1804m455ik2ql29jmd8rn1y1l")))

(define-public crate-irust_repl-0.20.1 (c (n "irust_repl") (v "0.20.1") (d (list (d (n "once_cell") (r "^1.17.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1g3d4s2y3ahzh340n7l8n9hs6pxlkhc2pby48wd0zkqkcg08ilkz")))

(define-public crate-irust_repl-0.20.2 (c (n "irust_repl") (v "0.20.2") (d (list (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0p86ddgp9ky5casg1wkxm86c0f4fp8286lbzk8i34h7kjkfxa7sq")))

(define-public crate-irust_repl-0.20.3 (c (n "irust_repl") (v "0.20.3") (d (list (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "15xdd0cjw255yfan8iy8vvj2c1zb2d49vhframizsq5vkqlq1d7m")))

(define-public crate-irust_repl-0.21.0 (c (n "irust_repl") (v "0.21.0") (d (list (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0hids2vmspi042fdxx6hfjny4krkrgqw47blaq3v13prhfw6h400")))

(define-public crate-irust_repl-0.22.0 (c (n "irust_repl") (v "0.22.0") (d (list (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "uuid") (r "^1.3.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "0zhhk7zl2rs93k8nglj317vx2dj1afci8pinx091l2plyc7fdavp")))

(define-public crate-irust_repl-0.23.0 (c (n "irust_repl") (v "0.23.0") (d (list (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 2)) (d (n "uuid") (r "^1.3.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "05zpigh22f1sayzwviaim2jgi2z878kfjshvq9038703rqfdd7lq")))

(define-public crate-irust_repl-0.24.0 (c (n "irust_repl") (v "0.24.0") (d (list (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 2)) (d (n "uuid") (r "^1.3.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "0h2as2dglj0x2q0pml27gvij2ncz2ibfdilw0q6f391xh6cv1cyi")))

(define-public crate-irust_repl-0.24.1 (c (n "irust_repl") (v "0.24.1") (d (list (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 2)) (d (n "uuid") (r "^1.4.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "1s3lvql89z2813vm51axvrrv38d773divjv4ddbkd6yjv25sqcw9")))

(define-public crate-irust_repl-0.24.2 (c (n "irust_repl") (v "0.24.2") (d (list (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 2)) (d (n "uuid") (r "^1.4.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "09xhlnmjb27pic2ym5ba0xx1cm4mm7ahra5bkjrjmp5k2r9ihcjm")))

(define-public crate-irust_repl-0.24.3 (c (n "irust_repl") (v "0.24.3") (d (list (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 2)) (d (n "uuid") (r "^1.4.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "0zymd6jmyq9mb6xqra33yd74hvkmva1szyabkvmz5dvv6kipcvi1")))

(define-public crate-irust_repl-0.24.5 (c (n "irust_repl") (v "0.24.5") (d (list (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 2)) (d (n "uuid") (r "^1.4.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "063qi21g26bvjhcvxr00l2qs3z2w222x3gci9d4qvsrx3g1njz6n")))

(define-public crate-irust_repl-0.24.6 (c (n "irust_repl") (v "0.24.6") (d (list (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 2)) (d (n "uuid") (r "^1.4.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "1acah28qx8lvzm9pnbaiiqp5gc580d4k3hmbrpgb8nnrsb1qvp50")))

