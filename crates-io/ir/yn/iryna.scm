(define-module (crates-io ir yn iryna) #:use-module (crates-io))

(define-public crate-iryna-0.1.0 (c (n "iryna") (v "0.1.0") (d (list (d (n "concurrent-hashmap") (r "^0.2.1") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)))) (h "09b8c1a5h6gpy63j64bpdvzncayz43n7r8afyivp7ngm5h97ylx0") (y #t)))

(define-public crate-iryna-0.1.1 (c (n "iryna") (v "0.1.1") (d (list (d (n "concurrent-hashmap") (r "^0.2.1") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)))) (h "1qzcz5nkx8wwi88p3452c2dkwklk4z2mc3fwxg35cxipa923cf0y") (y #t)))

(define-public crate-iryna-0.1.2 (c (n "iryna") (v "0.1.2") (d (list (d (n "concurrent-hashmap") (r "^0.2.1") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)))) (h "1wxlv60g1wxwf3s23734h4xra0n71ngv26fk5zwqnh9y0ni12v5h") (y #t)))

(define-public crate-iryna-0.1.3 (c (n "iryna") (v "0.1.3") (d (list (d (n "concurrent-hashmap") (r "^0.2.1") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)))) (h "10vicjp2lhk7d9q1x3gl0yrjagvmn2h8z257662z5vjxlahpa70w") (y #t)))

(define-public crate-iryna-0.1.4 (c (n "iryna") (v "0.1.4") (d (list (d (n "chashmap") (r "^2.2.0") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)))) (h "1k9d5fmm0z2qaj86m9ix8agdcc3fcxjr5rcz80764yzaw2f3gigc")))

