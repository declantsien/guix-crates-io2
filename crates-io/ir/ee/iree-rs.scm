(define-module (crates-io ir ee iree-rs) #:use-module (crates-io))

(define-public crate-iree-rs-0.1.0 (c (n "iree-rs") (v "0.1.0") (d (list (d (n "iree-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.93") (d #t) (k 2)))) (h "0fnzmqpcjanwcriwkwyzsvw14nq6bs2awrzw194j1spbaf1dbq1x")))

(define-public crate-iree-rs-0.1.1 (c (n "iree-rs") (v "0.1.1") (d (list (d (n "iree-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.93") (d #t) (k 2)))) (h "1wmfppd3pdga0wxq6hkxbzqrpxb9sq7cps0q46mxjmdx0zwkcqdw")))

