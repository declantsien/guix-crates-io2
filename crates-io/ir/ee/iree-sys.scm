(define-module (crates-io ir ee iree-sys) #:use-module (crates-io))

(define-public crate-iree-sys-0.1.0 (c (n "iree-sys") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "flatbuffers") (r "^23.1.21") (f (quote ("serde"))) (d #t) (k 2)) (d (n "once_cell") (r "^1.17.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.93") (d #t) (k 2)) (d (n "tch") (r "^0.10.1") (d #t) (k 2)) (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1.49") (d #t) (k 1)) (d (n "git2") (r "^0.16.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.26") (d #t) (k 1)))) (h "1sc9mjq1zywxwn3yqcjdsfkb9vps14iwvhdgcasylfdav10dq1g4") (l "iree")))

