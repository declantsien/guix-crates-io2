(define-module (crates-io ir eq ireq) #:use-module (crates-io))

(define-public crate-ireq-0.1.2 (c (n "ireq") (v "0.1.2") (d (list (d (n "reqwest") (r "^0.11.23") (f (quote ("http3"))) (d #t) (k 0)) (d (n "static_init") (r "^1.0.3") (d #t) (k 0)))) (h "1j1kvs6nrk5hn8b3iza4srshwd5wdsmjwqdm0n9m5g5kc1lhinha")))

(define-public crate-ireq-0.1.3 (c (n "ireq") (v "0.1.3") (d (list (d (n "aok") (r "^0.1.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.23") (f (quote ("http3"))) (d #t) (k 0)) (d (n "static_init") (r "^1.0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "1g79mnj0056gp0c986lgbqqqv9grj6ikp3w1vhinn7zpvsj60xzg")))

(define-public crate-ireq-0.1.4 (c (n "ireq") (v "0.1.4") (d (list (d (n "aok") (r "^0.1.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.23") (f (quote ("http3"))) (d #t) (k 0)) (d (n "static_init") (r "^1.0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "0hkrfjl7kkavn1zmm567g3f7r6d0p1z7pzdf4p518sxp0p53rm9d")))

(define-public crate-ireq-0.1.5 (c (n "ireq") (v "0.1.5") (d (list (d (n "aok") (r "^0.1.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.23") (f (quote ("http3"))) (d #t) (k 0)) (d (n "static_init") (r "^1.0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "1qdxfrb51nqw7v3vbhqwr3jzs34m2b7gv945as4r6hd84v9l7q7r")))

(define-public crate-ireq-0.1.6 (c (n "ireq") (v "0.1.6") (d (list (d (n "aok") (r "^0.1.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.23") (f (quote ("http3"))) (d #t) (k 0)) (d (n "static_init") (r "^1.0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "0la2pybx8qz4gzjbqhlrigzqhyck0p8a20mk7n5pji3y8mrjwvdi")))

(define-public crate-ireq-0.1.7 (c (n "ireq") (v "0.1.7") (d (list (d (n "aok") (r "^0.1.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.23") (f (quote ("http3"))) (d #t) (k 0)) (d (n "static_init") (r "^1.0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("macros" "rt" "rt-multi-thread" "time" "sync"))) (d #t) (k 0)))) (h "1zp7cy3gzihghvq3abz9m2ccjsb20gan9cm1y82wwsaadql1zx8m")))

(define-public crate-ireq-0.1.8 (c (n "ireq") (v "0.1.8") (d (list (d (n "aok") (r "^0.1.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.12.3") (f (quote ("h3" "rustls-tls" "rustls-tls-native-roots" "brotli" "stream"))) (d #t) (k 0)) (d (n "static_init") (r "^1.0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("macros" "rt" "rt-multi-thread" "time" "sync"))) (d #t) (k 0)))) (h "0n6589n83jmlc3pirmrrjj74jrf8yisq9qivb4ldbqcx6k99ici3")))

(define-public crate-ireq-0.1.9 (c (n "ireq") (v "0.1.9") (d (list (d (n "aok") (r "^0.1.11") (d #t) (k 0)) (d (n "reqwest") (r "^0.12.4") (f (quote ("h3" "rustls-tls" "rustls-tls-native-roots" "brotli" "stream"))) (d #t) (k 0)) (d (n "static_init") (r "^1.0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.59") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("macros" "rt" "rt-multi-thread" "time" "sync"))) (d #t) (k 0)))) (h "00wq3963myw6s3p0asgh8cb2mz9f1qdzinmnasy6lq88s65d40bq")))

(define-public crate-ireq-0.1.10 (c (n "ireq") (v "0.1.10") (d (list (d (n "aok") (r "^0.1.11") (d #t) (k 0)) (d (n "reqwest") (r "^0.12.4") (f (quote ("h3" "rustls-tls" "rustls-tls-native-roots" "brotli" "stream"))) (d #t) (k 0)) (d (n "static_init") (r "^1.0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.59") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("macros" "rt" "rt-multi-thread" "time" "sync"))) (d #t) (k 0)))) (h "03jrzz7c024nb6vks63vrnri7wzl0c1g4lqjhqim5kafa90fw9bb")))

(define-public crate-ireq-0.1.11 (c (n "ireq") (v "0.1.11") (d (list (d (n "aok") (r "^0.1.11") (d #t) (k 0)) (d (n "reqwest") (r "^0.12.4") (f (quote ("h3" "rustls-tls" "rustls-tls-native-roots" "brotli" "zstd" "stream"))) (d #t) (k 0)) (d (n "static_init") (r "^1.0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.59") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("macros" "rt" "rt-multi-thread" "time" "sync"))) (d #t) (k 0)))) (h "1558q9kc2q6h7hcbhkpd8p3l7cas0pi5qfrn8rsrbj0xvfjaqbwl")))

(define-public crate-ireq-0.1.12 (c (n "ireq") (v "0.1.12") (d (list (d (n "aok") (r "^0.1.11") (d #t) (k 0)) (d (n "reqwest") (r "^0.12.4") (f (quote ("rustls-tls" "rustls-tls-native-roots" "brotli" "zstd" "stream"))) (d #t) (k 0)) (d (n "static_init") (r "^1.0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.59") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("macros" "rt" "rt-multi-thread" "time" "sync"))) (d #t) (k 0)))) (h "1ka35lgxwv73bx1pr0x8f8p7k4pcha89h8frgxs9k86fyw421y0i") (f (quote (("proxy" "reqwest/socks") ("default"))))))

