(define-module (crates-io ir mi irmin) #:use-module (crates-io))

(define-public crate-irmin-0.3.1-beta.0 (c (n "irmin") (v "0.3.1-beta.0") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1b1zwhy0vq2zbbnbgxml20ivs41qgzl5fni9jm4jvlc833fbj93v") (f (quote (("docs"))))))

(define-public crate-irmin-0.3.3 (c (n "irmin") (v "0.3.3") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1lishhg4hsqvs1am5syq7qqd40lf8c5n8ha7wp7x6kwc0yvmzm7f") (f (quote (("docs"))))))

