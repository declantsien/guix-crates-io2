(define-module (crates-io ir re irrefutable) #:use-module (crates-io))

(define-public crate-irrefutable-0.1.0 (c (n "irrefutable") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.10") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.17") (f (quote ("full"))) (d #t) (k 0)))) (h "1xyxvykiap7bllsssb7vg2lgyyzg1mxg7yyimjl5sir3yzxxf2pa")))

(define-public crate-irrefutable-0.1.1 (c (n "irrefutable") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.10") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.17") (f (quote ("full"))) (d #t) (k 0)))) (h "0kq6bndbw07sswf390c7x37yy0qnq0r5p4nd32ihi0x40cvj48j1")))

(define-public crate-irrefutable-0.1.2 (c (n "irrefutable") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.10") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.18") (f (quote ("full"))) (d #t) (k 0)))) (h "1ja67bkfz3h29qpx63s97qgmpwij46n3zlh5r1bm921mpnmp4pl4")))

(define-public crate-irrefutable-0.1.3 (c (n "irrefutable") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0.18") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.30") (f (quote ("full"))) (d #t) (k 0)))) (h "0zsh3y5p2ygzms26gvsmc38fddxrl4l0b30y05si6jxyi69f6f8x")))

