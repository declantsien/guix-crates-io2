(define-module (crates-io ir c- irc-rust) #:use-module (crates-io))

(define-public crate-irc-rust-0.1.0 (c (n "irc-rust") (v "0.1.0") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "0k4yxq9yfkb9r0zk1kr6lk8kr5caxlxgcar79gc3w9dmdni107xr")))

(define-public crate-irc-rust-0.1.1 (c (n "irc-rust") (v "0.1.1") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "0hz9pwwyhhm5c2bb9c0dvbq6msgb82jx45qjj0ya2gxixccl6niz")))

(define-public crate-irc-rust-0.1.2 (c (n "irc-rust") (v "0.1.2") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "13g5cy627lridg04wdznzdsds9mrs5jhqdrm7lcz09kgacsx399c")))

(define-public crate-irc-rust-0.1.3 (c (n "irc-rust") (v "0.1.3") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "16hi1x0x3b0f6w5bsqfmnwv79si2j7h6g0032wfpy3j9sc67x71z")))

(define-public crate-irc-rust-0.1.4 (c (n "irc-rust") (v "0.1.4") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "04d5gzkx8f98g1p9727fahhjffx0i78hpgmf7fax75zn8yhzjqpn")))

(define-public crate-irc-rust-0.1.5 (c (n "irc-rust") (v "0.1.5") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "14rb9y20frsshz3baxgndr0fkn59limcs7r3j58kl8j38z95344r")))

(define-public crate-irc-rust-0.1.6 (c (n "irc-rust") (v "0.1.6") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "0kgsi8xl6ivg4jhyl19p059d591ai1r72ajw2w723a4nb0kljpxa")))

(define-public crate-irc-rust-0.2.0 (c (n "irc-rust") (v "0.2.0") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "serde") (r "^1.0.111") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.53") (d #t) (k 2)))) (h "00nl7hxk4asm4khxi36z2fgx51m0cl1ldam0scvf3w7r0avd33nf")))

(define-public crate-irc-rust-0.2.1 (c (n "irc-rust") (v "0.2.1") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "serde") (r "^1.0.111") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.53") (d #t) (k 2)))) (h "1qg8s6h0lairqz03z8kc2pmddmp59i10i8v6ay1gs7b1vxlqad4q")))

(define-public crate-irc-rust-0.3.0 (c (n "irc-rust") (v "0.3.0") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "serde") (r "^1.0.111") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.53") (d #t) (k 2)))) (h "0axj89cnr9kjax991krh7qcq0h8i115cydf9p8842ifzn1xk0rbi")))

(define-public crate-irc-rust-0.3.1 (c (n "irc-rust") (v "0.3.1") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "serde") (r "^1.0.111") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.53") (d #t) (k 2)))) (h "17v907zmprd6rlmbd45jd5sdc4f097rhviv0dp50dvdjcrhx1lz7")))

(define-public crate-irc-rust-0.3.2 (c (n "irc-rust") (v "0.3.2") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "serde") (r "^1.0.111") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.53") (d #t) (k 2)))) (h "1kzx88rasqbna9mcjyrfg3fi4i51llh7s6z9hfvrbijxzgvb3cic")))

(define-public crate-irc-rust-0.3.3 (c (n "irc-rust") (v "0.3.3") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "serde") (r "^1.0.111") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.53") (d #t) (k 2)))) (h "1zfds1l88vc3n70rvn44qz1376xfqsq0y9iyi6b2flnaixfcq397")))

(define-public crate-irc-rust-0.4.0 (c (n "irc-rust") (v "0.4.0") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "serde") (r "^1.0.111") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.53") (d #t) (k 2)))) (h "1fkgfh8zvnniln9aaa0hplq9d1ydsx8w2cr36b825yfjx1gbfc0s")))

