(define-module (crates-io ir c- irc-proto) #:use-module (crates-io))

(define-public crate-irc-proto-0.0.0 (c (n "irc-proto") (v "0.0.0") (h "1sy14zaxnfldrwc99yqynb21s5nwzdzb67yk067jqs1nhb5zrmm3")))

(define-public crate-irc-proto-0.14.0 (c (n "irc-proto") (v "0.14.0") (d (list (d (n "bytes") (r "^0.5.0") (o #t) (d #t) (k 0)) (d (n "encoding") (r "^0.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "tokio-util") (r "^0.3.0") (f (quote ("codec"))) (o #t) (d #t) (k 0)))) (h "0fg003hr73jjvnf5j65anyc28mm2a5shkhkzyfm8brxbafkccns1") (f (quote (("default" "bytes" "tokio" "tokio-util"))))))

(define-public crate-irc-proto-0.15.0 (c (n "irc-proto") (v "0.15.0") (d (list (d (n "bytes") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "encoding") (r "^0.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "tokio-util") (r "^0.6.0") (f (quote ("codec"))) (o #t) (d #t) (k 0)))) (h "0hrnlcglkwq336dqvrkp1w0flni4j0w51dx5msl9pr95v190myjm") (f (quote (("default" "bytes" "tokio" "tokio-util"))))))

(define-public crate-irc-proto-1.0.0 (c (n "irc-proto") (v "1.0.0") (d (list (d (n "bytes") (r "^1.4.0") (o #t) (d #t) (k 0)) (d (n "encoding") (r "^0.2.33") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "tokio") (r "^1.27.0") (o #t) (d #t) (k 0)) (d (n "tokio-util") (r "^0.7.7") (f (quote ("codec"))) (o #t) (d #t) (k 0)))) (h "1qfrsjpsc47wwx478f3ycy3ph195yv89276y665rx6031kxwgdbj") (f (quote (("default" "bytes" "tokio" "tokio-util")))) (r "1.60")))

