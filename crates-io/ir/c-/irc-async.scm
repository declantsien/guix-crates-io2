(define-module (crates-io ir c- irc-async) #:use-module (crates-io))

(define-public crate-irc-async-0.1.0 (c (n "irc-async") (v "0.1.0") (d (list (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "native-tls") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("tcp" "macros"))) (d #t) (k 0)) (d (n "tokio-tls") (r "^0.3") (d #t) (k 0)) (d (n "tokio-util") (r "^0.2") (f (quote ("codec"))) (d #t) (k 0)))) (h "10w7si3npq1dzqdh0kng20rrrqvjs2m4z2c34d3rdyh4kzrqp86r")))

