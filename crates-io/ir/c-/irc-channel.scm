(define-module (crates-io ir c- irc-channel) #:use-module (crates-io))

(define-public crate-irc-channel-0.1.0 (c (n "irc-channel") (v "0.1.0") (d (list (d (n "irc_message") (r "*") (d #t) (k 0)))) (h "0n28gyriwzyw10p968f8qyv2d0j7qbgr4i62kx5hzxvlw12ajfas")))

(define-public crate-irc-channel-0.1.1 (c (n "irc-channel") (v "0.1.1") (d (list (d (n "irc_message") (r "*") (d #t) (k 0)) (d (n "many2many") (r "*") (d #t) (k 0)))) (h "10ib5smnbn493psx16hbr3qz2mvkjxrj0sdgn0xk0cgqg259z0xk")))

