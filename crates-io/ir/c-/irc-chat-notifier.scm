(define-module (crates-io ir c- irc-chat-notifier) #:use-module (crates-io))

(define-public crate-irc-chat-notifier-0.1.0 (c (n "irc-chat-notifier") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "futures") (r "^0.3.4") (d #t) (k 0)) (d (n "irc") (r "^0.13.6") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "notifica") (r "^1.0.2") (d #t) (k 0)) (d (n "tokio") (r "^0.2.19") (d #t) (k 0)))) (h "14w7574pnqfxrpzafam8vn8wi639ksr8zjbmippph87ymi1vscyd")))

