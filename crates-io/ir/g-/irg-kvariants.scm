(define-module (crates-io ir g- irg-kvariants) #:use-module (crates-io))

(define-public crate-irg-kvariants-0.1.0 (c (n "irg-kvariants") (v "0.1.0") (d (list (d (n "csv") (r "^1.1.6") (d #t) (k 0)) (d (n "once_cell") (r "^1.16.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.148") (f (quote ("derive"))) (d #t) (k 0)) (d (n "csv") (r "^1.1.6") (d #t) (k 1)) (d (n "serde") (r "^1.0.148") (f (quote ("derive"))) (d #t) (k 1)))) (h "0y6z9w8884hvnf67fa8h28yfwrc8h0kb94zwvgwrqqk3hcli8cn7")))

