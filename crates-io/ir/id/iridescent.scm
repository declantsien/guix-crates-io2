(define-module (crates-io ir id iridescent) #:use-module (crates-io))

(define-public crate-iridescent-0.1.0 (c (n "iridescent") (v "0.1.0") (d (list (d (n "rand") (r "^0.8") (o #t) (d #t) (k 0)))) (h "0wm2wkqs587kbwbv70r4n9bdl2lpqa6zqhp8cbym7ihvbjm3273c") (f (quote (("random" "rand") ("default"))))))

(define-public crate-iridescent-0.1.1 (c (n "iridescent") (v "0.1.1") (d (list (d (n "rand") (r "^0.8") (o #t) (d #t) (k 0)))) (h "162v7jjmnc9jxphka4s30s8h3gzlgv605bf6m9xlp7icxsqa0f2x") (f (quote (("random" "rand") ("default"))))))

(define-public crate-iridescent-0.2.0 (c (n "iridescent") (v "0.2.0") (d (list (d (n "rand") (r "^0.8") (o #t) (d #t) (k 0)))) (h "0nyklbp0p17wbqxdqhl51kh6rq6z4darpffzjdpsdf7jvkyv8rn2") (f (quote (("random" "rand") ("default"))))))

(define-public crate-iridescent-0.2.1 (c (n "iridescent") (v "0.2.1") (d (list (d (n "rand") (r "^0.8") (o #t) (d #t) (k 0)))) (h "1ka81b750f3j5sfx6w0p1qqh33h77vmnma5nh6g03qc6k5gs8kjc") (f (quote (("random" "rand") ("default"))))))

