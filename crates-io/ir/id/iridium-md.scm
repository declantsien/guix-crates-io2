(define-module (crates-io ir id iridium-md) #:use-module (crates-io))

(define-public crate-iridium-md-0.1.0 (c (n "iridium-md") (v "0.1.0") (d (list (d (n "clap") (r ">=2.33.3, <3.0.0") (d #t) (k 0)) (d (n "comrak") (r ">=0.8.2, <0.9.0") (d #t) (k 0)) (d (n "gitignored") (r ">=0.4.0, <0.5.0") (d #t) (k 0)) (d (n "regex") (r ">=1.4.1, <2.0.0") (d #t) (k 0)) (d (n "scraper") (r ">=0.12.0, <0.13.0") (d #t) (k 0)))) (h "094pskhszf7n8mkf1fvnhblwg2wxqkzqjrxzijwi3b6xs38jf6fi")))

