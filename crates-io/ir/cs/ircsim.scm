(define-module (crates-io ir cs ircsim) #:use-module (crates-io))

(define-public crate-ircsim-0.1.0 (c (n "ircsim") (v "0.1.0") (d (list (d (n "clearscreen") (r "^1.0.10") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "crossterm") (r "^0.23.2") (d #t) (k 0)))) (h "0ibiv6ir92f7v897ywyavvh7cz6j410ayprfvhlh90xq069ppbfb")))

(define-public crate-ircsim-0.1.1 (c (n "ircsim") (v "0.1.1") (d (list (d (n "clearscreen") (r "^1.0.10") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "crossterm") (r "^0.23.2") (d #t) (k 0)))) (h "1gc34l76xlnqq3xrfmbvrry3715gs3p84q4k6dmqwsp869idivnz")))

(define-public crate-ircsim-0.1.2 (c (n "ircsim") (v "0.1.2") (d (list (d (n "clearscreen") (r "^1.0.10") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "crossterm") (r "^0.23.2") (d #t) (k 0)))) (h "0wd8b8a9zrj31zbvwwg38gfhv9rxhrky1ffvjjy74hgz9xi9pnq0")))

(define-public crate-ircsim-0.1.3 (c (n "ircsim") (v "0.1.3") (d (list (d (n "clearscreen") (r "^1.0.10") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "crossterm") (r "^0.23.2") (d #t) (k 0)))) (h "0fl9a46s7n88015mpc9gmy7ahwpz9nhwmpdkss6nqz126ba3cxfi")))

(define-public crate-ircsim-0.1.4 (c (n "ircsim") (v "0.1.4") (d (list (d (n "clearscreen") (r "^1.0.10") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "crossterm") (r "^0.23.2") (d #t) (k 0)))) (h "0mk47yfn517apnh4d0zd52m7i0h3ayyb2g2d63jhy2hli87iyzfd")))

(define-public crate-ircsim-0.1.5 (c (n "ircsim") (v "0.1.5") (d (list (d (n "clearscreen") (r "^1.0.10") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "crossterm") (r "^0.23.2") (d #t) (k 0)))) (h "0i15hwy7rs13m6fq6falsghgyrhzvx51kx15cqf0qkkxjs04wfar")))

