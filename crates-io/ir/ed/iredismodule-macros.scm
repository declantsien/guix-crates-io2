(define-module (crates-io ir ed iredismodule-macros) #:use-module (crates-io))

(define-public crate-iredismodule-macros-0.1.0 (c (n "iredismodule-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive" "parsing" "printing" "clone-impls" "proc-macro" "full"))) (d #t) (k 0)))) (h "0wyyqvggfbpgrpgq1q886hv1f8vjcnp1qrkd6xrnqzd8n8lcfjpp")))

(define-public crate-iredismodule-macros-0.1.1 (c (n "iredismodule-macros") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive" "parsing" "printing" "clone-impls" "proc-macro" "full"))) (d #t) (k 0)))) (h "025h4yqxhfmi2s3pzbwhzvram53kwvprcviyqmia7hrljwx8jk3k")))

(define-public crate-iredismodule-macros-0.1.2 (c (n "iredismodule-macros") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive" "parsing" "printing" "clone-impls" "proc-macro" "full"))) (d #t) (k 0)))) (h "1q36a6chbibzn0lx8wrs1gxcgry18v9c5rqi04lq8pncna9iqc64")))

(define-public crate-iredismodule-macros-0.1.3 (c (n "iredismodule-macros") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive" "parsing" "printing" "clone-impls" "proc-macro" "full"))) (d #t) (k 0)))) (h "11f8csqdhl1v67bx1xsw7s2sgx5vx52nvzhk286qfxxm5f3mzzvj")))

(define-public crate-iredismodule-macros-0.2.0 (c (n "iredismodule-macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive" "parsing" "printing" "clone-impls" "proc-macro" "full"))) (d #t) (k 0)))) (h "06h5ljp1m80gh2j1mcb3bgsd0arq7kxkayhhzl02kymh7v8isqiq")))

(define-public crate-iredismodule-macros-0.3.0 (c (n "iredismodule-macros") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive" "parsing" "printing" "clone-impls" "proc-macro" "full"))) (d #t) (k 0)))) (h "0qmhm5j1jd7lmipync0gmc7qhfyijhrqssa9nzjdx2q57hfpz6xm")))

