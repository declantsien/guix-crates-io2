(define-module (crates-io ir ap irapt) #:use-module (crates-io))

(define-public crate-irapt-0.1.0 (c (n "irapt") (v "0.1.0") (d (list (d (n "csv") (r "^1.1") (d #t) (k 2)) (d (n "itertools") (r "^0.10") (k 0)) (d (n "matches") (r "^0.1") (d #t) (k 2)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "rustfft") (r "^6.0") (d #t) (k 0)))) (h "1h3vmiqxvfrpw885dqsd5wm5wgkiki5rnyas9sy4rvh2rg004y32")))

(define-public crate-irapt-0.1.1 (c (n "irapt") (v "0.1.1") (d (list (d (n "csv") (r "^1.1") (d #t) (k 2)) (d (n "itertools") (r "^0.10") (k 0)) (d (n "matches") (r "^0.1") (d #t) (k 2)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "rustfft") (r "^6.0") (d #t) (k 0)))) (h "1g62icbx2wkw0d4k5162rj0n5rx2r7fqgi92ywivzamwa0rpzwiy")))

(define-public crate-irapt-0.2.0 (c (n "irapt") (v "0.2.0") (d (list (d (n "csv") (r "^1.1") (d #t) (k 2)) (d (n "itertools") (r "^0.10") (k 0)) (d (n "matches") (r "^0.1") (d #t) (k 2)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "rustfft") (r "^6.0") (d #t) (k 0)))) (h "1ivhn7s4kyyh8vns2gx8mp8q7wnkbskqs67iig1n598wvnxrvc27")))

