(define-module (crates-io ir rg irrgarten) #:use-module (crates-io))

(define-public crate-irrgarten-0.1.0 (c (n "irrgarten") (v "0.1.0") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0v4k15sv745z090mval4nif09rq4bk3vvyqcvg1zh13kmav2bdgw")))

(define-public crate-irrgarten-0.1.1 (c (n "irrgarten") (v "0.1.1") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1aj3zrrff8vq8spr2dg7jm82mwg484gqs2kjrsswir6g0q8g9jfs")))

