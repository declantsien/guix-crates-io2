(define-module (crates-io ir el irelia_encoder) #:use-module (crates-io))

(define-public crate-irelia_encoder-0.1.0 (c (n "irelia_encoder") (v "0.1.0") (d (list (d (n "base64") (r "^0.21.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "04k10m5459rcl0ka50wzqvpx6987bwad8iiqyfayyv5b6bmbpc05")))

(define-public crate-irelia_encoder-0.1.1 (c (n "irelia_encoder") (v "0.1.1") (d (list (d (n "base64") (r "^0.21.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1bzk34andasz7h2rb71jaaszlkzpd20a2a2s2zfip6rlnj8kg6gj")))

(define-public crate-irelia_encoder-0.1.2 (c (n "irelia_encoder") (v "0.1.2") (d (list (d (n "base64") (r "^0.21.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0vgkpx3mdkifh5qnziayr7bsgk3di97jzhnvd1k6zl14lcl14y5r") (f (quote (("simd" "nightly") ("nightly"))))))

(define-public crate-irelia_encoder-0.1.3 (c (n "irelia_encoder") (v "0.1.3") (d (list (d (n "base64") (r "^0.21.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1b8lm2ahnr43lbjmzz0hr9j19bc3jml1qn97c6v2lgi3i361j4ky") (f (quote (("simd" "nightly") ("nightly"))))))

(define-public crate-irelia_encoder-0.1.4 (c (n "irelia_encoder") (v "0.1.4") (d (list (d (n "base64") (r "^0.21.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1k0i4spcjvy5r96840sx2y5ryjq5pdd8ffspdqz0033klhdkysdm") (f (quote (("simd" "nightly") ("nightly"))))))

