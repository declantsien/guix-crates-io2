(define-module (crates-io ir ef iref__bbqsrc) #:use-module (crates-io))

(define-public crate-iref__bbqsrc-1.1.5 (c (n "iref__bbqsrc") (v "1.1.5") (d (list (d (n "pct-str") (r "^1.0.2") (d #t) (k 0)) (d (n "smallvec") (r "^1.2.0") (d #t) (k 0)))) (h "1s19napnlfqz6ckr5icbhqs53fwb4q1zl63rg357z865ldp95xfm") (y #t)))

