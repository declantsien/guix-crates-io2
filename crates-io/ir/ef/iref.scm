(define-module (crates-io ir ef iref) #:use-module (crates-io))

(define-public crate-iref-1.0.0 (c (n "iref") (v "1.0.0") (d (list (d (n "pct-str") (r "^1.0.2") (d #t) (k 0)) (d (n "smallvec") (r "^1.2.0") (d #t) (k 0)))) (h "0ayjz5lnv6dlx5hp42wb3y367xd34pk892lngqx0vwza8vxm6xn9")))

(define-public crate-iref-1.0.1 (c (n "iref") (v "1.0.1") (d (list (d (n "pct-str") (r "^1.0.2") (d #t) (k 0)) (d (n "smallvec") (r "^1.2.0") (d #t) (k 0)))) (h "0yckqz1qk6xqdj3fb4jigvqhw6iqsbhc4xncvirddvl755yxwcpk")))

(define-public crate-iref-1.1.0 (c (n "iref") (v "1.1.0") (d (list (d (n "pct-str") (r "^1.0.2") (d #t) (k 0)) (d (n "smallvec") (r "^1.2.0") (d #t) (k 0)))) (h "00cw5mrr2327v5lf82q4zw3i90hgw5z1g7ll5i16f52yr0q04xs7")))

(define-public crate-iref-1.1.1 (c (n "iref") (v "1.1.1") (d (list (d (n "pct-str") (r "^1.0.2") (d #t) (k 0)) (d (n "smallvec") (r "^1.2.0") (d #t) (k 0)))) (h "16b0dzkmagi4qygp25x3lr9s9sh0wy16ihw1gcxgpf52q4bm3qyl")))

(define-public crate-iref-1.1.2 (c (n "iref") (v "1.1.2") (d (list (d (n "pct-str") (r "^1.0.2") (d #t) (k 0)) (d (n "smallvec") (r "^1.2.0") (d #t) (k 0)))) (h "1qbyz4zkd8jyxb1gppqf6fzp14rnzqczjs7i9hwzg7g1i4vyjplj")))

(define-public crate-iref-1.1.3 (c (n "iref") (v "1.1.3") (d (list (d (n "pct-str") (r "^1.0.2") (d #t) (k 0)) (d (n "smallvec") (r "^1.2.0") (d #t) (k 0)))) (h "1hl9p26zsc94g4fxkj4b0bkl8sbpjl10pzpjd8601hklgldmc1hk")))

(define-public crate-iref-1.1.4 (c (n "iref") (v "1.1.4") (d (list (d (n "pct-str") (r "^1.0.2") (d #t) (k 0)) (d (n "smallvec") (r "^1.2.0") (d #t) (k 0)))) (h "06sq286db8f30kgyfbf8w9a0sz42nq2jlf8ivybwnncpkrcfy6jm")))

(define-public crate-iref-1.1.5 (c (n "iref") (v "1.1.5") (d (list (d (n "pct-str") (r "^1.0.2") (d #t) (k 0)) (d (n "smallvec") (r "^1.2.0") (d #t) (k 0)))) (h "0kjxk372gk0pdngqjjk4f2jdl0jim0vfhaj8ldlgv59nf4v5sak6")))

(define-public crate-iref-1.2.0 (c (n "iref") (v "1.2.0") (d (list (d (n "pct-str") (r "^1.0.2") (d #t) (k 0)) (d (n "smallvec") (r "^1.2.0") (d #t) (k 0)))) (h "04vc7vwdqnkhi0agkwgdakq696l7f63p4wpcslic781j7jifz2pq")))

(define-public crate-iref-1.3.0 (c (n "iref") (v "1.3.0") (d (list (d (n "pct-str") (r "^1.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.2") (d #t) (k 0)))) (h "07ad6vcig891z8jlnd43197wv35znypfnxk8n3as03ycjfjwx3r2") (y #t)))

(define-public crate-iref-1.3.1 (c (n "iref") (v "1.3.1") (d (list (d (n "pct-str") (r "^1.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.2") (d #t) (k 0)))) (h "0nxidk5w8jx90bb0505l948vwwz0v13w38raqh14yckh2x90pdx0") (y #t)))

(define-public crate-iref-1.4.0 (c (n "iref") (v "1.4.0") (d (list (d (n "pct-str") (r "^1.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.2") (d #t) (k 0)))) (h "04b9g45y91g250zq80wci2xpq8xv0qnl1xb70bhr2q2db3cpp1bw") (y #t)))

(define-public crate-iref-1.4.1 (c (n "iref") (v "1.4.1") (d (list (d (n "pct-str") (r "^1.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.2") (d #t) (k 0)))) (h "1grpv5inxbmdaabps81r0kqbzn7my9w6lqs7n6plh9cwcyn26nk3")))

(define-public crate-iref-1.4.2 (c (n "iref") (v "1.4.2") (d (list (d (n "pct-str") (r "^1.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.2") (d #t) (k 0)))) (h "1q840j4pmdb08ciqlpsb5d283w1xz5wc60vpydwa9phb7awppqyr")))

(define-public crate-iref-1.4.3 (c (n "iref") (v "1.4.3") (d (list (d (n "pct-str") (r "^1.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.2") (d #t) (k 0)))) (h "13sngi8k5xx3axn0my8krng7c2s4kr3hxfzh8xbypnlia86j0v6y")))

(define-public crate-iref-2.0.0 (c (n "iref") (v "2.0.0") (d (list (d (n "pct-str") (r "^1.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.2") (d #t) (k 0)))) (h "15fag95r7h7l3dzjc5ywnd44f9nzrd5z5i8hsabva5vmmhdjjclv") (y #t)))

(define-public crate-iref-2.0.1 (c (n "iref") (v "2.0.1") (d (list (d (n "pct-str") (r "^1.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.2") (d #t) (k 0)))) (h "11a0rzlwfsl8qnvrlhgni1dgia3qzc6kk2ddmw5y2f00grd9ca4j") (y #t)))

(define-public crate-iref-2.0.2 (c (n "iref") (v "2.0.2") (d (list (d (n "pct-str") (r "^1.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.2") (d #t) (k 0)))) (h "1rww23l9sw8nl5nkd61xkchbjdj3j8jks3rkn2hfxlcrq91pw86h") (y #t)))

(define-public crate-iref-2.0.3 (c (n "iref") (v "2.0.3") (d (list (d (n "pct-str") (r "^1.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.2") (d #t) (k 0)))) (h "1zln4s573dkr5acbyw1jfwzwcnncyfls7v4mj41wsj5pjpg866pf") (y #t)))

(define-public crate-iref-2.1.0 (c (n "iref") (v "2.1.0") (d (list (d (n "pct-str") (r "^1.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.2") (d #t) (k 0)))) (h "0q2apyyy1v3qhv47zw9q5kqxbvsv9kl90vhgjjb19r0wd8rpm96y") (y #t)))

(define-public crate-iref-2.1.1 (c (n "iref") (v "2.1.1") (d (list (d (n "pct-str") (r "^1.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.2") (d #t) (k 0)))) (h "0sph0mhlpasvgsldl0vnfyddy1agp3r9yxxa9gcqvj6l742w54iy")))

(define-public crate-iref-2.1.2 (c (n "iref") (v "2.1.2") (d (list (d (n "pct-str") (r "^1.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.2") (d #t) (k 0)))) (h "18axkkjdadfkhji02avhwz2j5w65y0yyrkzj8hwjbhlx3ridh0zq")))

(define-public crate-iref-2.1.3 (c (n "iref") (v "2.1.3") (d (list (d (n "pct-str") (r "^1.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.2") (d #t) (k 0)))) (h "08xsgvwpmi5ic7a97xnc13hdjgq91gczgw5g21w2kn8xl8jk317c")))

(define-public crate-iref-2.2.0 (c (n "iref") (v "2.2.0") (d (list (d (n "pct-str") (r "^1.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.2") (d #t) (k 0)))) (h "1z2dvf52y69gk0a66cg74l9rxqzqjkc461ajxj7iarrbjh3k895k")))

(define-public crate-iref-2.2.1 (c (n "iref") (v "2.2.1") (d (list (d (n "pct-str") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "smallvec") (r "^1.2") (d #t) (k 0)))) (h "15v3gq52zz3s0b6dp7p035dz9j4w9602r0rzb1sfpgzd6hca2slc")))

(define-public crate-iref-2.2.2 (c (n "iref") (v "2.2.2") (d (list (d (n "pct-str") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "smallvec") (r "^1.2") (d #t) (k 0)))) (h "0brqp3qiqd6h1bf3y709917mghynn75amx1ybcq7kf6p22yfraxw")))

(define-public crate-iref-2.2.3 (c (n "iref") (v "2.2.3") (d (list (d (n "hashbrown") (r "^0.13.1") (o #t) (d #t) (k 0)) (d (n "pct-str") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "smallvec") (r "^1.2") (d #t) (k 0)))) (h "1v781rvxglmv7053wigb4y7g9axh583n740q8f30l9bwismqsxbj")))

(define-public crate-iref-3.0.0 (c (n "iref") (v "3.0.0") (d (list (d (n "hashbrown") (r "^0.14.0") (o #t) (d #t) (k 0)) (d (n "pct-str") (r "^2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "smallvec") (r "^1.2") (d #t) (k 0)) (d (n "static-regular-grammar") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "1xcbb375kk02094kwl5gzzv5ix0qd8w7mx1p11arrm8kns9clsjx") (f (quote (("ignore-grammars") ("default")))) (y #t) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-iref-3.0.1 (c (n "iref") (v "3.0.1") (d (list (d (n "hashbrown") (r "^0.14.0") (o #t) (d #t) (k 0)) (d (n "pct-str") (r "^2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "smallvec") (r "^1.2") (d #t) (k 0)) (d (n "static-regular-grammar") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "18qblzi497pnb4k2i99n9vyvwbxyrfrybwmqq9l93q69s22g3nh2") (f (quote (("ignore-grammars") ("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-iref-3.0.2 (c (n "iref") (v "3.0.2") (d (list (d (n "hashbrown") (r "^0.14.0") (o #t) (d #t) (k 0)) (d (n "pct-str") (r "^2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "smallvec") (r "^1.2") (d #t) (k 0)) (d (n "static-regular-grammar") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "1m41n3fd7m1kgxjsqva83yhflzq06zhq5w3cffzj80lw01c6wrv9") (f (quote (("ignore-grammars") ("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-iref-3.1.0 (c (n "iref") (v "3.1.0") (d (list (d (n "hashbrown") (r "^0.14.0") (o #t) (d #t) (k 0)) (d (n "pct-str") (r "^2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "smallvec") (r "^1.2") (d #t) (k 0)) (d (n "static-regular-grammar") (r "^1.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0jx5iyq41id5qpyjjnzy9m68lsfz1yzv3zprm3kibmqv58hsgrkp") (f (quote (("ignore-grammars") ("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-iref-3.1.1 (c (n "iref") (v "3.1.1") (d (list (d (n "hashbrown") (r "^0.14.0") (o #t) (d #t) (k 0)) (d (n "pct-str") (r "^2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "smallvec") (r "^1.2") (d #t) (k 0)) (d (n "static-regular-grammar") (r "^1.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "1nnxhjk6pndc9z3hbpr24i0488ba3h05n96n7j739kwir3kslmmm") (f (quote (("ignore-grammars") ("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-iref-3.1.2 (c (n "iref") (v "3.1.2") (d (list (d (n "hashbrown") (r "^0.14.0") (o #t) (d #t) (k 0)) (d (n "pct-str") (r "^2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "smallvec") (r "^1.2") (d #t) (k 0)) (d (n "static-regular-grammar") (r "^1.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "1r3dizy6zxs6xnnaf6zaq9ir80sca8j53fdamz4ky2b3bz86hr1b") (f (quote (("ignore-grammars") ("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-iref-3.1.3 (c (n "iref") (v "3.1.3") (d (list (d (n "hashbrown") (r "^0.14.0") (o #t) (d #t) (k 0)) (d (n "pct-str") (r "^2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "smallvec") (r "^1.2") (d #t) (k 0)) (d (n "static-regular-grammar") (r "^1.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "01gd1f4pp0vk0zg73wrrjcxvhrs28g0lfvzx500ylin42wsyjr3f") (f (quote (("ignore-grammars") ("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-iref-3.1.4 (c (n "iref") (v "3.1.4") (d (list (d (n "hashbrown") (r "^0.14.0") (o #t) (d #t) (k 0)) (d (n "pct-str") (r "^2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "smallvec") (r "^1.2") (d #t) (k 0)) (d (n "static-regular-grammar") (r "^1.1.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "1d443fzvmvkx4ljg1wnd15bq9bm12wyqf334prjb8423mx949yqc") (f (quote (("ignore-grammars") ("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

