(define-module (crates-io ir ef iref-enum) #:use-module (crates-io))

(define-public crate-iref-enum-1.0.0 (c (n "iref-enum") (v "1.0.0") (d (list (d (n "iref") (r "^1.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "static-iref") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0kv5msxy85j8zh845bg0w5w8g9cvp3a91k1q1dilbfwl8fhk4p98")))

(define-public crate-iref-enum-1.1.0 (c (n "iref-enum") (v "1.1.0") (d (list (d (n "iref") (r "^1.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "static-iref") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1v1p3fmyxayw9x2gkxyccsva0ay2kfl9dzg49xc3g02yirr9c84b")))

(define-public crate-iref-enum-1.2.0 (c (n "iref-enum") (v "1.2.0") (d (list (d (n "iref") (r "^1.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "static-iref") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "085inryi8b3hv6xxkg5788zy5iqk6kxwdrkgqf7l4hqlvpc569pp")))

(define-public crate-iref-enum-2.0.0 (c (n "iref-enum") (v "2.0.0") (d (list (d (n "iref") (r "^2.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "static-iref") (r "^2.0") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0wzhpib7cf0jjy2bp2n2j1kd4cfhpm4hk4pryjzibqambv2m1k4d")))

(define-public crate-iref-enum-2.1.0 (c (n "iref-enum") (v "2.1.0") (d (list (d (n "iref") (r "^2.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "static-iref") (r "^2.0") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0hmxr3mrf4n9dgbqgsxdyx4n20df90n9ring3harbzwjq5rhb4m3")))

(define-public crate-iref-enum-3.0.0 (c (n "iref-enum") (v "3.0.0") (d (list (d (n "iref") (r "^3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "static-iref") (r "^3.0") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "092gprn40qdynkkvx30fzwfrhsnwskfvx1hbzgyxvngsjfqlsgfx")))

