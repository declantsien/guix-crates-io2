(define-module (crates-io ir oh iroha-internal) #:use-module (crates-io))

(define-public crate-iroha-internal-0.1.0 (c (n "iroha-internal") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.18") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.33") (d #t) (k 0)))) (h "1ajplx18jbbbf1r8krr11lvp8wj7cnyz89w8m1ksf77phia18sh8")))

(define-public crate-iroha-internal-0.1.1 (c (n "iroha-internal") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.18") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.33") (d #t) (k 0)))) (h "1s6gm90maca3xjd3vxlb9q3y61ay4ldka8yavq58qbla0a8cjd51")))

