(define-module (crates-io ir oh iroh-quinn-udp) #:use-module (crates-io))

(define-public crate-iroh-quinn-udp-0.4.1 (c (n "iroh-quinn-udp") (v "0.4.1") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "libc") (r "^0.2.113") (d #t) (k 0)) (d (n "socket2") (r "^0.5") (d #t) (k 0)) (d (n "tracing") (r "^0.1.10") (d #t) (k 0)) (d (n "windows-sys") (r "^0.48.0") (f (quote ("Win32_Networking_WinSock"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0qy005psbapvbpsrcwrsm841b223lnsc08lnmsflzhkifad9fyb6") (f (quote (("log" "tracing/log") ("default" "log")))) (r "1.63")))

(define-public crate-iroh-quinn-udp-0.4.2 (c (n "iroh-quinn-udp") (v "0.4.2") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "libc") (r "^0.2.113") (d #t) (k 0)) (d (n "socket2") (r "^0.5") (d #t) (k 0)) (d (n "tracing") (r "^0.1.10") (d #t) (k 0)) (d (n "windows-sys") (r "^0.48.0") (f (quote ("Win32_Networking_WinSock"))) (d #t) (t "cfg(windows)") (k 0)))) (h "056kgxi12ak2f1zyz08hd8abxmd5c56kzxq2pkh8xw1i79dr3izd") (f (quote (("log" "tracing/log") ("default" "log")))) (r "1.63")))

