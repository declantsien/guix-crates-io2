(define-module (crates-io ir oh iroha-derive) #:use-module (crates-io))

(define-public crate-iroha-derive-0.1.0 (c (n "iroha-derive") (v "0.1.0") (d (list (d (n "heck") (r "^0.3.1") (d #t) (k 0)) (d (n "iroha-internal") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.18") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.33") (d #t) (k 0)))) (h "1zjbgy24dizkwy2r8d1xin15xbgn128bfgf9qzqvhhdzm1dxr3kh")))

(define-public crate-iroha-derive-0.1.1 (c (n "iroha-derive") (v "0.1.1") (d (list (d (n "heck") (r "^0.3.1") (d #t) (k 0)) (d (n "iroha-internal") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.18") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.33") (d #t) (k 0)))) (h "040sn4waj227ankkf7sfi3y2w8hsjq1warm7ijvvxlxmwzawznj1")))

(define-public crate-iroha-derive-0.1.2 (c (n "iroha-derive") (v "0.1.2") (d (list (d (n "heck") (r "^0.3.1") (d #t) (k 0)) (d (n "iroha-internal") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.18") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.33") (d #t) (k 0)))) (h "1pj59qf3ddd4y1qz2a165j52v77ndp91xayydgay05r25isjwj81")))

