(define-module (crates-io ir oh iroh-localops) #:use-module (crates-io))

(define-public crate-iroh-localops-0.1.2 (c (n "iroh-localops") (v "0.1.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "nix") (r "^0.25") (f (quote ("signal" "process"))) (d #t) (t "cfg(unix)") (k 0)))) (h "1a47pk9swrqw9kmix0h5wbl9g6vxqkijii343f94mfamkwl8xqwp") (r "1.63")))

(define-public crate-iroh-localops-0.1.3 (c (n "iroh-localops") (v "0.1.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "nix") (r "^0.25") (f (quote ("signal" "process"))) (d #t) (t "cfg(unix)") (k 0)))) (h "1vkkhcfiq8hzv34y8n83j30xydmzcnsyz5p0hscvw8gn2qi0nrbm") (r "1.63")))

(define-public crate-iroh-localops-0.2.0 (c (n "iroh-localops") (v "0.2.0") (d (list (d (n "anyhow") (r "^1") (f (quote ("backtrace"))) (d #t) (k 0)) (d (n "nix") (r "^0.26") (f (quote ("signal" "process"))) (d #t) (t "cfg(unix)") (k 0)))) (h "123py9xr7sqm8pb8ca8360csmzrpvh17kq2kq56dzamwzlxbq6l4") (r "1.65")))

