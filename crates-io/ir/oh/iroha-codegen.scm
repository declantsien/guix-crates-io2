(define-module (crates-io ir oh iroha-codegen) #:use-module (crates-io))

(define-public crate-iroha-codegen-0.1.0 (c (n "iroha-codegen") (v "0.1.0") (d (list (d (n "heck") (r "^0.3.1") (d #t) (k 0)) (d (n "helpers") (r "^0.1.0") (d #t) (k 0) (p "iroha-helpers")) (d (n "proc-macro2") (r "^1.0.18") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.33") (d #t) (k 0)))) (h "0hdmym3nl4xpmjc5xl5d60ichwja0wc97qz5vsb6w5xf29490ifg")))

(define-public crate-iroha-codegen-0.1.1 (c (n "iroha-codegen") (v "0.1.1") (d (list (d (n "heck") (r "^0.3.1") (d #t) (k 0)) (d (n "helpers") (r "^0.1.0") (d #t) (k 0) (p "iroha-helpers")) (d (n "proc-macro2") (r "^1.0.18") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.33") (d #t) (k 0)))) (h "1q2hlaw4aq3m1fdryr5ikdn05rd07r1l1jaj3gnry8da3jyncyp5")))

(define-public crate-iroha-codegen-0.1.2 (c (n "iroha-codegen") (v "0.1.2") (d (list (d (n "heck") (r "^0.3.1") (d #t) (k 0)) (d (n "helpers") (r "^0.1.0") (d #t) (k 0) (p "iroha-helpers")) (d (n "proc-macro2") (r "^1.0.18") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.33") (d #t) (k 0)))) (h "1gfqr9ca0in4mpy4cj5ygzxzz2z0j3zwzlzpzs71inlgwki36frs")))

(define-public crate-iroha-codegen-0.1.3 (c (n "iroha-codegen") (v "0.1.3") (d (list (d (n "heck") (r "^0.3.1") (d #t) (k 0)) (d (n "helpers") (r "^0.1.1") (d #t) (k 0) (p "iroha-helpers")) (d (n "proc-macro2") (r "^1.0.18") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.33") (d #t) (k 0)))) (h "1zggbk16m49n3bfn2xwk0hmjb36ghs05295x5806a89rycqkwxa8")))

(define-public crate-iroha-codegen-0.1.4 (c (n "iroha-codegen") (v "0.1.4") (d (list (d (n "heck") (r "^0.3.1") (d #t) (k 0)) (d (n "helpers") (r "^0.1.2") (d #t) (k 0) (p "iroha-helpers")) (d (n "proc-macro2") (r "^1.0.18") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.33") (d #t) (k 0)))) (h "0w172pj6hhya52h7snv6kn94l160zbz3cppwgb0m0l46vngkf5dx")))

