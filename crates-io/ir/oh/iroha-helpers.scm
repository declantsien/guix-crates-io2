(define-module (crates-io ir oh iroha-helpers) #:use-module (crates-io))

(define-public crate-iroha-helpers-0.1.0 (c (n "iroha-helpers") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.18") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.33") (d #t) (k 0)))) (h "17nb67swjnqsrch4j2pdsq8j83f6fi37nvkfyrbkrkd5clz1mgqn")))

(define-public crate-iroha-helpers-0.1.1 (c (n "iroha-helpers") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.18") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.33") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "13nh770v7agznci3jw0b5qbjapigs81xpd65y7i5j742y1bzhfc6")))

(define-public crate-iroha-helpers-0.1.2 (c (n "iroha-helpers") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.18") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.33") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "1f3dslhg0ac1c61gdzs67bb2b2njwqfpjqd52wxack39kqfa3z72")))

