(define-module (crates-io ir oh iroh-test) #:use-module (crates-io))

(define-public crate-iroh-test-0.6.0-alpha.0 (c (n "iroh-test") (v "0.6.0-alpha.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("env-filter"))) (d #t) (k 0)))) (h "0d0w32k9mcdlfpp0glal91si3dj2ja6s5pk4hq60k0vv1dac7xhj") (r "1.67")))

(define-public crate-iroh-test-0.6.0-alpha.1 (c (n "iroh-test") (v "0.6.0-alpha.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("env-filter"))) (d #t) (k 0)))) (h "1gbv7z4m92jcgbh7gib6p0pnac9pya2rqxsq730p48ypkrknc1hc") (r "1.70")))

(define-public crate-iroh-test-0.6.0 (c (n "iroh-test") (v "0.6.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("env-filter"))) (d #t) (k 0)))) (h "04w5y0agkmjcacvdmk5q97vmv7x5sglgkalshanks4jdy65r1g1g") (r "1.70")))

(define-public crate-iroh-test-0.7.0 (c (n "iroh-test") (v "0.7.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("env-filter"))) (d #t) (k 0)))) (h "0m5wqq1dml9vyqnzxkmvgx8clg6nrjymcvy1007vdi46d3vfy5al") (r "1.70")))

(define-public crate-iroh-test-0.8.0 (c (n "iroh-test") (v "0.8.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("env-filter"))) (d #t) (k 0)))) (h "00hlw5w012129l82q27z5yizv7q4pnx85p4923n5dybyapyd3pnl") (r "1.72")))

(define-public crate-iroh-test-0.9.0 (c (n "iroh-test") (v "0.9.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("env-filter"))) (d #t) (k 0)))) (h "1d7s8rihd67ma0ci1ii9w0fn7xk6gf6ff6259ydf348iwhxc1ch7") (r "1.72")))

(define-public crate-iroh-test-0.10.0 (c (n "iroh-test") (v "0.10.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("env-filter"))) (d #t) (k 0)))) (h "1f4gkhmn65bqpkjdzp7hl3314f78z1dq4nv07a3c3ry6hg04b95h") (r "1.72")))

(define-public crate-iroh-test-0.11.0 (c (n "iroh-test") (v "0.11.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("env-filter"))) (d #t) (k 0)))) (h "0jc4kk1gw646l674f6rr235vbzvy75ihyqfj9lk03llrw6bsyvmz") (r "1.72")))

(define-public crate-iroh-test-0.12.0 (c (n "iroh-test") (v "0.12.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("env-filter"))) (d #t) (k 0)))) (h "1n2ygcylgw94sfzf5brad6xi4fig7a2wpjy1gr249pgb2s13gcgh") (r "1.72")))

(define-public crate-iroh-test-0.13.0 (c (n "iroh-test") (v "0.13.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("env-filter"))) (d #t) (k 0)))) (h "023x01036vbx1921zbdd62ciyqpk7jf8a7ri04k4kg3fk55rzphq") (r "1.75")))

(define-public crate-iroh-test-0.14.0 (c (n "iroh-test") (v "0.14.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("env-filter"))) (d #t) (k 0)))) (h "1jr9f2zvd91rixjr75vk8icrvv3rxakps90mis4am172gs3l5925") (r "1.75")))

(define-public crate-iroh-test-0.15.0 (c (n "iroh-test") (v "0.15.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("env-filter"))) (d #t) (k 0)))) (h "1d9xg0w6njg5470x3j347ph5wg2zyvak89d1j8am89343flamyrz") (r "1.75")))

(define-public crate-iroh-test-0.16.0 (c (n "iroh-test") (v "0.16.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("env-filter"))) (d #t) (k 0)))) (h "14zaxgbfngcd0mwhmp3gsahilm7wcyz33bn6ah7sags25c3lvcid") (r "1.75")))

(define-public crate-iroh-test-0.16.2 (c (n "iroh-test") (v "0.16.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("env-filter"))) (d #t) (k 0)))) (h "16y0b9vfywz0h61xd181c1vdxz92z4vcr2sinp56idk52qxcpbbd") (r "1.75")))

(define-public crate-iroh-test-0.17.0 (c (n "iroh-test") (v "0.17.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("env-filter"))) (d #t) (k 0)))) (h "1p4hscva4nh5xlpg6r2iqypmlq7lpmj8rcj870fkxhnlyy9q9m30") (r "1.76")))

