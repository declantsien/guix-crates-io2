(define-module (crates-io ir ge irgen) #:use-module (crates-io))

(define-public crate-irgen-0.1.0 (c (n "irgen") (v "0.1.0") (d (list (d (n "apodize") (r "^1.0.0") (d #t) (k 0)) (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "hound") (r "^3.5.0") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "rustfft") (r "^6.1.0") (d #t) (k 0)))) (h "1bn43h5ch4jj37mlwni69jzs9y34pz6ws3cy5hkq318a6ia9z1fg")))

(define-public crate-irgen-0.1.1 (c (n "irgen") (v "0.1.1") (d (list (d (n "apodize") (r "^1.0.0") (d #t) (k 0)) (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "hound") (r "^3.5.0") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "rustfft") (r "^6.1.0") (d #t) (k 0)))) (h "0wsgnnq3rvp705cad5319fx4x04kxw9wnqvm9wnxvbzkxh1bzlq2")))

(define-public crate-irgen-0.1.2 (c (n "irgen") (v "0.1.2") (d (list (d (n "apodize") (r "^1.0.0") (d #t) (k 0)) (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "hound") (r "^3.5.0") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "rustfft") (r "^6.1.0") (d #t) (k 0)))) (h "0fdymfavgf84f7d9pqxv7h9zy8scp7xp55r8av69l69najx2d5kj")))

(define-public crate-irgen-0.1.3 (c (n "irgen") (v "0.1.3") (d (list (d (n "apodize") (r "^1.0.0") (d #t) (k 0)) (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "hound") (r "^3.5.0") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "rustfft") (r "^6.1.0") (d #t) (k 0)))) (h "1iki5ihypb76qwnk6a7266c4br88ac3da9idjv08x89biaazjvqg")))

(define-public crate-irgen-0.1.4 (c (n "irgen") (v "0.1.4") (d (list (d (n "apodize") (r "^1.0.0") (d #t) (k 0)) (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "hound") (r "^3.5.0") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "rustfft") (r "^6.1.0") (d #t) (k 0)))) (h "0kwncfwjxj9yxhra2wkjsa8jsqz87hcl5ym398x02i3lxkva8p7z")))

(define-public crate-irgen-0.1.5 (c (n "irgen") (v "0.1.5") (d (list (d (n "apodize") (r "^1.0.0") (d #t) (k 0)) (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "hound") (r "^3.5.0") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "rustfft") (r "^6.1.0") (d #t) (k 0)))) (h "16fh0p1liyns745mssvi87nmj8nyhdawvbm8h666rgifpdyznjzl")))

