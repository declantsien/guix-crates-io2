(define-module (crates-io ir is irisguard) #:use-module (crates-io))

(define-public crate-irisguard-0.1.0 (c (n "irisguard") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1zcy0j0f4lc4ymbbz62hc7pkhh7c1s0q10fyrc8kb4dpbplp67lz")))

(define-public crate-irisguard-0.1.1 (c (n "irisguard") (v "0.1.1") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0afrxnf02hx3mkhpmjcg81j9mf1fgpgr2sblhhv0fb04n8f0vgdv")))

