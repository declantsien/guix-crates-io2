(define-module (crates-io ir is iris_client) #:use-module (crates-io))

(define-public crate-iris_client-0.1.0 (c (n "iris_client") (v "0.1.0") (d (list (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0n6g76d93yw5jv351m68cp7cakwadh5xxf2rwhbqhkypmhx3dnmw")))

(define-public crate-iris_client-0.1.1 (c (n "iris_client") (v "0.1.1") (d (list (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0qsn08jjai9d0kj096yrda45n7m3vfli3mh60qxv455vppl0nyja")))

(define-public crate-iris_client-0.1.2 (c (n "iris_client") (v "0.1.2") (d (list (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 0)))) (h "11gdy2pik2kdlx2yf96kf1lnxbqk64c4jdyjv0kjdwvk1wa98d6v")))

