(define-module (crates-io ir is iris-converters) #:use-module (crates-io))

(define-public crate-iris-converters-0.1.1 (c (n "iris-converters") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "deku") (r "^0.12.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "sha2") (r "^0.10.2") (d #t) (k 0)) (d (n "warts") (r "^0.2") (d #t) (k 0)))) (h "1gdmif7s1nm2pss13ddv1kapvd43sfcqgl029xn8g68dir62bcfj") (y #t)))

