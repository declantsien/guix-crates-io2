(define-module (crates-io ir is iris-lib) #:use-module (crates-io))

(define-public crate-iris-lib-0.1.0 (c (n "iris-lib") (v "0.1.0") (d (list (d (n "image") (r "^0.24.3") (o #t) (d #t) (k 0)))) (h "1ch8zbiby9hqimvbkqfsy3vmc2fixbcvbp8s02g61pjmxxhgya9c") (s 2) (e (quote (("image" "dep:image"))))))

