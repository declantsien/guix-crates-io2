(define-module (crates-io ir is iris-data) #:use-module (crates-io))

(define-public crate-iris-data-0.0.1 (c (n "iris-data") (v "0.0.1") (d (list (d (n "csv") (r "^1.1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_repr") (r "^0.1") (d #t) (k 0)))) (h "08dhb6hcfmbrd059354jqrjb8gvsi542k1pa01g192xaljh5apbb")))

