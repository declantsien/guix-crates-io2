(define-module (crates-io ir is iris-ng) #:use-module (crates-io))

(define-public crate-iris-ng-0.1.0 (c (n "iris-ng") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.56.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.66") (d #t) (k 1)) (d (n "errno") (r "^0.2.7") (d #t) (k 0)))) (h "05lqm24n5bn4d3zl09s3fr9cmn2qspklh8dlr81r2lz5lgqbc5h4")))

(define-public crate-iris-ng-0.1.1 (c (n "iris-ng") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.56.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.66") (d #t) (k 1)) (d (n "errno") (r "^0.2.7") (d #t) (k 0)))) (h "1xd56ycpcrzmazs1ay7w5mwfdb26syhv53mf9l3kn8sm9gj859z8")))

(define-public crate-iris-ng-0.1.2 (c (n "iris-ng") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.56.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.66") (d #t) (k 1)) (d (n "errno") (r "^0.2.7") (d #t) (k 0)))) (h "0mica9vpihwv13sxyfdn1f86plzz0nq26y36mnl2ipiscikhj1k6")))

