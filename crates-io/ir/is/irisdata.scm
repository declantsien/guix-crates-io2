(define-module (crates-io ir is irisdata) #:use-module (crates-io))

(define-public crate-irisdata-0.1.0 (c (n "irisdata") (v "0.1.0") (h "0cydivh9cpkwvywc0b6r6gz9z8snvlrbc7882ss7gxg3w3nw4gg9") (y #t)))

(define-public crate-irisdata-0.1.1 (c (n "irisdata") (v "0.1.1") (h "1nlgpik0rfdj5igf4hm8yvd1if4iwvy76czm1gk9z5j047gqb643")))

(define-public crate-irisdata-0.1.2 (c (n "irisdata") (v "0.1.2") (h "08w08xa28x1span7jz3y6ain6rrb3r6yl0hcan8ml6g6xkdni5sj")))

