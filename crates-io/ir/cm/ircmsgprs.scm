(define-module (crates-io ir cm ircmsgprs) #:use-module (crates-io))

(define-public crate-ircmsgprs-0.1.0 (c (n "ircmsgprs") (v "0.1.0") (h "0m2c6gpqasc9qncngqvyi6s4fb4nm49hhr0hp664zh1aj7m6l1h8")))

(define-public crate-ircmsgprs-0.1.1 (c (n "ircmsgprs") (v "0.1.1") (h "034ii1szz5ws9xa7y2gmqd69wyh7izwnxfrpr4jhcn1w9p6m5c8j")))

(define-public crate-ircmsgprs-0.2.0 (c (n "ircmsgprs") (v "0.2.0") (h "0x9z2rbq2rv59ph3s784wrnsrqikip6zbl4w1ngcwd75l2v1i0da")))

(define-public crate-ircmsgprs-0.2.1 (c (n "ircmsgprs") (v "0.2.1") (h "03pgzl8gngvxrvfhvndahi5hdl1yp0d97m9w15p51v38vzlil9bi")))

