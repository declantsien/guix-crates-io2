(define-module (crates-io ir cv ircv3_tags) #:use-module (crates-io))

(define-public crate-ircv3_tags-0.0.2 (c (n "ircv3_tags") (v "0.0.2") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)))) (h "0xgh66dd17rhav1bmjxzs3qzf6a2qjfbmfh5hglkk72jhfg2awqz")))

(define-public crate-ircv3_tags-0.0.3 (c (n "ircv3_tags") (v "0.0.3") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)))) (h "03ia03vy87nj66nmgb2awycjipmzcxafg1nq3fblxbbaq8rfm1cb")))

(define-public crate-ircv3_tags-0.0.4 (c (n "ircv3_tags") (v "0.0.4") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)))) (h "1mz0gxlhqa3xdhamary4nlp18i9wpmsbpm61fkpw4i2savr6jpdp")))

(define-public crate-ircv3_tags-0.0.5 (c (n "ircv3_tags") (v "0.0.5") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)))) (h "0xq132k85kyj3nagig8x0xw3aglfhnj1xvq6drdwgfcjgxbqyy8a")))

(define-public crate-ircv3_tags-0.0.6 (c (n "ircv3_tags") (v "0.0.6") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)))) (h "02vci3llkaqhawmxdrfsbz519yvl7nqc2nx1ca6fvayhs0jg94jd")))

(define-public crate-ircv3_tags-0.1.0 (c (n "ircv3_tags") (v "0.1.0") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)))) (h "00q0jxl198rimkn2a0izdsdc0jc89v2kc2nyl0afl1k13x5ssd8r")))

(define-public crate-ircv3_tags-0.1.1 (c (n "ircv3_tags") (v "0.1.1") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)))) (h "1znqm9sy6rcq1zrz5lws77indnd069j1kck46p48q5cplpxjj6kd")))

(define-public crate-ircv3_tags-0.1.2 (c (n "ircv3_tags") (v "0.1.2") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)))) (h "1dv096ycd31qyvim1fl4fgrakl3ar9a1wx2h0g83si18iqys2qrr")))

(define-public crate-ircv3_tags-0.1.3 (c (n "ircv3_tags") (v "0.1.3") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)))) (h "0shsn4fg62afbq2fih9ysgh0awdwsz3x2sik7ww5rqv6v6rcchhy")))

