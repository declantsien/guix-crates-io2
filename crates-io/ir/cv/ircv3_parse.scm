(define-module (crates-io ir cv ircv3_parse) #:use-module (crates-io))

(define-public crate-ircv3_parse-0.0.2 (c (n "ircv3_parse") (v "0.0.2") (d (list (d (n "ircv3_tags") (r "^0.0.2") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)))) (h "1vz8rsinnxbb5wf1df29s7kl9aqijvsij694fk5fqrjdj544zgv2")))

(define-public crate-ircv3_parse-0.0.3 (c (n "ircv3_parse") (v "0.0.3") (d (list (d (n "ircv3_tags") (r "^0.0.4") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)))) (h "1pqfxkbh7smsd0g0mzyb4h55x61amikkqzl6rxgijr2l7p4nqkds")))

(define-public crate-ircv3_parse-0.0.4 (c (n "ircv3_parse") (v "0.0.4") (d (list (d (n "ircv3_tags") (r "^0.0.4") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)))) (h "0m4yvcyp06i5ngwzcxh00qciq6qkfzk4dykbqd59qslbb12vzrjs")))

(define-public crate-ircv3_parse-0.0.5 (c (n "ircv3_parse") (v "0.0.5") (d (list (d (n "ircv3_tags") (r "^0.0.4") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)))) (h "0b2ijwfp6lircyqzapr369438as3l0yk8pdpaacy6rf34kf6l74g")))

(define-public crate-ircv3_parse-0.0.6 (c (n "ircv3_parse") (v "0.0.6") (d (list (d (n "ircv3_tags") (r "^0.0.4") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)))) (h "1wzqsp11c7qyvlx3nv3aj867zhqny6rsbdxp5ypn5kw0wc6sfbr6")))

(define-public crate-ircv3_parse-0.0.7 (c (n "ircv3_parse") (v "0.0.7") (d (list (d (n "ircv3_tags") (r "^0.0.4") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)))) (h "1yvs0mgfkk6agc88l6wg7yk9x5qq2mz373rl84kmy4qxv0zphzfp")))

(define-public crate-ircv3_parse-0.0.8 (c (n "ircv3_parse") (v "0.0.8") (d (list (d (n "ircv3_tags") (r "^0.0.6") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)))) (h "0z58wiwg0qi265f1aqcriprvkjr4pm86ppv0nhsakk3drjj48mqd")))

(define-public crate-ircv3_parse-0.1.0 (c (n "ircv3_parse") (v "0.1.0") (d (list (d (n "ircv3_tags") (r "^0.1.1") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)))) (h "064iw213v9gk9zy6dy6g47qa6hbr27421gd3j1bdfzb7fyxy5dqa")))

(define-public crate-ircv3_parse-0.1.1 (c (n "ircv3_parse") (v "0.1.1") (d (list (d (n "ircv3_tags") (r "^0.1.1") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)))) (h "18r71i4csjqzgcqg4xgarj53m0qzis71nqd2abahk77sfi0c9waz")))

(define-public crate-ircv3_parse-0.1.2 (c (n "ircv3_parse") (v "0.1.2") (d (list (d (n "ircv3_tags") (r "^0.1.1") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)))) (h "11qqi9yfhnyv66amw2i6idi4a18g67yqlmj2sprw9qska7557kvl")))

(define-public crate-ircv3_parse-0.1.3 (c (n "ircv3_parse") (v "0.1.3") (d (list (d (n "ircv3_tags") (r "^0.1.2") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)))) (h "1fwj1kkm68mjbamwv0qihmqsv97mpjzv69mrnv7p5xy9w7b021la")))

(define-public crate-ircv3_parse-0.1.4 (c (n "ircv3_parse") (v "0.1.4") (d (list (d (n "ircv3_tags") (r "^0.1.3") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)))) (h "15n35mb1d09wiz2xs7zkq8wv1kqz40mm82sv6jd154fxk9gs9spw")))

