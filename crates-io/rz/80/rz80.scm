(define-module (crates-io rz #{80}# rz80) #:use-module (crates-io))

(define-public crate-rz80-0.1.0 (c (n "rz80") (v "0.1.0") (d (list (d (n "minifb") (r "^0.8.3") (d #t) (k 2)) (d (n "time") (r "^0.1") (d #t) (k 2)))) (h "043llws61j7bmc3065ihkqjx0j7nji649c0baw9z0652nma4cl54")))

(define-public crate-rz80-0.1.1 (c (n "rz80") (v "0.1.1") (d (list (d (n "minifb") (r "^0.8.3") (d #t) (k 2)) (d (n "time") (r "^0.1") (d #t) (k 2)))) (h "1jbbmkwdrig9jmjzf5xpgafqzcbpwh95mfdjap57lfq92rlvcp3c")))

