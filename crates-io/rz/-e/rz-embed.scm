(define-module (crates-io rz -e rz-embed) #:use-module (crates-io))

(define-public crate-rz-embed-0.1.0 (c (n "rz-embed") (v "0.1.0") (d (list (d (n "flate2") (r "^1.0.30") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.82") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "syn") (r "^2.0.64") (d #t) (k 0)) (d (n "walkdir") (r "^2.5.0") (d #t) (k 0)))) (h "0hcns2k460l4y1c0yvkakgfwwrm8gqk8n63b3z34rdvqhsbi2fd9")))

