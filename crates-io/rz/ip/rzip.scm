(define-module (crates-io rz ip rzip) #:use-module (crates-io))

(define-public crate-rzip-0.9.0 (c (n "rzip") (v "0.9.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "regex") (r "^1.4.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "time") (r "^0.1.44") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)) (d (n "zip") (r "^0.5.12") (d #t) (k 0)))) (h "08ls0d28y7h6w3md05fkkahc93mk3bvhjzasnyj093vdzbj9jlv1")))

(define-public crate-rzip-0.9.1 (c (n "rzip") (v "0.9.1") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "regex") (r "^1.4.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "time") (r "^0.1.44") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)) (d (n "zip") (r "^0.5.12") (d #t) (k 0)))) (h "1h6mp9kckaflc4n53myfwzqqca1iv7rl6ir8ypjyzmhr16swy6gi")))

(define-public crate-rzip-0.9.2 (c (n "rzip") (v "0.9.2") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "regex") (r "^1.4.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "time") (r "^0.1.44") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)) (d (n "zip") (r "^0.5.12") (d #t) (k 0)))) (h "012mrk0n1q6lirzbsfbn2fxplfnaxjq78nrbgv3gi7cjhkmyh81n")))

(define-public crate-rzip-0.9.3 (c (n "rzip") (v "0.9.3") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "regex") (r "^1.4.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "time") (r "^0.1.44") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)) (d (n "zip") (r "^0.5.12") (d #t) (k 0)))) (h "0jg4bsr66wviad61a03y2y8nrpfg2i5pvhmfp434naf5l4y62lp7")))

(define-public crate-rzip-0.9.4 (c (n "rzip") (v "0.9.4") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "zip") (r "^0.5") (d #t) (k 0)))) (h "1jm8hc7j06fxxzv7wz2xc6ilmrzyn2a1651nkc85czic9alndkaf")))

(define-public crate-rzip-0.9.5 (c (n "rzip") (v "0.9.5") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "zip") (r "^0.5") (d #t) (k 0)))) (h "1763qbc9x6xdsy4dbcqw57x1wp30277r8805fkbs10kkbam0q8zg")))

(define-public crate-rzip-0.9.6 (c (n "rzip") (v "0.9.6") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "zip") (r "^0.5") (d #t) (k 0)))) (h "00biwiw7phqfpgcfg3qbqwi1nzinv16279n4nqc8m968g5rsm8rx")))

(define-public crate-rzip-0.9.8 (c (n "rzip") (v "0.9.8") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "zip") (r "^0.5") (d #t) (k 0)))) (h "0pqprjm8rx1087db3im7l7rd7s2prnimhpvp9qvsarw1ksmsm3rc")))

(define-public crate-rzip-0.9.9 (c (n "rzip") (v "0.9.9") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "zip") (r "^0.5") (d #t) (k 0)))) (h "1imq8n7q78g00ib5idi2wh1fmk66dzwwfbbwcz4nylq7vfll81cc")))

(define-public crate-rzip-0.9.11 (c (n "rzip") (v "0.9.11") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "zip") (r "^0.6") (d #t) (k 0)))) (h "0z75rnq1hlbw388hj9bg80qy149mlq35y7hl97r8n1828cbpzkw3")))

(define-public crate-rzip-0.9.12 (c (n "rzip") (v "0.9.12") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "zip") (r "^0.6") (d #t) (k 0)))) (h "0j3ym4qgj3xwhgazv97vc9zd11n4q7c4bm75z9kid1kaidrq83cl")))

(define-public crate-rzip-0.9.13 (c (n "rzip") (v "0.9.13") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "zip") (r "^0.6") (d #t) (k 0)))) (h "1z1cw2a4q0h13wigy36hy0ia3xzlh5vw4ka64dqng6czjyi6qbvf")))

(define-public crate-rzip-0.9.14 (c (n "rzip") (v "0.9.14") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "zip") (r "^0.6") (d #t) (k 0)))) (h "14xkinnx3b68bkwikd5b135vix7pi3lx33z127llv6hr2kbjj9xf")))

(define-public crate-rzip-0.9.15 (c (n "rzip") (v "0.9.15") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "zip") (r "^0.6") (d #t) (k 0)))) (h "1zkggcm1qq3zpbjcyjcshg7nsjwa1pd86cgwiaj1d6y11ky9amkk")))

(define-public crate-rzip-0.9.16 (c (n "rzip") (v "0.9.16") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "zip") (r "^0.6") (d #t) (k 0)))) (h "0mxmy09f2v7llg1p6w2vp1ixpcfkh5gai5c7aykj0as1jjlvs5h2")))

(define-public crate-rzip-0.9.17 (c (n "rzip") (v "0.9.17") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "zip") (r "^0.6") (d #t) (k 0)))) (h "06bvhvcbfasjgarxvrkzsgvgjcxk4sbm8mai4if2innflb7x8g3v")))

(define-public crate-rzip-0.9.18 (c (n "rzip") (v "0.9.18") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "zip") (r "^0.6") (d #t) (k 0)))) (h "0z2f92hg1n7zw00f77mdl44mcaxn5vm0dr17aw04pr3khkwm0h9d")))

(define-public crate-rzip-0.9.19 (c (n "rzip") (v "0.9.19") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "zip") (r "^0.6") (d #t) (k 0)))) (h "0j1v75pnp1b3liydnr8f6v4366kdgz77jss4sy53k8nyvwq683hb")))

(define-public crate-rzip-0.9.20 (c (n "rzip") (v "0.9.20") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "zip") (r "^0.6") (d #t) (k 0)))) (h "1c0pp3zfy83mr35frc5wq49r21bvdw3q1572iprjx5z9fw1xlpm8")))

