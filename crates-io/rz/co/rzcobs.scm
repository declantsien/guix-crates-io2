(define-module (crates-io rz co rzcobs) #:use-module (crates-io))

(define-public crate-rzcobs-0.1.1 (c (n "rzcobs") (v "0.1.1") (d (list (d (n "hex-literal") (r "^0.3.1") (d #t) (k 2)))) (h "1iw942kl022vb0h2sydrhga6jpkbw4i29f9lnwgi530jg5rkylv1") (f (quote (("std") ("default" "std"))))))

(define-public crate-rzcobs-0.1.2 (c (n "rzcobs") (v "0.1.2") (d (list (d (n "hex-literal") (r "^0.3.1") (d #t) (k 2)))) (h "0f9ry89a801564pvy8q09b01hbf856z0wg8753lqr1f49fg7bx9a") (f (quote (("std") ("default" "std"))))))

