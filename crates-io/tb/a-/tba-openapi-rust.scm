(define-module (crates-io tb a- tba-openapi-rust) #:use-module (crates-io))

(define-public crate-tba-openapi-rust-3.8.2 (c (n "tba-openapi-rust") (v "3.8.2") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)) (d (n "uuid") (r "^1.0") (f (quote ("serde"))) (d #t) (k 0)))) (h "1adkpmlf7p8jfkw2m2h2p26bjll2kd7zf5k4kq6qwpsvcvg2k275")))

