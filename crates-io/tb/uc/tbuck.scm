(define-module (crates-io tb uc tbuck) #:use-module (crates-io))

(define-public crate-tbuck-1.0.0 (c (n "tbuck") (v "1.0.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "hashbrown") (r "^0.1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1p8sxskshsfspjswa2vfskpkbx9kz7laii76xsn2z8lym6d8j6a7")))

(define-public crate-tbuck-1.0.1 (c (n "tbuck") (v "1.0.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "hashbrown") (r "^0.1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0v719hylhgag3psyw9a7qfkf9brm48h72zvrnd5zr3p3g5f9cyc7")))

(define-public crate-tbuck-1.1.0 (c (n "tbuck") (v "1.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "hashbrown") (r "^0.1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "12dkr9vv9a2rlwm53bbd3hn91avgyp2qg7lk5v8i6m160rrzyi1j")))

