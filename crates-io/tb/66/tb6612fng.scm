(define-module (crates-io tb #{66}# tb6612fng) #:use-module (crates-io))

(define-public crate-tb6612fng-0.1.0 (c (n "tb6612fng") (v "0.1.0") (d (list (d (n "defmt") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.9") (d #t) (k 2)))) (h "0nyfkmg9pgkg86rsd5niryvhbgz17bbxz2wl1lphjww8d34d3rn6") (r "1.60")))

(define-public crate-tb6612fng-0.2.0 (c (n "tb6612fng") (v "0.2.0") (d (list (d (n "defmt") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "embedded-hal") (r "^1.0.0-rc.1") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.10.0-rc.2") (f (quote ("eh1"))) (d #t) (k 2)))) (h "1nv0x9a8b8y03315zsznrvm9b6g56nzqn4g7fv0c7raamh5xc0xk") (r "1.63")))

