(define-module (crates-io tb yt tbytes) #:use-module (crates-io))

(define-public crate-tbytes-0.1.0-alpha1 (c (n "tbytes") (v "0.1.0-alpha1") (d (list (d (n "env_logger") (r "^0.10.1") (d #t) (k 2)) (d (n "log") (r "^0.4.20") (d #t) (k 2)))) (h "0qjhylwyxn9ngsav7qvh3znydqqswikgxfhcx7amv6ybyi7r5ik5") (f (quote (("std" "alloc") ("alloc"))))))

(define-public crate-tbytes-0.1.0-alpha2 (c (n "tbytes") (v "0.1.0-alpha2") (d (list (d (n "env_logger") (r "^0.10.1") (d #t) (k 2)) (d (n "log") (r "^0.4.20") (d #t) (k 2)))) (h "1flqkv7rc0mnkxy5l54mi9758cjl7bkmvgv2bkd7887v8sxydlg1") (f (quote (("std" "alloc") ("alloc"))))))

(define-public crate-tbytes-0.1.0 (c (n "tbytes") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.11.3") (d #t) (k 2)) (d (n "log") (r "^0.4.21") (d #t) (k 2)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (o #t) (k 0)))) (h "0x6hzx0j5mkdz4s09hzif82vrwcfbgv8h5a36arxiwiww14dkfbd") (f (quote (("std" "alloc" "serde/std") ("alloc" "serde/alloc")))) (s 2) (e (quote (("serde" "dep:serde"))))))

