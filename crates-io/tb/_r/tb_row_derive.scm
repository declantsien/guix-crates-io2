(define-module (crates-io tb _r tb_row_derive) #:use-module (crates-io))

(define-public crate-tb_row_derive-1.0.0 (c (n "tb_row_derive") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (f (quote ("raw_value"))) (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1l0j8vgij0gq5p2bx9cc3riklfq61rghlj9shcrmb16wz2axksrm")))

(define-public crate-tb_row_derive-1.0.1 (c (n "tb_row_derive") (v "1.0.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (f (quote ("raw_value"))) (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0v4xaax8gc1q3blhd60i5qajwsyap1zxrp0qsrr16qx9i9y38fx2")))

