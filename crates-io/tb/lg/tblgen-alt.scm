(define-module (crates-io tb lg tblgen-alt) #:use-module (crates-io))

(define-public crate-tblgen-alt-0.3.1 (c (n "tblgen-alt") (v "0.3.1") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "05if9jxc6ywjfszg96i7q7hdxj3q8328k2d3v6wmp4x7pz565zxs") (f (quote (("llvm18-0") ("llvm17-0") ("llvm16-0") ("default" "llvm18-0"))))))

(define-public crate-tblgen-alt-0.3.2 (c (n "tblgen-alt") (v "0.3.2") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "03iwbbfsdm0nl34zl6kyfikcdhz581z8zzsx6p4dw07874g2dsjy") (f (quote (("llvm18-0") ("llvm17-0") ("llvm16-0") ("default" "llvm18-0"))))))

(define-public crate-tblgen-alt-0.3.3 (c (n "tblgen-alt") (v "0.3.3") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "158ws1yz481mgqpm6sx3yr7bbnx8qicrcxqmrlr8ylcbn62fca41") (f (quote (("llvm18-0") ("llvm17-0") ("llvm16-0") ("default" "llvm18-0"))))))

