(define-module (crates-io tb lg tblgen) #:use-module (crates-io))

(define-public crate-tblgen-0.1.0 (c (n "tblgen") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.66") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1p6fxqz5ng680blwsq728r8ajrr7xhbzps6kbbsbjjw2jqpwf3ka")))

(define-public crate-tblgen-0.1.1 (c (n "tblgen") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.66") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "18mny9nimisi8v6xvyqi0mmqs0ii5b9zynm2c51x0bdyazjaq7x4") (y #t)))

(define-public crate-tblgen-0.1.2 (c (n "tblgen") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.66") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1hzh66g05x2y6izbivmikr8jfaid0lnx9n1bgdzvq5jy4h4ll74s")))

(define-public crate-tblgen-0.1.3 (c (n "tblgen") (v "0.1.3") (d (list (d (n "bindgen") (r "^0.66") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "018askz09qm8mkamm9s1rkmzl2hn907v7wy8fgj5h7251ipagvmr")))

(define-public crate-tblgen-0.2.0 (c (n "tblgen") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.66") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1l88mxp9xgdcplasskzwl6sgl8yk99r3ffl7193f3lwj48q8xilf")))

(define-public crate-tblgen-0.2.1 (c (n "tblgen") (v "0.2.1") (d (list (d (n "bindgen") (r "^0.66") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1rjs5j4a9i6lcjky0vxbniyrfv8jmqxpy4qyvmmazv4ncxkgsdzr")))

(define-public crate-tblgen-0.3.0 (c (n "tblgen") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.66") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "06vb0vfj2vjdxf5wwmcynnsa00s7s523066i31kv3f7ycs9c069x") (f (quote (("llvm17-0") ("llvm16-0") ("default" "llvm16-0"))))))

