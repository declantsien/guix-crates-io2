(define-module (crates-io tb um tbump) #:use-module (crates-io))

(define-public crate-tbump-1.0.3 (c (n "tbump") (v "1.0.3") (d (list (d (n "anyhow") (r "^1.0.28") (d #t) (k 0)) (d (n "colored") (r "^1.9.3") (d #t) (k 0)) (d (n "ignore") (r "^0.4.14") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.106") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.106") (d #t) (k 0)) (d (n "structopt") (r "^0.3.14") (d #t) (k 0)) (d (n "structopt-derive") (r "^0.4.7") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)))) (h "0vvihii8b3a8b4nj13cmqhhwsjr7rqnlpi0my7qfz7m5510l709d") (y #t)))

