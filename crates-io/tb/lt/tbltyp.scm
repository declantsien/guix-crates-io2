(define-module (crates-io tb lt tbltyp) #:use-module (crates-io))

(define-public crate-tbltyp-0.1.0 (c (n "tbltyp") (v "0.1.0") (d (list (d (n "ansistr") (r "^0.1.1") (d #t) (k 0)))) (h "1gikinxqwnvskzjii26csmfs76i8i4yd2sdc817vk24j1vga00zd")))

(define-public crate-tbltyp-0.1.1 (c (n "tbltyp") (v "0.1.1") (d (list (d (n "ansistr") (r "^0.1.1") (d #t) (k 0)))) (h "0kxlr0527jxww74rb3d2r67j57yqxa6wpl665aq4sks1spzd7ix4")))

