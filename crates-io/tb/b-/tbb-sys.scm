(define-module (crates-io tb b- tbb-sys) #:use-module (crates-io))

(define-public crate-tbb-sys-1.0.0+2021.5.0 (c (n "tbb-sys") (v "1.0.0+2021.5.0") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)) (d (n "glob") (r "^0.3") (d #t) (k 1)))) (h "1cqh5xdg7234w1ikh6rlv6zglxgbbvbxsra1cp0d082k00gv5smy") (l "tbb")))

(define-public crate-tbb-sys-1.0.1+2021.5.0 (c (n "tbb-sys") (v "1.0.1+2021.5.0") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)) (d (n "glob") (r "^0.3") (d #t) (k 1)))) (h "1ndcaavx1ip0vpc2mnwyp1mk71wiq7qlcpk736288vb1wv8h5i54") (l "tbb")))

(define-public crate-tbb-sys-1.0.2+2021.5.0 (c (n "tbb-sys") (v "1.0.2+2021.5.0") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)) (d (n "glob") (r "^0.3") (d #t) (k 1)))) (h "17qdninazgy7lz48z9a6kwn5y1jz06ljcs0yczvyx1l4lh41vzi8") (l "tbb")))

(define-public crate-tbb-sys-1.1.0+2021.5.0 (c (n "tbb-sys") (v "1.1.0+2021.5.0") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "glob") (r "^0.3") (d #t) (k 1)))) (h "1vnqlbqawn20v4dlmsra6jfw7fc9brhz70zl5g64zwa01mly2gxy") (l "tbb")))

