(define-module (crates-io tb -o tb-openpgp-certs) #:use-module (crates-io))

(define-public crate-tb-openpgp-certs-0.1.0 (c (n "tb-openpgp-certs") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "diesel") (r "^1.4") (f (quote ("sqlite"))) (d #t) (k 0)) (d (n "openpgp-keylist") (r "^0.2") (d #t) (k 0)) (d (n "sequoia-net") (r "^0.23") (d #t) (k 0)) (d (n "sequoia-openpgp") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^0.2.22") (f (quote ("fs" "io-std" "io-util" "rt-threaded" "sync" "signal" "macros"))) (d #t) (k 0)))) (h "0cqh0di5v096rfi3giarlk8bj6vp4b96xm18pj9jk85ampk1g5si")))

