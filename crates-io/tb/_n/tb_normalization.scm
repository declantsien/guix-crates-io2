(define-module (crates-io tb _n tb_normalization) #:use-module (crates-io))

(define-public crate-tb_normalization-0.9.9 (c (n "tb_normalization") (v "0.9.9") (d (list (d (n "regex") (r "^1.3.1") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.8") (d #t) (k 0)))) (h "1kwsjwxrgdv06yy0xv9f2vxc18yv7hjvm93fhbbb63rq7znxw0gv")))

(define-public crate-tb_normalization-1.0.0 (c (n "tb_normalization") (v "1.0.0") (d (list (d (n "regex") (r "^1.3.1") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.8") (d #t) (k 0)))) (h "18kqdds3vd34pcji84ws43g1h18fwww5i2a159k9rc52xh257r1x")))

