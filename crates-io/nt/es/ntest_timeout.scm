(define-module (crates-io nt es ntest_timeout) #:use-module (crates-io))

(define-public crate-ntest_timeout-0.3.0 (c (n "ntest_timeout") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "timebomb") (r "^0.1.2") (d #t) (k 0)))) (h "0xml9sha88n7vyw130af028djrq87q02an3sjbd870gfas6kmmzz")))

(define-public crate-ntest_timeout-0.3.1 (c (n "ntest_timeout") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "timebomb") (r "^0.1.2") (d #t) (k 0)))) (h "12irjy39s09l2x0cjdhzxq771g0isndxjfaf5air3jb662qlnyp9")))

(define-public crate-ntest_timeout-0.3.2 (c (n "ntest_timeout") (v "0.3.2") (d (list (d (n "ntest") (r "0.*") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "timebomb") (r "^0.1.2") (d #t) (k 0)))) (h "0nahwz88b7a1z1zj15mmcsidi1qggigc3c35qpv8pmrpg0m72r4b")))

(define-public crate-ntest_timeout-0.3.3 (c (n "ntest_timeout") (v "0.3.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "timebomb") (r "^0.1.2") (d #t) (k 0)))) (h "0klryn3rgjxnq3cv6j8bwcsr0b7zw3x216h63144v22aja18p0g0")))

(define-public crate-ntest_timeout-0.4.0 (c (n "ntest_timeout") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1j7d72pjzln4kg8783mpkgf2rp3pkpz1ra1qzsj2rkz7n57zhnwn")))

(define-public crate-ntest_timeout-0.4.1 (c (n "ntest_timeout") (v "0.4.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1mshmgwpzsiigm390y2hkprawh2dj98h89xh5vg189rnwn3ygn6k")))

(define-public crate-ntest_timeout-0.5.0 (c (n "ntest_timeout") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "04sk134ggmsx4pdva7vi5df7jmw5hr7nywdj5ns0s1kskby508bb")))

(define-public crate-ntest_timeout-0.5.1 (c (n "ntest_timeout") (v "0.5.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0a9fav1isf1a1rwipkk2gwwrkcv4g5847b049ar7sjfab39rc923")))

(define-public crate-ntest_timeout-0.6.0 (c (n "ntest_timeout") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "16paigy10raidh3gsnd2vkl8i3mxy7ayd6xq1mx6jsys8y92qyww")))

(define-public crate-ntest_timeout-0.7.0 (c (n "ntest_timeout") (v "0.7.0") (d (list (d (n "ntest_proc_macro_helper") (r "^0.7.0") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^0.1.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1b6f29l29h0c6hyqbskr2qfyii5dz8ik1wn66yyaww6k35k1c0hw")))

(define-public crate-ntest_timeout-0.7.1 (c (n "ntest_timeout") (v "0.7.1") (d (list (d (n "ntest_proc_macro_helper") (r "^0.7.1") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^0.1.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "026isn882xadp9c0pcfzhiz01zj7qn31mkmgcbmy8x3gd4dqx7rr")))

(define-public crate-ntest_timeout-0.7.2 (c (n "ntest_timeout") (v "0.7.2") (d (list (d (n "ntest_proc_macro_helper") (r "^0.7.1") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^0.1.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0x1kkr01msz395ywhycqbm8319lrbdnhlmnqrpv6fix8cwhs7i4r")))

(define-public crate-ntest_timeout-0.7.3 (c (n "ntest_timeout") (v "0.7.3") (d (list (d (n "ntest_proc_macro_helper") (r "^0.7.1") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^0.1.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1ryiaz3hrd8n60y5c64nsas6qk88v07nd912c2qlq8nl60630w58")))

(define-public crate-ntest_timeout-0.7.4 (c (n "ntest_timeout") (v "0.7.4") (d (list (d (n "ntest_proc_macro_helper") (r "^0.7") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0niyhcnq90xq080ca0gvqdc7xw631p2m9rqlzx09i4xznr1fj356")))

(define-public crate-ntest_timeout-0.7.5 (c (n "ntest_timeout") (v "0.7.5") (d (list (d (n "ntest_proc_macro_helper") (r "^0.7") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "08v3r6hggh43qabl887pkz88k6lg6hrc62mppxyabb0pw44v03di")))

(define-public crate-ntest_timeout-0.8.0 (c (n "ntest_timeout") (v "0.8.0") (d (list (d (n "ntest_proc_macro_helper") (r "^0.7") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1rj36jqajiqhsz60hs4knwv2x07lszlxnifwfwn3k32v6waw5vzq")))

(define-public crate-ntest_timeout-0.8.1 (c (n "ntest_timeout") (v "0.8.1") (d (list (d (n "ntest_proc_macro_helper") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "01vcdlz9xj471z5knk2qynm7adz3p614glf6n0pgn161qynym9mw")))

(define-public crate-ntest_timeout-0.9.0 (c (n "ntest_timeout") (v "0.9.0") (d (list (d (n "proc-macro-crate") (r "^1.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1948a5ps329acg8fy2c2dyjgc8f96l0gin271cpl0yjq420lcsq6")))

(define-public crate-ntest_timeout-0.9.1 (c (n "ntest_timeout") (v "0.9.1") (d (list (d (n "proc-macro-crate") (r ">=1.1, <=3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1avh5rsphav1lq2g06rf3xfiv0240wq9rjcamwwcf8a0sklhqaii")))

(define-public crate-ntest_timeout-0.9.2 (c (n "ntest_timeout") (v "0.9.2") (d (list (d (n "proc-macro-crate") (r ">=1.1, <=3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0px2967063kyb95kckvmkmz539hzi8i4grw7n98c140gz1f2njgg")))

