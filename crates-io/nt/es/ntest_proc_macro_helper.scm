(define-module (crates-io nt es ntest_proc_macro_helper) #:use-module (crates-io))

(define-public crate-ntest_proc_macro_helper-0.7.0 (c (n "ntest_proc_macro_helper") (v "0.7.0") (h "10s5m9y46n3fc20ba9v4ykck2zzi2dmg065xjc1xdw39hpmqv0s5")))

(define-public crate-ntest_proc_macro_helper-0.7.1 (c (n "ntest_proc_macro_helper") (v "0.7.1") (h "0xkyp5yc91bc3bc8441sjgdia3l6g7nclhy53nq5zw2fk3q6ww3y")))

(define-public crate-ntest_proc_macro_helper-0.7.2 (c (n "ntest_proc_macro_helper") (v "0.7.2") (h "0ig302qm7agxbs6vz0f5ngdqrl7f9hd6j8ff8l5dc6pibqz1v5vs")))

(define-public crate-ntest_proc_macro_helper-0.7.3 (c (n "ntest_proc_macro_helper") (v "0.7.3") (h "1xr2zk77slhyd01gzdd7nskka596li399la82yjpq532i4i64m8i")))

(define-public crate-ntest_proc_macro_helper-0.7.4 (c (n "ntest_proc_macro_helper") (v "0.7.4") (h "0sg65hzn1hz09m0wq6nrlnspqkgmxqypnrhfv8g2jf951xxx7j7g")))

(define-public crate-ntest_proc_macro_helper-0.7.5 (c (n "ntest_proc_macro_helper") (v "0.7.5") (h "0lkyfx97aynbm7cnhzyc9cr0rpq1xzng1hwmzizbf1a6855y6llg")))

(define-public crate-ntest_proc_macro_helper-0.8.0 (c (n "ntest_proc_macro_helper") (v "0.8.0") (h "154r3r9nnnp6qjzlayc54213bdrgdk8b68jjnn1xcyd6cz92iqx0")))

