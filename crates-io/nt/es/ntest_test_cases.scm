(define-module (crates-io nt es ntest_test_cases) #:use-module (crates-io))

(define-public crate-ntest_test_cases-0.1.0 (c (n "ntest_test_cases") (v "0.1.0") (h "0b3q8xys12swpv5mwpcss5frxvh6z12c38pfgl342hcgv8bk4pwr")))

(define-public crate-ntest_test_cases-0.1.1 (c (n "ntest_test_cases") (v "0.1.1") (h "16wb98zllx62246d26l4dnz3y89an107iiacwvaz76bi1kjrl19j")))

(define-public crate-ntest_test_cases-0.1.3 (c (n "ntest_test_cases") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15.44") (f (quote ("full"))) (d #t) (k 0)))) (h "0i76j3r179wm8l3h40mc3x2vr724mqyah9a989s4nj778g9kppq5")))

(define-public crate-ntest_test_cases-0.1.4 (c (n "ntest_test_cases") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15.44") (f (quote ("full"))) (d #t) (k 0)))) (h "0g3ljv7lxxfhi7n75ik6q3dbfnwzxhzwjhnjxw9g82lrs9xpsckd")))

(define-public crate-ntest_test_cases-0.1.5 (c (n "ntest_test_cases") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15.44") (f (quote ("full"))) (d #t) (k 0)))) (h "1j234vsml41vgbgcb41rlqf7qbn8r6bcvdmqkg29msx8z0vqnnl0")))

(define-public crate-ntest_test_cases-0.1.6 (c (n "ntest_test_cases") (v "0.1.6") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15.44") (f (quote ("full"))) (d #t) (k 0)))) (h "1dfamv0ws2isvp7f7hhgjdw89w2hah47alkwdhv6g070qs4czwza")))

(define-public crate-ntest_test_cases-0.1.7 (c (n "ntest_test_cases") (v "0.1.7") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15.44") (f (quote ("full"))) (d #t) (k 0)))) (h "1klfjdw0gxxz15j807ggkkgfx1amvdy1w1jimyl34yhpvwr7q1vb")))

(define-public crate-ntest_test_cases-0.1.8 (c (n "ntest_test_cases") (v "0.1.8") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0wljh1z1q5s7r7q0xy4qdi6qss3z0gdb758bkqqlrnl7yj41fgki")))

(define-public crate-ntest_test_cases-0.1.9 (c (n "ntest_test_cases") (v "0.1.9") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "11bzx08v9b55c3kwvzrf49z9b6ddig6cxpvlajqjrcsiqdfq58m0")))

(define-public crate-ntest_test_cases-0.1.10 (c (n "ntest_test_cases") (v "0.1.10") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1s91i8zxpjnm19jpilfx0wmcicwn27f9g8yylqci0ig94fgv014l")))

(define-public crate-ntest_test_cases-0.1.11 (c (n "ntest_test_cases") (v "0.1.11") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0k3p0iqw2v1hldv18ggjqklhmaxyz3ky9j0zl87crvmalaafxndn")))

(define-public crate-ntest_test_cases-0.1.12 (c (n "ntest_test_cases") (v "0.1.12") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0hd2v9rb276aanw0cjria16ds6395l4nbirp2x5zgqij1n756602")))

(define-public crate-ntest_test_cases-0.1.13 (c (n "ntest_test_cases") (v "0.1.13") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0vid35hajx4vd86pxfx88nk7xq3r0f14hv5cjiw8ggd095yfrs76")))

(define-public crate-ntest_test_cases-0.2.0 (c (n "ntest_test_cases") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1j5jhma3va3hl5zpq6aq70kz9cv8qx37cd9lpr80w7gqh3360bcz")))

(define-public crate-ntest_test_cases-0.2.1 (c (n "ntest_test_cases") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1r1m2cc1jw4xv8nnjkv23d48hxlv9vadrzqyfi9yiqynm4znc24r")))

(define-public crate-ntest_test_cases-0.3.0 (c (n "ntest_test_cases") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1br14cavzqxf6bszik55p1lj2rv26zlwvwgv6cf2ysh17fy3qw95")))

(define-public crate-ntest_test_cases-0.3.1 (c (n "ntest_test_cases") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "01y3712v9h63cpbvarqciwk59dn6vkkjrqblvwyhwaabr814s5wd")))

(define-public crate-ntest_test_cases-0.3.2 (c (n "ntest_test_cases") (v "0.3.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "01y6fvrrb6fvm08gyhd6jj3f8130aa1skdw7s9wvimmcwvrx6wzh")))

(define-public crate-ntest_test_cases-0.3.3 (c (n "ntest_test_cases") (v "0.3.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0z8wiin2fgnyfsc0xljibbkn169m3ivwkg39pp4dqm8c9yhrz84b")))

(define-public crate-ntest_test_cases-0.3.4 (c (n "ntest_test_cases") (v "0.3.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0b67m368599b2zgwx19psqz6n3m9m5532h1257x6vz1pym3gd2na")))

(define-public crate-ntest_test_cases-0.4.0 (c (n "ntest_test_cases") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0pwzcpbp8p1r4iclyp05pvpdhajp2dswzcfsdm9addavw412079w")))

(define-public crate-ntest_test_cases-0.4.1 (c (n "ntest_test_cases") (v "0.4.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "102kym7ryb2rilml3cp5q0lylls6b8zmppzc98khwgbns2akba0l")))

(define-public crate-ntest_test_cases-0.5.0 (c (n "ntest_test_cases") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "19q3gfh6q6rlnm7k91xrfn7scqg3xs34491d065ihwivxib6d3rd")))

(define-public crate-ntest_test_cases-0.5.1 (c (n "ntest_test_cases") (v "0.5.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "03fxnrkv05yhpfx4jvhh0akkq9lz1jsdky2wryi4x8scx8d9kmx1")))

(define-public crate-ntest_test_cases-0.6.0 (c (n "ntest_test_cases") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0czp9skf0z6bpbcismywnf42cx5s9q50ahdq1vwplbx477w3c1d3")))

(define-public crate-ntest_test_cases-0.7.0 (c (n "ntest_test_cases") (v "0.7.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "13nar2fijda2c62x6jkk4f5s5riib4h5qr6kp7wmcr4c9sj3nl2r")))

(define-public crate-ntest_test_cases-0.7.1 (c (n "ntest_test_cases") (v "0.7.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0d1h1w33sznc1y3l0ka5xa1mpznn8qqxjv3s5qi9y2fs7091fcvz")))

(define-public crate-ntest_test_cases-0.7.2 (c (n "ntest_test_cases") (v "0.7.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "00w3ifjjp9w4si7sswzifxyvq48ls0k05w06gwzgirqz6i4miy9n")))

(define-public crate-ntest_test_cases-0.7.3 (c (n "ntest_test_cases") (v "0.7.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0dw29cb06dd46iw1k4w10q1r40yl3s5zc7ngqn0ca5472h0k4gn0")))

(define-public crate-ntest_test_cases-0.7.4 (c (n "ntest_test_cases") (v "0.7.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1wa5lw8xsrmk4vrs41ngif242mf20h96vjmdggghn1w31wvwvl38")))

(define-public crate-ntest_test_cases-0.7.5 (c (n "ntest_test_cases") (v "0.7.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1ghal2rb03cnj7ciqgdq0dvifdf8qp2hnmi9z1ip1j5b02s1xa4r")))

(define-public crate-ntest_test_cases-0.8.0 (c (n "ntest_test_cases") (v "0.8.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "03ip2dpi7fd2wyz99yd17w302nci3b05slbl3rr6dfs2683ayz3g")))

(define-public crate-ntest_test_cases-0.9.0 (c (n "ntest_test_cases") (v "0.9.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "08ifw9zhm1l93wh24k8zrk25sj3k9vpw29sfwq4lsvwwf6z36zdy")))

(define-public crate-ntest_test_cases-0.9.1 (c (n "ntest_test_cases") (v "0.9.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0a0lfrih8im3alnnz27rcxqwrzi4wlfm032g2sh0rwzsralfr18h")))

(define-public crate-ntest_test_cases-0.9.2 (c (n "ntest_test_cases") (v "0.9.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0cjk3vp5mxlhwqk1d30i13ksjh0k7kx3ir3kc7gga3xq29ngyzhr")))

