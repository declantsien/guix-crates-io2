(define-module (crates-io nt es ntest) #:use-module (crates-io))

(define-public crate-ntest-0.1.0 (c (n "ntest") (v "0.1.0") (h "1fr69qcvd728ap39n12m3y8d33rkjh37fvr2a1hxyy4p9xyzawpz")))

(define-public crate-ntest-0.1.1 (c (n "ntest") (v "0.1.1") (d (list (d (n "ntest_test_cases") (r "^0.1.0") (d #t) (k 0)) (d (n "ntest_test_cases") (r "^0.1.0") (d #t) (k 2)))) (h "1j9xdb9jnrwgchlj7zdfv17fc91lsg9dxx649aa0jr7035mm91di")))

(define-public crate-ntest-0.1.2 (c (n "ntest") (v "0.1.2") (d (list (d (n "ntest_test_cases") (r "^0.1.1") (d #t) (k 0)) (d (n "ntest_test_cases") (r "^0.1.1") (d #t) (k 2)))) (h "1vp3l3b6yjs0nba1gnnqgmg1sjpjrcz3slwrf1zfxkm0a2ich59q")))

(define-public crate-ntest-0.1.3 (c (n "ntest") (v "0.1.3") (d (list (d (n "ntest_test_cases") (r "^0.1.3") (d #t) (k 0)) (d (n "ntest_test_cases") (r "^0.1.3") (d #t) (k 2)))) (h "0846iqbq8igs5qlqd0pd7iyp0irqi5afz09rb5vaw1cb88iw7k2g")))

(define-public crate-ntest-0.1.4 (c (n "ntest") (v "0.1.4") (d (list (d (n "ntest_test_cases") (r "^0.1.4") (d #t) (k 0)) (d (n "ntest_test_cases") (r "^0.1.4") (d #t) (k 2)))) (h "14a7zl307gak3rvvq16zdvk4i5zm8l882fsi76dgqnj39chcn99i")))

(define-public crate-ntest-0.1.5 (c (n "ntest") (v "0.1.5") (d (list (d (n "ntest_test_cases") (r "^0.1.5") (d #t) (k 0)) (d (n "ntest_test_cases") (r "^0.1.5") (d #t) (k 2)))) (h "0qry67rnfwh9f71lizmav4xwgy59nxv8liy9qha3wgbn6054wzln")))

(define-public crate-ntest-0.1.6 (c (n "ntest") (v "0.1.6") (d (list (d (n "ntest_test_cases") (r "^0.1.6") (d #t) (k 0)) (d (n "ntest_test_cases") (r "^0.1.6") (d #t) (k 2)))) (h "0yh6i1aajg8gmbikcfqdmyv1a22afr2n3gh3m7wvla036r25d2gr")))

(define-public crate-ntest-0.1.7 (c (n "ntest") (v "0.1.7") (d (list (d (n "ntest_test_cases") (r "^0.1.7") (d #t) (k 0)) (d (n "ntest_test_cases") (r "^0.1.7") (d #t) (k 2)))) (h "0i00vjc2hhq13xn5k4m9cav2a14n7kzpynydiwsxb3pa41sq1v2c")))

(define-public crate-ntest-0.1.8 (c (n "ntest") (v "0.1.8") (d (list (d (n "ntest_test_cases") (r "^0.1.8") (d #t) (k 0)) (d (n "ntest_test_cases") (r "^0.1.8") (d #t) (k 2)))) (h "1lrr0vnbp9kim5dp3mh48ymrsn3kvcn5q8gnpggawwkm486airv6")))

(define-public crate-ntest-0.1.10 (c (n "ntest") (v "0.1.10") (d (list (d (n "ntest_test_cases") (r "^0.1.10") (d #t) (k 0)) (d (n "ntest_test_cases") (r "^0.1.10") (d #t) (k 2)))) (h "0anjiam0w32a8p2c9lfy2bgw062lrxq9ij63jfh8m8r60znl2mbb")))

(define-public crate-ntest-0.1.11 (c (n "ntest") (v "0.1.11") (d (list (d (n "ntest_test_cases") (r "^0.1.11") (d #t) (k 0)) (d (n "ntest_test_cases") (r "^0.1.11") (d #t) (k 2)))) (h "12n7kl6wfx36iaipwkqn3yhmslhfgr0bvldiyvn22ixp0lnbjjbz")))

(define-public crate-ntest-0.1.12 (c (n "ntest") (v "0.1.12") (d (list (d (n "ntest_test_cases") (r "^0.1.12") (d #t) (k 0)) (d (n "ntest_test_cases") (r "^0.1.12") (d #t) (k 2)))) (h "1v3rncd9rdhas7rl5znc4d70ymhflr6pkw0n9kyrzj2z6z2kgyyf")))

(define-public crate-ntest-0.1.13 (c (n "ntest") (v "0.1.13") (d (list (d (n "ntest_test_cases") (r "^0.1.13") (d #t) (k 0)) (d (n "ntest_test_cases") (r "^0.1.13") (d #t) (k 2)))) (h "0c9f86ny3snyf6z22383qm195vjb1p7qvkh4jx88ghpf59hzh0rz")))

(define-public crate-ntest-0.2.0 (c (n "ntest") (v "0.2.0") (d (list (d (n "ntest_test_cases") (r "^0.2.0") (d #t) (k 0)) (d (n "ntest_test_cases") (r "^0.2.0") (d #t) (k 2)))) (h "1cc5k5v1123k9pqv7525vw9qr01x9wbmzg309k8f187233a1ijp3")))

(define-public crate-ntest-0.2.1 (c (n "ntest") (v "0.2.1") (d (list (d (n "ntest_test_cases") (r "^0.2.1") (d #t) (k 0)) (d (n "ntest_test_cases") (r "^0.2.1") (d #t) (k 2)))) (h "1m5n7b10lzcylvfh8x0m7f0xrps4lkj71xdh5fdrjvy3d8s36h3x")))

(define-public crate-ntest-0.3.0 (c (n "ntest") (v "0.3.0") (d (list (d (n "ntest_test_cases") (r "^0.3.0") (d #t) (k 0)) (d (n "ntest_test_cases") (r "^0.3.0") (d #t) (k 2)) (d (n "ntest_timeout") (r "^0.3.0") (d #t) (k 0)) (d (n "ntest_timeout") (r "^0.3.0") (d #t) (k 2)))) (h "1la49bdjd2p7i91iwnhp8vryc5lih2bsld4phppq8aj7gsw0bhyk")))

(define-public crate-ntest-0.3.1 (c (n "ntest") (v "0.3.1") (d (list (d (n "ntest_test_cases") (r "^0.3.1") (d #t) (k 0)) (d (n "ntest_test_cases") (r "^0.3.1") (d #t) (k 2)) (d (n "ntest_timeout") (r "^0.3.1") (d #t) (k 0)) (d (n "ntest_timeout") (r "^0.3.1") (d #t) (k 2)) (d (n "timebomb") (r "^0.1.2") (d #t) (k 0)) (d (n "timebomb") (r "^0.1.2") (d #t) (k 2)))) (h "1wdnj5dzxgijar95gqi49821zac8qycsmn701xpx1awh8hsydg77")))

(define-public crate-ntest-0.3.3 (c (n "ntest") (v "0.3.3") (d (list (d (n "ntest_test_cases") (r "^0.3.3") (d #t) (k 0)) (d (n "ntest_test_cases") (r "^0.3.3") (d #t) (k 2)) (d (n "ntest_timeout") (r "^0.3.3") (d #t) (k 0)) (d (n "ntest_timeout") (r "^0.3.3") (d #t) (k 2)) (d (n "timebomb") (r "^0.1.2") (d #t) (k 0)) (d (n "timebomb") (r "^0.1.2") (d #t) (k 2)))) (h "04cljndihkcqqwj061bgpnxyv7wqbd3f91ag1b3ryrayn7rrclxv")))

(define-public crate-ntest-0.4.0 (c (n "ntest") (v "0.4.0") (d (list (d (n "ntest_test_cases") (r "^0.4.0") (d #t) (k 0)) (d (n "ntest_test_cases") (r "^0.4.0") (d #t) (k 2)) (d (n "ntest_timeout") (r "^0.4.0") (d #t) (k 0)) (d (n "ntest_timeout") (r "^0.4.0") (d #t) (k 2)))) (h "0ki9z6n1qzafybr0gsr8jb6y5z1xpiml7j64p895jgqm3g8sjb1y")))

(define-public crate-ntest-0.4.1 (c (n "ntest") (v "0.4.1") (d (list (d (n "ntest_test_cases") (r "^0.4.1") (d #t) (k 0)) (d (n "ntest_test_cases") (r "^0.4.1") (d #t) (k 2)) (d (n "ntest_timeout") (r "^0.4.1") (d #t) (k 0)) (d (n "ntest_timeout") (r "^0.4.1") (d #t) (k 2)))) (h "11ra4dl2850g7py4dqzhj0lmqk2gg8r7l107lbvfw7hvf39sffpd")))

(define-public crate-ntest-0.5.0 (c (n "ntest") (v "0.5.0") (d (list (d (n "ntest_test_cases") (r "^0.5.0") (d #t) (k 0)) (d (n "ntest_test_cases") (r "^0.5.0") (d #t) (k 2)) (d (n "ntest_timeout") (r "^0.5.0") (d #t) (k 0)) (d (n "ntest_timeout") (r "^0.5.0") (d #t) (k 2)))) (h "0jbsw64ckxa3bjznp73p1h9c86r2nivwfkaqnargvyqb240xz9zk")))

(define-public crate-ntest-0.5.1 (c (n "ntest") (v "0.5.1") (d (list (d (n "ntest_test_cases") (r "^0.5.1") (d #t) (k 0)) (d (n "ntest_test_cases") (r "^0.5.1") (d #t) (k 2)) (d (n "ntest_timeout") (r "^0.5.1") (d #t) (k 0)) (d (n "ntest_timeout") (r "^0.5.1") (d #t) (k 2)))) (h "0991b3xx8qdlmfszsy2bvkcnlsdd64c9q6qa314ad92kyff77afx")))

(define-public crate-ntest-0.6.0 (c (n "ntest") (v "0.6.0") (d (list (d (n "ntest_test_cases") (r "^0.6.0") (d #t) (k 0)) (d (n "ntest_test_cases") (r "^0.6.0") (d #t) (k 2)) (d (n "ntest_timeout") (r "^0.6.0") (d #t) (k 0)) (d (n "ntest_timeout") (r "^0.6.0") (d #t) (k 2)))) (h "1id2m8nwsv7dpf81zjnyr4vzs4nfrxf92p95mnyn25bqdr0967fx")))

(define-public crate-ntest-0.7.0 (c (n "ntest") (v "0.7.0") (d (list (d (n "ntest_proc_macro_helper") (r "^0.7.0") (d #t) (k 0)) (d (n "ntest_test_cases") (r "^0.7.0") (d #t) (k 0)) (d (n "ntest_timeout") (r "^0.7.0") (d #t) (k 0)))) (h "1h89lxa6xq7v7z9c3502l6x0xs21p8kh6c8l73g57cjjxy88zxnf")))

(define-public crate-ntest-0.7.1 (c (n "ntest") (v "0.7.1") (d (list (d (n "ntest_proc_macro_helper") (r "^0.7.1") (d #t) (k 0)) (d (n "ntest_test_cases") (r "^0.7.1") (d #t) (k 0)) (d (n "ntest_timeout") (r "^0.7.1") (d #t) (k 0)))) (h "0kkk30ixlvfc97gpdjfv5584qmkhgx3mnphqf6rl2sxfckzxrg5k")))

(define-public crate-ntest-0.7.2 (c (n "ntest") (v "0.7.2") (d (list (d (n "ntest_proc_macro_helper") (r "^0.7.2") (d #t) (k 0)) (d (n "ntest_test_cases") (r "^0.7.2") (d #t) (k 0)) (d (n "ntest_timeout") (r "^0.7.2") (d #t) (k 0)))) (h "0w3cpd42hvpfkhvbvkjqnm3b31mbnzw0yvxg2slb0bimkhgrx47k")))

(define-public crate-ntest-0.7.3 (c (n "ntest") (v "0.7.3") (d (list (d (n "ntest_proc_macro_helper") (r "^0.7.3") (d #t) (k 0)) (d (n "ntest_test_cases") (r "^0.7.3") (d #t) (k 0)) (d (n "ntest_timeout") (r "^0.7.3") (d #t) (k 0)))) (h "0czzb8gmnfbazzmgfqwf6020rm87bmlgqql0z2742sd8i9nayk4q")))

(define-public crate-ntest-0.7.4 (c (n "ntest") (v "0.7.4") (d (list (d (n "ntest_proc_macro_helper") (r "^0.7.4") (d #t) (k 0)) (d (n "ntest_test_cases") (r "^0.7.4") (d #t) (k 0)) (d (n "ntest_timeout") (r "^0.7.4") (d #t) (k 0)) (d (n "tokio") (r "^1.6.1") (f (quote ("rt" "macros"))) (d #t) (k 2)))) (h "1qdfvghmq84s2kj0g0hnfd6i9sjj6vjp7jdswincvy8mj302r0mc")))

(define-public crate-ntest-0.7.5 (c (n "ntest") (v "0.7.5") (d (list (d (n "ntest_proc_macro_helper") (r "^0.7.5") (d #t) (k 0)) (d (n "ntest_test_cases") (r "^0.7.5") (d #t) (k 0)) (d (n "ntest_timeout") (r "^0.7.5") (d #t) (k 0)) (d (n "tokio") (r "^1.6.1") (f (quote ("rt" "macros"))) (d #t) (k 2)))) (h "0i4xsvx52hmcnga2xbjl74hdylz4jy8bc2swcichlvw1di4lwm2w")))

(define-public crate-ntest-0.8.0 (c (n "ntest") (v "0.8.0") (d (list (d (n "ntest_proc_macro_helper") (r "^0.8.0") (d #t) (k 0)) (d (n "ntest_test_cases") (r "^0.8.0") (d #t) (k 0)) (d (n "ntest_timeout") (r "^0.8.0") (d #t) (k 0)) (d (n "tokio") (r "^1.6.1") (f (quote ("rt" "macros"))) (d #t) (k 2)))) (h "04xzyvhqx64ifz3zavy0s8ic8y5zbwb0gkfsj46rv81ham2yf794")))

(define-public crate-ntest-0.8.1 (c (n "ntest") (v "0.8.1") (d (list (d (n "ntest_proc_macro_helper") (r "^0.8.0") (d #t) (k 0)) (d (n "ntest_test_cases") (r "^0.8.0") (d #t) (k 0)) (d (n "ntest_timeout") (r "^0.8.1") (d #t) (k 0)) (d (n "tokio") (r "^1.6.1") (f (quote ("rt" "macros"))) (d #t) (k 2)))) (h "1yyih3b0bcr9gj11m9xj330as2sjihblkmb2bmv10lp38q5m0rg8")))

(define-public crate-ntest-0.9.0 (c (n "ntest") (v "0.9.0") (d (list (d (n "ntest_test_cases") (r "^0.9.0") (d #t) (k 0)) (d (n "ntest_timeout") (r "^0.9.0") (d #t) (k 0)) (d (n "tokio") (r "^1.6.1") (f (quote ("rt" "macros"))) (d #t) (k 2)))) (h "04pmi0y7rzpkngv7lqw48c2831bn15lg8nkgj9z30i9xnz9cd3ns")))

(define-public crate-ntest-0.9.1 (c (n "ntest") (v "0.9.1") (d (list (d (n "ntest_test_cases") (r "^0.9.1") (d #t) (k 0)) (d (n "ntest_timeout") (r "^0.9.1") (d #t) (k 0)) (d (n "tokio") (r "^1.6.1") (f (quote ("rt" "macros"))) (d #t) (k 2)))) (h "1sw84h99fw3i0ich05bxpgg4j8hsdvwb1nmjryr540mgrfx5irxd")))

(define-public crate-ntest-0.9.2 (c (n "ntest") (v "0.9.2") (d (list (d (n "ntest_test_cases") (r "^0.9.2") (d #t) (k 0)) (d (n "ntest_timeout") (r "^0.9.2") (d #t) (k 0)) (d (n "tokio") (r "^1.6.1") (f (quote ("rt" "macros"))) (d #t) (k 2)))) (h "1mmdmxs14fkkd08l4h9nv6pdl29xjpb0r9bwgqv6aa4rwsi1dka1")))

