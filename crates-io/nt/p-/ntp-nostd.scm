(define-module (crates-io nt p- ntp-nostd) #:use-module (crates-io))

(define-public crate-ntp-nostd-0.0.1 (c (n "ntp-nostd") (v "0.0.1") (d (list (d (n "tungstenite") (r "^0.20.0") (f (quote ("rustls"))) (d #t) (k 2)) (d (n "url") (r "^2.4.0") (d #t) (k 2)))) (h "1h6yqp1v9zl1cnc0hlq24lglqa05ick8z49jvvkfbf66ic2aziv5")))

