(define-module (crates-io nt p- ntp-parser) #:use-module (crates-io))

(define-public crate-ntp-parser-0.1.1 (c (n "ntp-parser") (v "0.1.1") (d (list (d (n "nom") (r "^1.2.4") (d #t) (k 0)))) (h "1dil06lxnfx35qyha98cp8mksgd77q1md9vmbfvpph31yxa2zhjf")))

(define-public crate-ntp-parser-0.1.2 (c (n "ntp-parser") (v "0.1.2") (d (list (d (n "nom") (r "^1.2.4") (d #t) (k 0)))) (h "0dgl06nsva8gwjckrfr98880a4yk5gmfn38d3piqji6d7bj286sa")))

(define-public crate-ntp-parser-0.1.3 (c (n "ntp-parser") (v "0.1.3") (d (list (d (n "enum_primitive") (r "^0.1") (d #t) (k 0)) (d (n "nom") (r "^2.0") (d #t) (k 0)))) (h "1r70c4abqjb4cqpr4bvgah31r46k2riyffnqgsdhggjyk20jyvs2")))

(define-public crate-ntp-parser-0.1.4 (c (n "ntp-parser") (v "0.1.4") (d (list (d (n "enum_primitive") (r "^0.1") (d #t) (k 0)) (d (n "nom") (r "^2.0") (d #t) (k 0)))) (h "0z0rcfmqf6b7spa5i4dly90c82aqaqq6d1ngxyc27pzrc5blg1nx")))

(define-public crate-ntp-parser-0.1.5 (c (n "ntp-parser") (v "0.1.5") (d (list (d (n "enum_primitive") (r "^0.1") (d #t) (k 0)) (d (n "nom") (r "^3") (d #t) (k 0)))) (h "0pnzqfd5yazp6q5jm6gvzp60sadamig44sv83w3rdajj1i7w0ha9")))

(define-public crate-ntp-parser-0.2.0 (c (n "ntp-parser") (v "0.2.0") (d (list (d (n "nom") (r "^3") (d #t) (k 0)))) (h "1r9dv3nldi8jv4aq46vkcp9m02dk9q8vs8ijf6b2py1bw9fnk90s")))

(define-public crate-ntp-parser-0.2.1 (c (n "ntp-parser") (v "0.2.1") (d (list (d (n "nom") (r "^3") (d #t) (k 0)))) (h "0gsv0klr0mpcpaiw75my8i9gih0rz6akzzkqkfjhr0y0cgd1rz9i")))

(define-public crate-ntp-parser-0.3.0 (c (n "ntp-parser") (v "0.3.0") (d (list (d (n "nom") (r "^4.1") (d #t) (k 0)))) (h "0fz3ss7snnk6pblnncgadm7knyc15p2nqhwaiy45kl65cv6qmkqx")))

(define-public crate-ntp-parser-0.4.0 (c (n "ntp-parser") (v "0.4.0") (d (list (d (n "nom") (r "^5.0") (d #t) (k 0)))) (h "1mpzm05jvcz7q6mgd5y0lc0bcy3ndrg1b61wx9lhz9a9rfybvp5l")))

(define-public crate-ntp-parser-0.5.0 (c (n "ntp-parser") (v "0.5.0") (d (list (d (n "nom") (r "^6.0") (d #t) (k 0)) (d (n "nom-derive") (r "^0.7") (d #t) (k 0)))) (h "17jb21wwc546ml3qqvzgdx4xzdcccqzwan2krzd7jav3nm2y8qky")))

(define-public crate-ntp-parser-0.6.0 (c (n "ntp-parser") (v "0.6.0") (d (list (d (n "nom") (r "^7.0") (d #t) (k 0)) (d (n "nom-derive") (r "^0.10") (d #t) (k 0)))) (h "19dpmfwkfjjgf841zf0lrfp98gyr3arkjknx6rrlhba3pzlln23n")))

