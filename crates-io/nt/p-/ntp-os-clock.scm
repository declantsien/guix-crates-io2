(define-module (crates-io nt p- ntp-os-clock) #:use-module (crates-io))

(define-public crate-ntp-os-clock-0.3.0-alpha.0 (c (n "ntp-os-clock") (v "0.3.0-alpha.0") (d (list (d (n "libc") (r "^0.2.139") (d #t) (k 0)) (d (n "ntp-proto") (r "^0.3.0-alpha.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.39") (d #t) (k 0)))) (h "0xp6yp2k2i5sglp8qzfb0n3lmaxscdv42g66fmqssvwqich6pcm6")))

(define-public crate-ntp-os-clock-0.3.0-alpha.1 (c (n "ntp-os-clock") (v "0.3.0-alpha.1") (d (list (d (n "libc") (r "^0.2.139") (d #t) (k 0)) (d (n "ntp-proto") (r "^0.3.0-alpha.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.39") (d #t) (k 0)))) (h "0xm2zpvx5r3pzw0y9p3l69dsjvfha3rnd0vzqsz5rbllbrg3n9pl")))

(define-public crate-ntp-os-clock-0.3.0-alpha.2 (c (n "ntp-os-clock") (v "0.3.0-alpha.2") (d (list (d (n "libc") (r "^0.2.139") (d #t) (k 0)) (d (n "ntp-proto") (r "^0.3.0-alpha.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.39") (d #t) (k 0)))) (h "1wb882pzz65faj7xgl3qzxrd53vyczrrv8m1j09mx5ly2iiy5ab3")))

(define-public crate-ntp-os-clock-0.3.0-alpha.3 (c (n "ntp-os-clock") (v "0.3.0-alpha.3") (d (list (d (n "libc") (r "^0.2.139") (d #t) (k 0)) (d (n "ntp-proto") (r "^0.3.0-alpha.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.39") (d #t) (k 0)))) (h "1815165v72f1srcawzgv381xmgl05nb7mbmqyf1qrjnam4rm4l98")))

(define-public crate-ntp-os-clock-0.3.0-alpha.4 (c (n "ntp-os-clock") (v "0.3.0-alpha.4") (d (list (d (n "libc") (r "^0.2.139") (d #t) (k 0)) (d (n "ntp-proto") (r "^0.3.0-alpha.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.39") (d #t) (k 0)))) (h "19a7rjcy0vx8r4xzhnkjfpyyn1dyb6240wm4k1lfhafvaniqdip8")))

(define-public crate-ntp-os-clock-0.3.0 (c (n "ntp-os-clock") (v "0.3.0") (d (list (d (n "libc") (r "^0.2.141") (d #t) (k 0)) (d (n "ntp-proto") (r "^0.3.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "1609is2nq3c9hxdxzhwkcdwg02z5a5dzl978csnhwjlz94kb4f1i")))

(define-public crate-ntp-os-clock-0.3.1 (c (n "ntp-os-clock") (v "0.3.1") (d (list (d (n "libc") (r "^0.2.141") (d #t) (k 0)) (d (n "ntp-proto") (r "^0.3.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "135n2apjc25nn4x9n32ns8x73hm6yqf9bb9f486zhmkb8s5sscqp")))

(define-public crate-ntp-os-clock-0.3.2 (c (n "ntp-os-clock") (v "0.3.2") (d (list (d (n "libc") (r "^0.2.141") (d #t) (k 0)) (d (n "ntp-proto") (r "^0.3.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "1m5wdfsyixmn2sckmkkq9ww0102a3mzvxfhkqnlw2lakylyjm27m")))

(define-public crate-ntp-os-clock-0.3.3 (c (n "ntp-os-clock") (v "0.3.3") (d (list (d (n "libc") (r "^0.2.144") (d #t) (k 0)) (d (n "ntp-proto") (r "^0.3.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "1mqqyns339lvzm72npa2mrf7qk8jqjwm61qjfwxvw9c24n4vgzgz")))

(define-public crate-ntp-os-clock-0.3.5 (c (n "ntp-os-clock") (v "0.3.5") (d (list (d (n "libc") (r "^0.2.146") (d #t) (k 0)) (d (n "ntp-proto") (r "^0.3.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "0i3jkv8qf9lb6q629dp994mjj4l16ss3sb13xz5mgzcil7ck2jd6")))

(define-public crate-ntp-os-clock-0.3.6 (c (n "ntp-os-clock") (v "0.3.6") (d (list (d (n "libc") (r "^0.2.147") (d #t) (k 0)) (d (n "ntp-proto") (r "^0.3.6") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "1zfi9m1yi3vmcqhs2b88fymyi4kh8r5lakax7gdlicfi170kswyr")))

(define-public crate-ntp-os-clock-0.3.7 (c (n "ntp-os-clock") (v "0.3.7") (d (list (d (n "libc") (r "^0.2.147") (d #t) (k 0)) (d (n "ntp-proto") (r "^0.3.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "0pjy6jarhlnhcikq5k6c9x8jjbn32lp5zwcsvn82sls6gmz344qa")))

(define-public crate-ntp-os-clock-1.0.0-dev.20230907 (c (n "ntp-os-clock") (v "1.0.0-dev.20230907") (d (list (d (n "libc") (r "^0.2.145") (d #t) (k 0)) (d (n "ntp-proto") (r "^1.0.0-dev.20230907") (f (quote ("__internal-api"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.21") (d #t) (k 0)))) (h "0hh5vxhjkdd3j0bzyi9mmgd452xi0nw7n61rdmk7wric8cc5vqdj")))

(define-public crate-ntp-os-clock-1.0.0-rc.1 (c (n "ntp-os-clock") (v "1.0.0-rc.1") (d (list (d (n "libc") (r "^0.2.145") (d #t) (k 0)) (d (n "ntp-proto") (r "^1.0.0-rc.1") (f (quote ("__internal-api"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.21") (d #t) (k 0)))) (h "0900fwghg0qymssqv57zzgfla5hdzgd7sc2v827qiq29c8sfybbc") (r "1.66.0")))

(define-public crate-ntp-os-clock-1.0.0-rc.2 (c (n "ntp-os-clock") (v "1.0.0-rc.2") (d (list (d (n "libc") (r "^0.2.145") (d #t) (k 0)) (d (n "ntp-proto") (r "^1.0.0-rc.2") (f (quote ("__internal-api"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.21") (d #t) (k 0)))) (h "0njncd689jx9lm93mivja0idyrsfk91pi0kprdy8qjgawgni4ihc") (r "1.66.0")))

(define-public crate-ntp-os-clock-1.0.0-rc.3 (c (n "ntp-os-clock") (v "1.0.0-rc.3") (d (list (d (n "libc") (r "^0.2.145") (d #t) (k 0)) (d (n "ntp-proto") (r "^1.0.0-rc.3") (f (quote ("__internal-api"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.21") (d #t) (k 0)))) (h "1kc2bpy8chwwbfckmrcc4sr4ilip0pp5sq56zk76yk8fkklhl31v") (r "1.66.0")))

(define-public crate-ntp-os-clock-1.0.0-rc.4 (c (n "ntp-os-clock") (v "1.0.0-rc.4") (d (list (d (n "libc") (r "^0.2.145") (d #t) (k 0)) (d (n "ntp-proto") (r "^1.0.0-rc.4") (f (quote ("__internal-api"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.21") (d #t) (k 0)))) (h "1x9bn4w15kh4r18pzm1yadasxbw17gisvbm5yjla1vzm7134lp39") (r "1.66.0")))

(define-public crate-ntp-os-clock-1.0.0-rc.5 (c (n "ntp-os-clock") (v "1.0.0-rc.5") (d (list (d (n "libc") (r "^0.2.145") (d #t) (k 0)) (d (n "ntp-proto") (r "^1.0.0-rc.5") (f (quote ("__internal-api"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.21") (d #t) (k 0)))) (h "0f2xl1hb1i9n008f323glwbaicg0iaqjjch7cyy120dhfacs5fxh") (r "1.66.0")))

(define-public crate-ntp-os-clock-1.0.0 (c (n "ntp-os-clock") (v "1.0.0") (d (list (d (n "libc") (r "^0.2.145") (d #t) (k 0)) (d (n "ntp-proto") (r "^1.0.0") (f (quote ("__internal-api"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.21") (d #t) (k 0)))) (h "13dhpm6cdhbkn1vb2g40z7jqwc6gm0pqhf4c76sky46l7faq8vg3") (r "1.66.0")))

(define-public crate-ntp-os-clock-1.1.0-alpha.20231123 (c (n "ntp-os-clock") (v "1.1.0-alpha.20231123") (d (list (d (n "libc") (r "^0.2.145") (d #t) (k 0)) (d (n "ntp-proto") (r "^1.1.0-alpha.20231123") (f (quote ("__internal-api"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.21") (d #t) (k 0)))) (h "12czcib3jxlxf1n4fs6cg3g9nlp52xwzf2v1815zn2a9rqp2w58h") (r "1.67")))

(define-public crate-ntp-os-clock-1.1.0 (c (n "ntp-os-clock") (v "1.1.0") (d (list (d (n "libc") (r "^0.2.145") (d #t) (k 0)) (d (n "ntp-proto") (r "^1.1.0") (f (quote ("__internal-api"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.21") (d #t) (k 0)))) (h "1qbxpn0l7hkm3wm10kkd3wdi9hgsvk20hzs1ylpj30zd7ha9dkn5") (r "1.67")))

(define-public crate-ntp-os-clock-1.1.1-alpha.20231221 (c (n "ntp-os-clock") (v "1.1.1-alpha.20231221") (d (list (d (n "libc") (r "^0.2.145") (d #t) (k 0)) (d (n "ntp-proto") (r "^1.1.1-alpha.20231221") (f (quote ("__internal-api"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.21") (d #t) (k 0)))) (h "1zsxkb76zxmmplqpa42py11aqd2wk2y1p6irw252q7rxr01w4p75") (r "1.67")))

(define-public crate-ntp-os-clock-1.1.1-alpha.20240119 (c (n "ntp-os-clock") (v "1.1.1-alpha.20240119") (d (list (d (n "libc") (r "^0.2.145") (d #t) (k 0)) (d (n "ntp-proto") (r "^1.1.1-alpha.20240119") (f (quote ("__internal-api"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.21") (d #t) (k 0)))) (h "0zigwvk3p4vnh9kfh197ccnl0j2xcpkrvwrr9cqi0v4kznvlihh0") (r "1.67")))

(define-public crate-ntp-os-clock-1.1.1 (c (n "ntp-os-clock") (v "1.1.1") (d (list (d (n "libc") (r "^0.2.145") (d #t) (k 0)) (d (n "ntp-proto") (r "^1.1.1") (f (quote ("__internal-api"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.21") (d #t) (k 0)))) (h "025h9yhl7ri266h9cazs8g08g838m4x8cg6zdd038gdn19279mpy") (r "1.67")))

(define-public crate-ntp-os-clock-1.1.2 (c (n "ntp-os-clock") (v "1.1.2") (d (list (d (n "libc") (r "^0.2.145") (d #t) (k 0)) (d (n "ntp-proto") (r "^1.1.2") (f (quote ("__internal-api"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.21") (d #t) (k 0)))) (h "1mp5r9xy10xzhbhghbp8ak34x8mpj1s0bn5yslh5c6spq3svhvf2") (r "1.67")))

