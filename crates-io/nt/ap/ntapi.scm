(define-module (crates-io nt ap ntapi) #:use-module (crates-io))

(define-public crate-ntapi-0.1.0 (c (n "ntapi") (v "0.1.0") (d (list (d (n "winapi") (r "^0.3.5") (f (quote ("cfg" "evntrace" "in6addr" "inaddr" "minwinbase" "ntsecapi" "windef" "winioctl"))) (d #t) (k 0)))) (h "03nhgc58gsih4mslphai8lns11a4n5f3qw5p5r2199dwn35g5d0j") (f (quote (("user") ("nightly") ("kernel") ("impl-default" "winapi/impl-default") ("func-types") ("default" "user")))) (y #t)))

(define-public crate-ntapi-0.1.1 (c (n "ntapi") (v "0.1.1") (d (list (d (n "winapi") (r "^0.3.5") (f (quote ("cfg" "evntrace" "in6addr" "inaddr" "minwinbase" "ntsecapi" "windef" "winioctl"))) (d #t) (k 0)))) (h "1v7dbmdwlfcrxa301825yy1zzf8a7rphn9vf2whg104360rx8k3d") (f (quote (("user") ("nightly") ("kernel") ("impl-default" "winapi/impl-default") ("func-types") ("default" "user"))))))

(define-public crate-ntapi-0.2.0 (c (n "ntapi") (v "0.2.0") (d (list (d (n "winapi") (r "^0.3.6") (f (quote ("cfg" "evntrace" "in6addr" "inaddr" "minwinbase" "ntsecapi" "windef" "winioctl"))) (d #t) (k 0)))) (h "1qkkbavrghdsfwd64r1bswm3iy5xsxwsqh9srh1p6f4y4js13flp") (f (quote (("user") ("nightly") ("kernel") ("impl-default" "winapi/impl-default") ("func-types") ("default" "user"))))))

(define-public crate-ntapi-0.3.0 (c (n "ntapi") (v "0.3.0") (d (list (d (n "winapi") (r "^0.3.6") (f (quote ("cfg" "evntrace" "in6addr" "inaddr" "minwinbase" "ntsecapi" "windef" "winioctl"))) (d #t) (k 0)))) (h "13wjlynz87z9zbp1n3f1x8fdl18djz131fpjs3zahk28a1hsrnmw") (f (quote (("user") ("nightly") ("kernel") ("impl-default" "winapi/impl-default") ("func-types") ("default" "user"))))))

(define-public crate-ntapi-0.3.1 (c (n "ntapi") (v "0.3.1") (d (list (d (n "winapi") (r "^0.3.6") (f (quote ("cfg" "evntrace" "in6addr" "inaddr" "minwinbase" "ntsecapi" "windef" "winioctl"))) (d #t) (k 0)))) (h "1mvv4d23iyipvn45l9k829qw6xj81z3lkscrhql9cp763mlww0hp") (f (quote (("user") ("nightly") ("kernel") ("impl-default" "winapi/impl-default") ("func-types") ("default" "user"))))))

(define-public crate-ntapi-0.3.2 (c (n "ntapi") (v "0.3.2") (d (list (d (n "winapi") (r "^0.3.6") (f (quote ("cfg" "evntrace" "in6addr" "inaddr" "minwinbase" "ntsecapi" "windef" "winioctl"))) (d #t) (k 0)))) (h "1mn3waz29xbq76jfzrdjfcvk1vl3fiwql3x5jgp69aw9xl3f0n64") (f (quote (("user") ("nightly") ("kernel") ("impl-default" "winapi/impl-default") ("func-types") ("default" "user"))))))

(define-public crate-ntapi-0.3.3 (c (n "ntapi") (v "0.3.3") (d (list (d (n "winapi") (r "^0.3.6") (f (quote ("cfg" "evntrace" "in6addr" "inaddr" "minwinbase" "ntsecapi" "windef" "winioctl"))) (d #t) (k 0)))) (h "00h6xbvvlkiln9sy34p3s19a7kw00dvvmz1hgq4brb43v4f08vpj") (f (quote (("user") ("nightly") ("kernel") ("impl-default" "winapi/impl-default") ("func-types") ("default" "user"))))))

(define-public crate-ntapi-0.3.4 (c (n "ntapi") (v "0.3.4") (d (list (d (n "winapi") (r "^0.3.6") (f (quote ("cfg" "evntrace" "in6addr" "inaddr" "minwinbase" "ntsecapi" "windef" "winioctl"))) (d #t) (k 0)))) (h "1lm9rxl625zcmakkyj00nca19p7a3mby7q6s5p3kjd82x9yr6cbs") (f (quote (("user") ("nightly") ("kernel") ("impl-default" "winapi/impl-default") ("func-types") ("default" "user"))))))

(define-public crate-ntapi-0.3.5 (c (n "ntapi") (v "0.3.5") (d (list (d (n "winapi") (r "^0.3.9") (f (quote ("cfg" "evntrace" "in6addr" "inaddr" "minwinbase" "ntsecapi" "windef" "winioctl"))) (d #t) (k 0)))) (h "00vfmp6aa47kv09yzjkmfh1k1xqkzhaawri8ziwmkycmvamsnmxp") (f (quote (("user") ("nightly") ("kernel") ("impl-default" "winapi/impl-default") ("func-types") ("default" "user"))))))

(define-public crate-ntapi-0.3.6 (c (n "ntapi") (v "0.3.6") (d (list (d (n "winapi") (r "^0.3.9") (f (quote ("cfg" "evntrace" "in6addr" "inaddr" "minwinbase" "ntsecapi" "windef" "winioctl"))) (d #t) (k 0)))) (h "0i5daj9sr8wyi5jkpwpybln2jqpn59z0mqfc0dpdidipwh1bjsrz") (f (quote (("user") ("nightly") ("kernel") ("impl-default" "winapi/impl-default") ("func-types") ("default" "user"))))))

(define-public crate-ntapi-0.3.7 (c (n "ntapi") (v "0.3.7") (d (list (d (n "winapi") (r "^0.3.9") (f (quote ("cfg" "evntrace" "in6addr" "inaddr" "minwinbase" "ntsecapi" "windef" "winioctl"))) (d #t) (k 0)))) (h "03wqq2x5jv5xrsirbxmm460gcfmpakjpq8yqmc5lzfrgznkp91y2") (f (quote (("user") ("nightly" "beta") ("kernel") ("impl-default" "winapi/impl-default") ("func-types") ("default" "user") ("beta"))))))

(define-public crate-ntapi-0.4.0 (c (n "ntapi") (v "0.4.0") (d (list (d (n "winapi") (r "^0.3.9") (f (quote ("cfg" "evntrace" "in6addr" "inaddr" "minwinbase" "ntsecapi" "windef" "winioctl"))) (d #t) (k 0)))) (h "1z2a984vrmcg895gg34zjadfnmjic9nfachjyx0mj81b6rxxnldw") (f (quote (("user") ("kernel") ("impl-default" "winapi/impl-default") ("func-types") ("default" "user"))))))

(define-public crate-ntapi-0.4.1 (c (n "ntapi") (v "0.4.1") (d (list (d (n "winapi") (r "^0.3.9") (f (quote ("cfg" "evntrace" "in6addr" "inaddr" "minwinbase" "ntsecapi" "windef" "winioctl"))) (d #t) (k 0)))) (h "1r38zhbwdvkis2mzs6671cm1p6djgsl49i7bwxzrvhwicdf8k8z8") (f (quote (("user") ("kernel") ("impl-default" "winapi/impl-default") ("func-types") ("default" "user"))))))

