(define-module (crates-io nt ri ntriple) #:use-module (crates-io))

(define-public crate-ntriple-0.1.0 (c (n "ntriple") (v "0.1.0") (d (list (d (n "peg") (r "^0.5") (d #t) (k 1)))) (h "05skqdm4k0w2v0zpa333c5md4vyj8zg63gfd4knajv7mkrhx1cby")))

(define-public crate-ntriple-0.1.1 (c (n "ntriple") (v "0.1.1") (d (list (d (n "peg") (r "^0.5") (d #t) (k 1)))) (h "0c07dcnl8wc72nlq4fddgk8y8fj9s8hk5qc4pbj33wfxfk7vf3q2")))

