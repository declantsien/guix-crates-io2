(define-module (crates-io nt co ntcore-sys) #:use-module (crates-io))

(define-public crate-ntcore-sys-0.1.0 (c (n "ntcore-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.33") (d #t) (k 1)))) (h "0m12zmmsvkz2271my6v2risa143nzagd506ciw2j916f5hncsf5k")))

(define-public crate-ntcore-sys-0.1.1 (c (n "ntcore-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.33") (d #t) (k 1)))) (h "0aakcfiawaz94h3pxqd699fjf66p8nymxlc0dqi3aimbqwkb882h")))

