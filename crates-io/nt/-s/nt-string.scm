(define-module (crates-io nt -s nt-string) #:use-module (crates-io))

(define-public crate-nt-string-0.1.0 (c (n "nt-string") (v "0.1.0") (d (list (d (n "displaydoc") (r "^0.2.3") (k 0)) (d (n "widestring") (r "^1.0.2") (k 0)))) (h "1bbanvh7b7y55cicpzxfzizljy31c6xxi3m9iwj7r26gd0z157a2") (f (quote (("std" "alloc") ("default" "std") ("allocator_api") ("alloc" "widestring/alloc")))) (r "1.58")))

(define-public crate-nt-string-0.1.1 (c (n "nt-string") (v "0.1.1") (d (list (d (n "displaydoc") (r "^0.2.3") (k 0)) (d (n "widestring") (r "^1.0.2") (k 0)))) (h "0v0c7an4v3id7hagv9vna8m65fvzdqlfx79vnn3fh1clknqp6kzn") (f (quote (("std" "alloc") ("default" "std") ("allocator_api") ("alloc" "widestring/alloc")))) (r "1.58")))

