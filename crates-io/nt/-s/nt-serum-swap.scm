(define-module (crates-io nt -s nt-serum-swap) #:use-module (crates-io))

(define-public crate-NT-serum-swap-0.4.1 (c (n "NT-serum-swap") (v "0.4.1") (d (list (d (n "karima-anchor-lang") (r "^0.19.0") (d #t) (k 0)) (d (n "karima-anchor-spl") (r "^0.19.0") (f (quote ("dex"))) (d #t) (k 0)) (d (n "solana-program") (r "=1.8.5") (d #t) (k 0)))) (h "11bdfslh411f91pf7fnnfwxw98kih5bckswh5mgcg9rhwkqz4f9d") (f (quote (("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-NT-serum-swap-0.4.2 (c (n "NT-serum-swap") (v "0.4.2") (d (list (d (n "karima-anchor-lang") (r "^0.19.0") (d #t) (k 0)) (d (n "karima-anchor-spl") (r "^0.19.0") (f (quote ("dex"))) (d #t) (k 0)) (d (n "solana-program") (r "^1.7.11") (d #t) (k 0)))) (h "1csl6f8c4jxgvhpxznrvdj87ib99msnj8rambq72g1x85bzq0wk9") (f (quote (("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-NT-serum-swap-0.4.3 (c (n "NT-serum-swap") (v "0.4.3") (d (list (d (n "karima-anchor-lang") (r "^0.19.0") (d #t) (k 0)) (d (n "karima-anchor-spl") (r "^0.19.0") (f (quote ("dex"))) (d #t) (k 0)) (d (n "solana-program") (r "^1.7.11") (d #t) (k 0)))) (h "1n9q67ik5y6hikbi63iwvc81ilxzcxrqp1xylw5g54mw9l1m9x9p") (f (quote (("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-NT-serum-swap-0.4.4 (c (n "NT-serum-swap") (v "0.4.4") (d (list (d (n "karima-anchor-lang") (r "^0.19.0") (d #t) (k 0)) (d (n "karima-anchor-spl") (r "^0.19.0") (f (quote ("dex"))) (d #t) (k 0)) (d (n "solana-program") (r "^1.7.11") (d #t) (k 0)))) (h "1psdz1irfk29cwxsvbdj6aky7h2idz8jbqkgijy0j6wvh1qbh8x7") (f (quote (("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-NT-serum-swap-0.4.5 (c (n "NT-serum-swap") (v "0.4.5") (d (list (d (n "NT-anchor-lang") (r "^0.19.0") (d #t) (k 0)) (d (n "NT-anchor-spl") (r "^0.19.2") (f (quote ("dex"))) (d #t) (k 0)) (d (n "solana-program") (r "=1.8.5") (d #t) (k 0)))) (h "0hdm4xqs4p66z4qc5w74ic5fdchgagrqf6kympmgjf67j6jz96g5") (f (quote (("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-NT-serum-swap-0.4.6 (c (n "NT-serum-swap") (v "0.4.6") (d (list (d (n "NT-anchor-lang") (r "^0.19.0") (d #t) (k 0)) (d (n "NT-anchor-spl") (r "=0.19.2") (f (quote ("dex"))) (d #t) (k 0)) (d (n "solana-program") (r "^1.8.5") (d #t) (k 0)))) (h "0nmxaglripyaxwdhq10i697hycjf07yf29jl4gfmdvgzvjbbffrq") (f (quote (("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

