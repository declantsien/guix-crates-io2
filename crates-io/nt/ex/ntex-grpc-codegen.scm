(define-module (crates-io nt ex ntex-grpc-codegen) #:use-module (crates-io))

(define-public crate-ntex-grpc-codegen-0.1.0 (c (n "ntex-grpc-codegen") (v "0.1.0") (d (list (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "ntex-prost-build") (r "^0.10.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1wxzg4zw3vmrrk4s0nfnq9pgk7zbir3by6p756kcyy5aazhip3l2")))

(define-public crate-ntex-grpc-codegen-0.1.1 (c (n "ntex-grpc-codegen") (v "0.1.1") (d (list (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "ntex-prost-build") (r "^0.10.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0p9ap391gzgp23mfnhvvdyqm40dg813900mx7j1agc5g49c8k6wf")))

(define-public crate-ntex-grpc-codegen-0.1.2 (c (n "ntex-grpc-codegen") (v "0.1.2") (d (list (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "ntex-prost-build") (r "^0.10.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "03bd41kb5ciqdd1a0xsc10zqzgp26amsjwykqnk3k0ri3kgyapbv")))

(define-public crate-ntex-grpc-codegen-0.1.3 (c (n "ntex-grpc-codegen") (v "0.1.3") (d (list (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "ntex-prost-build") (r "^0.10.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "13fsnqwxvgjmlmy4sg6k17ca9vx6b6acdany1mj822w3klq9356f")))

(define-public crate-ntex-grpc-codegen-0.1.4 (c (n "ntex-grpc-codegen") (v "0.1.4") (d (list (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "ntex-prost-build") (r "^0.10.4") (d #t) (k 0)))) (h "0bw5hpfzl50m41cxhz5h7pyzb0ssa85ilxasvy912i77mwsibhy5")))

(define-public crate-ntex-grpc-codegen-0.1.5 (c (n "ntex-grpc-codegen") (v "0.1.5") (d (list (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "ntex-prost-build") (r "^0.10.4") (d #t) (k 0)))) (h "03mpsp1g7pzakwj0a3f6h61afa0bdx04iq15lfanikx7d05jr1ww")))

(define-public crate-ntex-grpc-codegen-0.1.6 (c (n "ntex-grpc-codegen") (v "0.1.6") (d (list (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "ntex-prost-build") (r "^0.10.4") (d #t) (k 0)))) (h "09lcl5102x0npfxagzaqnfpp4kwzq3qlxzc51ax0s0xmdzrzx7xm")))

(define-public crate-ntex-grpc-codegen-0.2.0-b.0 (c (n "ntex-grpc-codegen") (v "0.2.0-b.0") (d (list (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "ntex-prost-build") (r "^0.11.0-b.0") (d #t) (k 0)))) (h "15vszpyjghy2wmyfsdhg3ryjxc171nm8cx25n125fbvxkknzmhbl")))

(define-public crate-ntex-grpc-codegen-0.2.0-b.1 (c (n "ntex-grpc-codegen") (v "0.2.0-b.1") (d (list (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "ntex-prost-build") (r "^0.11.0-b.1") (d #t) (k 0)))) (h "0n27df825jg769k0njd17nsn7gpjaf40416pf6pkslf5v3vvg10h")))

(define-public crate-ntex-grpc-codegen-0.2.0 (c (n "ntex-grpc-codegen") (v "0.2.0") (d (list (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "ntex-prost-build") (r "^0.11.0") (d #t) (k 0)))) (h "0sdri1bkb8b9v5v76xdkrc3sj04crwfjwgq51691khva4b6yc6fw")))

(define-public crate-ntex-grpc-codegen-0.2.1 (c (n "ntex-grpc-codegen") (v "0.2.1") (d (list (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "ntex-prost-build") (r "^0.11.1") (d #t) (k 0)))) (h "1hww8p2y9xvzmhq15rz87xfyn9pminvnzly7dd3ar14kn12ff4h2")))

(define-public crate-ntex-grpc-codegen-0.2.2 (c (n "ntex-grpc-codegen") (v "0.2.2") (d (list (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "ntex-prost-build") (r "^0.11.2") (d #t) (k 0)))) (h "1gaf1rg8ydxgr7pmcc1hvgijz7k5nab1qiijizqhmlq1zqys4j1w")))

(define-public crate-ntex-grpc-codegen-0.2.3 (c (n "ntex-grpc-codegen") (v "0.2.3") (d (list (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "ntex-prost-build") (r "^0.11.3") (d #t) (k 0)))) (h "0jjxz5561wsvb31i3q42mb1xg6bz5v1vxp8px0rnib0qsjr9rmfz")))

(define-public crate-ntex-grpc-codegen-0.2.4 (c (n "ntex-grpc-codegen") (v "0.2.4") (d (list (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "ntex-prost-build") (r "^0.11.4") (d #t) (k 0)))) (h "0yii83qf2a8s6l30rmpwxpri4kmis8lagdcam9kvwxaph22dx9g3")))

(define-public crate-ntex-grpc-codegen-0.2.5 (c (n "ntex-grpc-codegen") (v "0.2.5") (d (list (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "ntex-prost-build") (r "^0.11.5") (d #t) (k 0)))) (h "1hnpn3lj1hkzc8xk335jkps82dq9mbi84yf5g562q2wb2bmv8im7")))

(define-public crate-ntex-grpc-codegen-0.2.6 (c (n "ntex-grpc-codegen") (v "0.2.6") (d (list (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "ntex-prost-build") (r "^0.11.6") (d #t) (k 0)))) (h "1a1bzdkz3a8a9m72yj0b28g1gv6y4rnn46lrfn8ckhpn343hnrmi")))

(define-public crate-ntex-grpc-codegen-0.2.7 (c (n "ntex-grpc-codegen") (v "0.2.7") (d (list (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "ntex-prost-build") (r "^0.11.7") (d #t) (k 0)))) (h "0v4fq18xagvn8jd6mhwiq3xrx7l14yi4jypxzfxp7sk39c0aqn1y")))

(define-public crate-ntex-grpc-codegen-0.2.8 (c (n "ntex-grpc-codegen") (v "0.2.8") (d (list (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "ntex-prost-build") (r "^0.11.8") (d #t) (k 0)))) (h "037hgjcrsfypyfv0mwhmyldfk09zxsmraj0fzqmal12ix3j5jnvr")))

(define-public crate-ntex-grpc-codegen-0.2.9 (c (n "ntex-grpc-codegen") (v "0.2.9") (d (list (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "ntex-prost-build") (r "^0.11.9") (d #t) (k 0)))) (h "1pp9qcxqdfj6by16fqfln43jf801s4wvn69nkgnim9ndwj3g3bvc")))

(define-public crate-ntex-grpc-codegen-0.2.10 (c (n "ntex-grpc-codegen") (v "0.2.10") (d (list (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "ntex-prost-build") (r "^0.11.10") (d #t) (k 0)))) (h "0aryhdk5p1r52c20nbm4k41sz0d48jfacd2ygqj067jb74pi375x")))

(define-public crate-ntex-grpc-codegen-0.2.11 (c (n "ntex-grpc-codegen") (v "0.2.11") (d (list (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "ntex-prost-build") (r "^0.11.11") (d #t) (k 0)))) (h "10vw0rxbkf92dgc413g32vshmsdkwp8dr3i9c4k2smanjy9m74w2")))

