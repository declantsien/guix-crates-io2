(define-module (crates-io nt ex ntex-grpc-derive) #:use-module (crates-io))

(define-public crate-ntex-grpc-derive-0.1.0 (c (n "ntex-grpc-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.3") (f (quote ("full" "extra-traits" "fold"))) (d #t) (k 0)))) (h "0d14lmilsw7i288jixfkvm8apxs2zvkbmjigvizxvn92n9l2lax4")))

(define-public crate-ntex-grpc-derive-0.2.0 (c (n "ntex-grpc-derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.3") (f (quote ("full" "extra-traits" "fold"))) (d #t) (k 0)))) (h "0c9gahc0vnyisrm4a2i5mhcf8ajnax4i66fca1b28pg2xm09cv70")))

(define-public crate-ntex-grpc-derive-0.3.0-beta.0 (c (n "ntex-grpc-derive") (v "0.3.0-beta.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.3") (f (quote ("full" "extra-traits" "fold"))) (d #t) (k 0)))) (h "135ndk169y5krxbhy8rs7xfy613xcsrfk1fg6b0glmlk0vv6c6g4")))

(define-public crate-ntex-grpc-derive-0.3.0 (c (n "ntex-grpc-derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.3") (f (quote ("full" "extra-traits" "fold"))) (d #t) (k 0)))) (h "19fn92p5grp7zh56j7ip8gk2y6zsiv3q8lz77jilkbgrw1kpbqf0")))

(define-public crate-ntex-grpc-derive-0.3.1 (c (n "ntex-grpc-derive") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.3") (f (quote ("full" "extra-traits" "fold"))) (d #t) (k 0)))) (h "0cvbvpll7q09xlrir6j25qzjiwwb13hifx17cvfp4g14nsvawv5g")))

(define-public crate-ntex-grpc-derive-0.4.0-beta.0 (c (n "ntex-grpc-derive") (v "0.4.0-beta.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.3") (f (quote ("full" "extra-traits" "fold"))) (d #t) (k 0)))) (h "0ybn5x8i6307a214hwyrlv07z5h99b8r8fyjz3i2lfj91j5pnlai")))

(define-public crate-ntex-grpc-derive-0.4.0-beta.1 (c (n "ntex-grpc-derive") (v "0.4.0-beta.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.3") (f (quote ("full" "extra-traits" "fold"))) (d #t) (k 0)))) (h "0k8fg829g7a3f24z2fcic31jcj9hwbc06ianbwm1i4613jk9z239")))

(define-public crate-ntex-grpc-derive-0.4.0 (c (n "ntex-grpc-derive") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.3") (f (quote ("full" "extra-traits" "fold"))) (d #t) (k 0)))) (h "1lzd1hqxc8hh83s3gzch8rm5ksxxg323b7565y5nsdjg2a6aa4jv")))

(define-public crate-ntex-grpc-derive-0.6.0-b.0 (c (n "ntex-grpc-derive") (v "0.6.0-b.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.3") (f (quote ("full" "extra-traits" "fold"))) (d #t) (k 0)))) (h "01g53ik10gsmzxc8mfj5fmgqa6bg9wbhswinsc1jsxs3l2q2i0pr")))

(define-public crate-ntex-grpc-derive-0.6.0 (c (n "ntex-grpc-derive") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.3") (f (quote ("full" "extra-traits" "fold"))) (d #t) (k 0)))) (h "1xl4dp68qz1ih6gm0iib3vmwjg9sngzkvyrv902c5r9p4b9l5ix4")))

