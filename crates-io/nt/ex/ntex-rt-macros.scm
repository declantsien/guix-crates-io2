(define-module (crates-io nt ex ntex-rt-macros) #:use-module (crates-io))

(define-public crate-ntex-rt-macros-0.1.0 (c (n "ntex-rt-macros") (v "0.1.0") (d (list (d (n "ntex") (r "^0.1.0") (d #t) (k 2)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1k2qm6a64vx4rx6bgrrl1n91xg59zfcnz90ra45hyz4rhyr135c0")))

(define-public crate-ntex-rt-macros-0.1.1 (c (n "ntex-rt-macros") (v "0.1.1") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1fhfjizj2b97y60qrw3sb1q7qkxr9a1qizamjd0hdm4v30j8fba4")))

