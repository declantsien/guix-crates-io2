(define-module (crates-io nt ex ntex-macros) #:use-module (crates-io))

(define-public crate-ntex-macros-0.1.0 (c (n "ntex-macros") (v "0.1.0") (d (list (d (n "futures") (r "^0.3.4") (d #t) (k 2)) (d (n "ntex") (r "^0.1.6") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "0gqldbchvjzjz9vwgxsinikfhm3cxj7mx8vcklagw084y5dhz6w4")))

(define-public crate-ntex-macros-0.1.1 (c (n "ntex-macros") (v "0.1.1") (d (list (d (n "futures") (r "^0.3.13") (d #t) (k 2)) (d (n "ntex") (r "^0.3.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "1ay70vmna5npc5rcr8bbx23r3bdizd4an4fnrajnigpd64xwzfd4")))

(define-public crate-ntex-macros-0.1.2 (c (n "ntex-macros") (v "0.1.2") (d (list (d (n "futures") (r "^0.3.13") (d #t) (k 2)) (d (n "ntex") (r "^0.3.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "1604lfcypkqd87p05jfp4sq20k3y663x9291aily7xymwipfiab9")))

(define-public crate-ntex-macros-0.1.3 (c (n "ntex-macros") (v "0.1.3") (d (list (d (n "futures") (r "^0.3.13") (d #t) (k 2)) (d (n "ntex") (r "^0.3.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "117d2w4fiq99lbv3k0ghgixdadql5g170l378q22nw8cl7r5k8sh")))

