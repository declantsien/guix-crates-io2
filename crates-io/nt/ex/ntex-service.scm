(define-module (crates-io nt ex ntex-service) #:use-module (crates-io))

(define-public crate-ntex-service-0.1.0 (c (n "ntex-service") (v "0.1.0") (d (list (d (n "futures-util") (r "^0.3.4") (d #t) (k 0)) (d (n "ntex-rt") (r "^0.1") (d #t) (k 2)) (d (n "pin-project") (r "^0.4.8") (d #t) (k 0)))) (h "069ymsb8hr7bfr1jq8lkvrbia1c8paqxg52zbaqcj24vpi98hm15")))

(define-public crate-ntex-service-0.1.1 (c (n "ntex-service") (v "0.1.1") (d (list (d (n "futures-util") (r "^0.3.4") (d #t) (k 0)) (d (n "ntex-rt") (r "^0.1") (d #t) (k 2)) (d (n "pin-project") (r "^0.4.8") (d #t) (k 0)))) (h "0k58bnrlyky20fn7vngh79pbd45d949ww5qc1dhyq7pp0s45526x")))

(define-public crate-ntex-service-0.1.2 (c (n "ntex-service") (v "0.1.2") (d (list (d (n "futures-util") (r "^0.3.4") (d #t) (k 0)) (d (n "ntex-rt") (r "^0.1") (d #t) (k 2)) (d (n "pin-project") (r "^0.4.8") (d #t) (k 0)))) (h "0bxy8wmq758flq7wisk3qp4v5gy2y9xwgq54rh2a94nki0sisr0d")))

(define-public crate-ntex-service-0.1.3 (c (n "ntex-service") (v "0.1.3") (d (list (d (n "futures-util") (r "^0.3.4") (d #t) (k 0)) (d (n "ntex-rt") (r "^0.1") (d #t) (k 2)) (d (n "pin-project") (r "^0.4.20") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.1.5") (d #t) (k 0)))) (h "0ib570fyldmg2w9d82gmixan6c0qalywcvm37s5q0nqydp3zvs4a")))

(define-public crate-ntex-service-0.1.4 (c (n "ntex-service") (v "0.1.4") (d (list (d (n "futures-util") (r "^0.3.5") (d #t) (k 0)) (d (n "ntex-rt") (r "^0.1") (d #t) (k 2)) (d (n "pin-project") (r "^0.4.20") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.1.5") (d #t) (k 0)))) (h "0jqvnhbjb4nc0x86c80v3ib67kd4sjn68kvzj8kbcxx2l349rb02")))

(define-public crate-ntex-service-0.1.5 (c (n "ntex-service") (v "0.1.5") (d (list (d (n "futures-util") (r "^0.3.9") (d #t) (k 0)) (d (n "ntex-rt") (r "^0.1") (d #t) (k 2)) (d (n "pin-project-lite") (r "^0.2.4") (d #t) (k 0)))) (h "1k9sqprziimhavbgd7042wrfx16m570vapd53m3y6qasvb22fna3")))

(define-public crate-ntex-service-0.1.6 (c (n "ntex-service") (v "0.1.6") (d (list (d (n "futures-util") (r "^0.3.13") (d #t) (k 0)) (d (n "ntex") (r "^0.3.1") (d #t) (k 2)) (d (n "pin-project-lite") (r "^0.2.4") (d #t) (k 0)))) (h "1mqldd00lr6zs32p9cr8v4pd0504idrk5wvyhq51fjg4f0ja7f00")))

(define-public crate-ntex-service-0.1.7 (c (n "ntex-service") (v "0.1.7") (d (list (d (n "ntex") (r "^0.3.13") (d #t) (k 2)) (d (n "pin-project-lite") (r "^0.2.4") (d #t) (k 0)))) (h "012r1282hpy7ld726zg3y2mn8krcrj2j576y2z7c0s3mrwj088mq")))

(define-public crate-ntex-service-0.1.8 (c (n "ntex-service") (v "0.1.8") (d (list (d (n "ntex") (r "^0.3.13") (d #t) (k 2)) (d (n "ntex-util") (r "^0.1.1") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2.6") (d #t) (k 0)))) (h "0ifpv50riqxn462cy3pkjhb1xbdvmh4lbv4i30yrkhz8fxswlxv6")))

(define-public crate-ntex-service-0.1.9 (c (n "ntex-service") (v "0.1.9") (d (list (d (n "ntex") (r "^0.3.13") (d #t) (k 2)) (d (n "ntex-util") (r "^0.1.1") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2.6") (d #t) (k 0)))) (h "1x75mqbk4rmqj7821xwp8ljikqln9hpq993m8s89jzwyxy6mp14a")))

(define-public crate-ntex-service-0.2.0-b.0 (c (n "ntex-service") (v "0.2.0-b.0") (d (list (d (n "ntex") (r "^0.3.13") (d #t) (k 2)) (d (n "ntex-util") (r "^0.1.1") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2.6") (d #t) (k 0)))) (h "1ahvpg0hi8lc0n1izakd0f4mky81h6mcr32vb6rc9dw81vk6cx83")))

(define-public crate-ntex-service-0.2.0 (c (n "ntex-service") (v "0.2.0") (d (list (d (n "ntex") (r "^0.3.13") (d #t) (k 2)) (d (n "ntex-util") (r "^0.1.1") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2.6") (d #t) (k 0)))) (h "0vsqy4ig7c3j2wk87j1z26fmzv2zvymszpmfpipb347jbjc0qk92")))

(define-public crate-ntex-service-0.2.1 (c (n "ntex-service") (v "0.2.1") (d (list (d (n "ntex") (r "^0.3.13") (d #t) (k 2)) (d (n "ntex-util") (r "^0.1.1") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2.6") (d #t) (k 0)))) (h "0bb3468jshc1ybpgqwww7jn9n4462z59yvvxgdc0942jly2512x1")))

(define-public crate-ntex-service-0.3.0-b.0 (c (n "ntex-service") (v "0.3.0-b.0") (d (list (d (n "ntex") (r "^0.5.0-b.0") (d #t) (k 2)) (d (n "ntex-util") (r "^0.1.4") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2.6") (d #t) (k 0)))) (h "0l9asprgp7p9n68yaszvdh2l06vf1nmn30rn7rplh7p78mq8dqb0")))

(define-public crate-ntex-service-0.3.0 (c (n "ntex-service") (v "0.3.0") (d (list (d (n "ntex") (r "^0.5.0-b.7") (d #t) (k 2)) (d (n "ntex-util") (r "^0.1.5") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2.6") (d #t) (k 0)))) (h "0ybhkx32vl7k38vk3j00cxnfza3g4b7vf4hqgsrf5gzzkqis4xsc")))

(define-public crate-ntex-service-0.3.1 (c (n "ntex-service") (v "0.3.1") (d (list (d (n "ntex") (r "^0.5.0") (d #t) (k 2)) (d (n "ntex-util") (r "^0.1.5") (d #t) (k 2)) (d (n "pin-project-lite") (r "^0.2.6") (d #t) (k 0)))) (h "0nsn32r4xlhj9wh4ab0mmz5jzyvnyhnvr5vgjgfm43fwxsmxbjbw")))

(define-public crate-ntex-service-0.3.2 (c (n "ntex-service") (v "0.3.2") (d (list (d (n "ntex") (r "^0.5") (f (quote ("tokio"))) (d #t) (k 2)) (d (n "ntex-util") (r "^0.1.13") (d #t) (k 2)) (d (n "pin-project-lite") (r "^0.2.6") (d #t) (k 0)))) (h "1wycf47zn2svcpa56cd3z428wz44vb12mdb6afvjl0fzyb6s6ixi")))

(define-public crate-ntex-service-0.3.3 (c (n "ntex-service") (v "0.3.3") (d (list (d (n "ntex") (r "^0.5") (f (quote ("tokio"))) (d #t) (k 2)) (d (n "ntex-util") (r "^0.1.5") (d #t) (k 2)) (d (n "pin-project-lite") (r "^0.2.6") (d #t) (k 0)))) (h "171v8854jkmnian6l7v6fxdanlrjqvn3s771n6h8vhfsrqqbcbrh")))

(define-public crate-ntex-service-1.0.0-beta.0 (c (n "ntex-service") (v "1.0.0-beta.0") (d (list (d (n "ntex") (r "^0.5") (f (quote ("tokio"))) (d #t) (k 2)) (d (n "ntex-util") (r "^0.1") (d #t) (k 2)) (d (n "pin-project-lite") (r "^0.2.6") (d #t) (k 0)))) (h "11999gc4dz6f4rjqwqwwnjq13gbpr9095ri88l9rqz01plywb9ma")))

(define-public crate-ntex-service-1.0.0 (c (n "ntex-service") (v "1.0.0") (d (list (d (n "ntex") (r "^0.6.0-beta.0") (f (quote ("tokio"))) (d #t) (k 2)) (d (n "ntex-util") (r "^0.2.0-beta.0") (d #t) (k 2)) (d (n "pin-project-lite") (r "^0.2.6") (d #t) (k 0)))) (h "0i5zw706l84kgxjcil4napy9r6gg775mwry61iyak21v8slv27rx")))

(define-public crate-ntex-service-1.0.1 (c (n "ntex-service") (v "1.0.1") (d (list (d (n "ntex") (r "^0.6.0") (f (quote ("tokio"))) (d #t) (k 2)) (d (n "ntex-util") (r "^0.2.0") (d #t) (k 2)) (d (n "pin-project-lite") (r "^0.2.6") (d #t) (k 0)))) (h "1zl8b3j2qnq47jw9hjxp2mkf9p36vqns20na0ykli354wwxbcaqz")))

(define-public crate-ntex-service-1.0.2 (c (n "ntex-service") (v "1.0.2") (d (list (d (n "ntex") (r "^0.6.0") (f (quote ("tokio"))) (d #t) (k 2)) (d (n "ntex-util") (r "^0.2.0") (d #t) (k 2)) (d (n "pin-project-lite") (r "^0.2.6") (d #t) (k 0)))) (h "1i2i5qczr08d30jrp6rg0kk7z17j5l60a99dy49hdhviwzyhgwna")))

(define-public crate-ntex-service-1.2.0-beta.0 (c (n "ntex-service") (v "1.2.0-beta.0") (d (list (d (n "pin-project-lite") (r "^0.2.6") (d #t) (k 0)) (d (n "slab") (r "^0.4") (d #t) (k 0)))) (h "094chc5qkp79qmywpjkl7gxl2d727ma156b3lacncfjgj7rr97lk")))

(define-public crate-ntex-service-1.2.0-beta.1 (c (n "ntex-service") (v "1.2.0-beta.1") (d (list (d (n "ntex") (r "^0.7.0-beta.0") (f (quote ("tokio"))) (d #t) (k 2)) (d (n "ntex-util") (r "^0.3.0-beta.0") (d #t) (k 2)) (d (n "pin-project-lite") (r "^0.2.6") (d #t) (k 0)) (d (n "slab") (r "^0.4") (d #t) (k 0)))) (h "095av2s33xj307yhbjwr49gnn3djg3jlsjp04169pcdj6ws2gfsl")))

(define-public crate-ntex-service-1.2.0-beta.2 (c (n "ntex-service") (v "1.2.0-beta.2") (d (list (d (n "ntex") (r "^0.7.0-beta.0") (f (quote ("tokio"))) (d #t) (k 2)) (d (n "ntex-util") (r "^0.3.0-beta.0") (d #t) (k 2)) (d (n "pin-project-lite") (r "^0.2.6") (d #t) (k 0)) (d (n "slab") (r "^0.4") (d #t) (k 0)))) (h "1vxiz4cwv6w4cng5cryjwmr45zs007dnmwbxq098vdnwckxkxx3y")))

(define-public crate-ntex-service-1.2.0-beta.3 (c (n "ntex-service") (v "1.2.0-beta.3") (d (list (d (n "ntex") (r "^0.7.0-beta.1") (f (quote ("tokio"))) (d #t) (k 2)) (d (n "ntex-util") (r "^0.3.0-beta.1") (d #t) (k 2)) (d (n "pin-project-lite") (r "^0.2.6") (d #t) (k 0)) (d (n "slab") (r "^0.4") (d #t) (k 0)))) (h "0z6m87bqww76k54bixgrlindrls2agr2rh6qivwz4y06snv3v9s0")))

(define-public crate-ntex-service-1.2.0 (c (n "ntex-service") (v "1.2.0") (d (list (d (n "ntex") (r "^0.7.0-beta.0") (f (quote ("tokio"))) (d #t) (k 2)) (d (n "ntex-util") (r "^0.3.0-beta.0") (d #t) (k 2)) (d (n "pin-project-lite") (r "^0.2.6") (d #t) (k 0)) (d (n "slab") (r "^0.4") (d #t) (k 0)))) (h "0jml9rd6w727r55vzghiiviiv7iy3ghbafrjzgy67n6ag2scg8p4")))

(define-public crate-ntex-service-1.2.1 (c (n "ntex-service") (v "1.2.1") (d (list (d (n "ntex") (r "^0.7.0") (f (quote ("tokio"))) (d #t) (k 2)) (d (n "ntex-util") (r "^0.3.0") (d #t) (k 2)) (d (n "pin-project-lite") (r "^0.2.6") (d #t) (k 0)) (d (n "slab") (r "^0.4") (d #t) (k 0)))) (h "052lcahmr1cj7z2p1jis3ipx6ramn7psqc6z68biss5nrwx6nwfg")))

(define-public crate-ntex-service-1.2.2 (c (n "ntex-service") (v "1.2.2") (d (list (d (n "ntex") (r "^0.7.0") (f (quote ("tokio"))) (d #t) (k 2)) (d (n "ntex-util") (r "^0.3.0") (d #t) (k 2)) (d (n "pin-project-lite") (r "^0.2.6") (d #t) (k 0)) (d (n "slab") (r "^0.4") (d #t) (k 0)))) (h "0jbpwha84p1g752jwr171yxfd4l59lh8a2gnr515hsz3rrkfjr0f")))

(define-public crate-ntex-service-1.2.3 (c (n "ntex-service") (v "1.2.3") (d (list (d (n "ntex") (r "^0.7.0") (f (quote ("tokio"))) (d #t) (k 2)) (d (n "ntex-util") (r "^0.3.0") (d #t) (k 2)) (d (n "pin-project-lite") (r "^0.2.6") (d #t) (k 0)) (d (n "slab") (r "^0.4") (d #t) (k 0)))) (h "0hvw7jh98yx5ay0hrxv5qwrw1z357408pjsxpr3kfvbmv8rhlwyg")))

(define-public crate-ntex-service-1.2.4 (c (n "ntex-service") (v "1.2.4") (d (list (d (n "ntex") (r "^0.7.0") (f (quote ("tokio"))) (d #t) (k 2)) (d (n "ntex-util") (r "^0.3.0") (d #t) (k 2)) (d (n "pin-project-lite") (r "^0.2.6") (d #t) (k 0)) (d (n "slab") (r "^0.4") (d #t) (k 0)))) (h "0q6qrcmkrh55189hcly5gpvskpir1vs4mj9abvbs8w0slp150r86")))

(define-public crate-ntex-service-1.2.5 (c (n "ntex-service") (v "1.2.5") (d (list (d (n "ntex") (r "^0.7.0") (f (quote ("tokio"))) (d #t) (k 2)) (d (n "ntex-util") (r "^0.3.0") (d #t) (k 2)) (d (n "pin-project-lite") (r "^0.2.6") (d #t) (k 0)) (d (n "slab") (r "^0.4") (d #t) (k 0)))) (h "064hxx6wqhgvrpk5c0bkiidbv4ydh25sd62bq7d8gc32qc7qjq50")))

(define-public crate-ntex-service-1.2.6 (c (n "ntex-service") (v "1.2.6") (d (list (d (n "ntex") (r "^0.7.0") (f (quote ("tokio"))) (d #t) (k 2)) (d (n "ntex-util") (r "^0.3.0") (d #t) (k 2)) (d (n "pin-project-lite") (r "^0.2.6") (d #t) (k 0)) (d (n "slab") (r "^0.4") (d #t) (k 0)))) (h "02x9in6y3vl71x28is049v85bbaj2x0h274l00qa5alwnd63f77z")))

(define-public crate-ntex-service-1.2.7 (c (n "ntex-service") (v "1.2.7") (d (list (d (n "ntex") (r "^0.7.0") (f (quote ("tokio"))) (d #t) (k 2)) (d (n "ntex-util") (r "^0.3.0") (d #t) (k 2)) (d (n "pin-project-lite") (r "^0.2.6") (d #t) (k 0)) (d (n "slab") (r "^0.4") (d #t) (k 0)))) (h "11mg2zqzwq05zvnpg3rv7cn1l69fa1jwarvl40b89iwr66jmvv80")))

(define-public crate-ntex-service-2.0.0-b.0 (c (n "ntex-service") (v "2.0.0-b.0") (d (list (d (n "pin-project-lite") (r "^0.2.6") (d #t) (k 0)) (d (n "slab") (r "^0.4") (d #t) (k 0)))) (h "0slpmjmh7gc40i7qgcsn2rj1m486z55khki40p6g5b32gagvr9z3")))

(define-public crate-ntex-service-2.0.0 (c (n "ntex-service") (v "2.0.0") (d (list (d (n "ntex") (r "^1.0.0-b.0") (f (quote ("tokio"))) (d #t) (k 2)) (d (n "ntex-util") (r "^1.0.0-b.1") (d #t) (k 2)) (d (n "slab") (r "^0.4") (d #t) (k 0)))) (h "014pjxvqjhmz40vp3pjchz62wgw9da46bds941i79xgfm6j5f7gv")))

(define-public crate-ntex-service-2.0.1 (c (n "ntex-service") (v "2.0.1") (d (list (d (n "ntex") (r "^1.0.0") (f (quote ("tokio"))) (d #t) (k 2)) (d (n "ntex-util") (r "^1.0.0") (d #t) (k 2)) (d (n "slab") (r "^0.4") (d #t) (k 0)))) (h "0m5j63lg9z12dadnniajj5m0r5pxxmhdpyrqfmggzs1wvfgrfk2b")))

(define-public crate-ntex-service-3.0.0 (c (n "ntex-service") (v "3.0.0") (d (list (d (n "ntex") (r "^1") (f (quote ("tokio"))) (d #t) (k 2)) (d (n "ntex-util") (r "^1") (d #t) (k 2)) (d (n "slab") (r "^0.4") (d #t) (k 0)))) (h "1m2hvr4jlkrrl7rkxiyl3klph05lm9brkhllxx36cd2dzb3ydsxw")))

