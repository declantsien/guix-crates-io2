(define-module (crates-io nt ex ntex-helmet) #:use-module (crates-io))

(define-public crate-ntex-helmet-0.1.0 (c (n "ntex-helmet") (v "0.1.0") (d (list (d (n "ntex") (r "^0.7") (f (quote ("tokio"))) (d #t) (k 0)) (d (n "ntex") (r "^0.7") (f (quote ("tokio"))) (d #t) (k 2)))) (h "1b3hdiiniwgn383dqp5d91d59nhi4g5jb5kywy1dn702rgh9y4y9")))

(define-public crate-ntex-helmet-0.1.1 (c (n "ntex-helmet") (v "0.1.1") (d (list (d (n "ntex") (r "^0.7") (f (quote ("tokio"))) (d #t) (k 0)) (d (n "ntex") (r "^0.7") (f (quote ("tokio"))) (d #t) (k 2)))) (h "0pzv5xdpnn1qz5m9z0m3m9jaf5qkgqcjw23i6lzjyv7p25bhc876")))

(define-public crate-ntex-helmet-0.1.2 (c (n "ntex-helmet") (v "0.1.2") (d (list (d (n "ntex") (r "^0.7") (f (quote ("tokio"))) (d #t) (k 0)) (d (n "ntex") (r "^0.7") (f (quote ("tokio"))) (d #t) (k 2)))) (h "18p96i3k0jsmykgbapzwknb06l0sba6g9lp291cpkbm8qxrvbdjq")))

(define-public crate-ntex-helmet-0.1.3 (c (n "ntex-helmet") (v "0.1.3") (d (list (d (n "helmet-core") (r "^0.1.0") (d #t) (k 0)) (d (n "ntex") (r "^0.7") (f (quote ("tokio"))) (d #t) (k 0)) (d (n "ntex") (r "^0.7") (f (quote ("tokio"))) (d #t) (k 2)))) (h "10nihgqjs3zdvwdjiva3dkvhd6x4ilvbr7axpwjzfsyncdrlylb1")))

