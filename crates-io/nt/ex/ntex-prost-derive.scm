(define-module (crates-io nt ex ntex-prost-derive) #:use-module (crates-io))

(define-public crate-ntex-prost-derive-0.10.1 (c (n "ntex-prost-derive") (v "0.10.1") (d (list (d (n "anyhow") (r "^1.0.1") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.3") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0dsdnb0sp5mplcqy0xvp44j5na10jp5hv38khq0cid0gdc12gdgr")))

(define-public crate-ntex-prost-derive-0.10.2 (c (n "ntex-prost-derive") (v "0.10.2") (d (list (d (n "anyhow") (r "^1.0.1") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.3") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1wmjpf14ps8bgxndgsp9yha6kba96s25pmqvh4ihv1nbd4ra16l4")))

(define-public crate-ntex-prost-derive-0.10.3 (c (n "ntex-prost-derive") (v "0.10.3") (d (list (d (n "anyhow") (r "^1.0.1") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.3") (f (quote ("full" "extra-traits" "fold"))) (d #t) (k 0)))) (h "11i2s32b33qxfr6rwj3i6yacc3waycjgrsp9ybwc1hcx0qdj8ncf")))

