(define-module (crates-io nt ex ntex-cors) #:use-module (crates-io))

(define-public crate-ntex-cors-0.1.0 (c (n "ntex-cors") (v "0.1.0") (d (list (d (n "derive_more") (r "^0.99.5") (d #t) (k 0)) (d (n "futures") (r "^0.3.5") (d #t) (k 0)) (d (n "ntex") (r "^0.1.24") (d #t) (k 0)))) (h "0fdpkrrn5xy8k39v2xbk8vzdfq72cdwawyafl746viizcckdc00a")))

(define-public crate-ntex-cors-0.3.0 (c (n "ntex-cors") (v "0.3.0") (d (list (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "ntex") (r "^0.6.5") (d #t) (k 0)) (d (n "ntex") (r "^0.6.5") (f (quote ("tokio"))) (d #t) (k 2)))) (h "036hyn0icyqazdjjdm8c3byvs1gwyaisvhz2ybsvf5hbylgw5m0y")))

(define-public crate-ntex-cors-0.4.0 (c (n "ntex-cors") (v "0.4.0") (d (list (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "ntex") (r "^0.7.0") (d #t) (k 0)) (d (n "ntex") (r "^0.7.0") (f (quote ("tokio"))) (d #t) (k 2)))) (h "06n21cj0nzrw663lxyh6a4l71ymz8n5abli0mynvi2qcqih2hr0l")))

(define-public crate-ntex-cors-0.5.0-b.0 (c (n "ntex-cors") (v "0.5.0-b.0") (d (list (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "ntex") (r "^1.0.0-b.1") (d #t) (k 0)) (d (n "ntex") (r "^1.0.0-b.1") (f (quote ("tokio"))) (d #t) (k 2)))) (h "1g3bv1k69m6r1kmd4zgh9k894yhyj7jpi090ypzmfz68m1wv5alv")))

(define-public crate-ntex-cors-0.5.0 (c (n "ntex-cors") (v "0.5.0") (d (list (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "ntex") (r "^1.0") (d #t) (k 0)) (d (n "ntex") (r "^1.0") (f (quote ("tokio"))) (d #t) (k 2)))) (h "1ylcm7iljlp8a8aijmkfinbjmgyphvfk1vim3akyc981ixj4jh2c")))

(define-public crate-ntex-cors-1.0.0 (c (n "ntex-cors") (v "1.0.0") (d (list (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "ntex") (r "^1.1") (d #t) (k 0)) (d (n "ntex") (r "^1.1") (f (quote ("tokio"))) (d #t) (k 2)))) (h "0mpilm9xgmfx6zrlc0y1098v7l5mxfrq5aq7zs36p5n4yrr394ik")))

(define-public crate-ntex-cors-2.0.0 (c (n "ntex-cors") (v "2.0.0") (d (list (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "ntex") (r "^2") (d #t) (k 0)) (d (n "ntex") (r "^2") (f (quote ("tokio"))) (d #t) (k 2)))) (h "1gqi4d2d671k03g0d3r9ksd0cv2jbjrbgzdspnybknmlh433ah2j")))

