(define-module (crates-io nt up ntuple) #:use-module (crates-io))

(define-public crate-ntuple-0.7.0 (c (n "ntuple") (v "0.7.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 1)) (d (n "bindgen") (r "^0.63") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "hepmc2") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "16n8hxhrgr1x3n9a8pl8xjg3k6dma977ngbbc35vli3d4swhm1pc")))

(define-public crate-ntuple-0.8.0 (c (n "ntuple") (v "0.8.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 1)) (d (n "bindgen") (r "^0.66") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "hepmc2") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0p8aryjh36rhsakjgniw3rvqhayz1fc1iyasrm1bljf7jz3fvkpc")))

