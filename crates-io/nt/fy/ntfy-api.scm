(define-module (crates-io nt fy ntfy-api) #:use-module (crates-io))

(define-public crate-ntfy-api-0.1.0 (c (n "ntfy-api") (v "0.1.0") (d (list (d (n "ntfy-types") (r "^0.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)))) (h "1x86yallxmh3lzgdc8himfnyazpkgi7sqp9xz450dn9p304yp6r6")))

(define-public crate-ntfy-api-0.1.1 (c (n "ntfy-api") (v "0.1.1") (d (list (d (n "ntfy-types") (r "^0.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "macros"))) (d #t) (k 2)))) (h "1x0h4r3xzs174c54sjn6hhf8gf06a8fxxpniaf542i4nvib4m2v8")))

