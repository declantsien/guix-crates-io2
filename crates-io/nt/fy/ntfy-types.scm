(define-module (crates-io nt fy ntfy-types) #:use-module (crates-io))

(define-public crate-ntfy-types-0.1.0 (c (n "ntfy-types") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "03rlp2apma1h8yx44rdxg8m2isingpb70ahmj8w01v0r2s75nxlk")))

(define-public crate-ntfy-types-0.2.0 (c (n "ntfy-types") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "11hx9x6n72ak533n7fbfl9l57bdpqbzv93c0jm0dq1fjmaka33wi")))

(define-public crate-ntfy-types-0.3.0 (c (n "ntfy-types") (v "0.3.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0qnc2vyx12dgh36aj4aldjjqax4qgacw1j92yh9bndj9b4x5kjaf")))

(define-public crate-ntfy-types-0.3.1 (c (n "ntfy-types") (v "0.3.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1xvk89dgaq9xi4r7dz5swaxqqn15bzlqx4qr69qgrv4fsg47hpax")))

(define-public crate-ntfy-types-0.3.2 (c (n "ntfy-types") (v "0.3.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1jffcs820ahpcskwsqxif1mw0bsy7qmsr9f0hwhqgqhiaa37ddws")))

(define-public crate-ntfy-types-0.3.3 (c (n "ntfy-types") (v "0.3.3") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0rz2rag5gcadi08c5d0zq2qhjvrn1kzns5vlq3dza898z2xc606b")))

