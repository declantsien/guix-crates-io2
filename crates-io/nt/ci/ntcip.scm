(define-module (crates-io nt ci ntcip) #:use-module (crates-io))

(define-public crate-ntcip-0.1.0 (c (n "ntcip") (v "0.1.0") (d (list (d (n "base64") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pix") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1ij2slwdw5h5ggm3aqj63hcbml3w8p9dwfawk28ji2fwlvg50z8v")))

(define-public crate-ntcip-0.1.1 (c (n "ntcip") (v "0.1.1") (d (list (d (n "base64") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pix") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1df5k9f5q34gjgi3lmsiiya3xd25wih21dbnldjsji8449qg6yjj")))

(define-public crate-ntcip-0.1.2 (c (n "ntcip") (v "0.1.2") (d (list (d (n "base64") (r "^0.11") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pix") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1rfj2gz8v7sj8akibjl5y1ig660axdk6y8p85v4gknzspwbf9zgn")))

(define-public crate-ntcip-0.2.0 (c (n "ntcip") (v "0.2.0") (d (list (d (n "base64") (r "^0.11") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pix") (r "^0.13") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1y5hlvs0hzbk3kv3a4wqz7a36b27982cl801nxc11xq875vp84ry")))

(define-public crate-ntcip-0.3.0 (c (n "ntcip") (v "0.3.0") (d (list (d (n "base64") (r "^0.12") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pix") (r "^0.13") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "17k6bpq69lhd2b7rlik46mp0dvlf7b5ik0cpbaw5h8gma4lxjnyl")))

(define-public crate-ntcip-0.3.1 (c (n "ntcip") (v "0.3.1") (d (list (d (n "base64") (r "^0.12") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pix") (r "^0.13") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "00cn4zrry3vxjy318a2cj9fwhdsm2hsg3cibm4d4hjvp2vglkhps")))

(define-public crate-ntcip-0.4.0 (c (n "ntcip") (v "0.4.0") (d (list (d (n "base64") (r "^0.12") (d #t) (k 0)) (d (n "gift") (r "^0.9.1") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pix") (r "^0.13") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "0nn2ws8pp5a33906c77f98cj1rq1bnprw2nd2z8cy5sbixal1pbk")))

(define-public crate-ntcip-0.4.1 (c (n "ntcip") (v "0.4.1") (d (list (d (n "base64") (r "^0.12") (d #t) (k 0)) (d (n "gift") (r "^0.9.1") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pix") (r "^0.13") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "06xrc5wj3i4lpxxw1dhxvwyan6q968vc2wjaf1k05c34kigvmf0a")))

(define-public crate-ntcip-0.5.0 (c (n "ntcip") (v "0.5.0") (d (list (d (n "base64") (r "^0.12") (d #t) (k 0)) (d (n "gift") (r "^0.10.0") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pix") (r "^0.13") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1vyyyc4rgvyxxg45nj319bb7103rl91rdc74kh644rckz7j69ap9")))

(define-public crate-ntcip-0.5.1 (c (n "ntcip") (v "0.5.1") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "gift") (r "^0.10.0") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pix") (r "^0.13") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "0i18zwvczp65gf2m1kn9hb9s2iinm417qcwn2cpsh9rardxbpf6f")))

(define-public crate-ntcip-0.5.2 (c (n "ntcip") (v "0.5.2") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "gift") (r "^0.10.0") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pix") (r "^0.13") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "0kcaavnqxnr7yf8rql69shl9n4xc0ilpvnkx2nck1mfl975bg66w")))

(define-public crate-ntcip-0.5.3 (c (n "ntcip") (v "0.5.3") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "gift") (r "^0.10.0") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pix") (r "^0.13") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "0ap5qbjl4krfh4705xqrwkvcjjqc9li6jr2kqfvnr85f9z54fv59")))

(define-public crate-ntcip-0.6.0 (c (n "ntcip") (v "0.6.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pix") (r "^0.13") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "161zhg785qsgzhcnf44aglf456bfkg2s119jkmnsc5r0gfm9v2vf")))

(define-public crate-ntcip-0.6.1 (c (n "ntcip") (v "0.6.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pix") (r "^0.13") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "04p7b8lkfda6nnv9gk3vpfix3a5pqkpwl62n7jhh99gvy3r28nr1")))

(define-public crate-ntcip-0.7.0 (c (n "ntcip") (v "0.7.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pix") (r "^0.13") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1gy5jh63f8slmdnkvhb7y8sg96k5amllqwy1k4v4cgvx4n9zk246")))

(define-public crate-ntcip-0.7.1 (c (n "ntcip") (v "0.7.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pix") (r "^0.13") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0ixr46i2q2rkxm2n4d7h1q2lr02xw377mvn2m86f8d8issd5mqbp")))

(define-public crate-ntcip-0.7.2 (c (n "ntcip") (v "0.7.2") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pix") (r "^0.13") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1m532hhbshyig9xnyig2zj2iml23gki1342h58pv5rkfl7rwwcx2")))

(define-public crate-ntcip-0.7.3 (c (n "ntcip") (v "0.7.3") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pix") (r "^0.13") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1kryas67sw6r8s79s50q2r0m8f7sd8isr8cbdmszam7jc8y8h1xi")))

(define-public crate-ntcip-0.7.4 (c (n "ntcip") (v "0.7.4") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pix") (r "^0.13") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "12kspayq6disa3xga07nv211qb3fdmikahwpsac5qa0lmnm6rv48")))

(define-public crate-ntcip-0.8.0 (c (n "ntcip") (v "0.8.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pix") (r "^0.13") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "06m6c91j2r0f8l3qc66l6dbnwsacxim2m0d3si48fw9dl3amhqcj")))

(define-public crate-ntcip-0.8.1 (c (n "ntcip") (v "0.8.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pix") (r "^0.13") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1181b3jfpfnxd9by7p3rjkcsg03d67hk6nvsxvdlmv3sx1v55s6m")))

(define-public crate-ntcip-0.8.2 (c (n "ntcip") (v "0.8.2") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pix") (r "^0.13") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0hv6bn13dzwr1v0fisdlq816j096l0j3kjdkjinv9iqgdrqw1ayc")))

(define-public crate-ntcip-0.9.0 (c (n "ntcip") (v "0.9.0") (d (list (d (n "enumflags2") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pix") (r "^0.13") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1z4my835hgs59x45dvvz4rs6r8p6ys0wn7pa4b9q2wxbhwcni07q")))

(define-public crate-ntcip-0.10.0 (c (n "ntcip") (v "0.10.0") (d (list (d (n "crc") (r "^3") (d #t) (k 0)) (d (n "enumflags2") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pix") (r "^0.13") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1w00kk106535dhcy0ishdn757r5k5rx19nxzwj8jnwbfidvsx5rx")))

(define-public crate-ntcip-0.10.1 (c (n "ntcip") (v "0.10.1") (d (list (d (n "crc") (r "^3") (d #t) (k 0)) (d (n "enumflags2") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pix") (r "^0.13") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1mvvfwajarbp4byqfibfkcfn1cqvqv4c6486fgqc5qcw2rfffbsb")))

(define-public crate-ntcip-0.10.2 (c (n "ntcip") (v "0.10.2") (d (list (d (n "crc") (r "^3") (d #t) (k 0)) (d (n "enumflags2") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pix") (r "^0.13") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "17gssgsydzprddgb958jiwb3dd3jd1s8x8p07zh3fkjpzh4ci076")))

(define-public crate-ntcip-0.11.0 (c (n "ntcip") (v "0.11.0") (d (list (d (n "crc") (r "^3") (d #t) (k 0)) (d (n "enumflags2") (r "^0.7") (d #t) (k 0)) (d (n "fstr") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pix") (r "^0.13") (d #t) (k 0)) (d (n "tfon") (r "^0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0756y3nn2j5xkc4g072qpg4bc6plw7al7ijmpl70mwn0l0c477g7")))

(define-public crate-ntcip-0.11.1 (c (n "ntcip") (v "0.11.1") (d (list (d (n "crc") (r "^3") (d #t) (k 0)) (d (n "enumflags2") (r "^0.7") (d #t) (k 0)) (d (n "fstr") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pix") (r "^0.13") (d #t) (k 0)) (d (n "tfon") (r "^0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1p67178dnvavb1b3y6j0bavsryms4i05hvcza31npyqhq0pll92q")))

(define-public crate-ntcip-0.12.0 (c (n "ntcip") (v "0.12.0") (d (list (d (n "crc") (r "^3") (d #t) (k 0)) (d (n "enumflags2") (r "^0.7") (d #t) (k 0)) (d (n "fstr") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pix") (r "^0.13") (d #t) (k 0)) (d (n "tfon") (r "^0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0ah9cs6c8vw23wgq15d56s7q57jfymnz3k5q5yjiya07pv69nxip")))

(define-public crate-ntcip-0.12.1 (c (n "ntcip") (v "0.12.1") (d (list (d (n "crc") (r "^3") (d #t) (k 0)) (d (n "enumflags2") (r "^0.7") (d #t) (k 0)) (d (n "fstr") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pix") (r "^0.13") (d #t) (k 0)) (d (n "tfon") (r "^0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1qy03fmjw71fg4bx445q8j21vnc0caj7mc0a4b6qgvd8pvjgfzpv")))

(define-public crate-ntcip-0.12.2 (c (n "ntcip") (v "0.12.2") (d (list (d (n "crc") (r "^3") (d #t) (k 0)) (d (n "enumflags2") (r "^0.7") (d #t) (k 0)) (d (n "fstr") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pix") (r "^0.13") (d #t) (k 0)) (d (n "tfon") (r "^0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0amm60czbhv8flm8ryrx4nbg0hqfr3p05vfbmhd356q3aabl8wm2")))

(define-public crate-ntcip-0.12.3 (c (n "ntcip") (v "0.12.3") (d (list (d (n "crc") (r "^3") (d #t) (k 0)) (d (n "enumflags2") (r "^0.7") (d #t) (k 0)) (d (n "fstr") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pix") (r "^0.13") (d #t) (k 0)) (d (n "tfon") (r "^0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "17aqikrgg75favpdbawm11vks710plz1jw8p9fb2xq6mqaqfdgfq")))

