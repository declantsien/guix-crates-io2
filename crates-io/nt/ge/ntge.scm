(define-module (crates-io nt ge ntge) #:use-module (crates-io))

(define-public crate-ntge-0.1.0 (c (n "ntge") (v "0.1.0") (d (list (d (n "clap") (r "~2.33.0") (d #t) (k 0)) (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "ntge-core") (r "^0.1.0") (d #t) (k 0)))) (h "1mwpx86rc8n8fdjwp08dvy8ssihk37mnsjyr18z5p42s66lxv361")))

