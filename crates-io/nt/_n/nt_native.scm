(define-module (crates-io nt _n nt_native) #:use-module (crates-io))

(define-public crate-nt_native-0.1.0 (c (n "nt_native") (v "0.1.0") (h "1wwg1draayp8yl9drd3c232pk060vvdq47ab3mx9524ffsp8q83y") (y #t)))

(define-public crate-nt_native-0.1.1 (c (n "nt_native") (v "0.1.1") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "cfg-if") (r "^0.1.10") (d #t) (k 0)) (d (n "ntapi") (r "^0.3.3") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (d #t) (k 0)) (d (n "wstr") (r "^0.2") (d #t) (k 0)))) (h "1fxfglkxnmxqx82bxl3a8ccm6za6z9jm77cv5brb050biml5sd5s") (f (quote (("user" "ntapi/user" "winapi/libloaderapi" "winapi/winbase") ("std" "user") ("kernel" "ntapi/kernel") ("default" "std")))) (y #t)))

(define-public crate-nt_native-0.1.2 (c (n "nt_native") (v "0.1.2") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "cfg-if") (r "^0.1.10") (d #t) (k 0)) (d (n "ntapi") (r "^0.3.3") (d #t) (k 0)) (d (n "rdisk_shared") (r "^0.1") (k 0)) (d (n "winapi") (r "^0.3") (d #t) (k 0)) (d (n "wstr") (r "^0.2") (d #t) (k 0)))) (h "0106h406xfglsf62sh1mdrll6xwm73bwajqlmrdi9b646z6gm5sl") (f (quote (("user" "ntapi/user" "winapi/libloaderapi" "winapi/winbase") ("std" "user" "rdisk_shared/std") ("kernel" "ntapi/kernel") ("default" "std"))))))

