(define-module (crates-io nt ha nthash) #:use-module (crates-io))

(define-public crate-nthash-0.2.0 (c (n "nthash") (v "0.2.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.5") (d #t) (k 2)))) (h "0zsmmr7qdy03pfvlgsjvjh9mahzwjrs2nvd7d4anrbfd7bsc61n1")))

(define-public crate-nthash-0.2.1 (c (n "nthash") (v "0.2.1") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.5") (d #t) (k 2)))) (h "1jzx9i1g5nandhh7wdcq7cygb3phgr3wzp0z8l3l8717cmp1kanf")))

(define-public crate-nthash-0.2.2 (c (n "nthash") (v "0.2.2") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.5") (d #t) (k 2)))) (h "0ib3f6mzfaf442p50zamv8rsays0aj78bv41i1rlzg67r3an2raw")))

(define-public crate-nthash-0.3.0 (c (n "nthash") (v "0.3.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "quickcheck") (r "^0.7.1") (d #t) (k 2)) (d (n "rand") (r "^0.5") (d #t) (k 2)))) (h "0y4p17f84l0y7d06jrwgpfj5hs5gqqniy5ilnvgznglif2140a2v")))

(define-public crate-nthash-0.4.0 (c (n "nthash") (v "0.4.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "error-chain") (r "^0.12.0") (d #t) (k 0)) (d (n "quickcheck") (r "^0.7.1") (d #t) (k 2)) (d (n "rand") (r "^0.5") (d #t) (k 2)))) (h "0x6vick243zl8s3ncv1159gngi8w2444c2ac0ymkj152parxb9d1")))

(define-public crate-nthash-0.4.1 (c (n "nthash") (v "0.4.1") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "error-chain") (r "^0.12.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1.0") (d #t) (k 0)) (d (n "quickcheck") (r "^0.7.1") (d #t) (k 2)) (d (n "rand") (r "^0.5") (d #t) (k 2)))) (h "15fyrkk9wppp44jxwwb6b9rmllkb1kirxpyc4n91p7ngzfyabhss")))

(define-public crate-nthash-0.4.3 (c (n "nthash") (v "0.4.3") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "error-chain") (r "^0.12.0") (d #t) (k 0)) (d (n "quickcheck") (r "^0.7.1") (d #t) (k 2)) (d (n "rand") (r "^0.5") (d #t) (k 2)))) (h "0qxnrflx6wbqp8bzddl5n4sifq6h36qvind4yd97sa9kfrf7830m")))

(define-public crate-nthash-0.5.0 (c (n "nthash") (v "0.5.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "quickcheck") (r "^0.7.1") (d #t) (k 2)) (d (n "rand") (r "^0.5") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0dvipw8gl3vqw5gbvj8w5qrxzq0iqskqq2pd1yzh5525v7768mnz")))

(define-public crate-nthash-0.5.1 (c (n "nthash") (v "0.5.1") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "quickcheck") (r "^0.7.1") (d #t) (k 2)) (d (n "rand") (r "^0.5") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1l62hvjnrwlqm9834zck7a8l77vv222gdkbg2siinflqcngdabkm")))

