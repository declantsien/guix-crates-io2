(define-module (crates-io nt se ntserum-swap) #:use-module (crates-io))

(define-public crate-ntserum-swap-0.4.1 (c (n "ntserum-swap") (v "0.4.1") (d (list (d (n "NT-anchor-lang") (r "^0.19.0") (d #t) (k 0)) (d (n "NT-anchor-spl") (r "^0.19.0") (f (quote ("dex"))) (d #t) (k 0)) (d (n "solana-program") (r "^1.7.11") (d #t) (k 0)))) (h "0qzx98jkmy1y2zrd12d6sr6lvpfqx9qhbyf43s2cvjf70r11dw7i") (f (quote (("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-ntserum-swap-0.4.2 (c (n "ntserum-swap") (v "0.4.2") (d (list (d (n "NT-anchor-lang") (r "^0.19.0") (d #t) (k 0)) (d (n "NT-anchor-spl") (r "^0.19.0") (f (quote ("dex"))) (d #t) (k 0)) (d (n "solana-program") (r "^1.7.11") (d #t) (k 0)))) (h "1nyc8bbzcf5idvsgvd7qa2ikhqmw1s96vd0fw32mgdhga8f47h9k") (f (quote (("no-idl") ("no-entrypoint") ("devnet") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-ntserum-swap-0.4.3 (c (n "ntserum-swap") (v "0.4.3") (d (list (d (n "NT-anchor-lang") (r "^0.19.0") (d #t) (k 0)) (d (n "NT-anchor-spl") (r "^0.19.0") (f (quote ("dex"))) (d #t) (k 0)) (d (n "solana-program") (r "^1.7.11") (d #t) (k 0)))) (h "19yajyk22jq8dkzbkn4kxf0xvdvjqkwcnh30mzxbqx61prvz9q5z") (f (quote (("no-idl") ("no-entrypoint") ("devnet") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-ntserum-swap-0.4.4 (c (n "ntserum-swap") (v "0.4.4") (d (list (d (n "NT-anchor-lang") (r "^0.19.0") (d #t) (k 0)) (d (n "NT-anchor-spl") (r "^0.19.0") (f (quote ("dex" "devnet"))) (d #t) (k 0)) (d (n "solana-program") (r "^1.7.11") (d #t) (k 0)))) (h "19x9d1pfxnd28vwiczb8gqxm9n57v09fvvk8kswsrc08l16gb5qf") (f (quote (("no-idl") ("no-entrypoint") ("devnet") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-ntserum-swap-0.4.5 (c (n "ntserum-swap") (v "0.4.5") (d (list (d (n "NT-anchor-lang") (r "^0.19.0") (d #t) (k 0)) (d (n "NT-anchor-spl") (r "^0.19.0") (f (quote ("dex" "devnet"))) (d #t) (k 0)) (d (n "solana-program") (r "^1.7.11") (d #t) (k 0)))) (h "0fcyz5vqba92brhcnxgyczg288a2a0zmprr09jbxsx4zfdbp5csz") (f (quote (("no-idl") ("no-entrypoint") ("devnet") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-ntserum-swap-0.4.6 (c (n "ntserum-swap") (v "0.4.6") (d (list (d (n "NT-anchor-lang") (r "^0.19.0") (d #t) (k 0)) (d (n "NT-anchor-spl") (r "^0.19.0") (f (quote ("dex" "devnet"))) (d #t) (k 0)) (d (n "solana-program") (r "^1.7.11") (d #t) (k 0)))) (h "02lc5j978wczf1diwfxrl82g8sa3s6aq5kygbg9yvm4fk2k12mj1") (f (quote (("no-idl") ("no-entrypoint") ("devnet") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-ntserum-swap-0.4.7 (c (n "ntserum-swap") (v "0.4.7") (d (list (d (n "NT-anchor-lang") (r "^0.19.0") (d #t) (k 0)) (d (n "NT-anchor-spl") (r "^0.19.0") (f (quote ("dex"))) (d #t) (k 0)) (d (n "solana-program") (r "^1.7.11") (d #t) (k 0)))) (h "11cbvj2slw9b7diw48c67g6aylxcjyx9zxan0whww8xm829ghc9i") (f (quote (("no-idl") ("no-entrypoint") ("devnet") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-ntserum-swap-0.4.8 (c (n "ntserum-swap") (v "0.4.8") (d (list (d (n "NT-anchor-lang") (r "^0.19.0") (d #t) (k 0)) (d (n "NT-anchor-spl") (r "^0.19.2") (f (quote ("dex"))) (d #t) (k 0)) (d (n "solana-program") (r "^1.7.11") (d #t) (k 0)))) (h "15fbzk6gmmd4x6wqk60qsl2zmcnh2wdsyjyx84xd7s15gx4yksaf") (f (quote (("no-idl") ("no-entrypoint") ("devnet") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-ntserum-swap-0.4.9 (c (n "ntserum-swap") (v "0.4.9") (d (list (d (n "NT-anchor-lang") (r "^0.19.0") (d #t) (k 0)) (d (n "NT-anchor-spl") (r "^0.19.0") (f (quote ("dex" "devnet"))) (d #t) (k 0)) (d (n "solana-program") (r "^1.7.11") (d #t) (k 0)))) (h "0fpli2r90qyfg5rmzqi5c49va8ssvgdmzn433mx91arqx52sbyh2") (f (quote (("no-idl") ("no-entrypoint") ("devnet") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-ntserum-swap-0.5.0 (c (n "ntserum-swap") (v "0.5.0") (d (list (d (n "NT-anchor-lang") (r "^0.19.0") (d #t) (k 0)) (d (n "NT-anchor-spl") (r "^0.19.2") (f (quote ("dex"))) (d #t) (k 0)) (d (n "solana-program") (r "^1.7.11") (d #t) (k 0)))) (h "1wxil1ypjk2653nqv714f9d38kggf9hphapdf9f8fc2bwxxk03w9") (f (quote (("no-idl") ("no-entrypoint") ("devnet") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-ntserum-swap-0.5.1 (c (n "ntserum-swap") (v "0.5.1") (d (list (d (n "NT-anchor-lang") (r "^0.19.0") (d #t) (k 0)) (d (n "NT-anchor-spl") (r "^0.19.2") (f (quote ("dex"))) (d #t) (k 0)) (d (n "solana-program") (r "^1.7.11") (d #t) (k 0)))) (h "096rpc674flikh01m8wzjsdhxmqrjy6qab303qhh9ipdnfm6rq7w") (f (quote (("no-idl") ("no-entrypoint") ("devnet") ("default") ("cpi" "no-entrypoint"))))))

