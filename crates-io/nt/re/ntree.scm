(define-module (crates-io nt re ntree) #:use-module (crates-io))

(define-public crate-ntree-0.1.0 (c (n "ntree") (v "0.1.0") (d (list (d (n "rand") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "ref_slice") (r "^1") (d #t) (k 0)))) (h "0090dk2r6850p58m7rz01n14r7v0xq133yv1ix8p4rlrh6b0qs7x") (f (quote (("nightly") ("default") ("bench" "nightly" "rand"))))))

