(define-module (crates-io nt re ntree-macros) #:use-module (crates-io))

(define-public crate-ntree-macros-0.1.0 (c (n "ntree-macros") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (d #t) (k 0)))) (h "01qzfazrchcqm29cxn988vlrvbas5442x5788ylyxcrms3cqlhyc")))

(define-public crate-ntree-macros-0.1.1 (c (n "ntree-macros") (v "0.1.1") (d (list (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (d #t) (k 0)))) (h "0zm1iaz45lvj586y7n06ch1vqq43d33r91aj7dx7ssfzqz1nwb6j")))

(define-public crate-ntree-macros-0.1.2 (c (n "ntree-macros") (v "0.1.2") (d (list (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (d #t) (k 0)))) (h "013w60wy1gjp3758r4gg158p438qcwmsrxlskgp7q59f6kv6lgs5")))

(define-public crate-ntree-macros-0.1.3 (c (n "ntree-macros") (v "0.1.3") (d (list (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (d #t) (k 0)))) (h "0m3fay97g60lg0pq9032f2bp8qpmv0h6vbzscbaxv9n0mxjkn2zj")))

(define-public crate-ntree-macros-0.1.4 (c (n "ntree-macros") (v "0.1.4") (d (list (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (d #t) (k 0)))) (h "1aqfxgsccg1lw5n4xi6md41pv1x4ck2738zrijbrskq6p4wyzblj")))

