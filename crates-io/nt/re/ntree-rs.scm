(define-module (crates-io nt re ntree-rs) #:use-module (crates-io))

(define-public crate-ntree-rs-0.1.0 (c (n "ntree-rs") (v "0.1.0") (d (list (d (n "async-recursion") (r "^1.0.4") (o #t) (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.24.1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1km68b57sxzrhsb9h987sfz7aikxfy4j7m9jg3pg6r57326abmwh") (f (quote (("async" "async-recursion" "futures"))))))

(define-public crate-ntree-rs-0.1.1 (c (n "ntree-rs") (v "0.1.1") (d (list (d (n "async-recursion") (r "^1.0.4") (o #t) (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.24.1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "07fkkcr2r7jyz3zvvwvqi1vd4czh10ylzdb45d92vi5iws0airai") (f (quote (("async" "async-recursion" "futures"))))))

(define-public crate-ntree-rs-0.1.3 (c (n "ntree-rs") (v "0.1.3") (d (list (d (n "async-recursion") (r "^1.0.4") (o #t) (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.24.1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0rm8m6ljdwmdj0r7kyvdk16fk3yc7x39j48769257wq8xc9vkfjx") (f (quote (("default" "async") ("async" "async-recursion" "futures"))))))

(define-public crate-ntree-rs-0.1.4 (c (n "ntree-rs") (v "0.1.4") (d (list (d (n "async-recursion") (r "^1.0.4") (o #t) (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.24.1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1ii7i2ha0m2pnn38gsfs3fylwkfxcjvjfbk1kxjgmhhba2p1nbhw") (f (quote (("default" "async") ("async" "async-recursion" "futures"))))))

(define-public crate-ntree-rs-0.1.5 (c (n "ntree-rs") (v "0.1.5") (d (list (d (n "async-recursion") (r "^1.0.5") (o #t) (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.24.1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "03l6vpczjsssf2j8w9xj8fw1ipgd5yc2s8wr6dxx42c1cmgphsvw") (f (quote (("default" "async") ("async" "async-recursion" "futures"))))))

(define-public crate-ntree-rs-0.1.6 (c (n "ntree-rs") (v "0.1.6") (d (list (d (n "async-recursion") (r "^1.0.5") (o #t) (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.24.1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1zhq54ywz2sw6m0gwkw9piqxh4plxp877928phljidd43pcz6c2k") (f (quote (("default" "async") ("async" "async-recursion" "futures"))))))

(define-public crate-ntree-rs-0.1.7 (c (n "ntree-rs") (v "0.1.7") (d (list (d (n "async-recursion") (r "^1.0.5") (o #t) (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.24.1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0xij8g5g91zp4b2fpbvxrxjffk1m507pk9m02x7g5m61w5vw6a5h") (f (quote (("default" "async") ("async" "async-recursion" "futures"))))))

(define-public crate-ntree-rs-0.1.8 (c (n "ntree-rs") (v "0.1.8") (d (list (d (n "async-recursion") (r "^1.0.5") (o #t) (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.24.1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1dkn5js5jh3i8rsff1wy7g335z3chz8nmsja2r35bqp9pa0b8h4l") (f (quote (("default" "async") ("async" "async-recursion" "futures"))))))

(define-public crate-ntree-rs-0.1.9 (c (n "ntree-rs") (v "0.1.9") (d (list (d (n "async-recursion") (r "^1.0.5") (o #t) (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.24.1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0y33vk1kizlkz9hc103rrjy4yz85892fn19qwdh2wjrlz6pzwbmf") (f (quote (("default" "async") ("async" "async-recursion" "futures"))))))

