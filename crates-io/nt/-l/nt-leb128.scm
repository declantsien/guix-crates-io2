(define-module (crates-io nt -l nt-leb128) #:use-module (crates-io))

(define-public crate-nt-leb128-0.2.4 (c (n "nt-leb128") (v "0.2.4") (d (list (d (n "bytes") (r "^0.4.9") (d #t) (k 0)))) (h "1h4rizd2gshgy0jaxrwmy0gwr7jl320rdac4chgyix049zf1zv4x") (f (quote (("use-bytes") ("std") ("nightly") ("default" "std"))))))

(define-public crate-nt-leb128-0.3.0 (c (n "nt-leb128") (v "0.3.0") (d (list (d (n "bytes") (r "^0.5.2") (d #t) (k 0)))) (h "18g8ysw100v6zh602fplmjf1b7n7dhzgiv22vcxynx7lqcn1zqwq")))

(define-public crate-nt-leb128-0.3.1 (c (n "nt-leb128") (v "0.3.1") (d (list (d (n "bytes") (r "^0.5.2") (d #t) (k 0)))) (h "0iassqbnxx6pnq51prk59li58j6pmvncafavm53f0lhg0niaf5h1")))

