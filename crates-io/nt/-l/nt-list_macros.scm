(define-module (crates-io nt -l nt-list_macros) #:use-module (crates-io))

(define-public crate-nt-list_macros-0.1.0 (c (n "nt-list_macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("derive" "parsing" "printing" "proc-macro"))) (k 0)))) (h "000p6rc3l0a3jkx1g9awy3cd5zc42vd6ygff04zypjk3xzr81n8l") (r "1.56")))

(define-public crate-nt-list_macros-0.2.0 (c (n "nt-list_macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("derive" "parsing" "printing" "proc-macro"))) (k 0)))) (h "0jrmq265a3dw0ah7a5z7vkmqs5xmwdpqszvz05yg8xndi5fg5l0w") (r "1.56")))

(define-public crate-nt-list_macros-0.3.0 (c (n "nt-list_macros") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("derive" "parsing" "printing" "proc-macro"))) (k 0)))) (h "0l8lwkn1gv0ap4vnmzbgymzn6wh7s7nzz5lszkng4q9khabpl09z") (r "1.56")))

