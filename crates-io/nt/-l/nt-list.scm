(define-module (crates-io nt -l nt-list) #:use-module (crates-io))

(define-public crate-nt-list-0.1.0 (c (n "nt-list") (v "0.1.0") (d (list (d (n "moveit") (r "^0.5.0") (d #t) (k 0)) (d (n "nt-list_macros") (r "^0.1.0") (d #t) (k 0)))) (h "199sxb6r0sdw4xh6d7klcbyy07m1rbqnczgx9spw56kggnfgavl2") (f (quote (("default" "alloc") ("alloc")))) (r "1.56")))

(define-public crate-nt-list-0.2.0 (c (n "nt-list") (v "0.2.0") (d (list (d (n "moveit") (r "^0.5.0") (d #t) (k 0)) (d (n "nt-list_macros") (r "^0.2.0") (d #t) (k 0)))) (h "0syfdxbp8gq2i3rmq3w98c3sm5bnw26q32yqi9x1l9br727mvmqx") (f (quote (("default" "alloc") ("alloc")))) (r "1.56")))

(define-public crate-nt-list-0.2.1 (c (n "nt-list") (v "0.2.1") (d (list (d (n "moveit") (r "^0.5.0") (d #t) (k 0)) (d (n "nt-list_macros") (r "^0.2.0") (d #t) (k 0)))) (h "1273a16vccan9792d0xnfx5j1bpigi2yrff29z106ccjilm4xw9m") (f (quote (("default" "alloc") ("alloc")))) (r "1.56")))

(define-public crate-nt-list-0.3.0 (c (n "nt-list") (v "0.3.0") (d (list (d (n "moveit") (r "^0.6.0") (d #t) (k 0)) (d (n "nt-list_macros") (r "^0.3.0") (d #t) (k 0)))) (h "1s2rcizxn7a6r41336b7cjx1fr0a58ivjxlfkkm8paaq2y1xsyjx") (f (quote (("default" "alloc") ("alloc")))) (r "1.56")))

