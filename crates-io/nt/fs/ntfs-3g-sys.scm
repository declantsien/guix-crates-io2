(define-module (crates-io nt fs ntfs-3g-sys) #:use-module (crates-io))

(define-public crate-ntfs-3g-sys-0.1.0 (c (n "ntfs-3g-sys") (v "0.1.0") (d (list (d (n "autotools") (r "^0.2") (d #t) (k 1)) (d (n "bindgen") (r "^0.53") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rm_rf") (r "^0.6") (d #t) (k 1)))) (h "0diyyc6li84ic8r5z4ln9qinf71cnw8hgfmq1w36ivagcqn7dk8c")))

