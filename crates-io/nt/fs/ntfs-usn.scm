(define-module (crates-io nt fs ntfs-usn) #:use-module (crates-io))

(define-public crate-ntfs-usn-0.1.0 (c (n "ntfs-usn") (v "0.1.0") (d (list (d (n "windows-sys") (r "^0.48") (f (quote ("Win32_Foundation" "Win32_System_IO" "Win32_System_Ioctl" "Win32_Storage_FileSystem" "Win32_Security" "Win32_System_Threading" "Win32_UI_WindowsAndMessaging"))) (d #t) (k 0)))) (h "0xv2ghrw89z8fzqc8i3fh1zl90z1v38kaxxb3f7fsgdpgghnc2ac")))

