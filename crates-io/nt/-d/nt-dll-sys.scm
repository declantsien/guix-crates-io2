(define-module (crates-io nt -d nt-dll-sys) #:use-module (crates-io))

(define-public crate-nt-dll-sys-0.1.0 (c (n "nt-dll-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.57.0") (d #t) (k 1)))) (h "14fq6k5pscnwpmds1h83x8njayai26izq2i5a55az1wamgp38z1d")))

(define-public crate-nt-dll-sys-0.1.1 (c (n "nt-dll-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.57.0") (d #t) (k 1)))) (h "0pkr9vfhnzp295xa2mryc9zq3bb8v4h8dag4rmf38hf5aiaiimnb")))

(define-public crate-nt-dll-sys-0.1.2 (c (n "nt-dll-sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.57.0") (d #t) (k 1)))) (h "0cgwwvcc6dl4bya9gizv68d3pn3p7kp9dkbdjja7qm14gvn1k35s")))

