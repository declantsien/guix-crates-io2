(define-module (crates-io nt st ntstatus) #:use-module (crates-io))

(define-public crate-ntstatus-0.1.0 (c (n "ntstatus") (v "0.1.0") (d (list (d (n "num_enum") (r "^0.6.1") (d #t) (k 0)))) (h "0dcqdg5485fpmlg9ijyvb2cqgcdfridpgyip443kkr3l5jjp2dq6")))

(define-public crate-ntstatus-0.1.2 (c (n "ntstatus") (v "0.1.2") (d (list (d (n "num_enum") (r "^0.7.0") (k 0)))) (h "105i5y974qsi1is4ski3pyq42jzc8nyk5dlrzvzfijx8m6k8xsln") (f (quote (("std") ("default" "std" "num_enum/default"))))))

