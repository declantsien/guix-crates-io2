(define-module (crates-io nt im ntimes) #:use-module (crates-io))

(define-public crate-ntimes-0.1.0 (c (n "ntimes") (v "0.1.0") (h "1qisxc8vz34g0b8flm4f918zwsvnq6dmvlgxk6iba2lz0zlspsy7")))

(define-public crate-ntimes-0.1.1 (c (n "ntimes") (v "0.1.1") (h "0j13lmj0lhfms8j5mcjarvldpi3ff5vi2fj7fp0gd201ki3mqmjg")))

