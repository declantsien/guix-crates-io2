(define-module (crates-io nt -a nt-apiset) #:use-module (crates-io))

(define-public crate-nt-apiset-0.1.0 (c (n "nt-apiset") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 2)) (d (n "bitflags") (r "^2.3.1") (d #t) (k 0)) (d (n "displaydoc") (r "^0.2.4") (k 0)) (d (n "nt-string") (r "^0.1.0") (k 0)) (d (n "pelite") (r "^0.10.0") (o #t) (d #t) (k 0)) (d (n "zerocopy") (r "^0.6.1") (d #t) (k 0)))) (h "097h00kcpi916gcvxwkg6ixc2wc97rimm5srnwnpcf3nw1p33arh") (f (quote (("std" "nt-string/std") ("default" "pelite" "std")))) (r "1.58")))

