(define-module (crates-io nt -a nt-anchor-spl-testnet) #:use-module (crates-io))

(define-public crate-NT-anchor-spl-testnet-0.19.2 (c (n "NT-anchor-spl-testnet") (v "0.19.2") (d (list (d (n "NT-anchor-lang") (r "^0.19.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serum_dex") (r "^0.4.0") (f (quote ("no-entrypoint"))) (o #t) (d #t) (k 0)) (d (n "solana-program") (r "^1.8.0") (d #t) (k 0)) (d (n "spl-associated-token-account") (r "^1.0.3") (f (quote ("no-entrypoint"))) (d #t) (k 0)) (d (n "spl-token") (r "^3.1.1") (f (quote ("no-entrypoint"))) (d #t) (k 0)))) (h "0jfnd7g9ykhqd0zqiw3g7y3368civ12hk0y6v7pj3vfr1ayr8khf") (f (quote (("token") ("shmem") ("mint") ("governance") ("dex" "serum_dex") ("devnet") ("default" "mint" "token" "associated_token" "dex") ("associated_token"))))))

