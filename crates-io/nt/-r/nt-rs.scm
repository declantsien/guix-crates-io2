(define-module (crates-io nt -r nt-rs) #:use-module (crates-io))

(define-public crate-nt-rs-0.0.1 (c (n "nt-rs") (v "0.0.1") (d (list (d (n "flume") (r "^0.11.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.30") (d #t) (k 0)) (d (n "http") (r "^1.0.0") (d #t) (k 0)) (d (n "rmp") (r "^0.8.12") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.53") (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-tungstenite") (r "^0.21.0") (d #t) (k 0)) (d (n "tungstenite") (r "^0.21.0") (d #t) (k 0)))) (h "03nv926slhiq2dzd5zqdmmi4i8xnzvzip6wv6kf3b5jgldic2kmg")))

