(define-module (crates-io nt tb nttb-api) #:use-module (crates-io))

(define-public crate-nttb-api-0.1.0 (c (n "nttb-api") (v "0.1.0") (d (list (d (n "base64ct") (r "^1.6.0") (f (quote ("std"))) (d #t) (k 0)) (d (n "hmac") (r "^0.12.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.16") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_with") (r "^2.3.2") (f (quote ("json"))) (d #t) (k 0)) (d (n "sha2") (r "^0.10.6") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "tokio") (r "^1.28.0") (f (quote ("rt" "macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "11bfd4vc4rjq8939icrqbb72yg3qxhm4cij66g24k94a22p9lc4h")))

