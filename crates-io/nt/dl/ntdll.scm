(define-module (crates-io nt dl ntdll) #:use-module (crates-io))

(define-public crate-ntdll-0.0.1 (c (n "ntdll") (v "0.0.1") (d (list (d (n "winapi") (r "^0.3.5") (f (quote ("ntdef"))) (d #t) (k 0)))) (h "1m6lw3wp9jxzmlf1v9mn824ab4jly8fdhszjqnz525cgyrf247vb") (l "ntdll")))

(define-public crate-ntdll-0.0.2 (c (n "ntdll") (v "0.0.2") (d (list (d (n "winapi") (r "^0.3.5") (f (quote ("ntdef"))) (d #t) (k 0)))) (h "0k95mpxg8zdxwifrgyi6c5s3wywinbrkydr59sa04ivwayzn666k") (l "ntdll")))

(define-public crate-ntdll-0.0.3 (c (n "ntdll") (v "0.0.3") (d (list (d (n "winapi") (r "^0.3.5") (f (quote ("ntdef"))) (d #t) (k 0)))) (h "1clv17bg3qw36mjv9nb3mjpks6aqb3igiyfq9pwx7pzyvz1hr446") (l "ntdll")))

