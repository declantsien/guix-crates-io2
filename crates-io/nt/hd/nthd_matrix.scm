(define-module (crates-io nt hd nthd_matrix) #:use-module (crates-io))

(define-public crate-nthD_Matrix-0.1.0 (c (n "nthD_Matrix") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "02s8ignl3589dqlm4zk9w34x7pfr9n9ihn9fpc6gwdizz3rji9q8")))

(define-public crate-nthD_Matrix-0.1.1 (c (n "nthD_Matrix") (v "0.1.1") (d (list (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "0rfk4w5fzj9q4jxrghfilv8g8wnw5r8fqp19p6shsw67cnsqnbzq")))

(define-public crate-nthD_Matrix-0.1.2 (c (n "nthD_Matrix") (v "0.1.2") (d (list (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "16wlr6w3i06xzr71svnbkvir9lhlrd401spv9yjx9110nwpznhbd")))

