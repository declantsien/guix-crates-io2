(define-module (crates-io nt ru ntrumls) #:use-module (crates-io))

(define-public crate-ntrumls-0.1.0 (c (n "ntrumls") (v "0.1.0") (d (list (d (n "gcc") (r "^0.3.19") (d #t) (k 1)) (d (n "libc") (r "^0.2.2") (d #t) (k 0)) (d (n "rand") (r "^0.3.12") (d #t) (k 2)))) (h "1pqzy6rkxq68f5dxi6y6k11zq15i1lz0mqd323w12hgdgzj4wbyi")))

(define-public crate-ntrumls-0.5.0 (c (n "ntrumls") (v "0.5.0") (d (list (d (n "gcc") (r "^0.3.19") (d #t) (k 1)) (d (n "libc") (r "^0.2.2") (d #t) (k 0)) (d (n "rand") (r "^0.3.12") (d #t) (k 2)))) (h "105g0g4gj6ni51hl6d0589q595cb5nzpg6xx78gln4afvhyxga9j")))

(define-public crate-ntrumls-0.6.0 (c (n "ntrumls") (v "0.6.0") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "1b1q1lly91dmc5j9f5xhj80bq5pwz0cv1kxnishklinwyidccg95")))

(define-public crate-ntrumls-0.6.1 (c (n "ntrumls") (v "0.6.1") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "1cx7c9zpmqgkscipypr6mc3i1cqgw7wjk14ffbdvq1g18g57cxmx")))

(define-public crate-ntrumls-0.6.2 (c (n "ntrumls") (v "0.6.2") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "1isra7f0qwx9v004zs1kaayl9pvs4n5484f6b2i8nld2nqwagir2")))

(define-public crate-ntrumls-0.7.0 (c (n "ntrumls") (v "0.7.0") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "0xac2ndiqa2yxn0czj4bkrzrwy5ay75zqg8d3g6ikh7qba09va66")))

(define-public crate-ntrumls-0.7.1 (c (n "ntrumls") (v "0.7.1") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "1k0q7s6w3vaall4vwpm296iamq23c5x38mqiglfzckqip6bf8iy2")))

(define-public crate-ntrumls-0.7.2 (c (n "ntrumls") (v "0.7.2") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "0n9sg4hcy897l87f5q5igm8yihxw90fdb8g64ydavwmvb9zvarxm")))

(define-public crate-ntrumls-0.7.3 (c (n "ntrumls") (v "0.7.3") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "0l2jsi0fxkrbvcjr83dih965a0xasna9m7dl2m9g2186vbwcib5d")))

(define-public crate-ntrumls-0.8.0 (c (n "ntrumls") (v "0.8.0") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "1rff4w2c3pvasj00vjqkax7h06s077xh1q79gkz33gj1mccbp70f")))

(define-public crate-ntrumls-0.8.1 (c (n "ntrumls") (v "0.8.1") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "1rkkxbh7hr0gdj35cnfr5yxgqdvs51dvahzyc6hkp6n0172zrh6w")))

(define-public crate-ntrumls-0.8.2 (c (n "ntrumls") (v "0.8.2") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "0874hssnrx3bby3ns948dpx9y3ixkwp0aq2fjazfvyha75xbz269")))

(define-public crate-ntrumls-0.9.1 (c (n "ntrumls") (v "0.9.1") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "1vsq8gzsvz4lcghqr4k2r2a3g69j20wqm3mm6xki48yn7mi6l7gn")))

(define-public crate-ntrumls-0.9.2 (c (n "ntrumls") (v "0.9.2") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "15x85gkqbsiw3b327sfsglqh58xmdhvsks0l0y1cy0qks0fnayf7")))

(define-public crate-ntrumls-0.9.3 (c (n "ntrumls") (v "0.9.3") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "0n5i08q9s9y9zm82jss1mf8472wkxlqdspbkryr4jpvmz38fpp10")))

(define-public crate-ntrumls-0.9.4 (c (n "ntrumls") (v "0.9.4") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "0507gjpnlyb7wz1w3v3lg5s210hngx8jx9y7vjdikngvl5vak9xl")))

(define-public crate-ntrumls-0.9.5 (c (n "ntrumls") (v "0.9.5") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "1pd92njg9lkjsyfiql9cpzwwk70azrwg2kfsm1b6wrp0dr0mw364")))

(define-public crate-ntrumls-0.9.6 (c (n "ntrumls") (v "0.9.6") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "016q55z3bjf2crmr9df99yvaspwzsxhjd5vizvbs8w8kvwaczwai")))

