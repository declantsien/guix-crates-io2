(define-module (crates-io nt ru ntru) #:use-module (crates-io))

(define-public crate-ntru-0.0.1 (c (n "ntru") (v "0.0.1") (d (list (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "0vibm2cyc97g530mc9mhngckna5r17y0fj3yz9vpblixxxc6i8c8")))

(define-public crate-ntru-0.1.0 (c (n "ntru") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.2") (d #t) (k 0)))) (h "0iphz3jmk4xcryf1kxfflpg7ilszsl0nhp4gk54c0cffi955xyb0")))

(define-public crate-ntru-0.2.0 (c (n "ntru") (v "0.2.0") (d (list (d (n "libc") (r "^0.2.2") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.34") (d #t) (k 2)))) (h "1q7sgl9l50jgwk6m22q005nkxs2417wk8r6hk39hi9a3dn996l44")))

(define-public crate-ntru-0.4.0-rc1 (c (n "ntru") (v "0.4.0-rc1") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 2)))) (h "0b5vaqprwns6psfnds4kwj305r3shn982f164xvf9b20hc41yy08") (f (quote (("sse") ("no-sse") ("default"))))))

(define-public crate-ntru-0.4.2-rc1 (c (n "ntru") (v "0.4.2-rc1") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 2)))) (h "1acvaiyfcs4z9rm242bwy819m5p78j3wl6hh1rshl5llycry08i6") (f (quote (("sse") ("no-sse") ("default"))))))

(define-public crate-ntru-0.4.2-rc2 (c (n "ntru") (v "0.4.2-rc2") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 2)))) (h "048hp6n8bvr6lv8cj8s8i9x76s0m12japhl5szvi9r2w1nj21ybg") (f (quote (("sse") ("no-sse") ("default"))))))

(define-public crate-ntru-0.4.2-rc3 (c (n "ntru") (v "0.4.2-rc3") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 2)))) (h "1igaicxc2knc0pc7pwj0m7h8v1nc8ihwrrgc73mjjdn5nfx20v66") (f (quote (("sse") ("no-sse") ("default"))))))

(define-public crate-ntru-0.4.2-rc4 (c (n "ntru") (v "0.4.2-rc4") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 2)))) (h "18142c7acvn5z8sgij6i9qzr2blmbvafq9y262l82z3jb2zl13l3") (f (quote (("sse") ("no-sse") ("default"))))))

(define-public crate-ntru-0.4.2-rc5 (c (n "ntru") (v "0.4.2-rc5") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 2)))) (h "1092idyim6yyxkwhwyxhq7m54l6j22yjn9lacqzlnhxkixgmwx8z") (f (quote (("sse") ("no-sse") ("default"))))))

(define-public crate-ntru-0.4.3 (c (n "ntru") (v "0.4.3") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 2)))) (h "0c5rxqf6lp2cdini2a03hilyglms33wd8hc1s3nss8w4rr73y6gc") (f (quote (("sse") ("no-sse") ("default"))))))

(define-public crate-ntru-0.5.0 (c (n "ntru") (v "0.5.0") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 2)))) (h "1lb1ana50gpc4xs2dg3msw3zv7qsva02mxlngx6wpiyxyhqcdjrs") (f (quote (("sse") ("no-sse") ("no-avx2") ("default") ("avx2"))))))

(define-public crate-ntru-0.5.1 (c (n "ntru") (v "0.5.1") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 2)))) (h "1j756wc0py41z5yb231qmxv318hsvylqpdqmkj75nzrpsr8n24mx") (f (quote (("sse") ("no-sse") ("no-avx2") ("default") ("avx2"))))))

(define-public crate-ntru-0.5.2 (c (n "ntru") (v "0.5.2") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 2)))) (h "1591r7vipjg9c2q1dib0983bc1zla1dkja6krg9p1bc3fvlly3wr") (f (quote (("sse") ("no-sse") ("no-avx2") ("default") ("avx2"))))))

(define-public crate-ntru-0.5.3 (c (n "ntru") (v "0.5.3") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 2)))) (h "198qqkw9scb87zmknslr9s9gmc9jsy35fsk7c0j09dahq53njm0x") (f (quote (("sse") ("no-sse") ("no-avx2") ("default") ("avx2"))))))

(define-public crate-ntru-0.5.4 (c (n "ntru") (v "0.5.4") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 2)))) (h "0q2n84qzw710ynmmz72zb2vq6skcsjbc80p9zhsks8fqbin0yyjs") (f (quote (("sse") ("no-sse") ("no-avx2") ("default") ("avx2"))))))

(define-public crate-ntru-0.5.5 (c (n "ntru") (v "0.5.5") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 2)))) (h "1bcih4dgnis195m03vfakkxmqmjiihybwi3qbbnpmja2k8i9g1mc") (f (quote (("sse") ("no-sse") ("no-avx2") ("default") ("avx2")))) (y #t)))

(define-public crate-ntru-0.5.6 (c (n "ntru") (v "0.5.6") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 2)))) (h "11x9cacw9bx9wxsdcg2402dkgv8607rz3kzx7kdjihcrnkpyqa8m") (f (quote (("sse") ("no-sse") ("no-avx2") ("default") ("avx2"))))))

