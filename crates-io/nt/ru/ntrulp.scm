(define-module (crates-io nt ru ntrulp) #:use-module (crates-io))

(define-public crate-ntrulp-0.1.4 (c (n "ntrulp") (v "0.1.4") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "num_cpus") (r "^1.16.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1rffnbkaw478sis6qsgmn869q28iyrvljkzxzydjr2r5gsbap3sg")))

(define-public crate-ntrulp-0.1.5 (c (n "ntrulp") (v "0.1.5") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "num_cpus") (r "^1.16.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1xan8gdqc8ijzbcwryqac0vfbayfyi28jd6gzz9kjfrjb6rxnhhz") (f (quote (("ntrulpr953") ("ntrulpr857") ("ntrulpr761") ("ntrulpr653") ("ntrulpr1277") ("ntrulpr1013") ("default" "ntrulpr761"))))))

(define-public crate-ntrulp-0.1.6 (c (n "ntrulp") (v "0.1.6") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "num_cpus") (r "^1.16.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1w282kysy2m06djv7hhjp5bngiww2ghjlbpznmzp9ccspw2258n2") (f (quote (("ntrup953") ("ntrup857") ("ntrup761") ("ntrup653") ("ntrup1277") ("ntrup1013"))))))

(define-public crate-ntrulp-0.1.7 (c (n "ntrulp") (v "0.1.7") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "num_cpus") (r "^1.16.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1x1gxama9ywdpgffas2bh014hy9ajary7y8202xaw672ba6fl2k1") (f (quote (("ntrup953") ("ntrup857") ("ntrup761") ("ntrup653") ("ntrup1277") ("ntrup1013"))))))

(define-public crate-ntrulp-0.1.8 (c (n "ntrulp") (v "0.1.8") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "num_cpus") (r "^1.16.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "16zhmr0hm2gbf05jn6zpf2p1gi8i72hzx56pgb1fwdz2frmdn9q8") (f (quote (("ntrup953") ("ntrup857") ("ntrup761") ("ntrup653") ("ntrup1277") ("ntrup1013"))))))

(define-public crate-ntrulp-0.1.9 (c (n "ntrulp") (v "0.1.9") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "num_cpus") (r "^1.16.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1v3lwfflfpajvxsnpwhvl49by642i2kwyrs89q1vis8qzmdm8l8c") (f (quote (("ntrup953") ("ntrup857") ("ntrup761") ("ntrup653") ("ntrup1277") ("ntrup1013"))))))

