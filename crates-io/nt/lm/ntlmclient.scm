(define-module (crates-io nt lm ntlmclient) #:use-module (crates-io))

(define-public crate-ntlmclient-0.1.0 (c (n "ntlmclient") (v "0.1.0") (d (list (d (n "base64") (r "^0.21") (d #t) (k 2)) (d (n "bitflags") (r "^2.4") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "cipher") (r "^0.4") (d #t) (k 0)) (d (n "des") (r "^0.8") (d #t) (k 0)) (d (n "digest") (r "^0.10") (d #t) (k 0)) (d (n "hmac") (r "^0.12") (d #t) (k 0)) (d (n "md-5") (r "^0.10") (d #t) (k 0)) (d (n "md4") (r "^0.10") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("cookies"))) (d #t) (k 2)) (d (n "windows") (r "^0.51") (f (quote ("Win32_Globalization"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0040f3kl1vb13r8a03kdc3gvy0d8vqr2mqmq3w5nh4c551jkl3p9")))

