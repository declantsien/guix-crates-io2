(define-module (crates-io nt -p nt-primes) #:use-module (crates-io))

(define-public crate-nt-primes-0.0.0 (c (n "nt-primes") (v "0.0.0") (d (list (d (n "number-theory") (r "^0.0.4") (d #t) (k 0)))) (h "00jkwrrhyawsybmxjlixarlxd8bv2d6z289qly2v0ml0wqmcs96g")))

(define-public crate-nt-primes-0.1.0 (c (n "nt-primes") (v "0.1.0") (d (list (d (n "number-theory") (r "^0.0.9") (d #t) (k 0)))) (h "1m1hw1d03k6mqh8kq2hrcbjlp0lkfk6shi46v78apa6r3qy37nnc")))

(define-public crate-nt-primes-0.1.1 (c (n "nt-primes") (v "0.1.1") (d (list (d (n "number-theory") (r "^0.0.24") (d #t) (k 0)))) (h "02yr2l81xp73vfrcizsd4ph42sgjlmsfk9clwiqsrkhb2m6rbjcq")))

