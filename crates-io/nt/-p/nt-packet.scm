(define-module (crates-io nt -p nt-packet) #:use-module (crates-io))

(define-public crate-nt-packet-0.1.0 (c (n "nt-packet") (v "0.1.0") (d (list (d (n "bytes") (r "^0.4.9") (d #t) (k 0)) (d (n "nt-leb128") (r "^0.2.4") (f (quote ("use-bytes"))) (k 0)))) (h "178yrlbhhfrjcji12v2r957wib0kga1vhwqxzzc0a2g10jjfd9v2")))

(define-public crate-nt-packet-0.1.1 (c (n "nt-packet") (v "0.1.1") (d (list (d (n "bytes") (r "^0.4.9") (d #t) (k 0)) (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "nt-leb128") (r "^0.2.4") (f (quote ("use-bytes"))) (k 0)))) (h "00w2s9h3lx64ivxxk7nbx9kkq3sjf5zk8hrby94cz5p5jasxllpg")))

(define-public crate-nt-packet-0.1.2 (c (n "nt-packet") (v "0.1.2") (d (list (d (n "bytes") (r "^0.4.9") (d #t) (k 0)) (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "nt-leb128") (r "^0.2.4") (f (quote ("use-bytes"))) (k 0)))) (h "04qnk8glb8fi4qzp57v7r8f9lf7m21m7gzg80jp7x4gqijfn5pfh")))

(define-public crate-nt-packet-0.1.3 (c (n "nt-packet") (v "0.1.3") (d (list (d (n "bytes") (r "^0.4.9") (d #t) (k 0)) (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "nt-leb128") (r "^0.2.4") (f (quote ("use-bytes"))) (k 0)))) (h "1hcw110c9x4gk8p00s5sr7kbrlaizv1x0cnnrp07vmi4j5m2njgz") (y #t)))

