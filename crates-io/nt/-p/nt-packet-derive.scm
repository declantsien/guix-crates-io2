(define-module (crates-io nt -p nt-packet-derive) #:use-module (crates-io))

(define-public crate-nt-packet-derive-0.1.0 (c (n "nt-packet-derive") (v "0.1.0") (d (list (d (n "nt-packet") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.9") (d #t) (k 0)) (d (n "quote") (r "^0.6.5") (d #t) (k 0)) (d (n "syn") (r "^0.14.7") (d #t) (k 0)))) (h "0m65ss79lx4fby36d5kxbiqlx00jkzhv0f10sx70nz7al6lqa8h2") (y #t)))

(define-public crate-nt-packet-derive-0.1.1 (c (n "nt-packet-derive") (v "0.1.1") (d (list (d (n "nt-packet") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.9") (d #t) (k 0)) (d (n "quote") (r "^0.6.5") (d #t) (k 0)) (d (n "syn") (r "^0.14.7") (d #t) (k 0)))) (h "0rymmgaqf9qg5s4abaz1lh3698pqd5gw69cbd446sah2ls0yy4fn")))

(define-public crate-nt-packet-derive-0.1.2 (c (n "nt-packet-derive") (v "0.1.2") (d (list (d (n "nt-packet") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.9") (d #t) (k 0)) (d (n "quote") (r "^0.6.5") (d #t) (k 0)) (d (n "syn") (r "^0.14.7") (d #t) (k 0)))) (h "0wka642lsbqil29465vaw8636lpcbhr1y2ajl0p1hf9z847jkbg5")))

(define-public crate-nt-packet-derive-0.1.3 (c (n "nt-packet-derive") (v "0.1.3") (d (list (d (n "nt-packet") (r "^0.1.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.9") (d #t) (k 0)) (d (n "quote") (r "^0.6.5") (d #t) (k 0)) (d (n "syn") (r "^0.14.7") (d #t) (k 0)))) (h "0l1g3ycq2bjxq3yd7x48qfhk9arvi5xdmj3ygy26v91x3dhdngph")))

(define-public crate-nt-packet-derive-0.1.4 (c (n "nt-packet-derive") (v "0.1.4") (d (list (d (n "nt-packet") (r "^0.1.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.9") (d #t) (k 0)) (d (n "quote") (r "^0.6.5") (d #t) (k 0)) (d (n "syn") (r "^0.14.7") (d #t) (k 0)))) (h "17zq3wfb9p5n66lmvyzw9jf77npmwsmxmd9n8yn98i190fk7w6zx") (y #t)))

