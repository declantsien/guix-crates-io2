(define-module (crates-io nt _v nt_version) #:use-module (crates-io))

(define-public crate-nt_version-0.1.0 (c (n "nt_version") (v "0.1.0") (d (list (d (n "winapi") (r "^0.3") (f (quote ("libloaderapi"))) (o #t) (d #t) (k 0)))) (h "0zfjy3h795911qisccskkkbcpa13ca6wcx9rfgcm3k5fyjcmxwwm") (f (quote (("fallback" "winapi") ("default")))) (y #t)))

(define-public crate-nt_version-0.1.1 (c (n "nt_version") (v "0.1.1") (d (list (d (n "winapi") (r "^0.3") (f (quote ("libloaderapi"))) (o #t) (d #t) (k 0)))) (h "0cvwk6inpq3l34i8z0hz43v4bncaiclswf9r4chz77flam5g18l3") (f (quote (("fallback" "winapi") ("default")))) (y #t)))

(define-public crate-nt_version-0.1.2 (c (n "nt_version") (v "0.1.2") (d (list (d (n "winapi") (r "^0.3") (f (quote ("libloaderapi"))) (o #t) (d #t) (k 0)))) (h "0kxnfc9dwldlkh4kbf9k8pbrvdmsjb92wpysp5j1827p8j9g9pc6") (f (quote (("fallback" "winapi") ("default")))) (y #t)))

(define-public crate-nt_version-0.1.3 (c (n "nt_version") (v "0.1.3") (d (list (d (n "winapi") (r "^0.3") (f (quote ("libloaderapi"))) (o #t) (d #t) (k 0)))) (h "19hl3ggrv1732z4l8cf78xcimfgm4j0jl1agr6aiin81dbyv73cm") (f (quote (("fallback" "winapi") ("default"))))))

