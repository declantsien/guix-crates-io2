(define-module (crates-io nt ca ntcall) #:use-module (crates-io))

(define-public crate-ntcall-0.1.0 (c (n "ntcall") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)) (d (n "iced-x86") (r "^1.15.0") (f (quote ("no_std" "decoder" "nasm"))) (k 1)) (d (n "ntapi") (r "^0.3.6") (k 0)) (d (n "ntapi") (r "^0.3.6") (d #t) (k 1)) (d (n "winapi") (r "^0.3.9") (f (quote ("ntdef"))) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("winnt" "libloaderapi"))) (k 1)))) (h "0pnryp5kp4p7jgxc6pc0pici9hn58k5f67hii2jxmn8p7cqa1d64")))

(define-public crate-ntcall-0.1.1 (c (n "ntcall") (v "0.1.1") (d (list (d (n "iced-x86") (r "^1.15.0") (f (quote ("no_std" "decoder" "nasm"))) (k 1)) (d (n "ntapi") (r "^0.3.6") (k 0)) (d (n "ntapi") (r "^0.3.6") (d #t) (k 1)) (d (n "winapi") (r "^0.3.9") (f (quote ("ntdef"))) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("winnt" "libloaderapi"))) (k 1)))) (h "1k2wz7bwhjav6dpdj1a5pkhmh1li2m4mwnvriq5gj262mmixwwyd")))

