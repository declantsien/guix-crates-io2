(define-module (crates-io d3 d9 d3d9-sys) #:use-module (crates-io))

(define-public crate-d3d9-sys-0.0.1 (c (n "d3d9-sys") (v "0.0.1") (d (list (d (n "winapi") (r "*") (d #t) (k 0)))) (h "0mrlwa0kpd154f2vp0r13hb3mba53g09a30iwb6nlphblr46ic9c")))

(define-public crate-d3d9-sys-0.0.2 (c (n "d3d9-sys") (v "0.0.2") (d (list (d (n "winapi") (r "*") (d #t) (k 0)))) (h "16mghc8bk2paj8b00fzaa34va1rq4ly5268b4sc51n27bhdpadna")))

(define-public crate-d3d9-sys-0.1.0 (c (n "d3d9-sys") (v "0.1.0") (d (list (d (n "winapi") (r "*") (d #t) (k 0)))) (h "0jjangmfns9sa5qpls5dzj1phvwnhv3db80gkpxy51d6cdv2as05")))

(define-public crate-d3d9-sys-0.1.1 (c (n "d3d9-sys") (v "0.1.1") (d (list (d (n "winapi") (r "^0.2") (d #t) (k 0)) (d (n "winapi-build") (r "^0.1.1") (d #t) (k 1)))) (h "1pny9yfzclq4kq6hks3b63yn0lazbx6v3a3y9ddg7dml15l6xrkn")))

