(define-module (crates-io d3 -d d3-derive) #:use-module (crates-io))

(define-public crate-d3-derive-0.1.0 (c (n "d3-derive") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0xzixcdc4isgq2r2fscn4c43c1dy7r6vqkrf6wpk2h2q4klhcwqd")))

(define-public crate-d3-derive-0.1.1 (c (n "d3-derive") (v "0.1.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0qyh3y74h3j14pqdv87fbrvknlh27p1bdxpx3vh4v0kfxzlvg1fn")))

(define-public crate-d3-derive-0.1.2 (c (n "d3-derive") (v "0.1.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "08axdzqp2lazz976y0024rmd91dmyqx4b7bqacmnyj3ww1kyvmrr")))

(define-public crate-d3-derive-0.1.3 (c (n "d3-derive") (v "0.1.3") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0dcz2mahw1sndwmkadwcflawlq0zc2d41zz0n3vwbfb3fblz1yg0")))

