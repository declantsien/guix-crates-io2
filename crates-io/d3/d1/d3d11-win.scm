(define-module (crates-io d3 d1 d3d11-win) #:use-module (crates-io))

(define-public crate-d3d11-win-0.1.0 (c (n "d3d11-win") (v "0.1.0") (d (list (d (n "dxgi-win") (r "*") (d #t) (k 0)) (d (n "winapi") (r "*") (d #t) (k 0)))) (h "1wwvn3m65gpvnpyaz88h455rgv674921bx0cw4b0h5cybljz0n66") (y #t)))

(define-public crate-d3d11-win-0.1.1 (c (n "d3d11-win") (v "0.1.1") (d (list (d (n "dxgi-win") (r "*") (d #t) (k 0)) (d (n "winapi") (r "*") (d #t) (k 0)))) (h "1wwpsa214wxjiw2zp0n8h04bsycbknsv92vhngyx0s0lvz74j591") (y #t)))

(define-public crate-d3d11-win-0.2.0 (c (n "d3d11-win") (v "0.2.0") (d (list (d (n "dxgi-win") (r "*") (d #t) (k 0)) (d (n "winapi") (r "*") (d #t) (k 0)))) (h "168z5skxqxnahc4d2h3nc3xcljr5hq4vc5dpipbdp5idqzhcmkbl") (y #t)))

(define-public crate-d3d11-win-0.2.1 (c (n "d3d11-win") (v "0.2.1") (d (list (d (n "dxgi-win") (r "^0.2.1") (d #t) (k 0)) (d (n "winapi") (r "^0.2.5") (d #t) (k 0)))) (h "0mpq2y4c2aglv7zbbsnqzaxg9l6ryvb1ynbda9w8wr87x4s7rdxi") (y #t)))

