(define-module (crates-io d3 d1 d3d11-sys) #:use-module (crates-io))

(define-public crate-d3d11-sys-0.0.1 (c (n "d3d11-sys") (v "0.0.1") (d (list (d (n "winapi") (r "*") (d #t) (k 0)))) (h "0k6w3cx0hq9xs3rgsb8ncipjxqd1n8zq30lip58yrnl8px897p46")))

(define-public crate-d3d11-sys-0.1.0 (c (n "d3d11-sys") (v "0.1.0") (d (list (d (n "winapi") (r "*") (d #t) (k 0)) (d (n "winapi-build") (r "*") (d #t) (k 1)))) (h "0sq0gkfbhn8y90jk0jwvnjz7b94c9m6s97px1va3wk2cc33rzhk9")))

(define-public crate-d3d11-sys-0.2.0 (c (n "d3d11-sys") (v "0.2.0") (d (list (d (n "winapi") (r "^0.2.5") (d #t) (k 0)) (d (n "winapi-build") (r "^0.1.1") (d #t) (k 1)))) (h "00la5b5d2nqbr4qzx5raah3wv3z6p3f6xg3ijdwy6wyqv4lzfvzn")))

