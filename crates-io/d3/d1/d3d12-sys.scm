(define-module (crates-io d3 d1 d3d12-sys) #:use-module (crates-io))

(define-public crate-d3d12-sys-0.0.1 (c (n "d3d12-sys") (v "0.0.1") (d (list (d (n "winapi") (r "*") (d #t) (k 0)))) (h "1ihdcr86ragmj33sbcjvx0xm83qjgclmffjdmvxa0161sb6sfi0l")))

(define-public crate-d3d12-sys-0.2.0 (c (n "d3d12-sys") (v "0.2.0") (d (list (d (n "winapi") (r "^0.2.5") (d #t) (k 0)) (d (n "winapi-build") (r "^0.1.1") (d #t) (k 1)))) (h "1cwn21fjaazsp662wbq2lypy259vw6drlfhxhs6kw5xsk3xr7bq6")))

