(define-module (crates-io d3 #{64}# d364cb02-af78-4e8d-9444-89788203d49b) #:use-module (crates-io))

(define-public crate-d364cb02-af78-4e8d-9444-89788203d49b-0.1.0 (c (n "d364cb02-af78-4e8d-9444-89788203d49b") (v "0.1.0") (d (list (d (n "d364cb02-af78-4e8d-9444-89788203d49b-internal") (r "^0.1.0") (d #t) (k 0)))) (h "0qp6kj56kgb5kcr02vipkcnh09rhmkzfnqgx5bqrglb4s9b83yn7") (y #t)))

(define-public crate-d364cb02-af78-4e8d-9444-89788203d49b-0.2.0 (c (n "d364cb02-af78-4e8d-9444-89788203d49b") (v "0.2.0") (d (list (d (n "d364cb02-af78-4e8d-9444-89788203d49b-internal") (r "^0.2.0") (d #t) (k 0)))) (h "08zs22xvnzzwazzvmvbs9xpikacpacarknbpi3zq1z9haq11b7qv") (y #t)))

(define-public crate-d364cb02-af78-4e8d-9444-89788203d49b-0.2.1 (c (n "d364cb02-af78-4e8d-9444-89788203d49b") (v "0.2.1") (d (list (d (n "d364cb02-af78-4e8d-9444-89788203d49b-internal") (r "^0.2.1") (d #t) (k 0)))) (h "18psggqmwdw4gmvibvrl6w81cfsvpd4abqff9wx36rxk8vqlf8z3") (y #t)))

