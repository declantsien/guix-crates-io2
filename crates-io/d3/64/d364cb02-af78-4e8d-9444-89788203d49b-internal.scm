(define-module (crates-io d3 #{64}# d364cb02-af78-4e8d-9444-89788203d49b-internal) #:use-module (crates-io))

(define-public crate-d364cb02-af78-4e8d-9444-89788203d49b-internal-0.1.0 (c (n "d364cb02-af78-4e8d-9444-89788203d49b-internal") (v "0.1.0") (h "098c9vzjwpqkgi45mahx46w7yx796adc85skfy0gzqgsyhzn391d") (y #t)))

(define-public crate-d364cb02-af78-4e8d-9444-89788203d49b-internal-0.2.0 (c (n "d364cb02-af78-4e8d-9444-89788203d49b-internal") (v "0.2.0") (h "04zxbwswkqnl2qwc9x0s01c7mbg5m2c3iyxpr425ai62628wigmn") (y #t)))

(define-public crate-d364cb02-af78-4e8d-9444-89788203d49b-internal-0.2.1 (c (n "d364cb02-af78-4e8d-9444-89788203d49b-internal") (v "0.2.1") (h "1k3jiqcy6iiqfxfgr2k111id6sss2cfydjbdpngz9yx0rrcjvnpj") (y #t)))

