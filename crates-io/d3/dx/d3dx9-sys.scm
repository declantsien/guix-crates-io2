(define-module (crates-io d3 dx d3dx9-sys) #:use-module (crates-io))

(define-public crate-d3dx9-sys-0.1.0 (c (n "d3dx9-sys") (v "0.1.0") (d (list (d (n "winapi") (r "^0.3.9") (f (quote ("d3d9" "d3d9types" "d3d9caps" "minwindef" "unknwnbase" "winnt" "winbase"))) (d #t) (k 0)))) (h "1rgbaiqpa2n71fy7pp64fvnnp6w6kyqaca5vkrma07xz2lwvhfy5") (y #t)))

