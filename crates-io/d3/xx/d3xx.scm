(define-module (crates-io d3 xx d3xx) #:use-module (crates-io))

(define-public crate-d3xx-0.0.1 (c (n "d3xx") (v "0.0.1") (d (list (d (n "libftd3xx-ffi") (r "^0.0.2") (f (quote ("bindgen"))) (d #t) (k 0)) (d (n "num_enum") (r "^0.7.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "1y7v8ndbjabn3xm8h7b3fdqsf3z6zrd3hxmi4vr8vwnvlf2cs7jn") (f (quote (("static" "libftd3xx-ffi/static") ("default") ("config")))) (y #t) (r "1.56.0")))

(define-public crate-d3xx-0.0.2 (c (n "d3xx") (v "0.0.2") (d (list (d (n "libftd3xx-ffi") (r "^0.0.2") (d #t) (k 0)) (d (n "num_enum") (r "^0.7.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)) (d (n "widestring") (r "^1.0.2") (d #t) (k 0)))) (h "02sgcnzy8yha39i3j11b697bg6372dww5x1kp1pby12184lxdxvl") (f (quote (("static" "libftd3xx-ffi/static") ("default") ("config")))) (r "1.56.0")))

(define-public crate-d3xx-0.0.3 (c (n "d3xx") (v "0.0.3") (d (list (d (n "libftd3xx-ffi") (r "^0.0.2") (d #t) (k 0)) (d (n "num_enum") (r "^0.7.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)) (d (n "widestring") (r "^1.0.2") (d #t) (k 0)))) (h "186f70pksmk5mzmgd5yg7y935mywp1j7wvi9wkrg355xrqil3vwc") (f (quote (("static" "libftd3xx-ffi/static") ("default") ("config")))) (r "1.58.0")))

