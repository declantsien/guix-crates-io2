(define-module (crates-io d3 dc d3dcompiler-sys) #:use-module (crates-io))

(define-public crate-d3dcompiler-sys-0.0.1 (c (n "d3dcompiler-sys") (v "0.0.1") (d (list (d (n "winapi") (r "*") (d #t) (k 0)))) (h "1778sc48yamqp5zckadlr9w1vkm869gz4bgkqsryn2vzx8y6w2s0")))

(define-public crate-d3dcompiler-sys-0.2.0 (c (n "d3dcompiler-sys") (v "0.2.0") (d (list (d (n "winapi") (r "^0.2.5") (d #t) (k 0)) (d (n "winapi-build") (r "^0.1.1") (d #t) (k 1)))) (h "191crqady50f2yqa3af1pg2r796yrmm485m2y01mvd6i6224f280")))

