(define-module (crates-io sj qm sjqmmo) #:use-module (crates-io))

(define-public crate-sjqmmo-0.1.0 (c (n "sjqmmo") (v "0.1.0") (d (list (d (n "bevy") (r "^0.12.1") (f (quote ("bevy_asset" "bevy_audio" "png" "hdr" "bevy_winit" "bevy_core_pipeline" "bevy_pbr" "bevy_render" "multi-threaded" "tonemapping_luts" "vorbis" "zstd" "x11" "bevy_gilrs"))) (k 0)) (d (n "bevy-inspector-egui") (r "^0.22.0") (d #t) (k 0)) (d (n "bevy_rapier3d") (r "^0.24.0") (f (quote ("simd-stable" "debug-render"))) (d #t) (k 0)) (d (n "bevy_third_person_camera") (r "^0.1.8") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0wnqmn0qk008yxqf0gcq2h08a1yw31daibvga18w7bfk6wsal0v8") (f (quote (("webgl2" "bevy/webgl2") ("default"))))))

