(define-module (crates-io sj is sjis-literals) #:use-module (crates-io))

(define-public crate-sjis-literals-0.1.0 (c (n "sjis-literals") (v "0.1.0") (d (list (d (n "encoding_rs") (r "^0.8") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "0mzmra670rsqgdq3dr0m8j00ns1sdf7v76b10ybbi1yj1av1kzqg")))

