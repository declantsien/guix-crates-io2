(define-module (crates-io sj lj sjlj) #:use-module (crates-io))

(define-public crate-sjlj-0.0.1 (c (n "sjlj") (v "0.0.1") (d (list (d (n "naked-function") (r "^0.1.0") (d #t) (k 0)))) (h "0zmy7xwrp1nhg3a5w663ckba96vh4vqsy618grfi6zv5jfngqa27")))

(define-public crate-sjlj-0.0.2 (c (n "sjlj") (v "0.0.2") (d (list (d (n "linux-raw-sys") (r "^0.2.1") (d #t) (k 0)) (d (n "naked-function") (r "^0.1.0") (d #t) (k 0)) (d (n "sc") (r "^0.2.7") (d #t) (k 0)))) (h "0p5ydky6sb4g84mh8gkx2013zmymms1mva5whgdncs5irvpq2akk")))

(define-public crate-sjlj-0.1.0 (c (n "sjlj") (v "0.1.0") (d (list (d (n "naked-function") (r "^0.1.1") (d #t) (k 0)) (d (n "linux-raw-sys") (r "^0.2.1") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "sc") (r "^0.2.7") (d #t) (t "cfg(target_os = \"linux\")") (k 0)))) (h "06cny7z9sqilgpidrclmryim3gph98s0xa087rain50d6hm314wd")))

(define-public crate-sjlj-0.1.1 (c (n "sjlj") (v "0.1.1") (d (list (d (n "naked-function") (r "^0.1.1") (d #t) (k 0)) (d (n "linux-raw-sys") (r "^0.2.1") (f (quote ("no_std" "general"))) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "sc") (r "^0.2.7") (d #t) (t "cfg(target_os = \"linux\")") (k 0)))) (h "1v10gn9dqw1wi4rqrh3rhxqcmr19nsqn74ajrkhlqjszr0r5xrsb")))

(define-public crate-sjlj-0.1.2 (c (n "sjlj") (v "0.1.2") (d (list (d (n "naked-function") (r "^0.1.1") (d #t) (k 0)) (d (n "linux-raw-sys") (r "^0.2.1") (f (quote ("no_std" "general"))) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "sc") (r "^0.2.7") (d #t) (t "cfg(target_os = \"linux\")") (k 0)))) (h "0mygyrgvhx0940w2dnfr260zv3whqmrdnm3c7v2y23xlnhpn4gcc")))

(define-public crate-sjlj-0.1.3 (c (n "sjlj") (v "0.1.3") (d (list (d (n "naked-function") (r "^0.1.2") (d #t) (k 0)) (d (n "linux-raw-sys") (r "^0.2.1") (f (quote ("no_std" "general"))) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "sc") (r "^0.2.7") (d #t) (t "cfg(target_os = \"linux\")") (k 0)))) (h "0fcfy3vyyy15il8g0bdn7hm07irp8clll4i7vjmrl1l7qgpl76rf")))

