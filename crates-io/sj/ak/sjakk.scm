(define-module (crates-io sj ak sjakk) #:use-module (crates-io))

(define-public crate-sjakk-0.1.0 (c (n "sjakk") (v "0.1.0") (h "1lp70hyqf7w7g4ya0g5fciqm6qk97fhxwjyk1dwk15l3vg134ah1")))

(define-public crate-sjakk-0.1.1 (c (n "sjakk") (v "0.1.1") (d (list (d (n "strum") (r "^0.25.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.25.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)))) (h "0iw4f15rgvgzsvn5qk48a90kn5w3mnywyncdarrz9yifj846n2db")))

(define-public crate-sjakk-0.1.2 (c (n "sjakk") (v "0.1.2") (d (list (d (n "strum") (r "^0.25.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.25.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)))) (h "07ds8a95in5asv4zpb3nnf8w99jvk1hakqj7wc3qc4kh9kcfahd3")))

(define-public crate-sjakk-0.2.0 (c (n "sjakk") (v "0.2.0") (d (list (d (n "strum") (r "^0.25.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.25.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)))) (h "0is92y49dysg624mv2mhf2aa9fp5p9ykd3xw7alyq3x5198sr7ci")))

(define-public crate-sjakk-0.2.1 (c (n "sjakk") (v "0.2.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "strum") (r "^0.25.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.25.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)))) (h "16rs8ps7z7q0a2wlbhm1kjf521gf61ms7hzjwff5184gq5m0x798")))

(define-public crate-sjakk-0.2.2 (c (n "sjakk") (v "0.2.2") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "smallvec") (r "^1.11.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)))) (h "1v2b04yg688qcp6sc80xh3ncywc825v3y6ipsw8c6kjwhna0x0xz")))

(define-public crate-sjakk-0.2.3 (c (n "sjakk") (v "0.2.3") (d (list (d (n "clap") (r "^4.4.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "futures") (r "^0.3.28") (f (quote ("thread-pool"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "smallvec") (r "^1.11.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)))) (h "0vfg4an0530sd9wiga7djz9692g8369laj3s7ymvj3dkajzvdf8b")))

