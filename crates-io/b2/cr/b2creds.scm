(define-module (crates-io b2 cr b2creds) #:use-module (crates-io))

(define-public crate-b2creds-0.1.0 (c (n "b2creds") (v "0.1.0") (d (list (d (n "directories") (r "^3.0") (d #t) (k 0)) (d (n "rusqlite") (r "^0.24.2") (f (quote ("bundled"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.2") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "050whf28vzdj31dd1azd3f0lqkjv99fyslh545h3d0f508k12z4g")))

(define-public crate-b2creds-0.2.0 (c (n "b2creds") (v "0.2.0") (d (list (d (n "directories") (r "^5.0.1") (d #t) (k 0)) (d (n "rusqlite") (r "^0.29.0") (f (quote ("bundled"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.2") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1519cjlvwii3bzd5a3zppzagp3l3kqfv3cymiiqfjmrilnyn6d9g")))

