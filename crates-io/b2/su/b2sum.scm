(define-module (crates-io b2 su b2sum) #:use-module (crates-io))

(define-public crate-b2sum-0.1.0 (c (n "b2sum") (v "0.1.0") (d (list (d (n "docopt") (r "^0.7.0") (d #t) (k 0)) (d (n "error-chain") (r "^0.9.0") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.22") (d #t) (k 0)))) (h "0igxsrmh22kwmy6yfk05sdjx5jmsr7zpjxznwlrc5x4xqrwafffp")))

(define-public crate-b2sum-0.2.0 (c (n "b2sum") (v "0.2.0") (d (list (d (n "blake2-rfc") (r "^0.2.17") (d #t) (k 0)) (d (n "docopt") (r "^0.7.0") (d #t) (k 0)) (d (n "error-chain") (r "^0.9.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.22") (d #t) (k 0)))) (h "0xy4dkhl3b13sj9nzz9v56q01drjw2qzl3jrlb7b73zhwwld62sj")))

(define-public crate-b2sum-0.3.0 (c (n "b2sum") (v "0.3.0") (d (list (d (n "blake2b_simd") (r "^0.5.2") (d #t) (k 0)) (d (n "docopt") (r "^1.1.0") (d #t) (k 0)) (d (n "error-chain") (r "^0.12.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.92") (f (quote ("derive"))) (d #t) (k 0)))) (h "141aivsv3rc916in5ya3dimpxqa7v6mjwbgsq1mj231dqdwfrgwb")))

(define-public crate-b2sum-0.4.0 (c (n "b2sum") (v "0.4.0") (d (list (d (n "blake2b_simd") (r "^0.5.11") (d #t) (k 0)) (d (n "docopt") (r "^1.1.1") (d #t) (k 0)) (d (n "eyre") (r "^0.6.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (d #t) (k 0)) (d (n "stable-eyre") (r "^0.2.2") (d #t) (k 0)))) (h "0sgl70k2bwwmqz3pf7m97k21f6szk305nxa918krrp4xwkcisg8c")))

