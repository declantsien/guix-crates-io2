(define-module (crates-io b2 su b2sum-rs) #:use-module (crates-io))

(define-public crate-b2sum-rs-0.1.0 (c (n "b2sum-rs") (v "0.1.0") (d (list (d (n "blake2-rfc") (r "^0.2.18") (d #t) (k 0)) (d (n "filebuffer") (r "^0.4.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.2") (d #t) (k 0)))) (h "0hi0b42gxwycbii6xf3cccw93cwmf0l5rmilrvmawy9x4y66k45b")))

(define-public crate-b2sum-rs-0.1.1 (c (n "b2sum-rs") (v "0.1.1") (d (list (d (n "blake2-rfc") (r "^0.2.18") (d #t) (k 0)) (d (n "filebuffer") (r "^0.4.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.2") (d #t) (k 0)))) (h "0g5dfj7fy8nayfqqidmyxgwm7z8x9bpgmnsqja1xjidrv0s9fw6k")))

