(define-module (crates-io b2 su b2sum-rust) #:use-module (crates-io))

(define-public crate-b2sum-rust-0.1.0 (c (n "b2sum-rust") (v "0.1.0") (d (list (d (n "blake2-rfc") (r "^0.2.18") (d #t) (k 0)) (d (n "filebuffer") (r "^0.4.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.0") (d #t) (k 0)))) (h "146yj90bww5l7hni97j9a452wa2iqd0q8s0sid0qiyh2rvwhgxvn")))

(define-public crate-b2sum-rust-0.1.1 (c (n "b2sum-rust") (v "0.1.1") (d (list (d (n "blake2-rfc") (r "^0.2.18") (d #t) (k 0)) (d (n "filebuffer") (r "^0.4.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.2") (d #t) (k 0)))) (h "1w03lgj4jsalyddgjrvq1ajn5vhazxgnvdfamj0nwi8f1rglvvk6")))

(define-public crate-b2sum-rust-0.2.0 (c (n "b2sum-rust") (v "0.2.0") (d (list (d (n "blake2-rfc") (r "^0.2.18") (d #t) (k 0)) (d (n "filebuffer") (r "^0.4.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.2") (d #t) (k 0)))) (h "1n8gw7rpv1xja094xckpi5mmli59c46ivxz5xm6xj4cqbmj51rvd")))

(define-public crate-b2sum-rust-0.2.1 (c (n "b2sum-rust") (v "0.2.1") (d (list (d (n "blake2-rfc") (r "^0.2.18") (d #t) (k 0)) (d (n "filebuffer") (r "^0.4.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.2") (d #t) (k 0)))) (h "13mgmz1aywh3mvx8ridq60z4p0bh0h00pqpv0yx6vd5j802aa25f")))

(define-public crate-b2sum-rust-0.3.0 (c (n "b2sum-rust") (v "0.3.0") (d (list (d (n "blake2-rfc") (r "^0.2.18") (d #t) (k 0)) (d (n "filebuffer") (r "^0.4.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.2") (d #t) (k 0)))) (h "0396avv9jn517blg1nhh55k1wrqjkhp824d8crnvxn4kia4izdpc")))

