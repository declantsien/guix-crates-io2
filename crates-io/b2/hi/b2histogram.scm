(define-module (crates-io b2 hi b2histogram) #:use-module (crates-io))

(define-public crate-b2histogram-1.0.0 (c (n "b2histogram") (v "1.0.0") (h "0dwi1ifvlkcfkrwbzyk84rngqxfcf2k8l9jh76hsyq8py4d45hsb") (f (quote (("default"))))))

(define-public crate-b2histogram-1.0.1 (c (n "b2histogram") (v "1.0.1") (h "0iak3326nmsy1dzm4c5hdk20gy5drrr6kk2fk42ivkn0zi9zbrid") (f (quote (("default"))))))

