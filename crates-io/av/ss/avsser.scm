(define-module (crates-io av ss avsser) #:use-module (crates-io))

(define-public crate-avsser-0.4.1 (c (n "avsser") (v "0.4.1") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "14ffk3wc5lpr6300ilgdw7z5xhzd2b0ww7ya8rwa899hdqd47fq9")))

(define-public crate-avsser-0.4.2 (c (n "avsser") (v "0.4.2") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "19fl47ay16l7vlc7w93a1wiwjg4z5yvh93hg6c12xxv1rwgann2x")))

(define-public crate-avsser-0.5.0 (c (n "avsser") (v "0.5.0") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "05mz0lq2n5g9mp7yq01hqpd8m5ivc9vr6j2y2ha8zfsvqrw8dn3h")))

(define-public crate-avsser-0.6.0 (c (n "avsser") (v "0.6.0") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1dimyipawhh2hpvli48ki6m857wqr0pcf8mnn4cmyjfypb7l4fxw")))

(define-public crate-avsser-0.6.1 (c (n "avsser") (v "0.6.1") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1f03cpazi2xip6xz57jlfrc0nd5qv44fakc2zn8ip36p4fpwvcnx")))

(define-public crate-avsser-0.6.2 (c (n "avsser") (v "0.6.2") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.2") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1qqc5xk9ma48ydkd5g8v4a4sriz9njsivq3f1vwlnbh21hp5f7p9")))

(define-public crate-avsser-0.6.3 (c (n "avsser") (v "0.6.3") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.2") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "09f1bisjsb6yd2xx23fp4b53ik6fr1f6d2f30h5f3w9cy02zdgpi")))

(define-public crate-avsser-0.6.4 (c (n "avsser") (v "0.6.4") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.2") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1cwg63bgjr701b2rbnr0imbjc0fcmc6wf4q1dy0qhvd41n3b2cmk")))

(define-public crate-avsser-0.6.5 (c (n "avsser") (v "0.6.5") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.2") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0wsqdkn3yidvwan2rr30ngjchgpsmg8vnkh753nr71nxd7xssck4")))

(define-public crate-avsser-0.7.0 (c (n "avsser") (v "0.7.0") (d (list (d (n "clippy") (r ">= 0.0.110") (o #t) (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.2") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0zddyw29mz242k094vkxhlfvp8wz0qz3aap4vzghyz93kag0gxpv")))

(define-public crate-avsser-0.7.1 (c (n "avsser") (v "0.7.1") (d (list (d (n "clippy") (r ">= 0.0.110") (o #t) (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.2") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "uuid") (r "^0.5.0") (d #t) (k 0)))) (h "01abckxpyvk6xy5sjbll8gbd4cdn5jhm4zgvs69b62xxayyihgbk")))

(define-public crate-avsser-0.7.2 (c (n "avsser") (v "0.7.2") (d (list (d (n "clippy") (r ">= 0.0.110") (o #t) (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.2") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "uuid") (r "^0.5.0") (d #t) (k 0)))) (h "0l6aalnfpk51arxqajzw4nj7vl7v70ibwfxxp66sziy55syddpz3")))

(define-public crate-avsser-0.7.3 (c (n "avsser") (v "0.7.3") (d (list (d (n "clippy") (r ">= 0.0.110") (o #t) (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.2") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "uuid") (r "^0.5.0") (d #t) (k 0)))) (h "176w2q06r6jy2jv0dzr3cw6ck19yw8pg6afg74yr8000bndyqs8m")))

(define-public crate-avsser-0.8.0 (c (n "avsser") (v "0.8.0") (d (list (d (n "clippy") (r ">= 0.0.110") (o #t) (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.2") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "uuid") (r "^0.5.0") (d #t) (k 0)))) (h "1s4v568p47gwvrbwla7xm21jahrc2y8asjwdv14ids6s5bczf7rl")))

