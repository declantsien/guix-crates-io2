(define-module (crates-io av dl avdl-serde-code-generator) #:use-module (crates-io))

(define-public crate-avdl-serde-code-generator-0.1.0 (c (n "avdl-serde-code-generator") (v "0.1.0") (d (list (d (n "pest") (r "^2.1.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "regex") (r "^1.2.1") (d #t) (k 0)))) (h "03cgah7shp6fzikkr5afks6xhs300raq8s0sliysmjghqxdlzkzj")))

(define-public crate-avdl-serde-code-generator-0.1.1 (c (n "avdl-serde-code-generator") (v "0.1.1") (d (list (d (n "pest") (r "^2.1.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "regex") (r "^1.2.1") (d #t) (k 0)))) (h "0xk6nx0p19w3kglbdmnqj46yq8gz3ybx5nx5vlb9sp42a02lpm2j")))

(define-public crate-avdl-serde-code-generator-0.2.0 (c (n "avdl-serde-code-generator") (v "0.2.0") (d (list (d (n "pest") (r "^2.1.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "regex") (r "^1.2.1") (d #t) (k 0)))) (h "1cgva3yzw402r6f3ms64b04liw3rdp7jhqkr8azyjb89m23cr0sz")))

