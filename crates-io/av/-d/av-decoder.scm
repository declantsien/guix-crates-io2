(define-module (crates-io av -d av-decoder) #:use-module (crates-io))

(define-public crate-av-decoder-0.1.0 (c (n "av-decoder") (v "0.1.0") (d (list (d (n "c_str_macro") (r "^1") (d #t) (k 0)) (d (n "ffmpeg-sys-next") (r "^5") (d #t) (k 0)) (d (n "libc") (r "0.2.*") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nalgebra") (r "^0.30") (d #t) (k 0)) (d (n "ofps") (r "^0.1") (d #t) (k 0)))) (h "1616vvjcpapxrvm06527j4i1x3rl45b2ni5nb7pdz1422rbzjh3n")))

(define-public crate-av-decoder-0.1.1 (c (n "av-decoder") (v "0.1.1") (d (list (d (n "c_str_macro") (r "^1") (d #t) (k 0)) (d (n "ffmpeg-sys-next") (r "^5") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nalgebra") (r "^0.30") (d #t) (k 0)) (d (n "ofps") (r "^0.1") (d #t) (k 0)))) (h "12va45flc9rrvvnh7vv0ia9zz5g8ziwp8swkc8fivyjsn57na3z6")))

