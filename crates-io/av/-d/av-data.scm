(define-module (crates-io av -d av-data) #:use-module (crates-io))

(define-public crate-av-data-0.1.0 (c (n "av-data") (v "0.1.0") (d (list (d (n "byte-slice-cast") (r "^0.2.0") (d #t) (k 0)) (d (n "bytes") (r "^0.4.4") (d #t) (k 0)) (d (n "enum-primitive-derive") (r "^0.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "num-rational") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "13kr4r80ry0fdh1l33zmchhhaycd5jkac07a7m9jw4vwf9n8wcvf")))

(define-public crate-av-data-0.2.0 (c (n "av-data") (v "0.2.0") (d (list (d (n "byte-slice-cast") (r "^0.3.1") (d #t) (k 0)) (d (n "bytes") (r "^0.4.12") (d #t) (k 0)) (d (n "err-derive") (r "^0.1.6") (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-rational") (r "^0.2.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)))) (h "05jv9kq2gqsppl75irs6gfsizis3ngy0hcx661b7dizn9aydknbw")))

(define-public crate-av-data-0.2.1 (c (n "av-data") (v "0.2.1") (d (list (d (n "byte-slice-cast") (r "^0.3.1") (d #t) (k 0)) (d (n "bytes") (r "^0.5.2") (d #t) (k 0)) (d (n "err-derive") (r "^0.2.3") (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-rational") (r "^0.2.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)))) (h "1672322hssf60g8zmdg364w73pmp72i139jpn5jwqsaik009q5yp")))

(define-public crate-av-data-0.2.2 (c (n "av-data") (v "0.2.2") (d (list (d (n "byte-slice-cast") (r "^0.3.1") (d #t) (k 0)) (d (n "bytes") (r "^0.5.2") (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-rational") (r "^0.3.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1i8ijdbzj677z0a141wk6zjmfif2wb8096243yxyac4a1v9ijkhs")))

(define-public crate-av-data-0.3.0 (c (n "av-data") (v "0.3.0") (d (list (d (n "byte-slice-cast") (r "^1.0.0") (d #t) (k 0)) (d (n "bytes") (r "^1.0.0") (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-rational") (r "^0.3.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "05yi91y1m22kxjf10ph4bk6ylgk3x3ghdhh8i7wp4aqsc0vlsf16")))

(define-public crate-av-data-0.4.0 (c (n "av-data") (v "0.4.0") (d (list (d (n "byte-slice-cast") (r "^1.2.1") (d #t) (k 0)) (d (n "bytes") (r "^1.2.1") (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-rational") (r "^0.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "15sjvf13c7r566569k4sasalx4d87si0sj9l92b9n5c0vqz34wdd")))

(define-public crate-av-data-0.4.1 (c (n "av-data") (v "0.4.1") (d (list (d (n "byte-slice-cast") (r "^1.2.1") (d #t) (k 0)) (d (n "bytes") (r "^1.2.1") (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-rational") (r "^0.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "06zi5b6jiacd28iagjhwshgn58nwzqmc3l28wn523gafhj5zw3mf")))

(define-public crate-av-data-0.4.2 (c (n "av-data") (v "0.4.2") (d (list (d (n "byte-slice-cast") (r "^1.2.1") (d #t) (k 0)) (d (n "bytes") (r "^1.2.1") (d #t) (k 0)) (d (n "num-derive") (r "^0.4") (d #t) (k 0)) (d (n "num-rational") (r "^0.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0lk5pq6jfmfp5ihvnzqdqxympk5rk7648bcsvwhgj02xaairhnyp")))

