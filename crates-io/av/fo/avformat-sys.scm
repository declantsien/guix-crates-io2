(define-module (crates-io av fo avformat-sys) #:use-module (crates-io))

(define-public crate-avformat-sys-0.1.0 (c (n "avformat-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.49") (d #t) (k 1)) (d (n "metadeps") (r "^1.1.2") (d #t) (k 1)))) (h "0hrllayxmmf4a84hxz9a9mksw9jfim3zv91p5nhqgjnbaqh93hm6") (f (quote (("build_sources"))))))

(define-public crate-avformat-sys-0.1.1 (c (n "avformat-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.52") (d #t) (k 1)) (d (n "metadeps") (r "^1.1.2") (d #t) (k 1)))) (h "021znz2qaq569jy22mmfjk3mnk2gvdvq1hqx3ngqa0hlv3yq2wnk") (f (quote (("build_sources"))))))

