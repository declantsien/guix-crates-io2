(define-module (crates-io av fo avfoundation) #:use-module (crates-io))

(define-public crate-avfoundation-0.1.0 (c (n "avfoundation") (v "0.1.0") (h "1hmi21abwkr5qljriy83zc07kg8frmca4k1xallrx9clha4psxa7")))

(define-public crate-avfoundation-0.1.1 (c (n "avfoundation") (v "0.1.1") (h "05yxgqyqfjxbmwi1hlh9s0z59pyvh386v5w7ch2hirklmrf5py3g")))

(define-public crate-avfoundation-0.1.2 (c (n "avfoundation") (v "0.1.2") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "block") (r "^0.1.5") (d #t) (k 0)) (d (n "cocoa-foundation") (r "^0.1") (d #t) (k 0)) (d (n "foreign-types") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "objc") (r "^0.2.4") (f (quote ("objc_exception"))) (d #t) (k 0)))) (h "198zzdha255mh9d1m0kihhy0yy3cybfp43nlgnbncknwzracvqwa")))

(define-public crate-avfoundation-0.1.3 (c (n "avfoundation") (v "0.1.3") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "block") (r "^0.1.5") (d #t) (k 0)) (d (n "cocoa-foundation") (r "^0.1") (d #t) (k 0)) (d (n "foreign-types") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "objc") (r "^0.2.4") (f (quote ("objc_exception"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.2") (d #t) (k 0)))) (h "0xhgn993qafm91lg6dc55y8mccfw1gad6lgdpiwsi085v820lfyz")))

