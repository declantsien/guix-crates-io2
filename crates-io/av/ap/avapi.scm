(define-module (crates-io av ap avapi) #:use-module (crates-io))

(define-public crate-avapi-0.1.0 (c (n "avapi") (v "0.1.0") (d (list (d (n "config") (r "^0.9") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "18vpz791bm185zmnp5bgq8cdxbg0l7vi12mllkklkx9nz9ic6k04") (y #t)))

