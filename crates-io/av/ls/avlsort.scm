(define-module (crates-io av ls avlsort) #:use-module (crates-io))

(define-public crate-avlsort-0.1.0 (c (n "avlsort") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "1bg3mjfavjr2ay1z72kk5ncrdwq9xx8dmfq98vkfvnw03injjabp")))

(define-public crate-avlsort-0.1.1 (c (n "avlsort") (v "0.1.1") (d (list (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "1zaw9yjvji4h7jl0bwyjh3sflqhji2g6fij9dnl17958b5nmkf4d")))

(define-public crate-avlsort-0.1.2 (c (n "avlsort") (v "0.1.2") (d (list (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "0n2lzsxgw875zk6isppyl1z9hhfx5dhhsnlshwrcirbwx67d3s1d")))

