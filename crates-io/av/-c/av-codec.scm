(define-module (crates-io av -c av-codec) #:use-module (crates-io))

(define-public crate-av-codec-0.1.0 (c (n "av-codec") (v "0.1.0") (d (list (d (n "av-data") (r "^0.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "num-rational") (r "^0.2") (d #t) (k 0)))) (h "0fimjfqpbr9xk6mn0fmkjvdvj9d7q39pbj5jg0b6xbisnlk4q14p")))

(define-public crate-av-codec-0.2.0 (c (n "av-codec") (v "0.2.0") (d (list (d (n "av-data") (r "^0.2.0") (d #t) (k 0)) (d (n "err-derive") (r "^0.1.6") (d #t) (k 0)) (d (n "num-rational") (r "^0.2.2") (d #t) (k 0)))) (h "15kr0l0rzh7q4aqkz39mgb2hr6lis9dbqhaf7c42jw16l9x27vdx")))

(define-public crate-av-codec-0.2.1 (c (n "av-codec") (v "0.2.1") (d (list (d (n "av-data") (r "^0.2.1") (d #t) (k 0)) (d (n "err-derive") (r "^0.2.3") (d #t) (k 0)) (d (n "num-rational") (r "^0.2.2") (d #t) (k 0)))) (h "130zc553qzwrkg59pfc254yc3ich54r6mrlsivbcnajd7kl8xmvg")))

(define-public crate-av-codec-0.2.2 (c (n "av-codec") (v "0.2.2") (d (list (d (n "av-data") (r "^0.3.0") (d #t) (k 0)) (d (n "num-rational") (r "^0.3.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1q1iq9h3pacjfqx4581sgpshc9m5kwrr3pwzjh1lm5zbimdhdyca")))

(define-public crate-av-codec-0.3.0 (c (n "av-codec") (v "0.3.0") (d (list (d (n "av-data") (r "^0.4.0") (d #t) (k 0)) (d (n "num-rational") (r "^0.4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "13y78pdq5w971m4k7aa5a40v23kbmr8ryy1lgcjldrkcq02c7xqz")))

