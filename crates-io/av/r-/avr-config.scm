(define-module (crates-io av r- avr-config) #:use-module (crates-io))

(define-public crate-avr-config-1.0.0 (c (n "avr-config") (v "1.0.0") (d (list (d (n "const_env--value") (r "^0.1") (d #t) (k 0)))) (h "1yp11jp6rr518d6fgk8i92wbk0qs72p27vxp70d7sj05bl6y9cs7")))

(define-public crate-avr-config-2.0.0 (c (n "avr-config") (v "2.0.0") (d (list (d (n "const_env--value") (r "^0.1") (d #t) (k 0)))) (h "1r41jrrlk3fgbiy7jf5c9hlpha3p3snpx787zvijf4p862ar6s11") (f (quote (("default") ("cpu-frequency"))))))

(define-public crate-avr-config-2.0.1 (c (n "avr-config") (v "2.0.1") (d (list (d (n "const_env--value") (r "^0.1") (d #t) (k 0)))) (h "17xbz4khjkrqn9z149prx9w4v8mqc64233b585bz584yy5njgcmb") (f (quote (("default") ("cpu-frequency"))))))

