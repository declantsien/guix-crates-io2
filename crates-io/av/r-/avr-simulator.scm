(define-module (crates-io av r- avr-simulator) #:use-module (crates-io))

(define-public crate-avr-simulator-0.2.0 (c (n "avr-simulator") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "simavr-ffi") (r "^0.1") (d #t) (k 0)) (d (n "test-case") (r "^2.1") (d #t) (k 2)))) (h "1haz6h0kq0ky10vjj4l0b4i45xz68n0sw3n0kkhxi99y3hh19z4k")))

(define-public crate-avr-simulator-0.2.1 (c (n "avr-simulator") (v "0.2.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "simavr-ffi") (r "^0.1") (d #t) (k 0)) (d (n "test-case") (r "^2.1") (d #t) (k 2)))) (h "0jbdz7mycaapsdwdwaf1qly5r1fhgffvqf5yglcf31vzks2vsljr")))

(define-public crate-avr-simulator-0.2.2 (c (n "avr-simulator") (v "0.2.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "simavr-ffi") (r "^0.1") (d #t) (k 0)) (d (n "test-case") (r "^2.1") (d #t) (k 2)))) (h "0xqlfy0y76mv7zz3vjx6pqx2xwjbg02cj9crb0rrzz34lrq12c44")))

