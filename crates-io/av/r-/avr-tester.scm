(define-module (crates-io av r- avr-tester) #:use-module (crates-io))

(define-public crate-avr-tester-0.1.0 (c (n "avr-tester") (v "0.1.0") (d (list (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "simavr-ffi") (r "^0.1") (d #t) (k 0)) (d (n "test-case") (r "^2.1") (d #t) (k 2)))) (h "10z8x38nn7z1q0yila9m7z16cymrxfvgig93qlzmpd78drnn0r5l")))

(define-public crate-avr-tester-0.2.0 (c (n "avr-tester") (v "0.2.0") (d (list (d (n "avr-simulator") (r "^0.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1y7gn0wh40kvb3lcnj4zqywdf29nznsyjhj49nql20n45v2n9y03")))

(define-public crate-avr-tester-0.2.1 (c (n "avr-tester") (v "0.2.1") (d (list (d (n "avr-simulator") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0z10i91qmv9q2bq8q9g4qpvwvn8z0f6rj59dlqq20fjqc6fx19xj")))

(define-public crate-avr-tester-0.2.2 (c (n "avr-tester") (v "0.2.2") (d (list (d (n "avr-simulator") (r "^0.2.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0y8f38hyrk3qrmg0nnbark3kq3bs29ipi0zp92zvrc975f26ni8v")))

