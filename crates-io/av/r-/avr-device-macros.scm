(define-module (crates-io av r- avr-device-macros) #:use-module (crates-io))

(define-public crate-avr-device-macros-0.1.0 (c (n "avr-device-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.35") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "05rrqqb5ipsdvjbqchibk0hsiak7n4jm016qyyd6ramxz64335ac")))

(define-public crate-avr-device-macros-0.2.0 (c (n "avr-device-macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.35") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "18117d5x87rjcrgb7hji9m0j41bb6dj07dbl1f8xymfkd9r0xrvn")))

(define-public crate-avr-device-macros-0.2.2 (c (n "avr-device-macros") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.35") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0za0w2i73x2fzk7hbs1355a1wjwswa6nhs82lg90mf6ssy6b4wvi")))

(define-public crate-avr-device-macros-0.2.3 (c (n "avr-device-macros") (v "0.2.3") (d (list (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.35") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1g9ldvl4h73rwkwqq7x0jvi8rh3ih5z1lbfsbgfw5zsjyqnbza7d")))

(define-public crate-avr-device-macros-0.3.0 (c (n "avr-device-macros") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.35") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1y5llylvv2qydmlc71i7kl5g6zc5pc8wr9rm7mav5yr3mhlbbndx")))

(define-public crate-avr-device-macros-0.3.1 (c (n "avr-device-macros") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.35") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0rcxd8g9cvvawn2xrl8xjfg78yffmmi9v6vqnim8m086km8mlg70")))

(define-public crate-avr-device-macros-0.3.2 (c (n "avr-device-macros") (v "0.3.2") (d (list (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.35") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "12wzw3q60sxwh0hm9sx9jhpb3pg1g51isjdbd0xyg4kap1yrjqic")))

(define-public crate-avr-device-macros-0.3.3 (c (n "avr-device-macros") (v "0.3.3") (d (list (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.35") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1njyp64pqmr0r664ac190mbv5grhkj1yfp5760i5dqwyamqblgdb")))

(define-public crate-avr-device-macros-0.3.4 (c (n "avr-device-macros") (v "0.3.4") (d (list (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.35") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1n3h032acf286m6jmh7s8iyiz353jngkrmczms9ma1jqwxzdlka7")))

(define-public crate-avr-device-macros-0.4.0 (c (n "avr-device-macros") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.35") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "08gxyy9ld4h4qswycrxhy1fm58aznqkin99fwan9vrwd83ly9qfs")))

(define-public crate-avr-device-macros-0.5.0 (c (n "avr-device-macros") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.35") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0f1djpp8868m0fqzfrylsa8px3pidxv8ri380r3di0a972i2ii47")))

(define-public crate-avr-device-macros-0.5.1 (c (n "avr-device-macros") (v "0.5.1") (d (list (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.35") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1jr6w2xf5n9skxd25dqmkwpi7w3ib0h0s66m6b2cwj2i3aa4k2xs")))

(define-public crate-avr-device-macros-0.5.2 (c (n "avr-device-macros") (v "0.5.2") (d (list (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.35") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "13zw36ymwjgdllw45aiaav0zvn7pyflbhbcgdmc9wfysyw1b8yw1")))

(define-public crate-avr-device-macros-0.5.3 (c (n "avr-device-macros") (v "0.5.3") (d (list (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.35") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1zp8kadq9s6jcgig4ckcb93w1drb9adgrrcr20yppqy07h96yjhd")))

(define-public crate-avr-device-macros-0.5.4 (c (n "avr-device-macros") (v "0.5.4") (d (list (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.35") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0vsvwq0lsnnh8j8m9jpcvbw5ixllxnysq95qm7gn180yvi0qa43b")))

