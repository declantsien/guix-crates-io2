(define-module (crates-io av r- avr-test-suite) #:use-module (crates-io))

(define-public crate-avr-test-suite-0.1.0 (c (n "avr-test-suite") (v "0.1.0") (d (list (d (n "bitflags") (r "^0.9") (d #t) (k 0)) (d (n "lit") (r "^0.2.6") (d #t) (k 0)) (d (n "simavr-sys") (r "^1.5.1") (d #t) (k 0)) (d (n "tempfile") (r "^2.1") (d #t) (k 0)) (d (n "vsprintf") (r "^1.0") (d #t) (k 0)))) (h "0l0ml3gyjn534pp2x47npf1nyp76cd1jjqgqvwj84xhw28lpkl8c")))

(define-public crate-avr-test-suite-0.1.1 (c (n "avr-test-suite") (v "0.1.1") (d (list (d (n "bitflags") (r "^0.9") (d #t) (k 0)) (d (n "lit") (r "^0.2.6") (d #t) (k 0)) (d (n "simavr-sys") (r "^1.5.4") (d #t) (k 0)) (d (n "tempfile") (r "^2.1") (d #t) (k 0)) (d (n "vsprintf") (r "^1.0") (d #t) (k 0)))) (h "0gkbbw8p28gfxi4f2p1jmb317ki1s2yq026h4pww7sbrw5kqbl9k")))

