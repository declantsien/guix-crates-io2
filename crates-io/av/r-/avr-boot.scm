(define-module (crates-io av r- avr-boot) #:use-module (crates-io))

(define-public crate-avr-boot-0.1.0 (c (n "avr-boot") (v "0.1.0") (d (list (d (n "avr-mcu") (r "^0.3.5") (d #t) (k 1)) (d (n "const_env--value") (r "^0.1") (d #t) (k 0)))) (h "14lrzw7j902klvqjkrbhnawhli69r65727yfn44a5ppzd71cwv6i")))

(define-public crate-avr-boot-0.2.0 (c (n "avr-boot") (v "0.2.0") (d (list (d (n "avr-mcu") (r "^0.3.5") (d #t) (k 1)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "const_env--value") (r "^0.1") (d #t) (k 0)))) (h "1ws60pyxm7ixdvf3brmbrk9b9mfkdqc073igq8lwkkfl507w8kk8")))

(define-public crate-avr-boot-0.2.1 (c (n "avr-boot") (v "0.2.1") (d (list (d (n "avr-mcu") (r "^0.3.5") (d #t) (k 1)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "const_env--value") (r "^0.1") (d #t) (k 0)))) (h "16wnj2il7vg530dfk4a86z4b284bfyyjdpx7xr1v2k97r9xhhigw")))

(define-public crate-avr-boot-0.2.2 (c (n "avr-boot") (v "0.2.2") (d (list (d (n "avr-mcu") (r "^0.3.5") (d #t) (k 1)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "const_env--value") (r "^0.1") (d #t) (k 0)))) (h "0k6nn69amyvv3j7sz025cpx1gmzxjw2lq5ra8ndq5min3k3n08c6")))

(define-public crate-avr-boot-0.2.3 (c (n "avr-boot") (v "0.2.3") (d (list (d (n "avr-mcu") (r "^0.3.5") (d #t) (k 1)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "const_env--value") (r "^0.1") (d #t) (k 0)))) (h "0bx91m49wsymbpsxrmy615bvbh3ghbry2ca1k7n9dnn4a567xrhl")))

