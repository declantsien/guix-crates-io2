(define-module (crates-io av r- avr-std-stub) #:use-module (crates-io))

(define-public crate-avr-std-stub-1.0.0 (c (n "avr-std-stub") (v "1.0.0") (h "1wcn3p75kgwryr2w7ghccl0lsm4sipx2m3c9hp5g4vb2syx8mn9z")))

(define-public crate-avr-std-stub-1.0.1 (c (n "avr-std-stub") (v "1.0.1") (h "1gy9v203dp8lndh0i6mvg2pbd4wn12hsmfjp3v1d94j6xcpbch0d")))

(define-public crate-avr-std-stub-1.0.2 (c (n "avr-std-stub") (v "1.0.2") (h "0xhm9yiil8h2ijv9k871w27k36g5g99idyyi6s19j70j4f68w75q")))

(define-public crate-avr-std-stub-1.0.3 (c (n "avr-std-stub") (v "1.0.3") (h "09mmf3fag6lzsvzsgpzjyxn1qcvg2swgbarm7hplgzrpf4bj16c3")))

