(define-module (crates-io av r- avr-libc) #:use-module (crates-io))

(define-public crate-avr-libc-0.1.0 (c (n "avr-libc") (v "0.1.0") (d (list (d (n "avr-mcu") (r "^0.2") (d #t) (k 1)) (d (n "bindgen") (r "^0.31") (d #t) (k 1)))) (h "1xknm6h2xxqipjd23gs3salf7y60yqf8ngzjz6ryillda7j76sdq")))

(define-public crate-avr-libc-0.1.2 (c (n "avr-libc") (v "0.1.2") (d (list (d (n "avr-mcu") (r "^0.2") (d #t) (k 1)) (d (n "bindgen") (r "^0.31") (d #t) (k 1)) (d (n "lazy_static") (r "^0.2") (d #t) (k 1)))) (h "0my7c2m2b45rdz8y4wz4p2iqkk0xdys1j747m4lklq2bc7r3mqwl")))

(define-public crate-avr-libc-0.1.3 (c (n "avr-libc") (v "0.1.3") (d (list (d (n "avr-mcu") (r "^0.2") (d #t) (k 1)) (d (n "bindgen") (r "^0.31") (d #t) (k 1)) (d (n "lazy_static") (r "^0.2") (d #t) (k 1)))) (h "1q8cv67ybdbhb2imj7i0yb1s9m1f5171ryk6zn8f4g7pdfj645yx")))

(define-public crate-avr-libc-0.2.0 (c (n "avr-libc") (v "0.2.0") (d (list (d (n "avr-mcu") (r "^0.3") (d #t) (k 1)) (d (n "bindgen") (r "^0.54") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4") (d #t) (k 1)))) (h "06nzrprbf3aglaqfw8qlkakvjpkvrl825kwwjd48dz0iq59hsji3")))

(define-public crate-avr-libc-0.2.1 (c (n "avr-libc") (v "0.2.1") (d (list (d (n "avr-mcu") (r "^0.3") (d #t) (k 1)) (d (n "bindgen") (r "^0.55") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4") (d #t) (k 1)))) (h "04ry15h54jh3yfa5q9fb1570g4p8bxkll62syagbbkcjgg5y4zx1")))

(define-public crate-avr-libc-0.2.2 (c (n "avr-libc") (v "0.2.2") (d (list (d (n "avr-mcu") (r "^0.3") (d #t) (k 1)) (d (n "bindgen") (r "^0.57") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4") (d #t) (k 1)))) (h "000pxm6k62fr9fzflvmirypw194fvw5xabwfl1xxq0a7asplacha")))

