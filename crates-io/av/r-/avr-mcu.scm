(define-module (crates-io av r- avr-mcu) #:use-module (crates-io))

(define-public crate-avr-mcu-0.1.0 (c (n "avr-mcu") (v "0.1.0") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "xmltree") (r "^0.4") (d #t) (k 0)))) (h "1darkam2pmzdfrjd3vb61h5vpsrql8ykd466kjv5m0671kfvd7cs")))

(define-public crate-avr-mcu-0.2.0 (c (n "avr-mcu") (v "0.2.0") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "xmltree") (r "^0.4") (d #t) (k 0)))) (h "1qc7ryzxd1y9qw177c8rhpmsj53jxhxg10jgadlkvxvhgq65qp75")))

(define-public crate-avr-mcu-0.2.1 (c (n "avr-mcu") (v "0.2.1") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "xmltree") (r "^0.4") (d #t) (k 0)))) (h "0wpa1vpw24v9m6n32d28px95gxab251g4ziipw5h1wljc4zxjb0m")))

(define-public crate-avr-mcu-0.2.2 (c (n "avr-mcu") (v "0.2.2") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "xmltree") (r "^0.4") (d #t) (k 0)))) (h "08cn921aa7b96zjfflpssbpljpfhzizx70ki9w7h1yfrd59fdha2")))

(define-public crate-avr-mcu-0.2.3 (c (n "avr-mcu") (v "0.2.3") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "xmltree") (r "^0.4") (d #t) (k 0)))) (h "0nsqmkvb1l8sws6r4xw9qk9fk39rkqll01j8lnfy3bp6xqcs68sk")))

(define-public crate-avr-mcu-0.2.4 (c (n "avr-mcu") (v "0.2.4") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "xmltree") (r "^0.4") (d #t) (k 0)))) (h "044pw2yb2d6lc4q7pi32bscvmj5p8y25pi0lyxwj4jxfdlkgychz")))

(define-public crate-avr-mcu-0.2.5 (c (n "avr-mcu") (v "0.2.5") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "xmltree") (r "^0.4") (d #t) (k 0)))) (h "093d4yra3hmfwa5g1samnp99drkg8a31ysx56ik5rdk0bj587wcs")))

(define-public crate-avr-mcu-0.2.6 (c (n "avr-mcu") (v "0.2.6") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "xmltree") (r "^0.4") (d #t) (k 0)))) (h "1cxfl501fsshb9dvbxmghi7m319n23w70zvqrgizjdw4f6cacq6m")))

(define-public crate-avr-mcu-0.3.0 (c (n "avr-mcu") (v "0.3.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "xmltree") (r "^0.10") (d #t) (k 0)))) (h "0lk5hiyxnjlm8pbx0hvd8blsq44q9zg5p1wz3fq8xfswmbiqq6rv")))

(define-public crate-avr-mcu-0.3.1 (c (n "avr-mcu") (v "0.3.1") (d (list (d (n "json") (r "^0.12") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "target-cpu-fetch") (r "^0.1.0") (d #t) (k 0)) (d (n "xmltree") (r "^0.10") (d #t) (k 0)))) (h "017aq30abcvzhsysayc6akndqzc2vrj17faxggnqxgkcb9m09h89")))

(define-public crate-avr-mcu-0.3.2 (c (n "avr-mcu") (v "0.3.2") (d (list (d (n "json") (r "^0.12") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "target-cpu-fetch") (r "^0.1.0") (d #t) (k 0)) (d (n "xmltree") (r "^0.10") (d #t) (k 0)))) (h "0vip91fc6cbcvi0sldinq9l7hvzinnqldz906ligx87wr9kdimnh")))

(define-public crate-avr-mcu-0.3.3 (c (n "avr-mcu") (v "0.3.3") (d (list (d (n "json") (r "^0.12") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "target-cpu-fetch") (r "^0.1") (d #t) (k 0)) (d (n "xmltree") (r "^0.10") (d #t) (k 0)))) (h "05mgi6rn78yl5lc4dd0z5xi05hlznmg6anib9jh8yr4f8f6q0wjc")))

(define-public crate-avr-mcu-0.3.4 (c (n "avr-mcu") (v "0.3.4") (d (list (d (n "json") (r "^0.12") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "target-cpu-fetch") (r "^0.1") (d #t) (k 0)) (d (n "xmltree") (r "^0.10") (d #t) (k 0)))) (h "1iqj04gpfnvygmyfyi761zpibm4bq7zz31zyqci1r777qxjycl80")))

(define-public crate-avr-mcu-0.3.5 (c (n "avr-mcu") (v "0.3.5") (d (list (d (n "json") (r "^0.12") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "target-cpu-fetch") (r "^0.1") (d #t) (k 0)) (d (n "xmltree") (r "^0.10") (d #t) (k 0)))) (h "0yja6p50dpzaj4605bhwlvkin3is8bph48qyr9za0iymqzr6v6q2")))

