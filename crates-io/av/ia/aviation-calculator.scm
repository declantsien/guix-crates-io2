(define-module (crates-io av ia aviation-calculator) #:use-module (crates-io))

(define-public crate-aviation-calculator-0.1.0 (c (n "aviation-calculator") (v "0.1.0") (h "0mcfrnlr4n0hdi8ck0pysifw4vk5b669bh6k88002na636jy1mz9")))

(define-public crate-aviation-calculator-0.1.1 (c (n "aviation-calculator") (v "0.1.1") (d (list (d (n "enterpolation") (r "^0.2") (f (quote ("std"))) (k 0)))) (h "0bh0qivr2y3v3j5fnn2vlfischqmxg97p96yrq3ja9nx2qivdz1g")))

(define-public crate-aviation-calculator-0.2.0 (c (n "aviation-calculator") (v "0.2.0") (d (list (d (n "enterpolation") (r "^0.2") (f (quote ("std"))) (k 0)) (d (n "snafu") (r "^0.7.5") (d #t) (k 0)))) (h "1l5n1qfax3mn4j053yb8dr4xg56hkxjbfsgjpzs1zyrjdkvl5viz")))

(define-public crate-aviation-calculator-0.2.1 (c (n "aviation-calculator") (v "0.2.1") (d (list (d (n "enterpolation") (r "^0.2") (f (quote ("std"))) (k 0)) (d (n "snafu") (r "^0.7.5") (d #t) (k 0)))) (h "0ym99llnw9dysvnw6zd8fga9q6cx3qy3bjknqj06y7rrr8whifl5")))

(define-public crate-aviation-calculator-0.2.2 (c (n "aviation-calculator") (v "0.2.2") (d (list (d (n "enterpolation") (r "^0.2") (f (quote ("std"))) (k 0)) (d (n "snafu") (r "^0.7.5") (d #t) (k 0)))) (h "1xcf63v60kavsgjg6cqmzf5vgsy93ddm6cy4jx53kr19vqbq5fcx")))

