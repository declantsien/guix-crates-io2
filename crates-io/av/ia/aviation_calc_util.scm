(define-module (crates-io av ia aviation_calc_util) #:use-module (crates-io))

(define-public crate-aviation_calc_util-2.0.0 (c (n "aviation_calc_util") (v "2.0.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "grib") (r "^0.8.0") (d #t) (k 0)) (d (n "ureq") (r "^2.9.1") (d #t) (k 0)))) (h "067qn75hixc1fxv4kf8w59hv9dimh9a7hg7a4sqvxr2y640091xi")))

(define-public crate-aviation_calc_util-2.1.0 (c (n "aviation_calc_util") (v "2.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "grib") (r "^0.8.0") (d #t) (k 0)) (d (n "ureq") (r "^2.9.1") (d #t) (k 0)))) (h "0697daj63fhj54wygfn15ppmjhk28p8j1yway989fb22dllnr85i")))

(define-public crate-aviation_calc_util-2.1.1 (c (n "aviation_calc_util") (v "2.1.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "grib") (r "^0.8.0") (d #t) (k 0)) (d (n "ureq") (r "^2.9.1") (d #t) (k 0)))) (h "118xvaa12xv9c02aw24pd6fxgxy2pcpagc4cqh6n6j9ba3q1chm5")))

(define-public crate-aviation_calc_util-2.2.0 (c (n "aviation_calc_util") (v "2.2.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "grib") (r "^0.8.0") (d #t) (k 0)) (d (n "ureq") (r "^2.9.1") (d #t) (k 0)))) (h "09936lgdmkj1m7rvgzdmpqfiyvq580sd9a3nhr0i5ggbz90db845")))

(define-public crate-aviation_calc_util-2.2.1 (c (n "aviation_calc_util") (v "2.2.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "grib") (r "^0.8.0") (d #t) (k 0)) (d (n "ureq") (r "^2.9.1") (d #t) (k 0)))) (h "05zb12flqsqxwgv4lkvgyzsnbh0ml8jxd389sr238fagz5fryf9k")))

(define-public crate-aviation_calc_util-2.3.0 (c (n "aviation_calc_util") (v "2.3.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "grib") (r "^0.8.0") (d #t) (k 0)) (d (n "ureq") (r "^2.9.1") (d #t) (k 0)))) (h "1rfs0qr1vj9hgwwvrsnizpl6fnprqyndqc0wlsd6zn7h6bcl9sjp")))

(define-public crate-aviation_calc_util-2.3.2 (c (n "aviation_calc_util") (v "2.3.2") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "grib") (r "^0.8.0") (d #t) (k 0)) (d (n "ureq") (r "^2.9.1") (d #t) (k 0)))) (h "0hymfvc5drrcwqmzjpjnz7fmb7dhwzm7dxfj8yspapphxzl9kzgv")))

(define-public crate-aviation_calc_util-2.3.3 (c (n "aviation_calc_util") (v "2.3.3") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "grib") (r "^0.8.0") (d #t) (k 0)) (d (n "ureq") (r "^2.9.1") (d #t) (k 0)))) (h "1gc46wcxzkwlapps8cmx3d8dqwfqn4r3ql3kwrpyiyc5s0pyw4h0")))

(define-public crate-aviation_calc_util-2.4.0 (c (n "aviation_calc_util") (v "2.4.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "grib") (r "^0.8.0") (d #t) (k 0)) (d (n "ureq") (r "^2.9.1") (d #t) (k 0)))) (h "1wsjib8mrp7fd0whs823jg0wpabnjg8q7a83ywgp5jyr67jrc922")))

(define-public crate-aviation_calc_util-2.5.0 (c (n "aviation_calc_util") (v "2.5.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "grib") (r "^0.8.0") (d #t) (k 0)) (d (n "ureq") (r "^2.9.1") (d #t) (k 0)))) (h "03wiwqsrxjszbrqf5azgpmzqv9fy2ix9b1wb3jzzccmxf8271ydi")))

(define-public crate-aviation_calc_util-2.6.0 (c (n "aviation_calc_util") (v "2.6.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "grib") (r "^0.8.0") (d #t) (k 0)) (d (n "ureq") (r "^2.9.1") (d #t) (k 0)))) (h "1ab0w4kmi0ccynd1y80mpvvd93csmdn8yifvxr918gj4ab84n31b")))

(define-public crate-aviation_calc_util-2.7.0 (c (n "aviation_calc_util") (v "2.7.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "grib") (r "^0.8.0") (d #t) (k 0)) (d (n "ureq") (r "^2.9.1") (d #t) (k 0)))) (h "1cpf8511bzndknrkjbaz8m8vkfficwdc6rs8ydy6s92cxkna8rlm")))

(define-public crate-aviation_calc_util-2.7.1 (c (n "aviation_calc_util") (v "2.7.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "grib") (r "^0.8.0") (d #t) (k 0)) (d (n "ureq") (r "^2.9.1") (d #t) (k 0)))) (h "1vylp4ha0f8hf11zi0f52r09002i7l2vdn2rpl3njixcmk569x5r")))

(define-public crate-aviation_calc_util-2.7.2 (c (n "aviation_calc_util") (v "2.7.2") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "grib") (r "^0.8.0") (d #t) (k 0)) (d (n "ureq") (r "^2.9.1") (d #t) (k 0)))) (h "1mhgydk86iy6cg4i454ki9v2jch6q1qxc7nm6flm1cxbcq4dccwp")))

(define-public crate-aviation_calc_util-2.8.0 (c (n "aviation_calc_util") (v "2.8.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "grib") (r "^0.8.0") (d #t) (k 0)) (d (n "ureq") (r "^2.9.1") (d #t) (k 0)))) (h "0bjg0iysa84v32jsy1ihrbdxg59wvpkxplvfznmblffnk25w53is")))

(define-public crate-aviation_calc_util-2.8.1 (c (n "aviation_calc_util") (v "2.8.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "grib") (r "^0.8.0") (d #t) (k 0)) (d (n "ureq") (r "^2.9.1") (d #t) (k 0)))) (h "0wzm8r856awlg8pkljxqkflxwjhqv3p0b0i4v9nw7v0hgy51xirm")))

(define-public crate-aviation_calc_util-2.9.0 (c (n "aviation_calc_util") (v "2.9.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "grib") (r "^0.8.0") (d #t) (k 0)) (d (n "ureq") (r "^2.9.1") (d #t) (k 0)))) (h "1zfbhl26knkqms935nbgx5rnxy5733xb453sldxajfcgnh48156q")))

(define-public crate-aviation_calc_util-2.10.0 (c (n "aviation_calc_util") (v "2.10.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "grib") (r "^0.8.0") (d #t) (k 0)) (d (n "ureq") (r "^2.9.1") (d #t) (k 0)))) (h "01mgj41dpr08gxyya462ia3n1k9ygiqz50rgidi6f4h6x3psb9fk")))

(define-public crate-aviation_calc_util-2.11.0 (c (n "aviation_calc_util") (v "2.11.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "grib") (r "^0.8.0") (d #t) (k 0)) (d (n "ureq") (r "^2.9.1") (d #t) (k 0)))) (h "09bg0br31z6lddp9qpya0zqq11rjp7z20vvfmpm3maiy0fsng4ci")))

(define-public crate-aviation_calc_util-2.11.1 (c (n "aviation_calc_util") (v "2.11.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "grib") (r "^0.8.0") (d #t) (k 0)) (d (n "ureq") (r "^2.9.1") (d #t) (k 0)))) (h "1qkai5679is645gr1m2h5kwmb9nn056g98mx6c55zagdfc1fz8n5") (y #t)))

(define-public crate-aviation_calc_util-2.11.3 (c (n "aviation_calc_util") (v "2.11.3") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "grib") (r "^0.8.0") (d #t) (k 0)) (d (n "ureq") (r "^2.9.1") (d #t) (k 0)))) (h "1dyfipg967j875s0hq0w825spyipf1whin7j1vshnhpg7l2zq5wl") (y #t)))

(define-public crate-aviation_calc_util-2.11.5 (c (n "aviation_calc_util") (v "2.11.5") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "grib") (r "^0.8.0") (d #t) (k 0)) (d (n "ureq") (r "^2.9.1") (d #t) (k 0)))) (h "1k1wp8qzlsswa54026rdqmrc3mk6ihhi1xawpssyw2g7rvg8jmc6") (y #t)))

(define-public crate-aviation_calc_util-2.11.7 (c (n "aviation_calc_util") (v "2.11.7") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "grib") (r "^0.8.0") (d #t) (k 0)) (d (n "ureq") (r "^2.9.1") (d #t) (k 0)))) (h "0g0xq10x8pif4i2hnfk7vpnja944skcmzrin2lwlbm9v5g8qgsm1") (y #t)))

(define-public crate-aviation_calc_util-2.11.8 (c (n "aviation_calc_util") (v "2.11.8") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "grib") (r "^0.8.0") (d #t) (k 0)) (d (n "ureq") (r "^2.9.1") (d #t) (k 0)))) (h "0vlfy367b8sk0r12kbadn5qhghwc5jb2926ykg0hwjj5bghjaksw")))

(define-public crate-aviation_calc_util-2.12.0 (c (n "aviation_calc_util") (v "2.12.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "grib") (r "^0.8.0") (d #t) (k 0)) (d (n "ureq") (r "^2.9.1") (d #t) (k 0)))) (h "14zlwxkn40s8snfcpcxx9g623fsb6nv5rxci5mk2dz65w3qcn0z5")))

(define-public crate-aviation_calc_util-2.12.1 (c (n "aviation_calc_util") (v "2.12.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "grib") (r "^0.8.0") (d #t) (k 0)) (d (n "ureq") (r "^2.9.1") (d #t) (k 0)))) (h "0qr9cvc6vmwlq3p3sfzlm7iyvzggp6kr6cbdfhbdka420q95hjbp")))

