(define-module (crates-io av ia aviary) #:use-module (crates-io))

(define-public crate-aviary-0.1.0 (c (n "aviary") (v "0.1.0") (h "1p1qnzaiyqj95ns1y4wadkmvvrkd7hb5jc4l4r2b1xd3yvh39hn7")))

(define-public crate-aviary-0.1.1 (c (n "aviary") (v "0.1.1") (h "17rzx2g8x749cr7k8hjblcz6ppy3prg99mzj729p0jhr3ic80b12")))

