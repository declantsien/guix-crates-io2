(define-module (crates-io av ro avrox-storage) #:use-module (crates-io))

(define-public crate-avrox-storage-0.0.0 (c (n "avrox-storage") (v "0.0.0") (d (list (d (n "avr-oxide") (r "^0.3.0") (d #t) (k 0)))) (h "1ypisranc4c6idal38dsmpnl033lbp7dgfk2zwixnip062byiid1")))

(define-public crate-avrox-storage-0.3.1 (c (n "avrox-storage") (v "0.3.1") (d (list (d (n "avr-oxide") (r "^0.3.1") (d #t) (k 0)))) (h "03c1d43kn4v24axvsyv6min769344wbf7rnhvb5caqly9jldbk33") (f (quote (("std_test" "avr-oxide/std_test") ("std_build" "avr-oxide/std_build"))))))

(define-public crate-avrox-storage-0.4.0 (c (n "avrox-storage") (v "0.4.0") (d (list (d (n "avr-oxide") (r "^0.4.0") (d #t) (k 0)) (d (n "avr-oxide") (r "^0.4.0") (k 2)) (d (n "rand") (r "^0.8.5") (f (quote ("small_rng"))) (k 2)) (d (n "ufmt") (r "^0.1") (d #t) (k 0)))) (h "10iaff7ahpg9bg3i5srmd6zyhdd7mnf71szn3lv9pc2h3a5zl4l3") (f (quote (("std_test" "avr-oxide/std_test") ("std_build" "avr-oxide/std_build") ("filesize_64bit") ("filesize_32bit") ("filesize_16bit") ("default" "filesize_32bit"))))))

(define-public crate-avrox-storage-0.4.1 (c (n "avrox-storage") (v "0.4.1") (d (list (d (n "avr-oxide") (r "^0.4.1") (d #t) (k 0)) (d (n "avr-oxide") (r "^0.4.1") (k 2)) (d (n "rand") (r "^0.8.5") (f (quote ("small_rng"))) (k 2)) (d (n "ufmt") (r "^0.1") (d #t) (k 0)))) (h "1205d380dm2ha4qvygmxx0xg8fkq0fqqv6r2rqps2vf9s01hl9pr") (f (quote (("std_test" "avr-oxide/std_test") ("std_build" "avr-oxide/std_build") ("filesize_64bit") ("filesize_32bit") ("filesize_16bit") ("default" "filesize_32bit"))))))

