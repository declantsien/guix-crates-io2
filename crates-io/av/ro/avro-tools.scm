(define-module (crates-io av ro avro-tools) #:use-module (crates-io))

(define-public crate-avro-tools-0.1.0 (c (n "avro-tools") (v "0.1.0") (d (list (d (n "avro-rs") (r "^0.6") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1324c71vxya09q7cmb7s9z2rp2yx0bsa84fqcicjxamv326z9x91")))

(define-public crate-avro-tools-0.1.1 (c (n "avro-tools") (v "0.1.1") (d (list (d (n "avro-rs") (r "^0.6") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0k971qywqzhsdxx29ch6vvdbrkmaa31r3px0jhkm6wq7pcsrriq1")))

