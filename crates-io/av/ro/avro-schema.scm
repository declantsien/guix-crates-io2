(define-module (crates-io av ro avro-schema) #:use-module (crates-io))

(define-public crate-avro-schema-0.1.0 (c (n "avro-schema") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("std"))) (k 0)))) (h "06z8799p29f2byyzkqwaqavnfvyxd2fq1z8hqjjqm7d6zjs337f6")))

(define-public crate-avro-schema-0.2.0 (c (n "avro-schema") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("std"))) (k 0)))) (h "1399zf2bkz89a7jpxz807cz02gqrvfjcf03x3x93h63q4c7x4g95")))

(define-public crate-avro-schema-0.2.1 (c (n "avro-schema") (v "0.2.1") (d (list (d (n "serde") (r "^1.0") (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("std"))) (k 0)))) (h "1a6f5zv726759aydki13n9lnvdl4i17sc8vx0qzamak7j2fwg1ls")))

(define-public crate-avro-schema-0.2.2 (c (n "avro-schema") (v "0.2.2") (d (list (d (n "serde") (r "^1.0") (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("std"))) (k 0)))) (h "1j372n527cyw2k17p5js4fxqy9z0rs4q0ggllqhqbxiicxh0g1yk")))

(define-public crate-avro-schema-0.3.0 (c (n "avro-schema") (v "0.3.0") (d (list (d (n "async-stream") (r "^0.3.2") (o #t) (d #t) (k 0)) (d (n "crc") (r "^2") (o #t) (d #t) (k 0)) (d (n "fallible-streaming-iterator") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "libflate") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("std"))) (k 0)) (d (n "snap") (r "^1") (o #t) (d #t) (k 0)))) (h "1gbvciwvi2isa6qanbzi4lbqzzgvhdlzjyzlsa29dflsndaiha5m") (f (quote (("full" "compression" "async") ("default") ("compression" "libflate" "snap" "crc") ("async" "futures" "async-stream"))))))

