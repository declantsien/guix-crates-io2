(define-module (crates-io av ro avro) #:use-module (crates-io))

(define-public crate-avro-0.1.0 (c (n "avro") (v "0.1.0") (d (list (d (n "serde_json") (r "^0.6") (d #t) (k 0)))) (h "1q0vasgylirwsvyn3d4qp77qkw0px0yqi38jrd0kh4n7cqb7xs6v")))

(define-public crate-avro-0.2.0 (c (n "avro") (v "0.2.0") (d (list (d (n "regex") (r "^0.1.48") (d #t) (k 0)) (d (n "serde_json") (r "^0.6") (d #t) (k 0)))) (h "0cdb4ln77d3vwcsr295a8skn8f1qp9cpgikwqh7f39rnnkwkjjb0")))

(define-public crate-avro-0.2.1 (c (n "avro") (v "0.2.1") (d (list (d (n "regex") (r "^0.1.48") (d #t) (k 0)) (d (n "serde_json") (r "^0.6") (d #t) (k 0)))) (h "0x2xrq0zg23laizv78ikjcvcyh2cv27fzvfs3gcr6cd9vbjwbij8")))

