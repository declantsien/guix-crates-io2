(define-module (crates-io av ro avrox-display) #:use-module (crates-io))

(define-public crate-avrox-display-0.4.0 (c (n "avrox-display") (v "0.4.0") (d (list (d (n "avr-oxide") (r "^0.4.0") (d #t) (k 0)) (d (n "avrox-storage") (r "^0.4.0") (k 0)) (d (n "ufmt") (r "^0.1") (d #t) (k 0)))) (h "17m7wqcdk1p2f48mllhw2dasr6b17khi6r25b7l5x1rl5i362ki1") (f (quote (("std_test" "avr-oxide/std_test") ("std_build" "avr-oxide/std_build"))))))

(define-public crate-avrox-display-0.4.1 (c (n "avrox-display") (v "0.4.1") (d (list (d (n "avr-oxide") (r "^0.4.1") (d #t) (k 0)) (d (n "avrox-storage") (r "^0.4.1") (k 0)) (d (n "ufmt") (r "^0.1") (d #t) (k 0)))) (h "0mzykf02c2cbfmfmq9pldhqaaxab2zpg40xsf4hiw009bc29cy9d") (f (quote (("std_test" "avr-oxide/std_test") ("std_build" "avr-oxide/std_build"))))))

