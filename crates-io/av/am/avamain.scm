(define-module (crates-io av am avamain) #:use-module (crates-io))

(define-public crate-avamain-0.1.0 (c (n "avamain") (v "0.1.0") (d (list (d (n "eyre") (r "^0.6") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "seqset") (r "^0.1.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread" "sync" "fs" "io-util" "time"))) (k 0)) (d (n "tokio-stream") (r "^0.1.8") (f (quote ("io-util"))) (k 0)) (d (n "whois-rust") (r "^1.4.0") (f (quote ("tokio"))) (d #t) (k 0)))) (h "0rrd495f8855nid9d9ayz3qbvdl468sgdkv8cl7c66bmk2bmrfl2")))

