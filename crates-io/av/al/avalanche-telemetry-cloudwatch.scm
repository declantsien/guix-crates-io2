(define-module (crates-io av al avalanche-telemetry-cloudwatch) #:use-module (crates-io))

(define-public crate-avalanche-telemetry-cloudwatch-0.0.74 (c (n "avalanche-telemetry-cloudwatch") (v "0.0.74") (d (list (d (n "aws-manager") (r "^0.25.0") (f (quote ("cloudwatch" "ec2"))) (d #t) (k 0)) (d (n "aws-sdk-cloudwatch") (r "^0.25.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "clap") (r "^4.2.1") (f (quote ("cargo" "derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "prometheus-manager") (r "^0.0.25") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.16") (d #t) (k 0)) (d (n "tokio") (r "^1.27.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0wdzi9pkd3n2izh4q0skscznwgavs1z1dyprp3qqhm542hsgzwb0") (r "1.68")))

