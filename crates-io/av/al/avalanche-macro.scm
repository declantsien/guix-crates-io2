(define-module (crates-io av al avalanche-macro) #:use-module (crates-io))

(define-public crate-avalanche-macro-0.1.0 (c (n "avalanche-macro") (v "0.1.0") (d (list (d (n "proc-macro-error") (r "^1.0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.72") (f (quote ("full"))) (d #t) (k 0)))) (h "0dnhjjflyk905wvdzgxqv973svzr3pw5xhm9dnjwxw96dcnd7qnq") (r "1.56")))

