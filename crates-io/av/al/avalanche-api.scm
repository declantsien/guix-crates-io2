(define-module (crates-io av al avalanche-api) #:use-module (crates-io))

(define-public crate-avalanche-api-0.1.0 (c (n "avalanche-api") (v "0.1.0") (h "083xj1mggmawbv37riv46bci5ghdpal9rrhjydb0xziqx0hyrd0p") (y #t)))

(define-public crate-avalanche-api-0.0.1 (c (n "avalanche-api") (v "0.0.1") (h "0jvilnwdj8d365m968f4gs1ki6fk3w3gm4m4s5cggfvzkr6cmx8c") (y #t)))

