(define-module (crates-io av al avalanchego) #:use-module (crates-io))

(define-public crate-avalanchego-0.0.0 (c (n "avalanchego") (v "0.0.0") (d (list (d (n "avalanche-types") (r "^0.0.23") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 2)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.3") (d #t) (k 2)) (d (n "random-manager") (r "^0.0.1") (d #t) (k 2)) (d (n "rust-embed") (r "^6.4.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.139") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)))) (h "1bykpj4b88na0sd8x2mpam4xx7i9hp99n3c8134mijmxihl3hs6l") (r "1.62")))

