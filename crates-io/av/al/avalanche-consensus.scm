(define-module (crates-io av al avalanche-consensus) #:use-module (crates-io))

(define-public crate-avalanche-consensus-0.0.0 (c (n "avalanche-consensus") (v "0.0.0") (h "15lgf89vzl7d037wqm3n67vw8zxfwhzs7smrcf7mr8iy1gg86pgg") (y #t)))

(define-public crate-avalanche-consensus-0.1.1 (c (n "avalanche-consensus") (v "0.1.1") (d (list (d (n "avalanche-types") (r "^0.1.1") (d #t) (k 0)) (d (n "bytes") (r "^1.4.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "serde") (r "^1.0.186") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.47") (d #t) (k 0)))) (h "0qqflch5hsqd342ll4jm6rhw3rp2y885i39scdhx10xd99jr78d8") (r "1.70")))

