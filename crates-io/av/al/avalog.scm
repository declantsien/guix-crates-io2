(define-module (crates-io av al avalog) #:use-module (crates-io))

(define-public crate-avalog-0.1.0 (c (n "avalog") (v "0.1.0") (d (list (d (n "monotonic_solver") (r "^0.2.1") (d #t) (k 0)) (d (n "piston_meta") (r "^1.0.0") (d #t) (k 0)))) (h "080gdqw7cdxvfwjs2s0mfq3jrhfcn7fvkj2izgfa65844cbyrjjm")))

(define-public crate-avalog-0.1.1 (c (n "avalog") (v "0.1.1") (d (list (d (n "monotonic_solver") (r "^0.2.1") (d #t) (k 0)) (d (n "piston_meta") (r "^1.0.0") (d #t) (k 0)))) (h "116m01h2546y0191fy4x9y4z8f3hbp3m2zj4srx5had7nw24p4dd")))

(define-public crate-avalog-0.1.2 (c (n "avalog") (v "0.1.2") (d (list (d (n "monotonic_solver") (r "^0.2.1") (d #t) (k 0)) (d (n "piston_meta") (r "^1.0.0") (d #t) (k 0)))) (h "0c4ll3r0vwcfjqqi461fij272w54ar5h7sh1bs3l6qxl8nkm927n")))

(define-public crate-avalog-0.2.0 (c (n "avalog") (v "0.2.0") (d (list (d (n "monotonic_solver") (r "^0.3.0") (d #t) (k 0)) (d (n "piston_meta") (r "^1.0.0") (d #t) (k 0)))) (h "12wfm6f21b9s3llyzzdj3ym1n4wynry39zxvg2rda583khizzax6")))

(define-public crate-avalog-0.2.1 (c (n "avalog") (v "0.2.1") (d (list (d (n "monotonic_solver") (r "^0.3.0") (d #t) (k 0)) (d (n "piston_meta") (r "^1.0.0") (d #t) (k 0)))) (h "0xgws32smbj5v8v1az4nr8p6r43n5vrrsp3rqaqrm8sz715dvmsc")))

(define-public crate-avalog-0.3.0 (c (n "avalog") (v "0.3.0") (d (list (d (n "monotonic_solver") (r "^0.3.0") (d #t) (k 0)) (d (n "piston_meta") (r "^1.0.0") (d #t) (k 0)))) (h "05l6l9zzy7yf2bw7xqjb8sra36gcmw6xk5nkz4sl5hhmdh2gh4hw")))

(define-public crate-avalog-0.3.1 (c (n "avalog") (v "0.3.1") (d (list (d (n "monotonic_solver") (r "^0.3.0") (d #t) (k 0)) (d (n "piston_meta") (r "^1.0.0") (d #t) (k 0)))) (h "1f7fq6k3zz0629z9skwmfs9p5xffii71d5ng816b1gy6083y29dp")))

(define-public crate-avalog-0.3.2 (c (n "avalog") (v "0.3.2") (d (list (d (n "monotonic_solver") (r "^0.3.0") (d #t) (k 0)) (d (n "piston_meta") (r "^1.0.0") (d #t) (k 0)))) (h "0l33r0xmf5li9h9mkn9nkb71idk4b20whd85b2gzcpmcim1zhcvi")))

(define-public crate-avalog-0.3.3 (c (n "avalog") (v "0.3.3") (d (list (d (n "monotonic_solver") (r "^0.3.0") (d #t) (k 0)) (d (n "piston_meta") (r "^1.0.0") (d #t) (k 0)))) (h "1csj3cfh8hakvc9lf8dfhqgb8rlgip2f7lfm8j4hbargww3pn1li")))

(define-public crate-avalog-0.3.4 (c (n "avalog") (v "0.3.4") (d (list (d (n "monotonic_solver") (r "^0.3.0") (d #t) (k 0)) (d (n "piston_meta") (r "^1.0.0") (d #t) (k 0)))) (h "0g8gxa58s1m32xvryy3rsc3zbp78ykjfg6bhwwy1cx49mlrzx3bk")))

(define-public crate-avalog-0.4.0 (c (n "avalog") (v "0.4.0") (d (list (d (n "monotonic_solver") (r "^0.4.0") (d #t) (k 0)) (d (n "piston_meta") (r "^1.0.0") (d #t) (k 0)))) (h "0332w8b28xkjad1kcngi4qdvljldxzphzv6q8h4jgc1vc7kdrxqh")))

(define-public crate-avalog-0.4.1 (c (n "avalog") (v "0.4.1") (d (list (d (n "monotonic_solver") (r "^0.4.0") (d #t) (k 0)) (d (n "piston_meta") (r "^1.0.0") (d #t) (k 0)) (d (n "read_token") (r "^1.0.0") (d #t) (k 2)))) (h "0fv8pamh11jqyn8gk9xpa7q4ahscs20ad0ld56nd5xg39yrx3shp")))

(define-public crate-avalog-0.4.2 (c (n "avalog") (v "0.4.2") (d (list (d (n "monotonic_solver") (r "^0.4.0") (d #t) (k 0)) (d (n "piston_meta") (r "^1.0.0") (d #t) (k 0)) (d (n "read_token") (r "^1.0.0") (d #t) (k 2)))) (h "014y7c1ddxx5vi4zjjr9zs0m783rlx8k70az3305p5k355l0qbwn")))

(define-public crate-avalog-0.4.3 (c (n "avalog") (v "0.4.3") (d (list (d (n "monotonic_solver") (r "^0.5.0") (d #t) (k 0)) (d (n "piston_meta") (r "^1.0.0") (d #t) (k 0)) (d (n "read_token") (r "^1.0.0") (d #t) (k 2)))) (h "0968ylsyly979mxx8vvmaapvj830d262xpijfshldldxw49ss653")))

(define-public crate-avalog-0.4.4 (c (n "avalog") (v "0.4.4") (d (list (d (n "monotonic_solver") (r "^0.5.0") (d #t) (k 0)) (d (n "piston_meta") (r "^2.0.1") (d #t) (k 0)) (d (n "read_token") (r "^1.0.0") (d #t) (k 2)))) (h "1vqjsi3p5hll2ydg5r0gqk9ysm0qj9v16lg1rynhfvjysdkwa9pd")))

(define-public crate-avalog-0.4.5 (c (n "avalog") (v "0.4.5") (d (list (d (n "monotonic_solver") (r "^0.5.0") (d #t) (k 0)) (d (n "piston_meta") (r "^2.0.1") (d #t) (k 0)) (d (n "read_token") (r "^1.0.0") (d #t) (k 2)))) (h "0ny9pr5ynxz5nfx5n5ss30ix91jq0f5966i6xiaj8w7rp63wy3p8")))

(define-public crate-avalog-0.5.0 (c (n "avalog") (v "0.5.0") (d (list (d (n "monotonic_solver") (r "^0.5.0") (d #t) (k 0)) (d (n "piston_meta") (r "^2.0.1") (d #t) (k 0)) (d (n "read_token") (r "^1.0.0") (d #t) (k 2)))) (h "02jpbg5v10y1wg1n3xp6dm8va486q3kpiiw19y1ck82h3900h0q7")))

(define-public crate-avalog-0.5.1 (c (n "avalog") (v "0.5.1") (d (list (d (n "monotonic_solver") (r "^0.5.0") (d #t) (k 0)) (d (n "piston_meta") (r "^2.0.1") (d #t) (k 0)) (d (n "read_token") (r "^1.0.0") (d #t) (k 2)))) (h "0djzbckngy27m301lb2cd4k82r388rnx40laznf999d5kh08ld72")))

(define-public crate-avalog-0.5.2 (c (n "avalog") (v "0.5.2") (d (list (d (n "monotonic_solver") (r "^0.5.0") (d #t) (k 0)) (d (n "piston_meta") (r "^2.0.1") (d #t) (k 0)) (d (n "read_token") (r "^1.0.0") (d #t) (k 2)))) (h "1rnwzd1yq068ywmhd34l6wplnnw541pxx3ckha0pmxih9h5gc148")))

(define-public crate-avalog-0.6.0 (c (n "avalog") (v "0.6.0") (d (list (d (n "monotonic_solver") (r "^0.5.0") (d #t) (k 0)) (d (n "piston_meta") (r "^2.0.1") (d #t) (k 0)) (d (n "read_token") (r "^1.0.0") (d #t) (k 2)))) (h "1sywwlajilx5masmxz8war964r8as17sszk59rkihvncgd2r82qz")))

(define-public crate-avalog-0.7.0 (c (n "avalog") (v "0.7.0") (d (list (d (n "monotonic_solver") (r "^0.5.0") (d #t) (k 0)) (d (n "piston_meta") (r "^2.0.1") (d #t) (k 0)) (d (n "read_token") (r "^1.0.0") (d #t) (k 2)))) (h "19p67mc2k82dlrz58j8340sn38jf5qhw07liscqdi7r3c22ry7kg")))

(define-public crate-avalog-0.7.1 (c (n "avalog") (v "0.7.1") (d (list (d (n "monotonic_solver") (r "^0.5.0") (d #t) (k 0)) (d (n "piston_meta") (r "^2.0.1") (d #t) (k 0)) (d (n "read_token") (r "^1.0.0") (d #t) (k 2)))) (h "0ywjq1llwlk7fasmhr17glmdwyhz39bqv6zjbbjgbmgf208zziw1")))

(define-public crate-avalog-0.7.2 (c (n "avalog") (v "0.7.2") (d (list (d (n "monotonic_solver") (r "^0.5.0") (d #t) (k 0)) (d (n "piston_meta") (r "^2.0.1") (d #t) (k 0)) (d (n "read_token") (r "^1.0.0") (d #t) (k 2)))) (h "1405vdkacrhrwc6lyldsgkqjqhb90f6gihky9cbv6mqr1gk96c9s")))

