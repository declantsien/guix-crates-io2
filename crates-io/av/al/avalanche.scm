(define-module (crates-io av al avalanche) #:use-module (crates-io))

(define-public crate-avalanche-0.0.0 (c (n "avalanche") (v "0.0.0") (h "0p9hvf1kmn5zyy039l8j9klqjn7nkxy5j0mi1pscv9z0jxl5apzl")))

(define-public crate-avalanche-0.1.0 (c (n "avalanche") (v "0.1.0") (d (list (d (n "avalanche-macro") (r "^0.1.0") (d #t) (k 0)) (d (n "avalanche-web") (r "^0.1.0") (d #t) (k 2)) (d (n "downcast-rs") (r "^1.1") (d #t) (k 0)))) (h "0gxihqaqfn2rggnwv8xgwblrir3qzi83gx8fv5f2pbhm9cwggmfj") (r "1.56")))

