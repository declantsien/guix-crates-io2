(define-module (crates-io av m1 avm1-tree) #:use-module (crates-io))

(define-public crate-avm1-tree-0.0.14 (c (n "avm1-tree") (v "0.0.14") (d (list (d (n "hex") (r "^0.3.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.43") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.43") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.16") (d #t) (k 0)))) (h "121g41iiqd7hnfy4qw48g8sm4dmkr4flkrhwswk3xgcikh6smv7s")))

(define-public crate-avm1-tree-0.0.15 (c (n "avm1-tree") (v "0.0.15") (d (list (d (n "hex") (r "^0.3.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.32") (d #t) (k 0)))) (h "0lipq7rlvsfkfb99wi1hay9hyybb8pkdr1lksgq9az0yipd4w323")))

(define-public crate-avm1-tree-0.2.0 (c (n "avm1-tree") (v "0.2.0") (d (list (d (n "hex") (r "^0.3.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.89") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.89") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)) (d (n "test-generator") (r "^0.2.2") (d #t) (k 2)))) (h "0imh10m1nzp1lv1l4b7y331s230226pda8qpqasjy9x2gha4j7d8")))

(define-public crate-avm1-tree-0.2.1 (c (n "avm1-tree") (v "0.2.1") (d (list (d (n "hex") (r "^0.3.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.89") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.89") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)) (d (n "test-generator") (r "^0.2.2") (d #t) (k 2)))) (h "1q2g4dr05nr3cg49w06la7rrr77qwis0fiv9xlwfs1s96illnjx6")))

(define-public crate-avm1-tree-0.3.0 (c (n "avm1-tree") (v "0.3.0") (d (list (d (n "hex") (r "^0.3.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.89") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.89") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)) (d (n "test-generator") (r "^0.2.2") (d #t) (k 2)))) (h "18wzmvbx97j8mxs499fbq035l2b2xr5b7195417lib3gp4h5q959")))

(define-public crate-avm1-tree-0.4.0 (c (n "avm1-tree") (v "0.4.0") (d (list (d (n "hex") (r "^0.3.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.91") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.91") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)) (d (n "serde_json_v8") (r "^0.0.1") (d #t) (k 2)) (d (n "test-generator") (r "^0.2.2") (d #t) (k 2)))) (h "0shgdac4wxkdxxqfqw2zl7y4cym4xmxq1kzmgvp96g4brgwcfvi6")))

(define-public crate-avm1-tree-0.5.0 (c (n "avm1-tree") (v "0.5.0") (d (list (d (n "hex") (r "^0.3.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.91") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.91") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)) (d (n "serde_json_v8") (r "^0.0.1") (d #t) (k 2)) (d (n "test-generator") (r "^0.2.2") (d #t) (k 2)))) (h "07n3ws77n4zlynb9n0xa6qpqmkwlzj0dcmdahn6x3aavjy9y21br")))

(define-public crate-avm1-tree-0.6.0 (c (n "avm1-tree") (v "0.6.0") (d (list (d (n "hex") (r "^0.3.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.91") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.91") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)) (d (n "serde_json_v8") (r "^0.0.1") (d #t) (k 2)) (d (n "test-generator") (r "^0.2.2") (d #t) (k 2)))) (h "11cv0qx3jw4ji3670jh5z7zhg8yj76w95n1h7vkwpbpkfncxrvab")))

(define-public crate-avm1-tree-0.7.0 (c (n "avm1-tree") (v "0.7.0") (d (list (d (n "hex") (r "^0.3.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.91") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.91") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)) (d (n "serde_json_v8") (r "^0.0.1") (d #t) (k 2)) (d (n "test-generator") (r "^0.2.2") (d #t) (k 2)))) (h "1by0hla4wi2249nqa0ya1bs9rifz69w1q6gzfxhc3y5rdn3fci5d")))

(define-public crate-avm1-tree-0.8.0 (c (n "avm1-tree") (v "0.8.0") (d (list (d (n "hex") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.101") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)) (d (n "serde_json_v8") (r "^0.0.1") (d #t) (k 2)) (d (n "test-generator") (r "^0.3.0") (d #t) (k 2)))) (h "0fih65s1flgqg15qk9dyg09p56k3pgnqvvzyrc5ln58dpkgmhrqd")))

