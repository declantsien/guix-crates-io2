(define-module (crates-io av m1 avm1-parser) #:use-module (crates-io))

(define-public crate-avm1-parser-0.0.1 (c (n "avm1-parser") (v "0.0.1") (d (list (d (n "avm1-tree") (r "^0.0.14") (d #t) (k 0)) (d (n "hex") (r "^0.3.2") (d #t) (k 0)) (d (n "libflate") (r "^0.1.18") (d #t) (k 0)) (d (n "nom") (r "^4.1.1") (d #t) (k 0)) (d (n "rust-lzma") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.43") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.32") (d #t) (k 0)))) (h "1k0q3hc5dkl9vghj66lfd0v4jdzigdfsx8f96nw7gaca4m7p2758")))

(define-public crate-avm1-parser-0.0.2 (c (n "avm1-parser") (v "0.0.2") (d (list (d (n "avm1-tree") (r "^0.0.15") (d #t) (k 0)) (d (n "hex") (r "^0.3.2") (d #t) (k 0)) (d (n "libflate") (r "^0.1.18") (d #t) (k 0)) (d (n "nom") (r "^4.1.1") (d #t) (k 0)) (d (n "rust-lzma") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.43") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.32") (d #t) (k 0)))) (h "0y6dp5rqb4i4xrlfzdsmkjc8iax24yhirx7xm32mgc2ywmdpwz67")))

(define-public crate-avm1-parser-0.2.0 (c (n "avm1-parser") (v "0.2.0") (d (list (d (n "avm1-tree") (r "^0.2.1") (d #t) (k 0)) (d (n "nom") (r "^4.2.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 2)) (d (n "test-generator") (r "^0.2.2") (d #t) (k 2)))) (h "0q5jlwhx18mnl59ay3srj6jifs3rz3znf1mspmc3vk5y0lcddb2k")))

(define-public crate-avm1-parser-0.4.0 (c (n "avm1-parser") (v "0.4.0") (d (list (d (n "avm1-tree") (r "^0.4.0") (d #t) (k 0)) (d (n "nom") (r "^4.2.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 2)) (d (n "test-generator") (r "^0.2.2") (d #t) (k 2)))) (h "111i9fkbgc5j1dn51gx12is2vv0614h8jr1gpcr05njxmhmfb3x8")))

(define-public crate-avm1-parser-0.5.0 (c (n "avm1-parser") (v "0.5.0") (d (list (d (n "avm1-tree") (r "^0.5.0") (d #t) (k 0)) (d (n "nom") (r "^4.2.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 2)) (d (n "test-generator") (r "^0.2.2") (d #t) (k 2)))) (h "0hdllc7k51blnmxbrsd0i5wfr5cm0hdhgd5qgz888laqqz4brawj")))

(define-public crate-avm1-parser-0.7.0 (c (n "avm1-parser") (v "0.7.0") (d (list (d (n "avm1-tree") (r "^0.7.0") (d #t) (k 0)) (d (n "nom") (r "^4.2.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 2)) (d (n "test-generator") (r "^0.2.2") (d #t) (k 2)))) (h "1sgi8qafn3zn7858bpjq5smhpr4glnypw24xqvk8hznirfjh7xca")))

(define-public crate-avm1-parser-0.8.0 (c (n "avm1-parser") (v "0.8.0") (d (list (d (n "avm1-tree") (r "^0.8.0") (d #t) (k 0)) (d (n "nom") (r "^4.2.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 2)) (d (n "test-generator") (r "^0.2.2") (d #t) (k 2)))) (h "0y9m59fmarbgl3y9bvb6kvb6cmz0avyvbhlfhwms8vawqr61lkdi")))

(define-public crate-avm1-parser-0.9.0 (c (n "avm1-parser") (v "0.9.0") (d (list (d (n "avm1-types") (r "^0.9.0") (d #t) (k 0)) (d (n "nom") (r "^4.2.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 2)) (d (n "test-generator") (r "^0.2.2") (d #t) (k 2)))) (h "0n54l18pmf2sf2pnrd58lg04b02l67m0f9h6fhbsn87h259kqfhb")))

(define-public crate-avm1-parser-0.10.0 (c (n "avm1-parser") (v "0.10.0") (d (list (d (n "avm1-types") (r "^0.10.0") (k 0)) (d (n "avm1-types") (r "^0.10.0") (d #t) (k 2)) (d (n "nom") (r "^5.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (d #t) (k 2)) (d (n "serde_json_v8") (r "^0.0.1") (d #t) (k 2)) (d (n "test-generator") (r "^0.3.0") (d #t) (k 2)) (d (n "vec1") (r "^1.4.0") (d #t) (k 0)))) (h "0g4kfwlvf905hqrxd3h8zxs14zmljz1yb8x7fxazcbd8dyyp63dh")))

(define-public crate-avm1-parser-0.11.0 (c (n "avm1-parser") (v "0.11.0") (d (list (d (n "avm1-types") (r "^0.11.0") (k 0)) (d (n "avm1-types") (r "^0.11.0") (d #t) (k 2)) (d (n "nom") (r "^5.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.115") (d #t) (k 2)) (d (n "serde_json_v8") (r "^0.0.1") (d #t) (k 2)) (d (n "test-generator") (r "^0.3.0") (d #t) (k 2)) (d (n "vec1") (r "^1.6.0") (d #t) (k 0)))) (h "1q1v5bvk9dp66xg6pb90s71y5h3y2v3wc2f0ajpclvxw0ry0135w")))

(define-public crate-avm1-parser-0.12.0 (c (n "avm1-parser") (v "0.12.0") (d (list (d (n "avm1-types") (r "^0.12.0") (k 0)) (d (n "avm1-types") (r "^0.12.0") (d #t) (k 2)) (d (n "nom") (r "^5.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.115") (d #t) (k 2)) (d (n "serde_json_v8") (r "^0.1.1") (d #t) (k 2)) (d (n "test-generator") (r "^0.3.0") (d #t) (k 2)) (d (n "vec1") (r "^1.6.0") (d #t) (k 0)))) (h "0r4gzmbhl12vkfqva2hgprxi6k1cj225mra3mv7mw7kqj0118j83")))

(define-public crate-avm1-parser-0.13.0 (c (n "avm1-parser") (v "0.13.0") (d (list (d (n "avm1-types") (r "^0.13.0") (k 0)) (d (n "avm1-types") (r "^0.13.0") (d #t) (k 2)) (d (n "nom") (r "^6.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 2)) (d (n "serde_json_v8") (r "^0.1.1") (d #t) (k 2)) (d (n "test-generator") (r "^0.3.0") (d #t) (k 2)) (d (n "vec1") (r "^1.8.0") (d #t) (k 0)))) (h "13kgrg5lxknl2v3w0hs3dl4qfn5r75nhz0m0932q4jlnsrpilrrp")))

(define-public crate-avm1-parser-0.14.0 (c (n "avm1-parser") (v "0.14.0") (d (list (d (n "avm1-types") (r "^0.14.0") (k 0)) (d (n "avm1-types") (r "^0.14.0") (d #t) (k 2)) (d (n "nom") (r "^7.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (d #t) (k 2)) (d (n "serde_json_v8") (r "^0.1.1") (d #t) (k 2)) (d (n "test-generator") (r "^0.3.0") (d #t) (k 2)) (d (n "vec1") (r "^1.8.0") (d #t) (k 0)))) (h "1v3ij08d2nxg6l00i0bmbvhwnvgrk058ksahp0vw9jzsrhvnnpjc") (r "1.60.0")))

