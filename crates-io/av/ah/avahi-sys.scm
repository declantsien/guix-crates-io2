(define-module (crates-io av ah avahi-sys) #:use-module (crates-io))

(define-public crate-avahi-sys-0.10.0 (c (n "avahi-sys") (v "0.10.0") (d (list (d (n "bindgen") (r "^0.55.1") (d #t) (k 1)) (d (n "libc") (r "^0.2.77") (d #t) (k 0)))) (h "1krrpm5nsj4hk21b13lr8gnnj72r20sqppd9fghqncrkwmk2hycz")))

(define-public crate-avahi-sys-0.10.1 (c (n "avahi-sys") (v "0.10.1") (d (list (d (n "bindgen") (r "^0.69.2") (d #t) (k 1)) (d (n "libc") (r "^0.2.77") (d #t) (k 0)))) (h "0x42ggvh7pg1da3ilcrka103b977qjrf3akd6bykay48nf1hrq3h")))

