(define-module (crates-io av -b av-bitstream) #:use-module (crates-io))

(define-public crate-av-bitstream-0.1.0 (c (n "av-bitstream") (v "0.1.0") (d (list (d (n "assert_matches") (r "^1.1") (d #t) (k 2)) (d (n "failure") (r "^0.1") (d #t) (k 0)))) (h "15v8rz28asvmwfkb86zvh7sba4njjylq694rw6kp8scfmdr2wh0b")))

(define-public crate-av-bitstream-0.1.1 (c (n "av-bitstream") (v "0.1.1") (d (list (d (n "assert_matches") (r "^1.3.0") (d #t) (k 2)) (d (n "err-derive") (r "^0.2.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)))) (h "1m91inp84n3qp8dn8yakgi7i9xpr45w1yn1rzkpm0d78b3dcx2kf")))

(define-public crate-av-bitstream-0.1.2 (c (n "av-bitstream") (v "0.1.2") (d (list (d (n "assert_matches") (r "^1.3.0") (d #t) (k 2)) (d (n "err-derive") (r "^0.2.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)) (d (n "paste") (r "^0.1.18") (d #t) (k 2)))) (h "03z3a4x7dpbrwq9hdmw61vi8053b3d5gc6wi3l81c6zp2fsnxmib")))

(define-public crate-av-bitstream-0.2.0 (c (n "av-bitstream") (v "0.2.0") (d (list (d (n "assert_matches") (r "^1.3.0") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)) (d (n "paste") (r "^1.0.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0v4rsmixkyq7g6p5sk5wrryq9vsv972dvg5wzn45c7mjim50zz7d")))

