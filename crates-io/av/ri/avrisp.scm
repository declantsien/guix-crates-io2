(define-module (crates-io av ri avrisp) #:use-module (crates-io))

(define-public crate-avrisp-0.1.0 (c (n "avrisp") (v "0.1.0") (h "1l92pfz0vafmpy44pp3bcas6gsaq2hnjhny2gi9bgwmrzm4x9f11")))

(define-public crate-avrisp-0.2.0 (c (n "avrisp") (v "0.2.0") (h "1r8qy7wlbsmfp3ycfm3a0p7pis9w1cy00p9g84cihbcy05s8xqwz")))

(define-public crate-avrisp-0.2.1 (c (n "avrisp") (v "0.2.1") (h "0q82g7csjc5sk8izyra2mkg9562g93a1cj70c118fri02cpkjc8b")))

(define-public crate-avrisp-0.3.1 (c (n "avrisp") (v "0.3.1") (h "1xfanvfvwfbz8q9l14vp1bqjwpbxkh0hc4h01s22jra8pmacxnln")))

(define-public crate-avrisp-0.3.0 (c (n "avrisp") (v "0.3.0") (d (list (d (n "claim") (r "^0.4") (d #t) (k 2)) (d (n "serial") (r "0.4.*") (d #t) (k 0)))) (h "0sixrbwaxkr5nr6vx3pjx0bcs2k1547g8fna95k1674mpdwyillb")))

