(define-module (crates-io av er aversion-macros) #:use-module (crates-io))

(define-public crate-aversion-macros-0.1.0 (c (n "aversion-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1plph68hgq742n67xj6zlim5z5s35crvzmzsaknk0hfil0davl4n")))

(define-public crate-aversion-macros-0.1.1 (c (n "aversion-macros") (v "0.1.1") (d (list (d (n "aversion") (r "^0.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "103gv8ns2vd9q13miby21zh1ymsyf9mdw7gc8kjzmrvc4al1hjfn")))

(define-public crate-aversion-macros-0.1.2 (c (n "aversion-macros") (v "0.1.2") (d (list (d (n "aversion") (r "^0.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0cjzyq20cs4ybfd909pz11jwyznfzippd3z346bm753yyrjqbh63")))

(define-public crate-aversion-macros-0.2.0 (c (n "aversion-macros") (v "0.2.0") (d (list (d (n "aversion") (r "<=0.2") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "152k39w4qb0g3nrdv1y309favrn84jxldkwnkr2q5g9x2g7hjc7d")))

(define-public crate-aversion-macros-0.2.1 (c (n "aversion-macros") (v "0.2.1") (d (list (d (n "aversion") (r "<=0.2") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1lj4yx4kbbhhx0sg31yyad7kn3w80m0bl9x9mh6am19rjmgpi9av")))

