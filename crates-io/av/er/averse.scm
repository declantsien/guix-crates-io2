(define-module (crates-io av er averse) #:use-module (crates-io))

(define-public crate-averse-0.1.0 (c (n "averse") (v "0.1.0") (d (list (d (n "clap") (r "^3.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "console") (r "^0.15.0") (d #t) (k 0)) (d (n "dialoguer") (r "^0.10.1") (f (quote ("fuzzy-select"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "tabled") (r "^0.6.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "03r0yn1575h3420c366m095l2w0jpr65712vrbalbiwmd9zw1wi6")))

