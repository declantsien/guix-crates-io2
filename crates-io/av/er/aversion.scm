(define-module (crates-io av er aversion) #:use-module (crates-io))

(define-public crate-aversion-0.1.0 (c (n "aversion") (v "0.1.0") (d (list (d (n "aversion-macros") (r "^0.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_cbor") (r "^0.11") (d #t) (k 2)))) (h "1d1b8hrxm4j1xhak4j85gngagdlrb4f1dhlajvkv5q72i52gn949")))

(define-public crate-aversion-0.1.2 (c (n "aversion") (v "0.1.2") (d (list (d (n "aversion-macros") (r "^0.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_cbor") (r "^0.11") (d #t) (k 2)))) (h "1cpiq0p9swbai9sw3fl13bvlq0h4nlmgjj4nzjngs6ixcfcvzlrp")))

(define-public crate-aversion-0.2.0 (c (n "aversion") (v "0.2.0") (d (list (d (n "aversion-macros") (r "^0.2") (d #t) (k 0)) (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_cbor") (r "^0.11") (o #t) (d #t) (k 0)) (d (n "serde_cbor") (r "^0.11") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1h1bg0r08w4dflrgysf2da07xqp2rl5cbp2sdkryk42bjy199d15") (f (quote (("default" "serde_cbor"))))))

(define-public crate-aversion-0.2.1 (c (n "aversion") (v "0.2.1") (d (list (d (n "aversion-macros") (r "^0.2") (d #t) (k 0)) (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_cbor") (r "^0.11") (o #t) (d #t) (k 0)) (d (n "serde_cbor") (r "^0.11") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0d7kdnmvl8hdwcjkihiz311w0y845c6gxvxwkbpjcc6cryw2m6a1") (f (quote (("default" "serde_cbor"))))))

