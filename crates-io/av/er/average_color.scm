(define-module (crates-io av er average_color) #:use-module (crates-io))

(define-public crate-average_color-0.1.0 (c (n "average_color") (v "0.1.0") (d (list (d (n "async-std") (r "^1.10.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.17") (d #t) (k 0)) (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "tokio") (r "^1.11.0") (f (quote ("macros" "rt" "rt-multi-thread"))) (d #t) (k 0)))) (h "1j94r3fxf66n12jcyajwdd1wj90w3kw1gjgz48l0mjjyifs3p003")))

(define-public crate-average_color-0.1.1 (c (n "average_color") (v "0.1.1") (d (list (d (n "async-std") (r "^1.10.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.17") (d #t) (k 0)) (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "tokio") (r "^1.11.0") (f (quote ("macros" "rt" "rt-multi-thread"))) (d #t) (k 0)))) (h "0mdjsxfaa7bv7g0ii7zzrim1gnl1rn31ifx2rd58lcam887bdhdh")))

