(define-module (crates-io av er averaged_collection) #:use-module (crates-io))

(define-public crate-averaged_collection-0.0.1 (c (n "averaged_collection") (v "0.0.1") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "16kz9k1rk2cd2v2wyx8996431ra7490bw1f70nvnfswvy9pgjkg8")))

