(define-module (crates-io av er aver) #:use-module (crates-io))

(define-public crate-aver-0.1.1 (c (n "aver") (v "0.1.1") (h "09nn6al2wvq3s7hy83vsx1p4xf9vcpw85awwacn4p0avyig62pg7")))

(define-public crate-aver-0.1.2 (c (n "aver") (v "0.1.2") (d (list (d (n "winapi") (r "^0.3.8") (f (quote ("wincon" "fileapi" "handleapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0wx11j03d69kk8dqffxjg9n0fqiw7a0k476bpn12z54z19gfmly3")))

(define-public crate-aver-0.1.3 (c (n "aver") (v "0.1.3") (d (list (d (n "winapi") (r "^0.3.8") (f (quote ("wincon" "fileapi" "handleapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1wxfbx7vbc78yxdsg6zgmr2vpphybwy445aql6gf73ymbfxqiv5j")))

(define-public crate-aver-0.1.4 (c (n "aver") (v "0.1.4") (d (list (d (n "winapi") (r "^0.3.8") (f (quote ("wincon" "fileapi" "handleapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "183crp83hfkk76xc6d7ni3sg5ab4v4klffsnr88lif6f0rm5zlcc")))

(define-public crate-aver-0.1.5 (c (n "aver") (v "0.1.5") (d (list (d (n "winapi") (r "^0.3.8") (f (quote ("wincon" "fileapi" "handleapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0097wfaamv34klspvv11zfxy88vvjfygf7b211rr3izkx4zs9ls8")))

