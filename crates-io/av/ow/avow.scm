(define-module (crates-io av ow avow) #:use-module (crates-io))

(define-public crate-avow-0.1.0 (c (n "avow") (v "0.1.0") (d (list (d (n "colored") (r "^1.3.1") (d #t) (k 0)) (d (n "tabwriter") (r "^0.1.25") (d #t) (k 0)))) (h "02a7rscq63xpi8x3k1365s4nyq9bic90xgmbvml0aaczd5qfqhhf")))

(define-public crate-avow-0.2.0 (c (n "avow") (v "0.2.0") (d (list (d (n "colored") (r "^1.3.1") (d #t) (k 0)) (d (n "tabwriter") (r "^0.1.25") (d #t) (k 0)))) (h "17vync7fxjvrridvw1ni2y7sm9dkxjsqmrrwws2l3sldjn7060v6")))

