(define-module (crates-io av -f av-format) #:use-module (crates-io))

(define-public crate-av-format-0.1.0 (c (n "av-format") (v "0.1.0") (d (list (d (n "av-data") (r "^0.1.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)))) (h "08645pgm2b1xi3325wy2ipbx8j0kq8j5wznhw74n45qm404rzc5i")))

(define-public crate-av-format-0.2.0 (c (n "av-format") (v "0.2.0") (d (list (d (n "av-data") (r "^0.2.0") (d #t) (k 0)) (d (n "err-derive") (r "^0.1.6") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)))) (h "035svjcb3ghsxqr0l92n6qh4yvqfnkhrk2jvvvkqdfvynnizc7gd")))

(define-public crate-av-format-0.2.1 (c (n "av-format") (v "0.2.1") (d (list (d (n "av-data") (r "^0.2.1") (d #t) (k 0)) (d (n "err-derive") (r "^0.2.3") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)))) (h "0si0il3phddzf1xknv3s5xakmbddrm7w4xazmjqgyq5x4p9anpyn")))

(define-public crate-av-format-0.3.0 (c (n "av-format") (v "0.3.0") (d (list (d (n "av-data") (r "^0.2.1") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1r2v6gsggqs86c3yyn7bwby343giac531632k28f7dx8hjxbmn0v")))

(define-public crate-av-format-0.3.1 (c (n "av-format") (v "0.3.1") (d (list (d (n "av-data") (r "^0.3.0") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0xk604nhdwj4s8mmih876fyz66xg5agnpmkag7m89qvhq2zmbcsk")))

(define-public crate-av-format-0.4.0 (c (n "av-format") (v "0.4.0") (d (list (d (n "av-data") (r "^0.3.0") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1bcwv4ayr7l104madbngl66iqlq70zk5xg0hfiwgdxz87pdc4p5f")))

(define-public crate-av-format-0.5.0 (c (n "av-format") (v "0.5.0") (d (list (d (n "av-data") (r "^0.3.0") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "16vd27da5q3zpgzzn7f1zig2pyp0gb8h9nzv86iwia0y0530a8pn")))

(define-public crate-av-format-0.6.0 (c (n "av-format") (v "0.6.0") (d (list (d (n "av-data") (r "^0.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "07z6r1yqbf8rksgvkfysdiykj0j02cpxp43fyy8bx0vz9jqrklys")))

(define-public crate-av-format-0.7.0 (c (n "av-format") (v "0.7.0") (d (list (d (n "av-data") (r "^0.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1dy35p0g2g0bzi83w9pc8khvd9hyl9mxa6wrccq38nf06r54jj9q")))

