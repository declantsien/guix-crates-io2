(define-module (crates-io av oc avocado_derive) #:use-module (crates-io))

(define-public crate-avocado_derive-0.0.5 (c (n "avocado_derive") (v "0.0.5") (d (list (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.14.9") (d #t) (k 0)))) (h "065a5rgjz9r2ajvdzcz5brp2xyhsbai99dwj3pmw46gvwq2rz8hh")))

(define-public crate-avocado_derive-0.1.0 (c (n "avocado_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.14.9") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1jp5nbm9ak4pvj83pgg8rg0ffzsng4ms8x7vhbgq94k4x7n4281g")))

(define-public crate-avocado_derive-0.1.1 (c (n "avocado_derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.14.9") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0ykll63bwzyaibqjkkjhhsfzxrifi9zq559ni2f33dshywcp9npj")))

(define-public crate-avocado_derive-0.2.0 (c (n "avocado_derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.14.9") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1rq78n5p4mv9p3njvzx03jkbnm542k7rww3dbfkj18b0jjd7ibrj")))

(define-public crate-avocado_derive-0.3.0 (c (n "avocado_derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^0.4.26") (d #t) (k 0)) (d (n "quote") (r "^0.6.11") (d #t) (k 0)) (d (n "syn") (r "^0.14.9") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "083yavpp9l8wdd8c9vqvlqrysyhbl7zcz30vpmp3vkxvs5spbs7a")))

(define-public crate-avocado_derive-0.3.1 (c (n "avocado_derive") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^0.4.26") (d #t) (k 0)) (d (n "quote") (r "^0.6.11") (d #t) (k 0)) (d (n "syn") (r "^0.14.9") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "167gsidbcj4ncxpwdhha5fz8fhkpsf6pp2c17czcycfll3gh330x")))

(define-public crate-avocado_derive-0.3.2 (c (n "avocado_derive") (v "0.3.2") (d (list (d (n "proc-macro2") (r "^0.4.26") (d #t) (k 0)) (d (n "quote") (r "^0.6.11") (d #t) (k 0)) (d (n "syn") (r "^0.14.9") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1gjw6bxl64ksn106vxxv2pq7haywzn82pl3vv2vwi2dwal9jgzlp")))

(define-public crate-avocado_derive-0.3.3 (c (n "avocado_derive") (v "0.3.3") (d (list (d (n "proc-macro2") (r "^0.4.26") (d #t) (k 0)) (d (n "quote") (r "^0.6.11") (d #t) (k 0)) (d (n "syn") (r "^0.14.9") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "036aqlqzygz5kipli37iixzgkjf1snv6y5l3d2016hz6bjq5k8iy")))

(define-public crate-avocado_derive-0.4.0 (c (n "avocado_derive") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^0.4.26") (d #t) (k 0)) (d (n "quote") (r "^0.6.11") (d #t) (k 0)) (d (n "syn") (r "^0.14.9") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0b0lnqwhzbq9895b8fwhgdmfnima4dlbd4nvr13iinf5lhcscs1n")))

(define-public crate-avocado_derive-0.5.0 (c (n "avocado_derive") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^0.4.26") (d #t) (k 0)) (d (n "quote") (r "^0.6.11") (d #t) (k 0)) (d (n "syn") (r "^0.14.9") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "08g6xf9d68ndspi9x6ym8dwsk15ylcj2s1svvk8px5qsns7ad4kr")))

(define-public crate-avocado_derive-0.6.0 (c (n "avocado_derive") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^0.4.26") (d #t) (k 0)) (d (n "quote") (r "^0.6.11") (d #t) (k 0)) (d (n "syn") (r "^0.14.9") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0a2m6x1nhlgbwn9mimj4nm5gi7rz6ai2ba98ai4sagwjcl8qpi10")))

