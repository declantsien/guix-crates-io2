(define-module (crates-io av oc avocado-schema-derive) #:use-module (crates-io))

(define-public crate-avocado-schema-derive-0.6.2 (c (n "avocado-schema-derive") (v "0.6.2") (d (list (d (n "avocado-schema") (r "^0.6.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0gy3gs4yknhlfq4lklfqfwff3lxsmhcl6vqwa7idfz5ypijygzp8")))

(define-public crate-avocado-schema-derive-0.6.3 (c (n "avocado-schema-derive") (v "0.6.3") (d (list (d (n "avocado-schema") (r "^0.6.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "09mz0niscg8hvcyfkyg3b927zn4v0c0mv9pnjyd4wxvi2qngmdn9")))

(define-public crate-avocado-schema-derive-0.7.0 (c (n "avocado-schema-derive") (v "0.7.0") (d (list (d (n "avocado-schema") (r "^0.7.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1916aml0v2hz6zgdlcqpkbf7qc4ycxchir1gp2aw6mfp64iafg2s")))

(define-public crate-avocado-schema-derive-0.8.0 (c (n "avocado-schema-derive") (v "0.8.0") (d (list (d (n "avocado-schema") (r "^0.8.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1nqxfsh721hr4mk1sbkyb6y8bngrbg7lls6yh0xh9pcsnfmdak9v")))

