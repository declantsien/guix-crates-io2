(define-module (crates-io av if avif_img) #:use-module (crates-io))

(define-public crate-avif_img-0.1.0 (c (n "avif_img") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "aom-decode") (r "^0.2.5") (d #t) (k 0)) (d (n "image") (r "^0.24.6") (d #t) (k 0)))) (h "0hw1snm0xlfjmzkh748rrdxkp90n63bvv871bwjsv8hg5ry8929c")))

(define-public crate-avif_img-0.1.1 (c (n "avif_img") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "aom-decode") (r "^0.2.5") (d #t) (k 0)) (d (n "image") (r "^0.24.6") (d #t) (k 0)))) (h "1sxgcapf6p69l4lqzi1xyxmpkcjrjfaiahvb22mavrkxav6hbsjs")))

(define-public crate-avif_img-0.1.2 (c (n "avif_img") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "aom-decode") (r "^0.2.5") (d #t) (k 0)) (d (n "image") (r "^0.24.6") (d #t) (k 0)))) (h "125ykqyfcnflr2p394zm7q4f45rbq01xjw2pr0a4crcq1h5k2fqq")))

