(define-module (crates-io av if avif-serialize) #:use-module (crates-io))

(define-public crate-avif-serialize-0.6.0 (c (n "avif-serialize") (v "0.6.0") (d (list (d (n "arrayvec") (r "^0.5.1") (d #t) (k 0)) (d (n "mp4parse") (r "^0.11.4") (d #t) (k 2)))) (h "1g9d41x4rnlv5irlb5g7cw0mlfl60gsi62lbdyvjpaxlc9gjk9fs") (y #t)))

(define-public crate-avif-serialize-0.6.1 (c (n "avif-serialize") (v "0.6.1") (d (list (d (n "arrayvec") (r "^0.5.1") (d #t) (k 0)) (d (n "avif-parse") (r "^0.12.1") (d #t) (k 2)) (d (n "mp4parse") (r "^0.11.4") (d #t) (k 2)))) (h "18m7mga92szgb0jg3zd3h38cxjb06psl1zfzhi9imdskszqx4ngg") (y #t)))

(define-public crate-avif-serialize-0.6.2 (c (n "avif-serialize") (v "0.6.2") (d (list (d (n "arrayvec") (r "^0.5.1") (d #t) (k 0)) (d (n "avif-parse") (r "^0.12.1") (d #t) (k 2)) (d (n "mp4parse") (r "^0.11.4") (d #t) (k 2)))) (h "0ja4w4glgh2xc1gycr5ipji8kagxjag7xdxqkv7lyqwdna4zbf3i") (y #t)))

(define-public crate-avif-serialize-0.6.3 (c (n "avif-serialize") (v "0.6.3") (d (list (d (n "arrayvec") (r "^0.5.1") (d #t) (k 0)) (d (n "avif-parse") (r "^0.12.2") (d #t) (k 2)) (d (n "mp4parse") (r "^0.11.4") (d #t) (k 2)))) (h "12vjljnhmh7rm1darxv9qqkaj419k9nw2hrr821ydwq5xh2zv4gy") (y #t)))

(define-public crate-avif-serialize-0.6.4 (c (n "avif-serialize") (v "0.6.4") (d (list (d (n "arrayvec") (r "^0.5.1") (d #t) (k 0)) (d (n "avif-parse") (r "^0.12.2") (d #t) (k 2)) (d (n "mp4parse") (r "^0.11.4") (d #t) (k 2)))) (h "17v537gi8a59yqycq3f12r9k0nbc120fwh77ifqnhcayn5pdp36s") (y #t)))

(define-public crate-avif-serialize-0.6.5 (c (n "avif-serialize") (v "0.6.5") (d (list (d (n "arrayvec") (r "^0.5.2") (d #t) (k 0)) (d (n "avif-parse") (r "^0.13.0") (d #t) (k 2)) (d (n "mp4parse") (r "^0.11.4") (d #t) (k 2)))) (h "130wq838lslkcqcp2kjci7q3aq9qpir07pvxndc81xqbn63wvdjg") (y #t)))

(define-public crate-avif-serialize-0.7.0 (c (n "avif-serialize") (v "0.7.0") (d (list (d (n "arrayvec") (r "^0.7.0") (d #t) (k 0)) (d (n "avif-parse") (r "^0.13.1") (d #t) (k 2)) (d (n "mp4parse") (r "^0.11.5") (d #t) (k 2)))) (h "1az9plxv2wf1fl24frxa7iw62px9z3y75mv09j73bcsmiycca5zy") (y #t)))

(define-public crate-avif-serialize-0.7.1 (c (n "avif-serialize") (v "0.7.1") (d (list (d (n "arrayvec") (r "^0.7.0") (d #t) (k 0)) (d (n "avif-parse") (r "^0.13.1") (d #t) (k 2)) (d (n "mp4parse") (r "^0.11.5") (d #t) (k 2)))) (h "1y6h45bigzmnbk9whngr6kmk0a25rwnn2fdpvnn2kkdydvarkdyb") (y #t)))

(define-public crate-avif-serialize-0.7.2 (c (n "avif-serialize") (v "0.7.2") (d (list (d (n "arrayvec") (r "^0.7.0") (d #t) (k 0)) (d (n "avif-parse") (r "^0.13.1") (d #t) (k 2)) (d (n "mp4parse") (r "^0.11.5") (d #t) (k 2)))) (h "18fkq2cchvmza304wh3142c09qgchs5ix68h1zr5y6ph5j67ikc9") (y #t)))

(define-public crate-avif-serialize-0.7.3 (c (n "avif-serialize") (v "0.7.3") (d (list (d (n "arrayvec") (r "^0.7.0") (d #t) (k 0)) (d (n "avif-parse") (r "^0.13.1") (d #t) (k 2)))) (h "1v8izvgz16ci7nc0a3ahbkmmf5n2ibds2wxz1a7div47189jim4n") (y #t)))

(define-public crate-avif-serialize-0.7.4 (c (n "avif-serialize") (v "0.7.4") (d (list (d (n "arrayvec") (r "^0.7.0") (d #t) (k 0)) (d (n "avif-parse") (r "^0.13.1") (d #t) (k 2)))) (h "1h9z0y72z383jqydxblzb0z6gj545l8n6h1lblzx9r73f3d0ahlw") (y #t)))

(define-public crate-avif-serialize-0.7.5 (c (n "avif-serialize") (v "0.7.5") (d (list (d (n "arrayvec") (r "^0.7.0") (d #t) (k 0)) (d (n "avif-parse") (r "^0.13.1") (d #t) (k 2)))) (h "16fnqqgnjlrw5cin8zq6hrwaqpgzylyq9aivcwgb7cf3jxr8l0dw") (y #t)))

(define-public crate-avif-serialize-0.7.6 (c (n "avif-serialize") (v "0.7.6") (d (list (d (n "arrayvec") (r "^0.7.2") (d #t) (k 0)) (d (n "avif-parse") (r "^1.0.0") (d #t) (k 2)))) (h "1fb9ld4iq8d5q5i9nr60hsdvdpjw4zb65kagv7xp08gphycwqy0f")))

(define-public crate-avif-serialize-0.7.7 (c (n "avif-serialize") (v "0.7.7") (d (list (d (n "arrayvec") (r "^0.7.2") (d #t) (k 0)) (d (n "avif-parse") (r "^1.0.0") (d #t) (k 2)))) (h "0d2makdw756978i8s3qhlhh1h91y5maxriay6r4kmsmk8pky2qfc")))

(define-public crate-avif-serialize-0.8.0 (c (n "avif-serialize") (v "0.8.0") (d (list (d (n "arrayvec") (r "^0.7.2") (d #t) (k 0)) (d (n "avif-parse") (r "^1.0.0") (d #t) (k 2)))) (h "13g3dypznqj7mg2igqswr47gc1zr8ljbx6nhndcl9c8vi9cf2xkr")))

(define-public crate-avif-serialize-0.8.1 (c (n "avif-serialize") (v "0.8.1") (d (list (d (n "arrayvec") (r "^0.7.2") (d #t) (k 0)) (d (n "avif-parse") (r "^1.0.0") (d #t) (k 2)))) (h "1llnwlj11wcifdlny8x8yksl3zmz8i6a35il0cd4ar335yj7av47")))

