(define-module (crates-io av at avatars_cli) #:use-module (crates-io))

(define-public crate-avatars_cli-0.1.0 (c (n "avatars_cli") (v "0.1.0") (h "0rs082q636is5c84vz9wq4rwsbwjlb5da2mlgxfzkvhkq5lqn82z")))

(define-public crate-avatars_cli-0.1.1 (c (n "avatars_cli") (v "0.1.1") (h "07gb8hm5k7hz1rk6pdi19w2v47pjjrh5c2j7y09zgzhsxkj08bg5")))

(define-public crate-avatars_cli-0.1.2 (c (n "avatars_cli") (v "0.1.2") (h "05wvdyvz8pp87gb4r4iv14jnd2nqi3v1qmm928qy4216y5yjk9yi")))

(define-public crate-avatars_cli-0.1.3 (c (n "avatars_cli") (v "0.1.3") (d (list (d (n "avatars") (r "^0.1.1") (d #t) (k 0)))) (h "0508a7srn13ifb9y83d04wb4k6zi9yfxsw93qp0plk5dzmakzfwx")))

(define-public crate-avatars_cli-0.1.4 (c (n "avatars_cli") (v "0.1.4") (d (list (d (n "avatars") (r "^0.1.1") (d #t) (k 0)) (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (k 0)) (d (n "duct") (r "^0.13.6") (d #t) (k 2)))) (h "0ysz1mc7jf4f4zbjm7rkz7h8q1l8hrr98aaha6bqj9lbzv83w0wg")))

(define-public crate-avatars_cli-0.1.5 (c (n "avatars_cli") (v "0.1.5") (d (list (d (n "avatars") (r "^0.1.2") (d #t) (k 0)) (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (k 0)) (d (n "duct") (r "^0.13.6") (d #t) (k 2)))) (h "0yaj0i40qi0fiqr8nfn2r9dy1ay6wr4fc4ilw45y9hfcs6852wil")))

(define-public crate-avatars_cli-0.1.6 (c (n "avatars_cli") (v "0.1.6") (d (list (d (n "avatars") (r "^0.1.2") (d #t) (k 0)) (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (k 0)) (d (n "duct") (r "^0.13.6") (d #t) (k 2)))) (h "03brn7mfx29if5lnzmivygmvq6bnmd60w9ayk129sgcpm6pqnqlv")))

(define-public crate-avatars_cli-0.1.7 (c (n "avatars_cli") (v "0.1.7") (d (list (d (n "avatars") (r "^0.1.4") (d #t) (k 0)) (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (k 0)) (d (n "duct") (r "^0.13.6") (d #t) (k 2)))) (h "0nqzir24srxcf2cvxhw922ilhdsl1y1mry13i828gb1kfg1shhjx")))

(define-public crate-avatars_cli-0.1.8 (c (n "avatars_cli") (v "0.1.8") (d (list (d (n "avatars") (r "^0.1.6") (d #t) (k 0)) (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (k 0)) (d (n "duct") (r "^0.13.6") (d #t) (k 2)))) (h "1k4day5ngwp8v2c1svkw57d26zkspkryj5hz609g93w78jss23ap")))

