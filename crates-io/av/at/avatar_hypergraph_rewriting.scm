(define-module (crates-io av at avatar_hypergraph_rewriting) #:use-module (crates-io))

(define-public crate-avatar_hypergraph_rewriting-0.1.0 (c (n "avatar_hypergraph_rewriting") (v "0.1.0") (h "1k1fcfqg1pxvz2illn8lgpxlr3y04j5q1bd3n4axax85w5lkwaz2")))

(define-public crate-avatar_hypergraph_rewriting-0.1.1 (c (n "avatar_hypergraph_rewriting") (v "0.1.1") (h "0w5gy0mbdavpkaaff0yaqga8hrwkh5mcbznznknvjp7rvskdkakj")))

