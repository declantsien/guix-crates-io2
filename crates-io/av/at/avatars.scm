(define-module (crates-io av at avatars) #:use-module (crates-io))

(define-public crate-avatars-0.1.0 (c (n "avatars") (v "0.1.0") (h "0vmg2bvdwlnw25i39c8k83ikp6sx2y5daq6sxd7sskyiavmajb6i")))

(define-public crate-avatars-0.1.1 (c (n "avatars") (v "0.1.1") (d (list (d (n "itoa") (r "^1.0.5") (d #t) (k 0)))) (h "0y1691pzicgc59i6i1rkpksy09q5h7mi087g5j2699mn0rzfj7m8")))

(define-public crate-avatars-0.1.2 (c (n "avatars") (v "0.1.2") (d (list (d (n "itoa") (r "^1.0.5") (d #t) (k 0)))) (h "0gfljcjzd8k59djfx10s0s77n8lvlv6zw979wizxlamdspj0w1ax")))

(define-public crate-avatars-0.1.3 (c (n "avatars") (v "0.1.3") (d (list (d (n "itoa") (r "^1.0.5") (d #t) (k 0)))) (h "1d8fxj3aby4j98aggby6fxl4vw6idzmqpnkwg5h3wwx0baclmfjk")))

(define-public crate-avatars-0.1.4 (c (n "avatars") (v "0.1.4") (d (list (d (n "itoa") (r "^1.0.5") (d #t) (k 0)))) (h "15caikacs5h00xly6ns0s7n0qllb428ganzb6lkq7cri68s8jzq3")))

(define-public crate-avatars-0.1.5 (c (n "avatars") (v "0.1.5") (d (list (d (n "itoa") (r "^1.0.5") (d #t) (k 0)))) (h "1s2yp11yk0bqn1q8aqxgj1f8bnpzxln4fji55shhyzbr5cr2f0sy")))

(define-public crate-avatars-0.1.6 (c (n "avatars") (v "0.1.6") (d (list (d (n "itoa") (r "^1.0.5") (d #t) (k 0)))) (h "1il0q62zdz9wh7raprmwsk76brgpcnza362cnnnmjmrjvxi9yjpf")))

(define-public crate-avatars-0.1.7 (c (n "avatars") (v "0.1.7") (d (list (d (n "itoa") (r "^1.0.5") (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "00sdgqll8s0123g2rczf9x3imqjv8mav77j2ih8hxcjjjk32xrdp") (y #t)))

(define-public crate-avatars-0.1.8 (c (n "avatars") (v "0.1.8") (d (list (d (n "itoa") (r "^1.0.5") (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0f8xb8f4p36fxghk26fmmjj2jf9l0rks6ni4whdgs9daxn0c3mpp")))

(define-public crate-avatars-0.1.9 (c (n "avatars") (v "0.1.9") (d (list (d (n "itoa") (r "^1.0.5") (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1f1d6qh763hflb6hnlrpkzyqrfdx75p15w485z879lck261c2v8y")))

