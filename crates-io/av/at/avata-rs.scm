(define-module (crates-io av at avata-rs) #:use-module (crates-io))

(define-public crate-avata-rs-0.0.0 (c (n "avata-rs") (v "0.0.0") (h "0q833qxz86bsnh7kndk5y4ix2fj86h77b5vpdh43w8sj41gzzlr5") (y #t)))

(define-public crate-avata-rs-0.0.1 (c (n "avata-rs") (v "0.0.1") (h "0isc3vbb82zi5lj28di431qh7gcrcpmzqmj6zcs3dyh78hh6dzxp")))

