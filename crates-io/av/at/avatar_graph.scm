(define-module (crates-io av at avatar_graph) #:use-module (crates-io))

(define-public crate-avatar_graph-0.1.0 (c (n "avatar_graph") (v "0.1.0") (d (list (d (n "piston") (r "^0.51.0") (d #t) (k 2)) (d (n "piston2d-graphics") (r "^0.36.0") (d #t) (k 2)) (d (n "piston2d-opengl_graphics") (r "^0.72.0") (d #t) (k 2)) (d (n "pistoncore-sdl2_window") (r "^0.65.0") (d #t) (k 2)))) (h "1vvfm7p9if9hgr5ba5y276xqhnmngcw6wr4p1gpcj2c6akyn6wh1")))

(define-public crate-avatar_graph-0.2.0 (c (n "avatar_graph") (v "0.2.0") (d (list (d (n "piston") (r "^0.51.0") (d #t) (k 2)) (d (n "piston2d-graphics") (r "^0.36.0") (d #t) (k 2)) (d (n "piston2d-opengl_graphics") (r "^0.72.0") (d #t) (k 2)) (d (n "pistoncore-sdl2_window") (r "^0.65.0") (d #t) (k 2)))) (h "1dm1pzlv6j8nsc0xnbwlp76zs76z4j26563gjvcgcixfvqsqmw7b")))

(define-public crate-avatar_graph-0.3.0 (c (n "avatar_graph") (v "0.3.0") (d (list (d (n "piston") (r "^0.51.0") (d #t) (k 2)) (d (n "piston2d-graphics") (r "^0.36.0") (d #t) (k 2)) (d (n "piston2d-opengl_graphics") (r "^0.72.0") (d #t) (k 2)) (d (n "pistoncore-sdl2_window") (r "^0.65.0") (d #t) (k 2)))) (h "0f2483w1lfgfs0afb3ymp0b4g2z607b6pygbwkwff5d5iwk02hi5")))

(define-public crate-avatar_graph-0.4.0 (c (n "avatar_graph") (v "0.4.0") (d (list (d (n "piston") (r "^0.51.0") (d #t) (k 2)) (d (n "piston2d-graphics") (r "^0.36.0") (d #t) (k 2)) (d (n "piston2d-opengl_graphics") (r "^0.72.0") (d #t) (k 2)) (d (n "pistoncore-sdl2_window") (r "^0.65.0") (d #t) (k 2)))) (h "0yw3691nc5ky672913rpk74v6vmgja0950mzm49irnd81g29whni")))

(define-public crate-avatar_graph-0.4.1 (c (n "avatar_graph") (v "0.4.1") (d (list (d (n "piston") (r "^0.51.0") (d #t) (k 2)) (d (n "piston2d-graphics") (r "^0.36.0") (d #t) (k 2)) (d (n "piston2d-opengl_graphics") (r "^0.72.0") (d #t) (k 2)) (d (n "pistoncore-sdl2_window") (r "^0.65.0") (d #t) (k 2)))) (h "1f4fm08z6hz6yks38mdvvig98lhq2468s2lzf1dvcsibbwlmvsd3")))

(define-public crate-avatar_graph-0.5.0 (c (n "avatar_graph") (v "0.5.0") (d (list (d (n "piston") (r "^0.51.0") (d #t) (k 2)) (d (n "piston2d-graphics") (r "^0.36.0") (d #t) (k 2)) (d (n "piston2d-opengl_graphics") (r "^0.72.0") (d #t) (k 2)) (d (n "pistoncore-sdl2_window") (r "^0.65.0") (d #t) (k 2)))) (h "14hir7fa2agm0q7az3rdpgw0l06bwaw8ry55fysbq1afdm0jq2qi")))

(define-public crate-avatar_graph-0.6.0 (c (n "avatar_graph") (v "0.6.0") (d (list (d (n "piston") (r "^0.51.0") (d #t) (k 2)) (d (n "piston2d-graphics") (r "^0.36.0") (d #t) (k 2)) (d (n "piston2d-opengl_graphics") (r "^0.72.0") (d #t) (k 2)) (d (n "pistoncore-sdl2_window") (r "^0.65.0") (d #t) (k 2)))) (h "0i7i33q4vzqarjcv17iw08hkypd1py4g90g5bpqvkjgfxxhnkqch")))

(define-public crate-avatar_graph-0.6.1 (c (n "avatar_graph") (v "0.6.1") (d (list (d (n "piston") (r "^0.51.0") (d #t) (k 2)) (d (n "piston2d-graphics") (r "^0.36.0") (d #t) (k 2)) (d (n "piston2d-opengl_graphics") (r "^0.72.0") (d #t) (k 2)) (d (n "pistoncore-sdl2_window") (r "^0.65.0") (d #t) (k 2)))) (h "0ksylyf1akpmqs5fjzam00n2fzmkj9c1j2jfcnwcdlapvdqqm5rx")))

(define-public crate-avatar_graph-0.6.2 (c (n "avatar_graph") (v "0.6.2") (d (list (d (n "piston") (r "^0.51.0") (d #t) (k 2)) (d (n "piston2d-graphics") (r "^0.36.0") (d #t) (k 2)) (d (n "piston2d-opengl_graphics") (r "^0.72.0") (d #t) (k 2)) (d (n "pistoncore-sdl2_window") (r "^0.65.0") (d #t) (k 2)))) (h "1wns91lch6hyg9myj8icvzxkvfphgk5mcfg19nifmlj16mbhn85w")))

(define-public crate-avatar_graph-0.6.3 (c (n "avatar_graph") (v "0.6.3") (d (list (d (n "piston") (r "^0.51.0") (d #t) (k 2)) (d (n "piston2d-graphics") (r "^0.36.0") (d #t) (k 2)) (d (n "piston2d-opengl_graphics") (r "^0.72.0") (d #t) (k 2)) (d (n "pistoncore-sdl2_window") (r "^0.65.0") (d #t) (k 2)))) (h "1d2qpj4yyhjnr3liql4g5wn01javxd7y2xigymjikb527wkhr21k")))

