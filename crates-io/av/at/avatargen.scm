(define-module (crates-io av at avatargen) #:use-module (crates-io))

(define-public crate-avatargen-0.1.0 (c (n "avatargen") (v "0.1.0") (d (list (d (n "png") (r "^0.17.10") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "00q200pldkicjm9l88zvak1bhglnx53jb1rlxjnkqncvj1f0i6q6")))

