(define-module (crates-io av at avatarmud-client) #:use-module (crates-io))

(define-public crate-avatarmud-client-0.1.0 (c (n "avatarmud-client") (v "0.1.0") (d (list (d (n "nonblock") (r "^0.1") (d #t) (k 0)) (d (n "telnet") (r "^0.2") (d #t) (k 0)) (d (n "termios") (r "^0.3") (d #t) (k 0)))) (h "0p4b835422dm8h1hxcf7kzm2w76idip5dbfvk97kg1204da1xrc1")))

