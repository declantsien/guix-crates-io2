(define-module (crates-io av ar avareum-timelock) #:use-module (crates-io))

(define-public crate-avareum-timelock-0.3.0 (c (n "avareum-timelock") (v "0.3.0") (d (list (d (n "borsh") (r "^0.9.1") (d #t) (k 0)) (d (n "solana-program") (r "^1.7.11") (d #t) (k 0)) (d (n "spl-associated-token-account") (r "^1.0.3") (d #t) (k 0)) (d (n "spl-token") (r "^3.2.0") (f (quote ("no-entrypoint"))) (d #t) (k 0)))) (h "1lpx2ps3nlqy70d9lnj8hrnpyyk4dx3k6yby39mmsq8x6zxa3csh")))

(define-public crate-avareum-timelock-0.3.1 (c (n "avareum-timelock") (v "0.3.1") (d (list (d (n "borsh") (r "^0.9.1") (d #t) (k 0)) (d (n "solana-program") (r "^1.7.11") (d #t) (k 0)) (d (n "spl-associated-token-account") (r "^1.0.3") (d #t) (k 0)) (d (n "spl-token") (r "^3.2.0") (f (quote ("no-entrypoint"))) (d #t) (k 0)))) (h "0k92abgpjxsigbcjq3wmjvh8lfdl6sv08kdlm344k41p8jipsf89")))

