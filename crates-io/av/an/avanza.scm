(define-module (crates-io av an avanza) #:use-module (crates-io))

(define-public crate-avanza-0.1.3 (c (n "avanza") (v "0.1.3") (d (list (d (n "envy") (r "^0.4.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.7") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.131") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.73") (d #t) (k 0)) (d (n "tokio") (r "^1.14.0") (f (quote ("macros"))) (d #t) (k 0)) (d (n "tokio-test") (r "^0.4.2") (d #t) (k 0)) (d (n "wiremock") (r "^0.5.8") (d #t) (k 0)))) (h "1vzafdhmndy0cyh59qwch0wh4ihfxjzn7y3mpkfrxxvpn7x9rkd1")))

