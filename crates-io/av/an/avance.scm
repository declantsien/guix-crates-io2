(define-module (crates-io av an avance) #:use-module (crates-io))

(define-public crate-avance-0.1.0 (c (n "avance") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.25") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "version_check") (r "^0.9") (d #t) (k 1)))) (h "0by5daj3zzwz8y93r67cq1m385s8j8dzv04v91ihrbz41nrmxmmq") (y #t)))

(define-public crate-avance-0.2.0 (c (n "avance") (v "0.2.0") (d (list (d (n "crossterm") (r "^0.25") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "strum") (r "^0.25.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "104v1hvdcswpj7qc2m0ap1vg3nypk4kbyzf45v95wk6ha78fzmc0")))

(define-public crate-avance-0.2.1 (c (n "avance") (v "0.2.1") (d (list (d (n "crossterm") (r "^0.25") (d #t) (k 0)) (d (n "strum") (r "^0.25.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0lg38czgicgzbxv3azjfr0n7n2pql91ss4zg74hzh0pzvymb2bi6")))

(define-public crate-avance-0.2.2 (c (n "avance") (v "0.2.2") (d (list (d (n "crossterm") (r "^0.25") (d #t) (k 0)) (d (n "strum") (r "^0.25.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1k0rcbhcpsfb4cp474f3b7bpaabj5hs0hadqvz4vkch58vkvkl93") (r "1.70.0")))

(define-public crate-avance-0.2.3 (c (n "avance") (v "0.2.3") (d (list (d (n "crossterm") (r "^0.25") (d #t) (k 0)) (d (n "strum") (r "^0.25.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1hv6kjy8lmlva6vxkgyq981kk3jgma8k3bxk5dama0c2wb55h7r7") (r "1.70.0")))

(define-public crate-avance-0.2.4 (c (n "avance") (v "0.2.4") (d (list (d (n "crossterm") (r "^0.25") (d #t) (k 0)) (d (n "strum") (r "^0.25.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1si3kfhrxv1qipbwijxwvz3p7hxafypyfqdkv7ilg2adgcs6n4nz") (r "1.70.0")))

(define-public crate-avance-0.2.5 (c (n "avance") (v "0.2.5") (d (list (d (n "crossterm") (r "^0.25") (d #t) (k 0)) (d (n "strum") (r "^0.25.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0sxlrzm910n78k0qcs3lvzy9k53gbdpmf4w4ly9avpw6dgsngvxb") (r "1.70.0")))

(define-public crate-avance-0.3.0 (c (n "avance") (v "0.3.0") (d (list (d (n "crossterm") (r "^0.25") (d #t) (k 0)) (d (n "strum") (r "^0.25.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1basnwpsvxl669i9wf7av075hb5pgpvs363dvzn3vxpl6pyj6kpm") (r "1.70.0")))

(define-public crate-avance-0.3.1 (c (n "avance") (v "0.3.1") (d (list (d (n "crossterm") (r "^0.25") (d #t) (k 0)) (d (n "strum") (r "^0.25.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0q58sipy3rljxq1w1nmjl98wwcgqcd25pijvfnrz2prldz5vlanz") (r "1.70.0")))

(define-public crate-avance-0.4.0 (c (n "avance") (v "0.4.0") (d (list (d (n "crossterm") (r "^0.25") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "strum") (r "^0.25.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "version_check") (r "^0.9.4") (d #t) (k 1)))) (h "0a38bz7kd04xh8q8qbiawprhvymp0ri8acdxnrnvgyqasc6my663") (r "1.63.0")))

(define-public crate-avance-0.4.1 (c (n "avance") (v "0.4.1") (d (list (d (n "crossterm") (r "^0.25") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "strum") (r "^0.25.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "version_check") (r "^0.9.4") (d #t) (k 1)))) (h "0jhzp4abxxmadsgkzhs23fdgl9x58qm8hxbj3q4nkga25h6ixavb") (r "1.63.0")))

(define-public crate-avance-0.4.2 (c (n "avance") (v "0.4.2") (d (list (d (n "crossterm") (r "^0.25") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "strum") (r "^0.25.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "version_check") (r "^0.9.4") (d #t) (k 1)))) (h "0lw46x19jqay4489dmmd2d4f4hc60rr6r9nmk2yn2dfyi1h3pxk9") (r "1.63.0")))

(define-public crate-avance-0.4.3 (c (n "avance") (v "0.4.3") (d (list (d (n "crossterm") (r "^0.25") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "strum") (r "^0.25.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "version_check") (r "^0.9.4") (d #t) (k 1)))) (h "1lhnjxrzighlyxsbjrxb20y3wlr2h30ls7y0mvn129f7r3flnzjz") (r "1.63.0")))

(define-public crate-avance-0.4.4 (c (n "avance") (v "0.4.4") (d (list (d (n "crossterm") (r "^0.25") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "strum") (r "^0.25.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "version_check") (r "^0.9.4") (d #t) (k 1)))) (h "03q849s7lch37cmaj133vyzwfra0nzx9006n4wd6dk1wsdlgl6zy") (r "1.63.0")))

(define-public crate-avance-0.5.0 (c (n "avance") (v "0.5.0") (d (list (d (n "crossterm") (r "^0.25") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "strum") (r "^0.25.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "version_check") (r "^0.9.4") (d #t) (k 1)))) (h "1l1yyxafw4xlqndcz51wy3hyklkvngh29zk4j90smb3wcmifwwaa") (r "1.63.0")))

(define-public crate-avance-0.5.1 (c (n "avance") (v "0.5.1") (d (list (d (n "crossterm") (r "^0.25") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "strum") (r "^0.25.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "version_check") (r "^0.9.4") (d #t) (k 1)))) (h "1q9g4zq19068v1c51i0h5k4yb3rq3snvzvdnhjyhak7mcysmpyvh") (r "1.63.0")))

(define-public crate-avance-0.6.0 (c (n "avance") (v "0.6.0") (d (list (d (n "crossterm") (r "^0.25") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "strum") (r "^0.25.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "version_check") (r "^0.9.4") (d #t) (k 1)))) (h "16x1fbl2bggi0b83gz2z0bnx52f99yhdmqn2la7p3vgsivgafapq") (r "1.63.0")))

(define-public crate-avance-0.6.1 (c (n "avance") (v "0.6.1") (d (list (d (n "crossterm") (r "^0.25") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "version_check") (r "^0.9.4") (d #t) (k 1)))) (h "1w5wbsrp04gps2wjjqs1jk8wdjylgp9b0im2rhm3kln3lblhx60c") (r "1.63.0")))

(define-public crate-avance-0.6.2 (c (n "avance") (v "0.6.2") (d (list (d (n "crossterm") (r "^0.25") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "version_check") (r "^0.9.4") (d #t) (k 1)))) (h "015bin8ki79ljnma85irazahqqpc2wspg7yl55rc61fm5dkahrfi") (r "1.63.0")))

(define-public crate-avance-0.6.3 (c (n "avance") (v "0.6.3") (d (list (d (n "crossterm") (r "^0.25") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "version_check") (r "^0.9.4") (d #t) (k 1)))) (h "1404q5p8ij430y7spy0kq7r0i702kg805v99c7lxzghlxc8jvwz7") (r "1.63.0")))

(define-public crate-avance-0.6.4 (c (n "avance") (v "0.6.4") (d (list (d (n "crossterm") (r "^0.25") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "version_check") (r "^0.9.4") (d #t) (k 1)))) (h "1b87havnqpr6qnri5g9h6ha5737rkbiz2i266z9kihvxkgbvdfaf") (r "1.63.0")))

(define-public crate-avance-0.6.5 (c (n "avance") (v "0.6.5") (d (list (d (n "crossterm") (r "^0.25") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "version_check") (r "^0.9.4") (d #t) (k 1)))) (h "0pf10k4jgnrqmxwpm7xs5hs7n9jnm4zw5niz9kihn9m67ja3ln9v") (r "1.63.0")))

