(define-module (crates-io av an avantis-utils-derive) #:use-module (crates-io))

(define-public crate-avantis-utils-derive-0.1.0 (c (n "avantis-utils-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1s7f4w0jy9djcij68bkyh61138ri4ah5hsn277p4xpshy4ryjyyh")))

