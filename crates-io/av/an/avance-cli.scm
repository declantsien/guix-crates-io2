(define-module (crates-io av an avance-cli) #:use-module (crates-io))

(define-public crate-avance-cli-0.1.0 (c (n "avance-cli") (v "0.1.0") (d (list (d (n "avance") (r "^0.1.0") (d #t) (k 0)) (d (n "clap") (r "^4.3.8") (d #t) (k 0)) (d (n "version") (r "^3.0.0") (d #t) (k 0)))) (h "0p7ffg0l457l281ag76i8z36r8svp7nmvmigl5w70vw9hkj4mx7a")))

