(define-module (crates-io av fa avfaudio-sys) #:use-module (crates-io))

(define-public crate-avfaudio-sys-0.1.0 (c (n "avfaudio-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.61.0") (d #t) (k 1)) (d (n "block") (r "^0.1.6") (d #t) (k 0)) (d (n "objc") (r "^0.2.7") (d #t) (k 0)))) (h "0gqhil7f37rn7cl5w7zvdq882x8xpck85v72w5bi9h82n4jbxbfg")))

(define-public crate-avfaudio-sys-0.2.0 (c (n "avfaudio-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.69.1") (d #t) (k 1)) (d (n "block") (r "^0.1.6") (d #t) (k 0)) (d (n "objc") (r "^0.2.7") (d #t) (k 0)))) (h "01qsrslfiwcyf7nignma5h7wdkhdkigbd1r40lchdkda6ss9mdh1")))

(define-public crate-avfaudio-sys-0.2.1 (c (n "avfaudio-sys") (v "0.2.1") (d (list (d (n "bindgen") (r "^0.69.1") (d #t) (k 1)) (d (n "block") (r "^0.1.6") (d #t) (k 0)) (d (n "objc") (r "^0.2.7") (d #t) (k 0)))) (h "01pl325iy5vk6bb2skinlv3xr5a9bfnrx3k27zy9s8f9b4h51zis")))

