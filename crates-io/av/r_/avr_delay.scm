(define-module (crates-io av r_ avr_delay) #:use-module (crates-io))

(define-public crate-avr_delay-0.2.0 (c (n "avr_delay") (v "0.2.0") (h "1jj0bj5ikqzdkjm4f2di16b6sy3nxyxziysqx93730ncx217zzyn")))

(define-public crate-avr_delay-0.2.1 (c (n "avr_delay") (v "0.2.1") (h "1vlkb8n7y90f7cq0hjdhxwvx689d1j8c1085sgkrvwqshri7ik7j")))

(define-public crate-avr_delay-0.3.0 (c (n "avr_delay") (v "0.3.0") (d (list (d (n "avr-config") (r "^1.0") (d #t) (k 0)))) (h "1bcnsm6h71g8rjpagsq2554zxbj5f23kx85mmyn0yp8zdjm26ssw")))

(define-public crate-avr_delay-0.3.1 (c (n "avr_delay") (v "0.3.1") (d (list (d (n "avr-config") (r "^1.0") (d #t) (k 0)))) (h "0dzjwhfd707awn7v0vbnc8yzpd6gq0bjlbvicmak8w3vi7l746hy")))

(define-public crate-avr_delay-0.3.2 (c (n "avr_delay") (v "0.3.2") (d (list (d (n "avr-config") (r "^2.0") (f (quote ("cpu-frequency"))) (d #t) (k 0)) (d (n "avr-std-stub") (r "^1.0") (d #t) (k 2)))) (h "1mzpdszzla0wf19fmikk5zwpcamwnv4ksdn86sb3nipdfr1wlh1j")))

