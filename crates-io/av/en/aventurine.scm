(define-module (crates-io av en aventurine) #:use-module (crates-io))

(define-public crate-aventurine-0.1.0 (c (n "aventurine") (v "0.1.0") (d (list (d (n "glium") (r "^0.18.0") (d #t) (k 0)) (d (n "topaz") (r "^0.2.0") (d #t) (k 0)))) (h "108hs6mjqbbyv29qfgqxxki0ppv7salvkc8dx2g1rfxxvggbdrwh")))

