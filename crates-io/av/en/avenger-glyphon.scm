(define-module (crates-io av en avenger-glyphon) #:use-module (crates-io))

(define-public crate-avenger-glyphon-0.3.0 (c (n "avenger-glyphon") (v "0.3.0") (d (list (d (n "cosmic-text") (r "^0.10") (d #t) (k 0)) (d (n "etagere") (r "^0.2.10") (d #t) (k 0)) (d (n "lru") (r "^0.11") (d #t) (k 0)) (d (n "pollster") (r "^0.3.0") (d #t) (k 2)) (d (n "wgpu") (r "^0.18") (d #t) (k 0)) (d (n "winit") (r "^0.28.7") (d #t) (k 2)))) (h "0q4kqxqcq641v22ywlki5z0fqgap356kj2srsv40iy2kyz1hm4jx")))

