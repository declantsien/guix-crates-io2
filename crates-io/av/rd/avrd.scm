(define-module (crates-io av rd avrd) #:use-module (crates-io))

(define-public crate-avrd-0.1.0 (c (n "avrd") (v "0.1.0") (d (list (d (n "xmltree") (r "^0.4") (d #t) (k 1)))) (h "0knslcsh7plakf2mm54k9vjanxn3bd9pq5yrdhs0qmi2j34mdi3s")))

(define-public crate-avrd-0.1.1 (c (n "avrd") (v "0.1.1") (d (list (d (n "xmltree") (r "^0.4") (d #t) (k 1)))) (h "0g8pmgwvskjfjy3pczxnhxx9a1adz863l3yzg2blxkbrx369afzw")))

(define-public crate-avrd-0.1.2 (c (n "avrd") (v "0.1.2") (d (list (d (n "avr-mcu") (r "^0.1") (d #t) (k 1)))) (h "1rg4x5blmni03x296cqnd2g82y2mrfjxcn60sp6f75s0kax51p32") (f (quote (("default") ("all_mcus"))))))

(define-public crate-avrd-0.2.0 (c (n "avrd") (v "0.2.0") (d (list (d (n "avr-mcu") (r "^0.2") (d #t) (k 1)))) (h "0nlk8npifgzhnq3xv10vigd64wfnrn3ad1h5lkaff2qc1jr99vyr") (f (quote (("default") ("all_mcus"))))))

(define-public crate-avrd-0.3.0 (c (n "avrd") (v "0.3.0") (d (list (d (n "avr-mcu") (r "^0.2") (d #t) (k 1)))) (h "1mbjs80j01461w08xfn3xhmwl31kplivr2j84zg6qa328r9rqh9y") (f (quote (("default") ("all_mcus"))))))

(define-public crate-avrd-0.3.1 (c (n "avrd") (v "0.3.1") (d (list (d (n "avr-mcu") (r "^0.3") (d #t) (k 1)))) (h "0zf1ng3gingj14kj5pibrq4qhyj48lsicmv4svsy0z04ivippz5m") (f (quote (("default") ("all_mcus"))))))

(define-public crate-avrd-1.0.0 (c (n "avrd") (v "1.0.0") (d (list (d (n "avr-mcu") (r "^0.3") (d #t) (k 1)))) (h "086iizkcnayk9jrybb77i0w3sdabc5liqcmgvsa1412wp0mjbc6w") (f (quote (("default") ("all-mcus"))))))

