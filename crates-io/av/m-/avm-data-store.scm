(define-module (crates-io av m- avm-data-store) #:use-module (crates-io))

(define-public crate-avm-data-store-0.1.0 (c (n "avm-data-store") (v "0.1.0") (h "0wzkg1zplg3jm7vv6zw0rg7nc54vcy0jaf4w0xlr2wzinyvvkj6s")))

(define-public crate-avm-data-store-0.2.0 (c (n "avm-data-store") (v "0.2.0") (d (list (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)))) (h "1y7fpq9hh0rhb8b897dl7ic2rqxyv7jzizrsc0xz3alll7xxgsbq")))

(define-public crate-avm-data-store-0.3.0 (c (n "avm-data-store") (v "0.3.0") (d (list (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)))) (h "12xs1fh5hajdyyxvc4jrikaiz9p931islf1zh7sdl32jkaql6wwi")))

(define-public crate-avm-data-store-0.4.0 (c (n "avm-data-store") (v "0.4.0") (d (list (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11.6") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 2)))) (h "0ppq6x5n34ii017s1z0ffkg60g27ghjz1r8vfn0lb3xzh1cy7vzk")))

(define-public crate-avm-data-store-0.4.1 (c (n "avm-data-store") (v "0.4.1") (d (list (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11.7") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 2)))) (h "1641fzrd2v7c8h5a5j00pyna4qhgnbhzhqh2gfsqag5j3lk4z660")))

(define-public crate-avm-data-store-0.5.0 (c (n "avm-data-store") (v "0.5.0") (d (list (d (n "avm-interface") (r "^0.28.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11.9") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.93") (d #t) (k 2)))) (h "0lj47c8ly26z7hmkkpcf5w1rkg09l5kpbkgq8rafwg62ja1lv762")))

(define-public crate-avm-data-store-0.6.0 (c (n "avm-data-store") (v "0.6.0") (d (list (d (n "avm-interface") (r "^0.28.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11.9") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.93") (d #t) (k 2)))) (h "0nf9rj3syypl28qq8vvrqklbww5159v4wradf4h5cyks30111dns")))

(define-public crate-avm-data-store-0.6.1 (c (n "avm-data-store") (v "0.6.1") (d (list (d (n "avm-interface") (r "^0.28.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.155") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11.9") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 2)))) (h "1r9ly0rh0aki0mlz1fd55cjxv5czz1j4wy5a4pd904zd2q4whm8z")))

(define-public crate-avm-data-store-0.6.2 (c (n "avm-data-store") (v "0.6.2") (d (list (d (n "avm-interface") (r "^0.28.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11.9") (d #t) (k 0)))) (h "1pvwk35zn1ab4f4i0vfx63fq4ddldky0kj0az38q80h9ynpd352n")))

(define-public crate-avm-data-store-0.6.3 (c (n "avm-data-store") (v "0.6.3") (d (list (d (n "avm-interface") (r "^0.28.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11.9") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 2)))) (h "01yc4fi7gldy1wshi9h0ag5i6cffd7c12znd570yjx5k1raflj27")))

(define-public crate-avm-data-store-0.7.0 (c (n "avm-data-store") (v "0.7.0") (d (list (d (n "avm-interface") (r "^0.29.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11.9") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 2)))) (h "1100q5z8sn72pfnka035lc4vijbqlw5km8fzn1a9ipi4hmnb27kl")))

(define-public crate-avm-data-store-0.7.1 (c (n "avm-data-store") (v "0.7.1") (d (list (d (n "avm-interface") (r "^0.29.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11.9") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 2)))) (h "080h5qksi7ccvcaahansmhc7j1h9nivh44fjsg13h6l7yvvk80zz")))

(define-public crate-avm-data-store-0.7.2 (c (n "avm-data-store") (v "0.7.2") (d (list (d (n "avm-interface") (r "^0.29.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11.9") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 2)))) (h "05pqi6wg7zac69gwnflzdqfjv9c2nwrglbb3kr0h1hwgd6wnfccb")))

(define-public crate-avm-data-store-0.7.3 (c (n "avm-data-store") (v "0.7.3") (d (list (d (n "avm-interface") (r "^0.29.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.190") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11.9") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 2)))) (h "09zkmrq7nhvk4p9246rxjvzfa3lv3aw2k98r4yfjqf45wpsjpv2v")))

(define-public crate-avm-data-store-0.7.5 (c (n "avm-data-store") (v "0.7.5") (d (list (d (n "avm-interface") (r "^0.31.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.190") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11.9") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 2)))) (h "1w804qkaz8cg5k0wbgv6cp5adds91f7m6kz7jnyap4rps0c9z0x6")))

(define-public crate-avm-data-store-0.7.6 (c (n "avm-data-store") (v "0.7.6") (d (list (d (n "avm-interface") (r "^0.31.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.190") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11.9") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 2)))) (h "11kr943cmcrw9xyky2aypl5lg8ixgw1ad5z8m8bnbg1axnvcyja6")))

(define-public crate-avm-data-store-0.7.7 (c (n "avm-data-store") (v "0.7.7") (d (list (d (n "avm-interface") (r "^0.31.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.190") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11.9") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 2)))) (h "1b0yf791dr5ppxg18ilwiwk3yjwb1mvrn79dzm6bpmac5bh29fw2")))

(define-public crate-avm-data-store-0.7.9 (c (n "avm-data-store") (v "0.7.9") (d (list (d (n "avm-interface") (r "^0.32.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.190") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11.9") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 2)))) (h "1aaskd6jhcn3m8ai0as5kxj0v86jjaq55rv8g4hnfm0kkmvvnazc")))

