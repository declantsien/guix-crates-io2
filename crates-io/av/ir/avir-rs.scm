(define-module (crates-io av ir avir-rs) #:use-module (crates-io))

(define-public crate-avir-rs-0.1.0 (c (n "avir-rs") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.57") (d #t) (k 1)) (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0821yvcbhjyy9ayb776j8ib30544a9qvqrrzl0hacc7clgimbwm2")))

