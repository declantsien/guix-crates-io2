(define-module (crates-io av ir avirus) #:use-module (crates-io))

(define-public crate-avirus-0.2.0 (c (n "avirus") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.2.3") (d #t) (k 0)))) (h "0jkszarzk82vwm4kb47ips43fc5rhzkavjd1a2qhc66ccc2h3fzi")))

(define-public crate-avirus-0.2.1 (c (n "avirus") (v "0.2.1") (d (list (d (n "byteorder") (r "^1.2.3") (d #t) (k 0)))) (h "03gxqbhy2qfmqpnxdb96l4wiy6sz47an7nbvqjxc86phywk4npha")))

(define-public crate-avirus-0.2.2 (c (n "avirus") (v "0.2.2") (d (list (d (n "byteorder") (r "^1.2.3") (d #t) (k 0)))) (h "1h8c46nj3r8iflqi2krpbkhzmhnc0xasq65hhg3pbhxhz2xphlnj")))

(define-public crate-avirus-0.2.3 (c (n "avirus") (v "0.2.3") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)))) (h "0cxcm1h1qb0wmz3kbkgi97lgf11hpb7bq9lsfxcs6xdbms73bl3x")))

(define-public crate-avirus-0.2.4 (c (n "avirus") (v "0.2.4") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)))) (h "1r0zsm8whl7cqj1chf0jd3k2cxb229z4njc9dhss6cax1c139d1z")))

(define-public crate-avirus-0.2.5 (c (n "avirus") (v "0.2.5") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)))) (h "1jy23255h9vvfdg7k789nf6ga968nwc0jfm5gjl051jbxsd9j9ha")))

