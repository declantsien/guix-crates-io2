(define-module (crates-io n- ro n-roman) #:use-module (crates-io))

(define-public crate-n-roman-0.1.0 (c (n "n-roman") (v "0.1.0") (h "1v81ivfi8mdc6rz58pcc97q1n5gwf2c63f0p4ggwydbd4acz0pxb") (y #t)))

(define-public crate-n-roman-0.1.1 (c (n "n-roman") (v "0.1.1") (d (list (d (n "wasm-bindgen") (r "^0.2.84") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.34") (d #t) (k 0)))) (h "09gx8h89jnybcf8qa5ayaqcl6zsjjb5pkcifam7xqzysmn1gdaz2") (y #t)))

(define-public crate-n-roman-0.1.2 (c (n "n-roman") (v "0.1.2") (d (list (d (n "wasm-bindgen") (r "^0.2.84") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.34") (d #t) (k 0)))) (h "1z737qhn6bl6wqhvp6gbr093afh936n3nyq5isjmaahi5p2nryh3")))

