(define-module (crates-io n- mb n-mb) #:use-module (crates-io))

(define-public crate-n-mb-1.1.0 (c (n "n-mb") (v "1.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.4") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "indicatif") (r "^0.17.7") (d #t) (k 0)) (d (n "pbr") (r "^1.1.1") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("macros" "rt-multi-thread" "process" "io-std" "sync" "time" "io-util"))) (d #t) (k 0)))) (h "1sgcy4j782b7fz07hdv6fa1c6vv4i338y7lzkzz34bc1xy3drnkz")))

(define-public crate-n-mb-1.1.1 (c (n "n-mb") (v "1.1.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.4") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "indicatif") (r "^0.17.7") (d #t) (k 0)) (d (n "pbr") (r "^1.1.1") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("macros" "rt-multi-thread" "process" "io-std" "sync" "time" "io-util"))) (d #t) (k 0)))) (h "1anbx70y75s132hcbiyahhydr2xw1kjgdcmqmgm17l4zpj334fj9")))

