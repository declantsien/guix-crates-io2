(define-module (crates-io al ip alipay_macros) #:use-module (crates-io))

(define-public crate-alipay_macros-0.1.0 (c (n "alipay_macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "100ygbkk5q6grrqy1d2hiw8sff9nnyksklahd0kxlsxyymb5qvvq")))

(define-public crate-alipay_macros-0.1.1 (c (n "alipay_macros") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "13dhhxbcbr1acqmf625qcn7mamaf0idj8gly2d254izqnw3lrjfv")))

