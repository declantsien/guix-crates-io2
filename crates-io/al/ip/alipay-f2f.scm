(define-module (crates-io al ip alipay-f2f) #:use-module (crates-io))

(define-public crate-alipay-f2f-0.1.0 (c (n "alipay-f2f") (v "0.1.0") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "rsa") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_urlencoded") (r "^0.7") (d #t) (k 0)) (d (n "sha2") (r "^0.9") (d #t) (k 0)) (d (n "ureq") (r "^1.5") (f (quote ("json"))) (d #t) (k 2)))) (h "0s83qisqz85s16g4zk0c6cb8zf59s0ygvqqb41n1ncrmva5l5any")))

