(define-module (crates-io al ip alipay_params) #:use-module (crates-io))

(define-public crate-alipay_params-0.1.0 (c (n "alipay_params") (v "0.1.0") (d (list (d (n "alipay_macros") (r "^0.1") (d #t) (k 0)))) (h "01lw7pn4mr7a259j7g0vkfdj2m55m7vny5h4rmcqn81m0n77zg98")))

(define-public crate-alipay_params-0.1.1 (c (n "alipay_params") (v "0.1.1") (d (list (d (n "alipay_macros") (r "^0.1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1vdqsd0nddb2zg0mdx2yn0hk5hl1jwvigyb55ylxmnnwxnp9lxv7")))

(define-public crate-alipay_params-0.1.2 (c (n "alipay_params") (v "0.1.2") (d (list (d (n "alipay_macros") (r "^0.1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "002gkpsf4982bg42ghv6v2rkrhriiqn1i4zrq63g3wrg46q01qqx")))

