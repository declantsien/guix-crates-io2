(define-module (crates-io al ip alipay_derive) #:use-module (crates-io))

(define-public crate-alipay_derive-0.1.0 (c (n "alipay_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0bhw8jj2r2wcd34d907v0ysz6dxihlhaiqaabhmapi6c0dvcxbpr") (y #t)))

(define-public crate-alipay_derive-0.1.1 (c (n "alipay_derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "04r0a7lh7d4pg49l5ifrha0b1bjx45c5rdnggyv8nvd0r96b18cj") (y #t)))

