(define-module (crates-io al g- alg-seq) #:use-module (crates-io))

(define-public crate-alg-seq-0.0.1 (c (n "alg-seq") (v "0.0.1") (h "1w9f27gyc3xax3876hnah6i6xr5mixafmldi95v712vb0wv8nyrf")))

(define-public crate-alg-seq-0.0.2 (c (n "alg-seq") (v "0.0.2") (h "0dl4lv4xrp9a5j63kv2i36zz088qpjw86c1mi6rds4414n3ar5h4")))

