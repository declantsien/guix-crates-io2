(define-module (crates-io al g- alg-grid) #:use-module (crates-io))

(define-public crate-alg-grid-0.1.0 (c (n "alg-grid") (v "0.1.0") (d (list (d (n "heapless") (r "^0.5") (d #t) (k 0)) (d (n "libm") (r "^0.2") (d #t) (k 0)) (d (n "map_vec") (r "^0.3") (d #t) (k 0)) (d (n "num-bigint") (r "^0.3") (k 0)) (d (n "num-rational") (r "^0.3") (f (quote ("num-bigint"))) (k 0)))) (h "1n15wcgw4w2iy1sp6csipssmajdny15z105i21bcs6w5vy5w8l3a") (f (quote (("two_dim") ("three_dim") ("reverse_path") ("default" "two_dim"))))))

(define-public crate-alg-grid-0.1.1 (c (n "alg-grid") (v "0.1.1") (d (list (d (n "heapless") (r "^0.5") (d #t) (k 0)) (d (n "libm") (r "^0.2") (d #t) (k 0)) (d (n "map_vec") (r "^0.3") (d #t) (k 0)) (d (n "num-bigint") (r "^0.3") (k 0)) (d (n "num-rational") (r "^0.3") (f (quote ("num-bigint"))) (k 0)))) (h "1bzgsmviwmzpnz26sdqsq83lj46ljynz5p5i07b0skrix92n07kn") (f (quote (("two_dim") ("three_dim") ("reverse_path") ("default" "two_dim"))))))

