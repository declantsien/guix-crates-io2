(define-module (crates-io al la allan-tools) #:use-module (crates-io))

(define-public crate-allan-tools-0.0.1 (c (n "allan-tools") (v "0.0.1") (d (list (d (n "probability") (r "^0.15.8") (d #t) (k 2)) (d (n "rand") (r "^0.3.16") (d #t) (k 2)))) (h "1f1h87s0pf8pc73vcdwzq6phdkv3affnyfzylnxhf63lj1xgsxdq")))

(define-public crate-allan-tools-0.0.2 (c (n "allan-tools") (v "0.0.2") (d (list (d (n "probability") (r "^0.15.8") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.2") (d #t) (k 0)) (d (n "statistical") (r "^1.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0z3q5bbqp6v32j1vbaljzwz16p516c5c93vgf835d414axm2df7d")))

(define-public crate-allan-tools-0.0.3 (c (n "allan-tools") (v "0.0.3") (d (list (d (n "plotters") (r "^0.3.1") (d #t) (k 2)) (d (n "probability") (r "^0.15.8") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.2") (d #t) (k 0)) (d (n "statistical") (r "^1.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0aqj0kkdy5mz99xdqx1rmm11svlr41qgliz6xnpk2ql0h3xmxdvb")))

(define-public crate-allan-tools-0.1.0 (c (n "allan-tools") (v "0.1.0") (d (list (d (n "gnuplot") (r "^0.0.37") (d #t) (k 2)) (d (n "probability") (r "^0.15.8") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.2") (d #t) (k 0)) (d (n "statistical") (r "^1.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "01q2yjfrw3y7y8aaczi995528zh6z6igfnd1sdi36l4mwfhkj1hs")))

(define-public crate-allan-tools-0.1.1 (c (n "allan-tools") (v "0.1.1") (d (list (d (n "gnuplot") (r "^0.0.37") (d #t) (k 2)) (d (n "probability") (r "^0.15.8") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.2") (d #t) (k 0)) (d (n "statistical") (r "^1.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "19i3nzpj3ajxarqlfrvgdrzndxgq8ag69rca2as698paqxwn4wv7")))

(define-public crate-allan-tools-0.1.2 (c (n "allan-tools") (v "0.1.2") (d (list (d (n "gnuplot") (r "^0.0.37") (d #t) (k 2)) (d (n "probability") (r "^0.15.8") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.2") (d #t) (k 0)) (d (n "statistical") (r "^1.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "01yhpkj8c4rhb5shhbcysmi2s5kd8asfl7vrpm4xc6lz26vc7rxc")))

(define-public crate-allan-tools-0.1.3 (c (n "allan-tools") (v "0.1.3") (d (list (d (n "gnuplot") (r "^0.0.37") (d #t) (k 2)) (d (n "probability") (r "^0.15.8") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.2") (d #t) (k 0)) (d (n "statistical") (r "^1.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1f00zjvyy4v0m1yclxqh8x8xv77yn9y6dgya8iwdn6wj5wcdzpga")))

