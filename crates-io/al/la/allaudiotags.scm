(define-module (crates-io al la allaudiotags) #:use-module (crates-io))

(define-public crate-allaudiotags-0.1.0 (c (n "allaudiotags") (v "0.1.0") (d (list (d (n "audiotags") (r "^0.2.7182") (d #t) (k 0)) (d (n "oggvorbismeta") (r "^0.1.0") (d #t) (k 0)))) (h "0pvpwx8l2gdzwc7ar62jsrh0wdr19n8dc43p9vgij7sqzx0b6snc")))

