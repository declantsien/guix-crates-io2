(define-module (crates-io al ic alicedb) #:use-module (crates-io))

(define-public crate-alicedb-0.1.0 (c (n "alicedb") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)))) (h "0mbg8d7yjgx8yw5c8qm4z0j9k2bad73pbxc4dqmsnmf6ybp9z79y")))

(define-public crate-alicedb-0.1.1 (c (n "alicedb") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)))) (h "19ickazdiyvs38qci4b2mrma2dwrnb69vlaa4d6q8s6rqx53njcz")))

(define-public crate-alicedb-0.1.2 (c (n "alicedb") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)))) (h "0sdp2hvkgc429vd6z9in52xnpi4ylp09f5rjrizwrnxbibam4fr3")))

