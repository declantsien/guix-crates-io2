(define-module (crates-io al ic alice) #:use-module (crates-io))

(define-public crate-alice-0.1.0-alpha (c (n "alice") (v "0.1.0-alpha") (d (list (d (n "array-init") (r "^0.1") (d #t) (k 0)) (d (n "assert_cmd") (r "^1") (d #t) (k 2)) (d (n "clap") (r ">=2.33, <2.34") (d #t) (k 0)) (d (n "encode_unicode") (r "^0.3") (d #t) (t "cfg(windows)") (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "rug") (r "^1.9") (f (quote ("integer"))) (k 0)) (d (n "time") (r "^0.2") (f (quote ("std"))) (k 0)))) (h "1cqyjzfb7lrxpzd94xmbargfgkql01c8y4dsdw7kajvr4fkmkl8i")))

(define-public crate-alice-0.1.0-alpha.1 (c (n "alice") (v "0.1.0-alpha.1") (d (list (d (n "array-init") (r "^0.1") (d #t) (k 0)) (d (n "assert_cmd") (r "^1") (d #t) (k 2)) (d (n "clap") (r ">= 2.33, < 2.34") (d #t) (k 0)) (d (n "encode_unicode") (r "^0.3") (d #t) (t "cfg(windows)") (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "rug") (r "^1.9") (f (quote ("integer"))) (k 0)) (d (n "time") (r "^0.2") (f (quote ("std"))) (k 0)))) (h "1wvdk8ffc2xnx9a1hncf2xwxsxz79nphm78rx7lbrssmd2x30xc2")))

