(define-module (crates-io al ic alice-sys) #:use-module (crates-io))

(define-public crate-alice-sys-0.1.0 (c (n "alice-sys") (v "0.1.0") (d (list (d (n "alice-open-data") (r "^0.1.0") (d #t) (k 0)) (d (n "bindgen") (r "^0.29.0") (d #t) (k 1)) (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2.36") (d #t) (k 0)))) (h "0saxp3dxbcn0r5crslaywz6j2zj19smzfx5bzv619rc4ysppjfsv")))

(define-public crate-alice-sys-0.1.1 (c (n "alice-sys") (v "0.1.1") (d (list (d (n "alice-open-data") (r "^0.1.0") (d #t) (k 0)) (d (n "bindgen") (r "^0.29.0") (d #t) (k 1)) (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2.36") (d #t) (k 0)))) (h "0qgmh33rh1p3qwh19h3kjis137r8hfxsmcj34vjvd2fpqqlmgshv")))

