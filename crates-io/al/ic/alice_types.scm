(define-module (crates-io al ic alice_types) #:use-module (crates-io))

(define-public crate-alice_types-0.1.0 (c (n "alice_types") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0y4snh2bvg49mjcl1cqk04ai2cym6rf0l6zc3fbf68zwc82csa99") (y #t)))

(define-public crate-alice_types-0.1.1 (c (n "alice_types") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0h7a0qrq3s68wzb67hyq9mzy8zbincmbwkrn90vqvmi0yvqfkfg3") (y #t)))

(define-public crate-alice_types-0.2.0 (c (n "alice_types") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "08mzj324i92kxf4ldm2isc5r1b7677r4pz4lzp4g05csl26msb9c") (y #t)))

(define-public crate-alice_types-0.0.2 (c (n "alice_types") (v "0.0.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0v1yc49bz73sv3rvdipn29bf7ql5wbidyxv04l3plifwsc9qhbxs")))

(define-public crate-alice_types-0.0.3 (c (n "alice_types") (v "0.0.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0g5fcwpfzcqlgnshs5qh4w28z52r51vkna3qp18sdksjkmc10ri4")))

(define-public crate-alice_types-0.0.4 (c (n "alice_types") (v "0.0.4") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "13xwax51gs4a6dpd6rz7bzqsl0dmn4pviraxh2vvlq59frnzsv3s")))

(define-public crate-alice_types-0.1.2 (c (n "alice_types") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0982aqzr3ajhxsag0rnbkcp8l0bzzrimy4dljzsvw42r8padjry9") (y #t)))

(define-public crate-alice_types-0.2.1 (c (n "alice_types") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1xddm4pazcr1d25b8183fg99ai3a9x6q7zc9nicvb1vws2rm6sf6")))

(define-public crate-alice_types-0.3.0 (c (n "alice_types") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0hlpmz3sv5212vpkhj4zcy0jfd46crr7v1lsswbg3y2d5shay50c")))

(define-public crate-alice_types-0.3.1 (c (n "alice_types") (v "0.3.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1hzvavvmm08jp2xzqf1wji43aly5wrkqlda1mzqfky2dsfgc82qs")))

