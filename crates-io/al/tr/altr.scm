(define-module (crates-io al tr altr) #:use-module (crates-io))

(define-public crate-altr-0.1.0 (c (n "altr") (v "0.1.0") (h "10nw6nszqdkbnli306m1nfjjksqiw8mmdv4hr4l6ii7ch3xxvrwn")))

(define-public crate-altr-0.2.0 (c (n "altr") (v "0.2.0") (d (list (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)))) (h "0vypnfn7awq3b62sbdikhv95s99zbsdxjlmjqas6qv165klxl19r")))

(define-public crate-altr-0.2.1 (c (n "altr") (v "0.2.1") (d (list (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)))) (h "0ga3902gz6kpdpzxaca34c7930p6ypi7k8s2xj6b033lbkkjw69j")))

(define-public crate-altr-0.3.0 (c (n "altr") (v "0.3.0") (d (list (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "10894kawi47mn6nswifvbvk61zx7in0ghbdbgb995nqhsgzys58v")))

(define-public crate-altr-0.3.1 (c (n "altr") (v "0.3.1") (d (list (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "0b17l4dbvxvq1l31sz6y5h7agmr8fs5ww7msyszkr1is39gqgr8k")))

(define-public crate-altr-0.3.2 (c (n "altr") (v "0.3.2") (d (list (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "0rsn269csk2giqzy0466n98yii5h84ihgq6s34jb0h6448gr1qdw")))

(define-public crate-altr-0.3.3 (c (n "altr") (v "0.3.3") (d (list (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "11k175vnk0ya3jm8572p2pnqx2kdyr2cir9rnc8bml9d8pj6cyja")))

(define-public crate-altr-0.3.4 (c (n "altr") (v "0.3.4") (d (list (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "14la8qprg2di3qn4jc0m6fck2byla9rf4c6s5s64cigf8jwikkq4")))

(define-public crate-altr-0.3.5 (c (n "altr") (v "0.3.5") (d (list (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "0dcsfy7md1j87idg7wnzsxv5xcpbfs8ng2074rv5nbjm7fq6s53d")))

(define-public crate-altr-0.4.0 (c (n "altr") (v "0.4.0") (d (list (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "0f0jibwb2kz2hcwqz82jy30j2alndqzx4p59pv85dw445lzkxjzh")))

(define-public crate-altr-0.5.0 (c (n "altr") (v "0.5.0") (d (list (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "1mr5yy93vvvxx4cm6fi00ixd3g62cq8b9wp7vwhmr7f8mzll7qr2")))

(define-public crate-altr-0.5.1 (c (n "altr") (v "0.5.1") (d (list (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "0p7svbicqdfz55d4616g7i7jq8n23f19myqldbc9ag3kk945hdkx")))

