(define-module (crates-io al to alto_logger) #:use-module (crates-io))

(define-public crate-alto_logger-0.1.0 (c (n "alto_logger") (v "0.1.0") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "once_cell") (r "^1.3.1") (d #t) (k 0)) (d (n "termcolor") (r "^1.1.0") (d #t) (k 0)))) (h "19bpi8kid5l8i7vcz2w8hq5w8bd1vfhndbhvp1yxxfl0zcgz0ldd")))

(define-public crate-alto_logger-0.1.1 (c (n "alto_logger") (v "0.1.1") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "once_cell") (r "^1.3.1") (d #t) (k 0)) (d (n "termcolor") (r "^1.1.0") (d #t) (k 0)))) (h "0rn0cbjaijgl42xij9zzfdwxxn1dw9pizmgqq515s8r46fq6mpz8")))

(define-public crate-alto_logger-0.1.2 (c (n "alto_logger") (v "0.1.2") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "once_cell") (r "^1.3.1") (d #t) (k 0)) (d (n "termcolor") (r "^1.1.0") (d #t) (k 0)))) (h "0daxplq8wf2m4jdiffwkax6mj37p27agxr8p9mkl8i0b0kl84cwm")))

(define-public crate-alto_logger-0.2.0 (c (n "alto_logger") (v "0.2.0") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 2)) (d (n "termcolor") (r "^1.1") (d #t) (k 0)) (d (n "time") (r "^0.2") (f (quote ("std"))) (o #t) (k 0)))) (h "0j4qb85cpbaqdf9b5ck937yckkfvvgnlmbimyp2hd8n66a8av5rb")))

(define-public crate-alto_logger-0.3.0 (c (n "alto_logger") (v "0.3.0") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 2)) (d (n "termcolor") (r "^1.1") (d #t) (k 0)) (d (n "time") (r "^0.2") (f (quote ("std"))) (o #t) (k 0)))) (h "1qpghcp211pvzq704zw68bmsh7lq32m2dlkbgw68lr6idk3pgxv7")))

(define-public crate-alto_logger-0.3.1 (c (n "alto_logger") (v "0.3.1") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 2)) (d (n "termcolor") (r "^1.1") (d #t) (k 0)) (d (n "time") (r "^0.2") (f (quote ("std"))) (o #t) (k 0)))) (h "1ajpxjwwm9mxx0ldzzgqpng2fca5gcs400yd3jyakpsgr9n0vksr")))

(define-public crate-alto_logger-0.3.2 (c (n "alto_logger") (v "0.3.2") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 2)) (d (n "termcolor") (r "^1.1") (d #t) (k 0)) (d (n "time") (r "^0.2") (f (quote ("std"))) (o #t) (k 0)))) (h "15qs2zq358yss7bqphwgc6b0r553pz60v3jnkmxlasn9vzhqwqs0")))

(define-public crate-alto_logger-0.3.3 (c (n "alto_logger") (v "0.3.3") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "termcolor") (r "^1.1") (d #t) (k 0)) (d (n "time") (r "^0.2") (f (quote ("std"))) (o #t) (k 0)))) (h "0aij20fgcfpc7f77sy0pads8vpir7hyh9z68yx9xfzkvsd1q8yl4")))

(define-public crate-alto_logger-0.3.4 (c (n "alto_logger") (v "0.3.4") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "termcolor") (r "^1.1") (d #t) (k 0)) (d (n "time") (r "^0.2") (f (quote ("std"))) (o #t) (k 0)))) (h "00r0jw8lhk9vdpav3nmgivifransbgzv7ndivcldb83axm4cfdn4")))

(define-public crate-alto_logger-0.3.5 (c (n "alto_logger") (v "0.3.5") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "termcolor") (r "^1.1") (d #t) (k 0)) (d (n "time") (r "^0.2") (f (quote ("std"))) (o #t) (k 0)))) (h "0xn1jrdbjm346vwgg6gwq915bycy3di9yz5ag66xshmiml4pbd0b")))

(define-public crate-alto_logger-0.3.6 (c (n "alto_logger") (v "0.3.6") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "termcolor") (r "^1.1") (d #t) (k 0)) (d (n "time") (r "^0.2") (f (quote ("std"))) (o #t) (k 0)))) (h "0h9h0mca9933l2i4q3a4p3slp4ihpdiiqnmbw6207ck8ymya4w2s")))

(define-public crate-alto_logger-0.3.7 (c (n "alto_logger") (v "0.3.7") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "termcolor") (r "^1.1") (d #t) (k 0)) (d (n "time") (r "^0.2") (f (quote ("std"))) (o #t) (k 0)))) (h "0awn1yr7kin0sl0x7j1ax79a46c0wrd7af7kwa0gc8bxwzkb0ybr")))

(define-public crate-alto_logger-0.4.0 (c (n "alto_logger") (v "0.4.0") (d (list (d (n "doc-comment") (r "^0.3.3") (d #t) (k 2)) (d (n "log") (r "^0.4.17") (f (quote ("std"))) (d #t) (k 0)) (d (n "log") (r "^0.4.17") (f (quote ("std"))) (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "termcolor") (r "^1.1.3") (d #t) (k 0)) (d (n "time") (r "^0.3.9") (f (quote ("std" "parsing" "formatting"))) (o #t) (k 0)) (d (n "time") (r "^0.3.9") (f (quote ("macros"))) (d #t) (k 2)))) (h "1pbqg38qf9816ni0a73f8fd4pr50k94v51c0f20nvsz6n7nfx79x")))

