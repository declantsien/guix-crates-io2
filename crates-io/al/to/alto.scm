(define-module (crates-io al to alto) #:use-module (crates-io))

(define-public crate-alto-0.1.0 (c (n "alto") (v "0.1.0") (d (list (d (n "al-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)))) (h "1glcwqarkkw8n15wqhnr3rk68ha516qji2l03f4fzzwlrrxpfnk6") (y #t)))

(define-public crate-alto-0.1.1 (c (n "alto") (v "0.1.1") (d (list (d (n "al-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)))) (h "1ifmmcnk6xn3339avz8r63njzbk2n70n7ii6zrs99adsic90bgj2") (y #t)))

(define-public crate-alto-0.2.0 (c (n "alto") (v "0.2.0") (d (list (d (n "al-sys") (r "^0.3.1") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)) (d (n "rental") (r "^0.3.2") (d #t) (k 0)))) (h "0plhl9386r424gxls1l6f70z6s3rzvky35jl2wmyvw6sawyp18l5") (y #t)))

(define-public crate-alto-0.2.1 (c (n "alto") (v "0.2.1") (d (list (d (n "al-sys") (r "^0.3.1") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)) (d (n "rental") (r "^0.3.2") (d #t) (k 0)))) (h "0l39zii9v3ri60xnfv1gg8jq5j8fnmky4w82lln7kwdf0n23x2ww") (y #t)))

(define-public crate-alto-0.2.2 (c (n "alto") (v "0.2.2") (d (list (d (n "al-sys") (r "^0.3.1") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)) (d (n "rental") (r "^0.3.2") (d #t) (k 0)))) (h "1j91mhrrmjhvyfk4s1y7g2c7yqj66369lmncy3j20da4cyh3hx4n") (y #t)))

(define-public crate-alto-0.2.3 (c (n "alto") (v "0.2.3") (d (list (d (n "al-sys") (r "^0.3.1") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)) (d (n "rental") (r "^0.3.2") (d #t) (k 0)))) (h "099rf472jqfb0fs1liqds23njy0r3pjdxjpp501jymy3w69jjy7d") (y #t)))

(define-public crate-alto-0.2.4 (c (n "alto") (v "0.2.4") (d (list (d (n "al-sys") (r "^0.3.1") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)) (d (n "rental") (r "^0.3.2") (d #t) (k 0)))) (h "0l6qlb367p37y95761mrwnw8y1sqczjgm9qyd1wlzknph810fk4a") (y #t)))

(define-public crate-alto-0.3.0 (c (n "alto") (v "0.3.0") (d (list (d (n "al-sys") (r "^0.3.1") (d #t) (k 0)) (d (n "enum_primitive") (r "^0.1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)) (d (n "rental") (r "^0.3.2") (d #t) (k 0)))) (h "0f3l22z07swxd3fcjbid6r787fqv5y7zap5m4pl16k91f30x61yl") (y #t)))

(define-public crate-alto-0.3.1 (c (n "alto") (v "0.3.1") (d (list (d (n "al-sys") (r "^0.3.1") (d #t) (k 0)) (d (n "enum_primitive") (r "^0.1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)) (d (n "rental") (r "^0.3.2") (d #t) (k 0)))) (h "060bhg9vipvfj383qzm11ga5n906ix5wmpm1jb1bc0aq8g5ikmqh") (y #t)))

(define-public crate-alto-1.0.0 (c (n "alto") (v "1.0.0") (d (list (d (n "al-sys") (r "^0.3.1") (d #t) (k 0)) (d (n "enum_primitive") (r "^0.1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)) (d (n "rental") (r "^0.3.2") (d #t) (k 0)))) (h "1a2883c2fwgzwxzqh2bkbxbrhalfb4i3h682k4vd653h3jny0s3q") (y #t)))

(define-public crate-alto-1.0.1 (c (n "alto") (v "1.0.1") (d (list (d (n "al-sys") (r "^0.3.1") (d #t) (k 0)) (d (n "enum_primitive") (r "^0.1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)) (d (n "rental") (r "^0.3.2") (d #t) (k 0)))) (h "02ki444snzza2piazqsi69xahqlinhdfsrdirqlyxpsnw2apyjx5") (y #t)))

(define-public crate-alto-1.0.2 (c (n "alto") (v "1.0.2") (d (list (d (n "al-sys") (r "^0.3.1") (d #t) (k 0)) (d (n "enum_primitive") (r "^0.1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)) (d (n "rental") (r "^0.3.2") (d #t) (k 0)))) (h "0q329f43y75aq6774sjqwyhs12ds8zvhigaa8yqcpn1zrcgf261w") (y #t)))

(define-public crate-alto-1.0.3 (c (n "alto") (v "1.0.3") (d (list (d (n "al-sys") (r "^0.3.1") (d #t) (k 0)) (d (n "enum_primitive") (r "^0.1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)) (d (n "rental") (r "^0.3.2") (d #t) (k 0)))) (h "1960d5xknzddqqgcpwmj1rxxhzhf22lsgphw3lhrvk4swr5qsb88") (y #t)))

(define-public crate-alto-1.0.4 (c (n "alto") (v "1.0.4") (d (list (d (n "al-sys") (r "^0.3.1") (d #t) (k 0)) (d (n "enum_primitive") (r "^0.1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)) (d (n "rental") (r "^0.3.2") (d #t) (k 0)))) (h "17lxhpany37mlyqidx513c3nxjg7mg2b51s0kd0jdmmpr3gj8sds") (y #t)))

(define-public crate-alto-1.0.5 (c (n "alto") (v "1.0.5") (d (list (d (n "al-sys") (r "^0.3.1") (d #t) (k 0)) (d (n "enum_primitive") (r "^0.1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)) (d (n "rental") (r "^0.3.2") (d #t) (k 0)))) (h "07xw1rjwin9k2pvmbvk5basrpv26yq9qb2qbmnqk4f1162zvzr5y") (y #t)))

(define-public crate-alto-1.0.6 (c (n "alto") (v "1.0.6") (d (list (d (n "al-sys") (r "^0.3.1") (d #t) (k 0)) (d (n "enum_primitive") (r "^0.1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)) (d (n "rental") (r "^0.3.2") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.5") (d #t) (k 0)))) (h "0mglr9m2q88p90dzvi78wib0pf9dmdr9ivb5dps2xdil3ycza7gb") (y #t)))

(define-public crate-alto-1.1.0 (c (n "alto") (v "1.1.0") (d (list (d (n "al-sys") (r "^0.3.1") (d #t) (k 0)) (d (n "enum_primitive") (r "^0.1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)) (d (n "rental") (r "^0.3.2") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.5") (d #t) (k 0)))) (h "1h4f49ajxi6awkl8v7p9r40wdvynj8vdsl449hy3mzrm4l9jwr9f") (y #t)))

(define-public crate-alto-1.1.1 (c (n "alto") (v "1.1.1") (d (list (d (n "al-sys") (r "^0.4.0") (d #t) (k 0)) (d (n "enum_primitive") (r "^0.1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)) (d (n "rental") (r "^0.3.2") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.5") (d #t) (k 0)))) (h "0sbpqpfby642sk1ajg6n7v20b238l0ppr5nsc8ah2y5lzccpb08m") (y #t)))

(define-public crate-alto-1.1.2 (c (n "alto") (v "1.1.2") (d (list (d (n "al-sys") (r "^0.4.0") (d #t) (k 0)) (d (n "enum_primitive") (r "^0.1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)) (d (n "rental") (r "^0.4.6") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.5") (d #t) (k 0)))) (h "0g22r0kqlmgjicxabrz9y74hf4kgp56vqymwr27lf89nkf8442zm") (y #t)))

(define-public crate-alto-1.1.3 (c (n "alto") (v "1.1.3") (d (list (d (n "al-sys") (r "^0.4.0") (d #t) (k 0)) (d (n "enum_primitive") (r "^0.1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)) (d (n "rental") (r "^0.4.6") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.5") (d #t) (k 0)))) (h "05cfjvwc55pv2qjgff1wnavzm7z5biirdfnxdc0fxdl0zwsshrd7") (y #t)))

(define-public crate-alto-1.1.4 (c (n "alto") (v "1.1.4") (d (list (d (n "al-sys") (r "^0.4.0") (d #t) (k 0)) (d (n "enum_primitive") (r "^0.1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)) (d (n "rental") (r "^0.4.6") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.5") (d #t) (k 0)))) (h "0dgmmgyym63i3zhlzi7d21rl3xy0bxfz5pkml3v6f7zpb9d3nbpm")))

(define-public crate-alto-2.0.0 (c (n "alto") (v "2.0.0") (d (list (d (n "al-sys") (r "^0.5.1") (d #t) (k 0)) (d (n "enum_primitive") (r "^0.1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)))) (h "0brzsx18gj3jylqzsdd2rq33r8xx59izy1ssafg783pfrd92hhxx")))

(define-public crate-alto-2.0.1 (c (n "alto") (v "2.0.1") (d (list (d (n "al-sys") (r "^0.5.1") (d #t) (k 0)) (d (n "enum_primitive") (r "^0.1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)))) (h "1slmzfr536jfmfa2snpwpv8l0g0h88xlb3khyd4wzffxwmjf7pdz")))

(define-public crate-alto-2.0.2 (c (n "alto") (v "2.0.2") (d (list (d (n "al-sys") (r "^0.5.1") (d #t) (k 0)) (d (n "enum_primitive") (r "^0.1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)))) (h "0rhqal8l66ljbcbc3hdjbw7yjzrvi0c3v8k7igih60h521mlg764")))

(define-public crate-alto-2.0.3 (c (n "alto") (v "2.0.3") (d (list (d (n "al-sys") (r "^0.5.1") (d #t) (k 0)) (d (n "enum_primitive") (r "^0.1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)))) (h "0fqi1w973s91aqv6whxgrha5f65cgss8vib8h1q04wizxrbhw7d1")))

(define-public crate-alto-2.0.4 (c (n "alto") (v "2.0.4") (d (list (d (n "al-sys") (r "^0.5.1") (d #t) (k 0)) (d (n "enum_primitive") (r "^0.1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)))) (h "13zh53ra9d49l9lz8v8jclbdrls1r97l91fa6p4bzi9yffr73p4q")))

(define-public crate-alto-2.0.5 (c (n "alto") (v "2.0.5") (d (list (d (n "al-sys") (r "^0.5.1") (d #t) (k 0)) (d (n "enum_primitive") (r "^0.1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.4.4") (d #t) (k 0)))) (h "0fx3qpd0zpdypd424x58wccxzqkknm32dswj2wylg4hpc2785vkg")))

(define-public crate-alto-2.0.6 (c (n "alto") (v "2.0.6") (d (list (d (n "al-sys") (r "^0.5.1") (d #t) (k 0)) (d (n "enum_primitive") (r "^0.1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.4.4") (d #t) (k 0)))) (h "1gwancv26nqdd1gdbcs4gjr5kn85gv6qjg03rlwv9n042l45x7f7")))

(define-public crate-alto-3.0.0 (c (n "alto") (v "3.0.0") (d (list (d (n "al-sys") (r "^0.5.1") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.4.4") (d #t) (k 0)))) (h "06y9f2skrxlmzaidn0dpghzaqgm7cxw1k9amflg0ylwq2rfh035d")))

(define-public crate-alto-3.0.1 (c (n "alto") (v "3.0.1") (d (list (d (n "al-sys") (r "^0.5.1") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.4.4") (d #t) (k 0)))) (h "14qqna8m1yrgk1pi4kajnnzd6r7ki593z3n5if5l57z595s8jcmm")))

(define-public crate-alto-3.0.2 (c (n "alto") (v "3.0.2") (d (list (d (n "al-sys") (r "^0.5.1") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.4.4") (d #t) (k 0)))) (h "0kzpy9wprav7a9kzmxwjsnmf19dgfkhkj8ryifijg235brvxm529")))

(define-public crate-alto-3.0.3 (c (n "alto") (v "3.0.3") (d (list (d (n "al-sys") (r "^0.6.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.4.4") (d #t) (k 0)))) (h "1a01narjgvn1r2jppr3cnm4qhhwmqxr5jnagvs53qyvipqww8jdl")))

(define-public crate-alto-3.0.4 (c (n "alto") (v "3.0.4") (d (list (d (n "al-sys") (r "^0.6.1") (k 0)) (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.4.4") (d #t) (k 0)))) (h "1rgsdmh346s3rwhzqacjc6nz7jap4dd72c1gfmkaq9sgzh9fhnyp") (f (quote (("dynamic" "al-sys/dynamic") ("default" "dynamic"))))))

