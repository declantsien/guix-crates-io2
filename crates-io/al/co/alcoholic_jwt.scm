(define-module (crates-io al co alcoholic_jwt) #:use-module (crates-io))

(define-public crate-alcoholic_jwt-0.1.0 (c (n "alcoholic_jwt") (v "0.1.0") (d (list (d (n "base64") (r "^0.9") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1d9cvwf4vmq2f5jvli8d97q89sfh93syiszf8rshwpvmzrnz71wa")))

(define-public crate-alcoholic_jwt-1.0.0 (c (n "alcoholic_jwt") (v "1.0.0") (d (list (d (n "base64") (r "^0.10") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1nrqknr323hdsfvzh4cvncv3bkzimbsn4y568c23m0ql2kh2cwnj")))

(define-public crate-alcoholic_jwt-1.0.1 (c (n "alcoholic_jwt") (v "1.0.1") (d (list (d (n "base64") (r "^0.10") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0ybm9wqfhqkb537gw2mqr2cbydiw3dx68j9zmwa9b04k76ym5qsa")))

(define-public crate-alcoholic_jwt-4084.0.0 (c (n "alcoholic_jwt") (v "4084.0.0") (d (list (d (n "base64") (r "^0.10") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0mmizpa01zbgac4ihwi7djqlmwpl1shkjcjjc3ri9rvczs2v9km7")))

(define-public crate-alcoholic_jwt-4091.0.0 (c (n "alcoholic_jwt") (v "4091.0.0") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "17lv05285c7ncmqpjswzvba308spwrcxkxyhl9qscabpmiq152pv")))

