(define-module (crates-io al oo alookup) #:use-module (crates-io))

(define-public crate-alookup-0.0.0 (c (n "alookup") (v "0.0.0") (h "0ii7ikmlv3bmg3yzhxdiypxyk44ijc3fj2yccznjrpg2k1c5xxbk")))

(define-public crate-alookup-0.1.0 (c (n "alookup") (v "0.1.0") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "resolv") (r "^0.1.4") (d #t) (k 0)))) (h "17377jjmggwbpgkvybas86iwgycgw0r6dc20gdi11586aabr8fb0")))

(define-public crate-alookup-0.1.1 (c (n "alookup") (v "0.1.1") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "resolv") (r "^0.1.4") (d #t) (k 0)))) (h "1s0l8c18jirk1ni68pqqny6xznkjl7i5qpyvr0vxd05wmxz7f16z")))

(define-public crate-alookup-0.2.0 (c (n "alookup") (v "0.2.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "resolv") (r "^0.1.4") (d #t) (k 0)))) (h "1v8k2f5xnq495gjm7c9092fc66jvc6pvla9fc7a0m6pifwxj7wc5")))

