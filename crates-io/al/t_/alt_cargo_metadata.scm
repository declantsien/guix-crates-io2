(define-module (crates-io al t_ alt_cargo_metadata) #:use-module (crates-io))

(define-public crate-alt_cargo_metadata-0.9.1 (c (n "alt_cargo_metadata") (v "0.9.1") (d (list (d (n "clap") (r "^2.26.0") (d #t) (k 2)) (d (n "docopt") (r "^1.0.2") (d #t) (k 2)) (d (n "semver") (r "^0.9") (f (quote ("serde"))) (d #t) (k 0) (p "alt_semver")) (d (n "serde") (r "^1.0.79") (d #t) (k 0) (p "alt_serde")) (d (n "serde_derive") (r "^1.0.79") (d #t) (k 0) (p "alt_serde_derive")) (d (n "serde_json") (r "^1.0.1") (d #t) (k 0) (p "alt_serde_json")) (d (n "structopt") (r "^0.2.10") (d #t) (k 2)))) (h "1dd05vd0c6bpcrw2nr4gwahbja5id1fdq04cbniaanmv2h7wawr3") (f (quote (("default"))))))

