(define-module (crates-io al t_ alt_serde_derive) #:use-module (crates-io))

(define-public crate-alt_serde_derive-1.0.104 (c (n "alt_serde_derive") (v "1.0.104") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("visit"))) (d #t) (k 0)))) (h "189q50akcmswdi6sf7zj6r167nqv5rx7mk9p33xgpk42m9gysmv7") (f (quote (("deserialize_in_place") ("default"))))))

(define-public crate-alt_serde_derive-1.0.119 (c (n "alt_serde_derive") (v "1.0.119") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0.58") (f (quote ("visit"))) (d #t) (k 0)))) (h "18x9x5q0v2r6g18nsgd8wh9s3plf1y3qh6jjx4krlpb7nbsxm7b2") (f (quote (("deserialize_in_place") ("default"))))))

