(define-module (crates-io al t_ alt_serde) #:use-module (crates-io))

(define-public crate-alt_serde-1.0.104 (c (n "alt_serde") (v "1.0.104") (d (list (d (n "serde_derive") (r "= 1.0.104") (o #t) (d #t) (k 0) (p "alt_serde_derive")) (d (n "serde_derive") (r "^1.0") (d #t) (k 2) (p "alt_serde_derive")))) (h "11i6fq71vanbh34v4cxavadzskz38sx7hvzjvsb9p209vpgfvgh3") (f (quote (("unstable") ("std") ("rc") ("derive" "serde_derive") ("default" "std") ("alloc"))))))

(define-public crate-alt_serde-1.0.119 (c (n "alt_serde") (v "1.0.119") (d (list (d (n "serde_derive") (r "=1.0.119") (o #t) (d #t) (k 0) (p "alt_serde_derive")) (d (n "serde_derive") (r "^1.0") (d #t) (k 2) (p "alt_serde_derive")))) (h "0zjkm90bjra1r774l8dlrxffir0d5gzbfax3jwsjx5y9dgr0ipdv") (f (quote (("unstable") ("std") ("rc") ("derive" "serde_derive") ("default" "std") ("alloc"))))))

