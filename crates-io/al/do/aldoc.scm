(define-module (crates-io al do aldoc) #:use-module (crates-io))

(define-public crate-aldoc-0.1.0 (c (n "aldoc") (v "0.1.0") (d (list (d (n "clap") (r "^3.0.0-beta.1") (d #t) (k 0)) (d (n "nom") (r "^5") (d #t) (k 0)) (d (n "numerals") (r "^0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1r32h7m1s9i8lw9rrfw3a43lxmgk292r853m6f9ikv7q4g9bv1ww")))

(define-public crate-aldoc-0.1.1 (c (n "aldoc") (v "0.1.1") (d (list (d (n "clap") (r "^3.0.0-beta.1") (d #t) (k 0)) (d (n "nom") (r "^5") (d #t) (k 0)) (d (n "numerals") (r "^0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1bh2v5257ljg51h30v5l8a2zqhaaszln42j26zragga5bl4z1qqh")))

(define-public crate-aldoc-0.1.2 (c (n "aldoc") (v "0.1.2") (d (list (d (n "clap") (r "^3.0.0-beta.1") (d #t) (k 0)) (d (n "nom") (r "^5") (d #t) (k 0)) (d (n "numerals") (r "^0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0ypzd8j2273s1pv40n1lc0chm8jls0xk7rfh8l0rsdxh571ib4an")))

(define-public crate-aldoc-0.1.3 (c (n "aldoc") (v "0.1.3") (d (list (d (n "clap") (r "^3.0.0-beta.1") (d #t) (k 0)) (d (n "nom") (r "^5") (d #t) (k 0)) (d (n "numerals") (r "^0.1") (d #t) (k 0)) (d (n "tectonic") (r "^0.1.15") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1ankx8h8a5lqp9kv7nvg6zhvr002dziacy0302bznz95z919mbv9")))

(define-public crate-aldoc-0.1.4 (c (n "aldoc") (v "0.1.4") (d (list (d (n "clap") (r "^3.0.0-beta.1") (d #t) (k 0)) (d (n "nom") (r "^5") (d #t) (k 0)) (d (n "numerals") (r "^0.1") (d #t) (k 0)) (d (n "tectonic") (r "^0.1.15") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1p2plvlv9h5lwr0gjx6yqngp57iqjcn8gl5if33am3z53zb5bp4m")))

(define-public crate-aldoc-0.2.0 (c (n "aldoc") (v "0.2.0") (d (list (d (n "clap") (r "^3.0.0-beta.1") (d #t) (k 0)) (d (n "nom") (r "^5") (d #t) (k 0)) (d (n "numerals") (r "^0.1") (d #t) (k 0)) (d (n "tectonic") (r "^0.1.15") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1j8dldrixqfs1argr3xma935579wrpq3kvjgnb87ssmzijladxlb")))

