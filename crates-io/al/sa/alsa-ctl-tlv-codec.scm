(define-module (crates-io al sa alsa-ctl-tlv-codec) #:use-module (crates-io))

(define-public crate-alsa-ctl-tlv-codec-0.1.0 (c (n "alsa-ctl-tlv-codec") (v "0.1.0") (h "12z1jz0xllp9wvxlghzrlasx6mj2k3r4ql2svh6rhj9p5nwws9ki")))

(define-public crate-alsa-ctl-tlv-codec-0.1.1 (c (n "alsa-ctl-tlv-codec") (v "0.1.1") (h "1zvwa7ga7y59x4wg46922rcpid4qvs9vfkb2qs35a4lvz1fbk6l4")))

