(define-module (crates-io al sa alsa) #:use-module (crates-io))

(define-public crate-alsa-0.0.2 (c (n "alsa") (v "0.0.2") (d (list (d (n "alsa-sys") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "15b1g6khzw3cwilfj5qpc1gjvkrin4xqlhljl46fcbi373s2z69a")))

(define-public crate-alsa-0.0.3 (c (n "alsa") (v "0.0.3") (d (list (d (n "alsa-sys") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "18727i8d0mlakh0c6ykschzlp42y20qycy35dhm6ya277pilmbgq")))

(define-public crate-alsa-0.0.4 (c (n "alsa") (v "0.0.4") (d (list (d (n "alsa-sys") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "0gafs1rqc1hhwmb08zbi5isr328l2n6a48lx558igbjn38wld513")))

(define-public crate-alsa-0.0.5 (c (n "alsa") (v "0.0.5") (d (list (d (n "alsa-sys") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "1piwpyv89b0v53viyg16v873c5190fhahv62bap7vwkqv4f1x9g2")))

(define-public crate-alsa-0.0.6 (c (n "alsa") (v "0.0.6") (d (list (d (n "alsa-sys") (r "*") (d #t) (k 0)) (d (n "bitflags") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "14mcc4rlw4gccrvzql1idwpspbllnzqph3f6f44mqqxnqpnxq91w")))

(define-public crate-alsa-0.1.0 (c (n "alsa") (v "0.1.0") (d (list (d (n "alsa-sys") (r "^0.0.8") (d #t) (k 0)) (d (n "bitflags") (r "^0.5") (d #t) (k 0)) (d (n "libc") (r "^0.2.7") (d #t) (k 0)))) (h "1lzwfqi1rdyfaaq7kxh6m9cwmrhzaw4px8b0gl390wjc0xi6v2hz")))

(define-public crate-alsa-0.1.1 (c (n "alsa") (v "0.1.1") (d (list (d (n "alsa-sys") (r "^0.0.8") (d #t) (k 0)) (d (n "bitflags") (r "^0.5") (d #t) (k 0)) (d (n "libc") (r "^0.2.7") (d #t) (k 0)))) (h "0p24yfj0kywb09hdxv22xfz8nhjdw4hx76fbjizg7cqjz49cgklc")))

(define-public crate-alsa-0.1.2 (c (n "alsa") (v "0.1.2") (d (list (d (n "alsa-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "bitflags") (r "^0.5") (d #t) (k 0)) (d (n "libc") (r "^0.2.7") (d #t) (k 0)))) (h "1bszckl759n463s33950px0iy9nkhy53p603qhyipw5vrm08p60p")))

(define-public crate-alsa-0.1.3 (c (n "alsa") (v "0.1.3") (d (list (d (n "alsa-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "bitflags") (r "^0.5") (d #t) (k 0)) (d (n "libc") (r "^0.2.7") (d #t) (k 0)))) (h "103n80bq045ky8cykfcjvkhpaf11izaqx4j54ib8cn4rx3jmxm4d")))

(define-public crate-alsa-0.1.4 (c (n "alsa") (v "0.1.4") (d (list (d (n "alsa-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "bitflags") (r "^0.5") (d #t) (k 0)) (d (n "libc") (r "^0.2.7") (d #t) (k 0)))) (h "0wsaf012ik6vzivzjs22vkk15a88l0qwg9q63vzwv45cgnmj7ii1")))

(define-public crate-alsa-0.1.5 (c (n "alsa") (v "0.1.5") (d (list (d (n "alsa-sys") (r "^0.1.1") (d #t) (k 0)) (d (n "bitflags") (r "^0.8") (d #t) (k 0)) (d (n "libc") (r "^0.2.21") (d #t) (k 0)))) (h "1q4qrc8krm2m3ympbimcc3l4h8mpa75w0m9dz6vdy0gzrqdgih4l")))

(define-public crate-alsa-0.1.6 (c (n "alsa") (v "0.1.6") (d (list (d (n "alsa-sys") (r "^0.1.1") (d #t) (k 0)) (d (n "bitflags") (r "^0.8") (d #t) (k 0)) (d (n "libc") (r "^0.2.21") (d #t) (k 0)))) (h "03xsmxk31nawn8cyj89jg8a4pjz2jwg7yb23g0n7by8a4q14bni2")))

(define-public crate-alsa-0.1.7 (c (n "alsa") (v "0.1.7") (d (list (d (n "alsa-sys") (r "^0.1.1") (d #t) (k 0)) (d (n "bitflags") (r "^0.8") (d #t) (k 0)) (d (n "libc") (r "^0.2.21") (d #t) (k 0)))) (h "1an2l7zb4w4jr7wvk5qhbv7gfr8c21mb8vvf10x1g4c09mghg0b6")))

(define-public crate-alsa-0.1.8 (c (n "alsa") (v "0.1.8") (d (list (d (n "alsa-sys") (r "^0.1.1") (d #t) (k 0)) (d (n "bitflags") (r "^0.8") (d #t) (k 0)) (d (n "libc") (r "^0.2.21") (d #t) (k 0)))) (h "1dhg2bss2lm3shdy6sxsskdx8s1q91q94dxfw1s9zv1v1awgbsly")))

(define-public crate-alsa-0.1.9 (c (n "alsa") (v "0.1.9") (d (list (d (n "alsa-sys") (r "^0.1.1") (d #t) (k 0)) (d (n "bitflags") (r "^0.8") (d #t) (k 0)) (d (n "libc") (r "^0.2.21") (d #t) (k 0)))) (h "1nmiri7ix4ji67916s9s42w3c2hr6fy1saynxnmkv8nn9wyim8aq")))

(define-public crate-alsa-0.1.10 (c (n "alsa") (v "0.1.10") (d (list (d (n "alsa-sys") (r "^0.1.1") (d #t) (k 0)) (d (n "bitflags") (r "^0.8") (d #t) (k 0)) (d (n "libc") (r "^0.2.21") (d #t) (k 0)))) (h "0jcjca5s98g2sd6ikfdnl53a7l3clpky1jmkjsbsi6y3wzn396fm")))

(define-public crate-alsa-0.2.0 (c (n "alsa") (v "0.2.0") (d (list (d (n "alsa-sys") (r "^0.1.1") (d #t) (k 0)) (d (n "bitflags") (r "^0.9") (d #t) (k 0)) (d (n "libc") (r "^0.2.21") (d #t) (k 0)) (d (n "nix") (r "^0.9") (d #t) (k 0)))) (h "1p2acslj7mbc1pcfaq6brsv5cza6qkdsmpnmvhih2f1kzkvj027y")))

(define-public crate-alsa-0.2.1 (c (n "alsa") (v "0.2.1") (d (list (d (n "alsa-sys") (r "^0.1.1") (d #t) (k 0)) (d (n "bitflags") (r "^0.9") (d #t) (k 0)) (d (n "libc") (r "^0.2.21") (d #t) (k 0)) (d (n "nix") (r "^0.9") (d #t) (k 0)))) (h "0i879m63s3a6dffm4n3kx5vzfvqxwxvq508a58yr8nnlf1gagmcg")))

(define-public crate-alsa-0.2.2 (c (n "alsa") (v "0.2.2") (d (list (d (n "alsa-sys") (r "^0.1.1") (d #t) (k 0)) (d (n "bitflags") (r "^0.9") (d #t) (k 0)) (d (n "libc") (r "^0.2.21") (d #t) (k 0)) (d (n "nix") (r "^0.9") (d #t) (k 0)))) (h "0cpb9bz4r4x564gp0yclb1589bab7ghsxjcvvv2l2c5jr3mx985l")))

(define-public crate-alsa-0.3.0 (c (n "alsa") (v "0.3.0") (d (list (d (n "alsa-sys") (r "^0.1.1") (d #t) (k 0)) (d (n "bitflags") (r "^0.9") (d #t) (k 0)) (d (n "libc") (r "^0.2.60") (d #t) (k 0)) (d (n "nix") (r "^0.14.1") (d #t) (k 0)))) (h "0ljwyg1ckyglkjf3axhpfs4hspw2pxzr4qcis6w7r7c7ni75wspy")))

(define-public crate-alsa-0.4.0 (c (n "alsa") (v "0.4.0") (d (list (d (n "alsa-sys") (r "^0.2") (d #t) (k 0)) (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.65") (d #t) (k 0)) (d (n "nix") (r "^0.15") (d #t) (k 0)))) (h "1y0h2r1mihq7f58gr5h689ajgnmpddyj8wgyglncfxcvxq78r24i")))

(define-public crate-alsa-0.4.1 (c (n "alsa") (v "0.4.1") (d (list (d (n "alsa-sys") (r "^0.2") (d #t) (k 0)) (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.65") (d #t) (k 0)) (d (n "nix") (r "^0.15") (d #t) (k 0)))) (h "19ai4sf599pln1wr0vrsj9gd23asyk8356x76mmidcfffmxq6rnq")))

(define-public crate-alsa-0.4.2 (c (n "alsa") (v "0.4.2") (d (list (d (n "alsa-sys") (r "^0.3.0") (d #t) (k 0)) (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.65") (d #t) (k 0)) (d (n "nix") (r "^0.15") (d #t) (k 0)))) (h "0c2rp29d98yaq4ijqrwi0709wda20dapncna5bissx6x3bfiln24")))

(define-public crate-alsa-0.4.3 (c (n "alsa") (v "0.4.3") (d (list (d (n "alsa-sys") (r "^0.3.0") (d #t) (k 0)) (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.65") (d #t) (k 0)) (d (n "nix") (r "^0.15") (d #t) (k 0)))) (h "0d69ngf70yyvn4alf99x21rpp9jag4sj1jii16k8052b7rmky8gb")))

(define-public crate-alsa-0.4.4 (c (n "alsa") (v "0.4.4") (d (list (d (n "alsa-sys") (r "^0.3.1") (d #t) (k 0)) (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.65") (d #t) (k 0)) (d (n "nix") (r "^0.20") (d #t) (k 0)))) (h "0whzsg3nxwqc9nyxs4b7f9hzpr0q0k0yq1510qnngmv60cl5xcvv") (y #t)))

(define-public crate-alsa-0.5.0 (c (n "alsa") (v "0.5.0") (d (list (d (n "alsa-sys") (r "^0.3.1") (d #t) (k 0)) (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.88") (d #t) (k 0)) (d (n "nix") (r "^0.20") (d #t) (k 0)))) (h "064y582qns69wvjx6qcrkws30yn17r7ln1lcfmgcxcnw19wxmi3m")))

(define-public crate-alsa-0.5.1 (c (n "alsa") (v "0.5.1") (d (list (d (n "alsa-sys") (r "^0.3.1") (d #t) (k 0)) (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.88") (d #t) (k 0)) (d (n "nix") (r "^0.21") (d #t) (k 0)))) (h "03nmld6vbpxqg22fy07p51x2rmwl7bzsc7rszhd03gyknd5ldaqb") (y #t)))

(define-public crate-alsa-0.6.0 (c (n "alsa") (v "0.6.0") (d (list (d (n "alsa-sys") (r "^0.3.1") (d #t) (k 0)) (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.105") (d #t) (k 0)) (d (n "nix") (r "^0.23") (d #t) (k 0)))) (h "0szx8finhqbffh08fp3bgh4ywz0b572vcdyh4hwyhrfgw8pza5ar")))

(define-public crate-alsa-0.6.1 (c (n "alsa") (v "0.6.1") (d (list (d (n "alsa-sys") (r "^0.3.1") (d #t) (k 0)) (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.24") (f (quote ("ioctl"))) (k 0)))) (h "1sicg23vc00fnr3zsib1b5wz20p5w54mpdlvk5msh5jn68wlfqzm") (y #t)))

(define-public crate-alsa-0.7.0 (c (n "alsa") (v "0.7.0") (d (list (d (n "alsa-sys") (r "^0.3.1") (d #t) (k 0)) (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.24") (f (quote ("ioctl"))) (k 0)))) (h "0i2ypsk1zarf4hbfagqfmhfskqhr6v5gp237c2skyrjrf08wj4l5")))

(define-public crate-alsa-0.7.1 (c (n "alsa") (v "0.7.1") (d (list (d (n "alsa-sys") (r "^0.3.1") (d #t) (k 0)) (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.24") (f (quote ("ioctl"))) (k 0)))) (h "0iwbdgb6lr81iji9sr4f91mys24pia5avnkgbkv8kxzhvkc2lmp2")))

(define-public crate-alsa-0.8.0 (c (n "alsa") (v "0.8.0") (d (list (d (n "alsa-sys") (r "^0.3.1") (d #t) (k 0)) (d (n "bitflags") (r "^2.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.26") (f (quote ("ioctl"))) (k 0)))) (h "02k93vj8h789qx6as8yzi7r2wycqgmcsmk6m1pbb99dkwkhhjwww")))

(define-public crate-alsa-0.8.1 (c (n "alsa") (v "0.8.1") (d (list (d (n "alsa-sys") (r "^0.3.1") (d #t) (k 0)) (d (n "bitflags") (r "^2.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.26") (f (quote ("ioctl"))) (k 0)))) (h "02pzlq2q8ml28ikvkvm77bwdqmi22d6ak1qvrc0cr6yjb9adwd6f")))

(define-public crate-alsa-0.9.0 (c (n "alsa") (v "0.9.0") (d (list (d (n "alsa-sys") (r "^0.3.1") (d #t) (k 0)) (d (n "bitflags") (r "^2.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1klxza8bn23qbyqs7bg3kls1wk9h82zc7ihsq248lf1mjdvn1zip")))

