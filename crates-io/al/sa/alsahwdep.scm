(define-module (crates-io al sa alsahwdep) #:use-module (crates-io))

(define-public crate-alsahwdep-0.0.90 (c (n "alsahwdep") (v "0.0.90") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "ffi") (r "^0.0.90") (d #t) (k 0) (p "alsahwdep-sys")) (d (n "gir-format-check") (r "^0.1") (d #t) (k 2)) (d (n "glib") (r "^0.15") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.12") (d #t) (k 2)))) (h "0ll5gb7h57lmf720zby9ihahhwfnmk82vi1b3swgnv51smb7iqk5") (y #t)))

(define-public crate-alsahwdep-0.0.91 (c (n "alsahwdep") (v "0.0.91") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "ffi") (r "^0.0.91") (d #t) (k 0) (p "alsahwdep-sys")) (d (n "gir-format-check") (r "^0.1") (d #t) (k 2)) (d (n "glib") (r "^0.15") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.12") (d #t) (k 2)))) (h "18zwmwm945rv56v8nkn3si6f2hz4rbc6iiy61w0hmwq5kg676a24") (f (quote (("dox" "ffi/dox" "glib/dox")))) (y #t)))

(define-public crate-alsahwdep-0.0.92 (c (n "alsahwdep") (v "0.0.92") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "ffi") (r "^0.0.92") (d #t) (k 0) (p "alsahwdep-sys")) (d (n "gir-format-check") (r "^0.1") (d #t) (k 2)) (d (n "glib") (r "^0.15") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.12") (d #t) (k 2)))) (h "1b4a4a5rnkq5kl0cil4yzc0kdgkvx2cb2ayx6zs09p1nk1lfjffa") (f (quote (("dox" "ffi/dox" "glib/dox")))) (y #t)))

(define-public crate-alsahwdep-0.4.0 (c (n "alsahwdep") (v "0.4.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "ffi") (r "^0.4.0") (d #t) (k 0) (p "alsahwdep-sys")) (d (n "gir-format-check") (r "^0.1") (d #t) (k 2)) (d (n "glib") (r "^0.15") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.12") (d #t) (k 2)))) (h "1b5c5ai1gjp0j4ac77c2dnq5g5gzs0yvv04g241rkq0gjklwx9s4") (f (quote (("dox" "ffi/dox" "glib/dox"))))))

(define-public crate-alsahwdep-0.5.0 (c (n "alsahwdep") (v "0.5.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "ffi") (r "^0.5.0") (d #t) (k 0) (p "alsahwdep-sys")) (d (n "gir-format-check") (r "^0.1") (d #t) (k 2)) (d (n "glib") (r "^0.18") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.12") (d #t) (k 2)))) (h "0xbnc46f35l5wdvj0pmawms7rc97586g20hvjvn2bkak06lrm2db")))

(define-public crate-alsahwdep-0.6.0 (c (n "alsahwdep") (v "0.6.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "ffi") (r "^0.6.0") (d #t) (k 0) (p "alsahwdep-sys")) (d (n "gir-format-check") (r "^0.1") (d #t) (k 2)) (d (n "glib") (r "^0.19") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.12") (d #t) (k 2)))) (h "1gz8gg3gmww0m12pdi0khpj0c05q4wib4j3019jai361qa3ih3mp")))

