(define-module (crates-io al sa alsarawmidi) #:use-module (crates-io))

(define-public crate-alsarawmidi-0.0.90 (c (n "alsarawmidi") (v "0.0.90") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "ffi") (r "^0.0.90") (d #t) (k 0) (p "alsarawmidi-sys")) (d (n "gir-format-check") (r "^0.1") (d #t) (k 2)) (d (n "glib") (r "^0.15") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "19hg0lchw3i1rjd9n1mlpkpsl2z37yh7x87d6ph907ha6pjqhvyd") (y #t)))

(define-public crate-alsarawmidi-0.0.91 (c (n "alsarawmidi") (v "0.0.91") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "ffi") (r "^0.0.91") (d #t) (k 0) (p "alsarawmidi-sys")) (d (n "gir-format-check") (r "^0.1") (d #t) (k 2)) (d (n "glib") (r "^0.15") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1xymq16w9jhrz6xb1fciwyxz63b2yf0hcjgqjq1km66rpxg0d6hs") (f (quote (("dox" "ffi/dox" "glib/dox")))) (y #t)))

(define-public crate-alsarawmidi-0.0.92 (c (n "alsarawmidi") (v "0.0.92") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "ffi") (r "^0.0.92") (d #t) (k 0) (p "alsarawmidi-sys")) (d (n "gir-format-check") (r "^0.1") (d #t) (k 2)) (d (n "glib") (r "^0.15") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0mh233c2kfva8kwqbj2dx5ay3sgg4ha3ma6b26vxad88v2j4zxvc") (f (quote (("dox" "ffi/dox" "glib/dox")))) (y #t)))

(define-public crate-alsarawmidi-0.4.0 (c (n "alsarawmidi") (v "0.4.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "ffi") (r "^0.4.0") (d #t) (k 0) (p "alsarawmidi-sys")) (d (n "gir-format-check") (r "^0.1") (d #t) (k 2)) (d (n "glib") (r "^0.15") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0j866wqa6i3ih7z5hpvf8prylkz91dsiycjvw7nhvyj4acgxc420") (f (quote (("dox" "ffi/dox" "glib/dox"))))))

(define-public crate-alsarawmidi-0.5.0 (c (n "alsarawmidi") (v "0.5.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "ffi") (r "^0.5.0") (d #t) (k 0) (p "alsarawmidi-sys")) (d (n "gir-format-check") (r "^0.1") (d #t) (k 2)) (d (n "glib") (r "^0.18") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1kk1f7vaw63j3fwmb33aixvdxqmrc0lz2p3nxx5w8xy332p1p9x6")))

(define-public crate-alsarawmidi-0.6.0 (c (n "alsarawmidi") (v "0.6.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "ffi") (r "^0.6.0") (d #t) (k 0) (p "alsarawmidi-sys")) (d (n "gir-format-check") (r "^0.1") (d #t) (k 2)) (d (n "glib") (r "^0.19") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1irn7fqyfynkrcdn6kl5v9fqp2cpfc80x6jkzxqpzaxx05904jfk")))

