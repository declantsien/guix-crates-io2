(define-module (crates-io al sa alsatimer) #:use-module (crates-io))

(define-public crate-alsatimer-0.0.90 (c (n "alsatimer") (v "0.0.90") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "ffi") (r "^0.0.90") (d #t) (k 0) (p "alsatimer-sys")) (d (n "gir-format-check") (r "^0.1") (d #t) (k 2)) (d (n "glib") (r "^0.15") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0pfgwbv3z9ss80xzqwg6bbmbnyr5bc0irl2q567rr2a47d623xsh") (y #t)))

(define-public crate-alsatimer-0.0.91 (c (n "alsatimer") (v "0.0.91") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "ffi") (r "^0.0.91") (d #t) (k 0) (p "alsatimer-sys")) (d (n "gir-format-check") (r "^0.1") (d #t) (k 2)) (d (n "glib") (r "^0.15") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0781fpj0wz06221zcp0jl7bcl61gpq3dvsd5hzy1b5883anffkrz") (f (quote (("dox" "ffi/dox" "glib/dox")))) (y #t)))

(define-public crate-alsatimer-0.0.92 (c (n "alsatimer") (v "0.0.92") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "ffi") (r "^0.0.92") (d #t) (k 0) (p "alsatimer-sys")) (d (n "gir-format-check") (r "^0.1") (d #t) (k 2)) (d (n "glib") (r "^0.15") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0vi0igqs3myss2sd01gvgrrwd3qwchdfmslb6lv8vbajz4y7payv") (f (quote (("dox" "ffi/dox" "glib/dox")))) (y #t)))

(define-public crate-alsatimer-0.4.0 (c (n "alsatimer") (v "0.4.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "ffi") (r "^0.4.0") (d #t) (k 0) (p "alsatimer-sys")) (d (n "gir-format-check") (r "^0.1") (d #t) (k 2)) (d (n "glib") (r "^0.15") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0pv4il2gpfvsqwym77zqhqi4cvsa10zl5lcm8mx51vzl44kfc099") (f (quote (("dox" "ffi/dox" "glib/dox"))))))

(define-public crate-alsatimer-0.5.0 (c (n "alsatimer") (v "0.5.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "ffi") (r "^0.5.0") (d #t) (k 0) (p "alsatimer-sys")) (d (n "gir-format-check") (r "^0.1") (d #t) (k 2)) (d (n "glib") (r "^0.18") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "103zxvwk8v4915py4a2xy28bzf4mkkfamvxbidymv1xf6sr2ag50")))

(define-public crate-alsatimer-0.6.0 (c (n "alsatimer") (v "0.6.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "ffi") (r "^0.6.0") (d #t) (k 0) (p "alsatimer-sys")) (d (n "gir-format-check") (r "^0.1") (d #t) (k 2)) (d (n "glib") (r "^0.19") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "082y8rj8cdgnq79bgcziixxlgw27pv49pbj9ckrm0a1v13jzxib7")))

