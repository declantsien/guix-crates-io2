(define-module (crates-io al sa alsa-sys) #:use-module (crates-io))

(define-public crate-alsa-sys-0.0.1 (c (n "alsa-sys") (v "0.0.1") (d (list (d (n "pkg-config") (r "^0.1.1") (d #t) (k 1)))) (h "018iaz0l0csvyc40g3fp494gghbpcifjk8n9yj8mymc671li8hnm")))

(define-public crate-alsa-sys-0.0.2 (c (n "alsa-sys") (v "0.0.2") (d (list (d (n "pkg-config") (r "^0.1.1") (d #t) (k 1)))) (h "1xad7l46cc8kbqj5x5a09yl8g51ffz817xvb45b9mgw3727ql783")))

(define-public crate-alsa-sys-0.0.3 (c (n "alsa-sys") (v "0.0.3") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "pkg-config") (r "^0.1.1") (d #t) (k 1)))) (h "08pjw7xq8yc01552j3wrhqhdl92mj7jwfczn5zxa6bih1lxy44x4")))

(define-public crate-alsa-sys-0.0.4 (c (n "alsa-sys") (v "0.0.4") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "pkg-config") (r "^0.1.1") (d #t) (k 1)))) (h "14fxvzmm6r46pq6s3bml5fydvs7vb6kdflhd9g9cbhiwx19dpa8q")))

(define-public crate-alsa-sys-0.0.5 (c (n "alsa-sys") (v "0.0.5") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0hmfyrdpdn1a4b0sm7cbawjrjgwjn4jh3vwjg64n0jqp62swfxd5")))

(define-public crate-alsa-sys-0.0.6 (c (n "alsa-sys") (v "0.0.6") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1hwg832qimwx8i38ayrkv817i6gsrl08156nmjc04lgw14r7hizw")))

(define-public crate-alsa-sys-0.0.7 (c (n "alsa-sys") (v "0.0.7") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0xy4bkh96zp1cnhcfg1j96769lcylzzx6qyl60zv93mvqw61a8q8")))

(define-public crate-alsa-sys-0.0.8 (c (n "alsa-sys") (v "0.0.8") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "13znmixryrsw5adwpck742r77qyk3qg5wnzycmw0zj7k1mpw9z98")))

(define-public crate-alsa-sys-0.1.0 (c (n "alsa-sys") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.7") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0g31h0ii2z8szjk4aw75szrw6hfkmsz0hrmzgiyksqgif7i88grc")))

(define-public crate-alsa-sys-0.1.1 (c (n "alsa-sys") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.7") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0krah390wgkjzdqdbd4m4bdlzc6nkgnc3d889i595aq8m1azh4wh")))

(define-public crate-alsa-sys-0.1.2 (c (n "alsa-sys") (v "0.1.2") (d (list (d (n "libc") (r "^0.2.7") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0n3xr2msblmqlsx313b2y2v9hamqh0hp43v23fp1b3znkszwpvdh")))

(define-public crate-alsa-sys-0.2.0 (c (n "alsa-sys") (v "0.2.0") (d (list (d (n "libc") (r "^0.2.65") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0dpgid583hq876lclvrd5zwd6gdhh83djkxrxz74g03vg2pfm5by") (l "alsa")))

(define-public crate-alsa-sys-0.2.1 (c (n "alsa-sys") (v "0.2.1") (d (list (d (n "libc") (r "^0.2.65") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0xs91sbdc195svx2s78xqrrvy262fh7qavaspagv7hi2b27k0kb4") (l "alsa")))

(define-public crate-alsa-sys-0.3.0 (c (n "alsa-sys") (v "0.3.0") (d (list (d (n "libc") (r "^0.2.65") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "18rnznwfixfxjlz46ydid7v94gm410ff92yrj0k4hyizrndmb86m") (l "alsa")))

(define-public crate-alsa-sys-0.3.1 (c (n "alsa-sys") (v "0.3.1") (d (list (d (n "libc") (r "^0.2.65") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "09qmmnpmlcj23zcgx2xsi4phcgm5i02g9xaf801y7i067mkfx3yv") (l "alsa")))

