(define-module (crates-io al io alioth-cli) #:use-module (crates-io))

(define-public crate-alioth-cli-0.1.0 (c (n "alioth-cli") (v "0.1.0") (d (list (d (n "alioth") (r "^0.1.0") (d #t) (k 0)) (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "flexi_logger") (r "^0.28") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1nzjx5y2g2njka8qpxs4iq5a3yrf52pl9mglvcbdvklc8snsagby")))

(define-public crate-alioth-cli-0.2.0 (c (n "alioth-cli") (v "0.2.0") (d (list (d (n "alioth") (r "^0.2.0") (d #t) (k 0)) (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "flexi_logger") (r "^0.28") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-aco") (r "^0.2.0") (d #t) (k 0)))) (h "10daay5mw856m3z13faiqlv0zrscnw6hzn37zd9vfgpk80c11556")))

