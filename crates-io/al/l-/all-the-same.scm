(define-module (crates-io al l- all-the-same) #:use-module (crates-io))

(define-public crate-all-the-same-1.0.0 (c (n "all-the-same") (v "1.0.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "parsing" "extra-traits"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("net"))) (k 2)))) (h "0534q3fadc4vph4v91n1p700vn03samqp6zysww201yf2485ja9g")))

(define-public crate-all-the-same-1.1.0 (c (n "all-the-same") (v "1.1.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "parsing" "extra-traits"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("net"))) (k 2)))) (h "19702wbg267j0w7mc4rd7d4xwzfk3bh9g7az3zy9vpkzhadbiscg")))

