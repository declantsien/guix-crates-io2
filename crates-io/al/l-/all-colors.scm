(define-module (crates-io al l- all-colors) #:use-module (crates-io))

(define-public crate-all-colors-0.0.1 (c (n "all-colors") (v "0.0.1") (h "0hznhcrczw87lkv6rzi7s5lfmzx9jj7yylqnqkvra7m53r0q7s4q")))

(define-public crate-all-colors-0.0.2 (c (n "all-colors") (v "0.0.2") (d (list (d (n "sha2") (r "^0.10") (d #t) (k 0)))) (h "0lyh5n78pmyzmp69fpgvkpdimyd2m73cylj9raf9djhdzd4j5hgv")))

(define-public crate-all-colors-0.0.3 (c (n "all-colors") (v "0.0.3") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)) (d (n "stack-map") (r "^1") (d #t) (k 0)))) (h "1981cz0dg8081a0a681kv9nxi1mchx429585xljcz9z53dpqsj85")))

(define-public crate-all-colors-0.0.4 (c (n "all-colors") (v "0.0.4") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)) (d (n "stack-map") (r "^1") (d #t) (k 0)))) (h "08bb98w3s6c0sy0np4cyhcsbgq7mlpdlf4lz53k71dnl5378n6qd")))

(define-public crate-all-colors-0.0.5 (c (n "all-colors") (v "0.0.5") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)) (d (n "stack-map") (r "^1") (d #t) (k 0)))) (h "1vjpq308rc49kmhp3qrwb03kxprn44c6jmsnamgcdy7xlgh2b4b2")))

(define-public crate-all-colors-0.0.6 (c (n "all-colors") (v "0.0.6") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)) (d (n "stack-map") (r "^1") (d #t) (k 0)))) (h "0ibzsqi5kwy627nlnnj4ws3ivy410x6wzyhj7mvxj6q3gdmkwq2b")))

