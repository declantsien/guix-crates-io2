(define-module (crates-io al of aloft) #:use-module (crates-io))

(define-public crate-aloft-0.1.0 (c (n "aloft") (v "0.1.0") (d (list (d (n "hyper") (r "^0.5.2") (d #t) (k 0)) (d (n "regex") (r "^0.1.8") (d #t) (k 0)) (d (n "regex_macros") (r "^0.1.8") (d #t) (k 0)))) (h "0bpfs244js6djy4qcf2zziykph7alkwjvs8m5rkiw6a72d3xild9")))

(define-public crate-aloft-0.2.0 (c (n "aloft") (v "0.2.0") (d (list (d (n "hyper") (r "^0.5.2") (d #t) (k 0)) (d (n "regex") (r "^0.1.8") (d #t) (k 0)) (d (n "regex_macros") (r "^0.1.8") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.14") (d #t) (k 0)))) (h "023y1kvqhq1jxkpczl31pq4zkzabf5556vaajp8vfrl0rkckaand")))

(define-public crate-aloft-0.3.0 (c (n "aloft") (v "0.3.0") (d (list (d (n "chrono") (r "^0.2.14") (d #t) (k 0)) (d (n "hyper") (r "^0.5.2") (d #t) (k 0)) (d (n "regex") (r "^0.1.8") (d #t) (k 0)) (d (n "regex_macros") (r "^0.1.8") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.14") (d #t) (k 0)))) (h "1v05wi2ls4bqa7lkwp0ijh2d19d5dz2nh71mqlmk94fdjglmcbq3")))

(define-public crate-aloft-0.3.1 (c (n "aloft") (v "0.3.1") (d (list (d (n "chrono") (r "^0.2.14") (d #t) (k 0)) (d (n "hyper") (r "^0.5.2") (d #t) (k 0)) (d (n "regex") (r "^0.1.8") (d #t) (k 0)) (d (n "regex_macros") (r "^0.1.8") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.14") (d #t) (k 0)))) (h "0rjac6v27pjxjag9sx8gj35dls4p5khhpzq804k6lbhll5rps4sk")))

