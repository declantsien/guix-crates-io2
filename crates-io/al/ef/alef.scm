(define-module (crates-io al ef alef) #:use-module (crates-io))

(define-public crate-alef-0.0.1-alpha (c (n "alef") (v "0.0.1-alpha") (d (list (d (n "anyhow") (r "^1.0.37") (d #t) (k 0)) (d (n "indexmap") (r "^1.6.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "swc_common") (r "^0.10.8") (f (quote ("sourcemap"))) (d #t) (k 0)) (d (n "swc_ecma_ast") (r "^0.36.2") (d #t) (k 0)) (d (n "swc_ecma_utils") (r "^0.26.0") (d #t) (k 0)) (d (n "swc_ecma_visit") (r "^0.22.0") (d #t) (k 0)) (d (n "swc_ecmascript") (r "^0.16.1") (f (quote ("codegen" "parser" "transforms" "visit"))) (d #t) (k 0)))) (h "0271d3310wjr2ixc8xi275pxy79lfljf36p7nzcd0ngy8hkxgzr3")))

