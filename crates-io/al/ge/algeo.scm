(define-module (crates-io al ge algeo) #:use-module (crates-io))

(define-public crate-algeo-0.1.0 (c (n "algeo") (v "0.1.0") (d (list (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "xops") (r "^0.1.0") (d #t) (k 0)))) (h "0ps3xvrl9bn4aqqj26sfyxxh5xjzkdqlhhhzzpp1p7gjrpyvdzzx")))

