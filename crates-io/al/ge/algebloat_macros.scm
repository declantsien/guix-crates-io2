(define-module (crates-io al ge algebloat_macros) #:use-module (crates-io))

(define-public crate-algebloat_macros-0.0.0 (c (n "algebloat_macros") (v "0.0.0") (h "141d09ylignqvni700f2n9bz8rmkql84pmr14w9rvjrcwvb3r73i")))

(define-public crate-algebloat_macros-0.0.1 (c (n "algebloat_macros") (v "0.0.1") (h "057zh8qdcdh2n3ci2sdvarzq9pa1l9glh5k78xjh8wfzdgfpcbyr")))

(define-public crate-algebloat_macros-0.0.2 (c (n "algebloat_macros") (v "0.0.2") (h "0clxs5nk96995l79hj60lmn0qi9573lqfa11sjlfabk8l60asg6z")))

(define-public crate-algebloat_macros-0.0.3 (c (n "algebloat_macros") (v "0.0.3") (h "1s1q6khz3c0glrjlzyv1ml8zshams2sjc4iydsys8l2b362r00yp")))

(define-public crate-algebloat_macros-0.0.4 (c (n "algebloat_macros") (v "0.0.4") (h "0mwqrw3nnnnrqg6dxwzk628a3dpkz83rif3qafklw1xrhpcll6l7")))

(define-public crate-algebloat_macros-0.0.5 (c (n "algebloat_macros") (v "0.0.5") (h "11jhiq5dzfmqb9r3klgsfc2acr9643606f6hh1w719r235plm0hz")))

(define-public crate-algebloat_macros-0.0.6 (c (n "algebloat_macros") (v "0.0.6") (h "0qs336i8iv5zy8q5gqzfm3f17c0a6pdl0rvgamwcz6g3k4z7y1mm")))

(define-public crate-algebloat_macros-0.0.7 (c (n "algebloat_macros") (v "0.0.7") (h "0cjd1l8qadz2z9hqybsz38zmghknkj61c6npmsq4dk985rs4lkgh")))

(define-public crate-algebloat_macros-0.0.8 (c (n "algebloat_macros") (v "0.0.8") (h "1bsi3z040r1wv2jvgryv9w7kfxps936rbrxwfgcshj6lj8d10rd1")))

(define-public crate-algebloat_macros-0.0.10 (c (n "algebloat_macros") (v "0.0.10") (h "07ls4dkc4klbghh23kjjifs6yhxnh369ssj22vfjccdhdbdmy2i3")))

(define-public crate-algebloat_macros-0.0.11 (c (n "algebloat_macros") (v "0.0.11") (h "03fzgxx6798y5w7gma8bvkfkxdi3w0k677a52nd2habqaxlphax4")))

(define-public crate-algebloat_macros-0.0.12 (c (n "algebloat_macros") (v "0.0.12") (h "14pcrq8720kzr88wzy3g2z9l12ic918d6jlcj38136s5hl30jid2")))

(define-public crate-algebloat_macros-0.0.13 (c (n "algebloat_macros") (v "0.0.13") (h "126vxwr1hfi3z6658a503k4w8vnmdjhja235yif9l5rj1qw10war")))

