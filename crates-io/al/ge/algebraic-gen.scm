(define-module (crates-io al ge algebraic-gen) #:use-module (crates-io))

(define-public crate-algebraic-gen-0.1.0 (c (n "algebraic-gen") (v "0.1.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1nmfz7akcdiy0m44wsmbamahjd30z7092bynp6vi0ksfkf6xx089")))

