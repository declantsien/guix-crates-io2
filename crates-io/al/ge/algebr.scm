(define-module (crates-io al ge algebr) #:use-module (crates-io))

(define-public crate-algebr-0.1.0 (c (n "algebr") (v "0.1.0") (h "118y4m190wh06myhcqdliy3w515nfaj905lsb40p93r021af9qk3")))

(define-public crate-algebr-0.1.1 (c (n "algebr") (v "0.1.1") (h "08waa1c35s06h5fxz4qa1fwc7dipbhh16dllniwcgjbxhjpcay7h")))

