(define-module (crates-io al ge algebloat) #:use-module (crates-io))

(define-public crate-algebloat-0.0.0 (c (n "algebloat") (v "0.0.0") (d (list (d (n "algebloat_macros") (r "= 0.0.0") (d #t) (k 0)))) (h "15cm3ynr1hb99fsgzvivvzg9p5zshmyib62z9f04ccjpnfm0abbw")))

(define-public crate-algebloat-0.0.1 (c (n "algebloat") (v "0.0.1") (d (list (d (n "algebloat_macros") (r "= 0.0.1") (d #t) (k 0)))) (h "0rnrhbxkcwq4ffcd7nli99dgzq0m4lwl9q9d7fx4mwnqkbx729yz")))

(define-public crate-algebloat-0.0.2 (c (n "algebloat") (v "0.0.2") (d (list (d (n "algebloat_macros") (r "= 0.0.2") (d #t) (k 0)))) (h "12v2b8ysdcsq6pgajz4m5r07yij6yg113py10c0162i1yyl0y7bc")))

(define-public crate-algebloat-0.0.3 (c (n "algebloat") (v "0.0.3") (d (list (d (n "algebloat_macros") (r "= 0.0.3") (d #t) (k 0)))) (h "0v0kn6mhn4yl5mx2px3znksaci5psgpk20zy92frz9gla06pknah")))

(define-public crate-algebloat-0.0.4 (c (n "algebloat") (v "0.0.4") (d (list (d (n "algebloat_macros") (r "= 0.0.4") (d #t) (k 0)))) (h "15sphv0zhq41my78bfk43b30k8ij69yjv2pzsz9yh3chn3mq7jja")))

(define-public crate-algebloat-0.0.5 (c (n "algebloat") (v "0.0.5") (d (list (d (n "algebloat_macros") (r "= 0.0.5") (d #t) (k 0)))) (h "198h48s09hijp075yf4x44z7y7g610w9wfbxx90pd3ix9q8h1bzz")))

(define-public crate-algebloat-0.0.6 (c (n "algebloat") (v "0.0.6") (d (list (d (n "algebloat_macros") (r "= 0.0.6") (d #t) (k 0)))) (h "1whyvlz2fdzafcwdfjlsxj20swdakvgkx07fwl5qadz910j1vd4l")))

(define-public crate-algebloat-0.0.7 (c (n "algebloat") (v "0.0.7") (d (list (d (n "algebloat_macros") (r "= 0.0.7") (d #t) (k 0)))) (h "0i1f6kn65b2b8kl3d05vxn4q74k9m68vcwz6bycdcbhmq80g01v8")))

(define-public crate-algebloat-0.0.8 (c (n "algebloat") (v "0.0.8") (d (list (d (n "algebloat_macros") (r "= 0.0.8") (d #t) (k 0)) (d (n "rand") (r "= 0.3.7") (d #t) (k 2)))) (h "0ap5m6vxm9dvm2hd3nk82xzfv9r0473mx62c5szzbgcs564gl4hb")))

(define-public crate-algebloat-0.0.10 (c (n "algebloat") (v "0.0.10") (d (list (d (n "algebloat_macros") (r "= 0.0.10") (d #t) (k 0)) (d (n "rand") (r "= 0.3.8") (d #t) (k 2)))) (h "1agwgwzmd7ck6zjxmfny2s9cqcpz14xas03mxyf2g0zhhmx8721m")))

(define-public crate-algebloat-0.0.11 (c (n "algebloat") (v "0.0.11") (d (list (d (n "algebloat_macros") (r "= 0.0.11") (d #t) (k 0)) (d (n "chrono") (r "= 0.2.16") (d #t) (k 2)))) (h "1fkphg75xdm6qsm141ckcn2dplicn3nqhhzvkdazlwny5qdyr026")))

(define-public crate-algebloat-0.0.12 (c (n "algebloat") (v "0.0.12") (d (list (d (n "algebloat_macros") (r "= 0.0.12") (d #t) (k 0)) (d (n "chrono") (r "= 0.2.22") (d #t) (k 2)))) (h "1mdxxf05yq01mhgzvfzalvh58wjfbh8sz17qmgdknhk1kai5n8ac")))

(define-public crate-algebloat-0.0.13 (c (n "algebloat") (v "0.0.13") (d (list (d (n "algebloat_macros") (r "= 0.0.13") (d #t) (k 0)) (d (n "chrono") (r "= 0.3.1") (d #t) (k 2)))) (h "0xwyffjalcgi87v0blp9vnnk19inynzgfcrzmkiklhdjijcify44")))

