(define-module (crates-io al ge algen) #:use-module (crates-io))

(define-public crate-algen-0.1.0 (c (n "algen") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)))) (h "0ral5ylcp7mdv82d187q2vfswf24rkx3b22xcc2d9lfnlnwcb1z3")))

(define-public crate-algen-0.1.1 (c (n "algen") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)))) (h "15bcmq47ls79d4v0pz9jb33cbz50lgv5pyf62yb7ri8adcff2bf1")))

(define-public crate-algen-0.1.2 (c (n "algen") (v "0.1.2") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (o #t) (d #t) (k 0)))) (h "1bbg1fhhridb6k33lv3gm2dihgxrky04y0cg8pkwqsg3mj2z4wvq") (s 2) (e (quote (("tracing" "dep:tracing"))))))

(define-public crate-algen-0.2.0 (c (n "algen") (v "0.2.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (o #t) (d #t) (k 0)))) (h "0x5ac79rphs90w9w5idd0b3y22glakbjdysxpm20chs4lmq3jlls") (s 2) (e (quote (("tracing" "dep:tracing"))))))

