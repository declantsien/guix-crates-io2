(define-module (crates-io al ge algexenotation) #:use-module (crates-io))

(define-public crate-algexenotation-0.1.0 (c (n "algexenotation") (v "0.1.0") (h "0gff8m1srxydyxam5skmjahzln3pv07a2c5ackbdgyaxl2812swz")))

(define-public crate-algexenotation-0.2.0 (c (n "algexenotation") (v "0.2.0") (d (list (d (n "num-prime") (r "^0.2.0") (f (quote ("big-table"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0mks5c52sbx5bigz1yg6jh3zf8wbawwmwq9q4zi6nhf8zqfyiiwm")))

(define-public crate-algexenotation-0.2.1 (c (n "algexenotation") (v "0.2.1") (d (list (d (n "num-prime") (r "^0.2.0") (f (quote ("big-table"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1wypwavh2rzw8pgvfhf4sq2bfdy31f8ymkgmx0zydagwn8fllsaa")))

(define-public crate-algexenotation-0.3.0 (c (n "algexenotation") (v "0.3.0") (d (list (d (n "num-prime") (r "^0.2.0") (f (quote ("big-table"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1vikpgm82ilqmrfn829m9c0l293dx4r0ina2yfzn5j7ki3wz4p00")))

(define-public crate-algexenotation-0.4.0 (c (n "algexenotation") (v "0.4.0") (d (list (d (n "num-prime") (r "^0.4.3") (f (quote ("big-table"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0bgblslprlkk4yc1nwljxgc17cq4imcb24882wgz6hcb3ix48sj5")))

