(define-module (crates-io al ge algebra) #:use-module (crates-io))

(define-public crate-algebra-0.1.0 (c (n "algebra") (v "0.1.0") (h "1pmlamwcfi6fpgzlx3zg71v5scgjw6k01mvpd88bqaylcddqpl5g")))

(define-public crate-algebra-0.2.0 (c (n "algebra") (v "0.2.0") (d (list (d (n "quickcheck") (r "^0.2") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.2") (d #t) (k 2)))) (h "0p7mrdj9z47xflfssvf1wmj2vrgarwzpd5svidq40ik45i0j7nrc")))

