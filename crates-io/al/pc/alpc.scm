(define-module (crates-io al pc alpc) #:use-module (crates-io))

(define-public crate-alpc-0.0.2 (c (n "alpc") (v "0.0.2") (h "0zjdp2bbmqipj6lq3a9mz17byim5nirp04x803ip0yy6i2gdw88b")))

(define-public crate-alpc-1.0.0 (c (n "alpc") (v "1.0.0") (d (list (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1861y8x45rjwdbmz5g1fhnmvb2iidya7j8ra6v20aki0kg3k4lj9")))

(define-public crate-alpc-1.0.1 (c (n "alpc") (v "1.0.1") (d (list (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "076mmppzhh79mgl7wm5i99ky2ayxbmrncyxs9svkgfr9m6412x9n")))

