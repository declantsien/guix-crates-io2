(define-module (crates-io al wa always_equal) #:use-module (crates-io))

(define-public crate-always_equal-1.0.0 (c (n "always_equal") (v "1.0.0") (h "06pc88fm4bpk5wghs5pizvcpi1fgb853q2flyfzsnl79ac0zprsa")))

(define-public crate-always_equal-1.0.1 (c (n "always_equal") (v "1.0.1") (h "1f9hkfbjsq4fl1w50dgxvj41pr6cwar05yxz3rbahczcjk2w4ph3")))

(define-public crate-always_equal-1.0.2 (c (n "always_equal") (v "1.0.2") (h "1f42dk6jqjcr888yqi6wadiaw8wwdpvdq0j6g2jikfzmx8f88gw2")))

(define-public crate-always_equal-1.0.3 (c (n "always_equal") (v "1.0.3") (h "04j2iqg8k0x8di0y3s9xfrrjcgniywn6xd4lllzkyhabh5xbqhjm")))

