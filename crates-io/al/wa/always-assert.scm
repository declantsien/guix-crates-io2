(define-module (crates-io al wa always-assert) #:use-module (crates-io))

(define-public crate-always-assert-0.1.0 (c (n "always-assert") (v "0.1.0") (d (list (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)))) (h "17c4rwi4n8rjfxw7xwfkc87dsbrc66cwamrx0plkzn99yfbbhmsm") (f (quote (("force"))))))

(define-public crate-always-assert-0.1.1 (c (n "always-assert") (v "0.1.1") (d (list (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)))) (h "0q2snm4qgdwpm49l5pxp2v5q887pi5jn2c8q06lcvh2vikvqcxvj") (f (quote (("force"))))))

(define-public crate-always-assert-0.1.2 (c (n "always-assert") (v "0.1.2") (d (list (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)))) (h "04ad9wbh70nii1ilcd1mxv85yqx18jf9vsmh3ddps886bmi8ixpv") (f (quote (("force"))))))

(define-public crate-always-assert-0.1.3 (c (n "always-assert") (v "0.1.3") (d (list (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)))) (h "09w16ab0jg4jimf89br6h7z7ai3hbqh62g4p88dn7fxi58ly0dj4") (f (quote (("force"))))))

(define-public crate-always-assert-0.2.0 (c (n "always-assert") (v "0.2.0") (d (list (d (n "tracing") (r "^0.1") (o #t) (d #t) (k 0)))) (h "14hq43ganc8jwgxlks97f3fnpmwn269as4c65n3v2d0yrshqy1x1") (f (quote (("force"))))))

