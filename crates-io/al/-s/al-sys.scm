(define-module (crates-io al -s al-sys) #:use-module (crates-io))

(define-public crate-al-sys-0.1.0 (c (n "al-sys") (v "0.1.0") (d (list (d (n "pkg-config") (r "^0.3.8") (d #t) (k 1)))) (h "1sccl6ghg4jy7bihapncm2ww8wyvcp71finamlsajqgcr5qxx6ma")))

(define-public crate-al-sys-0.2.0 (c (n "al-sys") (v "0.2.0") (d (list (d (n "libloading") (r "^0.3.1") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.8") (d #t) (k 1)) (d (n "rental") (r "^0.2.0") (d #t) (k 0)))) (h "1pkv8f9i9qwnjjiqsfa4jbq2iasyvl136c1b31mjbarplawd0y06") (y #t)))

(define-public crate-al-sys-0.2.1 (c (n "al-sys") (v "0.2.1") (d (list (d (n "libloading") (r "^0.3.1") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.8") (d #t) (k 1)) (d (n "rental") (r "^0.2.4") (d #t) (k 0)))) (h "1xz9za2cbwiwb7ymav3lsgv18ghfw5bvpginz72sjyaz9s4gbqwm")))

(define-public crate-al-sys-0.3.0 (c (n "al-sys") (v "0.3.0") (d (list (d (n "libloading") (r "^0.3.1") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.8") (d #t) (k 1)) (d (n "rental") (r "^0.3.0") (d #t) (k 0)))) (h "090dcv9701bp4h1zv8qj0bkfdm330j60ki87ia6q3ycm2jqnygmx")))

(define-public crate-al-sys-0.3.1 (c (n "al-sys") (v "0.3.1") (d (list (d (n "libloading") (r "^0.3.1") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.8") (d #t) (k 1)) (d (n "rental") (r "^0.3.1") (d #t) (k 0)))) (h "0h96w0ijiqf6ww5119nrjzi629b8z6p5024l26jw0dp4ajhlddzi")))

(define-public crate-al-sys-0.3.2 (c (n "al-sys") (v "0.3.2") (d (list (d (n "libloading") (r "^0.3.1") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.8") (d #t) (k 1)) (d (n "rental") (r "^0.3.1") (d #t) (k 0)))) (h "1ni6fcvc9ljg85ph1kay6gfpc38b1jfqfq5lrdpjiimic32z7yyi")))

(define-public crate-al-sys-0.3.3 (c (n "al-sys") (v "0.3.3") (d (list (d (n "libloading") (r "^0.3.1") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.8") (d #t) (k 1)) (d (n "rental") (r "^0.3.1") (d #t) (k 0)))) (h "11kg4rlxja8xlficslyvnyfsryh1d2sk24cvm20081gcismi43np")))

(define-public crate-al-sys-0.4.0 (c (n "al-sys") (v "0.4.0") (d (list (d (n "libloading") (r "^0.3.1") (d #t) (t "cfg(not(target_os = \"emscripten\"))") (k 0)) (d (n "pkg-config") (r "^0.3.8") (d #t) (k 1)) (d (n "rental") (r "^0.4.5") (d #t) (k 0)))) (h "07ycag4iz3fng32wg1xb0xlz488d03dkqr54c0b31yk3nhv06j02")))

(define-public crate-al-sys-0.4.1 (c (n "al-sys") (v "0.4.1") (d (list (d (n "libloading") (r "^0.4") (d #t) (t "cfg(not(target_os = \"emscripten\"))") (k 0)) (d (n "pkg-config") (r "^0.3.8") (d #t) (k 1)) (d (n "rental") (r "^0.4.5") (d #t) (k 0)))) (h "0zfxsgcxcmy051mscgghkny00i6ayn09gg9xnf4m3zaqd2wfhvlb")))

(define-public crate-al-sys-0.5.0 (c (n "al-sys") (v "0.5.0") (d (list (d (n "libloading") (r "^0.4") (d #t) (t "cfg(not(target_os = \"emscripten\"))") (k 0)) (d (n "pkg-config") (r "^0.3.8") (d #t) (k 1)) (d (n "rental") (r "^0.4.5") (d #t) (k 0)))) (h "1k20grj0qj7b6kar1lh492hbvh8pac3ib8dmg02jfx7ijipdw8kc") (y #t)))

(define-public crate-al-sys-0.5.1 (c (n "al-sys") (v "0.5.1") (d (list (d (n "libloading") (r "^0.4") (d #t) (t "cfg(not(target_os = \"emscripten\"))") (k 0)) (d (n "pkg-config") (r "^0.3.8") (d #t) (k 1)) (d (n "rental") (r "^0.4.5") (d #t) (k 0)))) (h "0a77wfy03j0qiw67ria4f6gfir7b82x6afy3bxa8rnzza7gg69i1")))

(define-public crate-al-sys-0.5.2 (c (n "al-sys") (v "0.5.2") (d (list (d (n "libloading") (r "^0.4") (d #t) (t "cfg(not(target_os = \"emscripten\"))") (k 0)) (d (n "pkg-config") (r "^0.3.8") (d #t) (k 1)) (d (n "rental") (r "^0.5.0") (d #t) (k 0)))) (h "0wrm24k9sikazzih2w5ry7bcscssrjam0j69kzail6rc2g0x804z")))

(define-public crate-al-sys-0.5.3 (c (n "al-sys") (v "0.5.3") (d (list (d (n "libloading") (r "^0.5") (d #t) (t "cfg(not(target_os = \"emscripten\"))") (k 0)) (d (n "pkg-config") (r "^0.3.8") (d #t) (k 1)) (d (n "rental") (r "^0.5.0") (d #t) (k 0)))) (h "1a9bznqb3kx2xdvbkvyrg6b8nxdwzzd2pdp2xl8rlp75k1rkx4vn")))

(define-public crate-al-sys-0.6.0 (c (n "al-sys") (v "0.6.0") (d (list (d (n "libloading") (r "^0.5") (d #t) (t "cfg(not(target_os = \"emscripten\"))") (k 0)) (d (n "pkg-config") (r "^0.3.8") (d #t) (k 1)) (d (n "rental") (r "^0.5.0") (d #t) (k 0)))) (h "1b5lir8cmjrmal5zxq6awgnsdxvhlci9a7kafdsk54239ycyz581")))

(define-public crate-al-sys-0.6.1 (c (n "al-sys") (v "0.6.1") (d (list (d (n "cmake") (r "^0.1.40") (d #t) (k 1)) (d (n "libloading") (r "^0.5") (o #t) (d #t) (t "cfg(not(target_os = \"emscripten\"))") (k 0)) (d (n "rental") (r "^0.5.0") (o #t) (d #t) (t "cfg(not(target_os = \"emscripten\"))") (k 0)))) (h "08whlcfrhn4gqi4nbglkdqv5ysdpnvnlsqg51q34q9hh9l7rp3gz") (f (quote (("dynamic" "libloading" "rental") ("default" "dynamic"))))))

