(define-module (crates-io al er alerts) #:use-module (crates-io))

(define-public crate-alerts-0.1.0 (c (n "alerts") (v "0.1.0") (d (list (d (n "winapi") (r "^0.3") (f (quote ("winuser"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1klxq7q776qd5bm2bnymnksilw6g7ldm0r3lj9fcilwjc3gl1qv9")))

(define-public crate-alerts-0.1.1 (c (n "alerts") (v "0.1.1") (d (list (d (n "winapi") (r "^0.3") (f (quote ("winuser"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0rrlskqy9bcq2w7naj4fzb2bgn43zjg1xk614idr8y478f30in5q")))

