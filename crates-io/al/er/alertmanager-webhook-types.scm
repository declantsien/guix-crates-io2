(define-module (crates-io al er alertmanager-webhook-types) #:use-module (crates-io))

(define-public crate-alertmanager-webhook-types-0.1.0 (c (n "alertmanager-webhook-types") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 2)) (d (n "time") (r "^0.3.34") (f (quote ("serde" "serde-human-readable"))) (d #t) (k 0)))) (h "1dfvrlpgdmn1p01ksgp5s501vjlkpqnrpli11613wzswf3906maa")))

