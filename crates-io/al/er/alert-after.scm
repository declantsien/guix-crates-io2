(define-module (crates-io al er alert-after) #:use-module (crates-io))

(define-public crate-alert-after-1.0.0 (c (n "alert-after") (v "1.0.0") (d (list (d (n "mac-notification-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "notify-rust") (r "^3.3") (d #t) (k 0)))) (h "1n9fsn0v0mh38ywb0i1pswj8yj78jww792057wm36nd54a8y2ny9")))

(define-public crate-alert-after-1.1.0 (c (n "alert-after") (v "1.1.0") (d (list (d (n "mac-notification-sys") (r "^0.1.0") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "notify-rust") (r "^3.3") (d #t) (k 0)))) (h "0jg38v23lvhmgkm3a7s5yg8ha38g8pqiak0iydlwls651akj4dsy")))

(define-public crate-alert-after-1.2.0 (c (n "alert-after") (v "1.2.0") (d (list (d (n "mac-notification-sys") (r "^0.1.0") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "notify-rust") (r "^3.3") (d #t) (k 0)))) (h "16a0n705jaic31f7yfw52mhpzy9j4phc0swyvbh7y266hjwbrxvm")))

(define-public crate-alert-after-1.2.1 (c (n "alert-after") (v "1.2.1") (d (list (d (n "mac-notification-sys") (r "^0.1.0") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "notify-rust") (r "^3.3") (d #t) (k 0)))) (h "0m5plyd94sqp3amlj3rsa6r427f4zhb19vd4prjxcfka0apzwxg0")))

(define-public crate-alert-after-1.3.0 (c (n "alert-after") (v "1.3.0") (d (list (d (n "mac-notification-sys") (r "^0.1.0") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "notify-rust") (r "^3.3") (d #t) (t "cfg(not(target_os = \"macos\"))") (k 0)))) (h "1n5v8i69rnm01w7f2qjmkrzbm1lkxbcaq517gpcd9xp3bxcr2sjw")))

(define-public crate-alert-after-1.4.0 (c (n "alert-after") (v "1.4.0") (d (list (d (n "mac-notification-sys") (r "^0.1.0") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "notify-rust") (r "^3.3") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "winrt") (r "^0.2.1") (f (quote ("windows-data" "windows-ui"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1akc3d42g0dsnbsrnddxdvmy4xvrdbm9iwy7g2w9vc5ikf5nnnm8")))

(define-public crate-alert-after-1.4.1 (c (n "alert-after") (v "1.4.1") (d (list (d (n "mac-notification-sys") (r "^0.1.0") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "notify-rust") (r "^3.3") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "winrt") (r "^0.2.1") (f (quote ("windows-data" "windows-ui"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0628dhf9v111drm0g6fmvjw2crklp46z2mlmmnpaqldqlr26ghnz")))

(define-public crate-alert-after-1.4.2 (c (n "alert-after") (v "1.4.2") (d (list (d (n "mac-notification-sys") (r "^0.1.0") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "notify-rust") (r "^3.3") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "winrt") (r "^0.3.0") (f (quote ("windows-data" "windows-ui"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0scapqwnyjr1imlc5lap7c9ahz63c23r0cx5116vm33mzqq01vl3")))

(define-public crate-alert-after-1.4.3 (c (n "alert-after") (v "1.4.3") (d (list (d (n "mac-notification-sys") (r "^0.1.0") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "notify-rust") (r "^3.3") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "winrt") (r "^0.3.0") (f (quote ("windows-data" "windows-ui"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1jraakgzqlpzlwrhwlv3gr05br01f4x2ds6z78db46xpnyy9dlsy")))

(define-public crate-alert-after-1.5.0 (c (n "alert-after") (v "1.5.0") (d (list (d (n "mac-notification-sys") (r "^0.2.0") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "notify-rust") (r "^3.3") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "winrt") (r "^0.3.0") (f (quote ("windows-data" "windows-ui"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1cgk1fsn9gdkvzqg6pakkrsx4h2yp4k0fybgac0q9v9nh7m6l2vs")))

(define-public crate-alert-after-1.5.1 (c (n "alert-after") (v "1.5.1") (d (list (d (n "notifica") (r "^1.0.2") (d #t) (k 0)))) (h "0c6pb2kiz649q1vaifqf5jf3apgb1i1cdfyhnlczjrqxpik65z3x")))

