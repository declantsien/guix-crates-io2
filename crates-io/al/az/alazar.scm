(define-module (crates-io al az alazar) #:use-module (crates-io))

(define-public crate-alazar-0.0.1 (c (n "alazar") (v "0.0.1") (d (list (d (n "devela") (r "^0.8.0") (k 0)) (d (n "rand_core") (r "^0.6.4") (o #t) (k 0)))) (h "0djf95a85p5ibq55pl4bzd08r0v8dm3gz03xjfh7iqdx8qhc10i9") (f (quote (("unsafest" "unsafe" "devela/unsafest") ("unsafe") ("safest" "safe" "devela/safest") ("safe") ("no_std") ("nightly_docs" "nightly" "std" "rand_core") ("nightly") ("default" "std") ("all" "rand_core")))) (s 2) (e (quote (("std" "alloc" "rand_core?/std") ("alloc" "rand_core?/alloc")))) (r "1.72.0")))

(define-public crate-alazar-0.0.2 (c (n "alazar") (v "0.0.2") (d (list (d (n "devela") (r "^0.9.0") (d #t) (k 0)) (d (n "rand_core") (r "^0.6.4") (o #t) (k 0)))) (h "0ya6r91549ja86ld8fg4mplg9c4q8ywb9ccrlqmk58jgqkkxwwjc") (f (quote (("unsafest" "unsafe" "devela/unsafest") ("unsafe") ("safest" "safe" "devela/safest") ("safe") ("no_std") ("nightly_docs" "nightly" "std" "rand_core") ("nightly") ("full" "rand_core") ("default") ("all" "full")))) (s 2) (e (quote (("std" "alloc" "rand_core?/std") ("alloc" "rand_core?/alloc")))) (r "1.72.0")))

