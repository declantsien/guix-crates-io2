(define-module (crates-io al pi alpine-css) #:use-module (crates-io))

(define-public crate-alpine-css-0.1.0 (c (n "alpine-css") (v "0.1.0") (d (list (d (n "alpine-html") (r "^0.1") (d #t) (k 0)) (d (n "alpine-markup") (r "^0.1") (d #t) (k 0)))) (h "04v8c56jm3rq9vrc0llrshirhmdhshf3gcgfmvhl2rlyl8yr2087")))

