(define-module (crates-io al pi alpine) #:use-module (crates-io))

(define-public crate-alpine-0.0.1 (c (n "alpine") (v "0.0.1") (h "1qd3phyjxq68c2xqn2lz7pf1hmbymn1nrsdjh7crkipq3969szii")))

(define-public crate-alpine-0.1.0 (c (n "alpine") (v "0.1.0") (d (list (d (n "alpine-css") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "alpine-html") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "alpine-markup") (r "^0.1") (d #t) (k 0)) (d (n "alpine-svg") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "anyhow") (r "^1.0.75") (d #t) (k 2)))) (h "1l9ldsgbbkkjrr1pviqcmmabj2mzdgicp5865lmk7fcr20ksnz15") (f (quote (("svg" "alpine-svg") ("html" "alpine-html") ("default" "css" "html" "svg") ("css" "alpine-css"))))))

