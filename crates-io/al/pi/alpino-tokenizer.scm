(define-module (crates-io al pi alpino-tokenizer) #:use-module (crates-io))

(define-public crate-alpino-tokenizer-0.1.0 (c (n "alpino-tokenizer") (v "0.1.0") (d (list (d (n "alpino-tokenizer-sys") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "widestring") (r "^0.4") (d #t) (k 0)))) (h "183l1fjk8n8hhzy5h6csqbmfbibp2km0n6gkdgrvg9q4f4cayicy")))

(define-public crate-alpino-tokenizer-0.2.0 (c (n "alpino-tokenizer") (v "0.2.0") (d (list (d (n "alpino-tokenizer-sys") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "widestring") (r "^0.4") (d #t) (k 0)))) (h "0dffw4x4hcid67jxnvzh21a2dx1i6ja82fcvk38npm0p0zxavzs5")))

(define-public crate-alpino-tokenizer-0.3.0 (c (n "alpino-tokenizer") (v "0.3.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "prost") (r "^0.6") (d #t) (k 0)) (d (n "prost-derive") (r "^0.6") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "static_assertions") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1z79sd899c6r7pyx838vl1q704nyrf1ks9qs99y01qsb1gxnmq7d")))

(define-public crate-alpino-tokenizer-0.4.0 (c (n "alpino-tokenizer") (v "0.4.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "prost") (r "^0.12") (d #t) (k 0)) (d (n "prost-derive") (r "^0.12") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "static_assertions") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1hzkg75hba0ql5blrmpgyxrma09ckgnb9nj5gvmqn6h1s29hw74v") (r "1.60.0")))

