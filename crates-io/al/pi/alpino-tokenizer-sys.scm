(define-module (crates-io al pi alpino-tokenizer-sys) #:use-module (crates-io))

(define-public crate-alpino-tokenizer-sys-0.1.0 (c (n "alpino-tokenizer-sys") (v "0.1.0") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 2)) (d (n "widestring") (r "^0.4") (d #t) (k 0)))) (h "1mvf7bm3b0y5cdmmnmlyg73pdzhiix0lqwghbqj4r4lcjz2ggwz9")))

(define-public crate-alpino-tokenizer-sys-0.2.0 (c (n "alpino-tokenizer-sys") (v "0.2.0") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 2)) (d (n "widestring") (r "^0.4") (d #t) (k 0)))) (h "0pj3gnj1x3rpc241j9bxb3gqqsxnhvr8fj9p18a4l2q0dw6v8ywh")))

(define-public crate-alpino-tokenizer-sys-0.2.1 (c (n "alpino-tokenizer-sys") (v "0.2.1") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 2)) (d (n "widestring") (r "^0.4") (d #t) (k 0)))) (h "09nvinwqlp43if5spw2lj835hcnpdckxywfzwxx61pzl0lzvz0rp")))

