(define-module (crates-io al pa alpaga-test) #:use-module (crates-io))

(define-public crate-alpaga-test-0.1.0 (c (n "alpaga-test") (v "0.1.0") (h "1k5gdj0cnb3zggp36ryrzy2p1pnzkd6zq3236p9fmbw5nbbl9jr2")))

(define-public crate-alpaga-test-0.1.2 (c (n "alpaga-test") (v "0.1.2") (h "07x77bd2px9zklx76jivnfvhlp4yw8mnb6jsd2yv26h2413nbvzw")))

(define-public crate-alpaga-test-0.1.3 (c (n "alpaga-test") (v "0.1.3") (h "0a8aqiy3wr8yqaldyj1hhxqwk3awjxgcn8sg2rggjd7gsfc2v92a")))

(define-public crate-alpaga-test-0.1.5 (c (n "alpaga-test") (v "0.1.5") (h "0lj2jqzfcrf4m4xlvk1r0l0z3kclphwgzziibrb6xkngxgg5ggl0")))

