(define-module (crates-io al pa alpaca_api_client) #:use-module (crates-io))

(define-public crate-alpaca_api_client-0.1.0 (c (n "alpaca_api_client") (v "0.1.0") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.93") (d #t) (k 0)) (d (n "ureq") (r "^2.7.0") (f (quote ("json"))) (d #t) (k 0)))) (h "0czadbj77mwhj7mfrvxgi2nbnqrkrk8jhz4av7am53ywk20vf902")))

(define-public crate-alpaca_api_client-0.1.1 (c (n "alpaca_api_client") (v "0.1.1") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.93") (d #t) (k 0)) (d (n "ureq") (r "^2.7.0") (f (quote ("json"))) (d #t) (k 0)))) (h "0r952bmi7yzixvvvmh818bq5nrhrjkgzbd3m22nknb47m69zhxhj")))

(define-public crate-alpaca_api_client-0.1.2 (c (n "alpaca_api_client") (v "0.1.2") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.93") (d #t) (k 0)) (d (n "ureq") (r "^2.7.0") (f (quote ("json"))) (d #t) (k 0)))) (h "07saqfk19fxb160jgxcc3y3f15w9kfjlf3mamrj706xmildgynnx")))

(define-public crate-alpaca_api_client-0.1.3 (c (n "alpaca_api_client") (v "0.1.3") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.93") (d #t) (k 0)) (d (n "ureq") (r "^2.7.0") (f (quote ("json"))) (d #t) (k 0)))) (h "0n5a2f1p2fdl2ikn5m33nwgkaag4xp1p73ccgvlp7bf1sdrbjpsz")))

(define-public crate-alpaca_api_client-0.1.4 (c (n "alpaca_api_client") (v "0.1.4") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.93") (d #t) (k 0)) (d (n "ureq") (r "^2.7.0") (f (quote ("json"))) (d #t) (k 0)))) (h "1w1y6vdxavda19qzx6idb1bz47gr6ag3sazx3wc0yh2q8k2f7xaf")))

(define-public crate-alpaca_api_client-0.1.5 (c (n "alpaca_api_client") (v "0.1.5") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.93") (d #t) (k 0)) (d (n "ureq") (r "^2.7.0") (f (quote ("json"))) (d #t) (k 0)))) (h "109bvp3pfy8c3msi49s6wdyq03c6556f192j904c4davw8vg6cbc")))

(define-public crate-alpaca_api_client-0.1.6 (c (n "alpaca_api_client") (v "0.1.6") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.93") (d #t) (k 0)) (d (n "ureq") (r "^2.7.0") (f (quote ("json"))) (d #t) (k 0)))) (h "0bii4ig69ni04ywh7wwcrhylrhy9zr3rw0k1k7wym4cn5xw864kr")))

(define-public crate-alpaca_api_client-0.2.0 (c (n "alpaca_api_client") (v "0.2.0") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.93") (d #t) (k 0)) (d (n "ureq") (r "^2.7.0") (f (quote ("json"))) (d #t) (k 0)))) (h "18k07pahmwc42xzixk565m0h7xlmif5gghmsd080fxrwz3ws31jb")))

(define-public crate-alpaca_api_client-0.2.1 (c (n "alpaca_api_client") (v "0.2.1") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.93") (d #t) (k 0)) (d (n "ureq") (r "^2.7.0") (f (quote ("json"))) (d #t) (k 0)))) (h "0hd9l0jhf3vqdmrk0nq4ydi93prvq20xi34hv4hrz2jvz9yd0yvk")))

(define-public crate-alpaca_api_client-0.3.0 (c (n "alpaca_api_client") (v "0.3.0") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.93") (d #t) (k 0)) (d (n "ureq") (r "^2.7.0") (f (quote ("json"))) (d #t) (k 0)))) (h "1kajfnvdxhbdz7ndvi2q1bf3cdqj640fg6y6wgqw0ypf0a7ajrrp")))

(define-public crate-alpaca_api_client-0.3.1 (c (n "alpaca_api_client") (v "0.3.1") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.93") (d #t) (k 0)) (d (n "ureq") (r "^2.7.0") (f (quote ("json"))) (d #t) (k 0)))) (h "1darsqd08ff1wwbgcacfhgqcr42sq5c0v1z6jjcbqbpzwxd35ikc")))

(define-public crate-alpaca_api_client-0.3.2 (c (n "alpaca_api_client") (v "0.3.2") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.167") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.100") (d #t) (k 0)) (d (n "ureq") (r "^2.7.1") (f (quote ("json"))) (d #t) (k 0)))) (h "017c6bagbdp909gym2wlbvxr801a7s8c06vc6s7nn4vpb9k2z6qk")))

(define-public crate-alpaca_api_client-0.4.0 (c (n "alpaca_api_client") (v "0.4.0") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.167") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.100") (d #t) (k 0)) (d (n "ureq") (r "^2.7.1") (f (quote ("json"))) (d #t) (k 0)))) (h "0f7wm7d1yhkamhdvqs99spg7ll43cj3w3cj29cnmjax3nj526xya")))

(define-public crate-alpaca_api_client-0.4.1 (c (n "alpaca_api_client") (v "0.4.1") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.167") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.100") (d #t) (k 0)) (d (n "ureq") (r "^2.7.1") (f (quote ("json"))) (d #t) (k 0)))) (h "0fwlcacbx2n5mn6x7ds14kfmha37wn6hiqn9h5klzdxyxnjgmydv")))

(define-public crate-alpaca_api_client-0.5.0 (c (n "alpaca_api_client") (v "0.5.0") (d (list (d (n "dotenvy") (r "^0.15.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.167") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.100") (d #t) (k 0)) (d (n "ureq") (r "^2.7.1") (f (quote ("json"))) (d #t) (k 0)))) (h "14p6gb7rkh66azhhw1v7p96hia580pjs4p79s4lc92ibvmjchq7x")))

