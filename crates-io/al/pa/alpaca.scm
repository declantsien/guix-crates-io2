(define-module (crates-io al pa alpaca) #:use-module (crates-io))

(define-public crate-alpaca-0.1.0 (c (n "alpaca") (v "0.1.0") (d (list (d (n "argparse") (r "*") (d #t) (k 0)) (d (n "bio") (r ">= 0.3.7") (d #t) (k 0)) (d (n "itertools") (r "*") (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)) (d (n "peg") (r "*") (d #t) (k 0)) (d (n "rust-htslib") (r ">= 0.4.0") (d #t) (k 0)) (d (n "simple_parallel") (r "*") (d #t) (k 0)) (d (n "tempdir") (r "*") (d #t) (k 0)))) (h "139xsbhgc9kyjp6d44h6swahsrz9a533xa00dy645ds34aw0bjx3")))

