(define-module (crates-io al pa alpaca-data-api) #:use-module (crates-io))

(define-public crate-alpaca-data-api-0.1.0 (c (n "alpaca-data-api") (v "0.1.0") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "dotenv_codegen") (r "^0.15") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "15dfjrwlzqmcsmx1mcs19bfnimhdd41dw3i1jq6dy2csq58sq5zy") (y #t)))

(define-public crate-alpaca-data-api-0.1.1 (c (n "alpaca-data-api") (v "0.1.1") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "dotenv_codegen") (r "^0.15") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "0an6j5k6lqidakagjs7wa5qf42brc5d79bbgzmbdql6773ka7cqn") (y #t)))

(define-public crate-alpaca-data-api-0.1.2 (c (n "alpaca-data-api") (v "0.1.2") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "dotenv_codegen") (r "^0.15") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "0x42sgv5gr7s35dnx59kvqlwmz1wc1lnjxyccvmp2wb22bmachhc") (y #t)))

