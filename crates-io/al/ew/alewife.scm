(define-module (crates-io al ew alewife) #:use-module (crates-io))

(define-public crate-alewife-0.0.1 (c (n "alewife") (v "0.0.1") (h "1lvgdfngpny6xm1wq5cwfik7y54856mviimf2cjvgicvjjgp5231") (y #t)))

(define-public crate-alewife-0.0.2 (c (n "alewife") (v "0.0.2") (h "1zbfk196as6gmax4mmg1di8aw45lk4rwrnnsdnclc2yj2pa12pr1") (y #t)))

