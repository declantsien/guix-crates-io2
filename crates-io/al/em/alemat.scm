(define-module (crates-io al em alemat) #:use-module (crates-io))

(define-public crate-alemat-0.2.0 (c (n "alemat") (v "0.2.0") (h "1ybi3m98qr6hvapz83pavsch2knjbvxk9696xb4j075k893il0jz")))

(define-public crate-alemat-0.3.0 (c (n "alemat") (v "0.3.0") (d (list (d (n "insta") (r "^1.34.0") (d #t) (k 2)) (d (n "xmlem") (r "^0.2.3") (d #t) (k 2)))) (h "07sv76fjiz0vwlgbvixig7xd23k17kxpyvgp1vi58yzca52scp8f")))

(define-public crate-alemat-0.4.0 (c (n "alemat") (v "0.4.0") (d (list (d (n "insta") (r "^1.34.0") (d #t) (k 2)) (d (n "xmlem") (r "^0.2.3") (d #t) (k 2)))) (h "0s5f5gw25270brl6lmvl4h3mdcaiz2a63fxfxnwg6wwpx9j00myi")))

(define-public crate-alemat-0.4.1 (c (n "alemat") (v "0.4.1") (d (list (d (n "insta") (r "^1.34.0") (d #t) (k 2)) (d (n "xmlem") (r "^0.2.3") (d #t) (k 2)))) (h "0b0r3n0ikr6s3h6vxbmib29ng0pq95crkyk9zay4s1hvixr0ky9d")))

(define-public crate-alemat-0.5.0 (c (n "alemat") (v "0.5.0") (d (list (d (n "insta") (r "^1.34.0") (d #t) (k 2)) (d (n "xmlem") (r "^0.2.3") (d #t) (k 2)))) (h "0mp7ck3ivx0cjgpyncj50dz36r8iaza8rypx5yczzg9361xi0baa")))

(define-public crate-alemat-0.5.1 (c (n "alemat") (v "0.5.1") (d (list (d (n "insta") (r "^1.34.0") (d #t) (k 2)) (d (n "xmlem") (r "^0.2.3") (d #t) (k 2)))) (h "171bw27af68cd1ig3rgvc1k3mdc1hg913jwmjcfhy2kkzg8d7jls")))

(define-public crate-alemat-0.6.0 (c (n "alemat") (v "0.6.0") (d (list (d (n "insta") (r "^1.34.0") (d #t) (k 2)) (d (n "xmlem") (r "^0.2.3") (d #t) (k 2)))) (h "1yn48sbxjbq1fhp6f5q656y9wl9kkmkw2kv6r5y7h4jy798i4k3a")))

(define-public crate-alemat-0.6.1 (c (n "alemat") (v "0.6.1") (d (list (d (n "insta") (r "^1.34.0") (d #t) (k 2)) (d (n "xmlem") (r "^0.2.3") (d #t) (k 2)))) (h "1147ln8z6hp1dg9kfgc672xihd9h7adfld2r2ag0ym4bvfy0qr5k")))

(define-public crate-alemat-0.7.0 (c (n "alemat") (v "0.7.0") (d (list (d (n "insta") (r "^1.34.0") (d #t) (k 2)) (d (n "xmlem") (r "^0.2.3") (d #t) (k 2)))) (h "1jslvnqgjk0fkfw2ylaqr1r6g817s7g54lc1fv5c5gj6xyqirj4r")))

(define-public crate-alemat-0.8.0 (c (n "alemat") (v "0.8.0") (d (list (d (n "insta") (r "^1.34.0") (d #t) (k 2)) (d (n "xmlem") (r "^0.2.3") (d #t) (k 2)))) (h "1ia4bm16jnmizc0drzn9iav4d1lf8nxv67ayq8d18l6lg1q2sznx")))

