(define-module (crates-io al iy aliyun-oss) #:use-module (crates-io))

(define-public crate-aliyun-oss-0.1.0 (c (n "aliyun-oss") (v "0.1.0") (h "1gjcqsbdc3q37c76ipbc6vczf6r2gqfi08div73v6jn6642vyx4p")))

(define-public crate-aliyun-oss-0.1.1 (c (n "aliyun-oss") (v "0.1.1") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "ring") (r "^0.16") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "03q5nfay4fg3f9cp6ka3yd4j58mq4w57vg2jiy5lyls7i12iva04")))

