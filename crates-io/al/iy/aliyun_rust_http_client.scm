(define-module (crates-io al iy aliyun_rust_http_client) #:use-module (crates-io))

(define-public crate-aliyun_rust_http_client-0.1.0 (c (n "aliyun_rust_http_client") (v "0.1.0") (d (list (d (n "base64") (r "^0.21.4") (d #t) (k 0)) (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.3.0") (d #t) (k 0)) (d (n "ring") (r "^0.17.3") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "url-escape") (r "^0.1.1") (d #t) (k 0)) (d (n "uuid") (r "^1.4.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "0rgzzv0kp06cqkwdh2336kfv3p6i981ha288ldws19nazz2d6gir")))

