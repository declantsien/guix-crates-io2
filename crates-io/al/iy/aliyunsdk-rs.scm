(define-module (crates-io al iy aliyunsdk-rs) #:use-module (crates-io))

(define-public crate-aliyunsdk-rs-0.1.0 (c (n "aliyunsdk-rs") (v "0.1.0") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "base64") (r "^0.21.2") (d #t) (k 0)) (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "http-types") (r "^2.12.0") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.102") (d #t) (k 0)) (d (n "surf") (r "^2.3.2") (d #t) (k 0)) (d (n "url") (r "^2.4.0") (d #t) (k 0)))) (h "1i9glf7qfk74z666k19b14nbfzi1gxpg8z1fivrd992mwikgzxjk")))

(define-public crate-aliyunsdk-rs-0.1.1 (c (n "aliyunsdk-rs") (v "0.1.1") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "base64") (r "^0.21.2") (d #t) (k 0)) (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "http-types") (r "^2.12.0") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.102") (d #t) (k 0)) (d (n "surf") (r "^2.3.2") (d #t) (k 0)) (d (n "url") (r "^2.4.0") (d #t) (k 0)))) (h "1liyfrd9mpcnydbfsicx4ran7rpsxsljg48fcy0p0yaa1hnla3al")))

