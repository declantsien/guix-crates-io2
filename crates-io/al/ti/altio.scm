(define-module (crates-io al ti altio) #:use-module (crates-io))

(define-public crate-altio-0.1.0 (c (n "altio") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.41") (f (quote ("full"))) (d #t) (k 0)))) (h "1ln3r1z4qpxp4zxxh2y7ng807n0xl5h5s72kywmmpglrh9n6ix73")))

(define-public crate-altio-0.1.1 (c (n "altio") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.41") (f (quote ("full"))) (d #t) (k 0)))) (h "0k4aanf6dpj9ysm9d2ixm9lyvfp6i2iq7svll7xhdjkhrgd8iij0")))

(define-public crate-altio-0.1.2 (c (n "altio") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.41") (f (quote ("full"))) (d #t) (k 0)))) (h "118v5048xmpisk0kxvjwqrmzl183n3kb4wfh8827rgkzxslpd409")))

(define-public crate-altio-0.2.0 (c (n "altio") (v "0.2.0") (h "11ya5aqgpq4icwj7x9hrfywsk5qna5rlml6hrvjanfg90ka6hqrs") (f (quote (("default" "altio") ("altio"))))))

