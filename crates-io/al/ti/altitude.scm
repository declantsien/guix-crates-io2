(define-module (crates-io al ti altitude) #:use-module (crates-io))

(define-public crate-altitude-0.1.0 (c (n "altitude") (v "0.1.0") (h "0sngs2b595nr9n7n00bbgdzwgcxdn8j6vfjgax0ajb23mqclf3ck")))

(define-public crate-altitude-0.1.3 (c (n "altitude") (v "0.1.3") (h "0rhf8clgbkia8kckgxx745kahz3fnc5y16jadj1r81zxf6lk1yh0") (y #t)))

