(define-module (crates-io al ti altium-macros) #:use-module (crates-io))

(define-public crate-altium-macros-0.1.0 (c (n "altium-macros") (v "0.1.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.32") (d #t) (k 0)) (d (n "regex") (r "^1.9.1") (d #t) (k 0)) (d (n "syn") (r "^2.0.27") (d #t) (k 0)))) (h "14y0yq8nj2npdiylwysd6miy2i7i146rq0qyixz2q50xjq60750v")))

(define-public crate-altium-macros-0.2.0 (c (n "altium-macros") (v "0.2.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (d #t) (k 0)))) (h "0i0yvqm47ri17z89hzs74lgqbpwbsv5y8jf2cg756hdywpp84kd7")))

(define-public crate-altium-macros-0.2.1 (c (n "altium-macros") (v "0.2.1") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (d #t) (k 0)))) (h "0yyjhjjkam3999xy35m39bwidwhqqfrpk0vf5klv8jp59gvki4ff")))

