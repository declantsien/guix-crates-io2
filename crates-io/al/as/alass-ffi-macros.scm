(define-module (crates-io al as alass-ffi-macros) #:use-module (crates-io))

(define-public crate-alass-ffi-macros-0.1.0 (c (n "alass-ffi-macros") (v "0.1.0") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 2)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.13") (f (quote ("full"))) (d #t) (k 0)))) (h "0pq6rw2621idq43brajmwigifh219yvhfc3idq0zs6qmj72z3vsp")))

(define-public crate-alass-ffi-macros-0.3.0 (c (n "alass-ffi-macros") (v "0.3.0") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 2)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.89") (f (quote ("full"))) (d #t) (k 0)))) (h "1g3gafaph9afcvq1rc5fwga0jp6g913g3s74gmj6sl6h2cr96643")))

