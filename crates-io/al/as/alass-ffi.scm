(define-module (crates-io al as alass-ffi) #:use-module (crates-io))

(define-public crate-alass-ffi-0.0.0 (c (n "alass-ffi") (v "0.0.0") (h "15bw3in9yaikvflkb4483a0s59gdb2dzp9sbs239r0jdlxxgx2cm")))

(define-public crate-alass-ffi-0.1.0 (c (n "alass-ffi") (v "0.1.0") (d (list (d (n "alass-core") (r "^2.0.0") (d #t) (k 0)) (d (n "alass-ffi-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "alass-util") (r "^0.1.0") (d #t) (k 0)) (d (n "cbindgen") (r "^0.13") (d #t) (k 1)) (d (n "chrono") (r "^0.4.10") (d #t) (k 0)) (d (n "fern") (r "^0.5.9") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "subparse") (r "^0.6.2") (d #t) (k 0)))) (h "0cgfffkp66fqcznfqpvr8miw8vnl7rzrsfigdlb1cfzrmc7prvd0")))

(define-public crate-alass-ffi-0.1.2 (c (n "alass-ffi") (v "0.1.2") (d (list (d (n "alass-core") (r "^2.0.0") (d #t) (k 0)) (d (n "alass-ffi-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "alass-util") (r "^0.1.2") (d #t) (k 0)) (d (n "cbindgen") (r "^0.13") (d #t) (k 1)) (d (n "chrono") (r "^0.4.10") (d #t) (k 0)) (d (n "fern") (r "^0.5.9") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "subparse") (r "^0.6.2") (d #t) (k 0)))) (h "02d034fnqcd5ma40v6sjcc1pmmxqbvqx8wl9ps5zgzxdnfrglzpz")))

(define-public crate-alass-ffi-0.3.0 (c (n "alass-ffi") (v "0.3.0") (d (list (d (n "alass-core") (r "^2.0.0") (d #t) (k 0)) (d (n "alass-ffi-macros") (r "^0.3.0") (d #t) (k 0)) (d (n "alass-util") (r "^0.3.0") (d #t) (k 0)) (d (n "cbindgen") (r "^0.20") (d #t) (k 1)) (d (n "chrono") (r "^0.4.10") (d #t) (k 0)) (d (n "fern") (r "^0.6.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "subparse") (r "^0.7.0") (d #t) (k 0)))) (h "1sda9apsvgwiip33n88wp63b8fh6chpnq9l6wiwnr8y7c44vq3jf")))

