(define-module (crates-io al as alass-core) #:use-module (crates-io))

(define-public crate-alass-core-1.0.0 (c (n "alass-core") (v "1.0.0") (d (list (d (n "arrayvec") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "09wfsfimv8fdzgnqd64p1vkmrwpy0j88sapyxyaxf9vp6zk5gin4") (f (quote (("statistics") ("nosplit-heap-sort") ("default"))))))

(define-public crate-alass-core-1.0.1 (c (n "alass-core") (v "1.0.1") (d (list (d (n "arrayvec") (r "^0.4.11") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "1v312adajwbi6i2cq6qr9q5ig37s0nkc22hcihbb7ykfllawi83p") (f (quote (("statistics") ("nosplit-heap-sort") ("default"))))))

(define-public crate-alass-core-1.1.0 (c (n "alass-core") (v "1.1.0") (d (list (d (n "arrayvec") (r "^0.4.11") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "002423xf66xsjki86iwdh7k65sr5s6qwcdh5k5vc1x84ynlppima") (f (quote (("statistics") ("nosplit-heap-sort") ("default"))))))

(define-public crate-alass-core-2.0.0 (c (n "alass-core") (v "2.0.0") (d (list (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "09012w850w8sb966nw5akiwgk6fkad4v43gvqhfp7lzv7vjmdbgl") (f (quote (("nosplit-heap-sort") ("default"))))))

