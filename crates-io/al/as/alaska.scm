(define-module (crates-io al as alaska) #:use-module (crates-io))

(define-public crate-alaska-0.1.0 (c (n "alaska") (v "0.1.0") (h "17k6hnqpyccvpwlf48a0kfl5plaa3rf6bbkiw5s73x4rszksg1g6") (y #t)))

(define-public crate-alaska-0.1.1 (c (n "alaska") (v "0.1.1") (h "0x7blffvi33q95yzskkycdwjj4lbarg4y3a9pv875dkixwpsc8y4")))

