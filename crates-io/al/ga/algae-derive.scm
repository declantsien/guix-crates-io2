(define-module (crates-io al ga algae-derive) #:use-module (crates-io))

(define-public crate-algae-derive-0.1.6 (c (n "algae-derive") (v "0.1.6") (d (list (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full"))) (d #t) (k 0)))) (h "1rxjivwqy0xgqwfzm44r0l4y5ar3v34pna8nhd6pxkkw42zmnm9s")))

(define-public crate-algae-derive-0.1.7 (c (n "algae-derive") (v "0.1.7") (d (list (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full"))) (d #t) (k 0)))) (h "1iindsfc10p1407q22knxi1xixc2l4pr3rlx7d78p1l4wzfdh7dn")))

(define-public crate-algae-derive-0.1.8 (c (n "algae-derive") (v "0.1.8") (d (list (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full"))) (d #t) (k 0)))) (h "1mr11afdp9dks9iyxn7hqjh6v0g68v7glxkr4dy4qda70r71zvkq")))

(define-public crate-algae-derive-0.1.9 (c (n "algae-derive") (v "0.1.9") (d (list (d (n "proc-macro2") (r "^1.0.42") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (d #t) (k 0)))) (h "0q0l2vjp22b5ri0kpm0qfplbx0nafkndhqmw2h6vj9yqarhngg8n")))

