(define-module (crates-io al ga algar) #:use-module (crates-io))

(define-public crate-algar-0.1.0 (c (n "algar") (v "0.1.0") (h "1kyx2wdcz78jkszr3i1wl0pwpbnc4g9dms36bh7a7cfz1grhpbbs")))

(define-public crate-algar-0.2.0 (c (n "algar") (v "0.2.0") (h "03vxrn5kmlpw8n3z2hc82pq77h61fbcjkjq4wjk0i5vnkaspn9kd")))

(define-public crate-algar-0.2.1 (c (n "algar") (v "0.2.1") (h "01isg14s90r6s5zrcqgk4v137ycmlqbqxm4nw74a1biqkrp5ifgj")))

(define-public crate-algar-0.3.0 (c (n "algar") (v "0.3.0") (h "1aqy0ffgkwaiy4qrqmxgjibw1yy1n1kcvxg475fipyiqmfmsdm1x")))

(define-public crate-algar-0.3.1 (c (n "algar") (v "0.3.1") (h "1cz2za9z1pbjsidrxlbnqbrbqq70rzfjdzvh8a9msyp79qnvpnz3")))

(define-public crate-algar-0.4.0 (c (n "algar") (v "0.4.0") (h "08jzr78prvbyk3yi5wbkbvvrff27j5gfn8nmf7pagdldf81dvz87")))

(define-public crate-algar-0.4.1 (c (n "algar") (v "0.4.1") (h "0r20dg88q60bs2j34vz21g8xhnj50jbvy33g8ydpbvih6p1rifd2")))

