(define-module (crates-io al ga alga) #:use-module (crates-io))

(define-public crate-alga-0.1.0 (c (n "alga") (v "0.1.0") (d (list (d (n "num-traits") (r "0.1.*") (d #t) (k 0)) (d (n "quickcheck") (r "^0.2") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.2") (d #t) (k 2)))) (h "0xmkkdgaxg48aq58vq56qrvsvff5lf4rk7r6d0mfjgizmy7fhz7r")))

(define-public crate-alga-0.2.0 (c (n "alga") (v "0.2.0") (d (list (d (n "approx") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.2") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.2") (d #t) (k 2)))) (h "0jrhf6gi4jbi4wrabnk8bi6k3adm88y5ipa8v83q1qplcjavhwj6")))

(define-public crate-alga-0.3.0 (c (n "alga") (v "0.3.0") (d (list (d (n "approx") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.3") (d #t) (k 2)))) (h "15fsl1pmrc8pd9nz7glz5p383qqrrkcyhzbwgxxlxz4qgdig078p")))

(define-public crate-alga-0.4.0 (c (n "alga") (v "0.4.0") (d (list (d (n "approx") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.3") (d #t) (k 2)))) (h "1zsgsv32d510q61lc2xalskgh4djmvy8nzlfbi1qav5nbba4df93")))

(define-public crate-alga-0.5.0 (c (n "alga") (v "0.5.0") (d (list (d (n "approx") (r "^0.1") (d #t) (k 0)) (d (n "num-complex") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.3") (d #t) (k 2)))) (h "0kwnm3j2s81lvjlsn303nblnxi061nyk4rl6jq4fw3ms3n6j0amv")))

(define-public crate-alga-0.5.1 (c (n "alga") (v "0.5.1") (d (list (d (n "approx") (r "^0.1") (d #t) (k 0)) (d (n "num-complex") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.3") (d #t) (k 2)))) (h "0yxqxxb9wa0jr9hrngshq03ydf3jj82w3qs1d461bsamibn4l7mc")))

(define-public crate-alga-0.5.2 (c (n "alga") (v "0.5.2") (d (list (d (n "alga_derive") (r "^0.5") (d #t) (k 2)) (d (n "approx") (r "^0.1") (d #t) (k 0)) (d (n "decimal") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "num-complex") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.3") (d #t) (k 2)))) (h "06s21fgs155pxy1yw698isl2cbafna7karyy6nn31jpxbk7lk5ws")))

(define-public crate-alga-0.5.3 (c (n "alga") (v "0.5.3") (d (list (d (n "alga_derive") (r "^0.5") (d #t) (k 2)) (d (n "approx") (r "^0.1") (d #t) (k 0)) (d (n "decimal") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "num-complex") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.3") (d #t) (k 2)))) (h "1agq5j3fxi05z0yzgwdxpqh6rhdhyzlh3fnk6dfhgs6213z0mn29")))

(define-public crate-alga-0.5.4 (c (n "alga") (v "0.5.4") (d (list (d (n "alga_derive") (r "^0.5") (d #t) (k 2)) (d (n "approx") (r "^0.1") (d #t) (k 0)) (d (n "decimal") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "num-complex") (r "^0.1") (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.3") (d #t) (k 2)))) (h "029x7rajpjp2i98rzmcfs6p12j7abnf4yfv35iw5w1wksd619i48")))

(define-public crate-alga-0.6.0 (c (n "alga") (v "0.6.0") (d (list (d (n "alga_derive") (r "^0.6") (d #t) (k 2)) (d (n "approx") (r "^0.2") (k 0)) (d (n "decimal") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "num-complex") (r "^0.2") (k 0)) (d (n "num-traits") (r "^0.2") (k 0)) (d (n "quickcheck") (r "^0.3") (d #t) (k 2)))) (h "10qynfq7l5sk6mbgbbnkybalq1c4xl7n7ivx4z8jvdwk53k7kg1x") (f (quote (("std") ("default" "std"))))))

(define-public crate-alga-0.7.0 (c (n "alga") (v "0.7.0") (d (list (d (n "alga_derive") (r "^0.7") (d #t) (k 2)) (d (n "approx") (r "^0.3") (k 0)) (d (n "decimal") (r "^2.0") (o #t) (k 0)) (d (n "num-complex") (r "^0.2") (k 0)) (d (n "num-traits") (r "^0.2") (k 0)) (d (n "quickcheck") (r "^0.3") (d #t) (k 2)))) (h "1va9l98z78kqlqajw6hwm1hnqfsnyamyh50ysczk9638zavxpmfk") (f (quote (("std") ("default" "std"))))))

(define-public crate-alga-0.7.1 (c (n "alga") (v "0.7.1") (d (list (d (n "alga_derive") (r "^0.7") (d #t) (k 2)) (d (n "approx") (r "^0.3") (k 0)) (d (n "decimal") (r "^2.0") (o #t) (k 0)) (d (n "libm") (r "^0.1") (d #t) (k 0)) (d (n "num-complex") (r "^0.2") (k 0)) (d (n "num-traits") (r "^0.2") (k 0)) (d (n "quickcheck") (r "^0.3") (d #t) (k 2)))) (h "0q9nvqbxfz2lxj7j8gfn8carvdbmyn433lzbxxs6xa7azfm22d3y") (f (quote (("std") ("default" "std"))))))

(define-public crate-alga-0.7.2 (c (n "alga") (v "0.7.2") (d (list (d (n "alga_derive") (r "^0.7") (d #t) (k 2)) (d (n "approx") (r "^0.3") (k 0)) (d (n "decimal") (r "^2.0") (o #t) (k 0)) (d (n "libm") (r "^0.1") (d #t) (k 0)) (d (n "num-complex") (r "^0.2") (k 0)) (d (n "num-traits") (r "^0.2") (k 0)) (d (n "quickcheck") (r "^0.7") (d #t) (k 2)))) (h "0y2p2n5i61a7qf7l49xy8vj0qcaj3lkjz33vfin9iwjrrbp01fr4") (f (quote (("std") ("default" "std"))))))

(define-public crate-alga-0.8.0 (c (n "alga") (v "0.8.0") (d (list (d (n "alga_derive") (r "^0.8") (d #t) (k 2)) (d (n "approx") (r "^0.3") (k 0)) (d (n "decimal") (r "^2.0") (o #t) (k 0)) (d (n "libm") (r "^0.1") (d #t) (k 0)) (d (n "num-complex") (r "^0.2") (k 0)) (d (n "num-traits") (r "^0.2") (k 0)) (d (n "quickcheck") (r "^0.7") (d #t) (k 2)))) (h "1d2p8z2c6qr1za0yr12sp14nl8m2d9f9biivvjy2q0nxq7mk73xz") (f (quote (("std") ("default" "std")))) (y #t)))

(define-public crate-alga-0.8.1 (c (n "alga") (v "0.8.1") (d (list (d (n "alga_derive") (r "^0.8") (d #t) (k 2)) (d (n "approx") (r "^0.3") (k 0)) (d (n "decimal") (r "^2.0") (o #t) (k 0)) (d (n "libm") (r "^0.1") (d #t) (k 0)) (d (n "num-complex") (r "^0.2") (k 0)) (d (n "num-traits") (r "^0.2") (k 0)) (d (n "quickcheck") (r "^0.7") (d #t) (k 2)))) (h "12b167wpgs4gvyl4h5n5a2lnvpl11l3yyssgy5pchfl1lpc82khh") (f (quote (("std") ("default" "std"))))))

(define-public crate-alga-0.8.2 (c (n "alga") (v "0.8.2") (d (list (d (n "alga_derive") (r "^0.8") (d #t) (k 2)) (d (n "approx") (r "^0.3") (k 0)) (d (n "decimal") (r "^2.0") (o #t) (k 0)) (d (n "libm") (r "^0.1") (d #t) (k 0)) (d (n "num-complex") (r "^0.2") (k 0)) (d (n "num-traits") (r "^0.2") (k 0)) (d (n "quickcheck") (r "^0.8") (d #t) (k 2)))) (h "154n1qplawwpsay7wkgaxrywfpczjwljlkjp9609vp20gankdj1c") (f (quote (("std") ("default" "std"))))))

(define-public crate-alga-0.9.0 (c (n "alga") (v "0.9.0") (d (list (d (n "alga_derive") (r "^0.9") (d #t) (k 2)) (d (n "approx") (r "^0.3") (k 0)) (d (n "decimal") (r "^2.0") (o #t) (k 0)) (d (n "libm") (r "^0.1") (d #t) (k 0)) (d (n "num-complex") (r "^0.2") (k 0)) (d (n "num-traits") (r "^0.2") (k 0)) (d (n "quickcheck") (r "^0.8") (d #t) (k 2)))) (h "1b4m2fgfpjy0lji89w7x6lj7qdrjfq4h3rwhch66spi5rhd1fcx0") (f (quote (("std") ("default" "std"))))))

(define-public crate-alga-0.9.1 (c (n "alga") (v "0.9.1") (d (list (d (n "alga_derive") (r "^0.9") (d #t) (k 2)) (d (n "approx") (r "^0.3") (k 0)) (d (n "decimal") (r "^2.0") (o #t) (k 0)) (d (n "libm") (r "^0.1") (d #t) (k 0)) (d (n "num-complex") (r "^0.2") (k 0)) (d (n "num-traits") (r "^0.2") (k 0)) (d (n "quickcheck") (r "^0.8") (d #t) (k 2)))) (h "1y2gdbplcxd8spx2cscsj318hxvs2pq513z69n2d2vhhqxlcn26p") (f (quote (("std") ("default" "std"))))))

(define-public crate-alga-0.9.2 (c (n "alga") (v "0.9.2") (d (list (d (n "alga_derive") (r "^0.9") (d #t) (k 2)) (d (n "approx") (r "^0.3") (k 0)) (d (n "decimal") (r "^2.0") (o #t) (k 0)) (d (n "libm") (r "^0.1") (d #t) (k 0)) (d (n "num-complex") (r "^0.2") (k 0)) (d (n "num-traits") (r "^0.2") (k 0)) (d (n "quickcheck") (r "^0.8") (d #t) (k 2)))) (h "069v4nmvny90scx0w7hghzg76jy7fk8i9l59dhgphd1x25l993v5") (f (quote (("std") ("default" "std"))))))

(define-public crate-alga-0.9.3 (c (n "alga") (v "0.9.3") (d (list (d (n "alga_derive") (r "^0.9") (d #t) (k 2)) (d (n "approx") (r "^0.3") (k 0)) (d (n "decimal") (r "^2.0") (o #t) (k 0)) (d (n "num-complex") (r "^0.2") (k 0)) (d (n "num-traits") (r "^0.2.11") (f (quote ("libm"))) (k 0)) (d (n "quickcheck") (r "^0.8") (d #t) (k 2)))) (h "1wl4z8ini9269x04g8wwdz1nn3hmmvaaysq4jwhymikyg81kv0jg") (f (quote (("std") ("default" "std"))))))

