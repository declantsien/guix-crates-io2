(define-module (crates-io al ga algae-graph) #:use-module (crates-io))

(define-public crate-algae-graph-0.1.14 (c (n "algae-graph") (v "0.1.14") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "scsys") (r "^0.1.26") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 0)))) (h "0x0y6avbyn7pzkrn5ff1wb2a9xrg216g2xwdq62w4qi6yhx90r48")))

(define-public crate-algae-graph-0.1.15 (c (n "algae-graph") (v "0.1.15") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "scsys") (r "^0.1.33") (f (quote ("full"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.88") (d #t) (k 0)))) (h "0fw4i1xf527mgnb2nn0pyp0d4x8dzqsvy373rhg9n1n08whszcbi")))

(define-public crate-algae-graph-0.1.16 (c (n "algae-graph") (v "0.1.16") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "scsys") (r "^0.1.40") (f (quote ("full"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0i4d9h7x6q0dsf5dclsj1l987wrp94f888gl8swg3153x2pa6jll")))

(define-public crate-algae-graph-0.1.17 (c (n "algae-graph") (v "0.1.17") (d (list (d (n "decanter") (r "^0.1.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "scsys") (r "^0.1.40") (f (quote ("full"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0l3qav6c1x1dpralx9vcd9z4cklvy5liw2lmyxlq3ji4arf3ssk3")))

(define-public crate-algae-graph-0.1.18 (c (n "algae-graph") (v "0.1.18") (d (list (d (n "decanter") (r "^0.1.3") (f (quote ("derive" "wasm"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "scsys") (r "^0.1.41") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "smart-default") (r "^0.6") (d #t) (k 0)) (d (n "strum") (r "^0.24") (f (quote ("derive"))) (d #t) (k 0)))) (h "00v8g3jz0gcm41rj9vkgi6v1backz72zx91x7yiqvc3f4gczp9ny") (f (quote (("default"))))))

(define-public crate-algae-graph-0.1.19 (c (n "algae-graph") (v "0.1.19") (d (list (d (n "decanter") (r "^0.1.5") (f (quote ("derive" "wasm"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "smart-default") (r "^0.6") (d #t) (k 0)) (d (n "strum") (r "^0.24") (f (quote ("derive"))) (d #t) (k 0)))) (h "1nar0r8lv60fqvp670clnmxchmyhwb7lrs1msl0ivgsb26g30wdm") (f (quote (("wasm") ("default"))))))

