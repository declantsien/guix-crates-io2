(define-module (crates-io al ga algae-trees) #:use-module (crates-io))

(define-public crate-algae-trees-0.1.0 (c (n "algae-trees") (v "0.1.0") (d (list (d (n "bson") (r "^2.3.0") (d #t) (k 0)) (d (n "config") (r "^0.13.1") (d #t) (k 0)) (d (n "digest") (r "^0.10.3") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r ">=1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sha2") (r "^0.10.2") (d #t) (k 0)))) (h "1yk270sx283kbfgnl0cxz8h74dwh258cl1w2sj5zzjsdd602dhc4")))

(define-public crate-algae-trees-0.1.1 (c (n "algae-trees") (v "0.1.1") (d (list (d (n "bson") (r "^2.3.0") (d #t) (k 0)) (d (n "config") (r "^0.13.1") (d #t) (k 0)) (d (n "digest") (r "^0.10.3") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r ">=1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sha2") (r "^0.10.2") (d #t) (k 0)))) (h "049h1gsd7maqafvw36mzv055kjhyhk2c5rdkmkcz9la39wkraap9")))

