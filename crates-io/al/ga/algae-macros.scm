(define-module (crates-io al ga algae-macros) #:use-module (crates-io))

(define-public crate-algae-macros-0.1.6 (c (n "algae-macros") (v "0.1.6") (d (list (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full"))) (d #t) (k 0)))) (h "041qpy5r3yiy9v5mn6gpi2ss32hl8zkbf283b5k1qs1w58djc7qy")))

(define-public crate-algae-macros-0.1.7 (c (n "algae-macros") (v "0.1.7") (d (list (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full"))) (d #t) (k 0)))) (h "0cs7kib5z574lw69gmpqi34ah2mli0r802k4l7nxxf0p7is1q1x4")))

(define-public crate-algae-macros-0.1.8 (c (n "algae-macros") (v "0.1.8") (d (list (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full"))) (d #t) (k 0)))) (h "092w4zznx6pd84wvy4m9zwgj2nsd6gm3xwcmlpxi7b24z1nplrdg")))

(define-public crate-algae-macros-0.1.9 (c (n "algae-macros") (v "0.1.9") (d (list (d (n "proc-macro2") (r "^1.0.42") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (d #t) (k 0)))) (h "0l9sc9x3ggd9kmnc7ibfzzdfpxw7fcdy8bqffmnly1cn3pwqrlb7")))

