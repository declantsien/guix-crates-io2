(define-module (crates-io al ga algae-rs) #:use-module (crates-io))

(define-public crate-algae-rs-0.1.0 (c (n "algae-rs") (v "0.1.0") (h "0cggdsmcjmhcyngcpyhz7j4l8frsmlfcgsjs7dg3x8am46dvcyq2") (y #t)))

(define-public crate-algae-rs-0.1.1 (c (n "algae-rs") (v "0.1.1") (h "073dnjkzqnh4jb1s2aq1x1j9w2np3a6vkvp57hpddrbl2yq53wrm") (y #t)))

(define-public crate-algae-rs-0.1.2 (c (n "algae-rs") (v "0.1.2") (h "1y5758rdmbhm8kg0i5zwznjzanvxa9wj502x031k11na8gzzw6j0") (y #t)))

(define-public crate-algae-rs-0.1.21 (c (n "algae-rs") (v "0.1.21") (h "1mvy6j6wqa1yz2l35yv3r5k955iqjlks06bjj2zmwmipcb2ssxi8") (y #t)))

(define-public crate-algae-rs-0.1.22 (c (n "algae-rs") (v "0.1.22") (h "1w341svjcc5im6k7aysncqx7zg91ph4pgnvfnfwvb31m12sq315k")))

(define-public crate-algae-rs-0.1.23 (c (n "algae-rs") (v "0.1.23") (h "1kdj8b7ahrpvc5m5yvpz80l5ps27xh25y3crkvbjb6rfw0b2399y")))

(define-public crate-algae-rs-0.1.24 (c (n "algae-rs") (v "0.1.24") (h "0mfd9zsrpc3gyjr0qx4flvlgvw1nszm4z0fygcnn0d7ydr1c9hbg")))

(define-public crate-algae-rs-0.1.241 (c (n "algae-rs") (v "0.1.241") (h "0hl7b7cb00w3s2s682rs923a8iyw2z1vrcz4l3hhpfxdr7ckjzp9")))

(define-public crate-algae-rs-0.1.25 (c (n "algae-rs") (v "0.1.25") (h "0wfa7s18yr5svhxqjpv7hblnpy714adag6d1g5w2mqkp5a499hxx") (y #t)))

(define-public crate-algae-rs-0.1.251 (c (n "algae-rs") (v "0.1.251") (h "1ywmwl18p45cpjqmagmr81n08ns01i4n233ncry67ampw5wmi021") (y #t)))

(define-public crate-algae-rs-0.1.252 (c (n "algae-rs") (v "0.1.252") (h "1ycccamfrhiyy4alz2jc2476k5wnb4aa45fdcx62w1779qaj59cb")))

