(define-module (crates-io al ga alga_derive) #:use-module (crates-io))

(define-public crate-alga_derive-0.5.0 (c (n "alga_derive") (v "0.5.0") (d (list (d (n "edit-distance") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "0l7zjjprjbfqy6ja8lixmp6byb1j6gv40m1659xzj6ipldmzgv22")))

(define-public crate-alga_derive-0.5.1 (c (n "alga_derive") (v "0.5.1") (d (list (d (n "edit-distance") (r "^1.0") (d #t) (k 0)) (d (n "quickcheck") (r "^0.3") (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "0vyi6jgchgv00r19lb6a6drms44wp6a52gam5dbqyzqb6m2shr1g")))

(define-public crate-alga_derive-0.6.0 (c (n "alga_derive") (v "0.6.0") (d (list (d (n "edit-distance") (r "^1.0") (d #t) (k 0)) (d (n "quickcheck") (r "^0.3") (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "0mdad1wj44l43ckfsmbmgbmz4a8sx4i0w2bwn0lz5g9l72kj43cb")))

(define-public crate-alga_derive-0.7.0 (c (n "alga_derive") (v "0.7.0") (d (list (d (n "edit-distance") (r "^1.0") (d #t) (k 0)) (d (n "quickcheck") (r "^0.3") (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "0wis9gr7fz15vjr91f0qm2ad33wyx1kwwpg9pfwkg3s216za2qsg")))

(define-public crate-alga_derive-0.7.1 (c (n "alga_derive") (v "0.7.1") (d (list (d (n "edit-distance") (r "^2.0") (d #t) (k 0)) (d (n "quickcheck") (r "^0.7") (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "05lccfjhm6sm1lalkmi1c2vdzcpz9h1s9yyk5cwkyh69s8a6xw46")))

(define-public crate-alga_derive-0.8.0 (c (n "alga_derive") (v "0.8.0") (d (list (d (n "edit-distance") (r "^2.0") (d #t) (k 0)) (d (n "quickcheck") (r "^0.7") (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "0g2ni04dqxrf8ngvqv99c8q2ba2zr9g5m17w090axqgfz8dgab8b") (y #t)))

(define-public crate-alga_derive-0.8.1 (c (n "alga_derive") (v "0.8.1") (d (list (d (n "edit-distance") (r "^2.0") (d #t) (k 0)) (d (n "quickcheck") (r "^0.7") (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "1gdz9fdyxf9fkh741hpbr8106px2ffsanwyhy82jpg2d4gg6jq3k")))

(define-public crate-alga_derive-0.8.2 (c (n "alga_derive") (v "0.8.2") (d (list (d (n "edit-distance") (r "^2.0") (d #t) (k 0)) (d (n "quickcheck") (r "^0.8") (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "10vp3k6bj5g3a6nydxcnxr0llicqkmv60c69dmgkj6647plk3k45")))

(define-public crate-alga_derive-0.9.0 (c (n "alga_derive") (v "0.9.0") (d (list (d (n "edit-distance") (r "^2.0") (d #t) (k 0)) (d (n "quickcheck") (r "^0.8") (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "01z8g68nd75hgagwsid8rlwibgg96gr7h557nilslxmlbrp10ygk")))

(define-public crate-alga_derive-0.9.1 (c (n "alga_derive") (v "0.9.1") (d (list (d (n "edit-distance") (r "^2.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quickcheck") (r "^0.8") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "02sz8dn9b986xgn1vg8vn4cdwra3aal3mbyknws5alam0c9xfj7l")))

(define-public crate-alga_derive-0.9.2 (c (n "alga_derive") (v "0.9.2") (d (list (d (n "edit-distance") (r "^2.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0a2594j6blczz18vfg85agr7vsjrbq6900d3xwxw0zzbqj9j2adz")))

