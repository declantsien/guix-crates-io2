(define-module (crates-io al ir aliri_traits) #:use-module (crates-io))

(define-public crate-aliri_traits-0.1.0 (c (n "aliri_traits") (v "0.1.0") (h "081hcb5hdx5d9l2b64fvshzg50maxcbazbpl4s75hgkxp2qwfbjz")))

(define-public crate-aliri_traits-0.1.1 (c (n "aliri_traits") (v "0.1.1") (h "1igdwqgfhq5dg1ivdvlhqdakab52z8nrplczs6mhbv26dcxaic4w")))

