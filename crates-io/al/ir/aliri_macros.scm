(define-module (crates-io al ir aliri_macros) #:use-module (crates-io))

(define-public crate-aliri_macros-0.1.0 (c (n "aliri_macros") (v "0.1.0") (d (list (d (n "serde") (r "^1") (d #t) (k 2)))) (h "0k9qsjrknarrv0m3w1d85y7rb39n77lznxv6gb1g5avqxaw113hs")))

(define-public crate-aliri_macros-0.1.1 (c (n "aliri_macros") (v "0.1.1") (d (list (d (n "serde") (r "^1") (d #t) (k 2)))) (h "1fdkkwkghxkjl5nj7ifnkpg83bsm5kzrzbldqkycwp2x26qcs5a1")))

