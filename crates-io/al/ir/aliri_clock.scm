(define-module (crates-io al ir aliri_clock) #:use-module (crates-io))

(define-public crate-aliri_clock-0.1.0 (c (n "aliri_clock") (v "0.1.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1fiwdfhgqnnydjlvypkpf4s28k3kjqp8jjjap4bzk7d8s6pv761q")))

(define-public crate-aliri_clock-0.1.1 (c (n "aliri_clock") (v "0.1.1") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1pq43bl4xhg8znfzgjmixsw23q7nfxad7jhcmwi52zg1vh0gbwv5")))

(define-public crate-aliri_clock-0.1.2 (c (n "aliri_clock") (v "0.1.2") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0g08sjd4zgkmlj8cvvghc0qg83qh0gaykyjxr5a058gr7aqflvzv")))

(define-public crate-aliri_clock-0.1.3 (c (n "aliri_clock") (v "0.1.3") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "15rrwkvxxsl6fcq1plnxbwls5y3kxafyx4lzdi2qkjmrhy17fh8n")))

(define-public crate-aliri_clock-0.1.4 (c (n "aliri_clock") (v "0.1.4") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0d70ri3k228k149dhz4g6gl9z1c99pja70nd5f3qhsdzssvi0mwc")))

