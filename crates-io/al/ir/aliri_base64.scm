(define-module (crates-io al ir aliri_base64) #:use-module (crates-io))

(define-public crate-aliri_base64-0.1.0 (c (n "aliri_base64") (v "0.1.0") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "0621rfm15r0118xkc8a1sngzhs3syhn8lfbg8n03qa1xssjw9icz")))

(define-public crate-aliri_base64-0.1.1 (c (n "aliri_base64") (v "0.1.1") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1hjkxnv715jflnzw4i59fa9z1p3vxrrd4xs06dmsrfgimbbkc9dp")))

(define-public crate-aliri_base64-0.1.2 (c (n "aliri_base64") (v "0.1.2") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1xhwp683j2shizlzsxv2z91gv2ynx2r0j1i32cv1q7qxc55xvgj2")))

(define-public crate-aliri_base64-0.1.3 (c (n "aliri_base64") (v "0.1.3") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "15lwi8wafmqgbsbgplj43j7wbmz9y7jf79sy0qwp1j5l7p9yh271")))

(define-public crate-aliri_base64-0.1.4 (c (n "aliri_base64") (v "0.1.4") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "0d25dzsk1inhshw5znkbagjbpqy5d7z58pssqh5nxds1lwzfm68v")))

(define-public crate-aliri_base64-0.1.5 (c (n "aliri_base64") (v "0.1.5") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "026x4jkk2jgpaydsl1xni3s4q8117dx46w1jr95p3cp7fw142q00")))

(define-public crate-aliri_base64-0.1.6 (c (n "aliri_base64") (v "0.1.6") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1ix2lzrlb62c8r92g3lrb4v13fg1zg95lkxbns8l94hi7d0x0rac")))

(define-public crate-aliri_base64-0.1.7 (c (n "aliri_base64") (v "0.1.7") (d (list (d (n "base64") (r "^0.21") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "0vdsafbn48qvl30i7b6zsvvnjdczvvhlypcwvk2bs7drgf4irgla")))

(define-public crate-aliri_base64-0.1.8 (c (n "aliri_base64") (v "0.1.8") (d (list (d (n "base64") (r "^0.22") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1x3ms4kz7hbb2h7qw7kv3fb7phrcbj9i2kap47xwn4lijrqpgn6r")))

