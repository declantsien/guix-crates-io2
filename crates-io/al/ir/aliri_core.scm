(define-module (crates-io al ir aliri_core) #:use-module (crates-io))

(define-public crate-aliri_core-0.1.0 (c (n "aliri_core") (v "0.1.0") (d (list (d (n "base64") (r "^0.12") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1jdvp5gakw311j1w884gfd03lcxrnd0i35fmz11yb57z55yvmlcg")))

(define-public crate-aliri_core-0.1.1 (c (n "aliri_core") (v "0.1.1") (d (list (d (n "base64") (r "^0.12") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1g59bml7x39mwqamhsb6898i61wnq2aaqxinf2mfjp2xk1xzx9j4")))

(define-public crate-aliri_core-0.2.0 (c (n "aliri_core") (v "0.2.0") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1fnxy1zskz85yha1vwmww0n7b0is7zds6jf2wyz6d9d8a9l407fg")))

