(define-module (crates-io al ly allyaudio) #:use-module (crates-io))

(define-public crate-allyaudio-0.1.0 (c (n "allyaudio") (v "0.1.0") (d (list (d (n "clap") (r "^3.1.17") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rodio") (r "^0.15.0") (d #t) (k 0)))) (h "1040d86clwlsk2gqnxdbrmg0aqcp845nm9dqbcsrms9s0r4294vd") (y #t)))

(define-public crate-allyaudio-0.2.0 (c (n "allyaudio") (v "0.2.0") (d (list (d (n "clap") (r "^3.1.17") (f (quote ("derive"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "rodio") (r "^0.15.0") (d #t) (k 0)))) (h "1znzhbxk848bwz45nr8ingp8g1jh7d9wkjhim9b8zpjx3c51n6g4") (y #t)))

(define-public crate-allyaudio-0.2.1 (c (n "allyaudio") (v "0.2.1") (d (list (d (n "clap") (r "^3.1.17") (f (quote ("derive"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "rodio") (r "^0.15.0") (d #t) (k 0)))) (h "01q52r68yx4kvhpkhq9lqfzzc2dvpsn503ihmxrm94smn4gmbbx6") (y #t)))

(define-public crate-allyaudio-0.2.2 (c (n "allyaudio") (v "0.2.2") (d (list (d (n "clap") (r "^3.1.17") (f (quote ("derive"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "rodio") (r "^0.15.0") (d #t) (k 0)))) (h "0fmh73599mimnzdf5h8hgjgghgvc94zr0jk2vxlw2pwnsaw22b0r") (y #t)))

(define-public crate-allyaudio-0.3.0 (c (n "allyaudio") (v "0.3.0") (d (list (d (n "clap") (r "^3.1.17") (f (quote ("derive"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "rodio") (r "^0.15.0") (f (quote ("symphonia-all"))) (d #t) (k 0)))) (h "1mcivwig2h0gnpwwmjcmphhg3is7rdm0730yl5k5jqngz82b0lxj") (y #t)))

(define-public crate-allyaudio-0.4.0 (c (n "allyaudio") (v "0.4.0") (d (list (d (n "clap") (r "^3.1.17") (f (quote ("derive"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "rodio") (r "^0.15.0") (f (quote ("symphonia-all"))) (d #t) (k 0)))) (h "1kgf98d4bzq8il1834clfpjlpqqm53rjly3wp0hkfsz4zk4zpzgy") (y #t)))

