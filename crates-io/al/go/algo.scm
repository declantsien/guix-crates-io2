(define-module (crates-io al go algo) #:use-module (crates-io))

(define-public crate-algo-0.1.0 (c (n "algo") (v "0.1.0") (h "1q5kpybh84hwn0fwbbr5awksvbpzmmi3mrmmpjq75bk23592a120")))

(define-public crate-algo-0.1.1 (c (n "algo") (v "0.1.1") (h "0mkzr1aywvi1vl05p0m20fazbyblsnx72vzw6p5mmln9gi25hsm4")))

(define-public crate-algo-0.1.2 (c (n "algo") (v "0.1.2") (h "0hzwvz0z0vf4f8plxjkpzha2alkwczskzplnf73raw6vq613qajk")))

(define-public crate-algo-0.1.3 (c (n "algo") (v "0.1.3") (h "16yjnrv89d781gqd0nkkphlz6k1linhjih176p3xr0c3z8slrmk8")))

(define-public crate-algo-0.1.4 (c (n "algo") (v "0.1.4") (h "1r42m7x8pldicm1xp79jcv2nwi98s7h78qygxywh4q2hcmzriii5")))

(define-public crate-algo-0.1.5 (c (n "algo") (v "0.1.5") (h "06rzm0d6sl3dyxxpa6jygzpxxvd39pzx5dcp009h16bjf16qzx4b")))

(define-public crate-algo-0.1.6 (c (n "algo") (v "0.1.6") (h "1a622y3l9vdp08jpy8cydivv05b95myl7ljx78a8viflnrg5lcii")))

(define-public crate-algo-0.1.7 (c (n "algo") (v "0.1.7") (h "1zica7vcq4aadwn919p9bg7lzr4qw9k23c7dra88l98zikmmab1g")))

(define-public crate-algo-0.1.8 (c (n "algo") (v "0.1.8") (h "00xc79iibdnghsaw9zvsd97scnlbrz4ary9275fgs8yn0xv51lx2")))

(define-public crate-algo-0.1.9 (c (n "algo") (v "0.1.9") (h "1wckqcvxd3h1k5w1ayg32ghrr5345hx03jwnndn9jawbymkrqvhl")))

