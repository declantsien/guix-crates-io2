(define-module (crates-io al go algos) #:use-module (crates-io))

(define-public crate-algos-0.1.0 (c (n "algos") (v "0.1.0") (d (list (d (n "rand") (r "^0.5.4") (d #t) (k 0)))) (h "17yfkmcqz4q9am4pdjfmypcnq90y7g31i49c51v7hqz6j9v4jzki")))

(define-public crate-algos-0.1.1 (c (n "algos") (v "0.1.1") (d (list (d (n "rand") (r "^0.5.4") (d #t) (k 0)))) (h "0j5ygl0l9vw202qjh972vn87vax3sqrbcrpm73882jgqf6bd8dkw")))

(define-public crate-algos-0.2.0 (c (n "algos") (v "0.2.0") (d (list (d (n "rand") (r "^0.5.4") (d #t) (k 0)))) (h "0sljsfdfilksdscgpjvc8f25fvplbqi65hqqb8jj50fnj79bfy52")))

(define-public crate-algos-0.3.0 (c (n "algos") (v "0.3.0") (d (list (d (n "rand") (r "^0.5.5") (d #t) (k 0)))) (h "0a6hl8a99fv9b9nxavjfvqd4zgbp0shr86b0ii1xj787qq65zqbx")))

(define-public crate-algos-0.3.1 (c (n "algos") (v "0.3.1") (d (list (d (n "rand") (r "^0.5.5") (d #t) (k 0)))) (h "08989m1si67i6b5rqyapjvcvicz8q78kx90451ihispg16hhwav6")))

(define-public crate-algos-0.3.2 (c (n "algos") (v "0.3.2") (d (list (d (n "rand") (r "^0.5.5") (d #t) (k 0)))) (h "16h51b1jqd5shj098qskjw7ciac25ypdd5ac834srkv3hr2wsvdl")))

(define-public crate-algos-0.4.0 (c (n "algos") (v "0.4.0") (d (list (d (n "num") (r "~0.2.1") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "1g9xi8sq2rwga713kdwlb4y4g3ff5pai7rgr9zdh1hb80l6niyg6") (f (quote (("default" "big_num") ("big_num" "num"))))))

