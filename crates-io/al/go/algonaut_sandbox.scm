(define-module (crates-io al go algonaut_sandbox) #:use-module (crates-io))

(define-public crate-algonaut_sandbox-0.0.1 (c (n "algonaut_sandbox") (v "0.0.1") (h "16mzlgq1kbfc65fqadbjykfcj0a5i7y1lcq8577w08ybv8b4vj71")))

(define-public crate-algonaut_sandbox-0.2.0 (c (n "algonaut_sandbox") (v "0.2.0") (d (list (d (n "algonaut") (r "^0.4.2") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.13") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "1678k4q2nhln7ccvhkvanga5ydxq1wqgrlpqyb86isnnd455ib3d")))

(define-public crate-algonaut_sandbox-0.2.1 (c (n "algonaut_sandbox") (v "0.2.1") (d (list (d (n "algonaut") (r "^0.4.2") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.13") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "0w2pavdrjjfa1fzf1pjrgcgjr9d52khc5ssir5flk157prxbkanl")))

