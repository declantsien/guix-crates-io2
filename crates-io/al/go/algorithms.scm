(define-module (crates-io al go algorithms) #:use-module (crates-io))

(define-public crate-algorithms-0.1.0 (c (n "algorithms") (v "0.1.0") (h "0apq3ibnfy7p9y65mybfd5zm27dg8yydg8b2m2dsbld709772sqr")))

(define-public crate-algorithms-0.2.0 (c (n "algorithms") (v "0.2.0") (h "1j9h5rzwy94dgjvvivnsc0rggqqn7q568416aj84fpwsz63d377l")))

(define-public crate-algorithms-0.2.1 (c (n "algorithms") (v "0.2.1") (h "1qp0qx0y01wfs8yhiql2f60x60hw7n94i5nghanxwymr4zjgrjnr")))

