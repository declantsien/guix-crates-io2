(define-module (crates-io al go algorithm) #:use-module (crates-io))

(define-public crate-algorithm-0.1.0 (c (n "algorithm") (v "0.1.0") (h "1qmhhrvh98rhw50xfvvi6din2ab9syds88qb51z2xk0b721yr962")))

(define-public crate-algorithm-0.1.1 (c (n "algorithm") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "11rfy5glhlwhd6nv078bd55apad86wvlp2pdzmjmq0sf7hxwm1r2")))

(define-public crate-algorithm-0.1.2 (c (n "algorithm") (v "0.1.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "slab") (r "^0.4.9") (d #t) (k 2)))) (h "01srspyz5dn3nxwi64sg399yyr8v31k743nb30aq15gwars8n4ml")))

