(define-module (crates-io al go algorithmia-entrypoint) #:use-module (crates-io))

(define-public crate-algorithmia-entrypoint-0.1.0 (c (n "algorithmia-entrypoint") (v "0.1.0") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (f (quote ("full"))) (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.59.1") (d #t) (k 0)))) (h "1a6qjmr4k83q9pb2vv1rp28as50gfpxa9lmfwah9dn3bj3ixjx4w")))

