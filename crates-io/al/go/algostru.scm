(define-module (crates-io al go algostru) #:use-module (crates-io))

(define-public crate-algostru-0.1.0 (c (n "algostru") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.18.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "tui") (r "^0.13.0") (f (quote ("crossterm"))) (k 0)))) (h "1m94k7brihygpr7ic0clysq79axhwnckxrixynbyh4a66mv4ssv4")))

(define-public crate-algostru-0.2.0 (c (n "algostru") (v "0.2.0") (d (list (d (n "crossterm") (r "^0.18.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nalgebra") (r "^0.24.0") (d #t) (k 0)) (d (n "num-bigint") (r "^0.3") (d #t) (k 0)) (d (n "num-integer") (r "^0.1.44") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "tui") (r "^0.13.0") (f (quote ("crossterm"))) (k 0)))) (h "0ws4jzpdd7b9n3b74f0nynlglp216xlzs4ccq9rpmipl9w9h4d59")))

(define-public crate-algostru-0.3.0 (c (n "algostru") (v "0.3.0") (d (list (d (n "crossterm") (r "^0.18.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nalgebra") (r "^0.24.0") (d #t) (k 0)) (d (n "num-bigint") (r "^0.3") (d #t) (k 0)) (d (n "num-integer") (r "^0.1.44") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "tui") (r "^0.13.0") (f (quote ("crossterm"))) (k 0)))) (h "141rpvcr6k1hvwjl9y5s7a2mdwjq780q4ldksbs36n0xa07pynhc")))

(define-public crate-algostru-0.4.0 (c (n "algostru") (v "0.4.0") (d (list (d (n "crossterm") (r "^0.18.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nalgebra") (r "^0.24.0") (d #t) (k 0)) (d (n "num-bigint") (r "^0.3") (d #t) (k 0)) (d (n "num-integer") (r "^0.1.44") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "tui") (r "^0.13.0") (f (quote ("crossterm"))) (k 0)))) (h "0nk3kai4wqs5rvpdji9a052dagpqapaajr0q3ly1lxw4kh3p8wd8")))

