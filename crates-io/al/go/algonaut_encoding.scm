(define-module (crates-io al go algonaut_encoding) #:use-module (crates-io))

(define-public crate-algonaut_encoding-0.0.1 (c (n "algonaut_encoding") (v "0.0.1") (h "012yb5x9h9w5z5v6dhdky12w6psgz85xgmc0nv5qazhxmyic2zkl")))

(define-public crate-algonaut_encoding-0.2.0 (c (n "algonaut_encoding") (v "0.2.0") (d (list (d (n "data-encoding") (r "^2.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "06hzx3g2amjbswfaxyakb5myrz7sgf6s32msrir12h3z8l6mv8pg")))

(define-public crate-algonaut_encoding-0.3.0 (c (n "algonaut_encoding") (v "0.3.0") (d (list (d (n "data-encoding") (r "^2.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0hsicdrcar0v68fvqma84gnk86fm6fkswcd80vf71z3gjr551czw")))

(define-public crate-algonaut_encoding-0.4.0 (c (n "algonaut_encoding") (v "0.4.0") (d (list (d (n "data-encoding") (r "^2.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "14gjk8zpifqwdalfiq8716q54cvgbqfacr7scm94xjy6g50msw7x")))

(define-public crate-algonaut_encoding-0.4.1 (c (n "algonaut_encoding") (v "0.4.1") (d (list (d (n "data-encoding") (r "^2.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0gvkxqcygknj7alkfzj82islm1fc98jibhvxgccr7xmkvqa7vv6q")))

(define-public crate-algonaut_encoding-0.4.2 (c (n "algonaut_encoding") (v "0.4.2") (d (list (d (n "data-encoding") (r "^2.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0nkvicphlpv1yjs39cny4hfg37i6aj381fyp8m9y5hyf28ah95l1")))

