(define-module (crates-io al go algorithmify_macros) #:use-module (crates-io))

(define-public crate-algorithmify_macros-0.1.0 (c (n "algorithmify_macros") (v "0.1.0") (h "1qv4kynwp4a1wnmrp1p0jj03ndvhnqa5yy7q8zg56b8wxlblfvcr")))

(define-public crate-algorithmify_macros-0.1.1 (c (n "algorithmify_macros") (v "0.1.1") (h "0gmh59b8bm2kv61fkx4jg0j7556g8ijg0l9smvaq6h6bga4nbm7n")))

