(define-module (crates-io al go algorithmica) #:use-module (crates-io))

(define-public crate-algorithmica-0.1.0 (c (n "algorithmica") (v "0.1.0") (h "1g79fzc347avihhqp7f43jac5vrzy4338vc7c7iw1c433578q8vs")))

(define-public crate-algorithmica-0.1.1 (c (n "algorithmica") (v "0.1.1") (h "0blahs0090cvzlw3jrqq5w6wdqfd55csqhs1s5hbyl9aa2hzyj1a")))

(define-public crate-algorithmica-0.1.2 (c (n "algorithmica") (v "0.1.2") (h "0fwm749swdz8x73r368jd5xxll9zzfal0860d5a0j73ynwx20kjr")))

(define-public crate-algorithmica-0.1.3 (c (n "algorithmica") (v "0.1.3") (h "01wrikc2q66h3ghg3h0dca3wcg0mb604smdfh1pk8hqdi059j432")))

(define-public crate-algorithmica-0.1.4 (c (n "algorithmica") (v "0.1.4") (h "0b98vhd06if0gl979p4a0lzw7gqxmd7zqfl7gha8rg5nsbk2zb9a")))

(define-public crate-algorithmica-0.1.5 (c (n "algorithmica") (v "0.1.5") (h "1j2my066ynw8qlfz1pdj4s9sr18xvz5qj4fyp3lsahgzb40hp0h7")))

(define-public crate-algorithmica-0.1.6 (c (n "algorithmica") (v "0.1.6") (h "04x55gpq4zzg44yfrly1cfjisg3j35d87bs9zg9pfg60mr18jqz3")))

(define-public crate-algorithmica-0.1.7 (c (n "algorithmica") (v "0.1.7") (h "13bzks9khvfzan0a6jbwrvhn4lyazxn66d7bbicvjyv70rlnxcym")))

(define-public crate-algorithmica-0.1.8 (c (n "algorithmica") (v "0.1.8") (h "0irzy46wccdpndxlinizyxcmj81xzs536b48x2akwwwgi1kk2wwf")))

(define-public crate-algorithmica-0.1.9 (c (n "algorithmica") (v "0.1.9") (h "0frzq1mdg49hsgzaykd42zc679pdfcp7pz0a6h7ls4lc3s1iin2i")))

(define-public crate-algorithmica-0.1.10 (c (n "algorithmica") (v "0.1.10") (h "1f8vkb04h4x0nhirkk2fhncb6v70pmrgpa9lw5bcksv25rf7mf8g")))

