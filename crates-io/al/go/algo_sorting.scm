(define-module (crates-io al go algo_sorting) #:use-module (crates-io))

(define-public crate-algo_sorting-0.0.1 (c (n "algo_sorting") (v "0.0.1") (h "04l5vlqgalg6s7fmvadizsrz0qi3s57mqrjin2w9if6q52asfghv")))

(define-public crate-algo_sorting-0.0.2 (c (n "algo_sorting") (v "0.0.2") (h "0bhh12fcx9l81i0cjynpzy3zplsq2f0qhc48z9lvxb482fmah7h2")))

