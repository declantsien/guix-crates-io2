(define-module (crates-io al go algolia-monitoring-rs) #:use-module (crates-io))

(define-public crate-algolia-monitoring-rs-0.1.0 (c (n "algolia-monitoring-rs") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 2)))) (h "0qr1mm010vl8k8535k9byya4544ziw54rz1akgs9cy2g5k5c4jhn") (y #t)))

(define-public crate-algolia-monitoring-rs-0.1.1 (c (n "algolia-monitoring-rs") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 2)))) (h "0x5fdd271j93yzi4yi6h7ixcmw739xgkpb38lwymyig33144chfr") (y #t)))

(define-public crate-algolia-monitoring-rs-0.1.2 (c (n "algolia-monitoring-rs") (v "0.1.2") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 2)))) (h "1p5dlz3vnfa29z46yyjh4ag6hp637h4wjwgr020r0nf4kqcak3z2") (y #t)))

(define-public crate-algolia-monitoring-rs-0.1.3 (c (n "algolia-monitoring-rs") (v "0.1.3") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 2)))) (h "1jkni0dg1v2jpqgvw2gim39n65hv6sswykba3wf3wxrnjnfinwmq") (y #t)))

(define-public crate-algolia-monitoring-rs-0.1.4 (c (n "algolia-monitoring-rs") (v "0.1.4") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 2)))) (h "1cynksnd680c6lzz7r6npr3xmjmh21h7y1c60b4d7maxhdbzhsf0")))

(define-public crate-algolia-monitoring-rs-0.1.5 (c (n "algolia-monitoring-rs") (v "0.1.5") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 2)))) (h "1a28q9blw6zlqblrwc4y6q6sg6cycvqizrg3z1906l985bh37s70")))

