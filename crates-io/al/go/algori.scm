(define-module (crates-io al go algori) #:use-module (crates-io))

(define-public crate-algori-0.1.0 (c (n "algori") (v "0.1.0") (h "1a3w4cf64rvglq3vzxni1gx4mnc2pizx26rwscl3sj63c7zfjc6d")))

(define-public crate-algori-0.2.0 (c (n "algori") (v "0.2.0") (h "13ag61ajnbd2cd1f6c7qvc53gfscv8vd7bhqg1i522lw2f980i9c")))

(define-public crate-algori-0.3.0 (c (n "algori") (v "0.3.0") (h "13x7x5x6x6yc1gsy2pnrkfj22c3kb2biydrl4cjy3hj0rqq69k2p")))

(define-public crate-algori-0.3.1 (c (n "algori") (v "0.3.1") (h "0bn0vrci91vljvg4jg40b3r6ij3qq7f1mvk6x7icd7mm6rwiy01c")))

(define-public crate-algori-0.4.0 (c (n "algori") (v "0.4.0") (h "1wn6c4lyhc2yji4znqs101630cliqrijixfs450rp0z9j2k6zjr3")))

(define-public crate-algori-0.5.0 (c (n "algori") (v "0.5.0") (h "1j845aggj54xzc9abdj81acmslgas6iah40nf0d0ikr280p4agb4")))

(define-public crate-algori-0.5.1 (c (n "algori") (v "0.5.1") (h "01ph4f6rm9f83vk4f6zfn29l9yv5kb2ccc2wq0k7fmgi891932fb") (y #t)))

(define-public crate-algori-0.6.0 (c (n "algori") (v "0.6.0") (h "032afx10sa5ffr208riv9g6mc02fh5fk25p190a2sc7ndpdgh4xc")))

(define-public crate-algori-0.7.0 (c (n "algori") (v "0.7.0") (h "0xxkj931bcwhdn1vii17qflza59vr27fnazgljhkr9irpz9icqp1")))

(define-public crate-algori-0.6.1 (c (n "algori") (v "0.6.1") (h "08cpnb9lsmmnbwrs8gcj9n3yrbi1qlig1lyn5szwqamdx95x3zy1")))

(define-public crate-algori-0.7.1 (c (n "algori") (v "0.7.1") (h "1rj9hn0mqyl8z75r0hipcz31vfik9ax94lclsfdrvm6zmval9w5f")))

(define-public crate-algori-0.8.0 (c (n "algori") (v "0.8.0") (h "05ihjwccp732izpsxkjnvp9j4dpsxl7jzbgxipdmplksry4bcldk")))

(define-public crate-algori-0.8.1 (c (n "algori") (v "0.8.1") (h "1pz8897pr2cp96fhrim1hwdn792z98f8vssxwlm9v9biimqjr5xk")))

(define-public crate-algori-0.8.2 (c (n "algori") (v "0.8.2") (h "020iwq3ffhinl1fv0am3hf7p3giha6i97xff14qwywrq1lhhaxsx")))

(define-public crate-algori-0.9.1 (c (n "algori") (v "0.9.1") (h "0ihizbikd3sdpdxb9bqkmn317354s4qava3dgz8r5wz3pc3ll28s")))

(define-public crate-algori-0.9.2 (c (n "algori") (v "0.9.2") (h "108qn8k47jjlwqvj3kicv6nih1aqpbi1pb0hdzvz54kvbsg2b077")))

(define-public crate-algori-0.10.0 (c (n "algori") (v "0.10.0") (h "146zn636400m5x8vay3vlq9mhndl959c4f2bys4wkiad0ml668z3")))

(define-public crate-algori-0.10.1 (c (n "algori") (v "0.10.1") (h "1kmjc7n1jj59xpzplw4cs17g9qxp1hbwv77l820bfylphikq9yfd")))

(define-public crate-algori-0.10.2 (c (n "algori") (v "0.10.2") (h "1fc2c7i9ww82y9rr6mz44d36lk95sf49di0j8g5csc45hmp9ciql")))

(define-public crate-algori-0.10.3 (c (n "algori") (v "0.10.3") (h "18nw8k6pdiw0zjf426dm1mw5nsb6y7dxl8gv0q2xg7vb7frzypva")))

(define-public crate-algori-0.10.4 (c (n "algori") (v "0.10.4") (h "07m02kws05pghhq1c2knll840vjjvsk2zl52s5r7gpv7aqg88j3a")))

(define-public crate-algori-0.10.5 (c (n "algori") (v "0.10.5") (h "0wv969a7i8z8yqj4irkrri9yc43nxm3ik5k8ca6v58z726j24rcc")))

(define-public crate-algori-0.11.0 (c (n "algori") (v "0.11.0") (h "0fqik7xg5ps6lihyly43rjs1hqwdlq1h3m57abpn7hvx6acmzilc")))

(define-public crate-algori-0.11.1 (c (n "algori") (v "0.11.1") (h "1ivi19mvfl6vk431wid6v7rlrv6v9663qgk0p5z21gs105qrdxra")))

(define-public crate-algori-0.11.2 (c (n "algori") (v "0.11.2") (h "13kpyih3f5pdzfd0mhmw5c0ll5irxs0aynzzxrplca1cdyc80xks")))

(define-public crate-algori-0.11.3 (c (n "algori") (v "0.11.3") (h "0n4hxv68f13vw3q6qsflnmqd1wdsg6vplp1smp8bfbzihmrycizz")))

(define-public crate-algori-0.11.4 (c (n "algori") (v "0.11.4") (h "0m3dkmcy6rnbdw5sypjh17liaws4csq2792psm79sjmjy7n40y9c")))

