(define-module (crates-io al go algorithm_rust) #:use-module (crates-io))

(define-public crate-algorithm_rust-0.1.0 (c (n "algorithm_rust") (v "0.1.0") (h "0qnpdnaw31dbd0ykaah80zq4xihqrnnchldp59nyxgqmn99wcvwj") (y #t)))

(define-public crate-algorithm_rust-0.1.1 (c (n "algorithm_rust") (v "0.1.1") (h "1c8lyvqdqkkhlmb9dkyfj245qa4dj1f9pb3mdr1fhj1nscs38q7g") (y #t)))

(define-public crate-algorithm_rust-0.1.2 (c (n "algorithm_rust") (v "0.1.2") (h "04lvrw2f7rm5kc8qcj4g41lh1gjm433sjr9rfhjpb2kp018dfzkb") (y #t)))

(define-public crate-algorithm_rust-0.1.3 (c (n "algorithm_rust") (v "0.1.3") (h "036641dn7xx7z3yldv6qs2ry3hy7kv25vhl1sq7zq44c29ffbamy")))

(define-public crate-algorithm_rust-0.1.4 (c (n "algorithm_rust") (v "0.1.4") (h "1gs6x9g6mpzsh8yh87gzbq9w7qs6g23ja47szm1kkhd0za3ghnq7")))

(define-public crate-algorithm_rust-0.1.5 (c (n "algorithm_rust") (v "0.1.5") (h "05rygg780za6vzgy1ppz9jyhm7vxpxy37yp3iavx582kya1sg1x2")))

(define-public crate-algorithm_rust-0.1.6 (c (n "algorithm_rust") (v "0.1.6") (h "1j6bd23s17cifqf74fj982bca6cxxk123j51d2n2phln0fxg7m6k")))

(define-public crate-algorithm_rust-0.1.7 (c (n "algorithm_rust") (v "0.1.7") (h "024g6s1bq0bwpy7b0ra5h72pp03bih7aj67lgsfvi5cq1wfp0s3d")))

(define-public crate-algorithm_rust-0.1.8 (c (n "algorithm_rust") (v "0.1.8") (h "1088rkyb59iwhzf7k328aqhymmycgj5a7bz1w7dj1928d7ys495j")))

(define-public crate-algorithm_rust-0.1.9 (c (n "algorithm_rust") (v "0.1.9") (h "12dh288afb6zhd9jvnp30a25qzckf0sm8x57qdgj510fpv3m41ic")))

(define-public crate-algorithm_rust-0.2.0 (c (n "algorithm_rust") (v "0.2.0") (h "1r0yw2mxlrf7nrrbzipcvdp4bbrva5f3c32p6042cdw0i1dvywfi")))

(define-public crate-algorithm_rust-0.2.1 (c (n "algorithm_rust") (v "0.2.1") (h "0wmxi9c2x4inann5z28jbssby33z61lsy0kakz1c130rxw6d252l")))

(define-public crate-algorithm_rust-0.2.2 (c (n "algorithm_rust") (v "0.2.2") (h "0s969ih01j71pf0y7wgsz6r8b58zmdipsb87y0wdj01lb0rgp02h")))

(define-public crate-algorithm_rust-0.2.3 (c (n "algorithm_rust") (v "0.2.3") (h "1yrb2w0a4yj9mqmff3yipaaal7gxrw5vykslc4rkz29mr3hmgkhk")))

(define-public crate-algorithm_rust-0.2.4 (c (n "algorithm_rust") (v "0.2.4") (h "0w4jhv6j9mn9dxcn79dl33qs9988hi3fqbgny628yvif4x3bp3bs")))

(define-public crate-algorithm_rust-0.3.0 (c (n "algorithm_rust") (v "0.3.0") (h "1m4w2x8xgyvw9sa25abda85x6vlg0w48rnrxj2qr4yc6zif57ysj")))

(define-public crate-algorithm_rust-0.3.1 (c (n "algorithm_rust") (v "0.3.1") (h "0zmimwnifbd7kcmyxrkkfrv1igwxgicbzkp0m9wpw0kc8fynasf3")))

(define-public crate-algorithm_rust-0.4.0 (c (n "algorithm_rust") (v "0.4.0") (h "1ik1k4mnw4grih7f3qldi1xxdi3jjqjfxi45i08wlg66b7c0d090")))

(define-public crate-algorithm_rust-0.5.0 (c (n "algorithm_rust") (v "0.5.0") (h "0pwqnp6lsc2k9fadmk9fv8003rp7ahwh4nckcv8vs242wppv4va9")))

(define-public crate-algorithm_rust-0.5.1 (c (n "algorithm_rust") (v "0.5.1") (h "1rzbf5vs82p582lg3rzchrx9mvr60lqgpjrgj0ykzm4srcvqhnwc")))

(define-public crate-algorithm_rust-0.6.0 (c (n "algorithm_rust") (v "0.6.0") (h "1krgbmq19va67zvpyxir35di3vdsp5yxp3i5imyj70nmwg8n9mb2")))

