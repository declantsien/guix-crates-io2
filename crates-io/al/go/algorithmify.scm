(define-module (crates-io al go algorithmify) #:use-module (crates-io))

(define-public crate-algorithmify-0.1.0 (c (n "algorithmify") (v "0.1.0") (d (list (d (n "algorithmify_macros") (r "^0.1.0") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)))) (h "075bx56yj8nwj4y4fgdzzg9k5i8bv7vxdcq3sbnd5gcz2rynn1dv")))

(define-public crate-algorithmify-0.1.1 (c (n "algorithmify") (v "0.1.1") (d (list (d (n "algorithmify_macros") (r "^0.1.1") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)))) (h "0ap53wiq5c8g6xbmmam6ywdsqq8p6mglzhwh9kmnnzj0llzybhx9")))

