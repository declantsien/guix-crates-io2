(define-module (crates-io al go algorithms_fourth) #:use-module (crates-io))

(define-public crate-algorithms_fourth-0.1.0 (c (n "algorithms_fourth") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "076wjk72cf73aj9wrrxrw4ww2cmaa55g2n36mxlcdwbnp9axhpf9")))

(define-public crate-algorithms_fourth-0.1.1 (c (n "algorithms_fourth") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "0k3mfgrq2qmybrbpibgzdm51hklr63c4iabr20f5sv40mjqwskg0")))

(define-public crate-algorithms_fourth-0.1.2 (c (n "algorithms_fourth") (v "0.1.2") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "0dzwlqppf4q2q48sa7qnf9qvqaw8w4rl234mi8lhim4d02clg9jf")))

(define-public crate-algorithms_fourth-0.1.3 (c (n "algorithms_fourth") (v "0.1.3") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "1bnfkjph0lcl2bwnrzs1xxvwll0vk1qlkzmhqbydnsjwhbpkahy8")))

(define-public crate-algorithms_fourth-0.1.4 (c (n "algorithms_fourth") (v "0.1.4") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "1yiynp18j58pq27dh71714xz7jkb32ppfbxa57nni0bds8q7v240")))

(define-public crate-algorithms_fourth-0.1.5 (c (n "algorithms_fourth") (v "0.1.5") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "1r43x0srawvan9zdm96rqk0bl98crl2rkggbp9x2ajpp92ij1j7n")))

(define-public crate-algorithms_fourth-0.1.6 (c (n "algorithms_fourth") (v "0.1.6") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "0qxgacqi6wj14l5c6fz0zfq0a68kvc3da6frxwm1ad4dfyjibpbd")))

(define-public crate-algorithms_fourth-0.1.7 (c (n "algorithms_fourth") (v "0.1.7") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "0cfykizml03jwynmgqkvzlqsd1y3am75y9axczmf4ywrlylp5b76")))

(define-public crate-algorithms_fourth-0.1.8 (c (n "algorithms_fourth") (v "0.1.8") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "0592kpibw4zjzqgwqwbzi2g6ll9kadhgjn7xv2672nh0xy74f5jp")))

(define-public crate-algorithms_fourth-0.1.9 (c (n "algorithms_fourth") (v "0.1.9") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "0c9azrbgpy8xqpxbmlxfybip3x57las9wd01bbkvkw7l3b92qszp")))

(define-public crate-algorithms_fourth-0.1.10 (c (n "algorithms_fourth") (v "0.1.10") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "1f2jxj23czxcxks1439b2qqbv37kd0bx99w3q17syf6sbdpdgvzi")))

