(define-module (crates-io al go algonaut_model) #:use-module (crates-io))

(define-public crate-algonaut_model-0.0.1 (c (n "algonaut_model") (v "0.0.1") (h "0qp2a3c3f6yricmqngm7mc6x6y3l6c1pjw01f4bz0qj0al0zjzlk")))

(define-public crate-algonaut_model-0.3.0 (c (n "algonaut_model") (v "0.3.0") (d (list (d (n "algonaut_core") (r "^0.3.0") (d #t) (k 0)) (d (n "algonaut_crypto") (r "^0.3.0") (d #t) (k 0)) (d (n "algonaut_encoding") (r "^0.3.0") (d #t) (k 0)) (d (n "data-encoding") (r "^2.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)))) (h "0icpr208wykif5qnhm4fsas725j23z1kqjzqdrrdcg2fc6m7xcwp")))

(define-public crate-algonaut_model-0.4.0 (c (n "algonaut_model") (v "0.4.0") (d (list (d (n "algonaut_core") (r "^0.4.0") (d #t) (k 0)) (d (n "algonaut_crypto") (r "^0.4.0") (d #t) (k 0)) (d (n "algonaut_encoding") (r "^0.4.0") (d #t) (k 0)) (d (n "data-encoding") (r "^2.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11.4") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)) (d (n "serde_with") (r "^1.9.4") (d #t) (k 0)))) (h "1ymyr7nqa650clk2sicvjc0wg1115981lvvks7ay02qwy1lm6dsv")))

(define-public crate-algonaut_model-0.4.1 (c (n "algonaut_model") (v "0.4.1") (d (list (d (n "algonaut_core") (r "^0.4.1") (d #t) (k 0)) (d (n "algonaut_crypto") (r "^0.4.1") (d #t) (k 0)) (d (n "algonaut_encoding") (r "^0.4.1") (d #t) (k 0)) (d (n "data-encoding") (r "^2.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11.4") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)) (d (n "serde_with") (r "^1.9.4") (d #t) (k 0)))) (h "01k5731ngs7gwwwpgr3p5rs2ilv68sq250dlv0l33k06hxly2gxr")))

(define-public crate-algonaut_model-0.4.2 (c (n "algonaut_model") (v "0.4.2") (d (list (d (n "algonaut_core") (r "^0.4.2") (d #t) (k 0)) (d (n "algonaut_crypto") (r "^0.4.2") (d #t) (k 0)) (d (n "algonaut_encoding") (r "^0.4.2") (d #t) (k 0)) (d (n "data-encoding") (r "^2.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11.4") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)) (d (n "serde_with") (r "^1.9.4") (d #t) (k 0)))) (h "0nb0m3wr3xx6qipypnfcmbyrl9yvp12zbabmjl5mxv288yyarai0")))

