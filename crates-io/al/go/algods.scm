(define-module (crates-io al go algods) #:use-module (crates-io))

(define-public crate-algods-0.1.0 (c (n "algods") (v "0.1.0") (d (list (d (n "bitvec") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4.0.9") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0b8ywaz2fcvg1v1gzkn2941nkmnzmx8d0lkh880k0a66911gvrac")))

