(define-module (crates-io al go algod) #:use-module (crates-io))

(define-public crate-Algod-0.1.0 (c (n "Algod") (v "0.1.0") (h "0avl9zzvp9wx90mni6c0iwpsdvd972xcg3g01da1k4ih8p8n3dwh")))

(define-public crate-Algod-0.1.1 (c (n "Algod") (v "0.1.1") (h "14z7bjrnrslqjv54gcy4dxg1vzy77vryqp7qs435v8sp19j9991g")))

