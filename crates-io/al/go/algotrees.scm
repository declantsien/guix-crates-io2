(define-module (crates-io al go algotrees) #:use-module (crates-io))

(define-public crate-algotrees-0.1.0 (c (n "algotrees") (v "0.1.0") (h "1pjvr1vzfnf39iwf032nq4gxl4j2w0a9dj44cx53d2mgzlmh6145")))

(define-public crate-algotrees-0.1.1 (c (n "algotrees") (v "0.1.1") (h "1vlaikybdg8934nwyqhzjb7lc8g579dan15v85p1v42sfmhsgv1m")))

(define-public crate-algotrees-0.1.2 (c (n "algotrees") (v "0.1.2") (h "1afm7jqnspj9nj18izi510s12dajq5n18hgmymkfiavz5ayn8ln7")))

(define-public crate-algotrees-0.1.3 (c (n "algotrees") (v "0.1.3") (h "0hxjjzk12irixkahf9jg48wih7xn7lj62pgg9vbjn9c00qg0kqf6")))

(define-public crate-algotrees-0.1.4 (c (n "algotrees") (v "0.1.4") (h "1a4baa9z15z8i51k30iz5j7vvgszkmsb4vrizl62ic9jq9b0ma31")))

(define-public crate-algotrees-0.1.5 (c (n "algotrees") (v "0.1.5") (h "0vn58iyjpjxs87jvx0b1xy9y3cii8f5w97vvwkjvk0ak864jsk9s")))

(define-public crate-algotrees-0.2.0 (c (n "algotrees") (v "0.2.0") (h "190nlbdsixm6i46s1a5n3xwqxky6svbg41z2rii9ccg61pc9vls7")))

