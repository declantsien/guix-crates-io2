(define-module (crates-io al go algo_no_std) #:use-module (crates-io))

(define-public crate-algo_no_std-0.0.0 (c (n "algo_no_std") (v "0.0.0") (d (list (d (n "heapless") (r "^0.7.16") (d #t) (k 0)) (d (n "itoa") (r "^1.0.9") (d #t) (k 0)))) (h "1vazcibf68c4357yyxy6lahs09asnsk2025v9c936ka2jcb7wfvs")))

(define-public crate-algo_no_std-0.0.1 (c (n "algo_no_std") (v "0.0.1") (d (list (d (n "heapless") (r "^0.7.16") (d #t) (k 0)) (d (n "itoa") (r "^1.0.9") (d #t) (k 0)))) (h "0wk626255z0y1kbm89aax6jwbdv8lk42vbmgpy41b2v7k280rfzz")))

(define-public crate-algo_no_std-0.0.2 (c (n "algo_no_std") (v "0.0.2") (d (list (d (n "heapless") (r "^0.7.16") (d #t) (k 0)) (d (n "itoa") (r "^1.0.9") (d #t) (k 0)))) (h "0dc8jawfwc03y8cb608wcca4ii21mr1yhhggpfw1sydy1mbvzma9")))

