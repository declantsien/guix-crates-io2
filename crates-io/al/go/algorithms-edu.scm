(define-module (crates-io al go algorithms-edu) #:use-module (crates-io))

(define-public crate-algorithms-edu-0.1.0 (c (n "algorithms-edu") (v "0.1.0") (d (list (d (n "num-traits") (r ">=0.2.14, <0.3.0") (d #t) (k 0)))) (h "1hkgwk6ahjq7wn94ay4f58z5a7mx02wql323saiy0j5csfi8i6gf")))

(define-public crate-algorithms-edu-0.1.1 (c (n "algorithms-edu") (v "0.1.1") (d (list (d (n "num-traits") (r ">=0.2.14, <0.3.0") (d #t) (k 0)))) (h "157avfbgd2qkx906cw5a6nimns7834hydl0fl2d2m3y0d7kzk88v")))

(define-public crate-algorithms-edu-0.2.0 (c (n "algorithms-edu") (v "0.2.0") (d (list (d (n "disjoint-sets") (r "^0.4.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "ordered-float") (r "^2.0") (d #t) (k 0)) (d (n "partial-min-max") (r "^0.4.0") (d #t) (k 0)) (d (n "priority-queue") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0gksm2g0c3zsvc06accydypcc3jm451psik92cpwzpv4kb3pvnyj")))

(define-public crate-algorithms-edu-0.2.1 (c (n "algorithms-edu") (v "0.2.1") (d (list (d (n "disjoint-sets") (r "^0.4.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "ordered-float") (r "^2.0") (d #t) (k 0)) (d (n "partial-min-max") (r "^0.4.0") (d #t) (k 0)) (d (n "priority-queue") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0i7fnrxhp98lncmjx8jhhxv70wcmyb3sp1rc1whg72ak45jdvvpd")))

(define-public crate-algorithms-edu-0.2.7 (c (n "algorithms-edu") (v "0.2.7") (d (list (d (n "disjoint-sets") (r "^0.4.2") (d #t) (k 0)) (d (n "kodama") (r "^0.2.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "ordered-float") (r "^2.0") (d #t) (k 0)) (d (n "partial-min-max") (r "^0.4.0") (d #t) (k 0)) (d (n "priority-queue") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "02q03n7kl3mpv6p6fnjiyyz33lfrkxb96wngin8986zs32d11rff")))

