(define-module (crates-io al go algorithmplus) #:use-module (crates-io))

(define-public crate-algorithmplus-0.1.0 (c (n "algorithmplus") (v "0.1.0") (h "014c4fci0bybnnn2pc208nvdagrqr66lh6k9f6311mzb1g299448")))

(define-public crate-algorithmplus-0.1.1 (c (n "algorithmplus") (v "0.1.1") (h "0c1ra502f39c18000cp5783c245mvrm2vgy9qi73a29f31p329zv")))

(define-public crate-algorithmplus-0.1.2 (c (n "algorithmplus") (v "0.1.2") (h "0xh4dpip8wywfra5can4v8rnd0i2k4mqcmh8zpalb04w5rahmfgn")))

(define-public crate-algorithmplus-0.1.3 (c (n "algorithmplus") (v "0.1.3") (h "117132ad8xkmzn29fa8s0lcsabzfv4grxqr5v9wyz2794g47saq2")))

(define-public crate-algorithmplus-0.1.4 (c (n "algorithmplus") (v "0.1.4") (h "1k2hx8b8q8gsjw75ahwpng7dxqi2qqq9flz89ffhgwxgv2lx45v8")))

