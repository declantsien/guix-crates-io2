(define-module (crates-io al go algorithms-illuminated) #:use-module (crates-io))

(define-public crate-Algorithms-Illuminated-0.1.0 (c (n "Algorithms-Illuminated") (v "0.1.0") (h "0a29h55rhq5wjznlmmbkds8s7qqmx44grz74a2zbknbs7rya5v2b")))

(define-public crate-Algorithms-Illuminated-0.1.1 (c (n "Algorithms-Illuminated") (v "0.1.1") (h "1gfiqh2klqlk8nwg9fvdhl3134kjha8xkmndch7y2b75410wmvvd")))

