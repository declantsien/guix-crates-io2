(define-module (crates-io al go algorona) #:use-module (crates-io))

(define-public crate-algorona-0.1.0 (c (n "algorona") (v "0.1.0") (h "0fjxppqjgh4djfvmbga4ldxhlpaq1r8hy49whvbw5sch5mdx5svw")))

(define-public crate-algorona-0.2.0 (c (n "algorona") (v "0.2.0") (h "173p5bwya84dranridmdq2snaz3facgjlpbkjdb9znp3rqr1ig1r")))

