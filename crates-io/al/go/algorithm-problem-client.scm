(define-module (crates-io al go algorithm-problem-client) #:use-module (crates-io))

(define-public crate-algorithm-problem-client-0.1.0 (c (n "algorithm-problem-client") (v "0.1.0") (h "0hmmk6b5js4hk23s4hlgwz7av1fr9bmv0x6iwj1kssig279hx53l")))

(define-public crate-algorithm-problem-client-0.3.1 (c (n "algorithm-problem-client") (v "0.3.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "scraper") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1k84dg7p7bvhx2hfhzmq2dfavs1ikry87nlp60gl6cfjlifd71yh")))

(define-public crate-algorithm-problem-client-0.4.0 (c (n "algorithm-problem-client") (v "0.4.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "scraper") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0rs5xsifzfygmf76i7ivgdy33hpq5sg74dghyvj5dlry5wpg6xsq")))

(define-public crate-algorithm-problem-client-0.4.1 (c (n "algorithm-problem-client") (v "0.4.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "scraper") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "173p1lj9psclw33nz6jxpg5f0gjd8ghnh27ndwg5vz327j5j1wf4")))

(define-public crate-algorithm-problem-client-0.5.0 (c (n "algorithm-problem-client") (v "0.5.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "scraper") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "surf") (r "^1.0") (d #t) (k 0)))) (h "1hmnmjm5fbb5n5xab8js61gjjxi39x6nh57n2ap50xxn0gx4f1py")))

(define-public crate-algorithm-problem-client-0.6.0 (c (n "algorithm-problem-client") (v "0.6.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "scraper") (r "^0.12") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "surf") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1ynfwhvix5rjvqlxyq9cvzrl4x6cv2nx2xja7a6iqhcxf0j6awf6")))

