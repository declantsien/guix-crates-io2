(define-module (crates-io al mo almond) #:use-module (crates-io))

(define-public crate-almond-0.1.0 (c (n "almond") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "insta") (r "^1.1.0") (d #t) (k 2)) (d (n "nom") (r "^6.0.1") (d #t) (k 0)) (d (n "nom_locate") (r "^3.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "unicode-xid") (r "^0.2.1") (d #t) (k 0)))) (h "1yix44371ias959xccbjdsvwb3klp3566c3c5a2kq2f5biyygh7k") (f (quote (("default"))))))

(define-public crate-almond-0.1.1 (c (n "almond") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "insta") (r "^1.1.0") (d #t) (k 2)) (d (n "nom") (r "^6.0.1") (d #t) (k 0)) (d (n "nom_locate") (r "^3.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "unicode-xid") (r "^0.2.1") (d #t) (k 0)))) (h "0sfgcsbiql4axl4654acahk3ylyywlmmchlywqz8jwhrzvniq5nq") (f (quote (("default"))))))

(define-public crate-almond-0.2.0 (c (n "almond") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "insta") (r "^1.1.0") (d #t) (k 2)) (d (n "logos") (r "^0.11.4") (d #t) (k 0)) (d (n "nom") (r "^6.0.1") (d #t) (k 0)) (d (n "nom_locate") (r "^3.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "unicode-xid") (r "^0.2.1") (d #t) (k 0)))) (h "12vsaw7y2z74hyvphdknr6yr4h8pn4i9yxr240q5ar5sv3i08bzc") (f (quote (("default"))))))

