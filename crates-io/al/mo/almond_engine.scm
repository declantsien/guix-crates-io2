(define-module (crates-io al mo almond_engine) #:use-module (crates-io))

(define-public crate-almond_engine-0.1.0 (c (n "almond_engine") (v "0.1.0") (d (list (d (n "almond_macros") (r "^0.1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "fern") (r "^0.6") (f (quote ("colored"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1vhh462ghyzc3i62di5cjl6bxl8br8i5p28dnmx7vz8vm618wrlx") (y #t)))

(define-public crate-almond_engine-0.1.1 (c (n "almond_engine") (v "0.1.1") (d (list (d (n "almond_macros") (r "^0.1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "fern") (r "^0.6") (f (quote ("colored"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1madq1gnvs5iql7apg7drl8c5k4q43nvcnz1xpqg1m7myi658166") (y #t)))

(define-public crate-almond_engine-0.1.3 (c (n "almond_engine") (v "0.1.3") (d (list (d (n "almond_macros") (r "^0.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "fern") (r "^0.6") (f (quote ("colored"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "19vprs2v080vmcfcs531krsvc7av45fcnfk6ygvx7ayynrvs1ji6") (y #t)))

(define-public crate-almond_engine-0.1.4 (c (n "almond_engine") (v "0.1.4") (d (list (d (n "almond_macros") (r "^0.1.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "fern") (r "^0.6") (f (quote ("colored"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1rhy1y5v6jv0ln5rn0zph4gn37aw3ivbfggsl50zgwkpdnyv86f1") (y #t)))

(define-public crate-almond_engine-0.1.5 (c (n "almond_engine") (v "0.1.5") (d (list (d (n "almond_macros") (r "^0.1.2") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "fern") (r "^0.6") (f (quote ("colored"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1n0vj5sghzz5j3xxabj3p7pq1z3l9pwj3hh44wasq38dj84hsp9h") (y #t)))

(define-public crate-almond_engine-0.1.6 (c (n "almond_engine") (v "0.1.6") (d (list (d (n "almond_macros") (r "^0.1.2") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "fern") (r "^0.6") (f (quote ("colored"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "14n0lmdmqhpxxqmajmz9pzgb1vdk23zlj4d7yyrfz2cnsngi4lf1") (y #t)))

(define-public crate-almond_engine-0.1.7 (c (n "almond_engine") (v "0.1.7") (d (list (d (n "almond_macros") (r "^0.1.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "fern") (r "^0.6") (f (quote ("colored"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "11gkj7jrn76zngs65q9fxgi89bnp9z9kykbh9lqgww17wwj8j0z3") (y #t)))

(define-public crate-almond_engine-0.1.9 (c (n "almond_engine") (v "0.1.9") (d (list (d (n "almond_macros") (r "^0.1.3") (o #t) (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "fern") (r "^0.6") (f (quote ("colored"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1bphg30gh1a6qzvnzsi9rn4fppiiwwydagz8kwwaslsn72is531c") (y #t) (s 2) (e (quote (("macros" "dep:almond_macros"))))))

