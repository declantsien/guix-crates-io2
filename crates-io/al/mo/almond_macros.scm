(define-module (crates-io al mo almond_macros) #:use-module (crates-io))

(define-public crate-almond_macros-0.1.0 (c (n "almond_macros") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("full"))) (d #t) (k 0)))) (h "1p5z96npjxbzl9zhz5jsvgad65ikzr4sa0w013xblgv8zsskg91l") (y #t)))

(define-public crate-almond_macros-0.1.1 (c (n "almond_macros") (v "0.1.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("full"))) (d #t) (k 0)))) (h "1irkmayl06pwamhwn39iyrrn00w4nadg4s6mf4yygjzdmkp5s1q0") (y #t)))

(define-public crate-almond_macros-0.1.2 (c (n "almond_macros") (v "0.1.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("full"))) (d #t) (k 0)))) (h "18s0s6n32qw7s252b85izw6ypfsq9l3c01qprrx5bx9bnnbaq70n") (y #t)))

(define-public crate-almond_macros-0.1.3 (c (n "almond_macros") (v "0.1.3") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("full"))) (d #t) (k 0)))) (h "0lsd1js66qlzmg51scngwdwxgzlqpnyc6vlrgjj5fcmh62xhb9rx") (y #t)))

