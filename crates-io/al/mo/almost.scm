(define-module (crates-io al mo almost) #:use-module (crates-io))

(define-public crate-almost-0.0.1 (c (n "almost") (v "0.0.1") (h "0444zmyv4g2yli1jgc9dm2q07hbk2xvx9m25zxymf2sz8gag6v4s")))

(define-public crate-almost-0.1.0 (c (n "almost") (v "0.1.0") (d (list (d (n "ieee754") (r "^0.2.6") (d #t) (k 2)) (d (n "rand") (r "^0.6.5") (k 2)))) (h "0k9yrj1hj0wqjfscl0lypscdxw2jnz77825420spp9wnbw19prcv")))

(define-public crate-almost-0.2.0 (c (n "almost") (v "0.2.0") (d (list (d (n "ieee754") (r "^0.2.6") (d #t) (k 2)) (d (n "rand") (r "^0.6.5") (k 2)))) (h "051w1bwq0zh298blxgqbnq9ydmvqcx20vly2cnximy3anjg9k8is")))

