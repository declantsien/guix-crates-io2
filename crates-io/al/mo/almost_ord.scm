(define-module (crates-io al mo almost_ord) #:use-module (crates-io))

(define-public crate-almost_ord-0.1.0 (c (n "almost_ord") (v "0.1.0") (h "0z3ldwbh8apw6wsg138f7lm4k20hgpfj07bvjjd0zgs86nf076dr") (y #t)))

(define-public crate-almost_ord-0.2.0 (c (n "almost_ord") (v "0.2.0") (h "0ncxyivi7srzj9kz7v0nzrmiw8dlyxhh2szaqhcvrk4rpf7f8m9s") (y #t)))

(define-public crate-almost_ord-0.2.1 (c (n "almost_ord") (v "0.2.1") (h "16cvldbphw2yzcy0gfqm0r6hmvv0i3s8rzlb8l91dml05lwdp4v7") (y #t)))

(define-public crate-almost_ord-0.2.2 (c (n "almost_ord") (v "0.2.2") (h "0dzszlbffi9hgvm6icmdapdhv9rjmnc2av8v5k9x8lmwlca99l0b") (y #t)))

