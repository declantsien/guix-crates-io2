(define-module (crates-io al pm alpm-rs) #:use-module (crates-io))

(define-public crate-alpm-rs-0.1.0 (c (n "alpm-rs") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.48") (d #t) (k 0)))) (h "15iv5irhbnfzvy2fbch8hr4i2vz425m0zqdwzkn3m68pqqlxfhln") (y #t)))

(define-public crate-alpm-rs-0.1.1 (c (n "alpm-rs") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.48") (d #t) (k 0)))) (h "0460qkfmgkmrrmn58mzhzaiwxfyiaf9s40m7b28lqbvmnp7qsb3x")))

(define-public crate-alpm-rs-0.1.2 (c (n "alpm-rs") (v "0.1.2") (d (list (d (n "libc") (r "^0.2.48") (d #t) (k 0)))) (h "18p2iifz3n1q0wiwhbwi8l9ybddlf4mr7ah5i0h0w6438hhzxf48")))

(define-public crate-alpm-rs-0.1.3 (c (n "alpm-rs") (v "0.1.3") (d (list (d (n "libc") (r "^0.2.48") (d #t) (k 0)))) (h "1gbcf39cdk56ivgf36msg74jn1axgfk2irr0asn9ii0gy72lygdj")))

(define-public crate-alpm-rs-0.1.4 (c (n "alpm-rs") (v "0.1.4") (d (list (d (n "libc") (r "^0.2.48") (d #t) (k 0)) (d (n "va_list") (r "^0.1.3") (d #t) (k 0)))) (h "02knvpyb5d12bc24hlrhbhw58zxqrcwkviambw9l5iv3gmgcvgbl")))

(define-public crate-alpm-rs-0.1.5 (c (n "alpm-rs") (v "0.1.5") (d (list (d (n "libc") (r "^0.2.48") (d #t) (k 0)) (d (n "va_list") (r "^0.1.3") (d #t) (k 0)))) (h "1f7hzya13nia6v7mm64m6439yi5k1xa9dr7qiscc3s91dx4vr38q")))

(define-public crate-alpm-rs-0.1.6 (c (n "alpm-rs") (v "0.1.6") (d (list (d (n "libc") (r "^0.2.48") (d #t) (k 0)) (d (n "va_list") (r "^0.1.3") (d #t) (k 0)))) (h "0yqkh381s6mi7yincqj72m1dbnvfs08dh33pplawh6gq086zkpai")))

(define-public crate-alpm-rs-0.1.7 (c (n "alpm-rs") (v "0.1.7") (d (list (d (n "libc") (r "^0.2.48") (d #t) (k 0)) (d (n "va_list") (r "^0.1.3") (d #t) (k 0)))) (h "0bax42mqkyzn83ay84f5vjhaiscinbbp79z73vf8ghq2dlm5fj79")))

(define-public crate-alpm-rs-0.1.8 (c (n "alpm-rs") (v "0.1.8") (d (list (d (n "libc") (r "^0.2.48") (d #t) (k 0)))) (h "0v9m1zadin03wp2zv4zhxwlga64kf2wfqx17c7j0gia4xvalwmsz")))

(define-public crate-alpm-rs-0.1.9 (c (n "alpm-rs") (v "0.1.9") (d (list (d (n "libc") (r "^0.2.48") (d #t) (k 0)))) (h "0nzfzm11yxsna3jr1y324nlp3lg7vc0fq9b9gb8sizvd1by1n03m")))

(define-public crate-alpm-rs-0.1.10 (c (n "alpm-rs") (v "0.1.10") (d (list (d (n "libc") (r "^0.2.48") (d #t) (k 0)))) (h "1il42i5jdrq049aar29dglglpiz677vkc1m4g3lxzkx0wx6dm9pg")))

(define-public crate-alpm-rs-0.1.11 (c (n "alpm-rs") (v "0.1.11") (d (list (d (n "libc") (r "^0.2.48") (d #t) (k 0)))) (h "01mrymziyrd4jwdaw2vqlrz1yzn0x9w905y0g4ym27r6ilpmlg52")))

(define-public crate-alpm-rs-0.1.12 (c (n "alpm-rs") (v "0.1.12") (d (list (d (n "libc") (r "^0.2.48") (d #t) (k 0)))) (h "1y7iwrf8k5ym5f1svx4f7vmzsvmbzqrkmqywivkdgjbbahaj60vp")))

(define-public crate-alpm-rs-0.1.13 (c (n "alpm-rs") (v "0.1.13") (d (list (d (n "libc") (r "^0.2.48") (d #t) (k 0)))) (h "0i05icjy00ai475hl6jr040v798zc00v4lsllqxxk8094x3vp2k8")))

(define-public crate-alpm-rs-0.1.14 (c (n "alpm-rs") (v "0.1.14") (d (list (d (n "libc") (r "^0.2.48") (d #t) (k 0)))) (h "0za8h23sz323cr283mrzjf4cz71dls5xgkdc98b3g2ilckk774dk")))

(define-public crate-alpm-rs-0.1.15 (c (n "alpm-rs") (v "0.1.15") (d (list (d (n "libc") (r "^0.2.48") (d #t) (k 0)))) (h "1v4vjd64p3rvm0480zlj8z80fnmcjpanqr0yy7vdpyr25nsd53ah")))

(define-public crate-alpm-rs-0.1.16 (c (n "alpm-rs") (v "0.1.16") (d (list (d (n "libc") (r "^0.2.48") (d #t) (k 0)))) (h "00951bv905mh6a05qr92qsx1b2mdvri490vgjrhnmi96rvwc9xsi")))

(define-public crate-alpm-rs-0.1.17 (c (n "alpm-rs") (v "0.1.17") (d (list (d (n "libc") (r "^0.2.48") (d #t) (k 0)))) (h "0r50x4vnxvwng5lyzcgxk0430phjbs735ndckd986k1bypy0qxli")))

(define-public crate-alpm-rs-0.1.19 (c (n "alpm-rs") (v "0.1.19") (d (list (d (n "libc") (r "^0.2.48") (d #t) (k 0)))) (h "09cr6d7i369hki73rkp0l1pj443hn8fghx9igbckdm2w6kcp0y38")))

(define-public crate-alpm-rs-0.1.20 (c (n "alpm-rs") (v "0.1.20") (d (list (d (n "libc") (r "^0.2.48") (d #t) (k 0)))) (h "1pdww5kybn7sb8v85shschc76wzpvpbnd58hlvzkdbnl6qi8h36s")))

(define-public crate-alpm-rs-0.1.21 (c (n "alpm-rs") (v "0.1.21") (d (list (d (n "libc") (r "^0.2.48") (d #t) (k 0)))) (h "1gjwxcn95az84ai4i83bzlm7vlg55by4wya9a99k46p9y5f4fk8q")))

(define-public crate-alpm-rs-0.1.22 (c (n "alpm-rs") (v "0.1.22") (d (list (d (n "libc") (r "^0.2.48") (d #t) (k 0)))) (h "0irvjf558sb31givdql5syl90s5ivcssdm2iym6znv71rijjbyzv")))

(define-public crate-alpm-rs-0.1.24 (c (n "alpm-rs") (v "0.1.24") (d (list (d (n "libc") (r "^0.2.48") (d #t) (k 0)))) (h "0nx4nq7m0gz0gg3m3371gfmlj99m42hd7cabvm1lvkfl8687dcc5")))

