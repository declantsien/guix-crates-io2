(define-module (crates-io al pm alpmver) #:use-module (crates-io))

(define-public crate-alpmver-0.1.0 (c (n "alpmver") (v "0.1.0") (d (list (d (n "clap") (r "^3.2.20") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "nom") (r "^7.1.1") (d #t) (k 0)))) (h "1acc5lx6isi2chd61vxl8xxjfx7sl8wghpmzs8pl4mlwgdrfvisd") (f (quote (("cli" "clap")))) (y #t)))

(define-public crate-alpmver-0.1.1 (c (n "alpmver") (v "0.1.1") (d (list (d (n "clap") (r "^3.2.20") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "nom") (r "^7.1.1") (d #t) (k 0)))) (h "1p03yzfxsjn82xiyq68318177887xy6iahhdn51vyl64iy5v50xc") (f (quote (("cli" "clap"))))))

(define-public crate-alpmver-0.1.2 (c (n "alpmver") (v "0.1.2") (d (list (d (n "clap") (r "^3.2.20") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "nom") (r "^7.1.1") (d #t) (k 0)))) (h "1hqhhw8qangbsamwam9fvjsyv177wn3bb6f56d9rnzh3dd876dl8") (f (quote (("cli" "clap"))))))

(define-public crate-alpmver-0.1.3 (c (n "alpmver") (v "0.1.3") (d (list (d (n "clap") (r "^3.2.20") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "nom") (r "^7.1.1") (d #t) (k 0)))) (h "0rgki89c9d1d1rwjqqiwrsafqvj1qz5b28ngmx873m2vcww5m59c") (f (quote (("cli" "clap"))))))

