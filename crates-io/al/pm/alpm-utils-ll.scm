(define-module (crates-io al pm alpm-utils-ll) #:use-module (crates-io))

(define-public crate-alpm-utils-ll-2.0.0 (c (n "alpm-utils-ll") (v "2.0.0") (d (list (d (n "alpm-ll") (r "^3.0.0") (o #t) (d #t) (k 0)) (d (n "pacmanconf") (r "^2.0.0") (o #t) (d #t) (k 0)))) (h "1l4xbknzplc25grwcsh49l32zgk3zr1frzwfd06mg1z0404r2iy7") (f (quote (("generate" "alpm-ll/generate") ("docs-rs" "alpm-ll/docs-rs") ("default" "alpm-ll" "conf") ("conf" "pacmanconf" "alpm-ll"))))))

