(define-module (crates-io al pm alpm-sys-ll) #:use-module (crates-io))

(define-public crate-alpm-sys-ll-3.0.0 (c (n "alpm-sys-ll") (v "3.0.0") (d (list (d (n "bindgen") (r "^0.60") (o #t) (d #t) (k 1)) (d (n "libloading") (r "^0.7") (d #t) (k 0)))) (h "16mq93v8b2ar9xkfadvnwv083c9mni6l7pqablpnviryxwj2f99q") (f (quote (("generate" "bindgen") ("docs-rs"))))))

