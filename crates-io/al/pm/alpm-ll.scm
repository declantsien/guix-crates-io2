(define-module (crates-io al pm alpm-ll) #:use-module (crates-io))

(define-public crate-alpm-ll-3.0.0 (c (n "alpm-ll") (v "3.0.0") (d (list (d (n "alpm-sys-ll") (r "^3.0.0") (d #t) (k 0)) (d (n "alpm-sys-ll") (r "^3.0.0") (d #t) (k 1)) (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "libarchive") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "libarchive3-sys") (r "^0.1.2") (o #t) (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)) (d (n "once_cell") (r "^1.13") (d #t) (k 0)))) (h "04nbf7d2r1lan98dagjxkjhhav6b53vn81szmnhgq3aszzsgdnk8") (f (quote (("mtree" "libarchive" "libarchive3-sys") ("generate" "alpm-sys-ll/generate") ("docs-rs" "alpm-sys-ll/docs-rs") ("default") ("checkver"))))))

