(define-module (crates-io al pm alpm-sys-fork) #:use-module (crates-io))

(define-public crate-alpm-sys-fork-0.1.2 (c (n "alpm-sys-fork") (v "0.1.2") (d (list (d (n "libarchive3-sys") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "19iqx440mxsxdlfiwv4y10243pp8b2xhzj7wgz1mwajckvrggw0x") (y #t)))

(define-public crate-alpm-sys-fork-0.2.0 (c (n "alpm-sys-fork") (v "0.2.0") (h "1lyk48z9z0lklzavjrz11r1l9c87199p0y80hyhz5xfq5jyzzijj") (f (quote (("git")))) (y #t)))

(define-public crate-alpm-sys-fork-0.2.1 (c (n "alpm-sys-fork") (v "0.2.1") (h "1w9m9xsfgb6drx65jimnspvmi452kkv6cg67mbmilhkzvbs93yrb") (f (quote (("git")))) (y #t)))

(define-public crate-alpm-sys-fork-0.3.0 (c (n "alpm-sys-fork") (v "0.3.0") (h "0fppgzypcl2kgpbrpj8n8cd5axj5f5p27ikvs2nr2m4jpbsyj7mc") (f (quote (("git")))) (y #t)))

(define-public crate-alpm-sys-fork-1.0.0 (c (n "alpm-sys-fork") (v "1.0.0") (h "0403qha13rwqhxdxj8hdsy2pmgjhx985dlb5qk466sqaxbgmayzc") (f (quote (("git")))) (y #t)))

(define-public crate-alpm-sys-fork-1.1.0 (c (n "alpm-sys-fork") (v "1.1.0") (h "1hwb0f22rkn689l4i1l1zksp7cyavzcx2dg8nl5bfrqwm45hwqm2") (f (quote (("git")))) (y #t)))

