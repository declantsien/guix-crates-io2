(define-module (crates-io al pm alpm-sys) #:use-module (crates-io))

(define-public crate-alpm-sys-0.1.0 (c (n "alpm-sys") (v "0.1.0") (d (list (d (n "libarchive3-sys") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0cfk93n8fqrf0l88bxnk773shmbrwkc1b25qa5cgc72wjv2jja7j")))

(define-public crate-alpm-sys-0.1.1 (c (n "alpm-sys") (v "0.1.1") (d (list (d (n "libarchive3-sys") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "18a2f5arh91j2w442g06jpln9h3whxivzgl3jnpl9cim3yviw3fl")))

(define-public crate-alpm-sys-0.1.2 (c (n "alpm-sys") (v "0.1.2") (d (list (d (n "libarchive3-sys") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0fwh12y81s181bwacjjvq16whzgwv7dqkmil9smkgqivamnsk7d5")))

(define-public crate-alpm-sys-1.0.0 (c (n "alpm-sys") (v "1.0.0") (h "0h54kr0xa7a2qyqp52z7yvk3m1pavdxjmmvli87l7qdg1m9f3w0i") (f (quote (("git"))))))

(define-public crate-alpm-sys-1.1.0 (c (n "alpm-sys") (v "1.1.0") (d (list (d (n "bindgen") (r "^0.55.1") (o #t) (d #t) (k 1)))) (h "1fgha9ask8cw6q4aszrsmgl0l64ma6mi7g0gwsnfxim61wibc0wd") (f (quote (("git") ("generate" "bindgen"))))))

(define-public crate-alpm-sys-1.1.1 (c (n "alpm-sys") (v "1.1.1") (d (list (d (n "bindgen") (r "^0.55.1") (f (quote ("runtime"))) (o #t) (k 1)))) (h "08rbnvccz5nphag275p49dmkvhzx5i290h1067g0dv41wyy23v5p") (f (quote (("git") ("generate" "bindgen"))))))

(define-public crate-alpm-sys-1.1.2 (c (n "alpm-sys") (v "1.1.2") (d (list (d (n "bindgen") (r ">=0.55.1, <0.56.0") (f (quote ("runtime"))) (o #t) (k 1)))) (h "0s1j1p04qic7fpdpzb5mvraz7xpjg5d9206kxlkiq55ds4b2p1v5") (f (quote (("git") ("generate" "bindgen"))))))

(define-public crate-alpm-sys-1.1.3 (c (n "alpm-sys") (v "1.1.3") (d (list (d (n "bindgen") (r "^0.55.1") (f (quote ("runtime"))) (o #t) (k 1)))) (h "1nnl501lqmgbra8gfr20sk65wja7jmnszhpp0sjzjsl7zwcji12i") (f (quote (("git") ("generate" "bindgen"))))))

(define-public crate-alpm-sys-1.1.4 (c (n "alpm-sys") (v "1.1.4") (d (list (d (n "bindgen") (r "^0.55.1") (f (quote ("runtime"))) (o #t) (k 1)))) (h "1xf80xisdf9p57zp26jp8m4xcx66b72hy05wls7bvnlz6igs1r6n") (f (quote (("git") ("generate" "bindgen"))))))

(define-public crate-alpm-sys-1.1.5 (c (n "alpm-sys") (v "1.1.5") (d (list (d (n "bindgen") (r "^0.55.1") (f (quote ("runtime"))) (o #t) (k 1)))) (h "08hwm60y6sc1ysyn8gzxz6v42j0c5vq93jif1nppm3fa3fifbq2c") (f (quote (("git") ("generate" "bindgen"))))))

(define-public crate-alpm-sys-1.1.6 (c (n "alpm-sys") (v "1.1.6") (d (list (d (n "bindgen") (r "^0.55.1") (f (quote ("runtime"))) (o #t) (k 1)))) (h "0n2ncmcxljv2p1gy0gcbasx0dpcp446lam97zii1dffqcraih0ds") (f (quote (("git") ("generate" "bindgen"))))))

(define-public crate-alpm-sys-1.1.7 (c (n "alpm-sys") (v "1.1.7") (d (list (d (n "bindgen") (r "^0.55.1") (f (quote ("runtime"))) (o #t) (k 1)))) (h "1anvkp1vsazdfsskbr6l35i8h3468sfhsqsii6916lvhgaydimd5") (f (quote (("git") ("generate" "bindgen"))))))

(define-public crate-alpm-sys-1.1.8 (c (n "alpm-sys") (v "1.1.8") (d (list (d (n "bindgen") (r "^0.55.1") (f (quote ("runtime"))) (o #t) (k 1)))) (h "13jw4w0dqkngqw1m72vzx1id55jh5pijyidzi5bzdibwlzs5szvg") (f (quote (("git") ("generate" "bindgen"))))))

(define-public crate-alpm-sys-1.1.9 (c (n "alpm-sys") (v "1.1.9") (d (list (d (n "bindgen") (r "^0.55.1") (f (quote ("runtime"))) (o #t) (k 1)))) (h "1d7q8wz0hr8wm3q4ixmxh4yv1d0x018r5wjp9pxwbwchk7qzbx5r") (f (quote (("git") ("generate" "bindgen"))))))

(define-public crate-alpm-sys-2.0.0 (c (n "alpm-sys") (v "2.0.0") (d (list (d (n "bindgen") (r "^0.55.1") (f (quote ("runtime"))) (o #t) (k 1)))) (h "0dia1aydb7j4vi76bkw4achx0hailgw1v6sr0r95x15wq3bpbjbb") (f (quote (("git") ("generate" "bindgen"))))))

(define-public crate-alpm-sys-2.1.0 (c (n "alpm-sys") (v "2.1.0") (d (list (d (n "bindgen") (r "^0.55.1") (f (quote ("runtime"))) (o #t) (k 1)) (d (n "pkg-config") (r "^0.3.19") (d #t) (k 1)))) (h "0i2p7yccfldx9cyhw5dghm6glyvb7saww8j03s30j5qc0bgwgvfq") (f (quote (("static") ("git") ("generate" "bindgen"))))))

(define-public crate-alpm-sys-2.1.1 (c (n "alpm-sys") (v "2.1.1") (d (list (d (n "bindgen") (r "^0.55.1") (f (quote ("runtime"))) (o #t) (k 1)) (d (n "pkg-config") (r "^0.3.19") (d #t) (k 1)))) (h "1p7946ljb5m2wd9waf071447bb1xhj7a63m8i93bwvxrkcj2fwb4") (f (quote (("static") ("git") ("generate" "bindgen"))))))

(define-public crate-alpm-sys-2.1.2 (c (n "alpm-sys") (v "2.1.2") (d (list (d (n "bindgen") (r "^0.55.1") (f (quote ("runtime"))) (o #t) (k 1)) (d (n "pkg-config") (r "^0.3.19") (d #t) (k 1)))) (h "1w7cxqbk86xjlj5f91snh7bzxgi88j8dgvrsy3nyr1a693iszqgy") (f (quote (("static") ("git") ("generate" "bindgen") ("docs-rs"))))))

(define-public crate-alpm-sys-2.1.3 (c (n "alpm-sys") (v "2.1.3") (d (list (d (n "bindgen") (r "^0.66.1") (f (quote ("runtime"))) (o #t) (k 1)) (d (n "pkg-config") (r "^0.3.27") (d #t) (k 1)))) (h "13ad3rab8kj6x5w1za96c3cm0pivndyhmfkji6a91542i12676bf") (f (quote (("static") ("git") ("generate" "bindgen") ("docs-rs"))))))

(define-public crate-alpm-sys-3.0.0 (c (n "alpm-sys") (v "3.0.0") (d (list (d (n "bindgen") (r "^0.66.1") (f (quote ("runtime"))) (o #t) (k 1)) (d (n "pkg-config") (r "^0.3.30") (d #t) (k 1)))) (h "1g8hn1a06dih9hv5xggk009pkb88i3p0diihdswd81jsy467x888") (f (quote (("static") ("git") ("generate" "bindgen") ("docs-rs"))))))

