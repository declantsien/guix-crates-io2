(define-module (crates-io al pm alpm-utils) #:use-module (crates-io))

(define-public crate-alpm-utils-0.1.0 (c (n "alpm-utils") (v "0.1.0") (d (list (d (n "alpm") (r "^0.6.0") (d #t) (k 0)))) (h "0n3x6baw0h5snlq72vfd7wmdcanynfplgxr91wf17r8dv5lh9qi4") (f (quote (("git" "alpm/git"))))))

(define-public crate-alpm-utils-0.1.1 (c (n "alpm-utils") (v "0.1.1") (d (list (d (n "alpm") (r "^0.6.0") (d #t) (k 0)))) (h "1p4v4727jvdzpfar83i7k1lcsdnh9xff82zd3qkh9v5znfwc8v7d") (f (quote (("git" "alpm/git"))))))

(define-public crate-alpm-utils-0.2.0 (c (n "alpm-utils") (v "0.2.0") (d (list (d (n "alpm") (r "^0.9.0") (d #t) (k 0)))) (h "01j5pcrj3w27apf9c7mpg85j7awzd9qdik2hvbx318r6x8iyk0qg") (f (quote (("git" "alpm/git"))))))

(define-public crate-alpm-utils-0.2.1 (c (n "alpm-utils") (v "0.2.1") (d (list (d (n "alpm") (r "^0.9.0") (d #t) (k 0)))) (h "0cpsprn5w64c7whwsvjc66w3lpfihzx7d38wlb7xxxwbdpfkkgww") (f (quote (("git" "alpm/git"))))))

(define-public crate-alpm-utils-0.3.0 (c (n "alpm-utils") (v "0.3.0") (d (list (d (n "alpm") (r "^0.10.0") (d #t) (k 0)))) (h "192fxjbzlc9q1chh5cjd3h5qywh8a3hl7rjyzgqylk5k8nr36j8q") (f (quote (("git" "alpm/git"))))))

(define-public crate-alpm-utils-0.3.1 (c (n "alpm-utils") (v "0.3.1") (d (list (d (n "alpm") (r "^0.10.0") (d #t) (k 0)))) (h "10yp9nq4117fgmicssqd16169c877cfz7qkz6zyxg68rh17rxrg8") (f (quote (("git" "alpm/git"))))))

(define-public crate-alpm-utils-0.4.0 (c (n "alpm-utils") (v "0.4.0") (d (list (d (n "alpm") (r "^1.0.0-rc1") (d #t) (k 0)))) (h "1lkncj7bw1chzfp7qg9f0ll06n2ffkx8rv2b7ipjjx262ibwqvfc") (f (quote (("git" "alpm/git") ("generate" "alpm/generate"))))))

(define-public crate-alpm-utils-0.5.0 (c (n "alpm-utils") (v "0.5.0") (d (list (d (n "alpm") (r "^1.0.0-rc1") (d #t) (k 0)))) (h "1s1ni6xiw3qd6sw3s6h71rcvkilnh1fmiv6fq72wg1dx4cf20cyv") (f (quote (("git" "alpm/git") ("generate" "alpm/generate"))))))

(define-public crate-alpm-utils-0.6.0 (c (n "alpm-utils") (v "0.6.0") (d (list (d (n "alpm") (r "^1.0.0") (d #t) (k 0)))) (h "11hra2g4p2zvz2vg369l9hngis0ms8v8c25n9fxdasljjmd2jwiy") (f (quote (("git" "alpm/git") ("generate" "alpm/generate"))))))

(define-public crate-alpm-utils-0.6.1 (c (n "alpm-utils") (v "0.6.1") (d (list (d (n "alpm") (r "^1.0.0") (o #t) (d #t) (k 0)))) (h "1s5lafx9v8pr50nnbigs2dryc4sdx63dsc6ibqxnp1qs1gs6n6ik") (f (quote (("git" "alpm/git") ("generate" "alpm/generate") ("default" "alpm"))))))

(define-public crate-alpm-utils-0.6.2 (c (n "alpm-utils") (v "0.6.2") (d (list (d (n "alpm") (r "^1.0.0") (o #t) (d #t) (k 0)))) (h "1ai12i61w3r62vgn4k1qw5v3sc12brkh5pkjslgw0na03050n55i") (f (quote (("git" "alpm/git") ("generate" "alpm/generate") ("default" "alpm"))))))

(define-public crate-alpm-utils-0.7.0 (c (n "alpm-utils") (v "0.7.0") (d (list (d (n "alpm") (r "^2.0.0") (o #t) (d #t) (k 0)))) (h "02r6z53si49indgr1kk4z4302ya61jlfk780maln4jzl9r2nak19") (f (quote (("git" "alpm/git") ("generate" "alpm/generate") ("default" "alpm"))))))

(define-public crate-alpm-utils-1.0.0 (c (n "alpm-utils") (v "1.0.0") (d (list (d (n "alpm") (r "^2.0.0") (o #t) (d #t) (k 0)) (d (n "pacmanconf") (r "^1.0.0") (o #t) (d #t) (k 0)))) (h "0xn812qmyksq0blhya1zwwvxjxp2vavrznsld7g6kcw9hlyvl6jk") (f (quote (("git" "alpm/git") ("generate" "alpm/generate") ("default" "alpm" "conf") ("conf" "pacmanconf" "alpm"))))))

(define-public crate-alpm-utils-1.1.0 (c (n "alpm-utils") (v "1.1.0") (d (list (d (n "alpm") (r "^2.1.0") (o #t) (d #t) (k 0)) (d (n "pacmanconf") (r "^1.0.0") (o #t) (d #t) (k 0)))) (h "0h9cf57nmz1mppghbjbzjfrk4wwlnkbb5cd0kqnh2ascnz7x3bhh") (f (quote (("static" "alpm/static") ("git" "alpm/git") ("generate" "alpm/generate") ("default" "alpm" "conf") ("conf" "pacmanconf" "alpm"))))))

(define-public crate-alpm-utils-1.1.2 (c (n "alpm-utils") (v "1.1.2") (d (list (d (n "alpm") (r "^2.1.2") (o #t) (d #t) (k 0)) (d (n "pacmanconf") (r "^1.0.0") (o #t) (d #t) (k 0)))) (h "005756mlgbx8x4d3hyrj2jj2izmmv0acfvmnrvx41qa8kvarqch9") (f (quote (("static" "alpm/static") ("git" "alpm/git") ("generate" "alpm/generate") ("docs-rs" "alpm/docs-rs") ("default" "alpm" "conf") ("conf" "pacmanconf" "alpm"))))))

(define-public crate-alpm-utils-2.0.0 (c (n "alpm-utils") (v "2.0.0") (d (list (d (n "alpm") (r "^2.1.2") (o #t) (d #t) (k 0)) (d (n "pacmanconf") (r "^2.0.0") (o #t) (d #t) (k 0)))) (h "0chj74g50m0sx2nyjdg5ngsrc338h2dfk7ky01l0h07dghm8kih1") (f (quote (("static" "alpm/static") ("git" "alpm/git") ("generate" "alpm/generate") ("docs-rs" "alpm/docs-rs") ("default" "alpm" "conf") ("conf" "pacmanconf" "alpm"))))))

(define-public crate-alpm-utils-2.0.1 (c (n "alpm-utils") (v "2.0.1") (d (list (d (n "alpm") (r "^2.1.3") (o #t) (d #t) (k 0)) (d (n "pacmanconf") (r "^2.0.0") (o #t) (d #t) (k 0)))) (h "1r9026a4zjx7wmy082bjsgk6ljkvc5amp4dgii16i5cz92wg516x") (f (quote (("static" "alpm/static") ("git" "alpm/git") ("generate" "alpm/generate") ("docs-rs" "alpm/docs-rs") ("default" "alpm" "conf") ("conf" "pacmanconf" "alpm"))))))

(define-public crate-alpm-utils-3.0.0 (c (n "alpm-utils") (v "3.0.0") (d (list (d (n "alpm") (r "^3.0.0") (o #t) (d #t) (k 0)) (d (n "pacmanconf") (r "^2.0.0") (o #t) (d #t) (k 0)))) (h "0wkqszr2akizkvb9ici9l3p4r7vqr3kklskwznff6y4px9zg16jb") (f (quote (("static" "alpm/static") ("git" "alpm/git") ("generate" "alpm/generate") ("docs-rs" "alpm/docs-rs") ("default" "alpm" "conf") ("conf" "pacmanconf" "alpm"))))))

(define-public crate-alpm-utils-3.0.2 (c (n "alpm-utils") (v "3.0.2") (d (list (d (n "alpm") (r "^3.0.4") (o #t) (d #t) (k 0)) (d (n "pacmanconf") (r "^2.0.0") (o #t) (d #t) (k 0)))) (h "1rrpzryi5imdzl13l4v6v76vd5nl6dkl6nl6gclx2l59p545x9pi") (f (quote (("static" "alpm/static") ("git" "alpm/git") ("generate" "alpm/generate") ("docs-rs" "alpm/docs-rs") ("default" "alpm" "conf") ("conf" "pacmanconf" "alpm"))))))

