(define-module (crates-io al so also_sync_macros) #:use-module (crates-io))

(define-public crate-also_sync_macros-0.1.0 (c (n "also_sync_macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0mvndxihk7n3hf0dq15vrv5wbjsz4r9vadnpps0604pkndchnhxf") (f (quote (("tokio") ("default" "tokio"))))))

