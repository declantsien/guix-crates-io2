(define-module (crates-io al so also_useless_from_container) #:use-module (crates-io))

(define-public crate-also_useless_from_container-0.1.0 (c (n "also_useless_from_container") (v "0.1.0") (h "0vjx54gibrbhq3qrbq79g2v2prrwrhfwmywf4zwilgq04ipd2pk4")))

(define-public crate-also_useless_from_container-0.2.0 (c (n "also_useless_from_container") (v "0.2.0") (h "0lw32hlc7bv7j0r7759cbx7yb9pg7x11w4zl4bcmv5q5fwbj8xh8")))

(define-public crate-also_useless_from_container-0.3.0 (c (n "also_useless_from_container") (v "0.3.0") (h "0k8c5yiq5q4axhbialdgd2kwd05hpha1vmxybm5dd0a86swc3z8w")))

(define-public crate-also_useless_from_container-0.4.0 (c (n "also_useless_from_container") (v "0.4.0") (h "1x1dg92zcbdxmv4zwad3l6xvp73nfch0dcw6c15f98zvmny4yr2l")))

