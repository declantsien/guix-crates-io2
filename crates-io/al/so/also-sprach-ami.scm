(define-module (crates-io al so also-sprach-ami) #:use-module (crates-io))

(define-public crate-also-sprach-ami-0.1.0 (c (n "also-sprach-ami") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tungstenite") (r "^0.11.1") (d #t) (k 0)) (d (n "url") (r "^2.1") (d #t) (k 0)))) (h "053rnsfrhm0zw55i4lri881k528xagi7hfisadv262gzfqnyxm9x")))

