(define-module (crates-io al so also_sync) #:use-module (crates-io))

(define-public crate-also_sync-0.1.0 (c (n "also_sync") (v "0.1.0") (d (list (d (n "also_sync_macros") (r "^0.1.0") (d #t) (k 0)) (d (n "async-dns") (r "^0.1.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 2)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "1448284gn9m4sn13h6i3g0ixq7753xslgaa70v4baycna1yjzc0i") (f (quote (("default" "tokio")))) (s 2) (e (quote (("tokio" "dep:tokio" "also_sync_macros/tokio"))))))

