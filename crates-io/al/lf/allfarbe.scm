(define-module (crates-io al lf allfarbe) #:use-module (crates-io))

(define-public crate-allfarbe-0.1.0 (c (n "allfarbe") (v "0.1.0") (d (list (d (n "glium") (r "^0.27.0") (d #t) (k 0)) (d (n "noise") (r "^0.6.0") (d #t) (k 0)))) (h "1vsby86sx1jh09sgb9kc6p0j3vymj3vlh8xqgaxl2gb3i708zknh")))

(define-public crate-allfarbe-0.1.1 (c (n "allfarbe") (v "0.1.1") (d (list (d (n "glium") (r "^0.27.0") (d #t) (k 0)) (d (n "noise") (r "^0.6.0") (d #t) (k 0)))) (h "0zmpd407vvkq7cp2a18asl2wwnbsf3bnl9ri6b2yh4ngg025pjgd")))

