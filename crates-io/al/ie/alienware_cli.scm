(define-module (crates-io al ie alienware_cli) #:use-module (crates-io))

(define-public crate-alienware_cli-0.1.0 (c (n "alienware_cli") (v "0.1.0") (d (list (d (n "alienware") (r "^0.1.0") (d #t) (k 0)) (d (n "argparse") (r "^0.2.2") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "0w9zw0hbp4vjcr334zp8yi2bc0m7lqlw5qqpliahfcfqlhrgi81i")))

(define-public crate-alienware_cli-0.1.1 (c (n "alienware_cli") (v "0.1.1") (d (list (d (n "alienware") (r "^0.1.1") (d #t) (k 0)) (d (n "argparse") (r "^0.2.2") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "0pb9ds6qcqz1pjik10zmwns96ylp21vx7jfnrj90wc0q2i3glmmb")))

(define-public crate-alienware_cli-0.1.2 (c (n "alienware_cli") (v "0.1.2") (d (list (d (n "alienware") (r "^0.1.2") (d #t) (k 0)) (d (n "argparse") (r "^0.2.2") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "1c4bkzxa5bk842nlrxwxz576dq2601q5h3f5ikp0rdg7snw1hr5g")))

(define-public crate-alienware_cli-0.1.4 (c (n "alienware_cli") (v "0.1.4") (d (list (d (n "alienware") (r "^0.1.4") (d #t) (k 0)) (d (n "argparse") (r "^0.2.2") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "07km6cl3rsc47b9j95ba7a15cr1xxjisxfbnqj7jppa1djdda6wa")))

(define-public crate-alienware_cli-1.0.0 (c (n "alienware_cli") (v "1.0.0") (d (list (d (n "alienware") (r "^1.0.0") (d #t) (k 0)) (d (n "argparse") (r "^0.2.2") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "0zanids09g5q8c79lqzy85k5h0c8i6s68xvx33d0znqbwgrv20mn")))

(define-public crate-alienware_cli-1.0.1 (c (n "alienware_cli") (v "1.0.1") (d (list (d (n "alienware") (r "^1.0.1") (d #t) (k 0)) (d (n "argparse") (r "^0.2.2") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "09pzf9k6spfs8qa8x4imf9pjwaci6j3l1j20mcri22im6lwfckym") (r "1.58")))

(define-public crate-alienware_cli-1.0.2 (c (n "alienware_cli") (v "1.0.2") (d (list (d (n "alienware") (r "^1.0.1") (d #t) (k 0)) (d (n "argparse") (r "^0.2.2") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)))) (h "1j93f2g7gq2phgd76svp8bz0srfxy1y6fsp14k0ydzzxipn8v44l") (r "1.58")))

(define-public crate-alienware_cli-1.0.3 (c (n "alienware_cli") (v "1.0.3") (d (list (d (n "alienware") (r "^1.0.1") (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)))) (h "0abdgz6d08wpl0bm49l7g2ddxfpmivg140acsa30a3cg357pd0zi") (r "1.58")))

(define-public crate-alienware_cli-1.0.4 (c (n "alienware_cli") (v "1.0.4") (d (list (d (n "alienware") (r "^1.0.1") (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)))) (h "0g1iyy9114m5d71zbbv2hn0gqbc5y8gdj3g0pzs54r16y1rc0iik") (r "1.58")))

(define-public crate-alienware_cli-1.0.5 (c (n "alienware_cli") (v "1.0.5") (d (list (d (n "alienware") (r "^1.0.1") (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)) (d (n "snapcraft") (r "^0.1.0") (d #t) (k 0)))) (h "1dfr6ylqma1l4hrz48b71w2n6hf685w74l6py6sy518snk1g61yr") (r "1.58")))

(define-public crate-alienware_cli-1.0.6 (c (n "alienware_cli") (v "1.0.6") (d (list (d (n "alienware") (r "^1.0.1") (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)) (d (n "snapcraft") (r "^0.1.0") (d #t) (k 0)))) (h "03hisl3j2zgmwixjl7b2ma5rhhbpxns68nj76yps014ys9av6760") (r "1.58")))

(define-public crate-alienware_cli-1.0.7 (c (n "alienware_cli") (v "1.0.7") (d (list (d (n "alienware") (r "^1.0.7") (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)) (d (n "snapcraft") (r "^0.3.0") (d #t) (k 0)))) (h "0wv23qr1c4hn4h2xk7i0jxl69l435yh5w9jiglyp6vy2wclsn5bk") (r "1.58")))

(define-public crate-alienware_cli-1.0.8 (c (n "alienware_cli") (v "1.0.8") (d (list (d (n "alienware") (r "^1.0.8") (d #t) (k 0)) (d (n "clap") (r "^4.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "snapcraft") (r "^0.3.0") (d #t) (k 0)))) (h "18i1jgyijxhmz81mcswy2knr09bw52653kjv7jyll88siwjh46p1") (r "1.58")))

(define-public crate-alienware_cli-1.0.9 (c (n "alienware_cli") (v "1.0.9") (d (list (d (n "alienware") (r "^1.0.9") (d #t) (k 0)) (d (n "clap") (r "^4.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "snapcraft") (r "^0.3.0") (d #t) (k 0)))) (h "1l2ikw1fakf641bgy25a9wy32fdr0pxpd3pbrb15mb47zsnyrpn3") (r "1.58")))

(define-public crate-alienware_cli-1.0.10 (c (n "alienware_cli") (v "1.0.10") (d (list (d (n "alienware") (r "^1.0.10") (d #t) (k 0)) (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "snapcraft") (r "^0.3") (d #t) (k 0)))) (h "0zpbw5fzcdww72yfws6hmp0p6wriqhrdp5rxk3ir34sk38sbapf7") (r "1.58")))

(define-public crate-alienware_cli-1.0.11 (c (n "alienware_cli") (v "1.0.11") (d (list (d (n "alienware") (r "^1.0.11") (d #t) (k 0)) (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "snapcraft") (r "^0.3") (d #t) (k 0)))) (h "11mmdkm5bq4whv6spq0nvfxn0a1ryjdz8mzzc366dj4jww2xq1m2") (r "1.58")))

(define-public crate-alienware_cli-1.0.12 (c (n "alienware_cli") (v "1.0.12") (d (list (d (n "alienware") (r "^1.0.12") (d #t) (k 0)) (d (n "clap") (r "^4.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "snapcraft") (r "^0.4") (d #t) (k 0)))) (h "1zq7v7bvb0npv4m49pkgv22jmril6ykapxksq4i2vwk3m77q141r") (r "1.58")))

