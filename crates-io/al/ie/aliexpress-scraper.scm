(define-module (crates-io al ie aliexpress-scraper) #:use-module (crates-io))

(define-public crate-aliexpress-scraper-0.1.0 (c (n "aliexpress-scraper") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11.15") (d #t) (k 0)) (d (n "serde") (r "^1.0.158") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)))) (h "0z4mnd4nw3lzi4yp487nl6jih0bcli18bxawgw1sfv6rfllnvw7c")))

(define-public crate-aliexpress-scraper-0.1.1 (c (n "aliexpress-scraper") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.11.15") (d #t) (k 0)) (d (n "serde") (r "^1.0.158") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)))) (h "0bnjivrv7i37l0g8qvain6zcxab3wjcncjfxh11srhl79npxqqc9")))

(define-public crate-aliexpress-scraper-0.1.2 (c (n "aliexpress-scraper") (v "0.1.2") (d (list (d (n "reqwest") (r "^0.11.15") (d #t) (k 0)) (d (n "serde") (r "^1.0.158") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)))) (h "134w2ds7fg6hzqbbh77dw1w8llbl06q6kppgmd2mb0bkk5l4cbmx")))

(define-public crate-aliexpress-scraper-0.1.3 (c (n "aliexpress-scraper") (v "0.1.3") (d (list (d (n "reqwest") (r "^0.11.15") (d #t) (k 0)) (d (n "serde") (r "^1.0.158") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)))) (h "0a28z7g1xnmv4gjdkqpgkqrjqar6cmsx9kx294xwf293qpa02cp2")))

(define-public crate-aliexpress-scraper-0.1.4 (c (n "aliexpress-scraper") (v "0.1.4") (d (list (d (n "reqwest") (r "^0.11.15") (d #t) (k 0)) (d (n "serde") (r "^1.0.158") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)))) (h "1r54mi3x5dnqkiisawqnsglh1mah92iknqn7z5ci2la3x90wp92c")))

