(define-module (crates-io al ie alien-say) #:use-module (crates-io))

(define-public crate-alien-say-0.1.0 (c (n "alien-say") (v "0.1.0") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3.18") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.6.0") (d #t) (k 0)))) (h "1ygjbrxliyg864jn646br4dr2nrjb03sa4nzclj5319xdiyqzvv4")))

