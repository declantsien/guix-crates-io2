(define-module (crates-io al ie alien_ffi) #:use-module (crates-io))

(define-public crate-alien_ffi-0.1.0 (c (n "alien_ffi") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "typed-arena") (r "^2.0.1") (d #t) (k 0)))) (h "0664vpjf1crm1fazvqvd4g322b7rz44jjccvc894ybgd5q72b52x")))

