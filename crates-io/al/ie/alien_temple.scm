(define-module (crates-io al ie alien_temple) #:use-module (crates-io))

(define-public crate-alien_temple-0.1.0 (c (n "alien_temple") (v "0.1.0") (h "15z0g99dv1cjbkk4fq2j8hsjyggv68fj1rcspba0vmyshsqkl2r1")))

(define-public crate-alien_temple-0.2.0 (c (n "alien_temple") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.29") (d #t) (k 0)) (d (n "clap") (r "^4.4.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.9.5") (d #t) (k 0)))) (h "0ngc7fdf3apy91a56xlkfdi9qj5d951wnmdhy3d8zg1zghlns3d4")))

(define-public crate-alien_temple-0.2.1 (c (n "alien_temple") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4.29") (d #t) (k 0)) (d (n "clap") (r "^4.4.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.9.5") (d #t) (k 0)))) (h "1b3a15rbph813iblkg1nq15b0jlppxi00arpmnxjj28ljcsz88xg")))

