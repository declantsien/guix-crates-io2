(define-module (crates-io al ie alienware) #:use-module (crates-io))

(define-public crate-alienware-0.1.0 (c (n "alienware") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "0lz9sp66d1hvbm6xncmdn5aypqs4cnsxmmdswq363nr5i6makrv9")))

(define-public crate-alienware-0.1.1 (c (n "alienware") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "0fd4ic9b3b52qm3gpgm4bsy6wicqvqcgzdiaiznimg3nqfd3r25k")))

(define-public crate-alienware-0.1.2 (c (n "alienware") (v "0.1.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "0yhaimskjvv9vzbs9yrklrr52smcsvq3ffxsrr9m72lh2901ipz1")))

(define-public crate-alienware-0.1.3 (c (n "alienware") (v "0.1.3") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "1p7pr7bv306yl57bwvsnw4laxayxr5vvgqvqb7h6w3lwgyxdabjd")))

(define-public crate-alienware-0.1.4 (c (n "alienware") (v "0.1.4") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "02dma3wkym73pah7g4mhs9bx8mi5scqj0kbz4r9qd17zy9nx8pgz")))

(define-public crate-alienware-1.0.0 (c (n "alienware") (v "1.0.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "1mc59qd70gpss6brrj0nckfz4qlykwaam1plq806n0aml6w90a0h")))

(define-public crate-alienware-1.0.1 (c (n "alienware") (v "1.0.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "0l64w4lyrkvwpimmr9qndsv9zdn9g8i92bwmv3l1lfs7xaqsikvj") (r "1.58")))

(define-public crate-alienware-1.0.2 (c (n "alienware") (v "1.0.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)))) (h "0f03qn8gmq5i26irnz2vc500d6yaqn2lbhlpk2nf7zzvz5x94cl4") (r "1.58")))

(define-public crate-alienware-1.0.3 (c (n "alienware") (v "1.0.3") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)))) (h "1ryy37ch7cfl16dz4l8wa06nxfky9adfyw1fj943sx7zy6g9wffw") (r "1.58")))

(define-public crate-alienware-1.0.4 (c (n "alienware") (v "1.0.4") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)))) (h "1fs4qiv87jzynx0qk64il26spw36ssf474hvi04mp4kx6c20jg4n") (r "1.58")))

(define-public crate-alienware-1.0.5 (c (n "alienware") (v "1.0.5") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)))) (h "1whrml2mfhaa8hc904l7mdfi7q052s94bs7jczs4yzy1py7r292z") (r "1.58")))

(define-public crate-alienware-1.0.6 (c (n "alienware") (v "1.0.6") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)))) (h "1dr2cdbh6biibv9whb3icxxdr0czfppnb1mz2mjqvs8l7vxjz1i4") (r "1.58")))

(define-public crate-alienware-1.0.7 (c (n "alienware") (v "1.0.7") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)))) (h "0g8iq0dfc71kh0gcqbx4zifbk5mqn3f9j0cdb2h4iyj57p7c8vnx") (r "1.58")))

(define-public crate-alienware-1.0.8 (c (n "alienware") (v "1.0.8") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)))) (h "15a79gh7n462ab4fxg8109bn0kg4h42a71a53qlh8jbqzgnicsbh") (r "1.58")))

(define-public crate-alienware-1.0.9 (c (n "alienware") (v "1.0.9") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)))) (h "0g0i7l514c97haya5jqbssikvzn0b9mp452rk53qhacnxb87js63") (r "1.58")))

(define-public crate-alienware-1.0.10 (c (n "alienware") (v "1.0.10") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0vydcsb6frca4jh8nnmj6bh5vs0wjx514j788zx8k3c70zp3lrmj") (r "1.58")))

(define-public crate-alienware-1.0.11 (c (n "alienware") (v "1.0.11") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0hxx64bqy2p31cxwq29p20gliclqzjq1m3qwlfkkcndfhqgpw39w") (r "1.58")))

(define-public crate-alienware-1.0.12 (c (n "alienware") (v "1.0.12") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "12s17dbypz10f5spw338nim5v6lgihghxclgsx5lrds1fxql21kf") (r "1.58")))

