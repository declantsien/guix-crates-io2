(define-module (crates-io al be albert_stream) #:use-module (crates-io))

(define-public crate-albert_stream-0.0.1 (c (n "albert_stream") (v "0.0.1") (h "1aqfin4hacwrqbm2mfxx8ilxzvp6jf49s2z89bdc6jcqj18vj3a9")))

(define-public crate-albert_stream-0.0.2 (c (n "albert_stream") (v "0.0.2") (h "0blw81iyi81hj0njn1an1lx2wjf3g6f7kpfmg2rlp5s2f37sirr1")))

(define-public crate-albert_stream-0.0.3 (c (n "albert_stream") (v "0.0.3") (h "1063ng9vqllsyp3ggr0i206r51rwb2hasfqbh8zw99wkaqhvcan4")))

(define-public crate-albert_stream-0.0.4 (c (n "albert_stream") (v "0.0.4") (h "1jpi17wdvvwzxh4rmjqcn61ag1xjjpssw4lq6fraw2kn1lp4bsy0")))

