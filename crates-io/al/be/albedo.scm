(define-module (crates-io al be albedo) #:use-module (crates-io))

(define-public crate-albedo-0.0.1-beta.0 (c (n "albedo") (v "0.0.1-beta.0") (d (list (d (n "enumflags2") (r "^0.7.1") (d #t) (k 2)) (d (n "glam") (r "^0.11.2") (d #t) (k 2)) (d (n "gltf") (r "^0.15.2") (d #t) (k 2)) (d (n "image") (r "^0.23") (f (quote ("hdr"))) (k 2)) (d (n "pollster") (r "^0.2") (d #t) (k 2)) (d (n "wgpu") (r "^0.11.0") (d #t) (k 2)) (d (n "winit") (r "^0.25") (f (quote ("web-sys"))) (d #t) (k 2)))) (h "0s50cqx66jh5rxxnzckz6z016acl00l433l66cyg9ygag5d3x1ab")))

