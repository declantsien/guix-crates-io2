(define-module (crates-io al be albert) #:use-module (crates-io))

(define-public crate-albert-0.1.0 (c (n "albert") (v "0.1.0") (h "1nd371h7yc3y0y7vrc0fb6a0976zjzdcjpa4nmkn17yyqcsjdjm5")))

(define-public crate-albert-0.1.5 (c (n "albert") (v "0.1.5") (h "0ywr0rv9kgsskp3i9fy07w12q8n8y99l8vz5siwfmppj0jd8f558")))

(define-public crate-albert-0.1.6 (c (n "albert") (v "0.1.6") (h "0k848b3djblshszsvnbkr6lh2pv9vp45h20hfzxpvwac8s942y8p")))

(define-public crate-albert-0.1.7 (c (n "albert") (v "0.1.7") (h "1s0cn8r7ddpzzb3ky5dcrarbpgsw3a0rdb1bbg05c7s5xgaf2g97")))

(define-public crate-albert-0.1.8 (c (n "albert") (v "0.1.8") (h "0xryjlzl87jnfsvk489azbqx80smiq8jyw7jafryx0zxqppma41d") (r "1.56.1")))

