(define-module (crates-io al ko alkomp) #:use-module (crates-io))

(define-public crate-alkomp-0.1.0 (c (n "alkomp") (v "0.1.0") (d (list (d (n "bytemuck") (r "^1.4.1") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.7") (d #t) (k 0)) (d (n "ndarray") (r "^0.13.1") (d #t) (k 0)) (d (n "shaderc") (r "^0.6.2") (o #t) (d #t) (k 0)) (d (n "wgpu") (r "^0.6") (d #t) (k 0)))) (h "03331c7zkd5lyqdsi04cqdj5j9sxnfj6my3s0zirbkm073a98ba5")))

