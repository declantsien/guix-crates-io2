(define-module (crates-io al is alisql) #:use-module (crates-io))

(define-public crate-alisql-0.1.0 (c (n "alisql") (v "0.1.0") (d (list (d (n "minijinja") (r "^0.23.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "0fl8n162s6699p5nv53sz2vhin14lzdlxwa5mcsbnz3gvap1mxym")))

(define-public crate-alisql-0.1.1 (c (n "alisql") (v "0.1.1") (d (list (d (n "minijinja") (r "^0.23.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "11q6cp33z7djk13nzblfj6swiilhvjymm5vak6d9cwi13xvqd5r4")))

(define-public crate-alisql-0.1.2 (c (n "alisql") (v "0.1.2") (d (list (d (n "minijinja") (r "^0.23.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "037prhwgm9azini1v0w8x3a64zmqbsa5pj2lafnm4xygmsibzyr1")))

(define-public crate-alisql-0.1.3 (c (n "alisql") (v "0.1.3") (d (list (d (n "minijinja") (r "^0.23.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "0bw0ylyp3g9yf8yzn34yh1nm0vs3h9nqzj6x8h9xz4rfzskvl3xi")))

(define-public crate-alisql-0.1.4 (c (n "alisql") (v "0.1.4") (d (list (d (n "minijinja") (r "^0.23.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "0zgsd5gy9a37wq7gzigvinkaw60f3x42kjnmmvn3wp74mhaz5h73")))

(define-public crate-alisql-0.1.5 (c (n "alisql") (v "0.1.5") (d (list (d (n "minijinja") (r "^0.23.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "0qhj6vy8i21zx9bjigl7prb22rmvw42jx9p0mga84ajhdbzz9sps")))

(define-public crate-alisql-0.1.6 (c (n "alisql") (v "0.1.6") (d (list (d (n "minijinja") (r "^0.23.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "1jpsp7czfiqmnij0lylv233ak5ajlkll4pnfvs2xsafpn308cvyk")))

(define-public crate-alisql-0.1.7 (c (n "alisql") (v "0.1.7") (d (list (d (n "minijinja") (r "^0.23.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "0mkbc0yvfx97nsmzydmqxd4njvsfzdgdik0j55sp6wi6jinpb1xh")))

(define-public crate-alisql-0.1.8 (c (n "alisql") (v "0.1.8") (d (list (d (n "minijinja") (r "^0.23.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "009rrm216a8k1w5i3naiflyafp92baqmkprkzh4xlcmgryzm0wk4")))

(define-public crate-alisql-0.1.9 (c (n "alisql") (v "0.1.9") (d (list (d (n "minijinja") (r "^0.23.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "14s6111c26lanyygi5wrvp2c2mj2d1w3cml6920jkci9g5ny2wp7")))

(define-public crate-alisql-0.1.10 (c (n "alisql") (v "0.1.10") (d (list (d (n "minijinja") (r "^0.23.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "11hgii2nhpj7d8pn2w3nhnbqhi6y30d1565psyh8jkznyc0xiz90")))

(define-public crate-alisql-0.1.11 (c (n "alisql") (v "0.1.11") (d (list (d (n "minijinja") (r "^0.23.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "1sw76q5gigd4basyvajsz07ak9rfqixrhvzwrwd1vlrw1bi2gx2k")))

(define-public crate-alisql-0.1.12 (c (n "alisql") (v "0.1.12") (d (list (d (n "minijinja") (r "^0.23.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "0a63k0xznm4knz3fspmyz0rgbn49b0lwkqwcg7sx82c84rvbxhkg")))

(define-public crate-alisql-0.1.13 (c (n "alisql") (v "0.1.13") (d (list (d (n "minijinja") (r "^0.23.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "0r9bghbkj9gnir3c0f6hzqwrf45y9mcz1q7q5im44ishlhb5md1q")))

