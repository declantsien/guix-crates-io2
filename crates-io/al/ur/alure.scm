(define-module (crates-io al ur alure) #:use-module (crates-io))

(define-public crate-alure-0.1.0 (c (n "alure") (v "0.1.0") (d (list (d (n "amplify_derive") (r "^2.7.1") (d #t) (k 0)) (d (n "amplify_num") (r "^0.1") (d #t) (k 0)) (d (n "bitcoin_hashes") (r "^0.9.6") (d #t) (k 0)) (d (n "curve25519-dalek") (r "^3.1") (o #t) (d #t) (k 0)) (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "secp256k1") (r "^0.20.2") (o #t) (d #t) (k 0)))) (h "0zjplkf2vd9sjvz5wp7p5hzdsjngs66rmrfb9grqrkyha26j68v7") (f (quote (("std" "amplify_num/std" "amplify_num/hex") ("default") ("curve25519" "curve25519-dalek") ("all" "std" "secp256k1" "curve25519"))))))

