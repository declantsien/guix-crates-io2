(define-module (crates-io al bi albion-handler) #:use-module (crates-io))

(define-public crate-albion-handler-0.1.0 (c (n "albion-handler") (v "0.1.0") (h "1i435x6ffp1xvfjr5y8yl62bsc0k52ars2qia6lc4mrhmzl4b8i7") (y #t)))

(define-public crate-albion-handler-1.0.0 (c (n "albion-handler") (v "1.0.0") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "photon_decode") (r "^0.2.1") (d #t) (k 0)) (d (n "pnet") (r "^0.34.0") (d #t) (k 0)))) (h "03f7n7abnr2h6677dmclv5c1w2lgwz46ql9ki45jf15za396dqla") (y #t)))

(define-public crate-albion-handler-1.0.1 (c (n "albion-handler") (v "1.0.1") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "photon_decode") (r "^0.2.1") (d #t) (k 0)) (d (n "pnet") (r "^0.34.0") (d #t) (k 0)))) (h "1hlc33dsbqn0s791bramm5pxsrh395138234cb6iss2rqva9d2hh")))

