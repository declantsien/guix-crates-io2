(define-module (crates-io al ni alnilam-log) #:use-module (crates-io))

(define-public crate-alnilam-log-0.0.3 (c (n "alnilam-log") (v "0.0.3") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "console_error_panic_hook") (r "^0.1.6") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "console_log") (r "^0.2") (f (quote ("color"))) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0dvyg5nq63r836spk0zinzscbl7zzxgs50hwjbacv6r9v7z4v26a")))

(define-public crate-alnilam-log-0.0.4 (c (n "alnilam-log") (v "0.0.4") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "console_error_panic_hook") (r "^0.1.6") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "console_log") (r "^0.2") (f (quote ("color"))) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0vhrb2yidkf62l50m4vdqa0y0rk1rsah9pss5a87ismambcryl80")))

