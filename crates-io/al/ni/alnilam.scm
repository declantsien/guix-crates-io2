(define-module (crates-io al ni alnilam) #:use-module (crates-io))

(define-public crate-alnilam-0.0.1 (c (n "alnilam") (v "0.0.1") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "tokio") (r "^1.4.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)))) (h "1psfqxd9lxnd7cpjnzfkjahm0f0qi261ga0pvvb7izynwf8hr1ij")))

(define-public crate-alnilam-0.0.2 (c (n "alnilam") (v "0.0.2") (h "0rafr6xyfn7sqijk8j1hhznkcr4537g2bnhmdlxp29vcym5bhdv0")))

(define-public crate-alnilam-0.0.3 (c (n "alnilam") (v "0.0.3") (d (list (d (n "alnilam-consts") (r "^0.0.3") (d #t) (k 0)) (d (n "alnilam-hal") (r "^0.0.3") (d #t) (k 0)) (d (n "alnilam-log") (r "^0.0.3") (d #t) (k 0)) (d (n "alnilam-types") (r "^0.0.3") (d #t) (k 0)) (d (n "alnilam-utils") (r "^0.0.3") (d #t) (k 0)))) (h "0b9sdm2d1dh3rb84n6n5w3jfp523nz6y1nl4m1r9xlh8hvaylj7n")))

(define-public crate-alnilam-0.0.4 (c (n "alnilam") (v "0.0.4") (d (list (d (n "alnilam-consts") (r "^0.0.4") (d #t) (k 0)) (d (n "alnilam-hal") (r "^0.0.4") (d #t) (k 0)) (d (n "alnilam-log") (r "^0.0.4") (d #t) (k 0)) (d (n "alnilam-types") (r "^0.0.4") (d #t) (k 0)) (d (n "alnilam-utils") (r "^0.0.4") (d #t) (k 0)))) (h "1xsm763g0hxz9fdj6icrz87z0nxxdrkfv52grz6id58h5nyaa1h8")))

