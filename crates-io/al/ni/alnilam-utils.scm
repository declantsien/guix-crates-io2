(define-module (crates-io al ni alnilam-utils) #:use-module (crates-io))

(define-public crate-alnilam-utils-0.0.3 (c (n "alnilam-utils") (v "0.0.3") (d (list (d (n "libc") (r "^0.2.0") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "19hn6aq1cy16rpi341gbbapn33m852rsb37k9bbni4argwwj0jgp")))

(define-public crate-alnilam-utils-0.0.4 (c (n "alnilam-utils") (v "0.0.4") (d (list (d (n "libc") (r "^0.2.0") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "00fl4nynyc67q3r3gmkirwhvsbh83zx269l5n2av0qn82my0i9f1")))

