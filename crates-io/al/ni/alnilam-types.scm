(define-module (crates-io al ni alnilam-types) #:use-module (crates-io))

(define-public crate-alnilam-types-0.0.3 (c (n "alnilam-types") (v "0.0.3") (d (list (d (n "alnilam-consts") (r "^0.0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uuid") (r "^1.1.1") (f (quote ("serde" "v1" "v3" "v4" "v5"))) (d #t) (k 0)) (d (n "uuid") (r "^1.1.1") (f (quote ("js" "v1" "v3" "v4" "v5"))) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "03bfqpxsn4z4s0ifcyj1frjzx6hljck93aaclmxfqxbwvfcqi1lr")))

(define-public crate-alnilam-types-0.0.4 (c (n "alnilam-types") (v "0.0.4") (d (list (d (n "alnilam-consts") (r "^0.0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uuid") (r "^1.1.1") (f (quote ("serde" "v1" "v3" "v4" "v5"))) (d #t) (k 0)) (d (n "uuid") (r "^1.1.1") (f (quote ("js" "v1" "v3" "v4" "v5"))) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "1yry53n393n1i5656lhpvvjihh2f4k19z8jp8z3sq07alw7yfi6q")))

