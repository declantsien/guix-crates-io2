(define-module (crates-io al ni alnilam-consts) #:use-module (crates-io))

(define-public crate-alnilam-consts-0.0.3 (c (n "alnilam-consts") (v "0.0.3") (d (list (d (n "vergen") (r "=5.1.5") (f (quote ("build" "cargo" "rustc"))) (k 1)))) (h "117sr5y4fwzl4g9m8byancchx35w1kh9rmrdsjxipx2w29yscp7i")))

(define-public crate-alnilam-consts-0.0.4 (c (n "alnilam-consts") (v "0.0.4") (d (list (d (n "vergen") (r "=5.1.5") (f (quote ("build" "cargo" "rustc"))) (k 1)))) (h "0pa39z2qilwhx1rcdw4fn4s61rvcgrj3f56y70dqszc8hwwlfyw1")))

