(define-module (crates-io al th althea_proto) #:use-module (crates-io))

(define-public crate-althea_proto-0.0.1 (c (n "althea_proto") (v "0.0.1") (d (list (d (n "cosmos-sdk-proto") (r "^0.13") (f (quote ("bech32ibc" "ethermint"))) (d #t) (k 0) (p "cosmos-sdk-proto-althea")) (d (n "deep_space") (r "^2.14") (d #t) (k 0)) (d (n "prost") (r "^0.10") (d #t) (k 0)) (d (n "prost-types") (r "^0.10") (d #t) (k 0)) (d (n "tonic") (r "^0.7") (d #t) (k 0)))) (h "0pza4zkhhspj6wirbfj7pj0mf20sx39g0l6fjv7h8c5xvxprgcf8")))

(define-public crate-althea_proto-0.0.2 (c (n "althea_proto") (v "0.0.2") (d (list (d (n "cosmos-sdk-proto") (r "^0.13") (f (quote ("bech32ibc" "ethermint"))) (d #t) (k 0) (p "cosmos-sdk-proto-althea")) (d (n "deep_space") (r "^2.14") (d #t) (k 0)) (d (n "prost") (r "^0.10") (d #t) (k 0)) (d (n "prost-types") (r "^0.10") (d #t) (k 0)) (d (n "tonic") (r "^0.7") (d #t) (k 0)))) (h "0f8x4x98935j0kzqll0s7m4h3pviscfnjib2i542ps26fpjcdms4")))

(define-public crate-althea_proto-0.0.3 (c (n "althea_proto") (v "0.0.3") (d (list (d (n "cosmos-sdk-proto") (r "^0.13") (f (quote ("bech32ibc" "ethermint"))) (d #t) (k 0) (p "cosmos-sdk-proto-althea")) (d (n "prost") (r "^0.10") (d #t) (k 0)) (d (n "prost-types") (r "^0.10") (d #t) (k 0)) (d (n "tonic") (r "^0.7") (d #t) (k 0)))) (h "0x7wdwa38mpbdqj667vpm9vgjls0q1zi4xd8ixwdrard2bb0kz24")))

(define-public crate-althea_proto-0.1.0 (c (n "althea_proto") (v "0.1.0") (d (list (d (n "cosmos-sdk-proto") (r "^0.14.0") (f (quote ("ethermint"))) (d #t) (k 0) (p "cosmos-sdk-proto-althea")) (d (n "prost") (r "^0.10") (d #t) (k 0)) (d (n "prost-types") (r "^0.10") (d #t) (k 0)) (d (n "tonic") (r "^0.7") (d #t) (k 0)))) (h "004k4v3ar0k7yg9rd9ygjyq5gpxz8b2gymdmkfsykbcifpvm1gjm")))

(define-public crate-althea_proto-0.1.1 (c (n "althea_proto") (v "0.1.1") (d (list (d (n "cosmos-sdk-proto") (r "^0.14.0") (f (quote ("ethermint"))) (d #t) (k 0) (p "cosmos-sdk-proto-althea")) (d (n "prost") (r "^0.10") (d #t) (k 0)) (d (n "prost-types") (r "^0.10") (d #t) (k 0)) (d (n "tonic") (r "^0.7") (d #t) (k 0)))) (h "0x34psmlfbbasm4mmplwqln3390x5h00kybv7cqva69l1b84d7aw")))

(define-public crate-althea_proto-0.1.2 (c (n "althea_proto") (v "0.1.2") (d (list (d (n "cosmos-sdk-proto") (r "^0.14.0") (f (quote ("ethermint"))) (d #t) (k 0) (p "cosmos-sdk-proto-althea")) (d (n "prost") (r "^0.10") (d #t) (k 0)) (d (n "prost-types") (r "^0.10") (d #t) (k 0)) (d (n "tonic") (r "^0.7") (d #t) (k 0)))) (h "08qjcm7rw3mg0mafyabmkpf2785yzzh3jj4qzrqmxqg7lc59ribm")))

(define-public crate-althea_proto-0.1.3 (c (n "althea_proto") (v "0.1.3") (d (list (d (n "cosmos-sdk-proto") (r "^0.14.0") (f (quote ("ethermint"))) (d #t) (k 0) (p "cosmos-sdk-proto-althea")) (d (n "prost") (r "^0.10") (d #t) (k 0)) (d (n "prost-types") (r "^0.10") (d #t) (k 0)) (d (n "tonic") (r "^0.7") (d #t) (k 0)))) (h "0r7758nm3r1qiwc78v32xgz95nvh9nlh8498bd2zb7gd2g6yx0xn")))

(define-public crate-althea_proto-0.1.4 (c (n "althea_proto") (v "0.1.4") (d (list (d (n "cosmos-sdk-proto") (r "^0.14.0") (f (quote ("ethermint"))) (d #t) (k 0) (p "cosmos-sdk-proto-althea")) (d (n "prost") (r "^0.10") (d #t) (k 0)) (d (n "prost-types") (r "^0.10") (d #t) (k 0)) (d (n "tonic") (r "^0.7") (d #t) (k 0)))) (h "09rd0aq2r3nw1fv9jsmb8i7wdw02s5xjr5yacndvc0hf9dh911m1")))

(define-public crate-althea_proto-0.2.0 (c (n "althea_proto") (v "0.2.0") (d (list (d (n "cosmos-sdk-proto") (r "^0.15.0") (f (quote ("ethermint"))) (d #t) (k 0) (p "cosmos-sdk-proto-althea")) (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "prost-types") (r "^0.11") (d #t) (k 0)) (d (n "tonic") (r "^0.9") (d #t) (k 0)))) (h "0j1jijp22c3l9gl371w7dm34b5gdrib151ad9fkha6d8kr16q3z3")))

(define-public crate-althea_proto-0.3.0 (c (n "althea_proto") (v "0.3.0") (d (list (d (n "cosmos-sdk-proto") (r "^0.16.0") (f (quote ("ethermint"))) (d #t) (k 0) (p "cosmos-sdk-proto-althea")) (d (n "prost") (r "^0.12") (d #t) (k 0)) (d (n "prost-types") (r "^0.12") (d #t) (k 0)) (d (n "tonic") (r "^0.10") (d #t) (k 0)))) (h "188jz0c4l2bqmk9ifqbk93lj6s3ya1n0qc5vfs3ljy77rwwybr6r")))

(define-public crate-althea_proto-0.4.0 (c (n "althea_proto") (v "0.4.0") (d (list (d (n "cosmos-sdk-proto") (r "^0.16.0") (f (quote ("ethermint"))) (d #t) (k 0) (p "cosmos-sdk-proto-althea")) (d (n "prost") (r "^0.12") (d #t) (k 0)) (d (n "prost-types") (r "^0.12") (d #t) (k 0)) (d (n "tonic") (r "^0.10") (d #t) (k 0)))) (h "1p5jr9hgwhrc497lwk6cpg84bg2zrpjj5rc688vlx89qm0k707k4")))

(define-public crate-althea_proto-0.5.0 (c (n "althea_proto") (v "0.5.0") (d (list (d (n "cosmos-sdk-proto") (r "^0.16.0") (f (quote ("ethermint"))) (d #t) (k 0) (p "cosmos-sdk-proto-althea")) (d (n "prost") (r "^0.12") (d #t) (k 0)) (d (n "prost-types") (r "^0.12") (d #t) (k 0)) (d (n "tonic") (r "^0.10") (d #t) (k 0)))) (h "122kb5zw117yd91sbjhdwx74m1d97jjw9qi822qafckc98sz6lir")))

(define-public crate-althea_proto-0.6.0 (c (n "althea_proto") (v "0.6.0") (d (list (d (n "cosmos-sdk-proto") (r "^0.16.0") (f (quote ("ethermint"))) (d #t) (k 0) (p "cosmos-sdk-proto-althea")) (d (n "prost") (r "^0.12") (d #t) (k 0)) (d (n "prost-types") (r "^0.12") (d #t) (k 0)) (d (n "tonic") (r "^0.10") (d #t) (k 0)))) (h "1h4380l2i45ic1735qnhwlv921s11zd128ilkgzd49ynj7gdhfbs")))

(define-public crate-althea_proto-0.6.1 (c (n "althea_proto") (v "0.6.1") (d (list (d (n "cosmos-sdk-proto") (r "^0.16.1") (f (quote ("ethermint"))) (d #t) (k 0) (p "cosmos-sdk-proto-althea")) (d (n "prost") (r "^0.12") (d #t) (k 0)) (d (n "prost-types") (r "^0.12") (d #t) (k 0)) (d (n "tonic") (r "^0.10") (d #t) (k 0)))) (h "0v85728y3zwjd0pccii39c3m9ldvbj5n9lhr0svdq532mgrj09ch")))

(define-public crate-althea_proto-0.7.0 (c (n "althea_proto") (v "0.7.0") (d (list (d (n "cosmos-sdk-proto") (r "^0.16.1") (f (quote ("ethermint"))) (d #t) (k 0) (p "cosmos-sdk-proto-althea")) (d (n "prost") (r "^0.12") (d #t) (k 0)) (d (n "prost-types") (r "^0.12") (d #t) (k 0)) (d (n "tonic") (r "^0.10") (d #t) (k 0)))) (h "13mn6al3fa0qggljxkilfdmw19d5hm3frydfcz3xmn13390f30cg")))

(define-public crate-althea_proto-0.7.1 (c (n "althea_proto") (v "0.7.1") (d (list (d (n "cosmos-sdk-proto") (r "^0.16.1") (f (quote ("ethermint"))) (d #t) (k 0) (p "cosmos-sdk-proto-althea")) (d (n "prost") (r "^0.12") (d #t) (k 0)) (d (n "prost-types") (r "^0.12") (d #t) (k 0)) (d (n "tonic") (r "^0.10") (d #t) (k 0)))) (h "002sn33kczmylfij4hz0nz73hi7rbapjlpgxfj2dm3y8mzyb0vvp")))

