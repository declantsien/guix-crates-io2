(define-module (crates-io al l4 all4art-authservice-cqrs) #:use-module (crates-io))

(define-public crate-all4art-authservice-cqrs-0.1.0 (c (n "all4art-authservice-cqrs") (v "0.1.0") (h "1p1qbna86l207n2vqqgqbq1sd06g2kzln7x9x75dhjmcwj8d8k8b")))

(define-public crate-all4art-authservice-cqrs-0.1.1 (c (n "all4art-authservice-cqrs") (v "0.1.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "10m2zh7l3ih13iay83q6k47xlzmgh5ckk1pxpbd1lpd49ij635vj")))

(define-public crate-all4art-authservice-cqrs-0.1.2 (c (n "all4art-authservice-cqrs") (v "0.1.2") (d (list (d (n "all4art-authservice-domain") (r "^0.1.3") (d #t) (k 0)) (d (n "all4art-authservice-persistence") (r "^0.1.5") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.52") (d #t) (k 0)) (d (n "futures") (r "^0.3.19") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1l6kcfyprs1s5y6qdca3a8ii5rqysh69cdfs4j720xwhxb9y0i8m")))

(define-public crate-all4art-authservice-cqrs-0.1.3 (c (n "all4art-authservice-cqrs") (v "0.1.3") (d (list (d (n "all4art-authservice-domain") (r "^0.1.5") (d #t) (k 0)) (d (n "all4art-authservice-persistence") (r "^0.1.8") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.52") (d #t) (k 0)) (d (n "futures") (r "^0.3.19") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "00z3agdxmcwcvd6x724qqpl9iiqhifgydqa134v49b77ppdgjyfk")))

(define-public crate-all4art-authservice-cqrs-0.1.4 (c (n "all4art-authservice-cqrs") (v "0.1.4") (d (list (d (n "all4art-authservice-domain") (r "^0.1.5") (d #t) (k 0)) (d (n "all4art-authservice-persistence") (r "^0.1.8") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.52") (d #t) (k 0)) (d (n "futures") (r "^0.3.19") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1y8c22fhv4nyfd8p7babi6yypdjlpccchdc88hiyhz4imxza0w13")))

(define-public crate-all4art-authservice-cqrs-0.1.5 (c (n "all4art-authservice-cqrs") (v "0.1.5") (d (list (d (n "all4art-authservice-domain") (r "^0.1.6") (d #t) (k 0)) (d (n "all4art-authservice-persistence") (r "^0.1.9") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.52") (d #t) (k 0)) (d (n "futures") (r "^0.3.19") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "116alap6xhkxa4f28c3zjqv9bx2s2pzxyna756mcqj4hyfl5v4bq")))

