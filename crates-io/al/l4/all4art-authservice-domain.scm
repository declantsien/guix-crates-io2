(define-module (crates-io al l4 all4art-authservice-domain) #:use-module (crates-io))

(define-public crate-all4art-authservice-domain-0.1.0 (c (n "all4art-authservice-domain") (v "0.1.0") (h "1qvzf52cmdlh190p1fjgc4vwnwlxjq5ijmb7nvndbqlfqa7z2dmn")))

(define-public crate-all4art-authservice-domain-0.1.1 (c (n "all4art-authservice-domain") (v "0.1.1") (h "040zad70kwj2h71jyzrncnhicwgp19r0nj78wh49i304nhniqn8h")))

(define-public crate-all4art-authservice-domain-0.1.2 (c (n "all4art-authservice-domain") (v "0.1.2") (h "0m3fzfh9cfy1dh09gwd9h78am6y7r67aws72d1wihv1s8jgjni67")))

(define-public crate-all4art-authservice-domain-0.1.3 (c (n "all4art-authservice-domain") (v "0.1.3") (h "0rwjfix7ysr3n7rcv54islsxxp3x38fxn4560v5gxj6p3b98mypk")))

(define-public crate-all4art-authservice-domain-0.1.4 (c (n "all4art-authservice-domain") (v "0.1.4") (h "1qb0cp7sbrawm9navqqgb9g644rwrnd4hpi7v2msvhkqkgfmzfs6")))

(define-public crate-all4art-authservice-domain-0.1.5 (c (n "all4art-authservice-domain") (v "0.1.5") (h "11jc11h29n9jqjd9r8gxsf5qkhmxxrlyjzmx5wv816gj10k5lyb4")))

(define-public crate-all4art-authservice-domain-0.1.6 (c (n "all4art-authservice-domain") (v "0.1.6") (h "0s1w2c31c3f3ms14wm0bizxxq05p1vf2yhdy6zkazw6y6b356xln")))

