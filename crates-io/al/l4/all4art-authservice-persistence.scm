(define-module (crates-io al l4 all4art-authservice-persistence) #:use-module (crates-io))

(define-public crate-all4art-authservice-persistence-0.1.0 (c (n "all4art-authservice-persistence") (v "0.1.0") (d (list (d (n "all4art-authservice-domain") (r "^0.1.0") (d #t) (k 0)))) (h "0bv1qpgas8qf1hk8a4plvq64z33l95zvyf42qykw5k7cc7mfbxck")))

(define-public crate-all4art-authservice-persistence-0.1.1 (c (n "all4art-authservice-persistence") (v "0.1.1") (d (list (d (n "all4art-authservice-domain") (r "^0.1.0") (d #t) (k 0)))) (h "1a48ff2xglzc3y4qgmxqy5x6nkl6s02lbhkkaslyhfllhjj74z1r")))

(define-public crate-all4art-authservice-persistence-0.1.2 (c (n "all4art-authservice-persistence") (v "0.1.2") (d (list (d (n "all4art-authservice-domain") (r "^0.1.1") (d #t) (k 0)))) (h "0g4smz2fcp1gh6bj55igzdmcfsny9dgg4x2xwcwh1qiw4vkr4hdn")))

(define-public crate-all4art-authservice-persistence-0.1.3 (c (n "all4art-authservice-persistence") (v "0.1.3") (d (list (d (n "all4art-authservice-domain") (r "^0.1.2") (d #t) (k 0)))) (h "0p9sqq2v1dck9bdns1v86gdnv5vxbsx8dvlxrl2hgb826y76xj16")))

(define-public crate-all4art-authservice-persistence-0.1.4 (c (n "all4art-authservice-persistence") (v "0.1.4") (d (list (d (n "all4art-authservice-domain") (r "^0.1.3") (d #t) (k 0)))) (h "08fizcx6vsfsni2ww8s54i6wzax9dkvgpqs9443374ixnx2kxch1")))

(define-public crate-all4art-authservice-persistence-0.1.5 (c (n "all4art-authservice-persistence") (v "0.1.5") (d (list (d (n "all4art-authservice-domain") (r "^0.1.3") (d #t) (k 0)))) (h "0v489vlm1n0bgn59dghdcw9fs0n9bs1psg30yn0sm24bq90xq4rh")))

(define-public crate-all4art-authservice-persistence-0.1.6 (c (n "all4art-authservice-persistence") (v "0.1.6") (d (list (d (n "all4art-authservice-domain") (r "^0.1.3") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.52") (d #t) (k 0)))) (h "0ayc3spx28g6mgka1ly1j5n502448qr87izhvba6vjc58jvr3gi5")))

(define-public crate-all4art-authservice-persistence-0.1.7 (c (n "all4art-authservice-persistence") (v "0.1.7") (d (list (d (n "all4art-authservice-domain") (r "^0.1.4") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.52") (d #t) (k 0)))) (h "1k72srf17d347sqnlh82i1nl028vm4jd7283fdpk4xyrx47hg8ld")))

(define-public crate-all4art-authservice-persistence-0.1.8 (c (n "all4art-authservice-persistence") (v "0.1.8") (d (list (d (n "all4art-authservice-domain") (r "^0.1.5") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.52") (d #t) (k 0)))) (h "13qra2zb6kwf7bdz6qpinxmignhrd0va87r3m3ydd58avj15s559")))

(define-public crate-all4art-authservice-persistence-0.1.9 (c (n "all4art-authservice-persistence") (v "0.1.9") (d (list (d (n "all4art-authservice-domain") (r "^0.1.6") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.52") (d #t) (k 0)))) (h "13b619mrnyh7wb1h66611x8g1w00n10100y284n5yqdkzdkbqpld")))

