(define-module (crates-io al l4 all4art-commons-utils) #:use-module (crates-io))

(define-public crate-all4art-commons-utils-0.1.0 (c (n "all4art-commons-utils") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "09vav30z3wghdx31ky6dbbh2b73h2fhhwzcalk73jx1z2d5pd8q4")))

(define-public crate-all4art-commons-utils-0.1.1 (c (n "all4art-commons-utils") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0kylrhq90250yslnjlrp6akmkvw1nv4k5p9pg1ya4iw35imdb4ap")))

(define-public crate-all4art-commons-utils-0.1.2 (c (n "all4art-commons-utils") (v "0.1.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "19ym15z0j3vlx5x0949ab9zipp0miw9djfvf0kv9nh7gqrcw99sj")))

(define-public crate-all4art-commons-utils-0.1.3 (c (n "all4art-commons-utils") (v "0.1.3") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0xi7vrc5n0zn1k7ia4jy0w5jp058x94kyf0c8b0qwxqqpwcz2izq")))

(define-public crate-all4art-commons-utils-0.1.4 (c (n "all4art-commons-utils") (v "0.1.4") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0k498ak65r10k22k1fh4bhkl7xmdj6kf3msrkkabp4dvv48wxjr7")))

(define-public crate-all4art-commons-utils-0.1.5 (c (n "all4art-commons-utils") (v "0.1.5") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1h7s3m2cjlh24kjvmcxwvgni3hhv6mgjgwgpx0dz0j0i7xv1arqa")))

(define-public crate-all4art-commons-utils-0.1.6 (c (n "all4art-commons-utils") (v "0.1.6") (d (list (d (n "jsonwebtoken") (r "^8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0h5djzfhrggd32n58l7djdxr9nkcapihzn9r58aa5rwq1synv50k")))

(define-public crate-all4art-commons-utils-0.1.7 (c (n "all4art-commons-utils") (v "0.1.7") (d (list (d (n "jsonwebtoken") (r "^8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "08wmsxbrqj7hw29lr5v01mvdhf54f1cp177ap5hgd48waqnpdpjs")))

(define-public crate-all4art-commons-utils-0.1.8 (c (n "all4art-commons-utils") (v "0.1.8") (d (list (d (n "jsonwebtoken") (r "^8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1mivx08k20ylkiawf13l4yq7qnfbmriq5j2nlhrg3wkx27d1x1s8")))

