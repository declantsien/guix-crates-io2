(define-module (crates-io al l4 all4art-authservice-mocks) #:use-module (crates-io))

(define-public crate-all4art-authservice-mocks-0.1.0 (c (n "all4art-authservice-mocks") (v "0.1.0") (d (list (d (n "all4art-authservice-domain") (r "^0.1.6") (d #t) (k 0)))) (h "15lndqnrw788f2wlcjc53yhv53896ml0q52s9236bmdc6mz56i6p")))

(define-public crate-all4art-authservice-mocks-0.1.1 (c (n "all4art-authservice-mocks") (v "0.1.1") (d (list (d (n "all4art-authservice-domain") (r "^0.1.6") (d #t) (k 0)))) (h "0dl3mln7wly0pdyyl7iy4bf4mnfbqwzdm1iks22126g5nc9xb59q")))

