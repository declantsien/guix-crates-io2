(define-module (crates-io al am alamofire-kit) #:use-module (crates-io))

(define-public crate-alamofire-kit-0.1.0 (c (n "alamofire-kit") (v "0.1.0") (d (list (d (n "semver") (r "^1.0") (k 0)))) (h "03j6pl5bvbzas2wdsmcgjvjqvrakrs7102pmgwdpjfxmn3wjgz4h")))

(define-public crate-alamofire-kit-0.1.1 (c (n "alamofire-kit") (v "0.1.1") (d (list (d (n "semver") (r "^1.0") (k 0)))) (h "1vcf6kdwzs850wgfv905pj9gqbyaighlgspiwm1k506bdq9hw2lg")))

(define-public crate-alamofire-kit-0.1.2 (c (n "alamofire-kit") (v "0.1.2") (d (list (d (n "semver") (r "^1.0") (k 0)))) (h "01lb7b2hvkvyvfqpa26jdwhdwq0m4v5bkz3jz0axl53v2nbrn29g")))

(define-public crate-alamofire-kit-0.2.0 (c (n "alamofire-kit") (v "0.2.0") (d (list (d (n "semver") (r "^1") (k 0)))) (h "0iy28wbryb2i23wqmsc15928fnim4vkwr7xbbyhyyh44xiy9vq56")))

(define-public crate-alamofire-kit-0.3.0 (c (n "alamofire-kit") (v "0.3.0") (d (list (d (n "semver") (r "^1") (k 0)))) (h "1sc0351p1gw8lzvym0idjaga61jh7xj273y3fkgpwzh679d5jirj")))

