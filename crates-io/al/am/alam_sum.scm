(define-module (crates-io al am alam_sum) #:use-module (crates-io))

(define-public crate-alam_sum-0.1.0 (c (n "alam_sum") (v "0.1.0") (h "03npfjwyc0hfx6ys7kg999vzzsy29s7548kp8zydc1xfs09qx4s9")))

(define-public crate-alam_sum-0.1.1 (c (n "alam_sum") (v "0.1.1") (h "0mgq2y00p57wh3dwv8g4as9ps8m1w8rfyb5416mndjm4sr1hdsr9")))

(define-public crate-alam_sum-0.1.2 (c (n "alam_sum") (v "0.1.2") (h "03afcmbi7h67p5px44k6d4j456kcvrnz6b88rcm8pl8a7pn4bfkg")))

