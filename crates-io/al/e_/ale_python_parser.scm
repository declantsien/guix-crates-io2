(define-module (crates-io al e_ ale_python_parser) #:use-module (crates-io))

(define-public crate-ale_python_parser-0.1.0 (c (n "ale_python_parser") (v "0.1.0") (d (list (d (n "aleph-syntax-tree") (r "^0.1.0") (d #t) (k 0)) (d (n "rustpython-parser") (r "^0.2.0") (d #t) (k 0)))) (h "1lm4q3s1xjb1fmd7y4ixmk53s4h1z4pf2mcjrxgxizz8vvv6b04f")))

