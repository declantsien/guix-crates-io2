(define-module (crates-io al ky alkyne) #:use-module (crates-io))

(define-public crate-alkyne-0.1.0 (c (n "alkyne") (v "0.1.0") (d (list (d (n "chashmap") (r "^2.2.2") (d #t) (k 0)) (d (n "crossbeam") (r "^0.7.2") (d #t) (k 0)) (d (n "pest") (r "^2.1.2") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2.3") (d #t) (k 0)) (d (n "rustyline") (r "^5.0.5") (d #t) (k 0)) (d (n "termion") (r "^1.1.1") (d #t) (k 0)) (d (n "thread_local") (r "^1.0.1") (d #t) (k 0)) (d (n "toolshed") (r "^0.8.1") (d #t) (k 0)))) (h "1qp73r4xg3kim610h8xdrhg2vdjz8jr1n777170axglz7drkawwm")))

