(define-module (crates-io al tp altp) #:use-module (crates-io))

(define-public crate-altp-0.1.0 (c (n "altp") (v "0.1.0") (d (list (d (n "clap") (r "^4.5.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dialoguer") (r "^0.11.0") (d #t) (k 0)) (d (n "toml") (r "^0.8.12") (d #t) (k 0)) (d (n "whoami") (r "^1.5.1") (d #t) (k 0)) (d (n "whoami") (r "^1.5.1") (d #t) (k 1)))) (h "0wj15ijqyky2x4sja2143h6x2ck48mm3r3f07wabnd9mw3fhh44f")))

(define-public crate-altp-0.2.0 (c (n "altp") (v "0.2.0") (d (list (d (n "clap") (r "^4.5.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dialoguer") (r "^0.11.0") (d #t) (k 0)) (d (n "toml") (r "^0.8.12") (d #t) (k 0)) (d (n "whoami") (r "^1.5.1") (d #t) (k 0)) (d (n "whoami") (r "^1.5.1") (d #t) (k 1)))) (h "0kjqz1ss31pql8692d4img35qjk3443a19vh4pkfqd3fi6hihcjz")))

(define-public crate-altp-0.2.1 (c (n "altp") (v "0.2.1") (d (list (d (n "clap") (r "^4.5.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dialoguer") (r "^0.11.0") (d #t) (k 0)) (d (n "toml") (r "^0.8.12") (d #t) (k 0)) (d (n "whoami") (r "^1.5.1") (d #t) (k 0)) (d (n "whoami") (r "^1.5.1") (d #t) (k 1)))) (h "0v3hvkjy97155d4nbvhk3fjmf5yh1dwbqblgxbpq5icqj6q9gsgk")))

(define-public crate-altp-0.2.2 (c (n "altp") (v "0.2.2") (d (list (d (n "clap") (r "^4.5.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dialoguer") (r "^0.11.0") (d #t) (k 0)) (d (n "toml") (r "^0.8.12") (d #t) (k 0)) (d (n "whoami") (r "^1.5.1") (d #t) (k 0)) (d (n "whoami") (r "^1.5.1") (d #t) (k 1)))) (h "0xbs9p8sxr8dycm4ggm1gfpmyg0jkx99d5ja2dsfhkbrxj56cvc1")))

