(define-module (crates-io al xi alximo-corr) #:use-module (crates-io))

(define-public crate-alximo-corr-0.1.0 (c (n "alximo-corr") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "ndarray-linalg") (r "^0.16.0") (d #t) (k 0)) (d (n "statrs") (r "^0.16.0") (d #t) (k 0)))) (h "02r44g3pfwvfiavwdp9kf0qpdmijprdm07jbysg7q52vh0liim0w")))

(define-public crate-alximo-corr-0.2.0 (c (n "alximo-corr") (v "0.2.0") (d (list (d (n "alximo-core") (r "^0.1.0") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "ndarray-linalg") (r "^0.16.0") (d #t) (k 0)) (d (n "ndarray-rand") (r "^0.14.0") (d #t) (k 0)) (d (n "statrs") (r "^0.16.0") (d #t) (k 0)))) (h "06gk90gq3kwpnymsb64q5i25p4k1v44a50s063m255dnql6s1aqj")))

