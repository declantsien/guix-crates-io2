(define-module (crates-io al xi alximo-core) #:use-module (crates-io))

(define-public crate-alximo-core-0.1.0 (c (n "alximo-core") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)))) (h "0gkkfvdgz90250w0yda0frax387zr0a0zbinr4jxszw5lssxldr4")))

