(define-module (crates-io al ac alacritty-theme) #:use-module (crates-io))

(define-public crate-alacritty-theme-0.1.0 (c (n "alacritty-theme") (v "0.1.0") (d (list (d (n "derive_more") (r "^0.99.2") (d #t) (k 0)) (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.11") (d #t) (k 0)) (d (n "structopt") (r "^0.3.5") (f (quote ("color"))) (d #t) (k 0)) (d (n "termion") (r "^1.5.3") (d #t) (k 0)))) (h "09qp2x4wkwxr85bihvqny38dlj8fmfnv33490cxj4a9fs250b836")))

(define-public crate-alacritty-theme-0.2.0 (c (n "alacritty-theme") (v "0.2.0") (d (list (d (n "derive_more") (r "^0.99.2") (d #t) (k 0)) (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.11") (d #t) (k 0)) (d (n "structopt") (r "^0.3.5") (f (quote ("color"))) (d #t) (k 0)) (d (n "termion") (r "^1.5.3") (d #t) (k 0)) (d (n "xdg") (r "^2") (d #t) (t "cfg(not(windows))") (k 0)))) (h "0k0s8fhsffysand61kfq7rgnnasbm25ddr6xamnm4wwxr5h6ad0c")))

