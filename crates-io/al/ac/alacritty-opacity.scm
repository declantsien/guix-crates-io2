(define-module (crates-io al ac alacritty-opacity) #:use-module (crates-io))

(define-public crate-alacritty-opacity-0.1.0 (c (n "alacritty-opacity") (v "0.1.0") (d (list (d (n "clap") (r "^4.0.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "shellexpand") (r "^2.1") (d #t) (k 0)))) (h "18j6rrvv20phigwpasijgyz25mw7k4b1p8dd3k1005rhghiw1wvr")))

(define-public crate-alacritty-opacity-0.1.1 (c (n "alacritty-opacity") (v "0.1.1") (d (list (d (n "clap") (r "^4.0.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "shellexpand") (r "^2.1") (d #t) (k 0)))) (h "0szmwi8swaaaxgg01cvqi2ggiafnawf1l9glhxydn948f3ipg2fq")))

