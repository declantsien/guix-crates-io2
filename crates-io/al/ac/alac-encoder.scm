(define-module (crates-io al ac alac-encoder) #:use-module (crates-io))

(define-public crate-alac-encoder-0.1.0 (c (n "alac-encoder") (v "0.1.0") (d (list (d (n "bincode") (r "^1.1.3") (d #t) (k 2)) (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "criterion") (r "^0.2.11") (d #t) (k 2)) (d (n "serde") (r "^1.0.90") (f (quote ("derive"))) (d #t) (k 2)))) (h "1scbps2kkmxyk59rk1wywi4rdvvyglf7gwdgsk5w601pbrbq4ygc")))

(define-public crate-alac-encoder-0.1.1 (c (n "alac-encoder") (v "0.1.1") (d (list (d (n "bincode") (r "^1.1.3") (d #t) (k 2)) (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "criterion") (r "^0.2.11") (d #t) (k 2)) (d (n "serde") (r "^1.0.90") (f (quote ("derive"))) (d #t) (k 2)))) (h "0db0mfl7qw7n8q6c40prp3jsnqdy7qkrymiw2rm524dk2f22dsl3")))

(define-public crate-alac-encoder-0.2.0 (c (n "alac-encoder") (v "0.2.0") (d (list (d (n "bincode") (r "^1.1.3") (d #t) (k 2)) (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "serde") (r "^1.0.90") (f (quote ("derive"))) (d #t) (k 2)))) (h "1kja9p8z4gzzwnlwrnz5li7wv48kiig3ycj413wmwgiv042f8a3g") (r "1.56")))

(define-public crate-alac-encoder-0.3.0 (c (n "alac-encoder") (v "0.3.0") (d (list (d (n "bincode") (r "^1.1.3") (d #t) (k 2)) (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "serde") (r "^1.0.90") (f (quote ("derive"))) (d #t) (k 2)))) (h "0acz5cii48v4k05vwmxyfba4qpv8418w44c7i5wydvf2xvka8cha") (r "1.57")))

