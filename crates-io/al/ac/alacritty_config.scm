(define-module (crates-io al ac alacritty_config) #:use-module (crates-io))

(define-public crate-alacritty_config-0.1.0-rc1 (c (n "alacritty_config") (v "0.1.0-rc1") (d (list (d (n "log") (r "^0.4.17") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.24") (d #t) (k 0)))) (h "1n610lkpy1v7khzhns433v4d5hpy66zz3bhdpf82si894v03ayxx") (r "1.57.0")))

(define-public crate-alacritty_config-0.1.0-rc3 (c (n "alacritty_config") (v "0.1.0-rc3") (d (list (d (n "log") (r "^0.4.17") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.24") (d #t) (k 0)))) (h "1w92wpp8c1wsjp95gkil01pkqi3mw6xc3rgksq82ai63zmfzbxww") (r "1.57.0")))

(define-public crate-alacritty_config-0.1.0 (c (n "alacritty_config") (v "0.1.0") (d (list (d (n "log") (r "^0.4.17") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.24") (d #t) (k 0)))) (h "1zbx4bknvcc3jwrgy9bcr0dk2b0glxglh647kwpc37w9lvrfvjdh") (r "1.57.0")))

(define-public crate-alacritty_config-0.1.1-rc2 (c (n "alacritty_config") (v "0.1.1-rc2") (d (list (d (n "log") (r "^0.4.17") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.24") (d #t) (k 0)) (d (n "winit") (r "^0.28.2") (f (quote ("serde"))) (t "cfg(target_os = \"macos\")") (k 0)))) (h "10fhd0pnmxjz2yhfpl86i06hw0vkpsn7618pq2dn2alk3dhc7a5j") (r "1.60.0")))

(define-public crate-alacritty_config-0.1.1-rc3 (c (n "alacritty_config") (v "0.1.1-rc3") (d (list (d (n "log") (r "^0.4.17") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.24") (d #t) (k 0)) (d (n "winit") (r "^0.28.2") (f (quote ("serde"))) (t "cfg(target_os = \"macos\")") (k 0)))) (h "1f36cv3253qj37jg49881giwhn3l50n3ri7d949vczqm23agk8l9") (r "1.60.0")))

(define-public crate-alacritty_config-0.1.1 (c (n "alacritty_config") (v "0.1.1") (d (list (d (n "log") (r "^0.4.17") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.24") (d #t) (k 0)) (d (n "winit") (r "^0.28.2") (f (quote ("serde"))) (t "cfg(target_os = \"macos\")") (k 0)))) (h "0imj95kqnpb6mlgdn4bs11lm472x8j5vf58wz14hbcfw2kag4fw6") (r "1.60.0")))

(define-public crate-alacritty_config-0.1.2-rc1 (c (n "alacritty_config") (v "0.1.2-rc1") (d (list (d (n "log") (r "^0.4.17") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (d #t) (k 0)) (d (n "toml") (r "^0.8.2") (d #t) (k 0)))) (h "079m8c7rcff0zbj1fr8mdbwvj3azp6yxfjhim17w8cqcxba303as") (r "1.70.0")))

(define-public crate-alacritty_config-0.1.2-rc2 (c (n "alacritty_config") (v "0.1.2-rc2") (d (list (d (n "log") (r "^0.4.17") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (d #t) (k 0)) (d (n "toml") (r "^0.8.2") (d #t) (k 0)))) (h "1979g1gmcwc5ig6hzpi2kygxsyykl3hgq35cpj6b4ixbnmwjrw8l") (r "1.70.0")))

(define-public crate-alacritty_config-0.1.2 (c (n "alacritty_config") (v "0.1.2") (d (list (d (n "log") (r "^0.4.17") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (d #t) (k 0)) (d (n "toml") (r "^0.8.2") (d #t) (k 0)))) (h "1wxdpvsv80hjs7kblgqf40if9hyqar2lpa8z0rxfbbm69wh6nm2f") (r "1.70.0")))

(define-public crate-alacritty_config-0.2.0 (c (n "alacritty_config") (v "0.2.0") (d (list (d (n "log") (r "^0.4.17") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 2)) (d (n "toml") (r "^0.8.2") (d #t) (k 0)))) (h "07fhxlmk1ckg4byvfc9xp10y0qcsfh8g8d3wbx0acckvl5ga9qfc") (r "1.70.0")))

(define-public crate-alacritty_config-0.2.1-rc1 (c (n "alacritty_config") (v "0.2.1-rc1") (d (list (d (n "log") (r "^0.4.17") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 2)) (d (n "toml") (r "^0.8.2") (d #t) (k 0)))) (h "1qdylqb2fhs4bhcw65mmydsc47p3y83lfw2xrddchrjpl5q7w97m") (r "1.70.0")))

(define-public crate-alacritty_config-0.2.1 (c (n "alacritty_config") (v "0.2.1") (d (list (d (n "log") (r "^0.4.17") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 2)) (d (n "toml") (r "^0.8.2") (d #t) (k 0)))) (h "1d5n32aqcdjbkczmjg0z3hg6xkh9rx4s4kz5nzwmb5fjrgmnabjy") (r "1.70.0")))

