(define-module (crates-io al ua alua-macros) #:use-module (crates-io))

(define-public crate-alua-macros-0.1.0 (c (n "alua-macros") (v "0.1.0") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "syn") (r "^2.0.23") (d #t) (k 0)))) (h "1wyyx2majb01rlx6d2ysx3ca6jsxblf7wqw12vrs1nx7hx9fsv6j")))

(define-public crate-alua-macros-0.1.1 (c (n "alua-macros") (v "0.1.1") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "syn") (r "^2.0.23") (d #t) (k 0)))) (h "09m5jhqcc29wd2niwqkxk66xb17kf5ng7p9wnyvqf0b2xpvw1nrh") (f (quote (("userdata"))))))

