(define-module (crates-io al on alone_ee) #:use-module (crates-io))

(define-public crate-alone_ee-1.0.0 (c (n "alone_ee") (v "1.0.0") (h "1zmayax3d6skx34sgvr7pb9mfd8xj31zdkrdmn26cpdswmvgdrh4")))

(define-public crate-alone_ee-1.0.1 (c (n "alone_ee") (v "1.0.1") (h "1y35kqnncbx4130axi3p7mhqfk5j4v14jpbgydbrw0ghmv226sba")))

(define-public crate-alone_ee-1.0.2 (c (n "alone_ee") (v "1.0.2") (h "0agil06jfnx4nvx9i26c69psnxfba07v3kq49mgs8f1wfpk3js1d")))

(define-public crate-alone_ee-1.0.3 (c (n "alone_ee") (v "1.0.3") (h "0qhm7wk3licz38k9nkv3y9dndvzyy7cf1dkq70kdj98w2wvgiff3")))

(define-public crate-alone_ee-1.1.0 (c (n "alone_ee") (v "1.1.0") (h "1jxlk4hm8y587raa2jvjcgsrd3ahq6cbcykw35fyhni2nksbzvp7")))

(define-public crate-alone_ee-1.2.0 (c (n "alone_ee") (v "1.2.0") (h "1k6ydxq1y6h16a359wf514cxxggjhay6a5isz7j6xgsaqj5msh04")))

(define-public crate-alone_ee-1.2.1 (c (n "alone_ee") (v "1.2.1") (h "1gcihy695pgsx8qwmvsyim8y8hdrh0j26b3hhsaslnig0zsxic5k")))

(define-public crate-alone_ee-1.2.2 (c (n "alone_ee") (v "1.2.2") (h "124nhfzvpxqrwj6dj61qsbxf6xlkksl3izafrvmyx552x5mnyljw")))

(define-public crate-alone_ee-1.2.3 (c (n "alone_ee") (v "1.2.3") (h "1q9nnslzmqxhc2gsncxwfijmj8rhw2hfmhfacxnxbamfvclbbrbk")))

(define-public crate-alone_ee-1.2.4 (c (n "alone_ee") (v "1.2.4") (h "0zf7y8l99ffh0h5j7djnzhvbagcs79bfnhqlmfa9hrk64sb6yx38")))

(define-public crate-alone_ee-1.2.5 (c (n "alone_ee") (v "1.2.5") (h "0wh3lyg53k16lywk9k3pgsgpk47jxpf47l32h0zcz6bxspf2ydwj")))

(define-public crate-alone_ee-1.3.0 (c (n "alone_ee") (v "1.3.0") (h "1g0y7k6gd4msr5b50izs73hh2aqcr1c7nnmvki0i6ybi18kkcdxh")))

(define-public crate-alone_ee-1.4.0 (c (n "alone_ee") (v "1.4.0") (h "0bj4ccsn042wl346d4902c4hq2plh78xaaf4x03y1j5i9y1lv90a")))

(define-public crate-alone_ee-1.4.1 (c (n "alone_ee") (v "1.4.1") (h "1vwgww5vmmzkbyw31fgp6kyw0jidyxnhnkhjzkbwpdmkjnslgrmq")))

(define-public crate-alone_ee-1.4.2 (c (n "alone_ee") (v "1.4.2") (h "1k4lh1xphmlimhncp876z003pbsjffixap6k5az2h33r4i05b6fl")))

(define-public crate-alone_ee-1.5.0 (c (n "alone_ee") (v "1.5.0") (h "0m2hijwndw93ivy0an3y4zhnyns94a5rxh2mp5k2gxy82zjdj7wg")))

(define-public crate-alone_ee-1.6.0 (c (n "alone_ee") (v "1.6.0") (h "140x1vf1mcqpzl2cgf609sv0hrv642gqkis484kq9gpfl6l9k332")))

(define-public crate-alone_ee-1.6.1 (c (n "alone_ee") (v "1.6.1") (h "1b01i09j9z3d82vsacwc9r51rxrwapxd3jrrlz2ilsr2ingzc0lp")))

(define-public crate-alone_ee-1.6.2 (c (n "alone_ee") (v "1.6.2") (h "0sa90nzq291x6f0xaj82ghfrhb8pybw844pzh3b39n6f7l623p4s")))

(define-public crate-alone_ee-1.6.3 (c (n "alone_ee") (v "1.6.3") (h "106g209shngzfa0riqq4kssfcvvl8pwpyr1lyjpsr21av55a60vs")))

(define-public crate-alone_ee-1.6.4 (c (n "alone_ee") (v "1.6.4") (d (list (d (n "criterion") (r "= 0.3.0") (d #t) (k 2)))) (h "0g0gz23p9fxnh72w8i6y5pgjkphz5acdph133lxrjlmhxv05wcn7")))

(define-public crate-alone_ee-1.6.5 (c (n "alone_ee") (v "1.6.5") (d (list (d (n "criterion") (r "= 0.3.0") (d #t) (k 2)) (d (n "odds") (r "^0.3") (f (quote ("std-vec"))) (d #t) (k 0)))) (h "1h6wnp7aijmi6sh72cn7affqi3dyb611c8j9wcmkr5y0mg3v5j9v")))

(define-public crate-alone_ee-1.7.0 (c (n "alone_ee") (v "1.7.0") (d (list (d (n "criterion") (r "= 0.3.0") (d #t) (k 2)) (d (n "odds") (r "^0.3") (f (quote ("std-vec"))) (d #t) (k 0)))) (h "0ifvps53lmlrnfmv8v9bk21s9q3xyq3gxhhnmdzjnx7z3m7srw42")))

(define-public crate-alone_ee-1.7.1 (c (n "alone_ee") (v "1.7.1") (d (list (d (n "criterion") (r "= 0.3.0") (d #t) (k 2)) (d (n "odds") (r "^0.3") (f (quote ("std-vec"))) (d #t) (k 0)))) (h "15r4j51w88p3z87zx8wr9r9bgpcpdwm7syfbm41sna2hmgnp7g01")))

(define-public crate-alone_ee-1.7.2 (c (n "alone_ee") (v "1.7.2") (d (list (d (n "criterion") (r "= 0.3.0") (d #t) (k 2)) (d (n "odds") (r "^0.3") (f (quote ("std-vec"))) (d #t) (k 0)))) (h "0kai9bqdsrazqi5mzh3cxzvpgbl9q12wdjdamjg7lcb257y5i7jw")))

(define-public crate-alone_ee-1.7.3 (c (n "alone_ee") (v "1.7.3") (d (list (d (n "criterion") (r "= 0.3.0") (d #t) (k 2)) (d (n "odds") (r "^0.3") (f (quote ("std-vec"))) (d #t) (k 0)))) (h "187jklxaxmi5y0r5hlqbsy3dq5dh2qdcyghhqnf3n5lr4zydwy5a")))

