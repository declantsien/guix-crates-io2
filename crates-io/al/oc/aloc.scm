(define-module (crates-io al oc aloc) #:use-module (crates-io))

(define-public crate-aloc-0.1.0 (c (n "aloc") (v "0.1.0") (h "0ikh0mrqgx7am7wa5pw9isbj7nfld2qf86wv7llirdw4fyw9hlzd")))

(define-public crate-aloc-0.1.1 (c (n "aloc") (v "0.1.1") (h "1jmwwf8fhqdw070g7nicfhnnqhx7dklr8qs914j5dy6rwxrmzaac")))

(define-public crate-aloc-0.1.2 (c (n "aloc") (v "0.1.2") (h "0nhdzj16281vh3pyga7mnb2as2x14k3843qxd46sfxc3y1by4jpy")))

(define-public crate-aloc-0.1.3 (c (n "aloc") (v "0.1.3") (h "0m3hxnkn599xlk4nsjnsdqzgg68dnicm487r0bg2vv0zxc89vysz")))

(define-public crate-aloc-0.1.4 (c (n "aloc") (v "0.1.4") (h "0y598c4in185qa1yrbgz08q5ffgvpvzg70za5j3091yjssrijfk6")))

(define-public crate-aloc-0.1.5 (c (n "aloc") (v "0.1.5") (h "1miywivp3l7yhbk7vksilgi176m4q2vjp75ynr6i51awj3rxpc5w")))

(define-public crate-aloc-0.1.6 (c (n "aloc") (v "0.1.6") (h "1918rm0a5fwkk0v14ddd6frfkbgn2649rzcfxifp91azj7d4fjfr")))

(define-public crate-aloc-0.1.7 (c (n "aloc") (v "0.1.7") (h "0q5zdk7kc9nvsr2xra0dk1wbgipmlr06a0myymaa6i98k96i9mjz")))

