(define-module (crates-io al in alinio) #:use-module (crates-io))

(define-public crate-alinio-0.1.0 (c (n "alinio") (v "0.1.0") (d (list (d (n "unicode-width") (r "^0.1.9") (d #t) (k 0)))) (h "0bnwrjxid9y8zyl53gw69kxbp5sc1527zacpdxzx71lk07n30hvs")))

(define-public crate-alinio-0.1.1 (c (n "alinio") (v "0.1.1") (d (list (d (n "unicode-width") (r "^0.1.9") (d #t) (k 0)))) (h "0rfk9szgvkbjjpx2zp69fj8n9dh9yqg6zl66vp0irp7vax5m11d8")))

(define-public crate-alinio-0.2.0 (c (n "alinio") (v "0.2.0") (d (list (d (n "unicode-width") (r "^0.1.9") (d #t) (k 0)))) (h "194n1f767pva8krf859izjcm5y0zx430l04jwairrqszfjsj4cxr")))

(define-public crate-alinio-0.2.1 (c (n "alinio") (v "0.2.1") (d (list (d (n "unicode-width") (r "^0.1.9") (d #t) (k 0)))) (h "159vmsm143dj6hdlmxxzq55jdf0m4bxdhrialzmzf7dd53i65rb6")))

