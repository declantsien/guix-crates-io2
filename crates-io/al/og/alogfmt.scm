(define-module (crates-io al og alogfmt) #:use-module (crates-io))

(define-public crate-alogfmt-0.1.0 (c (n "alogfmt") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 2)) (d (n "base16") (r "^0.2.1") (f (quote ("alloc"))) (k 0)) (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "dtoa") (r "^1.0.9") (d #t) (k 0)) (d (n "itoa") (r "^1.0.9") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("std"))) (k 0)) (d (n "serde") (r "^1") (f (quote ("std" "derive"))) (k 2)) (d (n "serde_bytes") (r "^0.11.12") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.43") (d #t) (k 0)))) (h "0hyqxfy61srv8rn18vmdpwkcg5hmhzfs0i77id06j4mpqj7ck61v")))

