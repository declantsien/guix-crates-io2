(define-module (crates-io al og alog) #:use-module (crates-io))

(define-public crate-alog-0.2.2 (c (n "alog") (v "0.2.2") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)))) (h "1sjwnk6vykaqj2f7zbiwz5j0grb8r5jqryb7qmly87q971nza1xx")))

(define-public crate-alog-0.2.3 (c (n "alog") (v "0.2.3") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)))) (h "0nwxsx76l0sidamngc0n895jkw5fizhway35bmb7phv2clnc0ch1")))

(define-public crate-alog-0.2.4 (c (n "alog") (v "0.2.4") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)))) (h "0q7gaaax33l0qlfmk3nzf100agvgmq44s2jxlxavnkl69bx5fkjj")))

(define-public crate-alog-0.3.0 (c (n "alog") (v "0.3.0") (d (list (d (n "clap") (r "^2.33.0") (o #t) (d #t) (k 0)))) (h "1lbjhn5s1nqn11z6pajbrnmmcb77rdcgncn5mjnjxz2w40hmi004") (f (quote (("alog-cli" "clap"))))))

(define-public crate-alog-0.4.1 (c (n "alog") (v "0.4.1") (d (list (d (n "clap") (r "^2.33.0") (o #t) (d #t) (k 0)))) (h "012nyl71visss843mmld77w63yadinnybbd5fssg3zdy3vhn1jlk") (f (quote (("alog-cli" "clap"))))))

(define-public crate-alog-0.4.2 (c (n "alog") (v "0.4.2") (d (list (d (n "clap") (r "^2.33.0") (o #t) (d #t) (k 0)))) (h "1b4s9a1yck7bilikqi75r6sgw5s1w5ycb48rkxqfklyhx706pjwc") (f (quote (("alog-cli" "clap"))))))

(define-public crate-alog-0.4.3 (c (n "alog") (v "0.4.3") (d (list (d (n "clap") (r "^2.33.0") (o #t) (d #t) (k 0)))) (h "0xd18mb5cfj045nxzg2vfl26b2lyvgb10i4ahrhz9w3n8l1m70p8") (f (quote (("alog-cli" "clap"))))))

(define-public crate-alog-0.5.2 (c (n "alog") (v "0.5.2") (d (list (d (n "clap") (r "^2.33.0") (o #t) (d #t) (k 0)))) (h "0bwx326hbyjx1cpvj8kmyk0qlk5vxwmshb5khq217ri6w24mngwj") (f (quote (("alog-cli" "clap"))))))

(define-public crate-alog-0.5.3 (c (n "alog") (v "0.5.3") (d (list (d (n "clap") (r "^2.33.0") (o #t) (d #t) (k 0)))) (h "0hsckyjhv96i8p6idakrj4kwmb5jn9gfknzjk0wssxdimj0dr1k4") (f (quote (("alog-cli" "clap"))))))

(define-public crate-alog-0.5.4 (c (n "alog") (v "0.5.4") (d (list (d (n "clap") (r "^2.33.0") (o #t) (d #t) (k 0)))) (h "182kazxys8q2kkfb5pszh4is4k4m6la9cwzzhnbzdx1xr0zvm922") (f (quote (("alog-cli" "clap"))))))

(define-public crate-alog-0.5.5 (c (n "alog") (v "0.5.5") (d (list (d (n "clap") (r "^2.33.0") (o #t) (d #t) (k 0)))) (h "1gbqmpv505hsb062qsfqm6v7zp2x9c4y58jwdldcsdd0vhxkjwlq") (f (quote (("alog-cli" "clap"))))))

(define-public crate-alog-0.5.6 (c (n "alog") (v "0.5.6") (d (list (d (n "clap") (r "^2.33.0") (o #t) (d #t) (k 0)))) (h "1rs0pjw2h05mm4vv25gxfinrfl27bx3xw11jwxgbxa5gcyq2hkn4") (f (quote (("alog-cli" "clap"))))))

(define-public crate-alog-0.5.7 (c (n "alog") (v "0.5.7") (d (list (d (n "clap") (r "^2.33.0") (o #t) (d #t) (k 0)))) (h "10yf9kgmchllxqhp10r12ab5sfrjmpbnbrkl23qmipbvp9ibgsyk") (f (quote (("alog-cli" "clap"))))))

(define-public crate-alog-0.5.8 (c (n "alog") (v "0.5.8") (d (list (d (n "clap") (r "^2.33.0") (o #t) (d #t) (k 0)))) (h "0nkaw26r1893pf4h4k2859bap0c71xs2xss2fixrz33jqymmrjp5") (f (quote (("alog-cli" "clap"))))))

(define-public crate-alog-0.5.9 (c (n "alog") (v "0.5.9") (d (list (d (n "clap") (r "^2.33.0") (o #t) (d #t) (k 0)))) (h "0njyv18k642fmhf2l8rkdjrq50x2kxmk3r453kg7wjjkxjrzg49q") (f (quote (("alog-cli" "clap"))))))

(define-public crate-alog-0.5.10 (c (n "alog") (v "0.5.10") (d (list (d (n "clap") (r "^2.33.0") (o #t) (d #t) (k 0)))) (h "1igs0dd4dfymmgjbmnkzixx62k9755amh15vyyzrad3l54q30f77") (f (quote (("alog-cli" "clap"))))))

(define-public crate-alog-0.6.0 (c (n "alog") (v "0.6.0") (d (list (d (n "clap") (r "^2.33") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "regex") (r "^1.4") (f (quote ("std" "perf-cache"))) (k 0)))) (h "0phb2dvsk3nr7lv8b6579h4wyxqvsjvqfrsqrpx5yk6pkacamki7") (f (quote (("alog-cli" "clap"))))))

(define-public crate-alog-0.6.1 (c (n "alog") (v "0.6.1") (d (list (d (n "clap") (r "^2.33") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "regex") (r "^1.4") (f (quote ("std" "perf-cache"))) (k 0)))) (h "0yiaqvjcymgljjajgb41dkckxjahyigg4wqsj3ibgkv3qw7nzdaj") (f (quote (("alog-cli" "clap"))))))

(define-public crate-alog-0.6.2 (c (n "alog") (v "0.6.2") (d (list (d (n "clap") (r "^2.33") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "regex") (r "^1.5") (f (quote ("std" "perf"))) (k 0)))) (h "1rch3jhn35hfc47kjsbra3d9jxqdfmznarivgbl6yi34xf9mzsdq") (f (quote (("alog-cli" "clap"))))))

(define-public crate-alog-0.6.3 (c (n "alog") (v "0.6.3") (d (list (d (n "clap") (r "^2.33") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "regex") (r "^1.5") (f (quote ("std" "perf"))) (k 0)))) (h "18bisrxl8i7l1l0i1v9sd9ccmq5hfmbp4fv8dvjnfaznrw62zsvi") (f (quote (("alog-cli" "clap"))))))

(define-public crate-alog-0.6.4 (c (n "alog") (v "0.6.4") (d (list (d (n "clap") (r "^2.33") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "regex") (r "^1.5") (f (quote ("std" "perf"))) (k 0)))) (h "180vr275vxsaf7qvcjx2bndkbk34jrbly77m627zlr3y948v528f") (f (quote (("alog-cli" "clap"))))))

(define-public crate-alog-0.6.5 (c (n "alog") (v "0.6.5") (d (list (d (n "clap") (r "^3") (f (quote ("std"))) (o #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "regex") (r "^1.5") (f (quote ("std" "perf"))) (k 0)))) (h "1n8mbyj1ccbs6igm5gzgg65g7ac8x1mj6rx14gs2bix76zsvsdhx") (f (quote (("alog-cli" "clap"))))))

(define-public crate-alog-0.7.0 (c (n "alog") (v "0.7.0") (d (list (d (n "clap") (r "^3") (f (quote ("std"))) (o #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "regex") (r "^1.6") (f (quote ("std" "perf"))) (k 0)))) (h "1kf9qcg0kyd4697yf147zqr21c4yrgsn2ngsp69y3w6dix463qkj") (f (quote (("alog-cli" "clap"))))))

(define-public crate-alog-0.7.1 (c (n "alog") (v "0.7.1") (d (list (d (n "clap") (r "^3") (f (quote ("std"))) (o #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "regex") (r "^1.6") (f (quote ("std" "perf"))) (k 0)))) (h "0vz7aq14nq08sajvra8ml5rz11l1pzvdjc0fxcnvmgs4fjq6xgzj") (f (quote (("alog-cli" "clap"))))))

(define-public crate-alog-0.7.2 (c (n "alog") (v "0.7.2") (d (list (d (n "clap") (r "^4") (f (quote ("std"))) (o #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "regex") (r "^1.6") (f (quote ("std" "perf"))) (k 0)))) (h "16wqpnq467nwk59yndyf03hgk4b4a49axahivh39fzzcgm38wknj") (f (quote (("alog-cli" "clap"))))))

(define-public crate-alog-0.7.3 (c (n "alog") (v "0.7.3") (d (list (d (n "clap") (r "^4") (f (quote ("std"))) (o #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "regex") (r "^1.6") (f (quote ("std" "perf"))) (k 0)))) (h "0c6sfw7sikghngxf60aj4xb4fsm0lgd8kjg7b916jxami96a8vpk") (f (quote (("alog-cli" "clap"))))))

