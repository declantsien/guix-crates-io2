(define-module (crates-io al gx algx) #:use-module (crates-io))

(define-public crate-algx-0.1.0 (c (n "algx") (v "0.1.0") (h "0zn4xfk0y0m8idbwbjwj1yqnmcvjmlraj942460npn0q4p2sxi1i")))

(define-public crate-algx-0.2.0 (c (n "algx") (v "0.2.0") (d (list (d (n "js-sys") (r "^0.3") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "wasm-bindgen-futures") (r "^0.4") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "076dmhvkmcx911caxm7m0vicg3inrh3mp2rshgmgcq9slvr1lapw")))

