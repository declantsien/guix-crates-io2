(define-module (crates-io al ek aleksandr-vinokurov-cv) #:use-module (crates-io))

(define-public crate-aleksandr-vinokurov-cv-2023.10.0 (c (n "aleksandr-vinokurov-cv") (v "2023.10.0") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "image") (r "^0.24.7") (d #t) (k 0)) (d (n "tectonic") (r "^0.14.1") (d #t) (k 0)))) (h "1qr36rs28q3vppy7yhl0md62xn3xy2gkhqlzl2rbhbj4y8g4nf5j")))

(define-public crate-aleksandr-vinokurov-cv-2023.10.1 (c (n "aleksandr-vinokurov-cv") (v "2023.10.1") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "image") (r "^0.24.7") (d #t) (k 0)) (d (n "tectonic") (r "^0.14.1") (d #t) (k 0)))) (h "0vfpv6l029v9jigh36wyl0ri95sysizw5ix23344k1pbx1v87lca")))

(define-public crate-aleksandr-vinokurov-cv-2024.1.16 (c (n "aleksandr-vinokurov-cv") (v "2024.1.16") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "image") (r "^0.24.7") (d #t) (k 0)) (d (n "tectonic") (r "^0.14.1") (d #t) (k 0)))) (h "19yxl9jky3k8v7axzyqxhh9rsg8pidm0ixspnmvl0favp6l7dr2r")))

(define-public crate-aleksandr-vinokurov-cv-2024.2.5 (c (n "aleksandr-vinokurov-cv") (v "2024.2.5") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "image") (r "^0.24.7") (d #t) (k 0)) (d (n "tectonic") (r "^0.14.1") (d #t) (k 0)))) (h "04jjphp7nxjcji24z2vh32fbspm5ghayqdzcs3wyx0j4q1njdn69")))

(define-public crate-aleksandr-vinokurov-cv-2024.2.6 (c (n "aleksandr-vinokurov-cv") (v "2024.2.6") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "image") (r "^0.24.7") (d #t) (k 0)) (d (n "tectonic") (r "^0.14.1") (d #t) (k 0)))) (h "0mizjs0043qvdc3zkbpdw5bv2s8xids7q9xr7dj6218yyc671ahm")))

