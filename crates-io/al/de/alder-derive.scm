(define-module (crates-io al de alder-derive) #:use-module (crates-io))

(define-public crate-alder-derive-0.1.0 (c (n "alder-derive") (v "0.1.0") (d (list (d (n "darling") (r "^0.10.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "unindent") (r "^0.1.5") (d #t) (k 0)))) (h "0kahx2684v9vc0q0y5nb63zgqrhid97iic04vflc4lzjzsm0gvq9")))

(define-public crate-alder-derive-0.1.1 (c (n "alder-derive") (v "0.1.1") (d (list (d (n "darling") (r "^0.10.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "unindent") (r "^0.1.5") (d #t) (k 0)))) (h "1dnb8hhxwl4hx670m2xbmk6cg9rz2360sfqk5gryb3nn5p32z7vx")))

(define-public crate-alder-derive-0.2.0 (c (n "alder-derive") (v "0.2.0") (d (list (d (n "darling") (r "^0.10.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "unindent") (r "^0.1.5") (d #t) (k 0)))) (h "112xn6imfprazjm8392g1s8rg3h1bznfhbr61xw60fnayqz2vpsq")))

(define-public crate-alder-derive-0.2.1 (c (n "alder-derive") (v "0.2.1") (d (list (d (n "darling") (r "^0.10.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "unindent") (r "^0.1.5") (d #t) (k 0)))) (h "0fdy0rwgflpn0xz84fd34cqanypiq4zcibca7dvb70740rgn7rh5")))

(define-public crate-alder-derive-0.2.2 (c (n "alder-derive") (v "0.2.2") (d (list (d (n "darling") (r "^0.10.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "unindent") (r "^0.1.5") (d #t) (k 0)))) (h "07gkiwn6r3f0jipdi5vdrr05yway9kddgljb56znpxlc0qn6xxx3")))

(define-public crate-alder-derive-0.2.3 (c (n "alder-derive") (v "0.2.3") (d (list (d (n "darling") (r "^0.10.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "unindent") (r "^0.1.5") (d #t) (k 0)))) (h "1a1dc5ikn5k1kbcw5zrb36q54xwi5s6pdsm01dy08nys0h0jliv2")))

