(define-module (crates-io al l_ all_env) #:use-module (crates-io))

(define-public crate-all_env-0.1.0 (c (n "all_env") (v "0.1.0") (d (list (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1g227gvpi1srb74a04ax142zpib032qnldim6m119mcq24hbiyhs")))

