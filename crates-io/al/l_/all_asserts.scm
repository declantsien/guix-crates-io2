(define-module (crates-io al l_ all_asserts) #:use-module (crates-io))

(define-public crate-all_asserts-0.1.0 (c (n "all_asserts") (v "0.1.0") (h "16m1b3i5z7hi421xryxqgfd8grwppz5b73af2yhylmkrrx74z66y") (y #t)))

(define-public crate-all_asserts-0.1.1 (c (n "all_asserts") (v "0.1.1") (h "1fi2la2si0prm7yhxldwqv6360l2gzykvx8grp6a8bldayylprjw") (y #t)))

(define-public crate-all_asserts-0.1.2 (c (n "all_asserts") (v "0.1.2") (h "1cz885w5r1cjv1r33bwg6yf97jfw3j170xb9hng9rlm3gqgw8qcg") (y #t)))

(define-public crate-all_asserts-0.1.3 (c (n "all_asserts") (v "0.1.3") (h "146w883iljdjc0jih8mmsi1sx26ycwz8m6cdhq34cbhdqxsgisfd")))

(define-public crate-all_asserts-0.1.4 (c (n "all_asserts") (v "0.1.4") (h "173gmsc4d120b9gkc7p5565xdgcnn5mkhd3znaflnzbq85rb146v")))

(define-public crate-all_asserts-1.0.0 (c (n "all_asserts") (v "1.0.0") (h "1vjklzyivgygx9g3bwy5c57sy51jhm6iczxjiwkibpf0l4bw2rqa")))

(define-public crate-all_asserts-1.0.1 (c (n "all_asserts") (v "1.0.1") (h "1h845b6lkxi3ivlb4rs8jzdi2hzdhb7caw408j9cj4c8paigblrr")))

(define-public crate-all_asserts-2.0.0 (c (n "all_asserts") (v "2.0.0") (h "0r2k7d9xv5pi5fy13xgs2d4djmhjb44hh5jdzjjd7vkdbcgmg56y")))

(define-public crate-all_asserts-2.0.1 (c (n "all_asserts") (v "2.0.1") (h "0grrmgw1k42k2bsghv1bhxmbksavx1c3k95qn9y0h5ihmc4qzp4h")))

(define-public crate-all_asserts-2.1.0 (c (n "all_asserts") (v "2.1.0") (h "16wx0s5hmcm7qg4irvrpvvzs8xr1hdv3ymn0119yfdy01070pfj6")))

(define-public crate-all_asserts-2.2.0 (c (n "all_asserts") (v "2.2.0") (h "0cxj7cnaxhcl5d393lx13lbrajz3jz3nwaqhbjb1a8imi1zi3bnx")))

(define-public crate-all_asserts-2.3.0 (c (n "all_asserts") (v "2.3.0") (h "0czn9zfv417hy3nv1cmsy5d45q2j67vmx5qih3xgxb9x0c6yk7sd")))

(define-public crate-all_asserts-2.3.1 (c (n "all_asserts") (v "2.3.1") (h "0051psbhdpz4zmm68bjzmmy2p29x6shws0x1rmsc4mqhrbqclxya")))

