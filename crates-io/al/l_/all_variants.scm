(define-module (crates-io al l_ all_variants) #:use-module (crates-io))

(define-public crate-all_variants-0.1.0 (c (n "all_variants") (v "0.1.0") (d (list (d (n "every_variant") (r "^0.1.0") (d #t) (k 0)))) (h "0l8vnhpmg6sr2mcyn8a3j3yd45a72l7ndfjpzhfb1dmh1669lqxy") (y #t)))

(define-public crate-all_variants-0.1.1 (c (n "all_variants") (v "0.1.1") (d (list (d (n "every_variant") (r "^0.1.1") (d #t) (k 0)))) (h "075rw9qpvm82s89z2448vsw33wlcz0zg9b0ks5wicpzp4k05byi6") (y #t)))

(define-public crate-all_variants-0.1.2 (c (n "all_variants") (v "0.1.2") (d (list (d (n "every_variant") (r "^0.1.1") (d #t) (k 0)))) (h "17d6xgii6k985a1qy4jlgn99ldmbih1fvz8vb96kf27mrfwc3jwq") (y #t)))

(define-public crate-all_variants-0.1.3 (c (n "all_variants") (v "0.1.3") (d (list (d (n "every_variant") (r "^0.1.1") (d #t) (k 0)))) (h "1574vsnrfyfmmmd7rwnc12xy64i4266zg376brh6zypgpgigp9i7") (y #t)))

(define-public crate-all_variants-0.1.4 (c (n "all_variants") (v "0.1.4") (d (list (d (n "every_variant") (r "^0.1.2") (d #t) (k 0)))) (h "0000cjkac6l78m2mjxmykkx1da5g35c78c44ybr9z7r5mlbm9v4i") (y #t)))

(define-public crate-all_variants-0.2.0 (c (n "all_variants") (v "0.2.0") (d (list (d (n "every_variant") (r "^0.1.1") (d #t) (k 0)))) (h "015y1s8nf0j5r2g503d5gjc7zwggbq5csxa9rbsyk373k11ma986") (y #t)))

(define-public crate-all_variants-0.2.1 (c (n "all_variants") (v "0.2.1") (d (list (d (n "every_variant") (r "^0.1.1") (d #t) (k 0)) (d (n "heapless") (r "^0.5") (o #t) (d #t) (k 0)))) (h "1k9whnvcfpnjxzkybgrimygyliwz96jqc0qf02ahryb9bll6l1f1") (f (quote (("ev_heapless" "heapless") ("default")))) (y #t)))

(define-public crate-all_variants-0.2.2 (c (n "all_variants") (v "0.2.2") (d (list (d (n "every_variant") (r "^0.1.1") (d #t) (k 0)) (d (n "heapless") (r "^0.5") (o #t) (d #t) (k 0)))) (h "1a1kpbrml2ibh7rigk1j0him23wn1fjrg1w38b6df5z0y7cyxj8j") (f (quote (("ev_heapless" "heapless") ("default")))) (y #t)))

(define-public crate-all_variants-0.2.3 (c (n "all_variants") (v "0.2.3") (d (list (d (n "every_variant") (r "^0.1.3") (d #t) (k 0)) (d (n "heapless") (r "^0.5") (o #t) (d #t) (k 0)))) (h "0a9j20285d5ah4nxw0n9xlx819bclqnz136rnir5zbkdcm2pvhwm") (f (quote (("ev_heapless" "heapless") ("default")))) (y #t)))

(define-public crate-all_variants-0.2.4 (c (n "all_variants") (v "0.2.4") (d (list (d (n "every_variant") (r "^0.1.5") (d #t) (k 0)) (d (n "heapless") (r "^0.5") (o #t) (d #t) (k 0)))) (h "1pyf8nxskm54w7v7zl3czkibk9l2ngs8b3zdzxrvpdvljq2w7510") (f (quote (("ev_heapless" "heapless") ("default")))) (y #t)))

(define-public crate-all_variants-0.2.5 (c (n "all_variants") (v "0.2.5") (d (list (d (n "every_variant") (r "^0.1.6") (d #t) (k 0)) (d (n "heapless") (r "^0.5") (o #t) (d #t) (k 0)))) (h "1sik7b6kzjqn1zc6r62qcgly0s9h92l4d6sgnhypxdfl737idmfm") (f (quote (("ev_heapless" "heapless") ("default")))) (y #t)))

(define-public crate-all_variants-0.2.6 (c (n "all_variants") (v "0.2.6") (d (list (d (n "every_variant") (r "^0.2.0") (d #t) (k 0)) (d (n "heapless") (r "^0.5") (o #t) (d #t) (k 0)))) (h "1zbln1axly6r6iwc12c44zicq0h2rcgk3dyvlx6hyg8s5380dgay") (f (quote (("ev_heapless" "heapless") ("default")))) (y #t)))

(define-public crate-all_variants-0.2.8 (c (n "all_variants") (v "0.2.8") (d (list (d (n "every_variant") (r "^0.2.1") (d #t) (k 0)) (d (n "heapless") (r "^0.5") (o #t) (d #t) (k 0)))) (h "0jdmvh7la02pvd2x9lj6lw2klcjphw4yjhf11nd3vr5sxxbj9v1f") (f (quote (("ev_heapless" "heapless") ("default"))))))

(define-public crate-all_variants-0.3.0 (c (n "all_variants") (v "0.3.0") (h "0bc6x4gggbkjp4wf71z40wmc70n8mpbnx4pq0nwjn14r818idq6i") (y #t)))

(define-public crate-all_variants-0.3.1 (c (n "all_variants") (v "0.3.1") (h "0vxc3mix2xlvcqvbiggy03qhbal0cihzlma09gxy4a2jx9bdrn0z")))

