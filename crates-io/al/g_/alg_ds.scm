(define-module (crates-io al g_ alg_ds) #:use-module (crates-io))

(define-public crate-alg_ds-0.0.1 (c (n "alg_ds") (v "0.0.1") (h "0c1604f3vhzzqy811fnhqj6a5864zbc16nk5i9ghcrw250x8ajaz")))

(define-public crate-alg_ds-0.0.2 (c (n "alg_ds") (v "0.0.2") (h "1xfgli66s6hvqc3sswd3r16djy84shja4j4h53cmj5h9bmma699j")))

(define-public crate-alg_ds-0.0.3 (c (n "alg_ds") (v "0.0.3") (h "04a8lbhg6z0563cfll786pjk98g78ysx2hh5m6ixmv1bl9m73dw9")))

(define-public crate-alg_ds-0.2.7 (c (n "alg_ds") (v "0.2.7") (h "13kkw1qwkz1xngcwqjdbqdvg9dzif9jl8gfamvk1pgm87vwwknvh")))

(define-public crate-alg_ds-0.3.0 (c (n "alg_ds") (v "0.3.0") (h "03r03czpwfi7xs1if4lj433wvlmh6wir8p8aha71nxxsyjafgpz2")))

(define-public crate-alg_ds-0.3.1 (c (n "alg_ds") (v "0.3.1") (h "0vck9bx05bvlyim5947k4ql72acrsxcyrhfbz017p83fgr4kc6iw")))

