(define-module (crates-io al ej alejandra) #:use-module (crates-io))

(define-public crate-alejandra-1.5.0 (c (n "alejandra") (v "1.5.0") (d (list (d (n "mimalloc") (r "^0") (t "aarch64-unknown-linux-musl") (k 0)) (d (n "mimalloc") (r "^0") (t "armv6l-unknown-linux-musleabihf") (k 0)) (d (n "mimalloc") (r "^0") (t "armv7l-unknown-linux-musleabihf") (k 0)) (d (n "mimalloc") (r "^0") (t "i686-unknown-linux-musl") (k 0)) (d (n "mimalloc") (r "^0") (t "x86_64-unknown-linux-gnu") (k 0)) (d (n "mimalloc") (r "^0") (t "x86_64-unknown-linux-musl") (k 0)) (d (n "rnix") (r "^0") (d #t) (k 0)) (d (n "rowan") (r "^0.12.6") (d #t) (k 0)))) (h "0zyb6981a6n4zj4qi2hr353ix9z7bz9c2j9j8j6b5x275wqy68r5")))

