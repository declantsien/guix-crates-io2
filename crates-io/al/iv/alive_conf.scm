(define-module (crates-io al iv alive_conf) #:use-module (crates-io))

(define-public crate-alive_conf-0.1.1 (c (n "alive_conf") (v "0.1.1") (d (list (d (n "aok") (r "^0.1.3") (d #t) (k 0)) (d (n "dashmap") (r "^5.5.3") (d #t) (k 0)) (d (n "ifs") (r "^0.1.27") (f (quote ("fs"))) (k 0)) (d (n "loginit") (r "^0.1.10") (d #t) (k 2)) (d (n "serde_yaml") (r "^0.9.34") (d #t) (k 0)) (d (n "static_init") (r "^1.0.3") (d #t) (k 2)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 2)))) (h "0aivq08h8nwdbz042h4cjy68xqq9mld32bddy8vpp70f1n3p1xyp")))

