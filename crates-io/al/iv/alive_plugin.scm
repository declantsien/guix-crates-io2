(define-module (crates-io al iv alive_plugin) #:use-module (crates-io))

(define-public crate-alive_plugin-0.1.5 (c (n "alive_plugin") (v "0.1.5") (d (list (d (n "aok") (r "^0.1.3") (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "rkyv") (r "^0.7.44") (d #t) (k 0)) (d (n "skiplist") (r "^0.5.1") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("macros" "rt" "rt-multi-thread" "time" "fs" "io-util"))) (d #t) (k 0)) (d (n "xerr") (r "^0.1.11") (d #t) (k 0)))) (h "0r44sjkdc4b1h0pr9z0pmnnqkli13vjx1cgdc2y2s2fdsmwn2nl3")))

(define-public crate-alive_plugin-0.1.10 (c (n "alive_plugin") (v "0.1.10") (d (list (d (n "alive_alter") (r "^0.1.1") (d #t) (k 0)) (d (n "alive_api") (r "^0.1.1") (d #t) (k 0)) (d (n "alive_watch") (r "^0.1.1") (d #t) (k 0)) (d (n "aok") (r "^0.1.3") (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "skiplist") (r "^0.5.1") (d #t) (k 0)) (d (n "speedy") (r "^0.8.7") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("macros" "rt" "rt-multi-thread" "time" "fs" "io-util"))) (d #t) (k 0)) (d (n "xerr") (r "^0.1.11") (d #t) (k 0)))) (h "1gygv3hgajr7z72m3z0y4bsqbw531fv0nkgm48j1avfa1kli2f0y") (f (quote (("enable") ("default"))))))

