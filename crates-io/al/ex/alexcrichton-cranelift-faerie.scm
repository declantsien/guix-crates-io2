(define-module (crates-io al ex alexcrichton-cranelift-faerie) #:use-module (crates-io))

(define-public crate-alexcrichton-cranelift-faerie-0.52.0 (c (n "alexcrichton-cranelift-faerie") (v "0.52.0") (d (list (d (n "alexcrichton-cranelift-codegen") (r "^0.52.0") (f (quote ("std"))) (k 0)) (d (n "alexcrichton-cranelift-module") (r "^0.52.0") (d #t) (k 0)) (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "faerie") (r "^0.14.0") (d #t) (k 0)) (d (n "goblin") (r "^0.1.0") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.10") (d #t) (k 0)))) (h "0s6h1raxi4m394yfh3nyrcj1jbhd425x36vszjzw22bipkb6ch1v")))

(define-public crate-alexcrichton-cranelift-faerie-0.53.0 (c (n "alexcrichton-cranelift-faerie") (v "0.53.0") (d (list (d (n "alexcrichton-cranelift-codegen") (r "^0.53.0") (f (quote ("std"))) (k 0)) (d (n "alexcrichton-cranelift-module") (r "^0.53.0") (d #t) (k 0)) (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "faerie") (r "^0.14.0") (d #t) (k 0)) (d (n "goblin") (r "^0.1.0") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.10") (d #t) (k 0)))) (h "1bk4zzk9ys5gbbcn5f54s86h12l8fq1nab1lcqgbxv137g4lbs8f")))

