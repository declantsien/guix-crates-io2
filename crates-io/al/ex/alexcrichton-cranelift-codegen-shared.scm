(define-module (crates-io al ex alexcrichton-cranelift-codegen-shared) #:use-module (crates-io))

(define-public crate-alexcrichton-cranelift-codegen-shared-0.52.0 (c (n "alexcrichton-cranelift-codegen-shared") (v "0.52.0") (h "0cz38l54swyi9kj5lkx162nix252cwpvj9bm9jzxhk3miv5fhqz1")))

(define-public crate-alexcrichton-cranelift-codegen-shared-0.53.0 (c (n "alexcrichton-cranelift-codegen-shared") (v "0.53.0") (h "1cdfya9w6d2yr8n0zinkzdwd8ycfrfjbs8y97pkgkl23lm3y8w8l")))

