(define-module (crates-io al ex alexcrichton-cranelift) #:use-module (crates-io))

(define-public crate-alexcrichton-cranelift-0.53.0 (c (n "alexcrichton-cranelift") (v "0.53.0") (d (list (d (n "alexcrichton-cranelift-codegen") (r "^0.53.0") (k 0)) (d (n "alexcrichton-cranelift-frontend") (r "^0.53.0") (k 0)))) (h "140znsaflv5c86kqnqr7nds28wxnz1lmg9r7cgx9pz87cjpgq127") (f (quote (("std" "alexcrichton-cranelift-codegen/std" "alexcrichton-cranelift-frontend/std") ("default" "std") ("core" "alexcrichton-cranelift-codegen/core" "alexcrichton-cranelift-frontend/core"))))))

