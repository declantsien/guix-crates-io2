(define-module (crates-io al ex alex-db-client) #:use-module (crates-io))

(define-public crate-alex-db-client-0.1.0 (c (n "alex-db-client") (v "0.1.0") (d (list (d (n "alex-db-lib") (r "^0.1") (d #t) (k 0)) (d (n "reedline-repl-rs") (r "^1.0") (f (quote ("async"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.25") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^1.3") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "0rjbnrj5c2k22zg2240kvl1cjpffk2w9hk1m7mp8x1ya6d88zqgy") (r "1.64")))

