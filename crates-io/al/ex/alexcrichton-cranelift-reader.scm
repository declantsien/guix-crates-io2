(define-module (crates-io al ex alexcrichton-cranelift-reader) #:use-module (crates-io))

(define-public crate-alexcrichton-cranelift-reader-0.52.0 (c (n "alexcrichton-cranelift-reader") (v "0.52.0") (d (list (d (n "alexcrichton-cranelift-codegen") (r "^0.52.0") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.10") (d #t) (k 0)))) (h "0ighgxidrc6wc9ypkkf7lai41n65k91llqpr9yh0zw85phsmkahn")))

(define-public crate-alexcrichton-cranelift-reader-0.53.0 (c (n "alexcrichton-cranelift-reader") (v "0.53.0") (d (list (d (n "alexcrichton-cranelift-codegen") (r "^0.53.0") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.10") (d #t) (k 0)))) (h "1k3fp9qa81270hgnwzz5qq0i4ra9nwi5jlf1y8lns0lv7gcvcf6d")))

