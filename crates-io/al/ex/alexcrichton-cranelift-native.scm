(define-module (crates-io al ex alexcrichton-cranelift-native) #:use-module (crates-io))

(define-public crate-alexcrichton-cranelift-native-0.52.0 (c (n "alexcrichton-cranelift-native") (v "0.52.0") (d (list (d (n "alexcrichton-cranelift-codegen") (r "^0.52.0") (k 0)) (d (n "raw-cpuid") (r "^7.0.3") (d #t) (t "cfg(any(target_arch = \"x86\", target_arch = \"x86_64\"))") (k 0)) (d (n "target-lexicon") (r "^0.10") (d #t) (k 0)))) (h "1s174a5bpd196i323ar3fsnrpxq6k1iajilrapqy2ffc3igpskip") (f (quote (("std" "alexcrichton-cranelift-codegen/std") ("default" "std") ("core" "alexcrichton-cranelift-codegen/core" "raw-cpuid/nightly"))))))

(define-public crate-alexcrichton-cranelift-native-0.53.0 (c (n "alexcrichton-cranelift-native") (v "0.53.0") (d (list (d (n "alexcrichton-cranelift-codegen") (r "^0.53.0") (k 0)) (d (n "raw-cpuid") (r "^7.0.3") (d #t) (t "cfg(any(target_arch = \"x86\", target_arch = \"x86_64\"))") (k 0)) (d (n "target-lexicon") (r "^0.10") (d #t) (k 0)))) (h "1rkvan0cs3ikhspxlk6q87gzgkjhnwb788247lc7mamg1i3i8bsx") (f (quote (("std" "alexcrichton-cranelift-codegen/std") ("default" "std") ("core" "alexcrichton-cranelift-codegen/core" "raw-cpuid/nightly"))))))

