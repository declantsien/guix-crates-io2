(define-module (crates-io al ex alexcrichton-cranelift-codegen-meta) #:use-module (crates-io))

(define-public crate-alexcrichton-cranelift-codegen-meta-0.52.0 (c (n "alexcrichton-cranelift-codegen-meta") (v "0.52.0") (d (list (d (n "alexcrichton-cranelift-codegen-shared") (r "^0.52.0") (d #t) (k 0)) (d (n "alexcrichton-cranelift-entity") (r "^0.52.0") (d #t) (k 0)))) (h "0a0pli0ib4ab6g4rghizcply6aigixa9qkfmcxmd1csqxxfyisaq")))

(define-public crate-alexcrichton-cranelift-codegen-meta-0.53.0 (c (n "alexcrichton-cranelift-codegen-meta") (v "0.53.0") (d (list (d (n "alexcrichton-cranelift-codegen-shared") (r "^0.53.0") (d #t) (k 0)) (d (n "alexcrichton-cranelift-entity") (r "^0.53.0") (d #t) (k 0)))) (h "0rw27av6gl9vpddirh9arrqc2b6fd1dzjh5jjgzxyv5iwi4v8v6j")))

