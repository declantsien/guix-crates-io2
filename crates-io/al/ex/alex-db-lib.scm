(define-module (crates-io al ex alex-db-lib) #:use-module (crates-io))

(define-public crate-alex-db-lib-0.1.0 (c (n "alex-db-lib") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "lz4_flex") (r "^0.10") (d #t) (k 0)) (d (n "regex") (r "^1.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "utoipa") (r "^3.0") (f (quote ("chrono"))) (d #t) (k 0)) (d (n "uuid") (r "^1.3") (f (quote ("serde" "v4"))) (d #t) (k 0)) (d (n "validator") (r "^0.16") (f (quote ("derive"))) (d #t) (k 0)))) (h "0rsk8pa8abrrdaifbkb9bgq8ydswkxric07r4hqlr9vhxy93py5s") (r "1.60")))

