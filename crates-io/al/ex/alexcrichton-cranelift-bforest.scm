(define-module (crates-io al ex alexcrichton-cranelift-bforest) #:use-module (crates-io))

(define-public crate-alexcrichton-cranelift-bforest-0.52.0 (c (n "alexcrichton-cranelift-bforest") (v "0.52.0") (d (list (d (n "alexcrichton-cranelift-entity") (r "^0.52.0") (k 0)))) (h "0qnnlfqml6q15213y008c8m8h6mymwkpr6zl87981vkz89v12yy2")))

(define-public crate-alexcrichton-cranelift-bforest-0.53.0 (c (n "alexcrichton-cranelift-bforest") (v "0.53.0") (d (list (d (n "alexcrichton-cranelift-entity") (r "^0.53.0") (k 0)))) (h "1wix65ps5gsnmmf6qj70hqplnmjcl3j61w58981z9w7c049l2zb0")))

