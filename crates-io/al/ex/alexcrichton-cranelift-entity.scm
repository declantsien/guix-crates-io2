(define-module (crates-io al ex alexcrichton-cranelift-entity) #:use-module (crates-io))

(define-public crate-alexcrichton-cranelift-entity-0.52.0 (c (n "alexcrichton-cranelift-entity") (v "0.52.0") (d (list (d (n "serde") (r "^1.0.94") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1sjncq1l5f57jrmyql929cv2ql500zdd8y7ciczma6a6wzdv5g4k") (f (quote (("enable-serde" "serde"))))))

(define-public crate-alexcrichton-cranelift-entity-0.53.0 (c (n "alexcrichton-cranelift-entity") (v "0.53.0") (d (list (d (n "serde") (r "^1.0.94") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1350vgbl7dqbcfwkz9gjh49rck7lw22dvxckcpkz8cyf8z47j00h") (f (quote (("enable-serde" "serde"))))))

