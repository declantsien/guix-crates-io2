(define-module (crates-io al ex alexa_sdk) #:use-module (crates-io))

(define-public crate-alexa_sdk-0.1.0 (c (n "alexa_sdk") (v "0.1.0") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "00lfagc7sml91csmh27s1zkgzhc61sw3wayc9xybm6szzc0p4lc9")))

(define-public crate-alexa_sdk-0.1.1 (c (n "alexa_sdk") (v "0.1.1") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1igiqzzi06ynnjzv0890b1yppy5jv9yv6hm7gvjgvybn3jmkc3rf")))

(define-public crate-alexa_sdk-0.1.2 (c (n "alexa_sdk") (v "0.1.2") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1zwqpfcwxryllln4hx3qiaz74gzw9hlxn9d9bjwskmkc7q6jrf3c")))

(define-public crate-alexa_sdk-0.1.3 (c (n "alexa_sdk") (v "0.1.3") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0k29vfbn1svzh5bcpp7brkzyf0sy863sqaq199n0mh2gdbx8cagy")))

(define-public crate-alexa_sdk-0.1.4 (c (n "alexa_sdk") (v "0.1.4") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0b69jrby1hljkalas2shn5vircxc5yscsdcw0m9rm3a8zi1w0wsi")))

(define-public crate-alexa_sdk-0.1.5 (c (n "alexa_sdk") (v "0.1.5") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1fsiahnm1llgcl4in7rz3g2zxyi633rgqhrzkrdq8mkmzm1ggij9")))

