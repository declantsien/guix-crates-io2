(define-module (crates-io al ex alexa) #:use-module (crates-io))

(define-public crate-alexa-0.1.0 (c (n "alexa") (v "0.1.0") (d (list (d (n "bodyparser") (r "^0.4.1") (d #t) (k 0)) (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "iron") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_codegen") (r "^0.8") (d #t) (k 1)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)))) (h "11rc27xag7mbbmxgqwh4skwwn71rwvrik5i2z0z4zkc2zi38nlkf")))

(define-public crate-alexa-0.1.1 (c (n "alexa") (v "0.1.1") (d (list (d (n "bodyparser") (r "^0.4.1") (d #t) (k 0)) (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "iron") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_codegen") (r "^0.8") (d #t) (k 1)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)))) (h "084pn3iv7mcnjk00sxjv97jsmmkn6pkw1q24hkp0c7mg8g51b71x")))

(define-public crate-alexa-0.1.2 (c (n "alexa") (v "0.1.2") (d (list (d (n "bodyparser") (r "^0.4.1") (d #t) (k 0)) (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "iron") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_codegen") (r "^0.8") (d #t) (k 1)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)))) (h "11bnz67hdx0h3wzx25m5q5rqg7jbbr6lgzrh083m9av7qx7d4yak")))

