(define-module (crates-io al ex alexcrichton-cranelift-preopt) #:use-module (crates-io))

(define-public crate-alexcrichton-cranelift-preopt-0.52.0 (c (n "alexcrichton-cranelift-preopt") (v "0.52.0") (d (list (d (n "alexcrichton-cranelift-codegen") (r "^0.52.0") (k 0)) (d (n "alexcrichton-cranelift-entity") (r "^0.52.0") (d #t) (k 0)))) (h "0wq4i5rm6nr5fjkmlhnghz9wkhqc8714raiifqk26lascjsh5m6c") (f (quote (("std" "alexcrichton-cranelift-codegen/std") ("default" "std") ("core" "alexcrichton-cranelift-codegen/core"))))))

(define-public crate-alexcrichton-cranelift-preopt-0.53.0 (c (n "alexcrichton-cranelift-preopt") (v "0.53.0") (d (list (d (n "alexcrichton-cranelift-codegen") (r "^0.53.0") (k 0)) (d (n "alexcrichton-cranelift-entity") (r "^0.53.0") (d #t) (k 0)))) (h "02a78fqqcbrqgyswsh69ykvpixza8b6s178n0v4rzdnz218y057m") (f (quote (("std" "alexcrichton-cranelift-codegen/std") ("default" "std") ("core" "alexcrichton-cranelift-codegen/core"))))))

