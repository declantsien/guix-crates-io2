(define-module (crates-io al ex alexflipnote) #:use-module (crates-io))

(define-public crate-alexflipnote-0.1.0 (c (n "alexflipnote") (v "0.1.0") (d (list (d (n "bytes") (r "^1.0.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.0") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.123") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)))) (h "0vssz0ixypk7fvgimyz36yy7n5njlf9bq826x2whn9mlzf9xdx61")))

