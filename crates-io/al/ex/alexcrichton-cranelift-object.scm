(define-module (crates-io al ex alexcrichton-cranelift-object) #:use-module (crates-io))

(define-public crate-alexcrichton-cranelift-object-0.52.0 (c (n "alexcrichton-cranelift-object") (v "0.52.0") (d (list (d (n "alexcrichton-cranelift-codegen") (r "^0.52.0") (f (quote ("std"))) (k 0)) (d (n "alexcrichton-cranelift-module") (r "^0.52.0") (d #t) (k 0)) (d (n "object") (r "^0.17") (f (quote ("write"))) (k 0)) (d (n "target-lexicon") (r "^0.10") (d #t) (k 0)))) (h "0bv76m7slav9zn55dlm161ydpqg0gd1gg28kmd5cz242y9xw0il6")))

(define-public crate-alexcrichton-cranelift-object-0.53.0 (c (n "alexcrichton-cranelift-object") (v "0.53.0") (d (list (d (n "alexcrichton-cranelift-codegen") (r "^0.53.0") (f (quote ("std"))) (k 0)) (d (n "alexcrichton-cranelift-module") (r "^0.53.0") (d #t) (k 0)) (d (n "object") (r "^0.17") (f (quote ("write"))) (k 0)) (d (n "target-lexicon") (r "^0.10") (d #t) (k 0)))) (h "1x7hw10ahizkrc735jjz5j2qrh8qfalbvnwx28sdr669rdixqzss")))

