(define-module (crates-io al oe aloe) #:use-module (crates-io))

(define-public crate-aloe-0.1.0 (c (n "aloe") (v "0.1.0") (h "19x616kfd8anzcnrq9hmlkajhz0kazrb7hcl36jr692g7j0gga2i") (y #t)))

(define-public crate-aloe-0.0.0 (c (n "aloe") (v "0.0.0") (d (list (d (n "glow") (r "^0.11.2") (d #t) (k 0)) (d (n "glutin") (r "^0.24") (d #t) (t "cfg(not(any(target_arch = \"wasm32\")))") (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("HtmlCanvasElement" "WebGl2RenderingContext" "Window"))) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "1q5pbg1whdjn1rwbr2ia0cb9kdg2f90k6gv50jq54qsa71bj07ig") (y #t)))

