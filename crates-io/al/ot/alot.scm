(define-module (crates-io al ot alot) #:use-module (crates-io))

(define-public crate-alot-0.1.0 (c (n "alot") (v "0.1.0") (h "103qxcdqvrjzw7ai5c3004zblj0ljm8ylyqk2j7j42p2wi99n455") (r "1.65.0")))

(define-public crate-alot-0.2.0 (c (n "alot") (v "0.2.0") (h "0jkbd5jys8wp0gql5pkikjvjzqgbka8an432vf9my5ax8d6by4a5") (r "1.65.0")))

(define-public crate-alot-0.3.0 (c (n "alot") (v "0.3.0") (h "0sa1afxl50l7880fdb4f3fglcnsd7x2cnmfbzxqx7iiaxvxn640g") (r "1.65.0")))

(define-public crate-alot-0.3.1 (c (n "alot") (v "0.3.1") (h "0aqfn3zjvghwgc6ypz7diw7cmnhisymdpv1d9qay98vk9clgqwmh") (r "1.65.0")))

