(define-module (crates-io al ox alox-48-derive) #:use-module (crates-io))

(define-public crate-alox-48-derive-0.1.0 (c (n "alox-48-derive") (v "0.1.0") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "itertools") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1fg4p8d7sgw73hfi51y0ylhh1wx4jcazxrddj4imc8i2cvh58ryf")))

