(define-module (crates-io al fr alfred-markdown-searcher) #:use-module (crates-io))

(define-public crate-alfred-markdown-searcher-0.1.0 (c (n "alfred-markdown-searcher") (v "0.1.0") (d (list (d (n "alfred") (r "^4.0.2") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "1xxgyr7ia35n0ac39kx5x7scx4w7vsqqrk54ah3c6fa9akvwqmis")))

