(define-module (crates-io al fr alfred-workflow) #:use-module (crates-io))

(define-public crate-alfred-workflow-1.0.0 (c (n "alfred-workflow") (v "1.0.0") (d (list (d (n "alfred") (r "^4.0.1") (d #t) (k 0)) (d (n "dirs") (r "^1.0.4") (d #t) (k 0)) (d (n "failure") (r "^0.1.4") (d #t) (k 0)) (d (n "rusqlite") (r "^0.16.0") (f (quote ("bundled"))) (d #t) (k 0)))) (h "1z8b4wmlpyw91yw18shd51dzkd4pfyyw7hd8j3ld0pqaran59cf0")))

(define-public crate-alfred-workflow-1.0.1 (c (n "alfred-workflow") (v "1.0.1") (d (list (d (n "alfred") (r "^4.0.1") (d #t) (k 0)) (d (n "dirs") (r "^1.0.4") (d #t) (k 0)) (d (n "failure") (r "^0.1.4") (d #t) (k 0)) (d (n "rusqlite") (r "^0.16.0") (f (quote ("bundled"))) (d #t) (k 0)))) (h "1a2w24dlscidhvqscvp1ghp3y0kd9rladsv5kdzhpgibxlsgdzp2")))

(define-public crate-alfred-workflow-1.1.0 (c (n "alfred-workflow") (v "1.1.0") (d (list (d (n "alfred") (r "^4.0.1") (d #t) (k 0)) (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "rusqlite") (r "^0.20.0") (f (quote ("chrono" "bundled"))) (d #t) (k 0)))) (h "1brdifpfgwa6y3ziyqw14qldhfv77xj4c18qmi1zrykk6vxdkgnj")))

