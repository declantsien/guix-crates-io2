(define-module (crates-io al fr alfred) #:use-module (crates-io))

(define-public crate-alfred-0.0.1 (c (n "alfred") (v "0.0.1") (h "01lr7z7iqxjk2k9h0343zpgfcb30s03nw2y2s6psqpdpk4r7gmhm")))

(define-public crate-alfred-0.0.2 (c (n "alfred") (v "0.0.2") (h "03pzhjyja5wisiy75x845qr8pwca4yw5f9kh6axzq8082gg5z99r")))

(define-public crate-alfred-0.0.3 (c (n "alfred") (v "0.0.3") (h "1qvijnipnky5j42519f9bvf8098npdm6pfbpwf8larccag3dfncy")))

(define-public crate-alfred-0.1.0 (c (n "alfred") (v "0.1.0") (h "1g315vsxa3c2yj8npsa5w5sy57769p8nhccxxyzmmiq8svpjb2fw")))

(define-public crate-alfred-0.1.1 (c (n "alfred") (v "0.1.1") (h "1d658gfh8b5wb53ag5if1gdxr0ljwag0dbkk6grlyal4qjmmzj0b")))

(define-public crate-alfred-0.2.0 (c (n "alfred") (v "0.2.0") (h "1vdilg6l4jqsh9a9qwqkicvyhc7hrkdinw49szvxa4jak5nmch8v")))

(define-public crate-alfred-0.2.1 (c (n "alfred") (v "0.2.1") (h "019xxsipv08wi4s5xhan0716w4xh560cipbxq7m0dpmwlxvjaryb")))

(define-public crate-alfred-0.2.2 (c (n "alfred") (v "0.2.2") (h "1x4vx8kkwba7ddizrq5vnh3qvnhkvz8rd367ii4fabiixfm8rjwq")))

(define-public crate-alfred-0.3.0 (c (n "alfred") (v "0.3.0") (h "02lgsm2w7d688bp1bck0h88nsaphcgnl2w4zpfrxxcjh20kw5pfh")))

(define-public crate-alfred-0.3.1 (c (n "alfred") (v "0.3.1") (h "0kab2p380z08a9g8gd9p4fgg2qbmdgf40qk6pk5fknmk5mhgpxl2")))

(define-public crate-alfred-1.0.0 (c (n "alfred") (v "1.0.0") (h "031sx67sn6s2lj9zvxpb5xgbp1qnmn7bdy6gcms0qy7i9yq9yajp")))

(define-public crate-alfred-1.0.1 (c (n "alfred") (v "1.0.1") (h "10ffdakshnmblhshwy57073zq5q8nbshi4x1is6vj7ni2vfalj1z")))

(define-public crate-alfred-2.0.0 (c (n "alfred") (v "2.0.0") (d (list (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0r9pcx4i3jvsazfzrgd2r494wndmmjrvx713rd2symhfd5gj7akl")))

(define-public crate-alfred-2.0.1 (c (n "alfred") (v "2.0.1") (d (list (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1cmp799zhxv87c4jar1mqlz9270djfmhxwgf801b42jkvg5a4775")))

(define-public crate-alfred-3.0.0 (c (n "alfred") (v "3.0.0") (d (list (d (n "serde_json") (r "^0.9") (d #t) (k 0)))) (h "0ir8qc6vp5yx657fbwjmcp9zx8gv7bxr76fp49clrychq3vvb4ax")))

(define-public crate-alfred-3.0.1 (c (n "alfred") (v "3.0.1") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1x25ms6yz2l1ykfzpgqkigm2hqzf80x1ayw6313vif0v0r8mq6ly")))

(define-public crate-alfred-3.0.2 (c (n "alfred") (v "3.0.2") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0vyhscgrdj4pgs22365wswpac3r9jxh3i26mc0p39bsx5nkj1sq5")))

(define-public crate-alfred-3.0.3 (c (n "alfred") (v "3.0.3") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1fhgvx371vbdnp66qdgy2amw6qhqfbnpmnsx3qirwmn113yc17ib")))

(define-public crate-alfred-4.0.0 (c (n "alfred") (v "4.0.0") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1ygzygsj5p82f4rmjgch7c7fj7ygydv42nqdy1is533a9w2ki535")))

(define-public crate-alfred-4.0.1 (c (n "alfred") (v "4.0.1") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1ilz565gdhah7ccamc3na7jsndjga83z72pk2v652x4xa3sq5bzn")))

(define-public crate-alfred-4.0.2 (c (n "alfred") (v "4.0.2") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "033hacfaflgpww1535z22jf51xf6hk44s1j1cs667ci47n3hzrsm")))

