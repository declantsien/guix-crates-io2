(define-module (crates-io al fr alfred-crates) #:use-module (crates-io))

(define-public crate-alfred-crates-0.1.0 (c (n "alfred-crates") (v "0.1.0") (d (list (d (n "crates-io") (r "^0.5.0") (d #t) (k 0)))) (h "01qzz3xr0yils4h0rpkgbdcdsddhxkm0snwpxgmlzbh10nyka07q")))

(define-public crate-alfred-crates-1.0.0 (c (n "alfred-crates") (v "1.0.0") (d (list (d (n "crates-io") (r "^0.5.0") (d #t) (k 0)))) (h "1snxsrwqjb086lnrs8vyvznard4zakmkmijbq6afdsm98qs5yack")))

(define-public crate-alfred-crates-1.0.1 (c (n "alfred-crates") (v "1.0.1") (d (list (d (n "crates-io") (r "^0.5.0") (d #t) (k 0)) (d (n "xml-rs") (r "^0.3.5") (d #t) (k 0)))) (h "0q082dalhvnq3gkmhg3l58qjzhlydx6yijhm8vsj6yjvjzf5lyjl")))

(define-public crate-alfred-crates-1.0.2 (c (n "alfred-crates") (v "1.0.2") (d (list (d (n "alfred") (r "^4.0.1") (d #t) (k 0)) (d (n "clap") (r "^2.20.0") (k 0)) (d (n "crates-io") (r "^0.24") (d #t) (k 0)) (d (n "curl") (r "^0.4.22") (d #t) (k 0)))) (h "05sj4r4lvwmpsxvqpikm2wwi55nijxbgh1p8wlirynvilccc4kbn")))

