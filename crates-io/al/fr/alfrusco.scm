(define-module (crates-io al fr alfrusco) #:use-module (crates-io))

(define-public crate-alfrusco-0.1.0 (c (n "alfrusco") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.80") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)) (d (n "fuzzy-matcher") (r "^0.3.7") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0s94bcxg11h1rkad4r89jaan0q4qmyyazhmyf6jas9zvr7g0zlhc")))

