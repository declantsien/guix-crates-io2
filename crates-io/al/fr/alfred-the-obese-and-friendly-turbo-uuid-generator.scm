(define-module (crates-io al fr alfred-the-obese-and-friendly-turbo-uuid-generator) #:use-module (crates-io))

(define-public crate-alfred-the-obese-and-friendly-turbo-uuid-generator-0.1.0 (c (n "alfred-the-obese-and-friendly-turbo-uuid-generator") (v "0.1.0") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0qjb0d8zdyqkr500xlri65lzqx8fvwpm3pby53p3368l0i0pbg26")))

(define-public crate-alfred-the-obese-and-friendly-turbo-uuid-generator-0.1.1 (c (n "alfred-the-obese-and-friendly-turbo-uuid-generator") (v "0.1.1") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "10k6bby9jam3qb3z4v03mizcdd54yd3z3x4kvfh7zghkjnrzxpyk")))

