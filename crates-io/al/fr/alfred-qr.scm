(define-module (crates-io al fr alfred-qr) #:use-module (crates-io))

(define-public crate-alfred-qr-0.1.0 (c (n "alfred-qr") (v "0.1.0") (d (list (d (n "clap") (r "^3.2.17") (d #t) (k 0)) (d (n "image") (r "^0.24.3") (d #t) (k 0)) (d (n "rqrr") (r "^0.5.1") (d #t) (k 0)))) (h "19y6qfk926qmb9kblzwd1vzmwvqcs6mpgqmgl0ybv30xb58zbfgc")))

