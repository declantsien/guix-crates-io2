(define-module (crates-io al fr alfred-thesaurus) #:use-module (crates-io))

(define-public crate-alfred-thesaurus-0.1.0 (c (n "alfred-thesaurus") (v "0.1.0") (d (list (d (n "alfred") (r "^4.0.2") (d #t) (k 0)) (d (n "clap") (r "^3.2.17") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)))) (h "1779009rifxjp7ripn1a7v2vxdwkqshxby6m7qsynq0qs222y5i2")))

