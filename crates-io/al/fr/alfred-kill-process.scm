(define-module (crates-io al fr alfred-kill-process) #:use-module (crates-io))

(define-public crate-alfred-kill-process-0.1.0 (c (n "alfred-kill-process") (v "0.1.0") (d (list (d (n "alfred") (r "^4.0.2") (d #t) (k 0)) (d (n "clap") (r "^3.2.17") (d #t) (k 0)) (d (n "sysinfo") (r "^0.25.2") (d #t) (k 0)))) (h "1d66zsgx81km41smdvaaisp4yv5jf1qwrysdhbs5d4gmyzm8h17m")))

