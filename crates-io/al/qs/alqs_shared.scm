(define-module (crates-io al qs alqs_shared) #:use-module (crates-io))

(define-public crate-alqs_shared-0.1.0 (c (n "alqs_shared") (v "0.1.0") (d (list (d (n "prost") (r "^0.12.3") (d #t) (k 0)) (d (n "tokio") (r "^1.35.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)) (d (n "tonic") (r "^0.10.2") (d #t) (k 0)) (d (n "tonic-build") (r "^0.10") (d #t) (k 1)) (d (n "tonic-health") (r "^0.10.2") (d #t) (k 0)))) (h "1nc67prs07g5nzv141hjhqncx9ik2ms415mmnfffs46qka6b7l10")))

