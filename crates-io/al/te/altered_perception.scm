(define-module (crates-io al te altered_perception) #:use-module (crates-io))

(define-public crate-altered_perception-0.1.0 (c (n "altered_perception") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rgb") (r "^0.8.34") (d #t) (k 0)) (d (n "rayon") (r "^1.6.1") (d #t) (k 2)))) (h "09b03rxcvm4vp0gb9dh6xbihaw74kayna17fa6810m42kddqbc9l")))

(define-public crate-altered_perception-0.2.0 (c (n "altered_perception") (v "0.2.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rgb") (r "^0.8.34") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.152") (d #t) (k 0)) (d (n "rayon") (r "^1.6.1") (d #t) (k 2)))) (h "1626cvcblkvvd0127bpdvkg94qllrwim25mk908ijvavrya9gpxl")))

(define-public crate-altered_perception-0.3.0 (c (n "altered_perception") (v "0.3.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rgb") (r "^0.8.34") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "rayon") (r "^1.6.1") (d #t) (k 2)))) (h "046nvr4r9scp1jhakww85nyfzz1rybc9xz11y5j11cf69ing7hrw")))

(define-public crate-altered_perception-0.3.1 (c (n "altered_perception") (v "0.3.1") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rgb") (r "^0.8.34") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "rayon") (r "^1.6.1") (d #t) (k 2)))) (h "19fnn0p40p11wq16bjdzj1gasxlnnvgbb3pb9jqsnb4v12km5n78") (y #t)))

(define-public crate-altered_perception-0.3.2 (c (n "altered_perception") (v "0.3.2") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rgb") (r "^0.8.34") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (o #t) (k 0)) (d (n "rayon") (r "^1.6.1") (d #t) (k 2)))) (h "1jp6j15b1lm6xp2nljw32kyxy2nzdqmawdb28l24xngx26jpl5cc")))

(define-public crate-altered_perception-0.4.0 (c (n "altered_perception") (v "0.4.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rgb") (r "^0.8.34") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (o #t) (k 0)) (d (n "rayon") (r "^1.6.1") (d #t) (k 2)))) (h "16mwpsyv3dj0597lalk2j3iciw4awhqyxav2qd36an0srmdr2fbi")))

(define-public crate-altered_perception-0.5.0 (c (n "altered_perception") (v "0.5.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rgb") (r "^0.8.34") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (o #t) (k 0)) (d (n "rayon") (r "^1.6.1") (d #t) (k 2)))) (h "16ihsn5dsxfjmvarf0w5awgcfw5j8pvdwmw8s469f3k5m3cbl3ja")))

