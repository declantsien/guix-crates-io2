(define-module (crates-io al te alter-ego) #:use-module (crates-io))

(define-public crate-alter-ego-0.1.0 (c (n "alter-ego") (v "0.1.0") (d (list (d (n "posix-acl") (r "^0.2.0") (d #t) (k 0)) (d (n "simple-error") (r "^0.2.1") (d #t) (k 0)) (d (n "users") (r "^0.9.1") (d #t) (k 0)))) (h "1lzp9844fz6f62n8wzyhpn7792zq7pwrbfj0fnvcryxj9mlcmckk")))

(define-public crate-alter-ego-0.2.0 (c (n "alter-ego") (v "0.2.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (f (quote ("std"))) (d #t) (k 0)) (d (n "posix-acl") (r "^0.2.0") (d #t) (k 0)) (d (n "simple-error") (r "^0.2.1") (d #t) (k 0)) (d (n "users") (r "^0.9.1") (d #t) (k 0)))) (h "0izirrgqpbkl7hqlqxf8jjv7c83082j6gfbxvjqgcnpgc89qzrii")))

(define-public crate-alter-ego-0.3.0 (c (n "alter-ego") (v "0.3.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (f (quote ("std"))) (d #t) (k 0)) (d (n "posix-acl") (r "^0.3.0") (d #t) (k 0)) (d (n "simple-error") (r "^0.2.1") (d #t) (k 0)) (d (n "users") (r "^0.9.1") (d #t) (k 0)) (d (n "which") (r "^3.1.0") (k 0)))) (h "1v5dnyj384bvnrrf87012i89nzfb22j7rryv7qvr6f7gymck8hy8")))

