(define-module (crates-io al te alterable_logger) #:use-module (crates-io))

(define-public crate-alterable_logger-1.0.0 (c (n "alterable_logger") (v "1.0.0") (d (list (d (n "arc-swap") (r "^1.6.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (f (quote ("std"))) (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)))) (h "0js0rvblyf8an4ww5g7c5ja5zzhabqnn2m0ms647vmg8l20jqjli")))

