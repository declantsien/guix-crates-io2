(define-module (crates-io al te alternate-future) #:use-module (crates-io))

(define-public crate-alternate-future-0.1.0 (c (n "alternate-future") (v "0.1.0") (h "0zvkzf34c5gcyd7sw4xy2m7ydvj0pz5pp1qj5ja3zxxb3a2j68aw")))

(define-public crate-alternate-future-0.1.1 (c (n "alternate-future") (v "0.1.1") (h "0y19xrkawwa3d8ic9whfqqk1kx5c27wacr0crcgmpk31z6s1rl0n")))

(define-public crate-alternate-future-0.1.2 (c (n "alternate-future") (v "0.1.2") (d (list (d (n "lazy_static") (r "^0.1") (d #t) (k 0)) (d (n "num_cpus") (r "^0.2") (d #t) (k 0)) (d (n "spin") (r "^0.3") (d #t) (k 0)))) (h "1jcakv3ban2r7c049wq56dnxnsl20q5z9657a4p75yxk1zb4iinh")))

(define-public crate-alternate-future-0.1.3 (c (n "alternate-future") (v "0.1.3") (d (list (d (n "lazy_static") (r "^0.1") (d #t) (k 0)) (d (n "num_cpus") (r "^0.2") (d #t) (k 0)) (d (n "spin") (r "^0.3") (d #t) (k 0)))) (h "0aqrshgbkpwg0lhklk2rzvnfcdsmxabs52139zwcaik3f6blm2fn")))

(define-public crate-alternate-future-0.1.4 (c (n "alternate-future") (v "0.1.4") (d (list (d (n "lazy_static") (r "^0.1") (d #t) (k 0)) (d (n "num_cpus") (r "^0.2") (d #t) (k 0)) (d (n "spin") (r "^0.3") (d #t) (k 0)))) (h "0hy20zwgw25jg13kw1whvamz27x29f1dlfa1gflhs7rjkyg4gr5b")))

