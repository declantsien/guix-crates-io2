(define-module (crates-io al te alternating-iter) #:use-module (crates-io))

(define-public crate-alternating-iter-0.1.0 (c (n "alternating-iter") (v "0.1.0") (h "1px8zj3d4p5571sjpjpyqd32nsm7jbcs62217f4ibv3i9s14xl84") (y #t)))

(define-public crate-alternating-iter-0.1.1 (c (n "alternating-iter") (v "0.1.1") (h "0srv7jcgrbn7fkixdxxxhh7pyqg8iki24vlgqd1452yc4p6xikx4") (y #t)))

(define-public crate-alternating-iter-0.1.2 (c (n "alternating-iter") (v "0.1.2") (h "1x0zzgxfwby38bg1vkp8r4yqxx6yf2i12140qf609dm51prcmawv") (y #t)))

(define-public crate-alternating-iter-0.2.0 (c (n "alternating-iter") (v "0.2.0") (h "1kbs6dk03ldnwrbiy1zyhzpczyl32gfmk0klscxjj6qdyjpyw7zn")))

