(define-module (crates-io al ep aleph-syntax-tree) #:use-module (crates-io))

(define-public crate-aleph-syntax-tree-0.1.0 (c (n "aleph-syntax-tree") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "strum") (r "^0.24") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24") (d #t) (k 0)))) (h "16wg99lwv7jmzlh91agsfyfykdwp5lanccv9ps6f4rbpm18916h9")))

