(define-module (crates-io al ep aleparser) #:use-module (crates-io))

(define-public crate-aleparser-0.1.1 (c (n "aleparser") (v "0.1.1") (d (list (d (n "aleph-syntax-tree") (r "^0.1.0") (d #t) (k 0)) (d (n "lalrpop") (r "^0.20") (f (quote ("lexer"))) (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.20") (f (quote ("lexer"))) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "12k4y4adjiqa0s2wlvahzfiv68q62qy09i9sp23j3mfcrw3y0h9p")))

