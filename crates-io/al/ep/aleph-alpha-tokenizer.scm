(define-module (crates-io al ep aleph-alpha-tokenizer) #:use-module (crates-io))

(define-public crate-aleph-alpha-tokenizer-0.1.0 (c (n "aleph-alpha-tokenizer") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3.1") (d #t) (k 2)) (d (n "fst") (r "^0.4.1") (d #t) (k 0)) (d (n "tokenizers") (r "^0.9.0") (o #t) (d #t) (k 0)))) (h "0mf0w8r1jqdb5jrc00h22wf3xhq9cyn8fgp4w87z1x2qj8rk7y7q") (f (quote (("huggingface" "tokenizers") ("default"))))))

(define-public crate-aleph-alpha-tokenizer-0.2.0 (c (n "aleph-alpha-tokenizer") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3.1") (d #t) (k 2)) (d (n "fst") (r "^0.4.3") (d #t) (k 0)) (d (n "tokenizers") (r "^0.9.0") (o #t) (d #t) (k 0)))) (h "01q874da4w9rwkmqvzha12zi4qi811r2blnb028880hyky9z60bv") (f (quote (("huggingface" "tokenizers") ("default"))))))

(define-public crate-aleph-alpha-tokenizer-0.3.0 (c (n "aleph-alpha-tokenizer") (v "0.3.0") (d (list (d (n "criterion") (r "^0.3.1") (d #t) (k 2)) (d (n "fst") (r "^0.4.3") (d #t) (k 0)) (d (n "tokenizers") (r "^0.10.1") (o #t) (d #t) (k 0)))) (h "0gvdadmskxxp6b9kfmkcir3p18i7h4pls8kcpps1hd9d63rh23b5") (f (quote (("huggingface" "tokenizers") ("default"))))))

(define-public crate-aleph-alpha-tokenizer-0.3.1 (c (n "aleph-alpha-tokenizer") (v "0.3.1") (d (list (d (n "criterion") (r "^0.3.1") (d #t) (k 2)) (d (n "fst") (r "^0.4.3") (d #t) (k 0)) (d (n "tokenizers") (r "^0.10.1") (o #t) (d #t) (k 0)))) (h "11a9c97181k3c9bs997qrsbjfvj5i0w4g5fcfazg0mlnfmq4yrs4") (f (quote (("huggingface" "tokenizers") ("default"))))))

