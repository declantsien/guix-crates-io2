(define-module (crates-io al ep aleph-solana-contract) #:use-module (crates-io))

(define-public crate-aleph-solana-contract-0.1.0 (c (n "aleph-solana-contract") (v "0.1.0") (d (list (d (n "anchor-lang") (r "^0.27.0") (d #t) (k 0)))) (h "0bm52h14w6f0i60rc362ia5m68zk9zl4480sxh22l7kx6kyjg49h") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-aleph-solana-contract-0.1.1 (c (n "aleph-solana-contract") (v "0.1.1") (d (list (d (n "anchor-lang") (r "^0.28.0") (d #t) (k 0)))) (h "1amz84cc2p8vmxn73lbn0kwarbgj4v1gns16dcj7yy5j6ppykwss") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

