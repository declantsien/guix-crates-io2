(define-module (crates-io al ep aleph-bft-types) #:use-module (crates-io))

(define-public crate-aleph-bft-types-0.2.1 (c (n "aleph-bft-types") (v "0.2.1") (d (list (d (n "aleph-bft-crypto") (r "^0.2") (d #t) (k 0)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "codec") (r "^2") (f (quote ("derive"))) (k 0) (p "parity-scale-codec")) (d (n "futures") (r "^0.3") (d #t) (k 0)))) (h "0dsk813hxji84kh9mxxc6kk7hlfdg5lfvksgpkvpqm67y9872f3s")))

(define-public crate-aleph-bft-types-0.3.0 (c (n "aleph-bft-types") (v "0.3.0") (d (list (d (n "aleph-bft-crypto") (r "^0.3") (d #t) (k 0)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "codec") (r "^3.0") (f (quote ("derive"))) (k 0) (p "parity-scale-codec")) (d (n "futures") (r "^0.3") (d #t) (k 0)))) (h "1dqvm0w4bnzf5y8fnw5p4645lpbi4x01dh7rb052aj574dlil80b")))

(define-public crate-aleph-bft-types-0.4.0 (c (n "aleph-bft-types") (v "0.4.0") (d (list (d (n "aleph-bft-crypto") (r "^0.4") (d #t) (k 0)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "codec") (r "^3.0") (f (quote ("derive"))) (k 0) (p "parity-scale-codec")) (d (n "futures") (r "^0.3") (d #t) (k 0)))) (h "0gm9p751g8r7qqphzrr2kwlkpp1mmn49dvxcn53b8msb41ig3mzr")))

(define-public crate-aleph-bft-types-0.5.0 (c (n "aleph-bft-types") (v "0.5.0") (d (list (d (n "aleph-bft-crypto") (r "^0.4") (d #t) (k 0)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "codec") (r "^3.0") (f (quote ("derive"))) (k 0) (p "parity-scale-codec")) (d (n "futures") (r "^0.3") (d #t) (k 0)))) (h "0n87v75yhdrahmdhgpcsg62d7nixlvywii352qs9ghfj7vhxnp2b")))

(define-public crate-aleph-bft-types-0.6.0 (c (n "aleph-bft-types") (v "0.6.0") (d (list (d (n "aleph-bft-crypto") (r "^0.4") (d #t) (k 0)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "codec") (r "^3.0") (f (quote ("derive"))) (k 0) (p "parity-scale-codec")) (d (n "futures") (r "^0.3") (d #t) (k 0)))) (h "106mgn8sz00ma5c84njq01liyf32rw069czi5q87cg5n4s2mp7xv")))

(define-public crate-aleph-bft-types-0.7.0 (c (n "aleph-bft-types") (v "0.7.0") (d (list (d (n "aleph-bft-crypto") (r "^0.5") (d #t) (k 0)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "codec") (r "^3.0") (f (quote ("derive"))) (k 0) (p "parity-scale-codec")) (d (n "futures") (r "^0.3") (d #t) (k 0)))) (h "0996sdwgykffhmab297ja6rrkd86l7xgs1lp2fq27zhv09ll0a7c")))

(define-public crate-aleph-bft-types-0.7.1 (c (n "aleph-bft-types") (v "0.7.1") (d (list (d (n "aleph-bft-crypto") (r "^0.5") (d #t) (k 0)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "codec") (r "^3.0") (f (quote ("derive"))) (k 0) (p "parity-scale-codec")) (d (n "futures") (r "^0.3") (d #t) (k 0)))) (h "1nnkljcq3gqd84kp288hmk2hy7l4nz783bbc77im6fr6cy8c18z0")))

(define-public crate-aleph-bft-types-0.7.2 (c (n "aleph-bft-types") (v "0.7.2") (d (list (d (n "aleph-bft-crypto") (r "^0.5") (d #t) (k 0)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "codec") (r "^3.0") (f (quote ("derive"))) (k 0) (p "parity-scale-codec")) (d (n "futures") (r "^0.3") (d #t) (k 0)))) (h "015rpz3a2jb53p184hdkar3ylhk488b87vv2vk3ww2b7g9f9kh0l")))

(define-public crate-aleph-bft-types-0.8.0 (c (n "aleph-bft-types") (v "0.8.0") (d (list (d (n "aleph-bft-crypto") (r "^0.5") (d #t) (k 0)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "codec") (r "^3.0") (f (quote ("derive"))) (k 0) (p "parity-scale-codec")) (d (n "futures") (r "^0.3") (d #t) (k 0)))) (h "0xamy79irfr5cfm8pjk27c9mmskg3dlqavkxmlms4ihx9lxk0q7n")))

(define-public crate-aleph-bft-types-0.8.1 (c (n "aleph-bft-types") (v "0.8.1") (d (list (d (n "aleph-bft-crypto") (r "^0.6") (d #t) (k 0)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "codec") (r "^3.0") (f (quote ("derive"))) (k 0) (p "parity-scale-codec")) (d (n "futures") (r "^0.3") (d #t) (k 0)))) (h "040mzhkxjfmmcj5ghzcklb92q0wvgy0cpqwggpg0lrxk7hxqjfab")))

(define-public crate-aleph-bft-types-0.10.0 (c (n "aleph-bft-types") (v "0.10.0") (d (list (d (n "aleph-bft-crypto") (r "^0.7") (d #t) (k 0)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "codec") (r "^3.0") (f (quote ("derive"))) (k 0) (p "parity-scale-codec")) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt" "rt-multi-thread" "time"))) (d #t) (k 2)))) (h "1z37rcj9m59ff758azi5k7qs8ra3m8p22ssj236315dg561qj7d0")))

(define-public crate-aleph-bft-types-0.11.0 (c (n "aleph-bft-types") (v "0.11.0") (d (list (d (n "aleph-bft-crypto") (r "^0.8") (d #t) (k 0)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "codec") (r "^3.0") (f (quote ("derive"))) (k 0) (p "parity-scale-codec")) (d (n "futures") (r "^0.3") (d #t) (k 0)))) (h "0l81szr10s3yxxw61m1sws0qy1ia1pb73zvgn3brzl4i4l87r85g")))

(define-public crate-aleph-bft-types-0.12.0 (c (n "aleph-bft-types") (v "0.12.0") (d (list (d (n "aleph-bft-crypto") (r "^0.9") (d #t) (k 0)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "codec") (r "^3.0") (f (quote ("derive"))) (k 0) (p "parity-scale-codec")) (d (n "futures") (r "^0.3") (d #t) (k 0)))) (h "14lzfyxf88frakkgwrf7ghk9kn69gbjmknkc73lwyv70hdrziyvs")))

(define-public crate-aleph-bft-types-0.13.0 (c (n "aleph-bft-types") (v "0.13.0") (d (list (d (n "aleph-bft-crypto") (r "^0.9") (d #t) (k 0)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "codec") (r "^3.0") (f (quote ("derive"))) (k 0) (p "parity-scale-codec")) (d (n "futures") (r "^0.3") (d #t) (k 0)))) (h "1fr8gvb6wjgrsgd51y2ka4qg5nxdz5qmw2y5nsi5s8c7ylcrvjib")))

