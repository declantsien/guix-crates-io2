(define-module (crates-io al ep aleph-zero-cargo-nono) #:use-module (crates-io))

(define-public crate-aleph-zero-cargo-nono-0.1.9 (c (n "aleph-zero-cargo-nono") (v "0.1.9") (d (list (d (n "cargo_metadata") (r "^0.8.2") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "console") (r "^0.7.7") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.1") (k 0)) (d (n "quote") (r "^1.0.1") (k 0)) (d (n "serde") (r "^1.0.99") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)) (d (n "syn") (r "^1.0.2") (f (quote ("full" "extra-traits" "parsing"))) (k 0)) (d (n "assert_cmd") (r "^0.11.1") (d #t) (k 2)))) (h "0k1p97znnq4ndq76za584dkdm1k02m6mbly8sjadzwm1y8cjcfdr") (f (quote (("proc_macro_spans") ("default"))))))

