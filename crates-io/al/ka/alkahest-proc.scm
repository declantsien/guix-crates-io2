(define-module (crates-io al ka alkahest-proc) #:use-module (crates-io))

(define-public crate-alkahest-proc-0.1.0 (c (n "alkahest-proc") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive" "printing"))) (d #t) (k 0)))) (h "0cvd2crkq7dax05p9hp465jynh7b7y92v468y5zxa4g2dfv2dskq")))

(define-public crate-alkahest-proc-0.1.1 (c (n "alkahest-proc") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive" "printing"))) (d #t) (k 0)))) (h "0m1i90svpsqdraya5a8a28l2z8jkq3rhrbl2bn13lwyqxarxawhm")))

(define-public crate-alkahest-proc-0.1.2 (c (n "alkahest-proc") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive" "printing"))) (d #t) (k 0)))) (h "0gwpxypxcrmdpzh458gzr2p3qvaayfiprvbingy3j5vd3wkn65qa")))

(define-public crate-alkahest-proc-0.1.3 (c (n "alkahest-proc") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive" "printing"))) (d #t) (k 0)))) (h "095z4y4z2fcic5v2pffpnp49i6xhsdpgrfy31h6iv5hjlrq7ibhk")))

(define-public crate-alkahest-proc-0.1.4 (c (n "alkahest-proc") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive" "printing"))) (d #t) (k 0)))) (h "1rz7b5m6cyxzfmrpswcxgy425vj8z3rx5ks8411v5v8nqcaxp1w0")))

(define-public crate-alkahest-proc-0.2.0-rc.1 (c (n "alkahest-proc") (v "0.2.0-rc.1") (d (list (d (n "proc-easy") (r "^0.2.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0iqx1aplj34cn5mvzf6hpyy55lg1993rnjxjcfr5lk17nbfyj8is")))

(define-public crate-alkahest-proc-0.2.0-rc.2 (c (n "alkahest-proc") (v "0.2.0-rc.2") (d (list (d (n "proc-easy") (r "^0.2.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0j7zqi638084w3xrrijsnl8k22mx22i12yrzs1rw513a9xwg9dvs")))

(define-public crate-alkahest-proc-0.2.0-rc.6 (c (n "alkahest-proc") (v "0.2.0-rc.6") (d (list (d (n "proc-easy") (r "^0.2.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "11db73g4h65lbwmjyprq3x4s22qydbpasxlnvgs7z5w0mcv2drh8")))

(define-public crate-alkahest-proc-0.2.0-rc.8 (c (n "alkahest-proc") (v "0.2.0-rc.8") (d (list (d (n "proc-easy") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1w17bl4kxbr5ai4zvqw3xk6j9zabikk29fwx02a6jiizvzg6rwp6")))

(define-public crate-alkahest-proc-0.2.0 (c (n "alkahest-proc") (v "0.2.0") (d (list (d (n "proc-easy") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1wjc9il4j9bgvwvbjr2gp09wh0p5z6c0c61b9cxmvinds2iqlwxh")))

(define-public crate-alkahest-proc-0.3.0 (c (n "alkahest-proc") (v "0.3.0") (d (list (d (n "proc-easy") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1x534rblml5si49jrxbn2l5p4ml1fzak69vdxzxmc4xy0h65pwdx")))

