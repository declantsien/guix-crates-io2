(define-module (crates-io al tc altcoin-bech32) #:use-module (crates-io))

(define-public crate-altcoin-bech32-0.11.0 (c (n "altcoin-bech32") (v "0.11.0") (d (list (d (n "bech32") (r "^0.7.1") (d #t) (k 0)))) (h "0kjakqln71hlar3k20fzsjfvkynz8dl7m9mc6rfjm66bhj2s6wr7") (f (quote (("strict"))))))

(define-public crate-altcoin-bech32-0.12.1 (c (n "altcoin-bech32") (v "0.12.1") (d (list (d (n "bech32") (r "^0.8.0") (d #t) (k 0)))) (h "0lk3482x3ibswjwz9laxfibfw4x7wldbbfdahf43psgd3xps6qgr") (f (quote (("strict"))))))

