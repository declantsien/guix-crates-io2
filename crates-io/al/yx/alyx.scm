(define-module (crates-io al yx alyx) #:use-module (crates-io))

(define-public crate-alyx-0.0.0 (c (n "alyx") (v "0.0.0") (h "1qkk2aqml6xm787031nc4j4apvrnxdglzi2cbim2cjq166lxfppc") (r "1.56")))

(define-public crate-alyx-0.0.1 (c (n "alyx") (v "0.0.1") (h "019dgxjzcadxh98sk8l7shjmhiyb7iywhpqizbc3gx2jw3wyb5mw") (r "1.56")))

(define-public crate-alyx-0.0.2 (c (n "alyx") (v "0.0.2") (h "063f4g66mdwn2fiqs7nvqih2a2wh0cg94j8msfv4saad9z85ldk0") (r "1.56")))

