(define-module (crates-io al t- alt-std) #:use-module (crates-io))

(define-public crate-alt-std-0.2.0 (c (n "alt-std") (v "0.2.0") (d (list (d (n "libc") (r "^0.2.69") (d #t) (k 0)))) (h "05i0j509amyh0n9i4jx31x1lwy50hcb07wjds568rmhx9wgisk5x")))

(define-public crate-alt-std-0.2.1 (c (n "alt-std") (v "0.2.1") (d (list (d (n "libc") (r "^0.2.69") (d #t) (k 0)))) (h "1abmlf35knp67wa6n1mryh5fa2wi10hw7scijkg6h86zl25lswa4")))

(define-public crate-alt-std-0.2.2 (c (n "alt-std") (v "0.2.2") (d (list (d (n "libc") (r "^0.2.71") (d #t) (k 0)))) (h "1vxacnnzvdxxjhc2zikz207fs8s7jjsvaark2zfhijqm8w8kvnnm")))

(define-public crate-alt-std-0.2.3 (c (n "alt-std") (v "0.2.3") (d (list (d (n "libc") (r "^0.2.71") (d #t) (k 0)))) (h "1jn02bx7xdxr32ynkpfkbr3a3p2i2xs287kb7sdf1wrhl5jv2hws")))

(define-public crate-alt-std-0.2.4 (c (n "alt-std") (v "0.2.4") (d (list (d (n "libc") (r "^0.2.71") (d #t) (k 0)))) (h "0z8nvq2mldk3wrp9h54gs54waw49kgxvs5j2qivlbldxf3b2d54k")))

(define-public crate-alt-std-0.2.5 (c (n "alt-std") (v "0.2.5") (d (list (d (n "libc") (r "^0.2.71") (d #t) (k 0)))) (h "08sdvzwd2xrayjpzwdv9ggwi9qyrhlykph22jhq5azfbc37737iy")))

(define-public crate-alt-std-0.2.6 (c (n "alt-std") (v "0.2.6") (d (list (d (n "libc") (r "^0.2.71") (d #t) (k 0)))) (h "1rl7mdqqzk9p398cb7586k4q4v6sq1x9rg8f6hx4gsicis14ayxs")))

(define-public crate-alt-std-0.2.7 (c (n "alt-std") (v "0.2.7") (d (list (d (n "libc") (r "^0.2.71") (d #t) (k 0)))) (h "12i3gddfkhca2yk5f2j0wsx96q9yv9c4zn054xaq3sxkqm6vx3l5")))

(define-public crate-alt-std-0.2.8 (c (n "alt-std") (v "0.2.8") (d (list (d (n "libc") (r "^0.2.71") (d #t) (k 0)))) (h "0qjb3fhggf19cf2bkqr95qncj1wkc6rszli8rvyzrkvy5lqkjmrf")))

(define-public crate-alt-std-0.2.9 (c (n "alt-std") (v "0.2.9") (d (list (d (n "libc") (r "^0.2.71") (d #t) (k 0)))) (h "0d2v51m9qmypx2c82ily12jiz8sn2gbbmirafvvmx0iygzlzizg4")))

