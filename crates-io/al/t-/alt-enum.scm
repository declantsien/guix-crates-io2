(define-module (crates-io al t- alt-enum) #:use-module (crates-io))

(define-public crate-alt-enum-0.1.0 (c (n "alt-enum") (v "0.1.0") (d (list (d (n "paste") (r "^1.0.8") (d #t) (k 0)) (d (n "value-enum") (r "^0.5.2") (o #t) (d #t) (k 0)))) (h "0g5p176hd9rh2zcknj0k81ym2ha0gmpz0l73ym0ma100ycqm6zxl") (f (quote (("value_enum" "value-enum")))) (y #t)))

(define-public crate-alt-enum-0.1.1 (c (n "alt-enum") (v "0.1.1") (d (list (d (n "paste") (r "^1.0.8") (d #t) (k 0)) (d (n "value-enum") (r "^0.5.2") (o #t) (d #t) (k 0)))) (h "0xz5p0k6qdyld4wpfnqdr85l765rrf1c7pz64wsjhrd09l5mivq9") (f (quote (("value_enum")))) (y #t)))

(define-public crate-alt-enum-0.1.2 (c (n "alt-enum") (v "0.1.2") (d (list (d (n "paste") (r "^1.0.8") (d #t) (k 0)) (d (n "value-enum") (r "^0.5.2") (o #t) (d #t) (k 0)))) (h "0z9pn17973cqcw26bb3269i3ckfm02zwl69qw3pkg6pp4racxjbs") (f (quote (("value_enum" "value-enum")))) (y #t)))

(define-public crate-alt-enum-0.1.3 (c (n "alt-enum") (v "0.1.3") (d (list (d (n "paste") (r "^1.0.8") (d #t) (k 0)) (d (n "value-enum") (r "^0.5.2") (o #t) (d #t) (k 0)))) (h "1pmw367aai7dmm4fy7bdgn9vw35fvb5d8z8nyjabigsvq8ai2azs") (f (quote (("value_enum" "value-enum")))) (y #t)))

(define-public crate-alt-enum-0.1.4 (c (n "alt-enum") (v "0.1.4") (d (list (d (n "paste") (r "^1.0.8") (d #t) (k 0)) (d (n "value-enum") (r "^0.6.0") (o #t) (d #t) (k 0)))) (h "1cck771fks617hjljg9i93jgsidkp8jfad6yy04s2jwginp88bbw") (f (quote (("value_enum" "value-enum")))) (y #t)))

