(define-module (crates-io al t- alt-failure) #:use-module (crates-io))

(define-public crate-alt-failure-0.1.5 (c (n "alt-failure") (v "0.1.5") (d (list (d (n "backtrace") (r "^0.3.3") (o #t) (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.5") (o #t) (d #t) (k 0)))) (h "0gfmq6h193pv9mm2g5dry4wvz342ar5mr8hwv24198fgx1wqbbkv") (f (quote (("std" "backtrace") ("derive" "failure_derive") ("default" "std" "derive"))))))

