(define-module (crates-io al t- alt-sam3x8e) #:use-module (crates-io))

(define-public crate-alt-sam3x8e-0.1.0 (c (n "alt-sam3x8e") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.0") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.10") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "1r7r3bhljz354vcxsxglk5m547cdi892x0mkxb1gd2hsyhxrhq76") (f (quote (("rt" "cortex-m-rt/device"))))))

