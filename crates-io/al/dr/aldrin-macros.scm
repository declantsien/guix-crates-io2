(define-module (crates-io al dr aldrin-macros) #:use-module (crates-io))

(define-public crate-aldrin-macros-0.1.0 (c (n "aldrin-macros") (v "0.1.0") (d (list (d (n "aldrin-codegen") (r "^0.1.0") (f (quote ("rust"))) (k 0)) (d (n "aldrin-parser") (r "^0.1.0") (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (k 0)) (d (n "proc-macro2") (r "^1.0.69") (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("parsing" "printing" "proc-macro"))) (k 0)))) (h "0if7r7m66jvg3pcbkhbb9vc8xvnxysj4zh4l0jk1vgybnid33qk5") (r "1.70.0")))

(define-public crate-aldrin-macros-0.2.0 (c (n "aldrin-macros") (v "0.2.0") (d (list (d (n "aldrin-codegen") (r "^0.2.0") (f (quote ("rust"))) (k 0)) (d (n "aldrin-parser") (r "^0.2.0") (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (k 0)) (d (n "proc-macro2") (r "^1.0.69") (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("parsing" "printing" "proc-macro"))) (k 0)))) (h "1glqww4nd50cirn6hkc4hw421r9vnnzlzda9va48y45ifbrsrp8c") (r "1.70.0")))

(define-public crate-aldrin-macros-0.3.0 (c (n "aldrin-macros") (v "0.3.0") (d (list (d (n "aldrin-codegen") (r "^0.3.0") (f (quote ("rust"))) (k 0)) (d (n "aldrin-parser") (r "^0.3.0") (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (k 0)) (d (n "proc-macro2") (r "^1.0.69") (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("parsing" "printing" "proc-macro"))) (k 0)))) (h "1jcq929xmncn014srcsmzs49d94qc92cxjbgzfwryxxrr4zy7isl") (r "1.70.0")))

(define-public crate-aldrin-macros-0.4.0 (c (n "aldrin-macros") (v "0.4.0") (d (list (d (n "aldrin-codegen") (r "^0.4.0") (f (quote ("rust"))) (k 0)) (d (n "aldrin-parser") (r "^0.4.0") (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (k 0)) (d (n "proc-macro2") (r "^1.0.69") (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("parsing" "printing" "proc-macro"))) (k 0)))) (h "07v48bkas0p9fs4gl59wmxn94vslbd9g5a0nhvkz5jipax9h1vib") (r "1.70.0")))

(define-public crate-aldrin-macros-0.5.0 (c (n "aldrin-macros") (v "0.5.0") (d (list (d (n "aldrin-codegen") (r "^0.5.0") (f (quote ("rust"))) (k 0)) (d (n "aldrin-parser") (r "^0.5.0") (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (k 0)) (d (n "proc-macro2") (r "^1.0.69") (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("parsing" "printing" "proc-macro"))) (k 0)))) (h "1dhpm8qbs0x9ngmavp5w69ld23bxfgrfybpra375g2a7alrghdd4") (r "1.70.0")))

