(define-module (crates-io al dr aldrin-codegen) #:use-module (crates-io))

(define-public crate-aldrin-codegen-0.0.0 (c (n "aldrin-codegen") (v "0.0.0") (h "01vz8ycm9ff17z5bg5qsp4qz5lfrxw01p0mdf13nb7bb0d9za3rf") (y #t)))

(define-public crate-aldrin-codegen-0.1.0 (c (n "aldrin-codegen") (v "0.1.0") (d (list (d (n "aldrin-parser") (r "^0.1.0") (k 0)) (d (n "diffy") (r "^0.3.0") (k 0)) (d (n "futures") (r "^0.3.29") (k 2)) (d (n "heck") (r "^0.4.1") (k 0)) (d (n "tokio") (r "^1.34.0") (f (quote ("macros" "rt"))) (d #t) (k 2)) (d (n "uuid") (r "^1.6.1") (d #t) (k 2)))) (h "0wczfnvs2wx2d3gqfr684w7qv02ml2if7ppmr2b6lx8qvh0m5pm4") (f (quote (("rust") ("default" "rust")))) (r "1.70.0")))

(define-public crate-aldrin-codegen-0.2.0 (c (n "aldrin-codegen") (v "0.2.0") (d (list (d (n "aldrin-parser") (r "^0.2.0") (k 0)) (d (n "diffy") (r "^0.3.0") (k 0)) (d (n "futures") (r "^0.3.29") (k 2)) (d (n "heck") (r "^0.4.1") (k 0)) (d (n "tokio") (r "^1.34.0") (f (quote ("macros" "rt"))) (d #t) (k 2)) (d (n "uuid") (r "^1.6.1") (d #t) (k 2)))) (h "1hhycm0h3jyyv9ybb78xslb8x2lji59f2vm004nny7vw5ahjfm0p") (f (quote (("rust") ("default" "rust")))) (r "1.70.0")))

(define-public crate-aldrin-codegen-0.2.1 (c (n "aldrin-codegen") (v "0.2.1") (d (list (d (n "aldrin-parser") (r "^0.2.0") (k 0)) (d (n "diffy") (r "^0.3.0") (k 0)) (d (n "futures") (r "^0.3.29") (k 2)) (d (n "heck") (r "^0.4.1") (k 0)) (d (n "tokio") (r "^1.34.0") (f (quote ("macros" "rt"))) (d #t) (k 2)) (d (n "uuid") (r "^1.6.1") (d #t) (k 2)))) (h "0gxmj5l8lbc64z28dxmpak6alv0ad1q43sj3x31rlriwqm25v2jz") (f (quote (("rust") ("default" "rust")))) (r "1.70.0")))

(define-public crate-aldrin-codegen-0.3.0 (c (n "aldrin-codegen") (v "0.3.0") (d (list (d (n "aldrin-parser") (r "^0.3.0") (k 0)) (d (n "diffy") (r "^0.3.0") (k 0)) (d (n "futures") (r "^0.3.29") (k 2)) (d (n "heck") (r "^0.4.1") (k 0)) (d (n "thiserror") (r "^1.0.50") (k 0)) (d (n "tokio") (r "^1.34.0") (f (quote ("macros" "rt"))) (d #t) (k 2)) (d (n "uuid") (r "^1.6.1") (d #t) (k 2)))) (h "13lhyvira623vj0kyq8srh41nxzwb2p6l1jbpcgm5mpp54sw7dc1") (f (quote (("rust") ("default" "rust")))) (r "1.70.0")))

(define-public crate-aldrin-codegen-0.4.0 (c (n "aldrin-codegen") (v "0.4.0") (d (list (d (n "aldrin-parser") (r "^0.4.0") (k 0)) (d (n "diffy") (r "^0.3.0") (k 0)) (d (n "futures") (r "^0.3.29") (k 2)) (d (n "heck") (r "^0.4.1") (k 0)) (d (n "thiserror") (r "^1.0.50") (k 0)) (d (n "tokio") (r "^1.34.0") (f (quote ("macros" "rt"))) (d #t) (k 2)) (d (n "uuid") (r "^1.6.1") (d #t) (k 2)))) (h "07nymy3824faq4r699aknppbdr0s6sqf1g4skapn6sgxvk7q3si9") (f (quote (("rust") ("default" "rust")))) (r "1.70.0")))

(define-public crate-aldrin-codegen-0.5.0 (c (n "aldrin-codegen") (v "0.5.0") (d (list (d (n "aldrin-parser") (r "^0.5.0") (k 0)) (d (n "diffy") (r "^0.3.0") (k 0)) (d (n "futures") (r "^0.3.29") (k 2)) (d (n "heck") (r "^0.4.1") (k 0)) (d (n "thiserror") (r "^1.0.50") (k 0)) (d (n "tokio") (r "^1.34.0") (f (quote ("macros" "rt"))) (d #t) (k 2)) (d (n "uuid") (r "^1.6.1") (d #t) (k 2)))) (h "1bka8d1zxkdhxrb2iwc2d08m6nqrv3azlnqa14g4g3bgj58mlng3") (f (quote (("rust") ("default" "rust")))) (r "1.70.0")))

