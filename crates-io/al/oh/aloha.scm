(define-module (crates-io al oh aloha) #:use-module (crates-io))

(define-public crate-aloha-0.1.0 (c (n "aloha") (v "0.1.0") (d (list (d (n "aead") (r "^0.5") (d #t) (k 0)) (d (n "bytes") (r "^1.0") (d #t) (k 0)) (d (n "generic-array") (r "^0.14") (d #t) (k 0)) (d (n "hex-literal") (r "^0.3") (d #t) (k 2)) (d (n "hkdf") (r "^0.12") (d #t) (k 0)) (d (n "hpke") (r "^0.10.0") (f (quote ("std"))) (d #t) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("std_rng"))) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("std_rng"))) (d #t) (k 2)) (d (n "rstest") (r "^0.16") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "x25519-dalek") (r "^2.0.0-pre.1") (d #t) (k 2)))) (h "07a9spg0m69dbakzp2wk4yg8qbfc3z41ah58xxbmkhw477br3jvq")))

