(define-module (crates-io al lo alloc-facade) #:use-module (crates-io))

(define-public crate-alloc-facade-0.1.0 (c (n "alloc-facade") (v "0.1.0") (h "1mvqsbjsq3yhlf28g0f51vzbpl46874rjb51971026zwi1qywqa2") (f (quote (("std") ("futures") ("default") ("alloc"))))))

(define-public crate-alloc-facade-0.2.0 (c (n "alloc-facade") (v "0.2.0") (h "07h893l17zzjgfidmpv5zhswy0758fi4rq6fxsl585kj91b9l02z") (f (quote (("std") ("futures") ("default") ("alloc"))))))

(define-public crate-alloc-facade-0.3.0 (c (n "alloc-facade") (v "0.3.0") (h "0zxx1w4y5hpyfmmnwppyk0lhxfgdbd2njqndsbyw1hp4av0br8ln") (f (quote (("std") ("futures") ("default") ("alloc"))))))

(define-public crate-alloc-facade-0.3.1 (c (n "alloc-facade") (v "0.3.1") (h "1927z7yp7fk8839nmads3hpbi0fxhygpfgy2219gpanv8ang9rnr") (f (quote (("std") ("futures") ("default") ("alloc"))))))

(define-public crate-alloc-facade-0.4.0 (c (n "alloc-facade") (v "0.4.0") (h "0adp75g9iml8zbxrljivj1lghxj7aypnhfjs3h1ly1xs87i92hj8") (f (quote (("std") ("futures") ("default") ("alloc"))))))

(define-public crate-alloc-facade-0.4.1 (c (n "alloc-facade") (v "0.4.1") (h "0xyhqsg88z8n66qs0bxdhyjl2wfqihdpvyb6pi68n2l66y96x17i") (f (quote (("std") ("futures") ("default") ("alloc"))))))

(define-public crate-alloc-facade-0.5.0 (c (n "alloc-facade") (v "0.5.0") (h "0h8cnrqcrdx5q4vlilig5s6x6kgck2b3pxfxcjr1qcn35zy1hnh4") (f (quote (("std") ("futures") ("default") ("alloc"))))))

