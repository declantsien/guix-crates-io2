(define-module (crates-io al lo alloc-from-pool) #:use-module (crates-io))

(define-public crate-alloc-from-pool-1.0.0 (c (n "alloc-from-pool") (v "1.0.0") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)))) (h "09pj87cwjdr8kpjzad9r5ixszx9iz6gww8ypx0yy1j5z03wzrxgx")))

(define-public crate-alloc-from-pool-1.0.1 (c (n "alloc-from-pool") (v "1.0.1") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)))) (h "168z7wv7npbhbwr8xz5cisphp5wflf2c9i40j2wz4ll15hs42djh")))

(define-public crate-alloc-from-pool-1.0.2 (c (n "alloc-from-pool") (v "1.0.2") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)))) (h "03cn79y1fidicv3fqwprjb5mwplcraj88szgbq2hv4rp841q7jaj")))

(define-public crate-alloc-from-pool-1.0.3 (c (n "alloc-from-pool") (v "1.0.3") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)))) (h "1rp73r6h4l8n87jr3gapfby8y49z6mrara0i5kl6gxbdn5ir9zk6")))

(define-public crate-alloc-from-pool-1.0.4 (c (n "alloc-from-pool") (v "1.0.4") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)))) (h "1zwpm4qk2xqyf59wwlpxynpzps71106wdwgrx87464rb3jzv8wlf")))

(define-public crate-alloc-from-pool-1.0.5 (c (n "alloc-from-pool") (v "1.0.5") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)))) (h "0v7sjg9jk93bqwshd2pnz7xkp4cz0fk5lvnhjfkqwr7mik2k1q5y")))

