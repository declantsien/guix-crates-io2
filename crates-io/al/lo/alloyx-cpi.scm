(define-module (crates-io al lo alloyx-cpi) #:use-module (crates-io))

(define-public crate-alloyx-cpi-0.0.1 (c (n "alloyx-cpi") (v "0.0.1") (d (list (d (n "anchor-gen") (r "^0.3.1") (d #t) (k 0)) (d (n "anchor-lang") (r "^0.26.0") (d #t) (k 0)) (d (n "borsh") (r "^0.9") (f (quote ("const-generics"))) (d #t) (k 0)) (d (n "solana-program") (r "=1.14.21") (d #t) (k 0)))) (h "1sm2br3c84xx2xaj8s6ffxd9izwv3i2khzlv2ckjqyajkj90q682") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("development") ("default" "cpi") ("cpi" "no-entrypoint"))))))

(define-public crate-alloyx-cpi-0.0.2 (c (n "alloyx-cpi") (v "0.0.2") (d (list (d (n "anchor-gen") (r "^0.3.1") (d #t) (k 0)) (d (n "anchor-lang") (r "^0.26.0") (d #t) (k 0)) (d (n "borsh") (r "^0.9") (f (quote ("const-generics"))) (d #t) (k 0)) (d (n "solana-program") (r "=1.14.21") (d #t) (k 0)))) (h "1nyxqmfffzvwm6r3m2zvccppfsj3qhb26j35jsxayaf5jdy9s25z") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("development") ("default" "cpi") ("cpi" "no-entrypoint"))))))

(define-public crate-alloyx-cpi-0.0.3 (c (n "alloyx-cpi") (v "0.0.3") (d (list (d (n "anchor-gen") (r "^0.3.1") (d #t) (k 0)) (d (n "anchor-lang") (r "^0.26.0") (d #t) (k 0)) (d (n "borsh") (r "^0.9") (f (quote ("const-generics"))) (d #t) (k 0)) (d (n "solana-program") (r "=1.14.21") (d #t) (k 0)))) (h "066s0pm8i858mpp6272ixk27p29g43cy1wz1nc8agkn6133n03aj") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("development") ("default" "cpi") ("cpi" "no-entrypoint"))))))

