(define-module (crates-io al lo allocator-api2) #:use-module (crates-io))

(define-public crate-allocator-api2-0.0.0 (c (n "allocator-api2") (v "0.0.0") (d (list (d (n "sptr") (r "^0.3") (d #t) (k 0)))) (h "0pz87hvkzqlg5p3zxnsw246s4qpsmg5rga7103fac9pq08hfjqci") (f (quote (("std" "alloc") ("nightly") ("default" "std") ("alloc"))))))

(define-public crate-allocator-api2-0.1.0 (c (n "allocator-api2") (v "0.1.0") (d (list (d (n "sptr") (r "^0.3") (d #t) (k 0)))) (h "0880xy3iizv2w5imgp6iinig2yqddjv1v8r6sj9hllbgnsm00fr6") (f (quote (("std" "alloc") ("nightly") ("default" "std") ("alloc")))) (y #t)))

(define-public crate-allocator-api2-0.1.1 (c (n "allocator-api2") (v "0.1.1") (d (list (d (n "sptr") (r "^0.3") (d #t) (k 0)))) (h "1hvyblsr45lswa6bdandfdwgqv5b4lpjw7n37qxkd47fjfgjy8sc") (f (quote (("std" "alloc") ("nightly") ("default" "std") ("alloc")))) (y #t)))

(define-public crate-allocator-api2-0.1.2 (c (n "allocator-api2") (v "0.1.2") (d (list (d (n "sptr") (r "^0.3") (d #t) (k 0)))) (h "1vab3ssyhv3i8awbwdyqqlhi4fhc90vppdk80fc662s25nphsh00") (f (quote (("std" "alloc") ("nightly") ("default" "std") ("alloc")))) (y #t)))

(define-public crate-allocator-api2-0.1.3 (c (n "allocator-api2") (v "0.1.3") (d (list (d (n "sptr") (r "^0.3") (d #t) (k 0)))) (h "0y3170xq3dp1g1wi50sgzri8985wsi99wy35jciibif5zyvhwjnb") (f (quote (("std" "alloc") ("nightly") ("default" "std") ("alloc"))))))

(define-public crate-allocator-api2-0.2.0 (c (n "allocator-api2") (v "0.2.0") (d (list (d (n "sptr") (r "^0.3") (d #t) (k 0)))) (h "1n327rfddb1alw5l767mxhm5vzxyrzkwb46pbkghahb1qp94mxsz") (f (quote (("std" "alloc") ("nightly") ("default" "std") ("alloc"))))))

(define-public crate-allocator-api2-0.2.1 (c (n "allocator-api2") (v "0.2.1") (d (list (d (n "sptr") (r "^0.3") (d #t) (k 0)))) (h "19c5physx6l4fqvb8w4lw1s60dk55qxny364lkfg2r87kdffcn59") (f (quote (("std" "alloc") ("nightly") ("default" "std") ("alloc"))))))

(define-public crate-allocator-api2-0.2.2 (c (n "allocator-api2") (v "0.2.2") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "sptr") (r "^0.3") (d #t) (k 0)))) (h "0r90sj61bz2hlqjqprxdlikzpggfhmxpbgxa2lrf56pjhzv17ikl") (f (quote (("std" "alloc") ("nightly") ("default" "std") ("alloc"))))))

(define-public crate-allocator-api2-0.2.3 (c (n "allocator-api2") (v "0.2.3") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "sptr") (r "^0.3") (d #t) (k 0)))) (h "1qw8pf3bf8q1zr012qx8l8sw7ffs93i307kjsdk5n1vfz9srw3dh") (f (quote (("std" "alloc") ("nightly") ("default" "std") ("alloc"))))))

(define-public crate-allocator-api2-0.2.4 (c (n "allocator-api2") (v "0.2.4") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "sptr") (r "^0.3") (d #t) (k 0)))) (h "04w23zd4rh8sjr80s9f8axcpfhg0jw66601sx50j4ql139zawz92") (f (quote (("std" "alloc") ("nightly") ("default" "std") ("alloc"))))))

(define-public crate-allocator-api2-0.2.5 (c (n "allocator-api2") (v "0.2.5") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "sptr") (r "^0.3") (d #t) (k 0)))) (h "1kh3cfx0s33mi0icdr8p95pngcmpas6kkywx0vg2vyk2fw0x1bg1") (f (quote (("std" "alloc") ("nightly") ("default" "std") ("alloc"))))))

(define-public crate-allocator-api2-0.2.6 (c (n "allocator-api2") (v "0.2.6") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "15r4dajn075mscsf5kjigygcwpfn1kwvw35papbk6xqgjcwjr1nm") (f (quote (("std" "alloc") ("nightly") ("default" "std") ("alloc"))))))

(define-public crate-allocator-api2-0.2.7-rc.1 (c (n "allocator-api2") (v "0.2.7-rc.1") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "194r5brxrl557r8k8ndpz8nnd70jpds9pg6rf84rng15h804055j") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-allocator-api2-0.2.7 (c (n "allocator-api2") (v "0.2.7") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1qxf732haw76wa1q17q9g9h367rqi4kfrz4n0a127xz6zy687sfw") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-allocator-api2-0.2.8 (c (n "allocator-api2") (v "0.2.8") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0kfpakj2bqfv1w9gsbfddrbaq8rv97vh7jhry0vy0scr4qlk3kz1") (f (quote (("std" "alloc") ("nightly") ("default" "std") ("alloc"))))))

(define-public crate-allocator-api2-0.2.9 (c (n "allocator-api2") (v "0.2.9") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "005qk5f0lz5lr8px6pildgn3p2zxdq9x1b5i9rxp1zzkz2f4lbym") (f (quote (("std" "alloc") ("nightly") ("default" "std") ("alloc"))))))

(define-public crate-allocator-api2-0.2.10 (c (n "allocator-api2") (v "0.2.10") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1cxi17pyqi7vmr32v2h7q8qhy7l9jf1l29am538nwz20ydk7yjjl") (f (quote (("std" "alloc") ("nightly") ("default" "std") ("alloc"))))))

(define-public crate-allocator-api2-0.2.11 (c (n "allocator-api2") (v "0.2.11") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1ba9nd0sz975rx9r148syg55y3x5va9jwmn0kb5lndxlqca102c1") (f (quote (("std" "alloc") ("nightly") ("default" "std") ("alloc"))))))

(define-public crate-allocator-api2-0.2.12 (c (n "allocator-api2") (v "0.2.12") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1wscjyzxwb0kfcf5wkrlv74pac4g575glada54kb3lzqyjhri53a") (f (quote (("std" "alloc") ("nightly") ("default" "std") ("alloc"))))))

(define-public crate-allocator-api2-0.2.13 (c (n "allocator-api2") (v "0.2.13") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1qh3pq46q3bn8krck3ns5l0x32wkp771nyb55rgj5n710sjs45pj") (f (quote (("std" "alloc") ("nightly") ("default" "std") ("alloc"))))))

(define-public crate-allocator-api2-0.2.14 (c (n "allocator-api2") (v "0.2.14") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0bjlw8v0fd9a65q2ah4pig2n00sx3j0zyhgb8ax1yq9mi9w67wn4") (f (quote (("std" "alloc") ("nightly") ("default" "std") ("alloc"))))))

(define-public crate-allocator-api2-0.2.15 (c (n "allocator-api2") (v "0.2.15") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1sdgkqs0pyk61m4yjksl5fv8wld11s5rp7v4v3p5hhccvkw6rz2n") (f (quote (("std" "alloc") ("nightly") ("default" "std") ("alloc"))))))

(define-public crate-allocator-api2-0.2.16 (c (n "allocator-api2") (v "0.2.16") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1iayppgq4wqbfbfcqmsbwgamj0s65012sskfvyx07pxavk3gyhh9") (f (quote (("std" "alloc") ("nightly") ("default" "std") ("alloc"))))))

(define-public crate-allocator-api2-0.2.17 (c (n "allocator-api2") (v "0.2.17") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "14k9ii4m9hg0p3d0pcyjkkbr93464fbdgj0b3k88pjhrcw9v5w7y") (f (quote (("std" "alloc") ("nightly") ("default" "std") ("alloc"))))))

(define-public crate-allocator-api2-0.2.18 (c (n "allocator-api2") (v "0.2.18") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0kr6lfnxvnj164j1x38g97qjlhb7akppqzvgfs0697140ixbav2w") (f (quote (("std" "alloc") ("nightly") ("default" "std") ("alloc"))))))

