(define-module (crates-io al lo alloc-metrics) #:use-module (crates-io))

(define-public crate-alloc-metrics-0.1.0 (c (n "alloc-metrics") (v "0.1.0") (d (list (d (n "derive_more") (r "^0.99") (f (quote ("add" "add_assign" "not"))) (k 0)) (d (n "spin") (r "^0.9") (f (quote ("spin_mutex" "mutex"))) (k 0)))) (h "0539knr97vckzakgq01naaswrzf9kgax5h1qdar70qvn1y0q864x") (f (quote (("thread" "std") ("std") ("global") ("default" "thread" "global"))))))

(define-public crate-alloc-metrics-0.1.1 (c (n "alloc-metrics") (v "0.1.1") (d (list (d (n "derive_more") (r "^0.99") (f (quote ("add" "add_assign" "not"))) (k 0)) (d (n "spin") (r "^0.9") (f (quote ("spin_mutex" "mutex"))) (k 0)))) (h "12pz1h2clygxk5vdh70zqz0byf8njyibd2yxg4lllw149w94nb36") (f (quote (("thread" "std") ("std") ("global") ("default" "thread" "global"))))))

