(define-module (crates-io al lo allocator-suite) #:use-module (crates-io))

(define-public crate-allocator-suite-0.1.1 (c (n "allocator-suite") (v "0.1.1") (d (list (d (n "either") (r "^1.5") (d #t) (k 0)) (d (n "libc") (r "^0.2.50") (d #t) (t "cfg(unix)") (k 0)) (d (n "syscall-alt") (r "^0.0.14") (d #t) (t "cfg(any(target_os = \"android\", target_os = \"linux\"))") (k 0)))) (h "0382qm1jnfkxvvizil9crma3d0yhnj2dglbnz8zczgxy5fnpcm6h")))

(define-public crate-allocator-suite-0.1.2 (c (n "allocator-suite") (v "0.1.2") (d (list (d (n "either") (r "^1.5") (d #t) (k 0)) (d (n "libc") (r "^0.2.50") (d #t) (t "cfg(unix)") (k 0)) (d (n "syscall-alt") (r "^0.0.14") (d #t) (t "cfg(any(target_os = \"android\", target_os = \"linux\"))") (k 0)))) (h "04y5wx2r0d6djza7s60k5x53pfxqvmskkpjngzi2d8c6h7mrwhpl")))

(define-public crate-allocator-suite-0.1.3 (c (n "allocator-suite") (v "0.1.3") (d (list (d (n "either") (r "^1.5") (d #t) (k 0)) (d (n "libc") (r "^0.2.50") (d #t) (t "cfg(unix)") (k 0)) (d (n "syscall-alt") (r "^0.0.14") (d #t) (t "cfg(any(target_os = \"android\", target_os = \"linux\"))") (k 0)))) (h "04qqm3nfmygn6vq8ccfxm3mzl8s402hyzv3nyiis6jz0pvqnk1l9")))

(define-public crate-allocator-suite-0.1.4 (c (n "allocator-suite") (v "0.1.4") (d (list (d (n "either") (r "^1.5") (d #t) (k 0)) (d (n "libc") (r "^0.2.50") (d #t) (t "cfg(unix)") (k 0)) (d (n "syscall-alt") (r "^0.0.14") (d #t) (t "cfg(any(target_os = \"android\", target_os = \"linux\"))") (k 0)))) (h "1jjksymxd8ypw0f07ja0cx8xv0dcqg3hfgcn2ydc5fi5ql245mi8")))

(define-public crate-allocator-suite-0.1.5 (c (n "allocator-suite") (v "0.1.5") (d (list (d (n "either") (r "^1.5") (d #t) (k 0)) (d (n "libc") (r "^0.2.50") (d #t) (t "cfg(unix)") (k 0)))) (h "1r0fwk4w3cidjcy4lg49iwf1f3nfgxji41w2z21vzvc49lya5cj9")))

(define-public crate-allocator-suite-0.1.6 (c (n "allocator-suite") (v "0.1.6") (d (list (d (n "either") (r "^1.5") (d #t) (k 0)) (d (n "libc") (r "^0.2.50") (d #t) (t "cfg(unix)") (k 0)))) (h "1nr84gm14pj7azhdyv9m4xv2wfmrhh7iimzkd1mda01k9903ksmr")))

(define-public crate-allocator-suite-0.1.7 (c (n "allocator-suite") (v "0.1.7") (d (list (d (n "either") (r "^1.5") (d #t) (k 0)) (d (n "libc") (r "^0.2.50") (d #t) (t "cfg(unix)") (k 0)))) (h "0igc86khywgka7a8nc0f4y1ry9bgym05k590agwprr8vmfpma93a")))

