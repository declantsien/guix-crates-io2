(define-module (crates-io al lo alloy-ethers-typecast) #:use-module (crates-io))

(define-public crate-alloy-ethers-typecast-0.1.0 (c (n "alloy-ethers-typecast") (v "0.1.0") (d (list (d (n "alloy-primitives") (r "^0.5.4") (f (quote ("rand"))) (d #t) (k 0)) (d (n "ethers") (r "^2.0.11") (d #t) (k 0)))) (h "0xmys7yg78r29666w3a1dbckgilyll77c27bbwfax1c2f6rzba5i")))

(define-public crate-alloy-ethers-typecast-0.2.0 (c (n "alloy-ethers-typecast") (v "0.2.0") (d (list (d (n "alloy-primitives") (r "^0.5.4") (f (quote ("rand"))) (d #t) (k 0)) (d (n "ethers") (r "^2.0") (f (quote ("legacy"))) (d #t) (k 0)))) (h "0d7bqwkv5c386l3f8qa37a7f1ajvrkidbg7ivixx20jrp8yj8jzc")))

