(define-module (crates-io al lo alloc-shim) #:use-module (crates-io))

(define-public crate-alloc-shim-0.1.0 (c (n "alloc-shim") (v "0.1.0") (h "1bqgvnw0mpdkhbyclm309zwx8vdm8c2x93f3dvvpgixvg5h4mbkr") (f (quote (("std"))))))

(define-public crate-alloc-shim-0.2.0 (c (n "alloc-shim") (v "0.2.0") (h "07ng074xmzbkvyww241qlq8m93sjjgm7awijqqc7isai9m5ip026") (f (quote (("std") ("alloc")))) (y #t)))

(define-public crate-alloc-shim-0.2.1 (c (n "alloc-shim") (v "0.2.1") (h "0v7rkv4fsmwr0gxv21k1c5swv3463bwkabw0z55an9xpy3gf2mqw") (f (quote (("std") ("alloc"))))))

(define-public crate-alloc-shim-0.3.0 (c (n "alloc-shim") (v "0.3.0") (h "1adfkw22xxah9g0ra1gn6nkmlhxla73dvi4w7qgnsdwf7bwvar4y") (f (quote (("std") ("futures") ("alloc"))))))

(define-public crate-alloc-shim-0.3.1 (c (n "alloc-shim") (v "0.3.1") (h "0lh3k7r2ahxa8mbz9vaimnf4r1p7v7kd5ws1iqfgms5hqqk4d4fg") (f (quote (("std") ("alloc"))))))

(define-public crate-alloc-shim-0.3.2 (c (n "alloc-shim") (v "0.3.2") (h "0sqdrnjz316pk7r5dxizkq14i7jvsznxpvhyw5znzvyihsm79xjp") (f (quote (("std") ("alloc"))))))

(define-public crate-alloc-shim-0.3.3 (c (n "alloc-shim") (v "0.3.3") (h "1lv2z2xnf4myy974x8a07mdwr7hvf1q5gih40gybr039xfbi4f4j") (f (quote (("std") ("alloc"))))))

(define-public crate-alloc-shim-0.3.4 (c (n "alloc-shim") (v "0.3.4") (h "14qzqg3qz38sbnx17x79byhmmal14i7zqjkqfgwhawfx54yv4qnc") (f (quote (("std") ("alloc"))))))

(define-public crate-alloc-shim-0.3.5 (c (n "alloc-shim") (v "0.3.5") (h "1amnkvv0ybk5v8p185mnz1hcilgaa73hgppxmaw5x050gvzzp4j3") (f (quote (("std") ("alloc"))))))

