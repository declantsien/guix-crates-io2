(define-module (crates-io al lo alloc_system) #:use-module (crates-io))

(define-public crate-alloc_system-0.1.0 (c (n "alloc_system") (v "0.1.0") (h "191zdyc3azd5ngdhif72b68baqm56jykb5za07jp2y7w7376hxkv")))

(define-public crate-alloc_system-0.1.1 (c (n "alloc_system") (v "0.1.1") (h "0lqcqydkyh82m8ww56fzmyjjfwimq5pfiwckxscnz6jkzm7nykar")))

(define-public crate-alloc_system-0.1.2 (c (n "alloc_system") (v "0.1.2") (h "0bk889cs422xk3z91pyi00cqc6zvjj26abr3f2grzycam6mdcsrq")))

