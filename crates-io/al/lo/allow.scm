(define-module (crates-io al lo allow) #:use-module (crates-io))

(define-public crate-allow-0.0.0-0.0.0-0.0.0 (c (n "allow") (v "0.0.0-0.0.0-0.0.0") (h "0yb17gihfbwkb7awszmgjsypmg5vjsnwy4qjy7wn872a3bxx4h34") (y #t)))

(define-public crate-allow-0.0.0-- (c (n "allow") (v "0.0.0--") (h "03z56kfyzw585m185y5nxxiflih6hnah2sczdhyrxnvm4c9h9l68") (y #t)))

(define-public crate-allow-0.1.0 (c (n "allow") (v "0.1.0") (d (list (d (n "allow_internal") (r "^0.1.1") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 1)))) (h "1w26bjpg71vmhfs65n169wimrv09sr4gg68rpsmkngw035ijmmdn") (r "1.45")))

(define-public crate-allow-0.1.1 (c (n "allow") (v "0.1.1") (d (list (d (n "allow_internal") (r "^0.1.1") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 1)))) (h "1rkmiy08xr55iqa5mg41pqxvl4vh03y7bpay8gyn0vygxnirmgry") (r "1.45")))

(define-public crate-allow-0.1.2 (c (n "allow") (v "0.1.2") (d (list (d (n "allow_internal") (r "^0.1.1") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 1)))) (h "08cwfqw2ad13mbnhwxrppzjggd45gnz0z5ks5a83p86qrpd26y2f") (r "1.45")))

(define-public crate-allow-0.1.3 (c (n "allow") (v "0.1.3") (d (list (d (n "allow_internal") (r "^0.1.1") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 1)))) (h "089k821kyx5gda0hksypvav5b365f42dwl5dmnfawym5v3pyaf40") (r "1.45")))

(define-public crate-allow-0.1.4 (c (n "allow") (v "0.1.4") (d (list (d (n "allow_internal") (r "^0.1.1") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 1)))) (h "0kk3gpzksshhl4c858y8w27j7ga5gqip0xrn3pzqvfq6b1an2i62") (r "1.45")))

(define-public crate-allow-0.1.5 (c (n "allow") (v "0.1.5") (d (list (d (n "allow_prefixed") (r "^0.1.5") (d #t) (k 0)) (d (n "paste") (r "1.*") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 1)))) (h "0zw338ksyw6prk7wa5lnii9x28kcq3jm7f2x6jll3by887ahkbbg") (r "1.45")))

