(define-module (crates-io al lo alloc_geiger) #:use-module (crates-io))

(define-public crate-alloc_geiger-0.1.0 (c (n "alloc_geiger") (v "0.1.0") (d (list (d (n "jemallocator") (r "^0.3") (d #t) (k 2)) (d (n "once_cell") (r "^0.2") (k 0)))) (h "1b3ng48j9imb2qacvl9zxna393jy6cqla90p1qpvflsbvp8gq2k7")))

(define-public crate-alloc_geiger-0.1.1 (c (n "alloc_geiger") (v "0.1.1") (d (list (d (n "jemallocator") (r "^0.3") (d #t) (k 2)) (d (n "once_cell") (r "^0.2") (k 0)))) (h "1qfq6csfy53z8lj65yy9hi48v4j7lz5wlb2bwp5k0k9272m8rymc")))

(define-public crate-alloc_geiger-0.2.0 (c (n "alloc_geiger") (v "0.2.0") (d (list (d (n "jemallocator") (r "^0.5") (d #t) (k 2)) (d (n "rodio") (r "^0.17") (k 0)))) (h "0cr43zdf9rmn2rsvw33ipp9njdlwg2dd8kzqg4fcrzk4hf54ymb4") (r "1.70")))

