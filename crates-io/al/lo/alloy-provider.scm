(define-module (crates-io al lo alloy-provider) #:use-module (crates-io))

(define-public crate-alloy-provider-0.1.0 (c (n "alloy-provider") (v "0.1.0") (h "08xrmyh124yhs5hizwrln6raxsp8igdg0wy8jc9a8g5m2clkq3kz")))

(define-public crate-alloy-provider-0.0.0-reserved (c (n "alloy-provider") (v "0.0.0-reserved") (h "027d3d7lm6h7dqwf0fmzjwxlibla5d86ni18f2ydryayxwc20hcc")))

