(define-module (crates-io al lo alloc_counter_macro) #:use-module (crates-io))

(define-public crate-alloc_counter_macro-0.0.0 (c (n "alloc_counter_macro") (v "0.0.0") (d (list (d (n "proc-macro2") (r "^0.4.27") (d #t) (k 0)) (d (n "quote") (r "^0.6.11") (d #t) (k 0)) (d (n "syn") (r "^0.15.26") (f (quote ("full"))) (d #t) (k 0)))) (h "0drxy357i2jjvgq3hl0mwi3bs66xkxi7sy9y54svq9fkka1npvnv")))

(define-public crate-alloc_counter_macro-0.0.1 (c (n "alloc_counter_macro") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^0.4.27") (d #t) (k 0)) (d (n "quote") (r "^0.6.11") (d #t) (k 0)) (d (n "syn") (r "^0.15.26") (f (quote ("full"))) (d #t) (k 0)))) (h "0pd04s5sxldwqra8mwvakz555klxlwpib8klvvl26lifh7frszf3")))

(define-public crate-alloc_counter_macro-0.0.2 (c (n "alloc_counter_macro") (v "0.0.2") (d (list (d (n "proc-macro2") (r "^1.0.5") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("full"))) (d #t) (k 0)))) (h "0nifqalryavmrdlkyv7cznp8yfjj16x0bjqzvjndw0fxk8gzhlhs")))

