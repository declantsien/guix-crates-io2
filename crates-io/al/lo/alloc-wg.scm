(define-module (crates-io al lo alloc-wg) #:use-module (crates-io))

(define-public crate-alloc-wg-0.1.0 (c (n "alloc-wg") (v "0.1.0") (h "0m52dqybzi3iwfj9zcz56bbh0y33g0gyxz6h8wmq7mkmka540xns") (f (quote (("std") ("default" "std"))))))

(define-public crate-alloc-wg-0.1.1 (c (n "alloc-wg") (v "0.1.1") (h "0dbhffnq3xv2qwn7g04cnx76cz8vlqygqcpj28lmsdgx11bdqbay") (f (quote (("std") ("default" "std"))))))

(define-public crate-alloc-wg-0.1.2 (c (n "alloc-wg") (v "0.1.2") (h "0k0a6xh0jl0df7g6py3zrmh07f71y49ihhjdjaskjxh5j9njrjji") (f (quote (("std") ("default" "std"))))))

(define-public crate-alloc-wg-0.1.3 (c (n "alloc-wg") (v "0.1.3") (h "0apfka48swxrgmjmf34dawvj7b1x5zqcsjymlmf7gjy443bjnbxi") (f (quote (("std") ("default" "std"))))))

(define-public crate-alloc-wg-0.1.4 (c (n "alloc-wg") (v "0.1.4") (h "1qlkzzxap0bpd2axbqabqaxmdx1gpzr7zidla1kk9q9al5fzd5dr") (f (quote (("std") ("default" "std"))))))

(define-public crate-alloc-wg-0.1.5 (c (n "alloc-wg") (v "0.1.5") (h "1cnnladx5xpc8b7ipvny187a9wrd58f7zx2389dsiyrmrs4p6j0p") (f (quote (("std") ("default" "std"))))))

(define-public crate-alloc-wg-0.2.0 (c (n "alloc-wg") (v "0.2.0") (h "0r5rz3sgkm7182x0kgcb0871w8gg997yxl50jgpvqk0dyigyhahg") (f (quote (("std") ("nightly" "dropck_eyepatch") ("dropck_eyepatch") ("default" "std"))))))

(define-public crate-alloc-wg-0.2.1 (c (n "alloc-wg") (v "0.2.1") (h "1lzp9kc2wxjhs0iqg85win0jpbhzbz4kir2gx0pz4575qb74lnkh") (f (quote (("std") ("nightly" "dropck_eyepatch") ("dropck_eyepatch") ("default" "std"))))))

(define-public crate-alloc-wg-0.3.0 (c (n "alloc-wg") (v "0.3.0") (h "1plhm78g79m2yicn81d4nx143ypg4spw03qpjpyy9xaqqwd7gkis") (f (quote (("std") ("ptr_internals") ("nightly" "dropck_eyepatch" "ptr_internals") ("dropck_eyepatch") ("default" "std"))))))

(define-public crate-alloc-wg-0.4.0 (c (n "alloc-wg") (v "0.4.0") (h "0w2kziwgq367yy5p1yiqg5jg3a5bz11icd94ddg53b69b38sjn5l") (f (quote (("std") ("ptr_internals") ("nightly" "dropck_eyepatch" "ptr_internals") ("dropck_eyepatch") ("default" "std"))))))

(define-public crate-alloc-wg-0.4.3 (c (n "alloc-wg") (v "0.4.3") (h "0yh4jlipm496ilzns955v23c6sy9gz33g61zqrwyxl5fdmfyl3pj") (f (quote (("std") ("receiver_trait") ("ptr_internals") ("nightly" "dropck_eyepatch" "ptr_internals" "coerce_unsized" "dispatch_from_dyn" "exact_size_is_empty" "fn_traits" "boxed_slice_try_from" "receiver_trait") ("fn_traits") ("exact_size_is_empty") ("dropck_eyepatch") ("dispatch_from_dyn") ("default" "std") ("coerce_unsized") ("boxed_slice_try_from"))))))

(define-public crate-alloc-wg-0.5.0 (c (n "alloc-wg") (v "0.5.0") (h "06jwhwdlg9yacyv56jj263y0bdwx05mhd7jhrvpqkmhdxyyvfimm") (f (quote (("std") ("receiver_trait") ("nightly" "dropck_eyepatch" "coerce_unsized" "dispatch_from_dyn" "exact_size_is_empty" "fn_traits" "boxed_slice_try_from" "receiver_trait") ("fn_traits") ("exact_size_is_empty") ("dropck_eyepatch") ("dispatch_from_dyn") ("default" "std") ("coerce_unsized") ("boxed_slice_try_from")))) (y #t)))

(define-public crate-alloc-wg-0.5.1 (c (n "alloc-wg") (v "0.5.1") (h "1glczm6wg3im5pvvc0p1ykx5jwldqj5ala2fqqdhz2dpni035pq2") (f (quote (("std") ("receiver_trait") ("nightly" "dropck_eyepatch" "coerce_unsized" "dispatch_from_dyn" "exact_size_is_empty" "fn_traits" "boxed_slice_try_from" "receiver_trait") ("fn_traits") ("exact_size_is_empty") ("dropck_eyepatch") ("dispatch_from_dyn") ("default" "std") ("coerce_unsized") ("boxed_slice_try_from"))))))

(define-public crate-alloc-wg-0.5.2 (c (n "alloc-wg") (v "0.5.2") (h "1knnppxhbb3qx2y4g6w7wpihwbym7xlv1ppk6nj3fxh8w21h2mpc") (f (quote (("std") ("receiver_trait") ("nightly" "dropck_eyepatch" "coerce_unsized" "dispatch_from_dyn" "exact_size_is_empty" "fn_traits" "boxed_slice_try_from" "receiver_trait") ("fn_traits") ("exact_size_is_empty") ("dropck_eyepatch") ("dispatch_from_dyn") ("default" "std") ("coerce_unsized") ("boxed_slice_try_from"))))))

(define-public crate-alloc-wg-0.6.0 (c (n "alloc-wg") (v "0.6.0") (h "04dwl1092dd55faip2g6irbichndjzjb12h0fggc0xl4s323mrd3") (f (quote (("std") ("receiver_trait") ("nightly" "dropck_eyepatch" "coerce_unsized" "dispatch_from_dyn" "exact_size_is_empty" "fn_traits" "boxed_slice_try_from" "receiver_trait") ("fn_traits") ("exact_size_is_empty") ("dropck_eyepatch") ("dispatch_from_dyn") ("default" "std") ("coerce_unsized") ("boxed_slice_try_from"))))))

(define-public crate-alloc-wg-0.6.1 (c (n "alloc-wg") (v "0.6.1") (h "19kkm82cla2jhhdrvwi02b8q5bsanv43r1l6q02qbkc6m722skld") (f (quote (("std") ("receiver_trait") ("nightly" "dropck_eyepatch" "coerce_unsized" "dispatch_from_dyn" "exact_size_is_empty" "fn_traits" "boxed_slice_try_from" "receiver_trait") ("fn_traits") ("exact_size_is_empty") ("dropck_eyepatch") ("dispatch_from_dyn") ("default" "std") ("coerce_unsized") ("boxed_slice_try_from"))))))

(define-public crate-alloc-wg-0.7.0 (c (n "alloc-wg") (v "0.7.0") (h "08p5a9wk71y1rr24rbbb2hshkk7vw90994sgsk1k24n0kzk72ks8") (f (quote (("std") ("default" "std"))))))

(define-public crate-alloc-wg-0.8.0 (c (n "alloc-wg") (v "0.8.0") (h "0y6j5c178fx8f8iq520p058gg0plh61nv37p3hyk6hgmvipqg0gw") (f (quote (("std") ("default" "std"))))))

(define-public crate-alloc-wg-0.9.0 (c (n "alloc-wg") (v "0.9.0") (h "1rm0xqsbki6w37s7fs1byg260glidznhy5jin8wbwd6w5vpx7i75") (f (quote (("std") ("default" "std"))))))

