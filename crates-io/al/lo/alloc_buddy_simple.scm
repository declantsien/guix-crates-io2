(define-module (crates-io al lo alloc_buddy_simple) #:use-module (crates-io))

(define-public crate-alloc_buddy_simple-0.1.0 (c (n "alloc_buddy_simple") (v "0.1.0") (d (list (d (n "spin") (r "^0.3.4") (o #t) (d #t) (k 0)))) (h "107marjbpp3fqlcx21nsi75svkkxd50xk41w1yxsl77115z6g5vq") (f (quote (("use-as-rust-allocator" "spin"))))))

(define-public crate-alloc_buddy_simple-0.1.2 (c (n "alloc_buddy_simple") (v "0.1.2") (d (list (d (n "spin") (r ">= 0.3.4, < 0.5") (o #t) (d #t) (k 0)))) (h "0a2w0gqbpn5i2yxbcrpgad0gjamcya2kxr6bhaa61hn8pbhbjam8") (f (quote (("use-as-rust-allocator" "spin"))))))

