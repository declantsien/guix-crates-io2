(define-module (crates-io al lo alloc_hoard) #:use-module (crates-io))

(define-public crate-alloc_hoard-0.1.0 (c (n "alloc_hoard") (v "0.1.0") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)))) (h "08092qb7dkyhcqnfn6fjkjq5g79qbp46njgwjjwsz23q6nskqv4a")))

(define-public crate-alloc_hoard-0.2.0 (c (n "alloc_hoard") (v "0.2.0") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)))) (h "0f5wxdphhw7f34wwgq5dnd8v9xk3813ld5y6zxxwrc635c270pqn")))

(define-public crate-alloc_hoard-0.3.0 (c (n "alloc_hoard") (v "0.3.0") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "08xz8dmksrj9j04ja5c43x085i28cbyldwnnpgh5drg03ixn7bck")))

