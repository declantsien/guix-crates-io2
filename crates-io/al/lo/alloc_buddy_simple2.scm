(define-module (crates-io al lo alloc_buddy_simple2) #:use-module (crates-io))

(define-public crate-alloc_buddy_simple2-0.1.1 (c (n "alloc_buddy_simple2") (v "0.1.1") (d (list (d (n "spin") (r ">= 0.3.4, < 0.5") (o #t) (d #t) (k 0)))) (h "0kx4nx01rwbrrd2smq2qvcl26qm3kf715imfjl31xds44axfwaz0") (f (quote (("use-as-rust-allocator" "spin"))))))

(define-public crate-alloc_buddy_simple2-0.1.2 (c (n "alloc_buddy_simple2") (v "0.1.2") (d (list (d (n "spin") (r ">= 0.3.4, < 0.5") (o #t) (d #t) (k 0)))) (h "03y15d94wbm62cxklckzkl71ibjfs1xv0x7iw9l325npl49gp453") (f (quote (("use-as-rust-allocator" "spin"))))))

