(define-module (crates-io al lo allochronic-util) #:use-module (crates-io))

(define-public crate-allochronic-util-0.0.0-reserve (c (n "allochronic-util") (v "0.0.0-reserve") (h "0xpy8nhn3n8gz1fsfv2d92ss5v3hh6r28d2gpz66071im0labq77")))

(define-public crate-allochronic-util-0.0.1-dev-1 (c (n "allochronic-util") (v "0.0.1-dev-1") (d (list (d (n "futures-executor") (r "^0.3") (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (d #t) (k 0)) (d (n "macros") (r "^0.0.1-dev-1") (d #t) (k 0) (p "allochronic-util-macros")) (d (n "macros") (r "^0.0.1-dev-1") (f (quote ("test"))) (d #t) (k 2) (p "allochronic-util-macros")) (d (n "pin-project") (r "^1") (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 0)))) (h "0fkyxlw1c8d3cnvx1lny81a83x3z3xcsdcyanm3abs2myxz1v649")))

