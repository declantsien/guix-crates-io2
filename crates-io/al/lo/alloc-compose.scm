(define-module (crates-io al lo alloc-compose) #:use-module (crates-io))

(define-public crate-alloc-compose-0.1.0 (c (n "alloc-compose") (v "0.1.0") (d (list (d (n "alloc") (r "^0.8.0") (k 0) (p "alloc-wg")))) (h "1abf9b1kqcqad3b5cl1ymcbphpr3z55lqfm06jszwhrnhkgdz8n8")))

(define-public crate-alloc-compose-0.2.0 (c (n "alloc-compose") (v "0.2.0") (h "02s25rf1zdhwyf9q97g97yc96vy72i6w8ry5hrvbwjx1kfvxpnq7") (f (quote (("alloc"))))))

(define-public crate-alloc-compose-0.3.0 (c (n "alloc-compose") (v "0.3.0") (h "13j7xxzvsd82w93n2dpp6c1q8lkxyhvrzc3icrgy7z62cixc80ab") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-alloc-compose-0.3.1 (c (n "alloc-compose") (v "0.3.1") (h "1fhzcmp00nlkc9wv99x19h21g3fqr5dp3hwi8fzgjisar54xmljz") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-alloc-compose-0.4.0 (c (n "alloc-compose") (v "0.4.0") (h "1sfp87b4hwfq3ds13kyl4fhrr1yxqyi3wqvma69y06x47dwsyd14") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-alloc-compose-0.4.1 (c (n "alloc-compose") (v "0.4.1") (h "0dpqnf9i6bvv9iyv5g3g5lls9mvpz4fzck3qds0w5q5vfy1d103k") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-alloc-compose-0.4.2 (c (n "alloc-compose") (v "0.4.2") (h "1pji88z4dj61dv15cvpgz5fwzgb7v8jwshb5fcd5l7q9nfx0br17") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-alloc-compose-0.5.0 (c (n "alloc-compose") (v "0.5.0") (h "1507rk5d55c9vy4h1yhjs8pkic443yhw2g2hwy9z6g2xvcygz0ib") (f (quote (("intrinsics") ("default" "alloc") ("alloc"))))))

