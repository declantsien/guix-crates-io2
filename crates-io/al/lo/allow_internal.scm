(define-module (crates-io al lo allow_internal) #:use-module (crates-io))

(define-public crate-allow_internal-0.1.0 (c (n "allow_internal") (v "0.1.0") (h "0vav9nfca4wa8gnnw3rq7sm4sw7p8vja45fy2846iv98k8wns6zg")))

(define-public crate-allow_internal-0.1.1 (c (n "allow_internal") (v "0.1.1") (h "12zm4qifraafkzqw4f0fipki6vv5xjvnhk2q7nbng7jwvfczmf96") (r "1.45")))

