(define-module (crates-io al lo alloy_graphene) #:use-module (crates-io))

(define-public crate-alloy_graphene-0.0.6 (c (n "alloy_graphene") (v "0.0.6") (d (list (d (n "gl") (r "^0.14.0") (o #t) (d #t) (k 0)) (d (n "strum") (r "^0.23.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.23.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0xdccnb0gi9fmhw9alis4m6sf5r2nljrwfap4hdc1hqip17x0s12") (f (quote (("opengl" "gl") ("default" "all") ("all" "opengl"))))))

