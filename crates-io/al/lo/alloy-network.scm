(define-module (crates-io al lo alloy-network) #:use-module (crates-io))

(define-public crate-alloy-network-0.1.0 (c (n "alloy-network") (v "0.1.0") (h "1kqpv13dp1hfalwzy4i3z1bxlj5fij4cld5bcgwn4bg2zya74ixi")))

(define-public crate-alloy-network-0.0.0-reserved (c (n "alloy-network") (v "0.0.0-reserved") (h "1q93jgpcs2fda9m6bk8s2jk2nbjm4gl75bvynsv1im64lydiqgal")))

