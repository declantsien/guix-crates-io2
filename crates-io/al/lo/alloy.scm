(define-module (crates-io al lo alloy) #:use-module (crates-io))

(define-public crate-alloy-0.1.0 (c (n "alloy") (v "0.1.0") (h "1r58n9apv37mxifiz4n9isn136br48r7pn7z7p2fs8qalziy8sc3")))

(define-public crate-alloy-0.0.0-reserved (c (n "alloy") (v "0.0.0-reserved") (h "1rpq68kvk9prcvqmc0784rlrvvhsgjzkgmrvq9qhbqhmmmli9m03")))

