(define-module (crates-io al lo allochronic-util-macros) #:use-module (crates-io))

(define-public crate-allochronic-util-macros-0.0.0-reserve (c (n "allochronic-util-macros") (v "0.0.0-reserve") (h "1rg67kdhkgjk916vg6ipnn8byvir5hidg9n3n9qj3vndghrm8qjl")))

(define-public crate-allochronic-util-macros-0.0.1-dev-1 (c (n "allochronic-util-macros") (v "0.0.1-dev-1") (d (list (d (n "futures-executor") (r "^0.3") (d #t) (k 2)) (d (n "futures-util") (r "^0.3") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0nps8nqbkcxyd16p094pdzhqi3a50kg0ffdy5zgz8slaycw4xfly") (f (quote (("test") ("default"))))))

