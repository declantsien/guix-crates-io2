(define-module (crates-io al lo allocations) #:use-module (crates-io))

(define-public crate-allocations-0.1.0-BETA (c (n "allocations") (v "0.1.0-BETA") (d (list (d (n "libc") (r "^0.2.155") (d #t) (k 0)) (d (n "windows") (r "^0.56.0") (f (quote ("Win32_System" "Win32_System_SystemInformation" "Win32_System_Memory_NonVolatile"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1f9z77z5scwz33h8cmmxc06iykpw6maz7mwmi762b862qaqhyb5k")))

