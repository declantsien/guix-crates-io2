(define-module (crates-io al lo alloc-traits) #:use-module (crates-io))

(define-public crate-alloc-traits-0.1.0 (c (n "alloc-traits") (v "0.1.0") (h "034ip7jxdrjx6hw69h4vl4b7avzpcak23z5mk1hh8sz6c2021dfq")))

(define-public crate-alloc-traits-0.1.1 (c (n "alloc-traits") (v "0.1.1") (h "10z4rmykwnp8ps5r5n34190h6gmzpj1f67fqildi1z8r6f2m8bbb")))

