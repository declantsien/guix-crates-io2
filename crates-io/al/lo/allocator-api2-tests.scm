(define-module (crates-io al lo allocator-api2-tests) #:use-module (crates-io))

(define-public crate-allocator-api2-tests-0.2.9 (c (n "allocator-api2-tests") (v "0.2.9") (d (list (d (n "allocator-api2") (r "=0.2.9") (d #t) (k 0)))) (h "0b2ya32h8cmfx3npsr1i9zf32zi8n59b36wabwb34ac2lsnbblgg") (f (quote (("nightly" "allocator-api2/nightly"))))))

(define-public crate-allocator-api2-tests-0.2.10 (c (n "allocator-api2-tests") (v "0.2.10") (d (list (d (n "allocator-api2") (r "=0.2.10") (d #t) (k 0)))) (h "056mxvj7ww6lipaai1r7ajrni8mpb1mwr9w4igqziq6cf2vx1k8b") (f (quote (("nightly" "allocator-api2/nightly"))))))

(define-public crate-allocator-api2-tests-0.2.11 (c (n "allocator-api2-tests") (v "0.2.11") (d (list (d (n "allocator-api2") (r "=0.2.11") (d #t) (k 0)))) (h "159mk32r2r03xv1ffcgpaavf3v01j9w92kqgb9fdmafacm2dgkrh") (f (quote (("nightly" "allocator-api2/nightly"))))))

(define-public crate-allocator-api2-tests-0.2.12 (c (n "allocator-api2-tests") (v "0.2.12") (d (list (d (n "allocator-api2") (r "=0.2.12") (d #t) (k 0)))) (h "0yjr7hcnc73irby5k2bb63pdk6yzbviviqp1v33b53jir85256w9") (f (quote (("nightly" "allocator-api2/nightly"))))))

(define-public crate-allocator-api2-tests-0.2.13 (c (n "allocator-api2-tests") (v "0.2.13") (d (list (d (n "allocator-api2") (r "=0.2.13") (d #t) (k 0)))) (h "0iira4sckdhib680b7sfjp07n7z05vfvrjq9711rq822hc7rqbc9") (f (quote (("nightly" "allocator-api2/nightly"))))))

(define-public crate-allocator-api2-tests-0.2.14 (c (n "allocator-api2-tests") (v "0.2.14") (d (list (d (n "allocator-api2") (r "=0.2.14") (d #t) (k 0)))) (h "129xza7aj8y9x8s4ycl8y23m6r1l2c7l4iy844dq9fbmrhrdirl2") (f (quote (("nightly" "allocator-api2/nightly"))))))

(define-public crate-allocator-api2-tests-0.2.15 (c (n "allocator-api2-tests") (v "0.2.15") (d (list (d (n "allocator-api2") (r "=0.2.18") (d #t) (k 0)))) (h "1ri2glzkffvgg8lqviv0py5ikbmpdckzf5ibij12m8p2q69zngiz") (f (quote (("nightly" "allocator-api2/nightly"))))))

