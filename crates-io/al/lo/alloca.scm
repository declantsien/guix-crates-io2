(define-module (crates-io al lo alloca) #:use-module (crates-io))

(define-public crate-alloca-0.1.0 (c (n "alloca") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1pyjrbhbxlps9dc9y9yz0ckji40rpshwinwgbx525w6sbdm2qckb")))

(define-public crate-alloca-0.1.1 (c (n "alloca") (v "0.1.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "01pgq7x8sgc1m5wv6hj83aik86nij0593hrlgxdxmyaycgm25qdj")))

(define-public crate-alloca-0.2.0 (c (n "alloca") (v "0.2.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1x9n9vdr6g084qgrx5rj0xa4bz1zwlj0irwxy4w2ykf7lriwvxzp")))

(define-public crate-alloca-0.2.1 (c (n "alloca") (v "0.2.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1qhl9d5iakg9hzpjsd572gppgrjcck6biz49diccshzsjyfyq6gg")))

(define-public crate-alloca-0.3.0 (c (n "alloca") (v "0.3.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0d5vnasv040z5qlkj8j5jqkw5lx2pjmiyzsa3lrfmgafwzaqrvbn")))

(define-public crate-alloca-0.3.1 (c (n "alloca") (v "0.3.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "04wpr7chr805zphg1wbiwrf4c5s3axrwgk3wi2ayq04kzficsm98")))

(define-public crate-alloca-0.3.2 (c (n "alloca") (v "0.3.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0w8cgvjs3nnqribg2y0p514dbzn5dxd5p0kmiyl707m58z9b2a6h")))

(define-public crate-alloca-0.3.3 (c (n "alloca") (v "0.3.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "09sjmmz6h2n0cmdnljc1had69zb83wl5wadv8nvk1s62rqwhic9p") (f (quote (("stack-clash-protection"))))))

(define-public crate-alloca-0.3.4 (c (n "alloca") (v "0.3.4") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0n0sxmns02c5k3cwcsw0bh7892plrdvq5946qk85pa5r1w2ryfl7") (f (quote (("stack-clash-protection"))))))

(define-public crate-alloca-0.4.0 (c (n "alloca") (v "0.4.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1x6p4387rz6j7h342kp3b7bgvqzyl9mibf959pkfk9xflrgd19z5") (f (quote (("stack-clash-protection"))))))

