(define-module (crates-io al lo alloy-consensus) #:use-module (crates-io))

(define-public crate-alloy-consensus-0.1.0 (c (n "alloy-consensus") (v "0.1.0") (h "1qa779ailrjls1pl9wlzjh31kwjh2665ymx9n99dfh4405xg6m8s")))

(define-public crate-alloy-consensus-0.0.0-reserved (c (n "alloy-consensus") (v "0.0.0-reserved") (h "11vvxq6i9f777dnrhwxkys9a5h1c443s0d8py3z9l61wfjvn7wcx")))

