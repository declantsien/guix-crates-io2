(define-module (crates-io al lo alloc-stdlib) #:use-module (crates-io))

(define-public crate-alloc-stdlib-0.1.0 (c (n "alloc-stdlib") (v "0.1.0") (d (list (d (n "alloc-no-stdlib") (r "~1") (d #t) (k 0)))) (h "1rcbhh8p6iw6zk754833amd43jmvdla61l4wfrm23gcpi215jijh") (f (quote (("unsafe"))))))

(define-public crate-alloc-stdlib-0.2.0 (c (n "alloc-stdlib") (v "0.2.0") (d (list (d (n "alloc-no-stdlib") (r "~2.0") (d #t) (k 0)))) (h "0rk6ymava9n7ww59hx4nia12ladh0g59rkyz9lqj1jpc40y221m4") (f (quote (("unsafe"))))))

(define-public crate-alloc-stdlib-0.2.1 (c (n "alloc-stdlib") (v "0.2.1") (d (list (d (n "alloc-no-stdlib") (r "~2.0") (d #t) (k 0)))) (h "1hj3r1x88aajnvigdck0diygj2isc90wa271kkj1swgiq3nxfzk9") (f (quote (("unsafe"))))))

(define-public crate-alloc-stdlib-0.2.2 (c (n "alloc-stdlib") (v "0.2.2") (d (list (d (n "alloc-no-stdlib") (r "^2.0.4") (d #t) (k 0)))) (h "1kkfbld20ab4165p29v172h8g0wvq8i06z8vnng14whw0isq5ywl") (f (quote (("unsafe"))))))

