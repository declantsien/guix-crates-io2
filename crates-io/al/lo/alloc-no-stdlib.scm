(define-module (crates-io al lo alloc-no-stdlib) #:use-module (crates-io))

(define-public crate-alloc-no-stdlib-1.0.0 (c (n "alloc-no-stdlib") (v "1.0.0") (h "0yfh3j76hh7g298f5f0mw99isgq080g8pvsydcjrjgvzlkqi7k83")))

(define-public crate-alloc-no-stdlib-1.1.0 (c (n "alloc-no-stdlib") (v "1.1.0") (h "1z4nwlqcwjaq23j25nsszp89ypdyh73gnzxapgsv3p0h5i3spb58") (f (quote (("unsafe") ("stdlib"))))))

(define-public crate-alloc-no-stdlib-1.2.0 (c (n "alloc-no-stdlib") (v "1.2.0") (h "1f3j7n1yc1bbfqhzha4n6fmkqaq93k9idvix1kbvazlmr7cnl7xj") (f (quote (("unsafe") ("no-stdlib"))))))

(define-public crate-alloc-no-stdlib-1.3.0 (c (n "alloc-no-stdlib") (v "1.3.0") (h "1538gkxsqym07wwxklrb8l9msm1vgx7qqqld49lwjy2x98bpx1vi") (f (quote (("unsafe") ("no-stdlib"))))))

(define-public crate-alloc-no-stdlib-2.0.0 (c (n "alloc-no-stdlib") (v "2.0.0") (h "113jwcrrkljcd3q5n34apdi82mk2ggyp10qraj5xr38q38gnldw9") (f (quote (("unsafe"))))))

(define-public crate-alloc-no-stdlib-2.0.1 (c (n "alloc-no-stdlib") (v "2.0.1") (h "19lhmi73fii1b6vrzh23vvp5yjqm33cb94h9yz17pn25b51yr4ji") (f (quote (("unsafe"))))))

(define-public crate-alloc-no-stdlib-2.0.2 (c (n "alloc-no-stdlib") (v "2.0.2") (h "0izmdmzb80qqz299l6ysgrzrffpl07kajqmimkiwfl6gwkvv9c40") (f (quote (("unsafe"))))))

(define-public crate-alloc-no-stdlib-2.0.3 (c (n "alloc-no-stdlib") (v "2.0.3") (h "1cqxcfhsd85cywzqb1f8wlix08gmjlm5ncn4wpmc9l8a94q4gvrm") (f (quote (("unsafe"))))))

(define-public crate-alloc-no-stdlib-2.0.4 (c (n "alloc-no-stdlib") (v "2.0.4") (h "1cy6r2sfv5y5cigv86vms7n5nlwhx1rbyxwcraqnmm1rxiib2yyc") (f (quote (("unsafe"))))))

