(define-module (crates-io al lo alloc-safe) #:use-module (crates-io))

(define-public crate-alloc-safe-0.1.0 (c (n "alloc-safe") (v "0.1.0") (h "05q3yy13lzlmlr605z6vc2ra6c8g9hvix7q02d1m0pywcs8kdark") (r "1.57")))

(define-public crate-alloc-safe-0.1.1 (c (n "alloc-safe") (v "0.1.1") (h "10kc04ycdr14xd35b7x8mypvx03sr4r8rm4hnkw4qxkn7jnv2jzf") (f (quote (("global-allocator") ("default" "global-allocator")))) (r "1.57")))

(define-public crate-alloc-safe-0.1.2 (c (n "alloc-safe") (v "0.1.2") (h "1x7kywkp7f1s7dg58glnajb1jd24n855819qg3j4r8z6by6y3hv8") (f (quote (("global-allocator") ("default" "global-allocator")))) (r "1.57")))

(define-public crate-alloc-safe-0.1.3 (c (n "alloc-safe") (v "0.1.3") (h "1jmdbxk2pxaap5mvwzpp8fkz9kv0kdgqvnpbc0ap5fn0m71ikfxs") (f (quote (("global-allocator") ("default" "global-allocator")))) (r "1.57")))

