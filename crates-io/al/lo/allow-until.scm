(define-module (crates-io al lo allow-until) #:use-module (crates-io))

(define-public crate-allow-until-0.1.0 (c (n "allow-until") (v "0.1.0") (d (list (d (n "semver") (r "^1.0.18") (d #t) (k 0)))) (h "1vi46dn7l16gmf4qp3r81a5pcg5473npzslfcjn9r1j2wwqbh712")))

(define-public crate-allow-until-0.2.0 (c (n "allow-until") (v "0.2.0") (d (list (d (n "semver") (r "^1.0.18") (d #t) (k 0)))) (h "1hzm6xz1gfkzcirzihcwqb9sk3i2j6vp2jh5a5xpyr91siqx3nrz")))

