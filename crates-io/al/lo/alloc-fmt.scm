(define-module (crates-io al lo alloc-fmt) #:use-module (crates-io))

(define-public crate-alloc-fmt-0.1.0 (c (n "alloc-fmt") (v "0.1.0") (d (list (d (n "backtrace") (r "^0.3.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "spin") (r "^0.4.6") (d #t) (k 0)))) (h "101j77xq62qvhwsg81lnhb6407w3jmpa4ls2a9pqcfcf9ax2zhq2")))

(define-public crate-alloc-fmt-0.2.0 (c (n "alloc-fmt") (v "0.2.0") (d (list (d (n "backtrace") (r "^0.3.3") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "spin") (r "^0.4.6") (d #t) (k 0)))) (h "1pph62hgrm4mq4rl3vbcv63xh7jnq4c23v4hcnz4lallp9adjzya") (f (quote (("print-backtrace" "backtrace") ("default" "print-backtrace"))))))

