(define-module (crates-io al lo allow_prefixed) #:use-module (crates-io))

(define-public crate-allow_prefixed-0.1.5 (c (n "allow_prefixed") (v "0.1.5") (d (list (d (n "allow_internal") (r "^0.1.1") (d #t) (k 0)) (d (n "cargo-toolchain") (r "^0.1.0") (d #t) (k 1)) (d (n "rustversion") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 1)))) (h "18b0zb1vyw1ijv251d2d9jjnnly251qmy1zz5sl5mlq6cnv51yr6") (r "1.45")))

(define-public crate-allow_prefixed-0.1.6 (c (n "allow_prefixed") (v "0.1.6") (d (list (d (n "allow_internal") (r "^0.1.1") (d #t) (k 0)) (d (n "cargo-toolchain") (r "^0.1.0") (d #t) (k 1)) (d (n "rustversion") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 1)))) (h "1pi8p3mx5cnn3h1jyrw8s6hyx27a5ifm2cx032ll38hj6xazvqqi") (f (quote (("no_nightly")))) (y #t) (r "1.45")))

