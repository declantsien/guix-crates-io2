(define-module (crates-io al lo alloc-singleton) #:use-module (crates-io))

(define-public crate-alloc-singleton-0.1.0 (c (n "alloc-singleton") (v "0.1.0") (d (list (d (n "as-slice") (r "^0.1.0") (d #t) (k 0)) (d (n "generic-array") (r "^0.12.0") (o #t) (d #t) (k 0)) (d (n "owned-singleton") (r "^0.1.0") (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.1.1") (k 0)))) (h "1ydrq5wqjf9a6i6w5469acs41z3szly6kk9zkd3lw02yx7m9hwxa") (f (quote (("nightly" "generic-array"))))))

