(define-module (crates-io al lo alloc-tls) #:use-module (crates-io))

(define-public crate-alloc-tls-0.1.0 (c (n "alloc-tls") (v "0.1.0") (d (list (d (n "alloc-fmt") (r "^0.1.0") (d #t) (k 0)))) (h "0cam4381fawcrr2yh8mjzzprlhq2vrcs5mb7yy3g518691my9xcm") (f (quote (("dylib"))))))

(define-public crate-alloc-tls-0.2.0 (c (n "alloc-tls") (v "0.2.0") (d (list (d (n "alloc-fmt") (r "^0.2.0") (d #t) (k 0)))) (h "1x42dd5b8gwvr1imjh496asd0c4f18g5gwxi0m85zxqj9hljjw96") (f (quote (("dylib"))))))

