(define-module (crates-io al lo alloc_counter) #:use-module (crates-io))

(define-public crate-alloc_counter-0.0.0 (c (n "alloc_counter") (v "0.0.0") (d (list (d (n "alloc_counter_macro") (r "^0.0.0") (o #t) (d #t) (k 0)))) (h "188xpwnddrvyk39fyvkpgmv8bj4n4jvx2ycp8r47zml16imxx9ig") (f (quote (("std") ("no_alloc") ("default" "std" "counters" "no_alloc" "alloc_counter_macro") ("counters"))))))

(define-public crate-alloc_counter-0.0.1 (c (n "alloc_counter") (v "0.0.1") (d (list (d (n "alloc_counter_macro") (r "^0.0.0") (o #t) (d #t) (k 0)))) (h "1dhnayr32baj1glcv63pxa3w72j7zjksdwyx575w80818lyplmv5") (f (quote (("std") ("no_alloc") ("default" "std" "counters" "no_alloc" "alloc_counter_macro") ("counters"))))))

(define-public crate-alloc_counter-0.0.2 (c (n "alloc_counter") (v "0.0.2") (d (list (d (n "alloc_counter_macro") (r "^0.0.1") (o #t) (d #t) (k 0)))) (h "0bqfy6p35n597vp03psj77whs4hhadgbnr4pgfj3hjl1hq2j6sd1") (f (quote (("std") ("no_alloc" "alloc_counter_macro") ("default" "std" "counters" "no_alloc") ("counters"))))))

(define-public crate-alloc_counter-0.0.3 (c (n "alloc_counter") (v "0.0.3") (d (list (d (n "alloc_counter_macro") (r "^0.0.2") (o #t) (d #t) (k 0)) (d (n "futures-executor-preview") (r "^0.3.0-alpha.19") (d #t) (k 2)) (d (n "pin-utils") (r "^0.1.0-alpha.4") (d #t) (k 0)))) (h "1fgcvqxp0dd2blxr5az6pwqkdaqpnin6wk0ryrsqjzyq9ahsqmrf") (f (quote (("std") ("macros" "alloc_counter_macro") ("default" "std" "macros"))))))

(define-public crate-alloc_counter-0.0.4 (c (n "alloc_counter") (v "0.0.4") (d (list (d (n "alloc_counter_macro") (r "^0.0.2") (o #t) (d #t) (k 0)) (d (n "futures-executor") (r "^0.3.0") (d #t) (k 2)) (d (n "pin-utils") (r "^0.1.0-alpha.4") (d #t) (k 0)))) (h "1agxzprqi37bcy9hh3clbsl3n0awbb34vrlv4rp5afib8w53m31s") (f (quote (("std") ("rustdoc") ("macros" "alloc_counter_macro") ("default" "std" "macros"))))))

