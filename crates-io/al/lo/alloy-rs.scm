(define-module (crates-io al lo alloy-rs) #:use-module (crates-io))

(define-public crate-alloy-rs-0.2.1 (c (n "alloy-rs") (v "0.2.1") (d (list (d (n "arrow2") (r "^0.18.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.150") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "0dv8hf2bc3hbapwbk8cdxn05rph8hzks1qk48d8mhxjqwkl19kfd")))

