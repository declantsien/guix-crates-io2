(define-module (crates-io al lo allocative_derive) #:use-module (crates-io))

(define-public crate-allocative_derive-0.2.0 (c (n "allocative_derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.27") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0jpk0rzav3y0i05jkg77cwjfyjg28xh41q9cnqzaky7kscac504f")))

(define-public crate-allocative_derive-0.3.0 (c (n "allocative_derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1y0fnpjp4d9hda291k9hx4m7lybslgcm2bjdz64nwnhh9bhmnbv7")))

(define-public crate-allocative_derive-0.3.1 (c (n "allocative_derive") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "05jpxdl6zvh22cwgpf7hk72s786g7yzk0kg1bzmqzdz16hsbw6p2")))

(define-public crate-allocative_derive-0.3.2 (c (n "allocative_derive") (v "0.3.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0ss55l2ynpkq1alkfy289j3lqlw34akczjbfd9xkf46k4mc1bj6s")))

(define-public crate-allocative_derive-0.3.3 (c (n "allocative_derive") (v "0.3.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1ax4wpf0v3cfy2wmwd1h3clmri6ymj87q7a2aqdgrq23fqvkl8zy")))

