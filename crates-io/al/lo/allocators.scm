(define-module (crates-io al lo allocators) #:use-module (crates-io))

(define-public crate-allocators-0.1.5 (c (n "allocators") (v "0.1.5") (h "0rhkbpsb9cjkgdlp1mzdr1ixy1wi0lpv3vf7lkihwxizi9rd9fy2")))

(define-public crate-allocators-0.1.6 (c (n "allocators") (v "0.1.6") (h "1yi4swsqf1daw0bjcbv74cjmpbdjfj64xi1dik8nhsfm24vv3jiy")))

(define-public crate-allocators-0.1.7 (c (n "allocators") (v "0.1.7") (h "0pc2bdx38yqz1nq1k47p4hfqq3rhbd5vaw0ck054j80849pb2lfb")))

(define-public crate-allocators-0.1.9 (c (n "allocators") (v "0.1.9") (h "1rc725gbd73cn49bzj7afp01bcwj0c01652ygyl8v25yxw6c7r2w")))

