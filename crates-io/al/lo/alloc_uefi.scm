(define-module (crates-io al lo alloc_uefi) #:use-module (crates-io))

(define-public crate-alloc_uefi-0.1.0 (c (n "alloc_uefi") (v "0.1.0") (h "1hjcwh8qgjial7biw67inlrpf56kf89ks0md9dnw5iii9m5g4zhs")))

(define-public crate-alloc_uefi-0.2.0 (c (n "alloc_uefi") (v "0.2.0") (h "07v5mh1yr83y5pd97spk1dc8vhmwg3n60szfrm9rb19inwgrkydm")))

