(define-module (crates-io al lo allocator_api) #:use-module (crates-io))

(define-public crate-allocator_api-0.0.1 (c (n "allocator_api") (v "0.0.1") (h "0rwy9phdagp2cass2i3dllscg450higk942fh58f4wwyc4g4va8j")))

(define-public crate-allocator_api-0.1.0 (c (n "allocator_api") (v "0.1.0") (d (list (d (n "rustc_version") (r "^0.2") (d #t) (k 1)))) (h "19nhzr6d7bc0b9q7qr38zrddax37r7x3hlqbjkl1fnqincp23jcn") (f (quote (("i128") ("fused"))))))

(define-public crate-allocator_api-0.2.0 (c (n "allocator_api") (v "0.2.0") (d (list (d (n "rustc_version") (r "^0.2") (d #t) (k 1)))) (h "12w4wz2nx2xw16vc2i9f06sfid576hl03z3931yrhy8pfnrwwyzh") (f (quote (("i128") ("fused"))))))

(define-public crate-allocator_api-0.2.1 (c (n "allocator_api") (v "0.2.1") (d (list (d (n "rustc_version") (r "^0.2") (d #t) (k 1)))) (h "1z2qpd3adz02glx2rm5rflrclq963px81pgc1xi0wmi2prig86k3") (f (quote (("i128") ("fused"))))))

(define-public crate-allocator_api-0.2.2 (c (n "allocator_api") (v "0.2.2") (d (list (d (n "rustc_version") (r "^0.2") (d #t) (k 1)))) (h "17brsvvi2b70mvv853cvh3smjd0dj746addf7pbwsczc81p3gc98") (f (quote (("i128") ("fused"))))))

(define-public crate-allocator_api-0.3.0 (c (n "allocator_api") (v "0.3.0") (d (list (d (n "rustc_version") (r "^0.2") (d #t) (k 1)))) (h "1wjzxjab88220n36r4lvc8hwh9ap6132kmzf541d20ix6v4vkjcd") (f (quote (("unstable_name_collision") ("i128") ("fused"))))))

(define-public crate-allocator_api-0.4.0 (c (n "allocator_api") (v "0.4.0") (d (list (d (n "rustc_version") (r "^0.2") (d #t) (k 1)))) (h "0yy85j3xfhw68b8dmkas06jpnqg0glkv6l09446cicxgr2x1rm91")))

(define-public crate-allocator_api-0.5.0 (c (n "allocator_api") (v "0.5.0") (d (list (d (n "rustc_version") (r "^0.2") (d #t) (k 1)))) (h "02p17p5vlqz1rrmpmb49phg8prk1xpxxvwa6mr5n9h58rw825n2a") (f (quote (("std") ("default" "std"))))))

(define-public crate-allocator_api-0.6.0 (c (n "allocator_api") (v "0.6.0") (h "1m32qzf0fanhd2x36l27k30j9782pp6j3hag2qb7779mxbmzqs28") (f (quote (("std") ("default" "std"))))))

