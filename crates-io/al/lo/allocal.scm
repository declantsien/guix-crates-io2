(define-module (crates-io al lo allocal) #:use-module (crates-io))

(define-public crate-allocal-0.1.0 (c (n "allocal") (v "0.1.0") (h "17pqwv89q5lll49vb0gjsajg8s3ia18dkdmry3w5gk2n7mrd0smi") (y #t)))

(define-public crate-allocal-0.1.1 (c (n "allocal") (v "0.1.1") (h "006sd1h25qs1d6f7ax0qzacfb2yq58kqmv53lnk20zhs311k6r5x") (y #t)))

