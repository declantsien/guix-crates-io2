(define-module (crates-io al lo alloy-merkle-tree) #:use-module (crates-io))

(define-public crate-alloy-merkle-tree-0.1.0 (c (n "alloy-merkle-tree") (v "0.1.0") (d (list (d (n "alloy-primitives") (r "^0.6.2") (d #t) (k 0)))) (h "0xshdfvf3ncsfwp5x7zgbbzsxm775s0hk34ccls076d3fmbxs3g0")))

(define-public crate-alloy-merkle-tree-0.1.1 (c (n "alloy-merkle-tree") (v "0.1.1") (d (list (d (n "alloy-primitives") (r "^0.6.2") (d #t) (k 0)))) (h "0cnzcsdr7yhrr3kmg9fllmy8ncxf7fr1l4b2hh49xb7kc1ajsrhh")))

(define-public crate-alloy-merkle-tree-0.2.0 (c (n "alloy-merkle-tree") (v "0.2.0") (d (list (d (n "alloy-primitives") (r "^0.6.2") (k 0)))) (h "042d9r2qsc6pmzljrynsjbf34n6bk9vgw05dfzjj457n4xlgppi5")))

(define-public crate-alloy-merkle-tree-0.3.0 (c (n "alloy-merkle-tree") (v "0.3.0") (d (list (d (n "alloy-dyn-abi") (r "^0.6.2") (k 0)) (d (n "alloy-primitives") (r "^0.6.2") (k 0)) (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.3") (d #t) (k 0)))) (h "0ldhk69w24sjpywqgh7qnb51v9jaihbqa8wyi4ydyjqd5bq5mp69")))

(define-public crate-alloy-merkle-tree-0.4.0 (c (n "alloy-merkle-tree") (v "0.4.0") (d (list (d (n "alloy-dyn-abi") (r "^0.6.2") (k 0)) (d (n "alloy-primitives") (r "^0.6.2") (k 0)) (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.3") (d #t) (k 0)))) (h "0x33klfb5mjfcybrdfnlhha8r1qba011z0451nj8qv46mmza4c03")))

(define-public crate-alloy-merkle-tree-0.5.0 (c (n "alloy-merkle-tree") (v "0.5.0") (d (list (d (n "alloy-dyn-abi") (r "^0.6.2") (k 0)) (d (n "alloy-primitives") (r "^0.6.2") (k 0)) (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.3") (d #t) (k 0)))) (h "1079cfljb9jyzlxy55m9k2cbrxjj4liblrcrc7pz4clz669cir25")))

