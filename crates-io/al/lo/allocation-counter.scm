(define-module (crates-io al lo allocation-counter) #:use-module (crates-io))

(define-public crate-allocation-counter-0.1.0 (c (n "allocation-counter") (v "0.1.0") (d (list (d (n "backtrace") (r "^0.3") (d #t) (k 0)))) (h "1xkpsw8jap8f9zwc0q4ncw6gmvbwzssm1i95vah89pm54gmwzkk4")))

(define-public crate-allocation-counter-0.2.0 (c (n "allocation-counter") (v "0.2.0") (d (list (d (n "backtrace") (r "^0.3") (d #t) (k 0)))) (h "19iaqn034mvw8f9grc2zwfqidg37v0z6bxy8nfflh9bv8q2p7y02")))

(define-public crate-allocation-counter-0.3.0 (c (n "allocation-counter") (v "0.3.0") (h "0yb4xh69zgjnr1mbglpinln6nz02rfnaysiiz3scqq056jbi4h47")))

(define-public crate-allocation-counter-0.4.0 (c (n "allocation-counter") (v "0.4.0") (h "005z2qisl5nqh407xsrgr41ijq3dnxw70vcr31s3j1b10xf0a29k")))

(define-public crate-allocation-counter-0.5.0 (c (n "allocation-counter") (v "0.5.0") (h "14789d9g2zafa1nj2vnc43v1pw3572qksm9yk2lj47dnqi6hzw16")))

(define-public crate-allocation-counter-0.5.1 (c (n "allocation-counter") (v "0.5.1") (h "08vlhqg077lkc1dm4dbn4v69rx4slk9y5np1cdi0r9h11qsyhl8w")))

(define-public crate-allocation-counter-0.6.0 (c (n "allocation-counter") (v "0.6.0") (h "1b0fdnhxcb118n08ik5s33bcby2p3vzs57nlrv51ba75fb0p184c")))

(define-public crate-allocation-counter-0.7.0 (c (n "allocation-counter") (v "0.7.0") (h "1h64fsfxyqxh1d21qiih4xqak3hx5vrq4wlp66xh4h2nkqcysd19")))

(define-public crate-allocation-counter-0.7.1 (c (n "allocation-counter") (v "0.7.1") (h "1p3hvpk7ymcp1qzqv3xnk66qw699gbpw7vdcvxj9r1q7n3dwm4wz")))

(define-public crate-allocation-counter-0.8.0 (c (n "allocation-counter") (v "0.8.0") (h "19nfhf7c58bq5an1ff8vhmahgpm9pik9fp453d305qbf264svx4j")))

(define-public crate-allocation-counter-0.8.1 (c (n "allocation-counter") (v "0.8.1") (h "1ja9l4nbxjjxw7rhn4yq5p3wq5fnxamsd1adk3qrjdm3q28fkfdy")))

