(define-module (crates-io al lo alloc-madvise) #:use-module (crates-io))

(define-public crate-alloc-madvise-0.2.0 (c (n "alloc-madvise") (v "0.2.0") (d (list (d (n "cbindgen") (r "^0.24.3") (d #t) (k 1)) (d (n "libc") (r "^0.2.139") (d #t) (k 0)))) (h "0wkrr3xsqi9vspdx5kp1gbr4zizr2d0lx1qy7vl0dqidcs9mwmsq") (r "1.66")))

(define-public crate-alloc-madvise-0.3.0 (c (n "alloc-madvise") (v "0.3.0") (d (list (d (n "cbindgen") (r "^0.24.3") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2.139") (d #t) (k 0)))) (h "0dx4c6cxvmljfp47a4fa5npfkj7awsbsas55cnwxvqn5sifmw5wd") (f (quote (("default" "ffi")))) (s 2) (e (quote (("ffi" "dep:cbindgen")))) (r "1.66")))

