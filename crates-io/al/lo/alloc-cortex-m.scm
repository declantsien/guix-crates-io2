(define-module (crates-io al lo alloc-cortex-m) #:use-module (crates-io))

(define-public crate-alloc-cortex-m-0.1.0 (c (n "alloc-cortex-m") (v "0.1.0") (d (list (d (n "cortex-m") (r "^0.1.5") (d #t) (k 0)) (d (n "linked_list_allocator") (r "^0.2.3") (d #t) (k 0)))) (h "1hg5lj8nk3jfbjy6zy68rf0r9z8ddfazq5nr1ag9s00g1inhxzj4")))

(define-public crate-alloc-cortex-m-0.2.0 (c (n "alloc-cortex-m") (v "0.2.0") (d (list (d (n "cortex-m") (r "^0.1.5") (d #t) (k 0)) (d (n "linked_list_allocator") (r "^0.2.3") (d #t) (k 0)))) (h "1jc33vdf2509fcr94di9v823kxjhjam7ws7zzfmfk3bidwvjai3l")))

(define-public crate-alloc-cortex-m-0.2.1 (c (n "alloc-cortex-m") (v "0.2.1") (d (list (d (n "cortex-m") (r "^0.1.5") (d #t) (k 0)) (d (n "linked_list_allocator") (r "^0.2.3") (d #t) (k 0)))) (h "13s46lvb5dfd36z2h5vc8159651b1gavnlkcly4h0q57g7iyawzv")))

(define-public crate-alloc-cortex-m-0.2.2 (c (n "alloc-cortex-m") (v "0.2.2") (d (list (d (n "cortex-m") (r "^0.1.5") (d #t) (k 0)) (d (n "linked_list_allocator") (r "^0.2.3") (d #t) (k 0)))) (h "05zr9f855lydpar8dy3fvhlin34nxp0024h3vsspx88jyqz23cf0")))

(define-public crate-alloc-cortex-m-0.3.0 (c (n "alloc-cortex-m") (v "0.3.0") (d (list (d (n "cortex-m") (r "^0.1.5") (d #t) (k 0)) (d (n "linked_list_allocator") (r "^0.4.2") (d #t) (k 0)))) (h "17jdz10y9d1ryn304wcp5k1l9nba07np6a3faclsg1kzk91zgw0p")))

(define-public crate-alloc-cortex-m-0.3.1 (c (n "alloc-cortex-m") (v "0.3.1") (d (list (d (n "cortex-m") (r "^0.1.5") (d #t) (k 0)) (d (n "linked_list_allocator") (r "^0.4.2") (d #t) (k 0)))) (h "1li2yc7115m54ja0742vggjxyyszqqmvc7js84c0sgxymciz2bx0")))

(define-public crate-alloc-cortex-m-0.3.2 (c (n "alloc-cortex-m") (v "0.3.2") (d (list (d (n "cortex-m") (r "^0.1.5") (d #t) (k 0)) (d (n "linked_list_allocator") (r "^0.5.0") (d #t) (k 0)))) (h "1g6v1gwy0941x5f06762gzak8nxvp40b6iqy3ncr3819053gwl3d")))

(define-public crate-alloc-cortex-m-0.3.3 (c (n "alloc-cortex-m") (v "0.3.3") (d (list (d (n "cortex-m") (r "^0.1.5") (d #t) (k 0)) (d (n "linked_list_allocator") (r "^0.6.0") (k 0)))) (h "012k9zn5vg5im1b7s134zkmp35jhaw8y0vhsrryhhp5z4plk54in")))

(define-public crate-alloc-cortex-m-0.3.4 (c (n "alloc-cortex-m") (v "0.3.4") (d (list (d (n "cortex-m") (r "^0.1.5") (d #t) (k 0)) (d (n "linked_list_allocator") (r "^0.6.0") (k 0)))) (h "1sk2ip203hs8kr756cdp667k6y3ybl8746j2120qipgiznmq3bpl")))

(define-public crate-alloc-cortex-m-0.3.5 (c (n "alloc-cortex-m") (v "0.3.5") (d (list (d (n "cortex-m") (r "^0.1.5") (d #t) (k 0)) (d (n "linked_list_allocator") (r "^0.6.0") (k 0)))) (h "1x5fwx058xf937h6f8vka7bybigxy7vldy9nwsfhiklkph0pspvd")))

(define-public crate-alloc-cortex-m-0.4.0 (c (n "alloc-cortex-m") (v "0.4.0") (d (list (d (n "cortex-m") (r "^0.6.2") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.12") (d #t) (k 2)) (d (n "linked_list_allocator") (r "^0.8.1") (k 0)))) (h "02y9bia66vx9ik6hnb72cnracw9l0ww7drly5fg0f4y723znfrf4")))

(define-public crate-alloc-cortex-m-0.4.1 (c (n "alloc-cortex-m") (v "0.4.1") (d (list (d (n "cortex-m") (r "^0.6.2") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.12") (d #t) (k 2)) (d (n "linked_list_allocator") (r "^0.8.11") (f (quote ("const_mut_refs"))) (k 0)))) (h "1bhlsr1m4kg6fb9jvnnviq97p1y1c54d28jp9vcpx25y2kzd0ym6")))

(define-public crate-alloc-cortex-m-0.4.2 (c (n "alloc-cortex-m") (v "0.4.2") (d (list (d (n "cortex-m") (r "^0.7.2") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.12") (d #t) (k 2)) (d (n "linked_list_allocator") (r "^0.8.11") (f (quote ("const_mut_refs"))) (k 0)))) (h "1z8yzihfndpyx4ipr9i2c3l6wvi0schdjmjc98x28fqzqqgf4sah")))

(define-public crate-alloc-cortex-m-0.4.3 (c (n "alloc-cortex-m") (v "0.4.3") (d (list (d (n "cortex-m") (r "^0.7.2") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.12") (d #t) (k 2)) (d (n "linked_list_allocator") (r "^0.10.4") (f (quote ("const_mut_refs"))) (k 0)))) (h "0ln46fn26i4iy9266x1g1glrb7164hh8hpwf7k5j8j48i5x9wc4v")))

(define-public crate-alloc-cortex-m-0.4.4 (c (n "alloc-cortex-m") (v "0.4.4") (d (list (d (n "cortex-m") (r "^0.7.2") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.12") (d #t) (k 2)) (d (n "linked_list_allocator") (r "^0.10.4") (f (quote ("const_mut_refs"))) (k 0)))) (h "0356y30fcd964g9w24w05m1pqs197lbmz3wqf8mrifx7z783ng28")))

