(define-module (crates-io al lo allocator-fallback) #:use-module (crates-io))

(define-public crate-allocator-fallback-0.1.0 (c (n "allocator-fallback") (v "0.1.0") (h "06flvhyr8masqf2lkx0n0s5ixwi338i7cbsvbxx0blqzbrw3sskj") (f (quote (("std") ("default" "std") ("allocator_api")))) (r "1.52")))

(define-public crate-allocator-fallback-0.1.1 (c (n "allocator-fallback") (v "0.1.1") (h "1295dx6jfpbb1gwmp4qhs989wjr77qd8f8hg3vzsddicg5k77zii") (f (quote (("std") ("default" "std") ("allocator_api")))) (r "1.52")))

(define-public crate-allocator-fallback-0.1.2 (c (n "allocator-fallback") (v "0.1.2") (h "0id9jhzn3n0xav0fml5l2w340vzkylyns4a7w0w9zpcfnlp2250y") (f (quote (("std") ("default" "std") ("allocator_api")))) (r "1.52")))

(define-public crate-allocator-fallback-0.1.3 (c (n "allocator-fallback") (v "0.1.3") (h "1kgm7bnarq9mgi49xgjrdwmw0b4qhdj54d3c7lwf20a0nj9jvwlg") (f (quote (("std") ("doc_cfg") ("default" "std") ("allocator_api")))) (r "1.52")))

(define-public crate-allocator-fallback-0.1.4 (c (n "allocator-fallback") (v "0.1.4") (h "1m4iwwh7iqf26ndmgxpvzjhd2ymqvmrwr3r44glm32j6hx8ml037") (f (quote (("std") ("doc_cfg") ("default" "std") ("allocator_api")))) (r "1.52")))

(define-public crate-allocator-fallback-0.1.5 (c (n "allocator-fallback") (v "0.1.5") (h "0jlcm41z46pqx8cm90yvi27x1bnv8z79f46mifzx9amjjp1z74mi") (f (quote (("std") ("doc_cfg") ("default" "std") ("allocator_api")))) (r "1.52")))

(define-public crate-allocator-fallback-0.1.6 (c (n "allocator-fallback") (v "0.1.6") (h "1i1mmhih29an3054dly6kd4j9w4rcbmm0y6iagy2bdvazs83qavp") (f (quote (("std") ("doc_cfg") ("default" "std") ("allocator_api")))) (r "1.52")))

(define-public crate-allocator-fallback-0.1.7 (c (n "allocator-fallback") (v "0.1.7") (h "08363hvnp3vxi9ylk82hlyrin0i0vpdc2s636jl0xzdmblrcfqcm") (f (quote (("std") ("doc_cfg") ("default" "std") ("allocator_api")))) (r "1.52")))

(define-public crate-allocator-fallback-0.1.8 (c (n "allocator-fallback") (v "0.1.8") (h "1gs9khb3pn4pfdrc1lb4k0bd0bm4lfs7145i1h2h2j30fvxxvnn3") (f (quote (("std") ("doc_cfg") ("default" "std") ("allocator_api")))) (r "1.52")))

