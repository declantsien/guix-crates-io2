(define-module (crates-io al lo allowance) #:use-module (crates-io))

(define-public crate-allowance-0.8.0 (c (n "allowance") (v "0.8.0") (h "1b1vhrzvrm3igs8imgrmdsd81imh2k5kl2chwk617d7i7bji51vi")))

(define-public crate-allowance-0.9.0 (c (n "allowance") (v "0.9.0") (h "1vvx2q7lbhiwfamx1xpnj56pjqlr0jkzlg703j49h2kxff3x55yw")))

(define-public crate-allowance-0.10.0 (c (n "allowance") (v "0.10.0") (h "045zvpwm3d2yii8f1r17ziyilmy06rhr9x66vxccwrppng5xcr6q")))

