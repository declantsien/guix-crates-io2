(define-module (crates-io al lo alloy-rlp-derive) #:use-module (crates-io))

(define-public crate-alloy-rlp-derive-0.1.0 (c (n "alloy-rlp-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0w3c648vrd2s9p95ilhia4nxi6vd2hfsxhrqp7v1ri40gmj5glcq") (r "1.65")))

(define-public crate-alloy-rlp-derive-0.2.0 (c (n "alloy-rlp-derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0hc80q36bjkxqzb29a0sn25y6qfpra9zb418qy3v9j1j0bgi3lss") (r "1.65")))

(define-public crate-alloy-rlp-derive-0.3.0 (c (n "alloy-rlp-derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0f93ach2hm0adil60h5zvgjph92cv8xjn61k4yqk823zw148pivn") (r "1.64")))

(define-public crate-alloy-rlp-derive-0.3.1 (c (n "alloy-rlp-derive") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "09b370pfvh8dylwms4kkcb22d3169sz1x9fprnghss34kgp9vrgc") (r "1.64")))

(define-public crate-alloy-rlp-derive-0.3.2 (c (n "alloy-rlp-derive") (v "0.3.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1zf44d7fs881730mlbi4a6agfbr6fpsiim3la066xi67id3bp9cs") (r "1.64")))

(define-public crate-alloy-rlp-derive-0.3.3 (c (n "alloy-rlp-derive") (v "0.3.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0mrilbnay4jki8fvankpq5qcd8bs546rvl846jp4xawzq1a1fff0") (r "1.64")))

(define-public crate-alloy-rlp-derive-0.3.4 (c (n "alloy-rlp-derive") (v "0.3.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "10wwhmxgyfmyn3kzd7lwgb2qsr4jramzs6jc4aqbpr1v6ybph10s") (r "1.64")))

(define-public crate-alloy-rlp-derive-0.3.5 (c (n "alloy-rlp-derive") (v "0.3.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0hb2v67rq2x86y32ym9w03d8k6js53d9zv6s50zhcaj6gwyf0dw0") (r "1.64")))

