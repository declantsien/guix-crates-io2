(define-module (crates-io al eo aleo-agent) #:use-module (crates-io))

(define-public crate-aleo-agent-1.0.0-alpha (c (n "aleo-agent") (v "1.0.0-alpha") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "indexmap") (r "^2") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3") (d #t) (k 0)) (d (n "snarkvm") (r "^0.16.19") (f (quote ("circuit" "console" "rocks"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "ureq") (r "^2.9.6") (d #t) (k 0)))) (h "0rb8ss9aaz2qgvpyxkxmckvb34r0a2ndjqzxywzrhq0hrkw93vhx") (r "1.78.0")))

(define-public crate-aleo-agent-1.0.1 (c (n "aleo-agent") (v "1.0.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "indexmap") (r "^2") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3") (d #t) (k 0)) (d (n "snarkvm") (r "^0.16.19") (f (quote ("circuit" "console" "rocks"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "ureq") (r "^2.9.6") (d #t) (k 0)))) (h "0c8bbi9n57k46p0b7sf2k5z4smh9m7wdi0v0qkn8lyr1caw5arn9") (r "1.78.0")))

