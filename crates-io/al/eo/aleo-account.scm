(define-module (crates-io al eo aleo-account) #:use-module (crates-io))

(define-public crate-aleo-account-0.3.0 (c (n "aleo-account") (v "0.3.0") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "rand") (r "^0.8.0") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3.0") (k 2)) (d (n "snarkvm-wasm") (r "^0.9.7") (f (quote ("parallel"))) (d #t) (k 0)))) (h "005ddzq3vxq43x1z5niyi9d7vcdmw361s81vghn4lfdxg918jlyp")))

(define-public crate-aleo-account-0.3.1 (c (n "aleo-account") (v "0.3.1") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "rand") (r "^0.8.0") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3.0") (k 2)) (d (n "snarkvm-wasm") (r "^0.9.8") (f (quote ("parallel"))) (d #t) (k 0)))) (h "0brkajhz66qn2i66v626z1xr7rzz1pjyi57pxm9qpd2m6cqzx4g7")))

(define-public crate-aleo-account-0.3.2 (c (n "aleo-account") (v "0.3.2") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "rand") (r "^0.8.0") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3.0") (k 2)) (d (n "snarkvm-wasm") (r "^0.9.11") (f (quote ("parallel"))) (d #t) (k 0)))) (h "1gv4balszf5p8zq4rdajkgpjl1fzg1fwva6488qhvw5d1ygmlmzn")))

(define-public crate-aleo-account-0.3.3 (c (n "aleo-account") (v "0.3.3") (d (list (d (n "snarkvm-wasm") (r "^0.9.11") (f (quote ("parallel"))) (d #t) (k 0)) (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "rand") (r "^0.8.0") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3.0") (k 2)))) (h "1nkv6gmhcqf443hlvihdjf3jy7851irpm34hnmppnl8lnxcj3lkn")))

