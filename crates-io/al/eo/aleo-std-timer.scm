(define-module (crates-io al eo aleo-std-timer) #:use-module (crates-io))

(define-public crate-aleo-std-timer-0.1.0 (c (n "aleo-std-timer") (v "0.1.0") (d (list (d (n "colored") (r "^2") (d #t) (k 0)))) (h "02qx583brrj0rfviaxpsa039ba3177fsqn8bflz3swgh453d7gi3") (f (quote (("timer") ("default"))))))

(define-public crate-aleo-std-timer-0.1.1 (c (n "aleo-std-timer") (v "0.1.1") (d (list (d (n "colored") (r "^2") (d #t) (k 0)))) (h "1rv2mwdg28gpjd73riz47rr61xzzckxqk55a2kyzkrphj6gndpay") (f (quote (("timer") ("default"))))))

(define-public crate-aleo-std-timer-0.1.2 (c (n "aleo-std-timer") (v "0.1.2") (d (list (d (n "colored") (r "^2") (o #t) (d #t) (k 0)))) (h "1i4jyidfcyl2svwajv86bgzjpxqkkdf2wqc9zz7fhwm3q4gihkvy") (f (quote (("timer" "colored") ("default"))))))

