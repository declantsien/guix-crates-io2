(define-module (crates-io al eo aleo-std-storage) #:use-module (crates-io))

(define-public crate-aleo-std-storage-0.1.0 (c (n "aleo-std-storage") (v "0.1.0") (d (list (d (n "dirs") (r "^4.0") (d #t) (k 0)))) (h "1vs6qwcp5ppx3j61d7pjkj0kvaq937z1wz1vl8dr0cbywhbf5c52")))

(define-public crate-aleo-std-storage-0.1.1 (c (n "aleo-std-storage") (v "0.1.1") (d (list (d (n "dirs") (r "^4.0") (d #t) (k 0)))) (h "18jwx6xr61w5pn7agcm34xh5ji8vzs06nhd6vfd7z75bh767mmgr")))

(define-public crate-aleo-std-storage-0.1.2 (c (n "aleo-std-storage") (v "0.1.2") (d (list (d (n "dirs") (r "^4.0") (d #t) (k 0)))) (h "051gs55csj078sxvj3bsvh4m9fxv4sajrk5zmgq4pdpnz21p6bii")))

(define-public crate-aleo-std-storage-0.1.3 (c (n "aleo-std-storage") (v "0.1.3") (d (list (d (n "dirs") (r "^4.0") (d #t) (k 0)))) (h "05y24agvk38ikx50223hk0844gx2z5a9f363r6dqd2qmslw2agjh")))

(define-public crate-aleo-std-storage-0.1.4 (c (n "aleo-std-storage") (v "0.1.4") (d (list (d (n "dirs") (r "^4.0") (d #t) (k 0)))) (h "01j5hrg1p1pag9jg9kr4w4i3aiy5mqz1rwi7ky5c1rzapvn7rlpg")))

(define-public crate-aleo-std-storage-0.1.5 (c (n "aleo-std-storage") (v "0.1.5") (d (list (d (n "dirs") (r "^4.0") (d #t) (k 0)))) (h "1bixrpafxm2fqy8k0490dygqba0yxsv1fkyz1jf8919wi58ibwwn")))

(define-public crate-aleo-std-storage-0.1.6 (c (n "aleo-std-storage") (v "0.1.6") (d (list (d (n "dirs") (r "^4.0") (d #t) (k 0)))) (h "09drdxzbcih3q0zxpx8c8z9ia2mdzq0zbxdws5vx7r6p8lmighy9")))

(define-public crate-aleo-std-storage-0.1.7 (c (n "aleo-std-storage") (v "0.1.7") (d (list (d (n "dirs") (r "^4.0") (d #t) (k 0)))) (h "1dcf5h9a3svwlwhg1gr0d6xc26qdcvpv5v2kb0k84rfm82ph0ca5")))

