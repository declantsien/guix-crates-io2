(define-module (crates-io al eo aleo-std-time) #:use-module (crates-io))

(define-public crate-aleo-std-time-0.1.0 (c (n "aleo-std-time") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15.42") (f (quote ("full"))) (d #t) (k 0)))) (h "13gwifha07in54kdskz8prjj6mi7ha18g9dmn7jz6qv7n9qy0rbh") (f (quote (("time") ("default"))))))

(define-public crate-aleo-std-time-0.1.1 (c (n "aleo-std-time") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15.42") (f (quote ("full"))) (d #t) (k 0)))) (h "0d1y4vja9k2znm949dnrqp10s7s0z5l20y969fq58148pdfp68lw") (f (quote (("time") ("default"))))))

(define-public crate-aleo-std-time-0.1.2 (c (n "aleo-std-time") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "02jlvxv26bmxsrq9iaacnsapqjwy38hfa4jkm6sswbjcy10siwkj") (f (quote (("time") ("default"))))))

