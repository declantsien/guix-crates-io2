(define-module (crates-io al eo aleo-std-timed) #:use-module (crates-io))

(define-public crate-aleo-std-timed-0.1.0 (c (n "aleo-std-timed") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "13y0dzqvy8nbnairpfqw82062ca6qx1v4jni2dk5p5nanz8sx19b") (f (quote (("timed") ("default"))))))

(define-public crate-aleo-std-timed-0.1.1 (c (n "aleo-std-timed") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "117k2r08iwzp2s0xa5iarcj741qmrnv540fgw7ld2fiz5j31s60i") (f (quote (("timed") ("default"))))))

(define-public crate-aleo-std-timed-0.1.2 (c (n "aleo-std-timed") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "09z4q9msd9d0423lsaz6k2fvyyy3558fmm9iic4czb45camvl631") (f (quote (("timed") ("default"))))))

