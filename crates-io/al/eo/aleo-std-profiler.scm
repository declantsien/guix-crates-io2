(define-module (crates-io al eo aleo-std-profiler) #:use-module (crates-io))

(define-public crate-aleo-std-profiler-0.1.9 (c (n "aleo-std-profiler") (v "0.1.9") (d (list (d (n "colored") (r "^2") (o #t) (d #t) (k 0)))) (h "014g3jvsx88kq2xqyjlha6kskklf75ny50wfhh2jhrcz19yijkx3") (f (quote (("profiler" "colored"))))))

(define-public crate-aleo-std-profiler-0.1.10 (c (n "aleo-std-profiler") (v "0.1.10") (d (list (d (n "colored") (r "^2") (o #t) (d #t) (k 0)))) (h "16dnrmb15aflnsfgil7x9yi5gpng9bkx45341j7xv527mbpsziw8") (f (quote (("profiler" "colored"))))))

(define-public crate-aleo-std-profiler-0.1.11 (c (n "aleo-std-profiler") (v "0.1.11") (d (list (d (n "colored") (r "^2") (o #t) (d #t) (k 0)))) (h "1pkm0bqigpj45si12q8qpik5adsw47y44ipmwkr7knfrhw0c3wjv") (f (quote (("profiler" "colored"))))))

(define-public crate-aleo-std-profiler-0.1.12 (c (n "aleo-std-profiler") (v "0.1.12") (d (list (d (n "colored") (r "^2") (o #t) (d #t) (k 0)))) (h "1zhsy2phk2bny8b6dl3s25vg5bmhmkrfnk8nh9rk3bqqyg99f2in") (f (quote (("profiler" "colored"))))))

(define-public crate-aleo-std-profiler-0.1.13 (c (n "aleo-std-profiler") (v "0.1.13") (d (list (d (n "colored") (r "^2") (o #t) (d #t) (k 0)))) (h "05fckjyfh6zz54rg3fyc4hjnqlm7bs7vbjzbxsnc4aw3wbnaqwix") (f (quote (("profiler" "colored"))))))

(define-public crate-aleo-std-profiler-0.1.14 (c (n "aleo-std-profiler") (v "0.1.14") (d (list (d (n "colored") (r "^2") (o #t) (d #t) (k 0)))) (h "19d3980q7g6ad84g97vk1vmrp05g2mmann7y84nscr6hqi37w04v") (f (quote (("profiler" "colored") ("default"))))))

(define-public crate-aleo-std-profiler-0.1.15 (c (n "aleo-std-profiler") (v "0.1.15") (d (list (d (n "colored") (r "^2") (o #t) (d #t) (k 0)))) (h "0gaccs02z3zlq0agr2glzz5g7wkdjpkqg7fijiishkzmnbfmbw1b") (f (quote (("profiler" "colored") ("default"))))))

