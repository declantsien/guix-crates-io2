(define-module (crates-io al eo aleo-std-cpu) #:use-module (crates-io))

(define-public crate-aleo-std-cpu-0.1.3 (c (n "aleo-std-cpu") (v "0.1.3") (h "1vp247jxa6ypnj4r2dfz8ccg0r0xp12g2mj7pqy7652xalwwgp6i")))

(define-public crate-aleo-std-cpu-0.1.4 (c (n "aleo-std-cpu") (v "0.1.4") (h "1f7gxa2xsjrppri1abw2sv650zgz2gwkrph235mbxzbmlqd3a9vm")))

