(define-module (crates-io al iu aliu) #:use-module (crates-io))

(define-public crate-aliu-0.1.0 (c (n "aliu") (v "0.1.0") (h "0wrkj2dfyz4hp6cn99jdrq5wxi2x18qd2cb8wjs2bifw8pwsjlh4")))

(define-public crate-aliu-0.1.1 (c (n "aliu") (v "0.1.1") (h "0ys866h1ziwx2zscgvfarj4xy3fkh36g6c3j3d2wcf62g5z5n4nx")))

(define-public crate-aliu-0.1.2 (c (n "aliu") (v "0.1.2") (h "1fdwp0sz9fdwvlvcj4ax8f6kzi9wyk1qx3mbgkzqnqf24j408b6c")))

(define-public crate-aliu-0.1.3 (c (n "aliu") (v "0.1.3") (h "1d2rgk7prcfx46mp4y0rfcriavringwg5ri532wlvzwx474xzzvw")))

(define-public crate-aliu-0.1.4 (c (n "aliu") (v "0.1.4") (h "0iwq4f5mhbxdqvs54cwpqyb9pg7wv7xkxlwjnsq9l8nqr9zj0fbg")))

(define-public crate-aliu-0.1.5 (c (n "aliu") (v "0.1.5") (h "1hpbyvldhxmr9m4h68bvr77gpqfiqniy9bzz4rq4xfqzjncf0277")))

(define-public crate-aliu-0.1.6 (c (n "aliu") (v "0.1.6") (h "0blqipaaaqqqzjp63c5jgi3ray3v5y7yyf135ayqjvbma1psvfma")))

(define-public crate-aliu-0.1.7 (c (n "aliu") (v "0.1.7") (h "1wpx555mg3yd7hnjdzrbypnbkymchm4474kqj2yc97q8xgxgd61r")))

(define-public crate-aliu-0.1.8 (c (n "aliu") (v "0.1.8") (h "1zp9m6r35b61qjm7aldjr66d36qjrfs0h3ypfzc548amx5bjbvqz")))

(define-public crate-aliu-0.1.9 (c (n "aliu") (v "0.1.9") (h "05bb11rdwka1bv9683nlspi1xwsq1ky8ny8mw53016d18wxzkzpr")))

(define-public crate-aliu-0.1.10 (c (n "aliu") (v "0.1.10") (h "1vnfbc3q7kpi7i7f2irid4k6l7ji3hvxz2xq1dbl93vzng2qb24l")))

(define-public crate-aliu-0.1.11 (c (n "aliu") (v "0.1.11") (h "1mpc6n3mac67iaamkwppqxgrz14zf6ff96n1yzp44kb504216ry2")))

(define-public crate-aliu-0.1.12 (c (n "aliu") (v "0.1.12") (h "1dbcgzwggkh2kd4ylq766g4bfr4kx773mqi0d96ri5cncibcic53")))

(define-public crate-aliu-0.1.13 (c (n "aliu") (v "0.1.13") (h "1q8p2iscfz4c9yjhx0mgn8wb0fqmhixxnnydn3zlqk3qwznfd5i5")))

(define-public crate-aliu-0.1.14 (c (n "aliu") (v "0.1.14") (d (list (d (n "libc") (r "=0.2.107") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("basetsd" "minwindef" "sysinfoapi" "memoryapi" "winnt"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1l7bdm6w0g5a56rg0xqn51pmcv3gpzdb3v46434lrmk4nbkahb9r")))

(define-public crate-aliu-0.1.15 (c (n "aliu") (v "0.1.15") (d (list (d (n "libc") (r "=0.2.107") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("basetsd" "minwindef" "sysinfoapi" "memoryapi" "winnt"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1a33z92k631px0iryvzgfwfhi116zsngnpyw9n30rnqzkapbg807")))

(define-public crate-aliu-0.1.16 (c (n "aliu") (v "0.1.16") (d (list (d (n "libc") (r "=0.2.107") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("basetsd" "minwindef" "sysinfoapi" "memoryapi" "winnt"))) (d #t) (t "cfg(windows)") (k 0)))) (h "02n5xbmky14km8f7qlrynvynf8hsrl339vzgkx952fzgpfjsixly")))

(define-public crate-aliu-0.1.17 (c (n "aliu") (v "0.1.17") (d (list (d (n "libc") (r "=0.2.107") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("minwindef" "memoryapi" "winnt"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0apfr4sh2lgynp4llvzgz226z2q05d1irab9c30lhcj6yi1w29cl")))

(define-public crate-aliu-0.1.18 (c (n "aliu") (v "0.1.18") (d (list (d (n "libc") (r "=0.2.107") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("minwindef" "memoryapi" "winnt"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1c2qd9mh704334fja04dvp56xv80327ghdp4yisih6jw33jy6kga")))

(define-public crate-aliu-0.1.19 (c (n "aliu") (v "0.1.19") (d (list (d (n "libc") (r "=0.2.107") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("minwindef" "memoryapi" "winnt"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1v86s4riigy7nv9jz63jmqpi9pdn6g3bbbj8i7p1n9w6c48w857y")))

(define-public crate-aliu-0.1.20 (c (n "aliu") (v "0.1.20") (d (list (d (n "libc") (r "=0.2.107") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("minwindef" "memoryapi" "winnt"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1fiflpvizyxxdqmwfjpjsx5bp125zp577xnhpcyr096rp84i6air")))

(define-public crate-aliu-0.1.21 (c (n "aliu") (v "0.1.21") (d (list (d (n "libc") (r "=0.2.107") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("minwindef" "memoryapi" "winnt"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0l342h06ryk8jk3zbhbik9jkmgbdl8vrkyr2zsd306k4fr3ngkgr")))

(define-public crate-aliu-0.1.22 (c (n "aliu") (v "0.1.22") (d (list (d (n "libc") (r "=0.2.107") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("minwindef" "memoryapi" "winnt"))) (d #t) (t "cfg(windows)") (k 0)))) (h "07lx0z19siwpw0sicd3kfrqw4gmy2sj6yrn5sw438p797avd0i9f")))

