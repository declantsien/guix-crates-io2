(define-module (crates-io al ph alphaid) #:use-module (crates-io))

(define-public crate-alphaid-0.1.0 (c (n "alphaid") (v "0.1.0") (h "0n09bj8sm7c0b6fpjjnvj0ipnr652qvz4n3dhj4z0gh2isi6mx6v")))

(define-public crate-alphaid-0.2.0 (c (n "alphaid") (v "0.2.0") (d (list (d (n "num") (r "^0.2.1") (d #t) (k 0)))) (h "1iia3bjbq83ljjwxl2man3ifmsddfkkgx06clfagwhhgf9q4ccna")))

