(define-module (crates-io al ph alpha_g_detector) #:use-module (crates-io))

(define-public crate-alpha_g_detector-0.1.0 (c (n "alpha_g_detector") (v "0.1.0") (d (list (d (n "crc32c") (r "^0.6") (d #t) (k 0)) (d (n "midasio") (r "^0.4") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1qck82yhv5bcqfzvc4q9kw7yvd0pjbzyf86p1v8v2aa6gp41qbv6")))

(define-public crate-alpha_g_detector-0.2.0 (c (n "alpha_g_detector") (v "0.2.0") (d (list (d (n "crc32c") (r "^0.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "midasio") (r "^0.4") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0izkdyixh6msczycz3mnbbwwwp8dm1ba7l2whv7lp8v5dya5yjgz")))

(define-public crate-alpha_g_detector-0.3.0 (c (n "alpha_g_detector") (v "0.3.0") (d (list (d (n "crc32c") (r "^0.6.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "midasio") (r "^0.5.0") (d #t) (k 2)) (d (n "ron") (r "^0.8.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "1jjkxkgx7g2z9z3hbq89gl4xg2nfrwcvcdb6az9f3fxnny9ikagq")))

(define-public crate-alpha_g_detector-0.3.1 (c (n "alpha_g_detector") (v "0.3.1") (d (list (d (n "crc32c") (r "^0.6.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "midasio") (r "^0.5.0") (d #t) (k 2)) (d (n "ron") (r "^0.8.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "0bdxzfnkm1x0cr78ysg1a7sbf3niz4m638j4h1i63pv0hx1q2nap")))

(define-public crate-alpha_g_detector-0.3.2 (c (n "alpha_g_detector") (v "0.3.2") (d (list (d (n "crc32c") (r "^0.6.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "midasio") (r "^0.5.0") (d #t) (k 2)) (d (n "ron") (r "^0.8.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "0blx25x1lz0xmbdpr5q97pxgn1nlz4i12mqswxl254zml7aw3kfq")))

(define-public crate-alpha_g_detector-0.4.0 (c (n "alpha_g_detector") (v "0.4.0") (d (list (d (n "crc32c") (r "^0.6.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "midasio") (r "^0.5.0") (d #t) (k 2)) (d (n "ron") (r "^0.8.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)) (d (n "winnow") (r "^0.6.1") (d #t) (k 0)))) (h "0692yc58n7xprqqq27nzwidk9z10i092kqa41yvxradv1s9chz7g")))

(define-public crate-alpha_g_detector-0.4.1 (c (n "alpha_g_detector") (v "0.4.1") (d (list (d (n "crc32c") (r "^0.6.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "midasio") (r "^0.5.0") (d #t) (k 2)) (d (n "ron") (r "^0.8.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)) (d (n "winnow") (r "^0.6.1") (d #t) (k 0)))) (h "1nllzfhxyf3dpjb0d3dzl6wzinvdg6b96yav3g8fpg7siw75bxk0")))

(define-public crate-alpha_g_detector-0.5.0 (c (n "alpha_g_detector") (v "0.5.0") (d (list (d (n "crc32c") (r "^0.6.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "midasio") (r "^0.5.0") (d #t) (k 2)) (d (n "ron") (r "^0.8.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)) (d (n "winnow") (r "^0.6.1") (d #t) (k 0)))) (h "17c7c1cjdh1fi4y4rlx8h6xcyag4f7ixk3jgiarv53nw6dpfa0lp")))

