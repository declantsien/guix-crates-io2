(define-module (crates-io al ph alpha-counter) #:use-module (crates-io))

(define-public crate-alpha-counter-0.1.0 (c (n "alpha-counter") (v "0.1.0") (h "14ww06mxg8snwr6wyn0ixf63s3aipmhsl8im88zb6ly0k2i2vi86")))

(define-public crate-alpha-counter-0.1.1 (c (n "alpha-counter") (v "0.1.1") (h "0g5r5lrypg6hqfdrxbfjf02pcbl24lsbfc9qg97gjd03kgwfq32c")))

(define-public crate-alpha-counter-0.2.0 (c (n "alpha-counter") (v "0.2.0") (h "0dz8y4gfzh21fc1jls0nrfabl5y1pijqhj8skczg08h838yqzj63")))

