(define-module (crates-io al ph alpha-counter-cli) #:use-module (crates-io))

(define-public crate-alpha-counter-cli-0.1.0 (c (n "alpha-counter-cli") (v "0.1.0") (d (list (d (n "alpha-counter") (r "^0.1.1") (d #t) (k 0)) (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)))) (h "0scjkgp0zjafxiiqmv2pd255iy44ss9p2cq07d2bg40xbjizvynk")))

(define-public crate-alpha-counter-cli-0.2.0 (c (n "alpha-counter-cli") (v "0.2.0") (d (list (d (n "alpha-counter") (r "^0.2.0") (d #t) (k 0)) (d (n "clap") (r "^4.1.6") (f (quote ("derive"))) (d #t) (k 0)))) (h "1ac9bm9bc1wfcbysk0p880a6jyky45k4gz5ibb2ivpsah0wm0z25")))

(define-public crate-alpha-counter-cli-0.2.1 (c (n "alpha-counter-cli") (v "0.2.1") (d (list (d (n "alpha-counter") (r "^0.2.0") (d #t) (k 0)) (d (n "clap") (r "^4.1.6") (f (quote ("derive"))) (d #t) (k 0)))) (h "0r97g2cyyxwjn8s6zjwf6cii0w6gpzcbc1mzk2fs22gx34xhxvs7")))

