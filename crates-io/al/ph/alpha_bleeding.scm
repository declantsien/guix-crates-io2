(define-module (crates-io al ph alpha_bleeding) #:use-module (crates-io))

(define-public crate-alpha_bleeding-0.1.0 (c (n "alpha_bleeding") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "image") (r "^0.24.7") (d #t) (k 0)))) (h "0dm5cy3i55pdjyrk10xrwn2n3a58v8p1lflavnkislgs34h9zw3y")))

