(define-module (crates-io al ph alphanumeric-sort) #:use-module (crates-io))

(define-public crate-alphanumeric-sort-1.0.0 (c (n "alphanumeric-sort") (v "1.0.0") (h "0l6xvng4i0n02kdnxcvnipzsgbxcsyd4lbs1qrvm3brid9bmrbl2")))

(define-public crate-alphanumeric-sort-1.0.1 (c (n "alphanumeric-sort") (v "1.0.1") (h "0ybdgxnbx6fmcl4dxpxnnjcz7k9m2hlpvrdksyf74h32lc79z4qx")))

(define-public crate-alphanumeric-sort-1.0.2 (c (n "alphanumeric-sort") (v "1.0.2") (h "1ad0ac4ifv0cw52z7qgfxfigwdzrziyn8jgqlhfb4m0xw81rckfr")))

(define-public crate-alphanumeric-sort-1.0.3 (c (n "alphanumeric-sort") (v "1.0.3") (h "1n2zdmg1x87fy3s7pqxj1wd6q32zc5mzam2cgq8q2hk789iaib6j")))

(define-public crate-alphanumeric-sort-1.0.4 (c (n "alphanumeric-sort") (v "1.0.4") (h "17li19w275la897nfikl2bp5x14d22lvm8pwi8jb3jhrkz4bwyqh")))

(define-public crate-alphanumeric-sort-1.0.5 (c (n "alphanumeric-sort") (v "1.0.5") (h "1wccylw5asyr0gjxb6123x91l1y77h3l8fj251fjqwjyhvd7gjwl")))

(define-public crate-alphanumeric-sort-1.0.6 (c (n "alphanumeric-sort") (v "1.0.6") (h "0fascv26jcmn6mdzmfn8w7sr78k7mw8a7501vf0xcm66jl65ilkw")))

(define-public crate-alphanumeric-sort-1.0.7 (c (n "alphanumeric-sort") (v "1.0.7") (h "0yri68n5qlwcf3yzv6ajisr7ywgj5jlkpxzvm547d32ylwvhg08p")))

(define-public crate-alphanumeric-sort-1.0.8 (c (n "alphanumeric-sort") (v "1.0.8") (h "1n76mcz668wcb9jwgf0r3v42yy92xiy8midpgyf8p08kd32lii57")))

(define-public crate-alphanumeric-sort-1.0.9 (c (n "alphanumeric-sort") (v "1.0.9") (h "19y9nh3d36knk4k02vak2wkbbh6frxfnr9dpwa3w6lfymk32i6lp")))

(define-public crate-alphanumeric-sort-1.0.10 (c (n "alphanumeric-sort") (v "1.0.10") (h "004kqaznkg8ja3klkscc6dj7prxqbimxyisr4rfrrkgpqacgzjb9")))

(define-public crate-alphanumeric-sort-1.0.11 (c (n "alphanumeric-sort") (v "1.0.11") (h "1i4zsmninxry7s9mmm9v6y7n1q8lg2ms6wbmz1hnjgypai0yjz7k")))

(define-public crate-alphanumeric-sort-1.0.12 (c (n "alphanumeric-sort") (v "1.0.12") (h "16qhncx157ib3f8x9277a323pv49df84xwhqxncvnm60v7rr3f92")))

(define-public crate-alphanumeric-sort-1.0.13 (c (n "alphanumeric-sort") (v "1.0.13") (h "0j041cc4sq6ycj1zfzps5w02xyjdgm9mr4dh8i7lxl7h4a78lcqd")))

(define-public crate-alphanumeric-sort-1.1.0 (c (n "alphanumeric-sort") (v "1.1.0") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)))) (h "1maq7wx2hw95zaj9iz7ic7vflbb80w8nlpi09bz1z9vhzpgsi1rq") (f (quote (("std") ("default" "std"))))))

(define-public crate-alphanumeric-sort-1.1.1 (c (n "alphanumeric-sort") (v "1.1.1") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)))) (h "0sfmk94jlrkpn0h15p2yznpc9ykw5kqn7kamzq8m6qkamz71mrgx") (f (quote (("std") ("default" "std"))))))

(define-public crate-alphanumeric-sort-1.2.0 (c (n "alphanumeric-sort") (v "1.2.0") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)))) (h "1ikbyw81lny2xh63zmcfqs78lhch5bakc6fpwda5b055kblc4xm8") (f (quote (("std") ("default" "std"))))))

(define-public crate-alphanumeric-sort-1.2.1 (c (n "alphanumeric-sort") (v "1.2.1") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)))) (h "1glxqxk259lmma8pbpjch5phji8basynk5vpv7vv5qk4v7d4apr6") (f (quote (("std") ("default" "std"))))))

(define-public crate-alphanumeric-sort-1.3.0 (c (n "alphanumeric-sort") (v "1.3.0") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)))) (h "1djkmaz63ylaydmdxm0ldwrpp6plcz3p58c1xxnhc4x2axzzrjcg") (f (quote (("std") ("default" "std"))))))

(define-public crate-alphanumeric-sort-1.3.1 (c (n "alphanumeric-sort") (v "1.3.1") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)))) (h "1ni7rgs8px8cbdhiiy4ndmmxh8kc7ma3g1cii5wnyfgbcnb2g0wn") (f (quote (("std") ("default" "std"))))))

(define-public crate-alphanumeric-sort-1.3.2 (c (n "alphanumeric-sort") (v "1.3.2") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)))) (h "04ndqfs6b9yq9810kd5nralf8vfnhfph4dapf1d6lm8gyryxsa6b") (f (quote (("std") ("default" "std"))))))

(define-public crate-alphanumeric-sort-1.3.3 (c (n "alphanumeric-sort") (v "1.3.3") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)))) (h "1klkmhr2ipgl9li80m5gzrczswsqjkadir839zvj94s4ws9xjyni") (f (quote (("std") ("default" "std"))))))

(define-public crate-alphanumeric-sort-1.4.0 (c (n "alphanumeric-sort") (v "1.4.0") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)))) (h "1f99pdzd3b4sj9mjdm1q74cj40nnd9x3mxvplam6jk7fyh35grz0") (f (quote (("std") ("default" "std"))))))

(define-public crate-alphanumeric-sort-1.4.1 (c (n "alphanumeric-sort") (v "1.4.1") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)))) (h "10h75k21xxzdy0vm23hgkz4jzcm7pw17kif1w91sch5rsc68kzjl") (f (quote (("std") ("default" "std"))))))

(define-public crate-alphanumeric-sort-1.4.2 (c (n "alphanumeric-sort") (v "1.4.2") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)))) (h "1ml224bhbzyc9sasyvdgnkdb23dg9x8nblylsb329ndl29qn0w91") (f (quote (("std") ("default" "std"))))))

(define-public crate-alphanumeric-sort-1.4.3 (c (n "alphanumeric-sort") (v "1.4.3") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)))) (h "166nijqc05kqb3qn8mn8h5lscrdc2qz4jvxg8nzzn7scrcn9pr90") (f (quote (("std") ("default" "std"))))))

(define-public crate-alphanumeric-sort-1.4.4 (c (n "alphanumeric-sort") (v "1.4.4") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)))) (h "1q7ww2c6s3my1d6cdws0fax9296mjlalcyldqwz944r6p2mwksbp") (f (quote (("std") ("default" "std"))))))

(define-public crate-alphanumeric-sort-1.5.0 (c (n "alphanumeric-sort") (v "1.5.0") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)))) (h "1a9z3zx1lhdxh8f24xzzg2phvrvkdwdqm0qdznkxkpil3nh2m5sy") (f (quote (("std") ("default" "std"))))))

(define-public crate-alphanumeric-sort-1.5.1 (c (n "alphanumeric-sort") (v "1.5.1") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)))) (h "13hzg58ynj2b3h5ngcww1hp4hiawksa5bkqdr1cbgqjls9890541") (f (quote (("std") ("default" "std"))))))

(define-public crate-alphanumeric-sort-1.5.2 (c (n "alphanumeric-sort") (v "1.5.2") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)))) (h "01k86hcd8g8w4z48hd5l38xr6xcpq8n1y8asvyc815knk447iwri") (f (quote (("std") ("default" "std")))) (y #t) (r "1.56")))

(define-public crate-alphanumeric-sort-1.5.3 (c (n "alphanumeric-sort") (v "1.5.3") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)))) (h "13vyx3cqpylvc0md4563rd42b7dvk3fv4wny0kpcc48gy72n0z6n") (f (quote (("std") ("default" "std")))) (r "1.56")))

