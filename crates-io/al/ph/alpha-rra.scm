(define-module (crates-io al ph alpha-rra) #:use-module (crates-io))

(define-public crate-alpha-rra-0.1.1 (c (n "alpha-rra") (v "0.1.1") (d (list (d (n "adjustp") (r "^0.1.4") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.62") (d #t) (k 0)) (d (n "hashbrown") (r "^0.12.3") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "ndarray-rand") (r "^0.14.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5.3") (d #t) (k 0)) (d (n "statrs") (r "^0.16.0") (d #t) (k 0)))) (h "0y514k8zyhbjl7ycmb2z0nvwq29zjj67wj3p2hdycg4nyc6m1biz")))

(define-public crate-alpha-rra-0.1.2 (c (n "alpha-rra") (v "0.1.2") (d (list (d (n "adjustp") (r "^0.1.4") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.62") (d #t) (k 0)) (d (n "hashbrown") (r "^0.12.3") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "ndarray-rand") (r "^0.14.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5.3") (d #t) (k 0)) (d (n "statrs") (r "^0.16.0") (d #t) (k 0)))) (h "159mc3nzq9y5x1gbmvvy0fwp8qzvv52s12x8pw8wblb2ff6qc7dz")))

(define-public crate-alpha-rra-0.1.3 (c (n "alpha-rra") (v "0.1.3") (d (list (d (n "adjustp") (r "^0.1.4") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.62") (d #t) (k 0)) (d (n "hashbrown") (r "^0.12.3") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "ndarray-rand") (r "^0.14.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5.3") (d #t) (k 0)) (d (n "statrs") (r "^0.16.0") (d #t) (k 0)))) (h "0gpra6qgqzn15sfc0rnvrdf21713ywg6vc0amwwbg964db0xppci")))

(define-public crate-alpha-rra-0.1.4 (c (n "alpha-rra") (v "0.1.4") (d (list (d (n "adjustp") (r "^0.1.4") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.62") (d #t) (k 0)) (d (n "hashbrown") (r "^0.12.3") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "ndarray-rand") (r "^0.14.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5.3") (d #t) (k 0)) (d (n "statrs") (r "^0.16.0") (d #t) (k 0)))) (h "01xqcjaswgz8qr9rh8hafwfjkx7nd4cf8d1yidg57i2dp9f6n0aa")))

(define-public crate-alpha-rra-0.1.5 (c (n "alpha-rra") (v "0.1.5") (d (list (d (n "adjustp") (r "^0.1.4") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.62") (d #t) (k 0)) (d (n "hashbrown") (r "^0.12.3") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "ndarray-rand") (r "^0.14.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5.3") (d #t) (k 0)) (d (n "statrs") (r "^0.16.0") (d #t) (k 0)))) (h "1n4xdzvgj270f69rvifvbbki8rsqx1fkgzdr0wwb1ykwmiipbnpw")))

