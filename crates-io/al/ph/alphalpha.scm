(define-module (crates-io al ph alphalpha) #:use-module (crates-io))

(define-public crate-alphalpha-0.1.0 (c (n "alphalpha") (v "0.1.0") (d (list (d (n "itertools") (r "^0.10.5") (o #t) (d #t) (k 0)) (d (n "lophat") (r "^0.9.1") (o #t) (k 0)) (d (n "spade") (r "^2.2.0") (d #t) (k 0)))) (h "1brqfkxgll136zaxb5nj15q1yhsqbxk9zk7mp51cccz5imhpv450") (f (quote (("default" "lophat")))) (s 2) (e (quote (("lophat" "dep:lophat" "dep:itertools"))))))

