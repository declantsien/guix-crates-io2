(define-module (crates-io al ph alphafix) #:use-module (crates-io))

(define-public crate-alphafix-0.1.0 (c (n "alphafix") (v "0.1.0") (d (list (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "image") (r "^0.24.5") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "1pil58ig7w1jmmjxf7rm6v6hjhbiwmwy2x7xdsbd92br3ll714iv")))

