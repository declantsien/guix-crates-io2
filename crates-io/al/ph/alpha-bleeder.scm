(define-module (crates-io al ph alpha-bleeder) #:use-module (crates-io))

(define-public crate-alpha-bleeder-0.1.0 (c (n "alpha-bleeder") (v "0.1.0") (d (list (d (n "image") (r "0.18.*") (d #t) (k 0)))) (h "1b46x14mds32ipvy85f0h7c1iw7wizyi4yz69ab3y0sa5ya8vs8r")))

(define-public crate-alpha-bleeder-0.2.0 (c (n "alpha-bleeder") (v "0.2.0") (d (list (d (n "image") (r "0.18.*") (d #t) (k 0)))) (h "08dva1bq6d1rnskv4mrn7nd1bqi10d6w4jk9z55ymcnhcrrxq29b")))

(define-public crate-alpha-bleeder-0.3.0 (c (n "alpha-bleeder") (v "0.3.0") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "glob") (r "^0.2.11") (d #t) (k 0)) (d (n "image") (r "0.18.*") (d #t) (k 0)) (d (n "threadpool") (r "^1.7.1") (d #t) (k 0)))) (h "03g3nq10s1pm4cpdi9qqfbxlqll6ypwg5rcfgnf6l60w3cg70j27")))

