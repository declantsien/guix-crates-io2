(define-module (crates-io al ph alphabet-encoder) #:use-module (crates-io))

(define-public crate-alphabet-encoder-0.1.0 (c (n "alphabet-encoder") (v "0.1.0") (h "1g1vclmbkbq6i6ihf93mfh08z0b97i14760wazk95784m4fyzgix")))

(define-public crate-alphabet-encoder-0.1.1 (c (n "alphabet-encoder") (v "0.1.1") (h "1hpsnm46v1j0z9bprisdcqsapak37jvq05s4lcxjhbbniqllws7k")))

