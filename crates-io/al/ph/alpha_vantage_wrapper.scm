(define-module (crates-io al ph alpha_vantage_wrapper) #:use-module (crates-io))

(define-public crate-alpha_vantage_wrapper-0.2.2 (c (n "alpha_vantage_wrapper") (v "0.2.2") (d (list (d (n "reqwest") (r "^0.11.22") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.189") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.33.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1zmq7bm5x1zix4xhsi7q59cmqqhsymji3xj01b9nn6zv1hvdz4m2") (y #t)))

(define-public crate-alpha_vantage_wrapper-0.2.3 (c (n "alpha_vantage_wrapper") (v "0.2.3") (d (list (d (n "reqwest") (r "^0.11.22") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.189") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.33.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1xdjp9h2w5brq8v0barqspwk0vxhgzskvj0qg380yvivv40vdk0n") (y #t)))

(define-public crate-alpha_vantage_wrapper-0.2.4 (c (n "alpha_vantage_wrapper") (v "0.2.4") (d (list (d (n "reqwest") (r "^0.11.22") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.189") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.33.0") (f (quote ("full"))) (d #t) (k 0)))) (h "195im5qcrrs0blri3xw9nxindb7ybnxm1jsi1d9ylprgnmm3gc14")))

