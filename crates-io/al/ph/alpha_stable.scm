(define-module (crates-io al ph alpha_stable) #:use-module (crates-io))

(define-public crate-alpha_stable-0.1.0 (c (n "alpha_stable") (v "0.1.0") (d (list (d (n "gkquad") (r "^0.0.4") (d #t) (k 0)) (d (n "plotters") (r "^0.3.3") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 0)) (d (n "spfunc") (r "^0.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)))) (h "1yjn3msw7k06qnbsp8vwlkpzsilazjgi418z8b6axz9s4yjzxgfl")))

(define-public crate-alpha_stable-0.1.1 (c (n "alpha_stable") (v "0.1.1") (d (list (d (n "gkquad") (r "^0.0.4") (d #t) (k 0)) (d (n "plotters") (r "^0.3.3") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 0)) (d (n "spfunc") (r "^0.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)))) (h "1vabc3hf4y546wiczmyg879lfwzl0wwjrdqrg15kvc1jv2h4bbvs")))

