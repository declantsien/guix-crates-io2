(define-module (crates-io al ph alpha-shell) #:use-module (crates-io))

(define-public crate-alpha-shell-0.2.1 (c (n "alpha-shell") (v "0.2.1") (d (list (d (n "clap") (r "^3.2.16") (f (quote ("derive"))) (d #t) (k 0)) (d (n "phf") (r "^0.11.0") (f (quote ("macros"))) (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "1alby8rifzvbg35pdxwccxqrr9gxyvhlfnvyr3xvi5jm8krn7j15")))

(define-public crate-alpha-shell-0.3.0 (c (n "alpha-shell") (v "0.3.0") (d (list (d (n "clap") (r "^3.2.16") (f (quote ("derive"))) (d #t) (k 0)) (d (n "phf") (r "^0.11.0") (f (quote ("macros"))) (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "0dly9g4zi31f7b42q785iwvp6mcch0dq6cy4zaindngkf2ldfa04")))

