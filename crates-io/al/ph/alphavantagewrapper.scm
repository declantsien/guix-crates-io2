(define-module (crates-io al ph alphavantagewrapper) #:use-module (crates-io))

(define-public crate-AlphaVantageWrapper-0.1.0 (c (n "AlphaVantageWrapper") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11.22") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.189") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.33.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0cnxr0s2z804g7kqrdrkhm5mm2lhv7f4blps0sivcdgyrizsskn2") (y #t)))

(define-public crate-AlphaVantageWrapper-0.1.1 (c (n "AlphaVantageWrapper") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.11.22") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.189") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.33.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1dzxrybp51mgsiyahwida24j1y8677a7a7b8nryy6bbxv9ip7m2y") (y #t)))

(define-public crate-AlphaVantageWrapper-0.1.3 (c (n "AlphaVantageWrapper") (v "0.1.3") (d (list (d (n "reqwest") (r "^0.11.22") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.189") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.33.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0j1y567bcqjbd5isb0wvq3vrv3b00g8ffl41lmirni8xbq45ql12") (y #t)))

(define-public crate-AlphaVantageWrapper-0.1.4 (c (n "AlphaVantageWrapper") (v "0.1.4") (d (list (d (n "reqwest") (r "^0.11.22") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.189") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.33.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0p5bq1plhc8qnjkbgw05k53prx08k3qj513kg7j5pnlg10dc04bs") (y #t)))

(define-public crate-AlphaVantageWrapper-0.2.0 (c (n "AlphaVantageWrapper") (v "0.2.0") (d (list (d (n "reqwest") (r "^0.11.22") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.189") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.33.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1zchs9mzgyi0fwzdg0r1lz9vvaai1mg71kzn9mclxrwv94vbxs9v") (y #t)))

