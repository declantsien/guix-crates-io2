(define-module (crates-io al ph alphabet) #:use-module (crates-io))

(define-public crate-alphabet-0.1.0 (c (n "alphabet") (v "0.1.0") (d (list (d (n "alphabet-macro") (r "^0.1") (d #t) (k 0)))) (h "1iskla5bn5ils96ixdwyb4hikd8cnh24094yv1hxy0gc0wafjzz6")))

(define-public crate-alphabet-0.1.1 (c (n "alphabet") (v "0.1.1") (d (list (d (n "alphabet-macro") (r "^0.1") (d #t) (k 0)))) (h "05lq25hchpi3zdbgyznx1vdzsmqvl1690df027p3d3i78ddl4cfy")))

(define-public crate-alphabet-0.2.0 (c (n "alphabet") (v "0.2.0") (d (list (d (n "alphabet-macro") (r "^0.1") (d #t) (k 0)) (d (n "more-asserts") (r "^0.2") (d #t) (k 0)))) (h "1m52xmpp570kmp9sfv3dx6v72382qxa7a1nh84c70kvz2lw2y8w3")))

(define-public crate-alphabet-0.2.1 (c (n "alphabet") (v "0.2.1") (d (list (d (n "alphabet-macro") (r "^0.1") (d #t) (k 0)) (d (n "more-asserts") (r "^0.2") (d #t) (k 0)))) (h "1fmksfgyw0qp3wha0jfvw852xngq9anfwsc3bvazxafrhw6mwiw9")))

(define-public crate-alphabet-0.2.2 (c (n "alphabet") (v "0.2.2") (d (list (d (n "alphabet-macro") (r "^0.1") (d #t) (k 0)) (d (n "more-asserts") (r "^0.2") (d #t) (k 0)))) (h "05c3fvphmi72a04kiygm48yzfj54nabkiriymrq5imqviaji2a8h")))

