(define-module (crates-io al ph alphred) #:use-module (crates-io))

(define-public crate-alphred-0.1.0 (c (n "alphred") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0i0jpxbb0bbk7ygflp39p6rznxqkai719dcwig8r7aqlgbcqk1fn")))

(define-public crate-alphred-0.1.1 (c (n "alphred") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "038pkvpvpcw2i0zdslcvshdc266jz02g1h3g93axj9n572y5dra9")))

(define-public crate-alphred-0.1.2 (c (n "alphred") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1rnjxc3a71dsb4rkmrkx71lf082nbbdf5g5s73vjsaqyh1v02rkx")))

(define-public crate-alphred-0.1.3 (c (n "alphred") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1cvlxigr80bs1lg1zwg40zpqna2f4b0crbh6jzjqad0y9i603l2l")))

(define-public crate-alphred-0.1.4 (c (n "alphred") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1ya4glbb1bzqcqxwz911gpjr5rdkzdg41xlrzg3d9mzzlnx4ivbd")))

(define-public crate-alphred-0.1.5 (c (n "alphred") (v "0.1.5") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1kdvkdngb5hfnbrpqsw2cq0413z9b3039264bvn48fabhh8bzyx3")))

(define-public crate-alphred-0.1.6 (c (n "alphred") (v "0.1.6") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1w74w8jw8b2rppppxbj4zcwjz4c93llsvi5g38mfr32z1xz1r52i")))

(define-public crate-alphred-0.1.7 (c (n "alphred") (v "0.1.7") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0dl0mbha39q6alkz08yrm5ijvv2jl2xw1i09zqwkjfz9pq3nmilx")))

(define-public crate-alphred-0.1.8 (c (n "alphred") (v "0.1.8") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "12yi7916jarhnx86vm46fbisks3abqlw872rf3baj18zgrqdb22b")))

(define-public crate-alphred-0.1.9 (c (n "alphred") (v "0.1.9") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "17bxjjshamlvnb0jjh29nd231f20ayik3l8w28gd2p6pjcy6lwxy")))

(define-public crate-alphred-0.1.10 (c (n "alphred") (v "0.1.10") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0jry9jbncz839v5k79bch88qxqsj4i75617jhm7q3n937sd4jqxw")))

(define-public crate-alphred-0.1.11 (c (n "alphred") (v "0.1.11") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0ijha4x8ydhzqqsvs1ad8yr70cv5kyvzax4sdcbgggy7bjkn6c8q")))

