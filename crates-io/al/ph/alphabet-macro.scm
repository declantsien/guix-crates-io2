(define-module (crates-io al ph alphabet-macro) #:use-module (crates-io))

(define-public crate-alphabet-macro-0.1.0 (c (n "alphabet-macro") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0qhjs914gx9ljx7b15z0nnm7sgr6z16icjv04k8jb1nwjdba9822")))

(define-public crate-alphabet-macro-0.1.1 (c (n "alphabet-macro") (v "0.1.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1n94z7vskcznr1lrn0y1j5f0ccrqpqpknawca1mmli7k5l58c28v")))

(define-public crate-alphabet-macro-0.1.2 (c (n "alphabet-macro") (v "0.1.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "060y1xdxlnf7vn45layyhbchdvhyx8iw3nqz3ykm6krrjd6z9fv8")))

