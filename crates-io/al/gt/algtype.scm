(define-module (crates-io al gt algtype) #:use-module (crates-io))

(define-public crate-algtype-0.1.0 (c (n "algtype") (v "0.1.0") (d (list (d (n "algtype_derive") (r "=0.1.0") (d #t) (k 0)) (d (n "proptest") (r "^1.4.0") (d #t) (k 2)) (d (n "proptest-derive") (r "^0.4.0") (d #t) (k 2)))) (h "11c02n4hs5hhhjll2q9fsy3gbh3fmpd0b39n9wnfw5ml85ia3nzg")))

