(define-module (crates-io al gt algtype_derive) #:use-module (crates-io))

(define-public crate-algtype_derive-0.1.0 (c (n "algtype_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "1.0.*") (d #t) (k 0)) (d (n "quote") (r "1.0.*") (d #t) (k 0)) (d (n "syn") (r "2.0.*") (d #t) (k 0)))) (h "0y7m4r2b65k94ggdzx1vrg0i7p9iwb415zkqy52zgrxa6j1w4rqw")))

