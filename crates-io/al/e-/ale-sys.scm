(define-module (crates-io al e- ale-sys) #:use-module (crates-io))

(define-public crate-ale-sys-0.0.1 (c (n "ale-sys") (v "0.0.1") (h "16ljrgry8kxn4s67hk6scj1zbgbvlzhgjsxla5mkvvj6i1f2g15z")))

(define-public crate-ale-sys-0.1.0 (c (n "ale-sys") (v "0.1.0") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0ayq8igxjanvml648mjh3alndcnc1k64j2aaihbvnrvfkw9yrvqa")))

(define-public crate-ale-sys-0.1.1 (c (n "ale-sys") (v "0.1.1") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 1)))) (h "09lr9wfjhl1vl1blnva01113yz7zp7nmxhkvjcbk3y6q141x66yc")))

(define-public crate-ale-sys-0.1.2 (c (n "ale-sys") (v "0.1.2") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 1)))) (h "0c0vkpq5bbd87chsprysf51q1mj629sqbpny09hy0s5hzya3mgwn")))

