(define-module (crates-io al ia alias-manager) #:use-module (crates-io))

(define-public crate-alias-manager-0.1.1 (c (n "alias-manager") (v "0.1.1") (d (list (d (n "home") (r "^0.5.3") (d #t) (k 0)))) (h "0mwb4shh9q0vj40x3ip6zsniwj494j2g8cjh1rjabj4xf355bx08") (y #t)))

(define-public crate-alias-manager-0.1.3 (c (n "alias-manager") (v "0.1.3") (d (list (d (n "home") (r "^0.5.3") (d #t) (k 0)))) (h "1nyck5lrk5jy1clmawpv3kfshfchn6hzsfb93p1abh95jb2knlh9") (y #t)))

(define-public crate-alias-manager-0.1.4 (c (n "alias-manager") (v "0.1.4") (d (list (d (n "home") (r "^0.5.3") (d #t) (k 0)))) (h "1q4kizfp95qg5ip01ixkc8hv5bk7fs8fc05hqchjjmnqwxf88jdp") (y #t)))

(define-public crate-alias-manager-0.1.5 (c (n "alias-manager") (v "0.1.5") (d (list (d (n "home") (r "^0.5.3") (d #t) (k 0)))) (h "1h4n384r7r97j7kvrs35sk13sn98csjb4h5hv10nwaqdy2swdphb") (y #t)))

