(define-module (crates-io al ia aliasable) #:use-module (crates-io))

(define-public crate-aliasable-0.1.0 (c (n "aliasable") (v "0.1.0") (d (list (d (n "aliasable_deref_trait") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.2") (o #t) (d #t) (k 0)))) (h "0fcyz60833w3wmbkcnkirdn1iyi4ij36la1671g0ndv2a0maxlbb") (f (quote (("traits" "stable_deref_trait" "aliasable_deref_trait") ("default"))))))

(define-public crate-aliasable-0.1.1 (c (n "aliasable") (v "0.1.1") (d (list (d (n "aliasable_deref_trait") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.2") (o #t) (d #t) (k 0)))) (h "0r4bkivrnjq1aw8nhhldqfq9j20pj6mzfsnqk3wilpli3a376sww") (f (quote (("traits" "stable_deref_trait" "aliasable_deref_trait") ("default"))))))

(define-public crate-aliasable-0.1.2 (c (n "aliasable") (v "0.1.2") (d (list (d (n "aliasable_deref_trait") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.2") (o #t) (d #t) (k 0)))) (h "04cyll1ivgfvwzfqj028xbvmxv1s969q42jp5qxmraysf5kg5m1m") (f (quote (("traits" "stable_deref_trait" "aliasable_deref_trait") ("default"))))))

(define-public crate-aliasable-0.1.3 (c (n "aliasable") (v "0.1.3") (d (list (d (n "aliasable_deref_trait") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.2") (o #t) (d #t) (k 0)))) (h "1z8548zdjlm4ps1k0d7x68lfdyji02crwcc9rw3q3bb106f643r5") (f (quote (("traits" "stable_deref_trait" "aliasable_deref_trait") ("default" "alloc") ("alloc"))))))

