(define-module (crates-io al ia alias_trie) #:use-module (crates-io))

(define-public crate-alias_trie-0.9.0 (c (n "alias_trie") (v "0.9.0") (d (list (d (n "regex") (r "^1.3") (d #t) (k 0)))) (h "0pcv31p37px6ia5fmgydz0lw3bscphgnvfb19k7np9chg2zfcy86")))

(define-public crate-alias_trie-0.9.1 (c (n "alias_trie") (v "0.9.1") (d (list (d (n "regex") (r "^1.3") (d #t) (k 0)))) (h "0adg0bl7v8sdj3l49mwd0m1d6xnkxqwlsxlrag1hqcbcyzhgr78j")))

