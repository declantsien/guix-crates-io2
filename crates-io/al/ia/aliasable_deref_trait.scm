(define-module (crates-io al ia aliasable_deref_trait) #:use-module (crates-io))

(define-public crate-aliasable_deref_trait-0.1.0 (c (n "aliasable_deref_trait") (v "0.1.0") (h "0wa067v9hkpcsxwvads1f2klnas2lw4v7igz1g02fbbvmjx0afp3") (f (quote (("std") ("default" "std") ("alloc"))))))

(define-public crate-aliasable_deref_trait-0.2.0 (c (n "aliasable_deref_trait") (v "0.2.0") (h "0kjfy3x9b0fmw586m7yr7ibkrs3xzg114244xdk714qn4jsx4wgw") (f (quote (("std") ("default" "std") ("alloc"))))))

(define-public crate-aliasable_deref_trait-0.2.1 (c (n "aliasable_deref_trait") (v "0.2.1") (h "0abzqsxd09jxl01brzjnwk8zg7k8zma76gzq87385q8fqm710jxb") (f (quote (("std") ("default" "std") ("alloc"))))))

(define-public crate-aliasable_deref_trait-1.0.0 (c (n "aliasable_deref_trait") (v "1.0.0") (h "10hr5zbiwhj5rl6r0lpcb5bashydjrbg9bfnwc1512q70qkvrx9y") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

