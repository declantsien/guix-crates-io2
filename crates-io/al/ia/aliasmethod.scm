(define-module (crates-io al ia aliasmethod) #:use-module (crates-io))

(define-public crate-aliasmethod-0.1.0 (c (n "aliasmethod") (v "0.1.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1pr5ibm97phfdqf4lf5m7ya1ar2gw28pgsjjx5dw2g0s0y5mymk5")))

(define-public crate-aliasmethod-0.2.0 (c (n "aliasmethod") (v "0.2.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0smyjcpyin0l0dcis2d108am6iwh4n4ja30xi8im8848qrw78kzc")))

(define-public crate-aliasmethod-0.3.0 (c (n "aliasmethod") (v "0.3.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1dlsmk0275gkz8p7wylpjkyd5b2h61msxc9yyz8b5ndvab4gasj5")))

(define-public crate-aliasmethod-0.4.0 (c (n "aliasmethod") (v "0.4.0") (d (list (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "1l8r205wsky4zi7iy468gmmyjd9qywryb5f7i4k15cg7983nksij")))

