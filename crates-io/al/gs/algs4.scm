(define-module (crates-io al gs algs4) #:use-module (crates-io))

(define-public crate-algs4-0.1.0 (c (n "algs4") (v "0.1.0") (d (list (d (n "rand") (r "*") (d #t) (k 0)))) (h "02y7hpnpzkp8q6vk9hq532sih1j9ssk6anm1h6j8317am1r5xwc8")))

(define-public crate-algs4-0.2.0 (c (n "algs4") (v "0.2.0") (d (list (d (n "rand") (r "*") (d #t) (k 0)))) (h "1b5kixa7x9w0m3hs8yhc2n65cpxszw34zfcnxckv79ab8vsh71mk")))

(define-public crate-algs4-0.3.0 (c (n "algs4") (v "0.3.0") (d (list (d (n "rand") (r "*") (d #t) (k 0)))) (h "1vzxchj9sjjg4190rdhnhzspdppbapvmy819cwwx5axl5h26wmam")))

(define-public crate-algs4-0.4.0 (c (n "algs4") (v "0.4.0") (d (list (d (n "rand") (r "*") (d #t) (k 0)))) (h "13mzisazww8z3a16lghx0548fziad7cv2fmw30m8bg845ibkcfbv")))

(define-public crate-algs4-0.5.0 (c (n "algs4") (v "0.5.0") (d (list (d (n "rand") (r "*") (d #t) (k 0)) (d (n "sdl2_gfx") (r "*") (d #t) (k 2)))) (h "1wpxz3d4r8v33wnamidyzxzzd7ixmyvmxwj3szpcjy9xnv52k04l")))

(define-public crate-algs4-0.6.0 (c (n "algs4") (v "0.6.0") (d (list (d (n "adivon") (r "*") (d #t) (k 0)) (d (n "byteorder") (r "*") (d #t) (k 0)) (d (n "mtl") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 0)) (d (n "sdl2") (r "*") (d #t) (k 2)) (d (n "sdl2_gfx") (r "*") (d #t) (k 2)))) (h "0n5wwb1ym3zbrmkq6zwr628ls241byg10f2c05kf1z48v28xkmkd")))

(define-public crate-algs4-0.7.0 (c (n "algs4") (v "0.7.0") (d (list (d (n "adivon") (r "^0.2") (d #t) (k 0)) (d (n "byteorder") (r "^0.3") (d #t) (k 0)) (d (n "mtl") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "sdl2") (r "*") (d #t) (k 2)) (d (n "sdl2_gfx") (r "*") (d #t) (k 2)))) (h "1z20swqs4zhnc6xjnk3f656h4sjiifzk9inxjfc33153gj16x6wd")))

