(define-module (crates-io al gs algs) #:use-module (crates-io))

(define-public crate-algs-0.1.0 (c (n "algs") (v "0.1.0") (h "0sf41bkpdybxpy8p2kmwkyxqfgh4p0h476fl6yidg2zxy5c6zpxn")))

(define-public crate-algs-0.1.1 (c (n "algs") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "funty") (r "^2.0.0") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "0nnv86xrxwq4k56i34kmh0scrx63qp7s8y4fg7ibp1p4z5qarklk")))

(define-public crate-algs-0.1.2 (c (n "algs") (v "0.1.2") (d (list (d (n "criterion") (r "^0.3.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "funty") (r "^2.0.0") (d #t) (k 0)))) (h "0v5dxgqmxxyxni9mxlp8vpp4ch5cd39j32jixj4615kk1z91jfsa")))

