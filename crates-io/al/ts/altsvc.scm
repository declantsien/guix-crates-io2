(define-module (crates-io al ts altsvc) #:use-module (crates-io))

(define-public crate-altsvc-0.1.0 (c (n "altsvc") (v "0.1.0") (h "01lcbxmazddlsfc7830wx467d7z5vpjx4jzsvphk94c544k90bjq")))

(define-public crate-altsvc-0.1.1 (c (n "altsvc") (v "0.1.1") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "15f3qx4c6ny21i4n7arkxraxk64jb7q2cy889a656hnkjsyfs5kx")))

(define-public crate-altsvc-0.1.2 (c (n "altsvc") (v "0.1.2") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1v95ilaaqzxp918jqibl087a8nvb2x9in367lvya374a805mqqrz")))

