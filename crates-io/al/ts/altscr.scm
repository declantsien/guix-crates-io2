(define-module (crates-io al ts altscr) #:use-module (crates-io))

(define-public crate-altscr-0.1.0 (c (n "altscr") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.56") (f (quote ("backtrace"))) (d #t) (k 0)) (d (n "clap") (r "^4.3.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.23.2") (d #t) (k 0)))) (h "0512z206n9py7h0p463yx0nmndkq061j8w192saim1pwhjn3g5kl")))

