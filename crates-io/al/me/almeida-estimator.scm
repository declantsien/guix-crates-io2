(define-module (crates-io al me almeida-estimator) #:use-module (crates-io))

(define-public crate-almeida-estimator-0.1.0 (c (n "almeida-estimator") (v "0.1.0") (d (list (d (n "nalgebra") (r "^0.30") (d #t) (k 0)) (d (n "ofps") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "02pl5m9b37dpxrgjs8lbi855h0fkwpdcd13838z9j02syzxkbqiq")))

