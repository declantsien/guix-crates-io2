(define-module (crates-io al li allium-daemon) #:use-module (crates-io))

(define-public crate-allium-daemon-0.1.3 (c (n "allium-daemon") (v "0.1.3") (d (list (d (n "allium") (r "^0.1.3") (d #t) (k 0)) (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bytes") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 0)) (d (n "rust-ini") (r "^0.16") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.8") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-stream") (r "^0.1") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "099w8dp225ir5dkdnhj8bk5q3gj5pzagnqw3fkwis9snxhsz3l4p")))

