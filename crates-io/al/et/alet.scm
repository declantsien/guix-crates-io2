(define-module (crates-io al et alet) #:use-module (crates-io))

(define-public crate-alet-0.1.0 (c (n "alet") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0b5v96mw043syrw64y80w1xhdff771xcb44llx63916lbxlm6ipp")))

(define-public crate-alet-0.1.1 (c (n "alet") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1d9xbci3l47hhprflw12qaqvjaxvjwjx9gxih2vby3w040cwz365")))

