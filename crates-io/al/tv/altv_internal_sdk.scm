(define-module (crates-io al tv altv_internal_sdk) #:use-module (crates-io))

(define-public crate-altv_internal_sdk-0.3.0 (c (n "altv_internal_sdk") (v "0.3.0") (d (list (d (n "autocxx") (r "^0.24.0") (d #t) (k 0)) (d (n "autocxx-build") (r "^0.24.0") (d #t) (k 1)) (d (n "cxx") (r "^1.0.91") (d #t) (k 0)) (d (n "miette") (r "^5") (f (quote ("fancy"))) (d #t) (k 1)) (d (n "primitive_enum") (r "^1.0.2") (d #t) (k 0)))) (h "1si6nx98fbkmpaj4yl8mjghm4qv63hapyhapnjnrdfqdnxbhlpmh") (y #t) (r "1.68.2")))

(define-public crate-altv_internal_sdk-0.3.1 (c (n "altv_internal_sdk") (v "0.3.1") (d (list (d (n "autocxx") (r "^0.24.0") (d #t) (k 0)) (d (n "autocxx-build") (r "^0.24.0") (d #t) (k 1)) (d (n "cxx") (r "^1.0.91") (d #t) (k 0)) (d (n "miette") (r "^5") (f (quote ("fancy"))) (d #t) (k 1)) (d (n "primitive_enum") (r "^1.0.2") (d #t) (k 0)))) (h "0ia8wbxjm9ssl4ywxx06ndydfjacrcqfw24nvr8w4vcr6zz27619") (y #t) (r "1.68.2")))

(define-public crate-altv_internal_sdk-15.0.0-dev.0 (c (n "altv_internal_sdk") (v "15.0.0-dev.0") (d (list (d (n "autocxx") (r "^0.24.0") (d #t) (k 0)) (d (n "autocxx-build") (r "^0.24.0") (d #t) (k 1)) (d (n "cxx") (r "^1.0.91") (d #t) (k 0)) (d (n "miette") (r "^5") (f (quote ("fancy"))) (d #t) (k 1)) (d (n "primitive_enum") (r "^1.0.2") (d #t) (k 0)))) (h "1hvhqwf84655cmjzn2i2mjkyzjwcb5v28661r1ind979b0rjs2gz") (y #t) (r "1.68.2")))

(define-public crate-altv_internal_sdk-15.0.0-dev.1 (c (n "altv_internal_sdk") (v "15.0.0-dev.1") (d (list (d (n "autocxx") (r "^0.24.0") (d #t) (k 0)) (d (n "autocxx-build") (r "^0.24.0") (d #t) (k 1)) (d (n "cxx") (r "^1.0.91") (d #t) (k 0)) (d (n "miette") (r "^5") (f (quote ("fancy"))) (d #t) (k 1)) (d (n "primitive_enum") (r "^1.0.2") (d #t) (k 0)))) (h "0zhwdd11dzfzawlwpx4m6dk48hjn8bwa66lpbl4dqwq860wzhwi5") (y #t) (r "1.68.2")))

(define-public crate-altv_internal_sdk-15.0.0-dev2 (c (n "altv_internal_sdk") (v "15.0.0-dev2") (d (list (d (n "autocxx") (r "^0.24.0") (d #t) (k 0)) (d (n "autocxx-build") (r "^0.24.0") (d #t) (k 1)) (d (n "cxx") (r "^1.0.91") (d #t) (k 0)) (d (n "miette") (r "^5") (f (quote ("fancy"))) (d #t) (k 1)) (d (n "primitive_enum") (r "^1.0.2") (d #t) (k 0)))) (h "03xp4ch623vw2knklslr3r423c0n9l8cjac7czg2wkq8wjfxpn1a") (y #t) (r "1.68.2")))

(define-public crate-altv_internal_sdk-15.0.0-dev3 (c (n "altv_internal_sdk") (v "15.0.0-dev3") (d (list (d (n "autocxx") (r "^0.24.0") (d #t) (k 0)) (d (n "autocxx-build") (r "^0.24.0") (d #t) (k 1)) (d (n "cxx") (r "^1.0.91") (d #t) (k 0)) (d (n "miette") (r "^5") (f (quote ("fancy"))) (d #t) (k 1)) (d (n "primitive_enum") (r "^1.0.2") (d #t) (k 0)))) (h "1ydmx6b0a04lhn70wgygaycjb3wpwcrfdqj509lsqzaa5m3zifxf") (y #t) (r "1.68.2")))

(define-public crate-altv_internal_sdk-15.0.0-dev4 (c (n "altv_internal_sdk") (v "15.0.0-dev4") (d (list (d (n "autocxx") (r "^0.24.0") (d #t) (k 0)) (d (n "autocxx-build") (r "^0.24.0") (d #t) (k 1)) (d (n "cxx") (r "^1.0.91") (d #t) (k 0)) (d (n "miette") (r "^5") (f (quote ("fancy"))) (d #t) (k 1)) (d (n "primitive_enum") (r "^1.0.2") (d #t) (k 0)))) (h "08l1nyz7m89lgpnam9fmdil00lx7c5plmk65wsrb5rykb2b4lkhw") (y #t) (r "1.68.2")))

(define-public crate-altv_internal_sdk-15.0.0-dev5 (c (n "altv_internal_sdk") (v "15.0.0-dev5") (d (list (d (n "autocxx") (r "^0.24.0") (d #t) (k 0)) (d (n "autocxx-build") (r "^0.24.0") (d #t) (k 1)) (d (n "cxx") (r "^1.0.91") (d #t) (k 0)) (d (n "miette") (r "^5") (f (quote ("fancy"))) (d #t) (k 1)) (d (n "primitive_enum") (r "^1.0.2") (d #t) (k 0)))) (h "10s72wc8hhcvq819mnzfnrlfxjcq8lawj9pigznb8alq4jqdg6qs") (y #t) (r "1.68.2")))

(define-public crate-altv_internal_sdk-15.0.0-dev6 (c (n "altv_internal_sdk") (v "15.0.0-dev6") (d (list (d (n "autocxx") (r "^0.24.0") (d #t) (k 0)) (d (n "autocxx-build") (r "^0.24.0") (d #t) (k 1)) (d (n "cxx") (r "^1.0.91") (d #t) (k 0)) (d (n "miette") (r "^5") (f (quote ("fancy"))) (d #t) (k 1)) (d (n "primitive_enum") (r "^1.0.2") (d #t) (k 0)))) (h "0sl0ms09xcd423qj2yp2356shqwcbb8fgg9mf0z17g4sqj1qzx24") (y #t) (r "1.68.2")))

(define-public crate-altv_internal_sdk-15.0.0-dev7 (c (n "altv_internal_sdk") (v "15.0.0-dev7") (d (list (d (n "autocxx") (r "^0.24.0") (d #t) (k 0)) (d (n "autocxx-build") (r "^0.24.0") (d #t) (k 1)) (d (n "cxx") (r "^1.0.91") (d #t) (k 0)) (d (n "miette") (r "^5") (f (quote ("fancy"))) (d #t) (k 1)) (d (n "primitive_enum") (r "^1.0.2") (d #t) (k 0)))) (h "11vmfywq3747azdyc24wf9ms6c6jygfvsmzv01a07n5mmj0s2zry") (y #t) (r "1.68.2")))

(define-public crate-altv_internal_sdk-15.0.0-dev8 (c (n "altv_internal_sdk") (v "15.0.0-dev8") (d (list (d (n "autocxx") (r "^0.24.0") (d #t) (k 0)) (d (n "autocxx-build") (r "^0.24.0") (d #t) (k 1)) (d (n "cxx") (r "^1.0.91") (d #t) (k 0)))) (h "0gsha72mh2xcfcl3dz1yxbxfsd87h5mjxa4nqg4bpdmrbb5i02pn") (y #t) (r "1.68.2")))

(define-public crate-altv_internal_sdk-15.0.0-dev9 (c (n "altv_internal_sdk") (v "15.0.0-dev9") (d (list (d (n "autocxx") (r "^0.24.0") (d #t) (k 0)) (d (n "autocxx-build") (r "^0.24.0") (d #t) (k 1)) (d (n "cxx") (r "^1.0.91") (d #t) (k 0)))) (h "1af0n2l36m7a3gyinpj8371fjvm6l9h5kdm2g0xdhprvimvxxgs1") (y #t) (r "1.68.2")))

(define-public crate-altv_internal_sdk-15.0.0-dev.10 (c (n "altv_internal_sdk") (v "15.0.0-dev.10") (d (list (d (n "autocxx") (r "^0.24.0") (d #t) (k 0)) (d (n "autocxx-build") (r "^0.24.0") (d #t) (k 1)) (d (n "cxx") (r "^1.0.91") (d #t) (k 0)))) (h "1bh10jvhxajpgcfra8yrcc73vrkzalr2b80fxsfpk4fldw6l1xdz") (r "1.68.2")))

(define-public crate-altv_internal_sdk-15.0.0-dev.11 (c (n "altv_internal_sdk") (v "15.0.0-dev.11") (d (list (d (n "autocxx") (r "^0.24.0") (d #t) (k 0)) (d (n "autocxx-build") (r "^0.24.0") (d #t) (k 1)) (d (n "cxx") (r "^1.0.91") (d #t) (k 0)))) (h "1n2wwv18l0j6p1dcy5fb8vsxhzlh17v3d3qjl0lchfcq6x19s3pj") (r "1.68.2")))

(define-public crate-altv_internal_sdk-15.0.0-dev.12 (c (n "altv_internal_sdk") (v "15.0.0-dev.12") (d (list (d (n "autocxx") (r "^0.24.0") (d #t) (k 0)) (d (n "autocxx-build") (r "^0.24.0") (d #t) (k 1)) (d (n "cxx") (r "^1.0.91") (d #t) (k 0)))) (h "1rxshlblyr2zf9dydmysc9gx4yhm8mwd4x0r4rkaj03kgqnbpniy") (r "1.68.2")))

(define-public crate-altv_internal_sdk-15.0.0-dev.13 (c (n "altv_internal_sdk") (v "15.0.0-dev.13") (d (list (d (n "autocxx") (r "^0.24.0") (d #t) (k 0)) (d (n "autocxx-build") (r "^0.24.0") (d #t) (k 1)) (d (n "cxx") (r "^1.0.91") (d #t) (k 0)))) (h "1lnynv8j6z6qyc6rfh864af8iwyjhkai9ciird3nqxqkys1j6393") (r "1.68.2")))

(define-public crate-altv_internal_sdk-15.0.0-dev.14 (c (n "altv_internal_sdk") (v "15.0.0-dev.14") (d (list (d (n "autocxx") (r "^0.24.0") (d #t) (k 0)) (d (n "autocxx-build") (r "^0.24.0") (d #t) (k 1)) (d (n "cxx") (r "^1.0.91") (d #t) (k 0)))) (h "0yq1f4s87hl0kzk4gg5jjh9cvhlbps6vzy1b71gqq0hz0ikj7hi5") (r "1.68.2")))

(define-public crate-altv_internal_sdk-15.0.0-dev.15 (c (n "altv_internal_sdk") (v "15.0.0-dev.15") (d (list (d (n "autocxx") (r "^0.24.0") (d #t) (k 0)) (d (n "autocxx-build") (r "^0.24.0") (d #t) (k 1)) (d (n "cxx") (r "^1.0.91") (d #t) (k 0)))) (h "14q4yjms69b7jwkvx7ag4h17hsy6jgpyg08gps2zk4mad3jbyv9c") (r "1.68.2")))

(define-public crate-altv_internal_sdk-15.0.0-dev.16 (c (n "altv_internal_sdk") (v "15.0.0-dev.16") (d (list (d (n "autocxx") (r "^0.24.0") (d #t) (k 0)) (d (n "autocxx-build") (r "^0.24.0") (d #t) (k 1)) (d (n "cxx") (r "^1.0.91") (d #t) (k 0)))) (h "1xn4smr1bg5kd5b3idbp4ql85jd3yvxmzfvfd89wdymgaiwd2zcm") (r "1.68.2")))

(define-public crate-altv_internal_sdk-15.0.0-dev.17 (c (n "altv_internal_sdk") (v "15.0.0-dev.17") (d (list (d (n "autocxx") (r "^0.24.0") (d #t) (k 0)) (d (n "autocxx-build") (r "^0.24.0") (d #t) (k 1)) (d (n "cxx") (r "^1.0.91") (d #t) (k 0)))) (h "0amnw4zdkar7x818bi2xyrslmhg6v4aa15yr5lnnyk6dg8sf5k0c") (r "1.68.2")))

(define-public crate-altv_internal_sdk-15.0.0-dev.18 (c (n "altv_internal_sdk") (v "15.0.0-dev.18") (d (list (d (n "autocxx") (r "^0.24.0") (d #t) (k 0)) (d (n "autocxx-build") (r "^0.24.0") (d #t) (k 1)) (d (n "cxx") (r "^1.0.91") (d #t) (k 0)))) (h "1hxs8857vy6qkmbyphvic9v5kvbb9z13k3003z6bxmc492nnc50f") (r "1.68.2")))

(define-public crate-altv_internal_sdk-15.0.0-dev.19 (c (n "altv_internal_sdk") (v "15.0.0-dev.19") (d (list (d (n "autocxx") (r "^0.24.0") (d #t) (k 0)) (d (n "autocxx-build") (r "^0.24.0") (d #t) (k 1)) (d (n "cxx") (r "^1.0.91") (d #t) (k 0)))) (h "0qfp4cjjaj9fwgj4rhl7nax51ly554pfjywfj27gnk1dkbdfzihr") (r "1.68.2")))

(define-public crate-altv_internal_sdk-15.0.0-dev.20 (c (n "altv_internal_sdk") (v "15.0.0-dev.20") (d (list (d (n "autocxx") (r "^0.24.0") (d #t) (k 0)) (d (n "autocxx-build") (r "^0.24.0") (d #t) (k 1)) (d (n "cxx") (r "^1.0.91") (d #t) (k 0)))) (h "0l061x7jm7hb29shcjqgfg3g3i59mxgqab9wzp6y941hrkq6c12l") (r "1.68.2")))

(define-public crate-altv_internal_sdk-15.0.0-dev.21 (c (n "altv_internal_sdk") (v "15.0.0-dev.21") (d (list (d (n "autocxx") (r "^0.24.0") (d #t) (k 0)) (d (n "autocxx-build") (r "^0.24.0") (d #t) (k 1)) (d (n "cxx") (r "^1.0.91") (d #t) (k 0)))) (h "1hj6r1y1d46mbakm90xxhcj7p7q9frgkvbrbz18fj2kpasyvvfbp") (r "1.68.2")))

(define-public crate-altv_internal_sdk-15.0.0-dev.22 (c (n "altv_internal_sdk") (v "15.0.0-dev.22") (d (list (d (n "autocxx") (r "^0.25.0") (d #t) (k 0)) (d (n "autocxx-build") (r "^0.25.0") (d #t) (k 1)) (d (n "cxx") (r "^1.0.91") (d #t) (k 0)))) (h "002l8y7n9qnxw2ayxs6mh7pjj9mk6zv2k10frpk2q8jdpx0lgmkn") (r "1.68.2")))

(define-public crate-altv_internal_sdk-15.0.0-dev.23 (c (n "altv_internal_sdk") (v "15.0.0-dev.23") (d (list (d (n "autocxx") (r "^0.25.0") (d #t) (k 0)) (d (n "autocxx-build") (r "^0.25.0") (d #t) (k 1)) (d (n "cxx") (r "^1.0.91") (d #t) (k 0)))) (h "18rwf9hcv9y51cjz9kjdnc825lr0ycqfm1i9wdda3mdhzy82kmaa") (r "1.68.2")))

(define-public crate-altv_internal_sdk-15.0.0-dev.24 (c (n "altv_internal_sdk") (v "15.0.0-dev.24") (d (list (d (n "autocxx") (r "^0.25.0") (d #t) (k 0)) (d (n "autocxx-build") (r "^0.25.0") (d #t) (k 1)) (d (n "cxx") (r "^1.0.91") (d #t) (k 0)))) (h "1y0v94kivvlq009ci7w46g6cwf8vxd056pwcf6j3xqddw64zp2pg") (r "1.68.2")))

(define-public crate-altv_internal_sdk-15.0.0-dev.25 (c (n "altv_internal_sdk") (v "15.0.0-dev.25") (d (list (d (n "autocxx") (r "^0.25.0") (d #t) (k 0)) (d (n "autocxx-build") (r "^0.25.0") (d #t) (k 1)) (d (n "cxx") (r "^1.0.91") (d #t) (k 0)))) (h "07sk37wydsrlvr55ac5w4gfyw9w38bk8qb3ym7iyhf3vslw6ryl7") (r "1.68.2")))

(define-public crate-altv_internal_sdk-15.0.0-dev.26 (c (n "altv_internal_sdk") (v "15.0.0-dev.26") (d (list (d (n "autocxx") (r "^0.25.0") (d #t) (k 0)) (d (n "autocxx-build") (r "^0.25.0") (d #t) (k 1)) (d (n "cxx") (r "^1.0.91") (d #t) (k 0)))) (h "061ayxwsvn9p1vxfpy532jp142shrx5zfqbxwnqbvij8yxx3cd9i") (r "1.68.2")))

(define-public crate-altv_internal_sdk-15.0.0-dev.27 (c (n "altv_internal_sdk") (v "15.0.0-dev.27") (d (list (d (n "autocxx") (r "^0.25.0") (d #t) (k 0)) (d (n "autocxx-build") (r "^0.25.0") (d #t) (k 1)) (d (n "cxx") (r "^1.0.91") (d #t) (k 0)))) (h "18847xjv99fk6wh4zk3bj4nrsyrzlsc8a5a605a2i4sjq8hpxkws") (r "1.68.2")))

(define-public crate-altv_internal_sdk-15.0.0-dev.28 (c (n "altv_internal_sdk") (v "15.0.0-dev.28") (d (list (d (n "autocxx") (r "^0.25.0") (d #t) (k 0)) (d (n "autocxx-build") (r "^0.25.0") (d #t) (k 1)) (d (n "cxx") (r "^1.0.91") (d #t) (k 0)))) (h "055mwnc40c31nvvffxkbjhh83y7a0kb7mcn4gbyn3nlpjqbgr5yd") (r "1.68.2")))

(define-public crate-altv_internal_sdk-15.0.0-dev.29 (c (n "altv_internal_sdk") (v "15.0.0-dev.29") (d (list (d (n "autocxx") (r "^0.25.0") (d #t) (k 0)) (d (n "autocxx-build") (r "^0.25.0") (d #t) (k 1)) (d (n "cxx") (r "^1.0.91") (d #t) (k 0)))) (h "12sm9hc8mwqgrz3vba9v27mwgxiq9f70wy8b25k5kgkph21xjpjl") (r "1.68.2")))

(define-public crate-altv_internal_sdk-15.0.0-dev.30 (c (n "altv_internal_sdk") (v "15.0.0-dev.30") (d (list (d (n "autocxx") (r "^0.25.0") (d #t) (k 0)) (d (n "autocxx-build") (r "^0.25.0") (d #t) (k 1)) (d (n "cxx") (r "^1.0.91") (d #t) (k 0)))) (h "1gvvnal1alvw1cqkkn5vb1k4x8yyi3k49nmpzg20bfm270d0bcrl") (r "1.68.2")))

(define-public crate-altv_internal_sdk-15.0.0-dev.31 (c (n "altv_internal_sdk") (v "15.0.0-dev.31") (d (list (d (n "autocxx") (r "^0.25.0") (d #t) (k 0)) (d (n "autocxx-build") (r "^0.25.0") (d #t) (k 1)) (d (n "cxx") (r "^1.0.91") (d #t) (k 0)))) (h "1i30gnkqlxvm34pg5nq4wpizmq24kfyfcv6s3rfx0chyyrkn6pzy") (r "1.68.2")))

(define-public crate-altv_internal_sdk-15.0.0-dev.32 (c (n "altv_internal_sdk") (v "15.0.0-dev.32") (d (list (d (n "autocxx") (r "^0.25.0") (d #t) (k 0)) (d (n "autocxx-build") (r "^0.25.0") (d #t) (k 1)) (d (n "cxx") (r "^1.0.91") (d #t) (k 0)))) (h "02sbznn74ajq9nq9dsvkfg0137fs6291l2wx1xcrgsj7ykann330") (r "1.68.2")))

(define-public crate-altv_internal_sdk-15.0.0-dev.33 (c (n "altv_internal_sdk") (v "15.0.0-dev.33") (d (list (d (n "autocxx") (r "^0.25.0") (d #t) (k 0)) (d (n "autocxx-build") (r "^0.25.0") (d #t) (k 1)) (d (n "cxx") (r "^1.0.91") (d #t) (k 0)))) (h "144mmr0q5bf5bzmpajwx480rnvrrjwyqcd9lnygz0b90jhsvvbdn") (r "1.68.2")))

(define-public crate-altv_internal_sdk-15.0.0-dev.34 (c (n "altv_internal_sdk") (v "15.0.0-dev.34") (d (list (d (n "autocxx") (r "^0.25.0") (d #t) (k 0)) (d (n "autocxx-build") (r "^0.25.0") (d #t) (k 1)) (d (n "cxx") (r "^1.0.91") (d #t) (k 0)))) (h "15l9qpf8fxgg73a5z19hqjn0ynl2yv51gmpa19li032x5rkhn1bd") (r "1.68.2")))

(define-public crate-altv_internal_sdk-15.0.0-dev.35 (c (n "altv_internal_sdk") (v "15.0.0-dev.35") (d (list (d (n "autocxx") (r "^0.25.0") (d #t) (k 0)) (d (n "autocxx-build") (r "^0.25.0") (d #t) (k 1)) (d (n "cxx") (r "^1.0.91") (d #t) (k 0)))) (h "1bsh8jf25y804p70lb41q19fcpw71difdj65mm7z1s6kcs3ik5jr") (r "1.68.2")))

(define-public crate-altv_internal_sdk-15.0.0-dev.36 (c (n "altv_internal_sdk") (v "15.0.0-dev.36") (d (list (d (n "autocxx") (r "^0.25.0") (d #t) (k 0)) (d (n "autocxx-build") (r "^0.25.0") (d #t) (k 1)) (d (n "cxx") (r "^1.0.91") (d #t) (k 0)))) (h "0vfyncidad9k8fnkzc583k9yv2nz7s890n0j4qf7r7lxpkagdi7j") (r "1.68.2")))

(define-public crate-altv_internal_sdk-15.0.0-dev.37 (c (n "altv_internal_sdk") (v "15.0.0-dev.37") (d (list (d (n "autocxx") (r "^0.25.0") (d #t) (k 0)) (d (n "autocxx-build") (r "^0.25.0") (d #t) (k 1)) (d (n "cxx") (r "^1.0.91") (d #t) (k 0)))) (h "1d13cyfl17svhfxljfn1prh7a7r6jh5i5gv18qcccf3fxlyp9rkj") (r "1.68.2")))

(define-public crate-altv_internal_sdk-15.0.0-dev.38 (c (n "altv_internal_sdk") (v "15.0.0-dev.38") (d (list (d (n "autocxx") (r "^0.25.0") (d #t) (k 0)) (d (n "autocxx-build") (r "^0.25.0") (d #t) (k 1)) (d (n "cxx") (r "^1.0.91") (d #t) (k 0)))) (h "1d1drkmlf559qkvsfj5v97wpw90rzdlwf2drs2m6161c0462r50k") (r "1.68.2")))

(define-public crate-altv_internal_sdk-15.0.0-dev.39 (c (n "altv_internal_sdk") (v "15.0.0-dev.39") (d (list (d (n "autocxx") (r "^0.25.0") (d #t) (k 0)) (d (n "autocxx-build") (r "^0.25.0") (d #t) (k 1)) (d (n "cxx") (r "^1.0.91") (d #t) (k 0)))) (h "1j7adbxdj1c00xz1qrhr4pf03frvf9cp9y6k2kn2y7y44y8h8p3i") (r "1.68.2")))

(define-public crate-altv_internal_sdk-15.0.0-dev.40 (c (n "altv_internal_sdk") (v "15.0.0-dev.40") (d (list (d (n "autocxx") (r "^0.25.0") (d #t) (k 0)) (d (n "autocxx-build") (r "^0.25.0") (d #t) (k 1)) (d (n "cxx") (r "^1.0.91") (d #t) (k 0)))) (h "12fxmlyp0s7zdk63k8vn66bf3lvz2vnnhhb1pbv1crlydcr1d8x2") (r "1.68.2")))

(define-public crate-altv_internal_sdk-15.0.0-dev.41 (c (n "altv_internal_sdk") (v "15.0.0-dev.41") (d (list (d (n "autocxx") (r "^0.25.0") (d #t) (k 0)) (d (n "autocxx-build") (r "^0.25.0") (d #t) (k 1)) (d (n "cxx") (r "^1.0.91") (d #t) (k 0)))) (h "17cbzndc24m9rj9q1w9flq8kfxn9maal2khfikkp4sj96il00dc8") (r "1.68.2")))

(define-public crate-altv_internal_sdk-15.0.0-dev42 (c (n "altv_internal_sdk") (v "15.0.0-dev42") (d (list (d (n "autocxx") (r "^0.25.0") (d #t) (k 0)) (d (n "autocxx-build") (r "^0.25.0") (d #t) (k 1)) (d (n "cxx") (r "^1.0.91") (d #t) (k 0)))) (h "0hynk0l45x87x9hhbfblybjakpciir2zsw54ip5r73b2q1b7ay77") (y #t) (r "1.68.2")))

(define-public crate-altv_internal_sdk-15.0.0-dev.43 (c (n "altv_internal_sdk") (v "15.0.0-dev.43") (d (list (d (n "autocxx") (r "^0.25.0") (d #t) (k 0)) (d (n "autocxx-build") (r "^0.25.0") (d #t) (k 1)) (d (n "cxx") (r "^1.0.91") (d #t) (k 0)))) (h "0mjrdsbfqyvvqxfxqxzhvwdkav124zzy2pdcwz6yyhqhzq3cqwrw") (y #t) (r "1.68.2")))

(define-public crate-altv_internal_sdk-15.0.0-dev.44 (c (n "altv_internal_sdk") (v "15.0.0-dev.44") (d (list (d (n "autocxx") (r "^0.25.0") (d #t) (k 0)) (d (n "autocxx-build") (r "^0.25.0") (d #t) (k 1)) (d (n "cxx") (r "^1.0.91") (d #t) (k 0)))) (h "1fwd9nwgfrm6zklkxbqsqji2aq4m6dnnr4v0cvf3c0ggzcvki8i9") (y #t) (r "1.68.2")))

(define-public crate-altv_internal_sdk-15.0.0-dev.45 (c (n "altv_internal_sdk") (v "15.0.0-dev.45") (d (list (d (n "autocxx") (r "^0.25.0") (d #t) (k 0)) (d (n "autocxx-build") (r "^0.25.0") (d #t) (k 1)) (d (n "cxx") (r "^1.0.91") (d #t) (k 0)))) (h "0ly2isfa1f8h87l9dzjbgcxw35cg9s275y3k572pm4q05hh6vj5r") (r "1.68.2")))

(define-public crate-altv_internal_sdk-15.0.0-dev.46 (c (n "altv_internal_sdk") (v "15.0.0-dev.46") (d (list (d (n "autocxx") (r "^0.25.0") (d #t) (k 0)) (d (n "autocxx-build") (r "^0.25.0") (d #t) (k 1)) (d (n "cxx") (r "^1.0.91") (d #t) (k 0)))) (h "1bkxkz70a9q4xwrwbx1f49bb41y3rgnb8sr3kg2l4zxmb3ksxb6f") (r "1.68.2")))

(define-public crate-altv_internal_sdk-15.0.0-dev.47 (c (n "altv_internal_sdk") (v "15.0.0-dev.47") (d (list (d (n "autocxx") (r "^0.25.0") (d #t) (k 0)) (d (n "autocxx-build") (r "^0.25.0") (d #t) (k 1)) (d (n "cxx") (r "^1.0.91") (d #t) (k 0)))) (h "1pwf5v63cm2xbw9swq6b890d6i295rzc290zqyrv876k146bqlzm") (r "1.68.2")))

(define-public crate-altv_internal_sdk-15.0.0-dev.48 (c (n "altv_internal_sdk") (v "15.0.0-dev.48") (d (list (d (n "autocxx") (r "^0.25.0") (d #t) (k 0)) (d (n "autocxx-build") (r "^0.25.0") (d #t) (k 1)) (d (n "cxx") (r "^1.0.91") (d #t) (k 0)))) (h "1nm36jac75rhdz3a53vdcc5159857ddd63i9q18rlbcy5r10lilp") (r "1.68.2")))

(define-public crate-altv_internal_sdk-15.0.0-dev.49 (c (n "altv_internal_sdk") (v "15.0.0-dev.49") (d (list (d (n "autocxx") (r "^0.25.0") (d #t) (k 0)) (d (n "autocxx-build") (r "^0.25.0") (d #t) (k 1)) (d (n "cxx") (r "^1.0.91") (d #t) (k 0)))) (h "127g5nnnx3w6xfwi7wv0sv5rci8jh73wg3rjxmaqnrfqshybg40f") (r "1.68.2")))

(define-public crate-altv_internal_sdk-15.0.0-dev.50 (c (n "altv_internal_sdk") (v "15.0.0-dev.50") (d (list (d (n "autocxx") (r "^0.25.0") (d #t) (k 0)) (d (n "autocxx-build") (r "^0.25.0") (d #t) (k 1)) (d (n "cxx") (r "^1.0.91") (d #t) (k 0)))) (h "0y3ic7k9g5niz5ffz6i35h7lffbwgmlkxc75m35bhglba9ks4p8y") (r "1.68.2")))

(define-public crate-altv_internal_sdk-15.0.0-dev.51 (c (n "altv_internal_sdk") (v "15.0.0-dev.51") (d (list (d (n "autocxx") (r "^0.25.0") (d #t) (k 0)) (d (n "autocxx-build") (r "^0.25.0") (d #t) (k 1)) (d (n "cxx") (r "^1.0.91") (d #t) (k 0)))) (h "0ibl6qlkjcdwm3mcvid4m0w7zr1kqfgygbgfzv3kxas1an4qid7l") (r "1.68.2")))

(define-public crate-altv_internal_sdk-15.0.0-dev.52 (c (n "altv_internal_sdk") (v "15.0.0-dev.52") (d (list (d (n "autocxx") (r "^0.25.0") (d #t) (k 0)) (d (n "autocxx-build") (r "^0.25.0") (d #t) (k 1)) (d (n "cxx") (r "^1.0.91") (d #t) (k 0)))) (h "0srj5mp78bzyhyagl3byrkkqa1q9hmd6isc30sc2m3q7sjfcblhr") (r "1.68.2")))

(define-public crate-altv_internal_sdk-15.0.0-dev.53 (c (n "altv_internal_sdk") (v "15.0.0-dev.53") (d (list (d (n "autocxx") (r "^0.25.0") (d #t) (k 0)) (d (n "autocxx-build") (r "^0.25.0") (d #t) (k 1)) (d (n "cxx") (r "^1.0.91") (d #t) (k 0)))) (h "1np0ig4slpl4c5flk72d6vbzxd2harwhiq8llvv5rg28mbhbpcla") (r "1.68.2")))

(define-public crate-altv_internal_sdk-15.0.0-dev.54 (c (n "altv_internal_sdk") (v "15.0.0-dev.54") (d (list (d (n "autocxx") (r "^0.25.0") (d #t) (k 0)) (d (n "autocxx-build") (r "^0.25.0") (d #t) (k 1)) (d (n "cxx") (r "^1.0.91") (d #t) (k 0)))) (h "1snzcvx57i1zbagnlgwwvlknsi52c2yi87kfan1k629iszvighpk") (r "1.68.2")))

(define-public crate-altv_internal_sdk-15.0.0-dev.55 (c (n "altv_internal_sdk") (v "15.0.0-dev.55") (d (list (d (n "autocxx") (r "^0.25.0") (d #t) (k 0)) (d (n "autocxx-build") (r "^0.25.0") (d #t) (k 1)) (d (n "cxx") (r "^1.0.91") (d #t) (k 0)))) (h "0jgxsym0yxflh97j105w1v135ywyl5pgyy6n3dd6xzppf1q5vl40") (r "1.68.2")))

(define-public crate-altv_internal_sdk-15.0.0-dev.56 (c (n "altv_internal_sdk") (v "15.0.0-dev.56") (d (list (d (n "autocxx") (r "^0.25.0") (d #t) (k 0)) (d (n "autocxx-build") (r "^0.25.0") (d #t) (k 1)) (d (n "cxx") (r "^1.0.91") (d #t) (k 0)))) (h "0p2b1f66dsgvr4gw9y3ziaisas3vczady3k6dv0jcx11qrn0c4l7") (r "1.68.2")))

(define-public crate-altv_internal_sdk-15.0.0-dev.57 (c (n "altv_internal_sdk") (v "15.0.0-dev.57") (d (list (d (n "autocxx") (r "^0.25.0") (d #t) (k 0)) (d (n "autocxx-build") (r "^0.25.0") (d #t) (k 1)) (d (n "cxx") (r "^1.0.91") (d #t) (k 0)))) (h "0l4y1r872b3f1gj5mcyzgbqk7caml44h0whn3g0npcw77va8via5") (r "1.68.2")))

(define-public crate-altv_internal_sdk-15.0.0-dev.58 (c (n "altv_internal_sdk") (v "15.0.0-dev.58") (d (list (d (n "autocxx") (r "^0.25.0") (d #t) (k 0)) (d (n "autocxx-build") (r "^0.25.0") (d #t) (k 1)) (d (n "cxx") (r "^1.0.91") (d #t) (k 0)))) (h "1fqjijf85kal3rvwfdp1z73s00hz19cxa5hl874if9cjk5larbxd") (r "1.68.2")))

(define-public crate-altv_internal_sdk-15.0.0-dev.59 (c (n "altv_internal_sdk") (v "15.0.0-dev.59") (d (list (d (n "autocxx") (r "^0.25.0") (d #t) (k 0)) (d (n "autocxx-build") (r "^0.25.0") (d #t) (k 1)) (d (n "cxx") (r "^1.0.91") (d #t) (k 0)))) (h "1grap96j959ng3vhadyrk5yj0g0lg7pk9ha4bb0a27p3z838rqzf") (r "1.68.2")))

(define-public crate-altv_internal_sdk-15.0.0-dev.60 (c (n "altv_internal_sdk") (v "15.0.0-dev.60") (d (list (d (n "autocxx") (r "^0.25.0") (d #t) (k 0)) (d (n "autocxx-build") (r "^0.25.0") (d #t) (k 1)) (d (n "cxx") (r "^1.0.91") (d #t) (k 0)))) (h "1yzlwv097l5sknm13fb8922kadlqrxcw4nblvx6c0cldpx75i2ci") (r "1.68.2")))

(define-public crate-altv_internal_sdk-15.0.0-dev.61 (c (n "altv_internal_sdk") (v "15.0.0-dev.61") (d (list (d (n "autocxx") (r "^0.25.0") (d #t) (k 0)) (d (n "autocxx-build") (r "^0.25.0") (d #t) (k 1)) (d (n "cxx") (r "^1.0.91") (d #t) (k 0)))) (h "0p53jfbwpmnn82b1fgvg89945ywzzpgw9f71wrqaqy1ch16zkxxf") (r "1.68.2")))

(define-public crate-altv_internal_sdk-15.0.0-dev.62 (c (n "altv_internal_sdk") (v "15.0.0-dev.62") (d (list (d (n "autocxx") (r "^0.25.0") (d #t) (k 0)) (d (n "autocxx-build") (r "^0.25.0") (d #t) (k 1)) (d (n "cxx") (r "^1.0.91") (d #t) (k 0)))) (h "1dbm2mbfv5x01qjxc6ai419if8knflgy3vvgr9kb3sr7pd0qz8ga") (r "1.68.2")))

(define-public crate-altv_internal_sdk-15.0.0-rc.1 (c (n "altv_internal_sdk") (v "15.0.0-rc.1") (d (list (d (n "autocxx") (r "^0.25.0") (d #t) (k 0)) (d (n "autocxx-build") (r "^0.25.0") (d #t) (k 1)) (d (n "cxx") (r "^1.0.91") (d #t) (k 0)))) (h "1kf06jwq5glcswfwxyqf9nmmpxh9m4r3am0ddlrla2rqa953q1w8") (r "1.68.2")))

(define-public crate-altv_internal_sdk-15.0.0-dev.63 (c (n "altv_internal_sdk") (v "15.0.0-dev.63") (d (list (d (n "autocxx") (r "^0.25.0") (d #t) (k 0)) (d (n "autocxx-build") (r "^0.25.0") (d #t) (k 1)) (d (n "cxx") (r "^1.0.91") (d #t) (k 0)))) (h "1p21fnkm1aykp025ni56bijxk65wa15l69g5846w471r5vy2nnpy") (r "1.68.2")))

(define-public crate-altv_internal_sdk-15.0.0-dev.64 (c (n "altv_internal_sdk") (v "15.0.0-dev.64") (d (list (d (n "autocxx") (r "^0.25.0") (d #t) (k 0)) (d (n "autocxx-build") (r "^0.25.0") (d #t) (k 1)) (d (n "cxx") (r "^1.0.91") (d #t) (k 0)))) (h "0fm3pxj91d6g0imnigk3qjpk40nryjhm2lwhflycq3qjy5q7jlm2") (r "1.68.2")))

(define-public crate-altv_internal_sdk-15.0.0-dev.65 (c (n "altv_internal_sdk") (v "15.0.0-dev.65") (d (list (d (n "autocxx") (r "^0.25.0") (d #t) (k 0)) (d (n "autocxx-build") (r "^0.25.0") (d #t) (k 1)) (d (n "cxx") (r "^1.0.91") (d #t) (k 0)))) (h "02z1v3mwi8bnmj56xwplgc1vnakjbbrzdmhdgwk5nr0ddkxk1ni9") (r "1.68.2")))

(define-public crate-altv_internal_sdk-15.0.0-dev.66 (c (n "altv_internal_sdk") (v "15.0.0-dev.66") (d (list (d (n "autocxx") (r "^0.25.0") (d #t) (k 0)) (d (n "autocxx-build") (r "^0.25.0") (d #t) (k 1)) (d (n "cxx") (r "^1.0.91") (d #t) (k 0)))) (h "0qz07m0wcj5w3b0lb9gmlsqa7f30n1iw3wr6xvw30k76wl7vkmpy") (r "1.68.2")))

(define-public crate-altv_internal_sdk-15.0.0-dev.67 (c (n "altv_internal_sdk") (v "15.0.0-dev.67") (d (list (d (n "autocxx") (r "^0.25.0") (d #t) (k 0)) (d (n "autocxx-build") (r "^0.25.0") (d #t) (k 1)) (d (n "cxx") (r "^1.0.91") (d #t) (k 0)))) (h "1cbsmh05byqsnsswfawb2k16rl1g7ijfa7lbqd16189f40fk1ba4") (r "1.68.2")))

(define-public crate-altv_internal_sdk-15.0.0-rc.2 (c (n "altv_internal_sdk") (v "15.0.0-rc.2") (d (list (d (n "autocxx") (r "^0.25.0") (d #t) (k 0)) (d (n "autocxx-build") (r "^0.25.0") (d #t) (k 1)) (d (n "cxx") (r "^1.0.91") (d #t) (k 0)))) (h "0av4lkc14iiiimzgz4d678gwg9im1jkqg1ssmq2jpbk381yyz9ai") (r "1.68.2")))

(define-public crate-altv_internal_sdk-15.0.0-dev.68 (c (n "altv_internal_sdk") (v "15.0.0-dev.68") (d (list (d (n "autocxx") (r "^0.25.0") (d #t) (k 0)) (d (n "autocxx-build") (r "^0.25.0") (d #t) (k 1)) (d (n "cxx") (r "^1.0.91") (d #t) (k 0)))) (h "1nd714znaq9ggqpnl7bsm73bl4jrj1mi7ykmiqbvgg8c3j0rhdxs") (r "1.68.2")))

(define-public crate-altv_internal_sdk-15.0.0-dev.69 (c (n "altv_internal_sdk") (v "15.0.0-dev.69") (d (list (d (n "autocxx") (r "^0.25.0") (d #t) (k 0)) (d (n "autocxx-build") (r "^0.25.0") (d #t) (k 1)) (d (n "cxx") (r "^1.0.91") (d #t) (k 0)))) (h "1w3fq5dlgapvj6vyhi6s1r99a496zqrdpmy4bsfpxq9a5zk79k9x") (r "1.68.2")))

(define-public crate-altv_internal_sdk-15.0.0-rc.3 (c (n "altv_internal_sdk") (v "15.0.0-rc.3") (d (list (d (n "autocxx") (r "^0.25.0") (d #t) (k 0)) (d (n "autocxx-build") (r "^0.25.0") (d #t) (k 1)) (d (n "cxx") (r "^1.0.91") (d #t) (k 0)))) (h "1628n0c82vw3ynzgr64xzsfvg5m1w2y3mmjyn5240921d79z5w9k") (r "1.68.2")))

(define-public crate-altv_internal_sdk-15.0.0-dev.70 (c (n "altv_internal_sdk") (v "15.0.0-dev.70") (d (list (d (n "autocxx") (r "^0.25.0") (d #t) (k 0)) (d (n "autocxx-build") (r "^0.25.0") (d #t) (k 1)) (d (n "cxx") (r "^1.0.91") (d #t) (k 0)))) (h "0nl9n8c7i6y2p955qvpnv6axk3sxig3mda6n1hfk6lw0l8d43d35") (r "1.68.2")))

(define-public crate-altv_internal_sdk-15.0.0-rc.4 (c (n "altv_internal_sdk") (v "15.0.0-rc.4") (d (list (d (n "autocxx") (r "^0.25.0") (d #t) (k 0)) (d (n "autocxx-build") (r "^0.25.0") (d #t) (k 1)) (d (n "cxx") (r "^1.0.91") (d #t) (k 0)))) (h "1xs7bc67vjjj8k2w62dl4y8nfm7azrdlscv3sv31chzqlzdpxd5i") (r "1.68.2")))

(define-public crate-altv_internal_sdk-15.0.0-dev.71 (c (n "altv_internal_sdk") (v "15.0.0-dev.71") (d (list (d (n "autocxx") (r "^0.25.0") (d #t) (k 0)) (d (n "autocxx-build") (r "^0.25.0") (d #t) (k 1)) (d (n "cxx") (r "^1.0.91") (d #t) (k 0)))) (h "0xd4wpx6a94nx4ypnlavsyafbfnnc79yl82xrqb34rc8jjnq2rc7") (r "1.68.2")))

(define-public crate-altv_internal_sdk-15.0.0-dev.72 (c (n "altv_internal_sdk") (v "15.0.0-dev.72") (d (list (d (n "autocxx") (r "^0.25.0") (d #t) (k 0)) (d (n "autocxx-build") (r "^0.25.0") (d #t) (k 1)) (d (n "cxx") (r "^1.0.91") (d #t) (k 0)))) (h "0bljsri4vl556hwsb01j9nc4051nqh2xi602zvaxjbzb7s9a1mkl") (r "1.68.2")))

(define-public crate-altv_internal_sdk-15.0.0-dev.73 (c (n "altv_internal_sdk") (v "15.0.0-dev.73") (d (list (d (n "autocxx") (r "^0.26.0") (d #t) (k 0)) (d (n "autocxx-build") (r "^0.25.0") (d #t) (k 1)) (d (n "cxx") (r "^1.0.91") (d #t) (k 0)))) (h "0qz4pxydmvjgw872vr5m9pqi4s12qq8nv6bjky4wvk0x1as8s5nf") (r "1.68.2")))

(define-public crate-altv_internal_sdk-15.0.0-dev.74 (c (n "altv_internal_sdk") (v "15.0.0-dev.74") (d (list (d (n "autocxx") (r "^0.26.0") (d #t) (k 0)) (d (n "autocxx-build") (r "^0.25.0") (d #t) (k 1)) (d (n "cxx") (r "^1.0.91") (d #t) (k 0)))) (h "0mvc1q9ji8ngfpvblp8lajhg5c1z2nva0rjqs6indb12adh30wg8") (r "1.68.2")))

(define-public crate-altv_internal_sdk-15.0.0-dev.75 (c (n "altv_internal_sdk") (v "15.0.0-dev.75") (d (list (d (n "autocxx") (r "^0.26.0") (d #t) (k 0)) (d (n "autocxx-build") (r "^0.25.0") (d #t) (k 1)) (d (n "cxx") (r "^1.0.91") (d #t) (k 0)))) (h "02h9cwxycb6pnxgfw14qq8ah3zsrv755v4mg5ysx655gq3bc3wgv") (r "1.68.2")))

(define-public crate-altv_internal_sdk-15.0.0-rc.5 (c (n "altv_internal_sdk") (v "15.0.0-rc.5") (d (list (d (n "autocxx") (r "^0.26.0") (d #t) (k 0)) (d (n "autocxx-build") (r "^0.25.0") (d #t) (k 1)) (d (n "cxx") (r "^1.0.91") (d #t) (k 0)))) (h "0rw8w8y9gcp75z6pizf7z9pndz983c6pza76qw5xhv5g02vb0n3p") (r "1.73.0")))

(define-public crate-altv_internal_sdk-15.0.0-rc.6 (c (n "altv_internal_sdk") (v "15.0.0-rc.6") (d (list (d (n "autocxx") (r "^0.26.0") (d #t) (k 0)) (d (n "autocxx-build") (r "^0.25.0") (d #t) (k 1)) (d (n "cxx") (r "^1.0.91") (d #t) (k 0)))) (h "1g8xa82l4yqncj24bc05awwdvwzrf716qk0f3ka6q7x9jmi7vlky") (r "1.73.0")))

(define-public crate-altv_internal_sdk-15.0.0-dev.76 (c (n "altv_internal_sdk") (v "15.0.0-dev.76") (d (list (d (n "autocxx") (r "^0.26.0") (d #t) (k 0)) (d (n "autocxx-build") (r "^0.25.0") (d #t) (k 1)) (d (n "cxx") (r "^1.0.91") (d #t) (k 0)))) (h "0w7kd5zw1ms78hz8khkfh1ral2mib5bvwrgcvz6pss63zs49r1h6") (r "1.73.0")))

(define-public crate-altv_internal_sdk-15.0.0 (c (n "altv_internal_sdk") (v "15.0.0") (d (list (d (n "autocxx") (r "^0.26.0") (d #t) (k 0)) (d (n "autocxx-build") (r "^0.25.0") (d #t) (k 1)) (d (n "cxx") (r "^1.0.91") (d #t) (k 0)))) (h "046s5c8g3zfphzw26c86mxzpnyh4b4yw19sx0qgx31b9bldm235p") (r "1.73.0")))

(define-public crate-altv_internal_sdk-15.0.1 (c (n "altv_internal_sdk") (v "15.0.1") (d (list (d (n "autocxx") (r "^0.26.0") (d #t) (k 0)) (d (n "autocxx-build") (r "^0.25.0") (d #t) (k 1)) (d (n "cxx") (r "^1.0.91") (d #t) (k 0)))) (h "0873yrdxfpr5fywcgkfam8gphyyxrdccqfi1dsw3f0lwx8liysgy") (r "1.73.0")))

(define-public crate-altv_internal_sdk-15.1.0 (c (n "altv_internal_sdk") (v "15.1.0") (d (list (d (n "autocxx") (r "^0.26.0") (d #t) (k 0)) (d (n "autocxx-build") (r "^0.25.0") (d #t) (k 1)) (d (n "cxx") (r "^1.0.91") (d #t) (k 0)))) (h "0m2q8m1dwr9m2lcap0f2d98w4776x7kz223vvbqg3y45gc4r1k9v") (r "1.73.0")))

(define-public crate-altv_internal_sdk-16.0.0-dev.1 (c (n "altv_internal_sdk") (v "16.0.0-dev.1") (d (list (d (n "autocxx") (r "^0.26.0") (d #t) (k 0)) (d (n "autocxx-build") (r "^0.25.0") (d #t) (k 1)) (d (n "cxx") (r "^1.0.91") (d #t) (k 0)))) (h "1mwy49b4wwqghnxz7q2y5xcsqwwj5687wd64bxvq7lwlh0asr7c4") (r "1.73.0")))

(define-public crate-altv_internal_sdk-16.0.0-dev.2 (c (n "altv_internal_sdk") (v "16.0.0-dev.2") (d (list (d (n "autocxx") (r "^0.26.0") (d #t) (k 0)) (d (n "autocxx-build") (r "^0.25.0") (d #t) (k 1)) (d (n "cxx") (r "^1.0.115") (d #t) (k 0)))) (h "0psb861iq49bnq54c9hnq6sihd8cnnifkv05y962fasmqkhaad4l") (r "1.73.0")))

(define-public crate-altv_internal_sdk-16.0.0 (c (n "altv_internal_sdk") (v "16.0.0") (d (list (d (n "autocxx") (r "^0.26.0") (d #t) (k 0)) (d (n "autocxx-build") (r "^0.25.0") (d #t) (k 1)) (d (n "cxx") (r "^1.0.115") (d #t) (k 0)))) (h "15c9x92y0kszl1jv5fma9mc2g74gmb21gr69zf3mcpxra669k55f") (r "1.73.0")))

(define-public crate-altv_internal_sdk-16.0.0-dev.3 (c (n "altv_internal_sdk") (v "16.0.0-dev.3") (d (list (d (n "autocxx") (r "^0.26.0") (d #t) (k 0)) (d (n "autocxx-build") (r "^0.25.0") (d #t) (k 1)) (d (n "cxx") (r "^1.0.115") (d #t) (k 0)))) (h "12i8lqy0xqp0gnzvpf7rg44g9hr0bcygwlqs489qflzq0k5gqgfi") (r "1.73.0")))

(define-public crate-altv_internal_sdk-16.0.1 (c (n "altv_internal_sdk") (v "16.0.1") (d (list (d (n "autocxx") (r "^0.26.0") (d #t) (k 0)) (d (n "autocxx-build") (r "^0.25.0") (d #t) (k 1)) (d (n "cxx") (r "^1.0.115") (d #t) (k 0)))) (h "05pi7ia9adwn9aiyls7bbvigbqvxcf9l6vpbxdjzhwk9k5x9am36") (r "1.73.0")))

(define-public crate-altv_internal_sdk-16.0.0-dev.4 (c (n "altv_internal_sdk") (v "16.0.0-dev.4") (d (list (d (n "autocxx") (r "^0.26.0") (d #t) (k 0)) (d (n "autocxx-build") (r "^0.25.0") (d #t) (k 1)) (d (n "cxx") (r "^1.0.115") (d #t) (k 0)))) (h "0mxp6gjxj24d0zs7d0qhh2g0yinf8ndjy7mza8baqkv5d71sis74") (r "1.73.0")))

(define-public crate-altv_internal_sdk-16.0.2 (c (n "altv_internal_sdk") (v "16.0.2") (d (list (d (n "autocxx") (r "^0.26.0") (d #t) (k 0)) (d (n "autocxx-build") (r "^0.25.0") (d #t) (k 1)) (d (n "cxx") (r "^1.0.115") (d #t) (k 0)))) (h "13694gy7llhr40rggzplr9dp4xdqd9chmzr7hq53l653i4nvdxnd") (r "1.73.0")))

