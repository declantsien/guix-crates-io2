(define-module (crates-io al tv altv_internal_resource_main_macro) #:use-module (crates-io))

(define-public crate-altv_internal_resource_main_macro-0.3.1 (c (n "altv_internal_resource_main_macro") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.105") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0cpf5drf5lk79wih12wwmf4mhpzg9qbq996xnxmzigckmgzas6wj") (y #t) (r "1.68.2")))

(define-public crate-altv_internal_resource_main_macro-15.0.0-dev.0 (c (n "altv_internal_resource_main_macro") (v "15.0.0-dev.0") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.105") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "18sbdmf7yzd78h6sadp48fvsw48wrxha6yi07nlswffnd9r8bhqr") (y #t) (r "1.68.2")))

(define-public crate-altv_internal_resource_main_macro-15.0.0-dev.1 (c (n "altv_internal_resource_main_macro") (v "15.0.0-dev.1") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.105") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1nf0h9ijxksh3s299i14zwkd3zabpjawlslani11m8fys7lmfcxw") (y #t) (r "1.68.2")))

(define-public crate-altv_internal_resource_main_macro-15.0.0-dev2 (c (n "altv_internal_resource_main_macro") (v "15.0.0-dev2") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.105") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0vjar5b4glr63xarcqcpaidc097hrzxxxcl2rq9bhsydzs1bf9j5") (y #t) (r "1.68.2")))

(define-public crate-altv_internal_resource_main_macro-15.0.0-dev3 (c (n "altv_internal_resource_main_macro") (v "15.0.0-dev3") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.105") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1rm6aq2iajiw2d5cv0s47774v7jxzbxnry5r9akrcf2rzb1lfyy9") (y #t) (r "1.68.2")))

(define-public crate-altv_internal_resource_main_macro-15.0.0-dev4 (c (n "altv_internal_resource_main_macro") (v "15.0.0-dev4") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.105") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0mxy4d4nga4pzkgda9yk34wdc7lh9rwk98x9z2ixbg6fc1glv3gr") (y #t) (r "1.68.2")))

(define-public crate-altv_internal_resource_main_macro-15.0.0-dev5 (c (n "altv_internal_resource_main_macro") (v "15.0.0-dev5") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.105") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "17bginp4m4b118mllnby3wdgnl8arcq828y74k62d0ij3wwgcyp3") (y #t) (r "1.68.2")))

(define-public crate-altv_internal_resource_main_macro-15.0.0-dev6 (c (n "altv_internal_resource_main_macro") (v "15.0.0-dev6") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.105") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "05zdvx230ylhjb5kkkpapd5c8qp3lvv4js2fydwjx19amq4kv6fy") (y #t) (r "1.68.2")))

(define-public crate-altv_internal_resource_main_macro-15.0.0-dev7 (c (n "altv_internal_resource_main_macro") (v "15.0.0-dev7") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.105") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0zhv6wjhji1w9h3l6lv2znarvph07bg1hbc3dpyf5r1zvwsk61l9") (y #t) (r "1.68.2")))

(define-public crate-altv_internal_resource_main_macro-15.0.0-dev8 (c (n "altv_internal_resource_main_macro") (v "15.0.0-dev8") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.105") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0zvci75zvk746mrvaj2gpvpb43586hnx1x55c0jj509mkvnbj1p7") (y #t) (r "1.68.2")))

(define-public crate-altv_internal_resource_main_macro-15.0.0-dev9 (c (n "altv_internal_resource_main_macro") (v "15.0.0-dev9") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.105") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0gazzsp9zwyx3fifl1xp15qxkv90mkbj77c5s45lfhln82ccdric") (y #t) (r "1.68.2")))

(define-public crate-altv_internal_resource_main_macro-15.0.0-dev.10 (c (n "altv_internal_resource_main_macro") (v "15.0.0-dev.10") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.105") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1mkw4505jqj5xcphwk0m49r6m81ab5j5ns5zihv0a5w1lqk9j9ib") (r "1.68.2")))

(define-public crate-altv_internal_resource_main_macro-15.0.0-dev.11 (c (n "altv_internal_resource_main_macro") (v "15.0.0-dev.11") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.105") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0vsx37j0l10fchnhphg73s3s512g44pxc7nrp1xmr66859fl06qy") (r "1.68.2")))

(define-public crate-altv_internal_resource_main_macro-15.0.0-dev.12 (c (n "altv_internal_resource_main_macro") (v "15.0.0-dev.12") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.105") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "176s07f9zcfxgdrz32068kv9hw45j34n20hphy0qrp937ivpn211") (r "1.68.2")))

(define-public crate-altv_internal_resource_main_macro-15.0.0-dev.13 (c (n "altv_internal_resource_main_macro") (v "15.0.0-dev.13") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.105") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "134a5g66davyzhmml046mmybkn5pibj08647hsyfx2ym4g1lspwr") (r "1.68.2")))

(define-public crate-altv_internal_resource_main_macro-15.0.0-dev.14 (c (n "altv_internal_resource_main_macro") (v "15.0.0-dev.14") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.105") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0nnp1z055wkw383naax7iisdqhh587zr1ibfl5z8qcyllfklyhxm") (r "1.68.2")))

(define-public crate-altv_internal_resource_main_macro-15.0.0-dev.15 (c (n "altv_internal_resource_main_macro") (v "15.0.0-dev.15") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.105") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0r59f7kxfzyw86d9jrs9xc3drcc9x8g6xsylp2scd25ibaxq12kk") (r "1.68.2")))

(define-public crate-altv_internal_resource_main_macro-15.0.0-dev.16 (c (n "altv_internal_resource_main_macro") (v "15.0.0-dev.16") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.105") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0ipdri7vx8lf0ydm2qif2mrkpddjjgfjl61asmyj2ybg5zrz08b1") (r "1.68.2")))

(define-public crate-altv_internal_resource_main_macro-15.0.0-dev.17 (c (n "altv_internal_resource_main_macro") (v "15.0.0-dev.17") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.105") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1zrxlkf263mfnwc663gdvfa66y2zkm38v22azmwdv9da4sx95pc0") (r "1.68.2")))

(define-public crate-altv_internal_resource_main_macro-15.0.0-dev.18 (c (n "altv_internal_resource_main_macro") (v "15.0.0-dev.18") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.105") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "018bz5wcbkmwp7aabwbjdcn93sh12kvi0dq312kqhl1n034a99fb") (r "1.68.2")))

(define-public crate-altv_internal_resource_main_macro-15.0.0-dev.19 (c (n "altv_internal_resource_main_macro") (v "15.0.0-dev.19") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.105") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "00hl7cpxim40fmraxv7ppijpsqybqms8d2jd9wi0df8f0lql5sf5") (r "1.68.2")))

(define-public crate-altv_internal_resource_main_macro-15.0.0-dev.20 (c (n "altv_internal_resource_main_macro") (v "15.0.0-dev.20") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.105") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0gh42w7v850vvazl1b59i1m4fv8jikxmnv6kh8gq4q3rqlnlan5l") (r "1.68.2")))

(define-public crate-altv_internal_resource_main_macro-15.0.0-dev.21 (c (n "altv_internal_resource_main_macro") (v "15.0.0-dev.21") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.105") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "11b3sp3hzdx0csnjixbq44hg0mnq2h27x159az61vki45m5xz6bn") (r "1.68.2")))

(define-public crate-altv_internal_resource_main_macro-15.0.0-dev.22 (c (n "altv_internal_resource_main_macro") (v "15.0.0-dev.22") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.105") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0939kvkqmf4bqh1vsgffgyvzjjcv9jp5ykad82w9dhh4dwl12p70") (r "1.68.2")))

(define-public crate-altv_internal_resource_main_macro-15.0.0-dev.23 (c (n "altv_internal_resource_main_macro") (v "15.0.0-dev.23") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.105") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "140bg17vpl0yzyf0m8d2gf107q22w9ww776jyznidfzbbprd30ci") (r "1.68.2")))

(define-public crate-altv_internal_resource_main_macro-15.0.0-dev.24 (c (n "altv_internal_resource_main_macro") (v "15.0.0-dev.24") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.105") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0pirib2064msfsrl34f0h5pvilpmry16l4b09s2m6s2q9indkav1") (r "1.68.2")))

(define-public crate-altv_internal_resource_main_macro-15.0.0-dev.25 (c (n "altv_internal_resource_main_macro") (v "15.0.0-dev.25") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.105") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0ik3vby45sjwx4lh4dp4lz5cbg1nfh3d9zf6p488m3kfr48ckmms") (r "1.68.2")))

(define-public crate-altv_internal_resource_main_macro-15.0.0-dev.26 (c (n "altv_internal_resource_main_macro") (v "15.0.0-dev.26") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.105") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0y78pf8zba0lrzf3rlvl9mls1lpdqd8ry7csfsvqp2dnlw3g0vzg") (r "1.68.2")))

(define-public crate-altv_internal_resource_main_macro-15.0.0-dev.27 (c (n "altv_internal_resource_main_macro") (v "15.0.0-dev.27") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.105") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0ksn6z2k8fgsg2csbx7qhb83zdfrkis0z302bn9r2ldw811h3pds") (r "1.68.2")))

(define-public crate-altv_internal_resource_main_macro-15.0.0-dev.28 (c (n "altv_internal_resource_main_macro") (v "15.0.0-dev.28") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.105") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0sczm332wqbkp95bixcb61n9x3qph0wakfrmmlk96hhxf65y0pkl") (r "1.68.2")))

(define-public crate-altv_internal_resource_main_macro-15.0.0-dev.29 (c (n "altv_internal_resource_main_macro") (v "15.0.0-dev.29") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.105") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "08q2k19bqz4z7frqq59f1hddc5lhzsn6cjq8gfk0r1w66z6hidr6") (r "1.68.2")))

(define-public crate-altv_internal_resource_main_macro-15.0.0-dev.30 (c (n "altv_internal_resource_main_macro") (v "15.0.0-dev.30") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.105") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1fvxc2zvpdirm8vyay4dgfzwl63lw27i87096gkgrd74jb4032m4") (r "1.68.2")))

(define-public crate-altv_internal_resource_main_macro-15.0.0-dev.31 (c (n "altv_internal_resource_main_macro") (v "15.0.0-dev.31") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.105") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0qfjis0qkr0h1vklxzglwllzdf5azc6s8hh0ayi979fp8011zp2k") (r "1.68.2")))

(define-public crate-altv_internal_resource_main_macro-15.0.0-dev.32 (c (n "altv_internal_resource_main_macro") (v "15.0.0-dev.32") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.105") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1xsfdi1p5iqf7lg6k71xykhb746z8ca0z1vdhcy3x7m20zmd5jv1") (r "1.68.2")))

(define-public crate-altv_internal_resource_main_macro-15.0.0-dev.33 (c (n "altv_internal_resource_main_macro") (v "15.0.0-dev.33") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.105") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1isbmajh4q1p8m8q11llwq4nc7x9pwlw3d0jvwfr18rlws119njl") (r "1.68.2")))

(define-public crate-altv_internal_resource_main_macro-15.0.0-dev.34 (c (n "altv_internal_resource_main_macro") (v "15.0.0-dev.34") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.105") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0jg05qh8jdqm1df4asyhpk4rx6dngzqy1smj0ql8yca734bvzfvp") (r "1.68.2")))

(define-public crate-altv_internal_resource_main_macro-15.0.0-dev.35 (c (n "altv_internal_resource_main_macro") (v "15.0.0-dev.35") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.105") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "18wbmy4qmj7msrqir4p81sk76ylyd05mn43lnclg4wvsbhnzv9mj") (r "1.68.2")))

(define-public crate-altv_internal_resource_main_macro-15.0.0-dev.36 (c (n "altv_internal_resource_main_macro") (v "15.0.0-dev.36") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.105") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1bhc82cwyc4nmhqhlq87a8x64nl616r9783pb5v31w7wsxddzikd") (r "1.68.2")))

(define-public crate-altv_internal_resource_main_macro-15.0.0-dev.37 (c (n "altv_internal_resource_main_macro") (v "15.0.0-dev.37") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.105") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "131n0m5a9prh37npsj3pasbgb20w4hbwcs8495bgggkn244jzcai") (r "1.68.2")))

(define-public crate-altv_internal_resource_main_macro-15.0.0-dev.38 (c (n "altv_internal_resource_main_macro") (v "15.0.0-dev.38") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.105") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0fgj77zqhvs2jz66bfc1l29hc1fyah7mrw1nagl3nv80ka6yg5dq") (r "1.68.2")))

(define-public crate-altv_internal_resource_main_macro-15.0.0-dev.39 (c (n "altv_internal_resource_main_macro") (v "15.0.0-dev.39") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.105") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1v6h0fm6whhciswjscc2i9yph8f8myf89cvdw9zkw8n3aml1xr3z") (r "1.68.2")))

(define-public crate-altv_internal_resource_main_macro-15.0.0-dev.40 (c (n "altv_internal_resource_main_macro") (v "15.0.0-dev.40") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.105") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0zmwpdr5xwmcfzbn09c8zgjhqanigc6av5izaa859665n7ddyxz7") (r "1.68.2")))

(define-public crate-altv_internal_resource_main_macro-15.0.0-dev.41 (c (n "altv_internal_resource_main_macro") (v "15.0.0-dev.41") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.105") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1fhmrnnmkb34d60bllw25mdybyw4ds9pcyvs7iknr80frgb852rq") (r "1.68.2")))

(define-public crate-altv_internal_resource_main_macro-15.0.0-dev42 (c (n "altv_internal_resource_main_macro") (v "15.0.0-dev42") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.105") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "17rrrx37dsqv0252y2vm471a3cm87f2csdgk79q8sk7awzhns193") (y #t) (r "1.68.2")))

(define-public crate-altv_internal_resource_main_macro-15.0.0-dev.45 (c (n "altv_internal_resource_main_macro") (v "15.0.0-dev.45") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.105") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1rhwpdawswsc7m5b7hhk0lm8vh1cz6i85vlnwzrgnk78assinx52") (r "1.68.2")))

(define-public crate-altv_internal_resource_main_macro-15.0.0-dev.46 (c (n "altv_internal_resource_main_macro") (v "15.0.0-dev.46") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.105") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1q0hd38vck3gkvjhxwc6s9lskqhhafllzf367ccwvv8kzmdar819") (r "1.68.2")))

(define-public crate-altv_internal_resource_main_macro-15.0.0-dev.47 (c (n "altv_internal_resource_main_macro") (v "15.0.0-dev.47") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.105") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1i8pws45fms68r9ggk6phvl148zhs6ic80kd2q5dcb0xl26zk0sz") (r "1.68.2")))

(define-public crate-altv_internal_resource_main_macro-15.0.0-dev.48 (c (n "altv_internal_resource_main_macro") (v "15.0.0-dev.48") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.105") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "00wyrgj9qqjia0i10ldji2w8n1pj2m1v19fzninqjakgavgmhrhj") (r "1.68.2")))

(define-public crate-altv_internal_resource_main_macro-15.0.0-dev.49 (c (n "altv_internal_resource_main_macro") (v "15.0.0-dev.49") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.105") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0phrvzfcmrv2zx63rn2lm1l66apjm929myg6c9fxsncr0sf285r9") (r "1.68.2")))

(define-public crate-altv_internal_resource_main_macro-15.0.0-dev.50 (c (n "altv_internal_resource_main_macro") (v "15.0.0-dev.50") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.105") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0kg69ggzslf8ai735sd5pgg16h9ym50hvx8wn3zwka3k9n856g5f") (r "1.68.2")))

(define-public crate-altv_internal_resource_main_macro-15.0.0-dev.51 (c (n "altv_internal_resource_main_macro") (v "15.0.0-dev.51") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.105") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1cfbsn9crqjz4nc1m6irx6i77hx1zp6p7xkqnm5fp8l97pgrihph") (r "1.68.2")))

(define-public crate-altv_internal_resource_main_macro-15.0.0-dev.52 (c (n "altv_internal_resource_main_macro") (v "15.0.0-dev.52") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.105") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "112g1y28xv7plfva6v8pmayf964367qayzjs37iicghyjn3xay72") (r "1.68.2")))

(define-public crate-altv_internal_resource_main_macro-15.0.0-dev.55 (c (n "altv_internal_resource_main_macro") (v "15.0.0-dev.55") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.105") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "00y3agnqrzw23yz26hjjq2vsw1fv6nr7c4i6dn77vqq4y8rsmb4k") (r "1.68.2")))

(define-public crate-altv_internal_resource_main_macro-15.0.0-dev.56 (c (n "altv_internal_resource_main_macro") (v "15.0.0-dev.56") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.105") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1yaqvyjizrk2mahkp23nm1k6208s2im3iczdhwwrzzgnqi2gi0jy") (r "1.68.2")))

(define-public crate-altv_internal_resource_main_macro-15.0.0-dev.57 (c (n "altv_internal_resource_main_macro") (v "15.0.0-dev.57") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.105") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0w5kqcf7iphf4ak7dz74xivy1bp6n48z8zrmk53hrjbrl4qhpb8q") (r "1.68.2")))

(define-public crate-altv_internal_resource_main_macro-15.0.0-dev.58 (c (n "altv_internal_resource_main_macro") (v "15.0.0-dev.58") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.105") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1l1mwc80l036jq5lyk24x20cqc9zy5h0p0cajbcg4qk4wdn78p7s") (r "1.68.2")))

(define-public crate-altv_internal_resource_main_macro-15.0.0-dev.59 (c (n "altv_internal_resource_main_macro") (v "15.0.0-dev.59") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.105") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0x1qcb7q573djrm7kkpy2m8ahbq1frvrgs53ivbs118f7hzbrjnd") (r "1.68.2")))

(define-public crate-altv_internal_resource_main_macro-15.0.0-dev.60 (c (n "altv_internal_resource_main_macro") (v "15.0.0-dev.60") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.105") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0xjvnxvgv3lyfrd7fnyb10kcg3x18a0wkb9ci85azmiynbygssmg") (r "1.68.2")))

(define-public crate-altv_internal_resource_main_macro-15.0.0-dev.61 (c (n "altv_internal_resource_main_macro") (v "15.0.0-dev.61") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.105") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1745zicvkzkr099sr27hqhvchml8ihaxpm2z7h7282161sqyclxb") (r "1.68.2")))

(define-public crate-altv_internal_resource_main_macro-15.0.0-dev.62 (c (n "altv_internal_resource_main_macro") (v "15.0.0-dev.62") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.105") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0vr6mnhpwgrg5p00198m0fkcl5nz1hhdx5idcn92yqay3djxkdym") (r "1.68.2")))

(define-public crate-altv_internal_resource_main_macro-15.0.0-rc.1 (c (n "altv_internal_resource_main_macro") (v "15.0.0-rc.1") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.105") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1fx9fpr449n12yr8b4wg70044xd7w9a913sfj4giqjih98dhyzs0") (r "1.68.2")))

(define-public crate-altv_internal_resource_main_macro-15.0.0-dev.65 (c (n "altv_internal_resource_main_macro") (v "15.0.0-dev.65") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.105") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0jzxsp1qjryzmpjk0rzssrj30pb1whridryq9nyjlyhda9csbaia") (r "1.68.2")))

(define-public crate-altv_internal_resource_main_macro-15.0.0-dev.67 (c (n "altv_internal_resource_main_macro") (v "15.0.0-dev.67") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.105") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1yl8zdrzy48y3qssa989kmx3nkg1lq14ycx8v2dqfsyhjqayryrn") (r "1.68.2")))

(define-public crate-altv_internal_resource_main_macro-15.0.0-rc.2 (c (n "altv_internal_resource_main_macro") (v "15.0.0-rc.2") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.105") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0ipdglxx5jsp6mss8bacqr40y8ql4ll6ri2mwm12f3w5wlavcmd4") (r "1.68.2")))

(define-public crate-altv_internal_resource_main_macro-15.0.0-dev.68 (c (n "altv_internal_resource_main_macro") (v "15.0.0-dev.68") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.105") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "17xdc32wzbpf6y5chgkl99c6wn04ida0im2716m7qwgig4k5p3ii") (r "1.68.2")))

(define-public crate-altv_internal_resource_main_macro-15.0.0-dev.69 (c (n "altv_internal_resource_main_macro") (v "15.0.0-dev.69") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.105") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1m2b9h17rykp51fjnkywxc4gh45367sa4zr440hw433pyvnqzx8q") (r "1.68.2")))

(define-public crate-altv_internal_resource_main_macro-15.0.0-rc.3 (c (n "altv_internal_resource_main_macro") (v "15.0.0-rc.3") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.105") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0li8k8s31i5vhpgsmy4y95zrlxjhyjapzz2m1nn6py3d5mpnzdj9") (r "1.68.2")))

(define-public crate-altv_internal_resource_main_macro-15.0.0-dev.70 (c (n "altv_internal_resource_main_macro") (v "15.0.0-dev.70") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.105") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0l1bjiycd883pkn1jbzzryvz441g0v9b0367z2jgmwapg40nandi") (r "1.68.2")))

(define-public crate-altv_internal_resource_main_macro-15.0.0-rc.4 (c (n "altv_internal_resource_main_macro") (v "15.0.0-rc.4") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.105") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "18f4n63ma0qw5whmg90pl059s5axvg8mc0lnw5jrkyxxgcy35ld1") (r "1.68.2")))

(define-public crate-altv_internal_resource_main_macro-15.0.0-dev.71 (c (n "altv_internal_resource_main_macro") (v "15.0.0-dev.71") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.105") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0l5izy44a3gsk426l8b9w5rpy1kk8ypzxbncw5isf9pnw8iv2dzy") (r "1.68.2")))

(define-public crate-altv_internal_resource_main_macro-15.0.0-dev.72 (c (n "altv_internal_resource_main_macro") (v "15.0.0-dev.72") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.105") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "19g8iyk1dkja94dwj0mcza6p0vlb7ya3vis2san0pdhl6qf45xmm") (r "1.68.2")))

(define-public crate-altv_internal_resource_main_macro-15.0.0-dev.73 (c (n "altv_internal_resource_main_macro") (v "15.0.0-dev.73") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.105") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0m7hm1dzj97gasc80bvnzzqrk81qwrfgayj69ca7p8h7gvzw3y33") (r "1.68.2")))

(define-public crate-altv_internal_resource_main_macro-15.0.0-dev.74 (c (n "altv_internal_resource_main_macro") (v "15.0.0-dev.74") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.105") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1pmckwg2mhi6l2s316rf7z4mb4xvis9hilv64sx008nrs1f4r7i6") (r "1.68.2")))

(define-public crate-altv_internal_resource_main_macro-15.0.0-dev.75 (c (n "altv_internal_resource_main_macro") (v "15.0.0-dev.75") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.105") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "05shra3da8sh65mym52dp7xx296q666rnmhflj3vq2z6xq6pl0rl") (r "1.68.2")))

(define-public crate-altv_internal_resource_main_macro-15.0.0-rc.6 (c (n "altv_internal_resource_main_macro") (v "15.0.0-rc.6") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.105") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "15kabd6f7drj1bgq1634rs90lba7fjpv3c34q7lb8f911d2cig6i") (r "1.73.0")))

(define-public crate-altv_internal_resource_main_macro-15.0.0-dev.76 (c (n "altv_internal_resource_main_macro") (v "15.0.0-dev.76") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.105") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "05wa6qfh98ciiy1ajh6fcp0mfkh1vbisx723rlnmd4kcv5x80qa9") (r "1.73.0")))

(define-public crate-altv_internal_resource_main_macro-15.0.0 (c (n "altv_internal_resource_main_macro") (v "15.0.0") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.105") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0s98j32d6hz0bhnsp70771g2x8wsrpnil9gwj231fk1l76pbvwvw") (r "1.73.0")))

(define-public crate-altv_internal_resource_main_macro-15.0.1 (c (n "altv_internal_resource_main_macro") (v "15.0.1") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.105") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0yf7ga4zr867ip8c4fc3myi7jffkacknfl5dx7zvvgrx00wzbgq1") (r "1.73.0")))

(define-public crate-altv_internal_resource_main_macro-15.1.0 (c (n "altv_internal_resource_main_macro") (v "15.1.0") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.105") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "04x8asxh10n7wsv2bpjprw9pmp92vp2ixd431j7az5rpzx10fski") (r "1.73.0")))

(define-public crate-altv_internal_resource_main_macro-16.0.0-dev.1 (c (n "altv_internal_resource_main_macro") (v "16.0.0-dev.1") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.105") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0bradys7mb1i5m8aa64rpw1m6nr2k6abdbxkv60hhib3wb7f8jc9") (r "1.73.0")))

(define-public crate-altv_internal_resource_main_macro-16.0.0-dev.2 (c (n "altv_internal_resource_main_macro") (v "16.0.0-dev.2") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "12x8ibbshs9nn7qj3yg0zj0kq92fiv3hha9z6b4zl4a4hs2xf7cf") (r "1.73.0")))

(define-public crate-altv_internal_resource_main_macro-16.0.0 (c (n "altv_internal_resource_main_macro") (v "16.0.0") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0159linpbdzaqa0jia5qgfl61bfqf471ba5fvzm1rx97f7hdp31j") (r "1.73.0")))

(define-public crate-altv_internal_resource_main_macro-16.0.0-dev.3 (c (n "altv_internal_resource_main_macro") (v "16.0.0-dev.3") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0l35wjx480jnchz7z9a7vzd4pxby3m8ax63igl50xqznmq0bjndp") (r "1.73.0")))

(define-public crate-altv_internal_resource_main_macro-16.0.1 (c (n "altv_internal_resource_main_macro") (v "16.0.1") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "18na1a3zbkfjvl2plh9gly2nac236m4k762j4kz8cibbjj1zf1b7") (r "1.73.0")))

(define-public crate-altv_internal_resource_main_macro-16.0.0-dev.4 (c (n "altv_internal_resource_main_macro") (v "16.0.0-dev.4") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "09rpg6pwd63sil7az401m0vmd7da0kvfrsv9q1ri3srp100pgqd1") (r "1.73.0")))

(define-public crate-altv_internal_resource_main_macro-16.0.2 (c (n "altv_internal_resource_main_macro") (v "16.0.2") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1gsq9gnaw9k9my3x3gv02zjnlmlsiwmz4gglm3jig671s7lhyknk") (r "1.73.0")))

