(define-module (crates-io al tv altv_internal_logger) #:use-module (crates-io))

(define-public crate-altv_internal_logger-0.3.1 (c (n "altv_internal_logger") (v "0.3.1") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "0yhh8ns0xhadbdy0hviflkvxk1is777g7jvadr28zr182vvfvki2") (y #t) (r "1.68.2")))

(define-public crate-altv_internal_logger-15.0.0-dev.0 (c (n "altv_internal_logger") (v "15.0.0-dev.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "1y2pnlqmbs8yxdi27pykcyzk7psan7zzvns086rqi6i393xkx8bs") (y #t) (r "1.68.2")))

(define-public crate-altv_internal_logger-15.0.0-dev.1 (c (n "altv_internal_logger") (v "15.0.0-dev.1") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "1hqip7x1x7vwx8syfj7ai21xsl7rbr68n8d59a2fpwimfprh1a6a") (y #t) (r "1.68.2")))

(define-public crate-altv_internal_logger-15.0.0-dev2 (c (n "altv_internal_logger") (v "15.0.0-dev2") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "138y4k1kl7134k8bfdy2g57md28ayqp8l675k187iy9wm7p7s3g5") (y #t) (r "1.68.2")))

(define-public crate-altv_internal_logger-15.0.0-dev3 (c (n "altv_internal_logger") (v "15.0.0-dev3") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "1nkjm0iw2aqv6c9ggjhmgymk8c276m9h7hwsvc8a42cqc1pb3a7z") (y #t) (r "1.68.2")))

(define-public crate-altv_internal_logger-15.0.0-dev4 (c (n "altv_internal_logger") (v "15.0.0-dev4") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "0rf8xndqahnay3a0az5ia9796ymamfzyzl997pwxv19sry6nm0ga") (y #t) (r "1.68.2")))

(define-public crate-altv_internal_logger-15.0.0-dev5 (c (n "altv_internal_logger") (v "15.0.0-dev5") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "0hwxmkcchghmi5llz4bbp1jl3iv87v6sc373q34ykva92d6hgh6h") (y #t) (r "1.68.2")))

(define-public crate-altv_internal_logger-15.0.0-dev6 (c (n "altv_internal_logger") (v "15.0.0-dev6") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "0pwd1110b96j5piyqg2brwr0wklrnsv1bg0wi59m59ar929b0dj1") (y #t) (r "1.68.2")))

(define-public crate-altv_internal_logger-15.0.0-dev7 (c (n "altv_internal_logger") (v "15.0.0-dev7") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "0c0hkd6jkvg5i19mw704li626kp2ac2rx9d4xm5s0y49bgk4x436") (y #t) (r "1.68.2")))

(define-public crate-altv_internal_logger-15.0.0-dev8 (c (n "altv_internal_logger") (v "15.0.0-dev8") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "0lcm4s9nhhinmznnm6d9w7bavwhi83xs87ci25wk70p343dkrg1x") (y #t) (r "1.68.2")))

(define-public crate-altv_internal_logger-15.0.0-dev9 (c (n "altv_internal_logger") (v "15.0.0-dev9") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "1ijci4awxar6hnv8ikhzzw22lpg8fqwa1f55shp9hnh7z3hg0rg0") (y #t) (r "1.68.2")))

(define-public crate-altv_internal_logger-15.0.0-dev.10 (c (n "altv_internal_logger") (v "15.0.0-dev.10") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "0j83ha7cigmq9r4gblxd9z2z3jwkn29s64dk3374qmph6q60m3ni") (r "1.68.2")))

(define-public crate-altv_internal_logger-15.0.0-dev.11 (c (n "altv_internal_logger") (v "15.0.0-dev.11") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "1sd3zs0qh5ghfp3jfwssr7p2hqv44lly8qc3lpfkwrmysi90vcld") (r "1.68.2")))

(define-public crate-altv_internal_logger-15.0.0-dev.12 (c (n "altv_internal_logger") (v "15.0.0-dev.12") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "00w79r0l0dznwplz1kbn0xwpc83rwvbn5b9bgrpk5mpbjmcr5r08") (r "1.68.2")))

(define-public crate-altv_internal_logger-15.0.0-dev.13 (c (n "altv_internal_logger") (v "15.0.0-dev.13") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "0yjz683p947g1q07s36dnscwh0p0fhgpp2yhifjqafw70zzw98gb") (r "1.68.2")))

(define-public crate-altv_internal_logger-15.0.0-dev.14 (c (n "altv_internal_logger") (v "15.0.0-dev.14") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "1d5ayffwrkdj9g3jykmnw6b9g6pzkgyc296sdml3ay2h9z4q16a3") (r "1.68.2")))

(define-public crate-altv_internal_logger-15.0.0-dev.15 (c (n "altv_internal_logger") (v "15.0.0-dev.15") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "0kfd69l72m90s0sz4y6jzlb7bwb245bnvz0c92b744xs7cx3sdrc") (r "1.68.2")))

(define-public crate-altv_internal_logger-15.0.0-dev.16 (c (n "altv_internal_logger") (v "15.0.0-dev.16") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "09bd0b0z61w2ibhd2c4h9wia4rh2s4061xlg066bvk8j801n91z3") (r "1.68.2")))

(define-public crate-altv_internal_logger-15.0.0-dev.17 (c (n "altv_internal_logger") (v "15.0.0-dev.17") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "1y96rbyzn2h7qrnxqg5210xhmfj6956rb6zbwkg460qd80bdyhpq") (r "1.68.2")))

(define-public crate-altv_internal_logger-15.0.0-dev.18 (c (n "altv_internal_logger") (v "15.0.0-dev.18") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "08x0jaacggz4vb89sqqql3vdyws90jjpbid916lklgrdawh9a5z1") (r "1.68.2")))

(define-public crate-altv_internal_logger-15.0.0-dev.19 (c (n "altv_internal_logger") (v "15.0.0-dev.19") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "1lhqcp6c007dv5ccv7a59k8vns52arpjbrbik48r7lrqlksg90z6") (r "1.68.2")))

(define-public crate-altv_internal_logger-15.0.0-dev.20 (c (n "altv_internal_logger") (v "15.0.0-dev.20") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "0ynbn40mf12svf2j57whah5v49kr2fipp02k5mw5a7sw8j21hwjz") (r "1.68.2")))

(define-public crate-altv_internal_logger-15.0.0-dev.21 (c (n "altv_internal_logger") (v "15.0.0-dev.21") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "1hw1y6vsrpshnzanjcha0as03l92ikg7xl3k0wzrhq6rh04i4h4c") (r "1.68.2")))

(define-public crate-altv_internal_logger-15.0.0-dev.22 (c (n "altv_internal_logger") (v "15.0.0-dev.22") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "09flp4y1jiir4bmxfaph4arv4n9d2vic3mvgfmhxbbq3215z4qrr") (r "1.68.2")))

(define-public crate-altv_internal_logger-15.0.0-dev.23 (c (n "altv_internal_logger") (v "15.0.0-dev.23") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "0iycmnk144vab3nmj216ypi143mfc9rpa1qmhms58v9k0mxzwm7z") (r "1.68.2")))

(define-public crate-altv_internal_logger-15.0.0-dev.24 (c (n "altv_internal_logger") (v "15.0.0-dev.24") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "1yw1lfviyg0mrlbyjlypfp7s70j0z186hcp81rhjbxv1q0jbfmn8") (r "1.68.2")))

(define-public crate-altv_internal_logger-15.0.0-dev.25 (c (n "altv_internal_logger") (v "15.0.0-dev.25") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "0z7a5ragmagvszcvz3yv0mq5vx5v5qiikwn2mix2nxbarxwqjb2r") (r "1.68.2")))

(define-public crate-altv_internal_logger-15.0.0-dev.26 (c (n "altv_internal_logger") (v "15.0.0-dev.26") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "0crzy18b97qphlfqq52lpbssc319i90dk2a4zkjcchdx024dbx9m") (r "1.68.2")))

(define-public crate-altv_internal_logger-15.0.0-dev.27 (c (n "altv_internal_logger") (v "15.0.0-dev.27") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "1a2frg6gbkqz4yvjizn1hvc48im62sd6hl29i1ipvjp2dzwpi9z9") (r "1.68.2")))

(define-public crate-altv_internal_logger-15.0.0-dev.28 (c (n "altv_internal_logger") (v "15.0.0-dev.28") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "012ddqsblg5wbwdd290cwr0zidd88j6hdi9mgl6aapjp2np5bmjg") (r "1.68.2")))

(define-public crate-altv_internal_logger-15.0.0-dev.29 (c (n "altv_internal_logger") (v "15.0.0-dev.29") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "0id4x3yz7y2qlhdlfd149swxc5fd6l2796hclxm98dnbff3wp12g") (r "1.68.2")))

(define-public crate-altv_internal_logger-15.0.0-dev.30 (c (n "altv_internal_logger") (v "15.0.0-dev.30") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "09giyj1j8r2j38nxq88azkdbca2f2q4g83bmhmfd8i616zv71sy1") (r "1.68.2")))

(define-public crate-altv_internal_logger-15.0.0-dev.31 (c (n "altv_internal_logger") (v "15.0.0-dev.31") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "0vy092b8vpd07d1w12yngfhmhn4kmvhd974cr54hghpvmr57a4zb") (r "1.68.2")))

(define-public crate-altv_internal_logger-15.0.0-dev.32 (c (n "altv_internal_logger") (v "15.0.0-dev.32") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "0n1473y3mx1drcsr26xc1q3x4mkyx9qq4lwhbvqjf5lrpnmh6ir3") (r "1.68.2")))

(define-public crate-altv_internal_logger-15.0.0-dev.33 (c (n "altv_internal_logger") (v "15.0.0-dev.33") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "1kz5v2w4pdnimwwcjkz7i7xhr6n85v6xfgpa735c03xsmzsm7ymd") (r "1.68.2")))

(define-public crate-altv_internal_logger-15.0.0-dev.34 (c (n "altv_internal_logger") (v "15.0.0-dev.34") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "0rvi5spm3p5inf9px7hfcd98y4dlzdqrbmqbvxhcdcwhmhslvp1p") (r "1.68.2")))

(define-public crate-altv_internal_logger-15.0.0-dev.35 (c (n "altv_internal_logger") (v "15.0.0-dev.35") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "1qfkamsh2pl08m2qw55b3i5zs5pa2wb3c839yp0f4amrqxc8l7p4") (r "1.68.2")))

(define-public crate-altv_internal_logger-15.0.0-dev.36 (c (n "altv_internal_logger") (v "15.0.0-dev.36") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "0dxnvg5jnn87h1rd4w2bi27la6fvnpmhg2qns7cc2jlas1qqmh7l") (r "1.68.2")))

(define-public crate-altv_internal_logger-15.0.0-dev.37 (c (n "altv_internal_logger") (v "15.0.0-dev.37") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "12pqwyh7f7mqjxbwkha7fr5jq743g1kqcjg6a8qsdyd17080jyas") (r "1.68.2")))

(define-public crate-altv_internal_logger-15.0.0-dev.38 (c (n "altv_internal_logger") (v "15.0.0-dev.38") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "1phkx1224wwzhzm6dyrzy7my6akbf4aqx87szvikqlv6696y3qwl") (r "1.68.2")))

(define-public crate-altv_internal_logger-15.0.0-dev.39 (c (n "altv_internal_logger") (v "15.0.0-dev.39") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "1qg40l8sbccvdxbybvypqc28g2v824ghdi14778xv8qkpqy4pfbd") (r "1.68.2")))

(define-public crate-altv_internal_logger-15.0.0-dev.40 (c (n "altv_internal_logger") (v "15.0.0-dev.40") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "1xkyx25z1sab2b07js9qsr2c5h4il54qcn5k3zlw9v87mgfb98dh") (r "1.68.2")))

(define-public crate-altv_internal_logger-15.0.0-dev.41 (c (n "altv_internal_logger") (v "15.0.0-dev.41") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "1hdnx17067jll1fij2p2v4vsbiwz67wqdpapspqdqxdsnk0a208z") (r "1.68.2")))

(define-public crate-altv_internal_logger-15.0.0-dev42 (c (n "altv_internal_logger") (v "15.0.0-dev42") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "1zvhydqf4qa6v2brcjinddpj32dc7dfhcjlmigpp7br06cw24cp1") (y #t) (r "1.68.2")))

(define-public crate-altv_internal_logger-15.0.0-dev.43 (c (n "altv_internal_logger") (v "15.0.0-dev.43") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "1pw0pvsdlndr58pvl2xzil447i1dppkynn055chdyc0zxm4gjngq") (y #t) (r "1.68.2")))

(define-public crate-altv_internal_logger-15.0.0-dev.44 (c (n "altv_internal_logger") (v "15.0.0-dev.44") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "0zqsjmak6w9315cby5pb6q947979q9p59aahlzrskf68q6i6jyn2") (y #t) (r "1.68.2")))

(define-public crate-altv_internal_logger-15.0.0-dev.45 (c (n "altv_internal_logger") (v "15.0.0-dev.45") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "0bm48bvslqb114g06wmh8sbc22qkp138j2xj11hnw4ym56nrksp2") (r "1.68.2")))

(define-public crate-altv_internal_logger-15.0.0-dev.46 (c (n "altv_internal_logger") (v "15.0.0-dev.46") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "0bmrwa6hahn7m4n3z46rzlb5p3rnigilwnm47nkmgd7kw7fp1gia") (r "1.68.2")))

(define-public crate-altv_internal_logger-15.0.0-dev.47 (c (n "altv_internal_logger") (v "15.0.0-dev.47") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "0fkxak7xl53gr1r4i2ik99khsrpwrqv17p6632k31gqhjp52j7kr") (r "1.68.2")))

(define-public crate-altv_internal_logger-15.0.0-dev.48 (c (n "altv_internal_logger") (v "15.0.0-dev.48") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "042z74w7k65aagdyfa99myf386s9g1g9ynkdgbc06097d1qzpg7p") (r "1.68.2")))

(define-public crate-altv_internal_logger-15.0.0-dev.49 (c (n "altv_internal_logger") (v "15.0.0-dev.49") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "1b6yhg64c5i6gw6wdczk4vqjdp8w4ykjpacz55nissb8csf9m4ak") (r "1.68.2")))

(define-public crate-altv_internal_logger-15.0.0-dev.50 (c (n "altv_internal_logger") (v "15.0.0-dev.50") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "1k37s64kmgylrdybgxw20x35w044m4fly2x5cv4p38i76pynzvk3") (r "1.68.2")))

(define-public crate-altv_internal_logger-15.0.0-dev.51 (c (n "altv_internal_logger") (v "15.0.0-dev.51") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "17mxwlkyqmyb3hajx89aqm8rz3q7vx68mi82l30ndx9czsyy3sg4") (r "1.68.2")))

(define-public crate-altv_internal_logger-15.0.0-dev.52 (c (n "altv_internal_logger") (v "15.0.0-dev.52") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "0vkvhh0bxksdbq1znh44i8s4vdqw96q3prbxfmdv05gpal4q4xil") (r "1.68.2")))

(define-public crate-altv_internal_logger-15.0.0-dev.54 (c (n "altv_internal_logger") (v "15.0.0-dev.54") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "09qgriyh6pv7dmjvf2af7d1g2fhprc9i8v279rz008vl0pyayhix") (r "1.68.2")))

(define-public crate-altv_internal_logger-15.0.0-dev.55 (c (n "altv_internal_logger") (v "15.0.0-dev.55") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "0kc2a5k3219fg0w8paq3ads6hvv7pbdiilng6gl70zwsmjdhfllj") (r "1.68.2")))

(define-public crate-altv_internal_logger-15.0.0-dev.56 (c (n "altv_internal_logger") (v "15.0.0-dev.56") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "037vdxhsg5z5g7nf9hcf3pf49ans4r0gz6j3s1k96l14d0rrv75v") (r "1.68.2")))

(define-public crate-altv_internal_logger-15.0.0-dev.57 (c (n "altv_internal_logger") (v "15.0.0-dev.57") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "12mc1p7m2yfhpk2y8x5h4iz332wdqfrxrlx2x9wx1phxvw0wj6av") (r "1.68.2")))

(define-public crate-altv_internal_logger-15.0.0-dev.58 (c (n "altv_internal_logger") (v "15.0.0-dev.58") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "1ywnhp6c4jih94gd6kmif2p2i5rwdpqgkjkavlbq1mcqzdg7pk77") (r "1.68.2")))

(define-public crate-altv_internal_logger-15.0.0-dev.59 (c (n "altv_internal_logger") (v "15.0.0-dev.59") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "1a2vz2l4bxkrd9ph0acwnap42cagrvicbxxwxj906hl09y836fsv") (r "1.68.2")))

(define-public crate-altv_internal_logger-15.0.0-dev.60 (c (n "altv_internal_logger") (v "15.0.0-dev.60") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "1sm6fn4hxbgy7hhnzg9yan5f9r293yd84d8pv9b1w2pmz969j5y2") (r "1.68.2")))

(define-public crate-altv_internal_logger-15.0.0-dev.61 (c (n "altv_internal_logger") (v "15.0.0-dev.61") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "0lj92hbkv5mqg6kpavdm3whaa4pc92hvkgaxs5pfhblrywnnvimg") (r "1.68.2")))

(define-public crate-altv_internal_logger-15.0.0-dev.62 (c (n "altv_internal_logger") (v "15.0.0-dev.62") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "0pmashj19f572d5zzhlzixszhpjbbd90xwv8knl8m38nz4j3ckhv") (r "1.68.2")))

(define-public crate-altv_internal_logger-15.0.0-rc.1 (c (n "altv_internal_logger") (v "15.0.0-rc.1") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "0v2wck1hfj63422f83jxncfv3hg1lqpzw4iiwlnmrmy4m0sb62a0") (r "1.68.2")))

(define-public crate-altv_internal_logger-15.0.0-dev.63 (c (n "altv_internal_logger") (v "15.0.0-dev.63") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "07igjjy6dcmsr5rw26f5bjj0r241wf7sjrf0b9hnm54981in310p") (r "1.68.2")))

(define-public crate-altv_internal_logger-15.0.0-dev.64 (c (n "altv_internal_logger") (v "15.0.0-dev.64") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "12qh8fm2paiqj98nh7bc1y1q6p9d4i5vak0jbclvvzsa56rxfqfi") (r "1.68.2")))

(define-public crate-altv_internal_logger-15.0.0-dev.65 (c (n "altv_internal_logger") (v "15.0.0-dev.65") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "15xbqqs5pr23757hrfhk41nypy3fprxa2z7xg4zaii84nnhq094d") (r "1.68.2")))

(define-public crate-altv_internal_logger-15.0.0-dev.67 (c (n "altv_internal_logger") (v "15.0.0-dev.67") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "1nhnjjv1z3jgig6vbp8pqla9hrr6xx02gpyq5l0d1c0va6z89riy") (r "1.68.2")))

(define-public crate-altv_internal_logger-15.0.0-rc.2 (c (n "altv_internal_logger") (v "15.0.0-rc.2") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "11vwj3wcf595gkknk8bf5g3j3hk54z44hdgqqpgv4i87ikib3pmb") (r "1.68.2")))

(define-public crate-altv_internal_logger-15.0.0-dev.68 (c (n "altv_internal_logger") (v "15.0.0-dev.68") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "1q83ln28ql042l8z2y4cx00bjfq61xiqsyn7phwxa9xz08qh99jz") (r "1.68.2")))

(define-public crate-altv_internal_logger-15.0.0-dev.69 (c (n "altv_internal_logger") (v "15.0.0-dev.69") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "0xgvc09gvfi1rky8adjf7k6pgmg4k5pc20ga7h0zsa8gj8hw9spj") (r "1.68.2")))

(define-public crate-altv_internal_logger-15.0.0-rc.3 (c (n "altv_internal_logger") (v "15.0.0-rc.3") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "1v0vg5j4xif0q7ab1amacf6lhfyj2c4c74c91m2mpw5nhrgvkhnd") (r "1.68.2")))

(define-public crate-altv_internal_logger-15.0.0-dev.70 (c (n "altv_internal_logger") (v "15.0.0-dev.70") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "004cv588dxc5yrg5h1ikx9767ibhc8xbpry4dbgspil4a83440j6") (r "1.68.2")))

(define-public crate-altv_internal_logger-15.0.0-rc.4 (c (n "altv_internal_logger") (v "15.0.0-rc.4") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "0yc716sw18jszcblkc11q8bgbijvbl3dn3b29ni8g2xhsbas160w") (r "1.68.2")))

(define-public crate-altv_internal_logger-15.0.0-dev.71 (c (n "altv_internal_logger") (v "15.0.0-dev.71") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "1x1h6j83kkjgj5gl11baix8kldyp5aydxp52b433mhq77ywqay1c") (r "1.68.2")))

(define-public crate-altv_internal_logger-15.0.0-dev.72 (c (n "altv_internal_logger") (v "15.0.0-dev.72") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "0my64b3wcr4zlizfifpp0vg2mxcbnmbsy0fzfz614vg7nq3dq7h8") (r "1.68.2")))

(define-public crate-altv_internal_logger-15.0.0-dev.73 (c (n "altv_internal_logger") (v "15.0.0-dev.73") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "0w0psh2whwwnfbsi38jrqljvbg39vggjmncklc37h9jrkg4hlm6s") (r "1.68.2")))

(define-public crate-altv_internal_logger-15.0.0-dev.74 (c (n "altv_internal_logger") (v "15.0.0-dev.74") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "01znr9d5zm9297rcz7f9yvimhn6plrmgzcqza6f9cbyhbip30bn0") (r "1.68.2")))

(define-public crate-altv_internal_logger-15.0.0-dev.75 (c (n "altv_internal_logger") (v "15.0.0-dev.75") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "1xm9hk5lzfwvg6w2xw2j8khp503w1hirhhjdzxdnp2l51c03scrp") (r "1.68.2")))

(define-public crate-altv_internal_logger-15.0.0-rc.6 (c (n "altv_internal_logger") (v "15.0.0-rc.6") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "082i0bpmrcfja01p7v46kqx8nrfyzl3irgjliqg06ckvrwp4fs39") (r "1.73.0")))

(define-public crate-altv_internal_logger-15.0.0-dev.76 (c (n "altv_internal_logger") (v "15.0.0-dev.76") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "1mcmjmjqv0h17pjp7fc3l32vdcij19417rkk94aalv9735gakbrx") (r "1.73.0")))

(define-public crate-altv_internal_logger-15.0.0 (c (n "altv_internal_logger") (v "15.0.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "0rk0vnd0rqr0bmkn1c7wkngpagdj3ib7xyrkmljv900x2sxbba3f") (r "1.73.0")))

(define-public crate-altv_internal_logger-15.0.1 (c (n "altv_internal_logger") (v "15.0.1") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "16k9y1gihis73q3gj5nx8758xvzcdm8xpy0khzll1ih4fpndyqjg") (r "1.73.0")))

(define-public crate-altv_internal_logger-15.1.0 (c (n "altv_internal_logger") (v "15.1.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "1fgwmd692sd7haj28k3k437br2n9kn5ajdipnyshw4m1db6n7w8r") (r "1.73.0")))

(define-public crate-altv_internal_logger-16.0.0-dev.1 (c (n "altv_internal_logger") (v "16.0.0-dev.1") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "0nc9a06zma0lhdycy3l9f2yy0si5wvr78pw32y9kyfkdgjyddr8j") (r "1.73.0")))

(define-public crate-altv_internal_logger-16.0.0-dev.2 (c (n "altv_internal_logger") (v "16.0.0-dev.2") (d (list (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "0xrwii0ljbf851ahdg0dfxknhan2gajzq9zfrjyk20bifiq9m7bd") (r "1.73.0")))

(define-public crate-altv_internal_logger-16.0.0 (c (n "altv_internal_logger") (v "16.0.0") (d (list (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "0cqik3fyik3ka1max5gyr3pjnqilipdh9z0g20fn8jwymr2g6n4c") (r "1.73.0")))

(define-public crate-altv_internal_logger-16.0.0-dev.3 (c (n "altv_internal_logger") (v "16.0.0-dev.3") (d (list (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "10wi9mfh30krm9fnrmna9i2s0088g28cb24md03xl88j8rrh9fjf") (r "1.73.0")))

(define-public crate-altv_internal_logger-16.0.1 (c (n "altv_internal_logger") (v "16.0.1") (d (list (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "14aw233fjp6ryx9xhpkq1vz34hydkbvglm09p6mia203mz45q0r8") (r "1.73.0")))

(define-public crate-altv_internal_logger-16.0.0-dev.4 (c (n "altv_internal_logger") (v "16.0.0-dev.4") (d (list (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "131a8cfwzzzxj620w3z0i0byaz9vph6g6sxshgq1alwi7v696a0q") (r "1.73.0")))

(define-public crate-altv_internal_logger-16.0.2 (c (n "altv_internal_logger") (v "16.0.2") (d (list (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "1xlsbmhxk5arasqfwnmlv6qwbbyy3dghxbay2q94abcjavp0f029") (r "1.73.0")))

