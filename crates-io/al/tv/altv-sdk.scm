(define-module (crates-io al tv altv-sdk) #:use-module (crates-io))

(define-public crate-altv-sdk-0.1.0 (c (n "altv-sdk") (v "0.1.0") (d (list (d (n "altv-core") (r "^0.1.0") (d #t) (k 0)) (d (n "bindgen") (r "^0.53.2") (d #t) (k 1)))) (h "0s3gk5rp5d1kr01fjk4jsxv589wzg5jp0nqr1slnvmz56svfd1nr") (y #t)))

(define-public crate-altv-sdk-0.2.0 (c (n "altv-sdk") (v "0.2.0") (d (list (d (n "altv-core") (r "^0.1.0") (d #t) (k 0)) (d (n "bindgen") (r "^0.53.2") (d #t) (k 1)) (d (n "nalgebra") (r "^0.21.1") (d #t) (k 0)))) (h "13kf5mq83pqxxp970y6hyk6nidj4p7qzlwqxm37nrrv7vrf9br4k") (y #t)))

