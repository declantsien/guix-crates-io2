(define-module (crates-io al tv altv_internal_core_shared) #:use-module (crates-io))

(define-public crate-altv_internal_core_shared-0.3.1 (c (n "altv_internal_core_shared") (v "0.3.1") (d (list (d (n "altv_sdk") (r "^0.3.0") (d #t) (k 0) (p "altv_internal_sdk")))) (h "0606iyp0kq4x436zzan7y4x5mydqg0nclgy2lgcvvx00yxvn4wma") (y #t) (r "1.68.2")))

(define-public crate-altv_internal_core_shared-15.0.0-dev.0 (c (n "altv_internal_core_shared") (v "15.0.0-dev.0") (d (list (d (n "altv_sdk") (r "^15.0.0-dev.0") (d #t) (k 0) (p "altv_internal_sdk")))) (h "10jf4dsc1vaanar14kz2nv04ln9nfjgq1bmj12hqb8vvk6x4ygl0") (y #t) (r "1.68.2")))

(define-public crate-altv_internal_core_shared-15.0.0-dev.1 (c (n "altv_internal_core_shared") (v "15.0.0-dev.1") (d (list (d (n "altv_sdk") (r "^15.0.0-dev.1") (d #t) (k 0) (p "altv_internal_sdk")))) (h "0rv6h61adr2xmrk3k6j95mwn4h0zs42vfmxxh77ciq4nfx27nyl5") (y #t) (r "1.68.2")))

(define-public crate-altv_internal_core_shared-15.0.0-dev2 (c (n "altv_internal_core_shared") (v "15.0.0-dev2") (d (list (d (n "altv_sdk") (r "^15.0.0-dev2") (d #t) (k 0) (p "altv_internal_sdk")))) (h "1p9hrnakb5c7mpw8w0f31nm9m43157f6z1r0iv0f8lnwsq0gx01v") (y #t) (r "1.68.2")))

(define-public crate-altv_internal_core_shared-15.0.0-dev3 (c (n "altv_internal_core_shared") (v "15.0.0-dev3") (d (list (d (n "altv_sdk") (r "^15.0.0-dev3") (d #t) (k 0) (p "altv_internal_sdk")))) (h "1fpq39ikl1g2rfpx9bf7wkzzrcnnvpv4fmk3fs99j9pmsam4c3n3") (y #t) (r "1.68.2")))

(define-public crate-altv_internal_core_shared-15.0.0-dev4 (c (n "altv_internal_core_shared") (v "15.0.0-dev4") (d (list (d (n "altv_sdk") (r "^15.0.0-dev4") (d #t) (k 0) (p "altv_internal_sdk")))) (h "03dqhnq5ap1ghsdid3hvwc4sy94069nmayajjqllashw1ml43c51") (y #t) (r "1.68.2")))

(define-public crate-altv_internal_core_shared-15.0.0-dev5 (c (n "altv_internal_core_shared") (v "15.0.0-dev5") (d (list (d (n "altv_sdk") (r "^15.0.0-dev5") (d #t) (k 0) (p "altv_internal_sdk")))) (h "0xggmc8z2zxhc8dw5dpgxv144n32k24hx3wcq0yb48y643vjhn9j") (y #t) (r "1.68.2")))

(define-public crate-altv_internal_core_shared-15.0.0-dev6 (c (n "altv_internal_core_shared") (v "15.0.0-dev6") (d (list (d (n "altv_sdk") (r "^15.0.0-dev6") (d #t) (k 0) (p "altv_internal_sdk")))) (h "1mdj40py0qykl2m9xvdd49ga7d92700gn7zgbisshd15drcdzim1") (y #t) (r "1.68.2")))

(define-public crate-altv_internal_core_shared-15.0.0-dev7 (c (n "altv_internal_core_shared") (v "15.0.0-dev7") (d (list (d (n "altv_sdk") (r "^15.0.0-dev7") (d #t) (k 0) (p "altv_internal_sdk")))) (h "0c20fz4xpzcgmnwfam00wpq2f56dzfm7smk6v72ryy0xqlrg6nab") (y #t) (r "1.68.2")))

(define-public crate-altv_internal_core_shared-15.0.0-dev8 (c (n "altv_internal_core_shared") (v "15.0.0-dev8") (d (list (d (n "altv_sdk") (r "^15.0.0-dev8") (d #t) (k 0) (p "altv_internal_sdk")))) (h "14vnylqnp0q2irqlf0bjvyflbk077znymprywfizgwjd47ch89cf") (y #t) (r "1.68.2")))

(define-public crate-altv_internal_core_shared-15.0.0-dev9 (c (n "altv_internal_core_shared") (v "15.0.0-dev9") (d (list (d (n "altv_sdk") (r "^15.0.0-dev9") (d #t) (k 0) (p "altv_internal_sdk")))) (h "17hd1w0fyrb5s76kfqjnx79nk35knpjpcqqq8cvnm69ngb80jgf9") (y #t) (r "1.68.2")))

(define-public crate-altv_internal_core_shared-15.0.0-dev.10 (c (n "altv_internal_core_shared") (v "15.0.0-dev.10") (d (list (d (n "altv_sdk") (r "^15.0.0-dev.10") (d #t) (k 0) (p "altv_internal_sdk")))) (h "01yxrvg8d16dy3sib9qn9xifww4nxkkvzbzf5g23izzf23q0v81z") (r "1.68.2")))

(define-public crate-altv_internal_core_shared-15.0.0-dev.11 (c (n "altv_internal_core_shared") (v "15.0.0-dev.11") (d (list (d (n "altv_sdk") (r "^15.0.0-dev.11") (d #t) (k 0) (p "altv_internal_sdk")))) (h "1ncky8kl3ijcyclln6sy0i8vzmnpparcrmbz82fplpk095xwgbk9") (r "1.68.2")))

(define-public crate-altv_internal_core_shared-15.0.0-dev.12 (c (n "altv_internal_core_shared") (v "15.0.0-dev.12") (d (list (d (n "altv_sdk") (r "^15.0.0-dev.12") (d #t) (k 0) (p "altv_internal_sdk")))) (h "03bnhaw4z1z5dvrjm1ilfm38f1kinl6w0gvmrad92aggxx5hbgav") (r "1.68.2")))

(define-public crate-altv_internal_core_shared-15.0.0-dev.13 (c (n "altv_internal_core_shared") (v "15.0.0-dev.13") (d (list (d (n "altv_sdk") (r "^15.0.0-dev.13") (d #t) (k 0) (p "altv_internal_sdk")))) (h "11hmhji4jfx432m9bsfnd49jd8j40b9misk1ld1ai5i4vil8jgv3") (r "1.68.2")))

(define-public crate-altv_internal_core_shared-15.0.0-dev.14 (c (n "altv_internal_core_shared") (v "15.0.0-dev.14") (d (list (d (n "altv_sdk") (r "^15.0.0-dev.14") (d #t) (k 0) (p "altv_internal_sdk")))) (h "1aws67x5ln92453p8c5x8lwkw4aibwv4ibqyi7mwxkh6y7cr5jhv") (r "1.68.2")))

(define-public crate-altv_internal_core_shared-15.0.0-dev.15 (c (n "altv_internal_core_shared") (v "15.0.0-dev.15") (d (list (d (n "altv_sdk") (r "^15.0.0-dev.15") (d #t) (k 0) (p "altv_internal_sdk")))) (h "0bw6024cvz11wrmlxkdkg57k2vcigg4jimf78m6fhmchj0sxji46") (r "1.68.2")))

(define-public crate-altv_internal_core_shared-15.0.0-dev.16 (c (n "altv_internal_core_shared") (v "15.0.0-dev.16") (d (list (d (n "altv_sdk") (r "^15.0.0-dev.16") (d #t) (k 0) (p "altv_internal_sdk")))) (h "1z1jf7zxc28dr7rd52qia1b1h1d2app68a3kqdgsirnh4nwwcwlg") (r "1.68.2")))

(define-public crate-altv_internal_core_shared-15.0.0-dev.17 (c (n "altv_internal_core_shared") (v "15.0.0-dev.17") (d (list (d (n "altv_sdk") (r "^15.0.0-dev.17") (d #t) (k 0) (p "altv_internal_sdk")))) (h "1pxrpayldz5h6sg28f4pvczfjy4wc4p0amccnr61ff0fhv2hd7w3") (r "1.68.2")))

(define-public crate-altv_internal_core_shared-15.0.0-dev.18 (c (n "altv_internal_core_shared") (v "15.0.0-dev.18") (d (list (d (n "altv_sdk") (r "^15.0.0-dev.18") (d #t) (k 0) (p "altv_internal_sdk")) (d (n "anyhow") (r "^0.0.1") (d #t) (k 0) (p "lmnkjgkj-std-backtrace-anyhow")))) (h "13774acd2hzdp1a6hgb42x1y84aks3fxvk46yf6rfi9g44m3i19c") (r "1.68.2")))

(define-public crate-altv_internal_core_shared-15.0.0-dev.19 (c (n "altv_internal_core_shared") (v "15.0.0-dev.19") (d (list (d (n "altv_sdk") (r "^15.0.0-dev.19") (d #t) (k 0) (p "altv_internal_sdk")) (d (n "anyhow") (r "^0.0.1") (d #t) (k 0) (p "lmnkjgkj-std-backtrace-anyhow")))) (h "0zyn34jcz8gkhapww1kzxrhdw18rgkncmj24j8g6y8166l7jc1rf") (r "1.68.2")))

(define-public crate-altv_internal_core_shared-15.0.0-dev.20 (c (n "altv_internal_core_shared") (v "15.0.0-dev.20") (d (list (d (n "altv_sdk") (r "^15.0.0-dev.20") (d #t) (k 0) (p "altv_internal_sdk")) (d (n "anyhow") (r "^0.0.1") (d #t) (k 0) (p "lmnkjgkj-std-backtrace-anyhow")))) (h "1q1vdndzi4rxmwdzyvw3im9z8gkzngl64djv6687hf8cf2xdcf1v") (r "1.68.2")))

(define-public crate-altv_internal_core_shared-15.0.0-dev.21 (c (n "altv_internal_core_shared") (v "15.0.0-dev.21") (d (list (d (n "altv_sdk") (r "^15.0.0-dev.21") (d #t) (k 0) (p "altv_internal_sdk")) (d (n "anyhow") (r "^0.0.1") (d #t) (k 0) (p "lmnkjgkj-std-backtrace-anyhow")))) (h "1cvdnzigs8vaa7c991kz3vnaznm0y2har8mkxzipzdm8gjplbqk9") (r "1.68.2")))

(define-public crate-altv_internal_core_shared-15.0.0-dev.22 (c (n "altv_internal_core_shared") (v "15.0.0-dev.22") (d (list (d (n "altv_sdk") (r "^15.0.0-dev.22") (d #t) (k 0) (p "altv_internal_sdk")) (d (n "anyhow") (r "^0.0.1") (d #t) (k 0) (p "lmnkjgkj-std-backtrace-anyhow")))) (h "0dyzw3y8ya9nya68rcgagayz7ncic1kynzb4ccf2gid93s25m01z") (r "1.68.2")))

(define-public crate-altv_internal_core_shared-15.0.0-dev.23 (c (n "altv_internal_core_shared") (v "15.0.0-dev.23") (d (list (d (n "altv_sdk") (r "^15.0.0-dev.23") (d #t) (k 0) (p "altv_internal_sdk")) (d (n "anyhow") (r "^0.0.1") (d #t) (k 0) (p "lmnkjgkj-std-backtrace-anyhow")))) (h "0714clcbpxa3xm5rn51d4i70nfh36c42ijxq0hzw69kvyiyl8ik9") (r "1.68.2")))

(define-public crate-altv_internal_core_shared-15.0.0-dev.24 (c (n "altv_internal_core_shared") (v "15.0.0-dev.24") (d (list (d (n "altv_sdk") (r "^15.0.0-dev.24") (d #t) (k 0) (p "altv_internal_sdk")) (d (n "anyhow") (r "^0.0.1") (d #t) (k 0) (p "lmnkjgkj-std-backtrace-anyhow")))) (h "0y2nrrlxkcs6mi97wrhj8x4103afy2v9q6ik5jw1wnli4v60qa1j") (r "1.68.2")))

(define-public crate-altv_internal_core_shared-15.0.0-dev.25 (c (n "altv_internal_core_shared") (v "15.0.0-dev.25") (d (list (d (n "altv_sdk") (r "^15.0.0-dev.25") (d #t) (k 0) (p "altv_internal_sdk")) (d (n "anyhow") (r "^0.0.1") (d #t) (k 0) (p "lmnkjgkj-std-backtrace-anyhow")))) (h "0i8lcx4c1s1zjj6iyrcha7d21hnbx6w7fm9j69dn75c05q3wqm75") (r "1.68.2")))

(define-public crate-altv_internal_core_shared-15.0.0-dev.26 (c (n "altv_internal_core_shared") (v "15.0.0-dev.26") (d (list (d (n "altv_sdk") (r "^15.0.0-dev.26") (d #t) (k 0) (p "altv_internal_sdk")) (d (n "anyhow") (r "^0.0.1") (d #t) (k 0) (p "lmnkjgkj-std-backtrace-anyhow")))) (h "1yiskls4msy4070gwqwml0w7kgw8rz3n2s2ihh8dnlfnpmfs88g5") (r "1.68.2")))

(define-public crate-altv_internal_core_shared-15.0.0-dev.27 (c (n "altv_internal_core_shared") (v "15.0.0-dev.27") (d (list (d (n "altv_sdk") (r "^15.0.0-dev.27") (d #t) (k 0) (p "altv_internal_sdk")) (d (n "anyhow") (r "^0.0.1") (d #t) (k 0) (p "lmnkjgkj-std-backtrace-anyhow")))) (h "170p74r752c6k5gkcra2nj7y64vr0pjqqhag3q37iy37b4rgg8g8") (r "1.68.2")))

(define-public crate-altv_internal_core_shared-15.0.0-dev.28 (c (n "altv_internal_core_shared") (v "15.0.0-dev.28") (d (list (d (n "altv_sdk") (r "^15.0.0-dev.28") (d #t) (k 0) (p "altv_internal_sdk")) (d (n "anyhow") (r "^0.0.1") (d #t) (k 0) (p "lmnkjgkj-std-backtrace-anyhow")))) (h "1y6gr95sbhf2b0k4m3a7ky7xq2mhkcp67v0nhh2grgrkp9d5qsck") (r "1.68.2")))

(define-public crate-altv_internal_core_shared-15.0.0-dev.29 (c (n "altv_internal_core_shared") (v "15.0.0-dev.29") (d (list (d (n "altv_sdk") (r "^15.0.0-dev.29") (d #t) (k 0) (p "altv_internal_sdk")) (d (n "anyhow") (r "^0.0.1") (d #t) (k 0) (p "lmnkjgkj-std-backtrace-anyhow")))) (h "0njnwi9605wxg3jqgya0i102yy00zxj9xkkvdwkzjzs6xwh8n80v") (r "1.68.2")))

(define-public crate-altv_internal_core_shared-15.0.0-dev.30 (c (n "altv_internal_core_shared") (v "15.0.0-dev.30") (d (list (d (n "altv_sdk") (r "^15.0.0-dev.30") (d #t) (k 0) (p "altv_internal_sdk")) (d (n "anyhow") (r "^0.0.1") (d #t) (k 0) (p "lmnkjgkj-std-backtrace-anyhow")))) (h "16nai3h955wcq28i5rxb07kpgbi8zmr2jxf4n46bnx1aijfmbd5g") (r "1.68.2")))

(define-public crate-altv_internal_core_shared-15.0.0-dev.31 (c (n "altv_internal_core_shared") (v "15.0.0-dev.31") (d (list (d (n "altv_sdk") (r "^15.0.0-dev.31") (d #t) (k 0) (p "altv_internal_sdk")) (d (n "anyhow") (r "^0.0.1") (d #t) (k 0) (p "lmnkjgkj-std-backtrace-anyhow")))) (h "1xbg2xrdq4xqs6l16fczd4blb1fgngi9y69yxqxp49p201ykv0zs") (r "1.68.2")))

(define-public crate-altv_internal_core_shared-15.0.0-dev.32 (c (n "altv_internal_core_shared") (v "15.0.0-dev.32") (d (list (d (n "altv_sdk") (r "^15.0.0-dev.32") (d #t) (k 0) (p "altv_internal_sdk")) (d (n "anyhow") (r "^0.0.1") (d #t) (k 0) (p "lmnkjgkj-std-backtrace-anyhow")))) (h "1bb6vrr6kc59rqls1p48afhwvv0mlclpy3a1bnnr2xzllw8rirf8") (r "1.68.2")))

(define-public crate-altv_internal_core_shared-15.0.0-dev.33 (c (n "altv_internal_core_shared") (v "15.0.0-dev.33") (d (list (d (n "altv_sdk") (r "^15.0.0-dev.33") (d #t) (k 0) (p "altv_internal_sdk")) (d (n "anyhow") (r "^0.0.1") (d #t) (k 0) (p "lmnkjgkj-std-backtrace-anyhow")))) (h "115w7bnv1s8xvdvw1wy260zlygsf2qkfdcmm605pwqz054sd0ypk") (r "1.68.2")))

(define-public crate-altv_internal_core_shared-15.0.0-dev.34 (c (n "altv_internal_core_shared") (v "15.0.0-dev.34") (d (list (d (n "altv_sdk") (r "^15.0.0-dev.34") (d #t) (k 0) (p "altv_internal_sdk")) (d (n "anyhow") (r "^0.0.1") (d #t) (k 0) (p "lmnkjgkj-std-backtrace-anyhow")))) (h "1i609mpyrs1qnvj9d27jisqvaq89d13wfnpidsddc6wn5j12hc1x") (r "1.68.2")))

(define-public crate-altv_internal_core_shared-15.0.0-dev.35 (c (n "altv_internal_core_shared") (v "15.0.0-dev.35") (d (list (d (n "altv_sdk") (r "^15.0.0-dev.35") (d #t) (k 0) (p "altv_internal_sdk")) (d (n "anyhow") (r "^0.0.1") (d #t) (k 0) (p "lmnkjgkj-std-backtrace-anyhow")))) (h "0yrhj824b6y0g1yka76ygw8rdhbiczjnmz1v97haps8mrwc7q5v2") (r "1.68.2")))

(define-public crate-altv_internal_core_shared-15.0.0-dev.36 (c (n "altv_internal_core_shared") (v "15.0.0-dev.36") (d (list (d (n "altv_sdk") (r "^15.0.0-dev.36") (d #t) (k 0) (p "altv_internal_sdk")) (d (n "anyhow") (r "^0.0.1") (d #t) (k 0) (p "lmnkjgkj-std-backtrace-anyhow")))) (h "1jffxm54l9mkc4ipnixaz2gy5dxbxjypnhjrrgdfhba4384cxx5a") (r "1.68.2")))

(define-public crate-altv_internal_core_shared-15.0.0-dev.37 (c (n "altv_internal_core_shared") (v "15.0.0-dev.37") (d (list (d (n "altv_sdk") (r "^15.0.0-dev.37") (d #t) (k 0) (p "altv_internal_sdk")) (d (n "anyhow") (r "^0.0.1") (d #t) (k 0) (p "lmnkjgkj-std-backtrace-anyhow")))) (h "05gw151zscndk553mjdha6j6i9i76bqsp65rcmz791dcddpaa496") (r "1.68.2")))

(define-public crate-altv_internal_core_shared-15.0.0-dev.38 (c (n "altv_internal_core_shared") (v "15.0.0-dev.38") (d (list (d (n "altv_sdk") (r "^15.0.0-dev.38") (d #t) (k 0) (p "altv_internal_sdk")) (d (n "anyhow") (r "^0.0.1") (d #t) (k 0) (p "lmnkjgkj-std-backtrace-anyhow")))) (h "0s9pfjvd6cfmn2yflzlj2r8iz3n0nl4djc5cb2b0v703fihqigwg") (r "1.68.2")))

(define-public crate-altv_internal_core_shared-15.0.0-dev.39 (c (n "altv_internal_core_shared") (v "15.0.0-dev.39") (d (list (d (n "altv_sdk") (r "^15.0.0-dev.39") (d #t) (k 0) (p "altv_internal_sdk")) (d (n "anyhow") (r "^0.0.1") (d #t) (k 0) (p "lmnkjgkj-std-backtrace-anyhow")))) (h "0ssx83i9q4sjh2mr4m9g2rlns25ixm9lk6d11v01i3syk0z6p307") (r "1.68.2")))

(define-public crate-altv_internal_core_shared-15.0.0-dev.40 (c (n "altv_internal_core_shared") (v "15.0.0-dev.40") (d (list (d (n "altv_sdk") (r "^15.0.0-dev.40") (d #t) (k 0) (p "altv_internal_sdk")) (d (n "anyhow") (r "^0.0.1") (d #t) (k 0) (p "lmnkjgkj-std-backtrace-anyhow")))) (h "0xms8gi9ashhavnfd2m1n1hkmg5m8y11w81c6icxv958nvskyc4s") (r "1.68.2")))

(define-public crate-altv_internal_core_shared-15.0.0-dev.41 (c (n "altv_internal_core_shared") (v "15.0.0-dev.41") (d (list (d (n "altv_sdk") (r "^15.0.0-dev.41") (d #t) (k 0) (p "altv_internal_sdk")) (d (n "anyhow") (r "^0.0.1") (d #t) (k 0) (p "lmnkjgkj-std-backtrace-anyhow")))) (h "00nhqdywv6pf318wffrhhzm52q550bgi096rvd54bm5sy072z4k8") (r "1.68.2")))

(define-public crate-altv_internal_core_shared-15.0.0-dev42 (c (n "altv_internal_core_shared") (v "15.0.0-dev42") (d (list (d (n "altv_sdk") (r "^15.0.0-dev42") (d #t) (k 0) (p "altv_internal_sdk")) (d (n "anyhow") (r "^0.0.1") (d #t) (k 0) (p "lmnkjgkj-std-backtrace-anyhow")))) (h "00h2k2in4c7006nq7qwpd5080f55yx8d2li3r7iicynhvllxlj97") (y #t) (r "1.68.2")))

(define-public crate-altv_internal_core_shared-15.0.0-dev.43 (c (n "altv_internal_core_shared") (v "15.0.0-dev.43") (d (list (d (n "altv_sdk") (r "^15.0.0-dev.43") (d #t) (k 0) (p "altv_internal_sdk")) (d (n "anyhow") (r "^0.0.1") (d #t) (k 0) (p "lmnkjgkj-std-backtrace-anyhow")))) (h "150db8l2781982ad7rgs0k9i2fz3ja8ahlhxhj59xvy4c34iamf6") (y #t) (r "1.68.2")))

(define-public crate-altv_internal_core_shared-15.0.0-dev.44 (c (n "altv_internal_core_shared") (v "15.0.0-dev.44") (d (list (d (n "altv_sdk") (r "^15.0.0-dev.44") (d #t) (k 0) (p "altv_internal_sdk")) (d (n "anyhow") (r "^0.0.1") (d #t) (k 0) (p "lmnkjgkj-std-backtrace-anyhow")))) (h "0k1g2fkpsrnndvs2y7wj9qhbdfwmylqklhq4arma21yzxr824x8k") (y #t) (r "1.68.2")))

(define-public crate-altv_internal_core_shared-15.0.0-dev.45 (c (n "altv_internal_core_shared") (v "15.0.0-dev.45") (d (list (d (n "altv_sdk") (r "^15.0.0-dev.45") (d #t) (k 0) (p "altv_internal_sdk")) (d (n "anyhow") (r "^0.0.1") (d #t) (k 0) (p "lmnkjgkj-std-backtrace-anyhow")))) (h "1jhrzkkdnby8dzw0y2qmm2knsx68qs91j59hx8g4akzsfddk3xg7") (r "1.68.2")))

(define-public crate-altv_internal_core_shared-15.0.0-dev.46 (c (n "altv_internal_core_shared") (v "15.0.0-dev.46") (d (list (d (n "altv_sdk") (r "^15.0.0-dev.46") (d #t) (k 0) (p "altv_internal_sdk")) (d (n "anyhow") (r "^0.0.1") (d #t) (k 0) (p "lmnkjgkj-std-backtrace-anyhow")))) (h "1p2f6si9dxwc3582d5lic553rskb6c3w27hl8w6bmiasp0539n8q") (r "1.68.2")))

(define-public crate-altv_internal_core_shared-15.0.0-dev.47 (c (n "altv_internal_core_shared") (v "15.0.0-dev.47") (d (list (d (n "altv_sdk") (r "^15.0.0-dev.47") (d #t) (k 0) (p "altv_internal_sdk")) (d (n "anyhow") (r "^0.0.1") (d #t) (k 0) (p "lmnkjgkj-std-backtrace-anyhow")))) (h "1qffb65bs8cyic1kjk972qkw8slgrsmwc0azi9jiiv8hkg9fs093") (r "1.68.2")))

(define-public crate-altv_internal_core_shared-15.0.0-dev.48 (c (n "altv_internal_core_shared") (v "15.0.0-dev.48") (d (list (d (n "altv_sdk") (r "^15.0.0-dev.48") (d #t) (k 0) (p "altv_internal_sdk")) (d (n "anyhow") (r "^0.0.1") (d #t) (k 0) (p "lmnkjgkj-std-backtrace-anyhow")))) (h "11h258v649gd56vppjzb29h0fpkk24n9klcafzb7ligydyl37i1m") (r "1.68.2")))

(define-public crate-altv_internal_core_shared-15.0.0-dev.49 (c (n "altv_internal_core_shared") (v "15.0.0-dev.49") (d (list (d (n "altv_sdk") (r "^15.0.0-dev.49") (d #t) (k 0) (p "altv_internal_sdk")) (d (n "anyhow") (r "^0.0.1") (d #t) (k 0) (p "lmnkjgkj-std-backtrace-anyhow")))) (h "0k17m47w3s9z1vrw6sgw7av2l9l4agml3gggly72ga81da27ri0w") (r "1.68.2")))

(define-public crate-altv_internal_core_shared-15.0.0-dev.50 (c (n "altv_internal_core_shared") (v "15.0.0-dev.50") (d (list (d (n "altv_sdk") (r "^15.0.0-dev.50") (d #t) (k 0) (p "altv_internal_sdk")) (d (n "anyhow") (r "^0.0.1") (d #t) (k 0) (p "lmnkjgkj-std-backtrace-anyhow")))) (h "1393v0z7fpy1583xyj507b38g950rdkimqi8dkmya362y2465rxa") (r "1.68.2")))

(define-public crate-altv_internal_core_shared-15.0.0-dev.51 (c (n "altv_internal_core_shared") (v "15.0.0-dev.51") (d (list (d (n "altv_sdk") (r "^15.0.0-dev.51") (d #t) (k 0) (p "altv_internal_sdk")) (d (n "anyhow") (r "^0.0.1") (d #t) (k 0) (p "lmnkjgkj-std-backtrace-anyhow")))) (h "0rsh7mm1jvgasivcb36gcj8pql21rr3rbyxq4c3hgbszai8fq7jw") (r "1.68.2")))

(define-public crate-altv_internal_core_shared-15.0.0-dev.52 (c (n "altv_internal_core_shared") (v "15.0.0-dev.52") (d (list (d (n "altv_sdk") (r "^15.0.0-dev.52") (d #t) (k 0) (p "altv_internal_sdk")) (d (n "anyhow") (r "^0.0.1") (d #t) (k 0) (p "lmnkjgkj-std-backtrace-anyhow")))) (h "132fi30v34kp5082s9j74w6yg73y0kyhrkppn5yk06q5fls2w25d") (r "1.68.2")))

(define-public crate-altv_internal_core_shared-15.0.0-dev.54 (c (n "altv_internal_core_shared") (v "15.0.0-dev.54") (d (list (d (n "altv_sdk") (r "^15.0.0-dev.54") (d #t) (k 0) (p "altv_internal_sdk")) (d (n "anyhow") (r "^0.0.1") (d #t) (k 0) (p "lmnkjgkj-std-backtrace-anyhow")))) (h "0iwp4f3lp347n80lhm3yd2cg8amyx7g2ijsa57x6hpzr9bd5v2v2") (r "1.68.2")))

(define-public crate-altv_internal_core_shared-15.0.0-dev.55 (c (n "altv_internal_core_shared") (v "15.0.0-dev.55") (d (list (d (n "altv_sdk") (r "^15.0.0-dev.55") (d #t) (k 0) (p "altv_internal_sdk")) (d (n "anyhow") (r "^0.0.1") (d #t) (k 0) (p "lmnkjgkj-std-backtrace-anyhow")))) (h "0zxccc2wcm9dviiqqizfs2nil4xr1ff8ikvfx8gphx33flkbz7f5") (r "1.68.2")))

(define-public crate-altv_internal_core_shared-15.0.0-dev.56 (c (n "altv_internal_core_shared") (v "15.0.0-dev.56") (d (list (d (n "altv_sdk") (r "^15.0.0-dev.56") (d #t) (k 0) (p "altv_internal_sdk")) (d (n "anyhow") (r "^0.0.1") (d #t) (k 0) (p "lmnkjgkj-std-backtrace-anyhow")))) (h "0akl9bs5xxy7f72lw836hxjyfix5dlx9c4f4yykx1w9gkrxaiixw") (r "1.68.2")))

(define-public crate-altv_internal_core_shared-15.0.0-dev.57 (c (n "altv_internal_core_shared") (v "15.0.0-dev.57") (d (list (d (n "altv_sdk") (r "^15.0.0-dev.57") (d #t) (k 0) (p "altv_internal_sdk")) (d (n "anyhow") (r "^0.0.1") (d #t) (k 0) (p "lmnkjgkj-std-backtrace-anyhow")))) (h "0i6qafw0f21d8mydpma5kviv8pm5hvlsn10mmrl9ig50dvr4gnyp") (r "1.68.2")))

(define-public crate-altv_internal_core_shared-15.0.0-dev.58 (c (n "altv_internal_core_shared") (v "15.0.0-dev.58") (d (list (d (n "altv_sdk") (r "^15.0.0-dev.58") (d #t) (k 0) (p "altv_internal_sdk")) (d (n "anyhow") (r "^0.0.1") (d #t) (k 0) (p "lmnkjgkj-std-backtrace-anyhow")))) (h "0mhvnjmx9lzb2b4npyasd3vm8hfnm52y2b9fyb4a5wyaii91kqn8") (r "1.68.2")))

(define-public crate-altv_internal_core_shared-15.0.0-dev.59 (c (n "altv_internal_core_shared") (v "15.0.0-dev.59") (d (list (d (n "altv_sdk") (r "^15.0.0-dev.59") (d #t) (k 0) (p "altv_internal_sdk")) (d (n "anyhow") (r "^0.0.1") (d #t) (k 0) (p "lmnkjgkj-std-backtrace-anyhow")))) (h "19f6jfjsjpn3r3pf102ixix2kb9dh01g96czybss6zgbnkr68h3v") (r "1.68.2")))

(define-public crate-altv_internal_core_shared-15.0.0-dev.60 (c (n "altv_internal_core_shared") (v "15.0.0-dev.60") (d (list (d (n "altv_sdk") (r "^15.0.0-dev.60") (d #t) (k 0) (p "altv_internal_sdk")) (d (n "anyhow") (r "^0.0.1") (d #t) (k 0) (p "lmnkjgkj-std-backtrace-anyhow")))) (h "0pv3gr594pnsfh0dfwkyxi0nzq7d9plwaalgy2a3j0phh7l97fzj") (r "1.68.2")))

(define-public crate-altv_internal_core_shared-15.0.0-dev.61 (c (n "altv_internal_core_shared") (v "15.0.0-dev.61") (d (list (d (n "altv_sdk") (r "^15.0.0-dev.61") (d #t) (k 0) (p "altv_internal_sdk")) (d (n "anyhow") (r "^0.0.1") (d #t) (k 0) (p "lmnkjgkj-std-backtrace-anyhow")))) (h "1cf2fk21516alqa6cjpd370rg283sjyri1yb0apyky4gypjs91c5") (r "1.68.2")))

(define-public crate-altv_internal_core_shared-15.0.0-dev.62 (c (n "altv_internal_core_shared") (v "15.0.0-dev.62") (d (list (d (n "altv_sdk") (r "^15.0.0-dev.62") (d #t) (k 0) (p "altv_internal_sdk")) (d (n "anyhow") (r "^0.0.1") (d #t) (k 0) (p "lmnkjgkj-std-backtrace-anyhow")))) (h "0wdl8yy27b89nasbhp35k9if9lspx52x114ga9ji27qw1j8s61hr") (r "1.68.2")))

(define-public crate-altv_internal_core_shared-15.0.0-rc.1 (c (n "altv_internal_core_shared") (v "15.0.0-rc.1") (d (list (d (n "altv_sdk") (r "^15.0.0-rc.1") (d #t) (k 0) (p "altv_internal_sdk")) (d (n "anyhow") (r "^0.0.1") (d #t) (k 0) (p "lmnkjgkj-std-backtrace-anyhow")))) (h "1vn4ydbf6330vd4wqvwm5n32nlf2csrlkaiiv8vncjbjb2dn7aq0") (r "1.68.2")))

(define-public crate-altv_internal_core_shared-15.0.0-dev.63 (c (n "altv_internal_core_shared") (v "15.0.0-dev.63") (d (list (d (n "altv_sdk") (r "^15.0.0-dev.63") (d #t) (k 0) (p "altv_internal_sdk")) (d (n "anyhow") (r "^0.0.1") (d #t) (k 0) (p "lmnkjgkj-std-backtrace-anyhow")))) (h "1mn9wg29xvksndk6hccl9bcvlci65bhyr475a42a9cmf2g20a2ig") (r "1.68.2")))

(define-public crate-altv_internal_core_shared-15.0.0-dev.64 (c (n "altv_internal_core_shared") (v "15.0.0-dev.64") (d (list (d (n "altv_sdk") (r "^15.0.0-dev.64") (d #t) (k 0) (p "altv_internal_sdk")) (d (n "anyhow") (r "^0.0.1") (d #t) (k 0) (p "lmnkjgkj-std-backtrace-anyhow")))) (h "1rrgzsi5zz4ykq40aqvzq66l1g9bfwafqy19irnr342mmigc34hf") (r "1.68.2")))

(define-public crate-altv_internal_core_shared-15.0.0-dev.65 (c (n "altv_internal_core_shared") (v "15.0.0-dev.65") (d (list (d (n "altv_sdk") (r "=15.0.0-dev.65") (d #t) (k 0) (p "altv_internal_sdk")) (d (n "anyhow") (r "^0.0.1") (d #t) (k 0) (p "lmnkjgkj-std-backtrace-anyhow")))) (h "156mrsf8xl6wjwwb3g4sw5648n66b9jl341qqlyby4b51hi29rw7") (r "1.68.2")))

(define-public crate-altv_internal_core_shared-15.0.0-dev.67 (c (n "altv_internal_core_shared") (v "15.0.0-dev.67") (d (list (d (n "altv_sdk") (r "=15.0.0-dev.67") (d #t) (k 0) (p "altv_internal_sdk")) (d (n "anyhow") (r "^0.0.1") (d #t) (k 0) (p "lmnkjgkj-std-backtrace-anyhow")))) (h "1j0jlh92ki4lakpks71lq8lnw553hy7ifn8481bk6dxqgcfha3l4") (r "1.68.2")))

(define-public crate-altv_internal_core_shared-15.0.0-rc.2 (c (n "altv_internal_core_shared") (v "15.0.0-rc.2") (d (list (d (n "altv_sdk") (r "=15.0.0-rc.2") (d #t) (k 0) (p "altv_internal_sdk")) (d (n "anyhow") (r "^0.0.1") (d #t) (k 0) (p "lmnkjgkj-std-backtrace-anyhow")))) (h "1hvbwhpq2bhzd7kgjlwyr9i3gjnnqypfpykh7k7iyw7sq0dm7gk8") (r "1.68.2")))

(define-public crate-altv_internal_core_shared-15.0.0-dev.68 (c (n "altv_internal_core_shared") (v "15.0.0-dev.68") (d (list (d (n "altv_sdk") (r "=15.0.0-dev.68") (d #t) (k 0) (p "altv_internal_sdk")) (d (n "anyhow") (r "^0.0.1") (d #t) (k 0) (p "lmnkjgkj-std-backtrace-anyhow")))) (h "0wk1g8yp26dfm9dvd91kyz9kzm5ynf63dvppl8inqw60y5gdgp8i") (r "1.68.2")))

(define-public crate-altv_internal_core_shared-15.0.0-dev.69 (c (n "altv_internal_core_shared") (v "15.0.0-dev.69") (d (list (d (n "altv_sdk") (r "=15.0.0-dev.69") (d #t) (k 0) (p "altv_internal_sdk")) (d (n "anyhow") (r "^0.0.1") (d #t) (k 0) (p "lmnkjgkj-std-backtrace-anyhow")))) (h "0yqlv532hczqbcygnibrjx9zq7ws73rdpf2q2b149cwlaadhxik8") (r "1.68.2")))

(define-public crate-altv_internal_core_shared-15.0.0-rc.3 (c (n "altv_internal_core_shared") (v "15.0.0-rc.3") (d (list (d (n "altv_sdk") (r "=15.0.0-rc.3") (d #t) (k 0) (p "altv_internal_sdk")) (d (n "anyhow") (r "^0.0.1") (d #t) (k 0) (p "lmnkjgkj-std-backtrace-anyhow")))) (h "1nk0qsv6klnydqa8wdyd95j7r2fdcd7ydfpyca8wh1y5h0ik3nim") (r "1.68.2")))

(define-public crate-altv_internal_core_shared-15.0.0-dev.70 (c (n "altv_internal_core_shared") (v "15.0.0-dev.70") (d (list (d (n "altv_sdk") (r "=15.0.0-dev.70") (d #t) (k 0) (p "altv_internal_sdk")) (d (n "anyhow") (r "^0.0.1") (d #t) (k 0) (p "lmnkjgkj-std-backtrace-anyhow")))) (h "02y9cyis19bswq2xs1s3lsjgndi63xr0kw5fqnm3kqivs30rkmvi") (r "1.68.2")))

(define-public crate-altv_internal_core_shared-15.0.0-rc.4 (c (n "altv_internal_core_shared") (v "15.0.0-rc.4") (d (list (d (n "altv_sdk") (r "=15.0.0-rc.4") (d #t) (k 0) (p "altv_internal_sdk")) (d (n "anyhow") (r "^0.0.1") (d #t) (k 0) (p "lmnkjgkj-std-backtrace-anyhow")))) (h "0ymz7747d6zz7hggbhq0aiax829f1pvmqnm8iv2ks5b8y7qyh86h") (r "1.68.2")))

(define-public crate-altv_internal_core_shared-15.0.0-dev.71 (c (n "altv_internal_core_shared") (v "15.0.0-dev.71") (d (list (d (n "altv_sdk") (r "=15.0.0-dev.71") (d #t) (k 0) (p "altv_internal_sdk")) (d (n "anyhow") (r "^0.0.1") (d #t) (k 0) (p "lmnkjgkj-std-backtrace-anyhow")))) (h "00r5az8kf2qk9lv2iin74kkprwrzr9zhczhy5sj4jk36ajilvk8f") (r "1.68.2")))

(define-public crate-altv_internal_core_shared-15.0.0-dev.72 (c (n "altv_internal_core_shared") (v "15.0.0-dev.72") (d (list (d (n "altv_sdk") (r "=15.0.0-dev.72") (d #t) (k 0) (p "altv_internal_sdk")) (d (n "anyhow") (r "^0.0.1") (d #t) (k 0) (p "lmnkjgkj-std-backtrace-anyhow")))) (h "1y07jjlpamq5n1pzq1nb182fc2b9vsrkffzh3w4dmygnkj0gxw8b") (r "1.68.2")))

(define-public crate-altv_internal_core_shared-15.0.0-dev.73 (c (n "altv_internal_core_shared") (v "15.0.0-dev.73") (d (list (d (n "altv_sdk") (r "=15.0.0-dev.73") (d #t) (k 0) (p "altv_internal_sdk")) (d (n "anyhow") (r "^0.0.1") (d #t) (k 0) (p "lmnkjgkj-std-backtrace-anyhow")))) (h "0m4cvhl9qq8f902khpl6i7p1k1slj4bhzqbhmdx7hjzyhasbcc1l") (r "1.68.2")))

(define-public crate-altv_internal_core_shared-15.0.0-dev.74 (c (n "altv_internal_core_shared") (v "15.0.0-dev.74") (d (list (d (n "altv_sdk") (r "=15.0.0-dev.74") (d #t) (k 0) (p "altv_internal_sdk")) (d (n "anyhow") (r "^0.0.1") (d #t) (k 0) (p "lmnkjgkj-std-backtrace-anyhow")))) (h "1sdcyv4jk43jcq21jxqlvh5wmrjrvj5pgp7mq1ys5w3w9bs9gkws") (r "1.68.2")))

(define-public crate-altv_internal_core_shared-15.0.0-dev.75 (c (n "altv_internal_core_shared") (v "15.0.0-dev.75") (d (list (d (n "altv_sdk") (r "=15.0.0-dev.75") (d #t) (k 0) (p "altv_internal_sdk")) (d (n "anyhow") (r "^0.0.1") (d #t) (k 0) (p "lmnkjgkj-std-backtrace-anyhow")))) (h "01ll4ppis70h7r1y2v2g2rpnn56d32nj018p0aaq6ikjycafs5pz") (r "1.68.2")))

(define-public crate-altv_internal_core_shared-15.0.0-rc.6 (c (n "altv_internal_core_shared") (v "15.0.0-rc.6") (d (list (d (n "altv_sdk") (r "=15.0.0-rc.6") (d #t) (k 0) (p "altv_internal_sdk")) (d (n "anyhow") (r "^0.0.1") (d #t) (k 0) (p "lmnkjgkj-std-backtrace-anyhow")))) (h "1707q6aqxlyis6l07gha7chmf0f5krqj37hjljkag63aq2hvmcyy") (r "1.73.0")))

(define-public crate-altv_internal_core_shared-15.0.0-dev.76 (c (n "altv_internal_core_shared") (v "15.0.0-dev.76") (d (list (d (n "altv_sdk") (r "=15.0.0-dev.76") (d #t) (k 0) (p "altv_internal_sdk")) (d (n "anyhow") (r "^0.0.1") (d #t) (k 0) (p "lmnkjgkj-std-backtrace-anyhow")))) (h "1ysdf2ns06a8fj73fiv77k2y9mdjkwpf6g77bpn1x0a6fgxpd47d") (r "1.73.0")))

(define-public crate-altv_internal_core_shared-15.0.0 (c (n "altv_internal_core_shared") (v "15.0.0") (d (list (d (n "altv_sdk") (r "=15.0.0") (d #t) (k 0) (p "altv_internal_sdk")) (d (n "anyhow") (r "^0.0.1") (d #t) (k 0) (p "lmnkjgkj-std-backtrace-anyhow")))) (h "0d0q8fzf2q5y0x32y641vljwcy7f1q0an45jysm4d66c9nxddqkx") (r "1.73.0")))

(define-public crate-altv_internal_core_shared-15.0.1 (c (n "altv_internal_core_shared") (v "15.0.1") (d (list (d (n "altv_sdk") (r "=15.0.1") (d #t) (k 0) (p "altv_internal_sdk")) (d (n "anyhow") (r "^0.0.1") (d #t) (k 0) (p "lmnkjgkj-std-backtrace-anyhow")))) (h "12q47ilpr3b81np4ybgn4zhn457x77r9njsjcx83b6i7f84qi2dp") (r "1.73.0")))

(define-public crate-altv_internal_core_shared-15.1.0 (c (n "altv_internal_core_shared") (v "15.1.0") (d (list (d (n "altv_sdk") (r "=15.1.0") (d #t) (k 0) (p "altv_internal_sdk")) (d (n "anyhow") (r "^0.0.1") (d #t) (k 0) (p "lmnkjgkj-std-backtrace-anyhow")))) (h "192yvhrcx34d0j1kvx72yrr7c67109ygrwnpv28cmwdw73sph93h") (r "1.73.0")))

(define-public crate-altv_internal_core_shared-16.0.0-dev.1 (c (n "altv_internal_core_shared") (v "16.0.0-dev.1") (d (list (d (n "altv_sdk") (r "=16.0.0-dev.1") (d #t) (k 0) (p "altv_internal_sdk")) (d (n "anyhow") (r "^0.0.1") (d #t) (k 0) (p "lmnkjgkj-std-backtrace-anyhow")))) (h "1501nvvrm017ik0vb8hjkw18xzsj5nn4dfxb738g3k73qzi6gqj3") (r "1.73.0")))

(define-public crate-altv_internal_core_shared-16.0.0-dev.2 (c (n "altv_internal_core_shared") (v "16.0.0-dev.2") (d (list (d (n "altv_sdk") (r "=16.0.0-dev.2") (d #t) (k 0) (p "altv_internal_sdk")) (d (n "anyhow") (r "^0.0.1") (d #t) (k 0) (p "lmnkjgkj-std-backtrace-anyhow")))) (h "1b1sj7f4rba8gj0j1mv4iss56nz4hwcvvd9vg6r0pf108a86ils1") (r "1.73.0")))

(define-public crate-altv_internal_core_shared-16.0.0 (c (n "altv_internal_core_shared") (v "16.0.0") (d (list (d (n "altv_sdk") (r "=16.0.0") (d #t) (k 0) (p "altv_internal_sdk")) (d (n "anyhow") (r "^0.0.1") (d #t) (k 0) (p "lmnkjgkj-std-backtrace-anyhow")))) (h "1m397sngzqm54kwcl00l78ycbg1rvnnqj1ayzxvdnq563agx8lqi") (r "1.73.0")))

(define-public crate-altv_internal_core_shared-16.0.0-dev.3 (c (n "altv_internal_core_shared") (v "16.0.0-dev.3") (d (list (d (n "altv_sdk") (r "=16.0.0-dev.3") (d #t) (k 0) (p "altv_internal_sdk")) (d (n "anyhow") (r "^0.0.1") (d #t) (k 0) (p "lmnkjgkj-std-backtrace-anyhow")))) (h "1m46qlrqmvc3k294nrbyk428y3p4z54nmg09c50xv2797fq3fviv") (r "1.73.0")))

(define-public crate-altv_internal_core_shared-16.0.1 (c (n "altv_internal_core_shared") (v "16.0.1") (d (list (d (n "altv_sdk") (r "=16.0.1") (d #t) (k 0) (p "altv_internal_sdk")) (d (n "anyhow") (r "^0.0.1") (d #t) (k 0) (p "lmnkjgkj-std-backtrace-anyhow")))) (h "096r8l2502hicsvjw74h82b505x2vy69dqrpnfyy4rpa2zmxbfy5") (r "1.73.0")))

(define-public crate-altv_internal_core_shared-16.0.0-dev.4 (c (n "altv_internal_core_shared") (v "16.0.0-dev.4") (d (list (d (n "altv_sdk") (r "=16.0.0-dev.4") (d #t) (k 0) (p "altv_internal_sdk")) (d (n "anyhow") (r "^1.0.83") (d #t) (k 0)))) (h "0h69wryvg49l7195ysl5qs4gb1ls5df8d20x6ygza8vcnsypza3p") (r "1.73.0")))

(define-public crate-altv_internal_core_shared-16.0.2 (c (n "altv_internal_core_shared") (v "16.0.2") (d (list (d (n "altv_sdk") (r "=16.0.2") (d #t) (k 0) (p "altv_internal_sdk")) (d (n "anyhow") (r "^1.0.83") (d #t) (k 0)))) (h "1nhvfr1i48n1988mvms1gykqapiw9q1m7nmijqpimnpf7vg7lnaw") (r "1.73.0")))

