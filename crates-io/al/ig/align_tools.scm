(define-module (crates-io al ig align_tools) #:use-module (crates-io))

(define-public crate-align_tools-0.1.1 (c (n "align_tools") (v "0.1.1") (d (list (d (n "bio") (r "^0.31") (d #t) (k 0)) (d (n "debruijn") (r "^0.3.2") (d #t) (k 0)) (d (n "itertools") (r ">=0.8, <=0.9") (d #t) (k 0)) (d (n "vector_utils") (r "^0.1.3") (d #t) (k 0)))) (h "0pv1ig29dsvzy30dvk30d2wsjbnr6ha7lqsjpilcfa3m2gdp8lf8")))

(define-public crate-align_tools-0.1.2 (c (n "align_tools") (v "0.1.2") (d (list (d (n "bio") (r ">=0.31") (d #t) (k 0)) (d (n "debruijn") (r "^0.3.2") (d #t) (k 0)) (d (n "itertools") (r ">=0.8, <=0.9") (d #t) (k 0)) (d (n "vector_utils") (r "^0.1.3") (d #t) (k 0)))) (h "12793csbc9idi1m9akhv6aaj5s1grdzx5dcan3afgfhbqwpcmrb1")))

(define-public crate-align_tools-0.1.3 (c (n "align_tools") (v "0.1.3") (d (list (d (n "bio") (r ">=0.33") (d #t) (k 0)) (d (n "debruijn") (r "^0.3.2") (d #t) (k 0)) (d (n "itertools") (r ">=0.8, <=0.11") (d #t) (k 0)) (d (n "string_utils") (r "^0.1") (d #t) (k 0)) (d (n "vector_utils") (r "^0.1.3") (d #t) (k 0)))) (h "1080m1cyhwjp15g3z8qca6np4bdggym0sackwvcia3kr6sdfag48")))

(define-public crate-align_tools-0.1.4 (c (n "align_tools") (v "0.1.4") (d (list (d (n "bio") (r ">=0.33") (d #t) (k 0)) (d (n "debruijn") (r "^0.3.2") (d #t) (k 0)) (d (n "itertools") (r ">=0.8, <=0.11") (d #t) (k 0)) (d (n "string_utils") (r "^0.1") (d #t) (k 0)) (d (n "vector_utils") (r "^0.1.3") (d #t) (k 0)))) (h "0klfrn8cjn13zsxz3m0bi9pn2pgh97lmgkwjgjriwdyyyf4ls67p")))

(define-public crate-align_tools-0.1.5 (c (n "align_tools") (v "0.1.5") (d (list (d (n "bio") (r ">=0.33") (d #t) (k 0)) (d (n "debruijn") (r "^0.3.2") (d #t) (k 0)) (d (n "itertools") (r ">=0.8, <=0.11") (d #t) (k 0)) (d (n "string_utils") (r "^0.1") (d #t) (k 0)) (d (n "vector_utils") (r "^0.1.3") (d #t) (k 0)))) (h "0zkvj03qqrvrzqqswsb3y0rfywlhdy5ny1jq30z4m7idb26y4mv2")))

(define-public crate-align_tools-0.1.6 (c (n "align_tools") (v "0.1.6") (d (list (d (n "bio") (r ">=0.33") (d #t) (k 0)) (d (n "debruijn") (r "^0.3.2") (d #t) (k 0)) (d (n "itertools") (r ">=0.8, <=0.11") (d #t) (k 0)) (d (n "string_utils") (r "^0.1") (d #t) (k 0)) (d (n "vector_utils") (r "^0.1.3") (d #t) (k 0)))) (h "0c7yr9lv9zqncq6fzd6k5g23q9qc2m7jg7mzfscqbzniz6zksijm")))

(define-public crate-align_tools-0.1.7 (c (n "align_tools") (v "0.1.7") (d (list (d (n "bio") (r ">=0.35") (d #t) (k 0)) (d (n "debruijn") (r "^0.3.2") (d #t) (k 0)) (d (n "itertools") (r ">=0.8, <=0.11") (d #t) (k 0)) (d (n "string_utils") (r "^0.1") (d #t) (k 0)) (d (n "vector_utils") (r "^0.1.3") (d #t) (k 0)))) (h "1bs5plpxbxmc2yr0vflirbdj4w4zp0gh90xixqb5g4vkpkn0m7aq")))

(define-public crate-align_tools-0.1.8 (c (n "align_tools") (v "0.1.8") (d (list (d (n "bio_edit") (r "^0.1") (d #t) (k 0)) (d (n "debruijn") (r "^0.3.2") (d #t) (k 0)) (d (n "itertools") (r ">=0.8, <=0.11") (d #t) (k 0)) (d (n "string_utils") (r "^0.1") (d #t) (k 0)) (d (n "vector_utils") (r "^0.1.3") (d #t) (k 0)))) (h "04pcr0sq9zlpy1a4phkrmf7yg00qhml2xk7813nywsdsn644fn4f")))

(define-public crate-align_tools-0.1.9 (c (n "align_tools") (v "0.1.9") (d (list (d (n "bio_edit") (r "^0.1") (d #t) (k 0)) (d (n "debruijn") (r "^0.3.2") (d #t) (k 0)) (d (n "itertools") (r ">=0.8, <=0.11") (d #t) (k 0)) (d (n "string_utils") (r "^0.1") (d #t) (k 0)) (d (n "vector_utils") (r "^0.1") (d #t) (k 0)))) (h "0mazin2ck6d9cpcpgyrhs7ljqmcp84l7hxmz2aa67x66ci2lhrzn")))

(define-public crate-align_tools-0.1.10 (c (n "align_tools") (v "0.1.10") (d (list (d (n "bio_edit") (r "^0.1") (d #t) (k 0)) (d (n "debruijn") (r "^0.3.2") (d #t) (k 0)) (d (n "itertools") (r ">=0.8, <=0.11") (d #t) (k 0)) (d (n "string_utils") (r "^0.1") (d #t) (k 0)) (d (n "vector_utils") (r "^0.1") (d #t) (k 0)))) (h "02qcz22792kxrrmxm33iqvjmh7s10n3qy27svjimajrh4wgf61ni")))

(define-public crate-align_tools-0.1.11 (c (n "align_tools") (v "0.1.11") (d (list (d (n "bio_edit") (r "^0.1") (d #t) (k 0)) (d (n "debruijn") (r "^0.3") (d #t) (k 0)) (d (n "itertools") (r ">=0.8, <=0.11") (d #t) (k 0)) (d (n "string_utils") (r "^0.1") (d #t) (k 0)) (d (n "vector_utils") (r "^0.1") (d #t) (k 0)))) (h "01k43303m0j9yz3jhv2xcmw8i33zbg2lml56y6g2qcs3zlz508gz")))

(define-public crate-align_tools-0.1.12 (c (n "align_tools") (v "0.1.12") (d (list (d (n "bio_edit") (r "^0.1") (d #t) (k 0)) (d (n "debruijn") (r "^0.3") (d #t) (k 0)) (d (n "itertools") (r ">=0.8, <=0.11") (d #t) (k 0)) (d (n "string_utils") (r "^0.1") (d #t) (k 0)) (d (n "vector_utils") (r "^0.1") (d #t) (k 0)))) (h "1nd4pa4971b481wydcq3nwqhsgwznmv274m130qv5arc6lb10lxx")))

