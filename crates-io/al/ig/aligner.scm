(define-module (crates-io al ig aligner) #:use-module (crates-io))

(define-public crate-aligner-0.1.0 (c (n "aligner") (v "0.1.0") (d (list (d (n "arrayvec") (r "^0.3.20") (d #t) (k 0)) (d (n "clap") (r "~2.20.0") (d #t) (k 0)) (d (n "combine") (r "~2.2.2") (d #t) (k 0)) (d (n "error-chain") (r "~0.9.0") (d #t) (k 0)) (d (n "pbr") (r "^1.0.0-alpha.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "114mw37drzg9skhd44r0mcw5s7ia06bm31dg298giskfq1sb56i2")))

(define-public crate-aligner-0.1.1 (c (n "aligner") (v "0.1.1") (d (list (d (n "arrayvec") (r "^0.3.20") (d #t) (k 0)) (d (n "clap") (r "~2.20.0") (d #t) (k 0)) (d (n "combine") (r "~2.2.2") (d #t) (k 0)) (d (n "error-chain") (r "~0.9.0") (d #t) (k 0)) (d (n "pbr") (r "^1.0.0-alpha.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "0mv4ynbpazqknpq3cg9245rym0db6n89dw6jdhxp311j43pw62w6")))

(define-public crate-aligner-0.1.2 (c (n "aligner") (v "0.1.2") (d (list (d (n "arrayvec") (r "^0.3.20") (d #t) (k 0)) (d (n "clap") (r "~2.20.0") (d #t) (k 0)) (d (n "combine") (r "~2.2.2") (d #t) (k 0)) (d (n "error-chain") (r "~0.9.0") (d #t) (k 0)) (d (n "pbr") (r "^1.0.0-alpha.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "03np2br4kzigv9yi5b10mzprda7qnn82sgn5pyr08nbpqmjd3y0y")))

(define-public crate-aligner-0.1.3 (c (n "aligner") (v "0.1.3") (d (list (d (n "arrayvec") (r "^0.3.20") (d #t) (k 0)) (d (n "clap") (r "~2.20.0") (d #t) (k 0)) (d (n "combine") (r "~2.2.2") (d #t) (k 0)) (d (n "error-chain") (r "~0.9.0") (d #t) (k 0)) (d (n "pbr") (r "^1.0.0-alpha.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "0352x0bg6qczmhlpfcmgzsn2jki5l0zrjis9kh0yqj3ildyvbfka")))

(define-public crate-aligner-0.1.4 (c (n "aligner") (v "0.1.4") (d (list (d (n "arrayvec") (r "^0.3.20") (d #t) (k 0)) (d (n "clap") (r "~2.21.1") (d #t) (k 0)) (d (n "error-chain") (r "~0.10.0") (d #t) (k 0)) (d (n "pbr") (r "^1.0.0-alpha.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "subparse") (r "~0.1.0") (d #t) (k 0)))) (h "0by9sycvc1wzjg1gajm6y17hsp54xajp7bfwazsbwgip3gyxvc6b")))

(define-public crate-aligner-0.1.5 (c (n "aligner") (v "0.1.5") (d (list (d (n "arrayvec") (r "^0.3.20") (d #t) (k 0)) (d (n "clap") (r "^2.22.2") (d #t) (k 0)) (d (n "error-chain") (r "~0.10.0") (d #t) (k 0)) (d (n "pbr") (r "^1.0.0-alpha.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "subparse") (r "~0.2.0") (d #t) (k 0)))) (h "1jbk0vkjsy2a0hsic5v149pya6vfd2cbfv97qrry61zix9r6lyqs")))

(define-public crate-aligner-0.1.6 (c (n "aligner") (v "0.1.6") (d (list (d (n "arrayvec") (r "^0.4.0") (d #t) (k 0)) (d (n "clap") (r "^2.22.2") (d #t) (k 0)) (d (n "encoding") (r "^0.2.33") (d #t) (k 0)) (d (n "error-chain") (r "~0.10.0") (d #t) (k 0)) (d (n "pbr") (r "^1.0.0-alpha.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "subparse") (r "^0.3.0") (d #t) (k 0)))) (h "1lf6jad1d9ksg1src8byqh9p1zwrq2xg5y0rzlj81cfqllnpd7zb")))

