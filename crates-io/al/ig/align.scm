(define-module (crates-io al ig align) #:use-module (crates-io))

(define-public crate-align-1.0.0 (c (n "align") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.11") (d #t) (k 0)))) (h "13wm4cs40q7jz05qqill3z5dmdn3ynf35dhknaccqgjd0f21bhqz")))

