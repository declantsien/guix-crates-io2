(define-module (crates-io al ig aligned_box) #:use-module (crates-io))

(define-public crate-aligned_box-0.1.0 (c (n "aligned_box") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 2)))) (h "0vwdh98ik2v4q1h3prb8mvzbl8cn1w9z6vhwdkcnw02bi4nd81jh") (y #t)))

(define-public crate-aligned_box-0.2.0 (c (n "aligned_box") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 2)))) (h "1cqzjdmczgbhz1pba5hhjlq23r8k9hgcym9h1vsbwffagbzs1h5a")))

(define-public crate-aligned_box-0.2.1 (c (n "aligned_box") (v "0.2.1") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 2)))) (h "11sl8rbrk1z94niaj7g76zaa109xr0nqd5yd68g70xkm92s2xh56")))

