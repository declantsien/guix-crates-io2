(define-module (crates-io al ig aligned) #:use-module (crates-io))

(define-public crate-aligned-0.1.0 (c (n "aligned") (v "0.1.0") (h "1f3ns4ysv33lkz7wmci36s6qr65xc3lwz9qff2miqn8xm34kwsmn")))

(define-public crate-aligned-0.1.1 (c (n "aligned") (v "0.1.1") (h "1rr9g0y7kfa1f4f2j2hd2frjnsadr9y5j4iyr73r31m4rsajkafi")))

(define-public crate-aligned-0.1.2 (c (n "aligned") (v "0.1.2") (h "0f0gxzixajazk3qym6yghsihqjcv96781n2vh71rfk58wqaws482") (f (quote (("default" "const-fn") ("const-fn"))))))

(define-public crate-aligned-0.2.0 (c (n "aligned") (v "0.2.0") (h "19f48jmk847n5ab42q0j2hwykl7ihgdvi0n0r41ira71iawak7fk") (f (quote (("const-fn"))))))

(define-public crate-aligned-0.3.0 (c (n "aligned") (v "0.3.0") (d (list (d (n "as-slice") (r "^0.1.0") (d #t) (k 0)))) (h "13a50pmkdzywmz7x0hw95b7kr0nqjy9cxsgh0may640s7i2s4s5v")))

(define-public crate-aligned-0.3.1 (c (n "aligned") (v "0.3.1") (d (list (d (n "as-slice") (r "^0.1.0") (d #t) (k 0)))) (h "1ypfzq76hq0psxljp0zmx56w2yjsxwnrjb46ak79w7lfxb3id8yk")))

(define-public crate-aligned-0.3.2 (c (n "aligned") (v "0.3.2") (d (list (d (n "as-slice") (r "^0.1.0") (d #t) (k 0)))) (h "157bvizg1w0845mz3mdjgs5ly507x1f1n6nkn5m165i072ryh77b")))

(define-public crate-aligned-0.3.3 (c (n "aligned") (v "0.3.3") (d (list (d (n "as-slice") (r "^0.1.0") (d #t) (k 0)))) (h "0lgsp90hz552rl362p6vhi2wynls5vm7nfag310r11jr873d2xdm")))

(define-public crate-aligned-0.3.4 (c (n "aligned") (v "0.3.4") (d (list (d (n "as-slice") (r "^0.1.0") (d #t) (k 0)))) (h "1gq9lgyb8l56rnrnpa869x3mj4wb9935nin29afilzs7inyrd5y1")))

(define-public crate-aligned-0.3.5 (c (n "aligned") (v "0.3.5") (d (list (d (n "as-slice") (r "^0.1.0") (d #t) (k 0)))) (h "1fqn7ww3x5shf5lnbzqz76xj3g1ibmjv4fz9wbjgah7a79a5ly1s")))

(define-public crate-aligned-0.4.0 (c (n "aligned") (v "0.4.0") (d (list (d (n "as-slice") (r "^0.2.0") (d #t) (k 0)))) (h "1mfh8lyhyrghj3pp0gz2ffyycnpv68f297n7lcmysmld9qf0q962")))

(define-public crate-aligned-0.4.1 (c (n "aligned") (v "0.4.1") (d (list (d (n "as-slice") (r "^0.2.0") (d #t) (k 0)))) (h "0md67hsr0yw5qydr9hjlfz4wfxbbl39y7abkhpycf9m682a1p8l0")))

(define-public crate-aligned-0.4.2 (c (n "aligned") (v "0.4.2") (d (list (d (n "as-slice") (r "^0.2.0") (d #t) (k 0)))) (h "08srg0rbz21s1kk97b1canffly7icr31sp7l1nqk2i1ym05lqzip")))

