(define-module (crates-io al ig aligners) #:use-module (crates-io))

(define-public crate-aligners-0.0.1 (c (n "aligners") (v "0.0.1") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "page_size") (r "^0.4.2") (d #t) (k 0)))) (h "09kn2ywi0q0ca2awpbg53jbjxq9zz8rxc18rh7s4cjk2rsw1zzz1") (f (quote (("simd") ("default" "simd"))))))

(define-public crate-aligners-0.0.2 (c (n "aligners") (v "0.0.2") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "page_size") (r "^0.4.2") (d #t) (k 0)))) (h "1i7fxhjrx5yqyy6fsq8sl2f36im3xb4y47igmd32hr2yq4kl0n5b") (f (quote (("simd") ("default" "simd"))))))

(define-public crate-aligners-0.0.3 (c (n "aligners") (v "0.0.3") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "page_size") (r "^0.4.2") (d #t) (k 0)))) (h "1p7wdcicqznpq21dcxkhg20ixhsj2292p4p7wgnfvnmd8g4796qd") (f (quote (("simd") ("default" "simd"))))))

(define-public crate-aligners-0.0.4 (c (n "aligners") (v "0.0.4") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 2)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "page_size") (r "^0.4.2") (d #t) (k 0)))) (h "06whrs0qqa0f7x97598x2wpifldfcwn87qlwjrs0375yq6ahrflw") (f (quote (("simd") ("default" "simd"))))))

(define-public crate-aligners-0.0.5 (c (n "aligners") (v "0.0.5") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 2)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "page_size") (r "^0.4.2") (d #t) (k 0)))) (h "0bdq68l00xd5n7ja5qf3mq75hggr629aamcxfc28zm25bimffjcw") (f (quote (("simd") ("default" "simd"))))))

(define-public crate-aligners-0.0.6 (c (n "aligners") (v "0.0.6") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 2)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "page_size") (r "^0.4.2") (d #t) (k 0)))) (h "0rqa6qfina8fhd4zjqdx4cdr51bibfqslszgz9h5npf28vmbmmpy") (f (quote (("simd") ("default" "simd"))))))

(define-public crate-aligners-0.0.7 (c (n "aligners") (v "0.0.7") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 2)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "page_size") (r "^0.4.2") (d #t) (k 0)))) (h "1abyjhgxxhw7gvszlapyyhrbmh9wl70kbb7pdjrr6jxi5rzxgx8d") (f (quote (("simd") ("default" "simd"))))))

(define-public crate-aligners-0.0.8 (c (n "aligners") (v "0.0.8") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 2)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "page_size") (r "^0.4.2") (d #t) (k 0)))) (h "1al7zwsk6bkxs0spg628kddhxczgvllyjmlqxdlq53gv59gb17bh") (f (quote (("simd") ("default" "simd"))))))

(define-public crate-aligners-0.0.9 (c (n "aligners") (v "0.0.9") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 2)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "page_size") (r "^0.4.2") (d #t) (k 0)))) (h "0bd7d0j60bqdw89bcym0h44ips6y4ircc00srhqcckfvamr5nl3v") (f (quote (("simd") ("default" "simd"))))))

(define-public crate-aligners-0.0.10 (c (n "aligners") (v "0.0.10") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 2)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "page_size") (r "^0.4.2") (d #t) (k 0)))) (h "1rwjgc04vd9xgqhlpjff1kgcvvgcnrgkvfnymf7vm147s4rmjdpw") (f (quote (("simd") ("default" "simd"))))))

