(define-module (crates-io al ig aligned-buffer-pool) #:use-module (crates-io))

(define-public crate-aligned-buffer-pool-0.0.0 (c (n "aligned-buffer-pool") (v "0.0.0") (d (list (d (n "aligned-buffer") (r "^0.0.4") (f (quote ("rkyv"))) (k 0)) (d (n "crossbeam-queue") (r "^0.3.11") (d #t) (k 0)) (d (n "fxhash") (r "^0.2.1") (k 0)) (d (n "rkyv") (r "^0.8.0-alpha.1") (f (quote ("std" "bytecheck"))) (k 0)))) (h "1ffwcf86gy8akbks9hixyabrwjc5rlfgsyry0fpydmr58jmks1vm")))

(define-public crate-aligned-buffer-pool-0.0.1 (c (n "aligned-buffer-pool") (v "0.0.1") (d (list (d (n "aligned-buffer") (r "^0.0.5") (f (quote ("rkyv"))) (k 0)) (d (n "crossbeam-queue") (r "^0.3.11") (d #t) (k 0)) (d (n "fxhash") (r "^0.2.1") (k 0)) (d (n "rkyv") (r "^0.8.0-alpha.1") (f (quote ("std" "bytecheck"))) (k 0)))) (h "1adyg1yp497s37lnd9allj3kgfrhap7vlpx66vvlxpbgaq208v9h")))

(define-public crate-aligned-buffer-pool-0.0.2 (c (n "aligned-buffer-pool") (v "0.0.2") (d (list (d (n "aligned-buffer") (r "^0.0.6") (f (quote ("rkyv"))) (k 0)) (d (n "crossbeam-queue") (r "^0.3.11") (d #t) (k 0)) (d (n "fxhash") (r "^0.2.1") (k 0)) (d (n "rkyv") (r "^0.8.0-alpha.1") (f (quote ("std" "bytecheck"))) (k 0)))) (h "1jfc7jnbg0ydjs7a2nx3f48aminvz6adiijp3iyw02x3r9ywz7kf")))

(define-public crate-aligned-buffer-pool-0.0.3 (c (n "aligned-buffer-pool") (v "0.0.3") (d (list (d (n "aligned-buffer") (r "^0.0.7") (f (quote ("rkyv"))) (k 0)) (d (n "crossbeam-queue") (r "^0.3.11") (d #t) (k 0)) (d (n "fxhash") (r "^0.2.1") (k 0)) (d (n "rkyv") (r "^0.8.0-alpha.2") (f (quote ("std" "bytecheck"))) (k 0)))) (h "0ha6ml35rliz1qzgipkavn11d9fzqd9mfxb3bk59vd1d7q01v84c")))

(define-public crate-aligned-buffer-pool-0.0.4 (c (n "aligned-buffer-pool") (v "0.0.4") (d (list (d (n "aligned-buffer") (r "^0.0.8") (f (quote ("rkyv"))) (k 0)) (d (n "crossbeam-queue") (r "^0.3.11") (d #t) (k 0)) (d (n "fxhash") (r "^0.2.1") (k 0)) (d (n "rkyv") (r "^0.8.0-alpha.2") (f (quote ("std" "bytecheck"))) (k 0)))) (h "0hr8hryj2yxm24gygw2h3pgi7yhzkzmq5b6sn63vqs5pys5fkpdd")))

(define-public crate-aligned-buffer-pool-0.0.5 (c (n "aligned-buffer-pool") (v "0.0.5") (d (list (d (n "aligned-buffer") (r "^0.0.8") (f (quote ("rkyv"))) (k 0)) (d (n "crossbeam-queue") (r "^0.3.11") (d #t) (k 0)) (d (n "fxhash") (r "^0.2.1") (k 0)) (d (n "rkyv") (r "^0.8.0-alpha.2") (f (quote ("std" "bytecheck"))) (k 0)))) (h "02gczfa3n2i9l6djfynrykfqgr988q4laln8bw0j48q3n872k59n")))

