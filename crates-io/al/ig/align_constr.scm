(define-module (crates-io al ig align_constr) #:use-module (crates-io))

(define-public crate-align_constr-0.1.0 (c (n "align_constr") (v "0.1.0") (d (list (d (n "as-slice") (r "^0.2.1") (o #t) (d #t) (k 0)) (d (n "remove_macro_call") (r "^0.1.1") (d #t) (k 0)) (d (n "unconst_trait_impl") (r "^0.1.4") (d #t) (k 0)))) (h "1viq69jvaf3rh4gdwyqd92qzw0dma3mvzbsvply2b0661w4f112x") (f (quote (("nightly" "const_trait_impl" "const_fn_trait_bound" "const_mut_refs") ("const_trait_impl") ("const_mut_refs") ("const_fn_trait_bound")))) (r "1.56.1")))

(define-public crate-align_constr-0.1.1 (c (n "align_constr") (v "0.1.1") (d (list (d (n "as-slice") (r "^0.2.1") (o #t) (d #t) (k 0)) (d (n "remove_macro_call") (r "^0.1.1") (d #t) (k 0)) (d (n "unconst_trait_impl") (r "^0.1.4") (d #t) (k 0)))) (h "1590vzr35p9x5l2d8009d5b0f6xk79a0vg289l9vadh83bivghhs") (f (quote (("nightly" "const_trait_impl" "const_fn_trait_bound" "const_mut_refs") ("const_trait_impl") ("const_mut_refs") ("const_fn_trait_bound")))) (r "1.56.1")))

(define-public crate-align_constr-0.1.2 (c (n "align_constr") (v "0.1.2") (d (list (d (n "as-slice") (r "^0.2.1") (o #t) (d #t) (k 0)) (d (n "remove_macro_call") (r "^0.1.1") (d #t) (k 0)) (d (n "unconst_trait_impl") (r "^0.1.4") (d #t) (k 0)))) (h "15q1si4dbv248mdl7n6z8z4dywmsbqkcma4q4mh8q5nyk9qrlr9p") (f (quote (("nightly" "const_trait_impl" "const_fn_trait_bound" "const_mut_refs") ("const_trait_impl") ("const_mut_refs") ("const_fn_trait_bound")))) (r "1.56.1")))

(define-public crate-align_constr-0.1.4 (c (n "align_constr") (v "0.1.4") (d (list (d (n "as-slice") (r "^0.2.1") (o #t) (d #t) (k 0)) (d (n "remove_macro_call") (r "^0.1.1") (d #t) (k 0)) (d (n "unconst_trait_impl") (r "^0.1.5") (d #t) (k 0)))) (h "0f03bml7j2szb7166c3rgyvw3v03gpskgbznvm9jsrk5i53gq86n") (f (quote (("nightly" "const_trait_impl" "const_fn_trait_bound" "const_mut_refs") ("const_trait_impl") ("const_mut_refs") ("const_fn_trait_bound")))) (r "1.56.1")))

(define-public crate-align_constr-0.2.0 (c (n "align_constr") (v "0.2.0") (d (list (d (n "as-slice") (r "^0.2.1") (o #t) (d #t) (k 0)))) (h "0ry4nrq6xyhj3p83k8f2dihd3xmkgdvaqw4995an59z57wy3yyfn")))

