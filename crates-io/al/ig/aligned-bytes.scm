(define-module (crates-io al ig aligned-bytes) #:use-module (crates-io))

(define-public crate-aligned-bytes-0.1.0 (c (n "aligned-bytes") (v "0.1.0") (h "1nj6cxlhny9kjpp8ym6q803d9qxg8sbl1050bzswfpin6nlpv95h") (y #t)))

(define-public crate-aligned-bytes-0.1.1 (c (n "aligned-bytes") (v "0.1.1") (h "1mnn05yq5dnnna3f0ablirli99gzvdfjismlb1x6xxm44ssdxb9a") (f (quote (("std") ("default" "std")))) (y #t)))

(define-public crate-aligned-bytes-0.1.2 (c (n "aligned-bytes") (v "0.1.2") (h "1cvxvr22ck85s0z1m8d38awqk1iqbs4ya27kkr8jvd80ygyhk609") (f (quote (("std") ("default" "std")))) (y #t)))

(define-public crate-aligned-bytes-0.1.3 (c (n "aligned-bytes") (v "0.1.3") (h "11qhrpmvlw62ncd2k8inym8axhb3yknrvn2fvcys5rza87g7s9hy") (f (quote (("unstable") ("std") ("default" "std")))) (y #t)))

(define-public crate-aligned-bytes-0.1.4 (c (n "aligned-bytes") (v "0.1.4") (h "1zs2gf5skmbv566fdkxgs3yj66vkfrcqn7qb5b9dapk9dbxsf51k") (f (quote (("unstable") ("std") ("default" "std")))) (y #t)))

(define-public crate-aligned-bytes-0.1.5 (c (n "aligned-bytes") (v "0.1.5") (h "0agn5nk08wca5qri7napdsq8hi9i5jk292mcgnnlk23kcjxzgnwl") (f (quote (("unstable") ("std") ("default" "std")))) (y #t)))

