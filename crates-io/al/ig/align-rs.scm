(define-module (crates-io al ig align-rs) #:use-module (crates-io))

(define-public crate-align-rs-0.0.1 (c (n "align-rs") (v "0.0.1") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "page_size") (r "^0.4.2") (d #t) (k 0)))) (h "180biah95n2rnd5k45a7ixqljnxb3cjg65xr5sh0x54y647z9b3d") (f (quote (("simd") ("default" "simd")))) (y #t)))

