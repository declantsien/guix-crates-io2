(define-module (crates-io al ig alignas) #:use-module (crates-io))

(define-public crate-alignas-0.1.0 (c (n "alignas") (v "0.1.0") (h "09wrl0y03h9dwwvhpyvyyk00hr7w033c3yghxavn37r3chn2v5sh")))

(define-public crate-alignas-0.2.0 (c (n "alignas") (v "0.2.0") (h "179fwahkdsn9k3m3hhnz1acp8xr4idldhdr553fxrxscvlg2yr2p")))

