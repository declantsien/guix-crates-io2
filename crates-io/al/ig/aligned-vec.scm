(define-module (crates-io al ig aligned-vec) #:use-module (crates-io))

(define-public crate-aligned-vec-0.1.0 (c (n "aligned-vec") (v "0.1.0") (h "1mifb89mj9aqxpaywaql95hhpffh1h418bzd9p8qp0q8qwdl9cf0")))

(define-public crate-aligned-vec-0.2.0 (c (n "aligned-vec") (v "0.2.0") (h "159kzbm0nn3mgyfcglg1mhpnkk77zjs89377xy9w2nb3agml946v")))

(define-public crate-aligned-vec-0.3.1 (c (n "aligned-vec") (v "0.3.1") (h "1dl00wf95ipy0liv7gr963v68v7wri602z1z7zi0ipbcwqcbbcvw")))

(define-public crate-aligned-vec-0.3.2 (c (n "aligned-vec") (v "0.3.2") (h "1y4f8zz5dv28n4451pn0pahhggnjk6b3lfnb6crl12sl4ci1y47l")))

(define-public crate-aligned-vec-0.4.0 (c (n "aligned-vec") (v "0.4.0") (h "0cz4fkinr9dqkjh0zz4grkrzcx9d92rafkyacqzisk330dkd6nh5")))

(define-public crate-aligned-vec-0.4.1 (c (n "aligned-vec") (v "0.4.1") (h "01wg2mwiz7y77w9zr9l4fnwksjlaynhrzmyzic8xdnhiaxdna3p2")))

(define-public crate-aligned-vec-0.4.2 (c (n "aligned-vec") (v "0.4.2") (h "1zx3bbqg8i6ai126sb0kya254kx26gg4m8b77h1hsilrrdhhjhwg")))

(define-public crate-aligned-vec-0.5.0 (c (n "aligned-vec") (v "0.5.0") (d (list (d (n "serde") (r "^1.0") (o #t) (k 0)))) (h "1lb8qjqfap028ylf8zap6rkwrnrqimc3v6h3cixycjrdx1y0vaaa") (f (quote (("std") ("default" "std")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-aligned-vec-0.6.0 (c (n "aligned-vec") (v "0.6.0") (d (list (d (n "equator") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (k 0)))) (h "1d78jswvhymaj91ng5h9fhi4sfmrzlyckzn52l0680j4wdbbglr1") (f (quote (("std") ("default" "std")))) (s 2) (e (quote (("serde" "dep:serde"))))))

