(define-module (crates-io al ig aligned-array) #:use-module (crates-io))

(define-public crate-aligned-array-1.0.0 (c (n "aligned-array") (v "1.0.0") (d (list (d (n "generic-array") (r "^0.14") (k 0)) (d (n "subtle") (r "^2.4") (o #t) (k 0)))) (h "1bhk6hydqn3dbxhfv4l8s61pc6hm9wz9n20g7flw054khhczgjvn")))

(define-public crate-aligned-array-1.0.1 (c (n "aligned-array") (v "1.0.1") (d (list (d (n "generic-array") (r "^0.14") (k 0)) (d (n "subtle") (r "^2.4") (o #t) (k 0)))) (h "0b7xqdjsm7v0i9wkdbdqz80x9xrbqqm29xhkh29m43r9hv894p70")))

