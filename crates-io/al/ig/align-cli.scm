(define-module (crates-io al ig align-cli) #:use-module (crates-io))

(define-public crate-align-cli-0.1.0 (c (n "align-cli") (v "0.1.0") (d (list (d (n "bio") (r "^1.2") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "rustyms") (r "^0.1") (d #t) (k 0)))) (h "0wsrc3r8c3vxmym7ghypi0g8gi4fqfxsb7rmvqc8ga30h6gc5gxf")))

(define-public crate-align-cli-0.2.0 (c (n "align-cli") (v "0.2.0") (d (list (d (n "bio") (r "^1.2") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "rustyms") (r "^0.1.1") (d #t) (k 0)))) (h "06ngkmkmbkgybfxzb6qn877q7r76601xg5mm1gn0imsdn313yfqf")))

(define-public crate-align-cli-0.2.1 (c (n "align-cli") (v "0.2.1") (d (list (d (n "bio") (r "^1.2") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "rustyms") (r "^0.2.0") (d #t) (k 0)))) (h "0xpp4cbkxsb80cn7v5cxdx0yyma0nmbwiml7lf3ik8rh0p6c32da")))

(define-public crate-align-cli-0.2.2 (c (n "align-cli") (v "0.2.2") (d (list (d (n "bio") (r "^1.2") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "rustyms") (r "^0.2.0") (d #t) (k 0)))) (h "1jd3ia9pljmfx6p6qrlrq260pkawgdmm4z45r8km35fmyd81svr9")))

(define-public crate-align-cli-0.2.3 (c (n "align-cli") (v "0.2.3") (d (list (d (n "bio") (r "^1.2") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "rustyms") (r "^0.3.0") (d #t) (k 0)))) (h "0wsrc0kbxb3xf2l1gb1wk5jcv62kzqy4ypfsnxmhafxwmpspgvwf")))

(define-public crate-align-cli-0.2.4 (c (n "align-cli") (v "0.2.4") (d (list (d (n "bio") (r "^1.2") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "rustyms") (r "^0.3.1") (d #t) (k 0)))) (h "0nlhbqyka2gklr845f58r0m286c3x28ly36fa0z7xfs2lvw0izz5")))

(define-public crate-align-cli-0.2.5 (c (n "align-cli") (v "0.2.5") (d (list (d (n "bio") (r "^1.2") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "rustyms") (r "^0.4.1") (d #t) (k 0)))) (h "06rn3ymyq1r9c57dw6c4lq3h593a9wh18pln297dg7r4zbij0c8d")))

(define-public crate-align-cli-0.3.0 (c (n "align-cli") (v "0.3.0") (d (list (d (n "bio") (r "^1.2") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "rustyms") (r "^0.6.1") (d #t) (k 0)))) (h "0337vpw230f1sz08jydvpdbxvn1f0dxmp6bb9w6d2iv1ywr3y3ba")))

(define-public crate-align-cli-0.4.0 (c (n "align-cli") (v "0.4.0") (d (list (d (n "clap") (r "^4") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "itertools") (r "^0.12") (d #t) (k 0)) (d (n "rayon") (r "^1.8") (d #t) (k 0)) (d (n "rustyms") (r "^0.8") (d #t) (k 0)))) (h "0739bdh0zz5k6gif2pdkwmpd7ahcbmjd7vgz9gr7mz7blmbbpy59")))

