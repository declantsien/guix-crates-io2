(define-module (crates-io al ig aligned_media) #:use-module (crates-io))

(define-public crate-aligned_media-0.1.0 (c (n "aligned_media") (v "0.1.0") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "isolang") (r "^0.2") (f (quote ("serde_serialize"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1f7ad0n3qdqmi24rxxff9kk9f1y0d163gawjq9nkmsf2h8iqynbn")))

