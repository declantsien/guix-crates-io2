(define-module (crates-io al ig aligned-utils) #:use-module (crates-io))

(define-public crate-aligned-utils-0.2.0 (c (n "aligned-utils") (v "0.2.0") (h "0vdiq4yzvc4kqrkrm35fsdcif01wj75g8dgliw8ndrzj17vxknby") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-aligned-utils-1.0.0 (c (n "aligned-utils") (v "1.0.0") (h "02hdm52ycklp6wp050zh26m4ii6qi3vc7rmbgh7m088cpbdmln2a") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-aligned-utils-1.0.1 (c (n "aligned-utils") (v "1.0.1") (h "157dnhn0anzzry5a9vz3akrj1ww4af23bfwwpmrkk9p0i9kyq575") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-aligned-utils-1.0.2 (c (n "aligned-utils") (v "1.0.2") (h "1ap2b3ch36xmw6m033sslqkgcmsl9z95xf730rzcd07wlkx2flcn") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

