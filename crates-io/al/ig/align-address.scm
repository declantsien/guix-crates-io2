(define-module (crates-io al ig align-address) #:use-module (crates-io))

(define-public crate-align-address-0.1.0 (c (n "align-address") (v "0.1.0") (h "1262qprwa6q59sir6sc8z1j2n4brp54dqsqz5s2g8xx9zgxhk76k")))

(define-public crate-align-address-0.2.0 (c (n "align-address") (v "0.2.0") (h "00p8qh31damgh2k4lmjn41kn612jbnzmd2yrjrygmw96wmf9rf8a")))

(define-public crate-align-address-0.3.0 (c (n "align-address") (v "0.3.0") (h "1w6ba2ilh9wni5bfhd2cymawylzznzckq82q50l58rbkcy5c1ija")))

