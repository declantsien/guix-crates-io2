(define-module (crates-io al ig aligned_alloc) #:use-module (crates-io))

(define-public crate-aligned_alloc-0.1.0 (c (n "aligned_alloc") (v "0.1.0") (d (list (d (n "kernel32-sys") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "winapi") (r "^0.2") (d #t) (k 0)))) (h "1bj8jqbvzznaxnla9ks57vjzzk7y5h5ch0kdhzyhcw0zk8gfyp9r") (y #t)))

(define-public crate-aligned_alloc-0.1.1 (c (n "aligned_alloc") (v "0.1.1") (d (list (d (n "kernel32-sys") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "winapi") (r "^0.2") (d #t) (k 0)))) (h "16pyqcdp9r2lvrc5bav1g20r3wszk8fsf6xi8m7f5ihs118p28bi") (y #t)))

(define-public crate-aligned_alloc-0.1.2 (c (n "aligned_alloc") (v "0.1.2") (d (list (d (n "kernel32-sys") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.2") (d #t) (k 0)))) (h "15nq4d4drgigaq8bx7kbhicr4bhdplv4ph0ncvx7ifyngilzjw79") (y #t)))

(define-public crate-aligned_alloc-0.1.3 (c (n "aligned_alloc") (v "0.1.3") (d (list (d (n "kernel32-sys") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.2") (d #t) (k 0)))) (h "02kgfis817ik658yvzyjzj00xaa8al6iv15w2nf7dpnc0aqbzklx") (y #t)))

