(define-module (crates-io al ig aligned-sized) #:use-module (crates-io))

(define-public crate-aligned-sized-0.1.0 (c (n "aligned-sized") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0wfndfz3bwhdsc62848glwmvmhlfdz97ikiwjqprynag0478nk56")))

(define-public crate-aligned-sized-0.1.1 (c (n "aligned-sized") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1pybavrq54bz19pd22zyqv3pyplysd9w9ikj842mdmczfl9mk4xp")))

(define-public crate-aligned-sized-0.1.4 (c (n "aligned-sized") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1p0g12lv2p83m06nqk7x2z4pnqz1smslimrqsqlshs1xk3gmcaxj")))

