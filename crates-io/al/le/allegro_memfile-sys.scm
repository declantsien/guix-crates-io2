(define-module (crates-io al le allegro_memfile-sys) #:use-module (crates-io))

(define-public crate-allegro_memfile-sys-0.0.34 (c (n "allegro_memfile-sys") (v "0.0.34") (d (list (d (n "allegro-sys") (r "= 0.0.34") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "0mrhkkabqx0cddg76y5wh7076al6521ak9h4izrzwrmkxr8jmifv") (f (quote (("link_static") ("link_none") ("link_debug")))) (l "allegro_memfile")))

(define-public crate-allegro_memfile-sys-0.0.35 (c (n "allegro_memfile-sys") (v "0.0.35") (d (list (d (n "allegro-sys") (r "= 0.0.35") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "1xza6wjw27nraahfv31495qcgwwns7wz3xj82dmd6gcqp8nzqi9z") (f (quote (("link_static") ("link_none") ("link_debug")))) (l "allegro_memfile")))

(define-public crate-allegro_memfile-sys-0.0.36 (c (n "allegro_memfile-sys") (v "0.0.36") (d (list (d (n "allegro-sys") (r "= 0.0.36") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "12szdm7jq883whf74gb9kj20xbv9s64i7amxsn0b0k6iih8d78c6") (f (quote (("link_static") ("link_none") ("link_debug")))) (l "allegro_memfile")))

(define-public crate-allegro_memfile-sys-0.0.37 (c (n "allegro_memfile-sys") (v "0.0.37") (d (list (d (n "allegro-sys") (r "= 0.0.37") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "13lmngc9jgii0vhl1n8ca2zv0794dk6yfvnf4yy7kkk4zj7n8whz") (f (quote (("link_static") ("link_none") ("link_debug")))) (l "allegro_memfile")))

(define-public crate-allegro_memfile-sys-0.0.38 (c (n "allegro_memfile-sys") (v "0.0.38") (d (list (d (n "allegro-sys") (r "= 0.0.38") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1kk0gbp08rdimf9f78lvgq4sq0hlgwa9knzihpcxbd54vlpj1swa") (f (quote (("link_static") ("link_none") ("link_debug")))) (l "allegro_memfile")))

(define-public crate-allegro_memfile-sys-0.0.39 (c (n "allegro_memfile-sys") (v "0.0.39") (d (list (d (n "allegro-sys") (r "= 0.0.39") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0s2cnf7bkhpxiwsyx7qa5q1kx1gn40ciz0rcjpyc5wql9yxxa4xd") (f (quote (("link_static") ("link_none") ("link_debug")))) (l "allegro_memfile")))

(define-public crate-allegro_memfile-sys-0.0.40 (c (n "allegro_memfile-sys") (v "0.0.40") (d (list (d (n "allegro-sys") (r "= 0.0.40") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0gbizzxzxqd8i50qv189w8ix50w36nyqnbh1bidwxxkszh28sww8") (f (quote (("link_static") ("link_none") ("link_debug")))) (l "allegro_memfile")))

(define-public crate-allegro_memfile-sys-0.0.41 (c (n "allegro_memfile-sys") (v "0.0.41") (d (list (d (n "allegro-sys") (r "=0.0.41") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1580cif4303x4czywrxgsc93lwxmnmjr4a2afs5w6gzai81x3gyk") (f (quote (("link_static") ("link_none") ("link_debug")))) (l "allegro_memfile")))

(define-public crate-allegro_memfile-sys-0.0.42 (c (n "allegro_memfile-sys") (v "0.0.42") (d (list (d (n "allegro-sys") (r "=0.0.42") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1vq95py9rxqfyj8ljnlmpza6pz7yilp6qfm3rcs6nagxapynz21b") (f (quote (("link_static") ("link_none") ("link_debug")))) (l "allegro_memfile")))

(define-public crate-allegro_memfile-sys-0.0.43 (c (n "allegro_memfile-sys") (v "0.0.43") (d (list (d (n "allegro-sys") (r "=0.0.43") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1548rbzaqcscij7m0qpcjpv507iff8ghspi96lgpw8ghd9ih9rgg") (f (quote (("link_static") ("link_none") ("link_debug")))) (l "allegro_memfile")))

(define-public crate-allegro_memfile-sys-0.0.44 (c (n "allegro_memfile-sys") (v "0.0.44") (d (list (d (n "allegro-sys") (r "=0.0.44") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "01klskfzfk6mqpffy634bx7wyp34rmm6jn3yad0szr2ikyljy7b2") (f (quote (("link_static") ("link_none") ("link_debug")))) (l "allegro_memfile")))

(define-public crate-allegro_memfile-sys-0.0.45 (c (n "allegro_memfile-sys") (v "0.0.45") (d (list (d (n "allegro-sys") (r "=0.0.45") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1by0xw7cwgs3vccwm1v20fp7dskq7cnn9yrcj6ggap860dwyhbfh") (f (quote (("link_static") ("link_none") ("link_debug")))) (l "allegro_memfile")))

(define-public crate-allegro_memfile-sys-0.0.46 (c (n "allegro_memfile-sys") (v "0.0.46") (d (list (d (n "allegro-sys") (r "=0.0.46") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "11waqvkysqcz4i3y69bnbab3lffzw9a995xkzrbnfayzyx211x1j") (f (quote (("link_static") ("link_none") ("link_debug")))) (l "allegro_memfile")))

