(define-module (crates-io al le allenap-unison-confgen) #:use-module (crates-io))

(define-public crate-allenap-unison-confgen-0.1.0 (c (n "allenap-unison-confgen") (v "0.1.0") (d (list (d (n "hostname") (r "=0.4.0") (d #t) (k 0)) (d (n "itertools") (r "=0.12.1") (d #t) (k 0)) (d (n "serde") (r "=1.0.201") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "=0.9.34") (d #t) (k 0)))) (h "06k7qfkx6yhh12j8hs6ndqp6gi6rkx06z0fbj1d4zaryjq73y511")))

(define-public crate-allenap-unison-confgen-0.1.1 (c (n "allenap-unison-confgen") (v "0.1.1") (d (list (d (n "hostname") (r "=0.4.0") (d #t) (k 0)) (d (n "itertools") (r "=0.12.1") (d #t) (k 0)) (d (n "serde") (r "=1.0.201") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "=0.9.34") (d #t) (k 0)))) (h "1y8p4006785n0pdfhzaqrzkw98m2kd6cylrrq0yy8235j0mzds8v")))

