(define-module (crates-io al le allegro_util) #:use-module (crates-io))

(define-public crate-allegro_util-0.0.14 (c (n "allegro_util") (v "0.0.14") (h "0mmhvb68v3j92wr6q2rxibl2fqb5bbifrvfjnjg0f1nlkjyvy1dx")))

(define-public crate-allegro_util-0.0.15 (c (n "allegro_util") (v "0.0.15") (h "0rxczkrdlvi1sdr57ahzhmi8yzbacabx4dyq6vwfz7s08bib2vyk")))

(define-public crate-allegro_util-0.0.16 (c (n "allegro_util") (v "0.0.16") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "1s1cl86gp0rf05b2s9ax8flrf8zmn16qij8hhivlcrzya74ig9zj")))

(define-public crate-allegro_util-0.0.17 (c (n "allegro_util") (v "0.0.17") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "06k0b3k1nqbp5ypxlmr6as0k7aja2w6v7rx65fzq6m25cswfim2k")))

(define-public crate-allegro_util-0.0.18 (c (n "allegro_util") (v "0.0.18") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "18yrvb6fv9pr9w0jpbwfiqmx0cxr6jxasfhldvrlakng18n6x92p")))

(define-public crate-allegro_util-0.0.20 (c (n "allegro_util") (v "0.0.20") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "0lpdxi1d8nbh5ijwfm8bsb76j1ffs6yc3y74ahcxivc1awg2nsji")))

(define-public crate-allegro_util-0.0.21 (c (n "allegro_util") (v "0.0.21") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "0j2k2q7ll74aa1krrdl766ynggv95cipxigs448cyba34cz406lg")))

(define-public crate-allegro_util-0.0.22 (c (n "allegro_util") (v "0.0.22") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "1w2f43r5lxsvp8dznndph2k6ib1mqwlsd6fczd2k8g280rid600c")))

(define-public crate-allegro_util-0.0.23 (c (n "allegro_util") (v "0.0.23") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "01x5cxcyglsajbw2q5c5j8b8d4jk1xhs08nd5vlbr75v48xipmhx")))

(define-public crate-allegro_util-0.0.24 (c (n "allegro_util") (v "0.0.24") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "09x022kxsic86f5bkqiv4582mbcadd44ycwb5m0zjc399c8531dv")))

(define-public crate-allegro_util-0.0.25 (c (n "allegro_util") (v "0.0.25") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "1s2d2vl0l3bdv4h9q31v93qfr6ywx2dmxa2054d71mivlks8i9r1")))

(define-public crate-allegro_util-0.0.26 (c (n "allegro_util") (v "0.0.26") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "1pic4by0mncj67n2ngqv3jk6blviyashgjmm44da7yyciwkp8gvw")))

(define-public crate-allegro_util-0.0.27 (c (n "allegro_util") (v "0.0.27") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "0bkyqzhfkpy0gng69zlf7z5lm2w3qrl40k1ppxw37s03vrmiwfxn")))

(define-public crate-allegro_util-0.0.28 (c (n "allegro_util") (v "0.0.28") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "1dbmd0qg3rj1vvhg9zbwvsm5vvswac6bgp5s9m0yj7k1g2vryd6q")))

(define-public crate-allegro_util-0.0.29 (c (n "allegro_util") (v "0.0.29") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "1wsci2bkmcjcky4qxvx8mq2959wgg9r8zfpzfm0mafwvpzi42w3d")))

(define-public crate-allegro_util-0.0.30 (c (n "allegro_util") (v "0.0.30") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "0bhfpx9vdfy7pr101688hm4wh6sha4jk1jf8navr2c4nd8aqm2vs")))

(define-public crate-allegro_util-0.0.31 (c (n "allegro_util") (v "0.0.31") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "0y3ingf9l78kjl61nr5wkjcf2yrkp5pzyznq3j8jhlbk9n136hj9")))

(define-public crate-allegro_util-0.0.32 (c (n "allegro_util") (v "0.0.32") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "11qvhzw5n5bdimzir3l3961gyc5p4lc90365azma5zl4anfjjmb5")))

(define-public crate-allegro_util-0.0.33 (c (n "allegro_util") (v "0.0.33") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "0a7x2gih6nvm7m7w070rf5acg589lrzvp5ll1qnig9zb3shry7nb")))

(define-public crate-allegro_util-0.0.34 (c (n "allegro_util") (v "0.0.34") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "1sfl8lvgj4rsv4p6rd7ypzqv8m4hl50gfivhqi7mikd1kay9pac2")))

(define-public crate-allegro_util-0.0.35 (c (n "allegro_util") (v "0.0.35") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "1l6bjmmfwaznplss9533k9j3svhfy8zxdqfdyyq73wr0ibgj3iv6")))

(define-public crate-allegro_util-0.0.36 (c (n "allegro_util") (v "0.0.36") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "1316sdka7vi2p6gwajn58fcccmwlss0pb6gwjigjgn3gs1bdcpvq")))

(define-public crate-allegro_util-0.0.37 (c (n "allegro_util") (v "0.0.37") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1hznbw37747g9gfnjwlzr918vzq6k81hg9l3jnszi9aa9ryp5bgc")))

(define-public crate-allegro_util-0.0.38 (c (n "allegro_util") (v "0.0.38") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "17vbra0nqlgj8kpx4zvv7hqbcwpm85g5nvfrxqdwzn6fmpsjyr0c")))

(define-public crate-allegro_util-0.0.39 (c (n "allegro_util") (v "0.0.39") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "06gc7lgcaqkzs3qibf6szf3l863lx8isqa0svp01r0plxmlqs9bi")))

(define-public crate-allegro_util-0.0.40 (c (n "allegro_util") (v "0.0.40") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1qvrj3hsylr3516b0ldc6j6vas1hjlb4nd94h8wj6vdhzsss3x9v")))

(define-public crate-allegro_util-0.0.41 (c (n "allegro_util") (v "0.0.41") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "06rrrzcd2y72yc4w0rz1jswa43sqqna8k6v61nfwc99w61rcdfjm")))

(define-public crate-allegro_util-0.0.42 (c (n "allegro_util") (v "0.0.42") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0iy9v54kml2smsakjvnhafm2w7yvzsh7npn9ql6wvr0qpac7q7yi")))

(define-public crate-allegro_util-0.0.43 (c (n "allegro_util") (v "0.0.43") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1l7sl4l77kc4770zqpzwnqh5kh4jcckmkg7d2n4wawlhyf5hrc5z")))

(define-public crate-allegro_util-0.0.44 (c (n "allegro_util") (v "0.0.44") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "004994852s41wb8f58am9i4qv11m1zjc5pg9vli1nqvvk344fkd1")))

(define-public crate-allegro_util-0.0.45 (c (n "allegro_util") (v "0.0.45") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1xbcgbqsf4h2dkdpzb7gj612gcll52691aq6d2y4aif11jv06p4m")))

(define-public crate-allegro_util-0.0.46 (c (n "allegro_util") (v "0.0.46") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1dr1bbl849vfp8h731bz7x4535spaqqg26q4hh8si6xhfg8vx42g")))

