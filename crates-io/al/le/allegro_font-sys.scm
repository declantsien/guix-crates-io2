(define-module (crates-io al le allegro_font-sys) #:use-module (crates-io))

(define-public crate-allegro_font-sys-0.0.2 (c (n "allegro_font-sys") (v "0.0.2") (d (list (d (n "allegro-sys") (r "= 0.0.2") (d #t) (k 0)))) (h "0hx924sk8bvrl2k6y9ydxii27x3llz9yiwbwxck08qvk8b94syqv") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_font-sys-0.0.3 (c (n "allegro_font-sys") (v "0.0.3") (d (list (d (n "allegro-sys") (r "= 0.0.3") (d #t) (k 0)))) (h "16r9xdhslfwb27qkw9hgf6240rn47zf9lspnjmjk704pl7nlcyrh") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_font-sys-0.0.4 (c (n "allegro_font-sys") (v "0.0.4") (d (list (d (n "allegro-sys") (r "= 0.0.4") (d #t) (k 0)))) (h "0001fd2b835v7qcn5hq2w5b65fr6is4z0h2i4fzfl0lpcd5vip3l") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_font-sys-0.0.5 (c (n "allegro_font-sys") (v "0.0.5") (d (list (d (n "allegro-sys") (r "= 0.0.5") (d #t) (k 0)))) (h "0wq0jfj2mcxh0459q87qngr1nq1sq573sp259i2vq9skq1nzx6cp") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_font-sys-0.0.6 (c (n "allegro_font-sys") (v "0.0.6") (d (list (d (n "allegro-sys") (r "= 0.0.6") (d #t) (k 0)))) (h "0wqj07irsfdp8qcw5s9yvlhvwgi0v6fr62gr7xkn42psrk894zky") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_font-sys-0.0.7 (c (n "allegro_font-sys") (v "0.0.7") (d (list (d (n "allegro-sys") (r "= 0.0.7") (d #t) (k 0)))) (h "0wnvsn5q8maf343gm1x2yyn48alf9bj4jnc2ypxpjv47z91dkdp7") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_font-sys-0.0.8 (c (n "allegro_font-sys") (v "0.0.8") (d (list (d (n "allegro-sys") (r "= 0.0.8") (d #t) (k 0)))) (h "0ispad0i6y9c93p08zyvv62qwgy46cbi980ws0pqqkhw1z7ckd29") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_font-sys-0.0.10 (c (n "allegro_font-sys") (v "0.0.10") (d (list (d (n "allegro-sys") (r "= 0.0.10") (d #t) (k 0)))) (h "0kkvchqh1n0jasvnrrkb0rnjng8p8s2ac357nr85b674wbjf22g6") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_font-sys-0.0.11 (c (n "allegro_font-sys") (v "0.0.11") (d (list (d (n "allegro-sys") (r "= 0.0.11") (d #t) (k 0)))) (h "0kyrm78gaf07cay5sn9z4j9ai82gdsbgdcxg4vxipy5vfpjl39rh") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_font-sys-0.0.12 (c (n "allegro_font-sys") (v "0.0.12") (d (list (d (n "allegro-sys") (r "= 0.0.12") (d #t) (k 0)))) (h "0l2d0654gqqg1v6298wvmdax8cxk4zm40faqynzzc7h5dz50y4cx") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_font-sys-0.0.13 (c (n "allegro_font-sys") (v "0.0.13") (d (list (d (n "allegro-sys") (r "= 0.0.13") (d #t) (k 0)))) (h "1wna0kdga2vjp0lpp75n3g7i13dglpzgjqam4rz85hj4jk0skkp6") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_font-sys-0.0.14 (c (n "allegro_font-sys") (v "0.0.14") (d (list (d (n "allegro-sys") (r "= 0.0.14") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.14") (d #t) (k 0)))) (h "0rjcp072zshll28xvcx00a1lzbn8dlwqkvn43snsjxnb37p4r9is") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_font-sys-0.0.15 (c (n "allegro_font-sys") (v "0.0.15") (d (list (d (n "allegro-sys") (r "= 0.0.15") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.15") (d #t) (k 0)))) (h "1i3c1p3ynfprvah7pqc4sa0hkzv71pcyg84zsmmzkzh7h7mqsqyw") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_font-sys-0.0.16 (c (n "allegro_font-sys") (v "0.0.16") (d (list (d (n "allegro-sys") (r "= 0.0.16") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.16") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "0n3iknl5yqq9l992f4rkqki971681qw5h21jvspbfzbg9qr4sg35") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_font-sys-0.0.17 (c (n "allegro_font-sys") (v "0.0.17") (d (list (d (n "allegro-sys") (r "= 0.0.17") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.17") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "0586524ay91qqvz9kgcin7gh0m2rni9njcg713y7lr0jn02nfc71") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_font-sys-0.0.18 (c (n "allegro_font-sys") (v "0.0.18") (d (list (d (n "allegro-sys") (r "= 0.0.18") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.18") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "1wrnxm9q3n56c3pjk13qz4jil7ib7f4mlxf9xh1sh87rg6mhf62z") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_font-sys-0.0.20 (c (n "allegro_font-sys") (v "0.0.20") (d (list (d (n "allegro-sys") (r "= 0.0.20") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.20") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "1rhm64brrc09l96zj5b4bcrahp5h8rnl9w528l1ylggzj0mdz894") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_font-sys-0.0.21 (c (n "allegro_font-sys") (v "0.0.21") (d (list (d (n "allegro-sys") (r "= 0.0.21") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.21") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "0z4vv4xcgv3hnb9dmnrzzr9amd0ksbhb7zighxbwdvn5iw7cqfss") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_font-sys-0.0.22 (c (n "allegro_font-sys") (v "0.0.22") (d (list (d (n "allegro-sys") (r "= 0.0.22") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.22") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "1qcsh434s62nass3mhicgb2ml978a6b0alyyd0fwwkyyzqmm0h1d") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_font-sys-0.0.23 (c (n "allegro_font-sys") (v "0.0.23") (d (list (d (n "allegro-sys") (r "= 0.0.23") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.23") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "1d0dwq8ahq6bxsggljgc3sani9miyrga9cdvnjy4nzqz8yjs4xdi") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_font-sys-0.0.24 (c (n "allegro_font-sys") (v "0.0.24") (d (list (d (n "allegro-sys") (r "= 0.0.24") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.24") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "19jcydcxhhvsnspshm39icyf1anb77nkidm6svhp0hvd5m6y45xr") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_font-sys-0.0.25 (c (n "allegro_font-sys") (v "0.0.25") (d (list (d (n "allegro-sys") (r "= 0.0.25") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.25") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "0b0mab5g74n2yv201kx6dw9ci4gzyx5hnxhg8yzshaq9w0jvwffn") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_font-sys-0.0.26 (c (n "allegro_font-sys") (v "0.0.26") (d (list (d (n "allegro-sys") (r "= 0.0.26") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.26") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "0vx2xcnsxj3jq5762c3l0jxz7bwb0znf4142a23zvy67rhf1j4hb") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_font-sys-0.0.27 (c (n "allegro_font-sys") (v "0.0.27") (d (list (d (n "allegro-sys") (r "= 0.0.27") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.27") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "0mw77la7vyn79f48ah9q4j7bi8ww4a9bn0zja500kvx22sj7a4zn") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_font-sys-0.0.28 (c (n "allegro_font-sys") (v "0.0.28") (d (list (d (n "allegro-sys") (r "= 0.0.28") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.28") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "0v2vvgbm0n44a1rr65q152pvjas0kc4pxkc4n6dvl55zrba1zr6c") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_font-sys-0.0.29 (c (n "allegro_font-sys") (v "0.0.29") (d (list (d (n "allegro-sys") (r "= 0.0.29") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.29") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "18n1rkgqvacy71nkir94kg7rc2ki5sgjnxmgydardj17iqcx9wym") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_font-sys-0.0.30 (c (n "allegro_font-sys") (v "0.0.30") (d (list (d (n "allegro-sys") (r "= 0.0.30") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.30") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "0mfp6smw8n5wim0wfhsp0yqlarc5xp8kk2alrwxp17l0zk1gnnzl") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_font-sys-0.0.31 (c (n "allegro_font-sys") (v "0.0.31") (d (list (d (n "allegro-sys") (r "= 0.0.31") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.31") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "0mdsfnri6g6l3wgxavkybxcbdrjfdnbkxkk6xyjmbyyibaajkg3j") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_font-sys-0.0.32 (c (n "allegro_font-sys") (v "0.0.32") (d (list (d (n "allegro-sys") (r "= 0.0.32") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.32") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "0c33p2kvv9n64v8la1igpiji49ckiqad5k7xx9vzvmywdnbwjqas") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_font-sys-0.0.33 (c (n "allegro_font-sys") (v "0.0.33") (d (list (d (n "allegro-sys") (r "= 0.0.33") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.33") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "1zjkja69zg15fxry2gy49vp3mb52r5ynh3dbprj127rvh8yddv9d") (f (quote (("link_static") ("link_none") ("link_debug")))) (l "allegro_font")))

(define-public crate-allegro_font-sys-0.0.34 (c (n "allegro_font-sys") (v "0.0.34") (d (list (d (n "allegro-sys") (r "= 0.0.34") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.34") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "1w52fr0qq1xy3q5pf8yyml2slxamvszniggsw5787nlfl1fbmv2h") (f (quote (("link_static") ("link_none") ("link_debug")))) (l "allegro_font")))

(define-public crate-allegro_font-sys-0.0.35 (c (n "allegro_font-sys") (v "0.0.35") (d (list (d (n "allegro-sys") (r "= 0.0.35") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.35") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "02rdvr16jjav3020726h0ga4wgpibj902g0rg4wzlakxcpc1bfag") (f (quote (("link_static") ("link_none") ("link_debug")))) (l "allegro_font")))

(define-public crate-allegro_font-sys-0.0.36 (c (n "allegro_font-sys") (v "0.0.36") (d (list (d (n "allegro-sys") (r "= 0.0.36") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.36") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "00rv0hml2zwh6f04mm7fc07n7fx1lcjdn6qzcqy7bagfcqvrrcr9") (f (quote (("link_static") ("link_none") ("link_debug")))) (l "allegro_font")))

(define-public crate-allegro_font-sys-0.0.37 (c (n "allegro_font-sys") (v "0.0.37") (d (list (d (n "allegro-sys") (r "= 0.0.37") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.37") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1aylqw30r64c18fydyvi64bahmf55hh4sqic8dha6kckpi00isp4") (f (quote (("link_static") ("link_none") ("link_debug")))) (l "allegro_font")))

(define-public crate-allegro_font-sys-0.0.38 (c (n "allegro_font-sys") (v "0.0.38") (d (list (d (n "allegro-sys") (r "= 0.0.38") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.38") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "15fbbgdagvhb4nkzayr1hfzbqjx5l86hj8cahd4x521k40wimrz9") (f (quote (("link_static") ("link_none") ("link_debug")))) (l "allegro_font")))

(define-public crate-allegro_font-sys-0.0.39 (c (n "allegro_font-sys") (v "0.0.39") (d (list (d (n "allegro-sys") (r "= 0.0.39") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.39") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "04vbknw2fjmfa7yjj5zlj19bl5l6m0d0nmsgs4if5wivwiq91x7q") (f (quote (("link_static") ("link_none") ("link_debug")))) (l "allegro_font")))

(define-public crate-allegro_font-sys-0.0.40 (c (n "allegro_font-sys") (v "0.0.40") (d (list (d (n "allegro-sys") (r "= 0.0.40") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.40") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "09raxifam6ph0fqdf2l0vkrhgdk1sgp5dphbk6kh7d1s7djaxzgk") (f (quote (("link_static") ("link_none") ("link_debug")))) (l "allegro_font")))

(define-public crate-allegro_font-sys-0.0.41 (c (n "allegro_font-sys") (v "0.0.41") (d (list (d (n "allegro-sys") (r "=0.0.41") (d #t) (k 0)) (d (n "allegro_util") (r "=0.0.41") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0423qd2n55jyff9qb2z4i6n24v7l7lc6bb51892g5mmm0gxr8z05") (f (quote (("link_static") ("link_none") ("link_debug")))) (l "allegro_font")))

(define-public crate-allegro_font-sys-0.0.42 (c (n "allegro_font-sys") (v "0.0.42") (d (list (d (n "allegro-sys") (r "=0.0.42") (d #t) (k 0)) (d (n "allegro_util") (r "=0.0.42") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1lsmhygscjcbjzc9vfpfiqd96k8dvf26lylr04q4dxpbmqih5aly") (f (quote (("link_static") ("link_none") ("link_debug")))) (l "allegro_font")))

(define-public crate-allegro_font-sys-0.0.43 (c (n "allegro_font-sys") (v "0.0.43") (d (list (d (n "allegro-sys") (r "=0.0.43") (d #t) (k 0)) (d (n "allegro_util") (r "=0.0.43") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0jr5ip0gzha43kf937l9s90pl2d73s3cljqs16j5ns0jb7grh7l0") (f (quote (("link_static") ("link_none") ("link_debug")))) (l "allegro_font")))

(define-public crate-allegro_font-sys-0.0.44 (c (n "allegro_font-sys") (v "0.0.44") (d (list (d (n "allegro-sys") (r "=0.0.44") (d #t) (k 0)) (d (n "allegro_util") (r "=0.0.44") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "19jkgdkilviir6cmc7hl1xim873n3z1n38daq1f5ypqp4ip1jzzb") (f (quote (("link_static") ("link_none") ("link_debug")))) (l "allegro_font")))

(define-public crate-allegro_font-sys-0.0.45 (c (n "allegro_font-sys") (v "0.0.45") (d (list (d (n "allegro-sys") (r "=0.0.45") (d #t) (k 0)) (d (n "allegro_util") (r "=0.0.45") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "19zg0g8yriflwvj5azy4mgd4rzd8shchp5cjx89jqhcxf201pjbf") (f (quote (("link_static") ("link_none") ("link_debug")))) (l "allegro_font")))

(define-public crate-allegro_font-sys-0.0.46 (c (n "allegro_font-sys") (v "0.0.46") (d (list (d (n "allegro-sys") (r "=0.0.46") (d #t) (k 0)) (d (n "allegro_util") (r "=0.0.46") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0r3k2mfa8nnrgfp0z9vp9ljkklsnh1rgzrqx1y7n7hqkfgxqxkia") (f (quote (("link_static") ("link_none") ("link_debug")))) (l "allegro_font")))

