(define-module (crates-io al le allegro-motor-derive) #:use-module (crates-io))

(define-public crate-allegro-motor-derive-0.1.0 (c (n "allegro-motor-derive") (v "0.1.0") (d (list (d (n "bilge") (r "^0.2.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (d #t) (k 0)))) (h "0qyp6yp2hbwb8px38jqavhlmmwqkj6byv6m9b6yfbqlqkcgx60yf") (r "1.65.0")))

