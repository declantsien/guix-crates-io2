(define-module (crates-io al le allegro_dialog-sys) #:use-module (crates-io))

(define-public crate-allegro_dialog-sys-0.0.2 (c (n "allegro_dialog-sys") (v "0.0.2") (d (list (d (n "allegro-sys") (r "= 0.0.2") (d #t) (k 0)))) (h "02rab7w7rvhncsm13yc9m88sqrqwyfd8c32lz86wr97rrkhaqf1k") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_dialog-sys-0.0.3 (c (n "allegro_dialog-sys") (v "0.0.3") (d (list (d (n "allegro-sys") (r "= 0.0.3") (d #t) (k 0)))) (h "1ami116nycxs9dajpbw9vvj042bhikrn8i88rwbmlhi5jix468ib") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_dialog-sys-0.0.4 (c (n "allegro_dialog-sys") (v "0.0.4") (d (list (d (n "allegro-sys") (r "= 0.0.4") (d #t) (k 0)))) (h "1qhvrva2d0wqh89s1fa8hxfysaczmvhpxkpwj5scc9s27py8z2p1") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_dialog-sys-0.0.5 (c (n "allegro_dialog-sys") (v "0.0.5") (d (list (d (n "allegro-sys") (r "= 0.0.5") (d #t) (k 0)))) (h "1vvzbyn1mwlw8nag01xf1d12qbzjaycx91bhinzdqfxpnzf20qxg") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_dialog-sys-0.0.6 (c (n "allegro_dialog-sys") (v "0.0.6") (d (list (d (n "allegro-sys") (r "= 0.0.6") (d #t) (k 0)))) (h "18112i7m3jwws88z1vqsc9kpi1mbjm82n4q03a0qwbw9r6487zpd") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_dialog-sys-0.0.7 (c (n "allegro_dialog-sys") (v "0.0.7") (d (list (d (n "allegro-sys") (r "= 0.0.7") (d #t) (k 0)))) (h "0yp4l41xb8bdhvv4wq59byamgh6a1zcfrxrdgv635ih3akag1w8y") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_dialog-sys-0.0.8 (c (n "allegro_dialog-sys") (v "0.0.8") (d (list (d (n "allegro-sys") (r "= 0.0.8") (d #t) (k 0)))) (h "0cwkrqakljybk9jn64dncmxgkqx0g783vf7nvcwf42hwr516hcng") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_dialog-sys-0.0.10 (c (n "allegro_dialog-sys") (v "0.0.10") (d (list (d (n "allegro-sys") (r "= 0.0.10") (d #t) (k 0)))) (h "0h2d0vdc3rf6l9mq11fivgyxmf8920a4iiqd8amr8y8hjjsh8rd0") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_dialog-sys-0.0.11 (c (n "allegro_dialog-sys") (v "0.0.11") (d (list (d (n "allegro-sys") (r "= 0.0.11") (d #t) (k 0)))) (h "1xpl3wzbqirwp59kr0vk9z9j4daq3k237cydd3c1nw11y9ihakc0") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_dialog-sys-0.0.12 (c (n "allegro_dialog-sys") (v "0.0.12") (d (list (d (n "allegro-sys") (r "= 0.0.12") (d #t) (k 0)))) (h "0nsysf0k0n1yjj7ihw55gpspywhszdvdb26b9ph72h1zpx6wccv0") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_dialog-sys-0.0.13 (c (n "allegro_dialog-sys") (v "0.0.13") (d (list (d (n "allegro-sys") (r "= 0.0.13") (d #t) (k 0)))) (h "0l7a0jyhgmkybd9dhg5nm7qh2sikbzpyjz9khxcw317wakmkzwq1") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_dialog-sys-0.0.14 (c (n "allegro_dialog-sys") (v "0.0.14") (d (list (d (n "allegro-sys") (r "= 0.0.14") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.14") (d #t) (k 0)))) (h "0s013q1iqh52jlkwfmpib49sl9r5kj0kvl9n5d7prlxn0pgiqv3y") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_dialog-sys-0.0.15 (c (n "allegro_dialog-sys") (v "0.0.15") (d (list (d (n "allegro-sys") (r "= 0.0.15") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.15") (d #t) (k 0)))) (h "01d8jrqn6rxql8jhm0v4nj494fhd28fbk0rzb1ya84d1hwlz918a") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_dialog-sys-0.0.16 (c (n "allegro_dialog-sys") (v "0.0.16") (d (list (d (n "allegro-sys") (r "= 0.0.16") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.16") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "0j8c1vnal6vf8sa9gsvnl62ffwm9cnwb78v6p6fy5zcxb35b7yyz") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_dialog-sys-0.0.17 (c (n "allegro_dialog-sys") (v "0.0.17") (d (list (d (n "allegro-sys") (r "= 0.0.17") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.17") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "0niahlwjkgyfh938953b8zsgr30p6jwjp10dihnfqabsghsydddk") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_dialog-sys-0.0.18 (c (n "allegro_dialog-sys") (v "0.0.18") (d (list (d (n "allegro-sys") (r "= 0.0.18") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.18") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "0isyiga6sv996xb827d0v57pwiskckim621mpsrf0gdg8prgzcrk") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_dialog-sys-0.0.20 (c (n "allegro_dialog-sys") (v "0.0.20") (d (list (d (n "allegro-sys") (r "= 0.0.20") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.20") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "0fmmw4z7v7519jkdhm1wbikkr9z5isxvhi1jflhzapw9j9vp7hnj") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_dialog-sys-0.0.21 (c (n "allegro_dialog-sys") (v "0.0.21") (d (list (d (n "allegro-sys") (r "= 0.0.21") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.21") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "18727nss4ky76ldsah7m023bgwx4fg7dp1k6ipwadpmlssybfyhm") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_dialog-sys-0.0.22 (c (n "allegro_dialog-sys") (v "0.0.22") (d (list (d (n "allegro-sys") (r "= 0.0.22") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.22") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "1iv258gqcf6hs4cw9239w7ijmy991sfml1rb9cffzbfg3arvmjsp") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_dialog-sys-0.0.23 (c (n "allegro_dialog-sys") (v "0.0.23") (d (list (d (n "allegro-sys") (r "= 0.0.23") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.23") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "07hnzc1vz6330d3c4agvll6s1byz4q1b67i9dnax9jznwv1c5jxr") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_dialog-sys-0.0.24 (c (n "allegro_dialog-sys") (v "0.0.24") (d (list (d (n "allegro-sys") (r "= 0.0.24") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.24") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "0dhrsah5294p4zhxddah4rw09xd7ni7p43bgn998x9i4mh81az35") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_dialog-sys-0.0.25 (c (n "allegro_dialog-sys") (v "0.0.25") (d (list (d (n "allegro-sys") (r "= 0.0.25") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.25") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "0d5a6k7bxqdaj2mr9w9jd99fpkw7fbahigrldc1nyz5blxp2fwqg") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_dialog-sys-0.0.26 (c (n "allegro_dialog-sys") (v "0.0.26") (d (list (d (n "allegro-sys") (r "= 0.0.26") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.26") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "04ykj46phm1ns5q8spa0d28dwag8l4aazpjg4zl2fpfqg7pipqn5") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_dialog-sys-0.0.27 (c (n "allegro_dialog-sys") (v "0.0.27") (d (list (d (n "allegro-sys") (r "= 0.0.27") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.27") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "0shida0lz8n152ybg3rwrwcn8scr9nzzrbs2dm7nn19x1i7iqbb5") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_dialog-sys-0.0.28 (c (n "allegro_dialog-sys") (v "0.0.28") (d (list (d (n "allegro-sys") (r "= 0.0.28") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.28") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "0nz65ziljbvrw7cr7gy6qsql73x6qnfa8b59f12qqfrfvy384z78") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_dialog-sys-0.0.29 (c (n "allegro_dialog-sys") (v "0.0.29") (d (list (d (n "allegro-sys") (r "= 0.0.29") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.29") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "119pgrm3nbmszzhkra6gss4vc5mva0k1yp6jivrwf44sbbp2gdn2") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_dialog-sys-0.0.30 (c (n "allegro_dialog-sys") (v "0.0.30") (d (list (d (n "allegro-sys") (r "= 0.0.30") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.30") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "0520k9iilscx6hzjv1ryqm8srmnh9caq6v1axf8ww8phf9c283y7") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_dialog-sys-0.0.31 (c (n "allegro_dialog-sys") (v "0.0.31") (d (list (d (n "allegro-sys") (r "= 0.0.31") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.31") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "0rnz68ycm5mwxvanr6a27102am4vk58y732svyl2y027lxk5ja41") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_dialog-sys-0.0.32 (c (n "allegro_dialog-sys") (v "0.0.32") (d (list (d (n "allegro-sys") (r "= 0.0.32") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.32") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "047qfskqz0yf1grs6vdjrbc5w5kq1q50mgk5lsi3r9vk9nzpbvzf") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_dialog-sys-0.0.33 (c (n "allegro_dialog-sys") (v "0.0.33") (d (list (d (n "allegro-sys") (r "= 0.0.33") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.33") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "1zhpdkba2mlwrjps7gijkaz7vc6psd7zwb3mkypzppypdzw0hjq9") (f (quote (("link_static") ("link_none") ("link_debug")))) (l "allegro_dialog")))

(define-public crate-allegro_dialog-sys-0.0.34 (c (n "allegro_dialog-sys") (v "0.0.34") (d (list (d (n "allegro-sys") (r "= 0.0.34") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.34") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "1qc9p8crx5cd0jpjvx0ad7mmr1yy6g0zck5h3hhncg0dhr46ds3z") (f (quote (("link_static") ("link_none") ("link_debug")))) (l "allegro_dialog")))

(define-public crate-allegro_dialog-sys-0.0.35 (c (n "allegro_dialog-sys") (v "0.0.35") (d (list (d (n "allegro-sys") (r "= 0.0.35") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.35") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "1xi5l7d68lh3x6chnf35g8sygjq922ngpgmghqkix2ppy6k3b03w") (f (quote (("link_static") ("link_none") ("link_debug")))) (l "allegro_dialog")))

(define-public crate-allegro_dialog-sys-0.0.36 (c (n "allegro_dialog-sys") (v "0.0.36") (d (list (d (n "allegro-sys") (r "= 0.0.36") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.36") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "05f5a7fffblgjwl01qhkay03aa1x3fdbhgycgvmzir5jy1v9vlw7") (f (quote (("link_static") ("link_none") ("link_debug")))) (l "allegro_dialog")))

(define-public crate-allegro_dialog-sys-0.0.37 (c (n "allegro_dialog-sys") (v "0.0.37") (d (list (d (n "allegro-sys") (r "= 0.0.37") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.37") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0lvr68ikzicbswzrxzkl0krvwda6wgyilgp14a6ap036akx2vfc0") (f (quote (("link_static") ("link_none") ("link_debug")))) (l "allegro_dialog")))

(define-public crate-allegro_dialog-sys-0.0.38 (c (n "allegro_dialog-sys") (v "0.0.38") (d (list (d (n "allegro-sys") (r "= 0.0.38") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.38") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0qxhvndm2dp8g29ifr8kl2n7icl7y2rli8fjd72a6xsbysyq9iwq") (f (quote (("link_static") ("link_none") ("link_debug")))) (l "allegro_dialog")))

(define-public crate-allegro_dialog-sys-0.0.39 (c (n "allegro_dialog-sys") (v "0.0.39") (d (list (d (n "allegro-sys") (r "= 0.0.39") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.39") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1ca6sklv01ymkjsgbp412s6yy01lfwb8dcdpqdkld6fxhgjc695p") (f (quote (("link_static") ("link_none") ("link_debug")))) (l "allegro_dialog")))

(define-public crate-allegro_dialog-sys-0.0.40 (c (n "allegro_dialog-sys") (v "0.0.40") (d (list (d (n "allegro-sys") (r "= 0.0.40") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.40") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1hpbbzlqcz07frlmcgd761pc70glwcdnzywqynz454a6f37gf2m8") (f (quote (("link_static") ("link_none") ("link_debug")))) (l "allegro_dialog")))

(define-public crate-allegro_dialog-sys-0.0.41 (c (n "allegro_dialog-sys") (v "0.0.41") (d (list (d (n "allegro-sys") (r "=0.0.41") (d #t) (k 0)) (d (n "allegro_util") (r "=0.0.41") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0v9ms5l7k3nkcl2609cx74mlhbghkpyv9vd6q9s8p9n57j65mjzg") (f (quote (("link_static") ("link_none") ("link_debug")))) (l "allegro_dialog")))

(define-public crate-allegro_dialog-sys-0.0.42 (c (n "allegro_dialog-sys") (v "0.0.42") (d (list (d (n "allegro-sys") (r "=0.0.42") (d #t) (k 0)) (d (n "allegro_util") (r "=0.0.42") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1k1xvw9lz8jivnhwfh102yg5a1402ziyidbndvrij8n2d7baq1dl") (f (quote (("link_static") ("link_none") ("link_debug")))) (l "allegro_dialog")))

(define-public crate-allegro_dialog-sys-0.0.43 (c (n "allegro_dialog-sys") (v "0.0.43") (d (list (d (n "allegro-sys") (r "=0.0.43") (d #t) (k 0)) (d (n "allegro_util") (r "=0.0.43") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1ik5ida1gnprif1gig93afk7x2n7mpmln13jvpl3m8wk0iqhc8fy") (f (quote (("link_static") ("link_none") ("link_debug")))) (l "allegro_dialog")))

(define-public crate-allegro_dialog-sys-0.0.44 (c (n "allegro_dialog-sys") (v "0.0.44") (d (list (d (n "allegro-sys") (r "=0.0.44") (d #t) (k 0)) (d (n "allegro_util") (r "=0.0.44") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0m6ymcwn19cc5qkml4c5qy2hgf0hw6pf0jz73aqr25z1y29y5c4h") (f (quote (("link_static") ("link_none") ("link_debug")))) (l "allegro_dialog")))

(define-public crate-allegro_dialog-sys-0.0.45 (c (n "allegro_dialog-sys") (v "0.0.45") (d (list (d (n "allegro-sys") (r "=0.0.45") (d #t) (k 0)) (d (n "allegro_util") (r "=0.0.45") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "12mzvl4snjiv0iisi593h6pd61wvphh6h1awn78bg2qcbzc6br9d") (f (quote (("link_static") ("link_none") ("link_debug")))) (l "allegro_dialog")))

(define-public crate-allegro_dialog-sys-0.0.46 (c (n "allegro_dialog-sys") (v "0.0.46") (d (list (d (n "allegro-sys") (r "=0.0.46") (d #t) (k 0)) (d (n "allegro_util") (r "=0.0.46") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1ldwxn0hj0c0fddnybwsh3aq1diip4c0danb2gmh4yrwv1dr2wb8") (f (quote (("link_static") ("link_none") ("link_debug")))) (l "allegro_dialog")))

