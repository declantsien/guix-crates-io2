(define-module (crates-io al le allegro_image-sys) #:use-module (crates-io))

(define-public crate-allegro_image-sys-0.0.2 (c (n "allegro_image-sys") (v "0.0.2") (h "1lbhyzal26jaydblhsxs6hbg7iqsrwlmqbgzz5zfcdmcysnd059p") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_image-sys-0.0.3 (c (n "allegro_image-sys") (v "0.0.3") (h "0n9lprpjqc1qm6qwnb9a9rfr3yfr7hknjz1rmrkic6cjhmxpfczz") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_image-sys-0.0.4 (c (n "allegro_image-sys") (v "0.0.4") (h "0bigk3gba96lfv0klwwlr51y74xcrixmf08q29y725rsm94h49h2") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_image-sys-0.0.5 (c (n "allegro_image-sys") (v "0.0.5") (h "1ps09dpbk6wi7pwfw54n2rspxi02yqlmfraniskh9bpbmr1qkvk6") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_image-sys-0.0.6 (c (n "allegro_image-sys") (v "0.0.6") (h "06pjhmqw4q4x1sf21fb7ns3nas75f2b2b1h8prps65c5yadcp7vs") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_image-sys-0.0.7 (c (n "allegro_image-sys") (v "0.0.7") (h "15lwp3vjzp635z6dhblbxk1n8ysazm02gpmwi938s63lpn1rpqnf") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_image-sys-0.0.8 (c (n "allegro_image-sys") (v "0.0.8") (h "1ip0pmafbfyamf31b6ni82lh9xc597y8383yhxb515c5vmniv5px") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_image-sys-0.0.10 (c (n "allegro_image-sys") (v "0.0.10") (h "1kglj71r0f53pwsfc1bj08svm3wzbn54b2kg6ajglc1yfqddnac8") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_image-sys-0.0.11 (c (n "allegro_image-sys") (v "0.0.11") (h "13c71jzlm8d60dxrms5za6qyz8xsg2anbw0a1zwklwpbqazl4xkr") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_image-sys-0.0.12 (c (n "allegro_image-sys") (v "0.0.12") (h "0ffjmhjik2irbq668h4phckbf9phn2ws8nq943arkb50kv383k2j") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_image-sys-0.0.13 (c (n "allegro_image-sys") (v "0.0.13") (h "01qbl5ganh9516j1gy3id65g8nmz01g9h1z19d9r2zj2l1mpdsza") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_image-sys-0.0.14 (c (n "allegro_image-sys") (v "0.0.14") (d (list (d (n "allegro_util") (r "= 0.0.14") (d #t) (k 0)))) (h "1x9wa6zhga9aw8cijzmfdqnr1a78sr0yp9cpvd3dw31yc8pgf8xf") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_image-sys-0.0.15 (c (n "allegro_image-sys") (v "0.0.15") (d (list (d (n "allegro_util") (r "= 0.0.15") (d #t) (k 0)))) (h "1hi3ayvph1disjmqnj2j17f8jfm94mhwl1chzxply983cf8xgy5s") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_image-sys-0.0.16 (c (n "allegro_image-sys") (v "0.0.16") (d (list (d (n "allegro_util") (r "= 0.0.16") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "0cy0r0ifq4ypz020zaxb3q3mkw4lcic1a5i5s5dmrk1k1c74g9ic") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_image-sys-0.0.17 (c (n "allegro_image-sys") (v "0.0.17") (d (list (d (n "allegro_util") (r "= 0.0.17") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "1hqz4s7k7dzlq33v306l8f9ar6q52aqcbhrnk8igh75yvd6n70n8") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_image-sys-0.0.18 (c (n "allegro_image-sys") (v "0.0.18") (d (list (d (n "allegro_util") (r "= 0.0.18") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "1nqx3qwkz7l96njslqlmindrhwh0229lgj6myfnas8lp6zw6apn8") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_image-sys-0.0.20 (c (n "allegro_image-sys") (v "0.0.20") (d (list (d (n "allegro_util") (r "= 0.0.20") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "0yqz4s8y92kp1w0f6801maxv029qa4j5q7jzi2959ifazk2ma37k") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_image-sys-0.0.21 (c (n "allegro_image-sys") (v "0.0.21") (d (list (d (n "allegro_util") (r "= 0.0.21") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "1fbjw0kh5f09svqr5b1v7dg3fiaz4b3259fkck36hr9pfhy4fh7k") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_image-sys-0.0.22 (c (n "allegro_image-sys") (v "0.0.22") (d (list (d (n "allegro_util") (r "= 0.0.22") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "1kkdhzgigq0z1m5qawisxvvj0bf4gna4i9bqgsyvlih9vj14ci8b") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_image-sys-0.0.23 (c (n "allegro_image-sys") (v "0.0.23") (d (list (d (n "allegro_util") (r "= 0.0.23") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "1wbikx9m9q1id3vy8wx7mpf101bh8vciq6scy1s5dwbr9isvq36q") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_image-sys-0.0.24 (c (n "allegro_image-sys") (v "0.0.24") (d (list (d (n "allegro_util") (r "= 0.0.24") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "071dmrjd85mnk66k5k6cxbscvg36b4489xahsh3987lrj5bjrfpw") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_image-sys-0.0.25 (c (n "allegro_image-sys") (v "0.0.25") (d (list (d (n "allegro_util") (r "= 0.0.25") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "0adng2aw3dnllcldm5plxb3d1qcza91lzqz9pqvz0c726h72nh67") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_image-sys-0.0.26 (c (n "allegro_image-sys") (v "0.0.26") (d (list (d (n "allegro_util") (r "= 0.0.26") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "0qqz013hd1f23w0g6mipj6zn4cqiliniiq0syrb3zhnwg3lchknb") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_image-sys-0.0.27 (c (n "allegro_image-sys") (v "0.0.27") (d (list (d (n "allegro_util") (r "= 0.0.27") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "1y0fmp5iki1znabmkjpwp3vnypnrh06viw08yis6q1m81hp73v24") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_image-sys-0.0.28 (c (n "allegro_image-sys") (v "0.0.28") (d (list (d (n "allegro_util") (r "= 0.0.28") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "03p7h674x8ji3nvwk828hxzaz2qs3gnclij43a9wba6798dziwag") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_image-sys-0.0.29 (c (n "allegro_image-sys") (v "0.0.29") (d (list (d (n "allegro_util") (r "= 0.0.29") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "0hk4bgkbsq3m4qvpybz3r97ni25c7l193hsjrwjnbwd9xqf2q3sr") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_image-sys-0.0.30 (c (n "allegro_image-sys") (v "0.0.30") (d (list (d (n "allegro_util") (r "= 0.0.30") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "0gb1k67661fcnmk0xqp6lrniygffi6f2iyyd7zn7dak1636azw7s") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_image-sys-0.0.31 (c (n "allegro_image-sys") (v "0.0.31") (d (list (d (n "allegro_util") (r "= 0.0.31") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "094l07b23m3cj51rmf9d0mqwrvg5dzzd3pa4k5nm2f1ir9l74zzr") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_image-sys-0.0.32 (c (n "allegro_image-sys") (v "0.0.32") (d (list (d (n "allegro_util") (r "= 0.0.32") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "079hc9lvzkicn2ghx0g3xvajz99ffp0a9cznf4hfib7mih4aq9q4") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_image-sys-0.0.33 (c (n "allegro_image-sys") (v "0.0.33") (d (list (d (n "allegro_util") (r "= 0.0.33") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "171nkr3bqw8z1mhyl89kh2xv2spixi2aiwi83vaabn23iskq28l5") (f (quote (("link_static") ("link_none") ("link_debug")))) (l "allegro_image")))

(define-public crate-allegro_image-sys-0.0.34 (c (n "allegro_image-sys") (v "0.0.34") (d (list (d (n "allegro_util") (r "= 0.0.34") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "089x27gj6di3dvk5s2vl6b5xcwqzx10q4mp8nv3rsah9zc9fdbm1") (f (quote (("link_static") ("link_none") ("link_debug")))) (l "allegro_image")))

(define-public crate-allegro_image-sys-0.0.35 (c (n "allegro_image-sys") (v "0.0.35") (d (list (d (n "allegro_util") (r "= 0.0.35") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "1hfnyximkslnm89yxnyl9cwnjz1x7fy8xir6hrm1caxn0n39a60x") (f (quote (("link_static") ("link_none") ("link_debug")))) (l "allegro_image")))

(define-public crate-allegro_image-sys-0.0.36 (c (n "allegro_image-sys") (v "0.0.36") (d (list (d (n "allegro_util") (r "= 0.0.36") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "1r5ikzfgssi9hjnblqbg0f7xbkg3gsf22zl8d4pcrx3k9k4pzk3p") (f (quote (("link_static") ("link_none") ("link_debug")))) (l "allegro_image")))

(define-public crate-allegro_image-sys-0.0.37 (c (n "allegro_image-sys") (v "0.0.37") (d (list (d (n "allegro_util") (r "= 0.0.37") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "031n0nisw6gf3pk7zg5kh5kks1kcry2d6lackmj7b9dm0n26z6yy") (f (quote (("link_static") ("link_none") ("link_debug")))) (l "allegro_image")))

(define-public crate-allegro_image-sys-0.0.38 (c (n "allegro_image-sys") (v "0.0.38") (d (list (d (n "allegro_util") (r "= 0.0.38") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1z3n4kfkpcad03h1vmy16q78clswdws5a470s9y1d2z892s2alj8") (f (quote (("link_static") ("link_none") ("link_debug")))) (l "allegro_image")))

(define-public crate-allegro_image-sys-0.0.39 (c (n "allegro_image-sys") (v "0.0.39") (d (list (d (n "allegro_util") (r "= 0.0.39") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1b7f03f3yncxgvlj6p514y5rx7pmyln20rnhn0wk6dlaprahbwc0") (f (quote (("link_static") ("link_none") ("link_debug")))) (l "allegro_image")))

(define-public crate-allegro_image-sys-0.0.40 (c (n "allegro_image-sys") (v "0.0.40") (d (list (d (n "allegro_util") (r "= 0.0.40") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0ihdh3xr3j3wsmcdrl9i52ipjdmhihywh18bd310g6jsfcrhyy4q") (f (quote (("link_static") ("link_none") ("link_debug")))) (l "allegro_image")))

(define-public crate-allegro_image-sys-0.0.41 (c (n "allegro_image-sys") (v "0.0.41") (d (list (d (n "allegro_util") (r "=0.0.41") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1nv21b6z05mpinhs2av9vpsbzwjvin58i0x5jbl42mm98f3h8n7i") (f (quote (("link_static") ("link_none") ("link_debug")))) (l "allegro_image")))

(define-public crate-allegro_image-sys-0.0.42 (c (n "allegro_image-sys") (v "0.0.42") (d (list (d (n "allegro_util") (r "=0.0.42") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "18fn9lm0g15s5arrvv2q3nprni22fniarb0x04dc9vrvp507pfmy") (f (quote (("link_static") ("link_none") ("link_debug")))) (l "allegro_image")))

(define-public crate-allegro_image-sys-0.0.43 (c (n "allegro_image-sys") (v "0.0.43") (d (list (d (n "allegro_util") (r "=0.0.43") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1sk8f79il3p120knrz8rvd8lmc3w49dzv0gqx0gl44l8375x3smh") (f (quote (("link_static") ("link_none") ("link_debug")))) (l "allegro_image")))

(define-public crate-allegro_image-sys-0.0.44 (c (n "allegro_image-sys") (v "0.0.44") (d (list (d (n "allegro_util") (r "=0.0.44") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0gnag2jhkjm313kwps1qs020fa3l792dhrrqgjr9ijn5fr03x8bz") (f (quote (("link_static") ("link_none") ("link_debug")))) (l "allegro_image")))

(define-public crate-allegro_image-sys-0.0.45 (c (n "allegro_image-sys") (v "0.0.45") (d (list (d (n "allegro_util") (r "=0.0.45") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0mnizv3q69vp6r5iwya7zaphaj48j60pjwjaayhkfcb7lpcsz8mm") (f (quote (("link_static") ("link_none") ("link_debug")))) (l "allegro_image")))

(define-public crate-allegro_image-sys-0.0.46 (c (n "allegro_image-sys") (v "0.0.46") (d (list (d (n "allegro_util") (r "=0.0.46") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0h42fwl6xm51a735bhhn907i917ypc72flya3aavj3mn9m2wbnvb") (f (quote (("link_static") ("link_none") ("link_debug")))) (l "allegro_image")))

