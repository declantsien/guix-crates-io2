(define-module (crates-io al le alleakator) #:use-module (crates-io))

(define-public crate-alleakator-1.0.0 (c (n "alleakator") (v "1.0.0") (h "1mkp2d45sj0085jfriahdxak9agyr0rs9s1wqnc1x3y0fnllpfx0")))

(define-public crate-alleakator-1.0.1 (c (n "alleakator") (v "1.0.1") (h "01qdwy5fgmf9lgfz07rvpkra9v0hg4nck931lsf7xg9jay3sl66m")))

(define-public crate-alleakator-1.0.2 (c (n "alleakator") (v "1.0.2") (h "1x81ax95d5pm5mxsvy9y040hjci15fyk3x34v9ygw7xn57sn4fsz")))

(define-public crate-alleakator-1.0.3 (c (n "alleakator") (v "1.0.3") (h "1i794ssd3vz1faxlpmn7yc3ysqszq70s4qxxaaacjygb1kh7g6ml")))

(define-public crate-alleakator-1.0.4 (c (n "alleakator") (v "1.0.4") (h "0w51l232lzq1nig0gwiwidgdaal1z0xx7skwr4y63ca5hg7lfpvg")))

