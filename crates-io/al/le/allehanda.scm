(define-module (crates-io al le allehanda) #:use-module (crates-io))

(define-public crate-allehanda-0.1.0 (c (n "allehanda") (v "0.1.0") (h "0y56z7508mbj45ikr5cqw641yckl24c33hzwp82j1r84algx9akz")))

(define-public crate-allehanda-0.1.1 (c (n "allehanda") (v "0.1.1") (h "010gldr5j9vbl5dfw13sy8k04n2c5f2aygl2skwwyknxlcc9yvk1")))

(define-public crate-allehanda-0.1.2 (c (n "allehanda") (v "0.1.2") (h "1zdk7zblraqvfn9g8gjhj5dmrsb576vl2ba4a4s58nwihiihixfb")))

(define-public crate-allehanda-0.1.3 (c (n "allehanda") (v "0.1.3") (h "0jiw8l07fxbfm1zcd4l3r9lc1dqiq0s9ybla5xnfnn66r8zlmrlf")))

(define-public crate-allehanda-0.1.4 (c (n "allehanda") (v "0.1.4") (h "15kii7cmq9wwl48gqkc6nqsnacgwzdmkzcs343p25211n09j8b07")))

