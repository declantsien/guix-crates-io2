(define-module (crates-io al le allenap-tftp-offload) #:use-module (crates-io))

(define-public crate-allenap-tftp-offload-0.2.0 (c (n "allenap-tftp-offload") (v "0.2.0") (d (list (d (n "allenap-libtftp") (r "^0.2.0") (d #t) (k 0)) (d (n "clap") (r "^2.19.1") (d #t) (k 0)) (d (n "slog") (r "^1.3.2") (d #t) (k 0)) (d (n "slog-term") (r "^1.3.3") (d #t) (k 0)))) (h "1bgawxgyf7fhipd2gavj5fg54z6kp10hw0js4pjsrsjfpk12zpwa")))

(define-public crate-allenap-tftp-offload-0.2.1 (c (n "allenap-tftp-offload") (v "0.2.1") (d (list (d (n "allenap-libtftp") (r "^0.2.1") (d #t) (k 0)) (d (n "clap") (r "^2.19.1") (d #t) (k 0)) (d (n "slog") (r "^2.4.0") (d #t) (k 0)) (d (n "slog-term") (r "^2.4.0") (d #t) (k 0)))) (h "0iwc7sqrmdjq5hgxf4zsk5ph9gx6584l77z8g62a6bfgjz8b5bbz")))

(define-public crate-allenap-tftp-offload-0.2.2 (c (n "allenap-tftp-offload") (v "0.2.2") (d (list (d (n "allenap-libtftp") (r "^0.2.1") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "slog") (r "^2.5.0") (d #t) (k 0)) (d (n "slog-term") (r "^2.4.1") (d #t) (k 0)))) (h "0lpdlrlw8skb6bjhc4cikkarjl7cgzq31wi6lb603h57wm02wyji")))

