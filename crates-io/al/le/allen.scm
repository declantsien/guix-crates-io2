(define-module (crates-io al le allen) #:use-module (crates-io))

(define-public crate-allen-0.1.0 (c (n "allen") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.60") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "hound") (r "^3.4.0") (d #t) (k 2)) (d (n "lewton") (r "^0.10.2") (d #t) (k 2)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0010xb06mpn1ga3h1vc6vrxlzv4ygk0jbvbjxnbilgfz78j5mzw4")))

(define-public crate-allen-0.2.0 (c (n "allen") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.60") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "hound") (r "^3.4.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "lewton") (r "^0.10.2") (d #t) (k 2)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "17qz5l76isiyyc482lsz25if3iw5s2nad6nww5dxqcpb4jzh2lm4")))

