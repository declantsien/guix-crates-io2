(define-module (crates-io al le allen_interval_algebra) #:use-module (crates-io))

(define-public crate-allen_interval_algebra-0.1.0 (c (n "allen_interval_algebra") (v "0.1.0") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "0k89c8j3yi80rh47a21w0yqwrb6mz1vnyqsv8hk3ym5yl88jlprw")))

(define-public crate-allen_interval_algebra-0.1.1 (c (n "allen_interval_algebra") (v "0.1.1") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "10sdqykyq8x3z388zqa0mgq7jgm2yfglawh4skbp6gfx198wrzxp")))

(define-public crate-allen_interval_algebra-0.1.2 (c (n "allen_interval_algebra") (v "0.1.2") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "07x49pihv48ji5wdzms6fs2cw0mddnffxdfg1ybhh6dc1pgywgf9")))

