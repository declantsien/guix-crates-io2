(define-module (crates-io al le allegro-motor-drivers) #:use-module (crates-io))

(define-public crate-allegro-motor-drivers-0.1.0 (c (n "allegro-motor-drivers") (v "0.1.0") (d (list (d (n "allegro-motor-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "bilge") (r "^0.2.0") (d #t) (k 0)) (d (n "embedded-hal") (r "^1.0.0-rc.1") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)))) (h "0m7b3a3llr9y3qg3c4j7sjl4jcfdinimrczc3inqkzj5dy017jkp") (r "1.65.0")))

