(define-module (crates-io al le allegro_ttf-sys) #:use-module (crates-io))

(define-public crate-allegro_ttf-sys-0.0.2 (c (n "allegro_ttf-sys") (v "0.0.2") (d (list (d (n "allegro_font-sys") (r "= 0.0.2") (d #t) (k 0)))) (h "0xhx0h5dwhbzp98w1yq2jmi8bzlxa97h3jskdg6b7jm5kgkgj4y0") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_ttf-sys-0.0.3 (c (n "allegro_ttf-sys") (v "0.0.3") (d (list (d (n "allegro_font-sys") (r "= 0.0.3") (d #t) (k 0)))) (h "0bzs743h5lnxnrm2bcvymaf8piw70mdlsffz4zdad1dhaivxxrr2") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_ttf-sys-0.0.4 (c (n "allegro_ttf-sys") (v "0.0.4") (d (list (d (n "allegro_font-sys") (r "= 0.0.4") (d #t) (k 0)))) (h "1yhd521fc4gahphprx3ja9vlambimfcnv5axaj8mvw9x39jfap97") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_ttf-sys-0.0.5 (c (n "allegro_ttf-sys") (v "0.0.5") (d (list (d (n "allegro_font-sys") (r "= 0.0.5") (d #t) (k 0)))) (h "03g6sjh24wwmv3mzxydq1h4h66hnx5cm16yiywrzzrrc5ipn23gr") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_ttf-sys-0.0.6 (c (n "allegro_ttf-sys") (v "0.0.6") (d (list (d (n "allegro_font-sys") (r "= 0.0.6") (d #t) (k 0)))) (h "0ij003b0nd3qc4yinlrb7zbgn79m064j5659k5j107mg6cnpl0an") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_ttf-sys-0.0.7 (c (n "allegro_ttf-sys") (v "0.0.7") (d (list (d (n "allegro_font-sys") (r "= 0.0.7") (d #t) (k 0)))) (h "0cyn5yaqijhhbp0yfpbg3dxkrp8qn6i5j45p3izlfgmmd39g3p85") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_ttf-sys-0.0.8 (c (n "allegro_ttf-sys") (v "0.0.8") (d (list (d (n "allegro_font-sys") (r "= 0.0.8") (d #t) (k 0)))) (h "0rqx0mfai2p4dk8v9rg8xvy60sp78nnda9wbvrwmzaqdnlr503vh") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_ttf-sys-0.0.10 (c (n "allegro_ttf-sys") (v "0.0.10") (d (list (d (n "allegro_font-sys") (r "= 0.0.10") (d #t) (k 0)))) (h "06ym6ki2kqcg984rz19vm8ivq095hb2a6q8y0w3pi1kj02n5g96m") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_ttf-sys-0.0.11 (c (n "allegro_ttf-sys") (v "0.0.11") (d (list (d (n "allegro_font-sys") (r "= 0.0.11") (d #t) (k 0)))) (h "0mvf4gbqg8l01yq4m8i4ic50cncjz20j72kfz3lpkwzk2186skgy") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_ttf-sys-0.0.12 (c (n "allegro_ttf-sys") (v "0.0.12") (d (list (d (n "allegro_font-sys") (r "= 0.0.12") (d #t) (k 0)))) (h "014wp34yx8v3z93y14nbcfk6swd3398mviwwaal8srkbdghqjs8k") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_ttf-sys-0.0.13 (c (n "allegro_ttf-sys") (v "0.0.13") (d (list (d (n "allegro_font-sys") (r "= 0.0.13") (d #t) (k 0)))) (h "18vxnrj7dpcpgwrhbh5vl0wfb407ydn0yp4lzbajksgsqmd596fs") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_ttf-sys-0.0.14 (c (n "allegro_ttf-sys") (v "0.0.14") (d (list (d (n "allegro_font-sys") (r "= 0.0.14") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.14") (d #t) (k 0)))) (h "15c9c1l1cdbvi6mbv92vk0dbnv95pim1rs8j5iwk24jpg7bpqw8b") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_ttf-sys-0.0.15 (c (n "allegro_ttf-sys") (v "0.0.15") (d (list (d (n "allegro_font-sys") (r "= 0.0.15") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.15") (d #t) (k 0)))) (h "0nbgad4407x60nnkkscf2l4hy8rf603abb7m1hpkk0v8cczzjzrv") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_ttf-sys-0.0.16 (c (n "allegro_ttf-sys") (v "0.0.16") (d (list (d (n "allegro_font-sys") (r "= 0.0.16") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.16") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "1fxmnq00vi08m9x2y091sna9mgl6wv8y407p0rdihy8pjqdrgz6p") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_ttf-sys-0.0.17 (c (n "allegro_ttf-sys") (v "0.0.17") (d (list (d (n "allegro_font-sys") (r "= 0.0.17") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.17") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "1az5p30kvj0rxsibn14l44y00dg5wvw25fzcx7hp2731j9zdbza8") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_ttf-sys-0.0.18 (c (n "allegro_ttf-sys") (v "0.0.18") (d (list (d (n "allegro_font-sys") (r "= 0.0.18") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.18") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "19p6lk6w51pmf29slqdj1j4g63p40yh66r5hy7ln8rr5zwc5yma4") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_ttf-sys-0.0.20 (c (n "allegro_ttf-sys") (v "0.0.20") (d (list (d (n "allegro_font-sys") (r "= 0.0.20") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.20") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "0m532wfv0wm6yzwvs1rs7w6zy4mhxa0mmyxqhcxaac594pk0p3nx") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_ttf-sys-0.0.21 (c (n "allegro_ttf-sys") (v "0.0.21") (d (list (d (n "allegro_font-sys") (r "= 0.0.21") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.21") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "009787qzzrf00f3w35v7kpq1x73n4hr4j3fc307fzpjhf5232hwf") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_ttf-sys-0.0.22 (c (n "allegro_ttf-sys") (v "0.0.22") (d (list (d (n "allegro_font-sys") (r "= 0.0.22") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.22") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "1v8whrpf7fijq1dwr4vr169a7imvhv1lkcwmc90v6z3gdwazp2h8") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_ttf-sys-0.0.23 (c (n "allegro_ttf-sys") (v "0.0.23") (d (list (d (n "allegro_font-sys") (r "= 0.0.23") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.23") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "0z4mkbh6xdv7klr93c8k6qd1ma393ms20h7axssajyg9msc5dfad") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_ttf-sys-0.0.24 (c (n "allegro_ttf-sys") (v "0.0.24") (d (list (d (n "allegro_font-sys") (r "= 0.0.24") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.24") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "1h45a36fryc5biff99q18nyhsrb6zz1w6gx00bjbc0s2jlkfigy4") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_ttf-sys-0.0.25 (c (n "allegro_ttf-sys") (v "0.0.25") (d (list (d (n "allegro_font-sys") (r "= 0.0.25") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.25") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "1ghdixj4ap0y3kqh7k5gkmgyq9grmr5fgdy7pjgc4h4f6wima7py") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_ttf-sys-0.0.26 (c (n "allegro_ttf-sys") (v "0.0.26") (d (list (d (n "allegro_font-sys") (r "= 0.0.26") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.26") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "0px88i0fdpvf2x8aig0w8bz0w2y9brxn9m4vfgxv5jdvrrss5n5q") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_ttf-sys-0.0.27 (c (n "allegro_ttf-sys") (v "0.0.27") (d (list (d (n "allegro_font-sys") (r "= 0.0.27") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.27") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "07ql77s8qmffsj642dvckspp4r8hwn4flsc53ixxd3zqhq62ndq7") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_ttf-sys-0.0.28 (c (n "allegro_ttf-sys") (v "0.0.28") (d (list (d (n "allegro_font-sys") (r "= 0.0.28") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.28") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "06vy3d1zsrm6m38r6s1ivpwrqgc64liiwabz3ymmqb7b0k721glj") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_ttf-sys-0.0.29 (c (n "allegro_ttf-sys") (v "0.0.29") (d (list (d (n "allegro_font-sys") (r "= 0.0.29") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.29") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "1fkadl3gkq3i7im349p2pkmz9di4cyf92gi0v8dwwyhp1r8iqkvz") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_ttf-sys-0.0.30 (c (n "allegro_ttf-sys") (v "0.0.30") (d (list (d (n "allegro_font-sys") (r "= 0.0.30") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.30") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "050qzwhj609793cfmhzdwma08ij843v3jw172sziyqk9k07kx3n7") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_ttf-sys-0.0.31 (c (n "allegro_ttf-sys") (v "0.0.31") (d (list (d (n "allegro_font-sys") (r "= 0.0.31") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.31") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "0piq6ri71xqsayrk33kn8z1b2kmwy6gl6fh3a4w5kdvrr0iql7dy") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_ttf-sys-0.0.32 (c (n "allegro_ttf-sys") (v "0.0.32") (d (list (d (n "allegro_font-sys") (r "= 0.0.32") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.32") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "1x0hb1dcbjjym1jiwvddbzp41slxm5548i7vk061l4jy4lg7n9nk") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_ttf-sys-0.0.33 (c (n "allegro_ttf-sys") (v "0.0.33") (d (list (d (n "allegro_font-sys") (r "= 0.0.33") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.33") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "08v3spj5jr8wnc1gpyany6jp62iayarp3dw07bvvfm0sd6spzqrr") (f (quote (("link_static") ("link_none") ("link_debug")))) (l "allegro_ttf")))

(define-public crate-allegro_ttf-sys-0.0.34 (c (n "allegro_ttf-sys") (v "0.0.34") (d (list (d (n "allegro-sys") (r "= 0.0.34") (d #t) (k 0)) (d (n "allegro_font-sys") (r "= 0.0.34") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.34") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "0np8bkg29mljq4c2lcagqwhznk4zgxmkxf1ssfmnbvxh4qlf0ij6") (f (quote (("link_static") ("link_none") ("link_debug")))) (l "allegro_ttf")))

(define-public crate-allegro_ttf-sys-0.0.35 (c (n "allegro_ttf-sys") (v "0.0.35") (d (list (d (n "allegro-sys") (r "= 0.0.35") (d #t) (k 0)) (d (n "allegro_font-sys") (r "= 0.0.35") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.35") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "14kbjdil30gplgzdnb6v3cq2v8vrrq1yz2wl50kdjfkfxd9kp51w") (f (quote (("link_static") ("link_none") ("link_debug")))) (l "allegro_ttf")))

(define-public crate-allegro_ttf-sys-0.0.36 (c (n "allegro_ttf-sys") (v "0.0.36") (d (list (d (n "allegro-sys") (r "= 0.0.36") (d #t) (k 0)) (d (n "allegro_font-sys") (r "= 0.0.36") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.36") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "08ipq6xnhsbwfivr6snlifbqr7gh3701nqq3qk0bng43sm4dj1s3") (f (quote (("link_static") ("link_none") ("link_debug")))) (l "allegro_ttf")))

(define-public crate-allegro_ttf-sys-0.0.37 (c (n "allegro_ttf-sys") (v "0.0.37") (d (list (d (n "allegro-sys") (r "= 0.0.37") (d #t) (k 0)) (d (n "allegro_font-sys") (r "= 0.0.37") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.37") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0ynlmma7rxbmcvwli5nlqyhyblq1iapclbmira8qfl2a9n3904k4") (f (quote (("link_static") ("link_none") ("link_debug")))) (l "allegro_ttf")))

(define-public crate-allegro_ttf-sys-0.0.38 (c (n "allegro_ttf-sys") (v "0.0.38") (d (list (d (n "allegro-sys") (r "= 0.0.38") (d #t) (k 0)) (d (n "allegro_font-sys") (r "= 0.0.38") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.38") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1mbw489fm57g4cp9ng178gghrljsncg11s8h00wfsm9nfinz9wh1") (f (quote (("link_static") ("link_none") ("link_debug")))) (l "allegro_ttf")))

(define-public crate-allegro_ttf-sys-0.0.39 (c (n "allegro_ttf-sys") (v "0.0.39") (d (list (d (n "allegro-sys") (r "= 0.0.39") (d #t) (k 0)) (d (n "allegro_font-sys") (r "= 0.0.39") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.39") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "134mvws4rgfsp572mkm18v3fhwp1jb76h9rwnrgs4hmxvdg29h19") (f (quote (("link_static") ("link_none") ("link_debug")))) (l "allegro_ttf")))

(define-public crate-allegro_ttf-sys-0.0.40 (c (n "allegro_ttf-sys") (v "0.0.40") (d (list (d (n "allegro-sys") (r "= 0.0.40") (d #t) (k 0)) (d (n "allegro_font-sys") (r "= 0.0.40") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.40") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1hr9ja72sp3zm5x8q6cx1rafbhfxx882g91zn5by689675dc84g6") (f (quote (("link_static") ("link_none") ("link_debug")))) (l "allegro_ttf")))

(define-public crate-allegro_ttf-sys-0.0.41 (c (n "allegro_ttf-sys") (v "0.0.41") (d (list (d (n "allegro-sys") (r "=0.0.41") (d #t) (k 0)) (d (n "allegro_font-sys") (r "=0.0.41") (d #t) (k 0)) (d (n "allegro_util") (r "=0.0.41") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0awii26wq69f4v7balkdbjak8r8xmm0ydyw0r6rb0ij94nyh9027") (f (quote (("link_static") ("link_none") ("link_debug")))) (l "allegro_ttf")))

(define-public crate-allegro_ttf-sys-0.0.42 (c (n "allegro_ttf-sys") (v "0.0.42") (d (list (d (n "allegro-sys") (r "=0.0.42") (d #t) (k 0)) (d (n "allegro_font-sys") (r "=0.0.42") (d #t) (k 0)) (d (n "allegro_util") (r "=0.0.42") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1paxvk9qj898shjgbf2qr3f4la10i4vvbyqnzq8d59yr2z72wkwn") (f (quote (("link_static") ("link_none") ("link_debug")))) (l "allegro_ttf")))

(define-public crate-allegro_ttf-sys-0.0.43 (c (n "allegro_ttf-sys") (v "0.0.43") (d (list (d (n "allegro-sys") (r "=0.0.43") (d #t) (k 0)) (d (n "allegro_font-sys") (r "=0.0.43") (d #t) (k 0)) (d (n "allegro_util") (r "=0.0.43") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "16f9rzbsk3g966y8h961a47jhdw9jwz187a5jcrc5wcrcq192gbw") (f (quote (("link_static") ("link_none") ("link_debug")))) (l "allegro_ttf")))

(define-public crate-allegro_ttf-sys-0.0.44 (c (n "allegro_ttf-sys") (v "0.0.44") (d (list (d (n "allegro-sys") (r "=0.0.44") (d #t) (k 0)) (d (n "allegro_font-sys") (r "=0.0.44") (d #t) (k 0)) (d (n "allegro_util") (r "=0.0.44") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0dl5gb8yb2xy7n6vkccwymklcsbd0raz2bkhdg1aw7iv96ixdy4a") (f (quote (("link_static") ("link_none") ("link_debug")))) (l "allegro_ttf")))

(define-public crate-allegro_ttf-sys-0.0.45 (c (n "allegro_ttf-sys") (v "0.0.45") (d (list (d (n "allegro-sys") (r "=0.0.45") (d #t) (k 0)) (d (n "allegro_font-sys") (r "=0.0.45") (d #t) (k 0)) (d (n "allegro_util") (r "=0.0.45") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "013qvq0wdc280ilh24gi7nalqbhj30hbn8fvjfj3w8c8bkjxj79m") (f (quote (("link_static") ("link_none") ("link_debug")))) (l "allegro_ttf")))

(define-public crate-allegro_ttf-sys-0.0.46 (c (n "allegro_ttf-sys") (v "0.0.46") (d (list (d (n "allegro-sys") (r "=0.0.46") (d #t) (k 0)) (d (n "allegro_font-sys") (r "=0.0.46") (d #t) (k 0)) (d (n "allegro_util") (r "=0.0.46") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1ainlypr337bxfn1n46mpvr1v2wwnif0km7p0vhhk52k7gk0awgw") (f (quote (("link_static") ("link_none") ("link_debug")))) (l "allegro_ttf")))

