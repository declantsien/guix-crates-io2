(define-module (crates-io al le allegro) #:use-module (crates-io))

(define-public crate-allegro-0.0.0 (c (n "allegro") (v "0.0.0") (h "1vz17b67payn2bxbrb1ns3cd3lwi94z89l0j6likr1k9vpqfkd87")))

(define-public crate-allegro-0.0.1 (c (n "allegro") (v "0.0.1") (h "0xcd46ma0k6gd2py5lynvmh44im1dq4n9y53cdlhvg0cvvb4iqc2")))

(define-public crate-allegro-0.0.2 (c (n "allegro") (v "0.0.2") (d (list (d (n "allegro-sys") (r "= 0.0.2") (d #t) (k 0)))) (h "0p9nfmx28977gcdfn49rmb7lbn4ajgd86gixck1i7x59p4qp9kk7") (f (quote (("link_static" "allegro-sys/link_static") ("link_none" "allegro-sys/link_none") ("link_monolith" "allegro-sys/link_monolith") ("link_debug" "allegro-sys/link_debug"))))))

(define-public crate-allegro-0.0.3 (c (n "allegro") (v "0.0.3") (d (list (d (n "allegro-sys") (r "= 0.0.3") (d #t) (k 0)))) (h "0xlsn083ixx7p336mkn5qa5qm44bfizy3l80knl6llj8fkab5695") (f (quote (("link_static" "allegro-sys/link_static") ("link_none" "allegro-sys/link_none") ("link_monolith" "allegro-sys/link_monolith") ("link_debug" "allegro-sys/link_debug"))))))

(define-public crate-allegro-0.0.4 (c (n "allegro") (v "0.0.4") (d (list (d (n "allegro-sys") (r "= 0.0.4") (d #t) (k 0)))) (h "1f4cmzd6b33y7j57vqqj698s0ffsn5iywgrf0ighah8y45zz6n3b") (f (quote (("link_static" "allegro-sys/link_static") ("link_none" "allegro-sys/link_none") ("link_monolith" "allegro-sys/link_monolith") ("link_debug" "allegro-sys/link_debug"))))))

(define-public crate-allegro-0.0.5 (c (n "allegro") (v "0.0.5") (d (list (d (n "allegro-sys") (r "= 0.0.5") (d #t) (k 0)))) (h "0xjmihy4dblcjhg2ffi4xb9zqayfcsk4hk6af1xqshjx22x5bgjk") (f (quote (("link_static" "allegro-sys/link_static") ("link_none" "allegro-sys/link_none") ("link_monolith" "allegro-sys/link_monolith") ("link_debug" "allegro-sys/link_debug"))))))

(define-public crate-allegro-0.0.6 (c (n "allegro") (v "0.0.6") (d (list (d (n "allegro-sys") (r "= 0.0.6") (d #t) (k 0)))) (h "1ylsr21vjf9fnsbkfkxgpx7kkhmcd8x0i11p4plxrcg9i2qdgmj4") (f (quote (("link_static" "allegro-sys/link_static") ("link_none" "allegro-sys/link_none") ("link_monolith" "allegro-sys/link_monolith") ("link_debug" "allegro-sys/link_debug"))))))

(define-public crate-allegro-0.0.7 (c (n "allegro") (v "0.0.7") (d (list (d (n "allegro-sys") (r "= 0.0.7") (d #t) (k 0)))) (h "0bg8mkrw9xqavrspldq484jsvkjwzs85kgzph5zj5apskac270s3") (f (quote (("link_static" "allegro-sys/link_static") ("link_none" "allegro-sys/link_none") ("link_monolith" "allegro-sys/link_monolith") ("link_debug" "allegro-sys/link_debug"))))))

(define-public crate-allegro-0.0.8 (c (n "allegro") (v "0.0.8") (d (list (d (n "allegro-sys") (r "= 0.0.8") (d #t) (k 0)))) (h "1mvacr37fqbirrji9sryc83v0s2sg31xvb4cgpdsmxaf1d0v63j0") (f (quote (("link_static" "allegro-sys/link_static") ("link_none" "allegro-sys/link_none") ("link_monolith" "allegro-sys/link_monolith") ("link_debug" "allegro-sys/link_debug"))))))

(define-public crate-allegro-0.0.10 (c (n "allegro") (v "0.0.10") (d (list (d (n "allegro-sys") (r "= 0.0.10") (d #t) (k 0)))) (h "18dzbba8n28cai8ph16mxnzzqi1fska329xz72s3ck2f10xm74xg") (f (quote (("link_static" "allegro-sys/link_static") ("link_none" "allegro-sys/link_none") ("link_monolith" "allegro-sys/link_monolith") ("link_debug" "allegro-sys/link_debug"))))))

(define-public crate-allegro-0.0.11 (c (n "allegro") (v "0.0.11") (d (list (d (n "allegro-sys") (r "= 0.0.11") (d #t) (k 0)))) (h "10vx1sy22sl5qzcl3r3vymdl36fp9qpv393zszxfqihxkczbp56p") (f (quote (("link_static" "allegro-sys/link_static") ("link_none" "allegro-sys/link_none") ("link_monolith" "allegro-sys/link_monolith") ("link_debug" "allegro-sys/link_debug"))))))

(define-public crate-allegro-0.0.12 (c (n "allegro") (v "0.0.12") (d (list (d (n "allegro-sys") (r "= 0.0.12") (d #t) (k 0)))) (h "0jndiwyvby7hnhnzqm66dy89h6671cr9vgbv0npapd1xy32inhjq") (f (quote (("link_static" "allegro-sys/link_static") ("link_none" "allegro-sys/link_none") ("link_monolith" "allegro-sys/link_monolith") ("link_debug" "allegro-sys/link_debug"))))))

(define-public crate-allegro-0.0.13 (c (n "allegro") (v "0.0.13") (d (list (d (n "allegro-sys") (r "= 0.0.13") (d #t) (k 0)))) (h "07gp2p3vgry6fjp7k5d50lgzagzl4js8w6479lnra6a9zcgmbknz") (f (quote (("link_static" "allegro-sys/link_static") ("link_none" "allegro-sys/link_none") ("link_monolith" "allegro-sys/link_monolith") ("link_debug" "allegro-sys/link_debug"))))))

(define-public crate-allegro-0.0.14 (c (n "allegro") (v "0.0.14") (d (list (d (n "allegro-sys") (r "= 0.0.14") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.14") (d #t) (k 0)))) (h "0zcxks8f8vrar4wdd808f1jv9cypns9bxjk6r683cij5i9m7nkbv") (f (quote (("link_static" "allegro-sys/link_static") ("link_none" "allegro-sys/link_none") ("link_debug" "allegro-sys/link_debug"))))))

(define-public crate-allegro-0.0.15 (c (n "allegro") (v "0.0.15") (d (list (d (n "allegro-sys") (r "= 0.0.15") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.15") (d #t) (k 0)))) (h "1p0lsr4ay1hkpwcxq8w2z0dg198v88av1pi64yxximmpmryvy49v") (f (quote (("link_static" "allegro-sys/link_static") ("link_none" "allegro-sys/link_none") ("link_debug" "allegro-sys/link_debug"))))))

(define-public crate-allegro-0.0.16 (c (n "allegro") (v "0.0.16") (d (list (d (n "allegro-sys") (r "= 0.0.16") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.16") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "030i8bkx6mhgikmhd9702362nbbgxcqhdxn71q6s9qlyilkh1xjb") (f (quote (("link_static" "allegro-sys/link_static") ("link_none" "allegro-sys/link_none") ("link_debug" "allegro-sys/link_debug"))))))

(define-public crate-allegro-0.0.17 (c (n "allegro") (v "0.0.17") (d (list (d (n "allegro-sys") (r "= 0.0.17") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.17") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "1sh17m19nrahc8q54mi9ppjksqkky4gw5azkf3ziz6yp7hjhcnlh") (f (quote (("link_static" "allegro-sys/link_static") ("link_none" "allegro-sys/link_none") ("link_debug" "allegro-sys/link_debug"))))))

(define-public crate-allegro-0.0.18 (c (n "allegro") (v "0.0.18") (d (list (d (n "allegro-sys") (r "= 0.0.18") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.18") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "1r6q8ff15pl07yrwj16b3wl56yic8dw6mxxwd6pszcd10fa2f3ia") (f (quote (("link_static" "allegro-sys/link_static") ("link_none" "allegro-sys/link_none") ("link_debug" "allegro-sys/link_debug"))))))

(define-public crate-allegro-0.0.20 (c (n "allegro") (v "0.0.20") (d (list (d (n "allegro-sys") (r "= 0.0.20") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.20") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "04sfgnvdv1gc73frna8dxsks8z7vj1j2cdmifv84msgip6dri0nr") (f (quote (("link_static" "allegro-sys/link_static") ("link_none" "allegro-sys/link_none") ("link_debug" "allegro-sys/link_debug"))))))

(define-public crate-allegro-0.0.21 (c (n "allegro") (v "0.0.21") (d (list (d (n "allegro-sys") (r "= 0.0.21") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.21") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "1jdfvkh11qn8058fqyj9w6rksz8n4sw119vc5nzy1aximwvj27w2") (f (quote (("link_static" "allegro-sys/link_static") ("link_none" "allegro-sys/link_none") ("link_debug" "allegro-sys/link_debug"))))))

(define-public crate-allegro-0.0.22 (c (n "allegro") (v "0.0.22") (d (list (d (n "allegro-sys") (r "= 0.0.22") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.22") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "1jj2q64pn54xklp1wjnb85vaq08cf0ppg96hdjb0ivrfgwjw3ry8") (f (quote (("link_static" "allegro-sys/link_static") ("link_none" "allegro-sys/link_none") ("link_debug" "allegro-sys/link_debug"))))))

(define-public crate-allegro-0.0.23 (c (n "allegro") (v "0.0.23") (d (list (d (n "allegro-sys") (r "= 0.0.23") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.23") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "0724ls01xcq6prj7yn2h0yqsh01h7blzxwsqn9wq8m7lqfmbjpfm") (f (quote (("link_static" "allegro-sys/link_static") ("link_none" "allegro-sys/link_none") ("link_debug" "allegro-sys/link_debug"))))))

(define-public crate-allegro-0.0.24 (c (n "allegro") (v "0.0.24") (d (list (d (n "allegro-sys") (r "= 0.0.24") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.24") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "0by3awg6vxblqsvz89jfzmj87657wxc83h1kciifqsrfhc2cg60z") (f (quote (("link_static" "allegro-sys/link_static") ("link_none" "allegro-sys/link_none") ("link_debug" "allegro-sys/link_debug"))))))

(define-public crate-allegro-0.0.25 (c (n "allegro") (v "0.0.25") (d (list (d (n "allegro-sys") (r "= 0.0.25") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.25") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "0rwgxn76pni969bpw8299xszffpv2xxzp7q9dy1hj4jzgkp48hnc") (f (quote (("link_static" "allegro-sys/link_static") ("link_none" "allegro-sys/link_none") ("link_debug" "allegro-sys/link_debug"))))))

(define-public crate-allegro-0.0.26 (c (n "allegro") (v "0.0.26") (d (list (d (n "allegro-sys") (r "= 0.0.26") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.26") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "1scshj60s1ina0cl2axwvplvhr0dw8hwmjcqzsf6j407152a5qny") (f (quote (("link_static" "allegro-sys/link_static") ("link_none" "allegro-sys/link_none") ("link_debug" "allegro-sys/link_debug"))))))

(define-public crate-allegro-0.0.27 (c (n "allegro") (v "0.0.27") (d (list (d (n "allegro-sys") (r "= 0.0.27") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.27") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "14l1fb1l4j0x2q0ikfhs277sv7klnppb2zsmr9m58v3lf5bxbh64") (f (quote (("link_static" "allegro-sys/link_static") ("link_none" "allegro-sys/link_none") ("link_debug" "allegro-sys/link_debug"))))))

(define-public crate-allegro-0.0.28 (c (n "allegro") (v "0.0.28") (d (list (d (n "allegro-sys") (r "= 0.0.28") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.28") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "1a00hsa6makmcshbk6yslh4rn6xb1lbxnnyg99gsrb2ji0i4gbv9") (f (quote (("link_static" "allegro-sys/link_static") ("link_none" "allegro-sys/link_none") ("link_debug" "allegro-sys/link_debug"))))))

(define-public crate-allegro-0.0.29 (c (n "allegro") (v "0.0.29") (d (list (d (n "allegro-sys") (r "= 0.0.29") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.29") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "058nqqnilwid5bz13hr4sdvyyp3dqx5lwdfay8dhq2gfs5m0k0mc") (f (quote (("link_static" "allegro-sys/link_static") ("link_none" "allegro-sys/link_none") ("link_debug" "allegro-sys/link_debug"))))))

(define-public crate-allegro-0.0.30 (c (n "allegro") (v "0.0.30") (d (list (d (n "allegro-sys") (r "= 0.0.30") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.30") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "046qfnpkhvdfvr8zvpk1ji8rq01xxxyhbsndf91k0swmwhx8gnc0") (f (quote (("link_static" "allegro-sys/link_static") ("link_none" "allegro-sys/link_none") ("link_debug" "allegro-sys/link_debug"))))))

(define-public crate-allegro-0.0.31 (c (n "allegro") (v "0.0.31") (d (list (d (n "allegro-sys") (r "= 0.0.31") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.31") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "1sd4fkdsmycxayhzkidjp2nzyx58lqrkfazxyp3v9l2flh0p4q90") (f (quote (("link_static" "allegro-sys/link_static") ("link_none" "allegro-sys/link_none") ("link_debug" "allegro-sys/link_debug"))))))

(define-public crate-allegro-0.0.32 (c (n "allegro") (v "0.0.32") (d (list (d (n "allegro-sys") (r "= 0.0.32") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.32") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "1xdrmvmv7h85inn7ffshmnq2qj8s0xli667fk181238mciamc9xa") (f (quote (("link_static" "allegro-sys/link_static") ("link_none" "allegro-sys/link_none") ("link_debug" "allegro-sys/link_debug"))))))

(define-public crate-allegro-0.0.33 (c (n "allegro") (v "0.0.33") (d (list (d (n "allegro-sys") (r "= 0.0.33") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.33") (d #t) (k 0)) (d (n "lazy_static") (r "= 1.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "1b1cmwm0pbrfsay92kkv4681ihjaq8prn0czidk20g1yl7c2swvc") (f (quote (("link_static" "allegro-sys/link_static") ("link_none" "allegro-sys/link_none") ("link_debug" "allegro-sys/link_debug"))))))

(define-public crate-allegro-0.0.34 (c (n "allegro") (v "0.0.34") (d (list (d (n "allegro-sys") (r "= 0.0.34") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.34") (d #t) (k 0)) (d (n "lazy_static") (r "= 1.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "0ipi89klv00m2js9lzjgfi7pa5sf7rzsac8r7y4wr3j2a1fk3wr0") (f (quote (("link_static" "allegro-sys/link_static") ("link_none" "allegro-sys/link_none") ("link_debug" "allegro-sys/link_debug"))))))

(define-public crate-allegro-0.0.35 (c (n "allegro") (v "0.0.35") (d (list (d (n "allegro-sys") (r "= 0.0.35") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.35") (d #t) (k 0)) (d (n "lazy_static") (r "= 1.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "1vp560m8vhhm3srlhwm8nr9mhf2iccsbmrjvfrxzw88wgc05lq95") (f (quote (("link_static" "allegro-sys/link_static") ("link_none" "allegro-sys/link_none") ("link_debug" "allegro-sys/link_debug"))))))

(define-public crate-allegro-0.0.36 (c (n "allegro") (v "0.0.36") (d (list (d (n "allegro-sys") (r "= 0.0.36") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.36") (d #t) (k 0)) (d (n "lazy_static") (r "= 1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "1idyjpbfn0qqy8bxh94r3d1rd687k1f9sv52l6ypcvbh4shvvv8r") (f (quote (("link_static" "allegro-sys/link_static") ("link_none" "allegro-sys/link_none") ("link_debug" "allegro-sys/link_debug"))))))

(define-public crate-allegro-0.0.37 (c (n "allegro") (v "0.0.37") (d (list (d (n "allegro-sys") (r "= 0.0.37") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.37") (d #t) (k 0)) (d (n "lazy_static") (r "= 1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "104aqlaarfbl1ws4x1q9xqqyjawqyrgf2znx316plhck78h19x33") (f (quote (("link_static" "allegro-sys/link_static") ("link_none" "allegro-sys/link_none") ("link_debug" "allegro-sys/link_debug"))))))

(define-public crate-allegro-0.0.38 (c (n "allegro") (v "0.0.38") (d (list (d (n "allegro-sys") (r "= 0.0.38") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.38") (d #t) (k 0)) (d (n "lazy_static") (r "= 1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1gcprqd84vgv3diqimmxz27vhsvzd1qlgkvm6b1pwdgjl65nzza1") (f (quote (("link_static" "allegro-sys/link_static") ("link_none" "allegro-sys/link_none") ("link_debug" "allegro-sys/link_debug"))))))

(define-public crate-allegro-0.0.39 (c (n "allegro") (v "0.0.39") (d (list (d (n "allegro-sys") (r "= 0.0.39") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.39") (d #t) (k 0)) (d (n "lazy_static") (r "= 1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1x9g5xvzlg8qmkhjs7dc0wsvc32khvwz6a6hrsk8shn5lln740ib") (f (quote (("link_static" "allegro-sys/link_static") ("link_none" "allegro-sys/link_none") ("link_debug" "allegro-sys/link_debug"))))))

(define-public crate-allegro-0.0.40 (c (n "allegro") (v "0.0.40") (d (list (d (n "allegro-sys") (r "= 0.0.40") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.40") (d #t) (k 0)) (d (n "lazy_static") (r "= 1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0yr0zi4dqqvl19a5pkj04acncd8y7y198n9yjrqrq15zcwmp52gm") (f (quote (("link_static" "allegro-sys/link_static") ("link_none" "allegro-sys/link_none") ("link_debug" "allegro-sys/link_debug"))))))

(define-public crate-allegro-0.0.41 (c (n "allegro") (v "0.0.41") (d (list (d (n "allegro-sys") (r "=0.0.41") (d #t) (k 0)) (d (n "allegro_util") (r "=0.0.41") (d #t) (k 0)) (d (n "lazy_static") (r "=1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "073r3pk6q7vg3fd1qsg7af4k1sxn7qjaa31ibzk181vb3ywr0s2d") (f (quote (("link_static" "allegro-sys/link_static") ("link_none" "allegro-sys/link_none") ("link_debug" "allegro-sys/link_debug"))))))

(define-public crate-allegro-0.0.42 (c (n "allegro") (v "0.0.42") (d (list (d (n "allegro-sys") (r "=0.0.42") (d #t) (k 0)) (d (n "allegro_util") (r "=0.0.42") (d #t) (k 0)) (d (n "lazy_static") (r "=1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "05jqb1liaa1vayydzbf2j8pvvr60bjv3vmbm40fysyixdbc09h9m") (f (quote (("link_static" "allegro-sys/link_static") ("link_none" "allegro-sys/link_none") ("link_debug" "allegro-sys/link_debug"))))))

(define-public crate-allegro-0.0.43 (c (n "allegro") (v "0.0.43") (d (list (d (n "allegro-sys") (r "=0.0.43") (d #t) (k 0)) (d (n "allegro_util") (r "=0.0.43") (d #t) (k 0)) (d (n "lazy_static") (r "=1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0sdj6w5z6m1qm1xmmqf0s6ia8sy2salwk3lksfx074374ld80yq8") (f (quote (("link_static" "allegro-sys/link_static") ("link_none" "allegro-sys/link_none") ("link_debug" "allegro-sys/link_debug"))))))

(define-public crate-allegro-0.0.44 (c (n "allegro") (v "0.0.44") (d (list (d (n "allegro-sys") (r "=0.0.44") (d #t) (k 0)) (d (n "allegro_util") (r "=0.0.44") (d #t) (k 0)) (d (n "lazy_static") (r "=1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0npz568plc7mvbsg0pqfwswsnkg456vpfh6ljqx0cb1m3fsdbcaz") (f (quote (("link_static" "allegro-sys/link_static") ("link_none" "allegro-sys/link_none") ("link_debug" "allegro-sys/link_debug"))))))

(define-public crate-allegro-0.0.45 (c (n "allegro") (v "0.0.45") (d (list (d (n "allegro-sys") (r "=0.0.45") (d #t) (k 0)) (d (n "allegro_util") (r "=0.0.45") (d #t) (k 0)) (d (n "lazy_static") (r "=1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1s9a0a7ivg2jrzgpg5y9k6qyr090jbfq93amxnrdnw88qxd518pm") (f (quote (("link_static" "allegro-sys/link_static") ("link_none" "allegro-sys/link_none") ("link_debug" "allegro-sys/link_debug"))))))

(define-public crate-allegro-0.0.46 (c (n "allegro") (v "0.0.46") (d (list (d (n "allegro-sys") (r "=0.0.46") (d #t) (k 0)) (d (n "allegro_util") (r "=0.0.46") (d #t) (k 0)) (d (n "lazy_static") (r "=1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0623alw0qljbhz94279pskdxxw4rvxq4hld5n2hqj7ndp1w1q422") (f (quote (("link_static" "allegro-sys/link_static") ("link_none" "allegro-sys/link_none") ("link_debug" "allegro-sys/link_debug"))))))

