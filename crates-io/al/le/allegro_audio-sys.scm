(define-module (crates-io al le allegro_audio-sys) #:use-module (crates-io))

(define-public crate-allegro_audio-sys-0.0.2 (c (n "allegro_audio-sys") (v "0.0.2") (d (list (d (n "allegro-sys") (r "= 0.0.2") (d #t) (k 0)))) (h "0nr3ady3jbpx5n99xpmy9kcyh9qj1kb9qpzvkj0klwz53bw5i8iw") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_audio-sys-0.0.3 (c (n "allegro_audio-sys") (v "0.0.3") (d (list (d (n "allegro-sys") (r "= 0.0.3") (d #t) (k 0)))) (h "1g16fwziid071mphn34y9pi3vmbxlyifj45sybzv94q037qznn5j") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_audio-sys-0.0.4 (c (n "allegro_audio-sys") (v "0.0.4") (d (list (d (n "allegro-sys") (r "= 0.0.4") (d #t) (k 0)))) (h "0ngk1s5jink2jp6xr9g92ax7qh58acbqhrqimb8xnsb48jw7ccc8") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_audio-sys-0.0.5 (c (n "allegro_audio-sys") (v "0.0.5") (d (list (d (n "allegro-sys") (r "= 0.0.5") (d #t) (k 0)))) (h "1xvrwdrhd7kkpn10lmrndj304mvq2rn65kz2dwmfmm2ks20yv73w") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_audio-sys-0.0.6 (c (n "allegro_audio-sys") (v "0.0.6") (d (list (d (n "allegro-sys") (r "= 0.0.6") (d #t) (k 0)))) (h "0d1gr2k469phrj5rcgi3hbjcp0q5jxqdamgljiw7phn7x1sdxk60") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_audio-sys-0.0.7 (c (n "allegro_audio-sys") (v "0.0.7") (d (list (d (n "allegro-sys") (r "= 0.0.7") (d #t) (k 0)))) (h "0rink01zy9bs6hcvkh3vnz3bhgaz8n4dkyikwdfgp4xz7k94kfl9") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_audio-sys-0.0.8 (c (n "allegro_audio-sys") (v "0.0.8") (d (list (d (n "allegro-sys") (r "= 0.0.8") (d #t) (k 0)))) (h "0xx5cjz88fpjidzb86bmhp4ais3vn06mz3fkhc9acxmab27lrh5x") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_audio-sys-0.0.10 (c (n "allegro_audio-sys") (v "0.0.10") (d (list (d (n "allegro-sys") (r "= 0.0.10") (d #t) (k 0)))) (h "11gn1k3nvhf5x5ym3j6pvb01ww4z258brbq2d0f2siw5swj6sah0") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_audio-sys-0.0.11 (c (n "allegro_audio-sys") (v "0.0.11") (d (list (d (n "allegro-sys") (r "= 0.0.11") (d #t) (k 0)))) (h "07jyakymdsh2snbxknvbazvx74c0lzf1hzhfrahwzxn37s5rl4zx") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_audio-sys-0.0.12 (c (n "allegro_audio-sys") (v "0.0.12") (d (list (d (n "allegro-sys") (r "= 0.0.12") (d #t) (k 0)))) (h "1ynniaqn8c2krqb5v4w3ciklnbvsk4ahf7p6n01l847lwzvd5z32") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_audio-sys-0.0.13 (c (n "allegro_audio-sys") (v "0.0.13") (d (list (d (n "allegro-sys") (r "= 0.0.13") (d #t) (k 0)))) (h "1l2sjv5cpijhhlz59w6xy29mkjgdqqcw4rbyz7a2n99chshnjf37") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_audio-sys-0.0.14 (c (n "allegro_audio-sys") (v "0.0.14") (d (list (d (n "allegro-sys") (r "= 0.0.14") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.14") (d #t) (k 0)))) (h "11v2v3070zylph22dqrxldn1j35rxlk1da3fnsbi3mxkj73fb79s") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_audio-sys-0.0.15 (c (n "allegro_audio-sys") (v "0.0.15") (d (list (d (n "allegro-sys") (r "= 0.0.15") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.15") (d #t) (k 0)))) (h "1swb0ibh4dkyc52cim6gv4xzh7q92crk1s4gqnilam0lh4nli1xn") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_audio-sys-0.0.16 (c (n "allegro_audio-sys") (v "0.0.16") (d (list (d (n "allegro-sys") (r "= 0.0.16") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.16") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "0vada514a2ycq7kfzs7nrmqwmsnk7kcf8r47rxi53376p0phmfik") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_audio-sys-0.0.17 (c (n "allegro_audio-sys") (v "0.0.17") (d (list (d (n "allegro-sys") (r "= 0.0.17") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.17") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "0dzja249mc0x0a503wb0i6al75pvnyaxdbxhdh45k2n4l7gnd892") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_audio-sys-0.0.18 (c (n "allegro_audio-sys") (v "0.0.18") (d (list (d (n "allegro-sys") (r "= 0.0.18") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.18") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "1f0y29gj6kf1j138nyqp40an2bjsvxazy0fyrrrimw7wxvs1lqr7") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_audio-sys-0.0.20 (c (n "allegro_audio-sys") (v "0.0.20") (d (list (d (n "allegro-sys") (r "= 0.0.20") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.20") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "01pr85njrfs78cmg0yb06w5fm67ivx8z0g0rigpqp9jgcvx14xmw") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_audio-sys-0.0.21 (c (n "allegro_audio-sys") (v "0.0.21") (d (list (d (n "allegro-sys") (r "= 0.0.21") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.21") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "1l9xr4idvmj5qfid83k4vm40jplg6amh6bl8paxmlicywvvk0sb2") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_audio-sys-0.0.22 (c (n "allegro_audio-sys") (v "0.0.22") (d (list (d (n "allegro-sys") (r "= 0.0.22") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.22") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "0rcdhqadkvwqa4yvgv1l3430b22512h71wxv3ijws1fdychp2gz2") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_audio-sys-0.0.23 (c (n "allegro_audio-sys") (v "0.0.23") (d (list (d (n "allegro-sys") (r "= 0.0.23") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.23") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "1dm818h7chh6bdcdbq06sapyak8480yjxbzacc468qjjwzdb68x9") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_audio-sys-0.0.24 (c (n "allegro_audio-sys") (v "0.0.24") (d (list (d (n "allegro-sys") (r "= 0.0.24") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.24") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "1mvv9kp7pxxll2f9b7qrq84p5128wlmwyw0d59xvmmq0a8a5w0gn") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_audio-sys-0.0.25 (c (n "allegro_audio-sys") (v "0.0.25") (d (list (d (n "allegro-sys") (r "= 0.0.25") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.25") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "0y2w3a0366my4aaz404mdp407y7q0z2bgzbvl4algkx4nbv9jzj4") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_audio-sys-0.0.26 (c (n "allegro_audio-sys") (v "0.0.26") (d (list (d (n "allegro-sys") (r "= 0.0.26") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.26") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "1b0whmcnkd6lrkilcqn9kx5i1h6w376apdx0bkpbpkgiisw79sf4") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_audio-sys-0.0.27 (c (n "allegro_audio-sys") (v "0.0.27") (d (list (d (n "allegro-sys") (r "= 0.0.27") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.27") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "0v65zbasrd2vsqgpqdk89gc6vim1dhzm05cj5gf6kc72pa2b92y0") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_audio-sys-0.0.28 (c (n "allegro_audio-sys") (v "0.0.28") (d (list (d (n "allegro-sys") (r "= 0.0.28") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.28") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "0c6yfxz1lw4fw1p36vm31xpgdacrj1riwc0p192p484gi4s04kq5") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_audio-sys-0.0.29 (c (n "allegro_audio-sys") (v "0.0.29") (d (list (d (n "allegro-sys") (r "= 0.0.29") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.29") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "06nw4nx12i7bg616bvdi76xqqc7hdnagc23i9ach4m2wf262ibhy") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_audio-sys-0.0.30 (c (n "allegro_audio-sys") (v "0.0.30") (d (list (d (n "allegro-sys") (r "= 0.0.30") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.30") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "0c761zz2rp8q2f0zgab4radxgnxzd2xbpp2qgw1vzsdxwbhg7zg0") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_audio-sys-0.0.31 (c (n "allegro_audio-sys") (v "0.0.31") (d (list (d (n "allegro-sys") (r "= 0.0.31") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.31") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "0fhyl05ljcj2swg4yhc1kyxi1gwjfx8aw9lnnsh7j5cngc28bd5r") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_audio-sys-0.0.32 (c (n "allegro_audio-sys") (v "0.0.32") (d (list (d (n "allegro-sys") (r "= 0.0.32") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.32") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "1jk02ss0mp06vf3sx3bsgx6ksa2mizrvfynknc1i5ky0x40cnbay") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_audio-sys-0.0.33 (c (n "allegro_audio-sys") (v "0.0.33") (d (list (d (n "allegro-sys") (r "= 0.0.33") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.33") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "06ybfmnx8hfhq0w6n2sq0rmpa7qrzfw6i540mwn4w42hdiiw3g7v") (f (quote (("link_static") ("link_none") ("link_debug")))) (l "allegro_audio")))

(define-public crate-allegro_audio-sys-0.0.34 (c (n "allegro_audio-sys") (v "0.0.34") (d (list (d (n "allegro-sys") (r "= 0.0.34") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.34") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "19i0p5fb10lpwdck3f24rrb92w3ihkc25w3bd2y6nxmjw9sczlg0") (f (quote (("link_static") ("link_none") ("link_debug")))) (l "allegro_audio")))

(define-public crate-allegro_audio-sys-0.0.35 (c (n "allegro_audio-sys") (v "0.0.35") (d (list (d (n "allegro-sys") (r "= 0.0.35") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.35") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "0hpshgcs9zp5pqp21qyjgmw4kq76p7jvhwjxfbn1ii4sy0qkmp7i") (f (quote (("link_static") ("link_none") ("link_debug")))) (l "allegro_audio")))

(define-public crate-allegro_audio-sys-0.0.36 (c (n "allegro_audio-sys") (v "0.0.36") (d (list (d (n "allegro-sys") (r "= 0.0.36") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.36") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "06sa6brlyzsrmly1wfi2g7cl6brnbhdil6bdk78qrwrw6npc5ly0") (f (quote (("link_static") ("link_none") ("link_debug")))) (l "allegro_audio")))

(define-public crate-allegro_audio-sys-0.0.37 (c (n "allegro_audio-sys") (v "0.0.37") (d (list (d (n "allegro-sys") (r "= 0.0.37") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.37") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1bl4xj69y12c1hagvk7nhgfk8rqmhrygmh6gwdfdvnnkdqc8lqs6") (f (quote (("link_static") ("link_none") ("link_debug")))) (l "allegro_audio")))

(define-public crate-allegro_audio-sys-0.0.38 (c (n "allegro_audio-sys") (v "0.0.38") (d (list (d (n "allegro-sys") (r "= 0.0.38") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.38") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1d016d80g09zpwniqq8y4h1ja1lx8w4l03n337mnywhy63gsmazq") (f (quote (("link_static") ("link_none") ("link_debug")))) (l "allegro_audio")))

(define-public crate-allegro_audio-sys-0.0.39 (c (n "allegro_audio-sys") (v "0.0.39") (d (list (d (n "allegro-sys") (r "= 0.0.39") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.39") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0q96mw67z9np99xmjzfic2w5ipjhsicbk5afgcs29vakh559l1j2") (f (quote (("link_static") ("link_none") ("link_debug")))) (l "allegro_audio")))

(define-public crate-allegro_audio-sys-0.0.40 (c (n "allegro_audio-sys") (v "0.0.40") (d (list (d (n "allegro-sys") (r "= 0.0.40") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.40") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "112vqdp8dpjcml57kb7bngmxxqcyr19v4hfpr97jh0lv6m6yi2kr") (f (quote (("link_static") ("link_none") ("link_debug")))) (l "allegro_audio")))

(define-public crate-allegro_audio-sys-0.0.41 (c (n "allegro_audio-sys") (v "0.0.41") (d (list (d (n "allegro-sys") (r "=0.0.41") (d #t) (k 0)) (d (n "allegro_util") (r "=0.0.41") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1fimn3781ir1kzc321qcmkm14nhmmawvp9af6dg82a44aqp8mlfh") (f (quote (("link_static") ("link_none") ("link_debug")))) (l "allegro_audio")))

(define-public crate-allegro_audio-sys-0.0.42 (c (n "allegro_audio-sys") (v "0.0.42") (d (list (d (n "allegro-sys") (r "=0.0.42") (d #t) (k 0)) (d (n "allegro_util") (r "=0.0.42") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "17clr23isxka8xnakyvdvy7gcqlb126kql42dfql4qa5davbz2xg") (f (quote (("link_static") ("link_none") ("link_debug")))) (l "allegro_audio")))

(define-public crate-allegro_audio-sys-0.0.43 (c (n "allegro_audio-sys") (v "0.0.43") (d (list (d (n "allegro-sys") (r "=0.0.43") (d #t) (k 0)) (d (n "allegro_util") (r "=0.0.43") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "060w112jrhmjmsz8l7xy7izg7y34zn4fs554l5mzgxlvyrblp7jg") (f (quote (("link_static") ("link_none") ("link_debug")))) (l "allegro_audio")))

(define-public crate-allegro_audio-sys-0.0.44 (c (n "allegro_audio-sys") (v "0.0.44") (d (list (d (n "allegro-sys") (r "=0.0.44") (d #t) (k 0)) (d (n "allegro_util") (r "=0.0.44") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "04crq2igfanyybx1azl6gnjk3vv32dds77jy3yjhzq8cdfkp1nf6") (f (quote (("link_static") ("link_none") ("link_debug")))) (l "allegro_audio")))

(define-public crate-allegro_audio-sys-0.0.45 (c (n "allegro_audio-sys") (v "0.0.45") (d (list (d (n "allegro-sys") (r "=0.0.45") (d #t) (k 0)) (d (n "allegro_util") (r "=0.0.45") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1k1nasmknv37iqfczrfj6zvqq3xcnzdqln3vyshgyq6xjkbi193c") (f (quote (("link_static") ("link_none") ("link_debug")))) (l "allegro_audio")))

(define-public crate-allegro_audio-sys-0.0.46 (c (n "allegro_audio-sys") (v "0.0.46") (d (list (d (n "allegro-sys") (r "=0.0.46") (d #t) (k 0)) (d (n "allegro_util") (r "=0.0.46") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0mzl24a00vk0v0iqlr1bn0m379w4akbh07ck55c286xpr4c6h981") (f (quote (("link_static") ("link_none") ("link_debug")))) (l "allegro_audio")))

