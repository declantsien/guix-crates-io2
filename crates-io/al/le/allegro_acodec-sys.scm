(define-module (crates-io al le allegro_acodec-sys) #:use-module (crates-io))

(define-public crate-allegro_acodec-sys-0.0.2 (c (n "allegro_acodec-sys") (v "0.0.2") (h "00ssfvnijm8pmi0bqw913k9i0q3c6zypxzxh7xfcv3kcwfqz5f89") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_acodec-sys-0.0.3 (c (n "allegro_acodec-sys") (v "0.0.3") (h "0cdwk00yazg7s3a30m3l27vh8jac0wlbw5k9421vpyv3mv47f8p9") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_acodec-sys-0.0.4 (c (n "allegro_acodec-sys") (v "0.0.4") (h "06v802sq4hplp328m3p239cjrc8z21wryrss6hc0dlfxijx8a073") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_acodec-sys-0.0.5 (c (n "allegro_acodec-sys") (v "0.0.5") (h "0hl2ikwgy7lnq71hi5yn2rb2afbwdclixlmi5v5l0swng4wfmbr3") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_acodec-sys-0.0.6 (c (n "allegro_acodec-sys") (v "0.0.6") (h "0l2z6qar2xbal8y23qghh5nr8p4rp4p9bm2q77zxnph1pw528nxp") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_acodec-sys-0.0.7 (c (n "allegro_acodec-sys") (v "0.0.7") (h "1lmxqsgghm0b4kf3g9rh4ywkb1339lvn9vr89wbal1cgfwg6iv6f") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_acodec-sys-0.0.8 (c (n "allegro_acodec-sys") (v "0.0.8") (h "1vxa4jaz4l90sywwl9b1f5354cjqxs2ccsmdm5wi4bsb6nigg97h") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_acodec-sys-0.0.10 (c (n "allegro_acodec-sys") (v "0.0.10") (h "0d3ji1s7073r17b8zj24wg6ai4sy0nrdn4g6rqn1sc4nx968wzcx") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_acodec-sys-0.0.11 (c (n "allegro_acodec-sys") (v "0.0.11") (h "03dkzbmkd1pl6wlpw5yyd9glmiin8jf48prwkp5mim7qikzjb6mk") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_acodec-sys-0.0.12 (c (n "allegro_acodec-sys") (v "0.0.12") (h "0fcmfmbk66dnkjpf2sgbp7z2wydl3cm5pgx4j2g40a5xq96g8f2p") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_acodec-sys-0.0.13 (c (n "allegro_acodec-sys") (v "0.0.13") (h "0gl5yh29ar2icmmk56alq7xzja8qayqzmixx2m67ag2gad8035x9") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_acodec-sys-0.0.14 (c (n "allegro_acodec-sys") (v "0.0.14") (d (list (d (n "allegro_util") (r "= 0.0.14") (d #t) (k 0)))) (h "1mqpr5zkkg5x9nd99w4q308hanjy46i2rbx53hypnhsy7n005dj6") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_acodec-sys-0.0.15 (c (n "allegro_acodec-sys") (v "0.0.15") (d (list (d (n "allegro_util") (r "= 0.0.15") (d #t) (k 0)))) (h "12z1hz1pip3xdy3axhc9lkd4bgy6kp9xa12wspsjv337fz6zkxyr") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_acodec-sys-0.0.16 (c (n "allegro_acodec-sys") (v "0.0.16") (d (list (d (n "allegro_util") (r "= 0.0.16") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "1pg6vzn2cqkax63f8ivpkwhrclssmz0sdd57mh4zmdgia35mig04") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_acodec-sys-0.0.17 (c (n "allegro_acodec-sys") (v "0.0.17") (d (list (d (n "allegro_util") (r "= 0.0.17") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "0vbivyi7927dad6jkk8wx25bcmafff1malc6aflrxxxs6j8hwxn5") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_acodec-sys-0.0.18 (c (n "allegro_acodec-sys") (v "0.0.18") (d (list (d (n "allegro_util") (r "= 0.0.18") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "053di9cryzjx9dd2jj91f1hd40s28b2s9j1qdqrc8hlb0cvpfcwb") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_acodec-sys-0.0.20 (c (n "allegro_acodec-sys") (v "0.0.20") (d (list (d (n "allegro_util") (r "= 0.0.20") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "0cx4vvai2kycfza3y1fyd8823yws4g0lph8yjcajdsszl1390640") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_acodec-sys-0.0.21 (c (n "allegro_acodec-sys") (v "0.0.21") (d (list (d (n "allegro_util") (r "= 0.0.21") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "1lwg8ga0id1j152dzzl3vka214ppk9rs53xn39vxz67zvzfz2mlq") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_acodec-sys-0.0.22 (c (n "allegro_acodec-sys") (v "0.0.22") (d (list (d (n "allegro_util") (r "= 0.0.22") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "18ixiaapd19ajibzwla2lr32gmg2k45b8mica16afvq2p9f81j00") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_acodec-sys-0.0.23 (c (n "allegro_acodec-sys") (v "0.0.23") (d (list (d (n "allegro_util") (r "= 0.0.23") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "190vjw701qmpg37lw22mhqcnsnbyv2c2lfx03h3k6zpxm6zblhfp") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_acodec-sys-0.0.24 (c (n "allegro_acodec-sys") (v "0.0.24") (d (list (d (n "allegro_util") (r "= 0.0.24") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "0m1c3q0ikc0iqmyh0la4vqhyf12y4kz4ayb51mja2zfha1v5jrxj") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_acodec-sys-0.0.25 (c (n "allegro_acodec-sys") (v "0.0.25") (d (list (d (n "allegro_util") (r "= 0.0.25") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "16s7xa88qsg8d2x8l205jhf1443p6zi4dq6pqcq83flx8bvv0by6") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_acodec-sys-0.0.26 (c (n "allegro_acodec-sys") (v "0.0.26") (d (list (d (n "allegro_util") (r "= 0.0.26") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "1l5j1f5cvbjgahqf7lwyw552098q5fhaxxyjmq6fvhs7sx99ksqd") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_acodec-sys-0.0.27 (c (n "allegro_acodec-sys") (v "0.0.27") (d (list (d (n "allegro_util") (r "= 0.0.27") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "0fx97vxiak5dglbmcyap5jmrcmp3g5cc3hl42hivzxainpyw61rf") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_acodec-sys-0.0.28 (c (n "allegro_acodec-sys") (v "0.0.28") (d (list (d (n "allegro_util") (r "= 0.0.28") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "18w9mc7da4fdsjp2cmfmg743j1lbsrp8x0mcwab0vb0n88m6nrvj") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_acodec-sys-0.0.29 (c (n "allegro_acodec-sys") (v "0.0.29") (d (list (d (n "allegro_util") (r "= 0.0.29") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "0j5aahlglsryssw2fn0ci94swrz85gx2filvzb37lg2g5chlv7rn") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_acodec-sys-0.0.30 (c (n "allegro_acodec-sys") (v "0.0.30") (d (list (d (n "allegro_util") (r "= 0.0.30") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "1adv3di5gwam08jbjj1xk6qy6zj77kn7ak3hg9y376mjxp7z0dhl") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_acodec-sys-0.0.31 (c (n "allegro_acodec-sys") (v "0.0.31") (d (list (d (n "allegro_util") (r "= 0.0.31") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "163ynvkmwk2rc0115gswrpzxw25ydf6x3bycskrdcr489iabz814") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_acodec-sys-0.0.32 (c (n "allegro_acodec-sys") (v "0.0.32") (d (list (d (n "allegro_util") (r "= 0.0.32") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "1d7ip1hyk9m4v71crcm5w8m99mdv6d4fz97mjwyr1ls04s289jrn") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_acodec-sys-0.0.33 (c (n "allegro_acodec-sys") (v "0.0.33") (d (list (d (n "allegro_util") (r "= 0.0.33") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "1cxfka6ddlkbs7xp4gc42v5v9gr9c1lv3142a5qg274z6j88kdv8") (f (quote (("link_static") ("link_none") ("link_debug")))) (l "allegro_acodec")))

(define-public crate-allegro_acodec-sys-0.0.34 (c (n "allegro_acodec-sys") (v "0.0.34") (d (list (d (n "allegro_util") (r "= 0.0.34") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "11n61sz4x3g3qxf2wljggpi7n7a0ccldm4cm7bzlgcji7gxw6f4d") (f (quote (("link_static") ("link_none") ("link_debug")))) (l "allegro_acodec")))

(define-public crate-allegro_acodec-sys-0.0.35 (c (n "allegro_acodec-sys") (v "0.0.35") (d (list (d (n "allegro_util") (r "= 0.0.35") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "03wvz0khbc1xyfsbq25ppf1jcffp9m1yxny3rjsdz04xdwclpwv4") (f (quote (("link_static") ("link_none") ("link_debug")))) (l "allegro_acodec")))

(define-public crate-allegro_acodec-sys-0.0.36 (c (n "allegro_acodec-sys") (v "0.0.36") (d (list (d (n "allegro_util") (r "= 0.0.36") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "1gd4p0qy5vy5r84bm2hk2b7d6jvl0igz4pqffsphcmhklmvzwkk9") (f (quote (("link_static") ("link_none") ("link_debug")))) (l "allegro_acodec")))

(define-public crate-allegro_acodec-sys-0.0.37 (c (n "allegro_acodec-sys") (v "0.0.37") (d (list (d (n "allegro_util") (r "= 0.0.37") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0s83x3481qci1g6vwyxbbrlxrglw81915j1q9xxfsi1845ibfffj") (f (quote (("link_static") ("link_none") ("link_debug")))) (l "allegro_acodec")))

(define-public crate-allegro_acodec-sys-0.0.38 (c (n "allegro_acodec-sys") (v "0.0.38") (d (list (d (n "allegro_util") (r "= 0.0.38") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1my8d5p0m1sabhifhvydy747p1jgqxcs5c5zhsgj2dmq399y5fyy") (f (quote (("link_static") ("link_none") ("link_debug")))) (l "allegro_acodec")))

(define-public crate-allegro_acodec-sys-0.0.39 (c (n "allegro_acodec-sys") (v "0.0.39") (d (list (d (n "allegro_util") (r "= 0.0.39") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "069v95s4a1qhfymycn55klblvdiccl8ynpj3bghm53nx7l96n55x") (f (quote (("link_static") ("link_none") ("link_debug")))) (l "allegro_acodec")))

(define-public crate-allegro_acodec-sys-0.0.40 (c (n "allegro_acodec-sys") (v "0.0.40") (d (list (d (n "allegro_util") (r "= 0.0.40") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0jv5fd9g280lmc2j56fsm26d8z5683j0r31ad819fvpgjbckpsrh") (f (quote (("link_static") ("link_none") ("link_debug")))) (l "allegro_acodec")))

(define-public crate-allegro_acodec-sys-0.0.41 (c (n "allegro_acodec-sys") (v "0.0.41") (d (list (d (n "allegro_util") (r "=0.0.41") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0r2pasra6ymhc7hrhw21c9lm2pw3z59flgzc6gf6q13l1s636pbv") (f (quote (("link_static") ("link_none") ("link_debug")))) (l "allegro_acodec")))

(define-public crate-allegro_acodec-sys-0.0.42 (c (n "allegro_acodec-sys") (v "0.0.42") (d (list (d (n "allegro_util") (r "=0.0.42") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0xpxrjzvyg8q4qmgih95nswxxd60xqvwdlfi40jh6x57piv6b1xd") (f (quote (("link_static") ("link_none") ("link_debug")))) (l "allegro_acodec")))

(define-public crate-allegro_acodec-sys-0.0.43 (c (n "allegro_acodec-sys") (v "0.0.43") (d (list (d (n "allegro_util") (r "=0.0.43") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0xl4cgbmkm29b42ihj3j9abfk0ljyyl6mwmm5acpfysvb604qykz") (f (quote (("link_static") ("link_none") ("link_debug")))) (l "allegro_acodec")))

(define-public crate-allegro_acodec-sys-0.0.44 (c (n "allegro_acodec-sys") (v "0.0.44") (d (list (d (n "allegro_util") (r "=0.0.44") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1200a6gba1x6aal5lkhs836kskiwxn60xcgdki6d3fjyn0wgj58l") (f (quote (("link_static") ("link_none") ("link_debug")))) (l "allegro_acodec")))

(define-public crate-allegro_acodec-sys-0.0.45 (c (n "allegro_acodec-sys") (v "0.0.45") (d (list (d (n "allegro_util") (r "=0.0.45") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1lmn87xh5fpss8hxga2maiw1d8vg3k5hcnan68912rk0ml7g7ank") (f (quote (("link_static") ("link_none") ("link_debug")))) (l "allegro_acodec")))

(define-public crate-allegro_acodec-sys-0.0.46 (c (n "allegro_acodec-sys") (v "0.0.46") (d (list (d (n "allegro_util") (r "=0.0.46") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "043y6gnlf6cnqd08gq89wvg6m52bk94jb5g3hy8xr141jnknm1j4") (f (quote (("link_static") ("link_none") ("link_debug")))) (l "allegro_acodec")))

