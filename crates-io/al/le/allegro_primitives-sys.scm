(define-module (crates-io al le allegro_primitives-sys) #:use-module (crates-io))

(define-public crate-allegro_primitives-sys-0.0.2 (c (n "allegro_primitives-sys") (v "0.0.2") (d (list (d (n "allegro-sys") (r "= 0.0.2") (d #t) (k 0)))) (h "1hc36hvk8dikjbc29r8si2rmak4f661i8grg6f54jywj7ck9xf24") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_primitives-sys-0.0.3 (c (n "allegro_primitives-sys") (v "0.0.3") (d (list (d (n "allegro-sys") (r "= 0.0.3") (d #t) (k 0)))) (h "14lng83mjb9ydiiiyhcsdykwq7hggmb9vwjhdlgzcig8zr7x5pm0") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_primitives-sys-0.0.4 (c (n "allegro_primitives-sys") (v "0.0.4") (d (list (d (n "allegro-sys") (r "= 0.0.4") (d #t) (k 0)))) (h "1pkmhgjgnr6gvf9mkzffnsrl60cz8kac1jxmsnxyxy06hw6hdbcn") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_primitives-sys-0.0.5 (c (n "allegro_primitives-sys") (v "0.0.5") (d (list (d (n "allegro-sys") (r "= 0.0.5") (d #t) (k 0)))) (h "02l64ri0nvn0zirvwkxdnwim1ywknhvjyzkjlhynqxzp4n5bqgl1") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_primitives-sys-0.0.6 (c (n "allegro_primitives-sys") (v "0.0.6") (d (list (d (n "allegro-sys") (r "= 0.0.6") (d #t) (k 0)))) (h "158876am6n62y9w4gkli3dg1zlcnjk5fnx3n5f292swky3l3sc70") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_primitives-sys-0.0.7 (c (n "allegro_primitives-sys") (v "0.0.7") (d (list (d (n "allegro-sys") (r "= 0.0.7") (d #t) (k 0)))) (h "10fbnnpr8fslqivbs710cwja1h3clkxqcl9g8a0mq0xkk0vqc78k") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_primitives-sys-0.0.8 (c (n "allegro_primitives-sys") (v "0.0.8") (d (list (d (n "allegro-sys") (r "= 0.0.8") (d #t) (k 0)))) (h "1hck5fk5r6807n98hcs35w1f6i6jwa54pwwsv7sa7r1qj6znrwrx") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_primitives-sys-0.0.10 (c (n "allegro_primitives-sys") (v "0.0.10") (d (list (d (n "allegro-sys") (r "= 0.0.10") (d #t) (k 0)))) (h "1r0g60gjcamr6p7dlwmwbl9m4nh0q915b7r9w36a6kmwimrvv5i0") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_primitives-sys-0.0.11 (c (n "allegro_primitives-sys") (v "0.0.11") (d (list (d (n "allegro-sys") (r "= 0.0.11") (d #t) (k 0)))) (h "094a92b7cnfinxsc9m0hxh6n4ivc7w8x6jn0ld00k1qzl94wxsg4") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_primitives-sys-0.0.12 (c (n "allegro_primitives-sys") (v "0.0.12") (d (list (d (n "allegro-sys") (r "= 0.0.12") (d #t) (k 0)))) (h "1flha86250vikyph02zj3ikyhrw97zfigjpvcpc0sn97azdahn6s") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_primitives-sys-0.0.13 (c (n "allegro_primitives-sys") (v "0.0.13") (d (list (d (n "allegro-sys") (r "= 0.0.13") (d #t) (k 0)))) (h "0qsiwbznvz0p58vpzp0k2qv025xvp4i5blnqxinkan811r8bb93r") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_primitives-sys-0.0.14 (c (n "allegro_primitives-sys") (v "0.0.14") (d (list (d (n "allegro-sys") (r "= 0.0.14") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.14") (d #t) (k 0)))) (h "1s29abkavn2cy4v6b8w4fk7wxs5ylfady2nkkf5y014ymk6z8265") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_primitives-sys-0.0.15 (c (n "allegro_primitives-sys") (v "0.0.15") (d (list (d (n "allegro-sys") (r "= 0.0.15") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.15") (d #t) (k 0)))) (h "12ymkl1w3x5rffrcjiw26gshkifszb3m5cc6irzmr0n3fr24bdnz") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_primitives-sys-0.0.16 (c (n "allegro_primitives-sys") (v "0.0.16") (d (list (d (n "allegro-sys") (r "= 0.0.16") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.16") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "1k1yjgsci2zg7qwyp3dyw3gd4cy5n493fmphnqk786b8kam18wgk") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_primitives-sys-0.0.17 (c (n "allegro_primitives-sys") (v "0.0.17") (d (list (d (n "allegro-sys") (r "= 0.0.17") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.17") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "0ssfjvylag5jjhl1sda0yjziljyyhp6bjb86v3p2knc4hr82srvr") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_primitives-sys-0.0.18 (c (n "allegro_primitives-sys") (v "0.0.18") (d (list (d (n "allegro-sys") (r "= 0.0.18") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.18") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "0s61wxb8a0ki2qxrdyzsk7bif3ddjxsawnwkim27sj52sbyqw8pj") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_primitives-sys-0.0.20 (c (n "allegro_primitives-sys") (v "0.0.20") (d (list (d (n "allegro-sys") (r "= 0.0.20") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.20") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "0vh7acax91ld8pir7h8zvkbwzi1snsgmxi7krnrrvpcbds9wwymy") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_primitives-sys-0.0.21 (c (n "allegro_primitives-sys") (v "0.0.21") (d (list (d (n "allegro-sys") (r "= 0.0.21") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.21") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "1y54ci93ysp65z4pxzclh91pj6y3s60gpdzdzx331s5shq7cd20i") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_primitives-sys-0.0.22 (c (n "allegro_primitives-sys") (v "0.0.22") (d (list (d (n "allegro-sys") (r "= 0.0.22") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.22") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "0k250854qbl9i2rhlzs511ld0m7rmd2ixpicz96nanhj0iv5i1fj") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_primitives-sys-0.0.23 (c (n "allegro_primitives-sys") (v "0.0.23") (d (list (d (n "allegro-sys") (r "= 0.0.23") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.23") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "0l600rhnzsdg8y4vwlxg828xqhzdakq0m9hv36n8fnhr5fkhyk0n") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_primitives-sys-0.0.24 (c (n "allegro_primitives-sys") (v "0.0.24") (d (list (d (n "allegro-sys") (r "= 0.0.24") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.24") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "15l8xyix96mg8bpli0a3sa4x2c8w6vps7sa7pg332jys520qq4iq") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_primitives-sys-0.0.25 (c (n "allegro_primitives-sys") (v "0.0.25") (d (list (d (n "allegro-sys") (r "= 0.0.25") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.25") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "16dq4xr7ld99pm7x818vc7gqx8vl3h7ryw7dd3bk3fh4xkd6zpkj") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_primitives-sys-0.0.26 (c (n "allegro_primitives-sys") (v "0.0.26") (d (list (d (n "allegro-sys") (r "= 0.0.26") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.26") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "0ivlnjrpaf2yxxmnvd5lcq6vxf3cwigrz2ksibhrcm4qwpk4krqd") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_primitives-sys-0.0.27 (c (n "allegro_primitives-sys") (v "0.0.27") (d (list (d (n "allegro-sys") (r "= 0.0.27") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.27") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "0pbs2kin9ys6wzhc9j83vh332aq8pjm3hhdnsxa868ny4xqs6kis") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_primitives-sys-0.0.28 (c (n "allegro_primitives-sys") (v "0.0.28") (d (list (d (n "allegro-sys") (r "= 0.0.28") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.28") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "0br50jbi63hn89rpc1ilaqrz9j3f5n0c924v581xhh3ygnfx1z9s") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_primitives-sys-0.0.29 (c (n "allegro_primitives-sys") (v "0.0.29") (d (list (d (n "allegro-sys") (r "= 0.0.29") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.29") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "1b6pfyz9bp24kdbmw9zwvrcfm5v4890md4mbhbihfmj7vg2vdwb4") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_primitives-sys-0.0.30 (c (n "allegro_primitives-sys") (v "0.0.30") (d (list (d (n "allegro-sys") (r "= 0.0.30") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.30") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "0bhjb7dqc2xfqkkx20sg5wy1b9cg2whvp7vy4b4jfgvn3yjbj6gq") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_primitives-sys-0.0.31 (c (n "allegro_primitives-sys") (v "0.0.31") (d (list (d (n "allegro-sys") (r "= 0.0.31") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.31") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "1dsz0pc2x8jiqv7a62mkknf0r84zq61qxrbwdh240lf4gk87hljb") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_primitives-sys-0.0.32 (c (n "allegro_primitives-sys") (v "0.0.32") (d (list (d (n "allegro-sys") (r "= 0.0.32") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.32") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "1d0038yadkasw0d0yr3j2lfw8aq8yc5l9p6mcv4z9wf8x5wj72r1") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_primitives-sys-0.0.33 (c (n "allegro_primitives-sys") (v "0.0.33") (d (list (d (n "allegro-sys") (r "= 0.0.33") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.33") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "0ph4qzidq0h9yfwvz9wb19r2cyn1s44vmcd4s65v98m268xw93fj") (f (quote (("link_static") ("link_none") ("link_debug")))) (l "allegro_primitives")))

(define-public crate-allegro_primitives-sys-0.0.34 (c (n "allegro_primitives-sys") (v "0.0.34") (d (list (d (n "allegro-sys") (r "= 0.0.34") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.34") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "1cgaam4qdcj26x2471zgml1fvnszch6xwn86v02zq6wqwh2rhfzl") (f (quote (("link_static") ("link_none") ("link_debug")))) (l "allegro_primitives")))

(define-public crate-allegro_primitives-sys-0.0.35 (c (n "allegro_primitives-sys") (v "0.0.35") (d (list (d (n "allegro-sys") (r "= 0.0.35") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.35") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "0iqwn5rf8haxlapvpdvg3s960d6l1kxrw74cl64gjal0mn5narpr") (f (quote (("link_static") ("link_none") ("link_debug")))) (l "allegro_primitives")))

(define-public crate-allegro_primitives-sys-0.0.36 (c (n "allegro_primitives-sys") (v "0.0.36") (d (list (d (n "allegro-sys") (r "= 0.0.36") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.36") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "0na8bn94q27j6bllkj5wn4ygwq3d427yzrhvin6mgdj7bxrxvzq5") (f (quote (("link_static") ("link_none") ("link_debug")))) (l "allegro_primitives")))

(define-public crate-allegro_primitives-sys-0.0.37 (c (n "allegro_primitives-sys") (v "0.0.37") (d (list (d (n "allegro-sys") (r "= 0.0.37") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.37") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0jnf610632xgr8h46vb8hjxry6z6fhciqimf1zv1g2imxl3w0i3n") (f (quote (("link_static") ("link_none") ("link_debug")))) (l "allegro_primitives")))

(define-public crate-allegro_primitives-sys-0.0.38 (c (n "allegro_primitives-sys") (v "0.0.38") (d (list (d (n "allegro-sys") (r "= 0.0.38") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.38") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "14lkph80hf7wclqspidpwl3c1a43q5vj38phaa7glv6pi7j2hfhz") (f (quote (("link_static") ("link_none") ("link_debug")))) (l "allegro_primitives")))

(define-public crate-allegro_primitives-sys-0.0.39 (c (n "allegro_primitives-sys") (v "0.0.39") (d (list (d (n "allegro-sys") (r "= 0.0.39") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.39") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "04cgpk855sgabbgpksp8i363nyriiwl4kd9rcwclbnqflmarzqry") (f (quote (("link_static") ("link_none") ("link_debug")))) (l "allegro_primitives")))

(define-public crate-allegro_primitives-sys-0.0.40 (c (n "allegro_primitives-sys") (v "0.0.40") (d (list (d (n "allegro-sys") (r "= 0.0.40") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.40") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1vsdq0fvrads86p9hvqvpfkjwz6rnbz9n0j97gzx9fxrx5ykg82y") (f (quote (("link_static") ("link_none") ("link_debug")))) (l "allegro_primitives")))

(define-public crate-allegro_primitives-sys-0.0.41 (c (n "allegro_primitives-sys") (v "0.0.41") (d (list (d (n "allegro-sys") (r "=0.0.41") (d #t) (k 0)) (d (n "allegro_util") (r "=0.0.41") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1rm63sr3g7bv4p87ndmbdxvplw1d7pcs2kr6j471pbpqjzbf9jmx") (f (quote (("link_static") ("link_none") ("link_debug")))) (l "allegro_primitives")))

(define-public crate-allegro_primitives-sys-0.0.42 (c (n "allegro_primitives-sys") (v "0.0.42") (d (list (d (n "allegro-sys") (r "=0.0.42") (d #t) (k 0)) (d (n "allegro_util") (r "=0.0.42") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0bgbhmh5z7vrfyjgmhhp73982wykf7ggg7dbq76v45al77ghqqim") (f (quote (("link_static") ("link_none") ("link_debug")))) (l "allegro_primitives")))

(define-public crate-allegro_primitives-sys-0.0.43 (c (n "allegro_primitives-sys") (v "0.0.43") (d (list (d (n "allegro-sys") (r "=0.0.43") (d #t) (k 0)) (d (n "allegro_util") (r "=0.0.43") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1hzcsadqpj17dryhnyibdz8jmiwqrwqv27m7kyx8am2kipj5yp6p") (f (quote (("link_static") ("link_none") ("link_debug")))) (l "allegro_primitives")))

(define-public crate-allegro_primitives-sys-0.0.44 (c (n "allegro_primitives-sys") (v "0.0.44") (d (list (d (n "allegro-sys") (r "=0.0.44") (d #t) (k 0)) (d (n "allegro_util") (r "=0.0.44") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1vnl3dr371r4halrpbyw1g2spszypv8vf72q1cc7fh59kalhs3ll") (f (quote (("link_static") ("link_none") ("link_debug")))) (l "allegro_primitives")))

(define-public crate-allegro_primitives-sys-0.0.45 (c (n "allegro_primitives-sys") (v "0.0.45") (d (list (d (n "allegro-sys") (r "=0.0.45") (d #t) (k 0)) (d (n "allegro_util") (r "=0.0.45") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1nsjq7y4v0551jri2b0dx2p24x3x9ffah7mwl39ikiqvlpc7dj42") (f (quote (("link_static") ("link_none") ("link_debug")))) (l "allegro_primitives")))

(define-public crate-allegro_primitives-sys-0.0.46 (c (n "allegro_primitives-sys") (v "0.0.46") (d (list (d (n "allegro-sys") (r "=0.0.46") (d #t) (k 0)) (d (n "allegro_util") (r "=0.0.46") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0657aqy9wn5hpfb93jrfdzq2g5spdi51ccg1aw8vnl8c2dcm833r") (f (quote (("link_static") ("link_none") ("link_debug")))) (l "allegro_primitives")))

