(define-module (crates-io al le allenap-flock-experiment) #:use-module (crates-io))

(define-public crate-allenap-flock-experiment-0.1.0 (c (n "allenap-flock-experiment") (v "0.1.0") (d (list (d (n "getch") (r "^0.2.1") (d #t) (k 0)) (d (n "nix") (r "^0.23.0") (d #t) (k 0)))) (h "0fnkcfg9wa9dn3qfcxcciipzjhfngk4lz2xfrzsxjnv6bly5mnw5")))

(define-public crate-allenap-flock-experiment-0.1.1 (c (n "allenap-flock-experiment") (v "0.1.1") (d (list (d (n "getch") (r "^0.3.1") (d #t) (k 0)) (d (n "nix") (r "^0.23.1") (d #t) (k 0)))) (h "0ag9n914qlzlszxacbz3qk2fn3nlqsmk0qz99lpm1shhra0sd6qj")))

(define-public crate-allenap-flock-experiment-0.2.0 (c (n "allenap-flock-experiment") (v "0.2.0") (d (list (d (n "getch") (r "^0.3.1") (d #t) (k 0)) (d (n "nix") (r "^0.23.1") (d #t) (k 0)))) (h "0q844yci72wjbdk7cfa06q4f9xx4dljx8d61jmn4cw85f2b68v2b")))

