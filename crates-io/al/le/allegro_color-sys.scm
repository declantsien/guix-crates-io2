(define-module (crates-io al le allegro_color-sys) #:use-module (crates-io))

(define-public crate-allegro_color-sys-0.0.23 (c (n "allegro_color-sys") (v "0.0.23") (d (list (d (n "allegro-sys") (r "= 0.0.23") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.23") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "1fl6nzqfahz21s0bjclsjly866rdgzr3isx2cfv9pvwgdv66wkqx") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_color-sys-0.0.24 (c (n "allegro_color-sys") (v "0.0.24") (d (list (d (n "allegro-sys") (r "= 0.0.24") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.24") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "17s6y537qyjmdbficiqy317wscjnh7dfjx8x1kd65wagnhpj6w81") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_color-sys-0.0.25 (c (n "allegro_color-sys") (v "0.0.25") (d (list (d (n "allegro-sys") (r "= 0.0.25") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.25") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "05z5jag952k4gmmxg8wk459hszci3ckz43dxmavsmnnhfjlywhhh") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_color-sys-0.0.26 (c (n "allegro_color-sys") (v "0.0.26") (d (list (d (n "allegro-sys") (r "= 0.0.26") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.26") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "0knqxssz8cwivwz07ysdbbnvh0s0n5cvr055daadf1aif3yyy4x2") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_color-sys-0.0.27 (c (n "allegro_color-sys") (v "0.0.27") (d (list (d (n "allegro-sys") (r "= 0.0.27") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.27") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "0z0dwass24hbk4502wb4gda6p5l09m7pvc0icpld49szy6z9z8li") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_color-sys-0.0.28 (c (n "allegro_color-sys") (v "0.0.28") (d (list (d (n "allegro-sys") (r "= 0.0.28") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.28") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "1a2jlbc1w4ncs0gm37ssxwqgszh9v58vh9f3k6pabhyvvxrdhwvn") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_color-sys-0.0.29 (c (n "allegro_color-sys") (v "0.0.29") (d (list (d (n "allegro-sys") (r "= 0.0.29") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.29") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "0vd2vyzz8ii6xqjqhr2nw0xk92ainwcr42348yawmvpqa2wg5268") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_color-sys-0.0.30 (c (n "allegro_color-sys") (v "0.0.30") (d (list (d (n "allegro-sys") (r "= 0.0.30") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.30") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "0crak8ypfwc7g0iyy2s7qwxad2z54vky1s63q1siqgbg3cww1733") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_color-sys-0.0.31 (c (n "allegro_color-sys") (v "0.0.31") (d (list (d (n "allegro-sys") (r "= 0.0.31") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.31") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "115qcjdmwpc9lamn01g95kzhfbmzs7l6islb0r5a3cyqivx0fhb5") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_color-sys-0.0.32 (c (n "allegro_color-sys") (v "0.0.32") (d (list (d (n "allegro-sys") (r "= 0.0.32") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.32") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "02l5zzvgqcj9cqgdfq4hhv93nvpb0irwrkr4wakrrbnk9znzgpla") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro_color-sys-0.0.33 (c (n "allegro_color-sys") (v "0.0.33") (d (list (d (n "allegro-sys") (r "= 0.0.33") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.33") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "16jrinjxcdkchdf8nvcfb37dj4y1bln7wwy7yj50gdqc4wysps75") (f (quote (("link_static") ("link_none") ("link_debug")))) (l "allegro_color")))

(define-public crate-allegro_color-sys-0.0.34 (c (n "allegro_color-sys") (v "0.0.34") (d (list (d (n "allegro-sys") (r "= 0.0.34") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.34") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "0a19al4rpik6mkkdvyzj2h05kik2qdxaa85g7w2q3lwnyc5z0c6j") (f (quote (("link_static") ("link_none") ("link_debug")))) (l "allegro_color")))

(define-public crate-allegro_color-sys-0.0.35 (c (n "allegro_color-sys") (v "0.0.35") (d (list (d (n "allegro-sys") (r "= 0.0.35") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.35") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "0h29ziw67p3w3rdcq1xa3icfwr223l8bjq3nj1g1kxj4p9p7b5dy") (f (quote (("link_static") ("link_none") ("link_debug")))) (l "allegro_color")))

(define-public crate-allegro_color-sys-0.0.36 (c (n "allegro_color-sys") (v "0.0.36") (d (list (d (n "allegro-sys") (r "= 0.0.36") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.36") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "0wi8ssgp5qq67y3zzlahrrdwnrsmxph58jg2a4kyw5lr0asp54qy") (f (quote (("link_static") ("link_none") ("link_debug")))) (l "allegro_color")))

(define-public crate-allegro_color-sys-0.0.37 (c (n "allegro_color-sys") (v "0.0.37") (d (list (d (n "allegro-sys") (r "= 0.0.37") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.37") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "06p2f4gkk6wc3psqn45zg40356jx8vd49dvlvwklg61s5xk6q7z0") (f (quote (("link_static") ("link_none") ("link_debug")))) (l "allegro_color")))

(define-public crate-allegro_color-sys-0.0.38 (c (n "allegro_color-sys") (v "0.0.38") (d (list (d (n "allegro-sys") (r "= 0.0.38") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.38") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0wb5akqxdw5vvarqp63z553j0ck937f7h1ag1qipzmpch38hcxvh") (f (quote (("link_static") ("link_none") ("link_debug")))) (l "allegro_color")))

(define-public crate-allegro_color-sys-0.0.39 (c (n "allegro_color-sys") (v "0.0.39") (d (list (d (n "allegro-sys") (r "= 0.0.39") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.39") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0xabi8yl50lhb3wcma13ji214yy2687p2j8dni06kb0va8q9axdk") (f (quote (("link_static") ("link_none") ("link_debug")))) (l "allegro_color")))

(define-public crate-allegro_color-sys-0.0.40 (c (n "allegro_color-sys") (v "0.0.40") (d (list (d (n "allegro-sys") (r "= 0.0.40") (d #t) (k 0)) (d (n "allegro_util") (r "= 0.0.40") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1jdd5aqwam63snix4ndfqky9bbb6zwcf2k44s0ckyjjdjsfnaird") (f (quote (("link_static") ("link_none") ("link_debug")))) (l "allegro_color")))

(define-public crate-allegro_color-sys-0.0.41 (c (n "allegro_color-sys") (v "0.0.41") (d (list (d (n "allegro-sys") (r "=0.0.41") (d #t) (k 0)) (d (n "allegro_util") (r "=0.0.41") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0v9r2s6hiscjchiq476jy4b1gl7d3gbn91vam1b14rc2znhp242l") (f (quote (("link_static") ("link_none") ("link_debug")))) (l "allegro_color")))

(define-public crate-allegro_color-sys-0.0.42 (c (n "allegro_color-sys") (v "0.0.42") (d (list (d (n "allegro-sys") (r "=0.0.42") (d #t) (k 0)) (d (n "allegro_util") (r "=0.0.42") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "19f6mkmy6myq4lzzm2x0r7aqkcy3qxami0gxp4wvr07b8460591l") (f (quote (("link_static") ("link_none") ("link_debug")))) (l "allegro_color")))

(define-public crate-allegro_color-sys-0.0.43 (c (n "allegro_color-sys") (v "0.0.43") (d (list (d (n "allegro-sys") (r "=0.0.43") (d #t) (k 0)) (d (n "allegro_util") (r "=0.0.43") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1mwpxfg3jsd8s763p5aax0ff4pmhbzppkx6snvssch62f65yyv0x") (f (quote (("link_static") ("link_none") ("link_debug")))) (l "allegro_color")))

(define-public crate-allegro_color-sys-0.0.44 (c (n "allegro_color-sys") (v "0.0.44") (d (list (d (n "allegro-sys") (r "=0.0.44") (d #t) (k 0)) (d (n "allegro_util") (r "=0.0.44") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0f0az97rz505kk8fws5hc5j0y8hbzncsvb2h4cghczfbwd6rk459") (f (quote (("link_static") ("link_none") ("link_debug")))) (l "allegro_color")))

(define-public crate-allegro_color-sys-0.0.45 (c (n "allegro_color-sys") (v "0.0.45") (d (list (d (n "allegro-sys") (r "=0.0.45") (d #t) (k 0)) (d (n "allegro_util") (r "=0.0.45") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0vwg0rf8f9wkawlv8qr9a0yqxl7zmcvx9msqs82fbqld6jaqmq8c") (f (quote (("link_static") ("link_none") ("link_debug")))) (l "allegro_color")))

(define-public crate-allegro_color-sys-0.0.46 (c (n "allegro_color-sys") (v "0.0.46") (d (list (d (n "allegro-sys") (r "=0.0.46") (d #t) (k 0)) (d (n "allegro_util") (r "=0.0.46") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1l23kbqfdk7xkk3l214bvsnmlvy6r38n93ja5jiyhf61m781yr2w") (f (quote (("link_static") ("link_none") ("link_debug")))) (l "allegro_color")))

