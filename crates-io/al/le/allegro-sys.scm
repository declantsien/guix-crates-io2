(define-module (crates-io al le allegro-sys) #:use-module (crates-io))

(define-public crate-allegro-sys-0.0.2 (c (n "allegro-sys") (v "0.0.2") (h "0cfihxn2rgj3c1p2lbf6ddw01hhzs2kh2ddqwwwhl87a3337z53q") (f (quote (("link_static") ("link_none") ("link_monolith") ("link_debug"))))))

(define-public crate-allegro-sys-0.0.3 (c (n "allegro-sys") (v "0.0.3") (h "0fd689jrs4a7fgl1dk0sriy2qy54bplifrf1ai0d10kvribshzq8") (f (quote (("link_static") ("link_none") ("link_monolith") ("link_debug"))))))

(define-public crate-allegro-sys-0.0.4 (c (n "allegro-sys") (v "0.0.4") (h "0lcc8pbbkviaj9z9f79gcpdmi0k0frrv9mrwbb8zkrly8p8lqn9n") (f (quote (("link_static") ("link_none") ("link_monolith") ("link_debug"))))))

(define-public crate-allegro-sys-0.0.5 (c (n "allegro-sys") (v "0.0.5") (h "1mspjgzqr804hjx6syrd4wd9d5bjs497bw3r4hd28ingqvxpbp1w") (f (quote (("link_static") ("link_none") ("link_monolith") ("link_debug"))))))

(define-public crate-allegro-sys-0.0.6 (c (n "allegro-sys") (v "0.0.6") (h "13q407n3qlrc4m4g6kvgppvwpshc9zkrnn7l3bcvgq6vzywmgqjp") (f (quote (("link_static") ("link_none") ("link_monolith") ("link_debug"))))))

(define-public crate-allegro-sys-0.0.7 (c (n "allegro-sys") (v "0.0.7") (h "160p5a5fyha177vf777ack528zij3wn53agi23i66fqik114j675") (f (quote (("link_static") ("link_none") ("link_monolith") ("link_debug"))))))

(define-public crate-allegro-sys-0.0.8 (c (n "allegro-sys") (v "0.0.8") (h "05zg5qlyyshidszkl765pq053idlic0cac43lg0p1c4k1ikl4y57") (f (quote (("link_static") ("link_none") ("link_monolith") ("link_debug"))))))

(define-public crate-allegro-sys-0.0.10 (c (n "allegro-sys") (v "0.0.10") (h "00hm2w7dmxkf9n3252lqfbhv8vwmhc0ik67ahva6anb2sz1ybqk0") (f (quote (("link_static") ("link_none") ("link_monolith") ("link_debug"))))))

(define-public crate-allegro-sys-0.0.11 (c (n "allegro-sys") (v "0.0.11") (h "1bqky55pvqrv5kz7kr5nsza8pxrhp6ya7mjp05w3khfwrd2syys3") (f (quote (("link_static") ("link_none") ("link_monolith") ("link_debug"))))))

(define-public crate-allegro-sys-0.0.12 (c (n "allegro-sys") (v "0.0.12") (h "1shndna6a83fakg0pvw5d7ddgsbiqi595z864dw4sfrfsd948c6w") (f (quote (("link_static") ("link_none") ("link_monolith") ("link_debug"))))))

(define-public crate-allegro-sys-0.0.13 (c (n "allegro-sys") (v "0.0.13") (h "03j81m1k71j3nr77qa14r11ynih5fijfwdvnm0v10q5qdwpj203c") (f (quote (("link_static") ("link_none") ("link_monolith") ("link_debug"))))))

(define-public crate-allegro-sys-0.0.14 (c (n "allegro-sys") (v "0.0.14") (d (list (d (n "allegro_util") (r "= 0.0.14") (d #t) (k 0)))) (h "0cbygzs540qhfimf669fqjs5hk6q1l4v9yg436iwvnzzc2i25v0w") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro-sys-0.0.15 (c (n "allegro-sys") (v "0.0.15") (d (list (d (n "allegro_util") (r "= 0.0.15") (d #t) (k 0)))) (h "10ybhzxglpz8xf9mw4792vbgyrjdrkbjlgblvy82a1fhr4sw19x0") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro-sys-0.0.16 (c (n "allegro-sys") (v "0.0.16") (d (list (d (n "allegro_util") (r "= 0.0.16") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "1zy4zahhcj2sg4ipflsdixkv6rbwnaj439sg3gdh1ja5lb6d4k76") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro-sys-0.0.17 (c (n "allegro-sys") (v "0.0.17") (d (list (d (n "allegro_util") (r "= 0.0.17") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "1ivh2w3hsj145306rn006p6pv4fzjn1x4j2c1nsm28nwi19aj7d4") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro-sys-0.0.18 (c (n "allegro-sys") (v "0.0.18") (d (list (d (n "allegro_util") (r "= 0.0.18") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "1jzcmin1i83frmfynkrf03b5l7nzxri1rm1xhcxn2bxjhr8g7q8q") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro-sys-0.0.20 (c (n "allegro-sys") (v "0.0.20") (d (list (d (n "allegro_util") (r "= 0.0.20") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "0wl91y7pvmg70vhx0dbxw68qmc23r35niawffwrrrmn7g6p1sq6q") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro-sys-0.0.21 (c (n "allegro-sys") (v "0.0.21") (d (list (d (n "allegro_util") (r "= 0.0.21") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "1wcl9nwd5cnzlh2wim8zkh53mxrcn6ypaw8pccsnbyznmbimfwag") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro-sys-0.0.22 (c (n "allegro-sys") (v "0.0.22") (d (list (d (n "allegro_util") (r "= 0.0.22") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "0nil4mfm645rwvwrkvk7il3h6zgzm433k8nqh6dj0b94n4as5qmw") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro-sys-0.0.23 (c (n "allegro-sys") (v "0.0.23") (d (list (d (n "allegro_util") (r "= 0.0.23") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "17rbxsxcfx54fwmkqkp0qbpqfc1rdi07aghfhfcr3227hk2h3ndb") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro-sys-0.0.24 (c (n "allegro-sys") (v "0.0.24") (d (list (d (n "allegro_util") (r "= 0.0.24") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "1y8sf3lf16xc9s6w7zs301y27ag02jqwb0ngp9g8xwp5y8zhijgd") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro-sys-0.0.25 (c (n "allegro-sys") (v "0.0.25") (d (list (d (n "allegro_util") (r "= 0.0.25") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "0vjkva04z5fs9gfqfjivqgl0s8gy4bdw6v4wrryibvzzgb2s18k9") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro-sys-0.0.26 (c (n "allegro-sys") (v "0.0.26") (d (list (d (n "allegro_util") (r "= 0.0.26") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "1vsksam1nj51397m2wry71zsqrn8c93f7zwbpmbbxrwqrjgfl6hn") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro-sys-0.0.27 (c (n "allegro-sys") (v "0.0.27") (d (list (d (n "allegro_util") (r "= 0.0.27") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "0yyrk7vp1a0rl4xh74b4ibbmkxibw1mpzx6wfcx7ijfbzci5s0k7") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro-sys-0.0.28 (c (n "allegro-sys") (v "0.0.28") (d (list (d (n "allegro_util") (r "= 0.0.28") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "17gl06m46a4gv6y9wvd95vr9gzwr35zhrmygxmi8282b6db0syvc") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro-sys-0.0.29 (c (n "allegro-sys") (v "0.0.29") (d (list (d (n "allegro_util") (r "= 0.0.29") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "02khbn4kj286n2arl7b2fp6x5c5rb389gyiwprhx8qrs5ck1rvw9") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro-sys-0.0.30 (c (n "allegro-sys") (v "0.0.30") (d (list (d (n "allegro_util") (r "= 0.0.30") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "070zgn2d2yhgfzya6r647dfjjl7x348bl9j433747bxkxm419i4p") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro-sys-0.0.31 (c (n "allegro-sys") (v "0.0.31") (d (list (d (n "allegro_util") (r "= 0.0.31") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "0hjizbww7nsf4adwn2mj26cm4ph9sd96k85s7x9lmpdfi7nxsp3n") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro-sys-0.0.32 (c (n "allegro-sys") (v "0.0.32") (d (list (d (n "allegro_util") (r "= 0.0.32") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "05k8rrbha6h9ydc3py271lqm7iwcsmnlb425mxgdq3v1m1g1hpvc") (f (quote (("link_static") ("link_none") ("link_debug"))))))

(define-public crate-allegro-sys-0.0.33 (c (n "allegro-sys") (v "0.0.33") (d (list (d (n "allegro_util") (r "= 0.0.33") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "07d16yh7li9piqaxgjgzsnpkwx1kqf99f4b04x4r33nazyiz1g6y") (f (quote (("link_static") ("link_none") ("link_debug")))) (l "allegro")))

(define-public crate-allegro-sys-0.0.34 (c (n "allegro-sys") (v "0.0.34") (d (list (d (n "allegro_util") (r "= 0.0.34") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "045as3fv829h1jcibx68sy2xqsfyzn5ls3ci6pwdpnfn6liacml9") (f (quote (("link_static") ("link_none") ("link_debug")))) (l "allegro")))

(define-public crate-allegro-sys-0.0.35 (c (n "allegro-sys") (v "0.0.35") (d (list (d (n "allegro_util") (r "= 0.0.35") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "1pi3r6y6qjrn1qkri1yg6kdi0flrf0qspkfawfkchbzqsh9v2ccq") (f (quote (("link_static") ("link_none") ("link_debug")))) (l "allegro")))

(define-public crate-allegro-sys-0.0.36 (c (n "allegro-sys") (v "0.0.36") (d (list (d (n "allegro_util") (r "= 0.0.36") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "1whwvqn2dnspsha9dnq46x0aq2qi80c3qar2x9jl80xwzh83sp7z") (f (quote (("link_static") ("link_none") ("link_debug")))) (l "allegro")))

(define-public crate-allegro-sys-0.0.37 (c (n "allegro-sys") (v "0.0.37") (d (list (d (n "allegro_util") (r "= 0.0.37") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "16awi1774i7g9dcb2cz38bc7hmfhjl0pwcsdvkabw98jhb4m5pqz") (f (quote (("link_static") ("link_none") ("link_debug")))) (l "allegro")))

(define-public crate-allegro-sys-0.0.38 (c (n "allegro-sys") (v "0.0.38") (d (list (d (n "allegro_util") (r "= 0.0.38") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0gnvy0fmha96syq2y2dl91pbjvmrk5v77ylbm5x6qpfhsnbzihxj") (f (quote (("link_static") ("link_none") ("link_debug")))) (l "allegro")))

(define-public crate-allegro-sys-0.0.39 (c (n "allegro-sys") (v "0.0.39") (d (list (d (n "allegro_util") (r "= 0.0.39") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "03h079d0wjrw0d8qsdnpq2dbp25npbqz5q0p452v3x74d11slqip") (f (quote (("link_static") ("link_none") ("link_debug")))) (l "allegro")))

(define-public crate-allegro-sys-0.0.40 (c (n "allegro-sys") (v "0.0.40") (d (list (d (n "allegro_util") (r "= 0.0.40") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1hdy3hpbspgp5b2kpgyp446d452jcvs6ga4dqwn1c1in7hh8lbzc") (f (quote (("link_static") ("link_none") ("link_debug")))) (l "allegro")))

(define-public crate-allegro-sys-0.0.41 (c (n "allegro-sys") (v "0.0.41") (d (list (d (n "allegro_util") (r "=0.0.41") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1300xkb7fb9aip0hm37j4ikw1yx480drs0df7iv1w96knz67p29z") (f (quote (("link_static") ("link_none") ("link_debug")))) (l "allegro")))

(define-public crate-allegro-sys-0.0.42 (c (n "allegro-sys") (v "0.0.42") (d (list (d (n "allegro_util") (r "=0.0.42") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "19svlcjqpwgxfm13a7q3r0h346kkqzhdim9v4r78k8yspn0sd1lg") (f (quote (("link_static") ("link_none") ("link_debug")))) (l "allegro")))

(define-public crate-allegro-sys-0.0.43 (c (n "allegro-sys") (v "0.0.43") (d (list (d (n "allegro_util") (r "=0.0.43") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "06p4hqyyx3x5s8af5zmb4ngf5rr40rhiiyy3iijr81j9vcmx4d21") (f (quote (("link_static") ("link_none") ("link_debug")))) (l "allegro")))

(define-public crate-allegro-sys-0.0.44 (c (n "allegro-sys") (v "0.0.44") (d (list (d (n "allegro_util") (r "=0.0.44") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "01qj1mgargfld6lj2dwrxwxmr3kgjh8mjnwzljphwsx8kmlxn634") (f (quote (("link_static") ("link_none") ("link_debug")))) (l "allegro")))

(define-public crate-allegro-sys-0.0.45 (c (n "allegro-sys") (v "0.0.45") (d (list (d (n "allegro_util") (r "=0.0.45") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0vlsiznh100nlh0ib8v9lakbf5q6dg6m09pyv3sd9f5jw1sw4b7v") (f (quote (("link_static") ("link_none") ("link_debug")))) (l "allegro")))

(define-public crate-allegro-sys-0.0.46 (c (n "allegro-sys") (v "0.0.46") (d (list (d (n "allegro_util") (r "=0.0.46") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1k2kzcm1fgazm25sk5gd8qn6gbig1rghzj6rg1yr7wq7qb23bdmr") (f (quote (("link_static") ("link_none") ("link_debug")))) (l "allegro")))

