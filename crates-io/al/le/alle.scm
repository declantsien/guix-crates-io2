(define-module (crates-io al le alle) #:use-module (crates-io))

(define-public crate-alle-0.1.0 (c (n "alle") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.23") (f (quote ("serde"))) (d #t) (k 0)) (d (n "clap") (r "^4.0.29") (d #t) (k 0)) (d (n "comfy-table") (r "^6.1.3") (d #t) (k 0)) (d (n "dirs") (r "^4.0") (d #t) (k 0)) (d (n "rocksdb") (r "^0.19.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.148") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "16qsvsy2vrzij6m7wn1a50b1ahl86xx9dmzynh2vz7xyy5lc0q57")))

(define-public crate-alle-0.2.1 (c (n "alle") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4.23") (f (quote ("serde"))) (d #t) (k 0)) (d (n "clap") (r "^4.0.29") (d #t) (k 0)) (d (n "comfy-table") (r "^6.1.3") (d #t) (k 0)) (d (n "dirs") (r "^4.0") (d #t) (k 0)) (d (n "rocksdb") (r "^0.19.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.148") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "13w89hckf87z0svbnnxnwxp3p4magypww3d5bk71mqk7wbnmyzf0")))

(define-public crate-alle-0.2.2 (c (n "alle") (v "0.2.2") (d (list (d (n "chrono") (r "^0.4.23") (f (quote ("serde"))) (d #t) (k 0)) (d (n "clap") (r "^4.0.29") (d #t) (k 0)) (d (n "comfy-table") (r "^6.1.3") (d #t) (k 0)) (d (n "dirs") (r "^4.0") (d #t) (k 0)) (d (n "rocksdb") (r "^0.19.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.148") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "11hxnbg16s102iacs26gvk6kd0m8di9ab1dp86h4cm39p41yd20y")))

