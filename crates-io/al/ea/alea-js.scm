(define-module (crates-io al ea alea-js) #:use-module (crates-io))

(define-public crate-alea-js-0.1.0 (c (n "alea-js") (v "0.1.0") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)))) (h "10ypmd9lsiqp3safwa91ba29xsk346z07mb20yzx381aq4ajxnjh") (y #t)))

(define-public crate-alea-js-0.1.1 (c (n "alea-js") (v "0.1.1") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)))) (h "12w4q5yxidffzpgvswr7wav903v6pv4lbdlsc80lgxlprvyz687x")))

