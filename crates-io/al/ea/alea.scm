(define-module (crates-io al ea alea) #:use-module (crates-io))

(define-public crate-alea-0.1.0 (c (n "alea") (v "0.1.0") (h "0vfva1b8snm9h3zba90q5gbc3n63nkh7wbw6f34qrg0jv902q4jw")))

(define-public crate-alea-0.2.0 (c (n "alea") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "fastrand") (r "^1") (d #t) (k 2)))) (h "1rbnvb4nz9k40wgkxg6a9k9d1rw4sp8x3ni4cpx71ss0jcvwiffy")))

(define-public crate-alea-0.2.1 (c (n "alea") (v "0.2.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "fastrand") (r "^1") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0047qkdxq08gmdzf6n3vy3v6p7fv9cm89pbrniycdrcdnsw97rpd")))

(define-public crate-alea-0.2.2 (c (n "alea") (v "0.2.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "fastrand") (r "^1") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_pcg") (r "^0.3") (d #t) (k 2)) (d (n "rand_xorshift") (r "^0.3") (d #t) (k 2)))) (h "0ilkn4xv7an94qx9iafk92b1bihzpcmajdh7sl1chqanb1v6945m")))

