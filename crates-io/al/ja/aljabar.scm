(define-module (crates-io al ja aljabar) #:use-module (crates-io))

(define-public crate-aljabar-0.1.0 (c (n "aljabar") (v "0.1.0") (h "1sglmjwwnwh8r45hq1hi5wzv9amg2ic8xz7nql52radg6iv6pxy5")))

(define-public crate-aljabar-0.2.0 (c (n "aljabar") (v "0.2.0") (h "034z472jid8njndrr55rcbpcm4i0h6rx5ic15inhc5xcpbhrymls")))

(define-public crate-aljabar-0.2.1 (c (n "aljabar") (v "0.2.1") (h "1w2jqnp7n3s26i8j0bqwy8aja13hnxh2c3l69p2y9h0lws37fvxy")))

(define-public crate-aljabar-0.2.2 (c (n "aljabar") (v "0.2.2") (h "00iv1i5im6hw80fybz67q27w0ipi9a5dzrw1hvfclkfj7w1h5i5g")))

(define-public crate-aljabar-0.2.3 (c (n "aljabar") (v "0.2.3") (h "1845wk6lvr267ah82bqsrha5w6cdkfgsyzjn24ny44pgr3vkmkvx")))

(define-public crate-aljabar-0.2.4 (c (n "aljabar") (v "0.2.4") (h "00vynalhrk8n73s1xi0xpkh920q6f7l971gm24w71ccdkfxlwjjq")))

(define-public crate-aljabar-0.2.5 (c (n "aljabar") (v "0.2.5") (h "1ndwkqczgjn2rcxagf0pr5fjprjm5i15qj7gx64wh26b5vb3kz25")))

(define-public crate-aljabar-0.2.6 (c (n "aljabar") (v "0.2.6") (d (list (d (n "paste") (r "^0.1") (d #t) (k 0)))) (h "15mrpc2arj7cbhq37zn5wnds9ky3666775nymc8pfdy47vp2zzxg")))

(define-public crate-aljabar-0.2.7 (c (n "aljabar") (v "0.2.7") (d (list (d (n "paste") (r "^0.1") (d #t) (k 0)))) (h "05489qi7xhpc776p8m53jim5s3rrslkqb8anlavivdr9zkjnfr1w")))

(define-public crate-aljabar-0.3.0 (c (n "aljabar") (v "0.3.0") (d (list (d (n "paste") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "191mcz7ky53xl9v58xb8fcragyrz57n8v26icn84d06c7dgsz6zf")))

(define-public crate-aljabar-0.3.1 (c (n "aljabar") (v "0.3.1") (d (list (d (n "paste") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0fngjmlnzgjldcrbvzl5bq1fdnx9csd8bv29himjnjaimni9ybin")))

(define-public crate-aljabar-0.3.2 (c (n "aljabar") (v "0.3.2") (d (list (d (n "paste") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0y56dhkym62ywryz5cb89h764056rnmfv9xcmhh3vl0wfyhv7py2")))

(define-public crate-aljabar-0.4.0 (c (n "aljabar") (v "0.4.0") (d (list (d (n "paste") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "smallvec") (r "^0.6") (d #t) (k 0)))) (h "01knm54lgq3kn9bbhr1zjmps2xd39y6lv518ai7gsyg5s8c15wab")))

(define-public crate-aljabar-0.4.1 (c (n "aljabar") (v "0.4.1") (d (list (d (n "paste") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "smallvec") (r "^0.6") (d #t) (k 0)))) (h "1k4wj4iivzmqxbi61jdln5r7b6dxlw8ah1yq5i6x4kz11qhiiw3g")))

(define-public crate-aljabar-0.4.2 (c (n "aljabar") (v "0.4.2") (d (list (d (n "mint") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "paste") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "smallvec") (r "^0.6") (d #t) (k 0)))) (h "0nz1p63ba913cm0iv2qvbxgvy4842cbw5sv0jmfmbd89vj98shzb")))

(define-public crate-aljabar-0.5.0 (c (n "aljabar") (v "0.5.0") (d (list (d (n "mint") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "paste") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "smallvec") (r "^0.6") (d #t) (k 0)))) (h "01pfgs7p960x8aw0cgm8xqzhi7z8rya2j6gsj032yaqwdjq8nsvr")))

(define-public crate-aljabar-1.0.0 (c (n "aljabar") (v "1.0.0") (d (list (d (n "approx") (r "^0.3.2") (d #t) (k 2)) (d (n "mint") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "paste") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "smallvec") (r "^0.6") (d #t) (k 0)))) (h "0n5bp4p64cjlmmr21sracpj7z7myy1k81g8mwhrmh6w33wwgq26k") (f (quote (("swizzle") ("default"))))))

(define-public crate-aljabar-1.0.1 (c (n "aljabar") (v "1.0.1") (d (list (d (n "approx") (r "^0.3.2") (d #t) (k 2)) (d (n "mint") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "paste") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "smallvec") (r "^0.6") (d #t) (k 0)))) (h "175jyhclk98a3nl1xgr9bjzphlsy6jqc2cwdbg1q8n4bkbhd5f3h") (f (quote (("swizzle") ("default"))))))

(define-public crate-aljabar-1.0.2 (c (n "aljabar") (v "1.0.2") (d (list (d (n "approx") (r "^0.3.2") (d #t) (k 2)) (d (n "mint") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "paste") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "smallvec") (r "^0.6") (d #t) (k 0)))) (h "0vb6z6m5460jvvwkh6a9bfjdih4aia8li5yxnvfh9lg68wgj4xrm") (f (quote (("swizzle") ("default"))))))

