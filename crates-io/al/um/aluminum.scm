(define-module (crates-io al um aluminum) #:use-module (crates-io))

(define-public crate-aluminum-0.2.0 (c (n "aluminum") (v "0.2.0") (d (list (d (n "clap") (r "^2.0") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.0") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.3") (d #t) (k 0)))) (h "0ivzsdanzwljr2h96b5cmbmq8qcq2wn8lfbzw2p7xywmsj5g13ja")))

(define-public crate-aluminum-0.3.0 (c (n "aluminum") (v "0.3.0") (d (list (d (n "clap") (r "^2.0") (d #t) (k 0)) (d (n "hyper") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "yaml-rust") (r "^0.3") (d #t) (k 0)))) (h "0xzf87spnp14wyamxlag17g7rx88x6dmlybbqqsvy097l3lm6dwp")))

