(define-module (crates-io al i- ali-nls-sr) #:use-module (crates-io))

(define-public crate-ali-nls-sr-0.1.0 (c (n "ali-nls-sr") (v "0.1.0") (d (list (d (n "ali-nls-drive") (r "^0.1") (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.0") (d #t) (k 0)) (d (n "uuid") (r "^1.3.0") (f (quote ("v4" "fast-rng" "macro-diagnostics"))) (d #t) (k 0)))) (h "15a7pp763zgvk5qd2bfxk9yi6mkm8p0my997sqlxgvzp76dfj4ah")))

(define-public crate-ali-nls-sr-0.1.1 (c (n "ali-nls-sr") (v "0.1.1") (d (list (d (n "ali-nls-drive") (r "^0.1") (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.0") (d #t) (k 0)) (d (n "uuid") (r "^1.3.0") (f (quote ("v4" "fast-rng" "macro-diagnostics"))) (d #t) (k 0)))) (h "1s06qlj00azh0czvxzihk91lgskfhp4ba6xmjxphp7hajh0m1ddd")))

(define-public crate-ali-nls-sr-0.1.2 (c (n "ali-nls-sr") (v "0.1.2") (d (list (d (n "ali-nls-drive") (r "^0.1") (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.0") (d #t) (k 0)) (d (n "uuid") (r "^1.3.0") (f (quote ("v4" "fast-rng" "macro-diagnostics"))) (d #t) (k 0)))) (h "0i8xycy28xcrw7bbd6kxglvh9day16z1mjbyp6gc4v8fyc9i5f0v")))

