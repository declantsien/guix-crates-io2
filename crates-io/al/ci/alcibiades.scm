(define-module (crates-io al ci alcibiades) #:use-module (crates-io))

(define-public crate-alcibiades-0.2.0 (c (n "alcibiades") (v "0.2.0") (d (list (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.3.14") (d #t) (k 0)) (d (n "regex") (r "^0.1.71") (d #t) (k 0)))) (h "0n8p82r6z3ygk1c87d5d5sgqnl888shxxcfbrllch1q2591jrrpc")))

(define-public crate-alcibiades-0.2.1 (c (n "alcibiades") (v "0.2.1") (d (list (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.3.14") (d #t) (k 0)) (d (n "regex") (r "^0.1.71") (d #t) (k 0)))) (h "1ik6rq05nxrykg6nd8gzd09r1prw0g2csjmwpczcak23yb9hbqy9")))

(define-public crate-alcibiades-0.2.2 (c (n "alcibiades") (v "0.2.2") (d (list (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.3.14") (d #t) (k 0)) (d (n "regex") (r "^0.1.71") (d #t) (k 0)))) (h "1hajsp1hfsrxq5vijp15wp8i9mdqymny2xyk07lhdck7dcy9p92z")))

(define-public crate-alcibiades-0.2.3 (c (n "alcibiades") (v "0.2.3") (d (list (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.3.14") (d #t) (k 0)) (d (n "regex") (r "^0.1.71") (d #t) (k 0)))) (h "1i811zjrj1fdfb0r4bmy0wpzfa4rmmk7yy5nwi6l7c00862s64hl")))

(define-public crate-alcibiades-0.2.4 (c (n "alcibiades") (v "0.2.4") (d (list (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.3.14") (d #t) (k 0)) (d (n "regex") (r "^0.1.71") (d #t) (k 0)))) (h "0bzj3aqnngn58nw4kdilahwpi3w5vx8w04ibiipbxjk25vfk95js")))

(define-public crate-alcibiades-0.2.5 (c (n "alcibiades") (v "0.2.5") (d (list (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.3.14") (d #t) (k 0)) (d (n "regex") (r "^0.2.1") (d #t) (k 0)))) (h "0vnnc0wn535r49cvhxbrgkv2cypc58x3ni99jislin0diy1rxa4p")))

(define-public crate-alcibiades-0.2.6 (c (n "alcibiades") (v "0.2.6") (d (list (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.18") (d #t) (k 0)) (d (n "rand") (r "^0.3.14") (d #t) (k 0)) (d (n "regex") (r "^0.2.1") (d #t) (k 0)))) (h "1ggci7z70f84knm487fr0s2p6m5yd9j2jsdwlr6lsiiapsg9lpcb")))

(define-public crate-alcibiades-0.2.7 (c (n "alcibiades") (v "0.2.7") (d (list (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.18") (d #t) (k 0)) (d (n "rand") (r "^0.3.14") (d #t) (k 0)) (d (n "regex") (r "^0.2.1") (d #t) (k 0)))) (h "16mhal7lnvdqgnbyvkvww5ls0r8g6pglxjfjp2365sf46fil6fw5")))

(define-public crate-alcibiades-0.3.0 (c (n "alcibiades") (v "0.3.0") (d (list (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.18") (d #t) (k 0)) (d (n "rand") (r "^0.3.14") (d #t) (k 0)) (d (n "regex") (r "^0.2.1") (d #t) (k 0)))) (h "0y66szzm01xspch98l47vc8mrmqxnrc7rpy5z43vs2y7q33kqz4y")))

