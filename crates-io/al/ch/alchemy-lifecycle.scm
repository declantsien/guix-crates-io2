(define-module (crates-io al ch alchemy-lifecycle) #:use-module (crates-io))

(define-public crate-alchemy-lifecycle-0.1.0 (c (n "alchemy-lifecycle") (v "0.1.0") (d (list (d (n "alchemy-styles") (r "^0.1") (d #t) (k 0)) (d (n "objc") (r "^0.2.6") (o #t) (d #t) (k 0)) (d (n "objc_id") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "15ssp7mrqfl7af8hj23752qkkdn1rii1kim0b4gkdxrs5v7aych4") (f (quote (("cocoa" "objc" "objc_id"))))))

