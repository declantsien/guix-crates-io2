(define-module (crates-io al ch alchemy-styles) #:use-module (crates-io))

(define-public crate-alchemy-styles-0.1.0 (c (n "alchemy-styles") (v "0.1.0") (d (list (d (n "cssparser") (r "^0.25.5") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (o #t) (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1v4msyxrgnfwjhil6piaqfbkbx6b2gz3m1g0475nh225cni8l3mi") (f (quote (("tokenize" "proc-macro2" "quote") ("parser" "cssparser"))))))

