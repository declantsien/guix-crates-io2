(define-module (crates-io al ch alchemy-cocoa) #:use-module (crates-io))

(define-public crate-alchemy-cocoa-0.1.0 (c (n "alchemy-cocoa") (v "0.1.0") (d (list (d (n "alchemy-lifecycle") (r "^0.1") (f (quote ("cocoa"))) (d #t) (k 0)) (d (n "alchemy-styles") (r "^0.1") (d #t) (k 0)) (d (n "cocoa") (r "^0.18.4") (d #t) (k 0)) (d (n "core-foundation") (r "^0.6") (d #t) (k 0)) (d (n "core-graphics") (r "^0.17.1") (d #t) (k 0)) (d (n "dispatch") (r "^0.1.4") (d #t) (k 0)) (d (n "objc") (r "^0.2.6") (d #t) (k 0)) (d (n "objc_id") (r "^0.1.1") (d #t) (k 0)))) (h "17cgz6dlnd06w0ppwn6zypi0l7w4m8797z707i6k61ddl69s7m5r")))

