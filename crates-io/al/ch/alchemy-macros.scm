(define-module (crates-io al ch alchemy-macros) #:use-module (crates-io))

(define-public crate-alchemy-macros-0.1.0 (c (n "alchemy-macros") (v "0.1.0") (d (list (d (n "alchemy-styles") (r "^0.1") (f (quote ("parser" "tokenize"))) (d #t) (k 0)) (d (n "ansi_term") (r "^0.11.0") (d #t) (k 0)) (d (n "lalrpop") (r "^0.16.1") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.16.1") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)) (d (n "version_check") (r "^0.1.5") (d #t) (k 1)))) (h "1qhbzckc2qznhgq53fvxql5dh6lyri9d7rrdrljrx6d9361rjia6")))

