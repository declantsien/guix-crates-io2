(define-module (crates-io al ch alchemy-rs) #:use-module (crates-io))

(define-public crate-alchemy-rs-0.1.0 (c (n "alchemy-rs") (v "0.1.0") (d (list (d (n "actix-rt") (r "^2.7.0") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.50") (k 0)) (d (n "ethers") (r "^0.17.0") (f (quote ("ws" "rustls"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (f (quote ("raw_value"))) (k 0)))) (h "01waiab15vb3mrscc96acv4l6lnmrwh1rbrp760igq8p3s31cxm1")))

