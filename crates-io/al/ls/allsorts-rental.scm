(define-module (crates-io al ls allsorts-rental) #:use-module (crates-io))

(define-public crate-allsorts-rental-0.5.6 (c (n "allsorts-rental") (v "0.5.6") (d (list (d (n "rental-impl") (r "=0.5.5") (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.0.0") (k 0)))) (h "1r6x6xpm84xk6c4d0n6ahz139xg1j084algfh2kcp21zz66rkv7f") (f (quote (("std" "stable_deref_trait/std") ("default" "std") ("alloc" "stable_deref_trait/alloc"))))))

