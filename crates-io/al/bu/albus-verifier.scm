(define-module (crates-io al bu albus-verifier) #:use-module (crates-io))

(define-public crate-albus-verifier-0.1.0 (c (n "albus-verifier") (v "0.1.0") (d (list (d (n "arrayref") (r "^0.3.7") (d #t) (k 0)) (d (n "solana-program") (r "^1.14") (d #t) (k 0)))) (h "14nri7rs6fcaywg9gm7ly096z57q2f7ymc0jk5jrasdzwz581vyg") (y #t)))

(define-public crate-albus-verifier-0.1.1 (c (n "albus-verifier") (v "0.1.1") (d (list (d (n "arrayref") (r "^0.3.7") (d #t) (k 0)) (d (n "solana-program") (r "^1.14") (d #t) (k 0)))) (h "1bmqd1m8582ywvbpm972gp7drd1jdhq8drfk3b72jwvyy0lgn8m8") (y #t)))

