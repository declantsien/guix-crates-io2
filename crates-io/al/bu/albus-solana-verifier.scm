(define-module (crates-io al bu albus-solana-verifier) #:use-module (crates-io))

(define-public crate-albus-solana-verifier-0.1.2 (c (n "albus-solana-verifier") (v "0.1.2") (d (list (d (n "anchor-lang") (r "^0.29.0") (o #t) (d #t) (k 0)) (d (n "arrayref") (r "^0.3.7") (d #t) (k 0)) (d (n "solana-program") (r "^1.16") (d #t) (k 0)))) (h "1a2vndwpddgm8fif1ypvca68qyzafabrzf7y2ck2fvkqvqc3wh5r") (f (quote (("cpi" "anchor-lang"))))))

(define-public crate-albus-solana-verifier-0.1.3 (c (n "albus-solana-verifier") (v "0.1.3") (d (list (d (n "anchor-lang") (r "^0.29.0") (o #t) (d #t) (k 0)) (d (n "arrayref") (r "^0.3.7") (d #t) (k 0)) (d (n "solana-program") (r "^1.16") (d #t) (k 0)))) (h "0qyk7b3rnj347hdlw1d14mj7c1lhkqhyck2lb3slqb04mc0j5xzp") (f (quote (("cpi" "anchor-lang"))))))

