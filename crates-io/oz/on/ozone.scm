(define-module (crates-io oz on ozone) #:use-module (crates-io))

(define-public crate-ozone-0.0.1 (c (n "ozone") (v "0.0.1") (h "0myzahprj7k8nn93l82qr7a83lhcni1ksy56k480zp42yb0ynj45")))

(define-public crate-ozone-0.1.0 (c (n "ozone") (v "0.1.0") (d (list (d (n "memmap") (r "^0.5.0") (d #t) (k 0)) (d (n "rand") (r "^0.3.16") (d #t) (k 0)))) (h "12wb4ddakbqvzkfyxsp0l1avv8rvb641ykjmfvvyzxmd9g7m13a8")))

