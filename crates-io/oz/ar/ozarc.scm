(define-module (crates-io oz ar ozarc) #:use-module (crates-io))

(define-public crate-ozarc-0.0.0 (c (n "ozarc") (v "0.0.0") (d (list (d (n "deku") (r "^0.16.0") (f (quote ("logging"))) (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "004pg40jb2z0s3ppvsfkbjmdc9nh6gvj5yz5krc2k3w08kg1pl8y")))

