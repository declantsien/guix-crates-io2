(define-module (crates-io oz _m oz_merkle_rs) #:use-module (crates-io))

(define-public crate-oz_merkle_rs-0.1.0 (c (n "oz_merkle_rs") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "ethers") (r "^2.0") (d #t) (k 0)))) (h "0cyrkn3nz2scdbbmvfs8jwjjibr43vq2vrjch865apd4nsq68qnb")))

(define-public crate-oz_merkle_rs-0.1.1 (c (n "oz_merkle_rs") (v "0.1.1") (d (list (d (n "alloy-primitives") (r "^0.7.0") (d #t) (k 0)) (d (n "anyhow") (r "^1.0") (d #t) (k 0)))) (h "13hrshrckfksndynx981am0wz1iqpyyz0ai8lgqqlnldwdmr19rr")))

(define-public crate-oz_merkle_rs-0.1.2 (c (n "oz_merkle_rs") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "ethers") (r "^2.0") (d #t) (k 0)))) (h "0vz5v067srkxv0v5pkc02y0fql0bdqvnjxj29pl7dl7sks2hgx2q")))

(define-public crate-oz_merkle_rs-0.1.3 (c (n "oz_merkle_rs") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "ethers") (r "^2.0") (d #t) (k 0)))) (h "0yddd35pbdacwbgqkpkzvy3dwj0k6b8jqibd9igljci0vld3kz2l")))

