(define-module (crates-io d1 -p d1-pac) #:use-module (crates-io))

(define-public crate-d1-pac-0.0.1 (c (n "d1-pac") (v "0.0.1") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "riscv") (r "^0.7.0") (d #t) (k 0)) (d (n "riscv-rt") (r "^0.8.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0cwn9hxbp7hxk1kpzjhr36nj647hqccp8v39y2x186i9dzy4bqss") (f (quote (("rt" "riscv-rt"))))))

(define-public crate-d1-pac-0.0.2 (c (n "d1-pac") (v "0.0.2") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "riscv") (r "^0.7.0") (d #t) (k 0)) (d (n "riscv-rt") (r "^0.8.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "19la9nriv1r5piwin0m9246cbacgfqwm9msamdjrx09anq5l23ai") (f (quote (("rt" "riscv-rt"))))))

(define-public crate-d1-pac-0.0.3 (c (n "d1-pac") (v "0.0.3") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "riscv") (r "^0.7.0") (d #t) (k 0)) (d (n "riscv-rt") (r "^0.8.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "16d8qad1hg6h83vvn238lcnhdqn7v0al0kfg60pg7qjsm2k1p5pb") (f (quote (("rt" "riscv-rt"))))))

(define-public crate-d1-pac-0.0.4 (c (n "d1-pac") (v "0.0.4") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "riscv") (r "^0.7.0") (d #t) (k 0)) (d (n "riscv-rt") (r "^0.8.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "073bllc0y6h3c2lbn8nyjga6cvdm73iiqpa99z8yvppfzjrkznvw") (f (quote (("rt" "riscv-rt"))))))

(define-public crate-d1-pac-0.0.5 (c (n "d1-pac") (v "0.0.5") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "riscv") (r "^0.7.0") (d #t) (k 0)) (d (n "riscv-rt") (r "^0.8.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0snf5z248hp81j9ssplhv4l9vgkdwk404n6k8lz50m6laf5vgqn7") (f (quote (("rt" "riscv-rt"))))))

(define-public crate-d1-pac-0.0.6 (c (n "d1-pac") (v "0.0.6") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "riscv") (r "^0.7.0") (d #t) (k 0)) (d (n "riscv-rt") (r "^0.8.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1py2dcfzivp4zd0avqsla7y7r0xpjb02l5f9j7gqyd4mkyyxx9g7") (f (quote (("rt" "riscv-rt"))))))

(define-public crate-d1-pac-0.0.7 (c (n "d1-pac") (v "0.0.7") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "riscv") (r "^0.7.0") (d #t) (k 0)) (d (n "riscv-rt") (r "^0.8.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0454xqsfbmlf9n4apsxkbw5i66w4cai5wxgsi1vxhpib3bb063bg") (f (quote (("rt" "riscv-rt"))))))

(define-public crate-d1-pac-0.0.8 (c (n "d1-pac") (v "0.0.8") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "riscv") (r "^0.7.0") (d #t) (k 0)) (d (n "riscv-rt") (r "^0.8.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "13yimr7bvva8a0fqlrqhia2gn4qm0g1l49vh1j4qjis7swh57ac9") (f (quote (("rt" "riscv-rt"))))))

(define-public crate-d1-pac-0.0.9 (c (n "d1-pac") (v "0.0.9") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "riscv") (r "^0.7.0") (d #t) (k 0)) (d (n "riscv-rt") (r "^0.8.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0hdp2ccirqgyr0zznhl305v5wfj5j258s7l7gq55vc9i9pgjrgj9") (f (quote (("rt" "riscv-rt"))))))

(define-public crate-d1-pac-0.0.10 (c (n "d1-pac") (v "0.0.10") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "riscv") (r "^0.7.0") (d #t) (k 0)) (d (n "riscv-rt") (r "^0.8.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0ydy034h9hsjjwns3l021yfncz78kmxxxw0px9lkg1alw11wcag1") (f (quote (("rt" "riscv-rt"))))))

(define-public crate-d1-pac-0.0.11 (c (n "d1-pac") (v "0.0.11") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "riscv") (r "^0.7.0") (d #t) (k 0)) (d (n "riscv-rt") (r "^0.8.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0nq5k01vjkax8jciv74jlq2al9h7wl13bkhpa253ak4vvbsrln4i") (f (quote (("rt" "riscv-rt"))))))

(define-public crate-d1-pac-0.0.12 (c (n "d1-pac") (v "0.0.12") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "riscv") (r "^0.7.0") (d #t) (k 0)) (d (n "riscv-rt") (r "^0.8.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0wgyx10pllfnq6rhcpb3rnw0bw1jlh6a6v9jsijapij630bl5gwb") (f (quote (("rt" "riscv-rt"))))))

(define-public crate-d1-pac-0.0.13 (c (n "d1-pac") (v "0.0.13") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "riscv") (r "^0.7.0") (d #t) (k 0)) (d (n "riscv-rt") (r "^0.8.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "01gsyyx8cxjs0njy5whjlxgmnm1qhjis5bw3y6ysdlhknyc4n7lh") (f (quote (("rt" "riscv-rt"))))))

(define-public crate-d1-pac-0.0.14 (c (n "d1-pac") (v "0.0.14") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "riscv") (r "^0.7.0") (d #t) (k 0)) (d (n "riscv-rt") (r "^0.8.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1llk79scn0g6gb1n0dfv9ml40d4q5wc5xnfvahjzv58ij6w2730a") (f (quote (("rt" "riscv-rt"))))))

(define-public crate-d1-pac-0.0.15 (c (n "d1-pac") (v "0.0.15") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "riscv") (r "^0.7.0") (d #t) (k 0)) (d (n "riscv-rt") (r "^0.8.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "12sy8chkqzfwp05fmjvv9c9ifi43yd689ba6s014bmlrwp54ib4f") (f (quote (("rt" "riscv-rt"))))))

(define-public crate-d1-pac-0.0.16 (c (n "d1-pac") (v "0.0.16") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "riscv") (r "^0.7.0") (d #t) (k 0)) (d (n "riscv-rt") (r "^0.8.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1q8i3wzq4f8syqsgb623wk7ws2k75sddn55s917zjfbqyzyzybx3") (f (quote (("rt" "riscv-rt"))))))

(define-public crate-d1-pac-0.0.17 (c (n "d1-pac") (v "0.0.17") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "riscv") (r "^0.7.0") (d #t) (k 0)) (d (n "riscv-rt") (r "^0.8.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0r062pnalpp6rjvflamf7hdbmby7kynfg8x6m57q3hfqcw4rgdlh") (f (quote (("rt" "riscv-rt"))))))

(define-public crate-d1-pac-0.0.18 (c (n "d1-pac") (v "0.0.18") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "riscv") (r "^0.8.0") (d #t) (k 0)) (d (n "riscv-rt") (r "^0.8.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1lwjyfpc3k6fv08ksd4dy7vpw260i2lbng3bvpy08hn6x3lsb8qf") (f (quote (("rt" "riscv-rt"))))))

(define-public crate-d1-pac-0.0.19 (c (n "d1-pac") (v "0.0.19") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "riscv") (r "^0.8.0") (d #t) (k 0)) (d (n "riscv-rt") (r "^0.8.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "10gj6wvr4dzzr0zy2fv6mnxx1fw8c88582zbfv1gs42kydf7kkmw") (f (quote (("rt" "riscv-rt"))))))

(define-public crate-d1-pac-0.0.20 (c (n "d1-pac") (v "0.0.20") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "riscv") (r "^0.8.0") (d #t) (k 0)) (d (n "riscv-rt") (r "^0.8.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1avvk0iciphxppmfh5rlmzr8q75ckxqr9jql61hfnx6hpfb8q6h8") (f (quote (("rt" "riscv-rt"))))))

(define-public crate-d1-pac-0.0.21 (c (n "d1-pac") (v "0.0.21") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "riscv") (r "^0.8.0") (d #t) (k 0)) (d (n "riscv-rt") (r "^0.8.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "16la02mbzvcin13hiwv5b0hvgpc3k76qfdrwvsk4xip1yf78q88p") (f (quote (("rt" "riscv-rt"))))))

(define-public crate-d1-pac-0.0.22 (c (n "d1-pac") (v "0.0.22") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "riscv") (r "^0.8.0") (d #t) (k 0)) (d (n "riscv-rt") (r "^0.8.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "13dglwrc2szrcw3rg3w2ppl98s884l3sagp8bzwv8p1jwaxj6g1h") (f (quote (("rt" "riscv-rt"))))))

(define-public crate-d1-pac-0.0.23 (c (n "d1-pac") (v "0.0.23") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "riscv") (r "^0.8.0") (d #t) (k 0)) (d (n "riscv-rt") (r "^0.8.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1pvf7n315c4m877pkfm4n7xf4m06ranrs05pjzmnl3b9p69mri29") (f (quote (("rt" "riscv-rt"))))))

(define-public crate-d1-pac-0.0.24 (c (n "d1-pac") (v "0.0.24") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "riscv") (r "^0.8.0") (d #t) (k 0)) (d (n "riscv-rt") (r "^0.9.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0nqlwg349326gbkvddw5mj28ga0wzh57mirgb19rraz09pmpx1jf") (f (quote (("rt" "riscv-rt"))))))

(define-public crate-d1-pac-0.0.25 (c (n "d1-pac") (v "0.0.25") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "riscv") (r "^0.8.0") (d #t) (k 0)) (d (n "riscv-rt") (r "^0.9.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0bqr5qav033w46d6yh8ym5hzs2hy6ywz083s24n1xmls7mjc6k51") (f (quote (("rt" "riscv-rt"))))))

(define-public crate-d1-pac-0.0.26 (c (n "d1-pac") (v "0.0.26") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "riscv") (r "^0.8.0") (d #t) (k 0)) (d (n "riscv-rt") (r "^0.9.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1x17cx71vns199wdlw887f4m4s2d6yh9j9jyc4vz8ywka3j4xr4h") (f (quote (("rt" "riscv-rt"))))))

(define-public crate-d1-pac-0.0.27 (c (n "d1-pac") (v "0.0.27") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "riscv") (r "^0.8.0") (d #t) (k 0)) (d (n "riscv-rt") (r "^0.9.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "05vn1hsp1hmpq4r4sfi165n3y4ncxhvax7lbzmzwgp27rjil86p1") (f (quote (("rt" "riscv-rt"))))))

(define-public crate-d1-pac-0.0.28 (c (n "d1-pac") (v "0.0.28") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "riscv") (r "^0.9.0") (d #t) (k 0)) (d (n "riscv-rt") (r "^0.9.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1vzd5q1s2kjjgcr5jcfnbc4jnlwjsa24vkldyxv9h1wzwciidg09") (f (quote (("rt" "riscv-rt"))))))

(define-public crate-d1-pac-0.0.29 (c (n "d1-pac") (v "0.0.29") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "riscv") (r "^0.9.0") (d #t) (k 0)) (d (n "riscv-rt") (r "^0.9.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0bp6fiqhais4sm6sz4pq3jj0jwwwqgn9bndlr16m3wihn8p1l1g3") (f (quote (("rt" "riscv-rt"))))))

(define-public crate-d1-pac-0.0.30 (c (n "d1-pac") (v "0.0.30") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "critical-section") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "riscv") (r "^0.9.0") (d #t) (k 0)) (d (n "riscv-rt") (r "^0.9.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1w005jx8y8jng8hx262dhnsvw7c8p71c1ig52q25phapdkrfrx50") (f (quote (("rt" "riscv-rt"))))))

(define-public crate-d1-pac-0.0.31 (c (n "d1-pac") (v "0.0.31") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "critical-section") (r "^1.1") (o #t) (d #t) (k 0)) (d (n "riscv") (r "^0.10.1") (d #t) (k 0)) (d (n "riscv-rt") (r "^0.11.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1sqccca495sdz7xhmvx6achl3qjbpdhg50g62sl2gnyiy2df0fz0") (f (quote (("rt" "riscv-rt"))))))

(define-public crate-d1-pac-0.0.32 (c (n "d1-pac") (v "0.0.32") (d (list (d (n "critical-section") (r "^1.1") (o #t) (d #t) (k 0)) (d (n "riscv") (r "^0.10.1") (d #t) (k 0)) (d (n "riscv-rt") (r "^0.11.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "06r1zf9rl75iqmldync3iyc1zf1lrs7sc4qvidswzznij7z8j787") (f (quote (("rt" "riscv-rt"))))))

