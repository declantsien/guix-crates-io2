(define-module (crates-io d1 -m d1-mini) #:use-module (crates-io))

(define-public crate-d1-mini-0.1.0 (c (n "d1-mini") (v "0.1.0") (d (list (d (n "esp8266-hal") (r "^0.2.0") (d #t) (k 0)) (d (n "nb") (r "^0.1.2") (d #t) (k 2)) (d (n "panic-halt") (r "^0.2.0") (d #t) (k 2)) (d (n "paste") (r "^1.0.0") (d #t) (k 0)))) (h "0hayvsl9s3182yhmjw03m4ngr7l6nbigxaq9rdzsswn9j80gczf8")))

(define-public crate-d1-mini-0.2.0 (c (n "d1-mini") (v "0.2.0") (d (list (d (n "bitbang-hal") (r "^0.3") (d #t) (k 0)) (d (n "esp8266-hal") (r "^0.4") (d #t) (k 0)) (d (n "panic-halt") (r "^0.2") (d #t) (k 2)) (d (n "paste") (r "^1.0") (d #t) (k 0)))) (h "0v4vcpjwwdx1xsmlrhs2fap5fh20wjjbz9afkja9s8m7s4gsvly2")))

