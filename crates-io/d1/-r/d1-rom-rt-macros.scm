(define-module (crates-io d1 -r d1-rom-rt-macros) #:use-module (crates-io))

(define-public crate-d1-rom-rt-macros-0.0.0 (c (n "d1-rom-rt-macros") (v "0.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "191hras3w7fmw5a623wwr6436aa9fq0p7w1rqd2yrn2y12s1lhf4")))

