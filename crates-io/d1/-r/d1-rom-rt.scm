(define-module (crates-io d1 -r d1-rom-rt) #:use-module (crates-io))

(define-public crate-d1-rom-rt-0.0.0 (c (n "d1-rom-rt") (v "0.0.0") (d (list (d (n "aw-soc") (r "^0.0.0") (f (quote ("d1"))) (d #t) (k 0)) (d (n "base-address") (r "^0.0.0") (d #t) (k 0)) (d (n "d1-rom-rt-macros") (r "^0.0.0") (d #t) (k 0)) (d (n "embedded-hal") (r "^1.0.0-alpha.9") (d #t) (k 0)) (d (n "nb") (r "^1.1.0") (d #t) (k 0)) (d (n "spin") (r "^0.9.5") (o #t) (d #t) (k 0)) (d (n "ufmt") (r "^0.2.0") (o #t) (d #t) (k 0)))) (h "0i40fl746blmz1s71xka0kwcp18l8d5iz91g9symv3x2207jwl9z") (f (quote (("nezha") ("lichee") ("default" "nezha")))) (s 2) (e (quote (("log" "dep:spin" "dep:ufmt"))))))

