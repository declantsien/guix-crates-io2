(define-module (crates-io zd um zdump) #:use-module (crates-io))

(define-public crate-zdump-0.1.0 (c (n "zdump") (v "0.1.0") (d (list (d (n "structopt") (r "^0.3.2") (k 0)) (d (n "tzparse") (r "^0.3.4") (d #t) (k 0)))) (h "04alarx7ilbc29qz6jbizy5lqnh01f992ix1g5hss4hybav79yl9")))

(define-public crate-zdump-0.1.1 (c (n "zdump") (v "0.1.1") (d (list (d (n "structopt") (r "^0.3.2") (k 0)) (d (n "tzparse") (r "^0.3.4") (d #t) (k 0)))) (h "1dsykjfdq4jq3zzn3rh6nxwladwi65k20d0gyimnavf327x4n75r")))

(define-public crate-zdump-0.1.2 (c (n "zdump") (v "0.1.2") (d (list (d (n "structopt") (r "^0.3.2") (k 0)) (d (n "tzparse") (r "^0.3.5") (d #t) (k 0)))) (h "12s5slk1lwak34pvmxpx91k4p91skz7a0izls8941z2xycphq7df")))

(define-public crate-zdump-0.2.0 (c (n "zdump") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3.2") (k 0)) (d (n "tzparse") (r "^0.4.0") (d #t) (k 0)))) (h "1alfpkvphg94ji8j9z1rmdipxg62cv7ki0fr9d8mdv9g1cja39fv")))

(define-public crate-zdump-0.2.2 (c (n "zdump") (v "0.2.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3.2") (k 0)) (d (n "tzparse") (r "^0.5.0") (d #t) (k 0)))) (h "0da0hgyi6zqkjrbl6c4aclrfw9qzbfakr43jgil0014gd21sczzf")))

(define-public crate-zdump-0.2.4 (c (n "zdump") (v "0.2.4") (d (list (d (n "structopt") (r "^0.3.2") (k 0)) (d (n "tzparse") (r "^0.5.1") (d #t) (k 0)))) (h "0pd9c2qnwww0zmmz1sr3qrbnp2i3s0fq620xw9b2s7m5ij3x9mgh")))

(define-public crate-zdump-0.2.5 (c (n "zdump") (v "0.2.5") (d (list (d (n "structopt") (r "^0.3.2") (k 0)) (d (n "tzparse") (r "^0.5.2") (d #t) (k 0)))) (h "1fqz6qdcyazg07mmkrbfv8vr3rlciygrkhpjh4zq9kljf6d272d8")))

(define-public crate-zdump-1.0.0 (c (n "zdump") (v "1.0.0") (d (list (d (n "libtzfile") (r "^1.0.5") (d #t) (k 0)) (d (n "structopt") (r "^0.3.2") (k 0)) (d (n "tzparse") (r "^1.0.1") (d #t) (k 0)))) (h "1iyf4x671c9hg95ng5z5ssjf4cbxvmrxpvkjp3zd5fzbvxaviirv")))

(define-public crate-zdump-1.0.1 (c (n "zdump") (v "1.0.1") (d (list (d (n "libtzfile") (r "^1.0.6") (d #t) (k 0)) (d (n "structopt") (r "^0.3.2") (k 0)) (d (n "tzparse") (r "^1.0.2") (d #t) (k 0)))) (h "0qsf0v78df3xq6si4l9mxr600z91f9av59md9ikn4vf0kw78schz")))

(define-public crate-zdump-1.0.2 (c (n "zdump") (v "1.0.2") (d (list (d (n "structopt") (r "^0.3.2") (k 0)) (d (n "tzparse") (r "^1.0.3") (d #t) (k 0)))) (h "1gyibq0mm2w7j30crs5608g4gzwzjl8zisnvhplrai90sm87fjj3")))

(define-public crate-zdump-1.1.0 (c (n "zdump") (v "1.1.0") (d (list (d (n "tzparse") (r "^1.1") (d #t) (k 0)))) (h "14hgj1p9a1nb2z6s5nf5c19h71hrpi0jxkclzq07mx35m2w67kba")))

(define-public crate-zdump-1.2.0 (c (n "zdump") (v "1.2.0") (d (list (d (n "tzparse") (r "^1.1") (d #t) (k 0)))) (h "02hhw3zdf6vfj76kb7sib87lczhbm0xiy2qlq64nfhw4m4324qjl")))

(define-public crate-zdump-1.2.1 (c (n "zdump") (v "1.2.1") (d (list (d (n "tzparse") (r "^1.1") (d #t) (k 0)))) (h "0xgjzyz2ficgrmjcfk7w5x3spdag4i87ka2qrqncapi2wffvh73z")))

(define-public crate-zdump-1.2.2 (c (n "zdump") (v "1.2.2") (d (list (d (n "tzparse") (r "^1.1") (d #t) (k 0)))) (h "1jfgih95qsi11p98220hbjfl7kfbkc6gc1wc0v1m4130maf59sv3")))

(define-public crate-zdump-1.2.3 (c (n "zdump") (v "1.2.3") (d (list (d (n "libtzfile") (r "^2.0") (f (quote ("parse"))) (d #t) (k 0)))) (h "0plbm5glpl5660hygldgbi2zvaw9v7sv14gm46gi9ca32m86sl51")))

(define-public crate-zdump-1.3.0 (c (n "zdump") (v "1.3.0") (d (list (d (n "libtzfile") (r "^2.0") (f (quote ("parse"))) (d #t) (k 0)))) (h "0ffg42d4yq4iyh5x7wc8pfz0wrsc9707bl30ib9dxn7by0z6n79n")))

(define-public crate-zdump-1.3.1 (c (n "zdump") (v "1.3.1") (d (list (d (n "libtzfile") (r "^2.0") (f (quote ("parse"))) (d #t) (k 0)))) (h "02342w6alwinz9kpngwz8kvjy88dhgxsf88r4mhsl46m7by7mywz")))

(define-public crate-zdump-1.3.2 (c (n "zdump") (v "1.3.2") (d (list (d (n "libtzfile") (r "^2.0") (f (quote ("parse"))) (d #t) (k 0)))) (h "1g5wjyhahf8nd0zymhjd736gyawlkxidczmq4cf6nyp3ygkkqv6s")))

(define-public crate-zdump-1.3.3 (c (n "zdump") (v "1.3.3") (d (list (d (n "libtzfile") (r "^2.0") (f (quote ("parse"))) (d #t) (k 0)))) (h "1zvafrrmn1r51ahjxlr3ym63m196c2gy5hv5hswy83z6dgr640im")))

(define-public crate-zdump-1.3.4 (c (n "zdump") (v "1.3.4") (d (list (d (n "libtzfile") (r "^2.0") (f (quote ("parse"))) (d #t) (k 0)))) (h "05jgbg28hv21hc60zkvbc7cyy3g7kv55ypjcp01wpkhzwz53aj0k")))

(define-public crate-zdump-1.3.5 (c (n "zdump") (v "1.3.5") (d (list (d (n "libtzfile") (r "^3.0") (f (quote ("parse"))) (d #t) (k 0)))) (h "04pndwcx512ik7kdi10rny5myfx9dz1rvb1b3ngjvmymsms9ymjc")))

(define-public crate-zdump-1.3.6 (c (n "zdump") (v "1.3.6") (d (list (d (n "libtzfile") (r "^3") (f (quote ("parse"))) (d #t) (k 0)))) (h "1icvs1s4r860xdqc9vlnmq7msskqklfgyjyia0hhy12y792jkb2s")))

