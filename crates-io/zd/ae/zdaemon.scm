(define-module (crates-io zd ae zdaemon) #:use-module (crates-io))

(define-public crate-zdaemon-0.0.2 (c (n "zdaemon") (v "0.0.2") (d (list (d (n "czmq") (r "^0.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "0.3.*") (d #t) (k 0)) (d (n "tempdir") (r "0.3.*") (d #t) (k 2)))) (h "0fdakdarkzkh3sks7mzyvhrxfmclklji3bh4b2zpcax90661qkav") (y #t)))

