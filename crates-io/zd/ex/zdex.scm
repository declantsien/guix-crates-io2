(define-module (crates-io zd ex zdex) #:use-module (crates-io))

(define-public crate-zdex-0.1.0 (c (n "zdex") (v "0.1.0") (d (list (d (n "bit_collection") (r "^0.2.3") (d #t) (k 0)) (d (n "vob") (r "^2.0.4") (d #t) (k 0)))) (h "1y8faqikxl9ni8a7hd68k3blfmbgp6nh3pwmisrpjjd8pshhm943")))

(define-public crate-zdex-0.2.0 (c (n "zdex") (v "0.2.0") (d (list (d (n "bit_collection") (r "^0.2.3") (d #t) (k 0)) (d (n "vob") (r "^2.0.4") (d #t) (k 0)))) (h "15q9myznxkhjsf4fxx7fywp9zdikwjm7snmpnh4fqjk3n1a3g39n")))

(define-public crate-zdex-0.3.0 (c (n "zdex") (v "0.3.0") (d (list (d (n "bit_collection") (r "^0.2.3") (d #t) (k 0)) (d (n "vob") (r "^2.0.4") (d #t) (k 0)))) (h "0rf4ww84c8whkz8rzbphj56i6nppc1yakkjikvrb9sr1s3xhs0vk")))

(define-public crate-zdex-0.3.1 (c (n "zdex") (v "0.3.1") (d (list (d (n "bit_collection") (r "^0.2.3") (d #t) (k 0)) (d (n "vob") (r "^2.0.4") (d #t) (k 0)))) (h "16gjsn3wqgp1h896kh28r9ayncfp7xhm8zbrn67r9wb5xmry6n9a")))

