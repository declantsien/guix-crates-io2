(define-module (crates-io dc od dcode) #:use-module (crates-io))

(define-public crate-dcode-0.0.2 (c (n "dcode") (v "0.0.2") (h "0f59pnhk73jhxbhicdkhnwjscxmknfsl96mms7dgh0krjxifzk7f")))

(define-public crate-dcode-0.0.3 (c (n "dcode") (v "0.0.3") (h "1yh8748gzm2i7ag1ip1lqqavmagg0lhk6ha2qlvsn3fmhkr6wfjq")))

(define-public crate-dcode-0.0.4 (c (n "dcode") (v "0.0.4") (h "0xrah5qida5n3mrv2jhichqv3nwy0fp1cwwmpifbfy86vnwvw8kb")))

