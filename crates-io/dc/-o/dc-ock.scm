(define-module (crates-io dc -o dc-ock) #:use-module (crates-io))

(define-public crate-dc-ock-0.1.0 (c (n "dc-ock") (v "0.1.0") (d (list (d (n "config") (r "^0.13.1") (d #t) (k 0)) (d (n "directories") (r "^4.0.1") (d #t) (k 0)))) (h "1y1jbhpzfrcxk21kn1z1zrafbqn6b4yx4sj20bmgy8c1d6q9n14i") (y #t)))

(define-public crate-dc-ock-0.1.1 (c (n "dc-ock") (v "0.1.1") (d (list (d (n "config") (r "^0.13.1") (d #t) (k 0)) (d (n "directories") (r "^4.0.1") (d #t) (k 0)))) (h "0ka2aj4b3hsmlw5021mpj9m0xz0hpyjv55vpgv0x0xq5zwla8qws") (y #t)))

(define-public crate-dc-ock-0.1.2 (c (n "dc-ock") (v "0.1.2") (d (list (d (n "config") (r "^0.13.1") (d #t) (k 0)) (d (n "directories") (r "^4.0.1") (d #t) (k 0)))) (h "0ld79hwg168xw0ps0554rlvwdhyvb343grgg7i957f050fhylvvg") (y #t)))

(define-public crate-dc-ock-0.2.0 (c (n "dc-ock") (v "0.2.0") (d (list (d (n "config") (r "^0.13.1") (d #t) (k 0)) (d (n "directories") (r "^4.0.1") (d #t) (k 0)))) (h "10i0wyn9v7v5mqbrxamvhh5cgjql7qv4ggfb4j2r6i64a3fdnmxy") (y #t)))

(define-public crate-dc-ock-0.3.0 (c (n "dc-ock") (v "0.3.0") (d (list (d (n "config") (r "^0.13.1") (d #t) (k 0)) (d (n "directories") (r "^4.0.1") (d #t) (k 0)))) (h "05ji8m3vdk5g3z87sdba8vawmkmwi516xpnlqn23299d1ad4s16p") (y #t)))

(define-public crate-dc-ock-1.0.0 (c (n "dc-ock") (v "1.0.0") (d (list (d (n "config") (r "^0.13.1") (o #t) (d #t) (k 0)) (d (n "directories") (r "^4.0.1") (o #t) (d #t) (k 0)))) (h "0mknpd7npxjwk4ph5g0rkbv60x68p4gcgbds2kmq3sz3bnjgh7zc") (f (quote (("bbin" "config" "directories")))) (y #t)))

(define-public crate-dc-ock-2.0.0 (c (n "dc-ock") (v "2.0.0") (h "0xslxqc59hfazy5jk1gsb2wfqn3dk6anv6jwc746xwcyrwz3n4yz")))

(define-public crate-dc-ock-2.0.1 (c (n "dc-ock") (v "2.0.1") (h "02hc00slk8nlyfic0xxl5vd7r6n9vabd2a5p24c74j6wjpsbm71r")))

