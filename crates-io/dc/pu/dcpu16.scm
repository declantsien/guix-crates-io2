(define-module (crates-io dc pu dcpu16) #:use-module (crates-io))

(define-public crate-dcpu16-0.0.1 (c (n "dcpu16") (v "0.0.1") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)))) (h "032cxhpdlb0zyl5m8v2cmrlmy3mqq5v9fh4607xdyfhf5qvlcpyn")))

(define-public crate-dcpu16-0.0.2 (c (n "dcpu16") (v "0.0.2") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)))) (h "1pj9zyk8kq01hc2vd4parc7aaicxggcqvhwq9a3d1p513lzcnbjm")))

(define-public crate-dcpu16-0.0.3 (c (n "dcpu16") (v "0.0.3") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)))) (h "14qpzj0z3hnjwbrlp32w6h42bdw1ydrlwz6v366ily5zacbwimnr")))

(define-public crate-dcpu16-0.0.4 (c (n "dcpu16") (v "0.0.4") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)))) (h "1m1dhq9bfl0k5aphdapspgvnx41mcg2nbs9jp97bvkjs55zph7lc")))

(define-public crate-dcpu16-0.0.5 (c (n "dcpu16") (v "0.0.5") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)))) (h "192vy7cg7zn67rrwm7b6yfzbf24ywhi666c6rxi64fhm6vwpx1wx")))

(define-public crate-dcpu16-0.0.6 (c (n "dcpu16") (v "0.0.6") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)))) (h "046nxgsgjmysy5fzkc6allcxzm3038mbz66548l9licyavk038m2")))

(define-public crate-dcpu16-0.0.7 (c (n "dcpu16") (v "0.0.7") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)))) (h "00ljrhz8ymf0y421jd8s9mk9d7m1cjnxz6ygm1qcfrgw9klas6fq")))

(define-public crate-dcpu16-0.1.0 (c (n "dcpu16") (v "0.1.0") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)))) (h "1bv9sv63k96dpci9w6vk16f17bz8gj7vffm5dkaygy2w0dch24nk")))

(define-public crate-dcpu16-0.2.0 (c (n "dcpu16") (v "0.2.0") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)))) (h "0sgs5bgq102kxv8slyklw5nzx29nga1kdbh85zjcpn9ln3s6rgjh")))

(define-public crate-dcpu16-0.3.0 (c (n "dcpu16") (v "0.3.0") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)))) (h "1md4bb1vbfww7pn0vfzfwirlgip2pdjil444dh8ivcypa860q3am")))

(define-public crate-dcpu16-0.4.0 (c (n "dcpu16") (v "0.4.0") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)))) (h "0ayqyxv1r7lgbgkj59nc9wz2csqf9hhdmnvxczhiv7bhy2phi5h4")))

