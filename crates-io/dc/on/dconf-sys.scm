(define-module (crates-io dc on dconf-sys) #:use-module (crates-io))

(define-public crate-dconf-sys-0.1.0 (c (n "dconf-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)) (d (n "glib") (r "^0.19.5") (d #t) (k 0)) (d (n "glib-sys") (r "^0.19.5") (d #t) (k 0)) (d (n "libc") (r "^0.2.154") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.30") (d #t) (k 1)))) (h "0vjpykwhhxgmgxmd9iig3nk6zc3l27qvdb5z53d77dwlhn6j7wd3")))

