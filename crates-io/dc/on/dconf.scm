(define-module (crates-io dc on dconf) #:use-module (crates-io))

(define-public crate-dconf-0.1.0 (c (n "dconf") (v "0.1.0") (d (list (d (n "dconf-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "glib") (r "^0.19.5") (d #t) (k 0)) (d (n "glib-sys") (r "^0.19.5") (d #t) (k 0)) (d (n "gobject-sys") (r "^0.19.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.59") (d #t) (k 0)))) (h "1nr62lxmin37gq1i5fks9v33lj2abwlnwi6r1cm17qyi0y081v4y")))

(define-public crate-dconf-0.1.1 (c (n "dconf") (v "0.1.1") (d (list (d (n "dconf-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "glib") (r "^0.19.5") (d #t) (k 0)) (d (n "glib-sys") (r "^0.19.5") (d #t) (k 0)) (d (n "gobject-sys") (r "^0.19.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.59") (d #t) (k 0)))) (h "0h410w06ppg1c6yqbcki5dhlkxgnk4fwkqgq4ym1xcqk8afisxys")))

