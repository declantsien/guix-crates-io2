(define-module (crates-io dc on dconf_rs) #:use-module (crates-io))

(define-public crate-dconf_rs-0.1.0 (c (n "dconf_rs") (v "0.1.0") (h "1268zb9bilxq662l8xzzm3kil9x6z8sp4wlkqycdrq2ap2yh0gf8")))

(define-public crate-dconf_rs-0.2.0 (c (n "dconf_rs") (v "0.2.0") (h "0rpnqxkjg7ak5kkpli9ny7jlpra9c4p6hccfril9varc51470kzs")))

(define-public crate-dconf_rs-0.2.1 (c (n "dconf_rs") (v "0.2.1") (h "1cq9ylr8q9a9kmx478lcpjwxq3ycg5kbhk2pv0q90i4vpw7fhmch")))

(define-public crate-dconf_rs-0.3.0 (c (n "dconf_rs") (v "0.3.0") (h "12swi0npq88kbdwnc3n324dzknir674agrh13h305876h654cikh")))

