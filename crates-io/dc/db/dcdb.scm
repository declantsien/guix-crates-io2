(define-module (crates-io dc db dcdb) #:use-module (crates-io))

(define-public crate-dcdb-0.1.0 (c (n "dcdb") (v "0.1.0") (h "1vw0p33dw3b20wbqzkb3mm7j7n3vhakdwvi5dq14sckwrxhk9rxx")))

(define-public crate-dcdb-0.1.1 (c (n "dcdb") (v "0.1.1") (h "1f2rvja9sb7j2jb0s1zxbl8i0k7s68i8hir9xxdqmgmjvz7y8y4i")))

(define-public crate-dcdb-0.1.2 (c (n "dcdb") (v "0.1.2") (h "1sjyfqn4z4q2d9as523micrcrf4lc81izn19sb9q6mmvyy9kn24l")))

