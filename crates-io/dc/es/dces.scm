(define-module (crates-io dc es dces) #:use-module (crates-io))

(define-public crate-dces-0.1.0 (c (n "dces") (v "0.1.0") (h "02jh38m3zlgvrhi5jx82yjvy8hdvki11748kigz89kq7sr706s42")))

(define-public crate-dces-0.1.1 (c (n "dces") (v "0.1.1") (h "1fymfn2iqv9nf3kjzvyz7418zq0zw2gch7bh2zkwsf7flary0a1y")))

(define-public crate-dces-0.1.2 (c (n "dces") (v "0.1.2") (h "0nkm3w0c3ny94qzw2i1qvjxgxpbc27ghmkahwxyr34lxqj7r9k8r")))

(define-public crate-dces-0.1.3 (c (n "dces") (v "0.1.3") (h "07allwan1mg5xqycx5pyph5rcsvhfnn55inz3ndl31akaym7ymg9")))

(define-public crate-dces-0.1.4 (c (n "dces") (v "0.1.4") (h "0x4mnvcf5bhflpfqppk355yjwikczgss0bifn35r42z4xa1844j4")))

(define-public crate-dces-0.1.5 (c (n "dces") (v "0.1.5") (h "015yzb23nh9zwb3xqni9pfddqqw1wmcna7bjwka2iv1mjm9lzrz0")))

(define-public crate-dces-0.1.6 (c (n "dces") (v "0.1.6") (h "0xzjphvz9imc8b18707gkrgxxrf5krvj8fmm6zp31rkzy47gpxsc")))

(define-public crate-dces-0.2.0 (c (n "dces") (v "0.2.0") (h "01nqapifv0vx8r58xngk7saync7hyi6lxzx12qf0cz0zrr4c5626") (f (quote (("no_std") ("default"))))))

(define-public crate-dces-0.3.0 (c (n "dces") (v "0.3.0") (h "1xvpn80krsy43c1dnwgxbkaa2aybbbk3v7rcrx303y6xk94jw7xa") (f (quote (("no_std") ("default"))))))

(define-public crate-dces-0.3.1 (c (n "dces") (v "0.3.1") (d (list (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)))) (h "17dyyi1xg1pa1lvbjdcvisfgqwfh23mwg1grljcnrnkshpcqd88g") (f (quote (("no_std") ("default"))))))

