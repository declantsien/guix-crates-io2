(define-module (crates-io dc ss dcss-api) #:use-module (crates-io))

(define-public crate-dcss-api-0.1.0 (c (n "dcss-api") (v "0.1.0") (d (list (d (n "flate2") (r "^1.0") (f (quote ("zlib-ng"))) (k 0)) (d (n "pyo3") (r "^0.18") (f (quote ("extension-module"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tungstenite") (r "^0.19") (f (quote ("native-tls"))) (d #t) (k 0)) (d (n "url") (r "^2.3") (d #t) (k 0)))) (h "15faa3ihpal79pv428s9bwfh0m2n644ibkflhhpzzdlbr2id1n1y")))

(define-public crate-dcss-api-0.1.1 (c (n "dcss-api") (v "0.1.1") (d (list (d (n "flate2") (r "^1.0") (f (quote ("zlib"))) (k 0)) (d (n "pyo3") (r "^0.18") (f (quote ("extension-module"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tungstenite") (r "^0.19") (f (quote ("native-tls-vendored"))) (d #t) (k 0)) (d (n "url") (r "^2.3") (d #t) (k 0)))) (h "16ivszibz7bgbzlj8n4ya0nc9dl4b341yi0xa1vd6kxnz8fravf8")))

(define-public crate-dcss-api-0.1.3 (c (n "dcss-api") (v "0.1.3") (d (list (d (n "flate2") (r "^1.0") (f (quote ("zlib"))) (k 0)) (d (n "pyo3") (r "^0.19") (f (quote ("extension-module"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tungstenite") (r "^0.20") (f (quote ("native-tls-vendored"))) (d #t) (k 0)) (d (n "url") (r "^2.3") (d #t) (k 0)))) (h "1hwsa4sd6v3jviibn0vmyyh6wri3mgf79z64mkw67k1jlj21n6bl")))

(define-public crate-dcss-api-0.1.4 (c (n "dcss-api") (v "0.1.4") (d (list (d (n "flate2") (r "^1.0") (f (quote ("zlib"))) (k 0)) (d (n "pyo3") (r "^0.20") (f (quote ("extension-module"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tungstenite") (r "^0.20") (f (quote ("native-tls-vendored"))) (d #t) (k 0)) (d (n "url") (r "^2.3") (d #t) (k 0)))) (h "02flf47dixvyi6gsixp84p553iyy4xfql1i74l0iidr3gh2ycpxm")))

