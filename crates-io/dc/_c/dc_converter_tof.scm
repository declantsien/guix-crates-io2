(define-module (crates-io dc _c dc_converter_tof) #:use-module (crates-io))

(define-public crate-dc_converter_tof-0.1.0 (c (n "dc_converter_tof") (v "0.1.0") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "opencv") (r "=0.83.0") (d #t) (k 0)))) (h "1q0zg1ac7vwsyhwqlwirxajj0gwpdwz2y4xvxay9y7sc6783kwj5") (r "1.72")))

(define-public crate-dc_converter_tof-0.1.1 (c (n "dc_converter_tof") (v "0.1.1") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "opencv") (r "=0.83.0") (d #t) (k 0)))) (h "1kjbd0p74i16hyaxpa291lr50z80q9934g2f8rmvm38qaf69wqgd") (r "1.72")))

(define-public crate-dc_converter_tof-0.1.2 (c (n "dc_converter_tof") (v "0.1.2") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "opencv") (r "=0.81.5") (d #t) (k 0)))) (h "1kli2kkrcp40rjb597920570jicvml500bp9xfw1jhfkym510hva") (r "1.72")))

(define-public crate-dc_converter_tof-0.1.3 (c (n "dc_converter_tof") (v "0.1.3") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "opencv") (r "=0.84.4") (f (quote ("imgproc" "imgcodecs"))) (k 0)))) (h "0an61b3z1cpm2bi926p41dijnd9gfw39wfj7n85p61w37phlaf31") (r "1.72")))

(define-public crate-dc_converter_tof-0.1.4 (c (n "dc_converter_tof") (v "0.1.4") (d (list (d (n "clap") (r "^4.4.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "opencv") (r "=0.84.5") (f (quote ("imgproc" "imgcodecs"))) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.4.0") (d #t) (k 0)))) (h "1jccw3plxkwb4a1mhzidlilnxjvibqi5rbpgac6k05x7vxn1kz64") (r "1.72")))

(define-public crate-dc_converter_tof-0.1.5 (c (n "dc_converter_tof") (v "0.1.5") (d (list (d (n "clap") (r "^4.4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "opencv") (r "=0.84.5") (f (quote ("imgproc" "imgcodecs"))) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.4.0") (d #t) (k 0)))) (h "0lapfgqa4hz4dm2104g0z3lp2vnxb9hld6jm9jydxafasvl6cy7x") (r "1.72")))

(define-public crate-dc_converter_tof-0.1.6 (c (n "dc_converter_tof") (v "0.1.6") (d (list (d (n "clap") (r "^4.4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "opencv") (r "=0.84.5") (f (quote ("imgproc" "imgcodecs"))) (k 0)) (d (n "rayon") (r "^1.8.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.4.0") (d #t) (k 0)))) (h "1pap551nbh8lbdcknr5j4csdv8yk6zp0wwzkqpsqji0g76vbbbx9") (r "1.72")))

(define-public crate-dc_converter_tof-0.1.7 (c (n "dc_converter_tof") (v "0.1.7") (d (list (d (n "clap") (r "^4.4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "opencv") (r "=0.84.5") (f (quote ("imgproc" "imgcodecs"))) (k 0)) (d (n "rayon") (r "^1.8.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.4.0") (d #t) (k 0)))) (h "0zdmp7d344pw80ibyp6slazpg2v3p6nka89rhprnp0yilm824fnq") (r "1.72")))

(define-public crate-dc_converter_tof-0.1.8 (c (n "dc_converter_tof") (v "0.1.8") (d (list (d (n "clap") (r "^4.4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "opencv") (r "=0.84.5") (f (quote ("imgproc" "imgcodecs"))) (k 0)) (d (n "rayon") (r "^1.8.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.4.0") (d #t) (k 0)))) (h "1kp1irnww76dax7661di21gv4rdc6f6i7xlgqmxlbp89raag0i0x") (r "1.72")))

(define-public crate-dc_converter_tof-0.1.10 (c (n "dc_converter_tof") (v "0.1.10") (d (list (d (n "clap") (r "^4.4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "opencv") (r "=0.84.5") (f (quote ("imgproc" "imgcodecs"))) (k 0)) (d (n "rayon") (r "^1.8.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.4.0") (d #t) (k 0)))) (h "1zb9n0r9qb9iampjh9prg0drz7dmgr9jxksi13gnhk0zgbs1xl88") (r "1.72")))

(define-public crate-dc_converter_tof-0.1.11 (c (n "dc_converter_tof") (v "0.1.11") (d (list (d (n "clap") (r "^4.4.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "opencv") (r "=0.84.5") (f (quote ("imgproc" "imgcodecs"))) (k 0)) (d (n "rayon") (r "^1.8.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.4.0") (d #t) (k 0)))) (h "18xj3ib7ixda5la1xr4c19zlyqczvnmfwhdxblz5bhc6bcgxgk78") (r "1.72")))

(define-public crate-dc_converter_tof-0.1.12 (c (n "dc_converter_tof") (v "0.1.12") (d (list (d (n "clap") (r "^4.4.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "opencv") (r "=0.84.5") (f (quote ("imgproc" "imgcodecs"))) (k 0)) (d (n "rayon") (r "^1.8.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.4.0") (d #t) (k 0)))) (h "1b57lzlk4cyan9pvivrm9sajkna6a84aib05nqp6n7kir6mia064") (r "1.72")))

(define-public crate-dc_converter_tof-0.1.13 (c (n "dc_converter_tof") (v "0.1.13") (d (list (d (n "clap") (r "^4.4.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "opencv") (r "=0.86.1") (f (quote ("imgproc" "imgcodecs"))) (k 0)) (d (n "rayon") (r "^1.8.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.4.0") (d #t) (k 0)))) (h "1p4czhcyls5d0kv559l7b4j4jw4dkmf9932nz8qb16k7s1q83p1p") (r "1.73")))

(define-public crate-dc_converter_tof-0.1.14 (c (n "dc_converter_tof") (v "0.1.14") (d (list (d (n "clap") (r "^4.4.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "opencv") (r "=0.86.1") (f (quote ("imgproc" "imgcodecs"))) (k 0)) (d (n "rayon") (r "^1.8.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.4.0") (d #t) (k 0)))) (h "00axixkac5930xi7bbva2y0dbl2yk5hr2qfdj249zpxwrv7z9g8q") (r "1.73")))

