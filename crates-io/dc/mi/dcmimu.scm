(define-module (crates-io dc mi dcmimu) #:use-module (crates-io))

(define-public crate-dcmimu-0.1.0 (c (n "dcmimu") (v "0.1.0") (d (list (d (n "libm") (r "^0.1.2") (d #t) (k 0)))) (h "041n9nn7k60ixavhnrgsymzijj55pnrp9qagwvhj30z493gynwn2")))

(define-public crate-dcmimu-0.1.1 (c (n "dcmimu") (v "0.1.1") (d (list (d (n "libm") (r "^0.1.2") (d #t) (k 0)))) (h "0wikwjb37mh2v97s9rz15hrzypcps9srbq2xnq4awq3l5zhbyb9n")))

(define-public crate-dcmimu-0.1.2 (c (n "dcmimu") (v "0.1.2") (d (list (d (n "libm") (r "^0.1.2") (d #t) (k 0)))) (h "1a3hc32cgbbyyr12wg850a4qja7dkd491bh7653vnpl9ajszn6fb")))

(define-public crate-dcmimu-0.1.3 (c (n "dcmimu") (v "0.1.3") (d (list (d (n "libm") (r "^0.1.2") (d #t) (k 0)))) (h "1pq5is8wqj9x7zaf3xmii49kncvhsaw3hd9kfahrv7m054666sa7")))

(define-public crate-dcmimu-0.1.5 (c (n "dcmimu") (v "0.1.5") (d (list (d (n "csv") (r "^1.0.0") (d #t) (k 2)) (d (n "libm") (r "^0.1.2") (d #t) (k 0)))) (h "0s81yy22bivmdc1vf95p4ih8bwmv9m7gy2q4gvblkjyvqyi1sjf8")))

(define-public crate-dcmimu-0.2.0 (c (n "dcmimu") (v "0.2.0") (d (list (d (n "csv") (r "^1.0.0") (d #t) (k 2)) (d (n "libm") (r "^0.1.2") (d #t) (k 0)))) (h "0r0m2nqwkk3hk56milw87hhcvrj8ms5krw8xclbcrm200s4azdjd")))

(define-public crate-dcmimu-0.2.1 (c (n "dcmimu") (v "0.2.1") (d (list (d (n "csv") (r "^1.0.0") (d #t) (k 2)) (d (n "libm") (r "^0.1.2") (d #t) (k 0)))) (h "1wmn682m8h2m37vw5mwwqbi0n0if5lz21s5mf3b0rd5chgn5lf29")))

(define-public crate-dcmimu-0.2.2 (c (n "dcmimu") (v "0.2.2") (d (list (d (n "csv") (r "^1.0.0") (d #t) (k 2)) (d (n "libm") (r "^0.1.2") (d #t) (k 0)) (d (n "ryu") (r "^0.2") (d #t) (k 2)))) (h "0w01izzv3zvjymkbvvpjjlxm9bvvb490b2f10chw4nii4f4iqav9")))

