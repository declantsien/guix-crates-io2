(define-module (crates-io dc sp dcspkg_common) #:use-module (crates-io))

(define-public crate-dcspkg_common-0.1.0 (c (n "dcspkg_common") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sqlx") (r "^0.5.13") (f (quote ("sqlite" "runtime-tokio-rustls"))) (d #t) (k 0)))) (h "1gnwkl77vfm46a90lxvk0nwl9yc3l5alv9r63x32rq4vg9pml6j0") (y #t) (r "1.63")))

(define-public crate-dcspkg_common-0.1.1 (c (n "dcspkg_common") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sqlx") (r "^0.5.13") (f (quote ("sqlite" "runtime-tokio-rustls"))) (d #t) (k 0)))) (h "1in3pd0szkkfgb3zf2i27m0ic488gdisiykdd2clskymy0qp8222") (y #t) (r "1.62")))

(define-public crate-dcspkg_common-0.1.2 (c (n "dcspkg_common") (v "0.1.2") (d (list (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sqlx") (r "^0.5.13") (f (quote ("sqlite" "runtime-tokio-rustls"))) (d #t) (k 0)))) (h "0yg95xp9vcqijfpg9g0al0izz888yj7nzknsx7yjv6n97p9lf5qz") (y #t) (r "1.62")))

