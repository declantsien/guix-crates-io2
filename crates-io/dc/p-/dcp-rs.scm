(define-module (crates-io dc p- dcp-rs) #:use-module (crates-io))

(define-public crate-dcp-rs-0.0.1 (c (n "dcp-rs") (v "0.0.1") (d (list (d (n "byteorder") (r "^1.5.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.31") (d #t) (k 2)) (d (n "env_logger") (r "^0.10.1") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "testcontainers") (r "^0.15.0") (d #t) (k 2)))) (h "14kanbilzckyl02vvalgvjm66mqxjvfyn788456pq1qj3nlm1v7l")))

