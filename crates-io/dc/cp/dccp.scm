(define-module (crates-io dc cp dccp) #:use-module (crates-io))

(define-public crate-dccp-0.1.0 (c (n "dccp") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nix") (r "^0.11") (d #t) (k 0)))) (h "03hbwny4f1d7diz7ywyrr0szkij8xnkj4kvqch5qxl2lbfdpxi2g")))

