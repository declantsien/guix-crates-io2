(define-module (crates-io dc e- dce-cli) #:use-module (crates-io))

(define-public crate-dce-cli-0.1.0 (c (n "dce-cli") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.73") (o #t) (d #t) (k 0)) (d (n "dce-macro") (r "0.*") (d #t) (k 0)) (d (n "dce-router") (r "0.*") (k 0)) (d (n "dce-util") (r "0.*") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "15ykyranlibdm1z1i2wl7zjf12kxskm342xncq46p3xc7p8c0bly") (f (quote (("default" "async") ("async" "dce-router/async" "async-trait"))))))

