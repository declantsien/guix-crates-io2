(define-module (crates-io dc e- dce-router) #:use-module (crates-io))

(define-public crate-dce-router-0.1.0 (c (n "dce-router") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.77") (o #t) (d #t) (k 0)) (d (n "bytes") (r "^1.5.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "dce-util") (r "0.*") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)))) (h "0zvcbqv8dl4vg74n6vvcyp1xa5jw07qnb3v33frnwvhn8x6glaw8") (f (quote (("default" "async") ("async" "async-trait"))))))

(define-public crate-dce-router-0.2.0 (c (n "dce-router") (v "0.2.0") (d (list (d (n "async-trait") (r "^0.1.77") (o #t) (d #t) (k 0)) (d (n "bytes") (r "^1.5.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "dce-util") (r "0.*") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)))) (h "0gzi9y47vy8bfflrksn8md8gl0n4rgv476fwyb4yx0ywq8gx8ids") (f (quote (("default" "async") ("async" "async-trait"))))))

