(define-module (crates-io dc e- dce-tokio) #:use-module (crates-io))

(define-public crate-dce-tokio-0.1.0 (c (n "dce-tokio") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "bytes") (r "^1.5.0") (d #t) (k 0)) (d (n "dce-macro") (r "0.*") (d #t) (k 0)) (d (n "dce-router") (r "0.*") (d #t) (k 0)) (d (n "dce-util") (r "0.*") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.28") (f (quote ("sink"))) (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-util") (r "^0.7.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1bk0p3rlgmvcdyas5rsmscv18z2cypsp99hjnr34r8przdrlxn4l")))

