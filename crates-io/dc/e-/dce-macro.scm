(define-module (crates-io dc e- dce-macro) #:use-module (crates-io))

(define-public crate-dce-macro-0.1.0 (c (n "dce-macro") (v "0.1.0") (d (list (d (n "dce-router") (r "0.*") (d #t) (k 0)) (d (n "dce-util") (r "0.*") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.58") (d #t) (k 0)) (d (n "quote") (r "^1.0.27") (d #t) (k 0)) (d (n "syn") (r "^2.0.16") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1aj7ckfkvsi6fqvdjjfsspgg8iv58iv328r3hwdx7pqg2krzr2ml")))

(define-public crate-dce-macro-0.2.0 (c (n "dce-macro") (v "0.2.0") (d (list (d (n "dce-router") (r "0.*") (d #t) (k 0)) (d (n "dce-util") (r "0.*") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.58") (d #t) (k 0)) (d (n "quote") (r "^1.0.27") (d #t) (k 0)) (d (n "syn") (r "^2.0.16") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "16s64sibahm8vyp1md4li1vcflbv1iqp9r050h204wkgydkqr49y")))

