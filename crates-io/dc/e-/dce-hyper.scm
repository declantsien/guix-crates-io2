(define-module (crates-io dc e- dce-hyper) #:use-module (crates-io))

(define-public crate-dce-hyper-0.1.0 (c (n "dce-hyper") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "dce-macro") (r "0.*") (d #t) (k 0)) (d (n "dce-router") (r "0.*") (d #t) (k 0)) (d (n "dce-util") (r "0.*") (d #t) (k 0)) (d (n "http-body-util") (r "^0.1") (d #t) (k 0)) (d (n "hyper") (r "^1.2.0") (f (quote ("http1" "http2" "server"))) (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "sailfish") (r "^0.8.3") (d #t) (k 0)))) (h "1h29p5dmbjkd6km2xzvn9f87j6aibd190mz0i4539ciy7x9nwyhr")))

(define-public crate-dce-hyper-0.2.0 (c (n "dce-hyper") (v "0.2.0") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "dce-macro") (r "0.*") (d #t) (k 0)) (d (n "dce-router") (r "0.*") (d #t) (k 0)) (d (n "dce-util") (r "0.*") (d #t) (k 0)) (d (n "http-body-util") (r "^0.1") (d #t) (k 0)) (d (n "hyper") (r "^1.2.0") (f (quote ("http1" "http2" "server"))) (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "sailfish") (r "^0.8.3") (d #t) (k 0)))) (h "1pdcx3wqsksqx8vq6w127q464zgjbx5l6w6km6ly0741v8z26idl")))

