(define-module (crates-io dc e- dce-util) #:use-module (crates-io))

(define-public crate-dce-util-0.1.0 (c (n "dce-util") (v "0.1.0") (h "1b21hjjc4xrrd73z7l16ksmhhhvyms891cg4nl3yljnl06g86pg8")))

(define-public crate-dce-util-0.2.0 (c (n "dce-util") (v "0.2.0") (h "11kb9zg7gc5arn6pz5vwazbixfwkpskv2rskcd25q2d9v3gp2sjv")))

