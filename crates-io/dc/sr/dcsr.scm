(define-module (crates-io dc sr dcsr) #:use-module (crates-io))

(define-public crate-DCSR-0.0.2 (c (n "DCSR") (v "0.0.2") (d (list (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "glfw") (r "^0.51.0") (d #t) (k 0)) (d (n "wry") (r "^0.27.0") (d #t) (k 0)))) (h "082gg006ahs2gdx9kmpipn5v48fs9g5rwnbxv1wk99lbpnq3c8lj")))

(define-public crate-DCSR-0.0.3 (c (n "DCSR") (v "0.0.3") (d (list (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "glfw") (r "^0.51.0") (d #t) (k 0)) (d (n "wry") (r "^0.27.0") (d #t) (k 0)))) (h "1v3g75d9yysqkyncfq6l90bjhc7dnqqr694x4x49rmsd9nsl1c50")))

(define-public crate-DCSR-0.0.4 (c (n "DCSR") (v "0.0.4") (d (list (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "wry") (r "^0.27.0") (d #t) (k 0)))) (h "125x792d5qa16bdawddigixxac5ab204pznwksi3rsd606yx98lv")))

(define-public crate-DCSR-0.0.5 (c (n "DCSR") (v "0.0.5") (d (list (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "wry") (r "^0.27.0") (d #t) (k 0)))) (h "1vyvfqipxpswc90a0fkap1d33182249f3lnb9xfmm3pc2wqawij9")))

