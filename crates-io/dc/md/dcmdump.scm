(define-module (crates-io dc md dcmdump) #:use-module (crates-io))

(define-public crate-dcmdump-0.1.0 (c (n "dcmdump") (v "0.1.0") (d (list (d (n "dicom") (r "^0.1.0") (d #t) (k 0)))) (h "0fk9dq68n5kbpc1dhgd58fmbwb0k93vva1dgvc28y42rzk769ryg")))

(define-public crate-dcmdump-0.2.0 (c (n "dcmdump") (v "0.2.0") (d (list (d (n "dicom") (r "^0.2.0") (k 0)))) (h "0y9f5jjbjdlpwbn44n8k1nzgihfjwq39r5v75w4g1f0w9cj6lwaq") (f (quote (("default" "dicom/inventory-registry"))))))

(define-public crate-dcmdump-0.3.0 (c (n "dcmdump") (v "0.3.0") (d (list (d (n "dicom") (r "^0.3.0") (k 0)) (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "snafu") (r "^0.6.8") (d #t) (k 0)) (d (n "term_size") (r "^0.3.2") (d #t) (k 0)))) (h "0gq4s3qnzvxkwaf4ylwjy6k30i1igmcgxwwpb4ry0kl22zqqcalg") (f (quote (("default" "dicom/inventory-registry" "dicom/backtraces"))))))

(define-public crate-dcmdump-0.4.0 (c (n "dcmdump") (v "0.4.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "dicom") (r "^0.4.0") (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "snafu") (r "^0.6.8") (d #t) (k 0)) (d (n "term_size") (r "^0.3.2") (d #t) (k 0)))) (h "05gcw75x7knsjc4nbfalpi9jffp345dz0lk1a5mlhimd4qdl8hmy") (f (quote (("default" "dicom/inventory-registry" "dicom/backtraces"))))))

(define-public crate-dcmdump-0.5.0-rc.1 (c (n "dcmdump") (v "0.5.0-rc.1") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "dicom") (r "^0.5.0-rc.1") (k 0)) (d (n "dicom-dump") (r "^0.5.0-rc.1") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "snafu") (r "^0.6.8") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)) (d (n "term_size") (r "^0.3.2") (d #t) (k 0)))) (h "07g78rjpmiil2kpvb9ihnzr8r364d1gm2w0hyycry1jr8qh55q3x") (f (quote (("default" "dicom/inventory-registry" "dicom/backtraces"))))))

(define-public crate-dcmdump-0.5.0-rc.2 (c (n "dcmdump") (v "0.5.0-rc.2") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "dicom") (r "^0.5.0-rc.2") (k 0)) (d (n "dicom-dump") (r "^0.5.0-rc.2") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "snafu") (r "^0.7.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)) (d (n "term_size") (r "^0.3.2") (d #t) (k 0)))) (h "1n8xv8ka5442hq1g924k26wz8zzchfsgd9wdgjbainj7300kb3a9") (f (quote (("default" "dicom/inventory-registry" "dicom/backtraces"))))))

(define-public crate-dcmdump-0.5.0 (c (n "dcmdump") (v "0.5.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "dicom") (r "^0.5.0") (k 0)) (d (n "dicom-dump") (r "^0.5.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "snafu") (r "^0.7.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)) (d (n "term_size") (r "^0.3.2") (d #t) (k 0)))) (h "0nqp6xwmaz27lr6ab85h044r87p4dslpac80rv5cd5c9syddclni") (f (quote (("default" "dicom/inventory-registry" "dicom/backtraces"))))))

