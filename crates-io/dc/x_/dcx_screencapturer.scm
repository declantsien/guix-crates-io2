(define-module (crates-io dc x_ dcx_screencapturer) #:use-module (crates-io))

(define-public crate-dcx_screencapturer-0.1.1 (c (n "dcx_screencapturer") (v "0.1.1") (d (list (d (n "windows-sys") (r "^0.48.0") (f (quote ("Win32_Foundation" "Win32_Graphics_Gdi"))) (d #t) (k 0)))) (h "1zb5p8bldiyg6298xwy0bs34607c2bxr8g3rsc2wvgpvfian1s46")))

(define-public crate-dcx_screencapturer-0.1.2 (c (n "dcx_screencapturer") (v "0.1.2") (d (list (d (n "windows-sys") (r "^0.48.0") (f (quote ("Win32_Foundation" "Win32_Graphics_Gdi"))) (d #t) (k 0)))) (h "09i8vnf9cz44cl16xdambjgzslvxf0pm31wlm00kcfvb0zs7w6qz")))

(define-public crate-dcx_screencapturer-0.1.3 (c (n "dcx_screencapturer") (v "0.1.3") (d (list (d (n "windows-sys") (r "^0.48.0") (f (quote ("Win32_Foundation" "Win32_Graphics_Gdi"))) (d #t) (k 0)))) (h "035wczmamnya239g61rl8lj37ga6vck5facqbw5ci9kcdq3dapvr")))

(define-public crate-dcx_screencapturer-0.1.4 (c (n "dcx_screencapturer") (v "0.1.4") (d (list (d (n "windows-sys") (r "^0.48.0") (f (quote ("Win32_Foundation" "Win32_Graphics_Gdi"))) (d #t) (k 0)))) (h "0jaghlfq613670yw5zf0p0gqxbc8z08h2sg2y7a8icz3gj50cc6b")))

