(define-module (crates-io dc rs dcrs) #:use-module (crates-io))

(define-public crate-dcrs-0.0.1 (c (n "dcrs") (v "0.0.1") (h "06mp8601nvb62vd2d3a7abya2lgw407al1mczvghrsxsssrdnlc5") (y #t)))

(define-public crate-dcrs-0.0.2 (c (n "dcrs") (v "0.0.2") (h "0rfhw0ispxjq0svrihnh6f58ipg5x3gc911clrsxl9irhdpwm7gr") (y #t)))

(define-public crate-dcrs-0.0.3 (c (n "dcrs") (v "0.0.3") (h "02qiaykfgm6121wbr87lgpz56g8rlnh8kh8dpglhq7dpi836x0fs") (y #t)))

