(define-module (crates-io dc ap dcap-ql-sys) #:use-module (crates-io))

(define-public crate-dcap-ql-sys-0.0.0 (c (n "dcap-ql-sys") (v "0.0.0") (h "1r0nqbmcdbs3kghjrfvpi8srr6awz3y79318a2fkn5avw5hjpwm4")))

(define-public crate-dcap-ql-sys-0.1.0 (c (n "dcap-ql-sys") (v "0.1.0") (d (list (d (n "num-derive") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "sgx-isa") (r "^0.2.0") (d #t) (k 0)))) (h "1lf46q0vp46fmmfn2mzfgzs1k8m24pd5dz48hvr5rgmka2x64k8z") (f (quote (("link")))) (l "sgx_dcap_ql")))

(define-public crate-dcap-ql-sys-0.2.0 (c (n "dcap-ql-sys") (v "0.2.0") (d (list (d (n "num-derive") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "sgx-isa") (r "^0.3.0") (d #t) (k 0)))) (h "0ch2vylza6h05qipd1cf9h6jiy6ycs0b8c4iv57pxbpk522n46q3") (f (quote (("link")))) (l "sgx_dcap_ql")))

(define-public crate-dcap-ql-sys-0.2.1 (c (n "dcap-ql-sys") (v "0.2.1") (d (list (d (n "num-derive") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "sgx-isa") (r "^0.4.0") (d #t) (k 0)))) (h "0kpvardpz6i2fznkfblc4wfy1qj9c2ddy7bilsxc30qj7sp26asm") (f (quote (("link")))) (l "sgx_dcap_ql")))

