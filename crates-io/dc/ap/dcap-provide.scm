(define-module (crates-io dc ap dcap-provide) #:use-module (crates-io))

(define-public crate-dcap-provide-0.0.0 (c (n "dcap-provide") (v "0.0.0") (h "0vd3aasyilr6s10cxi2zj97qygbvh54cpwl52qhl2p7d33b3lbvr")))

(define-public crate-dcap-provide-0.0.1 (c (n "dcap-provide") (v "0.0.1") (h "0w80dhd7bmvwvgmdxil8ifpq1ar8hby9di5qzpyp4m5hrngk5m41")))

