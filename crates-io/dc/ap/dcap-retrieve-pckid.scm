(define-module (crates-io dc ap dcap-retrieve-pckid) #:use-module (crates-io))

(define-public crate-dcap-retrieve-pckid-0.1.0 (c (n "dcap-retrieve-pckid") (v "0.1.0") (d (list (d (n "aesm-client") (r "^0.5.0") (f (quote ("sgxs"))) (d #t) (k 0)) (d (n "dcap-ql") (r "^0.3.0") (k 0)) (d (n "report-test") (r "^0.3.1") (d #t) (k 0)) (d (n "sgx-isa") (r "^0.3.0") (d #t) (k 0)) (d (n "sgxs-loaders") (r "^0.2.0") (d #t) (k 0)))) (h "1vpn0zsqqy0pvak7dslafyqcb4qa47jfwm81dp49dfycrc5pfpz3")))

(define-public crate-dcap-retrieve-pckid-0.1.1 (c (n "dcap-retrieve-pckid") (v "0.1.1") (d (list (d (n "aesm-client") (r "^0.5.0") (f (quote ("sgxs"))) (d #t) (k 0)) (d (n "dcap-ql") (r "^0.3.0") (k 0)) (d (n "report-test") (r "^0.3.1") (d #t) (k 0)) (d (n "sgx-isa") (r "^0.3.0") (d #t) (k 0)) (d (n "sgxs-loaders") (r "^0.3.0") (d #t) (k 0)))) (h "1vp0wkkaa7fqgk0vw7mn9zby973hghs5w3mklazyxqz35g5r9nyz")))

(define-public crate-dcap-retrieve-pckid-0.1.3 (c (n "dcap-retrieve-pckid") (v "0.1.3") (d (list (d (n "aesm-client") (r "^0.5.0") (f (quote ("sgxs"))) (d #t) (k 0)) (d (n "dcap-ql") (r "^0.3.0") (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "report-test") (r "^0.3.1") (d #t) (k 0)) (d (n "sgx-isa") (r "^0.4.0") (d #t) (k 0)) (d (n "sgxs-loaders") (r "^0.3.0") (d #t) (k 0)))) (h "0zp48k1g9fdkfk1dcnq00gz6jv89i1wpjd6hhjgwkv4xwxphmd9z")))

(define-public crate-dcap-retrieve-pckid-0.2.0 (c (n "dcap-retrieve-pckid") (v "0.2.0") (d (list (d (n "aesm-client") (r "^0.6.0") (f (quote ("sgxs"))) (d #t) (k 0)) (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "dcap-ql") (r "^0.4.0") (k 0)) (d (n "report-test") (r "^0.4.0") (d #t) (k 0)) (d (n "sgx-isa") (r "^0.4.0") (d #t) (k 0)) (d (n "sgxs-loaders") (r "^0.4.0") (d #t) (k 0)))) (h "1bwsbq75kzdrrk13c9qx8n93arr2b2gl4h8qh20y4qyfmm9sr8xw")))

