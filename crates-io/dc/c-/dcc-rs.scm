(define-module (crates-io dc c- dcc-rs) #:use-module (crates-io))

(define-public crate-dcc-rs-0.1.0 (c (n "dcc-rs") (v "0.1.0") (d (list (d (n "bitvec") (r "^1") (k 0)) (d (n "defmt") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.7") (f (quote ("unproven"))) (d #t) (k 0)))) (h "0l2a1acj4ycpg2c5mqb7pf4mi7i07hhj2qyqjn57kbpp8y81f6wf") (f (quote (("use-defmt" "defmt"))))))

(define-public crate-dcc-rs-0.2.0 (c (n "dcc-rs") (v "0.2.0") (d (list (d (n "bitvec") (r "^1") (k 0)) (d (n "defmt") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.7") (f (quote ("unproven"))) (d #t) (k 0)))) (h "1ggx871ifvqzq2y07wsqa87savq2mdk8csmnjrqixcaqkk0h6413") (f (quote (("use-defmt" "defmt"))))))

(define-public crate-dcc-rs-0.3.0 (c (n "dcc-rs") (v "0.3.0") (d (list (d (n "bitvec") (r "^1") (k 0)) (d (n "defmt") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.7") (f (quote ("unproven"))) (d #t) (k 0)))) (h "11mcshcrlxf7kzhq15cmkkxywvjp645fgyp2n3xpmxmzc1lg5156") (f (quote (("use-defmt" "defmt"))))))

