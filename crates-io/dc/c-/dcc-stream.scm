(define-module (crates-io dc c- dcc-stream) #:use-module (crates-io))

(define-public crate-dcc-stream-0.1.0 (c (n "dcc-stream") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "jtag-adi") (r "^0.1.0") (d #t) (k 0)) (d (n "jtag-taps") (r "^0.3.0") (d #t) (k 0)))) (h "0lqw0zq22sizq6f14csxl6pywl007xzx5zki4zxg49h2b0zh6r2y")))

(define-public crate-dcc-stream-0.1.1 (c (n "dcc-stream") (v "0.1.1") (d (list (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "jtag-adi") (r "^0.2") (d #t) (k 0)) (d (n "jtag-taps") (r "^0.4") (d #t) (k 0)))) (h "168316fjnpgpsnbpvgzc90a6y2i2l4fyvnqy3v2b277nwrivgllk")))

(define-public crate-dcc-stream-0.1.2 (c (n "dcc-stream") (v "0.1.2") (d (list (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ctrlc") (r "^3.4.1") (d #t) (k 0)) (d (n "jtag-adi") (r "^0.3") (d #t) (k 0)) (d (n "jtag-taps") (r "^0.5") (d #t) (k 0)))) (h "1110vg992fr65cbmf1l5bq5agh2234a52js75cyca3swfcy1fyg7")))

