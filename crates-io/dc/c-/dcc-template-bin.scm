(define-module (crates-io dc c- dcc-template-bin) #:use-module (crates-io))

(define-public crate-dcc-template-bin-0.1.0 (c (n "dcc-template-bin") (v "0.1.0") (h "1ls20lwwdnsfyrk7dygy8qi9axpdpkdx23pl7wdwbazwa531vnwq")))

(define-public crate-dcc-template-bin-0.1.1 (c (n "dcc-template-bin") (v "0.1.1") (h "1p36c3khshmy93j4a71lnzscc333i63aaf5gsfzszjyk045p5vpx")))

