(define-module (crates-io dc c- dcc-lsystem-derive) #:use-module (crates-io))

(define-public crate-dcc-lsystem-derive-0.1.0 (c (n "dcc-lsystem-derive") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "049pwlwsqinjpz4p9ljizg7zy5hasll3vxvlmd4p0by25vpq78bi")))

(define-public crate-dcc-lsystem-derive-0.1.1 (c (n "dcc-lsystem-derive") (v "0.1.1") (d (list (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0hirja8f2myjxfjfg9ksz67w3nqlz535qyr9vdxavjxlhwzflsp0")))

(define-public crate-dcc-lsystem-derive-0.1.2 (c (n "dcc-lsystem-derive") (v "0.1.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "17n8x6dpm59np31vcq39z6wz1f1bqqmx00485c0y72d8y5yadz5h")))

