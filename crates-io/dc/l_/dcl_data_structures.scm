(define-module (crates-io dc l_ dcl_data_structures) #:use-module (crates-io))

(define-public crate-dcl_data_structures-0.4.2 (c (n "dcl_data_structures") (v "0.4.2") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "deep_causality_macros") (r "^0.4.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (f (quote ("small_rng"))) (d #t) (k 2)))) (h "0cbyrrpvrs06k024b7mshzjnsrp9m79isiigkis523plyw1ialjw") (r "1.65")))

(define-public crate-dcl_data_structures-0.4.3 (c (n "dcl_data_structures") (v "0.4.3") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "deep_causality_macros") (r "^0.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (f (quote ("small_rng"))) (d #t) (k 2)))) (h "1y5gvxidql453sdaf2g8j3ycm7y0dx3c4b9kv4jh9jy3a2rw3ya2") (r "1.65")))

(define-public crate-dcl_data_structures-0.4.4 (c (n "dcl_data_structures") (v "0.4.4") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (f (quote ("small_rng"))) (d #t) (k 2)))) (h "05jia0fxvnz8qhycgallyx9kpxqh4hxj9iqsc8x2vv0r8asgrv54") (r "1.65")))

(define-public crate-dcl_data_structures-0.4.5 (c (n "dcl_data_structures") (v "0.4.5") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 2)))) (h "1cympygq0nwmwa50lmhz96zygp4pidr3xb4ad3pa1cqr440kdjw1") (r "1.65")))

(define-public crate-dcl_data_structures-0.4.6 (c (n "dcl_data_structures") (v "0.4.6") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 2)))) (h "1bwdhv43lpzb0zv1jbblnx08cb286fhvji0zccms583ckfmlcqv8") (r "1.65")))

(define-public crate-dcl_data_structures-0.4.7 (c (n "dcl_data_structures") (v "0.4.7") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 2)))) (h "0is6lany9hwq9xb7lpdimx4s8rcygg01ghkq27l8f0c7cb8id0q6") (r "1.65")))

