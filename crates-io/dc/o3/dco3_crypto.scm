(define-module (crates-io dc o3 dco3_crypto) #:use-module (crates-io))

(define-public crate-dco3_crypto-0.1.0 (c (n "dco3_crypto") (v "0.1.0") (d (list (d (n "openssl") (r "^0.10") (f (quote ("vendored"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.149") (f (quote ("serde_derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.89") (d #t) (k 0)))) (h "1av7k5nwc1n2gbxl2bd3sj0a2m9fppgw574pj3hnrh1dh7fshk0z")))

(define-public crate-dco3_crypto-0.2.0 (c (n "dco3_crypto") (v "0.2.0") (d (list (d (n "openssl") (r "^0.10") (f (quote ("vendored"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.149") (f (quote ("serde_derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.89") (d #t) (k 0)))) (h "1zfdx38sniy1ck8kl7x2y2pxas8jnz0qqdffmln02ddqfawzb1ai")))

(define-public crate-dco3_crypto-0.3.0 (c (n "dco3_crypto") (v "0.3.0") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (f (quote ("vendored"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.149") (f (quote ("serde_derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.89") (d #t) (k 0)))) (h "1pp1f8084mxq3s7apxy4fn6bahi2knirkzf7cgjjc45azxhvq6mb")))

(define-public crate-dco3_crypto-0.4.0 (c (n "dco3_crypto") (v "0.4.0") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (f (quote ("vendored"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.149") (f (quote ("serde_derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.89") (d #t) (k 0)))) (h "1zpfzqssrghi6mp1085n4n4am01l9z03hlvarwjda22rxvrnwzcp") (y #t)))

(define-public crate-dco3_crypto-0.4.1 (c (n "dco3_crypto") (v "0.4.1") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (f (quote ("vendored"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.149") (f (quote ("serde_derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.89") (d #t) (k 0)))) (h "0nlz4d2mnywdq9xqws35ww0vmzyh1ha0glnvz6hz4rwp67y7fljw")))

(define-public crate-dco3_crypto-0.5.0 (c (n "dco3_crypto") (v "0.5.0") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "openssl") (r "^0.10.45") (f (quote ("vendored"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.149") (f (quote ("serde_derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.89") (d #t) (k 0)))) (h "1jlhv4b4sh7fs4yzvsv0pqjfacnxx63xbdxzz19sn3hi0b4a0n0w")))

(define-public crate-dco3_crypto-0.5.1 (c (n "dco3_crypto") (v "0.5.1") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "openssl") (r "^0.10.45") (f (quote ("vendored"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.149") (f (quote ("serde_derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.89") (d #t) (k 0)))) (h "1y1b57fsa1pd4sylzyzkqqzl9amx3l8j0v83g7g6nvr6fssy1g10")))

(define-public crate-dco3_crypto-0.6.0 (c (n "dco3_crypto") (v "0.6.0") (d (list (d (n "openssl") (r "^0.10.64") (f (quote ("vendored"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("serde_derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "05lajd7gvslf3im2i8dr6blj2bngll6xik02v7y95k8g1x697y7r")))

(define-public crate-dco3_crypto-0.7.0 (c (n "dco3_crypto") (v "0.7.0") (d (list (d (n "openssl") (r "^0.10.64") (f (quote ("vendored"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("serde_derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "0y8pm2m589dfgnzg26fhqlfqd0knnr1iwvsdy22ddd1smd5h5hk3")))

