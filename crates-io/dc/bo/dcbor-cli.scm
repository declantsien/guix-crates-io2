(define-module (crates-io dc bo dcbor-cli) #:use-module (crates-io))

(define-public crate-dcbor-cli-0.3.0 (c (n "dcbor-cli") (v "0.3.0") (d (list (d (n "clap") (r "^4.0.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dcbor") (r "^0.11.0") (d #t) (k 0)) (d (n "indoc") (r "^2.0.0") (d #t) (k 2)))) (h "0rbxbiv4bsh6z9a41jv9mv31nz1nslmw46f5s0slrxnkh156jz4m")))

(define-public crate-dcbor-cli-0.3.1 (c (n "dcbor-cli") (v "0.3.1") (d (list (d (n "clap") (r "^4.0.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dcbor") (r "^0.11.0") (d #t) (k 0)) (d (n "indoc") (r "^2.0.0") (d #t) (k 2)))) (h "0c226zav8n83dby3c23mvqvgmbn5asp10lfmxsl5wl98l0ydyfsh")))

(define-public crate-dcbor-cli-0.3.2 (c (n "dcbor-cli") (v "0.3.2") (d (list (d (n "clap") (r "^4.0.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dcbor") (r "^0.11") (d #t) (k 0)) (d (n "indoc") (r "^2.0.0") (d #t) (k 2)))) (h "0ya56cv4j97dwjmwbxs8837c1sfgrmywmcqrysi20629s38hdibj")))

(define-public crate-dcbor-cli-0.4.0 (c (n "dcbor-cli") (v "0.4.0") (d (list (d (n "clap") (r "^4.0.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dcbor") (r "^0.13") (d #t) (k 0)) (d (n "indoc") (r "^2.0.0") (d #t) (k 2)))) (h "079s02rlcvq1hnwilm227rs0q16cifdqgn3l9f8sdbdpkxv1aykf")))

(define-public crate-dcbor-cli-0.4.1 (c (n "dcbor-cli") (v "0.4.1") (d (list (d (n "clap") (r "^4.0.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dcbor") (r "^0.13.0") (d #t) (k 0)) (d (n "indoc") (r "^2.0.0") (d #t) (k 2)))) (h "0daiqmxz1lckgxyyjndxdxkwz5r004pafm6mf7gxxklvm7c7g801")))

(define-public crate-dcbor-cli-0.5.0 (c (n "dcbor-cli") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0.83") (d #t) (k 0)) (d (n "clap") (r "^4.0.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dcbor") (r "^0.14.0") (d #t) (k 0)) (d (n "indoc") (r "^2.0.0") (d #t) (k 2)))) (h "0l5g8clc5rri9p6wf1zk606czllbf2c9rfp3ishk9760gr7z9ykh")))

