(define-module (crates-io dc s2 dcs2) #:use-module (crates-io))

(define-public crate-dcs2-0.1.0 (c (n "dcs2") (v "0.1.0") (d (list (d (n "bumpalo") (r "^3.11.0") (d #t) (k 0)) (d (n "heapless") (r "^0.7.16") (f (quote ("serde"))) (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (k 0)))) (h "1cdvnyj42qizsy0a2q79gigr7pxc23mbkikifzr8vhmdk9b8c063")))

