(define-module (crates-io dc s2 dcs2-raft) #:use-module (crates-io))

(define-public crate-dcs2-raft-0.1.0 (c (n "dcs2-raft") (v "0.1.0") (d (list (d (n "clock") (r "^0.1.0") (d #t) (k 0) (p "dcs2-clock")) (d (n "dcs") (r "^0.1.0") (d #t) (k 0) (p "dcs2")) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)))) (h "00bfl62w2wgiwzxlsjgc79n0bwddx7aidgwf4jq133j8044s30qz")))

