(define-module (crates-io dc ol dcolor) #:use-module (crates-io))

(define-public crate-dcolor-0.1.0 (c (n "dcolor") (v "0.1.0") (h "1qzpa9jc9hkgvhjinxvc8xx0n8z4va172328lzvn0d3r004irhif")))

(define-public crate-dcolor-0.3.0 (c (n "dcolor") (v "0.3.0") (d (list (d (n "dnum") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.8") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.8") (o #t) (d #t) (k 0)))) (h "053cprmjyid2vn6w7krs6bn0h86rzihnq8gwl3w5jpkg9g8fvgwc") (f (quote (("serde_" "serde" "serde_derive") ("default")))) (y #t)))

