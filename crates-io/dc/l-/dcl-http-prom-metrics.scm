(define-module (crates-io dc l- dcl-http-prom-metrics) #:use-module (crates-io))

(define-public crate-dcl-http-prom-metrics-0.1.0 (c (n "dcl-http-prom-metrics") (v "0.1.0") (d (list (d (n "actix-web") (r "^4.2.1") (o #t) (d #t) (k 0)) (d (n "actix-web-lab") (r "^0.18.9") (o #t) (d #t) (k 0)) (d (n "prometheus") (r "^0.13.3") (d #t) (k 0)))) (h "1bqryswhrsfr49s89pahh27wfyrm2dljap68if7lj5zcyqk1a9w3") (f (quote (("default" "actix")))) (s 2) (e (quote (("actix" "dep:actix-web" "dep:actix-web-lab")))) (r "1.65.0")))

(define-public crate-dcl-http-prom-metrics-0.2.0 (c (n "dcl-http-prom-metrics") (v "0.2.0") (d (list (d (n "actix-web") (r "^4.2.1") (o #t) (d #t) (k 0)) (d (n "actix-web-lab") (r "^0.18.9") (o #t) (d #t) (k 0)) (d (n "prometheus") (r "^0.13.3") (d #t) (k 0)))) (h "0a2iximxdd87a43q7frwalvpyfs811gv5n6790fl76qngw1ffy3j") (f (quote (("default" "actix")))) (s 2) (e (quote (("actix" "dep:actix-web" "dep:actix-web-lab")))) (r "1.65.0")))

