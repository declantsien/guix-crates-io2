(define-module (crates-io dc l- dcl-rpc-codegen) #:use-module (crates-io))

(define-public crate-dcl-rpc-codegen-0.1.0 (c (n "dcl-rpc-codegen") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "prost-build") (r "^0.11") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "1lb3ky52flzqfj66ldnnjjygjcmc924spmv4hbanpak1acydvdz4") (r "1.65.0")))

