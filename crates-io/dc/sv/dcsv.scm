(define-module (crates-io dc sv dcsv) #:use-module (crates-io))

(define-public crate-dcsv-0.1.0 (c (n "dcsv") (v "0.1.0") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "0i66l2s4a1bd7jijjijd0s2pvg5cxp2wvcfby86n6mbkw0cywmll")))

(define-public crate-dcsv-0.1.1 (c (n "dcsv") (v "0.1.1") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "17zldgf517k9zvbgivz7j37xrj3f46y09vhqzz2l48lj117di6rr")))

(define-public crate-dcsv-0.1.2 (c (n "dcsv") (v "0.1.2") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "0ql0y183c335lkzw6awmnsfdh8k0m7nm1r9ka8kq8jjb8hg0v2km")))

(define-public crate-dcsv-0.1.3 (c (n "dcsv") (v "0.1.3") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "05p8ilgpybkajawij95mg47lppvnr90lclkcqaajjxf6s32r32aa")))

(define-public crate-dcsv-0.1.4 (c (n "dcsv") (v "0.1.4") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "16azba36a0pxwzbpzfgj1s1rs5xix2lrkwa6axkd77vbwwq2awx3")))

(define-public crate-dcsv-0.2.0 (c (n "dcsv") (v "0.2.0") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "0c0rw5agpq2vjy8g1gbjliyws8zy7p9m6l2qyk606pd64bvii03h")))

(define-public crate-dcsv-0.3.0 (c (n "dcsv") (v "0.3.0") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "0h9kbqvak3wrj45453yb2qgzqx456yi6jr2768ig8n1bddwk2x76")))

(define-public crate-dcsv-0.3.1 (c (n "dcsv") (v "0.3.1") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "1hzw5gh491xm1lpm0cllhgj3f8p2bg6cg0fw2f7rr14hkw673lb0")))

(define-public crate-dcsv-0.3.2 (c (n "dcsv") (v "0.3.2") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "1jihavxjxzpfnq25fzbp59x265xa41p5brkzgpf6gfsh6dimygcq")))

(define-public crate-dcsv-0.3.3-rc1 (c (n "dcsv") (v "0.3.3-rc1") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "1z09nsd8cxlcz40bsmxnrnly7dds4f37rydnnrzrb1znqn622khd")))

(define-public crate-dcsv-0.3.3 (c (n "dcsv") (v "0.3.3") (d (list (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "0x460c8wg8glaqp89hm4jg9f6dncczkcripk4vqy20m78x92f96m")))

(define-public crate-dcsv-0.3.4-beta.1 (c (n "dcsv") (v "0.3.4-beta.1") (d (list (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.11") (d #t) (k 0)))) (h "0rgjirvhf22fwz2wjjhpzazw6lpk185834zcagy64wmkliw96iil")))

(define-public crate-dcsv-0.3.4-beta.2 (c (n "dcsv") (v "0.3.4-beta.2") (d (list (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.11") (d #t) (k 0)))) (h "12lhscv985qic5vdgnvrqbwbr117hd9zq7qzim3rf2zwfhlfrr8k")))

