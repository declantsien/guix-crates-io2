(define-module (crates-io dc f7 dcf77_utils) #:use-module (crates-io))

(define-public crate-dcf77_utils-0.1.0 (c (n "dcf77_utils") (v "0.1.0") (d (list (d (n "heapless") (r "^0.7.13") (d #t) (k 0)) (d (n "radio_datetime_utils") (r "^0.1.0") (d #t) (k 0)))) (h "12akbpz2yhxcvn5n40g5csfjr2jn1v2xr2wvzpprcvf8kxvpxmsq")))

(define-public crate-dcf77_utils-0.1.1 (c (n "dcf77_utils") (v "0.1.1") (d (list (d (n "heapless") (r "^0.7.13") (d #t) (k 0)) (d (n "radio_datetime_utils") (r "^0.1.0") (d #t) (k 0)))) (h "05z5mky47fva1751bphvzah092cbr4gd75khaxsiivymgavpm0c9")))

(define-public crate-dcf77_utils-0.2.0 (c (n "dcf77_utils") (v "0.2.0") (d (list (d (n "radio_datetime_utils") (r "^0.2.0") (d #t) (k 0)))) (h "00x8r4lwjjlpd42cdvdq0x62ci61lfmj1baqidy7drwhfmdjidi8")))

(define-public crate-dcf77_utils-0.2.1 (c (n "dcf77_utils") (v "0.2.1") (d (list (d (n "radio_datetime_utils") (r "^0.3.0") (d #t) (k 0)))) (h "0hiq3sall6g2b2zigr8s13jhn5l40hr3r82zkckg29s3jnk7q86w")))

(define-public crate-dcf77_utils-0.2.2 (c (n "dcf77_utils") (v "0.2.2") (d (list (d (n "radio_datetime_utils") (r "^0.4.0") (d #t) (k 0)))) (h "0bybsjvi7s7w5i7ynkpvwyw06l8z9j02mfxad16h00frfkxy89z6")))

(define-public crate-dcf77_utils-0.3.0 (c (n "dcf77_utils") (v "0.3.0") (d (list (d (n "radio_datetime_utils") (r "^0.4.2") (d #t) (k 0)))) (h "0lpwza9mmk9gfx8swpv0mkhf3243lg0f1h9ik2mrsd5dgv8c7qqy")))

(define-public crate-dcf77_utils-0.3.1 (c (n "dcf77_utils") (v "0.3.1") (d (list (d (n "radio_datetime_utils") (r "^0.4.2") (d #t) (k 0)))) (h "0w9v83yylpcaxvz37ks04vha45haj8jp844w2gccq1918sxi7fim")))

(define-public crate-dcf77_utils-0.4.0 (c (n "dcf77_utils") (v "0.4.0") (d (list (d (n "radio_datetime_utils") (r "^0.4.2") (d #t) (k 0)))) (h "0m7sbq4vin3r49sy6mqhmd7blhxrqli9m57h5x9kcnhg7zbjym2y")))

(define-public crate-dcf77_utils-0.5.0 (c (n "dcf77_utils") (v "0.5.0") (d (list (d (n "radio_datetime_utils") (r "^0.5.0") (d #t) (k 0)))) (h "1yscrcl7wg5ybyfk6jwdfjxar86wm7ppd329nl0rlmibmwbqckc2")))

(define-public crate-dcf77_utils-0.6.0 (c (n "dcf77_utils") (v "0.6.0") (d (list (d (n "radio_datetime_utils") (r "^0.5") (d #t) (k 0)))) (h "1pjwka57z0x1j4wy8qwsk3bd66klr6wavr10dgwn1vxaqarfrf1f")))

