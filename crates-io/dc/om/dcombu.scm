(define-module (crates-io dc om dcombu) #:use-module (crates-io))

(define-public crate-dcombu-0.0.1 (c (n "dcombu") (v "0.0.1") (d (list (d (n "combup") (r "^0.1") (d #t) (k 0)) (d (n "gdi32-sys") (r "^0.2") (d #t) (k 0)) (d (n "kernel32-sys") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "winapi") (r "^0.2") (d #t) (k 0)))) (h "10cljh55w3i1fh08gwm9fbi5ii8969m6lbfhlfsk4qx210qgyjhj")))

