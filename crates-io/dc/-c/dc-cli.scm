(define-module (crates-io dc -c dc-cli) #:use-module (crates-io))

(define-public crate-dc-cli-0.1.0 (c (n "dc-cli") (v "0.1.0") (d (list (d (n "config") (r "^0.13.1") (d #t) (k 0)) (d (n "dc-ock") (r "^2.0.0") (d #t) (k 0)) (d (n "directories") (r "^4.0.1") (d #t) (k 0)))) (h "19j66rq43f3npyg224nhv36py2acpg11mk3zf5v3iqq2bfzskvq1")))

