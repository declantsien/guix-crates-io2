(define-module (crates-io yd ev ydev) #:use-module (crates-io))

(define-public crate-ydev-0.1.0 (c (n "ydev") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^3.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "jlogger") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "0r0zd81h6cv6ym8cq3qc6g2x3as10xzpfqbaypa8y3h0l6xx9bpz")))

(define-public crate-ydev-0.1.1 (c (n "ydev") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^3.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "jlogger") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0kfq6bc4i26s64bqwbdmzld3212rmd8wd5d2gmv9r7v7f0fqzw9j")))

