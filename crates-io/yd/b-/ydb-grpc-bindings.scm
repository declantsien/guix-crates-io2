(define-module (crates-io yd b- ydb-grpc-bindings) #:use-module (crates-io))

(define-public crate-ydb-grpc-bindings-0.0.1 (c (n "ydb-grpc-bindings") (v "0.0.1") (d (list (d (n "prost") (r "^0.11.9") (d #t) (k 0)) (d (n "tonic") (r "^0.9.2") (f (quote ("tls-roots"))) (d #t) (k 0)) (d (n "tonic-build") (r "^0.9.2") (d #t) (k 1)))) (h "0m0jk34cx7r472alfsx29yikfama94s567xp310l0zhhqvzgpwx7")))

