(define-module (crates-io yd b- ydb-grpc-helpers) #:use-module (crates-io))

(define-public crate-ydb-grpc-helpers-0.0.0 (c (n "ydb-grpc-helpers") (v "0.0.0") (h "1s60w3an4mwv6x3mdq93ybwvw7f50fqbbzcy7a4ylvw8d624zsmk")))

(define-public crate-ydb-grpc-helpers-0.0.1 (c (n "ydb-grpc-helpers") (v "0.0.1") (h "0rcnyj33y3n3g5nkicc58w46mp4a1y1yh202zcw888qcxx2007bh")))

(define-public crate-ydb-grpc-helpers-0.0.2 (c (n "ydb-grpc-helpers") (v "0.0.2") (h "0c7n1qiz0cmca8rl4mmh9n1dr6fpmf3b6dl0wr3yzjh65w92b5j7")))

(define-public crate-ydb-grpc-helpers-0.0.3 (c (n "ydb-grpc-helpers") (v "0.0.3") (h "0agaizyg673yspqa6w7vqyzwd0yv8r2h2yivzyd3a9rdm9xbalbg")))

(define-public crate-ydb-grpc-helpers-0.0.4 (c (n "ydb-grpc-helpers") (v "0.0.4") (h "1c6cmk3prni50hkcfanyff5smixmpdl3k7v2dvgpdddnkz6zal8f")))

(define-public crate-ydb-grpc-helpers-0.0.5 (c (n "ydb-grpc-helpers") (v "0.0.5") (h "0c3ipnd0nwxmkz4y2vc0ks087s1yp78qn3zpl0n76gmvbhvm2d2m")))

(define-public crate-ydb-grpc-helpers-0.0.6 (c (n "ydb-grpc-helpers") (v "0.0.6") (h "1q5vl30s3hb78mwjkmd2i1qn2vw592hrjf5dfgkb51jxlbaz212r")))

(define-public crate-ydb-grpc-helpers-0.0.7 (c (n "ydb-grpc-helpers") (v "0.0.7") (h "05zr48rhkn2nbzwf48ig816rnrv7hqnqq9fhb9zfzxg3yqddjfv7")))

(define-public crate-ydb-grpc-helpers-0.0.8 (c (n "ydb-grpc-helpers") (v "0.0.8") (h "1qy6c81k0p7fy15wjaij09cpjy672haypsqx8b99qrngjsksbh43")))

(define-public crate-ydb-grpc-helpers-0.0.9 (c (n "ydb-grpc-helpers") (v "0.0.9") (h "10fv0xxscsk5xmqr2ylvry8sq5nw359kiky0fsv2znn82sc25dvj")))

(define-public crate-ydb-grpc-helpers-0.0.10 (c (n "ydb-grpc-helpers") (v "0.0.10") (h "03n77zgkld3nz924fsakypwvjc9mk5l1gs2apxjhavlbs69g5l87")))

