(define-module (crates-io yd li ydlidar_driver) #:use-module (crates-io))

(define-public crate-ydlidar_driver-0.1.0 (c (n "ydlidar_driver") (v "0.1.0") (d (list (d (n "clap") (r "^3.1.6") (f (quote ("derive"))) (d #t) (k 2)) (d (n "crossbeam-channel") (r "^0.5.8") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.8.16") (d #t) (k 0)) (d (n "piston_window") (r "^0.120.0") (d #t) (k 2)) (d (n "serialport") (r "^4.2.2") (d #t) (k 0)))) (h "1b51rf87rm76j3gshqvl6dlm6syz23pblrbfs758z5mgxw44jnfd")))

