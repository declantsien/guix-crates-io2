(define-module (crates-io ds al dsalgo) #:use-module (crates-io))

(define-public crate-dsalgo-0.1.0 (c (n "dsalgo") (v "0.1.0") (h "0dhl5gqwcnsm98p9ybxravlbk6vv8gi1ki8dr61mza0g9kcm4z57")))

(define-public crate-dsalgo-0.1.1 (c (n "dsalgo") (v "0.1.1") (h "0dr0sqpxnacdf320gly28xjg1d1z7f5n2n864a7dc5fki1mnbk5d") (r "1.58.1")))

(define-public crate-dsalgo-0.1.2 (c (n "dsalgo") (v "0.1.2") (h "1vnh9ddpyn3p77k8xck88zg73pk1kqlrsk6s65f74f7kbs0iwfvj") (r "1.58.1")))

(define-public crate-dsalgo-0.2.0 (c (n "dsalgo") (v "0.2.0") (h "1f2mm69z1v644w4y39hjks1r2rnckzisi32v4qnh48a06ipjgb95") (r "1.58.1")))

(define-public crate-dsalgo-0.2.1 (c (n "dsalgo") (v "0.2.1") (h "0hrxwacsjs8z94dy5i8x3hq9w3kzr9fiwnvdvkj59s6x4f6rjc4y") (r "1.58.1")))

(define-public crate-dsalgo-0.2.2 (c (n "dsalgo") (v "0.2.2") (h "0vyfrd923jjqzislxj8a5y60d1r5jab7f1xg8hwc37iw4njnigjq") (r "1.58.1")))

(define-public crate-dsalgo-0.2.3 (c (n "dsalgo") (v "0.2.3") (h "16l15lqvwglif1w7nb7bfnbpgh7bk4w8wc4i0gyv44dlg8g93ycg") (r "1.58.1")))

(define-public crate-dsalgo-0.2.4 (c (n "dsalgo") (v "0.2.4") (h "1113lmhwwyrml8cqyfhib0fj9g97q2wr0y6kghyyg0f503rk1pgn") (r "1.58.1")))

(define-public crate-dsalgo-0.2.5 (c (n "dsalgo") (v "0.2.5") (h "0h9b77730a9yjprbz44rw8nnax09s6kg23d6y8ma7v3kkijdcr4y") (r "1.58.1")))

(define-public crate-dsalgo-0.2.6 (c (n "dsalgo") (v "0.2.6") (h "1hba6zz3gm00llpnhs4wz62d2b9f8nvv570vm44b7b03xs309gy8") (r "1.58.1")))

(define-public crate-dsalgo-0.2.7 (c (n "dsalgo") (v "0.2.7") (h "06zvx2dkhvj97vxcrggng6rv9yq34cy06jvnwnxk9w685156vlfb") (r "1.58.1")))

(define-public crate-dsalgo-0.2.8 (c (n "dsalgo") (v "0.2.8") (h "1pwndwr0y3m9aldrkxfrc112jfg2z1fadi6kg50q4ybcbckcllii") (r "1.58.1")))

(define-public crate-dsalgo-0.2.9 (c (n "dsalgo") (v "0.2.9") (h "16c7bgc9dx5nqaja5w0cws3rrf2l7s5gw1mxbyqd55mh89kl92h1") (r "1.58.1")))

(define-public crate-dsalgo-0.2.10 (c (n "dsalgo") (v "0.2.10") (h "0sfnf33nja240mhapyizxr305xv0h1p6j9w2d3vmpc8a9ah43jnb") (r "1.58.1")))

(define-public crate-dsalgo-0.2.11 (c (n "dsalgo") (v "0.2.11") (h "00vp3968vi58jipmgzqwiway0mdgpzj041m304p5pvxvyp5nvkqp") (r "1.58.1")))

(define-public crate-dsalgo-0.2.12 (c (n "dsalgo") (v "0.2.12") (h "0r9h5c22bmix33l0a4yj2aw2krma039b0nky1az8x6ipy763h6df") (r "1.58.1")))

(define-public crate-dsalgo-0.2.13 (c (n "dsalgo") (v "0.2.13") (h "1x7nasw2ngb9lzjd6cia4ac0gm584hp6ljaik987qhjq17szr30p") (r "1.58.1")))

(define-public crate-dsalgo-0.2.14 (c (n "dsalgo") (v "0.2.14") (h "18xljhbg624kyx2nizmzn5x76x04ay5i6hwn9l4w5svc42f3bcy0") (r "1.58.1")))

(define-public crate-dsalgo-0.2.15 (c (n "dsalgo") (v "0.2.15") (h "0shxb987m1758b499sdj3b6cd6mzzpiz4kr1hjg10cahm05b3sz5") (r "1.58.1")))

(define-public crate-dsalgo-0.2.16 (c (n "dsalgo") (v "0.2.16") (h "143ab46h851788r1kik56wnrxflxzz173dqq924yy15l1a1xdavr") (r "1.58.1")))

(define-public crate-dsalgo-0.2.17 (c (n "dsalgo") (v "0.2.17") (h "07m09nd26gyck08nwdv5g4rp6cyb2v3k29ip0nzixhyn8k94y70n") (r "1.58.1")))

(define-public crate-dsalgo-0.2.18 (c (n "dsalgo") (v "0.2.18") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1ayzbvrwhaza9a80sv4zwkql6ya6ccg966filll5h65r8gxamhwi") (r "1.58.1")))

(define-public crate-dsalgo-0.2.19 (c (n "dsalgo") (v "0.2.19") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1vza34r7j689fmnwijyglih431rbz7s12ksjga1wmd003x5xyz5y") (r "1.58.1")))

(define-public crate-dsalgo-0.2.20 (c (n "dsalgo") (v "0.2.20") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1jg7izllbix2vicng2s802nj6qr7rfmmdf3wn2n9cj4q3xh5b6yz") (r "1.58.1")))

(define-public crate-dsalgo-0.2.21 (c (n "dsalgo") (v "0.2.21") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0gaifaddqsixqdqc9kkhzkd1060mqfy785jf2cvpi4cjnx5nv3my") (r "1.58.1")))

(define-public crate-dsalgo-0.2.22 (c (n "dsalgo") (v "0.2.22") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "10invrac0j9crcjmnj1k3jx5way7v25y90kwjvbfkx2ngld8a24w") (r "1.58.1")))

(define-public crate-dsalgo-0.2.23 (c (n "dsalgo") (v "0.2.23") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1x8320v89jc45i8gf0jqs6c2rs6iw3pq66nf460m21m9xwz3m8jc") (r "1.58.1")))

(define-public crate-dsalgo-0.2.24 (c (n "dsalgo") (v "0.2.24") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1fk2y5glpx61bzyi8gindi36v09q35hr7c6fky23z6hcaxw4irdd") (r "1.58.1")))

(define-public crate-dsalgo-0.2.25 (c (n "dsalgo") (v "0.2.25") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1ys3vgwgf7p61i2imjyh024ddxpzn1rm20i98ak4bs577ggq1xr4") (r "1.58.1")))

(define-public crate-dsalgo-0.2.26 (c (n "dsalgo") (v "0.2.26") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1zgk0q3kcsw2x4pjwiazcw29ja55hz663ab1wpgiys6r6z6s0aq1") (r "1.58.1")))

(define-public crate-dsalgo-0.2.27 (c (n "dsalgo") (v "0.2.27") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0p5qkwgmrkqp182jc463308x7m99l1b5bywcbv3ldd3kfm6mxww5") (r "1.58.1")))

(define-public crate-dsalgo-0.3.0 (c (n "dsalgo") (v "0.3.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0xz05sbm9yirfk9126i2ica90knl1696ksmslmhchsgrkkkn0dv7") (r "1.58.1")))

(define-public crate-dsalgo-0.3.1 (c (n "dsalgo") (v "0.3.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0f7sad8qcnmi9cryislp0flq04ndxxi5617rarxqs3n2qrazdsqd") (r "1.63.0")))

(define-public crate-dsalgo-0.3.2 (c (n "dsalgo") (v "0.3.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1g3gq2p9b0fk7rypprki2mnf2d3wlvvli9103dqd1w0cpg2j6lkl") (r "1.63.0")))

(define-public crate-dsalgo-0.3.3 (c (n "dsalgo") (v "0.3.3") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0w5q6vcsbyri3j4jkxbpyhyhx12pm6492901q39x6irar3y6apqr") (r "1.63.0")))

(define-public crate-dsalgo-0.3.4 (c (n "dsalgo") (v "0.3.4") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1zd0gf7fibppazaawjwvvmhi5y6zc1jd2fhs0m48r02bh79zv21a") (r "1.63.0")))

(define-public crate-dsalgo-0.3.5 (c (n "dsalgo") (v "0.3.5") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0786h1djcvzlp4cnvgngi2hb58qik6v3p4h1p3rpcxg4pagwyh1c") (r "1.63.0")))

(define-public crate-dsalgo-0.3.6 (c (n "dsalgo") (v "0.3.6") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1qk5wilw48yqmd89p6rvj41bbzlwrm37b8hyw54l8c9n4w14fr42") (r "1.63.0")))

(define-public crate-dsalgo-0.3.7 (c (n "dsalgo") (v "0.3.7") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0srapxcks5yysmmxpvvrsccjycav04fpymgsmqaqja3ph5xfg6kr") (r "1.63.0")))

(define-public crate-dsalgo-0.3.8 (c (n "dsalgo") (v "0.3.8") (h "0vdmws3qkg5x00zglg771pdhq61jmhxlnr4zb3l3xdjxd104ap9n")))

(define-public crate-dsalgo-0.3.9 (c (n "dsalgo") (v "0.3.9") (h "1i7n36lgaxchfqmp53n8jl2yanijazlzj1wdlk6xafkkvwpa6gk4")))

(define-public crate-dsalgo-0.3.10 (c (n "dsalgo") (v "0.3.10") (h "05bh8hqa1him947lf5fqlbicqf9xqcils81w72lv8fxv5zz25rhz")))

