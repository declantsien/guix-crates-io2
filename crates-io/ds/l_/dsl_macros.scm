(define-module (crates-io ds l_ dsl_macros) #:use-module (crates-io))

(define-public crate-dsl_macros-0.1.0 (c (n "dsl_macros") (v "0.1.0") (h "16fynw215a1ndlavkpkhi8af2hnwj7lx8a208jk455wzhm0mvks5")))

(define-public crate-dsl_macros-0.1.1 (c (n "dsl_macros") (v "0.1.1") (h "0gnrd92wi4nraiwyy8jvmmbj4iiyy2n4yn9xbkxdxnj41q2mqim2")))

(define-public crate-dsl_macros-0.1.2 (c (n "dsl_macros") (v "0.1.2") (h "0i43cnl3cziv3wpcjhww2flbcwnpzj0pm2jdprxrdn0rpb6vdvjd")))

