(define-module (crates-io ds tv dstv) #:use-module (crates-io))

(define-public crate-dstv-0.1.0 (c (n "dstv") (v "0.1.0") (h "18wcfbd7pzbqx5yqxw4ayx4gvmayr7j5kcz6amk2cjxnc16qw51s") (r "1.56")))

(define-public crate-dstv-0.2.0 (c (n "dstv") (v "0.2.0") (h "1gkb8mgyjw4gi4p2pkzbzjx1blj8z5236n5c8d72b874z15i2svz") (r "1.56")))

(define-public crate-dstv-0.3.0 (c (n "dstv") (v "0.3.0") (h "03dhr9vwigxi66sv358c669a5dqwmw21g5c5yg2qz7khpdjwgg36") (r "1.56")))

(define-public crate-dstv-0.4.0 (c (n "dstv") (v "0.4.0") (h "1m5w3j8lvilbwvpz18dxbzlfmvhn2blrr29pw6d1yn6zj0rvwch0") (r "1.56")))

(define-public crate-dstv-0.5.0 (c (n "dstv") (v "0.5.0") (h "0683rbf5plc7347k2zdkqmx2gsz4bp68l8z6dm8y26c0wcs8fbry") (r "1.56")))

