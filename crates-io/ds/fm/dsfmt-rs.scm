(define-module (crates-io ds fm dsfmt-rs) #:use-module (crates-io))

(define-public crate-dsfmt-rs-0.0.1 (c (n "dsfmt-rs") (v "0.0.1") (h "14c2r130nry20c7nkxa3ndjsbhc3ss24ji0wg78hvpqw01jw6jag")))

(define-public crate-dsfmt-rs-0.0.2 (c (n "dsfmt-rs") (v "0.0.2") (h "1hzj166andir00wl1m7j14r097z0yh209l7wf1465nksix4c47n9")))

(define-public crate-dsfmt-rs-0.1.0 (c (n "dsfmt-rs") (v "0.1.0") (h "0wdnwn42wvl1x4wx0f88dmgdx3xnrviismkqbhgr9cykwlpcqwrk")))

(define-public crate-dsfmt-rs-0.1.1 (c (n "dsfmt-rs") (v "0.1.1") (d (list (d (n "rand") (r "^0.1") (d #t) (k 0)))) (h "1jrqspp9kn811m93kssggih2l8h3jdv77g5m6p63lg3q8rbl41m7")))

