(define-module (crates-io ds fs dsfs) #:use-module (crates-io))

(define-public crate-dsfs-0.0.1 (c (n "dsfs") (v "0.0.1") (d (list (d (n "rocket") (r "^0.4.10") (d #t) (k 0)) (d (n "rocket_contrib") (r "^0.4.10") (d #t) (k 0)))) (h "1hb3nf3ri85flfqrghvjgh6lp1liq1q9c987z5s99ljhmh85n3i2")))

(define-public crate-dsfs-0.0.2 (c (n "dsfs") (v "0.0.2") (d (list (d (n "rocket") (r "^0.4.10") (d #t) (k 0)) (d (n "rocket_contrib") (r "^0.4.10") (d #t) (k 0)))) (h "1f7l77flb68k1qha15x0gbybw4x0ypq9jfgk8pdgsda592f8qnj9")))

(define-public crate-dsfs-0.0.3 (c (n "dsfs") (v "0.0.3") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "rocket") (r "^0.4.10") (d #t) (k 0)) (d (n "rocket_contrib") (r "^0.4.10") (d #t) (k 0)))) (h "10gxn8155x6i551zvsxckcn94x6hibgiyh6nkqx0zyf88rp3irgm")))

