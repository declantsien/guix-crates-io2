(define-module (crates-io ds #{13}# ds1307) #:use-module (crates-io))

(define-public crate-ds1307-0.1.0 (c (n "ds1307") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.2") (d #t) (k 2)) (d (n "linux-embedded-hal") (r "^0.2") (d #t) (k 2)))) (h "1gn79zcz6hf4yvzw5r77igs6ibwif5zx20lz81f62b9y8w4hq8rp") (f (quote (("default"))))))

(define-public crate-ds1307-0.2.0 (c (n "ds1307") (v "0.2.0") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.2") (d #t) (k 2)) (d (n "linux-embedded-hal") (r "^0.2") (d #t) (k 2)))) (h "00sdi2rdhgsp5dn2iwm0khm9yclr62dycig9pvga44fl27kl5n2q") (f (quote (("default"))))))

(define-public crate-ds1307-0.2.1 (c (n "ds1307") (v "0.2.1") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.2") (d #t) (k 2)) (d (n "linux-embedded-hal") (r "^0.2") (d #t) (k 2)))) (h "02rx3lsbsb32qhi1zazpp7pvb601l5i98lb0s0frj8pw02vdy2sv")))

(define-public crate-ds1307-0.3.0 (c (n "ds1307") (v "0.3.0") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.7") (d #t) (k 2)) (d (n "linux-embedded-hal") (r "^0.3") (d #t) (k 2)) (d (n "rtcc") (r "^0.2") (d #t) (k 0)))) (h "0hpd7cak73zgc8i1b3jy0144s3g8hdnc7h4k1myp158wvh7hslsc")))

(define-public crate-ds1307-0.4.0 (c (n "ds1307") (v "0.4.0") (d (list (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.8") (d #t) (k 2)) (d (n "linux-embedded-hal") (r "^0.3.2") (d #t) (k 2)) (d (n "rtcc") (r "^0.3") (d #t) (k 0)))) (h "1q16wsqqkmj30s6qai474q57pvvxxp6k0j9dbnf8px20nbi5bxsa")))

(define-public crate-ds1307-0.5.0 (c (n "ds1307") (v "0.5.0") (d (list (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.9") (d #t) (k 2)) (d (n "linux-embedded-hal") (r "^0.3.2") (d #t) (k 2)) (d (n "rtcc") (r "^0.3") (d #t) (k 0)))) (h "17wvylgy9kf6q4723j0hk7nin1xx4vqqcaz0icqa6bdmd3wal6qi") (r "1.62")))

(define-public crate-ds1307-0.6.0 (c (n "ds1307") (v "0.6.0") (d (list (d (n "embedded-hal") (r "^1.0") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.10") (f (quote ("eh1"))) (k 2)) (d (n "linux-embedded-hal") (r "^0.4") (d #t) (k 2)) (d (n "rtcc") (r "^0.3") (d #t) (k 0)))) (h "113b18k1frm3gdzc66qqvsk693jhhvfhm1a76bfvdbv6dsc1xfvq") (r "1.62")))

