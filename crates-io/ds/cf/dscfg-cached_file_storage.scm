(define-module (crates-io ds cf dscfg-cached_file_storage) #:use-module (crates-io))

(define-public crate-dscfg-cached_file_storage-0.1.0 (c (n "dscfg-cached_file_storage") (v "0.1.0") (d (list (d (n "dscfg-server") (r "^0.1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "void") (r "^1") (d #t) (k 0)))) (h "1f1pi261k71pfgvdsbyh5pfw495f4g90wmxk31ps4gg9q9bqifqa")))

