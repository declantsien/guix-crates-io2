(define-module (crates-io ds cf dscfg-server) #:use-module (crates-io))

(define-public crate-dscfg-server-0.1.0 (c (n "dscfg-server") (v "0.1.0") (d (list (d (n "dscfg-proto") (r "^0.1") (f (quote ("server"))) (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "same") (r "^0.1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "slog") (r "^2") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)) (d (n "void") (r "^1") (d #t) (k 0)))) (h "0giw3vkh77g99hnrkng0k2pc9x31d0h6zmx4bl9b1vqm4smcf1wa")))

