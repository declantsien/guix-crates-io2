(define-module (crates-io ds cf dscfg-proto) #:use-module (crates-io))

(define-public crate-dscfg-proto-0.1.0 (c (n "dscfg-proto") (v "0.1.0") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "10b3k2bz9ih9g3pyrfz40x4kdk387y5688dlscqv5g6hlld7ws10") (f (quote (("server") ("default") ("client"))))))

