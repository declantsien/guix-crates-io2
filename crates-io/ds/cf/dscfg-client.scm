(define-module (crates-io ds cf dscfg-client) #:use-module (crates-io))

(define-public crate-dscfg-client-0.1.0 (c (n "dscfg-client") (v "0.1.0") (d (list (d (n "dscfg-proto") (r "^0.1") (f (quote ("client"))) (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)))) (h "142c2dik4rqszgj1sivr0nv0aa7rkf0ss9wz0dnj8wf4gbhdfpnp")))

