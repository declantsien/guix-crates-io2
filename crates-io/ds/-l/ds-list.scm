(define-module (crates-io ds -l ds-list) #:use-module (crates-io))

(define-public crate-ds-list-0.1.0 (c (n "ds-list") (v "0.1.0") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "15rqf1w1mphqwm3r7dpgaka458hf91lzdpb6zlfxz6fg22zr6jdi")))

(define-public crate-ds-list-1.1.0 (c (n "ds-list") (v "1.1.0") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "11p992bgqrhphzbcab0yhzl7bqqk4iwnlq7b64n26gy0s2wbaklz")))

(define-public crate-ds-list-2.1.0 (c (n "ds-list") (v "2.1.0") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "0j1460srdz62zxijk8ivxw5gzdnylkb7hpvj70kpxfad27lc3bjc")))

