(define-module (crates-io ds -l ds-learn-rust) #:use-module (crates-io))

(define-public crate-ds-learn-rust-0.1.0 (c (n "ds-learn-rust") (v "0.1.0") (h "1kv7fmrdzzjwa3sh0kpni3dsyyl5ppyw1ylmrcqij7jz6xnxv7s4")))

(define-public crate-ds-learn-rust-0.1.1 (c (n "ds-learn-rust") (v "0.1.1") (h "1sk7g8x23sh2b0n2nac42mdhl9hi22rhkl0gfabc7l54g0r2q5jn")))

