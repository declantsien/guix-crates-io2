(define-module (crates-io ds yn dsync-hasezoey) #:use-module (crates-io))

(define-public crate-dsync-hasezoey-0.1.0 (c (n "dsync-hasezoey") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (f (quote ("backtrace"))) (d #t) (k 0)) (d (n "clap") (r "^4.3") (f (quote ("derive" "wrap_help"))) (d #t) (k 0)) (d (n "clap_complete") (r "^4.3") (d #t) (k 0)) (d (n "libdsync-hasezoey") (r "^0.1.0") (d #t) (k 0)))) (h "02wn9glkfhf8ybc3fyfw79ybjvpxr7hjwc1h48vs7jmcl3kbx6pg") (f (quote (("tsync" "libdsync-hasezoey/tsync") ("default" "tsync" "backtrace") ("backtrace" "libdsync-hasezoey/backtrace") ("async" "libdsync-hasezoey/async"))))))

(define-public crate-dsync-hasezoey-0.2.0 (c (n "dsync-hasezoey") (v "0.2.0") (d (list (d (n "anyhow") (r "^1") (f (quote ("backtrace"))) (d #t) (k 0)) (d (n "clap") (r "^4.3") (f (quote ("derive" "wrap_help"))) (d #t) (k 0)) (d (n "clap_complete") (r "^4.3") (d #t) (k 0)) (d (n "libdsync-hasezoey") (r "^0.2.0") (d #t) (k 0)))) (h "0kv1q22qiycxlgv9mmwbr01xii8cmhxg6mjy0lkd31fj265abxqn") (f (quote (("tsync" "libdsync-hasezoey/tsync") ("default" "tsync" "backtrace") ("backtrace" "libdsync-hasezoey/backtrace") ("async" "libdsync-hasezoey/async"))))))

