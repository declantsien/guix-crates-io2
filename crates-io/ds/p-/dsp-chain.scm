(define-module (crates-io ds p- dsp-chain) #:use-module (crates-io))

(define-public crate-dsp-chain-0.2.0 (c (n "dsp-chain") (v "0.2.0") (d (list (d (n "sound_stream") (r "*") (d #t) (k 0)))) (h "0pb2qg22m1jlckzdw3v490hwi9f6zi25ahrlrbdk89j9fj4gqzwf")))

(define-public crate-dsp-chain-0.3.1 (c (n "dsp-chain") (v "0.3.1") (d (list (d (n "sound_stream") (r "*") (d #t) (k 0)))) (h "1v3v5n45q8m5kkiym3275lfcwicn61ydrnbhxh89173r47nq39vm")))

(define-public crate-dsp-chain-0.4.0 (c (n "dsp-chain") (v "0.4.0") (d (list (d (n "petgraph") (r "*") (d #t) (k 0)) (d (n "sound_stream") (r "*") (d #t) (k 0)))) (h "0n5zjn2i8372c98ni1nc6cl9l76ss5dlikc41siancdsnsi1y453")))

(define-public crate-dsp-chain-0.4.1 (c (n "dsp-chain") (v "0.4.1") (d (list (d (n "petgraph") (r "*") (d #t) (k 0)) (d (n "sound_stream") (r "*") (d #t) (k 0)))) (h "1mjf8zbgby2a9lmwn54pxd4k2yyvkr2bkvq1qc9aq5wmycf5b3bj")))

(define-public crate-dsp-chain-0.4.2 (c (n "dsp-chain") (v "0.4.2") (d (list (d (n "petgraph") (r "*") (d #t) (k 0)) (d (n "sound_stream") (r "*") (d #t) (k 0)))) (h "17i9p1kz8k5jg72xp39qph20y1a5swpjs9sjzhv5bd18p7slv8qf")))

(define-public crate-dsp-chain-0.4.3 (c (n "dsp-chain") (v "0.4.3") (d (list (d (n "petgraph") (r "*") (d #t) (k 0)) (d (n "sound_stream") (r "*") (d #t) (k 0)))) (h "0kgb67wp18j3w4557d5rnqpykwillphvqilw5w3nn5z6hd6is39b")))

(define-public crate-dsp-chain-0.4.4 (c (n "dsp-chain") (v "0.4.4") (d (list (d (n "petgraph") (r "*") (d #t) (k 0)) (d (n "sound_stream") (r "*") (d #t) (k 0)))) (h "129awh05szlai2rp3hc7bz0i5azhzkcpj5ccgpza9wsfnb28nqia")))

(define-public crate-dsp-chain-0.4.5 (c (n "dsp-chain") (v "0.4.5") (d (list (d (n "petgraph") (r "*") (d #t) (k 0)) (d (n "sound_stream") (r "*") (d #t) (k 0)))) (h "1034syiw5rx96fr6wll1hhz1md0ya226vcg93kvbafbjy4k03940")))

(define-public crate-dsp-chain-0.4.6 (c (n "dsp-chain") (v "0.4.6") (d (list (d (n "num") (r "*") (d #t) (k 0)) (d (n "petgraph") (r "*") (d #t) (k 0)) (d (n "sound_stream") (r "*") (d #t) (k 0)))) (h "0x9b047k6jxlzsk9wn6qbk4gmni6iwwcjns697fh0i2c7bqkn1c2")))

(define-public crate-dsp-chain-0.4.7 (c (n "dsp-chain") (v "0.4.7") (d (list (d (n "num") (r "*") (d #t) (k 0)) (d (n "petgraph") (r "*") (d #t) (k 0)) (d (n "sound_stream") (r "*") (d #t) (k 0)))) (h "0xl219wp6rl45mgr3cfpdiyrhf435m1w08rjw0dsxcd50icly491")))

(define-public crate-dsp-chain-0.4.8 (c (n "dsp-chain") (v "0.4.8") (d (list (d (n "num") (r "*") (d #t) (k 0)) (d (n "petgraph") (r "*") (d #t) (k 0)) (d (n "sound_stream") (r "*") (d #t) (k 0)))) (h "1y33whwx9pnq70nsvdjcrj5icjizhcf5llk871gqcpqr8668v8n6")))

(define-public crate-dsp-chain-0.5.0 (c (n "dsp-chain") (v "0.5.0") (d (list (d (n "num") (r "*") (d #t) (k 0)) (d (n "petgraph") (r "*") (d #t) (k 0)) (d (n "sound_stream") (r "*") (d #t) (k 0)))) (h "0c8b9jzldxs04dbrw6cny2n2zybdq784c8j8llc6jd28kb6gjdyn")))

(define-public crate-dsp-chain-0.5.1 (c (n "dsp-chain") (v "0.5.1") (d (list (d (n "num") (r "*") (d #t) (k 0)) (d (n "petgraph") (r "*") (d #t) (k 0)) (d (n "sound_stream") (r "*") (d #t) (k 0)))) (h "0nkj7rj6v1d63rkv5qg18x261h5gfafq2hrfhdg08rps2nlqy4fc")))

(define-public crate-dsp-chain-0.5.2 (c (n "dsp-chain") (v "0.5.2") (d (list (d (n "num") (r "*") (k 0)) (d (n "petgraph") (r "*") (d #t) (k 0)) (d (n "sound_stream") (r "*") (d #t) (k 0)))) (h "11gnbs655gxyywfiizbgkf3yh5b4g4pmlyrj6wiaqzcszrm75941")))

(define-public crate-dsp-chain-0.5.3 (c (n "dsp-chain") (v "0.5.3") (d (list (d (n "num") (r "*") (k 0)) (d (n "petgraph") (r "*") (d #t) (k 0)) (d (n "sound_stream") (r "*") (d #t) (k 0)))) (h "06wg12pkcanl7bw9wa4z9nmnzb5q4bhvn20c6nhq9v4igy6cg3qi")))

(define-public crate-dsp-chain-0.5.4 (c (n "dsp-chain") (v "0.5.4") (d (list (d (n "num") (r "*") (k 0)) (d (n "petgraph") (r "*") (d #t) (k 0)) (d (n "sound_stream") (r "*") (d #t) (k 0)))) (h "0crp1ysjx5srd4kw7c9blrdv4jc1p3fga3vnw5f2xzf6674frxks")))

(define-public crate-dsp-chain-0.5.5 (c (n "dsp-chain") (v "0.5.5") (d (list (d (n "num") (r "*") (k 0)) (d (n "petgraph") (r "*") (d #t) (k 0)) (d (n "sound_stream") (r "^0.4.4") (d #t) (k 0)))) (h "05sx617nbgf807dv44ia7fdgf9b7klzwvxrq816k08hn9p33ki7r")))

(define-public crate-dsp-chain-0.5.6 (c (n "dsp-chain") (v "0.5.6") (d (list (d (n "num") (r "*") (k 0)) (d (n "petgraph") (r "*") (d #t) (k 0)) (d (n "sound_stream") (r "^0.4.4") (d #t) (k 0)))) (h "03grg6sd4a4izky1q77v2fbm4rdsl82nmh0kyimmm1dvi68kmxbi")))

(define-public crate-dsp-chain-0.6.0 (c (n "dsp-chain") (v "0.6.0") (d (list (d (n "num") (r "^0.1.27") (k 0)) (d (n "petgraph") (r "^0.1.11") (d #t) (k 0)) (d (n "sound_stream") (r "^0.4.4") (d #t) (k 0)))) (h "0zsq7kg9i6nyynknnj1wc8diz6yjsm4d4klj9l7hx0v2j31kx88v")))

(define-public crate-dsp-chain-0.7.0 (c (n "dsp-chain") (v "0.7.0") (d (list (d (n "daggy") (r "^0.1.3") (d #t) (k 0)) (d (n "num") (r "^0.1.27") (k 0)) (d (n "sound_stream") (r "^0.4.4") (d #t) (k 0)))) (h "100vbjxdibhmyyfy1ksw03zibknvrr7id6rvwg81ki5z8l2gfw4a")))

(define-public crate-dsp-chain-0.7.1 (c (n "dsp-chain") (v "0.7.1") (d (list (d (n "daggy") (r "^0.1.3") (d #t) (k 0)) (d (n "num") (r "^0.1.27") (k 0)) (d (n "sound_stream") (r "^0.4.4") (d #t) (k 0)))) (h "0sy8z43m6360xmdxi9q3zlwykzz8rxvp2psb7sdbdq8g7xvik7ia")))

(define-public crate-dsp-chain-0.7.2 (c (n "dsp-chain") (v "0.7.2") (d (list (d (n "daggy") (r "^0.1.4") (d #t) (k 0)) (d (n "num") (r "^0.1.27") (k 0)) (d (n "sound_stream") (r "^0.4.4") (d #t) (k 0)))) (h "0ik1hidkh8kgifr92apnrfflymxzjjrflmhraly5vbprf8jfv0y6")))

(define-public crate-dsp-chain-0.8.0 (c (n "dsp-chain") (v "0.8.0") (d (list (d (n "daggy") (r "^0.1.4") (d #t) (k 0)) (d (n "num") (r "^0.1.27") (k 0)) (d (n "sound_stream") (r "^0.5.0") (d #t) (k 0)))) (h "1cbb4076371ni5xbx4n3lj8bvb5yj726na2bnk6qk9iycg0sccih")))

(define-public crate-dsp-chain-0.8.1 (c (n "dsp-chain") (v "0.8.1") (d (list (d (n "daggy") (r "^0.1.4") (d #t) (k 0)) (d (n "num") (r "^0.1.27") (k 0)) (d (n "sound_stream") (r "^0.6.0") (d #t) (k 0)))) (h "0vvb9sdfs12995kcy5w2bxzfp51bwzwidk5ksddg14ncygqh98kd") (y #t)))

(define-public crate-dsp-chain-0.9.0 (c (n "dsp-chain") (v "0.9.0") (d (list (d (n "daggy") (r "^0.1.4") (d #t) (k 0)) (d (n "num") (r "^0.1.27") (k 0)) (d (n "sound_stream") (r "^0.6.0") (d #t) (k 0)))) (h "0jdsi98l8afjqcwd70sq7b3cdl68324ncf9hnbr3r05k9h3kx99h")))

(define-public crate-dsp-chain-0.10.0 (c (n "dsp-chain") (v "0.10.0") (d (list (d (n "daggy") (r "^0.1.4") (d #t) (k 0)) (d (n "portaudio") (r "^0.6.1") (d #t) (k 2)) (d (n "sample") (r "^0.2.0") (d #t) (k 0)))) (h "0mx3wb6f3slfviqvcgwfis3qf51lp6hd24vybvvgq6ibqby7gip2")))

(define-public crate-dsp-chain-0.11.0 (c (n "dsp-chain") (v "0.11.0") (d (list (d (n "daggy") (r "^0.4.0") (d #t) (k 0)) (d (n "portaudio") (r "^0.6.2") (d #t) (k 2)) (d (n "sample") (r "^0.2.0") (d #t) (k 0)))) (h "1dh8lqzhipls11rg0k9xm1bzb3xmsf7925r4psim76xg0wr83ycg")))

(define-public crate-dsp-chain-0.12.0 (c (n "dsp-chain") (v "0.12.0") (d (list (d (n "daggy") (r "^0.4.0") (d #t) (k 0)) (d (n "portaudio") (r "^0.6.3") (d #t) (k 2)) (d (n "sample") (r "^0.3.0") (d #t) (k 0)))) (h "0aciaj3w1p6xwc1wa976s74zk59lsgas7g8i32mrb4ks1hqsd3s6")))

(define-public crate-dsp-chain-0.13.0 (c (n "dsp-chain") (v "0.13.0") (d (list (d (n "daggy") (r "^0.4.0") (d #t) (k 0)) (d (n "portaudio") (r "^0.6.4") (d #t) (k 2)) (d (n "sample") (r "^0.6.0") (d #t) (k 0)))) (h "03qwsx6hbwnqqdmv6jxhln0sdfxfhx1acrdpnywjkc8wim8660ln")))

(define-public crate-dsp-chain-0.13.1 (c (n "dsp-chain") (v "0.13.1") (d (list (d (n "daggy") (r "^0.4.0") (d #t) (k 0)) (d (n "portaudio") (r "^0.6.4") (d #t) (k 2)) (d (n "sample") (r "^0.6.0") (d #t) (k 0)))) (h "05vyl4q76m4mm1jwd9fip9i1c303dxd1lfs0a2zzzbr7ayqvnlaj")))

