(define-module (crates-io ds #{32}# ds323x) #:use-module (crates-io))

(define-public crate-ds323x-0.1.0 (c (n "ds323x") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.4") (d #t) (k 2)) (d (n "linux-embedded-hal") (r "^0.2") (d #t) (k 2)))) (h "1xmjl29bdbhkldr25x9cm0v2h2qvz2a86lnqk4rz6jr4kjkihw5s")))

(define-public crate-ds323x-0.2.0 (c (n "ds323x") (v "0.2.0") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.4") (d #t) (k 2)) (d (n "linux-embedded-hal") (r "^0.2") (d #t) (k 2)))) (h "0sm17hzgvnnjhdqqdgr0211j8x67r72v0dsns6j2067kmdf5a4fd")))

(define-public crate-ds323x-0.3.0 (c (n "ds323x") (v "0.3.0") (d (list (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.7") (d #t) (k 2)) (d (n "linux-embedded-hal") (r "^0.3") (d #t) (k 2)) (d (n "rtcc") (r "^0.2") (d #t) (k 0)))) (h "0c5c501fjgg2lhm2i3xz35xz1kzb31ma0z19v28pbfgfxr0mpwxf")))

(define-public crate-ds323x-0.3.1 (c (n "ds323x") (v "0.3.1") (d (list (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.7") (d #t) (k 2)) (d (n "linux-embedded-hal") (r "^0.3") (d #t) (k 2)) (d (n "rtcc") (r "^0.2") (d #t) (k 0)))) (h "0yaw8wcdpp0m3692vyhl3bghm9mvf8fca83crkvsv23kxxd7yl2k")))

(define-public crate-ds323x-0.3.2 (c (n "ds323x") (v "0.3.2") (d (list (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.7") (d #t) (k 2)) (d (n "linux-embedded-hal") (r "^0.3") (d #t) (k 2)) (d (n "rtcc") (r "^0.2") (d #t) (k 0)))) (h "15cvrvbjg00vbkwgp3nvmsvaxq7lr3kj57k6hsk217w1raimrkcz")))

(define-public crate-ds323x-0.4.0 (c (n "ds323x") (v "0.4.0") (d (list (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.7") (d #t) (k 2)) (d (n "linux-embedded-hal") (r "^0.3") (d #t) (k 2)) (d (n "rtcc") (r "^0.2") (d #t) (k 0)))) (h "013b5fblcdm4r9abxw9ih8rw38pngd7ksnrhj4n5j783kndrfniy")))

(define-public crate-ds323x-0.5.0 (c (n "ds323x") (v "0.5.0") (d (list (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.8.0") (d #t) (k 2)) (d (n "linux-embedded-hal") (r "^0.3.2") (d #t) (k 2)) (d (n "rtcc") (r "^0.3") (d #t) (k 0)))) (h "0arsyw3lk00mlz3bw6rzqnq07qk0kiwrp8dpr79q7srv7b866zh5")))

(define-public crate-ds323x-0.5.1 (c (n "ds323x") (v "0.5.1") (d (list (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.9.0") (d #t) (k 2)) (d (n "linux-embedded-hal") (r "^0.3.2") (d #t) (k 2)) (d (n "rtcc") (r "^0.3") (d #t) (k 0)))) (h "19lhz1yyxxkd9mx2cp0yxh0f773bv3nr3dgj80drgyihqlb7q5fm")))

