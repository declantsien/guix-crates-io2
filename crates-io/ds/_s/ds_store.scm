(define-module (crates-io ds _s ds_store) #:use-module (crates-io))

(define-public crate-ds_store-0.1.0 (c (n "ds_store") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.2") (k 0)))) (h "1vz8kd1zlw76d2h1zfm38pav5fyr33q4gzqjkch8bnrlilkxlgvy")))

(define-public crate-ds_store-0.1.1 (c (n "ds_store") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.2") (k 0)))) (h "0r98r6zzsra3gjidw4zs2610ppy86i2bhh9dlfkv7i89xhq6sazx")))

(define-public crate-ds_store-0.1.2 (c (n "ds_store") (v "0.1.2") (d (list (d (n "byteorder") (r "^1.2") (k 0)))) (h "04vgm2zpdb23bhbj7glww0x42r16qjfx8kw463xirr9lk35kjhq3")))

(define-public crate-ds_store-0.2.0 (c (n "ds_store") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.2") (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "1nxra51ssaq6p0a88ryvzwvn564i3xrh6x5l9mqlpawgglxlwc3r")))

(define-public crate-ds_store-0.2.1 (c (n "ds_store") (v "0.2.1") (d (list (d (n "byteorder") (r "^1.2") (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "1klslpi6wq0dhczpw45869qx7n78xn6qn4q9vnv8zq1ifv1c0vmn")))

(define-public crate-ds_store-0.3.0 (c (n "ds_store") (v "0.3.0") (d (list (d (n "byteorder") (r "^1.2") (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "18hxyxplmc0kmdr3fs7cw65q994bzq50qxyyp5c7rc17aia6qxi1")))

