(define-module (crates-io ds ll dsll) #:use-module (crates-io))

(define-public crate-dsll-0.1.0 (c (n "dsll") (v "0.1.0") (h "05b3mw8k7dywv8zrb78053h9v4x7ympdm8m3sf0k2alxhy5ws0ag")))

(define-public crate-dsll-0.1.1 (c (n "dsll") (v "0.1.1") (h "0a6dz9v2b7zm4zkylhr9gb5rmhpls6qfxgsijzibi4zm9z1hqwvl")))

