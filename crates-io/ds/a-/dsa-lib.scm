(define-module (crates-io ds a- dsa-lib) #:use-module (crates-io))

(define-public crate-dsa-lib-0.1.0 (c (n "dsa-lib") (v "0.1.0") (d (list (d (n "indexmap") (r "^1.9.1") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.97") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "0pzkg2hnjbvkzi9clj2mqss65647sgypyiyv2dvk7apxmdmq7jkj")))

(define-public crate-dsa-lib-0.1.1 (c (n "dsa-lib") (v "0.1.1") (d (list (d (n "indexmap") (r "^1.9.1") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.97") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "0gai6qjbii94i7rixw41rnczi5kls1ibjyc5hzd0sp41lnlg63fg")))

(define-public crate-dsa-lib-0.1.2 (c (n "dsa-lib") (v "0.1.2") (d (list (d (n "indexmap") (r "^1.9.1") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.97") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "1l85hgrvnf7np6qz1vqxi386njm0m6y78ww39cjhkhcim96wn3yp")))

(define-public crate-dsa-lib-0.1.3 (c (n "dsa-lib") (v "0.1.3") (d (list (d (n "indexmap") (r "^1.9.1") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.97") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "0hh5006a5nfgmp9wrp6d3zghil4zcvidjscc810dlsa17vviqmkn")))

(define-public crate-dsa-lib-0.1.4 (c (n "dsa-lib") (v "0.1.4") (d (list (d (n "indexmap") (r "^1.9.1") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.97") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "0fgr5sbxyzvagw44l39j9yyckhs26v1bqwg4qvpwbl6b67vx2f7r")))

(define-public crate-dsa-lib-0.1.5 (c (n "dsa-lib") (v "0.1.5") (d (list (d (n "indexmap") (r "^1.9.1") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.97") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "0yb6qp0fzxysmlkxqwxp4yi3s60chmi48vmfgafg08yrff44iskz")))

(define-public crate-dsa-lib-0.1.6 (c (n "dsa-lib") (v "0.1.6") (d (list (d (n "indexmap") (r "^1.9.1") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.97") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "0izhwqc6hvnv466pb8qszc88byfgiwv66hrvk4r6v6b7g783g281")))

(define-public crate-dsa-lib-0.1.7 (c (n "dsa-lib") (v "0.1.7") (d (list (d (n "indexmap") (r "^1.9.1") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.97") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "152nzs990z2vnp0q0fdrw32pm397vxkw1wka2r5rhbx4k52yif37")))

(define-public crate-dsa-lib-0.1.8 (c (n "dsa-lib") (v "0.1.8") (d (list (d (n "indexmap") (r "^1.9.1") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.97") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "1w0fxa4yk0rp4ryld7zs5cxa7ixgrj9b4xxzh8z1548xr3363702")))

