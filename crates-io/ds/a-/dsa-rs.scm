(define-module (crates-io ds a- dsa-rs) #:use-module (crates-io))

(define-public crate-dsa-rs-0.1.0 (c (n "dsa-rs") (v "0.1.0") (h "0x1wmnagqlb4vz8dybzqgm30v9mg7646bx95lincadbfcymgdc51") (y #t)))

(define-public crate-dsa-rs-0.0.0 (c (n "dsa-rs") (v "0.0.0") (h "0fj8rrhqgzg6rh6gglra5sqyfrq1qwwy9bvr46ywyj35av47p73j") (y #t)))

(define-public crate-dsa-rs-0.1.1 (c (n "dsa-rs") (v "0.1.1") (h "03mska825gb7pbgx98acgjdsqqmsqc9bdh6mm8wilwrykmxmf2bh") (y #t)))

