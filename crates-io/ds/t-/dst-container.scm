(define-module (crates-io ds t- dst-container) #:use-module (crates-io))

(define-public crate-dst-container-0.1.0 (c (n "dst-container") (v "0.1.0") (d (list (d (n "dst-container-derive") (r "^0.1") (d #t) (k 0)))) (h "0zwzgynjaskaxvcd6f1qg59dxpf9a17a6z786vbsra0n990pidsj")))

(define-public crate-dst-container-0.1.1 (c (n "dst-container") (v "0.1.1") (d (list (d (n "dst-container-derive") (r "^0.1") (d #t) (k 0)))) (h "00rfxacldzzxqc6knyq0zhqasdbnp6x7zafk0pq08c0s76g60di3")))

