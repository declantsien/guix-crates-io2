(define-module (crates-io ds t- dst-container-derive) #:use-module (crates-io))

(define-public crate-dst-container-derive-0.1.0 (c (n "dst-container-derive") (v "0.1.0") (d (list (d (n "proc-macro-crate") (r "^1.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0vagbif3p7fg3x5j2hb2bjr49xx73dska0yyh0xk4rblqkqxcrwj")))

