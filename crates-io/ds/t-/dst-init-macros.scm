(define-module (crates-io ds t- dst-init-macros) #:use-module (crates-io))

(define-public crate-dst-init-macros-0.1.0 (c (n "dst-init-macros") (v "0.1.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "1.*") (d #t) (k 0)) (d (n "syn") (r "1.*") (f (quote ("full" "extra-traits" "derive"))) (d #t) (k 0)))) (h "132crw60clqp6q2ykr8s53clf0wm1kz2zx8xff95aj8b4b9vg2nr")))

(define-public crate-dst-init-macros-0.1.1 (c (n "dst-init-macros") (v "0.1.1") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "1.*") (d #t) (k 0)) (d (n "syn") (r "1.*") (f (quote ("full" "extra-traits" "derive"))) (d #t) (k 0)))) (h "041400q24825a5k4anq4ky25fav3gbk4mzd3naimn0fp6qhx22ky")))

(define-public crate-dst-init-macros-0.2.0 (c (n "dst-init-macros") (v "0.2.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "1.*") (d #t) (k 0)) (d (n "syn") (r "1.*") (f (quote ("full" "extra-traits" "derive"))) (d #t) (k 0)))) (h "17cyjba3j1r0ybr3l1k3qmdk7vybacdyb0l92qhfa4j12f6h45j0")))

(define-public crate-dst-init-macros-0.3.0 (c (n "dst-init-macros") (v "0.3.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "1.*") (d #t) (k 0)) (d (n "syn") (r "1.*") (f (quote ("full" "extra-traits" "derive"))) (d #t) (k 0)))) (h "1dgd7lfl4qnhn8mv6b6vw38ha8rps8qvm7h11s4jwf6a8zdnm56k")))

(define-public crate-dst-init-macros-0.4.0 (c (n "dst-init-macros") (v "0.4.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "1.*") (d #t) (k 0)) (d (n "syn") (r "1.*") (f (quote ("full" "extra-traits" "derive"))) (d #t) (k 0)))) (h "1k36p7rkp8v7yz36z3ll41n9h0cnskpgxppgsahxhakqagy6i633")))

