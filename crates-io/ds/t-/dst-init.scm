(define-module (crates-io ds t- dst-init) #:use-module (crates-io))

(define-public crate-dst-init-0.1.0 (c (n "dst-init") (v "0.1.0") (d (list (d (n "dst-init-macros") (r "^0.1.0") (d #t) (k 0)))) (h "1aqrh0rrzlq29ksvajhsinz0b7wxia2dk30qnrb000fgr2yc231h")))

(define-public crate-dst-init-0.1.1 (c (n "dst-init") (v "0.1.1") (d (list (d (n "dst-init-macros") (r "^0.1.0") (d #t) (k 0)))) (h "05v5s5fxicjbpsmx83zfbb0zba8phq37qsy09bq1widrq5cf2hq5") (y #t)))

(define-public crate-dst-init-0.2.0 (c (n "dst-init") (v "0.2.0") (d (list (d (n "dst-init-macros") (r "^0.1.0") (d #t) (k 0)))) (h "0mq6y001jd02r5kbdg6nj7p886knj6y6bxj4xgppxgv7nhkgn2m2") (y #t)))

(define-public crate-dst-init-0.3.0 (c (n "dst-init") (v "0.3.0") (d (list (d (n "dst-init-macros") (r "^0.3.0") (d #t) (k 0)))) (h "0qam3k49gwnhraf0z7w6w58zg8lrblfhdscwhzjww0rlwjk4vr1v")))

(define-public crate-dst-init-0.4.0 (c (n "dst-init") (v "0.4.0") (d (list (d (n "dst-init-macros") (r "^0.3.0") (d #t) (k 0)))) (h "0dkyysyf9hcp2ajx9w8m6sahsbniwflr07fjfa6lqi0zsaxx4ics")))

