(define-module (crates-io ds ar dsar) #:use-module (crates-io))

(define-public crate-dsar-0.1.0 (c (n "dsar") (v "0.1.0") (h "0ba0hn2b26h002nz8qygsbp6gw625iywwgzj13mr65wc24yx8ci3")))

(define-public crate-dsar-0.2.0 (c (n "dsar") (v "0.2.0") (h "0lal2x26lic0rp013ax2j5skbrhnkwqhns36gqm3rwrgkrcswfdx")))

