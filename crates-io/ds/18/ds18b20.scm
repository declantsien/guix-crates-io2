(define-module (crates-io ds #{18}# ds18b20) #:use-module (crates-io))

(define-public crate-ds18b20-0.1.0 (c (n "ds18b20") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2.3") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "one-wire-bus") (r "^0.1.1") (d #t) (k 0)))) (h "1g09366zdnwjx0lbcnsjfh6mpm8jxyzldqxifc9sw3a4jrpzf21g")))

(define-public crate-ds18b20-0.1.1 (c (n "ds18b20") (v "0.1.1") (d (list (d (n "embedded-hal") (r "^0.2.3") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "one-wire-bus") (r "^0.1.1") (d #t) (k 0)))) (h "1kjk3flsn4qyn1w7iiim87ir7r4da0sp3mjchz130v21w2n3gkf9")))

