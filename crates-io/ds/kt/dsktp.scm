(define-module (crates-io ds kt dsktp) #:use-module (crates-io))

(define-public crate-dsktp-0.1.0 (c (n "dsktp") (v "0.1.0") (d (list (d (n "clap") (r "^3.1.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1q9q9vy690b3xdl5svci03n7pk4bz534494ib5gcfv6zyp1y7c7k")))

