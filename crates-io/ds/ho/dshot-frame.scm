(define-module (crates-io ds ho dshot-frame) #:use-module (crates-io))

(define-public crate-dshot-frame-0.1.0 (c (n "dshot-frame") (v "0.1.0") (h "0h3v8grmcl833xka8ysbab5lb5gh0633yxzmbi82qafxxq1qqv41")))

(define-public crate-dshot-frame-0.1.1 (c (n "dshot-frame") (v "0.1.1") (h "1vhw1761zhmf1r9s921pim4nvf807ys227mcwrhl7vpd2h65yggf")))

(define-public crate-dshot-frame-0.1.2 (c (n "dshot-frame") (v "0.1.2") (h "13dil3iddgy1pgz7gziwa5620nk7znspplp9d7afdncss8n6456a")))

(define-public crate-dshot-frame-0.1.3 (c (n "dshot-frame") (v "0.1.3") (h "1q0y893ndf3dil3d490zhjxsgg09zqga8yjhc2ipz09k3pq3qrl4")))

