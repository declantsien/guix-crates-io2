(define-module (crates-io ds -r ds-rs) #:use-module (crates-io))

(define-public crate-ds-rs-0.1.0 (c (n "ds-rs") (v "0.1.0") (d (list (d (n "clap") (r "^4.3.0") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.8.3") (d #t) (k 0)))) (h "0d9ml2q9p7pyadvvx8ksbvvgv2sbk8nydsp1103j06lmckx8nww4")))

