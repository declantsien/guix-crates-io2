(define-module (crates-io ds ta dstat) #:use-module (crates-io))

(define-public crate-dstat-0.0.1 (c (n "dstat") (v "0.0.1") (d (list (d (n "colored") (r "^2") (d #t) (k 0)))) (h "1b9nhvx2d3kkbx572pfkgxxhcd48c2aa5sb2r7lv7d4fnqp5hxa6")))

(define-public crate-dstat-0.0.2 (c (n "dstat") (v "0.0.2") (d (list (d (n "colored") (r "^2") (d #t) (k 0)))) (h "0qwmia55fwcrx3p9ssy7np3db3af4w64m9pvvpr64j552xjrxya4")))

