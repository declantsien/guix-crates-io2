(define-module (crates-io ds nt dsntk-common) #:use-module (crates-io))

(define-public crate-dsntk-common-0.0.0 (c (n "dsntk-common") (v "0.0.0") (d (list (d (n "dsntk-macros") (r "^0.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.190") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uriparse") (r "^0.6.4") (d #t) (k 0)) (d (n "url") (r "^2.4.1") (d #t) (k 0)) (d (n "uuid") (r "^1.5.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "11liwq0i3jppyywrdpbm0ybrchrb26ygldfxs84nac4qdvqx50lg")))

(define-public crate-dsntk-common-0.0.1 (c (n "dsntk-common") (v "0.0.1") (d (list (d (n "dsntk-macros") (r "^0.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uriparse") (r "^0.6.4") (d #t) (k 0)) (d (n "url") (r "^2.4.1") (d #t) (k 0)) (d (n "uuid") (r "^1.5.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "0iysk4dgjdgx5gb9pkdphyyscsddvsggbclq81iq84h2459p8iws")))

(define-public crate-dsntk-common-0.0.2 (c (n "dsntk-common") (v "0.0.2") (d (list (d (n "dsntk-macros") (r "^0.0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uriparse") (r "^0.6.4") (d #t) (k 0)) (d (n "url") (r "^2.4.1") (d #t) (k 0)) (d (n "uuid") (r "^1.5.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "10c7xbbq0w3476aggjj78ls4k42vh38bx21j3a3sx2dmsrdngzl9")))

(define-public crate-dsntk-common-0.0.3 (c (n "dsntk-common") (v "0.0.3") (d (list (d (n "dsntk-macros") (r "^0.0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uriparse") (r "^0.6.4") (d #t) (k 0)) (d (n "url") (r "^2.5.0") (d #t) (k 0)) (d (n "uuid") (r "^1.6.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "09wigigm55bxyz5zdciwczqk2w8fr4f86sr0z1v1k0jcpw8yzk7z")))

(define-public crate-dsntk-common-0.0.4 (c (n "dsntk-common") (v "0.0.4") (d (list (d (n "dsntk-macros") (r "^0.0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uriparse") (r "^0.6.4") (d #t) (k 0)) (d (n "url") (r "^2.5.0") (d #t) (k 0)) (d (n "uuid") (r "^1.6.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "129124a76vw86nwwjgapg8v137f62is7jh3cf904jgrxwzknp6h2")))

(define-public crate-dsntk-common-0.0.5 (c (n "dsntk-common") (v "0.0.5") (d (list (d (n "dsntk-macros") (r "^0.0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.198") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uriparse") (r "^0.6.4") (d #t) (k 0)) (d (n "url") (r "^2.5.0") (d #t) (k 0)) (d (n "uuid") (r "^1.8.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "1vf4wzp6s58nr332fcd6gscwxazpy82ncpnbpng6rbpc9m4cvhp1")))

