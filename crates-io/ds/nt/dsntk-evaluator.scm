(define-module (crates-io ds nt dsntk-evaluator) #:use-module (crates-io))

(define-public crate-dsntk-evaluator-0.0.0 (c (n "dsntk-evaluator") (v "0.0.0") (d (list (d (n "dsntk-common") (r "^0.0.0") (d #t) (k 0)) (d (n "dsntk-feel") (r "^0.0.0") (d #t) (k 0)) (d (n "dsntk-feel-evaluator") (r "^0.0.0") (d #t) (k 0)) (d (n "dsntk-feel-parser") (r "^0.0.0") (d #t) (k 0)) (d (n "dsntk-model-evaluator") (r "^0.0.0") (d #t) (k 0)))) (h "14dfkfz93raab3pakwzaxy82y84b57zyx40hl83s5k45sl1ijlfp")))

(define-public crate-dsntk-evaluator-0.0.1 (c (n "dsntk-evaluator") (v "0.0.1") (d (list (d (n "dsntk-common") (r "^0.0.1") (d #t) (k 0)) (d (n "dsntk-feel") (r "^0.0.1") (d #t) (k 0)) (d (n "dsntk-feel-evaluator") (r "^0.0.1") (d #t) (k 0)) (d (n "dsntk-feel-parser") (r "^0.0.1") (d #t) (k 0)) (d (n "dsntk-model-evaluator") (r "^0.0.1") (d #t) (k 0)))) (h "0src89aifxiar2ba90w0hslb6hs5n9vilcpv6kp4f64h9vzbkpl5")))

(define-public crate-dsntk-evaluator-0.0.2 (c (n "dsntk-evaluator") (v "0.0.2") (d (list (d (n "dsntk-common") (r "^0.0.2") (d #t) (k 0)) (d (n "dsntk-feel") (r "^0.0.2") (d #t) (k 0)) (d (n "dsntk-feel-evaluator") (r "^0.0.2") (d #t) (k 0)) (d (n "dsntk-feel-parser") (r "^0.0.2") (d #t) (k 0)) (d (n "dsntk-model-evaluator") (r "^0.0.2") (d #t) (k 0)))) (h "15fqfrlms8g7fgnwagqhv8hgq77f0rq5kc1i44zvabym5wyv74dd")))

(define-public crate-dsntk-evaluator-0.0.3 (c (n "dsntk-evaluator") (v "0.0.3") (d (list (d (n "dsntk-common") (r "^0.0.3") (d #t) (k 0)) (d (n "dsntk-feel") (r "^0.0.3") (d #t) (k 0)) (d (n "dsntk-feel-evaluator") (r "^0.0.3") (d #t) (k 0)) (d (n "dsntk-feel-parser") (r "^0.0.3") (d #t) (k 0)) (d (n "dsntk-model-evaluator") (r "^0.0.3") (d #t) (k 0)))) (h "1v5fz6lnllx2bjigx5116490ckx11vhla3wl75xw3fnjzgpv4f77")))

(define-public crate-dsntk-evaluator-0.0.4 (c (n "dsntk-evaluator") (v "0.0.4") (d (list (d (n "dsntk-common") (r "^0.0.4") (d #t) (k 0)) (d (n "dsntk-feel") (r "^0.0.4") (d #t) (k 0)) (d (n "dsntk-feel-evaluator") (r "^0.0.4") (d #t) (k 0)) (d (n "dsntk-feel-parser") (r "^0.0.4") (d #t) (k 0)) (d (n "dsntk-model-evaluator") (r "^0.0.4") (d #t) (k 0)))) (h "1h2j1kmdnilycw2y467gjkymbnb0ygi5bi14j44f6papmzl63a98")))

(define-public crate-dsntk-evaluator-0.0.5 (c (n "dsntk-evaluator") (v "0.0.5") (d (list (d (n "dsntk-common") (r "^0.0.5") (d #t) (k 0)) (d (n "dsntk-feel") (r "^0.0.5") (d #t) (k 0)) (d (n "dsntk-feel-evaluator") (r "^0.0.5") (d #t) (k 0)) (d (n "dsntk-feel-parser") (r "^0.0.5") (d #t) (k 0)) (d (n "dsntk-model-evaluator") (r "^0.0.5") (d #t) (k 0)))) (h "0brpi738m1jz7bf31dlb6fc4y7c3rmlrzmhjjmlr087zhphd696f")))

