(define-module (crates-io ds nt dsntk-feel-grammar) #:use-module (crates-io))

(define-public crate-dsntk-feel-grammar-0.0.0 (c (n "dsntk-feel-grammar") (v "0.0.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "1ix2cbrrfk1wabbqn47cqx7syxnskl01mv11qbbzvapyr3l8mg6n")))

(define-public crate-dsntk-feel-grammar-0.0.1 (c (n "dsntk-feel-grammar") (v "0.0.1") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "0vz0b6831q4gkjr4yvhbsbljbj9q7dprfnl94j5mq35acsqm2aqh")))

(define-public crate-dsntk-feel-grammar-0.0.2 (c (n "dsntk-feel-grammar") (v "0.0.2") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "0div57k3jb30wzy7jan8s5s78013wc45q3kh6rjnawfwsb91x1ld")))

(define-public crate-dsntk-feel-grammar-0.0.3 (c (n "dsntk-feel-grammar") (v "0.0.3") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "1jp6jcg6yngyynvmal8xw82f3vz7c88ix8zhjhccam72ayqz7807")))

(define-public crate-dsntk-feel-grammar-0.0.4 (c (n "dsntk-feel-grammar") (v "0.0.4") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "0z22lhz04s62ghz0bb3gw6lqjs5hhj3wm1v6fq4vcw6lwdk1nh97")))

(define-public crate-dsntk-feel-grammar-0.0.5 (c (n "dsntk-feel-grammar") (v "0.0.5") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)))) (h "0qgk2mkvh6v77hmwsv8lwa0hs9wx0d6c9a139kix58scg1llgmyv")))

