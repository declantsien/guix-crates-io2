(define-module (crates-io ds nt dsntk-feel-number) #:use-module (crates-io))

(define-public crate-dsntk-feel-number-0.0.0 (c (n "dsntk-feel-number") (v "0.0.0") (d (list (d (n "dfp-number-sys") (r "^0.0.14") (d #t) (k 0)) (d (n "dsntk-common") (r "^0.0.0") (d #t) (k 0)) (d (n "dsntk-macros") (r "^0.0.0") (d #t) (k 0)))) (h "1wgyg2xzddav0qx2nbjirn5imldapjjyi6m8ynhsa2y382hvmjd4")))

(define-public crate-dsntk-feel-number-0.0.1 (c (n "dsntk-feel-number") (v "0.0.1") (d (list (d (n "dfp-number-sys") (r "^0.0.14") (d #t) (k 0)) (d (n "dsntk-common") (r "^0.0.1") (d #t) (k 0)) (d (n "dsntk-macros") (r "^0.0.1") (d #t) (k 0)))) (h "045manidixh8wiglz0kkwjcv81k02yk8ivz5bd8lbjyhxmp6yyy4")))

(define-public crate-dsntk-feel-number-0.0.2 (c (n "dsntk-feel-number") (v "0.0.2") (d (list (d (n "dfp-number-sys") (r "^0.0.15") (d #t) (k 0)) (d (n "dsntk-common") (r "^0.0.2") (d #t) (k 0)) (d (n "dsntk-macros") (r "^0.0.2") (d #t) (k 0)))) (h "1j4lyv7v6ywzhq1x0a40c3d577mljijl2fr836xd94pi9zcckaj8")))

(define-public crate-dsntk-feel-number-0.0.3 (c (n "dsntk-feel-number") (v "0.0.3") (d (list (d (n "dfp-number-sys") (r "^0.0.19") (d #t) (k 0)) (d (n "dsntk-common") (r "^0.0.3") (d #t) (k 0)) (d (n "dsntk-macros") (r "^0.0.3") (d #t) (k 0)))) (h "1rbs0q5s2rd9f3k92gkb0pr0jiy6nr40fi18l5z6pavsijkkifjg")))

(define-public crate-dsntk-feel-number-0.0.4 (c (n "dsntk-feel-number") (v "0.0.4") (d (list (d (n "dfp-number-sys") (r "^0.0.19") (d #t) (k 0)) (d (n "dsntk-common") (r "^0.0.4") (d #t) (k 0)) (d (n "dsntk-macros") (r "^0.0.4") (d #t) (k 0)))) (h "1fl1q34qrnx4jj77i4n1xkn8wwkals6mwh9x5bj3mlily197lj6i")))

(define-public crate-dsntk-feel-number-0.0.5 (c (n "dsntk-feel-number") (v "0.0.5") (d (list (d (n "dfp-number-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "dsntk-common") (r "^0.0.5") (d #t) (k 0)) (d (n "dsntk-macros") (r "^0.0.5") (d #t) (k 0)))) (h "1614b8j72jvvqjd7svcl3d6jnd6akvynhvki4hsd333hlj1hh3zk")))

