(define-module (crates-io ds nt dsntk-feel-parser) #:use-module (crates-io))

(define-public crate-dsntk-feel-parser-0.0.0 (c (n "dsntk-feel-parser") (v "0.0.0") (d (list (d (n "difference") (r "^2.0.0") (d #t) (k 2)) (d (n "dsntk-common") (r "^0.0.0") (d #t) (k 0)) (d (n "dsntk-feel") (r "^0.0.0") (d #t) (k 0)) (d (n "dsntk-feel-grammar") (r "^0.0.0") (d #t) (k 1)) (d (n "dsntk-macros") (r "^0.0.0") (d #t) (k 0)))) (h "02g9p05rh2vanyjq4kzkj8ydv78hjmd2cq6p9a395hvcxm7hbqyi") (f (quote (("parsing-tables"))))))

(define-public crate-dsntk-feel-parser-0.0.1 (c (n "dsntk-feel-parser") (v "0.0.1") (d (list (d (n "difference") (r "^2.0.0") (d #t) (k 2)) (d (n "dsntk-common") (r "^0.0.1") (d #t) (k 0)) (d (n "dsntk-feel") (r "^0.0.1") (d #t) (k 0)) (d (n "dsntk-feel-grammar") (r "^0.0.1") (d #t) (k 1)) (d (n "dsntk-macros") (r "^0.0.1") (d #t) (k 0)))) (h "00w9v8wi9xkvlwv2mngjgwiv6mw7sb2az9hpyk4b4zrqgcifr5fj") (f (quote (("parsing-tables"))))))

(define-public crate-dsntk-feel-parser-0.0.2 (c (n "dsntk-feel-parser") (v "0.0.2") (d (list (d (n "difference") (r "^2.0.0") (d #t) (k 2)) (d (n "dsntk-common") (r "^0.0.2") (d #t) (k 0)) (d (n "dsntk-feel") (r "^0.0.2") (d #t) (k 0)) (d (n "dsntk-feel-grammar") (r "^0.0.2") (d #t) (k 1)) (d (n "dsntk-macros") (r "^0.0.2") (d #t) (k 0)))) (h "1klf3ykpz7gj9xfpydnr68mkbkvibh9mxazkd2b09qrp6hkzxb5k") (f (quote (("parsing-tables"))))))

(define-public crate-dsntk-feel-parser-0.0.3 (c (n "dsntk-feel-parser") (v "0.0.3") (d (list (d (n "difference") (r "^2.0.0") (d #t) (k 2)) (d (n "dsntk-common") (r "^0.0.3") (d #t) (k 0)) (d (n "dsntk-feel") (r "^0.0.3") (d #t) (k 0)) (d (n "dsntk-feel-grammar") (r "^0.0.3") (d #t) (k 1)) (d (n "dsntk-macros") (r "^0.0.3") (d #t) (k 0)))) (h "1jsll785f0cdknzc425hfcsw2jfdml569hi6p8nzmb6gmhg50bvd") (f (quote (("parsing-tables"))))))

(define-public crate-dsntk-feel-parser-0.0.4 (c (n "dsntk-feel-parser") (v "0.0.4") (d (list (d (n "difference") (r "^2.0.0") (d #t) (k 2)) (d (n "dsntk-common") (r "^0.0.4") (d #t) (k 0)) (d (n "dsntk-feel") (r "^0.0.4") (d #t) (k 0)) (d (n "dsntk-feel-grammar") (r "^0.0.4") (d #t) (k 1)) (d (n "dsntk-macros") (r "^0.0.4") (d #t) (k 0)))) (h "07am0lrpy1k7fmbr7zinz45vdz9kwzwhqyaqzbz4dzjs7dga950x") (f (quote (("parsing-tables"))))))

(define-public crate-dsntk-feel-parser-0.0.5 (c (n "dsntk-feel-parser") (v "0.0.5") (d (list (d (n "difference") (r "^2.0.0") (d #t) (k 2)) (d (n "dsntk-common") (r "^0.0.5") (d #t) (k 0)) (d (n "dsntk-feel") (r "^0.0.5") (d #t) (k 0)) (d (n "dsntk-feel-grammar") (r "^0.0.5") (d #t) (k 1)) (d (n "dsntk-macros") (r "^0.0.5") (d #t) (k 0)))) (h "1h3mqkqky8gmv9y0lrvz47yzg8ij3s6l2am3508ly7p61hvc9fxm") (f (quote (("parsing-tables"))))))

