(define-module (crates-io ds nt dsntk-macros) #:use-module (crates-io))

(define-public crate-dsntk-macros-0.0.0 (c (n "dsntk-macros") (v "0.0.0") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (d #t) (k 0)))) (h "19xvip4aayhgbyy268kyg50svhq9zadjdrgfn87j7hii6f3k84a6")))

(define-public crate-dsntk-macros-0.0.1 (c (n "dsntk-macros") (v "0.0.1") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (d #t) (k 0)))) (h "0xgfxzj15g86f54wlpfnp8y4snpnmjywa33z2671hqrzamybshxm")))

(define-public crate-dsntk-macros-0.0.2 (c (n "dsntk-macros") (v "0.0.2") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (d #t) (k 0)))) (h "0lrbkgczzcp6v4jsfxg0dc677smmxq1k98b010skp8rrdzfb2khf")))

(define-public crate-dsntk-macros-0.0.3 (c (n "dsntk-macros") (v "0.0.3") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.43") (d #t) (k 0)))) (h "1p104b5d19hm4vk6czdnlyqfdygcdfczss1wfk3413977vw3k8bg")))

(define-public crate-dsntk-macros-0.0.4 (c (n "dsntk-macros") (v "0.0.4") (d (list (d (n "proc-macro2") (r "^1.0.76") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (f (quote ("fold" "full"))) (d #t) (k 0)))) (h "0m14i0abwv3blrd8sfsd0f9jj7if89s9jl4yjdrnrlij5mvmm5wp")))

(define-public crate-dsntk-macros-0.0.5 (c (n "dsntk-macros") (v "0.0.5") (d (list (d (n "proc-macro2") (r "^1.0.81") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.60") (f (quote ("fold" "full"))) (d #t) (k 0)))) (h "0w07mr2zwj7ab9i23w18brph0zmv7jg1dlimynqjyb86xhkqjyb5")))

