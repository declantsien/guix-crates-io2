(define-module (crates-io ds nt dsntk-examples) #:use-module (crates-io))

(define-public crate-dsntk-examples-0.0.0 (c (n "dsntk-examples") (v "0.0.0") (d (list (d (n "walkdir") (r "^2.4.0") (d #t) (k 2)))) (h "1zqv0hzjl8bhfj9pjcfi29nvxy6naq7bs0bi6dqh5mhbliv84kml")))

(define-public crate-dsntk-examples-0.0.1 (c (n "dsntk-examples") (v "0.0.1") (d (list (d (n "walkdir") (r "^2.4.0") (d #t) (k 2)))) (h "1gj67vn673g8bsma26rqylzcasykviqg625qc6mwrn0bjkmlgqfj")))

(define-public crate-dsntk-examples-0.0.2 (c (n "dsntk-examples") (v "0.0.2") (d (list (d (n "walkdir") (r "^2.4.0") (d #t) (k 2)))) (h "0ml7811rmycdsgysi3j8mdsz5cjsp9an49i5qn8mcfzjpapjvbpq")))

(define-public crate-dsntk-examples-0.0.3 (c (n "dsntk-examples") (v "0.0.3") (d (list (d (n "walkdir") (r "^2.4.0") (d #t) (k 2)))) (h "1wbyr95d8hizknilj4bpgc0pw4ryh6qs5gkyxrmaj8ybch73l2n1")))

(define-public crate-dsntk-examples-0.0.4 (c (n "dsntk-examples") (v "0.0.4") (d (list (d (n "walkdir") (r "^2.4.0") (d #t) (k 2)))) (h "0p5j92jajlrnfm0nwx13g8rqp9z8rjbwgm2vskf5xxl8rhhjj6p6")))

(define-public crate-dsntk-examples-0.0.5 (c (n "dsntk-examples") (v "0.0.5") (d (list (d (n "walkdir") (r "^2.5.0") (d #t) (k 2)))) (h "12i1csvr0d2xxa2ndd0yzvfxlnv7lpfjgc3hsisgmbqslmv0a3n8")))

