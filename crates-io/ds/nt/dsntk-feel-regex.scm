(define-module (crates-io ds nt dsntk-feel-regex) #:use-module (crates-io))

(define-public crate-dsntk-feel-regex-0.0.0 (c (n "dsntk-feel-regex") (v "0.0.0") (d (list (d (n "onig") (r "^6.4.0") (o #t) (k 0)))) (h "1sxx6f2vx0blv40h880k2kkdnip3f2ynda2k92qd66m95xcvryjv") (f (quote (("oniguruma" "onig") ("default"))))))

(define-public crate-dsntk-feel-regex-0.0.4 (c (n "dsntk-feel-regex") (v "0.0.4") (h "0fyizbf8bd15grkrzjyfrgiwcyyxrf4w9cqa2wmyyni4q6fhjvqm")))

(define-public crate-dsntk-feel-regex-0.0.5 (c (n "dsntk-feel-regex") (v "0.0.5") (h "0c7lm6v30qv0ws56v82lcgckxiiq8agr7x1r3a6x3krkk73dgalx")))

