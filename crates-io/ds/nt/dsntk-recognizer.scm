(define-module (crates-io ds nt dsntk-recognizer) #:use-module (crates-io))

(define-public crate-dsntk-recognizer-0.0.0 (c (n "dsntk-recognizer") (v "0.0.0") (d (list (d (n "dsntk-common") (r "^0.0.0") (d #t) (k 0)) (d (n "dsntk-examples") (r "^0.0.0") (d #t) (k 2)) (d (n "dsntk-macros") (r "^0.0.0") (d #t) (k 0)) (d (n "dsntk-model") (r "^0.0.0") (d #t) (k 0)))) (h "1qi11x6fwvb9yixhnjdiqk0gl3v6wjvn4lqdxkz5rcyw7iwpghrb")))

(define-public crate-dsntk-recognizer-0.0.1 (c (n "dsntk-recognizer") (v "0.0.1") (d (list (d (n "dsntk-common") (r "^0.0.1") (d #t) (k 0)) (d (n "dsntk-examples") (r "^0.0.1") (d #t) (k 2)) (d (n "dsntk-macros") (r "^0.0.1") (d #t) (k 0)) (d (n "dsntk-model") (r "^0.0.1") (d #t) (k 0)))) (h "12j6cy81jgddmcwvx2s1f9ra6cvk9fhbzl4776gmchyrsydbn06i")))

(define-public crate-dsntk-recognizer-0.0.2 (c (n "dsntk-recognizer") (v "0.0.2") (d (list (d (n "dsntk-common") (r "^0.0.2") (d #t) (k 0)) (d (n "dsntk-examples") (r "^0.0.2") (d #t) (k 2)) (d (n "dsntk-macros") (r "^0.0.2") (d #t) (k 0)) (d (n "dsntk-model") (r "^0.0.2") (d #t) (k 0)))) (h "1ankjgk5ldnf95l0q4m0lyhb6ssn9fzqv412jman68knvxwn8kp2")))

(define-public crate-dsntk-recognizer-0.0.3 (c (n "dsntk-recognizer") (v "0.0.3") (d (list (d (n "dsntk-common") (r "^0.0.3") (d #t) (k 0)) (d (n "dsntk-examples") (r "^0.0.3") (d #t) (k 2)) (d (n "dsntk-macros") (r "^0.0.3") (d #t) (k 0)) (d (n "dsntk-model") (r "^0.0.3") (d #t) (k 0)))) (h "0c557lhsrlyc5hfx6gx58jqrbq17pm1d4slqz68p4ds4iw4jqbsi")))

(define-public crate-dsntk-recognizer-0.0.4 (c (n "dsntk-recognizer") (v "0.0.4") (d (list (d (n "dsntk-common") (r "^0.0.4") (d #t) (k 0)) (d (n "dsntk-examples") (r "^0.0.4") (d #t) (k 2)) (d (n "dsntk-macros") (r "^0.0.4") (d #t) (k 0)) (d (n "dsntk-model") (r "^0.0.4") (d #t) (k 0)))) (h "1dmdnmksfzag9f7q6fjvqi2y24rzlz44dwlpvcw66202qc9la4xa")))

(define-public crate-dsntk-recognizer-0.0.5 (c (n "dsntk-recognizer") (v "0.0.5") (d (list (d (n "dsntk-common") (r "^0.0.5") (d #t) (k 0)) (d (n "dsntk-examples") (r "^0.0.5") (d #t) (k 2)) (d (n "dsntk-macros") (r "^0.0.5") (d #t) (k 0)) (d (n "dsntk-model") (r "^0.0.5") (d #t) (k 0)))) (h "0m93whbihpsa356hfz5qk6jg2kn4n0skmbwpw7apjvz18zq45hxs")))

