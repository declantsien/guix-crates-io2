(define-module (crates-io ds v- dsv-seeker) #:use-module (crates-io))

(define-public crate-dsv-seeker-0.1.0 (c (n "dsv-seeker") (v "0.1.0") (d (list (d (n "clap") (r "^4.0.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "csv") (r "^1.1.6") (d #t) (k 0)) (d (n "pest") (r "^2.4.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0khh46ajcgp442plb9mj1m1jp4mfmq3ks79jyinbxx1ypdh7h8dy")))

