(define-module (crates-io ds i- dsi-progress-logger) #:use-module (crates-io))

(define-public crate-dsi-progress-logger-0.1.0 (c (n "dsi-progress-logger") (v "0.1.0") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "num-format") (r "^0.4.4") (d #t) (k 0)) (d (n "pluralizer") (r "^0.4.0") (d #t) (k 0)) (d (n "stderrlog") (r "^0.5.4") (d #t) (k 2)) (d (n "sysinfo") (r "^0.29.0") (d #t) (k 0)))) (h "0v8c9ng0rn39bw1shz8z8rckxsk7hhr2mr43v6gmmhjz5g7p415p")))

(define-public crate-dsi-progress-logger-0.1.1 (c (n "dsi-progress-logger") (v "0.1.1") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "num-format") (r "^0.4.4") (d #t) (k 0)) (d (n "pluralizer") (r "^0.4.0") (d #t) (k 0)) (d (n "stderrlog") (r "^0.5.4") (d #t) (k 2)) (d (n "sysinfo") (r "^0.29.0") (d #t) (k 0)))) (h "13l1iv3fak1k0qvd2p20qzcjkysqf95kagj0gkrb4k4d7ci6l6ib")))

(define-public crate-dsi-progress-logger-0.1.2 (c (n "dsi-progress-logger") (v "0.1.2") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "num-format") (r "^0.4.4") (d #t) (k 0)) (d (n "pluralizer") (r "^0.4.0") (d #t) (k 0)) (d (n "stderrlog") (r "^0.5.4") (d #t) (k 2)) (d (n "sysinfo") (r "^0.29.0") (d #t) (k 0)))) (h "0sa3v9wad1z0fhlbymh2fdy7siias4s8xx6g9p1wajy3spg8n68l")))

(define-public crate-dsi-progress-logger-0.1.3 (c (n "dsi-progress-logger") (v "0.1.3") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "num-format") (r "^0.4.4") (d #t) (k 0)) (d (n "pluralizer") (r "^0.4.0") (d #t) (k 0)) (d (n "stderrlog") (r "^0.5.4") (d #t) (k 2)) (d (n "sysinfo") (r "^0.29.0") (d #t) (k 0)))) (h "0dqpd3vynfc4lchv51x8yvdcr2ykv3rg1v9s3p7ps0adgqq408ls")))

(define-public crate-dsi-progress-logger-0.1.4 (c (n "dsi-progress-logger") (v "0.1.4") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "num-format") (r "^0.4.4") (d #t) (k 0)) (d (n "pluralizer") (r "^0.4.0") (d #t) (k 0)) (d (n "stderrlog") (r "^0.5.4") (d #t) (k 2)) (d (n "sysinfo") (r "^0.29.0") (d #t) (k 0)))) (h "0kb17c3x2549src1c9zcha2dgs67ygfm33m2py5cfbb781igm2gb")))

(define-public crate-dsi-progress-logger-0.2.0 (c (n "dsi-progress-logger") (v "0.2.0") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "num-format") (r "^0.4.4") (d #t) (k 0)) (d (n "pluralizer") (r "^0.4.0") (d #t) (k 0)) (d (n "stderrlog") (r "^0.5.4") (d #t) (k 2)) (d (n "sysinfo") (r "^0.29.0") (d #t) (k 0)))) (h "0dk01bd1m0v18f1a1w1n1yr8c5gq8bjf5viba2k2j1cywfl30s9s")))

(define-public crate-dsi-progress-logger-0.2.1 (c (n "dsi-progress-logger") (v "0.2.1") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "num-format") (r "^0.4.4") (d #t) (k 0)) (d (n "pluralizer") (r "^0.4.0") (d #t) (k 0)) (d (n "stderrlog") (r "^0.5.4") (d #t) (k 2)) (d (n "sysinfo") (r "^0.29.10") (d #t) (k 0)))) (h "0wr5hwksnqdlmkydnjyypxbll66rpb1y6zlr16j6a115w4ljy8s6")))

(define-public crate-dsi-progress-logger-0.2.2 (c (n "dsi-progress-logger") (v "0.2.2") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "num-format") (r "^0.4.4") (d #t) (k 0)) (d (n "pluralizer") (r "^0.4.0") (d #t) (k 0)) (d (n "stderrlog") (r "^0.5.4") (d #t) (k 2)) (d (n "sysinfo") (r "^0.29.10") (d #t) (k 0)))) (h "0v6x4d3s03b4777bk1m9zdflpzr2n65n14rwwvm0yns9qbakn0kc")))

(define-public crate-dsi-progress-logger-0.2.3 (c (n "dsi-progress-logger") (v "0.2.3") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "num-format") (r "^0.4.4") (d #t) (k 0)) (d (n "pluralizer") (r "^0.4.0") (d #t) (k 0)) (d (n "stderrlog") (r "^0.5.4") (d #t) (k 2)) (d (n "sysinfo") (r "^0.29.10") (d #t) (k 0)))) (h "1d70njn93zc4idkhx0l0jc2hjsisdzpwm31gcnq44mi54k4cy1xw")))

(define-public crate-dsi-progress-logger-0.2.4 (c (n "dsi-progress-logger") (v "0.2.4") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "num-format") (r "^0.4.4") (d #t) (k 0)) (d (n "pluralizer") (r "^0.4.0") (d #t) (k 0)) (d (n "stderrlog") (r "^0.5.4") (d #t) (k 2)) (d (n "sysinfo") (r "^0.29.10") (d #t) (k 0)))) (h "0nahj2zfqsnql04jiml4i254yj475c8rr8qhrk13j79rqhrn89jl")))

