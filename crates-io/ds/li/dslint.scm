(define-module (crates-io ds li dslint) #:use-module (crates-io))

(define-public crate-dslint-0.0.1 (c (n "dslint") (v "0.0.1") (d (list (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)))) (h "0nsm43m1w91149rmyvnnd6pm7amlgga56gvgsla2sjjksf5yshmr")))

(define-public crate-dslint-0.0.2 (c (n "dslint") (v "0.0.2") (d (list (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)))) (h "17a7rpg7l9s2g0gzh5zidqb8wikb75az9l9klljynm7d3n3fxg25")))

