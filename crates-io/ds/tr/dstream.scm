(define-module (crates-io ds tr dstream) #:use-module (crates-io))

(define-public crate-dstream-0.1.0 (c (n "dstream") (v "0.1.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "linked-hash-map") (r "^0.5.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("time"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "macros"))) (d #t) (k 2)))) (h "0qb7zczylf89pw9wz5sv5v7ca5swmaz52b9dn6zmdrjrcnlfz2br")))

(define-public crate-dstream-0.1.1 (c (n "dstream") (v "0.1.1") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "linked-hash-map") (r "^0.5.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("time"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "macros"))) (d #t) (k 2)))) (h "1mdaz9613jhl9767fdzfb9dcvd89g36k5s5jfimvz2a4yd0ynzxg")))

