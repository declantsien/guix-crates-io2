(define-module (crates-io ds -t ds-transcriber) #:use-module (crates-io))

(define-public crate-ds-transcriber-0.1.0 (c (n "ds-transcriber") (v "0.1.0") (d (list (d (n "cpal") (r "^0.13.3") (d #t) (k 0)) (d (n "deepspeech") (r "^0.9.0") (d #t) (k 0)) (d (n "nnnoiseless") (r "^0.3.2") (d #t) (k 0)))) (h "1yqmhazdqzpxc9384dpzi1qaszr9aaxlckjmnn1ybj6h77jp6max") (y #t)))

(define-public crate-ds-transcriber-0.1.1 (c (n "ds-transcriber") (v "0.1.1") (d (list (d (n "cpal") (r "^0.13.3") (d #t) (k 0)) (d (n "deepspeech") (r "^0.9.0") (d #t) (k 0)) (d (n "nnnoiseless") (r "^0.3.2") (d #t) (k 0)))) (h "0l0bk7p04phdcw6kvxjpswh256myl8cr8izm7gw7m5qcsh83fp48") (y #t)))

(define-public crate-ds-transcriber-0.1.2 (c (n "ds-transcriber") (v "0.1.2") (d (list (d (n "cpal") (r "^0.13.3") (d #t) (k 0)) (d (n "deepspeech") (r "^0.9.0") (d #t) (k 0)) (d (n "nnnoiseless") (r "^0.3.2") (d #t) (k 0)))) (h "0dgwnbskdwhy9fi4kwcshp4kv9xaw7pwb7gfa3i8b1hcwnbsksvw")))

(define-public crate-ds-transcriber-0.1.3 (c (n "ds-transcriber") (v "0.1.3") (d (list (d (n "cpal") (r "^0.13.3") (d #t) (k 0)) (d (n "deepspeech") (r "^0.9.0") (d #t) (k 0)) (d (n "nnnoiseless") (r "^0.3.2") (d #t) (k 0)))) (h "1n1b8222xgrrxz80j0wkrm4578yhbbsjipf0acw3xm2a1ihici2w")))

(define-public crate-ds-transcriber-1.0.0-beta.0 (c (n "ds-transcriber") (v "1.0.0-beta.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("cargo"))) (d #t) (k 2)) (d (n "cpal") (r "^0.13.3") (d #t) (k 0)) (d (n "deepspeech") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nnnoiseless") (r "^0.3.2") (o #t) (d #t) (k 0)))) (h "1ib9wwd111zrl240ybsryqr7qi4ycbvcvfwdpgjy02s4f7rkng3v") (f (quote (("full" "denoise") ("denoise" "nnnoiseless") ("default"))))))

(define-public crate-ds-transcriber-1.0.0 (c (n "ds-transcriber") (v "1.0.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("cargo"))) (d #t) (k 2)) (d (n "cpal") (r "^0.13.3") (d #t) (k 0)) (d (n "deepspeech") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nnnoiseless") (r "^0.3.2") (o #t) (d #t) (k 0)))) (h "16y84m9jrcmdq8wd6fr7j81hic6ady2hn1yffm0p7g8ic17g9rkg") (f (quote (("full" "denoise") ("denoise" "nnnoiseless") ("default"))))))

(define-public crate-ds-transcriber-1.0.1 (c (n "ds-transcriber") (v "1.0.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("cargo"))) (d #t) (k 2)) (d (n "cpal") (r "^0.13.3") (d #t) (k 0)) (d (n "deepspeech") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nnnoiseless") (r "^0.3.2") (o #t) (d #t) (k 0)))) (h "0k21dz2lzrzisxqiayz9b1586hw7jkngasc0fyfxfxsqlm0s50s2") (f (quote (("full" "denoise") ("denoise" "nnnoiseless") ("default"))))))

