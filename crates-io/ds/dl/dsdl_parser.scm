(define-module (crates-io ds dl dsdl_parser) #:use-module (crates-io))

(define-public crate-dsdl_parser-0.0.1 (c (n "dsdl_parser") (v "0.0.1") (d (list (d (n "log") (r "^0.3.8") (d #t) (k 0)) (d (n "nom") (r "^3.2.0") (d #t) (k 0)) (d (n "test-logger") (r "^0.1.0") (d #t) (k 2)))) (h "1i0akk0clb7hm9050zqywl8bjq8wpiivnwh4059iwblrllbkb817")))

(define-public crate-dsdl_parser-0.1.0 (c (n "dsdl_parser") (v "0.1.0") (d (list (d (n "log") (r "^0.3.8") (d #t) (k 0)) (d (n "nom") (r "^3.2") (d #t) (k 0)) (d (n "test-logger") (r "^0.1.0") (d #t) (k 2)))) (h "1wqzcyw3kxqzwm86lp21ld0brfhmfjvykr0ckwa036ffbcx3bnz6")))

(define-public crate-dsdl_parser-0.1.1 (c (n "dsdl_parser") (v "0.1.1") (d (list (d (n "log") (r "^0.3.8") (d #t) (k 0)) (d (n "nom") (r "^3.2") (d #t) (k 0)) (d (n "test-logger") (r "^0.1.0") (d #t) (k 2)))) (h "1bkf7xygyav33vsgy59z6x465lqxk4av9wc0d8h8w7y5mrpsf4i0")))

(define-public crate-dsdl_parser-0.1.2 (c (n "dsdl_parser") (v "0.1.2") (d (list (d (n "log") (r "^0.3.8") (d #t) (k 0)) (d (n "nom") (r "^3.2") (d #t) (k 0)) (d (n "test-logger") (r "^0.1.0") (d #t) (k 2)))) (h "02m45n4aya1cjslrvvdccv0lviqgy0pxjjrvn6vxs3qfy0lvdghj")))

(define-public crate-dsdl_parser-0.1.3 (c (n "dsdl_parser") (v "0.1.3") (d (list (d (n "log") (r "^0.3.8") (d #t) (k 0)) (d (n "nom") (r "^3.2") (d #t) (k 0)) (d (n "test-logger") (r "^0.1.0") (d #t) (k 2)))) (h "0dsvm7k1ca1n5wpi7m2syi5lk4gfqwhvz797iawd52pr8mal24w1")))

