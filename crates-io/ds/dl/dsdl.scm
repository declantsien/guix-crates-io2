(define-module (crates-io ds dl dsdl) #:use-module (crates-io))

(define-public crate-dsdl-0.0.1 (c (n "dsdl") (v "0.0.1") (d (list (d (n "cyphal") (r "^0.0.2") (d #t) (k 0)))) (h "0pzqx2a5mbws3bjsfrdi4qaplfv5hvjwwf2wzhjh5acs5cwxz0cx")))

(define-public crate-dsdl-0.0.2 (c (n "dsdl") (v "0.0.2") (d (list (d (n "cyphal") (r "^0.0.2") (d #t) (k 0)))) (h "1r97majjsp2ga6mpz7z2harcz0sidq5mfcsnsf4fd6vvhgrs48qb")))

(define-public crate-dsdl-0.0.3 (c (n "dsdl") (v "0.0.3") (d (list (d (n "cyphal") (r "^0.0.3") (d #t) (k 0)))) (h "1hlx731b75ms29iii198cqjhgp34n2k5znyd70142g417a4nh14m")))

(define-public crate-dsdl-0.0.4 (c (n "dsdl") (v "0.0.4") (d (list (d (n "cyphal") (r "^0.0.4") (d #t) (k 0)))) (h "0i0zafbij65wazxrxlijrws8p4rly60959jp9d2lmm9al08k22l1")))

(define-public crate-dsdl-0.0.5 (c (n "dsdl") (v "0.0.5") (d (list (d (n "cyphal") (r "^0.0.5") (d #t) (k 0)))) (h "02g01ib2720q818iklj1qpbn9xjpwgpa4gmkdm2ild87h70lbcwq")))

(define-public crate-dsdl-0.0.6 (c (n "dsdl") (v "0.0.6") (d (list (d (n "cyphal") (r "^0.0.6") (d #t) (k 0)))) (h "0q4mpl7pfhc6wpisz598q6sp23fnkmmpb9qdd69hlx86y175a363")))

(define-public crate-dsdl-0.0.7 (c (n "dsdl") (v "0.0.7") (d (list (d (n "cyphal") (r "^0.0.7") (d #t) (k 0)))) (h "0n24jxi9k8ywq6d8dkgijh51ydn7fpnnskmbwmp068fyw0fjwv1n")))

(define-public crate-dsdl-0.0.8 (c (n "dsdl") (v "0.0.8") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.60") (d #t) (k 0)))) (h "1va5hkhdil0bb923vk2r1w04fzlw03xi8pygprkrwi9vgxfvdqnp")))

(define-public crate-dsdl-0.0.9 (c (n "dsdl") (v "0.0.9") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "cyphal-dsdl") (r "^0.0.9") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.60") (d #t) (k 0)))) (h "0si2rwgzwbfqvfqchaq38a5vasf3zrydrbiv7fd9wmpmw0w0w935")))

