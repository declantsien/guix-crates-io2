(define-module (crates-io ds dl dsdl_compiler) #:use-module (crates-io))

(define-public crate-dsdl_compiler-0.0.1 (c (n "dsdl_compiler") (v "0.0.1") (d (list (d (n "badlog") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "dsdl_parser") (r "^0.1") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "inflections") (r "^1.1") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (f (quote ("full"))) (d #t) (k 0)))) (h "1zysgvs9vhxdrkkdzx36qk3121wb4c2x2mgm9hcd9i7vpkv5v9xy") (f (quote (("default" "binary") ("binary" "getopts" "badlog"))))))

