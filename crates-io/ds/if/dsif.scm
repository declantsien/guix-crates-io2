(define-module (crates-io ds if dsif) #:use-module (crates-io))

(define-public crate-dsif-1.0.0 (c (n "dsif") (v "1.0.0") (d (list (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "traceidr") (r "^1.0.0") (d #t) (k 0)))) (h "0bavhw2ac51nrdr15x0pqvndh2vwky5h0hj0dycxiyriallh2frc")))

