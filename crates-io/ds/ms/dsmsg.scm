(define-module (crates-io ds ms dsmsg) #:use-module (crates-io))

(define-public crate-dsmsg-1.0.0 (c (n "dsmsg") (v "1.0.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "12k23x1qqmf3jpvyqp12lj1ngm5dsdxsfyjia25s5yzqsabjjm9z") (f (quote (("newline"))))))

(define-public crate-dsmsg-1.0.1 (c (n "dsmsg") (v "1.0.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "014ilfqcv3p2wpclcbl26fk53izif3zn4iaa0f4lxa9k0jrmdh3j") (f (quote (("newline"))))))

(define-public crate-dsmsg-1.0.2 (c (n "dsmsg") (v "1.0.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "08m0jbvcca70s3p16bhr2l94mm5wcbw5g0fpfm1qhwdzfs21b1pq") (f (quote (("newline"))))))

(define-public crate-dsmsg-1.1.0 (c (n "dsmsg") (v "1.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "1s8q7whijz5h6rphpk4bd89wdpi0spxfg2jck0jvnc20rkp3sm29") (f (quote (("newline") ("ds3") ("ds2") ("ds1") ("demons") ("default" "ds1" "ds2" "ds3") ("bloodborne"))))))

(define-public crate-dsmsg-1.1.1 (c (n "dsmsg") (v "1.1.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "14y8b7r7k8f6rdl2zbh4m7xdqa3yyys65i2kg2f1hir69fcx2wjf") (f (quote (("newline") ("ds3") ("ds2") ("ds1") ("demons") ("default" "ds1" "ds2" "ds3") ("bloodborne"))))))

(define-public crate-dsmsg-1.1.2 (c (n "dsmsg") (v "1.1.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "1ih5fpz0aicx3xzfbiqjf16fhrx7ls8d5d409094g1cmpiq0gf9v") (f (quote (("newline") ("ds3") ("ds2") ("ds1") ("demons") ("default" "ds1" "ds2" "ds3") ("bloodborne"))))))

(define-public crate-dsmsg-1.2.0 (c (n "dsmsg") (v "1.2.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "13zz3429yqlknjlnv7j1drp761vn39bkjf22kgjgx1vmc1cjp6lb") (f (quote (("newline") ("ds3") ("ds2") ("ds1") ("demons") ("default" "ds1" "ds2" "ds3") ("bloodborne") ("all-sets" "ds1" "ds2" "ds3" "bloodborne" "demons"))))))

(define-public crate-dsmsg-1.3.0 (c (n "dsmsg") (v "1.3.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "1nv1vwz1mpg0d1ks9x2rlyrnkqq7dj3fjijp29c5r5pp5cfbazpa") (f (quote (("newline") ("ds3") ("ds2") ("ds1") ("demons") ("default" "ds1" "ds2" "ds3") ("bloodborne") ("all-sets" "ds1" "ds2" "ds3" "bloodborne" "demons"))))))

(define-public crate-dsmsg-1.4.0 (c (n "dsmsg") (v "1.4.0") (d (list (d (n "argh") (r "^0.1.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "1b6q4dzigkzx53idiyb5qjzzz3hvjcxqm71b7ynrwqq3vgpgbqib") (f (quote (("newline") ("ds3") ("ds2") ("ds1") ("demons") ("default" "ds1" "ds2" "ds3") ("bloodborne") ("all-sets" "ds1" "ds2" "ds3" "bloodborne" "demons"))))))

(define-public crate-dsmsg-1.5.0 (c (n "dsmsg") (v "1.5.0") (d (list (d (n "argh") (r "^0.1.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "1hq0szsys6ggiphlw62nqgscglnh1nnqabj55ng6rp8ld46gaxx7") (f (quote (("newline") ("ds3") ("ds2") ("ds1") ("demons") ("default" "ds1" "ds2" "ds3") ("bloodborne") ("all-sets" "ds1" "ds2" "ds3" "bloodborne" "demons"))))))

(define-public crate-dsmsg-1.5.1 (c (n "dsmsg") (v "1.5.1") (d (list (d (n "argh") (r "^0.1.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "0dgh2kjssjmn1nsm2lj9pw08aj6yc80w1fy3d4kdy79dc3nh06cl") (f (quote (("newline") ("ds3") ("ds2") ("ds1") ("demons") ("default" "ds1" "ds2" "ds3") ("bloodborne") ("all-sets" "ds1" "ds2" "ds3" "bloodborne" "demons"))))))

(define-public crate-dsmsg-1.6.0 (c (n "dsmsg") (v "1.6.0") (d (list (d (n "argh") (r "^0.1.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "1p5c88bh0calvqf86qdnqqr2mc3vpjbyv602rgy0blhzvyy49fj5") (f (quote (("sekiro") ("newline") ("ds3") ("ds2") ("ds1") ("demons") ("default" "ds1" "ds2" "ds3") ("bloodborne") ("all-sets" "ds1" "ds2" "ds3" "bloodborne" "demons" "sekiro"))))))

(define-public crate-dsmsg-1.7.0 (c (n "dsmsg") (v "1.7.0") (d (list (d (n "argh") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1") (d #t) (k 0)))) (h "0wyi4rh4gqjp1g9bnxihxmqgrdwdm0rwig1x388j576pw7a5jhw2") (f (quote (("sekiro") ("newline") ("eldenring") ("ds3") ("ds2") ("ds1") ("demons") ("default" "darksouls") ("darksouls" "ds1" "ds2" "ds3") ("bloodborne") ("all-sets" "darksouls" "bloodborne" "demons" "sekiro" "eldenring"))))))

(define-public crate-dsmsg-1.7.1 (c (n "dsmsg") (v "1.7.1") (d (list (d (n "argh") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1") (d #t) (k 0)))) (h "16r2f09dq32hk4i0j94kmdwbm9hk3p1kwd289zpzqdpdq59mx8hi") (f (quote (("sekiro") ("newline") ("eldenring") ("ds3") ("ds2") ("ds1") ("demons") ("default" "darksouls") ("darksouls" "ds1" "ds2" "ds3") ("bloodborne") ("all-sets" "darksouls" "bloodborne" "demons" "sekiro" "eldenring"))))))

