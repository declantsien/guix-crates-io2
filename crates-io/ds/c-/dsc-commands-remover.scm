(define-module (crates-io ds c- dsc-commands-remover) #:use-module (crates-io))

(define-public crate-dsc-commands-remover-0.1.0 (c (n "dsc-commands-remover") (v "0.1.0") (d (list (d (n "clap") (r "^4.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dialoguer") (r "^0.10") (d #t) (k 0)) (d (n "poise") (r "^0.5") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1cys0mj1a0fi47zjiczkc183hpgy5pl0qv83b5gb9jwbji884i5g")))

(define-public crate-dsc-commands-remover-0.2.0 (c (n "dsc-commands-remover") (v "0.2.0") (d (list (d (n "clap") (r "^4.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dialoguer") (r "^0.11") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)))) (h "0p0vqf1w7356ma3nrhx1jmdbqxqpsvasppir2f6w27d83wh70isb")))

