(define-module (crates-io ds mr dsmr5) #:use-module (crates-io))

(define-public crate-dsmr5-0.1.0 (c (n "dsmr5") (v "0.1.0") (d (list (d (n "crc16") (r "^0.4.0") (d #t) (k 0)))) (h "1a119r1phncasy6wsgjylz56pkc9q2pyfs57w8lcw3hd25mzl4w6")))

(define-public crate-dsmr5-0.1.1 (c (n "dsmr5") (v "0.1.1") (d (list (d (n "crc16") (r "^0.4.0") (d #t) (k 0)))) (h "0jap721i6nqr0ram29x22sylymqqb7jg2bfh7pja2wjx5irnf5pm")))

(define-public crate-dsmr5-0.1.2 (c (n "dsmr5") (v "0.1.2") (d (list (d (n "crc16") (r "^0.4.0") (d #t) (k 0)))) (h "1iymm4y6hcc9g0d78hfjid0y1mwqi5lma1xx7y27806h0axlxifs")))

(define-public crate-dsmr5-0.2.0 (c (n "dsmr5") (v "0.2.0") (d (list (d (n "crc16") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0m0qk2y49n299ds4l3k91gp2cnhlqqypg8gjq3dszwf9ad3g2dgc")))

(define-public crate-dsmr5-0.2.1 (c (n "dsmr5") (v "0.2.1") (d (list (d (n "crc16") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0j0hqvc73gd6ap0ygmaarkgxj99w6yn7nw7l7s4nzikxaynnamz3")))

(define-public crate-dsmr5-0.2.2 (c (n "dsmr5") (v "0.2.2") (d (list (d (n "crc16") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (k 0)))) (h "0w6wcayzrn50b8riy85iqnrx7m9man2aaxbp4m6awjsacandrf7p")))

(define-public crate-dsmr5-0.3.0 (c (n "dsmr5") (v "0.3.0") (d (list (d (n "crc16") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (k 0)))) (h "1y86vvp5pak1saj266inrhg162b6l4q1l04x3f897pwwvclchcvz")))

(define-public crate-dsmr5-0.4.0 (c (n "dsmr5") (v "0.4.0") (d (list (d (n "crc16") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (k 0)))) (h "0fxpvzwd74nb985hbqxzz3wp5fd753lxjkjcr0h9s9vxs122bfsr")))

