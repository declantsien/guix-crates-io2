(define-module (crates-io cj _a cj_ascii) #:use-module (crates-io))

(define-public crate-cj_ascii-0.1.2 (c (n "cj_ascii") (v "0.1.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1qn7l0yfapqxyb25ld14ppc7qxwyy0wx6j6v7cqsa9ahhmp47ijz") (f (quote (("serialize" "serde") ("default"))))))

(define-public crate-cj_ascii-0.1.3 (c (n "cj_ascii") (v "0.1.3") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "15j08h1s8f20sh5jh25526ng2xn17lhrarbhvzsc4jzq0dskl8xp") (f (quote (("serialize" "serde") ("default"))))))

(define-public crate-cj_ascii-0.1.4 (c (n "cj_ascii") (v "0.1.4") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0kmpcff1bjbdq1an0kzsawschv1971n8kdq2ff3y9ada3mzj9xnp") (f (quote (("serialize" "serde") ("default"))))))

(define-public crate-cj_ascii-0.1.5 (c (n "cj_ascii") (v "0.1.5") (d (list (d (n "futures") (r "^0.3.28") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "tokio") (r "^1.13") (f (quote ("macros" "rt" "fs"))) (d #t) (k 2)) (d (n "tokio-util") (r "^0.7.8") (f (quote ("compat"))) (d #t) (k 2)))) (h "0fnip7jfkzl6764x2amr4hb4xl6wshz0shbbwm8567a2angvn4a6") (f (quote (("serialize" "serde") ("default" "async") ("async" "futures"))))))

(define-public crate-cj_ascii-0.2.0 (c (n "cj_ascii") (v "0.2.0") (d (list (d (n "futures") (r "^0.3.28") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "tokio") (r "^1.13") (f (quote ("macros" "rt" "fs"))) (d #t) (k 2)) (d (n "tokio-util") (r "^0.7.8") (f (quote ("compat"))) (d #t) (k 2)))) (h "158xb6kvmvmjx5wvd9f2qrf086jifr449vk0yjyr8a178jpqg6xa") (f (quote (("serialize" "serde") ("default" "async") ("async" "futures"))))))

