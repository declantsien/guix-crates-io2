(define-module (crates-io cj se cjseq) #:use-module (crates-io))

(define-public crate-cjseq-0.1.0 (c (n "cjseq") (v "0.1.0") (d (list (d (n "clap") (r "4.4.*") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "04rdg2pai9vmwkxnzmm2yjcc6kl3yirmd7r3hcsyxfr8f3vx4994")))

(define-public crate-cjseq-0.2.0 (c (n "cjseq") (v "0.2.0") (d (list (d (n "clap") (r "4.4.*") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "122bh0nnsywg8lx910i9r7z8bjd0b3wcm88x6rm42rl5w1mzh53d")))

(define-public crate-cjseq-0.3.0 (c (n "cjseq") (v "0.3.0") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0bb91a88ymslq9w4cdff41n0z5nwarv8pkrh243n1y4kr7gm97hw")))

