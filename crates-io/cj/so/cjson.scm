(define-module (crates-io cj so cjson) #:use-module (crates-io))

(define-public crate-cjson-0.1.0 (c (n "cjson") (v "0.1.0") (d (list (d (n "itoa") (r "^0.4.3") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "08gsm2kxx2zqc69ajm2bcxaj8f0xsq5rpz4agy4441lszn41yj3z")))

(define-public crate-cjson-0.1.1 (c (n "cjson") (v "0.1.1") (d (list (d (n "itoa") (r "^0.4.3") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 2)) (d (n "serde") (r "^1.0.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "19wlavdrpjv68kb87jpxv1j0ia8z2kraw6gv5yyxnbqf6pxh3dm2")))

(define-public crate-cjson-0.1.2 (c (n "cjson") (v "0.1.2") (d (list (d (n "itoa") (r "^0.4.3") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 2)) (d (n "serde") (r "^1.0.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1kpp50m9azs0h33nmkq14n7dw7ryhbf7g6fdvqk62p717wfinlwi")))

