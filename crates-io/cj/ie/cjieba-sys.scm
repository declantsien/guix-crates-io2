(define-module (crates-io cj ie cjieba-sys) #:use-module (crates-io))

(define-public crate-cjieba-sys-0.1.0 (c (n "cjieba-sys") (v "0.1.0") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1jdhmgpsfmpxg37jck6y7x9dbvjky9vy27l5s4vy88xssgwnzk6p")))

(define-public crate-cjieba-sys-0.1.1 (c (n "cjieba-sys") (v "0.1.1") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "084zi2qa6l04ivjdlph31sd74n040lwmzmgyg266cc2sxxqvwz9s")))

