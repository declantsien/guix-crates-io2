(define-module (crates-io cj _b cj_bitmask_vec) #:use-module (crates-io))

(define-public crate-cj_bitmask_vec-0.1.0 (c (n "cj_bitmask_vec") (v "0.1.0") (d (list (d (n "cj_common") (r "^0.2") (d #t) (k 0)))) (h "1rvpqb4fq09qf8j4d63jzr89r3hz56x29z4qfnngd21s7zxwgvc0")))

(define-public crate-cj_bitmask_vec-0.1.1 (c (n "cj_bitmask_vec") (v "0.1.1") (d (list (d (n "cj_common") (r "^0.2") (d #t) (k 0)))) (h "1ghzi7n4mnsqcy1wyd7xknk3489d7w8fjp379pbb6rqplx8ij4yi")))

(define-public crate-cj_bitmask_vec-0.1.2 (c (n "cj_bitmask_vec") (v "0.1.2") (d (list (d (n "cj_common") (r "^0.2") (d #t) (k 0)))) (h "073k741yvrznkla4wdgr7yc2gkhiam0d3a0zhj2kh7bjffpn4fg4")))

(define-public crate-cj_bitmask_vec-0.1.3 (c (n "cj_bitmask_vec") (v "0.1.3") (d (list (d (n "cj_common") (r "^0.2") (d #t) (k 0)))) (h "1kkrqqv80ynszyw4qb45ypz52v835g5inhminqfx30zh9r3pj3ay")))

(define-public crate-cj_bitmask_vec-0.1.4 (c (n "cj_bitmask_vec") (v "0.1.4") (d (list (d (n "cj_common") (r "^1.0.0") (d #t) (k 0)))) (h "01js9bims1ff0d9jiymaajvsbdiq28c1bvs8qxn976f754y3n2bq")))

(define-public crate-cj_bitmask_vec-1.0.0 (c (n "cj_bitmask_vec") (v "1.0.0") (d (list (d (n "cj_common") (r "^1.0.0") (d #t) (k 0)))) (h "01yj2mkqq7npl7gnph3qhq53jsfiyfxkhpw65kmq2jfr3mvyaj0x")))

