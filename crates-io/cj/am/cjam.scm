(define-module (crates-io cj am cjam) #:use-module (crates-io))

(define-public crate-cjam-0.1.0 (c (n "cjam") (v "0.1.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "17nwmn1lfjcw6vl618cj35pcsw89klkkpswllf3kjflkn3nwzkrk")))

(define-public crate-cjam-0.1.1 (c (n "cjam") (v "0.1.1") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0zwxv4bbnl6x6jcv9xvib76bpnnpka6ss3dp285j3zwahhbd8s0y")))

(define-public crate-cjam-0.1.2 (c (n "cjam") (v "0.1.2") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1c9wm0a9hx91cwa0vr4l78bpylxdssii4x8cnxxhx1d9icgmmbn3")))

(define-public crate-cjam-0.1.3 (c (n "cjam") (v "0.1.3") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "05m20c7qslgpd1dvhq373kj2h641zm41g1fh0c497pk778lqw556")))

