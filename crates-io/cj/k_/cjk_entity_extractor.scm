(define-module (crates-io cj k_ cjk_entity_extractor) #:use-module (crates-io))

(define-public crate-cjk_entity_extractor-0.0.1 (c (n "cjk_entity_extractor") (v "0.0.1") (h "1y8cfzx849gpq5x7fms2p357cz5hyljvdn8jxmysir11yr8pnp3n") (y #t)))

(define-public crate-cjk_entity_extractor-0.0.2 (c (n "cjk_entity_extractor") (v "0.0.2") (d (list (d (n "cjk") (r "^0.1") (d #t) (k 0)))) (h "1vc9caqgk4zbzdlvr0ahjcms0m6na214dz7ymd0szg9zsr2a7bpj") (y #t)))

