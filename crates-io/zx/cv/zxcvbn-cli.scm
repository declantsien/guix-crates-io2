(define-module (crates-io zx cv zxcvbn-cli) #:use-module (crates-io))

(define-public crate-zxcvbn-cli-1.0.0 (c (n "zxcvbn-cli") (v "1.0.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "colored") (r "^1.8.0") (d #t) (k 0)) (d (n "crossterm_cursor") (r "^0.2.3") (d #t) (k 0)) (d (n "rpassword") (r "^3.0.2") (d #t) (k 0)) (d (n "zxcvbn") (r "^1.0.1") (d #t) (k 0)))) (h "0qp20rfxjlkg1phl7c1lf0ri1rnj3xaz121v64hh11xvjlpm81nd")))

(define-public crate-zxcvbn-cli-1.0.1 (c (n "zxcvbn-cli") (v "1.0.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "colored") (r "^1.8.0") (d #t) (k 0)) (d (n "crossterm_cursor") (r "^0.2.3") (d #t) (k 0)) (d (n "rpassword") (r "^3.0.2") (d #t) (k 0)) (d (n "zxcvbn") (r "^1.0.1") (d #t) (k 0)))) (h "081dkijcz0qlkrcqjcjk2j09881vb9l0bgm7gzwk45kvqhig4cqc")))

(define-public crate-zxcvbn-cli-1.1.0 (c (n "zxcvbn-cli") (v "1.1.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "colored") (r "^1.8.0") (d #t) (k 0)) (d (n "crossterm_cursor") (r "^0.2.3") (d #t) (k 0)) (d (n "rpassword") (r "^3.0.2") (d #t) (k 0)) (d (n "zxcvbn") (r "^1.0.1") (d #t) (k 0)))) (h "0wkj9k8czgdsncq3dhaax55sbnbd9d4mwrpq5wb2n0gi250kwlc2")))

(define-public crate-zxcvbn-cli-2.0.0 (c (n "zxcvbn-cli") (v "2.0.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "colored") (r "^1.9.0") (d #t) (k 0)) (d (n "crossterm_cursor") (r "^0.4.0") (d #t) (k 0)) (d (n "rpassword") (r "^4.0.1") (d #t) (k 0)) (d (n "zxcvbn") (r "^2.0.0") (d #t) (k 0)))) (h "09zn24hdgmmxfp1n7clj06kc5dsy2pyl43zjk28fj4kpdhjby55d")))

(define-public crate-zxcvbn-cli-2.0.1 (c (n "zxcvbn-cli") (v "2.0.1") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "crossterm_cursor") (r "^0.4.0") (d #t) (k 0)) (d (n "rpassword") (r "^5.0.0") (d #t) (k 0)) (d (n "zxcvbn") (r "^2.0.1") (d #t) (k 0)))) (h "044mb29mlr5x82v359qwlfhbc9w7nplp6g8577gb0f3krsx2k5y3")))

(define-public crate-zxcvbn-cli-2.0.2 (c (n "zxcvbn-cli") (v "2.0.2") (d (list (d (n "clap") (r "^3.1.18") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "crossterm") (r "^0.23.2") (d #t) (k 0)) (d (n "rpassword") (r "^6.0.1") (d #t) (k 0)) (d (n "zxcvbn") (r "^2.1.0") (d #t) (k 0)))) (h "1aaf31w5kl3w4hv9xk39ab1hwg4fn15sj5r8g2fnl09flclgg1mh")))

(define-public crate-zxcvbn-cli-2.0.3 (c (n "zxcvbn-cli") (v "2.0.3") (d (list (d (n "clap") (r "^4.1") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "crossterm") (r "^0.25") (d #t) (k 0)) (d (n "rpassword") (r "^7.2") (d #t) (k 0)) (d (n "zxcvbn") (r "^2.1") (d #t) (k 0)))) (h "0k4qsf0js10634x3fhc0whc86nvk9n1a646ibqi8nvh74sihkh6n")))

