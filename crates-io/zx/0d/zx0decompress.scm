(define-module (crates-io zx #{0d}# zx0decompress) #:use-module (crates-io))

(define-public crate-zx0decompress-0.1.0 (c (n "zx0decompress") (v "0.1.0") (d (list (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "1qnlg2x1wv81nkx9bfnvv4k0kgpwmd5i6iwlclidbqmxyhwvvyl4")))

(define-public crate-zx0decompress-0.1.1 (c (n "zx0decompress") (v "0.1.1") (d (list (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "08bqighk5b91n4v7gg4w80lniycqzlgi9j013i8ryz4l4753dwa8")))

(define-public crate-zx0decompress-0.1.2 (c (n "zx0decompress") (v "0.1.2") (d (list (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "10nmh1vfr53hjni8311501h596l8fks4w78g6jnj7k5s2zfygm75")))

