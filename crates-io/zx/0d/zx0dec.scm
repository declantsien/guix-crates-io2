(define-module (crates-io zx #{0d}# zx0dec) #:use-module (crates-io))

(define-public crate-zx0dec-0.1.0 (c (n "zx0dec") (v "0.1.0") (d (list (d (n "clap") (r "^4.3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "zx0decompress") (r "^0.1.2") (d #t) (k 0)))) (h "1i6xhflhx72rlc06byw88jz1pjvpdfil3a93531sbpn19cnskihb")))

