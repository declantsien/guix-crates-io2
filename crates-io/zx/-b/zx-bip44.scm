(define-module (crates-io zx -b zx-bip44) #:use-module (crates-io))

(define-public crate-zx-bip44-0.1.0 (c (n "zx-bip44") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "1q5dy75gdh8gnk9nf78ps1n82py4dln60qf0blyr19hykmb7pl9f")))

