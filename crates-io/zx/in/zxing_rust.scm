(define-module (crates-io zx in zxing_rust) #:use-module (crates-io))

(define-public crate-zxing_rust-0.1.0 (c (n "zxing_rust") (v "0.1.0") (h "156xw3686zg3cj94dja04nssc3n8z11sahhaz48nfkhp1s7y59py")))

(define-public crate-zxing_rust-0.1.1 (c (n "zxing_rust") (v "0.1.1") (d (list (d (n "image") (r "^0.23") (o #t) (d #t) (k 0)))) (h "05sdf9yg8a6pc3vpj9qq65cfahxlncp43di3w2mrn080lwv67fkn")))

