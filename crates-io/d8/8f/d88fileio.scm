(define-module (crates-io d8 #{8f}# d88fileio) #:use-module (crates-io))

(define-public crate-D88FileIO-0.0.1 (c (n "D88FileIO") (v "0.0.1") (h "1k1lk385bldhdlns3n80av7v3kyfh768wmswm3ajx0i1ypxk0vqs")))

(define-public crate-D88FileIO-0.0.2 (c (n "D88FileIO") (v "0.0.2") (h "0fhpv57vs74ynkj011p8ynybhrp4b367xy603xac1r4bp5dbvzy8")))

(define-public crate-D88FileIO-0.0.3 (c (n "D88FileIO") (v "0.0.3") (h "12gpamfppsm87smn7lq9frqlxbk700cbd696fsgnsafgixxznms3")))

(define-public crate-D88FileIO-0.0.4 (c (n "D88FileIO") (v "0.0.4") (h "0mwf3jnx49y01z8rfwx7555y9v8n9jac17xdd6hxbxnp6g1givjh")))

(define-public crate-D88FileIO-0.0.5 (c (n "D88FileIO") (v "0.0.5") (h "0jm18whw9q65ijxryq1fg6h6575zk9nf7awhpl4flxsr31hp77f1")))

(define-public crate-D88FileIO-0.0.6 (c (n "D88FileIO") (v "0.0.6") (h "19ay3skj85xbsq6hpvafq5aibynha8023yz9k9a3ka85x2ckxncb")))

(define-public crate-D88FileIO-0.0.7 (c (n "D88FileIO") (v "0.0.7") (h "0l4y221lbyv6acd1bjak94z7gnj48k7nb6bcx77lnp0sw2ippnv3")))

(define-public crate-D88FileIO-0.0.8 (c (n "D88FileIO") (v "0.0.8") (h "0m8jj64vzsak2hy93scvkhhxqllmsxggkb25xxwv3g722wba12w2")))

