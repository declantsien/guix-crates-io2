(define-module (crates-io d8 #{8d}# d88dmp) #:use-module (crates-io))

(define-public crate-d88dmp-0.12.2 (c (n "d88dmp") (v "0.12.2") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "clap") (r "^3.0.13") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1d7ycsk70mxadwlr01xfj4xpd2mag0zcjf748rnfqn2b001aw1rv") (y #t)))

(define-public crate-d88dmp-0.12.3 (c (n "d88dmp") (v "0.12.3") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "clap") (r "^3.0.13") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "09md3zhpnalcs54ab40nwmngssxjw526m5p8d0y400ra1xf1y8fx") (y #t)))

(define-public crate-d88dmp-0.12.4 (c (n "d88dmp") (v "0.12.4") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "clap") (r "^3.0.13") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "16jp6hwwp9w13lv3qagv6v7ysj7qjv5l3pfssvz0p2hp5a4nnajl")))

(define-public crate-d88dmp-0.12.5 (c (n "d88dmp") (v "0.12.5") (d (list (d (n "D88FileIO") (r "^0.0.1") (d #t) (k 0)) (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "clap") (r "^3.0.13") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "012lv16cmbg43vql0bli8b25bzvb7rnnn5lgfl5s866pqn5bsj6a")))

(define-public crate-d88dmp-0.12.6 (c (n "d88dmp") (v "0.12.6") (d (list (d (n "D88FileIO") (r "^0.0.3") (d #t) (k 0)) (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "clap") (r "^3.0.13") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0xbv7rx7mhz4xajays0xrsh39wfw2abc2rf7scn3bpcgiyg38m10")))

(define-public crate-d88dmp-0.13.0 (c (n "d88dmp") (v "0.13.0") (d (list (d (n "D88FileIO") (r "^0.0.5") (d #t) (k 0)) (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "clap") (r "^3.0.13") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1wv8bhdcihpp8nr40vaj8ya7rxpra73c79bh72vbmjz9b4gz7hsn")))

(define-public crate-d88dmp-0.13.1 (c (n "d88dmp") (v "0.13.1") (d (list (d (n "D88FileIO") (r "^0.0.7") (d #t) (k 0)) (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "clap") (r "^3.0.13") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "08zk47lcbnz01kbrvlf7dsl0jks7jsg5jb56gyqr1d1z6cqp11ff")))

(define-public crate-d88dmp-0.13.2 (c (n "d88dmp") (v "0.13.2") (d (list (d (n "D88FileIO") (r "^0.0.7") (d #t) (k 0)) (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "clap") (r "^3.0.13") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1rzjsqpd4h75wdyirm7m121qxn9l9j4x0rmlhq5whh6bjbrk815n")))

(define-public crate-d88dmp-0.13.3 (c (n "d88dmp") (v "0.13.3") (d (list (d (n "D88FileIO") (r "^0.0.8") (d #t) (k 0)) (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "clap") (r "^3.0.13") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1xpp09vb4nr5g8ks6lvrlx09q791bdrk7dkp8nanh90px6v15i03")))

(define-public crate-d88dmp-0.13.4 (c (n "d88dmp") (v "0.13.4") (d (list (d (n "D88FileIO") (r "^0.0.8") (d #t) (k 0)) (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "clap") (r "^3.0.13") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0p05j4j9n6249360bxvax44n8wza9irvkb8w9n0f2xr38m9v8djx")))

(define-public crate-d88dmp-0.14.0 (c (n "d88dmp") (v "0.14.0") (d (list (d (n "D88FileIO") (r "^0.0.8") (d #t) (k 0)) (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "clap") (r "^3.0.13") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0ansk3fjyfzpwqpkkdkrpgw7ba1slr9gws9s6r5mlixxplwk2k2c")))

