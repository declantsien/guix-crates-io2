(define-module (crates-io kr un krunner) #:use-module (crates-io))

(define-public crate-krunner-0.1.1 (c (n "krunner") (v "0.1.1") (d (list (d (n "async-trait") (r "^0.1.73") (o #t) (d #t) (k 0)) (d (n "dbus") (r "^0.9.7") (d #t) (k 0)) (d (n "dbus-crossroads") (r "^0.5.2") (d #t) (k 0)) (d (n "dbus-tokio") (r "^0.7.6") (o #t) (d #t) (k 0)) (d (n "krunner-derive") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("rt" "sync"))) (o #t) (d #t) (k 0)))) (h "01c69254ipqxnjzs2l4hdilq3byl9x40xaawhywzwswgc2kgq8rj") (s 2) (e (quote (("tokio" "dep:async-trait" "dep:dbus-tokio" "dep:tokio") ("derive" "dep:krunner-derive"))))))

