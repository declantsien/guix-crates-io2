(define-module (crates-io kr un krunner-derive) #:use-module (crates-io))

(define-public crate-krunner-derive-0.1.0 (c (n "krunner-derive") (v "0.1.0") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (d #t) (k 0)))) (h "0809q5jinp6bqdfan0nnj8b4njxxlf9h9jpgmavam0hi4lg7xhz3")))

