(define-module (crates-io kr un krunvm) #:use-module (crates-io))

(define-public crate-krunvm-0.1.6 (c (n "krunvm") (v "0.1.6") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "confy") (r "^0.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.82") (d #t) (k 0)) (d (n "serde") (r "^1.0.120") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.120") (d #t) (k 0)) (d (n "text_io") (r "^0.1.8") (d #t) (k 0)))) (h "1pgvmg833d9292gnlh26ms7cfvfhjbqx1vnhii6d0k3zz0228ljy")))

