(define-module (crates-io kr h_ krh_earcut) #:use-module (crates-io))

(define-public crate-krh_earcut-0.1.0 (c (n "krh_earcut") (v "0.1.0") (d (list (d (n "glam") (r "^0.27.0") (k 0)) (d (n "macroquad") (r "^0.4.5") (d #t) (k 2)))) (h "0ak5iab95m6r4qxs7lfypgs1gnrrchkcm0pzrqd5wjqm5rfyxdgx") (f (quote (("std" "glam/std") ("libm" "glam/libm") ("default" "std"))))))

