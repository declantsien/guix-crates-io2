(define-module (crates-io kr us krust_audio) #:use-module (crates-io))

(define-public crate-krust_audio-0.0.1 (c (n "krust_audio") (v "0.0.1") (d (list (d (n "krust_asset_loader") (r "^0.0.1") (d #t) (k 0)) (d (n "rodio") (r "^0.8.1") (d #t) (k 0)) (d (n "walkdir") (r "^2.2.7") (d #t) (k 1)))) (h "0w2s31v4q9a9n9vimrd95ryp62mldgr7l5ww5yp86h0ir8658ran")))

(define-public crate-krust_audio-0.0.2 (c (n "krust_audio") (v "0.0.2") (d (list (d (n "krust_asset_loader") (r "^0.0.1") (d #t) (k 0)) (d (n "rodio") (r "^0.8.1") (d #t) (k 0)) (d (n "walkdir") (r "^2.2.7") (d #t) (k 1)))) (h "0zgnlb2q023jnnb0bldq1vpyxdpvlx6sqj0kxhlkf1y5wjldfac8")))

