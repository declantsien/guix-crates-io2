(define-module (crates-io kr us krust_timer) #:use-module (crates-io))

(define-public crate-krust_timer-0.0.1 (c (n "krust_timer") (v "0.0.1") (h "1w9kwxlcaa6gnzk47qwqndn2g6m0v9lcx878ybi77y6dy0qj9xac")))

(define-public crate-krust_timer-0.0.2 (c (n "krust_timer") (v "0.0.2") (h "062lz42kkvl07mb3k8hjdmprk2a2bz8hp1blvkbmlav2nvz3x3qd")))

