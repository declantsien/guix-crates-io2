(define-module (crates-io kr us krush-engine) #:use-module (crates-io))

(define-public crate-krush-engine-0.1.0 (c (n "krush-engine") (v "0.1.0") (h "1gr79wryw1h5zn7qbm5yp20y7bdm1xjd8v3vx7k2824x1p5gxpr3")))

(define-public crate-krush-engine-0.2.0 (c (n "krush-engine") (v "0.2.0") (h "0z3rw32li18ycygvnnv2qiym2jr5mnlx87xhf2r725qydwgx746d")))

(define-public crate-krush-engine-0.3.0 (c (n "krush-engine") (v "0.3.0") (h "003yks0yh2qqvsf15l8m6ygabdyrysk7scgklk2c1vrknrg67331")))

