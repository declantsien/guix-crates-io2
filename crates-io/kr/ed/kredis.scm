(define-module (crates-io kr ed kredis) #:use-module (crates-io))

(define-public crate-kredis-0.1.0 (c (n "kredis") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "kresp") (r "^0.1.1") (d #t) (k 0)) (d (n "multistream") (r "^0.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1yg421wbsnb69qcdwfazxaghaaka44wmwzynxv2hrlbc84fclc2j")))

