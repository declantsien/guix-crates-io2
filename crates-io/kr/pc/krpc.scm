(define-module (crates-io kr pc krpc) #:use-module (crates-io))

(define-public crate-krpc-0.1.0 (c (n "krpc") (v "0.1.0") (d (list (d (n "rmp-serde") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("net" "io-util" "sync" "rt" "macros" "time"))) (d #t) (k 0)))) (h "188fyhlnynzjmy206mnh1z27lmcv31hf9wjqy59d2ij95rq10pvi")))

(define-public crate-krpc-0.1.1 (c (n "krpc") (v "0.1.1") (d (list (d (n "rmp-serde") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("net" "io-util" "sync" "rt" "macros" "time"))) (d #t) (k 0)))) (h "1kpq4c3jcnqp155fwm4672h47g2ghxis7blhkxd1pv17v35r0w6w")))

(define-public crate-krpc-0.1.2 (c (n "krpc") (v "0.1.2") (d (list (d (n "rmp-serde") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("net" "io-util" "sync" "rt" "macros" "time"))) (d #t) (k 0)))) (h "0xw5f27vq2dv1hzs2xa34h7gwvvb9h4ma8vzxj9ar03jg596p1yi")))

(define-public crate-krpc-0.1.3 (c (n "krpc") (v "0.1.3") (d (list (d (n "rmp-serde") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("net" "io-util" "sync" "rt" "macros" "time"))) (d #t) (k 0)))) (h "0q61l94zdcc9phwbpv8d8l1lq3jsg9hja8pw237kj7qadvfqg1qi")))

(define-public crate-krpc-0.2.0 (c (n "krpc") (v "0.2.0") (d (list (d (n "rmp-serde") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("net" "io-util" "sync" "rt" "macros" "time"))) (d #t) (k 0)))) (h "13z432j2karnc2aw2kqw8nr44ypgil2k82agr93wxan69620rbbh")))

