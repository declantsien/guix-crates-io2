(define-module (crates-io kr pc krpc-rs) #:use-module (crates-io))

(define-public crate-krpc-rs-0.0.1 (c (n "krpc-rs") (v "0.0.1") (d (list (d (n "quick-protobuf") (r "^0.5.0") (d #t) (k 0)))) (h "0y40nkbckgq9ws4xav738hr0racf8ylp9mjhpmcp07dclanphprd")))

(define-public crate-krpc-rs-0.0.2 (c (n "krpc-rs") (v "0.0.2") (d (list (d (n "quick-protobuf") (r "^0.5.0") (d #t) (k 0)))) (h "1ajgq03044r2fhwh79ixmibgfjmmjhihzqkg04l3sw73g8aw177x")))

(define-public crate-krpc-rs-0.1.0 (c (n "krpc-rs") (v "0.1.0") (d (list (d (n "quick-protobuf") (r "^0.5.0") (d #t) (k 0)))) (h "0q7ariwrw1z41y0vivlx37y2wlaxlhp8iz4b7vchkmllf2jfm69d")))

(define-public crate-krpc-rs-0.1.1 (c (n "krpc-rs") (v "0.1.1") (d (list (d (n "quick-protobuf") (r "^0.5.0") (d #t) (k 0)))) (h "1nax6qpcphhwsl12kybh3hx48ha2m9g91jav63m33s67m45nakwl")))

(define-public crate-krpc-rs-0.2.0 (c (n "krpc-rs") (v "0.2.0") (d (list (d (n "protobuf") (r "^1.4.1") (d #t) (k 0)))) (h "15ylz7jygw09agldqdygjaryq8d00b4fg889diypc9rzkv8d5sr0")))

(define-public crate-krpc-rs-0.3.0 (c (n "krpc-rs") (v "0.3.0") (d (list (d (n "protobuf") (r "^1.4.1") (d #t) (k 0)))) (h "0wpmwqhh6px0diwr0ljwal6ngrfqyydrq78z8fz6r6lp4qrwrish")))

(define-public crate-krpc-rs-0.3.1 (c (n "krpc-rs") (v "0.3.1") (d (list (d (n "protobuf") (r "^1.4.1") (d #t) (k 0)))) (h "07n1s1qs1m5z6k2ry92gvxwwd9xy6qi0ad2r86ihwh9zb3b663fp")))

