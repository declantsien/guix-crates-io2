(define-module (crates-io kr pc krpc-macro) #:use-module (crates-io))

(define-public crate-krpc-macro-0.2.0 (c (n "krpc-macro") (v "0.2.0") (d (list (d (n "krpc-common") (r "^0.2.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1x8zi3m6mv1yrvjjrnva1lwq372zjlgvq6hm8m32wr4vbjhlnwh3")))

