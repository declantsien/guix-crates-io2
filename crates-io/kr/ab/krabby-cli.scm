(define-module (crates-io kr ab krabby-cli) #:use-module (crates-io))

(define-public crate-krabby-cli-0.1.0 (c (n "krabby-cli") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "clap") (r "^4.3.3") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "indexmap") (r "^1.9.3") (f (quote ("serde"))) (d #t) (k 0)) (d (n "owo-colors") (r "^3.5.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "04vlh1jcipxj1g63jmfy0i2j74y93imdz2a32wg36cssvfypmci5")))

