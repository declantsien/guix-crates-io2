(define-module (crates-io kr ab krab) #:use-module (crates-io))

(define-public crate-krab-0.1.0 (c (n "krab") (v "0.1.0") (d (list (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.39") (d #t) (k 0)))) (h "1x13sl406r18ys9s3f7xbnafmwv3q7p2hg11h45gjbh4c1k806my")))

(define-public crate-krab-0.1.1 (c (n "krab") (v "0.1.1") (d (list (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.39") (d #t) (k 0)))) (h "117i93g53h1n9adihy5va33xpklx35vixlw9nla4xnm9a6alv6hx")))

(define-public crate-krab-0.1.2 (c (n "krab") (v "0.1.2") (d (list (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.39") (d #t) (k 0)))) (h "08hcgchlsx566mfkapykm9w96bgzh5yplism5hgjyqbriy9hwafa")))

(define-public crate-krab-0.1.3 (c (n "krab") (v "0.1.3") (d (list (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.39") (d #t) (k 0)))) (h "1kxm77mg6cw4swyf5ad6777w6401467r6n5x8m4khcsni09w8rgz")))

