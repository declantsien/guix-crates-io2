(define-module (crates-io kr ab krabligraphy) #:use-module (crates-io))

(define-public crate-krabligraphy-0.1.0 (c (n "krabligraphy") (v "0.1.0") (d (list (d (n "clap") (r "^4.2.7") (f (quote ("derive"))) (d #t) (k 0)))) (h "06v90sch7wj75hcml94s3iy1056ngz2vhp5pkhybrs82z12szv2p")))

(define-public crate-krabligraphy-0.1.1 (c (n "krabligraphy") (v "0.1.1") (d (list (d (n "clap") (r "^4.2.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0v1ls1rnzx3n3qa85my0k8aj1xagqb7qsn0fivq37cia3cbahcns")))

(define-public crate-krabligraphy-0.1.2 (c (n "krabligraphy") (v "0.1.2") (d (list (d (n "clap") (r "^4.2.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1im5z6qvzwrxhfc23f1bxs1qycda908g8kyp7x2chjdkfldidf18")))

(define-public crate-krabligraphy-0.2.0 (c (n "krabligraphy") (v "0.2.0") (d (list (d (n "clap") (r "^4.2.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0jyhgfcmnnn7fab10qvxd879r6kdy39hn9y31c99bwjpzfbvi07b")))

(define-public crate-krabligraphy-0.2.1 (c (n "krabligraphy") (v "0.2.1") (d (list (d (n "clap") (r "^4.2.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "00z78d7v5mmq5wfypnqjgpd1p4zadp4a4a23gcqq19vj867zwmb7")))

