(define-module (crates-io kr cl krcli) #:use-module (crates-io))

(define-public crate-krcli-0.1.0 (c (n "krcli") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "exitcode") (r "^1.1.2") (d #t) (k 0)) (d (n "keyring") (r "^0.10.1") (d #t) (k 0)))) (h "0nvapf5g50m9brha1kq75j80djz2ks4s3rw6vx47mkbq142j32ys")))

(define-public crate-krcli-0.1.1 (c (n "krcli") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "exitcode") (r "^1.1.2") (d #t) (k 0)) (d (n "keyring") (r "^0.10.1") (d #t) (k 0)))) (h "1sfzcn2rhbf1w2iybz56r0i358g6hsk4pf2bi8c87q7bg4q7fxml")))

