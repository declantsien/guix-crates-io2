(define-module (crates-io kr ec krecik-cli) #:use-module (crates-io))

(define-public crate-krecik-cli-0.2.0 (c (n "krecik-cli") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.11") (d #t) (k 0)) (d (n "colored") (r "^1.9.3") (d #t) (k 0)) (d (n "fern") (r "^0.6.0") (f (quote ("colored"))) (d #t) (k 0)) (d (n "krecik") (r "^0.6.8") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "0knfqmjvswfy54kjq90skpxh22gzwwi7ad11yy5anz7h1d3h6zzn") (y #t)))

(define-public crate-krecik-cli-0.2.1 (c (n "krecik-cli") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4.11") (d #t) (k 0)) (d (n "colored") (r "^1.9.3") (d #t) (k 0)) (d (n "fern") (r "^0.6.0") (f (quote ("colored"))) (d #t) (k 0)) (d (n "krecik") (r "^0.6.10") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "1d6vgwj1ia742d6d8j4rnbzjry0k4807n8v6kkvjq25lwzzaymjh") (y #t)))

(define-public crate-krecik-cli-0.3.0 (c (n "krecik-cli") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4.13") (d #t) (k 0)) (d (n "colored") (r "^1.9.3") (d #t) (k 0)) (d (n "ctrlc") (r "^3.1.4") (d #t) (k 0)) (d (n "fern") (r "^0.6.0") (f (quote ("colored"))) (d #t) (k 0)) (d (n "krecik") (r "^0.8.0") (d #t) (k 0)) (d (n "lockfile") (r "^0.2.2") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "1srgkp7ih8gqgnh1dc59z91j8ck9zz1irgr0hgcrv1nxpz90qcwg") (y #t)))

(define-public crate-krecik-cli-0.3.1 (c (n "krecik-cli") (v "0.3.1") (d (list (d (n "chrono") (r "^0.4.13") (d #t) (k 0)) (d (n "colored") (r "^1.9.3") (d #t) (k 0)) (d (n "ctrlc") (r "^3.1.4") (d #t) (k 0)) (d (n "fern") (r "^0.6.0") (f (quote ("colored"))) (d #t) (k 0)) (d (n "krecik") (r "^0.8.1") (d #t) (k 0)) (d (n "lockfile") (r "^0.2.2") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "1z026hrbyqbhrw4abm6g0dz1asz5lf7rcm9xnvmhdzyk6frw7jqa") (y #t)))

(define-public crate-krecik-cli-0.3.2 (c (n "krecik-cli") (v "0.3.2") (d (list (d (n "chrono") (r "^0.4.13") (d #t) (k 0)) (d (n "colored") (r "^1.9.3") (d #t) (k 0)) (d (n "ctrlc") (r "^3.1.4") (d #t) (k 0)) (d (n "fern") (r "^0.6.0") (f (quote ("colored"))) (d #t) (k 0)) (d (n "krecik") (r "^0.8.2") (d #t) (k 0)) (d (n "lockfile") (r "^0.2.2") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "1r8z7ybq48y61l5gx4igxb73iyz61ifzp7xqc05px2c57g2pbypf") (y #t)))

(define-public crate-krecik-cli-0.3.3 (c (n "krecik-cli") (v "0.3.3") (d (list (d (n "chrono") (r "^0.4.13") (d #t) (k 0)) (d (n "colored") (r "^1.9.3") (d #t) (k 0)) (d (n "ctrlc") (r "^3.1.4") (d #t) (k 0)) (d (n "fern") (r "^0.6.0") (f (quote ("colored"))) (d #t) (k 0)) (d (n "krecik") (r "^0.8.3") (d #t) (k 0)) (d (n "lockfile") (r "^0.2.2") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "15pqc69wgshpgdyq9dcv0v3vgdn6314xnsz8mczwqqd08rw27ixq") (y #t)))

