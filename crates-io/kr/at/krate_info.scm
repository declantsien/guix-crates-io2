(define-module (crates-io kr at krate_info) #:use-module (crates-io))

(define-public crate-krate_info-0.1.0 (c (n "krate_info") (v "0.1.0") (d (list (d (n "crates_io_api") (r "^0.8.1") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (d #t) (k 0)) (d (n "semver") (r "^1.0.18") (d #t) (k 0)))) (h "004vyka38w7sxnplkp7kzlvhnnma0wbzjm8fqnz8hyyskzv70r2f")))

(define-public crate-krate_info-0.1.1 (c (n "krate_info") (v "0.1.1") (d (list (d (n "crates_io_api") (r "^0.8.1") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (d #t) (k 0)) (d (n "semver") (r "^1.0.18") (d #t) (k 0)))) (h "12vhmzqw5xhp6r9vv6mzpdr28mgbqh1wzpp3lhgzkjwz039xbw5v")))

