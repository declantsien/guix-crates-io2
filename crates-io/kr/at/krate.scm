(define-module (crates-io kr at krate) #:use-module (crates-io))

(define-public crate-krate-0.1.0 (c (n "krate") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.14") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "tokio") (r "^1.24.2") (f (quote ("full"))) (d #t) (k 2)))) (h "0r49rqpz8359ynr396sqvpn4im66rzif53al1bzh0n860wp6ws9q")))

(define-public crate-krate-0.2.0 (c (n "krate") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.14") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "tokio") (r "^1.24.2") (f (quote ("full"))) (d #t) (k 2)))) (h "1mvk846f9c23y5dlfbhj29qsjy7w4y7m2icni7fd7ijjl9hnknbg")))

(define-public crate-krate-0.3.0 (c (n "krate") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.14") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "tokio") (r "^1.24.2") (f (quote ("full"))) (d #t) (k 2)))) (h "0d4m44dp54pvdm428fxvy3nnmr7gyylfhz920dgprw63s8lcgdnc")))

(define-public crate-krate-0.4.0 (c (n "krate") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.14") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "tokio") (r "^1.24.2") (f (quote ("full"))) (d #t) (k 2)))) (h "09z2m0i1nra7n90jrrggsn76mzc9kd8xr1mwjrh5abp7g8hjff53")))

(define-public crate-krate-0.4.1 (c (n "krate") (v "0.4.1") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.14") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "tokio") (r "^1.24.2") (f (quote ("full"))) (d #t) (k 2)))) (h "0m6sqb7dlg3dvg498c4vfd0ic91063h7pw8gwskrfcqnk2xzq3vq")))

(define-public crate-krate-1.0.0 (c (n "krate") (v "1.0.0") (d (list (d (n "reqwest") (r "^0.11.14") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "tokio") (r "^1.24.2") (f (quote ("full"))) (d #t) (k 2)))) (h "1w1d8addqpdz19w9a4b5986yly4vyf4lxf53z5n5bv885w5107dm")))

