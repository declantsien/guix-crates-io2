(define-module (crates-io kr at krata-xenstore) #:use-module (crates-io))

(define-public crate-krata-xenstore-0.0.1 (c (n "krata-xenstore") (v "0.0.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "env_logger") (r "^0.11.0") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("full"))) (d #t) (k 0)))) (h "07cyf63jgm7fmgkjbby9ndgcqrmm6dz9vbzib2vl20w9ba7ik7yi")))

(define-public crate-krata-xenstore-0.0.2 (c (n "krata-xenstore") (v "0.0.2") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "env_logger") (r "^0.11.0") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("full"))) (d #t) (k 0)))) (h "01fyb9sr33c1r58by3pwydydrbbcbz9imx8rw2ggnxmlxgscxmkd")))

(define-public crate-krata-xenstore-0.0.3 (c (n "krata-xenstore") (v "0.0.3") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "env_logger") (r "^0.11.0") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("full"))) (d #t) (k 0)))) (h "1balw94cknbp03nw9clrc6jjb00vw2zqs4lffb04bx4rzbxp2q6a")))

(define-public crate-krata-xenstore-0.0.4 (c (n "krata-xenstore") (v "0.0.4") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "env_logger") (r "^0.11.0") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("full"))) (d #t) (k 0)))) (h "0zswh26yxjf3x83mhiydvykdy000zn4wvrkx6x6mxk0j0fi8n3c7")))

(define-public crate-krata-xenstore-0.0.5 (c (n "krata-xenstore") (v "0.0.5") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "env_logger") (r "^0.11.0") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("full"))) (d #t) (k 0)))) (h "0bjzn804lkfb673pd2gylsfyzh7arsn38rbadvh59rc6mlp0h450")))

(define-public crate-krata-xenstore-0.0.6 (c (n "krata-xenstore") (v "0.0.6") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "env_logger") (r "^0.11.0") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("full"))) (d #t) (k 0)))) (h "00kwykhrcgbx4kx06cnajvxsz612p0nsi69r86jrjzz42i2hrs7y")))

(define-public crate-krata-xenstore-0.0.7 (c (n "krata-xenstore") (v "0.0.7") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "env_logger") (r "^0.11.0") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("full"))) (d #t) (k 0)))) (h "0l4218nd0sr3gcir9sivrlmwgf39nb8ks7qfg5w1hcixm8dqcvnb")))

(define-public crate-krata-xenstore-0.0.8 (c (n "krata-xenstore") (v "0.0.8") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "env_logger") (r "^0.11.0") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("full"))) (d #t) (k 0)))) (h "1pzfbda2xi5rdfssj1j37ifv9ys7dhs45s48blj837rr6dlrfdih")))

(define-public crate-krata-xenstore-0.0.9 (c (n "krata-xenstore") (v "0.0.9") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "env_logger") (r "^0.11.0") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("full"))) (d #t) (k 0)))) (h "1xafxdzz4nw0nhkifv0h5vpl59q44z8zjg7a9zbczcfsamnf8lvb")))

(define-public crate-krata-xenstore-0.0.10 (c (n "krata-xenstore") (v "0.0.10") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "env_logger") (r "^0.11.0") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("full"))) (d #t) (k 0)))) (h "0bf2zfkragx8k2nx0pxvq6bwwqqw0c210nz1rgl8sykg967lh17n")))

