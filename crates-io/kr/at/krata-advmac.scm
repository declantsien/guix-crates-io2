(define-module (crates-io kr at krata-advmac) #:use-module (crates-io))

(define-public crate-krata-advmac-1.1.0 (c (n "krata-advmac") (v "1.1.0") (d (list (d (n "arrayvec") (r "^0.7.2") (k 0)) (d (n "rand") (r "^0.8.5") (f (quote ("getrandom"))) (o #t) (k 0)) (d (n "rstest") (r "^0.16.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.145") (o #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 2)) (d (n "serde_json") (r "^1.0.86") (d #t) (k 2)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "06i1f7016l885bykad9wpi3kfmzxq9z6s1wzzlllfixr9fnny5ck") (f (quote (("default" "std" "rand" "serde")))) (s 2) (e (quote (("std" "serde?/std") ("serde" "dep:serde" "arrayvec/serde") ("rand" "dep:rand")))) (r "1.64")))

