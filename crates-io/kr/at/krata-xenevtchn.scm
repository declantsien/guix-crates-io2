(define-module (crates-io kr at krata-xenevtchn) #:use-module (crates-io))

(define-public crate-krata-xenevtchn-0.0.2 (c (n "krata-xenevtchn") (v "0.0.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "nix") (r "^0.28.0") (f (quote ("ioctl"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("full"))) (d #t) (k 0)))) (h "0p6apc12ij9bap47l3xjqzzjh4q5mxx2c6sfg0ib5jz7bjzkgxna")))

(define-public crate-krata-xenevtchn-0.0.3 (c (n "krata-xenevtchn") (v "0.0.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "nix") (r "^0.28.0") (f (quote ("ioctl"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("full"))) (d #t) (k 0)))) (h "07qa9n1nvbrd7g5d8pfmxwmawgc1xwj3spyb85r1xbdlkjx3db5x")))

(define-public crate-krata-xenevtchn-0.0.4 (c (n "krata-xenevtchn") (v "0.0.4") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "nix") (r "^0.28.0") (f (quote ("ioctl"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("full"))) (d #t) (k 0)))) (h "1jj8z894fbgazxgcml9rrmf6v4gv87xwdhqra05a3g4j9inf5645")))

(define-public crate-krata-xenevtchn-0.0.5 (c (n "krata-xenevtchn") (v "0.0.5") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "nix") (r "^0.28.0") (f (quote ("ioctl"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("full"))) (d #t) (k 0)))) (h "18sm2wgnv3cxyn1pwcy8dc163s6mz7klyz9l5c91dmpyirlf78k1")))

(define-public crate-krata-xenevtchn-0.0.6 (c (n "krata-xenevtchn") (v "0.0.6") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "nix") (r "^0.28.0") (f (quote ("ioctl"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("full"))) (d #t) (k 0)))) (h "1hzd1ij0zwlad1jv8mrcr3ylby0n00sdna1cdizawsfzyap6vy5p")))

(define-public crate-krata-xenevtchn-0.0.7 (c (n "krata-xenevtchn") (v "0.0.7") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "nix") (r "^0.28.0") (f (quote ("ioctl"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("full"))) (d #t) (k 0)))) (h "0dgkqq2zmvarlp3svq7dikjcj8nwgq9mm2gs48jyi899r16v8m35")))

(define-public crate-krata-xenevtchn-0.0.8 (c (n "krata-xenevtchn") (v "0.0.8") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "nix") (r "^0.28.0") (f (quote ("ioctl"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("full"))) (d #t) (k 0)))) (h "0p3wkdhicbk31jshm0kfn8921l159ck9660nhlrd74blnnb7ld5j")))

(define-public crate-krata-xenevtchn-0.0.9 (c (n "krata-xenevtchn") (v "0.0.9") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "nix") (r "^0.28.0") (f (quote ("ioctl"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("full"))) (d #t) (k 0)))) (h "17lx5g6payg90bzin5glhc6c9yzviari1mq2278l3ka8l3anbgbd")))

(define-public crate-krata-xenevtchn-0.0.10 (c (n "krata-xenevtchn") (v "0.0.10") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "nix") (r "^0.28.0") (f (quote ("ioctl"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("full"))) (d #t) (k 0)))) (h "1lbrj6s5nn9qf7mjhzmpjwrfimyp4wm7njss8zl5cn7daqk5s63a")))

