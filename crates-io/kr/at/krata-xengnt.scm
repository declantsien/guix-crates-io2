(define-module (crates-io kr at krata-xengnt) #:use-module (crates-io))

(define-public crate-krata-xengnt-0.0.2 (c (n "krata-xengnt") (v "0.0.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.28.0") (f (quote ("ioctl"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0sk970lawik4hpwbkjnqirnk01n5mha2n9v08rcdh1g4gn061yjd")))

(define-public crate-krata-xengnt-0.0.3 (c (n "krata-xengnt") (v "0.0.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.28.0") (f (quote ("ioctl"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1dsy1si3dmyk16xpzjj9abf97ghw3c8rs40igbnpcvfx7h4li278")))

(define-public crate-krata-xengnt-0.0.4 (c (n "krata-xengnt") (v "0.0.4") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.28.0") (f (quote ("ioctl"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0xah5wxl3p2b047wpl0192qgy0f7g7s1q39kj701zjqi5nrzdfwq")))

(define-public crate-krata-xengnt-0.0.5 (c (n "krata-xengnt") (v "0.0.5") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.28.0") (f (quote ("ioctl"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1a4gdcs75xbp082imsfp72x4igcbnaqg5kivhk9q73bqvj5dadda")))

(define-public crate-krata-xengnt-0.0.6 (c (n "krata-xengnt") (v "0.0.6") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.28.0") (f (quote ("ioctl"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0hy6k6naj031k075f19y960wm2f1a2fg8g676dwq8h96kjjx52p9")))

(define-public crate-krata-xengnt-0.0.7 (c (n "krata-xengnt") (v "0.0.7") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.28.0") (f (quote ("ioctl"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1gj7h61js759828872vhf0c9dhbz7ixy84pgfr8nchrvsi6r8pj5")))

(define-public crate-krata-xengnt-0.0.8 (c (n "krata-xengnt") (v "0.0.8") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.28.0") (f (quote ("ioctl"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "02b7nl1wvsyr0s0518xxdbvkq3y0dp4c53yswig1h2xfz8kzahsh")))

(define-public crate-krata-xengnt-0.0.9 (c (n "krata-xengnt") (v "0.0.9") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.28.0") (f (quote ("ioctl"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1f8la6cpmc858mfv09ap8g310ccdnxch2mmjxnyfb4md7wah90wh")))

(define-public crate-krata-xengnt-0.0.10 (c (n "krata-xengnt") (v "0.0.10") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.28.0") (f (quote ("ioctl"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "142firjcw6xpld323sy41irdg97vifddqn5mamqmglhbb7hjyrd3")))

