(define-module (crates-io kr at krata-tokio-tar) #:use-module (crates-io))

(define-public crate-krata-tokio-tar-0.4.0 (c (n "krata-tokio-tar") (v "0.4.0") (d (list (d (n "filetime") (r "^0.2") (d #t) (k 0)) (d (n "futures-core") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "redox_syscall") (r "^0.3") (d #t) (t "cfg(target_os = \"redox\")") (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("fs" "io-util" "rt"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "tokio-stream") (r "^0.1") (d #t) (k 0)) (d (n "xattr") (r "^1.0") (o #t) (d #t) (t "cfg(unix)") (k 0)))) (h "0wx9mig9glvcvnmkjigk7asb5smw8kvya5i1va95h74chdl4k15s") (f (quote (("default" "xattr"))))))

