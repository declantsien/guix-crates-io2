(define-module (crates-io kr it krita-to-svg) #:use-module (crates-io))

(define-public crate-krita-to-svg-0.2.0 (c (n "krita-to-svg") (v "0.2.0") (d (list (d (n "base64") (r "^0.12") (d #t) (k 0)) (d (n "krita") (r "^0.2") (d #t) (k 0)) (d (n "minidom") (r "^0.12") (d #t) (k 0)) (d (n "png") (r "^0.16") (d #t) (k 0)))) (h "09bc0b338zp0ssyjiz73c94ak6km30423zqp8x07l8jxjhjs8ac2")))

(define-public crate-krita-to-svg-0.2.1 (c (n "krita-to-svg") (v "0.2.1") (d (list (d (n "base64") (r "^0.12") (d #t) (k 0)) (d (n "krita") (r "^0.2") (d #t) (k 0)) (d (n "minidom") (r "^0.12") (d #t) (k 0)) (d (n "png") (r "^0.16") (d #t) (k 0)))) (h "0v2h862iv1vs41r6g3nvxgwgld9ww5ds7fjjgs6jc94n2bp132rn")))

(define-public crate-krita-to-svg-0.2.2 (c (n "krita-to-svg") (v "0.2.2") (d (list (d (n "base64") (r "^0.12") (d #t) (k 0)) (d (n "krita") (r "^0.2") (d #t) (k 0)) (d (n "minidom") (r "^0.12") (d #t) (k 0)) (d (n "png") (r "^0.16") (d #t) (k 0)))) (h "1hj8fnyb6j5m9g32chdldp0qxxwyjd16qrwrm8zbkb6wi1hwcrdd")))

