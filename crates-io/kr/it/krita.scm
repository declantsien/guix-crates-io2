(define-module (crates-io kr it krita) #:use-module (crates-io))

(define-public crate-krita-0.1.0 (c (n "krita") (v "0.1.0") (d (list (d (n "lexical-core") (r "^0.7") (d #t) (k 0)) (d (n "lzf") (r "^0.3") (d #t) (k 0)) (d (n "nom") (r "^5") (f (quote ("std"))) (k 0)) (d (n "png") (r "^0.16") (d #t) (k 2)))) (h "0gr8v051yq6sggvq1mnyfif4db21yazzzhy6px3q67kkynwa71vm")))

(define-public crate-krita-0.2.0 (c (n "krita") (v "0.2.0") (d (list (d (n "lexical-core") (r "^0.7") (d #t) (k 0)) (d (n "lzf") (r "^0.3") (d #t) (k 0)) (d (n "minidom") (r "^0.12") (d #t) (k 0)) (d (n "nom") (r "^5") (f (quote ("std"))) (k 0)) (d (n "png") (r "^0.16") (d #t) (k 2)) (d (n "zip") (r "^0.5") (d #t) (k 0)))) (h "0c3si0ij3xpnm3fi92rhb5887yygqhw47rmwlc7nxz0dg5c3xpag")))

(define-public crate-krita-0.2.1 (c (n "krita") (v "0.2.1") (d (list (d (n "lexical-core") (r "^0.7") (d #t) (k 0)) (d (n "lzf") (r "^0.3") (d #t) (k 0)) (d (n "minidom") (r "^0.12") (d #t) (k 0)) (d (n "nom") (r "^5") (f (quote ("std"))) (k 0)) (d (n "png") (r "^0.16") (d #t) (k 2)) (d (n "zip") (r "^0.5") (d #t) (k 0)))) (h "1aj77g9fzj230g89w9z44bx6fnfa6q6hq3srb77izzvd7kmb7hvf")))

