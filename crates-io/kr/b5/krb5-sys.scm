(define-module (crates-io kr b5 krb5-sys) #:use-module (crates-io))

(define-public crate-krb5-sys-0.1.0 (c (n "krb5-sys") (v "0.1.0") (h "01m1hcm5c5nkj44xxrqbqbdms8sp5nc5ilqd5bkf8ljfbq1sl9r2") (f (quote (("krb5_old_crypto") ("krb5_deprecated") ("default"))))))

(define-public crate-krb5-sys-0.1.1 (c (n "krb5-sys") (v "0.1.1") (h "02s4775wg0bi0y50bsidy6bpd40xmrhn5hlr1s8frfqzgkbamn91") (f (quote (("krb5_old_crypto") ("krb5_deprecated") ("default"))))))

(define-public crate-krb5-sys-0.1.2 (c (n "krb5-sys") (v "0.1.2") (h "0kpvhngmn54kqnj3zkxbkbnc039v6jc01yl1jvzgm6v1qf8rnz7y") (f (quote (("krb5_old_crypto") ("krb5_deprecated") ("default"))))))

(define-public crate-krb5-sys-0.1.3 (c (n "krb5-sys") (v "0.1.3") (h "10jlclw4l702k52dmrhx6gr1zlbx5z2790wf9n233i6mbch01aii") (f (quote (("krb5_old_crypto") ("krb5_deprecated") ("default"))))))

(define-public crate-krb5-sys-0.1.4 (c (n "krb5-sys") (v "0.1.4") (h "1ifp4cgxwbs9fj5pq0vbkb71r90iq1kcc05lszqqlh704rx9dr8a") (f (quote (("krb5_old_crypto") ("krb5_deprecated") ("default"))))))

(define-public crate-krb5-sys-0.2.0 (c (n "krb5-sys") (v "0.2.0") (h "1g9bl57pq9q8ka34wi0dlfhw922jqnzxh9ygpb6wdlf34cvizm6l") (f (quote (("krb5_old_crypto") ("krb5_deprecated") ("default"))))))

(define-public crate-krb5-sys-0.3.0 (c (n "krb5-sys") (v "0.3.0") (h "07vpkgx96cq7a3mgk5p5z37skllrc3rp910qd8rji6rc4dld9k32") (f (quote (("krb5_old_crypto") ("krb5_deprecated") ("default"))))))

