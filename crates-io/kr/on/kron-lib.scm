(define-module (crates-io kr on kron-lib) #:use-module (crates-io))

(define-public crate-kron-lib-2.0.0 (c (n "kron-lib") (v "2.0.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "time") (r "^0.3.21") (f (quote ("formatting"))) (d #t) (k 0)))) (h "19wkcffx2f9izr7w85ipw0gjl8fx04ky6rmwwdx2x84g5gpg5f07")))

(define-public crate-kron-lib-2.1.0 (c (n "kron-lib") (v "2.1.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "time") (r "^0.3.21") (f (quote ("formatting"))) (d #t) (k 0)))) (h "1qn4ll05c4csv125785w705lyvdyp3qaiyv48iadxk46n8i9b1lw")))

