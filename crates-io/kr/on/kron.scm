(define-module (crates-io kr on kron) #:use-module (crates-io))

(define-public crate-kron-0.1.0 (c (n "kron") (v "0.1.0") (h "1cj1ly3cz24b4fbham6x2v3q0k6vyf7jcf2ar4iknax7j2fa5bvk")))

(define-public crate-kron-1.0.0 (c (n "kron") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "time") (r "^0.3.21") (f (quote ("formatting"))) (d #t) (k 0)))) (h "16xhpid4z6q8rhriw628lizza3w5ihrha9dhy27wwjkmql6idzb2")))

(define-public crate-kron-1.1.0 (c (n "kron") (v "1.1.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "time") (r "^0.3.21") (f (quote ("formatting"))) (d #t) (k 0)))) (h "1pa0mcdpr8np6d55ibp4llibwizrms3nrv5dcq1znrwvc9irky3c")))

(define-public crate-kron-2.0.0 (c (n "kron") (v "2.0.0") (d (list (d (n "assert_cmd") (r "^2.0.11") (d #t) (k 2)) (d (n "clap") (r "^4.3.0") (f (quote ("derive" "wrap_help"))) (d #t) (k 0)) (d (n "kron-lib") (r "^2.0.0") (d #t) (k 0)))) (h "1mghdyjb8a7lchspyzw4wwfnp4zcss0vdjx8izp7nhlyhj6naq7a")))

(define-public crate-kron-2.1.0 (c (n "kron") (v "2.1.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0.11") (d #t) (k 2)) (d (n "clap") (r "^4.3.0") (f (quote ("derive" "wrap_help"))) (d #t) (k 0)) (d (n "kron-lib") (r "^2.1.0") (d #t) (k 0)) (d (n "pager") (r "^0.16.1") (d #t) (k 0)))) (h "05mspbwlwf00v6lhsspcrd1qc77gppwc4ij8kc7qds0hjzawn39r")))

