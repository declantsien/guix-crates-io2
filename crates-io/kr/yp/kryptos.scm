(define-module (crates-io kr yp kryptos) #:use-module (crates-io))

(define-public crate-kryptos-0.1.2 (c (n "kryptos") (v "0.1.2") (h "1755wvig8d7pv5hb1f3jrpfh359i2sc80jp9bgrjiqafcrxh0vws")))

(define-public crate-kryptos-0.3.0 (c (n "kryptos") (v "0.3.0") (h "1xq08za2v893c02h121hszm75hsjchcqym2qr1k4anha04hhvjvm")))

(define-public crate-kryptos-0.4.0 (c (n "kryptos") (v "0.4.0") (h "1bjbii5vb3r4cmv1w4w2x2i15v8fj41dylzd46972rmzlxb91yr8")))

(define-public crate-kryptos-0.4.1 (c (n "kryptos") (v "0.4.1") (h "00jv5ja7xfahj3k38k4m3kjmg6wyqjqmsk5k64wh4hpda12s014i")))

(define-public crate-kryptos-0.5.0 (c (n "kryptos") (v "0.5.0") (h "097g7n3ac860h56r6vnhgi9gry8sjh3hnclrwry4nhgdgylq3y0c")))

(define-public crate-kryptos-0.6.0 (c (n "kryptos") (v "0.6.0") (d (list (d (n "regex") (r "^0.2.6") (d #t) (k 0)))) (h "1c2sfcmayyz628ws58r7njb7211hdzk88p1hpn253vbf1gs18rah")))

(define-public crate-kryptos-0.6.1 (c (n "kryptos") (v "0.6.1") (d (list (d (n "regex") (r "^1.0.0") (d #t) (k 0)))) (h "0i1dm9lwashmk1glg1f1nh9wb47x75j0xrssp7sglpnzjr840mv8")))

(define-public crate-kryptos-0.6.2 (c (n "kryptos") (v "0.6.2") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "09lh701dh7dc04kbsfkmkr1rgh73d4kal20f0jbswx87alpwzvw0")))

(define-public crate-kryptos-0.6.3 (c (n "kryptos") (v "0.6.3") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "13f53n521dx007a70bmq1y0psphcmahhqyaj8bh0li641635cb49")))

