(define-module (crates-io kr yp kryptor) #:use-module (crates-io))

(define-public crate-kryptor-0.1.0 (c (n "kryptor") (v "0.1.0") (d (list (d (n "assert_cmd") (r "^2") (d #t) (k 2)) (d (n "base32") (r "^0.4.0") (d #t) (k 0)) (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "predicates") (r "^2") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)))) (h "0vb73gl06ln5imjpz4wsz4r7x66cxzxcj2b3firhrfryzzl1sf16")))

(define-public crate-kryptor-0.1.1 (c (n "kryptor") (v "0.1.1") (d (list (d (n "assert_cmd") (r "^2") (d #t) (k 2)) (d (n "base32") (r "^0.4.0") (d #t) (k 0)) (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "predicates") (r "^2") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)))) (h "0m9g3zhbxmk3f31r4q8k6jxl459vb5wzvf5kxlj11qy2sxpxlis0")))

(define-public crate-kryptor-0.1.2 (c (n "kryptor") (v "0.1.2") (d (list (d (n "assert_cmd") (r "^2") (d #t) (k 2)) (d (n "base32") (r "^0.4.0") (d #t) (k 0)) (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "predicates") (r "^2") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)))) (h "0p764cdv96rrwhs0xsmcaf52n8m2xwdilm76nvahgcy3w89f0db7")))

(define-public crate-kryptor-0.1.3 (c (n "kryptor") (v "0.1.3") (d (list (d (n "assert_cmd") (r "^2") (d #t) (k 2)) (d (n "base32") (r "^0.4.0") (d #t) (k 0)) (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "predicates") (r "^2") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)))) (h "03wyaqhp5vkyjhj8b9vh2r83y8wx5g6fmlfkbnnp3jys6b11wz29")))

