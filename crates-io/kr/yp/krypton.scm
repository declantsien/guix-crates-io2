(define-module (crates-io kr yp krypton) #:use-module (crates-io))

(define-public crate-krypton-0.1.0 (c (n "krypton") (v "0.1.0") (h "0khhw65dmj3h2lfh215w5sjl30av3gy1nnilgl6rli9028mswr1a")))

(define-public crate-krypton-0.1.0-rc.0.1 (c (n "krypton") (v "0.1.0-rc.0.1") (d (list (d (n "rand") (r "^0.6") (d #t) (k 0)))) (h "0mw13y5d48hmd67yy6y1xjwca06dajmi1810mlk9h415kjynwaw2")))

