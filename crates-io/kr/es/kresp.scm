(define-module (crates-io kr es kresp) #:use-module (crates-io))

(define-public crate-kresp-0.1.0 (c (n "kresp") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bstr") (r "^0.2.17") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1fymzdfc6yk0y6dvykhd7lw11fl8q7rfyhfjrwi96gbi87gbjh7m")))

(define-public crate-kresp-0.1.1 (c (n "kresp") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bstr") (r "^0.2.17") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0d6xpwa4j089c0ligqz41g2lbll261x6wm85nafy5kk8807zx1hd")))

(define-public crate-kresp-0.1.2 (c (n "kresp") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bstr") (r "^0.2.17") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1l9djal0jg00ncjasfwys2mbc38v8f0ngb5pdqhh9lkhdf55v0bl")))

