(define-module (crates-io kr ep krep) #:use-module (crates-io))

(define-public crate-krep-0.1.0 (c (n "krep") (v "0.1.0") (d (list (d (n "doe") (r "^0.1.32") (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 0)))) (h "146wvdhf1b3xph7icjch4zm29pb91jpi6i8ywbfzhhpx1g8p4p1y") (y #t)))

(define-public crate-krep-0.1.1 (c (n "krep") (v "0.1.1") (d (list (d (n "doe") (r "^0.1.32") (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 0)))) (h "1m0pyims0y0bc3p775vbrryy4hhjaiwi6im5pxfldxhgajpkg5vh")))

(define-public crate-krep-1.0.0 (c (n "krep") (v "1.0.0") (d (list (d (n "doe") (r "^0.1.32") (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 0)))) (h "1wfxzz4q9jwhg96jc9nr5ha9q88pgpl4016yv64nvlq7b1m8apmn") (y #t)))

(define-public crate-krep-2.0.0 (c (n "krep") (v "2.0.0") (d (list (d (n "doe") (r "^0.1.32") (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 0)))) (h "13d69hma7jc7mwna5yfzqblf6l05amgn0vql6cdbpmvp2sxc9c1g") (y #t)))

(define-public crate-krep-3.0.0 (c (n "krep") (v "3.0.0") (d (list (d (n "doe") (r "^0.1.32") (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 0)))) (h "1ff6raibs0dkw4bcqqdmi2nvq1gr6iiqfbi8l6fyh49rhnk5i2g5") (y #t)))

