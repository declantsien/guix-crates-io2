(define-module (crates-io kr am kramer) #:use-module (crates-io))

(define-public crate-kramer-0.1.0 (c (n "kramer") (v "0.1.0") (d (list (d (n "async-std") (r "^0.99") (o #t) (d #t) (k 0)))) (h "1vs7wddci4ya1fqsnmnrnsrj332q7v6z36symq7wd0425d2fswng") (f (quote (("kramer-io" "async-std"))))))

(define-public crate-kramer-0.2.0 (c (n "kramer") (v "0.2.0") (d (list (d (n "async-std") (r "^0.99") (o #t) (d #t) (k 0)))) (h "0ah1ni2dy51zh4bx7sdkxpa09ngkflmww1varcmgjsnwis3i7p1c") (f (quote (("kramer-async" "async-std"))))))

(define-public crate-kramer-0.2.1 (c (n "kramer") (v "0.2.1") (d (list (d (n "async-std") (r "^0.99") (o #t) (d #t) (k 0)))) (h "1nvn6zrhi4fhqzl7lci921xqsh4s7a63n7gm77ljhj2l7fb72qmi") (f (quote (("kramer-async" "async-std"))))))

(define-public crate-kramer-0.3.0 (c (n "kramer") (v "0.3.0") (d (list (d (n "async-std") (r "^0.99") (o #t) (d #t) (k 0)))) (h "1s3sals2nvpp51rqpp8qr3jn8ydhbfg9qpyc52nj52rd327pmfyw") (f (quote (("kramer-async" "async-std"))))))

(define-public crate-kramer-1.0.0 (c (n "kramer") (v "1.0.0") (d (list (d (n "async-std") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1vbw8mzpbb5iyv8fdqxzzvkgapy5hd5sbjlc8c9bh75zahp5k41v") (f (quote (("kramer-async" "async-std"))))))

(define-public crate-kramer-1.0.1 (c (n "kramer") (v "1.0.1") (d (list (d (n "async-std") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0gd7xb62w05zd7kckwxmrh3ggdcvggj5ijzd4kzmz6vslf87jnmk") (f (quote (("kramer-async" "async-std"))))))

(define-public crate-kramer-1.1.0 (c (n "kramer") (v "1.1.0") (d (list (d (n "async-std") (r "^1.0") (o #t) (d #t) (k 0)))) (h "053r87spm9way30d2s83wz8x7swfqmgs7lad78q769j8vqqb4s58") (f (quote (("kramer-async" "async-std"))))))

(define-public crate-kramer-1.2.0 (c (n "kramer") (v "1.2.0") (d (list (d (n "async-std") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0zfc8c9zz1myicxxcqcrh8sz81m69515159yhw9zhinnd5ch9sqk") (f (quote (("kramer-async" "async-std") ("acl"))))))

(define-public crate-kramer-1.3.0 (c (n "kramer") (v "1.3.0") (d (list (d (n "async-std") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0i4dmp9ddqh37ysnp38a9jg44l9gpn3cj6vf1ja3d3p897jccrmn") (f (quote (("kramer-async" "async-std") ("acl"))))))

(define-public crate-kramer-1.3.1 (c (n "kramer") (v "1.3.1") (d (list (d (n "async-std") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1jj1jzp3hp99cwflawwakf1hbqwz4zc272j8ilx9wln6811fan1i") (f (quote (("kramer-async" "async-std") ("acl"))))))

(define-public crate-kramer-1.3.2 (c (n "kramer") (v "1.3.2") (d (list (d (n "async-std") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1jhfjyzjlmnqwxnvq624yzy90db9hxx9ip4rpaknalc6v47nwpmm") (f (quote (("kramer-async" "async-std") ("acl"))))))

(define-public crate-kramer-2.0.0 (c (n "kramer") (v "2.0.0") (d (list (d (n "async-std") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1gflzm3rws451m9mjx575a44g62vjdd7isw4krkg7qnrh782kn24") (f (quote (("kramer-async" "async-std") ("acl"))))))

(define-public crate-kramer-2.0.1 (c (n "kramer") (v "2.0.1") (d (list (d (n "async-std") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0yrl7xq8m3q1fv9dvx607pk9h4b5qv5pqycdhxlm8ifyds7wnvrz") (f (quote (("kramer-async" "async-std") ("acl"))))))

(define-public crate-kramer-2.1.0 (c (n "kramer") (v "2.1.0") (d (list (d (n "async-std") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1849778wxa6wcdj8f672dnjfs1fi0p5g71iysbi7wdkv0rxap86v") (f (quote (("kramer-async" "async-std") ("acl"))))))

(define-public crate-kramer-3.0.0 (c (n "kramer") (v "3.0.0") (d (list (d (n "async-std") (r "^1.0") (o #t) (d #t) (k 0)))) (h "082wvcg2n1mlzfqxfw0njy2zrvy23d9nbxina7wz1hq4jmxrf8cl") (f (quote (("kramer-async-read" "kramer-async") ("kramer-async" "async-std") ("acl"))))))

