(define-module (crates-io kr il krile) #:use-module (crates-io))

(define-public crate-krile-0.1.0 (c (n "krile") (v "0.1.0") (d (list (d (n "argh") (r "~0.1") (d #t) (k 0)) (d (n "minifemme") (r "~1.0") (d #t) (k 0)) (d (n "shtola") (r "^0.3.0-alpha1") (f (quote ("full"))) (d #t) (k 0)))) (h "0imbj2w7rsx20431v9wzi15kcp6jawh251cn4zg4xbi6hhgq69p6")))

(define-public crate-krile-0.1.1 (c (n "krile") (v "0.1.1") (d (list (d (n "argh") (r "~0.1") (d #t) (k 0)) (d (n "minifemme") (r "~1.0") (d #t) (k 0)) (d (n "shtola") (r "^0.3.0-alpha1") (f (quote ("full"))) (d #t) (k 0)))) (h "1fqbwszhal7v33pa6d3g5nj5d34gal8wdr3yy9rks0sppaxn7cjy")))

(define-public crate-krile-0.2.0 (c (n "krile") (v "0.2.0") (d (list (d (n "argh") (r "~0.1") (d #t) (k 0)) (d (n "minifemme") (r "~1.0") (d #t) (k 0)) (d (n "shtola") (r "^0.3.0-alpha1") (f (quote ("full"))) (d #t) (k 0)))) (h "1yn82if1fch0p30k10aq763x7fnw2ycj3prf5qi2821gcijipzl4")))

(define-public crate-krile-0.3.0 (c (n "krile") (v "0.3.0") (d (list (d (n "argh") (r "~0.1") (d #t) (k 0)) (d (n "minifemme") (r "~1.0") (d #t) (k 0)) (d (n "shtola") (r "^0.3.0-alpha2") (f (quote ("full"))) (d #t) (k 0)))) (h "1klpq0wh20aypa58djw0m1ilwf7jc6mb6njnss3639pcsn8hnp3z")))

(define-public crate-krile-0.3.1 (c (n "krile") (v "0.3.1") (d (list (d (n "argh") (r "~0.1") (d #t) (k 0)) (d (n "minifemme") (r "~1.0") (d #t) (k 0)) (d (n "shtola") (r "^0.3.0-alpha3") (f (quote ("full"))) (d #t) (k 0)))) (h "0rg1hkf1grcg7rvskbg1w58a1xd38v9vrl6zza5cg4kvms3zkg5g")))

(define-public crate-krile-0.4.0 (c (n "krile") (v "0.4.0") (d (list (d (n "argh") (r "~0.1") (d #t) (k 0)) (d (n "minifemme") (r "~1.0") (d #t) (k 0)) (d (n "shtola") (r "^0.3.0-alpha3") (f (quote ("full"))) (d #t) (k 0)))) (h "170c8zv4bla2m8j9blyhl360d72ip9y5525apwhb49sh6nzxmyi2")))

(define-public crate-krile-0.4.1 (c (n "krile") (v "0.4.1") (d (list (d (n "argh") (r "~0.1") (d #t) (k 0)) (d (n "minifemme") (r "~1.0") (d #t) (k 0)) (d (n "shtola") (r "^0.3.0-alpha4") (f (quote ("full"))) (d #t) (k 0)))) (h "0lmphrqid173hv9i6d84cpph513v95dg273sjjm2w086w4fshpy0")))

(define-public crate-krile-0.5.0 (c (n "krile") (v "0.5.0") (d (list (d (n "argh") (r "~0.1") (d #t) (k 0)) (d (n "minifemme") (r "~1.0") (d #t) (k 0)) (d (n "shtola") (r "^0.3.0-alpha5") (f (quote ("full"))) (d #t) (k 0)))) (h "0g74hlx6rzc68q7szy3nqlgpc8znaynpqji60mry7afi5790afb8")))

(define-public crate-krile-0.5.1 (c (n "krile") (v "0.5.1") (d (list (d (n "argh") (r "~0.1") (d #t) (k 0)) (d (n "minifemme") (r "~1.0") (d #t) (k 0)) (d (n "shtola") (r "^0.3.0-alpha6") (f (quote ("full"))) (d #t) (k 0)))) (h "1hxiw0mdbv9w9c238qac9m9qgzfkkrm4jv0q42i9dzfdka138b19")))

(define-public crate-krile-0.5.2 (c (n "krile") (v "0.5.2") (d (list (d (n "argh") (r "~0.1") (d #t) (k 0)) (d (n "minifemme") (r "~1.0") (d #t) (k 0)) (d (n "shtola") (r "^0.3.0-alpha6") (f (quote ("full"))) (d #t) (k 0)))) (h "12dynxi56crpkwm0js0xaisklyn60jl0dihnygnbl0dg8z9ib7i5")))

(define-public crate-krile-0.5.3 (c (n "krile") (v "0.5.3") (d (list (d (n "argh") (r "~0.1") (d #t) (k 0)) (d (n "minifemme") (r "~1.0") (d #t) (k 0)) (d (n "shtola") (r "^0.3.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1axyw5fxx4p4apirwvmyc5136piij45v70jb3s6y5dam1219clz2")))

(define-public crate-krile-0.5.4 (c (n "krile") (v "0.5.4") (d (list (d (n "argh") (r "~0.1") (d #t) (k 0)) (d (n "minifemme") (r "~1.0") (d #t) (k 0)) (d (n "shtola") (r "^0.4.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1n7xnz2hpddhnj82adxks7hipffl4y4y938bj363pvpmbg9pph8k")))

(define-public crate-krile-0.5.5 (c (n "krile") (v "0.5.5") (d (list (d (n "argh") (r "~0.1") (d #t) (k 0)) (d (n "minifemme") (r "~1.0") (d #t) (k 0)) (d (n "shtola") (r "^0.4.1") (f (quote ("full"))) (d #t) (k 0)))) (h "1sywpqb0zh5xdlbbp0nis8zw9sli4zbjw767xs65avlg2zwdz15h")))

(define-public crate-krile-0.5.6 (c (n "krile") (v "0.5.6") (d (list (d (n "argh") (r "~0.1") (d #t) (k 0)) (d (n "minifemme") (r "~1.0") (d #t) (k 0)) (d (n "shtola") (r "^0.4.2") (f (quote ("full"))) (d #t) (k 0)))) (h "1h601xpdhl2nprxb6n2xq1jjrwiav3r3pgg7s4v45lyw07z0c33z")))

(define-public crate-krile-0.5.7 (c (n "krile") (v "0.5.7") (d (list (d (n "argh") (r "~0.1") (d #t) (k 0)) (d (n "minifemme") (r "~1.0") (d #t) (k 0)) (d (n "shtola") (r "^0.4.3") (f (quote ("full"))) (d #t) (k 0)))) (h "1blzyb5pz9g3bl2wd7yklpzadhrb8c29i63mwqb52ybk4jga8wj4")))

