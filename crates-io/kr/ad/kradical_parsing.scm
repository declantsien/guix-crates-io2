(define-module (crates-io kr ad kradical_parsing) #:use-module (crates-io))

(define-public crate-kradical_parsing-0.1.0 (c (n "kradical_parsing") (v "0.1.0") (d (list (d (n "encoding") (r "^0") (d #t) (k 0)) (d (n "kradical_jis") (r "^0.1.0") (d #t) (k 0)) (d (n "nom") (r "^6") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1") (d #t) (k 0)))) (h "11q0dcix2817m4y5iinpiz2h0sp54ii0bxd09fp8yj0sayz2vrnp")))

