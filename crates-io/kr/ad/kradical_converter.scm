(define-module (crates-io kr ad kradical_converter) #:use-module (crates-io))

(define-public crate-kradical_converter-0.1.0 (c (n "kradical_converter") (v "0.1.0") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "kradical_parsing") (r "^0.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "02wn98a0r86mr1hmgk2d9q4b7w9cps105chq9116ihn46c2iyzx4")))

