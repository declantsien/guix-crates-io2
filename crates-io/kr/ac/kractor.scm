(define-module (crates-io kr ac kractor) #:use-module (crates-io))

(define-public crate-kractor-0.4.0 (c (n "kractor") (v "0.4.0") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "clap") (r "^4.3.19") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossbeam") (r "^0.8.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "niffler") (r "^2.5.0") (f (quote ("gz" "bz2" "gz_cloudflare_zlib"))) (k 0)) (d (n "noodles") (r "^0.51.0") (f (quote ("fastq" "fasta"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "18nngbcn7gmp4p3fn7qpcnfgw6vpc9b08v1ssqfdh3jn8vsmqb3z")))

