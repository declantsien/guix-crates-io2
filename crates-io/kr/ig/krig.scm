(define-module (crates-io kr ig krig) #:use-module (crates-io))

(define-public crate-krig-0.0.0 (c (n "krig") (v "0.0.0") (d (list (d (n "cfg_eval") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (o #t) (d #t) (k 0)) (d (n "serde_with") (r "^3.8") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "zerocopy") (r "^0.7") (d #t) (k 0)) (d (n "zerocopy-derive") (r "^0.7") (d #t) (k 0)) (d (n "zeroize") (r "^1.7") (o #t) (d #t) (k 0)))) (h "0fv94cxfxi64ih7fkb8kc16wfk6sgqxs69l7cr5hs24x0l5wac6f") (f (quote (("default" "serde" "zeroize")))) (s 2) (e (quote (("zeroize" "dep:zeroize") ("serde" "dep:cfg_eval" "dep:serde" "dep:serde_with")))) (r "1.70")))

