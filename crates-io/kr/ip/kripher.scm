(define-module (crates-io kr ip kripher) #:use-module (crates-io))

(define-public crate-kripher-0.4.2 (c (n "kripher") (v "0.4.2") (h "0i2id1mvr7vbcashqz1zbqhii5r2x99zbgz9qg03wx527my4njl1")))

(define-public crate-kripher-0.6.0 (c (n "kripher") (v "0.6.0") (h "0kkzkn507bx8k7mxbqp2lrkw0ahda2qs3gxi5mbnjxppg3ggcj8a")))

(define-public crate-kripher-0.6.1 (c (n "kripher") (v "0.6.1") (h "0yq7s9jshwhdji7xbdn769pfpkjf9c7jgdwvzznx7d40ka56p4wa")))

(define-public crate-kripher-0.8.0 (c (n "kripher") (v "0.8.0") (h "13mjlxbq7fx5cxn2v2vw511xf26ck82kzj4z33s1imj4ssyh7fh7")))

