(define-module (crates-io kr ak kraken_rust_api_client) #:use-module (crates-io))

(define-public crate-kraken_rust_api_client-0.1.0 (c (n "kraken_rust_api_client") (v "0.1.0") (d (list (d (n "base64") (r "^0.9.2") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "hmac") (r "^0.5") (d #t) (k 0)) (d (n "hyper") (r "^0.10.13") (d #t) (k 0)) (d (n "rand") (r "^0.4.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.6.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.9") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.2") (d #t) (k 0)) (d (n "sha2") (r "^0.7") (d #t) (k 0)))) (h "0ifdk3sn8v7rzyxxkhkhhgl910hh31hjpj0h1cbwd7yqdmgznd30")))

