(define-module (crates-io kr ak krakel) #:use-module (crates-io))

(define-public crate-krakel-0.1.0 (c (n "krakel") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)) (d (n "vector-traits") (r "^0.1.0") (f (quote ("approx"))) (d #t) (k 0)))) (h "0nh40zj5qac2qv63dxj0d50a59rdg7dwj62j2pzhz1kvn01wzgxy") (f (quote (("glam" "vector-traits/glam"))))))

(define-public crate-krakel-0.2.0 (c (n "krakel") (v "0.2.0") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 0)) (d (n "glam") (r "^0.24.2") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "17w8rzxcwdz4darxwzrmh1xl5x32ka52rrmbk25s7s7y91si9w1m") (s 2) (e (quote (("glam" "dep:glam"))))))

(define-public crate-krakel-0.2.1 (c (n "krakel") (v "0.2.1") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)) (d (n "vector-traits") (r ">=0.3.2, <1.0.0") (o #t) (d #t) (k 0)))) (h "00ghl6fj70rwxgnw6cz2pjya6nwwlpa5x3fl2mdz4ryk17czhra8") (f (quote (("glam" "vector-traits/glam") ("cgmath" "vector-traits/cgmath")))) (s 2) (e (quote (("vector-traits" "dep:vector-traits"))))))

(define-public crate-krakel-0.2.2 (c (n "krakel") (v "0.2.2") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)) (d (n "vector-traits") (r ">=0.3.2, <1.0.0") (o #t) (d #t) (k 0)))) (h "150h85lalddck29pg6m6wc1rwygzg00b65h78pabykm31iifcv5x") (f (quote (("glam" "vector-traits/glam") ("cgmath" "vector-traits/cgmath")))) (s 2) (e (quote (("vector-traits" "dep:vector-traits"))))))

(define-public crate-krakel-0.2.3 (c (n "krakel") (v "0.2.3") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)) (d (n "vector-traits") (r ">=0.3.2, <1.0.0") (o #t) (d #t) (k 0)))) (h "1i0vlya2ln6n3sbls4y7yjljy9frybx9rdgwz22dx18ycyrq74d6") (f (quote (("glam" "vector-traits/glam") ("cgmath" "vector-traits/cgmath")))) (s 2) (e (quote (("vector-traits" "dep:vector-traits"))))))

