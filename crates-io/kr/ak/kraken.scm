(define-module (crates-io kr ak kraken) #:use-module (crates-io))

(define-public crate-kraken-0.0.1 (c (n "kraken") (v "0.0.1") (d (list (d (n "config") (r "^0.6") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "hyper") (r "^0.11") (d #t) (k 0)) (d (n "lettre") (r "^0.7") (d #t) (k 0)))) (h "1gvinnf9a3rsm63r1761bi1vhrjgp8hlmjhc52hnigqszxjzbvi2")))

