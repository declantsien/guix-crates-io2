(define-module (crates-io kr ak kraken-btc) #:use-module (crates-io))

(define-public crate-kraken-btc-0.1.0 (c (n "kraken-btc") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.35") (d #t) (k 0)) (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "console") (r "^0.13.0") (d #t) (k 0)) (d (n "ring") (r "^0.16.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.20") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)) (d (n "ureq") (r "^1.5.2") (f (quote ("json"))) (d #t) (k 0)))) (h "1dcp5lxkj8wrmgh5l2ks7c3337iazypvzgf1mrxifjhy8rrkky5m")))

