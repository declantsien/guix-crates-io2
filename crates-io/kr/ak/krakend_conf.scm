(define-module (crates-io kr ak krakend_conf) #:use-module (crates-io))

(define-public crate-krakend_conf-0.1.0 (c (n "krakend_conf") (v "0.1.0") (d (list (d (n "openapiv3") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.99") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.9") (d #t) (k 0)))) (h "02zr2pg7gzlba5apilzq98ragq1pa0ci31n5w7bqp8rz812xsqxg")))

