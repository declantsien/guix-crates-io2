(define-module (crates-io kr ak kraken-animation) #:use-module (crates-io))

(define-public crate-kraken-animation-0.1.0 (c (n "kraken-animation") (v "0.1.0") (h "0ych2incl1rlxmmxadx1bm3k3rycwq3pl6r7hq6rfhkpy2z3p1gg")))

(define-public crate-kraken-animation-0.1.1 (c (n "kraken-animation") (v "0.1.1") (h "05mhd7hsind0cxlhsbphbsz5gjlyh1k6h20w9ps3rmv0p74pk9cl")))

(define-public crate-kraken-animation-0.1.2 (c (n "kraken-animation") (v "0.1.2") (d (list (d (n "futures") (r "^0.3.5") (d #t) (k 0)) (d (n "windows") (r "^0.30.0") (f (quote ("alloc" "build" "ApplicationModel_Activation" "ApplicationModel_Core" "Graphics" "System" "UI" "UI_Core" "UI_Composition_Desktop" "UI_Composition" "UI_WindowManagement_Preview" "UI_Xaml_Controls" "UI_Xaml" "Foundation" "Foundation_Collections" "Foundation_Numerics" "Win32_System_Com"))) (d #t) (k 0)))) (h "0qq8j17aydv826fv69pkn81s76dmbv64ysca4isa1d5mqlx71d1z")))

