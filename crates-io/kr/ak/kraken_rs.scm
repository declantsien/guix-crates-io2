(define-module (crates-io kr ak kraken_rs) #:use-module (crates-io))

(define-public crate-kraken_rs-0.1.0 (c (n "kraken_rs") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "hidapi") (r "^1.2.1") (d #t) (k 0)))) (h "05j7saii7pk0i290g3dkcb3mwgpby3kqkm30523ak763bxpk9swd")))

(define-public crate-kraken_rs-0.2.0 (c (n "kraken_rs") (v "0.2.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "hidapi") (r "^1.2.1") (d #t) (k 0)))) (h "1divzpjhsivi52kvamh2jd9g6qdbcj0i5z705dvbk0gds7kjzq9s")))

