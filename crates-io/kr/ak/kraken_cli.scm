(define-module (crates-io kr ak kraken_cli) #:use-module (crates-io))

(define-public crate-kraken_cli-0.22.0 (c (n "kraken_cli") (v "0.22.0") (h "149qadc7qmwna0hdir8h4nbbgw5lgmi798i26b5cq92wdzjcdb36")))

(define-public crate-kraken_cli-0.23.0 (c (n "kraken_cli") (v "0.23.0") (h "019g827q4fkyrx7pfi05j0sprf4f9j2cz3kzfd9zrangwyy6nimb")))

(define-public crate-kraken_cli-0.24.0 (c (n "kraken_cli") (v "0.24.0") (h "1qndvc1lkx2xkfw4sqmd3gdx56xsk53b68vbhfhnww44bh3j6gwl")))

(define-public crate-kraken_cli-0.25.0 (c (n "kraken_cli") (v "0.25.0") (h "0f4h7y97acj0k286yr7f8204gcyjy6qi1r1b97sr8wcxlfsr26ka")))

