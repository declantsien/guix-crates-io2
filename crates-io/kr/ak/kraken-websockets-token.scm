(define-module (crates-io kr ak kraken-websockets-token) #:use-module (crates-io))

(define-public crate-kraken-websockets-token-0.1.8 (c (n "kraken-websockets-token") (v "0.1.8") (d (list (d (n "base64") (r "^0.11.0") (d #t) (k 0)) (d (n "die") (r "^0.2.0") (d #t) (k 0)) (d (n "hmac") (r "^0.7.1") (d #t) (k 0)) (d (n "logfast") (r "^0.1.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.24") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.45") (d #t) (k 0)) (d (n "sha2") (r "^0.8.0") (d #t) (k 0)) (d (n "ws") (r "^0.9.1") (f (quote ("ssl"))) (d #t) (k 0)))) (h "1i4qxfips9qi66vnmlxb577czwanwz9v9gq33074w2ws3m2kdmgl")))

(define-public crate-kraken-websockets-token-0.1.9 (c (n "kraken-websockets-token") (v "0.1.9") (d (list (d (n "base64") (r "^0.11.0") (d #t) (k 0)) (d (n "hmac") (r "^0.7.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.24") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.45") (d #t) (k 0)) (d (n "sha2") (r "^0.8.0") (d #t) (k 0)))) (h "0kmc3fnvbc20cfc5ih1b7dgac0axa9j30sr9rgxxk7yp1yl4z01x")))

(define-public crate-kraken-websockets-token-0.2.1 (c (n "kraken-websockets-token") (v "0.2.1") (d (list (d (n "base64") (r "^0.11.0") (d #t) (k 0)) (d (n "hmac") (r "^0.7.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.24") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.45") (d #t) (k 0)) (d (n "sha2") (r "^0.8.0") (d #t) (k 0)))) (h "18fgqd2iakvy9b0nihj8w9q2rn040h5hadz4wdish7b6xg4hcmr0")))

(define-public crate-kraken-websockets-token-0.2.2 (c (n "kraken-websockets-token") (v "0.2.2") (d (list (d (n "base64") (r "^0.11.0") (d #t) (k 0)) (d (n "hmac") (r "^0.7.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.24") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.45") (d #t) (k 0)) (d (n "sha2") (r "^0.8.0") (d #t) (k 0)))) (h "029s4dcscgrjb2ym4srpj90sl7kbxn7a4hsxcc75vmsb2an011f5")))

(define-public crate-kraken-websockets-token-1.2.3 (c (n "kraken-websockets-token") (v "1.2.3") (d (list (d (n "base64") (r "^0.11.0") (d #t) (k 0)) (d (n "hmac") (r "^0.7.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.24") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.45") (d #t) (k 0)) (d (n "sha2") (r "^0.8.0") (d #t) (k 0)))) (h "01biy7vq8qnv13v4m7srfp2m8h6jsmjcd29k48w01hrd59a5xii6")))

(define-public crate-kraken-websockets-token-1.2.4 (c (n "kraken-websockets-token") (v "1.2.4") (d (list (d (n "base64") (r "=0.13.0") (d #t) (k 0)) (d (n "hmac") (r "=0.7.1") (d #t) (k 0)) (d (n "reqwest") (r "=0.9.24") (d #t) (k 0)) (d (n "serde_json") (r "=1.0.73") (d #t) (k 0)) (d (n "sha2") (r "=0.8.0") (d #t) (k 0)))) (h "08yryh9jakxgmvjmms96g2rfx3pkjwrxjwq21x87xmdicx85gzdx")))

(define-public crate-kraken-websockets-token-1.2.5 (c (n "kraken-websockets-token") (v "1.2.5") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "hmac") (r "^0.7.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.24") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.73") (d #t) (k 0)) (d (n "sha2") (r "^0.8.0") (d #t) (k 0)))) (h "1vpkfnr84isv5a6p3bimmy4rhixrv7136d1fbphapmyv0mfvms0l")))

(define-public crate-kraken-websockets-token-1.2.6 (c (n "kraken-websockets-token") (v "1.2.6") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "hmac") (r "^0.7.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.24") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.73") (d #t) (k 0)) (d (n "sha2") (r "^0.8.0") (d #t) (k 0)))) (h "0fixhybpwgfrm192vv8jd2ldd6sdhfvayvpwhzmi4lmy0zi1qngz")))

(define-public crate-kraken-websockets-token-1.2.7 (c (n "kraken-websockets-token") (v "1.2.7") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "hmac") (r "^0.7.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.24") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.73") (d #t) (k 0)) (d (n "sha2") (r "^0.8.0") (d #t) (k 0)))) (h "066n1rvh7ql4jyk2spy8mphkc72wq2gs6acwjy292z0p0wrxw50a")))

