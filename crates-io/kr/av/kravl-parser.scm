(define-module (crates-io kr av kravl-parser) #:use-module (crates-io))

(define-public crate-kravl-parser-0.1.0 (c (n "kravl-parser") (v "0.1.0") (h "1mplffnxzr05cvdinm6iarz7fhpwkmi3288wmv18aflbyqbrzf6k")))

(define-public crate-kravl-parser-0.1.1 (c (n "kravl-parser") (v "0.1.1") (h "1p9prynmx7y15g4ficrb3z68py4qaw86j68slzb5ja23zrs2xls8")))

(define-public crate-kravl-parser-0.1.2 (c (n "kravl-parser") (v "0.1.2") (h "0m09x71pff0lyzribbmi4rqzd3m6nq5642bm5363rm36g9a4n6zq")))

(define-public crate-kravl-parser-0.1.3 (c (n "kravl-parser") (v "0.1.3") (h "0kp6r6669ndwghrr2aid6gs2632llfid97lndnihwqhx6gg13xaa")))

(define-public crate-kravl-parser-0.1.4 (c (n "kravl-parser") (v "0.1.4") (h "091adhs5i807jwvjgz38rxryn4nm0d4i0yii13h558f479z6s2n5")))

(define-public crate-kravl-parser-0.1.5 (c (n "kravl-parser") (v "0.1.5") (h "1sy434k9rj7lv288ilvgw7x8psjinm46bjfzkgmm1qj6f12bzwpf")))

(define-public crate-kravl-parser-0.1.6 (c (n "kravl-parser") (v "0.1.6") (h "0fpqw7rjm8q5ykv73iplk9ymzh7vpy6wr4vhcfwz3c233s51r46d")))

(define-public crate-kravl-parser-0.1.7 (c (n "kravl-parser") (v "0.1.7") (h "1g3idlw5hk2y54bhq180bzpp2szkmrm1q0xp5qws2nrwv3zdr9pf")))

(define-public crate-kravl-parser-0.1.8 (c (n "kravl-parser") (v "0.1.8") (h "1v29hz24zm88f7krki1fff6g01cm3hmra8lkw61d84qx5xs56bd0")))

(define-public crate-kravl-parser-0.1.9 (c (n "kravl-parser") (v "0.1.9") (h "1lbmz8zvng5yvnbjgnnn4zmcgr3k0zgk53mcyqxa5x7qizmy3p0m")))

(define-public crate-kravl-parser-0.2.0 (c (n "kravl-parser") (v "0.2.0") (h "0xq9hliny9znnrd0pih3jj7j7wslpward331gapflmydk5wdqs2s")))

(define-public crate-kravl-parser-0.2.1 (c (n "kravl-parser") (v "0.2.1") (h "0vzvhjwhmi1kwy0fqngrkd6migfilg7cjmfwc33df75qrvqzxvji")))

(define-public crate-kravl-parser-0.2.2 (c (n "kravl-parser") (v "0.2.2") (h "11f022nnyyzh7gdnd0kaskzrk0psjji66hvn38vx67cbg3xsk2c3")))

(define-public crate-kravl-parser-0.2.3 (c (n "kravl-parser") (v "0.2.3") (h "1gi2wil9gfrcs6jnv0v1gk4pp8qxprambm5bjp5mrcnv72j4vwja")))

(define-public crate-kravl-parser-0.2.4 (c (n "kravl-parser") (v "0.2.4") (h "0qkisq3syzczspcz1rscv8mzciqsd2yda0w5plhk4xs15zqbwmcq")))

(define-public crate-kravl-parser-0.2.5 (c (n "kravl-parser") (v "0.2.5") (h "1b0y98lyg3ql8x00hwz86b04fyyyi3r39yva46dzdqr2whz3q4zs")))

(define-public crate-kravl-parser-0.2.6 (c (n "kravl-parser") (v "0.2.6") (h "1xyy197x17iqlcivfna8lj7dpd51z8zpgh2mxybwx13by18vshbx")))

(define-public crate-kravl-parser-0.2.7 (c (n "kravl-parser") (v "0.2.7") (h "1d5zfd82fm5m0234f5jzbzwvgcd62spnshx2jdgsiqqifiimh31w")))

(define-public crate-kravl-parser-0.2.9 (c (n "kravl-parser") (v "0.2.9") (h "0a2vnc4zasc44r41znq52wb33w7ifqzgh4d6w7vns4d0jlj40l56")))

(define-public crate-kravl-parser-0.3.0 (c (n "kravl-parser") (v "0.3.0") (h "1nzjvqr3l5aiam7xxpb21qpjl6yfa6zmzxh7zfqmawqiiqrw3dsk")))

(define-public crate-kravl-parser-0.3.1 (c (n "kravl-parser") (v "0.3.1") (h "161sb4nwbp7kqkpcwf2cx9qy25dlk17ymn0lw0jkl4c947ms4aw7")))

(define-public crate-kravl-parser-0.3.2 (c (n "kravl-parser") (v "0.3.2") (h "165mbxqi6cvl7jwbysk71dmc7wwl9mnxmz5av5rs4hxqrrvks9al")))

(define-public crate-kravl-parser-0.3.3 (c (n "kravl-parser") (v "0.3.3") (h "108080y91skc8dfpzlll30808armxshr0d7g1chw2387a4v17h9x")))

