(define-module (crates-io s3 -t s3-types) #:use-module (crates-io))

(define-public crate-s3-types-0.1.0 (c (n "s3-types") (v "0.1.0") (d (list (d (n "conv") (r "^0.3.3") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.1") (d #t) (k 0)))) (h "14g7r594w9nm96lc0xkw5nm6p0k53ddjgmywwk0zrljkgbz9m6s4") (y #t)))

(define-public crate-s3-types-1.0.0 (c (n "s3-types") (v "1.0.0") (d (list (d (n "conv") (r "^0.3.3") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.1") (d #t) (k 0)))) (h "0hh591gvr24wmyaiza4y86kv03f20vbwnvsxn5lpyc7r12vkaza4") (y #t)))

