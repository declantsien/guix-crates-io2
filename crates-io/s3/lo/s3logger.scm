(define-module (crates-io s3 lo s3logger) #:use-module (crates-io))

(define-public crate-s3logger-0.1.0 (c (n "s3logger") (v "0.1.0") (d (list (d (n "rust-s3") (r "^0.32.0") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "tokio-test") (r "^0.4.2") (d #t) (k 2)))) (h "1rf7rwjjwx3j2m9wki6g5dcv5fjjkzzq25ax8zsb4mrj6y0hkw8b")))

(define-public crate-s3logger-0.1.1 (c (n "s3logger") (v "0.1.1") (d (list (d (n "rust-s3") (r "^0.32.0") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "tokio-test") (r "^0.4.2") (d #t) (k 2)))) (h "0rlhh221pz658h0lynq3i0mny0rbcxf6cik3p2sr5y6rnzga7xg0")))

(define-public crate-s3logger-0.2.0 (c (n "s3logger") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.22") (d #t) (k 0)) (d (n "rust-s3") (r "^0.32.0") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "tokio-test") (r "^0.4.2") (d #t) (k 2)))) (h "1wgyn9kcpp6zgy754rg4b622vamdvpzwslk9b3192jfkv97y3spk")))

