(define-module (crates-io s3 -e s3-extract) #:use-module (crates-io))

(define-public crate-s3-extract-0.1.0 (c (n "s3-extract") (v "0.1.0") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "s3-types") (r "^0.1") (d #t) (k 0)))) (h "17wyyyyhg1h2lhvdagg373v6k8rg9v5hspkfvg7jfrcb90nisgz6") (y #t)))

(define-public crate-s3-extract-1.0.0 (c (n "s3-extract") (v "1.0.0") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "s3-types") (r "^1.0.0") (d #t) (k 0)))) (h "1p6bjgkmrjcsgvc9lphzyi5zbrica5jzijzfw29a734jyv4nvciw") (y #t)))

