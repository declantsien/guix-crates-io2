(define-module (crates-io s3 -m s3-manifest) #:use-module (crates-io))

(define-public crate-s3-manifest-0.1.0 (c (n "s3-manifest") (v "0.1.0") (d (list (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0hyyswmks86f0h4bmfhyc3ld9rcjqhj694knnx1837wadlnnpdwv") (f (quote (("inventory") ("default" "inventory"))))))

(define-public crate-s3-manifest-0.2.0 (c (n "s3-manifest") (v "0.2.0") (d (list (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "06hn64g25nj2xszfr713rd95a8c6y1gbav33jpmrjkd752xbd81a") (f (quote (("inventory") ("default" "inventory" "batch") ("batch"))))))

(define-public crate-s3-manifest-0.3.0 (c (n "s3-manifest") (v "0.3.0") (d (list (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1kdwknbfgljci53dl2wv81py97b59papwvzg41zv3cb03q612mpr") (f (quote (("lens") ("inventory") ("default" "inventory" "batch" "lens") ("batch"))))))

