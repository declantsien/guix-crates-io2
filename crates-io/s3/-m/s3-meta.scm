(define-module (crates-io s3 -m s3-meta) #:use-module (crates-io))

(define-public crate-s3-meta-1.0.0 (c (n "s3-meta") (v "1.0.0") (d (list (d (n "humantime") (r "^1.1") (d #t) (k 0)) (d (n "pretty-bytes") (r "^0.2") (d #t) (k 0)) (d (n "quick-xml") (r "^0.12") (d #t) (k 0)) (d (n "rusoto_core") (r "^0.32") (d #t) (k 0)) (d (n "rusoto_credential") (r "^0.11") (d #t) (k 0)) (d (n "rusoto_s3") (r "^0.32") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)))) (h "0v7zq3an8ma02ggwj0q5l2cy9zwwx049nkamnkac967g2mykakiw")))

(define-public crate-s3-meta-1.0.1 (c (n "s3-meta") (v "1.0.1") (d (list (d (n "humantime") (r "^1.1") (d #t) (k 0)) (d (n "pretty-bytes") (r "^0.2") (d #t) (k 0)) (d (n "quick-xml") (r "^0.12") (d #t) (k 0)) (d (n "rusoto_core") (r "^0.34") (d #t) (k 0)) (d (n "rusoto_s3") (r "^0.34") (d #t) (k 0)))) (h "09aj9wsbg8hva5mhx12wk4gd1wlkqgz0jfmjsivv3pz7f8fs0888")))

