(define-module (crates-io s3 re s3reader) #:use-module (crates-io))

(define-public crate-s3reader-0.1.0 (c (n "s3reader") (v "0.1.0") (d (list (d (n "aws-config") (r "^0.47.0") (d #t) (k 0)) (d (n "aws-sdk-s3") (r "^0.17.0") (d #t) (k 0)) (d (n "aws-types") (r "^0.47.0") (d #t) (k 0)) (d (n "bytes") (r "^1.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.32") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0hbyx9dcxg2n880l52fjxf0mq6w06cwvvyhhb8xy2zkgpm14as76")))

(define-public crate-s3reader-0.2.0 (c (n "s3reader") (v "0.2.0") (d (list (d (n "aws-config") (r "^0.47.0") (d #t) (k 0)) (d (n "aws-sdk-s3") (r "^0.17.0") (d #t) (k 0)) (d (n "aws-types") (r "^0.47.0") (d #t) (k 0)) (d (n "bytes") (r "^1.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.32") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1vv0iy5cas4g233jh23g7cx4frlb7fpqfy9f3zavnwmw5g40sxlq")))

(define-public crate-s3reader-0.3.0 (c (n "s3reader") (v "0.3.0") (d (list (d (n "aws-config") (r "^0.47.0") (d #t) (k 0)) (d (n "aws-sdk-s3") (r "^0.17.0") (d #t) (k 0)) (d (n "aws-types") (r "^0.47.0") (d #t) (k 0)) (d (n "bytes") (r "^1.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.32") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1rz9ghwr4w73gzm5rrr0scziz6mm5rh1fh222gg3a2l0d7pssv25")))

(define-public crate-s3reader-0.4.0 (c (n "s3reader") (v "0.4.0") (d (list (d (n "aws-config") (r "^0.47.0") (d #t) (k 0)) (d (n "aws-sdk-s3") (r "^0.17.0") (d #t) (k 0)) (d (n "aws-types") (r "^0.47.0") (d #t) (k 0)) (d (n "bytes") (r "^1.2.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "loggerv") (r "^0.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.32") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0gr92wzdj6aag46hpj0zbjy0jclls9ws4psdawy60av3zhpxggwm")))

(define-public crate-s3reader-1.0.0 (c (n "s3reader") (v "1.0.0") (d (list (d (n "aws-config") (r "^0.49.0") (d #t) (k 0)) (d (n "aws-sdk-s3") (r "^0.19.0") (d #t) (k 0)) (d (n "aws-types") (r "^0.49.0") (d #t) (k 0)) (d (n "bytes") (r "^1.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0ilpw42xigv7cw7ma3f1izkihbvl5vr7crw1fd1c2w8r0yvwiadc")))

(define-public crate-s3reader-1.1.0 (c (n "s3reader") (v "1.1.0") (d (list (d (n "aws-config") (r "^1") (d #t) (k 0)) (d (n "aws-sdk-s3") (r "^1") (d #t) (k 0)) (d (n "aws-types") (r "^1") (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0744898xwhdpkp398ip9q8daj6h010xhl2zszh9micg4311y57xd")))

(define-public crate-s3reader-1.2.0 (c (n "s3reader") (v "1.2.0") (d (list (d (n "aws-config") (r "^1") (d #t) (k 0)) (d (n "aws-sdk-s3") (r "^1") (d #t) (k 0)) (d (n "aws-types") (r "^1") (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1n40a08mjd4448sv58m7fv5gmlbpi4klc62i7vgk3490ixvi9ifz")))

(define-public crate-s3reader-1.2.1 (c (n "s3reader") (v "1.2.1") (d (list (d (n "aws-config") (r "^1") (d #t) (k 0)) (d (n "aws-sdk-s3") (r "^1") (d #t) (k 0)) (d (n "aws-types") (r "^1") (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1qqvw80h30bd3jznyrmrbv5sg7k1vp9mhf5y30svfplii6zw9a3q")))

