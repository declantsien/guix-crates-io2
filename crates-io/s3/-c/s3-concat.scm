(define-module (crates-io s3 -c s3-concat) #:use-module (crates-io))

(define-public crate-s3-concat-1.0.0 (c (n "s3-concat") (v "1.0.0") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "quick-xml") (r "^0.12") (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 0)) (d (n "rusoto_core") (r "^0.35") (d #t) (k 0)) (d (n "rusoto_s3") (r "^0.35") (d #t) (k 0)))) (h "0w238pgj81ql1rsaji1viqdi0ay28s3nis4fj20fx1shin1j7hqr")))

(define-public crate-s3-concat-1.1.0 (c (n "s3-concat") (v "1.1.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "quick-xml") (r "^0.16") (d #t) (k 0)) (d (n "regex") (r "^1.2") (d #t) (k 0)) (d (n "rusoto_core") (r "^0.40") (d #t) (k 0)) (d (n "rusoto_s3") (r "^0.40") (d #t) (k 0)))) (h "1z1ybizgh0ky5xn2z92d15bhwkr27rv5xm7zg1yjspfddxwh3x3n")))

