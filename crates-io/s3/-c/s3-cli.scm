(define-module (crates-io s3 -c s3-cli) #:use-module (crates-io))

(define-public crate-s3-cli-0.1.0 (c (n "s3-cli") (v "0.1.0") (d (list (d (n "clap") (r "~2.29.0") (d #t) (k 0)) (d (n "conv") (r "^0.3.3") (d #t) (k 0)) (d (n "failure") (r "~0.1.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)) (d (n "s3-extract") (r "~0.1.0") (d #t) (k 0)) (d (n "s3-types") (r "~0.1.0") (d #t) (k 0)) (d (n "s3-vault") (r "~0.1.0") (d #t) (k 0)))) (h "0p3074z47pjzcb82z44034fn35536ch3ynhxpiags2wcz3v25iyx") (y #t)))

(define-public crate-s3-cli-1.0.0 (c (n "s3-cli") (v "1.0.0") (d (list (d (n "clap") (r "~2.29.0") (d #t) (k 0)) (d (n "conv") (r "^0.3.3") (d #t) (k 0)) (d (n "failure") (r "~0.1.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)) (d (n "s3-extract") (r "^1.0.0") (d #t) (k 0)) (d (n "s3-types") (r "^1.0.0") (d #t) (k 0)) (d (n "s3-vault") (r "^1.0.0") (d #t) (k 0)))) (h "01a3wkp5ifi7bq4307v4c0kc99mh500h8yp9xpwz3nzh64ny4ndy") (y #t)))

