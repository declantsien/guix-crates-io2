(define-module (crates-io s3 -b s3-batch-put-tar) #:use-module (crates-io))

(define-public crate-s3-batch-put-tar-0.1.0 (c (n "s3-batch-put-tar") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rusoto_core") (r "^0.45") (f (quote ("rustls"))) (k 0)) (d (n "rusoto_s3") (r "^0.45") (f (quote ("rustls"))) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)) (d (n "tempfile") (r "^3.1") (d #t) (k 0)) (d (n "tokio") (r "^0.2.5") (f (quote ("full"))) (d #t) (k 0)))) (h "1qp8rq65l3ccd7hzd1h39hdrjgzfpvcaxm8ziy5d2ipzp1schsya")))

