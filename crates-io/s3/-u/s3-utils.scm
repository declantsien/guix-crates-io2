(define-module (crates-io s3 -u s3-utils) #:use-module (crates-io))

(define-public crate-s3-utils-1.0.0 (c (n "s3-utils") (v "1.0.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "humantime") (r "^1.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "pretty-bytes") (r "^0.2") (d #t) (k 0)) (d (n "quick-xml") (r "^0.16") (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 0)) (d (n "rusoto_core") (r "^0.40") (d #t) (k 0)) (d (n "rusoto_s3") (r "^0.40") (d #t) (k 0)))) (h "0zdlrlj50kv24c0vkzhxzhwphk5kkad3x2d1s9yxi8qhhwsicyi3")))

(define-public crate-s3-utils-1.1.0 (c (n "s3-utils") (v "1.1.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "humantime") (r "^2.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "pretty-bytes") (r "^0.2") (d #t) (k 0)) (d (n "quick-xml") (r "^0.17") (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 0)) (d (n "rusoto_core") (r "^0.43.0-beta.1") (d #t) (k 0)) (d (n "rusoto_s3") (r "^0.43.0-beta.1") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (d #t) (k 0)))) (h "1h7kmgmijwhksr6sb5js56zf2x05rbz6bfzf2crxavqbg01y31cr")))

