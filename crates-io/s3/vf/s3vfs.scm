(define-module (crates-io s3 vf s3vfs) #:use-module (crates-io))

(define-public crate-s3vfs-0.0.0 (c (n "s3vfs") (v "0.0.0") (h "0rg7shhakl55acbbqvhz3l3p3ppsg7q2r8l6giw0rqd2nhq9480z")))

(define-public crate-s3vfs-0.1.0 (c (n "s3vfs") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.47") (d #t) (k 0)))) (h "0hir48kp12wbypx2n20ay41n5z5jljgnr6mwqrm9xwyj0mzg9g0m")))

(define-public crate-s3vfs-0.1.1 (c (n "s3vfs") (v "0.1.1") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.47") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("macros" "rt"))) (d #t) (k 0)))) (h "1qsgapqmaqm07vll3lqqb33skynyxqpsx0424mq7qh7ba2ardkfg")))

