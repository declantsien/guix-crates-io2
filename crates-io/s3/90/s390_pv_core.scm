(define-module (crates-io s3 #{90}# s390_pv_core) #:use-module (crates-io))

(define-public crate-s390_pv_core-0.1.0 (c (n "s390_pv_core") (v "0.1.0") (h "1m0baxgpisd867j2mnpjk637d15f16sskpw1dczvgb8b9sxjvxz6") (y #t)))

(define-public crate-s390_pv_core-0.10.0 (c (n "s390_pv_core") (v "0.10.0") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1") (d #t) (k 2)) (d (n "libc") (r "^0.2.49") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (f (quote ("std" "release_max_level_debug"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.139") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_test") (r "^1.0.139") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.33") (d #t) (k 0)) (d (n "zerocopy") (r "^0.7") (f (quote ("derive"))) (d #t) (k 0)))) (h "09s6cn25m3akcbn7qw6n5n0lx3x4m52igiyq4zrnbc1sj38r7k6s")))

