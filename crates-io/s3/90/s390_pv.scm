(define-module (crates-io s3 #{90}# s390_pv) #:use-module (crates-io))

(define-public crate-s390_pv-0.1.0 (c (n "s390_pv") (v "0.1.0") (d (list (d (n "s390_pv_core") (r "^0.1.0") (d #t) (k 0)))) (h "0bix2gqid2dgfnf99bxmhv5rldgn1vivy594wk3icwgbha5nxsv6") (y #t)))

(define-public crate-s390_pv-0.10.0 (c (n "s390_pv") (v "0.10.0") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "curl") (r "^0.4.44") (d #t) (k 0)) (d (n "foreign-types") (r "^0.3.1") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (f (quote ("std" "release_max_level_debug"))) (d #t) (k 0)) (d (n "openssl") (r "^0.10.57") (d #t) (k 0)) (d (n "openssl-sys") (r "^0.9.92") (d #t) (k 0)) (d (n "pv_core") (r "^0.10.0") (d #t) (k 0) (p "s390_pv_core")) (d (n "serde") (r "^1.0.139") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_test") (r "^1.0.139") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.33") (d #t) (k 0)) (d (n "zerocopy") (r "^0.7") (f (quote ("derive"))) (d #t) (k 0)))) (h "0j2zyadpjfg82105r5wnka8azkvg825mxvfw9y73wnxvqnkb6xcz")))

