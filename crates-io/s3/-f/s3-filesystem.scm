(define-module (crates-io s3 -f s3-filesystem) #:use-module (crates-io))

(define-public crate-s3-filesystem-0.0.1 (c (n "s3-filesystem") (v "0.0.1") (d (list (d (n "aws-config") (r "^0.57.1") (d #t) (k 0)) (d (n "aws-sdk-s3") (r "^0.35.0") (d #t) (k 0)) (d (n "aws-smithy-runtime-api") (r "^0.57.1") (d #t) (k 0)) (d (n "tokio") (r "^1.33.0") (f (quote ("fs" "io-util" "io-std"))) (d #t) (k 0)) (d (n "tokio") (r "^1.33.0") (f (quote ("full"))) (d #t) (k 2)) (d (n "tokio-stream") (r "^0.1.14") (d #t) (k 0)))) (h "00cdqx7yfgzjdqbzwqwcn78sqqvji411kdw1hkzjbmhn4ar9hzjp")))

