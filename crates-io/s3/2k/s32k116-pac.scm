(define-module (crates-io s3 #{2k}# s32k116-pac) #:use-module (crates-io))

(define-public crate-s32k116-pac-0.1.0 (c (n "s32k116-pac") (v "0.1.0") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "091lppgl4p9gan9yf22wqj7m7cqpz11dld267lkjwwfzkxzh685s") (f (quote (("rt" "cortex-m-rt/device"))))))

