(define-module (crates-io s3 #{2k}# s32k146-pac) #:use-module (crates-io))

(define-public crate-s32k146-pac-0.1.0 (c (n "s32k146-pac") (v "0.1.0") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "14y7zb3d6qpk31nfd47xbq48mb9if6kp1nxws4zn4aa6w01674xs") (f (quote (("rt" "cortex-m-rt/device"))))))

