(define-module (crates-io s3 #{2k}# s32k118-pac) #:use-module (crates-io))

(define-public crate-s32k118-pac-0.1.0 (c (n "s32k118-pac") (v "0.1.0") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "1dy312whvf4j1c57rhi3lb8ziw97mmsjsvrxwn2ccifpspmf8jly") (f (quote (("rt" "cortex-m-rt/device"))))))

