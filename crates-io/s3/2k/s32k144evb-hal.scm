(define-module (crates-io s3 #{2k}# s32k144evb-hal) #:use-module (crates-io))

(define-public crate-s32k144evb-hal-0.8.0 (c (n "s32k144evb-hal") (v "0.8.0") (d (list (d (n "bit_field") (r "^0.9.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.5.8") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.7") (d #t) (k 0)) (d (n "cortex-m-rtfm") (r "^0.4.0") (f (quote ("timer-queue"))) (d #t) (k 2)) (d (n "embedded_types") (r "^0.3.2") (d #t) (k 0)) (d (n "s32k144") (r "^0.10.0") (f (quote ("rt"))) (d #t) (k 0)))) (h "0n16kaa5va9hk6q093cwnacl3rp499gk2klswicb97llzp9pzc6v") (f (quote (("panic-over-serial") ("panic-over-itm" "itm") ("itm") ("default" "panic-over-serial")))) (y #t)))

