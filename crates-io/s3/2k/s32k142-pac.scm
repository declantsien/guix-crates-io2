(define-module (crates-io s3 #{2k}# s32k142-pac) #:use-module (crates-io))

(define-public crate-s32k142-pac-0.1.0 (c (n "s32k142-pac") (v "0.1.0") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "0jl1q9bl8nkqmrnvmz7jy44c4wk2qwg0r9qbwavfpakvzsv7q1xc") (f (quote (("rt" "cortex-m-rt/device"))))))

