(define-module (crates-io s3 #{2k}# s32k144evb-quickstart) #:use-module (crates-io))

(define-public crate-s32k144evb-quickstart-0.1.0 (c (n "s32k144evb-quickstart") (v "0.1.0") (d (list (d (n "cortex-m") (r "^0.3.1") (d #t) (k 0)) (d (n "s32k144evb") (r "^0.5.0") (d #t) (k 0)))) (h "12hh8hm56i27b9mav3cz8zps5jsaf3p9ks4s2vjyi3s4pqc8g4lj")))

(define-public crate-s32k144evb-quickstart-0.2.0 (c (n "s32k144evb-quickstart") (v "0.2.0") (d (list (d (n "cortex-m") (r "^0.3.1") (d #t) (k 0)) (d (n "s32k144evb") (r "^0.6.1") (d #t) (k 0)))) (h "1zpx83yl494dbqhv8r44fkd2qjw34j3c3wwfd9dri8cdy2br9vc6")))

(define-public crate-s32k144evb-quickstart-0.2.1 (c (n "s32k144evb-quickstart") (v "0.2.1") (d (list (d (n "cortex-m") (r "^0.3.1") (d #t) (k 0)) (d (n "s32k144evb") (r "^0.6.1") (d #t) (k 0)))) (h "0nr1hs87h1pkrnvd93wblf8am6d88qygz53z7vsavwqd5pns75xr")))

