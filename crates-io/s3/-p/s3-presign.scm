(define-module (crates-io s3 -p s3-presign) #:use-module (crates-io))

(define-public crate-s3-presign-0.0.1 (c (n "s3-presign") (v "0.0.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "hmac") (r "^0.12") (d #t) (k 0)) (d (n "percent-encoding") (r "^2") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "0y9yk4vzkkyyniqhcv00a35gzhn2bsrx54abcpqk4gphbbgy54r2")))

(define-public crate-s3-presign-0.0.2 (c (n "s3-presign") (v "0.0.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "hmac") (r "^0.12") (d #t) (k 0)) (d (n "percent-encoding") (r "^2") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "14kfskyzcjgczbjw5mksxi7dwhwf7kiv4vcsgsc1g0xvlmxj13kl")))

