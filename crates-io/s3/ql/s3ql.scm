(define-module (crates-io s3 ql s3ql) #:use-module (crates-io))

(define-public crate-s3ql-0.1.0 (c (n "s3ql") (v "0.1.0") (h "1nxnklh498fn993wc06yxcqycjyhi9sf13ly765cqkzbi2i2gkr7")))

(define-public crate-s3ql-0.1.1 (c (n "s3ql") (v "0.1.1") (d (list (d (n "async-trait") (r "^0.1.42") (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "cargo-husky") (r "^1") (f (quote ("run-cargo-fmt" "run-cargo-clippy"))) (k 2)) (d (n "rusoto_core") (r "^0.45.0") (d #t) (k 0)) (d (n "rusoto_s3") (r "^0.45.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2.22") (f (quote ("full"))) (d #t) (k 0)))) (h "0mamx6lwmx57mf1karfaysh7pwvz247xjsya5shxfv4m41nx2mbg")))

(define-public crate-s3ql-0.1.2 (c (n "s3ql") (v "0.1.2") (d (list (d (n "async-trait") (r "^0.1.42") (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "cargo-husky") (r "^1") (f (quote ("run-cargo-fmt" "run-cargo-clippy"))) (k 2)) (d (n "rusoto_core") (r "^0.45.0") (d #t) (k 0)) (d (n "rusoto_s3") (r "^0.45.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2.22") (f (quote ("full"))) (d #t) (k 0)))) (h "05hsv95n1baqx36fz3gwshgnikadyxwzym07xyg6z4ni9jfw012b")))

(define-public crate-s3ql-0.1.3 (c (n "s3ql") (v "0.1.3") (d (list (d (n "async-trait") (r "^0.1.42") (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "cargo-husky") (r "^1") (f (quote ("run-cargo-fmt" "run-cargo-clippy"))) (k 2)) (d (n "rusoto_core") (r "^0.45.0") (d #t) (k 0)) (d (n "rusoto_credential") (r "^0.45.0") (o #t) (d #t) (k 0)) (d (n "rusoto_s3") (r "^0.45.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2.22") (f (quote ("full"))) (d #t) (k 0)))) (h "1319ay9iw1ksbzndzd9jcp4w4rcwzyby04gxnis9xh1wpx12cnkx") (f (quote (("auth" "rusoto_credential"))))))

