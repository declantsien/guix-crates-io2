(define-module (crates-io s3 sb s3sb) #:use-module (crates-io))

(define-public crate-s3sb-0.1.0 (c (n "s3sb") (v "0.1.0") (d (list (d (n "byte-unit") (r "^4.0.14") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "cotton") (r "^0.0.20") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "running-average") (r "^0.1.0") (d #t) (k 0)) (d (n "s3-sync") (r "^0.2.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.2") (d #t) (k 0)))) (h "022mfwh33pl1983nkbsp1g74p68s5896fnl8nv8igvqy3da88m45")))

(define-public crate-s3sb-0.1.1 (c (n "s3sb") (v "0.1.1") (d (list (d (n "byte-unit") (r "^4.0.14") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "cotton") (r "^0.0.20") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "running-average") (r "^0.1.0") (d #t) (k 0)) (d (n "s3-sync") (r "^0.2.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.2") (d #t) (k 0)))) (h "0r881210xbg1vlg14m0j0haanw6zlrsflnph9h5d36n19f302ldy")))

