(define-module (crates-io fu te futex-queue) #:use-module (crates-io))

(define-public crate-futex-queue-0.1.0 (c (n "futex-queue") (v "0.1.0") (d (list (d (n "heapless") (r "^0.7") (d #t) (k 0)) (d (n "linux-futex") (r "^0.1") (d #t) (k 0)))) (h "1yygvanxwhsivgsl3kwwv8yh020jmi7v5l845i3aw0fd5kd518yd")))

(define-public crate-futex-queue-0.1.1 (c (n "futex-queue") (v "0.1.1") (d (list (d (n "heapless") (r "^0.7") (d #t) (k 0)) (d (n "linux-futex") (r "^0.1") (d #t) (k 0)))) (h "06zcinf4kfa4akf140m453vwwjsf92dy8ry088yvfyijfsib9jfl")))

