(define-module (crates-io fu te futex) #:use-module (crates-io))

(define-public crate-futex-0.1.0 (c (n "futex") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1szxdn1ym9gpcg34dypf53q65i2q1fl52mx3isdl3gvvnxbmg1nj")))

(define-public crate-futex-0.1.1 (c (n "futex") (v "0.1.1") (d (list (d (n "integer-atomics") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "19f6vrh6ys1nfl66p4jsxm0sz7iba65c76hv5ssn2gmi2p1vqgb1") (f (quote (("nightly" "integer-atomics/nightly"))))))

(define-public crate-futex-0.1.2 (c (n "futex") (v "0.1.2") (d (list (d (n "integer-atomics") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1zp4295ayy05ka6cxd0k37ryygpsbqy1hvwmsh6jlk8kq5n0j0dr") (f (quote (("nightly" "integer-atomics/nightly"))))))

(define-public crate-futex-0.1.3 (c (n "futex") (v "0.1.3") (d (list (d (n "integer-atomics") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "lock-wrappers") (r "^0.1.2") (d #t) (k 0)))) (h "1wyfqs6jhkz036gk97zrbzl0jc8q67ikh1nmd98fwbcpm0ahwdsd") (f (quote (("nightly" "integer-atomics/nightly"))))))

