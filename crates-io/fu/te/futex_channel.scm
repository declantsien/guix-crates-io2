(define-module (crates-io fu te futex_channel) #:use-module (crates-io))

(define-public crate-futex_channel-0.0.0 (c (n "futex_channel") (v "0.0.0") (d (list (d (n "linux-futex") (r "^0.1") (d #t) (k 0)) (d (n "threadpool") (r "^1.8.1") (d #t) (k 2)))) (h "112mqxb3fm3ical3339dvj17ixkx5sjspivbk09lksawng91b8z0")))

