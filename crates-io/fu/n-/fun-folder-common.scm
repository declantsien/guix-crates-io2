(define-module (crates-io fu n- fun-folder-common) #:use-module (crates-io))

(define-public crate-fun-folder-common-0.1.0 (c (n "fun-folder-common") (v "0.1.0") (h "1g1arxqy4gcxyzl9m40qzj2di7fsdpkgg5vch81irsb0a4zwpz3d")))

(define-public crate-fun-folder-common-0.1.1 (c (n "fun-folder-common") (v "0.1.1") (h "0zf0rm88cyvvzjliw97yx5c50i57i9n1xf2xqfn6252s6hj7yh38")))

(define-public crate-fun-folder-common-0.1.2 (c (n "fun-folder-common") (v "0.1.2") (d (list (d (n "notify") (r "^4.0.15") (d #t) (k 0)))) (h "1s0hqpjz0agw0j2x9dm7y0nbv7zlbakpz17nysrlznbap73yklf2")))

