(define-module (crates-io fu n- fun-pkg-impl) #:use-module (crates-io))

(define-public crate-fun-pkg-impl-0.2.0 (c (n "fun-pkg-impl") (v "0.2.0") (d (list (d (n "fun-pkg-api") (r "^0.1.1") (d #t) (k 0)))) (h "0ys42dkwgyks34zq30kxbw7dkl45lcvxqs3sdf4gkf03z7qpaxnv")))

(define-public crate-fun-pkg-impl-0.3.0 (c (n "fun-pkg-impl") (v "0.3.0") (d (list (d (n "fun-pkg-api") (r "^0.1.1") (d #t) (k 0)))) (h "08nq83n9dyk7rpbz9ivr3fvr1hd8zynf7znyx4f7jwcpvlva3zjf")))

