(define-module (crates-io fu se fusebox) #:use-module (crates-io))

(define-public crate-fusebox-0.1.0 (c (n "fusebox") (v "0.1.0") (h "183xy489gc6s4gi6rszdc26lay2ndyv39arrjazysj36dld8wnp6") (y #t)))

(define-public crate-fusebox-0.2.0 (c (n "fusebox") (v "0.2.0") (h "1ira0a4g24zxxpiswrzsaq6yd5yn5zr3cnmc31niqb0grpasqbfk") (y #t)))

(define-public crate-fusebox-0.3.0 (c (n "fusebox") (v "0.3.0") (h "0dba0vfad9nnrqq2md3dk6h742y3dkkihxc2qqkpv0linq0qprk4") (y #t)))

(define-public crate-fusebox-0.4.0 (c (n "fusebox") (v "0.4.0") (h "17fr9884p7x5lk6iww08qjcldbphqmijhrs563sv8q9kdk9nps8g") (y #t)))

(define-public crate-fusebox-0.5.0 (c (n "fusebox") (v "0.5.0") (h "0v9mbnw2cynl0dmyqj9lwaisf3457sflzfy6zn1gmgzd0bv0gyiv") (y #t)))

(define-public crate-fusebox-0.6.0 (c (n "fusebox") (v "0.6.0") (d (list (d (n "criterion") (r "^0.4.0") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "pprof") (r "^0.11.0") (f (quote ("flamegraph" "criterion"))) (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "10kqpzkkckbnw9nhhjrv2i40x583c3kvbpc98in1r1bj8xylsnfa") (y #t)))

(define-public crate-fusebox-0.6.1 (c (n "fusebox") (v "0.6.1") (d (list (d (n "criterion") (r "^0.4.0") (f (quote ("html_reports"))) (o #t) (d #t) (k 0)) (d (n "pprof") (r "^0.11.0") (f (quote ("flamegraph" "criterion"))) (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (o #t) (d #t) (k 0)))) (h "00ndaacb2halzc8qd3bv1fc5j0sssri1dxpdy6ybw2i2ag8fmjaf") (y #t) (s 2) (e (quote (("bench" "dep:criterion" "dep:rand" "dep:pprof"))))))

(define-public crate-fusebox-0.8.0 (c (n "fusebox") (v "0.8.0") (d (list (d (n "bumpalo") (r "^3.11.1") (f (quote ("boxed"))) (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (f (quote ("html_reports"))) (o #t) (d #t) (k 0)) (d (n "pprof") (r "^0.11.0") (f (quote ("flamegraph" "criterion"))) (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (o #t) (d #t) (k 0)))) (h "0i7ch83ad26an115kj8c1a78cqai0ssa39ggwjlpycm4ybwscy99") (y #t) (s 2) (e (quote (("bench" "dep:criterion" "dep:rand" "dep:pprof" "dep:bumpalo"))))))

(define-public crate-fusebox-0.8.1 (c (n "fusebox") (v "0.8.1") (d (list (d (n "bumpalo") (r "^3.11.1") (f (quote ("boxed"))) (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (f (quote ("html_reports"))) (o #t) (d #t) (k 0)) (d (n "pprof") (r "^0.11.0") (f (quote ("flamegraph" "criterion"))) (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (o #t) (d #t) (k 0)))) (h "0akgfd1530zzhp7l9a0zzpjnzq0mxbyslhmsy787i5nnmjy9fyym") (s 2) (e (quote (("bench" "dep:criterion" "dep:rand" "dep:pprof" "dep:bumpalo"))))))

(define-public crate-fusebox-0.8.2 (c (n "fusebox") (v "0.8.2") (d (list (d (n "bumpalo") (r "^3.11.1") (f (quote ("boxed"))) (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (f (quote ("html_reports"))) (o #t) (d #t) (k 0)) (d (n "pprof") (r "^0.11.0") (f (quote ("flamegraph" "criterion"))) (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (o #t) (d #t) (k 0)))) (h "13rcrwz42k9k81m8w7aywhlqxsgnznnf1zc7hw63hb1w8v4jq9ci") (s 2) (e (quote (("bench" "dep:criterion" "dep:rand" "dep:pprof" "dep:bumpalo"))))))

(define-public crate-fusebox-0.8.3 (c (n "fusebox") (v "0.8.3") (d (list (d (n "bumpalo") (r "^3.11.1") (f (quote ("boxed"))) (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (f (quote ("html_reports"))) (o #t) (d #t) (k 0)) (d (n "pprof") (r "^0.11.0") (f (quote ("flamegraph" "criterion"))) (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0vgliwsizl5256h6shmcvmqmsaxnnh8vbgdv7pwvp3j8ciz4myc9") (s 2) (e (quote (("bench" "dep:criterion" "dep:pprof" "dep:bumpalo"))))))

