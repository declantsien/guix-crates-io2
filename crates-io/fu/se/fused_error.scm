(define-module (crates-io fu se fused_error) #:use-module (crates-io))

(define-public crate-fused_error-0.1.0 (c (n "fused_error") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^1") (o #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("derive" "full" "parsing"))) (d #t) (k 2)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "1vmalkq9x7p345i3hqy1wap2883v8fdhbl21ln6pk9kysdny1i7c")))

(define-public crate-fused_error-0.1.1 (c (n "fused_error") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^1") (o #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("derive" "full" "parsing"))) (d #t) (k 2)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "065sxr47qy7v4png6ql9rxdfrycf3ypwsmiambrzagl0g9pc7xnj")))

(define-public crate-fused_error-0.1.2 (c (n "fused_error") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^1") (o #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("derive" "full" "parsing"))) (d #t) (k 2)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "0l69dsrh8lxrj5myin6d6wv2i9wch3cmf61g48xvx0krmvx9gj8a")))

