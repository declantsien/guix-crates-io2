(define-module (crates-io fu se fusen-macro) #:use-module (crates-io))

(define-public crate-fusen-macro-0.2.0 (c (n "fusen-macro") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1fr78w6s19qmy0fyphgn52ywl7bsicpvj9w1s6mbghpqry99jfpx")))

(define-public crate-fusen-macro-0.3.0 (c (n "fusen-macro") (v "0.3.0") (d (list (d (n "fusen-common") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1q439x2n7rxfby403xzph1kyibnr2rpxc78ffx9h3vqk84rjd6lv")))

(define-public crate-fusen-macro-0.3.1 (c (n "fusen-macro") (v "0.3.1") (d (list (d (n "fusen-common") (r "^0.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0g1r4aplbq1vppxigldk3cc176m1p59g4xmizl3qwsd3a2jzjgjg")))

(define-public crate-fusen-macro-0.3.2 (c (n "fusen-macro") (v "0.3.2") (d (list (d (n "fusen-common") (r "^0.3.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1qbcs94vakgdvxwnm8vz38aqhgiy7hz79x6q30z86235i662y9g7")))

(define-public crate-fusen-macro-0.5.1 (c (n "fusen-macro") (v "0.5.1") (d (list (d (n "fusen-common") (r "^0.5.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0y8kq2xhybwh2h7xl51xprclkkgksdamrdfj0fx78rr15qg9yfb9")))

