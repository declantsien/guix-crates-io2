(define-module (crates-io fu se fused-lock) #:use-module (crates-io))

(define-public crate-fused-lock-0.1.0 (c (n "fused-lock") (v "0.1.0") (d (list (d (n "parking_lot") (r "^0.11.1") (d #t) (k 0)))) (h "0853lc9ln6fi17wqzhl0f3nyd4ldh2r0lhp2pnd63smrj1bvi3a8")))

(define-public crate-fused-lock-0.1.1 (c (n "fused-lock") (v "0.1.1") (d (list (d (n "parking_lot") (r "^0.11.1") (d #t) (k 0)))) (h "0bas31xra04y13inyj2578c0biag9wp6zrfvcf2qa3rhfylw6i7b")))

