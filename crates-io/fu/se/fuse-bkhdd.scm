(define-module (crates-io fu se fuse-bkhdd) #:use-module (crates-io))

(define-public crate-fuse-bkhdd-0.1.0 (c (n "fuse-bkhdd") (v "0.1.0") (h "1cx2vrpbgpknggiklfdbjm5g9dwbfk1bvk9dnywcif4cw9fgvn81")))

(define-public crate-fuse-bkhdd-0.1.1 (c (n "fuse-bkhdd") (v "0.1.1") (h "0x0xiyd3cgmpzrg87bbn3v0mv1xk82z1790xjv3isy2b3f58401q")))

(define-public crate-fuse-bkhdd-0.1.2 (c (n "fuse-bkhdd") (v "0.1.2") (h "09wm4g76vspnszw1vjqlmzcs35v9xn4j0fc72idi2bpazc6kw5jk")))

