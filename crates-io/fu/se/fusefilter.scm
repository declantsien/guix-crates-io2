(define-module (crates-io fu se fusefilter) #:use-module (crates-io))

(define-public crate-fusefilter-0.0.1 (c (n "fusefilter") (v "0.0.1") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "fastrand") (r "^1.4.1") (d #t) (k 2)))) (h "09s3k4b1wnfags84qv88hcrfpxq6kv6736l5i4siaihfyllmsqfy") (y #t)))

(define-public crate-fusefilter-0.0.2 (c (n "fusefilter") (v "0.0.2") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "fastrand") (r "^1.4.1") (d #t) (k 2)))) (h "16h99cd4ryzqf0xp3x41li2r8kpg7kw1mv45qx0b75l6ycyk7m6i")))

(define-public crate-fusefilter-0.1.0 (c (n "fusefilter") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "fastrand") (r "^1.4.1") (d #t) (k 2)))) (h "07nlymvd47mkvxgkldim8jmly4f4nn2yhdsfdqjf53bnsbqchy7p")))

