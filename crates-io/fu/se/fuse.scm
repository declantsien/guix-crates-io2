(define-module (crates-io fu se fuse) #:use-module (crates-io))

(define-public crate-fuse-0.2.0 (c (n "fuse") (v "0.2.0") (d (list (d (n "log") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)))) (h "18blr9bw7cajwc9yd9dsd3zj90hj1abh3350a8gcgqy0p2ib1rq3")))

(define-public crate-fuse-0.2.1 (c (n "fuse") (v "0.2.1") (d (list (d (n "log") (r "*") (d #t) (k 0)) (d (n "pkg-config") (r "*") (d #t) (k 1)) (d (n "time") (r "*") (d #t) (k 0)))) (h "0c35yn5ds53rpl70a87444jx8spgvlh3682qm59s9x70qzzab1z1")))

(define-public crate-fuse-0.2.2 (c (n "fuse") (v "0.2.2") (d (list (d (n "log") (r "*") (d #t) (k 0)) (d (n "pkg-config") (r "*") (d #t) (k 1)) (d (n "time") (r "*") (d #t) (k 0)))) (h "03kz9pbiyng4807v87hwfcjcf8g7wwhdzxiv6p47n12idhix16xy")))

(define-public crate-fuse-0.2.3 (c (n "fuse") (v "0.2.3") (d (list (d (n "log") (r "*") (d #t) (k 0)) (d (n "pkg-config") (r "*") (d #t) (k 1)) (d (n "time") (r "*") (d #t) (k 0)))) (h "068xs6vfcgvr4bpxpq2l6n7ajvaavmhcc5ajl3lnbwrmcf7b32ck")))

(define-public crate-fuse-0.2.4 (c (n "fuse") (v "0.2.4") (d (list (d (n "log") (r "*") (d #t) (k 0)) (d (n "pkg-config") (r "*") (d #t) (k 1)) (d (n "time") (r "*") (d #t) (k 0)))) (h "0960wpny4g9p2k9j5gvia6vrl0fjdn251slbkgwzqd0mb8x725ba")))

(define-public crate-fuse-0.2.5 (c (n "fuse") (v "0.2.5") (d (list (d (n "log") (r "*") (d #t) (k 0)) (d (n "pkg-config") (r "*") (d #t) (k 1)) (d (n "time") (r "*") (d #t) (k 0)))) (h "1j66hgp63v647hhrhxfw0lxyjvb8v3kmb4hgl06h4za7fl27y054")))

(define-public crate-fuse-0.2.6 (c (n "fuse") (v "0.2.6") (d (list (d (n "log") (r "*") (d #t) (k 0)) (d (n "pkg-config") (r "*") (d #t) (k 1)) (d (n "time") (r "*") (d #t) (k 0)))) (h "0l834l5xa5prxfpaj7wmv2ljb9cnw7v3ncinzk31axrymininrhz")))

(define-public crate-fuse-0.2.7 (c (n "fuse") (v "0.2.7") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "thread-scoped") (r "^1.0") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1s6v5mp8lxpjs8cjn5v7mi4hgqxygczgg6x230c387hhmq52r8p4")))

(define-public crate-fuse-0.2.8 (c (n "fuse") (v "0.2.8") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.7") (d #t) (k 1)) (d (n "thread-scoped") (r "^1.0") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1xh1h1sfzs0pyz92h0wc70c1qv9579sa59ys6iggkkrrxdw89w5d")))

(define-public crate-fuse-0.3.0 (c (n "fuse") (v "0.3.0") (d (list (d (n "env_logger") (r "^0.3") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "thread-scoped") (r "^1") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0a2a25pxwrapc2syqzxh5dd0b3g44j10mgsarmnfsvxkwln2d1sh")))

(define-public crate-fuse-0.3.1 (c (n "fuse") (v "0.3.1") (d (list (d (n "env_logger") (r "^0.3") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "thread-scoped") (r "^1") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0q46kr5z0d0ljallydb417dwgcd2ib5q2ak6jgpvyrh9a5q71rc0")))

