(define-module (crates-io fu se fuse_mt) #:use-module (crates-io))

(define-public crate-fuse_mt-0.1.1 (c (n "fuse_mt") (v "0.1.1") (d (list (d (n "fuse") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "threadpool") (r "^1.0") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1ivc8nq8dz27amy8mqlwk0ljisjkr7803x3az3993d0wf3hqx4fv") (y #t)))

(define-public crate-fuse_mt-0.1.2 (c (n "fuse_mt") (v "0.1.2") (d (list (d (n "fuse") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "threadpool") (r "^1.0") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "178yhwhiwjdb1hy0fjwnfxaclsgy6499q93i7gd7lfas7bmval29")))

(define-public crate-fuse_mt-0.2.0 (c (n "fuse_mt") (v "0.2.0") (d (list (d (n "fuse") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "threadpool") (r "^1.0") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1sy6jibw11hgjllydyrliww4m24aaqlq8lz87nj9a5gh4j87n8zq")))

(define-public crate-fuse_mt-0.2.1 (c (n "fuse_mt") (v "0.2.1") (d (list (d (n "fuse") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "threadpool") (r "^1.0") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0pz3jh5426326ggbs7namwckf9yrxvqdy8mfrfncfmckd8v7lkqy")))

(define-public crate-fuse_mt-0.2.2 (c (n "fuse_mt") (v "0.2.2") (d (list (d (n "fuse") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "threadpool") (r "^1.0") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0xnrrshscqfg8s5pmswpj3mmsjzg08cwp2ysw13d9xpvxydlsyww")))

(define-public crate-fuse_mt-0.3.0 (c (n "fuse_mt") (v "0.3.0") (d (list (d (n "fuse") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "threadpool") (r "^1.0") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0n466baswiynr85q69vs7ij6hn1dac3m2db8ar69sk3vsga8immh")))

(define-public crate-fuse_mt-0.4.0 (c (n "fuse_mt") (v "0.4.0") (d (list (d (n "fuse") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "threadpool") (r "^1.0") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0y2gckvyx2ik3jc5bh7m71c9g719cfcvixla696cc557733snqi0")))

(define-public crate-fuse_mt-0.4.1 (c (n "fuse_mt") (v "0.4.1") (d (list (d (n "fuse") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "threadpool") (r "^1.0") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1ia5q426sq5b88kwnh2y61ijk82b2j6x8f6ydib9pvfd5vsfz70v")))

(define-public crate-fuse_mt-0.4.2 (c (n "fuse_mt") (v "0.4.2") (d (list (d (n "fuse") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "threadpool") (r "^1.0") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0bcr7mslsdz6f6d5nsr1ypfwvh7k3pg8zhl9l8k4r46pxpa743jw")))

(define-public crate-fuse_mt-0.4.3 (c (n "fuse_mt") (v "0.4.3") (d (list (d (n "fuse") (r "^0.3.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "threadpool") (r "^1.0") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1li69g4bj03fv0v4hbcfidc5wn5rkm8rhsy7c68xj615ffzlbzpg")))

(define-public crate-fuse_mt-0.4.4 (c (n "fuse_mt") (v "0.4.4") (d (list (d (n "fuse") (r "^0.3.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "threadpool") (r "^1.0") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "158f2hym63cw62gik0qxqjwnr6qz1kq2vn2bziqkjvg67m5ban4p")))

(define-public crate-fuse_mt-0.5.0 (c (n "fuse_mt") (v "0.5.0") (d (list (d (n "fuse") (r "^0.3.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "threadpool") (r "^1.0") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0js1fw2h88y15zm5907sf0k7n4a852akwnv5r3vyvh29d22dfg76")))

(define-public crate-fuse_mt-0.5.1 (c (n "fuse_mt") (v "0.5.1") (d (list (d (n "fuse") (r "^0.3.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "threadpool") (r "^1.0") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1yksh6j3j8695yliab6vkzib0b1kc9wmnsqs60rkkwjwfvxr2ijr")))

(define-public crate-fuse_mt-0.6.0 (c (n "fuse_mt") (v "0.6.0") (d (list (d (n "fuser") (r "^0.11") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "threadpool") (r "^1.0") (d #t) (k 0)))) (h "01pw64qjsymkk6mmm251r1rwgjam14sc0n706444ffl5jzp35yxn")))

(define-public crate-fuse_mt-0.6.1 (c (n "fuse_mt") (v "0.6.1") (d (list (d (n "fuser") (r "^0.13") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "threadpool") (r "^1.8") (d #t) (k 0)))) (h "0j296nfz2bs3f6kc4r1w4f8q279727pxz34w3nirnbnk9kfbi670")))

