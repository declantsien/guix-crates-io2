(define-module (crates-io fu se fuse-rust) #:use-module (crates-io))

(define-public crate-fuse-rust-0.1.0 (c (n "fuse-rust") (v "0.1.0") (d (list (d (n "crossbeam") (r "^0.7.3") (d #t) (k 0)))) (h "0bnn0dhyn0x17jakaiz8nqdzlyq1r7whgd59nbyj7ir3c6wl1qzx")))

(define-public crate-fuse-rust-0.1.1 (c (n "fuse-rust") (v "0.1.1") (d (list (d (n "crossbeam") (r "^0.7.3") (d #t) (k 0)))) (h "0f8h7k768i9hajw86lhxy7hmz18wxwvjvd1a7g5rfyidsrybizy2")))

(define-public crate-fuse-rust-0.1.2 (c (n "fuse-rust") (v "0.1.2") (d (list (d (n "crossbeam") (r "^0.7.3") (d #t) (k 0)))) (h "0hmdaz9bgh4mj838pn9j3z14qpfximlk3iw3vgfdq45002iq9hk7")))

(define-public crate-fuse-rust-0.1.3 (c (n "fuse-rust") (v "0.1.3") (d (list (d (n "crossbeam") (r "^0.7.3") (d #t) (k 0)))) (h "1yn2z4lrcxdrz70b5bk0qrhhdya1q8mmprb0x88g8xrkymmc04k4")))

(define-public crate-fuse-rust-0.1.4 (c (n "fuse-rust") (v "0.1.4") (d (list (d (n "crossbeam-utils") (r "^0.8.1") (d #t) (k 0)))) (h "15d7wvxrwc8zxig7ixpn9k14p5fhdypxp21zgyz36kh21rs69xw3")))

(define-public crate-fuse-rust-0.2.0 (c (n "fuse-rust") (v "0.2.0") (d (list (d (n "crossbeam-utils") (r "^0.8.1") (d #t) (k 0)))) (h "0piyhl8h5hlf0d5y8vcs4vvk0zk79d9d92v5cwplrcynri8i7pq0")))

(define-public crate-fuse-rust-0.3.0 (c (n "fuse-rust") (v "0.3.0") (d (list (d (n "crossbeam-utils") (r "^0.8.5") (o #t) (d #t) (k 0)))) (h "1bqfzjj2jmvpw98dclbnwq4p6vlszgkgyxihw03syarvnw4zg7qd") (f (quote (("async" "crossbeam-utils"))))))

(define-public crate-fuse-rust-0.3.1 (c (n "fuse-rust") (v "0.3.1") (d (list (d (n "crossbeam-utils") (r "^0.8.14") (o #t) (d #t) (k 0)))) (h "1h37f31in149c65m9jqiygydvsid5hx245ryg169ryrvy1kd4wsa") (f (quote (("async" "crossbeam-utils"))))))

(define-public crate-fuse-rust-0.3.2 (c (n "fuse-rust") (v "0.3.2") (d (list (d (n "crossbeam-utils") (r "^0.8.14") (o #t) (d #t) (k 0)))) (h "0x6ahisqyy35qdhdjy5w9n9vkkk4xy3hrqqn863q3ffd1j732f0v") (f (quote (("async" "crossbeam-utils"))))))

