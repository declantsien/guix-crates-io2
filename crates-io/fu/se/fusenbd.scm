(define-module (crates-io fu se fusenbd) #:use-module (crates-io))

(define-public crate-fusenbd-0.1.0 (c (n "fusenbd") (v "0.1.0") (d (list (d (n "bufstream") (r "^0.1") (d #t) (k 0)) (d (n "fuse") (r "^0.3.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nbd") (r "^0.2.1") (d #t) (k 0)) (d (n "readwriteseekfs") (r "^0.1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0hqjxpmvv4n0i7n4jv9711a863l80nmvixgbdwb6h909mq7p9fnj")))

