(define-module (crates-io fu nk funk) #:use-module (crates-io))

(define-public crate-funk-0.2.0 (c (n "funk") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "quickcheck") (r "^0.6") (d #t) (k 2)) (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "1prq0p836szai7431nb6fnb4b5gbr5cvp7cqb2iqq7y5d7dvgw8m") (f (quote (("schedule") ("default"))))))

