(define-module (crates-io fu nk funkjon) #:use-module (crates-io))

(define-public crate-funkjon-1.0.0 (c (n "funkjon") (v "1.0.0") (h "1mj6zxcgvpay1xrbg6cs9hjb8zyw6md57xjr6lm9jg123qhahfhg")))

(define-public crate-funkjon-1.0.1 (c (n "funkjon") (v "1.0.1") (h "0dn95agkijn4s1yxgcw0zq54m49q9rf5sqffq7bla5xibagfj2d9")))

(define-public crate-funkjon-1.0.2 (c (n "funkjon") (v "1.0.2") (h "0gvdpx5ij19l83d5kkmb4ylmlpifig889m9n5pn5l4ldvyv8fsh7")))

