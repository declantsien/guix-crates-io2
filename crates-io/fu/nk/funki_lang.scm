(define-module (crates-io fu nk funki_lang) #:use-module (crates-io))

(define-public crate-funki_lang-0.1.0 (c (n "funki_lang") (v "0.1.0") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "lalrpop") (r "^0.19.7") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.19.7") (f (quote ("lexer"))) (d #t) (k 0)) (d (n "match_cast") (r "^0.1.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0vrw45j9x11sd59wmsrj10h10hvpkzc9jsqakgjy8n0vr9vrx8x5")))

(define-public crate-funki_lang-0.1.1 (c (n "funki_lang") (v "0.1.1") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "lalrpop") (r "^0.19.7") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.19.7") (f (quote ("lexer"))) (d #t) (k 0)) (d (n "match_cast") (r "^0.1.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1ivq1161vfriz4szjqs1l0j5jyzgqd81cbn1d5ifkgb396wvpyg7")))

(define-public crate-funki_lang-0.1.2 (c (n "funki_lang") (v "0.1.2") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "lalrpop") (r "^0.19.7") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.19.7") (f (quote ("lexer"))) (d #t) (k 0)) (d (n "match_cast") (r "^0.1.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "07h77nqi0zzlxab0iwrbjr0qx19rlqcgxff88cy9mcd8c051picm")))

(define-public crate-funki_lang-0.1.3 (c (n "funki_lang") (v "0.1.3") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "lalrpop") (r "^0.19.7") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.19.7") (f (quote ("lexer"))) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0y1h0spwj70g10w0dscsw44g8scrbj1hsrd5fpjh3s42xsr620j8")))

