(define-module (crates-io fu nk funki_templates) #:use-module (crates-io))

(define-public crate-funki_templates-0.1.0 (c (n "funki_templates") (v "0.1.0") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "lalrpop") (r "^0.19.7") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.19.7") (f (quote ("lexer"))) (d #t) (k 0)) (d (n "match_cast") (r "^0.1.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0w8cnj2wxzrryj0q8046srj0wcp6sa9kbnidc5775vs6nvvy7mbn")))

