(define-module (crates-io fu nk funksteckdose) #:use-module (crates-io))

(define-public crate-funksteckdose-0.1.0 (c (n "funksteckdose") (v "0.1.0") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "structopt") (r "^0.2.16") (d #t) (k 0)) (d (n "wiringpi") (r "^0.2.4") (o #t) (d #t) (k 0)))) (h "1x2z864inp20n5fjiwdpknmjyqlz5wz41vk6l50asrlmy0fvwph6") (f (quote (("default"))))))

