(define-module (crates-io fu nk funksteckdosen-rest-rs) #:use-module (crates-io))

(define-public crate-funksteckdosen-rest-rs-0.1.0 (c (n "funksteckdosen-rest-rs") (v "0.1.0") (d (list (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "funksteckdose") (r "^0.1") (f (quote ("wiringpi"))) (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rocket") (r "^0.4") (d #t) (k 0)))) (h "0h18s2ssq4sysaikschwykrzr4zjqwdf40xa4h19yqi8nvzwzv0k") (f (quote (("wiringpi" "funksteckdose") ("default" "wiringpi"))))))

