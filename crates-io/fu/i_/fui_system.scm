(define-module (crates-io fu i_ fui_system) #:use-module (crates-io))

(define-public crate-fui_system-0.1.0 (c (n "fui_system") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.58.1") (d #t) (k 1)) (d (n "cbindgen") (r "^0.19.0") (d #t) (k 1)) (d (n "gl") (r "^0.14.0") (d #t) (k 2)) (d (n "rust-embed") (r "^5.8.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "10v2kdz08j0jk0fpaaiqxwm7s7149mxy4lxmwc1w7x469jfijlpi")))

(define-public crate-fui_system-0.2.0 (c (n "fui_system") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "cbindgen") (r "^0.20") (d #t) (k 1)) (d (n "fui_system_core") (r "^0.1") (d #t) (k 0)) (d (n "gl") (r "^0.14") (d #t) (k 2)) (d (n "rust-embed") (r "^6.3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0nd7ycwi6laizywfallyrsv9ipxykbggdd2qb2ivrf9l96r3dlq2")))

(define-public crate-fui_system-0.3.0 (c (n "fui_system") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "cbindgen") (r "^0.20") (d #t) (k 1)) (d (n "fui_system_core") (r "^0.1") (d #t) (k 0)) (d (n "gl") (r "^0.14") (d #t) (k 2)) (d (n "rust-embed") (r "^6.3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1cfw67nw2p6342zklgspiynl7lih5xmjcak5xd7zq6c0ylizc07c")))

(define-public crate-fui_system-0.3.1 (c (n "fui_system") (v "0.3.1") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "cbindgen") (r "^0.20") (d #t) (k 1)) (d (n "fui_system_core") (r "^0.1") (d #t) (k 0)) (d (n "gl") (r "^0.14") (d #t) (k 2)) (d (n "rust-embed") (r "^6.3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "060sb8b9ygfqvsdh8l1d5ij8gcj25makgbi0dm7m37kqc07v395v")))

(define-public crate-fui_system-0.5.0 (c (n "fui_system") (v "0.5.0") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "cbindgen") (r "^0.20") (d #t) (k 1)) (d (n "fui_system_core") (r "^0.1") (d #t) (k 0)) (d (n "gl") (r "^0.14") (d #t) (k 2)) (d (n "rust-embed") (r "^6.3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1alnn5dd99qi1f2kc0iq6lkb78fiyd873i1jjrxlvz4vlf0j5136")))

(define-public crate-fui_system-0.5.1 (c (n "fui_system") (v "0.5.1") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "cbindgen") (r "^0.20") (d #t) (k 1)) (d (n "fui_system_core") (r "^0.1") (d #t) (k 0)) (d (n "gl") (r "^0.14") (d #t) (k 2)) (d (n "rust-embed") (r "^6.3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "075755n25dqx9wl23spq5p2a2i7dd1dapswnj8h1insl8rccjah1")))

(define-public crate-fui_system-0.10.0 (c (n "fui_system") (v "0.10.0") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "cbindgen") (r "^0.20") (d #t) (k 1)) (d (n "fui_system_core") (r "^0.1") (d #t) (k 0)) (d (n "gl") (r "^0.14") (d #t) (k 2)) (d (n "rust-embed") (r "^6.3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0hfnmjcdimjwzn6bf71sks1qkd1cyilv0yywfs6lcdwkyjrc9i8h")))

(define-public crate-fui_system-0.11.0 (c (n "fui_system") (v "0.11.0") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "cbindgen") (r "^0.20") (d #t) (k 1)) (d (n "fui_system_core") (r "^0.11") (d #t) (k 0)) (d (n "gl") (r "^0.14") (d #t) (k 2)) (d (n "rust-embed") (r "^6.3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "01pcbgh20pg8vbx4c3nkc7p1427x9m8p2zi7qi81j7rp452gff7l")))

(define-public crate-fui_system-0.11.1 (c (n "fui_system") (v "0.11.1") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "cbindgen") (r "^0.20") (d #t) (k 1)) (d (n "fui_system_core") (r "^0.11") (d #t) (k 0)) (d (n "gl") (r "^0.14") (d #t) (k 2)) (d (n "rust-embed") (r "^6.3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1ka2gh8g79f882hds6j1315yxlpa54nwx7zh6fvw2i6gbpl02dy4")))

(define-public crate-fui_system-0.11.2 (c (n "fui_system") (v "0.11.2") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "cbindgen") (r "^0.20") (d #t) (k 1)) (d (n "fui_system_core") (r "^0.11") (d #t) (k 0)) (d (n "gl") (r "^0.14") (d #t) (k 2)) (d (n "rust-embed") (r "^6.3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "05bprfmrfbih5prn9phvnkx0mjjin42pbms2rmazliazy3v9944d")))

(define-public crate-fui_system-0.11.3 (c (n "fui_system") (v "0.11.3") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "cbindgen") (r "^0.20") (d #t) (k 1)) (d (n "fui_system_core") (r "^0.11") (d #t) (k 0)) (d (n "gl") (r "^0.14") (d #t) (k 2)) (d (n "rust-embed") (r "^6.3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "13dcglpq3p01y8f74g572hlyzdn9g6h5lhh8aba7707kww8ky8xz")))

(define-public crate-fui_system-0.11.4 (c (n "fui_system") (v "0.11.4") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "cbindgen") (r "^0.20") (d #t) (k 1)) (d (n "fui_system_core") (r "^0.11") (d #t) (k 0)) (d (n "gl") (r "^0.14") (d #t) (k 2)) (d (n "rust-embed") (r "^6.3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1s52hfs4796kw95halbkl0d7zv0wsgxqmfya4raw52p0fkrzss4h")))

(define-public crate-fui_system-0.11.5 (c (n "fui_system") (v "0.11.5") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "cbindgen") (r "^0.20") (d #t) (k 1)) (d (n "fui_system_core") (r "^0.11") (d #t) (k 0)) (d (n "gl") (r "^0.14") (d #t) (k 2)) (d (n "rust-embed") (r "^6.3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0ib9dkmnayzpgm3xzsla951frv6jhzs37zw0sqsxs2g0hnydimhh")))

(define-public crate-fui_system-0.11.6 (c (n "fui_system") (v "0.11.6") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "bitflags") (r "^2.1") (d #t) (k 0)) (d (n "cbindgen") (r "^0.20") (d #t) (k 1)) (d (n "fui_system_core") (r "^0.11") (d #t) (k 0)) (d (n "gl") (r "^0.14") (d #t) (k 2)) (d (n "rust-embed") (r "^6.3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "163gxf3i03d7qk5knak5018qjb1m0p8w6h1f6b2a4xb3bwlk1d16")))

(define-public crate-fui_system-0.12.0 (c (n "fui_system") (v "0.12.0") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "cbindgen") (r "^0.20") (d #t) (k 1)) (d (n "fui_system_core") (r "^0.12") (d #t) (k 0)) (d (n "gl") (r "^0.14") (d #t) (k 2)) (d (n "rust-embed") (r "^6.3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "19bsrmxm0w3alq0w4c34jd7ag5whsq0s0rdwyj34wn3k1azh89kd")))

(define-public crate-fui_system-0.12.1 (c (n "fui_system") (v "0.12.1") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "cbindgen") (r "^0.20") (d #t) (k 1)) (d (n "fui_system_core") (r "^0.12") (d #t) (k 0)) (d (n "gl") (r "^0.14") (d #t) (k 2)) (d (n "rust-embed") (r "^6.3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1c4vjb7ydwh3llsgl1k27y0gfyq6b53813baf8hg7b5iwzplvsbc")))

(define-public crate-fui_system-0.13.0 (c (n "fui_system") (v "0.13.0") (d (list (d (n "bindgen") (r "^0.69") (d #t) (k 1)) (d (n "cbindgen") (r "^0.26") (d #t) (k 1)) (d (n "fui_system_core") (r "^0.13") (d #t) (k 0)) (d (n "gl") (r "^0.14") (d #t) (k 2)) (d (n "rust-embed") (r "^8.4") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0shyajn80p2f6qwjs6vaxb30grmr1snsb8xdv8ra05k4bjbchssx")))

