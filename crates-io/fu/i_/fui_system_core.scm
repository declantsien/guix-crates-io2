(define-module (crates-io fu i_ fui_system_core) #:use-module (crates-io))

(define-public crate-fui_system_core-0.1.0 (c (n "fui_system_core") (v "0.1.0") (h "19y6bc7in3p7x7djvrlsj8z5cv0mi05yc5gzb8x5vbhj6aanwn46")))

(define-public crate-fui_system_core-0.11.0 (c (n "fui_system_core") (v "0.11.0") (h "18i3nm00vhll8xczv3kg4p41l82b5wjws27q2fkrgk28gjjd1rzq")))

(define-public crate-fui_system_core-0.12.0 (c (n "fui_system_core") (v "0.12.0") (d (list (d (n "bitflags") (r "^2.1") (d #t) (k 0)))) (h "15ky3vp7gi8k61c3vzizb6908dwv9byrvlb2dnppka6l88haj9a6")))

(define-public crate-fui_system_core-0.13.0 (c (n "fui_system_core") (v "0.13.0") (d (list (d (n "bitflags") (r "^2.5") (d #t) (k 0)))) (h "08xjwmpsa3qfb9lxc6v64pcgz4fd3nr8jg3fdx5b9v4lzx3w9q7i")))

