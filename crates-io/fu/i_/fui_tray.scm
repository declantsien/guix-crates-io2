(define-module (crates-io fu i_ fui_tray) #:use-module (crates-io))

(define-public crate-fui_tray-0.1.0 (c (n "fui_tray") (v "0.1.0") (d (list (d (n "fui_core") (r "^0.1") (d #t) (k 0)))) (h "034y0giq98f4dmhfxsvrcwclxyzhpgsqh02npcg9g7ys97l260wz") (y #t)))

(define-public crate-fui_tray-0.1.1 (c (n "fui_tray") (v "0.1.1") (d (list (d (n "fui_core") (r "^0.1") (d #t) (k 0)))) (h "0wjjbrz8lbxv5731rij25xx7vf22pvbbpxw4c0q9nfa3zpch7k9a") (y #t)))

