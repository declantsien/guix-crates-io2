(define-module (crates-io fu i_ fui_macros) #:use-module (crates-io))

(define-public crate-fui_macros-0.1.0 (c (n "fui_macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "typed-builder") (r "^0.7") (d #t) (k 2)) (d (n "typemap") (r "^0.3") (d #t) (k 2)))) (h "1r434rqk9gg6h6r55gjgqrqrlw58vcgqsvq45ggnnhw09b73z3j9")))

(define-public crate-fui_macros-0.3.0 (c (n "fui_macros") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "typed-builder") (r "^0.10") (d #t) (k 2)) (d (n "typemap") (r "^0.3") (d #t) (k 2)))) (h "0wzn2bfljw1xn6lzz9vky3r0gr92x5jk0mbqkf216zzh1xcrfz12")))

(define-public crate-fui_macros-0.4.0 (c (n "fui_macros") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "typed-builder") (r "^0.18") (d #t) (k 2)) (d (n "typemap") (r "^0.3") (d #t) (k 2)))) (h "0w2nb1fxlpfncanjfh9g17di4bji74ha67ns512k73xs34ix999m")))

