(define-module (crates-io fu x_ fux_kdtree) #:use-module (crates-io))

(define-public crate-fux_kdtree-0.1.0 (c (n "fux_kdtree") (v "0.1.0") (d (list (d (n "bencher") (r "~0.1.2") (d #t) (k 2)) (d (n "quickcheck") (r "~0.3") (d #t) (k 2)) (d (n "rand") (r "~0.3.12") (d #t) (k 2)))) (h "1h7582yrjrq93m3splh8iq5h79qyl8rmjhh2wvbmn1w1z5cj8xjc")))

(define-public crate-fux_kdtree-0.2.0 (c (n "fux_kdtree") (v "0.2.0") (d (list (d (n "bencher") (r "~0.1.2") (d #t) (k 2)) (d (n "quickcheck") (r "~0.3") (d #t) (k 2)) (d (n "rand") (r "~0.3.12") (d #t) (k 2)))) (h "1rvldx2piwib5ah30xkm1jhfhkj4j8mghr03pya77a1ld0jzhsr1")))

