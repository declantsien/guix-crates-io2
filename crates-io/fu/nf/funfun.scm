(define-module (crates-io fu nf funfun) #:use-module (crates-io))

(define-public crate-funfun-0.1.0 (c (n "funfun") (v "0.1.0") (h "0yn31qylc3qsk7n1waifd2l72fb8q36471v9i0k08fp5h3jksb4i")))

(define-public crate-funfun-0.1.1 (c (n "funfun") (v "0.1.1") (h "1qsbw87q006vkka2pl8c1czvmfbz2x12qhdd8jhqjzw1sildwmxg")))

(define-public crate-funfun-0.2.1 (c (n "funfun") (v "0.2.1") (h "0hn4zg6zsmjn417pz9y5i9r6hh68y6y66js9zpid64xyk8v42j5a")))

(define-public crate-funfun-0.2.2 (c (n "funfun") (v "0.2.2") (h "1390jlm45c8nq61qs8hjsa4wynnwqnj61s3fzz05rgd68x9is8i8")))

(define-public crate-funfun-0.2.3 (c (n "funfun") (v "0.2.3") (h "1bhwa21q3fbrl9c4hzsd491hdd51xnaqmdiz184qkzjg7zp7vkx7")))

(define-public crate-funfun-0.2.4 (c (n "funfun") (v "0.2.4") (h "17apapj4wzjkzxjrn2kljf7rfbndhs0jglixlk74lgkf7mlxqcbi")))

