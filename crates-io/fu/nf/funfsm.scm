(define-module (crates-io fu nf funfsm) #:use-module (crates-io))

(define-public crate-funfsm-0.1.0 (c (n "funfsm") (v "0.1.0") (h "0ddcwpgb3innrbyh12fndfl6z0db4lq4515qw9k645fj05cv8z8m")))

(define-public crate-funfsm-0.2.0 (c (n "funfsm") (v "0.2.0") (d (list (d (n "assert_matches") (r "^1.0.1") (d #t) (k 2)))) (h "08ciq7g7szvgkzg76xkygxwn12y8jdb4754xnbqpn0frjbz0a5sg")))

(define-public crate-funfsm-0.2.1 (c (n "funfsm") (v "0.2.1") (d (list (d (n "assert_matches") (r "^1.0.1") (d #t) (k 2)))) (h "0l4n0v6gwxhj9shmlf3107l88bgld080p76z873bnl77jkz8m5mq")))

