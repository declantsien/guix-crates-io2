(define-module (crates-io fu lg fulgurobot_db) #:use-module (crates-io))

(define-public crate-fulgurobot_db-0.1.0 (c (n "fulgurobot_db") (v "0.1.0") (d (list (d (n "diesel") (r "^1.4.2") (f (quote ("sqlite"))) (d #t) (k 0)) (d (n "dotenv") (r "^0.14.1") (d #t) (k 0)))) (h "0r4kqf4r4n9qdwr5q314wdp05n9piqff0jcffbdzyihiiw5skpry")))

(define-public crate-fulgurobot_db-0.2.0 (c (n "fulgurobot_db") (v "0.2.0") (d (list (d (n "diesel") (r "^1.4.2") (f (quote ("sqlite"))) (d #t) (k 0)) (d (n "dotenv") (r "^0.14.1") (d #t) (k 0)))) (h "0y8g00c7x477njxn1p2qh3k2mh51bcprya2b0a91lg55mvfr5nmb")))

(define-public crate-fulgurobot_db-0.3.0 (c (n "fulgurobot_db") (v "0.3.0") (d (list (d (n "diesel") (r "^1.4.2") (f (quote ("sqlite"))) (d #t) (k 0)) (d (n "dotenv") (r "^0.14.1") (d #t) (k 0)))) (h "0pamybvvq6gilb1mxwx5bqslmg7ainbzmgy4lq5wlvwcp4bq8bc4")))

(define-public crate-fulgurobot_db-0.3.1 (c (n "fulgurobot_db") (v "0.3.1") (d (list (d (n "diesel") (r "^1.4.2") (f (quote ("sqlite"))) (d #t) (k 0)) (d (n "dotenv") (r "^0.14.1") (d #t) (k 0)))) (h "0yw36s2f645bssjlpz6xwlfjhg5ax6nyjldrz45i5n0p5852r6fr")))

(define-public crate-fulgurobot_db-0.4.0 (c (n "fulgurobot_db") (v "0.4.0") (d (list (d (n "diesel") (r "^1.4.2") (f (quote ("sqlite"))) (d #t) (k 0)) (d (n "dotenv") (r "^0.14.1") (d #t) (k 0)))) (h "1iy3lq0r3v2mvv16if1fmfkhc049gfg0xk6laz718h3v8cprp7x9")))

(define-public crate-fulgurobot_db-0.4.1 (c (n "fulgurobot_db") (v "0.4.1") (d (list (d (n "diesel") (r "^1.4.2") (f (quote ("sqlite"))) (d #t) (k 0)) (d (n "dotenv") (r "^0.14.1") (d #t) (k 0)))) (h "1k86r1vz8iw8d4y0sig14dff1fpqv0d7df4d0z00xglfav4llrdk")))

(define-public crate-fulgurobot_db-0.4.2 (c (n "fulgurobot_db") (v "0.4.2") (d (list (d (n "diesel") (r "^1.4.2") (f (quote ("sqlite"))) (d #t) (k 0)) (d (n "dotenv") (r "^0.14.1") (d #t) (k 0)))) (h "1q2bh34l33a04fxd9gb11mwyqwvyr8k7qawh2ywxh85d6h2i1drv")))

(define-public crate-fulgurobot_db-0.4.3 (c (n "fulgurobot_db") (v "0.4.3") (d (list (d (n "diesel") (r "^1.4.2") (f (quote ("sqlite"))) (d #t) (k 0)) (d (n "dotenv") (r "^0.14.1") (d #t) (k 0)))) (h "1vrhhn6pgq50gdkgcwsjp7xa37v7jqnnfp5a287aq7qmsjc2v7fd")))

(define-public crate-fulgurobot_db-0.4.4 (c (n "fulgurobot_db") (v "0.4.4") (d (list (d (n "diesel") (r "^1.4.2") (f (quote ("sqlite"))) (d #t) (k 0)) (d (n "dotenv") (r "^0.14.1") (d #t) (k 0)))) (h "1hyhdh1if065nz6pbbq4m8mj8nzff2cic9vnrpwrcbwjj9fpfdi0")))

(define-public crate-fulgurobot_db-0.4.5 (c (n "fulgurobot_db") (v "0.4.5") (d (list (d (n "diesel") (r "^1.4.2") (f (quote ("sqlite"))) (d #t) (k 0)) (d (n "dotenv") (r "^0.14.1") (d #t) (k 0)))) (h "03qwd14dsn3zla0r6di02cii3k1gs6waqbimslm7l1pjf7rjcwg6")))

