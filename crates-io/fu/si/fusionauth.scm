(define-module (crates-io fu si fusionauth) #:use-module (crates-io))

(define-public crate-fusionauth-1.49.0 (c (n "fusionauth") (v "1.49.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)) (d (n "uuid") (r "^1.0") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "1hkndkl8j2gljbgkr5lmlwz601ciqk4bj0wbcg8rmb3pfn7lqiik") (y #t)))

