(define-module (crates-io fu me fumen) #:use-module (crates-io))

(define-public crate-fumen-0.1.0 (c (n "fumen") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0b1yx14h5l85zy8l8xxxiqq5mkfqilnx85kya1npn5lwfi40g65f")))

(define-public crate-fumen-0.1.1 (c (n "fumen") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0i9yk3zfqqja51mihxm49dgjkszcmhhcz0i7vb82zj2nv2591ryn")))

(define-public crate-fumen-0.1.2 (c (n "fumen") (v "0.1.2") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1casqr1488lc14pxni205xd552xc74g76wrgy41zwazr8gkjqbxf")))

