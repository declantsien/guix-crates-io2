(define-module (crates-io fu tm futmio) #:use-module (crates-io))

(define-public crate-futmio-0.0.1 (c (n "futmio") (v "0.0.1") (d (list (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "mio") (r "^0.6.20") (d #t) (k 0)))) (h "0xjbsnh6zbs4ibrj9r71jnb7yqpccbf1n8r3jfyz3karc9sf47za")))

