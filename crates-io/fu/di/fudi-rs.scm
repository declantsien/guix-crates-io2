(define-module (crates-io fu di fudi-rs) #:use-module (crates-io))

(define-public crate-fudi-rs-0.3.0 (c (n "fudi-rs") (v "0.3.0") (d (list (d (n "nom") (r "^6.1.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0jxpf2bx5llysq1n7jcb36d7y6l5f0klzxcygs8dsm2ifp5hbrmv")))

(define-public crate-fudi-rs-0.3.1 (c (n "fudi-rs") (v "0.3.1") (d (list (d (n "nom") (r "^6.1.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0xjl270q4cw8jyfqswgzm3kmzaas8avim031x8722nmnwq210b3a")))

