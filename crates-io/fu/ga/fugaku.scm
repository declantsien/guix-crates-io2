(define-module (crates-io fu ga fugaku) #:use-module (crates-io))

(define-public crate-fugaku-0.1.0 (c (n "fugaku") (v "0.1.0") (h "073vdpj5ynkg01v2dk5a30bpgfqya5bcnjbaspyymggrcqzdl05b") (y #t)))

(define-public crate-fugaku-0.0.0 (c (n "fugaku") (v "0.0.0") (h "04m9lvrdvya4x058m40bm9fr1h5aixxzfmqfm9bc571cpavxgmw6")))

