(define-module (crates-io fu ga fuga-json-seq-parser) #:use-module (crates-io))

(define-public crate-fuga-json-seq-parser-0.1.0 (c (n "fuga-json-seq-parser") (v "0.1.0") (d (list (d (n "aquamarine") (r "^0.1.12") (d #t) (k 0)) (d (n "embedded-io") (r "^0.3.0") (d #t) (k 0)) (d (n "heapless") (r "^0.7.16") (d #t) (k 0)) (d (n "nom") (r "^7.1.1") (k 0)) (d (n "uuid") (r "^1.2.1") (k 0)))) (h "1j8q1qfs3wn6wy7iybmlpwy6rg9lmf84w93vfphyh141jqsbzqq0") (f (quote (("std" "embedded-io/std") ("default"))))))

(define-public crate-fuga-json-seq-parser-0.2.0 (c (n "fuga-json-seq-parser") (v "0.2.0") (d (list (d (n "aquamarine") (r "^0.1.12") (d #t) (k 0)) (d (n "embedded-io") (r "^0.3.0") (d #t) (k 0)) (d (n "heapless") (r "^0.7.16") (d #t) (k 0)) (d (n "nom") (r "^7.1.1") (k 0)) (d (n "uuid") (r "^1.2.1") (k 0)))) (h "1irs516pb0ypdpc3v015cy8g82kc2saf1i30nnndqqc6rkvbigw4") (f (quote (("std" "embedded-io/std") ("default"))))))

