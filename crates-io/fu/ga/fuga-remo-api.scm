(define-module (crates-io fu ga fuga-remo-api) #:use-module (crates-io))

(define-public crate-fuga-remo-api-0.1.1 (c (n "fuga-remo-api") (v "0.1.1") (d (list (d (n "bbqueue") (r "^0.5.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.22") (k 0)) (d (n "embedded-io") (r "^0.3.0") (d #t) (k 0)) (d (n "fuga-json-seq-parser") (r "^0.2.0") (d #t) (k 0)) (d (n "heapless") (r "^0.7.16") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (k 0)) (d (n "nom") (r "^7.1.1") (k 0)) (d (n "uuid") (r "^1.2.1") (k 0)))) (h "1j88j1aqlp2rzpw90nd92ygyvdlh20i9ys7mp5mi1cdzkg3i4knz") (f (quote (("std" "embedded-io/std" "fuga-json-seq-parser/std") ("default"))))))

(define-public crate-fuga-remo-api-0.1.2 (c (n "fuga-remo-api") (v "0.1.2") (d (list (d (n "bbqueue") (r "^0.5.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.22") (k 0)) (d (n "embedded-io") (r "^0.3.0") (d #t) (k 0)) (d (n "fuga-json-seq-parser") (r "^0.2.0") (d #t) (k 0)) (d (n "heapless") (r "^0.7.16") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (k 0)) (d (n "nom") (r "^7.1.1") (k 0)) (d (n "uuid") (r "^1.2.1") (k 0)))) (h "1vsi9sq1lhd6kwsszq34x616c5028axs103cwy47475lh61machy") (f (quote (("std" "embedded-io/std" "fuga-json-seq-parser/std") ("default"))))))

