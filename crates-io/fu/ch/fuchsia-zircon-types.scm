(define-module (crates-io fu ch fuchsia-zircon-types) #:use-module (crates-io))

(define-public crate-fuchsia-zircon-types-0.4.0-alpha.1 (c (n "fuchsia-zircon-types") (v "0.4.0-alpha.1") (d (list (d (n "zerocopy") (r "^0.8.0-alpha.5") (o #t) (d #t) (k 0)))) (h "0735kq1kp2rgz1527n9379l9w4wxd40bhbs98m4gq740cn627lj0")))

