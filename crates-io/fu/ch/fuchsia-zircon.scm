(define-module (crates-io fu ch fuchsia-zircon) #:use-module (crates-io))

(define-public crate-fuchsia-zircon-0.2.0 (c (n "fuchsia-zircon") (v "0.2.0") (d (list (d (n "fuchsia-zircon-sys") (r "^0.2.0") (d #t) (k 0)))) (h "1mjmwkjdfcmyv0ghgr1mp00hxp80jjznmq8r389dnrw4ri8qzr0k")))

(define-public crate-fuchsia-zircon-0.2.1 (c (n "fuchsia-zircon") (v "0.2.1") (d (list (d (n "fuchsia-zircon-sys") (r "^0.2.0") (d #t) (k 0)))) (h "0ngiizz8mc2fj9jycdgcqrhm6hg3mzi9xxc75gjn4cin9qd5ih7n")))

(define-public crate-fuchsia-zircon-0.3.1 (c (n "fuchsia-zircon") (v "0.3.1") (d (list (d (n "bitflags") (r "^1.0.0") (d #t) (k 0)) (d (n "fuchsia-zircon-sys") (r "^0.3.1") (d #t) (k 0)))) (h "1bnpfja8b7i17s800kqg1mqvkcw09y24w8jsfxp93pqzs2pnalrv")))

(define-public crate-fuchsia-zircon-0.3.2 (c (n "fuchsia-zircon") (v "0.3.2") (d (list (d (n "bitflags") (r "^1.0.0") (d #t) (k 0)) (d (n "fuchsia-zircon-sys") (r "^0.3.2") (d #t) (k 0)))) (h "06fc7j914ibnmlyb1f6wk8qq428v10f8zgpk4jx57br5qf3h0ldx")))

(define-public crate-fuchsia-zircon-0.3.3 (c (n "fuchsia-zircon") (v "0.3.3") (d (list (d (n "bitflags") (r "^1.0.0") (d #t) (k 0)) (d (n "fuchsia-zircon-sys") (r "^0.3.3") (d #t) (k 0)))) (h "10jxc5ks1x06gpd0xg51kcjrxr35nj6qhx2zlc5n7bmskv3675rf")))

