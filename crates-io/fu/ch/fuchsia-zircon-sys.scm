(define-module (crates-io fu ch fuchsia-zircon-sys) #:use-module (crates-io))

(define-public crate-fuchsia-zircon-sys-0.2.0 (c (n "fuchsia-zircon-sys") (v "0.2.0") (d (list (d (n "bitflags") (r "^0.7.0") (d #t) (k 0)))) (h "10jad1bz3xbppgvaw27dkmpnfbnymln9fsrs2b38s15f9ddpkws3")))

(define-public crate-fuchsia-zircon-sys-0.3.1 (c (n "fuchsia-zircon-sys") (v "0.3.1") (h "1rlsrwb8726hhkn7y1xvzxm3m1adsax6wpv3428pxssz1sdfz786")))

(define-public crate-fuchsia-zircon-sys-0.3.2 (c (n "fuchsia-zircon-sys") (v "0.3.2") (h "0n8q91a6rlrx0xnypk2w0nkz2ciiaisazrrwnmr6bffn7bqsdcq8")))

(define-public crate-fuchsia-zircon-sys-0.3.3 (c (n "fuchsia-zircon-sys") (v "0.3.3") (h "19zp2085qsyq2bh1gvcxq1lb8w6v6jj9kbdkhpdjrl95fypakjix")))

(define-public crate-fuchsia-zircon-sys-0.4.0-alpha.1 (c (n "fuchsia-zircon-sys") (v "0.4.0-alpha.1") (d (list (d (n "fuchsia-zircon-types") (r "^0.4.0-alpha.1") (d #t) (k 0)))) (h "1hwpywn7519696xkng1c3gvi0jmnqldzbn1c4r1vww2pazy6xcfm")))

