(define-module (crates-io fu rl furl) #:use-module (crates-io))

(define-public crate-furl-0.1.0 (c (n "furl") (v "0.1.0") (d (list (d (n "clap") (r "^3.2.22") (f (quote ("derive"))) (d #t) (k 0)) (d (n "test_bin") (r "^0.4.0") (d #t) (k 2)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "0ry8pixwiwf279y89v0pkhz6hhvc4q5ziirycakh9vb047sc7bhk")))

(define-public crate-furl-0.1.1 (c (n "furl") (v "0.1.1") (d (list (d (n "clap") (r "^3.2.22") (f (quote ("derive"))) (d #t) (k 0)) (d (n "test_bin") (r "^0.4.0") (d #t) (k 2)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "0xhmak139w3ffr2iz2bwq7jvd2zaa8xz99z3cqi21jcivj1hvipd")))

(define-public crate-furl-0.1.2 (c (n "furl") (v "0.1.2") (d (list (d (n "clap") (r "^3.2.22") (f (quote ("derive"))) (d #t) (k 0)) (d (n "test_bin") (r "^0.4.0") (d #t) (k 2)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "0wnqrvqhmfrs8s1ljinrisnfm76xk7jj6czbrnngdgddq85i891m")))

