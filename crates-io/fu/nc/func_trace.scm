(define-module (crates-io fu nc func_trace) #:use-module (crates-io))

(define-public crate-func_trace-1.0.0 (c (n "func_trace") (v "1.0.0") (d (list (d (n "env_logger") (r "^0.8") (d #t) (k 2)) (d (n "gag") (r "^0.1.10") (d #t) (k 2)) (d (n "log") (r "^0.4.8") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0nl58wwr1vjf5n266yyb28xrgbc3s9bv8p4k1lxa74jgmzryvhrl")))

(define-public crate-func_trace-1.0.1 (c (n "func_trace") (v "1.0.1") (d (list (d (n "env_logger") (r "^0.8") (d #t) (k 2)) (d (n "gag") (r "^0.1.10") (d #t) (k 2)) (d (n "log") (r "^0.4.8") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1cckkhlwi87vkldjal9ynwvr8n40d9f4n6pq6hhlhpvklskzx89f")))

(define-public crate-func_trace-1.0.2 (c (n "func_trace") (v "1.0.2") (d (list (d (n "env_logger") (r "^0.8") (d #t) (k 2)) (d (n "gag") (r "^0.1.10") (d #t) (k 2)) (d (n "log") (r "^0.4.8") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1a6apvhxm1pw9chww0ynh480lv25q4nwk0faw3yxa0qbqz5nlwmz")))

(define-public crate-func_trace-1.0.3 (c (n "func_trace") (v "1.0.3") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 2)) (d (n "gag") (r "^0.1.10") (d #t) (k 2)) (d (n "log") (r "^0.4.14") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.63") (f (quote ("full"))) (d #t) (k 0)))) (h "1rck3mzyac88ayirap2qdl6zs249wvfiahkqhrngnpicnwpnnn68")))

