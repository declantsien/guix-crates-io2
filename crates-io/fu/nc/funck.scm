(define-module (crates-io fu nc funck) #:use-module (crates-io))

(define-public crate-funck-0.1.0 (c (n "funck") (v "0.1.0") (h "1d9x928hcywdaifz1hm43zvxkwhla6fd18pd054y29ph3jpas42y")))

(define-public crate-funck-0.2.0 (c (n "funck") (v "0.2.0") (d (list (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.106") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.50") (d #t) (k 2)))) (h "10z576a4f300ydhkffczksh34l8n74q75hi5bh7pyazxnghyrqr0")))

