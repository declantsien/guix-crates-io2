(define-module (crates-io fu nc functions_loop) #:use-module (crates-io))

(define-public crate-functions_loop-0.1.0 (c (n "functions_loop") (v "0.1.0") (h "1mgn5lmhpxb9jc6ps7y0rw4kc4p2hl6cm2acrjn9x4r6vmqs4aqd")))

(define-public crate-functions_loop-0.1.1 (c (n "functions_loop") (v "0.1.1") (h "0xni50d5wvxnzqipq4ckv31xg7zg4y9ff96l3z3fli3h1m4ai18q")))

