(define-module (crates-io fu nc function-timer) #:use-module (crates-io))

(define-public crate-function-timer-0.1.0 (c (n "function-timer") (v "0.1.0") (d (list (d (n "function-timer-macro") (r "^0.1") (d #t) (k 0)) (d (n "metrics") (r "^0.20") (d #t) (k 0)) (d (n "metrics-exporter-prometheus") (r "^0.11") (k 2)) (d (n "metrics-util") (r "^0.14") (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0mrysh7ididgb4m7ix0igsi6sffjr18zjs9m3pn34qfhqc5qs9ph")))

(define-public crate-function-timer-0.2.0 (c (n "function-timer") (v "0.2.0") (d (list (d (n "function-timer-macro") (r "^0.2") (d #t) (k 0)) (d (n "metrics") (r "^0.20") (d #t) (k 0)) (d (n "metrics-exporter-prometheus") (r "^0.11") (k 2)) (d (n "metrics-util") (r "^0.14") (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1jfy33mai5ssbbx6rdn2s0sf8hx2i4zhykzzmm774w85hk92ay9j")))

(define-public crate-function-timer-0.3.0 (c (n "function-timer") (v "0.3.0") (d (list (d (n "function-timer-macro") (r "^0.3") (d #t) (k 0)) (d (n "metrics") (r "^0.20") (d #t) (k 0)) (d (n "metrics-exporter-prometheus") (r "^0.11") (k 2)) (d (n "metrics-util") (r "^0.14") (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "19lr9lnabr94khbv3vlmdppp16wm5f8gm0ff6r2iz6143f9q8nzr")))

(define-public crate-function-timer-0.4.0 (c (n "function-timer") (v "0.4.0") (d (list (d (n "function-timer-macro") (r "^0.4") (d #t) (k 0)) (d (n "metrics") (r "^0.20") (d #t) (k 0)) (d (n "metrics-exporter-prometheus") (r "^0.11") (k 2)) (d (n "metrics-util") (r "^0.14") (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0mpgcr375psi2swp0p17cn4k11cfy2yd73lbhkigs8nl66dlw03m")))

(define-public crate-function-timer-0.5.0 (c (n "function-timer") (v "0.5.0") (d (list (d (n "function-timer-macro") (r "^0.5") (d #t) (k 0)) (d (n "metrics") (r "^0.20") (d #t) (k 0)) (d (n "metrics-exporter-prometheus") (r "^0.11") (k 2)) (d (n "metrics-util") (r "^0.14") (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "00pfp9v3lxr1x6ckqgg8mdjh8vvvw700icjv2w3f7xbfzi5c4fic")))

(define-public crate-function-timer-0.6.0 (c (n "function-timer") (v "0.6.0") (d (list (d (n "function-timer-macro") (r "^0.5") (d #t) (k 0)) (d (n "metrics") (r "^0.21") (d #t) (k 0)) (d (n "metrics-exporter-prometheus") (r "^0.12") (k 2)) (d (n "metrics-util") (r "^0.15") (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "05qgzmwp1wz6sbf90bzprazkx9lfpaqxnhhnzzzwb92clchmdadv")))

(define-public crate-function-timer-0.7.0 (c (n "function-timer") (v "0.7.0") (d (list (d (n "function-timer-macro") (r "^0.6") (d #t) (k 0)) (d (n "futures-test") (r "^0.3") (d #t) (k 2)) (d (n "metrics") (r "^0.22") (d #t) (k 0)) (d (n "metrics-exporter-prometheus") (r "^0.13") (k 2)) (d (n "metrics-util") (r "^0.16") (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "035ra9l5p4nip09mi9h360al3qzdwdd2w32jvrbysvk0nwa14ifw")))

