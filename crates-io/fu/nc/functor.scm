(define-module (crates-io fu nc functor) #:use-module (crates-io))

(define-public crate-functor-0.1.0 (c (n "functor") (v "0.1.0") (h "0j8blxqlnxcslkbd2gm67hmibpxvnakx3nifv4lkabvixj989jw6")))

(define-public crate-functor-0.1.1 (c (n "functor") (v "0.1.1") (h "0w1acr4ijm7dlvwm33lbhfq6kahlm4mxz5nj5ah2hfwzzp2blffq")))

(define-public crate-functor-0.1.2 (c (n "functor") (v "0.1.2") (d (list (d (n "morphism") (r "^0.4.0") (d #t) (k 0)))) (h "1v17l2mv40zyjw4hk2k2zxhv8r35klmx7127b509l3jfbrgqimwr")))

