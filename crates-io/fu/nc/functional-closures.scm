(define-module (crates-io fu nc functional-closures) #:use-module (crates-io))

(define-public crate-functional-closures-0.1.0 (c (n "functional-closures") (v "0.1.0") (h "1zlhcqx8z7si0isbzfa9xlhbbanhky0vwi57hqf0y4lwg6hgz4w9")))

(define-public crate-functional-closures-0.1.1 (c (n "functional-closures") (v "0.1.1") (h "0nyq0smy17k68gdzj0j7f65rax98ax9xy13jrc6vsr688hs0z4sd")))

(define-public crate-functional-closures-0.1.2 (c (n "functional-closures") (v "0.1.2") (h "1fk6vqlvi99gm43g1v929ibr1hnpg660h7wsd1mi9q36a2p5xc90")))

(define-public crate-functional-closures-0.2.0 (c (n "functional-closures") (v "0.2.0") (h "01avfa8hiqmll7r4yjgqcbiwmb4vp95kaqc62r6g92m8qg0xnjq1")))

(define-public crate-functional-closures-0.3.0 (c (n "functional-closures") (v "0.3.0") (h "1v0xgnzhx9dvx587wf0fvac94v21bl8lxd2hmzg8c0cv5pspcp2z")))

(define-public crate-functional-closures-0.4.0 (c (n "functional-closures") (v "0.4.0") (d (list (d (n "num") (r "^0.4.1") (d #t) (k 0)))) (h "0r6nhbsr29a1nznl454gy6f67fxj7g43b8zdmcxwz744gjbdsin8")))

