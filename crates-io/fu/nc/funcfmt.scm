(define-module (crates-io fu nc funcfmt) #:use-module (crates-io))

(define-public crate-funcfmt-0.1.0 (c (n "funcfmt") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "smartstring") (r "^1.0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0m007mjdpb78l2kaby7a55krvcy7dhv53np8j4543xr2knwwh17b")))

(define-public crate-funcfmt-0.2.0 (c (n "funcfmt") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "smartstring") (r "^1.0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "159mkd7xjbfsvbpv1mg8qhbv86c3nn697qp4d4i7zjsnxr85x7s3")))

(define-public crate-funcfmt-0.3.0 (c (n "funcfmt") (v "0.3.0") (d (list (d (n "fnv") (r "^1.0.7") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 2)) (d (n "proptest") (r "^1.1.0") (d #t) (k 2)) (d (n "smallvec") (r "^1.10.0") (f (quote ("union"))) (d #t) (k 0)) (d (n "smartstring") (r "^1.0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "1hihmghz24f1k0jr5c2dpv0n4vp120kj6vfb3y3kwv6hsynbjv8f") (r "1.57.0")))

