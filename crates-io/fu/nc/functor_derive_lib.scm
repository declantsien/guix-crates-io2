(define-module (crates-io fu nc functor_derive_lib) #:use-module (crates-io))

(define-public crate-functor_derive_lib-0.0.1 (c (n "functor_derive_lib") (v "0.0.1") (h "0xjbp6dbf8rxli0shwr6zc8xfqkjjf82rppvamn47zjxqk81hwmp") (y #t)))

(define-public crate-functor_derive_lib-0.1.0 (c (n "functor_derive_lib") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.33") (f (quote ("full"))) (d #t) (k 0)))) (h "1mdd6sda2ckahmlaz8z8hg78gslzs1dnl2fg2q0fscgqk6rlnhld")))

(define-public crate-functor_derive_lib-0.1.1 (c (n "functor_derive_lib") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.33") (f (quote ("full"))) (d #t) (k 0)))) (h "1pywwwfd8xvqp6cjvr4f0441686rib0wrrzq144bqsxii260dl7w")))

(define-public crate-functor_derive_lib-0.2.0 (c (n "functor_derive_lib") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.33") (f (quote ("full"))) (d #t) (k 0)))) (h "1kq5jk50nf3m9ws2dywvlrihhalsikpgwyqb3fdi2axyb0z878x0")))

(define-public crate-functor_derive_lib-0.2.1 (c (n "functor_derive_lib") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.33") (f (quote ("full"))) (d #t) (k 0)))) (h "00xx54p54c5vkicd15vy2jyc96rvngn0qviazhf7cnwi8pcidfm1")))

(define-public crate-functor_derive_lib-0.2.2 (c (n "functor_derive_lib") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.33") (f (quote ("full"))) (d #t) (k 0)))) (h "0sf0bs34f94yv5k2r85z5b0y41wxj1cvzpwwgn4d141zadpb4bif")))

(define-public crate-functor_derive_lib-0.2.3 (c (n "functor_derive_lib") (v "0.2.3") (d (list (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.33") (f (quote ("full"))) (d #t) (k 0)))) (h "01sh4zqk4sligzv4kimf4c50py6h8rwvfbj8xi12x108fzhwx3aj")))

(define-public crate-functor_derive_lib-0.3.0 (c (n "functor_derive_lib") (v "0.3.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.33") (f (quote ("full" "derive" "extra-traits"))) (d #t) (k 0)))) (h "1c2z62czhxsawlg795gcb9xdfxp7cyf6kqgmmrfkps97cazq1wgg") (y #t)))

(define-public crate-functor_derive_lib-0.3.1 (c (n "functor_derive_lib") (v "0.3.1") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.33") (f (quote ("full" "derive" "extra-traits"))) (d #t) (k 0)))) (h "17ql7gj40qicnmqxyfksbzc2yi2xcrfm1z94r00fkc01lckzqb2l") (y #t)))

(define-public crate-functor_derive_lib-0.3.2 (c (n "functor_derive_lib") (v "0.3.2") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.33") (f (quote ("full" "derive" "extra-traits"))) (d #t) (k 0)))) (h "1f4l2lrmyw2k1q3gk6lvxvxc9z3rmqkvf5gx6pqmzpj61i1id9lh")))

(define-public crate-functor_derive_lib-0.4.0 (c (n "functor_derive_lib") (v "0.4.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.33") (f (quote ("full" "derive" "extra-traits"))) (d #t) (k 0)))) (h "0220hcrb81jj87j66c8qj7lncx0kmf08ydbjyfzd449khxhg3jw8") (y #t)))

(define-public crate-functor_derive_lib-0.4.1 (c (n "functor_derive_lib") (v "0.4.1") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.33") (f (quote ("full"))) (d #t) (k 0)))) (h "1ipqr7irh1vg9braii6v9i875qgszfx4h0zjqyb3qc3i5n0vyiy1")))

(define-public crate-functor_derive_lib-0.4.2 (c (n "functor_derive_lib") (v "0.4.2") (d (list (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.33") (f (quote ("full"))) (d #t) (k 0)))) (h "085b00h3kyisywh480b2hacqk8ldhksgr0r5cz9r9i73x7cgk7pz")))

(define-public crate-functor_derive_lib-0.4.3 (c (n "functor_derive_lib") (v "0.4.3") (d (list (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.33") (f (quote ("full"))) (d #t) (k 0)))) (h "08zasq5xw03205ppwjsxfgv6si0x0436dy9vfycbwqqf00ly0xd8")))

