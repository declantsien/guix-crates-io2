(define-module (crates-io fu nc functional_macro) #:use-module (crates-io))

(define-public crate-functional_macro-0.0.1 (c (n "functional_macro") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0.0") (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "prettyplease") (r "^0.2.15") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits" "parsing"))) (d #t) (k 0)))) (h "0s8667rn46n14bbi7syiab6r770jzqzsqpwm0ycbikgi0m7p33s0")))

(define-public crate-functional_macro-0.0.2 (c (n "functional_macro") (v "0.0.2") (d (list (d (n "anyhow") (r "^1.0.0") (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "prettyplease") (r "^0.2.15") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits" "parsing"))) (d #t) (k 0)))) (h "19m8grq1jy5jmj6f20s4m22bll0q0pc7jzgnh2j8gmfa3bhx0dv9")))

