(define-module (crates-io fu nc function_string_builder) #:use-module (crates-io))

(define-public crate-function_string_builder-1.0.0 (c (n "function_string_builder") (v "1.0.0") (h "024npiabjhshmai7lc7fqvqwb5mpzpqi44hh0dhmbs812v7l7jas")))

(define-public crate-function_string_builder-1.0.1 (c (n "function_string_builder") (v "1.0.1") (h "1g5smgzfhh9b7sckli9gxccxi04bfbcs06nc3n2wmbwi8izpa37i")))

(define-public crate-function_string_builder-1.0.2 (c (n "function_string_builder") (v "1.0.2") (h "11j0j8j7q60292srdl264k5h3sfisi2rmpbcas5nd161s7p1jixh")))

(define-public crate-function_string_builder-1.0.3 (c (n "function_string_builder") (v "1.0.3") (h "02fzwy2ah2nzkxjwk6gn34hwr6f8h9101idnxhsfvyix41qc7sk0")))

