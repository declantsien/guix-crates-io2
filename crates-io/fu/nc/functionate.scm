(define-module (crates-io fu nc functionate) #:use-module (crates-io))

(define-public crate-functionate-0.1.0 (c (n "functionate") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "07w38p3j1p5a4pp5m9a12idwv6vp9i8qs42g1qd9xwynavpzfjz2")))

