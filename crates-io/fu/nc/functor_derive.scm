(define-module (crates-io fu nc functor_derive) #:use-module (crates-io))

(define-public crate-functor_derive-0.0.1 (c (n "functor_derive") (v "0.0.1") (d (list (d (n "functor_derive_lib") (r "^0.0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.33") (f (quote ("full"))) (d #t) (k 0)))) (h "01mdvdxvfw5spxl7kq8kfsq6vgib5jn60dyvdkxhb08zqx2jl0j5") (y #t)))

(define-public crate-functor_derive-0.0.2 (c (n "functor_derive") (v "0.0.2") (d (list (d (n "functor_derive_lib") (r "^0.0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.33") (f (quote ("full"))) (d #t) (k 0)))) (h "0a540j4knqk0awqz3w5dyar556nj9xdp17h2qcbarqbm2qf2bqk0") (y #t)))

(define-public crate-functor_derive-0.1.0 (c (n "functor_derive") (v "0.1.0") (d (list (d (n "functor_derive_lib") (r "^0.1.0") (d #t) (k 0)))) (h "19vb72l6x7grl71b695ys6yjbni6m9dw94ixq5fj3017pyjff108")))

(define-public crate-functor_derive-0.1.1 (c (n "functor_derive") (v "0.1.1") (d (list (d (n "functor_derive_lib") (r "^0.1.1") (d #t) (k 0)))) (h "19230c22i6r7hnpf8cclg7ax0kxq8xqc5v9j61v1gaf1q4w3gxjr")))

(define-public crate-functor_derive-0.2.0 (c (n "functor_derive") (v "0.2.0") (d (list (d (n "functor_derive_lib") (r "^0.2.0") (d #t) (k 0)))) (h "1456qvbva4qnafl98c2vhag6d4rl884fnxmimywz3kj9zcldwppn")))

(define-public crate-functor_derive-0.2.1 (c (n "functor_derive") (v "0.2.1") (d (list (d (n "functor_derive_lib") (r "^0.2.1") (d #t) (k 0)))) (h "0nfhzb6drlrbayjk5xpjdikrszi06zy0ldnx2ajldx1ap3m8f329")))

(define-public crate-functor_derive-0.2.2 (c (n "functor_derive") (v "0.2.2") (d (list (d (n "functor_derive_lib") (r "^0.2.2") (d #t) (k 0)))) (h "02bkmaap6216049d0aq6xhv826qzq3ng661q52ic74cx3rv194sf")))

(define-public crate-functor_derive-0.2.3 (c (n "functor_derive") (v "0.2.3") (d (list (d (n "functor_derive_lib") (r "^0.2.3") (d #t) (k 0)))) (h "1072q4gd14nw3znm8ymvmbrqr2bqp0k92as7wbn80hrfwlp3p5qw")))

(define-public crate-functor_derive-0.3.0 (c (n "functor_derive") (v "0.3.0") (d (list (d (n "functor_derive_lib") (r "^0.3.0") (d #t) (k 0)))) (h "1qr75kys41119vwbw3ap1kwriag01x63hsm4vg3xkr45isnjssgv") (y #t)))

(define-public crate-functor_derive-0.3.1 (c (n "functor_derive") (v "0.3.1") (d (list (d (n "functor_derive_lib") (r "^0.3.1") (d #t) (k 0)))) (h "1nmyyllj2pgl928ywzrd0r1s88mqfmsizp08w94jb3c8mra4hk3b") (y #t)))

(define-public crate-functor_derive-0.3.2 (c (n "functor_derive") (v "0.3.2") (d (list (d (n "functor_derive_lib") (r "^0.3.2") (d #t) (k 0)))) (h "1vg4a5jw37i4px8i3lhr2q3nfrcg1vxs01g7dfikv22rglax840k")))

(define-public crate-functor_derive-0.4.0 (c (n "functor_derive") (v "0.4.0") (d (list (d (n "functor_derive_lib") (r "^0.4.0") (d #t) (k 0)))) (h "1lslmxr0dxmgr1a1gjadgczqxv9gli51a94pnspcg94cv395lmcf") (y #t)))

(define-public crate-functor_derive-0.4.1 (c (n "functor_derive") (v "0.4.1") (d (list (d (n "functor_derive_lib") (r "^0.4.1") (d #t) (k 0)))) (h "1lss6r4w1f46cjn7lnww4617x5ky4pilkv5g1s4dxcn0m8vxqm7p")))

(define-public crate-functor_derive-0.4.2 (c (n "functor_derive") (v "0.4.2") (d (list (d (n "functor_derive_lib") (r "^0.4.2") (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)))) (h "0g1zsika15nszjgf7grr2zfs0pfl9cy6mfjs1lzpkacdkx90m86k")))

(define-public crate-functor_derive-0.4.3 (c (n "functor_derive") (v "0.4.3") (d (list (d (n "functor_derive_lib") (r "=0.4.3") (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)))) (h "0jb7wf78v1ai8307r2m7iinkqi384913x18028rzxkis6m61rn1i")))

