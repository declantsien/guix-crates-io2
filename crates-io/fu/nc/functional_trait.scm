(define-module (crates-io fu nc functional_trait) #:use-module (crates-io))

(define-public crate-functional_trait-0.1.0 (c (n "functional_trait") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (f (quote ("full"))) (d #t) (k 0)))) (h "0bwspxzglg83p2j5jxfk8k91zg9mk2840br6pbv5ww60dwskj1ww")))

(define-public crate-functional_trait-0.1.1 (c (n "functional_trait") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (f (quote ("full"))) (d #t) (k 0)))) (h "03s018930arms145f7s2vhryjh7985y52ndcjvdqz0ri0w76ywxn")))

