(define-module (crates-io fu nc functions_ai) #:use-module (crates-io))

(define-public crate-functions_ai-0.1.0 (c (n "functions_ai") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.27") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "185kjcp2qasnxxmxjazw6i0idszn2bfwp68wmcw0nb6l2fsd4kcq")))

(define-public crate-functions_ai-0.1.1 (c (n "functions_ai") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.27") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "0cjdchxp2v7lq945kz5fr9bb3pb9mngskzx7afbdf88zcwx819sp")))

(define-public crate-functions_ai-0.1.2 (c (n "functions_ai") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.27") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "05z81n7dimkkaaiqrabgjbdj5d88fqgnfr5xp6gqld57cxhpnqgb")))

(define-public crate-functions_ai-0.1.3 (c (n "functions_ai") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.27") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "0qs8rm3dfmcfbylashx2q34c4nbw97n4g3wg4fj51v0vd30rv3ap")))

(define-public crate-functions_ai-0.1.4 (c (n "functions_ai") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.27") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "196wvsyqcdzcdwqg35zr640gmwnink96rz0ywaznxgbxa80106c1")))

