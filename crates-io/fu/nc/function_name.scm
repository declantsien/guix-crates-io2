(define-module (crates-io fu nc function_name) #:use-module (crates-io))

(define-public crate-function_name-0.1.0 (c (n "function_name") (v "0.1.0") (d (list (d (n "function_name-proc-macro") (r "^0.1.0") (d #t) (k 0)))) (h "0rri4ww4x0nz3c1qc1rc08f62nzqlj078wdwg8ggxhy4y19mlsr9") (f (quote (("nightly") ("default"))))))

(define-public crate-function_name-0.1.1 (c (n "function_name") (v "0.1.1") (d (list (d (n "function_name-proc-macro") (r "^0.1.0") (d #t) (k 0)))) (h "1nqs35i61fka2ihv5nlsnmfk2k12wxrw6bs3ib23xa30b2v6pksp") (f (quote (("nightly") ("default"))))))

(define-public crate-function_name-0.2.0 (c (n "function_name") (v "0.2.0") (d (list (d (n "function_name-proc-macro") (r "^0.2.0") (d #t) (k 0)))) (h "1kd0q3alhdan8y8q9pl63dpypiz12hclvwkcmxskmp0lnnlszcl8") (f (quote (("nightly") ("dev" "function_name-proc-macro/test") ("default"))))))

(define-public crate-function_name-0.2.1 (c (n "function_name") (v "0.2.1") (d (list (d (n "function_name-proc-macro") (r "^0.2.1") (d #t) (k 0)))) (h "06bacgzny01lwi0sy7dfbh7lg9nnbdpaqjj79ivnmy06s6j6ryk8") (f (quote (("nightly") ("dev" "function_name-proc-macro/test") ("default"))))))

(define-public crate-function_name-0.2.2-rc1 (c (n "function_name") (v "0.2.2-rc1") (d (list (d (n "function_name-proc-macro") (r "^0.2.2-rc1") (d #t) (k 0)))) (h "1wfrpa2kxn4rd0cqxxwgp4hqmxqw4xw8m072vhw4xhblgf9nxddl") (f (quote (("docs-rs" "better-docs" "dev") ("dev" "function_name-proc-macro/test") ("default") ("better-docs"))))))

(define-public crate-function_name-0.2.2 (c (n "function_name") (v "0.2.2") (d (list (d (n "function_name-proc-macro") (r "^0.2.2") (d #t) (k 0)))) (h "0k5hy51x4f2rpg7bspzwdx3ygf7x31nzvcirmjr26bbiwy0m6dxm") (f (quote (("docs-rs" "better-docs" "dev") ("dev" "function_name-proc-macro/test") ("default") ("better-docs"))))))

(define-public crate-function_name-0.2.3 (c (n "function_name") (v "0.2.3") (d (list (d (n "function_name-proc-macro") (r "^0.2.3") (d #t) (k 0)))) (h "04l5hhgj4gfwh4np7jz4sg205n8b2vvi7nd4zycjnvnwcp335xmy") (f (quote (("docs-rs" "better-docs" "dev") ("dev" "function_name-proc-macro/test") ("default") ("better-docs"))))))

(define-public crate-function_name-0.3.0-rc1 (c (n "function_name") (v "0.3.0-rc1") (d (list (d (n "function_name-proc-macro") (r "^0.3.0-rc1") (d #t) (k 0)))) (h "16fpryvz627p6x4zhw61hv200qbyjb3vbrqxpyhpb0qb1xqvrxl1") (f (quote (("docs-rs" "better-docs") ("default") ("better-docs"))))))

(define-public crate-function_name-0.3.0 (c (n "function_name") (v "0.3.0") (d (list (d (n "function_name-proc-macro") (r "^0.3.0") (d #t) (k 0)))) (h "19wq3s8ajl1p7b01kjb2zwgj3n7rf6pcabp1bw5r82bdi5x5gaxi") (f (quote (("docs-rs" "better-docs") ("default") ("better-docs"))))))

