(define-module (crates-io fu nc function-frame) #:use-module (crates-io))

(define-public crate-function-frame-0.1.0 (c (n "function-frame") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "1d98kr2wl1rkx53fm6k941ca1q8rp2h92c0j2hqj1d0q7dg9zw78")))

(define-public crate-function-frame-0.1.1 (c (n "function-frame") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "1sz1h8941780mc4g7a7hk0iyg66by37i5r3bddrya8iavhlcrny1")))

