(define-module (crates-io fu nc func_core) #:use-module (crates-io))

(define-public crate-func_core-0.1.0 (c (n "func_core") (v "0.1.0") (d (list (d (n "proc-macro2") (r "0.4.*") (d #t) (k 0)) (d (n "quote") (r "0.6.*") (d #t) (k 0)) (d (n "syn") (r "0.15.*") (d #t) (k 0)))) (h "07imabm7q3nqdjlf6hhv6pv9x1d855qc7fn8vwr8l6f2w8jcb0c7")))

