(define-module (crates-io fu nc function_history_backend_thread) #:use-module (crates-io))

(define-public crate-function_history_backend_thread-0.1.0 (c (n "function_history_backend_thread") (v "0.1.0") (d (list (d (n "git_function_history") (r "^0.5.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0whb8r11ldrv7qggyp14b6bd7mz0vn7lss2870fg2psm6qk4hqgr")))

(define-public crate-function_history_backend_thread-0.2.0 (c (n "function_history_backend_thread") (v "0.2.0") (d (list (d (n "git_function_history") (r "^0.6.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0da2x6qy4j3cn965zm1l6iyrc48ghrr6zgwy0bncha01xg058r8y")))

(define-public crate-function_history_backend_thread-0.2.1 (c (n "function_history_backend_thread") (v "0.2.1") (d (list (d (n "git_function_history") (r "^0.6.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0v1cnh9glflsybhnm82z70pln2px0p25vrc9kcj7bccwh7vzxhm9")))

(define-public crate-function_history_backend_thread-0.2.2 (c (n "function_history_backend_thread") (v "0.2.2") (d (list (d (n "git_function_history") (r "^0.6.2") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "00mggpjq5hyf9z42plyf43bnqscb07arv4y39jh7n9mkl0vpqwiv") (f (quote (("parallel" "git_function_history/parallel") ("not-parallel") ("default" "parallel"))))))

(define-public crate-function_history_backend_thread-0.3.0 (c (n "function_history_backend_thread") (v "0.3.0") (d (list (d (n "git_function_history") (r "^0.7.0") (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "1j0kzsv9jzvihsi19lb7p7q62wil5zbk0h5vyf3qhqzg1qimd0wg") (f (quote (("unstable" "git_function_history/unstable") ("parallel" "git_function_history/parallel") ("default" "parallel"))))))

(define-public crate-function_history_backend_thread-0.3.1 (c (n "function_history_backend_thread") (v "0.3.1") (d (list (d (n "git_function_history") (r "^0.7.1") (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "1hwjwcq3jmlpiz9capi6wlcinv3nxybq28661jjrsmlpwxav5bl0") (f (quote (("parallel" "git_function_history/parallel") ("default" "parallel")))) (r "1.70.0")))

