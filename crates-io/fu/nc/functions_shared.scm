(define-module (crates-io fu nc functions_shared) #:use-module (crates-io))

(define-public crate-functions_shared-0.1.0 (c (n "functions_shared") (v "0.1.0") (h "0i3v497v2c3abxf2sc2095xpgy91a79vsclkgq82l8rcxgrkcz45")))

(define-public crate-functions_shared-0.1.1 (c (n "functions_shared") (v "0.1.1") (h "1indrf87rg8lbg4a86l1614874qa34ggf09y7pyq739kbwfi9miw")))

