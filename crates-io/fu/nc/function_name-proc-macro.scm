(define-module (crates-io fu nc function_name-proc-macro) #:use-module (crates-io))

(define-public crate-function_name-proc-macro-0.1.0 (c (n "function_name-proc-macro") (v "0.1.0") (d (list (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "syn") (r "^0.15.35") (f (quote ("full"))) (d #t) (k 0)))) (h "0m52jnkmg8i740shxyfh8bw2ck089fkvw166i83cjhn4mg4p67pd") (f (quote (("extra-traits" "syn/extra-traits"))))))

(define-public crate-function_name-proc-macro-0.2.0 (c (n "function_name-proc-macro") (v "0.2.0") (d (list (d (n "proc-macro-crate") (r "^0.1.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "syn") (r "^0.15.35") (f (quote ("full"))) (d #t) (k 0)))) (h "0zacg3517gmqvglwnj3agp18f2m5j9mqc7hqf9wmvxnjav9si437") (f (quote (("test") ("extra-traits" "syn/extra-traits") ("default"))))))

(define-public crate-function_name-proc-macro-0.2.1 (c (n "function_name-proc-macro") (v "0.2.1") (d (list (d (n "proc-macro-crate") (r "^0.1.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "syn") (r "^0.15.35") (f (quote ("full"))) (d #t) (k 0)))) (h "00ji9k2sfryhd4f8fhn3cdsagvwy4snh6w0dbpahbkb475m3z881") (f (quote (("test") ("extra-traits" "syn/extra-traits") ("default"))))))

(define-public crate-function_name-proc-macro-0.2.2-rc1 (c (n "function_name-proc-macro") (v "0.2.2-rc1") (d (list (d (n "proc-macro-crate") (r "^0.1.4") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0gj9ickvi7lmsdgxkvs5rr5isx8h6pd9gh3zi3jm1a3x1pr5z018") (f (quote (("test") ("extra-traits" "syn/extra-traits") ("default"))))))

(define-public crate-function_name-proc-macro-0.2.2 (c (n "function_name-proc-macro") (v "0.2.2") (d (list (d (n "proc-macro-crate") (r "^0.1.4") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1gkr813ga7zifhqiv6ppwkzga3ayxglichlv7ai6jds8lcldbk8s") (f (quote (("test") ("extra-traits" "syn/extra-traits") ("default"))))))

(define-public crate-function_name-proc-macro-0.2.3 (c (n "function_name-proc-macro") (v "0.2.3") (d (list (d (n "proc-macro-crate") (r "^0.1.4") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("full"))) (d #t) (k 0)))) (h "12ijg105pg4nkdj6p8zvpip49bzdc4xh24689zvcz4hghww257an") (f (quote (("test") ("extra-traits" "syn/extra-traits") ("default"))))))

(define-public crate-function_name-proc-macro-0.3.0-rc1 (c (n "function_name-proc-macro") (v "0.3.0-rc1") (h "0sj04zsijsvn9imrnhj1mj0q7cxircicrfz0j10wwnwlw7al2f6g")))

(define-public crate-function_name-proc-macro-0.3.0 (c (n "function_name-proc-macro") (v "0.3.0") (h "0cvkp002v6zzphrv0s9vgq68vclfkv4sni4mznh6gp8lwghn8d37")))

