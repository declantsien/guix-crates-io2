(define-module (crates-io fu nc funcy) #:use-module (crates-io))

(define-public crate-funcy-0.1.0 (c (n "funcy") (v "0.1.0") (h "1783cc569dwxfpac154799y80wgalns4zmwz8f3fvrybp24qsf9v")))

(define-public crate-funcy-1.0.0 (c (n "funcy") (v "1.0.0") (h "0ln3022n6q3j3pdh8wjcyxfpsfm8a04vxy7gidaswa0xr1m8yzh3")))

