(define-module (crates-io fu nc function_group) #:use-module (crates-io))

(define-public crate-function_group-0.1.0 (c (n "function_group") (v "0.1.0") (h "1hg2dpiqqd2yhri3wzhfs5s6paazs54d4bdxn6r5ajnf31xf0vd4")))

(define-public crate-function_group-0.1.1 (c (n "function_group") (v "0.1.1") (h "0pm95qfv081w3ips71hy2xs06zi6c4lw844madya4kggza54qz40")))

(define-public crate-function_group-0.1.2 (c (n "function_group") (v "0.1.2") (h "03x3cpkyj0sdlsr3rplb20x3c4fay6qk7xs1nq6m9976288f7m63")))

(define-public crate-function_group-0.2.0 (c (n "function_group") (v "0.2.0") (d (list (d (n "quote") (r "0.6.*") (d #t) (k 0)) (d (n "syn") (r "0.15.*") (f (quote ("full"))) (d #t) (k 0)))) (h "08phqzblxg2jfkkgfccd13glgps348qjiwmsrfj6cj7hj97h7rpj")))

(define-public crate-function_group-0.2.1 (c (n "function_group") (v "0.2.1") (d (list (d (n "quote") (r "0.6.*") (d #t) (k 0)) (d (n "syn") (r "0.15.*") (f (quote ("full"))) (d #t) (k 0)))) (h "157qznrwvrrcw63vbpnk29qrcaaak91c9321dgi23wgp18465ayk")))

(define-public crate-function_group-0.2.2 (c (n "function_group") (v "0.2.2") (d (list (d (n "quote") (r "0.6.*") (d #t) (k 0)) (d (n "syn") (r "0.15.*") (f (quote ("full"))) (d #t) (k 0)))) (h "0aadg04nmfspbnc8szb1cmk5p52rrpbvn4hlqzb2yqnk1qsxk2ws")))

(define-public crate-function_group-0.2.3 (c (n "function_group") (v "0.2.3") (d (list (d (n "quote") (r "0.6.*") (d #t) (k 0)) (d (n "syn") (r "0.15.*") (f (quote ("full"))) (d #t) (k 0)))) (h "1k66qrn9n9y4afmnhb9bs393jqik5pl47v82da9inkr614ysf74y")))

(define-public crate-function_group-0.2.4 (c (n "function_group") (v "0.2.4") (d (list (d (n "quote") (r "0.6.*") (d #t) (k 0)) (d (n "syn") (r "0.15.*") (f (quote ("full"))) (d #t) (k 0)))) (h "0hj7gbis5nncj518fkb5d1q2h5nycx1sgrmnzj5narj87h5ckvw2")))

