(define-module (crates-io fu nc funcmap) #:use-module (crates-io))

(define-public crate-funcmap-0.1.0 (c (n "funcmap") (v "0.1.0") (d (list (d (n "funcmap_derive") (r "=0.1.0") (d #t) (k 0)))) (h "1sgp8a6fjaz4vcz1c6a59v57rm9vyjk0vm5gxqzg302637lifar3") (f (quote (("std" "alloc") ("default" "std") ("alloc")))) (r "1.56")))

(define-public crate-funcmap-0.1.1 (c (n "funcmap") (v "0.1.1") (d (list (d (n "funcmap_derive") (r "=0.1.1") (d #t) (k 0)))) (h "1ybqswyc18pp7sgixfxz6cg8zdj76gc8z976rhll42viwp7r9zgr") (f (quote (("std" "alloc") ("default" "std") ("alloc")))) (r "1.56")))

(define-public crate-funcmap-0.1.2 (c (n "funcmap") (v "0.1.2") (d (list (d (n "funcmap_derive") (r "=0.1.2") (d #t) (k 0)))) (h "1xr189y87aj2zswscrm4pz0yhvgsq8v6nz88d2gg2fy44g95nvaf") (f (quote (("std" "alloc") ("default" "std") ("alloc")))) (r "1.56")))

(define-public crate-funcmap-0.1.3 (c (n "funcmap") (v "0.1.3") (d (list (d (n "funcmap_derive") (r "=0.1.3") (d #t) (k 0)))) (h "0wvz75nhfkp3w08qfsp6v0qql3q69w6fdfs625p1dhmrsfz8dbiv") (f (quote (("std" "alloc") ("default" "std") ("alloc")))) (r "1.56")))

(define-public crate-funcmap-0.1.4 (c (n "funcmap") (v "0.1.4") (d (list (d (n "funcmap_derive") (r "=0.1.4") (d #t) (k 0)))) (h "0xjqlybhwk3fkih7yk5yj64x24wyyfppywklh1h77q6ndirgbjzg") (f (quote (("std" "alloc") ("default" "std") ("alloc")))) (r "1.64")))

