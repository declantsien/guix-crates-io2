(define-module (crates-io fu nc functils) #:use-module (crates-io))

(define-public crate-functils-0.0.1 (c (n "functils") (v "0.0.1") (h "106c1kpjrms259sr4kacsv3yfpb6cz1y2xkllzddxgkqn5f48dgy") (y #t)))

(define-public crate-functils-0.0.2 (c (n "functils") (v "0.0.2") (d (list (d (n "kinder") (r "^0.1.1") (d #t) (k 0)))) (h "0ppxq9p5avc440ca4a2cq4ji3bd4srgiww66403ppr6p1b7472kw") (y #t)))

