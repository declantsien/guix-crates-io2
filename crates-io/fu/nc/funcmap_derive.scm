(define-module (crates-io fu nc funcmap_derive) #:use-module (crates-io))

(define-public crate-funcmap_derive-0.1.0 (c (n "funcmap_derive") (v "0.1.0") (d (list (d (n "indexmap") (r "^1") (f (quote ("std"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("extra-traits" "fold" "full" "visit"))) (d #t) (k 0)))) (h "0hmh3z445qqnxyiszywf53f93jmb30a6c4i6q2jm9k5mk301m3fw") (r "1.56")))

(define-public crate-funcmap_derive-0.1.1 (c (n "funcmap_derive") (v "0.1.1") (d (list (d (n "indexmap") (r "^1") (f (quote ("std"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("extra-traits" "fold" "full" "visit"))) (d #t) (k 0)))) (h "0m6qj5l2npb2swcjzbicdf6pzzqimrjj1zqk9kif1zk6gyjkv290") (r "1.56")))

(define-public crate-funcmap_derive-0.1.2 (c (n "funcmap_derive") (v "0.1.2") (d (list (d (n "indexmap") (r "^1") (f (quote ("std"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("extra-traits" "fold" "full" "visit"))) (d #t) (k 0)))) (h "05idpp4v1yzh9sn9j7rs7nhrq6i82kgdb9lx6zcr7rfhjpdaxirm") (r "1.56")))

(define-public crate-funcmap_derive-0.1.3 (c (n "funcmap_derive") (v "0.1.3") (d (list (d (n "indexmap") (r "^1") (f (quote ("std"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits" "fold" "full" "visit"))) (d #t) (k 0)))) (h "1sqks08fpyyc089c9kqjwx519lh7xb48k6jaax14g4810v4yylzg") (r "1.56")))

(define-public crate-funcmap_derive-0.1.4 (c (n "funcmap_derive") (v "0.1.4") (d (list (d (n "indexmap") (r "^2") (f (quote ("std"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits" "fold" "full" "visit"))) (d #t) (k 0)))) (h "0gwm51v2qyc3vqxwfwa2swb9wx1mv3r1hfd2cq226w3zbcglw21n") (r "1.64")))

