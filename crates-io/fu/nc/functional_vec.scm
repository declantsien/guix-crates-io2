(define-module (crates-io fu nc functional_vec) #:use-module (crates-io))

(define-public crate-functional_vec-0.1.0 (c (n "functional_vec") (v "0.1.0") (h "07zz2rlhvgrzmqhrbg5axb6zx4dsgb1jh0sga8b8j0f5ni1mkczj")))

(define-public crate-functional_vec-0.2.0 (c (n "functional_vec") (v "0.2.0") (d (list (d (n "smallvec") (r "^1.10.0") (o #t) (d #t) (k 0)))) (h "1smb7p6yvsh6649w59wn0n8v477rj60pq4rxnq2yliqbvl01i6py") (f (quote (("vecdeque") ("vec") ("default" "vec" "vecdeque")))) (s 2) (e (quote (("smallvec" "dep:smallvec"))))))

