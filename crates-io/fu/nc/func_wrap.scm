(define-module (crates-io fu nc func_wrap) #:use-module (crates-io))

(define-public crate-func_wrap-0.1.0-rc1 (c (n "func_wrap") (v "0.1.0-rc1") (d (list (d (n "proc-macro2") (r "1.0.*") (d #t) (k 0)) (d (n "quote") (r "1.0.*") (d #t) (k 0)) (d (n "syn") (r "1.0.*") (f (quote ("full"))) (d #t) (k 0)))) (h "1pxbs6myn6bjyhgpj4k8l0ymkw5zckv7ldrgx3gzy2yd6451l1fi")))

(define-public crate-func_wrap-0.1.0 (c (n "func_wrap") (v "0.1.0") (d (list (d (n "proc-macro2") (r "1.0.*") (d #t) (k 0)) (d (n "quote") (r "1.0.*") (d #t) (k 0)) (d (n "syn") (r "1.0.*") (f (quote ("full"))) (d #t) (k 0)))) (h "0yylq3rfpqdd0pk0a6mdvrc9fjdgf2rabf6dfzv8s0yfa9c9019r")))

(define-public crate-func_wrap-0.1.1 (c (n "func_wrap") (v "0.1.1") (d (list (d (n "proc-macro2") (r "1.0.*") (d #t) (k 0)) (d (n "quote") (r "1.0.*") (d #t) (k 0)) (d (n "syn") (r "1.0.*") (f (quote ("full"))) (d #t) (k 0)))) (h "03knl6ny17dhxb70fxa9nhi7iq0x9n6l8m0radvwkpqzxakjfnij")))

(define-public crate-func_wrap-0.1.2 (c (n "func_wrap") (v "0.1.2") (d (list (d (n "proc-macro2") (r "1.0.*") (d #t) (k 0)) (d (n "quote") (r "1.0.*") (d #t) (k 0)) (d (n "syn") (r "1.0.*") (f (quote ("full"))) (d #t) (k 0)))) (h "19k0ad7pc8aqrlxa9nm6nwblqmg05nn2h1lbn9mfdmjgayp79iis")))

(define-public crate-func_wrap-0.1.3 (c (n "func_wrap") (v "0.1.3") (d (list (d (n "proc-macro2") (r ">=1.0.0, <1.1.0") (d #t) (k 0)) (d (n "quote") (r ">=1.0.0, <1.1.0") (d #t) (k 0)) (d (n "syn") (r ">=1.0.0, <1.1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "01gcbvx4r6qb10w624dhj4fngd8ikwljx4n76sycr8l33gi97596")))

