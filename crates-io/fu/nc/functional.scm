(define-module (crates-io fu nc functional) #:use-module (crates-io))

(define-public crate-functional-0.0.0 (c (n "functional") (v "0.0.0") (h "0gpp3p62f2yg8caffmsvccx1v3ly4vmy03qvx8mi51hwxfnpxadj")))

(define-public crate-functional-0.0.1 (c (n "functional") (v "0.0.1") (h "0zgbkbxhg4qnfbk7bv8c1fy3agf9mp1q15zgj580may1qr0sxx3k")))

(define-public crate-functional-0.0.2 (c (n "functional") (v "0.0.2") (h "11x7jxajv51n2ic5pm7lb61inc40xj24m25vj3kjbrhaf78ggk5z")))

(define-public crate-functional-0.0.3 (c (n "functional") (v "0.0.3") (h "0gdashsmwyy0zqxmw7pk8vhgcmj5vhyn9ccqjwlmn2kpvgfj1dvm")))

(define-public crate-functional-0.0.4 (c (n "functional") (v "0.0.4") (h "0lv2k6rmap4mwrgszj9y6gxa8dc9pml0kgrcybbnva4h32l382d7")))

(define-public crate-functional-0.0.5 (c (n "functional") (v "0.0.5") (h "1s01dkv5vjr288mab0yr64v8ypwqkka4i32b00kfnfw66sdwbk1a")))

(define-public crate-functional-0.0.7 (c (n "functional") (v "0.0.7") (h "08z7fwm7x877iq72smxzf3gs84f2l0vgwnibvvpjg026ik93y149")))

