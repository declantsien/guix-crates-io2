(define-module (crates-io fu zz fuzzt) #:use-module (crates-io))

(define-public crate-fuzzt-0.1.0 (c (n "fuzzt") (v "0.1.0") (d (list (d (n "rstest") (r "^0.18.2") (d #t) (k 2)))) (h "0p0hwvdc5iygx0i0mashh06azpq9701jb086bls47b6x83rc6id4") (r "1.70.0")))

(define-public crate-fuzzt-0.2.0 (c (n "fuzzt") (v "0.2.0") (d (list (d (n "rstest") (r "^0.18.2") (d #t) (k 2)))) (h "07jya2ghfiw4dl5vsb2byyqi62j43fwjzsjxx378j32xd53cblh1") (r "1.70.0")))

(define-public crate-fuzzt-0.3.0 (c (n "fuzzt") (v "0.3.0") (d (list (d (n "rstest") (r "^0.18.2") (d #t) (k 2)))) (h "1l43qwmj38vvj74jigk8xzgck3wg1q3admpilj3zp6wvan1aq18d") (f (quote (("sorensen_dice") ("optimal_string_alignment") ("levenshtein") ("jaro") ("hamming") ("gestalt") ("default" "damerau_levenshtein" "gestalt" "hamming" "jaro" "levenshtein" "optimal_string_alignment" "sorensen_dice") ("damerau_levenshtein")))) (r "1.70.0")))

(define-public crate-fuzzt-0.3.1 (c (n "fuzzt") (v "0.3.1") (d (list (d (n "rstest") (r "^0.18.2") (d #t) (k 2)))) (h "1ihqwl2nr4jdl9kdpn25y97yxd6s7ml0kdjzbrv3la22zb8g65ca") (f (quote (("sorensen_dice") ("optimal_string_alignment") ("levenshtein") ("jaro") ("hamming") ("default" "damerau_levenshtein" "hamming" "jaro" "levenshtein" "optimal_string_alignment" "sorensen_dice") ("damerau_levenshtein")))) (r "1.70.0")))

