(define-module (crates-io fu zz fuzzy_dbscan) #:use-module (crates-io))

(define-public crate-fuzzy_dbscan-0.1.0 (c (n "fuzzy_dbscan") (v "0.1.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "1af84jv199qfyb41j9f49ag0pmhz0s7nhq22hkrlskiqj5nnmjnx")))

(define-public crate-fuzzy_dbscan-0.2.0 (c (n "fuzzy_dbscan") (v "0.2.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "svg") (r "^0.5") (d #t) (k 2)))) (h "1b12z8rzv9jv3bfyvq48iimzn9gmdc0mmv0bn0k7irbj4plnqkaa")))

(define-public crate-fuzzy_dbscan-0.3.0 (c (n "fuzzy_dbscan") (v "0.3.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "serde") (r "^1.0.59") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.59") (d #t) (k 0)) (d (n "utils") (r "0.*") (d #t) (k 2)) (d (n "wasm-bindgen") (r "^0.2.28") (f (quote ("serde-serialize"))) (d #t) (k 0)))) (h "0g8aj0ch4i1ka3h12nfninlcb9nrp1l4f0acq3q260c84kdrfp1g")))

