(define-module (crates-io fu zz fuzzy_match_flex) #:use-module (crates-io))

(define-public crate-fuzzy_match_flex-0.1.0 (c (n "fuzzy_match_flex") (v "0.1.0") (d (list (d (n "regex") (r "^1.8.3") (d #t) (k 0)))) (h "12rmv0vdh6k44izg90y5hk5s89nr2in3gqnbk1yhl9akyh30c3py") (y #t)))

(define-public crate-fuzzy_match_flex-0.1.1 (c (n "fuzzy_match_flex") (v "0.1.1") (d (list (d (n "regex") (r "^1.8.3") (d #t) (k 0)))) (h "1s4740dddxv04fzvib1f42plcfj808axdk2sh756wpq5s8hfj72s")))

(define-public crate-fuzzy_match_flex-0.1.2 (c (n "fuzzy_match_flex") (v "0.1.2") (d (list (d (n "regex") (r "^1.8.3") (d #t) (k 0)))) (h "19nlw2pa5drfdyh5zg91fygb4qk591yjkpv5w4zha05mlll5m4dx")))

