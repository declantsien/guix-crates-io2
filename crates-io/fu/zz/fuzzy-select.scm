(define-module (crates-io fu zz fuzzy-select) #:use-module (crates-io))

(define-public crate-fuzzy-select-0.1.0 (c (n "fuzzy-select") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "nucleo") (r "^0.3.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.10.1") (d #t) (k 0)))) (h "13a2gis2zqyi9nxb3kidrkq6iqm43aanz5598wipjzgxghk015qs") (r "1.75.0")))

(define-public crate-fuzzy-select-0.1.1 (c (n "fuzzy-select") (v "0.1.1") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "nucleo") (r "^0.3.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.10.1") (d #t) (k 0)))) (h "1xz44j9hl4gdaqvrcx38vshrx7l8rsmi8iq6db8wf9mvgijlbvvf") (r "1.75.0")))

