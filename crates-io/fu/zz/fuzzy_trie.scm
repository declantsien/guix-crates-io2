(define-module (crates-io fu zz fuzzy_trie) #:use-module (crates-io))

(define-public crate-fuzzy_trie-0.2.0 (c (n "fuzzy_trie") (v "0.2.0") (d (list (d (n "levenshtein_automata") (r "^0.1.1") (d #t) (k 0)))) (h "0pl5zqgibrdnxrhm12p30p512vqshg0n2wvy9d38bisj5wjkmmrb")))

(define-public crate-fuzzy_trie-0.2.1 (c (n "fuzzy_trie") (v "0.2.1") (d (list (d (n "levenshtein_automata") (r "^0.1.1") (d #t) (k 0)))) (h "1jn3gql20f3magqis7r79pid93ilqrhmy8937dv2zr02jfs4xf8g")))

(define-public crate-fuzzy_trie-0.2.2 (c (n "fuzzy_trie") (v "0.2.2") (d (list (d (n "levenshtein_automata") (r "^0.1.1") (d #t) (k 0)))) (h "1hxdmdbj1gj033aqyvhmh4xi477jk2ap7sm0xfg4bh8jwlklcdgc")))

(define-public crate-fuzzy_trie-0.3.0 (c (n "fuzzy_trie") (v "0.3.0") (d (list (d (n "levenshtein_automata") (r "^0.1.1") (d #t) (k 0)))) (h "0a8zbi14x9w8j55bd41v46d3sby93rcnpx2czdbhpb5ijj04yy3q")))

(define-public crate-fuzzy_trie-0.4.0 (c (n "fuzzy_trie") (v "0.4.0") (d (list (d (n "levenshtein_automata") (r "^0.1.1") (d #t) (k 0)))) (h "1dxaj5xbipx7v8jydah0ir3brdvp0ng952sppjfy2iadcjmyda5s")))

(define-public crate-fuzzy_trie-0.5.0 (c (n "fuzzy_trie") (v "0.5.0") (d (list (d (n "levenshtein_automata") (r "^0.1.1") (d #t) (k 0)))) (h "1rd8wirii0yidmvbzwkb5kghxgbz8srlnbswxryanqhx6bmrz3ik")))

(define-public crate-fuzzy_trie-0.6.0 (c (n "fuzzy_trie") (v "0.6.0") (d (list (d (n "levenshtein_automata") (r "^0.1.1") (d #t) (k 0)))) (h "0g4bx3b71r4kvf3gcambznbgf3by6y3cinkvn5hsvi5vslzdgmni")))

(define-public crate-fuzzy_trie-1.0.0 (c (n "fuzzy_trie") (v "1.0.0") (d (list (d (n "levenshtein_automata") (r "^0.2.0") (d #t) (k 0)))) (h "04l7fcbsybmcnngv7x651mgw2k9r30ksq9lnm3d8p9j76y13viqy")))

(define-public crate-fuzzy_trie-1.0.1 (c (n "fuzzy_trie") (v "1.0.1") (d (list (d (n "levenshtein_automata") (r "^0.2.0") (d #t) (k 0)))) (h "0qsz8j5y63kgqxfmgvcmb9fqjarhbi4j6sg7mb3b6l05rq6ib924")))

(define-public crate-fuzzy_trie-1.1.0 (c (n "fuzzy_trie") (v "1.1.0") (d (list (d (n "levenshtein_automata") (r "^0.2.0") (d #t) (k 0)))) (h "067r8ypd1qfcam6xlqlkyhrfads74wj905m5r5dlb9rdy09amxr0")))

(define-public crate-fuzzy_trie-1.1.1 (c (n "fuzzy_trie") (v "1.1.1") (d (list (d (n "levenshtein_automata") (r "^0.2.0") (d #t) (k 0)))) (h "1y99salmzfzsps4ry9743g6xz948cni8abwn7j5vkglcg7hqhi38")))

(define-public crate-fuzzy_trie-1.2.0 (c (n "fuzzy_trie") (v "1.2.0") (d (list (d (n "levenshtein_automata") (r "^0.2.1") (d #t) (k 0)))) (h "0hbbm8zd2a46gwzxp4i8hq7jv6gk1bapk68ay1xr8fqarxdwyfnn")))

