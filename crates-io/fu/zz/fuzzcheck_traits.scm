(define-module (crates-io fu zz fuzzcheck_traits) #:use-module (crates-io))

(define-public crate-fuzzcheck_traits-0.3.0 (c (n "fuzzcheck_traits") (v "0.3.0") (h "11jp2jndsz67bjvqnkwjgr4h7xy2qy6flmg1h2832zzcn93qrhrv")))

(define-public crate-fuzzcheck_traits-0.4.0 (c (n "fuzzcheck_traits") (v "0.4.0") (h "1ixdi2lg0ic6affkd3qfmi88waq2rp4spfdpvlvh03q3qdhw91xi")))

(define-public crate-fuzzcheck_traits-0.5.0 (c (n "fuzzcheck_traits") (v "0.5.0") (h "0p82srfqcdy3iscqfr561axfp3cly63kh4cxa576d65sbj7kmd5i")))

(define-public crate-fuzzcheck_traits-0.6.0 (c (n "fuzzcheck_traits") (v "0.6.0") (h "0411ks006djr7rs34f4kqiiz3cp0rjp0m2d05jzgdvgvy4rsvrqq")))

