(define-module (crates-io fu zz fuzzydate) #:use-module (crates-io))

(define-public crate-fuzzydate-0.1.0 (c (n "fuzzydate") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "0vkcd0mqchigd6x535wpyl0j7l0mx7ja6kci50afdxymvk2s64av")))

(define-public crate-fuzzydate-0.1.1 (c (n "fuzzydate") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "0w2hlmxp4ldp2sj6jc74kq6cpf72d9icma57wqippjm8mnn22ahg")))

(define-public crate-fuzzydate-0.1.2 (c (n "fuzzydate") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "15j29n0xdvr97bx793h1msil58njyr93z0h87j3k9z6bqrhd06jq")))

(define-public crate-fuzzydate-0.1.3 (c (n "fuzzydate") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "0il30pmfj0j5q70j3agqvh1pzv47svrpcjbnx3g0rd1x0vsz1pyg")))

(define-public crate-fuzzydate-0.1.4 (c (n "fuzzydate") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "1v39z4a32s53p304sxvhdknd29qcj5y6jmak3rqyqjfli5fc61dp") (y #t)))

(define-public crate-fuzzydate-0.1.5 (c (n "fuzzydate") (v "0.1.5") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "1iggc7kvkjihyvjfgp72j3802gdr5q83dik3khsprz2bz98ilnnn")))

(define-public crate-fuzzydate-0.2.0 (c (n "fuzzydate") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "06np4w79v23lv6xr7vi9hgvrbbb78zs5wpdwwfb10bvjpqgis26l")))

(define-public crate-fuzzydate-0.2.1 (c (n "fuzzydate") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "04y8agz1s6xm6glrv7y5dfva0jdmkzw5r2k4q83y6m4zwf667k0h")))

(define-public crate-fuzzydate-0.2.2 (c (n "fuzzydate") (v "0.2.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0745plydzhsyppcnwkwh73yj2pzgvahj74w2mmd6a37lq5fg6rbj")))

