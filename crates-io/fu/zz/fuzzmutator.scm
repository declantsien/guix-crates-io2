(define-module (crates-io fu zz fuzzmutator) #:use-module (crates-io))

(define-public crate-fuzzmutator-0.1.0 (c (n "fuzzmutator") (v "0.1.0") (d (list (d (n "rand") (r "^0.3.0") (d #t) (k 0)))) (h "04n2jv1g64dv56icrir9b7ra46ybpvl83hx9whqv5kgmi708jz6i")))

(define-public crate-fuzzmutator-0.1.1 (c (n "fuzzmutator") (v "0.1.1") (d (list (d (n "rand") (r "^0.3.0") (d #t) (k 0)))) (h "1k8lqbmvfx1aam6x23q69q83pvdxnhzsfkaljga0228r5s8g562k")))

(define-public crate-fuzzmutator-0.1.2 (c (n "fuzzmutator") (v "0.1.2") (d (list (d (n "rand") (r "^0.3.0") (d #t) (k 0)))) (h "1925pqdqycvpmq6s6jk4ii1b7khyw501kfh4nvxcd6665vpbng7p")))

(define-public crate-fuzzmutator-0.1.3 (c (n "fuzzmutator") (v "0.1.3") (d (list (d (n "rand") (r "^0.3.0") (d #t) (k 0)))) (h "0bibfca8cw3wrfhr2fb6hmpgbhzkvkng7frzpvdjz3sk3q4hq8cv")))

(define-public crate-fuzzmutator-0.1.4 (c (n "fuzzmutator") (v "0.1.4") (d (list (d (n "rand") (r "^0.3.0") (d #t) (k 0)))) (h "106lyc1g8dl5c7j5h2y2lajzm6zwn20arq31apy063gqg33a6jy7")))

(define-public crate-fuzzmutator-0.1.5 (c (n "fuzzmutator") (v "0.1.5") (d (list (d (n "rand") (r "^0.3.0") (d #t) (k 0)))) (h "033sh7xrzx1gbibd04257imsa2bqd0vaficwgr3dj29vs1wzqc6a")))

(define-public crate-fuzzmutator-0.1.6 (c (n "fuzzmutator") (v "0.1.6") (d (list (d (n "rand") (r "^0.3.0") (d #t) (k 0)))) (h "1dpaqi9815iswx2bdgblj5hpn4yn6dnvf5hpqmqdrx04n9cy2051")))

(define-public crate-fuzzmutator-0.1.7 (c (n "fuzzmutator") (v "0.1.7") (d (list (d (n "rand") (r "^0.3.0") (d #t) (k 0)))) (h "0mbl3dch1lv322rrfwa1jb6yaq5r7x8xfd1vp9zkl4r3vn27km7n")))

(define-public crate-fuzzmutator-0.1.8 (c (n "fuzzmutator") (v "0.1.8") (d (list (d (n "rand") (r "^0.3.0") (d #t) (k 0)))) (h "07ldqf1ibsfpqii4b0lmv58yh8jngh0m641pab98abvn0qbqjchw")))

(define-public crate-fuzzmutator-0.1.9 (c (n "fuzzmutator") (v "0.1.9") (d (list (d (n "rand") (r "^0.4.2") (d #t) (k 0)))) (h "1ap5pi2vviiahpi1000isj662q137fgr2kb4y5vzl1qlfsywmwqi")))

(define-public crate-fuzzmutator-0.2.0 (c (n "fuzzmutator") (v "0.2.0") (d (list (d (n "rand") (r "^0.4.2") (d #t) (k 0)))) (h "01ir31q0g8lfp4lv8y6dfaqjcb65gdbwdbbnpkyp4km48scqz7m8")))

(define-public crate-fuzzmutator-0.2.1 (c (n "fuzzmutator") (v "0.2.1") (d (list (d (n "rand") (r "^0.4.2") (d #t) (k 0)))) (h "1mn4yhka81dcf199rijpk7x32bii83ffplam8x6ja5i975vvxn44")))

