(define-module (crates-io fu zz fuzzyfocus) #:use-module (crates-io))

(define-public crate-fuzzyfocus-0.1.0 (c (n "fuzzyfocus") (v "0.1.0") (d (list (d (n "fuzzy-matcher") (r "^0.3.4") (d #t) (k 0)) (d (n "rustyline") (r "^6.1.2") (d #t) (k 0)) (d (n "rustyline-derive") (r "^0.3.1") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winuser" "minwindef" "windef" "processthreadsapi"))) (d #t) (k 0)))) (h "1kzsw5z7fync8xlmn0sx01qlrrfqz5v2gphhswf58h7a72k6811d")))

