(define-module (crates-io fu zz fuzzcheck_serializer) #:use-module (crates-io))

(define-public crate-fuzzcheck_serializer-0.1.0 (c (n "fuzzcheck_serializer") (v "0.1.0") (d (list (d (n "fuzzcheck") (r "^0.1.0") (d #t) (k 0)))) (h "1ncvvjxckcj2zc5kgf7dxgy6v45mvn9z0riq6sqayrk9d1qpl0xw") (y #t)))

(define-public crate-fuzzcheck_serializer-0.1.1 (c (n "fuzzcheck_serializer") (v "0.1.1") (d (list (d (n "fuzzcheck") (r "^0.1.1") (d #t) (k 0)))) (h "1khx30g2hfmsj6cfq6ch7fsvzhafrkw3aj5kdyymc4j2jchqqjyn")))

(define-public crate-fuzzcheck_serializer-0.2.0 (c (n "fuzzcheck_serializer") (v "0.2.0") (d (list (d (n "fuzzcheck") (r "^0.2.0") (d #t) (k 0)))) (h "0029sx7hn05wp4krdgajq03jcn9lasnm13kprbmlskgx026d3ybg") (y #t)))

(define-public crate-fuzzcheck_serializer-0.2.1 (c (n "fuzzcheck_serializer") (v "0.2.1") (d (list (d (n "fuzzcheck") (r "^0.2.1") (d #t) (k 0)))) (h "0hr5gwwp9bjrgk6aj6n852sd6rhi3p94dq9cszmxl99j5k1gvflp")))

(define-public crate-fuzzcheck_serializer-0.3.0 (c (n "fuzzcheck_serializer") (v "0.3.0") (d (list (d (n "fuzzcheck_traits") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "031g9nhmdw7z1wll7vz5zndnkcp4wgdj4mazg7dxmwyskrylihgh") (f (quote (("serde_serializer" "serde" "serde_json") ("default"))))))

(define-public crate-fuzzcheck_serializer-0.4.0 (c (n "fuzzcheck_serializer") (v "0.4.0") (d (list (d (n "fuzzcheck_traits") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0vi8kr1cgb7qdkad08ph4a50qxc5vxz3qgrmbps49lfdq9gp789x") (f (quote (("serde_serializer" "serde" "serde_json") ("default"))))))

(define-public crate-fuzzcheck_serializer-0.5.0 (c (n "fuzzcheck_serializer") (v "0.5.0") (d (list (d (n "decent-serde-json-alternative") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "fuzzcheck_traits") (r "^0.5.0") (d #t) (k 0)) (d (n "json") (r "^0.12") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "194s0dnszlgajqa0a2268rzvf3dyjypxjqjlkn8gxgib4v1w3328") (f (quote (("serde-json-alternative" "json" "decent-serde-json-alternative") ("serde-json" "serde" "serde_json") ("default"))))))

(define-public crate-fuzzcheck_serializer-0.6.0 (c (n "fuzzcheck_serializer") (v "0.6.0") (d (list (d (n "decent-serde-json-alternative") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "fuzzcheck_traits") (r "^0.6.0") (d #t) (k 0)) (d (n "json") (r "^0.12") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "03bv7ljlack5wn9lgcqmspi7kar8bg23jip7k4wlmcbfnxp5klm9") (f (quote (("serde-json-alternative" "json" "decent-serde-json-alternative") ("serde-json" "serde" "serde_json") ("default"))))))

