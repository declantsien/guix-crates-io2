(define-module (crates-io fu zz fuzzycomp) #:use-module (crates-io))

(define-public crate-fuzzycomp-0.2.0 (c (n "fuzzycomp") (v "0.2.0") (h "0xcrnhwiqama1sw7nj7d8wpfsrx9xg7jm610pgwnqbvv28zjgb2b")))

(define-public crate-fuzzycomp-0.2.1 (c (n "fuzzycomp") (v "0.2.1") (h "0sk3vl4k1ab2mzgwz25lf2jyv4nad4fisz0xgdkq0c2zr33cilaq")))

(define-public crate-fuzzycomp-0.3.0 (c (n "fuzzycomp") (v "0.3.0") (h "19q7kcg6f324qfxnf32nzk0iysczwyf9w181nsdiasnyqa7prsy0")))

(define-public crate-fuzzycomp-0.4.0 (c (n "fuzzycomp") (v "0.4.0") (d (list (d (n "assert_approx_eq") (r "^1.1") (d #t) (k 0)))) (h "1q1p0bldvy0mi6xk5ivw4vqm6z31aj0drhla3bp3awf77415i4f8")))

