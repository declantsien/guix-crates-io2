(define-module (crates-io fu zz fuzzed-data-provider-rs) #:use-module (crates-io))

(define-public crate-fuzzed-data-provider-rs-0.1.0 (c (n "fuzzed-data-provider-rs") (v "0.1.0") (h "039kyc3ncdy4hjrma065m3ipdizvz7m9p17fg78dpks9g6990516") (y #t)))

(define-public crate-fuzzed-data-provider-rs-0.1.1 (c (n "fuzzed-data-provider-rs") (v "0.1.1") (h "1dcs97xha646qixa54d7f07r69cdxknahnxyxjya430pajw56hxs")))

