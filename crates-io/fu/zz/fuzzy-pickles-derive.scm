(define-module (crates-io fu zz fuzzy-pickles-derive) #:use-module (crates-io))

(define-public crate-fuzzy-pickles-derive-0.1.0 (c (n "fuzzy-pickles-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4.0") (d #t) (k 0)) (d (n "quote") (r "^0.6.0") (d #t) (k 0)) (d (n "syn") (r "^0.15.10") (d #t) (k 0)))) (h "0hr0kaygbmsapq9nalr8056b5cp0wxq7jglkxh5j2kkyvxanarh3")))

(define-public crate-fuzzy-pickles-derive-0.1.1 (c (n "fuzzy-pickles-derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.38") (d #t) (k 0)))) (h "1mbzh2adla9l2agk15az56hxksxf4cf01ga8cccz0xmpr1ynfya4")))

