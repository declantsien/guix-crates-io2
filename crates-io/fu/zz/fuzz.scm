(define-module (crates-io fu zz fuzz) #:use-module (crates-io))

(define-public crate-fuzz-0.1.0 (c (n "fuzz") (v "0.1.0") (d (list (d (n "base64") (r "^0.12.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.11") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.1") (d #t) (k 0) (p "clap-v3")) (d (n "fern") (r "^0.6.0") (f (quote ("colored"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("gzip" "cookies"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2.18") (f (quote ("fs" "io-util" "io-std" "sync" "time"))) (d #t) (k 0)))) (h "1ry2vlkrfn1w8vzy34g4cyac9in7hfgaj2pvipqy99i012crhlr6")))

(define-public crate-fuzz-0.1.1 (c (n "fuzz") (v "0.1.1") (d (list (d (n "base64") (r "^0.12.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.11") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.1") (d #t) (k 0) (p "clap-v3")) (d (n "fern") (r "^0.6.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("gzip" "cookies"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2.18") (f (quote ("fs" "io-util" "io-std" "sync" "time" "stream"))) (d #t) (k 0)))) (h "1ivran85qc439fc091bc23ip5nkm8jwmac15jyaqnx8jicha7vxw")))

