(define-module (crates-io fu zz fuzzy-muff) #:use-module (crates-io))

(define-public crate-fuzzy-muff-0.3.8 (c (n "fuzzy-muff") (v "0.3.8") (d (list (d (n "termion") (r "^2.0.1") (d #t) (k 2)) (d (n "thread_local") (r "^1.1.7") (d #t) (k 0)))) (h "19xv7r0gigf4355ma1njnk2hiz4r84jnbrp1qv3r4rn4z0qyvxjj") (f (quote (("default") ("compact"))))))

(define-public crate-fuzzy-muff-0.3.9 (c (n "fuzzy-muff") (v "0.3.9") (d (list (d (n "termion") (r "^2.0.1") (d #t) (k 2)) (d (n "thread_local") (r "^1.1.7") (d #t) (k 0)))) (h "106m5mnvgvg8372k2z0xkz5fp8w07lqgsnwaxyw1ibyvv0wicn3q") (f (quote (("default") ("compact"))))))

(define-public crate-fuzzy-muff-0.3.10 (c (n "fuzzy-muff") (v "0.3.10") (d (list (d (n "termion") (r "^2.0.1") (d #t) (k 2)) (d (n "thread_local") (r "^1.1.7") (d #t) (k 0)))) (h "1fnhs67wy0r3ywsi28v7f6afivx2ss2qi3mllcplmvzjikhpj49k") (f (quote (("default") ("compact"))))))

(define-public crate-fuzzy-muff-0.3.11 (c (n "fuzzy-muff") (v "0.3.11") (d (list (d (n "termion") (r "^3.0.0") (d #t) (k 2)) (d (n "thread_local") (r "^1.1.8") (d #t) (k 0)))) (h "1m7l7vc3xm8csqcj6c01l33vmyzifyglbwzzkilkp9r8z83h2mjn") (f (quote (("default") ("compact"))))))

(define-public crate-fuzzy-muff-0.3.12 (c (n "fuzzy-muff") (v "0.3.12") (d (list (d (n "termion") (r "^3.0.0") (d #t) (k 2)) (d (n "thread_local") (r "^1.1.8") (d #t) (k 0)))) (h "1kcsn9r72ykxml0fnkwgdj4kim92fdp9a046dw29144lzyc8vjvw") (f (quote (("default") ("compact"))))))

(define-public crate-fuzzy-muff-0.3.13 (c (n "fuzzy-muff") (v "0.3.13") (d (list (d (n "termion") (r "^3.0.0") (d #t) (k 2)) (d (n "thread_local") (r "^1.1.8") (d #t) (k 0)))) (h "15b8qsnm82ksf1345wjg519g2nnnfsgk44wj9wbc4bb0wyrxnnmj") (f (quote (("default") ("compact"))))))

(define-public crate-fuzzy-muff-0.3.14 (c (n "fuzzy-muff") (v "0.3.14") (d (list (d (n "termion") (r "^3.0.0") (d #t) (k 2)) (d (n "thread_local") (r "^1.1.8") (d #t) (k 0)))) (h "0yqngrzx3g9i5myzzq7gjka7740d6dmbni6qv6226x39kcpk5rqf") (f (quote (("default") ("compact"))))))

(define-public crate-fuzzy-muff-0.3.15 (c (n "fuzzy-muff") (v "0.3.15") (d (list (d (n "termion") (r "^3.0.0") (d #t) (k 2)) (d (n "thread_local") (r "^1.1.8") (d #t) (k 0)))) (h "043xqpspan7jxczf7zkcb2046sm8w29qfhzyk86grp4lcl0gkp06") (f (quote (("default") ("compact"))))))

(define-public crate-fuzzy-muff-0.3.16 (c (n "fuzzy-muff") (v "0.3.16") (d (list (d (n "termion") (r "^3.0.0") (d #t) (k 2)) (d (n "thread_local") (r "^1.1.8") (d #t) (k 0)))) (h "0xqk5hj9gik9bzba1bv3cnq6lmvmhw7qryjcfgihziks7dik3r61") (f (quote (("default") ("compact"))))))

(define-public crate-fuzzy-muff-0.3.17 (c (n "fuzzy-muff") (v "0.3.17") (d (list (d (n "termion") (r "^3.0.0") (d #t) (k 2)) (d (n "thread_local") (r "^1.1.8") (d #t) (k 0)))) (h "11ihmmhl3w33fr87r8mbh7y55f2ymiq2iygbqkf878gbqnc73fv7") (f (quote (("default") ("compact"))))))

(define-public crate-fuzzy-muff-0.3.18 (c (n "fuzzy-muff") (v "0.3.18") (d (list (d (n "termion") (r "^3.0.0") (d #t) (k 2)) (d (n "thread_local") (r "^1.1.8") (d #t) (k 0)))) (h "09vkm2bn6hi1n1lqw8i55rkpw3575njvhw0smyn0pcqj9vxkmcli") (f (quote (("default") ("compact"))))))

(define-public crate-fuzzy-muff-0.3.19 (c (n "fuzzy-muff") (v "0.3.19") (d (list (d (n "termion") (r "^3.0.0") (d #t) (k 2)) (d (n "thread_local") (r "^1.1.8") (d #t) (k 0)))) (h "0v5i5rhx17hy13jwr2xcmwys1ih65fsmm09vgyzpcxzc2k78kbj5") (f (quote (("default") ("compact"))))))

(define-public crate-fuzzy-muff-0.3.20 (c (n "fuzzy-muff") (v "0.3.20") (d (list (d (n "termion") (r "^3.0.0") (d #t) (k 2)) (d (n "thread_local") (r "^1.1.8") (d #t) (k 0)))) (h "050970571hdz0fkyn952cvm0byj59ns53g9wngwsgs9dygrf4dz2") (f (quote (("default") ("compact"))))))

(define-public crate-fuzzy-muff-0.3.21 (c (n "fuzzy-muff") (v "0.3.21") (d (list (d (n "termion") (r "^3.0.0") (d #t) (k 2)) (d (n "thread_local") (r "^1.1.8") (d #t) (k 0)))) (h "1x37xybzn8428s5vaqkx6wllb1qivwd0ybrmc7397smns4v3apdq") (f (quote (("default") ("compact"))))))

(define-public crate-fuzzy-muff-0.3.22 (c (n "fuzzy-muff") (v "0.3.22") (d (list (d (n "termion") (r "^3.0.0") (d #t) (k 2)) (d (n "thread_local") (r "^1.1.8") (d #t) (k 0)))) (h "1wf7dzhfbnv9cr35b0rkfymvibjvyvgdy56sxyhj1kng90s5c3sp") (f (quote (("default") ("compact"))))))

(define-public crate-fuzzy-muff-0.3.23 (c (n "fuzzy-muff") (v "0.3.23") (d (list (d (n "termion") (r "^3.0.0") (d #t) (k 2)) (d (n "thread_local") (r "^1.1.8") (d #t) (k 0)))) (h "1lip9qm02a9vds2sm6bi8xxm6g3wh4lzb1ndfzw8gn904lrp3d6r") (f (quote (("default") ("compact"))))))

