(define-module (crates-io fu zz fuzzy-pickles) #:use-module (crates-io))

(define-public crate-fuzzy-pickles-0.1.0 (c (n "fuzzy-pickles") (v "0.1.0") (d (list (d (n "fuzzy-pickles-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "peresil") (r "^0.4.0-alpha") (d #t) (k 0)) (d (n "unicode-xid") (r "^0.1.0") (d #t) (k 0)))) (h "13h57rmrng4ndf0nvcz8bwhr51h2bvqy5j14pjgpb4vvapf2fp1k")))

(define-public crate-fuzzy-pickles-0.1.1 (c (n "fuzzy-pickles") (v "0.1.1") (d (list (d (n "fuzzy-pickles-derive") (r "^0.1.1") (d #t) (k 0)) (d (n "peresil") (r "^0.4.0-alpha") (d #t) (k 0)) (d (n "unicode-xid") (r "^0.2.0") (d #t) (k 0)))) (h "01hpij0r9x426jb102lyrghxykvzz8j6qnwi07zbcgbwwggi8hy6")))

