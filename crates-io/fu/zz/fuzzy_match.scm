(define-module (crates-io fu zz fuzzy_match) #:use-module (crates-io))

(define-public crate-fuzzy_match-0.1.0 (c (n "fuzzy_match") (v "0.1.0") (d (list (d (n "rustc_version") (r "^0.1") (d #t) (k 1)) (d (n "sliding_windows") (r "^3.0") (d #t) (k 0)))) (h "194rszmh1lzn5mcr3p6xzqc9dfbl9x9220xjasqqnrp3f7q0lni4") (f (quote (("nightly"))))))

(define-public crate-fuzzy_match-0.1.1 (c (n "fuzzy_match") (v "0.1.1") (d (list (d (n "rustc_version") (r "^0.1") (d #t) (k 1)) (d (n "sliding_windows") (r "^3.0") (d #t) (k 0)))) (h "0ahh2mvd6fnjwbzn9myx0h76nrwxjj2zrlh3b1xcbazzrlrbfcn5") (f (quote (("nightly"))))))

(define-public crate-fuzzy_match-0.2.1 (c (n "fuzzy_match") (v "0.2.1") (d (list (d (n "rustc_version") (r "^0.1") (d #t) (k 1)) (d (n "sliding_windows") (r "^3.0") (d #t) (k 0)))) (h "1s0n3jd6dli8yp6prvxf77jqrxmjrdq3kv63xj0xhpzcyjlqq0az") (f (quote (("nightly"))))))

