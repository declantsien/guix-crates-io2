(define-module (crates-io fu zz fuzzy-matcher) #:use-module (crates-io))

(define-public crate-fuzzy-matcher-0.1.0 (c (n "fuzzy-matcher") (v "0.1.0") (d (list (d (n "termion") (r "^1.5.1") (d #t) (k 2)))) (h "0s463i7nmhaq6hv5q8mz518clz4wb6mdai2wbqdyc6rg7r8x7755")))

(define-public crate-fuzzy-matcher-0.2.0 (c (n "fuzzy-matcher") (v "0.2.0") (d (list (d (n "termion") (r "^1.5.1") (d #t) (k 2)))) (h "0ry64ay4a5lp5pmr7hiar8hf92jmnvqh3082pk940nzxiybrim8h")))

(define-public crate-fuzzy-matcher-0.2.1 (c (n "fuzzy-matcher") (v "0.2.1") (d (list (d (n "termion") (r "^1.5.1") (d #t) (k 2)))) (h "0rrzjaajdypgvfdzrbvjhszg9vhca7vbg7ahzbsm0wjxfr9ckf1d")))

(define-public crate-fuzzy-matcher-0.2.2 (c (n "fuzzy-matcher") (v "0.2.2") (d (list (d (n "termion") (r "^1.5.1") (d #t) (k 2)))) (h "0qw3fv7wlza5m28jiqri6z26gmz309ip369dqqzlivw31i9az2qb")))

(define-public crate-fuzzy-matcher-0.3.1 (c (n "fuzzy-matcher") (v "0.3.1") (d (list (d (n "termion") (r "^1.5.1") (d #t) (k 2)) (d (n "thread_local") (r "^1.0.0") (d #t) (k 0)))) (h "1wzrj3n47xih4n78kry6igx8bp7p6qy87ddxzizkczrs0iphr1jd")))

(define-public crate-fuzzy-matcher-0.3.2 (c (n "fuzzy-matcher") (v "0.3.2") (d (list (d (n "termion") (r "^1.5.1") (d #t) (k 2)) (d (n "thread_local") (r "^1.0.0") (d #t) (k 0)))) (h "0w6q4xsjx6gm9kjlzr735k1bgkvjhmr1s862aqz1w2a2vylaj98g")))

(define-public crate-fuzzy-matcher-0.3.3 (c (n "fuzzy-matcher") (v "0.3.3") (d (list (d (n "termion") (r "^1.5.1") (d #t) (k 2)) (d (n "thread_local") (r "^1.0.0") (d #t) (k 0)))) (h "1dir10jcj9z048vf5sbbyn317n2jf2nls39gvbka2x849383s362")))

(define-public crate-fuzzy-matcher-0.3.4 (c (n "fuzzy-matcher") (v "0.3.4") (d (list (d (n "termion") (r "^1.5.1") (d #t) (k 2)) (d (n "thread_local") (r "^1.0.0") (d #t) (k 0)))) (h "0w0ih9rsvhl852zr00mb42k3iz47kbssglz9xi8x3z19hrnkv83m")))

(define-public crate-fuzzy-matcher-0.3.5 (c (n "fuzzy-matcher") (v "0.3.5") (d (list (d (n "termion") (r "^1.5.1") (d #t) (k 2)) (d (n "thread_local") (r "^1.0.0") (d #t) (k 0)))) (h "0ajg0m3lpbni6q41v0399l9j47cx689brdhabs9js60krd58r8nd") (f (quote (("default") ("compact"))))))

(define-public crate-fuzzy-matcher-0.3.6 (c (n "fuzzy-matcher") (v "0.3.6") (d (list (d (n "termion") (r "^1.5.1") (d #t) (k 2)) (d (n "thread_local") (r "^1.0.0") (d #t) (k 0)))) (h "1a4c841nidhh2mm7kag3kknvyqhip9sz82dhl4klfq0b1mrji8rb") (f (quote (("default") ("compact"))))))

(define-public crate-fuzzy-matcher-0.3.7 (c (n "fuzzy-matcher") (v "0.3.7") (d (list (d (n "termion") (r "^1.5.1") (d #t) (k 2)) (d (n "thread_local") (r "^1.0.0") (d #t) (k 0)))) (h "153csv8rsk2vxagb68kpmiknvdd3bzqj03x805khckck28rllqal") (f (quote (("default") ("compact"))))))

