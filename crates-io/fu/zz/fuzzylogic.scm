(define-module (crates-io fu zz fuzzylogic) #:use-module (crates-io))

(define-public crate-fuzzylogic-0.1.0 (c (n "fuzzylogic") (v "0.1.0") (h "1dr4y0rafirfhw3r0sz33g3063jll8qfgf011bwm60nv9jl15i5m")))

(define-public crate-fuzzylogic-0.2.0 (c (n "fuzzylogic") (v "0.2.0") (h "1y5wzxzvbc1l56m6x9awavyvxkpz78v3wmjsrw1n09w171dhx30s")))

