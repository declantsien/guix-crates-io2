(define-module (crates-io fu zz fuzzcheck_mutators_derive) #:use-module (crates-io))

(define-public crate-fuzzcheck_mutators_derive-0.3.0 (c (n "fuzzcheck_mutators_derive") (v "0.3.0") (h "08d822dxd0f5inzia67h3kv6zxvl4jgv1fxla407r3a2jza3nhhm")))

(define-public crate-fuzzcheck_mutators_derive-0.4.0 (c (n "fuzzcheck_mutators_derive") (v "0.4.0") (h "0w4p9nrysqjjm0wgg9ij0h7p5l0f8hc5y6s2n99qh67yi0fmcyg4")))

(define-public crate-fuzzcheck_mutators_derive-0.5.0 (c (n "fuzzcheck_mutators_derive") (v "0.5.0") (d (list (d (n "decent-synquote-alternative") (r "^0.2.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)))) (h "0p25xc2a3v3zpmr72q30gy7p4ljkyh0a2sqbhjg4p7lbqr458jf2")))

(define-public crate-fuzzcheck_mutators_derive-0.6.0 (c (n "fuzzcheck_mutators_derive") (v "0.6.0") (d (list (d (n "decent-synquote-alternative") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)))) (h "1hjzw3f3hf6a5a4rprj2rpgr2dnqgabf0h2r61jp21r5qcjn3mpx")))

(define-public crate-fuzzcheck_mutators_derive-0.7.0 (c (n "fuzzcheck_mutators_derive") (v "0.7.0") (d (list (d (n "decent-synquote-alternative") (r "^0.4") (d #t) (k 0) (p "decent-synquote-alternative")) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)))) (h "0zjzdbqq4r94vx5blgak2bl561wis050qi2azhdyb8a0kjhv0n3y")))

(define-public crate-fuzzcheck_mutators_derive-0.8.0 (c (n "fuzzcheck_mutators_derive") (v "0.8.0") (d (list (d (n "decent-synquote-alternative") (r "^0.4") (d #t) (k 0) (p "decent-synquote-alternative")) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)))) (h "0iqcf8778zadxilvbj81vy8zadxl9ghh34ydy9zkc7qzzp5nbf0s")))

(define-public crate-fuzzcheck_mutators_derive-0.9.0 (c (n "fuzzcheck_mutators_derive") (v "0.9.0") (d (list (d (n "decent-synquote-alternative") (r "^0.4") (d #t) (k 0) (p "decent-synquote-alternative")) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)))) (h "12w76idl29ln4dh7jf7s3hfyw6hgdbwha59pxg89pyhnysyma22g")))

(define-public crate-fuzzcheck_mutators_derive-0.10.0 (c (n "fuzzcheck_mutators_derive") (v "0.10.0") (d (list (d (n "decent-synquote-alternative") (r "^0.4") (d #t) (k 0) (p "decent-synquote-alternative")) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)))) (h "1xbg1gb75wdfvwsal36q4w6979zjkn9iq77pk03l7hzjls01qmla")))

(define-public crate-fuzzcheck_mutators_derive-0.11.0 (c (n "fuzzcheck_mutators_derive") (v "0.11.0") (d (list (d (n "decent-synquote-alternative") (r "^0.4") (d #t) (k 0) (p "decent-synquote-alternative")) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)))) (h "0ik85mk1r03mlrxv5xrbpprfhsr07wgi8rrirq7w9j1r8f898m85")))

(define-public crate-fuzzcheck_mutators_derive-0.12.0 (c (n "fuzzcheck_mutators_derive") (v "0.12.0") (d (list (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.91") (d #t) (k 0)))) (h "1j30p8j4df4lmrkgfqw96ra8m6gmk7kpz46gimsy1rxkpgl03kih")))

