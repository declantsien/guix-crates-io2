(define-module (crates-io fu zz fuzzcheck_nix_subset) #:use-module (crates-io))

(define-public crate-fuzzcheck_nix_subset-0.1.0 (c (n "fuzzcheck_nix_subset") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.1") (d #t) (k 0)) (d (n "cfg-if") (r "^0.1.10") (d #t) (k 0)) (d (n "libc") (r "^0.2") (k 0)))) (h "07973cg5c6wykcmpwfki0cc346ampfdbcs4n3x6ny00440s6zh35") (y #t)))

(define-public crate-fuzzcheck_nix_subset-0.1.1 (c (n "fuzzcheck_nix_subset") (v "0.1.1") (d (list (d (n "bitflags") (r "^1.1") (d #t) (k 0)) (d (n "cfg-if") (r "^0.1.10") (d #t) (k 0)) (d (n "libc") (r "^0.2") (k 0)))) (h "1y4xyabg09bmx3zn5q2r7yc9b18lbrrr76nhacpl207sw5n4l4p6")))

