(define-module (crates-io fu zz fuzzywuzzy) #:use-module (crates-io))

(define-public crate-fuzzywuzzy-0.0.1 (c (n "fuzzywuzzy") (v "0.0.1") (h "0aga2lwzx5yza3ahmnmj2xq56kykrqsbn9ci1h1ikiv6hz46k4ni")))

(define-public crate-fuzzywuzzy-0.0.2 (c (n "fuzzywuzzy") (v "0.0.2") (h "0l03slcfy5bz33zlagp707d0dax81ilfiq3fxgmihgdn0bqxqxf0")))

