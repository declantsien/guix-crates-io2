(define-module (crates-io fu zz fuzzcheck_common) #:use-module (crates-io))

(define-public crate-fuzzcheck_common-0.5.0 (c (n "fuzzcheck_common") (v "0.5.0") (d (list (d (n "decent-serde-json-alternative") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "json") (r "^0.12") (o #t) (d #t) (k 0)))) (h "04mmmdgx7qy89nvxqr7wipy9dyzfqw1z4vz33xvlkxnjynjm46ax") (f (quote (("ui" "json" "decent-serde-json-alternative"))))))

(define-public crate-fuzzcheck_common-0.6.0 (c (n "fuzzcheck_common") (v "0.6.0") (d (list (d (n "decent-serde-json-alternative") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "json") (r "^0.12") (o #t) (d #t) (k 0)))) (h "19711yif39ls1amfksqfqwzh6k3zz7vv247jki94g54b2s0m0gwb") (f (quote (("ui" "json" "decent-serde-json-alternative"))))))

(define-public crate-fuzzcheck_common-0.7.0 (c (n "fuzzcheck_common") (v "0.7.0") (d (list (d (n "decent-serde-json-alternative") (r "^0.5.0") (o #t) (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "json") (r "^0.12") (o #t) (d #t) (k 0)))) (h "03gs1vmpgpynppnihwd0n9j9l3y5imrjczsd6jj57hqis257axp7") (f (quote (("ui" "json" "decent-serde-json-alternative"))))))

(define-public crate-fuzzcheck_common-0.8.0 (c (n "fuzzcheck_common") (v "0.8.0") (d (list (d (n "decent-serde-json-alternative") (r "^0.5.0") (o #t) (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "json") (r "^0.12") (o #t) (d #t) (k 0)))) (h "0cqsnvav6lq2k4j0kf01x25rsrrpshn7yjnkcn99fz5xy72s9d4g")))

(define-public crate-fuzzcheck_common-0.9.0 (c (n "fuzzcheck_common") (v "0.9.0") (d (list (d (n "decent-serde-json-alternative") (r "^0.5.0") (o #t) (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "json") (r "^0.12") (o #t) (d #t) (k 0)))) (h "0z5wmvwxnswg7fgbgg0vxm69bq9aizyycd1p44n61rj1znldaxjp")))

(define-public crate-fuzzcheck_common-0.10.0 (c (n "fuzzcheck_common") (v "0.10.0") (d (list (d (n "decent-serde-json-alternative") (r "^0.5.0") (o #t) (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "json") (r "^0.12") (o #t) (d #t) (k 0)))) (h "1bxg78msxq174dnkjg2kx52p2sv46spqi7v5k7dydrzyd7vw41qg")))

(define-public crate-fuzzcheck_common-0.11.0 (c (n "fuzzcheck_common") (v "0.11.0") (d (list (d (n "decent-serde-json-alternative") (r "^0.5.0") (o #t) (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "json") (r "^0.12") (o #t) (d #t) (k 0)))) (h "12zcn2aj7b90y23yc8sqj83ldhi5sx6dlki6q5qqlxrci4zhhjzy")))

(define-public crate-fuzzcheck_common-0.12.0 (c (n "fuzzcheck_common") (v "0.12.0") (d (list (d (n "getopts") (r "^0.2.21") (d #t) (k 0)))) (h "0vzh90nm8g8vrx6as40b8qj386drwfndplxj7va3ajmi4n6nzq6x")))

