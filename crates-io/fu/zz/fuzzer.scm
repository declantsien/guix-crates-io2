(define-module (crates-io fu zz fuzzer) #:use-module (crates-io))

(define-public crate-fuzzer-0.0.1 (c (n "fuzzer") (v "0.0.1") (d (list (d (n "clap") (r "^4.0.17") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1hg80x4da58gzf106avpnbhbm4sxqdhbwii7gghm16qlf6gvw0nc") (y #t)))

(define-public crate-fuzzer-0.0.2 (c (n "fuzzer") (v "0.0.2") (d (list (d (n "clap") (r "^4.0.17") (f (quote ("derive"))) (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0jpn6vm27l9i222rw6iq3xjxswl1ndh3hsa0rgqsb2rpxxmihw93")))

