(define-module (crates-io fu zz fuzzy-logic_rs) #:use-module (crates-io))

(define-public crate-fuzzy-logic_rs-0.2.0 (c (n "fuzzy-logic_rs") (v "0.2.0") (h "1iw11p4xwl6443zschf6by91nrk5d0xc79awybp9hksxs69qw20r")))

(define-public crate-fuzzy-logic_rs-0.2.1 (c (n "fuzzy-logic_rs") (v "0.2.1") (h "0qkvbldqsigc1504pp3ac9jh3nzc90qcnx7ldnzg65cmvbj759f4")))

(define-public crate-fuzzy-logic_rs-0.2.2 (c (n "fuzzy-logic_rs") (v "0.2.2") (h "0rnq4qn1xk13zfsrq2s71k21yf2q3f6d03ddq1fg3phiiklkmv63")))

(define-public crate-fuzzy-logic_rs-0.3.0 (c (n "fuzzy-logic_rs") (v "0.3.0") (h "1h4ja1j81l5icp3h798x6ckn85blnw3068j684nywb32vhlr3n5z")))

(define-public crate-fuzzy-logic_rs-0.3.0-patch1 (c (n "fuzzy-logic_rs") (v "0.3.0-patch1") (h "1b1z3hdkagynagmmgi5yjj12m3117p6znjnzssq1nqzq051kxdgm")))

(define-public crate-fuzzy-logic_rs-0.4.0 (c (n "fuzzy-logic_rs") (v "0.4.0") (h "1qscm0mqwzfamqxb24z454n67csis24p9ll7xq1s1mjwj0qa0g2l")))

(define-public crate-fuzzy-logic_rs-0.5.0 (c (n "fuzzy-logic_rs") (v "0.5.0") (h "1js5pgdfwmrrpzpinyx3p5jvrmlkdn6nnhjcxvb100g9vf1ls48c")))

