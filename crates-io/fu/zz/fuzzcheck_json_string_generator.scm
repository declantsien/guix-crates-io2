(define-module (crates-io fu zz fuzzcheck_json_string_generator) #:use-module (crates-io))

(define-public crate-fuzzcheck_json_string_generator-0.1.0 (c (n "fuzzcheck_json_string_generator") (v "0.1.0") (d (list (d (n "fuzzcheck") (r "^0.12.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.83") (d #t) (k 2)))) (h "10f3fyj6pklafp7v6lwbcvd6mky7x626nlp473ssxlal4q3bcnbp")))

