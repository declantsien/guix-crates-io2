(define-module (crates-io fu zz fuzzcheck_mutators) #:use-module (crates-io))

(define-public crate-fuzzcheck_mutators-0.1.0 (c (n "fuzzcheck_mutators") (v "0.1.0") (d (list (d (n "fuzzcheck") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.7") (f (quote ("small_rng"))) (k 0)))) (h "0gc2bsfx3xn4frnlfvscx02wzk1njbhpxbcrvjkvnikch81kzyfq") (y #t)))

(define-public crate-fuzzcheck_mutators-0.1.1 (c (n "fuzzcheck_mutators") (v "0.1.1") (d (list (d (n "fuzzcheck") (r "^0.1.1") (d #t) (k 0)) (d (n "rand") (r "^0.7") (f (quote ("small_rng"))) (k 0)))) (h "05a4w5m7c1y2fq727cdpc4gnj4x0456q9sn0ak470743c89n1vvx")))

(define-public crate-fuzzcheck_mutators-0.2.0 (c (n "fuzzcheck_mutators") (v "0.2.0") (d (list (d (n "fuzzcheck") (r "^0.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.7") (f (quote ("small_rng"))) (k 0)))) (h "047g2g0zsvn0g99axmpryjxqj666rmkf2sgmxpdcf41zvx03skif") (y #t)))

(define-public crate-fuzzcheck_mutators-0.2.1 (c (n "fuzzcheck_mutators") (v "0.2.1") (d (list (d (n "fuzzcheck") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.7") (f (quote ("small_rng"))) (k 0)))) (h "07qafw6ss4df7n1ibhlfjhwr9p726msgcacl4p5dx9r25kckimqn")))

(define-public crate-fuzzcheck_mutators-0.3.0 (c (n "fuzzcheck_mutators") (v "0.3.0") (d (list (d (n "fastrand") (r "^1.0") (d #t) (k 0)) (d (n "fuzzcheck_mutators_derive") (r "^0.3.0") (d #t) (k 0)) (d (n "fuzzcheck_traits") (r "^0.3.0") (d #t) (k 0)))) (h "1jm002d06j9w9c5wy6mp9niniilp0w7ys342ywqkl17h378mknd3")))

(define-public crate-fuzzcheck_mutators-0.4.0 (c (n "fuzzcheck_mutators") (v "0.4.0") (d (list (d (n "fastrand") (r "^1.0") (d #t) (k 0)) (d (n "fuzzcheck_mutators_derive") (r "^0.3.0") (d #t) (k 0)) (d (n "fuzzcheck_traits") (r "^0.4.0") (d #t) (k 0)))) (h "03ik9pwvwkidykhpdkj2zvxjls18sbwr1jd0qb7pxrnd0h5a4sr9") (y #t)))

(define-public crate-fuzzcheck_mutators-0.4.1 (c (n "fuzzcheck_mutators") (v "0.4.1") (d (list (d (n "fastrand") (r "^1.0") (d #t) (k 0)) (d (n "fuzzcheck_mutators_derive") (r "^0.4.0") (d #t) (k 0)) (d (n "fuzzcheck_traits") (r "^0.4.0") (d #t) (k 0)))) (h "17ql35l6qklhy30f590zali7m2j217ms5l584vzsrch2lncmv79d")))

(define-public crate-fuzzcheck_mutators-0.5.0 (c (n "fuzzcheck_mutators") (v "0.5.0") (d (list (d (n "fastrand") (r "^1.0") (d #t) (k 0)) (d (n "fuzzcheck_mutators_derive") (r "^0.5.0") (d #t) (k 0)) (d (n "fuzzcheck_traits") (r "^0.5.0") (d #t) (k 0)))) (h "0pnh2dsvk3x2c73q4811j01p9xn3nqgcl17g05sknx9wfp6i8mzp")))

(define-public crate-fuzzcheck_mutators-0.6.0 (c (n "fuzzcheck_mutators") (v "0.6.0") (d (list (d (n "fastrand") (r "^1.0") (d #t) (k 0)) (d (n "fuzzcheck_mutators_derive") (r "^0.6.0") (d #t) (k 0)) (d (n "fuzzcheck_traits") (r "^0.6.0") (d #t) (k 0)))) (h "0wgg5zqacngii385pqr6rn7dgzclay8cwh1j50bncnfk4hcx7rm2")))

