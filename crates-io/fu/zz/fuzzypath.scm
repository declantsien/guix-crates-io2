(define-module (crates-io fu zz fuzzypath) #:use-module (crates-io))

(define-public crate-fuzzypath-1.0.0 (c (n "fuzzypath") (v "1.0.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1g1i2cqmmc533nl6cdrbh331kiqcxq282y71a7mfv9h0mhbfk8hf")))

(define-public crate-fuzzypath-1.0.1 (c (n "fuzzypath") (v "1.0.1") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0wr1g8qhgik4m0m57xk2w8rjfwkjqrk4rb9x0qka6qw8lpnlmiwr")))

(define-public crate-fuzzypath-2.0.0 (c (n "fuzzypath") (v "2.0.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0avhq16y3vvncwslhlfviqanmq5w5vhylkqahjjlkqmhk9lh3pm5")))

