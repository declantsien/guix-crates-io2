(define-module (crates-io fu zz fuzzy-expert) #:use-module (crates-io))

(define-public crate-fuzzy-expert-0.1.0 (c (n "fuzzy-expert") (v "0.1.0") (d (list (d (n "fixed-map") (r "^0.8") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "slotmap") (r "^1") (d #t) (k 0)))) (h "0sg2bqgv146d345c7scp1nz5aiykpbmspwm1qx03plk4ridh5svi")))

