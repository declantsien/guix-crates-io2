(define-module (crates-io fu zz fuzzy-tree) #:use-module (crates-io))

(define-public crate-fuzzy-tree-0.1.0 (c (n "fuzzy-tree") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)))) (h "1anl7av2rjk6i0bxihk2v174vgvi8vvchmv75i2vqs2v4qnsq365")))

(define-public crate-fuzzy-tree-0.1.1 (c (n "fuzzy-tree") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)))) (h "0fl99yq86h198i1l16i78dnnwhxaa7z6id6m988x44y5q2h33m2p")))

(define-public crate-fuzzy-tree-0.1.2 (c (n "fuzzy-tree") (v "0.1.2") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)) (d (n "which") (r "^3.1.1") (d #t) (k 0)))) (h "12fxzdyamm1avv1x6jx1v8ga3bpf0g4aj3bywc9i099rmfavcqzk")))

