(define-module (crates-io fu zz fuzz-search) #:use-module (crates-io))

(define-public crate-fuzz-search-0.1.0 (c (n "fuzz-search") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1") (d #t) (k 0)))) (h "0h2b8niy5ss3md7rinp4nac1h0m4j5cilw70f5qknl1rz9bwhmyw")))

