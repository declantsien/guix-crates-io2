(define-module (crates-io fu zz fuzzcheck_serde_json_generator) #:use-module (crates-io))

(define-public crate-fuzzcheck_serde_json_generator-0.1.0 (c (n "fuzzcheck_serde_json_generator") (v "0.1.0") (d (list (d (n "fuzzcheck") (r "^0.12.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.83") (d #t) (k 0)))) (h "0czya6qs1lrd7pf7x8hdznx1is1rg48hjwhrw452ws2szsli79dg")))

(define-public crate-fuzzcheck_serde_json_generator-0.1.1 (c (n "fuzzcheck_serde_json_generator") (v "0.1.1") (d (list (d (n "fuzzcheck") (r "^0.12.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.83") (d #t) (k 0)))) (h "0xg7d0dznpwsdx2i6lzqwr1qzwqmxm2kx7ybzy385f8sw49kszzs")))

