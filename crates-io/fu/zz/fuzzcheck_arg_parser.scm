(define-module (crates-io fu zz fuzzcheck_arg_parser) #:use-module (crates-io))

(define-public crate-fuzzcheck_arg_parser-0.1.0 (c (n "fuzzcheck_arg_parser") (v "0.1.0") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)))) (h "0anmhj6wykmxc4dwa30vrvz3r3a2vs36k21a53rc7vah2a2wxkmw") (y #t)))

(define-public crate-fuzzcheck_arg_parser-0.1.1 (c (n "fuzzcheck_arg_parser") (v "0.1.1") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)))) (h "1jqyh6cv0z91a7vcvlfhjl86dgbj5dkr0bwfbm59z59yfxsm2vwh")))

(define-public crate-fuzzcheck_arg_parser-0.2.0 (c (n "fuzzcheck_arg_parser") (v "0.2.0") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)))) (h "1hzflij19gph412gk1cnqgd2af3fy8nhmbn9ijyiw10z24w91ik5")))

(define-public crate-fuzzcheck_arg_parser-0.2.1 (c (n "fuzzcheck_arg_parser") (v "0.2.1") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)))) (h "1bfa6d3lp0sph1fgm3zgabnjj3hks8x8l11cdcpxjjfh4vxlbvhn")))

(define-public crate-fuzzcheck_arg_parser-0.3.0 (c (n "fuzzcheck_arg_parser") (v "0.3.0") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)))) (h "155ihpkapki18syx79dv761gzgk7p1bg732f9js4nczxmi3396b4")))

(define-public crate-fuzzcheck_arg_parser-0.4.0 (c (n "fuzzcheck_arg_parser") (v "0.4.0") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)))) (h "1qabxfgm6529vn48pva6ixj1rwl4ahpfrk3zr18vyaayv5rafdd5")))

