(define-module (crates-io fu el fuel-derive) #:use-module (crates-io))

(define-public crate-fuel-derive-0.37.0 (c (n "fuel-derive") (v "0.37.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.13") (d #t) (k 0)))) (h "1yyzvpcy6m9l94h2q1akb4m4bvdj2mf33mjb23zi5y42y5n24p8j")))

(define-public crate-fuel-derive-0.38.0 (c (n "fuel-derive") (v "0.38.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.13") (d #t) (k 0)))) (h "072fskf8ixgaap7x4q4cj1y2lrdj131xhg7z3vvb1wpna5wld2wc")))

(define-public crate-fuel-derive-0.39.0 (c (n "fuel-derive") (v "0.39.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.13") (d #t) (k 0)))) (h "1znm17k9ami2a5riiil3wqyzarhb533hmy5dfs8zq1kmwm829ywj")))

(define-public crate-fuel-derive-0.40.0 (c (n "fuel-derive") (v "0.40.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.13") (d #t) (k 0)))) (h "1qfi9gr49dahqnv2czgnj8zrlf9c3446mpfg6q3q6lrawvp9vfmd")))

(define-public crate-fuel-derive-0.41.0 (c (n "fuel-derive") (v "0.41.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.13") (d #t) (k 0)))) (h "1r5dqq3g3cagllkqiyjmmqj9j44zai7d9q8q656f26zvcxpl4fnf")))

(define-public crate-fuel-derive-0.42.0 (c (n "fuel-derive") (v "0.42.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.13") (d #t) (k 0)))) (h "1bplvqvajnf3p3lzhdnjdgc55kfsjdyc1qxdglpvd2alzrbc7a97")))

(define-public crate-fuel-derive-0.42.1 (c (n "fuel-derive") (v "0.42.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.13") (d #t) (k 0)))) (h "0h30wgz3w45alyylx2b8qpg263grsk42l5575b80zl166mlzyslc")))

(define-public crate-fuel-derive-0.42.2 (c (n "fuel-derive") (v "0.42.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.13") (d #t) (k 0)))) (h "1v7j037j60rxff2nmn4kdyqwj6qbcl3yrplclggr77ddk4wjychv")))

(define-public crate-fuel-derive-0.42.3 (c (n "fuel-derive") (v "0.42.3") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.13") (d #t) (k 0)))) (h "0nn3q7yxr1mkv42gmliljm8y5l4j88v7gg9m7j85rr5rkrg5k2f3")))

(define-public crate-fuel-derive-0.43.0 (c (n "fuel-derive") (v "0.43.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.13") (d #t) (k 0)))) (h "1lxhsvl68xqz7w8238q09bihqpn8nsr1w5l81vapr50lgnjckz8y")))

(define-public crate-fuel-derive-0.43.1 (c (n "fuel-derive") (v "0.43.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.13") (d #t) (k 0)))) (h "1s2apacwiihv1v2p2yl6m7jya1iifjwgkaczw7kw3p3blh9xyyjr")))

(define-public crate-fuel-derive-0.43.2 (c (n "fuel-derive") (v "0.43.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.13") (d #t) (k 0)))) (h "0yiswrkpv9zfx363jdjb9v3hffzx9lan91rsqr099yx4056wyn7z")))

(define-public crate-fuel-derive-0.44.0 (c (n "fuel-derive") (v "0.44.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.13") (d #t) (k 0)))) (h "1rq5xsf34i488v4l7infgk9425k2jx6r4jp8k15majrcc3hybwcj")))

(define-public crate-fuel-derive-0.45.0 (c (n "fuel-derive") (v "0.45.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.13") (d #t) (k 0)))) (h "1wbi1l9dmj4svlnz9yw2vhz3ij902042vrr7h83sqa4ggnckah71")))

(define-public crate-fuel-derive-0.46.0 (c (n "fuel-derive") (v "0.46.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.13") (d #t) (k 0)))) (h "1car4dkphkx6z47v2328kb7zf5zrfwyz6lvgh5vh3f0k1v3c736b")))

(define-public crate-fuel-derive-0.47.0 (c (n "fuel-derive") (v "0.47.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.13") (d #t) (k 0)))) (h "1z4bzzfh6jcxjvw3h2mm02rz0rq51n0sdc5lw808bzvr3iv4chva")))

(define-public crate-fuel-derive-0.47.1 (c (n "fuel-derive") (v "0.47.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.13") (d #t) (k 0)))) (h "03q0k9h1dg1nk1n43p1hx39mim85626rpbjv79hkv7gycxa2d6qb")))

(define-public crate-fuel-derive-0.48.0 (c (n "fuel-derive") (v "0.48.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.13") (d #t) (k 0)))) (h "0sq39cdfmck6cqyrfqjykw8zp6f71ncy5fnxidzg414zhcxn15jq")))

(define-public crate-fuel-derive-0.49.0 (c (n "fuel-derive") (v "0.49.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.13") (d #t) (k 0)))) (h "114qr4bb3bqxzrawrwgy674hjw0vjblwypq7ca10hvdj13jyi19b")))

(define-public crate-fuel-derive-0.50.0 (c (n "fuel-derive") (v "0.50.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.13") (d #t) (k 0)))) (h "0aajghiihpix9029ndb9ypy123l881n1qan181k93sv83dnydmnq")))

