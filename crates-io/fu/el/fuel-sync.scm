(define-module (crates-io fu el fuel-sync) #:use-module (crates-io))

(define-public crate-fuel-sync-0.9.1 (c (n "fuel-sync") (v "0.9.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "fuel-core-interfaces") (r "^0.9.1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "tokio") (r "^1.14") (f (quote ("full"))) (d #t) (k 0)))) (h "1z4fadncwlwnlvq2wwmq589azsk3y02wsxmiz08dw7xcngvnx6jp")))

(define-public crate-fuel-sync-0.9.2 (c (n "fuel-sync") (v "0.9.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "fuel-core-interfaces") (r "^0.9.2") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "tokio") (r "^1.14") (f (quote ("full"))) (d #t) (k 0)))) (h "0z20n45jmzp7ri6s9vzc0hnpyskddm289w3vn4kwrc387kz8v8nq")))

(define-public crate-fuel-sync-0.9.3 (c (n "fuel-sync") (v "0.9.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "fuel-core-interfaces") (r "^0.9.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "tokio") (r "^1.14") (f (quote ("full"))) (d #t) (k 0)))) (h "1bqxnnf80rlmczsq9b77l1is9jnpcq0x0221vly5xvpj0k8k5cy3")))

(define-public crate-fuel-sync-0.9.4 (c (n "fuel-sync") (v "0.9.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "fuel-core-interfaces") (r "^0.9.4") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "tokio") (r "^1.14") (f (quote ("full"))) (d #t) (k 0)))) (h "157dpqshgc6zkz9zbk09g9vxkyavn7apclr0fkna4r368s8w82cz")))

(define-public crate-fuel-sync-0.9.5 (c (n "fuel-sync") (v "0.9.5") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "fuel-core-interfaces") (r "^0.9.5") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "tokio") (r "^1.14") (f (quote ("full"))) (d #t) (k 0)))) (h "00sqccr8hkb1ds7c4nm2m8pwida2232vgh9dai3rcr7rs8j53z8j")))

(define-public crate-fuel-sync-0.9.6 (c (n "fuel-sync") (v "0.9.6") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "fuel-core-interfaces") (r "^0.9.6") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "tokio") (r "^1.14") (f (quote ("full"))) (d #t) (k 0)))) (h "09955m43mbzkvkqgq2a7iyybj6196by1mb4zh11qmrmzccdidx8z")))

(define-public crate-fuel-sync-0.9.8 (c (n "fuel-sync") (v "0.9.8") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "fuel-core-interfaces") (r "^0.9.8") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "tokio") (r "^1.14") (f (quote ("full"))) (d #t) (k 0)))) (h "1w5avpj8k2y9kqihmd1q8n08qkzay6i8vq00ddiihijac8fvbf83")))

(define-public crate-fuel-sync-0.10.0 (c (n "fuel-sync") (v "0.10.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "fuel-core-interfaces") (r "^0.10.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "tokio") (r "^1.14") (f (quote ("full"))) (d #t) (k 0)))) (h "000q63ks89yw0hxr48bzrh09snkqxvqcplxvl0krf8y7pfg3znm0")))

(define-public crate-fuel-sync-0.10.1 (c (n "fuel-sync") (v "0.10.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "fuel-core-interfaces") (r "^0.10.1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "tokio") (r "^1.14") (f (quote ("full"))) (d #t) (k 0)))) (h "0998wwkj1dxv5b47hmscshxh8a57wlfr9fi87ixb8q8v2da14d8k")))

(define-public crate-fuel-sync-0.11.2 (c (n "fuel-sync") (v "0.11.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "fuel-core-interfaces") (r "^0.11.2") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "tokio") (r "^1.21") (f (quote ("full"))) (d #t) (k 0)))) (h "1cmgfazlfx5fdnxqnlp2s5ynzri5wl08g677j56sfcj13rz3y6nz")))

(define-public crate-fuel-sync-0.12.0 (c (n "fuel-sync") (v "0.12.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "fuel-core-interfaces") (r "^0.12.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "tokio") (r "^1.21") (f (quote ("full"))) (d #t) (k 0)))) (h "02s1fcayy6hkqkhhpjd67d8q4dmv0r8xjhplcjmwyszdm4d9qdjq")))

(define-public crate-fuel-sync-0.13.0 (c (n "fuel-sync") (v "0.13.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "fuel-core-interfaces") (r "^0.13.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "tokio") (r "^1.21") (f (quote ("full"))) (d #t) (k 0)))) (h "1qnlp10zr25hhq4p5vsxjzpfsi1qmz9xd2v2dhd5q1x5718rhk8i")))

(define-public crate-fuel-sync-0.13.1 (c (n "fuel-sync") (v "0.13.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "fuel-core-interfaces") (r "^0.13.1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "tokio") (r "^1.21") (f (quote ("full"))) (d #t) (k 0)))) (h "1ilk9ink2yg25skl15a0kwgljl14v0da95m0dn08qlr49mrff6mm")))

(define-public crate-fuel-sync-0.13.2 (c (n "fuel-sync") (v "0.13.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "fuel-core-interfaces") (r "^0.13.2") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "tokio") (r "^1.21") (f (quote ("full"))) (d #t) (k 0)))) (h "066vzw426343az4gjc942bdx51kwi9n4881pz7qswfd82r81d4l7")))

(define-public crate-fuel-sync-0.14.0 (c (n "fuel-sync") (v "0.14.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "fuel-core-interfaces") (r "^0.14.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "tokio") (r "^1.21") (f (quote ("full"))) (d #t) (k 0)))) (h "02rk74drxrz8jvrn77w7g8s9jyl58x5ii03hgfzcda4nwz02q1wn")))

(define-public crate-fuel-sync-0.14.1 (c (n "fuel-sync") (v "0.14.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "fuel-core-interfaces") (r "^0.14.1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "tokio") (r "^1.21") (f (quote ("full"))) (d #t) (k 0)))) (h "05xvibhjsgckp6nyv3mkwjgissgw3d9pzhviczmiqa157yaaynas")))

(define-public crate-fuel-sync-0.15.0 (c (n "fuel-sync") (v "0.15.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "fuel-core-interfaces") (r "^0.15.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "tokio") (r "^1.21") (f (quote ("full"))) (d #t) (k 0)))) (h "16vpv9nhvxxcr7lslv9c7a5jmh6gkdlfr3xr3q23z06lqgm19d1a")))

(define-public crate-fuel-sync-0.15.1 (c (n "fuel-sync") (v "0.15.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "fuel-core-interfaces") (r "^0.15.1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "tokio") (r "^1.21") (f (quote ("full"))) (d #t) (k 0)))) (h "1wgd9nbamxxdcyxyp01l7qh2qpm0f69cd7rbfhypv1sr88wq9517")))

(define-public crate-fuel-sync-0.14.2 (c (n "fuel-sync") (v "0.14.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "fuel-core-interfaces") (r "^0.14.2") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "tokio") (r "^1.21") (f (quote ("full"))) (d #t) (k 0)))) (h "0ag2xhgsqcay6z1zkldz9qzb0bazcmvrqnrpxrk72c9s1m5g8avd")))

(define-public crate-fuel-sync-0.15.3 (c (n "fuel-sync") (v "0.15.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "fuel-core-interfaces") (r "^0.15.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "tokio") (r "^1.21") (f (quote ("full"))) (d #t) (k 0)))) (h "1dggw1m8yb60m4311lcz5h3qlqfp7p71dr1i7yx4sinc1i8m8agp")))

