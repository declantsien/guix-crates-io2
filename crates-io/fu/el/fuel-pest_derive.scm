(define-module (crates-io fu el fuel-pest_derive) #:use-module (crates-io))

(define-public crate-fuel-pest_derive-3.0.4 (c (n "fuel-pest_derive") (v "3.0.4") (d (list (d (n "pest") (r "^3.0.4") (k 0) (p "fuel-pest")) (d (n "pest_generator") (r "^3.0.4") (k 0) (p "fuel-pest_generator")))) (h "049dg9hh1i4fr05mx3kmy1wk8ck0lx3jqxk4z52rvy8hr639sv90") (f (quote (("std" "pest/std" "pest_generator/std") ("default" "std"))))))

