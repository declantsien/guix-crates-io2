(define-module (crates-io fu el fuel-etk-ops) #:use-module (crates-io))

(define-public crate-fuel-etk-ops-0.3.0-dev (c (n "fuel-etk-ops") (v "0.3.0-dev") (d (list (d (n "educe") (r "^0.4.19") (d #t) (k 0)) (d (n "snafu") (r "^0.7.1") (f (quote ("std"))) (k 0)) (d (n "indexmap") (r "^1.8.1") (f (quote ("serde"))) (d #t) (k 1)) (d (n "quote") (r "^1.0.18") (d #t) (k 1)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 1)) (d (n "toml") (r "^0.5.9") (d #t) (k 1)))) (h "1wwfir0jcwxbmiham1vf3d8p6b68gsfdbxfjsjm4nyznbabxyazd") (f (quote (("backtraces" "snafu/backtraces"))))))

(define-public crate-fuel-etk-ops-0.3.1-dev (c (n "fuel-etk-ops") (v "0.3.1-dev") (d (list (d (n "educe") (r "^0.4.19") (d #t) (k 0)) (d (n "snafu") (r "^0.7.1") (f (quote ("std"))) (k 0)) (d (n "indexmap") (r "^1.8.1") (f (quote ("serde"))) (d #t) (k 1)) (d (n "quote") (r "^1.0.18") (d #t) (k 1)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 1)) (d (n "toml") (r "^0.5.9") (d #t) (k 1)))) (h "0i1xiaijvvl8dwyj36ywanma7wh96lgrfvfw8jf54h4shvl53vby") (f (quote (("backtraces" "snafu/backtraces"))))))

