(define-module (crates-io fu el fuel-dummy-test-subcrate-f) #:use-module (crates-io))

(define-public crate-fuel-dummy-test-subcrate-f-0.2.20 (c (n "fuel-dummy-test-subcrate-f") (v "0.2.20") (h "0cz7rw87y5vw6mmgwkrdachndjhpmqd4nnd0im4bhfdfjg1g7pyd")))

(define-public crate-fuel-dummy-test-subcrate-f-0.2.21 (c (n "fuel-dummy-test-subcrate-f") (v "0.2.21") (h "1p77dp9sj48amdg1i0zxznva50iwx5pd3f6lidivzr0lljc4gryk")))

(define-public crate-fuel-dummy-test-subcrate-f-0.2.22 (c (n "fuel-dummy-test-subcrate-f") (v "0.2.22") (h "0vnwxikxykqwizdpqzs1zq9138y58j532xwxnwhxl8g8bbpw22ki")))

