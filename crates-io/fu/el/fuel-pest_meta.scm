(define-module (crates-io fu el fuel-pest_meta) #:use-module (crates-io))

(define-public crate-fuel-pest_meta-3.0.4 (c (n "fuel-pest_meta") (v "3.0.4") (d (list (d (n "maplit") (r "^1.0") (d #t) (k 0)) (d (n "pest") (r "^3.0.4") (d #t) (k 0) (p "fuel-pest")) (d (n "sha-1") (r "^0.9") (k 1)))) (h "1klb469z74mm0kpwksl2zw5r83p8r42h9hy2y8y41lv696msgsxy")))

