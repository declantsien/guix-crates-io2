(define-module (crates-io fu el fuel-indexer-metrics) #:use-module (crates-io))

(define-public crate-fuel-indexer-metrics-0.0.0 (c (n "fuel-indexer-metrics") (v "0.0.0") (h "1lbwxzzrhm56fdq7c8ks5whfg5hf4fsx32naxl5ckqxvpw99rnc2")))

(define-public crate-fuel-indexer-metrics-0.1.7 (c (n "fuel-indexer-metrics") (v "0.1.7") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "prometheus") (r "^0.13") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0ah3wrd2mmp8iwha5rrfkfxnfyg4zahs0x4vvbfwbdfpdyc5i1a4")))

(define-public crate-fuel-indexer-metrics-0.1.8 (c (n "fuel-indexer-metrics") (v "0.1.8") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "prometheus") (r "^0.13") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1isliwdgblq7vvf1ys4drp462ab55v3lcc0p1mqddlgfl58wlfpp")))

(define-public crate-fuel-indexer-metrics-0.1.9 (c (n "fuel-indexer-metrics") (v "0.1.9") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "prometheus") (r "^0.13") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "08p1g51qfpa3pz2l18q251b9gff0zj6h47lryj3x9j142r85lzfv")))

(define-public crate-fuel-indexer-metrics-0.1.10 (c (n "fuel-indexer-metrics") (v "0.1.10") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "prometheus") (r "^0.13") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0x99i28yf15iz4pwp1wjyw1is9d5mic84k2l4vhcsbh0j0zsv4s3")))

(define-public crate-fuel-indexer-metrics-0.1.11 (c (n "fuel-indexer-metrics") (v "0.1.11") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "prometheus") (r "^0.13") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0vka4dvmgqz2g5xrdsf7n4rdmkgj29a0gfl9ziirndvh97jycx27")))

(define-public crate-fuel-indexer-metrics-0.1.12 (c (n "fuel-indexer-metrics") (v "0.1.12") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "prometheus") (r "^0.13") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1zqm3vqwc20izh3bfkqyvb4lggav3j4apmsz60fci9dgfibx4gnw")))

(define-public crate-fuel-indexer-metrics-0.1.13 (c (n "fuel-indexer-metrics") (v "0.1.13") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "prometheus") (r "^0.13") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1v8mvaq1wblp3x392xbx04xd8qf3wshd15iwsidik2spd3ha9wmx")))

(define-public crate-fuel-indexer-metrics-0.2.0 (c (n "fuel-indexer-metrics") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "prometheus") (r "^0.13") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1mjbikk4ibb9qayxwj7hc3f55fn371cid57bnam076c4cp4km8ib")))

(define-public crate-fuel-indexer-metrics-0.2.2 (c (n "fuel-indexer-metrics") (v "0.2.2") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "prometheus") (r "^0.13") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "004xh96mbnsbagni2vydm2413nbahfwvg8898icvx2zaw5x0vy21")))

(define-public crate-fuel-indexer-metrics-0.2.3 (c (n "fuel-indexer-metrics") (v "0.2.3") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "prometheus") (r "^0.13") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0n038fhkh26npj4bi114lg66pf7shv6vm8dwd0gfzsczwkjnvh8v")))

(define-public crate-fuel-indexer-metrics-0.3.0 (c (n "fuel-indexer-metrics") (v "0.3.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "prometheus") (r "^0.13") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "092x4f3x3nc5gf619fj8qzkww6l1ndm1bx7cmsiwnh0skkdw20r1")))

(define-public crate-fuel-indexer-metrics-0.4.0 (c (n "fuel-indexer-metrics") (v "0.4.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "prometheus") (r "^0.13") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0jhawilcg0lhssndbybcnwz3k7zrsnvfr22fg9qw8l0prcpd6wjw")))

(define-public crate-fuel-indexer-metrics-0.5.0 (c (n "fuel-indexer-metrics") (v "0.5.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "prometheus") (r "^0.13") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0i78p7i047lynia7r2aaa8gmjs0j47jfj78lbgfh15gl68sqkg00") (r "1.67.0")))

(define-public crate-fuel-indexer-metrics-0.6.0 (c (n "fuel-indexer-metrics") (v "0.6.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "prometheus") (r "^0.13") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "124z5nz0gmfzh1s3yc7f4wm6zlvjlnzf0x7wkgz35acpvsmarbnb") (r "1.67.0")))

(define-public crate-fuel-indexer-metrics-0.6.1 (c (n "fuel-indexer-metrics") (v "0.6.1") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "prometheus") (r "^0.13") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0jcm558q307qkz9i5j1kynwvpnspwrghnaczh2fylaj8dnf89kjl") (r "1.67.0")))

(define-public crate-fuel-indexer-metrics-0.7.0 (c (n "fuel-indexer-metrics") (v "0.7.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "prometheus") (r "^0.13") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "099clhg3cs0qyj67f93pdmh4avr6pjvrazg8dmlh2rygsxpax4il") (r "1.68.1")))

(define-public crate-fuel-indexer-metrics-0.8.0 (c (n "fuel-indexer-metrics") (v "0.8.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "prometheus") (r "^0.13") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0a35wwby8v9ziz9cfw6yqrbdh3a5ygndik92jwqjyizvc2w4f4f2") (r "1.68.1")))

(define-public crate-fuel-indexer-metrics-0.9.0 (c (n "fuel-indexer-metrics") (v "0.9.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "prometheus") (r "^0.13") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "105h7kvwcd8qg2c6dqnz0xij0y9dfaxzdfvk35ym2w1jjvcmlz5v") (r "1.68.1")))

(define-public crate-fuel-indexer-metrics-0.10.0 (c (n "fuel-indexer-metrics") (v "0.10.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "prometheus") (r "^0.13") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "015pnww3m4x7h97am8abxfj01gbhpl2d2qp7jpkk54pfcrw22sq9") (r "1.68.1")))

(define-public crate-fuel-indexer-metrics-0.11.0 (c (n "fuel-indexer-metrics") (v "0.11.0") (d (list (d (n "axum") (r "^0.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "prometheus") (r "^0.13") (d #t) (k 0)) (d (n "prometheus-client") (r "^0.20") (d #t) (k 0)))) (h "09knya38pxs6yvb7labjs3m9slv4k3wfygpnjq2jf9ayw0cy4650") (r "1.68.1")))

(define-public crate-fuel-indexer-metrics-0.11.1 (c (n "fuel-indexer-metrics") (v "0.11.1") (d (list (d (n "axum") (r "^0.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "prometheus") (r "^0.13") (d #t) (k 0)) (d (n "prometheus-client") (r "^0.20") (d #t) (k 0)))) (h "1qhzl8y7imd05zaysbq3a3c31phgc2z8lpn9pjpcw8db6xib6h0r") (r "1.68.1")))

(define-public crate-fuel-indexer-metrics-0.11.2 (c (n "fuel-indexer-metrics") (v "0.11.2") (d (list (d (n "axum") (r "^0.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "prometheus") (r "^0.13") (d #t) (k 0)) (d (n "prometheus-client") (r "^0.20") (d #t) (k 0)))) (h "0j0z9szcjg5lszypdyb53fd0p5k5c634sv652pjyqkia4i7inbd6") (r "1.68.1")))

(define-public crate-fuel-indexer-metrics-0.12.0 (c (n "fuel-indexer-metrics") (v "0.12.0") (d (list (d (n "axum") (r "^0.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "prometheus") (r "^0.13") (d #t) (k 0)) (d (n "prometheus-client") (r "^0.20") (d #t) (k 0)))) (h "0qkcc92wph7q85pgs3nzsj8yjsbygl1k31jgl8nyjmfshp4fszi8") (r "1.69.0")))

(define-public crate-fuel-indexer-metrics-0.12.1 (c (n "fuel-indexer-metrics") (v "0.12.1") (d (list (d (n "axum") (r "^0.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "prometheus") (r "^0.13") (d #t) (k 0)) (d (n "prometheus-client") (r "^0.20") (d #t) (k 0)))) (h "1c9p7sazxqhg6nj7h9d5bk6j37qj8p9b8spjskyj4lpdzwd2qiqp") (r "1.69.0")))

(define-public crate-fuel-indexer-metrics-0.13.0 (c (n "fuel-indexer-metrics") (v "0.13.0") (d (list (d (n "axum") (r "^0.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "prometheus") (r "^0.13") (d #t) (k 0)) (d (n "prometheus-client") (r "^0.20") (d #t) (k 0)))) (h "1mhdgn5mmyab42in31bwa251r389czk9g2mbkbd8pk7bax1z02n4") (r "1.69.0")))

(define-public crate-fuel-indexer-metrics-0.13.2 (c (n "fuel-indexer-metrics") (v "0.13.2") (d (list (d (n "axum") (r "^0.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "prometheus") (r "^0.13") (d #t) (k 0)) (d (n "prometheus-client") (r "^0.20") (d #t) (k 0)))) (h "0bxq3dzgi367s7vrd6aih82g379c4nccaa1j1w1dm6m85jiyk9dx") (r "1.69.0")))

(define-public crate-fuel-indexer-metrics-0.14.0 (c (n "fuel-indexer-metrics") (v "0.14.0") (d (list (d (n "axum") (r "^0.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "prometheus") (r "^0.13") (d #t) (k 0)) (d (n "prometheus-client") (r "^0.20") (d #t) (k 0)))) (h "08kqr0n585n0gn2r3kdwlih606dy4xr9sn3w9ww5akch59bksi6a") (r "1.69.0")))

(define-public crate-fuel-indexer-metrics-0.15.0 (c (n "fuel-indexer-metrics") (v "0.15.0") (d (list (d (n "axum") (r "^0.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "prometheus") (r "^0.13") (d #t) (k 0)) (d (n "prometheus-client") (r "^0.20") (d #t) (k 0)))) (h "1as7552yxjnj7ssqi4h7myi6l6zn8074mdq2pj5rfxgkngz94k4g") (r "1.69.0")))

(define-public crate-fuel-indexer-metrics-0.15.1 (c (n "fuel-indexer-metrics") (v "0.15.1") (d (list (d (n "axum") (r "^0.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "prometheus") (r "^0.13") (d #t) (k 0)) (d (n "prometheus-client") (r "^0.20") (d #t) (k 0)))) (h "1j7chgzh617mp15bq7wnyaj8zd0g074iiklfn104prln25sqaw3h") (r "1.69.0")))

(define-public crate-fuel-indexer-metrics-0.16.0 (c (n "fuel-indexer-metrics") (v "0.16.0") (d (list (d (n "axum") (r "^0.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "prometheus") (r "^0.13") (d #t) (k 0)) (d (n "prometheus-client") (r "^0.20") (d #t) (k 0)))) (h "0zv8ijb4n2ac5f6w41anlhca8m3ls8n5a91b867c581z2gqlw74j") (r "1.69.0")))

(define-public crate-fuel-indexer-metrics-0.16.1 (c (n "fuel-indexer-metrics") (v "0.16.1") (d (list (d (n "axum") (r "^0.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "prometheus") (r "^0.13") (d #t) (k 0)) (d (n "prometheus-client") (r "^0.20") (d #t) (k 0)))) (h "1jbiplp9kazhqzvhm6v39kv4pj1gxdvjhfqg3agis9kr7kncrbjm") (r "1.69.0")))

(define-public crate-fuel-indexer-metrics-0.16.2 (c (n "fuel-indexer-metrics") (v "0.16.2") (d (list (d (n "axum") (r "^0.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "prometheus") (r "^0.13") (d #t) (k 0)) (d (n "prometheus-client") (r "^0.20") (d #t) (k 0)))) (h "174xbp7mv38nh7l5rkslg3n9fk9ciq2j508jkzhcphigsnvm9l03") (r "1.69.0")))

(define-public crate-fuel-indexer-metrics-0.17.0 (c (n "fuel-indexer-metrics") (v "0.17.0") (d (list (d (n "axum") (r "^0.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "prometheus") (r "^0.13") (d #t) (k 0)) (d (n "prometheus-client") (r "^0.20") (d #t) (k 0)))) (h "0n3i7isdqgkqfqsm88igc5dzzccdfwsrgg3959jj21s1cwmyp3h9") (r "1.69.0")))

(define-public crate-fuel-indexer-metrics-0.17.1 (c (n "fuel-indexer-metrics") (v "0.17.1") (d (list (d (n "axum") (r "^0.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "prometheus") (r "^0.13") (d #t) (k 0)) (d (n "prometheus-client") (r "^0.20") (d #t) (k 0)))) (h "19l6qmyn5w0lmf7v3vpgdb7s3w8nv118rmsv0izm1fc30nmk64ma") (r "1.69.0")))

(define-public crate-fuel-indexer-metrics-0.17.2 (c (n "fuel-indexer-metrics") (v "0.17.2") (d (list (d (n "axum") (r "^0.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "prometheus") (r "^0.13") (d #t) (k 0)) (d (n "prometheus-client") (r "^0.20") (d #t) (k 0)))) (h "0bk4iqwpigb6icgw0mrv3n2x7q7mz0rhl1cn51g8d4vc7bhisayy") (r "1.69.0")))

(define-public crate-fuel-indexer-metrics-0.17.3 (c (n "fuel-indexer-metrics") (v "0.17.3") (d (list (d (n "axum") (r "^0.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "prometheus") (r "^0.13") (d #t) (k 0)) (d (n "prometheus-client") (r "^0.20") (d #t) (k 0)))) (h "1x7gxvm0z39ij3gsqvr2g1alm6gns7mzsfcgwzayf39x9bhcwivq") (r "1.69.0")))

(define-public crate-fuel-indexer-metrics-0.18.0 (c (n "fuel-indexer-metrics") (v "0.18.0") (d (list (d (n "axum") (r "^0.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "prometheus") (r "^0.13") (d #t) (k 0)) (d (n "prometheus-client") (r "^0.20") (d #t) (k 0)))) (h "04qxzja9pl209hgv6g506v2iy5py8hfvwrc0skznk6zl7kjlkhgs") (r "1.69.0")))

(define-public crate-fuel-indexer-metrics-0.18.1 (c (n "fuel-indexer-metrics") (v "0.18.1") (d (list (d (n "axum") (r "^0.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "prometheus") (r "^0.13") (d #t) (k 0)) (d (n "prometheus-client") (r "^0.20") (d #t) (k 0)))) (h "10g364rfdy9w9wlkwvs55zq0mlgfi0x9pakz2gmydpx45j8rcpd0") (r "1.69.0")))

(define-public crate-fuel-indexer-metrics-0.18.2 (c (n "fuel-indexer-metrics") (v "0.18.2") (d (list (d (n "axum") (r "^0.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "prometheus") (r "^0.13") (d #t) (k 0)) (d (n "prometheus-client") (r "^0.20") (d #t) (k 0)))) (h "1c5w7v4kyqwkr7199hhr046mwm235h5h8jb8naajnha28rqc5fym") (r "1.69.0")))

(define-public crate-fuel-indexer-metrics-0.18.3 (c (n "fuel-indexer-metrics") (v "0.18.3") (d (list (d (n "axum") (r "^0.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "prometheus") (r "^0.13") (d #t) (k 0)) (d (n "prometheus-client") (r "^0.20") (d #t) (k 0)))) (h "0z4cr9jg53pw1v4hd6dk3y2arszari6csrzz7zcvf60y6zmqhiws") (r "1.69.0")))

(define-public crate-fuel-indexer-metrics-0.18.4 (c (n "fuel-indexer-metrics") (v "0.18.4") (d (list (d (n "axum") (r "^0.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "prometheus") (r "^0.13") (d #t) (k 0)) (d (n "prometheus-client") (r "^0.20") (d #t) (k 0)))) (h "07bq06mjr7ci14mld6vnkzbfxx6fg68m0m71q7ly479m16qfjzz4") (r "1.69.0")))

(define-public crate-fuel-indexer-metrics-0.18.5 (c (n "fuel-indexer-metrics") (v "0.18.5") (d (list (d (n "axum") (r "^0.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "prometheus") (r "^0.13") (d #t) (k 0)) (d (n "prometheus-client") (r "^0.20") (d #t) (k 0)))) (h "16r0hvdjm42r1zc0amffv7mn58ifhla23h99k44ahy4414phwa6r") (r "1.69.0")))

(define-public crate-fuel-indexer-metrics-0.19.0 (c (n "fuel-indexer-metrics") (v "0.19.0") (d (list (d (n "axum") (r "^0.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "prometheus") (r "^0.13") (d #t) (k 0)) (d (n "prometheus-client") (r "^0.20") (d #t) (k 0)))) (h "1wr27hpdix8q87ll67syvzdyj2w7mmr4129z43wl74r48ms7lpbb") (r "1.69.0")))

(define-public crate-fuel-indexer-metrics-0.19.1 (c (n "fuel-indexer-metrics") (v "0.19.1") (d (list (d (n "axum") (r "^0.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "prometheus") (r "^0.13") (d #t) (k 0)) (d (n "prometheus-client") (r "^0.20") (d #t) (k 0)))) (h "05h2qgqz5352sz164569a9iw0pnc6f5isnljcidlac569wyg0af4") (r "1.69.0")))

(define-public crate-fuel-indexer-metrics-0.19.2 (c (n "fuel-indexer-metrics") (v "0.19.2") (d (list (d (n "axum") (r "^0.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "prometheus") (r "^0.13") (d #t) (k 0)) (d (n "prometheus-client") (r "^0.20") (d #t) (k 0)))) (h "04gapaak9qnbk15xg79kpvfbihzykr0283a1bmrh1gpym6363k8g") (r "1.69.0")))

(define-public crate-fuel-indexer-metrics-0.19.3 (c (n "fuel-indexer-metrics") (v "0.19.3") (d (list (d (n "axum") (r "^0.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "prometheus") (r "^0.13") (d #t) (k 0)) (d (n "prometheus-client") (r "^0.20") (d #t) (k 0)))) (h "0grwzknql5y0lq69yajq73s4dhn78j916gawi7zbpv1ai6jspdql") (r "1.69.0")))

(define-public crate-fuel-indexer-metrics-0.19.4 (c (n "fuel-indexer-metrics") (v "0.19.4") (d (list (d (n "axum") (r "^0.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "prometheus") (r "^0.13") (d #t) (k 0)) (d (n "prometheus-client") (r "^0.20") (d #t) (k 0)))) (h "1jaamjil20s2z06rs26awghn1fznwa0pjrdldrgmmsikrdzwija2") (r "1.69.0")))

(define-public crate-fuel-indexer-metrics-0.19.5 (c (n "fuel-indexer-metrics") (v "0.19.5") (d (list (d (n "axum") (r "^0.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "prometheus") (r "^0.13") (d #t) (k 0)) (d (n "prometheus-client") (r "^0.20") (d #t) (k 0)))) (h "0j639vddaclwynqpff736hwscfvrsd31xmngi2dcnwsgmx3lq5gv") (r "1.69.0")))

(define-public crate-fuel-indexer-metrics-0.20.0 (c (n "fuel-indexer-metrics") (v "0.20.0") (d (list (d (n "axum") (r "^0.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "prometheus") (r "^0.13") (d #t) (k 0)) (d (n "prometheus-client") (r "^0.20") (d #t) (k 0)))) (h "0z19gwnwwkx2i79p1f7kq4vpwrvf074xlh3lapzwkglhh4xidqj4") (r "1.71.1")))

(define-public crate-fuel-indexer-metrics-0.20.1 (c (n "fuel-indexer-metrics") (v "0.20.1") (d (list (d (n "axum") (r "^0.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "prometheus") (r "^0.13") (d #t) (k 0)) (d (n "prometheus-client") (r "^0.20") (d #t) (k 0)))) (h "0fhf2r7x1mg9yvqm8nx62asiz6kncp5zwqsxlmgqb2ya0glmdwxw") (r "1.71.1")))

(define-public crate-fuel-indexer-metrics-0.20.2 (c (n "fuel-indexer-metrics") (v "0.20.2") (d (list (d (n "axum") (r "^0.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "prometheus") (r "^0.13") (d #t) (k 0)) (d (n "prometheus-client") (r "^0.20") (d #t) (k 0)))) (h "1ym3f0m2n3yg1d1rhc1iyvhqki62m51378r58qd4y2y6jzzqfxhf") (r "1.71.1")))

(define-public crate-fuel-indexer-metrics-0.20.3 (c (n "fuel-indexer-metrics") (v "0.20.3") (d (list (d (n "axum") (r "^0.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "prometheus") (r "^0.13") (d #t) (k 0)) (d (n "prometheus-client") (r "^0.20") (d #t) (k 0)))) (h "1gxx1gflrgfncg3c7armvx47fs58xlzya6s9ssydk8na3cca3ynb") (r "1.71.1")))

(define-public crate-fuel-indexer-metrics-0.20.4 (c (n "fuel-indexer-metrics") (v "0.20.4") (d (list (d (n "axum") (r "^0.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "prometheus") (r "^0.13") (d #t) (k 0)) (d (n "prometheus-client") (r "^0.20") (d #t) (k 0)))) (h "05k402cqfi7gskna1l5w6036qc4kiwi19hlgl8dj8jp0yzhzximf") (r "1.71.1")))

(define-public crate-fuel-indexer-metrics-0.20.5 (c (n "fuel-indexer-metrics") (v "0.20.5") (d (list (d (n "axum") (r "^0.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "prometheus") (r "^0.13") (d #t) (k 0)) (d (n "prometheus-client") (r "^0.20") (d #t) (k 0)))) (h "0395v2zsxziwsflqvj3z7aqkwkwpjsvvfw0b6w6x8jpn76z4fv17") (r "1.72.0")))

(define-public crate-fuel-indexer-metrics-0.20.6 (c (n "fuel-indexer-metrics") (v "0.20.6") (d (list (d (n "axum") (r "^0.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "prometheus") (r "^0.13") (d #t) (k 0)) (d (n "prometheus-client") (r "^0.20") (d #t) (k 0)))) (h "1k6r0lc334adm8kv85fx1pnfijndn0c2cr0y8l8qrlhz8dyl8nlf") (r "1.72.0")))

(define-public crate-fuel-indexer-metrics-0.20.7 (c (n "fuel-indexer-metrics") (v "0.20.7") (d (list (d (n "axum") (r "^0.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "prometheus") (r "^0.13") (d #t) (k 0)) (d (n "prometheus-client") (r "^0.20") (d #t) (k 0)))) (h "0gm5kgbq58i04jm5mmdfy1bi104djc4la5arxh392v793wa5vcpl") (r "1.72.0")))

(define-public crate-fuel-indexer-metrics-0.20.8 (c (n "fuel-indexer-metrics") (v "0.20.8") (d (list (d (n "axum") (r "^0.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "prometheus") (r "^0.13") (d #t) (k 0)) (d (n "prometheus-client") (r "^0.20") (d #t) (k 0)))) (h "0w7h559dv6n4ibs0ahb8w0gb911y9f0a3bg619rmhd59dxs366l8") (r "1.72.0")))

(define-public crate-fuel-indexer-metrics-0.20.9 (c (n "fuel-indexer-metrics") (v "0.20.9") (d (list (d (n "axum") (r "^0.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "prometheus") (r "^0.13") (d #t) (k 0)) (d (n "prometheus-client") (r "^0.20") (d #t) (k 0)))) (h "0n8ar754fz18vhp0bqcxgwji2hnkdxdz7w4fw9ij6s032c6lzn58") (r "1.72.0")))

(define-public crate-fuel-indexer-metrics-0.20.10 (c (n "fuel-indexer-metrics") (v "0.20.10") (d (list (d (n "axum") (r "^0.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "prometheus") (r "^0.13") (d #t) (k 0)) (d (n "prometheus-client") (r "^0.20") (d #t) (k 0)))) (h "1cm01qwlik94pd7ary0ja0m3q6nlq28afx6nlnjkii5fby2gx7q2") (r "1.72.0")))

(define-public crate-fuel-indexer-metrics-0.20.11 (c (n "fuel-indexer-metrics") (v "0.20.11") (d (list (d (n "axum") (r "^0.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "prometheus") (r "^0.13") (d #t) (k 0)) (d (n "prometheus-client") (r "^0.20") (d #t) (k 0)))) (h "1z2qrlijkzn9z19b9ak0dgg4xz1sr7whyplrp21p4i53l9c5z290") (r "1.72.0")))

(define-public crate-fuel-indexer-metrics-0.21.0 (c (n "fuel-indexer-metrics") (v "0.21.0") (d (list (d (n "axum") (r "^0.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "prometheus") (r "^0.13") (d #t) (k 0)) (d (n "prometheus-client") (r "^0.20") (d #t) (k 0)))) (h "1y9chimgdx9c84x18ini9wcdc4qygs11np7biw5c9jwq9rr3il09") (r "1.72.1")))

(define-public crate-fuel-indexer-metrics-0.21.1 (c (n "fuel-indexer-metrics") (v "0.21.1") (d (list (d (n "axum") (r "^0.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "prometheus") (r "^0.13") (d #t) (k 0)) (d (n "prometheus-client") (r "^0.20") (d #t) (k 0)))) (h "0xwsp8yhs7fhr25l4ns51mhxq1c43in8sizzxhnnsfcjfm397mxk") (r "1.73.0")))

(define-public crate-fuel-indexer-metrics-0.21.2 (c (n "fuel-indexer-metrics") (v "0.21.2") (d (list (d (n "axum") (r "^0.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "prometheus") (r "^0.13") (d #t) (k 0)) (d (n "prometheus-client") (r "^0.20") (d #t) (k 0)))) (h "13cg4y3rhh94ayp1f476xdzshzfyn6bkcbpp4ck3rx54pdd8akjr") (r "1.73.0")))

(define-public crate-fuel-indexer-metrics-0.22.0 (c (n "fuel-indexer-metrics") (v "0.22.0") (d (list (d (n "axum") (r "^0.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "prometheus") (r "^0.13") (d #t) (k 0)) (d (n "prometheus-client") (r "^0.20") (d #t) (k 0)))) (h "0yy5hw0ipahag1fw1hn3yvym2xw6svzch4ighbi9hv3wy21w1qdq") (r "1.73.0")))

(define-public crate-fuel-indexer-metrics-0.23.0 (c (n "fuel-indexer-metrics") (v "0.23.0") (d (list (d (n "axum") (r "^0.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "prometheus") (r "^0.13") (d #t) (k 0)) (d (n "prometheus-client") (r "^0.20") (d #t) (k 0)))) (h "0zdn0c40pcwm0x55y0bay25x0zyci6fw9fdwg964h4znzjzngj6s") (r "1.73.0")))

(define-public crate-fuel-indexer-metrics-0.24.0 (c (n "fuel-indexer-metrics") (v "0.24.0") (d (list (d (n "axum") (r "^0.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "prometheus") (r "^0.13") (d #t) (k 0)) (d (n "prometheus-client") (r "^0.20") (d #t) (k 0)))) (h "0ciy311cria46bcal0m75aah5jw3yzm6lgdgr353mjwpb4l2dhaj") (r "1.73.0")))

(define-public crate-fuel-indexer-metrics-0.24.1 (c (n "fuel-indexer-metrics") (v "0.24.1") (d (list (d (n "axum") (r "^0.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "prometheus") (r "^0.13") (d #t) (k 0)) (d (n "prometheus-client") (r "^0.20") (d #t) (k 0)))) (h "10lwqd56byigb05jiibqbi48wh4qjnbm4bjg6id06z790xxxcwpk") (r "1.73.0")))

(define-public crate-fuel-indexer-metrics-0.24.2 (c (n "fuel-indexer-metrics") (v "0.24.2") (d (list (d (n "axum") (r "^0.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "prometheus") (r "^0.13") (d #t) (k 0)) (d (n "prometheus-client") (r "^0.20") (d #t) (k 0)))) (h "1niydm81sv2fm842ddq3zqqpxbi7v9kkkbczmvnbm8ips503jw1f") (r "1.73.0")))

(define-public crate-fuel-indexer-metrics-0.24.3 (c (n "fuel-indexer-metrics") (v "0.24.3") (d (list (d (n "axum") (r "^0.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "prometheus") (r "^0.13") (d #t) (k 0)) (d (n "prometheus-client") (r "^0.20") (d #t) (k 0)))) (h "0c3a0bx8dx08dk63x5b9ch17dydi625r34zj5lc5gnfxs21xm00g") (r "1.73.0")))

(define-public crate-fuel-indexer-metrics-0.25.0 (c (n "fuel-indexer-metrics") (v "0.25.0") (d (list (d (n "axum") (r "^0.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "prometheus") (r "^0.13") (d #t) (k 0)) (d (n "prometheus-client") (r "^0.20") (d #t) (k 0)))) (h "1ivia15zyp7d2jhsj1s0b876wckg2k1ixi4wjhic5ahm753c3v1x") (r "1.75.0")))

