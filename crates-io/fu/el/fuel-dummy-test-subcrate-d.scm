(define-module (crates-io fu el fuel-dummy-test-subcrate-d) #:use-module (crates-io))

(define-public crate-fuel-dummy-test-subcrate-d-0.2.20 (c (n "fuel-dummy-test-subcrate-d") (v "0.2.20") (h "1k5d2qj6z36dlma3s3g6l34hp4sr3nj3d3kjzkqacdxba70cmgxk")))

(define-public crate-fuel-dummy-test-subcrate-d-0.2.21 (c (n "fuel-dummy-test-subcrate-d") (v "0.2.21") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 0)))) (h "0v97cqyfycrkza1yw6wildzyj9g3gwgbnqnql1kj0inql9v61njs")))

(define-public crate-fuel-dummy-test-subcrate-d-0.2.22 (c (n "fuel-dummy-test-subcrate-d") (v "0.2.22") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 2)))) (h "1k066csilsds55khz7hfcqc04bzlv6fgcjgi1fiq7czbd1z0jb5r")))

