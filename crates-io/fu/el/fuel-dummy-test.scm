(define-module (crates-io fu el fuel-dummy-test) #:use-module (crates-io))

(define-public crate-fuel-dummy-test-0.1.0 (c (n "fuel-dummy-test") (v "0.1.0") (h "10qz6h7107zziv1mr3vw1n7h822rss89fm1qiz1apbywgx6iavzb")))

(define-public crate-fuel-dummy-test-0.2.0 (c (n "fuel-dummy-test") (v "0.2.0") (h "13kgnhx460iwvyb760cgws5hms37kcr6xjwkjiz5lx78xl95lvia")))

(define-public crate-fuel-dummy-test-0.2.3 (c (n "fuel-dummy-test") (v "0.2.3") (h "151jirxx11isj1dqva3wc7p3xn6awywk4yci790mlkwk5jspdh2z")))

(define-public crate-fuel-dummy-test-0.2.4 (c (n "fuel-dummy-test") (v "0.2.4") (h "081m165f8c4r2c2ra79vy0340cb1finizfz7zyynw0v26d1a4zqw")))

