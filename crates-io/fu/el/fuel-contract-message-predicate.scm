(define-module (crates-io fu el fuel-contract-message-predicate) #:use-module (crates-io))

(define-public crate-fuel-contract-message-predicate-0.2.0 (c (n "fuel-contract-message-predicate") (v "0.2.0") (d (list (d (n "fuel-asm") (r "^0.31.2") (d #t) (k 0)) (d (n "fuel-core-interfaces") (r "^0.15.3") (d #t) (k 2)) (d (n "fuel-tx") (r "^0.31.2") (d #t) (k 0)) (d (n "fuels") (r "^0.43.0") (f (quote ("fuel-core-lib"))) (d #t) (k 2)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "insta") (r "^1.28") (d #t) (k 2)) (d (n "sha2") (r "^0.10.6") (d #t) (k 0)) (d (n "tokio") (r "^1.27") (f (quote ("rt" "macros"))) (d #t) (k 2)))) (h "1cvpf8qh6ns9yjvpx33lx0zzm3a82mlnk7jj7g0qix0wgbijgsfw") (r "1.69.0")))

