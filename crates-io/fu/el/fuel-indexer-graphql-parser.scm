(define-module (crates-io fu el fuel-indexer-graphql-parser) #:use-module (crates-io))

(define-public crate-fuel-indexer-graphql-parser-0.0.0 (c (n "fuel-indexer-graphql-parser") (v "0.0.0") (h "147i7r2s21f4lxlxlxdrss3h16af2bpln6lgivv0689xnk1a3pd9")))

(define-public crate-fuel-indexer-graphql-parser-0.10.0 (c (n "fuel-indexer-graphql-parser") (v "0.10.0") (d (list (d (n "combine") (r "^3.2.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.5.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.11") (d #t) (k 0)))) (h "0aj8rmc27m4hy9il3r44sz9c6xsi6i4hw6fiyjga3drmir2n8d80") (r "1.68.1")))

(define-public crate-fuel-indexer-graphql-parser-0.11.0 (c (n "fuel-indexer-graphql-parser") (v "0.11.0") (d (list (d (n "combine") (r "^3.2.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.5.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.11") (d #t) (k 0)))) (h "1wpxlmpjcfk09djm1axdcz5z3ybp65b2qa8vgkqf7n6ljqb32b25") (r "1.68.1")))

(define-public crate-fuel-indexer-graphql-parser-0.11.1 (c (n "fuel-indexer-graphql-parser") (v "0.11.1") (d (list (d (n "combine") (r "^3.2.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.5.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.11") (d #t) (k 0)))) (h "1ifxvn81jbzxw3gb83m8p38kcaafxvc0n7vqp72l26klk5hgy4va") (r "1.68.1")))

(define-public crate-fuel-indexer-graphql-parser-0.11.2 (c (n "fuel-indexer-graphql-parser") (v "0.11.2") (d (list (d (n "combine") (r "^3.2.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.5.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.11") (d #t) (k 0)))) (h "1av2y77f426b0ck5y35msi0fc37mdv3z3hdvglfs5s1kax3jsfqv") (r "1.68.1")))

