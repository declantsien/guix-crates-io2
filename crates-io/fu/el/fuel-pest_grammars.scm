(define-module (crates-io fu el fuel-pest_grammars) #:use-module (crates-io))

(define-public crate-fuel-pest_grammars-3.0.4 (c (n "fuel-pest_grammars") (v "3.0.4") (d (list (d (n "pest") (r "^3.0.4") (d #t) (k 0) (p "fuel-pest")) (d (n "pest_derive") (r "^3.0.4") (d #t) (k 0) (p "fuel-pest_derive")))) (h "1sp0pjnlqp40lbinnc4axp8gmh0fqamklid9xgcnz1ay49d1hfdn")))

