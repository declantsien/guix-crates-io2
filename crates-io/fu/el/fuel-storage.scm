(define-module (crates-io fu el fuel-storage) #:use-module (crates-io))

(define-public crate-fuel-storage-0.1.0 (c (n "fuel-storage") (v "0.1.0") (h "1kj2y4bg190yh59lc8gg8l1m0fbirmmh4isscinli3ipqdl6lypr")))

(define-public crate-fuel-storage-0.2.0 (c (n "fuel-storage") (v "0.2.0") (h "1mrgz61narpygfibgjxvndsf0hhzyyj315a0i0qajd6lhqg1df9l")))

(define-public crate-fuel-storage-0.3.0 (c (n "fuel-storage") (v "0.3.0") (h "1hy4s8l1bcckb6w427g989pgxwwgc14zk307xdh2siqq7m19by5h")))

(define-public crate-fuel-storage-0.24.0 (c (n "fuel-storage") (v "0.24.0") (h "19dqv71d86xa64h1a6bnlvxv93jizw1wfgd42ys4k4i5wh5gla5j")))

(define-public crate-fuel-storage-0.25.0 (c (n "fuel-storage") (v "0.25.0") (h "0c9srzmg7mxfms1vada7p154389c6mpdm1zz0yxzawqpk8dm58v3")))

(define-public crate-fuel-storage-0.25.1 (c (n "fuel-storage") (v "0.25.1") (h "0fasphrmxx3r6pbp4lkzqd7pj46jhps1ckqqwrf1nl7ixixf181b")))

(define-public crate-fuel-storage-0.25.2 (c (n "fuel-storage") (v "0.25.2") (h "04j2i03wlby30gjbsa1lmcpmbcmws3nl9xw3v5fd3lf5qy16w1wd")))

(define-public crate-fuel-storage-0.25.3 (c (n "fuel-storage") (v "0.25.3") (h "1m5lk3p2nl4i2nknh1g8wdz405yf3jrmawrgl9hy89w18ppwzzr0")))

(define-public crate-fuel-storage-0.26.0 (c (n "fuel-storage") (v "0.26.0") (h "0kqn5gp6zmwfmz2cyk5zliv9rpwjykmpw8vk61df6gdss6km8cxr")))

(define-public crate-fuel-storage-0.26.1 (c (n "fuel-storage") (v "0.26.1") (h "01cywila0h4dwkk9s0kybffjpja0zycdzfddkbnx0qmlzahnjyfc")))

(define-public crate-fuel-storage-0.27.0 (c (n "fuel-storage") (v "0.27.0") (h "1vrlxfzrrm2976xxas9ipmgkv5yab7qzks76cg20s0n6n5dh99vl")))

(define-public crate-fuel-storage-0.28.1 (c (n "fuel-storage") (v "0.28.1") (h "1g0k3hm8k1xp7a20k566x5rxnm5fv3sj5nd6025r08j3sjnfks77")))

(define-public crate-fuel-storage-0.29.0 (c (n "fuel-storage") (v "0.29.0") (h "1skd07gav87avhi132as93qzlfkimic03jc7g5v16zp74dpmr3aj")))

(define-public crate-fuel-storage-0.30.0 (c (n "fuel-storage") (v "0.30.0") (h "1hh6nm4hwpid4j5fa5kswv2rdm5ckk83i4y78r22gimsk7514chp")))

(define-public crate-fuel-storage-0.26.2 (c (n "fuel-storage") (v "0.26.2") (h "11s33gyx3sb9fw1yilvkzz734i5kqj72khci6kclj174mkvjqiwr")))

(define-public crate-fuel-storage-0.26.3 (c (n "fuel-storage") (v "0.26.3") (h "1sbp6bf8x6jslqnivq3l8jpcm1gyq9sm01s8wd2ybqwadj94k3cr")))

(define-public crate-fuel-storage-0.31.0 (c (n "fuel-storage") (v "0.31.0") (h "0n77r6p4vplm4x3cbqahgr4qvgngv0dw20h06smwgqg13f56c2fx")))

(define-public crate-fuel-storage-0.31.1 (c (n "fuel-storage") (v "0.31.1") (h "1yxzgsy2vh6k3gvl45ij5skhlqvw858n2vd6dzqqaxhrypklbmxb")))

(define-public crate-fuel-storage-0.32.0 (c (n "fuel-storage") (v "0.32.0") (h "1r2jk5y3izfavrgm6j4dslpcf26x64h9wgbr46szr5b6fisy5k5d")))

(define-public crate-fuel-storage-0.33.0 (c (n "fuel-storage") (v "0.33.0") (h "1fp06haarrr6bdcbqaamg2pj1kxsy2pxvc4b3bg3adrf37f8014z")))

(define-public crate-fuel-storage-0.31.2 (c (n "fuel-storage") (v "0.31.2") (h "09qvb963vyhws55isgnmwzsqjmh80dl1rf10sb8vcm03m56l8whv")))

(define-public crate-fuel-storage-0.34.0 (c (n "fuel-storage") (v "0.34.0") (h "0frg94qhgxc2yrd9c40rx4kgj278wcq7hy4dchyizxfz8pa3wwxj")))

(define-public crate-fuel-storage-0.34.1 (c (n "fuel-storage") (v "0.34.1") (h "0cndm9xk3kvr0p3h1dbv2hzx16kdjwl7fwcg778pph5v93hprqsm")))

(define-public crate-fuel-storage-0.35.0 (c (n "fuel-storage") (v "0.35.0") (h "037dmchw9y1cyhmvga8a43cqvw21pp96s3wb4wppd4m31m0nix05")))

(define-public crate-fuel-storage-0.35.1 (c (n "fuel-storage") (v "0.35.1") (h "1dn8d6dkd9pjk4pwsaqf7fhmqcriaqbq30ldxy2ghvycv9bwhxd7")))

(define-public crate-fuel-storage-0.35.2 (c (n "fuel-storage") (v "0.35.2") (h "015a6zvs7ww0m3ak9lrv5jhbf7clin0jrq40758bx13c9ikyyj90")))

(define-public crate-fuel-storage-0.35.3 (c (n "fuel-storage") (v "0.35.3") (h "017jxxrvblsv43ap2carvjfziga2ww6zpyl5s6yyaq8llw8jc1b9")))

(define-public crate-fuel-storage-0.36.0 (c (n "fuel-storage") (v "0.36.0") (h "1m00w01spj65d6f1zyfklrfgxdnif191nj5hwwjky0m2hidzhsfv")))

(define-public crate-fuel-storage-0.36.1 (c (n "fuel-storage") (v "0.36.1") (h "18xp8xvi4djpryx7k6j0jlf8p7wvwllld5fssxrw9cvhjb2c56g5")))

(define-public crate-fuel-storage-0.35.4 (c (n "fuel-storage") (v "0.35.4") (h "0vcps489hdyx140gn6yz0r4l3s2xrbd4cv03d9zym7g5kc0qn65f")))

(define-public crate-fuel-storage-0.37.0 (c (n "fuel-storage") (v "0.37.0") (h "1fffnq5zpwh3cq9vk8cgliafr0hziplc1x4s65n5bs11swxpm3ka")))

(define-public crate-fuel-storage-0.38.0 (c (n "fuel-storage") (v "0.38.0") (h "0ahy0b1vd34zkj9c6l8v9w98yj2lscybmf32z06bcn51sycsffgd")))

(define-public crate-fuel-storage-0.39.0 (c (n "fuel-storage") (v "0.39.0") (h "1vn9jfr2i34y1dmi1gd1pgcx3spasvlhn135pcsw166rmnfxbhyx")))

(define-public crate-fuel-storage-0.40.0 (c (n "fuel-storage") (v "0.40.0") (h "0irfgzc3wj8xgzlg83iixv41m079ib877bk9gic48pqr5in2hvb3")))

(define-public crate-fuel-storage-0.41.0 (c (n "fuel-storage") (v "0.41.0") (h "01s6rr1dlxkf7wxab17id2x8an0db2cr5rbfj2jliw5jz0bi35qj")))

(define-public crate-fuel-storage-0.42.0 (c (n "fuel-storage") (v "0.42.0") (h "1d1camrcwy3n7y49dasi9gvfxkjjq075j4l6ih446j09c018582x")))

(define-public crate-fuel-storage-0.42.1 (c (n "fuel-storage") (v "0.42.1") (h "0gy7h2m61a6ipgmrla6cygm1bmcwdchjb73xdfnxq06l6k6dsafw")))

(define-public crate-fuel-storage-0.42.2 (c (n "fuel-storage") (v "0.42.2") (h "124jymrdb5bv1k0v4r9knqwjghshvi608c51jkcz57r1m14pvyhj")))

(define-public crate-fuel-storage-0.42.3 (c (n "fuel-storage") (v "0.42.3") (h "1jldwcl2770j34v50hbcimvlbx0w95b0cc2zfz3pl9bnsbfax145")))

(define-public crate-fuel-storage-0.43.0 (c (n "fuel-storage") (v "0.43.0") (h "1h1gdg5lqj749cxw9ddkyis02a9s4fd51x9ybbfk6vmq5jvpqscp")))

(define-public crate-fuel-storage-0.43.1 (c (n "fuel-storage") (v "0.43.1") (h "16w7nczqmv1ckc5f1h71h0nr81wqgqdk9327a5fcsp2qmj6bs84z")))

(define-public crate-fuel-storage-0.43.2 (c (n "fuel-storage") (v "0.43.2") (h "1apxn0iypfa4gd8678y5g9rxmk5qq5d0xm045hylhkk88r5yw6lh")))

(define-public crate-fuel-storage-0.44.0 (c (n "fuel-storage") (v "0.44.0") (h "1fcqi8l6873413c7g5xjajk7jw0s88n8kmjqcxnvmx195z67dsby")))

(define-public crate-fuel-storage-0.45.0 (c (n "fuel-storage") (v "0.45.0") (h "10c52iac0878jwznkm46qll2di8ghaf7vvfxq8bm3qxwj8fx4wmr")))

(define-public crate-fuel-storage-0.46.0 (c (n "fuel-storage") (v "0.46.0") (h "1qpyvw13qbnc55h9ni2zq77hn5scn52zn7mygnnh3divflf2ax7k")))

(define-public crate-fuel-storage-0.47.0 (c (n "fuel-storage") (v "0.47.0") (h "171vazky00w0z5jkp8f0qsp1fz8kj3bj3q4qryljd7l9qdkdwfk3")))

(define-public crate-fuel-storage-0.47.1 (c (n "fuel-storage") (v "0.47.1") (h "1lpnhvfcnialkq1c0jfgw4rfqzb4qiccn2csxd08hi2ffixraj0h")))

(define-public crate-fuel-storage-0.48.0 (c (n "fuel-storage") (v "0.48.0") (h "0l9k2nx4gpf1ihnzm41likiiq26i51m8vyddn0vda2cbpwmlb6b8")))

(define-public crate-fuel-storage-0.49.0 (c (n "fuel-storage") (v "0.49.0") (h "1l1dfkm2q94wq5wwbyyfmw15ya4sxh4g3blzy0m32gsg4jf8wwzs")))

(define-public crate-fuel-storage-0.50.0 (c (n "fuel-storage") (v "0.50.0") (h "0m9h29r0hzdf418q5yr7wxa8gj55pg313xmbyrzfh601qh95zvxy")))

