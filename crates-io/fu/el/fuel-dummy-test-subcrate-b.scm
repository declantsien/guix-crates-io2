(define-module (crates-io fu el fuel-dummy-test-subcrate-b) #:use-module (crates-io))

(define-public crate-fuel-dummy-test-subcrate-b-0.2.5 (c (n "fuel-dummy-test-subcrate-b") (v "0.2.5") (h "0l8nba3jmsdayk30y73xp5zpk9lr94zl5v8q07cav17w4d04q53p")))

(define-public crate-fuel-dummy-test-subcrate-b-0.2.6 (c (n "fuel-dummy-test-subcrate-b") (v "0.2.6") (h "15ai49nm5xhr3dn1rbd92yaqn7slhq2xmfv0cibw1n7f4wq4mmav")))

(define-public crate-fuel-dummy-test-subcrate-b-0.2.8 (c (n "fuel-dummy-test-subcrate-b") (v "0.2.8") (h "02x6ysjssdqi58jv549mfl2d7vgjfps034jvfsdz35bhd0r4mgav")))

(define-public crate-fuel-dummy-test-subcrate-b-0.2.9 (c (n "fuel-dummy-test-subcrate-b") (v "0.2.9") (h "13kaj3934wr91hzw10mp2kpv97d49y8yd6cgp84b2b4g0m6rxvj7")))

(define-public crate-fuel-dummy-test-subcrate-b-0.2.10 (c (n "fuel-dummy-test-subcrate-b") (v "0.2.10") (h "1ng87150b8g0r7jd7dpxzf817q9i0046qazq3281hbw81ri1vr8d")))

(define-public crate-fuel-dummy-test-subcrate-b-0.2.11 (c (n "fuel-dummy-test-subcrate-b") (v "0.2.11") (d (list (d (n "fuel-dummy-test-subcrate-a") (r "^0.2.11") (d #t) (k 0)) (d (n "fuel-dummy-test-subcrate-c") (r "^0.2.11") (d #t) (k 2)))) (h "0bmcnn99xqd37n5bcb1lc164zx0sgwwsn3n2ach5ywyrcvf7sh3k")))

(define-public crate-fuel-dummy-test-subcrate-b-0.2.19 (c (n "fuel-dummy-test-subcrate-b") (v "0.2.19") (d (list (d (n "fuel-dummy-test-subcrate-a") (r "^0.2.19") (d #t) (k 0)))) (h "1s8vc8dfpby26p5gmjcnfwv7prxlan47jlsp2xrki8bhkyisfcym")))

(define-public crate-fuel-dummy-test-subcrate-b-0.2.20 (c (n "fuel-dummy-test-subcrate-b") (v "0.2.20") (d (list (d (n "fuel-dummy-test-subcrate-a") (r "^0.2.20") (d #t) (k 0)))) (h "0hv9bs16ikak6nx2qz03vm9yr30wn0fldc7hwb7pkkshirygh8bq")))

(define-public crate-fuel-dummy-test-subcrate-b-0.2.21 (c (n "fuel-dummy-test-subcrate-b") (v "0.2.21") (d (list (d (n "fuel-dummy-test-subcrate-a") (r "^0.2.21") (d #t) (k 0)))) (h "0285v8vw6z9br6ccax1aps37vmprmbn0jrh2xvc3saszjifi7qy0")))

(define-public crate-fuel-dummy-test-subcrate-b-0.2.22 (c (n "fuel-dummy-test-subcrate-b") (v "0.2.22") (d (list (d (n "fuel-dummy-test-subcrate-a") (r "^0.2.22") (d #t) (k 0)))) (h "0vkb7dp4dl3dqk8c5vp9kif0c47b0s3bm3swlci7k0rchbchcr90")))

