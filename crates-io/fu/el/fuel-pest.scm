(define-module (crates-io fu el fuel-pest) #:use-module (crates-io))

(define-public crate-fuel-pest-3.0.2 (c (n "fuel-pest") (v "3.0.2") (d (list (d (n "serde") (r "^1.0.89") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (o #t) (d #t) (k 0)) (d (n "ucd-trie") (r "^0.1.1") (d #t) (k 0)))) (h "0g3pwlv75ms2ivkxx0ik86g19s0hcl76dbizafyc7gsl4czmhxdd") (f (quote (("std") ("pretty-print" "serde" "serde_json") ("default" "std") ("const_prec_climber"))))))

(define-public crate-fuel-pest-3.0.4 (c (n "fuel-pest") (v "3.0.4") (d (list (d (n "serde") (r "^1.0.89") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (o #t) (d #t) (k 0)) (d (n "ucd-trie") (r "^0.1.1") (d #t) (k 0)))) (h "19h5nfqdig7fz6f677zawjgc77d5gdqk0myfswmhw3xgpfz83mxv") (f (quote (("std") ("pretty-print" "serde" "serde_json") ("default" "std") ("const_prec_climber"))))))

