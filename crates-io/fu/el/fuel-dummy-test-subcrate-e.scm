(define-module (crates-io fu el fuel-dummy-test-subcrate-e) #:use-module (crates-io))

(define-public crate-fuel-dummy-test-subcrate-e-0.2.20 (c (n "fuel-dummy-test-subcrate-e") (v "0.2.20") (d (list (d (n "fuel-dummy-test-subcrate-d") (r "^0.2.20") (d #t) (k 0)))) (h "0wpalpn3xpjs230i1zrbwycc0rq07ac9fzsc7n2rbs1sgdyjyj8i")))

(define-public crate-fuel-dummy-test-subcrate-e-0.2.21 (c (n "fuel-dummy-test-subcrate-e") (v "0.2.21") (d (list (d (n "fuel-dummy-test-subcrate-d") (r "^0.2.21") (d #t) (k 0)))) (h "0z01hffrs470fl6p6cx16k3ck1prgijnb2bjp5xbl9fna2bj3si3")))

(define-public crate-fuel-dummy-test-subcrate-e-0.2.22 (c (n "fuel-dummy-test-subcrate-e") (v "0.2.22") (d (list (d (n "fuel-dummy-test-subcrate-d") (r "^0.2.22") (d #t) (k 0)))) (h "1iqlw30nvkc4dk3r45952rac7qkh13lsz6m9fda9m7m7sydppz9q")))

