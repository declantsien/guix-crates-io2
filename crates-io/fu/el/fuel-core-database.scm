(define-module (crates-io fu el fuel-core-database) #:use-module (crates-io))

(define-public crate-fuel-core-database-0.16.1 (c (n "fuel-core-database") (v "0.16.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "fuel-core-storage") (r "^0.16.1") (d #t) (k 0)) (d (n "fuel-core-types") (r "^0.16.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1lcisskb52362s1zs3rj3s1dsp0hqzwb34xy6g7c874bps0nf3hs")))

(define-public crate-fuel-core-database-0.17.0 (c (n "fuel-core-database") (v "0.17.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "fuel-core-storage") (r "^0.17.0") (d #t) (k 0)) (d (n "fuel-core-types") (r "^0.17.0") (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "19d553fkqlap2siskxh1s2j5kcmbi7hdvh8d6vpg0yh34r77ygm6")))

(define-public crate-fuel-core-database-0.17.1 (c (n "fuel-core-database") (v "0.17.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "fuel-core-storage") (r "^0.17.1") (d #t) (k 0)) (d (n "fuel-core-types") (r "^0.17.1") (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1nlh83psyrrvc8ikz3sn3bamb7cf6mpbzk29f6lpp5zz7zwjz36f")))

(define-public crate-fuel-core-database-0.17.2 (c (n "fuel-core-database") (v "0.17.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "fuel-core-storage") (r "^0.17.2") (d #t) (k 0)) (d (n "fuel-core-types") (r "^0.17.2") (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1k3k9y53l31va3d93x4j5zsvkz47psmjz46cacd4jw0clra9sjdf")))

(define-public crate-fuel-core-database-0.17.3 (c (n "fuel-core-database") (v "0.17.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "fuel-core-storage") (r "^0.17.3") (d #t) (k 0)) (d (n "fuel-core-types") (r "^0.17.3") (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "02j0pjy4inlm4nz3vwk3bvbklhxqnkccyfwwfc9xcd8sjd4svvxw")))

(define-public crate-fuel-core-database-0.17.4 (c (n "fuel-core-database") (v "0.17.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "fuel-core-storage") (r "^0.17.4") (d #t) (k 0)) (d (n "fuel-core-types") (r "^0.17.4") (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "09ax08v43rdnp1xwxdvv5bvravrj632y5p5x1nn88inb4mx3bxqc")))

(define-public crate-fuel-core-database-0.17.5 (c (n "fuel-core-database") (v "0.17.5") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "fuel-core-storage") (r "^0.17.5") (d #t) (k 0)) (d (n "fuel-core-types") (r "^0.17.5") (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1q1zydk3c896asgy5iq55gyl7n0314vyw05d6mm0zrs7v1qaqv1c")))

(define-public crate-fuel-core-database-0.17.6 (c (n "fuel-core-database") (v "0.17.6") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "fuel-core-storage") (r "^0.17.6") (d #t) (k 0)) (d (n "fuel-core-types") (r "^0.17.6") (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "17grpxhbsij8nls8aj0ncywkpzrb4zvzydarg1qq1rmjikygi6nb")))

(define-public crate-fuel-core-database-0.17.7 (c (n "fuel-core-database") (v "0.17.7") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "fuel-core-storage") (r "^0.17.7") (d #t) (k 0)) (d (n "fuel-core-types") (r "^0.17.7") (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "07vwns6j230ng19cvj6m92ly45l5gpshabh2xk7l5iaflzyyr8is")))

(define-public crate-fuel-core-database-0.17.8 (c (n "fuel-core-database") (v "0.17.8") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "fuel-core-storage") (r "^0.17.8") (d #t) (k 0)) (d (n "fuel-core-types") (r "^0.17.8") (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0gifn81lf2mjbf4v0c9pbvhnazy542qwm1flqgbyb0nx87zh4213")))

(define-public crate-fuel-core-database-0.17.9 (c (n "fuel-core-database") (v "0.17.9") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "fuel-core-storage") (r "^0.17.9") (d #t) (k 0)) (d (n "fuel-core-types") (r "^0.17.9") (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1ycnag2ljpfm09yzz6634f72arvywc9rsx6xp13c364s300j2dxy")))

(define-public crate-fuel-core-database-0.17.10 (c (n "fuel-core-database") (v "0.17.10") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "fuel-core-storage") (r "^0.17.10") (d #t) (k 0)) (d (n "fuel-core-types") (r "^0.17.10") (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "10snmqhg419j46x4j8p4b7fwvq70s249jhm9ac52wr4vmg3q4ark")))

(define-public crate-fuel-core-database-0.17.11 (c (n "fuel-core-database") (v "0.17.11") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "fuel-core-storage") (r "^0.17.11") (d #t) (k 0)) (d (n "fuel-core-types") (r "^0.17.11") (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0q3phlrlfm4njscas047iqz5w4cwzwdwl94vpp0cxk81jjgy7x53")))

(define-public crate-fuel-core-database-0.18.0 (c (n "fuel-core-database") (v "0.18.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "fuel-core-storage") (r "^0.18.0") (d #t) (k 0)) (d (n "fuel-core-types") (r "^0.18.0") (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "08dkzi1frs8p0mp4d16dzljwi7rww3rwjcv9mabfwvv9kmfybr4h")))

(define-public crate-fuel-core-database-0.18.1 (c (n "fuel-core-database") (v "0.18.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "fuel-core-storage") (r "^0.18.1") (d #t) (k 0)) (d (n "fuel-core-types") (r "^0.18.1") (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1vnfbhx70mh1ykagimwkriwdl6szk8w7ih899gfg7sp4ap803i0v")))

(define-public crate-fuel-core-database-0.17.12 (c (n "fuel-core-database") (v "0.17.12") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "fuel-core-storage") (r "^0.17.12") (d #t) (k 0)) (d (n "fuel-core-types") (r "^0.17.12") (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1aaw5jx33mi9zq4ih89kxmrlv5sy5zq5qs3mixaiy2476990ca3c")))

(define-public crate-fuel-core-database-0.18.2 (c (n "fuel-core-database") (v "0.18.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "fuel-core-storage") (r "^0.18.2") (d #t) (k 0)) (d (n "fuel-core-types") (r "^0.18.2") (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "050c2crqx50zk11wclpiajplhq5nnn4y7dxw8wx3d303n161d7qd")))

(define-public crate-fuel-core-database-0.18.3 (c (n "fuel-core-database") (v "0.18.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "fuel-core-storage") (r "^0.18.3") (d #t) (k 0)) (d (n "fuel-core-types") (r "^0.18.3") (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1114cmqsxd7hh78hz4vbl2i8lbmsryz2rqwfwsjxzdz82a7qynwg")))

(define-public crate-fuel-core-database-0.19.0 (c (n "fuel-core-database") (v "0.19.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "fuel-core-storage") (r "^0.19.0") (d #t) (k 0)) (d (n "fuel-core-types") (r "^0.19.0") (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0783cyr09w6g937q1177l62hdah3rj70j0bfaszldmfzvc6d88hl")))

(define-public crate-fuel-core-database-0.19.1 (c (n "fuel-core-database") (v "0.19.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "fuel-core-storage") (r "^0.19.1") (d #t) (k 0)) (d (n "fuel-core-types") (r "^0.19.1") (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1x6f2ig54c595301fdl6a7fqbdl3lmxcb5zwg3n2wvql6ykh4vv5")))

(define-public crate-fuel-core-database-0.17.13 (c (n "fuel-core-database") (v "0.17.13") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "fuel-core-storage") (r "^0.17.13") (d #t) (k 0)) (d (n "fuel-core-types") (r "^0.17.13") (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0xjykryvpid3fjcydbf30kz40q4zb5n1nldswqgji2fc146gv5qs")))

(define-public crate-fuel-core-database-0.20.0 (c (n "fuel-core-database") (v "0.20.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "fuel-core-storage") (r "^0.20.0") (d #t) (k 0)) (d (n "fuel-core-types") (r "^0.20.0") (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1na2axfvhsi9vndzihgib8kpl8x1kvij2vakr8sshc662rv2s4ql")))

(define-public crate-fuel-core-database-0.20.1 (c (n "fuel-core-database") (v "0.20.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "fuel-core-storage") (r "^0.20.1") (d #t) (k 0)) (d (n "fuel-core-types") (r "^0.20.1") (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0lv1hc8q0x2x3pgy6wkb9w6ddga157gp243a3jwksdbjcnwl216i")))

(define-public crate-fuel-core-database-0.20.2 (c (n "fuel-core-database") (v "0.20.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "fuel-core-storage") (r "^0.20.2") (d #t) (k 0)) (d (n "fuel-core-types") (r "^0.20.2") (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1f8r1sdlaf0ywkr1gm4sxl2z3hamfk82k9671337a5vxax9xkpcf")))

(define-public crate-fuel-core-database-0.20.3 (c (n "fuel-core-database") (v "0.20.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "fuel-core-storage") (r "^0.20.3") (d #t) (k 0)) (d (n "fuel-core-types") (r "^0.20.3") (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0vmgmar7vhpjf3irwbnkvxy07b0q4sd8pbbzky85nyyw96n218gi")))

(define-public crate-fuel-core-database-0.20.4 (c (n "fuel-core-database") (v "0.20.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "fuel-core-storage") (r "^0.20.4") (d #t) (k 0)) (d (n "fuel-core-types") (r "^0.20.4") (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "12rkgrjc7mmlrr6l2c0kcgmkb08himg7x6bzwl90wg4qrmcicjqv")))

(define-public crate-fuel-core-database-0.19.2 (c (n "fuel-core-database") (v "0.19.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "fuel-core-storage") (r "^0.19.2") (d #t) (k 0)) (d (n "fuel-core-types") (r "^0.19.2") (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1k97c7nk4bqqyd6ry8k1l4q0wi5yaxfc1khapwlkilc0dj0xq00n")))

(define-public crate-fuel-core-database-0.20.5 (c (n "fuel-core-database") (v "0.20.5") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "fuel-core-storage") (r "^0.20.5") (d #t) (k 0)) (d (n "fuel-core-types") (r "^0.20.5") (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1xqb3lcl3by3g087hahpdsm7h4kvlk82fs9bqa6bjhdphpifhmij")))

(define-public crate-fuel-core-database-0.20.6 (c (n "fuel-core-database") (v "0.20.6") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "fuel-core-storage") (r "^0.20.6") (d #t) (k 0)) (d (n "fuel-core-types") (r "^0.20.6") (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1rymmbv3dypksw6llgxxpb67ly93a4szabqr4x4fp0w9p0fljvxh")))

(define-public crate-fuel-core-database-0.20.7 (c (n "fuel-core-database") (v "0.20.7") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "fuel-core-storage") (r "^0.20.7") (d #t) (k 0)) (d (n "fuel-core-types") (r "^0.20.7") (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1g0jxwzd5r1jb5hq3irzh5bamdbdrjjvgzk6sq9hjhqpk29c0rgh")))

(define-public crate-fuel-core-database-0.20.8 (c (n "fuel-core-database") (v "0.20.8") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "fuel-core-storage") (r "^0.20.8") (d #t) (k 0)) (d (n "fuel-core-types") (r "^0.20.8") (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0f8lnih4s4d92l19j7l6xd62ch8d10rih0kvkiin82a0k21rnij2")))

(define-public crate-fuel-core-database-0.21.0-rc.1 (c (n "fuel-core-database") (v "0.21.0-rc.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "fuel-core-storage") (r "^0.21.0-rc.1") (d #t) (k 0)) (d (n "fuel-core-types") (r "^0.21.0-rc.1") (k 0)))) (h "1qzrlpw8rfra5bx6kl8pnilm5y5s87qw13kdbvdgdsicfrnqnj53")))

(define-public crate-fuel-core-database-0.21.0 (c (n "fuel-core-database") (v "0.21.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "fuel-core-storage") (r "^0.21.0") (d #t) (k 0)) (d (n "fuel-core-types") (r "^0.21.0") (k 0)))) (h "02grip69n3qpckjjxivardjhk1q9kvqinm5lf66dzq9ddba7qyzv")))

(define-public crate-fuel-core-database-0.22.0 (c (n "fuel-core-database") (v "0.22.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "fuel-core-storage") (r "^0.22.0") (d #t) (k 0)) (d (n "fuel-core-types") (r "^0.22.0") (k 0)))) (h "0329rnidmy1h6zb7adsl1lxajbjxk45anqw62xiyc0km0bbjs80i")))

(define-public crate-fuel-core-database-0.22.1 (c (n "fuel-core-database") (v "0.22.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "fuel-core-storage") (r "^0.22.1") (d #t) (k 0)) (d (n "fuel-core-types") (r "^0.22.1") (k 0)))) (h "11xz9yqs5pln33lw0h4pp63h56ps6gn425ssm8p9hkm9c5fr6v6y")))

(define-public crate-fuel-core-database-0.23.0 (c (n "fuel-core-database") (v "0.23.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "fuel-core-storage") (r "^0.23.0") (d #t) (k 0)) (d (n "fuel-core-types") (r "^0.23.0") (k 0)))) (h "08l0br1zq9cjxng5dcczj1fyfm4n2cjwrk6qf79szaq0cy1wh5cv")))

(define-public crate-fuel-core-database-0.22.2 (c (n "fuel-core-database") (v "0.22.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "fuel-core-storage") (r "^0.22.2") (d #t) (k 0)) (d (n "fuel-core-types") (r "^0.22.2") (k 0)))) (h "0kkipckk5dyxcs9mbji9y7vf653sdszv3429s06yvqzc4q95svwa")))

(define-public crate-fuel-core-database-0.22.3 (c (n "fuel-core-database") (v "0.22.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "fuel-core-storage") (r "^0.22.3") (d #t) (k 0)) (d (n "fuel-core-types") (r "^0.22.3") (k 0)))) (h "0c5ab0xhzagcmrbssm3f0l7pn4bmi6fml9kp6wp41lc8l786iahm")))

(define-public crate-fuel-core-database-0.22.4 (c (n "fuel-core-database") (v "0.22.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "fuel-core-storage") (r "^0.22.4") (d #t) (k 0)) (d (n "fuel-core-types") (r "^0.22.4") (k 0)))) (h "0cggqx2vwzf02ibs2jwbqkjjkmzlrw2y9r6dr085f2mxbzp47anq")))

(define-public crate-fuel-core-database-0.24.0 (c (n "fuel-core-database") (v "0.24.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "fuel-core-storage") (r "^0.24.0") (k 0)) (d (n "fuel-core-types") (r "^0.24.0") (k 0)))) (h "00fp6y7w77imnvmqvfgjrvvhzqji2cp658j9ki9y8kz1b3h5017y")))

(define-public crate-fuel-core-database-0.24.1 (c (n "fuel-core-database") (v "0.24.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "fuel-core-storage") (r "^0.24.1") (k 0)) (d (n "fuel-core-types") (r "^0.24.1") (k 0)))) (h "0mkyzxvv41wvcj8blpwdfiyhyxdywcvpr45mqwhhahyjb947d83x")))

(define-public crate-fuel-core-database-0.24.2 (c (n "fuel-core-database") (v "0.24.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "fuel-core-storage") (r "^0.24.2") (k 0)) (d (n "fuel-core-types") (r "^0.24.2") (k 0)))) (h "0pabcbpd4xzvl73izsj0mv2wq1sing2k2p5fbj6j4iw4w5anr5kq")))

(define-public crate-fuel-core-database-0.25.0 (c (n "fuel-core-database") (v "0.25.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "fuel-core-storage") (r "^0.25.0") (k 0)) (d (n "fuel-core-types") (r "^0.25.0") (k 0)))) (h "03ch46lxxfcgw9x7x624pl02s2ax3hya5nm6yc4221z8h44lb4w4")))

(define-public crate-fuel-core-database-0.25.1 (c (n "fuel-core-database") (v "0.25.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "fuel-core-storage") (r "^0.25.1") (k 0)) (d (n "fuel-core-types") (r "^0.25.1") (k 0)))) (h "0fr6g3nsqaz58adbwsajxjgn0788yw54s930591jln6cjqxmd57b")))

(define-public crate-fuel-core-database-0.25.2 (c (n "fuel-core-database") (v "0.25.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "fuel-core-storage") (r "^0.25.2") (k 0)) (d (n "fuel-core-types") (r "^0.25.2") (k 0)))) (h "0swb7vmy78vrpp97pz7d0lriqpjwlcsx4d72h8qp291z9qyrp79y")))

(define-public crate-fuel-core-database-0.24.3 (c (n "fuel-core-database") (v "0.24.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "fuel-core-storage") (r "^0.24.3") (k 0)) (d (n "fuel-core-types") (r "^0.24.3") (k 0)))) (h "0giw1f8darpll531fpjr80sj0xmqnd5r0wv699fj18yma4iwr1rs")))

(define-public crate-fuel-core-database-0.25.3 (c (n "fuel-core-database") (v "0.25.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "fuel-core-storage") (r "^0.25.3") (k 0)) (d (n "fuel-core-types") (r "^0.25.3") (k 0)))) (h "0w2ggsznhvnawv6k31h1jx5533yybm979wc2mydscizdvk7b9f1q")))

(define-public crate-fuel-core-database-0.26.0 (c (n "fuel-core-database") (v "0.26.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "fuel-core-storage") (r "^0.26.0") (k 0)) (d (n "fuel-core-types") (r "^0.26.0") (k 0)))) (h "0sscckqbbh5463yg9s8mpram082bgaimjqqhr3qfn296lw4gvcgm") (f (quote (("test-helpers"))))))

(define-public crate-fuel-core-database-0.27.0 (c (n "fuel-core-database") (v "0.27.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "fuel-core-storage") (r "^0.27.0") (k 0)) (d (n "fuel-core-types") (r "^0.27.0") (k 0)))) (h "1jcn669bj5bignhbxg61xg7zjn67s1arsl02k7isqdlx81mv6h6m") (f (quote (("test-helpers"))))))

