(define-module (crates-io fu el fuel-metrics) #:use-module (crates-io))

(define-public crate-fuel-metrics-0.11.0 (c (n "fuel-metrics") (v "0.11.0") (d (list (d (n "axum") (r "^0.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "prometheus") (r "^0.13") (d #t) (k 0)))) (h "1ynp27bclmzw9pvsb6lp6z51pr94zhdn1v0l35a9836zzhdmy0n2")))

(define-public crate-fuel-metrics-0.11.1 (c (n "fuel-metrics") (v "0.11.1") (d (list (d (n "axum") (r "^0.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "prometheus") (r "^0.13") (d #t) (k 0)))) (h "1v4zbfyslwaj871gry4v3bzcb2dl4rjsjlb684lpccpxjmkqarlc")))

(define-public crate-fuel-metrics-0.11.2 (c (n "fuel-metrics") (v "0.11.2") (d (list (d (n "axum") (r "^0.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "prometheus") (r "^0.13") (d #t) (k 0)))) (h "1h3rcha0hiyksbs4m916hnjsg70zh8afyck8crrzqz9mx8flbna4")))

(define-public crate-fuel-metrics-0.12.0 (c (n "fuel-metrics") (v "0.12.0") (d (list (d (n "axum") (r "^0.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "prometheus") (r "^0.13") (d #t) (k 0)))) (h "1fmnkl6yxqr7j14sbznphn0d4xxn0ih836qaxxh08jgzkhpdskfa")))

(define-public crate-fuel-metrics-0.13.0 (c (n "fuel-metrics") (v "0.13.0") (d (list (d (n "axum") (r "^0.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "prometheus") (r "^0.13") (d #t) (k 0)))) (h "0jl118p4l7m4359pfig3hnaj97sq6s1p9qn1wxkm9lxnmrn15xfy")))

(define-public crate-fuel-metrics-0.13.1 (c (n "fuel-metrics") (v "0.13.1") (d (list (d (n "axum") (r "^0.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "prometheus") (r "^0.13") (d #t) (k 0)))) (h "1x15xib9dpnycsgrvzcdsfbl26q1hkspwha811nll6dvhzik43n3")))

(define-public crate-fuel-metrics-0.13.2 (c (n "fuel-metrics") (v "0.13.2") (d (list (d (n "axum") (r "^0.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "prometheus") (r "^0.13") (d #t) (k 0)))) (h "12l6gzkqryp635w5w3nxxjrii1pi0b4y0cickd400z69kx1sayf9")))

(define-public crate-fuel-metrics-0.14.0 (c (n "fuel-metrics") (v "0.14.0") (d (list (d (n "axum") (r "^0.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "prometheus") (r "^0.13") (d #t) (k 0)))) (h "0i8j6axprh4pah7m7dvkldfnn7yin4y76f6dmiyps72vqd0kn99h")))

(define-public crate-fuel-metrics-0.14.1 (c (n "fuel-metrics") (v "0.14.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "axum") (r "^0.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.16") (d #t) (k 0)) (d (n "prometheus-client") (r "^0.18") (d #t) (k 0)))) (h "126ra5cflaiwa13qj8pwqs1rlxv1z7qbpaflv9rdyb99b5s7n1v1")))

(define-public crate-fuel-metrics-0.15.0 (c (n "fuel-metrics") (v "0.15.0") (d (list (d (n "axum") (r "^0.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.16") (d #t) (k 0)) (d (n "prometheus-client") (r "^0.18") (d #t) (k 0)))) (h "0j3sba75ka4zmpa76njh4hnggbfrfc6yc6gmn5m1vrxsvc6j700f")))

(define-public crate-fuel-metrics-0.15.1 (c (n "fuel-metrics") (v "0.15.1") (d (list (d (n "axum") (r "^0.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.16") (d #t) (k 0)) (d (n "prometheus-client") (r "^0.18") (d #t) (k 0)))) (h "1n529nlbvlxyp7jgq2za1q6cwzcg13qmyh6p5ry9vqsml2yh1d5v")))

(define-public crate-fuel-metrics-0.14.2 (c (n "fuel-metrics") (v "0.14.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "axum") (r "^0.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.16") (d #t) (k 0)) (d (n "prometheus-client") (r "^0.18") (d #t) (k 0)))) (h "02n75ic6571f3xyg610zr2nxjbmh2v02bxi84a8mqqykcg1k60al")))

(define-public crate-fuel-metrics-0.15.3 (c (n "fuel-metrics") (v "0.15.3") (d (list (d (n "axum") (r "^0.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.16") (d #t) (k 0)) (d (n "prometheus-client") (r "^0.18") (d #t) (k 0)))) (h "1llzx7r0z4mp5vrjnz36ia1glxmagw9n3xdskabbkssw41s0jmfg")))

