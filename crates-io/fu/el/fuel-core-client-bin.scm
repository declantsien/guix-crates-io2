(define-module (crates-io fu el fuel-core-client-bin) #:use-module (crates-io))

(define-public crate-fuel-core-client-bin-0.16.1 (c (n "fuel-core-client-bin") (v "0.16.1") (d (list (d (n "clap") (r "^4.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fuel-core-client") (r "^0.16.1") (d #t) (k 0)) (d (n "fuel-core-types") (r "^0.16.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("raw_value"))) (d #t) (k 0)) (d (n "tokio") (r "^1.21") (f (quote ("macros"))) (d #t) (k 0)))) (h "1bvszbdkaidl5dihkbprd0s1vjqzr9hk4fjiin4g9d2mjnxihg9z")))

(define-public crate-fuel-core-client-bin-0.17.0 (c (n "fuel-core-client-bin") (v "0.17.0") (d (list (d (n "clap") (r "^4.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fuel-core-client") (r "^0.17.0") (d #t) (k 0)) (d (n "fuel-core-types") (r "^0.17.0") (f (quote ("serde"))) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("raw_value"))) (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("macros"))) (d #t) (k 0)))) (h "0qj86kc7acwxs72z02vi5jybsfhpiq949dbckv8m710fzvw80lj6")))

(define-public crate-fuel-core-client-bin-0.17.1 (c (n "fuel-core-client-bin") (v "0.17.1") (d (list (d (n "clap") (r "^4.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fuel-core-client") (r "^0.17.1") (d #t) (k 0)) (d (n "fuel-core-types") (r "^0.17.1") (f (quote ("serde"))) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("raw_value"))) (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("macros"))) (d #t) (k 0)))) (h "13x87f123naqz85iykj9ja911s57m5glfq5nzx6rcmckgv5y5mwl")))

(define-public crate-fuel-core-client-bin-0.17.2 (c (n "fuel-core-client-bin") (v "0.17.2") (d (list (d (n "clap") (r "^4.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fuel-core-client") (r "^0.17.2") (d #t) (k 0)) (d (n "fuel-core-types") (r "^0.17.2") (f (quote ("serde"))) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("raw_value"))) (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("macros"))) (d #t) (k 0)))) (h "151n0yl8hxayscn09mmvxa018skc0y1h7z24q677j06n611q6xp4")))

(define-public crate-fuel-core-client-bin-0.17.3 (c (n "fuel-core-client-bin") (v "0.17.3") (d (list (d (n "clap") (r "^4.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fuel-core-client") (r "^0.17.3") (d #t) (k 0)) (d (n "fuel-core-types") (r "^0.17.3") (f (quote ("serde"))) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("raw_value"))) (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("macros"))) (d #t) (k 0)))) (h "0knrxi86i6921dp0l9hr9fwaskdy77lk23cada60r6pslb3m56pa")))

(define-public crate-fuel-core-client-bin-0.17.4 (c (n "fuel-core-client-bin") (v "0.17.4") (d (list (d (n "clap") (r "^4.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fuel-core-client") (r "^0.17.4") (d #t) (k 0)) (d (n "fuel-core-types") (r "^0.17.4") (f (quote ("serde"))) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("raw_value"))) (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("macros"))) (d #t) (k 0)))) (h "05yw8qm428ssm1m7mzm6d4vsxd02zvvm0dw3ncschw9s9pn1b3a6")))

(define-public crate-fuel-core-client-bin-0.17.5 (c (n "fuel-core-client-bin") (v "0.17.5") (d (list (d (n "clap") (r "^4.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fuel-core-client") (r "^0.17.5") (d #t) (k 0)) (d (n "fuel-core-types") (r "^0.17.5") (f (quote ("serde"))) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("raw_value"))) (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("macros"))) (d #t) (k 0)))) (h "008hb283rfrpgcm0q2addxlhwc38hxd19rnggbbnzns8scxj19vm")))

(define-public crate-fuel-core-client-bin-0.17.6 (c (n "fuel-core-client-bin") (v "0.17.6") (d (list (d (n "clap") (r "^4.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fuel-core-client") (r "^0.17.6") (d #t) (k 0)) (d (n "fuel-core-types") (r "^0.17.6") (f (quote ("serde"))) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("raw_value"))) (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("macros"))) (d #t) (k 0)))) (h "1cprmzh1aphkaja9li36a1w0rhxwq0ir6ijw52qh796mj3s2iy82")))

(define-public crate-fuel-core-client-bin-0.17.7 (c (n "fuel-core-client-bin") (v "0.17.7") (d (list (d (n "clap") (r "^4.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fuel-core-client") (r "^0.17.7") (d #t) (k 0)) (d (n "fuel-core-types") (r "^0.17.7") (f (quote ("serde"))) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("raw_value"))) (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("macros"))) (d #t) (k 0)))) (h "1sfixv8vn5kq5z7ax46810v990hp8z09srj1yvlpfrpxh7njdvy8")))

(define-public crate-fuel-core-client-bin-0.17.8 (c (n "fuel-core-client-bin") (v "0.17.8") (d (list (d (n "clap") (r "^4.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fuel-core-client") (r "^0.17.8") (d #t) (k 0)) (d (n "fuel-core-types") (r "^0.17.8") (f (quote ("serde"))) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("raw_value"))) (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("macros"))) (d #t) (k 0)))) (h "17yc1r65qdr14kvb87sw8r8vyh6nrn9fg3h9hqg97nsrvpbkaqzw")))

(define-public crate-fuel-core-client-bin-0.17.9 (c (n "fuel-core-client-bin") (v "0.17.9") (d (list (d (n "clap") (r "^4.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fuel-core-client") (r "^0.17.9") (d #t) (k 0)) (d (n "fuel-core-types") (r "^0.17.9") (f (quote ("serde"))) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("raw_value"))) (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("macros"))) (d #t) (k 0)))) (h "0bnl3ycrn7882kpkb24d0a7mrbkavb5917fc8ma883ii2627lf7q")))

(define-public crate-fuel-core-client-bin-0.17.10 (c (n "fuel-core-client-bin") (v "0.17.10") (d (list (d (n "clap") (r "^4.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fuel-core-client") (r "^0.17.10") (d #t) (k 0)) (d (n "fuel-core-types") (r "^0.17.10") (f (quote ("serde"))) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("raw_value"))) (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("macros"))) (d #t) (k 0)))) (h "1vz24kl6n3g2ag44kgxf057k9kmz13jiwlwyrk1cx4s1j418a31a")))

(define-public crate-fuel-core-client-bin-0.17.11 (c (n "fuel-core-client-bin") (v "0.17.11") (d (list (d (n "clap") (r "^4.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fuel-core-client") (r "^0.17.11") (d #t) (k 0)) (d (n "fuel-core-types") (r "^0.17.11") (f (quote ("serde"))) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("raw_value"))) (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("macros"))) (d #t) (k 0)))) (h "0cfs2icrci1b2lyy5fgdd02j4qfzmbdrchbnvmxwhn9xx5cwa1nk")))

(define-public crate-fuel-core-client-bin-0.18.0 (c (n "fuel-core-client-bin") (v "0.18.0") (d (list (d (n "clap") (r "^4.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fuel-core-client") (r "^0.18.0") (d #t) (k 0)) (d (n "fuel-core-types") (r "^0.18.0") (f (quote ("serde"))) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("raw_value"))) (d #t) (k 0)) (d (n "tokio") (r "^1.27") (f (quote ("macros"))) (d #t) (k 0)))) (h "0v19g4xcmwadz17lq56fm9imflgrma0iknnkzxhdxyxkrcx7jgjy")))

(define-public crate-fuel-core-client-bin-0.18.1 (c (n "fuel-core-client-bin") (v "0.18.1") (d (list (d (n "clap") (r "^4.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fuel-core-client") (r "^0.18.1") (d #t) (k 0)) (d (n "fuel-core-types") (r "^0.18.1") (f (quote ("serde"))) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("raw_value"))) (d #t) (k 0)) (d (n "tokio") (r "^1.27") (f (quote ("macros"))) (d #t) (k 0)))) (h "1s1nbbw1bl4542az0f6q890nbqrcpqx2fwwrq7vd0w8xki06b9q2")))

(define-public crate-fuel-core-client-bin-0.17.12 (c (n "fuel-core-client-bin") (v "0.17.12") (d (list (d (n "clap") (r "^4.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fuel-core-client") (r "^0.17.12") (d #t) (k 0)) (d (n "fuel-core-types") (r "^0.17.12") (f (quote ("serde"))) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("raw_value"))) (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("macros"))) (d #t) (k 0)))) (h "1ajbfhjhdjwyg66hqrdgvmjs6a8d7hx3bxjlr20p3m93rca2qw9a")))

(define-public crate-fuel-core-client-bin-0.18.2 (c (n "fuel-core-client-bin") (v "0.18.2") (d (list (d (n "clap") (r "^4.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fuel-core-client") (r "^0.18.2") (d #t) (k 0)) (d (n "fuel-core-types") (r "^0.18.2") (f (quote ("serde"))) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("raw_value"))) (d #t) (k 0)) (d (n "tokio") (r "^1.27") (f (quote ("macros"))) (d #t) (k 0)))) (h "1ixb5msrqqabdgjihqlnwylfgr78aqfmiqmnc3r666y660579in9")))

(define-public crate-fuel-core-client-bin-0.18.3 (c (n "fuel-core-client-bin") (v "0.18.3") (d (list (d (n "clap") (r "^4.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fuel-core-client") (r "^0.18.3") (d #t) (k 0)) (d (n "fuel-core-types") (r "^0.18.3") (f (quote ("serde"))) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("raw_value"))) (d #t) (k 0)) (d (n "tokio") (r "^1.27") (f (quote ("macros"))) (d #t) (k 0)))) (h "1bygkdjyws8fndvi027fzcyqnky5rkzcwx5rm5b9jq91clf8nydf")))

(define-public crate-fuel-core-client-bin-0.19.0 (c (n "fuel-core-client-bin") (v "0.19.0") (d (list (d (n "clap") (r "^4.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fuel-core-client") (r "^0.19.0") (d #t) (k 0)) (d (n "fuel-core-types") (r "^0.19.0") (f (quote ("serde"))) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("raw_value"))) (d #t) (k 0)) (d (n "tokio") (r "^1.27") (f (quote ("macros"))) (k 0)))) (h "1bnq128racwmxmw3cr8n6r09ryk747yhiaz0362xg2x7k9cdp2l1")))

(define-public crate-fuel-core-client-bin-0.19.1 (c (n "fuel-core-client-bin") (v "0.19.1") (d (list (d (n "clap") (r "^4.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fuel-core-client") (r "^0.19.1") (d #t) (k 0)) (d (n "fuel-core-types") (r "^0.19.1") (f (quote ("serde"))) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("raw_value"))) (d #t) (k 0)) (d (n "tokio") (r "^1.27") (f (quote ("macros"))) (k 0)))) (h "1sjww7w29syxfvhh653i6w3pj6p5cd25wn22f1bzvn3lzpa3djy3")))

(define-public crate-fuel-core-client-bin-0.17.13 (c (n "fuel-core-client-bin") (v "0.17.13") (d (list (d (n "clap") (r "^4.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fuel-core-client") (r "^0.17.13") (d #t) (k 0)) (d (n "fuel-core-types") (r "^0.17.13") (f (quote ("serde"))) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("raw_value"))) (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("macros"))) (d #t) (k 0)))) (h "1kbi3kj65anrii4qy40g0rpfh92623m7b45fdq4kg2b6ic0mw9vc")))

(define-public crate-fuel-core-client-bin-0.20.0 (c (n "fuel-core-client-bin") (v "0.20.0") (d (list (d (n "clap") (r "^4.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fuel-core-client") (r "^0.20.0") (d #t) (k 0)) (d (n "fuel-core-types") (r "^0.20.0") (f (quote ("serde"))) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("raw_value"))) (d #t) (k 0)) (d (n "tokio") (r "^1.27") (f (quote ("macros"))) (k 0)))) (h "1g7qwh2p0bcbdvqhbal3x017kszncnwn6pfxfz8gaxb9xlxv8w7k")))

(define-public crate-fuel-core-client-bin-0.20.1 (c (n "fuel-core-client-bin") (v "0.20.1") (d (list (d (n "clap") (r "^4.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fuel-core-client") (r "^0.20.1") (d #t) (k 0)) (d (n "fuel-core-types") (r "^0.20.1") (f (quote ("serde"))) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("raw_value"))) (d #t) (k 0)) (d (n "tokio") (r "^1.27") (f (quote ("macros"))) (k 0)))) (h "1366c4mn24n9210yr0s3cag2ihsn4w82shgpchs7v4g5h3k7xlwv")))

(define-public crate-fuel-core-client-bin-0.20.2 (c (n "fuel-core-client-bin") (v "0.20.2") (d (list (d (n "clap") (r "^4.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fuel-core-client") (r "^0.20.2") (d #t) (k 0)) (d (n "fuel-core-types") (r "^0.20.2") (f (quote ("serde"))) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("raw_value"))) (d #t) (k 0)) (d (n "tokio") (r "^1.27") (f (quote ("macros"))) (k 0)))) (h "0h3z2ga9g1xgs8lgrv5n6m813kmwn2c6fqmixziqi8am3crlwal8")))

(define-public crate-fuel-core-client-bin-0.20.3 (c (n "fuel-core-client-bin") (v "0.20.3") (d (list (d (n "clap") (r "^4.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fuel-core-client") (r "^0.20.3") (d #t) (k 0)) (d (n "fuel-core-types") (r "^0.20.3") (f (quote ("serde"))) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("raw_value"))) (d #t) (k 0)) (d (n "tokio") (r "^1.27") (f (quote ("macros"))) (k 0)))) (h "0wwq6s8i1168j4byj9bkd9yza05y8kghh18c0xgaff4lm8w0zaph")))

(define-public crate-fuel-core-client-bin-0.20.4 (c (n "fuel-core-client-bin") (v "0.20.4") (d (list (d (n "clap") (r "^4.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fuel-core-client") (r "^0.20.4") (d #t) (k 0)) (d (n "fuel-core-types") (r "^0.20.4") (f (quote ("serde"))) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("raw_value"))) (d #t) (k 0)) (d (n "tokio") (r "^1.27") (f (quote ("macros"))) (k 0)))) (h "15jy2ggg61wr5mdgs56wvx0qy0ic45i7qxg7qbr4ql1mfl8pxvxr")))

(define-public crate-fuel-core-client-bin-0.19.2 (c (n "fuel-core-client-bin") (v "0.19.2") (d (list (d (n "clap") (r "^4.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fuel-core-client") (r "^0.19.2") (d #t) (k 0)) (d (n "fuel-core-types") (r "^0.19.2") (f (quote ("serde"))) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("raw_value"))) (d #t) (k 0)) (d (n "tokio") (r "^1.27") (f (quote ("macros"))) (k 0)))) (h "1wnzg1qj186447qr3mygzcvz2mil7ncw7bl3d5fgvnzzj829a1ah")))

(define-public crate-fuel-core-client-bin-0.20.5 (c (n "fuel-core-client-bin") (v "0.20.5") (d (list (d (n "clap") (r "^4.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fuel-core-client") (r "^0.20.5") (d #t) (k 0)) (d (n "fuel-core-types") (r "^0.20.5") (f (quote ("serde"))) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("raw_value"))) (d #t) (k 0)) (d (n "tokio") (r "^1.27") (f (quote ("macros"))) (k 0)))) (h "1xykf40ljslczqkr9r1g7cwi87j46sc7ymb8rlla921m933kfv87")))

(define-public crate-fuel-core-client-bin-0.20.6 (c (n "fuel-core-client-bin") (v "0.20.6") (d (list (d (n "clap") (r "^4.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fuel-core-client") (r "^0.20.6") (d #t) (k 0)) (d (n "fuel-core-types") (r "^0.20.6") (f (quote ("serde"))) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("raw_value"))) (d #t) (k 0)) (d (n "tokio") (r "^1.27") (f (quote ("macros"))) (k 0)))) (h "1fpg3dig0xf72fyvlxpc36whki64p4qk5c9c9dix4b8j3iyd0ycc")))

(define-public crate-fuel-core-client-bin-0.20.7 (c (n "fuel-core-client-bin") (v "0.20.7") (d (list (d (n "clap") (r "^4.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fuel-core-client") (r "^0.20.7") (d #t) (k 0)) (d (n "fuel-core-types") (r "^0.20.7") (f (quote ("serde"))) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("raw_value"))) (d #t) (k 0)) (d (n "tokio") (r "^1.27") (f (quote ("macros"))) (k 0)))) (h "0a4zasvdjlvyqp324dhpc1h8nj7k7dp4ixzavfqm77rig5lq4wa4")))

(define-public crate-fuel-core-client-bin-0.20.8 (c (n "fuel-core-client-bin") (v "0.20.8") (d (list (d (n "clap") (r "^4.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fuel-core-client") (r "^0.20.8") (d #t) (k 0)) (d (n "fuel-core-types") (r "^0.20.8") (f (quote ("serde"))) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("raw_value"))) (d #t) (k 0)) (d (n "tokio") (r "^1.27") (f (quote ("macros"))) (k 0)))) (h "09svb8c2862a6hv3k66a21djw6dxipnj1gzy26xb9ld1qfvby7aw")))

(define-public crate-fuel-core-client-bin-0.21.0-rc.1 (c (n "fuel-core-client-bin") (v "0.21.0-rc.1") (d (list (d (n "clap") (r "^4.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fuel-core-client") (r "^0.21.0-rc.1") (d #t) (k 0)) (d (n "fuel-core-types") (r "^0.21.0-rc.1") (f (quote ("serde"))) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("raw_value"))) (d #t) (k 0)) (d (n "tokio") (r "^1.27") (f (quote ("macros"))) (k 0)))) (h "0qlaa9m75bpr75myr7zs51772pznbvnyiqjdqivlrygh323qi7d6")))

(define-public crate-fuel-core-client-bin-0.21.0 (c (n "fuel-core-client-bin") (v "0.21.0") (d (list (d (n "clap") (r "^4.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fuel-core-client") (r "^0.21.0") (d #t) (k 0)) (d (n "fuel-core-types") (r "^0.21.0") (f (quote ("serde"))) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("raw_value"))) (d #t) (k 0)) (d (n "tokio") (r "^1.27") (f (quote ("macros"))) (k 0)))) (h "0cfpx0ad2s6pjnwv22wcpb4ykr327rwi08mx72q34j5dhqrfdib7")))

(define-public crate-fuel-core-client-bin-0.22.0 (c (n "fuel-core-client-bin") (v "0.22.0") (d (list (d (n "clap") (r "^4.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fuel-core-client") (r "^0.22.0") (d #t) (k 0)) (d (n "fuel-core-types") (r "^0.22.0") (f (quote ("serde"))) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("raw_value"))) (d #t) (k 0)) (d (n "tokio") (r "^1.27") (f (quote ("macros"))) (k 0)))) (h "1lrlrgcnm9br5w2v9dyqfz31rbc6qq2i59wc589z26xvag6bza2g")))

(define-public crate-fuel-core-client-bin-0.22.1 (c (n "fuel-core-client-bin") (v "0.22.1") (d (list (d (n "clap") (r "^4.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fuel-core-client") (r "^0.22.1") (d #t) (k 0)) (d (n "fuel-core-types") (r "^0.22.1") (f (quote ("serde"))) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("raw_value"))) (d #t) (k 0)) (d (n "tokio") (r "^1.27") (f (quote ("macros"))) (k 0)))) (h "08bm67m1dr5mr9f43pd3l6fzd3j2vmg4jh08mm4wb60dgpdbh7j0")))

(define-public crate-fuel-core-client-bin-0.23.0 (c (n "fuel-core-client-bin") (v "0.23.0") (d (list (d (n "clap") (r "^4.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fuel-core-client") (r "^0.23.0") (d #t) (k 0)) (d (n "fuel-core-types") (r "^0.23.0") (f (quote ("serde"))) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("raw_value"))) (d #t) (k 0)) (d (n "tokio") (r "^1.27") (f (quote ("macros"))) (k 0)))) (h "0hkl8cpw5ahqxy34508n4bxcqn7lkfqapl27k1m85a4r1mjchs9q")))

(define-public crate-fuel-core-client-bin-0.22.2 (c (n "fuel-core-client-bin") (v "0.22.2") (d (list (d (n "clap") (r "^4.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fuel-core-client") (r "^0.22.2") (d #t) (k 0)) (d (n "fuel-core-types") (r "^0.22.2") (f (quote ("serde"))) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("raw_value"))) (d #t) (k 0)) (d (n "tokio") (r "^1.27") (f (quote ("macros"))) (k 0)))) (h "0qblm4z5a09w5msdpd8ig6zksph8j9cchn8da2acsfmihbw2adx4")))

(define-public crate-fuel-core-client-bin-0.22.3 (c (n "fuel-core-client-bin") (v "0.22.3") (d (list (d (n "clap") (r "^4.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fuel-core-client") (r "^0.22.3") (d #t) (k 0)) (d (n "fuel-core-types") (r "^0.22.3") (f (quote ("serde"))) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("raw_value"))) (d #t) (k 0)) (d (n "tokio") (r "^1.27") (f (quote ("macros"))) (k 0)))) (h "045visz43av4qx75mmrmwpavmsb5ar0vixrk0ricq30wpgzshry2")))

(define-public crate-fuel-core-client-bin-0.22.4 (c (n "fuel-core-client-bin") (v "0.22.4") (d (list (d (n "clap") (r "^4.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fuel-core-client") (r "^0.22.4") (d #t) (k 0)) (d (n "fuel-core-types") (r "^0.22.4") (f (quote ("serde"))) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("raw_value"))) (d #t) (k 0)) (d (n "tokio") (r "^1.27") (f (quote ("macros"))) (k 0)))) (h "1pcfivfqgbaqqhwj7xss57afwcj4fcwji67kd4hbp7phpx76z3k0")))

(define-public crate-fuel-core-client-bin-0.24.0 (c (n "fuel-core-client-bin") (v "0.24.0") (d (list (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fuel-core-client") (r "^0.24.0") (d #t) (k 0)) (d (n "fuel-core-types") (r "^0.24.0") (f (quote ("serde"))) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("raw_value"))) (d #t) (k 0)) (d (n "tokio") (r "^1.27") (f (quote ("macros"))) (k 0)))) (h "15nhp8qwspnv41cczqb4c8pb3wcjcdiaw3lgm739zv1xq9ldv7nc")))

(define-public crate-fuel-core-client-bin-0.24.1 (c (n "fuel-core-client-bin") (v "0.24.1") (d (list (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fuel-core-client") (r "^0.24.1") (d #t) (k 0)) (d (n "fuel-core-types") (r "^0.24.1") (f (quote ("serde"))) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("raw_value"))) (d #t) (k 0)) (d (n "tokio") (r "^1.27") (f (quote ("macros"))) (k 0)))) (h "0p5jwl9w0n5j0c6j0zwz3y53mfwlgvvjmx4xm006q8sgfj5p30vg")))

(define-public crate-fuel-core-client-bin-0.24.2 (c (n "fuel-core-client-bin") (v "0.24.2") (d (list (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fuel-core-client") (r "^0.24.2") (d #t) (k 0)) (d (n "fuel-core-types") (r "^0.24.2") (f (quote ("serde"))) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("raw_value"))) (d #t) (k 0)) (d (n "tokio") (r "^1.27") (f (quote ("macros"))) (k 0)))) (h "01c6x8bqmms028fqgrhmn6zyjawmgcn7xlsaw9lg1834rdjmqhns")))

(define-public crate-fuel-core-client-bin-0.25.0 (c (n "fuel-core-client-bin") (v "0.25.0") (d (list (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fuel-core-client") (r "^0.25.0") (d #t) (k 0)) (d (n "fuel-core-types") (r "^0.25.0") (f (quote ("serde"))) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("raw_value"))) (d #t) (k 0)) (d (n "tokio") (r "^1.27") (f (quote ("macros"))) (k 0)))) (h "0h860bp58jw92c7ffrm51kid25w2rnj4yx07g4gsmxrq6ghwh196")))

(define-public crate-fuel-core-client-bin-0.25.1 (c (n "fuel-core-client-bin") (v "0.25.1") (d (list (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fuel-core-client") (r "^0.25.1") (d #t) (k 0)) (d (n "fuel-core-types") (r "^0.25.1") (f (quote ("serde"))) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("raw_value"))) (d #t) (k 0)) (d (n "tokio") (r "^1.27") (f (quote ("macros"))) (k 0)))) (h "0qgw9942barwj0j7sqc7cg11ighfdpa6jdn86cqh3fv8vnwbjnjk")))

(define-public crate-fuel-core-client-bin-0.25.2 (c (n "fuel-core-client-bin") (v "0.25.2") (d (list (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fuel-core-client") (r "^0.25.2") (d #t) (k 0)) (d (n "fuel-core-types") (r "^0.25.2") (f (quote ("serde"))) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("raw_value"))) (d #t) (k 0)) (d (n "tokio") (r "^1.27") (f (quote ("macros"))) (k 0)))) (h "0k480fza3j6aj2as4fkrq2p1kwny8ikn07dsbqj0kqcvig6z2yb3")))

(define-public crate-fuel-core-client-bin-0.24.3 (c (n "fuel-core-client-bin") (v "0.24.3") (d (list (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fuel-core-client") (r "^0.24.3") (d #t) (k 0)) (d (n "fuel-core-types") (r "^0.24.3") (f (quote ("serde"))) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("raw_value"))) (d #t) (k 0)) (d (n "tokio") (r "^1.27") (f (quote ("macros"))) (k 0)))) (h "1bbvp136pk3j7mdm36cq2l4vh2s9sq6njp5s1n3lcgh7ln5vq1rq")))

(define-public crate-fuel-core-client-bin-0.25.3 (c (n "fuel-core-client-bin") (v "0.25.3") (d (list (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fuel-core-client") (r "^0.25.3") (d #t) (k 0)) (d (n "fuel-core-types") (r "^0.25.3") (f (quote ("serde"))) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("raw_value"))) (d #t) (k 0)) (d (n "tokio") (r "^1.27") (f (quote ("macros"))) (k 0)))) (h "0xg0x4a4ph5jb7m8b228ps4whxrhyj0s8j0qfjax7giinlx7lb9a")))

(define-public crate-fuel-core-client-bin-0.26.0 (c (n "fuel-core-client-bin") (v "0.26.0") (d (list (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fuel-core-client") (r "^0.26.0") (d #t) (k 0)) (d (n "fuel-core-types") (r "^0.26.0") (f (quote ("serde"))) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("raw_value"))) (d #t) (k 0)) (d (n "tokio") (r "^1.27") (f (quote ("macros"))) (k 0)))) (h "0fnl46al71798cva2xlm5402117kb8y0hfl294q8rk0j59nih329")))

(define-public crate-fuel-core-client-bin-0.27.0 (c (n "fuel-core-client-bin") (v "0.27.0") (d (list (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fuel-core-client") (r "^0.27.0") (d #t) (k 0)) (d (n "fuel-core-types") (r "^0.27.0") (f (quote ("serde"))) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("raw_value"))) (d #t) (k 0)) (d (n "tokio") (r "^1.27") (f (quote ("macros"))) (k 0)))) (h "1qs6pmp6z34hazx68qp5kb26mvlzd89c0iq604v5dw74xnmyrhax")))

