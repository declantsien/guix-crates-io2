(define-module (crates-io fu el fuel-core-bft) #:use-module (crates-io))

(define-public crate-fuel-core-bft-0.9.1 (c (n "fuel-core-bft") (v "0.9.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "fuel-core-interfaces") (r "^0.9.1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "tokio") (r "^1.14") (f (quote ("full"))) (d #t) (k 0)))) (h "0rbbyxkbk9hzflxkj4zf6109g7yzwspdin8pbdy3m9n17221rc1b")))

(define-public crate-fuel-core-bft-0.9.2 (c (n "fuel-core-bft") (v "0.9.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "fuel-core-interfaces") (r "^0.9.2") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "tokio") (r "^1.14") (f (quote ("full"))) (d #t) (k 0)))) (h "06ppp0msssqbipc8l0ll1ph4s3wk88ghyg81jlbz99l0kwcrnyh3")))

(define-public crate-fuel-core-bft-0.9.3 (c (n "fuel-core-bft") (v "0.9.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "fuel-core-interfaces") (r "^0.9.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "tokio") (r "^1.14") (f (quote ("full"))) (d #t) (k 0)))) (h "07m3c50fs0fw3s0arvdh81jslds18xd3jh4xzri2ddxpwwlprrqz")))

(define-public crate-fuel-core-bft-0.9.4 (c (n "fuel-core-bft") (v "0.9.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "fuel-core-interfaces") (r "^0.9.4") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "tokio") (r "^1.14") (f (quote ("full"))) (d #t) (k 0)))) (h "0czli1b3bmkfh46rs4libmjaisjc2fdrb32xqdrbd6zf40516wqg")))

(define-public crate-fuel-core-bft-0.9.5 (c (n "fuel-core-bft") (v "0.9.5") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "fuel-core-interfaces") (r "^0.9.5") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "tokio") (r "^1.14") (f (quote ("full"))) (d #t) (k 0)))) (h "06d8y0hyhxr89ki1zlls27wsmnxxi9pam2frqj1iklzvhfphkxaf")))

(define-public crate-fuel-core-bft-0.9.6 (c (n "fuel-core-bft") (v "0.9.6") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "fuel-core-interfaces") (r "^0.9.6") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "tokio") (r "^1.14") (f (quote ("full"))) (d #t) (k 0)))) (h "01kcsd0xxn3lc42pxmmb7zdf8nx3z71qjflqlgral6bvhqldkdw2")))

(define-public crate-fuel-core-bft-0.9.8 (c (n "fuel-core-bft") (v "0.9.8") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "fuel-core-interfaces") (r "^0.9.8") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "tokio") (r "^1.14") (f (quote ("full"))) (d #t) (k 0)))) (h "11rh2lcqhbx2kkr7sh32ff28f4vqb0yxq5dxr1qflrgzzpfcps28")))

(define-public crate-fuel-core-bft-0.10.0 (c (n "fuel-core-bft") (v "0.10.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "fuel-core-interfaces") (r "^0.10.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "tokio") (r "^1.14") (f (quote ("full"))) (d #t) (k 0)))) (h "0msxjgaksz7wq4gnkbvws2b6j5wawi4sihznhg1r47arppxd1hc0")))

(define-public crate-fuel-core-bft-0.10.1 (c (n "fuel-core-bft") (v "0.10.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "fuel-core-interfaces") (r "^0.10.1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "tokio") (r "^1.14") (f (quote ("full"))) (d #t) (k 0)))) (h "1dh1lqhd0g3cr42pk5cpmvl4yzjsy37lq289iv5vpiwg7k7khxdz")))

(define-public crate-fuel-core-bft-0.11.2 (c (n "fuel-core-bft") (v "0.11.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "fuel-core-interfaces") (r "^0.11.2") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "tokio") (r "^1.21") (f (quote ("full"))) (d #t) (k 0)))) (h "1jrlwvdiwyby1mmp5qb03y4pf026vvr1yz6pjfihk7s1nn497wrf")))

(define-public crate-fuel-core-bft-0.12.0 (c (n "fuel-core-bft") (v "0.12.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "fuel-core-interfaces") (r "^0.12.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "tokio") (r "^1.21") (f (quote ("full"))) (d #t) (k 0)))) (h "0xvf4hnh10wmclswmcxz8dc464hz4vvmzhr5j3lvfxvvvnzfzskm")))

(define-public crate-fuel-core-bft-0.13.0 (c (n "fuel-core-bft") (v "0.13.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "fuel-core-interfaces") (r "^0.13.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "tokio") (r "^1.21") (f (quote ("full"))) (d #t) (k 0)))) (h "1ia3509xp3kqlicnqm0nw3s3qxbz5rdzjiacq9dqf8p1ncwl2hyn")))

(define-public crate-fuel-core-bft-0.13.1 (c (n "fuel-core-bft") (v "0.13.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "fuel-core-interfaces") (r "^0.13.1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "tokio") (r "^1.21") (f (quote ("full"))) (d #t) (k 0)))) (h "11hb2kf5m3k2r1h75y3czk1ybv8s8xxs0qq5d8f1aadvacjkld5c")))

(define-public crate-fuel-core-bft-0.13.2 (c (n "fuel-core-bft") (v "0.13.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "fuel-core-interfaces") (r "^0.13.2") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "tokio") (r "^1.21") (f (quote ("full"))) (d #t) (k 0)))) (h "12l58mky5m9gaj122nzpfzdzp269agsgkg0kap9vk7h71ripb5ns")))

(define-public crate-fuel-core-bft-0.14.0 (c (n "fuel-core-bft") (v "0.14.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "fuel-core-interfaces") (r "^0.14.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "tokio") (r "^1.21") (f (quote ("full"))) (d #t) (k 0)))) (h "0q6hdb4z2y3wpv6qhkbasnnvpi7fkf9ifna4kd1w0sq4vqm4v3f9")))

(define-public crate-fuel-core-bft-0.14.1 (c (n "fuel-core-bft") (v "0.14.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "fuel-core-interfaces") (r "^0.14.1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "tokio") (r "^1.21") (f (quote ("full"))) (d #t) (k 0)))) (h "0zxx5ip16grhr4q3xk0wc39s6if9lv2iy5b1lzd1dsbzk034mrsx")))

(define-public crate-fuel-core-bft-0.15.0 (c (n "fuel-core-bft") (v "0.15.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "fuel-core-interfaces") (r "^0.15.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "tokio") (r "^1.21") (f (quote ("full"))) (d #t) (k 0)))) (h "0v9pk2q7lw7v3nw9zxqpkll4ilxqqwq1a90gzcnjy3fj1bxscvds")))

(define-public crate-fuel-core-bft-0.15.1 (c (n "fuel-core-bft") (v "0.15.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "fuel-core-interfaces") (r "^0.15.1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "tokio") (r "^1.21") (f (quote ("full"))) (d #t) (k 0)))) (h "0l3c9dlvbh4ys44hffwmii0390i3jw4jqccvjy4br05l983b8m8y")))

(define-public crate-fuel-core-bft-0.14.2 (c (n "fuel-core-bft") (v "0.14.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "fuel-core-interfaces") (r "^0.14.2") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "tokio") (r "^1.21") (f (quote ("full"))) (d #t) (k 0)))) (h "0xwxcnx9jn97bcfxldqbmxnip2w2h1v76dv4w5440xx7sk64ng8k")))

(define-public crate-fuel-core-bft-0.15.3 (c (n "fuel-core-bft") (v "0.15.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "fuel-core-interfaces") (r "^0.15.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "tokio") (r "^1.21") (f (quote ("full"))) (d #t) (k 0)))) (h "1b90p6ipaaczss256n9dych84c16ac4q7dx0n2wig6f5zmw4zirz")))

(define-public crate-fuel-core-bft-0.16.1 (c (n "fuel-core-bft") (v "0.16.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "tokio") (r "^1.21") (f (quote ("full"))) (d #t) (k 0)))) (h "1fdphxx42n9mvlmwj78idw3c7zs5fhwgs6xsy40bxz06jpk664dc")))

(define-public crate-fuel-core-bft-0.17.0 (c (n "fuel-core-bft") (v "0.17.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0rixyqvlzas5ixhw01gaakc0pypnw836c90g52z74cw8nqyfb899")))

(define-public crate-fuel-core-bft-0.17.1 (c (n "fuel-core-bft") (v "0.17.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "11pfj0b91qd5r7p8h5wfh0xp3hv7i91ypbji7q7x2rn4zsqc7bpq")))

(define-public crate-fuel-core-bft-0.17.2 (c (n "fuel-core-bft") (v "0.17.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0bc0f4c677rnhggdwp9impmmp985nabpynn7fhvw4j3ci3abnij6")))

(define-public crate-fuel-core-bft-0.17.3 (c (n "fuel-core-bft") (v "0.17.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "07fb8r3ckh5dzcnxv22m9b9pndsaliwkb4ai79sk87fb8x8my1b1")))

(define-public crate-fuel-core-bft-0.17.4 (c (n "fuel-core-bft") (v "0.17.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1n2a78dkbxdgaxj631qr7pd2n1gn7p402ki7pa3kyk5ixml8rpil")))

(define-public crate-fuel-core-bft-0.17.5 (c (n "fuel-core-bft") (v "0.17.5") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0vwyr1c2cq4wdkvfhqv6g4vg57cz844zqwqdn2nz1qjxmfffp0ps")))

(define-public crate-fuel-core-bft-0.17.6 (c (n "fuel-core-bft") (v "0.17.6") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0k2w2zilmqmfp0m0ggnwr34ahjm0lqsp5drdfkvq6icp21pkjcwv")))

(define-public crate-fuel-core-bft-0.17.7 (c (n "fuel-core-bft") (v "0.17.7") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1zd0cgj2xfpzckrxb00v2ibax9lisxj3k89ka35nvq1vgh34946x")))

(define-public crate-fuel-core-bft-0.17.8 (c (n "fuel-core-bft") (v "0.17.8") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1b6r1s5qkc7gbb3jlihvk4bpz8lckypjjyqzaldrq201gb6nd59v")))

(define-public crate-fuel-core-bft-0.17.9 (c (n "fuel-core-bft") (v "0.17.9") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1qw1aj64jci134kpwazagx7x4dn8dxn19h5lz7a96rslbxlnyrbi")))

(define-public crate-fuel-core-bft-0.17.10 (c (n "fuel-core-bft") (v "0.17.10") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "11hyc5wljlbw391mzrlvimhxyhgg4imlsg14gqhbn7d2jbs6zzmj")))

(define-public crate-fuel-core-bft-0.17.11 (c (n "fuel-core-bft") (v "0.17.11") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "07509qgqrj1mdmcmv7zbi8kcd3bxsp1xq2ly35dsxznkdd92i6w2")))

(define-public crate-fuel-core-bft-0.18.0 (c (n "fuel-core-bft") (v "0.18.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "tokio") (r "^1.27") (f (quote ("full"))) (d #t) (k 0)))) (h "0y922k09499v8pxg1ii367vnmh7v6lspvrz134rpw7sbq89m4axd")))

(define-public crate-fuel-core-bft-0.18.1 (c (n "fuel-core-bft") (v "0.18.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "tokio") (r "^1.27") (f (quote ("full"))) (d #t) (k 0)))) (h "1rdsl2dslh5lhgc1yi3cmr5v34ms7vi1jv1mjaymdyyy6s56pikn")))

(define-public crate-fuel-core-bft-0.17.12 (c (n "fuel-core-bft") (v "0.17.12") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "19vmcqb1gfr3h7lpjhyplhv8wzww69mbr15mrrqrmkf4xi29d7w5")))

(define-public crate-fuel-core-bft-0.18.2 (c (n "fuel-core-bft") (v "0.18.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "tokio") (r "^1.27") (f (quote ("full"))) (d #t) (k 0)))) (h "1yii81llz5qhi1rxy67yfqf11vzg2q6lw7pg4mqwhv62lsbifjxz")))

(define-public crate-fuel-core-bft-0.18.3 (c (n "fuel-core-bft") (v "0.18.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "tokio") (r "^1.27") (f (quote ("full"))) (d #t) (k 0)))) (h "16c355rfk59rw2lap2k40b0qahasgwv2ad81iq2s3ficxp6h086w")))

(define-public crate-fuel-core-bft-0.19.0 (c (n "fuel-core-bft") (v "0.19.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "tokio") (r "^1.27") (f (quote ("full"))) (k 0)))) (h "07ps94wd94yngx0yvi3xgicgz5nsg1p5qn7lpxgvwgmcf17cafhh")))

(define-public crate-fuel-core-bft-0.19.1 (c (n "fuel-core-bft") (v "0.19.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "tokio") (r "^1.27") (f (quote ("full"))) (k 0)))) (h "18lv6i8l94xi5mhpsbyigi0x8wmjcz2s3gar1sz5ii7g5s4nz985")))

(define-public crate-fuel-core-bft-0.17.13 (c (n "fuel-core-bft") (v "0.17.13") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0yqqm34bijxnw00m9d31ck4xgfyd01yl9h0jb3cw9g6z9xr3fj7p")))

(define-public crate-fuel-core-bft-0.20.0 (c (n "fuel-core-bft") (v "0.20.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "tokio") (r "^1.27") (f (quote ("full"))) (k 0)))) (h "16qdzyfjyl01n4mh8ygz4yvkhz8y84vb689vfc1n58gqbkgj3yv5")))

(define-public crate-fuel-core-bft-0.20.1 (c (n "fuel-core-bft") (v "0.20.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "tokio") (r "^1.27") (f (quote ("full"))) (k 0)))) (h "0dlwxlm63drvm9ndd89ngf8avvwqyyci1mf7cdh85jzyp3iyg6j6")))

(define-public crate-fuel-core-bft-0.20.2 (c (n "fuel-core-bft") (v "0.20.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "tokio") (r "^1.27") (f (quote ("full"))) (k 0)))) (h "0y42yajnr2f7xyy40lraph8s2ln8qrbccqbz7rdr1w4ivhdhksw9")))

(define-public crate-fuel-core-bft-0.20.3 (c (n "fuel-core-bft") (v "0.20.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "tokio") (r "^1.27") (f (quote ("full"))) (k 0)))) (h "1ackzakaci08i93b6syk6mg9fa65yjjjzhsbq5y0m29hbrnq3lwl")))

(define-public crate-fuel-core-bft-0.20.4 (c (n "fuel-core-bft") (v "0.20.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "tokio") (r "^1.27") (f (quote ("full"))) (k 0)))) (h "0zaybdxd313nria33qwdbabbnl32r7pxwk9msv1l1l7f8xyjsvxb")))

(define-public crate-fuel-core-bft-0.19.2 (c (n "fuel-core-bft") (v "0.19.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "tokio") (r "^1.27") (f (quote ("full"))) (k 0)))) (h "0g60fnnxdqs1gzzfghs2a8dpl5v56kf04q6rmhvw56yh3i0mij6s")))

(define-public crate-fuel-core-bft-0.20.5 (c (n "fuel-core-bft") (v "0.20.5") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "tokio") (r "^1.27") (f (quote ("full"))) (k 0)))) (h "0gszhy61j66x2r8li6fncj5qj71xkbjsdxhyz838q7akl861idm3")))

(define-public crate-fuel-core-bft-0.20.6 (c (n "fuel-core-bft") (v "0.20.6") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "tokio") (r "^1.27") (f (quote ("full"))) (k 0)))) (h "0hy9pdfqazmyn3hvikw2h4paqvn9dlywjwyxmny9kv1i2cr3y77x")))

(define-public crate-fuel-core-bft-0.20.7 (c (n "fuel-core-bft") (v "0.20.7") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "tokio") (r "^1.27") (f (quote ("full"))) (k 0)))) (h "0yzrf7qkaj8cvjvcifm3k4xbw4b71q29lhwnm3v6kqrvwjmhx2w8")))

(define-public crate-fuel-core-bft-0.20.8 (c (n "fuel-core-bft") (v "0.20.8") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "tokio") (r "^1.27") (f (quote ("full"))) (k 0)))) (h "1pglc6w31cz9gm4ph7fgr8q2j9ghikrkixxvzlavs4c8npf9pfyv")))

(define-public crate-fuel-core-bft-0.21.0-rc.1 (c (n "fuel-core-bft") (v "0.21.0-rc.1") (h "024291myrzjk9bxanfhdrdksk9ga49cmfnlgnqzqjz2kdw2nlx8n")))

(define-public crate-fuel-core-bft-0.21.0 (c (n "fuel-core-bft") (v "0.21.0") (h "0b3sws4xm8fmhpw5w93nyhrgji8waqwkz8b1hh0nrr7hwdbvr5cq")))

(define-public crate-fuel-core-bft-0.22.0 (c (n "fuel-core-bft") (v "0.22.0") (h "1i5j6jxi4bihr25arzkpwhs82j1xqsh73zrmpyhndph8yagwjy4a")))

(define-public crate-fuel-core-bft-0.22.1 (c (n "fuel-core-bft") (v "0.22.1") (h "0pwal6cq8j15m575ymj599wrvclspgjcka7ass0cd59qxvmyg66d")))

(define-public crate-fuel-core-bft-0.23.0 (c (n "fuel-core-bft") (v "0.23.0") (h "04znigr7x3xc1j3cm34qwxcnsm7yywrg1zdhzzcac3idgvhygqhg")))

(define-public crate-fuel-core-bft-0.22.2 (c (n "fuel-core-bft") (v "0.22.2") (h "0v67zhda3ifcgahk2wplljkaj92azwm11y1azfm7avvw865wch30")))

(define-public crate-fuel-core-bft-0.22.3 (c (n "fuel-core-bft") (v "0.22.3") (h "0scmkl8pvp5nidsml4pv1cji534i6i9xy2y3y69hx59ghsgsy484")))

(define-public crate-fuel-core-bft-0.22.4 (c (n "fuel-core-bft") (v "0.22.4") (h "0sf3fkzcl3jmv4j0id893iiamp87648a4l6rcwfjn0gjdhmgs1gr")))

(define-public crate-fuel-core-bft-0.24.0 (c (n "fuel-core-bft") (v "0.24.0") (h "036w3ajgvganv0jl6zvfvnmix0rzpx7k29kpri7dm7dwbcz92pmb")))

(define-public crate-fuel-core-bft-0.24.1 (c (n "fuel-core-bft") (v "0.24.1") (h "1pkjbfxplhhyafpsiwpn0fr422p57fx1azx0cln2705j3h2ciwm6")))

(define-public crate-fuel-core-bft-0.24.2 (c (n "fuel-core-bft") (v "0.24.2") (h "14dh03a8x0lhyw48mr4dqkq4hqlzmjjclpq0m28q74wam5nynna6")))

(define-public crate-fuel-core-bft-0.25.0 (c (n "fuel-core-bft") (v "0.25.0") (h "07y5f337gna07gfkybjnwa1rbww8xvx6qfi9d07cc5qq8cx6b6xp")))

(define-public crate-fuel-core-bft-0.25.1 (c (n "fuel-core-bft") (v "0.25.1") (h "198cnb0rqf98acr3qzbid30rjsj3is03n5isdnby5yk9nqfxffjx")))

(define-public crate-fuel-core-bft-0.25.2 (c (n "fuel-core-bft") (v "0.25.2") (h "0mc2hv62cf6jcpfzdwv6ncfh7x86vpdwljv4jsaa3iiqg0kmhkc8")))

(define-public crate-fuel-core-bft-0.24.3 (c (n "fuel-core-bft") (v "0.24.3") (h "1lq6w89n0vma5zk48rldqigmgzf7qc87irdj0agf5l0z1mwh745x")))

(define-public crate-fuel-core-bft-0.25.3 (c (n "fuel-core-bft") (v "0.25.3") (h "0idm05y7aazmivaqm1w2ai30ia7s8p8sw1857lrdsz0vaana1zh6")))

(define-public crate-fuel-core-bft-0.26.0 (c (n "fuel-core-bft") (v "0.26.0") (h "0h47had24vqcab2ncbkk7aqayknh9hif2c7b6cldqqs5fb4a8h53")))

(define-public crate-fuel-core-bft-0.27.0 (c (n "fuel-core-bft") (v "0.27.0") (h "1ryyh3hakfrn60qbjkphq8822ysya2hr733k90ax2fm7hlav9acj")))

