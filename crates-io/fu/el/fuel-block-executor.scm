(define-module (crates-io fu el fuel-block-executor) #:use-module (crates-io))

(define-public crate-fuel-block-executor-0.9.1 (c (n "fuel-block-executor") (v "0.9.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "fuel-core-interfaces") (r "^0.9.1") (d #t) (k 0)) (d (n "tokio") (r "^1.14") (f (quote ("full"))) (d #t) (k 0)))) (h "0rsx6jki75nb1p9xapc1n4iglgzbip5x3yh5z5qzqqkjqhb9k34d")))

(define-public crate-fuel-block-executor-0.9.2 (c (n "fuel-block-executor") (v "0.9.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "fuel-core-interfaces") (r "^0.9.2") (d #t) (k 0)) (d (n "tokio") (r "^1.14") (f (quote ("full"))) (d #t) (k 0)))) (h "0cgqn8rpifp7p80fg6wh1bngd3gy60bzidh7f7657a6r5kgvvagp")))

(define-public crate-fuel-block-executor-0.9.3 (c (n "fuel-block-executor") (v "0.9.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "fuel-core-interfaces") (r "^0.9.3") (d #t) (k 0)) (d (n "tokio") (r "^1.14") (f (quote ("full"))) (d #t) (k 0)))) (h "093jz9rb84j8hbb58id4q01amrnqsg01p7b77icbx4nn62gl1y5k")))

(define-public crate-fuel-block-executor-0.9.4 (c (n "fuel-block-executor") (v "0.9.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "fuel-core-interfaces") (r "^0.9.4") (d #t) (k 0)) (d (n "tokio") (r "^1.14") (f (quote ("full"))) (d #t) (k 0)))) (h "01w2gzxmvh7bxddbhlysqdlwang9xqn0ls5wxnlx61pzk6pb6ni3")))

(define-public crate-fuel-block-executor-0.9.5 (c (n "fuel-block-executor") (v "0.9.5") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "fuel-core-interfaces") (r "^0.9.5") (d #t) (k 0)) (d (n "tokio") (r "^1.14") (f (quote ("full"))) (d #t) (k 0)))) (h "1rnr4q4dfm32f4j35lwv4w77l796rcbps6fbpif0hyysdnxcqr3n")))

(define-public crate-fuel-block-executor-0.9.6 (c (n "fuel-block-executor") (v "0.9.6") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "fuel-core-interfaces") (r "^0.9.6") (d #t) (k 0)) (d (n "tokio") (r "^1.14") (f (quote ("full"))) (d #t) (k 0)))) (h "10xrdribllickrlj96j16b47bmz7gwfhhyjd8c99h0a5bh8c18rq")))

(define-public crate-fuel-block-executor-0.9.8 (c (n "fuel-block-executor") (v "0.9.8") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "fuel-core-interfaces") (r "^0.9.8") (d #t) (k 0)) (d (n "tokio") (r "^1.14") (f (quote ("full"))) (d #t) (k 0)))) (h "0rxy4gcahilmg62xm8hxfhgyw65ila2865na3s1rizgzc85a650z")))

(define-public crate-fuel-block-executor-0.10.0 (c (n "fuel-block-executor") (v "0.10.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "fuel-core-interfaces") (r "^0.10.0") (d #t) (k 0)) (d (n "tokio") (r "^1.14") (f (quote ("full"))) (d #t) (k 0)))) (h "1ypn9zgarnzrka9rrimyyd0bahdnfyvz79mngs95wyzqwlym61a3")))

(define-public crate-fuel-block-executor-0.10.1 (c (n "fuel-block-executor") (v "0.10.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "fuel-core-interfaces") (r "^0.10.1") (d #t) (k 0)) (d (n "tokio") (r "^1.14") (f (quote ("full"))) (d #t) (k 0)))) (h "0sji4nfq9bp7zdprcfzf4pwz4rfkriz7jjiw60zjcjivvycrgxp2")))

(define-public crate-fuel-block-executor-0.11.2 (c (n "fuel-block-executor") (v "0.11.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "fuel-core-interfaces") (r "^0.11.2") (d #t) (k 0)) (d (n "tokio") (r "^1.21") (f (quote ("full"))) (d #t) (k 0)))) (h "149q98xima9m463i1bz6b7295b1sgy5gvg79flzcdwpf6n0wf79p")))

(define-public crate-fuel-block-executor-0.12.0 (c (n "fuel-block-executor") (v "0.12.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "fuel-core-interfaces") (r "^0.12.0") (d #t) (k 0)) (d (n "tokio") (r "^1.21") (f (quote ("full"))) (d #t) (k 0)))) (h "0bxrzs9bn02kkq5k9kh3dpjb6j0c1dkqs682rm5imaa3pmiarx66")))

(define-public crate-fuel-block-executor-0.13.0 (c (n "fuel-block-executor") (v "0.13.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "fuel-core-interfaces") (r "^0.13.0") (d #t) (k 0)) (d (n "tokio") (r "^1.21") (f (quote ("full"))) (d #t) (k 0)))) (h "07a2i3bjrsrnp823mh5s9sq0hwlfjj5jhq17yn38i1zwypn6if3m")))

(define-public crate-fuel-block-executor-0.13.1 (c (n "fuel-block-executor") (v "0.13.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "fuel-core-interfaces") (r "^0.13.1") (d #t) (k 0)) (d (n "tokio") (r "^1.21") (f (quote ("full"))) (d #t) (k 0)))) (h "1hv94pvzlx37h5ldig55xalp91hf7a1iji59zsf14cjcwnk7jjbd")))

(define-public crate-fuel-block-executor-0.13.2 (c (n "fuel-block-executor") (v "0.13.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "fuel-core-interfaces") (r "^0.13.2") (d #t) (k 0)) (d (n "tokio") (r "^1.21") (f (quote ("full"))) (d #t) (k 0)))) (h "0jdq5rxinvbkywfn07y538bvvs91rjzw7f6amd9h801dny9rlhvh")))

(define-public crate-fuel-block-executor-0.14.0 (c (n "fuel-block-executor") (v "0.14.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "fuel-core-interfaces") (r "^0.14.0") (d #t) (k 0)) (d (n "tokio") (r "^1.21") (f (quote ("full"))) (d #t) (k 0)))) (h "1yr4qhh1nm12hkcydp004my7waw1d66ggiq430hp56w4ghf6xrgk")))

(define-public crate-fuel-block-executor-0.14.1 (c (n "fuel-block-executor") (v "0.14.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "fuel-core-interfaces") (r "^0.14.1") (d #t) (k 0)) (d (n "tokio") (r "^1.21") (f (quote ("full"))) (d #t) (k 0)))) (h "05gfjfx6v9klavbis5195p4bqzsypmgxwj6ry3jhvbidpvf3zhq9")))

(define-public crate-fuel-block-executor-0.15.0 (c (n "fuel-block-executor") (v "0.15.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "fuel-core-interfaces") (r "^0.15.0") (d #t) (k 0)) (d (n "tokio") (r "^1.21") (f (quote ("full"))) (d #t) (k 0)))) (h "14w2l2zfwi67yiga7498f4awv3c6mmi18xws4f079k3wpc8pyihh")))

(define-public crate-fuel-block-executor-0.15.1 (c (n "fuel-block-executor") (v "0.15.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "fuel-chain-config") (r "^0.15.1") (d #t) (k 0)) (d (n "fuel-core-interfaces") (r "^0.15.1") (d #t) (k 0)) (d (n "tokio") (r "^1.21") (f (quote ("full"))) (d #t) (k 0)))) (h "1aqzxcldi9np4h3h3g3sjp5w1pch4nkgmh12s383lsvcb2ws5zd9")))

(define-public crate-fuel-block-executor-0.14.2 (c (n "fuel-block-executor") (v "0.14.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "fuel-core-interfaces") (r "^0.14.2") (d #t) (k 0)) (d (n "tokio") (r "^1.21") (f (quote ("full"))) (d #t) (k 0)))) (h "1rn80d9nrwbkzky6wal4ph9ad96c3xdrdghyhkpczkyzmz60hs7c")))

(define-public crate-fuel-block-executor-0.15.3 (c (n "fuel-block-executor") (v "0.15.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "fuel-chain-config") (r "^0.15.3") (d #t) (k 0)) (d (n "fuel-core-interfaces") (r "^0.15.3") (d #t) (k 0)) (d (n "tokio") (r "^1.21") (f (quote ("full"))) (d #t) (k 0)))) (h "120ga8xhnl1mp7pckgnl0bwb4zrhv12fbs3hw2n24fz02vvnvjnl")))

