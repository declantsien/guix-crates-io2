(define-module (crates-io fu el fuel-etk-cli) #:use-module (crates-io))

(define-public crate-fuel-etk-cli-0.3.0-dev (c (n "fuel-etk-cli") (v "0.3.0-dev") (d (list (d (n "clap") (r "^3.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "snafu") (r "^0.7.1") (f (quote ("std"))) (k 0)) (d (n "hex-literal") (r "^0.3.1") (d #t) (k 2)))) (h "08x6yigx63aybrdm1nla8vrdzbznv8cl5r3mbbmdj41r7xg0cqcl")))

(define-public crate-fuel-etk-cli-0.3.1-dev (c (n "fuel-etk-cli") (v "0.3.1-dev") (d (list (d (n "clap") (r "^3.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "snafu") (r "^0.7.1") (f (quote ("std"))) (k 0)) (d (n "hex-literal") (r "^0.3.1") (d #t) (k 2)))) (h "0zspr2d85pf9rjm8g4psmfq1ir89rs195nrfkig5kx6rhx6b7i57")))

