(define-module (crates-io fu el fuel-core-client-ext) #:use-module (crates-io))

(define-public crate-fuel-core-client-ext-0.1.0 (c (n "fuel-core-client-ext") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "cynic") (r "^2.2") (f (quote ("http-reqwest"))) (d #t) (k 0)) (d (n "fuel-core-client") (r "^0.22.3") (d #t) (k 0)) (d (n "fuel-core-client") (r "^0.22.3") (d #t) (k 1)) (d (n "fuel-core-types") (r "^0.22.3") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 2)))) (h "08cbzr1yfvbsbcvf2k5fq0rwkn3ygqka5zp4kijfv6dsyzkr4znm")))

