(define-module (crates-io fu el fuel-indexer-utils) #:use-module (crates-io))

(define-public crate-fuel-indexer-utils-0.1.0 (c (n "fuel-indexer-utils") (v "0.1.0") (h "0s5vzfgs5hwxy5w9ljc53xvg2hjh2pl71k5i28al96zqy80ifpmh")))

(define-public crate-fuel-indexer-utils-0.16.0 (c (n "fuel-indexer-utils") (v "0.16.0") (d (list (d (n "fuel-indexer-macros") (r "^0.16.0") (k 0)) (d (n "fuel-indexer-plugin") (r "^0.16.0") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "derive"))) (k 0)))) (h "1i3qg769ynl7s30sjqgkddpzhf0r201wgyk3wgw2q2ccd98cgx1w") (f (quote (("native-execution" "fuel-indexer-plugin/native-execution")))) (r "1.69.0")))

(define-public crate-fuel-indexer-utils-0.16.1 (c (n "fuel-indexer-utils") (v "0.16.1") (d (list (d (n "fuel-indexer-macros") (r "^0.16.1") (k 0)) (d (n "fuel-indexer-plugin") (r "^0.16.1") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "derive"))) (k 0)))) (h "1kyw27cympl9wylcdz30s6smikxr4k0k8ylrv50sxqay2kdh2kr0") (f (quote (("native-execution" "fuel-indexer-plugin/native-execution")))) (r "1.69.0")))

(define-public crate-fuel-indexer-utils-0.16.2 (c (n "fuel-indexer-utils") (v "0.16.2") (d (list (d (n "fuel-indexer-macros") (r "^0.16.2") (k 0)) (d (n "fuel-indexer-plugin") (r "^0.16.2") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "derive"))) (k 0)))) (h "0vldnqn9ys841k57fqwmppch0060zxq8r44l9862qp754hdihddz") (f (quote (("native-execution" "fuel-indexer-plugin/native-execution")))) (r "1.69.0")))

(define-public crate-fuel-indexer-utils-0.17.0 (c (n "fuel-indexer-utils") (v "0.17.0") (d (list (d (n "fuel-indexer-macros") (r "^0.17.0") (k 0)) (d (n "fuel-indexer-plugin") (r "^0.17.0") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "derive"))) (k 0)))) (h "1lwqn41c2bpb6yvs6d7gza3sa86yw6w8dp5qsglq6irlbch0b5kx") (f (quote (("native-execution" "fuel-indexer-plugin/native-execution")))) (r "1.69.0")))

(define-public crate-fuel-indexer-utils-0.17.1 (c (n "fuel-indexer-utils") (v "0.17.1") (d (list (d (n "fuel-indexer-macros") (r "^0.17.1") (k 0)) (d (n "fuel-indexer-plugin") (r "^0.17.1") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "derive"))) (k 0)))) (h "1xl1nyvhb2yr173xihz9jaxz8bv3k2mj4vabg9ym5191zg1yj8x0") (f (quote (("native-execution" "fuel-indexer-plugin/native-execution")))) (r "1.69.0")))

(define-public crate-fuel-indexer-utils-0.17.2 (c (n "fuel-indexer-utils") (v "0.17.2") (d (list (d (n "fuel-indexer-macros") (r "^0.17.2") (k 0)) (d (n "fuel-indexer-plugin") (r "^0.17.2") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "derive"))) (k 0)))) (h "1dbf44drrycqv9jdym7q6f87clgj4fv6967148r0ss655xdab0h5") (f (quote (("native-execution" "fuel-indexer-plugin/native-execution")))) (r "1.69.0")))

(define-public crate-fuel-indexer-utils-0.17.3 (c (n "fuel-indexer-utils") (v "0.17.3") (d (list (d (n "fuel-indexer-macros") (r "^0.17.3") (k 0)) (d (n "fuel-indexer-plugin") (r "^0.17.3") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "derive"))) (k 0)))) (h "03zhasbb3j2n6prg6zmh0lz6sya5gpd6f2wb0lb648hf9x9ypbhj") (f (quote (("native-execution" "fuel-indexer-plugin/native-execution")))) (r "1.69.0")))

(define-public crate-fuel-indexer-utils-0.18.0 (c (n "fuel-indexer-utils") (v "0.18.0") (d (list (d (n "fuel-indexer-macros") (r "^0.18.0") (k 0)) (d (n "fuel-indexer-plugin") (r "^0.18.0") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "derive"))) (k 0)))) (h "0xmbbnm218xb1j9dmhrf2g33547cs7qp0yzwcjcgl29z4n0rarb0") (f (quote (("native-execution" "fuel-indexer-plugin/native-execution")))) (r "1.69.0")))

(define-public crate-fuel-indexer-utils-0.18.1 (c (n "fuel-indexer-utils") (v "0.18.1") (d (list (d (n "fuel-indexer-macros") (r "^0.18.1") (k 0)) (d (n "fuel-indexer-plugin") (r "^0.18.1") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "derive"))) (k 0)))) (h "15nf47hbvliax6fc3527ng7fsaax44xfrd5fhhr56024n08z25xn") (f (quote (("native-execution" "fuel-indexer-plugin/native-execution")))) (r "1.69.0")))

(define-public crate-fuel-indexer-utils-0.18.2 (c (n "fuel-indexer-utils") (v "0.18.2") (d (list (d (n "fuel-indexer-macros") (r "^0.18.2") (k 0)) (d (n "fuel-indexer-plugin") (r "^0.18.2") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "derive"))) (k 0)))) (h "05l6mmi77la5krgvracdnksamh21a220v1s83na5vw2l4pkswvia") (f (quote (("native-execution" "fuel-indexer-plugin/native-execution")))) (r "1.69.0")))

(define-public crate-fuel-indexer-utils-0.18.3 (c (n "fuel-indexer-utils") (v "0.18.3") (d (list (d (n "fuel-indexer-macros") (r "^0.18.3") (k 0)) (d (n "fuel-indexer-plugin") (r "^0.18.3") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "derive"))) (k 0)))) (h "0hw3z3kf3i7a7fddjqbnnym0876ynkksr22f3msrj1rgsknqx21l") (f (quote (("native-execution" "fuel-indexer-plugin/native-execution")))) (r "1.69.0")))

(define-public crate-fuel-indexer-utils-0.18.4 (c (n "fuel-indexer-utils") (v "0.18.4") (d (list (d (n "fuel-indexer-macros") (r "^0.18.4") (k 0)) (d (n "fuel-indexer-plugin") (r "^0.18.4") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "derive"))) (k 0)))) (h "1p7gar1gd475yq8f6b8hxjliwr07s4dhxpjn8hq2fd326schl9h9") (f (quote (("native-execution" "fuel-indexer-plugin/native-execution")))) (r "1.69.0")))

(define-public crate-fuel-indexer-utils-0.18.5 (c (n "fuel-indexer-utils") (v "0.18.5") (d (list (d (n "fuel-indexer-macros") (r "^0.18.5") (k 0)) (d (n "fuel-indexer-plugin") (r "^0.18.5") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "derive"))) (k 0)))) (h "0zl4hbi7prgqrgpi95rcj6328h52ni8kbkcij443ylrnl0b52s5m") (f (quote (("native-execution" "fuel-indexer-plugin/native-execution")))) (r "1.69.0")))

(define-public crate-fuel-indexer-utils-0.19.0 (c (n "fuel-indexer-utils") (v "0.19.0") (d (list (d (n "fuel-indexer-macros") (r "^0.19.0") (k 0)) (d (n "fuel-indexer-plugin") (r "^0.19.0") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "derive"))) (k 0)))) (h "0raar9sbcbld0563gdrbhxfr7wgxyamvq9jsfxp95b3a4nx06ydy") (f (quote (("native-execution" "fuel-indexer-plugin/native-execution")))) (r "1.69.0")))

(define-public crate-fuel-indexer-utils-0.19.1 (c (n "fuel-indexer-utils") (v "0.19.1") (d (list (d (n "fuel-indexer-macros") (r "^0.19.1") (k 0)) (d (n "fuel-indexer-plugin") (r "^0.19.1") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "derive"))) (k 0)))) (h "0xy1r7m2bkhkikpp57xjw6r7crq39g610yb27vzlgppl6ar45v6i") (f (quote (("native-execution" "fuel-indexer-plugin/native-execution")))) (r "1.69.0")))

(define-public crate-fuel-indexer-utils-0.19.2 (c (n "fuel-indexer-utils") (v "0.19.2") (d (list (d (n "fuel-indexer-macros") (r "^0.19.2") (k 0)) (d (n "fuel-indexer-plugin") (r "^0.19.2") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "derive"))) (k 0)))) (h "1ymiz9dxs94cpfp5w2731yyrg23qinc69gip37qpxggg33ccfkbc") (f (quote (("native-execution" "fuel-indexer-plugin/native-execution")))) (r "1.69.0")))

(define-public crate-fuel-indexer-utils-0.19.3 (c (n "fuel-indexer-utils") (v "0.19.3") (d (list (d (n "fuel-indexer-macros") (r "^0.19.3") (k 0)) (d (n "fuel-indexer-plugin") (r "^0.19.3") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "derive"))) (k 0)))) (h "0sxjiy640ws4qd0z62vnlhm967rbkcrnfr9w7dk2jbzdmfjbxdhv") (f (quote (("native-execution" "fuel-indexer-plugin/native-execution")))) (r "1.69.0")))

(define-public crate-fuel-indexer-utils-0.19.4 (c (n "fuel-indexer-utils") (v "0.19.4") (d (list (d (n "fuel-indexer-macros") (r "^0.19.4") (k 0)) (d (n "fuel-indexer-plugin") (r "^0.19.4") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "derive"))) (k 0)))) (h "0z5iqizdrcxaqxzwp75qnp4yj2l0x0467fsmdrdclmi45k2czy52") (f (quote (("native-execution" "fuel-indexer-plugin/native-execution")))) (r "1.69.0")))

(define-public crate-fuel-indexer-utils-0.19.5 (c (n "fuel-indexer-utils") (v "0.19.5") (d (list (d (n "fuel-indexer-macros") (r "^0.19.5") (k 0)) (d (n "fuel-indexer-plugin") (r "^0.19.5") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "derive"))) (k 0)))) (h "1hyg9arbnypq28nyl3z8h57z89nyhljfy8ivjzbcrk3ydw0ywyq3") (f (quote (("native-execution" "fuel-indexer-plugin/native-execution")))) (r "1.69.0")))

(define-public crate-fuel-indexer-utils-0.20.0 (c (n "fuel-indexer-utils") (v "0.20.0") (d (list (d (n "fuel-indexer-macros") (r "^0.20.0") (k 0)) (d (n "fuel-indexer-plugin") (r "^0.20.0") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "derive"))) (k 0)))) (h "1r609rpmgf2h7idr1wmba6y5mwiiwpl25qcsh277kbmq2ihxvhfk") (f (quote (("native-execution" "fuel-indexer-plugin/native-execution")))) (r "1.71.1")))

(define-public crate-fuel-indexer-utils-0.20.1 (c (n "fuel-indexer-utils") (v "0.20.1") (d (list (d (n "fuel-indexer-macros") (r "^0.20.1") (k 0)) (d (n "fuel-indexer-plugin") (r "^0.20.1") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "derive"))) (k 0)))) (h "030f6gpy6a70z7k3l58m4la19s3a18hg6kwhwizpnzspr2pxx6s5") (f (quote (("native-execution" "fuel-indexer-plugin/native-execution")))) (r "1.71.1")))

(define-public crate-fuel-indexer-utils-0.20.5 (c (n "fuel-indexer-utils") (v "0.20.5") (d (list (d (n "fuel-indexer-macros") (r "^0.20.5") (k 0)) (d (n "fuel-indexer-plugin") (r "^0.20.5") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "derive"))) (k 0)))) (h "0cqpzn3rdx46l793hv1fiz91qg9zqd0q54gkpbll25fgp8x45y39") (f (quote (("native-execution" "fuel-indexer-plugin/native-execution")))) (r "1.72.0")))

(define-public crate-fuel-indexer-utils-0.20.7 (c (n "fuel-indexer-utils") (v "0.20.7") (d (list (d (n "fuel-indexer-macros") (r "^0.20.7") (k 0)) (d (n "fuel-indexer-plugin") (r "^0.20.7") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "derive"))) (k 0)))) (h "1hwyk5r86w2sxw0p2jms3y9wxp138rxl68lfziq77nb3msd385ac") (f (quote (("native-execution" "fuel-indexer-plugin/native-execution")))) (r "1.72.0")))

(define-public crate-fuel-indexer-utils-0.20.8 (c (n "fuel-indexer-utils") (v "0.20.8") (d (list (d (n "fuel-indexer-macros") (r "^0.20.8") (k 0)) (d (n "fuel-indexer-plugin") (r "^0.20.8") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "derive"))) (k 0)))) (h "0yvinql69c4xb90yf0w8jp9vbmvmblwqy36j0vjggsv5593nbq9r") (f (quote (("native-execution" "fuel-indexer-plugin/native-execution")))) (r "1.72.0")))

(define-public crate-fuel-indexer-utils-0.20.9 (c (n "fuel-indexer-utils") (v "0.20.9") (d (list (d (n "fuel-indexer-macros") (r "^0.20.9") (k 0)) (d (n "fuel-indexer-plugin") (r "^0.20.9") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "derive"))) (k 0)))) (h "17vvf0qkwljbsj9yim2cj60ibdx3977jcjv6vj25mpa5aydlnaly") (f (quote (("native-execution" "fuel-indexer-plugin/native-execution")))) (r "1.72.0")))

(define-public crate-fuel-indexer-utils-0.20.10 (c (n "fuel-indexer-utils") (v "0.20.10") (d (list (d (n "fuel-indexer-macros") (r "^0.20.10") (k 0)) (d (n "fuel-indexer-plugin") (r "^0.20.10") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "derive"))) (k 0)))) (h "1gjqq4b62f3336mmi6cpx8dpivd1bd9yhmslgg2sva2pbxsikvs5") (f (quote (("native-execution" "fuel-indexer-plugin/native-execution")))) (r "1.72.0")))

(define-public crate-fuel-indexer-utils-0.20.11 (c (n "fuel-indexer-utils") (v "0.20.11") (d (list (d (n "fuel-indexer-macros") (r "^0.20.11") (k 0)) (d (n "fuel-indexer-plugin") (r "^0.20.11") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "derive"))) (k 0)))) (h "041h3lwq6cyh8bxhpy7lzanqxnipz1p6py27g3908j0bpxkvi6rm") (f (quote (("native-execution" "fuel-indexer-plugin/native-execution")))) (r "1.72.0")))

(define-public crate-fuel-indexer-utils-0.21.0 (c (n "fuel-indexer-utils") (v "0.21.0") (d (list (d (n "fuel-indexer-macros") (r "^0.21.0") (k 0)) (d (n "fuel-indexer-plugin") (r "^0.21.0") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "derive"))) (k 0)))) (h "0jsa5wz2g370lhaq7k5hhi22wqbghpb61hxjv4frgfvzc60jfc92") (f (quote (("native-execution" "fuel-indexer-plugin/native-execution")))) (r "1.72.1")))

(define-public crate-fuel-indexer-utils-0.21.1 (c (n "fuel-indexer-utils") (v "0.21.1") (d (list (d (n "fuel-indexer-macros") (r "^0.21.1") (k 0)) (d (n "fuel-indexer-plugin") (r "^0.21.1") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "derive"))) (k 0)))) (h "0i6xmgxinsl6bp72njmnpji7lq6rc2f9dyrdkzng3pc99pdrj4k2") (f (quote (("native-execution" "fuel-indexer-plugin/native-execution")))) (r "1.73.0")))

(define-public crate-fuel-indexer-utils-0.21.2 (c (n "fuel-indexer-utils") (v "0.21.2") (d (list (d (n "fuel-indexer-macros") (r "^0.21.2") (k 0)) (d (n "fuel-indexer-plugin") (r "^0.21.2") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "derive"))) (k 0)))) (h "19cmlx0b25ig9k7f408c27n3sxd5m9s049f68zkbwsa771rz9907") (f (quote (("native-execution" "fuel-indexer-plugin/native-execution")))) (r "1.73.0")))

(define-public crate-fuel-indexer-utils-0.22.0 (c (n "fuel-indexer-utils") (v "0.22.0") (d (list (d (n "fuel-indexer-macros") (r "^0.22.0") (k 0)) (d (n "fuel-indexer-plugin") (r "^0.22.0") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "derive"))) (k 0)))) (h "0c4wpas2dlkxzcdpa26p40cz3iqhpq32yjpl2azvcvycx1l1m8hk") (r "1.73.0")))

(define-public crate-fuel-indexer-utils-0.23.0 (c (n "fuel-indexer-utils") (v "0.23.0") (d (list (d (n "fuel-indexer-macros") (r "^0.23.0") (k 0)) (d (n "fuel-indexer-plugin") (r "^0.23.0") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "derive"))) (k 0)))) (h "0hz7y9fqm6293iwidfw12h48w808c4swfbg412630qfwghnviiaz") (r "1.73.0")))

(define-public crate-fuel-indexer-utils-0.24.0 (c (n "fuel-indexer-utils") (v "0.24.0") (d (list (d (n "fuel-indexer-macros") (r "^0.24.0") (k 0)) (d (n "fuel-indexer-plugin") (r "^0.24.0") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "derive"))) (k 0)))) (h "0div1gq3cricya5qgd2shvhw2hj4ll2ckgpi7r67gwwig2lrh1kn") (r "1.73.0")))

(define-public crate-fuel-indexer-utils-0.24.1 (c (n "fuel-indexer-utils") (v "0.24.1") (d (list (d (n "fuel-indexer-macros") (r "^0.24.1") (k 0)) (d (n "fuel-indexer-plugin") (r "^0.24.1") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "derive"))) (k 0)))) (h "0c5fz9cnwv8kbgxajvdnwdd7hq1788wv4lh3srgpkw0a23i995n7") (r "1.73.0")))

(define-public crate-fuel-indexer-utils-0.24.2 (c (n "fuel-indexer-utils") (v "0.24.2") (d (list (d (n "fuel-indexer-macros") (r "^0.24.2") (k 0)) (d (n "fuel-indexer-plugin") (r "^0.24.2") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "derive"))) (k 0)))) (h "0r9iyajgcd7lz217j4z6i1c88hs29zcsx3zgv8vnhivz00bf79sl") (r "1.73.0")))

(define-public crate-fuel-indexer-utils-0.24.3 (c (n "fuel-indexer-utils") (v "0.24.3") (d (list (d (n "fuel-indexer-macros") (r "^0.24.3") (k 0)) (d (n "fuel-indexer-plugin") (r "^0.24.3") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "derive"))) (k 0)))) (h "1x08lz0m3vqddsvnv7ps02fp4h7hfd035zfk1z79sqqpap3p7kjm") (r "1.73.0")))

(define-public crate-fuel-indexer-utils-0.25.0 (c (n "fuel-indexer-utils") (v "0.25.0") (d (list (d (n "fuel-indexer-macros") (r "^0.25.0") (k 0)) (d (n "fuel-indexer-plugin") (r "^0.25.0") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "derive"))) (k 0)))) (h "0xnc20cp7m4s07lvqc6d7ddv82k7blza04zgfki2vl0hn51hqb7i") (r "1.75.0")))

