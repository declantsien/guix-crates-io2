(define-module (crates-io fu el fuel-indexer-macro-utils) #:use-module (crates-io))

(define-public crate-fuel-indexer-macro-utils-0.0.0 (c (n "fuel-indexer-macro-utils") (v "0.0.0") (h "0rxgwjpb6kf1y7fldq4yq5h1qm3q8dx6j94azydzd21fj500iyg4")))

(define-public crate-fuel-indexer-macro-utils-0.12.0 (c (n "fuel-indexer-macro-utils") (v "0.12.0") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1kj5kq4ps6g1ijbgds7cnzv7p5mbk4vjkj6cg87bj9xb310r4q0i") (r "1.69.0")))

(define-public crate-fuel-indexer-macro-utils-0.12.1 (c (n "fuel-indexer-macro-utils") (v "0.12.1") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0kjswsbfjvi9sxnam9iww1ir453p57dxjmi3p7kmz737ifwc9kpy") (r "1.69.0")))

(define-public crate-fuel-indexer-macro-utils-0.13.0 (c (n "fuel-indexer-macro-utils") (v "0.13.0") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1myy7dp39fs6k1i8i90dshqwai202r5hywgb98qq51611dpgdb93") (r "1.69.0")))

(define-public crate-fuel-indexer-macro-utils-0.13.2 (c (n "fuel-indexer-macro-utils") (v "0.13.2") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1wsmbn5ggahx2sdj0xqra5a0nq1p1fkrsizq86ppx1bb559scgcf") (r "1.69.0")))

(define-public crate-fuel-indexer-macro-utils-0.14.0 (c (n "fuel-indexer-macro-utils") (v "0.14.0") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1kyyd1y6flawi8nw1hq6rn8621502dsaf145kkkw534f2af3cxzv") (r "1.69.0")))

(define-public crate-fuel-indexer-macro-utils-0.15.0 (c (n "fuel-indexer-macro-utils") (v "0.15.0") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0kgifxlw6w0180a2k1bga3yvl6dyibxgqfqsf6rzijd4y3w9db01") (r "1.69.0")))

(define-public crate-fuel-indexer-macro-utils-0.15.1 (c (n "fuel-indexer-macro-utils") (v "0.15.1") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "103fmsxz6fbcr9s85ayji3y568qc206f10sjml97zn8nfbf61a7w") (r "1.69.0")))

(define-public crate-fuel-indexer-macro-utils-0.16.0 (c (n "fuel-indexer-macro-utils") (v "0.16.0") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "057z3ajk84s02nxsl8aqxzc2a0bgz4qkvp0nmcga7lb36z1jp8xa") (r "1.69.0")))

(define-public crate-fuel-indexer-macro-utils-0.16.1 (c (n "fuel-indexer-macro-utils") (v "0.16.1") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "11jz7s6zbja2wsvc7rhcr46xgamn49rjbmd7lvvkjhxg1w28i3ww") (r "1.69.0")))

(define-public crate-fuel-indexer-macro-utils-0.16.2 (c (n "fuel-indexer-macro-utils") (v "0.16.2") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0gf11dcg0l7fyx939j8fi2rnba8ikagjdv0p0n9mq00m6ckwg5h9") (r "1.69.0")))

(define-public crate-fuel-indexer-macro-utils-0.17.0 (c (n "fuel-indexer-macro-utils") (v "0.17.0") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "12zxlp7li0xns555rjrr5r9qngvs8jl0pxw1awxqim03nxd1hb07") (r "1.69.0")))

(define-public crate-fuel-indexer-macro-utils-0.17.1 (c (n "fuel-indexer-macro-utils") (v "0.17.1") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0aj4wfz4bnjlgvzq71kj4jscbsr6hm0pyp3i9hzsvv5zbj9jg6kq") (r "1.69.0")))

(define-public crate-fuel-indexer-macro-utils-0.17.2 (c (n "fuel-indexer-macro-utils") (v "0.17.2") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1vwha6fd6v3yl67227iwl0xlcdzlywiqy99sf9ynb4h66i8xb1r8") (r "1.69.0")))

(define-public crate-fuel-indexer-macro-utils-0.17.3 (c (n "fuel-indexer-macro-utils") (v "0.17.3") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "13q7afyg36dbm76v7g9gshpf1a4qgfw1hbb4g86fn4im5wz84i6v") (r "1.69.0")))

(define-public crate-fuel-indexer-macro-utils-0.18.0 (c (n "fuel-indexer-macro-utils") (v "0.18.0") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0d142lmr77ff99akyvnx3xacxaqr5bm37605qn0nsrxakn24jyrb") (r "1.69.0")))

(define-public crate-fuel-indexer-macro-utils-0.18.1 (c (n "fuel-indexer-macro-utils") (v "0.18.1") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "12i4fpy10xjikdrqblxb6m4342bv8hjd5vvxrz112c2adwvzxpcb") (r "1.69.0")))

(define-public crate-fuel-indexer-macro-utils-0.18.2 (c (n "fuel-indexer-macro-utils") (v "0.18.2") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1bp6q58qpjhg341kxj7jxzkj3ivmagq4rpplqr04fhjvxrq710zn") (r "1.69.0")))

(define-public crate-fuel-indexer-macro-utils-0.18.3 (c (n "fuel-indexer-macro-utils") (v "0.18.3") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0aynzh7c6pb5m4ii28kkvvkj1pspla7w56rqh0farh21f1qp8p8q") (r "1.69.0")))

(define-public crate-fuel-indexer-macro-utils-0.18.4 (c (n "fuel-indexer-macro-utils") (v "0.18.4") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "04561f9zfb2kpsfcjsgqzs4fjbmlhi35g6j1fcjg078hmy00q90n") (r "1.69.0")))

(define-public crate-fuel-indexer-macro-utils-0.18.5 (c (n "fuel-indexer-macro-utils") (v "0.18.5") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "054hi44dw34qs5ms59f9rsar33nhdc86dg0kygcv09w5k0rnvzhc") (r "1.69.0")))

(define-public crate-fuel-indexer-macro-utils-0.19.0 (c (n "fuel-indexer-macro-utils") (v "0.19.0") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0nd8s4n7gmyhjl4i987l7hshhn5lzkdxx073g7zirhz0ligmq5vb") (r "1.69.0")))

(define-public crate-fuel-indexer-macro-utils-0.19.1 (c (n "fuel-indexer-macro-utils") (v "0.19.1") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1f471lh589p8l7vbzqi1c47xzmq9w2ayvvdc1wplma5grkyl2bjs") (r "1.69.0")))

(define-public crate-fuel-indexer-macro-utils-0.19.2 (c (n "fuel-indexer-macro-utils") (v "0.19.2") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "184m9wqa8hfdfz7lh3d49lkyw0f9vmvfwv08sifxdvq92hli2abk") (r "1.69.0")))

(define-public crate-fuel-indexer-macro-utils-0.19.3 (c (n "fuel-indexer-macro-utils") (v "0.19.3") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0chj1118s889awk4x57gn48h1gr3fspb3k6kq6p0q59970g11lby") (r "1.69.0")))

(define-public crate-fuel-indexer-macro-utils-0.19.4 (c (n "fuel-indexer-macro-utils") (v "0.19.4") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1vi1iqd1pqy16lly469bd56b965krbifrc0lzbvrbhw5bcp4fl11") (r "1.69.0")))

(define-public crate-fuel-indexer-macro-utils-0.19.5 (c (n "fuel-indexer-macro-utils") (v "0.19.5") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1w12ivny6aaacf4bx60771gf147clalhq425p2fkwd5wn8b5pf83") (r "1.69.0")))

(define-public crate-fuel-indexer-macro-utils-0.20.0 (c (n "fuel-indexer-macro-utils") (v "0.20.0") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "009x5lpvpx6v5hrq0h0nacc7qffqi5xyx9xrjig5sv8lvdrba6fc") (r "1.71.1")))

(define-public crate-fuel-indexer-macro-utils-0.20.1 (c (n "fuel-indexer-macro-utils") (v "0.20.1") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "03li94xdjizflrn4qhwckahpw29lsg16zdh7cvddi4a3ylp8kw5k") (r "1.71.1")))

(define-public crate-fuel-indexer-macro-utils-0.20.2 (c (n "fuel-indexer-macro-utils") (v "0.20.2") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0mq356fjnj8akk6d1ygs3rg9c4y9hk93yhixddd2lx96v2j9w8kl") (r "1.71.1")))

(define-public crate-fuel-indexer-macro-utils-0.20.3 (c (n "fuel-indexer-macro-utils") (v "0.20.3") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0gsl60f1yq40iji90gaq29fkabpziiqvm09rvaz3pywnqxyhpqdr") (r "1.71.1")))

(define-public crate-fuel-indexer-macro-utils-0.20.4 (c (n "fuel-indexer-macro-utils") (v "0.20.4") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "09dsxa0hc8j00cammgmpbfzpjdryb6xnv1ydz705m1q4va3ympkl") (r "1.71.1")))

(define-public crate-fuel-indexer-macro-utils-0.20.5 (c (n "fuel-indexer-macro-utils") (v "0.20.5") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0f4ap4kpzbviksasp8v37qsk7v8g4nalap91gv1ccjsh5rzmixxd") (r "1.72.0")))

(define-public crate-fuel-indexer-macro-utils-0.20.6 (c (n "fuel-indexer-macro-utils") (v "0.20.6") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1hy4aqyl64n8xi70va177imln1k6pnppb6gk31r6nnin1snyf2rz") (r "1.72.0")))

(define-public crate-fuel-indexer-macro-utils-0.20.7 (c (n "fuel-indexer-macro-utils") (v "0.20.7") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1czfgddi9slqib0rf9pcly4gf6dfncfmga0wm2ch207pwbq72mdc") (r "1.72.0")))

(define-public crate-fuel-indexer-macro-utils-0.20.8 (c (n "fuel-indexer-macro-utils") (v "0.20.8") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1k8wmsr041lyg84b9sxv8rqxsjnq39qmh7kblkgl9jplzyiafaqr") (r "1.72.0")))

(define-public crate-fuel-indexer-macro-utils-0.20.9 (c (n "fuel-indexer-macro-utils") (v "0.20.9") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0rkam284kq390lrhb1qvm1i19jd37skv5fvqgnaasqvjya8ik44w") (r "1.72.0")))

(define-public crate-fuel-indexer-macro-utils-0.20.10 (c (n "fuel-indexer-macro-utils") (v "0.20.10") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "04xdnvfqp795aq8gldgckablzwxnzfnp2pyy5wpc0p523yprr6fn") (r "1.72.0")))

(define-public crate-fuel-indexer-macro-utils-0.20.11 (c (n "fuel-indexer-macro-utils") (v "0.20.11") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "01janmd17flw62cyqwbis701d4nyc8bc009m1v7z9554ccjr8wrg") (r "1.72.0")))

(define-public crate-fuel-indexer-macro-utils-0.21.0 (c (n "fuel-indexer-macro-utils") (v "0.21.0") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1jg3glgbnz0acdqhcqxv1fv7ifab2ajyjnqylb94f7b2iaq5phq2") (r "1.72.1")))

(define-public crate-fuel-indexer-macro-utils-0.21.1 (c (n "fuel-indexer-macro-utils") (v "0.21.1") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0p7arydffav2hnq6x5icaldv03f223w11cmn5zrvjzkwvizwgxyy") (r "1.73.0")))

(define-public crate-fuel-indexer-macro-utils-0.21.2 (c (n "fuel-indexer-macro-utils") (v "0.21.2") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0i6kqp20qx4jw8cs2p7prvba9ig1ambjhqvnkxw545j68waxj1qk") (r "1.73.0")))

(define-public crate-fuel-indexer-macro-utils-0.22.0 (c (n "fuel-indexer-macro-utils") (v "0.22.0") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0vq2mm2sfrxfx2ihyx2j91kf9aqdsvih2smhargdjwq3disghkpp") (r "1.73.0")))

(define-public crate-fuel-indexer-macro-utils-0.23.0 (c (n "fuel-indexer-macro-utils") (v "0.23.0") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0mvvgj7yh7l5r37i5jfm5yc1nmg3cpr00v8swshlgfgwpnka96jq") (r "1.73.0")))

(define-public crate-fuel-indexer-macro-utils-0.24.0 (c (n "fuel-indexer-macro-utils") (v "0.24.0") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1rp5mk1gp6jwn7xnbaj42xk6wx2z98v5mkzxgrggqmsi2lgrh8y7") (r "1.73.0")))

(define-public crate-fuel-indexer-macro-utils-0.24.1 (c (n "fuel-indexer-macro-utils") (v "0.24.1") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1kwkkysimvhri9zp53mgk5qgnr3z4x53zy8sdgiwzcb6mm9hjyc0") (r "1.73.0")))

(define-public crate-fuel-indexer-macro-utils-0.24.2 (c (n "fuel-indexer-macro-utils") (v "0.24.2") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "03f8iqdvg8f9pn7pnr7h39p48hhjhjc87g864g04xs2005ypg9fm") (r "1.73.0")))

(define-public crate-fuel-indexer-macro-utils-0.24.3 (c (n "fuel-indexer-macro-utils") (v "0.24.3") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "03fsix5dkksl2rh7gdvnr10baamrmzh92hxj888yx4jah2i17x22") (r "1.73.0")))

(define-public crate-fuel-indexer-macro-utils-0.25.0 (c (n "fuel-indexer-macro-utils") (v "0.25.0") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "04g49jvar8nhj1vz46wh10l2b7x59d2zx295rdxsikdc1vmmpldv") (r "1.75.0")))

