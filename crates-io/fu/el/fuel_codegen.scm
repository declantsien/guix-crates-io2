(define-module (crates-io fu el fuel_codegen) #:use-module (crates-io))

(define-public crate-fuel_codegen-0.0.1 (c (n "fuel_codegen") (v "0.0.1") (d (list (d (n "cargo-expand") (r "^0.4.12") (d #t) (k 2)) (d (n "json") (r "^0.11.14") (d #t) (k 0)) (d (n "log") (r "^0.4.7") (d #t) (k 0)) (d (n "pest") (r "^2.1.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.30") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "rocket") (r "^0.4.2") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "1iqkmzqlasx2cqkx6py2a532idrz8b1vw72cv81bs7jy8zsknaba")))

