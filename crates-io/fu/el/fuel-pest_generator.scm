(define-module (crates-io fu el fuel-pest_generator) #:use-module (crates-io))

(define-public crate-fuel-pest_generator-3.0.4 (c (n "fuel-pest_generator") (v "3.0.4") (d (list (d (n "pest") (r "^3.0.4") (k 0) (p "fuel-pest")) (d (n "pest_meta") (r "^3.0.4") (d #t) (k 0) (p "fuel-pest_meta")) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0s25p4mlwmkx5l1bpd5sz1c685fsxby42dfl4j3ckg6xkla14cg7") (f (quote (("std" "pest/std") ("default" "std"))))))

