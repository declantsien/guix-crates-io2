(define-module (crates-io fu el fuel-dummy-test-subcrate-c) #:use-module (crates-io))

(define-public crate-fuel-dummy-test-subcrate-c-0.2.11 (c (n "fuel-dummy-test-subcrate-c") (v "0.2.11") (h "0gb6hd6dp6ag6bn7ncxw27fik691q0h239wv2q11ld735p67yam8")))

(define-public crate-fuel-dummy-test-subcrate-c-0.2.19 (c (n "fuel-dummy-test-subcrate-c") (v "0.2.19") (h "15v1i96wwxiampyfb5klpjgnybd1q8yyzcpvbgjq22yfra6j8x6l")))

(define-public crate-fuel-dummy-test-subcrate-c-0.2.20 (c (n "fuel-dummy-test-subcrate-c") (v "0.2.20") (h "19fq7qr6y25klarx8ljy8zyxm4h23wr4507srsxhjx7kpw4iql7n")))

(define-public crate-fuel-dummy-test-subcrate-c-0.2.21 (c (n "fuel-dummy-test-subcrate-c") (v "0.2.21") (h "0g964dvndfr6pffyx32zllw2b3rhg85zh6wvk9k3z8fbwxbn4j28")))

(define-public crate-fuel-dummy-test-subcrate-c-0.2.22 (c (n "fuel-dummy-test-subcrate-c") (v "0.2.22") (h "0lmnsjglpr3sfxcrqi8dwza83dfbrf2hi0aqbxrqxyvxfdm9ck43")))

