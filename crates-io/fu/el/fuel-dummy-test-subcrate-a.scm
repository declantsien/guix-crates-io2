(define-module (crates-io fu el fuel-dummy-test-subcrate-a) #:use-module (crates-io))

(define-public crate-fuel-dummy-test-subcrate-a-0.2.5 (c (n "fuel-dummy-test-subcrate-a") (v "0.2.5") (h "1hgy0v16qq5458in428caixwdmm9idl70agdwx1j0yqb044lvjxc")))

(define-public crate-fuel-dummy-test-subcrate-a-0.2.6 (c (n "fuel-dummy-test-subcrate-a") (v "0.2.6") (d (list (d (n "subcrate_b") (r "^0.2.6") (d #t) (k 0) (p "fuel-dummy-test-subcrate-b")))) (h "1wz25sfxyfciy02vz8xmhpbq8wj9xvjh71b8ib6rblw9wplly6pf")))

(define-public crate-fuel-dummy-test-subcrate-a-0.2.7 (c (n "fuel-dummy-test-subcrate-a") (v "0.2.7") (h "0b0yy9mbkj1x1p8vh5jid4zc57sfd6vhfjz39p6bs595ibf9iyb4")))

(define-public crate-fuel-dummy-test-subcrate-a-0.2.8 (c (n "fuel-dummy-test-subcrate-a") (v "0.2.8") (d (list (d (n "subcrate_b") (r "^0.2") (d #t) (k 0) (p "fuel-dummy-test-subcrate-b")))) (h "0bbw66a5j874ck7ww74ji2m8yi7vjyif4salqidhvq3nrbjpmbzy")))

(define-public crate-fuel-dummy-test-subcrate-a-0.2.10 (c (n "fuel-dummy-test-subcrate-a") (v "0.2.10") (d (list (d (n "subcrate_b") (r "^0.2.10") (d #t) (k 0) (p "fuel-dummy-test-subcrate-b")))) (h "0vgay2ad5b2ixk88kzj6d7dfh3m95s486hsw57fv8qbgwgq60m3h")))

(define-public crate-fuel-dummy-test-subcrate-a-0.2.11 (c (n "fuel-dummy-test-subcrate-a") (v "0.2.11") (h "0xlr87iz2ndbqzd1vi0rhrwk7yvma7srr59qmwm0sadiv1jd8z0i")))

(define-public crate-fuel-dummy-test-subcrate-a-0.2.19 (c (n "fuel-dummy-test-subcrate-a") (v "0.2.19") (h "0q2iw3dlndzvc9xz7qiiq0f2rdl96mfaq3dcl8rzhykhd1mqjv8x")))

(define-public crate-fuel-dummy-test-subcrate-a-0.2.20 (c (n "fuel-dummy-test-subcrate-a") (v "0.2.20") (d (list (d (n "fuel-dummy-test-subcrate-d") (r "^0.2.20") (d #t) (k 0)) (d (n "fuel-dummy-test-subcrate-e") (r "^0.2.20") (d #t) (k 0)) (d (n "fuel-dummy-test-subcrate-f") (r "^0.2.20") (d #t) (k 0)))) (h "0aa6fibnqmlcirg5w130ndpg5wsql3hwadxb1cf2l7fsy3cja41g")))

(define-public crate-fuel-dummy-test-subcrate-a-0.2.21 (c (n "fuel-dummy-test-subcrate-a") (v "0.2.21") (d (list (d (n "fuel-dummy-test-subcrate-d") (r "^0.2.21") (d #t) (k 0)) (d (n "fuel-dummy-test-subcrate-e") (r "^0.2.21") (d #t) (k 0)) (d (n "fuel-dummy-test-subcrate-f") (r "^0.2.21") (d #t) (k 0)))) (h "0g36jsx6v79ycyf8jrdclq22d72wcfvd5f26ay7h1zyh7msxr4ix")))

(define-public crate-fuel-dummy-test-subcrate-a-0.2.22 (c (n "fuel-dummy-test-subcrate-a") (v "0.2.22") (d (list (d (n "fuel-dummy-test-subcrate-d") (r "^0.2.22") (d #t) (k 0)) (d (n "fuel-dummy-test-subcrate-e") (r "^0.2.22") (d #t) (k 0)) (d (n "fuel-dummy-test-subcrate-f") (r "^0.2.22") (d #t) (k 0)))) (h "1kf0cblsyghfcbc6kg7xslb7hc4c642gabs2rjmqgkszdd7jqs1h")))

