(define-module (crates-io fu el fuel-block-importer) #:use-module (crates-io))

(define-public crate-fuel-block-importer-0.9.1 (c (n "fuel-block-importer") (v "0.9.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "fuel-core-interfaces") (r "^0.9.1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "tokio") (r "^1.14") (f (quote ("full"))) (d #t) (k 0)))) (h "19dpy693b2civhvwwkvi6gca5yxhaqg72abkay5zrwppd3frln3b")))

(define-public crate-fuel-block-importer-0.9.2 (c (n "fuel-block-importer") (v "0.9.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "fuel-core-interfaces") (r "^0.9.2") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "tokio") (r "^1.14") (f (quote ("full"))) (d #t) (k 0)))) (h "0b4kj4wsksr8rrz464y09wa3b717wb0c75qpwrpb8hmwh0979y65")))

(define-public crate-fuel-block-importer-0.9.3 (c (n "fuel-block-importer") (v "0.9.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "fuel-core-interfaces") (r "^0.9.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "tokio") (r "^1.14") (f (quote ("full"))) (d #t) (k 0)))) (h "0jr1n501whmavmc77iq5zrznqviif5jscw4s61aaq236xsqc0i4i")))

(define-public crate-fuel-block-importer-0.9.4 (c (n "fuel-block-importer") (v "0.9.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "fuel-core-interfaces") (r "^0.9.4") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "tokio") (r "^1.14") (f (quote ("full"))) (d #t) (k 0)))) (h "060r99l2ps5ksx0sv7rcnk81m150xa0vcp3paqpjinqj8l72jhlf")))

(define-public crate-fuel-block-importer-0.9.5 (c (n "fuel-block-importer") (v "0.9.5") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "fuel-core-interfaces") (r "^0.9.5") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "tokio") (r "^1.14") (f (quote ("full"))) (d #t) (k 0)))) (h "0cc56rrnxj6z9lm9zphnksi8kfq88y0bhwcpb8biv2jbcr9hi92i")))

(define-public crate-fuel-block-importer-0.9.6 (c (n "fuel-block-importer") (v "0.9.6") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "fuel-core-interfaces") (r "^0.9.6") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "tokio") (r "^1.14") (f (quote ("full"))) (d #t) (k 0)))) (h "10zbni6s96rzkvclaz41qlf30ncx0c34nfh0705jaa63ykxyb7z7")))

(define-public crate-fuel-block-importer-0.9.8 (c (n "fuel-block-importer") (v "0.9.8") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "fuel-core-interfaces") (r "^0.9.8") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "tokio") (r "^1.14") (f (quote ("full"))) (d #t) (k 0)))) (h "0z1i10xm07byfxajvjklc48468nl7b6n1mpf7iwpik05z3kfym4n")))

(define-public crate-fuel-block-importer-0.10.0 (c (n "fuel-block-importer") (v "0.10.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "fuel-core-interfaces") (r "^0.10.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "tokio") (r "^1.14") (f (quote ("full"))) (d #t) (k 0)))) (h "0q5c51j051f64hc914q15x1f95si6liq9w94rqw3f4ja0ij104v0")))

(define-public crate-fuel-block-importer-0.10.1 (c (n "fuel-block-importer") (v "0.10.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "fuel-core-interfaces") (r "^0.10.1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "tokio") (r "^1.14") (f (quote ("full"))) (d #t) (k 0)))) (h "1p53385sbgqrcpfpw3307cwzmfq7av5cn25q9f63z0yzn7348m8j")))

(define-public crate-fuel-block-importer-0.11.2 (c (n "fuel-block-importer") (v "0.11.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "fuel-core-interfaces") (r "^0.11.2") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "tokio") (r "^1.21") (f (quote ("full"))) (d #t) (k 0)))) (h "0nmgxir1rnw6c213i98286frs782n8h5prqyla8m2l1fpxcxj84g")))

(define-public crate-fuel-block-importer-0.12.0 (c (n "fuel-block-importer") (v "0.12.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "fuel-core-interfaces") (r "^0.12.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "tokio") (r "^1.21") (f (quote ("full"))) (d #t) (k 0)))) (h "0dx2pcs5i6fsx0h5z15dzqh2vr9zh0dnfgrlz4gyf3f6h7jl2zjg")))

(define-public crate-fuel-block-importer-0.13.0 (c (n "fuel-block-importer") (v "0.13.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "fuel-core-interfaces") (r "^0.13.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "tokio") (r "^1.21") (f (quote ("full"))) (d #t) (k 0)))) (h "1q0a50i17l8mchfkfz027lrbzpv8d9qladgdxr2ydqf32yf63sqz")))

(define-public crate-fuel-block-importer-0.13.1 (c (n "fuel-block-importer") (v "0.13.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "fuel-core-interfaces") (r "^0.13.1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "tokio") (r "^1.21") (f (quote ("full"))) (d #t) (k 0)))) (h "1gk144cq1l63vmbjzs5y7gx6gddjmg6xvb4nbgwgjl6k0cl3dxjv")))

(define-public crate-fuel-block-importer-0.13.2 (c (n "fuel-block-importer") (v "0.13.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "fuel-core-interfaces") (r "^0.13.2") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "tokio") (r "^1.21") (f (quote ("full"))) (d #t) (k 0)))) (h "0vhj0hbk4kn1w83xxmhdrydrgvik3x39w2g5mac7wslxbjr5ar39")))

(define-public crate-fuel-block-importer-0.14.0 (c (n "fuel-block-importer") (v "0.14.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "fuel-core-interfaces") (r "^0.14.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "tokio") (r "^1.21") (f (quote ("full"))) (d #t) (k 0)))) (h "1ka8qrg6626s61z1p15qf8bw4zx4ranp50lcwmv8axyvmlrdqz8y")))

(define-public crate-fuel-block-importer-0.14.1 (c (n "fuel-block-importer") (v "0.14.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "fuel-core-interfaces") (r "^0.14.1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "tokio") (r "^1.21") (f (quote ("full"))) (d #t) (k 0)))) (h "1q5vmqaxkyg3sk1jrvddx204a39b0x1n5r72mg9mlm38hn5k7chl")))

(define-public crate-fuel-block-importer-0.15.0 (c (n "fuel-block-importer") (v "0.15.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "fuel-core-interfaces") (r "^0.15.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "tokio") (r "^1.21") (f (quote ("full"))) (d #t) (k 0)))) (h "1x87rcc4jfbzn6d45bxxzwvh419gim7m14cjz2zamf4fm0dwrsjn")))

(define-public crate-fuel-block-importer-0.15.1 (c (n "fuel-block-importer") (v "0.15.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "fuel-core-interfaces") (r "^0.15.1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "tokio") (r "^1.21") (f (quote ("full"))) (d #t) (k 0)))) (h "1rlc3kr3j3h7yv1l41i8cgd2rd8sprhqyiaaqaxbwairjh6wslcy")))

(define-public crate-fuel-block-importer-0.14.2 (c (n "fuel-block-importer") (v "0.14.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "fuel-core-interfaces") (r "^0.14.2") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "tokio") (r "^1.21") (f (quote ("full"))) (d #t) (k 0)))) (h "1m8a40h32rqbqg0gb1pifpdpzn3scs5sybbb6346aa89n1r360xf")))

(define-public crate-fuel-block-importer-0.15.3 (c (n "fuel-block-importer") (v "0.15.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "fuel-core-interfaces") (r "^0.15.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "tokio") (r "^1.21") (f (quote ("full"))) (d #t) (k 0)))) (h "0641h5gaxmjrxif14wk069wf8ll12sbbww0fzvf6k6494djqdg1j")))

