(define-module (crates-io fu el fuel_line) #:use-module (crates-io))

(define-public crate-fuel_line-0.1.0 (c (n "fuel_line") (v "0.1.0") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "fuel_line_derive") (r "^0.1") (d #t) (k 0)) (d (n "uuid") (r "^0.6") (f (quote ("v4"))) (d #t) (k 0)))) (h "034j40a6g5zykdlbqnrkd5h7xh5aqaccvgdh2baviiw0gdyy9b1f")))

(define-public crate-fuel_line-0.1.2 (c (n "fuel_line") (v "0.1.2") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "fuel_line_derive") (r "^0.1.2") (d #t) (k 0)) (d (n "uuid") (r "^0.6") (f (quote ("v4"))) (d #t) (k 0)))) (h "0sw1jrjankvhcyjkj5ddvvxlj3dg4lw4sxw5rpi2jdaxrmd4nzdp")))

