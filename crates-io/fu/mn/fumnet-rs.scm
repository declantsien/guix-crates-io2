(define-module (crates-io fu mn fumnet-rs) #:use-module (crates-io))

(define-public crate-fumnet-rs-0.1.0 (c (n "fumnet-rs") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0irb130hhvqd8aibl0wk6rc2ib785lq2yhdyvcx2jzccx1hfmnlz")))

