(define-module (crates-io fu mn fumnet) #:use-module (crates-io))

(define-public crate-fumnet-0.1.0 (c (n "fumnet") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1ai7j2wl1yrq06j07kms73gn26dfhjdj8q0ya0ff04jj17igwhlw")))

(define-public crate-fumnet-0.2.0 (c (n "fumnet") (v "0.2.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (d #t) (k 0)))) (h "1d4qc1hmz4ff0kyv9y87zc03hji94xqqhl4iycl8l4vh749jswk0")))

(define-public crate-fumnet-0.2.1 (c (n "fumnet") (v "0.2.1") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)))) (h "19dlj8xzb9k0wjdxi1nmb5fp3cx7c2m9gkwsn9njdn0cdfs16h4k")))

(define-public crate-fumnet-0.2.2 (c (n "fumnet") (v "0.2.2") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)))) (h "0jzrfqylq6hwiijx52lbpsc9d8q9j3nxzbmyvqvwnbk517vx2yry")))

(define-public crate-fumnet-0.2.3 (c (n "fumnet") (v "0.2.3") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)))) (h "159n8qvj3ggmfjvhrf3n140ndjbvl25la0mg18vqc0m4mc8f356d")))

