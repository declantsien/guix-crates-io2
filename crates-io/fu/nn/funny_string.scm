(define-module (crates-io fu nn funny_string) #:use-module (crates-io))

(define-public crate-funny_string-0.1.0 (c (n "funny_string") (v "0.1.0") (h "10j9x3j42m7wx8qilki97xngbki9yjad21xdn0r36q3qvbijam86")))

(define-public crate-funny_string-0.1.1 (c (n "funny_string") (v "0.1.1") (h "0ihviq081mhzhrqfml0rkzws1synnklyiwdpvfrggnwprdcwk34r")))

