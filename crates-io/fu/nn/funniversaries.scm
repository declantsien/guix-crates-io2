(define-module (crates-io fu nn funniversaries) #:use-module (crates-io))

(define-public crate-funniversaries-0.0.1 (c (n "funniversaries") (v "0.0.1") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1c0bfw6nw4vvgs5pgk0a4wzjiw3dq0jsmrk1s534d8y2jvzqn6v6")))

(define-public crate-funniversaries-0.0.2 (c (n "funniversaries") (v "0.0.2") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "12xxvmmzypci6qinfiahnqdwqz3inh8p4iac421j1y8z7qc8m4l0") (y #t)))

(define-public crate-funniversaries-0.0.3 (c (n "funniversaries") (v "0.0.3") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "02hwaawccxsdzgkmr001l2vwsx7ja4fwqqzwjafmwqvggrz2d7df")))

(define-public crate-funniversaries-0.0.4 (c (n "funniversaries") (v "0.0.4") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0a6ch69l068lmqfsyw05m8kzlghrdnyg7f8qwm7yfkyx4xb49s79")))

(define-public crate-funniversaries-0.1.0 (c (n "funniversaries") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "17grwsalcp4jzn602c8pln12247xp0r0qwmbglqyw4xsmzg4dga2") (y #t)))

(define-public crate-funniversaries-0.1.1 (c (n "funniversaries") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1cg5gks6a0adp4rj2q2pm2v27a553vd77nmf505d39l6pj1irmjh")))

