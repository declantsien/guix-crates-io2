(define-module (crates-io fu nn funnel) #:use-module (crates-io))

(define-public crate-funnel-1.0.0 (c (n "funnel") (v "1.0.0") (h "1aw280dvr0bc3b3bp9z1dalz9ivy65wgc1zkklxwsc9ykmavsg18")))

(define-public crate-funnel-1.0.1 (c (n "funnel") (v "1.0.1") (h "1c8wqshvwjd6yqfknyff1q693cns18q9xmmchb80bas5g7jk0pfy")))

(define-public crate-funnel-1.1.0 (c (n "funnel") (v "1.1.0") (h "0g2icb32hk3i2q6fkgwp1yxqabd3yjbadpnrc6czlc5xj74sp607")))

