(define-module (crates-io fu tu future-local-storage) #:use-module (crates-io))

(define-public crate-future-local-storage-0.1.0-preview1 (c (n "future-local-storage") (v "0.1.0-preview1") (d (list (d (n "futures-util") (r "^0.3") (d #t) (k 2)) (d (n "pin-project") (r "^1.1.3") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "state") (r "^0.6.0") (f (quote ("tls"))) (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("macros" "rt" "rt-multi-thread"))) (d #t) (k 2)))) (h "086a34n2j8g61rbvn7sf0lmd7s5hh71s4z6x44x3361sk59z1qi2") (f (quote (("default"))))))

(define-public crate-future-local-storage-0.1.0 (c (n "future-local-storage") (v "0.1.0") (d (list (d (n "futures-util") (r "^0.3") (d #t) (k 2)) (d (n "pin-project") (r "^1.1.3") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "state") (r "^0.6.0") (f (quote ("tls"))) (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("macros" "rt" "rt-multi-thread"))) (d #t) (k 2)))) (h "1q298vdyzxcnv505nj60i3rl3j4axnvnbjzml4np2yni76dw2fs9") (f (quote (("default"))))))

(define-public crate-future-local-storage-0.1.1 (c (n "future-local-storage") (v "0.1.1") (d (list (d (n "futures-util") (r "^0.3") (d #t) (k 2)) (d (n "pin-project") (r "^1.1.3") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "state") (r "^0.6.0") (f (quote ("tls"))) (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("macros" "rt" "rt-multi-thread"))) (d #t) (k 2)))) (h "0m7x81cz9iz5nqj3zigz049mfdi1b90qwa5kyf64yrajp004mbwk") (f (quote (("default"))))))

