(define-module (crates-io fu tu futures-macros-lite) #:use-module (crates-io))

(define-public crate-futures-macros-lite-0.1.0 (c (n "futures-macros-lite") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits" "parsing"))) (d #t) (k 0)))) (h "1909sxrhil90dznamr4k57zn8lnnbhsvcfnq9pcqnm1b3h7js9p6")))

