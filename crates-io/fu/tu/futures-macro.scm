(define-module (crates-io fu tu futures-macro) #:use-module (crates-io))

(define-public crate-futures-macro-0.3.0 (c (n "futures-macro") (v "0.3.0") (d (list (d (n "proc-macro-hack") (r "^0.5.9") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1b6i8x0ckq1lb5sxyh39m666vfy3vb7x6rq67ij0fjylipw7xjg7")))

(define-public crate-futures-macro-0.3.1 (c (n "futures-macro") (v "0.3.1") (d (list (d (n "proc-macro-hack") (r "^0.5.9") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0r3px32wgqfbmfyb1lrj9wplvjris5magdyhfr7xnyjk2mncbrsj")))

(define-public crate-futures-macro-0.3.2 (c (n "futures-macro") (v "0.3.2") (d (list (d (n "proc-macro-hack") (r "^0.5.9") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "11ry56z0ilrnljj98akmn4bzmri12dpnflrj18aag73i9hhc9v7b")))

(define-public crate-futures-macro-0.3.3 (c (n "futures-macro") (v "0.3.3") (d (list (d (n "proc-macro-hack") (r "^0.5.9") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1d3iw08rf1fg86d732m60zzqpx19kp25ydci01708y2bj1r5pmx1")))

(define-public crate-futures-macro-0.3.4 (c (n "futures-macro") (v "0.3.4") (d (list (d (n "proc-macro-hack") (r "^0.5.9") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "19r3akc8y0br6jbx0lh1cfq07n8f23g7qfaag4m59xz17nm82l4s")))

(define-public crate-futures-macro-0.3.5 (c (n "futures-macro") (v "0.3.5") (d (list (d (n "proc-macro-hack") (r "^0.5.9") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0f8d4kmdxx0h7nca90zdpnbk429w6d3w8dw2hxrm9ar88c5a7dfh")))

(define-public crate-futures-macro-0.3.6 (c (n "futures-macro") (v "0.3.6") (d (list (d (n "proc-macro-hack") (r "^0.5.9") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "06sqcky1g0jkf75rdnw866snfxaxqvrjz7rfd1a2afv0li6x2zpm")))

(define-public crate-futures-macro-0.3.7 (c (n "futures-macro") (v "0.3.7") (d (list (d (n "proc-macro-hack") (r "^0.5.9") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1zlyallwjz4fx0kr8j7dgxfncfvwc9y04rfj2i53nmjqzkrwqvz3")))

(define-public crate-futures-macro-0.3.8 (c (n "futures-macro") (v "0.3.8") (d (list (d (n "proc-macro-hack") (r "^0.5.19") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0mjmb46zapb59iilsbljpj7l0hq6w19df0f03p3br5qz5xlqlh3p")))

(define-public crate-futures-macro-0.3.9 (c (n "futures-macro") (v "0.3.9") (d (list (d (n "proc-macro-hack") (r "^0.5.19") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0r1nl7xwpsbjsj87kbl5vak0qqf0y875gr7gycs5wg0z1v51k1qg")))

(define-public crate-futures-macro-0.3.10 (c (n "futures-macro") (v "0.3.10") (d (list (d (n "proc-macro-hack") (r "^0.5.19") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0hjf20wwpwcgbmzz30l21mzrj4p27qkm2bdrf7bqhkkh9vmy081m")))

(define-public crate-futures-macro-0.3.11 (c (n "futures-macro") (v "0.3.11") (d (list (d (n "proc-macro-hack") (r "^0.5.19") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0n4pv17krlxyz2l8c3q8bis7vaj0kc7mmzfqs2f1fg7fqch8fxwm")))

(define-public crate-futures-macro-0.3.12 (c (n "futures-macro") (v "0.3.12") (d (list (d (n "proc-macro-hack") (r "^0.5.19") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1pgyb219ybh3fj3ig0h1c2b7k4cqldy95ifdpnd9yb9jvmdd51y2")))

(define-public crate-futures-macro-0.3.13 (c (n "futures-macro") (v "0.3.13") (d (list (d (n "proc-macro-hack") (r "^0.5.19") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1dxzgz1sb0qin9nqsdrqc1aky52j3n9fnarcz2db77qkllb5hh7a")))

(define-public crate-futures-macro-0.3.14 (c (n "futures-macro") (v "0.3.14") (d (list (d (n "proc-macro-hack") (r "^0.5.19") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "12xvq5rsgi6jyynbxkrvbf1j620jy8xvmrqx9zmpvkc2l4rng336")))

(define-public crate-futures-macro-0.3.15 (c (n "futures-macro") (v "0.3.15") (d (list (d (n "autocfg") (r "^1") (d #t) (k 1)) (d (n "proc-macro-hack") (r "^0.5.19") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.56") (f (quote ("full"))) (d #t) (k 0)))) (h "08fixp6b34lr58cs85kacp3h599bi63njvfd03655pvc92c05i54")))

(define-public crate-futures-macro-0.3.16 (c (n "futures-macro") (v "0.3.16") (d (list (d (n "autocfg") (r "^1") (d #t) (k 1)) (d (n "proc-macro-hack") (r "^0.5.19") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.56") (f (quote ("full"))) (d #t) (k 0)))) (h "0mxcw0hjsx78zd0r1wz3928ycwmac81wjvywykvqvysnx6x16jf5")))

(define-public crate-futures-macro-0.3.17 (c (n "futures-macro") (v "0.3.17") (d (list (d (n "autocfg") (r "^1") (d #t) (k 1)) (d (n "proc-macro-hack") (r "^0.5.19") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.56") (f (quote ("full"))) (d #t) (k 0)))) (h "1fvriw31ippggp8496dpbvkykr2fgk57amhwrz5lqjzabjws9r0q")))

(define-public crate-futures-macro-0.3.18 (c (n "futures-macro") (v "0.3.18") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.56") (f (quote ("full"))) (d #t) (k 0)))) (h "1pbz9s2zhs9l8pfylxiq38zlmzggl34zk6n6fgblxg252sr1g7x8") (r "1.45")))

(define-public crate-futures-macro-0.3.19 (c (n "futures-macro") (v "0.3.19") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.56") (f (quote ("full"))) (d #t) (k 0)))) (h "0g5xp1xmyfibyscynig2m5gvp5smgg7xvcwr0p3yzc7zvxx99gbd") (r "1.45")))

(define-public crate-futures-macro-0.3.20 (c (n "futures-macro") (v "0.3.20") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.56") (f (quote ("full"))) (d #t) (k 0)))) (h "1lfkqbmsyxmjq0lv4brnshfz37l6jyw75x5d866yrp9n7wx0d1zg") (y #t) (r "1.45")))

(define-public crate-futures-macro-0.3.21 (c (n "futures-macro") (v "0.3.21") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.56") (f (quote ("full"))) (d #t) (k 0)))) (h "04pmj5xfk5rdhlj69wc7w3zvdg3xardg8srig96lszrk00wf3h9k") (r "1.45")))

(define-public crate-futures-macro-0.3.22 (c (n "futures-macro") (v "0.3.22") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.56") (f (quote ("full"))) (d #t) (k 0)))) (h "1wbgij43bd1wm8w1w4nc6xs7mhnjwc0qi4f7nqh83f1iax9qna60") (r "1.45")))

(define-public crate-futures-macro-0.3.23 (c (n "futures-macro") (v "0.3.23") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.56") (f (quote ("full"))) (d #t) (k 0)))) (h "0p84r80c3ys94l6f0iflysf6pd8ly53b4sknyb6f5smh6bjwrf8d") (r "1.45")))

(define-public crate-futures-macro-0.3.24 (c (n "futures-macro") (v "0.3.24") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.56") (f (quote ("full"))) (d #t) (k 0)))) (h "05rgwsq0bdbld7hl8r4dg4x4zmv0jzbbr23yvzdh8v25qz8ibka2") (r "1.45")))

(define-public crate-futures-macro-0.3.25 (c (n "futures-macro") (v "0.3.25") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.56") (f (quote ("full"))) (d #t) (k 0)))) (h "0pc3c5mydmwy50f0whcljcd41f0z1ci0r65dka8r2syqagh8ryxx") (r "1.45")))

(define-public crate-futures-macro-0.3.26 (c (n "futures-macro") (v "0.3.26") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.56") (f (quote ("full"))) (d #t) (k 0)))) (h "0w3ahy69varlfw57rb2pag7jwngy771vvzmcag7mlfx3gpw3m9wm") (r "1.45")))

(define-public crate-futures-macro-0.3.27 (c (n "futures-macro") (v "0.3.27") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.56") (f (quote ("full"))) (d #t) (k 0)))) (h "1dk3m7jpbj9qkxcwmqxnan1pli23hhcjqzwpp2wdh6v36zclxc9y") (r "1.45")))

(define-public crate-futures-macro-0.3.28 (c (n "futures-macro") (v "0.3.28") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.8") (f (quote ("full"))) (d #t) (k 0)))) (h "0wpfsqxwqk5k569xl0jzz4zxy85x695mndf7y9jn66q6jid59jl9") (r "1.56")))

(define-public crate-futures-macro-0.3.29 (c (n "futures-macro") (v "0.3.29") (d (list (d (n "proc-macro2") (r "^1.0.60") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.8") (f (quote ("full"))) (d #t) (k 0)))) (h "1nwd18i8kvpkdfwm045hddjli0n96zi7pn6f99zi9c74j7ym7cak") (r "1.56")))

(define-public crate-futures-macro-0.3.30 (c (n "futures-macro") (v "0.3.30") (d (list (d (n "proc-macro2") (r "^1.0.60") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.8") (f (quote ("full"))) (d #t) (k 0)))) (h "1b49qh9d402y8nka4q6wvvj0c88qq91wbr192mdn5h54nzs0qxc7") (r "1.56")))

