(define-module (crates-io fu tu futurize-derive) #:use-module (crates-io))

(define-public crate-futurize-derive-0.3.0 (c (n "futurize-derive") (v "0.3.0") (d (list (d (n "heck") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.12") (d #t) (k 0)) (d (n "quote") (r "^0.6.6") (d #t) (k 0)) (d (n "syn") (r "^0.14.8") (d #t) (k 0)))) (h "12aybr2yx0vs9i2fmdy9f02zcys6m24s8phh9jalhyvxcp36lmjs")))

(define-public crate-futurize-derive-0.4.0 (c (n "futurize-derive") (v "0.4.0") (d (list (d (n "heck") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.12") (d #t) (k 0)) (d (n "quote") (r "^0.6.6") (d #t) (k 0)) (d (n "syn") (r "^0.14.8") (d #t) (k 0)))) (h "1xy8zbg53dpcycc4c90p11fc52napbdlqdckm8v9cdiiqwvzqbwz")))

(define-public crate-futurize-derive-0.4.1 (c (n "futurize-derive") (v "0.4.1") (d (list (d (n "heck") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.12") (d #t) (k 0)) (d (n "quote") (r "^0.6.6") (d #t) (k 0)) (d (n "syn") (r "^0.14.8") (d #t) (k 0)))) (h "0fwpg19wn1n3l6w6lkn5n21444028686l3g775af8brzy67xdnfq")))

(define-public crate-futurize-derive-0.5.0 (c (n "futurize-derive") (v "0.5.0") (d (list (d (n "heck") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.12") (d #t) (k 0)) (d (n "quote") (r "^0.6.6") (d #t) (k 0)) (d (n "syn") (r "^0.14.8") (d #t) (k 0)))) (h "1drq8ihihpasr6yic3xvb77kdjq519ssv5mp499x9blpsdx9xa09")))

