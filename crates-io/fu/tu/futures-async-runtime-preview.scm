(define-module (crates-io fu tu futures-async-runtime-preview) #:use-module (crates-io))

(define-public crate-futures-async-runtime-preview-0.2.2 (c (n "futures-async-runtime-preview") (v "0.2.2") (d (list (d (n "futures-core-preview") (r "^0.2.2") (k 0)) (d (n "futures-stable-preview") (r "^0.2.2") (k 0)))) (h "0i6vqih135mh7cw72s7j4nmp9z70zh4rvkpvcb05qqs9fcjddsi0") (f (quote (("std" "futures-core-preview/std" "futures-stable-preview/std") ("nightly" "futures-core-preview/nightly" "futures-stable-preview/nightly") ("default" "std"))))))

(define-public crate-futures-async-runtime-preview-0.2.3 (c (n "futures-async-runtime-preview") (v "0.2.3") (d (list (d (n "futures-core-preview") (r "^0.2.2") (k 0)) (d (n "futures-stable-preview") (r "^0.2.2") (k 0)))) (h "0dyh1vr1ykv0a6p4jlny7w28c80zrd59iih5grxn5bhxpqsk1h1k") (f (quote (("std" "futures-core-preview/std" "futures-stable-preview/std") ("nightly" "futures-core-preview/nightly" "futures-stable-preview/nightly") ("default" "std"))))))

