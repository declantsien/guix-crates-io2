(define-module (crates-io fu tu future-utils) #:use-module (crates-io))

(define-public crate-future-utils-0.1.0 (c (n "future-utils") (v "0.1.0") (d (list (d (n "futures") (r "~0.1.16") (d #t) (k 0)) (d (n "log") (r "~0.3.8") (d #t) (k 0)) (d (n "unwrap") (r "^1.1") (d #t) (k 0)) (d (n "void") (r "^1") (d #t) (k 0)))) (h "18xfgcg47srh1n7yab8jdpmysidijjz286bh16947jk9l6m1yy8g")))

(define-public crate-future-utils-0.2.0 (c (n "future-utils") (v "0.2.0") (d (list (d (n "futures") (r "~0.1.15") (d #t) (k 0)) (d (n "log") (r "~0.3.8") (d #t) (k 0)) (d (n "unwrap") (r "^1.1") (d #t) (k 0)) (d (n "void") (r "^1") (d #t) (k 0)))) (h "01irki3lkmjw9y683ph73xw41gc7yr8qcm0pgh59d0lgvy6zccga")))

(define-public crate-future-utils-0.2.1 (c (n "future-utils") (v "0.2.1") (d (list (d (n "futures") (r "~0.1.15") (d #t) (k 0)) (d (n "log") (r "~0.3.8") (d #t) (k 0)) (d (n "unwrap") (r "^1.1") (d #t) (k 0)) (d (n "void") (r "^1") (d #t) (k 0)))) (h "1bj5npv3shn47sgv7yffn6xyv7dd16awh38f4r7hiakx4wgh2dii")))

(define-public crate-future-utils-0.3.0 (c (n "future-utils") (v "0.3.0") (d (list (d (n "futures") (r "~0.1.15") (d #t) (k 0)) (d (n "log") (r "~0.3.8") (d #t) (k 0)) (d (n "unwrap") (r "^1.1") (d #t) (k 0)) (d (n "void") (r "^1") (d #t) (k 0)))) (h "1kc2b43yaxligx32sbs3npjh4f5gkrkk1hxyzpfsl2bw6rayw58c")))

(define-public crate-future-utils-0.3.1 (c (n "future-utils") (v "0.3.1") (d (list (d (n "futures") (r "~0.1.15") (d #t) (k 0)) (d (n "log") (r "~0.3.8") (d #t) (k 0)) (d (n "unwrap") (r "^1.1") (d #t) (k 0)) (d (n "void") (r "^1") (d #t) (k 0)))) (h "16rkc162zchd1lmiyi1xgmq625ba3lsmkf6a3yj7n7znks6cfbz0")))

(define-public crate-future-utils-0.4.0 (c (n "future-utils") (v "0.4.0") (d (list (d (n "futures") (r "~0.1.15") (d #t) (k 0)) (d (n "log") (r "~0.3.8") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1.10") (d #t) (k 0)) (d (n "unwrap") (r "^1.1") (d #t) (k 0)) (d (n "void") (r "^1") (d #t) (k 0)))) (h "0mg65qj4jq9rzqy51hdgnqkki7rshz89nf3639vafgmf7006mk05")))

(define-public crate-future-utils-0.5.0 (c (n "future-utils") (v "0.5.0") (d (list (d (n "futures") (r "~0.1.15") (d #t) (k 0)) (d (n "log") (r "~0.3.8") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1.10") (d #t) (k 0)) (d (n "unwrap") (r "^1.1") (d #t) (k 0)) (d (n "void") (r "^1") (d #t) (k 0)))) (h "1671ylp38gvq3vqbjwidpdn63ka8nlmcikw0za2qk23ip5ymi9ps")))

(define-public crate-future-utils-0.6.0 (c (n "future-utils") (v "0.6.0") (d (list (d (n "futures") (r "~0.1.15") (d #t) (k 0)) (d (n "log") (r "~0.3.8") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1.10") (d #t) (k 0)) (d (n "unwrap") (r "^1.1") (d #t) (k 0)) (d (n "void") (r "^1") (d #t) (k 0)))) (h "0xbbjbnwswinkd93ck4y0dqabgyhbkha1n9gn3d45gxi8mpd0pfq")))

(define-public crate-future-utils-0.7.0 (c (n "future-utils") (v "0.7.0") (d (list (d (n "futures") (r "~0.1.15") (d #t) (k 0)) (d (n "log") (r "~0.3.8") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1.10") (d #t) (k 0)) (d (n "unwrap") (r "^1.1") (d #t) (k 0)) (d (n "void") (r "^1") (d #t) (k 0)))) (h "1s5x2wf61hrbj0w31bifqibbrrs2c986dg1hs556rz3crns76xnc")))

(define-public crate-future-utils-0.8.0 (c (n "future-utils") (v "0.8.0") (d (list (d (n "futures") (r "~0.1.15") (d #t) (k 0)) (d (n "log") (r "~0.3.8") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1.10") (d #t) (k 0)) (d (n "unwrap") (r "^1.1") (d #t) (k 0)) (d (n "void") (r "^1") (d #t) (k 0)))) (h "07g3wsrghlbqhrah58mcjap3iicbmmidprry7zwbqp9aa8hyrpqb")))

(define-public crate-future-utils-0.9.0 (c (n "future-utils") (v "0.9.0") (d (list (d (n "futures") (r "~0.1.15") (d #t) (k 0)) (d (n "log") (r "~0.3.8") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1.10") (d #t) (k 0)) (d (n "unwrap") (r "^1.1") (d #t) (k 0)) (d (n "void") (r "^1") (d #t) (k 0)))) (h "0a93jx4r1ay1lhflr99m00wmwksflgy81h2v8n27gvxrjhgw5y66")))

(define-public crate-future-utils-0.10.0 (c (n "future-utils") (v "0.10.0") (d (list (d (n "futures") (r "~0.1.15") (d #t) (k 0)) (d (n "log") (r "~0.3.8") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1.10") (d #t) (k 0)) (d (n "unwrap") (r "^1.1") (d #t) (k 0)) (d (n "void") (r "^1") (d #t) (k 0)))) (h "1x58r8vgkgb4662vj7sq14yzds9j57lh6qc9sxg1na5knzf2m040")))

(define-public crate-future-utils-0.11.0 (c (n "future-utils") (v "0.11.0") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "futures") (r "~0.1.15") (d #t) (k 0)) (d (n "log") (r "~0.3.8") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1.10") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)) (d (n "unwrap") (r "^1.1") (d #t) (k 0)) (d (n "void") (r "^1") (d #t) (k 0)))) (h "1w91cnnfilsz98yx70q58g2a2vwgn5d9hkb2qjr45aic7phdy9vw")))

(define-public crate-future-utils-0.12.0 (c (n "future-utils") (v "0.12.0") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "futures") (r "~0.1.15") (d #t) (k 0)) (d (n "log") (r "~0.3.8") (d #t) (k 0)) (d (n "tokio") (r "^0.1.7") (d #t) (k 0)) (d (n "unwrap") (r "^1.1") (d #t) (k 0)) (d (n "void") (r "^1") (d #t) (k 0)))) (h "166f01x2y12s24v71b1ncadny28mmzs0x163zda0xc64a0ai94sw")))

(define-public crate-future-utils-0.11.1 (c (n "future-utils") (v "0.11.1") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "futures") (r "~0.1.15") (d #t) (k 0)) (d (n "log") (r "~0.3.8") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1.10") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)) (d (n "unwrap") (r "^1.1") (d #t) (k 0)) (d (n "void") (r "^1") (d #t) (k 0)))) (h "0vf1mrhdrglfqj1qapwjglhmw2aq2mwkkikiskfnc197dx7rm8jm")))

(define-public crate-future-utils-0.12.1 (c (n "future-utils") (v "0.12.1") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "futures") (r "~0.1.15") (d #t) (k 0)) (d (n "log") (r "~0.3.8") (d #t) (k 0)) (d (n "tokio") (r "^0.1.7") (d #t) (k 0)) (d (n "unwrap") (r "^1.1") (d #t) (k 0)) (d (n "void") (r "^1") (d #t) (k 0)))) (h "1j4p7v806qcsl27c0grfh59khq9xzxkgkfdnaiflhr792cxzwhgs")))

