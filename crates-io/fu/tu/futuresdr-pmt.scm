(define-module (crates-io fu tu futuresdr-pmt) #:use-module (crates-io))

(define-public crate-futuresdr-pmt-0.0.2-experimental (c (n "futuresdr-pmt") (v "0.0.2-experimental") (d (list (d (n "flexbuffers") (r "^2.0.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.127") (f (quote ("derive"))) (d #t) (k 0)))) (h "0jing4snmg16mkcqgzb2y4nl7f87li3hw52mz40a870jc9rb09ac")))

(define-public crate-futuresdr-pmt-0.0.2 (c (n "futuresdr-pmt") (v "0.0.2") (d (list (d (n "flexbuffers") (r "^2.0.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.127") (f (quote ("derive"))) (d #t) (k 0)))) (h "1pg9s77xp7ij5wkkkr5rzwarc23ia5m8c24gsvcv217r4pi8vap1")))

(define-public crate-futuresdr-pmt-0.0.3 (c (n "futuresdr-pmt") (v "0.0.3") (d (list (d (n "flexbuffers") (r "^2.0.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "086xdzy5z9xdrd4n396zd4f9ibnwq4i1qbzh0q8vj18zn9mjs3jz")))

(define-public crate-futuresdr-pmt-0.0.5 (c (n "futuresdr-pmt") (v "0.0.5") (d (list (d (n "dyn-clone") (r "^1.0.9") (d #t) (k 0)) (d (n "flexbuffers") (r "^2.0.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0xm48fs57ccbc0dvvaq8vfwlwf8acdh5xccym990x1bp9d84qq8l")))

(define-public crate-futuresdr-pmt-0.0.6 (c (n "futuresdr-pmt") (v "0.0.6") (d (list (d (n "dyn-clone") (r "^1.0.9") (d #t) (k 0)) (d (n "flexbuffers") (r "^2.0.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1dnk0nzha852ck4nd06i9ji80yiyrx2a8qfzifrn2ccg0h4dd3dj")))

(define-public crate-futuresdr-pmt-0.0.8 (c (n "futuresdr-pmt") (v "0.0.8") (d (list (d (n "dyn-clone") (r "^1.0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "flexbuffers") (r "^2.0.0") (d #t) (k 2)))) (h "10aly0p2bxsb918yr8c39ksylcbrqnsqcniwk9la0kh10iq5qv9b")))

