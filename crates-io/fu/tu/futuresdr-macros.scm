(define-module (crates-io fu tu futuresdr-macros) #:use-module (crates-io))

(define-public crate-futuresdr-macros-0.0.1 (c (n "futuresdr-macros") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1.0.46") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full"))) (d #t) (k 0)))) (h "02sbg6yamyy4lqjr3rgyv6ys93x077qi4ksjn8pzpca6dsc5589d")))

(define-public crate-futuresdr-macros-0.0.2 (c (n "futuresdr-macros") (v "0.0.2") (d (list (d (n "proc-macro2") (r "^1.0.46") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0ph4bcb8zgs9kqgkvvvaxrdskg3jx2a1242d9z7awjc2jpp89x5a")))

(define-public crate-futuresdr-macros-0.0.3 (c (n "futuresdr-macros") (v "0.0.3") (d (list (d (n "proc-macro2") (r "^1.0.46") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1iaz2wx29s92pxscqy9p6lm7hks049x4sdq1jw6ylshisjv96x7g")))

(define-public crate-futuresdr-macros-0.0.4 (c (n "futuresdr-macros") (v "0.0.4") (d (list (d (n "proc-macro2") (r "^1.0.46") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "17lj77px8xykc8savm4yikm1211vw5zi04fhg75bcm3hrc0fn3b6")))

(define-public crate-futuresdr-macros-0.0.5 (c (n "futuresdr-macros") (v "0.0.5") (d (list (d (n "indexmap") (r "^1.9.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.46") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1gxw3n93cyd4ryzqfczpafrddvq1w6g6w12gmbcjhg20pd3n2qvq")))

