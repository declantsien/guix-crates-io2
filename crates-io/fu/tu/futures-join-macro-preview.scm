(define-module (crates-io fu tu futures-join-macro-preview) #:use-module (crates-io))

(define-public crate-futures-join-macro-preview-0.3.0-alpha.18 (c (n "futures-join-macro-preview") (v "0.3.0-alpha.18") (d (list (d (n "proc-macro-hack") (r "^0.5.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15.25") (f (quote ("full"))) (d #t) (k 0)))) (h "1najgspgsb0m01nx9i28gyjxvb9pq05hvkf59bz9q58jqh21w58v")))

(define-public crate-futures-join-macro-preview-0.3.0-alpha.19 (c (n "futures-join-macro-preview") (v "0.3.0-alpha.19") (d (list (d (n "proc-macro-hack") (r "^0.5.9") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1smwaja466yjh5adlhgggfk9k942sy4702n46scxkrwcnkk61qjr")))

