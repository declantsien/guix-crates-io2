(define-module (crates-io fu tu futures-core) #:use-module (crates-io))

(define-public crate-futures-core-0.2.0-alpha (c (n "futures-core") (v "0.2.0-alpha") (d (list (d (n "either") (r "^1.4") (o #t) (k 0)))) (h "1a2shx8msd50mhvax7sh6hiq99il8yydf2g91z8sndn07hylbp4f") (f (quote (("std" "either/use_std") ("default" "std"))))))

(define-public crate-futures-core-0.2.0-beta (c (n "futures-core") (v "0.2.0-beta") (d (list (d (n "either") (r "^1.4") (o #t) (k 0)) (d (n "pin-api") (r "^0.2.1") (k 0)))) (h "1k0iajdl89swjms0ywqhy9nwl8r5fzv5f02z2svwnnl93hdkra4h") (f (quote (("std" "either/use_std" "pin-api/std") ("nightly" "pin-api/nightly") ("default" "std"))))))

(define-public crate-futures-core-0.2.0 (c (n "futures-core") (v "0.2.0") (d (list (d (n "either") (r "^1.4") (o #t) (k 0)))) (h "15sb93m3wraq459jfvnbzbabc2jnbbzmr8wlsbzjdm8i8jc1j7wc") (f (quote (("std" "either/use_std") ("nightly") ("default" "std"))))))

(define-public crate-futures-core-0.2.1 (c (n "futures-core") (v "0.2.1") (d (list (d (n "either") (r "^1.4") (o #t) (k 0)))) (h "09g2cdf08m71a3axkvrkycd7cxbcfsz7xxqkn0rqzqzan8gcjmbl") (f (quote (("std" "either/use_std") ("nightly") ("default" "std"))))))

(define-public crate-futures-core-0.3.0 (c (n "futures-core") (v "0.3.0") (h "1wii72x9vp2bch78c7bz160as5bcll2lp7v8a9fyvmrmy1wapw1h") (f (quote (("unstable") ("std" "alloc") ("default" "std") ("cfg-target-has-atomic") ("alloc"))))))

(define-public crate-futures-core-0.3.1 (c (n "futures-core") (v "0.3.1") (h "0rh8q6pg08dizk5hwksvjgvkw26s3sr3b199nggv3ypyg914qmkr") (f (quote (("unstable") ("std" "alloc") ("default" "std") ("cfg-target-has-atomic") ("alloc"))))))

(define-public crate-futures-core-0.3.2 (c (n "futures-core") (v "0.3.2") (h "07p9ax825spd09fgcdz7dqvbjkdcn38l1kvnffk41s891cdk1yyc") (f (quote (("unstable") ("std" "alloc") ("default" "std") ("cfg-target-has-atomic") ("alloc"))))))

(define-public crate-futures-core-0.3.3 (c (n "futures-core") (v "0.3.3") (h "17b0hff4xzcy5nz419h1asrlclypav3afa2ivvgy5751l5mb35xm") (f (quote (("unstable") ("std" "alloc") ("default" "std") ("cfg-target-has-atomic") ("alloc"))))))

(define-public crate-futures-core-0.3.4 (c (n "futures-core") (v "0.3.4") (h "0jj6mjvjbvv36r6w86lmjkh62mwndz901nsq4fwqjpl2d7vr4mgj") (f (quote (("unstable") ("std" "alloc") ("default" "std") ("cfg-target-has-atomic") ("alloc"))))))

(define-public crate-futures-core-0.3.5 (c (n "futures-core") (v "0.3.5") (h "16d35ic4b4dkv1gy39cvy2v4391b9014hrxd6vwp3nfm1zwzzxar") (f (quote (("unstable") ("std" "alloc") ("default" "std") ("cfg-target-has-atomic") ("alloc"))))))

(define-public crate-futures-core-0.3.6 (c (n "futures-core") (v "0.3.6") (h "0jvqwbhz5qkr1lbfh8b6gdmf9lpajyzhv40rlnnxb5k80nhflx6n") (f (quote (("unstable") ("std" "alloc") ("default" "std") ("cfg-target-has-atomic") ("alloc"))))))

(define-public crate-futures-core-0.3.7 (c (n "futures-core") (v "0.3.7") (h "0ijf1nhhwigjxl69gjh8yp3v9qrwzxn04fgai0nfsjwq09hsbshq") (f (quote (("unstable") ("std" "alloc") ("default" "std") ("cfg-target-has-atomic") ("alloc"))))))

(define-public crate-futures-core-0.3.8 (c (n "futures-core") (v "0.3.8") (h "0j0pixxv8dmqas1h5cgy92z4r9lpmnlis8ls22v17yrgnwqy2z44") (f (quote (("unstable") ("std" "alloc") ("default" "std") ("cfg-target-has-atomic") ("alloc"))))))

(define-public crate-futures-core-0.3.9 (c (n "futures-core") (v "0.3.9") (h "022yjh7fradc7ffmcmqadds4mb4gh413q4vkc6ls4qzz2w4kp3fv") (f (quote (("unstable") ("std" "alloc") ("default" "std") ("cfg-target-has-atomic") ("alloc"))))))

(define-public crate-futures-core-0.3.10 (c (n "futures-core") (v "0.3.10") (h "0gfdlnf7mmgpi892dlymhf4djm1hym6csm3gpsx3w95bnvifm2pd") (f (quote (("unstable") ("std" "alloc") ("default" "std") ("cfg-target-has-atomic") ("alloc"))))))

(define-public crate-futures-core-0.3.11 (c (n "futures-core") (v "0.3.11") (h "0491sph93c5bg31367lhvpzxzj2gzvzkhqsl2rlj3lr4y2xd16q8") (f (quote (("unstable") ("std" "alloc") ("default" "std") ("cfg-target-has-atomic") ("alloc"))))))

(define-public crate-futures-core-0.3.12 (c (n "futures-core") (v "0.3.12") (h "0r8ag0mkxx9cd74yrccjk31lph4gr6lhgb9di6rx39wdvrfi9rbr") (f (quote (("unstable") ("std" "alloc") ("default" "std") ("cfg-target-has-atomic") ("alloc"))))))

(define-public crate-futures-core-0.3.13 (c (n "futures-core") (v "0.3.13") (h "151c26lzgmk3y67bl407vlhr4hl76ydaa4fzqfyn43mzz9r6lj8m") (f (quote (("unstable") ("std" "alloc") ("default" "std") ("cfg-target-has-atomic") ("alloc"))))))

(define-public crate-futures-core-0.3.14 (c (n "futures-core") (v "0.3.14") (h "058qmyvpvhzkw9aykm6ls20k3dsy4jaafdqs1xjh3jm6vp3d3309") (f (quote (("unstable") ("std" "alloc") ("default" "std") ("cfg-target-has-atomic") ("alloc"))))))

(define-public crate-futures-core-0.3.15 (c (n "futures-core") (v "0.3.15") (h "1lbrw5087ndaqvbjrckbz9ld4ya67k72d6w87c22d6m8v1jzf0h4") (f (quote (("unstable") ("std" "alloc") ("default" "std") ("cfg-target-has-atomic") ("alloc"))))))

(define-public crate-futures-core-0.3.16 (c (n "futures-core") (v "0.3.16") (h "168zy072rgzkvi3hsbjzhn8ynwy60a4dwffv0cvh7zzxlysb2ldg") (f (quote (("unstable") ("std" "alloc") ("default" "std") ("cfg-target-has-atomic") ("alloc"))))))

(define-public crate-futures-core-0.3.17 (c (n "futures-core") (v "0.3.17") (h "0gd5hfxcy5fqc3wm55a4yhcbh921wvzv09nkbhwh6dpjaxlw5lc8") (f (quote (("unstable") ("std" "alloc") ("default" "std") ("cfg-target-has-atomic") ("alloc"))))))

(define-public crate-futures-core-0.3.18 (c (n "futures-core") (v "0.3.18") (h "0id4niawvf6vxv2s7w782jmm6m5arrcx2isvlsws1hp75zj1d4v2") (f (quote (("unstable") ("std" "alloc") ("default" "std") ("cfg-target-has-atomic") ("alloc")))) (y #t) (r "1.36")))

(define-public crate-futures-core-0.3.19 (c (n "futures-core") (v "0.3.19") (h "1mw34nxzggvr2jvk4ljygy077wy32lrdxkyw1j0mj9dqc42gzj6h") (f (quote (("unstable") ("std" "alloc") ("default" "std") ("cfg-target-has-atomic") ("alloc")))) (r "1.36")))

(define-public crate-futures-core-0.3.20 (c (n "futures-core") (v "0.3.20") (h "0gpjxsb44i4mrg9xlx62bdhsnri6ngj9pkrjv77i9gz4jm710mzw") (f (quote (("unstable") ("std" "alloc") ("default" "std") ("cfg-target-has-atomic") ("alloc")))) (y #t) (r "1.36")))

(define-public crate-futures-core-0.3.21 (c (n "futures-core") (v "0.3.21") (h "1lqhc6mqklh5bmkpr77p42lqwjj8gaskk5ba2p3kl1z4nw2gs28c") (f (quote (("unstable") ("std" "alloc") ("default" "std") ("cfg-target-has-atomic") ("alloc")))) (r "1.36")))

(define-public crate-futures-core-0.3.22 (c (n "futures-core") (v "0.3.22") (h "09pj8s3s143knbn2azngwl0fpcvig93zlsfi1vffwgsd3rwnyz59") (f (quote (("unstable") ("std" "alloc") ("default" "std") ("cfg-target-has-atomic") ("alloc")))) (r "1.36")))

(define-public crate-futures-core-0.3.23 (c (n "futures-core") (v "0.3.23") (h "058ii8sbxmf20cy5wvqkd0mppvgw21mlf8irdj9kb0nki2pfvb6j") (f (quote (("unstable") ("std" "alloc") ("default" "std") ("cfg-target-has-atomic") ("alloc")))) (r "1.36")))

(define-public crate-futures-core-0.3.24 (c (n "futures-core") (v "0.3.24") (h "1gsnrvmzddzfvywx9jnc75vxwp78jri1wlz6inw3yb1n0pga6njf") (f (quote (("unstable") ("std" "alloc") ("default" "std") ("cfg-target-has-atomic") ("alloc")))) (r "1.36")))

(define-public crate-futures-core-0.3.25 (c (n "futures-core") (v "0.3.25") (h "1b6k9fd6bkga9556jyx78di278bdp2p81cls99nawcs6grx9m404") (f (quote (("unstable") ("std" "alloc") ("default" "std") ("cfg-target-has-atomic") ("alloc")))) (r "1.36")))

(define-public crate-futures-core-0.3.26 (c (n "futures-core") (v "0.3.26") (d (list (d (n "portable-atomic") (r "^1") (o #t) (k 0)))) (h "02467z5mv0219hkrgmpvsb3h7vb8pg31s1j901h7vxg11x6zz47c") (f (quote (("unstable") ("std" "alloc") ("default" "std") ("cfg-target-has-atomic") ("alloc")))) (r "1.36")))

(define-public crate-futures-core-0.3.27 (c (n "futures-core") (v "0.3.27") (d (list (d (n "portable-atomic") (r "^1") (o #t) (k 0)))) (h "1pb9vsvh6xnv0xpzj73g6wl1357nzacyxq0x96n3sdknmb0s1mw6") (f (quote (("unstable") ("std" "alloc") ("default" "std") ("cfg-target-has-atomic") ("alloc")))) (r "1.36")))

(define-public crate-futures-core-0.3.28 (c (n "futures-core") (v "0.3.28") (d (list (d (n "portable-atomic") (r "^1") (o #t) (k 0)))) (h "137fdxy5amg9zkpa1kqnj7bnha6b94fmddz59w973x96gqxmijjb") (f (quote (("unstable") ("std" "alloc") ("default" "std") ("cfg-target-has-atomic") ("alloc")))) (r "1.36")))

(define-public crate-futures-core-0.3.29 (c (n "futures-core") (v "0.3.29") (d (list (d (n "portable-atomic") (r "^1.3") (f (quote ("require-cas"))) (o #t) (k 0)))) (h "1308bpj0g36nhx2y6bl4mm6f1gnh9xyvvw2q2wpdgnb6dv3247gb") (f (quote (("unstable") ("std" "alloc") ("default" "std") ("cfg-target-has-atomic") ("alloc")))) (r "1.36")))

(define-public crate-futures-core-0.3.30 (c (n "futures-core") (v "0.3.30") (d (list (d (n "portable-atomic") (r "^1.3") (f (quote ("require-cas"))) (o #t) (k 0)))) (h "07aslayrn3lbggj54kci0ishmd1pr367fp7iks7adia1p05miinz") (f (quote (("unstable") ("std" "alloc") ("default" "std") ("cfg-target-has-atomic") ("alloc")))) (r "1.36")))

