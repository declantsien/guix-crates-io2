(define-module (crates-io fu tu futuristic) #:use-module (crates-io))

(define-public crate-futuristic-0.1.0 (c (n "futuristic") (v "0.1.0") (d (list (d (n "either") (r "^1.5.0") (d #t) (k 0)) (d (n "futures") (r "^0.1.21") (d #t) (k 0)))) (h "0yzzyh1a8w8q8fxwq0cqbxl52yms42ch4cg8vryjr02ipg6rh3qz")))

(define-public crate-futuristic-0.2.0 (c (n "futuristic") (v "0.2.0") (d (list (d (n "either") (r "^1.5.0") (d #t) (k 0)) (d (n "futures") (r "^0.1.25") (d #t) (k 0)))) (h "0iwadghcnr8x2s3ql87m2qp0ikx5hv3ib8ichr7qgzr43714r8zr")))

(define-public crate-futuristic-0.2.1 (c (n "futuristic") (v "0.2.1") (d (list (d (n "either") (r "^1.5.0") (d #t) (k 0)) (d (n "futures") (r "^0.1.25") (d #t) (k 0)))) (h "1y62z69kmqkn8izhvd2hf9hnb6m2p8syk389cpq4k21m83whjrvb")))

(define-public crate-futuristic-0.3.0-alpha.0 (c (n "futuristic") (v "0.3.0-alpha.0") (d (list (d (n "either") (r "^1.5.0") (d #t) (k 0)) (d (n "futures-preview") (r "^0.3.0-alpha") (d #t) (k 0)))) (h "175r30dy2x7vx6flc3zgx3bs5ryfwl7zm30fr697a89kryx4ppk7")))

(define-public crate-futuristic-0.3.0 (c (n "futuristic") (v "0.3.0") (d (list (d (n "either") (r "^1.5.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)))) (h "0p1cg51fgpsq05y0cdkkivivbavqflafn38hz4vaqd2xk6yx43sq")))

(define-public crate-futuristic-0.4.0 (c (n "futuristic") (v "0.4.0") (d (list (d (n "either") (r "^1.6.1") (d #t) (k 0)) (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "pin-project") (r "^1.0.10") (d #t) (k 0)))) (h "1xsgsibpjghyfmwmbbzl6r2cwg7zfldvhvcmlji9nx0v81zigw9m")))

(define-public crate-futuristic-0.5.0 (c (n "futuristic") (v "0.5.0") (d (list (d (n "either") (r "^1.8.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.25") (d #t) (k 0)) (d (n "pin-project") (r "^1.0.12") (d #t) (k 0)))) (h "1wdbijq9afcrzk8c5i9zrks1f0y41m7yii1ipq61rqa9vy77n2ax")))

