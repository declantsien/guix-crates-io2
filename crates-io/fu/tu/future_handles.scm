(define-module (crates-io fu tu future_handles) #:use-module (crates-io))

(define-public crate-future_handles-0.1.0 (c (n "future_handles") (v "0.1.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "futures") (r "^0.3") (f (quote ("executor" "thread-pool"))) (d #t) (k 2)) (d (n "spinning_top") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1bb8d325f221gfqby5jxigcsadw4hajn25k1g27snxpkmih4bp3i") (f (quote (("sync" "spinning_top"))))))

(define-public crate-future_handles-0.1.1 (c (n "future_handles") (v "0.1.1") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "futures") (r "^0.3") (f (quote ("executor" "thread-pool"))) (d #t) (k 2)) (d (n "spinning_top") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0lds0jrfs5hqmw9zmf6iim521yimyzcxpdwzjrfy6ihmapdl1dsk") (f (quote (("sync" "spinning_top"))))))

(define-public crate-future_handles-0.2.0 (c (n "future_handles") (v "0.2.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "futures") (r "^0.3") (f (quote ("executor" "thread-pool"))) (d #t) (k 2)) (d (n "spinning_top") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "15n9d2qy1iqgp4p8qyp0fp2bai3mb5ck1jny1p1cgjdzl6py6hfm") (f (quote (("sync" "spinning_top"))))))

