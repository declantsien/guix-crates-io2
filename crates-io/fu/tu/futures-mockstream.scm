(define-module (crates-io fu tu futures-mockstream) #:use-module (crates-io))

(define-public crate-futures-mockstream-0.1.0 (c (n "futures-mockstream") (v "0.1.0") (d (list (d (n "futures-core") (r "^0.3.4") (d #t) (k 0)) (d (n "futures-io") (r "^0.3.4") (d #t) (k 0)) (d (n "futures-task") (r "^0.3.4") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.4") (d #t) (k 2)) (d (n "smol") (r "^0.1.4") (d #t) (k 2)))) (h "0qk5yqly1zbhl60m2h0lzwi14llijsb1c3ny2yfdv5pc0knj992y")))

(define-public crate-futures-mockstream-0.1.1 (c (n "futures-mockstream") (v "0.1.1") (d (list (d (n "futures-core") (r "^0.3.4") (d #t) (k 0)) (d (n "futures-io") (r "^0.3.4") (d #t) (k 0)) (d (n "futures-task") (r "^0.3.4") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.4") (d #t) (k 2)) (d (n "smol") (r "^0.1.4") (d #t) (k 2)))) (h "1y63jaxgpagzn3zl3wli3v631xr684acc83dw98z6h2i2gxzs3nd")))

(define-public crate-futures-mockstream-0.1.2 (c (n "futures-mockstream") (v "0.1.2") (d (list (d (n "futures-core") (r "^0.3.4") (d #t) (k 0)) (d (n "futures-io") (r "^0.3.4") (d #t) (k 0)) (d (n "futures-task") (r "^0.3.4") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.4") (d #t) (k 2)) (d (n "smol") (r "^0.1.4") (d #t) (k 2)))) (h "0h97h9k7mnhy0f6a8pdig2njlcfvwpms8qlgncvn2p317x209vy8")))

