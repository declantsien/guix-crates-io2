(define-module (crates-io fu tu futures-await-async-macro) #:use-module (crates-io))

(define-public crate-futures-await-async-macro-0.1.0 (c (n "futures-await-async-macro") (v "0.1.0") (d (list (d (n "futures-await-quote") (r "^0.4") (d #t) (k 0)) (d (n "futures-await-syn") (r "^0.12") (f (quote ("full" "fold" "parsing" "printing"))) (k 0)) (d (n "futures-await-synom") (r "^0.12") (k 0)) (d (n "proc-macro2") (r "^0.1") (f (quote ("unstable"))) (d #t) (k 0)))) (h "0f9549k5kqzx7r93nndp0xcdcqmc4xcj59mirifh5y993n6c5k72")))

(define-public crate-futures-await-async-macro-0.1.1 (c (n "futures-await-async-macro") (v "0.1.1") (d (list (d (n "futures-await-quote") (r "^0.4") (d #t) (k 0)) (d (n "futures-await-syn") (r "^0.12") (f (quote ("full" "fold" "parsing" "printing"))) (k 0)) (d (n "futures-await-synom") (r "^0.12") (k 0)) (d (n "proc-macro2") (r "^0.1") (f (quote ("unstable"))) (d #t) (k 0)))) (h "1df231fk4d2pmyz5zzp3jjvzzkydbdk8zganlvw17car0iawjs18")))

(define-public crate-futures-await-async-macro-0.2.0-alpha (c (n "futures-await-async-macro") (v "0.2.0-alpha") (d (list (d (n "proc-macro2") (r "^0.2") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.4") (d #t) (k 0)) (d (n "syn") (r "^0.12.13") (f (quote ("full" "fold" "parsing" "printing" "extra-traits"))) (k 0)))) (h "0016k4p56c5fivqhx31fc8b79f4fqm9fb1m045h6jd9j9rfgb5k3")))

(define-public crate-futures-await-async-macro-0.1.2 (c (n "futures-await-async-macro") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^0.3") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.5") (d #t) (k 0)) (d (n "syn") (r "^0.13.1") (f (quote ("full" "fold" "parsing" "printing" "proc-macro"))) (k 0)))) (h "1c0riwavc9clawgbsirhl67dzayffxcy8si4i5n02g3fb0lvhzxq")))

(define-public crate-futures-await-async-macro-0.1.3 (c (n "futures-await-async-macro") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^0.3") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.5") (d #t) (k 0)) (d (n "syn") (r "^0.13.1") (f (quote ("full" "fold" "parsing" "printing" "proc-macro"))) (k 0)))) (h "1wnllhdf7vbzrrsbkv0lipwp7iimmhfdznng053nx1jf6kgb2n4r")))

(define-public crate-futures-await-async-macro-0.1.4 (c (n "futures-await-async-macro") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^0.4") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.14") (f (quote ("full" "fold" "parsing" "printing" "proc-macro"))) (k 0)))) (h "116zknsivr0a07gq5ckfzcsfaalbcxl81nxxvhdg7lazdah5glpx")))

