(define-module (crates-io fu tu futures-shuttle) #:use-module (crates-io))

(define-public crate-futures-shuttle-0.1.0 (c (n "futures-shuttle") (v "0.1.0") (d (list (d (n "futures") (r "^0.2.0-alpha") (d #t) (k 2)) (d (n "futures-core") (r "^0.2.0-alpha") (d #t) (k 0)) (d (n "parking_lot") (r "^0.5") (d #t) (k 0)))) (h "07c5ggg73pxxymj4102ibdxf8n8ww62sicy52d57p5xzj77jnxb3")))

(define-public crate-futures-shuttle-0.1.1 (c (n "futures-shuttle") (v "0.1.1") (d (list (d (n "futures") (r "^0.2.0-alpha") (d #t) (k 2)) (d (n "futures-core") (r "^0.2.0-alpha") (d #t) (k 0)) (d (n "parking_lot") (r "^0.5") (d #t) (k 0)))) (h "1wc7fpd4spjp4nvgyqhwpk0xqif4hfd17yvjgh6g8kwihj7gpqw2")))

(define-public crate-futures-shuttle-0.1.2 (c (n "futures-shuttle") (v "0.1.2") (d (list (d (n "futures") (r "^0.2.0-beta") (d #t) (k 2)) (d (n "futures-core") (r "^0.2.0-beta") (d #t) (k 0)) (d (n "parking_lot") (r "^0.5") (d #t) (k 0)))) (h "0mvsdzd1hvy8f7dh12rrp9k0v9nv7ly85yr5fi8fklf4g5ylhm8j")))

(define-public crate-futures-shuttle-0.2.0 (c (n "futures-shuttle") (v "0.2.0") (d (list (d (n "futures") (r "^0.2.0-beta") (d #t) (k 2)) (d (n "futures-core") (r "^0.2.0-beta") (d #t) (k 0)) (d (n "parking_lot") (r "^0.5") (d #t) (k 0)))) (h "1sqh5wgjr7pp5ydizs8dx9jx0paanh3wg0grbzps0fpw3w3lgbxy") (f (quote (("pedantic"))))))

(define-public crate-futures-shuttle-0.2.1 (c (n "futures-shuttle") (v "0.2.1") (d (list (d (n "futures") (r "^0.2.0") (d #t) (k 2)) (d (n "futures-core") (r "^0.2.0-beta") (d #t) (k 0)) (d (n "parking_lot") (r "^0.5") (d #t) (k 0)))) (h "17qrqj0fmxx66r1n058w3swgsc2sn7yh90vgdz145d6q6dxnjz3d") (f (quote (("pedantic"))))))

