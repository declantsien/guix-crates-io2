(define-module (crates-io fu tu futures_future) #:use-module (crates-io))

(define-public crate-futures_future-0.1.0 (c (n "futures_future") (v "0.1.0") (d (list (d (n "futures") (r "^0.1.24") (d #t) (k 0)))) (h "1n2qzdfjn17mzjk0hgbyvs9vmdwpnzmm6ny04fal8zpj1sdmv1pb")))

(define-public crate-futures_future-0.1.1 (c (n "futures_future") (v "0.1.1") (d (list (d (n "futures") (r "^0.1.24") (d #t) (k 0)))) (h "1kbnbzq87x33z85q7r5gyyyyx6dp0xklxqgymx2b03mihb4afnd5")))

