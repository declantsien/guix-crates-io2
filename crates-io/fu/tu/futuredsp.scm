(define-module (crates-io fu tu futuredsp) #:use-module (crates-io))

(define-public crate-futuredsp-0.0.1 (c (n "futuredsp") (v "0.0.1") (d (list (d (n "criterion") (r "^0.3.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "num-complex") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "rustc_version") (r "^0.4.0") (d #t) (k 1)))) (h "17v318m01rizwif14ihkb8577nhai2v9ifw0ixv1mawhv3w64kyx")))

(define-public crate-futuredsp-0.0.2 (c (n "futuredsp") (v "0.0.2") (d (list (d (n "criterion") (r "^0.3.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "num-complex") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "rustc_version") (r "^0.4.0") (d #t) (k 1)))) (h "0prnr0ag5k2kqqmfzcv48v6hhnlw7fnfc38pyd38s6g61vln9n7z")))

(define-public crate-futuredsp-0.0.3 (c (n "futuredsp") (v "0.0.3") (d (list (d (n "criterion") (r "^0.3.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-complex") (r "^0.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "rustc_version") (r "^0.4.0") (d #t) (k 1)))) (h "09a68q6h7jq1g1dmxwny3xlrylwj9nk6ylfpryh065n5zgx8z9ab")))

(define-public crate-futuredsp-0.0.4 (c (n "futuredsp") (v "0.0.4") (d (list (d (n "criterion") (r "^0.3.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-complex") (r "^0.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "rustc_version") (r "^0.4.0") (d #t) (k 1)))) (h "1i0qp8ra7rb93wikj1arwf6i1j55y95s5px60y7fkp9fr9k54l1h")))

(define-public crate-futuredsp-0.0.6 (c (n "futuredsp") (v "0.0.6") (d (list (d (n "criterion") (r "^0.3.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-complex") (r "^0.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "rustc_version") (r "^0.4.0") (d #t) (k 1)))) (h "12ciljkvxdpz028ypghxl4z8a7l37hvx0mpkrylxhq9j0gmp4xhg")))

