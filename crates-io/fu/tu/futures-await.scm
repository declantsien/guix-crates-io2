(define-module (crates-io fu tu futures-await) #:use-module (crates-io))

(define-public crate-futures-await-0.1.0 (c (n "futures-await") (v "0.1.0") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "futures-await-async-macro") (r "^0.1") (d #t) (k 0)) (d (n "futures-await-await-macro") (r "^0.1") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 2)) (d (n "tokio-io") (r "^0.1") (d #t) (k 2)))) (h "0z25ylxsw2d5gk8zm3h1sr37x1zzhnirzb97fyyh538n1z5ilhw4")))

(define-public crate-futures-await-0.2.0-alpha (c (n "futures-await") (v "0.2.0-alpha") (d (list (d (n "futures") (r "^0.2.0-alpha") (d #t) (k 0)) (d (n "futures-await-async-macro") (r "^0.2.0-alpha") (d #t) (k 0)) (d (n "futures-await-await-macro") (r "^0.2.0-alpha") (d #t) (k 0)) (d (n "futures-stable") (r "^0.2.0-alpha") (d #t) (k 0)) (d (n "pin-api") (r "^0.1.1") (d #t) (k 0)))) (h "07samg6834507hflzdzmaaa8447fyghckxnnwfy9g56fnpv9zgl9")))

(define-public crate-futures-await-0.1.1 (c (n "futures-await") (v "0.1.1") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "futures-await-async-macro") (r "^0.1") (d #t) (k 0)) (d (n "futures-await-await-macro") (r "^0.1") (d #t) (k 0)) (d (n "futures-cpupool") (r "^0.1") (d #t) (k 2)) (d (n "tokio-core") (r "^0.1") (d #t) (k 2)) (d (n "tokio-io") (r "^0.1") (d #t) (k 2)))) (h "0qvlcp3z1zf4758ix9yrl8wgi5xhd7kj642j7sfdrajhqbq74sfp")))

