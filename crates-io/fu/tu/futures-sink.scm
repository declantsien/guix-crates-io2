(define-module (crates-io fu tu futures-sink) #:use-module (crates-io))

(define-public crate-futures-sink-0.2.0-alpha (c (n "futures-sink") (v "0.2.0-alpha") (d (list (d (n "futures-channel") (r "^0.2.0-alpha") (k 0)) (d (n "futures-core") (r "^0.2.0-alpha") (k 0)))) (h "0i0y31kgdgkip8jhk8gpc0d6m7lq26xwsgvakb789prdiwkijxd3") (f (quote (("std" "futures-core/std" "futures-channel/std") ("default" "std"))))))

(define-public crate-futures-sink-0.2.0-beta (c (n "futures-sink") (v "0.2.0-beta") (d (list (d (n "futures-channel") (r "^0.2.0-beta") (k 0)) (d (n "futures-core") (r "^0.2.0-beta") (k 0)))) (h "0640v5mz2s2jqdp3zl7bvfshwq7892b83h5qik1w9wh308km96f9") (f (quote (("std" "futures-core/std" "futures-channel/std") ("default" "std"))))))

(define-public crate-futures-sink-0.2.0 (c (n "futures-sink") (v "0.2.0") (d (list (d (n "futures-channel") (r "^0.2.0") (k 0)) (d (n "futures-core") (r "^0.2.0") (k 0)))) (h "012wsx7a3nx7qz38kvrjb9x7hyln5zwqwgsqqpfarbbcxa9lgxh6") (f (quote (("std" "futures-core/std" "futures-channel/std") ("default" "std"))))))

(define-public crate-futures-sink-0.2.1 (c (n "futures-sink") (v "0.2.1") (d (list (d (n "either") (r "^1.4") (o #t) (k 0)) (d (n "futures-channel") (r "^0.2.1") (k 0)) (d (n "futures-core") (r "^0.2.1") (k 0)))) (h "04k4q4s8ica9nyh2aspaflsl5j5rgqp04w6x5khbhxh891y3mad8") (f (quote (("std" "either/use_std" "futures-core/std" "futures-channel/std") ("default" "std"))))))

(define-public crate-futures-sink-0.3.0 (c (n "futures-sink") (v "0.3.0") (h "1pi13a76g3ihm3r4jn2cwsqk87x062165k9383wq2gmg7n5v2v9l") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-futures-sink-0.3.1 (c (n "futures-sink") (v "0.3.1") (h "05iwskzxq3yqvxv9l1kqnd7kkmp0dwc39fnvwrcjsg76z8zf66qp") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-futures-sink-0.3.2 (c (n "futures-sink") (v "0.3.2") (h "191nxardrpzfh2673xchprxqn4rraf00wl4g6mlkd2iyfsfjg184") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-futures-sink-0.3.3 (c (n "futures-sink") (v "0.3.3") (h "0v56fn50p7cyaknkwhq0cw5zrxa265q7dcz1ydb7rsj76l99wa84") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-futures-sink-0.3.4 (c (n "futures-sink") (v "0.3.4") (h "19n88jlh8cwam4gqzj4vj9g130vg9ir0m1bv11dxj5619cdq4ril") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-futures-sink-0.3.5 (c (n "futures-sink") (v "0.3.5") (h "1k4azaq6x7g9kmpvc91vx5w541y4p340rkl5bnhcfd5p7j4k481z") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-futures-sink-0.3.6 (c (n "futures-sink") (v "0.3.6") (h "1gdbi002xczcxhh5v716234cmdaxm63cz1fikrfvqknniqjn91qd") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-futures-sink-0.3.7 (c (n "futures-sink") (v "0.3.7") (h "048b183c63vjf8cb715ckpl6yfibbg9sgxrxbnp0923fgpqs6g0f") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-futures-sink-0.3.8 (c (n "futures-sink") (v "0.3.8") (h "0gfb1z97q861ki6lqsvpgfn3hnm9w3vkrf82dc00xrff95d1jy7q") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-futures-sink-0.3.9 (c (n "futures-sink") (v "0.3.9") (h "1sqzny97b66xw7rq28ncy71x4ji4nqpijycz13x9q44h2anapbgn") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-futures-sink-0.3.10 (c (n "futures-sink") (v "0.3.10") (h "1pp5vg5xc5g5c0ga961vmds1ch54sx00w567r335729ng621hbf7") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-futures-sink-0.3.11 (c (n "futures-sink") (v "0.3.11") (h "0q0khf144gkrfyg45zgdng80nq6xb0cpbysqzmr7fdf01szgdq6l") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-futures-sink-0.3.12 (c (n "futures-sink") (v "0.3.12") (h "1mj22d4w6digh7wfp6jkr5fdcl9r138q41fxzm1yg8mx568cdxfa") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-futures-sink-0.3.13 (c (n "futures-sink") (v "0.3.13") (h "1cwbbd9dik0p54d53v4kd28lgj2a9bxgprp8ypabfhaqk2c4sxc5") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-futures-sink-0.3.14 (c (n "futures-sink") (v "0.3.14") (h "08vc88majhvizpxpr44h0c240k2ag7is99v153cf6pam7i1jjmjw") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-futures-sink-0.3.15 (c (n "futures-sink") (v "0.3.15") (h "10j21j3k358512qyglqpfyxj309kr6bcspj6izg6s3gzrv8flyx5") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-futures-sink-0.3.16 (c (n "futures-sink") (v "0.3.16") (h "0lsbxbz72a82hyld35wz69k7l6i07krsafkl2ac12g9ncym0mwy0") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-futures-sink-0.3.17 (c (n "futures-sink") (v "0.3.17") (h "04dwqbcwxil6iyv7fif2xjncz849mm63sghbb60f8kq22cy1bsin") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-futures-sink-0.3.18 (c (n "futures-sink") (v "0.3.18") (h "1bq5jdv9d1llmlzq8ycmkbhw2z8yqkwhd6fd68dd4qkv8d168v4r") (f (quote (("std" "alloc") ("default" "std") ("alloc")))) (y #t) (r "1.36")))

(define-public crate-futures-sink-0.3.19 (c (n "futures-sink") (v "0.3.19") (h "026m2x353l7x7apa3hdx26ma7kwgxgbghl0393v4zmv8rfn5n1g3") (f (quote (("std" "alloc") ("default" "std") ("alloc")))) (r "1.36")))

(define-public crate-futures-sink-0.3.20 (c (n "futures-sink") (v "0.3.20") (h "11z7ffl0jdzrwddlskbn55nqnww5a7vrrpi7iyq8h4d62fhsd34g") (f (quote (("std" "alloc") ("default" "std") ("alloc")))) (y #t) (r "1.36")))

(define-public crate-futures-sink-0.3.21 (c (n "futures-sink") (v "0.3.21") (h "0s58gx5yw1a21xviw2qgc0wzk225vgn4kbzddrp141m3kw9kw5i1") (f (quote (("std" "alloc") ("default" "std") ("alloc")))) (r "1.36")))

(define-public crate-futures-sink-0.3.22 (c (n "futures-sink") (v "0.3.22") (h "01ng5pbwf8xcp70lbjf5sxinzd9nkzj3bx5zs0cq663dp2lr9x1j") (f (quote (("std" "alloc") ("default" "std") ("alloc")))) (r "1.36")))

(define-public crate-futures-sink-0.3.23 (c (n "futures-sink") (v "0.3.23") (h "0rb79vnbgbz0pmf78kbiphvppyb3mrsccr00kgyzfb3mx4gsw2ya") (f (quote (("std" "alloc") ("default" "std") ("alloc")))) (r "1.36")))

(define-public crate-futures-sink-0.3.24 (c (n "futures-sink") (v "0.3.24") (h "0mmsyw3ipg3gddm0nh95ly9sr51xc834g0vj1sipnwifm6jhpci1") (f (quote (("std" "alloc") ("default" "std") ("alloc")))) (r "1.36")))

(define-public crate-futures-sink-0.3.25 (c (n "futures-sink") (v "0.3.25") (h "1ygwh57nzinpj2sk6akc6sgavl3njsrjyimvy50dyydalkqmrh9r") (f (quote (("std" "alloc") ("default" "std") ("alloc")))) (r "1.36")))

(define-public crate-futures-sink-0.3.26 (c (n "futures-sink") (v "0.3.26") (h "0r43djzf0caz89c724ishpzxy59y6nw7ykfvh1nd9kz8nc5q447k") (f (quote (("std" "alloc") ("default" "std") ("alloc")))) (r "1.36")))

(define-public crate-futures-sink-0.3.27 (c (n "futures-sink") (v "0.3.27") (h "1wnjp3h90mm6qfq59fy26ayrvbncy3hrsp481alb5bzc98x0i4zc") (f (quote (("std" "alloc") ("default" "std") ("alloc")))) (r "1.36")))

(define-public crate-futures-sink-0.3.28 (c (n "futures-sink") (v "0.3.28") (h "0vkv4frf4c6gm1ag9imjz8d0xvpnn22lkylsls0rffx147zf8fzl") (f (quote (("std" "alloc") ("default" "std") ("alloc")))) (r "1.36")))

(define-public crate-futures-sink-0.3.29 (c (n "futures-sink") (v "0.3.29") (h "05q8jykqddxzp8nwf00wjk5m5mqi546d7i8hsxma7hiqxrw36vg3") (f (quote (("std" "alloc") ("default" "std") ("alloc")))) (r "1.36")))

(define-public crate-futures-sink-0.3.30 (c (n "futures-sink") (v "0.3.30") (h "1dag8xyyaya8n8mh8smx7x6w2dpmafg2din145v973a3hw7f1f4z") (f (quote (("std" "alloc") ("default" "std") ("alloc")))) (r "1.36")))

