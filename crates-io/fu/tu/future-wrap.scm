(define-module (crates-io fu tu future-wrap) #:use-module (crates-io))

(define-public crate-future-wrap-0.1.0 (c (n "future-wrap") (v "0.1.0") (d (list (d (n "pin-project") (r "^1.0.10") (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("time" "macros" "rt"))) (d #t) (k 2)))) (h "0fyw0g7jsia2ck31d8nx86917pn7yid75x1c0qzlapqzh460hi5d")))

(define-public crate-future-wrap-0.1.1 (c (n "future-wrap") (v "0.1.1") (d (list (d (n "pin-project") (r "^1.0.10") (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("time" "macros" "rt"))) (d #t) (k 2)))) (h "0c265jz7rld3f5mkfpmcbplwpxdnxqmz5jir2dn3k4v5a2r15asv")))

