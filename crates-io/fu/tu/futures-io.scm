(define-module (crates-io fu tu futures-io) #:use-module (crates-io))

(define-public crate-futures-io-0.1.0 (c (n "futures-io") (v "0.1.0") (d (list (d (n "futures") (r "^0.1.0") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "1grk2srdhffps4g376rwxg3a80s41p3a3q50wrigfbr7nw26dnak")))

(define-public crate-futures-io-0.2.0-alpha (c (n "futures-io") (v "0.2.0-alpha") (d (list (d (n "futures-core") (r "^0.2.0-alpha") (k 0)) (d (n "iovec") (r "^0.1") (o #t) (d #t) (k 0)))) (h "07qrn024mi394f66liz45p8yj50fyjgzv5v773w75y4jxsagp4qf") (f (quote (("std" "futures-core/std" "iovec") ("default" "std"))))))

(define-public crate-futures-io-0.2.0-beta (c (n "futures-io") (v "0.2.0-beta") (d (list (d (n "futures-core") (r "^0.2.0-beta") (k 0)) (d (n "iovec") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0i9hyf1wzgpw9wnf760h39dj6n9sal30hkq3i35acgvvmy83lhh5") (f (quote (("std" "futures-core/std" "iovec") ("default" "std"))))))

(define-public crate-futures-io-0.2.0 (c (n "futures-io") (v "0.2.0") (d (list (d (n "futures-core") (r "^0.2.0") (k 0)) (d (n "iovec") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1lsdwd9ppz495gc31kgywscvva8bxxzgj3xzw3fx3ar3npyl7ifl") (f (quote (("std" "futures-core/std" "iovec") ("default" "std"))))))

(define-public crate-futures-io-0.2.1 (c (n "futures-io") (v "0.2.1") (d (list (d (n "futures-core") (r "^0.2.1") (k 0)) (d (n "iovec") (r "^0.1") (o #t) (d #t) (k 0)))) (h "18i1md3cx33j94swj9ps787hhkn0binssr1mgb18gp59vc7lg85n") (f (quote (("std" "futures-core/std" "iovec") ("default" "std"))))))

(define-public crate-futures-io-0.3.0 (c (n "futures-io") (v "0.3.0") (h "16cvykxg3p3cw7027gvl2qcvlqlpzyakpzs4kvazac9rg1aglxik") (f (quote (("unstable") ("std") ("read-initializer") ("default" "std"))))))

(define-public crate-futures-io-0.3.1 (c (n "futures-io") (v "0.3.1") (h "1zxm41fmkrb0r39ajk3rr9invcd5bdwlafazn8m9aw4y49ymfxp6") (f (quote (("unstable") ("std") ("read-initializer") ("default" "std"))))))

(define-public crate-futures-io-0.3.2 (c (n "futures-io") (v "0.3.2") (h "074k6sblkva08f83qj5jp1plbs1mfjr9yzd6i0mw83631660kzy0") (f (quote (("unstable") ("std") ("read-initializer") ("default" "std"))))))

(define-public crate-futures-io-0.3.3 (c (n "futures-io") (v "0.3.3") (h "13lb1fld694vnp869kbhsp3giq9pb80f2m4fzi2vspay9f19yhix") (f (quote (("unstable") ("std") ("read-initializer") ("default" "std"))))))

(define-public crate-futures-io-0.3.4 (c (n "futures-io") (v "0.3.4") (h "1ihd1429kyfncxpc4mvina1hbppywfqzql6zrnjcflk1m6d9af56") (f (quote (("unstable") ("std") ("read-initializer") ("default" "std"))))))

(define-public crate-futures-io-0.3.5 (c (n "futures-io") (v "0.3.5") (h "128pnpcaqj9sxr1d0scy51yfk2ggvqp6szlm2jf8d3is04mi89yy") (f (quote (("unstable") ("std") ("read-initializer") ("default" "std"))))))

(define-public crate-futures-io-0.3.6 (c (n "futures-io") (v "0.3.6") (h "0g0j9fkyj41kyr4m8g35fhxyxqqqpyv905rg8d73nm1rpdj4pjaz") (f (quote (("unstable") ("std") ("read-initializer") ("default" "std"))))))

(define-public crate-futures-io-0.3.7 (c (n "futures-io") (v "0.3.7") (h "0ayxbl36z81bmzx2s3dq3nl7mkjqkycsl4kv9aagy9s79a2rh5vf") (f (quote (("unstable") ("std") ("read-initializer") ("default" "std"))))))

(define-public crate-futures-io-0.3.8 (c (n "futures-io") (v "0.3.8") (h "1frh7d0n96lczy22al3bkgwpq0p1agbgax5kqh9vv8da33738631") (f (quote (("unstable") ("std") ("read-initializer") ("default" "std"))))))

(define-public crate-futures-io-0.3.9 (c (n "futures-io") (v "0.3.9") (h "0fr5yd3slv0lfydy8767sjmxhp1cd68fv3jbhqi0kf1pn18ilz73") (f (quote (("unstable") ("std") ("read-initializer") ("default" "std"))))))

(define-public crate-futures-io-0.3.10 (c (n "futures-io") (v "0.3.10") (h "17s6blgy6hj733qs5w8q3vbidrahdzpp8fan04ysg32y3pkk84nl") (f (quote (("unstable") ("std") ("read-initializer") ("default" "std"))))))

(define-public crate-futures-io-0.3.11 (c (n "futures-io") (v "0.3.11") (h "15rr4y43jsdfyrjpfwzfrb5cpxpfksa9jv0pw8a6df2s9rz9cs1q") (f (quote (("unstable") ("std") ("read-initializer") ("default" "std"))))))

(define-public crate-futures-io-0.3.12 (c (n "futures-io") (v "0.3.12") (h "0005hz30pdxlbdamhd0imixmxcjpwrfxxr27kljxh6i84lshbgi8") (f (quote (("unstable") ("std") ("read-initializer") ("default" "std"))))))

(define-public crate-futures-io-0.3.13 (c (n "futures-io") (v "0.3.13") (h "0ndshyr6vgnx0z421h2asd7jkhy2cwqj5ha1a8pw613pqmjjq76p") (f (quote (("unstable") ("std") ("read-initializer") ("default" "std"))))))

(define-public crate-futures-io-0.3.14 (c (n "futures-io") (v "0.3.14") (h "016b6pfcsaavwcpx22psh6n36s13ymc23nqghcxc188fncgilnin") (f (quote (("unstable") ("std") ("read-initializer") ("default" "std"))))))

(define-public crate-futures-io-0.3.15 (c (n "futures-io") (v "0.3.15") (h "1899vd5yri6zv88by53pzxb1h4x96mc42riziy6qyd1vzgg9ki5c") (f (quote (("unstable") ("std") ("read-initializer") ("default" "std"))))))

(define-public crate-futures-io-0.3.16 (c (n "futures-io") (v "0.3.16") (h "10hms4pzsxay7s9qclp78xis9mlklzf5ris6y9p3m3q6jg1hc3hb") (f (quote (("unstable") ("std") ("read-initializer") ("default" "std"))))))

(define-public crate-futures-io-0.3.17 (c (n "futures-io") (v "0.3.17") (h "0xrk0xsrmhxrndhby79j31mkzbwg21s09fkpqldhyf1yzshf4baj") (f (quote (("unstable") ("std") ("read-initializer") ("default" "std"))))))

(define-public crate-futures-io-0.3.18 (c (n "futures-io") (v "0.3.18") (h "04dakn3362gw8q2jd9wdq19df4am1j5p4sngdhj57hxmnr6kb0g4") (f (quote (("unstable") ("std") ("read-initializer") ("default" "std")))) (y #t) (r "1.36")))

(define-public crate-futures-io-0.3.19 (c (n "futures-io") (v "0.3.19") (h "1ckir41haa2hs9znrwavgh33hv3l23jmywqg73xwdam1ym5d7ydi") (f (quote (("unstable") ("std") ("default" "std")))) (r "1.36")))

(define-public crate-futures-io-0.3.20 (c (n "futures-io") (v "0.3.20") (h "07n5jpc1mq4gln6g96fh3pjy74943qjyy85h25fkwfyp38kl4x7l") (f (quote (("unstable") ("std") ("default" "std")))) (y #t) (r "1.36")))

(define-public crate-futures-io-0.3.21 (c (n "futures-io") (v "0.3.21") (h "0swn29fysas36ikk5aw55104fi98117amvgxw9g96pjs5ab4ah7w") (f (quote (("unstable") ("std") ("default" "std")))) (r "1.36")))

(define-public crate-futures-io-0.3.22 (c (n "futures-io") (v "0.3.22") (h "0qdd7931xxjq85wqcwwf3skrw46lcz8ylsx00q0b8d39dwlzqabw") (f (quote (("unstable") ("std") ("default" "std")))) (r "1.36")))

(define-public crate-futures-io-0.3.23 (c (n "futures-io") (v "0.3.23") (h "1r9829x3y4a98ksvw4jm9jp67njylfyd59jmw8x6m8ims336z9lk") (f (quote (("unstable") ("std") ("default" "std")))) (r "1.36")))

(define-public crate-futures-io-0.3.24 (c (n "futures-io") (v "0.3.24") (h "0s7bhmfnbs8pywlhp83bixxi5fn7w73if2vwcdw4bz88lfkx5x5v") (f (quote (("unstable") ("std") ("default" "std")))) (r "1.36")))

(define-public crate-futures-io-0.3.25 (c (n "futures-io") (v "0.3.25") (h "1szl4w206x2inliipf5hvjbrd8w8i0gnglz8akmsvp3bl19gpx80") (f (quote (("unstable") ("std") ("default" "std")))) (r "1.36")))

(define-public crate-futures-io-0.3.26 (c (n "futures-io") (v "0.3.26") (h "0cc5s3qdgls25rlm3zpdf9fdk6gwmfp0fiiph39b5bmjdwdkgf5z") (f (quote (("unstable") ("std") ("default" "std")))) (r "1.36")))

(define-public crate-futures-io-0.3.27 (c (n "futures-io") (v "0.3.27") (h "14cbfwn1d9qzdzys7vjpx9c85javpdx0iavllpf40fxy7kx25m49") (f (quote (("unstable") ("std") ("default" "std")))) (r "1.36")))

(define-public crate-futures-io-0.3.28 (c (n "futures-io") (v "0.3.28") (h "0r4rhkdhq1my4fahlhz59barqa511bylq813w3w4gvbidq4p9zsg") (f (quote (("unstable") ("std") ("default" "std")))) (r "1.36")))

(define-public crate-futures-io-0.3.29 (c (n "futures-io") (v "0.3.29") (h "1ajsljgny3zfxwahba9byjzclrgvm1ypakca8z854k2w7cb4mwwb") (f (quote (("unstable") ("std") ("default" "std")))) (r "1.36")))

(define-public crate-futures-io-0.3.30 (c (n "futures-io") (v "0.3.30") (h "1hgh25isvsr4ybibywhr4dpys8mjnscw4wfxxwca70cn1gi26im4") (f (quote (("unstable") ("std") ("default" "std")))) (r "1.36")))

