(define-module (crates-io fu tu futures-threadpool) #:use-module (crates-io))

(define-public crate-futures-threadpool-0.0.1 (c (n "futures-threadpool") (v "0.0.1") (h "0vaxy1wjsqp7p9zfd258sm9q8mvzyx5qa67rnxw5fy41b38pbf1c")))

(define-public crate-futures-threadpool-0.1.0 (c (n "futures-threadpool") (v "0.1.0") (d (list (d (n "crossbeam") (r "^0.2") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "futures-spawn") (r "^0.1") (f (quote ("use_std"))) (k 0)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)))) (h "05ahrjdlcnv9zk2cvddbksqv04518wvxbpsp2xciv6zy2g09qsnc")))

