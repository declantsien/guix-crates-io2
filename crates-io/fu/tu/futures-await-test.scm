(define-module (crates-io fu tu futures-await-test) #:use-module (crates-io))

(define-public crate-futures-await-test-0.1.0 (c (n "futures-await-test") (v "0.1.0") (d (list (d (n "futures-await") (r "^0.1") (d #t) (k 2)) (d (n "futures-await-quote") (r "^0.4") (d #t) (k 0)) (d (n "futures-await-syn") (r "^0.12") (f (quote ("full" "fold" "parsing" "printing"))) (k 0)) (d (n "proc-macro2") (r "^0.1") (f (quote ("unstable"))) (d #t) (k 0)))) (h "1r88p2s23fm1liqicp97mgjxzsmgkpi802kz76fn8dzpkn68lrm0")))

(define-public crate-futures-await-test-0.1.1 (c (n "futures-await-test") (v "0.1.1") (d (list (d (n "futures-await") (r "^0.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^0.2") (d #t) (k 0)) (d (n "quote") (r "^0.4") (d #t) (k 0)) (d (n "syn") (r "^0.12") (f (quote ("full" "parsing" "printing"))) (k 0)))) (h "04xccxqh7dkvwr0qnzrd0n8lhb4znqql88hq3bqdca6gyz98yw5g")))

(define-public crate-futures-await-test-0.1.2 (c (n "futures-await-test") (v "0.1.2") (d (list (d (n "futures-await") (r "^0.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^0.2") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.4") (d #t) (k 0)) (d (n "syn") (r "^0.12") (f (quote ("full" "parsing" "printing"))) (k 0)))) (h "036p72im1r2rar3yqlkqszarbcbfhvymdw83hyv76bza8xa99yic")))

(define-public crate-futures-await-test-0.1.3 (c (n "futures-await-test") (v "0.1.3") (d (list (d (n "futures-await") (r "^0.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^0.2") (d #t) (k 0)) (d (n "quote") (r "^0.4") (d #t) (k 0)) (d (n "syn") (r "^0.12") (f (quote ("full" "parsing" "printing"))) (k 0)))) (h "103chv8yjkjqvlmdzj1hsdwspcw815f8qm9zmnd1dr20c4cvkdfw")))

(define-public crate-futures-await-test-0.1.4 (c (n "futures-await-test") (v "0.1.4") (d (list (d (n "futures-await") (r "^0.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^0.3") (d #t) (k 0)) (d (n "quote") (r "^0.5") (d #t) (k 0)) (d (n "syn") (r "^0.13") (f (quote ("full" "parsing" "printing" "proc-macro"))) (k 0)))) (h "1kvbd3ykw7xmq3yckqk5p2id5szmyaai06160mgrjgyvnwmk04mf")))

(define-public crate-futures-await-test-0.1.5 (c (n "futures-await-test") (v "0.1.5") (d (list (d (n "futures-await") (r "^0.1.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^0.3") (d #t) (k 0)) (d (n "quote") (r "^0.5") (d #t) (k 0)) (d (n "syn") (r "^0.13") (f (quote ("full" "parsing" "printing" "proc-macro"))) (k 0)))) (h "08bxd83hfdg7mmqv2ii4vwrddx921xs3i63nhnd1rlgb8v6qfdwg")))

(define-public crate-futures-await-test-0.2.0 (c (n "futures-await-test") (v "0.2.0") (d (list (d (n "futures") (r "^0.2.1") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.5") (d #t) (k 0)) (d (n "syn") (r "^0.13") (f (quote ("full" "parsing" "printing" "proc-macro"))) (k 0)))) (h "0wm90qg9qn3xa7lgrbihyx0wn06h702w6zhvlkwi1fpy9gm1bnf9") (y #t)))

(define-public crate-futures-await-test-0.2.1 (c (n "futures-await-test") (v "0.2.1") (d (list (d (n "futures-preview") (r "^0.2.2") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.14") (f (quote ("full" "parsing" "printing" "proc-macro"))) (k 0)))) (h "0v1maan1l6a7q6ama6l8bxkcag1a1h9g4cb5pd2fry4y0q9h70v1") (y #t)))

(define-public crate-futures-await-test-0.1.6 (c (n "futures-await-test") (v "0.1.6") (d (list (d (n "futures-await") (r "^0.1.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.14") (f (quote ("full" "parsing" "printing" "proc-macro"))) (k 0)))) (h "13zy463xjpq3zd205frhgm0jh05syr0rci5xykfn41v82v9v4v8z")))

(define-public crate-futures-await-test-0.1.7 (c (n "futures-await-test") (v "0.1.7") (d (list (d (n "futures-await") (r "^0.1.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "parsing" "printing" "proc-macro"))) (k 0)))) (h "06qm8diwh04wxwm474k8gl9m2p0lnhdmxwrjmn5bipds23m1ihl3")))

(define-public crate-futures-await-test-0.3.0 (c (n "futures-await-test") (v "0.3.0") (d (list (d (n "futures-await-test-macro") (r "= 0.3.0") (d #t) (k 0)) (d (n "futures-executor") (r "^0.3.0") (d #t) (k 0)))) (h "1s6790zdmah6yikkm70na20w2r01yakz9r41llmmffp7ph2ks3df")))

