(define-module (crates-io fu tu futures-retry) #:use-module (crates-io))

(define-public crate-futures-retry-0.1.0 (c (n "futures-retry") (v "0.1.0") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 2)) (d (n "tokio-timer") (r "^0.1") (d #t) (k 0)))) (h "0dq5xbrbfpm7w7g5h2lm0n1zv4pbz56q99yznxmjv3rkh5nwdim2") (y #t)))

(define-public crate-futures-retry-0.1.1 (c (n "futures-retry") (v "0.1.1") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 2)) (d (n "tokio-timer") (r "^0.1") (d #t) (k 0)))) (h "0wyr79y9k1qa3l6wicqhjbzzj8rl3z1v4s1qx7d7mm6zf0a5nqzv") (y #t)))

(define-public crate-futures-retry-0.1.2 (c (n "futures-retry") (v "0.1.2") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 2)) (d (n "tokio-timer") (r "^0.1") (d #t) (k 0)))) (h "0y87laha49c0a79jhm8545cvd1b6gibnb01fgrvavlrab3jsdyaz")))

(define-public crate-futures-retry-0.1.3 (c (n "futures-retry") (v "0.1.3") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 2)) (d (n "tokio-timer") (r "^0.2") (d #t) (k 0)))) (h "0sryjcd497fsfwd2y9gxdlwisxyh44whpsjzz0xrkz6ra01zq7b9")))

(define-public crate-futures-retry-0.1.4 (c (n "futures-retry") (v "0.1.4") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 2)) (d (n "tokio-timer") (r "^0.2") (d #t) (k 0)))) (h "0ljd7ivrlyn1b626d9d76zydcdk4r2cil67j6aya7rpzjjsp67r9")))

(define-public crate-futures-retry-0.2.0 (c (n "futures-retry") (v "0.2.0") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 2)) (d (n "tokio-timer") (r "^0.2") (d #t) (k 0)))) (h "1w55650sj15kywnc35w0xbnppbkcaqw5qsj1lipy1xp8nrbriwz8")))

(define-public crate-futures-retry-0.2.1 (c (n "futures-retry") (v "0.2.1") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 2)) (d (n "tokio-timer") (r "^0.2") (d #t) (k 0)))) (h "0jgwb3sjxvyl3jbyx6rvwvdiyj55rmlfzc0q2rdnb5hjff87xki6")))

(define-public crate-futures-retry-0.3.0 (c (n "futures-retry") (v "0.3.0") (d (list (d (n "futures") (r "^0.1.20") (d #t) (k 0)) (d (n "tokio") (r "^0.1.5") (d #t) (k 2)) (d (n "tokio-timer") (r "^0.2.1") (d #t) (k 0)))) (h "0296n6mg2x6qfwzwkk70k607cj2n2rsfw8fvzg6fhf3ja6c8j2i2")))

(define-public crate-futures-retry-0.3.2 (c (n "futures-retry") (v "0.3.2") (d (list (d (n "futures") (r "^0.1.21") (d #t) (k 0)) (d (n "tokio") (r "^0.1.12") (d #t) (k 2)) (d (n "tokio-fs") (r "^0.1") (d #t) (k 2)) (d (n "tokio-timer") (r "^0.2.1") (d #t) (k 0)))) (h "1yryg02l4ganix1ln2sr97xic66l4dnzpqnp6y64hv8kxgq4c319")))

(define-public crate-futures-retry-0.3.3 (c (n "futures-retry") (v "0.3.3") (d (list (d (n "futures") (r "^0.1.21") (d #t) (k 0)) (d (n "tokio") (r "^0.1.12") (d #t) (k 2)) (d (n "tokio-fs") (r "^0.1") (d #t) (k 2)) (d (n "tokio-timer") (r "^0.2.1") (d #t) (k 0)))) (h "15pcc8pczppwllz01283k6zd23lxa2p47gdmjkr7xc1sbf3ix6yp")))

(define-public crate-futures-retry-0.4.0 (c (n "futures-retry") (v "0.4.0") (d (list (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "pin-project") (r "^0.4.6") (d #t) (k 0)) (d (n "tokio") (r "^0.2.3") (f (quote ("time"))) (k 0)) (d (n "tokio") (r "^0.2.3") (f (quote ("full"))) (d #t) (k 2)))) (h "0m6m578i1dn3n01fbw9pykigwx2hsyf5zqjrfp8bkdrx4zn9b6nc")))

(define-public crate-futures-retry-0.5.0 (c (n "futures-retry") (v "0.5.0") (d (list (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "pin-project") (r "^0.4.6") (d #t) (k 0)) (d (n "tokio") (r "^0.2.3") (f (quote ("time"))) (k 0)) (d (n "tokio") (r "^0.2.3") (f (quote ("full"))) (d #t) (k 2)))) (h "083lp8lhd2xgg52layzqrv6w85qzw3h77acp47q8k599cisgx6vm")))

(define-public crate-futures-retry-0.6.0 (c (n "futures-retry") (v "0.6.0") (d (list (d (n "futures") (r "^0.3.9") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2.4") (d #t) (k 0)) (d (n "tokio") (r "^1.0.1") (f (quote ("time"))) (k 0)) (d (n "tokio") (r "^1.0.1") (f (quote ("full"))) (d #t) (k 2)))) (h "03ywb9q86z7ixbb0bqmix55sxg9z3k4dk7zdllm5b5hzlrradrgx")))

