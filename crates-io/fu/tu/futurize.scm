(define-module (crates-io fu tu futurize) #:use-module (crates-io))

(define-public crate-futurize-0.1.0 (c (n "futurize") (v "0.1.0") (d (list (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "futures") (r "^0.1.23") (d #t) (k 0)) (d (n "heck") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.12") (d #t) (k 0)) (d (n "quote") (r "^0.6.6") (d #t) (k 0)) (d (n "syn") (r "^0.14.8") (d #t) (k 0)) (d (n "tokio") (r "^0.1.7") (d #t) (k 0)))) (h "02dmwdcvfkylig7ra2gi6hc1adxap8ifznd0v10dqvqlidslhs0f")))

(define-public crate-futurize-0.2.0 (c (n "futurize") (v "0.2.0") (d (list (d (n "futures") (r "^0.1.23") (d #t) (k 0)) (d (n "heck") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.12") (d #t) (k 0)) (d (n "quote") (r "^0.6.6") (d #t) (k 0)) (d (n "syn") (r "^0.14.8") (d #t) (k 0)) (d (n "tokio") (r "^0.1.7") (d #t) (k 2)))) (h "0p11ias366svgyd0y8wq420f33yjwflq7s6jf4k4paad8gh61gyi")))

(define-public crate-futurize-0.2.1 (c (n "futurize") (v "0.2.1") (d (list (d (n "futures") (r "^0.1.23") (d #t) (k 0)) (d (n "heck") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.12") (d #t) (k 0)) (d (n "quote") (r "^0.6.6") (d #t) (k 0)) (d (n "syn") (r "^0.14.8") (d #t) (k 0)) (d (n "tokio") (r "^0.1.7") (d #t) (k 2)))) (h "06p2z4n387g0n5j9bvsd84yx41y9mq0ywpd49bvyvfjssw294vz1")))

(define-public crate-futurize-0.4.1 (c (n "futurize") (v "0.4.1") (d (list (d (n "futures") (r "^0.1.23") (d #t) (k 0)))) (h "106s3r93scmj6a0ii5j6n96brr0q3jdz7aq0d18l38z77jrhhrdz")))

(define-public crate-futurize-0.4.2 (c (n "futurize") (v "0.4.2") (d (list (d (n "futures") (r "^0.1.23") (d #t) (k 0)))) (h "1vbh9qiq9diwqnchd2qa83b9xb8wdijckcrgiw4vx5rs1m2hc0ba")))

(define-public crate-futurize-0.5.0 (c (n "futurize") (v "0.5.0") (d (list (d (n "futures") (r "^0.1.23") (d #t) (k 0)))) (h "0qxllll362h4rl030mq374353f4nny00yzgsg23kajqyw7053zan")))

