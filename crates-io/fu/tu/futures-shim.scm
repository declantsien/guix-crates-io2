(define-module (crates-io fu tu futures-shim) #:use-module (crates-io))

(define-public crate-futures-shim-0.1.0 (c (n "futures-shim") (v "0.1.0") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 2)))) (h "1l3g1di243i0r9y2kg2jwmva9mjkjyjjb69cgpy1x037kg362m5h")))

(define-public crate-futures-shim-0.1.1 (c (n "futures-shim") (v "0.1.1") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 2)))) (h "0nbhmhpdf6z75qc6dk7d8ms0j3c0lg6xxh59iglkp5f7b7r12b47")))

(define-public crate-futures-shim-0.1.2 (c (n "futures-shim") (v "0.1.2") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 2)))) (h "0hdw61n5wrnh37g8nkjrv6bvd9ynbiav82pyv8h44lrnmlhjfbhs")))

(define-public crate-futures-shim-0.1.3 (c (n "futures-shim") (v "0.1.3") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 2)))) (h "0jxd51w8rs87vxjww093l3vh6ddk2zbyjjk1znwfzkzihlyrd1sb")))

