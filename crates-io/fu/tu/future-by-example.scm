(define-module (crates-io fu tu future-by-example) #:use-module (crates-io))

(define-public crate-future-by-example-0.1.0 (c (n "future-by-example") (v "0.1.0") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)))) (h "0aqs90l3vsirz5gha16r4sghmlwg29p2dh179p28q24gd9i7d6mi")))

