(define-module (crates-io fu tu future_rss) #:use-module (crates-io))

(define-public crate-future_rss-1.0.0 (c (n "future_rss") (v "1.0.0") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "quick-xml") (r "^0.18.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.4") (d #t) (k 0)) (d (n "tokio") (r "^0.2.21") (f (quote ("macros" "rt-core"))) (d #t) (k 0)))) (h "14pmjyp8lj0j1gk6fd6s1ncz62djvvqyzzsvc6viaa7h68xpib3d")))

(define-public crate-future_rss-1.0.1 (c (n "future_rss") (v "1.0.1") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "quick-xml") (r "^0.18.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.4") (d #t) (k 0)) (d (n "tokio") (r "^0.2.21") (f (quote ("macros" "rt-core"))) (d #t) (k 0)))) (h "1r4xgc4ysmnshhxbj91wvwzy7r2vfmbjmh432kbc8qf05mnlv4p4")))

(define-public crate-future_rss-1.0.2 (c (n "future_rss") (v "1.0.2") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "quick-xml") (r "^0.18.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.4") (d #t) (k 0)) (d (n "tokio") (r "^0.2.21") (f (quote ("macros" "rt-core"))) (d #t) (k 0)))) (h "1qk1fm979s1k92rlvbyxwz8s3pl1hdvv7mkp3az4jzla4rwinkxd")))

