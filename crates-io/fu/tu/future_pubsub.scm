(define-module (crates-io fu tu future_pubsub) #:use-module (crates-io))

(define-public crate-future_pubsub-0.1.0 (c (n "future_pubsub") (v "0.1.0") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 2)))) (h "0gcplqrbbxgzxld49xm5hs6pf8fx22pj1xcfdcz8wjdfv3y3wzbz")))

(define-public crate-future_pubsub-0.1.1 (c (n "future_pubsub") (v "0.1.1") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 2)))) (h "0p62cmbdp39cih1dl1gizly03qbldjzqx54fdjqlsc2iih8yj41v")))

(define-public crate-future_pubsub-0.1.2 (c (n "future_pubsub") (v "0.1.2") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 2)))) (h "0nxnrbljk380ssw2pivjq4qjp464xxf8igizxjg36607cqgvhc2w")))

(define-public crate-future_pubsub-0.1.3 (c (n "future_pubsub") (v "0.1.3") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 2)))) (h "13blxm856fsw1wkpfv12a7f041822mbw86ax9dgj8l10qdwngfkf")))

