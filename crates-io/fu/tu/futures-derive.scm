(define-module (crates-io fu tu futures-derive) #:use-module (crates-io))

(define-public crate-futures-derive-0.1.0 (c (n "futures-derive") (v "0.1.0") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "1qrys030hpb9mkcga73d324a242gjjvwznsx8vky40f8p9da2122")))

