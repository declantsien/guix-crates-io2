(define-module (crates-io fu tu futures-dagtask) #:use-module (crates-io))

(define-public crate-futures-dagtask-0.1.0 (c (n "futures-dagtask") (v "0.1.0") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)))) (h "1k8lijv2gq2ca1vj28cbjpqn7khxlhdss3ycpybvbdzr01q5rs99")))

(define-public crate-futures-dagtask-0.2.0 (c (n "futures-dagtask") (v "0.2.0") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0zkl6l7fjd00srwl8dp0b9mwrh7m162biq728yynvpjkw8nxvfd3")))

(define-public crate-futures-dagtask-0.3.0 (c (n "futures-dagtask") (v "0.3.0") (d (list (d (n "futures") (r "^0.3") (f (quote ("bilock" "unstable"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "070z9rr08iikqj6zcr7b384c5w6si84fdpbl3jmc9ps0hy9nfd9i")))

