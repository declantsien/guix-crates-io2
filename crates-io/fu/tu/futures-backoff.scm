(define-module (crates-io fu tu futures-backoff) #:use-module (crates-io))

(define-public crate-futures-backoff-0.1.0 (c (n "futures-backoff") (v "0.1.0") (d (list (d (n "futures") (r "^0.1.15") (d #t) (k 0)) (d (n "futures-timer") (r "^0.1.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.6.0") (d #t) (k 2)) (d (n "rand") (r "^0.4.0") (d #t) (k 0)))) (h "06wp2cx289h8ifjrxrd3agvpnzhwm16bg3866qgxzkp8bqs91xbr")))

