(define-module (crates-io fu tu futures-stream-reader) #:use-module (crates-io))

(define-public crate-futures-stream-reader-0.1.0 (c (n "futures-stream-reader") (v "0.1.0") (d (list (d (n "futures-util") (r "^0.3") (f (quote ("io"))) (k 0)) (d (n "futures-executor") (r "^0.3") (d #t) (k 2)))) (h "0g2bfj5gdkpivnca4ijy9j1mhn47fqmpnz3ys6wi5jxjk1l2zr96")))

(define-public crate-futures-stream-reader-0.2.0 (c (n "futures-stream-reader") (v "0.2.0") (d (list (d (n "futures-util") (r "^0.3") (f (quote ("io"))) (k 0)) (d (n "futures-executor") (r "^0.3") (d #t) (k 2)))) (h "01b6pjqwpi0zlkpvkq7j1xidhmapbmp8440bxsm26kgwk861rv14")))

