(define-module (crates-io fu tu futures-log) #:use-module (crates-io))

(define-public crate-futures-log-0.1.0 (c (n "futures-log") (v "0.1.0") (d (list (d (n "futures") (r "^0.1.18") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0gwp8679ingb59q3zrv4cfmz47hcbklx4zhshbayla5zfb8pm4hn")))

(define-public crate-futures-log-0.1.1 (c (n "futures-log") (v "0.1.1") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1pk5999gnpkznf6b5gdan6fimamc48m8q68faxq1x75j4gf1f2l0")))

