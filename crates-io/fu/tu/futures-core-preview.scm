(define-module (crates-io fu tu futures-core-preview) #:use-module (crates-io))

(define-public crate-futures-core-preview-0.2.2 (c (n "futures-core-preview") (v "0.2.2") (d (list (d (n "either") (r "^1.4") (o #t) (k 0)))) (h "199y6649lp3mzg61asf6im5632m34rrdsq64gkma1li86zbi356f") (f (quote (("std" "either/use_std") ("nightly") ("default" "std"))))))

(define-public crate-futures-core-preview-0.2.3 (c (n "futures-core-preview") (v "0.2.3") (d (list (d (n "either") (r "^1.4") (o #t) (k 0)))) (h "0zgrmwc6yf1z546ypcxclnz9344s0cj3rhhz7fzmskml7m0qb1q9") (f (quote (("std" "either/use_std") ("nightly") ("default" "std"))))))

(define-public crate-futures-core-preview-0.3.0-alpha.1 (c (n "futures-core-preview") (v "0.3.0-alpha.1") (d (list (d (n "either") (r "^1.4") (o #t) (k 0)))) (h "0a5rr7skh9753ld2dfpsf6q3ydwvy8mb7g38w4hlgwkclz03j09l") (f (quote (("std" "either/use_std") ("default" "std"))))))

(define-public crate-futures-core-preview-0.3.0-alpha.2 (c (n "futures-core-preview") (v "0.3.0-alpha.2") (d (list (d (n "either") (r "^1.4") (o #t) (k 0)))) (h "1fgd0kc1yv46pm6wla4f5vfsq0bxrswzz1yybkrjg76vp3ljwdij") (f (quote (("std" "either/use_std") ("default" "std"))))))

(define-public crate-futures-core-preview-0.3.0-alpha.3 (c (n "futures-core-preview") (v "0.3.0-alpha.3") (d (list (d (n "either") (r "^1.4") (o #t) (k 0)))) (h "0wxgmf4qzh6i3dkqw3nd183qfqv66gr5912i4hy4xlavi5x8lxik") (f (quote (("std" "either/use_std") ("default" "std"))))))

(define-public crate-futures-core-preview-0.3.0-alpha.4 (c (n "futures-core-preview") (v "0.3.0-alpha.4") (d (list (d (n "either") (r "^1.4") (o #t) (k 0)))) (h "05a16mg9d6gpiapbgsg2y01yws16i1pfama3fkpwjnqgwi4khpda") (f (quote (("std" "either/use_std") ("default" "std"))))))

(define-public crate-futures-core-preview-0.3.0-alpha.5 (c (n "futures-core-preview") (v "0.3.0-alpha.5") (d (list (d (n "either") (r "^1.4") (o #t) (k 0)))) (h "0iyim90lfr4nd98in8xx7s7smm7sanrkdgd5f2cjvpqka693nv1k") (f (quote (("std" "either/use_std") ("default" "std"))))))

(define-public crate-futures-core-preview-0.3.0-alpha.6 (c (n "futures-core-preview") (v "0.3.0-alpha.6") (d (list (d (n "either") (r "^1.4") (o #t) (k 0)))) (h "1mqkw9ss8kdf0jys51glamsl8hxbm2mgnmnwbpi6wjaix5mwfzw0") (f (quote (("std" "either/use_std") ("default" "std"))))))

(define-public crate-futures-core-preview-0.3.0-alpha.7 (c (n "futures-core-preview") (v "0.3.0-alpha.7") (d (list (d (n "either") (r "^1.4") (o #t) (k 0)))) (h "100jq3cx8w0yym2m2vaap1wq8p6zwy61ampx50gdqc6f5xr5nrwy") (f (quote (("std" "either/use_std") ("default" "std"))))))

(define-public crate-futures-core-preview-0.3.0-alpha.8 (c (n "futures-core-preview") (v "0.3.0-alpha.8") (d (list (d (n "either") (r "^1.4") (o #t) (k 0)))) (h "1dc7nbg5amhmlps42zx7x28ps72qcrrblbbn44ijxr5iqv8y91y1") (f (quote (("std" "either/use_std") ("default" "std"))))))

(define-public crate-futures-core-preview-0.3.0-alpha.9 (c (n "futures-core-preview") (v "0.3.0-alpha.9") (d (list (d (n "either") (r "^1.4") (o #t) (k 0)))) (h "0ls1dkj2z26yl227nxj4nsskzl4fsfyllqpqhxfp9w8bjbaadzp5") (f (quote (("std" "either/use_std") ("default" "std"))))))

(define-public crate-futures-core-preview-0.3.0-alpha.10 (c (n "futures-core-preview") (v "0.3.0-alpha.10") (d (list (d (n "either") (r "^1.4") (o #t) (k 0)))) (h "1y0f4rq72mhnc9l41c5ljvf6g88m4pq8icf71n864qv1h210w3a9") (f (quote (("std" "either/use_std") ("nightly") ("default" "std"))))))

(define-public crate-futures-core-preview-0.3.0-alpha.11 (c (n "futures-core-preview") (v "0.3.0-alpha.11") (d (list (d (n "either") (r "^1.4") (o #t) (k 0)))) (h "12d5m3xvp0l95dya2mhyxpsri84kfbnrjh4ccrj25ldz5k7hdw6y") (f (quote (("std" "either/use_std") ("nightly") ("default" "std"))))))

(define-public crate-futures-core-preview-0.3.0-alpha.12 (c (n "futures-core-preview") (v "0.3.0-alpha.12") (d (list (d (n "either") (r "^1.4") (o #t) (k 0)))) (h "0dpdzrq9wkjz3jjvk4n6xdziplwdp76k64cizjnafjm4jsw87h10") (f (quote (("std" "either/use_std") ("nightly") ("default" "std"))))))

(define-public crate-futures-core-preview-0.3.0-alpha.13 (c (n "futures-core-preview") (v "0.3.0-alpha.13") (d (list (d (n "either") (r "^1.4") (o #t) (k 0)))) (h "1c38286p2kr77q4dsi4vfbb8cm3lfdqaqdgpaqcwmhigkf5l9207") (f (quote (("std" "either/use_std") ("nightly") ("default" "std") ("cfg-target-has-atomic"))))))

(define-public crate-futures-core-preview-0.3.0-alpha.14 (c (n "futures-core-preview") (v "0.3.0-alpha.14") (d (list (d (n "either") (r "^1.4") (o #t) (k 0)))) (h "1cl0wxwvhacn1741r51ry75ik5bmq4dwfisl1qwqkxcik4cpn7va") (f (quote (("std" "alloc" "either/use_std") ("nightly") ("default" "std") ("cfg-target-has-atomic") ("alloc"))))))

(define-public crate-futures-core-preview-0.3.0-alpha.15 (c (n "futures-core-preview") (v "0.3.0-alpha.15") (h "0s7s3gghh3ia8bb5vbw106yc12rwm79kwq830ajb627xb0yq78qh") (f (quote (("std" "alloc") ("nightly") ("default" "std") ("cfg-target-has-atomic") ("alloc"))))))

(define-public crate-futures-core-preview-0.3.0-alpha.16 (c (n "futures-core-preview") (v "0.3.0-alpha.16") (h "0dlclp85bbay5q900c5vsn6m7fbxc1dbnmlqhixnaav453rp15vi") (f (quote (("std" "alloc") ("nightly") ("default" "std") ("cfg-target-has-atomic") ("alloc"))))))

(define-public crate-futures-core-preview-0.3.0-alpha.17 (c (n "futures-core-preview") (v "0.3.0-alpha.17") (h "1xaq8m609k6cz8xydwhwp8xxyxigabcw1w9ngycfy0bnkg7iq52b") (f (quote (("std" "alloc") ("nightly") ("default" "std") ("cfg-target-has-atomic") ("alloc"))))))

(define-public crate-futures-core-preview-0.3.0-alpha.18 (c (n "futures-core-preview") (v "0.3.0-alpha.18") (h "1saki42jrcbw1hws1rb99zxsq4v39fyq236svi3kh6xqfkvjcbsa") (f (quote (("std" "alloc") ("nightly") ("default" "std") ("cfg-target-has-atomic") ("alloc"))))))

(define-public crate-futures-core-preview-0.3.0-alpha.19 (c (n "futures-core-preview") (v "0.3.0-alpha.19") (h "02n66jkjhpy210dv24pz0j30lvyin5kzlrb50p1j7x8yzdin4nxk") (f (quote (("unstable") ("std" "alloc") ("default" "std") ("cfg-target-has-atomic") ("alloc"))))))

