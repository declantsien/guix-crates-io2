(define-module (crates-io fu tu futures-lite-byteorder) #:use-module (crates-io))

(define-public crate-futures-lite-byteorder-0.1.0 (c (n "futures-lite-byteorder") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "futures-lite") (r "^1.13.0") (d #t) (k 0)))) (h "0jylgqxr2hjcp1b2rpffy04hm35k2r0nj7wdfhpks98xk1s4i0lb")))

(define-public crate-futures-lite-byteorder-0.1.1 (c (n "futures-lite-byteorder") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "futures-lite") (r "^2.2.0") (d #t) (k 0)))) (h "0pya28w6k6alyjk78q555yqlns4m1b2lpb21r8a29gs7y8dfvl59")))

