(define-module (crates-io fu tu futures-bounded) #:use-module (crates-io))

(define-public crate-futures-bounded-0.1.0 (c (n "futures-bounded") (v "0.1.0") (d (list (d (n "futures-timer") (r "^3.0.2") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.28") (d #t) (k 0)) (d (n "tokio") (r "^1.29.1") (f (quote ("macros" "rt"))) (d #t) (k 2)))) (h "1q37vk9jq1dwg9284dbw7b3xsyk2fpc1ixy68jaq0y3ygnzbn1wb") (r "1.65.0")))

(define-public crate-futures-bounded-0.2.0 (c (n "futures-bounded") (v "0.2.0") (d (list (d (n "futures-timer") (r "^3.0.2") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.28") (d #t) (k 0)) (d (n "tokio") (r "^1.33.0") (f (quote ("macros" "rt"))) (d #t) (k 2)))) (h "0g9fvjn9l32vxs9mn6v137sbp7bww1xamhz19lcg0n7lpqn77g05") (r "1.73.0")))

(define-public crate-futures-bounded-0.2.1 (c (n "futures-bounded") (v "0.2.1") (d (list (d (n "futures-timer") (r "^3.0.2") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.29") (d #t) (k 0)) (d (n "tokio") (r "^1.33.0") (f (quote ("macros" "rt"))) (d #t) (k 2)))) (h "08nml9z7ciin28c312496mdk3ad7i06n1g9n2ip3qnqxwz1pnasa") (r "1.73.0")))

(define-public crate-futures-bounded-0.2.2 (c (n "futures-bounded") (v "0.2.2") (d (list (d (n "futures") (r "^0.3.28") (d #t) (k 2)) (d (n "futures-timer") (r "^3.0.2") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.29") (d #t) (k 0)) (d (n "tokio") (r "^1.34.0") (f (quote ("macros" "rt" "sync"))) (d #t) (k 2)))) (h "1iy98qm5nj7as0jdjyz0zyjw0nf4qiwgal1fd4vvlwv712fbxg23") (r "1.73.0")))

(define-public crate-futures-bounded-0.2.3 (c (n "futures-bounded") (v "0.2.3") (d (list (d (n "futures") (r "^0.3.28") (d #t) (k 2)) (d (n "futures-timer") (r "^3.0.2") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.29") (d #t) (k 0)) (d (n "tokio") (r "^1.34.0") (f (quote ("macros" "rt" "sync"))) (d #t) (k 2)))) (h "0wz1rrii9wklc6ja7dkcblja6vzq82mz87ry7ppriq84q567gqp1") (r "1.73.0")))

