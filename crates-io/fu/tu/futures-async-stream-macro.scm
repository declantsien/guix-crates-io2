(define-module (crates-io fu tu futures-async-stream-macro) #:use-module (crates-io))

(define-public crate-futures-async-stream-macro-0.0.0 (c (n "futures-async-stream-macro") (v "0.0.0") (h "06hz06sbqrpnfd97s9xzg65q7pjxw8xxd3iw5g9dfb0n1rp899fl") (y #t)))

(define-public crate-futures-async-stream-macro-0.1.0-alpha.1 (c (n "futures-async-stream-macro") (v "0.1.0-alpha.1") (d (list (d (n "proc-macro2") (r "^0.4.13") (d #t) (k 0)) (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "syn") (r "^0.15.41") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "0bhq5bwr29k8n3jpqcilr8fcc1kz04hav4ri0hybp944f9knrgab")))

(define-public crate-futures-async-stream-macro-0.1.0-alpha.2 (c (n "futures-async-stream-macro") (v "0.1.0-alpha.2") (d (list (d (n "proc-macro2") (r "^0.4.13") (d #t) (k 0)) (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "syn") (r "^0.15.41") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "044gpd6xasvrgnixf91czkvxkv3p431gbz8l9irr0698k6hymcdw")))

(define-public crate-futures-async-stream-macro-0.1.0-alpha.3 (c (n "futures-async-stream-macro") (v "0.1.0-alpha.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "1mry7k0s3wmhxl21wnnwkvx6xngx49xz5whr21kg103d6fa2jpmw")))

(define-public crate-futures-async-stream-macro-0.1.0-alpha.4 (c (n "futures-async-stream-macro") (v "0.1.0-alpha.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "061hjyxlxckz5nvp5y5hk1nqk1hana0lf8wfj11pxaa8nz1vrg0b")))

(define-public crate-futures-async-stream-macro-0.1.0-alpha.5 (c (n "futures-async-stream-macro") (v "0.1.0-alpha.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "1ni9mliwzbgd3shic5k4v87vaf15yp0v2j4y63ywfagaq88mhrn7")))

(define-public crate-futures-async-stream-macro-0.1.0-alpha.6 (c (n "futures-async-stream-macro") (v "0.1.0-alpha.6") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "0gf7g6whazwp1c0nsjjh7gd3a00mqqz1yqikd1bkidyd85ijhv9b")))

(define-public crate-futures-async-stream-macro-0.1.0-alpha.7 (c (n "futures-async-stream-macro") (v "0.1.0-alpha.7") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "01g2l8m6d46kway7zw3s1mg0qrf25im4yjjfm8sfd174d227q718")))

(define-public crate-futures-async-stream-macro-0.1.0 (c (n "futures-async-stream-macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "1r0cr8qcbgvg1a0kpv6ysy8s574dz5nlwff2rmcinxninv8d5grs")))

(define-public crate-futures-async-stream-macro-0.1.1 (c (n "futures-async-stream-macro") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "10rqdd21xv1nchvd234df6zps4iy47py93f00cy3fam1p8h1bl7x")))

(define-public crate-futures-async-stream-macro-0.1.2 (c (n "futures-async-stream-macro") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "1gql1ksldjjgv86w3ap87dw9zcrzx6jhn3lrz0wfq9da31p1yxv7")))

(define-public crate-futures-async-stream-macro-0.1.3 (c (n "futures-async-stream-macro") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "08psfjzf3y0wa16waml7kx8d63n8hy15d8wa4sa38ds73p2rni81")))

(define-public crate-futures-async-stream-macro-0.1.4 (c (n "futures-async-stream-macro") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "1p8ndyfdzsd1wfn1ggyx0i68zn7scb9qqhi34iph5idi9kb7hbnv")))

(define-public crate-futures-async-stream-macro-0.1.5 (c (n "futures-async-stream-macro") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "1380j12aqq5psngl4y5yg75j8inwhs2fp1rhaxiwfcrwamabnb0w")))

(define-public crate-futures-async-stream-macro-0.2.0 (c (n "futures-async-stream-macro") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "1hhfl2gdglb6km4kpdl8g3slvyni119j21rsgnc3p8nmhzw3rp5n")))

(define-public crate-futures-async-stream-macro-0.2.1 (c (n "futures-async-stream-macro") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.23") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "131jvmiky1r1rixl7hw1bnmvbh19k2icd9p6ssbz7l2yrfjlabap")))

(define-public crate-futures-async-stream-macro-0.2.2 (c (n "futures-async-stream-macro") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.23") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "0azv752j45q82k4spxcyxv24kzladialzd7id3i7l0rr6f3dq9cv")))

(define-public crate-futures-async-stream-macro-0.2.3 (c (n "futures-async-stream-macro") (v "0.2.3") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.23") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "0ln952wnm1f4c4wkl5dxi962yvph84lcq8f790914gcxgdxmd9xc")))

(define-public crate-futures-async-stream-macro-0.2.4 (c (n "futures-async-stream-macro") (v "0.2.4") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.23") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "03nflygkb4gzr1g2iw4nrswyb8qa9pdmspkb0qw1mrg0cgmdaj7m")))

(define-public crate-futures-async-stream-macro-0.2.5 (c (n "futures-async-stream-macro") (v "0.2.5") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.23") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "1435n113nn1nxp0w1a7qlfh11fpvc4dxgjhqk84r1rpbpkdzq5k7")))

(define-public crate-futures-async-stream-macro-0.2.6 (c (n "futures-async-stream-macro") (v "0.2.6") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.84") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "06dzk8g14998hhkvyjpsc7nfxcb5a411zjpgflclhdmjqzalcrpv")))

(define-public crate-futures-async-stream-macro-0.2.7 (c (n "futures-async-stream-macro") (v "0.2.7") (d (list (d (n "proc-macro2") (r "^1.0.60") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.84") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "105z2f8w6r5y6fnfm7dibc40cd3cs1sx1fjyp842i3fw0vp4haya")))

(define-public crate-futures-async-stream-macro-0.2.8 (c (n "futures-async-stream-macro") (v "0.2.8") (d (list (d (n "proc-macro2") (r "^1.0.60") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.1") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "0fbngv9n6rk7c7wk8y0g9ydrcf8jdb013cwpw85h6h5rnpydnj3s")))

(define-public crate-futures-async-stream-macro-0.2.9 (c (n "futures-async-stream-macro") (v "0.2.9") (d (list (d (n "proc-macro2") (r "^1.0.60") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.1") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "0n772w7yz0xlbw6150dpgdyk4bs8ykqfdvckq0z8mjy890yw3wjx")))

(define-public crate-futures-async-stream-macro-0.2.10 (c (n "futures-async-stream-macro") (v "0.2.10") (d (list (d (n "proc-macro2") (r "^1.0.60") (d #t) (k 0)) (d (n "quote") (r "^1.0.25") (d #t) (k 0)) (d (n "syn") (r "^2.0.1") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "1k1k3b4yfdisg25azvvmpwyywdxasazb91jgl0wvsfsxfzrcwcvl")))

(define-public crate-futures-async-stream-macro-0.2.11 (c (n "futures-async-stream-macro") (v "0.2.11") (d (list (d (n "proc-macro2") (r "^1.0.60") (d #t) (k 0)) (d (n "quote") (r "^1.0.25") (d #t) (k 0)) (d (n "syn") (r "^2.0.1") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "023dl5ksnbs35wfr6f8wp7sh11zqjjhnh9w6d3mi1lfvpp85xi2s")))

