(define-module (crates-io fu tu futura) #:use-module (crates-io))

(define-public crate-futura-0.2.2 (c (n "futura") (v "0.2.2") (d (list (d (n "futures") (r "^0.3.24") (d #t) (k 0)) (d (n "futures-timer") (r "^3.0.2") (d #t) (k 0)) (d (n "tracing") (r "^0.1.36") (d #t) (k 0)))) (h "1ci2rklk0j4qz0mg69cy6yv7jw2s2pn1s6c4864q9gsxzx7z0pnc")))

