(define-module (crates-io fu tu futures-crypto) #:use-module (crates-io))

(define-public crate-futures-crypto-0.1.0 (c (n "futures-crypto") (v "0.1.0") (d (list (d (n "bytes") (r "^0.4.5") (d #t) (k 0)) (d (n "futures") (r "^0.1.17") (d #t) (k 0)) (d (n "itertools") (r "^0.7.2") (d #t) (k 2)) (d (n "openssl") (r "^0.9.20") (d #t) (k 0)) (d (n "quickcheck") (r "^0.4.1") (d #t) (k 2)))) (h "0gcjgwfwwz3dkfcv5qby7yy0n16wd1w34m1rbkyszlailzq7d7ag")))

