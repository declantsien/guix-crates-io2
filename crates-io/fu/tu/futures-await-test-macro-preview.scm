(define-module (crates-io fu tu futures-await-test-macro-preview) #:use-module (crates-io))

(define-public crate-futures-await-test-macro-preview-0.3.0-alpha.16 (c (n "futures-await-test-macro-preview") (v "0.3.0-alpha.16") (d (list (d (n "proc-macro2") (r "^0.4.13") (d #t) (k 0)) (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "syn") (r "^0.15.0") (f (quote ("clone-impls" "full" "parsing" "printing" "proc-macro"))) (k 0)))) (h "0sp14wmwkr73md2857mn343aswmn9g833pbcml27dj6j3k21jc9d")))

(define-public crate-futures-await-test-macro-preview-0.3.0-alpha.19 (c (n "futures-await-test-macro-preview") (v "0.3.0-alpha.19") (d (list (d (n "proc-macro2") (r "^1.0.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("clone-impls" "full" "parsing" "printing" "proc-macro"))) (k 0)))) (h "1z1jns8ing2l24wsqr0iaqmma9ah5giav3rvy8dk2bmwld330sl6")))

