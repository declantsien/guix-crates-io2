(define-module (crates-io fu tu futures-async-runtime) #:use-module (crates-io))

(define-public crate-futures-async-runtime-0.2.1 (c (n "futures-async-runtime") (v "0.2.1") (d (list (d (n "futures-core") (r "^0.2.1") (k 0)) (d (n "futures-stable") (r "^0.2.1") (k 0)))) (h "1xdj5pghiahajzp21sjvzr5cfi5zi4l60q492hrkpg5wz6bd3f5s") (f (quote (("std" "futures-core/std" "futures-stable/std") ("nightly" "futures-core/nightly" "futures-stable/nightly") ("default" "std"))))))

