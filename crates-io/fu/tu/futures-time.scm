(define-module (crates-io fu tu futures-time) #:use-module (crates-io))

(define-public crate-futures-time-0.0.0 (c (n "futures-time") (v "0.0.0") (d (list (d (n "async-io") (r "^1.6.0") (d #t) (k 0)) (d (n "futures-core") (r "^0.3.19") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2.8") (d #t) (k 0)))) (h "05cma56zyb98wnzz3q6ss7c3jr8l2qvhc8zhv0h3sqlsg1lygpv7")))

(define-public crate-futures-time-0.1.0 (c (n "futures-time") (v "0.1.0") (d (list (d (n "async-io") (r "^1.6.0") (d #t) (k 0)) (d (n "futures-core") (r "^0.3.19") (d #t) (k 0)) (d (n "futures-lite") (r "^1.12.0") (d #t) (k 2)) (d (n "pin-project-lite") (r "^0.2.8") (d #t) (k 0)))) (h "02lgsilmgnzg3mjzsrbi094z260l5gw44kz85ffn17pl2iqgrw4z")))

(define-public crate-futures-time-0.1.1 (c (n "futures-time") (v "0.1.1") (d (list (d (n "async-io") (r "^1.6.0") (d #t) (k 0)) (d (n "futures-core") (r "^0.3.19") (d #t) (k 0)) (d (n "futures-lite") (r "^1.12.0") (d #t) (k 2)) (d (n "pin-project-lite") (r "^0.2.8") (d #t) (k 0)))) (h "1jsivh1zjafb29zw9jba472fv0mnwv5cfwsp7anzdij7hn7h8xh7")))

(define-public crate-futures-time-0.2.0 (c (n "futures-time") (v "0.2.0") (d (list (d (n "async-io") (r "^1.6.0") (d #t) (k 0)) (d (n "futures-core") (r "^0.3.19") (d #t) (k 0)) (d (n "futures-lite") (r "^1.12.0") (d #t) (k 2)) (d (n "pin-project-lite") (r "^0.2.8") (d #t) (k 0)))) (h "179j8iqqjp0sj0rfnz6nds564ikzipk9bwk4n6rx7g6fngvclbmx")))

(define-public crate-futures-time-0.3.0 (c (n "futures-time") (v "0.3.0") (d (list (d (n "async-io") (r "^1.6.0") (d #t) (k 0)) (d (n "futures-core") (r "^0.3.19") (d #t) (k 0)) (d (n "futures-lite") (r "^1.12.0") (d #t) (k 2)) (d (n "pin-project-lite") (r "^0.2.8") (d #t) (k 0)))) (h "13xa6m06l1vyrpai69whzmf2dmypwddmwq3s660v0iliw5lcyc5i")))

(define-public crate-futures-time-0.3.1 (c (n "futures-time") (v "0.3.1") (d (list (d (n "async-channel") (r "^1.6.1") (d #t) (k 0)) (d (n "async-io") (r "^1.6.0") (d #t) (k 0)) (d (n "futures-core") (r "^0.3.19") (d #t) (k 0)) (d (n "futures-lite") (r "^1.12.0") (d #t) (k 2)) (d (n "pin-project-lite") (r "^0.2.8") (d #t) (k 0)))) (h "15k2q0w2ily0ivi987slhxnz4fav7bq3fz4mbw655x615b31i9j9")))

(define-public crate-futures-time-1.0.0 (c (n "futures-time") (v "1.0.0") (d (list (d (n "async-channel") (r "^1.6.1") (d #t) (k 0)) (d (n "async-io") (r "^1.6.0") (d #t) (k 0)) (d (n "futures-core") (r "^0.3.19") (d #t) (k 0)) (d (n "futures-lite") (r "^1.12.0") (d #t) (k 2)) (d (n "pin-project-lite") (r "^0.2.8") (d #t) (k 0)))) (h "1c7d740k4gwi6ygp679fk3ahymj8n5c9wfnnkvknazqqgkhp6djm")))

(define-public crate-futures-time-2.0.0 (c (n "futures-time") (v "2.0.0") (d (list (d (n "async-channel") (r "^1.6.1") (d #t) (k 0)) (d (n "async-io") (r "^1.6.0") (d #t) (k 0)) (d (n "futures-core") (r "^0.3.19") (d #t) (k 0)) (d (n "futures-lite") (r "^1.12.0") (d #t) (k 2)) (d (n "pin-project-lite") (r "^0.2.8") (d #t) (k 0)))) (h "0rn6cl3l8vgkq317ny6mh71mkimhmbhprdc89r8jn6fydiq1yxli")))

(define-public crate-futures-time-3.0.0 (c (n "futures-time") (v "3.0.0") (d (list (d (n "async-channel") (r "^1.6.1") (d #t) (k 0)) (d (n "async-io") (r "^1.6.0") (d #t) (k 0)) (d (n "futures-core") (r "^0.3.19") (d #t) (k 0)) (d (n "futures-lite") (r "^1.12.0") (d #t) (k 2)) (d (n "pin-project-lite") (r "^0.2.8") (d #t) (k 0)))) (h "12km02vdlw9a4y7kg84v4p9fr12fvi3x2qnnyzjiz214d0x8a134")))

