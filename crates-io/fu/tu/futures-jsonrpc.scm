(define-module (crates-io fu tu futures-jsonrpc) #:use-module (crates-io))

(define-public crate-futures-jsonrpc-0.1.0 (c (n "futures-jsonrpc") (v "0.1.0") (d (list (d (n "futures") (r "^0.1.26") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)))) (h "1cxy1kmczmx7iz6qrgmrwlvngdkxw6d9fy5hwjyzh2rpp7siz4kd")))

(define-public crate-futures-jsonrpc-0.1.1 (c (n "futures-jsonrpc") (v "0.1.1") (d (list (d (n "futures") (r "^0.1.26") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)) (d (n "uuid") (r "^0.7.4") (f (quote ("v4"))) (d #t) (k 0)))) (h "1wkzbd8w4p18nq25lavi6kc0wfqj7agilkhi1cix5v08208hf8zd")))

(define-public crate-futures-jsonrpc-0.1.2 (c (n "futures-jsonrpc") (v "0.1.2") (d (list (d (n "futures") (r "^0.1.26") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)) (d (n "uuid") (r "^0.7.4") (f (quote ("v4"))) (d #t) (k 0)))) (h "0nankms5wjv6frh2d06rpsl2pyiiznwllx7q31zdzs53hi981s1q")))

(define-public crate-futures-jsonrpc-0.1.3 (c (n "futures-jsonrpc") (v "0.1.3") (d (list (d (n "futures") (r "^0.1.26") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)) (d (n "uuid") (r "^0.7.4") (f (quote ("v4"))) (d #t) (k 0)))) (h "1l53pal8javjvjyfajz0gq653113zpd1wr3j4rzg1fxzlnrz4r0c")))

(define-public crate-futures-jsonrpc-0.2.0 (c (n "futures-jsonrpc") (v "0.2.0") (d (list (d (n "futures") (r "^0.1.26") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)) (d (n "uuid") (r "^0.7.4") (f (quote ("v4"))) (d #t) (k 0)))) (h "0ghnz19n9jzksbbcpfsrqid7bagrwsjyw7i2dxf4adaqmnlnk5pc")))

(define-public crate-futures-jsonrpc-0.2.1 (c (n "futures-jsonrpc") (v "0.2.1") (d (list (d (n "futures") (r "^0.1.26") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)) (d (n "uuid") (r "^0.7.4") (f (quote ("v4"))) (d #t) (k 0)))) (h "1imi4jivfqfnqfqxll5qp2fbsgvy92i1ck7c7f2mq60vl7y7nbgx")))

