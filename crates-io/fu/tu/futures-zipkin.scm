(define-module (crates-io fu tu futures-zipkin) #:use-module (crates-io))

(define-public crate-futures-zipkin-0.1.0 (c (n "futures-zipkin") (v "0.1.0") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "zipkin") (r "^0.1.0") (d #t) (k 0)))) (h "1kf54rchy24678zmn3p82iz5sf90wy05hp2i52phfssqf6jmlc6i")))

(define-public crate-futures-zipkin-0.2.0 (c (n "futures-zipkin") (v "0.2.0") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "zipkin") (r "^0.1.0") (d #t) (k 0)))) (h "07vrmrmarzv7n92v850h1bifymxq2hxkws4ykhncqanar817fm56")))

(define-public crate-futures-zipkin-0.3.0 (c (n "futures-zipkin") (v "0.3.0") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "zipkin") (r "^0.2.0") (d #t) (k 0)))) (h "1frdvp2r56qypp4di28v08k6rsl34zinbsnlmpn67wgz0s157b36")))

(define-public crate-futures-zipkin-0.4.0 (c (n "futures-zipkin") (v "0.4.0") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "zipkin") (r "^0.3.0") (d #t) (k 0)))) (h "00qmyy13g6yy2k7zbc58b3xxz0sbc0f83n3qzvp9c5xhrsmg6pmy")))

