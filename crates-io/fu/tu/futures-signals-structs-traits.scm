(define-module (crates-io fu tu futures-signals-structs-traits) #:use-module (crates-io))

(define-public crate-futures-signals-structs-traits-0.1.0 (c (n "futures-signals-structs-traits") (v "0.1.0") (h "1bmnv0vr23w41lvkjhg4a9fb6swn8pb9sljfa3xrgmbw494zcl5z")))

(define-public crate-futures-signals-structs-traits-0.2.0 (c (n "futures-signals-structs-traits") (v "0.2.0") (d (list (d (n "futures-signals") (r "^0.3.15") (d #t) (k 0)))) (h "0f1il0ni108nsxwgkxcwp3dpfhl9vylhaq50malx0qpyri48w67a")))

