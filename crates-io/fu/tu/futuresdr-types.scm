(define-module (crates-io fu tu futuresdr-types) #:use-module (crates-io))

(define-public crate-futuresdr-types-0.0.9 (c (n "futuresdr-types") (v "0.0.9") (d (list (d (n "dyn-clone") (r "^1.0.9") (d #t) (k 0)) (d (n "flexbuffers") (r "^2.0.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "19apb4k2kwy4f721ysf14l5l2yfcg9yfba7lib7bfn2y283rxfi9")))

(define-public crate-futuresdr-types-0.0.10 (c (n "futuresdr-types") (v "0.0.10") (d (list (d (n "dyn-clone") (r "^1.0.9") (d #t) (k 0)) (d (n "flexbuffers") (r "^2.0.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "1yfsyls8gnxqh0vjw3qnlwhs8cznyvjng5qkf6h9qqq624wb8c5s")))

(define-public crate-futuresdr-types-0.0.11 (c (n "futuresdr-types") (v "0.0.11") (d (list (d (n "dyn-clone") (r "^1.0.9") (d #t) (k 0)) (d (n "flexbuffers") (r "^2.0.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "0hax3frmz7nldp7lq1iycsf7gbjzyywdx0vd3y2rhz093ijafqns")))

