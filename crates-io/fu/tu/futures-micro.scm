(define-module (crates-io fu tu futures-micro) #:use-module (crates-io))

(define-public crate-futures-micro-0.1.0 (c (n "futures-micro") (v "0.1.0") (d (list (d (n "futures-lite") (r "^0.1") (d #t) (k 2)))) (h "1lfa3fpjnnbr20n28lj2y07ddsrw4r0xap22y7nxl4vlhfgn1ygw")))

(define-public crate-futures-micro-0.1.1 (c (n "futures-micro") (v "0.1.1") (d (list (d (n "futures-lite") (r "^0.1") (d #t) (k 2)))) (h "04b1dd7zdc6r8lski29a4ih8lwap8ila46bmfmdd8m04zhh71lmh")))

(define-public crate-futures-micro-0.2.0 (c (n "futures-micro") (v "0.2.0") (d (list (d (n "futures-lite") (r "^0.1") (d #t) (k 2)))) (h "0m5anxnnsm8vihbrr149dnz5hmi3lfn7d5s1g3silxd76p77amvn")))

(define-public crate-futures-micro-0.3.0 (c (n "futures-micro") (v "0.3.0") (d (list (d (n "futures-lite") (r "^0.1") (d #t) (k 2)))) (h "1gwwbcbfq6qrh8g9z3jqzn8hccygvl6kshs4f6ga6mvdqnkx3kqf")))

(define-public crate-futures-micro-0.3.1 (c (n "futures-micro") (v "0.3.1") (d (list (d (n "futures-lite") (r "^0.1") (d #t) (k 2)))) (h "0nm9d72367i0xxqc6gjpphhgkhc8m57jj46i5h482mawwmdk5sb1")))

(define-public crate-futures-micro-0.4.0 (c (n "futures-micro") (v "0.4.0") (d (list (d (n "futures-lite") (r "^0.1") (d #t) (k 2)))) (h "1b89xvl58nhczbzy5041yzrdwz8hlrqvg4ywlsr64zfj0l4dkw8h")))

(define-public crate-futures-micro-0.5.0 (c (n "futures-micro") (v "0.5.0") (d (list (d (n "futures-lite") (r "^0.1") (d #t) (k 2)) (d (n "pin-project-lite") (r "^0.2.4") (d #t) (k 0)))) (h "09n8d1qnpk6mjpnv338wkbgyppvd4aygfddwlwb8pmlk6m5jcq5l")))

(define-public crate-futures-micro-1.0.0-rc0 (c (n "futures-micro") (v "1.0.0-rc0") (d (list (d (n "futures-lite") (r "^0.1") (d #t) (k 2)) (d (n "pin-project-lite") (r "^0.2.4") (d #t) (k 0)))) (h "0swvhhj4dswsjwmmsn951mi9xdwn9055ah7f29hx0a9rdqjh5w1x")))

