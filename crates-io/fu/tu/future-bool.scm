(define-module (crates-io fu tu future-bool) #:use-module (crates-io))

(define-public crate-future-bool-0.1.0 (c (n "future-bool") (v "0.1.0") (d (list (d (n "tokio") (r "^1.9") (f (quote ("macros" "rt-multi-thread" "sync" "time"))) (d #t) (k 0)))) (h "0vyipwr0amws0h8yiwgsv4bzazk07c4yzmjayqp5hljdlygbcr70") (y #t)))

(define-public crate-future-bool-0.1.1 (c (n "future-bool") (v "0.1.1") (d (list (d (n "tokio") (r "^1.9") (f (quote ("macros" "rt-multi-thread" "sync" "time"))) (d #t) (k 0)))) (h "1daf6bdkdp45ik2rwm5753pxibn8kiq0af45min5z1iwf9j25cj8")))

