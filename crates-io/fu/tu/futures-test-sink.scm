(define-module (crates-io fu tu futures-test-sink) #:use-module (crates-io))

(define-public crate-futures-test-sink-0.1.0 (c (n "futures-test-sink") (v "0.1.0") (d (list (d (n "async-task") (r "^1.3") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pin-project") (r "^0.4") (d #t) (k 0)))) (h "16bd1gij1q38aj7x7dz9k7i6fw9vy3s2i25sqr38x6pqywg3ligb")))

(define-public crate-futures-test-sink-0.1.1 (c (n "futures-test-sink") (v "0.1.1") (d (list (d (n "async-task") (r "^1.3") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 0)))) (h "0yh1l3hpygfklnzb82lbjylra7khnjixai0gz46hx7qxmaiaapa4")))

