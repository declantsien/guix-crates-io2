(define-module (crates-io fu tu futures-prio) #:use-module (crates-io))

(define-public crate-futures-prio-0.1.0 (c (n "futures-prio") (v "0.1.0") (d (list (d (n "futures-executor-preview") (r "^0.3.0-alpha.19") (d #t) (k 2)) (d (n "pin-project") (r "^0.4.3") (d #t) (k 0)))) (h "1b00y7lh3r6bcpgv93rc0khmfsf6q65jrfhgs8cbf05kmm8pbbgy")))

(define-public crate-futures-prio-0.1.1 (c (n "futures-prio") (v "0.1.1") (d (list (d (n "futures-executor-preview") (r "^0.3.0-alpha.19") (d #t) (k 2)) (d (n "pin-project") (r "^0.4.3") (d #t) (k 0)))) (h "0674fm6lckzl4md024klxfz6wgrwclkz6aylqd0p4pncrg1mgq1a")))

