(define-module (crates-io fu tu future_gadget_laboratory) #:use-module (crates-io))

(define-public crate-future_gadget_laboratory-0.0.1048596 (c (n "future_gadget_laboratory") (v "0.0.1048596") (d (list (d (n "proc-macro2") (r "^1.0.83") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.66") (f (quote ("full"))) (d #t) (k 0)))) (h "0rzwqalpmw0gviy7d67k6mcvwwikg5ap0dh98b3q81mlcd7dh4km") (y #t)))

(define-public crate-future_gadget_laboratory-0.1.1048596 (c (n "future_gadget_laboratory") (v "0.1.1048596") (d (list (d (n "proc-macro2") (r "^1.0.83") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.66") (f (quote ("full"))) (d #t) (k 0)))) (h "0l6wwcw2rpddjb7z57ipld4f471lvcfyzrlwgqwsm8csylj9vrpz")))

