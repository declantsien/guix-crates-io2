(define-module (crates-io fu tu futures-promises) #:use-module (crates-io))

(define-public crate-futures-promises-0.1.0 (c (n "futures-promises") (v "0.1.0") (d (list (d (n "futures") (r "^0.1.26") (d #t) (k 0)))) (h "0i0xfi69fzr3066slnic6hxm2kx8gqf6wdaf08r394dlmrdkqnbs")))

(define-public crate-futures-promises-0.1.1 (c (n "futures-promises") (v "0.1.1") (d (list (d (n "futures") (r "^0.1.26") (d #t) (k 0)))) (h "1829wnw26y42pf9s1zf9m6f56ghk6rwfqap9yp132ag7vrkzk4ga")))

(define-public crate-futures-promises-0.1.2 (c (n "futures-promises") (v "0.1.2") (d (list (d (n "futures") (r "^0.1.26") (d #t) (k 0)))) (h "0l9y0rfb9w3mwmd5xpkcqxy9z10spc7sh7j12rnh1cl3aqn8n3m4")))

(define-public crate-futures-promises-0.1.3 (c (n "futures-promises") (v "0.1.3") (d (list (d (n "futures") (r "^0.1.26") (d #t) (k 0)))) (h "15phsfnnq1hg97p0rb72hjyvv3wyq6zw885xadd1xbc7lq29q5nq")))

(define-public crate-futures-promises-0.1.4 (c (n "futures-promises") (v "0.1.4") (d (list (d (n "futures") (r "^0.1.26") (d #t) (k 0)))) (h "19i3bvn3p79bd9mc7dcx6l1nhza22ynnd2q359gq0qzaj93ls2bq")))

(define-public crate-futures-promises-0.1.5 (c (n "futures-promises") (v "0.1.5") (d (list (d (n "futures") (r "^0.1.26") (d #t) (k 0)))) (h "0h4dvah5idrrl5s6p6vami9s81w6hbrq4ih3vr78mwkhvssfi89m")))

(define-public crate-futures-promises-0.1.6 (c (n "futures-promises") (v "0.1.6") (d (list (d (n "futures") (r "^0.1.26") (d #t) (k 0)))) (h "0d7wmd72l5bwndsxz8fbpw7djxl4hpnnizgwp3r16gqbwmlj72nn")))

(define-public crate-futures-promises-0.1.7 (c (n "futures-promises") (v "0.1.7") (d (list (d (n "futures") (r "^0.1.27") (d #t) (k 0)))) (h "1z83rjph3zdwrlwqkm0mipzqkb3nh7ksd6cph7sqvqzcank572qs")))

(define-public crate-futures-promises-0.1.8 (c (n "futures-promises") (v "0.1.8") (d (list (d (n "futures") (r "^0.1.27") (d #t) (k 0)))) (h "1r2h0khq83414yplahl3kg13cjddg6v70i7jacz2l1z5alp818bm")))

(define-public crate-futures-promises-0.1.9 (c (n "futures-promises") (v "0.1.9") (d (list (d (n "futures") (r "^0.1.28") (d #t) (k 0)))) (h "0ym4xydidfkk0d123w6fyzb9q3wbqyqc7bkgccx4p1vvy3861axb")))

(define-public crate-futures-promises-0.1.10 (c (n "futures-promises") (v "0.1.10") (d (list (d (n "futures") (r "^0.1.28") (d #t) (k 0)))) (h "12j03xyw8p845mr27r8m21qpkqzn22m04h73bz6r43mniwzddr7s")))

(define-public crate-futures-promises-0.1.11 (c (n "futures-promises") (v "0.1.11") (d (list (d (n "futures") (r "^0.1.28") (d #t) (k 0)))) (h "0afn0zr6v0lb7i8j9ls855dv70fa9g789fkrjqbz0i65678j2a0y")))

