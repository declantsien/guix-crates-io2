(define-module (crates-io fu tu futures-length-delimited-frame) #:use-module (crates-io))

(define-public crate-futures-length-delimited-frame-0.1.0 (c (n "futures-length-delimited-frame") (v "0.1.0") (d (list (d (n "futures-core") (r "^0.3") (k 0)) (d (n "futures-io") (r "^0.3") (k 0)) (d (n "futures-sink") (r "^0.3") (k 0)) (d (n "pin-project-lite") (r "^0.2") (k 0)) (d (n "futures-executor") (r "^0.3") (d #t) (k 2)) (d (n "futures-util") (r "^0.3") (f (quote ("io" "sink"))) (d #t) (k 2)))) (h "0hpflgf3wilkyrxd61qsd250ags4c2shxgndg8d6chh71g6zz242")))

