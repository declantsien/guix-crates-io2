(define-module (crates-io fu tu futures-utils-wasm) #:use-module (crates-io))

(define-public crate-futures-utils-wasm-0.1.0 (c (n "futures-utils-wasm") (v "0.1.0") (h "1sapb9grci03nlssfgja1w1d8z8gz4kkgzj5nmcal0af0q7jn0a2") (f (quote (("std" "alloc") ("default" "alloc") ("alloc")))) (r "1.36")))

