(define-module (crates-io fu tu futures-scopes) #:use-module (crates-io))

(define-public crate-futures-scopes-0.1.0 (c (n "futures-scopes") (v "0.1.0") (d (list (d (n "crossbeam-channel") (r "^0.5.6") (d #t) (k 0)) (d (n "futures") (r "^0.3.24") (f (quote ("thread-pool"))) (d #t) (k 0)) (d (n "pin-project") (r "^1.0.12") (d #t) (k 0)))) (h "0wlh4gchsgk7609z4k3dp08ci5sdlhw3ygv31dpqrvb8l8wmsnsy") (y #t)))

(define-public crate-futures-scopes-0.1.1 (c (n "futures-scopes") (v "0.1.1") (d (list (d (n "crossbeam-channel") (r "^0.5.6") (d #t) (k 0)) (d (n "futures") (r "^0.3.24") (f (quote ("thread-pool"))) (d #t) (k 0)) (d (n "pin-project") (r "^1.0.12") (d #t) (k 0)))) (h "06l7gynbgwd2q1gzwadkgymj1zydiwhn6gl69sp9khm7ipg14ycl")))

(define-public crate-futures-scopes-0.1.2 (c (n "futures-scopes") (v "0.1.2") (d (list (d (n "crossbeam-channel") (r "^0.5.6") (d #t) (k 0)) (d (n "dashmap") (r "^5.4.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.24") (f (quote ("thread-pool"))) (d #t) (k 0)) (d (n "pin-project") (r "^1.0.12") (d #t) (k 0)))) (h "0rk8qr19ln7s16sq6lb0wjm0mxm7rhjz9cihlsn232rnskyj236g")))

(define-public crate-futures-scopes-0.1.3 (c (n "futures-scopes") (v "0.1.3") (d (list (d (n "crossbeam-channel") (r "^0.5.6") (d #t) (k 0)) (d (n "dashmap") (r "^5.4.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.24") (f (quote ("thread-pool"))) (d #t) (k 0)) (d (n "pin-project") (r "^1.0.12") (d #t) (k 0)))) (h "0i38k9j5pi05jb7xqgsf5prb6791jhlbk091817in7x4194naz1p")))

(define-public crate-futures-scopes-0.1.5 (c (n "futures-scopes") (v "0.1.5") (d (list (d (n "crossbeam-channel") (r "^0.5.6") (d #t) (k 0)) (d (n "dashmap") (r "^5.4.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.24") (f (quote ("thread-pool"))) (d #t) (k 0)) (d (n "pin-project") (r "^1.0.12") (d #t) (k 0)))) (h "128778lpgh8dx4w8wliixrrwmc7fww2k1ws3fsg6fkrqq64sjv4w")))

(define-public crate-futures-scopes-0.1.6 (c (n "futures-scopes") (v "0.1.6") (d (list (d (n "crossbeam-channel") (r "^0.5.6") (d #t) (k 0)) (d (n "dashmap") (r "^5.4.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.24") (f (quote ("thread-pool"))) (d #t) (k 0)) (d (n "pin-project") (r "^1.0.12") (d #t) (k 0)))) (h "1qmzfiy4c2i90cclilk9wj8cab4n9205mh07gc2slrz84vd4z4k1")))

(define-public crate-futures-scopes-0.1.7 (c (n "futures-scopes") (v "0.1.7") (d (list (d (n "crossbeam-channel") (r "^0.5.6") (o #t) (d #t) (k 0)) (d (n "dashmap") (r "^5.4.0") (o #t) (d #t) (k 0)) (d (n "futures") (r "^0.3.24") (f (quote ("std"))) (k 0)) (d (n "pin-project") (r "^1.0.12") (d #t) (k 0)))) (h "0iw5jzkzj8b9snxfysbaav9rlxm5in7cvb7brd15g4h32p6ck5l7") (f (quote (("local") ("default" "local" "relay")))) (s 2) (e (quote (("relay" "dep:dashmap" "dep:crossbeam-channel"))))))

(define-public crate-futures-scopes-0.2.0 (c (n "futures-scopes") (v "0.2.0") (d (list (d (n "crossbeam-channel") (r "^0.5.6") (o #t) (d #t) (k 0)) (d (n "dashmap") (r "^5.4.0") (o #t) (d #t) (k 0)) (d (n "futures") (r "^0.3.24") (f (quote ("std"))) (k 0)) (d (n "futures") (r "^0.3.24") (f (quote ("thread-pool"))) (d #t) (k 2)) (d (n "pin-project") (r "^1.0.12") (d #t) (k 0)))) (h "0k5jwfhyzvs1hlzkjfwajssnq5crxfgk3i04rniz20z84wlg7p7z") (f (quote (("local") ("default" "local" "relay")))) (s 2) (e (quote (("relay" "dep:dashmap" "dep:crossbeam-channel"))))))

