(define-module (crates-io fu tu future-union-impl) #:use-module (crates-io))

(define-public crate-future-union-impl-0.1.0 (c (n "future-union-impl") (v "0.1.0") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "0wzqsdjby71wh4cq8bid2q2ddvk6581c7vmfr67d9jkw404lalar")))

