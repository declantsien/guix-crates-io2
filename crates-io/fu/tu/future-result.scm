(define-module (crates-io fu tu future-result) #:use-module (crates-io))

(define-public crate-future-result-0.0.0 (c (n "future-result") (v "0.0.0") (h "0hsyl7q0lgz3wp3xqpciyd5mjafjyspbw730yaplckw9kcv3igj4")))

(define-public crate-future-result-0.1.0 (c (n "future-result") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.36") (d #t) (k 0)) (d (n "futures") (r "^0.3.5") (d #t) (k 2)))) (h "0jjsn9pshss2gqv1xv50mmjirfaaifiry89l149gaxgh89fjqs54")))

