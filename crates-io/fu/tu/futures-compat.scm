(define-module (crates-io fu tu futures-compat) #:use-module (crates-io))

(define-public crate-futures-compat-0.0.0 (c (n "futures-compat") (v "0.0.0") (h "19852888a5fa0q23xx62vs7mpwy5slv56km4n7az11dhhx9igack")))

(define-public crate-futures-compat-0.0.1 (c (n "futures-compat") (v "0.0.1") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "futures-core") (r "^0.2") (d #t) (k 0)) (d (n "futures-io") (r "^0.2") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1.6") (d #t) (k 0)))) (h "1d76ayv2b27shqdk07zr9yqszvybqi0rrflj5zkmim2yk1jm8whx")))

