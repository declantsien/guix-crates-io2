(define-module (crates-io fu tu future) #:use-module (crates-io))

(define-public crate-future-0.1.0 (c (n "future") (v "0.1.0") (h "1nkwk92x5bsv1a9rvjdqdl5aarwdmljwlpr30djmizjcm61wa46j") (y #t)))

(define-public crate-future-0.1.1 (c (n "future") (v "0.1.1") (h "15mxvb4pry7k5v10zzfsvalpvcxnsnrymwdmsbfzzbr7ps37izpg") (y #t)))

(define-public crate-future-0.1.2 (c (n "future") (v "0.1.2") (h "0i94y0wdgbc2vnczykpb2jlv7j6w8ynbr6vjmfr5lzzzaxmb8f66") (y #t)))

(define-public crate-future-0.1.3 (c (n "future") (v "0.1.3") (h "1wn5yyb1s2j6fi5rpq92nh5yyi10lzfiks5395hh0m2jgq2lv4ni") (y #t)))

