(define-module (crates-io fu tu futures-spawn) #:use-module (crates-io))

(define-public crate-futures-spawn-0.1.0 (c (n "futures-spawn") (v "0.1.0") (d (list (d (n "futures") (r "^0.1") (k 0)) (d (n "tokio-core") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1cx9h8r1vvva7xl7xv7n390yi6ax9km2s6fpicy039c8fbf08ra6") (f (quote (("use_std" "futures/use_std") ("tokio" "tokio-core") ("default" "tokio" "use_std")))) (y #t)))

(define-public crate-futures-spawn-0.1.1 (c (n "futures-spawn") (v "0.1.1") (d (list (d (n "futures") (r "^0.1") (k 0)) (d (n "tokio-core") (r "^0.1") (o #t) (d #t) (k 0)))) (h "18f600364flb4rfxd5a8a1v5v6hz1m37n7n3rcfn08vdi2b8smd8") (f (quote (("use_std" "futures/use_std") ("tokio" "tokio-core") ("default" "tokio" "use_std"))))))

