(define-module (crates-io fu tu futures-fs) #:use-module (crates-io))

(define-public crate-futures-fs-0.0.0 (c (n "futures-fs") (v "0.0.0") (h "0bc0h0fyfb14g7c832b3z3y9klamd22pqva6n4qqil83wmwgp4bl")))

(define-public crate-futures-fs-0.0.1 (c (n "futures-fs") (v "0.0.1") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "futures") (r "^0.1.10") (d #t) (k 0)) (d (n "futures-cpupool") (r "^0.1") (d #t) (k 0)))) (h "0hi9bn83m9gqd1b2k6v2x7vcm5zd2sd7agihdf0zn87zlkdngh4g")))

(define-public crate-futures-fs-0.0.2 (c (n "futures-fs") (v "0.0.2") (d (list (d (n "bytes") (r "^0.4.3") (d #t) (k 0)) (d (n "futures") (r "^0.1.13") (d #t) (k 0)) (d (n "futures-cpupool") (r "^0.1") (d #t) (k 0)))) (h "15wqpq2wr7371bh2lvdmx1nrxjm5448vi0f0l8wyqa65yvz4dvgy")))

(define-public crate-futures-fs-0.0.3 (c (n "futures-fs") (v "0.0.3") (d (list (d (n "bytes") (r "^0.4.3") (d #t) (k 0)) (d (n "futures") (r "^0.1.13") (d #t) (k 0)) (d (n "futures-cpupool") (r "^0.1") (d #t) (k 0)))) (h "0fppbhwhdnbpzvdgcmi9qx5k5l4iblb2vay2j6f02d2xf1mwbs81")))

(define-public crate-futures-fs-0.0.4 (c (n "futures-fs") (v "0.0.4") (d (list (d (n "bytes") (r "^0.4.3") (d #t) (k 0)) (d (n "futures") (r "^0.1.13") (d #t) (k 0)) (d (n "futures-cpupool") (r "^0.1") (d #t) (k 0)))) (h "1f90lpkvbr49dww5x74a8cz16npj5dqmf5di9bncf61hxnv1hvm2")))

(define-public crate-futures-fs-0.0.5 (c (n "futures-fs") (v "0.0.5") (d (list (d (n "bytes") (r "^0.4.3") (d #t) (k 0)) (d (n "futures") (r "^0.1.13") (d #t) (k 0)) (d (n "futures-cpupool") (r "^0.1") (d #t) (k 0)))) (h "1m9nlpi5vcm0m9xhnbaqj2sxkzhf78jwp618mcdhb0rkc3mjm7wv")))

