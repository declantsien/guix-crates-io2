(define-module (crates-io fu tu futures-option) #:use-module (crates-io))

(define-public crate-futures-option-0.1.0 (c (n "futures-option") (v "0.1.0") (d (list (d (n "futures-preview") (r "^0.3.0-alpha.17") (d #t) (k 0)) (d (n "futures-preview") (r "^0.3.0-alpha.17") (f (quote ("nightly" "async-await"))) (d #t) (k 2)))) (h "0kbkvj4866n22xqjwdk4l3z52zrp1n84sjsd7q0lq7hb1w8p54fv")))

(define-public crate-futures-option-0.1.1 (c (n "futures-option") (v "0.1.1") (d (list (d (n "futures-preview") (r "^0.3.0-alpha.17") (d #t) (k 0)) (d (n "futures-preview") (r "^0.3.0-alpha.17") (f (quote ("nightly" "async-await"))) (d #t) (k 2)))) (h "09y5rb4pzancf28qh7g6ga3c4ms87a12q5h2b87dg5xvavrvznk4")))

(define-public crate-futures-option-0.1.2 (c (n "futures-option") (v "0.1.2") (d (list (d (n "futures-preview") (r "^0.3.0-alpha.18") (d #t) (k 0)) (d (n "futures-preview") (r "^0.3.0-alpha.18") (f (quote ("nightly" "async-await"))) (d #t) (k 2)))) (h "1gd6wwa7mb7h7s228mixb80bazkxcgbyk9y4rnfwjy3lvcm1gjaj")))

(define-public crate-futures-option-0.2.0 (c (n "futures-option") (v "0.2.0") (d (list (d (n "futures") (r "^0.3.1") (d #t) (k 2)) (d (n "futures-core") (r "^0.3.1") (d #t) (k 0)))) (h "1xgmiakp4mfh4hjgq2fjxg96nv2h43msgrks5qx400x8q7qin501")))

