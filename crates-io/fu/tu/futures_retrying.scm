(define-module (crates-io fu tu futures_retrying) #:use-module (crates-io))

(define-public crate-futures_retrying-0.1.0 (c (n "futures_retrying") (v "0.1.0") (d (list (d (n "futures-timer") (r "^2.0") (d #t) (k 0)) (d (n "pin-project") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (f (quote ("log"))) (d #t) (k 0)))) (h "0jcaq0xl762gnrmcc5chxkpzqg9avap9ds5y4yqczyb9rjv19wns") (y #t)))

(define-public crate-futures_retrying-0.1.1 (c (n "futures_retrying") (v "0.1.1") (d (list (d (n "futures-timer") (r "^2.0") (d #t) (k 0)) (d (n "pin-project") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (f (quote ("log"))) (d #t) (k 0)))) (h "0kz4knbski642nlv8f7lhjwlp15vng1dnm5v1l62n574pw667b4h")))

