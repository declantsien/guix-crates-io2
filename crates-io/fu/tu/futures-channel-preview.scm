(define-module (crates-io fu tu futures-channel-preview) #:use-module (crates-io))

(define-public crate-futures-channel-preview-0.2.2 (c (n "futures-channel-preview") (v "0.2.2") (d (list (d (n "futures-core-preview") (r "^0.2.2") (k 0)))) (h "07l2h7abx33sxzxphc0y3089d93i5fx6yrpc8cc2h7gbn33axy6n") (f (quote (("std" "futures-core-preview/std") ("default" "std"))))))

(define-public crate-futures-channel-preview-0.3.0-alpha.1 (c (n "futures-channel-preview") (v "0.3.0-alpha.1") (d (list (d (n "futures-core-preview") (r "^0.3.0-alpha.1") (k 0)) (d (n "futures-preview") (r "^0.3.0-alpha.1") (d #t) (k 2)))) (h "0ydkv2bw6ck00va8ha4g35hf039nrnhxzlmkkizl0d77r37byi8l") (f (quote (("std" "futures-core-preview/std") ("default" "std"))))))

(define-public crate-futures-channel-preview-0.3.0-alpha.2 (c (n "futures-channel-preview") (v "0.3.0-alpha.2") (d (list (d (n "futures-core-preview") (r "^0.3.0-alpha.2") (k 0)) (d (n "futures-preview") (r "^0.3.0-alpha.2") (d #t) (k 2)))) (h "0k3pfhzj9yg9sh5v136i0hpj57h603f3aw7rk1qlhwvm7zmfxc4d") (f (quote (("std" "futures-core-preview/std") ("default" "std"))))))

(define-public crate-futures-channel-preview-0.3.0-alpha.3 (c (n "futures-channel-preview") (v "0.3.0-alpha.3") (d (list (d (n "futures-core-preview") (r "^0.3.0-alpha.3") (k 0)) (d (n "futures-preview") (r "^0.3.0-alpha.3") (d #t) (k 2)) (d (n "pin-utils") (r "^0.1.0-alpha.1") (d #t) (k 2)))) (h "0fpkzjba1l9wlci4s1wi2phgp5wr6k4xgyzda2j4ndj61b8nj5cb") (f (quote (("std" "futures-core-preview/std") ("default" "std"))))))

(define-public crate-futures-channel-preview-0.3.0-alpha.4 (c (n "futures-channel-preview") (v "0.3.0-alpha.4") (d (list (d (n "futures-core-preview") (r "^0.3.0-alpha.4") (k 0)) (d (n "futures-preview") (r "^0.3.0-alpha.4") (d #t) (k 2)) (d (n "pin-utils") (r "^0.1.0-alpha.2") (d #t) (k 2)))) (h "0cywc8vpw4ilz91jy29y44pwp7l0yc9pr1vjjlzal4pdvg6sf6k1") (f (quote (("std" "futures-core-preview/std") ("default" "std"))))))

(define-public crate-futures-channel-preview-0.3.0-alpha.5 (c (n "futures-channel-preview") (v "0.3.0-alpha.5") (d (list (d (n "futures-core-preview") (r "^0.3.0-alpha.5") (k 0)) (d (n "futures-preview") (r "^0.3.0-alpha.5") (d #t) (k 2)) (d (n "pin-utils") (r "^0.1.0-alpha.2") (d #t) (k 2)))) (h "09n3q7l8ddaxmw4l0bmz1dm9ik3hancchjphaz61i2h97qhp1sq0") (f (quote (("std" "futures-core-preview/std") ("default" "std"))))))

(define-public crate-futures-channel-preview-0.3.0-alpha.6 (c (n "futures-channel-preview") (v "0.3.0-alpha.6") (d (list (d (n "futures-core-preview") (r "^0.3.0-alpha.6") (k 0)))) (h "08qbljp6zawhx09m18lyss4lhpl1pfwy4qww28mzf8xvfjh123l3") (f (quote (("std" "futures-core-preview/std") ("default" "std"))))))

(define-public crate-futures-channel-preview-0.3.0-alpha.7 (c (n "futures-channel-preview") (v "0.3.0-alpha.7") (d (list (d (n "futures-core-preview") (r "^0.3.0-alpha.7") (k 0)))) (h "0mn9wzdxl8mjhndxafhb9cpmj25ndv4pcwncfwfm6jw138ksi50c") (f (quote (("std" "futures-core-preview/std") ("default" "std"))))))

(define-public crate-futures-channel-preview-0.3.0-alpha.8 (c (n "futures-channel-preview") (v "0.3.0-alpha.8") (d (list (d (n "futures-core-preview") (r "^0.3.0-alpha.8") (k 0)))) (h "17b9jpl33zqqi6apb9njpbp0clyyybsqh2d2xdrrbjb56xqly5mx") (f (quote (("std" "futures-core-preview/std") ("default" "std"))))))

(define-public crate-futures-channel-preview-0.3.0-alpha.9 (c (n "futures-channel-preview") (v "0.3.0-alpha.9") (d (list (d (n "futures-core-preview") (r "^0.3.0-alpha.9") (k 0)))) (h "1vcnr8f0jjipd7rpqrdm6nxdibriv74nrrh38zzv2442jkab8wdb") (f (quote (("std" "futures-core-preview/std") ("default" "std"))))))

(define-public crate-futures-channel-preview-0.3.0-alpha.10 (c (n "futures-channel-preview") (v "0.3.0-alpha.10") (d (list (d (n "futures-core-preview") (r "^0.3.0-alpha.10") (k 0)))) (h "1l01vsdxdq12s55bb10cfy3303s2llf7qjrd5f6w35g3irwb1d05") (f (quote (("std" "futures-core-preview/std") ("default" "std"))))))

(define-public crate-futures-channel-preview-0.3.0-alpha.11 (c (n "futures-channel-preview") (v "0.3.0-alpha.11") (d (list (d (n "futures-core-preview") (r "^0.3.0-alpha.11") (k 0)))) (h "0hwxzxf1m87d1vvz04mh0wryihpc85382rhx7h2iz6lm0h59kb48") (f (quote (("std" "futures-core-preview/std") ("default" "std"))))))

(define-public crate-futures-channel-preview-0.3.0-alpha.12 (c (n "futures-channel-preview") (v "0.3.0-alpha.12") (d (list (d (n "futures-core-preview") (r "= 0.3.0-alpha.12") (k 0)))) (h "1w8gqp3jard51m73hg9f0fnfs66r3ndnpbwdmh6lfhwpkyh0pa8r") (f (quote (("std" "futures-core-preview/std") ("default" "std"))))))

(define-public crate-futures-channel-preview-0.3.0-alpha.13 (c (n "futures-channel-preview") (v "0.3.0-alpha.13") (d (list (d (n "futures-core-preview") (r "= 0.3.0-alpha.13") (k 0)))) (h "0fq58idjzgzlnd41mlm6ff47w2b10i7bqw26g5zpa437fki14nym") (f (quote (("std" "futures-core-preview/std") ("default" "std"))))))

(define-public crate-futures-channel-preview-0.3.0-alpha.14 (c (n "futures-channel-preview") (v "0.3.0-alpha.14") (d (list (d (n "futures-core-preview") (r "= 0.3.0-alpha.14") (k 0)))) (h "1g4yfy5zsi6vd2mmhkyc62z9prijs94rycqyjsr6adznlqmf7h3m") (f (quote (("std" "futures-core-preview/std") ("default" "std"))))))

(define-public crate-futures-channel-preview-0.3.0-alpha.15 (c (n "futures-channel-preview") (v "0.3.0-alpha.15") (d (list (d (n "futures-core-preview") (r "= 0.3.0-alpha.15") (k 0)))) (h "11c5jqfxqkizdn0531prd9lvi91w72d4fbkk7l7md454gf451wgd") (f (quote (("std" "futures-core-preview/std") ("default" "std"))))))

(define-public crate-futures-channel-preview-0.3.0-alpha.16 (c (n "futures-channel-preview") (v "0.3.0-alpha.16") (d (list (d (n "futures-core-preview") (r "= 0.3.0-alpha.16") (k 0)))) (h "0yph1k6xbh67rpy9nh3ybddfxsrdhfi6b4b62jvyjwn25xqj7mac") (f (quote (("std" "futures-core-preview/std") ("default" "std"))))))

(define-public crate-futures-channel-preview-0.3.0-alpha.17 (c (n "futures-channel-preview") (v "0.3.0-alpha.17") (d (list (d (n "futures-core-preview") (r "= 0.3.0-alpha.17") (k 0)) (d (n "futures-sink-preview") (r "= 0.3.0-alpha.17") (o #t) (k 0)))) (h "1blgpikhw391lzrfqcgg4xsn5xc0dlybni77ka7f0vb08zaixir1") (f (quote (("std" "futures-core-preview/std") ("sink" "futures-sink-preview") ("default" "std"))))))

(define-public crate-futures-channel-preview-0.3.0-alpha.18 (c (n "futures-channel-preview") (v "0.3.0-alpha.18") (d (list (d (n "futures-core-preview") (r "= 0.3.0-alpha.18") (k 0)) (d (n "futures-sink-preview") (r "= 0.3.0-alpha.18") (o #t) (k 0)))) (h "04jvrrx8nsdln2dpaps0mm17j80knkrffm240ivsx964j81gsxzl") (f (quote (("std" "alloc" "futures-core-preview/std") ("sink" "futures-sink-preview") ("nightly" "futures-core-preview/nightly") ("default" "std") ("cfg-target-has-atomic" "futures-core-preview/cfg-target-has-atomic") ("alloc" "futures-core-preview/alloc"))))))

(define-public crate-futures-channel-preview-0.3.0-alpha.19 (c (n "futures-channel-preview") (v "0.3.0-alpha.19") (d (list (d (n "futures-core-preview") (r "= 0.3.0-alpha.19") (k 0)) (d (n "futures-sink-preview") (r "= 0.3.0-alpha.19") (o #t) (k 0)))) (h "0fi6bi4lpyxjigy11y5sjg6wlc8nc71vbpmxz31c3aagjvgz9rfm") (f (quote (("unstable" "futures-core-preview/unstable") ("std" "alloc" "futures-core-preview/std") ("sink" "futures-sink-preview") ("default" "std") ("cfg-target-has-atomic" "futures-core-preview/cfg-target-has-atomic") ("alloc" "futures-core-preview/alloc"))))))

