(define-module (crates-io fu tu futures-join-stream) #:use-module (crates-io))

(define-public crate-futures-join-stream-0.0.1 (c (n "futures-join-stream") (v "0.0.1") (d (list (d (n "futures-core") (r "^0.3") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2") (d #t) (k 0)))) (h "10hd0vk0bmw5r6l80bngbzyy565kvfim5487mxy9ajqxb43nkn5b")))

(define-public crate-futures-join-stream-0.0.2 (c (n "futures-join-stream") (v "0.0.2") (d (list (d (n "ordered-stream") (r "^0.1") (d #t) (k 0)))) (h "1dxcx0pix8wd8n2zywi0xj33alr98d2q810vawmqvkidcrsyzjlm")))

