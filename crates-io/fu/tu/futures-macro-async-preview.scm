(define-module (crates-io fu tu futures-macro-async-preview) #:use-module (crates-io))

(define-public crate-futures-macro-async-preview-0.2.2 (c (n "futures-macro-async-preview") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^0.4.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.14") (f (quote ("full" "fold" "parsing" "printing" "extra-traits" "proc-macro"))) (k 0)))) (h "192rk6vd5n5zfgmqgkaqk0d0a1dzvnsj65j3q7g0s555z69mvz85") (f (quote (("nightly" "proc-macro2/nightly"))))))

