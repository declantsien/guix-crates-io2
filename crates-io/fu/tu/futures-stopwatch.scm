(define-module (crates-io fu tu futures-stopwatch) #:use-module (crates-io))

(define-public crate-futures-stopwatch-0.1.0 (c (n "futures-stopwatch") (v "0.1.0") (d (list (d (n "futures") (r "^0.1.25") (d #t) (k 0)) (d (n "tokio") (r "^0.1.22") (d #t) (k 2)))) (h "02ywyyr0mxkxhkmmssx7qhbympzn9n7ir8lc2dwbrj6a1jj4dp7y")))

(define-public crate-futures-stopwatch-0.2.0 (c (n "futures-stopwatch") (v "0.2.0") (d (list (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "pin-project") (r "^0.4.6") (d #t) (k 0)) (d (n "tokio") (r "^0.2.9") (f (quote ("time" "rt-threaded"))) (d #t) (k 2)))) (h "0d1wmjhd9al52bsvfrz2r6zngc87ns4q5abypwf5k31idzm3gnfl")))

(define-public crate-futures-stopwatch-0.3.0 (c (n "futures-stopwatch") (v "0.3.0") (d (list (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "pin-project") (r "^0.4.6") (d #t) (k 0)) (d (n "tokio") (r "^0.2.9") (f (quote ("time" "macros" "rt-threaded"))) (d #t) (k 2)))) (h "0iax07ifcaj291fikixs01c5gji1m2q27frl3yc211jd8ry7bswm")))

