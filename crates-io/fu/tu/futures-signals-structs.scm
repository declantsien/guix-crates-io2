(define-module (crates-io fu tu futures-signals-structs) #:use-module (crates-io))

(define-public crate-futures-signals-structs-0.1.0 (c (n "futures-signals-structs") (v "0.1.0") (d (list (d (n "futures-signals") (r "^0.3.15") (d #t) (k 0)) (d (n "futures-signals-structs-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "futures-signals-structs-traits") (r "^0.1.0") (d #t) (k 0)))) (h "0iv12ii2gwb2jlyrk7k3kpimals67ch0bq88c4d1cvsclzppr091")))

(define-public crate-futures-signals-structs-0.2.0 (c (n "futures-signals-structs") (v "0.2.0") (d (list (d (n "futures-signals") (r "^0.3.15") (d #t) (k 0)) (d (n "futures-signals-structs-derive") (r "^0.2.0") (d #t) (k 0)) (d (n "futures-signals-structs-traits") (r "^0.2.0") (d #t) (k 0)))) (h "0aklql1b2l49hvj69d7cfnxgpi6dzm2g7ra25g1hbpa98jkqygn6")))

