(define-module (crates-io fu tu futures-stable-preview) #:use-module (crates-io))

(define-public crate-futures-stable-preview-0.2.2 (c (n "futures-stable-preview") (v "0.2.2") (d (list (d (n "futures-core-preview") (r "^0.2.2") (k 0)) (d (n "futures-executor-preview") (r "^0.2.2") (k 0)))) (h "02pkvvfsvsxw8hz1dpl46cmm2h52895jg16j5hiaj1navpc30r2k") (f (quote (("std" "futures-core-preview/std" "futures-executor-preview/std") ("nightly" "futures-core-preview/nightly") ("default" "std"))))))

(define-public crate-futures-stable-preview-0.2.3 (c (n "futures-stable-preview") (v "0.2.3") (d (list (d (n "futures-core-preview") (r "^0.2.2") (k 0)) (d (n "futures-executor-preview") (r "^0.2.2") (k 0)))) (h "1njl73rl6nwhvchy0jx1ir9l9zvaqbcqmk21lyli9g5vp1hajsqa") (f (quote (("std" "futures-core-preview/std" "futures-executor-preview/std") ("nightly" "futures-core-preview/nightly") ("default" "std"))))))

