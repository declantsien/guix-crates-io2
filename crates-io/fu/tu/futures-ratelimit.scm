(define-module (crates-io fu tu futures-ratelimit) #:use-module (crates-io))

(define-public crate-futures-ratelimit-0.1.0 (c (n "futures-ratelimit") (v "0.1.0") (d (list (d (n "futures") (r "^0.3.29") (d #t) (k 0)) (d (n "pin-project") (r "^1.1.3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "tokio-test") (r "^0.4.3") (d #t) (k 2)))) (h "0fazmawxsyx0cygvaxz7vizkx55023r0jkw2izw1dnj2rybk5jfw") (y #t)))

(define-public crate-futures-ratelimit-1.0.0 (c (n "futures-ratelimit") (v "1.0.0") (d (list (d (n "futures") (r "^0.3.29") (d #t) (k 0)) (d (n "pin-project") (r "^1.1.3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "tokio-test") (r "^0.4.3") (d #t) (k 2)))) (h "1bxc2csspa8jhq60rzzwyjpr3858y1sznschi4gyn9ryag386n60")))

(define-public crate-futures-ratelimit-1.0.1 (c (n "futures-ratelimit") (v "1.0.1") (d (list (d (n "futures") (r "^0.3.29") (d #t) (k 0)) (d (n "pin-project") (r "^1.1.3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "tokio-test") (r "^0.4.3") (d #t) (k 2)))) (h "0nczhygkvdva06cb062ln9vmxvrl0ic7abnalkk6qc4px1b08qak")))

