(define-module (crates-io fu tu futures-io-traits-sync-wrapper) #:use-module (crates-io))

(define-public crate-futures-io-traits-sync-wrapper-0.1.0 (c (n "futures-io-traits-sync-wrapper") (v "0.1.0") (d (list (d (n "futures-executor") (r "^0.3") (f (quote ("default"))) (k 2)) (d (n "futures-util") (r "^0.3") (f (quote ("io"))) (k 0)))) (h "1pdn17fnnnyi1r9vv6alzq49q926yrkxigrnnvmkjz4zj8bwhp56")))

(define-public crate-futures-io-traits-sync-wrapper-0.1.1 (c (n "futures-io-traits-sync-wrapper") (v "0.1.1") (d (list (d (n "async-stream-packed") (r "^0.1") (f (quote ("syncable_with_context"))) (k 0)))) (h "0f7hbjdax4acrv2ikxrqv7bvkb7f3yv5qcsngn8k66d16giqbqhk")))

