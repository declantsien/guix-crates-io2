(define-module (crates-io fu tu futures-task) #:use-module (crates-io))

(define-public crate-futures-task-0.0.0 (c (n "futures-task") (v "0.0.0") (h "10g72hqq09prk9hdyx9kl4zwzim0k585xrgf2v3pv3b469rdvw3p")))

(define-public crate-futures-task-0.3.0 (c (n "futures-task") (v "0.3.0") (h "18jrjc2xln415dh23wsjqmkxfpg4jns0gksqwcqsswyvywry3b74") (f (quote (("unstable") ("std" "alloc") ("default" "std") ("cfg-target-has-atomic") ("alloc"))))))

(define-public crate-futures-task-0.3.1 (c (n "futures-task") (v "0.3.1") (h "1yg5d7b3z58mhqbrax5a0qdsfvzfclwyqvw5k3i41x4wnbb55bhb") (f (quote (("unstable") ("std" "alloc") ("default" "std") ("cfg-target-has-atomic") ("alloc"))))))

(define-public crate-futures-task-0.3.2 (c (n "futures-task") (v "0.3.2") (h "1ji6q7hpl4bcljqwhikgx7i5ankpzh4jw81zzmd88wycmargmzyf") (f (quote (("unstable") ("std" "alloc") ("default" "std") ("cfg-target-has-atomic") ("alloc"))))))

(define-public crate-futures-task-0.3.3 (c (n "futures-task") (v "0.3.3") (h "160kayl1aj4dbbx4dvwnw5v2wssd85yfrc88wrg5ainf9fmwxyc6") (f (quote (("unstable") ("std" "alloc") ("default" "std") ("cfg-target-has-atomic") ("alloc"))))))

(define-public crate-futures-task-0.3.4 (c (n "futures-task") (v "0.3.4") (h "09ss6zg7w36s3y2715bnvfrnwr3b8vnklmxag00wvkgn7kjk82kv") (f (quote (("unstable") ("std" "alloc") ("default" "std") ("cfg-target-has-atomic") ("alloc"))))))

(define-public crate-futures-task-0.3.5 (c (n "futures-task") (v "0.3.5") (d (list (d (n "once_cell") (r "^1.3.1") (f (quote ("std"))) (o #t) (k 0)))) (h "09m647nghwsg46ckys2846jfga6fbdwgfc08mfqij87215gnpdmx") (f (quote (("unstable") ("std" "alloc" "once_cell") ("default" "std") ("cfg-target-has-atomic") ("alloc"))))))

(define-public crate-futures-task-0.3.6 (c (n "futures-task") (v "0.3.6") (d (list (d (n "once_cell") (r "^1.3.1") (f (quote ("std"))) (o #t) (k 0)))) (h "150gncflybaf9ni4rdb46kjxnfpk7yxcx2ys089pyqzkm4h6iljd") (f (quote (("unstable") ("std" "alloc" "once_cell") ("default" "std") ("cfg-target-has-atomic") ("alloc"))))))

(define-public crate-futures-task-0.3.7 (c (n "futures-task") (v "0.3.7") (d (list (d (n "once_cell") (r "^1.3.1") (f (quote ("std"))) (o #t) (k 0)))) (h "0g6fg21dr0ivdgkc3p69xshzi0s66wzg0ga5k7plyv0q6yph5mcn") (f (quote (("unstable") ("std" "alloc" "once_cell") ("default" "std") ("cfg-target-has-atomic") ("alloc"))))))

(define-public crate-futures-task-0.3.8 (c (n "futures-task") (v "0.3.8") (d (list (d (n "once_cell") (r "^1.3.1") (f (quote ("std"))) (o #t) (k 0)))) (h "03ad39v8scy353src2f9dkkvcs24n736iavi8xn45cj8pyslwmbw") (f (quote (("unstable") ("std" "alloc" "once_cell") ("default" "std") ("cfg-target-has-atomic") ("alloc"))))))

(define-public crate-futures-task-0.3.9 (c (n "futures-task") (v "0.3.9") (d (list (d (n "once_cell") (r "^1.3.1") (f (quote ("std"))) (o #t) (k 0)))) (h "0f00xi1mc1v35m0jqmikwmj2pgh9kbzbyxvwiwx84rpzl91hham9") (f (quote (("unstable") ("std" "alloc" "once_cell") ("default" "std") ("cfg-target-has-atomic") ("alloc"))))))

(define-public crate-futures-task-0.3.10 (c (n "futures-task") (v "0.3.10") (d (list (d (n "once_cell") (r "^1.3.1") (f (quote ("std"))) (o #t) (k 0)))) (h "1irwyd44958964wphn98xkclg0faq1crh5l768w0s5r1j3m4r508") (f (quote (("unstable") ("std" "alloc" "once_cell") ("default" "std") ("cfg-target-has-atomic") ("alloc"))))))

(define-public crate-futures-task-0.3.11 (c (n "futures-task") (v "0.3.11") (d (list (d (n "once_cell") (r "^1.3.1") (f (quote ("std"))) (o #t) (k 0)))) (h "15lb6fs2zrgh7610h55kiwlnwm07v18qr4v2fk5vcpcjibr91046") (f (quote (("unstable") ("std" "alloc" "once_cell") ("default" "std") ("cfg-target-has-atomic") ("alloc"))))))

(define-public crate-futures-task-0.3.12 (c (n "futures-task") (v "0.3.12") (d (list (d (n "once_cell") (r "^1.3.1") (f (quote ("std"))) (o #t) (k 0)))) (h "11ldlpl7mis5crys731dj7s7npgigdgrzdm7mi2y86m8ivmhgphk") (f (quote (("unstable") ("std" "alloc" "once_cell") ("default" "std") ("cfg-target-has-atomic") ("alloc"))))))

(define-public crate-futures-task-0.3.13 (c (n "futures-task") (v "0.3.13") (h "106zr4rvl3w2h9iibljh4ba0g7pxq7hwyvvscq1fcpn127r9w67s") (f (quote (("unstable") ("std" "alloc") ("default" "std") ("cfg-target-has-atomic") ("alloc"))))))

(define-public crate-futures-task-0.3.14 (c (n "futures-task") (v "0.3.14") (h "1g41ymlqvfjm7l1x4w2xamgwcpnh9gvh5xx1v6k3nvq7jl8aayms") (f (quote (("unstable") ("std" "alloc") ("default" "std") ("cfg-target-has-atomic") ("alloc"))))))

(define-public crate-futures-task-0.3.15 (c (n "futures-task") (v "0.3.15") (h "1bixscylw61w61dizqxhrdlnb8mv7yg9h775pssxsk8szkwvw5la") (f (quote (("unstable") ("std" "alloc") ("default" "std") ("cfg-target-has-atomic") ("alloc"))))))

(define-public crate-futures-task-0.3.16 (c (n "futures-task") (v "0.3.16") (h "1wlr0yl3vjk24281n1iz3yjxj4782fnzcqcmj2zg65q0cyc4mrdv") (f (quote (("unstable") ("std" "alloc") ("default" "std") ("cfg-target-has-atomic") ("alloc"))))))

(define-public crate-futures-task-0.3.17 (c (n "futures-task") (v "0.3.17") (h "169fi44d8sbfpkkyhncj0pgkp1amrllg5523lncf8wyvxps00g8x") (f (quote (("unstable") ("std" "alloc") ("default" "std") ("cfg-target-has-atomic") ("alloc"))))))

(define-public crate-futures-task-0.3.18 (c (n "futures-task") (v "0.3.18") (h "04nyz21ymr2r4fh9hdk1ppi8g61ryp97c8ighf3chcmbm9r1igys") (f (quote (("unstable") ("std" "alloc") ("default" "std") ("cfg-target-has-atomic") ("alloc")))) (y #t) (r "1.36")))

(define-public crate-futures-task-0.3.19 (c (n "futures-task") (v "0.3.19") (h "0wmd3b70sgp1dr3q24439hkm7zj9m1lcafmqvzj7q5ihbi4cdrvf") (f (quote (("unstable") ("std" "alloc") ("default" "std") ("cfg-target-has-atomic") ("alloc")))) (r "1.36")))

(define-public crate-futures-task-0.3.20 (c (n "futures-task") (v "0.3.20") (h "0k9q6kc2raxqivgjw9m3k2bhdwld76nhzdpa7ks8szw7ra1jylm2") (f (quote (("unstable") ("std" "alloc") ("default" "std") ("cfg-target-has-atomic") ("alloc")))) (y #t) (r "1.45")))

(define-public crate-futures-task-0.3.21 (c (n "futures-task") (v "0.3.21") (h "0skpiz2ljisywajv79p70yapfwhkqhb39wxy3f09v47mdfbnmijp") (f (quote (("unstable") ("std" "alloc") ("default" "std") ("cfg-target-has-atomic") ("alloc")))) (r "1.45")))

(define-public crate-futures-task-0.3.22 (c (n "futures-task") (v "0.3.22") (h "1qm7jw860hzvykc5g6xqr66k26xmchv3h6jdyzky83ywmijkd47c") (f (quote (("unstable") ("std" "alloc") ("default" "std") ("cfg-target-has-atomic") ("alloc")))) (r "1.45")))

(define-public crate-futures-task-0.3.23 (c (n "futures-task") (v "0.3.23") (h "01jkvifc3l2accz0cvl1c7i4q4vj2bxi7pjr9ni5ch0zjcxwcbw4") (f (quote (("unstable") ("std" "alloc") ("default" "std") ("cfg-target-has-atomic") ("alloc")))) (r "1.45")))

(define-public crate-futures-task-0.3.24 (c (n "futures-task") (v "0.3.24") (h "1wcln9clj15n7c7igm5wp9yj4kfgj526siwhyf9i51bkgi38ql56") (f (quote (("unstable") ("std" "alloc") ("default" "std") ("cfg-target-has-atomic") ("alloc")))) (r "1.45")))

(define-public crate-futures-task-0.3.25 (c (n "futures-task") (v "0.3.25") (h "1sp6k18py8nv3dmy3j00w83bfmk6fzi7mwzxsflym9nrqlx3kyrg") (f (quote (("unstable") ("std" "alloc") ("default" "std") ("cfg-target-has-atomic") ("alloc")))) (r "1.45")))

(define-public crate-futures-task-0.3.26 (c (n "futures-task") (v "0.3.26") (h "0rk3jg6lri1rrn03ns89cmw8lircbaf2i2d4mr10zc8hyqdrmxyw") (f (quote (("unstable") ("std" "alloc") ("default" "std") ("cfg-target-has-atomic") ("alloc")))) (r "1.45")))

(define-public crate-futures-task-0.3.27 (c (n "futures-task") (v "0.3.27") (h "0y98b6mkb3svh0zvs1axmka11bgfd8pc2f0459a1cyxk6c6m8rgx") (f (quote (("unstable") ("std" "alloc") ("default" "std") ("cfg-target-has-atomic") ("alloc")))) (r "1.45")))

(define-public crate-futures-task-0.3.28 (c (n "futures-task") (v "0.3.28") (h "0ravgihyarbplj32zp60asirfnaalw2wfsa0afhnl3kcpqrd3lvn") (f (quote (("unstable") ("std" "alloc") ("default" "std") ("cfg-target-has-atomic") ("alloc")))) (r "1.56")))

(define-public crate-futures-task-0.3.29 (c (n "futures-task") (v "0.3.29") (h "1qmsss8rb5ppql4qvd4r70h9gpfcpd0bg2b3qilxrnhdkc397lgg") (f (quote (("unstable") ("std" "alloc") ("default" "std") ("cfg-target-has-atomic") ("alloc")))) (r "1.56")))

(define-public crate-futures-task-0.3.30 (c (n "futures-task") (v "0.3.30") (h "013h1724454hj8qczp8vvs10qfiqrxr937qsrv6rhii68ahlzn1q") (f (quote (("unstable") ("std" "alloc") ("default" "std") ("cfg-target-has-atomic") ("alloc")))) (r "1.56")))

