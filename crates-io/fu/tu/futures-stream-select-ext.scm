(define-module (crates-io fu tu futures-stream-select-ext) #:use-module (crates-io))

(define-public crate-futures-stream-select-ext-0.1.0 (c (n "futures-stream-select-ext") (v "0.1.0") (d (list (d (n "futures-executor") (r "^0.3") (d #t) (k 2)) (d (n "futures-timer") (r "^3.0") (d #t) (k 2)) (d (n "futures-util") (r "^0.3") (k 0)) (d (n "pin-project-lite") (r "^0.2") (d #t) (k 0)))) (h "0fvxj1lq0qq8hzgni6i9syky0dvx039bnf69fl4i6x1zvb89z0bm") (f (quote (("std" "alloc" "futures-util/std") ("default" "std") ("alloc" "futures-util/alloc")))) (y #t)))

(define-public crate-futures-stream-select-ext-0.1.1 (c (n "futures-stream-select-ext") (v "0.1.1") (d (list (d (n "async-timer") (r "^1.0.0-beta") (d #t) (k 2)) (d (n "futures-executor") (r "^0.3") (d #t) (k 2)) (d (n "futures-util") (r "^0.3") (k 0)) (d (n "pin-project-lite") (r "^0.2") (d #t) (k 0)))) (h "0cyvylsjj3vb1jray87mx1x08ygl7cy8bkk4dx5d3gb5ry25vfa7") (f (quote (("std" "alloc" "futures-util/std") ("default" "std") ("alloc" "futures-util/alloc")))) (y #t)))

(define-public crate-futures-stream-select-ext-0.1.2 (c (n "futures-stream-select-ext") (v "0.1.2") (d (list (d (n "async-timer") (r "^1.0.0-beta") (d #t) (k 2)) (d (n "futures-executor") (r "^0.3") (d #t) (k 2)) (d (n "futures-util") (r "^0.3") (k 0)) (d (n "pin-project-lite") (r "^0.2") (d #t) (k 0)))) (h "176vsljwhzzvxb58bps0zy8yfwk1ygax1ivdfk0y6h9jlz10lib8") (f (quote (("std" "alloc" "futures-util/std") ("default" "std") ("alloc" "futures-util/alloc"))))))

(define-public crate-futures-stream-select-ext-0.1.3 (c (n "futures-stream-select-ext") (v "0.1.3") (d (list (d (n "futures-executor") (r "^0.3") (d #t) (k 2)) (d (n "futures-util") (r "^0.3") (k 0)) (d (n "pin-project-lite") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1.16") (f (quote ("macros" "rt-multi-thread" "time"))) (d #t) (k 2)))) (h "00ihmc57s93q5d92iyfvcp5n36fz4bjb9yjp9nsjlj34ghv5nzqm") (f (quote (("std" "alloc" "futures-util/std") ("default" "std") ("alloc" "futures-util/alloc"))))))

