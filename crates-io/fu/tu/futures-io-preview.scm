(define-module (crates-io fu tu futures-io-preview) #:use-module (crates-io))

(define-public crate-futures-io-preview-0.2.2 (c (n "futures-io-preview") (v "0.2.2") (d (list (d (n "futures-core-preview") (r "^0.2.2") (k 0)) (d (n "iovec") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1qgqcyfy8w1qwjf9yd3wlndvrcixp62wykbvap0i4jchmjk6k9xa") (f (quote (("std" "futures-core-preview/std" "iovec") ("default" "std"))))))

(define-public crate-futures-io-preview-0.3.0-alpha.1 (c (n "futures-io-preview") (v "0.3.0-alpha.1") (d (list (d (n "futures-core-preview") (r "^0.3.0-alpha.1") (k 0)) (d (n "iovec") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1d5w4hlpqa9r90nykm4cgz2jdx36sjzzma95224k9dawzvgih917") (f (quote (("std" "futures-core-preview/std" "iovec") ("default" "std"))))))

(define-public crate-futures-io-preview-0.3.0-alpha.2 (c (n "futures-io-preview") (v "0.3.0-alpha.2") (d (list (d (n "assert_matches") (r "^1.3.0") (d #t) (k 2)) (d (n "futures-core-preview") (r "^0.3.0-alpha.2") (k 0)) (d (n "futures-preview") (r "^0.3.0-alpha.2") (d #t) (k 2)) (d (n "iovec") (r "^0.1") (o #t) (d #t) (k 0)))) (h "00dracfdxqgjy01ivp0qnk8vpg42a1rml8vmhimlz5rkmr9lg9bl") (f (quote (("std" "futures-core-preview/std" "iovec") ("default" "std"))))))

(define-public crate-futures-io-preview-0.3.0-alpha.3 (c (n "futures-io-preview") (v "0.3.0-alpha.3") (d (list (d (n "assert_matches") (r "^1.3.0") (d #t) (k 2)) (d (n "futures-core-preview") (r "^0.3.0-alpha.3") (k 0)) (d (n "futures-preview") (r "^0.3.0-alpha.3") (d #t) (k 2)) (d (n "iovec") (r "^0.1") (o #t) (d #t) (k 0)))) (h "079iw65xp1na7kr4l056j38nx29m0mdbp2zd8xrxbch98652sbvp") (f (quote (("std" "futures-core-preview/std" "iovec") ("default" "std"))))))

(define-public crate-futures-io-preview-0.3.0-alpha.4 (c (n "futures-io-preview") (v "0.3.0-alpha.4") (d (list (d (n "assert_matches") (r "^1.3.0") (d #t) (k 2)) (d (n "futures-core-preview") (r "^0.3.0-alpha.4") (k 0)) (d (n "futures-preview") (r "^0.3.0-alpha.4") (d #t) (k 2)) (d (n "iovec") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1npxp3v9anq6lr2lzg985n2vqygqwn62dzh36zf8piw0hxpvxmvd") (f (quote (("std" "futures-core-preview/std" "iovec") ("default" "std"))))))

(define-public crate-futures-io-preview-0.3.0-alpha.5 (c (n "futures-io-preview") (v "0.3.0-alpha.5") (d (list (d (n "assert_matches") (r "^1.3.0") (d #t) (k 2)) (d (n "futures-core-preview") (r "^0.3.0-alpha.5") (k 0)) (d (n "futures-preview") (r "^0.3.0-alpha.5") (d #t) (k 2)) (d (n "iovec") (r "^0.1") (o #t) (d #t) (k 0)))) (h "07j07j2fcjmd3ilbas0i8p5jkcm79ayqz4yxh4kajcfaz41fwjql") (f (quote (("std" "futures-core-preview/std" "iovec") ("default" "std"))))))

(define-public crate-futures-io-preview-0.3.0-alpha.6 (c (n "futures-io-preview") (v "0.3.0-alpha.6") (d (list (d (n "futures-core-preview") (r "^0.3.0-alpha.6") (k 0)) (d (n "iovec") (r "^0.1") (o #t) (d #t) (k 0)))) (h "169z36gjcbk1dfr83mrs8nrxzcbsv4wnlxwyw2zsrb7fl091gzz6") (f (quote (("std" "futures-core-preview/std" "iovec") ("default" "std"))))))

(define-public crate-futures-io-preview-0.3.0-alpha.7 (c (n "futures-io-preview") (v "0.3.0-alpha.7") (d (list (d (n "futures-core-preview") (r "^0.3.0-alpha.7") (k 0)) (d (n "iovec") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1vk2vv7jdp2s3archllwsgkrih2wgkb9jn9qkiln9hmydl2h57i6") (f (quote (("std" "futures-core-preview/std" "iovec") ("default" "std"))))))

(define-public crate-futures-io-preview-0.3.0-alpha.8 (c (n "futures-io-preview") (v "0.3.0-alpha.8") (d (list (d (n "futures-core-preview") (r "^0.3.0-alpha.8") (k 0)) (d (n "iovec") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0giala9zr056pjcjgv92ky89jqdqvka6pg9cba3pphxxw1izgybr") (f (quote (("std" "futures-core-preview/std" "iovec") ("default" "std"))))))

(define-public crate-futures-io-preview-0.3.0-alpha.9 (c (n "futures-io-preview") (v "0.3.0-alpha.9") (d (list (d (n "futures-core-preview") (r "^0.3.0-alpha.9") (k 0)) (d (n "iovec") (r "^0.1") (o #t) (d #t) (k 0)))) (h "08wiz8wxgmnayax648r9bhlv98c01ybfk9jc1wn56bgmcfx6mgci") (f (quote (("std" "futures-core-preview/std" "iovec") ("default" "std"))))))

(define-public crate-futures-io-preview-0.3.0-alpha.10 (c (n "futures-io-preview") (v "0.3.0-alpha.10") (d (list (d (n "futures-core-preview") (r "^0.3.0-alpha.10") (k 0)) (d (n "iovec") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1x63xkpbflffzn68b3zy1ih6sjssnvizgj4l06y5iq3xlhr8qr5x") (f (quote (("std" "futures-core-preview/std" "iovec") ("default" "std"))))))

(define-public crate-futures-io-preview-0.3.0-alpha.11 (c (n "futures-io-preview") (v "0.3.0-alpha.11") (d (list (d (n "futures-core-preview") (r "^0.3.0-alpha.11") (k 0)) (d (n "iovec") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1fsqlmp7q5j6qpmiggf78h16kyn3h7r1chxravp1740nh5virmra") (f (quote (("std" "futures-core-preview/std" "iovec") ("default" "std"))))))

(define-public crate-futures-io-preview-0.3.0-alpha.12 (c (n "futures-io-preview") (v "0.3.0-alpha.12") (d (list (d (n "futures-core-preview") (r "= 0.3.0-alpha.12") (k 0)) (d (n "iovec") (r "^0.1") (o #t) (d #t) (k 0)))) (h "15aph4lcpnvpm1m6bcclz12v8q5s9nnijdzi8b0lim5k2j9yfw4v") (f (quote (("std" "futures-core-preview/std" "iovec") ("default" "std"))))))

(define-public crate-futures-io-preview-0.3.0-alpha.13 (c (n "futures-io-preview") (v "0.3.0-alpha.13") (d (list (d (n "futures-core-preview") (r "= 0.3.0-alpha.13") (k 0)) (d (n "iovec") (r "^0.1") (o #t) (d #t) (k 0)))) (h "19im811w5wsd426nax447dslcalfii1kqvfndwjjvjdzjnlgv6jd") (f (quote (("std" "futures-core-preview/std" "iovec") ("default" "std"))))))

(define-public crate-futures-io-preview-0.3.0-alpha.14 (c (n "futures-io-preview") (v "0.3.0-alpha.14") (d (list (d (n "futures-core-preview") (r "= 0.3.0-alpha.14") (k 0)) (d (n "iovec") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0p8bpz0k46hgszirsqndkwszywgqhkiqjyxjv5c4361xj3casxfx") (f (quote (("std" "futures-core-preview/std" "iovec") ("default" "std"))))))

(define-public crate-futures-io-preview-0.3.0-alpha.15 (c (n "futures-io-preview") (v "0.3.0-alpha.15") (d (list (d (n "futures-core-preview") (r "= 0.3.0-alpha.15") (k 0)) (d (n "iovec") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0wwhdnhfj2fi645ck73ysgcmp7ypgzsy956l1iz5yi719jcr2j5j") (f (quote (("std" "futures-core-preview/std" "iovec") ("default" "std"))))))

(define-public crate-futures-io-preview-0.3.0-alpha.16 (c (n "futures-io-preview") (v "0.3.0-alpha.16") (d (list (d (n "futures-core-preview") (r "= 0.3.0-alpha.16") (k 0)))) (h "18sry5ynzb4vaimsssi3jqbsfalksn9yy3bvg4mx7j9r3xxbz86c") (f (quote (("std" "futures-core-preview/std") ("default" "std"))))))

(define-public crate-futures-io-preview-0.3.0-alpha.17 (c (n "futures-io-preview") (v "0.3.0-alpha.17") (h "0fhvwhdb8ywjjbfng0ra1r8yyc9yzpyxg9sv3spb3f7w0lk40bh8") (f (quote (("std") ("default" "std"))))))

(define-public crate-futures-io-preview-0.3.0-alpha.18 (c (n "futures-io-preview") (v "0.3.0-alpha.18") (h "1lfxnmvr6ljff5z5j1n9qff4vriwcv3zwdq4gd2zj8zdr70y0zgf") (f (quote (("std") ("default" "std"))))))

(define-public crate-futures-io-preview-0.3.0-alpha.19 (c (n "futures-io-preview") (v "0.3.0-alpha.19") (h "1npb04xbn2gw5rjllz88cb88fql44xxfkgcidjjj26fva3j4m4gl") (f (quote (("unstable") ("std") ("read_initializer") ("default" "std"))))))

