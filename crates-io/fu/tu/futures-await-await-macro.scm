(define-module (crates-io fu tu futures-await-await-macro) #:use-module (crates-io))

(define-public crate-futures-await-await-macro-0.1.0 (c (n "futures-await-await-macro") (v "0.1.0") (h "15gqvfgly08c6m145y0mkc4afi7mwj2irm3b9yi8in0znlcbmbdp")))

(define-public crate-futures-await-await-macro-0.2.0-alpha (c (n "futures-await-await-macro") (v "0.2.0-alpha") (h "0gdq9ikn3dq2ijf2xbpz25b241myg1325gz4czp7w8zxg4g7gz9q")))

(define-public crate-futures-await-await-macro-0.1.1 (c (n "futures-await-await-macro") (v "0.1.1") (h "19645s8jz52bwcb24ir189cryhkm4k92mwp37w7bzlglwhsbvf7k")))

