(define-module (crates-io fu tu futures-black-hole) #:use-module (crates-io))

(define-public crate-futures-black-hole-0.1.0 (c (n "futures-black-hole") (v "0.1.0") (d (list (d (n "futures-await") (r "^0.1") (d #t) (k 0)) (d (n "futures-cpupool") (r "^0.1") (d #t) (k 0)))) (h "1g0mqs3i81iy19sjn2mizakhji9bj44ryd577hws2yc0p1k91lc6")))

(define-public crate-futures-black-hole-0.2.0 (c (n "futures-black-hole") (v "0.2.0") (d (list (d (n "futures-await") (r "^0.1") (d #t) (k 0)) (d (n "futures-cpupool") (r "^0.1") (d #t) (k 0)))) (h "0sclir8vwzkdk06l456v1wk1ivz4pn7vckx3crrrbvlh9ca1xlyx")))

(define-public crate-futures-black-hole-0.2.1 (c (n "futures-black-hole") (v "0.2.1") (d (list (d (n "futures-await") (r "^0.1") (d #t) (k 0)) (d (n "futures-cpupool") (r "^0.1") (d #t) (k 0)))) (h "13xm7zspydnz8g1gqzl1l0qdvjkn7j83341w8218dkv555zjs7jz")))

(define-public crate-futures-black-hole-0.2.2 (c (n "futures-black-hole") (v "0.2.2") (d (list (d (n "futures-await") (r "^0.1") (d #t) (k 0)) (d (n "futures-cpupool") (r "^0.1") (d #t) (k 0)))) (h "0w8cvxg8c2hky3sqc9ap54gp1xbhp3ls800skj7i8lq3dscj3jpg")))

