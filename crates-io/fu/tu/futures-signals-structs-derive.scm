(define-module (crates-io fu tu futures-signals-structs-derive) #:use-module (crates-io))

(define-public crate-futures-signals-structs-derive-0.1.0 (c (n "futures-signals-structs-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.39") (f (quote ("full"))) (d #t) (k 0)))) (h "1r417d5vn64ch8zha1y9acmk72phsm02mqgkbazhi3nmvgdb1370")))

(define-public crate-futures-signals-structs-derive-0.2.0 (c (n "futures-signals-structs-derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.39") (f (quote ("full"))) (d #t) (k 0)))) (h "0waal86kgn9i9j5b8yxi2mmvycyl7wwgjl6llrj7dj2pzq4rm5ki")))

