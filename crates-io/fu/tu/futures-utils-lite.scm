(define-module (crates-io fu tu futures-utils-lite) #:use-module (crates-io))

(define-public crate-futures-utils-lite-0.1.0 (c (n "futures-utils-lite") (v "0.1.0") (d (list (d (n "futures-lite") (r "^1.12.0") (d #t) (k 0)) (d (n "futures-macros-lite") (r "^0.1.0") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2.8") (d #t) (k 0)))) (h "1n808mk5kylgxgqcnrfgi5s971yw2kcmszpsgynr8lfjkzm1cajg") (f (quote (("std" "alloc") ("default" "alloc") ("alloc"))))))

(define-public crate-futures-utils-lite-0.1.1 (c (n "futures-utils-lite") (v "0.1.1") (d (list (d (n "futures-lite") (r "^1.12.0") (d #t) (k 0)) (d (n "futures-macros-lite") (r "^0.1.0") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2.8") (d #t) (k 0)))) (h "1vfcal0a30nr0ic62i870nw96cdv8vwc996yi9qkx8w27lc754j2") (f (quote (("std" "alloc") ("default" "alloc") ("alloc"))))))

