(define-module (crates-io fu tu futures-await-test-preview) #:use-module (crates-io))

(define-public crate-futures-await-test-preview-0.2.2 (c (n "futures-await-test-preview") (v "0.2.2") (d (list (d (n "futures-preview") (r "^0.2.2") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.14") (f (quote ("full" "parsing" "printing" "proc-macro"))) (k 0)))) (h "1vrdir2ccxsmb8wqsd8xg44vrw6lyydcr7yxxhf4ia1j233gxaij")))

(define-public crate-futures-await-test-preview-0.3.0-alpha.1 (c (n "futures-await-test-preview") (v "0.3.0-alpha.1") (d (list (d (n "futures-preview") (r "^0.3.0-alpha.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.14") (f (quote ("full" "parsing" "printing" "proc-macro"))) (k 0)))) (h "18lachzs3bqx0x3w9s5p1ng0r0fpnph3jadb056ryzy868hhdlh5")))

(define-public crate-futures-await-test-preview-0.3.0-alpha.2 (c (n "futures-await-test-preview") (v "0.3.0-alpha.2") (d (list (d (n "futures-preview") (r "^0.3.0-alpha.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.14") (f (quote ("full" "parsing" "printing" "proc-macro"))) (k 0)))) (h "0b33qh93gj3nc5lm233r01kjdl28nafcn6fn626iznhnr9pnfnz0")))

(define-public crate-futures-await-test-preview-0.3.0-alpha.9 (c (n "futures-await-test-preview") (v "0.3.0-alpha.9") (d (list (d (n "futures-preview") (r "^0.3.0-alpha.9") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "parsing" "printing" "proc-macro"))) (k 0)))) (h "1yygyh8f0vcp6r7mddp16yysz4mhdxp8dgcfpk4m1yv5yk6g53hb")))

(define-public crate-futures-await-test-preview-0.3.0-alpha.10 (c (n "futures-await-test-preview") (v "0.3.0-alpha.10") (d (list (d (n "futures-preview") (r "^0.3.0-alpha.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "parsing" "printing" "proc-macro"))) (k 0)))) (h "0mgv09dqhf51wkbwfn5h8p4z9r5j2z96876hcx875llfxv5x5agg")))

(define-public crate-futures-await-test-preview-0.3.0-alpha.13 (c (n "futures-await-test-preview") (v "0.3.0-alpha.13") (d (list (d (n "futures-preview") (r "^0.3.0-alpha.13") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "parsing" "printing" "proc-macro"))) (k 0)))) (h "1292k9353wln87mdqj41j16jcwxn168h7fsshd5k67p1mg3286xc")))

(define-public crate-futures-await-test-preview-0.3.0-alpha.14 (c (n "futures-await-test-preview") (v "0.3.0-alpha.14") (d (list (d (n "futures-preview") (r "^0.3.0-alpha.14") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.19") (d #t) (k 0)) (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full" "parsing" "printing" "proc-macro"))) (k 0)))) (h "11356y0d64ixjmgvsy3mhljcv8f2hsf6z35rxbzr5h82mjiq3swc")))

(define-public crate-futures-await-test-preview-0.3.0-alpha.15 (c (n "futures-await-test-preview") (v "0.3.0-alpha.15") (d (list (d (n "futures-preview") (r "^0.3.0-alpha.15") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.13") (d #t) (k 0)) (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "syn") (r "^0.15.0") (f (quote ("clone-impls" "full" "parsing" "printing" "proc-macro"))) (k 0)))) (h "17gb5mpkddg4kwkmh91hlgzg1q76ygyg1azipcbxqdmmz31d2jjj")))

(define-public crate-futures-await-test-preview-0.3.0-alpha.16 (c (n "futures-await-test-preview") (v "0.3.0-alpha.16") (d (list (d (n "futures-await-test-macro-preview") (r "= 0.3.0-alpha.16") (d #t) (k 0)) (d (n "futures-executor-preview") (r "^0.3.0-alpha.16") (d #t) (k 0)))) (h "0xpy3bpvh563wvmxh7r432qdnvdwsmy2ss46l01j69nd4xllbwxb")))

(define-public crate-futures-await-test-preview-0.3.0-alpha.19 (c (n "futures-await-test-preview") (v "0.3.0-alpha.19") (d (list (d (n "futures-await-test-macro-preview") (r "= 0.3.0-alpha.19") (d #t) (k 0)) (d (n "futures-executor-preview") (r "^0.3.0-alpha.19") (d #t) (k 0)))) (h "1dn56n5drs8vhq3wzqyp2c0wj0kkcwx5ia4xbgk3c0npv5ppl8gg")))

