(define-module (crates-io fu tu futures-mpsc) #:use-module (crates-io))

(define-public crate-futures-mpsc-0.1.0 (c (n "futures-mpsc") (v "0.1.0") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)))) (h "00cy17mm7zyv1m68jy0ma2r27d9cimpfyfg3f9a9fm4xvk9p7m4h")))

(define-public crate-futures-mpsc-0.1.1 (c (n "futures-mpsc") (v "0.1.1") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)))) (h "02vs3d9w9pvx1hjz2r937kn8a75mqnab5ym4yfd4y3dpmrb0x5c6")))

