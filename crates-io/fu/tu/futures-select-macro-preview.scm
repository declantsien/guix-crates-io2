(define-module (crates-io fu tu futures-select-macro-preview) #:use-module (crates-io))

(define-public crate-futures-select-macro-preview-0.3.0-alpha.11 (c (n "futures-select-macro-preview") (v "0.3.0-alpha.11") (d (list (d (n "proc-macro-hack") (r "^0.5.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)))) (h "09gsvcrg8lqwx9z87i2ipblvl9ii1nfq4ij0h0b8zbqsa60n1vyr") (f (quote (("std"))))))

(define-public crate-futures-select-macro-preview-0.3.0-alpha.12 (c (n "futures-select-macro-preview") (v "0.3.0-alpha.12") (d (list (d (n "proc-macro-hack") (r "^0.5.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)))) (h "0gsfbs6066qvq648169p9g3z9scwhkqlglhw81wx4yy7j08hmsdm") (f (quote (("std"))))))

(define-public crate-futures-select-macro-preview-0.3.0-alpha.13 (c (n "futures-select-macro-preview") (v "0.3.0-alpha.13") (d (list (d (n "proc-macro-hack") (r "^0.5.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)))) (h "14bwc8jhwcyv822laj0nflqnhfk843sipmq0gjjcv1icpdsrpm9v") (f (quote (("std"))))))

(define-public crate-futures-select-macro-preview-0.3.0-alpha.14 (c (n "futures-select-macro-preview") (v "0.3.0-alpha.14") (d (list (d (n "proc-macro-hack") (r "^0.5.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)))) (h "11b7by01hr7jkj9xj25ks28bg8qpkap0agm0fiqgmr63axdx3h69") (f (quote (("std"))))))

(define-public crate-futures-select-macro-preview-0.3.0-alpha.15 (c (n "futures-select-macro-preview") (v "0.3.0-alpha.15") (d (list (d (n "proc-macro-hack") (r "^0.5.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)))) (h "1b4g1wlmfmr727ymymlrvrzz2i3dvj81hiz8yir4bbv65dvnbhnd") (f (quote (("std"))))))

(define-public crate-futures-select-macro-preview-0.3.0-alpha.16 (c (n "futures-select-macro-preview") (v "0.3.0-alpha.16") (d (list (d (n "proc-macro-hack") (r "^0.5.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)))) (h "1c7h57y5id6i2i31276zqkgvgr5c8kh21frhjm9adj67md22dvmg") (f (quote (("std"))))))

(define-public crate-futures-select-macro-preview-0.3.0-alpha.17 (c (n "futures-select-macro-preview") (v "0.3.0-alpha.17") (d (list (d (n "proc-macro-hack") (r "^0.5.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15.25") (f (quote ("full"))) (d #t) (k 0)))) (h "1a90ivjzkgz7msiz5si05xzi8xwsk5gar1gkrbmrgqpgkliqd7a6") (f (quote (("std"))))))

(define-public crate-futures-select-macro-preview-0.3.0-alpha.18 (c (n "futures-select-macro-preview") (v "0.3.0-alpha.18") (d (list (d (n "proc-macro-hack") (r "^0.5.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15.25") (f (quote ("full"))) (d #t) (k 0)))) (h "1hsldb378bym9rv3bzk82vqjxr1jx75j0yrj3zf1ba6bmjwvnzbn") (f (quote (("std"))))))

(define-public crate-futures-select-macro-preview-0.3.0-alpha.19 (c (n "futures-select-macro-preview") (v "0.3.0-alpha.19") (d (list (d (n "proc-macro-hack") (r "^0.5.9") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1xsq55cf2rnf7k6r04q8wynmxiy9svm3pi840vjva47bc0sy8anz") (f (quote (("std"))))))

