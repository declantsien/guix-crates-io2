(define-module (crates-io fu tu future-union) #:use-module (crates-io))

(define-public crate-future-union-0.1.0 (c (n "future-union") (v "0.1.0") (d (list (d (n "future-union-impl") (r "^0.1.0") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 2)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)))) (h "120jr73hqqq32fdgpml4hqrnd59cl0rqkm6gcs501zprhm9bbyyp")))

(define-public crate-future-union-0.1.1 (c (n "future-union") (v "0.1.1") (d (list (d (n "future-union-impl") (r "^0.1.0") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 2)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)))) (h "0gvp5iah8rri7dlkw64q7rd947nb1rqp3nk9vm1hyd0abh1cmphm")))

