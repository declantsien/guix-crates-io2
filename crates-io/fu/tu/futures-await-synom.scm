(define-module (crates-io fu tu futures-await-synom) #:use-module (crates-io))

(define-public crate-futures-await-synom-0.12.0 (c (n "futures-await-synom") (v "0.12.0") (d (list (d (n "futures-await-quote") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "proc-macro2") (r "^0.1") (d #t) (k 0)))) (h "0c004jk4p1xwsbcj56alj1cad7akp8c34kpvq1zpv9h4pbh4vj57") (f (quote (("printing" "futures-await-quote") ("parsing") ("extra-traits") ("default") ("clone-impls"))))))

