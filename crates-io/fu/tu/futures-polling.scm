(define-module (crates-io fu tu futures-polling) #:use-module (crates-io))

(define-public crate-futures-polling-0.0.0 (c (n "futures-polling") (v "0.0.0") (h "1r9f8g9dgys992ikim38wqf2ai46vyh8fpkhja5l3jii9wjjxzs2")))

(define-public crate-futures-polling-0.1.0 (c (n "futures-polling") (v "0.1.0") (d (list (d (n "futures-lite") (r "^1.0") (d #t) (k 2)))) (h "1bnxxqi2qlyfjjnz6p5xvs01plghzd4mrj7cbm0l1vi3gmjhrj7z")))

(define-public crate-futures-polling-0.1.1 (c (n "futures-polling") (v "0.1.1") (d (list (d (n "futures-lite") (r "^1.0") (d #t) (k 2)))) (h "0755r5mvlx9wm0fypkp3kpwbygpwyv4zx2rfz7sldf72xz2pciyb")))

