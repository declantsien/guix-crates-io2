(define-module (crates-io fu tu future-parking_lot) #:use-module (crates-io))

(define-public crate-future-parking_lot-0.1.0 (c (n "future-parking_lot") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.3") (d #t) (k 2)) (d (n "parking_lot") (r "^0.8") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 0)))) (h "0kqy38afpmwbwy4wdd6gv7zl6pxib6965vgb2wdi9fsgm0b3lwd5")))

(define-public crate-future-parking_lot-0.1.1 (c (n "future-parking_lot") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.3") (d #t) (k 2)) (d (n "lock_api") (r "^0.2") (d #t) (k 0)) (d (n "parking_lot") (r "^0.8") (d #t) (k 2)) (d (n "tokio") (r "^0.1") (d #t) (k 0)))) (h "09ah880rhxy172v6vr47b1x1qwndyhk09fgqrbi24g04kprm589q")))

(define-public crate-future-parking_lot-0.2.0-alpha.2 (c (n "future-parking_lot") (v "0.2.0-alpha.2") (d (list (d (n "lazy_static") (r "^1.3") (d #t) (k 2)) (d (n "lock_api") (r "^0.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.9") (d #t) (k 2)) (d (n "tokio") (r "= 0.2.0-alpha.2") (d #t) (k 2)))) (h "0m7qssmarkcjv37missgrlpzpc7adb8pza8xkkqiz12zaa0mdrjh")))

(define-public crate-future-parking_lot-0.3.0-alpha.2 (c (n "future-parking_lot") (v "0.3.0-alpha.2") (d (list (d (n "crossbeam") (r "^0.7") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "lock_api") (r "^0.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.9") (d #t) (k 0)) (d (n "tokio") (r "= 0.2.0-alpha.2") (d #t) (k 2)))) (h "0751svlwskgbgb6r6y8cfhynlgpprqg0c54zymgp4ji16q6771x0")))

(define-public crate-future-parking_lot-0.3.1-alpha.2 (c (n "future-parking_lot") (v "0.3.1-alpha.2") (d (list (d (n "crossbeam-queue") (r "^0.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.6") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "lock_api") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "parking_lot") (r "^0.9") (d #t) (k 0)) (d (n "tokio") (r "= 0.2.0-alpha.2") (d #t) (k 2)))) (h "0rkrd1sajgwp322w949cx50as7j21i2r655w7fqh4d1r59kn4wq3")))

(define-public crate-future-parking_lot-0.3.2-alpha.2 (c (n "future-parking_lot") (v "0.3.2-alpha.2") (d (list (d (n "crossbeam-queue") (r "^0.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.6") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "lock_api") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "parking_lot") (r "^0.9") (d #t) (k 0)) (d (n "tokio") (r "= 0.2.0-alpha.2") (d #t) (k 2)))) (h "1js784ghbdnwsl6vvfvc11mvqypk77n62d7idcgpfkm3kg1v3bbj")))

(define-public crate-future-parking_lot-0.3.3 (c (n "future-parking_lot") (v "0.3.3") (d (list (d (n "crossbeam-queue") (r "^0.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.7") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "lock_api") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "parking_lot") (r "^0.9") (d #t) (k 0)) (d (n "tokio") (r "= 0.2.0-alpha.6") (d #t) (k 2)))) (h "121l17csqgrhj911g2wpd2aqd78ydvv1b6abk1kg211kpxw08qp2")))

