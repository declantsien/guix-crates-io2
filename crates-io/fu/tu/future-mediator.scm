(define-module (crates-io fu tu future-mediator) #:use-module (crates-io))

(define-public crate-future-mediator-0.1.2 (c (n "future-mediator") (v "0.1.2") (d (list (d (n "bitmask-enum") (r "^2.2.3") (d #t) (k 0)) (d (n "futures") (r "^0.3.29") (f (quote ("executor" "thread-pool"))) (d #t) (k 0)) (d (n "futures-test") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.5") (d #t) (k 2)))) (h "1a6yhqki9cdls35lb3hagk39cskabys485zsn5a7dkkva6mf5a2x")))

