(define-module (crates-io fu tu futures-race) #:use-module (crates-io))

(define-public crate-futures-race-1.0.0 (c (n "futures-race") (v "1.0.0") (d (list (d (n "pin-project") (r "~0.4") (d #t) (k 0)) (d (n "smol") (r "~0.1") (d #t) (k 2)))) (h "0y9xdvhgbrwf341gzgmdhjna3kb9vma7ycajrnyvfzk84cgpn7r0")))

(define-public crate-futures-race-1.1.0 (c (n "futures-race") (v "1.1.0") (d (list (d (n "pin-project-lite") (r "^0.1") (d #t) (k 0)) (d (n "smol") (r "^0.3") (d #t) (k 2)))) (h "0klgh7ag1j4f3b0ky3py19xk17ds08v1mxw9v2yzbyw1mklziypf")))

(define-public crate-futures-race-1.2.0 (c (n "futures-race") (v "1.2.0") (d (list (d (n "pin-project-lite") (r "^0.1") (d #t) (k 0)))) (h "1lalf9xpcbj18pz2zf7bvxdjqrbk3pg6dr40v02bgifsmj8dr7pr")))

