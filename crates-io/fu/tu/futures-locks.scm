(define-module (crates-io fu tu futures-locks) #:use-module (crates-io))

(define-public crate-futures-locks-0.1.0 (c (n "futures-locks") (v "0.1.0") (d (list (d (n "futures") (r "^0.1.20") (d #t) (k 0)) (d (n "tokio") (r "^0.1.4") (d #t) (k 2)))) (h "07n94xs3b2i10p599wzhr156d8flmk58s13163hrfnp6gbrj6v4k")))

(define-public crate-futures-locks-0.1.1 (c (n "futures-locks") (v "0.1.1") (d (list (d (n "futures") (r "^0.1.20") (d #t) (k 0)) (d (n "tokio") (r "^0.1.4") (d #t) (k 2)))) (h "18wa589fx8324r7bpw2ifxdxz5bmrx7zfl6l4s6kr3f8k1h1p6n3")))

(define-public crate-futures-locks-0.2.0 (c (n "futures-locks") (v "0.2.0") (d (list (d (n "futures") (r "^0.1.20") (d #t) (k 0)) (d (n "tokio") (r "^0.1.4") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^0.1.4") (d #t) (k 2)))) (h "02963sv82lyb9gnb2238nafd2rb4b8km00iv13xhda1gdddydy2a")))

(define-public crate-futures-locks-0.2.1 (c (n "futures-locks") (v "0.2.1") (d (list (d (n "futures") (r "^0.1.20") (d #t) (k 0)) (d (n "tokio") (r "^0.1.4") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^0.1.4") (d #t) (k 2)))) (h "0i7q44q94s8yggr2az4lrd12k8pgifm9287hqlmmv7w2jnwhz3kd") (f (quote (("default" "tokio"))))))

(define-public crate-futures-locks-0.3.0 (c (n "futures-locks") (v "0.3.0") (d (list (d (n "futures") (r "^0.1.20") (d #t) (k 0)) (d (n "tokio") (r "^0.1.7") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^0.1.4") (d #t) (k 2)))) (h "0xyfyxiaw4j64v5wq0n9aqflrlfnfgq9qq6f0kc780zgn69jz40j") (f (quote (("default" "tokio"))))))

(define-public crate-futures-locks-0.3.1 (c (n "futures-locks") (v "0.3.1") (d (list (d (n "futures") (r "^0.1.20") (d #t) (k 0)) (d (n "tokio-current-thread") (r "^0.1.4") (o #t) (d #t) (k 0)) (d (n "tokio-executor") (r "^0.1.5") (o #t) (d #t) (k 0)) (d (n "tokio_") (r "^0.1.8") (d #t) (k 2) (p "tokio")))) (h "09fa7hax8dv1vyv4zfl1hnhkhqj21v0gajp2ifc4g1r17v9vkydy") (f (quote (("tokio" "tokio-current-thread" "tokio-executor") ("default" "tokio"))))))

(define-public crate-futures-locks-0.3.2 (c (n "futures-locks") (v "0.3.2") (d (list (d (n "futures") (r "^0.1.25") (d #t) (k 0)) (d (n "tokio-current-thread") (r "^0.1.4") (o #t) (d #t) (k 0)) (d (n "tokio-executor") (r "^0.1.5") (o #t) (d #t) (k 0)) (d (n "tokio_") (r "^0.1.8") (d #t) (k 2) (p "tokio")))) (h "0zfdh0ah0z9d3qi4vizmzr6dlcgkmhxfzjh0whln2k961p6a3z93") (f (quote (("tokio" "tokio-current-thread" "tokio-executor") ("nightly-docs") ("default" "tokio"))))))

(define-public crate-futures-locks-0.3.3 (c (n "futures-locks") (v "0.3.3") (d (list (d (n "futures") (r "^0.1.25") (d #t) (k 0)) (d (n "tokio-current-thread") (r "^0.1.4") (o #t) (d #t) (k 0)) (d (n "tokio-executor") (r "^0.1.5") (o #t) (d #t) (k 0)) (d (n "tokio_") (r "^0.1.8") (d #t) (k 2) (p "tokio")))) (h "10c9ky7mknj7m5b0ari7fa92zwpw8ddwjnrwk4c3kbm5bh3mhmmx") (f (quote (("tokio" "tokio-current-thread" "tokio-executor") ("nightly-docs") ("default" "tokio"))))))

(define-public crate-futures-locks-0.4.0 (c (n "futures-locks") (v "0.4.0") (d (list (d (n "futures") (r "^0.1.25") (d #t) (k 0)) (d (n "tokio-current-thread") (r "^0.1.4") (o #t) (d #t) (k 0)) (d (n "tokio-executor") (r "^0.1.5") (o #t) (d #t) (k 0)) (d (n "tokio_") (r "^0.1.8") (d #t) (k 2) (p "tokio")))) (h "10677ldd48ax9xs9fs2zl8jrqkzdcz1v51lbc8k7k619va46i80l") (f (quote (("tokio" "tokio-current-thread" "tokio-executor") ("nightly-docs") ("default" "tokio"))))))

(define-public crate-futures-locks-0.5.0 (c (n "futures-locks") (v "0.5.0") (d (list (d (n "futures") (r "^0.1.25") (d #t) (k 0)) (d (n "tokio-current-thread") (r "^0.1.4") (o #t) (d #t) (k 0)) (d (n "tokio-executor") (r "^0.1.5") (o #t) (d #t) (k 0)) (d (n "tokio_") (r "^0.1.8") (d #t) (k 2) (p "tokio")))) (h "0ld28d69x4wq4zrcxqf7rvcg06zwj2ffnfprwhvq49nnv78psag4") (f (quote (("tokio" "tokio-current-thread" "tokio-executor") ("nightly-docs") ("default" "tokio"))))))

(define-public crate-futures-locks-0.6.0 (c (n "futures-locks") (v "0.6.0") (d (list (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "tokio") (r "^0.2.7") (f (quote ("rt-core" "rt-util"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^0.2.7") (f (quote ("sync" "macros" "rt-threaded"))) (d #t) (k 2)) (d (n "tokio-test") (r "^0.2.0") (d #t) (k 2)))) (h "0m29b66db646a9pjnhd49dkjk8bd2lsjp858bkds9n5jvn2fdi2h") (f (quote (("nightly-docs") ("default" "tokio"))))))

(define-public crate-futures-locks-0.7.0 (c (n "futures-locks") (v "0.7.0") (d (list (d (n "futures") (r "^0.3.1") (d #t) (k 2)) (d (n "futures-channel") (r "^0.3.1") (d #t) (k 0)) (d (n "futures-task") (r "^0.3.1") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("rt"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("sync" "macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "tokio-test") (r "^0.2.0") (d #t) (k 2)))) (h "12jmgrycw0ppbidz0jasj9ca6f0a4ksrx7s2g1bvw9r2nx7jvd1y") (f (quote (("default" "tokio"))))))

(define-public crate-futures-locks-0.7.1 (c (n "futures-locks") (v "0.7.1") (d (list (d (n "futures") (r "^0.3.1") (d #t) (k 2)) (d (n "futures-channel") (r "^0.3.1") (d #t) (k 0)) (d (n "futures-task") (r "^0.3.1") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("rt"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("sync" "macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "tokio-test") (r "^0.4.2") (d #t) (k 2)))) (h "01jdx6w1flwwli7drgl65z2ycd2fsjww0djmgkk6gyasczinzv25") (f (quote (("default" "tokio"))))))

