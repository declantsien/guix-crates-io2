(define-module (crates-io fu tu futures-mio) #:use-module (crates-io))

(define-public crate-futures-mio-0.1.0 (c (n "futures-mio") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.3") (d #t) (k 2)) (d (n "futures") (r "^0.1.0") (d #t) (k 0)) (d (n "futures-io") (r "^0.1.0") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "scoped-tls") (r "^0.1.0") (d #t) (k 0)) (d (n "slab") (r "^0.2.0") (d #t) (k 0)))) (h "0cy34cw4hdzhnfa38y3qwkhznlryplmr28szbkicqzy5qra5s080")))

