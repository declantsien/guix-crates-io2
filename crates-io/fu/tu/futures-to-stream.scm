(define-module (crates-io fu tu futures-to-stream) #:use-module (crates-io))

(define-public crate-futures-to-stream-0.2.0 (c (n "futures-to-stream") (v "0.2.0") (d (list (d (n "futures") (r "~0.3") (d #t) (k 0)) (d (n "tokio") (r "~1") (f (quote ("rt" "time" "macros"))) (d #t) (k 0)))) (h "0fk6cj1b0ih1f87mzzk12s36d2gqh6pcnz7j4hb5999rs2b2j06b")))

