(define-module (crates-io fu tu futures-stable) #:use-module (crates-io))

(define-public crate-futures-stable-0.2.0-alpha (c (n "futures-stable") (v "0.2.0-alpha") (d (list (d (n "futures-core") (r "^0.2.0-alpha") (k 0)) (d (n "futures-executor") (r "^0.2.0-alpha") (k 0)) (d (n "pin-api") (r "^0.1.1") (k 0)))) (h "0w5cvyf0wg91h75hkbpcc2xpgd9mslbi6srg1856ry74l38ss7hw") (f (quote (("std" "pin-api/std" "futures-core/std" "futures-executor/std") ("default" "std"))))))

(define-public crate-futures-stable-0.2.0-beta (c (n "futures-stable") (v "0.2.0-beta") (d (list (d (n "futures-core") (r "^0.2.0-beta") (k 0)) (d (n "futures-executor") (r "^0.2.0-beta") (k 0)) (d (n "pin-api") (r "^0.2.1") (k 0)))) (h "1w25pdvqb2n3iry8pk83jwxhmpjavq5zmsys2g79kjv9zilz0dnp") (f (quote (("std" "pin-api/std" "futures-core/std" "futures-executor/std") ("nightly" "pin-api/nightly") ("default" "std"))))))

(define-public crate-futures-stable-0.2.0 (c (n "futures-stable") (v "0.2.0") (d (list (d (n "futures-core") (r "^0.2.0") (k 0)) (d (n "futures-executor") (r "^0.2.0") (k 0)))) (h "0va3m30q2v7vn45dh4m8kn9jz9jz76bxx7xqi9lv25i05mbciyxy") (f (quote (("std" "futures-core/std" "futures-executor/std") ("nightly" "futures-core/nightly") ("default" "std"))))))

(define-public crate-futures-stable-0.2.1 (c (n "futures-stable") (v "0.2.1") (d (list (d (n "futures-core") (r "^0.2.1") (k 0)) (d (n "futures-executor") (r "^0.2.1") (k 0)))) (h "109fs3rsxkcm8xl0m51b888l6byqbiml12p0bckrr84lvd0ga8m2") (f (quote (("std" "futures-core/std" "futures-executor/std") ("nightly" "futures-core/nightly") ("default" "std"))))))

