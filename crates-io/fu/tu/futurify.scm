(define-module (crates-io fu tu futurify) #:use-module (crates-io))

(define-public crate-futurify-0.1.0 (c (n "futurify") (v "0.1.0") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)))) (h "0lgkxcx0syxxd9h9malc8ql9wrawmznhj0qlgx62k4rn0b358x12")))

(define-public crate-futurify-0.1.1 (c (n "futurify") (v "0.1.1") (d (list (d (n "actix-web") (r "^1.0") (d #t) (k 2)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 2)))) (h "15zwzh374vkhnjgdhijz3m5h40lrjn14v78gibxi6ka6v7yzkj62")))

(define-public crate-futurify-0.2.0 (c (n "futurify") (v "0.2.0") (d (list (d (n "actix-web") (r "^1.0") (d #t) (k 2)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 2)))) (h "18cfmb7pb1wrcrc86imq7pjw4sibk54q80vmzmhpg796ml9ghfna")))

(define-public crate-futurify-0.3.0 (c (n "futurify") (v "0.3.0") (d (list (d (n "actix-web-1") (r "^1.0") (d #t) (k 2) (p "actix-web")) (d (n "actix-web-2") (r "^2.0.0-alpha.1") (d #t) (k 2) (p "actix-web")) (d (n "futures01") (r "^0.1") (o #t) (k 0) (p "futures")) (d (n "futures03") (r "^0.3") (o #t) (d #t) (k 0) (p "futures")))) (h "0jr9ivpmjzc25xvdpw9w0ipm6jcic72ngx8992q0sxq2sdfll89v") (f (quote (("futures_03" "futures03") ("futures_01" "futures01") ("default" "futures_03"))))))

