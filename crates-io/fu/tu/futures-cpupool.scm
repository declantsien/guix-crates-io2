(define-module (crates-io fu tu futures-cpupool) #:use-module (crates-io))

(define-public crate-futures-cpupool-0.1.0 (c (n "futures-cpupool") (v "0.1.0") (d (list (d (n "crossbeam") (r "^0.2") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)))) (h "0nqnl2a1zik4x7lf7ks1bikaag48ma8xbhh330x2p9f9mdm05j03") (y #t)))

(define-public crate-futures-cpupool-0.1.1 (c (n "futures-cpupool") (v "0.1.1") (d (list (d (n "crossbeam") (r "^0.2.10") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)))) (h "1d6iqrpavr3ajj33xyir24av0h59fnwv8kiq8mjn9bgjyihcy48l")))

(define-public crate-futures-cpupool-0.1.2 (c (n "futures-cpupool") (v "0.1.2") (d (list (d (n "crossbeam") (r "^0.2.10") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)))) (h "10nvjmbjp1mhy2sbr0ns2c8zz169ah1lcfpbm2k5vynqbjr2p65v")))

(define-public crate-futures-cpupool-0.1.3 (c (n "futures-cpupool") (v "0.1.3") (d (list (d (n "crossbeam") (r "^0.2.10") (d #t) (k 0)) (d (n "futures") (r "^0.1") (f (quote ("use_std"))) (k 0)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)))) (h "0af2lbkc6mn6mqckmi46ffc6lhahjrcx44zd3vgyk3x5yvzs6j4y")))

(define-public crate-futures-cpupool-0.1.4 (c (n "futures-cpupool") (v "0.1.4") (d (list (d (n "crossbeam") (r "^0.2.10") (d #t) (k 0)) (d (n "futures") (r "^0.1") (f (quote ("use_std"))) (k 0)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)))) (h "1aya2hwdd66fxnlqnjkccjl57w41x3xxzsjq3c4ncqnz8dpkayr5") (f (quote (("with-deprecated" "futures/with-deprecated") ("default" "with-deprecated"))))))

(define-public crate-futures-cpupool-0.1.5 (c (n "futures-cpupool") (v "0.1.5") (d (list (d (n "futures") (r "^0.1") (f (quote ("use_std"))) (k 0)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)))) (h "1zy5sa1h1xcn10z2lc6xrh4z4wagliy2lfk7xbjssb79052wi0x2") (f (quote (("with-deprecated" "futures/with-deprecated") ("default" "with-deprecated"))))))

(define-public crate-futures-cpupool-0.1.6 (c (n "futures-cpupool") (v "0.1.6") (d (list (d (n "futures") (r "^0.1") (f (quote ("use_std"))) (k 0)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)))) (h "1dfj3amvzy1dhyrbcbkhjs5z3vrpcs8flgm4zpb206xrx1yrxm3p") (f (quote (("with-deprecated" "futures/with-deprecated") ("default" "with-deprecated"))))))

(define-public crate-futures-cpupool-0.1.7 (c (n "futures-cpupool") (v "0.1.7") (d (list (d (n "futures") (r "^0.1") (f (quote ("use_std"))) (k 0)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)))) (h "1pvgfvg9syg2m570858gn35p50i0aqnw63lqlnbipzlj1p64jvz8") (f (quote (("with-deprecated" "futures/with-deprecated") ("default" "with-deprecated"))))))

(define-public crate-futures-cpupool-0.1.8 (c (n "futures-cpupool") (v "0.1.8") (d (list (d (n "futures") (r "^0.1") (f (quote ("use_std"))) (k 0)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)))) (h "1r32456gpblzfvnkf60545v8acqk7gh5zhyhi1jn669k9gicv45b") (f (quote (("with-deprecated" "futures/with-deprecated") ("default" "with-deprecated"))))))

