(define-module (crates-io fu tu futures-await-test-macro) #:use-module (crates-io))

(define-public crate-futures-await-test-macro-0.3.0 (c (n "futures-await-test-macro") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("clone-impls" "full" "parsing" "printing" "proc-macro"))) (k 0)))) (h "0ff8g0rlgxh7shd9v2pm685klmnwwh00wm9l0d8009l3wrsh25ag")))

