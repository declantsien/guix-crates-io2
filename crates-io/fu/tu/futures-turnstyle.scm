(define-module (crates-io fu tu futures-turnstyle) #:use-module (crates-io))

(define-public crate-futures-turnstyle-0.1.0 (c (n "futures-turnstyle") (v "0.1.0") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)))) (h "1n9400ksxxfdymq73xflyycfybj32j2cn77rcqgv1483x6b899n9")))

(define-public crate-futures-turnstyle-1.0.0 (c (n "futures-turnstyle") (v "1.0.0") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)))) (h "049fvhrpifr790w751l15dmwpqdbp3kg2si7babwghcyvh31h4lv")))

(define-public crate-futures-turnstyle-2.0.0 (c (n "futures-turnstyle") (v "2.0.0") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)))) (h "18k0s6r7mr179003ak2w25zdjxjkjhd9czkc2qnkadsq603sapgw")))

(define-public crate-futures-turnstyle-2.0.1 (c (n "futures-turnstyle") (v "2.0.1") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)))) (h "1pln2zyljs3nn5qipdl29cvg8rp2xas0ljgkl41k5qrx0qbswm8a")))

(define-public crate-futures-turnstyle-3.0.0 (c (n "futures-turnstyle") (v "3.0.0") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)))) (h "08zk37xazk5zqnly6rfw06ip7d3f6cd4lqm11lgp9p4khplgfkfc")))

