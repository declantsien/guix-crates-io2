(define-module (crates-io fu tu futures-macro-async) #:use-module (crates-io))

(define-public crate-futures-macro-async-0.2.1 (c (n "futures-macro-async") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^0.3") (d #t) (k 0)) (d (n "quote") (r "^0.5") (d #t) (k 0)) (d (n "syn") (r "^0.13") (f (quote ("full" "fold" "parsing" "printing" "extra-traits" "proc-macro"))) (k 0)))) (h "07wdm2xz1nwa0p63rk66h08ydbgkhxcgaisv1lmfc2bjv6f2gz0r") (f (quote (("nightly" "proc-macro2/nightly"))))))

