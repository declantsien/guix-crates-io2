(define-module (crates-io fu tu futures-rate) #:use-module (crates-io))

(define-public crate-futures-rate-0.1.0 (c (n "futures-rate") (v "0.1.0") (d (list (d (n "crossbeam-queue") (r "^0.2.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (f (quote ("thread-pool"))) (d #t) (k 2)))) (h "0a1d1a2fjgf36mk5ih7486x84hnqvqdwxn2lff7fpfsd98fc4pxh")))

(define-public crate-futures-rate-0.1.1 (c (n "futures-rate") (v "0.1.1") (d (list (d (n "crossbeam-queue") (r "^0.2.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (f (quote ("thread-pool"))) (d #t) (k 2)))) (h "1fsw9kwfj16mnycj9ng0x12qfpxs8xa26d7f1pibdamg6nsvilif")))

(define-public crate-futures-rate-0.1.2 (c (n "futures-rate") (v "0.1.2") (d (list (d (n "crossbeam-queue") (r "^0.2.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (f (quote ("thread-pool"))) (d #t) (k 2)))) (h "1kbhkbvdg7jdvbh8mrndsyn3qmpgzpydmdw7wb29ks4w37gn5lm5")))

(define-public crate-futures-rate-0.1.3 (c (n "futures-rate") (v "0.1.3") (d (list (d (n "crossbeam-queue") (r "^0.2.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (f (quote ("thread-pool"))) (d #t) (k 2)))) (h "04rpmgxv4rbj1pxmbi8mq0l5x9bgslr9xk5nn09nq88fx8jpkwx4")))

(define-public crate-futures-rate-0.1.4 (c (n "futures-rate") (v "0.1.4") (d (list (d (n "crossbeam-queue") (r "^0.2.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (f (quote ("thread-pool"))) (d #t) (k 2)))) (h "0dyqg4qakb6557066gh6qk85yklhkwv6cqi1fk3la4lx4lankagi")))

(define-public crate-futures-rate-0.1.5 (c (n "futures-rate") (v "0.1.5") (d (list (d (n "crossbeam-queue") (r "^0.2.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (f (quote ("thread-pool"))) (d #t) (k 2)))) (h "01d772cp8ssigmk8igm7dyn4bj2wgc0796wp8sfqf3b2nzd9j9sp")))

