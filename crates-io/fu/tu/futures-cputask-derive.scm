(define-module (crates-io fu tu futures-cputask-derive) #:use-module (crates-io))

(define-public crate-futures-cputask-derive-0.3.0-alpha.19 (c (n "futures-cputask-derive") (v "0.3.0-alpha.19") (d (list (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("full"))) (d #t) (k 0)))) (h "1d2y1g54wlh4al366v8ba8ynazx5j3pid1ms3pvbgwh45r13nvdx")))

(define-public crate-futures-cputask-derive-0.3.0 (c (n "futures-cputask-derive") (v "0.3.0") (d (list (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("full"))) (d #t) (k 0)))) (h "1xjri61z1q3pa4gvbdhlcwhwagvikxfwrx9ni8kkad7jraizhy5p")))

