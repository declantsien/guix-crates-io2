(define-module (crates-io fu tu futures01) #:use-module (crates-io))

(define-public crate-futures01-0.1.27 (c (n "futures01") (v "0.1.27") (d (list (d (n "futures") (r "^0.1.27") (k 0)))) (h "0khszzng01mx57al5c8bqf1wb8id80fv24gglf4zd0idkpxig6r8") (f (quote (("with-deprecated" "futures/with-deprecated") ("use_std" "futures/use_std") ("nightly" "futures/nightly") ("default" "use_std" "with-deprecated"))))))

(define-public crate-futures01-0.1.28 (c (n "futures01") (v "0.1.28") (d (list (d (n "futures") (r "^0.1.28") (k 0)))) (h "1lggpzs55q4hmx7vik48ry73nnrfm2a19z81jrjhcbligk82j1v0") (f (quote (("with-deprecated" "futures/with-deprecated") ("use_std" "futures/use_std") ("nightly" "futures/nightly") ("default" "use_std" "with-deprecated"))))))

(define-public crate-futures01-0.1.29 (c (n "futures01") (v "0.1.29") (d (list (d (n "futures") (r "^0.1.29") (k 0)))) (h "08jghsm2r21a9rqyxardcrhdnga3c9dc1ij06l2p14chaazwpy3y") (f (quote (("with-deprecated" "futures/with-deprecated") ("use_std" "futures/use_std") ("nightly" "futures/nightly") ("default" "use_std" "with-deprecated"))))))

(define-public crate-futures01-0.1.30 (c (n "futures01") (v "0.1.30") (d (list (d (n "futures") (r "^0.1.30") (k 0)))) (h "026n1ivc8nj2qaxa7xrq17m58bqs2a5c229rfcjcm3i1pq340cs2") (f (quote (("with-deprecated" "futures/with-deprecated") ("use_std" "futures/use_std") ("nightly" "futures/nightly") ("default" "use_std" "with-deprecated"))))))

(define-public crate-futures01-0.1.31 (c (n "futures01") (v "0.1.31") (d (list (d (n "futures") (r "^0.1.31") (k 0)))) (h "1s9alz7cdcnkvj4w9mcznqg4wh09viqr56ss05qh76xmvfqp8gs2") (f (quote (("with-deprecated" "futures/with-deprecated") ("use_std" "futures/use_std") ("nightly" "futures/nightly") ("default" "use_std" "with-deprecated"))))))

