(define-module (crates-io fu tu futures-retry-policies-core) #:use-module (crates-io))

(define-public crate-futures-retry-policies-core-0.1.0 (c (n "futures-retry-policies-core") (v "0.1.0") (d (list (d (n "pin-project") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "0cwrgx6pbjcvy3j3di1qjzlic0jfcw7mkay6ija820sc3x2pc9r2")))

