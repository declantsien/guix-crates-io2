(define-module (crates-io fu rt furtive) #:use-module (crates-io))

(define-public crate-furtive-0.1.0 (c (n "furtive") (v "0.1.0") (h "077mns1h421fin7dzsv65hj9m8c7h6y2g21rfsr90k9a6xl9zfwh")))

(define-public crate-furtive-0.0.1 (c (n "furtive") (v "0.0.1") (h "0rygfmn92d8s6kgmfbvx155pqcsa95p87wa5gry7c69swad4xq0k")))

(define-public crate-furtive-0.1.1 (c (n "furtive") (v "0.1.1") (h "0ias63crq1zvbb4r0sjnibwiqp5pmbbq6ir5j1hd1xbxazvm99ip")))

(define-public crate-furtive-0.1.2 (c (n "furtive") (v "0.1.2") (h "0mshhxwkz80a7mzzq7y2c8qlyyp6iz24k3d45m3cp1r2hd9h4ppc")))

