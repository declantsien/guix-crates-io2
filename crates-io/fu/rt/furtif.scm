(define-module (crates-io fu rt furtif) #:use-module (crates-io))

(define-public crate-furtif-0.0.0 (c (n "furtif") (v "0.0.0") (h "0kj7im6mq5y8asicz41wl43ghvk6y7nk13y7l19vcfyk2vfi9da6") (y #t)))

(define-public crate-furtif-0.0.1 (c (n "furtif") (v "0.0.1") (h "0kijdbhina2x4nfjxkkawxa8kvvzm0ijs75hj2k9wrydyhcy9z7p") (y #t)))

(define-public crate-furtif-0.0.2 (c (n "furtif") (v "0.0.2") (h "1xz5ip4dv46mk5dg6g75gi6rw1jigr06p2lnn6dgvx9mgjz19829")))

