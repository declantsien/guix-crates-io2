(define-module (crates-io fu zi fuzion-flows) #:use-module (crates-io))

(define-public crate-fuzion-flows-0.8.1 (c (n "fuzion-flows") (v "0.8.1") (d (list (d (n "cosmwasm-schema") (r "^1.5") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.5") (d #t) (k 0)) (d (n "cw20") (r "^1.1") (d #t) (k 0)) (d (n "fuzion-std") (r "^0.8") (d #t) (k 0)) (d (n "schemars") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "05l0i7y5bcbc2ah36mn14mv93hwcg7zixpbnkx9m0z1lnflvhv9k")))

(define-public crate-fuzion-flows-0.8.3 (c (n "fuzion-flows") (v "0.8.3") (d (list (d (n "cosmwasm-schema") (r "^1.5") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.5") (d #t) (k 0)) (d (n "cw20") (r "^1.1") (d #t) (k 0)) (d (n "fuzion-std") (r "^0.8") (d #t) (k 0)) (d (n "schemars") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0ci3d328f2j5b6l6ai6py2b7f19gs7l9mdymzkynrx2mzvlqjn30")))

