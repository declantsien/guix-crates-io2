(define-module (crates-io fu zi fuzion-std) #:use-module (crates-io))

(define-public crate-fuzion-std-0.8.1 (c (n "fuzion-std") (v "0.8.1") (d (list (d (n "cosmwasm-schema") (r "^1.5") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.5") (d #t) (k 0)) (d (n "cw20") (r "^1.1") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "sha2") (r "^0.10") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "04zwbqrrh92j2sv7hszyaxkll5dgzdla7p723ccs16kf2m0b8mgy")))

(define-public crate-fuzion-std-0.8.3 (c (n "fuzion-std") (v "0.8.3") (d (list (d (n "cosmwasm-schema") (r "^1.5") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.5") (d #t) (k 0)) (d (n "cw20") (r "^1.1") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "sha2") (r "^0.10") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "19ahgyini0qj5sinq4j7w1srg8nwipmnisc3h8wy97lzg0g0dkc5")))

