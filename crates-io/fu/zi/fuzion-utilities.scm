(define-module (crates-io fu zi fuzion-utilities) #:use-module (crates-io))

(define-public crate-fuzion-utilities-0.8.3 (c (n "fuzion-utilities") (v "0.8.3") (d (list (d (n "cosmwasm-schema") (r "^1.5") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.5") (d #t) (k 0)) (d (n "cw20") (r "^1.1") (d #t) (k 0)) (d (n "fuzion-std") (r "^0.8") (d #t) (k 0)) (d (n "schemars") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0yr70why5qmffafpggfyskqjf43fkkz848fqjbp75aq2bjwvhpa4")))

