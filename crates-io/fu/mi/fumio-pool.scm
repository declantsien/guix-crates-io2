(define-module (crates-io fu mi fumio-pool) #:use-module (crates-io))

(define-public crate-fumio-pool-0.1.0 (c (n "fumio-pool") (v "0.1.0") (d (list (d (n "fumio-utils") (r "^0.1.0") (k 0)) (d (n "futures-core-preview") (r "^0.3.0-alpha.18") (d #t) (k 0)) (d (n "futures-executor-preview") (r "^0.3.0-alpha.18") (d #t) (k 0)) (d (n "futures-util-preview") (r "^0.3.0-alpha.18") (d #t) (k 0)))) (h "10gy3k4pjhmv5y9rjfsafaapl1bp2343zclgya0p863acnn634sk")))

