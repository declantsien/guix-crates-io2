(define-module (crates-io fu mi fumio) #:use-module (crates-io))

(define-public crate-fumio-0.1.0 (c (n "fumio") (v "0.1.0") (d (list (d (n "fumio-pool") (r "^0.1.0") (d #t) (k 0)) (d (n "fumio-reactor") (r "^0.1.0") (d #t) (k 0)) (d (n "fumio-utils") (r "^0.1.0") (k 0)) (d (n "futures-core-preview") (r "^0.3.0-alpha.18") (d #t) (k 0)) (d (n "futures-executor-preview") (r "^0.3.0-alpha.18") (d #t) (k 0)) (d (n "futures-preview") (r "^0.3.0-alpha.18") (f (quote ("nightly" "async-await"))) (d #t) (k 2)) (d (n "tokio-executor") (r "^0.2.0-alpha.2") (d #t) (k 0)) (d (n "tokio-timer") (r "^0.3.0-alpha.2") (d #t) (k 0)))) (h "10gad3fn2csbcjx2z9qpad1j5q8h07ib33f005z3k7p953bfid4n")))

