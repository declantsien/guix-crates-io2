(define-module (crates-io fu mi fumio-reactor) #:use-module (crates-io))

(define-public crate-fumio-reactor-0.1.0 (c (n "fumio-reactor") (v "0.1.0") (d (list (d (n "fumio-utils") (r "^0.1.0") (k 0)) (d (n "futures-core-preview") (r "^0.3.0-alpha.18") (d #t) (k 0)) (d (n "futures-executor-preview") (r "^0.3.0-alpha.18") (d #t) (k 0)) (d (n "futures-io-preview") (r "^0.3.0-alpha.18") (d #t) (k 0)) (d (n "futures-util-preview") (r "^0.3.0-alpha.18") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "mio") (r "^0.6.16") (d #t) (k 0)) (d (n "net2") (r "^0.2.33") (d #t) (k 0)))) (h "1y349lphr4n2mhlcf4l5lzl9hy21wrgdyl5m8wbr8hyxwc9w1zz2")))

