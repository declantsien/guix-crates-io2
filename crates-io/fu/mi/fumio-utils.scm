(define-module (crates-io fu mi fumio-utils) #:use-module (crates-io))

(define-public crate-fumio-utils-0.1.0 (c (n "fumio-utils") (v "0.1.0") (d (list (d (n "futures-executor-preview") (r "^0.3.0-alpha.18") (d #t) (k 0)) (d (n "futures-util-preview") (r "^0.3.0-alpha.18") (o #t) (d #t) (k 0)))) (h "0xqhkfcwcc8kpds3r21kxdgw8zj0aqp3ry317afzqcd2zhf3da7l") (f (quote (("park-thread" "futures-util-preview") ("default" "park-thread"))))))

