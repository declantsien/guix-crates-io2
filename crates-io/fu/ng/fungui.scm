(define-module (crates-io fu ng fungui) #:use-module (crates-io))

(define-public crate-fungui-0.1.0 (c (n "fungui") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "fnv") (r "^1.0.6") (d #t) (k 0)) (d (n "fungui_syntax") (r "^0.1.0") (d #t) (k 0)) (d (n "ref_filter_map") (r "^1.0.1") (d #t) (k 0)))) (h "1v8s3l4nxj4knh7yh52h9p784hbqc20850c2934p4iznnhx1zd5x") (f (quote (("tests"))))))

(define-public crate-fungui-0.1.1 (c (n "fungui") (v "0.1.1") (d (list (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "fnv") (r "^1.0.6") (d #t) (k 0)) (d (n "fungui_syntax") (r "^0.1.0") (d #t) (k 0)) (d (n "ref_filter_map") (r "^1.0.1") (d #t) (k 0)))) (h "1ihg4pdc96dfv9jvisvb36n98znfxpybywnvjwnn8ix07nfhygjc") (f (quote (("tests"))))))

