(define-module (crates-io fu ng fungui_syntax) #:use-module (crates-io))

(define-public crate-fungui_syntax-0.1.0 (c (n "fungui_syntax") (v "0.1.0") (d (list (d (n "combine") (r "^3.6.1") (d #t) (k 0)) (d (n "fnv") (r "^1.0.6") (d #t) (k 0)))) (h "19i0djr84s7w8gm6g079hbg8px9vb9y2y8kyvsml9frqr1l0mvnz")))

(define-public crate-fungui_syntax-0.1.1 (c (n "fungui_syntax") (v "0.1.1") (d (list (d (n "combine") (r "^3.6.1") (d #t) (k 0)) (d (n "fnv") (r "^1.0.6") (d #t) (k 0)))) (h "0ld4hqchi0g9sqf691wqqmr690fs6sh9gg5vggg81q120jikyhwy")))

