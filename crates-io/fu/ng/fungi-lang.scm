(define-module (crates-io fu ng fungi-lang) #:use-module (crates-io))

(define-public crate-fungi-lang-0.1.0 (c (n "fungi-lang") (v "0.1.0") (d (list (d (n "adapton") (r "^0.3") (d #t) (k 0)) (d (n "iodyn") (r "^0.2") (d #t) (k 0)))) (h "0p4yyf9m2npdq36php26ky96jcbg30p79q6wk2hwdmshi2n89kpw")))

(define-public crate-fungi-lang-0.1.1 (c (n "fungi-lang") (v "0.1.1") (d (list (d (n "adapton") (r "^0.3") (d #t) (k 0)) (d (n "iodyn") (r "^0.2") (d #t) (k 0)))) (h "07r10gxr08fkr6x09fwpbi66546fc0zikr8fjvy0qhii61is78wc")))

(define-public crate-fungi-lang-0.1.2 (c (n "fungi-lang") (v "0.1.2") (d (list (d (n "adapton") (r "^0.3") (d #t) (k 0)) (d (n "iodyn") (r "^0.2") (d #t) (k 0)))) (h "0p3z1b3v62bjg1h2jdd6wxm6lgv1jbdx99mqira094nq6g9ck8k8")))

(define-public crate-fungi-lang-0.1.3 (c (n "fungi-lang") (v "0.1.3") (d (list (d (n "adapton") (r "^0.3") (d #t) (k 0)) (d (n "iodyn") (r "^0.2") (d #t) (k 0)))) (h "0v1q6kcfhbdsx84n29hmph6s7rdinllk18ix4zwqwgrym9wxnsxz")))

(define-public crate-fungi-lang-0.1.4 (c (n "fungi-lang") (v "0.1.4") (d (list (d (n "adapton") (r "^0.3") (d #t) (k 0)) (d (n "iodyn") (r "^0.2") (d #t) (k 0)))) (h "0mc2vkiy7755px4wmv7xg7h6l2z80g4ikfk4as1460bgfppgcncz")))

(define-public crate-fungi-lang-0.1.5 (c (n "fungi-lang") (v "0.1.5") (d (list (d (n "adapton") (r "^0.3") (d #t) (k 0)) (d (n "iodyn") (r "^0.2") (d #t) (k 0)))) (h "1kx8qxqiqqiyv04p0mplpmwl4clb4h5xcvplq4yv2p4a9i9hkv20")))

(define-public crate-fungi-lang-0.1.6 (c (n "fungi-lang") (v "0.1.6") (d (list (d (n "adapton") (r "^0.3") (d #t) (k 0)) (d (n "iodyn") (r "^0.2") (d #t) (k 0)))) (h "1bnaj3y4qi0vw62ll8c665n5jwp8wi83v1nc43rc5d28qlkivgzb")))

(define-public crate-fungi-lang-0.1.7 (c (n "fungi-lang") (v "0.1.7") (d (list (d (n "adapton") (r "^0.3") (d #t) (k 0)) (d (n "iodyn") (r "^0.2") (d #t) (k 0)))) (h "1pmx74wzcwx751xr9n8ryp2d0m5iq6dj4j803a27rn779y7439np")))

(define-public crate-fungi-lang-0.1.8 (c (n "fungi-lang") (v "0.1.8") (d (list (d (n "adapton") (r "^0.3") (d #t) (k 0)) (d (n "iodyn") (r "^0.2") (d #t) (k 0)))) (h "0rzjswlp0i5jmllv2r4ln6946j6j6qmhmnzafmjgm7m0a17hi2fj")))

(define-public crate-fungi-lang-0.1.10 (c (n "fungi-lang") (v "0.1.10") (d (list (d (n "adapton") (r "^0.3") (d #t) (k 0)) (d (n "iodyn") (r "^0.2") (d #t) (k 0)))) (h "0cydv68f1gazqwck7zsgh560z64dg0q5kgzy54yf0vmwjffh6r8b")))

(define-public crate-fungi-lang-0.1.11 (c (n "fungi-lang") (v "0.1.11") (d (list (d (n "adapton") (r "^0.3") (d #t) (k 0)) (d (n "iodyn") (r "^0.2") (d #t) (k 0)))) (h "0zdazvzqz8fm3s1p1ax2s87nivqa7bm2k57l14isz69cz6xqga8l")))

(define-public crate-fungi-lang-0.1.12 (c (n "fungi-lang") (v "0.1.12") (d (list (d (n "adapton") (r "^0.3") (d #t) (k 0)) (d (n "iodyn") (r "^0.2") (d #t) (k 0)))) (h "0slfawb79c64l2wm3wb06a917if8bs9cnv8wrpcq97dw4wckazhq")))

(define-public crate-fungi-lang-0.1.13 (c (n "fungi-lang") (v "0.1.13") (d (list (d (n "adapton") (r "^0.3") (d #t) (k 0)) (d (n "iodyn") (r "^0.2") (d #t) (k 0)))) (h "1lpc3ss70si43pd1sy256z7j6xz8nby1sy592733r2pg03zcs4s7")))

(define-public crate-fungi-lang-0.1.14 (c (n "fungi-lang") (v "0.1.14") (d (list (d (n "adapton") (r "^0.3") (d #t) (k 0)) (d (n "iodyn") (r "^0.2") (d #t) (k 0)))) (h "0jrfaswqj5ajasyayzcg2aj5r8q8l1xy2w9x3bsy4ng3za5dhkw7")))

(define-public crate-fungi-lang-0.1.15 (c (n "fungi-lang") (v "0.1.15") (d (list (d (n "adapton") (r "^0.3") (d #t) (k 0)) (d (n "iodyn") (r "^0.2") (d #t) (k 0)))) (h "0bpxcg1q6rzw3razjx2b8qppcx6b7qclsn0d20xrg7lrzk3g17cj")))

(define-public crate-fungi-lang-0.1.16 (c (n "fungi-lang") (v "0.1.16") (d (list (d (n "adapton") (r "^0.3") (d #t) (k 0)) (d (n "iodyn") (r "^0.2") (d #t) (k 0)))) (h "183znlbrwr6l5sny0zwjd6f1vhj0xll79flxl527msdw9hkk80cj")))

(define-public crate-fungi-lang-0.1.17 (c (n "fungi-lang") (v "0.1.17") (d (list (d (n "adapton") (r "^0.3") (d #t) (k 0)) (d (n "iodyn") (r "^0.2") (d #t) (k 0)))) (h "0j2q3wiqqzvpnrbbaq727d9v9008z4psmk7azarjk6bnlwl8brlk")))

(define-public crate-fungi-lang-0.1.18 (c (n "fungi-lang") (v "0.1.18") (d (list (d (n "adapton") (r "^0.3") (d #t) (k 0)) (d (n "iodyn") (r "^0.2") (d #t) (k 0)))) (h "13x2rgcsxr162n6l0sw1s2s4n34lh5687209viyg9ikz45a2d9rv")))

(define-public crate-fungi-lang-0.1.19 (c (n "fungi-lang") (v "0.1.19") (d (list (d (n "adapton") (r "^0.3") (d #t) (k 0)) (d (n "iodyn") (r "^0.2") (d #t) (k 0)))) (h "0qnalphh32g3yg8nri0r8w6q3xc73vv26zxarm0c2jymq8n0gk8w")))

(define-public crate-fungi-lang-0.1.20 (c (n "fungi-lang") (v "0.1.20") (d (list (d (n "adapton") (r "^0.3") (d #t) (k 0)) (d (n "iodyn") (r "^0.2") (d #t) (k 0)))) (h "1gm63c5k1sqwnkgy8yk31avn450dk0hfgj33rkhbzyajgq9gxvhh")))

(define-public crate-fungi-lang-0.1.21 (c (n "fungi-lang") (v "0.1.21") (d (list (d (n "adapton") (r "^0.3") (d #t) (k 0)) (d (n "iodyn") (r "^0.2") (d #t) (k 0)))) (h "0f09p6y8irq42r7172vihkvi387h1p92bvpl8gpc2kw8wylhhxxk")))

(define-public crate-fungi-lang-0.1.22 (c (n "fungi-lang") (v "0.1.22") (d (list (d (n "adapton") (r "^0.3") (d #t) (k 0)) (d (n "iodyn") (r "^0.2") (d #t) (k 0)))) (h "0hrkg381dbmy85cqv9djyq19plj88pdsp6x1l4b0rq9raqihg56c")))

(define-public crate-fungi-lang-0.1.23 (c (n "fungi-lang") (v "0.1.23") (d (list (d (n "adapton") (r "^0.3") (d #t) (k 0)) (d (n "iodyn") (r "^0.2") (d #t) (k 0)))) (h "1y33lfdnijcsprd3xq9b0i3c4vj8isxl96l6xlmqs0qx45fhnvhx")))

(define-public crate-fungi-lang-0.1.24 (c (n "fungi-lang") (v "0.1.24") (d (list (d (n "adapton") (r "^0.3") (d #t) (k 0)) (d (n "iodyn") (r "^0.2") (d #t) (k 0)))) (h "1fhcvz6idnz2n4db5a2bh6y4fdd0yz0gwg6mwmcld5lwdi2bnacd")))

(define-public crate-fungi-lang-0.1.25 (c (n "fungi-lang") (v "0.1.25") (d (list (d (n "adapton") (r "^0.3") (d #t) (k 0)) (d (n "iodyn") (r "^0.2") (d #t) (k 0)))) (h "13yn3ni46qq7pzw9vjk8a3nra8dwdljcnqf6gi1dxcm87ibsc1di")))

(define-public crate-fungi-lang-0.1.26 (c (n "fungi-lang") (v "0.1.26") (d (list (d (n "adapton") (r "^0.3") (d #t) (k 0)) (d (n "iodyn") (r "^0.2") (d #t) (k 0)))) (h "0lxqwmkda07a4dq242nq5y8zi1brhjh9m3h5fjcv6iwzwipam49s")))

(define-public crate-fungi-lang-0.1.27 (c (n "fungi-lang") (v "0.1.27") (d (list (d (n "adapton") (r "^0.3") (d #t) (k 0)) (d (n "iodyn") (r "^0.2") (d #t) (k 0)))) (h "1m4c0fay9fhvspsp6z0dxvrkya7z9629v27yspazcnk2mpw87mw0")))

(define-public crate-fungi-lang-0.1.28 (c (n "fungi-lang") (v "0.1.28") (d (list (d (n "adapton") (r "^0.3") (d #t) (k 0)) (d (n "iodyn") (r "^0.2") (d #t) (k 0)))) (h "13s2kz2vn3fynn5ppfjr9qq7ra2p839chsp7yy8b2ssg0gd4ihi4")))

(define-public crate-fungi-lang-0.1.29 (c (n "fungi-lang") (v "0.1.29") (d (list (d (n "adapton") (r "^0.3") (d #t) (k 0)) (d (n "iodyn") (r "^0.2") (d #t) (k 0)))) (h "14r05nnbw5gvac26kv7wppams6cij7aa2jjy4ffv010yz1sqb27b")))

(define-public crate-fungi-lang-0.1.30 (c (n "fungi-lang") (v "0.1.30") (d (list (d (n "adapton") (r "^0.3") (d #t) (k 0)) (d (n "iodyn") (r "^0.2") (d #t) (k 0)))) (h "04ybbigw9m7r7mmx26mrss9854rpxhcl0c7y79lpa3m5xmw4fc47")))

(define-public crate-fungi-lang-0.1.31 (c (n "fungi-lang") (v "0.1.31") (d (list (d (n "adapton") (r "^0.3") (d #t) (k 0)) (d (n "iodyn") (r "^0.2") (d #t) (k 0)))) (h "0l22xr56f9jmggb8mvgk8yillf669j0n14m83x28kqpbzinr03rf")))

(define-public crate-fungi-lang-0.1.32 (c (n "fungi-lang") (v "0.1.32") (d (list (d (n "adapton") (r "^0.3") (d #t) (k 0)) (d (n "iodyn") (r "^0.2") (d #t) (k 0)))) (h "13jrc19qivl35fssrrmcwd72rak0wgvzgq80wkv8l3kpn9mypq6n")))

(define-public crate-fungi-lang-0.1.33 (c (n "fungi-lang") (v "0.1.33") (d (list (d (n "adapton") (r "^0.3") (d #t) (k 0)) (d (n "iodyn") (r "^0.2") (d #t) (k 0)))) (h "1mkplp6svaq9xci62wfvjbskq2pbcknrzr1wra3wg36apxg0wmqz")))

(define-public crate-fungi-lang-0.1.34 (c (n "fungi-lang") (v "0.1.34") (d (list (d (n "adapton") (r "^0.3") (d #t) (k 0)) (d (n "iodyn") (r "^0.2") (d #t) (k 0)))) (h "1zg6sd2xmh12wwwljflnpg1al6jmp5j7p45c2zdab09vl78win85")))

(define-public crate-fungi-lang-0.1.35 (c (n "fungi-lang") (v "0.1.35") (d (list (d (n "adapton") (r "^0.3") (d #t) (k 0)) (d (n "iodyn") (r "^0.2") (d #t) (k 0)))) (h "11bdy4h1jkvx6q1k0pz6p2xj03cjif0gl6wb23kdb6h20j1cfxqr")))

(define-public crate-fungi-lang-0.1.36 (c (n "fungi-lang") (v "0.1.36") (d (list (d (n "adapton") (r "^0.3") (d #t) (k 0)) (d (n "iodyn") (r "^0.2") (d #t) (k 0)))) (h "0xs5lh2bk5zqqx19qg22b1nxvpdnbbnaba21cb5jsqwsprx3pv79")))

(define-public crate-fungi-lang-0.1.37 (c (n "fungi-lang") (v "0.1.37") (d (list (d (n "adapton") (r "^0.3") (d #t) (k 0)) (d (n "iodyn") (r "^0.2") (d #t) (k 0)))) (h "13bvabm5rcgnal6ads32gsa56hwha687pd8mcb0abhdq42mfi8fg")))

(define-public crate-fungi-lang-0.1.38 (c (n "fungi-lang") (v "0.1.38") (d (list (d (n "adapton") (r "^0.3") (d #t) (k 0)) (d (n "iodyn") (r "^0.2") (d #t) (k 0)))) (h "11cpj7ak80ag9j40ig83sxqmjkw5ib9513mk63km11yz291wm6bf")))

(define-public crate-fungi-lang-0.1.39 (c (n "fungi-lang") (v "0.1.39") (d (list (d (n "adapton") (r "^0.3") (d #t) (k 0)) (d (n "iodyn") (r "^0.2") (d #t) (k 0)))) (h "0vpg6zxwmdvi7hlxk5rvdgirnqsk7jfvgahpd2dvflhms601932h")))

(define-public crate-fungi-lang-0.1.40 (c (n "fungi-lang") (v "0.1.40") (d (list (d (n "adapton") (r "^0.3") (d #t) (k 0)) (d (n "iodyn") (r "^0.2") (d #t) (k 0)))) (h "1q4z9vz8wqks3sb0n4401mvmgn317di164nqp64m9h47x5di4sjd")))

(define-public crate-fungi-lang-0.1.41 (c (n "fungi-lang") (v "0.1.41") (d (list (d (n "adapton") (r "^0.3") (d #t) (k 0)) (d (n "iodyn") (r "^0.2") (d #t) (k 0)))) (h "06c4d5p0wazdzmxzpjbi5pz0yj58i19pq2g12njgjc5790jnvfc8")))

(define-public crate-fungi-lang-0.1.42 (c (n "fungi-lang") (v "0.1.42") (d (list (d (n "adapton") (r "^0.3") (d #t) (k 0)) (d (n "iodyn") (r "^0.2") (d #t) (k 0)))) (h "1kvmn1schxlzm30k8vsm6chk92j75mbyp01na2q7mw0n3346ads9")))

(define-public crate-fungi-lang-0.1.43 (c (n "fungi-lang") (v "0.1.43") (d (list (d (n "adapton") (r "^0.3") (d #t) (k 0)) (d (n "iodyn") (r "^0.2") (d #t) (k 0)))) (h "1h0wr2c8n31jn6v7dd76shs2x2msqrj3ih3jnykij6mnvn1rl3kx")))

(define-public crate-fungi-lang-0.1.44 (c (n "fungi-lang") (v "0.1.44") (d (list (d (n "adapton") (r "^0.3") (d #t) (k 0)) (d (n "iodyn") (r "^0.2") (d #t) (k 0)))) (h "1qcip9i4h5v12maxagmxbkkw3zgayp8mawlf99hr58sqr1j9spjg")))

(define-public crate-fungi-lang-0.1.45 (c (n "fungi-lang") (v "0.1.45") (d (list (d (n "adapton") (r "^0.3") (d #t) (k 0)) (d (n "iodyn") (r "^0.2") (d #t) (k 0)))) (h "0yrdh9yljrg13nhrgvdn4ip81s6bhpj6cb01a2177avsfpfr9l7d")))

(define-public crate-fungi-lang-0.1.46 (c (n "fungi-lang") (v "0.1.46") (d (list (d (n "adapton") (r "^0.3") (d #t) (k 0)) (d (n "iodyn") (r "^0.2") (d #t) (k 0)))) (h "1m0i8y2vq041bhaijnhd0mhgqaslvxdd5qflrf7hrd63m7h811rj")))

(define-public crate-fungi-lang-0.1.47 (c (n "fungi-lang") (v "0.1.47") (d (list (d (n "adapton") (r "^0.3") (d #t) (k 0)) (d (n "iodyn") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("rc"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0i5n64kp7kd90j6301j8kfhssli3ksnqk5r4shkv8wcv9pkcma5a")))

(define-public crate-fungi-lang-0.1.48 (c (n "fungi-lang") (v "0.1.48") (d (list (d (n "adapton") (r "^0.3") (d #t) (k 0)) (d (n "iodyn") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("rc"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1mclqy86rqs7mdzd9akscq94g1gqz1zya5j9lf1hpwzrp368bsf5")))

(define-public crate-fungi-lang-0.1.49 (c (n "fungi-lang") (v "0.1.49") (d (list (d (n "adapton") (r "^0.3") (d #t) (k 0)) (d (n "iodyn") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("rc"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "17b54v24f7zf5n3w5i74x79i1gccp6vr64rmzvd4vf2ip326zh4l")))

(define-public crate-fungi-lang-0.1.50 (c (n "fungi-lang") (v "0.1.50") (d (list (d (n "adapton") (r "^0.3") (d #t) (k 0)) (d (n "iodyn") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("rc"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1lph93hc642rr889k7jkk0dgz3g8r536nywvlmckmgh6lczfxmvi")))

(define-public crate-fungi-lang-0.1.51 (c (n "fungi-lang") (v "0.1.51") (d (list (d (n "adapton") (r "^0.3") (d #t) (k 0)) (d (n "iodyn") (r "^0.2") (d #t) (k 0)) (d (n "nom") (r "^4.0") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("rc"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "06vgb5s1a3qmmcaflzm51xqqf9469pmb0sz7n03dcwi537s4ij57")))

(define-public crate-fungi-lang-0.1.52 (c (n "fungi-lang") (v "0.1.52") (d (list (d (n "adapton") (r "^0.3") (d #t) (k 0)) (d (n "iodyn") (r "^0.2") (d #t) (k 0)) (d (n "nom") (r "^4.0") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("rc"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1yx652dxdqlr5x3k2xnzkm0rkf2kz7pk2zz8wxrk759blkaxnxh0")))

(define-public crate-fungi-lang-0.1.53 (c (n "fungi-lang") (v "0.1.53") (d (list (d (n "adapton") (r "^0.3") (d #t) (k 0)) (d (n "iodyn") (r "^0.2") (d #t) (k 0)) (d (n "nom") (r "^4.0") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("rc"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1wx1kjwaqc1c017d8c0708fkdq4b1sc0nyc2y5chsksn1is18bqy")))

(define-public crate-fungi-lang-0.1.54 (c (n "fungi-lang") (v "0.1.54") (d (list (d (n "adapton") (r "^0.3") (d #t) (k 0)) (d (n "iodyn") (r "^0.2") (d #t) (k 0)) (d (n "nom") (r "^4.0") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("rc"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "063lzimqmxsa86yww05xnfbz1b10d0xna06289gj8q5dbi93li4s")))

(define-public crate-fungi-lang-0.1.55 (c (n "fungi-lang") (v "0.1.55") (d (list (d (n "adapton") (r "^0.3") (d #t) (k 0)) (d (n "iodyn") (r "^0.2") (d #t) (k 0)) (d (n "nom") (r "^4.0") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("rc"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0ak2kblzibdp8cwc1nccqajfc65wg5y82zba7k1ifa19z95n78b1")))

(define-public crate-fungi-lang-0.1.56 (c (n "fungi-lang") (v "0.1.56") (d (list (d (n "adapton") (r "^0.3") (d #t) (k 0)) (d (n "iodyn") (r "^0.2") (d #t) (k 0)) (d (n "nom") (r "^4.0") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("rc"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1m0dhd2h4bbkcmq5prg2vcmib6hd6fka5alww2hqw8dq5mndflmv")))

(define-public crate-fungi-lang-0.1.57 (c (n "fungi-lang") (v "0.1.57") (d (list (d (n "adapton") (r "^0.3") (d #t) (k 0)) (d (n "iodyn") (r "^0.2") (d #t) (k 0)) (d (n "nom") (r "^4.0") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("rc"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0djymvj09hd5dznbkihx2n39y6dj1pgswzrjjw6vw5bsj1qda2s7")))

(define-public crate-fungi-lang-0.1.58 (c (n "fungi-lang") (v "0.1.58") (d (list (d (n "adapton") (r "^0.3") (d #t) (k 0)) (d (n "nom") (r "^4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("rc"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0k9283rbrans8dwh2hgzlng8whnrj5r738ilvxbphrp20mpbc1vg")))

(define-public crate-fungi-lang-0.1.59 (c (n "fungi-lang") (v "0.1.59") (d (list (d (n "adapton") (r "^0.3") (d #t) (k 0)) (d (n "nom") (r "^4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("rc"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0lds7i1bj8p9q2awm4p622w1dx4g8ql1s1lcrfpk7rnsyj2rfxf4")))

(define-public crate-fungi-lang-0.1.60 (c (n "fungi-lang") (v "0.1.60") (d (list (d (n "adapton") (r "^0.3") (d #t) (k 0)) (d (n "nom") (r "^4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("rc"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "05s7r96594p2hjcc8w46wz8rnz7q5l53bg7hw35izn2dcmhc0a32")))

(define-public crate-fungi-lang-0.1.61 (c (n "fungi-lang") (v "0.1.61") (d (list (d (n "adapton") (r "^0.3") (d #t) (k 0)) (d (n "nom") (r "^4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("rc"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1wa1glkk08bvqi3s2m8qgm38sad8klym2h3sbhj2fpi9pwgqvvrj")))

(define-public crate-fungi-lang-0.1.62 (c (n "fungi-lang") (v "0.1.62") (d (list (d (n "adapton") (r "^0.3") (d #t) (k 0)) (d (n "nom") (r "^4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("rc"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1i6qpkwi8y8rnshnxr09hvb0ajp3481i72adiwygbrbv6xvpscic")))

(define-public crate-fungi-lang-0.1.63 (c (n "fungi-lang") (v "0.1.63") (d (list (d (n "adapton") (r "^0.3") (d #t) (k 0)) (d (n "nom") (r "^4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("rc"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0vzk0075y7bmlap9siqw84r1k7x0pg1fcp6zabbdsc9rj3wc6mi2")))

