(define-module (crates-io fu ng fungtaai) #:use-module (crates-io))

(define-public crate-fungtaai-0.1.0 (c (n "fungtaai") (v "0.1.0") (d (list (d (n "aesni") (r "^0.1") (d #t) (k 2)) (d (n "byteorder") (r "^1") (f (quote ("i128"))) (k 0)) (d (n "digest") (r "^0.6") (d #t) (k 2)) (d (n "sha2") (r "^0.6") (d #t) (k 2)))) (h "0n5xliy68yvf9pi3k5nhq2za3iyrsnk36hl2pfsgl4bscgzr4ivk")))

(define-public crate-fungtaai-0.1.1 (c (n "fungtaai") (v "0.1.1") (d (list (d (n "aesni") (r "^0.1") (d #t) (k 2)) (d (n "byteorder") (r "^1") (f (quote ("i128"))) (k 0)) (d (n "digest") (r "^0.6") (d #t) (k 2)) (d (n "sha2") (r "^0.6") (d #t) (k 2)))) (h "1sc3jmw0y7gj80cf3njzb1sj8dnwyzk86bag795zka2r49rayb6m")))

