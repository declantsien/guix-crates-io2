(define-module (crates-io fu ns funst) #:use-module (crates-io))

(define-public crate-funst-0.1.0 (c (n "funst") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "ordered-float") (r "^2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "00xfcvjafib65ly20qfhdzp48xdidprxmvkk3hwnqyl3sfzwir78")))

(define-public crate-funst-0.1.1 (c (n "funst") (v "0.1.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "ordered-float") (r "^2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1f8qcglrx4vi95xrc9wm4rdxrkdlhq7vxxci8vbnccm78pkrxmwz")))

