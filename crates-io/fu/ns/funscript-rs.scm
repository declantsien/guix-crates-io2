(define-module (crates-io fu ns funscript-rs) #:use-module (crates-io))

(define-public crate-funscript-rs-0.5.0 (c (n "funscript-rs") (v "0.5.0") (d (list (d (n "mint") (r "^0.5.9") (d #t) (k 0)) (d (n "native-dialog") (r "^0.6.3") (d #t) (k 0)) (d (n "ramer_douglas_peucker") (r "^0.2.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.92") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "1l3z3lxaj9jclvxgrn2vpc2i2b5kswcx8i5fn1sqysabw3z2n55s") (y #t)))

