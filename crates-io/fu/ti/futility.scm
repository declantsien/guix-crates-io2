(define-module (crates-io fu ti futility) #:use-module (crates-io))

(define-public crate-futility-0.1.0 (c (n "futility") (v "0.1.0") (d (list (d (n "color-eyre") (r "^0.6") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0mr7mrw8slryiixzs2fji507bd9kwb69rwdl04nxd0wm77bb6rjg")))

(define-public crate-futility-0.1.1 (c (n "futility") (v "0.1.1") (d (list (d (n "color-eyre") (r "^0.6") (d #t) (k 2)) (d (n "futility-try-catch") (r "^0.1.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0w8qjsxqd36q01nqq3n7zkwy5zwdf19wlihcp5nllg2sxf50hqqx")))

