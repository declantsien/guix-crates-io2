(define-module (crates-io fu ti futilities) #:use-module (crates-io))

(define-public crate-futilities-0.1.1 (c (n "futilities") (v "0.1.1") (d (list (d (n "quick-xml") (r "^0.21.0") (d #t) (k 0)))) (h "01k9hdnn8w6drjrrgxmazwma7vggsmakwy57fdhnmv8i30jw0v2f")))

(define-public crate-futilities-0.1.2 (c (n "futilities") (v "0.1.2") (d (list (d (n "quick-xml") (r "^0.21.0") (d #t) (k 0)))) (h "1fvb7w10zmfvx31f3f4w7dbkmdyypxyb289rxf5yd8brbmn1qyad")))

(define-public crate-futilities-0.1.3 (c (n "futilities") (v "0.1.3") (d (list (d (n "quick-xml") (r "^0.21.0") (d #t) (k 0)))) (h "0whmd5x4n43hlr355q7kcprjgd7lwwck4462km4mznis0792kzg2")))

(define-public crate-futilities-0.1.4 (c (n "futilities") (v "0.1.4") (d (list (d (n "quick-xml") (r "^0.21.0") (d #t) (k 0)))) (h "1rj897vz2mh2b0j3q0mjsfqbgkdrw3b4rkvvqlq60drk129gib0l")))

(define-public crate-futilities-0.1.5 (c (n "futilities") (v "0.1.5") (d (list (d (n "quick-xml") (r "^0.21.0") (d #t) (k 0)))) (h "1sgbsv31j8i3hagw41139f1pmrra51wf3ql7jv3g0645hdfal7jl")))

