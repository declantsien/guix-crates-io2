(define-module (crates-io fu gu fugue-ghidra) #:use-module (crates-io))

(define-public crate-fugue-ghidra-0.2.0 (c (n "fugue-ghidra") (v "0.2.0") (d (list (d (n "fugue-db") (r "^0.2") (d #t) (k 0)) (d (n "tempfile") (r "=3.4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)) (d (n "which") (r "^4") (d #t) (k 0)) (d (n "whoami") (r "^1") (d #t) (k 0)))) (h "1k8g7pxw2rp5y30wxzs5k0hp0l7f2s1f6ggz1h63nk8l2lh9zv32")))

