(define-module (crates-io fu gu fugue-fp) #:use-module (crates-io))

(define-public crate-fugue-fp-0.2.2 (c (n "fugue-fp") (v "0.2.2") (d (list (d (n "fugue-bv") (r "^0.2") (f (quote ("bigint"))) (k 0)) (d (n "fugue-ir") (r "^0.2") (d #t) (k 0)) (d (n "rug") (r "^1") (f (quote ("float"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "119rf95a97qh5lbz6l8fw9isvv4r2n86zmdgdq5iwf3hcm8323x0")))

