(define-module (crates-io fu gu fuguex-hooks) #:use-module (crates-io))

(define-public crate-fuguex-hooks-0.2.6 (c (n "fuguex-hooks") (v "0.2.6") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "fugue") (r "^0.2") (d #t) (k 0)) (d (n "fuguex-state") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "02cwbbvii2qk48zdd7qrw4117j2knfnvzlzjdhx4zbmijv2lgmla")))

