(define-module (crates-io fu gu fugue-idapro) #:use-module (crates-io))

(define-public crate-fugue-idapro-0.2.1 (c (n "fugue-idapro") (v "0.2.1") (d (list (d (n "fugue-db") (r "^0.2") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)) (d (n "which") (r "^4") (d #t) (k 0)))) (h "0qmi0c2721d52c7icg8pphiqkrl1r6n3ffnqghm9clq3qayyb7q2")))

