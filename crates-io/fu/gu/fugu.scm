(define-module (crates-io fu gu fugu) #:use-module (crates-io))

(define-public crate-fugu-0.0.0 (c (n "fugu") (v "0.0.0") (h "0ba0sm4gri9q2d0dmvn7vfyw3icjpp765hwl5p9dgj6ww1pzcnwn")))

(define-public crate-fugu-0.1.0 (c (n "fugu") (v "0.1.0") (d (list (d (n "glow") (r "^0.11.2") (d #t) (k 0)) (d (n "glutin") (r "^0.28.0") (d #t) (k 2)) (d (n "image") (r "^0.24.1") (d #t) (k 2)))) (h "1id7lfywgw7i5x7ribhc4nxyqzxpsg60m1v29ij1gr7bwdfvwl1s") (r "1.56")))

