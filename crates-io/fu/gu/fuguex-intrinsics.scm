(define-module (crates-io fu gu fuguex-intrinsics) #:use-module (crates-io))

(define-public crate-fuguex-intrinsics-0.2.2 (c (n "fuguex-intrinsics") (v "0.2.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "dyn-clone") (r "^1") (d #t) (k 0)) (d (n "fugue") (r "^0.2") (d #t) (k 0)) (d (n "fuguex-state") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1wnmwsr4jqp8qwk90bj9hf66h0305yl7nvcb90xpkqjvv9da5k5y")))

