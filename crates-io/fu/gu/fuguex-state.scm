(define-module (crates-io fu gu fuguex-state) #:use-module (crates-io))

(define-public crate-fuguex-state-0.2.19 (c (n "fuguex-state") (v "0.2.19") (d (list (d (n "fugue") (r "^0.2") (k 0)) (d (n "fuguex-state-derive") (r "^0.2") (d #t) (k 0)) (d (n "iset") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "ustr") (r "^0.9") (d #t) (k 0)))) (h "10qnbnhifgr3l3ixc4wk42a23zi405xa4n1ygivgyf7jhs5a0qk9")))

(define-public crate-fuguex-state-0.2.20 (c (n "fuguex-state") (v "0.2.20") (d (list (d (n "fugue") (r "^0.2") (k 0)) (d (n "fuguex-state-derive") (r "^0.2") (d #t) (k 0)) (d (n "iset") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "ustr") (r "^0.9") (d #t) (k 0)))) (h "1m734ailpfhpp33y74rrj49p20x8bmw8bg4q79sdxk9rb53j0acv")))

(define-public crate-fuguex-state-0.2.21 (c (n "fuguex-state") (v "0.2.21") (d (list (d (n "fugue") (r "^0.2") (k 0)) (d (n "fuguex-state-derive") (r "^0.2") (d #t) (k 0)) (d (n "iset") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "ustr") (r "^0.9") (d #t) (k 0)))) (h "0c7w1c7hzy8bnq5jhs4c0j50n5x7swsx7hd7z2sk666l6wlfrjpp")))

(define-public crate-fuguex-state-0.2.22 (c (n "fuguex-state") (v "0.2.22") (d (list (d (n "fugue") (r "^0.2") (k 0)) (d (n "fuguex-state-derive") (r "^0.2") (d #t) (k 0)) (d (n "iset") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "ustr") (r "^0.9") (d #t) (k 0)))) (h "0ax0g9ych4wn11bvgzw9fky4kvaw2mdhz2isyfvjim87kmby980f")))

