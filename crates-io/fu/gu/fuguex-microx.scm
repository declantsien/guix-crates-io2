(define-module (crates-io fu gu fuguex-microx) #:use-module (crates-io))

(define-public crate-fuguex-microx-0.1.3 (c (n "fuguex-microx") (v "0.1.3") (d (list (d (n "fugue") (r "^0.2") (d #t) (k 0)) (d (n "fuguex-hooks") (r "^0.2") (d #t) (k 0)) (d (n "fuguex-state") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1a5va2mlynjr4nhcs9yicjd2y1jb3g4wqhiqjb8mm87dyqr9v2lm")))

