(define-module (crates-io fu gu fuguex-loader) #:use-module (crates-io))

(define-public crate-fuguex-loader-0.2.4 (c (n "fuguex-loader") (v "0.2.4") (d (list (d (n "either") (r "^1") (d #t) (k 0)) (d (n "fugue") (r "^0.2") (f (quote ("db"))) (d #t) (k 0)) (d (n "fugue-ghidra") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "fugue-idapro") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "fugue-radare") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "fuguex-state") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1filyhf7ak4vnya5xahqnhhs6yywya1f9gj8v5chbr0237cpg2d6") (f (quote (("radare" "fugue-radare") ("idapro" "fugue-idapro") ("ghidra" "fugue-ghidra") ("default") ("all" "idapro" "ghidra" "radare"))))))

