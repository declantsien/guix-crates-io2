(define-module (crates-io fu gu fuguex-state-derive) #:use-module (crates-io))

(define-public crate-fuguex-state-derive-0.2.0 (c (n "fuguex-state-derive") (v "0.2.0") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "11nlvqvgjbjc37966901b822jaxbac4glbz222dgz5q2cgxi33x8")))

