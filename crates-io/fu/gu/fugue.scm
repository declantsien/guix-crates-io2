(define-module (crates-io fu gu fugue) #:use-module (crates-io))

(define-public crate-fugue-0.1.0 (c (n "fugue") (v "0.1.0") (h "1jr4afjbn42qgkyrlirh5jylgs5ll2y6i60amq3v2q09aw4542mg")))

(define-public crate-fugue-0.2.8 (c (n "fugue") (v "0.2.8") (d (list (d (n "fugue-arch") (r "^0.2") (d #t) (k 0)) (d (n "fugue-bv") (r "^0.2") (k 0)) (d (n "fugue-bytes") (r "^0.2") (d #t) (k 0)) (d (n "fugue-db") (r "^0.2") (o #t) (k 0)) (d (n "fugue-fp") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "fugue-ir") (r "^0.2") (k 0)))) (h "1ms3jvggs1zs2sgids7cvk0md4xk6dazqwd16bs8pdggxrxy3zwi") (f (quote (("fp" "bigint" "fugue-fp") ("extra-logging" "fugue-ir/extra-logging") ("extra-integer-types" "fugue-bytes/extra-integer-types") ("default" "bigint") ("db" "fugue-db")))) (s 2) (e (quote (("fixed-u64" "fugue-bv/fixed-u64" "fugue-db?/fixed-u64" "fugue-ir/fixed-u64") ("fixed-u128" "fugue-bv/fixed-u128" "fugue-db?/fixed-u128" "fugue-ir/fixed-u128") ("bigint" "fugue-bv/bigint" "fugue-db?/bigint" "fugue-ir/bigint"))))))

