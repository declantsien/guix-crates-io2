(define-module (crates-io fu gu fugue-arch) #:use-module (crates-io))

(define-public crate-fugue-arch-0.2.4 (c (n "fugue-arch") (v "0.2.4") (d (list (d (n "fugue-bytes") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "unicase") (r "^2.6") (d #t) (k 0)))) (h "13v634j3vg6l0smzch2madrpazpw6c6wjj5c6bpd036gjrabq8d0")))

