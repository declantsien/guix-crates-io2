(define-module (crates-io fu gu fugue-bytes) #:use-module (crates-io))

(define-public crate-fugue-bytes-0.2.2 (c (n "fugue-bytes") (v "0.2.2") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ux") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0px8aycvqi4y2r4xrbd8hgp6kmqacnszpf1d7nbcbwy8lscapwck") (f (quote (("extra-integer-types" "ux") ("default"))))))

