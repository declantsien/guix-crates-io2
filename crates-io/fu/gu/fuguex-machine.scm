(define-module (crates-io fu gu fuguex-machine) #:use-module (crates-io))

(define-public crate-fuguex-machine-0.2.10 (c (n "fuguex-machine") (v "0.2.10") (d (list (d (n "fugue") (r "^0.2") (d #t) (k 0)) (d (n "fuguex-state") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0bw7j716alibfpg7iydlkgli8ji70yr8v7j8jc4mn8dig1shld9i")))

