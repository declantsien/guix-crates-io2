(define-module (crates-io fu nd fund-me-core) #:use-module (crates-io))

(define-public crate-fund-me-core-0.2.0 (c (n "fund-me-core") (v "0.2.0") (d (list (d (n "cosmwasm-bignumber") (r "^2.2.0") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^0.16.0") (f (quote ("iterator"))) (d #t) (k 0)) (d (n "cosmwasm-storage") (r "^0.16.0") (f (quote ("iterator"))) (d #t) (k 0)) (d (n "cw20") (r "^0.8.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)) (d (n "terra-cosmwasm") (r "^2.2.0") (d #t) (k 0)))) (h "03z17x3aga70pvm965kfj0gfzsb7nfi05k936507b2n3gpyq86ll") (f (quote (("backtraces" "cosmwasm-std/backtraces"))))))

