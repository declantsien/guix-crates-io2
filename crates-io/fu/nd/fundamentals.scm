(define-module (crates-io fu nd fundamentals) #:use-module (crates-io))

(define-public crate-fundamentals-0.0.1-alpha.1 (c (n "fundamentals") (v "0.0.1-alpha.1") (d (list (d (n "fundamentals-derive") (r "^0.0.1-alpha.1") (d #t) (k 0)))) (h "0l9nhvhnxxb2a838k90rhqxgx5pqmh0i51i7jbyyk57b1g7iwdhr")))

(define-public crate-fundamentals-0.0.1-alpha.2 (c (n "fundamentals") (v "0.0.1-alpha.2") (d (list (d (n "fundamentals-derive") (r "^0.0.1-alpha.1") (d #t) (k 0)))) (h "0052nzinc79466gr7399khzq73gcz9hlj9lk7hg21hray57nkxhr")))

(define-public crate-fundamentals-0.0.1-alpha.3 (c (n "fundamentals") (v "0.0.1-alpha.3") (d (list (d (n "fundamentals-derive") (r "^0.0.1-alpha.2") (d #t) (k 0)))) (h "17jmh9rr2ysx105cbhj7swaznrw7hr2lcczb8jlamz1q9gwawzp2")))

