(define-module (crates-io fu nd fundamentals-derive) #:use-module (crates-io))

(define-public crate-fundamentals-derive-0.0.1-alpha.1 (c (n "fundamentals-derive") (v "0.0.1-alpha.1") (d (list (d (n "kproc-parser") (r "^0.0.1-beta.5") (f (quote ("proc_macro_wrapper"))) (d #t) (k 0)))) (h "0ri2c87ii15a0x90wz7iljj8ilm2rcnmsl4g3daricdr6l19fhv6")))

(define-public crate-fundamentals-derive-0.0.1-alpha.2 (c (n "fundamentals-derive") (v "0.0.1-alpha.2") (d (list (d (n "kproc-parser") (r "^0.0.1-beta.6") (f (quote ("proc_macro_wrapper"))) (d #t) (k 0)))) (h "07mcgnjf2ksyhqszqar83n9527slczplm1lmjpsp1rnzwvdy7bgl")))

