(define-module (crates-io fu nd fundamental_data_type) #:use-module (crates-io))

(define-public crate-fundamental_data_type-0.1.0 (c (n "fundamental_data_type") (v "0.1.0") (d (list (d (n "test_tools") (r "~0.1") (d #t) (k 2)))) (h "0xnlhpq0iqmq8l046c0dhr8jp3m9hry9whv66dcykzf96dwip17i")))

(define-public crate-fundamental_data_type-0.1.1 (c (n "fundamental_data_type") (v "0.1.1") (d (list (d (n "test_tools") (r "~0.1") (d #t) (k 2)))) (h "1d2fzblfz83rr09kkwjqimsshfvd04dax7mryqy8g0bsg2gk93zy")))

(define-public crate-fundamental_data_type-0.1.2 (c (n "fundamental_data_type") (v "0.1.2") (d (list (d (n "test_tools") (r "~0.1") (d #t) (k 2)))) (h "151p39ziprpxpqks68zih5jd5jqnrr1p3xhisj87hz35ii2k30qk")))

(define-public crate-fundamental_data_type-0.1.3 (c (n "fundamental_data_type") (v "0.1.3") (d (list (d (n "test_tools") (r "~0.1") (d #t) (k 2)) (d (n "type_constructor") (r "~0.1") (d #t) (k 0)))) (h "04lqlh4djdkady6h3h0xhyl45milss58xif649cwhhgn2645jwbn")))

(define-public crate-fundamental_data_type-0.1.4 (c (n "fundamental_data_type") (v "0.1.4") (d (list (d (n "test_tools") (r "~0.1") (d #t) (k 2)) (d (n "type_constructor") (r "~0.1") (d #t) (k 0)))) (h "1hjm44wj8prdrig163fyy1g1k2ap77fksbl7cdfxixynyx8d502j")))

(define-public crate-fundamental_data_type-0.1.5 (c (n "fundamental_data_type") (v "0.1.5") (d (list (d (n "test_tools") (r "~0.1") (d #t) (k 2)) (d (n "type_constructor") (r "~0.1") (d #t) (k 0)))) (h "00aanzprd99yzwircpiscimvmkgc8bsijjcd82v0vnyzsrc3pacl")))

(define-public crate-fundamental_data_type-0.1.6 (c (n "fundamental_data_type") (v "0.1.6") (d (list (d (n "test_tools") (r "~0.1") (d #t) (k 2)) (d (n "type_constructor") (r "~0.1") (d #t) (k 0)))) (h "1mq2xbr7x8fvr0n5mm0p0xhmdwm9awn46dwlv0rrn539w11glgvx") (f (quote (("make" "type_constructor/make") ("default" "type_constructor/default") ("all" "type_constructor/all"))))))

(define-public crate-fundamental_data_type-0.2.0 (c (n "fundamental_data_type") (v "0.2.0") (d (list (d (n "derive_tools") (r "~0.17.0") (f (quote ("enabled"))) (k 0)) (d (n "test_tools") (r "~0.6.0") (d #t) (k 2)))) (h "0gjfpqy7pswj4ylqigqqww151rfs982iwn5mf0qhy72qfrk2rdnn") (f (quote (("use_alloc") ("no_std") ("full" "derive_tools/full") ("enabled") ("default" "derive_tools/default"))))))

