(define-module (crates-io fu nd fundamentals_rust) #:use-module (crates-io))

(define-public crate-fundamentals_rust-0.1.0 (c (n "fundamentals_rust") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.126") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "19dahw7wy83ahv115b6d31kq0yabjiyigafg00b7v336lv6fwwv8") (y #t)))

