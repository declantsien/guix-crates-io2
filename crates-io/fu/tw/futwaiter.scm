(define-module (crates-io fu tw futwaiter) #:use-module (crates-io))

(define-public crate-futwaiter-0.1.0 (c (n "futwaiter") (v "0.1.0") (d (list (d (n "futures") (r "^0.3.30") (d #t) (k 0)) (d (n "futures-timer") (r "^3.0.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2.13") (d #t) (k 0)))) (h "02wlq9i99j4wi3mw0573i8sfhaqah6ag7k2xk4q9a977r9hn3ah5") (f (quote (("global"))))))

