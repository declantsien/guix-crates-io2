(define-module (crates-io fu rz furze) #:use-module (crates-io))

(define-public crate-furze-0.0.1 (c (n "furze") (v "0.0.1") (d (list (d (n "cbindgen") (r "^0.20.0") (d #t) (k 1)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1w5zfjnqb6554plld48b1xzv5csf3vfrmwgf6zjrmm48jx1ci1ys")))

(define-public crate-furze-0.0.2 (c (n "furze") (v "0.0.2") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "varintrs") (r "^0.2.1") (d #t) (k 0)))) (h "10pxiqicybs958c2djhz6zxd10cqy07c8wdbqizxbhcj2kpyqsj8")))

(define-public crate-furze-0.0.3 (c (n "furze") (v "0.0.3") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "varintrs") (r "^0.2.1") (d #t) (k 0)))) (h "0fxm1s9gnx4yib7ndf944an3x4h4rf66pjc3xx648plfhm5vcqcw")))

(define-public crate-furze-0.0.4 (c (n "furze") (v "0.0.4") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "varintrs") (r "^0.2.1") (d #t) (k 0)))) (h "1zi8pkwiwcdjab36s3jsvxb8w3j11n4msbjhjpr8pqyxj8w6fxnj")))

(define-public crate-furze-0.0.5 (c (n "furze") (v "0.0.5") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "varintrs") (r "^0.2.1") (d #t) (k 0)))) (h "02dlsp4x54bg7i8xrvin02n3b0lgkq6a0xh7ay1bbnc53w22vnkf")))

(define-public crate-furze-0.0.6 (c (n "furze") (v "0.0.6") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "varintrs") (r "^0.2.1") (d #t) (k 0)))) (h "0dzvra1a7whffm4mbghnbf34wshqal88pk2qkqw93x13vf4b1h5n")))

