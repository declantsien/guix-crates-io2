(define-module (crates-io fu nv funver) #:use-module (crates-io))

(define-public crate-funver-1.0.0-experimental-glam.8.2 (c (n "funver") (v "1.0.0-experimental-glam.8.2") (h "0id3dqlr48q0ig4yhk5gzdxlsdl9fdfm1044a0s958xc8kfidpvc")))

(define-public crate-funver-1.0.0-experimental-serde.1.0 (c (n "funver") (v "1.0.0-experimental-serde.1.0") (h "1gcfvifnh1qd2y7kgpvrn8jv9p8sajxq8kw23yz5y3n950dzffxc")))

