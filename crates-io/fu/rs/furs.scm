(define-module (crates-io fu rs furs) #:use-module (crates-io))

(define-public crate-furs-0.1.0 (c (n "furs") (v "0.1.0") (d (list (d (n "rpds") (r "^0.6.0") (d #t) (k 0)))) (h "0iqpl1yxln14pkskx5khpbajqhxblqvbi7anjhn6n6ykmfc77llg") (y #t)))

(define-public crate-furs-0.2.0 (c (n "furs") (v "0.2.0") (d (list (d (n "rpds") (r "^0.6.0") (d #t) (k 0)))) (h "1lrq2z2fb7hmhy9hni4nz04p1fvil3s5ybvhvi8wnvrhf9w43bzl") (y #t)))

