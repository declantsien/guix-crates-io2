(define-module (crates-io fu rd furdb_core) #:use-module (crates-io))

(define-public crate-furdb_core-0.1.1 (c (n "furdb_core") (v "0.1.1") (d (list (d (n "bitvec") (r "^1.0.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.133") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "00j47pskb435zqk64wircv3gfb0bhil449cgv5qw1837snqcyryv") (y #t)))

(define-public crate-furdb_core-0.1.2 (c (n "furdb_core") (v "0.1.2") (d (list (d (n "bitvec") (r "^1.0.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.133") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "09sfdwzhik5p8kq2v9adhgc58d0jn91hqqzdydv7nylv6br04kd1") (y #t)))

