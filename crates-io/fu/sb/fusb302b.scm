(define-module (crates-io fu sb fusb302b) #:use-module (crates-io))

(define-public crate-fusb302b-0.0.1 (c (n "fusb302b") (v "0.0.1") (d (list (d (n "byteorder") (r "^1.4.3") (k 0)) (d (n "defmt") (r "^0.3.5") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)) (d (n "fugit") (r "^0.3.7") (d #t) (k 0)) (d (n "heapless") (r "^0.7.16") (f (quote ("defmt"))) (d #t) (k 0)) (d (n "paste") (r "^1.0.13") (d #t) (k 0)) (d (n "proc-bitfield") (r "^0.2.4") (d #t) (k 0)) (d (n "usb-pd") (r "^0.0.1") (d #t) (k 0)))) (h "0y4sdrgdpc3qvc7xbpwpfg87r46jgqv7xg3rzbxh4mzrbarzzf7d")))

