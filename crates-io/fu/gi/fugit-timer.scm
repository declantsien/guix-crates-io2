(define-module (crates-io fu gi fugit-timer) #:use-module (crates-io))

(define-public crate-fugit-timer-0.1.0 (c (n "fugit-timer") (v "0.1.0") (d (list (d (n "fugit") (r "^0.3.3") (d #t) (k 0)) (d (n "nb") (r "^1.0.0") (d #t) (k 0)))) (h "1aayfn9p21bdlf02krmrr4a45vvhc13c542qbns58v95065z08vd") (y #t)))

(define-public crate-fugit-timer-0.1.1 (c (n "fugit-timer") (v "0.1.1") (d (list (d (n "fugit") (r "^0.3.3") (d #t) (k 0)) (d (n "nb") (r "^1.0.0") (d #t) (k 0)))) (h "17z8k32cq8dmv4vddjc4ibcw5cszyy2yd5d0p60p7ac9fdgpnvpp") (y #t)))

(define-public crate-fugit-timer-0.1.2 (c (n "fugit-timer") (v "0.1.2") (d (list (d (n "fugit") (r "^0.3.3") (d #t) (k 0)) (d (n "nb") (r "^1.0.0") (d #t) (k 0)))) (h "08agqskv6i7ywk3f3b2x015ld3zljs9kcx6q4n947rcdvr6wb6h9")))

(define-public crate-fugit-timer-0.1.3 (c (n "fugit-timer") (v "0.1.3") (d (list (d (n "fugit") (r "^0.3.3") (d #t) (k 0)) (d (n "nb") (r "^1.0.0") (d #t) (k 0)))) (h "0jp0vizdr1vwzkbbrcqpsk54cx809bg6xx84jxi9v3rq9ky7nq6r")))

