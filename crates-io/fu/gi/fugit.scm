(define-module (crates-io fu gi fugit) #:use-module (crates-io))

(define-public crate-fugit-0.1.0 (c (n "fugit") (v "0.1.0") (d (list (d (n "defmt") (r "^0.2.3") (o #t) (d #t) (k 0)))) (h "0jh6794khads1nm4ir0kkwgkvsqrw48qlwi05hzv59lzqpmpm6xi")))

(define-public crate-fugit-0.1.1 (c (n "fugit") (v "0.1.1") (d (list (d (n "defmt") (r "^0.2.3") (o #t) (d #t) (k 0)))) (h "1qap13wncis9g3f3asd9n8p80jrk8m40rnma3zmgghnlsw9ny409")))

(define-public crate-fugit-0.1.2 (c (n "fugit") (v "0.1.2") (d (list (d (n "defmt") (r "^0.2.3") (o #t) (d #t) (k 0)))) (h "10019z4p0812yb6awjy1nfm4b3k01h77cfcplmnanrgriyzwcsgv")))

(define-public crate-fugit-0.1.3 (c (n "fugit") (v "0.1.3") (d (list (d (n "defmt") (r "^0.2.3") (o #t) (d #t) (k 0)))) (h "0d3pqmfk07p405rxflrrlhp5r6pk58b5b9a7r29i2axdy9py2f9g")))

(define-public crate-fugit-0.1.4 (c (n "fugit") (v "0.1.4") (d (list (d (n "defmt") (r "^0.2.3") (o #t) (d #t) (k 0)))) (h "1cxg49may5biqc2mk9a63bnic7gyrkq1360izkz13hxaw5hqpf07")))

(define-public crate-fugit-0.2.0 (c (n "fugit") (v "0.2.0") (d (list (d (n "defmt") (r "^0.2.3") (o #t) (d #t) (k 0)))) (h "1cdkxaql00xsigk8fkmbb3sxsjgh1s5qsg5lgms5sw8dg5y1b65c") (y #t)))

(define-public crate-fugit-0.2.1 (c (n "fugit") (v "0.2.1") (d (list (d (n "defmt") (r "^0.2.3") (o #t) (d #t) (k 0)))) (h "0syzxsab5lzvrpzf2wyycj38762sn84pm4p7a67v2r4p894dxxgx")))

(define-public crate-fugit-0.3.0 (c (n "fugit") (v "0.3.0") (d (list (d (n "defmt") (r "^0.2.3") (o #t) (d #t) (k 0)))) (h "1gcxxdfpa8x6bcq4a1021fn4pwfwkpv14nzz6by74fj5il9hhiaf") (y #t)))

(define-public crate-fugit-0.3.1 (c (n "fugit") (v "0.3.1") (d (list (d (n "defmt") (r ">=0.2.0, <0.4") (o #t) (d #t) (k 0)))) (h "17qsym9hf5z84kbdj7s4qn55fq048zm6a4hbrqgli5yxmbrfn051") (y #t)))

(define-public crate-fugit-0.3.2 (c (n "fugit") (v "0.3.2") (d (list (d (n "defmt") (r ">=0.2.0, <0.4") (o #t) (d #t) (k 0)))) (h "0d1pwfwm1asbvn03jfk6xgj387a1qsd3k82sxkrsbdlpxav4c8sh")))

(define-public crate-fugit-0.3.3 (c (n "fugit") (v "0.3.3") (d (list (d (n "defmt") (r ">=0.2.0, <0.4") (o #t) (d #t) (k 0)) (d (n "gcd") (r ">=2.1, <3.0") (d #t) (k 0)))) (h "061wpy9zbgh98w757a12wm6hip5vb4j0g3mic6v0v98iakwqkr12")))

(define-public crate-fugit-0.3.4 (c (n "fugit") (v "0.3.4") (d (list (d (n "defmt") (r ">=0.2.0, <0.4") (o #t) (d #t) (k 0)) (d (n "gcd") (r ">=2.1, <3.0") (d #t) (k 0)))) (h "1xs7vzgs20lwbfpbg45rmzch59hlalykrh5rl3l3diad8d9wcf67") (y #t)))

(define-public crate-fugit-0.3.5 (c (n "fugit") (v "0.3.5") (d (list (d (n "defmt") (r ">=0.2.0, <0.4") (o #t) (d #t) (k 0)) (d (n "gcd") (r ">=2.1, <3.0") (d #t) (k 0)))) (h "0n6bjwlxhb8zwkyvasyc1yp9ydagsarkc043x7q55jnmhdbmkn7n")))

(define-public crate-fugit-0.3.6 (c (n "fugit") (v "0.3.6") (d (list (d (n "defmt") (r ">=0.2.0, <0.4") (o #t) (d #t) (k 0)) (d (n "gcd") (r ">=2.1, <3.0") (d #t) (k 0)))) (h "1rc7g8v9a6i8g5457av0jbwyff4r4i9c1dlc0l6p5xnyg6r7pcbs")))

(define-public crate-fugit-0.3.7 (c (n "fugit") (v "0.3.7") (d (list (d (n "defmt") (r ">=0.2.0, <0.4") (o #t) (d #t) (k 0)) (d (n "gcd") (r ">=2.1, <3.0") (d #t) (k 0)))) (h "1rzp49521akq49vs9m8llgmdkk08zb77rry10a7srm9797b6l60p")))

