(define-module (crates-io fu n_ fun_run) #:use-module (crates-io))

(define-public crate-fun_run-0.1.0 (c (n "fun_run") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "which_problem") (r "^0.1") (o #t) (d #t) (k 0)))) (h "06ndy132li1nzwqcrqq3pinf2zwxncfy12xrz51f48jiirkq1k39") (s 2) (e (quote (("which_problem" "dep:which_problem"))))))

(define-public crate-fun_run-0.1.1 (c (n "fun_run") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "which_problem") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1myf29998x3v3pzq2x31xpj2ww4q1xfgv5jgvzncaa563746h77i") (s 2) (e (quote (("which_problem" "dep:which_problem"))))))

(define-public crate-fun_run-0.1.2 (c (n "fun_run") (v "0.1.2") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "which_problem") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0qj9klahffv4lkwh4vjwsr1ywn54jcpdzgcpmfsn13wcvg20j70g") (s 2) (e (quote (("which_problem" "dep:which_problem"))))))

