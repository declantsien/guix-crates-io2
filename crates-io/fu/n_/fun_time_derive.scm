(define-module (crates-io fu n_ fun_time_derive) #:use-module (crates-io))

(define-public crate-fun_time_derive-0.1.0 (c (n "fun_time_derive") (v "0.1.0") (d (list (d (n "darling") (r "^0.14.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0bjl7kmfd0ax6c9pxqj42c1kbyjm5gydw51rfrccx9m3xvszdfxm") (f (quote (("log") ("default"))))))

(define-public crate-fun_time_derive-0.2.0 (c (n "fun_time_derive") (v "0.2.0") (d (list (d (n "darling") (r "^0.14.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "19cgqwz2k6787j6z2yhc2nh4lnsj475q5xj9lzhyz33nvqlzwnia") (f (quote (("log") ("default"))))))

(define-public crate-fun_time_derive-0.2.1 (c (n "fun_time_derive") (v "0.2.1") (d (list (d (n "darling") (r "^0.14.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0vdjxpylrka25qfyphrlh9n8cfpr79vxaymdchmj42y7fa5g403m") (f (quote (("log") ("default"))))))

(define-public crate-fun_time_derive-0.3.0 (c (n "fun_time_derive") (v "0.3.0") (d (list (d (n "darling") (r "^0.14.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1jpa8xx8b0cva43c23a0smlzljsf5yfvz10qmdc7i9r4fnb3x6md") (f (quote (("log") ("default"))))))

(define-public crate-fun_time_derive-0.3.1 (c (n "fun_time_derive") (v "0.3.1") (d (list (d (n "darling") (r "^0.14.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "13h7bh4wc7jlia6ds6vh3k2c2c4mg6ynkk749qb1csipcxblb4p9") (f (quote (("log") ("default"))))))

(define-public crate-fun_time_derive-0.3.2 (c (n "fun_time_derive") (v "0.3.2") (d (list (d (n "darling") (r "^0.14.2") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (o #t) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "015dhh4a3rmag434n5fxydvmd7fzs44l77gs30iwp6pm3zk6m11x") (f (quote (("default")))) (s 2) (e (quote (("log" "dep:log"))))))

(define-public crate-fun_time_derive-0.3.3 (c (n "fun_time_derive") (v "0.3.3") (d (list (d (n "darling") (r "^0.14.2") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (o #t) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0dm7yf2n7cw3mm0yg59dy6gyrwf67csjdpgr1v89rkk08fbrpn53") (f (quote (("default")))) (s 2) (e (quote (("log" "dep:log"))))))

(define-public crate-fun_time_derive-0.3.4 (c (n "fun_time_derive") (v "0.3.4") (d (list (d (n "darling") (r "^0.14.2") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (o #t) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "04zh2a98vsbrbxvqscic2qnfs2p858mad3wxsa18v4q0vg95ymbi") (f (quote (("default")))) (s 2) (e (quote (("log" "dep:log"))))))

