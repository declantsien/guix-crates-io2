(define-module (crates-io fu n_ fun_time) #:use-module (crates-io))

(define-public crate-fun_time-0.1.0 (c (n "fun_time") (v "0.1.0") (d (list (d (n "fun_time_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (o #t) (d #t) (k 0)) (d (n "simple_logger") (r "^4.0.0") (d #t) (k 2)))) (h "0rjyz0bl739s0r56yl1iir5jding9p5vd7fiam74m4613d3kzw7s") (f (quote (("default")))) (s 2) (e (quote (("log" "dep:log" "fun_time_derive/log"))))))

(define-public crate-fun_time-0.2.0 (c (n "fun_time") (v "0.2.0") (d (list (d (n "fun_time_derive") (r "^0.2.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (o #t) (d #t) (k 0)) (d (n "simple_logger") (r "^4.0.0") (d #t) (k 2)))) (h "1qx9ysklsxkbmghs9yzdgniz51mrw406jbyv24h2xhx1pv9d6s52") (f (quote (("default")))) (s 2) (e (quote (("log" "dep:log" "fun_time_derive/log"))))))

(define-public crate-fun_time-0.2.1 (c (n "fun_time") (v "0.2.1") (d (list (d (n "fun_time_derive") (r "^0.2.1") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (o #t) (d #t) (k 0)) (d (n "simple_logger") (r "^4.0.0") (d #t) (k 2)))) (h "040g8f6b7d84fh0g6c3nw34qhm1zmzgni8zdwk2aci68v9xpz7vv") (f (quote (("default")))) (s 2) (e (quote (("log" "dep:log" "fun_time_derive/log"))))))

(define-public crate-fun_time-0.3.0 (c (n "fun_time") (v "0.3.0") (d (list (d (n "fun_time_derive") (r "^0.3.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (o #t) (d #t) (k 0)) (d (n "simple_logger") (r "^4.0.0") (d #t) (k 2)))) (h "1arn53i2ckr0zx2097d25nkkmic2zhd3vzkf6af8vrps2zzm1cd8") (f (quote (("default")))) (s 2) (e (quote (("log" "dep:log" "fun_time_derive/log"))))))

(define-public crate-fun_time-0.3.1 (c (n "fun_time") (v "0.3.1") (d (list (d (n "fun_time_derive") (r "^0.3.1") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (o #t) (d #t) (k 0)) (d (n "simple_logger") (r "^4.0.0") (d #t) (k 2)))) (h "1bkylkss0avm48903gaim5bmpbqw4cw14pjn8lf85r3agcs9zwls") (f (quote (("default")))) (s 2) (e (quote (("log" "dep:log" "fun_time_derive/log"))))))

(define-public crate-fun_time-0.3.2 (c (n "fun_time") (v "0.3.2") (d (list (d (n "fun_time_derive") (r "^0.3.2") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (o #t) (d #t) (k 0)) (d (n "simple_logger") (r "^4.2.0") (d #t) (k 2)))) (h "0d6kkn3p7ckvz28yqwmfgl3akwfpcrcr9fm4sb4mf5lp0bxcyxch") (f (quote (("default")))) (s 2) (e (quote (("log" "dep:log" "fun_time_derive/log"))))))

(define-public crate-fun_time-0.3.3 (c (n "fun_time") (v "0.3.3") (d (list (d (n "fun_time_derive") (r "^0.3.3") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (o #t) (d #t) (k 0)) (d (n "simple_logger") (r "^4.2.0") (d #t) (k 2)))) (h "19g61ca7zg3bppkxz90ip6zv7lxjgcqbmc36imlmandb0a2dsgvq") (f (quote (("default")))) (s 2) (e (quote (("log" "dep:log" "fun_time_derive/log"))))))

(define-public crate-fun_time-0.3.4 (c (n "fun_time") (v "0.3.4") (d (list (d (n "fun_time_derive") (r "^0.3.4") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (o #t) (d #t) (k 0)) (d (n "simple_logger") (r "^4.2.0") (d #t) (k 2)))) (h "1c7xcxhxl7g22ahbh312walb3yh1j1gsyhmglz687sh56va99qdy") (f (quote (("default")))) (s 2) (e (quote (("log" "dep:log" "fun_time_derive/log"))))))

