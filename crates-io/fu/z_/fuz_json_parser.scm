(define-module (crates-io fu z_ fuz_json_parser) #:use-module (crates-io))

(define-public crate-fuz_json_parser-0.1.0 (c (n "fuz_json_parser") (v "0.1.0") (d (list (d (n "args") (r "^2.2.0") (d #t) (k 2)) (d (n "getopts") (r "^0.2.21") (d #t) (k 2)) (d (n "json") (r "^0.12.4") (d #t) (k 2)))) (h "0aby93lvmm58shx8nb2fffivz5nqbk4xdjnjl7s74225prf32di3")))

(define-public crate-fuz_json_parser-0.1.1 (c (n "fuz_json_parser") (v "0.1.1") (d (list (d (n "args") (r "^2.2.0") (d #t) (k 2)) (d (n "getopts") (r "^0.2.21") (d #t) (k 2)) (d (n "json") (r "^0.12.4") (d #t) (k 2)))) (h "0yfbxz65f6dyk2lcz06ly3pk4cvnvr28dx9inrsdyfgzf820hix6")))

(define-public crate-fuz_json_parser-0.1.2 (c (n "fuz_json_parser") (v "0.1.2") (h "1irmfm1qg3pfcay70zlfnw4qd92a07njs3ldd30l0dvwxkgqqkxs")))

