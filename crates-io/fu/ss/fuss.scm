(define-module (crates-io fu ss fuss) #:use-module (crates-io))

(define-public crate-fuss-0.2.0 (c (n "fuss") (v "0.2.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1sx4akgia94lvsigp3k9a1g5f7a10d1v7nfd8qaaknaqfcczw01z")))

(define-public crate-fuss-0.2.1 (c (n "fuss") (v "0.2.1") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0mw2lk1k0g9hpxv5767s4ymj5zh286wpn80zi9g9zn7fjb0lkq9v")))

(define-public crate-fuss-0.2.2 (c (n "fuss") (v "0.2.2") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0pjcnclp5hczmi4gily6vqid0cz839fdn1m1k6c2k0kbqwp1kzl1")))

