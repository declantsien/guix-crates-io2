(define-module (crates-io fu ll fullerene) #:use-module (crates-io))

(define-public crate-Fullerene-0.1.0 (c (n "Fullerene") (v "0.1.0") (h "1w16w6f4iabv82jdxggpc72zbj0019adrnggkjsdsdvbiqijbxy7") (y #t)))

(define-public crate-Fullerene-0.1.1 (c (n "Fullerene") (v "0.1.1") (d (list (d (n "num") (r "^0.4") (d #t) (k 0)))) (h "1p3s63gvmcq0kjiqmai9m1j5q1fbr3g7d8mnlk3z0hmc1mrsn3cr") (y #t)))

(define-public crate-Fullerene-0.1.2 (c (n "Fullerene") (v "0.1.2") (d (list (d (n "num") (r "^0.4") (d #t) (k 0)))) (h "0jfvi6gsiv0pivnl2ka94mqb465jlwgwhvvbvz77wfhz3k2shav4") (y #t)))

(define-public crate-Fullerene-0.1.3 (c (n "Fullerene") (v "0.1.3") (d (list (d (n "num") (r "^0.4") (d #t) (k 0)))) (h "1iw4ca5bz868sksqv3qdqr4nw4m787l0xx5pn4x64z9vndw8iiq5") (y #t)))

(define-public crate-Fullerene-0.1.4 (c (n "Fullerene") (v "0.1.4") (d (list (d (n "num") (r "^0.4") (d #t) (k 0)))) (h "0f8wknvmkg6d8v98yz3lf4dghhk23xx57xdhzm8ch7savsahgpvl") (y #t)))

(define-public crate-Fullerene-0.1.5 (c (n "Fullerene") (v "0.1.5") (d (list (d (n "num") (r "^0.4") (d #t) (k 0)))) (h "0ps7b848l59dlv6ncl0my8x42avd94rp3sbfimd7273gnh6dy8h2") (y #t)))

(define-public crate-Fullerene-0.1.6 (c (n "Fullerene") (v "0.1.6") (d (list (d (n "num") (r "^0.4") (d #t) (k 0)))) (h "04sdc6jlmnpf9jp6a60rli0czgcp92d6n9mfgpy2ar1k1b17apn7") (y #t)))

(define-public crate-Fullerene-0.2.1 (c (n "Fullerene") (v "0.2.1") (d (list (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "polyhedron-faces") (r "^0.0") (d #t) (k 0)))) (h "1lf8swqgwpah8jpvinyb5bngv4j5rjxjw6ch7izidj5fjx0139h2") (y #t)))

(define-public crate-Fullerene-0.2.2 (c (n "Fullerene") (v "0.2.2") (d (list (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "polyhedron-faces") (r "^0.1") (d #t) (k 0)))) (h "0jyqr4d8pxa3294fsisysxrv9f502pmdifhj9cv5kkwgaw5civfd") (y #t)))

(define-public crate-Fullerene-0.2.3 (c (n "Fullerene") (v "0.2.3") (d (list (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "polyhedron-faces") (r "^0.2") (d #t) (k 0)))) (h "16zqbaya7dm3zbpl4j43lxhccdjlm59qybqb9svl38cw0by4lcx8") (y #t)))

(define-public crate-Fullerene-0.3.0 (c (n "Fullerene") (v "0.3.0") (d (list (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "polyhedron-faces") (r "^0.3") (d #t) (k 0)))) (h "1zq148laidh8c9bx2frvzix91jii8dh9iaqh93lh0xbcsd86syx5") (y #t)))

(define-public crate-Fullerene-0.3.1 (c (n "Fullerene") (v "0.3.1") (d (list (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "polyhedron-faces") (r "^0.3") (d #t) (k 0)))) (h "1p5hhqpg22phi88mhp0lf44cym89yx24l38jl02g7zqkz45w71i1") (y #t)))

(define-public crate-Fullerene-0.4.1 (c (n "Fullerene") (v "0.4.1") (d (list (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "polyhedron-faces") (r "^0.4") (d #t) (k 0)))) (h "16p7ihg0mb29d26hlg37ddbcvmiwchx7mch3pzlj8i66fwym1f8f")))

