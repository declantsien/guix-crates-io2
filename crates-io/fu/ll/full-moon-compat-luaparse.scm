(define-module (crates-io fu ll full-moon-compat-luaparse) #:use-module (crates-io))

(define-public crate-full-moon-compat-luaparse-0.1.0 (c (n "full-moon-compat-luaparse") (v "0.1.0") (d (list (d (n "full_moon") (r "^0.6.1") (k 0)) (d (n "miniserde") (r "^0.1") (d #t) (k 0)))) (h "19bib0bqvx9l5sfn2k5fvri7incpwscqvsn9amcd236gwm7hrcnr")))

