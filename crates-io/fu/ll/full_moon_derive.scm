(define-module (crates-io fu ll full_moon_derive) #:use-module (crates-io))

(define-public crate-full_moon_derive-0.1.0 (c (n "full_moon_derive") (v "0.1.0") (d (list (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0kpcz77l7kzzc01w4ndgkzpxm01lvxa071pbwahyw81zzlajngfz")))

(define-public crate-full_moon_derive-0.1.1 (c (n "full_moon_derive") (v "0.1.1") (d (list (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0xwfvadfs62j7vy3acf4b3qz93cvhcs146371d1l5vsa3ixqba0l")))

(define-public crate-full_moon_derive-0.2.0 (c (n "full_moon_derive") (v "0.2.0") (d (list (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1xg4q5f0cysy1vwkhmikzsjdxck67px5w8rj7vdi62qkffmbvw8w")))

(define-public crate-full_moon_derive-0.3.0 (c (n "full_moon_derive") (v "0.3.0") (d (list (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0vi7hqp31i8v4akyps5v4zn7gzgsyc43ghxx6919dgam93n6ryqa")))

(define-public crate-full_moon_derive-0.3.1 (c (n "full_moon_derive") (v "0.3.1") (d (list (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0x34ngixmf3l7s7xqaxrkw264ds9fgdmyqjnxq1xn4a9jjzvkvwp")))

(define-public crate-full_moon_derive-0.4.0 (c (n "full_moon_derive") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0qw1na75ydk4x1n5calzi8vsgyi4hqzgaz1vrh2npkaq8ab319ss")))

(define-public crate-full_moon_derive-0.4.0-rc.13 (c (n "full_moon_derive") (v "0.4.0-rc.13") (d (list (d (n "indexmap") (r "^1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1dprrc08b0mjgrj2wi00v4chg2xq91r40fsb0ca3k64zw3dx56g8")))

(define-public crate-full_moon_derive-0.5.0 (c (n "full_moon_derive") (v "0.5.0") (d (list (d (n "indexmap") (r "^1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.16") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "12wggbnm67cwang3d674avi81v7vp9g6flg1lip823d1c5kw00dq")))

(define-public crate-full_moon_derive-0.6.0 (c (n "full_moon_derive") (v "0.6.0") (d (list (d (n "indexmap") (r "^1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.16") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1gvv86k88m9kq84pwc1a1slhh34zj913d5s9mql2g839rlpdapp5")))

(define-public crate-full_moon_derive-0.7.0 (c (n "full_moon_derive") (v "0.7.0") (d (list (d (n "indexmap") (r "^1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.16") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0jb4i5nqwwyxksiqlll4l8y3xk0n3l88cns7jr92cajs0bq5ijpw")))

(define-public crate-full_moon_derive-0.8.0 (c (n "full_moon_derive") (v "0.8.0") (d (list (d (n "indexmap") (r "^1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.16") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0p3jyjcflr5kr55adf4q4y8xrzxby4wslfwnr9qi8gc306wmrads")))

(define-public crate-full_moon_derive-0.9.0 (c (n "full_moon_derive") (v "0.9.0") (d (list (d (n "indexmap") (r "^1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.16") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "16jl26c73dq2vq87w55nb35fxlckwhy7kbky661wcs2c748zl9sr")))

(define-public crate-full_moon_derive-0.10.0 (c (n "full_moon_derive") (v "0.10.0") (d (list (d (n "indexmap") (r "^1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.16") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0nw5bqdryriggghjyfjmrnmnz9ah2krsjzp5hyi3lrj9lr3nm0l7")))

(define-public crate-full_moon_derive-0.11.0 (c (n "full_moon_derive") (v "0.11.0") (d (list (d (n "indexmap") (r "^1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.16") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1xy6lfi52lva7c2giy2zm0ybki58ir9233a7qlfpv4jnrq9bvd4r")))

