(define-module (crates-io fu ll fuller_canvas_auth) #:use-module (crates-io))

(define-public crate-fuller_canvas_auth-0.1.0 (c (n "fuller_canvas_auth") (v "0.1.0") (d (list (d (n "fuller_config") (r "^0.1.0") (d #t) (k 0)) (d (n "keyring") (r "^2.3.2") (d #t) (k 0)) (d (n "oauth2") (r "^4.4.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("serde_derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "195s1ixrjzimlpppwb67r21p5liyf0xhbz8aannz6q195g2w44dg")))

(define-public crate-fuller_canvas_auth-0.1.1 (c (n "fuller_canvas_auth") (v "0.1.1") (d (list (d (n "fuller_config") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("serde_derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "17x39hqvwxdhv6k4vxjj7zhhqm87ixzkdbldf45q4g0yw703gj8d")))

(define-public crate-fuller_canvas_auth-0.1.2 (c (n "fuller_canvas_auth") (v "0.1.2") (d (list (d (n "fuller_config") (r "^0.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("serde_derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "1b5wnmrkr7rs4f4x1y8wz0012dglj8qy8yhjp52z7a3xjxv9j6i3")))

(define-public crate-fuller_canvas_auth-0.1.3 (c (n "fuller_canvas_auth") (v "0.1.3") (d (list (d (n "fuller_config") (r "^0.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("serde_derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "0s8027m5b7hpkjlv541m4d4i8m48bhwid1adavr9p7hbkfsclqvj")))

(define-public crate-fuller_canvas_auth-0.1.4 (c (n "fuller_canvas_auth") (v "0.1.4") (d (list (d (n "fuller_config") (r "^0.1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("serde_derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "0f1q2v8ax7q8fwbysslfvj4w8hf308an3vi6qfnv0p8q95shpia5")))

(define-public crate-fuller_canvas_auth-0.1.5 (c (n "fuller_canvas_auth") (v "0.1.5") (d (list (d (n "fuller_config") (r "^0.1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("serde_derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "12j6p1bjd1gyq0mqk0f7i2mmv649rmigxn062n1bk9scd1j7g45j")))

(define-public crate-fuller_canvas_auth-0.1.6 (c (n "fuller_canvas_auth") (v "0.1.6") (d (list (d (n "fuller_config") (r "^0.1.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("serde_derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "0jvnix1ahw69w76k8pzv86mvgpkykzsyr8j8qpig1xz2aq4xs67v")))

