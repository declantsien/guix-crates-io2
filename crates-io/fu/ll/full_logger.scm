(define-module (crates-io fu ll full_logger) #:use-module (crates-io))

(define-public crate-full_logger-0.1.0 (c (n "full_logger") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "pretty_ini") (r "^0.1.6") (d #t) (k 0)) (d (n "toml_edit") (r "^0.19.3") (d #t) (k 0)))) (h "09rrcb9hl0h3qljrhhhj7iwq7l1sdhm2cnc8i44xxw6wj6wwlc5l")))

(define-public crate-full_logger-0.1.1 (c (n "full_logger") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "pretty_ini") (r "^0.1.6") (d #t) (k 0)) (d (n "toml_edit") (r "^0.19.3") (d #t) (k 0)))) (h "1nzr75z35pp8ky8fdl7yxz837wng5cfqam6rqnxaqzrwpx2rbh9i")))

(define-public crate-full_logger-0.1.2 (c (n "full_logger") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "pretty_ini") (r "^0.1.6") (d #t) (k 0)) (d (n "toml_edit") (r "^0.19.3") (d #t) (k 0)))) (h "1rsqyfj0mlilbc7jqzpjasybiqm0irv2vz0drby87n3w4y9nrcip")))

(define-public crate-full_logger-0.1.3 (c (n "full_logger") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "fltk") (r "^1.4.8") (d #t) (k 0)) (d (n "pretty_ini") (r "^0.1.6") (d #t) (k 0)) (d (n "toml_edit") (r "^0.19.3") (d #t) (k 0)))) (h "0mx698al0kg7wngh6m519zg6qrip0ynw367bw3f4r2i9pvlqcszj")))

