(define-module (crates-io fu ll full) #:use-module (crates-io))

(define-public crate-full-0.1.0 (c (n "full") (v "0.1.0") (d (list (d (n "num-complex") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "opimps") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4") (d #t) (k 0)))) (h "17j2l9yh606kamsnkpp42npwyh5mj4a9i3grxi4khxc2ajdhddml")))

