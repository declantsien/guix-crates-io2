(define-module (crates-io fu ll full-duplex-async-copy) #:use-module (crates-io))

(define-public crate-full-duplex-async-copy-0.1.0 (c (n "full-duplex-async-copy") (v "0.1.0") (d (list (d (n "fastrand") (r "^1.4.0") (d #t) (k 0)) (d (n "futures-lite") (r "^1.11.3") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2.4") (d #t) (k 0)))) (h "17nrb2yd9gkknbd6h342z7b10r0zq9q67qw1d72rqdf8271gqsgv")))

