(define-module (crates-io fu ll fullcodec-jubjub) #:use-module (crates-io))

(define-public crate-fullcodec-jubjub-0.2.0 (c (n "fullcodec-jubjub") (v "0.2.0") (d (list (d (n "blake2") (r "^0.9") (d #t) (k 2)) (d (n "bls12_381") (r "^0.2") (k 0) (p "fullcodec-bls12_381")) (d (n "canonical") (r "^0.6") (o #t) (k 0)) (d (n "canonical_derive") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "dusk-bytes") (r "^0.1") (k 0)) (d (n "rand_core") (r "^0.6") (k 0)) (d (n "rand_xorshift") (r "^0.2") (k 2) (p "fullcodec_rand_xorshift")) (d (n "subtle") (r "^0.2.0") (k 0) (p "fullcodec-subtle")))) (h "090lhh0bc6l5x8d8gbjdj9zkwgx4mls6w31y73qvys5bpc67i68v")))

