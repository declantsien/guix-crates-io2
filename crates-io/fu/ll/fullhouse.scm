(define-module (crates-io fu ll fullhouse) #:use-module (crates-io))

(define-public crate-fullhouse-0.1.0 (c (n "fullhouse") (v "0.1.0") (h "14xxdrpl3wz1simqppylrqfwfh9l43dfz1nbvz2y0wg98yf5hhp7") (r "1.60")))

(define-public crate-fullhouse-0.2.0 (c (n "fullhouse") (v "0.2.0") (h "0v6q4qnh2why0vyjsgk15bgm49bahqq0vwy2qi5i88yd6z4851bl") (r "1.60")))

