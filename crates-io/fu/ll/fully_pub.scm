(define-module (crates-io fu ll fully_pub) #:use-module (crates-io))

(define-public crate-fully_pub-0.1.0 (c (n "fully_pub") (v "0.1.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "16bpww0yy3pxaqb1zv06clw6rk71wfhvkyyccf4anr8caf322hff")))

(define-public crate-fully_pub-0.1.1 (c (n "fully_pub") (v "0.1.1") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "10i6qvsflf940i2wcqvc0vy9pr2p8zfj3xqm9jga8inanrlzk6pg")))

(define-public crate-fully_pub-0.1.2 (c (n "fully_pub") (v "0.1.2") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1qvsvlrq0srm7fs92mk40392i4s4rrd3x0li3ifvfxih24b59qa8")))

(define-public crate-fully_pub-0.1.3 (c (n "fully_pub") (v "0.1.3") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0hymbpj1jgbm91fd7rjnmkyadb61yakp93p9izpljjrhnw0m566s")))

(define-public crate-fully_pub-0.1.4 (c (n "fully_pub") (v "0.1.4") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1pslsz7sjasifjr9l6vra9jjiqfb4gif9a7n393qnkpbxi4cpn3z")))

