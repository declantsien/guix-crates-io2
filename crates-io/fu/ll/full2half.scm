(define-module (crates-io fu ll full2half) #:use-module (crates-io))

(define-public crate-full2half-0.1.0 (c (n "full2half") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.16") (f (quote ("derive"))) (d #t) (k 0)))) (h "08dgp4hikpjfpi5jjc9mm38ilaibsrx9kqj893fg9sswixbccmkn")))

(define-public crate-full2half-0.1.1 (c (n "full2half") (v "0.1.1") (d (list (d (n "clap") (r "^4.4.16") (f (quote ("derive"))) (d #t) (k 0)))) (h "1jzks573fajdyyhhiidpll9rgg3s6hxyz6cy9nv6mcwhwll1gkqx")))

