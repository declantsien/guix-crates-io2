(define-module (crates-io fu ll fullcodec-subtle) #:use-module (crates-io))

(define-public crate-fullcodec-subtle-0.2.0 (c (n "fullcodec-subtle") (v "0.2.0") (d (list (d (n "parity-scale-codec") (r "^2.0") (f (quote ("derive"))) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (k 0)))) (h "0n7ng8n514pv2n7ry19pw5jqq4wyp362m5v4lvky7l52x7r3ky5l") (f (quote (("nightly") ("i128") ("default" "i128"))))))

