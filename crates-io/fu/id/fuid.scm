(define-module (crates-io fu id fuid) #:use-module (crates-io))

(define-public crate-fuid-0.1.0 (c (n "fuid") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "uuid") (r "^0.8.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "1a8mxnpyhq317mk6cd6aglsfginm5d91yjccxph4dsnq7n829p6j")))

(define-public crate-fuid-1.0.0 (c (n "fuid") (v "1.0.0") (d (list (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "uuid") (r "^1") (f (quote ("v4"))) (d #t) (k 0)))) (h "0a5wh8k8zx7rr49sb2j5srxcakbrzvlzvzbk2dqfp9nic039h9k5")))

(define-public crate-fuid-1.1.0 (c (n "fuid") (v "1.1.0") (d (list (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "uuid") (r "^1") (f (quote ("v4"))) (d #t) (k 0)))) (h "1zvj49an15cg4iq3ipfs21k4f8gimqizf9sayk5yhv6jkx1114f1")))

(define-public crate-fuid-1.2.1 (c (n "fuid") (v "1.2.1") (d (list (d (n "serde") (r "^1") (o #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "uuid") (r "^1") (f (quote ("v4"))) (k 0)))) (h "0g6gpyvxzn9lnr8jg10in79sdrhs6pnc6w49f2xnj3mg8rg5b55b") (f (quote (("std" "serde/std" "uuid/std") ("default" "std"))))))

(define-public crate-fuid-1.2.2 (c (n "fuid") (v "1.2.2") (d (list (d (n "serde") (r "^1") (o #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "uuid") (r "^1") (f (quote ("v4"))) (k 0)))) (h "1416z1gqwv9ap7qg4mwl61f5qfvb43gaxmx3rnf9lc86qa7vf7kl") (f (quote (("std" "serde/std" "uuid/std") ("default" "std"))))))

