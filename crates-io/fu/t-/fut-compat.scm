(define-module (crates-io fu t- fut-compat) #:use-module (crates-io))

(define-public crate-fut-compat-0.1.0 (c (n "fut-compat") (v "0.1.0") (d (list (d (n "async-std") (r "^1") (f (quote ("unstable"))) (o #t) (d #t) (k 0)) (d (n "async-std") (r "^1") (f (quote ("unstable"))) (d #t) (k 2)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "net" "io-util" "fs"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "rt-multi-thread" "macros"))) (d #t) (k 2)) (d (n "tokio-stream") (r "^0.1") (f (quote ("fs"))) (o #t) (d #t) (k 0)))) (h "0kf1z5gs028xxh5zcdv78rbaarlbxdamqps8fbsnpk5n0wmjv6pk") (f (quote (("tokio-rt" "tokio" "tokio-stream") ("default") ("async-std-rt" "async-std"))))))

