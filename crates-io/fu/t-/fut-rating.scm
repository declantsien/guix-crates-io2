(define-module (crates-io fu t- fut-rating) #:use-module (crates-io))

(define-public crate-fut-rating-0.1.0 (c (n "fut-rating") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "itertools") (r "^0.10.0") (d #t) (k 0)))) (h "0d18xhdqsac7a8y84x73zgxqp91k1gdx6kl16f5hjzlp1bxfg1l0")))

(define-public crate-fut-rating-0.1.1 (c (n "fut-rating") (v "0.1.1") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8") (d #t) (k 0)))) (h "1dmc1s4f4xd4zyfzjqv5v0vgn41nxdc9pqks1z7imah8wn5jfc2c")))

