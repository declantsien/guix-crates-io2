(define-module (crates-io fu t- fut-ret) #:use-module (crates-io))

(define-public crate-fut-ret-0.1.0 (c (n "fut-ret") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "01qliql246qbm4kfw20kjyhbw58qyzcg496j00bclhf1hyk9fizq")))

(define-public crate-fut-ret-0.2.0 (c (n "fut-ret") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0k9nwfafx8j56dmb32641fnvfakvhjg2yq2dh1ymypgs2xkp4616")))

