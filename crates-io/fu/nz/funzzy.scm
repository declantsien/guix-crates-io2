(define-module (crates-io fu nz funzzy) #:use-module (crates-io))

(define-public crate-funzzy-0.1.0 (c (n "funzzy") (v "0.1.0") (d (list (d (n "docopt") (r "^0.6") (d #t) (k 0)) (d (n "glob") (r "^0.2.11") (d #t) (k 0)) (d (n "notify") (r "^2.5.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.3.2") (d #t) (k 0)))) (h "174rrsmv7xqilyhj819nwx1vlh6hij793anb0dwhmd759f7iv33x")))

(define-public crate-funzzy-0.2.0 (c (n "funzzy") (v "0.2.0") (d (list (d (n "docopt") (r "^0.6") (d #t) (k 0)) (d (n "glob") (r "^0.2.11") (d #t) (k 0)) (d (n "notify") (r "^2.5.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.3.2") (d #t) (k 0)))) (h "047dil4kynahxpzfih9m5wkyhicig1gspyr9vzqn1xyc7kdrfbab")))

(define-public crate-funzzy-0.2.4 (c (n "funzzy") (v "0.2.4") (d (list (d (n "docopt") (r "^0.6") (d #t) (k 0)) (d (n "glob") (r "^0.2.11") (d #t) (k 0)) (d (n "notify") (r "^2.5.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.3.2") (d #t) (k 0)))) (h "0734qhmgza52qlyn3rz2q24xpqj0j3346jf1wzi7c8ndswn95dx3")))

(define-public crate-funzzy-0.2.5 (c (n "funzzy") (v "0.2.5") (d (list (d (n "docopt") (r "^0.6") (d #t) (k 0)) (d (n "glob") (r "^0.2.11") (d #t) (k 0)) (d (n "notify") (r "^2.5.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.3.2") (d #t) (k 0)))) (h "0gfwanqmm0mb21cpmxkllykspwh3wd8jg85by9r59fqj6acr793l")))

(define-public crate-funzzy-0.3.0 (c (n "funzzy") (v "0.3.0") (d (list (d (n "docopt") (r "^0.6") (d #t) (k 0)) (d (n "glob") (r "^0.2.11") (d #t) (k 0)) (d (n "notify") (r "^2.5.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.3.2") (d #t) (k 0)))) (h "1xg4b6kp4syh5svw6ring37ab4zg76aras19b7jg9x93rp1dl5fz")))

(define-public crate-funzzy-0.3.1 (c (n "funzzy") (v "0.3.1") (d (list (d (n "docopt") (r "^0.6") (d #t) (k 0)) (d (n "glob") (r "^0.2.11") (d #t) (k 0)) (d (n "notify") (r "^2.5.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.3.2") (d #t) (k 0)))) (h "1f3iywx3qdrdf0hfjwhsm0fsiqk54rcg2xml30bww7dn3wa44lss")))

(define-public crate-funzzy-0.3.3 (c (n "funzzy") (v "0.3.3") (d (list (d (n "docopt") (r "^0.6") (d #t) (k 0)) (d (n "glob") (r "^0.2.11") (d #t) (k 0)) (d (n "notify") (r "^2.5.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.3.2") (d #t) (k 0)))) (h "113g3sfa651ga0za9bajmqjwgb9jfkw8y1fabyyi58i77ysa2g47")))

(define-public crate-funzzy-0.4.0 (c (n "funzzy") (v "0.4.0") (d (list (d (n "docopt") (r "^1") (d #t) (k 0)) (d (n "glob") (r "^0.2.11") (d #t) (k 0)) (d (n "notify") (r "^4.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.3.2") (d #t) (k 0)))) (h "0zzhsrxmqyj9h08z170qf95rm3kgd2v77ris0k9szy6grxrqksbh")))

(define-public crate-funzzy-0.5.0 (c (n "funzzy") (v "0.5.0") (d (list (d (n "docopt") (r "^1") (d #t) (k 0)) (d (n "glob") (r "^0.2.11") (d #t) (k 0)) (d (n "notify") (r "^4.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.3.2") (d #t) (k 0)))) (h "1iw5wjq853hyf5ykk6sn9lx2ns8ff0qyayd8cw6hm6r06wdvc4n9")))

(define-public crate-funzzy-0.6.0 (c (n "funzzy") (v "0.6.0") (d (list (d (n "docopt") (r "^1") (d #t) (k 0)) (d (n "glob") (r "^0.2.11") (d #t) (k 0)) (d (n "notify") (r "^4.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.3.2") (d #t) (k 0)))) (h "1f255hlsnks0hsw2w50sh2livw5cg95dj3bp8i4igd6khw2qyvl1")))

(define-public crate-funzzy-0.6.1 (c (n "funzzy") (v "0.6.1") (d (list (d (n "docopt") (r "^1") (d #t) (k 0)) (d (n "glob") (r "^0.2.11") (d #t) (k 0)) (d (n "notify") (r "^4.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.1") (d #t) (k 0)))) (h "1dkcfci2wjiqn4s6q1bl7j62c415d3jg31ib9kk1nv1gix3v6na0")))

(define-public crate-funzzy-1.0.0 (c (n "funzzy") (v "1.0.0") (d (list (d (n "docopt") (r "^1") (d #t) (k 0)) (d (n "glob") (r "^0.2.11") (d #t) (k 0)) (d (n "notify") (r "^4.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.1") (d #t) (k 0)))) (h "0wdwplnr7pxarkg9zyp9r6papcv21m357a4wgc5xnpfjdlr9vc8i")))

(define-public crate-funzzy-1.0.1 (c (n "funzzy") (v "1.0.1") (d (list (d (n "docopt") (r "^1") (d #t) (k 0)) (d (n "glob") (r "^0.2.11") (d #t) (k 0)) (d (n "notify") (r "^4.0.0") (d #t) (k 0)) (d (n "notify-debouncer-mini") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.1") (d #t) (k 0)))) (h "1n54x0r655bmypawzw3g6kqkp4kmxmn8bwgivkr3ljmyzwkkawdy")))

(define-public crate-funzzy-1.1.0 (c (n "funzzy") (v "1.1.0") (d (list (d (n "docopt") (r "^1") (d #t) (k 0)) (d (n "glob") (r "^0.2.11") (d #t) (k 0)) (d (n "nix") (r "^0.26.2") (d #t) (k 0)) (d (n "notify") (r "^4.0.0") (d #t) (k 0)) (d (n "notify-debouncer-mini") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.1") (d #t) (k 0)))) (h "17wapipcgy31mynppimblmlxhlv0i0ckh67p9wis22bqc0z83hyn")))

(define-public crate-funzzy-1.1.1 (c (n "funzzy") (v "1.1.1") (d (list (d (n "docopt") (r "^1") (d #t) (k 0)) (d (n "glob") (r "^0.2.11") (d #t) (k 0)) (d (n "nix") (r "^0.26.2") (d #t) (k 0)) (d (n "notify") (r "^4.0.0") (d #t) (k 0)) (d (n "notify-debouncer-mini") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.1") (d #t) (k 0)))) (h "000sfzl8wmv364wnkcqs6yxybfb1hsl8b133sh5pms0lr58bab0c")))

(define-public crate-funzzy-1.2.0 (c (n "funzzy") (v "1.2.0") (d (list (d (n "docopt") (r "^1") (d #t) (k 0)) (d (n "glob") (r "^0.2.11") (d #t) (k 0)) (d (n "nix") (r "^0.26.2") (d #t) (k 0)) (d (n "notify") (r "^4.0.0") (d #t) (k 0)) (d (n "notify-debouncer-mini") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.1") (d #t) (k 0)))) (h "0g63c5hm4rivlyw4v0khjqvil712d551h200fvvj8zj6a1xvibv8")))

