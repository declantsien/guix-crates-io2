(define-module (crates-io fu ck fucker) #:use-module (crates-io))

(define-public crate-fucker-0.4.0 (c (n "fucker") (v "0.4.0") (d (list (d (n "docopt") (r "^1.1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "053339x0whbc7lyb00bv0rrcbbqwm9h3alk20v9i8iadjq436hdb")))

(define-public crate-fucker-0.5.0 (c (n "fucker") (v "0.5.0") (d (list (d (n "docopt") (r "^1.1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "01g5y40zzh3nxh2kqrxl89bkhkavbv6rqd2mykr355f5dyjh29d0")))

(define-public crate-fucker-0.5.1 (c (n "fucker") (v "0.5.1") (d (list (d (n "docopt") (r "^1.1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0q7ksjp2aas25pa2rh9ibrilhphmwidips2pyhm0qd6v72ny6xib")))

(define-public crate-fucker-0.5.2 (c (n "fucker") (v "0.5.2") (d (list (d (n "docopt") (r "^1.1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0614ak90xbj1prsb4ljsnj1zahj1w22p90pvh3fgj8wpxyyjh0si")))

(define-public crate-fucker-0.5.3 (c (n "fucker") (v "0.5.3") (d (list (d (n "docopt") (r "^1.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "03nmcwrrj3fvn187m0r2dhgcv50cmdc13dqgqc909qybs76gbyb0")))

