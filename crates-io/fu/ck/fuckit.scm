(define-module (crates-io fu ck fuckit) #:use-module (crates-io))

(define-public crate-fuckit-0.1.0 (c (n "fuckit") (v "0.1.0") (h "1ckkpf5wg6yb8vlhvdm6qy9a7h9qvrlskh7m0qk4ykwnbp9sxrdw") (y #t)))

(define-public crate-fuckit-0.1.1 (c (n "fuckit") (v "0.1.1") (h "1vml9rym1pz246g341wrj8x3dp2pbgjj19cahl6bnzm97j0p8c61") (y #t)))

(define-public crate-fuckit-0.2.0 (c (n "fuckit") (v "0.2.0") (h "1rqd85nx88azhzipjkk4a7phhw5i9wiw5fjvqy38ixwf0ii34idb") (y #t)))

(define-public crate-fuckit-0.2.1 (c (n "fuckit") (v "0.2.1") (h "1jdw992lnj29ipl94k9s30rln7vm9qvx2rdj5gazmy0bcp5s7fsg") (y #t)))

(define-public crate-fuckit-0.2.2-fuckit (c (n "fuckit") (v "0.2.2-fuckit") (h "0zw9smjf6di2i6n59hbaggl2aw9c8yn5dc9idr51bmjkqn09d0gz") (y #t)))

(define-public crate-fuckit-0.0.0-- (c (n "fuckit") (v "0.0.0--") (h "1gcn14fbvlxs7b2hhcvmiizjyz3bh79md7w9qxh4fpbj4s2195b3") (y #t)))

