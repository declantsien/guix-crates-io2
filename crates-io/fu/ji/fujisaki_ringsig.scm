(define-module (crates-io fu ji fujisaki_ringsig) #:use-module (crates-io))

(define-public crate-fujisaki_ringsig-0.0.1 (c (n "fujisaki_ringsig") (v "0.0.1") (d (list (d (n "blake2") (r "^0.6") (d #t) (k 0)) (d (n "curve25519-dalek") (r "^0.9") (f (quote ("yolocrypto"))) (d #t) (k 0)) (d (n "digest") (r "^0.6") (d #t) (k 0)) (d (n "generic-array") (r "^0.8") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1n68dgh8bs33lc7mfymf6a8zl9bkr7rsvfr9n7kz815ksm30dh0y")))

(define-public crate-fujisaki_ringsig-0.0.2 (c (n "fujisaki_ringsig") (v "0.0.2") (d (list (d (n "blake2") (r "^0.6") (d #t) (k 0)) (d (n "curve25519-dalek") (r "^0.9") (f (quote ("yolocrypto"))) (d #t) (k 0)) (d (n "digest") (r "^0.6") (d #t) (k 0)) (d (n "generic-array") (r "^0.8") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "16a8vkk489mnl3908ms33qg41p74c78bk01hv6rcnnpqknlfp3lj")))

(define-public crate-fujisaki_ringsig-0.0.3 (c (n "fujisaki_ringsig") (v "0.0.3") (d (list (d (n "blake2") (r "^0.6") (d #t) (k 0)) (d (n "curve25519-dalek") (r "^0.13") (d #t) (k 0)) (d (n "digest") (r "^0.6") (d #t) (k 0)) (d (n "generic-array") (r "^0.9") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "09mkrq7bygrphxzjj6svap9dvdrqbn52pg14wba1sr9mrz729ygq")))

(define-public crate-fujisaki_ringsig-0.1.0 (c (n "fujisaki_ringsig") (v "0.1.0") (d (list (d (n "blake2") (r "^0.9") (k 0)) (d (n "curve25519-dalek") (r "^3.0") (f (quote ("u64_backend"))) (k 0)) (d (n "digest") (r "^0.9") (k 0)) (d (n "generic-array") (r "^0.9") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "rand_core") (r "^0.5") (k 0)))) (h "1k63sw5y0q5kj3c336d86dc3sh4v5yy8yf8xd6b6g723imkw87nc") (f (quote (("std" "blake2/std" "curve25519-dalek/std" "digest/std") ("default" "std"))))))

(define-public crate-fujisaki_ringsig-0.1.1 (c (n "fujisaki_ringsig") (v "0.1.1") (d (list (d (n "blake2") (r "^0.9") (k 0)) (d (n "curve25519-dalek") (r "^3.0") (f (quote ("u64_backend"))) (k 0)) (d (n "digest") (r "^0.9") (k 0)) (d (n "generic-array") (r "^0.9") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "rand_core") (r "^0.5") (k 0)))) (h "0xmmcrmbkn4b2v4agpxy58pjqaa2gickq5c62014n4ny6f6nq3qk") (f (quote (("std" "blake2/std" "curve25519-dalek/std" "digest/std") ("default" "std"))))))

