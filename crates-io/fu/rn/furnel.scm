(define-module (crates-io fu rn furnel) #:use-module (crates-io))

(define-public crate-furnel-0.1.0 (c (n "furnel") (v "0.1.0") (d (list (d (n "brotli") (r "^3.3.3") (d #t) (k 0)) (d (n "clap") (r "^3.0.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)))) (h "134mv950v2mxhh7fx5xwdmjfsk918cv8bnlqq6qxdwvy8n9lwq7r") (r "1.56.1")))

