(define-module (crates-io fu rn furnace-iui) #:use-module (crates-io))

(define-public crate-furnace-iui-0.2.0 (c (n "furnace-iui") (v "0.2.0") (d (list (d (n "furnace") (r "^0.2.0") (d #t) (k 0)) (d (n "iui") (r "^0.3") (d #t) (k 0)))) (h "126jp0lkhxw8fnyyf53gjk6j8x70gi2zhrz38x5dbc75rj685d8c")))

(define-public crate-furnace-iui-0.3.0 (c (n "furnace-iui") (v "0.3.0") (d (list (d (n "furnace") (r "^0.3.0") (d #t) (k 0)) (d (n "iui") (r "^0.3") (d #t) (k 0)))) (h "10lbn2mg0hamn8j5awxq3kmrmyw86g4lxvca5w5ma2864jcy5yw5")))

