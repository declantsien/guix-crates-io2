(define-module (crates-io fu ry fury-renegade-rgb) #:use-module (crates-io))

(define-public crate-fury-renegade-rgb-0.1.0 (c (n "fury-renegade-rgb") (v "0.1.0") (d (list (d (n "bpaf") (r "^0.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.5") (d #t) (k 0)) (d (n "i2cdev") (r "^0.5.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0sn5h639ldnpfdvp4wypvz8gqhx9wn51jw60bj450a54qx3sjy0z")))

