(define-module (crates-io fu nt funtime) #:use-module (crates-io))

(define-public crate-funtime-0.1.0 (c (n "funtime") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "16izal3h3i5vda9k33k9zwnl3y32fbqwmra7p0361r0f1y8g2022")))

(define-public crate-funtime-0.2.0 (c (n "funtime") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0576w82sp649p30bfb9dmg1gcf2bc2b8l3r4s1x7mfh9gd8jf26c")))

(define-public crate-funtime-0.2.1 (c (n "funtime") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0235d9za99lzl5pxfjayf49lskym7crdpr2m9p231c1i78dx0ny5")))

(define-public crate-funtime-0.3.0 (c (n "funtime") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1hqyvw4vxhicp6i152n2lbx7j7h383igs137ic0znss1mx4gb8sv")))

(define-public crate-funtime-0.3.1 (c (n "funtime") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "19x97gjlqqwddqbaw5mz9qrvz0ir5qcv0jbvqpnqgwaz0vqk94hi")))

