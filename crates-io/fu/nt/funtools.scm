(define-module (crates-io fu nt funtools) #:use-module (crates-io))

(define-public crate-funtools-0.1.0 (c (n "funtools") (v "0.1.0") (h "01784za94jrbajdbq66q98b7qzy2iplfzzz5jdpcscbx4k6y542a") (y #t)))

(define-public crate-funtools-0.1.1 (c (n "funtools") (v "0.1.1") (h "13bpk7cs4qgh4ws14s62nsilc1ma8r3jc2268z4sb6wca8y08rg0") (y #t)))

(define-public crate-funtools-0.2.0 (c (n "funtools") (v "0.2.0") (h "0lhm9m1ya61bbccglzh6sn7mgkkqa5gjkbywf8cj5a01441hc6b8") (y #t)))

