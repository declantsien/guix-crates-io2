(define-module (crates-io fu ni funix) #:use-module (crates-io))

(define-public crate-funix-1.0.0 (c (n "funix") (v "1.0.0") (d (list (d (n "directories") (r "^4.0.1") (d #t) (k 0)) (d (n "git2") (r "^0.14.1") (f (quote ("https"))) (k 0)) (d (n "spinners") (r "^3.0.1") (d #t) (k 0)))) (h "15dc7acylf7cfq35054khsvicxv3fqgwjj6c955nrzq0wck01jcb") (y #t) (r "1.56.0")))

(define-public crate-funix-1.0.1 (c (n "funix") (v "1.0.1") (d (list (d (n "directories") (r "^4.0.1") (d #t) (k 0)) (d (n "git2") (r "^0.14.1") (f (quote ("https"))) (k 0)) (d (n "spinners") (r "^3.0.1") (d #t) (k 0)))) (h "1khg6055jqd752if10n9si3nhgd7zd9vlcx9z549ng5bqjllg5h4") (y #t) (r "1.56.0")))

