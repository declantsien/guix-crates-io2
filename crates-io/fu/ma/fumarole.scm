(define-module (crates-io fu ma fumarole) #:use-module (crates-io))

(define-public crate-fumarole-0.1.0 (c (n "fumarole") (v "0.1.0") (d (list (d (n "glium") (r "^0.26.0-alpha6") (d #t) (k 0)) (d (n "image") (r "^0.22") (d #t) (k 0)) (d (n "rusttype") (r "^0.3.0") (d #t) (k 0)))) (h "0m0i2d3jfhrnh2cyxl5q4d15jzyd3rvjjpnvq2zpja4293g2g4kx")))

(define-public crate-fumarole-0.1.1 (c (n "fumarole") (v "0.1.1") (d (list (d (n "glium") (r "^0.26.0-alpha6") (d #t) (k 0)) (d (n "image") (r "^0.22") (d #t) (k 0)) (d (n "rusttype") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)))) (h "0grb656ssh3lv5185r6a9511sy4vwsam6bdyfkfq6xxbdp08llaa")))

(define-public crate-fumarole-0.1.2 (c (n "fumarole") (v "0.1.2") (d (list (d (n "glium") (r "^0.26.0-alpha6") (d #t) (k 0)) (d (n "image") (r "^0.22") (d #t) (k 0)) (d (n "rusttype") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)))) (h "07jc57rkwinhl3djrs2b91z4vvby45qw6dflffvbfrr15fkrwqq1")))

(define-public crate-fumarole-0.1.21 (c (n "fumarole") (v "0.1.21") (d (list (d (n "glium") (r "^0.26.0-alpha6") (d #t) (k 0)) (d (n "image") (r "^0.22") (d #t) (k 0)) (d (n "rusttype") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)))) (h "0kxwpc3h5ksih4wqwmdqh0k8pk09ig1phdjj80n4iykpy5vqv2c9")))

(define-public crate-fumarole-0.1.22 (c (n "fumarole") (v "0.1.22") (d (list (d (n "glium") (r "^0.26.0-alpha6") (d #t) (k 0)) (d (n "image") (r "^0.22") (d #t) (k 0)) (d (n "rusttype") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)))) (h "1w1yxfjns2mf0pfhalyqc8y0673f4s151cn9d315yhbsy3589iv2")))

(define-public crate-fumarole-0.1.23 (c (n "fumarole") (v "0.1.23") (d (list (d (n "glium") (r "^0.26.0-alpha6") (d #t) (k 0)) (d (n "image") (r "^0.22") (d #t) (k 0)) (d (n "rusttype") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)))) (h "078pinhxlfqpfdyp5va6wi2ikipps2giz9627108g8vdal7d6ixg")))

