(define-module (crates-io fu ze fuzed-iterator) #:use-module (crates-io))

(define-public crate-fuzed-iterator-1.0.0 (c (n "fuzed-iterator") (v "1.0.0") (h "0q2pik1i77d0skg2s6ij70l6jzxxz7sd83ay2kqpwl3cpqg125qi")))

(define-public crate-fuzed-iterator-1.0.1 (c (n "fuzed-iterator") (v "1.0.1") (h "1dn62ir0406iddff1m0cdqrf40npsvh3a82mwdcr3imxakkf9ja6")))

