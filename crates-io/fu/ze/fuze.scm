(define-module (crates-io fu ze fuze) #:use-module (crates-io))

(define-public crate-fuze-1.0.0 (c (n "fuze") (v "1.0.0") (d (list (d (n "async-std") (r "^1.9.0") (d #t) (k 0)))) (h "0hvgmvhidcha73f1r5x2gr5vlvpb1ka6ayidhw0j089fshplxhrp")))

(define-public crate-fuze-2.0.0 (c (n "fuze") (v "2.0.0") (d (list (d (n "async-std") (r "^1.9.0") (d #t) (k 0)))) (h "04whqd6lfkqsl9bvv4yfc5fha6ykbhsj36xqlz627vxl4d3xd04v")))

(define-public crate-fuze-3.0.0 (c (n "fuze") (v "3.0.0") (d (list (d (n "async-std") (r "^1.9.0") (o #t) (d #t) (k 0)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "tokio") (r "^1.8.2") (f (quote ("sync"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.8.2") (f (quote ("sync" "rt" "time" "rt-multi-thread"))) (d #t) (k 2)))) (h "1gq3rq9k8g24z227l1rlwb50w4yj8mg1la9xbmny2h3zwk31jxmz") (f (quote (("default" "async-std"))))))

