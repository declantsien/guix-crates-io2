(define-module (crates-io fu mo fumola_proc_macro) #:use-module (crates-io))

(define-public crate-fumola_proc_macro-0.0.29 (c (n "fumola_proc_macro") (v "0.0.29") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "fumola") (r "^0.0.28") (f (quote ("parser"))) (k 0)) (d (n "num-bigint") (r "^0.4.3") (f (quote ("serde"))) (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.44") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (d #t) (k 0)) (d (n "syn") (r "^1.0.100") (d #t) (k 0)) (d (n "test-log") (r "^0.2.11") (d #t) (k 2)))) (h "1wpgi8ls8a6zkbwv0zj8pdz5cjqi1c7m55bsayliycsrnxi31d5a")))

