(define-module (crates-io fu ba fubar-cli) #:use-module (crates-io))

(define-public crate-fubar-cli-0.1.0 (c (n "fubar-cli") (v "0.1.0") (d (list (d (n "arboard") (r "^3.2.0") (d #t) (k 0)) (d (n "crossterm") (r "^0.26.1") (d #t) (k 0)) (d (n "tui") (r "^0.19.0") (d #t) (k 0)))) (h "01zyhmjb9w437lbw95j059jkgszm08zzskiqh42h264bv7qy832b") (y #t)))

(define-public crate-fubar-cli-0.1.1 (c (n "fubar-cli") (v "0.1.1") (d (list (d (n "arboard") (r "^3.2.0") (d #t) (k 0)) (d (n "crossterm") (r "^0.26.1") (d #t) (k 0)) (d (n "tui") (r "^0.19.0") (d #t) (k 0)))) (h "0dpfw80ry6lh10yr3hpz7hdvkl2kpxvph2a9anrgxbksbn9k9q1m")))

(define-public crate-fubar-cli-0.1.2 (c (n "fubar-cli") (v "0.1.2") (d (list (d (n "arboard") (r "^3.2.0") (d #t) (k 0)) (d (n "crossterm") (r "^0.26.1") (d #t) (k 0)) (d (n "tui") (r "^0.19.0") (d #t) (k 0)))) (h "1ymawlpymr3nwfwid66p68cp7rl82dvn3n4ymapcc7xqpf64ydc1")))

(define-public crate-fubar-cli-0.1.3 (c (n "fubar-cli") (v "0.1.3") (d (list (d (n "arboard") (r "^3.2.0") (d #t) (k 0)) (d (n "crossterm") (r "^0.26.1") (d #t) (k 0)) (d (n "tui") (r "^0.19.0") (d #t) (k 0)))) (h "0cvvb6hwrc6vinjyxyy9s5lx1ks2cp3n832zk7n49a9980xc2g8k")))

