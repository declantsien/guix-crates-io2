(define-module (crates-io fu ri furigana) #:use-module (crates-io))

(define-public crate-furigana-0.1.0 (c (n "furigana") (v "0.1.0") (d (list (d (n "wana_kana") (r "^2.1.0") (d #t) (k 0)))) (h "0hz1qz36pzvsb95hqx7h98nw8kh5ha7gvxq3dxzg4ly2a70078g8") (r "1.56")))

(define-public crate-furigana-0.1.1 (c (n "furigana") (v "0.1.1") (h "1kbvn3hlbrafq53z3klxrq85hqqcv1mwvxalvy6inx3n1l5s6rlz") (r "1.56")))

