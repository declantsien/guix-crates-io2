(define-module (crates-io fu tf futf) #:use-module (crates-io))

(define-public crate-futf-0.1.0 (c (n "futf") (v "0.1.0") (d (list (d (n "mac") (r "^0") (d #t) (k 0)))) (h "10ih28zvmjll4d3mbfdp0bybw6b4wczpl8vvd4h3h90q683rx96b")))

(define-public crate-futf-0.1.1 (c (n "futf") (v "0.1.1") (d (list (d (n "debug_unreachable") (r "^0.0") (d #t) (k 0)) (d (n "mac") (r "^0.0") (d #t) (k 0)))) (h "05l0a3q5i9d9as8ds664c1dbk9ii8b7s37ds4xh8ghdyswas1kjl")))

(define-public crate-futf-0.1.2 (c (n "futf") (v "0.1.2") (d (list (d (n "debug_unreachable") (r "^0.1.1") (d #t) (k 0)) (d (n "mac") (r "^0.0.2") (d #t) (k 0)))) (h "1b98djs2izdchg3p4y9r1fyqxikmh0bxkcv4qm8knmd2h29niag7")))

(define-public crate-futf-0.1.3 (c (n "futf") (v "0.1.3") (d (list (d (n "debug_unreachable") (r "^0.1.1") (d #t) (k 0)) (d (n "mac") (r "^0.1.0") (d #t) (k 0)))) (h "1wwqwcj4pg07hiy4hrl2m5chlq04s133a2w1spf985xswqykzyai")))

(define-public crate-futf-0.1.4 (c (n "futf") (v "0.1.4") (d (list (d (n "mac") (r "^0.1.0") (d #t) (k 0)) (d (n "new_debug_unreachable") (r "^1.0.0") (d #t) (k 0)))) (h "0fxc18bnabird5jl941nsd6d25vq8cn8barmz4d30dlkzbiir73w")))

(define-public crate-futf-0.1.5 (c (n "futf") (v "0.1.5") (d (list (d (n "mac") (r "^0.1.0") (d #t) (k 0)) (d (n "new_debug_unreachable") (r "^1.0.2") (d #t) (k 0)))) (h "0hvqk2r7v4fnc34hvc3vkri89gn52d5m9ihygmwn75l1hhp0whnz")))

