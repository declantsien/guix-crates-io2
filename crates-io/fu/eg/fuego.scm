(define-module (crates-io fu eg fuego) #:use-module (crates-io))

(define-public crate-fuego-0.1.0 (c (n "fuego") (v "0.1.0") (d (list (d (n "dns-lookup") (r "^1.0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.133") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.74") (d #t) (k 0)) (d (n "tokio") (r "^1.15.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-test") (r "^0.4.2") (d #t) (k 0)))) (h "1smf7wivc8ih9fkhfp0yqdn9x657q4asdmf47yq2n8chkm2svyf0")))

(define-public crate-fuego-0.1.1 (c (n "fuego") (v "0.1.1") (d (list (d (n "dns-lookup") (r "^1.0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.133") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.74") (d #t) (k 0)) (d (n "tokio") (r "^1.15.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-test") (r "^0.4.2") (d #t) (k 0)))) (h "1bri1qkc1qyg448yfdw3yll5rkz74fjkgqrlwpwv81hp368sgrcf")))

