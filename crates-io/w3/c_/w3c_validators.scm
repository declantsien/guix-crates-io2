(define-module (crates-io w3 c_ w3c_validators) #:use-module (crates-io))

(define-public crate-w3c_validators-0.1.0 (c (n "w3c_validators") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "019g0w4l9ra9ziq3m8c0hjkd6pgn5rsmy110kl56rcazj1s6s25d")))

