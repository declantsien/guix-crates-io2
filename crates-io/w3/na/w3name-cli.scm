(define-module (crates-io w3 na w3name-cli) #:use-module (crates-io))

(define-public crate-w3name-cli-0.1.0 (c (n "w3name-cli") (v "0.1.0") (d (list (d (n "clap") (r "^3.2.22") (f (quote ("derive"))) (d #t) (k 0)) (d (n "error-stack") (r "^0.1.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "w3name") (r "^0.1.0") (d #t) (k 0)))) (h "0gk9491x2nqp06bi6xysgb3pxdcaqshxkna8kj56nw94na57sipk")))

(define-public crate-w3name-cli-0.1.4 (c (n "w3name-cli") (v "0.1.4") (d (list (d (n "clap") (r "^3.2.22") (f (quote ("derive"))) (d #t) (k 0)) (d (n "error-stack") (r "^0.1.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "w3name") (r "^0.1.4") (d #t) (k 0)))) (h "11y0ffshcpwgwyqs9msshs15nfisdpmhwi64b7758hqnfzcpj8fk")))

(define-public crate-w3name-cli-0.2.2 (c (n "w3name-cli") (v "0.2.2") (d (list (d (n "clap") (r "^3.2.22") (f (quote ("derive"))) (d #t) (k 0)) (d (n "error-stack") (r "^0.1.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "w3name") (r "^0.1.5") (d #t) (k 0)))) (h "0xcwax5i7i7iaz51fz8nipr1jb1iqpimf3dzj7bh1iprigkmbs04")))

(define-public crate-w3name-cli-0.2.3 (c (n "w3name-cli") (v "0.2.3") (d (list (d (n "clap") (r "^3.2.22") (f (quote ("derive"))) (d #t) (k 0)) (d (n "error-stack") (r "^0.1.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "w3name") (r "^0.1.6") (d #t) (k 0)))) (h "1nj0a7pxpzyy2wdv2wf2izymhj8ix4ndkdxv3la9p05awjsvrcvy")))

(define-public crate-w3name-cli-0.2.4 (c (n "w3name-cli") (v "0.2.4") (d (list (d (n "clap") (r "^3.2.22") (f (quote ("derive"))) (d #t) (k 0)) (d (n "error-stack") (r "^0.1.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "w3name") (r "^0.1.7") (d #t) (k 0)))) (h "06cjj3qfqr7ag6853in51drkcaidrnz4mkz2x100fzq3w8p3kkrl")))

