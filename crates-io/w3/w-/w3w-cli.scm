(define-module (crates-io w3 w- w3w-cli) #:use-module (crates-io))

(define-public crate-w3w-cli-0.1.0 (c (n "w3w-cli") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "strum") (r "^0.20.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.2") (f (quote ("json"))) (d #t) (k 0)) (d (n "w3w-api") (r "^0.1") (d #t) (k 0)))) (h "08y12k7q81zf9l94rv7y71fdsgcqzrqk0hw872l8h4acjpdna5yj")))

(define-public crate-w3w-cli-0.1.1 (c (n "w3w-cli") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "strum") (r "^0.20.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.2") (f (quote ("json"))) (d #t) (k 0)) (d (n "w3w-api") (r "^0.1") (d #t) (k 0)))) (h "1vxm0rba9w5gqf5xv640m68jy5lpdbp6z9azjaqhph4zj0dxq2gl")))

