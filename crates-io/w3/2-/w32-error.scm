(define-module (crates-io w3 #{2-}# w32-error) #:use-module (crates-io))

(define-public crate-w32-error-1.0.0 (c (n "w32-error") (v "1.0.0") (d (list (d (n "winapi") (r "^0.3.0") (f (quote ("errhandlingapi" "winbase"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "0hzvz97zfd3crz8p3ydnir2nn3rl4mbrhw615z0niqcipnk62z7s") (f (quote (("std" "winapi/std"))))))

