(define-module (crates-io g_ li g_lib) #:use-module (crates-io))

(define-public crate-g_lib-0.1.0 (c (n "g_lib") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 0)) (d (n "sqlite") (r "^0.30.3") (d #t) (k 0)) (d (n "tokio") (r "^1.24.2") (f (quote ("full"))) (d #t) (k 0)))) (h "0i3kk5cr4risb94k9xk5z6s9xm9h2z7fjw1rklkig24gj05hlpw2")))

(define-public crate-g_lib-0.2.0 (c (n "g_lib") (v "0.2.0") (d (list (d (n "actix-web") (r "^4.3.1") (d #t) (k 0)) (d (n "image") (r "^0.24.5") (d #t) (k 0)) (d (n "lettre") (r "^0.10") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 0)) (d (n "sqlite") (r "^0.30.3") (d #t) (k 0)) (d (n "tokio") (r "^1.24.2") (f (quote ("full"))) (d #t) (k 0)))) (h "1mqsa3lkaxjky8x06y6krr6w21z9dql611iyl72bzz2cf96hn5k7")))

