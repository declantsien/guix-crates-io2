(define-module (crates-io tf -k tf-kubernetes) #:use-module (crates-io))

(define-public crate-tf-kubernetes-0.1.0 (c (n "tf-kubernetes") (v "0.1.0") (d (list (d (n "tf-bindgen") (r "^0.1.0") (d #t) (k 0)) (d (n "tf-bindgen") (r "^0.1.0") (d #t) (k 1)))) (h "0s6v702m617mnpl05pc7zl0yk8i0sslazpdy6jak4mjal2lhx2n8")))

(define-public crate-tf-kubernetes-0.1.1 (c (n "tf-kubernetes") (v "0.1.1") (d (list (d (n "tf-bindgen") (r "^0.1.0") (d #t) (k 0)) (d (n "tf-bindgen") (r "^0.1.0") (d #t) (k 1)))) (h "0afmzfnwhk0ki023bc5gbd6h22xqsb32xmxfjbwkgfnicz4hq3ki")))

