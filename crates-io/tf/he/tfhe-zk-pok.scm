(define-module (crates-io tf he tfhe-zk-pok) #:use-module (crates-io))

(define-public crate-tfhe-zk-pok-0.1.0 (c (n "tfhe-zk-pok") (v "0.1.0") (d (list (d (n "ark-bls12-381") (r "^0.4.0") (d #t) (k 0)) (d (n "ark-ec") (r "^0.4.2") (d #t) (k 0)) (d (n "ark-ff") (r "^0.4.2") (d #t) (k 0)) (d (n "ark-poly") (r "^0.4.2") (d #t) (k 0)) (d (n "ark-serialize") (r "^0.4.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rayon") (r "^1.8.0") (d #t) (k 0)) (d (n "serde") (r "~1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "~1.0") (d #t) (k 2)) (d (n "sha3") (r "^0.10.8") (d #t) (k 0)) (d (n "zeroize") (r "^1.7.0") (d #t) (k 0)))) (h "1jdqchj0nm7zqidq67fn9h8wmnyl2m729x3ycnf78h4pmaq62nr9")))

