(define-module (crates-io tf he tfhe-cuda-backend) #:use-module (crates-io))

(define-public crate-tfhe-cuda-backend-0.1.2 (c (n "tfhe-cuda-backend") (v "0.1.2") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1nz5mm0ih8da95rv6wwc86s7z1x8g5im9d3bwmk4819pfs7y8n8z")))

(define-public crate-tfhe-cuda-backend-0.1.3 (c (n "tfhe-cuda-backend") (v "0.1.3") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1qhwhiy7kdsbj2z6vzzcrsp4gpsfbbdg87hj7bs47p48j0ya8l7s")))

(define-public crate-tfhe-cuda-backend-0.2.0 (c (n "tfhe-cuda-backend") (v "0.2.0") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "093z4yag2b7wd8g8qgjjzicwnwz98vckf4fa6ppddnk6my9q8cv5")))

