(define-module (crates-io tf he tfhe-c-api-dynamic-buffer) #:use-module (crates-io))

(define-public crate-tfhe-c-api-dynamic-buffer-0.1.0 (c (n "tfhe-c-api-dynamic-buffer") (v "0.1.0") (d (list (d (n "cbindgen") (r "^0.26.0") (o #t) (d #t) (k 1)))) (h "1yq0zx549cr5gmk5y197mwm4ipi6xlv185hbla83y68am6m139l8") (f (quote (("c_api_print_error_source")))) (s 2) (e (quote (("c_api" "dep:cbindgen"))))))

