(define-module (crates-io tf le tfledge) #:use-module (crates-io))

(define-public crate-tfledge-0.0.0 (c (n "tfledge") (v "0.0.0") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)) (d (n "env_logger") (r "^0.11.3") (d #t) (k 0)) (d (n "libc") (r "^0.2.154") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "rgb") (r "^0.8.37") (d #t) (k 0)))) (h "1rwzpbr1qvwrl18hik39m5vzijh8gjlha5kx5pfz85c3ibfrr8ix") (f (quote (("throttled") ("direct") ("default" "throttled") ("__bindgen"))))))

