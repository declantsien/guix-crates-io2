(define-module (crates-io tf on tfon) #:use-module (crates-io))

(define-public crate-tfon-0.1.0 (c (n "tfon") (v "0.1.0") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1zazr7z8w0vx0pn0k5c38jr2pfkhhynvwha7zzq2gk9qgd1k0sfl")))

(define-public crate-tfon-0.1.1 (c (n "tfon") (v "0.1.1") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0962fr3gp5b44c6zi7z2il72ylprs90ghjp63aqmbh802i60clk1")))

