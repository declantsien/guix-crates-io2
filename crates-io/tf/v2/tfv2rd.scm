(define-module (crates-io tf v2 tfv2rd) #:use-module (crates-io))

(define-public crate-tfv2rd-0.1.0 (c (n "tfv2rd") (v "0.1.0") (d (list (d (n "jsonschema") (r "^0.12") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "path-absolutize") (r "^3.0") (d #t) (k 0)) (d (n "pathdiff") (r "^0.2") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("raw_value"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0657zmnhbnybvc07lcsi6dq01g196sd64lyqlkvvq2nlmxw4rfy1")))

