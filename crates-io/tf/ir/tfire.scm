(define-module (crates-io tf ir tfire) #:use-module (crates-io))

(define-public crate-tfire-0.1.0 (c (n "tfire") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "termion") (r "^1.5.6") (d #t) (k 0)))) (h "0fl2z9iklvrqvr7a8qi2gwwviarhy8vcjmmc7n9g7nlg1qcgpxd0")))

