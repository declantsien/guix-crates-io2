(define-module (crates-io tf ut tfutils) #:use-module (crates-io))

(define-public crate-tfutils-0.1.0 (c (n "tfutils") (v "0.1.0") (d (list (d (n "metrics-exporter-prometheus") (r "^0.14.0") (d #t) (k 0)))) (h "0f9k19k4p0365xsvgyazpjhay2lgb04p9rc2n80sq8amj3k3wf30")))

(define-public crate-tfutils-0.1.1 (c (n "tfutils") (v "0.1.1") (d (list (d (n "arc-swap") (r "^1.7.1") (d #t) (k 0)) (d (n "config") (r "^0.14.0") (d #t) (k 0)) (d (n "eyre") (r "^0.6.12") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "metrics-exporter-prometheus") (r "^0.14.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.201") (f (quote ("derive"))) (d #t) (k 0)))) (h "0az8kfl9sh7prs8my7dqy2ci3ma3h56nwg14lnff4s7krc3shmx9")))

(define-public crate-tfutils-0.1.2 (c (n "tfutils") (v "0.1.2") (d (list (d (n "arc-swap") (r "^1.7.1") (d #t) (k 0)) (d (n "config") (r "^0.14.0") (d #t) (k 0)) (d (n "eyre") (r "^0.6.12") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "metrics-exporter-prometheus") (r "^0.14.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.201") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.18") (f (quote ("fmt" "env-filter" "json"))) (d #t) (k 0)))) (h "1vkl5yffvspkzwjdzizdcyvx5bj3i9aq8v8rfvplc9rldiq4vn9k")))

(define-public crate-tfutils-0.1.3 (c (n "tfutils") (v "0.1.3") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "metrics-exporter-prometheus") (r "^0.14.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.201") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.18") (f (quote ("fmt" "env-filter" "json"))) (d #t) (k 0)))) (h "1a66bnr25q3b7g31j1f6visx6ncd9ddbgz371fy4x06y231v44rg")))

