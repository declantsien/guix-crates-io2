(define-module (crates-io tf _f tf_filter) #:use-module (crates-io))

(define-public crate-tf_filter-0.1.0 (c (n "tf_filter") (v "0.1.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tf_observer") (r "^0.1.0") (d #t) (k 0)))) (h "12fas8w6wm1aa3iwk6i9zrqg15qby5kh4p3m2x8pky8qx7m3qg8v")))

(define-public crate-tf_filter-0.1.1 (c (n "tf_filter") (v "0.1.1") (d (list (d (n "tf_observer") (r "^0.1.1") (d #t) (k 0)))) (h "15zq8y8jd9b64gw7ql6sjja6lvmc6rxyxq5jy8bhv41wf0bghfb8")))

(define-public crate-tf_filter-0.1.2 (c (n "tf_filter") (v "0.1.2") (d (list (d (n "tf_observer") (r "^0.1.2") (d #t) (k 0)))) (h "1mj5j9mdg5238cklklzv6s99l4vghfld8ajsgawzl0wr793vgdrl")))

(define-public crate-tf_filter-0.1.3 (c (n "tf_filter") (v "0.1.3") (d (list (d (n "tf_observer") (r "^0.1.3") (d #t) (k 0)))) (h "1s8phnxypvy1wyp7l5zbxrrnqif8la4kasbs0yccahrxc6mlq8mm")))

