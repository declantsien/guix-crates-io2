(define-module (crates-io tf s- tfs-fuse-sys) #:use-module (crates-io))

(define-public crate-tfs-fuse-sys-0.1.0 (c (n "tfs-fuse-sys") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1g4mlv7idqv8frk42qs2i8fcpw9py9gwka2iv6hbx1jzjn994vkl")))

(define-public crate-tfs-fuse-sys-0.1.1 (c (n "tfs-fuse-sys") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1gys22c33r5pabpva9320digib45wyaxllyb1iivm0r6akvkxkaw")))

