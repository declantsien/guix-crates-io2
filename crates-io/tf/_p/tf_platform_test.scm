(define-module (crates-io tf _p tf_platform_test) #:use-module (crates-io))

(define-public crate-tf_platform_test-0.1.0 (c (n "tf_platform_test") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("rustls-tls"))) (k 0)) (d (n "tf_core") (r "^0.1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.9") (f (quote ("macros"))) (d #t) (k 0)))) (h "0sxyl4fcz0nbj8j2z9h0z0n4b34fs27dynb7yv59pv88yq21rshl")))

(define-public crate-tf_platform_test-0.1.1 (c (n "tf_platform_test") (v "0.1.1") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("rustls-tls"))) (k 0)) (d (n "tf_core") (r "^0.1.1") (d #t) (k 0)) (d (n "tokio") (r "^1.9") (f (quote ("macros"))) (d #t) (k 0)))) (h "0d51vjjqhr43v7dikpgjalkmxidr77mi0al75dbqg4hdrckg98rb")))

(define-public crate-tf_platform_test-0.1.2 (c (n "tf_platform_test") (v "0.1.2") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("rustls-tls"))) (k 0)) (d (n "tf_core") (r "^0.1.2") (d #t) (k 0)) (d (n "tokio") (r "^1.17") (f (quote ("macros"))) (d #t) (k 0)))) (h "09hlidpjsyfq8bjjg3k4w31xvivas0fiw8ygz0bq9n69cnr5rlmd")))

(define-public crate-tf_platform_test-0.1.3 (c (n "tf_platform_test") (v "0.1.3") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("rustls-tls"))) (k 0)) (d (n "tf_core") (r "^0.1.3") (d #t) (k 0)) (d (n "tokio") (r "^1.29") (f (quote ("macros"))) (d #t) (k 0)))) (h "1dd4y0lwjl4v8i8cx6qypc6k9873k06fpp21px8ngqir0cbi3njn")))

