(define-module (crates-io tf _p tf_playlist) #:use-module (crates-io))

(define-public crate-tf_playlist-0.1.1 (c (n "tf_playlist") (v "0.1.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tf_observer") (r "^0.1.0") (d #t) (k 0)))) (h "1hwg34h9jkhmdv70sz5h6bbfybgjq1k0id7vv1lngr70dj7sxgq0")))

(define-public crate-tf_playlist-0.1.2 (c (n "tf_playlist") (v "0.1.2") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tf_observer") (r "^0.1.0") (d #t) (k 0)))) (h "1zyfa9ibd06mkgsfsw6zr6jvjvbq7px5vsx7rdi3az5mpg0vcbbv")))

(define-public crate-tf_playlist-0.1.3 (c (n "tf_playlist") (v "0.1.3") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tf_observer") (r "^0.1.1") (d #t) (k 0)))) (h "090imfa4yzw6s7glc78mqgm43in12xpdzgwdd4nvxakr18mmdhca")))

(define-public crate-tf_playlist-0.1.4 (c (n "tf_playlist") (v "0.1.4") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tf_observer") (r "^0.1.3") (d #t) (k 0)))) (h "0mwsips15v49c9aw0f3hx5ganc52s6zq4pkzk8lxwcqiiifi40r5")))

