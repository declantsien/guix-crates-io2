(define-module (crates-io tf -b tf-bindgen-core) #:use-module (crates-io))

(define-public crate-tf-bindgen-core-0.1.0 (c (n "tf-bindgen-core") (v "0.1.0") (d (list (d (n "sha1") (r "^0.10.5") (d #t) (k 0)) (d (n "tf-bindgen-schema") (r "^0.1.0") (d #t) (k 0)))) (h "11hxyw81nl5x07z0qc813jinqx5d04v4f0wgbs808v6dpd1rfr55")))

