(define-module (crates-io tf -b tf-bindgen-schema) #:use-module (crates-io))

(define-public crate-tf-bindgen-schema-0.1.0 (c (n "tf-bindgen-schema") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 0)))) (h "14sqcr4cfvca5bgcy9y50j5kf5sfayj5mxd7zvw0ck3m1v2lxz2r")))

