(define-module (crates-io tf -b tf-bindgen-cli) #:use-module (crates-io))

(define-public crate-tf-bindgen-cli-0.1.0 (c (n "tf-bindgen-cli") (v "0.1.0") (d (list (d (n "serde_json") (r "^1.0.95") (d #t) (k 0)) (d (n "tf-bindgen-core") (r "^0.1.0") (d #t) (k 0)))) (h "1pagw75zkn41spw63xijaq8pwma38rcjajlw0amkbw4m35pf7gi8")))

