(define-module (crates-io tf -b tf-bindgen-codegen) #:use-module (crates-io))

(define-public crate-tf-bindgen-codegen-0.1.0 (c (n "tf-bindgen-codegen") (v "0.1.0") (d (list (d (n "darling") (r "^0.14.4") (d #t) (k 0)) (d (n "heck") (r "^0.4.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^1.0.91") (f (quote ("derive" "full"))) (d #t) (k 0)))) (h "1k43jh7c5bm9ya27nphbqffxmx2m6kw6d06h44jkb8nw9ksgg59k")))

