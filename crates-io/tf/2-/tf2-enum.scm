(define-module (crates-io tf #{2-}# tf2-enum) #:use-module (crates-io))

(define-public crate-tf2-enum-0.1.0 (c (n "tf2-enum") (v "0.1.0") (d (list (d (n "num_enum") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_repr") (r "^0.1") (d #t) (k 0)) (d (n "strum") (r "^0.21.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.21") (d #t) (k 0)))) (h "0c306975rcvy106la28da8ridy6cihlpnxb8rawgjgb80xdz3f6n")))

(define-public crate-tf2-enum-0.1.1 (c (n "tf2-enum") (v "0.1.1") (d (list (d (n "num_enum") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_repr") (r "^0.1") (d #t) (k 0)) (d (n "strum") (r "^0.21.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.21") (d #t) (k 0)))) (h "05ddvhpk5w3k5ygl69lsw046f4i7c068fv4fbbjr5p35d47yivaq")))

(define-public crate-tf2-enum-0.2.0 (c (n "tf2-enum") (v "0.2.0") (d (list (d (n "num_enum") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_repr") (r "^0.1") (d #t) (k 0)) (d (n "strum") (r "^0.21.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.21") (d #t) (k 0)))) (h "1nf5cq15dz68ax2fnbqp259i6s4l1wqdsycn9wzhwajr16ia99vb")))

(define-public crate-tf2-enum-0.3.0 (c (n "tf2-enum") (v "0.3.0") (d (list (d (n "num_enum") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_repr") (r "^0.1") (d #t) (k 0)) (d (n "strum") (r "^0.21.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.21") (d #t) (k 0)))) (h "0nbqicjig78ycc4ykpmh32cviya4m708hjppkihlvyr6cn0svw92")))

(define-public crate-tf2-enum-0.3.1 (c (n "tf2-enum") (v "0.3.1") (d (list (d (n "num_enum") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_repr") (r "^0.1") (d #t) (k 0)) (d (n "strum") (r "^0.21.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.21") (d #t) (k 0)))) (h "0368ssga8gfcgphllcfsw3qpl6z7300nnb1y6l63qi967ggqpd5n")))

(define-public crate-tf2-enum-0.4.0 (c (n "tf2-enum") (v "0.4.0") (d (list (d (n "num_enum") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_repr") (r "^0.1") (d #t) (k 0)) (d (n "strum") (r "^0.21.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.21") (d #t) (k 0)))) (h "1cgnl87rpmbkrl4pln6rwcvysbp0b7l2r3ymzr4kqsml175jxvpz")))

(define-public crate-tf2-enum-0.5.0 (c (n "tf2-enum") (v "0.5.0") (d (list (d (n "num_enum") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_repr") (r "^0.1") (d #t) (k 0)) (d (n "strum") (r "^0.21") (d #t) (k 0)) (d (n "strum_macros") (r "^0.21") (d #t) (k 0)))) (h "1s0s4gbc61cmhaj411bjb2d2ylv60z5607q263kjxssjn2923dx0")))

(define-public crate-tf2-enum-0.6.0 (c (n "tf2-enum") (v "0.6.0") (d (list (d (n "num_enum") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_repr") (r "^0.1") (d #t) (k 0)) (d (n "strum") (r "^0.21") (d #t) (k 0)) (d (n "strum_macros") (r "^0.21") (d #t) (k 0)))) (h "1b0i8pizpsawy9kvw0z0ip8s2vz8gqcihbmf9610gnfv8lsln9fi")))

(define-public crate-tf2-enum-0.6.1 (c (n "tf2-enum") (v "0.6.1") (d (list (d (n "num_enum") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_repr") (r "^0.1") (d #t) (k 0)) (d (n "strum") (r "^0.21") (d #t) (k 0)) (d (n "strum_macros") (r "^0.21") (d #t) (k 0)))) (h "1c9pmr8nvafpw54x0q0fh53sg8if08c5j6r7srvpqbbncsx8cax9")))

(define-public crate-tf2-enum-0.7.0 (c (n "tf2-enum") (v "0.7.0") (d (list (d (n "num_enum") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_repr") (r "^0.1") (d #t) (k 0)) (d (n "strum") (r "^0.21") (d #t) (k 0)) (d (n "strum_macros") (r "^0.21") (d #t) (k 0)))) (h "12q29f0c365c0k4nfi433wrmwiarkrphigzbq5mnqnqjzlz4f0s7")))

(define-public crate-tf2-enum-0.8.0 (c (n "tf2-enum") (v "0.8.0") (d (list (d (n "num_enum") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_repr") (r "^0.1") (d #t) (k 0)) (d (n "strum") (r "^0.21") (d #t) (k 0)) (d (n "strum_macros") (r "^0.21") (d #t) (k 0)))) (h "1v56bs3ykz7qr87z5lv8ikb1kaz704h45159vv71ybmf0plkhmp2")))

(define-public crate-tf2-enum-0.8.1 (c (n "tf2-enum") (v "0.8.1") (d (list (d (n "num_enum") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.93") (d #t) (k 2)) (d (n "serde_repr") (r "^0.1") (d #t) (k 0)) (d (n "strum") (r "^0.21") (d #t) (k 0)) (d (n "strum_macros") (r "^0.21") (d #t) (k 0)))) (h "0b2xdb2si41b9pyzfl7q285w4mkkr8apyp4pb0z2cgwys33yjjh1")))

(define-public crate-tf2-enum-0.8.2 (c (n "tf2-enum") (v "0.8.2") (d (list (d (n "num_enum") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.93") (d #t) (k 2)) (d (n "serde_repr") (r "^0.1") (d #t) (k 0)) (d (n "strum") (r "^0.21") (d #t) (k 0)) (d (n "strum_macros") (r "^0.21") (d #t) (k 0)))) (h "0yjq45ksf1f1y80r2is4rfgnpi6a0zavxs0f0rw8vmgzxjfm4kx9")))

(define-public crate-tf2-enum-0.8.3 (c (n "tf2-enum") (v "0.8.3") (d (list (d (n "num_enum") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.93") (d #t) (k 2)) (d (n "serde_repr") (r "^0.1") (d #t) (k 0)) (d (n "strum") (r "^0.21") (d #t) (k 0)) (d (n "strum_macros") (r "^0.21") (d #t) (k 0)))) (h "1a0vad3fwyzjb0v2j6g2rsgjz9665fcz8ajg5vhcr5dg2rsgjlqh")))

(define-public crate-tf2-enum-0.9.0 (c (n "tf2-enum") (v "0.9.0") (d (list (d (n "num_enum") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.93") (d #t) (k 2)) (d (n "serde_repr") (r "^0.1") (d #t) (k 0)) (d (n "strum") (r "^0.21") (d #t) (k 0)) (d (n "strum_macros") (r "^0.21") (d #t) (k 0)))) (h "18q09gx0j9zx2hqn1nmfjv1cnda65a5ir4k07mq4c69an3hi1bab")))

(define-public crate-tf2-enum-0.9.1 (c (n "tf2-enum") (v "0.9.1") (d (list (d (n "num_enum") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.93") (d #t) (k 2)) (d (n "serde_repr") (r "^0.1") (d #t) (k 0)) (d (n "strum") (r "^0.21") (d #t) (k 0)) (d (n "strum_macros") (r "^0.21") (d #t) (k 0)))) (h "0ddjaqky49vkc5ph0ya78s21q509rxsp8a0fajix4xny1s1hia6i")))

(define-public crate-tf2-enum-0.9.2 (c (n "tf2-enum") (v "0.9.2") (d (list (d (n "num_enum") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.93") (d #t) (k 2)) (d (n "serde_repr") (r "^0.1") (d #t) (k 0)) (d (n "strum") (r "^0.26") (d #t) (k 0)) (d (n "strum_macros") (r "^0.26") (d #t) (k 0)))) (h "0fvhlvbjalmjh08awbhprp8aw216v6bl3hqsipn81i7bzbry7qr8")))

(define-public crate-tf2-enum-0.9.3 (c (n "tf2-enum") (v "0.9.3") (d (list (d (n "num_enum") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.93") (d #t) (k 2)) (d (n "serde_repr") (r "^0.1") (d #t) (k 0)) (d (n "strum") (r "^0.26") (d #t) (k 0)) (d (n "strum_macros") (r "^0.26") (d #t) (k 0)))) (h "0a8ggr7g2zna2fv15ywp0qkk6zr1jrvrjby9fj6kagmx446bs5gc")))

