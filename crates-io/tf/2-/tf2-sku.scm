(define-module (crates-io tf #{2-}# tf2-sku) #:use-module (crates-io))

(define-public crate-tf2-sku-0.1.0 (c (n "tf2-sku") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "tf2-enum") (r "^0") (d #t) (k 0)))) (h "0qma219a5kyq35dx41l4sk20dhi0h8zlqw1178vsxn9fxx0ysxa1")))

(define-public crate-tf2-sku-0.2.0 (c (n "tf2-sku") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "tf2-enum") (r "^0.6") (d #t) (k 0)))) (h "051d3725ibw1gpiqvfgpvd6blcxxvaw7043zzv4fh8dv31ndq7a5")))

(define-public crate-tf2-sku-0.3.0 (c (n "tf2-sku") (v "0.3.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "tf2-enum") (r "^0.8") (d #t) (k 0)))) (h "0ydjdjfcmidlsrbxf3hidcfggs8z5r7zd8qp4r0xx1yy0kpamfib")))

(define-public crate-tf2-sku-0.4.0 (c (n "tf2-sku") (v "0.4.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "tf2-enum") (r "^0.9.0") (d #t) (k 0)))) (h "1m9hn214bdp6ykmbcm6f9xc1q7xslsjjkwnyimy3pdf967dsz4fg")))

(define-public crate-tf2-sku-0.4.1 (c (n "tf2-sku") (v "0.4.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "tf2-enum") (r "^0.9.0") (d #t) (k 0)))) (h "00fq0jc6ayg6lp4wamc9nrsar3d6dhkpmlf8592dk6swn2qidq01")))

(define-public crate-tf2-sku-0.4.2 (c (n "tf2-sku") (v "0.4.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "tf2-enum") (r "^0.9.0") (d #t) (k 0)))) (h "144mj4dq92fy6f6agkbnwx164xcc268qkphvx0qiim1kg0sfmk77")))

