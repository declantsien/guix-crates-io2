(define-module (crates-io tf #{2-}# tf2-types) #:use-module (crates-io))

(define-public crate-tf2-types-0.1.0 (c (n "tf2-types") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.132") (o #t) (d #t) (k 0)))) (h "0dxjn3726bg4chczkgry94ww8fkb62vsyv1lyhhfcgqf0pi1ra4l") (f (quote (("events")))) (s 2) (e (quote (("cevents" "dep:libc"))))))

(define-public crate-tf2-types-0.1.1 (c (n "tf2-types") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.132") (o #t) (d #t) (k 0)))) (h "0vaz411q3cpl9br31s1xh9fw25b8szl7yxxicaqza1vsfpznpf2m") (f (quote (("events")))) (s 2) (e (quote (("cevents" "dep:libc"))))))

(define-public crate-tf2-types-0.1.2 (c (n "tf2-types") (v "0.1.2") (d (list (d (n "libc") (r "^0.2.132") (o #t) (d #t) (k 0)))) (h "0716sp4z569fb8nbx09413pqlpmvynrp8hwksqql7ai8w4kxx0ss") (f (quote (("events")))) (s 2) (e (quote (("cevents" "dep:libc"))))))

(define-public crate-tf2-types-0.1.3 (c (n "tf2-types") (v "0.1.3") (d (list (d (n "libc") (r "^0.2.132") (o #t) (d #t) (k 0)))) (h "0icwp3r0hq12mgliajfqy07j8pp2ln9226lp3nl445g1ndmhaafr") (f (quote (("events")))) (s 2) (e (quote (("cevents" "dep:libc"))))))

(define-public crate-tf2-types-0.1.4 (c (n "tf2-types") (v "0.1.4") (d (list (d (n "libc") (r "^0.2.132") (o #t) (d #t) (k 0)))) (h "145pwjkdlviavc990s7rsmycvnw400f26qs3647xlv5appfip0xj") (f (quote (("events")))) (s 2) (e (quote (("cevents" "dep:libc"))))))

(define-public crate-tf2-types-0.1.5 (c (n "tf2-types") (v "0.1.5") (d (list (d (n "libc") (r "^0.2.132") (o #t) (d #t) (k 0)))) (h "0c1nl729kl7zb5zd6hp2f1bkppi8qy4f3zs0ak2vr1fid67zc5d1") (f (quote (("events")))) (s 2) (e (quote (("cevents" "dep:libc"))))))

(define-public crate-tf2-types-0.1.6 (c (n "tf2-types") (v "0.1.6") (d (list (d (n "libc") (r "^0.2.132") (o #t) (d #t) (k 0)))) (h "0m7ddfcjwwpwvvjavxv8z1466w3kgnrr051n80q27b5qq89rn2gq") (f (quote (("events")))) (s 2) (e (quote (("cevents" "dep:libc"))))))

(define-public crate-tf2-types-0.1.7 (c (n "tf2-types") (v "0.1.7") (d (list (d (n "libc") (r "^0.2.132") (o #t) (d #t) (k 0)))) (h "0zy77b3aydg0h4fyjxjph4v4a6dcxdiisv6427f3nqwzmqvinypm") (f (quote (("events")))) (s 2) (e (quote (("cevents" "dep:libc"))))))

