(define-module (crates-io tf #{2-}# tf2-price) #:use-module (crates-io))

(define-public crate-tf2-price-0.1.0 (c (n "tf2-price") (v "0.1.0") (d (list (d (n "lazy-regex") (r "^2.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1mgrs2wimfg2nn8sz0383bwpd8p2szik0rfr78mviyrp3cygs44f")))

(define-public crate-tf2-price-0.2.0 (c (n "tf2-price") (v "0.2.0") (d (list (d (n "assert-json-diff") (r "^2.0.1") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "06m1w7nbqygja5h0b5wk6xxqzbsvrfwcg3q3dvm5yvvf91igblmy")))

(define-public crate-tf2-price-0.3.0 (c (n "tf2-price") (v "0.3.0") (d (list (d (n "assert-json-diff") (r "^2.0.1") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0lms78w6b8j4hzh4nznhhimvsxlybzfkg3aj9p353ghny633y135")))

(define-public crate-tf2-price-0.4.0 (c (n "tf2-price") (v "0.4.0") (d (list (d (n "assert-json-diff") (r "^2.0.1") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "05jq1y97hypxmb1lj93hac5zgyz648n422495i5nsfpdprw165jl")))

(define-public crate-tf2-price-0.5.0 (c (n "tf2-price") (v "0.5.0") (d (list (d (n "assert-json-diff") (r "^2.0.1") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1c2ilgm9cwpdblqmdj3q9fnzswx7aqvywxd37gb12gin64ag8krz")))

(define-public crate-tf2-price-0.5.1 (c (n "tf2-price") (v "0.5.1") (d (list (d (n "assert-json-diff") (r "^2.0.1") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "impl_ops") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1nc03zhc2dfmalr3wz7jyy1klfg02f098f8ay465nfb7xiq0qgw4")))

(define-public crate-tf2-price-0.5.2 (c (n "tf2-price") (v "0.5.2") (d (list (d (n "assert-json-diff") (r "^2.0.1") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "impl_ops") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1akij6hgmcmyz87wx95zympg877cjlsjlgrbjjbirs77qfyw4p2q")))

(define-public crate-tf2-price-0.5.3 (c (n "tf2-price") (v "0.5.3") (d (list (d (n "assert-json-diff") (r "^2.0.1") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "impl_ops") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1law9drvg29x3dv8xqjynbbam59hs5fd3ifrmzjrc5n5h6bv6ac9")))

(define-public crate-tf2-price-0.6.0 (c (n "tf2-price") (v "0.6.0") (d (list (d (n "assert-json-diff") (r "^2.0.1") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "impl_ops") (r "^0.1.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "04363x01y4jgs7s5bfqyk1n93zskf1sg4xrzhi4v6mcc0ds3z2g7")))

(define-public crate-tf2-price-0.7.0 (c (n "tf2-price") (v "0.7.0") (d (list (d (n "assert-json-diff") (r "^2.0.1") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "impl_ops") (r "^0.1.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0plnagbl4q9n4wyqgvcjks4lwdljb27ndlyl4kk7j3vj0wal2hll")))

(define-public crate-tf2-price-0.7.1 (c (n "tf2-price") (v "0.7.1") (d (list (d (n "assert-json-diff") (r "^2.0.1") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "impl_ops") (r "^0.1.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0vy9krv4iyl11vldxj8ilcm38nkf10i478k6fzb7k5602wjlgi31")))

(define-public crate-tf2-price-0.7.2 (c (n "tf2-price") (v "0.7.2") (d (list (d (n "assert-json-diff") (r "^2.0.1") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "impl_ops") (r "^0.1.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0nyjagl0prm52gag7vr3r0qbs9blj1vbnhw72gmz43xywaspx68r")))

(define-public crate-tf2-price-0.8.0 (c (n "tf2-price") (v "0.8.0") (d (list (d (n "assert-json-diff") (r "^2.0.1") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "impl_ops") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "10975mkmzc3g5dbwbxbl8cygvkwxyjh9dg7nck9fvi7dddn7h6c0")))

(define-public crate-tf2-price-0.8.1 (c (n "tf2-price") (v "0.8.1") (d (list (d (n "assert-json-diff") (r "^2.0.1") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "impl_ops") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0mqrcqz6iz8874labpbc3xd8j1bp93z9jv06cjg0lsbvasj1w14f")))

(define-public crate-tf2-price-0.9.0 (c (n "tf2-price") (v "0.9.0") (d (list (d (n "assert-json-diff") (r "^2.0.1") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "impl_ops") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1cv95k116fc1pp1hd9xx3q2pqh3am2kxk1nxzgvl7ng6ifnpljhk")))

(define-public crate-tf2-price-0.10.0 (c (n "tf2-price") (v "0.10.0") (d (list (d (n "assert-json-diff") (r "^2.0.1") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "impl_ops") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0q6zsxfk84767dxmf5g2zhg3i01ksnjkn7bgsvjv1vwkqpmrjlrd")))

(define-public crate-tf2-price-0.11.0 (c (n "tf2-price") (v "0.11.0") (d (list (d (n "assert-json-diff") (r "^2.0.1") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "impl_ops") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "1r3wygp05qcf76c8s2ybf35ijpy6dkn1d037vavkvw5pq22mj7xs")))

(define-public crate-tf2-price-0.12.0 (c (n "tf2-price") (v "0.12.0") (d (list (d (n "assert-json-diff") (r "^2.0.1") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "impl_ops") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "1v3djd560rwqz16jp4f959q8ib9r0bcldnij8wlkkia1ygqy4nj6") (f (quote (("b32"))))))

(define-public crate-tf2-price-0.13.0 (c (n "tf2-price") (v "0.13.0") (d (list (d (n "assert-json-diff") (r "^2.0.1") (d #t) (k 2)) (d (n "auto_ops") (r "=0.3.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0v93w8ijdkqbq6r5n2cyj11cvnlca6j33rjjl4xdmq4mfcwjs60i") (f (quote (("b32")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-tf2-price-0.13.1 (c (n "tf2-price") (v "0.13.1") (d (list (d (n "assert-json-diff") (r "^2.0.1") (d #t) (k 2)) (d (n "auto_ops") (r "=0.3.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "06fzpn4g0jhm1nhzlm4z6knarxq2l453l51lj7ak3b9diwi4ymbj") (f (quote (("b32")))) (s 2) (e (quote (("serde" "dep:serde"))))))

