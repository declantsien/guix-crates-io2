(define-module (crates-io tf st tfstarter) #:use-module (crates-io))

(define-public crate-tfstarter-0.1.0 (c (n "tfstarter") (v "0.1.0") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "csv") (r "^1.3.0") (d #t) (k 0)))) (h "1w8ycqx6fl8pgyy8awlbzvldvzmyvrpww4saw5h048anflfb3nfa")))

(define-public crate-tfstarter-0.2.0 (c (n "tfstarter") (v "0.2.0") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "csv") (r "^1.3.0") (d #t) (k 0)))) (h "1a8naxkgzd62d5k5pzq18gby7iwx7wch39vdw5qqbpzrprlrlpxc")))

(define-public crate-tfstarter-0.3.0 (c (n "tfstarter") (v "0.3.0") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "csv") (r "^1.3.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "036bgb8qwaaailqf9i52amjll52chab56b5djpjdpj2x2vdkfny6")))

(define-public crate-tfstarter-0.3.1 (c (n "tfstarter") (v "0.3.1") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "csv") (r "^1.3.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1ciq4zvp69q72jxgdw6k5lrp7n865039nvjq6awz98j7nlqaydag")))

(define-public crate-tfstarter-0.4.0 (c (n "tfstarter") (v "0.4.0") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.9.0") (d #t) (k 0)) (d (n "zip") (r "^0.6.6") (d #t) (k 0)))) (h "0lsv6k2il3y3vdql9x289iadl1z8qhzkxrhj2qzgzw6yhk42v3z4")))

(define-public crate-tfstarter-0.4.1 (c (n "tfstarter") (v "0.4.1") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.9.0") (d #t) (k 0)) (d (n "zip") (r "^0.6.6") (d #t) (k 0)))) (h "1dcwmm75dzqi7fhfvck10ihq505czlbb92xzc53pmhrmrpyda30k")))

(define-public crate-tfstarter-0.5.1 (c (n "tfstarter") (v "0.5.1") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.9.0") (d #t) (k 0)) (d (n "zip") (r "^0.6.6") (d #t) (k 0)))) (h "1g81qp7sl1d05skszqqj4p31lcigkg5mdngrma4i6xhmd3vd3ck8")))

