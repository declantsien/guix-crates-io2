(define-module (crates-io tf mt tfmt) #:use-module (crates-io))

(define-public crate-tfmt-0.4.0 (c (n "tfmt") (v "0.4.0") (d (list (d (n "heapless") (r "^0.8.0") (d #t) (k 0)) (d (n "tfmt-macros") (r "^0.4.0") (d #t) (k 0)))) (h "1as4n26jpbswf06ckd5ypv2cxyk81gd4m2dd0vazz07jmkbd0879") (f (quote (("std"))))))

