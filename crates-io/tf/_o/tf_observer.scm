(define-module (crates-io tf _o tf_observer) #:use-module (crates-io))

(define-public crate-tf_observer-0.1.0 (c (n "tf_observer") (v "0.1.0") (d (list (d (n "mockall") (r "^0.10.2") (d #t) (k 2)) (d (n "mockito") (r "^0.30.0") (d #t) (k 2)))) (h "18z98f33499jbbn3knikzvmi2hkiaa68spaqmqnyiz0s5mhnr6m7")))

(define-public crate-tf_observer-0.1.1 (c (n "tf_observer") (v "0.1.1") (d (list (d (n "mockall") (r "^0.10.2") (d #t) (k 2)) (d (n "mockito") (r "^0.30.0") (d #t) (k 2)))) (h "03v25a1zgij59w7nkk5djyxhvg5l4lvn12bwh49xfpyda60my4v9")))

(define-public crate-tf_observer-0.1.2 (c (n "tf_observer") (v "0.1.2") (d (list (d (n "mockall") (r "^0.11.0") (d #t) (k 2)) (d (n "mockito") (r "^0.31.0") (d #t) (k 2)))) (h "1dx2wajl06w9pchzd11r7l5794ym6b0cir9zwc12c42d90cavdnl")))

(define-public crate-tf_observer-0.1.3 (c (n "tf_observer") (v "0.1.3") (d (list (d (n "mockall") (r "^0.11.4") (d #t) (k 2)) (d (n "mockito") (r "^0.31.1") (d #t) (k 2)))) (h "0hpbfx0dd4m2dc02d3yipfl4zn9nfy4kkfvpz3pipggc5pzh8hhw")))

