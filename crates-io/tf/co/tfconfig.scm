(define-module (crates-io tf co tfconfig) #:use-module (crates-io))

(define-public crate-tfconfig-0.1.0 (c (n "tfconfig") (v "0.1.0") (d (list (d (n "hcl-rs") (r "^0.14.4") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.41") (d #t) (k 0)))) (h "1l1s8qpcybsnlld8nmjys3nbgajz0af8q9sn8w3046sc03j5hm80")))

(define-public crate-tfconfig-0.2.0 (c (n "tfconfig") (v "0.2.0") (d (list (d (n "hcl-rs") (r "^0.16.2") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.41") (d #t) (k 0)))) (h "1j7mxjj2j33ga5py0243b5klawgd4cwr31pbwz0w84y22xrgmcy9")))

(define-public crate-tfconfig-0.2.1 (c (n "tfconfig") (v "0.2.1") (d (list (d (n "hcl-rs") (r "^0.16.2") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.41") (d #t) (k 0)))) (h "0xi5x2gr78g2fbdjss27wq3fmf0jzknnhks81ic8n6390nn2q05p")))

(define-public crate-tfconfig-0.2.2 (c (n "tfconfig") (v "0.2.2") (d (list (d (n "hcl-rs") (r "^0.16.2") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.41") (d #t) (k 0)))) (h "0jrf7schf1lv410kj7b5gb592z70y08lggxxpqwbh8w4shmskrpc")))

(define-public crate-tfconfig-0.2.3 (c (n "tfconfig") (v "0.2.3") (d (list (d (n "hcl-rs") (r "^0.18.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.41") (d #t) (k 0)))) (h "091x1flqcc7aqm097n0c5cavc333rx7n7f1si5iq0hkpbp89pvk4")))

