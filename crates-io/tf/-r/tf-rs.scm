(define-module (crates-io tf -r tf-rs) #:use-module (crates-io))

(define-public crate-tf-rs-0.0.1 (c (n "tf-rs") (v "0.0.1") (d (list (d (n "error-chain") (r "^0.11.0") (d #t) (k 0)) (d (n "num-complex") (r "^0.1.40") (k 0)) (d (n "tensorflow") (r "^0.6.0") (d #t) (k 0)) (d (n "uuid") (r "^0.4") (f (quote ("v4"))) (d #t) (k 0)))) (h "0kvcfv4p71vfgzqamgl9safg19i79833ihngh34y1q2izz0ak5m0")))

(define-public crate-tf-rs-0.0.2 (c (n "tf-rs") (v "0.0.2") (d (list (d (n "error-chain") (r "^0.11.0") (d #t) (k 0)) (d (n "num-complex") (r "^0.1.40") (k 0)) (d (n "tensorflow") (r "^0.7.0") (d #t) (k 0)) (d (n "uuid") (r "^0.4") (f (quote ("v4"))) (d #t) (k 0)))) (h "1iinpq1pia9v3qgss3rqvljazy5ccyfcr09nkqqpy14s2m4bxwac")))

(define-public crate-tf-rs-0.0.4 (c (n "tf-rs") (v "0.0.4") (d (list (d (n "error-chain") (r "^0.12") (d #t) (k 0)) (d (n "num-complex") (r "^0.2.4") (k 0)) (d (n "tensorflow") (r "^0.14.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "0d9zndp8yrfz0h2l9g842wk4n24v435504df3mnpgangc9bc4j0l")))

(define-public crate-tf-rs-0.0.5 (c (n "tf-rs") (v "0.0.5") (d (list (d (n "error-chain") (r "^0.12") (d #t) (k 0)) (d (n "num-complex") (r "^0.2.4") (k 0)) (d (n "tensorflow") (r "^0.14.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "0820d2d4zsrylca27f76n4f5yq2b1xhdks8py7yd6zjw4nrzpgag")))

