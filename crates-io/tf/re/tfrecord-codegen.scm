(define-module (crates-io tf re tfrecord-codegen) #:use-module (crates-io))

(define-public crate-tfrecord-codegen-0.1.0 (c (n "tfrecord-codegen") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "flate2") (r "^1.0.27") (d #t) (k 0)) (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "prost-build") (r "^0.12.0") (d #t) (k 0)) (d (n "tar") (r "^0.4.40") (d #t) (k 0)) (d (n "ureq") (r "^2.7.1") (d #t) (k 0)))) (h "1khk5phmrkpl37wivpjnhilbaq6vbwbvzagkglc78y1447sj4q9m")))

