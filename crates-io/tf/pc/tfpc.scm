(define-module (crates-io tf pc tfpc) #:use-module (crates-io))

(define-public crate-tfpc-0.1.0 (c (n "tfpc") (v "0.1.0") (h "16765cn6jvdq69fbbyyzabxrid3bsi8wmgai3lgmvy43ll94v5id")))

(define-public crate-tfpc-0.1.1 (c (n "tfpc") (v "0.1.1") (h "1rq9vc7chip5iqyy5armk04m9kwdgm13i0bspwvnpwmwaizdzh2q")))

(define-public crate-tfpc-0.1.2 (c (n "tfpc") (v "0.1.2") (h "14aa0v79llcbwm1mh07cy53safa34hvq775ab7gpiqjd48nkb6v0")))

(define-public crate-tfpc-0.1.3 (c (n "tfpc") (v "0.1.3") (h "15vinpg050fagidsc1imzs0cwlsrk54xawalkkiwn9mdq07c199f")))

(define-public crate-tfpc-0.1.4 (c (n "tfpc") (v "0.1.4") (h "0d6q9fc3ikk1prdydhl44l4xzn7kg5gsm472jjkzh9yrswrac31z")))

