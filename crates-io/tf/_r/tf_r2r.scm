(define-module (crates-io tf _r tf_r2r) #:use-module (crates-io))

(define-public crate-tf_r2r-0.1.0 (c (n "tf_r2r") (v "0.1.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "nalgebra") (r "^0.30") (d #t) (k 0)) (d (n "r2r") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "10fp4gkjsyhdsv7ks3f5qlb0pp8nnxxly2hw98qgh6g9phz2ri4m") (f (quote (("ros2" "r2r"))))))

(define-public crate-tf_r2r-0.2.0 (c (n "tf_r2r") (v "0.2.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "nalgebra") (r "^0.30") (d #t) (k 0)) (d (n "r2r") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0vsi2d47dz4237qn34a97gz10rlwxx24irkayrb0m03l4xnbyaf7") (f (quote (("ros2" "r2r"))))))

