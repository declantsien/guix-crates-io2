(define-module (crates-io tf _r tf_rosrust) #:use-module (crates-io))

(define-public crate-tf_rosrust-0.0.1 (c (n "tf_rosrust") (v "0.0.1") (d (list (d (n "nalgebra") (r "^0.26") (d #t) (k 0)) (d (n "rosrust") (r "^0.9") (d #t) (k 0)))) (h "1y38dmfd69qkp75dpnqywd0dgm777765kfkibvwp14ip654j6kzv")))

(define-public crate-tf_rosrust-0.0.2 (c (n "tf_rosrust") (v "0.0.2") (d (list (d (n "nalgebra") (r "^0.26") (d #t) (k 0)) (d (n "rosrust") (r "^0.9") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1g9hwql1hrh9dx17xb9hx9wi5rsq7bw5cxamcwz62qxdm40kjwin")))

(define-public crate-tf_rosrust-0.0.3 (c (n "tf_rosrust") (v "0.0.3") (d (list (d (n "nalgebra") (r "^0.26") (d #t) (k 0)) (d (n "rosrust") (r "^0.9") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1nss8j1q3y6a0qm4mw2nv6nnngwqm0mgddibrifp55gj5v24b27j")))

(define-public crate-tf_rosrust-0.0.4 (c (n "tf_rosrust") (v "0.0.4") (d (list (d (n "nalgebra") (r "^0.26") (d #t) (k 0)) (d (n "rosrust") (r "^0.9") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "06zd7im6fh0m56cxnyr6n9zwvsmsyndsw6ji9f7siy15qjnvrpxl")))

(define-public crate-tf_rosrust-0.0.5 (c (n "tf_rosrust") (v "0.0.5") (d (list (d (n "nalgebra") (r "^0.29") (d #t) (k 0)) (d (n "rosrust") (r "^0.9") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1p3brcqgbmk3c53fbxkiqlg3dy7xqi1vmlcsa5yviv3jrzbbwdn7")))

(define-public crate-tf_rosrust-0.1.0 (c (n "tf_rosrust") (v "0.1.0") (d (list (d (n "nalgebra") (r "^0.29") (d #t) (k 0)) (d (n "rosrust") (r "^0.9") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1b7q6x2vi40i1lwpmxb5hcg29cy677fy026lw9n7ph9cmv8y1acy")))

