(define-module (crates-io tf do tfdoc) #:use-module (crates-io))

(define-public crate-tfdoc-0.1.4 (c (n "tfdoc") (v "0.1.4") (h "03wvzdlrqrb9rqk0zkwxz87lr1smr2blb38nnw1gh2kja2zrglys")))

(define-public crate-tfdoc-0.1.5 (c (n "tfdoc") (v "0.1.5") (h "1wdi3pic116yi2kyl4llslbl3i7wyv7il3a59v1h385cw88aac22")))

