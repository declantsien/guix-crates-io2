(define-module (crates-io tf -s tf-semver) #:use-module (crates-io))

(define-public crate-tf-semver-1.0.17 (c (n "tf-semver") (v "1.0.17") (d (list (d (n "serde") (r "^1.0") (o #t) (k 0)))) (h "1qwf2kly5rq6qdnm5pqk7j5y1w0cmq61pz7xmwg5jza88988m032") (f (quote (("std") ("default" "std")))) (r "1.31")))

