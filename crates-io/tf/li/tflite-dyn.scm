(define-module (crates-io tf li tflite-dyn) #:use-module (crates-io))

(define-public crate-tflite-dyn-0.1.0 (c (n "tflite-dyn") (v "0.1.0") (d (list (d (n "libloading") (r "^0.7.2") (d #t) (k 0)) (d (n "semver") (r "^1.0.4") (d #t) (k 0)))) (h "0zgalg5g1x4pq6xb2s64r4gbihyl6l8k0i8j4npq8m24hy0567vx")))

(define-public crate-tflite-dyn-0.1.1 (c (n "tflite-dyn") (v "0.1.1") (d (list (d (n "libloading") (r "^0.7.2") (d #t) (k 0)) (d (n "semver") (r "^1.0.4") (d #t) (k 0)))) (h "1km3bz4lh1znix31v1r8pimyr6f6z346fxn5g655rmhq5mlpmacs")))

