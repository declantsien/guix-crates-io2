(define-module (crates-io tf li tflitec) #:use-module (crates-io))

(define-public crate-tflitec-0.3.0 (c (n "tflitec") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.59.1") (d #t) (k 1)))) (h "0nr2hkcfy3rqqqz488f8sfc74qb94f4i6jn5pi3cmlkf6a0j8d6g") (f (quote (("xnnpack_qu8" "xnnpack") ("xnnpack_qs8" "xnnpack") ("xnnpack"))))))

(define-public crate-tflitec-0.3.1 (c (n "tflitec") (v "0.3.1") (d (list (d (n "bindgen") (r "^0.59.1") (d #t) (k 1)))) (h "0dq95ggv43a8z6rryy43zgfan2w31calc3yx0wx51zbhmyjvpgf3") (f (quote (("xnnpack_qu8" "xnnpack") ("xnnpack_qs8" "xnnpack") ("xnnpack"))))))

(define-public crate-tflitec-0.3.2 (c (n "tflitec") (v "0.3.2") (d (list (d (n "bindgen") (r "^0.59.1") (d #t) (k 1)))) (h "1i0sa1g8vig7cxj5f8rfnff78mjqxmm15k9l5hcb41mxlh9kxi4a") (f (quote (("xnnpack_qu8" "xnnpack") ("xnnpack_qs8" "xnnpack") ("xnnpack"))))))

(define-public crate-tflitec-0.3.3 (c (n "tflitec") (v "0.3.3") (d (list (d (n "bindgen") (r "^0.59.1") (d #t) (k 1)))) (h "0syhhm441zlkvxdn3ygpznbfvkcfvmakj08bqlwm6qsiyrgwk5fd") (f (quote (("xnnpack_qu8" "xnnpack") ("xnnpack_qs8" "xnnpack") ("xnnpack"))))))

(define-public crate-tflitec-0.4.0 (c (n "tflitec") (v "0.4.0") (d (list (d (n "bindgen") (r "^0.59.1") (d #t) (k 1)))) (h "1zwzyijwmbsigyaj6vdi7v1f5h0kyvw1q5awssqk4fg9gw2smq0g") (f (quote (("xnnpack_qu8" "xnnpack") ("xnnpack_qs8" "xnnpack") ("xnnpack"))))))

(define-public crate-tflitec-0.4.1 (c (n "tflitec") (v "0.4.1") (d (list (d (n "bindgen") (r "^0.59.1") (d #t) (k 1)))) (h "0yz4a8hi2ijfaiwfmshslhgm7akyy499sqxmj2gf5jwg63wdwryn") (f (quote (("xnnpack_qu8" "xnnpack") ("xnnpack_qs8" "xnnpack") ("xnnpack"))))))

(define-public crate-tflitec-0.5.0-rc.1 (c (n "tflitec") (v "0.5.0-rc.1") (d (list (d (n "bindgen") (r "^0.59.1") (d #t) (k 1)) (d (n "curl") (r "^0.4.43") (d #t) (k 1)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 1)))) (h "14f2yn1bam52k9mvhy2vwpmqaxd6ag72v0dkdn541w6ixfn4gcaa") (f (quote (("xnnpack_qu8" "xnnpack") ("xnnpack_qs8" "xnnpack") ("xnnpack"))))))

(define-public crate-tflitec-0.5.0 (c (n "tflitec") (v "0.5.0") (d (list (d (n "bindgen") (r "^0.59.1") (d #t) (k 1)) (d (n "curl") (r "^0.4.43") (d #t) (k 1)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 1)))) (h "10lg95k8bh8zlizmddvbv1ly5xy85xb6v6qdgyq97rxv23wmhx9h") (f (quote (("xnnpack_qu8" "xnnpack") ("xnnpack_qs8" "xnnpack") ("xnnpack"))))))

(define-public crate-tflitec-0.5.1 (c (n "tflitec") (v "0.5.1") (d (list (d (n "bindgen") (r "^0.59.1") (d #t) (k 1)) (d (n "curl") (r "^0.4.43") (d #t) (k 1)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 1)))) (h "070pyg5wa4a9apxd4wcvqpz1kavf5fzjrzppwsdx2lkci6x2adc3") (f (quote (("xnnpack_qu8" "xnnpack") ("xnnpack_qs8" "xnnpack") ("xnnpack"))))))

(define-public crate-tflitec-0.5.2 (c (n "tflitec") (v "0.5.2") (d (list (d (n "bindgen") (r "^0.59.1") (d #t) (k 1)) (d (n "curl") (r "^0.4.43") (d #t) (k 1)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 1)))) (h "0zd2vj1rbrmxf7p101apq69v8if8p3k9k8hzb584h6vksmzdz4b0") (f (quote (("xnnpack_qu8" "xnnpack") ("xnnpack_qs8" "xnnpack") ("xnnpack"))))))

(define-public crate-tflitec-0.6.0 (c (n "tflitec") (v "0.6.0") (d (list (d (n "bindgen") (r "^0.65") (d #t) (k 1)) (d (n "curl") (r "^0.4") (d #t) (k 1)) (d (n "fs_extra") (r "^1.3") (d #t) (k 1)))) (h "0rpv2r7gcfn8xw9igr6h52iydi4rl1psv6pwxf38ifrpp6n06swx") (f (quote (("xnnpack_qu8" "xnnpack") ("xnnpack_qs8" "xnnpack") ("xnnpack"))))))

