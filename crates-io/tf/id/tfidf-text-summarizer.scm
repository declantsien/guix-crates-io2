(define-module (crates-io tf id tfidf-text-summarizer) #:use-module (crates-io))

(define-public crate-tfidf-text-summarizer-0.0.1 (c (n "tfidf-text-summarizer") (v "0.0.1") (d (list (d (n "punkt") (r "^1.0.5") (d #t) (k 0)))) (h "008b1gjwpc27qgmghpr9b8wgp0wdab1qavhd7bj246rdqv3gwmmj")))

(define-public crate-tfidf-text-summarizer-0.0.2 (c (n "tfidf-text-summarizer") (v "0.0.2") (d (list (d (n "jni") (r "^0.21.1") (o #t) (d #t) (k 0)) (d (n "punkt") (r "^1.0.5") (d #t) (k 0)) (d (n "rayon") (r "^1.8") (d #t) (k 0)))) (h "0vl2lprsc1d67dvxk2g4r7ly9wk9d35n56i0bd3idd9qiklcx0mx") (s 2) (e (quote (("android" "dep:jni"))))))

(define-public crate-tfidf-text-summarizer-0.0.3 (c (n "tfidf-text-summarizer") (v "0.0.3") (d (list (d (n "jni") (r "^0.21.1") (o #t) (d #t) (k 0)) (d (n "punkt") (r "^1.0.5") (d #t) (k 0)) (d (n "rayon") (r "^1.8") (d #t) (k 0)))) (h "14z3x1icj3dcmzvc53c5zz5z89axy9wsbs5ycg9wz0c1ph612ai6") (s 2) (e (quote (("android" "dep:jni"))))))

