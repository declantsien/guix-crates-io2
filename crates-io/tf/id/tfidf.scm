(define-module (crates-io tf id tfidf) #:use-module (crates-io))

(define-public crate-tfidf-0.1.0 (c (n "tfidf") (v "0.1.0") (h "0nbh3115p8z60vkyikh5l97bg5wb75536qxgi53142m5001syypj")))

(define-public crate-tfidf-0.2.0 (c (n "tfidf") (v "0.2.0") (h "0n5mphv3w8dlhw8hbycfczfjydd0lvwldfix1684i4cix8yk6z2k")))

(define-public crate-tfidf-0.3.0 (c (n "tfidf") (v "0.3.0") (h "0k7ik9x8m92yg4ddb4rg6r4sksc4hhfz2c17ckbkka43cl54gybb")))

