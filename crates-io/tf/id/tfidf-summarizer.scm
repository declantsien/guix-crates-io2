(define-module (crates-io tf id tfidf-summarizer) #:use-module (crates-io))

(define-public crate-tfidf-summarizer-2.0.0 (c (n "tfidf-summarizer") (v "2.0.0") (d (list (d (n "unicode-segmentation") (r "^1.8.0") (d #t) (k 0)))) (h "04a05yibgdx5bxfkiydlmnhqh1r3vlgjn854xhqbyxfj5n9sd880")))

