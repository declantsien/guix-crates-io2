(define-module (crates-io tf mi tfmicro) #:use-module (crates-io))

(define-public crate-tfmicro-0.0.0 (c (n "tfmicro") (v "0.0.0") (h "1swq1hbq6nk58dphz7yfpq5fh81rvpbj9b68zjcjdxwqzwik1i2q")))

(define-public crate-tfmicro-0.1.0 (c (n "tfmicro") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.53.2") (d #t) (k 1)) (d (n "cc") (r "^1.0.52") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "cpp") (r "^0.5.5") (d #t) (k 0)) (d (n "cpp_build") (r "^0.5.4") (d #t) (k 1)) (d (n "cty") (r "^0.2.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 2)) (d (n "error-chain") (r "^0.12.2") (d #t) (k 1)) (d (n "fs_extra") (r "^1.1.0") (d #t) (k 1)) (d (n "glob") (r "^0.3.0") (d #t) (k 1)) (d (n "itertools") (r "^0.9.0") (d #t) (k 2)) (d (n "log") (r "^0.4.8") (k 0)) (d (n "managed") (r "^0.7") (k 0)) (d (n "ordered-float") (r "^1.0") (k 0)))) (h "152fai1szyvjr1507z55rl18fipvr4qklq1dzjniplc5ppm8pnmj") (f (quote (("std" "managed/std") ("no-c-warnings") ("cmsis-nn") ("build") ("alloc" "managed/alloc"))))))

