(define-module (crates-io tf io tfio) #:use-module (crates-io))

(define-public crate-tfio-0.1.0 (c (n "tfio") (v "0.1.0") (d (list (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "0f34xzs0xjjl4zlx43599p62zn23jih2lci094d4sl364ds06d27")))

(define-public crate-tfio-0.1.1 (c (n "tfio") (v "0.1.1") (d (list (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "1sirsl7764zvmn56wzz0fml1yw9ngrph02x31pf8pzlcr132f5dj")))

(define-public crate-tfio-0.1.2 (c (n "tfio") (v "0.1.2") (d (list (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "1g4mmwa8b2m3ny7v12f7bhsnm227fb09b9dkllwk98yn69j3prls")))

(define-public crate-tfio-0.1.3 (c (n "tfio") (v "0.1.3") (d (list (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "0xzhvh3h4iagfqja6xl35fmrjqkxkb1cqmcpic76fhg3q5i5pkzl")))

(define-public crate-tfio-0.1.31 (c (n "tfio") (v "0.1.31") (d (list (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "0150g8ziwhawdpkrzqc6q3n16c4y7339k52a0kg9rrbxralpzg0x")))

(define-public crate-tfio-0.1.32 (c (n "tfio") (v "0.1.32") (d (list (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "1b7v8cw3lq3ggmznb2kvgffn2af5znfs8jyddkay6cff0768zf88")))

