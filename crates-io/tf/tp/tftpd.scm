(define-module (crates-io tf tp tftpd) #:use-module (crates-io))

(define-public crate-tftpd-0.0.0 (c (n "tftpd") (v "0.0.0") (h "1hrf2wjzi6wkfh3cxwnykm2450z4g7nq2zl73d86wi8pij8ckg0z") (y #t)))

(define-public crate-tftpd-0.1.0 (c (n "tftpd") (v "0.1.0") (h "06bnhf317ma3rxvk34ky6npj9l40k38lrhq2wawr12mnwaiz0vxc") (y #t)))

(define-public crate-tftpd-0.1.1 (c (n "tftpd") (v "0.1.1") (h "167k90847m0ayq1dvvjzpvayzwjfkwqzry0zqxkpvz2lmkw19507")))

(define-public crate-tftpd-0.1.2 (c (n "tftpd") (v "0.1.2") (h "1zd8nvnn3f86lx2wz897qy85nm39dy5qa5w8sy77qsn739rj57sg") (y #t)))

(define-public crate-tftpd-0.1.3 (c (n "tftpd") (v "0.1.3") (h "0wlw71f2bmvy1hibrlm6jhhlg569gmp4qrz7ziyc1mxp6g1d2bxk")))

(define-public crate-tftpd-0.1.4 (c (n "tftpd") (v "0.1.4") (h "0lvgi7kx11z5d3grx09mc1hlks6g0xpiwpla4xzhsld7kpww364f")))

(define-public crate-tftpd-0.1.5 (c (n "tftpd") (v "0.1.5") (h "09xvdvd6a59a89f2x1cd543jpqf5hkcah12sdsjjkjgd44k4gyia")))

(define-public crate-tftpd-0.2.0 (c (n "tftpd") (v "0.2.0") (h "05hf42mj6zrdm17lr0v11gn7kvcpr280r7g2aqb7a2pmbwzm2qdc")))

(define-public crate-tftpd-0.2.1 (c (n "tftpd") (v "0.2.1") (h "0b0m9js1lfwimyrhm00s0d4845mhw03jh9hpf1rfc5657q79wnfm")))

(define-public crate-tftpd-0.2.2 (c (n "tftpd") (v "0.2.2") (h "0vpbjjqr74kqsi7z3rqgxk8mhn5kn91yjsigg4b9wv8gf5cqvxbz")))

(define-public crate-tftpd-0.2.4 (c (n "tftpd") (v "0.2.4") (h "0g3sg4a73rah4zhab7wk5r3vnbagkvf91k4ib9dipc8gbc30mzsb") (y #t)))

(define-public crate-tftpd-0.2.5 (c (n "tftpd") (v "0.2.5") (h "0wz495ip3d4h2qg2bnw997icv7c794zwyq72i0rf5kkjmjlvpqvr")))

(define-public crate-tftpd-0.2.6 (c (n "tftpd") (v "0.2.6") (h "1p16v2b2nr6y3kyssp88b5h3pji41vm5ydjd8za2rmqwnhi0bj5m")))

(define-public crate-tftpd-0.2.7 (c (n "tftpd") (v "0.2.7") (h "0s08lqvwplhncfjrh8d4scvllr0wwlnlmfpz2qsncbni2rczns4a")))

(define-public crate-tftpd-0.2.8 (c (n "tftpd") (v "0.2.8") (h "1hsjryy7vs4aacvvg09vsc05nw9fcwarwrhxn6xbzgw3jnpqvnyn") (y #t)))

(define-public crate-tftpd-0.2.9 (c (n "tftpd") (v "0.2.9") (h "04mjxdamkklzrc0xlxshbqvd929123ayf4apc1qb8c1g29527ajz")))

(define-public crate-tftpd-0.2.10 (c (n "tftpd") (v "0.2.10") (h "0x6s7nb2gik25dm93nh8lxgfr4x6r27a5i37mjvbfqnggbgq1pi2") (f (quote (("integration"))))))

(define-public crate-tftpd-0.2.11 (c (n "tftpd") (v "0.2.11") (h "0kp5a5inysrybfbyvpgx2vwhzk9sr7x2rn32kbax9ng9cbl62b4w") (f (quote (("integration"))))))

(define-public crate-tftpd-0.2.12 (c (n "tftpd") (v "0.2.12") (h "1vlc83bc4l48hq3h3rswsb89ia6nq7rhbw690b8jprg9yjgc6znz") (f (quote (("integration"))))))

