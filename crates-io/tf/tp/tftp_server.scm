(define-module (crates-io tf tp tftp_server) #:use-module (crates-io))

(define-public crate-tftp_server-0.0.1 (c (n "tftp_server") (v "0.0.1") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "16n4w1gbx5ya1insafg15p39p14wf8v83c5h4h212f6rxfy97hca")))

(define-public crate-tftp_server-0.0.2 (c (n "tftp_server") (v "0.0.2") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "env_logger") (r "^0.3.5") (d #t) (k 0)) (d (n "env_logger") (r "^0.3.5") (d #t) (k 2)) (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0ki0m55f84620iwqzbjjs7qr1hlm269vdkkz6w4d34ds5hj63vh9")))

(define-public crate-tftp_server-0.0.3 (c (n "tftp_server") (v "0.0.3") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "env_logger") (r "^0.3.5") (d #t) (k 0)) (d (n "env_logger") (r "^0.3.5") (d #t) (k 2)) (d (n "getopts") (r "^0.2") (d #t) (k 2)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "mio-extras") (r "^2.0.5") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "138gmin5gvspj4djrwpvlyz59yp26zm7ak0c3i5m3gv4mxkbiv7z")))

