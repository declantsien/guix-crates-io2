(define-module (crates-io tf tp tftp) #:use-module (crates-io))

(define-public crate-tftp-0.1.0 (c (n "tftp") (v "0.1.0") (d (list (d (n "ascii") (r "^1") (d #t) (k 0)) (d (n "displaydoc") (r "^0.2") (d #t) (k 0)) (d (n "enumn") (r "^0.1") (d #t) (k 0)) (d (n "expect-test") (r "^1") (d #t) (k 2)))) (h "17ahrq0a5jbc701rpw86mpx5k3xzp9f1b6wk66hybbllksn02ldb") (f (quote (("default" "alloc") ("alloc"))))))

