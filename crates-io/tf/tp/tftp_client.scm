(define-module (crates-io tf tp tftp_client) #:use-module (crates-io))

(define-public crate-tftp_client-0.1.0 (c (n "tftp_client") (v "0.1.0") (d (list (d (n "byte-strings") (r "^0.3") (d #t) (k 0)) (d (n "paste") (r "^1") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0zrc806qy6zn2zvk6c6wwbbznh2wy00vw0z4ljl6v62wqd9ndkj5")))

(define-public crate-tftp_client-0.2.0 (c (n "tftp_client") (v "0.2.0") (d (list (d (n "byte-strings") (r "^0.3") (d #t) (k 0)) (d (n "paste") (r "^1") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0dz27mfwwic6qfbjm1bpzcahsv7l2w313gp3isqyp2cxr2ya8vxh")))

