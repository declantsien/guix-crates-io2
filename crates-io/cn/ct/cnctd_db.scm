(define-module (crates-io cn ct cnctd_db) #:use-module (crates-io))

(define-public crate-cnctd_db-0.1.0 (c (n "cnctd_db") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.115") (d #t) (k 0)) (d (n "sqlx") (r "^0.7.4") (f (quote ("postgres" "runtime-tokio-rustls" "tls-rustls"))) (d #t) (k 0)) (d (n "state") (r "^0.6.0") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 0)))) (h "16w2irwcagcvpsgk1j8pl23w5w27r1k0z5jrzm46y1j013197sb0")))

