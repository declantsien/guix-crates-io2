(define-module (crates-io cn ct cnctd_shell) #:use-module (crates-io))

(define-public crate-cnctd_shell-0.1.0 (c (n "cnctd_shell") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0i9jyhs792sh2liyz1mjwxbrysydd2h3s1p8v7dzvzjjgpd4xvnz") (y #t)))

(define-public crate-cnctd_shell-0.1.1 (c (n "cnctd_shell") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0zqqwqhdf42svkr886jkkvwpvywvw9rkdzb3qmhvgajzj8673vs1") (y #t)))

(define-public crate-cnctd_shell-0.1.2 (c (n "cnctd_shell") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("full"))) (d #t) (k 0)))) (h "02fg7h3abb35fnkj8xxgy8mmj7gnscc3mn8yc1xk87bmj68n8nzc")))

(define-public crate-cnctd_shell-0.1.3 (c (n "cnctd_shell") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0bd40ffsa9hrpb321ndynqpii01j0ssvysnfi2kw1bmf4ri6dfw1")))

(define-public crate-cnctd_shell-0.1.4 (c (n "cnctd_shell") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0m1paw24v53akxpziqdqsv0yayvr4n8s39aib37bks85bcrp3ljq")))

(define-public crate-cnctd_shell-0.1.5 (c (n "cnctd_shell") (v "0.1.5") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1n1hhym4x5sv4mblc441b9zm6v49qh4js2gpnws6917y6z6y7nqp")))

(define-public crate-cnctd_shell-0.1.6 (c (n "cnctd_shell") (v "0.1.6") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "cnctd") (r "^0.1.3") (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("full"))) (d #t) (k 0)))) (h "15am0zdg13cbd4n5860rs3rzbalkp0187y6n15dcp8x0gp1j31dh")))

(define-public crate-cnctd_shell-0.1.7 (c (n "cnctd_shell") (v "0.1.7") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0p43bij21sym8w43kjdkrf5j8mm9ad425bak7f5c6yrncljpyr7l")))

