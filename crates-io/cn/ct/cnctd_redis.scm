(define-module (crates-io cn ct cnctd_redis) #:use-module (crates-io))

(define-public crate-cnctd_redis-0.1.0 (c (n "cnctd_redis") (v "0.1.0") (h "18401h7xclp44s6fk4bq4a7pzz5s8qsgw45vpqd1hkfmkmmy6zg7")))

(define-public crate-cnctd_redis-0.1.1 (c (n "cnctd_redis") (v "0.1.1") (h "033nvg8k89g9h8c0hrs17dkv3ffgjf2fl3bid2xdqp2v8xdxsfgx")))

(define-public crate-cnctd_redis-0.1.3 (c (n "cnctd_redis") (v "0.1.3") (h "1r52vyvgijh72f79kn6hx3nqc41j1b5rzpwg1j5500z8v21pyh2d")))

(define-public crate-cnctd_redis-0.1.4 (c (n "cnctd_redis") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "redis") (r "^0.23.3") (d #t) (k 0)))) (h "156y5fsjm8cjxdiw22g7qjldc7llwxqhgjg2grrskng8nhgqz51q")))

(define-public crate-cnctd_redis-0.1.5 (c (n "cnctd_redis") (v "0.1.5") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "r2d2") (r "^0.8.10") (d #t) (k 0)) (d (n "r2d2_redis") (r "^0.14.0") (d #t) (k 0)) (d (n "redis") (r "^0.25.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.115") (d #t) (k 0)) (d (n "state") (r "^0.6.0") (d #t) (k 0)))) (h "129i9dmdvnmi3gdgvdsck3qksxgyynn54a5lfr7wycd18xdnp3zr")))

(define-public crate-cnctd_redis-0.1.6 (c (n "cnctd_redis") (v "0.1.6") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "r2d2") (r "^0.8.10") (d #t) (k 0)) (d (n "r2d2_redis") (r "^0.14.0") (d #t) (k 0)) (d (n "redis") (r "^0.25.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.115") (d #t) (k 0)) (d (n "state") (r "^0.6.0") (d #t) (k 0)))) (h "0k106k0jdp4j8glllbmk9iy78790nrgh18rsxl1nms3v4qpg78gz")))

(define-public crate-cnctd_redis-0.1.7 (c (n "cnctd_redis") (v "0.1.7") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "r2d2") (r "^0.8.10") (d #t) (k 0)) (d (n "r2d2_redis") (r "^0.14.0") (d #t) (k 0)) (d (n "redis") (r "^0.25.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.115") (d #t) (k 0)) (d (n "state") (r "^0.6.0") (d #t) (k 0)))) (h "1k4yf0mjjzrf08h1ynfi9fiaf6n8zqa0aiapkcvqw8ibgwiin103")))

