(define-module (crates-io cn ct cnctd_docker) #:use-module (crates-io))

(define-public crate-cnctd_docker-0.1.0 (c (n "cnctd_docker") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "cnctd_shell") (r "^0.1.1") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (d #t) (k 0)))) (h "1cv58y3869xhpifdjv4159293g1j8z7ajwspj1gb1sy0kpyrzn1h")))

(define-public crate-cnctd_docker-0.1.1 (c (n "cnctd_docker") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "cnctd_shell") (r "^0.1.1") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (d #t) (k 0)))) (h "0v52rwwg51hi9clq6b8b60a40jqh3sidrccmyg8ivm88gr9nyii0")))

