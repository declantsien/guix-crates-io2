(define-module (crates-io cn ct cnctd_config) #:use-module (crates-io))

(define-public crate-cnctd_config-0.1.0 (c (n "cnctd_config") (v "0.1.0") (h "0jp21cp7kas0x10kx426grffbihs6dnxanlm3m9r4fi7adbhbhzj")))

(define-public crate-cnctd_config-0.1.1 (c (n "cnctd_config") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)))) (h "1kr81pfxnlc25ws8myryp8fw42mmcq4dyni75jfy1vk5ip6ic3ln")))

(define-public crate-cnctd_config-0.1.2 (c (n "cnctd_config") (v "0.1.2") (d (list (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)))) (h "0b611n9m5andfhiba4l3jjix4xi366s7rygh03ns907s4vigbwwr")))

