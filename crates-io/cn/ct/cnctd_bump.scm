(define-module (crates-io cn ct cnctd_bump) #:use-module (crates-io))

(define-public crate-cnctd_bump-0.1.0 (c (n "cnctd_bump") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "cnctd_cargo") (r "^0.1") (d #t) (k 0)) (d (n "cnctd_npm") (r "^0.1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1nxkmpxh1rphpfqkgfww267clf6h5lndi1msxkfl06xb5csmfr2c")))

(define-public crate-cnctd_bump-0.1.1 (c (n "cnctd_bump") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "cnctd_cargo") (r "^0.1.2") (d #t) (k 0)) (d (n "cnctd_npm") (r "^0.1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1s02n7n00k9jgm3jkcnjawiihb6ds68v7f2s16nh85p830wlwckj")))

(define-public crate-cnctd_bump-0.1.2 (c (n "cnctd_bump") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "cnctd_cargo") (r "^0.1.2") (d #t) (k 0)) (d (n "cnctd_npm") (r "^0.1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1rvsf4dkkj7gn7sg9jdzq0f2qpil3h4f7vwgc0caabik524vyk28")))

(define-public crate-cnctd_bump-0.1.3 (c (n "cnctd_bump") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "cnctd_cargo") (r "^0.1.2") (d #t) (k 0)) (d (n "cnctd_npm") (r "^0.1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1mskbcd4qsz03lxra4xkrn20imvxkbrdc9mrjwpk54zhqkw6ygw4")))

(define-public crate-cnctd_bump-0.1.4 (c (n "cnctd_bump") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "cnctd_cargo") (r "^0.1.2") (d #t) (k 0)) (d (n "cnctd_npm") (r "^0.1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0gl7x3qwdpd8z4yc5rdxqw8mq3znb7jzynhs5s733z865lgxxyi3")))

(define-public crate-cnctd_bump-0.1.5 (c (n "cnctd_bump") (v "0.1.5") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "cnctd_cargo") (r "^0.1.2") (d #t) (k 0)) (d (n "cnctd_npm") (r "^0.1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("full"))) (d #t) (k 0)))) (h "03hmrrfl3pzp3nlkjchzhsjzrwpgwpzjnfhyi0xz1acvy5srm15c")))

(define-public crate-cnctd_bump-0.1.6 (c (n "cnctd_bump") (v "0.1.6") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "cnctd_cargo") (r "^0.1.2") (d #t) (k 0)) (d (n "cnctd_npm") (r "^0.1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0bj7snpqwpd3yzi885r9cpkrlhd484sq5vdc8fhafpdahjckspz1")))

(define-public crate-cnctd_bump-0.1.7 (c (n "cnctd_bump") (v "0.1.7") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "cnctd_cargo") (r "^0.1.2") (d #t) (k 0)) (d (n "cnctd_npm") (r "^0.1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1gmjrjl7iv3j89jwwh8jjhnkz1zki5waswrk48sl74ic5jg4qf22")))

(define-public crate-cnctd_bump-0.1.8 (c (n "cnctd_bump") (v "0.1.8") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "cnctd_cargo") (r "^0.1.2") (d #t) (k 0)) (d (n "cnctd_npm") (r "^0.1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("full"))) (d #t) (k 0)))) (h "19b7minynk910zb84sv2929iqhxnls9fliq8wg05h2478cvlxfz1")))

(define-public crate-cnctd_bump-0.1.9 (c (n "cnctd_bump") (v "0.1.9") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "cnctd_cargo") (r "^0.1.2") (d #t) (k 0)) (d (n "cnctd_npm") (r "^0.1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("full"))) (d #t) (k 0)))) (h "00frqh7m754f25ad5ynlzrp2n1r1s37m5kipxkrp1q314z8nzxxa")))

