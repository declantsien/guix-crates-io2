(define-module (crates-io cn ct cnctd_roku) #:use-module (crates-io))

(define-public crate-cnctd_roku-0.1.0 (c (n "cnctd_roku") (v "0.1.0") (h "15a6y7m3s79bf8wiq0dqkbb69i2v7l3xx5v6szy5vbnzl81hfdiw")))

(define-public crate-cnctd_roku-0.1.2 (c (n "cnctd_roku") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "chrono") (r "^0.4.34") (d #t) (k 0)) (d (n "cnctd_rest") (r "^0.1.4") (d #t) (k 0)) (d (n "cnctd_shell") (r "^0.1.7") (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.113") (d #t) (k 0)) (d (n "telnet") (r "^0.2.1") (d #t) (k 0)))) (h "0aqkkxj8h52mkw4dkiywkipla3cgp9m72fxnqndfbgr9gnc2k4ag")))

