(define-module (crates-io cn ct cnctd_mta) #:use-module (crates-io))

(define-public crate-cnctd_mta-0.1.0 (c (n "cnctd_mta") (v "0.1.0") (h "146ng34bbggk8xbclwy7syck52gi7q5q21xklf0jjz1s55jc3cp5")))

(define-public crate-cnctd_mta-0.1.1 (c (n "cnctd_mta") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "gtfs-rt") (r "^0.4.0") (d #t) (k 0)) (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "prost-derive") (r "^0.11") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.26") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1sqnp9kgyz5py0f085n6nswlwqywqx7yrngh6k53fqr5rxbkldww")))

(define-public crate-cnctd_mta-0.1.2 (c (n "cnctd_mta") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "futures") (r "^0.3.30") (d #t) (k 0)) (d (n "gtfs-rt") (r "^0.4.0") (d #t) (k 0)) (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "prost-derive") (r "^0.11") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.26") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1l0w74jlrircljd0nbig6z4pmrvjnmgxkzmwwgp5bddk1vbdjp5v")))

