(define-module (crates-io cn ct cnctd_utils) #:use-module (crates-io))

(define-public crate-cnctd_utils-0.1.0 (c (n "cnctd_utils") (v "0.1.0") (h "1py7kdal73fnpkzsgjvjlmg28ciqq7gq2vk6z26smcbadd058jms")))

(define-public crate-cnctd_utils-0.1.1 (c (n "cnctd_utils") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)))) (h "04728c5v98k7lfrsafl2677skaqb9lzpr3mc3nzkbrw6akwaly7k")))

(define-public crate-cnctd_utils-0.1.2 (c (n "cnctd_utils") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "figlet-rs") (r "^0.1.5") (d #t) (k 0)))) (h "1in8jx3h7gb52s0zraijhvbixipijbj6zkqbhv1xjqm7b6acw076")))

