(define-module (crates-io cn ct cnctd_rest) #:use-module (crates-io))

(define-public crate-cnctd_rest-0.1.0 (c (n "cnctd_rest") (v "0.1.0") (h "02jhnqwxh20ksnm56py034srixdhdxv8ndjx60vh4q201k5ggv6d")))

(define-public crate-cnctd_rest-0.1.1 (c (n "cnctd_rest") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "0cj4d38kwwjd1swapvnz2ymkinld7z9azjk1kybwfhrvzk9qb64g")))

(define-public crate-cnctd_rest-0.1.2 (c (n "cnctd_rest") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "14pvdx0zpcwcfph56101ri8zavarj52j9riydsq7xq4vmfnrj0p7")))

(define-public crate-cnctd_rest-0.1.3 (c (n "cnctd_rest") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "13z7v0hqw844mjnwmrsds6xz74vz5p5bcmh8nmdrw54i5jszdn9c")))

(define-public crate-cnctd_rest-0.1.4 (c (n "cnctd_rest") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "0xhlpadhhwsg9xrin4dw66nxb8is7nm6bf6r3klh6a4wi1za3jr5")))

(define-public crate-cnctd_rest-0.1.5 (c (n "cnctd_rest") (v "0.1.5") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "reqwest") (r "^0.12.3") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.115") (d #t) (k 0)))) (h "145h298yxwcpqby90rnwb40i7476413sqfsfvjh65kcnc5jaid2c")))

(define-public crate-cnctd_rest-0.1.6 (c (n "cnctd_rest") (v "0.1.6") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "reqwest") (r "^0.12.3") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.115") (d #t) (k 0)))) (h "0ss03im37f1gvqwnhdz5l6zrg286vdzrh5czkmina70dqj0pjfhy")))

