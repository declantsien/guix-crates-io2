(define-module (crates-io cn ct cnctd_server_api) #:use-module (crates-io))

(define-public crate-cnctd_server_api-0.1.0 (c (n "cnctd_server_api") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("full"))) (d #t) (k 0)))) (h "08vjxk46a2pj13rm1glzpsr6mwxdmhrjw6794n19nmg9m9sw7f8x")))

(define-public crate-cnctd_server_api-0.1.1 (c (n "cnctd_server_api") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("full"))) (d #t) (k 0)))) (h "087z7fwdm0my3yv6w5avcqzh0zfas5l8j7ka73iyrhaky1wx4z94")))

