(define-module (crates-io cn ct cnctd_xcode) #:use-module (crates-io))

(define-public crate-cnctd_xcode-0.1.0 (c (n "cnctd_xcode") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)))) (h "1yxckbz5ncfmf0s397gf8zg8nwnxrny64m25w4qw44pgfj83qj07")))

(define-public crate-cnctd_xcode-0.1.1 (c (n "cnctd_xcode") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)))) (h "0axpl7shyp43fmx8d7i49lilxvgqqn4mjyi3gzgwlaqfgm1w2g2r")))

