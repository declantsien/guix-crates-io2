(define-module (crates-io cn ct cnctd_dialogue) #:use-module (crates-io))

(define-public crate-cnctd_dialogue-0.1.0 (c (n "cnctd_dialogue") (v "0.1.0") (d (list (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "dialoguer") (r "^0.10.4") (d #t) (k 0)) (d (n "strum") (r "^0.25.0") (d #t) (k 0)))) (h "0pfgdsfhcadcm93j8026xys7ia2jkxksmfgj88q03j55jkv2l680")))

(define-public crate-cnctd_dialogue-0.1.1 (c (n "cnctd_dialogue") (v "0.1.1") (d (list (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "dialoguer") (r "^0.10.4") (d #t) (k 0)) (d (n "strum") (r "^0.25.0") (d #t) (k 0)))) (h "1h3qqcphb8qmqljq2wgxiwv36c2whvl7pg9cyq3k57434azv66yx")))

(define-public crate-cnctd_dialogue-0.1.2 (c (n "cnctd_dialogue") (v "0.1.2") (d (list (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "dialoguer") (r "^0.10.4") (d #t) (k 0)) (d (n "strum") (r "^0.25.0") (d #t) (k 0)))) (h "0kzpxy0mw8nqdmcgph934s8vmm9j9xpb4wvjarkll2cr1qzqrh35")))

(define-public crate-cnctd_dialogue-0.1.3 (c (n "cnctd_dialogue") (v "0.1.3") (d (list (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "dialoguer") (r "^0.10.4") (d #t) (k 0)) (d (n "strum") (r "^0.25.0") (d #t) (k 0)))) (h "1v56lark9hdvj5aab2lm17ia32bq6nyyhz9jr9pnj5i7n33qh89v")))

(define-public crate-cnctd_dialogue-0.1.4 (c (n "cnctd_dialogue") (v "0.1.4") (d (list (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "dialoguer") (r "^0.10.4") (d #t) (k 0)) (d (n "strum") (r "^0.25.0") (d #t) (k 0)))) (h "03nys9rynlaw32n7480la001hrs1x3psgwj56v9di0l84mi507w6")))

(define-public crate-cnctd_dialogue-0.1.5 (c (n "cnctd_dialogue") (v "0.1.5") (d (list (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "dialoguer") (r "^0.11.0") (d #t) (k 0)) (d (n "strum") (r "^0.25.0") (d #t) (k 0)))) (h "0fi7ysip39rbqlhjxzmq56m8zcc0wq1fazmvw9mmlk1a49vxv09j")))

(define-public crate-cnctd_dialogue-0.1.6 (c (n "cnctd_dialogue") (v "0.1.6") (d (list (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "dialoguer") (r "^0.11.0") (d #t) (k 0)) (d (n "strum") (r "^0.25.0") (d #t) (k 0)))) (h "18fbs79wpbm5jscb7r0jjq3nl5la4lqydhhqms534x30vnn72l3z")))

