(define-module (crates-io cn ct cnctd_smart_home) #:use-module (crates-io))

(define-public crate-cnctd_smart_home-0.1.0 (c (n "cnctd_smart_home") (v "0.1.0") (h "1csxhaaibzmrzal516faxf42bwdgipcjvh870nm6bpcia30qni33")))

(define-public crate-cnctd_smart_home-0.1.4 (c (n "cnctd_smart_home") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0.80") (d #t) (k 0)) (d (n "govee-api") (r "^1.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tplinker") (r "^0.4.4") (d #t) (k 0)))) (h "0ajg1va44arrilcbrc53mn84kml7ri2cj5f0vlpfy98nph4immqj")))

