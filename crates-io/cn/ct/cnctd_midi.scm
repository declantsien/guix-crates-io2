(define-module (crates-io cn ct cnctd_midi) #:use-module (crates-io))

(define-public crate-cnctd_midi-0.1.0 (c (n "cnctd_midi") (v "0.1.0") (h "1ckizvshhfqahj2ksyrrahbshbm6z2w6zicv9bwp81pw4aiw9225")))

(define-public crate-cnctd_midi-0.1.2 (c (n "cnctd_midi") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "midir") (r "^0.9.1") (d #t) (k 0)) (d (n "tune") (r "^0.34.0") (d #t) (k 0)))) (h "1qkdvirxlzahhhfxrii9d4x8x4q26xvg6dff8mijyr15s9b3fc5i")))

(define-public crate-cnctd_midi-0.1.3 (c (n "cnctd_midi") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "midir") (r "^0.9.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("full"))) (d #t) (k 0)))) (h "11xpl2dhzi4php9vs5jbd05jdbgsr4ja59iqajqqjk6cgayw68c7")))

(define-public crate-cnctd_midi-0.1.4 (c (n "cnctd_midi") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "midir") (r "^0.9.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("full"))) (d #t) (k 0)))) (h "13mahmffi2cpphfa9bjqa3ga43grvsc298k8cn9bzad1ca0lmip5")))

