(define-module (crates-io cn ct cnctd_android_project) #:use-module (crates-io))

(define-public crate-cnctd_android_project-0.1.0 (c (n "cnctd_android_project") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "regex") (r "^1.9.5") (d #t) (k 0)))) (h "00f3pnq9p01q63h3zfqc9kcfa01ixnyvssihsax66hxx5338gxyn")))

(define-public crate-cnctd_android_project-0.1.1 (c (n "cnctd_android_project") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "regex") (r "^1.9.5") (d #t) (k 0)))) (h "0spps5b0s34r7ll89fvyc9c0r7kw457qhb2ix20hb671zm3jfi0x")))

