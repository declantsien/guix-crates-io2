(define-module (crates-io cn et cnetworks) #:use-module (crates-io))

(define-public crate-cnetworks-0.1.0 (c (n "cnetworks") (v "0.1.0") (d (list (d (n "queues") (r "^1.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "1x5chc3c387rf1yzrx8zransayanvwzwryz26ib73619w4gw4v2m")))

(define-public crate-cnetworks-0.1.1 (c (n "cnetworks") (v "0.1.1") (d (list (d (n "queues") (r "^1.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "0gxxjchlchgqg19q90lx78h03ra0cjdfzk1qyrh2qi3lsksv6h0q")))

(define-public crate-cnetworks-0.2.0 (c (n "cnetworks") (v "0.2.0") (d (list (d (n "queues") (r "^1.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "1780i84g1f9a4rhdsj5z7p2qwkayp97kwp27jbrsxvk6030jr5i9")))

(define-public crate-cnetworks-0.3.0 (c (n "cnetworks") (v "0.3.0") (d (list (d (n "queues") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("serde1"))) (d #t) (k 0)) (d (n "rand_xoshiro") (r "^0.6") (f (quote ("serde1"))) (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0440srys046rd3jlfjh8j2n4v0alw4a4rlk518mrdghw7amvfj3v")))

(define-public crate-cnetworks-0.3.1 (c (n "cnetworks") (v "0.3.1") (d (list (d (n "queues") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("serde1"))) (d #t) (k 0)) (d (n "rand_xoshiro") (r "^0.6") (f (quote ("serde1"))) (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0c70d84r6bksir2zvyg2nx27i4pxa8rbaiz2khp5773rg3jx4fx2")))

(define-public crate-cnetworks-0.3.2 (c (n "cnetworks") (v "0.3.2") (d (list (d (n "queues") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("serde1"))) (d #t) (k 0)) (d (n "rand_xoshiro") (r "^0.6") (f (quote ("serde1"))) (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "01kw49rx78140siq35d1flza9nd1w6qjc40ywrxd336rncgfz432")))

(define-public crate-cnetworks-0.4.0 (c (n "cnetworks") (v "0.4.0") (d (list (d (n "queues") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("serde1"))) (d #t) (k 0)) (d (n "rand_seeder") (r "^0.2") (d #t) (k 0)) (d (n "rand_xoshiro") (r "^0.6") (f (quote ("serde1"))) (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "15np0ffw8maq2dlviny3p21lx87h07nxjkf5pikia35si4qd4ff3")))

(define-public crate-cnetworks-0.5.0 (c (n "cnetworks") (v "0.5.0") (d (list (d (n "queues") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("serde1"))) (d #t) (k 0)) (d (n "rand_seeder") (r "^0.2") (d #t) (k 0)) (d (n "rand_xoshiro") (r "^0.6") (f (quote ("serde1"))) (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1zsqc9731h5nbvpgrq8pr2y98dwrr2nh97am9ylxb6wrfzn5y83k")))

(define-public crate-cnetworks-0.6.0 (c (n "cnetworks") (v "0.6.0") (d (list (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "indexmap") (r "^1.7") (d #t) (k 0)) (d (n "nalgebra") (r "^0.29") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rand_seeder") (r "^0.2") (d #t) (k 0)) (d (n "rand_xoshiro") (r "^0.6") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1") (d #t) (k 0)))) (h "1dcpnnlswzyj6xlk0cqskhg21nzr0svl3a4l4w26cy2g1ax76cnm")))

(define-public crate-cnetworks-0.6.1 (c (n "cnetworks") (v "0.6.1") (d (list (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "indexmap") (r "^1.7") (d #t) (k 0)) (d (n "nalgebra") (r "^0.29") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rand_seeder") (r "^0.2") (d #t) (k 0)) (d (n "rand_xoshiro") (r "^0.6") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1") (d #t) (k 0)))) (h "07xnsnvxsa0bcmmik7aq1sffi6yc3wz4vxnijs5d50lfxi41x5lw")))

(define-public crate-cnetworks-0.7.0 (c (n "cnetworks") (v "0.7.0") (d (list (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "indexmap") (r "^1.7") (d #t) (k 0)) (d (n "nalgebra") (r "^0.29") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rand_seeder") (r "^0.2") (d #t) (k 0)) (d (n "rand_xoshiro") (r "^0.6") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1") (d #t) (k 0)))) (h "1nc0avj5izmdm215mckbcinpq7dmixjshq76c8i8h33gfplqvgij")))

