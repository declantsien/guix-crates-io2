(define-module (crates-io cn et cnet) #:use-module (crates-io))

(define-public crate-cnet-0.0.1 (c (n "cnet") (v "0.0.1") (d (list (d (n "reqwest") (r "^0.11.4") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.36") (d #t) (k 0)))) (h "0lgxp5wzng8kg2aw6wv9dv61bj8pywmdk5ylhbywa88yrgidxp09")))

(define-public crate-cnet-0.0.2 (c (n "cnet") (v "0.0.2") (d (list (d (n "reqwest") (r "^0.11.4") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.36") (d #t) (k 0)))) (h "045f4zq48vsdllpnm375h24gj6v6v59s4y9l317l2jhd22jd1zv8")))

(define-public crate-cnet-0.0.3 (c (n "cnet") (v "0.0.3") (d (list (d (n "reqwest") (r "^0.11.4") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.36") (d #t) (k 0)))) (h "1dgbn5h777yjp04jh58ji5d21xm76k3w1ah8b3yqrkkxiivf2w6d")))

