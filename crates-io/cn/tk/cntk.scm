(define-module (crates-io cn tk cntk) #:use-module (crates-io))

(define-public crate-cntk-0.0.1 (c (n "cntk") (v "0.0.1") (d (list (d (n "cpp") (r "^0.3.2") (d #t) (k 0)) (d (n "cpp_build") (r "^0.3.2") (d #t) (k 1)))) (h "0nkgl2wb41757ir0jig3czdjpflnr14n4fp93iqnxvdl6ck7r8mw")))

(define-public crate-cntk-0.0.2 (c (n "cntk") (v "0.0.2") (d (list (d (n "cpp") (r "^0.3.2") (d #t) (k 0)) (d (n "cpp_build") (r "^0.3.2") (d #t) (k 1)))) (h "18w4df2m0078cm160qwikdhrmvdca2js5044mq56a79iks269av7")))

(define-public crate-cntk-0.0.3 (c (n "cntk") (v "0.0.3") (d (list (d (n "cpp") (r "^0.3.2") (d #t) (k 0)) (d (n "cpp_build") (r "^0.3.2") (d #t) (k 1)))) (h "04f0g7avn4xzn1kdwzmdr3np6zxjdfy1p920av2clxfahyji2pbq")))

(define-public crate-cntk-0.0.4 (c (n "cntk") (v "0.0.4") (d (list (d (n "cpp") (r "^0.3.2") (d #t) (k 0)) (d (n "cpp_build") (r "^0.3.2") (d #t) (k 1)))) (h "1flr8rwnzp8sc1i02qv8rj64fwnyklnprxbs0n0mixb8n0ls4b1k")))

(define-public crate-cntk-0.1.0 (c (n "cntk") (v "0.1.0") (d (list (d (n "cpp") (r "^0.3.2") (d #t) (k 0)) (d (n "cpp_build") (r "^0.3.2") (d #t) (k 1)) (d (n "mnist") (r "^0.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "regex") (r "^0.2") (d #t) (k 2)))) (h "05sxy246w0sm7fivnx6yqphpkfgysk7y3aymbvaxb14ajp22d26i")))

(define-public crate-cntk-0.1.1 (c (n "cntk") (v "0.1.1") (d (list (d (n "cpp") (r "^0.3.2") (d #t) (k 0)) (d (n "cpp_build") (r "^0.3.2") (d #t) (k 1)) (d (n "mnist") (r "^0.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "regex") (r "^0.2") (d #t) (k 2)))) (h "1hyl3d1c3y4sxmxc57wy8cnwf1sj409phmk0w6g81q2d7vzkhnah")))

(define-public crate-cntk-0.2.0 (c (n "cntk") (v "0.2.0") (d (list (d (n "cpp") (r "^0.3.2") (d #t) (k 0)) (d (n "cpp_build") (r "^0.3.2") (d #t) (k 1)) (d (n "mnist") (r "^0.4.0") (d #t) (k 2)) (d (n "ndarray") (r "^0.10") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "regex") (r "^0.2") (d #t) (k 2)))) (h "0hqg4fp6697z1fd6c9a02azxhpl8l7czqd8z8gdsmgh0bw0zki9s")))

(define-public crate-cntk-0.2.1 (c (n "cntk") (v "0.2.1") (d (list (d (n "cpp") (r "^0.3.2") (d #t) (k 0)) (d (n "cpp_build") (r "^0.3.2") (d #t) (k 1)) (d (n "mnist") (r "^0.4.0") (d #t) (k 2)) (d (n "ndarray") (r "^0.10") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "regex") (r "^0.2") (d #t) (k 2)))) (h "1iiqc4h4qfzfx0rssicj85aclkbwk4fm3739jvilnjf6yi1pc624")))

