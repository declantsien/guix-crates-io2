(define-module (crates-io cn sl cnsl) #:use-module (crates-io))

(define-public crate-cnsl-0.1.0 (c (n "cnsl") (v "0.1.0") (h "044yyw6a5jl5ag8jdzx8427bpl17zr570zpbah5m6282psnivxf0") (y #t) (r "1.60.0")))

(define-public crate-cnsl-0.1.1 (c (n "cnsl") (v "0.1.1") (h "14x9k6rbfjkffkq0ac0gjmv5wdgx3a8ihx2rcrj7vm4q5c8h3c1v") (r "1.60.0")))

(define-public crate-cnsl-0.1.2 (c (n "cnsl") (v "0.1.2") (h "1xm0qmbagfv8dapdaqc4kpg419xja3mhqcdyj69qppgari7sgpwa") (r "1.60.0")))

(define-public crate-cnsl-0.1.3 (c (n "cnsl") (v "0.1.3") (h "0ylqiyn4yajn8bcg8ysy0di2pbk7wfk6yy0rrf08g0k817kjimwn") (r "1.60.0")))

