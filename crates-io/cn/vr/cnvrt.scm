(define-module (crates-io cn vr cnvrt) #:use-module (crates-io))

(define-public crate-cnvrt-0.1.0 (c (n "cnvrt") (v "0.1.0") (h "19g6m28kjj1vgdhabg2376z4nbp8pgf3f7r920j34sr45bfampcv")))

(define-public crate-cnvrt-0.2.0 (c (n "cnvrt") (v "0.2.0") (h "0ilkf729ig8j600p0aycyh2bnlqg9p67p79d0gdfi8gqh2hn268m")))

