(define-module (crates-io cn -s cn-stratum) #:use-module (crates-io))

(define-public crate-cn-stratum-0.3.0 (c (n "cn-stratum") (v "0.3.0") (d (list (d (n "arrayvec") (r "^0.4") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "02zbkfnqbibi98j2cz7f7rp8lwgmpvknqfb4kyh4wa5ilq4rbb99") (f (quote (("default" "client") ("client"))))))

(define-public crate-cn-stratum-0.4.0 (c (n "cn-stratum") (v "0.4.0") (d (list (d (n "arrayvec") (r "^0.4") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1r2p8y2xp28lh7rji7fnkx4qfpn0agmxbayg7459hdbhwdxl7614") (f (quote (("default" "client") ("client"))))))

(define-public crate-cn-stratum-0.4.1 (c (n "cn-stratum") (v "0.4.1") (d (list (d (n "arrayvec") (r "^0.4") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0am4nydqwdgpriph3my0916v8n77dvfcwbn328wac3av8a04dq86") (f (quote (("default" "client") ("client"))))))

