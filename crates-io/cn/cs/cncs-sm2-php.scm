(define-module (crates-io cn cs cncs-sm2-php) #:use-module (crates-io))

(define-public crate-cncs-sm2-php-0.1.0 (c (n "cncs-sm2-php") (v "0.1.0") (d (list (d (n "base64") (r "^0.13") (f (quote ("std"))) (k 0)) (d (n "cncs-sm2-kit") (r "^0.1") (k 0)) (d (n "ext-php-rs") (r "^0.7") (k 0)))) (h "03npfyrj86i7kwsq0f3j113d2asfp3wnz41j5xzhbmz618rak1m5")))

(define-public crate-cncs-sm2-php-0.1.1 (c (n "cncs-sm2-php") (v "0.1.1") (d (list (d (n "base64") (r "^0.13") (f (quote ("std"))) (k 0)) (d (n "cncs-sm2-kit") (r "^0.1") (k 0)) (d (n "ext-php-rs") (r "^0.7") (k 0)))) (h "04jb9xgm7ndjvfcl6xwkl7pn165v234rh8paicclga79fa0d5g1l")))

(define-public crate-cncs-sm2-php-0.1.2 (c (n "cncs-sm2-php") (v "0.1.2") (d (list (d (n "base64") (r "^0.13") (f (quote ("std"))) (k 0)) (d (n "cncs-sm2-kit") (r "^0.1") (k 0)) (d (n "ext-php-rs") (r "^0.7") (k 0)))) (h "1xdbnhilcmjdzrd9bprcdp947ms677xrnz2pcsgrmr4rhzkk6r8i")))

