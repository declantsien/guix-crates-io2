(define-module (crates-io cn cs cncs-sm2-kit) #:use-module (crates-io))

(define-public crate-cncs-sm2-kit-0.1.0 (c (n "cncs-sm2-kit") (v "0.1.0") (d (list (d (n "gmsm") (r "^0.1") (k 0)) (d (n "libsm") (r "^0.4") (k 0)) (d (n "num-bigint") (r "^0.4") (k 0)) (d (n "num-traits") (r "^0.2") (k 0)))) (h "05p5bpaxq1y11g2hlc1c41gn852kxbsahyvnqyps4gyzl24m9ljp")))

(define-public crate-cncs-sm2-kit-0.1.1 (c (n "cncs-sm2-kit") (v "0.1.1") (d (list (d (n "gmsm") (r "^0.1") (k 0)) (d (n "libsm") (r "^0.4") (k 0)) (d (n "num-bigint") (r "^0.4") (k 0)) (d (n "num-traits") (r "^0.2") (k 0)))) (h "038bv0ama8w5gxy6kb878q8j65955cnxjf2ym7i2b3mbxyp59m9x")))

(define-public crate-cncs-sm2-kit-0.1.2 (c (n "cncs-sm2-kit") (v "0.1.2") (d (list (d (n "gmsm") (r "^0.1") (k 0)) (d (n "libsm") (r "^0.4") (k 0)) (d (n "num-bigint") (r "^0.4") (k 0)) (d (n "num-traits") (r "^0.2") (k 0)))) (h "16y8m78981xdcz2pc3csm9203hzmq7pm5brxqc439rv44vairggk")))

(define-public crate-cncs-sm2-kit-0.1.3 (c (n "cncs-sm2-kit") (v "0.1.3") (d (list (d (n "gmsm") (r "^0.1") (k 0)) (d (n "hex-simd") (r "^0.6.2") (d #t) (k 0)) (d (n "libsm") (r "^0.4") (k 0)) (d (n "num-bigint") (r "^0.4") (k 0)) (d (n "num-traits") (r "^0.2") (k 0)))) (h "0yid912k36vw4ldrwhg0y1hi0xx1wrihn63ds6c4c2jj01n21r4n")))

