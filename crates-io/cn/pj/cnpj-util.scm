(define-module (crates-io cn pj cnpj-util) #:use-module (crates-io))

(define-public crate-cnpj-util-0.1.0 (c (n "cnpj-util") (v "0.1.0") (h "0366gyn7almgh6yqwg5binmkgqiwlnshbgji3pd4ck0mlv58mywb")))

(define-public crate-cnpj-util-0.1.1 (c (n "cnpj-util") (v "0.1.1") (h "119iszx990gplsjwb4g138hpn4w510m0jiirdn54vq80m51cl4v6")))

(define-public crate-cnpj-util-0.1.2 (c (n "cnpj-util") (v "0.1.2") (h "0bijb2iwr8a3a9ky2incca0fcskxr30jlzjjdk1d8cj845cwkxk1")))

(define-public crate-cnpj-util-0.1.3 (c (n "cnpj-util") (v "0.1.3") (h "1ql4gbvrw3vxxh5qmdgbbgvj4v1jl9lnlsj3gkz2dldwa3bqikh2")))

(define-public crate-cnpj-util-0.1.4 (c (n "cnpj-util") (v "0.1.4") (h "1jdpc5d5wx0641a8n5r334979zp69wps43gicy9lghxqvm8k7vjm")))

