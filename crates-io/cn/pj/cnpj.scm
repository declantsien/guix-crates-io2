(define-module (crates-io cn pj cnpj) #:use-module (crates-io))

(define-public crate-cnpj-0.0.1 (c (n "cnpj") (v "0.0.1") (d (list (d (n "arrayvec") (r "^0.7") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (o #t) (d #t) (k 0)))) (h "1669i76k8hmr844gbdfmffrdwmk1h2wlvkp6ac54rq8pxpbxc5ad") (f (quote (("default"))))))

(define-public crate-cnpj-0.1.4 (c (n "cnpj") (v "0.1.4") (d (list (d (n "arrayvec") (r "^0.7") (d #t) (k 0)) (d (n "rand") (r "^0.8") (o #t) (d #t) (k 0)))) (h "1xrvaa9dw1fv0lwjbdnvixln7jlz0acbsjrccg07cgkjwdd8xk56") (f (quote (("std") ("full" "std" "rand") ("default" "std"))))))

(define-public crate-cnpj-0.2.1 (c (n "cnpj") (v "0.2.1") (d (list (d (n "rand") (r "^0.8") (o #t) (d #t) (k 0)))) (h "06k4z22xr8fxgl5xa73vyx0dcbkankvzmlgccss1s3v59gvcpxss") (f (quote (("std") ("full" "std" "rand") ("default" "std"))))))

(define-public crate-cnpj-0.2.2 (c (n "cnpj") (v "0.2.2") (d (list (d (n "rand") (r "^0.8") (o #t) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("std" "std_rng" "small_rng"))) (k 2)))) (h "05ddx3qixqbi8wp84gnicfz4cb9kjw42jmgp45wzk60nch2vmgh7") (f (quote (("std") ("full" "std" "rand") ("default" "std"))))))

