(define-module (crates-io cn fp cnfpack) #:use-module (crates-io))

(define-public crate-cnfpack-0.1.0 (c (n "cnfpack") (v "0.1.0") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "flussab-cnf") (r "^0.2.0") (d #t) (k 0)) (d (n "itoap") (r "^1.0.1") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.22") (d #t) (k 0)) (d (n "zstd") (r "^0.9.0") (d #t) (k 0)))) (h "1bzh4sxnj7ck0hjp81jh7s0mq01cnsa1a5sqifn543abqhmfizki")))

(define-public crate-cnfpack-0.1.1 (c (n "cnfpack") (v "0.1.1") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "flussab-cnf") (r "^0.3.0") (d #t) (k 0)) (d (n "itoap") (r "^1.0.1") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)) (d (n "zstd") (r "^0.10.0") (d #t) (k 0)))) (h "0xxgjaf7hqykhwnsp90fqd1znvphwc12wd693j91iklj94lg46f5")))

