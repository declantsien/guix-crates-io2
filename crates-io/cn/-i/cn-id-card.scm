(define-module (crates-io cn #{-i}# cn-id-card) #:use-module (crates-io))

(define-public crate-cn-id-card-0.1.0 (c (n "cn-id-card") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1cjcnk6xlw0sk8gv5ldd2xvcpi4y0in8vcpbr4zvk2s0k1gcrjjw")))

(define-public crate-cn-id-card-0.1.1 (c (n "cn-id-card") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1mlfqw1bmxspk3d49xic4p6m9phasnaib66w5wn6j9m7i2nilv78")))

(define-public crate-cn-id-card-0.1.2 (c (n "cn-id-card") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1d4bj8m4l77f4aalbzg5ypb9gx1g9g1xg15va6hfc514jamyjir4")))

(define-public crate-cn-id-card-0.1.3 (c (n "cn-id-card") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "179jcdffzvpih9m6ilf3rlc45dayizv22h0gkf6fn49c02msm709") (f (quote (("region" "lazy_static") ("default" "region"))))))

(define-public crate-cn-id-card-0.1.5 (c (n "cn-id-card") (v "0.1.5") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.9") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1sgkdsjin70gss6bvnndhcbgr7gaq7ybbbs1ackadlxim30lp1wp") (f (quote (("region") ("default" "region"))))))

(define-public crate-cn-id-card-0.1.6 (c (n "cn-id-card") (v "0.1.6") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.9") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1xzd7vfbbdsgjl4kc2dxqzm3rnj10q12khjx6id8dy1rphfqr25v") (f (quote (("region") ("default" "region"))))))

(define-public crate-cn-id-card-0.1.7 (c (n "cn-id-card") (v "0.1.7") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.9") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0ky60mci1859qh0plyyqlwk27va2bblkb11nasvkhac4baw1770y") (f (quote (("region") ("default" "region"))))))

(define-public crate-cn-id-card-0.1.8 (c (n "cn-id-card") (v "0.1.8") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.9") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0j2snk0g7zxjppg8hrqkqqfba15hw4abrpw1hxc98hhb98r4rv17") (f (quote (("region") ("default" "region"))))))

