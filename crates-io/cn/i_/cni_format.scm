(define-module (crates-io cn i_ cni_format) #:use-module (crates-io))

(define-public crate-cni_format-0.4.3 (c (n "cni_format") (v "0.4.3") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0n517jy3382pfdscqrj1qdmijzg9w7b1yzdgh55kq01fi50xbgd5") (f (quote (("more-keys") ("ini") ("default" "ini"))))))

(define-public crate-cni_format-0.4.4 (c (n "cni_format") (v "0.4.4") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1n8hmn2sd254b9xf6n3pvmsbrspqy8cp0v1mjq5l6x5kphwqsa18") (f (quote (("more-keys") ("ini") ("default" "ini"))))))

(define-public crate-cni_format-0.5.0 (c (n "cni_format") (v "0.5.0") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "utf") (r "^0.1") (d #t) (k 2)))) (h "0aq0sa99s1hjwz2wj0mwih5ak0my2xrhgqryiv841l4zkkvxn441") (f (quote (("serializer") ("more-keys") ("ini") ("default" "api") ("api"))))))

(define-public crate-cni_format-0.5.1 (c (n "cni_format") (v "0.5.1") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "utf") (r "^0.1") (d #t) (k 2)))) (h "1ki138mh61bii1cyv5vh847f9b4gnhk85fabr5f4nn4jjv1sirvz") (f (quote (("serializer") ("more-keys") ("ini") ("default" "api") ("api"))))))

(define-public crate-cni_format-0.6.0 (c (n "cni_format") (v "0.6.0") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1w3v3h53p2662yh7fxh7q2y9xbqzw3p455llfim72f431s7ig9jr") (f (quote (("serializer") ("default" "api") ("api"))))))

(define-public crate-cni_format-0.6.1 (c (n "cni_format") (v "0.6.1") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1y6hfc0rziyhp2db6502h62f0j3igq6rzp3bfjx2yifh3fm7r9g6") (f (quote (("serializer") ("default" "api") ("api"))))))

