(define-module (crates-io cn fg cnfgen) #:use-module (crates-io))

(define-public crate-cnfgen-0.2.1 (c (n "cnfgen") (v "0.2.1") (d (list (d (n "generic-array") (r "^0.14") (d #t) (k 0)) (d (n "itoap") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0qbjdkz277fja9x0y1lvi98i231ym6hlcccxby5fi303a9w5d4w0")))

(define-public crate-cnfgen-0.2.2 (c (n "cnfgen") (v "0.2.2") (d (list (d (n "generic-array") (r "^0.14") (d #t) (k 0)) (d (n "itoap") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0k60nj9ayipzj2bxw6xamrr48lgx512k0sk6z096xccc562q6yg3")))

(define-public crate-cnfgen-0.3.0 (c (n "cnfgen") (v "0.3.0") (d (list (d (n "generic-array") (r "^0.14") (d #t) (k 0)) (d (n "itoap") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0hqvhzbf14z2pav6l98psnsqlnlwrhiqhgvjjz4bdx8biarcd84l")))

(define-public crate-cnfgen-0.3.1 (c (n "cnfgen") (v "0.3.1") (d (list (d (n "generic-array") (r "^0.14") (d #t) (k 0)) (d (n "itoap") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0q39mdz84fdivw675afqv39gff2jfbgkhwy2qfy1c3hnv1r9h8ad")))

(define-public crate-cnfgen-0.3.2 (c (n "cnfgen") (v "0.3.2") (d (list (d (n "generic-array") (r "^0.14") (d #t) (k 0)) (d (n "itoap") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0b0bxpanxjdap4kp8savaxzkia3n7ag3l72xcr805s0z5bykwrbf")))

(define-public crate-cnfgen-0.3.3 (c (n "cnfgen") (v "0.3.3") (d (list (d (n "generic-array") (r "^0.14") (d #t) (k 0)) (d (n "itoap") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0c47rh90pyyyzcbdfavk0zgb8knd6qhvdh17y3n5zbl1ybgx2vqq")))

