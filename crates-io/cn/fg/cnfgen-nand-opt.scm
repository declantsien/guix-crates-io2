(define-module (crates-io cn fg cnfgen-nand-opt) #:use-module (crates-io))

(define-public crate-cnfgen-nand-opt-0.1.0 (c (n "cnfgen-nand-opt") (v "0.1.0") (d (list (d (n "cnfgen") (r "^0.2.1") (d #t) (k 0)) (d (n "exec-sat") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "00j0ba8fppm7dlz57k9xn5jwd2wydavjdip650syd4bfcgsgixl8")))

(define-public crate-cnfgen-nand-opt-0.1.1 (c (n "cnfgen-nand-opt") (v "0.1.1") (d (list (d (n "cnfgen") (r "^0.2.1") (d #t) (k 0)) (d (n "exec-sat") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0phh40warnyzd0sfqxcqi7a738ccmfldqmf9ybpfndlsfzy0mc7m")))

