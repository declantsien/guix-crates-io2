(define-module (crates-io cn oc cnocr_rs) #:use-module (crates-io))

(define-public crate-cnocr_rs-0.1.0 (c (n "cnocr_rs") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "opencv") (r "^0.70.0") (d #t) (k 0)))) (h "0p7axibzb33ba87mx4kd299nyg3q9xq4lr1byg68jgl58412gxvz")))

(define-public crate-cnocr_rs-0.1.1 (c (n "cnocr_rs") (v "0.1.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "opencv") (r "^0.70.0") (d #t) (k 0)))) (h "0a62nqsz1rc47sznbfjzm62ws49xvsckrn5f0fllsn3kz9dvc3f9")))

(define-public crate-cnocr_rs-0.1.2 (c (n "cnocr_rs") (v "0.1.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "opencv") (r "^0.70.0") (d #t) (k 0)))) (h "094ir8icqvajpaxjm98prlqqqxgpw7fprxah3fd6hayfsh0wkgwj")))

(define-public crate-cnocr_rs-0.1.3 (c (n "cnocr_rs") (v "0.1.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "opencv") (r "^0.70.0") (d #t) (k 0)))) (h "09601p2n0n4aspys35i453ab5mckcs8k75flhnia0f5kf9cg1lbp")))

(define-public crate-cnocr_rs-0.1.4 (c (n "cnocr_rs") (v "0.1.4") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "opencv") (r "^0.70.0") (d #t) (k 0)))) (h "055lskpcf18x29cr2p5b55bqlhlrnc9kf1yvjp5llzpvqd1l7myi")))

