(define-module (crates-io cn tr cntr-fuse-sys) #:use-module (crates-io))

(define-public crate-cntr-fuse-sys-0.4.0 (c (n "cntr-fuse-sys") (v "0.4.0") (d (list (d (n "pkg-config") (r "^0.3.14") (d #t) (k 1)))) (h "06j9i3x6q7dqvarnx667657ydxd4lkndwxdjfx2bwjd6jll8kqwa") (f (quote (("libfuse")))) (l "fuse")))

(define-public crate-cntr-fuse-sys-0.4.1 (c (n "cntr-fuse-sys") (v "0.4.1") (d (list (d (n "pkg-config") (r "^0.3.14") (d #t) (k 1)))) (h "1y94q8haz4v3cavbbl338q3b069cnlddix0s0j2c7jgmc9ddmc5z") (f (quote (("libfuse")))) (l "fuse")))

