(define-module (crates-io cn tr cntrlr) #:use-module (crates-io))

(define-public crate-cntrlr-0.1.0 (c (n "cntrlr") (v "0.1.0") (d (list (d (n "bit_field") (r "^0.10.1") (d #t) (k 0)) (d (n "cntrlr-build") (r "^0.1.0") (d #t) (k 1)) (d (n "cntrlr-macros") (r "^0.1.0") (d #t) (k 0)))) (h "0pzay2wq9gpwvjnivfg7yzycx5myh5cdk8gw5dcl6gik5yh06rf4") (f (quote (("doc-cfg") ("default" "doc-cfg"))))))

