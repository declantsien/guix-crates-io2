(define-module (crates-io cn nk cnnks) #:use-module (crates-io))

(define-public crate-cnnks-0.1.0 (c (n "cnnks") (v "0.1.0") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1igxyikcp1w2adx1r795z8v9x3pfjicsdras1ggq6dymdrp54nb2") (y #t)))

(define-public crate-cnnks-0.1.1 (c (n "cnnks") (v "0.1.1") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0xk99rf1jc0yn2kmws3x547v4pawqnicbwrjnx1yb2cjybrxn7cn") (y #t)))

