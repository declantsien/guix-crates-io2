(define-module (crates-io an -r an-rope) #:use-module (crates-io))

(define-public crate-an-rope-0.0.1 (c (n "an-rope") (v "0.0.1") (d (list (d (n "tendril") (r "^0.2.3") (o #t) (d #t) (k 0)))) (h "155nhcfdyv0y39p9h2zp41v7nqfg6fkd0sfy029q1sw6pvrldymp") (f (quote (("with_tendrils" "tendril") ("rebalance") ("default"))))))

(define-public crate-an-rope-0.0.2 (c (n "an-rope") (v "0.0.2") (d (list (d (n "quickcheck") (r "^0.3") (d #t) (k 2)) (d (n "tendril") (r "^0.2.3") (o #t) (d #t) (k 0)))) (h "1698236c2zzaj5ya2ngw819i4qchl6i29h21zw6pkd9cvzq7y368") (f (quote (("unstable") ("rebalance") ("default"))))))

(define-public crate-an-rope-0.0.3 (c (n "an-rope") (v "0.0.3") (d (list (d (n "quickcheck") (r "^0.3") (d #t) (k 2)) (d (n "tendril") (r "^0.2.3") (o #t) (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.0.1") (d #t) (k 0)))) (h "0gl68q6j8xzrbmp3aygrqhp48vjaaz20n1qvydwawl0gd2qk7gaw") (f (quote (("unstable") ("rebalance") ("default"))))))

(define-public crate-an-rope-0.1.2 (c (n "an-rope") (v "0.1.2") (d (list (d (n "clippy") (r "^0.0.106") (o #t) (d #t) (k 0)) (d (n "macro-attr") (r "^0.2.0") (d #t) (k 0)) (d (n "newtype_derive") (r "^0.1.6") (d #t) (k 0)) (d (n "quickcheck") (r "^0.3") (d #t) (k 2)) (d (n "tendril") (r "^0.2.3") (o #t) (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.0.1") (d #t) (k 0)))) (h "1rpz0d6dkhaxi3r7x42bvg98xhckdh15bk09xixqh6g3kw7j09m9") (f (quote (("unstable") ("rebalance") ("default"))))))

(define-public crate-an-rope-0.2.0 (c (n "an-rope") (v "0.2.0") (d (list (d (n "clippy") (r "^0.0.106") (o #t) (d #t) (k 0)) (d (n "macro-attr") (r "^0.2.0") (d #t) (k 0)) (d (n "newtype_derive") (r "^0.1.6") (d #t) (k 0)) (d (n "quickcheck") (r "^0.3") (d #t) (k 2)) (d (n "tendril") (r "^0.2.3") (o #t) (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.0.1") (d #t) (k 0)))) (h "0cb3fj1ijfbq2dwb98pv1qz5x4ggzcfbwfbn05zwvbg9c5f8qdwj") (f (quote (("unstable") ("rebalance") ("default") ("atomic"))))))

(define-public crate-an-rope-0.3.0 (c (n "an-rope") (v "0.3.0") (d (list (d (n "clippy") (r "^0.0.106") (o #t) (d #t) (k 0)) (d (n "macro-attr") (r "^0.2.0") (d #t) (k 0)) (d (n "newtype_derive") (r "^0.1.6") (d #t) (k 0)) (d (n "quickcheck") (r "^0.3") (d #t) (k 2)) (d (n "tendril") (r "^0.2.3") (o #t) (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.0.1") (d #t) (k 0)))) (h "1y26cqnm4cgp1cs8rv5vwnwqrcan0vbcap40f1007vfp0kw9b6vg") (f (quote (("unstable") ("rebalance") ("default") ("atomic"))))))

(define-public crate-an-rope-0.3.1 (c (n "an-rope") (v "0.3.1") (d (list (d (n "clippy") (r "^0.0.106") (o #t) (d #t) (k 0)) (d (n "macro-attr") (r "^0.2.0") (d #t) (k 0)) (d (n "newtype_derive") (r "^0.1.6") (d #t) (k 0)) (d (n "quickcheck") (r "^0.3") (d #t) (k 2)) (d (n "tendril") (r "^0.2.3") (o #t) (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.0.1") (d #t) (k 0)))) (h "01g140mf5n2rjvcnahh9n8j27yriq7y0lpvsjbdfm65hcmpzlp9r") (f (quote (("unstable") ("rebalance") ("default") ("atomic"))))))

