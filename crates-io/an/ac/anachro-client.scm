(define-module (crates-io an ac anachro-client) #:use-module (crates-io))

(define-public crate-anachro-client-0.1.0 (c (n "anachro-client") (v "0.1.0") (d (list (d (n "anachro-icd") (r "^0.1.2") (d #t) (k 0)) (d (n "postcard") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (k 0)))) (h "0kbgqkv4jizvpqx4is6mdjqdkay8dpp7jv7avsaxn48ib0vn2ygq")))

