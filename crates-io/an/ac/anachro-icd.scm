(define-module (crates-io an ac anachro-icd) #:use-module (crates-io))

(define-public crate-anachro-icd-0.1.0 (c (n "anachro-icd") (v "0.1.0") (d (list (d (n "heapless") (r "^0.5.5") (d #t) (k 0)) (d (n "postcard") (r "^0.5.1") (k 2)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (k 0)))) (h "1xj86prnkiaai2d62pplzwaj555llj0yh1j55pbdykgzm492p0h6") (f (quote (("std" "postcard/use-std"))))))

(define-public crate-anachro-icd-0.1.1 (c (n "anachro-icd") (v "0.1.1") (d (list (d (n "heapless") (r "^0.5.5") (d #t) (k 0)) (d (n "postcard") (r "^0.5.1") (k 2)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (k 0)))) (h "162pdy37vgx5kddrc7x5kpl7bi1qsj08f1ibmsrvv5nd2c83cc5a") (f (quote (("std" "postcard/use-std"))))))

(define-public crate-anachro-icd-0.1.2 (c (n "anachro-icd") (v "0.1.2") (d (list (d (n "heapless") (r "^0.5.5") (d #t) (k 0)) (d (n "postcard") (r "^0.5.1") (k 2)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (k 0)))) (h "1dgwfqhp408mards9g003y2fyx57k1vp83jgsr35m0r1dic0a7ch") (f (quote (("std" "postcard/use-std"))))))

(define-public crate-anachro-icd-0.1.3 (c (n "anachro-icd") (v "0.1.3") (d (list (d (n "heapless") (r "^0.5.5") (d #t) (k 0)) (d (n "postcard") (r "^0.5.1") (k 2)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (k 0)))) (h "0jdd4jav1bb9s10pfrmnyzhjqryz3zkcn3vxx02wl0zafhnq6asy") (f (quote (("std" "postcard/use-std"))))))

