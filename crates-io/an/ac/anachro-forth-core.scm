(define-module (crates-io an ac anachro-forth-core) #:use-module (crates-io))

(define-public crate-anachro-forth-core-0.0.1 (c (n "anachro-forth-core") (v "0.0.1") (d (list (d (n "heapless") (r "^0.7.8") (d #t) (k 0)) (d (n "postcard") (r "^0.7.2") (d #t) (k 0)) (d (n "postcard") (r "^0.7.2") (f (quote ("use-std"))) (d #t) (k 2)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive" "std"))) (k 2)))) (h "0piicdhjwy5zjllp1jn2i280416n97vmmm0nsdaski7zrw51yqjg") (f (quote (("std" "serde/std") ("default"))))))

(define-public crate-anachro-forth-core-0.0.2 (c (n "anachro-forth-core") (v "0.0.2") (d (list (d (n "heapless") (r "^0.7.8") (d #t) (k 0)) (d (n "postcard") (r "^0.7.2") (d #t) (k 0)) (d (n "postcard") (r "^0.7.2") (f (quote ("use-std"))) (d #t) (k 2)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive" "std"))) (k 2)))) (h "0x5byy8g4f6932sm50iazhm2zwijr2cj6p1zxmzy10m08g7plajc") (f (quote (("std" "serde/std") ("default"))))))

