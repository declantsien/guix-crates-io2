(define-module (crates-io an ac anacrolix-libc) #:use-module (crates-io))

(define-public crate-anacrolix-libc-0.2.151 (c (n "anacrolix-libc") (v "0.2.151") (d (list (d (n "rustc-std-workspace-core") (r "^1.0.0") (o #t) (d #t) (k 0)))) (h "0svpn39kgw7ar450ba3yd9mvkc8ll0vxwd351s7z7v171g1a7yid") (f (quote (("std") ("rustc-dep-of-std" "align" "rustc-std-workspace-core") ("extra_traits") ("default" "std") ("const-extern-fn") ("align")))) (r "1.71.0")))

