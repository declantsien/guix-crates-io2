(define-module (crates-io an as anaso_sdk) #:use-module (crates-io))

(define-public crate-anaso_sdk-0.1.0 (c (n "anaso_sdk") (v "0.1.0") (h "016p6ncb7qx9vfh83vzpy9by6mrwql5x13if9idq4kb6q91fbrcm")))

(define-public crate-anaso_sdk-0.2.0 (c (n "anaso_sdk") (v "0.2.0") (d (list (d (n "bitflags") (r "^2.4.1") (f (quote ("serde"))) (o #t) (d #t) (k 0)) (d (n "bs58") (r "^0.5.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.189") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "serde-email") (r "^3.0.0") (o #t) (d #t) (k 0)))) (h "06b29inj1qgc1390618vdscqag6nmjalrz7llhrr0rf57fzvz1q3") (f (quote (("models" "bitflags" "serde-email" "chrono") ("default" "client" "models") ("client"))))))

