(define-module (crates-io an as anas) #:use-module (crates-io))

(define-public crate-anas-0.1.0 (c (n "anas") (v "0.1.0") (h "0ghz248a9h69nr2yam2b5m3ck4mmgdslmjxxig2ig1c29g0lh195")))

(define-public crate-anas-0.1.1 (c (n "anas") (v "0.1.1") (h "0ryw2i5jphhs11hay552lwv12pxd2r43fbbvy9v6s62dyf6a5h0y")))

