(define-module (crates-io an it anitable) #:use-module (crates-io))

(define-public crate-anitable-0.1.0 (c (n "anitable") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "mockito") (r "^0.22") (d #t) (k 2)) (d (n "num_enum") (r "^0.4.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.0-alpha.2") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "= 0.2.0-alpha.6") (d #t) (k 2)))) (h "09si3f6pfwmykgqqy0b1mnycviiklhas70nhh8r63c7p5hhb9hwi")))

