(define-module (crates-io an it anitomy-sys) #:use-module (crates-io))

(define-public crate-anitomy-sys-0.1.0 (c (n "anitomy-sys") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1rw1ydxzcx86bjfk3rzdj3bpx0gyc0h4znagbd62k9ca6w21j21w")))

(define-public crate-anitomy-sys-0.1.1 (c (n "anitomy-sys") (v "0.1.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1bkqvnfmh0ycbl9k87dxsdlglwfim07g01bf0cc6c2yyhxm49wgg")))

(define-public crate-anitomy-sys-0.1.2 (c (n "anitomy-sys") (v "0.1.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1sih5q50f57rwg57jpcw7lagh55aid9jcvwqj0gykmmhy7gygbdi")))

(define-public crate-anitomy-sys-0.2.0 (c (n "anitomy-sys") (v "0.2.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1nqp7514mf333fvnnh1gc4xpdj85zpsgjkhz0g6jzsgm0qyy3aij")))

