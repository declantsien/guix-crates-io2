(define-module (crates-io an it anitable-tui) #:use-module (crates-io))

(define-public crate-anitable-tui-0.1.0 (c (n "anitable-tui") (v "0.1.0") (d (list (d (n "anitable") (r "^0.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "termion") (r "^1.5") (d #t) (k 0)) (d (n "tokio") (r "= 0.2.0-alpha.6") (d #t) (k 0)) (d (n "tui") (r "^0.7") (d #t) (k 0)))) (h "1agllqz9z73b3vq3c581cpqhsyyin35ckkin8cnlsf5bcvr2pjnp")))

