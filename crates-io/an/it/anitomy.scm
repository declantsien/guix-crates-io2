(define-module (crates-io an it anitomy) #:use-module (crates-io))

(define-public crate-anitomy-0.1.0 (c (n "anitomy") (v "0.1.0") (d (list (d (n "anitomy-sys") (r "^0.1") (d #t) (k 0)))) (h "0l65sy7gxj583lwmr0kaydm2i2wb4blza983rabr6bpqrwnyzgwk")))

(define-public crate-anitomy-0.1.1 (c (n "anitomy") (v "0.1.1") (d (list (d (n "anitomy-sys") (r "^0.1") (d #t) (k 0)))) (h "1gcmsv1y403c741b46kr20jrq5wh1apxqkj1n5551k3jjjca0xwg")))

(define-public crate-anitomy-0.1.2 (c (n "anitomy") (v "0.1.2") (d (list (d (n "anitomy-sys") (r "^0.1") (d #t) (k 0)))) (h "17aaqjj6d1qyab3xyga78d8ycnd80j41wb0qahll8fjdn77x5nx5")))

(define-public crate-anitomy-0.2.0 (c (n "anitomy") (v "0.2.0") (d (list (d (n "anitomy-sys") (r "^0.2") (d #t) (k 0)))) (h "19qvadbm9zm7r19v268pn0qg3xd7ijxgv3b63g7fqjr3gpyg0l7c")))

