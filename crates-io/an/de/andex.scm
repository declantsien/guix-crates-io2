(define-module (crates-io an de andex) #:use-module (crates-io))

(define-public crate-andex-0.0.3 (c (n "andex") (v "0.0.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)))) (h "0q6xhrbc98rz4982jcvc3g5i0r29kzv01dkdks0664whdrj7grsh")))

(define-public crate-andex-0.0.4 (c (n "andex") (v "0.0.4") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)))) (h "16a6vc6r8lzmd7k16njwggcipq4c1ms1njndqv2y4ddiawbbvfdd")))

(define-public crate-andex-0.0.5 (c (n "andex") (v "0.0.5") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)))) (h "094ai65i3mvbcncircff160yppijcysjcsj2idvvavw9jhcdc3xs")))

(define-public crate-andex-0.0.6 (c (n "andex") (v "0.0.6") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)))) (h "0bl4ahay0868bqynw6xgxwv6wsv1hrqxgc0bxnm24lm81wsm3hmv")))

(define-public crate-andex-0.0.7 (c (n "andex") (v "0.0.7") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)))) (h "1fzvfvhg95s98r41dpijkpzqi4xw0qmarz4vikrzxmz49sbx3nnn")))

(define-public crate-andex-0.0.8 (c (n "andex") (v "0.0.8") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)))) (h "1dy7jzm2nbhm3xfzi4hd070lk9zakp6vmkd6c1hs841la04b51j4")))

(define-public crate-andex-0.0.9 (c (n "andex") (v "0.0.9") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)))) (h "1fq685akqrnd3x5lnvyyr9gnv9ssqslpmsp13mw185nyx1kpd554")))

(define-public crate-andex-0.0.10 (c (n "andex") (v "0.0.10") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)))) (h "155rixq09ywzb4zy5xxrx7lyp0cp456319hmpm326xq4yd5f6fqq")))

(define-public crate-andex-0.0.11 (c (n "andex") (v "0.0.11") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)))) (h "053y2vqdkqzr6xhddzcwkwl7vdpgh8xv387969hmgfhzxfvcyj8g")))

(define-public crate-andex-0.0.12 (c (n "andex") (v "0.0.12") (d (list (d (n "anyhow") (r "^1.0.42") (d #t) (k 2)))) (h "1fr4v9wgrnpx1276sgkgaiwjm2524bnv4px3qvc1skd2bk6mpxy5")))

(define-public crate-andex-0.0.13 (c (n "andex") (v "0.0.13") (d (list (d (n "anyhow") (r "^1.0.42") (d #t) (k 2)))) (h "1hm9skdaq0jdz0jd30gw286xsy3klr707k1ijdlxsclvdmzgmwpg")))

(define-public crate-andex-0.0.14 (c (n "andex") (v "0.0.14") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 2)))) (h "1yhkg8jcrrqza8wpzw7qyfiqif6ynvmykplgn6l7xkaznl92yjpy")))

