(define-module (crates-io an ki ankirs) #:use-module (crates-io))

(define-public crate-ankirs-0.1.0 (c (n "ankirs") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.71") (d #t) (k 0)) (d (n "bcrypt") (r "^0.15.0") (d #t) (k 0)) (d (n "clap") (r "^4.3.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 0)) (d (n "sqlx") (r "^0.7.0") (f (quote ("sqlite" "json" "time" "macros" "runtime-tokio"))) (d #t) (k 0)) (d (n "strum") (r "^0.25.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.29.1") (f (quote ("full"))) (d #t) (k 0)))) (h "03jiis24405ws8pxn66nqn4a5wz71bzvk9f5mmsl310b3f8i7qr6")))

