(define-module (crates-io an ki anki-status) #:use-module (crates-io))

(define-public crate-anki-status-0.1.0 (c (n "anki-status") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11.8") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.133") (f (quote ("derive"))) (d #t) (k 0)) (d (n "time") (r "^0.3.5") (f (quote ("local-offset"))) (d #t) (k 0)))) (h "1mhk8k5qff6iv9r5g5kqrs7zaal1a8kjnv5ibai60dncr9fk8wcg")))

(define-public crate-anki-status-0.2.0 (c (n "anki-status") (v "0.2.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "time") (r "^0.3") (f (quote ("local-offset"))) (d #t) (k 0)))) (h "0q4v3iil42594ckfhlgvc985kb8xdvpy9byf7rhhk9h9vclmrvqv")))

(define-public crate-anki-status-0.2.1 (c (n "anki-status") (v "0.2.1") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "time") (r "^0.3") (f (quote ("local-offset"))) (d #t) (k 0)))) (h "0pv9h2mv3k07dh0z17vxymbg396pn974ngxh8a1y9f8zjflgqkw6")))

(define-public crate-anki-status-0.2.2 (c (n "anki-status") (v "0.2.2") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "time") (r "^0.3") (f (quote ("local-offset"))) (d #t) (k 0)))) (h "0fz35qzgx5311vmvd5xcacgmn09qf58asac6rj54dqjcp7nsk3bl")))

(define-public crate-anki-status-0.2.3 (c (n "anki-status") (v "0.2.3") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "time") (r "^0.3") (f (quote ("local-offset"))) (d #t) (k 0)))) (h "0n152zg4h65liwbp6nd7mdwql4cgn4sw9wxn2w6qjnqq268aafld")))

