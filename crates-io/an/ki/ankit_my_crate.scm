(define-module (crates-io an ki ankit_my_crate) #:use-module (crates-io))

(define-public crate-ankit_my_crate-0.1.0 (c (n "ankit_my_crate") (v "0.1.0") (h "0p7gnbwlcmvcympai8qa21q6vsbvkpkpzdd7c4lmnn6r6phnyjmg")))

(define-public crate-ankit_my_crate-0.1.1 (c (n "ankit_my_crate") (v "0.1.1") (h "19kpvdp0i74fn2ps0wjyk5kcqc3s6fq5nzr2nh0z92f9xnnpacbc")))

