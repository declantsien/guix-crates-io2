(define-module (crates-io an ki ankiconnect) #:use-module (crates-io))

(define-public crate-ankiconnect-0.1.0 (c (n "ankiconnect") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.97") (d #t) (k 0)))) (h "139v9dfzmw1agffi7arzvwi4fbl0bb3f1xinwnx8a0jpla8ypa2b")))

(define-public crate-ankiconnect-0.2.0 (c (n "ankiconnect") (v "0.2.0") (d (list (d (n "serde") (r "^1.0.164") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ureq") (r "^2.7.1") (f (quote ("json"))) (d #t) (k 0)))) (h "1g6d1ghls8f8017pw1q0r0ns34ays4dsrkbls03j52ldd564i1jf")))

