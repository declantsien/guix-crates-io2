(define-module (crates-io an ki ankidb) #:use-module (crates-io))

(define-public crate-ankidb-0.1.0 (c (n "ankidb") (v "0.1.0") (d (list (d (n "rusqlite") (r "^0.29.0") (f (quote ("trace" "functions" "collation" "bundled"))) (d #t) (k 0)) (d (n "unicase") (r "=2.6.0") (d #t) (k 0)))) (h "1p32dh0yy1mgmqk7z4chawjgpndh6sfqnp5czms0cd3vpw6han7i") (r "1.65")))

(define-public crate-ankidb-0.1.1 (c (n "ankidb") (v "0.1.1") (d (list (d (n "rusqlite") (r "^0.29.0") (f (quote ("trace" "functions" "collation" "bundled"))) (d #t) (k 0)) (d (n "unicase") (r "=2.6.0") (d #t) (k 0)))) (h "1x90965bihng7r72zaqnagknyrfi0y3qc48vgybx7687z0hxql21") (r "1.65")))

(define-public crate-ankidb-0.2.0 (c (n "ankidb") (v "0.2.0") (d (list (d (n "rusqlite") (r "^0.29.0") (f (quote ("trace" "functions" "collation" "bundled"))) (d #t) (k 0)) (d (n "sea-query") (r "^0.30.1") (f (quote ("backend-sqlite" "derive"))) (k 0)) (d (n "sea-query-rusqlite") (r "^0.4.0") (d #t) (k 0)) (d (n "unicase") (r "=2.6.0") (d #t) (k 0)))) (h "0ciqf2jigyx35mxviqpwjg6n4w72c3i2xvj02abnb6y0dfc40bfh") (r "1.65")))

(define-public crate-ankidb-0.2.1 (c (n "ankidb") (v "0.2.1") (d (list (d (n "postgres-types") (r "^0.2.5") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "rusqlite") (r "^0.29.0") (f (quote ("trace" "functions" "collation" "bundled"))) (d #t) (k 0)) (d (n "sea-query") (r "^0.30.1") (f (quote ("backend-sqlite" "derive"))) (k 0)) (d (n "sea-query-rusqlite") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.192") (o #t) (d #t) (k 0)) (d (n "unicase") (r "=2.6.0") (d #t) (k 0)))) (h "0nlf7bmx2j33k0ldz2x797r7bvjn3jsnvcbwq0vh60fp4fbcm6h7") (r "1.65")))

(define-public crate-ankidb-0.3.0 (c (n "ankidb") (v "0.3.0") (d (list (d (n "postgres-types") (r "^0.2.6") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "rusqlite") (r "^0.29.0") (f (quote ("trace" "functions" "collation" "bundled"))) (d #t) (k 0)) (d (n "sea-query") (r "^0.30.5") (f (quote ("backend-sqlite" "derive"))) (k 0)) (d (n "sea-query-rusqlite") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (o #t) (d #t) (k 0)) (d (n "unicase") (r "=2.6.0") (d #t) (k 0)))) (h "1w80vhql6lqwaxb7b8q8wyvhay6a4p3y95k2alsif21fg618l5r9") (r "1.65")))

