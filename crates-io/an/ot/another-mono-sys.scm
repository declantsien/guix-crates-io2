(define-module (crates-io an ot another-mono-sys) #:use-module (crates-io))

(define-public crate-another-mono-sys-0.1.0 (c (n "another-mono-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.73") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.25") (d #t) (k 1)))) (h "14vk3ddgyi1z7pbizl8ckksbsi244nbpinjl30r6yw9bgfyh2007")))

(define-public crate-another-mono-sys-0.1.1 (c (n "another-mono-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.73") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.25") (d #t) (k 1)))) (h "15abc7azdnsdj3lrnhipk376f0b4hl78lh965n48dwppsh3asqdz") (l "mono-2")))

(define-public crate-another-mono-sys-0.1.2 (c (n "another-mono-sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.73") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.25") (d #t) (k 1)))) (h "12y7m170r3v16fszl3jpkf4psd9b2rp70asyylqx5mh5frsylwlg") (l "mono-2.0")))

