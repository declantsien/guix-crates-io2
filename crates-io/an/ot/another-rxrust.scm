(define-module (crates-io an ot another-rxrust) #:use-module (crates-io))

(define-public crate-another-rxrust-0.0.10 (c (n "another-rxrust") (v "0.0.10") (d (list (d (n "anyhow") (r "^1") (f (quote ("backtrace"))) (d #t) (k 0)))) (h "1byi0sjc0igdybpmh01sz7kwx13dljq85c2552hzcn8yrxrpz1ba")))

(define-public crate-another-rxrust-0.0.11 (c (n "another-rxrust") (v "0.0.11") (d (list (d (n "anyhow") (r "^1") (f (quote ("backtrace"))) (d #t) (k 0)))) (h "0r94jad243ab5jhhldd5n6f6g7b6gcqgz0vjnsbzxxam4wiycdgr")))

(define-public crate-another-rxrust-0.0.12 (c (n "another-rxrust") (v "0.0.12") (d (list (d (n "anyhow") (r "^1") (f (quote ("backtrace"))) (d #t) (k 0)))) (h "0zp03ags3rsrc1fkkzibgjyrn7adhn6vggnfaq2cns79rx7ax2im")))

(define-public crate-another-rxrust-0.0.13 (c (n "another-rxrust") (v "0.0.13") (d (list (d (n "anyhow") (r "^1") (f (quote ("backtrace"))) (d #t) (k 0)))) (h "0niqn386idb41lzzmqqzp752mcrfxlpyddvanbmds69lwdbc1fnw")))

(define-public crate-another-rxrust-0.0.14 (c (n "another-rxrust") (v "0.0.14") (d (list (d (n "anyhow") (r "^1") (f (quote ("backtrace"))) (d #t) (k 0)))) (h "10b07ylbd278gn6cqkjx0jci83ik1ykf3rp2d03z21krxhrlzz5w")))

(define-public crate-another-rxrust-0.0.15 (c (n "another-rxrust") (v "0.0.15") (d (list (d (n "anyhow") (r "^1") (f (quote ("backtrace"))) (d #t) (k 0)))) (h "0g9z1ld31hi4wqnnxc2ajgl3jq20kxh7mh9y31qz9c4h7yzp92s6")))

(define-public crate-another-rxrust-0.0.17 (c (n "another-rxrust") (v "0.0.17") (d (list (d (n "anyhow") (r "^1") (o #t) (d #t) (k 0)) (d (n "anyhow") (r "^1") (f (quote ("backtrace"))) (d #t) (k 2)))) (h "1gdcvnvxqh4csp66y32c6dqi784gn55hyrafrnnb8y5fj9firqgx") (s 2) (e (quote (("anyhow" "dep:anyhow"))))))

(define-public crate-another-rxrust-0.0.18 (c (n "another-rxrust") (v "0.0.18") (d (list (d (n "anyhow") (r "^1") (o #t) (d #t) (k 0)) (d (n "anyhow") (r "^1") (f (quote ("backtrace"))) (d #t) (k 2)))) (h "1bhpmmv288j3ldg9m2rzbaiczigczm31n5zxsp3grfrj06ygx9gj") (s 2) (e (quote (("anyhow" "dep:anyhow"))))))

(define-public crate-another-rxrust-0.0.19 (c (n "another-rxrust") (v "0.0.19") (d (list (d (n "anyhow") (r "^1") (o #t) (d #t) (k 0)) (d (n "anyhow") (r "^1") (f (quote ("backtrace"))) (d #t) (k 2)))) (h "1gwj6crcywriiw0j9fwvfr17z2aky3vmzbqyvi9z28mz80ji14ls") (s 2) (e (quote (("anyhow" "dep:anyhow"))))))

(define-public crate-another-rxrust-0.0.20 (c (n "another-rxrust") (v "0.0.20") (d (list (d (n "anyhow") (r "^1") (o #t) (d #t) (k 0)) (d (n "anyhow") (r "^1") (f (quote ("backtrace"))) (d #t) (k 2)))) (h "1zbr05hkp5zs0f22qzvvzdzgzdr6ac3d20rfkqd4bszmxjh8hj67") (s 2) (e (quote (("anyhow" "dep:anyhow"))))))

(define-public crate-another-rxrust-0.0.21 (c (n "another-rxrust") (v "0.0.21") (d (list (d (n "anyhow") (r "^1") (o #t) (d #t) (k 0)) (d (n "anyhow") (r "^1") (f (quote ("backtrace"))) (d #t) (k 2)))) (h "1gr0qs3bni0wpq5v6pynnvix9vljq1wxk86bapfwnimxa962j3aj") (s 2) (e (quote (("anyhow" "dep:anyhow"))))))

(define-public crate-another-rxrust-0.0.22 (c (n "another-rxrust") (v "0.0.22") (d (list (d (n "anyhow") (r "^1") (o #t) (d #t) (k 0)) (d (n "anyhow") (r "^1") (f (quote ("backtrace"))) (d #t) (k 2)))) (h "0745z121ifl828q9msm2qbgqzp23501wyz0pv81vy15zg6fxy0zx") (s 2) (e (quote (("anyhow" "dep:anyhow"))))))

(define-public crate-another-rxrust-0.0.23 (c (n "another-rxrust") (v "0.0.23") (d (list (d (n "anyhow") (r "^1") (o #t) (d #t) (k 0)) (d (n "anyhow") (r "^1") (f (quote ("backtrace"))) (d #t) (k 2)))) (h "1w0jbkxfgkhs57c9yfcwjfcplbsxbk9a0nackbi45i3qp4wqpl2j") (s 2) (e (quote (("anyhow" "dep:anyhow"))))))

(define-public crate-another-rxrust-0.0.24 (c (n "another-rxrust") (v "0.0.24") (d (list (d (n "anyhow") (r "^1") (o #t) (d #t) (k 0)) (d (n "anyhow") (r "^1") (f (quote ("backtrace"))) (d #t) (k 2)))) (h "12jhjh28nkd767fab7l2qjfyppa91bqmgsaivmxjmzmrnfjfppsx") (s 2) (e (quote (("anyhow" "dep:anyhow"))))))

(define-public crate-another-rxrust-0.0.25 (c (n "another-rxrust") (v "0.0.25") (d (list (d (n "anyhow") (r "^1") (o #t) (d #t) (k 0)) (d (n "anyhow") (r "^1") (f (quote ("backtrace"))) (d #t) (k 2)))) (h "020nzasyqlyxil48xzb22cark1g18hl6md9fb0qdqmdq6fyaas75") (s 2) (e (quote (("anyhow" "dep:anyhow"))))))

(define-public crate-another-rxrust-0.0.26 (c (n "another-rxrust") (v "0.0.26") (d (list (d (n "anyhow") (r "^1") (o #t) (d #t) (k 0)) (d (n "anyhow") (r "^1") (f (quote ("backtrace"))) (d #t) (k 2)))) (h "05b6gh45zn0zbn267s7p4b2dh7l1v3m5jdaamsmbysmzbbvba9s0") (s 2) (e (quote (("anyhow" "dep:anyhow"))))))

(define-public crate-another-rxrust-0.0.27 (c (n "another-rxrust") (v "0.0.27") (d (list (d (n "anyhow") (r "^1") (o #t) (d #t) (k 0)) (d (n "anyhow") (r "^1") (f (quote ("backtrace"))) (d #t) (k 2)))) (h "1fzql1sr4hfhbldm1xsmfj878g1hyhsyr7k8mw79rmprm67p5n91") (s 2) (e (quote (("anyhow" "dep:anyhow"))))))

(define-public crate-another-rxrust-0.0.28 (c (n "another-rxrust") (v "0.0.28") (d (list (d (n "anyhow") (r "^1") (o #t) (d #t) (k 0)) (d (n "anyhow") (r "^1") (f (quote ("backtrace"))) (d #t) (k 2)))) (h "0j28xx56p8r42qypw03zpgb88car54gqcpfqp7p2zjz6c64ns1yc") (s 2) (e (quote (("anyhow" "dep:anyhow"))))))

(define-public crate-another-rxrust-0.0.29 (c (n "another-rxrust") (v "0.0.29") (d (list (d (n "anyhow") (r "^1") (o #t) (d #t) (k 0)) (d (n "anyhow") (r "^1") (f (quote ("backtrace"))) (d #t) (k 2)))) (h "0sasriwhcjbb9zfyqzp8cfhnnp838rq9dfh0i4zz8sqfly6q4pr1") (s 2) (e (quote (("anyhow" "dep:anyhow"))))))

(define-public crate-another-rxrust-0.0.30 (c (n "another-rxrust") (v "0.0.30") (d (list (d (n "anyhow") (r "^1") (o #t) (d #t) (k 0)) (d (n "anyhow") (r "^1") (f (quote ("backtrace"))) (d #t) (k 2)))) (h "1136xdhhgdqyj0rni2ryyg88984dzncli0giw21qfk5lvbi2zz76") (s 2) (e (quote (("anyhow" "dep:anyhow"))))))

(define-public crate-another-rxrust-0.0.31 (c (n "another-rxrust") (v "0.0.31") (d (list (d (n "anyhow") (r "^1") (o #t) (d #t) (k 0)) (d (n "anyhow") (r "^1") (f (quote ("backtrace"))) (d #t) (k 2)))) (h "13f3kd0z77llz4baikgh9z3c9w3cw7g5vbb7wjx3hnkpafdymnnb") (s 2) (e (quote (("anyhow" "dep:anyhow"))))))

(define-public crate-another-rxrust-0.0.32 (c (n "another-rxrust") (v "0.0.32") (d (list (d (n "anyhow") (r "^1") (o #t) (d #t) (k 0)) (d (n "anyhow") (r "^1") (f (quote ("backtrace"))) (d #t) (k 2)))) (h "1ypqd298iq270i8l6p89ivkiy422knjpkgsmvyslig5a03gz2x1x") (s 2) (e (quote (("anyhow" "dep:anyhow"))))))

(define-public crate-another-rxrust-0.0.33 (c (n "another-rxrust") (v "0.0.33") (d (list (d (n "anyhow") (r "^1") (f (quote ("backtrace"))) (d #t) (k 2)))) (h "1j666s5n3awjq7203m0k88ijz2vlfjhilr2smcpa1qnck8k9x46r")))

(define-public crate-another-rxrust-0.0.34 (c (n "another-rxrust") (v "0.0.34") (d (list (d (n "anyhow") (r "^1") (f (quote ("backtrace"))) (d #t) (k 2)))) (h "1h4lb79hzzrfj89j8r1bcl6hrsjdx4q2ssh87kqixirr6jrqigkd")))

(define-public crate-another-rxrust-0.0.35 (c (n "another-rxrust") (v "0.0.35") (d (list (d (n "anyhow") (r "^1") (f (quote ("backtrace"))) (d #t) (k 2)) (d (n "tokio") (r "^1.27") (f (quote ("full"))) (d #t) (k 2)))) (h "0w9ja9y6428d2jjbzh6wxwqxvhv2plmff8knib8rqc6b7crmj601")))

(define-public crate-another-rxrust-0.0.36 (c (n "another-rxrust") (v "0.0.36") (d (list (d (n "anyhow") (r "^1") (f (quote ("backtrace"))) (d #t) (k 2)) (d (n "tokio") (r "^1.27") (f (quote ("full"))) (d #t) (k 2)) (d (n "wasm-bindgen") (r "^0.2.83") (o #t) (d #t) (k 0)) (d (n "web-sys") (r "^0.3.60") (f (quote ("Window"))) (o #t) (d #t) (k 0)))) (h "04jsyhqiyjh7c5yl07xa5az60cb682h05df94idwamr227j61sj3") (s 2) (e (quote (("web" "dep:wasm-bindgen" "dep:web-sys"))))))

(define-public crate-another-rxrust-0.0.37 (c (n "another-rxrust") (v "0.0.37") (d (list (d (n "anyhow") (r "^1") (f (quote ("backtrace"))) (d #t) (k 2)) (d (n "tokio") (r "^1.27") (f (quote ("full"))) (d #t) (k 2)) (d (n "wasm-bindgen") (r "^0.2.83") (o #t) (d #t) (k 0)) (d (n "web-sys") (r "^0.3.60") (f (quote ("Window"))) (o #t) (d #t) (k 0)))) (h "1712lpvjcvfppr68v8qqwknlncmfgz8q9rzjn0523f06hv92pi9n") (s 2) (e (quote (("web" "dep:wasm-bindgen" "dep:web-sys"))))))

(define-public crate-another-rxrust-0.0.38 (c (n "another-rxrust") (v "0.0.38") (d (list (d (n "anyhow") (r "^1") (f (quote ("backtrace"))) (d #t) (k 2)) (d (n "tokio") (r "^1.27") (f (quote ("full"))) (d #t) (k 2)) (d (n "wasm-bindgen") (r "^0.2.83") (o #t) (d #t) (k 0)) (d (n "web-sys") (r "^0.3.60") (f (quote ("Window"))) (o #t) (d #t) (k 0)))) (h "0hfcrjlgf7jcqjicdhk9laxjj1zzywarsbfv0cbh50vcb54zi6c0") (s 2) (e (quote (("web" "dep:wasm-bindgen" "dep:web-sys"))))))

(define-public crate-another-rxrust-0.0.39 (c (n "another-rxrust") (v "0.0.39") (d (list (d (n "anyhow") (r "^1") (f (quote ("backtrace"))) (d #t) (k 2)) (d (n "tokio") (r "^1.27") (f (quote ("full"))) (d #t) (k 2)) (d (n "wasm-bindgen") (r "^0.2.83") (o #t) (d #t) (k 0)) (d (n "web-sys") (r "^0.3.60") (f (quote ("Window"))) (o #t) (d #t) (k 0)))) (h "1klnvrdv3ww9ff2gjqnmg88jg37gnf4aw1ichspak6mdqdkxnqn1") (s 2) (e (quote (("web" "dep:wasm-bindgen" "dep:web-sys"))))))

(define-public crate-another-rxrust-0.0.40 (c (n "another-rxrust") (v "0.0.40") (d (list (d (n "anyhow") (r "^1") (f (quote ("backtrace"))) (d #t) (k 2)) (d (n "tokio") (r "^1.27") (f (quote ("full"))) (d #t) (k 2)) (d (n "wasm-bindgen") (r "^0.2.83") (o #t) (d #t) (k 0)) (d (n "web-sys") (r "^0.3.60") (f (quote ("Window"))) (o #t) (d #t) (k 0)))) (h "00whc62g5db4jsqn1mjf8b8lkqamksz0p7km3rzybbxyp36d5l9b") (s 2) (e (quote (("web" "dep:wasm-bindgen" "dep:web-sys"))))))

(define-public crate-another-rxrust-0.0.41 (c (n "another-rxrust") (v "0.0.41") (d (list (d (n "anyhow") (r "^1") (f (quote ("backtrace"))) (d #t) (k 2)) (d (n "tokio") (r "^1.27") (f (quote ("full"))) (d #t) (k 2)) (d (n "wasm-bindgen") (r "^0.2.83") (o #t) (d #t) (k 0)) (d (n "web-sys") (r "^0.3.60") (f (quote ("Window"))) (o #t) (d #t) (k 0)))) (h "1w9cwhr28bmabvdvyi64xv7rgijg2c0axb8nprpy7ywk74xiga2h") (s 2) (e (quote (("web" "dep:wasm-bindgen" "dep:web-sys"))))))

(define-public crate-another-rxrust-0.0.42 (c (n "another-rxrust") (v "0.0.42") (d (list (d (n "anyhow") (r "^1") (f (quote ("backtrace"))) (d #t) (k 2)) (d (n "tokio") (r "^1.27") (f (quote ("full"))) (d #t) (k 2)) (d (n "wasm-bindgen") (r "^0.2.83") (o #t) (d #t) (k 0)) (d (n "web-sys") (r "^0.3.60") (f (quote ("Window"))) (o #t) (d #t) (k 0)))) (h "15xrvxg89syzb75pi64aaxrcrwbcshg6lw1gi904980sw3vvd3h5") (s 2) (e (quote (("web" "dep:wasm-bindgen" "dep:web-sys"))))))

(define-public crate-another-rxrust-0.0.43 (c (n "another-rxrust") (v "0.0.43") (d (list (d (n "anyhow") (r "^1") (f (quote ("backtrace"))) (d #t) (k 2)) (d (n "tokio") (r "^1.27") (f (quote ("full"))) (d #t) (k 2)) (d (n "wasm-bindgen") (r "^0.2.83") (o #t) (d #t) (k 0)) (d (n "web-sys") (r "^0.3.60") (f (quote ("Window"))) (o #t) (d #t) (k 0)))) (h "0qnavmvbqz9pv1psjc6m3l15bjgn7zv9wx8qgvgrgzf7nc3rzjgf") (s 2) (e (quote (("web" "dep:wasm-bindgen" "dep:web-sys"))))))

(define-public crate-another-rxrust-0.0.44 (c (n "another-rxrust") (v "0.0.44") (d (list (d (n "anyhow") (r "^1") (f (quote ("backtrace"))) (d #t) (k 2)) (d (n "tokio") (r "^1.27") (f (quote ("full"))) (d #t) (k 2)) (d (n "wasm-bindgen") (r "^0.2.83") (o #t) (d #t) (k 0)) (d (n "web-sys") (r "^0.3.60") (f (quote ("Window"))) (o #t) (d #t) (k 0)))) (h "0s53jb684q34vyzkx643vc1x91yxjdlysr4bqpxjx33by8nqgwbp") (s 2) (e (quote (("web" "dep:wasm-bindgen" "dep:web-sys"))))))

(define-public crate-another-rxrust-0.0.45 (c (n "another-rxrust") (v "0.0.45") (d (list (d (n "anyhow") (r "^1") (f (quote ("backtrace"))) (d #t) (k 2)) (d (n "tokio") (r "^1.27") (f (quote ("full"))) (d #t) (k 2)) (d (n "wasm-bindgen") (r "^0.2.83") (o #t) (d #t) (k 0)) (d (n "web-sys") (r "^0.3.60") (f (quote ("Window"))) (o #t) (d #t) (k 0)))) (h "08hsrgyfr3crvivxj01524wf68xdfqhcfp878qfnaawi85axb0aq") (s 2) (e (quote (("web" "dep:wasm-bindgen" "dep:web-sys"))))))

(define-public crate-another-rxrust-0.0.46 (c (n "another-rxrust") (v "0.0.46") (d (list (d (n "anyhow") (r "^1") (f (quote ("backtrace"))) (d #t) (k 2)) (d (n "tokio") (r "^1.27") (f (quote ("full"))) (d #t) (k 2)) (d (n "wasm-bindgen") (r "^0.2.83") (o #t) (d #t) (k 0)) (d (n "web-sys") (r "^0.3.60") (f (quote ("Window"))) (o #t) (d #t) (k 0)))) (h "134x57ijg7m73dg88fyvm7rpbgby0vpl0dzaq0xf4x9gdbpfg3hw") (s 2) (e (quote (("web" "dep:wasm-bindgen" "dep:web-sys"))))))

