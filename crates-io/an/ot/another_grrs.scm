(define-module (crates-io an ot another_grrs) #:use-module (crates-io))

(define-public crate-another_grrs-0.1.0 (c (n "another_grrs") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "assert_cmd") (r "^0.10") (d #t) (k 2)) (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "predicates") (r "^1") (d #t) (k 2)) (d (n "structopt") (r "^0.2.10") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "0yj9gpin9xjay0nqbn2vwqdy70r3rp6ynj80mnxi2jmp8zmzkh4a")))

