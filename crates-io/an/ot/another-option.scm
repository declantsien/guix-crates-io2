(define-module (crates-io an ot another-option) #:use-module (crates-io))

(define-public crate-another-option-0.1.0 (c (n "another-option") (v "0.1.0") (h "0wc4nd31g44kzrzlslc3pzrrpgl3kgndrkzslhw83rzyljwzxmd6")))

(define-public crate-another-option-0.1.1 (c (n "another-option") (v "0.1.1") (h "1lh8zysiikkhzb3n8g4n2wxhzxpyrg4j7crpvmji5zb48s1yg5dn")))

