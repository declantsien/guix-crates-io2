(define-module (crates-io an ot another_json_minimal) #:use-module (crates-io))

(define-public crate-another_json_minimal-0.0.1 (c (n "another_json_minimal") (v "0.0.1") (h "0l99cw18z9wh7n6j1mg45smqsd7pvy4lv3rg9sj8ilfj9xhjx4h8")))

(define-public crate-another_json_minimal-0.0.2 (c (n "another_json_minimal") (v "0.0.2") (h "1gfr08hls0b0kiggkxa613zg4mhjyd3ippk2kwvqlv1rw50q7fkp")))

