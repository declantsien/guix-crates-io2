(define-module (crates-io an ot anothertls) #:use-module (crates-io))

(define-public crate-anothertls-0.1.0 (c (n "anothertls") (v "0.1.0") (d (list (d (n "ibig") (r "^0.3.6") (k 0)))) (h "0ynqkgml3swb5wanxc6k2pprkkwyvqxv3d32crzc9qfglsi1krhb")))

(define-public crate-anothertls-0.1.2 (c (n "anothertls") (v "0.1.2") (d (list (d (n "ibig") (r "^0.3.6") (k 0)))) (h "0clpdh2sq4rgp9gzrg8v9q174fzx2n1nm6fyyzn7qkgc956h34vp")))

(define-public crate-anothertls-0.1.3 (c (n "anothertls") (v "0.1.3") (d (list (d (n "ibig") (r "^0.3.6") (k 0)))) (h "15d43llkphyrx5f1h0sm3r9q9lwj5nq60xkzwwr91dkvxlczgjhn") (r "1.66")))

