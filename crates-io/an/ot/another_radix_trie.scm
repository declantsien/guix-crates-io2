(define-module (crates-io an ot another_radix_trie) #:use-module (crates-io))

(define-public crate-another_radix_trie-0.1.0 (c (n "another_radix_trie") (v "0.1.0") (h "1yjdwqpxm5gc4nll3vwgq8wayyjx942xjhfqp0vsimpygid96319")))

(define-public crate-another_radix_trie-0.1.1 (c (n "another_radix_trie") (v "0.1.1") (h "11lc9z1rwg5zj8kvhfg2j2pd0d3kzwbzzilik7kjr8flgkfklxpf")))

(define-public crate-another_radix_trie-0.1.2 (c (n "another_radix_trie") (v "0.1.2") (h "0mw1d1ddadinyr3zlvyz5zjr8axm6cbsmzyldcygd6z5cqxh7nkn")))

(define-public crate-another_radix_trie-0.1.3 (c (n "another_radix_trie") (v "0.1.3") (h "1ak9rcfv9gk2dgh2jimwm41kdw56llnlsmw2f839sd866jclrz9j")))

(define-public crate-another_radix_trie-0.1.4 (c (n "another_radix_trie") (v "0.1.4") (h "17l1090n17bw3hcbds0mp0yx5vsi8yzxrv4n7hxcg3ilyz91qrhi")))

