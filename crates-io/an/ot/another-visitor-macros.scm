(define-module (crates-io an ot another-visitor-macros) #:use-module (crates-io))

(define-public crate-another-visitor-macros-0.1.0 (c (n "another-visitor-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0xp86p0wrf5crh6pp0aqr8vavmzigl0yw39w4wkyhs4h2prg1m0f")))

