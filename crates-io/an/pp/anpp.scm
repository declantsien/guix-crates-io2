(define-module (crates-io an pp anpp) #:use-module (crates-io))

(define-public crate-anpp-1.0.0 (c (n "anpp") (v "1.0.0") (d (list (d (n "arrayvec") (r "^0.4") (d #t) (k 0)) (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "criterion") (r "^0.2.4") (d #t) (k 2)))) (h "0q486hp0smdd1mwfpcf18ns5kpr8ksanzgn31cdwd20510mrl32j") (f (quote (("std" "arrayvec/std"))))))

(define-public crate-anpp-1.0.1 (c (n "anpp") (v "1.0.1") (d (list (d (n "arrayvec") (r "^0.4") (d #t) (k 0)) (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "criterion") (r "^0.2.4") (d #t) (k 2)))) (h "0p3zc9cjbwjik6xa0rrr9d3c5ffzasazawrfp8lf81wqqiic5qx3") (f (quote (("std" "arrayvec/std"))))))

(define-public crate-anpp-2.0.0 (c (n "anpp") (v "2.0.0") (d (list (d (n "arrayvec") (r "^0.4") (d #t) (k 0)) (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "criterion") (r "^0.2.4") (d #t) (k 2)))) (h "0aw5hfgk1d5fbnmgji80vfdgblg6mzz0978gyaqkv3bqydkkrryl") (f (quote (("std" "arrayvec/std"))))))

