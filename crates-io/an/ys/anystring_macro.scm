(define-module (crates-io an ys anystring_macro) #:use-module (crates-io))

(define-public crate-anystring_macro-0.0.0 (c (n "anystring_macro") (v "0.0.0") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)))) (h "0x64h3dk954hs2fmn2ayf56jkndsfmnmkj54m786kv5yd440kghy")))

(define-public crate-anystring_macro-0.0.1 (c (n "anystring_macro") (v "0.0.1") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)))) (h "1l1bbw3h09p5s8x0vwgj83f8y89kh1s9g9n5935ml1v9vnq7lyq1")))

