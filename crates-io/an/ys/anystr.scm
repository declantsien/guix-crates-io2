(define-module (crates-io an ys anystr) #:use-module (crates-io))

(define-public crate-anystr-0.0.0 (c (n "anystr") (v "0.0.0") (d (list (d (n "ascii") (r "^1.1.0") (d #t) (k 0)) (d (n "widestring") (r "^1.1.0") (d #t) (k 0)))) (h "0rl64qrqgp6z2mf1in30w7km95pycj0ibs91wcmqnq0s02nh41w8") (f (quote (("std" "alloc" "ascii/std" "widestring/std") ("default" "std") ("alloc" "ascii/alloc" "widestring/alloc"))))))

(define-public crate-anystr-0.1.0 (c (n "anystr") (v "0.1.0") (d (list (d (n "ascii") (r "^1.1.0") (d #t) (k 0)) (d (n "parse-display") (r "^0.9.0") (d #t) (k 0)) (d (n "widestring") (r "^1.1.0") (d #t) (k 0)))) (h "06gjf34kvpax88llrfn4zhm734g1c7z2114f49271wpzmlg4kwlj") (f (quote (("unstable") ("std" "alloc" "ascii/std" "widestring/std") ("default" "std") ("alloc" "ascii/alloc" "widestring/alloc"))))))

(define-public crate-anystr-0.1.1 (c (n "anystr") (v "0.1.1") (d (list (d (n "ascii") (r "^1.1.0") (d #t) (k 0)) (d (n "parse-display") (r "^0.9.0") (d #t) (k 0)) (d (n "widestring") (r "^1.1.0") (d #t) (k 0)))) (h "1kvli6f18vaji2zp71gdfx52mjs7nysd62jhhshd8afla0n2ix0w") (f (quote (("unstable") ("std" "alloc" "ascii/std" "widestring/std") ("default" "std") ("alloc" "ascii/alloc" "widestring/alloc"))))))

