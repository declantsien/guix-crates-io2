(define-module (crates-io an ys anyslot) #:use-module (crates-io))

(define-public crate-anyslot-0.0.1 (c (n "anyslot") (v "0.0.1") (d (list (d (n "bindgen") (r "=0.65.1") (d #t) (k 1)) (d (n "cc") (r "=1.0.79") (d #t) (k 1)) (d (n "home") (r "=0.5.5") (d #t) (k 0)))) (h "1rzr0n3nwdbvlgkyanj4af2hyg33wxh43ndyvzfmvvn10j60jslp") (y #t)))

(define-public crate-anyslot-0.1.0 (c (n "anyslot") (v "0.1.0") (d (list (d (n "bindgen") (r "=0.65.1") (d #t) (k 1)) (d (n "cc") (r "=1.0.79") (d #t) (k 1)) (d (n "home") (r "=0.5.5") (d #t) (k 0)))) (h "0jfgvf2c37rgsj87bahjv0yhq9wb3s0skxq0iwcnyhqgx0bkzz95") (y #t)))

(define-public crate-anyslot-0.1.1 (c (n "anyslot") (v "0.1.1") (d (list (d (n "bindgen") (r "=0.65.1") (d #t) (k 1)) (d (n "cc") (r "=1.0.79") (d #t) (k 1)) (d (n "home") (r "=0.5.5") (d #t) (k 0)))) (h "0ky9crl5fq2qx5pl5ysg2a2yrlliv1si34vxi6x63d24610pkma2")))

