(define-module (crates-io an ys anystring) #:use-module (crates-io))

(define-public crate-anystring-0.0.0 (c (n "anystring") (v "0.0.0") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)))) (h "0h3gizzbg5a9dnvcisv162i5qqi5djjbc8sgmhslgs1avxk8i868")))

(define-public crate-anystring-0.0.1 (c (n "anystring") (v "0.0.1") (d (list (d (n "anystring_macro") (r "^0.0.0") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)))) (h "0b7s6grw3yf7c66x1bqsf8p62rakqp3lwigh3vb69vyg16xq7h88")))

(define-public crate-anystring-0.0.2 (c (n "anystring") (v "0.0.2") (d (list (d (n "anystring_macro") (r "^0.0") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)))) (h "18qgphh5rhgy9dsigsk02ikryfnkhn4sszidi1zd6y7s5iqxqgr7")))

