(define-module (crates-io an ys anysexpr) #:use-module (crates-io))

(define-public crate-anysexpr-0.1.0 (c (n "anysexpr") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.4") (d #t) (k 0)) (d (n "clap") (r "^3.2.1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "genawaiter") (r "^0.99") (k 0)) (d (n "kstring") (r "^1.0") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "utf-8") (r "^0.7.5") (d #t) (k 0)))) (h "1fqzlikafzwmivmbhsc30g3n5fkxdd6dk1vh3cg8d5fkvbbn4yh3")))

(define-public crate-anysexpr-0.2.0 (c (n "anysexpr") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.14") (d #t) (k 0)) (d (n "clap") (r "^3.2.1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "genawaiter") (r "^0.99") (k 0)) (d (n "kstring") (r "^1.0") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "utf-8") (r "^0.7.5") (d #t) (k 0)))) (h "11vcv7h5sdfy446hfc8xm2synkzxivbl420kn8y0qm4lhswd7iak")))

(define-public crate-anysexpr-0.3.0 (c (n "anysexpr") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.14") (d #t) (k 0)) (d (n "clap") (r "^3.2.1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "genawaiter") (r "^0.99") (k 0)) (d (n "kstring") (r "^1.0") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "utf-8") (r "^0.7.5") (d #t) (k 0)))) (h "0yivp90lzwpaw6a7p97i8pa8yvqa5q9kkkdfmy1ccppib54i04cs")))

(define-public crate-anysexpr-0.4.0 (c (n "anysexpr") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.14") (d #t) (k 0)) (d (n "clap") (r "^3.2.1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "genawaiter") (r "^0.99") (k 0)) (d (n "kstring") (r "^1.0") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "utf-8") (r "^0.7.5") (d #t) (k 0)))) (h "0q4p1zbzsvwcgbiak5hxh58y3l3cxrjfij318qnf3h7frwghnbfq")))

