(define-module (crates-io an om anomalocaris) #:use-module (crates-io))

(define-public crate-anomalocaris-0.1.0 (c (n "anomalocaris") (v "0.1.0") (d (list (d (n "cc") (r "^1") (f (quote ("parallel"))) (d #t) (k 1)))) (h "0qdckvfsm11xj2zfklmybkps1krck5vyfja9ij19srff9p97zzdg")))

(define-public crate-anomalocaris-0.1.1 (c (n "anomalocaris") (v "0.1.1") (d (list (d (n "cc") (r "^1") (f (quote ("parallel"))) (d #t) (k 1)))) (h "1rxm797aa49vy32qfvb9y7zbw6kanga1w9g9zy0419f6lb2rq6nl")))

