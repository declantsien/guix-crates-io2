(define-module (crates-io an om anomaly_detection) #:use-module (crates-io))

(define-public crate-anomaly_detection-0.1.0 (c (n "anomaly_detection") (v "0.1.0") (d (list (d (n "statrs") (r "^0.15") (d #t) (k 0)) (d (n "stlrs") (r "^0.1") (d #t) (k 0)))) (h "11rh859c23qy0wv0jfnlavp50l58hmf3bg7f0xjjsk34pa249xrn")))

(define-public crate-anomaly_detection-0.1.1 (c (n "anomaly_detection") (v "0.1.1") (d (list (d (n "statrs") (r "^0.15") (d #t) (k 0)) (d (n "stlrs") (r "^0.1") (d #t) (k 0)))) (h "05rcvbjgp38k9bdbv7w0dffzpd68ilfg7jz7sv0l3j35wgdnds3c")))

(define-public crate-anomaly_detection-0.1.2 (c (n "anomaly_detection") (v "0.1.2") (d (list (d (n "statrs") (r "^0.15") (d #t) (k 0)) (d (n "stlrs") (r "^0.1") (d #t) (k 0)))) (h "17zlani6ab9xwf5128dqsczcknl73bxrl5lb15hccdh3m883qpy2")))

(define-public crate-anomaly_detection-0.2.0 (c (n "anomaly_detection") (v "0.2.0") (d (list (d (n "statrs") (r "^0.15") (d #t) (k 0)) (d (n "stlrs") (r "^0.2") (d #t) (k 0)))) (h "14l3j2rnvppz20x3945pmshyk5ls0bvvnj2h6pdai7fwyfcwp1wc")))

(define-public crate-anomaly_detection-0.2.1 (c (n "anomaly_detection") (v "0.2.1") (d (list (d (n "statrs") (r "^0.15") (d #t) (k 0)) (d (n "stlrs") (r "^0.2") (d #t) (k 0)))) (h "077ps91il2c1awsywskzr4gxpqcdr3hv3xb9dr1808hv4jpm6p1k")))

(define-public crate-anomaly_detection-0.2.2 (c (n "anomaly_detection") (v "0.2.2") (d (list (d (n "distrs") (r "^0.1") (d #t) (k 0)) (d (n "stlrs") (r "^0.2") (d #t) (k 0)))) (h "16h58fr1z3255n2yghi2n3br47f6z44b5nbm878j54wkx5z0n421")))

(define-public crate-anomaly_detection-0.2.3 (c (n "anomaly_detection") (v "0.2.3") (d (list (d (n "distrs") (r "^0.1") (d #t) (k 0)) (d (n "stlrs") (r "^0.2") (d #t) (k 0)))) (h "0ap2aim17qji8wccgd65x60nzflp5qlcprq4dh1y876nkn0954qp")))

(define-public crate-anomaly_detection-0.3.0 (c (n "anomaly_detection") (v "0.3.0") (d (list (d (n "distrs") (r "^0.2") (d #t) (k 0)) (d (n "stlrs") (r "^0.3") (d #t) (k 0)))) (h "1bxbw1ddvril7bz2klwq1c3spyz79d13yfiw1wzjwlpvw07b7zjj") (r "1.56.0")))

