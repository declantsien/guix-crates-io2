(define-module (crates-io an om anomaly) #:use-module (crates-io))

(define-public crate-anomaly-0.0.0 (c (n "anomaly") (v "0.0.0") (h "0b76yqgy1ac8n7qp1qsjd73l3nddqdz3nfy6csgr4sbxdl05lr2d") (y #t)))

(define-public crate-anomaly-0.1.0 (c (n "anomaly") (v "0.1.0") (d (list (d (n "backtrace") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "085ks103f4v9lh46j9ff9qz611viyqppnxb4prkidgzdcv13cjiw") (f (quote (("gimli-symbolize" "backtrace/gimli-symbolize") ("default" "backtrace")))) (y #t)))

(define-public crate-anomaly-0.1.1 (c (n "anomaly") (v "0.1.1") (d (list (d (n "backtrace") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "0rpr01sfig1irnwarcznl0sk199in4s6vqr51r50053br6si9xgp") (f (quote (("gimli-symbolize" "backtrace/gimli-symbolize") ("default" "backtrace")))) (y #t)))

(define-public crate-anomaly-0.1.2 (c (n "anomaly") (v "0.1.2") (d (list (d (n "backtrace") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "1rknbd9i94179lvsc9agncxixrl2jw5sik58dcb6qb69hxhrwzrw") (f (quote (("gimli-symbolize" "backtrace/gimli-symbolize") ("default" "backtrace")))) (y #t)))

(define-public crate-anomaly-0.2.0 (c (n "anomaly") (v "0.2.0") (d (list (d (n "backtrace") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 2)))) (h "1jswpi7zcz1kr89ikjcykg20y0v3hnj3m34r8xgimbk82pik41jm") (f (quote (("serializer" "backtrace/serde" "serde") ("gimli-symbolize" "backtrace/gimli-symbolize") ("default" "backtrace"))))))

(define-public crate-anomaly-0.99.0 (c (n "anomaly") (v "0.99.0") (h "03sk4fddsz33my3b6wlspk8pmhcxmhgnmbh39is9z214xydw7cwr")))

