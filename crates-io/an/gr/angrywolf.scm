(define-module (crates-io an gr angrywolf) #:use-module (crates-io))

(define-public crate-AngryWolf-0.1.0 (c (n "AngryWolf") (v "0.1.0") (d (list (d (n "clap") (r "^4.1.11") (f (quote ("derive"))) (d #t) (k 0)))) (h "1zsy9qnq0m2acy0m6phx56qnh1h0h5llwg4bgzqs72a2hzn9fq7k")))

(define-public crate-AngryWolf-0.1.1 (c (n "AngryWolf") (v "0.1.1") (d (list (d (n "clap") (r "^4.1.11") (f (quote ("derive"))) (d #t) (k 0)))) (h "1vzilbrvawl8llih1yr1dsv8wn0rs9db7i6x66ap4wnp947yy521")))

