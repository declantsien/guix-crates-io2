(define-module (crates-io an gr angrylion-rdp-plus-sys) #:use-module (crates-io))

(define-public crate-angrylion-rdp-plus-sys-0.1.0 (c (n "angrylion-rdp-plus-sys") (v "0.1.0") (d (list (d (n "cmake") (r "^0.1.45") (d #t) (k 1)))) (h "1ndw19a4zqk7jjn9pjzyakwhirkqjvcbg5gs5zxa67280qbjvcd5") (l "mupen64plus-video-angrylion-plus")))

(define-public crate-angrylion-rdp-plus-sys-0.1.1 (c (n "angrylion-rdp-plus-sys") (v "0.1.1") (d (list (d (n "cmake") (r "^0.1.45") (d #t) (k 1)))) (h "1yq62h8vyp7iccmdw765fdpq0nrz073di4qb55crhyr8isx2fra0") (f (quote (("no-build")))) (l "mupen64plus-video-angrylion-plus")))

(define-public crate-angrylion-rdp-plus-sys-0.2.0 (c (n "angrylion-rdp-plus-sys") (v "0.2.0") (d (list (d (n "cc") (r "^1.0.68") (o #t) (d #t) (k 1)))) (h "1dj4his54qpdrbgri0ipdignm8qgdz16d2h0ad4xaaxjk3bskkqx") (f (quote (("default" "build") ("build" "cc")))) (l "alp-core")))

