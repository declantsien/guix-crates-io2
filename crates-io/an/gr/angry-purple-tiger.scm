(define-module (crates-io an gr angry-purple-tiger) #:use-module (crates-io))

(define-public crate-angry-purple-tiger-0.1.0 (c (n "angry-purple-tiger") (v "0.1.0") (d (list (d (n "md5") (r "^0") (d #t) (k 0)))) (h "0g1a2b02mp9pgn6qf2fn87ziifklafx5phwshxdci9chigmlkis9")))

(define-public crate-angry-purple-tiger-1.0.0 (c (n "angry-purple-tiger") (v "1.0.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "helium-crypto") (r ">=0.6") (o #t) (d #t) (k 0)) (d (n "md5") (r "^0") (d #t) (k 0)))) (h "15savjfjcx1zbwl2rvivqmm82a6pzx6xl7yf3rn3g0hd5dpvwmkm") (f (quote (("helium_crypto" "helium-crypto"))))))

