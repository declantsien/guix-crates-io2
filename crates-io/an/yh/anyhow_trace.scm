(define-module (crates-io an yh anyhow_trace) #:use-module (crates-io))

(define-public crate-anyhow_trace-0.1.0 (c (n "anyhow_trace") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.53") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "0pr4bzmafdvkiqwn08ajqp3b6v68bpd4d9qdjrgxxzc9c9v64ycl")))

(define-public crate-anyhow_trace-0.1.1 (c (n "anyhow_trace") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.53") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "018jdh6s8rrqdhzb9fr0gsswjlbi0bs6mlzbz2xrv8cs0chx09iv")))

(define-public crate-anyhow_trace-0.1.2 (c (n "anyhow_trace") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.53") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "1gqx4rkjh4m3fl128p1irc37z4fkwdw1bl06bgxwhnx4fhdgbfa3")))

(define-public crate-anyhow_trace-0.1.3 (c (n "anyhow_trace") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.53") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "1prcvas44gzz0n60rd8a05rv23zknjsh6ix33rmhra0lvhhnb4kp")))

