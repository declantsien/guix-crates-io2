(define-module (crates-io an yh anyhash-macros) #:use-module (crates-io))

(define-public crate-anyhash-macros-0.1.0 (c (n "anyhash-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("clone-impls" "derive" "parsing" "printing" "proc-macro"))) (k 0)))) (h "0rm1b4njf92d19csybkcv55gbb4k3kihs5yxlw91x94wb814kznl")))

