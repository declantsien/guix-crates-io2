(define-module (crates-io an yh anyhow-std) #:use-module (crates-io))

(define-public crate-anyhow-std-0.1.0 (c (n "anyhow-std") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "derive-new") (r "^0.5.9") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "tempfile") (r "^3.5.0") (d #t) (k 2)) (d (n "test-case") (r "^3.0.0") (d #t) (k 2)))) (h "19d14a40k8xlkx1707dd98zvkad94nfc3sk9ss1nx5g8wl86gh42")))

(define-public crate-anyhow-std-0.1.1 (c (n "anyhow-std") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "tempfile") (r "^3.5.0") (d #t) (k 2)) (d (n "test-case") (r "^3.0.0") (d #t) (k 2)))) (h "0w8bziijf8qj0ybvr4r4v8mc7ahdrj70wh5vhfwmy4m74lkzz4g1")))

(define-public crate-anyhow-std-0.1.2 (c (n "anyhow-std") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "tempfile") (r "^3.5.0") (d #t) (k 2)) (d (n "test-case") (r "^3.0.0") (d #t) (k 2)))) (h "1ljw7crj1v7658vyf25myn7cz7vasni1gs8yb7g9xsfgnr8zrcy7")))

(define-public crate-anyhow-std-0.1.3 (c (n "anyhow-std") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "tempfile") (r "^3.5.0") (d #t) (k 2)) (d (n "test-case") (r "^3.0.0") (d #t) (k 2)))) (h "03dw4hgqqlgn25jz36hj0lcw9p5p2a6zqfd0v8g8248b43sg06ip")))

(define-public crate-anyhow-std-0.1.4 (c (n "anyhow-std") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "tempfile") (r "^3.5.0") (d #t) (k 2)) (d (n "test-case") (r "^3.0.0") (d #t) (k 2)))) (h "0h5q5as3qm2vnz6jb8rqn8ajibsrahar3hfb836h82bkwqdhsc4n")))

