(define-module (crates-io an yh anyhash) #:use-module (crates-io))

(define-public crate-anyhash-0.1.0 (c (n "anyhash") (v "0.1.0") (d (list (d (n "anyhash-macros") (r "^0.1") (d #t) (k 0)) (d (n "bnum") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "bytemuck") (r "^1.14") (f (quote ("min_const_generics"))) (o #t) (d #t) (k 0)))) (h "0kxnjykanm34dnd9z5m8387a84ynw4z3rpl0kn1vx9sfbxdv999n") (f (quote (("xxh64" "bytemuck") ("std" "alloc") ("spooky" "bytemuck") ("nightly") ("fnv") ("alloc")))) (s 2) (e (quote (("bytemuck" "dep:bytemuck") ("bnum" "dep:bnum"))))))

(define-public crate-anyhash-0.1.1 (c (n "anyhash") (v "0.1.1") (d (list (d (n "anyhash-macros") (r "^0.1") (d #t) (k 0)) (d (n "bnum") (r "^0.11") (o #t) (d #t) (k 0)) (d (n "bytemuck") (r "^1.14") (f (quote ("min_const_generics"))) (o #t) (d #t) (k 0)))) (h "0dvj248h1jp9d8flcqclgbrjaay0025xbikr9197ndfanswzjfw8") (f (quote (("xxh64" "bytemuck") ("std" "alloc") ("spooky" "bytemuck") ("nightly") ("fnv") ("alloc")))) (y #t) (s 2) (e (quote (("bytemuck" "dep:bytemuck") ("bnum" "dep:bnum"))))))

(define-public crate-anyhash-0.1.2 (c (n "anyhash") (v "0.1.2") (d (list (d (n "anyhash-macros") (r "^0.1") (d #t) (k 0)) (d (n "bnum") (r ">=0.10, <0.12") (o #t) (d #t) (k 0)) (d (n "bytemuck") (r "^1.14") (f (quote ("min_const_generics"))) (o #t) (d #t) (k 0)))) (h "1xqrby6v5a6gy059pdlc6pz4jj4f4y71c527blnkxrb5p2wkprig") (f (quote (("xxh64" "bytemuck") ("std" "alloc") ("spooky" "bytemuck") ("nightly") ("fnv") ("alloc")))) (s 2) (e (quote (("bytemuck" "dep:bytemuck") ("bnum" "dep:bnum"))))))

