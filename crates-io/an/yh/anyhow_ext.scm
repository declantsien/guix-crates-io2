(define-module (crates-io an yh anyhow_ext) #:use-module (crates-io))

(define-public crate-anyhow_ext-0.1.0 (c (n "anyhow_ext") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)))) (h "06bj31140df66ygr5gqjx7l5q9yvl0bxzq2xk3nbahww12ik10dh")))

(define-public crate-anyhow_ext-0.2.0 (c (n "anyhow_ext") (v "0.2.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)))) (h "120lbvczc3w2d71bnl763dmi20cjgr18i5ibrqrm025yqyb8yr14")))

(define-public crate-anyhow_ext-0.2.1 (c (n "anyhow_ext") (v "0.2.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)))) (h "0a3m07lnmwzysn2la7kcj3vi637n8b19p5qjsx72bcpm5d9cndd1")))

