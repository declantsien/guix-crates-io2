(define-module (crates-io an y2 any2utf8) #:use-module (crates-io))

(define-public crate-any2utf8-0.1.0 (c (n "any2utf8") (v "0.1.0") (d (list (d (n "chardetng") (r "^0.1.17") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8.33") (d #t) (k 0)))) (h "1smjm4g5s8xrvflsb952ad2h17dnvz2rbwjnni9mmb93bxlxdha0")))

(define-public crate-any2utf8-0.1.1 (c (n "any2utf8") (v "0.1.1") (d (list (d (n "chardetng") (r "^0.1.17") (d #t) (k 0)) (d (n "clap") (r "^4.5.3") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8.33") (d #t) (k 0)))) (h "0cp6vmnpvdrvw369wlvnmw3z8y0p0vfcxrdc733iqan8hd5riqfn")))

