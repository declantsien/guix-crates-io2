(define-module (crates-io an gl anglosaxon) #:use-module (crates-io))

(define-public crate-anglosaxon-0.1.0-rc1 (c (n "anglosaxon") (v "0.1.0-rc1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^3") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8") (d #t) (k 0)))) (h "08d9n5nz834ddzrddyd8s2s2adpzgb98001nnig0lfzbibwb5h4n")))

(define-public crate-anglosaxon-0.1.0 (c (n "anglosaxon") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^3") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8") (d #t) (k 0)))) (h "0ps2lzflvixlwjfgih9496i791yvakwb3drzkdqxpqc5vy06qy5v")))

(define-public crate-anglosaxon-0.1.1 (c (n "anglosaxon") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "xml-rs") (r "^0.8") (d #t) (k 0)))) (h "14w7srcr5g4hmac942b0rq0g6xyjzyl6r198iavdqfdjx4bf95d0")))

