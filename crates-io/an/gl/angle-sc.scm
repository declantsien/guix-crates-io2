(define-module (crates-io an gl angle-sc) #:use-module (crates-io))

(define-public crate-angle-sc-0.1.0 (c (n "angle-sc") (v "0.1.0") (d (list (d (n "libm") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1q7rkk95nn1g60csbnhighwis0x6yqwg7xj1b03z6r82h5g1l694")))

(define-public crate-angle-sc-0.1.1 (c (n "angle-sc") (v "0.1.1") (d (list (d (n "libm") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0b1wl61smq8w40x81bg4bsrkynywqlqw5bkrfgh6hga8vp8f74n9")))

(define-public crate-angle-sc-0.1.2 (c (n "angle-sc") (v "0.1.2") (d (list (d (n "libm") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "05xxd0q0gxbbfsb34xlcp05qy6wqx1rj9iajlrl5mkv6f0mr5nkm")))

