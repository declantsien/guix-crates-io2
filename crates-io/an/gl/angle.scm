(define-module (crates-io an gl angle) #:use-module (crates-io))

(define-public crate-angle-0.1.0 (c (n "angle") (v "0.1.0") (d (list (d (n "num") (r "*") (d #t) (k 0)))) (h "1lz0dyfz0x96656ar03ki9xm8gvb10k8za3v43cf93jrwibli8ic")))

(define-public crate-angle-0.1.1 (c (n "angle") (v "0.1.1") (d (list (d (n "num") (r "*") (d #t) (k 0)))) (h "0ggarsc1p8spcm9lh2wxz2i8439ymisr24ga0yhqf09hvlymw0lb")))

(define-public crate-angle-0.1.2 (c (n "angle") (v "0.1.2") (d (list (d (n "num") (r "^0.1.31") (d #t) (k 0)))) (h "0axazm91pi1i44ahsfy67i9a8dk35mv69cjspyq4qdfdm8vb08rs")))

(define-public crate-angle-0.1.3 (c (n "angle") (v "0.1.3") (d (list (d (n "num") (r "^0.1.31") (d #t) (k 0)))) (h "1kaizmcnswzwngnsfd8czdc6f76ckrw49pdi8aw7p4k1p8wbkhf6")))

(define-public crate-angle-0.1.4 (c (n "angle") (v "0.1.4") (d (list (d (n "num") (r "^0.1.31") (d #t) (k 0)))) (h "1fglq6b945ii9mghcf9rabvp5bnb0nnsk09rj118ddpb30kpmdcx")))

(define-public crate-angle-0.1.55555 (c (n "angle") (v "0.1.55555") (d (list (d (n "num") (r "^0.1.31") (d #t) (k 0)))) (h "0cszi3zxfm2zb4f972amxb951yjkj55a7k2fk8h9wy16apq4nl73")))

(define-public crate-angle-0.2.0 (c (n "angle") (v "0.2.0") (d (list (d (n "num-traits") (r "^0.1.40") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "00jlk5f2fk49cwy005izkbdzsyf0qqgvj009880avbn4ashj19c8")))

(define-public crate-angle-0.3.0 (c (n "angle") (v "0.3.0") (d (list (d (n "num-traits") (r "^0.1.40") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0h09hhlwr8h3kdwyix7x9gvjqgilf7x62b5r0iq5lhclgs0vcwkb")))

(define-public crate-angle-0.3.1 (c (n "angle") (v "0.3.1") (d (list (d (n "num-traits") (r "^0.1.40") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1hz7k56nvjyhvp5vcsgwrmrimlbkr1f7rn4zmkg1fgb3bp4f7fjm")))

(define-public crate-angle-0.3.2 (c (n "angle") (v "0.3.2") (d (list (d (n "num-traits") (r "^0.1.40") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "026v7r4ibsvdkqnrh4nn6fzfgc3klz01mvgnh0wc0d67s9r6vnpn")))

(define-public crate-angle-0.3.3 (c (n "angle") (v "0.3.3") (d (list (d (n "num-traits") (r "^0.1.40") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1qjzpvr6q5rwrdmsrbk6ppbp9dd6vsy5x8m2sghx84bkvjf2spnq")))

(define-public crate-angle-0.3.4 (c (n "angle") (v "0.3.4") (d (list (d (n "num-traits") (r "^0.1.40") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "08idvhy1mbj4pibp3dw5qkj57w2dbsd6d59rmnym0wmilcf605z8")))

(define-public crate-angle-0.3.5 (c (n "angle") (v "0.3.5") (d (list (d (n "num-traits") (r "^0.1.40") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0m045jaq9d0bidj8880pakxqd76hnbqwqhspwnmipsh38syzw262")))

(define-public crate-angle-0.4.0 (c (n "angle") (v "0.4.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "10bqmh5r0zi1afzjvx3jscnz7wvgmw0n4zqi08ccwlm9hxrahaxs")))

(define-public crate-angle-0.5.0 (c (n "angle") (v "0.5.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "simba") (r "^0.7") (o #t) (d #t) (k 0)))) (h "1ym5qjvnqqnqgwlcbci2wrsgxw0pgm9fy5g8a20wdj22n6v6by8b")))

