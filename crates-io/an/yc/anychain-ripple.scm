(define-module (crates-io an yc anychain-ripple) #:use-module (crates-io))

(define-public crate-anychain-ripple-0.1.0 (c (n "anychain-ripple") (v "0.1.0") (d (list (d (n "anychain-core") (r "^0.1.2") (d #t) (k 0)) (d (n "base58") (r "^0.2") (d #t) (k 0)) (d (n "sha2") (r "^0.10.2") (k 0)))) (h "197gpz5wfr4y27ack4lv2h2wbw122jf2bxqh44hjcay4r3ahgnpc")))

(define-public crate-anychain-ripple-0.1.1 (c (n "anychain-ripple") (v "0.1.1") (d (list (d (n "anychain-core") (r "^0.1.2") (d #t) (k 0)) (d (n "base58") (r "^0.2") (d #t) (k 0)) (d (n "sha2") (r "^0.10.2") (k 0)))) (h "0jrlnzzzqrqc7d53xl5d2gajajzbsnp55kkybm1pi9v6y1p5v91z")))

(define-public crate-anychain-ripple-0.1.2 (c (n "anychain-ripple") (v "0.1.2") (d (list (d (n "anychain-core") (r "^0.1.2") (d #t) (k 0)) (d (n "base58") (r "^0.2") (d #t) (k 0)) (d (n "sha2") (r "^0.10.2") (k 0)))) (h "06crsnpx79l2ip3c6a6hji4bppyx5bkpklhs9z2qvlhb0mcnb9p5")))

(define-public crate-anychain-ripple-0.1.3 (c (n "anychain-ripple") (v "0.1.3") (d (list (d (n "anychain-core") (r "^0.1.2") (d #t) (k 0)) (d (n "base58") (r "^0.2") (d #t) (k 0)) (d (n "sha2") (r "^0.10.2") (k 0)))) (h "0pmdap1wd4405chh87927sdbc970g7qny6hhsrn8q73g2swsghs4")))

(define-public crate-anychain-ripple-0.1.4 (c (n "anychain-ripple") (v "0.1.4") (d (list (d (n "anychain-core") (r "^0.1.2") (d #t) (k 0)) (d (n "base58") (r "^0.2") (d #t) (k 0)) (d (n "sha2") (r "^0.10.2") (k 0)))) (h "08h344c5i30vgc6hzfapcdiqwv6r2w78biq264nsnxsyr69k7i9x")))

(define-public crate-anychain-ripple-0.1.5 (c (n "anychain-ripple") (v "0.1.5") (d (list (d (n "anychain-core") (r "^0.1.3") (d #t) (k 0)) (d (n "base58") (r "^0.2") (d #t) (k 0)) (d (n "sha2") (r "^0.10.2") (k 0)))) (h "0m8s4a0aqm6af99ashd47yr53rvkqycnzcg5r30l413v3nbp0mgq")))

(define-public crate-anychain-ripple-0.1.6 (c (n "anychain-ripple") (v "0.1.6") (d (list (d (n "anychain-core") (r "^0.1.5") (d #t) (k 0)) (d (n "base58") (r "^0.2") (d #t) (k 0)) (d (n "libsecp256k1") (r "^0.7.1") (d #t) (k 0)))) (h "0qxpbjafp7dkiwg9a3aqhq7qkad9b3clk38kgw019f8dkcvyniz1")))

(define-public crate-anychain-ripple-0.1.7 (c (n "anychain-ripple") (v "0.1.7") (d (list (d (n "anychain-core") (r "^0.1.6") (d #t) (k 0)) (d (n "base58") (r "^0.2") (d #t) (k 0)) (d (n "libsecp256k1") (r "^0.7.1") (d #t) (k 0)))) (h "0fa4gac6f48nn26nw1bg7z04yvb0q2w5ddjgg8cf4hmwjblx0nh7")))

(define-public crate-anychain-ripple-0.1.8 (c (n "anychain-ripple") (v "0.1.8") (d (list (d (n "anychain-core") (r "^0.1.6") (d #t) (k 0)) (d (n "base58") (r "^0.2") (d #t) (k 0)) (d (n "libsecp256k1") (r "^0.7.1") (d #t) (k 0)))) (h "1g1fjgrmz2vsh1kkq6ydz066n3cadhala2dbmx8r91l36c0km7fv")))

