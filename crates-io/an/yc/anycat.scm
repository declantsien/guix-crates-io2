(define-module (crates-io an yc anycat) #:use-module (crates-io))

(define-public crate-anycat-0.1.0 (c (n "anycat") (v "0.1.0") (d (list (d (n "brotli2") (r "^0.3.2") (d #t) (k 0)) (d (n "bzip2") (r "^0.3.2") (d #t) (k 0)) (d (n "clap") (r "^2.28.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0.1") (d #t) (k 0)) (d (n "xz2") (r "^0.1.3") (d #t) (k 0)))) (h "187c6sbqhdl02kla4c1c7ifi881sakfd2z1csl4c9jmad69wh9vy")))

(define-public crate-anycat-0.1.1 (c (n "anycat") (v "0.1.1") (d (list (d (n "brotli2") (r "^0.3.2") (d #t) (k 0)) (d (n "bzip2") (r "^0.3.2") (d #t) (k 0)) (d (n "clap") (r "^2.28.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0.1") (d #t) (k 0)) (d (n "xz2") (r "^0.1.3") (d #t) (k 0)))) (h "0xfirnxhhzi8aa60nv24whr7in7j2pp4w3xs7yxs23c96n95kyd5")))

(define-public crate-anycat-0.1.2 (c (n "anycat") (v "0.1.2") (d (list (d (n "brotli2") (r "^0.3.2") (d #t) (k 0)) (d (n "bzip2") (r "^0.3.3") (d #t) (k 0)) (d (n "clap") (r "^2.28.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0.6") (d #t) (k 0)) (d (n "xz2") (r "^0.1.6") (d #t) (k 0)))) (h "19f4w16rlzhq6mzcrqqhli5094r4yf9aw741m68qfj6b9r5wnizi")))

(define-public crate-anycat-0.2.0 (c (n "anycat") (v "0.2.0") (d (list (d (n "brotli2") (r "^0.3.2") (d #t) (k 0)) (d (n "bzip2") (r "^0.4.1") (d #t) (k 0)) (d (n "clap") (r "^2.28.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0.20") (d #t) (k 0)) (d (n "xz2") (r "^0.1.6") (d #t) (k 0)))) (h "0mq43k8add1vwar5wvqdgl4gncgplax57vm0snkszy80n2x1ix6q")))

