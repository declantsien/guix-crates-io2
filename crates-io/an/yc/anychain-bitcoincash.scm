(define-module (crates-io an yc anychain-bitcoincash) #:use-module (crates-io))

(define-public crate-anychain-bitcoincash-0.1.0 (c (n "anychain-bitcoincash") (v "0.1.0") (d (list (d (n "anychain-bitcoin") (r "^0.1.0") (d #t) (k 0)) (d (n "anychain-core") (r "^0.0.1") (d #t) (k 0)) (d (n "base58") (r "^0.2.0") (d #t) (k 0)) (d (n "bech32") (r "^0.9.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "17sprrxprbnnyrr9p9bbpwqq00d68d8jnfsn6lihzzmzawf2gjkl") (y #t)))

