(define-module (crates-io an yc anyctx) #:use-module (crates-io))

(define-public crate-anyctx-0.1.0 (c (n "anyctx") (v "0.1.0") (d (list (d (n "ahash") (r "^0.8.6") (d #t) (k 0)) (d (n "dashmap") (r "^5.5.3") (d #t) (k 0)))) (h "1qsm8y5fw95gq7j79qa4x5ci6gaj0zg33vfpfswp23gg5dra7rcg")))

