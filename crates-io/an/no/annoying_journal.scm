(define-module (crates-io an no annoying_journal) #:use-module (crates-io))

(define-public crate-annoying_journal-0.1.0 (c (n "annoying_journal") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "fltk") (r "^1.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1g1744jz3pz43ww68mcvfv268aq5kh9k6cs426fpkfb10wmy7x95")))

(define-public crate-annoying_journal-0.1.1 (c (n "annoying_journal") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "fltk") (r "^1.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1cgvysvs3d9bjm1v7h270vmjxny88189kg9zyprr56rzy6vmr13r")))

(define-public crate-annoying_journal-0.1.2 (c (n "annoying_journal") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "fltk") (r "^1.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1nim97z47wlmnk39bv1ar8sqaa6a3mkp16ascwp9lds257r2klbp")))

(define-public crate-annoying_journal-0.1.3 (c (n "annoying_journal") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "fltk") (r "^1.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0mrv8bzkn4lyl54mk37ry4c8r9bhis6zs39wyjh7asrkzzcvbdwk")))

