(define-module (crates-io an no annotation-rs-codegen) #:use-module (crates-io))

(define-public crate-annotation-rs-codegen-0.1.0 (c (n "annotation-rs-codegen") (v "0.1.0") (d (list (d (n "heck") (r "^0.3.1") (d #t) (k 0)) (d (n "helpers") (r "^0.1.0") (d #t) (k 0) (p "annotation-rs-helpers")) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("fold" "full"))) (d #t) (k 0)))) (h "06yq5vd6h9dnk3k14xd88fjiz0wnjh9j911v3zpaa0cjmjjqwk0s")))

