(define-module (crates-io an no annoyify) #:use-module (crates-io))

(define-public crate-annoyify-0.1.0 (c (n "annoyify") (v "0.1.0") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)))) (h "1n8hiyap4br9q067ivzkc9n399zy9dd7q6i1iimc2fq9xddybwpl")))

(define-public crate-annoyify-0.2.0 (c (n "annoyify") (v "0.2.0") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)))) (h "0jj5s87xvw6m8wh5vgx6a8wgf9dnarr5ia1frvgmnxppilvrrqr4")))

