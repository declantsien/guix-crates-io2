(define-module (crates-io an no annotation-rs-helpers) #:use-module (crates-io))

(define-public crate-annotation-rs-helpers-0.1.0 (c (n "annotation-rs-helpers") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "192zknh7mjkpkj79yqs1163b5cnqyni9sckyvgh6q220l5v1h1jp")))

