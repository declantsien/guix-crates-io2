(define-module (crates-io an no annoy-rs) #:use-module (crates-io))

(define-public crate-annoy-rs-0.1.0 (c (n "annoy-rs") (v "0.1.0") (d (list (d (n "cfg-if") (r "^1") (d #t) (k 0)) (d (n "hashbrown") (r "^0") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "libc") (r "^0") (o #t) (d #t) (k 0)) (d (n "memmap2") (r "^0") (d #t) (k 0)) (d (n "rand") (r "^0") (d #t) (k 2)))) (h "04l9sf7a5zb93zmwv8vvhd1mq71f5j0ri73hc54y5yf53bis33q5") (f (quote (("simd") ("default" "cffi") ("cffi" "libc") ("bench"))))))

