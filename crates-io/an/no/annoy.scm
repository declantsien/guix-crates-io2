(define-module (crates-io an no annoy) #:use-module (crates-io))

(define-public crate-annoy-0.1.0 (c (n "annoy") (v "0.1.0") (d (list (d (n "ndarray") (r "^0.12.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)))) (h "1gby4jnp9gvbk9i4f7q0s9amabpl2ll8b1aj3783cdq70sy30kx6")))

