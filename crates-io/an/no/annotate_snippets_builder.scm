(define-module (crates-io an no annotate_snippets_builder) #:use-module (crates-io))

(define-public crate-annotate_snippets_builder-0.1.1 (c (n "annotate_snippets_builder") (v "0.1.1") (d (list (d (n "annotate-snippets") (r "^0.9.1") (d #t) (k 0)) (d (n "stringx") (r "^0.1.1") (d #t) (k 2)))) (h "0fc9dljxm7hhx482vv7cdlvi46z9mxyj8qbyaz5i8w88w3m0y48h") (f (quote (("yansi-term" "annotate-snippets/yansi-term") ("default" "color") ("color" "annotate-snippets/color")))) (y #t)))

