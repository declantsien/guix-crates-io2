(define-module (crates-io an no annotate-output) #:use-module (crates-io))

(define-public crate-annotate-output-0.1.0 (c (n "annotate-output") (v "0.1.0") (d (list (d (n "chrono") (r ">=0.4.0, <0.5.0") (d #t) (k 0)) (d (n "nix") (r ">=0.19.0, <0.20.0") (d #t) (k 0)))) (h "0ajxi78kyxc50qzxmr62q8fk6m8582yn8fxdcvs2qs5rs5fl9wl5")))

(define-public crate-annotate-output-0.1.1 (c (n "annotate-output") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "nix") (r "^0.24") (d #t) (k 0)))) (h "0wjlwc4mfsdl7hzgwga8dg178k347ddc6p1wdfnc5r2sm2m185pm")))

