(define-module (crates-io an no annotation-rs) #:use-module (crates-io))

(define-public crate-annotation-rs-0.1.0 (c (n "annotation-rs") (v "0.1.0") (d (list (d (n "derive") (r "^0.1.0") (d #t) (k 0) (p "annotation-rs-codegen")) (d (n "helpers") (r "^0.1.0") (d #t) (k 0) (p "annotation-rs-helpers")) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1wn3di2dxbpv2mqz2chsyyv5s09xwkmki1ny1367k4zy1zm31rhb") (f (quote (("annotation_reader"))))))

