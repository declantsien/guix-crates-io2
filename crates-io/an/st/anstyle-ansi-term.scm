(define-module (crates-io an st anstyle-ansi-term) #:use-module (crates-io))

(define-public crate-anstyle-ansi-term-0.1.1 (c (n "anstyle-ansi-term") (v "0.1.1") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "anstyle") (r "^0.2.0") (d #t) (k 0)))) (h "1jnz8h1rsxfghs9mx4hf3qfwhsy2r3b5bdpki6syqrdnfdd95ydb") (r "1.56.0")))

(define-public crate-anstyle-ansi-term-0.1.2 (c (n "anstyle-ansi-term") (v "0.1.2") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "anstyle") (r "^0.2.0") (d #t) (k 0)))) (h "1ar8zr9vkccw1i62ykq1f9mglrhd1hy73m1hc5p3wkr9zan3hd9x") (r "1.56.0")))

(define-public crate-anstyle-ansi-term-0.2.0 (c (n "anstyle-ansi-term") (v "0.2.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "anstyle") (r "^0.3.0") (d #t) (k 0)))) (h "0ypy2z2gd2wbmsal9n5jc6qz0m8ccz7mykvvi71ssds803h2gbdh") (r "1.64.0")))

(define-public crate-anstyle-ansi-term-1.0.0 (c (n "anstyle-ansi-term") (v "1.0.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "anstyle") (r "^1.0.0") (d #t) (k 0)))) (h "0f2kkfay4qfn9s8h9m7gv8ppwqjinwpbw4p1c2pbp5kj34kqj12n") (r "1.64.0")))

(define-public crate-anstyle-ansi-term-1.0.1 (c (n "anstyle-ansi-term") (v "1.0.1") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "anstyle") (r "^1.0.0") (d #t) (k 0)))) (h "1ik824m9sj2l33zlxc82c5jpdfzvpgvicyzfiswcplkv0y88zzwx") (r "1.65.0")))

