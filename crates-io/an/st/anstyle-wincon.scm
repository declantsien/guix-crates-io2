(define-module (crates-io an st anstyle-wincon) #:use-module (crates-io))

(define-public crate-anstyle-wincon-0.0.1 (c (n "anstyle-wincon") (v "0.0.1") (d (list (d (n "anstyle") (r "^0.2.5") (d #t) (k 0)) (d (n "lexopt") (r "^0.3.0") (d #t) (k 2)) (d (n "windows-sys") (r "^0.45.0") (f (quote ("Win32_System_Console" "Win32_Foundation"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1321pm5xd5a055498flk8k0m8ivniijjf932i7g4iy8mr8jn54i7") (r "1.64.0")))

(define-public crate-anstyle-wincon-0.1.0 (c (n "anstyle-wincon") (v "0.1.0") (d (list (d (n "anstyle") (r "^0.3.0") (d #t) (k 0)) (d (n "lexopt") (r "^0.3.0") (d #t) (k 2)) (d (n "windows-sys") (r "^0.45.0") (f (quote ("Win32_System_Console" "Win32_Foundation"))) (d #t) (t "cfg(windows)") (k 0)))) (h "03izinva6dhnr8d3v3zk3nz2m0x52rv7zm81dcv3289f59mywc18") (r "1.64.0")))

(define-public crate-anstyle-wincon-0.1.1 (c (n "anstyle-wincon") (v "0.1.1") (d (list (d (n "anstyle") (r "^0.3.0") (d #t) (k 0)) (d (n "lexopt") (r "^0.3.0") (d #t) (k 2)) (d (n "windows-sys") (r "^0.45.0") (f (quote ("Win32_System_Console" "Win32_Foundation"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1yancs8y33fhyk38pvwi5gnr42gdd5r5m7yv0hwdrljlbbzsgvf4") (r "1.64.0")))

(define-public crate-anstyle-wincon-0.2.0 (c (n "anstyle-wincon") (v "0.2.0") (d (list (d (n "anstyle") (r "^0.3.0") (d #t) (k 0)) (d (n "lexopt") (r "^0.3.0") (d #t) (k 2)) (d (n "windows-sys") (r "^0.45.0") (f (quote ("Win32_System_Console" "Win32_Foundation"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1yil703c9gp5hn1d8fn5m3dr4mlwml80v6mvhwr9y52v2kv7l4n3") (r "1.64.0")))

(define-public crate-anstyle-wincon-1.0.0 (c (n "anstyle-wincon") (v "1.0.0") (d (list (d (n "anstyle") (r "^1.0.0") (d #t) (k 0)) (d (n "lexopt") (r "^0.3.0") (d #t) (k 2)) (d (n "windows-sys") (r "^0.48.0") (f (quote ("Win32_System_Console" "Win32_Foundation"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1zc8x98wwvnk0z123yhcjzzv9mqxa328p1qq1v3qmpa0lf8q5kab") (r "1.64.0")))

(define-public crate-anstyle-wincon-1.0.1 (c (n "anstyle-wincon") (v "1.0.1") (d (list (d (n "anstyle") (r "^1.0.0") (d #t) (k 0)) (d (n "lexopt") (r "^0.3.0") (d #t) (k 2)) (d (n "windows-sys") (r "^0.48.0") (f (quote ("Win32_System_Console" "Win32_Foundation"))) (d #t) (t "cfg(windows)") (k 0)))) (h "12714vwjf4c1wm3qf49m5vmd93qvq2nav6zpjc0bxbh3ayjby2hq") (r "1.64.0")))

(define-public crate-anstyle-wincon-1.0.2 (c (n "anstyle-wincon") (v "1.0.2") (d (list (d (n "anstyle") (r "^1.0.0") (d #t) (k 0)) (d (n "lexopt") (r "^0.3.0") (d #t) (k 2)) (d (n "windows-sys") (r "^0.48.0") (f (quote ("Win32_System_Console" "Win32_Foundation"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0k6gcx8qih4gzb5glrl37rqvj5qc893cvkkssdnjjm4iw02snxy6") (r "1.64.0")))

(define-public crate-anstyle-wincon-2.0.0 (c (n "anstyle-wincon") (v "2.0.0") (d (list (d (n "anstyle") (r "^1.0.0") (d #t) (k 0)) (d (n "lexopt") (r "^0.3.0") (d #t) (k 2)) (d (n "windows-sys") (r "^0.48.0") (f (quote ("Win32_System_Console" "Win32_Foundation"))) (d #t) (t "cfg(windows)") (k 0)))) (h "19fz0c0d8shk2aq443ffl2fi2pxqbxv1nrvify66appajsv23g6f") (r "1.64.0")))

(define-public crate-anstyle-wincon-2.1.0 (c (n "anstyle-wincon") (v "2.1.0") (d (list (d (n "anstyle") (r "^1.0.0") (d #t) (k 0)) (d (n "lexopt") (r "^0.3.0") (d #t) (k 2)) (d (n "windows-sys") (r "^0.48.0") (f (quote ("Win32_System_Console" "Win32_Foundation"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1zcxnwgmgr2578j4kah0mqzx2y5bq4zapkk6l21i59fzqq84vxaq") (r "1.70.0")))

(define-public crate-anstyle-wincon-3.0.0 (c (n "anstyle-wincon") (v "3.0.0") (d (list (d (n "anstyle") (r "^1.0.0") (d #t) (k 0)) (d (n "lexopt") (r "^0.3.0") (d #t) (k 2)) (d (n "windows-sys") (r "^0.48.0") (f (quote ("Win32_System_Console" "Win32_Foundation"))) (d #t) (t "cfg(windows)") (k 0)))) (h "15ppq9ny0vr1nb6yrvmc00jk54fmdn4chwy3yyykgykdr5bclf02") (r "1.70.0")))

(define-public crate-anstyle-wincon-3.0.1 (c (n "anstyle-wincon") (v "3.0.1") (d (list (d (n "anstyle") (r "^1.0.0") (d #t) (k 0)) (d (n "lexopt") (r "^0.3.0") (d #t) (k 2)) (d (n "windows-sys") (r "^0.48.0") (f (quote ("Win32_System_Console" "Win32_Foundation"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0a066gr4p7bha8qwnxyrpbrqzjdvk8l7pdg7isljimpls889ssgh") (r "1.70.0")))

(define-public crate-anstyle-wincon-3.0.2 (c (n "anstyle-wincon") (v "3.0.2") (d (list (d (n "anstyle") (r "^1.0.0") (d #t) (k 0)) (d (n "lexopt") (r "^0.3.0") (d #t) (k 2)) (d (n "windows-sys") (r "^0.52.0") (f (quote ("Win32_System_Console" "Win32_Foundation"))) (d #t) (t "cfg(windows)") (k 0)))) (h "19v0fv400bmp4niqpzxnhg83vz12mmqv7l2l8vi80qcdxj0lpm8w") (r "1.70.0")))

(define-public crate-anstyle-wincon-3.0.3 (c (n "anstyle-wincon") (v "3.0.3") (d (list (d (n "anstyle") (r "^1.0.0") (d #t) (k 0)) (d (n "lexopt") (r "^0.3.0") (d #t) (k 2)) (d (n "windows-sys") (r "^0.52.0") (f (quote ("Win32_System_Console" "Win32_Foundation"))) (d #t) (t "cfg(windows)") (k 0)))) (h "06gv2vbj4hvwb8fxqjmvabp5kx2w01cjgh86pd98y1mpzr4q98v1") (r "1.65.0")))

