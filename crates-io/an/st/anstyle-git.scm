(define-module (crates-io an st anstyle-git) #:use-module (crates-io))

(define-public crate-anstyle-git-0.2.1 (c (n "anstyle-git") (v "0.2.1") (d (list (d (n "anstyle") (r "^0.2.0") (d #t) (k 0)))) (h "0dkag1pbz8x5pysp8kfksc1mvlf1crlabf5fr3m8wg01av5f4j1f") (r "1.56.0")))

(define-public crate-anstyle-git-0.2.2 (c (n "anstyle-git") (v "0.2.2") (d (list (d (n "anstyle") (r "^0.2.0") (d #t) (k 0)))) (h "1hvcyg2023samdzqrsm77bbbnw5sm773w6x02fp2gzw4gf7paarn") (r "1.56.0")))

(define-public crate-anstyle-git-0.2.3 (c (n "anstyle-git") (v "0.2.3") (d (list (d (n "anstyle") (r "^0.2.0") (d #t) (k 0)))) (h "031npw7l6ihbwm04f2rqvqy47gfyg470cb6yr84zcixgdgycdzmd") (r "1.56.0")))

(define-public crate-anstyle-git-0.3.0 (c (n "anstyle-git") (v "0.3.0") (d (list (d (n "anstyle") (r "^0.3.0") (d #t) (k 0)))) (h "0l26gzy8bjixk7k6vnh8p580l5nnyrdsy9kfim8382k67366a001") (r "1.64.0")))

(define-public crate-anstyle-git-1.0.0 (c (n "anstyle-git") (v "1.0.0") (d (list (d (n "anstyle") (r "^1.0.0") (d #t) (k 0)))) (h "1y9drrc16rn8n7j68a6xj90hjjjp2h5zizzand8wcc7390knfjbc") (r "1.64.0")))

(define-public crate-anstyle-git-1.0.1 (c (n "anstyle-git") (v "1.0.1") (d (list (d (n "anstyle") (r "^1.0.0") (d #t) (k 0)))) (h "0g4gki7nb6f10lx8dq3np0xwk026257qg34yccg73dd6zakshfc1") (r "1.65.0")))

