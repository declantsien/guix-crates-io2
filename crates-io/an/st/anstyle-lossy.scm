(define-module (crates-io an st anstyle-lossy) #:use-module (crates-io))

(define-public crate-anstyle-lossy-0.1.1 (c (n "anstyle-lossy") (v "0.1.1") (d (list (d (n "anstyle") (r "^0.2.0") (d #t) (k 0)))) (h "0f1gfb9vj5n875pdk1cas7rrivcxj7k1rxyyw4ln0bqc0jg2hv8k") (r "1.56.0")))

(define-public crate-anstyle-lossy-0.2.0 (c (n "anstyle-lossy") (v "0.2.0") (d (list (d (n "anstyle") (r "^0.3.0") (d #t) (k 0)))) (h "1kyi3xllzj4w2sa85lvhlmrnq285vbm2r4584zaf88j0dr4zlx72") (r "1.64.0")))

(define-public crate-anstyle-lossy-1.0.0 (c (n "anstyle-lossy") (v "1.0.0") (d (list (d (n "anstyle") (r "^1.0.0") (d #t) (k 0)))) (h "09w7bd16cjfp6fsd2l0z5kl90b6rwpxxnyxi4jh1dyzkahl2m8iq") (r "1.64.0")))

(define-public crate-anstyle-lossy-1.1.0 (c (n "anstyle-lossy") (v "1.1.0") (d (list (d (n "anstyle") (r "^1.0.0") (d #t) (k 0)))) (h "10c1cg8vjb7mxwky2xc8j7zdp15i1qvpmd2w6nlsxm6vcx3l9859") (r "1.70.0")))

(define-public crate-anstyle-lossy-1.1.1 (c (n "anstyle-lossy") (v "1.1.1") (d (list (d (n "anstyle") (r "^1.0.0") (d #t) (k 0)))) (h "017hmj6dqmazvjlidifivn4c6zg6sv654l68cl0ipqh6kxczdkvg") (r "1.65.0")))

