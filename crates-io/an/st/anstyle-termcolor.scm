(define-module (crates-io an st anstyle-termcolor) #:use-module (crates-io))

(define-public crate-anstyle-termcolor-0.1.1 (c (n "anstyle-termcolor") (v "0.1.1") (d (list (d (n "anstyle") (r "^0.1.0") (d #t) (k 0)) (d (n "termcolor") (r "^1.1.3") (d #t) (k 0)))) (h "007w1y1cc77bivjk9p4q9crafxl744claiwkqbc83y6p7ahgcq6l") (r "1.56.0")))

(define-public crate-anstyle-termcolor-0.2.0 (c (n "anstyle-termcolor") (v "0.2.0") (d (list (d (n "anstyle") (r "^0.2.0") (d #t) (k 0)) (d (n "termcolor") (r "^1.1.3") (d #t) (k 0)))) (h "1pxhgllckpwa0xgnn04ij3w6p045j7yig5i1sgjapp6rdjj2imjj") (r "1.56.0")))

(define-public crate-anstyle-termcolor-0.2.1 (c (n "anstyle-termcolor") (v "0.2.1") (d (list (d (n "anstyle") (r "^0.2.0") (d #t) (k 0)) (d (n "termcolor") (r "^1.1.3") (d #t) (k 0)))) (h "1flhv2c1bww6nqw34ygqfmhm9g8jchgfi2hq2a7wlb037y4y7c92") (r "1.56.0")))

(define-public crate-anstyle-termcolor-0.2.2 (c (n "anstyle-termcolor") (v "0.2.2") (d (list (d (n "anstyle") (r "^0.2.0") (d #t) (k 0)) (d (n "termcolor") (r "^1.1.3") (d #t) (k 0)))) (h "1irpn53crh7fl9hgvqxc0nszbfw2bv66rk6zg81i63s9xrvf7pfj") (r "1.56.0")))

(define-public crate-anstyle-termcolor-0.3.0 (c (n "anstyle-termcolor") (v "0.3.0") (d (list (d (n "anstyle") (r "^0.3.0") (d #t) (k 0)) (d (n "termcolor") (r "^1.1.3") (d #t) (k 0)))) (h "0xzanwd5nsmnpzqn3s0731r8fbwlpaxsllsqfas14p00l9rj5sq5") (r "1.64.0")))

(define-public crate-anstyle-termcolor-1.0.0 (c (n "anstyle-termcolor") (v "1.0.0") (d (list (d (n "anstyle") (r "^1.0.0") (d #t) (k 0)) (d (n "termcolor") (r "^1.1.3") (d #t) (k 0)))) (h "10q7pjyda84k8rgfs6rprl24f1h7hj6rwgkr7ld5v9bl9pl60ggy") (r "1.64.0")))

(define-public crate-anstyle-termcolor-1.1.0 (c (n "anstyle-termcolor") (v "1.1.0") (d (list (d (n "anstyle") (r "^1.0.0") (d #t) (k 0)) (d (n "termcolor") (r "^1.1.3") (d #t) (k 0)))) (h "0d38wnbams9zzlj93yfriag0yaai3g3w7bkw2xxqlk0z3x0x3hqi") (r "1.70.0")))

(define-public crate-anstyle-termcolor-1.1.1 (c (n "anstyle-termcolor") (v "1.1.1") (d (list (d (n "anstyle") (r "^1.0.0") (d #t) (k 0)) (d (n "termcolor") (r "^1.1.3") (d #t) (k 0)))) (h "1byyfvspvpghf019mrfnv56q4cirw9yj7i4sk3wk235szs7dwhl7") (r "1.65.0")))

