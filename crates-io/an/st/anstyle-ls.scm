(define-module (crates-io an st anstyle-ls) #:use-module (crates-io))

(define-public crate-anstyle-ls-0.1.1 (c (n "anstyle-ls") (v "0.1.1") (d (list (d (n "anstyle") (r "^0.2.0") (d #t) (k 0)))) (h "0g3c9xlwx2kbsi7g0pvdhci62dz4lnqia4r7vavr8ijrvs9ispzs") (r "1.56.0")))

(define-public crate-anstyle-ls-0.2.0 (c (n "anstyle-ls") (v "0.2.0") (d (list (d (n "anstyle") (r "^0.3.0") (d #t) (k 0)))) (h "0a0s91q26ifvx00yy2vqc3vy5w6rqldpiqwdk8mngj3ppkng4k9x") (r "1.64.0")))

(define-public crate-anstyle-ls-1.0.0 (c (n "anstyle-ls") (v "1.0.0") (d (list (d (n "anstyle") (r "^1.0.0") (d #t) (k 0)))) (h "0wr8idbihn566skfi7b57cqn1bl4a1kxv3snm0rz322s8dsf578f") (r "1.64.0")))

(define-public crate-anstyle-ls-1.0.1 (c (n "anstyle-ls") (v "1.0.1") (d (list (d (n "anstyle") (r "^1.0.0") (d #t) (k 0)))) (h "16bzcz9anbc4jk4am4k648xxhbd0j7hdg7zwsi81jq3caq9yaf9p") (r "1.64.0")))

(define-public crate-anstyle-ls-1.0.2 (c (n "anstyle-ls") (v "1.0.2") (d (list (d (n "anstyle") (r "^1.0.0") (d #t) (k 0)))) (h "0f9wdx51rcb8kv24wkcjzkascp6q5qb3rl6ryv6zn71xizw2icv0") (r "1.65.0")))

