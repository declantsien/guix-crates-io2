(define-module (crates-io an st anstyle-roff) #:use-module (crates-io))

(define-public crate-anstyle-roff-0.1.0 (c (n "anstyle-roff") (v "0.1.0") (d (list (d (n "anstyle") (r "^0.2.5") (d #t) (k 0)) (d (n "anstyle-lossy") (r "^0.1.1") (d #t) (k 0)) (d (n "cansi") (r "^2.2.1") (d #t) (k 0)) (d (n "roff") (r "^0.2.1") (d #t) (k 0)) (d (n "snapbox") (r "^0.3.3") (d #t) (k 2)))) (h "0jn6ikx5p1sh1bvapxysdj74wbpj1msx0jj1ncwh17ihik9q838f") (r "1.64.0")))

(define-public crate-anstyle-roff-0.2.0 (c (n "anstyle-roff") (v "0.2.0") (d (list (d (n "anstyle") (r "^0.3.0") (d #t) (k 0)) (d (n "anstyle-lossy") (r "^0.2.0") (d #t) (k 0)) (d (n "cansi") (r "^2.2.1") (d #t) (k 0)) (d (n "roff") (r "^0.2.1") (d #t) (k 0)) (d (n "snapbox") (r "^0.4.7") (d #t) (k 2)))) (h "0lrizglfw4k62gvjmhxc3a2n72rz7dh4cx36jifdpfq9yk21nbb3") (r "1.64.0")))

(define-public crate-anstyle-roff-0.3.0 (c (n "anstyle-roff") (v "0.3.0") (d (list (d (n "anstyle") (r "^1.0.0") (d #t) (k 0)) (d (n "anstyle-lossy") (r "^1.0.0") (d #t) (k 0)) (d (n "cansi") (r "^2.2.1") (d #t) (k 0)) (d (n "roff") (r "^0.2.1") (d #t) (k 0)) (d (n "snapbox") (r "^0.4.10") (d #t) (k 2)))) (h "0vckdwdavmn3ak3yxjxl1h14sixxmn2i95acn1m9wfzxgfdh9935") (r "1.64.0")))

(define-public crate-anstyle-roff-0.3.1 (c (n "anstyle-roff") (v "0.3.1") (d (list (d (n "anstyle") (r "^1.0.0") (d #t) (k 0)) (d (n "anstyle-lossy") (r "^1.0.0") (d #t) (k 0)) (d (n "cansi") (r "^2.2.1") (d #t) (k 0)) (d (n "roff") (r "^0.2.1") (d #t) (k 0)) (d (n "snapbox") (r "^0.4.11") (d #t) (k 2)))) (h "1h07ka0ag964z6j2wp5dr0xshzwc8xcqlh9jw1057lpls434gkz0") (r "1.64.0")))

(define-public crate-anstyle-roff-0.3.2 (c (n "anstyle-roff") (v "0.3.2") (d (list (d (n "anstyle") (r "^1.0.0") (d #t) (k 0)) (d (n "anstyle-lossy") (r "^1.0.0") (d #t) (k 0)) (d (n "cansi") (r "^2.2.1") (d #t) (k 0)) (d (n "roff") (r "^0.2.1") (d #t) (k 0)) (d (n "snapbox") (r "^0.4.12") (d #t) (k 2)))) (h "0xwrlmdhsdya9hgf2kq5ifg423cl987jsppdnsm982k0jv3d35w7") (r "1.70.0")))

(define-public crate-anstyle-roff-0.3.3 (c (n "anstyle-roff") (v "0.3.3") (d (list (d (n "anstyle") (r "^1.0.0") (d #t) (k 0)) (d (n "anstyle-lossy") (r "^1.0.0") (d #t) (k 0)) (d (n "cansi") (r "^2.2.1") (d #t) (k 0)) (d (n "roff") (r "^0.2.1") (d #t) (k 0)) (d (n "snapbox") (r "^0.5.0") (d #t) (k 2)))) (h "0myarj79fvv088yzs0l45y6ibn8myifbi1v610w9bsgwfln1aq10") (r "1.65.0")))

