(define-module (crates-io an st anstyle-svg) #:use-module (crates-io))

(define-public crate-anstyle-svg-0.1.0 (c (n "anstyle-svg") (v "0.1.0") (d (list (d (n "anstream") (r "^0.6") (k 0)) (d (n "anstyle") (r "^1.0.0") (d #t) (k 0)) (d (n "anstyle-lossy") (r "^1.0.0") (d #t) (k 0)) (d (n "html-escape") (r "^0.2.13") (d #t) (k 0)) (d (n "snapbox") (r "^0.5.1") (d #t) (k 2)) (d (n "unicode-width") (r "^0.1.11") (d #t) (k 0)))) (h "12cahhbxm5kd7iz9vczcsxrd1whpp2lwms4vafg811sc04lz7zl4") (r "1.70.0")))

(define-public crate-anstyle-svg-0.1.1 (c (n "anstyle-svg") (v "0.1.1") (d (list (d (n "anstream") (r "^0.6") (k 0)) (d (n "anstyle") (r "^1.0.0") (d #t) (k 0)) (d (n "anstyle-lossy") (r "^1.0.0") (d #t) (k 0)) (d (n "html-escape") (r "^0.2.13") (d #t) (k 0)) (d (n "snapbox") (r "^0.5.1") (d #t) (k 2)) (d (n "unicode-width") (r "^0.1.11") (d #t) (k 0)))) (h "0wfl5i2yfcrjg1z7ns84474zpzs59drpmmifrj7w5183dkh8f3r6") (r "1.70.0")))

(define-public crate-anstyle-svg-0.1.2 (c (n "anstyle-svg") (v "0.1.2") (d (list (d (n "anstream") (r "^0.6") (k 0)) (d (n "anstyle") (r "^1.0.0") (d #t) (k 0)) (d (n "anstyle-lossy") (r "^1.0.0") (d #t) (k 0)) (d (n "html-escape") (r "^0.2.13") (d #t) (k 0)) (d (n "snapbox") (r "^0.5.1") (d #t) (k 2)) (d (n "unicode-width") (r "^0.1.11") (d #t) (k 0)))) (h "0ncxmp0b3hzl5znb02ghq0lqrgqam7i36yjl44spgd7r7fnw2miw") (r "1.70.0")))

(define-public crate-anstyle-svg-0.1.3 (c (n "anstyle-svg") (v "0.1.3") (d (list (d (n "anstream") (r "^0.6") (k 0)) (d (n "anstyle") (r "^1.0.0") (d #t) (k 0)) (d (n "anstyle-lossy") (r "^1.0.0") (d #t) (k 0)) (d (n "html-escape") (r "^0.2.13") (d #t) (k 0)) (d (n "snapbox") (r "^0.5.1") (d #t) (k 2)) (d (n "unicode-width") (r "^0.1.11") (d #t) (k 0)))) (h "0yi8d0cv1spcisch35b1f7q7lz7j7yyircrnvgaxcj5l8zadlvcb") (r "1.70.0")))

(define-public crate-anstyle-svg-0.1.4 (c (n "anstyle-svg") (v "0.1.4") (d (list (d (n "anstream") (r "^0.6") (k 0)) (d (n "anstyle") (r "^1.0.0") (d #t) (k 0)) (d (n "anstyle-lossy") (r "^1.0.0") (d #t) (k 0)) (d (n "html-escape") (r "^0.2.13") (d #t) (k 0)) (d (n "snapbox") (r "^0.5.1") (d #t) (k 2)) (d (n "unicode-width") (r "^0.1.11") (d #t) (k 0)))) (h "1aniq4hyb3d3g1szldch7cgibacdl26g4cj11c7h2qyn8zwhpgxv") (r "1.65.0")))

