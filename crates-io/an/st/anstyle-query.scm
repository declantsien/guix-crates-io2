(define-module (crates-io an st anstyle-query) #:use-module (crates-io))

(define-public crate-anstyle-query-1.0.0 (c (n "anstyle-query") (v "1.0.0") (d (list (d (n "windows-sys") (r "^0.48.0") (f (quote ("Win32_System_Console" "Win32_Foundation"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0js9bgpqz21g0p2nm350cba1d0zfyixsma9lhyycic5sw55iv8aw") (r "1.64.0")))

(define-public crate-anstyle-query-1.0.1 (c (n "anstyle-query") (v "1.0.1") (d (list (d (n "windows-sys") (r "^0.52.0") (f (quote ("Win32_System_Console" "Win32_Foundation"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0j37b7j95ivcpavhp8sssapqamw2zmdna2c21x01h94dygqii8x3") (r "1.70.0")))

(define-public crate-anstyle-query-1.0.2 (c (n "anstyle-query") (v "1.0.2") (d (list (d (n "windows-sys") (r "^0.52.0") (f (quote ("Win32_System_Console" "Win32_Foundation"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0j3na4b1nma39g4x7cwvj009awxckjf3z2vkwhldgka44hqj72g2") (r "1.70.0")))

(define-public crate-anstyle-query-1.0.3 (c (n "anstyle-query") (v "1.0.3") (d (list (d (n "windows-sys") (r "^0.52.0") (f (quote ("Win32_System_Console" "Win32_Foundation"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1x9pyl231rry5g45dvkdb2sfnl2dx2f4qd9a5v3ml8kr9ryr0k56") (r "1.65.0")))

