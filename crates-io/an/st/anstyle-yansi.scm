(define-module (crates-io an st anstyle-yansi) #:use-module (crates-io))

(define-public crate-anstyle-yansi-0.1.1 (c (n "anstyle-yansi") (v "0.1.1") (d (list (d (n "anstyle") (r "^0.1.0") (d #t) (k 0)) (d (n "yansi") (r "^0.5.1") (d #t) (k 0)))) (h "1dsm5q1j5zxwfacy8gvmvww9bkvi4m53wjn85mz9x0gz71h250v3") (r "1.56.0")))

(define-public crate-anstyle-yansi-0.2.0 (c (n "anstyle-yansi") (v "0.2.0") (d (list (d (n "anstyle") (r "^0.2.0") (d #t) (k 0)) (d (n "yansi") (r "^0.5.1") (d #t) (k 0)))) (h "1nnd20kcpk3nnd6yd9xf7wlnfw4f5x13ia23ffyxsal0pr6g1sdg") (r "1.56.0")))

(define-public crate-anstyle-yansi-0.2.1 (c (n "anstyle-yansi") (v "0.2.1") (d (list (d (n "anstyle") (r "^0.2.0") (d #t) (k 0)) (d (n "yansi") (r "^0.5.1") (d #t) (k 0)))) (h "1ss1f4ga4lnwdca25xnjyn993xvwds3qbcxlmg3xz4xil19d3dsf") (r "1.56.0")))

(define-public crate-anstyle-yansi-0.2.2 (c (n "anstyle-yansi") (v "0.2.2") (d (list (d (n "anstyle") (r "^0.2.0") (d #t) (k 0)) (d (n "yansi") (r "^0.5.1") (d #t) (k 0)))) (h "0bbll03744f7yiqhrqn7fry7a7mpfpr6lhr0mvjdql7wfl00gn90") (r "1.56.0")))

(define-public crate-anstyle-yansi-0.3.0 (c (n "anstyle-yansi") (v "0.3.0") (d (list (d (n "anstyle") (r "^0.3.0") (d #t) (k 0)) (d (n "yansi") (r "^0.5.1") (d #t) (k 0)))) (h "04dracxgxpn8rcq2cins3vm7xl5cy5vrrdw7h9f5z2m7228kn8yw") (r "1.64.0")))

(define-public crate-anstyle-yansi-1.0.0 (c (n "anstyle-yansi") (v "1.0.0") (d (list (d (n "anstyle") (r "^1.0.0") (d #t) (k 0)) (d (n "yansi") (r "^0.5.1") (d #t) (k 0)))) (h "0a0zkqlpg07pvpczw7pgpa2y10d9y0jvgs68xdgwx8k05nnjazvv") (r "1.64.0")))

(define-public crate-anstyle-yansi-1.0.1 (c (n "anstyle-yansi") (v "1.0.1") (d (list (d (n "anstyle") (r "^1.0.0") (d #t) (k 0)) (d (n "yansi") (r "^0.5.1") (d #t) (k 0)))) (h "16xyhaiislfabf94jhxchmjvcsb616yz4s9pjbw7fjxj0y6730sc") (r "1.65.0")))

