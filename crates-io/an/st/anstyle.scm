(define-module (crates-io an st anstyle) #:use-module (crates-io))

(define-public crate-anstyle-0.1.1 (c (n "anstyle") (v "0.1.1") (h "167nhr4qrcbmr74n8rmzizd3v60hk2qsxq3dqpg8dk51nxm98xvg") (r "1.56.0")))

(define-public crate-anstyle-0.2.0 (c (n "anstyle") (v "0.2.0") (h "0f34k15jg8d0khw0gsi2z9jhsaadi34zmxh7a6wfnjr561jv6vls") (f (quote (("std") ("default" "std")))) (r "1.56.0")))

(define-public crate-anstyle-0.2.1 (c (n "anstyle") (v "0.2.1") (h "1hfapxnx16dph5gpgy4c4dkr1b4jll3nv607gba1d0xh8i4lqi8w") (f (quote (("std") ("default" "std")))) (r "1.56.0")))

(define-public crate-anstyle-0.2.2 (c (n "anstyle") (v "0.2.2") (h "1p6pyp0h4k8p4d9g8mqnjs0fxi1568wlkgrqd6m2w0q0h427720s") (f (quote (("std") ("default" "std")))) (r "1.56.0")))

(define-public crate-anstyle-0.2.3 (c (n "anstyle") (v "0.2.3") (h "17dryfwbjxqk4c88a3pylv62b49j1xkgvd0r3p5nq8yg6ij77hhy") (f (quote (("std") ("default" "std")))) (r "1.56.0")))

(define-public crate-anstyle-0.2.4 (c (n "anstyle") (v "0.2.4") (h "05vbychs1cmcv9nzyjvymc02p20andwx1mbzlq40rsgqjwl1jjm1") (f (quote (("std") ("default" "std")))) (r "1.56.0")))

(define-public crate-anstyle-0.2.5 (c (n "anstyle") (v "0.2.5") (h "1dvdy7ndv5ycg2486cqlfvzvgz17jfs61k9dwd0iq17y8k4hxfsq") (f (quote (("std") ("default" "std")))) (r "1.56.0")))

(define-public crate-anstyle-0.2.6 (c (n "anstyle") (v "0.2.6") (d (list (d (n "lexopt") (r "^0.3.0") (d #t) (k 2)))) (h "0x9gn09msnmccl55n44g7344cav6a4ns0gxszg6q1p0jl87v800x") (f (quote (("std") ("default" "std")))) (r "1.64.0")))

(define-public crate-anstyle-0.2.7 (c (n "anstyle") (v "0.2.7") (d (list (d (n "lexopt") (r "^0.3.0") (d #t) (k 2)))) (h "1scnq94l7sjsds2yn7jv09hww16pnli70grgrvb29cdnws2bcwfz") (f (quote (("std") ("default" "std")))) (r "1.64.0")))

(define-public crate-anstyle-0.2.8 (c (n "anstyle") (v "0.2.8") (d (list (d (n "lexopt") (r "^0.3.0") (d #t) (k 2)))) (h "10kdjcyks9hcvmhk44afagnrxi4pczg6jnldjdadzbi4kyi2wqah") (f (quote (("std") ("default" "std")))) (r "1.64.0")))

(define-public crate-anstyle-0.3.0 (c (n "anstyle") (v "0.3.0") (d (list (d (n "lexopt") (r "^0.3.0") (d #t) (k 2)))) (h "16fpid23xqr7g9jl5iassajkjkk16ys3hf9knharnjja9icvf1lx") (f (quote (("std") ("default" "std")))) (r "1.64.0")))

(define-public crate-anstyle-0.3.1 (c (n "anstyle") (v "0.3.1") (d (list (d (n "lexopt") (r "^0.3.0") (d #t) (k 2)))) (h "1pc6fvfjcl7k65hm1k7igxibwkv826ifici61jmh46w56g69gil0") (f (quote (("std") ("default" "std")))) (r "1.64.0")))

(define-public crate-anstyle-0.3.2 (c (n "anstyle") (v "0.3.2") (d (list (d (n "lexopt") (r "^0.3.0") (d #t) (k 2)))) (h "18xcy951g0pyvbdydf9mkcklydgg2hmwal898qwk49jzq0s9dyy6") (f (quote (("std") ("default" "std")))) (r "1.64.0")))

(define-public crate-anstyle-0.3.3 (c (n "anstyle") (v "0.3.3") (d (list (d (n "lexopt") (r "^0.3.0") (d #t) (k 2)))) (h "0g4qz3fw9pgv254mwc9hinc013b45li6qb2vrv8w9y31nakw4fs5") (f (quote (("std") ("default" "std")))) (r "1.64.0")))

(define-public crate-anstyle-0.3.4 (c (n "anstyle") (v "0.3.4") (d (list (d (n "lexopt") (r "^0.3.0") (d #t) (k 2)))) (h "1ks7qwx1wcvljy4xzrr9arkr2c9crripjkk8vcm81ah149fbb80v") (f (quote (("std") ("default" "std")))) (r "1.64.0")))

(define-public crate-anstyle-0.3.5 (c (n "anstyle") (v "0.3.5") (d (list (d (n "lexopt") (r "^0.3.0") (d #t) (k 2)))) (h "1ll42s5ch4ldp0fw8laidqji540p7higd06h2v111qq2pn0rxsi3") (f (quote (("std") ("default" "std")))) (r "1.64.0")))

(define-public crate-anstyle-1.0.0 (c (n "anstyle") (v "1.0.0") (d (list (d (n "lexopt") (r "^0.3.0") (d #t) (k 2)))) (h "0zbazbfqs4mfw93573f61iy8c78vbbv824m3w206bbljpy39mva1") (f (quote (("std") ("default" "std")))) (r "1.64.0")))

(define-public crate-anstyle-1.0.1 (c (n "anstyle") (v "1.0.1") (d (list (d (n "lexopt") (r "^0.3.0") (d #t) (k 2)))) (h "1kff80219d5rvvi407wky2zdlb0naxvbbg005s274pidbxfdlc1s") (f (quote (("std") ("default" "std")))) (r "1.64.0")))

(define-public crate-anstyle-1.0.2 (c (n "anstyle") (v "1.0.2") (d (list (d (n "lexopt") (r "^0.3.0") (d #t) (k 2)))) (h "1shjvv4kwampx4ca8h0394iwmykid65rjcx78mc2wlw17z4c5i0m") (f (quote (("std") ("default" "std")))) (r "1.64.0")))

(define-public crate-anstyle-1.0.3 (c (n "anstyle") (v "1.0.3") (d (list (d (n "lexopt") (r "^0.3.0") (d #t) (k 2)))) (h "0ihfi7r8m3dkysxm5zrm7hchiakvx2v6p8vgxgjq6amvbfhg0jxq") (f (quote (("std") ("default" "std")))) (r "1.70.0")))

(define-public crate-anstyle-1.0.4 (c (n "anstyle") (v "1.0.4") (d (list (d (n "lexopt") (r "^0.3.0") (d #t) (k 2)))) (h "11yxw02b6parn29s757z96rgiqbn8qy0fk9a3p3bhczm85dhfybh") (f (quote (("std") ("default" "std")))) (r "1.70.0")))

(define-public crate-anstyle-1.0.5 (c (n "anstyle") (v "1.0.5") (d (list (d (n "lexopt") (r "^0.3.0") (d #t) (k 2)))) (h "0822w1r2ql7i25dqzfwr5iljm4hkxkl30sk7rrksdd2arjjcxb1g") (f (quote (("std") ("default" "std")))) (r "1.70.0")))

(define-public crate-anstyle-1.0.6 (c (n "anstyle") (v "1.0.6") (d (list (d (n "lexopt") (r "^0.3.0") (d #t) (k 2)))) (h "1g1ngvxrz9d6xsymxzzzg581jzyz1sn8d0jpjcwxks07cff2c0c9") (f (quote (("std") ("default" "std")))) (r "1.70.0")))

(define-public crate-anstyle-1.0.7 (c (n "anstyle") (v "1.0.7") (d (list (d (n "lexopt") (r "^0.3.0") (d #t) (k 2)))) (h "06qxmrba0xbhv07jpdvrdrhw1hjlb9icj88bqvlnissz9bqgr383") (f (quote (("std") ("default" "std")))) (r "1.65.0")))

