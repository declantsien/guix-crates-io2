(define-module (crates-io an st anstyle-owo-colors) #:use-module (crates-io))

(define-public crate-anstyle-owo-colors-0.1.1 (c (n "anstyle-owo-colors") (v "0.1.1") (d (list (d (n "anstyle") (r "^0.1.0") (d #t) (k 0)) (d (n "owo-colors") (r "^3.4.0") (d #t) (k 0)))) (h "0ca45sfkv8k5yyvvyajs220q4y7ax3j7801kf06f8w9h3r1rjc69") (r "1.56.0")))

(define-public crate-anstyle-owo-colors-0.2.0 (c (n "anstyle-owo-colors") (v "0.2.0") (d (list (d (n "anstyle") (r "^0.2.0") (d #t) (k 0)) (d (n "owo-colors") (r "^3.4.0") (d #t) (k 0)))) (h "0044rkafg3cm84cqg2c5b19rpbhabf4x0wvlj38xxkf0qrbxzphz") (r "1.56.0")))

(define-public crate-anstyle-owo-colors-0.2.1 (c (n "anstyle-owo-colors") (v "0.2.1") (d (list (d (n "anstyle") (r "^0.2.0") (d #t) (k 0)) (d (n "owo-colors") (r "^3.4.0") (d #t) (k 0)))) (h "0sajsq5qfcp95vrr96pbanz5i0nb3c6r6virsvllbaxlsz72vc05") (r "1.56.0")))

(define-public crate-anstyle-owo-colors-0.2.2 (c (n "anstyle-owo-colors") (v "0.2.2") (d (list (d (n "anstyle") (r "^0.2.0") (d #t) (k 0)) (d (n "owo-colors") (r "^3.5.0") (d #t) (k 0)))) (h "1q8j58rmpv7saazd0k0ps53wwr5pz17bk8bbbd3nn3apvlw5lpv5") (r "1.56.0")))

(define-public crate-anstyle-owo-colors-0.3.0 (c (n "anstyle-owo-colors") (v "0.3.0") (d (list (d (n "anstyle") (r "^0.3.0") (d #t) (k 0)) (d (n "owo-colors") (r "^3.5.0") (d #t) (k 0)))) (h "0iwwd2mdqlfnyipmfjzicibzrwdw5jpbhsxqlm0a1k3cf76i2pn0") (r "1.64.0")))

(define-public crate-anstyle-owo-colors-1.0.0 (c (n "anstyle-owo-colors") (v "1.0.0") (d (list (d (n "anstyle") (r "^1.0.0") (d #t) (k 0)) (d (n "owo-colors") (r "^3.5.0") (d #t) (k 0)))) (h "1g4ypxzbs49fhgsxyikmqp4q0dgcqrprwaka62b83c1z79svcy77") (r "1.64.0")))

(define-public crate-anstyle-owo-colors-1.0.1 (c (n "anstyle-owo-colors") (v "1.0.1") (d (list (d (n "anstyle") (r "^1.0.0") (d #t) (k 0)) (d (n "owo-colors") (r "^3.5.0") (d #t) (k 0)))) (h "14x6qlil1wcmqr02gkmv5gbwbgxk0ka2491sdgi9hv5ya04crrmq") (r "1.64.0")))

(define-public crate-anstyle-owo-colors-2.0.0 (c (n "anstyle-owo-colors") (v "2.0.0") (d (list (d (n "anstyle") (r "^1.0.0") (d #t) (k 0)) (d (n "owo-colors") (r "^4.0.0") (d #t) (k 0)))) (h "065qifs7s4cl55dllbsaq6kzs8b7023ss4l3apxhv0l638cqh39y") (r "1.70.0")))

(define-public crate-anstyle-owo-colors-2.0.1 (c (n "anstyle-owo-colors") (v "2.0.1") (d (list (d (n "anstyle") (r "^1.0.0") (d #t) (k 0)) (d (n "owo-colors") (r "^4.0.0") (d #t) (k 0)))) (h "0sxgrwsp3m0cbxf2b1rr3kjmnx16lp2hl34b8v0cpvpavfwfnqp5") (r "1.65.0")))

