(define-module (crates-io an st anstyle-crossterm) #:use-module (crates-io))

(define-public crate-anstyle-crossterm-0.1.1 (c (n "anstyle-crossterm") (v "0.1.1") (d (list (d (n "anstyle") (r "^0.2.0") (d #t) (k 0)) (d (n "crossterm") (r "^0.25.0") (k 0)))) (h "0q64rdn3bc1ndxzjljq8kg8nlp4xqqx7915bi1zmyvhkqp8cmb2g") (r "1.56.0")))

(define-public crate-anstyle-crossterm-0.2.0 (c (n "anstyle-crossterm") (v "0.2.0") (d (list (d (n "anstyle") (r "^0.3.0") (d #t) (k 0)) (d (n "crossterm") (r "^0.26.1") (k 0)))) (h "1b6dm1dyljdikzirgxy7sgh3kz3whix0hc3xhpn7nn8f11hrwik5") (r "1.64.0")))

(define-public crate-anstyle-crossterm-1.0.0 (c (n "anstyle-crossterm") (v "1.0.0") (d (list (d (n "anstyle") (r "^1.0.0") (d #t) (k 0)) (d (n "crossterm") (r "^0.26.1") (k 0)))) (h "1abikac7nynx3l6gqw3s3ni93yi6lrdsp1minaln6r2pkccv821p") (r "1.64.0")))

(define-public crate-anstyle-crossterm-2.0.0 (c (n "anstyle-crossterm") (v "2.0.0") (d (list (d (n "anstyle") (r "^1.0.0") (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (f (quote ("windows"))) (k 0)))) (h "0qayhmxcg2h6w69dxxacphwa1qpvlbmcd1qr1si4kscwdaqk9ism") (r "1.70.0")))

(define-public crate-anstyle-crossterm-2.0.1 (c (n "anstyle-crossterm") (v "2.0.1") (d (list (d (n "anstyle") (r "^1.0.0") (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (f (quote ("windows"))) (k 0)))) (h "0g4hr6fgjmn8ar79787q7799vrws8zpjm0gwpc6bw59dwbg2lf6c") (r "1.65.0")))

