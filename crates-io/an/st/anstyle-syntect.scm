(define-module (crates-io an st anstyle-syntect) #:use-module (crates-io))

(define-public crate-anstyle-syntect-0.1.1 (c (n "anstyle-syntect") (v "0.1.1") (d (list (d (n "anstyle") (r "^0.2.0") (d #t) (k 0)) (d (n "syntect") (r "^5.0.0") (k 0)))) (h "1g96jzpwgpd0v1r6fj6fazky598iizsigyxmqrmbyxhir8wx0fi8") (r "1.56.0")))

(define-public crate-anstyle-syntect-0.1.2 (c (n "anstyle-syntect") (v "0.1.2") (d (list (d (n "anstyle") (r "^0.2.0") (d #t) (k 0)) (d (n "syntect") (r "^5.0.0") (k 0)))) (h "1z3fp32y60gr6p1fdlcvg6dj1zyfvgwzggygy30rv2gklwz4nkd5") (r "1.56.0")))

(define-public crate-anstyle-syntect-0.2.0 (c (n "anstyle-syntect") (v "0.2.0") (d (list (d (n "anstyle") (r "^0.3.0") (d #t) (k 0)) (d (n "syntect") (r "^5.0.0") (k 0)))) (h "09yhkr0g7zymp0z71finlbmpr54jr6dmlm0skhk6zhb7xryrrp9c") (r "1.64.0")))

(define-public crate-anstyle-syntect-1.0.0 (c (n "anstyle-syntect") (v "1.0.0") (d (list (d (n "anstyle") (r "^1.0.0") (d #t) (k 0)) (d (n "syntect") (r "^5.0.0") (k 0)))) (h "1i88ywvf76sipq6gk71z3pbgkiyg86dv3hp43vmi3ck8zkrlcwv4") (r "1.64.0")))

(define-public crate-anstyle-syntect-1.0.1 (c (n "anstyle-syntect") (v "1.0.1") (d (list (d (n "anstyle") (r "^1.0.0") (d #t) (k 0)) (d (n "syntect") (r "^5.0.0") (k 0)))) (h "0xdvzz2zch6gp3w8grs6nnjvyp2lylm75iydspargknqiml01w4q") (r "1.65.0")))

