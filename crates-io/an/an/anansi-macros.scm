(define-module (crates-io an an anansi-macros) #:use-module (crates-io))

(define-public crate-anansi-macros-0.1.0 (c (n "anansi-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "046ph9ing0lrlcbv5wa19hqiv6klkkd3v2g0qzvaw2gwx3fig5f9")))

(define-public crate-anansi-macros-0.2.0 (c (n "anansi-macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "11wzzd0gzvdwwifskcrg720014c0xizqm269n95gql8c17bvbrfw")))

(define-public crate-anansi-macros-0.3.0 (c (n "anansi-macros") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1k5grzi08m29kwwgap3ckl85kjxnx31b64dss0sy8ax85si2s3jc")))

(define-public crate-anansi-macros-0.4.0 (c (n "anansi-macros") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "0fr6731nf8rhrln3n0i95bk547s3800dly0nir4krvjqjpqbxf9i")))

(define-public crate-anansi-macros-0.4.1 (c (n "anansi-macros") (v "0.4.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1ljxp1vi0vmp5g47i3f92kzfp8cmjr0marrpr5faz831grgm4pqq")))

(define-public crate-anansi-macros-0.5.0 (c (n "anansi-macros") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "19xnxyz5a9f5rlvpmabh77f0ip485in5bb2nkahr98ib0hzrp2v5")))

(define-public crate-anansi-macros-0.6.0 (c (n "anansi-macros") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1hapyz59gmnsjmvangyakcblipsk6ba4wcmj54g06nwgcwrl8lwb")))

(define-public crate-anansi-macros-0.7.0 (c (n "anansi-macros") (v "0.7.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "0lf664zwky6qb17b6s5khzdw5q6cicjpg5rv81j7p6vmdb9x6m41")))

(define-public crate-anansi-macros-0.8.0 (c (n "anansi-macros") (v "0.8.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1hvba8hqn883ik4zhi9h3s475yl5ywg6zm0ygqqjzpnv3pmaxgmg")))

(define-public crate-anansi-macros-0.9.0 (c (n "anansi-macros") (v "0.9.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1a3j2mlpmlqg47wffvdawb56pkkykrq7lyr52w9c50iriijb1702")))

(define-public crate-anansi-macros-0.10.0 (c (n "anansi-macros") (v "0.10.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "0a1yjfcv4r3y73aksp508kwnlvh9lr6bg9gpfy02sfi3swybxshr")))

(define-public crate-anansi-macros-0.11.0 (c (n "anansi-macros") (v "0.11.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1zb5ciklm2rhkiyccs3kbnv3046xnf5jjprc9r5219a7fn7cv5w0")))

(define-public crate-anansi-macros-0.11.1 (c (n "anansi-macros") (v "0.11.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "0da1jri7dhvd0xadsc4cksrh2z34pp1vywf154kf52nz900ys3i9")))

(define-public crate-anansi-macros-0.12.0 (c (n "anansi-macros") (v "0.12.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "11n96msahx7v015gykgqhic36d7zwycpy3341d63mzwc29cnj6fn")))

(define-public crate-anansi-macros-0.12.1 (c (n "anansi-macros") (v "0.12.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "137i776qpzn903llg5pfy7cxwb3dxybbljcnpsllbg31qvb4q7bl")))

(define-public crate-anansi-macros-0.13.0 (c (n "anansi-macros") (v "0.13.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "10ibdqbsrw955sk9c2a70j9c0v7xksh0273p8h8rqq2jv97krjn9")))

(define-public crate-anansi-macros-0.13.1 (c (n "anansi-macros") (v "0.13.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "0d59g0x8rw2j9dmkvi0v9r68hik0v8kixzax9bpv1f9dlfa9347a")))

(define-public crate-anansi-macros-0.13.2 (c (n "anansi-macros") (v "0.13.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "0c6pvnfki8m8xpkhgw84zq5qjai31cp7sg7scnnp5qsz225qn34k")))

(define-public crate-anansi-macros-0.13.3 (c (n "anansi-macros") (v "0.13.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "0vq75x86bs6cwll5sqbwyhcfsh24v3dvxvvy26c35igp04hkbg84")))

(define-public crate-anansi-macros-0.14.0 (c (n "anansi-macros") (v "0.14.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1zhwlf56c2fz4j41r2xi4nr614jj7gh6ljrcv71c8pcpjvglij9k")))

(define-public crate-anansi-macros-0.14.1 (c (n "anansi-macros") (v "0.14.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1risn5yqjf2ncxkz1dlrxr8w267s0nmb9vnjm2mj5pf3c7cganxf")))

(define-public crate-anansi-macros-0.14.2 (c (n "anansi-macros") (v "0.14.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "0f0ijqsa0pps22x3avb7kxc8gbjpifi7jxnig5qyybs63bz9hqb5")))

