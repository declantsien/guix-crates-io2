(define-module (crates-io an an ananas) #:use-module (crates-io))

(define-public crate-ananas-0.1.0 (c (n "ananas") (v "0.1.0") (h "1q4mmfgv39554g0833gyypy1zs8md6vxvn7mg4n6mkwyvms84dy7")))

(define-public crate-ananas-0.2.0 (c (n "ananas") (v "0.2.0") (h "13sqqnq8n6ak8gvnp6sh04r7k4w3hdw9816nzv3lcan136837yh7")))

