(define-module (crates-io an an ananc) #:use-module (crates-io))

(define-public crate-ananc-0.1.0 (c (n "ananc") (v "0.1.0") (h "15d8xsqcn0s34ni2gvj3ajygy8ij2q72dn82pg63cnycdm69kvcr")))

(define-public crate-ananc-0.2.0 (c (n "ananc") (v "0.2.0") (d (list (d (n "which") (r "^4.3.0") (d #t) (k 0)))) (h "1jfnafc4lrkcqf575q2bc7brnj2xqv6v70hqxkj75py4vwi14dh5")))

(define-public crate-ananc-0.3.0 (c (n "ananc") (v "0.3.0") (d (list (d (n "which") (r "^4.3.0") (d #t) (k 0)))) (h "127p75vb580g1p2j6b62gpanzwbixicvbvn6vifsxq5yaqfmj7m7")))

(define-public crate-ananc-0.4.0 (c (n "ananc") (v "0.4.0") (d (list (d (n "which") (r "^4.3.0") (d #t) (k 0)))) (h "1xkwxlfyc3z8rr1yzk7frlcmlzbdwkwalmdz1ckcb9qcn29hphv5")))

(define-public crate-ananc-0.4.1 (c (n "ananc") (v "0.4.1") (d (list (d (n "which") (r "^4.3.0") (d #t) (k 0)))) (h "0q1f9wkxf2cnrigw0srpmzk5pgsyyzclkr8hysqswpni9haw4i8h")))

(define-public crate-ananc-0.5.0 (c (n "ananc") (v "0.5.0") (d (list (d (n "which") (r "^4.3.0") (d #t) (k 0)))) (h "147k8gaghl6mvzmghqy1my567zs8akm8skvkbzxgm4vw5d2jdpmf")))

(define-public crate-ananc-0.6.0 (c (n "ananc") (v "0.6.0") (d (list (d (n "which") (r "^4.3.0") (d #t) (k 0)))) (h "1f319bzwqf4c8jxy7lvngxw4ck6nrfscagzrp48k7rdvslpzln4c")))

(define-public crate-ananc-0.7.0 (c (n "ananc") (v "0.7.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "fold"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "which") (r "^4.3.0") (d #t) (k 0)))) (h "1x5gagpk5r22gwcnkw6hy1k45ijnmclaxrvqxf0m6nds0ndiijn5")))

(define-public crate-ananc-0.8.0 (c (n "ananc") (v "0.8.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "fold"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "which") (r "^4.3.0") (d #t) (k 0)))) (h "1bvygw4rssz9gk0s3a1iq9qplmp1zyga4hsqaa93frsn3g2y8ahv")))

(define-public crate-ananc-0.9.0 (c (n "ananc") (v "0.9.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "fold"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "which") (r "^4.3.0") (d #t) (k 0)))) (h "1c6m6y8bjghr5n47b5s81z7av7b8zsncjxr17zxh74zf8r4wbdgd")))

(define-public crate-ananc-0.10.0 (c (n "ananc") (v "0.10.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "fold"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "which") (r "^4.3.0") (d #t) (k 0)))) (h "0wbc6r6bpw6k36y1dp4nv4bbhkv1hgs2p7zfg5csddcq44k06l0x")))

(define-public crate-ananc-0.11.0 (c (n "ananc") (v "0.11.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "fold"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "which") (r "^4.3.0") (d #t) (k 0)))) (h "1g7h8dx217m4ilisvdc2313snj5wlcmpak68bbxvlv1vjv9fpjdh")))

(define-public crate-ananc-0.11.1 (c (n "ananc") (v "0.11.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "fold"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "which") (r "^4.3.0") (d #t) (k 0)))) (h "18fs8x8f93f42wqzgc41i9yxbq4qn46hl7v7kvr56qf1q5kz8asw")))

(define-public crate-ananc-0.12.0 (c (n "ananc") (v "0.12.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "fold"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "which") (r "^4.3.0") (d #t) (k 0)))) (h "00wmy8l4231vw0hp1qijmv2k1ygkvw9plsw6dzavs2wmcm1xlvy4")))

(define-public crate-ananc-0.12.1 (c (n "ananc") (v "0.12.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "fold"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "which") (r "^4.3.0") (d #t) (k 0)))) (h "0qvqvcjp7y6xckfn6n6j8cba79ap7mi3k2gdbqfxgfac9yp3snwd")))

(define-public crate-ananc-0.13.0 (c (n "ananc") (v "0.13.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "fold"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "which") (r "^4.3.0") (d #t) (k 0)))) (h "16y03pn8b4yjmm52r4qpjcpg7s43hq1q3fk7ki5ylg6w0c447ycz")))

(define-public crate-ananc-0.13.1 (c (n "ananc") (v "0.13.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "fold"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "which") (r "^4.3.0") (d #t) (k 0)))) (h "1bd5lbxl8risf1ygwrhbc8qy10zg5ir98xqxln8hwy205sn62ck5")))

(define-public crate-ananc-0.13.2 (c (n "ananc") (v "0.13.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "fold"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "which") (r "^4.3.0") (d #t) (k 0)))) (h "1cb8p6daj7l4bf1g48lgcndvbc70nnka299yj6ahjfqq5w0na7h2")))

(define-public crate-ananc-0.13.3 (c (n "ananc") (v "0.13.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "fold"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "which") (r "^4.3.0") (d #t) (k 0)))) (h "051xlmii1aa96y5zlpm7bdjk6qaby48v9z8qdmi9hmhzh0i0w4cg")))

(define-public crate-ananc-0.14.0 (c (n "ananc") (v "0.14.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "fold"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "which") (r "^4.3.0") (d #t) (k 0)))) (h "1mjvrysl91k1bbb4n2n8qm4xxnaqv5h1vdwsn632arn0bad4bbv4")))

(define-public crate-ananc-0.14.1 (c (n "ananc") (v "0.14.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "fold"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "which") (r "^4.3.0") (d #t) (k 0)))) (h "1mfpbbg6qhgryl2rjki3vbld4lnwkhc82gj871nwgxz86k2yzigw")))

(define-public crate-ananc-0.14.2 (c (n "ananc") (v "0.14.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "fold"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "which") (r "^4.3.0") (d #t) (k 0)))) (h "0c5b3msz72qp3sf6bjnw6k1y3r1fmgnkwcp4g3dpn9lgj99bgmwn")))

