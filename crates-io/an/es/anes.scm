(define-module (crates-io an es anes) #:use-module (crates-io))

(define-public crate-anes-0.0.1 (c (n "anes") (v "0.0.1") (h "19i020cavnf5m4m2xx1mrkpms2s3mz63gig6q5b89k28ayzg83vy")))

(define-public crate-anes-0.0.2 (c (n "anes") (v "0.0.2") (h "1ixpnzfxigb4pa878sfvvl1qkiysl8z9pcdncwii6a05i0wwj1xb")))

(define-public crate-anes-0.1.0 (c (n "anes") (v "0.1.0") (h "0say24qq3ghnj8akb0i8hix8bl85nzqm4f9m4f68cm2h9nf48sax")))

(define-public crate-anes-0.1.1 (c (n "anes") (v "0.1.1") (h "16lwwj7jvjnxf4i2ll5609hg9957q3ay5q5s6zckjcjr9clj70sp")))

(define-public crate-anes-0.1.2 (c (n "anes") (v "0.1.2") (h "0fx0cwa71gi8p0hcziq085sp6jcd5kka5zxv9mx0mpr27jr4ysmz")))

(define-public crate-anes-0.1.3 (c (n "anes") (v "0.1.3") (h "1s663xpifzs4001brsw957q4hp6c2n19fwn409klv561krddmvid")))

(define-public crate-anes-0.1.4 (c (n "anes") (v "0.1.4") (h "1r0l8dzjkflfaxikflb15kzvyyrw44kynfsn0sgqakdj7hk4pq75")))

(define-public crate-anes-0.1.5 (c (n "anes") (v "0.1.5") (h "1ad5v1fna725ppavc0c2b0vrpkvjq37vcl81bg0vw8sdyzdbf3dn")))

(define-public crate-anes-0.1.6 (c (n "anes") (v "0.1.6") (d (list (d (n "bitflags") (r "^1.2") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "libc") (r "^0.2.66") (d #t) (k 2)))) (h "16bj1ww1xkwzbckk32j2pnbn5vk6wgsl3q4p3j9551xbcarwnijb") (f (quote (("parser" "bitflags") ("default"))))))

(define-public crate-anes-0.2.0 (c (n "anes") (v "0.2.0") (d (list (d (n "bitflags") (r "^2.4.1") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "libc") (r "^0.2.150") (d #t) (k 2)))) (h "111yf4wav38if2p48bmjbfzg76iv3kw2np12h0lglz55ihwlypbk") (f (quote (("parser" "bitflags") ("default"))))))

