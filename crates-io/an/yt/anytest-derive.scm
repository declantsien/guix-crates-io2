(define-module (crates-io an yt anytest-derive) #:use-module (crates-io))

(define-public crate-anytest-derive-0.1.0 (c (n "anytest-derive") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "syn") (r "^2.0.55") (d #t) (k 0)))) (h "1qxha1ycwp287ykv8scaal8j4xlkr7i6qw4pgr54wbn4dsxrc12g")))

