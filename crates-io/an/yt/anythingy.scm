(define-module (crates-io an yt anythingy) #:use-module (crates-io))

(define-public crate-anythingy-0.1.0 (c (n "anythingy") (v "0.1.0") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)))) (h "109641arg84zabk8hkqg2qvd7i578rdvpj6ivb0ffjiy49qcdzq6")))

(define-public crate-anythingy-0.1.1 (c (n "anythingy") (v "0.1.1") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)))) (h "15kcl46qaz6qq3yqcysi5aba1qcx93k42smys7za9pfy7m3g6f60")))

(define-public crate-anythingy-0.1.2 (c (n "anythingy") (v "0.1.2") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)))) (h "0al9q9gkd0qf4gg207pjz37r6jmsw2i7dyr9xd6yc2hff18j500a")))

