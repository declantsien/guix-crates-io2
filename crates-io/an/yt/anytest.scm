(define-module (crates-io an yt anytest) #:use-module (crates-io))

(define-public crate-anytest-0.1.0 (c (n "anytest") (v "0.1.0") (d (list (d (n "anytest-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "clap") (r "^4.5.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "rev_buf_reader") (r "^0.3.0") (d #t) (k 0)) (d (n "shlex") (r "^1.3.0") (d #t) (k 0)) (d (n "smart-default") (r "^0.7.1") (d #t) (k 0)) (d (n "which") (r "^6.0.1") (d #t) (k 0)))) (h "1fylcvl7z52qd9mlw8w3ysf8nb8malrvmg3arl4xk3six7dv6kx6")))

