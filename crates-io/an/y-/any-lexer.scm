(define-module (crates-io an y- any-lexer) #:use-module (crates-io))

(define-public crate-any-lexer-0.0.1 (c (n "any-lexer") (v "0.0.1") (d (list (d (n "text-scanner") (r "^0.0.1") (d #t) (k 0)))) (h "1r6pqdk4r2iwbm1g3q4n0bwnzyqh3jxwclmzb7rlsv0kwixc1brs")))

(define-public crate-any-lexer-0.0.2 (c (n "any-lexer") (v "0.0.2") (d (list (d (n "text-scanner") (r "^0.0.2") (d #t) (k 0)))) (h "048nmdx8j3d6s48b0k8idcczsbg0gfz3115wx72bcwkm1wfx7igq")))

(define-public crate-any-lexer-0.0.3 (c (n "any-lexer") (v "0.0.3") (d (list (d (n "text-scanner") (r "^0.0.3") (d #t) (k 0)))) (h "1nvg2gaa4mw5hqcv5c1j2a2rsannggmpm7yszf0icvazw06rfm8k")))

