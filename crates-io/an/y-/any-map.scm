(define-module (crates-io an y- any-map) #:use-module (crates-io))

(define-public crate-any-map-0.0.0 (c (n "any-map") (v "0.0.0") (h "06mchn6683099178ia4hwyg87yk05cg260isnmkr4hamg76j6j4m") (y #t)))

(define-public crate-any-map-0.1.0 (c (n "any-map") (v "0.1.0") (d (list (d (n "cloneable-any") (r "^0.1") (f (quote ("std"))) (o #t) (k 0)) (d (n "fnv") (r "^1.0") (f (quote ("std"))) (o #t) (k 0)))) (h "1z42599vf8fsrw7ah3w6ngylkavc9zamsn6wbdv0971ms9f1d2xm") (f (quote (("default" "cloneable-any" "fnv"))))))

