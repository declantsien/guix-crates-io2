(define-module (crates-io an y- any-range) #:use-module (crates-io))

(define-public crate-any-range-0.1.0 (c (n "any-range") (v "0.1.0") (h "0wy4m0dnn3xk54q32hx9wxk43i4qf137gmym5sflwz40ps9ii9yx")))

(define-public crate-any-range-0.1.1 (c (n "any-range") (v "0.1.1") (h "00nr42nccl5d4kwjr5q6s5j5aiy43nlmi9crkc035yrx64117id6")))

(define-public crate-any-range-0.1.2 (c (n "any-range") (v "0.1.2") (h "0phm8jy3wi790qrz4cziaspw1mhgp50z25zpwlhippd4fdk0b0n9")))

(define-public crate-any-range-0.1.3 (c (n "any-range") (v "0.1.3") (h "07sm9qyazqi0bcj0ckp9k300h550h66p8dkycil33g1nhf2r1a1h")))

(define-public crate-any-range-0.1.4 (c (n "any-range") (v "0.1.4") (h "0d0lnyy73q3bqcvskbnqvbh4yqc6wrlxnvh1514sxwl85k1iy1fh")))

