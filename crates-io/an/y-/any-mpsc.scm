(define-module (crates-io an y- any-mpsc) #:use-module (crates-io))

(define-public crate-any-mpsc-0.1.0 (c (n "any-mpsc") (v "0.1.0") (d (list (d (n "dfb") (r "^1.0") (d #t) (k 0)))) (h "0fmd1vhk8lk1qqg3cly9dp98ljazjpzfyc2j1z5v4m5x8s0v572b") (f (quote (("default" "buf_recv") ("buf_recv")))) (y #t)))

(define-public crate-any-mpsc-0.1.1 (c (n "any-mpsc") (v "0.1.1") (d (list (d (n "dfb") (r "^1.0") (d #t) (k 0)))) (h "1g9xg0rby7r9cd0hks5lcyiczzipx2wmkxcy00j11wlif3vjakq3") (f (quote (("default" "buf_recv") ("buf_recv")))) (y #t)))

(define-public crate-any-mpsc-0.1.2 (c (n "any-mpsc") (v "0.1.2") (d (list (d (n "dfb") (r "^1.1") (d #t) (k 0)))) (h "17aqfmqjb87k6zjq3yqd351mk1jn9krbh81pvjncrlnx9yg2k4ym") (f (quote (("default" "buf_recv") ("buf_recv"))))))

(define-public crate-any-mpsc-0.1.3 (c (n "any-mpsc") (v "0.1.3") (d (list (d (n "dfb") (r "^1.1") (d #t) (k 0)))) (h "1svh6ppx7iypr3vw1kq2dq14h0051avzq62v3m54h209ai0fiy9c") (f (quote (("default" "buf_recv") ("buf_recv"))))))

(define-public crate-any-mpsc-0.2.0 (c (n "any-mpsc") (v "0.2.0") (d (list (d (n "dfb") (r "^1.1") (d #t) (k 0)))) (h "01qgwz023fjg0nmwh2jjw3rnigbc177s751dvlympsbazw0ccdyp") (f (quote (("default" "buf_recv") ("buf_recv"))))))

(define-public crate-any-mpsc-0.2.1 (c (n "any-mpsc") (v "0.2.1") (d (list (d (n "dfb") (r "^1.1") (d #t) (k 0)))) (h "1dj4kqysydxsinx5c126060l24y3jwgvh4bjignn86aj6g7c2jkp") (f (quote (("default" "buf_recv") ("buf_recv"))))))

(define-public crate-any-mpsc-0.3.0 (c (n "any-mpsc") (v "0.3.0") (d (list (d (n "dfb") (r "^1.1") (d #t) (k 0)))) (h "0shqjmilbh1cyq7ck5j2d0r49mdr6206wfhdyw0mqrdwy467r0xa") (f (quote (("default" "buf_recv") ("buf_recv"))))))

(define-public crate-any-mpsc-0.3.1 (c (n "any-mpsc") (v "0.3.1") (d (list (d (n "dfb") (r "^1.1") (d #t) (k 0)))) (h "1zysjj6xnb66bqss8j7y67297mddiswcw0c070w6k0k7zci2ja37") (f (quote (("default" "buf_recv") ("buf_recv"))))))

