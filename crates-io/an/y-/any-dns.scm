(define-module (crates-io an y- any-dns) #:use-module (crates-io))

(define-public crate-any-dns-0.1.0 (c (n "any-dns") (v "0.1.0") (d (list (d (n "ctrlc") (r "^3.4.2") (d #t) (k 0)) (d (n "dyn-clone") (r "^1.0.16") (d #t) (k 0)) (d (n "simple-dns") (r "^0.6.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "0zdal3fdkwqcf6p6mh0j5yjas2034k9c61iy0968vcmx5y093gyx")))

(define-public crate-any-dns-0.1.1 (c (n "any-dns") (v "0.1.1") (d (list (d (n "ctrlc") (r "^3.4.2") (d #t) (k 0)) (d (n "dyn-clone") (r "^1.0.16") (d #t) (k 0)) (d (n "simple-dns") (r "^0.6.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "1h2d0h46r7jw7mqyx9vgbvr26rdmdyjdhsfkrxr0q80g02lgvpkf")))

(define-public crate-any-dns-0.1.2 (c (n "any-dns") (v "0.1.2") (d (list (d (n "ctrlc") (r "^3.4.2") (d #t) (k 0)) (d (n "dyn-clone") (r "^1.0.16") (d #t) (k 0)) (d (n "simple-dns") (r "^0.6.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "0rvd6il4x02lpyrpmyna9wpk5d7g7w5mxv6p2ad4kic38chaizbf")))

(define-public crate-any-dns-0.2.0 (c (n "any-dns") (v "0.2.0") (d (list (d (n "async-trait") (r "^0.1.77") (d #t) (k 0)) (d (n "ctrlc") (r "^3.4.2") (d #t) (k 0)) (d (n "dyn-clone") (r "^1.0.16") (d #t) (k 0)) (d (n "simple-dns") (r "^0.6.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)))) (h "12g53xg480l9jv7yniw6y5iigi1ypzch2mic8px220whqyaj2afv")))

(define-public crate-any-dns-0.2.1 (c (n "any-dns") (v "0.2.1") (d (list (d (n "async-trait") (r "^0.1.77") (d #t) (k 0)) (d (n "dyn-clone") (r "^1.0.16") (d #t) (k 0)) (d (n "simple-dns") (r "^0.6.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0bywm3caznz288zp401558hbhk38s1agjskdnwd1ns1v6i8zr1wi")))

(define-public crate-any-dns-0.2.2 (c (n "any-dns") (v "0.2.2") (d (list (d (n "async-trait") (r "^0.1.77") (d #t) (k 0)) (d (n "dyn-clone") (r "^1.0.16") (d #t) (k 0)) (d (n "simple-dns") (r "^0.6.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1q9l6x452hhs4w1p08szs63b71xzay9zi958v967jwna9xdakivf")))

(define-public crate-any-dns-0.2.3 (c (n "any-dns") (v "0.2.3") (d (list (d (n "async-trait") (r "^0.1.77") (d #t) (k 0)) (d (n "dyn-clone") (r "^1.0.16") (d #t) (k 0)) (d (n "simple-dns") (r "^0.6.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1iw2gkf0mzgfy2g1fwmaamznya244m9m8zvadxhsl4xiq5sdrj44")))

(define-public crate-any-dns-0.2.4 (c (n "any-dns") (v "0.2.4") (d (list (d (n "async-trait") (r "^0.1.77") (d #t) (k 0)) (d (n "dyn-clone") (r "^1.0.16") (d #t) (k 0)) (d (n "simple-dns") (r "^0.6.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)))) (h "103rlbm1y4psna9ip2h0n4ifwamd06z5ylk2bb1kzh6bxk5xl7my")))

