(define-module (crates-io an y- any-opt) #:use-module (crates-io))

(define-public crate-any-opt-1.0.0 (c (n "any-opt") (v "1.0.0") (h "00i3kghdq9qgzkaq6azaqn3wva99na9z9z609zlg2niy2msdzip3")))

(define-public crate-any-opt-1.0.1 (c (n "any-opt") (v "1.0.1") (h "1rj03icadcjj9m3wnisb04iw0q012zlc03xkqyb8jlh6lwiyy7ry")))

