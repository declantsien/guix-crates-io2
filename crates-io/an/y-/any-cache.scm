(define-module (crates-io an y- any-cache) #:use-module (crates-io))

(define-public crate-any-cache-0.1.0 (c (n "any-cache") (v "0.1.0") (h "037cg603zlsm92k4rjcss9lkssyr59wqmgv5jhcyiixh1q34nqn2")))

(define-public crate-any-cache-0.2.0 (c (n "any-cache") (v "0.2.0") (h "1sxg0i8c7wfl945r40n33yn5igm7g9rj272qsa7di6800bdn94ym")))

(define-public crate-any-cache-0.2.1 (c (n "any-cache") (v "0.2.1") (h "0z815hckxchzqyj8ykvw2zhqsvg54l5cqplm17hplh2j8nw115fh")))

(define-public crate-any-cache-0.2.2 (c (n "any-cache") (v "0.2.2") (h "0rxvgv9a26dwwv75lr2kzy7pc1xaq6xk4c8mhar3i1yvvsdbgwa1")))

(define-public crate-any-cache-0.2.3 (c (n "any-cache") (v "0.2.3") (h "1va7bdvbldprgr3mqcqlfxcaxrn9y74v7bdms5l5zcl4r7jphhaf")))

