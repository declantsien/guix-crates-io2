(define-module (crates-io an ab anabot) #:use-module (crates-io))

(define-public crate-anabot-1.0.0 (c (n "anabot") (v "1.0.0") (d (list (d (n "anagrambot") (r "^1.0.0") (d #t) (k 0)) (d (n "clap") (r "^3.2.12") (f (quote ("derive"))) (d #t) (k 0)))) (h "08i82gxk8w4byl0jbp91xlk9kp84b643kjb0rwwpi7ghjs0j5wcy")))

(define-public crate-anabot-1.0.1 (c (n "anabot") (v "1.0.1") (d (list (d (n "anagrambot") (r "^1.0.1") (d #t) (k 0)) (d (n "clap") (r "^3.2.12") (f (quote ("derive"))) (d #t) (k 0)))) (h "1r3pgkbpxr6zm5lx8y12li5iigf3yg5zzrdip1nnqx9k7pzfjra0")))

