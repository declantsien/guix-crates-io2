(define-module (crates-io an ab anabaena) #:use-module (crates-io))

(define-public crate-anabaena-0.1.0 (c (n "anabaena") (v "0.1.0") (d (list (d (n "getrandom") (r "^0.2.8") (f (quote ("js"))) (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1.0.0") (d #t) (k 2)))) (h "0qc0kb89gmpgd26jm75pklif89755l3b2fwa8dxclqar1i8k2rkw") (s 2) (e (quote (("js" "dep:getrandom"))))))

(define-public crate-anabaena-0.2.0 (c (n "anabaena") (v "0.2.0") (d (list (d (n "getrandom") (r "^0.2.8") (f (quote ("js"))) (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1.0.0") (d #t) (k 2)))) (h "1inw9x3hh0jzrihlsqipxhr1z6p2hxb6z6pr59hnv5g6vf4cmcdy") (s 2) (e (quote (("js" "dep:getrandom"))))))

(define-public crate-anabaena-0.3.0 (c (n "anabaena") (v "0.3.0") (d (list (d (n "getrandom") (r "^0.2.8") (f (quote ("js"))) (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1.0.0") (d #t) (k 2)))) (h "1f8rxjkxqy437plnf2nxdq6lmzv66dy7s0f61clw629mffikpbg5") (s 2) (e (quote (("js" "dep:getrandom"))))))

(define-public crate-anabaena-0.4.0 (c (n "anabaena") (v "0.4.0") (d (list (d (n "getrandom") (r "^0.2.8") (f (quote ("js"))) (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1.0.0") (d #t) (k 2)))) (h "0dqz57p7v36kxbvn4bf2fck61l5ddrl8j2ka6jskkmjlr9zkv6zi") (s 2) (e (quote (("js" "dep:getrandom"))))))

(define-public crate-anabaena-0.4.1 (c (n "anabaena") (v "0.4.1") (d (list (d (n "getrandom") (r "^0.2.8") (f (quote ("js"))) (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1.0.0") (d #t) (k 2)))) (h "18rr4kz9xh9zi952sw3d0xj6dkp56bfn34k8abb76ypb0lk26adk") (s 2) (e (quote (("js" "dep:getrandom"))))))

(define-public crate-anabaena-0.5.0 (c (n "anabaena") (v "0.5.0") (d (list (d (n "getrandom") (r "^0.2.8") (f (quote ("js"))) (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1.0.0") (d #t) (k 2)))) (h "029p7r4f65pn2k65x704s0mnj9b9lzk9zwvymp2cyzsgh947ri35") (s 2) (e (quote (("js" "dep:getrandom"))))))

(define-public crate-anabaena-0.6.0 (c (n "anabaena") (v "0.6.0") (d (list (d (n "getrandom") (r "^0.2.8") (f (quote ("js"))) (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1.0.0") (d #t) (k 2)))) (h "1a6lac0mz63arzvsnid3fcy43pj0w2fyq9sbwa35f8k04yqial0v") (s 2) (e (quote (("js" "dep:getrandom"))))))

(define-public crate-anabaena-0.7.0 (c (n "anabaena") (v "0.7.0") (d (list (d (n "getrandom") (r "^0.2.8") (f (quote ("js"))) (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "streaming-iterator") (r "^0.1.9") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1.0.0") (d #t) (k 2)))) (h "0vgy6nfvxknfzil4v2xb1ikmr50mw15dl2msgv6w104drsmq9d6h") (s 2) (e (quote (("js" "dep:getrandom"))))))

