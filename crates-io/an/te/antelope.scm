(define-module (crates-io an te antelope) #:use-module (crates-io))

(define-public crate-antelope-0.0.1 (c (n "antelope") (v "0.0.1") (h "06s0y4jjqfjd35fxwxbs0z8rspfq1cznzw71d98icsk53qyxfriz") (y #t)))

(define-public crate-antelope-0.0.2 (c (n "antelope") (v "0.0.2") (h "0yqwqflm92331mpiv7q2c68w48ik51lgxslhln4hkj8dcjz0s9im")))

(define-public crate-antelope-0.0.3 (c (n "antelope") (v "0.0.3") (h "0q2g85n6k8bjp29iiwfiyii2ygqdj95cxzfygx1zzaynmv2653d5") (y #t)))

(define-public crate-antelope-0.0.4 (c (n "antelope") (v "0.0.4") (h "1x1i8vy8hl6jkcbb5y5ls60y9gwmzf1yki502z9n02qdgrv6b16n") (y #t)))

(define-public crate-antelope-0.0.5 (c (n "antelope") (v "0.0.5") (h "1rcxrdpg6mk1yacxbnbbcwc987q3y2m7wg6zvqgk0l7llkvsyykh")))

(define-public crate-antelope-0.0.6 (c (n "antelope") (v "0.0.6") (h "1x0pjdsv9hjibvkhki10dls67nmgjil6ddzdmiis6dmlxlppiwck")))

(define-public crate-antelope-0.0.7 (c (n "antelope") (v "0.0.7") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)))) (h "09n8lwxhnq6ak0ybry5r9sakgx47d8pfi7yl5pw1j7djj5bzyqrr")))

(define-public crate-antelope-0.0.8 (c (n "antelope") (v "0.0.8") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)))) (h "1d6x242568c3hc9n3knvdkdx9ikrmwzifikr5j9whkr99rqh955q")))

(define-public crate-antelope-0.0.9 (c (n "antelope") (v "0.0.9") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)))) (h "13lvq36jwbv8sjjln6qj4xs8zfk1d5s80gxnnyzhcn527pbv6rci")))

(define-public crate-antelope-0.0.10 (c (n "antelope") (v "0.0.10") (d (list (d (n "cargo-husky") (r "^1.5") (f (quote ("precommit-hook" "run-cargo-fmt" "run-cargo-clippy" "run-cargo-test"))) (k 2)) (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)))) (h "04hwq7wsxqqkyv3080sz06jf814sd7v4npnmrrnpr3asqm3y369v")))

(define-public crate-antelope-0.0.11 (c (n "antelope") (v "0.0.11") (d (list (d (n "proptest") (r "^1.0.0") (d #t) (k 2)))) (h "1df29mw61y88d6av7cx039xi5cwhd6c0vzmpxd7a73yw2cybl6p8")))

(define-public crate-antelope-0.0.12 (c (n "antelope") (v "0.0.12") (d (list (d (n "proptest") (r "^1.0.0") (d #t) (k 2)))) (h "00zss0wnvllz8wdzxh9f7r1vwp0n9f2q79bzssfyy479y8c2171f")))

(define-public crate-antelope-0.1.0 (c (n "antelope") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)))) (h "1hfrzz3kssn6447idhqc26svdr5w62xbxa0npzfg29j90flj9y9b")))

(define-public crate-antelope-0.1.1 (c (n "antelope") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)))) (h "0xggz9jx2hbgwl3yjg12izj52lj9dkm26j20w2hdhf23iwhm9k88")))

(define-public crate-antelope-0.1.2 (c (n "antelope") (v "0.1.2") (d (list (d (n "proptest") (r "^1.0.0") (d #t) (k 2)))) (h "11whrdbq9bdxhlmrbml71fzgrscd7340g8p5jbbfaxw1q7cl9qxh")))

(define-public crate-antelope-0.1.3 (c (n "antelope") (v "0.1.3") (d (list (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "time") (r "^0.3.20") (f (quote ("parsing" "formatting"))) (d #t) (k 0)))) (h "1hh65f4vd3s7pbppbbf3c5kdfyhsq09ydshnfmi5l38b8lg25is4")))

(define-public crate-antelope-0.1.4 (c (n "antelope") (v "0.1.4") (d (list (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "time") (r "^0.3.20") (f (quote ("parsing" "formatting"))) (d #t) (k 0)))) (h "0n40apsh5sx87gri4qq7kd9gf42m91icz3zgy3m183gjsrg47png")))

(define-public crate-antelope-0.1.5 (c (n "antelope") (v "0.1.5") (d (list (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "time") (r "^0.3.20") (f (quote ("parsing" "formatting"))) (d #t) (k 0)))) (h "1b3vs6ykfb60pcr7m9z2n1vvrpbh4b0xzwnxs4zqdxsrmradjrq7")))

(define-public crate-antelope-0.1.6 (c (n "antelope") (v "0.1.6") (d (list (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "time") (r "^0.3.20") (f (quote ("parsing" "formatting"))) (d #t) (k 0)))) (h "1i2n7q06jz04k24bg8h4japp4vk6l4l50arsj6jsg908pk9zfq1h")))

(define-public crate-antelope-0.2.0 (c (n "antelope") (v "0.2.0") (d (list (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "time") (r "^0.3.20") (f (quote ("parsing" "formatting"))) (d #t) (k 0)))) (h "07b2ma8gh6ffdxiisgn1rvx2m8ymxdnz65n5rdrx8dchwr41byrs")))

