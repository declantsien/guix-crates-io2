(define-module (crates-io an o_ ano_jit_bfi_rs) #:use-module (crates-io))

(define-public crate-ano_jit_bfi_rs-0.2.0 (c (n "ano_jit_bfi_rs") (v "0.2.0") (h "03xx8sjazinngk4w8v42imabfkp1fmsgipn72v5s95qzpiy3327b")))

(define-public crate-ano_jit_bfi_rs-0.2.1 (c (n "ano_jit_bfi_rs") (v "0.2.1") (h "1cwb3s1abm9w69p9w81k0l2j5ra58hf7ikafgbdnsv52c92zpy7v")))

(define-public crate-ano_jit_bfi_rs-0.2.2 (c (n "ano_jit_bfi_rs") (v "0.2.2") (h "010qdxhgkzkhfigkc84zx213bxdab0c4z23rlkphqfmpkm5vlql2")))

(define-public crate-ano_jit_bfi_rs-0.2.3 (c (n "ano_jit_bfi_rs") (v "0.2.3") (h "1i09yylngkggsh1qsgam9gid65b4xrbm0bs6x2ml5qmn4s546zi1")))

(define-public crate-ano_jit_bfi_rs-0.2.4 (c (n "ano_jit_bfi_rs") (v "0.2.4") (h "07949gf0b0yv8zz7x0cl7s5lhk47lww7kz366bfh9fs021ry1dk5")))

(define-public crate-ano_jit_bfi_rs-0.2.5 (c (n "ano_jit_bfi_rs") (v "0.2.5") (h "1kphbsp2y6a4hnb78h3abdj2rza6fvm6dvwficpggk7kk0375c37")))

