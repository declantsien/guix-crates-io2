(define-module (crates-io an sc anscape) #:use-module (crates-io))

(define-public crate-anscape-0.1.0 (c (n "anscape") (v "0.1.0") (h "0x97br8vgvp7kn18bmnk7crplv71zj1wa58vfdda3r7i9i7hy5rs")))

(define-public crate-anscape-0.1.1 (c (n "anscape") (v "0.1.1") (h "1n4jf880p5axhv61f69d93ig4jf0frklkm16jk81hjv5f6gnc3vm")))

(define-public crate-anscape-0.2.1 (c (n "anscape") (v "0.2.1") (d (list (d (n "paste") (r "^1.0.8") (d #t) (k 0)))) (h "0bkfaxb369v3bflxdikaa7p279fpiq524bixccfqm5rma8c719q7")))

(define-public crate-anscape-0.2.2 (c (n "anscape") (v "0.2.2") (d (list (d (n "paste") (r "^1.0.8") (d #t) (k 0)))) (h "0iknw1g3rx0ccrb1j1lwfv5f786zg42lgyd892risivan61pkm61")))

