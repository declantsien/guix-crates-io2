(define-module (crates-io an t- ant-design-rs) #:use-module (crates-io))

(define-public crate-ant-design-rs-0.1.0 (c (n "ant-design-rs") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "implicit-clone") (r "^0") (d #t) (k 0)) (d (n "palette") (r "^0") (f (quote ("rand" "serializing"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "stylist") (r "^0") (f (quote ("yew_integration" "yew_use_media_query" "yew_use_style"))) (d #t) (k 0)) (d (n "yew") (r "^0") (f (quote ("csr"))) (d #t) (k 0)))) (h "0dy5d7cigkfil9jnq999nb7ya8gsr8hyv2kkz14gcxs62sicqz0l")))

