(define-module (crates-io an in aninmals) #:use-module (crates-io))

(define-public crate-aninmals-0.1.0 (c (n "aninmals") (v "0.1.0") (h "0gw89v070vwv1i5lvixqs2m944ykqppdjir05gb82crf0vz71j7x")))

(define-public crate-aninmals-0.1.1 (c (n "aninmals") (v "0.1.1") (h "1h6m4imw742agim5pqb8i8ffy2qw7akcinhnq40d18j9xbx7rj98")))

(define-public crate-aninmals-0.1.2 (c (n "aninmals") (v "0.1.2") (d (list (d (n "rand") (r "^0.5.0") (d #t) (k 0)))) (h "11zw61i6vg64gdk4dbh6jzsfxkmwpi7521az7p02v8jsg9kwzzf2")))

(define-public crate-aninmals-0.1.3 (c (n "aninmals") (v "0.1.3") (d (list (d (n "rand") (r "^0.5.0") (d #t) (k 0)))) (h "0jkxhd2sqfhylc6d49qax6zh24njp7lwbgn53w5y6f6jw4b9irzw")))

