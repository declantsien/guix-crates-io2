(define-module (crates-io an in aninamer) #:use-module (crates-io))

(define-public crate-aninamer-0.1.0 (c (n "aninamer") (v "0.1.0") (d (list (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "walkdir") (r "^2.3") (d #t) (k 0)))) (h "0lrr5f16gvak2wwy6j6pyy8rk8snqj67jsis32bjjn5myzsbj3x5")))

(define-public crate-aninamer-0.1.1 (c (n "aninamer") (v "0.1.1") (d (list (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "walkdir") (r "^2.3") (d #t) (k 0)))) (h "1l5wlyv0xk7y8js24ac2mqw52pby61pbsrm12pjbhapzcd02qp21")))

(define-public crate-aninamer-1.0.0 (c (n "aninamer") (v "1.0.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "walkdir") (r "^2.3") (d #t) (k 0)))) (h "185b2blnv0myaafzq54n5cjwlrh6x3j7hpsasalbxqq9g0qrk2qi")))

