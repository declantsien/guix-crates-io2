(define-module (crates-io an yo anyotp) #:use-module (crates-io))

(define-public crate-anyotp-0.0.1 (c (n "anyotp") (v "0.0.1") (d (list (d (n "base32") (r "^0.4.0") (d #t) (k 0)) (d (n "hmac") (r "^0.12.1") (d #t) (k 0)) (d (n "sha1") (r "^0.10.6") (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)) (d (n "urlencoding") (r "^2.1.3") (d #t) (k 0)))) (h "0dd7pvg4svjfnqq8diz06w994lhwwn8av0l6x70rjd1qisp4d96c")))

