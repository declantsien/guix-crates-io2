(define-module (crates-io an ag anaglyph) #:use-module (crates-io))

(define-public crate-anaglyph-0.1.0 (c (n "anaglyph") (v "0.1.0") (d (list (d (n "image") (r "^0.24.7") (d #t) (k 0)))) (h "1j5ch7jyyq5b26l8hr8ymwg5w7k18nwx6cy0b3n495mcy0xzja9r")))

(define-public crate-anaglyph-0.1.1 (c (n "anaglyph") (v "0.1.1") (d (list (d (n "image") (r "^0.24.7") (d #t) (k 0)))) (h "1lvc2q7myw7qgx985x9sydb5z34ds5lqgr90c7fni1ann7zbxsyk")))

