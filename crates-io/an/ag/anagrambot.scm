(define-module (crates-io an ag anagrambot) #:use-module (crates-io))

(define-public crate-anagrambot-1.0.0 (c (n "anagrambot") (v "1.0.0") (h "0fq555pah7abzci8bcmwdkxlzwk3j25ykf4c74bpja20lnyjxp5r") (f (quote (("no-default-wordlist"))))))

(define-public crate-anagrambot-1.0.1 (c (n "anagrambot") (v "1.0.1") (h "1vmm68zcjs8pffaiflczv2yvplnar81zyi2a4w4wlwqs7p304q44") (f (quote (("no-default-wordlist"))))))

