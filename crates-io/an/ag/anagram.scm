(define-module (crates-io an ag anagram) #:use-module (crates-io))

(define-public crate-anagram-0.0.0 (c (n "anagram") (v "0.0.0") (h "1v3yk4pigx8dkw6pml9gldm97arr53wailgl02j8777aj9nwc37a")))

(define-public crate-anagram-0.1.0 (c (n "anagram") (v "0.1.0") (d (list (d (n "counter") (r "^0.5.2") (d #t) (k 0)))) (h "091wf7y2k5aqb3hk7in7dihi9cd6bb4qwrrf32j2sng1z51h2rly")))

(define-public crate-anagram-0.2.0 (c (n "anagram") (v "0.2.0") (d (list (d (n "counter") (r "^0.5.2") (d #t) (k 0)))) (h "05rdcshxjb6g73xq6pksy5rwbl1ff61i8rx19fllsxkbqh6ydn38")))

(define-public crate-anagram-0.3.0 (c (n "anagram") (v "0.3.0") (d (list (d (n "counter") (r "^0.5.2") (d #t) (k 0)))) (h "0hnkg7kvbq5npkr25cd2f57dbdvm82ylnp4rgnz7w0k1cqmqydpn")))

(define-public crate-anagram-0.4.0 (c (n "anagram") (v "0.4.0") (d (list (d (n "counter") (r "^0.5.2") (d #t) (k 0)))) (h "0shxwnzc53blzps14m8cj3jszl8arq5g8a3sqac6wvhi647paxv7")))

