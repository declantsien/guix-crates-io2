(define-module (crates-io an ch anchorpy_core) #:use-module (crates-io))

(define-public crate-anchorpy_core-0.2.0 (c (n "anchorpy_core") (v "0.2.0") (d (list (d (n "anchor-syn") (r "^0.29.0") (f (quote ("idl-types"))) (d #t) (k 0)) (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "pyo3") (r "^0.17.2") (f (quote ("extension-module" "abi3-py37"))) (d #t) (k 0)) (d (n "pythonize") (r "^0.17.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 0)) (d (n "solders-macros") (r "^0.6.0") (d #t) (k 0)) (d (n "solders-traits") (r "^0.9.3") (d #t) (k 0)))) (h "0f7bnw7sz53acapy7yydgxlhx3ygxqxcpcnwkacgr5xs8wkgs0yb")))

