(define-module (crates-io an ch anchors) #:use-module (crates-io))

(define-public crate-anchors-0.1.0 (c (n "anchors") (v "0.1.0") (h "1x6lacr3ss46b0iqaf8hn7m7ff43mr2z4ddijbmragk66bg6845n")))

(define-public crate-anchors-0.2.0 (c (n "anchors") (v "0.2.0") (d (list (d (n "petgraph") (r "^0.5.1") (d #t) (k 0)) (d (n "slotmap") (r "^0.4.0") (f (quote ("unstable"))) (d #t) (k 0)))) (h "10nbf91vznjbln6hgmac6xshs5s3gphwg9ax0imn6j5s6xih0zp2")))

(define-public crate-anchors-0.3.0 (c (n "anchors") (v "0.3.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "petgraph") (r "^0.5.1") (d #t) (k 0)) (d (n "slotmap") (r "^0.4.0") (f (quote ("unstable"))) (d #t) (k 0)))) (h "1mib2jz2vx7amla6vw2ggpm0afz2w4akyhffl10vs1n5d4gqiaka")))

(define-public crate-anchors-0.4.0 (c (n "anchors") (v "0.4.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "petgraph") (r "^0.5.1") (d #t) (k 0)) (d (n "slotmap") (r "^0.4.0") (f (quote ("unstable"))) (d #t) (k 0)))) (h "1zsvsvwcs72jr8y1pr3kb11421355bfzps0map7ixqpjc5a2ild8")))

(define-public crate-anchors-0.5.0 (c (n "anchors") (v "0.5.0") (d (list (d (n "arena-graph") (r "^0.1.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "im") (r "^15.0.0") (d #t) (k 0)) (d (n "typed-arena") (r "^2.0.1") (d #t) (k 0)))) (h "0kr90k0k3l9ndgvjzi70ds7lmkx1c54jfc88gk2x565y5c2lfm9s")))

(define-public crate-anchors-0.6.0 (c (n "anchors") (v "0.6.0") (d (list (d (n "arena-graph") (r "^0.1.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "im") (r "^15.0.0") (d #t) (k 0)) (d (n "typed-arena") (r "^2.0.1") (d #t) (k 0)))) (h "1h5jnzq5sl6yzifdj7dk30yqikdnich4gl5lis5hd90awdc57gyc")))

