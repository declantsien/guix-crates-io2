(define-module (crates-io an ch anchor-generate-cpi-crate-tulip) #:use-module (crates-io))

(define-public crate-anchor-generate-cpi-crate-tulip-0.4.0 (c (n "anchor-generate-cpi-crate-tulip") (v "0.4.0") (d (list (d (n "anchor-idl-tulip") (r "^0.4.0") (d #t) (k 0)) (d (n "anchor-lang") (r "^0.24.2") (d #t) (k 2)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1m490n2wzs2c904677213gykk4qk8yvzf6152nb5w3c9mkl7d341") (f (quote (("compat-program-result" "anchor-idl-tulip/compat-program-result"))))))

(define-public crate-anchor-generate-cpi-crate-tulip-0.4.1 (c (n "anchor-generate-cpi-crate-tulip") (v "0.4.1") (d (list (d (n "anchor-idl-tulip") (r "^0.4.1") (d #t) (k 0)) (d (n "anchor-lang") (r "^0.24.2") (d #t) (k 2)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0c0y1jsh57nbk1j2ixilvh82nd5ldrsw2jk9a3x1idwlg3f048l8") (f (quote (("compat-program-result" "anchor-idl-tulip/compat-program-result"))))))

