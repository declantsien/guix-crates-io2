(define-module (crates-io an ch anchor-generate-cpi-crate) #:use-module (crates-io))

(define-public crate-anchor-generate-cpi-crate-0.3.0 (c (n "anchor-generate-cpi-crate") (v "0.3.0") (d (list (d (n "anchor-idl") (r "^0.3.0") (d #t) (k 0)) (d (n "anchor-lang") (r "^0.24.2") (d #t) (k 2)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1h3pgbpzfkb0laa80z5iqnr931d0hw0vmy9j8hs2bg2ph3vzm06z")))

(define-public crate-anchor-generate-cpi-crate-0.3.1 (c (n "anchor-generate-cpi-crate") (v "0.3.1") (d (list (d (n "anchor-idl") (r "^0.3.0") (d #t) (k 0)) (d (n "anchor-lang") (r "^0.24.2") (d #t) (k 2)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0l5ghh9gjj53wqb0msg5z67pb1lpfqjrq8bg1pg2njdqalyp7s03") (f (quote (("compat-program-result" "anchor-idl/compat-program-result"))))))

