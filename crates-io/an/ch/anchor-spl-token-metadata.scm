(define-module (crates-io an ch anchor-spl-token-metadata) #:use-module (crates-io))

(define-public crate-anchor-spl-token-metadata-0.1.0 (c (n "anchor-spl-token-metadata") (v "0.1.0") (d (list (d (n "anchor-lang") (r "^0.19.0") (d #t) (k 0)) (d (n "spl-token-metadata") (r "^0.0.1") (f (quote ("no-entrypoint"))) (d #t) (k 0)))) (h "0jpf4a1fdvlsgc63q1w6rh062yj9ay9lwvnmmp0yb25xq5dvnm8w")))

(define-public crate-anchor-spl-token-metadata-0.1.1 (c (n "anchor-spl-token-metadata") (v "0.1.1") (d (list (d (n "anchor-lang") (r "^0.20.1") (d #t) (k 0)) (d (n "spl-token-metadata") (r "^0.0.1") (f (quote ("no-entrypoint"))) (d #t) (k 0)))) (h "12mp5iswsh7qzfc6hidcvr68y8jal32lm9jzhs2da20sr400kxp7")))

