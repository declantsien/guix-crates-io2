(define-module (crates-io an ch anchor-cereal-derive) #:use-module (crates-io))

(define-public crate-anchor-cereal-derive-0.1.0 (c (n "anchor-cereal-derive") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.96") (f (quote ("derive"))) (d #t) (k 0)))) (h "0c1qj3564jpv9dy5zxxaqx7dyci8c6rvg7c79q3hv83f97fq1w04")))

(define-public crate-anchor-cereal-derive-0.2.0 (c (n "anchor-cereal-derive") (v "0.2.0") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.96") (f (quote ("derive"))) (d #t) (k 0)))) (h "13fca3ad7j93mspzn9ch8ax9xhmkylchkmakiss2b5fn1jq5vc90")))

(define-public crate-anchor-cereal-derive-0.2.1 (c (n "anchor-cereal-derive") (v "0.2.1") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.96") (f (quote ("derive"))) (d #t) (k 0)))) (h "0l87ws4d3vwsdwnlayiax3xs8c3hhmxhl3kq6pm7549yimnakcvz")))

(define-public crate-anchor-cereal-derive-0.2.3 (c (n "anchor-cereal-derive") (v "0.2.3") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.96") (f (quote ("derive"))) (d #t) (k 0)))) (h "1w1n2cn2qgmn9v8sshqnzlh1sznycwaa24l51mipjd8cwnbbw22k")))

(define-public crate-anchor-cereal-derive-0.2.4 (c (n "anchor-cereal-derive") (v "0.2.4") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.96") (f (quote ("derive"))) (d #t) (k 0)))) (h "0qi3rvs8if1rjp8p81pk51l023grp2mpf9hyvypawmazxrcnwcz3")))

(define-public crate-anchor-cereal-derive-0.2.5 (c (n "anchor-cereal-derive") (v "0.2.5") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.96") (f (quote ("derive"))) (d #t) (k 0)))) (h "05561hmp3zxjksq37mbwck6ynin9v5rla8ka85ykb06imv60byiw")))

