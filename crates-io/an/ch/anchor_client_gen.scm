(define-module (crates-io an ch anchor_client_gen) #:use-module (crates-io))

(define-public crate-anchor_client_gen-0.1.0 (c (n "anchor_client_gen") (v "0.1.0") (d (list (d (n "utils") (r "^0.1.1") (d #t) (k 0) (p "anchor-client-gen-utils")))) (h "1dmlxv421cc2lzl2pn3dhjfv5kgl0fv0c2n778baflygxcd28css")))

(define-public crate-anchor_client_gen-0.1.1 (c (n "anchor_client_gen") (v "0.1.1") (d (list (d (n "utils") (r "^0.1.2") (d #t) (k 0) (p "anchor-client-gen-utils")))) (h "11w8hqyyjzxqjjd153k460fxwdx50yr9qxvcwa5a5z3kk6cs8kh2")))

(define-public crate-anchor_client_gen-0.1.2 (c (n "anchor_client_gen") (v "0.1.2") (d (list (d (n "utils") (r "^0.1.3") (d #t) (k 0) (p "anchor-client-gen-utils")))) (h "19vrz8375xgdmwzx8cr8hxfak3y8lsq6gd5s5k764636cfpvlnzc")))

