(define-module (crates-io an ch anchored-macros) #:use-module (crates-io))

(define-public crate-anchored-macros-0.1.0 (c (n "anchored-macros") (v "0.1.0") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.76") (f (quote ("full"))) (d #t) (k 0)))) (h "1pxzjfgm5pvy812ajipszs4sidh3r6qvx8gpvavrcbdin2q3qijb")))

