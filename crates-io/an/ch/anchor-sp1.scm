(define-module (crates-io an ch anchor-sp1) #:use-module (crates-io))

(define-public crate-anchor-sp1-0.1.0 (c (n "anchor-sp1") (v "0.1.0") (d (list (d (n "anchor-lang") (r "^0.18.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serum_dex") (r "^0.4.0") (f (quote ("no-entrypoint"))) (o #t) (d #t) (k 0)) (d (n "solana-program") (r "^1.8.0") (d #t) (k 0)) (d (n "spl-associated-token-account") (r "^1.0.3") (f (quote ("no-entrypoint"))) (d #t) (k 0)) (d (n "spl-token") (r "^3.1.1") (f (quote ("no-entrypoint"))) (d #t) (k 0)))) (h "0rr4dd1mvpqxfgb8wl5rjfnbi5qdw50dgi0jzlhmks7wsc4hqwik") (f (quote (("token") ("shmem") ("mint") ("governance") ("dex" "serum_dex") ("devnet") ("default" "mint" "token" "associated_token") ("associated_token"))))))

