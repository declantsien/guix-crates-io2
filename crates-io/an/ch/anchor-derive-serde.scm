(define-module (crates-io an ch anchor-derive-serde) #:use-module (crates-io))

(define-public crate-anchor-derive-serde-0.29.0 (c (n "anchor-derive-serde") (v "0.29.0") (d (list (d (n "anchor-syn") (r "^0.29.0") (d #t) (k 0)) (d (n "borsh-derive-internal") (r ">=0.9, <0.11") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1gnzkdrvjlar774fcyf7hdsn1vnv8dlg59x2j0yhbilfa6zfbqpl") (f (quote (("idl-build" "anchor-syn/idl-build")))) (r "1.60")))

(define-public crate-anchor-derive-serde-0.30.0 (c (n "anchor-derive-serde") (v "0.30.0") (d (list (d (n "anchor-syn") (r "^0.30.0") (d #t) (k 0)) (d (n "borsh-derive-internal") (r ">=0.9, <0.11") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "07gkywa9pxnfwwrz8x6d8s9j8fk2r4k17160xmq71hyrda7ykv5v") (f (quote (("idl-build" "anchor-syn/idl-build")))) (r "1.60")))

