(define-module (crates-io an ch anchor-merkle-tree) #:use-module (crates-io))

(define-public crate-anchor-merkle-tree-1.15.0 (c (n "anchor-merkle-tree") (v "1.15.0") (d (list (d (n "fast-math") (r "^0.1") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "matches") (r "^0.1.9") (d #t) (t "bpfel-unknown-unknown") (k 0)) (d (n "solana-program") (r "^1.13.3") (d #t) (k 0)))) (h "04fynwhfdsy9h3id0w2b0xynnzlvnjh1mn5b26f439biyaha2xrj") (y #t)))

(define-public crate-anchor-merkle-tree-1.15.1 (c (n "anchor-merkle-tree") (v "1.15.1") (d (list (d (n "fast-math") (r "^0.1") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "matches") (r "^0.1.9") (d #t) (t "bpfel-unknown-unknown") (k 0)) (d (n "solana-program") (r "^1.10.41") (d #t) (k 0)))) (h "0d5m28mrf6dpkfc8jg2nsr5g0ay1saipxx0346gc10yzk8yqjyyv")))

