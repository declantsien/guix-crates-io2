(define-module (crates-io an ch anchor_token_metadata) #:use-module (crates-io))

(define-public crate-anchor_token_metadata-0.1.0 (c (n "anchor_token_metadata") (v "0.1.0") (d (list (d (n "anchor-lang") (r "^0.19.0") (d #t) (k 0)) (d (n "spl-token-metadata") (r "^0.0.1") (f (quote ("no-entrypoint"))) (d #t) (k 0)))) (h "1vkp61x51maq2rzrxhgykgmndrwlpj8r9vhjvf8k8hm6ch9abfc9") (y #t)))

