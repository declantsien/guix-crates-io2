(define-module (crates-io an ch anchor-generate-cpi-interface) #:use-module (crates-io))

(define-public crate-anchor-generate-cpi-interface-0.3.0 (c (n "anchor-generate-cpi-interface") (v "0.3.0") (d (list (d (n "anchor-idl") (r "^0.3.0") (d #t) (k 0)) (d (n "anchor-lang") (r "^0.24.2") (d #t) (k 2)) (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1r3lvsshqk5wzh5jskzpcvygzzc9yzkv1xhz2vx9i4csznck73rx")))

(define-public crate-anchor-generate-cpi-interface-0.3.1 (c (n "anchor-generate-cpi-interface") (v "0.3.1") (d (list (d (n "anchor-idl") (r "^0.3.0") (d #t) (k 0)) (d (n "anchor-lang") (r "^0.24.2") (d #t) (k 2)) (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1brx0f332fjhh0p2jicczakqxk6li1zb0b0nc0sgbssrz46ab4kh") (f (quote (("compat-program-result" "anchor-idl/compat-program-result"))))))

