(define-module (crates-io an ch anchor-client-gen-utils) #:use-module (crates-io))

(define-public crate-anchor-client-gen-utils-0.1.0 (c (n "anchor-client-gen-utils") (v "0.1.0") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "serde") (r "^1.0.183") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.16.9") (d #t) (k 0)))) (h "1wvk2psx82imsp8qf5bqxqw0w6z6wf04pqjfnaqjpds56pj282db")))

(define-public crate-anchor-client-gen-utils-0.1.1 (c (n "anchor-client-gen-utils") (v "0.1.1") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "serde") (r "^1.0.183") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.16.9") (d #t) (k 0)))) (h "1vmyvvga5hyvqd1r9l9p03313qkmgjl5pw46rpfq4d106g64hf02")))

(define-public crate-anchor-client-gen-utils-0.1.2 (c (n "anchor-client-gen-utils") (v "0.1.2") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "serde") (r "^1.0.183") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.16.9") (d #t) (k 0)))) (h "0mngy82hpx8ziaf9ih777v0gdrx1rvx5mzfqg6fp5rw3sy6kcjdb")))

(define-public crate-anchor-client-gen-utils-0.1.3 (c (n "anchor-client-gen-utils") (v "0.1.3") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "serde") (r "^1.0.183") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.16.9") (d #t) (k 0)))) (h "0giy1iw9y0lny04qabc8a9k5kzk06zf8ncwisqzwz8756hbfrxpb")))

