(define-module (crates-io an ch anchor-generate-cpi-crate-mod) #:use-module (crates-io))

(define-public crate-anchor-generate-cpi-crate-mod-0.3.2 (c (n "anchor-generate-cpi-crate-mod") (v "0.3.2") (d (list (d (n "anchor-idl-mod") (r "^0.3.2") (d #t) (k 0)) (d (n "anchor-lang") (r "^0.24.2") (d #t) (k 2)) (d (n "bytemuck") (r "^1.13.1") (f (quote ("min_const_generics" "derive"))) (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1p2njiv39vha3ycv4q2n0vfy55snhbywc1wzj22hs73jgdhw953v") (f (quote (("compat-program-result" "anchor-idl-mod/compat-program-result"))))))

(define-public crate-anchor-generate-cpi-crate-mod-0.3.3 (c (n "anchor-generate-cpi-crate-mod") (v "0.3.3") (d (list (d (n "anchor-idl-mod") (r "^0.3.3") (d #t) (k 0)) (d (n "anchor-lang") (r "^0.24.2") (d #t) (k 2)) (d (n "bytemuck") (r "^1.13.1") (f (quote ("min_const_generics" "derive"))) (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1ypjk2r757xb6hbj6c8mbn27i9cm7am7hg46k8ij5q7gmlyvda0j") (f (quote (("compat-program-result" "anchor-idl-mod/compat-program-result"))))))

