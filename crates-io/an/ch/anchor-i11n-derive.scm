(define-module (crates-io an ch anchor-i11n-derive) #:use-module (crates-io))

(define-public crate-anchor-i11n-derive-0.1.0 (c (n "anchor-i11n-derive") (v "0.1.0") (d (list (d (n "anchor-lang") (r "^0.29.0") (d #t) (k 0)) (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "sha2") (r "^0.10.0") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1yywxxjq2jpzmpf3277p3mgzb43n9i0rj2fpmv7j6nf1ryw74bkd")))

