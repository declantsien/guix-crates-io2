(define-module (crates-io an ch anchor-i11n) #:use-module (crates-io))

(define-public crate-anchor-i11n-0.1.0 (c (n "anchor-i11n") (v "0.1.0") (d (list (d (n "anchor-i11n-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "anchor-lang") (r "^0.29.0") (d #t) (k 0)) (d (n "hex-literal") (r "^0.4.1") (d #t) (k 2)) (d (n "solana-program") (r "^1.18.12") (d #t) (k 2)))) (h "153cbir881h090h66xgm70pb4w197yp64j54cc656gk6jri4ak7m")))

