(define-module (crates-io an ch anchor-generate-cpi-interface-tulip) #:use-module (crates-io))

(define-public crate-anchor-generate-cpi-interface-tulip-0.4.0 (c (n "anchor-generate-cpi-interface-tulip") (v "0.4.0") (d (list (d (n "anchor-idl-tulip") (r "^0.4.0") (d #t) (k 0)) (d (n "anchor-lang") (r "^0.24.2") (d #t) (k 2)) (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1ldmc855idwas3dzmadcdc87vz5h2xxj7gj8s15dsik18nx9ysk9") (f (quote (("compat-program-result" "anchor-idl-tulip/compat-program-result"))))))

(define-public crate-anchor-generate-cpi-interface-tulip-0.4.1 (c (n "anchor-generate-cpi-interface-tulip") (v "0.4.1") (d (list (d (n "anchor-idl-tulip") (r "^0.4.1") (d #t) (k 0)) (d (n "anchor-lang") (r "^0.24.2") (d #t) (k 2)) (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0srbnbmwdd7d1cypfhx7lj1nw82x5w8av8wf65xdk626gvnhv6v7") (f (quote (("compat-program-result" "anchor-idl-tulip/compat-program-result"))))))

