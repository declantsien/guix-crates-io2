(define-module (crates-io an ch anchor-attribute-constant) #:use-module (crates-io))

(define-public crate-anchor-attribute-constant-0.19.0 (c (n "anchor-attribute-constant") (v "0.19.0") (d (list (d (n "anchor-syn") (r "^0.19.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("full"))) (d #t) (k 0)))) (h "18ipk6lmbd90xvfckgci027qy12hchxz8aa8v3d97i6x3ffh3csp") (f (quote (("anchor-debug" "anchor-syn/anchor-debug"))))))

(define-public crate-anchor-attribute-constant-0.20.0 (c (n "anchor-attribute-constant") (v "0.20.0") (d (list (d (n "anchor-syn") (r "^0.20.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("full"))) (d #t) (k 0)))) (h "1jskjgn556sbhf8ybzxb2psq0cmnb7cgsb366rgsy2abc6nb2m6j") (f (quote (("anchor-debug" "anchor-syn/anchor-debug"))))))

(define-public crate-anchor-attribute-constant-0.20.1 (c (n "anchor-attribute-constant") (v "0.20.1") (d (list (d (n "anchor-syn") (r "^0.20.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("full"))) (d #t) (k 0)))) (h "11q600ky9a2j4hmbmw8h4saxkimy6xcn4w5am2z0n7c82x98cmk3") (f (quote (("anchor-debug" "anchor-syn/anchor-debug"))))))

(define-public crate-anchor-attribute-constant-0.21.0 (c (n "anchor-attribute-constant") (v "0.21.0") (d (list (d (n "anchor-syn") (r "^0.21.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("full"))) (d #t) (k 0)))) (h "1n28p0kw1fqgkp11npglkq291w1cjvk7fmsd0hi3dzl4igi65kjg") (f (quote (("anchor-debug" "anchor-syn/anchor-debug"))))))

(define-public crate-anchor-attribute-constant-0.22.0 (c (n "anchor-attribute-constant") (v "0.22.0") (d (list (d (n "anchor-syn") (r "^0.22.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("full"))) (d #t) (k 0)))) (h "1jb9srdy8w50d7vgpx4384knqa8kp2ij2z4yjhnp9rfzqpg57zhv") (f (quote (("anchor-debug" "anchor-syn/anchor-debug"))))))

(define-public crate-anchor-attribute-constant-0.22.1 (c (n "anchor-attribute-constant") (v "0.22.1") (d (list (d (n "anchor-syn") (r "^0.22.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("full"))) (d #t) (k 0)))) (h "1q1xw7cr71cddlvvb4sys8ks00lp1nfjravb44ylg97b5i3cwiyk") (f (quote (("anchor-debug" "anchor-syn/anchor-debug"))))))

(define-public crate-anchor-attribute-constant-0.23.0 (c (n "anchor-attribute-constant") (v "0.23.0") (d (list (d (n "anchor-syn") (r "^0.23.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("full"))) (d #t) (k 0)))) (h "1w4f1z599nbhhmr26wv5rclzcrd2ahrz0nxlhb1g9bc4xgl2kclh") (f (quote (("anchor-debug" "anchor-syn/anchor-debug")))) (r "1.56")))

(define-public crate-anchor-attribute-constant-0.24.0 (c (n "anchor-attribute-constant") (v "0.24.0") (d (list (d (n "anchor-syn") (r "^0.24.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("full"))) (d #t) (k 0)))) (h "1gp5kpm0i1hl6a9bkyv8qh1aaw1s2zxc6m5idr232shmlnlqnrlc") (f (quote (("anchor-debug" "anchor-syn/anchor-debug")))) (r "1.56")))

(define-public crate-anchor-attribute-constant-0.24.1 (c (n "anchor-attribute-constant") (v "0.24.1") (d (list (d (n "anchor-syn") (r "^0.24.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("full"))) (d #t) (k 0)))) (h "1552ih2df0jw4c1hq30j13zwjwmmar21925nzqgkw4wlbjvrpr8i") (f (quote (("anchor-debug" "anchor-syn/anchor-debug")))) (r "1.56")))

(define-public crate-anchor-attribute-constant-0.24.2 (c (n "anchor-attribute-constant") (v "0.24.2") (d (list (d (n "anchor-syn") (r "^0.24.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("full"))) (d #t) (k 0)))) (h "1q61rkvig1x19ypxyg8n0g9i6w7m8z68f1dhg0d0bp8k15a2qqyw") (f (quote (("anchor-debug" "anchor-syn/anchor-debug")))) (r "1.56")))

(define-public crate-anchor-attribute-constant-0.25.0 (c (n "anchor-attribute-constant") (v "0.25.0") (d (list (d (n "anchor-syn") (r "^0.25.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("full"))) (d #t) (k 0)))) (h "02mpa1s68aqyi2f1i2wb8c3mip5wgrvn63bb0mifv32yqd59fi08") (f (quote (("anchor-debug" "anchor-syn/anchor-debug")))) (r "1.56")))

(define-public crate-anchor-attribute-constant-0.26.0 (c (n "anchor-attribute-constant") (v "0.26.0") (d (list (d (n "anchor-syn") (r "^0.26.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("full"))) (d #t) (k 0)))) (h "0a1xbja9gfc5m3wgshmf5prjrgkqc0i7ya0ph85y0prrisj69gp1") (f (quote (("anchor-debug" "anchor-syn/anchor-debug")))) (r "1.59")))

(define-public crate-anchor-attribute-constant-0.27.0 (c (n "anchor-attribute-constant") (v "0.27.0") (d (list (d (n "anchor-syn") (r "^0.27.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("full"))) (d #t) (k 0)))) (h "0vr964hafmm0za7n04l943ijmyv8687xlacjdzmsn7ahx3wl93kq") (f (quote (("anchor-debug" "anchor-syn/anchor-debug")))) (r "1.60")))

(define-public crate-anchor-attribute-constant-0.28.0 (c (n "anchor-attribute-constant") (v "0.28.0") (d (list (d (n "anchor-syn") (r "^0.28.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1vqp20q857iq72h5nxpcms5pkqpwqm02mwxkxwm4q57qkrzqx52r") (f (quote (("anchor-debug" "anchor-syn/anchor-debug")))) (r "1.60")))

(define-public crate-anchor-attribute-constant-0.29.0 (c (n "anchor-attribute-constant") (v "0.29.0") (d (list (d (n "anchor-syn") (r "^0.29.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "19ywwgz1ypxjnr2b73xji9n2rxpnp9w8wa0316rz09snnmf988wl") (f (quote (("idl-build" "anchor-syn/idl-build") ("anchor-debug" "anchor-syn/anchor-debug")))) (r "1.60")))

(define-public crate-anchor-attribute-constant-0.30.0 (c (n "anchor-attribute-constant") (v "0.30.0") (d (list (d (n "anchor-syn") (r "^0.30.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0653yn77wjyn9v613vf89a4d7asd0z5798j6w313d1ng8lcdrc9y") (f (quote (("idl-build" "anchor-syn/idl-build") ("anchor-debug" "anchor-syn/anchor-debug")))) (r "1.60")))

