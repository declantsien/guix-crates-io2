(define-module (crates-io an ch anchor-cereal) #:use-module (crates-io))

(define-public crate-anchor-cereal-0.1.0 (c (n "anchor-cereal") (v "0.1.0") (d (list (d (n "anchor-cereal-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "borsh") (r "^0.9") (d #t) (k 0)))) (h "0qfr1qhhi8yklr7scnw0awldi06qhlwk38c1hcghdhs0kzp1nqcw")))

(define-public crate-anchor-cereal-0.1.1 (c (n "anchor-cereal") (v "0.1.1") (d (list (d (n "anchor-cereal-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "borsh") (r "^0.9") (d #t) (k 0)))) (h "04yzry0hrzh904m5sqixvf79pxdc2647nywmmd0w5wly82bndgdd")))

(define-public crate-anchor-cereal-0.1.2 (c (n "anchor-cereal") (v "0.1.2") (d (list (d (n "anchor-cereal-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "borsh") (r "^0.9") (d #t) (k 0)))) (h "16wpg4wxsylg1hmcvwmlbp1461kmn3iyjs2zvigac2xzbckd739y")))

(define-public crate-anchor-cereal-0.2.0 (c (n "anchor-cereal") (v "0.2.0") (d (list (d (n "anchor-cereal-derive") (r "^0.2.0") (d #t) (k 0)) (d (n "borsh") (r "^0.9") (d #t) (k 0)))) (h "1j7zvz3bskh282g438fjiagm15xay4c8s7xk04460fgzpdw5jyyb")))

(define-public crate-anchor-cereal-0.2.1 (c (n "anchor-cereal") (v "0.2.1") (d (list (d (n "anchor-cereal-derive") (r "^0.2.1") (d #t) (k 0)) (d (n "borsh") (r "^0.9") (d #t) (k 0)))) (h "03jgkyfqi13fs3n9qs00gr6k8pxw5lfyizpp6l4lchdwbcg4315k")))

(define-public crate-anchor-cereal-0.2.2 (c (n "anchor-cereal") (v "0.2.2") (d (list (d (n "anchor-cereal-derive") (r "^0.2.1") (d #t) (k 0)) (d (n "borsh") (r "^0.9") (d #t) (k 0)))) (h "18y8f7wsifi15d17iq8c5vvjxk6v78nyp4rqk4xhn49i9whb9qaf")))

(define-public crate-anchor-cereal-0.2.3 (c (n "anchor-cereal") (v "0.2.3") (d (list (d (n "anchor-cereal-derive") (r "^0.2.3") (d #t) (k 0)) (d (n "borsh") (r "^0.9") (d #t) (k 0)))) (h "12qmx0mcz4l6x5n9ay1mq66wgp9dpycqh3z7c6wkywhn5ax80y64")))

(define-public crate-anchor-cereal-0.2.4 (c (n "anchor-cereal") (v "0.2.4") (d (list (d (n "anchor-cereal-derive") (r "^0.2.4") (d #t) (k 0)) (d (n "borsh") (r "^0.9") (d #t) (k 0)))) (h "0ks7abssmz2j01zwizhf0zkb9g10qqvr3fakm7x0n5yizyn73xnm")))

(define-public crate-anchor-cereal-0.2.5 (c (n "anchor-cereal") (v "0.2.5") (d (list (d (n "anchor-cereal-derive") (r "^0.2.5") (d #t) (k 0)) (d (n "borsh") (r "^0.9") (d #t) (k 0)))) (h "1j991zqdj8fhxy0rw9i9p1gyw6mvil456zprd1dx9xjp6ciy1gg6")))

