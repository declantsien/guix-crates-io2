(define-module (crates-io an ch anchor-gen-tulip) #:use-module (crates-io))

(define-public crate-anchor-gen-tulip-0.4.0 (c (n "anchor-gen-tulip") (v "0.4.0") (d (list (d (n "anchor-generate-cpi-crate-tulip") (r "^0.4.0") (d #t) (k 0)) (d (n "anchor-generate-cpi-interface-tulip") (r "^0.4.0") (d #t) (k 0)))) (h "1ppgd6zdw97ni2g51sh4x9zbqkvlyjrdxixahvnkqdf7qh7fq9wn") (f (quote (("compat-program-result" "anchor-generate-cpi-crate-tulip/compat-program-result" "anchor-generate-cpi-interface-tulip/compat-program-result"))))))

(define-public crate-anchor-gen-tulip-0.4.1 (c (n "anchor-gen-tulip") (v "0.4.1") (d (list (d (n "anchor-generate-cpi-crate-tulip") (r "^0.4.1") (d #t) (k 0)) (d (n "anchor-generate-cpi-interface-tulip") (r "^0.4.1") (d #t) (k 0)))) (h "0g7prha3lkr38q0m75gb7af6y9hdn1a6jmph6wymadxcjr27j4xq") (f (quote (("compat-program-result" "anchor-generate-cpi-crate-tulip/compat-program-result" "anchor-generate-cpi-interface-tulip/compat-program-result"))))))

