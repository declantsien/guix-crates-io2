(define-module (crates-io an ch anchor-derive-space) #:use-module (crates-io))

(define-public crate-anchor-derive-space-0.27.0 (c (n "anchor-derive-space") (v "0.27.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "08si1ln9z6m0m9z3rri9977p140vr91jp2rb3gn36krcz632izn4") (r "1.60")))

(define-public crate-anchor-derive-space-0.28.0 (c (n "anchor-derive-space") (v "0.28.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0ajmsrxsxxwy9qiwnpp706syim678y94j7dpfzmxv5mxh1afi5gl") (r "1.60")))

(define-public crate-anchor-derive-space-0.29.0 (c (n "anchor-derive-space") (v "0.29.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "06f4afx8f7npl646116yb42dfjgapi29v5vs9gkl0j55kz8k3k0y") (r "1.60")))

(define-public crate-anchor-derive-space-0.30.0 (c (n "anchor-derive-space") (v "0.30.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0lxcay02gjshjfk7c567za7szqp52aamyyjsw1gai6dw90kbxbcb") (r "1.60")))

