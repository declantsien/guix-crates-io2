(define-module (crates-io an ch anchor-safe-math) #:use-module (crates-io))

(define-public crate-anchor-safe-math-0.1.0 (c (n "anchor-safe-math") (v "0.1.0") (d (list (d (n "anchor-lang") (r "^0.20.1") (d #t) (k 0)))) (h "0wk1y40f94bfs741w4il3cmmkmkr7wm920m6apf2a7kkjkymk07d")))

(define-public crate-anchor-safe-math-0.1.1 (c (n "anchor-safe-math") (v "0.1.1") (d (list (d (n "anchor-lang") (r "^0.20.1") (d #t) (k 0)))) (h "0skjqipngjqz915z7nzcx2wml4p3l6yya3vpc03v5r2gk6qfaykx")))

(define-public crate-anchor-safe-math-0.1.2 (c (n "anchor-safe-math") (v "0.1.2") (d (list (d (n "anchor-lang") (r "^0.20.1") (d #t) (k 0)))) (h "1ibyfxz09frkj5xhygyrax4v5rqbac25xnvkbrkljcp0dllrrsyf")))

(define-public crate-anchor-safe-math-0.1.3 (c (n "anchor-safe-math") (v "0.1.3") (d (list (d (n "anchor-lang") (r "^0.24.2") (d #t) (k 0)))) (h "0q4qpm2w2b3hjzv7sh88c3a3bp6rsl9n6341dcjf6s2ij5fhk6nc")))

(define-public crate-anchor-safe-math-0.2.0 (c (n "anchor-safe-math") (v "0.2.0") (d (list (d (n "anchor-lang") (r "^0.25.0") (d #t) (k 0)))) (h "14zaibv7icwg374p7zy3ay4ls84ws9qnr7sdggindlvi9wcpf3xg")))

(define-public crate-anchor-safe-math-0.2.1 (c (n "anchor-safe-math") (v "0.2.1") (d (list (d (n "anchor-lang") (r "^0.25.0") (d #t) (k 0)))) (h "1g57r1xr99blyy4pp1qvbfadqbwm3500yykk7m3mqmf4wvzj5hkr")))

(define-public crate-anchor-safe-math-0.2.2 (c (n "anchor-safe-math") (v "0.2.2") (d (list (d (n "anchor-lang") (r "^0.26.0") (d #t) (k 0)))) (h "0dcnr5lc5cidyn1h1w8778lhi3y0045rn077syzfaghvbcphlrij")))

(define-public crate-anchor-safe-math-0.4.0 (c (n "anchor-safe-math") (v "0.4.0") (d (list (d (n "anchor-lang") (r "^0.29.0") (d #t) (k 0)))) (h "0cday4bh9hgpgxazs07xn1h0b5kyrclqd9kwxn9n0zzwlk9czrc8")))

(define-public crate-anchor-safe-math-0.5.0 (c (n "anchor-safe-math") (v "0.5.0") (d (list (d (n "anchor-lang") (r "^0.30.0") (d #t) (k 0)))) (h "1d4lvhcmhlck08cmdispnf27a0cs7kan44sd7b0isk6kn9ia65zf")))

