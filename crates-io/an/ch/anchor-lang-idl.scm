(define-module (crates-io an ch anchor-lang-idl) #:use-module (crates-io))

(define-public crate-anchor-lang-idl-0.1.0 (c (n "anchor-lang-idl") (v "0.1.0") (d (list (d (n "anchor-syn") (r "^0.30.0") (o #t) (d #t) (k 0)) (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1axd6vgkyy8nr6q979vcsag6z76blaw0d6vl8sw1p2s7mqgai7dj") (f (quote (("build" "anchor-syn" "regex")))) (r "1.60")))

