(define-module (crates-io an ch anchor-gen-mod) #:use-module (crates-io))

(define-public crate-anchor-gen-mod-0.3.1 (c (n "anchor-gen-mod") (v "0.3.1") (d (list (d (n "anchor-generate-cpi-crate") (r "^0.3.0") (d #t) (k 0)) (d (n "anchor-generate-cpi-interface") (r "^0.3.0") (d #t) (k 0)))) (h "032dfrddp0xnlr705llsswycmwqib7mg6casmk3lv40g638qxiy2") (f (quote (("compat-program-result" "anchor-generate-cpi-crate/compat-program-result" "anchor-generate-cpi-interface/compat-program-result"))))))

(define-public crate-anchor-gen-mod-0.3.2 (c (n "anchor-gen-mod") (v "0.3.2") (d (list (d (n "anchor-generate-cpi-crate-mod") (r "^0.3.2") (d #t) (k 0)) (d (n "anchor-generate-cpi-interface-mod") (r "^0.3.2") (d #t) (k 0)))) (h "1zkgx7a7x183y4hq7l5iiy7dqj9fxmzr6dv9b4wj14vx5v8x5g5v") (f (quote (("compat-program-result-mod" "anchor-generate-cpi-crate-mod/compat-program-result" "anchor-generate-cpi-interface-mod/compat-program-result-mod"))))))

(define-public crate-anchor-gen-mod-0.3.3 (c (n "anchor-gen-mod") (v "0.3.3") (d (list (d (n "anchor-generate-cpi-crate-mod") (r "^0.3.3") (d #t) (k 0)) (d (n "anchor-generate-cpi-interface-mod") (r "^0.3.3") (d #t) (k 0)))) (h "00wrb0a03iyr40xgndgmdmccpnf96jrg3h884i6mqdw3z9afiazp") (f (quote (("compat-program-result-mod" "anchor-generate-cpi-crate-mod/compat-program-result" "anchor-generate-cpi-interface-mod/compat-program-result-mod"))))))

