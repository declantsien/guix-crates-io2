(define-module (crates-io an yb anybar) #:use-module (crates-io))

(define-public crate-anybar-0.1.0 (c (n "anybar") (v "0.1.0") (h "18csjzvs9p0c6jjgv0yz076v1v1sqkl4y2d20lzglpnr8hwzy6dq")))

(define-public crate-anybar-0.1.1 (c (n "anybar") (v "0.1.1") (h "1ynl92wy6857sscgkjhrpidzyqga1cd6bvjyq5j5h5vhfjm7nibz")))

(define-public crate-anybar-0.1.2 (c (n "anybar") (v "0.1.2") (h "08akl7q72mrag93fgpy116szmd2rhkkpsf95rbbpmlqgjyzjwp6p")))

(define-public crate-anybar-0.1.3 (c (n "anybar") (v "0.1.3") (h "1ha9z8kd0wmmgswghxlc41hbazdp9v0xyklyzkcnv2y6hrsb7lia")))

