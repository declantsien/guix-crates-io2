(define-module (crates-io an yb anybox) #:use-module (crates-io))

(define-public crate-anybox-0.1.0 (c (n "anybox") (v "0.1.0") (h "0n0k90di0gdwvv21lkbsz6yzz44r8fwm4cdz0s31phrmps062829")))

(define-public crate-anybox-0.1.1 (c (n "anybox") (v "0.1.1") (h "0ar89p1bdqdqhdmydsagchdwapywql9735gpjnfb9k21h3ypd1bg")))

