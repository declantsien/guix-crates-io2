(define-module (crates-io an yb anybar_rs) #:use-module (crates-io))

(define-public crate-anybar_rs-0.1.0 (c (n "anybar_rs") (v "0.1.0") (h "1jbayv3s91agcjlcxw004lkbw6zrpv31fzxnbf3877yk8vipf6jy")))

(define-public crate-anybar_rs-0.2.0 (c (n "anybar_rs") (v "0.2.0") (h "0giikadaij30g9l4qwxr36m8fv8bidxas4iv238d13kigcyy7w1x")))

(define-public crate-anybar_rs-0.3.0 (c (n "anybar_rs") (v "0.3.0") (h "0yqi9xsq3mvi698wjnlsg9z2nqjrmxp37nyid0f91hk167xip5mp")))

(define-public crate-anybar_rs-0.4.0 (c (n "anybar_rs") (v "0.4.0") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)))) (h "1cq3ylzqm2f28szvvby922yaani7c4vp9lz3w8shcpfvfcln1mnw")))

(define-public crate-anybar_rs-0.5.0 (c (n "anybar_rs") (v "0.5.0") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)))) (h "1zip9lmy50p1qgym677m41l9h00rgf6is45gwn4yh42x6z6nirq5")))

(define-public crate-anybar_rs-0.6.0 (c (n "anybar_rs") (v "0.6.0") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)))) (h "10w5r9k1vg32b5xnrfn6gqkz95614i6zmfv0lw04p2mz4l7wh7sj")))

(define-public crate-anybar_rs-0.7.0 (c (n "anybar_rs") (v "0.7.0") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)))) (h "0d66bi8ih9d6adhlchv0xczkxpc4a65vcr6v9yx37izjbajl7qfm")))

(define-public crate-anybar_rs-0.8.0 (c (n "anybar_rs") (v "0.8.0") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)))) (h "1fh28lvxpfy7nn59yacbnbl0z00iwriqm32xdsrrz34dsw82m6n7")))

(define-public crate-anybar_rs-0.9.0 (c (n "anybar_rs") (v "0.9.0") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)))) (h "0dc103jzxhky27mi853cjffxs5scnsqi2fkssyzl588qlrkfw92w")))

(define-public crate-anybar_rs-0.9.1 (c (n "anybar_rs") (v "0.9.1") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)))) (h "1wgrrphrabg48psw3vzlprlgdhr0rh05yzkpm1g20aqssqi2kcdy")))

(define-public crate-anybar_rs-0.9.2 (c (n "anybar_rs") (v "0.9.2") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)))) (h "1smw0pmvfcg1mg63g6iz42y44x5jdla1c0j8c5gn5w9h8lmwaa35")))

(define-public crate-anybar_rs-0.9.3 (c (n "anybar_rs") (v "0.9.3") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)))) (h "0g4v09y231c0cisd5h5cswf8bldk1p8j78ci904ci14prfdf3q65")))

(define-public crate-anybar_rs-0.9.4 (c (n "anybar_rs") (v "0.9.4") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)))) (h "1s8q8wwnp1bmyx92zsc44gjd7aixyhl3d72m1pis63n16w0l5v28")))

(define-public crate-anybar_rs-0.9.5 (c (n "anybar_rs") (v "0.9.5") (d (list (d (n "clap") (r "^1.5.4") (d #t) (k 0)) (d (n "getopts") (r "^0.2.14") (d #t) (k 0)))) (h "1q9a4dkfrqljrr5ra197gj6nbkhxlknvzpiaxr9dvflpm5rbnsdx")))

(define-public crate-anybar_rs-0.9.6 (c (n "anybar_rs") (v "0.9.6") (d (list (d (n "clap") (r "^1.5.4") (d #t) (k 0)))) (h "1qprq1m6wb9ypjhalqv9zfx3f6bgc80l8xzvjhba0jvhab7316ic")))

(define-public crate-anybar_rs-0.9.7 (c (n "anybar_rs") (v "0.9.7") (d (list (d (n "clap") (r "^1.5.5") (d #t) (k 0)))) (h "0jssal4n8lan3jl475r09sprkcg5nys91l0vvpwsbqd80sjw9n25")))

(define-public crate-anybar_rs-0.9.8 (c (n "anybar_rs") (v "0.9.8") (d (list (d (n "clap") (r "^2.0.2") (d #t) (k 0)))) (h "1b19rbvj4syikvz9y5caxvngl09xsg74mjlmhsr7la4zx956jaff")))

(define-public crate-anybar_rs-0.9.9 (c (n "anybar_rs") (v "0.9.9") (d (list (d (n "clap") (r "^2.0.2") (d #t) (k 0)))) (h "0w0f2awqzm483fc5awr7v8610ipbmcf8nz024smi3221fmqn2h1d")))

(define-public crate-anybar_rs-0.9.10 (c (n "anybar_rs") (v "0.9.10") (d (list (d (n "clap") (r "^2.0.2") (d #t) (k 0)))) (h "1i9f3dgv83dyfvgjsv5gzk599a5db1g3iami53hf5j2bnmd7yin4")))

(define-public crate-anybar_rs-0.9.11 (c (n "anybar_rs") (v "0.9.11") (d (list (d (n "clap") (r "^2.0.2") (d #t) (k 0)))) (h "10s1j5j2xaipvihph7cray8wn50l6ssmms3rxhwvdy1mq88kv472")))

(define-public crate-anybar_rs-0.9.12 (c (n "anybar_rs") (v "0.9.12") (d (list (d (n "clap") (r "^2.0.2") (d #t) (k 0)))) (h "04zfjwh6lfa9nvb3yqn6wwpm34pllck64fw9i7sb0m7k9bwdxpcc")))

(define-public crate-anybar_rs-1.0.0 (c (n "anybar_rs") (v "1.0.0") (d (list (d (n "clap") (r "^2.0.2") (d #t) (k 0)))) (h "0ijmwn01n162kipk6s1hdxhpzzrlsz319fby192ihqlf26zwc8f4")))

(define-public crate-anybar_rs-1.0.1 (c (n "anybar_rs") (v "1.0.1") (d (list (d (n "clap") (r "^2.0.2") (d #t) (k 0)))) (h "10ji0dm8bqhz89kyl12sf7mcf7gp0qjwr3k4gyf6jb0qm5811lal")))

(define-public crate-anybar_rs-1.0.2 (c (n "anybar_rs") (v "1.0.2") (d (list (d (n "clap") (r "^2.0.2") (d #t) (k 0)))) (h "088l6vqslnm70csxrzmm53nh1ii98ygrgvhnlkm9plzy1v3yrjjn")))

(define-public crate-anybar_rs-1.0.3 (c (n "anybar_rs") (v "1.0.3") (d (list (d (n "clap") (r "^2.0.2") (d #t) (k 0)))) (h "0pghxniwsksj6vb2pml4fa72s8y4j5bsas0mxmmy0mbgyhnd79la")))

(define-public crate-anybar_rs-1.0.4 (c (n "anybar_rs") (v "1.0.4") (d (list (d (n "clap") (r "^2.0.2") (d #t) (k 0)))) (h "1rs718zvih2afnwwv25h3rds90rlalqlnimd18x7x10zisq4rpw1")))

(define-public crate-anybar_rs-1.0.5 (c (n "anybar_rs") (v "1.0.5") (d (list (d (n "clap") (r "^2.23.3") (d #t) (k 0)))) (h "0jv7xl3nvj1v2aw02x4diggznw66dsa2qx47ip7rb4ppbnggix44")))

(define-public crate-anybar_rs-1.0.6 (c (n "anybar_rs") (v "1.0.6") (d (list (d (n "clap") (r "^2.23.3") (d #t) (k 0)))) (h "0azjy2552sz7c04fqk26b0vk6zwipvq8q1j117jhk3585a18jy26")))

(define-public crate-anybar_rs-1.0.7 (c (n "anybar_rs") (v "1.0.7") (d (list (d (n "clap") (r "^2.23.3") (d #t) (k 0)))) (h "0hslnxj4p80k477mbz5428fzkqnx1syqf1ka5c6miaaxiqnvh332")))

(define-public crate-anybar_rs-1.0.11 (c (n "anybar_rs") (v "1.0.11") (d (list (d (n "clap") (r "^2.30.0") (d #t) (k 0)))) (h "16048f414p9knpccriwj46adxc5ngjalji2fhcv8n266xpfl0qp8")))

(define-public crate-anybar_rs-1.0.12 (c (n "anybar_rs") (v "1.0.12") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)))) (h "1ailzh83scpgadaj9lap78p1z785v649gf47rsidcs182lash63h")))

(define-public crate-anybar_rs-1.0.14 (c (n "anybar_rs") (v "1.0.14") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)))) (h "08wp0np3wziivwlxl3bxk83jdxcia9w0xcg485y7f9416lbrr2s6")))

