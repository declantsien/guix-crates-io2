(define-module (crates-io an yb anybuf) #:use-module (crates-io))

(define-public crate-anybuf-0.1.0 (c (n "anybuf") (v "0.1.0") (h "1sz4q1vckixway0d5nsg9m9p1dk4xvfivbhsn49drvvbbgfkhigi")))

(define-public crate-anybuf-0.2.0 (c (n "anybuf") (v "0.2.0") (d (list (d (n "hex-literal") (r "^0.4.1") (d #t) (k 2)))) (h "1p5hxnsf3lkzm4mqzg4p9lvfp225789p2wd88aann1n7m3xa81py")))

(define-public crate-anybuf-0.3.0-rc.1 (c (n "anybuf") (v "0.3.0-rc.1") (d (list (d (n "hex-literal") (r "^0.4.1") (d #t) (k 2)))) (h "0yi9p4wi25dfm8f47vm0chiikyyhh38in93jds71pd6hwxw3sz7c")))

(define-public crate-anybuf-0.3.0 (c (n "anybuf") (v "0.3.0") (d (list (d (n "hex-literal") (r "^0.4.1") (d #t) (k 2)))) (h "0ndqxjvrhklk3zvcljz07qi829gq0yz30ybjmin4c8gsam6yn0ws")))

(define-public crate-anybuf-0.4.0 (c (n "anybuf") (v "0.4.0") (d (list (d (n "hex-literal") (r "^0.4.1") (d #t) (k 2)))) (h "1nr0xnmxbmk4k23d0lb7nji41jrk1yhsw3mcs72rdwqlvjvl66jw")))

