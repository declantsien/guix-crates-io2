(define-module (crates-io an yu anyu-minigrep) #:use-module (crates-io))

(define-public crate-anyu-minigrep-0.1.0 (c (n "anyu-minigrep") (v "0.1.0") (h "0avlgflasm2xa73r4ay9xglvmh37g7ymvfr67rw7afqa7n2spcnm")))

(define-public crate-anyu-minigrep-0.1.1 (c (n "anyu-minigrep") (v "0.1.1") (h "1yx4d6l6cpdfvzdmwj1x1azvgxwbi2mxcdbrmw95bx6ynbyf9r3j")))

