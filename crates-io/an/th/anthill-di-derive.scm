(define-module (crates-io an th anthill-di-derive) #:use-module (crates-io))

(define-public crate-anthill-di-derive-0.1.0 (c (n "anthill-di-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.92") (d #t) (k 0)))) (h "1slw0xz62jn27bq3b7h6wwjl5cz831vm42jyfc3imbwfdj7bh9sa") (y #t)))

(define-public crate-anthill-di-derive-1.2.3 (c (n "anthill-di-derive") (v "1.2.3") (d (list (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.92") (d #t) (k 0)))) (h "1wbjg2pjlp21bqn3pgpl9jqbim4gclxh2jh6hiqdm5318vfkxfwg")))

(define-public crate-anthill-di-derive-1.2.4 (c (n "anthill-di-derive") (v "1.2.4") (d (list (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.92") (d #t) (k 0)))) (h "1n0m3wssk0gh5pr1adyibn3m782ad1r67ci51k7jddpmjw4ifg59") (f (quote (("default" "async-mode") ("async-mode"))))))

