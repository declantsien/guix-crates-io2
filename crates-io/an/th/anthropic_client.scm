(define-module (crates-io an th anthropic_client) #:use-module (crates-io))

(define-public crate-anthropic_client-0.1.0 (c (n "anthropic_client") (v "0.1.0") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0rn59xza418sld8n0mz01l0bfqc37vsflyykaqx06p14m4mhs2x9")))

(define-public crate-anthropic_client-0.1.1 (c (n "anthropic_client") (v "0.1.1") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "17bsjdrbhkm0mrjf7q90yhis75w6v45b1byxb32a5k3bcs2shwfp")))

(define-public crate-anthropic_client-0.1.2 (c (n "anthropic_client") (v "0.1.2") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0fsicg9b6rxzclwkayj1qlgpda6z2vlb7l03hig5cy28rd8d9xz4")))

(define-public crate-anthropic_client-0.1.3 (c (n "anthropic_client") (v "0.1.3") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1c0bx85yggmfjkmzb1hpwhfn61a24a0nmwc31y7q7kw3v6wvk71f")))

(define-public crate-anthropic_client-0.1.4 (c (n "anthropic_client") (v "0.1.4") (d (list (d (n "base64") (r "^0.21.0") (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1qgjqy7n6jlzysqv5xv44yri62x35zls4gnfra4a9iyzgwb8rfhv")))

(define-public crate-anthropic_client-1.0.0 (c (n "anthropic_client") (v "1.0.0") (d (list (d (n "base64") (r "^0.21.0") (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1awx2pvxmyv01z9nmjf0jc5xmgk7pkfvfyj0mk53afgy9c3ljg7n")))

