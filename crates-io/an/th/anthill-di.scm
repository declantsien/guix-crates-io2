(define-module (crates-io an th anthill-di) #:use-module (crates-io))

(define-public crate-anthill-di-0.1.0 (c (n "anthill-di") (v "0.1.0") (h "0p6ckl85mn7vr8kkajja1phbb1zycp9d3cxnx0a1q6a8jjf41y9w") (y #t)))

(define-public crate-anthill-di-0.1.1 (c (n "anthill-di") (v "0.1.1") (h "1kd0cv0xh7rbnk4ayy8dn5l8hp48sjgqib9rjc7ninijz1hf5xqz") (y #t)))

(define-public crate-anthill-di-0.1.2 (c (n "anthill-di") (v "0.1.2") (h "1ghrlvi56bchlgzw35w3bck0jvfq3aybqq62chxxx3bggh2p1j79") (y #t)))

(define-public crate-anthill-di-0.1.3 (c (n "anthill-di") (v "0.1.3") (h "0qkrlsrc2w0zvk81xyimvya8jm76y7vry2vvv3v1vyfvjg8b11j7") (y #t)))

(define-public crate-anthill-di-0.1.4 (c (n "anthill-di") (v "0.1.4") (h "0ff9abb2qrah2lprgdz0v4m86yy8387i4zhcs42zclx54px16bfx") (y #t)))

(define-public crate-anthill-di-0.1.5 (c (n "anthill-di") (v "0.1.5") (d (list (d (n "tokio") (r "^1.12.0") (f (quote ("full"))) (d #t) (k 0)))) (h "06x58ldka5sz81rk0jl53finj2abjnghpvlizn6sf615hp927il6") (y #t)))

(define-public crate-anthill-di-0.1.6 (c (n "anthill-di") (v "0.1.6") (d (list (d (n "tokio") (r "^1.12.0") (f (quote ("full"))) (d #t) (k 0)))) (h "03xl6nj4mf4qfsq5b1hi4z7ay52z859l2k2qjmq9dsg7ikcpylpf") (y #t)))

(define-public crate-anthill-di-0.1.7 (c (n "anthill-di") (v "0.1.7") (d (list (d (n "tokio") (r "^1.12.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1r7vbnpdbb7854c16ayz1gj7afh779z59i4s38g0znwk5i3g7h4k") (y #t)))

(define-public crate-anthill-di-0.1.8 (c (n "anthill-di") (v "0.1.8") (d (list (d (n "tokio") (r "^1.12.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0r6w8gfccdpdarnq24ww7is9h8fh8a2125y6gcl1lkrlzmhlz2rw") (y #t)))

(define-public crate-anthill-di-1.0.0 (c (n "anthill-di") (v "1.0.0") (d (list (d (n "async-recursion") (r "^1.0.0") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.52") (d #t) (k 0)) (d (n "tokio") (r "^1.15.0") (f (quote ("full"))) (d #t) (k 0)))) (h "02qbiplsvy95vqp6axz8jq40clzqk6ai9m9l68jh34l2b3jkw3ck") (y #t)))

(define-public crate-anthill-di-1.0.1 (c (n "anthill-di") (v "1.0.1") (d (list (d (n "async-recursion") (r "^1.0.0") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.52") (d #t) (k 0)) (d (n "tokio") (r "^1.15.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1ysc3wjhdsvhr890lakh44mz9l4hqw8ilbfvf2if0dayh3ccr9xg") (y #t)))

(define-public crate-anthill-di-1.0.2 (c (n "anthill-di") (v "1.0.2") (d (list (d (n "async-recursion") (r "^1.0.0") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.52") (d #t) (k 0)) (d (n "tokio") (r "^1.15.0") (f (quote ("full"))) (d #t) (k 0)))) (h "120k0sqi14gqb9m909vlrpzczn6k34hh17m46yy0igy6fbyiqscm") (y #t)))

(define-public crate-anthill-di-1.0.3 (c (n "anthill-di") (v "1.0.3") (d (list (d (n "async-recursion") (r "^1.0.0") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.52") (d #t) (k 0)) (d (n "tokio") (r "^1.15.0") (f (quote ("full"))) (d #t) (k 0)))) (h "142pfjccd1q2kvvrbkfif4s3j66bdramw5yzk4102bkyb4wvm2ch") (y #t)))

(define-public crate-anthill-di-1.0.4 (c (n "anthill-di") (v "1.0.4") (d (list (d (n "async-recursion") (r "^1.0.0") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.52") (d #t) (k 0)) (d (n "tokio") (r "^1.15.0") (f (quote ("full"))) (d #t) (k 0)))) (h "08wmvhc9psmk3slqbrrzdwhba5644f49kcn1j3gv7170wzhkpkv2") (y #t)))

(define-public crate-anthill-di-1.0.5 (c (n "anthill-di") (v "1.0.5") (d (list (d (n "async-recursion") (r "^1.0.0") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.52") (d #t) (k 0)) (d (n "tokio") (r "^1.15.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1yjx58b5a62rfai56fib0adl3j6kdwmrrhpx3y1flk0rsxr31jnv") (y #t)))

(define-public crate-anthill-di-1.0.6 (c (n "anthill-di") (v "1.0.6") (d (list (d (n "async-recursion") (r "^1.0.0") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.52") (d #t) (k 0)) (d (n "tokio") (r "^1.15.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0i8zjf0s02lrlkfbpjssbq0hlsqz76rc2fmwhav9d974ddymlc20") (y #t)))

(define-public crate-anthill-di-1.1.0 (c (n "anthill-di") (v "1.1.0") (d (list (d (n "async-recursion") (r "^1.0.0") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.52") (d #t) (k 0)) (d (n "tokio") (r "^1.15.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (d #t) (k 0)))) (h "118ijv7ijp6yi86vv5jvh1hfdd1n9lwsgdqndg784z4qaq0qyy5a") (y #t)))

(define-public crate-anthill-di-1.2.0 (c (n "anthill-di") (v "1.2.0") (d (list (d (n "async-trait") (r "^0.1.52") (d #t) (k 0)) (d (n "criterion") (r "^0.3.4") (f (quote ("async_tokio"))) (d #t) (k 2)) (d (n "criterion-macro") (r "^0.3.4") (d #t) (k 2)) (d (n "derive-new") (r "^0.5.9") (d #t) (k 0)) (d (n "tokio") (r "^1.15.0") (f (quote ("full"))) (d #t) (k 0)))) (h "060nqkjq201srq7wygfz4kj0a11wp0j30h9axj6ra316qh62diyk") (y #t)))

(define-public crate-anthill-di-1.2.1 (c (n "anthill-di") (v "1.2.1") (d (list (d (n "async-trait") (r "^0.1.52") (d #t) (k 0)) (d (n "criterion") (r "^0.3.4") (f (quote ("async_tokio"))) (d #t) (k 2)) (d (n "criterion-macro") (r "^0.3.4") (d #t) (k 2)) (d (n "derive-new") (r "^0.5.9") (d #t) (k 0)) (d (n "tokio") (r "^1.15.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1lw5l2wq36rh5l27aqri3xypfi3h7zc7ys9lqihjdxjyc9d1n8a0") (y #t)))

(define-public crate-anthill-di-1.2.2 (c (n "anthill-di") (v "1.2.2") (d (list (d (n "async-trait") (r "^0.1.52") (d #t) (k 0)) (d (n "criterion") (r "^0.3.4") (f (quote ("async_tokio"))) (d #t) (k 2)) (d (n "criterion-macro") (r "^0.3.4") (d #t) (k 2)) (d (n "derive-new") (r "^0.5.9") (d #t) (k 0)) (d (n "tokio") (r "^1.15.0") (f (quote ("full"))) (d #t) (k 0)))) (h "13qb9a9jyx6706s36vdkvhgz43lsw3914a690a2f0p6llqvairia") (y #t)))

(define-public crate-anthill-di-1.2.3 (c (n "anthill-di") (v "1.2.3") (d (list (d (n "async-trait-with-sync") (r "^0.1.36") (d #t) (k 0)) (d (n "criterion") (r "^0.3.4") (f (quote ("async_tokio"))) (d #t) (k 2)) (d (n "derive-new") (r "^0.5.9") (d #t) (k 0)) (d (n "tokio") (r "^1.15.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1wwc0dnlrk0glxxcinnm0cr8skwlqagsnghjzlv7sf87bgjxq26x")))

(define-public crate-anthill-di-1.2.4 (c (n "anthill-di") (v "1.2.4") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "async-trait-with-sync") (r "^0.1.36") (d #t) (k 0)) (d (n "criterion") (r "^0.3.4") (f (quote ("async_tokio"))) (d #t) (k 2)) (d (n "derive-new") (r "^0.5.9") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1.15.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0bygjayjijdcj6qvbpwfh53n9sqkkbf5v0lfqkc2w9vwjii57znn") (f (quote (("loop-check") ("default" "loop-check" "async-mode") ("debug-type-info") ("blocking" "async-mode") ("async-mode"))))))

