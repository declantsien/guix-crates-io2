(define-module (crates-io an yl anylist) #:use-module (crates-io))

(define-public crate-anylist-0.1.0 (c (n "anylist") (v "0.1.0") (h "0qiq19v9aivcbbkyck6d0zq4cnxvcj9k28xjrrxwks377b88whyk") (y #t) (r "1.56")))

(define-public crate-anylist-0.2.0 (c (n "anylist") (v "0.2.0") (h "0sq9lsjp85w5vka33djjdnr7ssr50qa1kb9cbcd5jhwhg3b7pshm") (y #t) (r "1.56")))

(define-public crate-anylist-0.2.1 (c (n "anylist") (v "0.2.1") (h "02w8r5i047c3xpkmzsm97i0zmqla7ysw6w1yvsnrkacxns1vr3ln") (y #t) (r "1.56")))

(define-public crate-anylist-0.2.2 (c (n "anylist") (v "0.2.2") (h "0alab4vc8qkwlk3338byx4n9m7p3yrk2ayji08cp3k0hb623g82m") (y #t) (r "1.56")))

(define-public crate-anylist-0.2.3 (c (n "anylist") (v "0.2.3") (h "0qnzqqfzf89km9a8fmawsd6hakkl5m0lyqxsr5frvyxh9hrzwajh") (y #t) (r "1.56")))

(define-public crate-anylist-0.3.3 (c (n "anylist") (v "0.3.3") (h "1xqsbbk4jdymml354hjyyk5w8difxf5sf13l3hydp983k0fm1abv") (y #t) (r "1.56")))

(define-public crate-anylist-0.4.3 (c (n "anylist") (v "0.4.3") (h "08r35nfgh21wr8m70lpp45843mls6sqsvnjq5ji9is5kczjq3icl") (y #t) (r "1.56")))

(define-public crate-anylist-0.5.3 (c (n "anylist") (v "0.5.3") (h "0mg8cpicki169xlvjs6j2kd82hlkhj4yaybz7aqi9jxhjv0mr897") (y #t) (r "1.56")))

(define-public crate-anylist-0.5.4 (c (n "anylist") (v "0.5.4") (h "10xyxz14w3qp7mb4ac7qg60g8ap8d5dlqs8zd3n0vzvzi05mzc8d") (r "1.56")))

(define-public crate-anylist-0.6.4 (c (n "anylist") (v "0.6.4") (h "0a8l18r13qawf7zwbzbm06ml86z8lx1kasivfbhpcajm6b1p22jr") (r "1.56")))

