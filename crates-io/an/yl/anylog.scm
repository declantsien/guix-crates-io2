(define-module (crates-io an yl anylog) #:use-module (crates-io))

(define-public crate-anylog-0.1.0 (c (n "anylog") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.11") (d #t) (k 0)) (d (n "regex") (r "^0.2.2") (d #t) (k 0)))) (h "101zh7hhms36r9ag7aif59vi2ihx5iivdm517b47n4j0smy63796")))

(define-public crate-anylog-0.2.0 (c (n "anylog") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.11") (d #t) (k 0)) (d (n "regex") (r "^0.2.2") (d #t) (k 0)))) (h "12xjy3dfwq8qy7s3sh4szzqckxgjqcyxhbsww5x2bf9v3i675b3f")))

(define-public crate-anylog-0.2.1 (c (n "anylog") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.11") (d #t) (k 0)) (d (n "regex") (r "^0.2.2") (d #t) (k 0)))) (h "1mfkjfdg8nc96y2rrwdkkh3nd9hky2rh16c864y8a2cz23wdrr1r")))

(define-public crate-anylog-0.3.0 (c (n "anylog") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.0") (d #t) (k 0)))) (h "0giwk9ljxsp4icknq6c1wjvdsz15611gibxz0ji5r1q1jdmff37v")))

(define-public crate-anylog-0.3.1 (c (n "anylog") (v "0.3.1") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.0") (d #t) (k 0)))) (h "1waarkd98fk2xpp6q7d3hb1rzvgrjd37xjsmjyg5gg1r8b36hmbf")))

(define-public crate-anylog-0.4.0 (c (n "anylog") (v "0.4.0") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "insta") (r "^0.6.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.0") (d #t) (k 0)))) (h "196h08lpl7bxp1y0p0m1v6s5c1v3099k0a4nfdmm5j3vywfpbkrg")))

(define-public crate-anylog-0.5.0 (c (n "anylog") (v "0.5.0") (d (list (d (n "chrono") (r "^0.4.10") (d #t) (k 0)) (d (n "insta") (r "^0.12.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.3") (f (quote ("std"))) (k 0)))) (h "0vmqzww48mh5xzfppqf4drmjj0xdfrws02w5crqfhx3z1ml7fdkv")))

(define-public crate-anylog-0.6.0 (c (n "anylog") (v "0.6.0") (d (list (d (n "chrono") (r "^0.4.10") (d #t) (k 0)) (d (n "insta") (r "^0.12.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.3") (f (quote ("std"))) (k 0)))) (h "0pxybj2ynhsslsgh3j20s6w99sz9h6gip7rwz3xfpblpsisgs225")))

(define-public crate-anylog-0.6.1 (c (n "anylog") (v "0.6.1") (d (list (d (n "chrono") (r "^0.4.10") (d #t) (k 0)) (d (n "insta") (r "^0.12.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.3") (f (quote ("std"))) (k 0)))) (h "0c279var6hvins8waifap9mvcs74y5dr4nv14nfvp47nzngdmhbb")))

(define-public crate-anylog-0.6.2 (c (n "anylog") (v "0.6.2") (d (list (d (n "chrono") (r "^0.4.10") (d #t) (k 0)) (d (n "insta") (r "^1.21.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.3") (f (quote ("std"))) (k 0)))) (h "02sh89cnj3zbjgbdfd8yi939ik6rymhjfi4vihry294g0nwgmlbv")))

(define-public crate-anylog-0.6.3 (c (n "anylog") (v "0.6.3") (d (list (d (n "chrono") (r "^0.4.10") (d #t) (k 0)) (d (n "insta") (r "^1.21.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.3") (f (quote ("std"))) (k 0)))) (h "1w8j5vp7r09c4cdmyrmgw9p7mi4jkpb3b4fkjxc2d5v6gcqcc5ls")))

(define-public crate-anylog-0.6.4 (c (n "anylog") (v "0.6.4") (d (list (d (n "chrono") (r "^0.4.10") (f (quote ("clock" "std"))) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.3") (f (quote ("std"))) (k 0)) (d (n "insta") (r "^1.21.0") (d #t) (k 2)))) (h "0pr2zkl4cq4gwf94iy0dk0vwyif81bi0lqbbi1p2pjk75mxrlbhy")))

