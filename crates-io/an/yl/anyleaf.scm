(define-module (crates-io an yl anyleaf) #:use-module (crates-io))

(define-public crate-anyleaf-0.1.0 (c (n "anyleaf") (v "0.1.0") (d (list (d (n "ads1x1x") (r "^0.2.0") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.3") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "nb") (r "^0.1.2") (d #t) (k 0)))) (h "1pqshpvkrmki9f07irwffq12gh5g5apm3fvygpph0i4b4108sv3b")))

(define-public crate-anyleaf-0.1.1 (c (n "anyleaf") (v "0.1.1") (d (list (d (n "ads1x1x") (r "^0.2.0") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.3") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "nb") (r "^0.1.2") (d #t) (k 0)))) (h "1gqhfkyn611qddp3vcv80rah728dhj6whxdgbww3snm2cr6n96i0")))

(define-public crate-anyleaf-0.1.3 (c (n "anyleaf") (v "0.1.3") (d (list (d (n "ads1x1x") (r "^0.2.0") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.3") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "nb") (r "^0.1.2") (d #t) (k 0)))) (h "1cfq9cx5xf4jyiwwfhxkca2rgp5d2dpnpjz4d5wb4z8mp97pz664")))

(define-public crate-anyleaf-0.1.4 (c (n "anyleaf") (v "0.1.4") (d (list (d (n "ads1x1x") (r "^0.2.0") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.3") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "nb") (r "^0.1.2") (d #t) (k 0)))) (h "02bilrfbr8b33y58yv7kdkv2mm8f0lhl11haddm983l1s1p5b12v")))

(define-public crate-anyleaf-0.1.5 (c (n "anyleaf") (v "0.1.5") (d (list (d (n "ads1x1x") (r "^0.2.0") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.3") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "filter") (r "^0.1.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.18.0") (k 0)) (d (n "nb") (r "^0.1.2") (d #t) (k 0)))) (h "0w59is0c5lg6mnqklggpcj44xp91ixbmcc5r8yikcxhzraqgdng9")))

(define-public crate-anyleaf-0.1.6 (c (n "anyleaf") (v "0.1.6") (d (list (d (n "ads1x1x") (r "^0.2.1") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.3") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "filter") (r "^0.2.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.21.0") (k 0)) (d (n "nb") (r "^0.1.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.11") (f (quote ("libm"))) (k 0)))) (h "1drzz34qis9nj2xwj71gii61avh8v9qjk6ziji7v9wl14vw7nn71")))

(define-public crate-anyleaf-1.0.0 (c (n "anyleaf") (v "1.0.0") (d (list (d (n "embedded-hal") (r "^0.2.4") (f (quote ("unproven"))) (d #t) (k 0)))) (h "1qpy14a17nkczccz5c5kcv4f1wfw808fypkk2dgbkac7y6987lz9")))

(define-public crate-anyleaf-1.0.1 (c (n "anyleaf") (v "1.0.1") (d (list (d (n "embedded-hal") (r "^0.2.4") (f (quote ("unproven"))) (d #t) (k 0)))) (h "0nhy7jpx8s77l57p0b2zp2b6kbvkzjy98qinlds6l0z6a8dd1yfb")))

