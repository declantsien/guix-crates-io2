(define-module (crates-io an co ancora) #:use-module (crates-io))

(define-public crate-ancora-0.1.0 (c (n "ancora") (v "0.1.0") (d (list (d (n "anchor-lang") (r "^0.25.0") (d #t) (k 0)) (d (n "anchor-spl") (r "^0.25.0") (d #t) (k 0)))) (h "1pfvkwrz4g22bk8g9g3wvz2fg8iwk77rrrmb24qvwagzaibs0fxy") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

