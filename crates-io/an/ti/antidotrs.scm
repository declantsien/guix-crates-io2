(define-module (crates-io an ti antidotrs) #:use-module (crates-io))

(define-public crate-antidotrs-0.1.0 (c (n "antidotrs") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "daemonize") (r "^0.5.0") (d #t) (k 0)) (d (n "toml") (r "^0.8.8") (d #t) (k 0)))) (h "0qn2h1yfcj0cmfmijp7070x2d8wlm0kqrlhr9zwq5x2i8s2avw5c") (y #t)))

