(define-module (crates-io an ti anti-r) #:use-module (crates-io))

(define-public crate-anti-r-0.9.0 (c (n "anti-r") (v "0.9.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "rstar") (r "^0.8.2") (d #t) (k 2)))) (h "1ca1pp27ds358dppllhdq6h18p6cn0kwh6n4gv77lxj236hpalz0") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-anti-r-0.9.1 (c (n "anti-r") (v "0.9.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "rstar") (r "^0.8.2") (d #t) (k 2)))) (h "1jcabczc5pj9m9w4a1z1f9g24nil82gcfgw3x3ylrln7b6rylplm") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-anti-r-0.9.2 (c (n "anti-r") (v "0.9.2") (d (list (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "rstar") (r "^0.8.2") (d #t) (k 2)))) (h "18jb72pmf0bwmffxq49b4m12m3mi0p4aivql2wcf954hvlmww3j2") (f (quote (("default" "alloc") ("alloc"))))))

