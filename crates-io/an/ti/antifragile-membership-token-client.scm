(define-module (crates-io an ti antifragile-membership-token-client) #:use-module (crates-io))

(define-public crate-antifragile-membership-token-client-0.1.0 (c (n "antifragile-membership-token-client") (v "0.1.0") (d (list (d (n "ic-cdk") (r "^0.3.0") (d #t) (k 0)) (d (n "ic-event-hub") (r "^0.1.1") (d #t) (k 0)) (d (n "ic-event-hub-macros") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)))) (h "1nabf6zvq9wx9xvwd7g03q9766b8wxiw0p9ksaw439i996s8yzki")))

(define-public crate-antifragile-membership-token-client-0.1.1 (c (n "antifragile-membership-token-client") (v "0.1.1") (d (list (d (n "ic-cdk") (r "^0.3.0") (d #t) (k 0)) (d (n "ic-event-hub") (r "^0.1.1") (d #t) (k 0)) (d (n "ic-event-hub-macros") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)))) (h "0hzf9c2q0rhdmb07igclsm55lq7r8pv8l9v5fmywpp27k0xm7k5l")))

(define-public crate-antifragile-membership-token-client-0.1.2 (c (n "antifragile-membership-token-client") (v "0.1.2") (d (list (d (n "ic-cdk") (r "^0.3.0") (d #t) (k 0)) (d (n "ic-event-hub") (r "^0.1.1") (d #t) (k 0)) (d (n "ic-event-hub-macros") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)))) (h "0c8p07qb8813vka646c94p9g75g4q324xia7rsw42w3g2kpaxlic")))

(define-public crate-antifragile-membership-token-client-0.1.3 (c (n "antifragile-membership-token-client") (v "0.1.3") (d (list (d (n "ic-cdk") (r "^0.3.0") (d #t) (k 0)) (d (n "ic-event-hub") (r "^0.1.1") (d #t) (k 0)) (d (n "ic-event-hub-macros") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)))) (h "17ji73advg7narkm88m1fz64ba25hgghy7hwwzp46h825llbxmzx")))

