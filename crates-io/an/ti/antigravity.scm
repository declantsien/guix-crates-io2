(define-module (crates-io an ti antigravity) #:use-module (crates-io))

(define-public crate-antigravity-0.0.1 (c (n "antigravity") (v "0.0.1") (h "1rhdpp06rmvqf06wffcdx33l0x6hyivn015xrqay98iljy0gp43x")))

(define-public crate-antigravity-0.0.2 (c (n "antigravity") (v "0.0.2") (h "1vbjajjffvzyl4084zqsflj93wj10i4wl8m71hv4rznqnhc27hlz")))

(define-public crate-antigravity-0.0.3 (c (n "antigravity") (v "0.0.3") (h "027y4hnjg4x2fbdcjrggcy6yyaii9fpgq98sc12bris4i9fac9fm")))

(define-public crate-antigravity-0.0.4 (c (n "antigravity") (v "0.0.4") (h "05g0g9h6d4i9gynqgcvy4zcwy5xhrv8dcfghpx6nibs1ya7wr736")))

(define-public crate-antigravity-0.0.5 (c (n "antigravity") (v "0.0.5") (d (list (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "webbrowser") (r "^0.7.1") (d #t) (k 0)))) (h "1jadigf3v1ny480cf9212dkajk4scyv29qy1nmalzjplgi0z4zxl")))

