(define-module (crates-io an ti antidot-rs) #:use-module (crates-io))

(define-public crate-antidot-rs-0.0.1 (c (n "antidot-rs") (v "0.0.1") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "envmnt") (r "^0.10.4") (d #t) (k 0)) (d (n "ezio") (r "^0.1.2") (d #t) (k 0)) (d (n "snor") (r "^0.1.1") (d #t) (k 0)))) (h "0da29r669pd9x7gbi2qha55bcj9gf0kh28mwl3gfxxbpgr2dnljj") (y #t)))

(define-public crate-antidot-rs-0.0.2 (c (n "antidot-rs") (v "0.0.2") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "envmnt") (r "^0.10.4") (d #t) (k 0)) (d (n "ezio") (r "^0.1.2") (d #t) (k 0)) (d (n "snor") (r "^0.1.1") (d #t) (k 0)))) (h "0lrrayv6h9l1yhdx1lzjdxhbnmxqc4g2bjak3cnwdf9vq47as7a3") (y #t)))

(define-public crate-antidot-rs-0.1.4 (c (n "antidot-rs") (v "0.1.4") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "daemonize") (r "^0.5.0") (d #t) (k 0)) (d (n "envmnt") (r "^0.10.4") (d #t) (k 0)) (d (n "ezio") (r "^0.1.2") (d #t) (k 0)) (d (n "snor") (r "^0.1.1") (d #t) (k 0)))) (h "10qa04cc52z832y82c8hizh8hqzwg4jdgbqf64x7c1zrrjl1ir8p") (y #t)))

(define-public crate-antidot-rs-0.1.5 (c (n "antidot-rs") (v "0.1.5") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "daemonize") (r "^0.5.0") (d #t) (k 0)) (d (n "envmnt") (r "^0.10.4") (d #t) (k 0)) (d (n "ezio") (r "^0.1.2") (d #t) (k 0)) (d (n "snor") (r "^0.1.1") (d #t) (k 0)))) (h "0p0x15c8wy8lyibr98nfzdc9dvajks6fnhz1gs6kwwxylp0br1hx") (y #t)))

(define-public crate-antidot-rs-0.1.6 (c (n "antidot-rs") (v "0.1.6") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "daemonize") (r "^0.5.0") (d #t) (k 0)) (d (n "envmnt") (r "^0.10.4") (d #t) (k 0)) (d (n "ezio") (r "^0.1.2") (d #t) (k 0)) (d (n "snor") (r "^0.1.1") (d #t) (k 0)))) (h "138sc501ls4dbfhd9f7crvrhqac9fksjrs7sh4rsknfkvhgf5c5h") (y #t)))

(define-public crate-antidot-rs-0.1.7 (c (n "antidot-rs") (v "0.1.7") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "daemonize") (r "^0.5.0") (d #t) (k 0)) (d (n "envmnt") (r "^0.10.4") (d #t) (k 0)) (d (n "ezio") (r "^0.1.2") (d #t) (k 0)) (d (n "snor") (r "^0.1.1") (d #t) (k 0)))) (h "1gl248jq42hygr38m7w5p9m1wqgv7ycw1vccy1nwmzcdsavbi0sj") (y #t)))

(define-public crate-antidot-rs-0.1.8 (c (n "antidot-rs") (v "0.1.8") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "daemonize") (r "^0.5.0") (d #t) (k 0)) (d (n "envmnt") (r "^0.10.4") (d #t) (k 0)) (d (n "ezio") (r "^0.1.2") (d #t) (k 0)) (d (n "snor") (r "^0.1.1") (d #t) (k 0)))) (h "11np699dpcls3fkz3925wd1b5gr3pxz4hkq3zd7xdgs39w9azicg") (y #t)))

(define-public crate-antidot-rs-0.1.9 (c (n "antidot-rs") (v "0.1.9") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "daemonize") (r "^0.5.0") (d #t) (k 0)) (d (n "envmnt") (r "^0.10.4") (d #t) (k 0)) (d (n "ezio") (r "^0.1.2") (d #t) (k 0)))) (h "0zfpzmh4rrcj1yavpl114ls03gkv6fjhshj74fnk0zhyq1dwjaw6") (y #t)))

(define-public crate-antidot-rs-0.2.0 (c (n "antidot-rs") (v "0.2.0") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "daemonize") (r "^0.5.0") (d #t) (k 0)) (d (n "envmnt") (r "^0.10.4") (d #t) (k 0)) (d (n "ezio") (r "^0.1.2") (d #t) (k 0)) (d (n "snor") (r "^0.1.1") (d #t) (k 0)))) (h "1gd12j97k04gadkl5fd4vp74si055hh634x4aak8hv9sd8xkl972") (y #t)))

(define-public crate-antidot-rs-0.2.1 (c (n "antidot-rs") (v "0.2.1") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "daemonize") (r "^0.5.0") (d #t) (k 0)) (d (n "envmnt") (r "^0.10.4") (d #t) (k 0)) (d (n "ezio") (r "^0.1.2") (d #t) (k 0)) (d (n "snor") (r "^0.1.1") (d #t) (k 0)))) (h "1b3y7igkm4hw6b0l8q45dbm745vkgsh8j2qprscyz0cyb2d4vsd6") (y #t)))

