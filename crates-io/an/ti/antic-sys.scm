(define-module (crates-io an ti antic-sys) #:use-module (crates-io))

(define-public crate-antic-sys-0.1.0 (c (n "antic-sys") (v "0.1.0") (d (list (d (n "flint-sys") (r "^0.4") (d #t) (k 0)))) (h "0j5zsfzw4ck6v4flvx9fwhxlkb2645nd0b57fbc3hnjviws5h179")))

(define-public crate-antic-sys-0.1.1 (c (n "antic-sys") (v "0.1.1") (d (list (d (n "flint-sys") (r "^0.4") (d #t) (k 0)))) (h "16vdbq8h3vhc1j4d64hm6kks8skan3laln2z9ihw1kqlgis62d2h")))

(define-public crate-antic-sys-0.1.2 (c (n "antic-sys") (v "0.1.2") (d (list (d (n "flint-sys") (r "^0.5") (d #t) (k 0)))) (h "1dqyb7jgj2k3y3wll1fjr1y32igh6kskv0shyi71d7zwlr6z031p")))

(define-public crate-antic-sys-0.2.0 (c (n "antic-sys") (v "0.2.0") (d (list (d (n "flint-sys") (r "^0.5") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1cd1y5z1x9yfnaknx4pf71018b5083339jdrmxl7r7kfxjvh44pf")))

(define-public crate-antic-sys-0.2.1 (c (n "antic-sys") (v "0.2.1") (d (list (d (n "flint-sys") (r "^0.5") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "11j7fqx6z1xwlsb60wfs58wm4fp0vbm0zjivpc58q3kgpiin6i36")))

(define-public crate-antic-sys-0.3.1 (c (n "antic-sys") (v "0.3.1") (d (list (d (n "flint-sys") (r "^0.6") (f (quote ("disable-make-check"))) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("combaseapi" "knownfolders" "shlobj" "winbase" "winerror"))) (d #t) (t "cfg(target_os = \"windows\")") (k 1)))) (h "0dw7glf4z7b4r6jm26qjxmg8i2rc88lciqsmzwlk296k0khr97vi") (f (quote (("disable-make-check"))))))

(define-public crate-antic-sys-0.3.2 (c (n "antic-sys") (v "0.3.2") (d (list (d (n "flint-sys") (r "^0.6") (f (quote ("disable-make-check"))) (d #t) (k 0)) (d (n "gmp-mpfr-sys") (r "^1.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("combaseapi" "knownfolders" "shlobj" "winbase" "winerror"))) (d #t) (t "cfg(target_os = \"windows\")") (k 1)))) (h "1fv49dalijixmypb36r5sm1yjrarkn2fpkv1mh1rn17fkd9pq929") (f (quote (("disable-make-check"))))))

(define-public crate-antic-sys-0.3.3 (c (n "antic-sys") (v "0.3.3") (d (list (d (n "flint-sys") (r "^0.7") (f (quote ("disable-make-check"))) (d #t) (k 0)) (d (n "gmp-mpfr-sys") (r "^1.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("combaseapi" "knownfolders" "shlobj" "winbase" "winerror"))) (d #t) (t "cfg(target_os = \"windows\")") (k 1)))) (h "0r68jzjzn0bpfmfqxwc4gx17969vbx963r94773zwkij40a6sh9b") (f (quote (("disable-make-check"))))))

(define-public crate-antic-sys-0.3.4 (c (n "antic-sys") (v "0.3.4") (d (list (d (n "flint-sys") (r "^0.7") (f (quote ("disable-make-check"))) (d #t) (k 0)) (d (n "gmp-mpfr-sys") (r "^1.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("combaseapi" "knownfolders" "shlobj" "winbase" "winerror"))) (d #t) (t "cfg(target_os = \"windows\")") (k 1)))) (h "1545wzmpfa5vak8h0kf84lhfr6dz4jzkm2lw5gj879jabickp33d") (f (quote (("disable-make-check") ("default" "disable-make-check"))))))

(define-public crate-antic-sys-0.3.5 (c (n "antic-sys") (v "0.3.5") (d (list (d (n "derivative") (r "^2.0") (d #t) (k 0)) (d (n "flint-sys") (r "^0.7") (f (quote ("disable-make-check"))) (d #t) (k 0)) (d (n "gmp-mpfr-sys") (r "^1.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("combaseapi" "knownfolders" "shlobj" "winbase" "winerror"))) (d #t) (t "cfg(target_os = \"windows\")") (k 1)))) (h "0qx02ykc0cg15nka6ic7h9bar3w011961ywprkby3rp4c5zpymxi") (f (quote (("disable-make-check") ("default" "disable-make-check"))))))

