(define-module (crates-io an ti antiquarian) #:use-module (crates-io))

(define-public crate-antiquarian-0.1.0 (c (n "antiquarian") (v "0.1.0") (d (list (d (n "axum") (r "^0.7.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.202") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.117") (d #t) (k 0)) (d (n "sqlx") (r "^0.7.4") (f (quote ("sqlite" "runtime-tokio"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.60") (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("full"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.18") (d #t) (k 0)) (d (n "uuid") (r "^1.8.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "1rxkbm8c3i7q5fcqxir8mww6f44jkdgx1yam5yjrr6vs17dzm57d")))

