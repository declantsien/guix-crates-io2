(define-module (crates-io an ti antidote) #:use-module (crates-io))

(define-public crate-antidote-0.1.0 (c (n "antidote") (v "0.1.0") (h "0385i3rcz6vv88grjbfnnv3ilzpjnjknzmrz31g7syf8qdqhkzjp")))

(define-public crate-antidote-1.0.0 (c (n "antidote") (v "1.0.0") (h "19g2sw2qa2ibnh4x7j1snk46593jgx6y7rnvva496ynq61af5z9l")))

