(define-module (crates-io an ti antimony) #:use-module (crates-io))

(define-public crate-antimony-0.0.0 (c (n "antimony") (v "0.0.0") (h "0qwm4i521rjj340sa1p0iap4brcgc0r6l6xmi9pa7m302fccclzq")))

(define-public crate-antimony-0.0.1 (c (n "antimony") (v "0.0.1") (d (list (d (n "futures") (r "^0.1.16") (d #t) (k 0)) (d (n "futures-cpupool") (r "^0.1.6") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.24") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1.9") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1.3") (d #t) (k 0)) (d (n "tokio-uds") (r "^0.1.5") (d #t) (k 0)))) (h "043259zxj1vk6xwxz0b03i1zi8hmfw2cvra7lym4kq54q3fdd3nx")))

