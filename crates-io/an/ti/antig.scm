(define-module (crates-io an ti antig) #:use-module (crates-io))

(define-public crate-antig-0.1.0 (c (n "antig") (v "0.1.0") (d (list (d (n "clap") (r "^4.2.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.26.1") (d #t) (k 0)) (d (n "error-stack") (r "^0.3.1") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0rigy5yx5v8dih9psdvv7man9ggbddl1m5fpq9x1gdsm23m85m9x")))

(define-public crate-antig-0.1.1 (c (n "antig") (v "0.1.1") (d (list (d (n "clap") (r "^4.2.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.26.1") (d #t) (k 0)) (d (n "error-stack") (r "^0.3.1") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "022m8bpv2mm4vf281jcdxijngngbdq66wh9fxn8n657j3qiqb075")))

