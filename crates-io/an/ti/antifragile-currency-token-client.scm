(define-module (crates-io an ti antifragile-currency-token-client) #:use-module (crates-io))

(define-public crate-antifragile-currency-token-client-0.1.0 (c (n "antifragile-currency-token-client") (v "0.1.0") (d (list (d (n "ic-cdk") (r "^0.3.0") (d #t) (k 0)) (d (n "ic-event-hub") (r "^0.1.1") (d #t) (k 0)) (d (n "ic-event-hub-macros") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)))) (h "0q525163mfw5qkanqy9i490nq0m51iw86j6a7a85sc5nfsllgyxr")))

(define-public crate-antifragile-currency-token-client-0.1.3 (c (n "antifragile-currency-token-client") (v "0.1.3") (d (list (d (n "ic-cdk") (r "^0.3.0") (d #t) (k 0)) (d (n "ic-event-hub") (r "^0.1.1") (d #t) (k 0)) (d (n "ic-event-hub-macros") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)))) (h "0jdmrvjdninb598hhrdz0wcf8lgsnsjs1b0ldpg6bpayagzihppc")))

