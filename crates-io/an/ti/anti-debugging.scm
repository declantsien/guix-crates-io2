(define-module (crates-io an ti anti-debugging) #:use-module (crates-io))

(define-public crate-anti-debugging-0.0.0 (c (n "anti-debugging") (v "0.0.0") (d (list (d (n "js-sys") (r "^0.3") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "obfstr") (r "^0.3") (d #t) (k 0)) (d (n "ptrace") (r "^0.1") (d #t) (t "cfg(target_family = \"unix\")") (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("debugapi"))) (d #t) (t "cfg(all(target_arch = \"x86_64\", target_os = \"windows\"))") (k 0)))) (h "1ml6j9b3q46h9gpml1k8q1jad7j7523g0ls80yca692kcsyzshhg")))

