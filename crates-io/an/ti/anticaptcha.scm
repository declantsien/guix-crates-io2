(define-module (crates-io an ti anticaptcha) #:use-module (crates-io))

(define-public crate-anticaptcha-0.1.0 (c (n "anticaptcha") (v "0.1.0") (h "1d1nv3qspzxrqaxqsa2j2iq82w89gy017kpdk6xm260kd60d6d22")))

(define-public crate-anticaptcha-0.1.1 (c (n "anticaptcha") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1kn3n916amkq5ys0axyi5v9lsfw2k34bhck1hgiwhxhyb3lx06s4")))

