(define-module (crates-io an ti antilysis) #:use-module (crates-io))

(define-public crate-antilysis-0.1.0 (c (n "antilysis") (v "0.1.0") (d (list (d (n "sysinfo") (r "^0.29.0") (d #t) (k 0)))) (h "0fcddb0zhwz1lpg2mf3p1s55isrqsjfynkf5d3h0v3cjbi8zxyl8")))

(define-public crate-antilysis-0.1.1 (c (n "antilysis") (v "0.1.1") (d (list (d (n "sysinfo") (r "^0.29.0") (d #t) (k 0)))) (h "1a8pqap7z421v7mr701pbk3dfj8r53wwvli9knip8dr3929kchwp")))

(define-public crate-antilysis-0.1.2 (c (n "antilysis") (v "0.1.2") (d (list (d (n "sysinfo") (r "^0.30.7") (d #t) (k 0)))) (h "1fzgl5z1is16kv6pprdss02kcsqxk2g89csjp2d8vlx4ddh03k6c")))

