(define-module (crates-io an ti anti-war-handbook-crate) #:use-module (crates-io))

(define-public crate-anti-war-handbook-crate-0.1.0 (c (n "anti-war-handbook-crate") (v "0.1.0") (h "1ax1dd18f8yfvh16zlhvwna8glddbm3zvvbq7jbjk21307i87giq")))

(define-public crate-anti-war-handbook-crate-0.1.1 (c (n "anti-war-handbook-crate") (v "0.1.1") (h "1rfxr7baixdyg3fgzdwwzjh02il713jb7ny7m3796ijdl898qahs")))

