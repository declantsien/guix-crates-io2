(define-module (crates-io an ti antimony-lang) #:use-module (crates-io))

(define-public crate-antimony-lang-0.5.1 (c (n "antimony-lang") (v "0.5.1") (d (list (d (n "inkwell") (r "^0.1.0-beta.2") (f (quote ("llvm10-0"))) (o #t) (d #t) (k 0)) (d (n "rust-embed") (r "^5.7.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 0)))) (h "1x25mlgb5952j4cyr2pacz5n3jlhf98mb0np4jzjz58m3qvdq3fl") (f (quote (("default" "backend_node") ("backend_node") ("backend_llvm" "inkwell") ("backend_c"))))))

(define-public crate-antimony-lang-0.6.0 (c (n "antimony-lang") (v "0.6.0") (d (list (d (n "inkwell") (r "^0.1.0-beta.2") (f (quote ("llvm10-0"))) (o #t) (d #t) (k 0)) (d (n "rust-embed") (r "^5.7.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 0)))) (h "0nc30bhc86k79d3rc5dyakkglpl29qf4in3xxmaibs96i31yrwf7") (f (quote (("llvm" "inkwell"))))))

(define-public crate-antimony-lang-0.7.0 (c (n "antimony-lang") (v "0.7.0") (d (list (d (n "inkwell") (r "^0.1.0-beta.2") (f (quote ("llvm10-0"))) (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "qbe") (r "^1.0.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)) (d (n "rust-embed") (r "^5.7.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "1k6b19is9b53x9qx0dn6mawxa9pw93xd83lfdh8qmhbyp4rbxy0k") (f (quote (("llvm" "inkwell"))))))

(define-public crate-antimony-lang-0.8.0 (c (n "antimony-lang") (v "0.8.0") (d (list (d (n "inkwell") (r "^0.4.0") (f (quote ("llvm10-0"))) (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "qbe") (r "^1.0.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "rust-embed") (r "^5.7.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)))) (h "0pmrwqlb2arwb3zyhsvk1fb1acv136a6yw72kq8hwq44k56hhcdf") (f (quote (("llvm" "inkwell"))))))

