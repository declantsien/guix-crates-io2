(define-module (crates-io an ge angel) #:use-module (crates-io))

(define-public crate-angel-0.1.0-alpha.1 (c (n "angel") (v "0.1.0-alpha.1") (h "17j5f3ym0qjdljxbg53dx6rg64cmfs3q59fxllf1krv5l9jzzfc5")))

(define-public crate-angel-0.1.0-alpha.2 (c (n "angel") (v "0.1.0-alpha.2") (d (list (d (n "tracing") (r "^0.1.34") (o #t) (k 0)))) (h "1qrmxr8bmgmxichzyssiv7bzswvfn499cjv9dnrc92xamjwrg9rg") (f (quote (("default" "std")))) (s 2) (e (quote (("trace-errors" "dep:tracing") ("trace-calls" "dep:tracing") ("std" "tracing?/std"))))))

