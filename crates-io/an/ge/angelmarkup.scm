(define-module (crates-io an ge angelmarkup) #:use-module (crates-io))

(define-public crate-angelmarkup-1.0.0 (c (n "angelmarkup") (v "1.0.0") (d (list (d (n "regex") (r "^1.5.5") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)))) (h "1jnbpdqlr5iwpfnk8bsxra2hna3gn9iqcnk9chhs290braq9459k")))

(define-public crate-angelmarkup-1.1.0 (c (n "angelmarkup") (v "1.1.0") (d (list (d (n "cleasy") (r "^1.0.0") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)))) (h "1ypfgm6060bybmqslh7gkqcszxhrbc0k93hz3534nvp5z9pndh0l")))

