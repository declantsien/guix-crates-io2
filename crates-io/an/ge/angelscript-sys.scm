(define-module (crates-io an ge angelscript-sys) #:use-module (crates-io))

(define-public crate-angelscript-sys-0.1.0 (c (n "angelscript-sys") (v "0.1.0") (d (list (d (n "cmake") (r "^0.1.31") (d #t) (k 1)))) (h "1n7baycyd536lw344zm4mp8yf8y3zn5ajsg4kapvjx80p4d40xrd")))

(define-public crate-angelscript-sys-0.1.1 (c (n "angelscript-sys") (v "0.1.1") (d (list (d (n "cmake") (r "^0.1.31") (d #t) (k 1)))) (h "05abs11ipf0z722psdp5m0ahjjrf9mnkg5mxnr9s0sykaqkisxjq")))

(define-public crate-angelscript-sys-0.2.312 (c (n "angelscript-sys") (v "0.2.312") (d (list (d (n "cmake") (r "^0.1.31") (d #t) (k 1)))) (h "1wymrq41zg3ilnx4ldnlk6ingi3kf9nyh9acm7sjvfr8acj2r67g")))

