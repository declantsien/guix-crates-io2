(define-module (crates-io an im anim8) #:use-module (crates-io))

(define-public crate-anim8-1.0.0 (c (n "anim8") (v "1.0.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "040dkk8hhxhcp6m7wa82g7lsd3g9jvji2i6m74xwzl3vknwx343k") (f (quote (("scalar64"))))))

(define-public crate-anim8-1.0.1 (c (n "anim8") (v "1.0.1") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1mvky7gl7lwqrjhk0xhxj904n7dqphwmf6w61nfh15p35nqh23c9") (f (quote (("scalar64"))))))

(define-public crate-anim8-1.0.2 (c (n "anim8") (v "1.0.2") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1nh6y7hblfgpq3i19gyljsmjcc3lj7m0y4ckzgqxn3f7d250swzd") (f (quote (("scalar64"))))))

