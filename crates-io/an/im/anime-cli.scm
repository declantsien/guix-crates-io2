(define-module (crates-io an im anime-cli) #:use-module (crates-io))

(define-public crate-anime-cli-0.1.0 (c (n "anime-cli") (v "0.1.0") (d (list (d (n "getopts") (r "^0.2.19") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "mpv") (r "^0.2") (d #t) (k 0)) (d (n "pbr") (r "^1.0.1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.19") (d #t) (k 0)) (d (n "serde") (r "^1.0.98") (d #t) (k 0)))) (h "0w0b1v3xra718h5hqc5qhpzcm0188rvvfj869isqy3di2gpyl20r")))

