(define-module (crates-io an im animated-emojis-rs) #:use-module (crates-io))

(define-public crate-animated-emojis-rs-0.1.0 (c (n "animated-emojis-rs") (v "0.1.0") (d (list (d (n "config") (r "^0.13.4") (d #t) (k 0)) (d (n "insta") (r "^1.36.1") (f (quote ("yaml"))) (d #t) (k 2)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.12.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("macros"))) (d #t) (k 0)))) (h "1z47mc0vikv8p8jzr5g077sf6i9d6v39cxcazgzvjgc0370ryk3f")))

(define-public crate-animated-emojis-rs-0.1.1 (c (n "animated-emojis-rs") (v "0.1.1") (d (list (d (n "config") (r "^0.13") (d #t) (k 0)) (d (n "insta") (r "^1.36.1") (f (quote ("yaml"))) (d #t) (k 2)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.12.2") (d #t) (k 2)) (d (n "serde") (r "^1.0.197") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("macros"))) (d #t) (k 2)))) (h "0byi0wnmdx5gn4ada83slc8ibl2j44aziwm64bh645rsaw2drx3j")))

