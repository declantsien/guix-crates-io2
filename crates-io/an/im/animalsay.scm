(define-module (crates-io an im animalsay) #:use-module (crates-io))

(define-public crate-animalsay-0.1.0 (c (n "animalsay") (v "0.1.0") (d (list (d (n "assert_cmd") (r "^1.0.3") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.8") (d #t) (k 0)) (d (n "predicates") (r "^1.0.7") (d #t) (k 0)) (d (n "structopt") (r "^0.1.3") (d #t) (k 0)) (d (n "structopt-derive") (r "^0.1.3") (d #t) (k 0)))) (h "1lyn81l9dqq325azkqr9k8267ss5gx8lrwknrgfz47706irnl8g5")))

