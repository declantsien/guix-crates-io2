(define-module (crates-io an im animechan) #:use-module (crates-io))

(define-public crate-animechan-0.1.0 (c (n "animechan") (v "0.1.0") (d (list (d (n "dotenv") (r "^0.15.0") (o #t) (d #t) (k 0)) (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "tokio") (r "^1.19.2") (f (quote ("macros"))) (d #t) (k 2)) (d (n "tokio-test") (r "^0.4.2") (d #t) (k 2)))) (h "0v7s8dzyhd4p295ldsa4bzg72awf9cdl9dl04skr11sfk2krra56")))

