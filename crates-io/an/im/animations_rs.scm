(define-module (crates-io an im animations_rs) #:use-module (crates-io))

(define-public crate-animations_rs-0.1.0 (c (n "animations_rs") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)))) (h "1qinbab6c74q9raika1ki1kqy2kp5dspa8z9i7blw6z1mamq3fm9") (y #t)))

(define-public crate-animations_rs-0.1.1 (c (n "animations_rs") (v "0.1.1") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)))) (h "195srwiark6vc1724bjyljc75gf9wshvhy49hxfn3m46zp594kvi") (y #t)))

