(define-module (crates-io an im animalhash) #:use-module (crates-io))

(define-public crate-animalhash-0.1.0 (c (n "animalhash") (v "0.1.0") (d (list (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "08k9x20v566jrjaa6clqlw4yyp588i7iq60l7slfyp8ladnlivs9")))

(define-public crate-animalhash-0.2.0 (c (n "animalhash") (v "0.2.0") (d (list (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "09a0b1lr2ziwh911zqidj5r1vp0nlycx4kaz4qgv3k6r4nrdnrhp")))

(define-public crate-animalhash-0.2.1 (c (n "animalhash") (v "0.2.1") (d (list (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "001qycp987yp93k3k4adm4m20742fashxkvihrsc30gvjacdfnh5")))

(define-public crate-animalhash-0.2.2 (c (n "animalhash") (v "0.2.2") (d (list (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "07mypjp98vxdmb9pck72x4jdb9brn24z2kmjn4cancv30lga3h13")))

(define-public crate-animalhash-0.2.3 (c (n "animalhash") (v "0.2.3") (d (list (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "02yc9pkpgw9c8s5la4s5wgwm86xa4l987xrm7qaykn2kkpjdbmmf")))

