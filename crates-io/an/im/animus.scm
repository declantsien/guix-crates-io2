(define-module (crates-io an im animus) #:use-module (crates-io))

(define-public crate-animus-0.1.0 (c (n "animus") (v "0.1.0") (d (list (d (n "lerp") (r "^0.4.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0bqy5fk4dqyglj9g8fngrzfyp10i14sn377cmlrllfmj33zq7jkk") (y #t)))

(define-public crate-animus-0.2.0 (c (n "animus") (v "0.2.0") (d (list (d (n "lerp") (r "^0.4") (d #t) (k 0)))) (h "0x3kpaj2ixcrdw46kyf1snbj8dc76dmniryv64f5izxmbqv9vbn3") (y #t)))

