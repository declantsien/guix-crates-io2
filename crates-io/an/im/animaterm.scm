(define-module (crates-io an im animaterm) #:use-module (crates-io))

(define-public crate-animaterm-0.1.0 (c (n "animaterm") (v "0.1.0") (d (list (d (n "termios") (r "^0.3.3") (d #t) (k 0)))) (h "13iwk5flra6ciqpp61xsd8hcgr4fgrvbdn32x52vm81sbmwwhdir")))

(define-public crate-animaterm-0.1.1 (c (n "animaterm") (v "0.1.1") (d (list (d (n "termios") (r "^0.3.3") (d #t) (k 0)))) (h "1qvrqrxydr2flqi1gcqymixyc3gf891gi5k9sk74bix3br9bkfm6")))

