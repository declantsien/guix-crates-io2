(define-module (crates-io an im animgraph) #:use-module (crates-io))

(define-public crate-animgraph-0.0.0 (c (n "animgraph") (v "0.0.0") (h "0ric8wl4gl4d9a5j8yz41h86hprss6vnw9sjw4lj98k2wdhj3kgy")))

(define-public crate-animgraph-0.1.0 (c (n "animgraph") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "glam") (r "^0.24") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^1.3.3") (f (quote ("v4" "fast-rng" "macro-diagnostics" "serde"))) (o #t) (d #t) (k 0)))) (h "1g8yyq5a2a3kwpcr64a127zw3qhvgw8k77r45a7zpg999b0k37fc") (f (quote (("default" "compiler") ("compiler" "uuid"))))))

