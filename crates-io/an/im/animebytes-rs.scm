(define-module (crates-io an im animebytes-rs) #:use-module (crates-io))

(define-public crate-animebytes-rs-0.1.0 (c (n "animebytes-rs") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.51") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.6") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.68") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("full"))) (d #t) (k 2)))) (h "00ffx5mmgddii2zplybrn2vnk8g1npxmwyp7rh9d6a2a2mxkwyhw")))

(define-public crate-animebytes-rs-1.0.0 (c (n "animebytes-rs") (v "1.0.0") (d (list (d (n "reqwest") (r "^0.11.6") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.68") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("full"))) (d #t) (k 2)))) (h "1c0ic92fvhjbpks38zrcfvc7yrlrmk89yk28k78xjq97y17c60xf")))

