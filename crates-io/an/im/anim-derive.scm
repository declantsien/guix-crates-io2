(define-module (crates-io an im anim-derive) #:use-module (crates-io))

(define-public crate-anim-derive-0.1.0 (c (n "anim-derive") (v "0.1.0") (d (list (d (n "proc-macro-crate") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "proc-quote") (r "^0.4") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "16703s9a7l2fh2r4jkwlgyl73829achx5c7nr6vw3v89qm9fyfff")))

