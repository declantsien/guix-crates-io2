(define-module (crates-io an im animal_chess) #:use-module (crates-io))

(define-public crate-animal_chess-0.1.0 (c (n "animal_chess") (v "0.1.0") (d (list (d (n "array-macro") (r "^1.0.5") (d #t) (k 0)) (d (n "sdl2") (r "^0.34.0") (f (quote ("image" "unsafe_textures"))) (k 0)))) (h "05ydsnngcnl9kpwinb1xrli00rq3fs2wnjd6h6nlc085179xal4z")))

(define-public crate-animal_chess-0.1.1 (c (n "animal_chess") (v "0.1.1") (d (list (d (n "sdl2") (r "^0.34.0") (f (quote ("image" "unsafe_textures"))) (k 0)))) (h "1i0vlxdh2n6yp4a9fwkh4mlg2ii8aqyfrcqqmfb3m8vbcmk5vwws")))

