(define-module (crates-io an im animator) #:use-module (crates-io))

(define-public crate-animator-0.1.0 (c (n "animator") (v "0.1.0") (h "00y927dy3bkh8sn4cfn6dr623jvgj69f2h1kkvsic2b5jlgz9m86")))

(define-public crate-animator-0.1.1 (c (n "animator") (v "0.1.1") (h "1hgbjl6ny74vi8lbjb5bgprpp7rcl9h8vzcc0238wijddqsf6a1g")))

