(define-module (crates-io an im animality) #:use-module (crates-io))

(define-public crate-animality-0.1.0 (c (n "animality") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0ms1zkf3ddyxag3yyjkgljd8q9mgnfhps7pxg6ngg8md3d933hfa")))

(define-public crate-animality-1.0.0 (c (n "animality") (v "1.0.0") (d (list (d (n "native-tls") (r "^0.2.10") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)))) (h "1ka8m1jcb98rbbgccgjnr3r9wb6cfm16sdm6iyz6vfw3wdja1dmi")))

