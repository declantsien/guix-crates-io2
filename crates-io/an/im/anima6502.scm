(define-module (crates-io an im anima6502) #:use-module (crates-io))

(define-public crate-anima6502-0.1.0 (c (n "anima6502") (v "0.1.0") (d (list (d (n "pyo3") (r "^0.17.1") (f (quote ("extension-module"))) (d #t) (k 0)) (d (n "vm6502") (r "^0.1.0") (d #t) (k 0)))) (h "14cfkgn5lrlmk7fwsy4sp0i86h246vg1w28ql3m743saan8ix45h")))

(define-public crate-anima6502-0.1.1 (c (n "anima6502") (v "0.1.1") (d (list (d (n "pyo3") (r "^0.17.1") (f (quote ("extension-module"))) (d #t) (k 0)) (d (n "vm6502") (r "^0.1.0") (d #t) (k 0)))) (h "0jj9w9227zidqi9898c17kgavkvn3805sdxp9mhc1f8gb3g2jb2a")))

