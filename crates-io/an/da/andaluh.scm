(define-module (crates-io an da andaluh) #:use-module (crates-io))

(define-public crate-andaluh-0.1.0 (c (n "andaluh") (v "0.1.0") (h "1cfcn3y0vzfylv88pnjqk5fyxszcn9pp99mig0i5f0r9xfv0jf3q")))

(define-public crate-andaluh-0.1.1 (c (n "andaluh") (v "0.1.1") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0.1") (d #t) (k 0)) (d (n "pest") (r "^2.1.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.3.0") (d #t) (k 0)))) (h "1m8l036h0q62xjisx48qgkdg3nfi621d8456prqpbzkl1x88mn74")))

(define-public crate-andaluh-0.1.2 (c (n "andaluh") (v "0.1.2") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0.1") (d #t) (k 0)) (d (n "pest") (r "^2.1.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.3.0") (d #t) (k 0)))) (h "17vgzl5wkp48s0zvlixsin58f25wx5rcfy6l8ddw8na461pv3mfx")))

