(define-module (crates-io an ak anakin) #:use-module (crates-io))

(define-public crate-anakin-0.1.0 (c (n "anakin") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.81") (f (quote ("backtrace"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.11.3") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "nix") (r "^0.28.0") (f (quote ("process" "signal"))) (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("process" "tokio-macros" "macros" "rt" "fs" "io-util" "time"))) (d #t) (k 0)))) (h "10a7zcfy56i9xn0000dx716kq8r1rc21anw8v5braq7d0iwrpp5x")))

