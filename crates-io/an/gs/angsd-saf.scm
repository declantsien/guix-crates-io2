(define-module (crates-io an gs angsd-saf) #:use-module (crates-io))

(define-public crate-angsd-saf-0.1.0 (c (n "angsd-saf") (v "0.1.0") (d (list (d (n "angsd-io-core") (r "^0.1") (d #t) (k 0)) (d (n "bgzf") (r "^0.17") (d #t) (k 0) (p "noodles-bgzf")) (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "indexmap") (r "^1.8") (d #t) (k 0)))) (h "12ybsgi8fvprr0khdfr2mqb8jxv5brslm1583dd456pznlih3492") (f (quote (("libdeflate" "bgzf/libdeflater") ("default"))))))

