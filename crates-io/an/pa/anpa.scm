(define-module (crates-io an pa anpa) #:use-module (crates-io))

(define-public crate-anpa-0.2.0 (c (n "anpa") (v "0.2.0") (h "0k6jdv6ml0rgcvj1j9z6k11gsbgclk860fyxl9rjpihq4x4ssvrn") (r "1.75.0")))

(define-public crate-anpa-0.3.0 (c (n "anpa") (v "0.3.0") (h "1sas65xg2ba1rs5kb7fkrw1qjiwiqhd2v6cl3clgq2nphy5aj2y9") (r "1.75.0")))

(define-public crate-anpa-0.4.0 (c (n "anpa") (v "0.4.0") (h "061vnr3w2gl69swqbiyf4y4ikikkhryc001v6z4pr9aysk361x3x") (r "1.75.0")))

(define-public crate-anpa-0.4.1 (c (n "anpa") (v "0.4.1") (h "0r3424vk0b85lbn17gmdfpnv90n7zjwlypvqhay4jlsffn4gs9g4") (r "1.75.0")))

(define-public crate-anpa-0.5.0 (c (n "anpa") (v "0.5.0") (h "030457rkj0vf5m8vbgcmywhscra135zm951m9wg9wk25glgzsz8z") (r "1.75.0")))

