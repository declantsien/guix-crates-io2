(define-module (crates-io an tl antlion) #:use-module (crates-io))

(define-public crate-antlion-0.1.0 (c (n "antlion") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^1.2.1") (f (quote ("v4" "fast-rng" "macro-diagnostics"))) (d #t) (k 0)))) (h "0la91jzlxhxjr3a6r7fsj45wjvvlfml2ir3fmim0z8hb6l6d6hw4")))

(define-public crate-antlion-0.2.0 (c (n "antlion") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "05342ai5j38dg7iq5faaiywvx9fmhwnsdjz50s5cspkzv1ryb7zq")))

(define-public crate-antlion-0.3.0 (c (n "antlion") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "1jnyiqqnrl8adc1j7mzl8lkwhjh36is25mspbzzsqmxcm2jgpqq5")))

(define-public crate-antlion-0.3.1 (c (n "antlion") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "0nab2scf8w412hlalm1y0mksjdf2s997b9skpf3m272b4gaaxxxw") (r "1.56.1")))

(define-public crate-antlion-0.3.2 (c (n "antlion") (v "0.3.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "1xp1hcc5acy9bpmgwsjli79478bzzm5qplkrpsqmsingnp4ksx6d") (r "1.56.1")))

