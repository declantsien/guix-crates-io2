(define-module (crates-io an u- anu-macros) #:use-module (crates-io))

(define-public crate-anu-macros-0.1.0 (c (n "anu-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0m2z4vhwzpddwi2255q85ld1p87q3b79b4s014lgcfpymfsbsdxh")))

