(define-module (crates-io an cs ancs) #:use-module (crates-io))

(define-public crate-ancs-0.1.0 (c (n "ancs") (v "0.1.0") (d (list (d (n "nom") (r "^7.1.1") (d #t) (k 0)))) (h "077r80mp9c6hi8sn147kzg0mlqamzi38zw1qmfwp8wlnh8rpwdx8")))

(define-public crate-ancs-0.2.0 (c (n "ancs") (v "0.2.0") (d (list (d (n "nom") (r "^7.1.1") (d #t) (k 0)))) (h "17mp66kk71l1r66hi50h176d26qyk8si5lwc3gy5ysa14lc5rh2d")))

