(define-module (crates-io an at anat-rs) #:use-module (crates-io))

(define-public crate-anat-rs-0.1.0 (c (n "anat-rs") (v "0.1.0") (h "1lyvqbw6ariyjryshhs9941bh8xm3cksjlk7hdqv0pl8mfk7hjhl")))

(define-public crate-anat-rs-0.1.1 (c (n "anat-rs") (v "0.1.1") (h "0qygicac5667bgvb6gigp4crj4f4x17ljsx2wi3fh7mddx4cpzw2")))

