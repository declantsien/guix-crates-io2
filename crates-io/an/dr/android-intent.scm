(define-module (crates-io an dr android-intent) #:use-module (crates-io))

(define-public crate-android-intent-0.1.0 (c (n "android-intent") (v "0.1.0") (d (list (d (n "jni") (r "^0.20.0") (d #t) (k 0)) (d (n "ndk-context") (r "^0.1.1") (d #t) (k 0)))) (h "02nlb962gp476xlz7a0wklwfi5jhpf52j9q3752p7wnvk1gfbdv6")))

(define-public crate-android-intent-0.2.0 (c (n "android-intent") (v "0.2.0") (d (list (d (n "jni") (r "^0.20.0") (d #t) (k 0)) (d (n "ndk-context") (r "^0.1.1") (d #t) (k 0)))) (h "0alq4xzyzpb1s520ch093sza6bq8fin7dq6i15gwh91cfq1kpkd8")))

(define-public crate-android-intent-0.2.1 (c (n "android-intent") (v "0.2.1") (d (list (d (n "jni") (r "^0.20.0") (d #t) (k 0)) (d (n "ndk-context") (r "^0.1.1") (d #t) (k 0)))) (h "1m47p7fk0hdwhivvhs91jv0x2x86g6h37y8d8p1b3sqkpsr3z5vg")))

