(define-module (crates-io an dr andromeda-data-storage) #:use-module (crates-io))

(define-public crate-andromeda-data-storage-1.0.0 (c (n "andromeda-data-storage") (v "1.0.0") (d (list (d (n "andromeda-std") (r "^1.0.0") (f (quote ("module_hooks"))) (k 0)) (d (n "cosmwasm-schema") (r "^1.5.2") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.5.2") (d #t) (k 0)) (d (n "cw-utils") (r "^1.0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.127") (f (quote ("derive"))) (k 0)))) (h "0zi9p5450nhl1bn5znyi2wh5ai27kd8lwvsvqwac6gdwlcblqmsm") (f (quote (("backtraces" "cosmwasm-std/backtraces")))) (r "1.65.0")))

