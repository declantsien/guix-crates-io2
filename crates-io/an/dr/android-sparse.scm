(define-module (crates-io an dr android-sparse) #:use-module (crates-io))

(define-public crate-android-sparse-0.1.0 (c (n "android-sparse") (v "0.1.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)))) (h "0isfim2sq2ca552mm9w6r9rlqlgqhbg0175i1rrid9s0zq954371")))

(define-public crate-android-sparse-0.1.1 (c (n "android-sparse") (v "0.1.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)))) (h "0fnl35hvvh39mhc6g1vrngpchxk7swpi3iyv899lgns0mj05asbl")))

(define-public crate-android-sparse-0.2.0 (c (n "android-sparse") (v "0.2.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "crc") (r "^1") (d #t) (k 0)))) (h "1i1bg9mc274wgq0hgj464s58g82lzxgzbhmmgqf4x8p9dxlh4k4n")))

(define-public crate-android-sparse-0.3.0 (c (n "android-sparse") (v "0.3.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "crc") (r "^1") (d #t) (k 0)))) (h "114synb7kf9s24nf5cb4fcz997kib32gspa89nh91z5z8lbg3acy")))

(define-public crate-android-sparse-0.3.1 (c (n "android-sparse") (v "0.3.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "crc") (r "^1") (d #t) (k 0)))) (h "1qfkjpia6hnp927n5rj7bz6vbn835g3xzr2awi6ma3cc8f5bs0wj")))

(define-public crate-android-sparse-0.4.0 (c (n "android-sparse") (v "0.4.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "crc") (r "^1") (d #t) (k 0)))) (h "1kc2xpqf4ra2cacvcw358cj84x530w0y0gzslhh0dj0x0dd12mmx")))

(define-public crate-android-sparse-0.5.0 (c (n "android-sparse") (v "0.5.0") (d (list (d (n "assert_cmd") (r "^0.9") (d #t) (k 2)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^2") (k 0)) (d (n "crc") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1avs77xm07z8kc8k8qlgmnwrfhs9ag8h59il8fkvnmqa03rsd59b")))

(define-public crate-android-sparse-0.6.0 (c (n "android-sparse") (v "0.6.0") (d (list (d (n "assert_cmd") (r "^0.9") (d #t) (k 2)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^2") (k 0)) (d (n "crc") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "0i7y58iv6c9pgy0ijl56bzxmmv0c2kzgb7vnbalxxy8akdswaxlw")))

