(define-module (crates-io an dr andromeda-app) #:use-module (crates-io))

(define-public crate-andromeda-app-1.0.0 (c (n "andromeda-app") (v "1.0.0") (d (list (d (n "andromeda-std") (r "^1.0.0") (k 0)) (d (n "cosmwasm-schema") (r "^1.5.2") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.5.2") (f (quote ("cosmwasm_1_2"))) (d #t) (k 0)) (d (n "cw-multi-test") (r "^1.0.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.127") (d #t) (k 0)))) (h "10x7qzdzpw3hhw0vlsp6x3nirn8qash8ddkq9x3vicivr95i276b") (f (quote (("backtraces" "cosmwasm-std/backtraces")))) (r "1.65.0")))

