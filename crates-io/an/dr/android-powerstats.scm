(define-module (crates-io an dr android-powerstats) #:use-module (crates-io))

(define-public crate-android-powerstats-0.0.0 (c (n "android-powerstats") (v "0.0.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "binder") (r "^0.2.0") (d #t) (k 0) (p "binder_ndk")) (d (n "bytemuck") (r "^1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1737mlblmza69i5i42wxd7ai41x8zmqa6pmnpc9wv3xjg6f38wzn")))

