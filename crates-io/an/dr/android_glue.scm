(define-module (crates-io an dr android_glue) #:use-module (crates-io))

(define-public crate-android_glue-0.0.1 (c (n "android_glue") (v "0.0.1") (h "006psnlbpb8kv4a9gllk56swnyplgxb26brzzc24zac6g7nsfsqn")))

(define-public crate-android_glue-0.0.2 (c (n "android_glue") (v "0.0.2") (h "1nmahczgrn083ahgn7isxgprqvhdskwvdphr7qwdyby2bx3m62a5")))

(define-public crate-android_glue-0.1.0 (c (n "android_glue") (v "0.1.0") (h "0s0mw5mhp4aig3qakbfk9pp347pbj5793gnck4jf0q1lqc2p901m")))

(define-public crate-android_glue-0.1.1 (c (n "android_glue") (v "0.1.1") (h "0d7rkvi5jm9fks9znwaqmpnd1wv6pz1mbdhf7a7gwi7snbbg4ij4")))

(define-public crate-android_glue-0.1.2 (c (n "android_glue") (v "0.1.2") (h "0m32bjq7aa6jcsqqkb40d4c6n8hn651h4i8idzi8m3mipmqg766y")))

(define-public crate-android_glue-0.1.3 (c (n "android_glue") (v "0.1.3") (h "0y07v7vvirxlxbbkajglfdx4hfrm2vbzqbpwzkh5ib3vid7j25zp")))

(define-public crate-android_glue-0.2.0 (c (n "android_glue") (v "0.2.0") (h "1d5n3nxjikjhh4dnp3iylxh3byv9958m04sq3czhdq2bxhlq6nqm")))

(define-public crate-android_glue-0.2.1 (c (n "android_glue") (v "0.2.1") (h "12c134bw0gqx0hskshl8wn1p1az9yca30vzwkmkpf41kbm280avy")))

(define-public crate-android_glue-0.2.2 (c (n "android_glue") (v "0.2.2") (h "0f6dkdc6wkdnhrn12a5wi2z0ay8if84mp6dijb63k6a36yb9wa6q")))

(define-public crate-android_glue-0.2.3 (c (n "android_glue") (v "0.2.3") (h "01y495x4i9vqkwmklwn2xk7sqg666az2axjcpkr4iwngdwi48100")))

