(define-module (crates-io an dr android-sdk-manager-rs) #:use-module (crates-io))

(define-public crate-android-sdk-manager-rs-0.1.0 (c (n "android-sdk-manager-rs") (v "0.1.0") (d (list (d (n "bytes") (r "^1.1") (d #t) (k 0)) (d (n "dunce") (r "^1") (d #t) (k 0)) (d (n "globalenv") (r "^0.4") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)) (d (n "roxmltree") (r "^0.14") (d #t) (k 0)) (d (n "ureq") (r "^2.4") (d #t) (k 0)) (d (n "zip") (r "^0.5") (d #t) (k 0)))) (h "09agmaz03i0g4g6rpsq60q3rr68ick03nh5vcd4kasgc55y8cp60") (y #t)))

