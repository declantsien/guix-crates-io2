(define-module (crates-io an dr andromeda-ecosystem) #:use-module (crates-io))

(define-public crate-andromeda-ecosystem-1.0.0 (c (n "andromeda-ecosystem") (v "1.0.0") (d (list (d (n "andromeda-std") (r "^1.0.0") (k 0)) (d (n "cosmwasm-schema") (r "^1.5.2") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.5.2") (d #t) (k 0)) (d (n "cw-asset") (r "^3.0.0") (d #t) (k 0)) (d (n "cw-storage-plus") (r "^1.2.0") (d #t) (k 0)) (d (n "cw20") (r "^1.1.2") (d #t) (k 0)) (d (n "schemars") (r "^0.8.10") (d #t) (k 0)) (d (n "serde") (r "^1.0.127") (d #t) (k 0)))) (h "1ycfyjyvh7n4v021f7gww5kpn6vz0cbmvbxji9vl4xgjbd8f3h08") (f (quote (("backtraces" "cosmwasm-std/backtraces")))) (r "1.65.0")))

