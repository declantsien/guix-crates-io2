(define-module (crates-io an dr android-ndk) #:use-module (crates-io))

(define-public crate-android-ndk-0.0.1 (c (n "android-ndk") (v "0.0.1") (d (list (d (n "ffi") (r "^0.1") (d #t) (k 0) (p "android-ndk-sys")) (d (n "num_enum") (r "^0.2") (d #t) (k 0)))) (h "170fif8s3ysd9yh174vd4bqbkp9pf2fpxn3z3006n00f1jb7rhmv")))

(define-public crate-android-ndk-0.0.2 (c (n "android-ndk") (v "0.0.2") (d (list (d (n "ffi") (r "^0.1") (d #t) (k 0) (p "android-ndk-sys")) (d (n "num_enum") (r "^0.2") (d #t) (k 0)))) (h "1ic2y1b9j1aw7dn8fpms31yy2gmicbppq3iz9kw02kpc4748jvnd") (f (quote (("rustdoc" "ffi/rustdoc") ("default"))))))

(define-public crate-android-ndk-0.0.3 (c (n "android-ndk") (v "0.0.3") (d (list (d (n "ffi") (r "^0.2") (d #t) (k 0) (p "android-ndk-sys")) (d (n "num_enum") (r "^0.2") (d #t) (k 0)))) (h "0vc2gw94z4b09ycdcz2gp0q0kil2zrm9fqh2c0d85s4xp6q0am79") (f (quote (("rustdoc" "ffi/rustdoc"))))))

(define-public crate-android-ndk-0.0.4 (c (n "android-ndk") (v "0.0.4") (d (list (d (n "ffi") (r "^0.2") (d #t) (k 0) (p "android-ndk-sys")) (d (n "jni") (r "^0.13.1") (d #t) (k 0)) (d (n "num_enum") (r "^0.2") (d #t) (k 0)))) (h "1qzz600hiys8k1m2wdgmp6673w7a02fgbcwxzl7hcbg99d58ssd6") (f (quote (("rustdoc" "ffi/rustdoc") ("native_app_glue") ("default" "native_app_glue"))))))

(define-public crate-android-ndk-0.0.5 (c (n "android-ndk") (v "0.0.5") (d (list (d (n "ffi") (r "^0.2") (d #t) (k 0) (p "android-ndk-sys")) (d (n "jni") (r "^0.13.1") (d #t) (k 0)) (d (n "num_enum") (r "^0.2") (d #t) (k 0)))) (h "0xwpdpmzyjhkzfl7sl3s59q5nvzncv2a5zn6fq1r0sfmhdc25h2l") (f (quote (("rustdoc" "ffi/rustdoc") ("native_app_glue") ("default" "native_app_glue"))))))

(define-public crate-android-ndk-0.0.6 (c (n "android-ndk") (v "0.0.6") (d (list (d (n "ffi") (r "^0.2") (d #t) (k 0) (p "android-ndk-sys")) (d (n "jni") (r "^0.13.1") (d #t) (k 0)) (d (n "num_enum") (r "^0.2") (d #t) (k 0)))) (h "1vwjwhya1va2lbk7xikhwrwqqyb22ppvif3ix5vlf3v4vd3kffvv") (f (quote (("rustdoc" "ffi/rustdoc") ("native_app_glue") ("default" "native_app_glue"))))))

