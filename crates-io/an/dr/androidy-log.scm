(define-module (crates-io an dr androidy-log) #:use-module (crates-io))

(define-public crate-androidy-log-1.0.0 (c (n "androidy-log") (v "1.0.0") (h "0iv9xrbwb5sslv69pn1kgq59fvyfabph4qabmraz16n2z4z33ja1") (f (quote (("std")))) (y #t)))

(define-public crate-androidy-log-1.0.1 (c (n "androidy-log") (v "1.0.1") (h "0sxzdw41pqx30glw2w01qppd12q0lqqj8gi7wcnb35fbvijdbckl") (f (quote (("std")))) (y #t)))

(define-public crate-androidy-log-1.0.2 (c (n "androidy-log") (v "1.0.2") (h "1rcjwarw6v20hivqv2q9w6nikhvi0yfnn0jlxcsjzywxvrllk4m1") (f (quote (("std")))) (y #t)))

(define-public crate-androidy-log-1.0.3 (c (n "androidy-log") (v "1.0.3") (h "1286w26hy09wdxf07b69lwpwxydsychxhm44algdaxfyfa681sya") (f (quote (("std")))) (y #t)))

(define-public crate-androidy-log-1.0.4 (c (n "androidy-log") (v "1.0.4") (h "0ff5clyd33bp9ywmkkmkjwb8bssbzlaravqq8z81zx14ijiyz4rh") (f (quote (("std"))))))

