(define-module (crates-io an dr android_system_properties) #:use-module (crates-io))

(define-public crate-android_system_properties-0.1.0 (c (n "android_system_properties") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.126") (d #t) (k 0)))) (h "0kkdvvxqh1rvjrnhyd3gixsgq3fk1w85m5i0y3wvhazjrv11kfjy") (y #t)))

(define-public crate-android_system_properties-0.1.1 (c (n "android_system_properties") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.126") (d #t) (k 0)))) (h "0s3kqknmfq7p9mzwhyy11wlvipqawp7ccya0jsgpi7p3inqnm420")))

(define-public crate-android_system_properties-0.1.2 (c (n "android_system_properties") (v "0.1.2") (d (list (d (n "libc") (r "^0.2.126") (d #t) (k 0)))) (h "14hwsr9y4rvwz9hb4vyahf183ac7338g1wmkw8cg4qb2w9yfc2m2")))

(define-public crate-android_system_properties-0.1.4 (c (n "android_system_properties") (v "0.1.4") (d (list (d (n "libc") (r "^0.2.126") (d #t) (k 0)))) (h "17mahdmxq7gq6qyibpjhprcdl8mgh815884lwyiiq4jycghp5vfp")))

(define-public crate-android_system_properties-0.1.5 (c (n "android_system_properties") (v "0.1.5") (d (list (d (n "libc") (r "^0.2.126") (d #t) (k 0)))) (h "04b3wrz12837j7mdczqd95b732gw5q7q66cv4yn4646lvccp57l1")))

