(define-module (crates-io an dr android-tzdata) #:use-module (crates-io))

(define-public crate-android-tzdata-0.1.0 (c (n "android-tzdata") (v "0.1.0") (d (list (d (n "zip") (r "^0.6.4") (d #t) (k 2)))) (h "0p7hz4f3klh15vn78ipfg2jydj6aa84rb6f0624k5zv2c6mv8adc")))

(define-public crate-android-tzdata-0.1.1 (c (n "android-tzdata") (v "0.1.1") (d (list (d (n "zip") (r "^0.6.4") (d #t) (k 2)))) (h "1w7ynjxrfs97xg3qlcdns4kgfpwcdv824g611fq32cag4cdr96g9")))

