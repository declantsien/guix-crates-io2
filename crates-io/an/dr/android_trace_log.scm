(define-module (crates-io an dr android_trace_log) #:use-module (crates-io))

(define-public crate-android_trace_log-0.1.0 (c (n "android_trace_log") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^6") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1dbr4cjp29pc0j517ngja2p5b25f7nf0rxdmh5sz9asg0c3ikv1y")))

(define-public crate-android_trace_log-0.2.0 (c (n "android_trace_log") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("clock" "serde"))) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^6") (f (quote ("alloc" "std"))) (k 0)) (d (n "serde_crate") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0) (p "serde")))) (h "00grv3ni2g7a6nf078gmlv49hkdxkfvwianjlx59wcnc71xdimyk") (f (quote (("serde" "chrono/serde" "serde_crate"))))))

(define-public crate-android_trace_log-0.2.1 (c (n "android_trace_log") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4") (f (quote ("clock" "serde"))) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^7") (f (quote ("alloc" "std"))) (k 0)) (d (n "serde_crate") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0) (p "serde")))) (h "15jaac7ix2kllam4hpi58bhnkij232c569r3942yzczzkmii5pnv") (f (quote (("serde" "chrono/serde" "serde_crate"))))))

(define-public crate-android_trace_log-0.3.0 (c (n "android_trace_log") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("clock" "serde"))) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^7") (f (quote ("alloc" "std"))) (k 0)) (d (n "serde_crate") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0) (p "serde")))) (h "061pl0iqcdpl8f3pln8vl0x3q3rkcjfg503bq35xsr06sbk56ssv") (f (quote (("serde" "chrono/serde" "serde_crate"))))))

