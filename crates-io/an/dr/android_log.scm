(define-module (crates-io an dr android_log) #:use-module (crates-io))

(define-public crate-android_log-0.1.1 (c (n "android_log") (v "0.1.1") (d (list (d (n "android_liblog-sys") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "18w97bdkg3niard6yc4knm8msv1hwb2cbxkzmjkwbyfl29kh5q07")))

(define-public crate-android_log-0.1.2 (c (n "android_log") (v "0.1.2") (d (list (d (n "android_liblog-sys") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "09c5jvvjdp1p7whzm5d8nzm6bw983z19p61ndwbzk395mmdysflp")))

(define-public crate-android_log-0.1.3 (c (n "android_log") (v "0.1.3") (d (list (d (n "android_liblog-sys") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "16h0aycrr9pc3a4k0ls0ymwir44kkb7495lg6gxf7k30l39y006c")))

