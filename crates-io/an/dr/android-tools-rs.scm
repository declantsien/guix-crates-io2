(define-module (crates-io an dr android-tools-rs) #:use-module (crates-io))

(define-public crate-android-tools-rs-0.1.0 (c (n "android-tools-rs") (v "0.1.0") (d (list (d (n "displaydoc") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3") (d #t) (k 0)))) (h "1x649ns9ch7pdmmfnvjwj83j26xfdrsdd94246grhz6767w5ars5") (f (quote (("default" "aapt2" "bundletool") ("bundletool") ("aapt2"))))))

(define-public crate-android-tools-rs-0.1.1 (c (n "android-tools-rs") (v "0.1.1") (d (list (d (n "displaydoc") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3") (d #t) (k 0)))) (h "0c29iz4i2gvfbfx6irmq56q6aqqz85dh1syz0ydyg53b6ajq9w61") (f (quote (("default" "aapt2" "bundletool") ("bundletool") ("aapt2"))))))

