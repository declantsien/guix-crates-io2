(define-module (crates-io an dr andromeda-fungible-tokens) #:use-module (crates-io))

(define-public crate-andromeda-fungible-tokens-1.0.0 (c (n "andromeda-fungible-tokens") (v "1.0.0") (d (list (d (n "andromeda-std") (r "^1.0.0") (k 0)) (d (n "cosmwasm-schema") (r "^1.5.2") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.5.2") (d #t) (k 0)) (d (n "cw-asset") (r "^3.0.0") (d #t) (k 0)) (d (n "cw-utils") (r "^1.0.3") (d #t) (k 0)) (d (n "cw20") (r "^1.1.2") (d #t) (k 0)) (d (n "cw20-base") (r "^1.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.127") (f (quote ("derive"))) (k 0)))) (h "1zm0dsvbz5n0n583s3whzdnwrwsimkwx21cf13vmx076if528iid") (f (quote (("backtraces" "cosmwasm-std/backtraces")))) (r "1.65.0")))

