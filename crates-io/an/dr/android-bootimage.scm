(define-module (crates-io an dr android-bootimage) #:use-module (crates-io))

(define-public crate-android-bootimage-0.1.0 (c (n "android-bootimage") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.1") (d #t) (k 0)) (d (n "clap") (r "^2.26") (d #t) (k 0)) (d (n "humansize") (r "^1.0") (d #t) (k 0)) (d (n "quick-error") (r "^1.2") (d #t) (k 0)) (d (n "termcolor") (r "^0.3.2") (d #t) (k 0)))) (h "1106na6xcwd5pg2gipgs65cdf3s36m9dis5i6r6m9v8ql7w2fijk")))

