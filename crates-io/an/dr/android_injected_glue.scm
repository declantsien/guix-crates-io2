(define-module (crates-io an dr android_injected_glue) #:use-module (crates-io))

(define-public crate-android_injected_glue-0.2.2 (c (n "android_injected_glue") (v "0.2.2") (h "1z6had7pmxm0p57v9zpav6p8h52xcpjwykd24d96n600w72qph3y")))

(define-public crate-android_injected_glue-0.2.3 (c (n "android_injected_glue") (v "0.2.3") (h "0l99difb9vz4n5lnf5nnrablal29kg7frckwaiihb77jrd7y7fc0")))

