(define-module (crates-io an dr android-cli) #:use-module (crates-io))

(define-public crate-android-cli-0.1.0 (c (n "android-cli") (v "0.1.0") (h "0bc3rx38a1q2dkix793y1a8xi4h530r7h4yr4380xk65qxk6h3ph")))

(define-public crate-android-cli-0.2.0 (c (n "android-cli") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)) (d (n "clap") (r "^3.2.23") (f (quote ("default" "derive"))) (d #t) (k 0)) (d (n "dialoguer") (r "^0.10.4") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "guidon") (r "^0.4.1") (d #t) (k 0)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "ron") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.183") (f (quote ("derive"))) (d #t) (k 0)) (d (n "which") (r "^4.4.0") (d #t) (k 0)))) (h "1knfr3iggwsd732psdffblbibgkllwqab0b6p60jxyq9spfr9vw5")))

