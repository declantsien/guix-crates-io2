(define-module (crates-io an dr android_looper) #:use-module (crates-io))

(define-public crate-android_looper-0.1.0 (c (n "android_looper") (v "0.1.0") (d (list (d (n "android_looper-sys") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "05x6wmnd4d26xcaw966f60qx1v70f9jvrsqgaw03wq2lm50862d7")))

(define-public crate-android_looper-0.3.0 (c (n "android_looper") (v "0.3.0") (d (list (d (n "android_looper-sys") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1si28ayshyj4qiiaxdg27zv9ljrwdbwn5yg9ck17ls1zzcpzjppr")))

