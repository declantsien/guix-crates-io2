(define-module (crates-io an dr android_logger_lite) #:use-module (crates-io))

(define-public crate-android_logger_lite-0.0.1 (c (n "android_logger_lite") (v "0.0.1") (h "1qmmrv695dv0cc6s6hd5f8q2ybd7r15z5ppgzgh275jhj8lfsknd")))

(define-public crate-android_logger_lite-0.0.2 (c (n "android_logger_lite") (v "0.0.2") (h "0jijfd8l4m80bga7g965rfzy3b05rdlag2ri8x5rnxkijm02kq03")))

(define-public crate-android_logger_lite-0.0.3 (c (n "android_logger_lite") (v "0.0.3") (h "14hvclvrs8psx4x55x5xsjqns2ighcmp89zpql78zhdp5wrlb1rf")))

(define-public crate-android_logger_lite-0.0.4 (c (n "android_logger_lite") (v "0.0.4") (h "11cn56s8x3rmpncasw9ry4rmylf55p2r6hd4v01107kkzlj950gp")))

(define-public crate-android_logger_lite-0.1.0 (c (n "android_logger_lite") (v "0.1.0") (h "13rbww619w83dbcbkhn2h957766q9z526pimv9wnwi5dg2fx2749")))

