(define-module (crates-io an dr android_sensor-sys) #:use-module (crates-io))

(define-public crate-android_sensor-sys-0.1.0 (c (n "android_sensor-sys") (v "0.1.0") (d (list (d (n "android_looper-sys") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1n4p63988c5lsvpqcn5swhpmpcnybizac0bskznndnldnp4gp4p9")))

(define-public crate-android_sensor-sys-0.1.1 (c (n "android_sensor-sys") (v "0.1.1") (d (list (d (n "android_looper-sys") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1ahrczvm3wn3myq81ayxmnzbay9va3g5qp24jc768f49ywgryhl6")))

(define-public crate-android_sensor-sys-0.1.2 (c (n "android_sensor-sys") (v "0.1.2") (d (list (d (n "android_looper-sys") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0kpb5awadd5hn40jj6yj0rv7fjad4bd8kvm3sr6a8z1lx7242cj8")))

(define-public crate-android_sensor-sys-0.1.3 (c (n "android_sensor-sys") (v "0.1.3") (d (list (d (n "android_looper-sys") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1a8f3zdf9pbska02lcr4gphk7xdv2wqqj9h1lrhfhr1323a5qjk5")))

(define-public crate-android_sensor-sys-0.1.4 (c (n "android_sensor-sys") (v "0.1.4") (d (list (d (n "android_looper-sys") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1p957843r02cw32yxc8ipg9h1arsvchy0s4pgypmawz9hsvifpxm")))

