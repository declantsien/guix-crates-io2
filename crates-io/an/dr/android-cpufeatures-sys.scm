(define-module (crates-io an dr android-cpufeatures-sys) #:use-module (crates-io))

(define-public crate-android-cpufeatures-sys-1.0.0 (c (n "android-cpufeatures-sys") (v "1.0.0") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)))) (h "15nfbn89p4gjxzn37h8fw440xz952igcx76p0a3d3ys8b5srpvi6") (y #t)))

(define-public crate-android-cpufeatures-sys-1.0.1 (c (n "android-cpufeatures-sys") (v "1.0.1") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)))) (h "1dwz7niprr5db075mnwc5aim6n1a8rk61gy0fnkfqc2pyvjap529")))

(define-public crate-android-cpufeatures-sys-2.0.0 (c (n "android-cpufeatures-sys") (v "2.0.0") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)))) (h "0my6llbnfqsmw1871h6841q5jscr50hq87wbn4s8y6yczxxfbbm1")))

