(define-module (crates-io an dr android_logger) #:use-module (crates-io))

(define-public crate-android_logger-0.1.0 (c (n "android_logger") (v "0.1.0") (d (list (d (n "android_log-sys") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "1rzn9zpwi788acygrh9yq4vagfyjjzxkhydjjq7k5mspiyniyzgz")))

(define-public crate-android_logger-0.2.0 (c (n "android_logger") (v "0.2.0") (d (list (d (n "android_log-sys") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "1nd965kwg854hc3v9l6nj0427xa8xgmckcx6in4vzgsr70nb2pvh")))

(define-public crate-android_logger-0.2.1 (c (n "android_logger") (v "0.2.1") (d (list (d (n "android_log-sys") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "0w1fylwagg4wvlhzdp8i7y7gg3wx8vvpb0lnmgnl2a4q89nsyyzc")))

(define-public crate-android_logger-0.2.2 (c (n "android_logger") (v "0.2.2") (d (list (d (n "android_log-sys") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "00ds037qpsjhzs0p78kq05d16v1dnjj3kg7lgm2iwg4gp5wdnxms")))

(define-public crate-android_logger-0.2.3 (c (n "android_logger") (v "0.2.3") (d (list (d (n "android_log-sys") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "0a0qlpb1syl3iyxrssvb7il74i3rc0q1plq8g5l2zvjxrnqiglpl")))

(define-public crate-android_logger-0.2.4 (c (n "android_logger") (v "0.2.4") (d (list (d (n "android_log-sys") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "08jwsqya5gjl0kmzxb1qk72ljajdb2ysglfdcqlkkbs7ixslzrxm")))

(define-public crate-android_logger-0.3.0 (c (n "android_logger") (v "0.3.0") (d (list (d (n "android_log-sys") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "087wypffzmlbyzfndg8fa4a97c9y4l800kijqn0bkmr2ng170aqy")))

(define-public crate-android_logger-0.4.0 (c (n "android_logger") (v "0.4.0") (d (list (d (n "android_log-sys") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1acwfximf9863fw4hg4l1fpznqyg8qdjlniaqq7l5v8jdrjc4vn1")))

(define-public crate-android_logger-0.5.0 (c (n "android_logger") (v "0.5.0") (d (list (d (n "android_log-sys") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1fdng6a663y1gbzla5sph80b2m07iz9ifhv6vn1maxfnp60as438")))

(define-public crate-android_logger-0.5.2 (c (n "android_logger") (v "0.5.2") (d (list (d (n "android_log-sys") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "14jdqynwxsc26d7hx92k7sx30wwkv934qds14z71s1655djyy60q")))

(define-public crate-android_logger-0.5.3 (c (n "android_logger") (v "0.5.3") (d (list (d (n "android_log-sys") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1vgwdxj3vd5zygzrqwpijkhk13y1zih6vk3i2kih58n3wxski646")))

(define-public crate-android_logger-0.6.0 (c (n "android_logger") (v "0.6.0") (d (list (d (n "android_log-sys") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0fm4dky07y3i3xrlh2i1r65barv6f4k0qx4i2mp7jphrpj2r3nds")))

(define-public crate-android_logger-0.7.0 (c (n "android_logger") (v "0.7.0") (d (list (d (n "android_log-sys") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "02xy7mjy30hc6bh0npb83w1d10igais6ccsqivq4hh96h673fi6g")))

(define-public crate-android_logger-0.8.0 (c (n "android_logger") (v "0.8.0") (d (list (d (n "android_log-sys") (r "^0.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1zv64j8jx6khqk4azrlcrc4h17w5a4vp52bl8x4rsgmm5vnkfjjf")))

(define-public crate-android_logger-0.8.1 (c (n "android_logger") (v "0.8.1") (d (list (d (n "android_log-sys") (r "^0.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0aj2r23m9w751mm2v69jzaa1wwvr28i63dd2bngnqslk71yshxvk")))

(define-public crate-android_logger-0.8.2 (c (n "android_logger") (v "0.8.2") (d (list (d (n "android_log-sys") (r "^0.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1i2kg0s8gqsf1zg1jm11prrw54wwr3ij4k45c7kh05i8i80ja5rh")))

(define-public crate-android_logger-0.8.3 (c (n "android_logger") (v "0.8.3") (d (list (d (n "android_log-sys") (r "^0.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1xfpl82jp4vcyhcmz78v5581waa7nr5b9vlxca2z34gi2h3ab5fa")))

(define-public crate-android_logger-0.8.4 (c (n "android_logger") (v "0.8.4") (d (list (d (n "android_log-sys") (r "^0.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1aq48zcwrai712x0j29w1pv2rmjgbslw084a8vdc8f7ba3a29h5a")))

(define-public crate-android_logger-0.8.5 (c (n "android_logger") (v "0.8.5") (d (list (d (n "android_log-sys") (r "^0.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.6") (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1nrkywj7gj6pddlmy64qdici560kyss1yff8z9mxsdy29n05jrfr") (f (quote (("regex" "env_logger/regex") ("default" "regex"))))))

(define-public crate-android_logger-0.8.6 (c (n "android_logger") (v "0.8.6") (d (list (d (n "android_log-sys") (r "^0.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.7") (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0kj8i03fqqwxd803hrk27j2399v27ajjj9zxi2nnyml0s4nm9gcc") (f (quote (("regex" "env_logger/regex") ("default" "regex"))))))

(define-public crate-android_logger-0.9.0 (c (n "android_logger") (v "0.9.0") (d (list (d (n "android_log-sys") (r "^0.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.7") (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1jkq2a5mb6dw0yxfk277kr4dccdb18q5lhclr1nw6b8kp13sf9sh") (f (quote (("regex" "env_logger/regex") ("default" "regex"))))))

(define-public crate-android_logger-0.9.1 (c (n "android_logger") (v "0.9.1") (d (list (d (n "android_log-sys") (r "^0.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.7") (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0bvcwh5d28d4c44ljhbrh737d9k2rcn40mmz4r0xppdbzdkw74vy") (f (quote (("regex" "env_logger/regex") ("default" "regex"))))))

(define-public crate-android_logger-0.9.2 (c (n "android_logger") (v "0.9.2") (d (list (d (n "android_log-sys") (r "^0.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.7") (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0wspwzkn3fakpyz3ka0lh6h4pa66zk9kkvic2q9n70jx30y37hif") (f (quote (("regex" "env_logger/regex") ("default" "regex"))))))

(define-public crate-android_logger-0.10.0 (c (n "android_logger") (v "0.10.0") (d (list (d (n "android_log-sys") (r "^0.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.8") (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "18zld2vbz3vvi1ya39gbkhwllxdh9dv20wv8iy7087b392pbsjbv") (f (quote (("regex" "env_logger/regex") ("default" "regex"))))))

(define-public crate-android_logger-0.10.1 (c (n "android_logger") (v "0.10.1") (d (list (d (n "android_log-sys") (r "^0.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.8") (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0rigzgkaik2y7pvsilpjdy19mdq1kkamw2rdf9fjkvb5hfqhkvfr") (f (quote (("regex" "env_logger/regex") ("default" "regex"))))))

(define-public crate-android_logger-0.11.0 (c (n "android_logger") (v "0.11.0") (d (list (d (n "android_log-sys") (r "^0.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.9") (d #t) (k 0)))) (h "00pf8xwar012600xhkvrwh0nxcqc3hfa4yhrbm0jxqvx37gpsjxp") (f (quote (("regex" "env_logger/regex") ("default" "regex"))))))

(define-public crate-android_logger-0.11.1 (c (n "android_logger") (v "0.11.1") (d (list (d (n "android_log-sys") (r "^0.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.9") (d #t) (k 0)))) (h "1yshfmxsjhhc3az5b270ihvimgahvj8pbis8mw65bskxydidvsdm") (f (quote (("regex" "env_logger/regex") ("default" "regex"))))))

(define-public crate-android_logger-0.11.2 (c (n "android_logger") (v "0.11.2") (d (list (d (n "android_log-sys") (r "^0.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.9") (d #t) (k 0)))) (h "0gk1fc2y2vzk3a4692n6c9si9r3qzdw6lcf0c7a79s346bynq32c") (f (quote (("regex" "env_logger/regex") ("default" "regex")))) (y #t)))

(define-public crate-android_logger-0.11.3 (c (n "android_logger") (v "0.11.3") (d (list (d (n "android_log-sys") (r "^0.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.9") (d #t) (k 0)))) (h "0fl8ix7q1cj73lzy6xcwyrqwpvnx5aaxszawidivv9ra4h6bh6c6") (f (quote (("regex" "env_logger/regex") ("default" "regex"))))))

(define-public crate-android_logger-0.12.0 (c (n "android_logger") (v "0.12.0") (d (list (d (n "android_log-sys") (r "^0.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.9") (d #t) (k 0)))) (h "1dqflswnsjszngvfb46zggb5mha2nxc95rih0mvsgnrdlcfkwzq3") (f (quote (("regex" "env_logger/regex") ("default" "regex"))))))

(define-public crate-android_logger-0.13.0 (c (n "android_logger") (v "0.13.0") (d (list (d (n "android_log-sys") (r "^0.3") (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.9") (d #t) (k 0)))) (h "0mnpqyzv186a7nmjrmljb4zhjzr3pidpcdnyamsxraqzf5sajw3x") (f (quote (("regex" "env_logger/regex") ("default" "regex"))))))

(define-public crate-android_logger-0.13.1 (c (n "android_logger") (v "0.13.1") (d (list (d (n "android_log-sys") (r "^0.3") (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.9") (d #t) (k 0)))) (h "17na4g3x8l7g66zsh070ghrw4lnaisc1i7ybmjwqx1zka7kr191z") (f (quote (("regex" "env_logger/regex") ("default" "regex"))))))

(define-public crate-android_logger-0.13.2 (c (n "android_logger") (v "0.13.2") (d (list (d (n "android_log-sys") (r "^0.3") (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.9") (d #t) (k 0)))) (h "1ki3gvja11iw7kf7qyyz66lmfgdiym960mp7as93vi15gb2691w8") (f (quote (("regex" "env_logger/regex") ("default" "regex"))))))

(define-public crate-android_logger-0.13.3 (c (n "android_logger") (v "0.13.3") (d (list (d (n "android_log-sys") (r "^0.3") (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.9") (d #t) (k 0)))) (h "0bvp6lf39q0zykn70lys562kdb14r9dfm91m79jxq53cfi7i7564") (f (quote (("regex" "env_logger/regex") ("default" "regex"))))))

