(define-module (crates-io an dr android-properties) #:use-module (crates-io))

(define-public crate-android-properties-0.1.0 (c (n "android-properties") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)))) (h "153pnrn1vhy88ax7ipj3vvydfy2wpwz3id8l1ydyqv3b9bxwjhh3") (f (quote (("bionic-deprecated"))))))

(define-public crate-android-properties-0.2.0 (c (n "android-properties") (v "0.2.0") (h "0q0zw1za1fax9yd9vdy7gpc3zdk52nra7yyvdjagaiyq57399s0z") (f (quote (("bionic-deprecated")))) (y #t)))

(define-public crate-android-properties-0.2.1 (c (n "android-properties") (v "0.2.1") (h "0phzc7xizb8nxdy88j9k8kkxn2w96vm5wbllv2m90npl2fv6rp22") (f (quote (("bionic-deprecated")))) (y #t)))

(define-public crate-android-properties-0.2.2 (c (n "android-properties") (v "0.2.2") (h "016slvg269c0y120p9qd8vdfqa2jbw4j0g18gfw6p3ain44v4zpw") (f (quote (("bionic-deprecated"))))))

