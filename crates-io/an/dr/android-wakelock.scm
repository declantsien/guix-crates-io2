(define-module (crates-io an dr android-wakelock) #:use-module (crates-io))

(define-public crate-android-wakelock-0.1.0 (c (n "android-wakelock") (v "0.1.0") (d (list (d (n "jni") (r "^0.21") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "ndk-context") (r "^0.1") (d #t) (k 0)))) (h "0l8qbk6hkfmkbhv8f0qb1zhjnbvp9dh1062b351jgcxd4dy5nvi9")))

