(define-module (crates-io an dr andromeda) #:use-module (crates-io))

(define-public crate-andromeda-0.0.0 (c (n "andromeda") (v "0.0.0") (d (list (d (n "bincode") (r "^1.3.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "message-io") (r "^0.11.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1sdnhqdfzamwn7v4nxgq1f5p9qh7j08j00kba4zmfr94pdipvi5q") (y #t)))

