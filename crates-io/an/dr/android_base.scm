(define-module (crates-io an dr android_base) #:use-module (crates-io))

(define-public crate-android_base-0.1.0 (c (n "android_base") (v "0.1.0") (d (list (d (n "android_glue") (r "^0.2.3") (d #t) (k 0)) (d (n "glutin") (r "^0.19.0") (d #t) (k 0)) (d (n "optimisticpeach-opengles_graphics") (r "^0.1.3") (d #t) (k 0)) (d (n "piston") (r "^0.39.0") (d #t) (k 0)) (d (n "piston-viewport") (r "^0.5.0") (d #t) (k 0)) (d (n "piston2d-graphics") (r "^0.28.0") (d #t) (k 0)) (d (n "pistoncore-glutin_window") (r "^0.51.1") (d #t) (k 0)) (d (n "shader_version") (r "^0.3.0") (d #t) (k 0)) (d (n "winit") (r "^0.18.0") (d #t) (k 0)))) (h "1l0ilhfmm8vbnib4z0mghqkk04jc8md03mpdmvb3372ydzpbcbdp")))

