(define-module (crates-io an dr android-ndk-sys) #:use-module (crates-io))

(define-public crate-android-ndk-sys-0.1.0 (c (n "android-ndk-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.51") (d #t) (k 1)))) (h "0r1z15j6416xgnyvs0fcp211ycjl72isk0k6j9lh395ghv44pvlx")))

(define-public crate-android-ndk-sys-0.1.1 (c (n "android-ndk-sys") (v "0.1.1") (h "1fnny84m205481dcz4d2zrnfk9vv97pclznaq9waymw8c5zfhk7v")))

(define-public crate-android-ndk-sys-0.1.2 (c (n "android-ndk-sys") (v "0.1.2") (h "0xzz7w0wqdpnrw5lh4m851cq2s4rq5w9dzrnkmd2phi3ybh2dzkd") (f (quote (("rustdoc") ("default"))))))

(define-public crate-android-ndk-sys-0.1.3 (c (n "android-ndk-sys") (v "0.1.3") (h "0birgfancbyadppm90l3l4jqms1vqfwchq3hz8xkh95vb8k7grh8") (f (quote (("rustdoc") ("default"))))))

(define-public crate-android-ndk-sys-0.2.0 (c (n "android-ndk-sys") (v "0.2.0") (h "1zl40asqyg5c0axzl9790na2dqh82s8v60i2iqvxndcq8wlfziiy") (f (quote (("rustdoc"))))))

