(define-module (crates-io an dr andromeda-modules) #:use-module (crates-io))

(define-public crate-andromeda-modules-1.0.0 (c (n "andromeda-modules") (v "1.0.0") (d (list (d (n "andromeda-std") (r "^1.0.0") (f (quote ("module_hooks"))) (k 0)) (d (n "cosmwasm-schema") (r "^1.5.2") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.5.2") (d #t) (k 0)) (d (n "cw-utils") (r "^1.0.3") (d #t) (k 0)) (d (n "cw721") (r "^0.18.0") (d #t) (k 0)) (d (n "cw721-base") (r "^0.18.0") (f (quote ("library"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.127") (f (quote ("derive"))) (k 0)))) (h "18xfnpbmrc7pxsj56llq8mj66vwb0jnddfcxndcvbpy2bfr3ash5") (f (quote (("backtraces" "cosmwasm-std/backtraces")))) (r "1.65.0")))

