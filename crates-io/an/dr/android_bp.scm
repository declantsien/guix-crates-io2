(define-module (crates-io an dr android_bp) #:use-module (crates-io))

(define-public crate-android_bp-0.1.0 (c (n "android_bp") (v "0.1.0") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)))) (h "0g5zgvc0czrha12fiw3vb6hpknwpmhgs79yplagz92jyvl53kzwp")))

(define-public crate-android_bp-0.2.0 (c (n "android_bp") (v "0.2.0") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)))) (h "1v2aply9fdlm1wf5bg9kw2jk0wrk9smg8pzf0m1bhs9n2wsp73qj")))

