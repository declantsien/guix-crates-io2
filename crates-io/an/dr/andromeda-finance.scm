(define-module (crates-io an dr andromeda-finance) #:use-module (crates-io))

(define-public crate-andromeda-finance-1.0.0 (c (n "andromeda-finance") (v "1.0.0") (d (list (d (n "andromeda-std") (r "^1.0.0") (k 0)) (d (n "cosmwasm-schema") (r "^1.5.2") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.5.2") (d #t) (k 0)) (d (n "cw-utils") (r "^1.0.3") (d #t) (k 0)) (d (n "cw721") (r "^0.18.0") (d #t) (k 0)) (d (n "cw721-base") (r "^0.18.0") (f (quote ("library"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.127") (f (quote ("derive"))) (k 0)))) (h "1hrqb4cx4x0n56qfk4bmm5sjb92q8j9i2yni4sck11grbv0wc62g") (f (quote (("backtraces" "cosmwasm-std/backtraces")))) (r "1.65.0")))

