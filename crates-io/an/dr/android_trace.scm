(define-module (crates-io an dr android_trace) #:use-module (crates-io))

(define-public crate-android_trace-0.1.0 (c (n "android_trace") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.153") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 2)))) (h "02fyq7jymw771hmwssxm0svbp0xq5vifggrbzpkpvpddxm8xmzzj") (f (quote (("default" "api_level_23") ("api_level_29" "api_level_23") ("api_level_23")))) (r "1.77")))

