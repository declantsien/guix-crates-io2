(define-module (crates-io an dr andromeda-macros) #:use-module (crates-io))

(define-public crate-andromeda-macros-0.1.0 (c (n "andromeda-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0kd50k1r8azhn24csyg55snwz55y2vw89mvd4761l2lnkq26c9f8") (f (quote (("withdraw") ("modules") ("module_hooks")))) (r "1.65.0")))

(define-public crate-andromeda-macros-1.0.0-rc1 (c (n "andromeda-macros") (v "1.0.0-rc1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "190ysd91bdwhcsw8b9rd9x1aw5cxkv0vh4rmy0szhb8ysq36sd2r") (f (quote (("modules") ("module_hooks")))) (r "1.69.0")))

(define-public crate-andromeda-macros-1.0.0 (c (n "andromeda-macros") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1v4cv54bjcvlgb90mz8qapphw08zalzyxnkxrx4zd7nihbpnqq55") (f (quote (("modules") ("module_hooks")))) (r "1.69.0")))

