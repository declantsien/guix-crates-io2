(define-module (crates-io an dr android_looper-sys) #:use-module (crates-io))

(define-public crate-android_looper-sys-0.1.0 (c (n "android_looper-sys") (v "0.1.0") (d (list (d (n "bitflags") (r "^0.5") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "06kplvhcckafxx11iq5j1asvjsbh43migqhyjglmrllrmb3qzbr9")))

(define-public crate-android_looper-sys-0.1.1 (c (n "android_looper-sys") (v "0.1.1") (d (list (d (n "bitflags") (r "^0.5") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0v9jfyc50a8b21l370qm3lcamax9kk1464wgrs76ydr9a44z0ayl")))

(define-public crate-android_looper-sys-0.1.3 (c (n "android_looper-sys") (v "0.1.3") (d (list (d (n "bitflags") (r "^0.5") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1njw3brpv8igi5ffry0zfkd3pwz8lzia3w5jcdq9qx4kzyjpvy3p")))

(define-public crate-android_looper-sys-0.1.4 (c (n "android_looper-sys") (v "0.1.4") (d (list (d (n "bitflags") (r "^0.7") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "05n8svklpyym163hyrm4gwridqc1j8jd65g0gy6z83kdjfn1pqwr")))

(define-public crate-android_looper-sys-0.2.0 (c (n "android_looper-sys") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0nmlmqm46zmvp8rxc06xrwlk950cz7b40z70cj45y0wl5kqchgqs")))

(define-public crate-android_looper-sys-0.3.0 (c (n "android_looper-sys") (v "0.3.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0723ws4nrhbsyz1i0j7q6nkxhsa2qq4gygsmzk2bx89qc8z64ii7")))

