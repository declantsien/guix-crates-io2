(define-module (crates-io an dr android_liblog-sys) #:use-module (crates-io))

(define-public crate-android_liblog-sys-0.1.1 (c (n "android_liblog-sys") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1qjddlg1ghccrjdjz1wacvbhh4cnps8fd2mqpb0ag7j8frw5sz7k")))

(define-public crate-android_liblog-sys-0.1.2 (c (n "android_liblog-sys") (v "0.1.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1qa2ndhcag70nl92j007vxl1mj7nklipz98c9m719a9yy6sha9nd")))

(define-public crate-android_liblog-sys-0.1.3 (c (n "android_liblog-sys") (v "0.1.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0xdq40hmdbffw61j8l10a5jkh7d9dc4kjbxhsd5p9zqa5c7gynfy")))

(define-public crate-android_liblog-sys-0.1.4 (c (n "android_liblog-sys") (v "0.1.4") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1rrqlz3faw18k6l9fzgavvwps9cavwb4v9cmhnrp5jkq241jry5a")))

