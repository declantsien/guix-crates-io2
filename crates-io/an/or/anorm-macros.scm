(define-module (crates-io an or anorm-macros) #:use-module (crates-io))

(define-public crate-anorm-macros-0.1.1 (c (n "anorm-macros") (v "0.1.1") (d (list (d (n "Inflector") (r "^0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1l5dx0adq4cvgydfd0rqklkswp4qwma0262cznkmdnrzfiy9l830") (f (quote (("sqlite") ("postgres") ("mysql") ("mssql") ("default" "mysql"))))))

(define-public crate-anorm-macros-0.1.2 (c (n "anorm-macros") (v "0.1.2") (d (list (d (n "Inflector") (r "^0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1n70jc0dgbfh4b799m5hjj23666mq9lcb6y645jq32f39r2spyib") (f (quote (("sqlite") ("postgres") ("mysql") ("mssql") ("default" "mysql"))))))

(define-public crate-anorm-macros-0.1.3 (c (n "anorm-macros") (v "0.1.3") (d (list (d (n "Inflector") (r "^0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0pqcx13h60ss4jw77yz5v0f9pl23p6jxj7j4dz0p9imci6zysps9") (f (quote (("sqlite") ("postgres") ("mysql") ("mssql") ("default" "mysql"))))))

(define-public crate-anorm-macros-0.1.5 (c (n "anorm-macros") (v "0.1.5") (d (list (d (n "Inflector") (r "^0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1bn64i59w8l4330na81m8lg0f0l5xdggp1kv3p26l264555dpmz8") (f (quote (("sqlite") ("postgres") ("mysql") ("mssql") ("default" "mysql"))))))

(define-public crate-anorm-macros-0.1.6 (c (n "anorm-macros") (v "0.1.6") (d (list (d (n "Inflector") (r "^0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "07n2kfkvadc759kn4hx30kg3np3ah5q77ghnwd64fi364hb0208n") (f (quote (("sqlite") ("postgres") ("mysql") ("mssql") ("default" "mysql"))))))

