(define-module (crates-io an or anor-storage) #:use-module (crates-io))

(define-public crate-anor-storage-0.1.3 (c (n "anor-storage") (v "0.1.3") (d (list (d (n "anor-utils") (r "^0.1") (d #t) (k 0)) (d (n "bincode") (r "^2.0.0-rc.3") (d #t) (k 0)) (d (n "fs2") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "log4rs") (r "^1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)) (d (n "uuid") (r "^1.4.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "0nv04qxsghbw5p9jcmv8fi5np4mnapvzrxw6kps9mjdn8qc5d1wc")))

