(define-module (crates-io an or anor-api) #:use-module (crates-io))

(define-public crate-anor-api-0.1.0 (c (n "anor-api") (v "0.1.0") (h "04l00ryavkrl1h02pnqnpnz092smd5c0i9vnx2xd333j4h97np27")))

(define-public crate-anor-api-0.1.1 (c (n "anor-api") (v "0.1.1") (d (list (d (n "anor-storage") (r "^0.1") (d #t) (k 0)) (d (n "anor-utils") (r "^0.1") (d #t) (k 0)) (d (n "bytes") (r "^1.5.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "log4rs") (r "^1.2") (d #t) (k 0)) (d (n "prost") (r "^0.12") (d #t) (k 0)) (d (n "prost-build") (r "^0.12.1") (d #t) (k 1)))) (h "1mak8yy79bzxjivlqx09j78kvqb8jikymbpakgv6izlayrqmwqwx")))

