(define-module (crates-io an or anor-common) #:use-module (crates-io))

(define-public crate-anor-common-0.1.0 (c (n "anor-common") (v "0.1.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "log4rs") (r "^1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)))) (h "1bm24qfl1jfkiibjx2w9k8lnc1bv8v2c9jl69zi2mf5vqjv2nyyz")))

(define-public crate-anor-common-0.1.1 (c (n "anor-common") (v "0.1.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "log4rs") (r "^1.2") (d #t) (k 0)) (d (n "regex") (r "^1.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)))) (h "1al2s5xbd40rff12jpwvrfra89fd8scm5jq6ix8m14zjnwjlrwfx")))

