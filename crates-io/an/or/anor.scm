(define-module (crates-io an or anor) #:use-module (crates-io))

(define-public crate-anor-0.1.0 (c (n "anor") (v "0.1.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "log4rs") (r "^1.2") (d #t) (k 0)))) (h "16k50hxrcyfq7n7jbq4kbsdl7q4a0h2waspp0vnifp3kv6nx8w6p")))

(define-public crate-anor-0.1.1 (c (n "anor") (v "0.1.1") (d (list (d (n "bincode") (r "^2.0.0-rc.3") (d #t) (k 0)) (d (n "fs2") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "log4rs") (r "^1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)) (d (n "uuid") (r "^1.4.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "0ssjm7lwc3f512pwgkfz69gv38q3fnh5r45zfa1qdnf3i76cbgzs")))

(define-public crate-anor-0.1.2 (c (n "anor") (v "0.1.2") (d (list (d (n "bincode") (r "^2.0.0-rc.3") (d #t) (k 0)) (d (n "fs2") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "log4rs") (r "^1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)) (d (n "uuid") (r "^1.4.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "158fn8ayn8imr1lgmllh76hs874fimzd7sj1q2qpmsvb3h1pmhwx")))

