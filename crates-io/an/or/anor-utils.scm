(define-module (crates-io an or anor-utils) #:use-module (crates-io))

(define-public crate-anor-utils-0.1.0 (c (n "anor-utils") (v "0.1.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "log4rs") (r "^1.2") (d #t) (k 0)) (d (n "regex") (r "^1.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)))) (h "0z9rksn3lzbrwqn2r6i4bdfqmpz2vbdjh2kaan6q0jpi9jmc7amz")))

(define-public crate-anor-utils-0.1.1 (c (n "anor-utils") (v "0.1.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "log4rs") (r "^1.2") (d #t) (k 0)) (d (n "regex") (r "^1.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)))) (h "157z7329ajkrjbgsdrdm64k4jksswvys3nfdxg93fgml5cx5iki5")))

(define-public crate-anor-utils-0.1.4 (c (n "anor-utils") (v "0.1.4") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "log4rs") (r "^1.2") (d #t) (k 0)) (d (n "regex") (r "^1.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)))) (h "0izx21vvxsajmb8wn7pvgiw1985iwbw86iwvc5frwzizmc4dla17")))

