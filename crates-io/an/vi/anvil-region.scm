(define-module (crates-io an vi anvil-region) #:use-module (crates-io))

(define-public crate-anvil-region-0.1.0 (c (n "anvil-region") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.1") (d #t) (k 2)))) (h "062akby84vifkhdr0f4b5v8jvxcbj6y5ni0hwb4hiwzd1zg70qvg")))

(define-public crate-anvil-region-0.2.0 (c (n "anvil-region") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "named-binary-tag") (r "^0.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.1") (d #t) (k 2)))) (h "0sj8a53wm7y5n9dsxh1kqf86qidry6wv011xyj3dpqsm7kgpg19s")))

(define-public crate-anvil-region-0.3.0 (c (n "anvil-region") (v "0.3.0") (d (list (d (n "bitvec") (r "^0.15") (d #t) (k 0)) (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "named-binary-tag") (r "^0.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.1") (d #t) (k 2)))) (h "0mk948wib4c9pid4mnk9dn793cxvxhghwyq39mfvza1k7cysjv5n")))

(define-public crate-anvil-region-0.3.1 (c (n "anvil-region") (v "0.3.1") (d (list (d (n "bitvec") (r "^0.17") (d #t) (k 0)) (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "named-binary-tag") (r "^0.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.1") (d #t) (k 2)))) (h "1nmcaq2mch2mjyb6m7dvvk076z2wqsjc8h9cfcr81sk94393c7gc")))

(define-public crate-anvil-region-0.3.2 (c (n "anvil-region") (v "0.3.2") (d (list (d (n "bitvec") (r "^0.17") (d #t) (k 0)) (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "named-binary-tag") (r "^0.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.1") (d #t) (k 2)))) (h "0y3ki6j755dy72rf55fp3rly1zn1kqpbfm935hqbvkmxk4rm99a0")))

(define-public crate-anvil-region-0.4.0 (c (n "anvil-region") (v "0.4.0") (d (list (d (n "bitvec") (r "^0.17") (d #t) (k 0)) (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "named-binary-tag") (r "^0.3") (d #t) (k 0)) (d (n "tempfile") (r "^3.1") (d #t) (k 2)))) (h "17y5nn7x0bsb22rixn2pjw75kicvl1wxib0bzg4qw4lkywz63qfq")))

(define-public crate-anvil-region-0.5.0 (c (n "anvil-region") (v "0.5.0") (d (list (d (n "bitvec") (r "^0.17") (d #t) (k 0)) (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "named-binary-tag") (r "^0.3") (d #t) (k 0)))) (h "1ml72ivdbizcq7wllyip8zllmqqrs3yf28ivh23vphzaklr9h2ly")))

(define-public crate-anvil-region-0.6.0 (c (n "anvil-region") (v "0.6.0") (d (list (d (n "bitvec") (r "^0.17") (d #t) (k 0)) (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "named-binary-tag") (r "^0.3") (d #t) (k 0)))) (h "129mnqg2iaxys5n6kanpcpn79mi6kpmr2bxiszdkb3ls8sx30q6p")))

(define-public crate-anvil-region-0.7.0 (c (n "anvil-region") (v "0.7.0") (d (list (d (n "bitvec") (r "^0.17") (d #t) (k 0)) (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "named-binary-tag") (r "^0.3") (d #t) (k 0)))) (h "1ylh2m5zwkx2xh76gls8zfw5qksf6jjz5s95qrsn1l4s6w7pg40y")))

(define-public crate-anvil-region-0.8.0 (c (n "anvil-region") (v "0.8.0") (d (list (d (n "bitvec") (r "^0.17") (d #t) (k 0)) (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "named-binary-tag") (r "^0.3") (d #t) (k 0)))) (h "1vq70k8h1vcisy650rcc5pxrn5m00wvcn72cglff6vbskgnxql7j")))

(define-public crate-anvil-region-0.8.1 (c (n "anvil-region") (v "0.8.1") (d (list (d (n "bitvec") (r "^0.17") (d #t) (k 0)) (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "named-binary-tag") (r "^0.6") (d #t) (k 0)))) (h "1yf9vy2mc1ij3k0j74p5nxkykldsr9n7dvcak48qxlj94kamynr2")))

