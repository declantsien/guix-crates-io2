(define-module (crates-io an vi anvil_db) #:use-module (crates-io))

(define-public crate-anvil_db-0.2.0 (c (n "anvil_db") (v "0.2.0") (h "0w07nq7hbw2hxmn484y1l0bf078l7623f05gbi9mldj7x9drpn0r")))

(define-public crate-anvil_db-0.2.1 (c (n "anvil_db") (v "0.2.1") (h "0vb95qxvnc6v6s0an0fg46wzczx6lx4nfl7r5yn7i7lizf2mps0s")))

