(define-module (crates-io an vi anvil) #:use-module (crates-io))

(define-public crate-anvil-0.0.0 (c (n "anvil") (v "0.0.0") (h "00zi6sa6pxxyasn9l86zr6ibcippyrg727nr8y9lqh6h3l8b839c")))

(define-public crate-anvil-0.0.1 (c (n "anvil") (v "0.0.1") (h "03vf9x16kzxahd2sayr8c7hndd6n9jhvnnn9lb82k5wpyqlyw1fk")))

