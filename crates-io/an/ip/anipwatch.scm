(define-module (crates-io an ip anipwatch) #:use-module (crates-io))

(define-public crate-anipwatch-0.1.0 (c (n "anipwatch") (v "0.1.0") (d (list (d (n "chobitlibs") (r "^0.8") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "eframe") (r "^0.23") (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)))) (h "0vyp3mk5wps6y94c993hcdxf50kl028zlshmmivvg6x8xb6xgbpn")))

