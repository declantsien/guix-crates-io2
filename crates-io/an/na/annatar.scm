(define-module (crates-io an na annatar) #:use-module (crates-io))

(define-public crate-annatar-0.2.1 (c (n "annatar") (v "0.2.1") (d (list (d (n "artano") (r "^0.1") (d #t) (k 0)) (d (n "clap") (r "^2.24") (d #t) (k 0)) (d (n "reqwest") (r "^0.6") (d #t) (k 0)))) (h "1v2cps1kc41smn6ifxjx7hr1gr21ynjw71zwzjc94yzqrcinwb23")))

(define-public crate-annatar-0.2.3 (c (n "annatar") (v "0.2.3") (d (list (d (n "artano") (r "^0.1") (d #t) (k 0)) (d (n "clap") (r "^2.27") (d #t) (k 0)) (d (n "reqwest") (r "^0.8.1") (d #t) (k 0)))) (h "0lr42r2z703k5li4v3gz292jidmgpvb5xjk3ljsvfdqmpiqrla3c")))

(define-public crate-annatar-0.3.0 (c (n "annatar") (v "0.3.0") (d (list (d (n "artano") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^2.28") (d #t) (k 0)) (d (n "reqwest") (r "^0.8") (d #t) (k 0)))) (h "1s1ccjvdmm9r014pckb2iaxsikczsi7fy6arv95slppw2xrj3n55")))

(define-public crate-annatar-0.3.1 (c (n "annatar") (v "0.3.1") (d (list (d (n "artano") (r "^0.2.2") (d #t) (k 0)) (d (n "clap") (r "^2.28") (d #t) (k 0)) (d (n "reqwest") (r "^0.8") (d #t) (k 0)))) (h "1ia5a3fipmhd43rk6ll3k7v47hl6wfwxfpgj986kz40qyn1w9rf0")))

(define-public crate-annatar-0.3.2 (c (n "annatar") (v "0.3.2") (d (list (d (n "artano") (r "^0.2.3") (d #t) (k 0)) (d (n "clap") (r "^2.28") (d #t) (k 0)) (d (n "reqwest") (r "^0.8") (d #t) (k 0)))) (h "0340qlsa2zpxd779cw4zxdawgc9dqmrka1qbjxpr6z98570ain1k")))

(define-public crate-annatar-0.3.3 (c (n "annatar") (v "0.3.3") (d (list (d (n "artano") (r "^0.2.3") (d #t) (k 0)) (d (n "clap") (r "^2.28") (d #t) (k 0)) (d (n "reqwest") (r "^0.8") (d #t) (k 0)))) (h "12my6naqqggvcsc69bp2qsimz6mnmcpvf4b2hadf0dd1pq189xkz")))

(define-public crate-annatar-0.3.4 (c (n "annatar") (v "0.3.4") (d (list (d (n "artano") (r "^0.2.6") (d #t) (k 0)) (d (n "clap") (r "^2.31") (d #t) (k 0)) (d (n "reqwest") (r "^0.8") (d #t) (k 0)))) (h "12vqx00ljlvxjj77g5wlh19gva3ci6zsq9lf1q0ni669aq9aqa5n")))

(define-public crate-annatar-0.4.0 (c (n "annatar") (v "0.4.0") (d (list (d (n "artano") (r "^0.2.6") (d #t) (k 0)) (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.8") (d #t) (k 0)))) (h "11rqnnj2j6fipgblihm1c2vbny6n0shfqy147ci2fd6rnn7cfdfp")))

(define-public crate-annatar-0.4.1 (c (n "annatar") (v "0.4.1") (d (list (d (n "artano") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.8") (d #t) (k 0)))) (h "1ydiz6diqm7jyr9azzwcaz1dkpikdcplhqnzdy3zis5javyxv5rs")))

(define-public crate-annatar-0.4.2 (c (n "annatar") (v "0.4.2") (d (list (d (n "artano") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.8") (d #t) (k 0)))) (h "0wwy26zyikwjgcifr50nldfg957ihhi72khbcz5jr34vp6pwanqm")))

(define-public crate-annatar-0.4.3 (c (n "annatar") (v "0.4.3") (d (list (d (n "artano") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)))) (h "1gdqa1cciqn8ysxr2gl57b3hwdfm2l8ydbwl50jbll7h6ldmpp6h")))

(define-public crate-annatar-0.4.4 (c (n "annatar") (v "0.4.4") (d (list (d (n "artano") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("blocking"))) (d #t) (k 0)))) (h "0rp3y9wgvwcml3bqnxrxj401ld5jh2ziw45yw8sx88kaccja33jp")))

(define-public crate-annatar-0.5.0 (c (n "annatar") (v "0.5.0") (d (list (d (n "artano") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1lhwalycmkwvm41mil43180j95a6dc6ynambjasw42gi62xn3zwa")))

(define-public crate-annatar-0.5.1 (c (n "annatar") (v "0.5.1") (d (list (d (n "artano") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "10q02rn7z3zdzrbdrk68gw56xkbsm3xnvlcmzx6533y6vz3jl84n")))

(define-public crate-annatar-0.5.2 (c (n "annatar") (v "0.5.2") (d (list (d (n "artano") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1lbln3k082iscczag8iwxhjwrbbsaihsyingd05bhy4vqxvxpf26")))

(define-public crate-annatar-0.5.4 (c (n "annatar") (v "0.5.4") (d (list (d (n "artano") (r "^0.3") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "font-kit") (r "^0.6") (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("blocking"))) (d #t) (k 0)))) (h "1zwbnjsq461vabgr435zlgws4lm4707f1g5yk3dnhqkgqal7bav1")))

(define-public crate-annatar-0.5.5 (c (n "annatar") (v "0.5.5") (d (list (d (n "artano") (r "^0.3") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "font-kit") (r "^0.6") (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("blocking"))) (d #t) (k 0)))) (h "1hs2xy1hfz9dj0wspd82lmm1q7p1yyqjpk5dj7kcqngy2mmf1pxh")))

(define-public crate-annatar-0.5.6 (c (n "annatar") (v "0.5.6") (d (list (d (n "artano") (r "^0.3") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "font-kit") (r "^0.6") (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("blocking"))) (d #t) (k 0)))) (h "1lx5va4vj9rcwy0s446qyilsjcyayg8h89c161n4vprn0ly4ilkp")))

(define-public crate-annatar-0.5.8 (c (n "annatar") (v "0.5.8") (d (list (d (n "artano") (r "^0.3.5") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "font-kit") (r "^0.10.1") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.6") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "textwrap") (r "^0.14.2") (f (quote ("terminal_size"))) (d #t) (k 0)))) (h "111rbmphbrvxbxm03bzk975sr241wkz3afiv5w4xylk84bf4kadp")))

(define-public crate-annatar-0.6.1 (c (n "annatar") (v "0.6.1") (d (list (d (n "artano") (r "^0.3.7") (d #t) (k 0)) (d (n "clap") (r "^3.2.17") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "font-kit") (r "^0.11.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "textwrap") (r "^0.15.0") (f (quote ("terminal_size"))) (d #t) (k 0)))) (h "02b29nwhz9bzqlnb63zzycwjfx84ihvghzzfablq78nk02fmqvfw")))

(define-public crate-annatar-0.6.2 (c (n "annatar") (v "0.6.2") (d (list (d (n "artano") (r "^0.3.8") (d #t) (k 0)) (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "font-kit") (r "^0.11.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.13") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "textwrap") (r "^0.16.0") (f (quote ("terminal_size"))) (d #t) (k 0)))) (h "0kn70yp5b9ym4scxy8g1isgbpl9qz76sbxi5sbz4xkbrl6z3b3qp")))

(define-public crate-annatar-0.6.3 (c (n "annatar") (v "0.6.3") (d (list (d (n "artano") (r "^0.3.8") (d #t) (k 0)) (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "font-kit") (r "^0.11.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.14") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "textwrap") (r "^0.16.0") (f (quote ("terminal_size"))) (d #t) (k 0)))) (h "1g4cilijk83gib5lmmm4na9dvq1ac63qf933k1hqm2dv0cqlc2mw")))

(define-public crate-annatar-0.6.4 (c (n "annatar") (v "0.6.4") (d (list (d (n "artano") (r "^0.3.8") (d #t) (k 0)) (d (n "clap") (r "^4.1.13") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "font-kit") (r "^0.11.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.15") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "textwrap") (r "^0.16.0") (f (quote ("terminal_size"))) (d #t) (k 0)))) (h "0m8vdyi65pm26z1a66rwm2zq4cg5k4zsr2qm3ddz4aar3npzc31i")))

(define-public crate-annatar-0.6.5 (c (n "annatar") (v "0.6.5") (d (list (d (n "artano") (r "^0.3.9") (d #t) (k 0)) (d (n "clap") (r "^4.2.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "font-kit") (r "^0.11.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.16") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "textwrap") (r "^0.16.0") (f (quote ("terminal_size"))) (d #t) (k 0)))) (h "0ybfzbxgls1v09q3si2r39h7jhg6jmanpb21yxpqyqkz5d0kvx09")))

(define-public crate-annatar-0.6.6 (c (n "annatar") (v "0.6.6") (d (list (d (n "artano") (r "^0.3.9") (d #t) (k 0)) (d (n "clap") (r "^4.2.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "font-kit") (r "^0.11.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.16") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "textwrap") (r "^0.16.0") (f (quote ("terminal_size"))) (d #t) (k 0)))) (h "1zi7l3y7jq2l4xcjxcgqkkw1m03i47ncp7xz9rvvm2vw6kj5x86i")))

(define-public crate-annatar-0.6.7 (c (n "annatar") (v "0.6.7") (d (list (d (n "artano") (r "^0.3.9") (d #t) (k 0)) (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "font-kit") (r "^0.11.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.22") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "textwrap") (r "^0.16.0") (f (quote ("terminal_size"))) (d #t) (k 0)))) (h "1ay537jby1qrvaisv8g4vl36qr0x092gn8wybdr0gdgw6qi4528d")))

(define-public crate-annatar-0.6.8 (c (n "annatar") (v "0.6.8") (d (list (d (n "artano") (r "^0.3.10") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "font-kit") (r "^0.13.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.12.2") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "textwrap") (r "^0.16.1") (f (quote ("terminal_size"))) (d #t) (k 0)))) (h "18mdsbhlmwdv0sn06mlw6vh54wg0jqa43gnqlq47p8nhd21n1bvv")))

