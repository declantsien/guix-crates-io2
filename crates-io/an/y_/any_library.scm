(define-module (crates-io an y_ any_library) #:use-module (crates-io))

(define-public crate-any_library-0.1.0 (c (n "any_library") (v "0.1.0") (h "1ywbv81ccni2m0yc6h5vz4yka7s1yi0fzdw75n1l2plqd3i2g0hr") (r "1.60")))

(define-public crate-any_library-0.1.1 (c (n "any_library") (v "0.1.1") (h "0lx3bs0sfjpgyl9s9ik9sxi7vwwdjs9fbfwqb9yr8372qpvr61jp") (r "1.60")))

(define-public crate-any_library-0.1.2 (c (n "any_library") (v "0.1.2") (h "00dphdx32av5aga8qdwli9xjwv8v02l0bbfj79igb1qh9fa1kd5s") (r "1.60")))

(define-public crate-any_library-0.1.3 (c (n "any_library") (v "0.1.3") (h "1ims818zlfr01skmh7hi6svv4ivr77am72qrkay6nx5cniadicvl") (r "1.60")))

(define-public crate-any_library-0.1.4 (c (n "any_library") (v "0.1.4") (h "082gbbgdyby0jqznds49y3b7yphky9xpkrnj9hjw39z956prn4cr") (r "1.60")))

(define-public crate-any_library-0.1.5 (c (n "any_library") (v "0.1.5") (h "1zznphv86nxfq8wv42h0mqg96ay0smdld777aj2isppr1s2vg4yj") (r "1.60")))

(define-public crate-any_library-0.1.6 (c (n "any_library") (v "0.1.6") (h "0kkr8cglv51cw0w8bqnmmk4fs23sj3vwq2irckizf1yl8pym4hmc") (r "1.60")))

(define-public crate-any_library-0.1.7 (c (n "any_library") (v "0.1.7") (h "06j62hj36m0pax9wbfk50z8990sdxrz6q3a0s09cazmxw98ix6ga") (r "1.60")))

