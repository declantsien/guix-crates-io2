(define-module (crates-io an y_ any_handle) #:use-module (crates-io))

(define-public crate-any_handle-0.1.0 (c (n "any_handle") (v "0.1.0") (h "0hhx92vid9v32kc00pqbhir91sm0vsc3yhk34blqhkks4pcnj248")))

(define-public crate-any_handle-0.1.1 (c (n "any_handle") (v "0.1.1") (h "0li2b9f5dds0d0amv04dibsdjpyn6asf62x1m08xxk1d04mpnnki")))

(define-public crate-any_handle-0.1.2 (c (n "any_handle") (v "0.1.2") (h "1kgq592w3rzid3q1x8wjfhm3jyl1x8p4n8bw6xc2b8yjsnij85mx")))

(define-public crate-any_handle-0.1.3 (c (n "any_handle") (v "0.1.3") (h "1dsm9x4l4qxfvbs6m58ii8za1xv80nxd041q12p3mw653knixqjz")))

(define-public crate-any_handle-0.1.4 (c (n "any_handle") (v "0.1.4") (h "0al4518sir3ilff1ydfvx2nmj81rafjnp9gfj5p3xbk3x2y5957d")))

