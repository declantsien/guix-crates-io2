(define-module (crates-io an y_ any_key) #:use-module (crates-io))

(define-public crate-any_key-0.1.0 (c (n "any_key") (v "0.1.0") (d (list (d (n "debugit") (r "^0.1.2") (d #t) (k 0)) (d (n "mopa") (r "^0.2.2") (d #t) (k 0)))) (h "04xqpi505a25qpqvgv4fqdm6ndjb6cw5z1zii9w8q73mj34lv6f1")))

(define-public crate-any_key-0.1.1 (c (n "any_key") (v "0.1.1") (d (list (d (n "debugit") (r "^0.1.2") (d #t) (k 0)) (d (n "mopa") (r "^0.2.2") (d #t) (k 0)))) (h "0lml7jd3kw09sss5bhj7z0fxxgldc2fdj7a1d6fyv1w0mg6v46yj")))

