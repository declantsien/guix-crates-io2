(define-module (crates-io an y_ any_ref_macro) #:use-module (crates-io))

(define-public crate-any_ref_macro-0.1.0 (c (n "any_ref_macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1xadw7pc7pnr0428hpg2f9d8iv9gig3p9fm4x8s08ljsxcmxxpdl") (y #t)))

(define-public crate-any_ref_macro-0.1.1 (c (n "any_ref_macro") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0fmircx20dnibz8k8als72dbsjc7gzkcbfpgs47xmxfd3kq2aqma") (y #t)))

(define-public crate-any_ref_macro-0.1.2 (c (n "any_ref_macro") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "13i98wdivl676g2dfw48mxzcw0800yipks7b68ghxlygws5h6n82") (y #t)))

(define-public crate-any_ref_macro-0.1.3 (c (n "any_ref_macro") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1z4p2v7m419sl95dchqdznb5mml86az6mizv4wv5vq96nc2mrqf5")))

(define-public crate-any_ref_macro-1.0.0 (c (n "any_ref_macro") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "09fan0wll2gxvyc9b0gjk4iv1303kg4sjyzwk8nqnj1pvzxb0vs0")))

(define-public crate-any_ref_macro-1.0.1 (c (n "any_ref_macro") (v "1.0.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0idxwv0yq4vpj3ahdp0gkk1s35l85835phgjgibjxwmg7n1ndqll")))

