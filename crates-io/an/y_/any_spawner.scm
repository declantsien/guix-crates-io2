(define-module (crates-io an y_ any_spawner) #:use-module (crates-io))

(define-public crate-any_spawner-0.1.0 (c (n "any_spawner") (v "0.1.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "glib") (r "^0.19") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt"))) (o #t) (k 0)) (d (n "tracing") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "wasm-bindgen-futures") (r "^0.4") (o #t) (d #t) (k 0)))) (h "178w1n58vq9f8fhshxkxpsq8j2zxqjjs4wkp3hlm1a19f6mwq3sp") (f (quote (("futures-executor" "futures/thread-pool" "futures/executor")))) (s 2) (e (quote (("wasm-bindgen" "dep:wasm-bindgen-futures") ("tracing" "dep:tracing") ("tokio" "dep:tokio") ("glib" "dep:glib"))))))

