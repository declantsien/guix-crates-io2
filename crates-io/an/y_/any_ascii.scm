(define-module (crates-io an y_ any_ascii) #:use-module (crates-io))

(define-public crate-any_ascii-0.1.2 (c (n "any_ascii") (v "0.1.2") (h "0p50nqkxm2c9sc6s7n1j87chn45dw1jcar0bwgp8sdsp2c40v9sq")))

(define-public crate-any_ascii-0.1.3 (c (n "any_ascii") (v "0.1.3") (h "0ggy1clp3lmcqz4i4lry7sdaxcg3l9y2lify4al05c22i67mn45n")))

(define-public crate-any_ascii-0.1.4 (c (n "any_ascii") (v "0.1.4") (h "0njpsjz7qfn9gzp4pxqmhlxjpsjb822hbxjdli9wmpflrrizngdz")))

(define-public crate-any_ascii-0.1.5 (c (n "any_ascii") (v "0.1.5") (h "0f8zwwwvrqyg4djj9z9vzzfd071wkbb81i02j1p81sfzm84c83wh")))

(define-public crate-any_ascii-0.1.6 (c (n "any_ascii") (v "0.1.6") (h "1ff2r5v6wibd6m6fs40knmb46lh9y3a0s2wrlz110j3hxjmwhpin")))

(define-public crate-any_ascii-0.1.7 (c (n "any_ascii") (v "0.1.7") (h "07kyb9is518jr1sbc6804kgg5pnx8djl328q3al28lcbxdvkf0vh")))

(define-public crate-any_ascii-0.2.0 (c (n "any_ascii") (v "0.2.0") (h "0w447dbd76vp37zqha3qbnpgr7310lj94v0zcdp4ibambwkv3ka9")))

(define-public crate-any_ascii-0.3.0 (c (n "any_ascii") (v "0.3.0") (h "05fnnmiar25z42i0w6x8w6kasih64a4xw3yhxy4zac3lpv5ymx1g")))

(define-public crate-any_ascii-0.3.1 (c (n "any_ascii") (v "0.3.1") (h "127njhkxkdjc9cwg6zcbp91nmj8y8ybk9h96pp8wrisr94ggdhmi")))

(define-public crate-any_ascii-0.3.2 (c (n "any_ascii") (v "0.3.2") (h "0b6c2vb3wlzcsd7s5l6s9fyq4r07a8yab9r7qvw474sbg95v2l7a")))

