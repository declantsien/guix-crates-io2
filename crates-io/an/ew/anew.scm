(define-module (crates-io an ew anew) #:use-module (crates-io))

(define-public crate-anew-0.1.0 (c (n "anew") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "indexmap") (r "^2.1.0") (d #t) (k 0)))) (h "17p755xpg6fwh7475cp2pf3fdkb88paa5jm4nm2i2qj76kqxbknw")))

(define-public crate-anew-0.1.2 (c (n "anew") (v "0.1.2") (d (list (d (n "clap") (r "^4.4.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "indexmap") (r "^2.1.0") (d #t) (k 0)))) (h "03i5dbxkr93hs0sa2nc5v229rxp2w3wjj2d6f2mbnk0p758qglj9")))

