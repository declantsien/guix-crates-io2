(define-module (crates-io an ew anewer) #:use-module (crates-io))

(define-public crate-anewer-0.1.2 (c (n "anewer") (v "0.1.2") (d (list (d (n "ahash") (r "^0.5.6") (d #t) (k 0)) (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "memchr") (r "^2") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0snl8k3xp872hdzq7s7pdr4pqgcrpqys0lr3bvfyvh6f736m1h68")))

(define-public crate-anewer-0.1.4 (c (n "anewer") (v "0.1.4") (d (list (d (n "ahash") (r "^0.5.6") (d #t) (k 0)) (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "memchr") (r "^2") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "10nzqz15kjy367im8lsmz7y81arm47q9s2r0cp2c611cdm8dlrqi") (y #t)))

(define-public crate-anewer-0.1.5 (c (n "anewer") (v "0.1.5") (d (list (d (n "ahash") (r "^0.5.6") (d #t) (k 0)) (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "memchr") (r "^2") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1h9xmqamrhrmf48yq8a6k8wq44y9zmqlasc1ndx17jfyd5hrwj5i")))

(define-public crate-anewer-0.1.6 (c (n "anewer") (v "0.1.6") (d (list (d (n "ahash") (r "^0.7") (d #t) (k 0)) (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "memchr") (r "^2") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "15ci4mhzdcbng9iq2v6q4qaf31fkcw5vapis24jkfafilifj5ml0")))

