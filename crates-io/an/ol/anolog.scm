(define-module (crates-io an ol anolog) #:use-module (crates-io))

(define-public crate-anolog-0.1.0 (c (n "anolog") (v "0.1.0") (d (list (d (n "lazy-regex") (r "^0.1.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 0)))) (h "11lwlgci4nmqbj9sr83n3d6d2ahgz1nci22zc5cz76pmqbajgxan")))

(define-public crate-anolog-0.1.1 (c (n "anolog") (v "0.1.1") (d (list (d (n "lazy-regex") (r "^0.1.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 0)))) (h "1f86h2zqpyi4cl6rgxic3bc0cjqhv6jnckjnvj8gm3lm6i3bkins")))

(define-public crate-anolog-0.1.2 (c (n "anolog") (v "0.1.2") (d (list (d (n "lazy-regex") (r "^0.1.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 0)))) (h "1m1by1px4k5qjccd7ly973vzdqka7w2k83v9cb8w7dwdmdcy40y5")))

(define-public crate-anolog-0.1.4 (c (n "anolog") (v "0.1.4") (d (list (d (n "lazy-regex") (r "^0.1.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 0)))) (h "1gs8nryvsdkiy160glf5hwij01vmi79gss5hpvqy4w6cw7z2gwps")))

(define-public crate-anolog-0.1.5 (c (n "anolog") (v "0.1.5") (d (list (d (n "fnv") (r "^1.0.7") (d #t) (k 0)) (d (n "lazy-regex") (r "^1.1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 0)))) (h "1v5xs5iasn64dnd85ra0g0iyvpid1ja2y34k47p2927rq7g5lyv0")))

