(define-module (crates-io an ub anubis) #:use-module (crates-io))

(define-public crate-anubis-0.0.1 (c (n "anubis") (v "0.0.1") (h "1ji12918s0ijdv1jpp2az871hrksllnr8mv4c0ah6y2495cfm7yi")))

(define-public crate-anubis-0.0.2 (c (n "anubis") (v "0.0.2") (d (list (d (n "gilrs") (r "^0.10.4") (d #t) (k 0)) (d (n "slint") (r "^1.3.2") (d #t) (k 0)) (d (n "slint-build") (r "^1.3.2") (d #t) (k 1)))) (h "07h2xhw8rvpq16s2pqr8z1nnc4sdkd7ihri59j07l0i9kbi35y2n")))

