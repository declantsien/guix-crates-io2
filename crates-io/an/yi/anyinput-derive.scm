(define-module (crates-io an yi anyinput-derive) #:use-module (crates-io))

(define-public crate-anyinput-derive-0.1.0 (c (n "anyinput-derive") (v "0.1.0") (d (list (d (n "anyinput-core") (r "^0.1.0") (d #t) (k 0)))) (h "0l8b5my2kl9666mnfbl3i33bklp7nf76y92xhbc17jvxzxir6la1") (f (quote (("ndarray" "anyinput-core/ndarray"))))))

(define-public crate-anyinput-derive-0.1.2 (c (n "anyinput-derive") (v "0.1.2") (d (list (d (n "anyinput-core") (r "^0.1.2") (d #t) (k 0)))) (h "1b3lf2c1lyr3lxjn04lp96qczpdns21732063vgc1sjggbh268l6") (f (quote (("ndarray" "anyinput-core/ndarray"))))))

(define-public crate-anyinput-derive-0.1.3 (c (n "anyinput-derive") (v "0.1.3") (d (list (d (n "anyinput-core") (r "^0.1.3") (d #t) (k 0)))) (h "02igsbcp44cfqj8kgk81ry7088all81gqsi85jbnanyi1rwn0jwx") (f (quote (("ndarray" "anyinput-core/ndarray"))))))

(define-public crate-anyinput-derive-0.1.4 (c (n "anyinput-derive") (v "0.1.4") (d (list (d (n "anyinput-core") (r "^0.1.4") (d #t) (k 0)))) (h "08nrl958hjrbiw0crc4xh6bgznzqjhh00h5s9ir6vm71mkf4pblf") (f (quote (("ndarray" "anyinput-core/ndarray"))))))

(define-public crate-anyinput-derive-0.1.5 (c (n "anyinput-derive") (v "0.1.5") (d (list (d (n "anyinput-core") (r "^0.1.5") (d #t) (k 0)))) (h "17acc2jzr06nbqi696pf6276zw2mjln2mjvln1r8rgp2gy25ahkn") (f (quote (("ndarray" "anyinput-core/ndarray"))))))

(define-public crate-anyinput-derive-0.1.6 (c (n "anyinput-derive") (v "0.1.6") (d (list (d (n "anyinput-core") (r "^0.1.6") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)))) (h "1f85nnzvgh4i2jcn5r7qx373br05ayhygmsa8qm4ydmq8x6xpg1j") (f (quote (("ndarray" "anyinput-core/ndarray"))))))

