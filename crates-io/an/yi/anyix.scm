(define-module (crates-io an yi anyix) #:use-module (crates-io))

(define-public crate-anyix-0.1.0 (c (n "anyix") (v "0.1.0") (d (list (d (n "solana-program") (r ">=1.9") (d #t) (k 0)) (d (n "spl-token") (r ">=1.0") (d #t) (k 2)))) (h "07srd05ggy6zjpscl5nfmxwnadp0gd6mzgamhxybwv5pjzccgnwk")))

(define-public crate-anyix-0.1.1 (c (n "anyix") (v "0.1.1") (d (list (d (n "solana-program") (r ">=1.9") (d #t) (k 0)) (d (n "spl-token") (r ">=1.0") (d #t) (k 2)))) (h "10mp6xpa09k3v44lblsphfliy6ls3icbn9faif7hx6xpb20k89ln")))

(define-public crate-anyix-0.1.2 (c (n "anyix") (v "0.1.2") (d (list (d (n "solana-program") (r ">=1.9") (d #t) (k 0)) (d (n "spl-token") (r ">=1.0") (d #t) (k 2)))) (h "0prfb9xvw5sbk9lzkivqj911j2hm1b8vqg3ikkfpkcy45c4xs4nl")))

(define-public crate-anyix-0.1.3 (c (n "anyix") (v "0.1.3") (d (list (d (n "solana-program") (r ">=1.9") (d #t) (k 0)) (d (n "spl-token") (r ">=1.0") (d #t) (k 2)))) (h "095i8spjmdjn56yx40fcka9bjw9sfk48pir6s9n720qdhj0z5vkp")))

(define-public crate-anyix-0.1.4 (c (n "anyix") (v "0.1.4") (d (list (d (n "solana-program") (r ">=1.9") (d #t) (k 0)) (d (n "spl-token") (r ">=1.0") (d #t) (k 2)))) (h "11qwf4q49hvd1jlvv7zpy9fmnd00lp6a1lrd4ngxzkf9419l45y5")))

(define-public crate-anyix-0.1.5 (c (n "anyix") (v "0.1.5") (d (list (d (n "solana-program") (r ">=1.9") (d #t) (k 0)) (d (n "spl-token") (r ">=1.0") (d #t) (k 2)))) (h "187xkhgnqkkf1d8b0x0pv70krmv68xabzc9xq7dxrqw6bblxgcid")))

(define-public crate-anyix-0.1.6 (c (n "anyix") (v "0.1.6") (d (list (d (n "solana-program") (r ">=1.9") (d #t) (k 0)) (d (n "spl-token") (r ">=1.0") (d #t) (k 2)))) (h "1k51a7apr2vifjwkndahnxhknp7mbzhpya0p4hbwzy53hs3gzjfm")))

(define-public crate-anyix-0.1.7 (c (n "anyix") (v "0.1.7") (d (list (d (n "solana-program") (r ">=1.9") (d #t) (k 0)) (d (n "spl-token") (r ">=1.0") (d #t) (k 2)))) (h "1gsn7i1pbmym0r6njmq9nq23wz9bnlfnq3ybw73h5xrm2hm2zlwm")))

(define-public crate-anyix-0.1.8 (c (n "anyix") (v "0.1.8") (d (list (d (n "solana-program") (r ">=1.9") (d #t) (k 0)) (d (n "spl-token") (r ">=1.0") (d #t) (k 2)))) (h "09g0553fqy9a53gcrz0i9l2qvg9104xwjahqqwg2v2sxhinpb4pq")))

(define-public crate-anyix-0.1.9 (c (n "anyix") (v "0.1.9") (d (list (d (n "solana-program") (r ">=1.9") (d #t) (k 0)) (d (n "spl-token") (r ">=1.0") (d #t) (k 2)))) (h "085s2g2dvxxyl24856q7hrm16ggznfi9dfz34bg6cclycj3admcn")))

(define-public crate-anyix-0.1.10 (c (n "anyix") (v "0.1.10") (d (list (d (n "solana-program") (r ">=1.9") (d #t) (k 0)) (d (n "spl-token") (r ">=1.0") (d #t) (k 2)))) (h "09zz07yqg4vii00zacy7k7vdp6kh218k87zb7c8lr21sqca3bnk1")))

(define-public crate-anyix-0.1.11 (c (n "anyix") (v "0.1.11") (d (list (d (n "solana-program") (r ">=1.9") (d #t) (k 0)) (d (n "spl-token") (r ">=1.0") (d #t) (k 2)))) (h "0b1fh5b4345srmhiqfk26n7wdwhdx00fmwavm3918j164qmpfn07")))

(define-public crate-anyix-0.1.12 (c (n "anyix") (v "0.1.12") (d (list (d (n "solana-program") (r ">=1.9") (d #t) (k 0)) (d (n "spl-token") (r ">=1.0") (d #t) (k 2)))) (h "1j63kgc348np8kfbb0hj78a68jjn2i4g8szn926vinfvrwf3767v")))

(define-public crate-anyix-0.1.13 (c (n "anyix") (v "0.1.13") (d (list (d (n "solana-program") (r ">=1.9") (d #t) (k 0)) (d (n "spl-token") (r ">=1.0") (d #t) (k 2)))) (h "1ddl0ngd965rq4dk9r4sz746j9jvwyh9kp3779rrf6jlvr96paxj")))

