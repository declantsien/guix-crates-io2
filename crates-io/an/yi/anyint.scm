(define-module (crates-io an yi anyint) #:use-module (crates-io))

(define-public crate-anyint-0.1.0 (c (n "anyint") (v "0.1.0") (d (list (d (n "anyint_macros") (r "^0.1") (d #t) (k 0)) (d (n "displaydoc") (r "^0.2") (o #t) (k 0)) (d (n "num-traits") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1ix3r4ihjw84apjhavymsk4f2fc6j90xnyr2pj6gaf32zrzlvdll") (f (quote (("num_traits" "num-traits") ("default" "std" "displaydoc")))) (s 2) (e (quote (("std" "thiserror" "displaydoc?/std"))))))

