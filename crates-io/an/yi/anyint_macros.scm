(define-module (crates-io an yi anyint_macros) #:use-module (crates-io))

(define-public crate-anyint_macros-0.1.0 (c (n "anyint_macros") (v "0.1.0") (d (list (d (n "proc-macro-crate") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "10ipxngbsrv6diq82i4gi3pypss2z9f509j8apji4zipvf0z3zcd")))

