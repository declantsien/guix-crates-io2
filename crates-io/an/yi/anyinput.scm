(define-module (crates-io an yi anyinput) #:use-module (crates-io))

(define-public crate-anyinput-0.1.0 (c (n "anyinput") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "anyinput-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (d #t) (k 2)))) (h "1lmxb1bag7hpgbjs63m7isclq3fkv6hzdqsrmpybznp7bn4jmv72") (f (quote (("ndarray" "anyinput-derive/ndarray"))))))

(define-public crate-anyinput-0.1.1 (c (n "anyinput") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "anyinput-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (d #t) (k 2)))) (h "0c70xwmgapws8sj4n5m28y7vm2dmsirxwawrzjcq93aswhx6rs28") (f (quote (("ndarray" "anyinput-derive/ndarray"))))))

(define-public crate-anyinput-0.1.2 (c (n "anyinput") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "anyinput-derive") (r "^0.1.2") (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (d #t) (k 2)))) (h "07lghnys92yid0nsvh4s1zvb9c9wwillj7zpcjnyg7f4wwz085p8") (f (quote (("ndarray" "anyinput-derive/ndarray"))))))

(define-public crate-anyinput-0.1.3 (c (n "anyinput") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "anyinput-derive") (r "^0.1.3") (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (d #t) (k 2)))) (h "011sq9qqn6z01zcflm5hnw5ibcb5swkz1y9pl78yipnn0rc3h1c3") (f (quote (("ndarray" "anyinput-derive/ndarray"))))))

(define-public crate-anyinput-0.1.4 (c (n "anyinput") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "anyinput-derive") (r "^0.1.4") (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (d #t) (k 2)))) (h "18661gmprblj5q81xb9nah4v2b9zf018s8fkcjhr7n1vpghz253s") (f (quote (("ndarray" "anyinput-derive/ndarray"))))))

(define-public crate-anyinput-0.1.5 (c (n "anyinput") (v "0.1.5") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "anyinput-derive") (r "^0.1.5") (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (d #t) (k 2)))) (h "0bzj7pc0920b8k8q93n653lz4yv5j5zhxwzyrmayms9lzf6x5bkb") (f (quote (("ndarray" "anyinput-derive/ndarray"))))))

(define-public crate-anyinput-0.1.6 (c (n "anyinput") (v "0.1.6") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "anyinput-derive") (r "^0.1.6") (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1z8552gjk8qp10y4q83vwb2zdhgb1x69ksvsmw1xx9573shbcpf8") (f (quote (("ndarray" "anyinput-derive/ndarray"))))))

