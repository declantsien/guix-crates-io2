(define-module (crates-io an yi anyid) #:use-module (crates-io))

(define-public crate-anyid-0.1.0 (c (n "anyid") (v "0.1.0") (d (list (d (n "dyn-hash") (r "^0.2.0") (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)) (d (n "uuid") (r "^1") (f (quote ("v4"))) (d #t) (k 2)))) (h "12h62rd6454b14s1191s5qvxc3iq49643ql1qak3w6jxc4f177vr")))

(define-public crate-anyid-0.1.1 (c (n "anyid") (v "0.1.1") (d (list (d (n "dyn-hash") (r "^0.2.0") (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)) (d (n "uuid") (r "^1") (f (quote ("v4"))) (d #t) (k 2)))) (h "08rknbdwwpy0nhxflnzqcrnk5a8yhgr5hkn5kn9lai5j9lkchwvm")))

