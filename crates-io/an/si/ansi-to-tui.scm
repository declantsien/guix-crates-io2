(define-module (crates-io an si ansi-to-tui) #:use-module (crates-io))

(define-public crate-ansi-to-tui-0.1.6 (c (n "ansi-to-tui") (v "0.1.6") (d (list (d (n "simdutf8") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "tui") (r "^0.14.0") (k 0)))) (h "18d26cm8xcv1p8q3d1pw43hs87n9b8jlr45wk25rgdpifj1jxs3m") (f (quote (("simd" "simdutf8"))))))

(define-public crate-ansi-to-tui-0.1.7 (c (n "ansi-to-tui") (v "0.1.7") (d (list (d (n "simdutf8") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "tui") (r "^0.14.0") (k 0)))) (h "0k674wp6dv0m2gzs4v3kbikxpsn8m7hzs77zvdh3gxsvgc781vpk") (f (quote (("simd" "simdutf8")))) (y #t)))

(define-public crate-ansi-to-tui-0.1.8 (c (n "ansi-to-tui") (v "0.1.8") (d (list (d (n "simdutf8") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "tui") (r "^0.14.0") (k 0)))) (h "0gjfi0z4j7y0ckv054hnr2df3gxas3xq9mfs4jfw56pz7k2l3rk8") (f (quote (("simd" "simdutf8"))))))

(define-public crate-ansi-to-tui-0.1.9 (c (n "ansi-to-tui") (v "0.1.9") (d (list (d (n "simdutf8") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "tui") (r "^0.14.0") (k 0)))) (h "1wpw2b2sxa9nzcag31dqa8ijfigwx8lz3kp1668am3dzq8cbk4ah") (f (quote (("simd" "simdutf8"))))))

(define-public crate-ansi-to-tui-0.2.0 (c (n "ansi-to-tui") (v "0.2.0") (d (list (d (n "simdutf8") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "tui") (r "^0.14.0") (k 0)))) (h "158vszadyjdcq5f38swf09dkqs9k4d4jx3x5idbmjlmpdxv1srxw") (f (quote (("simd" "simdutf8"))))))

(define-public crate-ansi-to-tui-0.2.1 (c (n "ansi-to-tui") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 2)) (d (n "simdutf8") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "tui") (r "^0.14.0") (k 0)))) (h "0pb41r7ylhiw61bs045xk2agi145vkbxhjy96s0x8rd18finfpn7") (f (quote (("simd" "simdutf8"))))))

(define-public crate-ansi-to-tui-0.2.2 (c (n "ansi-to-tui") (v "0.2.2") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 2)) (d (n "simdutf8") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "tui") (r "^0.15.0") (k 0)))) (h "13wcnmqjvpvddzpxwxygxlc4srhpl9fhfsr634zh237x1nsqfp7y") (f (quote (("simd" "simdutf8")))) (y #t)))

(define-public crate-ansi-to-tui-0.3.0 (c (n "ansi-to-tui") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 2)) (d (n "simdutf8") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "tui") (r "^0.15.0") (k 0)))) (h "1gwdsjgbgd04yknb7c9ynwq4bdazilcs05hmf1cyak107vhqls3q") (f (quote (("simd" "simdutf8"))))))

(define-public crate-ansi-to-tui-0.4.0 (c (n "ansi-to-tui") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 2)) (d (n "simdutf8") (r "^0.1.3") (o #t) (d #t) (k 0)) (d (n "tui") (r "^0.16.0") (k 0)))) (h "19n41d7kh23c7xqf1m9w1b7c75mrg18f7zcwhhwh8aanfbzqmvi7") (f (quote (("simd" "simdutf8"))))))

(define-public crate-ansi-to-tui-0.3.1 (c (n "ansi-to-tui") (v "0.3.1") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 2)) (d (n "simdutf8") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "tui") (r "^0.15.0") (k 0)))) (h "0cwabbl7kl3h17xjnyabr563s3bxw7w5mvs1ms1cmk8mzjsd417b") (f (quote (("simd" "simdutf8"))))))

(define-public crate-ansi-to-tui-0.4.1 (c (n "ansi-to-tui") (v "0.4.1") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 2)) (d (n "simdutf8") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "tui") (r "^0.16.0") (k 0)))) (h "1pvbdq5g72swqhpgaxzbw7lplgwss226ryf9nr6486wxgisbagsc") (f (quote (("simd" "simdutf8"))))))

(define-public crate-ansi-to-tui-0.5.0 (c (n "ansi-to-tui") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 2)) (d (n "simdutf8") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "tui") (r "^0.17.0") (k 0)))) (h "1788pmb0nynfdhjhfl5g6406107mkbvdn06j0ixv7y5ka974as1p") (f (quote (("simd" "simdutf8"))))))

(define-public crate-ansi-to-tui-0.5.1 (c (n "ansi-to-tui") (v "0.5.1") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 2)) (d (n "simdutf8") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "tui") (r "^0.17.0") (k 0)))) (h "168avk0x6b90hs5mm9fsy3r98dc4dlyb8vr9l8mkni76hmmasas3") (f (quote (("simd" "simdutf8"))))))

(define-public crate-ansi-to-tui-0.6.0 (c (n "ansi-to-tui") (v "0.6.0") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.2.1") (d #t) (k 2)) (d (n "simdutf8") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "tui") (r "0.*") (k 0)))) (h "0876lbsnkhw9pk6x5wqgna0kpyz6mnh57wd4dcb2dx1fnfvibvrk") (f (quote (("simd" "simdutf8"))))))

(define-public crate-ansi-to-tui-1.0.1 (c (n "ansi-to-tui") (v "1.0.1") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "nom") (r "^7.1.1") (d #t) (k 0)) (d (n "simdutf8") (r "^0.1.4") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)) (d (n "tui") (r "^0.18.0") (k 0)))) (h "08kg6dlwd6m49mqi38pbiny5rg515hsaalna1pw8jm3w4z5vs4qj") (f (quote (("simd" "simdutf8"))))))

(define-public crate-ansi-to-tui-2.0.0 (c (n "ansi-to-tui") (v "2.0.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "nom") (r "^7.1.1") (d #t) (k 0)) (d (n "simdutf8") (r "^0.1.4") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)) (d (n "tui") (r "0.*") (k 0)))) (h "1sa7dcrpiq1wgzcdfdyvxw04ylfdih1xlcsrll4jq6cbmyzdfq1l") (f (quote (("simd" "simdutf8"))))))

(define-public crate-ansi-to-tui-2.0.1 (c (n "ansi-to-tui") (v "2.0.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "nom") (r "^7.1") (d #t) (k 0)) (d (n "simdutf8") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tui") (r "0.*") (k 0)))) (h "00ixy57q4rl7qppm858yssbyql6ywyzwv1glqv5g7hhnigy2nhhr") (f (quote (("simd" "simdutf8"))))))

(define-public crate-ansi-to-tui-2.0.2 (c (n "ansi-to-tui") (v "2.0.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "nom") (r "^7.1") (d #t) (k 0)) (d (n "simdutf8") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tui") (r "0.*") (k 0)))) (h "0l43nyj2difngwjbiy6vd5p8bw96w06swgw5hx6vi9zvqzs8wyqm") (f (quote (("simd" "simdutf8"))))))

(define-public crate-ansi-to-tui-3.0.0 (c (n "ansi-to-tui") (v "3.0.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "nom") (r "^7.1") (d #t) (k 0)) (d (n "simdutf8") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tui") (r "0.*") (k 0) (p "ratatui")))) (h "1qhyzgvcarba9y40qhjafx8s359y2cpfi0qsbcigp66di5liagaz") (f (quote (("simd" "simdutf8"))))))

(define-public crate-ansi-to-tui-3.1.0 (c (n "ansi-to-tui") (v "3.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "nom") (r "^7.1") (d #t) (k 0)) (d (n "simdutf8") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tui") (r ">=0.21") (k 0) (p "ratatui")))) (h "0mvzkfgcv6q6r99q4583fgp803kwib4appnm8ins0sr5rn6k83hb") (f (quote (("simd" "simdutf8"))))))

(define-public crate-ansi-to-tui-4.0.0 (c (n "ansi-to-tui") (v "4.0.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "nom") (r "^7.1") (d #t) (k 0)) (d (n "simdutf8") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "smallvec") (r "^1.10.0") (f (quote ("const_generics"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tui") (r "0.*, >=0.21") (k 0) (p "ratatui")))) (h "0si27g3yq4289wmmmrqkym4w2wmnkbp411q0iqddxlfhfpjxaf8a") (f (quote (("zero-copy") ("default" "zero-copy" "simd")))) (s 2) (e (quote (("simd" "dep:simdutf8"))))))

(define-public crate-ansi-to-tui-4.0.1 (c (n "ansi-to-tui") (v "4.0.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "nom") (r "^7.1") (d #t) (k 0)) (d (n "simdutf8") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "smallvec") (r "^1.10.0") (f (quote ("const_generics"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tui") (r "0.*, >=0.21") (k 0) (p "ratatui")))) (h "1c1ds8absqd0r5gm1zysjhifr6vxb18m6ppmxycdpk3xgqysyf44") (f (quote (("zero-copy") ("default" "zero-copy" "simd")))) (s 2) (e (quote (("simd" "dep:simdutf8"))))))

