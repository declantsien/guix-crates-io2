(define-module (crates-io an si ansify) #:use-module (crates-io))

(define-public crate-ansify-0.1.0 (c (n "ansify") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "image") (r "^0.23") (d #t) (k 0)) (d (n "kd-tree") (r "^0.5.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "rayon") (r "^1.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)))) (h "18z2bm0ac4jgixxkiaz7g5nwc73zv43ppa08cpqszn6mqamqwvqr") (f (quote (("rayon" "kd-tree/rayon"))))))

