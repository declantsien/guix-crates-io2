(define-module (crates-io an si ansi2svg) #:use-module (crates-io))

(define-public crate-ansi2svg-0.1.1 (c (n "ansi2svg") (v "0.1.1") (d (list (d (n "ansi2") (r "^0.1") (d #t) (k 0)) (d (n "html-escape") (r "^0.2.13") (d #t) (k 0)))) (h "1sy8l2ar3b2pydmqs26srs93pjr197q34315jiggasqkqhhm3xdc")))

(define-public crate-ansi2svg-0.2.0 (c (n "ansi2svg") (v "0.2.0") (d (list (d (n "ansi2") (r "^0.2") (d #t) (k 0)) (d (n "html-escape") (r "^0.2.13") (d #t) (k 0)))) (h "1232phy6qvwvq33il4087ql3fxxigrgnki9jr5bi7pmv9z8a0ahw")))

(define-public crate-ansi2svg-0.2.2 (c (n "ansi2svg") (v "0.2.2") (d (list (d (n "ansi2") (r "^0.2") (d #t) (k 0)) (d (n "html-escape") (r "^0.2.13") (d #t) (k 0)))) (h "1sjchgj6wi1j8cg1h99f023ah4cnhlckczqvvyld6xb4ji1l511p")))

