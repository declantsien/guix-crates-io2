(define-module (crates-io an si ansistr) #:use-module (crates-io))

(define-public crate-ansistr-0.1.0 (c (n "ansistr") (v "0.1.0") (d (list (d (n "regex") (r "^1.3.6") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.7") (d #t) (k 0)))) (h "1h0s1l3lcm4y7pr0nzm0kj7k7m6syayw26kwm2rik1fqgjfdc98v")))

(define-public crate-ansistr-0.1.1 (c (n "ansistr") (v "0.1.1") (d (list (d (n "regex") (r "^1.3.6") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.7") (d #t) (k 0)))) (h "00q9gh4kx8yb4f862qh92cls7zvk15nj7k2hbdz3kgzkymvkw0nn")))

