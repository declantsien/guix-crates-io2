(define-module (crates-io an si ansi_colour) #:use-module (crates-io))

(define-public crate-ansi_colour-0.1.0 (c (n "ansi_colour") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1w0194dh74krjv13mclqlbyz0vgk0hd2ccjwqm0cli3gpzgschnk")))

(define-public crate-ansi_colour-0.2.0 (c (n "ansi_colour") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1glw05x1668jq6k7m0qdj5jyq7wd0wq8k57hr1ny9c0ys0da101h")))

(define-public crate-ansi_colour-0.2.1 (c (n "ansi_colour") (v "0.2.1") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0jhc8in88bi89lcrzzi4zjhxh1pbma31z55ydidgkwfdbxak9bch")))

(define-public crate-ansi_colour-0.3.0 (c (n "ansi_colour") (v "0.3.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "062z32ymmdkdi756crnjav9dlmznn8wzhhlzs4ji9436wh2m22xv")))

(define-public crate-ansi_colour-0.4.0 (c (n "ansi_colour") (v "0.4.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0wl7wga55mi6r4cp65gp35mjlkp5jry5y1y1sfr0niwnwhkrw7yg")))

(define-public crate-ansi_colour-0.5.0 (c (n "ansi_colour") (v "0.5.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "130pmah7ng6f1s3kp899ims3f584p91ybwahfxnrsj3ih6x57jw8")))

(define-public crate-ansi_colour-0.5.1 (c (n "ansi_colour") (v "0.5.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "14s0bk477q8chw44hvj271vamhrhk5jp7bd1lajvkq894vydv4vv") (f (quote (("serialize" "serde"))))))

(define-public crate-ansi_colour-0.5.2 (c (n "ansi_colour") (v "0.5.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "1izhxkg4a6qz8l5lcbva0mhy75irpj2zqxq7mmmxy3x61njwlp5x") (f (quote (("serialize" "serde"))))))

(define-public crate-ansi_colour-0.5.3 (c (n "ansi_colour") (v "0.5.3") (d (list (d (n "rgb24") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "1ss03pkf00bsxip5hbcmf89ngsvil1dldb7ba79myls862p3mj7q") (f (quote (("serialize" "serde" "rgb24/serialize"))))))

