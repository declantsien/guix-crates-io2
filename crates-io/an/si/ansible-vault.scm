(define-module (crates-io an si ansible-vault) #:use-module (crates-io))

(define-public crate-ansible-vault-0.1.0 (c (n "ansible-vault") (v "0.1.0") (d (list (d (n "aes-ctr") (r "^0.3.0") (d #t) (k 0)) (d (n "base16") (r "^0.1.1") (d #t) (k 0)) (d (n "base64") (r "^0.10") (d #t) (k 0)) (d (n "pkcs7") (r "^1.0.0") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)))) (h "0x4jq7slxfam4mhd3v1wl6lqkn295gz245cc41b9yj7gp3qh9d87")))

(define-public crate-ansible-vault-0.1.1 (c (n "ansible-vault") (v "0.1.1") (d (list (d (n "aes-ctr") (r "^0.3.0") (d #t) (k 0)) (d (n "base16") (r "^0.1.1") (d #t) (k 0)) (d (n "hmac") (r "^0.7.0") (d #t) (k 0)) (d (n "pbkdf2") (r "^0.3.0") (d #t) (k 0)) (d (n "pkcs7") (r "^1.0.0") (d #t) (k 0)) (d (n "sha2") (r "^0.8.0") (d #t) (k 0)))) (h "0x25hsnr3kg6nfdi3w3cr65fik59j26cwb33j9qyy02mbsy6w3jd")))

(define-public crate-ansible-vault-0.1.3 (c (n "ansible-vault") (v "0.1.3") (d (list (d (n "aes-ctr") (r "^0.3.0") (d #t) (k 0)) (d (n "base16") (r "^0.1.1") (d #t) (k 0)) (d (n "block-padding") (r "^0.1.0") (d #t) (k 0)) (d (n "hmac") (r "^0.7.0") (d #t) (k 0)) (d (n "pbkdf2") (r "^0.3.0") (d #t) (k 0)) (d (n "sha2") (r "^0.8.0") (d #t) (k 0)))) (h "08sxyh5sq63vh8gkdvdkcx7z713wg7sn2f201l5bx2a3djygfq49")))

(define-public crate-ansible-vault-0.2.0 (c (n "ansible-vault") (v "0.2.0") (d (list (d (n "aes-ctr") (r "^0.6") (d #t) (k 0)) (d (n "block-padding") (r "^0.2") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "hmac") (r "^0.10") (d #t) (k 0)) (d (n "pbkdf2") (r "^0.7") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "sha2") (r "^0.9") (d #t) (k 0)))) (h "00r9b3vzvspahfnasssg1rbms3m2kglil20mppddgpjy4c41yr3d")))

(define-public crate-ansible-vault-0.2.1 (c (n "ansible-vault") (v "0.2.1") (d (list (d (n "aes-ctr") (r "^0.6") (d #t) (k 0)) (d (n "block-padding") (r "^0.2") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "hmac") (r "^0.10") (d #t) (k 0)) (d (n "pbkdf2") (r "^0.7") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "sha2") (r "^0.9") (d #t) (k 0)))) (h "1w2ivmick4gh7x87sqi92zr5846csk34picmfrfk95kk5nbl53pf")))

