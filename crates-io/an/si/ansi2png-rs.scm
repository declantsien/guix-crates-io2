(define-module (crates-io an si ansi2png-rs) #:use-module (crates-io))

(define-public crate-ansi2png-rs-0.1.0 (c (n "ansi2png-rs") (v "0.1.0") (d (list (d (n "image") (r "^0.23") (d #t) (k 0)) (d (n "imageproc") (r "^0.22") (d #t) (k 0)) (d (n "rusttype") (r "^0.9") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "vte") (r "^0.10") (d #t) (k 0)))) (h "0yf0dl117cfrfsrx3vn38fnwnqrw5h03ir4fga25lcki8r4qga1l")))

(define-public crate-ansi2png-rs-0.1.1 (c (n "ansi2png-rs") (v "0.1.1") (d (list (d (n "image") (r "^0.23") (d #t) (k 0)) (d (n "imageproc") (r "^0.22") (d #t) (k 0)) (d (n "rusttype") (r "^0.9") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "vte") (r "^0.10") (d #t) (k 0)))) (h "0rc9wvd1gi1hgm5fli641habrpkcs649w64faswnvlkvj4hr2gxl")))

