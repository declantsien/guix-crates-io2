(define-module (crates-io an si ansible-inventory) #:use-module (crates-io))

(define-public crate-ansible-inventory-0.0.0 (c (n "ansible-inventory") (v "0.0.0") (h "1w7qg5v4q4m1b88b3yw5wa5awl4xjfaamzng33ldbv7mpw1lymcw")))

(define-public crate-ansible-inventory-0.1.0 (c (n "ansible-inventory") (v "0.1.0") (d (list (d (n "indexmap") (r "^1") (f (quote ("serde-1"))) (k 0)) (d (n "serde") (r "^1") (f (quote ("std" "derive"))) (k 0)) (d (n "serde-enum-str") (r "^0.3") (k 0)) (d (n "serde_json") (r "^1") (f (quote ("std"))) (k 0)) (d (n "wrapping-macro") (r "^0.2") (f (quote ("std"))) (k 0)))) (h "11nkrlbx81psiyaf0xbpbbxfxmdqbfahilqci95k3yp90dcw2366")))

