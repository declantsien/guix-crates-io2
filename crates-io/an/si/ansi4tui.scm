(define-module (crates-io an si ansi4tui) #:use-module (crates-io))

(define-public crate-ansi4tui-0.1.0 (c (n "ansi4tui") (v "0.1.0") (d (list (d (n "termwiz") (r "^0.9") (d #t) (k 0)) (d (n "tui") (r "^0.10") (d #t) (k 0)))) (h "1sgy5spfijkhmyjjv9iczzssha806xg8l4ikbkn9lh2fljq4gzsf")))

(define-public crate-ansi4tui-0.2.0 (c (n "ansi4tui") (v "0.2.0") (d (list (d (n "termion") (r "^1.5") (d #t) (k 2)) (d (n "termwiz") (r "^0.11.0") (d #t) (k 0)) (d (n "tui") (r "^0.14") (d #t) (k 0)))) (h "1ysp9v82mmndb31mv719vp7m99vz3v5z4z7lfbr3a2mpgaq9b4w3")))

