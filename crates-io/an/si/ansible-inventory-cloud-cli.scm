(define-module (crates-io an si ansible-inventory-cloud-cli) #:use-module (crates-io))

(define-public crate-ansible-inventory-cloud-cli-0.0.0 (c (n "ansible-inventory-cloud-cli") (v "0.0.0") (h "1ggvw3qn6f8dgria4606zx7b73xv2yqcaapbqr2r5r2lmhwyp6i5")))

(define-public crate-ansible-inventory-cloud-cli-0.1.0 (c (n "ansible-inventory-cloud-cli") (v "0.1.0") (d (list (d (n "ansible-inventory") (r "^0.1") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "isahc") (r "^1") (f (quote ("http2"))) (o #t) (k 0)) (d (n "serde_json") (r "^1") (f (quote ("std"))) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (k 0)) (d (n "url") (r "^2") (f (quote ("serde"))) (k 0)))) (h "0y34shw12l8vjvx4ans1iad49h6lwr4y1h3iciv256cv7iklk54h") (f (quote (("with_http" "isahc") ("default" "with_http"))))))

