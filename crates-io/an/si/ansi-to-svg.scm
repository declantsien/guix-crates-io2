(define-module (crates-io an si ansi-to-svg) #:use-module (crates-io))

(define-public crate-ansi-to-svg-0.1.0 (c (n "ansi-to-svg") (v "0.1.0") (d (list (d (n "ansi-to") (r "^0.1") (d #t) (k 0)))) (h "14c3q58zibji2h431wwsvdr682rck5wlv0qakza454i0pd31x66g")))

(define-public crate-ansi-to-svg-0.1.1 (c (n "ansi-to-svg") (v "0.1.1") (d (list (d (n "ansi-to") (r "^0.1") (d #t) (k 0)))) (h "0qxpngmnh05vr7bpbpfag50ybzz0h0bkpd84mf23p86ja2g3m3xv")))

