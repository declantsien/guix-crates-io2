(define-module (crates-io an si ansi_term) #:use-module (crates-io))

(define-public crate-ansi_term-0.2.0 (c (n "ansi_term") (v "0.2.0") (h "1ymkgj0zj35rcbvgbhz8vw0lsndz2n3zc4jr81s3fd64jn8720fd")))

(define-public crate-ansi_term-0.3.0 (c (n "ansi_term") (v "0.3.0") (h "0431a2z0jhkvvhgnc5mvaf4hyyj08jqbkss4lk7xihcbc6zrrl78")))

(define-public crate-ansi_term-0.4.0 (c (n "ansi_term") (v "0.4.0") (h "0rkfdd6w7k03bi5v68yg49wk7jl9bvl5yr0gqc3w2jb87qgkddr1")))

(define-public crate-ansi_term-0.4.1 (c (n "ansi_term") (v "0.4.1") (d (list (d (n "regex") (r "^0.1.8") (d #t) (k 0)) (d (n "regex_macros") (r "^0.1.2") (d #t) (k 0)))) (h "1da2va9pirvg0768w1jfibqz5l9dn9ls4favixlhs400zyr2c4gq")))

(define-public crate-ansi_term-0.4.2 (c (n "ansi_term") (v "0.4.2") (d (list (d (n "regex") (r "^0.1.8") (d #t) (k 0)) (d (n "regex_macros") (r "^0.1.2") (d #t) (k 0)))) (h "1n0h57fww83xhq47v2fsnf0gr9q5d89kwn96vj1fcdfv5q4n6kys")))

(define-public crate-ansi_term-0.4.3 (c (n "ansi_term") (v "0.4.3") (d (list (d (n "regex") (r "^0.1.8") (d #t) (k 0)) (d (n "regex_macros") (r "^0.1.2") (d #t) (k 0)))) (h "0zgvydpsv59ilfqvnwvmrmmyvwm4phndq312bj56sf8vcbq9882i")))

(define-public crate-ansi_term-0.4.4 (c (n "ansi_term") (v "0.4.4") (d (list (d (n "regex") (r "^0.1.13") (d #t) (k 0)) (d (n "regex_macros") (r "^0.1.8") (d #t) (k 0)))) (h "05r0vlqlwfnzpvcq1g3jhvnv4rmf46yr39jypbc24fkmbkz9n2il")))

(define-public crate-ansi_term-0.4.5 (c (n "ansi_term") (v "0.4.5") (d (list (d (n "regex") (r "^0.1.13") (d #t) (k 0)) (d (n "regex_macros") (r "^0.1.8") (d #t) (k 0)))) (h "0vd67pyv8x0mlglgd7hckf8fd34hr5zfcg7vsiwvz11gqfzzpvia")))

(define-public crate-ansi_term-0.4.6 (c (n "ansi_term") (v "0.4.6") (d (list (d (n "regex") (r "^0.1.13") (d #t) (k 0)) (d (n "regex_macros") (r "^0.1.8") (d #t) (k 0)))) (h "0hwg032dnhvjhvjnskdgjqg7kmk29rrdzplcds147asg5nfw6rl4")))

(define-public crate-ansi_term-0.5.0 (c (n "ansi_term") (v "0.5.0") (h "1kmpcf7zvblb4xz53b4fj6a082aa1mzjrg924lnss8bdcscyrj46")))

(define-public crate-ansi_term-0.5.1 (c (n "ansi_term") (v "0.5.1") (h "0yjcihicyywdq23ac8msddlc7qpzhpbha8ybd9dvhlvjd8kcfpxh")))

(define-public crate-ansi_term-0.5.2 (c (n "ansi_term") (v "0.5.2") (h "03laq056p690fqi2hnyg63a2m4lq6ijxa6ia4a8na2kwwjak3mfp")))

(define-public crate-ansi_term-0.6.0 (c (n "ansi_term") (v "0.6.0") (h "0gcq2fs1r3jk1g147fv9xk47x6k2fk9rg5q24aidk7cjvw7vflbg")))

(define-public crate-ansi_term-0.6.1 (c (n "ansi_term") (v "0.6.1") (h "02radi6l08r5hqi8j3iz61yb4i84dgpycjgbaj6cjqxp5r82afxi")))

(define-public crate-ansi_term-0.6.2 (c (n "ansi_term") (v "0.6.2") (h "12zxsx80j2jmi8ablgk2jbbd179izs31hw4f4ck92b9s1lq03s2k")))

(define-public crate-ansi_term-0.6.3 (c (n "ansi_term") (v "0.6.3") (h "09a3aardklwmxdwysw6mdq2l6641v9k8pjdbndrrnisr0a36q85f")))

(define-public crate-ansi_term-0.7.0 (c (n "ansi_term") (v "0.7.0") (h "1ki6rimnzmd5wc3k5yz6a5hadars8da9a4wypaf9pccp8xmajlx0")))

(define-public crate-ansi_term-0.7.1 (c (n "ansi_term") (v "0.7.1") (h "0wnandjz7f54hbyjx30drawjccz826bvdvkap77qzs0khijz1vjw")))

(define-public crate-ansi_term-0.7.2 (c (n "ansi_term") (v "0.7.2") (h "0qkr1zvgphn1v46dsqwk8y0k4aw37nkygpwj7y7963363mdwsihz")))

(define-public crate-ansi_term-0.7.4 (c (n "ansi_term") (v "0.7.4") (h "1kq7r28vn2ymj4n4q8krw36bw2w9aky6dkz6q8f64pjvik07xqkq")))

(define-public crate-ansi_term-0.7.5 (c (n "ansi_term") (v "0.7.5") (h "03gpxx3vf9mlc1q5xa31dpbvf1k07k9ggcz3vl31rv44mp85l9rh")))

(define-public crate-ansi_term-0.8.0 (c (n "ansi_term") (v "0.8.0") (h "0s5ywf6kzsdcs5659mj9c43bhm202b84m9szmx0a5izy15z3jxy8")))

(define-public crate-ansi_term-0.9.0 (c (n "ansi_term") (v "0.9.0") (h "1xif1bh938qpfc3d0f9xgidibpm65xix11w9gszwqnia00q7rb13")))

(define-public crate-ansi_term-0.10.1 (c (n "ansi_term") (v "0.10.1") (h "01gmf976yf532j8m3562269jbfasxp0prq3y3fx8r0lflx65c985")))

(define-public crate-ansi_term-0.10.2 (c (n "ansi_term") (v "0.10.2") (h "0m84yw95yz6c0kgqispb576gnllyk2s9npqjrswadvvwifs6hdbb")))

(define-public crate-ansi_term-0.11.0 (c (n "ansi_term") (v "0.11.0") (d (list (d (n "winapi") (r "^0.3.4") (f (quote ("errhandlingapi" "consoleapi" "processenv"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "16wpvrghvd0353584i1idnsgm0r3vchg8fyrm0x8ayv1rgvbljgf")))

(define-public crate-ansi_term-0.12.0 (c (n "ansi_term") (v "0.12.0") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^1.0.90") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 2)) (d (n "winapi") (r "^0.3.4") (f (quote ("errhandlingapi" "consoleapi" "processenv" "handleapi"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "0dmvziqx1j06xbv3zx62k7w81dyaqviag1rk5a0iynjqqdk2g9za") (f (quote (("derive_serde_style" "serde"))))))

(define-public crate-ansi_term-0.12.1 (c (n "ansi_term") (v "0.12.1") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)) (d (n "regex") (r "^1.1.9") (d #t) (k 2)) (d (n "serde") (r "^1.0.90") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 2)) (d (n "winapi") (r "^0.3.4") (f (quote ("consoleapi" "errhandlingapi" "fileapi" "handleapi" "processenv"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "1ljmkbilxgmhavxvxqa7qvm6f3fjggi7q2l3a72q9x0cxjvrnanm") (f (quote (("derive_serde_style" "serde"))))))

