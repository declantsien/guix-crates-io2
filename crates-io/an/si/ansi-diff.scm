(define-module (crates-io an si ansi-diff) #:use-module (crates-io))

(define-public crate-ansi-diff-1.0.0 (c (n "ansi-diff") (v "1.0.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "signal-hook") (r "^0.3.13") (f (quote ("iterator" "extended-siginfo"))) (d #t) (k 2)) (d (n "term_size") (r "^0.3.2") (d #t) (k 2)))) (h "13vp598bsj1qfanp0jjmfmjp2py4kyxmf1l4nj3263hfqd845r7m")))

(define-public crate-ansi-diff-1.0.1 (c (n "ansi-diff") (v "1.0.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "signal-hook") (r "^0.3.13") (f (quote ("iterator" "extended-siginfo"))) (d #t) (k 2)) (d (n "term_size") (r "^0.3.2") (d #t) (k 2)))) (h "0hmg46w7jmkn8cla6vgvgxav7p0zl3nhmfy9iyd9x1lj3jl6847r")))

(define-public crate-ansi-diff-1.1.0 (c (n "ansi-diff") (v "1.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "signal-hook") (r "^0.3.13") (f (quote ("iterator" "extended-siginfo"))) (d #t) (k 2)) (d (n "term_size") (r "^0.3.2") (d #t) (k 2)))) (h "182m47kbmzh1cvx944864pjc4i7arjfjmqq4hpjlk81zmy2q5fhd")))

