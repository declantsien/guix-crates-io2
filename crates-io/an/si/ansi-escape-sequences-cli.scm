(define-module (crates-io an si ansi-escape-sequences-cli) #:use-module (crates-io))

(define-public crate-ansi-escape-sequences-cli-0.1.0 (c (n "ansi-escape-sequences-cli") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)))) (h "07jlj8r1dfsn1xi31jscjja9j0r41r45syjj2pf2nzdzac4m7x56")))

(define-public crate-ansi-escape-sequences-cli-0.1.1 (c (n "ansi-escape-sequences-cli") (v "0.1.1") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)))) (h "19278ydjcjhdjclbbwdp3rrxkjri56rq8dmr0ibwhjx291smw9xz")))

(define-public crate-ansi-escape-sequences-cli-0.1.2 (c (n "ansi-escape-sequences-cli") (v "0.1.2") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)))) (h "1c1hq0vbni3sgc01wh10cnjg5sg52y74ps4dqknqrq1x251d9h2i")))

(define-public crate-ansi-escape-sequences-cli-0.1.4 (c (n "ansi-escape-sequences-cli") (v "0.1.4") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)))) (h "13pz8s2gkrpv3kn53nlgdc325cl0x9bbxmg6ggq8kjacavsrvxaj")))

