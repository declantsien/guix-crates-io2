(define-module (crates-io an si ansi-style) #:use-module (crates-io))

(define-public crate-ansi-style-1.0.0 (c (n "ansi-style") (v "1.0.0") (h "1z3lczm82y49y63r3my0spq199nlbkpns0b4rzdaldqgjx9kimxw")))

(define-public crate-ansi-style-1.1.0 (c (n "ansi-style") (v "1.1.0") (h "0l23g7pxxf19p5jrbsvvzzryw0cvvh4xg4ll9z2d9zh3fba5sn3l")))

(define-public crate-ansi-style-1.2.0 (c (n "ansi-style") (v "1.2.0") (h "1108cicsmiriykvjp9s4zzk658c3j0r6hsy3qxxl4qgp46lr5l05")))

(define-public crate-ansi-style-1.2.1 (c (n "ansi-style") (v "1.2.1") (h "1hbb3wkwj9kz0k1z4ykn1wd42jcj0pa4q9jfnz2p7w5krbjygx3n")))

