(define-module (crates-io an si ansi-parser) #:use-module (crates-io))

(define-public crate-ansi-parser-0.1.0 (c (n "ansi-parser") (v "0.1.0") (d (list (d (n "nom") (r "^4.2.3") (d #t) (k 0)) (d (n "num-derive") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "14aqkr0qsbhrdm8pp4dygd3phqyzcr9ygc1wih6lz6118bd2ihzh")))

(define-public crate-ansi-parser-0.2.0 (c (n "ansi-parser") (v "0.2.0") (d (list (d (n "nom") (r "^4.2.3") (d #t) (k 0)) (d (n "num-derive") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1mya0n4yhf61cjhzfwi4a7j30k710sv3gmcmlzbmlivwwzg06lgl")))

(define-public crate-ansi-parser-0.3.0 (c (n "ansi-parser") (v "0.3.0") (d (list (d (n "nom") (r "^4.2.3") (d #t) (k 0)) (d (n "num-derive") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0him30q2zg2zp1fg0b286rm84s5dn2vgzz9kpxwhf9fv3r7pqiip")))

(define-public crate-ansi-parser-0.4.0 (c (n "ansi-parser") (v "0.4.0") (d (list (d (n "nom") (r "^4.2.3") (d #t) (k 0)) (d (n "num-derive") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1y2mn8i45zw2k30y9lga7zbqks1x449kk05c0sfm4rbi8zd7diph")))

(define-public crate-ansi-parser-0.5.0 (c (n "ansi-parser") (v "0.5.0") (d (list (d (n "nom") (r "^4.2.3") (d #t) (k 0)) (d (n "num-derive") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1afkfpmjr6p9sxz5xmma7zrjhhmsnspwlvwlffp63ljx0wq7rkz9")))

(define-public crate-ansi-parser-0.5.1 (c (n "ansi-parser") (v "0.5.1") (d (list (d (n "nom") (r "^4.2.3") (d #t) (k 0)))) (h "0g5hqh9qjcvyqf6r4967046nz00phz03bcx2krw2w630qiykvjl6")))

(define-public crate-ansi-parser-0.5.2 (c (n "ansi-parser") (v "0.5.2") (d (list (d (n "nom") (r "^4.2.3") (d #t) (k 0)))) (h "0m1dn74pj4bfxpf02s2f7raz66iq7f0c0glchwhlfw8vldd7gjpq")))

(define-public crate-ansi-parser-0.5.3 (c (n "ansi-parser") (v "0.5.3") (d (list (d (n "nom") (r "^4.2.3") (d #t) (k 0)))) (h "05i8j20rhg1w8smd2vmhs81fp88nbrxl8bgbrsrrclqlb79x3hcd")))

(define-public crate-ansi-parser-0.6.0 (c (n "ansi-parser") (v "0.6.0") (d (list (d (n "nom") (r "^4.2.3") (d #t) (k 0)))) (h "0di6x9635gwp1rm7xzppd7ls9h16wpb7gzhpiqrl34xs53hhg7mj")))

(define-public crate-ansi-parser-0.6.1 (c (n "ansi-parser") (v "0.6.1") (d (list (d (n "nom") (r "^4.2.3") (d #t) (k 0)))) (h "0409lj02h1p7w2w0acrzqyp7d29yn2iisd65idbmm1hvk1zfzkbb")))

(define-public crate-ansi-parser-0.6.2 (c (n "ansi-parser") (v "0.6.2") (d (list (d (n "nom") (r "^4.2.3") (d #t) (k 0)))) (h "1x8zw1bmaw21wbmv7i8sll4gqhi4da7w13igiw379dk1nikxmzzg")))

(define-public crate-ansi-parser-0.6.3 (c (n "ansi-parser") (v "0.6.3") (d (list (d (n "nom") (r "^4.2.3") (d #t) (k 0)))) (h "1wz069h9z476kqcgp3iyjs91pdzw2dk1cahfsjvh34j7by7qnn0i")))

(define-public crate-ansi-parser-0.6.4 (c (n "ansi-parser") (v "0.6.4") (d (list (d (n "nom") (r "^4.2.3") (d #t) (k 0)))) (h "126iwspk4c2nbv7854pwfpwjhzhwfq3fn9mvk2gzhjqz2rml93xf")))

(define-public crate-ansi-parser-0.6.5 (c (n "ansi-parser") (v "0.6.5") (d (list (d (n "nom") (r "^4.2.3") (d #t) (k 0)))) (h "152idb8a6gwdxzj6m099h3xgx8vw0sjc6skgw94nm2k3y5swc6kn")))

(define-public crate-ansi-parser-0.7.0 (c (n "ansi-parser") (v "0.7.0") (d (list (d (n "heapless") (r "^0.5.6") (d #t) (k 0)) (d (n "nom") (r "^4.2.3") (d #t) (k 0)))) (h "1wyyx15zs3bnzk8slv8mbsnwii8qsrrxfvajd6ykc0km7g1rbah1") (f (quote (("std") ("default" "std"))))))

(define-public crate-ansi-parser-0.8.0 (c (n "ansi-parser") (v "0.8.0") (d (list (d (n "heapless") (r "^0.5.6") (d #t) (k 0)) (d (n "nom") (r "^4.2.3") (k 0)))) (h "09vi51kdnbwj6c4vdiiydpqxiiwyppn7kbynf22ij9xzg4h3kcmw") (f (quote (("std" "nom/std") ("default" "std"))))))

(define-public crate-ansi-parser-0.9.0 (c (n "ansi-parser") (v "0.9.0") (d (list (d (n "heapless") (r "^0.6.1") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (k 0)))) (h "0sicrp0s6vagmk2axqss76gxdk1fr8lbpqidikb1n43mlyabvmgs") (f (quote (("std" "nom/std") ("default" "std"))))))

(define-public crate-ansi-parser-0.9.1 (c (n "ansi-parser") (v "0.9.1") (d (list (d (n "heapless") (r "^0.8.0") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (k 0)))) (h "0wwicv3sx7sw3qhkkpjvnmynkpzc31b8bhj3s45ms0jg53c7ygn4") (f (quote (("std" "nom/std") ("default" "std"))))))

