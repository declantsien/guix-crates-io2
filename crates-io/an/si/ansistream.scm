(define-module (crates-io an si ansistream) #:use-module (crates-io))

(define-public crate-ansistream-0.1.1 (c (n "ansistream") (v "0.1.1") (h "0rz73plmxlhwdjmhxgdnd3h5hn611qjhnczzdv4bybryizzfhw63") (y #t)))

(define-public crate-ansistream-0.1.2 (c (n "ansistream") (v "0.1.2") (h "0gm0ahm6qxb0sgipkd52hhg516p6raxinmjcb39hwaxqb0lxv2nc")))

(define-public crate-ansistream-0.1.3 (c (n "ansistream") (v "0.1.3") (h "0c3kkx1zh67a8pri6wgxrcca9jimdrx4v3k4bdmdqw8bq252gk5n")))

(define-public crate-ansistream-0.1.4 (c (n "ansistream") (v "0.1.4") (h "0l908ksmp7icdn50plq2vh2vydcz8ip2ph6g3n15km9zbc6w5xrc")))

(define-public crate-ansistream-0.1.5 (c (n "ansistream") (v "0.1.5") (h "1qvvqgy9jqv273ygz02scd6qp7qldka2g7f48yfwgz3j9azjza40")))

(define-public crate-ansistream-0.1.6 (c (n "ansistream") (v "0.1.6") (h "0f5cjnx9fl5sr9qlfx30x66djp7aymfaxaqy15511vxdpcaiih97")))

(define-public crate-ansistream-0.2.0 (c (n "ansistream") (v "0.2.0") (h "1dkksk2r6nvsryp3igi1dqcqj4kwl49l9b9bh6cq23aaaig8v0db")))

