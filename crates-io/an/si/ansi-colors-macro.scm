(define-module (crates-io an si ansi-colors-macro) #:use-module (crates-io))

(define-public crate-ansi-colors-macro-0.1.0 (c (n "ansi-colors-macro") (v "0.1.0") (h "08lc4dmm2b8kfb7hbigj50izzkx8n98nm2pr3ir4kxs3w2h18hq2")))

(define-public crate-ansi-colors-macro-0.1.1 (c (n "ansi-colors-macro") (v "0.1.1") (h "1xk7pqvdihnajwrh31jzbi19l801v01p4izvrjrh6rgkp2vqsqmk")))

(define-public crate-ansi-colors-macro-0.1.2 (c (n "ansi-colors-macro") (v "0.1.2") (h "1xiw7kdxccjyvir2yfbff953g7n8wy2gvxkdy27yh5i99alazhpl")))

