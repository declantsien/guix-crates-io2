(define-module (crates-io an si ansi_codes) #:use-module (crates-io))

(define-public crate-ansi_codes-0.1.0 (c (n "ansi_codes") (v "0.1.0") (h "16lw7k7z5dnjwkrdf74hks0sh2fcci9wcpax8smd5gaw59sjv03m")))

(define-public crate-ansi_codes-0.1.1 (c (n "ansi_codes") (v "0.1.1") (h "1kacfbmkx477jpykpkl29776x01d197szzkv8131415y5wfwbrcf")))

(define-public crate-ansi_codes-0.2.1 (c (n "ansi_codes") (v "0.2.1") (h "0wakajgbd72k8pbn8w8ndizfvj88j1r9h0qinyzra7yckasj1j26")))

