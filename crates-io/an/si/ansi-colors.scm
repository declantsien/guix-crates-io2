(define-module (crates-io an si ansi-colors) #:use-module (crates-io))

(define-public crate-ansi-colors-0.1.0 (c (n "ansi-colors") (v "0.1.0") (h "0flf4856v0gnj44l6qhygiwh09nq46k4ibx83ycfgzicbspk5v6i")))

(define-public crate-ansi-colors-0.1.1 (c (n "ansi-colors") (v "0.1.1") (h "0mgvj3pr75215xhngmq30b7z8f0qqk8fqzgg920a904h9kw30xs4")))

(define-public crate-ansi-colors-0.2.0 (c (n "ansi-colors") (v "0.2.0") (h "1qyb8s3jwvzffqw476361b4vyvzcjzgd3nib6hfpwgac6hm0s7hz")))

(define-public crate-ansi-colors-0.3.0 (c (n "ansi-colors") (v "0.3.0") (h "02sgrgr5b6a0955chk2wbv98c85dvymxi2yqwsfwddxaibz34ch9")))

