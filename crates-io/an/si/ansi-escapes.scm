(define-module (crates-io an si ansi-escapes) #:use-module (crates-io))

(define-public crate-ansi-escapes-0.1.0 (c (n "ansi-escapes") (v "0.1.0") (d (list (d (n "tempfile") (r "^2.1.4") (d #t) (k 2)))) (h "1cv85bik542kzpvxm17bwfsr6g317cmfl0ih1vcy4gsqi1gzyfqn")))

(define-public crate-ansi-escapes-0.1.1 (c (n "ansi-escapes") (v "0.1.1") (d (list (d (n "tempfile") (r "^2.1.4") (d #t) (k 2)))) (h "1byp22h6cnq5wf5pdg2vph0j384ggxi8kdilayczakg2mam0sg3y")))

