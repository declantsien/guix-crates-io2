(define-module (crates-io an si ansi_brush) #:use-module (crates-io))

(define-public crate-ansi_brush-0.0.1 (c (n "ansi_brush") (v "0.0.1") (h "0pl86zy0h4iwx4c79ad5m049awyfcvmzfwdyzdk3vl994wwjnymz")))

(define-public crate-ansi_brush-0.0.2 (c (n "ansi_brush") (v "0.0.2") (h "1yaia7q6356nn96c6qhf9w6b0z0cj5kr86r75a71qmw496y77frl")))

(define-public crate-ansi_brush-0.0.21 (c (n "ansi_brush") (v "0.0.21") (h "0iwdfmqw1hvjp7l1byph3pj96s0iisa5h28rnh21gl9bsydfa69m")))

(define-public crate-ansi_brush-0.0.22 (c (n "ansi_brush") (v "0.0.22") (h "0si135k1cm0bqlkj011x7vfj608cvs4q93p2iz6aqvskvysfdvh6")))

(define-public crate-ansi_brush-0.0.23 (c (n "ansi_brush") (v "0.0.23") (h "00sfkx86g2rm42cak04xhmy0pmw3grfpiir2yc11917bx3sz3wk1")))

(define-public crate-ansi_brush-0.0.3 (c (n "ansi_brush") (v "0.0.3") (h "1pf3sifa40w6rd0k679376w42spzcln59hfdsxrxwqzgdx58kwgc")))

(define-public crate-ansi_brush-0.0.31 (c (n "ansi_brush") (v "0.0.31") (h "1dr5ssjsbcsmr7wmjhz2acm0d4isk1hmniqdqgzy3syirk4py6qc")))

