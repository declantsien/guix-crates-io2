(define-module (crates-io an si ansi2html) #:use-module (crates-io))

(define-public crate-ansi2html-0.1.0 (c (n "ansi2html") (v "0.1.0") (d (list (d (n "ansi2") (r "^0.1") (d #t) (k 0)) (d (n "html-escape") (r "^0.2.13") (d #t) (k 0)))) (h "0k2b29lp2mpvwgxvyyb1a30iw8pgc0mzny1rnfg9yrlcmr8j8w00")))

(define-public crate-ansi2html-0.2.0 (c (n "ansi2html") (v "0.2.0") (d (list (d (n "ansi2") (r "^0.2") (d #t) (k 0)) (d (n "html-escape") (r "^0.2.13") (d #t) (k 0)))) (h "1f8b57ca80dvsc6n2cr0gv3dyq9a139mmrmlmny2nv653a6zlz3r")))

(define-public crate-ansi2html-0.2.2 (c (n "ansi2html") (v "0.2.2") (d (list (d (n "ansi2") (r "^0.2") (d #t) (k 0)) (d (n "html-escape") (r "^0.2.13") (d #t) (k 0)))) (h "0sq16fyzqgsfzq7pm1w2bxllxy8mszsvy5bzj3yqp7iiv0lff58x")))

