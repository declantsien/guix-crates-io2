(define-module (crates-io an si ansi-hex-color) #:use-module (crates-io))

(define-public crate-ansi-hex-color-0.1.0 (c (n "ansi-hex-color") (v "0.1.0") (d (list (d (n "raster") (r "^0.2.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0vnacais2pprjjypdmhg1xnhjq1v775q4kymp95z02yq4z76qvpz")))

(define-public crate-ansi-hex-color-0.1.1 (c (n "ansi-hex-color") (v "0.1.1") (d (list (d (n "raster") (r "^0.2.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0hn46bvq5j9hnr9fyq8rzk8discplgbfv49m2i3nhigaz18niwam")))

