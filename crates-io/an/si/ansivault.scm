(define-module (crates-io an si ansivault) #:use-module (crates-io))

(define-public crate-ansivault-0.1.0 (c (n "ansivault") (v "0.1.0") (d (list (d (n "ansible-vault") (r "^0.2.0") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.40") (d #t) (k 0)) (d (n "gumdrop") (r "^0.8.0") (d #t) (k 0)) (d (n "gumdrop_derive") (r "^0.8.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "stderrlog") (r "^0.5.1") (d #t) (k 0)))) (h "1p0pk4gm28ixxa75wa7vw45c5h9md1q4smkj3dxkpdmk7vfidipf")))

(define-public crate-ansivault-0.1.1 (c (n "ansivault") (v "0.1.1") (d (list (d (n "ansible-vault") (r "^0.2.0") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.40") (d #t) (k 0)) (d (n "gumdrop") (r "^0.8.0") (d #t) (k 0)) (d (n "gumdrop_derive") (r "^0.8.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "stderrlog") (r "^0.5.1") (d #t) (k 0)))) (h "1l90kn0snjbrlv6hs8nyq07pw5jwd5b3z2dd5r36jkvzgb3wfw2f")))

