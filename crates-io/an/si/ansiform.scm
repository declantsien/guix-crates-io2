(define-module (crates-io an si ansiform) #:use-module (crates-io))

(define-public crate-ansiform-0.1.0 (c (n "ansiform") (v "0.1.0") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0marbgprfc1514nwq49fk7mxf5hcxq5fb9kcajs6562g8xwlxvm2")))

(define-public crate-ansiform-0.1.1 (c (n "ansiform") (v "0.1.1") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "proc-macro"))) (k 0)))) (h "0m8xclnkf5bm032k33z1q4akqn0hrlhvcv0s653fkdvcn6hps7d8")))

