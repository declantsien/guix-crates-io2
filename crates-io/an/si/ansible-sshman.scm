(define-module (crates-io an si ansible-sshman) #:use-module (crates-io))

(define-public crate-ansible-sshman-0.2.0 (c (n "ansible-sshman") (v "0.2.0") (d (list (d (n "clap") (r "^3.2.20") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.11") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)))) (h "0a42wd0qnnh6jgdrc5z911h30k8pvgxn11fqng42j49vmnh9ngcb")))

(define-public crate-ansible-sshman-1.0.0 (c (n "ansible-sshman") (v "1.0.0") (d (list (d (n "clap") (r "^4.4.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.27") (d #t) (k 0)) (d (n "tempfile") (r "^3.8.1") (d #t) (k 0)))) (h "1b58vk3hi1nps923k32jaxny97k7z3k9wnnf0k8adlq22hgqi89v")))

(define-public crate-ansible-sshman-2.0.0 (c (n "ansible-sshman") (v "2.0.0") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.34") (d #t) (k 0)) (d (n "tempfile") (r "^3.10.1") (d #t) (k 0)))) (h "10il6ja82n2bp5s1w8qirlskpbq625bbcw5ahnr0v41gsbrpz27p")))

