(define-module (crates-io an si ansi-color-codec) #:use-module (crates-io))

(define-public crate-ansi-color-codec-0.2.9 (c (n "ansi-color-codec") (v "0.2.9") (d (list (d (n "clap") (r "^4.0.23") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ctrlc") (r "^3.2.3") (d #t) (k 0)))) (h "04dsrd7d7djz6qarr1bz35nnj3c1vdfg73gfwa5dj4wzbcql12x1") (y #t)))

(define-public crate-ansi-color-codec-0.3.1 (c (n "ansi-color-codec") (v "0.3.1") (d (list (d (n "clap") (r "^4.0.23") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ctrlc") (r "^3.2.3") (d #t) (k 0)))) (h "1i42vzdks62xxxb897ingw3axphk3hzwi7vqwgnpw0hbkxrk2i5i") (y #t)))

(define-public crate-ansi-color-codec-0.3.2 (c (n "ansi-color-codec") (v "0.3.2") (d (list (d (n "clap") (r "^4.0.23") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ctrlc") (r "^3.2.3") (d #t) (k 0)))) (h "0vy2800rk9ng4b4h0v2s58a6rbfz540xd1lid9mqa853rd36vm7y") (y #t)))

(define-public crate-ansi-color-codec-0.3.3 (c (n "ansi-color-codec") (v "0.3.3") (d (list (d (n "clap") (r "^4.0.23") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ctrlc") (r "^3.2.3") (d #t) (k 0)))) (h "0n949j5ak46cfs3jnx2sdxk4idkxni5m9imlp191pxj79vy28nmd") (y #t)))

(define-public crate-ansi-color-codec-0.3.4 (c (n "ansi-color-codec") (v "0.3.4") (d (list (d (n "clap") (r "^4.0.23") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ctrlc") (r "^3.2.3") (d #t) (k 0)))) (h "0avj6dwnghasr0r294p4zzsmg0hwnfi8vxx822llq2lfq7jg4j0h") (y #t)))

(define-public crate-ansi-color-codec-0.3.5 (c (n "ansi-color-codec") (v "0.3.5") (d (list (d (n "clap") (r "^4.0.23") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ctrlc") (r "^3.2.3") (d #t) (k 0)))) (h "0jipp433r4sggn0qdvsg86491m96rmlk53grabdhqlimkwvyqmlv") (y #t)))

(define-public crate-ansi-color-codec-0.3.11 (c (n "ansi-color-codec") (v "0.3.11") (d (list (d (n "clap") (r "^4.0.23") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ctrlc") (r "^3.2.3") (d #t) (k 0)))) (h "12l1r71cgqadzp88z5fd8bjyf6vyxxv6745ry2hgpzhr9bwrin4j") (y #t)))

(define-public crate-ansi-color-codec-0.3.13 (c (n "ansi-color-codec") (v "0.3.13") (d (list (d (n "clap") (r "^4.0.23") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ctrlc") (r "^3.2.3") (d #t) (k 0)))) (h "1bf27c9ljspdvgklxynhb135nsyis2drgh5cswsrwxiawhs2ch81") (y #t)))

(define-public crate-ansi-color-codec-0.4.0 (c (n "ansi-color-codec") (v "0.4.0") (d (list (d (n "clap") (r "^4.0.23") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ctrlc") (r "^3.2.3") (d #t) (k 0)))) (h "0qwglhwm8q2q31g9smvh2xq8w1s6z6viri7c12826arx50bf648q") (y #t)))

(define-public crate-ansi-color-codec-0.5.0 (c (n "ansi-color-codec") (v "0.5.0") (d (list (d (n "clap") (r "^4.0.23") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ctrlc") (r "^3.2.3") (d #t) (k 0)))) (h "1ck48ydifpqcarax2419a3nizpwxz6sfnn3dj5dm96c27103xn3d") (y #t)))

(define-public crate-ansi-color-codec-0.5.1 (c (n "ansi-color-codec") (v "0.5.1") (d (list (d (n "clap") (r "^4.0.23") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ctrlc") (r "^3.2.3") (d #t) (k 0)))) (h "0xmhxk3ng1pvfnbm9xdzgq3yxpd7kyddqvbk0x9c53fafprj2c5i") (y #t)))

(define-public crate-ansi-color-codec-0.5.2 (c (n "ansi-color-codec") (v "0.5.2") (d (list (d (n "clap") (r "^4.0.23") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ctrlc") (r "^3.2.3") (d #t) (k 0)))) (h "091wydhhd8vmb0mhfk9x4sl7r4lyg0i208dybnd7z46imrqviiqq") (y #t)))

(define-public crate-ansi-color-codec-0.5.3 (c (n "ansi-color-codec") (v "0.5.3") (d (list (d (n "clap") (r "^4.0.23") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ctrlc") (r "^3.2.3") (d #t) (k 0)))) (h "0555rrwq95dn4vvnkvhwrg56gp24k0j759wmkw3xkmm7kx2m31nh") (y #t)))

(define-public crate-ansi-color-codec-0.5.4 (c (n "ansi-color-codec") (v "0.5.4") (d (list (d (n "clap") (r "^4.0.23") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ctrlc") (r "^3.2.3") (d #t) (k 0)))) (h "0zc0b8fmdcv1f1cf4naapcx0f502wvnya754clxhphdc036dxsdl") (y #t)))

(define-public crate-ansi-color-codec-0.5.5 (c (n "ansi-color-codec") (v "0.5.5") (d (list (d (n "clap") (r "^4.0.23") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ctrlc") (r "^3.2.3") (d #t) (k 0)))) (h "1pxcq76s792zic0ydwyjc6g9ajkjbnsl9xilc70h6z50xabsvk9f") (y #t)))

(define-public crate-ansi-color-codec-0.5.6 (c (n "ansi-color-codec") (v "0.5.6") (d (list (d (n "clap") (r "^4.0.23") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ctrlc") (r "^3.2.3") (d #t) (k 0)))) (h "0n42r2cgc0a22gzk4xwhf27nsmgjlndpxgg8ylx8waafnh09h322") (y #t)))

(define-public crate-ansi-color-codec-0.5.7 (c (n "ansi-color-codec") (v "0.5.7") (d (list (d (n "clap") (r "^4.0.23") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ctrlc") (r "^3.2.3") (d #t) (k 0)))) (h "080wm0kgvwwn0w8835nv5y6i8q7nm4avn80xb9ngvcfzlzjvfvih") (y #t)))

(define-public crate-ansi-color-codec-0.5.8 (c (n "ansi-color-codec") (v "0.5.8") (d (list (d (n "clap") (r "^4.0.23") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ctrlc") (r "^3.2.3") (d #t) (k 0)))) (h "063k5a4prlhsglj8f5lgh1dxxk34yf1qg8q6227r5f5padn8f8d0") (y #t)))

(define-public crate-ansi-color-codec-0.5.9 (c (n "ansi-color-codec") (v "0.5.9") (d (list (d (n "clap") (r "^4.0.23") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ctrlc") (r "^3.2.3") (d #t) (k 0)))) (h "0p8xk6047n63q8y6drgncxv17lk5hjfc7iz74cxbi1nc4km4f5nh") (y #t)))

(define-public crate-ansi-color-codec-0.5.10 (c (n "ansi-color-codec") (v "0.5.10") (d (list (d (n "clap") (r "^4.0.23") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ctrlc") (r "^3.2.3") (d #t) (k 0)))) (h "0c1dvn4rcv2w6im8jfpl57r2a8spgp0pyv5rkn1lh1g1ikydxqzk") (y #t)))

(define-public crate-ansi-color-codec-0.5.11 (c (n "ansi-color-codec") (v "0.5.11") (d (list (d (n "clap") (r "^4.0.23") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ctrlc") (r "^3.2.3") (d #t) (k 0)))) (h "0v8389cs9wkl5b0z0d6q5h1nrqfw4fkw721qgwri96q79rmjmr5i") (y #t)))

(define-public crate-ansi-color-codec-0.5.12 (c (n "ansi-color-codec") (v "0.5.12") (d (list (d (n "clap") (r "^4.0.23") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ctrlc") (r "^3.2.3") (d #t) (k 0)))) (h "1f8pidrpcf3svq2k1s3phn2ydjzai7rnq5za90g2cx9f0hfbwvln") (y #t)))

(define-public crate-ansi-color-codec-0.5.13 (c (n "ansi-color-codec") (v "0.5.13") (d (list (d (n "clap") (r "^4.0.23") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ctrlc") (r "^3.2.3") (d #t) (k 0)))) (h "1sj9b0rdgk2a3k2gv00glmzjaqj5a71ch2jna0haq5zkxhk06xak")))

(define-public crate-ansi-color-codec-0.5.14 (c (n "ansi-color-codec") (v "0.5.14") (d (list (d (n "clap") (r "^4.0.23") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ctrlc") (r "^3.2.3") (d #t) (k 0)))) (h "09nm6zfx20cmn393ayk1sxk8zfqj2k4v0df5iagvzrf6s41nvarh")))

(define-public crate-ansi-color-codec-0.6.0 (c (n "ansi-color-codec") (v "0.6.0") (d (list (d (n "clap") (r "^4.0.23") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ctrlc") (r "^3.2.3") (d #t) (k 0)))) (h "032yp86sfk0hkkbk0xqjac3qhpmc6clqfcbz18cgv6005xl13hj2")))

(define-public crate-ansi-color-codec-0.6.1 (c (n "ansi-color-codec") (v "0.6.1") (d (list (d (n "clap") (r "^4.0.23") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ctrlc") (r "^3.2.3") (d #t) (k 0)))) (h "1gq98pfrinfwxf6ijjmqxrisiha3wdz33kcll0lslrlkb1lzcc1f")))

(define-public crate-ansi-color-codec-0.7.0 (c (n "ansi-color-codec") (v "0.7.0") (d (list (d (n "clap") (r "^4.0.23") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ctrlc") (r "^3.2.3") (d #t) (k 0)))) (h "187lp90kp20k6h31g8r6gsjsmvh8h9p9gcn1n99kr4y1rwwx1cdn")))

