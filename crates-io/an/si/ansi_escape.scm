(define-module (crates-io an si ansi_escape) #:use-module (crates-io))

(define-public crate-ansi_escape-0.1.0 (c (n "ansi_escape") (v "0.1.0") (d (list (d (n "string_utils") (r "^0.1.1") (d #t) (k 0)) (d (n "vector_utils") (r "^0.1.3") (d #t) (k 0)))) (h "1pvmmp5rcmkkkpq1sdfbhr3f8pn0xsyqdhil67pmbr4ir4rxcz1j")))

(define-public crate-ansi_escape-0.1.1 (c (n "ansi_escape") (v "0.1.1") (d (list (d (n "string_utils") (r "^0.1") (d #t) (k 0)) (d (n "vector_utils") (r "^0.1.3") (d #t) (k 0)))) (h "1wrzqshykqidip20isylw7f4h8ymlyz93ggkvdv9h4aabhkdshrs")))

(define-public crate-ansi_escape-0.1.2 (c (n "ansi_escape") (v "0.1.2") (d (list (d (n "string_utils") (r "^0.1") (d #t) (k 0)) (d (n "vector_utils") (r "^0.1") (d #t) (k 0)))) (h "1i2xpxyjr50h279ixrgx51gay5g2iw7glklajz3v22abidwq0nr2")))

(define-public crate-ansi_escape-0.1.3 (c (n "ansi_escape") (v "0.1.3") (d (list (d (n "string_utils") (r "^0.1") (d #t) (k 0)) (d (n "vector_utils") (r "^0.1") (d #t) (k 0)))) (h "0ms514iknfj7zyh9cfbjfd4si2892qkabp2mgkhc3jvc9zd4sw4f")))

