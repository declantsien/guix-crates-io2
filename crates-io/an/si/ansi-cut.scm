(define-module (crates-io an si ansi-cut) #:use-module (crates-io))

(define-public crate-ansi-cut-0.1.0 (c (n "ansi-cut") (v "0.1.0") (d (list (d (n "ansi-parser") (r "^0.8.0") (d #t) (k 0)) (d (n "owo-colors") (r "^2.0.0") (d #t) (k 2)) (d (n "strip-ansi-escapes") (r "^0.1.0") (d #t) (k 0)))) (h "17km0mqif9r2rdbhbfzr4hlvc5wc34m0q9gaagjd4ikldsfdks84")))

(define-public crate-ansi-cut-0.1.1 (c (n "ansi-cut") (v "0.1.1") (d (list (d (n "ansi-parser") (r "^0.8.0") (d #t) (k 0)) (d (n "owo-colors") (r "^2.0.0") (d #t) (k 2)) (d (n "strip-ansi-escapes") (r "^0.1.0") (d #t) (k 0)))) (h "0f98g39gkqyyw2rl8s1q6ii2gy211608prrz1g1jp54hgahnfvqi")))

(define-public crate-ansi-cut-0.2.0 (c (n "ansi-cut") (v "0.2.0") (d (list (d (n "ansi-parser") (r "^0.8.0") (d #t) (k 0)) (d (n "owo-colors") (r "^3.2.0") (d #t) (k 2)))) (h "1kn8n8hi1r9qbp9g7439gvyzi2caa7whkaajxfij1blh8fcx5s7z")))

