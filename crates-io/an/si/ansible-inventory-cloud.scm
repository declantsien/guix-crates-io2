(define-module (crates-io an si ansible-inventory-cloud) #:use-module (crates-io))

(define-public crate-ansible-inventory-cloud-0.0.0 (c (n "ansible-inventory-cloud") (v "0.0.0") (h "09r3rq84r7n1z3dzv9cyf0fagmhvm4kzbz7a2mx58n6db7si7kky")))

(define-public crate-ansible-inventory-cloud-0.1.0 (c (n "ansible-inventory-cloud") (v "0.1.0") (d (list (d (n "ansible-inventory") (r "^0.1") (d #t) (k 0)) (d (n "axum") (r "^0.6") (f (quote ("json" "query" "headers"))) (o #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("std" "derive"))) (k 0)) (d (n "axum") (r "^0.6") (f (quote ("http1" "tokio"))) (k 2)) (d (n "isahc") (r "^1") (k 2)) (d (n "portpicker") (r "^0.1") (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (k 2)))) (h "01x7cbnn19qb7p6nmanp5xljxlxnlzhcvmf60lzl77fpxcn3lgl2") (f (quote (("impl_axum" "axum") ("default" "impl_axum"))))))

