(define-module (crates-io an si ansi_rgb) #:use-module (crates-io))

(define-public crate-ansi_rgb-0.1.0 (c (n "ansi_rgb") (v "0.1.0") (h "1yqbvijgf6il1jq9y38srn8v5wa2fxyb2ry32xdwrm1hdsw04509")))

(define-public crate-ansi_rgb-0.2.0 (c (n "ansi_rgb") (v "0.2.0") (d (list (d (n "rgb") (r "^0.8") (d #t) (k 0)))) (h "12mhz61dj3mzvssakhiaxyqijjldbjw091g6y6h45s2fn5g0jc57")))

(define-public crate-ansi_rgb-0.3.1 (c (n "ansi_rgb") (v "0.3.1") (d (list (d (n "rgb") (r "^0.8") (o #t) (k 0)))) (h "03ambi7qfb5b4ygr8pdxwljz0j2a50p7iwq3ss7i310dkaxwb4pn") (f (quote (("default" "rgb")))) (y #t)))

(define-public crate-ansi_rgb-0.3.1-alpha (c (n "ansi_rgb") (v "0.3.1-alpha") (d (list (d (n "rgb") (r "^0.8") (o #t) (k 0)))) (h "0pjhbcvs7fc8vb046b58diqgr9lkz30h5lv0nigs5wn8q2067gvh") (f (quote (("default" "rgb"))))))

(define-public crate-ansi_rgb-0.3.1-alpha2 (c (n "ansi_rgb") (v "0.3.1-alpha2") (d (list (d (n "rgb") (r "^0.8") (o #t) (k 0)))) (h "1czlfyafi9rbs9hm4wq8d2w1q2jy2cqmdzhqjfsjsxcr5kxg29ba") (f (quote (("default" "rgb"))))))

(define-public crate-ansi_rgb-0.3.1-alpha3 (c (n "ansi_rgb") (v "0.3.1-alpha3") (d (list (d (n "rgb") (r "^0.8") (o #t) (k 0)))) (h "1whan99m403kr2d3cc01jqr1bp8dsx61qlri8h8lg8498j9m8xw2") (f (quote (("default" "rgb"))))))

(define-public crate-ansi_rgb-0.3.2-alpha (c (n "ansi_rgb") (v "0.3.2-alpha") (d (list (d (n "rgb") (r "^0.8") (o #t) (k 0)))) (h "1nca0nv4mg5vh1bbznq5zpjpsrrym7ipcbv34jj4z0zlw7n1cacz") (f (quote (("default" "rgb"))))))

