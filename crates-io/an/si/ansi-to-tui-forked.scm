(define-module (crates-io an si ansi-to-tui-forked) #:use-module (crates-io))

(define-public crate-ansi-to-tui-forked-0.5.2-fix.offset (c (n "ansi-to-tui-forked") (v "0.5.2-fix.offset") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.2.1") (d #t) (k 2)) (d (n "simdutf8") (r "^0.1.4") (o #t) (d #t) (k 0)) (d (n "tui") (r "^0.18.0") (k 0)))) (h "1ki7yd1kpcrwh0wl16nrjrfs0hx6ys74pgvdhwm49xy1ysdfkrdn") (f (quote (("simd" "simdutf8"))))))

(define-public crate-ansi-to-tui-forked-3.0.0-ratatui (c (n "ansi-to-tui-forked") (v "3.0.0-ratatui") (d (list (d (n "nom") (r "^7.1") (d #t) (k 0)) (d (n "simdutf8") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tui") (r "0.*") (k 0) (p "ratatui")) (d (n "anyhow") (r "^1.0") (d #t) (k 2)))) (h "1cxshlzraxwsiw407phknsix80zfxjdaw4a14n18dbvzgav0ifgh") (f (quote (("simd" "simdutf8"))))))

