(define-module (crates-io an si ansitok) #:use-module (crates-io))

(define-public crate-ansitok-0.1.0 (c (n "ansitok") (v "0.1.0") (d (list (d (n "nom") (r "^7.1.1") (d #t) (k 0)))) (h "1cfmf55n8n5jmkkn6dl8r7j1ny3c5m2jdss8z4fzrn1rylqypin2") (f (quote (("std" "nom/std") ("default" "std"))))))

(define-public crate-ansitok-0.1.1 (c (n "ansitok") (v "0.1.1") (d (list (d (n "nom") (r "^7.1.1") (d #t) (k 0)) (d (n "vte") (r "^0.10.1") (d #t) (k 0)))) (h "02vf80v09xghf14k4h4irkl7s173fkns7331sfbvlgd9xid4fhjv") (f (quote (("std" "nom/std") ("default" "std")))) (y #t)))

(define-public crate-ansitok-0.2.0 (c (n "ansitok") (v "0.2.0") (d (list (d (n "nom") (r "^7.1.1") (d #t) (k 0)) (d (n "vte") (r "^0.10.1") (d #t) (k 0)))) (h "10vc2d1325qsbvbnqnj48zg55wv7jz929drx9vpdscdvl7k48012") (f (quote (("std" "nom/std") ("default" "std"))))))

