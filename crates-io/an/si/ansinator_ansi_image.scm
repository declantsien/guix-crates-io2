(define-module (crates-io an si ansinator_ansi_image) #:use-module (crates-io))

(define-public crate-ansinator_ansi_image-0.1.0 (c (n "ansinator_ansi_image") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "ansinator_ascii_font") (r "^0.1.0") (d #t) (k 0)) (d (n "ansinator_image_binarize") (r "^0.1.0") (d #t) (k 0)) (d (n "ansinator_image_window") (r "^0.1.0") (d #t) (k 0)) (d (n "ansinator_terminal_colors") (r "^0.1.0") (d #t) (k 0)) (d (n "image") (r "^0.24.3") (d #t) (k 0)) (d (n "terminal_size") (r "^0.2.1") (d #t) (k 0)))) (h "11bzcryvgjlnqx1waqcyvwn78j6qq8dfgzardnn7pwfyy4wax7wa")))

(define-public crate-ansinator_ansi_image-0.1.1 (c (n "ansinator_ansi_image") (v "0.1.1") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "ansinator_ascii_font") (r "^0.1.0") (d #t) (k 0)) (d (n "ansinator_image_binarize") (r "^0.1.0") (d #t) (k 0)) (d (n "ansinator_terminal_colors") (r "^0.1.0") (d #t) (k 0)) (d (n "image") (r "^0.24.3") (d #t) (k 0)) (d (n "terminal_size") (r "^0.2.1") (d #t) (k 0)))) (h "1rr5n12nh4rbfch8zwwya9523m0rzaicqds23jr91r5li53rimk7")))

(define-public crate-ansinator_ansi_image-0.1.2 (c (n "ansinator_ansi_image") (v "0.1.2") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "ansinator_ascii_font") (r "^0.1.0") (d #t) (k 0)) (d (n "ansinator_image_binarize") (r "^0.1.0") (d #t) (k 0)) (d (n "ansinator_terminal_colors") (r "^0.1.0") (d #t) (k 0)) (d (n "image") (r "^0.24.3") (d #t) (k 0)) (d (n "terminal_size") (r "^0.2.1") (d #t) (k 0)))) (h "16jv2hq8aiz692sm6cv35wh7r8c93l15y9q1bf8mcnrhl6xcsl3a")))

