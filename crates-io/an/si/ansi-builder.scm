(define-module (crates-io an si ansi-builder) #:use-module (crates-io))

(define-public crate-ansi-builder-0.1.0 (c (n "ansi-builder") (v "0.1.0") (h "1k2ra4sa3fjv0npqpgxhp4d6nv6smvskjgyxdxhd8ph1fr49fvhw")))

(define-public crate-ansi-builder-0.1.1 (c (n "ansi-builder") (v "0.1.1") (h "05dq46cgim2b69b51dnjxsgv0iwrsr0q8pkirjw97ygvjwbvv597")))

(define-public crate-ansi-builder-0.1.2 (c (n "ansi-builder") (v "0.1.2") (h "0i94v5m8mc44a81bi4g9hfjhzqg6dl1nq3gammv3xs7s6bfwvfp4")))

(define-public crate-ansi-builder-0.1.3 (c (n "ansi-builder") (v "0.1.3") (h "0s05vicgsmpc3x0gmsfc8mh3x25kmsxndzjf2id1jxqqly389rh8")))

(define-public crate-ansi-builder-0.1.4 (c (n "ansi-builder") (v "0.1.4") (h "0j8vya7gys8nyycjxwgxmmi77vd9jdgr4a2bs8y213r9675y20lb")))

(define-public crate-ansi-builder-0.1.5 (c (n "ansi-builder") (v "0.1.5") (d (list (d (n "windows") (r "^0.10.0") (d #t) (t "cfg(windows)") (k 0)) (d (n "windows") (r "^0.10.0") (d #t) (t "cfg(windows)") (k 1)))) (h "0250k0s70gylgv0b3ibsdr26ni4m0kmvayl0c8b071q0w8vpr008")))

(define-public crate-ansi-builder-0.1.6 (c (n "ansi-builder") (v "0.1.6") (d (list (d (n "windows") (r "^0.10.0") (d #t) (t "cfg(windows)") (k 0)) (d (n "windows") (r "^0.10.0") (d #t) (t "cfg(windows)") (k 1)))) (h "0pvr1xd7fkh325w91pkw1vz4479rp0w0mip35yk28w6fgxznzc0z")))

