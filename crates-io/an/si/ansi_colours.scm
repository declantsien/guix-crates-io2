(define-module (crates-io an si ansi_colours) #:use-module (crates-io))

(define-public crate-ansi_colours-1.0.0 (c (n "ansi_colours") (v "1.0.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "delta_e") (r "^0.2") (d #t) (k 2)) (d (n "lab") (r "^0.4") (d #t) (k 2)))) (h "02vbmn95z32ai1k1581rr3xb4kjyrhc4qj3vzh1k5qnmyyhdwv15")))

(define-public crate-ansi_colours-1.0.1 (c (n "ansi_colours") (v "1.0.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "delta_e") (r "^0.2") (d #t) (k 2)) (d (n "lab") (r "^0.4") (d #t) (k 2)))) (h "1dnqmpk68mzvncj37jlv0362kdgsgjxg010c6psagimgh4m303qx")))

(define-public crate-ansi_colours-1.0.2 (c (n "ansi_colours") (v "1.0.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "delta_e") (r "^0.2") (d #t) (k 2)) (d (n "lab") (r "^0.8") (d #t) (k 2)))) (h "1qawkpbcrbk168lgckyi0cdw31clw9h86dj0985nga5fhhxndjsj")))

(define-public crate-ansi_colours-1.0.3 (c (n "ansi_colours") (v "1.0.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "delta_e") (r "^0.2") (d #t) (k 2)) (d (n "lab") (r "0.*") (d #t) (k 2)))) (h "11n3q3pxx5axfa4jxsrk415n964a8bdwarhr68i833ffnwy8pdh6")))

(define-public crate-ansi_colours-1.0.4 (c (n "ansi_colours") (v "1.0.4") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "empfindung") (r "^0.2") (d #t) (k 2)) (d (n "lab") (r ">=0.4, <1.0") (d #t) (k 2)))) (h "13yjl5ywszm46nsc25xkvas64pnxrf68yv9wrgqxk6m471hzpqk0")))

(define-public crate-ansi_colours-1.1.0 (c (n "ansi_colours") (v "1.1.0") (d (list (d (n "crc64") (r "^1") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (k 2)) (d (n "empfindung") (r "^0.2") (d #t) (k 2)) (d (n "lab") (r ">=0.4, <1.0") (d #t) (k 2)) (d (n "rgb") (r "^0.8") (o #t) (d #t) (k 0)))) (h "0r1hw06j14pqq85k36aviwwlh7ii4dmahv5da5w9prsbc5lxyn8g") (f (quote (("default" "rgb"))))))

(define-public crate-ansi_colours-1.1.1 (c (n "ansi_colours") (v "1.1.1") (d (list (d (n "crc64") (r "^1") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (k 2)) (d (n "empfindung") (r "^0.2") (d #t) (k 2)) (d (n "lab") (r ">=0.4, <1.0") (d #t) (k 2)) (d (n "rgb") (r "^0.8") (o #t) (d #t) (k 0)))) (h "03b2365y0ffkvqw61bc4imz6661jvi39vcs4q6q5d43znqrq4rrj") (f (quote (("default" "rgb"))))))

(define-public crate-ansi_colours-1.2.0 (c (n "ansi_colours") (v "1.2.0") (d (list (d (n "ansi_term") (r "^0.12") (o #t) (d #t) (k 0)) (d (n "crc64") (r "^1") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (k 2)) (d (n "empfindung") (r "^0.2") (d #t) (k 2)) (d (n "lab") (r ">=0.4, <1.0") (d #t) (k 2)) (d (n "rgb") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "termcolor") (r "^1.1") (o #t) (d #t) (k 0)))) (h "1fpb4kh5s5vrgsxk2l25hdnqxg8m1azp655nnk7diqj5ik7iwrcf") (f (quote (("default" "rgb"))))))

(define-public crate-ansi_colours-1.2.1 (c (n "ansi_colours") (v "1.2.1") (d (list (d (n "ansi_term") (r "^0.12") (o #t) (d #t) (k 0)) (d (n "crc64") (r "^1") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (k 2)) (d (n "empfindung") (r "^0.2.6") (d #t) (k 2)) (d (n "lab") (r ">=0.4, <1.0") (d #t) (k 2)) (d (n "rgb") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "rgb") (r "^0.8") (d #t) (k 2)) (d (n "termcolor") (r "^1.1") (o #t) (d #t) (k 0)))) (h "07msyzh5qscrv7ll90l2jgr8i5skhbhnww9sjf1lywnygxvdkfbx") (f (quote (("default" "rgb"))))))

(define-public crate-ansi_colours-1.2.2 (c (n "ansi_colours") (v "1.2.2") (d (list (d (n "ansi_term") (r "^0.12") (o #t) (d #t) (k 0)) (d (n "crc64") (r "^2") (d #t) (k 2)) (d (n "criterion") (r "^0.5") (k 2)) (d (n "empfindung") (r "^0.2.6") (d #t) (k 2)) (d (n "lab") (r ">=0.4, <1.0") (d #t) (k 2)) (d (n "rgb") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "rgb") (r "^0.8") (d #t) (k 2)) (d (n "termcolor") (r ">=1.0, <=1.2") (o #t) (d #t) (k 0)))) (h "104aj4fi8nxdb9c5ahpwn53afmfcdzmwi3k9rawl3lvm42ymh5ba") (f (quote (("default" "rgb"))))))

