(define-module (crates-io an si ansiplay) #:use-module (crates-io))

(define-public crate-ansiplay-0.1.0 (c (n "ansiplay") (v "0.1.0") (d (list (d (n "basic_waves") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rodio") (r "^0.14") (d #t) (k 0)))) (h "1sy81fka139v3jp95c2lcdvday1x5xv4r2kqgkcaw0sbh53913yc")))

