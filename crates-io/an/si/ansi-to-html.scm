(define-module (crates-io an si ansi-to-html) #:use-module (crates-io))

(define-public crate-ansi-to-html-0.1.0 (c (n "ansi-to-html") (v "0.1.0") (d (list (d (n "once_cell") (r "^1.5") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1vjbjkhg52q0g6i7xnh1w2ag7jhfx3y49vxxgjhq3ca50pg85vhr") (f (quote (("lazy-init" "once_cell") ("default"))))))

(define-public crate-ansi-to-html-0.1.2 (c (n "ansi-to-html") (v "0.1.2") (d (list (d (n "once_cell") (r "^1.14.0") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.34") (d #t) (k 0)))) (h "18mw64bj55y67yklwyf1jdxsv6lwi7vvn0vy7iz32wp1wkk6hm93") (f (quote (("lazy-init" "once_cell") ("default"))))))

(define-public crate-ansi-to-html-0.1.3 (c (n "ansi-to-html") (v "0.1.3") (d (list (d (n "insta") (r "^1.29.0") (d #t) (k 2)) (d (n "once_cell") (r "^1.17.1") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1.7.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "18kwlgr3vfsij8gvl7vxw11yl628b1s8z2pldh73z4zzq2693gf7") (f (quote (("lazy-init" "once_cell") ("default"))))))

(define-public crate-ansi-to-html-0.2.1 (c (n "ansi-to-html") (v "0.2.1") (d (list (d (n "insta") (r "^1.29.0") (d #t) (k 2)) (d (n "once_cell") (r "^1.17.1") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1.7.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0pszk42nqdi36nakg5gpjzifj1fn1brqjiqiflx258lzw1d4ag6p") (f (quote (("lazy-init" "once_cell") ("default"))))))

