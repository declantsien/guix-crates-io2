(define-module (crates-io an si ansinator) #:use-module (crates-io))

(define-public crate-ansinator-0.1.0 (c (n "ansinator") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "clap") (r "^3.2.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "image") (r "^0.24.2") (d #t) (k 0)) (d (n "terminal_size") (r "^0.2.1") (d #t) (k 0)))) (h "1wj7sdgki769j3b94if706iph4l0gsl1gjnawfldmfqhv90y7kvh")))

(define-public crate-ansinator-0.1.1 (c (n "ansinator") (v "0.1.1") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "clap") (r "^3.2.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "image") (r "^0.24.2") (d #t) (k 0)) (d (n "terminal_size") (r "^0.2.1") (d #t) (k 0)))) (h "029kkw49n1lirbngl5x7r11xs83jiixd4k9fljj5m3zig8f890cs")))

(define-public crate-ansinator-0.2.0 (c (n "ansinator") (v "0.2.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "clap") (r "^3.2.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "image") (r "^0.24.2") (d #t) (k 0)) (d (n "terminal_size") (r "^0.2.1") (d #t) (k 0)))) (h "1yvln3xg87dhyfbcfwyslv6r38g6c56kd9dm65xl85pgs9w45cb1")))

(define-public crate-ansinator-0.2.1 (c (n "ansinator") (v "0.2.1") (d (list (d (n "ansinator_ansi_image") (r "^0.1.0") (d #t) (k 0)) (d (n "clap") (r "^3.2.20") (f (quote ("derive"))) (d #t) (k 0)))) (h "1nr9cmr5ndmgdc5w0sdbrlkn86xws64g4yb9ybp8dfmdb6yn8alp") (y #t)))

(define-public crate-ansinator-0.2.2 (c (n "ansinator") (v "0.2.2") (d (list (d (n "ansinator_ansi_image") (r "^0.1.0") (d #t) (k 0)) (d (n "clap") (r "^3.2.20") (f (quote ("derive"))) (d #t) (k 0)))) (h "0yfis10a0j52k75cdlpcm2afrsczvf4by2qm66wlz8vcirjd8bp6") (y #t)))

(define-public crate-ansinator-0.2.3 (c (n "ansinator") (v "0.2.3") (d (list (d (n "ansinator_ansi_image") (r "^0.1.2") (d #t) (k 0)) (d (n "clap") (r "^3.2.20") (f (quote ("derive"))) (d #t) (k 0)))) (h "0mj006zszv3r3dqwn985vavwg5qmzkddvjb2bys35i8p95w8bqbs")))

