(define-module (crates-io an si ansipix) #:use-module (crates-io))

(define-public crate-ansipix-0.1.0 (c (n "ansipix") (v "0.1.0") (d (list (d (n "image") (r "^0.23.14") (d #t) (k 0)))) (h "0q64ppasmmxr3jnvrv8hhx9nxl96856n5bzailr5p14c9ipwdcbl")))

(define-public crate-ansipix-0.1.1 (c (n "ansipix") (v "0.1.1") (d (list (d (n "image") (r "^0.23.14") (d #t) (k 0)))) (h "1kb55d2bzw178ln8qs20lcpnrsmk3042z91ygxk1z8cczfi0rmdq")))

(define-public crate-ansipix-0.1.2 (c (n "ansipix") (v "0.1.2") (d (list (d (n "image") (r "^0.23.14") (d #t) (k 0)))) (h "1yr7qvrgyg5cyrkszr1wa91mncrg665j9p3s30716q6lwqkkhlby")))

(define-public crate-ansipix-1.0.0 (c (n "ansipix") (v "1.0.0") (d (list (d (n "image") (r "^0.24.2") (d #t) (k 0)))) (h "1xh38vxznv83dprxb3l81h1rf5xggg66p7rk9ysg45hapn8nvck9")))

