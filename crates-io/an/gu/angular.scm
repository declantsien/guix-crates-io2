(define-module (crates-io an gu angular) #:use-module (crates-io))

(define-public crate-angular-0.1.0 (c (n "angular") (v "0.1.0") (d (list (d (n "hamcrest") (r "^0.1.0") (d #t) (k 2)) (d (n "num") (r "^0.1.27") (d #t) (k 0)) (d (n "quickcheck") (r "^0.2.21") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.2.21") (d #t) (k 2)))) (h "08rcc7vhhiqdn2kl8wc0sq74vsq02nlljm1kja6p4mdg4041y9b6")))

(define-public crate-angular-0.1.1 (c (n "angular") (v "0.1.1") (d (list (d (n "hamcrest") (r "^0.1.0") (d #t) (k 2)) (d (n "num") (r "^0.1.27") (d #t) (k 0)) (d (n "quickcheck") (r "^0.2.21") (d #t) (k 2)))) (h "0klvvk60rpbfrgg7s1ymjiq51nl6kf7g47y6y6hsxk4nbg5s5g9l")))

