(define-module (crates-io an gu angular-units) #:use-module (crates-io))

(define-public crate-angular-units-0.1.0 (c (n "angular-units") (v "0.1.0") (d (list (d (n "approx") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "0d812m8rm9i1claxhxjh84lxvfzj5hsq9ba3n9fd788w7mkwn1cy") (f (quote (("default" "approx"))))))

(define-public crate-angular-units-0.1.1 (c (n "angular-units") (v "0.1.1") (d (list (d (n "approx") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "012fny6986rswyldh6aqnyblwhkg02ag1avicd420bz17riq39l5") (f (quote (("default" "approx"))))))

(define-public crate-angular-units-0.1.2 (c (n "angular-units") (v "0.1.2") (d (list (d (n "approx") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "0qan090yh1rp8r2v68j0ypaifjaji0d92457f9w3yf1lb3d7nd3w") (f (quote (("default" "approx"))))))

(define-public crate-angular-units-0.1.3 (c (n "angular-units") (v "0.1.3") (d (list (d (n "approx") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "0mfyaab277hj79drybz70v3kawawifkaxbbfpgw49mvafd01x6in") (f (quote (("default" "approx"))))))

(define-public crate-angular-units-0.2.0 (c (n "angular-units") (v "0.2.0") (d (list (d (n "approx") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "1i8kvp6kqn5fmls6468zwpldiasg2x944jiz2g76dwzlnbhzk12c") (f (quote (("default" "approx"))))))

(define-public crate-angular-units-0.2.1 (c (n "angular-units") (v "0.2.1") (d (list (d (n "approx") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "13gmsc6gdsf76dwkylmg56ihdbr9bw1dfwqqikdvj2jjlicnzwdk") (f (quote (("default" "approx"))))))

(define-public crate-angular-units-0.2.2 (c (n "angular-units") (v "0.2.2") (d (list (d (n "approx") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "0jng9slk0hsds2pg9ldhws0p7rpc4c81aj6q38hlpm7a30ymvli9") (f (quote (("default" "approx"))))))

(define-public crate-angular-units-0.2.3 (c (n "angular-units") (v "0.2.3") (d (list (d (n "approx") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1rbday691p63a6lr9r4w17pn3jgix7m5bylsf5ayl2iyj22bsc1y") (f (quote (("default" "approx"))))))

(define-public crate-angular-units-0.2.4 (c (n "angular-units") (v "0.2.4") (d (list (d (n "approx") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "13arklqy4mj7jsba4ndk4azxfjdnqzyq126ki98shnmpwk6kc72g") (f (quote (("default" "approx"))))))

