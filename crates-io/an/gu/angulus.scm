(define-module (crates-io an gu angulus) #:use-module (crates-io))

(define-public crate-angulus-0.1.0 (c (n "angulus") (v "0.1.0") (d (list (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1b68vhdy75cn66fh8r4sy8xpbfi6d5rqxs9iqkfj090aa6fn2p7f")))

(define-public crate-angulus-0.2.0 (c (n "angulus") (v "0.2.0") (d (list (d (n "float_eq") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1064fs148wq28jk7qxa37z6sbmhjifiyky5gsz30v5lgm7q1rcif")))

(define-public crate-angulus-0.3.0 (c (n "angulus") (v "0.3.0") (d (list (d (n "float_eq") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1699al434p0palq7npfikaxfg4ays343v42ypsd21a0zjlf5n9ca") (r "1.61")))

(define-public crate-angulus-0.4.0 (c (n "angulus") (v "0.4.0") (d (list (d (n "float_eq") (r "^1") (d #t) (k 2)) (d (n "rand") (r "^0.8") (o #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1p5a2kvk633hgkaksv7shyd3fj0c6x4ii6lpi87vklzc5x18aadv") (y #t) (r "1.61")))

(define-public crate-angulus-0.4.1 (c (n "angulus") (v "0.4.1") (d (list (d (n "float_eq") (r "^1") (d #t) (k 2)) (d (n "rand") (r "^0.8") (o #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "0kb7ik5ls7zl6417spllrsm5c1d3w18srllwd30qnl5in6iscwlx") (r "1.61")))

(define-public crate-angulus-0.5.0 (c (n "angulus") (v "0.5.0") (d (list (d (n "float_eq") (r "^1") (d #t) (k 2)) (d (n "libm") (r "^0.2") (o #t) (k 0)) (d (n "rand") (r "^0.8") (o #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "00zybgbx341hiavf7kg0pr6hp0ybsbmw13dk4mr7y4lcg5vsn40a") (f (quote (("std") ("default" "std")))) (r "1.61")))

(define-public crate-angulus-0.6.0 (c (n "angulus") (v "0.6.0") (d (list (d (n "float_eq") (r "^1") (d #t) (k 2)) (d (n "libm") (r "^0.2") (o #t) (k 0)) (d (n "rand") (r "^0.8") (o #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "13rnvd4spq52myjli026lmiqc2aqlclzk9mnd7c06nkrarsfr2cb") (f (quote (("std") ("default" "std")))) (r "1.61")))

