(define-module (crates-io an al analyticord) #:use-module (crates-io))

(define-public crate-analyticord-0.1.0 (c (n "analyticord") (v "0.1.0") (d (list (d (n "hyper") (r "^0.11.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.7.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.11") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.11") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.2") (d #t) (k 0)) (d (n "serde_urlencoded") (r "^0.5.1") (d #t) (k 0)))) (h "1dcshl132isjbbyp811vin28bx83mzq2iib0c581cxpfg1pnqfch")))

(define-public crate-analyticord-0.0.2 (c (n "analyticord") (v "0.0.2") (d (list (d (n "derive-error") (r "^0.0.3") (d #t) (k 0)) (d (n "hyper") (r "^0.11.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.7.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.11") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.11") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.2") (d #t) (k 0)) (d (n "serde_urlencoded") (r "^0.5.1") (d #t) (k 0)))) (h "0jvbk38b99dygg6c4xx5inn6lmzjz2wbb4rpqz54vnvaxq7az8mk")))

(define-public crate-analyticord-0.1.1 (c (n "analyticord") (v "0.1.1") (d (list (d (n "derive-error") (r "^0.0.3") (d #t) (k 0)) (d (n "hyper") (r "^0.11.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.7.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.11") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.11") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.2") (d #t) (k 0)) (d (n "serde_urlencoded") (r "^0.5.1") (d #t) (k 0)))) (h "0sbcviw6zfb4wm529w2wxq5g6r4qpaff9lz5qmd7la25vqxag6sj")))

(define-public crate-analyticord-0.1.2 (c (n "analyticord") (v "0.1.2") (d (list (d (n "derive-error") (r "^0.0.3") (d #t) (k 0)) (d (n "hyper") (r "^0.11.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.7.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.11") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.11") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.2") (d #t) (k 0)) (d (n "serde_urlencoded") (r "^0.5.1") (d #t) (k 0)))) (h "0is5sw8vg25k367d7gav798gclrsibkr7l3g9rabv361bpqwbm5w")))

