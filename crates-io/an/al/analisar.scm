(define-module (crates-io an al analisar) #:use-module (crates-io))

(define-public crate-analisar-0.1.0 (c (n "analisar") (v "0.1.0") (d (list (d (n "bstr") (r "^0.2") (d #t) (k 0)) (d (n "lex_lua") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 0)))) (h "0gdhacnhi8hz3765w1f44hd3l8cqbxcwc8jvq93rx295xkgv6fhn")))

(define-public crate-analisar-0.2.0 (c (n "analisar") (v "0.2.0") (d (list (d (n "bstr") (r "^0.2") (d #t) (k 0)) (d (n "lex_lua") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 0)))) (h "0d4lnfl1bchyshv19ss0rz88nghs02dv91h86abbnnbl9gl8m2hs") (y #t)))

(define-public crate-analisar-0.2.1 (c (n "analisar") (v "0.2.1") (d (list (d (n "bstr") (r "^0.2") (d #t) (k 0)) (d (n "lex_lua") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 0)))) (h "1h7zpi5wjx3rs7k8ih3wck1zb119038al3psfmxpgiyxlkwwqzif")))

(define-public crate-analisar-0.3.0 (c (n "analisar") (v "0.3.0") (d (list (d (n "bstr") (r "^0.2") (d #t) (k 0)) (d (n "lex_lua") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.5") (d #t) (k 2)))) (h "02gfkv80di6zs7k1mj8ma1nx5i0qw33snl5fb2qhz8m08pya03yn")))

(define-public crate-analisar-0.4.0 (c (n "analisar") (v "0.4.0") (d (list (d (n "bstr") (r "^1.9") (d #t) (k 0)) (d (n "lex_lua") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.5") (d #t) (k 2)))) (h "1rv24iyq858k2v6zh9x3kdfnb8qqpwgq1hmi3qan817jh1bb62aq")))

