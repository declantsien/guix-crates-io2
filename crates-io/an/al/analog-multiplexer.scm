(define-module (crates-io an al analog-multiplexer) #:use-module (crates-io))

(define-public crate-analog-multiplexer-1.0.0 (c (n "analog-multiplexer") (v "1.0.0") (d (list (d (n "embedded-hal") (r "^0.2.4") (d #t) (k 0)))) (h "1gdq10gmg5sj4vcw7kjfx38rf32m49jf48mi8favij6ivndw25jb")))

(define-public crate-analog-multiplexer-1.0.1 (c (n "analog-multiplexer") (v "1.0.1") (d (list (d (n "embedded-hal") (r "^0.2.4") (d #t) (k 0)))) (h "0lgkajv40vfpgwrhhiqm03gw2bxld1ls8hg0pis5z1arcxcnn2p2")))

(define-public crate-analog-multiplexer-1.0.2 (c (n "analog-multiplexer") (v "1.0.2") (d (list (d (n "embedded-hal") (r "^0.2.4") (d #t) (k 0)))) (h "0na69pg83jazrmgrzingzalrdbifyvz1darxq3s5g4ggjm1gi7gi")))

