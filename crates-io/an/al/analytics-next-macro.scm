(define-module (crates-io an al analytics-next-macro) #:use-module (crates-io))

(define-public crate-analytics-next-macro-0.1.4 (c (n "analytics-next-macro") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^2.0.48") (d #t) (k 0)))) (h "1lk77himd97xszip4jzjg96nqifdcmqsx0awbwa3sr5jf8842wy9")))

(define-public crate-analytics-next-macro-0.1.6 (c (n "analytics-next-macro") (v "0.1.6") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^2.0.48") (d #t) (k 0)))) (h "00ngim65j1carw7665v9sx0kjy45yh4sdwpvp842a7p8fjbl2468")))

