(define-module (crates-io an al analytics) #:use-module (crates-io))

(define-public crate-analytics-0.1.0 (c (n "analytics") (v "0.1.0") (h "0bcfxp6hlpbw6f4l67csxcqgj861821pymm8y0jkzsk908dj3jqv")))

(define-public crate-analytics-0.2.0 (c (n "analytics") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.6") (f (quote ("serde"))) (d #t) (k 0)) (d (n "clap") (r "^2.33") (o #t) (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0.93") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)))) (h "1rfgwl3b3x2941l9c32mh1cnl03v7p20sx7a8qgqwqm94bvjsgi7") (f (quote (("cli" "clap"))))))

