(define-module (crates-io an al analytics-rs) #:use-module (crates-io))

(define-public crate-analytics-rs-1.0.0 (c (n "analytics-rs") (v "1.0.0") (d (list (d (n "prost") (r "^0.11.6") (d #t) (k 0)) (d (n "prost-types") (r "^0.11.6") (d #t) (k 0)) (d (n "tonic") (r "^0.8.3") (d #t) (k 0)) (d (n "tonic-build") (r "^0.8.4") (d #t) (k 1)))) (h "0ybhfyacqxn1qf0i4nzj5572ifrxyhj3q5sbmrnljx39khj60br5")))

(define-public crate-analytics-rs-1.0.1 (c (n "analytics-rs") (v "1.0.1") (d (list (d (n "prost") (r "^0.11.6") (d #t) (k 0)) (d (n "prost-types") (r "^0.11.6") (d #t) (k 0)) (d (n "tonic") (r "^0.8.3") (d #t) (k 0)) (d (n "tonic-build") (r "^0.8.4") (d #t) (k 1)))) (h "0mxp79maag1zamgwdcgkbqbxcqffp1kwvvp537jyzvwc6dhv14ci")))

