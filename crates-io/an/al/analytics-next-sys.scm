(define-module (crates-io an al analytics-next-sys) #:use-module (crates-io))

(define-public crate-analytics-next-sys-0.1.0-alpha.1 (c (n "analytics-next-sys") (v "0.1.0-alpha.1") (d (list (d (n "js-sys") (r "^0.3") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "wasm-bindgen-futures") (r "^0.4") (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("Node"))) (d #t) (k 0)))) (h "0sjanvlzzvqabh6c625f65rjz8r5hzm1jbaga9lsc9g9qagd8ikv")))

(define-public crate-analytics-next-sys-0.1.0 (c (n "analytics-next-sys") (v "0.1.0") (d (list (d (n "js-sys") (r "^0.3") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "wasm-bindgen-futures") (r "^0.4") (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("Node"))) (d #t) (k 0)))) (h "1946sawpsrg7257dy6sv4vb0226747ma4a9hqc6x38q6sc29mdnw")))

(define-public crate-analytics-next-sys-0.1.1 (c (n "analytics-next-sys") (v "0.1.1") (d (list (d (n "js-sys") (r "^0.3") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "wasm-bindgen-futures") (r "^0.4") (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("Node"))) (d #t) (k 0)))) (h "1gj3ad9v50j1zqdpzrkj6944ahikpsmdarxrbww3xz8frlix5fk7")))

(define-public crate-analytics-next-sys-0.1.2 (c (n "analytics-next-sys") (v "0.1.2") (d (list (d (n "js-sys") (r "^0.3") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "wasm-bindgen-futures") (r "^0.4") (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("Node"))) (d #t) (k 0)))) (h "15jpc214i3rmyxdcjmpsrlk7fqd992d3c8zv5i8pn52q3grn7d7k")))

(define-public crate-analytics-next-sys-0.1.3 (c (n "analytics-next-sys") (v "0.1.3") (d (list (d (n "js-sys") (r "^0.3") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "wasm-bindgen-futures") (r "^0.4") (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("Node"))) (d #t) (k 0)))) (h "1yfb076zjixvk7lvbxmkc2y0psf9sms63kv0cjmwvcwphkwyw8fj")))

(define-public crate-analytics-next-sys-0.1.4 (c (n "analytics-next-sys") (v "0.1.4") (d (list (d (n "js-sys") (r "^0.3") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "wasm-bindgen-futures") (r "^0.4") (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("Node"))) (d #t) (k 0)))) (h "1580laq61fajbak8g4s42smsrv5706ild73sg4prr0vx9m8ia19j")))

(define-public crate-analytics-next-sys-0.1.6 (c (n "analytics-next-sys") (v "0.1.6") (d (list (d (n "js-sys") (r "^0.3") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "wasm-bindgen-futures") (r "^0.4") (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("Node"))) (d #t) (k 0)))) (h "0fs439p7npzdnq7qswliwpdxbg0xfvcsrzxrv9f3l0jdziwfbain")))

