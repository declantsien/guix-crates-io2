(define-module (crates-io an al analog) #:use-module (crates-io))

(define-public crate-analog-0.0.0 (c (n "analog") (v "0.0.0") (h "1z7cl9yl001pvxha82nw94blhlsrfqaiapp7rfc55v165axwgsh1") (y #t)))

(define-public crate-analog-0.0.1 (c (n "analog") (v "0.0.1") (h "1fx9hdzc2k5aw5dcmlkbnk80mvrn50q2f6lbvk1nsx0cz86nisf0") (y #t)))

(define-public crate-analog-0.0.2 (c (n "analog") (v "0.0.2") (h "1bs37qf8cjgydkvl4zx2iq0gsfg43nykp6475cdhj2kr8832hpz2") (y #t)))

(define-public crate-analog-0.0.3 (c (n "analog") (v "0.0.3") (h "1naslp2kipmwh7agf043npgfbicw2k28zx0c2w6c25qzfw1491s5") (y #t)))

(define-public crate-analog-0.0.4 (c (n "analog") (v "0.0.4") (h "06rcrxk7b2wic1dyjbg9w8j5bpcbr73c38gdsfk50fr9hnnbkgjz") (y #t)))

