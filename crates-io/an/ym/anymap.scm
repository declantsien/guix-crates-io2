(define-module (crates-io an ym anymap) #:use-module (crates-io))

(define-public crate-anymap-0.9.0 (c (n "anymap") (v "0.9.0") (h "15jfxx61vgmr7j7z1hni5lbay09iq2d328mnssg8fabpnpsicavw")))

(define-public crate-anymap-0.9.1 (c (n "anymap") (v "0.9.1") (h "1lmvy0awljm9mci71i0nxpask7kjk6gbb2fnadw1spgc2i6ilhwq")))

(define-public crate-anymap-0.9.2 (c (n "anymap") (v "0.9.2") (h "1fmf4p8x7nl72l8y1zx4iihcjjhw2jfca6z22769qw2lhzixn25c")))

(define-public crate-anymap-0.9.3 (c (n "anymap") (v "0.9.3") (h "00df537qd7wgni8gxdadpa1l0glhwh418ph31va5iv02w844n0hm")))

(define-public crate-anymap-0.9.4 (c (n "anymap") (v "0.9.4") (h "166mjr3249hawwsjrkcis02frmj0p8344ic54rypmhjal20zh5mh")))

(define-public crate-anymap-0.9.5 (c (n "anymap") (v "0.9.5") (h "1vjj1z745ifnswqir3khiwjikmpw50i4ph8nxyq11g6n8357gp88") (y #t)))

(define-public crate-anymap-0.9.6 (c (n "anymap") (v "0.9.6") (h "0jxlb4yl8y2ymq6cwszpa2p0yba38x70hhxxch2d9z41fiwqrbh3")))

(define-public crate-anymap-0.9.7 (c (n "anymap") (v "0.9.7") (h "07man7lrgdc4k15brk4pa887b7c54rjkinhi7p7j4i75lp8ih4br")))

(define-public crate-anymap-0.9.8 (c (n "anymap") (v "0.9.8") (h "1ixi20wicf0dlgfinlzc8hyid9ii62bf51bx1hwd8jw1xnv5dap6")))

(define-public crate-anymap-0.9.9 (c (n "anymap") (v "0.9.9") (h "1b5a8lp7qhsnqsn9cq6bxf4c7zzsayki957dsqiv9pakllqksc5w")))

(define-public crate-anymap-0.9.10 (c (n "anymap") (v "0.9.10") (h "134prbf7asqpicgyy3gw5qip23xv38a0iwi8vgg25cm1v52zd9l3")))

(define-public crate-anymap-0.9.11 (c (n "anymap") (v "0.9.11") (h "05fd1d837fyiamkhy5sdpx8i8gkdfdx7qmv9wxm5lniwijd4xns4")))

(define-public crate-anymap-0.9.12 (c (n "anymap") (v "0.9.12") (h "1g67pr37rblxs6vk4bn62g5s1g6bbi9bifndb7vmz7iy9mxqsm3c")))

(define-public crate-anymap-0.9.13 (c (n "anymap") (v "0.9.13") (h "0z4p2jylcw3lbrf3gx6sb7kqr870m0fgpgzap1b0vfc2ghm458wq")))

(define-public crate-anymap-0.10.0 (c (n "anymap") (v "0.10.0") (h "17i2f1yliil33f5j7520jk8j3kfnjfx83ysx378jx3hqgdb9s266") (f (quote (("clone"))))))

(define-public crate-anymap-0.10.2 (c (n "anymap") (v "0.10.2") (h "1snq3m5vmka50gzysldqm45h40vla1z5yndzbvgdh6bgbc2gx4pv") (f (quote (("clone"))))))

(define-public crate-anymap-0.10.3 (c (n "anymap") (v "0.10.3") (h "0mm8fcnwi0pdq8g3zafv20r3h5hm5iipy6qx71nrm4s5bkfyxlbg") (f (quote (("nightly") ("clone"))))))

(define-public crate-anymap-0.11.0 (c (n "anymap") (v "0.11.0") (h "1q86p7hzxnp7is9zw3sh0dqwsgghgzijcizh1ch433mgk97af7fs") (f (quote (("unstable"))))))

(define-public crate-anymap-0.11.1 (c (n "anymap") (v "0.11.1") (h "10agz8rzm9b3f9x1b3yrl7k5jz4pplsvfj2z3i0iga64bilj8zml") (f (quote (("unstable"))))))

(define-public crate-anymap-0.11.2 (c (n "anymap") (v "0.11.2") (h "0yxkm6pzg8m2s29k0gw00h42maqq0h68cifwd5bz3bmflm522j31") (f (quote (("unstable"))))))

(define-public crate-anymap-0.12.0 (c (n "anymap") (v "0.12.0") (h "048llfh86kilxa2vk8xw1gjj6pkd21xmvc070j9lghs8s5xl40n7") (f (quote (("bench"))))))

(define-public crate-anymap-0.12.1 (c (n "anymap") (v "0.12.1") (h "0i23vv38i22aazp5z2hz0a44b943b6w5121kwwnpq1brpm1l559k") (f (quote (("bench"))))))

(define-public crate-anymap-1.0.0-beta.1 (c (n "anymap") (v "1.0.0-beta.1") (d (list (d (n "hashbrown") (r ">=0.1.1, <0.13") (o #t) (d #t) (k 0)))) (h "0amxwizzny9lhmm53dcz8snxrpckc817myjcii3hrvz53x6wvl0x") (f (quote (("std") ("default" "std")))) (r "1.36")))

(define-public crate-anymap-1.0.0-beta.2 (c (n "anymap") (v "1.0.0-beta.2") (d (list (d (n "hashbrown") (r ">=0.1.1, <0.13") (o #t) (d #t) (k 0)))) (h "0whxr4r34rzvw5xwr80clamb4szra0x5kmwp6ygdhl1xdxd8y7wg") (f (quote (("std") ("default" "std")))) (r "1.36")))

