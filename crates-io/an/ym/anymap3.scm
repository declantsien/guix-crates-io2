(define-module (crates-io an ym anymap3) #:use-module (crates-io))

(define-public crate-anymap3-1.0.0 (c (n "anymap3") (v "1.0.0") (d (list (d (n "hashbrown") (r ">=0.1.1, <0.13") (o #t) (d #t) (k 0)))) (h "09vwpspwx9bx1513n31zic8szinkqnqkyk209vahn9s7z05ia5lk") (f (quote (("std") ("default" "std")))) (r "1.36")))

