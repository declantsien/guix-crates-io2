(define-module (crates-io an ym anym) #:use-module (crates-io))

(define-public crate-anym-0.0.0 (c (n "anym") (v "0.0.0") (d (list (d (n "nanoid") (r "^0.4.0") (d #t) (k 0)) (d (n "numtoa") (r "^0.2.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0b949j0mhd6q2929yyfbb4cx0wrxq0h3a3i013yhzy6zywx4lapi")))

