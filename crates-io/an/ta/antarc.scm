(define-module (crates-io an ta antarc) #:use-module (crates-io))

(define-public crate-antarc-0.1.0 (c (n "antarc") (v "0.1.0") (d (list (d (n "antarc-protocol") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "089hdzmjckifg4r8w7lj3rfxkkncy9r9hax4bh0s9cb0mq3kga5r")))

