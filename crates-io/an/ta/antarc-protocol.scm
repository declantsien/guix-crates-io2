(define-module (crates-io an ta antarc-protocol) #:use-module (crates-io))

(define-public crate-antarc-protocol-0.1.0 (c (n "antarc-protocol") (v "0.1.0") (d (list (d (n "crc32fast") (r "^1.2") (f (quote ("std" "nightly"))) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1hckvs4y2vwjw93k92mmbvy84ppnq5ki6y6wb94sv5j5kwsnscsk")))

