(define-module (crates-io an yw anywhere) #:use-module (crates-io))

(define-public crate-anywhere-0.0.0 (c (n "anywhere") (v "0.0.0") (h "08yyw3wzygywhirdsjj570a18mp3yrrvf4bribmmmlizyrb9wqq5")))

(define-public crate-anywhere-0.0.1 (c (n "anywhere") (v "0.0.1") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "dashmap") (r "^5.4.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "lunchbox") (r "^0.1") (f (quote ("serde"))) (k 0)) (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "pin-project") (r "^1") (d #t) (k 0)) (d (n "relative-path") (r "^1.7") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("io-util" "sync" "rt"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "08pa3p4khpc00nbayj8wicvr35jfm4kq24wdaf95qyx4ywawnyvs") (f (quote (("tcp" "tokio/net"))))))

