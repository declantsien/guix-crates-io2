(define-module (crates-io an yw anyways) #:use-module (crates-io))

(define-public crate-anyways-0.1.0 (c (n "anyways") (v "0.1.0") (d (list (d (n "backtrace") (r "^0.3.65") (d #t) (k 0)) (d (n "owo-colors") (r "^3.4.0") (d #t) (k 0)))) (h "10jhfyp6jk2jmcl8jacnsdr2zkw9gs71q4bazidmd4bf5150illl")))

(define-public crate-anyways-0.1.1 (c (n "anyways") (v "0.1.1") (d (list (d (n "backtrace") (r "^0.3.65") (d #t) (k 0)) (d (n "owo-colors") (r "^3.4.0") (d #t) (k 0)))) (h "0jj3lf1n3bfi2lki6jnlplhlsna7ac5lnn19xwfpzivcf4bk1v3h")))

(define-public crate-anyways-0.1.2 (c (n "anyways") (v "0.1.2") (d (list (d (n "backtrace") (r "^0.3.65") (d #t) (k 0)) (d (n "owo-colors") (r "^3.4.0") (d #t) (k 0)))) (h "14qik7fzkkl1lngga18n257rnmj836w5dm6sv8k9pnlaqdpqr4lj")))

(define-public crate-anyways-0.1.3 (c (n "anyways") (v "0.1.3") (d (list (d (n "backtrace") (r "^0.3.65") (d #t) (k 0)) (d (n "owo-colors") (r "^3.4.0") (d #t) (k 0)))) (h "1hgwvgp8fc1xhx7q0iqmm4q0x35fhh00r06pkz0arpp03bh6f263")))

(define-public crate-anyways-0.1.4 (c (n "anyways") (v "0.1.4") (d (list (d (n "backtrace") (r "^0.3.65") (d #t) (k 0)) (d (n "owo-colors") (r "^3.4.0") (d #t) (k 0)))) (h "1sh9dishz6ymsb23rja1hp5w0ajg6rj346nyskfgp126vijn8i73")))

(define-public crate-anyways-0.2.0 (c (n "anyways") (v "0.2.0") (d (list (d (n "backtrace") (r "^0.3.65") (d #t) (k 0)) (d (n "owo-colors") (r "^3.4.0") (d #t) (k 0)))) (h "1pwddmabbdk7pfpirl3y6f2lvidj6gi3kcy8f6qbcn87pg1abj9y")))

(define-public crate-anyways-0.3.0 (c (n "anyways") (v "0.3.0") (d (list (d (n "backtrace") (r "^0.3.65") (d #t) (k 0)) (d (n "owo-colors") (r "^3.4.0") (d #t) (k 0)))) (h "1ik4q4hg0zfskzrg054118whrwhaara9fcs5z0415rqaqf3mb9sl") (f (quote (("sync") ("send"))))))

