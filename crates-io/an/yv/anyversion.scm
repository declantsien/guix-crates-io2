(define-module (crates-io an yv anyversion) #:use-module (crates-io))

(define-public crate-anyversion-0.1.0 (c (n "anyversion") (v "0.1.0") (h "1fvcd17cp60g3xskfwi0mcddjkvy75ybxfs9zva2yhw9azb0ns9p")))

(define-public crate-anyversion-0.2.0 (c (n "anyversion") (v "0.2.0") (h "1n3ij001chvjpknv2kswng06sf5qva50lkm8164g8qbs4fbrwzbz")))

