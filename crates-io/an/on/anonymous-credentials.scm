(define-module (crates-io an on anonymous-credentials) #:use-module (crates-io))

(define-public crate-anonymous-credentials-0.1.0 (c (n "anonymous-credentials") (v "0.1.0") (d (list (d (n "brave-miracl") (r "^0.1") (f (quote ("std"))) (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0p0wn1j4g6a92dpwjh7mkc101is5qnyd4g29sak8p52b80nqjmh8")))

(define-public crate-anonymous-credentials-0.1.1 (c (n "anonymous-credentials") (v "0.1.1") (d (list (d (n "brave-miracl") (r "^0.1") (f (quote ("std"))) (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0dnxiwjm4sig1mz3gfg0w3n83cbn2c96vl7afpd6am6pf32mnz9d")))

(define-public crate-anonymous-credentials-0.1.2 (c (n "anonymous-credentials") (v "0.1.2") (d (list (d (n "brave-miracl") (r "^0.1") (f (quote ("std"))) (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "08vzszkn7h6pr0xnkjjlbn0sm3whfcjgxlab9hm3zvkag6kd698z")))

