(define-module (crates-io an on anon-vec) #:use-module (crates-io))

(define-public crate-anon-vec-0.1.0 (c (n "anon-vec") (v "0.1.0") (h "1amfxqjg0v7ci52ccxp1jd9aaxsz2szcz1z6b0n88q2h2vga7hc5")))

(define-public crate-anon-vec-0.1.1 (c (n "anon-vec") (v "0.1.1") (h "0kd8iwx5z8xc7bph9faynqbx1lmaxzd7vpasnb95bcb7dw0bsa89")))

