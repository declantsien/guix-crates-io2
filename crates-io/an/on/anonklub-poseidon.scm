(define-module (crates-io an on anonklub-poseidon) #:use-module (crates-io))

(define-public crate-anonklub-poseidon-0.1.0 (c (n "anonklub-poseidon") (v "0.1.0") (d (list (d (n "ark-ff") (r "^0.4.2") (d #t) (k 0)) (d (n "ark-secp256k1") (r "^0.4.0") (d #t) (k 2)) (d (n "ark-serialize") (r "^0.4.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tiny-keccak") (r "^2.0.2") (f (quote ("keccak"))) (d #t) (k 0)))) (h "17jbbcjl0iwlmfx99p0djl5jwklk1sh4zhwpshsk18mmgjcxzcdy")))

(define-public crate-anonklub-poseidon-1.0.0 (c (n "anonklub-poseidon") (v "1.0.0") (d (list (d (n "ark-ff") (r "^0.4.2") (d #t) (k 0)) (d (n "ark-secp256k1") (r "^0.4.0") (d #t) (k 2)) (d (n "ark-serialize") (r "^0.4.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tiny-keccak") (r "^2.0.2") (f (quote ("keccak"))) (d #t) (k 0)))) (h "0hlghqs3hz9x3x9s2qca2w4l712wl71p4dvv5r7yasflysvjavln")))

