(define-module (crates-io an on anon-csv-cli) #:use-module (crates-io))

(define-public crate-anon-csv-cli-1.0.0 (c (n "anon-csv-cli") (v "1.0.0") (d (list (d (n "csv") (r "^1.0.5") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "failure-tools") (r "^4.0.2") (d #t) (k 0)) (d (n "fake") (r "^1.2.2") (d #t) (k 0)) (d (n "structopt") (r "^0.2.14") (d #t) (k 0)) (d (n "strum") (r "^0.14.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.14.0") (d #t) (k 0)))) (h "1bnm0r83gpa4cwfmmq2dh8ysjxp23qxyikzp49jcppk2q7y9vlgx")))

(define-public crate-anon-csv-cli-1.0.1 (c (n "anon-csv-cli") (v "1.0.1") (d (list (d (n "csv") (r "^1.0.5") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "failure-tools") (r "^4.0.2") (d #t) (k 0)) (d (n "fake") (r "^1.2.2") (d #t) (k 0)) (d (n "structopt") (r "^0.2.14") (d #t) (k 0)) (d (n "strum") (r "^0.14.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.14.0") (d #t) (k 0)))) (h "1gk5fhxd1clcw5xjm78gsfv7dwhfzr87sid3nxybjarsh27jsy8r")))

(define-public crate-anon-csv-cli-1.0.2 (c (n "anon-csv-cli") (v "1.0.2") (d (list (d (n "csv") (r "^1.1.3") (d #t) (k 0)) (d (n "failure") (r "^0.1.6") (d #t) (k 0)) (d (n "failure-tools") (r "^4.0.3") (d #t) (k 0)) (d (n "fake") (r "^1.3.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.9") (d #t) (k 0)) (d (n "strum") (r "^0.17.1") (d #t) (k 0)) (d (n "strum_macros") (r "^0.17.1") (d #t) (k 0)))) (h "1x05ig0d124xnb4587dx1kfqmkgkkwkjxv3j4hjcqgw0a3mwv8w0")))

(define-public crate-anon-csv-cli-1.0.3 (c (n "anon-csv-cli") (v "1.0.3") (d (list (d (n "csv") (r "^1.1.3") (d #t) (k 0)) (d (n "failure") (r "^0.1.6") (d #t) (k 0)) (d (n "failure-tools") (r "^4.0.3") (d #t) (k 0)) (d (n "fake") (r "^1.3.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.9") (d #t) (k 0)) (d (n "strum") (r "^0.17.1") (d #t) (k 0)) (d (n "strum_macros") (r "^0.17.1") (d #t) (k 0)))) (h "1acv149d2ykfmm9y70ylmyl8mb0cjyvp1pvmi08m9flchjpmmwsf")))

(define-public crate-anon-csv-cli-1.0.4 (c (n "anon-csv-cli") (v "1.0.4") (d (list (d (n "csv") (r "^1.1.3") (d #t) (k 0)) (d (n "failure") (r "^0.1.6") (d #t) (k 0)) (d (n "failure-tools") (r "^4.0.3") (d #t) (k 0)) (d (n "fake") (r "^1.3.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.9") (d #t) (k 0)) (d (n "strum") (r "^0.18.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.17.1") (d #t) (k 0)))) (h "01jxrkhqvaclqynan24n6i0rqaf7g74ybpmq9qamw2fjqlkry8d3")))

