(define-module (crates-io an on anonymous-trait) #:use-module (crates-io))

(define-public crate-anonymous-trait-0.1.0 (c (n "anonymous-trait") (v "0.1.0") (d (list (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "02zdd0b7lkx6mcpiaf2vxm28bf7jsdrhr280v4dw87rdqil8gy5v")))

(define-public crate-anonymous-trait-0.1.1 (c (n "anonymous-trait") (v "0.1.1") (d (list (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt"))) (d #t) (k 2)))) (h "0rvkcr9n0fv48dj88lcc6grqrdqw856b9ig4mgi3sjw01lkx1fpn")))

(define-public crate-anonymous-trait-0.1.2 (c (n "anonymous-trait") (v "0.1.2") (d (list (d (n "mockall") (r "^0.12") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt"))) (d #t) (k 2)))) (h "0hg7dr7qg8ncqy18jj3l5nm9wlxr9gnhyk04y4rnqvs31rwaqi67")))

(define-public crate-anonymous-trait-0.1.3 (c (n "anonymous-trait") (v "0.1.3") (d (list (d (n "mockall") (r "^0.12") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt"))) (d #t) (k 2)))) (h "015cmch58xf3c0v9mcii3yml0fshb24x4qxrpp65x9258d7w922i")))

