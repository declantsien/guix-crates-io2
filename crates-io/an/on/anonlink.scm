(define-module (crates-io an on anonlink) #:use-module (crates-io))

(define-public crate-anonlink-0.1.0 (c (n "anonlink") (v "0.1.0") (d (list (d (n "iced-x86") (r "^1.17.0") (d #t) (k 0)) (d (n "pdb") (r "^0.8.0") (d #t) (k 0)) (d (n "pelite") (r "^0.9.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "ureq") (r "^2.5.0") (d #t) (k 0)))) (h "0rxjna44dwgifg5m6150k1j590vxczkbl8wgby78i01fkhjmsc3p")))

(define-public crate-anonlink-0.1.1 (c (n "anonlink") (v "0.1.1") (d (list (d (n "iced-x86") (r "^1.18.0") (d #t) (k 0)) (d (n "pdb") (r "^0.8.0") (d #t) (k 0)) (d (n "pelite") (r "^0.10.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "ureq") (r "^2.6.2") (d #t) (k 0)))) (h "12apy7nr95i48h093v86nnkqh4fdc10fpkx4lwzmcd35wf2x6yhq")))

