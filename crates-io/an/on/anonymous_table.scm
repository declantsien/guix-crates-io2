(define-module (crates-io an on anonymous_table) #:use-module (crates-io))

(define-public crate-anonymous_table-0.1.0 (c (n "anonymous_table") (v "0.1.0") (h "0v7bb2f73m27pa3rmar5x7hz07vrp7ql743p4bkvi7fl2dihpcic")))

(define-public crate-anonymous_table-0.1.1 (c (n "anonymous_table") (v "0.1.1") (h "1dc6s4g1v9i9fjrr8nf1ri48an0851244qyw63aw7hqp34cdipjq")))

(define-public crate-anonymous_table-0.2.0 (c (n "anonymous_table") (v "0.2.0") (h "04yb0k38fmh6syiyv802fp0z3q727lif49nmxjapcvxpqf8iaqpq")))

(define-public crate-anonymous_table-0.2.1 (c (n "anonymous_table") (v "0.2.1") (h "0j4v6l42ilm4dmqznpjlib18yzjvbxv1p1y1ynwjbafd75hz0fra")))

(define-public crate-anonymous_table-0.2.2 (c (n "anonymous_table") (v "0.2.2") (h "1rh9sgzimynbvf30hl5m0k1xkkqlss04yq9pp9ql6idb9f5qv7mr")))

(define-public crate-anonymous_table-0.2.3 (c (n "anonymous_table") (v "0.2.3") (h "0fary18k5la9mrbj1sk2x01zx0svr94cgikx4z3y68ylb09m07k9")))

(define-public crate-anonymous_table-0.2.4 (c (n "anonymous_table") (v "0.2.4") (h "0alp1hj66vl4jr50nbi09c75b71f0xbz4fc0xy2sc1aplvdpbl71")))

(define-public crate-anonymous_table-0.3.0 (c (n "anonymous_table") (v "0.3.0") (h "10ksbzf9rwxjlmfa8iix0b3661qj18gpryxg69amx2iy8r0ljnbz")))

(define-public crate-anonymous_table-0.3.1 (c (n "anonymous_table") (v "0.3.1") (h "1anzxf4whmcbpggdqa6xb9xdl32zvy9j15fwf944ir22w7cfihz1")))

(define-public crate-anonymous_table-0.3.2 (c (n "anonymous_table") (v "0.3.2") (h "0l3hijprlzj8jwsdi7qvxc1lcbn07yrqpxk6sc36ampxkykk76bb")))

(define-public crate-anonymous_table-0.3.3 (c (n "anonymous_table") (v "0.3.3") (h "0jlsgh9bj7lrm2i7nrc4f44d295gbs5f2k464nfhwxnlkmg15k4v")))

