(define-module (crates-io an on anonbattle) #:use-module (crates-io))

(define-public crate-anonbattle-0.1.0 (c (n "anonbattle") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.60") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "05jplfcs3zwsc1xxxid9nbarzcajbrykxxiv368hr7p41ddh7vba")))

(define-public crate-anonbattle-0.1.1 (c (n "anonbattle") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.60") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "1ak7nckcc5x8cyzvk4vja48lgi3v9m4brmzrf7wbsrids32isswv")))

(define-public crate-anonbattle-0.1.2 (c (n "anonbattle") (v "0.1.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.60") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "03wkj2sk3lkzcpin47ph0b5kg1vrmjq412fwinrvsr3480hv3795")))

(define-public crate-anonbattle-0.1.3 (c (n "anonbattle") (v "0.1.3") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.60") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "071zf9wwn5sc94gkls6h8nfvfj32wdyrqxz4ki5v8xi978b0gzv9")))

(define-public crate-anonbattle-0.1.4 (c (n "anonbattle") (v "0.1.4") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.60") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "1mkvzkmcn2mfff6pvhx8gzhd1p1m0v2r5x8k1p8rm26q3zlfd5jv")))

(define-public crate-anonbattle-0.1.5 (c (n "anonbattle") (v "0.1.5") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.60") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "0hmcwzids7g6bi23clpxzsc4vdmmbla7s34iaj41n9x6ipzl2ymh")))

