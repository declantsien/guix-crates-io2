(define-module (crates-io an yd anydate) #:use-module (crates-io))

(define-public crate-anydate-0.1.0 (c (n "anydate") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("std"))) (k 0)) (d (n "criterion") (r "^0.3.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1m2wzmd14j7wjca6cn0fp7qx14apbk9sfp5n3s670sysi5rhmd9w")))

(define-public crate-anydate-0.2.0 (c (n "anydate") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("std"))) (k 0)) (d (n "criterion") (r "^0.3.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0laxk6607rjh622pjdyyl1w7x60yhzjxzszfaj03yqqxphsk287g")))

(define-public crate-anydate-0.3.0 (c (n "anydate") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("std"))) (k 0)) (d (n "criterion") (r "^0.3.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.70") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1d8xqaibyv668hgwq0nl3f7bkx2245fvvkxzx8x2kaac95aiqfkw") (f (quote (("default"))))))

(define-public crate-anydate-0.4.0 (c (n "anydate") (v "0.4.0") (d (list (d (n "chrono") (r "^0.4.31") (f (quote ("std"))) (k 0)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "18xw9pfq98cis4nz8hsllhkk9d5643bv6m4dx1m2nil0hjcr4nmb") (f (quote (("default"))))))

