(define-module (crates-io an _l an_logger) #:use-module (crates-io))

(define-public crate-an_logger-0.1.0 (c (n "an_logger") (v "0.1.0") (d (list (d (n "android_logger") (r "^0.11.1") (d #t) (k 0)) (d (n "jni") (r "^0.19.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.84") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "ndk-sys") (r "^0.4.0") (d #t) (t "cfg(target_os = \"android\")") (k 0)))) (h "0qcdnazjx1c3wk632kk8043m8vw23c11ziy3rpvd8gy0adr70jdj")))

(define-public crate-an_logger-0.1.1 (c (n "an_logger") (v "0.1.1") (d (list (d (n "android_logger") (r "^0.11.1") (d #t) (k 0)) (d (n "jni") (r "^0.19.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.84") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "ndk-sys") (r "^0.4.0") (d #t) (t "cfg(target_os = \"android\")") (k 0)))) (h "180sfms573vfycj310rz74968p918bp9glibrg9ckl8bz0la10wk")))

(define-public crate-an_logger-0.1.2 (c (n "an_logger") (v "0.1.2") (d (list (d (n "android_logger") (r "^0.11.1") (d #t) (k 0)) (d (n "jni") (r "^0.19.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.84") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "ndk-sys") (r "^0.4.0") (d #t) (t "cfg(target_os = \"android\")") (k 0)))) (h "166jqjhjk2q2wqga6882l795kml3xhxx6kzcfz08kxh53i3iw3f6")))

(define-public crate-an_logger-0.1.3 (c (n "an_logger") (v "0.1.3") (d (list (d (n "android_logger") (r "^0.11.1") (d #t) (k 0)) (d (n "jni") (r "^0.20.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.84") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "ndk-sys") (r "^0.4.0") (d #t) (t "cfg(target_os = \"android\")") (k 0)))) (h "0l290hclj33fvh0zq7n0s7j585gkzckbpll4c8b0rnj3v1c4ssnw")))

(define-public crate-an_logger-0.1.4 (c (n "an_logger") (v "0.1.4") (d (list (d (n "android_logger") (r "^0.11") (d #t) (k 0)) (d (n "jni") (r "^0.21.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "ndk-sys") (r "^0.4.0") (d #t) (t "cfg(target_os = \"android\")") (k 0)))) (h "0vwks09b7m1dskzdz305iqanzgbd6r6i8kkbgr6pp7rqgksk7618")))

(define-public crate-an_logger-0.2.0 (c (n "an_logger") (v "0.2.0") (d (list (d (n "android_logger") (r "^0.11") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "ndk-sys") (r "^0.4.0") (d #t) (t "cfg(target_os = \"android\")") (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)))) (h "09jwa7v4sla8zq23xy7q7hqvnvf3sfn4h8szlpixwyyys7ddg8pk")))

(define-public crate-an_logger-0.2.1 (c (n "an_logger") (v "0.2.1") (d (list (d (n "android_logger") (r "^0.11") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "ndk-sys") (r "^0.4.0") (d #t) (t "cfg(target_os = \"android\")") (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)))) (h "1wlpsj7aavw55c9x5vv5sgkvkm2wgqd4q888yhvg1ip6lyc6sinb")))

