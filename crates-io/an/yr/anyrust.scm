(define-module (crates-io an yr anyrust) #:use-module (crates-io))

(define-public crate-anyrust-0.0.0 (c (n "anyrust") (v "0.0.0") (d (list (d (n "dyn-clone") (r "^1.0.16") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0mb3g5612qxi1xarabl9l888cy7r4bg95ilhs9ncf8wbhas4c574")))

(define-public crate-anyrust-0.1.0 (c (n "anyrust") (v "0.1.0") (d (list (d (n "dyn-clone") (r "^1.0.16") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0ss3xfijk8amz8m5z650i0y7r8xhjyhkzxg9vwcfmy92zc36fbv2")))

(define-public crate-anyrust-0.2.0 (c (n "anyrust") (v "0.2.0") (d (list (d (n "dyn-clone") (r "^1.0.16") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1pnskfq4c2aph8i7zkakydv2gr2vwkn4z0shsga3gzy19jpmag02")))

(define-public crate-anyrust-0.2.1 (c (n "anyrust") (v "0.2.1") (d (list (d (n "dyn-clone") (r "^1.0.16") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0pplk98zjl8hvny2q4b0vsrmrfsqvkf522rw8d3sjpy8dlhp98sc")))

(define-public crate-anyrust-0.2.2 (c (n "anyrust") (v "0.2.2") (d (list (d (n "dyn-clone") (r "^1.0.16") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "19dny5d36y40mwqdpiz1z4fd79gslkkj761m75shcq05ia8nnl32")))

(define-public crate-anyrust-0.2.3 (c (n "anyrust") (v "0.2.3") (d (list (d (n "dyn-clone") (r "^1.0.16") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0fjvl4j7bq1r2d7hms79zwwl9cjhsrz15cg41ldjal9xqmc5ha66")))

