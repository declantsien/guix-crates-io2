(define-module (crates-io an yr anyrun-plugin) #:use-module (crates-io))

(define-public crate-anyrun-plugin-0.1.0 (c (n "anyrun-plugin") (v "0.1.0") (d (list (d (n "abi_stable") (r "^0.11.1") (d #t) (k 0)) (d (n "anyrun-interface") (r "^0.1.0") (d #t) (k 0)) (d (n "anyrun-macros") (r "^0.1.0") (d #t) (k 0)))) (h "14jpddqlxm5nc511gr95969l5sxjiy7pcgbk9msnklkh6f4psb41")))

