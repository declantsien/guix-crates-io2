(define-module (crates-io an ar anarchist-readable-name-generator-lib) #:use-module (crates-io))

(define-public crate-anarchist-readable-name-generator-lib-0.1.0 (c (n "anarchist-readable-name-generator-lib") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_pcg") (r "^0.3.1") (d #t) (k 2)))) (h "1skcj75mj353lifmkflpfbs4g5zqfffxfsf9igl39kal302053wp")))

(define-public crate-anarchist-readable-name-generator-lib-0.1.1 (c (n "anarchist-readable-name-generator-lib") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_pcg") (r "^0.3.1") (d #t) (k 2)))) (h "16wf6v94qsf678ki9jh5l1npxbgf3gdm7m0cn8z4y8367i23sv24")))

