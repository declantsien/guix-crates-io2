(define-module (crates-io an d- and-then2) #:use-module (crates-io))

(define-public crate-and-then2-0.1.0 (c (n "and-then2") (v "0.1.0") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)))) (h "1vx9yg259qs90s515kpixs0vxgd1d2iasdynx76chy4v7mf9bg67")))

(define-public crate-and-then2-0.1.1 (c (n "and-then2") (v "0.1.1") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)))) (h "1kh664sy6wxbkk6mgdjdc4baa817401h974qs80k41jhsbav05iw")))

