(define-module (crates-io an d- and-then-concurrent) #:use-module (crates-io))

(define-public crate-and-then-concurrent-0.1.0 (c (n "and-then-concurrent") (v "0.1.0") (d (list (d (n "futures-util") (r "^0.3") (d #t) (k 0)) (d (n "pin-project") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^0.3") (f (quote ("full"))) (d #t) (k 2)))) (h "01l1c5g7fscj2pammzz82d5c6cyddp404lr8gmlfr2layrwczk4g")))

