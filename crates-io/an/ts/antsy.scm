(define-module (crates-io an ts antsy) #:use-module (crates-io))

(define-public crate-antsy-1.0.0 (c (n "antsy") (v "1.0.0") (h "0qwlpmffn0kpyjpicv8k5kwqxhkyyxv4sbwp59ylxnr7zz3z2kxm")))

(define-public crate-antsy-1.1.0 (c (n "antsy") (v "1.1.0") (h "1il0g5x1vggha7ad1v6cqzlk9g6an8fdvsxscvqv4a62k257sirw")))

