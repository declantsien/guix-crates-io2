(define-module (crates-io an ni anni-vgmdb) #:use-module (crates-io))

(define-public crate-anni-vgmdb-0.2.1 (c (n "anni-vgmdb") (v "0.2.1") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "rustls-tls"))) (k 0)) (d (n "select") (r "^0.6.0-alpha.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.5.0") (f (quote ("full"))) (d #t) (k 2)))) (h "1sysckpvq6zhgkn00q621xf7fr7v3hmr7kvf3nrs394b5lb2015w")))

(define-public crate-anni-vgmdb-0.2.2 (c (n "anni-vgmdb") (v "0.2.2") (d (list (d (n "reqwest") (r "^0.11.10") (f (quote ("rustls-tls"))) (k 0)) (d (n "select") (r "^0.6.0-alpha.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.5.0") (f (quote ("full"))) (d #t) (k 2)))) (h "1kmw9vz93sdn9iaj5rsfx6sjs5fwln6wlizqc3zbwcdv7hwc68kk")))

(define-public crate-anni-vgmdb-0.2.3 (c (n "anni-vgmdb") (v "0.2.3") (d (list (d (n "reqwest") (r "^0.11.10") (f (quote ("rustls-tls"))) (k 0)) (d (n "select") (r "^0.6.0-alpha.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "0zqncgcmph5s750h36rcm4cs2i26jkl105d0657ljx696wdl2l4q")))

(define-public crate-anni-vgmdb-0.2.4 (c (n "anni-vgmdb") (v "0.2.4") (d (list (d (n "reqwest") (r "^0.11.10") (f (quote ("rustls-tls"))) (k 0)) (d (n "select") (r "^0.6.0-alpha.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "0s54w33cyrdzr6zc44n8v0x9gmvk4190swka079814aihjfq0x7m")))

(define-public crate-anni-vgmdb-0.3.0 (c (n "anni-vgmdb") (v "0.3.0") (d (list (d (n "reqwest") (r "^0.11.10") (f (quote ("rustls-tls"))) (k 0)) (d (n "select") (r "^0.6.0-alpha.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1dcmqvnfrlnwv4sv7ihnayqndf819g9sc0ykqb95s5k8ydijcpx5")))

(define-public crate-anni-vgmdb-0.3.1 (c (n "anni-vgmdb") (v "0.3.1") (d (list (d (n "reqwest") (r "^0.11.13") (f (quote ("rustls-tls"))) (k 0)) (d (n "select") (r "^0.6.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "0y8rvi446c6px7wgdscr401skxbj71cyffz2zl5dvi60clm42axs")))

