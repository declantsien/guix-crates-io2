(define-module (crates-io an ni anni-flac) #:use-module (crates-io))

(define-public crate-anni-flac-0.2.2 (c (n "anni-flac") (v "0.2.2") (d (list (d (n "async-trait") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (o #t) (d #t) (k 0)))) (h "1f8z6df4nshvmpz1p363f0x81s7qc1fwbmb7f1lry0mwdl2sz4h4") (f (quote (("async" "tokio" "async-trait"))))))

