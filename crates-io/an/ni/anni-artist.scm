(define-module (crates-io an ni anni-artist) #:use-module (crates-io))

(define-public crate-anni-artist-0.1.0 (c (n "anni-artist") (v "0.1.0") (h "1jc31r55h1y93hw0krcdd4cx4h01idqzagyw17gvy9wk772briys")))

(define-public crate-anni-artist-0.1.1 (c (n "anni-artist") (v "0.1.1") (h "1vahbbwvq7x0ky3nnfq3vfw142qnl9rkad7qi84mjni66q434rpc")))

