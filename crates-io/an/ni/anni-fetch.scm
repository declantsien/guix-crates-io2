(define-module (crates-io an ni anni-fetch) #:use-module (crates-io))

(define-public crate-anni-fetch-0.1.0 (c (n "anni-fetch") (v "0.1.0") (d (list (d (n "miniz_oxide") (r "^0.4.4") (d #t) (k 0)) (d (n "sha-1") (r "^0.9.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "ureq") (r "^2") (d #t) (k 0)))) (h "07gigb7l6cqygngivggl44s4mxxw63afwg5y46ksid04k08i0q1n")))

(define-public crate-anni-fetch-0.1.1 (c (n "anni-fetch") (v "0.1.1") (d (list (d (n "miniz_oxide") (r "^0.4.4") (d #t) (k 0)) (d (n "sha-1") (r "^0.9.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "ureq") (r "^2") (d #t) (k 0)))) (h "1hny36d73k2xg4k2441zqjwr3h9wgj4pn5qrgp4ws6r3d4ppiz47")))

(define-public crate-anni-fetch-0.2.0 (c (n "anni-fetch") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "miniz_oxide") (r "^0.4.4") (d #t) (k 0)) (d (n "sha-1") (r "^0.9.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "ureq") (r "^2") (d #t) (k 0)))) (h "0h6za7wakbp7hjljc3bx5vxy7ws0950y1w2aj7a065myir1ibs12")))

