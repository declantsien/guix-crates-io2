(define-module (crates-io an ni annis) #:use-module (crates-io))

(define-public crate-annis-0.0.1 (c (n "annis") (v "0.0.1") (d (list (d (n "reqwest") (r "^0.9.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1k329qk89fm86q9wc8z7hypzj5rphs7zw9g359lyx6x0p9nkad1f") (y #t)))

(define-public crate-annis-0.0.2 (c (n "annis") (v "0.0.2") (d (list (d (n "reqwest") (r "^0.9.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "0kw50wqqibgbjkq7vnn2gvlr0zckw3s9xsln564xm46mih6gi308") (y #t)))

(define-public crate-annis-0.0.3 (c (n "annis") (v "0.0.3") (d (list (d (n "reqwest") (r "^0.9.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "0kqfrrvjm9rk48zz03plikvbpjl75rbhia57g70k02gdvnx7ry7f") (y #t)))

(define-public crate-annis-0.0.4 (c (n "annis") (v "0.0.4") (d (list (d (n "reqwest") (r "^0.9.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "1lk0chsdp0j3pylz25h1zyc4wfszmq355y5i03r6gcck1194ppyr")))

(define-public crate-annis-0.0.5 (c (n "annis") (v "0.0.5") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "0ng4jla9ilxxb9z89cm33sll1blcj5kwiv86wdrd8l7byvac17zf")))

(define-public crate-annis-0.0.6 (c (n "annis") (v "0.0.6") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros"))) (d #t) (k 2)))) (h "1y8nmp9j3p1my2ixjxby9mnr9y4l00jv3l1djz1fqqxiggcaxd49")))

