(define-module (crates-io an ne annembed) #:use-module (crates-io))

(define-public crate-annembed-0.0.1 (c (n "annembed") (v "0.0.1") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "clap") (r "^2.29") (d #t) (k 0)) (d (n "cpu-time") (r "^1.0") (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.8") (d #t) (k 0)) (d (n "hnsw_rs") (r "^0.1.12") (d #t) (k 0)) (d (n "indexmap") (r "^1.6") (d #t) (k 0)) (d (n "lapacke") (r "^0.5") (d #t) (k 0)) (d (n "lax") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("max_level_debug" "release_max_level_info"))) (d #t) (k 0)) (d (n "ndarray") (r "^0.14") (f (quote ("rayon" "serde"))) (d #t) (k 0)) (d (n "ndarray-linalg") (r "^0.13.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "num_cpus") (r "^1.8.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "quantiles") (r "^0.7") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4") (d #t) (k 0)) (d (n "rand_xoshiro") (r "^0.6") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sprs") (r "^0.10.0") (d #t) (k 0)) (d (n "time") (r "^0.2") (d #t) (k 0)))) (h "152anmld04ran9vnr4wk7kcvlrdvjdsb47313szqglxz0mlzz1c5") (f (quote (("openblas" "ndarray-linalg/openblas") ("netlib" "ndarray-linalg/netlib") ("intel-mkl" "ndarray-linalg/intel-mkl") ("default" "intel-mkl"))))))

(define-public crate-annembed-0.0.2 (c (n "annembed") (v "0.0.2") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "clap") (r "^2.29") (d #t) (k 0)) (d (n "cpu-time") (r "^1.0") (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "hnsw_rs") (r "^0.1.15") (d #t) (k 0)) (d (n "indexmap") (r "^1.6") (d #t) (k 0)) (d (n "katexit") (r "^0.1.0") (d #t) (k 0)) (d (n "lapacke") (r "^0.5") (d #t) (k 0)) (d (n "lax") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("max_level_debug" "release_max_level_info"))) (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (f (quote ("rayon" "serde"))) (d #t) (k 0)) (d (n "ndarray-linalg") (r "^0.14") (f (quote ("openblas-static"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "num_cpus") (r "^1.8.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "quantiles") (r "^0.7") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4") (d #t) (k 0)) (d (n "rand_xoshiro") (r "^0.6") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sprs") (r "^0.11") (d #t) (k 0)))) (h "16fnwf65id962i76f5q4gj649ch3myzfigc4m22qin8z8mbr7ww6")))

(define-public crate-annembed-0.0.3 (c (n "annembed") (v "0.0.3") (d (list (d (n "anyhow") (r "<=1.0.48") (d #t) (k 0)) (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "cpu-time") (r "^1.0") (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "hnsw_rs") (r "^0.1.15") (d #t) (k 0)) (d (n "indexmap") (r "^1.6") (d #t) (k 0)) (d (n "katexit") (r "^0.1.0") (d #t) (k 0)) (d (n "lax") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (f (quote ("rayon" "serde"))) (d #t) (k 0)) (d (n "ndarray-linalg") (r "^0.14") (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "num_cpus") (r "^1.8.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "quantiles") (r "^0.7") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4") (d #t) (k 0)) (d (n "rand_xoshiro") (r "^0.6") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sprs") (r "^0.11") (d #t) (k 0)))) (h "0lcg069fg4j0r82s2lalvpg58qg1bkkrwznfp8axcvym7bi9nw82") (f (quote (("openblas-static" "ndarray/blas" "ndarray-linalg/openblas-static") ("intel-mkl-static" "ndarray/blas" "ndarray-linalg/intel-mkl-static") ("default"))))))

(define-public crate-annembed-0.0.4 (c (n "annembed") (v "0.0.4") (d (list (d (n "anyhow") (r "<=1.0.48") (d #t) (k 0)) (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "cpu-time") (r "^1.0") (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "hnsw_rs") (r "^0.1.15") (d #t) (k 0)) (d (n "indexmap") (r "^1.6") (d #t) (k 0)) (d (n "katexit") (r "^0.1.0") (d #t) (k 0)) (d (n "lax") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (f (quote ("rayon" "serde"))) (d #t) (k 0)) (d (n "ndarray-linalg") (r "^0.14") (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "num_cpus") (r "^1.8.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "quantiles") (r "^0.7") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4") (d #t) (k 0)) (d (n "rand_xoshiro") (r "^0.6") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sprs") (r "^0.11") (d #t) (k 0)))) (h "03n466xba3s4sh16nlyh3i3hdwy3dbq2hynr7slcjmjv5mik3dgv") (f (quote (("openblas-static" "ndarray/blas" "ndarray-linalg/openblas-static") ("intel-mkl-static" "ndarray/blas" "ndarray-linalg/intel-mkl-static") ("default"))))))

(define-public crate-annembed-0.0.5 (c (n "annembed") (v "0.0.5") (d (list (d (n "anyhow") (r "<=1.0.48") (d #t) (k 0)) (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "bson") (r "^2.2") (d #t) (k 0)) (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "clap") (r "^3.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "cpu-time") (r "^1.0") (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "hnsw_rs") (r "^0.1.16") (f (quote ("simdeez_f"))) (d #t) (k 0)) (d (n "indexmap") (r "^1.6") (d #t) (k 0)) (d (n "katexit") (r "^0.1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (f (quote ("rayon" "serde"))) (d #t) (k 0)) (d (n "ndarray-linalg") (r "^0.14") (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "num_cpus") (r "^1.8.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "quantiles") (r "^0.7") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4") (d #t) (k 0)) (d (n "rand_xoshiro") (r "^0.6") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sprs") (r "^0.11") (d #t) (k 0)))) (h "0gyspddw8yw85f1rsrwyha7qmmf03s80frd5xcp1zhm7ya0ddz03") (f (quote (("openblas-system" "ndarray/blas" "ndarray-linalg/openblas-system") ("openblas-static" "ndarray/blas" "ndarray-linalg/openblas-static") ("intel-mkl-static" "ndarray/blas" "ndarray-linalg/intel-mkl-static") ("default"))))))

(define-public crate-annembed-0.0.6 (c (n "annembed") (v "0.0.6") (d (list (d (n "anyhow") (r "<=1.0.48") (d #t) (k 0)) (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "bson") (r "^2.2") (d #t) (k 0)) (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "clap") (r "^3.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "cpu-time") (r "^1.0") (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "hnsw_rs") (r "^0.1.16") (f (quote ("simdeez_f"))) (d #t) (k 0)) (d (n "indexmap") (r "^1.6") (d #t) (k 0)) (d (n "katexit") (r "^0.1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (f (quote ("rayon" "serde"))) (d #t) (k 0)) (d (n "ndarray-linalg") (r "^0.14") (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "num_cpus") (r "^1.8.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "quantiles") (r "^0.7") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4") (d #t) (k 0)) (d (n "rand_xoshiro") (r "^0.6") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sprs") (r "^0.11") (d #t) (k 0)))) (h "1bby019r8cab1k0prp4hgzxhmns6jb8qsmb4qmmag1v72pvz99y0") (f (quote (("openblas-system" "ndarray/blas" "ndarray-linalg/openblas-system") ("openblas-static" "ndarray/blas" "ndarray-linalg/openblas-static") ("intel-mkl-static" "ndarray/blas" "ndarray-linalg/intel-mkl-static") ("default"))))))

(define-public crate-annembed-0.0.7 (c (n "annembed") (v "0.0.7") (d (list (d (n "anyhow") (r "<=1.0.48") (d #t) (k 0)) (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "bson") (r "^2.2") (d #t) (k 0)) (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "clap") (r "^3.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "cpu-time") (r "^1.0") (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "hnsw_rs") (r "^0.1.16") (f (quote ("simdeez_f"))) (d #t) (k 0)) (d (n "indexmap") (r "^1.6") (d #t) (k 0)) (d (n "katexit") (r "^0.1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (f (quote ("rayon" "serde"))) (d #t) (k 0)) (d (n "ndarray-linalg") (r "^0.14") (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "num_cpus") (r "^1.8.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "quantiles") (r "^0.7") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4") (d #t) (k 0)) (d (n "rand_xoshiro") (r "^0.6") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sprs") (r "^0.11") (d #t) (k 0)))) (h "0j0jz4wwigqdz6bj7yjjn2qxmrnvc57vv9z9bq8yly9x8088f32a") (f (quote (("openblas-system" "ndarray/blas" "ndarray-linalg/openblas-system") ("openblas-static" "ndarray/blas" "ndarray-linalg/openblas-static") ("intel-mkl-static" "ndarray/blas" "ndarray-linalg/intel-mkl-static") ("default"))))))

(define-public crate-annembed-0.0.8 (c (n "annembed") (v "0.0.8") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "bson") (r "^2.4") (d #t) (k 0)) (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "clap") (r "^3.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "cpu-time") (r "^1.0") (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "hnsw_rs") (r "^0.1.16") (f (quote ("simdeez_f"))) (d #t) (k 0)) (d (n "indexmap") (r "^1.6") (d #t) (k 0)) (d (n "katexit") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (f (quote ("rayon" "serde"))) (d #t) (k 0)) (d (n "ndarray-linalg") (r "^0.15") (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "num_cpus") (r "^1.8.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "quantiles") (r "^0.7") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4") (d #t) (k 0)) (d (n "rand_xoshiro") (r "^0.6") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sprs") (r "^0.11") (d #t) (k 0)))) (h "072zqhz9r6ii476bs14mwqxk0fl411m39f73bxshfvyckxgx3yg1") (f (quote (("openblas-system" "ndarray/blas" "ndarray-linalg/openblas-system") ("openblas-static" "ndarray/blas" "ndarray-linalg/openblas-static") ("intel-mkl-static" "ndarray/blas" "ndarray-linalg/intel-mkl-static") ("default"))))))

(define-public crate-annembed-0.0.9 (c (n "annembed") (v "0.0.9") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "bson") (r "^2.4") (d #t) (k 0)) (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "clap") (r "^3.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "cpu-time") (r "^1.0") (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 0)) (d (n "hdrhistogram") (r "^7.5") (d #t) (k 0)) (d (n "hnsw_rs") (r "^0.1.18") (f (quote ("simdeez_f"))) (d #t) (k 0)) (d (n "indexmap") (r "^1.9") (d #t) (k 0)) (d (n "indxvec") (r "^1.4") (d #t) (k 0)) (d (n "katexit") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (f (quote ("rayon" "serde"))) (d #t) (k 0)) (d (n "ndarray-linalg") (r "^0.15") (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "num_cpus") (r "^1.8.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "quantiles") (r "^0.7") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4") (d #t) (k 0)) (d (n "rand_xoshiro") (r "^0.6") (d #t) (k 0)) (d (n "rayon") (r "^1.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sprs") (r "^0.11") (d #t) (k 0)))) (h "0q328jh0npdmjsnamw04jiink1ckps77d2rxavqddz221i6b3gan") (f (quote (("openblas-system" "ndarray/blas" "ndarray-linalg/openblas-system") ("openblas-static" "ndarray/blas" "ndarray-linalg/openblas-static") ("intel-mkl-static" "ndarray/blas" "ndarray-linalg/intel-mkl-static") ("default"))))))

(define-public crate-annembed-0.0.10 (c (n "annembed") (v "0.0.10") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "bson") (r "^2.4") (d #t) (k 0)) (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "clap") (r "^4.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "cpu-time") (r "^1.0") (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 0)) (d (n "hdrhistogram") (r "^7.5") (d #t) (k 0)) (d (n "hnsw_rs") (r "^0.1.19") (f (quote ("simdeez_f"))) (d #t) (k 0)) (d (n "indexmap") (r "^1.9") (d #t) (k 0)) (d (n "indxvec") (r "^1.4") (d #t) (k 0)) (d (n "katexit") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (f (quote ("rayon" "serde"))) (d #t) (k 0)) (d (n "ndarray-linalg") (r "^0.16") (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "num_cpus") (r "^1.8.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "quantiles") (r "^0.7") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4") (d #t) (k 0)) (d (n "rand_xoshiro") (r "^0.6") (d #t) (k 0)) (d (n "rayon") (r "^1.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sprs") (r "^0.11") (d #t) (k 0)))) (h "1k7875izfa0nndxh20890xp9gcgncf6b8j3amh9xj96abrfaw09f") (f (quote (("openblas-system" "ndarray/blas" "ndarray-linalg/openblas-system") ("openblas-static" "ndarray/blas" "ndarray-linalg/openblas-static") ("intel-mkl-static" "ndarray/blas" "ndarray-linalg/intel-mkl-static") ("default"))))))

(define-public crate-annembed-0.0.11 (c (n "annembed") (v "0.0.11") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "bson") (r "^2.4") (d #t) (k 0)) (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "clap") (r "^4.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "cpu-time") (r "^1.0") (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 0)) (d (n "hdrhistogram") (r "^7.5") (d #t) (k 0)) (d (n "hnsw_rs") (r "^0.1.19") (f (quote ("simdeez_f"))) (d #t) (k 0)) (d (n "indexmap") (r "^1.9") (d #t) (k 0)) (d (n "indxvec") (r "^1.4") (d #t) (k 0)) (d (n "katexit") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (f (quote ("rayon" "serde"))) (d #t) (k 0)) (d (n "ndarray-linalg") (r "^0.16") (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "num_cpus") (r "^1.8.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "quantiles") (r "^0.7") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4") (d #t) (k 0)) (d (n "rand_xoshiro") (r "^0.6") (d #t) (k 0)) (d (n "rayon") (r "^1.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sprs") (r "^0.11") (d #t) (k 0)))) (h "13lrb5fjc17hajw5y7yp7iqk2919wassixb37r03m65k32zl794y") (f (quote (("openblas-system" "ndarray/blas" "ndarray-linalg/openblas-system") ("openblas-static" "ndarray/blas" "ndarray-linalg/openblas-static") ("intel-mkl-static" "ndarray/blas" "ndarray-linalg/intel-mkl-static") ("default"))))))

(define-public crate-annembed-0.1.0 (c (n "annembed") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "blas-src") (r "^0.8") (f (quote ("accelerate"))) (o #t) (d #t) (k 0)) (d (n "bson") (r "^2.4") (d #t) (k 0)) (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "cpu-time") (r "^1.0") (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 0)) (d (n "hdrhistogram") (r "^7.5") (d #t) (k 0)) (d (n "hnsw_rs") (r "^0.2") (f (quote ("simdeez_f"))) (d #t) (k 0)) (d (n "indexmap") (r "^1.9") (d #t) (k 0)) (d (n "indxvec") (r "^1.4") (d #t) (k 0)) (d (n "katexit") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (f (quote ("rayon" "serde"))) (d #t) (k 0)) (d (n "ndarray-linalg") (r "^0.16") (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "num_cpus") (r "^1.8.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "quantiles") (r "^0.7") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4") (d #t) (k 0)) (d (n "rand_xoshiro") (r "^0.6") (d #t) (k 0)) (d (n "rayon") (r "^1.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sprs") (r "^0.11") (d #t) (k 0)))) (h "1y6ip6hly5vh13jzfqx5qgnnbfdll7077rmsjpaxi0bfyy10jd33") (f (quote (("openblas-system" "ndarray/blas" "ndarray-linalg/openblas-system") ("openblas-static" "ndarray/blas" "ndarray-linalg/openblas-static") ("macos-accelerate" "blas-src" "ndarray/blas") ("intel-mkl-static" "ndarray/blas" "ndarray-linalg/intel-mkl-static") ("default"))))))

(define-public crate-annembed-0.1.1 (c (n "annembed") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "blas-src") (r "^0.8") (f (quote ("accelerate"))) (o #t) (d #t) (k 0)) (d (n "bson") (r "^2.4") (d #t) (k 0)) (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "cpu-time") (r "^1.0") (d #t) (k 0)) (d (n "csv") (r "^1.3") (d #t) (k 0)) (d (n "env_logger") (r "^0.11") (d #t) (k 0)) (d (n "hdrhistogram") (r "^7.5") (d #t) (k 0)) (d (n "hnsw_rs") (r "^0.2") (f (quote ("simdeez_f"))) (d #t) (k 0)) (d (n "indexmap") (r "^1.9") (d #t) (k 0)) (d (n "indxvec") (r "^1.4") (d #t) (k 0)) (d (n "katexit") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (f (quote ("rayon" "serde"))) (d #t) (k 0)) (d (n "ndarray-linalg") (r "^0.16") (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "num_cpus") (r "^1.16") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "quantiles") (r "^0.7") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4") (d #t) (k 0)) (d (n "rand_xoshiro") (r "^0.6") (d #t) (k 0)) (d (n "rayon") (r "^1.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sprs") (r "^0.11") (d #t) (k 0)))) (h "0v2yl6mjpr1g9i1dl6qd79n56395w0qcqisrg2k7w511q5mypsyd") (f (quote (("openblas-system" "ndarray/blas" "ndarray-linalg/openblas-system") ("openblas-static" "ndarray/blas" "ndarray-linalg/openblas-static") ("macos-accelerate" "blas-src" "ndarray/blas") ("intel-mkl-static" "ndarray/blas" "ndarray-linalg/intel-mkl-static") ("default"))))))

(define-public crate-annembed-0.1.2 (c (n "annembed") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "blas-src") (r "^0.8") (f (quote ("accelerate"))) (o #t) (d #t) (k 0)) (d (n "bson") (r "^2.10") (d #t) (k 0)) (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "cpu-time") (r "^1.0") (d #t) (k 0)) (d (n "csv") (r "^1.3") (d #t) (k 0)) (d (n "env_logger") (r "^0.11") (d #t) (k 0)) (d (n "hdrhistogram") (r "^7.5") (d #t) (k 0)) (d (n "hnsw_rs") (r "^0.2.1") (d #t) (k 0)) (d (n "indexmap") (r "^2.2") (d #t) (k 0)) (d (n "indxvec") (r "^1.9") (d #t) (k 0)) (d (n "katexit") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (f (quote ("rayon" "serde"))) (d #t) (k 0)) (d (n "ndarray-linalg") (r "^0.16") (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "num_cpus") (r "^1.16") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "quantiles") (r "^0.7") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4") (d #t) (k 0)) (d (n "rand_xoshiro") (r "^0.6") (d #t) (k 0)) (d (n "rayon") (r "^1.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sprs") (r "^0.11") (d #t) (k 0)))) (h "1kri2waick2pgaw643zq75z2mn3lmzlllqrhawwk3vvrsd57dyrk") (f (quote (("stdsimd" "hnsw_rs/stdsimd") ("simdeez_f" "hnsw_rs/simdeez_f") ("openblas-system" "ndarray/blas" "ndarray-linalg/openblas-system") ("openblas-static" "ndarray/blas" "ndarray-linalg/openblas-static") ("macos-accelerate" "blas-src" "ndarray/blas") ("intel-mkl-static" "ndarray/blas" "ndarray-linalg/intel-mkl-static") ("default"))))))

(define-public crate-annembed-0.1.3 (c (n "annembed") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "blas-src") (r "^0.8") (f (quote ("accelerate"))) (o #t) (d #t) (k 0)) (d (n "bson") (r "^2.10") (d #t) (k 0)) (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "cpu-time") (r "^1.0") (d #t) (k 0)) (d (n "csv") (r "^1.3") (d #t) (k 0)) (d (n "env_logger") (r "^0.11") (d #t) (k 0)) (d (n "hdrhistogram") (r "^7.5") (d #t) (k 0)) (d (n "hnsw_rs") (r "^0.3") (d #t) (k 0)) (d (n "indexmap") (r "^2.2") (d #t) (k 0)) (d (n "indxvec") (r "^1.9") (d #t) (k 0)) (d (n "katexit") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (f (quote ("rayon" "serde"))) (d #t) (k 0)) (d (n "ndarray-linalg") (r "^0.16") (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "num_cpus") (r "^1.16") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "quantiles") (r "^0.7") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4") (d #t) (k 0)) (d (n "rand_xoshiro") (r "^0.6") (d #t) (k 0)) (d (n "rayon") (r "^1.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sprs") (r "^0.11") (d #t) (k 0)))) (h "1q1wjfvxlvr55a8gzxbgll9dmppg346agyr685fij9har2fk8x9g") (f (quote (("stdsimd" "hnsw_rs/stdsimd") ("simdeez_f" "hnsw_rs/simdeez_f") ("openblas-system" "ndarray/blas" "ndarray-linalg/openblas-system") ("openblas-static" "ndarray/blas" "ndarray-linalg/openblas-static") ("macos-accelerate" "blas-src" "ndarray/blas") ("intel-mkl-static" "ndarray/blas" "ndarray-linalg/intel-mkl-static") ("default"))))))

