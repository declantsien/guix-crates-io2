(define-module (crates-io an ne anne) #:use-module (crates-io))

(define-public crate-anne-0.1.0 (c (n "anne") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.4.3") (d #t) (k 0)) (d (n "log") (r "^0.4.0") (d #t) (k 0)) (d (n "quick-xml") (r "^0.10.1") (d #t) (k 0)) (d (n "zip") (r "^0.2.6") (d #t) (k 0)))) (h "14281gd66liqjma36kgmkdhq1d8g9ivxm285864sjysygiz5zm87")))

