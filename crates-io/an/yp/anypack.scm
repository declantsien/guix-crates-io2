(define-module (crates-io an yp anypack) #:use-module (crates-io))

(define-public crate-anypack-0.1.2 (c (n "anypack") (v "0.1.2") (d (list (d (n "axum") (r "^0.6.18") (d #t) (k 0)) (d (n "msgpacker") (r "^0.4.3") (d #t) (k 0)) (d (n "paste") (r "^1.0.12") (d #t) (k 0)))) (h "1n69ghpzqhha3bmkdqddkicsxanjijd2dsf159q3ixs4x1c2k4i5")))

(define-public crate-anypack-0.1.4 (c (n "anypack") (v "0.1.4") (d (list (d (n "axum") (r "^0.6.18") (d #t) (k 0)) (d (n "msgpacker") (r "^0.4.3") (d #t) (k 0)) (d (n "paste") (r "^1.0.12") (d #t) (k 0)))) (h "0ng1nhgdr2wwsxfv4jc6d29v7dpzlnba1aiyazx8g8z8k7rqrnxj")))

(define-public crate-anypack-0.1.5 (c (n "anypack") (v "0.1.5") (d (list (d (n "axum") (r "^0.6.18") (d #t) (k 0)) (d (n "msgpacker") (r "^0.4.3") (d #t) (k 0)) (d (n "paste") (r "^1.0.12") (d #t) (k 0)))) (h "1zbbaqsiq65vn25vc2c1qq6ibcry5l85259zdknxnwhgzfnqw6sb")))

(define-public crate-anypack-0.1.6 (c (n "anypack") (v "0.1.6") (d (list (d (n "axum") (r "^0.6.18") (d #t) (k 0)) (d (n "msgpacker") (r "^0.4.3") (d #t) (k 0)) (d (n "paste") (r "^1.0.12") (d #t) (k 0)))) (h "1wx85p67gd95zzd7rz6xfz2ddb5wh7q21j9nzp5m8w9vpy7rwxh4")))

(define-public crate-anypack-0.1.7 (c (n "anypack") (v "0.1.7") (d (list (d (n "axum") (r "^0.6.18") (d #t) (k 0)) (d (n "msgpacker") (r "^0.4.3") (d #t) (k 0)) (d (n "paste") (r "^1.0.12") (d #t) (k 0)))) (h "089px9xmf7v1sr5bahiqhq38gc5lhynyj7r2hj0f7h5z1dvprnvk")))

(define-public crate-anypack-0.1.8 (c (n "anypack") (v "0.1.8") (d (list (d (n "axum") (r "^0.6.18") (d #t) (k 0)) (d (n "msgpacker") (r "^0.4.4") (d #t) (k 0) (p "xxai-msgpacker")) (d (n "paste") (r "^1.0.12") (d #t) (k 0)))) (h "13hy2l0fa8pxbfga42h2yfj83bwypssc6rrj76rffmacihkv1isv")))

(define-public crate-anypack-0.1.9 (c (n "anypack") (v "0.1.9") (d (list (d (n "axum") (r "^0.6.18") (d #t) (k 0)) (d (n "msgpacker") (r "^0.4.4") (d #t) (k 0) (p "xxai-msgpacker")) (d (n "paste") (r "^1.0.12") (d #t) (k 0)))) (h "0xnzma9l5jwgpz9fpm4rp35d66i5p714ma527pczaznyqbhqm4rl")))

(define-public crate-anypack-0.1.10 (c (n "anypack") (v "0.1.10") (d (list (d (n "axum") (r "^0.6.18") (d #t) (k 0)) (d (n "msgpacker") (r "^0.4.4") (d #t) (k 0) (p "xxai-msgpacker")) (d (n "paste") (r "^1.0.12") (d #t) (k 0)))) (h "19igyd513jihkdh8zd3r8dq4zpb01hplg1iqxf9757n9ssw4izws")))

(define-public crate-anypack-0.1.11 (c (n "anypack") (v "0.1.11") (d (list (d (n "async-trait") (r "^0.1.68") (d #t) (k 0)) (d (n "axum") (r "^0.6.18") (d #t) (k 0)) (d (n "msgpacker") (r "^0.4.4") (d #t) (k 0) (p "xxai-msgpacker")) (d (n "paste") (r "^1.0.12") (d #t) (k 0)))) (h "0cm87dvmx6scfpiiy34j05dz4nwqyp1wni451jpyxa6h7xp5jjf8")))

(define-public crate-anypack-0.1.13 (c (n "anypack") (v "0.1.13") (d (list (d (n "async-trait") (r "^0.1.68") (d #t) (k 0)) (d (n "axum") (r "^0.6.18") (d #t) (k 0)) (d (n "msgpacker") (r "^0.4.4") (d #t) (k 0) (p "xxai-msgpacker")) (d (n "paste") (r "^1.0.12") (d #t) (k 0)))) (h "01k76jxd0dcyygffl1113ghc3i3hljp4xs3d43hx5flgmvkifhq8")))

(define-public crate-anypack-0.1.16 (c (n "anypack") (v "0.1.16") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "axum") (r "^0.6.20") (d #t) (k 0)) (d (n "msgpacker") (r "^0.4.4") (d #t) (k 0) (p "xxai-msgpacker")) (d (n "paste") (r "^1.0.14") (d #t) (k 0)))) (h "036iim40jknirr25bpv8w51amn9id091n5zz7x7r0f574zir6rqr")))

(define-public crate-anypack-0.1.17 (c (n "anypack") (v "0.1.17") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "axum") (r "^0.6.20") (d #t) (k 0)) (d (n "msgpacker") (r "^0.4.4") (d #t) (k 0) (p "xxai-msgpacker")) (d (n "paste") (r "^1.0.14") (d #t) (k 0)))) (h "1zcis23ad59sii8v44w1akcr2ly56i9971rqhwp79cqjhqj40qw4")))

(define-public crate-anypack-0.1.18 (c (n "anypack") (v "0.1.18") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "axum") (r "^0.6.20") (d #t) (k 0)) (d (n "msgpacker") (r "^0.4.4") (d #t) (k 0) (p "xxai-msgpacker")) (d (n "paste") (r "^1.0.14") (d #t) (k 0)))) (h "1ckvsh6i3ykvh5j365pqfimk20h1xj2sb68pgjx85jbas4jd7i8y")))

(define-public crate-anypack-0.1.19 (c (n "anypack") (v "0.1.19") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "axum") (r "^0.6.20") (d #t) (k 0)) (d (n "msgpacker") (r "^0.4.4") (d #t) (k 0) (p "xxai-msgpacker")) (d (n "paste") (r "^1.0.14") (d #t) (k 0)))) (h "0pkr7fyszfnqnb6rddskpszyh22lsjwki7i6s89pb7j8ax5ymg3i")))

