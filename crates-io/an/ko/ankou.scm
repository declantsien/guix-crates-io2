(define-module (crates-io an ko ankou) #:use-module (crates-io))

(define-public crate-ankou-0.0.1 (c (n "ankou") (v "0.0.1") (d (list (d (n "git2") (r "^0.13.12") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "simple_logger") (r "^1.11.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)) (d (n "time") (r "^0.1.44") (d #t) (k 0)))) (h "0v3q7czh5xiyl420plxx18fknns4h3hr208jw3qsgqjl2mafyr8a")))

(define-public crate-ankou-0.0.2 (c (n "ankou") (v "0.0.2") (d (list (d (n "git2") (r "^0.13.12") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "paw") (r "^1.0.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "simple_logger") (r "^1.11.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (f (quote ("paw"))) (d #t) (k 0)) (d (n "time") (r "^0.1.44") (d #t) (k 0)))) (h "0jnpxh9amv20pdm8ha6d31bd7awc8a16a93612qq5f6f7gjfyi92")))

