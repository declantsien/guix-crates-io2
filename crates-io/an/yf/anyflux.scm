(define-module (crates-io an yf anyflux) #:use-module (crates-io))

(define-public crate-anyflux-0.1.0 (c (n "anyflux") (v "0.1.0") (d (list (d (n "anymap") (r "^0.12.1") (d #t) (k 0)) (d (n "async-std") (r "^1.11") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "slab") (r "^0.4") (d #t) (k 0)))) (h "15yrpxrn5cbi6dcx8y7vhclx64jvbkysbsmny9wjzlqh6p06qiqk") (f (quote (("future") ("default" "future"))))))

