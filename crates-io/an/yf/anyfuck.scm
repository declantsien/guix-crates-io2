(define-module (crates-io an yf anyfuck) #:use-module (crates-io))

(define-public crate-anyfuck-0.1.0 (c (n "anyfuck") (v "0.1.0") (d (list (d (n "clap") (r "^4.5.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "libbfi") (r "^6.0.1") (d #t) (k 0)))) (h "1vvklcr98wjy0lahqm7fxh4xlaxq1m4xi3x2fggc450hz4kriicf")))

(define-public crate-anyfuck-0.1.1 (c (n "anyfuck") (v "0.1.1") (d (list (d (n "clap") (r "^4.5.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "libbfi") (r "^6.0.1") (d #t) (k 0)))) (h "0b7jgwkc6wh3zgdknyzdy8avck0k51ffzw8k6iak2bfy8xm9ir9v")))

