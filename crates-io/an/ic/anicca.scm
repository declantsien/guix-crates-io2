(define-module (crates-io an ic anicca) #:use-module (crates-io))

(define-public crate-anicca-0.1.0 (c (n "anicca") (v "0.1.0") (d (list (d (n "clap") (r "^2.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0rbb34l70r0k9s7b1pzx4d8kwg0n311azw4ayrid7r55zah1469x")))

