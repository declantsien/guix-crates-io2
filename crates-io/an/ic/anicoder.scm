(define-module (crates-io an ic anicoder) #:use-module (crates-io))

(define-public crate-anicoder-0.1.0 (c (n "anicoder") (v "0.1.0") (d (list (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.3") (d #t) (k 0)))) (h "0bvq5xza4j7lnqzgmwbz960mld3r05wdcdvxi92yrpwk2m9m5l90")))

(define-public crate-anicoder-0.1.1 (c (n "anicoder") (v "0.1.1") (d (list (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.3") (d #t) (k 0)))) (h "0kh0w1gfbiiq9prph4745jp8y951931fx4i025bcvgmwva9n2jam")))

(define-public crate-anicoder-0.1.2 (c (n "anicoder") (v "0.1.2") (d (list (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.3") (d #t) (k 0)))) (h "0s9z0xf3s95hrx4vas76bj7bmn599b8akdr6yik1xc4mcd1ywbzb")))

