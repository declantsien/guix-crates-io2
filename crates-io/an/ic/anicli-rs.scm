(define-module (crates-io an ic anicli-rs) #:use-module (crates-io))

(define-public crate-anicli-rs-0.1.0 (c (n "anicli-rs") (v "0.1.0") (d (list (d (n "clap") (r "^4.3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "genpdf") (r "^0.2.0") (f (quote ("image" "images"))) (d #t) (k 0)) (d (n "image") (r "^0.24.6") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "pdf-canvas") (r "^0.7.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.28.1") (f (quote ("full"))) (d #t) (k 0)))) (h "08k5wszn9gas86ki8mwnzj3jnizrvki2cc99m6gp197l9ql4rd1w")))

