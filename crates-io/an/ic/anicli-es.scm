(define-module (crates-io an ic anicli-es) #:use-module (crates-io))

(define-public crate-anicli-es-0.2.0 (c (n "anicli-es") (v "0.2.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "curl") (r "^0.4.39") (d #t) (k 0)) (d (n "dialoguer") (r "^0.9.0") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "ureq") (r "^2.2.0") (d #t) (k 0)))) (h "0hnd0w8la0f6rcxllhsk0flgdmsg601i71h6ng3vlgif8bwl1yg4")))

(define-public crate-anicli-es-0.2.1 (c (n "anicli-es") (v "0.2.1") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "curl") (r "^0.4.39") (d #t) (k 0)) (d (n "dialoguer") (r "^0.9.0") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "ureq") (r "^2.2.0") (d #t) (k 0)))) (h "0ysif1b74p7n1vl6r671v8ajw3cnrf82iy19lyw862wb7z1aan91")))

(define-public crate-anicli-es-0.2.3 (c (n "anicli-es") (v "0.2.3") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "curl") (r "^0.4.39") (d #t) (k 0)) (d (n "dialoguer") (r "^0.9.0") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "ureq") (r "^2.2.0") (d #t) (k 0)))) (h "0sfkzdgqw4v0w22224vlp3yh2q6g4ny6p862x6wlm3spag9m47g0")))

(define-public crate-anicli-es-0.2.31 (c (n "anicli-es") (v "0.2.31") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "curl") (r "^0.4.39") (d #t) (k 0)) (d (n "dialoguer") (r "^0.9.0") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "ureq") (r "^2.2.0") (d #t) (k 0)))) (h "16kqrshzpgn8ivhq6apqdds0qb42xfnkv5srh2d6qbc4yp1mg391")))

(define-public crate-anicli-es-0.2.32 (c (n "anicli-es") (v "0.2.32") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "curl") (r "^0.4.39") (d #t) (k 0)) (d (n "dialoguer") (r "^0.9.0") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "ureq") (r "^2.2.0") (d #t) (k 0)))) (h "0b920q7qbkhp886969ss03mfyk10c2hh5jk4ki8x25lwxklks0lr")))

(define-public crate-anicli-es-0.2.33 (c (n "anicli-es") (v "0.2.33") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "curl") (r "^0.4.39") (d #t) (k 0)) (d (n "dialoguer") (r "^0.9.0") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "ureq") (r "^2.2.0") (d #t) (k 0)))) (h "1blcvafmdzb9qzd8isv0d4syilyvjyg2b4qqrnpbb6j9x3nn0afj")))

(define-public crate-anicli-es-0.2.34 (c (n "anicli-es") (v "0.2.34") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "curl") (r "^0.4.39") (d #t) (k 0)) (d (n "dialoguer") (r "^0.9.0") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "ureq") (r "^2.2.0") (d #t) (k 0)))) (h "0k342ads9fya5g7gw7yjp58c17byr0h3mzi9ak43pkn0xg9qr4y6")))

(define-public crate-anicli-es-0.3.0 (c (n "anicli-es") (v "0.3.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "dialoguer") (r "^0.10.2") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "ureq") (r "^2.4.0") (d #t) (k 0)))) (h "0bpna3cp4irp2g0ky1lc6b0bx886nl4nwr3yj1zmczsw4zsds0pc")))

(define-public crate-anicli-es-0.3.1 (c (n "anicli-es") (v "0.3.1") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "dialoguer") (r "^0.10.2") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "ureq") (r "^2.4.0") (d #t) (k 0)))) (h "1hfxjks4sn62yyi09g0s7p2pvybbigvqbb84h57697cp2w40jn5a")))

(define-public crate-anicli-es-0.3.2 (c (n "anicli-es") (v "0.3.2") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "dialoguer") (r "^0.10.2") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "ureq") (r "^2.4.0") (d #t) (k 0)))) (h "0dx9343hmk3hkgma2hdywrxi7j0jh74i1p1qqbjzlma217nrbznl")))

(define-public crate-anicli-es-0.3.3 (c (n "anicli-es") (v "0.3.3") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "dialoguer") (r "^0.10.2") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "ureq") (r "^2.4.0") (d #t) (k 0)))) (h "1azqln8k22nlwmzibpw70hxvnj63aqakjcirrx4wvc86blyharfm")))

(define-public crate-anicli-es-0.3.31 (c (n "anicli-es") (v "0.3.31") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "dialoguer") (r "^0.10.2") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "ureq") (r "^2.4.0") (d #t) (k 0)))) (h "0n07i4inqxcbrgl11d0nsg4fw88i4xvz4g9bmrpnk2c4q231jp2i")))

