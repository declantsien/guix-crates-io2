(define-module (crates-io op ca opcalc) #:use-module (crates-io))

(define-public crate-opcalc-0.2.1 (c (n "opcalc") (v "0.2.1") (d (list (d (n "approx") (r "^0.3.2") (d #t) (k 0)) (d (n "console_error_panic_hook") (r "^0.1.6") (o #t) (d #t) (k 0)) (d (n "statrs") (r "^0.12.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.69") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.13") (d #t) (k 2)) (d (n "web-sys") (r "^0.3") (f (quote ("console"))) (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4.5") (o #t) (d #t) (k 0)))) (h "10vhwp5rlg8ax2rnm7njnmxws7r6kfnkdnpa5xm2h8lkfykdxv56") (f (quote (("default" "console_error_panic_hook"))))))

