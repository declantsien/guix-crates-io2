(define-module (crates-io op -s op-stages) #:use-module (crates-io))

(define-public crate-op-stages-0.1.0 (c (n "op-stages") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "maplit") (r "^1.0") (d #t) (k 0)) (d (n "op-composer") (r "^0.1.0") (d #t) (k 0)) (d (n "op-config") (r "^0.1.0") (d #t) (k 0)) (d (n "op-contracts") (r "^0.1.0") (d #t) (k 0)) (d (n "op-primitives") (r "^0.1.0") (d #t) (k 0)) (d (n "project-root") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "0hyz8zqs2k90f3ydk7ra60pn23mkwq3kq5p1xqfaf4p32cassaf9") (r "1.72")))

