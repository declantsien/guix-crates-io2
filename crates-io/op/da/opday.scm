(define-module (crates-io op da opday) #:use-module (crates-io))

(define-public crate-opday-0.1.0 (c (n "opday") (v "0.1.0") (d (list (d (n "base64") (r "^0.22.0") (d #t) (k 0)) (d (n "clap") (r "^4.5.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.11.3") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.30") (d #t) (k 0)) (d (n "term") (r "^0.7.0") (d #t) (k 0)) (d (n "toml") (r "^0.8.11") (d #t) (k 0)))) (h "1mnhclrch5qvg6527j0hd6y6dg7fq58y9dfxs8yk7khadhayj4p6")))

