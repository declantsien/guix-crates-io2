(define-module (crates-io op bo opbox) #:use-module (crates-io))

(define-public crate-opbox-0.1.0 (c (n "opbox") (v "0.1.0") (d (list (d (n "argparse") (r "^0.2.1") (d #t) (k 0)) (d (n "gtk") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.91") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.91") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)))) (h "0p6vskqysvfxhkmyira4sq3k1d1hprd6kqnnp4n8dflp5g5hy658")))

(define-public crate-opbox-0.1.1 (c (n "opbox") (v "0.1.1") (d (list (d (n "argparse") (r "^0.2.1") (d #t) (k 0)) (d (n "gtk") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.91") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.91") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)))) (h "1cd1q5yxfdrfkf79d71rn4cg1lfxylacrr8ccxgm9bbxaxl3k3pl")))

