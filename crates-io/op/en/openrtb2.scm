(define-module (crates-io op en openrtb2) #:use-module (crates-io))

(define-public crate-openrtb2-0.1.0 (c (n "openrtb2") (v "0.1.0") (d (list (d (n "default-ext") (r "^0.1") (d #t) (k 0)) (d (n "json-ext") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_repr") (r "^0.1") (d #t) (k 0)))) (h "0inqww361x931w4cg9w9y1z9ifb9q7bk4aims7wlnj38y285220j")))

(define-public crate-openrtb2-0.2.0 (c (n "openrtb2") (v "0.2.0") (d (list (d (n "default-ext") (r "^0.1") (d #t) (k 0)) (d (n "json-ext") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "serde_repr") (r "^0.1") (d #t) (k 0)))) (h "0s9k46fsjdd9sfhmyinv0f3rb34n27dghp62fa78kgx50g2kfpir")))

(define-public crate-openrtb2-0.3.0 (c (n "openrtb2") (v "0.3.0") (d (list (d (n "default-ext") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_repr") (r "^0.1") (d #t) (k 0)))) (h "0sbnq1i6frfahvm0ki4fcyhlbq313qpf0273sz9y3c8acgh3h2br")))

