(define-module (crates-io op en openhuman) #:use-module (crates-io))

(define-public crate-openhuman-0.1.0 (c (n "openhuman") (v "0.1.0") (d (list (d (n "actix-web") (r "^3.3.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "time") (r "^0.1.12") (d #t) (k 0)))) (h "1d7rdb641f4s7jd2kwlp7aamv4cpvqx1splg14a0a6xhprg749gg")))

