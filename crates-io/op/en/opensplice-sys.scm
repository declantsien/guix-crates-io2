(define-module (crates-io op en opensplice-sys) #:use-module (crates-io))

(define-public crate-opensplice-sys-0.0.1 (c (n "opensplice-sys") (v "0.0.1") (d (list (d (n "bindgen") (r "^0.40.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "1rqicq9k41p5y0p1c4asjb3c655n7x6nm0p1fg9ic9nd0qmd0kh4")))

(define-public crate-opensplice-sys-0.0.2 (c (n "opensplice-sys") (v "0.0.2") (d (list (d (n "bindgen") (r "^0.40.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "1r8vlly73jcgslvii36jzh2116zzcll3wgbsp7a9hxc1vg30254p")))

(define-public crate-opensplice-sys-0.0.3 (c (n "opensplice-sys") (v "0.0.3") (h "0j47gps5fm7zjih4rax3apnb855i585m5iawf79wla7h0l4arbs9")))

