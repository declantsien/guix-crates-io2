(define-module (crates-io op en opentracingrust) #:use-module (crates-io))

(define-public crate-opentracingrust-0.1.0 (c (n "opentracingrust") (v "0.1.0") (d (list (d (n "rand") (r "^0.4.0") (d #t) (k 0)))) (h "1jxr3isnw8plgk8ipf4cwhigp2sgx2dndwhd903lgswdx3azab5p")))

(define-public crate-opentracingrust-0.1.1 (c (n "opentracingrust") (v "0.1.1") (d (list (d (n "rand") (r "^0.4.0") (d #t) (k 0)))) (h "1iyra8gjpwj5ssnf040xwyln0fkdmm823nb7bd2936k0xy7dhgqa")))

(define-public crate-opentracingrust-0.2.0 (c (n "opentracingrust") (v "0.2.0") (d (list (d (n "rand") (r "^0.4.0") (d #t) (k 0)))) (h "00c9ilhjfa3yp89xf2ajwadf27w1viiwxiynzvcrpmam2x8ancy0")))

(define-public crate-opentracingrust-0.2.1 (c (n "opentracingrust") (v "0.2.1") (d (list (d (n "rand") (r "^0.4.0") (d #t) (k 0)))) (h "1q2gx2h81l0b765jd0mgmb686igwaj7hpz2sip2d7xlhzd7ixbf9")))

(define-public crate-opentracingrust-0.3.0 (c (n "opentracingrust") (v "0.3.0") (d (list (d (n "crossbeam-channel") (r "^0.1.2") (d #t) (k 0)) (d (n "rand") (r "^0.4.0") (d #t) (k 0)))) (h "0q5b1g3brpln2dy99scbkyqks0ww651ymax7xh9snpgvckpk5gzm")))

(define-public crate-opentracingrust-0.3.1 (c (n "opentracingrust") (v "0.3.1") (d (list (d (n "crossbeam-channel") (r "^0.1.2") (d #t) (k 0)) (d (n "rand") (r "^0.4.0") (d #t) (k 0)))) (h "1f07zv26an3dk57k5amjjrwfp6mpav7wy0f8n0cshlxna0qxp8ws")))

(define-public crate-opentracingrust-0.3.2 (c (n "opentracingrust") (v "0.3.2") (d (list (d (n "crossbeam-channel") (r "^0.3.8") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)))) (h "14lflj37zka6xn8cdhxwixihphys7znwpd7ayj0d06kqpkcb2a4p")))

(define-public crate-opentracingrust-0.3.3 (c (n "opentracingrust") (v "0.3.3") (d (list (d (n "crossbeam-channel") (r "^0.3.8") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)))) (h "16nvz2z693i9zmngk090p6dww1989kzaizp2ph1gcf8kjjrxmjca")))

(define-public crate-opentracingrust-0.3.4 (c (n "opentracingrust") (v "0.3.4") (d (list (d (n "crossbeam-channel") (r "^0.3.8") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)))) (h "1rscg76m35faxzpp6pa5716zj8358wdy4hbxrd6kvja1gc2cf3i9")))

(define-public crate-opentracingrust-0.4.0 (c (n "opentracingrust") (v "0.4.0") (d (list (d (n "crossbeam-channel") (r "^0.4.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0qd1gn4xivf6inr7a3ishc811lz1kd8yfk4ylzlm1z63lx6rvhdc")))

