(define-module (crates-io op en openexchangerates) #:use-module (crates-io))

(define-public crate-openexchangerates-0.1.0 (c (n "openexchangerates") (v "0.1.0") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "hyper") (r "^0.8") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "yup-hyper-mock") (r "^1.3") (d #t) (k 0)))) (h "1r0bacamkqzv33wl68zp5s3xppha22cwc64avyb345xashg8qlc8")))

(define-public crate-openexchangerates-0.1.2 (c (n "openexchangerates") (v "0.1.2") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "hyper") (r "^0.8") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "yup-hyper-mock") (r "^1.3") (d #t) (k 0)))) (h "1c5l7yi4kw2b7ygfnvajl641hzpiz1bhj00163g1vd2sy8if0ykf")))

(define-public crate-openexchangerates-0.1.3 (c (n "openexchangerates") (v "0.1.3") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "hyper") (r "^0.8") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "yup-hyper-mock") (r "^1.3") (d #t) (k 0)))) (h "1c03799yfnbvrz2j81rp2xk2dv0ar4v0a56lm40i10z4hipdyram")))

(define-public crate-openexchangerates-0.1.5 (c (n "openexchangerates") (v "0.1.5") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "hyper") (r "^0.8") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "yup-hyper-mock") (r "^1.3") (d #t) (k 0)))) (h "0pq06d3a88wnfjvnpnns4haxqdqd70dba2spy1p08i9rvp74xcn6")))

(define-public crate-openexchangerates-0.1.6 (c (n "openexchangerates") (v "0.1.6") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "hyper") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "yup-hyper-mock") (r "^1.3") (d #t) (k 2)))) (h "0x6wkxvjnxmqm185n531lw5wxxb417mb0dn9gp80w9q8l3zg1prc")))

(define-public crate-openexchangerates-0.1.7 (c (n "openexchangerates") (v "0.1.7") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "hyper") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_codegen") (r "^0.8") (o #t) (d #t) (k 1)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)) (d (n "serde_macros") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "yup-hyper-mock") (r "^1.3") (d #t) (k 2)))) (h "04x2a2aigifdlcn4q7l89pc0iwkam6zf70mn66da8jgsk0fgphq5") (f (quote (("unstable" "serde_macros") ("default" "serde_codegen"))))))

