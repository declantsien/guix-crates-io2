(define-module (crates-io op en openslide) #:use-module (crates-io))

(define-public crate-openslide-0.1.0 (c (n "openslide") (v "0.1.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "image") (r "^0.19") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1092ifsjyp7vqn44jjwcd4g0d5mavy137cvzv26kj8nmdn9xk85i")))

(define-public crate-openslide-0.2.0 (c (n "openslide") (v "0.2.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "clap") (r "^2.32") (o #t) (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "float-cmp") (r "^0.4") (d #t) (k 2)) (d (n "image") (r "^0.20") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num") (r "^0.2") (d #t) (k 0)))) (h "1954kvrhlkss67ja80bxpxc8ij7bqy0abzj1syf3j971qc5h06jv") (f (quote (("binaries" "clap"))))))

