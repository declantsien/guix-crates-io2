(define-module (crates-io op en openssl-magic-public-key) #:use-module (crates-io))

(define-public crate-openssl-magic-public-key-0.1.0 (c (n "openssl-magic-public-key") (v "0.1.0") (d (list (d (n "base64") (r "^0.10") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (d #t) (k 0)))) (h "0pb201zngpijbdymcz4hb20jfxn1qqcl3za38r4v0hhlm2lfmcmk")))

