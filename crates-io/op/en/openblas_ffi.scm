(define-module (crates-io op en openblas_ffi) #:use-module (crates-io))

(define-public crate-openblas_ffi-0.0.1 (c (n "openblas_ffi") (v "0.0.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "walkdir") (r "^1") (d #t) (k 1)))) (h "11r878q9iyz18jw0gn9dpv5h23pqc1gmdrdg024g6qwxfsrn48xw")))

(define-public crate-openblas_ffi-0.1.0 (c (n "openblas_ffi") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "walkdir") (r "^1") (d #t) (k 1)))) (h "14hbi93kbxrh7r66crmqwvp7w46xjf7z91dd7d8bspdf0ynbs8aw")))

(define-public crate-openblas_ffi-0.1.1 (c (n "openblas_ffi") (v "0.1.1") (d (list (d (n "cblas_ffi") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "walkdir") (r "^1") (d #t) (k 1)))) (h "0shjhxishl1ydjnl4m6cbxg0q8m1rajph2g7mjya8nb1151vg1g1")))

