(define-module (crates-io op en openssl_enc) #:use-module (crates-io))

(define-public crate-openssl_enc-0.1.0 (c (n "openssl_enc") (v "0.1.0") (d (list (d (n "openssl") (r "^0.10") (d #t) (k 0)) (d (n "ring") (r "^0.16.11") (d #t) (k 0)))) (h "11nna9b40yny7r0az66xd5gsgcpqk89ibfax7sskiqschp15lpar")))

