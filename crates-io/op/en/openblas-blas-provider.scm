(define-module (crates-io op en openblas-blas-provider) #:use-module (crates-io))

(define-public crate-openblas-blas-provider-0.0.1 (c (n "openblas-blas-provider") (v "0.0.1") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "171hfmzk3j6rjlqmn6jgrb53v6zsfp7j5ac8fadrlxj090khyvy3")))

(define-public crate-openblas-blas-provider-0.0.2 (c (n "openblas-blas-provider") (v "0.0.2") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "0zj7lm9z0lxkqmv0zwvg9f62676v7b4yr4hx1g4nv8z3xqhfan8j") (f (quote (("system-openblas") ("static-openblas"))))))

(define-public crate-openblas-blas-provider-0.0.3 (c (n "openblas-blas-provider") (v "0.0.3") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "1qgmad3ijl6ciz40r47mpn77p0m602r2sgz2z4x1wjhmg1wv2cfc") (f (quote (("system-openblas") ("static-openblas"))))))

(define-public crate-openblas-blas-provider-0.0.4 (c (n "openblas-blas-provider") (v "0.0.4") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "18ac74hfb40xijv913ng1abal274i68s6dpr7hnqzkifl9hxd5p4") (f (quote (("system-openblas") ("static-openblas"))))))

(define-public crate-openblas-blas-provider-0.0.5 (c (n "openblas-blas-provider") (v "0.0.5") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "0bngh7bvp36zyfnczsc083iamwhhazgkjjdazd21wagr2pc7v7f4") (f (quote (("system-openblas") ("static-openblas"))))))

