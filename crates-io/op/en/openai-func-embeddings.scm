(define-module (crates-io op en openai-func-embeddings) #:use-module (crates-io))

(define-public crate-openai-func-embeddings-0.3.0 (c (n "openai-func-embeddings") (v "0.3.0") (d (list (d (n "async-openai") (r "^0.18.2") (d #t) (k 0)) (d (n "rkyv") (r "^0.7.44") (f (quote ("validation"))) (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("sync"))) (d #t) (k 0)))) (h "0r0x7cvnicq25fznrsdzwn4dkgmd307pvwfk4lrdqmnazz0b4y0v")))

(define-public crate-openai-func-embeddings-0.3.1 (c (n "openai-func-embeddings") (v "0.3.1") (d (list (d (n "async-openai") (r "^0.18.2") (d #t) (k 0)) (d (n "rkyv") (r "^0.7.44") (f (quote ("validation"))) (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("sync"))) (d #t) (k 0)))) (h "0sa5jqsiq7h4v0n0zpcc2zbj39acidbb4sb8b324hjalzag0aqra")))

(define-public crate-openai-func-embeddings-0.4.0 (c (n "openai-func-embeddings") (v "0.4.0") (d (list (d (n "async-openai") (r "^0.19.0") (d #t) (k 0)) (d (n "rkyv") (r "^0.7.44") (f (quote ("validation"))) (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("sync"))) (d #t) (k 0)))) (h "02m6a99h392qzgbckgs9nqgzbyjc69wzmi0pic94ih5sizl60pkr")))

