(define-module (crates-io op en openjpeg2-sys) #:use-module (crates-io))

(define-public crate-openjpeg2-sys-0.1.0 (c (n "openjpeg2-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.32.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1.29") (d #t) (k 1)) (d (n "gcc") (r "^0.3.54") (d #t) (k 1)))) (h "0sd68ljz1z6wjg35a37x2bb2birhxkv4xldl6cq28nyczgi8pak0")))

(define-public crate-openjpeg2-sys-0.1.1 (c (n "openjpeg2-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.35.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1.29") (d #t) (k 1)) (d (n "gcc") (r "^0.3.54") (d #t) (k 1)))) (h "11cmsfnv1ji07adqkf2psz79wcdbc9aqxlk9p326rlcg86sv1f3l")))

(define-public crate-openjpeg2-sys-0.1.2 (c (n "openjpeg2-sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.40") (d #t) (k 1)) (d (n "cmake") (r "^0.1.29") (d #t) (k 1)) (d (n "gcc") (r "^0.3.54") (d #t) (k 1)))) (h "1w3r7izcy5n3j2qb3j10iwbvj0q9ibshsj7iyqcpsm7p2ivqv2zj") (l "openjp2")))

(define-public crate-openjpeg2-sys-0.1.3 (c (n "openjpeg2-sys") (v "0.1.3") (d (list (d (n "bindgen") (r "^0.40") (d #t) (k 1)) (d (n "cmake") (r "^0.1.29") (d #t) (k 1)) (d (n "gcc") (r "^0.3.54") (d #t) (k 1)))) (h "1cba1ymqyh7prdp08gmy98xd3maq4y1yi4jx1q4177db3fy9qgsf") (l "openjp2")))

(define-public crate-openjpeg2-sys-0.1.4 (c (n "openjpeg2-sys") (v "0.1.4") (d (list (d (n "bindgen") (r "^0.51") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1.29") (d #t) (k 1)))) (h "1vqvgd0w26mpchdh37ailn6jws0v2d7s8kl09j776xlg16dywj28") (l "openjp2")))

