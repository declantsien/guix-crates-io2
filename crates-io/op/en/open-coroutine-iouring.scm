(define-module (crates-io op en open-coroutine-iouring) #:use-module (crates-io))

(define-public crate-open-coroutine-iouring-0.5.0 (c (n "open-coroutine-iouring") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 2)) (d (n "cc") (r "^1.0.82") (d #t) (t "cfg(target_os = \"linux\")") (k 1)) (d (n "cfg-if") (r "^1.0.0") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 1)) (d (n "io-uring") (r "^0.6.1") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "libc") (r "^0.2.147") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "slab") (r "^0.4.8") (d #t) (k 2)))) (h "0k4jdmf4pj6pxkx4ix9syrqpibz53gx08ipk1b0ydvxqcz14w7qs")))

