(define-module (crates-io op en open-coroutine) #:use-module (crates-io))

(define-public crate-open-coroutine-0.0.1 (c (n "open-coroutine") (v "0.0.1") (d (list (d (n "base-coroutine") (r "^0.0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.119") (d #t) (k 0)) (d (n "libhook") (r "^0.0.1") (d #t) (k 0)))) (h "12p2v1mncyhnwqvij3v1gcr1466j2l5lcfd10jkya9p2f91ggp7x")))

(define-public crate-open-coroutine-0.0.2 (c (n "open-coroutine") (v "0.0.2") (d (list (d (n "base-coroutine") (r "^0.0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.119") (d #t) (k 0)) (d (n "libhook") (r "^0.0.2") (d #t) (k 0)))) (h "1sfgmmxx5knd4j2h4hfpyiicj3lxzjfgkywmhn01nvj0dcbs6y53")))

(define-public crate-open-coroutine-0.0.3 (c (n "open-coroutine") (v "0.0.3") (d (list (d (n "base-coroutine") (r "^0.0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.119") (d #t) (k 0)) (d (n "libhook") (r "^0.0.2") (d #t) (k 0)))) (h "0p9frxi5k1x0afdjbgj7rpj4hnaijc0cmjnc44cf9jyysknnz6cm")))

(define-public crate-open-coroutine-0.0.4 (c (n "open-coroutine") (v "0.0.4") (d (list (d (n "base-coroutine") (r "^0.0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.119") (d #t) (k 0)))) (h "0ri0h1k15606xi91vgf6yj967ai6ai0za240wb09qi1k2wqxbfqj")))

(define-public crate-open-coroutine-0.0.5 (c (n "open-coroutine") (v "0.0.5") (d (list (d (n "base-coroutine") (r "^0.0.1") (d #t) (k 0)) (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "libc") (r "^0.2.119") (d #t) (k 0)))) (h "0p3m7km27d9vzxzjw8nbhplk76ji7sbd0hfwpyz57s49zcj0g2gv")))

(define-public crate-open-coroutine-0.0.6 (c (n "open-coroutine") (v "0.0.6") (d (list (d (n "base-coroutine") (r "^0.0.1") (d #t) (k 0)))) (h "1bxhbpd1pf7alpq0p4412ba75brvmby96akal02glj8l3l1li2fb")))

(define-public crate-open-coroutine-0.0.7 (c (n "open-coroutine") (v "0.0.7") (d (list (d (n "base-coroutine") (r "^0.0.2") (d #t) (k 0)) (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "libc") (r "^0.2.119") (d #t) (k 0)))) (h "0f852rfv331y1lbl6fsw33xvxfad8ky8756lviz2qrfi6f6q2vmr")))

(define-public crate-open-coroutine-0.1.0 (c (n "open-coroutine") (v "0.1.0") (d (list (d (n "base-coroutine") (r "^0.1.0") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "libc") (r "^0.2.119") (d #t) (k 0)))) (h "1bnbh85z8gz61cr038l4jaz719arb5g4l8lqg71zcrfnzi8ghl95")))

(define-public crate-open-coroutine-0.2.0 (c (n "open-coroutine") (v "0.2.0") (d (list (d (n "base-coroutine") (r "^0.2.0") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "libc") (r "^0.2.119") (d #t) (k 0)) (d (n "open-coroutine-macros") (r "^0.1.0") (d #t) (k 0)))) (h "0dg14mgnnmmrkqwg1gn9gry11iwfqxj1jfxfykh74hixlrv9i9yv")))

(define-public crate-open-coroutine-0.4.0 (c (n "open-coroutine") (v "0.4.0") (d (list (d (n "glob") (r "^0.3.1") (d #t) (k 1)) (d (n "libc") (r "^0.2.119") (d #t) (k 0)) (d (n "open-coroutine-core") (r "^0.4.0") (d #t) (k 0)) (d (n "open-coroutine-hooks") (r "^0.4.0") (d #t) (k 0)) (d (n "open-coroutine-macros") (r "^0.1.0") (d #t) (k 0)))) (h "05by23mj66qqm2zdyz1fz8g22azjmvq5cwc9a3ncmys008sn425g")))

(define-public crate-open-coroutine-0.4.1 (c (n "open-coroutine") (v "0.4.1") (d (list (d (n "glob") (r "^0.3.1") (d #t) (k 1)) (d (n "libc") (r "^0.2.119") (d #t) (k 0)) (d (n "open-coroutine-core") (r "^0.4.0") (d #t) (k 0)) (d (n "open-coroutine-hooks") (r "^0.4.0") (d #t) (k 0)) (d (n "open-coroutine-macros") (r "^0.4.0") (d #t) (k 0)))) (h "0yy1dnv1w4gfwr7arwvzhjk0wv1b8pijy2ryjiwsxays95iwcsc2")))

(define-public crate-open-coroutine-0.4.2 (c (n "open-coroutine") (v "0.4.2") (d (list (d (n "glob") (r "^0.3.1") (d #t) (k 1)) (d (n "libc") (r "^0.2.119") (d #t) (k 0)) (d (n "open-coroutine-core") (r "^0.4.0") (d #t) (k 0)) (d (n "open-coroutine-hooks") (r "^0.4.0") (d #t) (k 0)) (d (n "open-coroutine-macros") (r "^0.1.0") (d #t) (k 0)))) (h "1vc8vn48v781y4wn37l2861ybrdmjgchq59w3vs1v92m6byhp3yl")))

(define-public crate-open-coroutine-0.4.3 (c (n "open-coroutine") (v "0.4.3") (d (list (d (n "glob") (r "^0.3.1") (d #t) (k 1)) (d (n "libc") (r "^0.2.119") (d #t) (k 0)) (d (n "open-coroutine-core") (r "^0.4.0") (d #t) (k 0)) (d (n "open-coroutine-hooks") (r "^0.4.0") (d #t) (k 0)) (d (n "open-coroutine-macros") (r "^0.1.0") (d #t) (k 0)))) (h "1kq2fjxwfs2abk30vfv6dmm6bv9325b8y2sc3ak0a0j4gzan9h0i")))

(define-public crate-open-coroutine-0.4.4 (c (n "open-coroutine") (v "0.4.4") (d (list (d (n "glob") (r "^0.3.1") (d #t) (k 1)) (d (n "libc") (r "^0.2.119") (d #t) (k 0)) (d (n "open-coroutine-core") (r "^0.4.0") (d #t) (k 0)) (d (n "open-coroutine-hooks") (r "^0.4.0") (d #t) (k 0)) (d (n "open-coroutine-macros") (r "^0.1.0") (d #t) (k 0)))) (h "0pwx6pnj6nay3fjz52m3x3lz41c5s2wglybjnyp2bhnla9xdx6dk")))

(define-public crate-open-coroutine-0.4.5 (c (n "open-coroutine") (v "0.4.5") (d (list (d (n "libc") (r "^0.2.119") (d #t) (k 0)) (d (n "open-coroutine-core") (r "^0.4.0") (d #t) (k 0)) (d (n "open-coroutine-macros") (r "^0.1.0") (d #t) (k 0)))) (h "1vs54xqqnanp694pqv7jyx9pyra2fgsp6qz6d56x0pg0kbw097r1")))

(define-public crate-open-coroutine-0.4.6 (c (n "open-coroutine") (v "0.4.6") (d (list (d (n "libc") (r "^0.2.119") (d #t) (k 0)) (d (n "open-coroutine-core") (r "^0.4.0") (d #t) (k 0)) (d (n "open-coroutine-macros") (r "^0.1.1") (d #t) (k 0)))) (h "193wbbdcbszayxn30jh5znjcqijrm8x9bsv27sa8rxs1b2py3b5b")))

(define-public crate-open-coroutine-0.5.0 (c (n "open-coroutine") (v "0.5.0") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "glob") (r "^0.3.1") (d #t) (k 1)) (d (n "libc") (r "^0.2.119") (d #t) (k 0)) (d (n "open-coroutine-core") (r "^0.5.0") (d #t) (k 0)) (d (n "open-coroutine-hooks") (r "^0.5.0") (d #t) (k 0)) (d (n "open-coroutine-macros") (r "^0.5.0") (d #t) (k 0)))) (h "11gnkq6qx8qvlh343by626vrzdjfnl5lqgqg8vdny7c5kb25hia2") (f (quote (("preemptive-schedule" "open-coroutine-core/preemptive-schedule" "open-coroutine-hooks/preemptive-schedule") ("logs" "open-coroutine-core/logs" "open-coroutine-hooks/logs") ("io_uring" "open-coroutine-core/io_uring" "open-coroutine-hooks/io_uring") ("full" "preemptive-schedule" "io_uring" "logs") ("default" "full"))))))

