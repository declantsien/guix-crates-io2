(define-module (crates-io op en openair) #:use-module (crates-io))

(define-public crate-openair-0.1.2 (c (n "openair") (v "0.1.2") (d (list (d (n "env_logger") (r "^0.6") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1942n2628kix9x1mgm78y4pa8z5abvxg9mbsip8ahcm0qbq4yp0s")))

(define-public crate-openair-0.1.3 (c (n "openair") (v "0.1.3") (d (list (d (n "env_logger") (r "^0.6") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1brmw0w2wfhb3np31vi7iwxb6h3jc5qf0qh7wdnzhaj0kr7l6p5h")))

(define-public crate-openair-0.1.4 (c (n "openair") (v "0.1.4") (d (list (d (n "env_logger") (r "^0.6") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1700mak3iqn23mxgpfx0z4jbsl0i4fsljiii67c2iyird1src6fc")))

(define-public crate-openair-0.2.0 (c (n "openair") (v "0.2.0") (d (list (d (n "env_logger") (r "^0.6") (d #t) (k 2)) (d (n "indoc") (r "^0.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1qal72nvddk60h8w6fklihrkv9f96wvc209l3g68bajss1zs8ijm")))

