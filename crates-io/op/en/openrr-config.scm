(define-module (crates-io op en openrr-config) #:use-module (crates-io))

(define-public crate-openrr-config-0.0.6 (c (n "openrr-config") (v "0.0.6") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "assert_approx_eq") (r "^1") (d #t) (k 2)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "toml-query") (r "^0.10") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (f (quote ("log"))) (d #t) (k 0)))) (h "1vb7r8x8j705br4y3i09hzxlgfbz1422zr34x0j5fk7cx17mnhyw")))

(define-public crate-openrr-config-0.0.7 (c (n "openrr-config") (v "0.0.7") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "assert_approx_eq") (r "^1") (d #t) (k 2)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "toml-query") (r "^0.10") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (f (quote ("log"))) (d #t) (k 0)))) (h "0ni1nkq921laa5lcxskqwl0f58bzb33f3dcbzmhyg6c98jc1kpqb")))

(define-public crate-openrr-config-0.1.0 (c (n "openrr-config") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "toml-query") (r "^0.10") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (f (quote ("log"))) (d #t) (k 0)) (d (n "assert_approx_eq") (r "^1.1") (d #t) (k 2)))) (h "19h7dai2mjmfrc9n3zrx4rzzpmvg2g60x9l53ckcvrsd8ncfqf0h")))

