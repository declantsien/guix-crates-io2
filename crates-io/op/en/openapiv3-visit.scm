(define-module (crates-io op en openapiv3-visit) #:use-module (crates-io))

(define-public crate-openapiv3-visit-0.1.0 (c (n "openapiv3-visit") (v "0.1.0") (d (list (d (n "openapiv3") (r "^1.0.2") (d #t) (k 0)))) (h "1g0jbcgak2763m89y5z67s0xycfcllmwyj17q33w74kjld83vpgz")))

(define-public crate-openapiv3-visit-0.2.0 (c (n "openapiv3-visit") (v "0.2.0") (d (list (d (n "openapiv3") (r "^1.0.2") (d #t) (k 0)))) (h "1h722smpdaip1ryvn67q0dqn0ylqv2f5yijzf91b6w2rw2y3yh1x")))

(define-public crate-openapiv3-visit-0.3.0 (c (n "openapiv3-visit") (v "0.3.0") (d (list (d (n "openapiv3") (r "^1.0.2") (d #t) (k 0)) (d (n "openapiv3") (r "^1.0.2") (d #t) (k 2)) (d (n "serde_yaml") (r "^0.9.19") (d #t) (k 2)))) (h "01kh4c5ax5yi6rqhbd5a60kwiz9p813igrdmzysh02b5n4k6zbsc")))

