(define-module (crates-io op en openapi-trustfall-adapter) #:use-module (crates-io))

(define-public crate-openapi-trustfall-adapter-0.1.0 (c (n "openapi-trustfall-adapter") (v "0.1.0") (d (list (d (n "openapiv3") (r "^2.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.34") (d #t) (k 0)) (d (n "trustfall") (r "^0.7.1") (d #t) (k 0)) (d (n "yaml-hash") (r "^0.3.0") (d #t) (k 0)))) (h "06rng4ykvl1ajawlzcf23h011b94hxpz7k1rvx82lis19bqgmf1m")))

(define-public crate-openapi-trustfall-adapter-0.1.1 (c (n "openapi-trustfall-adapter") (v "0.1.1") (d (list (d (n "openapiv3") (r "^2.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.34") (d #t) (k 0)) (d (n "trustfall") (r "^0.7.1") (d #t) (k 0)) (d (n "yaml-hash") (r "^0.3.0") (d #t) (k 0)))) (h "0m0d9m1caxnk662vqzvnsyn64p8m29ldlccl1mlic3w3zppf6av8")))

(define-public crate-openapi-trustfall-adapter-0.2.0 (c (n "openapi-trustfall-adapter") (v "0.2.0") (d (list (d (n "openapiv3") (r "^2.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.34") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)) (d (n "trustfall") (r "^0.7.1") (d #t) (k 0)) (d (n "yaml-hash") (r "^0.3.0") (d #t) (k 0)))) (h "1kfkfvfvda44psdgfsik3chx8ch1lfq2n3bxgzf1vki0hd996x1l")))

