(define-module (crates-io op en openai-lib) #:use-module (crates-io))

(define-public crate-openai-lib-0.1.0 (c (n "openai-lib") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart" "rustls-tls"))) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_with") (r "^2.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)) (d (n "uuid") (r "^1.0") (f (quote ("serde"))) (d #t) (k 0)))) (h "13w2y29dwnd6fv5jiyjwxsn6n4zihqz4qd7mj2jl550qz309g6r3")))

