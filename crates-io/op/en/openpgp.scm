(define-module (crates-io op en openpgp) #:use-module (crates-io))

(define-public crate-openpgp-0.1.0 (c (n "openpgp") (v "0.1.0") (d (list (d (n "bitflags") (r "^0.7") (d #t) (k 0)) (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "env_logger") (r "^0.3") (d #t) (k 2)) (d (n "flate2") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "openssl") (r "^0.7") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "040paf91d60wvxpx1cy03w0wc54khc4l8ip0hx6yc77ifyyn4r90")))

(define-public crate-openpgp-0.2.0 (c (n "openpgp") (v "0.2.0") (d (list (d (n "bitflags") (r "^0.7") (d #t) (k 0)) (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "env_logger") (r "^0.3") (d #t) (k 2)) (d (n "flate2") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "openssl") (r "^0.7") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "13midjwqbal66jbrsr12sf6ii1kvngx75v7s3z3gxlzssrzgbicp")))

(define-public crate-openpgp-0.3.0 (c (n "openpgp") (v "0.3.0") (h "1f54fgab8455vi0x7g49rcmz0n6bv5ygnz618vjbkhih8s13ky31")))

