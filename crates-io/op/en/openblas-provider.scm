(define-module (crates-io op en openblas-provider) #:use-module (crates-io))

(define-public crate-openblas-provider-0.0.5 (c (n "openblas-provider") (v "0.0.5") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "15qj3m1b6lg90xmjb511fjhw9j6qhcrsx13mrbh27bxb5sdwyxp4") (f (quote (("system-openblas") ("static-openblas"))))))

(define-public crate-openblas-provider-0.1.0 (c (n "openblas-provider") (v "0.1.0") (h "1hj3gkjzly48fqgycxnr6c3sgg99lw5z5b4m0mnyzvisr9bb5kdw") (f (quote (("system-openblas") ("static-openblas") ("exclude-cblas"))))))

(define-public crate-openblas-provider-0.1.1 (c (n "openblas-provider") (v "0.1.1") (h "1nf503bj3vlxknlddsnqzwpmmddl754khi8frivs1q324iwrvhrz") (f (quote (("system-openblas") ("static-openblas") ("exclude-cblas"))))))

(define-public crate-openblas-provider-0.1.2 (c (n "openblas-provider") (v "0.1.2") (h "18anj4a4z9gwzih1v4i6jryfk2w43fy6l3v9xqncqdc9m2kzc9yd") (f (quote (("system-openblas") ("static-openblas") ("exclude-cblas"))))))

(define-public crate-openblas-provider-0.1.3 (c (n "openblas-provider") (v "0.1.3") (h "0md65rn3ifnblg0jsdgm13jjhbl9zg1al9q0kzxqpm29z7cwkr5p") (f (quote (("system-openblas") ("static-openblas") ("exclude-cblas"))))))

(define-public crate-openblas-provider-0.1.4 (c (n "openblas-provider") (v "0.1.4") (h "0p30vgzw0d37lf5ad4nl5w9ixn83hmjb65g0c9l3j4pbdjskf34d") (f (quote (("system-openblas") ("static-openblas") ("exclude-cblas"))))))

(define-public crate-openblas-provider-0.1.5 (c (n "openblas-provider") (v "0.1.5") (h "1kzzr1hkyzg9bdw0ya2za13vzzsz81w760xs37viwhz7wli038mv") (f (quote (("system-openblas") ("static-openblas") ("exclude-cblas"))))))

(define-public crate-openblas-provider-0.1.6 (c (n "openblas-provider") (v "0.1.6") (h "1h514z1a8zjrzx0ba81415dwnd9czbdwwrmqn8gyyzxghbzwqfxb") (f (quote (("system-openblas") ("static-openblas") ("exclude-cblas"))))))

(define-public crate-openblas-provider-0.1.7 (c (n "openblas-provider") (v "0.1.7") (h "1m4svk2syxy1pylv0sz93m9gvsp47sslkpgmaskb6dyr518xwp0p") (f (quote (("system-openblas") ("static-openblas") ("exclude-cblas"))))))

(define-public crate-openblas-provider-0.2.0 (c (n "openblas-provider") (v "0.2.0") (h "0v3rfw20p93wwz27wymb1i0s45lrz7xzhbqc8b3z9nj66hzp81m9") (f (quote (("system-openblas") ("static-openblas") ("exclude-cblas"))))))

(define-public crate-openblas-provider-0.3.0 (c (n "openblas-provider") (v "0.3.0") (h "03152zylr2jyvczf0bic7d6pbx6yh04baabpvfn0lqfymxajfmld") (f (quote (("system-openblas") ("static-openblas") ("include-cblas") ("default" "include-cblas"))))))

(define-public crate-openblas-provider-0.4.0 (c (n "openblas-provider") (v "0.4.0") (h "1yay49xwqh67rw834iy79nbi9mqczlnm558rbkjmbvm6zzkln6xs") (f (quote (("system") ("static") ("default" "cblas") ("cblas"))))))

(define-public crate-openblas-provider-0.4.1 (c (n "openblas-provider") (v "0.4.1") (h "14cfz0rjj4gisqmj6110qnqx5fap159sf3fdfi2ylkdr0wkg2d82") (f (quote (("system") ("static") ("default" "cblas") ("cblas"))))))

