(define-module (crates-io op en opencomputers-stem) #:use-module (crates-io))

(define-public crate-opencomputers-stem-0.1.0 (c (n "opencomputers-stem") (v "0.1.0") (d (list (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-util") (r "^0.3") (f (quote ("codec"))) (d #t) (k 0)))) (h "1r5wqw0r2vymnwx7lhik2z2kim6rpf8xvvhkzqrfip99jv5kagzq")))

(define-public crate-opencomputers-stem-0.1.1 (c (n "opencomputers-stem") (v "0.1.1") (d (list (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-util") (r "^0.3") (f (quote ("codec"))) (d #t) (k 0)))) (h "1s8cn3lxcj4zndbaj8bhhqdvhjq526f4l9lvsynn2gppl3limhsl")))

(define-public crate-opencomputers-stem-0.1.2 (c (n "opencomputers-stem") (v "0.1.2") (d (list (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-util") (r "^0.3") (f (quote ("codec"))) (d #t) (k 0)))) (h "17nxbrxckck7y9f6g0058zkqvrm688xwwsc9g3vpzrn29sc7w5wy")))

(define-public crate-opencomputers-stem-0.1.3 (c (n "opencomputers-stem") (v "0.1.3") (d (list (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("tcp" "dns"))) (d #t) (k 0)) (d (n "tokio-util") (r "^0.3") (f (quote ("codec"))) (d #t) (k 0)))) (h "1bi32g9vc43dahqywj7fkzd99b4dxf79jfl9sybip8v3gg6rfc5l")))

(define-public crate-opencomputers-stem-0.2.0 (c (n "opencomputers-stem") (v "0.2.0") (d (list (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("tcp" "dns"))) (d #t) (k 0)) (d (n "tokio-util") (r "^0.3") (f (quote ("codec"))) (d #t) (k 0)))) (h "0060jl4pcjwx0z49iiwfp549a87m7x7i4insb54z9qxagbqydzbr")))

