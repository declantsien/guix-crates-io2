(define-module (crates-io op en opentelemetry-jaeger-propagator) #:use-module (crates-io))

(define-public crate-opentelemetry-jaeger-propagator-0.1.0 (c (n "opentelemetry-jaeger-propagator") (v "0.1.0") (d (list (d (n "opentelemetry") (r "^0.22") (f (quote ("trace"))) (k 0)))) (h "09h895ryxj0wa7rpk43078ym5wl6z4j2dfc9m85bcdy5xxifrd5y") (f (quote (("default")))) (r "1.65")))

(define-public crate-opentelemetry-jaeger-propagator-0.2.0 (c (n "opentelemetry-jaeger-propagator") (v "0.2.0") (d (list (d (n "opentelemetry") (r "^0.23") (f (quote ("trace"))) (k 0)))) (h "0v0xzs6199s987mjnpmg3vfqdika8iq4dycn70s0kjgf19g7b461") (f (quote (("default")))) (r "1.65")))

