(define-module (crates-io op en openai-magic-instantiate-derive) #:use-module (crates-io))

(define-public crate-openai-magic-instantiate-derive-0.1.0 (c (n "openai-magic-instantiate-derive") (v "0.1.0") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1wn0y4x7a9wcnb2vlcf1009p26wkbgnp46cynh1z5h2190mmxmn7")))

(define-public crate-openai-magic-instantiate-derive-0.1.1 (c (n "openai-magic-instantiate-derive") (v "0.1.1") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "112nlq833z09fbz434si57sxlphjgj9sfcy6jvvj3i3mqhzznfp2")))

(define-public crate-openai-magic-instantiate-derive-0.1.2 (c (n "openai-magic-instantiate-derive") (v "0.1.2") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0smxm3nldnc3fp0yyhqv4l55iipi2aqrkajfdjgylv69rpm223nl")))

(define-public crate-openai-magic-instantiate-derive-0.1.3 (c (n "openai-magic-instantiate-derive") (v "0.1.3") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1cxzb9hnw57i01rr6xsq8v9bxpn5j2gnh32qbzy2s1wzd2yxixxa")))

(define-public crate-openai-magic-instantiate-derive-0.1.4 (c (n "openai-magic-instantiate-derive") (v "0.1.4") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "099279y42kxblp626pxvmwahvriwxh5akzddhccc8jqcjnpdl9d4")))

(define-public crate-openai-magic-instantiate-derive-0.1.5 (c (n "openai-magic-instantiate-derive") (v "0.1.5") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0qmjjn9v7xghk71x3v4apnlvxk4c6gzypr8pjkg607y07yy963sq")))

(define-public crate-openai-magic-instantiate-derive-0.1.6 (c (n "openai-magic-instantiate-derive") (v "0.1.6") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1w1rwf9qdi68771277532gql7r8sw88piy93npm0l1mr0r5qbwj9")))

(define-public crate-openai-magic-instantiate-derive-0.2.0 (c (n "openai-magic-instantiate-derive") (v "0.2.0") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1m44j9bkm87h7cbnhnbr3353lq4ldw31cdm0dildib88fz7islwi")))

