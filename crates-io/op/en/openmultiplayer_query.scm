(define-module (crates-io op en openmultiplayer_query) #:use-module (crates-io))

(define-public crate-openmultiplayer_query-0.1.0 (c (n "openmultiplayer_query") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.5.0") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8.33") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "02z1hgc5k8yxs09iqhz3galh35jz2kg26b2bp20lmy26n9v3dl32")))

(define-public crate-openmultiplayer_query-0.1.1 (c (n "openmultiplayer_query") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.5.0") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8.33") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "1069mp4sb5831s45c85v12566fgm8gb74w3lriiw2kcnvhins7nl")))

(define-public crate-openmultiplayer_query-0.1.2 (c (n "openmultiplayer_query") (v "0.1.2") (d (list (d (n "byteorder") (r "^1.5.0") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8.33") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "1n2q3516nn9a1jc9dy05rh7jajs0dyr173sjhaqqqrp2nyvr8iph")))

