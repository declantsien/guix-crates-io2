(define-module (crates-io op en opentalk-diesel-newtype-impl) #:use-module (crates-io))

(define-public crate-opentalk-diesel-newtype-impl-0.0.0 (c (n "opentalk-diesel-newtype-impl") (v "0.0.0") (h "0nq10naafy7y67kmznn3b6kq67ifw5w7jrrlq9xidrirs7xzaa15")))

(define-public crate-opentalk-diesel-newtype-impl-0.8.0-rc.1 (c (n "opentalk-diesel-newtype-impl") (v "0.8.0-rc.1") (d (list (d (n "proc-macro-crate") (r "^2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1zz852kbqa8jjnml24lqdaccdxq31s7km1q9pv9h55ifhsryk1k1")))

(define-public crate-opentalk-diesel-newtype-impl-0.9.0-alpha (c (n "opentalk-diesel-newtype-impl") (v "0.9.0-alpha") (d (list (d (n "proc-macro-crate") (r "^2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "15gswayd2kbf6ggws3lk8gkxcswhzlcycwjbi8qb1ls23kablxxv")))

(define-public crate-opentalk-diesel-newtype-impl-0.9.0 (c (n "opentalk-diesel-newtype-impl") (v "0.9.0") (d (list (d (n "proc-macro-crate") (r "^3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0hr3l33i9x84a934161dz2za00qci92akdb0wzg1rh9viibmwkcx")))

(define-public crate-opentalk-diesel-newtype-impl-0.10.0-rc.1 (c (n "opentalk-diesel-newtype-impl") (v "0.10.0-rc.1") (d (list (d (n "proc-macro-crate") (r "^3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1i8zlbf8mbwzdnqjgrgr0adr3df4kyli6r6kp2dhq8f694282i6n")))

(define-public crate-opentalk-diesel-newtype-impl-0.11.0-alpha (c (n "opentalk-diesel-newtype-impl") (v "0.11.0-alpha") (d (list (d (n "proc-macro-crate") (r "^3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0wlq264ma17h9z9qqx1kz72rrd4awpyxp6knmybzgdqa9r28x8pl")))

(define-public crate-opentalk-diesel-newtype-impl-0.12.0-alpha (c (n "opentalk-diesel-newtype-impl") (v "0.12.0-alpha") (d (list (d (n "proc-macro-crate") (r "^3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0fzi1750vndfg5hhhxzg986zqy4486nk7ggf77zq33jb9rvza9j7")))

(define-public crate-opentalk-diesel-newtype-impl-0.12.0-alpha.1 (c (n "opentalk-diesel-newtype-impl") (v "0.12.0-alpha.1") (d (list (d (n "proc-macro-crate") (r "^3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0xx92qfiik2k2fd4qxfbx0mfh186d128nhwbzx7m5j5x3dzyl9xd")))

(define-public crate-opentalk-diesel-newtype-impl-0.11.0-rc.2 (c (n "opentalk-diesel-newtype-impl") (v "0.11.0-rc.2") (d (list (d (n "proc-macro-crate") (r "^3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1slvibqvh9hph1isd4b2a0ml7sypa2bd8h7q0p9jfbwyl474ksc5")))

(define-public crate-opentalk-diesel-newtype-impl-0.10.0 (c (n "opentalk-diesel-newtype-impl") (v "0.10.0") (d (list (d (n "proc-macro-crate") (r "^3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0s62y46wb6rr4kwdbwgg07fs8yisrd2vhmg985ri0lqcz5hai6gf")))

(define-public crate-opentalk-diesel-newtype-impl-0.12.0 (c (n "opentalk-diesel-newtype-impl") (v "0.12.0") (d (list (d (n "proc-macro-crate") (r "^3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "03bhbw15jrn6iysglwiy3hk4i9py63jvz3wgw36y2sibal546m7h")))

(define-public crate-opentalk-diesel-newtype-impl-0.11.0 (c (n "opentalk-diesel-newtype-impl") (v "0.11.0") (d (list (d (n "proc-macro-crate") (r "^3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0bircsj6k929pcjp8vp8cj79kgycns53ain877qxxr90pjyvdcbn")))

