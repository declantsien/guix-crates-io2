(define-module (crates-io op en opensrdk-optimization) #:use-module (crates-io))

(define-public crate-opensrdk-optimization-0.1.0 (c (n "opensrdk-optimization") (v "0.1.0") (d (list (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "rayon") (r "^1.3.1") (d #t) (k 0)))) (h "03rghz9k86172zxxjjjgglgsh78mywvj0dvhvdl3ksj071fmwz9l")))

(define-public crate-opensrdk-optimization-0.1.1 (c (n "opensrdk-optimization") (v "0.1.1") (d (list (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "rayon") (r "^1.3.1") (d #t) (k 0)))) (h "0z5d93xi558391mzi7qvmkxfxih3k7wa5i0yjwjpk5aazcv99mgi")))

(define-public crate-opensrdk-optimization-0.1.2 (c (n "opensrdk-optimization") (v "0.1.2") (d (list (d (n "blas-src") (r "^0.6") (f (quote ("intel-mkl"))) (d #t) (k 2)) (d (n "lapack-src") (r "^0.6") (f (quote ("intel-mkl"))) (d #t) (k 2)) (d (n "opensrdk-linear-algebra") (r "^0.5.0") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "rayon") (r "^1.3.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "1jj6lij4m8hbdb3a6ldr1fwhy3dps4ghm4jwqdsbslbn1lkqm7jb")))

(define-public crate-opensrdk-optimization-0.1.3 (c (n "opensrdk-optimization") (v "0.1.3") (d (list (d (n "blas-src") (r "^0.7") (f (quote ("intel-mkl"))) (d #t) (k 2)) (d (n "lapack-src") (r "^0.6") (f (quote ("intel-mkl"))) (d #t) (k 2)) (d (n "opensrdk-linear-algebra") (r "^0.6.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "rayon") (r "^1.5.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "1icnyyb6wm5qacz1q5bfr1bh0a8kbvm90f05j4q0ndizwd8xifks")))

(define-public crate-opensrdk-optimization-0.1.4 (c (n "opensrdk-optimization") (v "0.1.4") (d (list (d (n "blas-src") (r "^0.8") (f (quote ("intel-mkl"))) (d #t) (k 2)) (d (n "lapack-src") (r "^0.8") (f (quote ("intel-mkl"))) (d #t) (k 2)) (d (n "opensrdk-linear-algebra") (r "^0.7.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.28") (d #t) (k 0)))) (h "061f904rxyappfvybs97d9wivf92ccm5vfylsdsag33d3mn33vb5")))

