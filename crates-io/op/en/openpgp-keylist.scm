(define-module (crates-io op en openpgp-keylist) #:use-module (crates-io))

(define-public crate-openpgp-keylist-0.1.0 (c (n "openpgp-keylist") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0gyq382nxvs0730m4agi6xfgz4k8n3z79ndixxgl96mbjgywanv3")))

(define-public crate-openpgp-keylist-0.2.0 (c (n "openpgp-keylist") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0z4060znvnk9vyjn6d50pjxz05k6s3fgz8irwnyynx8gmmhc0as3")))

