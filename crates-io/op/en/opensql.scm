(define-module (crates-io op en opensql) #:use-module (crates-io))

(define-public crate-opensql-0.1.0 (c (n "opensql") (v "0.1.0") (h "05m24h9gvvhc5yn8ni2i5na1dy1d7fb3cgzbdblprv9pbif8hrg4")))

(define-public crate-opensql-0.1.1 (c (n "opensql") (v "0.1.1") (h "0nrrgkpb67zkwg4hdzrdzx8x8hgc9nprq8jfmaq111na5ijz7n87")))

(define-public crate-opensql-0.1.2 (c (n "opensql") (v "0.1.2") (h "0zqjm4m5vgxjz1n9xm7lid9p681jl9azp3n8m3xlqqz1nqifp0ml")))

(define-public crate-opensql-0.1.3 (c (n "opensql") (v "0.1.3") (h "04bnx0k6abjlcqla730lapjgpmbd8nvyhg0bhg9rnxs9na39wmgw")))

(define-public crate-opensql-0.1.4 (c (n "opensql") (v "0.1.4") (h "1g3662z77xsh8ydg7fhy4cl150b65xyg1ljmjzpbm4f2m1kn2583")))

(define-public crate-opensql-0.1.5 (c (n "opensql") (v "0.1.5") (h "1pm8v4312rjgg4dzwpw6c39lbqn22r90kpkb1vr23v5rl3kddpcx")))

