(define-module (crates-io op en opencc-rs) #:use-module (crates-io))

(define-public crate-opencc-rs-0.1.0 (c (n "opencc-rs") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.138") (d #t) (k 0)) (d (n "opencc-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "138d06jf1lanx4jdfh231ij82dzkcmrwaq2h835kq0a8ql508dwa")))

(define-public crate-opencc-rs-0.1.2 (c (n "opencc-rs") (v "0.1.2") (d (list (d (n "libc") (r "^0.2.138") (d #t) (k 0)) (d (n "opencc-sys") (r "^0.1.2") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "02yxn2bxz9x2hpah8bl2msch8mpz5lwagk55ymjl3xxln8wqv95i")))

(define-public crate-opencc-rs-0.1.3 (c (n "opencc-rs") (v "0.1.3") (d (list (d (n "libc") (r "^0.2.138") (d #t) (k 0)) (d (n "opencc-sys") (r "^0.1.3") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "191fiw1clyvrxf87v7whw5q59i4yzhd0p3n6dsy38449zcs18a69")))

(define-public crate-opencc-rs-0.1.4 (c (n "opencc-rs") (v "0.1.4") (d (list (d (n "libc") (r "^0.2.138") (d #t) (k 0)) (d (n "opencc-sys") (r "^0.1.4") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "0w73pvzax7jf6xw5r4dq1bg5yg3vyh8fiidj2qy4jrngjwqikwyq")))

(define-public crate-opencc-rs-0.2.0 (c (n "opencc-rs") (v "0.2.0") (d (list (d (n "libc") (r "^0.2.138") (d #t) (k 0)) (d (n "opencc-sys") (r "^0.1.5") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "063a0xg629iyxhmfwcrhw8cn17d608nnipj72fr2r25vrrc760wl")))

(define-public crate-opencc-rs-0.3.0 (c (n "opencc-rs") (v "0.3.0") (d (list (d (n "libc") (r "^0.2.139") (f (quote ("std"))) (k 0)) (d (n "opencc-sys") (r "^0.1.7") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (f (quote ("std"))) (k 2)) (d (n "tempfile") (r "^3.4.0") (k 0)) (d (n "thiserror") (r "^1.0.38") (k 0)))) (h "1bm5lmvdg21dap897wjq9rjps20vvk0jh3b38d6b7m0jkx2lil6q")))

(define-public crate-opencc-rs-0.3.1 (c (n "opencc-rs") (v "0.3.1") (d (list (d (n "libc") (r "^0.2.147") (f (quote ("std"))) (k 0)) (d (n "opencc-sys") (r "^0.1.8") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (f (quote ("std"))) (k 2)) (d (n "tempfile") (r "^3.6.0") (k 0)) (d (n "thiserror") (r "^1.0.43") (k 0)))) (h "0c6knhmpf47ggvpw66as6dygcg8pj9k0jrh913b4hraamb7w8yd7") (r "1.65")))

(define-public crate-opencc-rs-0.3.2 (c (n "opencc-rs") (v "0.3.2") (d (list (d (n "libc") (r "^0.2.147") (f (quote ("std"))) (k 0)) (d (n "opencc-sys") (r "^0.1.9") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (f (quote ("std"))) (k 2)) (d (n "tempfile") (r "^3.7.0") (k 0)) (d (n "thiserror") (r "^1.0.44") (k 0)))) (h "0vn6wqy3cyj9w22av24amx1kj473lbx75zsdyjfm8ikp7m3p9yix") (r "1.65")))

(define-public crate-opencc-rs-0.3.3 (c (n "opencc-rs") (v "0.3.3") (d (list (d (n "libc") (r "^0.2.153") (f (quote ("std"))) (k 0)) (d (n "opencc-sys") (r "^0.2.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (f (quote ("std"))) (k 2)) (d (n "tempfile") (r "^3.10.1") (k 0)) (d (n "thiserror") (r "^1.0.58") (k 0)))) (h "0zy2anc10f33hg7khzg5pc9w1cxpcjxbf90d5dfiag9x0875mjbp") (r "1.65")))

