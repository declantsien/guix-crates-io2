(define-module (crates-io op en openssl-errors) #:use-module (crates-io))

(define-public crate-openssl-errors-0.1.0 (c (n "openssl-errors") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "openssl") (r "^0.10.19") (d #t) (k 2)) (d (n "openssl-sys") (r "^0.9.42") (d #t) (k 0)))) (h "0v980gss35mv55j6ckg69k4f432wz2qbhkfaw51a0nq61nsn6sl9")))

(define-public crate-openssl-errors-0.2.0 (c (n "openssl-errors") (v "0.2.0") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "openssl") (r "^0.10.19") (d #t) (k 2)) (d (n "openssl-sys") (r "^0.9.64") (d #t) (k 0)))) (h "1r8affvkjhqkrazvrx05i9mckibm3raxifsgwaq0xrcnrgng5qvr")))

