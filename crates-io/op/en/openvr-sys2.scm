(define-module (crates-io op en openvr-sys2) #:use-module (crates-io))

(define-public crate-openvr-sys2-0.1.0 (c (n "openvr-sys2") (v "0.1.0") (d (list (d (n "autocxx") (r "^0.25.0") (d #t) (k 0)) (d (n "autocxx-build") (r "^0.25.0") (d #t) (k 1)) (d (n "cxx") (r "^1.0.94") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.0") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "0s8gmaayqnb72lqv58h14ri8j9wl4a5rzf91mm2ihwf0n4cv6xzz")))

(define-public crate-openvr-sys2-0.1.1 (c (n "openvr-sys2") (v "0.1.1") (d (list (d (n "autocxx") (r "^0.26.0") (d #t) (k 0)) (d (n "autocxx-build") (r "^0.26.0") (d #t) (k 1)) (d (n "cxx") (r "^1.0.94") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.0") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "0j7w1fk5kg49m250f6i300l0mvwl47201f1318vn05bwqbymr6af")))

(define-public crate-openvr-sys2-0.1.2 (c (n "openvr-sys2") (v "0.1.2") (d (list (d (n "autocxx") (r "^0.26.0") (d #t) (k 0)) (d (n "autocxx-build") (r "^0.26.0") (d #t) (k 1)) (d (n "cxx") (r "^1.0.94") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.0") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "0qd1rs6izy1d5bvslc9cwg4dgv7zrd6mm0dy9shjc4mk369gs8al")))

(define-public crate-openvr-sys2-0.1.3 (c (n "openvr-sys2") (v "0.1.3") (d (list (d (n "autocxx") (r "^0.26.0") (d #t) (k 0)) (d (n "autocxx-build") (r "^0.26.0") (d #t) (k 1)) (d (n "cxx") (r "^1.0.103") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.0") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "1f9i0523k0b3dc6glp3cwnsf2mvkv4sksgh7bsj4pxc8klcpfj6x")))

