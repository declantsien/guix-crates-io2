(define-module (crates-io op en openai_http_client) #:use-module (crates-io))

(define-public crate-openai_http_client-0.3.0 (c (n "openai_http_client") (v "0.3.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "rustls-tls"))) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "01yh21vmhhzzj8fnkwh1d5qnag81i9xqkmcbg1ik8va4c1741cjb")))

(define-public crate-openai_http_client-0.4.0 (c (n "openai_http_client") (v "0.4.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "rustls-tls"))) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "03sakrfinraam7xd3mb6y696891f9093iwmaqdr88y0ph5n90y63")))

