(define-module (crates-io op en open_ai) #:use-module (crates-io))

(define-public crate-open_ai-0.1.0 (c (n "open_ai") (v "0.1.0") (d (list (d (n "cpython") (r "^0.1.0") (d #t) (k 0)))) (h "15xbn588k6qhi9f761pp000fy0yj4ajw8wx13lfb5rbn14ypqnak")))

(define-public crate-open_ai-0.1.1 (c (n "open_ai") (v "0.1.1") (d (list (d (n "cpython") (r "^0.1.0") (d #t) (k 0)))) (h "08i2ac8vv8jjifgi131zmlichxyscyk5l2pr2rgi68q5kyvd19d9")))

