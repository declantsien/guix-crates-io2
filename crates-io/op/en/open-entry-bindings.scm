(define-module (crates-io op en open-entry-bindings) #:use-module (crates-io))

(define-public crate-open-entry-bindings-1.0.0 (c (n "open-entry-bindings") (v "1.0.0") (d (list (d (n "async-ffi") (r "^0.4.0") (d #t) (k 0)) (d (n "fast-async-mutex") (r "^0.6.7") (d #t) (k 0)) (d (n "libloading") (r "^0.7.3") (d #t) (k 0)) (d (n "paste") (r "^1.0.7") (d #t) (k 0)) (d (n "tokio") (r "^1.18.2") (f (quote ("rt" "rt-multi-thread" "time" "macros" "sync"))) (d #t) (k 0)))) (h "1zib30g0l14vh24sivi955d55676prinq2anwlbllzx00vqrb5b3")))

(define-public crate-open-entry-bindings-1.0.1 (c (n "open-entry-bindings") (v "1.0.1") (d (list (d (n "async-ffi") (r "^0.4.0") (d #t) (k 0)) (d (n "fast-async-mutex") (r "^0.6.7") (d #t) (k 0)) (d (n "libloading") (r "^0.7.3") (d #t) (k 0)) (d (n "paste") (r "^1.0.7") (d #t) (k 0)) (d (n "tokio") (r "^1.18.2") (f (quote ("rt" "rt-multi-thread" "time" "macros" "sync"))) (d #t) (k 0)))) (h "0ni5pna7vc83dj1ximv48v249zcvd14y5j3y70gna4nm3bcwb0wg")))

(define-public crate-open-entry-bindings-1.0.2 (c (n "open-entry-bindings") (v "1.0.2") (d (list (d (n "async-ffi") (r "^0.4.0") (d #t) (k 0)) (d (n "fast-async-mutex") (r "^0.6.7") (d #t) (k 0)) (d (n "libloading") (r "^0.7.3") (d #t) (k 0)) (d (n "paste") (r "^1.0.7") (d #t) (k 0)) (d (n "tokio") (r "^1.18.2") (f (quote ("rt" "rt-multi-thread" "time" "macros" "sync"))) (d #t) (k 0)))) (h "0anrnrc0wji1xhyf61smfrwbzmzk30zqfp0gs5a22yq32n1wsj2j")))

