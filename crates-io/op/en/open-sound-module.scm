(define-module (crates-io op en open-sound-module) #:use-module (crates-io))

(define-public crate-open-sound-module-0.1.0 (c (n "open-sound-module") (v "0.1.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "sample") (r "^0.10.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2.18") (d #t) (k 0)))) (h "1dn2igki10ld1w7ba2yc3f91y4gvgwgcy4rk5a47gjlwcanjl02h")))

