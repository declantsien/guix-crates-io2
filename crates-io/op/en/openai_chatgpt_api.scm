(define-module (crates-io op en openai_chatgpt_api) #:use-module (crates-io))

(define-public crate-openai_chatgpt_api-0.1.0 (c (n "openai_chatgpt_api") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11.10") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.19.2") (f (quote ("full"))) (d #t) (k 2)))) (h "1gw9w52riwg8h8s1d9s4h8qgklygbf65jmvzrkmmwihyji5my5mb")))

(define-public crate-openai_chatgpt_api-0.1.1 (c (n "openai_chatgpt_api") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.11.10") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.19.2") (f (quote ("full"))) (d #t) (k 2)))) (h "0h9rkdc9m58fk459lf641i8hnpbw4w5lis5s2szzksnwanrpgnpc")))

(define-public crate-openai_chatgpt_api-0.1.2 (c (n "openai_chatgpt_api") (v "0.1.2") (d (list (d (n "reqwest") (r "^0.11.10") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.19.2") (f (quote ("full"))) (d #t) (k 2)))) (h "0pdx0zary01vrfkm454vql7pspjn03pynbw5qirgqj1v0rn9n934")))

