(define-module (crates-io op en opengraph-rs) #:use-module (crates-io))

(define-public crate-opengraph-rs-0.2.6 (c (n "opengraph-rs") (v "0.2.6") (d (list (d (n "html5ever") (r "^0.22") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "08a7szag7j9519m8zvm3wcxswsq495nm12zz1w6fz6b8gqdr00bm") (f (quote (("default" "reqwest"))))))

