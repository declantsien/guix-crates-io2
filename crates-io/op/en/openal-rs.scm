(define-module (crates-io op en openal-rs) #:use-module (crates-io))

(define-public crate-openal-rs-0.1.0 (c (n "openal-rs") (v "0.1.0") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "0agqcalnfrvzvi4ch08pcppyvism9klzb5cdykj58f1dzzdf5ayb") (y #t)))

(define-public crate-openal-rs-0.1.1 (c (n "openal-rs") (v "0.1.1") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "0nbi6q0bg43v55n3jl5a301ddkf3qqjqg91mvz1isyb9d9an62cn") (y #t)))

(define-public crate-openal-rs-0.1.2 (c (n "openal-rs") (v "0.1.2") (d (list (d (n "libc") (r "^0.2.6") (d #t) (k 0)))) (h "087chcr6xgy298qb515bfpxvggls1rkqia1vn8h84fkrkjgka1n2") (y #t)))

(define-public crate-openal-rs-0.1.3 (c (n "openal-rs") (v "0.1.3") (d (list (d (n "libc") (r "^0.2.6") (d #t) (k 0)))) (h "1vrk2qrj94a0w8j5aafpqff98xljyvd3krfqbfh6l07mva81jv3a") (y #t)))

(define-public crate-openal-rs-0.1.4 (c (n "openal-rs") (v "0.1.4") (d (list (d (n "libc") (r "^0.2.6") (d #t) (k 0)))) (h "11ayc7d0z454mg8y1ag9d8zccgr91g36c2nzqarwan17bgir8bk6") (y #t)))

(define-public crate-openal-rs-0.1.5 (c (n "openal-rs") (v "0.1.5") (d (list (d (n "libc") (r "^0.2.6") (d #t) (k 0)))) (h "01h4yvfjq90kqi4fh1s8f6byc2gvscv3q63k2mib1jrrby9lyx8j") (y #t)))

(define-public crate-openal-rs-0.1.6 (c (n "openal-rs") (v "0.1.6") (d (list (d (n "libc") (r "^0.2.6") (d #t) (k 0)))) (h "0vb33xpkpprj83zamvcd6si8wk2i9b9h7i0xpik00vypgma7p9rf") (y #t)))

(define-public crate-openal-rs-0.2.0 (c (n "openal-rs") (v "0.2.0") (d (list (d (n "libc") (r "^0.2.6") (d #t) (k 0)))) (h "18pqhk1377hnkzmd15s1s42g93b0wcprq2g0n7k8cv1x3ijzkmys") (y #t)))

(define-public crate-openal-rs-0.3.0 (c (n "openal-rs") (v "0.3.0") (d (list (d (n "libc") (r "^0.2.17") (d #t) (k 0)))) (h "0y7p4b0k1sf6famn802kkh7dnm2jnjjfp4hqrvhaxy7pynxlmmd4") (y #t)))

(define-public crate-openal-rs-0.3.1 (c (n "openal-rs") (v "0.3.1") (d (list (d (n "libc") (r "^0.2.17") (d #t) (k 0)))) (h "00iffv2j91jqgrqbzswyncibxdv1cds599rid15hqs65dsxcaxp5") (y #t)))

(define-public crate-openal-rs-0.3.2 (c (n "openal-rs") (v "0.3.2") (d (list (d (n "libc") (r "^0.2.17") (d #t) (k 0)))) (h "1myi0384l37wdql7515a6148b1z07gm56pcs27q5ng0m6qhiphbv") (y #t)))

(define-public crate-openal-rs-0.3.3 (c (n "openal-rs") (v "0.3.3") (d (list (d (n "libc") (r "^0.2.17") (d #t) (k 0)))) (h "11rr05zr3cb9l48ymhaxynvd8njf4ypkzisjp3yz508qakpals8f") (y #t)))

