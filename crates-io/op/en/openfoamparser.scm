(define-module (crates-io op en openfoamparser) #:use-module (crates-io))

(define-public crate-openfoamparser-0.1.0 (c (n "openfoamparser") (v "0.1.0") (d (list (d (n "approx") (r "^0.4.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.24.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)))) (h "18mdlm8idw93nv0cbq87wn6ykx9riyhd32zh9za3ini2ih7kq360")))

(define-public crate-openfoamparser-0.1.1 (c (n "openfoamparser") (v "0.1.1") (d (list (d (n "approx") (r "^0.4.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.24.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)))) (h "0sv1p3kfp90j58jl1k20ahcb093il1vk6ywrpin11grsxwjz24c6")))

(define-public crate-openfoamparser-0.1.2 (c (n "openfoamparser") (v "0.1.2") (d (list (d (n "approx") (r "^0.4.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.24.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)))) (h "1y28qaww89zyyzh7cavkl93vjjpi4wxz75nbv3d6w2p4zngxza1d")))

