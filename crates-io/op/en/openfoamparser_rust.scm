(define-module (crates-io op en openfoamparser_rust) #:use-module (crates-io))

(define-public crate-openfoamparser_rust-0.1.0 (c (n "openfoamparser_rust") (v "0.1.0") (d (list (d (n "approx") (r "^0.4.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.24.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)))) (h "16kwa88573jnkwm688wxcq437nwviql77g59rzr16m26iy13k30q")))

