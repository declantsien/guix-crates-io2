(define-module (crates-io op en opensky_api) #:use-module (crates-io))

(define-public crate-opensky_api-0.1.0 (c (n "opensky_api") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.68") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.12") (f (quote ("full"))) (d #t) (k 2)))) (h "09plpk183285cfv8zlqh4ic5h4316whki5jx3l9zd5nnk8a65r20")))

(define-public crate-opensky_api-0.1.2 (c (n "opensky_api") (v "0.1.2") (d (list (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.68") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.12") (f (quote ("full"))) (d #t) (k 2)))) (h "1b1qnx36ql0gw8lxzm7z2h9q4m22451w1sk4355gh0pgwbzvpyfk")))

(define-public crate-opensky_api-0.1.3 (c (n "opensky_api") (v "0.1.3") (d (list (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.68") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.12") (f (quote ("full"))) (d #t) (k 2)))) (h "1fivnvjqkgh2cvgf4pc02pfwifn0niqynigvwgynrg7p3z4w01b3")))

(define-public crate-opensky_api-0.1.4 (c (n "opensky_api") (v "0.1.4") (d (list (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.68") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.12") (f (quote ("full"))) (d #t) (k 2)))) (h "1cq1d9py2q0f03dn0962zwp7ylw02ffn8g8cvcl4djl3rfvsw4j7")))

