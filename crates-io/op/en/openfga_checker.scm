(define-module (crates-io op en openfga_checker) #:use-module (crates-io))

(define-public crate-openfga_checker-0.1.0 (c (n "openfga_checker") (v "0.1.0") (d (list (d (n "openfga_common") (r "^0.1.0") (d #t) (k 0)))) (h "0fjhnp6p39m8nyvw3x8za2wrmvcdwszbl1kwfdnrd9344zcr5i1w")))

(define-public crate-openfga_checker-0.2.0 (c (n "openfga_checker") (v "0.2.0") (d (list (d (n "openfga_common") (r "^0.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "00wkwxvhfw354jwj50v5rks29p70f7hcxsw1da3c9i2pjbmc6fr0")))

