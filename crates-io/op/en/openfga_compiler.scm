(define-module (crates-io op en openfga_compiler) #:use-module (crates-io))

(define-public crate-openfga_compiler-0.2.0 (c (n "openfga_compiler") (v "0.2.0") (d (list (d (n "ariadne") (r "^0.1.5") (d #t) (k 0)) (d (n "chumsky") (r "^0.8.0") (d #t) (k 0)) (d (n "clap") (r "^4.0.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "openfga_checker") (r "^0.2.0") (d #t) (k 0)) (d (n "openfga_common") (r "^0.2.0") (d #t) (k 0)) (d (n "openfga_model_dsl_parser") (r "^0.1.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.86") (d #t) (k 0)))) (h "0wvfzp16rmcb8gj2fwmsdwzsgfr45h3dnvkw2l05836f64wpd6hs")))

