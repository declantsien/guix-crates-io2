(define-module (crates-io op en openshaiya-extractor) #:use-module (crates-io))

(define-public crate-openshaiya-extractor-0.0.1 (c (n "openshaiya-extractor") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "libshaiya") (r "^0.0.4") (d #t) (k 0)))) (h "1ang5634lxdvkw63fbh6vs8ishqh0k3r22iil43x4lcxxgz1n1sf")))

(define-public crate-openshaiya-extractor-0.0.2 (c (n "openshaiya-extractor") (v "0.0.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "libshaiya") (r "^0.0.4") (d #t) (k 0)))) (h "0lpyh5w764639c7i95i412zl2ylvxydy94lnq9l82w997s7808bl")))

