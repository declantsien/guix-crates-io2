(define-module (crates-io op en opendht) #:use-module (crates-io))

(define-public crate-opendht-0.1.0 (c (n "opendht") (v "0.1.0") (d (list (d (n "cc") (r "^1.0.26") (d #t) (k 1)) (d (n "futures") (r "^0.1.25") (d #t) (k 0)) (d (n "libc") (r "^0.2.45") (d #t) (k 0)) (d (n "nix") (r "^0.12.0") (d #t) (k 0)) (d (n "ring") (r "^0.13.5") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (f (quote ("async-await-preview"))) (d #t) (k 2)))) (h "1m2k5p7zjxhspjqfib89mrkb0l3cfs7scx1291g6pjpja4i3nxdh")))

(define-public crate-opendht-0.2.0 (c (n "opendht") (v "0.2.0") (d (list (d (n "cc") (r "^1.0.26") (d #t) (k 1)) (d (n "futures-preview") (r "^0.3.0-alpha.15") (f (quote ("compat"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.45") (d #t) (k 0)) (d (n "nix") (r "^0.12.0") (d #t) (k 0)) (d (n "ring") (r "^0.13.5") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 2)))) (h "03qq178xfzk9ymfz95xvklwcs4jl8ba1502ziwg9wxbpshq0yjff")))

(define-public crate-opendht-0.3.0 (c (n "opendht") (v "0.3.0") (d (list (d (n "cc") (r "^1.0.26") (d #t) (k 1)) (d (n "futures-preview") (r "^0.3.0-alpha.15") (f (quote ("compat"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.45") (d #t) (k 0)) (d (n "nix") (r "^0.12.0") (d #t) (k 0)) (d (n "ring") (r "^0.13.5") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 2)))) (h "09q7nak56giwph7f7xkh3wl8d1ls7nxp7ildri4r7qnldhdp94j8")))

(define-public crate-opendht-0.3.1 (c (n "opendht") (v "0.3.1") (d (list (d (n "cc") (r "^1.0.26") (d #t) (k 1)) (d (n "futures-preview") (r "^0.3.0-alpha.15") (f (quote ("compat"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.45") (d #t) (k 0)) (d (n "nix") (r "^0.12.0") (d #t) (k 0)) (d (n "ring") (r "^0.13.5") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 2)))) (h "18cl72s8z853skx10s9yamcdg293vy01zpm9czsscwzq8qvbnqqb")))

(define-public crate-opendht-0.4.0 (c (n "opendht") (v "0.4.0") (d (list (d (n "async-std") (r "^1.5.0") (f (quote ("unstable"))) (d #t) (k 2)) (d (n "cc") (r "^1.0.26") (d #t) (k 1)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.45") (d #t) (k 0)) (d (n "nix") (r "^0.12.0") (d #t) (k 0)) (d (n "ring") (r "^0.13.5") (d #t) (k 0)))) (h "04c9gzcljk8sr3zrslj4v3vja8xyiipmjsp4jdb1xk2yi3njqk3r")))

(define-public crate-opendht-0.4.1 (c (n "opendht") (v "0.4.1") (d (list (d (n "async-std") (r "^1.5.0") (f (quote ("unstable"))) (d #t) (k 2)) (d (n "cc") (r "^1.0.26") (d #t) (k 1)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.45") (d #t) (k 0)) (d (n "nix") (r "^0.12.0") (d #t) (k 0)) (d (n "ring") (r "^0.13.5") (d #t) (k 0)))) (h "1hq0xmv8xvhpck4qhsmlhcc6ickq6h8hscs2yifq34n84yrhkd9j")))

(define-public crate-opendht-0.5.0 (c (n "opendht") (v "0.5.0") (d (list (d (n "async-std") (r "^1.5.0") (f (quote ("unstable"))) (d #t) (k 2)) (d (n "cc") (r "^1.0.26") (d #t) (k 1)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.45") (d #t) (k 0)) (d (n "nix") (r "^0.12.0") (d #t) (k 0)) (d (n "ring") (r "^0.13.5") (d #t) (k 0)))) (h "0ijdghrhyfh5vlfjzf3qlhcvwq3236yvx63gnn3fivwnkjz002k3")))

(define-public crate-opendht-0.5.1 (c (n "opendht") (v "0.5.1") (d (list (d (n "async-std") (r "^1.5.0") (f (quote ("unstable"))) (d #t) (k 2)) (d (n "cc") (r "^1.0.26") (d #t) (k 1)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.45") (d #t) (k 0)) (d (n "nix") (r "^0.12.0") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.19") (d #t) (k 1)) (d (n "ring") (r "^0.13.5") (d #t) (k 0)))) (h "1lxy4nckrz75ldym40116fwzqsgbvrvbgb1p7pgdl9g1798i10mk")))

(define-public crate-opendht-0.5.2 (c (n "opendht") (v "0.5.2") (d (list (d (n "async-std") (r "^1.5.0") (f (quote ("unstable"))) (d #t) (k 2)) (d (n "cc") (r "^1.0.26") (d #t) (k 1)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.45") (d #t) (k 0)) (d (n "nix") (r "^0.12.0") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.19") (d #t) (k 1)) (d (n "ring") (r "^0.13.5") (d #t) (k 0)))) (h "1snn6kyf8hi3cwasda38p39dyfhb53nngr4pfx7ds4da62ibv1ba")))

