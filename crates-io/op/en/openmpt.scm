(define-module (crates-io op en openmpt) #:use-module (crates-io))

(define-public crate-openmpt-0.2.0 (c (n "openmpt") (v "0.2.0") (d (list (d (n "hound") (r "^3.1.0") (d #t) (k 2)) (d (n "openmpt-sys") (r "^0.1.0") (d #t) (k 0)))) (h "1y1ikq2hqndp1qagff1sxks4nkgjrwhhaliav1xdrgr3bswi5hjm")))

(define-public crate-openmpt-0.3.0 (c (n "openmpt") (v "0.3.0") (d (list (d (n "hound") (r "^3.1.0") (d #t) (k 2)) (d (n "openmpt-sys") (r "^0.1.0") (d #t) (k 0)))) (h "00a0n9vg8lc48dncdybiqp32ckxcqkg5zk3vxff2wn9d1mm1czy6")))

(define-public crate-openmpt-0.3.1 (c (n "openmpt") (v "0.3.1") (d (list (d (n "hound") (r "^3.1.0") (d #t) (k 2)) (d (n "openmpt-sys") (r "^0.1.0") (d #t) (k 0)))) (h "0gqb1n04j3nzazb3f1lmnzgciaxyk85g690lxq27h7x2nciklsb7")))

