(define-module (crates-io op en opengauss-protocol) #:use-module (crates-io))

(define-public crate-opengauss-protocol-0.1.0 (c (n "opengauss-protocol") (v "0.1.0") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "bytes") (r "^1.0") (d #t) (k 0)) (d (n "fallible-iterator") (r "^0.2") (d #t) (k 0)) (d (n "hmac") (r "^0.11") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "md-5") (r "^0.9") (d #t) (k 0)) (d (n "memchr") (r "^2.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "ring") (r "^0.16.9") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)) (d (n "sha2") (r "^0.9") (d #t) (k 0)) (d (n "stringprep") (r "^0.1") (d #t) (k 0)))) (h "0ca4nfg3vfm5wns1kn1zl77cc334f70615lsbnw51vg8d0ziby0g")))

