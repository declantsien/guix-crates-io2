(define-module (crates-io op en opencorpora) #:use-module (crates-io))

(define-public crate-opencorpora-0.1.0 (c (n "opencorpora") (v "0.1.0") (d (list (d (n "bzip2") (r "^0.3") (d #t) (k 2)) (d (n "hyper") (r "^0.8") (d #t) (k 2)) (d (n "quick-error") (r "^1.0") (d #t) (k 0)) (d (n "quick-xml") (r "^0.1") (d #t) (k 0)))) (h "0i5l6mpkfzv11l6p98fr9cd2chhlcwxaamk758kaizwspybrxg7l")))

(define-public crate-opencorpora-0.1.1 (c (n "opencorpora") (v "0.1.1") (d (list (d (n "bzip2") (r "^0.3") (d #t) (k 2)) (d (n "hyper") (r "^0.8") (d #t) (k 2)) (d (n "quick-error") (r "^1.0") (d #t) (k 0)) (d (n "quick-xml") (r "^0.1") (d #t) (k 0)))) (h "1sm5cnmasygqc7dhqdgj0f05xg14clim8p9v55yrynl5z5zzn5sz")))

(define-public crate-opencorpora-0.1.2 (c (n "opencorpora") (v "0.1.2") (d (list (d (n "bzip2") (r "^0.3") (d #t) (k 2)) (d (n "hyper") (r "^0.8") (d #t) (k 2)) (d (n "quick-error") (r "^1.0") (d #t) (k 0)) (d (n "quick-xml") (r "^0.1") (d #t) (k 0)))) (h "0yygjahyrphnjk8jsj8lvd8kmmp2yp1qppk0mrqnmn5pzdyss6g7")))

(define-public crate-opencorpora-0.1.3 (c (n "opencorpora") (v "0.1.3") (d (list (d (n "bzip2") (r "^0.3.1") (d #t) (k 2)) (d (n "hyper") (r "^0.10.5") (d #t) (k 2)) (d (n "quick-error") (r "^1.1.0") (d #t) (k 0)) (d (n "quick-xml") (r "^0.6.2") (d #t) (k 0)))) (h "0givmcxhyblh5vbfwmw9mmi9gwqskwc5hy6hhjgczblspxa7d80p")))

(define-public crate-opencorpora-0.1.4 (c (n "opencorpora") (v "0.1.4") (d (list (d (n "bzip2") (r "^0.3.1") (d #t) (k 2)) (d (n "hyper") (r "^0.10.5") (d #t) (k 2)) (d (n "quick-error") (r "^1.1.0") (d #t) (k 0)) (d (n "quick-xml") (r "^0.6.2") (d #t) (k 0)))) (h "0mbgffn363j2vz01fz9ww48qvld92q1rgpkcv5an4jhssqr97s6m")))

(define-public crate-opencorpora-0.2.0 (c (n "opencorpora") (v "0.2.0") (d (list (d (n "bzip2") (r "^0.3.0") (d #t) (k 2)) (d (n "error-chain") (r "^0.11.0") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 2)) (d (n "hyper") (r "^0.11.0") (d #t) (k 2)) (d (n "quick-xml") (r "^0.10.0") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 2)))) (h "0kh7anxwbc3mv8n5fc5hyzkvnc3iwmg1ppi3va152kd31y42c86i")))

(define-public crate-opencorpora-0.3.0 (c (n "opencorpora") (v "0.3.0") (d (list (d (n "bzip2") (r "^0.4.3") (d #t) (k 2)) (d (n "hyper") (r "^0.14") (f (quote ("full"))) (d #t) (k 2)) (d (n "quick-xml") (r "^0.23") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 2)))) (h "0w5487yh5wc6z8l9wmsny0g6nja98k5w255n155hiy4s9w19y11n")))

