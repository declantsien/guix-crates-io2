(define-module (crates-io op en openweather-api) #:use-module (crates-io))

(define-public crate-openweather-api-0.1.0 (c (n "openweather-api") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "dotenvy") (r "^0.15") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.14") (f (quote ("macros" "rt" "rt-multi-thread"))) (d #t) (k 0)))) (h "0cnw3mlsfcgl3w770czi35w83pl70i0gjzsjpclscl3xml7q7x5a")))

