(define-module (crates-io op en openrr-sleep) #:use-module (crates-io))

(define-public crate-openrr-sleep-0.0.1 (c (n "openrr-sleep") (v "0.0.1") (h "0r5nqxg5x1x38xfcw31snpkllhqgyly07zsxanrqvhsy2dhyg40v")))

(define-public crate-openrr-sleep-0.0.3 (c (n "openrr-sleep") (v "0.0.3") (h "16w05hmz8jq0322f1xl01gjxs5acf79az9p9c20b0p9w6qn5gw85")))

(define-public crate-openrr-sleep-0.0.4 (c (n "openrr-sleep") (v "0.0.4") (h "1if4f3gxx77vv0c616lzml0avad14gkj2w65fvaryarsby3xr1lg")))

(define-public crate-openrr-sleep-0.0.5 (c (n "openrr-sleep") (v "0.0.5") (h "088hv2w3marrhilcla5msdj1bs7acihipclhxn4vskkxlq51sbry")))

(define-public crate-openrr-sleep-0.0.6 (c (n "openrr-sleep") (v "0.0.6") (h "1jxrw6l4l82l9kqs6jhi09l5v1mfnk1swbb9j22gxhnh8y3i7lgd")))

