(define-module (crates-io op en openssh-sftp-protocol-error) #:use-module (crates-io))

(define-public crate-openssh-sftp-protocol-error-0.1.0 (c (n "openssh-sftp-protocol-error") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "vec-strings") (r "^0.4.5") (f (quote ("serde"))) (d #t) (k 0)))) (h "1y3gjmm038qsivphqfdjf8xxdhfj9ss9rj3yw1m8cdzhnfg2c687")))

