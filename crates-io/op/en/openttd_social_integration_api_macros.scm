(define-module (crates-io op en openttd_social_integration_api_macros) #:use-module (crates-io))

(define-public crate-openttd_social_integration_api_macros-0.1.0 (c (n "openttd_social_integration_api_macros") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (f (quote ("full"))) (d #t) (k 0)))) (h "0k229hcdjd3qavxjbl312d2qimrhazmvj2i31cdkczg22giq3png")))

