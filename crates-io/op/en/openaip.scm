(define-module (crates-io op en openaip) #:use-module (crates-io))

(define-public crate-openaip-0.1.0 (c (n "openaip") (v "0.1.0") (d (list (d (n "approx") (r "^0.1.1") (d #t) (k 2)) (d (n "xmltree") (r "^0.4.0") (d #t) (k 0)))) (h "1jd4h9n802msr0lzr6pwjzgrlng596w0my4kanzirarn26hwkr10")))

(define-public crate-openaip-0.2.0 (c (n "openaip") (v "0.2.0") (d (list (d (n "approx") (r "^0.4.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "minidom") (r "^0.12.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0575cvc1x84wqgnwqizy09s45pgsvyxw21agrk765wwyrsbhlzky")))

(define-public crate-openaip-0.2.1 (c (n "openaip") (v "0.2.1") (d (list (d (n "approx") (r "^0.5.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "minidom") (r "^0.12.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1hkllwqfzwpswrv646k5r3jywknb6mn8j3d8483546xlq88inapg")))

