(define-module (crates-io op en openapiv3) #:use-module (crates-io))

(define-public crate-openapiv3-0.1.0 (c (n "openapiv3") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "1w3bvvbazglm61ryllmjn1q0kq15ljbpf7cyjjcf8rcji28jqi2n")))

(define-public crate-openapiv3-0.1.1 (c (n "openapiv3") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "17lhg832v6s9cly39wz0q6a809g9xa6gq4lm6m34hyhpmwmmdlms")))

(define-public crate-openapiv3-0.1.2 (c (n "openapiv3") (v "0.1.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "1m2yywmzsn12q8xs7ajshx9dq2cnv77hb5l6rfk1s3zp002mnql6")))

(define-public crate-openapiv3-0.1.3 (c (n "openapiv3") (v "0.1.3") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "1kdaqc7c45dg6f0vysspkb68kk8j43c7hq3izrzwszwnzfv10g87")))

(define-public crate-openapiv3-0.1.4 (c (n "openapiv3") (v "0.1.4") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "1chsakxp8655ydfby0rm2yjri813kx4hba6h5vzlsxakq15c9dqk")))

(define-public crate-openapiv3-0.2.0 (c (n "openapiv3") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "13ld4p5x4mwp5li7lqpkla9qzjqiz7ycllc9dch4pmsaf2hmdkr3")))

(define-public crate-openapiv3-0.3.0 (c (n "openapiv3") (v "0.3.0") (d (list (d (n "indexmap") (r "^1.0") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "0raxjc386r4p5ffz7jr69sib0glj9paxz2i7p8q3v82qd9ynqxy8")))

(define-public crate-openapiv3-0.3.2 (c (n "openapiv3") (v "0.3.2") (d (list (d (n "indexmap") (r "^1.0") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "0g4sk9i1sfw4k9kfijb2j80smlxin8qwsia1yx63r4nxxdza3s9y")))

(define-public crate-openapiv3-0.3.3 (c (n "openapiv3") (v "0.3.3") (d (list (d (n "indexmap") (r "^1.0") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "0cwnr482am8w9m73xpyh3xw6sws4ylfiwdmlsjp21mqqchjbra72") (y #t)))

(define-public crate-openapiv3-0.4.0 (c (n "openapiv3") (v "0.4.0") (d (list (d (n "indexmap") (r "^1.0") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "1vmmivacyhrwlidjxpzv8wpx9c7jgh9b7rihjwdrr8a9h8m64g0b")))

(define-public crate-openapiv3-0.5.0 (c (n "openapiv3") (v "0.5.0") (d (list (d (n "indexmap") (r "^1.0") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "11a0rcfc8b9c585582r78l872lkzwr0hv6yifhqzz7qdy8s8s8lh") (f (quote (("skip_serializing_defaults"))))))

(define-public crate-openapiv3-1.0.0-beta.0 (c (n "openapiv3") (v "1.0.0-beta.0") (d (list (d (n "indexmap") (r "^1.0") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "newline-converter") (r "^0.2.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "1lvrh4sxsm6lzi7a4lrn1k9yg5hxjp522w7hxi484zpy6fyn7hnc") (f (quote (("skip_serializing_defaults"))))))

(define-public crate-openapiv3-1.0.0-beta.1 (c (n "openapiv3") (v "1.0.0-beta.1") (d (list (d (n "indexmap") (r "^1.0") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "newline-converter") (r "^0.2.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "1jbylqbdry6b2yd4xmjkfizxcgwyq77f5nqax7b32jdwx3gy3rmb") (f (quote (("skip_serializing_defaults"))))))

(define-public crate-openapiv3-1.0.0-beta.2 (c (n "openapiv3") (v "1.0.0-beta.2") (d (list (d (n "indexmap") (r "^1.0") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "newline-converter") (r "^0.2.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "127nz3498x3p453ga75xr6rgbprsj85wz3cyn8gx39aw85k5y7vr") (f (quote (("skip_serializing_defaults"))))))

(define-public crate-openapiv3-1.0.0-beta.3 (c (n "openapiv3") (v "1.0.0-beta.3") (d (list (d (n "indexmap") (r "^1.0") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "newline-converter") (r "^0.2.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 2)))) (h "09g8hj1948pys59bsqp3k88v69jlypzxd37pvr132712i1f32q7k") (f (quote (("skip_serializing_defaults"))))))

(define-public crate-openapiv3-1.0.0-beta.4 (c (n "openapiv3") (v "1.0.0-beta.4") (d (list (d (n "indexmap") (r "^1.0") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "newline-converter") (r "^0.2.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 2)))) (h "1qs8yjpalbxgpvy4j02b4347g1i94wdnlgfw4k661yz8xdchs359") (f (quote (("skip_serializing_defaults"))))))

(define-public crate-openapiv3-1.0.0-beta.5 (c (n "openapiv3") (v "1.0.0-beta.5") (d (list (d (n "indexmap") (r "^1.0") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "newline-converter") (r "^0.2.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 2)))) (h "0ddmifvxh12iwsnkvz16h3wzim9xb8y3sj4klzpj32l793x1hmfl") (f (quote (("skip_serializing_defaults"))))))

(define-public crate-openapiv3-1.0.0 (c (n "openapiv3") (v "1.0.0") (d (list (d (n "indexmap") (r "^1.0") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "newline-converter") (r "^0.2.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 2)))) (h "1y3rd2awm718q63zgw1hbgkk7wi7z1a3s5ss5y1jxy7hsqqbiqcx") (f (quote (("skip_serializing_defaults"))))))

(define-public crate-openapiv3-1.0.1 (c (n "openapiv3") (v "1.0.1") (d (list (d (n "indexmap") (r "^1.0") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "newline-converter") (r "^0.2.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 2)))) (h "0k2lbdrvl3f0128g3ipv87m4jjqa1ynz3gagjfbg2wgx228nid5n") (f (quote (("skip_serializing_defaults"))))))

(define-public crate-openapiv3-1.0.2 (c (n "openapiv3") (v "1.0.2") (d (list (d (n "indexmap") (r "^1.0") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "newline-converter") (r "^0.2.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 2)))) (h "194s9sxd7mfl1apy5jqbcdhk8gl4w2lgrabvs6mq19xhdq89y6kv") (f (quote (("skip_serializing_defaults"))))))

(define-public crate-openapiv3-1.0.3 (c (n "openapiv3") (v "1.0.3") (d (list (d (n "indexmap") (r ">=1.6.1, <3.0.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "newline-converter") (r "^0.3.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 2)))) (h "04sgvkws25v8g2saix1lk13clcy97hif7dv5a51bcr8r8if6vrbm") (f (quote (("skip_serializing_defaults")))) (y #t)))

(define-public crate-openapiv3-2.0.0-rc.0 (c (n "openapiv3") (v "2.0.0-rc.0") (d (list (d (n "indexmap") (r "^2.0.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "newline-converter") (r "^0.3.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 2)))) (h "1cgahwrl1lqpgnrz2qznqsz0wc6z3w2bmc8npmji8r3zsr5si0n2") (f (quote (("skip_serializing_defaults"))))))

(define-public crate-openapiv3-2.0.0-rc.1 (c (n "openapiv3") (v "2.0.0-rc.1") (d (list (d (n "indexmap") (r "^2.0.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "newline-converter") (r "^0.3.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 2)))) (h "12nah9wfcv4pbvgrhk98wxfpfc1inqqrgnankhc5j58ry0368c95") (f (quote (("skip_serializing_defaults"))))))

(define-public crate-openapiv3-2.0.0 (c (n "openapiv3") (v "2.0.0") (d (list (d (n "indexmap") (r "^2.0.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "newline-converter") (r "^0.3.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 2)))) (h "0p4wgphi48f22x1y4zi0b80xg6h9dd7r3ra4h9q0gs7zagmdw0nc") (f (quote (("skip_serializing_defaults"))))))

(define-public crate-openapiv3-1.0.4 (c (n "openapiv3") (v "1.0.4") (d (list (d (n "indexmap") (r "^1.0") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "newline-converter") (r "^0.3.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 2)))) (h "0ixsa5w7an2a2x92dd2y227ax2pr4vwx5pcqx5ak7k2y60q3df1k") (f (quote (("skip_serializing_defaults"))))))

