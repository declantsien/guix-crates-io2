(define-module (crates-io op en openni2) #:use-module (crates-io))

(define-public crate-openni2-0.0.1 (c (n "openni2") (v "0.0.1") (d (list (d (n "openni2-sys") (r "^1.0") (d #t) (k 0)))) (h "0v5zaz0zv5apc4giazsnq6v0s4v8mqnzbd24jjc5rpisrzfmxxj7")))

(define-public crate-openni2-0.1.0 (c (n "openni2") (v "0.1.0") (d (list (d (n "minifb") (r "^0.10") (d #t) (k 2)) (d (n "openni2-sys") (r "^1.0") (d #t) (k 0)))) (h "15x05kl555njnzr986qmh4dsfgigjv9knrkq22f86kqr571clly5")))

(define-public crate-openni2-0.2.0 (c (n "openni2") (v "0.2.0") (d (list (d (n "minifb") (r "^0.10") (d #t) (k 2)) (d (n "openni2-sys") (r "^1.0") (d #t) (k 0)))) (h "108bjc2pbyd6k8ng7yx66zsindmd9mnrf8wrv9qwd59ldas2wsfi")))

(define-public crate-openni2-0.3.0 (c (n "openni2") (v "0.3.0") (d (list (d (n "minifb") (r "^0.10") (d #t) (k 2)) (d (n "openni2-sys") (r "^1.0") (d #t) (k 0)))) (h "1hdifss19pvm5wjymr0ssl3zs3yr6nbdwh2c3v9j304d8rc8mi5w")))

