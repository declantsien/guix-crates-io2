(define-module (crates-io op en opentracing-rs) #:use-module (crates-io))

(define-public crate-opentracing-rs-0.1.0 (c (n "opentracing-rs") (v "0.1.0") (h "14zzjqxsfv9ri8dg9k0wdl1wzy2wjj1w5jaip5zq74g2scna49i4") (y #t)))

(define-public crate-opentracing-rs-0.1.1 (c (n "opentracing-rs") (v "0.1.1") (d (list (d (n "futures") (r "^0.1.25") (d #t) (k 2)) (d (n "opentracing-rs-core") (r "^0.1.0") (d #t) (k 0)) (d (n "opentracing-rs-jaeger") (r "^0.1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.1.13") (d #t) (k 2)))) (h "1gcpz45qbqly9nyg2hys7f0qwx7mfyrikppz3bvcgkpf0ljw4fx1") (y #t)))

(define-public crate-opentracing-rs-0.0.0 (c (n "opentracing-rs") (v "0.0.0") (d (list (d (n "futures") (r "^0.1.25") (d #t) (k 2)) (d (n "opentracing-rs-core") (r "^0.1.0") (d #t) (k 0)) (d (n "opentracing-rs-jaeger") (r "^0.1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.1.13") (d #t) (k 2)))) (h "0cwvy3vvzni6s4394y6gp0f2wfwqfvm25d26r60n6flw36d4vcvq")))

(define-public crate-opentracing-rs-0.1.2 (c (n "opentracing-rs") (v "0.1.2") (d (list (d (n "futures") (r "^0.1") (d #t) (k 2)) (d (n "opentracing-rs-core") (r "^0.1.0") (d #t) (k 0)) (d (n "opentracing-rs-jaeger") (r "^0.1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 2)))) (h "1psxfvk426ygxcb6pm9hh7fql99bxjfqpafbzzzwwpg0pmi9ip3v")))

