(define-module (crates-io op en openrtb) #:use-module (crates-io))

(define-public crate-openrtb-0.1.0 (c (n "openrtb") (v "0.1.0") (d (list (d (n "phf") (r "^0.7") (d #t) (k 0)) (d (n "phf_macros") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0jxwyablswk6a8x1ijddnyv24svc06psj1rvix7d9q3v2rr92cc0")))

(define-public crate-openrtb-0.2.0 (c (n "openrtb") (v "0.2.0") (d (list (d (n "phf") (r "~0.8.0") (f (quote ("macros"))) (d #t) (k 0)) (d (n "phf_macros") (r "~0.8.0") (d #t) (k 0)) (d (n "reqwest") (r "~0.10.6") (f (quote ("json" "blocking"))) (d #t) (k 2)) (d (n "serde") (r "~1.0") (d #t) (k 0)) (d (n "serde_derive") (r "~1.0") (d #t) (k 0)) (d (n "serde_json") (r "~1.0") (d #t) (k 0)))) (h "1q12njairxnlrdz0qhg2nw07zvmsyly3h9kp85km343ljhx9g542")))

(define-public crate-openrtb-0.2.1 (c (n "openrtb") (v "0.2.1") (d (list (d (n "phf") (r "~0.8.0") (f (quote ("macros"))) (d #t) (k 0)) (d (n "phf_macros") (r "~0.8.0") (d #t) (k 0)) (d (n "reqwest") (r "~0.10.6") (f (quote ("json" "blocking"))) (d #t) (k 2)) (d (n "serde") (r "~1.0") (d #t) (k 0)) (d (n "serde_derive") (r "~1.0") (d #t) (k 0)) (d (n "serde_json") (r "~1.0") (d #t) (k 0)))) (h "11191ykhkm920xjcpvzav230aavavh6r0j09dcl59dqxrrg1b8ap")))

