(define-module (crates-io op en open) #:use-module (crates-io))

(define-public crate-open-1.0.0 (c (n "open") (v "1.0.0") (h "1x1jq5p7ka0jc79kd07pnwm9wd13a0pnwgr9lggxy58jw8ihx4fh")))

(define-public crate-open-1.0.1 (c (n "open") (v "1.0.1") (h "0xzd5v3f85c7mh0pn4bg012adc4079r97b4plj81blmqsh1fw4cp")))

(define-public crate-open-1.0.2 (c (n "open") (v "1.0.2") (h "1xp3x71lm6pzkh5i5gr9fl89qivi961qvfgns4j93cmsr5hlg92a")))

(define-public crate-open-1.0.3 (c (n "open") (v "1.0.3") (h "1xdbd8zg45jpx4wimzbxy7di1mxsx9crzjp55dm93cmbyng4n3zz")))

(define-public crate-open-1.1.0 (c (n "open") (v "1.1.0") (h "1jdzcxh5pba7vbm7wrrzvi81yl3ydfir9yx3yza24b5zf6kjlma1")))

(define-public crate-open-1.1.1 (c (n "open") (v "1.1.1") (h "07pswn7pqm89l3awsg0n9p5vycy0iaacgxvqw9v6hjmwfxqmja62")))

(define-public crate-open-1.2.0 (c (n "open") (v "1.2.0") (h "0hmqywph6kldcr67yx7grg4zl1ibr6ml1ac1m74004xxhqbfsy1l")))

(define-public crate-open-1.2.1 (c (n "open") (v "1.2.1") (h "04si1m2qlzz34n1rlj57h8ar419x01kr95lrlz7k4i1fk66k30f2")))

(define-public crate-open-1.2.2 (c (n "open") (v "1.2.2") (d (list (d (n "winapi") (r "^0.3") (f (quote ("shellapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0rsq2yfgdlbghqv8rmrd6qgx2rz7ha7qn1gxifa4vn2lgg5a1pzf")))

(define-public crate-open-1.2.3 (c (n "open") (v "1.2.3") (d (list (d (n "winapi") (r "^0.3") (f (quote ("shellapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1z2x0vg1z3017890556p8acf49x3ysg1inv2fmqvfm1jdv2w9fq2")))

(define-public crate-open-1.3.0 (c (n "open") (v "1.3.0") (d (list (d (n "winapi") (r "^0.3") (f (quote ("shellapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1bfgrz6wz02rqs2wsa0pqndbbrgvss8bhbfl1qi1wwx172ysqx9g")))

(define-public crate-open-1.3.1 (c (n "open") (v "1.3.1") (d (list (d (n "winapi") (r "^0.3") (f (quote ("shellapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0pz0s84rx36fa42lz8ad50498rl3hk055r6fav2ccddplfyllb4x")))

(define-public crate-open-1.3.2 (c (n "open") (v "1.3.2") (d (list (d (n "winapi") (r "^0.3") (f (quote ("shellapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0jqkq28r4zcrfsgavgl1303hhdz6ggs6yp1323gv0a3313hj9d4l")))

(define-public crate-open-1.3.3 (c (n "open") (v "1.3.3") (d (list (d (n "winapi") (r "^0.3") (f (quote ("shellapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1yz3cnza13wm09m1xk1k1a0zr41hj26h7319x7hh4rfn44k67yix")))

(define-public crate-open-1.3.4 (c (n "open") (v "1.3.4") (d (list (d (n "winapi") (r "^0.3") (f (quote ("shellapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "027dnj00a64999y2p9dsx20vvrzmn5dc5vdhw800rm8yqgn8jag0")))

(define-public crate-open-1.4.0 (c (n "open") (v "1.4.0") (d (list (d (n "winapi") (r "^0.3") (f (quote ("shellapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0j6ci7jc2aabvw0rvq7a51sw9s2rk3mfsq0s5zjakzjf27q3na3w")))

(define-public crate-open-1.5.0 (c (n "open") (v "1.5.0") (d (list (d (n "which") (r "^4") (d #t) (t "cfg(all(unix, not(macos)))") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("shellapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0vqxnavfgs536mmmrai52aa3xgx2817bc61w5zldv5nqa2nqwa4f") (y #t)))

(define-public crate-open-1.5.1 (c (n "open") (v "1.5.1") (d (list (d (n "which") (r "^4") (d #t) (t "cfg(all(unix, not(macos)))") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("shellapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0kbs5dzdc399pxsz7ghx3sgah0ihrmmy3xgcd13v1m0dcf9ky0xj") (y #t)))

(define-public crate-open-1.6.0 (c (n "open") (v "1.6.0") (d (list (d (n "which") (r "^4") (d #t) (t "cfg(all(unix, not(macos)))") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("shellapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1xw9lg272935b4nqb494avzx1rkf79rjl0691jhgblawy6yz3sd7")))

(define-public crate-open-1.7.0 (c (n "open") (v "1.7.0") (d (list (d (n "which") (r "^4") (d #t) (t "cfg(all(unix, not(macos)))") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("shellapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "083vcabairxahinl8j6407b7lp7ylkxqsv9i1xdx6jnf655yn48p")))

(define-public crate-open-1.7.1 (c (n "open") (v "1.7.1") (d (list (d (n "pathdiff") (r "^0.2.0") (d #t) (t "cfg(all(unix, not(macos)))") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("shellapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "00828zcxdy3r38inz48jgnszgvqgi1a3bi2rrhij86mqsqq7msnw")))

(define-public crate-open-2.0.0 (c (n "open") (v "2.0.0") (d (list (d (n "pathdiff") (r "^0.2.0") (d #t) (t "cfg(all(unix, not(macos)))") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("shellapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0zzkyv8sxlb56srrxzk0r28jpq5q2wx2wq9x1w4ycfaziw5hw3cp")))

(define-public crate-open-2.0.1 (c (n "open") (v "2.0.1") (d (list (d (n "pathdiff") (r "^0.2.0") (d #t) (t "cfg(all(unix, not(macos)))") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("shellapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1gwn9259bjyywj0sbsyc147fi12v7gd2vbj3zrkw2fyqwwyj6sxl")))

(define-public crate-open-2.0.2 (c (n "open") (v "2.0.2") (d (list (d (n "pathdiff") (r "^0.2.0") (d #t) (t "cfg(all(unix, not(macos)))") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("shellapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1dq9c6dgq7iv5kvqfk8gvkfq34j5pdinfcs1hbdd4x6i62vf8vhp")))

(define-public crate-open-2.0.3 (c (n "open") (v "2.0.3") (d (list (d (n "pathdiff") (r "^0.2.0") (d #t) (t "cfg(all(unix, not(macos)))") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("shellapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0rss5ffgw1r6rsqma6h33bkgrxm8d469w4c768pag3d0jr0027jb")))

(define-public crate-open-2.1.0 (c (n "open") (v "2.1.0") (d (list (d (n "pathdiff") (r "^0.2.0") (d #t) (t "cfg(all(unix, not(macos)))") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("shellapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1p092xqdvqzigirwmzca1g6rqcj29j44vim3nsdiahzg6rc930js")))

(define-public crate-open-2.1.1 (c (n "open") (v "2.1.1") (d (list (d (n "pathdiff") (r "^0.2.0") (d #t) (t "cfg(all(unix, not(macos)))") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("shellapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0660lsi0rqv23smbm0mm48wjmwnz3869iqlfh8i7fsm0davff4wj")))

(define-public crate-open-2.1.2 (c (n "open") (v "2.1.2") (d (list (d (n "pathdiff") (r "^0.2.0") (d #t) (t "cfg(all(unix, not(macos)))") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("shellapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0jkn1l7af6gi4pb7id33ixs2frs5c1c0kkhxni75r6wga3wlllp0")))

(define-public crate-open-2.1.3 (c (n "open") (v "2.1.3") (d (list (d (n "pathdiff") (r "^0.2.0") (d #t) (t "cfg(all(unix, not(macos)))") (k 0)) (d (n "windows-sys") (r "^0.36") (f (quote ("Win32_UI_Shell" "Win32_Foundation"))) (d #t) (t "cfg(windows)") (k 0)))) (h "137r49qinkxvw85z2cjlz03ds8w9jrakcm5iqdc2xf25ykxkyhpj")))

(define-public crate-open-3.0.0 (c (n "open") (v "3.0.0") (d (list (d (n "pathdiff") (r "^0.2.0") (d #t) (t "cfg(all(unix, not(macos)))") (k 0)) (d (n "windows-sys") (r "^0.36") (f (quote ("Win32_UI_Shell" "Win32_Foundation"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0vas786dghis90ixp0shi1mzs4j8fwj4yisjfvlqcxmakq41y2zw")))

(define-public crate-open-3.0.1 (c (n "open") (v "3.0.1") (d (list (d (n "pathdiff") (r "^0.2.0") (d #t) (t "cfg(all(unix, not(macos)))") (k 0)) (d (n "windows-sys") (r "^0.36") (f (quote ("Win32_UI_Shell" "Win32_Foundation"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1zhhz4a9ag0418cmj0ky11xidpcavv2crhsl76m66qxz2s1wq2rn")))

(define-public crate-open-3.0.2 (c (n "open") (v "3.0.2") (d (list (d (n "pathdiff") (r "^0.2.0") (d #t) (t "cfg(all(unix, not(macos)))") (k 0)) (d (n "windows-sys") (r "^0.36") (f (quote ("Win32_UI_Shell" "Win32_Foundation"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0if0n34hf5c99ww8gldaiy927phl1mc4b6rzx59ky0x10iq40fpj")))

(define-public crate-open-3.0.3 (c (n "open") (v "3.0.3") (d (list (d (n "pathdiff") (r "^0.2.0") (d #t) (t "cfg(all(unix, not(macos)))") (k 0)) (d (n "windows-sys") (r "^0.36") (f (quote ("Win32_UI_Shell" "Win32_Foundation"))) (d #t) (t "cfg(windows)") (k 0)))) (h "05m7drbvjmxcj2hhj8zjbrrk0wqiwc51nf2k1fj3wwzi840i18xl")))

(define-public crate-open-3.1.0 (c (n "open") (v "3.1.0") (d (list (d (n "pathdiff") (r "^0.2.0") (d #t) (t "cfg(all(unix, not(macos)))") (k 0)) (d (n "windows-sys") (r "^0.42") (f (quote ("Win32_UI_Shell" "Win32_Foundation"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1rg9cgrffg5nwb0gagqkkz3s984nxrlljzcixaj2ickkmpmmpmf2") (y #t)))

(define-public crate-open-3.2.0 (c (n "open") (v "3.2.0") (d (list (d (n "pathdiff") (r "^0.2.0") (d #t) (t "cfg(all(unix, not(macos)))") (k 0)) (d (n "windows-sys") (r "^0.42") (f (quote ("Win32_UI_Shell" "Win32_Foundation" "Win32_UI_WindowsAndMessaging"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1y3nhpgh444ns5vqy87jbcnlryqmw62akyi85k2a0m3akq1w0y10")))

(define-public crate-open-3.3.0 (c (n "open") (v "3.3.0") (d (list (d (n "pathdiff") (r "^0.2.0") (d #t) (t "cfg(all(unix, not(macos)))") (k 0)))) (h "13ajg7d54saqhy858bc1872grb49bwn9nsn78bypc3bi43546jx4") (y #t)))

(define-public crate-open-3.4.0 (c (n "open") (v "3.4.0") (d (list (d (n "pathdiff") (r "^0.2.0") (d #t) (t "cfg(all(unix, not(macos)))") (k 0)))) (h "16hpcd4lsbi4izjsxda1nj3mjy06c64wbljlaxrh95krgr4g5v11") (y #t)))

(define-public crate-open-4.0.0 (c (n "open") (v "4.0.0") (d (list (d (n "pathdiff") (r "^0.2.0") (d #t) (t "cfg(all(unix, not(macos)))") (k 0)))) (h "0nq7c6yq2sjrfag7x8919fcl6zqz8d9bljw6xrr6r5bqknzy6qdx") (y #t) (r "1.62")))

(define-public crate-open-4.0.1 (c (n "open") (v "4.0.1") (d (list (d (n "pathdiff") (r "^0.2.0") (d #t) (t "cfg(all(unix, not(macos)))") (k 0)))) (h "0hraav8b7978cvm16l0xflsj2633c8ghphf6fay9idm2nc1m4p07") (r "1.62")))

(define-public crate-open-4.0.2 (c (n "open") (v "4.0.2") (d (list (d (n "is-wsl") (r "^0.4.0") (d #t) (t "cfg(any(target_os = \"linux\", target_os = \"android\", target_os = \"freebsd\", target_os = \"dragonfly\", target_os = \"netbsd\", target_os = \"openbsd\", target_os = \"illumos\", target_os = \"solaris\"))") (k 0)) (d (n "pathdiff") (r "^0.2.0") (d #t) (t "cfg(all(unix, not(macos)))") (k 0)))) (h "1mzlbigbrbwwlwayl7vpjmb6lijx4icl6fdzs664rm04ljj40cl7") (r "1.62")))

(define-public crate-open-4.1.0 (c (n "open") (v "4.1.0") (d (list (d (n "is-wsl") (r "^0.4.0") (d #t) (t "cfg(any(target_os = \"linux\", target_os = \"android\", target_os = \"freebsd\", target_os = \"dragonfly\", target_os = \"netbsd\", target_os = \"openbsd\", target_os = \"illumos\", target_os = \"solaris\"))") (k 0)) (d (n "pathdiff") (r "^0.2.0") (d #t) (t "cfg(all(unix, not(macos)))") (k 0)))) (h "0i19csry2bsz58453ngm5syjlpxcw2dqr1jklvyiai28cyh18s6i") (r "1.62")))

(define-public crate-open-4.2.0 (c (n "open") (v "4.2.0") (d (list (d (n "is-wsl") (r "^0.4.0") (d #t) (t "cfg(any(target_os = \"linux\", target_os = \"android\", target_os = \"freebsd\", target_os = \"dragonfly\", target_os = \"netbsd\", target_os = \"openbsd\", target_os = \"illumos\", target_os = \"solaris\"))") (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "pathdiff") (r "^0.2.0") (d #t) (t "cfg(all(unix, not(macos)))") (k 0)))) (h "04kw6xmsjm0wdqidjndhpzl79iksyrhwyik32z28wjjygq63q21s") (r "1.62")))

(define-public crate-open-5.0.0 (c (n "open") (v "5.0.0") (d (list (d (n "is-wsl") (r "^0.4.0") (d #t) (t "cfg(any(target_os = \"linux\", target_os = \"android\", target_os = \"freebsd\", target_os = \"dragonfly\", target_os = \"netbsd\", target_os = \"openbsd\", target_os = \"illumos\", target_os = \"solaris\"))") (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "pathdiff") (r "^0.2.0") (d #t) (t "cfg(all(unix, not(macos)))") (k 0)))) (h "1y6fk1n36isp2fi0qy7csg702qah1a534qrxavgnykffgn9g3ayg") (r "1.62")))

(define-public crate-open-5.0.1 (c (n "open") (v "5.0.1") (d (list (d (n "is-wsl") (r "^0.4.0") (d #t) (t "cfg(any(target_os = \"linux\", target_os = \"android\", target_os = \"freebsd\", target_os = \"dragonfly\", target_os = \"netbsd\", target_os = \"openbsd\", target_os = \"illumos\", target_os = \"solaris\" ))") (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "pathdiff") (r "^0.2.0") (d #t) (t "cfg(all(unix, not(macos)))") (k 0)))) (h "0jbkmhd76m2np6l78wqmgqzkmqii5385licjwp2592s4cjv8z1wh") (r "1.62")))

(define-public crate-open-5.0.2 (c (n "open") (v "5.0.2") (d (list (d (n "is-wsl") (r "^0.4.0") (d #t) (t "cfg(any(target_os = \"linux\", target_os = \"android\", target_os = \"freebsd\", target_os = \"dragonfly\", target_os = \"netbsd\", target_os = \"openbsd\", target_os = \"illumos\", target_os = \"solaris\" ))") (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "pathdiff") (r "^0.2.0") (d #t) (t "cfg(all(unix, not(macos)))") (k 0)))) (h "1g1c04dif8ia674qvqs3r1inkqvs61rly8h0yfzkdls9pikzgpzf") (r "1.62")))

(define-public crate-open-5.1.0 (c (n "open") (v "5.1.0") (d (list (d (n "is-wsl") (r "^0.4.0") (d #t) (t "cfg(any(target_os = \"linux\", target_os = \"android\", target_os = \"freebsd\", target_os = \"dragonfly\", target_os = \"netbsd\", target_os = \"openbsd\", target_os = \"illumos\", target_os = \"solaris\" ))") (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "pathdiff") (r "^0.2.0") (d #t) (t "cfg(all(unix, not(macos)))") (k 0)))) (h "09wg7hvr4jj3l0mlfhrx7z066krl0gr7pmgyl7kmdpi2yvnqh98z") (y #t) (r "1.62")))

(define-public crate-open-5.1.1 (c (n "open") (v "5.1.1") (d (list (d (n "is-wsl") (r "^0.4.0") (d #t) (t "cfg(any(target_os = \"linux\", target_os = \"android\", target_os = \"freebsd\", target_os = \"dragonfly\", target_os = \"netbsd\", target_os = \"openbsd\", target_os = \"illumos\", target_os = \"solaris\" ))") (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "pathdiff") (r "^0.2.0") (d #t) (t "cfg(all(unix, not(macos)))") (k 0)))) (h "1v6xxj0rjaikhj1gkgrsshqn04xa0g1x58sv4aswpw1bsnqgpcv8") (f (quote (("shellexecute-on-windows")))) (r "1.62")))

(define-public crate-open-5.1.2 (c (n "open") (v "5.1.2") (d (list (d (n "is-wsl") (r "^0.4.0") (d #t) (t "cfg(any(target_os = \"linux\", target_os = \"android\", target_os = \"freebsd\", target_os = \"dragonfly\", target_os = \"netbsd\", target_os = \"openbsd\", target_os = \"illumos\", target_os = \"solaris\" ))") (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "pathdiff") (r "^0.2.0") (d #t) (t "cfg(all(unix, not(macos)))") (k 0)))) (h "0ciy22dh82rdgmmsiai2wksx7sj9abb4ddnmxpqxnpfqapw0z7s4") (f (quote (("shellexecute-on-windows")))) (r "1.62")))

(define-public crate-open-5.1.3 (c (n "open") (v "5.1.3") (d (list (d (n "is-wsl") (r "^0.4.0") (d #t) (t "cfg(any(target_os = \"linux\", target_os = \"android\", target_os = \"freebsd\", target_os = \"dragonfly\", target_os = \"netbsd\", target_os = \"openbsd\", target_os = \"illumos\", target_os = \"solaris\" ))") (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "pathdiff") (r "^0.2.0") (d #t) (t "cfg(all(unix, not(macos)))") (k 0)))) (h "0mq6vdjy03fzb2rf99s9wrv49nk36imbjb36fjchwn0nasyrzd1f") (f (quote (("shellexecute-on-windows")))) (r "1.62")))

