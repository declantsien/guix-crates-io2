(define-module (crates-io op en opentdb) #:use-module (crates-io))

(define-public crate-opentdb-0.1.0 (c (n "opentdb") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.9.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.84") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.84") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.34") (d #t) (k 0)))) (h "00mgsycnwghj1misv2ai7sgi060xl0pn00qkandj4finvr7xfbrh")))

(define-public crate-opentdb-0.1.1 (c (n "opentdb") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.9.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.84") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.84") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.34") (d #t) (k 0)))) (h "0lvnsrjjvh4v3gcfcrxsiyr0yhl1xvhgnsxx0nlwfwp1wsd497p9")))

(define-public crate-opentdb-1.0.0 (c (n "opentdb") (v "1.0.0") (d (list (d (n "mockito") (r "^0.15.0") (d #t) (k 2)) (d (n "percent-encoding") (r "^1.0.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.84") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.84") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.34") (d #t) (k 0)))) (h "0mzgkc30a82jq8f2njj8axrvmz9d6ay2pbc1x9z8d8bcp5zs8bym")))

