(define-module (crates-io op en openalias) #:use-module (crates-io))

(define-public crate-openalias-0.1.0 (c (n "openalias") (v "0.1.0") (d (list (d (n "clap") (r "^2.26") (d #t) (k 0)) (d (n "crc") (r "^1.5") (d #t) (k 0)) (d (n "embed-resource") (r "^1.1") (d #t) (k 1)) (d (n "peg") (r "^0.5") (d #t) (k 1)) (d (n "resolve") (r "^0.1") (d #t) (k 0)) (d (n "rustfmt") (r "^0.9") (d #t) (k 1)))) (h "0chfv5r1gdgq8rrkxc2qpy5ynnldibzrkd3z0060gls12afags0f")))

(define-public crate-openalias-0.2.0 (c (n "openalias") (v "0.2.0") (d (list (d (n "clap") (r "^2.26") (d #t) (k 0)) (d (n "crc") (r "^1.5") (d #t) (k 0)) (d (n "embed-resource") (r "^1.1") (d #t) (k 1)) (d (n "peg") (r "^0.5") (d #t) (k 1)) (d (n "resolve") (r "^0.1") (d #t) (k 0)) (d (n "rustfmt") (r "^0.9") (d #t) (k 1)))) (h "10ry754bkzcmldrlncprhx7gzb26q44xbynis9k7wssll26j675v")))

