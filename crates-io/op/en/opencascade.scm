(define-module (crates-io op en opencascade) #:use-module (crates-io))

(define-public crate-opencascade-0.1.0 (c (n "opencascade") (v "0.1.0") (d (list (d (n "cxx") (r "^1") (d #t) (k 0)) (d (n "opencascade-sys") (r "^0.1") (d #t) (k 0)))) (h "0lwshbbxi4zx6bcgpgzlkn4sqy78l260nwhp2y14g7qjb0c5pvc1")))

(define-public crate-opencascade-0.2.0 (c (n "opencascade") (v "0.2.0") (d (list (d (n "cxx") (r "^1") (d #t) (k 0)) (d (n "glam") (r "^0.23") (f (quote ("bytemuck"))) (d #t) (k 0)) (d (n "opencascade-sys") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1ihavpipc4x2irays1ha9612ch3gc4732lw893zyvs6k6kahgb3w")))

