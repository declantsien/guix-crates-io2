(define-module (crates-io op en openai-gpt-client) #:use-module (crates-io))

(define-public crate-openai-gpt-client-0.0.0 (c (n "openai-gpt-client") (v "0.0.0") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.14") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.93") (d #t) (k 0)) (d (n "serde_with") (r "^2.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "0lcpxy5daq4h4lhzfh9h665jycj0k7zph3nvwiriq8yacnrg14pb")))

