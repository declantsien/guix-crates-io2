(define-module (crates-io op en openfpga-xc2bit) #:use-module (crates-io))

(define-public crate-openfpga-xc2bit-0.0.0 (c (n "openfpga-xc2bit") (v "0.0.0") (h "0b2psj8bqygk4fg8jlfgwb80wc8xhib03f7vnyqmic9xac6dq42q") (y #t)))

(define-public crate-openfpga-xc2bit-0.0.1 (c (n "openfpga-xc2bit") (v "0.0.1") (h "11ql1g6lhp2ixhkghbnmm4dny2f6gkmjfml5d4j1bpzzkcqzhil6") (y #t)))

