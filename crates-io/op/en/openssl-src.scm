(define-module (crates-io op en openssl-src) #:use-module (crates-io))

(define-public crate-openssl-src-110.0.0 (c (n "openssl-src") (v "110.0.0") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 0)))) (h "16lc7lxvprv5zsps6a9rrh8wwzypx9zkq1ngpgqfqyssyh2b5dmg")))

(define-public crate-openssl-src-110.0.0+1.1.0f (c (n "openssl-src") (v "110.0.0+1.1.0f") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 0)))) (h "10qa9s6fsqr77hk9pk8ld6slc0djcsw2sag39s2dv6nmwzb57phs")))

(define-public crate-openssl-src-110.0.1+1.1.0f (c (n "openssl-src") (v "110.0.1+1.1.0f") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 0)))) (h "06kifa9vad6ja2r642fy5x92svwng6isnlgmkn0wqkfpw0m042kp")))

(define-public crate-openssl-src-110.0.2+1.1.0h (c (n "openssl-src") (v "110.0.2+1.1.0h") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 0)))) (h "1hhxbaqgy9y7dzr0mahjnhggd9rx5wg85dgq73jyykj05haan4q4")))

(define-public crate-openssl-src-110.0.3+1.1.0h (c (n "openssl-src") (v "110.0.3+1.1.0h") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 0)))) (h "1h7w23h185n23dnk7jax28jmch0plb3g1ijdwgrmv9v89p06aqdf")))

(define-public crate-openssl-src-110.0.4+1.1.0h (c (n "openssl-src") (v "110.0.4+1.1.0h") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 0)))) (h "08b726yawss4yj4d0w91b87mfzijlgd6xgmnrg7rv8rml4nz73vz")))

(define-public crate-openssl-src-110.0.5+1.1.0h (c (n "openssl-src") (v "110.0.5+1.1.0h") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 0)))) (h "01hahr6m5kxhwy0jq6i19m76gkgdc2ww924clfr34l32az48vzxc")))

(define-public crate-openssl-src-110.0.6+1.1.0h (c (n "openssl-src") (v "110.0.6+1.1.0h") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)))) (h "1d53i6lbzwd3lr1yl9gl0jdilidzkhf74digk0p0z70x047ja490")))

(define-public crate-openssl-src-110.0.7+1.1.0i (c (n "openssl-src") (v "110.0.7+1.1.0i") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)))) (h "05jmmlc1h36654m1w3s4g50baf8ik0yqydd2hc63y6vxick1abf0")))

(define-public crate-openssl-src-111.0.0+1.1.1 (c (n "openssl-src") (v "111.0.0+1.1.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)))) (h "1jj87l00s7avzsrzf62hcy0rrvcwrmpwys5wsq64m1gh0vm1v7pp")))

(define-public crate-openssl-src-111.0.1+1.1.1 (c (n "openssl-src") (v "111.0.1+1.1.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)))) (h "1gpw4zdsi27hg7y4i9xilgfyzp9i56ch5gs64r5ab1gxi3caxvfi")))

(define-public crate-openssl-src-111.1.0+1.1.1a (c (n "openssl-src") (v "111.1.0+1.1.1a") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)))) (h "19vdpk6a4kcxswwzqd12ij8s0b1dw7fsb1mz96ng86vk4whn7fr6")))

(define-public crate-openssl-src-111.1.1+1.1.1a (c (n "openssl-src") (v "111.1.1+1.1.1a") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)))) (h "1k7ilf1ai0izfywf9ajgfs79vrdgdvfnc297czf3riw08bb05pi6")))

(define-public crate-openssl-src-111.2.1+1.1.1b (c (n "openssl-src") (v "111.2.1+1.1.1b") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)))) (h "0f94spq1p9xzf6lcm4568d61c4mqfpiax58pyl6lpnm3qll4lbm4")))

(define-public crate-openssl-src-111.3.0+1.1.1c (c (n "openssl-src") (v "111.3.0+1.1.1c") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)))) (h "15y7fdw33idbdc3y7dv3k87gdc2l4xn202mslkvzbgcls8qmzvak")))

(define-public crate-openssl-src-111.4.0+1.1.1c (c (n "openssl-src") (v "111.4.0+1.1.1c") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)))) (h "10s3hvkfk6bi4ba1ssvj914rjs31vs8plssy4kbvsa0w7idkqfkq")))

(define-public crate-openssl-src-111.5.0+1.1.1c (c (n "openssl-src") (v "111.5.0+1.1.1c") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)))) (h "17h4jwa3n1i91h0q8g72c1d9xzm97bnkxn1s7rljyghp94zvzpjb")))

(define-public crate-openssl-src-111.6.0+1.1.1d (c (n "openssl-src") (v "111.6.0+1.1.1d") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)))) (h "172xh95hp7aygahah1940kg1dnx60c5m80cwj5hgi8x7x0fxmhmr")))

(define-public crate-openssl-src-111.6.1+1.1.1d (c (n "openssl-src") (v "111.6.1+1.1.1d") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)))) (h "01ahygfhr2i7j12gyv1ddnbmswd5w89dc32fjc7aka618g5h86y9")))

(define-public crate-openssl-src-111.7.0+1.1.1e (c (n "openssl-src") (v "111.7.0+1.1.1e") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)))) (h "0b6bm2ag5j8wwmk0pjhghkc1wn28waxgkanhz0gs72pg0665mpkg")))

(define-public crate-openssl-src-111.8.0+1.1.1f (c (n "openssl-src") (v "111.8.0+1.1.1f") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)))) (h "0b8hqq9p6psmia2z1cmiwagnm9cv2nm3jfib3pi2hnkb5v7k74va") (y #t)))

(define-public crate-openssl-src-111.8.1+1.1.1f (c (n "openssl-src") (v "111.8.1+1.1.1f") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)))) (h "0wlbgjr2i3vrjwr6m2aq7p7qsjcm3089jbldwpfrir8xm6ch4kzh")))

(define-public crate-openssl-src-111.9.0+1.1.1g (c (n "openssl-src") (v "111.9.0+1.1.1g") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)))) (h "1i4yyckhjx4q4fbgsqi53m27n6ryl7mb43kqlfmkbcqyvl6y3nx2")))

(define-public crate-openssl-src-111.10.0+1.1.1g (c (n "openssl-src") (v "111.10.0+1.1.1g") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)))) (h "1rk5y9r9fq2z3kxmiw88av3k184qm75q12lfmi6byflwsjb4mka7")))

(define-public crate-openssl-src-111.10.1+1.1.1g (c (n "openssl-src") (v "111.10.1+1.1.1g") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)))) (h "10rgw74l0sf0gypvaiqvhnb4p5apm2h90a0yrzvn41yzdlqi4prp")))

(define-public crate-openssl-src-111.10.2+1.1.1g (c (n "openssl-src") (v "111.10.2+1.1.1g") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)))) (h "0w2sxxpk4m055b2dph0ffdzpg0my5nhag9fl4h3bdd9j5srgv1x2") (f (quote (("weak-crypto") ("seed") ("idea") ("default") ("camellia"))))))

(define-public crate-openssl-src-111.11.0+1.1.1h (c (n "openssl-src") (v "111.11.0+1.1.1h") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)))) (h "0nzsp7kimv4v0c6r6aazc52b1ba3jgndzyirabs03sib2cjf63rq") (f (quote (("weak-crypto") ("seed") ("idea") ("default") ("camellia"))))))

(define-public crate-openssl-src-111.12.0+1.1.1h (c (n "openssl-src") (v "111.12.0+1.1.1h") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)))) (h "0qdzdaayamkmx1ia3m7jcm2vqg5jhng65f4yxskp11ag34r432l5") (f (quote (("weak-crypto") ("seed") ("idea") ("default") ("camellia"))))))

(define-public crate-openssl-src-111.13.0+1.1.1i (c (n "openssl-src") (v "111.13.0+1.1.1i") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)))) (h "1dnw179mivfi2bd98r48yhkaw8ijnj4mg235ss9ssypmib24sph4") (f (quote (("weak-crypto") ("seed") ("idea") ("default") ("camellia"))))))

(define-public crate-openssl-src-111.14.0+1.1.1j (c (n "openssl-src") (v "111.14.0+1.1.1j") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)))) (h "1ac0pc69yzxgfr0hdkmmfcfnk1ylqz3rbx802wm4drfpbfdmcnq5") (f (quote (("weak-crypto") ("seed") ("idea") ("default") ("camellia"))))))

(define-public crate-openssl-src-111.15.0+1.1.1k (c (n "openssl-src") (v "111.15.0+1.1.1k") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)))) (h "02ppia85vlgkd4h8gqpsjcyvzaags06717za2yr96hy05apgd9di") (f (quote (("weak-crypto") ("seed") ("idea") ("default") ("camellia"))))))

(define-public crate-openssl-src-111.16.0+1.1.1l (c (n "openssl-src") (v "111.16.0+1.1.1l") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)))) (h "17w5l9gpracs32jxa9xim4ips4j4s8imifyy2bng6v21d4zigcks") (f (quote (("weak-crypto") ("seed") ("idea") ("default") ("camellia"))))))

(define-public crate-openssl-src-300.0.0+3.0.0 (c (n "openssl-src") (v "300.0.0+3.0.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)))) (h "1jawb17g0z5ibw9k0b1gp4xm715bc6rdwggb838ign0wv9fa1zq0") (f (quote (("weak-crypto") ("seed") ("idea") ("default") ("camellia"))))))

(define-public crate-openssl-src-300.0.1+3.0.0 (c (n "openssl-src") (v "300.0.1+3.0.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)))) (h "19nbywr690ra2jiya29qb99ac0xdxpqzh21484fq71axicczjign") (f (quote (("weak-crypto") ("seed") ("idea") ("default") ("camellia"))))))

(define-public crate-openssl-src-300.0.2+3.0.0 (c (n "openssl-src") (v "300.0.2+3.0.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)))) (h "1yyi8s0yxsapkyciy6df6hjpfvhszzvd8x10yzdabcch2fhn19ql") (f (quote (("weak-crypto") ("seed") ("idea") ("default") ("camellia"))))))

(define-public crate-openssl-src-300.0.3+3.0.0 (c (n "openssl-src") (v "300.0.3+3.0.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)))) (h "0d5is4lp710cdzjkk7ym50yrywm98hfwwkcl0g1j7jwdybgcw7n5") (f (quote (("weak-crypto") ("seed") ("legacy") ("idea") ("default") ("camellia"))))))

(define-public crate-openssl-src-300.0.4+3.0.1 (c (n "openssl-src") (v "300.0.4+3.0.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)))) (h "1rr3gf7dl6415mxlrx6sd658h52lcj7jdanpp6143qj98mmiqvi1") (f (quote (("weak-crypto") ("seed") ("legacy") ("idea") ("default") ("camellia"))))))

(define-public crate-openssl-src-111.17.0+1.1.1m (c (n "openssl-src") (v "111.17.0+1.1.1m") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)))) (h "1x7wcrk56073ik51jh9k60gn2dkkrlfakqk6iwci826imcva7mh5") (f (quote (("weak-crypto") ("seed") ("idea") ("default") ("camellia"))))))

(define-public crate-openssl-src-111.18.0+1.1.1n (c (n "openssl-src") (v "111.18.0+1.1.1n") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)))) (h "0v7zlbnc6whd929davb6rbjr4hna1q9j1h3x28ch5l78w4kak5vq") (f (quote (("weak-crypto") ("seed") ("idea") ("default") ("camellia"))))))

(define-public crate-openssl-src-300.0.5+3.0.2 (c (n "openssl-src") (v "300.0.5+3.0.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)))) (h "18ab35rqy3jcsr4ggr9gyl0fwbmkrnhkkf6rd2bl7r0ya6pm7l4v") (f (quote (("weak-crypto") ("seed") ("legacy") ("idea") ("default") ("camellia"))))))

(define-public crate-openssl-src-300.0.6+3.0.3 (c (n "openssl-src") (v "300.0.6+3.0.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)))) (h "04g3zg6qaa0x6w8ygl90f8rfnfvvamvbcc2rwcwl164pbf13xp4x") (f (quote (("weak-crypto") ("seed") ("legacy") ("idea") ("default") ("camellia"))))))

(define-public crate-openssl-src-111.19.0+1.1.1o (c (n "openssl-src") (v "111.19.0+1.1.1o") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)))) (h "067dfygp6ywpxl3gm0zf3hm3pag8x8qzfzvjha3vfqhzrhblljlk") (f (quote (("weak-crypto") ("seed") ("idea") ("default") ("camellia"))))))

(define-public crate-openssl-src-300.0.7+3.0.3 (c (n "openssl-src") (v "300.0.7+3.0.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)))) (h "18hgrvp9xfvycbjkpmc58ibv0im41plqb0z98p8xyswk5diqa3hg") (f (quote (("weak-crypto") ("seed") ("legacy") ("idea") ("default") ("camellia"))))))

(define-public crate-openssl-src-111.20.0+1.1.1o (c (n "openssl-src") (v "111.20.0+1.1.1o") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)))) (h "1v1df7zhcixyl0xggpa6fv8dxnlg2bqpkkls8rp3fvnmhx7jr2cj") (f (quote (("weak-crypto") ("seed") ("idea") ("default") ("camellia"))))))

(define-public crate-openssl-src-111.21.0+1.1.1p (c (n "openssl-src") (v "111.21.0+1.1.1p") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)))) (h "020h6slgi1w270aw2n6n9khg5iszgaj5rfgi6qwr24cjf89q62kd") (f (quote (("weak-crypto") ("seed") ("idea") ("default") ("camellia"))))))

(define-public crate-openssl-src-300.0.8+3.0.4 (c (n "openssl-src") (v "300.0.8+3.0.4") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)))) (h "0krbjn4ayabx1argnp60c711x1mrnyrj4pm0s49fimhz59badlq0") (f (quote (("weak-crypto") ("seed") ("legacy") ("idea") ("default") ("camellia"))))))

(define-public crate-openssl-src-300.0.9+3.0.5 (c (n "openssl-src") (v "300.0.9+3.0.5") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)))) (h "0x7ig38caa1gm0pmvrpbk98nyr7vsa20pgqwb98pdlp2hxk12lnz") (f (quote (("weak-crypto") ("seed") ("legacy") ("idea") ("default") ("camellia"))))))

(define-public crate-openssl-src-111.22.0+1.1.1q (c (n "openssl-src") (v "111.22.0+1.1.1q") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)))) (h "0lwqcmynial2wiwqh2fm3y1k6ygkz2gm7yd2mnfaxhfi17az0ccg") (f (quote (("weak-crypto") ("seed") ("idea") ("default") ("camellia"))))))

(define-public crate-openssl-src-300.0.10+3.0.6 (c (n "openssl-src") (v "300.0.10+3.0.6") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)))) (h "15pd2cf70z6sg5qzrcls7x5ja722iwr71jn6b7107br7vrdv61n2") (f (quote (("weak-crypto") ("seed") ("legacy") ("idea") ("default") ("camellia")))) (y #t)))

(define-public crate-openssl-src-111.23.0+1.1.1r (c (n "openssl-src") (v "111.23.0+1.1.1r") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)))) (h "18jwg9waz249qp0bfi40inl5bkzjfg37bj3ci19xd4mfa0jlb6s7") (f (quote (("weak-crypto") ("seed") ("idea") ("force-engine") ("default") ("camellia")))) (y #t)))

(define-public crate-openssl-src-111.24.0+1.1.1s (c (n "openssl-src") (v "111.24.0+1.1.1s") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)))) (h "1gaax9rpf8k1g00qqr0dwbas5lhfxzf00swc4b37h4dhv9cz561l") (f (quote (("weak-crypto") ("seed") ("idea") ("force-engine") ("default") ("camellia"))))))

(define-public crate-openssl-src-300.0.11+3.0.7 (c (n "openssl-src") (v "300.0.11+3.0.7") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)))) (h "0ymibk1mbq6xli3bkq778qy52sh9dl7g7k7nfn80j64810lj87hq") (f (quote (("weak-crypto") ("seed") ("legacy") ("idea") ("default") ("camellia"))))))

(define-public crate-openssl-src-111.25.0+1.1.1t (c (n "openssl-src") (v "111.25.0+1.1.1t") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)))) (h "1mj3nz7m5kr2g0cyr37yqng8smkf4wm449xpn5a3hgn44qvcswri") (f (quote (("weak-crypto") ("seed") ("idea") ("force-engine") ("default") ("camellia"))))))

(define-public crate-openssl-src-300.0.12+3.0.8 (c (n "openssl-src") (v "300.0.12+3.0.8") (d (list (d (n "cc") (r "^1.0.79") (d #t) (k 0)))) (h "1386jy4ar51j6x14qx0iaqciwzj7zmwfa9fvbfrvlwv3fdal15sd") (f (quote (("weak-crypto") ("seed") ("legacy") ("idea") ("default") ("camellia"))))))

(define-public crate-openssl-src-111.25.1+1.1.1t (c (n "openssl-src") (v "111.25.1+1.1.1t") (d (list (d (n "cc") (r "^1.0.79") (d #t) (k 0)))) (h "045s995hd9vbr7z1vbq1vznmkqyh915xq4x9qkkxbnd7dv6aky8y") (f (quote (("weak-crypto") ("seed") ("idea") ("force-engine") ("default") ("camellia"))))))

(define-public crate-openssl-src-111.25.2+1.1.1t (c (n "openssl-src") (v "111.25.2+1.1.1t") (d (list (d (n "cc") (r "^1.0.79") (d #t) (k 0)))) (h "0ca4pxiaw93r6gi3bj6gchynimplgjw5v2v82krkp6xdajh0h1rj") (f (quote (("weak-crypto") ("seed") ("idea") ("force-engine") ("default") ("camellia"))))))

(define-public crate-openssl-src-300.0.13+3.0.8 (c (n "openssl-src") (v "300.0.13+3.0.8") (d (list (d (n "cc") (r "^1.0.79") (d #t) (k 0)))) (h "02jgh5pqjnd8scrf6d943gzdrbw2hljz5c5dm76iz5f87sk8j1zh") (f (quote (("weak-crypto") ("seed") ("legacy") ("idea") ("default") ("camellia"))))))

(define-public crate-openssl-src-300.1.0+3.1.0 (c (n "openssl-src") (v "300.1.0+3.1.0") (d (list (d (n "cc") (r "^1.0.79") (d #t) (k 0)))) (h "08lkzkn43s4fg627v7ysnc74qinhj2zxg1iyr9k4zdwcx5rdg5l5") (f (quote (("weak-crypto") ("seed") ("legacy") ("idea") ("default") ("camellia"))))))

(define-public crate-openssl-src-111.25.3+1.1.1t (c (n "openssl-src") (v "111.25.3+1.1.1t") (d (list (d (n "cc") (r "^1.0.79") (d #t) (k 0)))) (h "037qc9iwdp88688fppc2vn1j5dfj6hd33l3xbzd61gr6lak5fiwj") (f (quote (("weak-crypto") ("seed") ("idea") ("force-engine") ("default") ("camellia"))))))

(define-public crate-openssl-src-300.1.1+3.1.0 (c (n "openssl-src") (v "300.1.1+3.1.0") (d (list (d (n "cc") (r "^1.0.79") (d #t) (k 0)))) (h "11bv5llc29vn0mbg8avi7cvc6gpy2yry48nmbarxa5rsny3cn4ak") (f (quote (("weak-crypto") ("seed") ("legacy") ("idea") ("default") ("camellia"))))))

(define-public crate-openssl-src-300.1.2+3.1.1 (c (n "openssl-src") (v "300.1.2+3.1.1") (d (list (d (n "cc") (r "^1.0.79") (d #t) (k 0)))) (h "0p8lnpgrdm71iff57vr3v53lm5jq685vppiig9riznd465hklxwl") (f (quote (("weak-crypto") ("seed") ("legacy") ("idea") ("default") ("camellia"))))))

(define-public crate-openssl-src-111.26.0+1.1.1u (c (n "openssl-src") (v "111.26.0+1.1.1u") (d (list (d (n "cc") (r "^1.0.79") (d #t) (k 0)))) (h "0dvg9xd2n4jj70iq1pwbkycybcj2lh074fn21198yaxj2agjripg") (f (quote (("weak-crypto") ("seed") ("idea") ("force-engine") ("default") ("camellia"))))))

(define-public crate-openssl-src-300.1.3+3.1.2 (c (n "openssl-src") (v "300.1.3+3.1.2") (d (list (d (n "cc") (r "^1.0.79") (d #t) (k 0)))) (h "01z15d43wl11dvl2pi23g62cgcasb5lldvsdwcsrkzsz2qd10b6d") (f (quote (("weak-crypto") ("seed") ("legacy") ("idea") ("default") ("camellia"))))))

(define-public crate-openssl-src-111.27.0+1.1.1v (c (n "openssl-src") (v "111.27.0+1.1.1v") (d (list (d (n "cc") (r "^1.0.79") (d #t) (k 0)))) (h "00izwsjj8dgdrss277grspbl1npy3sdhq0qln32i2x9dr2bz3s06") (f (quote (("weak-crypto") ("seed") ("idea") ("force-engine") ("default") ("camellia"))))))

(define-public crate-openssl-src-111.28.0+1.1.1w (c (n "openssl-src") (v "111.28.0+1.1.1w") (d (list (d (n "cc") (r "^1.0.79") (d #t) (k 0)))) (h "07vhmqidnc2cm6cwn72d22i8wxa2wkml7zcabfwxz6gryvhmxs9w") (f (quote (("weak-crypto") ("seed") ("idea") ("force-engine") ("default") ("camellia"))))))

(define-public crate-openssl-src-300.1.4+3.1.2 (c (n "openssl-src") (v "300.1.4+3.1.2") (d (list (d (n "cc") (r "^1.0.79") (d #t) (k 0)))) (h "18pzjc077kcvka3cvwhd36l7b851ayfgwv7j7a9bl7py465nbdl0") (f (quote (("weak-crypto") ("seed") ("legacy") ("idea") ("force-engine") ("default") ("camellia"))))))

(define-public crate-openssl-src-300.1.5+3.1.3 (c (n "openssl-src") (v "300.1.5+3.1.3") (d (list (d (n "cc") (r "^1.0.79") (d #t) (k 0)))) (h "14b465p5v9rimdqf1y1zq17qvly04lbscmqqmbfdfl19q7j6i42m") (f (quote (("weak-crypto") ("seed") ("legacy") ("idea") ("force-engine") ("default") ("camellia"))))))

(define-public crate-openssl-src-300.1.6+3.1.4 (c (n "openssl-src") (v "300.1.6+3.1.4") (d (list (d (n "cc") (r "^1.0.79") (d #t) (k 0)))) (h "11c0p3vapkcd7090j11rpnsv6fk4wkfqa336ld179kcjw19sr7s3") (f (quote (("weak-crypto") ("seed") ("legacy") ("idea") ("force-engine") ("default") ("camellia"))))))

(define-public crate-openssl-src-300.2.0+3.2.0 (c (n "openssl-src") (v "300.2.0+3.2.0") (d (list (d (n "cc") (r "^1.0.79") (d #t) (k 0)))) (h "1jlzhp1hddy17j93260iikcz74hhzv0xcwyd5d6dck4c30fyvsxi") (f (quote (("weak-crypto") ("seed") ("legacy") ("idea") ("force-engine") ("default") ("camellia"))))))

(define-public crate-openssl-src-300.2.1+3.2.0 (c (n "openssl-src") (v "300.2.1+3.2.0") (d (list (d (n "cc") (r "^1.0.79") (d #t) (k 0)))) (h "1lrj0fsa8y3yc7aszxrfxaxspfw5c3lrgikk286wm9cijz17dr1z") (f (quote (("weak-crypto") ("seed") ("legacy") ("idea") ("force-engine") ("default") ("camellia"))))))

(define-public crate-openssl-src-111.28.1+1.1.1w (c (n "openssl-src") (v "111.28.1+1.1.1w") (d (list (d (n "cc") (r "^1.0.79") (d #t) (k 0)))) (h "1d59xx6f8mq0h3kyhm43nasni7shhpyhnsi14ijnwgbdzlpyixsb") (f (quote (("weak-crypto") ("seed") ("idea") ("force-engine") ("default") ("camellia"))))))

(define-public crate-openssl-src-300.2.2+3.2.1 (c (n "openssl-src") (v "300.2.2+3.2.1") (d (list (d (n "cc") (r "^1.0.79") (d #t) (k 0)))) (h "1g03h5f0p0v3jaw56964fmd5a1rbkrrq8x4zwxpc42k1cc0avgwb") (f (quote (("weak-crypto") ("seed") ("legacy") ("idea") ("force-engine") ("default") ("camellia"))))))

(define-public crate-openssl-src-300.2.3+3.2.1 (c (n "openssl-src") (v "300.2.3+3.2.1") (d (list (d (n "cc") (r "^1.0.79") (d #t) (k 0)))) (h "0hs83dvgr581x87i48hnaf3kv91wv9j7qprip4dvcm8myyv95zsw") (f (quote (("weak-crypto") ("seed") ("legacy") ("idea") ("force-engine") ("default") ("camellia"))))))

(define-public crate-openssl-src-300.3.0+3.3.0 (c (n "openssl-src") (v "300.3.0+3.3.0") (d (list (d (n "cc") (r "^1.0.79") (d #t) (k 0)))) (h "1qb38vs4qjkc0jhf2c4qpkgfxazbjxl7x41z9f6b2rap3i581a7b") (f (quote (("weak-crypto") ("seed") ("legacy") ("ktls") ("idea") ("force-engine") ("default") ("camellia"))))))

(define-public crate-openssl-src-111.28.2+1.1.1w (c (n "openssl-src") (v "111.28.2+1.1.1w") (d (list (d (n "cc") (r "^1.0.79") (d #t) (k 0)))) (h "03wjaxw5h1k744fbrnmpbin3qv536v8c3a4ci757baa81bi3065v") (f (quote (("weak-crypto") ("seed") ("idea") ("force-engine") ("default") ("camellia"))))))

