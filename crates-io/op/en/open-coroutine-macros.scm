(define-module (crates-io op en open-coroutine-macros) #:use-module (crates-io))

(define-public crate-open-coroutine-macros-0.1.0 (c (n "open-coroutine-macros") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("full"))) (d #t) (k 0)))) (h "04vw39b8pz3jf1vzxym3vlnvld54lqlnzbwvgscxnl86l0c6qphc")))

(define-public crate-open-coroutine-macros-0.4.0 (c (n "open-coroutine-macros") (v "0.4.0") (d (list (d (n "num_cpus") (r "^1.14.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^2.0.4") (f (quote ("full"))) (d #t) (k 0)))) (h "1qzgc81lx853jq62drhhxp9p3naj3ddd6xd88g21px7rpgcx9gxd")))

(define-public crate-open-coroutine-macros-0.1.1 (c (n "open-coroutine-macros") (v "0.1.1") (d (list (d (n "num_cpus") (r "^1.14.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^2.0.4") (f (quote ("full"))) (d #t) (k 0)))) (h "1d6dwcpgh104r4ar9q6pm3p8axbr76dsbmbp0nngslc54lr2sl6k")))

(define-public crate-open-coroutine-macros-0.5.0 (c (n "open-coroutine-macros") (v "0.5.0") (d (list (d (n "num_cpus") (r "^1.14.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.32") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (f (quote ("full"))) (d #t) (k 0)))) (h "0xwjzkzcpgpjavaqyfln3wpsn2wh64jz5ydk1d3cvll8bmrzpdbq")))

