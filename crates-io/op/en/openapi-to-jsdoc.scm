(define-module (crates-io op en openapi-to-jsdoc) #:use-module (crates-io))

(define-public crate-openapi-to-jsdoc-0.1.1 (c (n "openapi-to-jsdoc") (v "0.1.1") (d (list (d (n "clap") (r "^4.5.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "oas3") (r "^0.4.0") (d #t) (k 0)) (d (n "openapiv3-extended") (r "^6.0.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.12.1") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0vlxay4nxi5ax8hx5rwacq0aijrvg04x4xnmlg9r7fk9l944r645")))

(define-public crate-openapi-to-jsdoc-0.1.2 (c (n "openapi-to-jsdoc") (v "0.1.2") (d (list (d (n "clap") (r "^4.5.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "oas3") (r "^0.4.0") (d #t) (k 0)) (d (n "openapiv3-extended") (r "^6.0.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.12.1") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0q5aqnlxrlb44cylipha2mfj9jwjh6ydl7l2k8ln4852mav0lswn")))

(define-public crate-openapi-to-jsdoc-0.1.3 (c (n "openapi-to-jsdoc") (v "0.1.3") (d (list (d (n "clap") (r "^4.5.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "oas3") (r "^0.4.0") (d #t) (k 0)) (d (n "openapiv3-extended") (r "^6.0.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.12.1") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1q62582r1qlk9sgl5myjws02ri9w3bpgsh747a0pf0vkhr2xgkvm")))

