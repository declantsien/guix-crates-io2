(define-module (crates-io op en openldap) #:use-module (crates-io))

(define-public crate-openldap-1.0.0 (c (n "openldap") (v "1.0.0") (d (list (d (n "libc") (r "^0.2.10") (d #t) (k 0)))) (h "0fd77xrhj77h9wch9wwcsbwdvmc2p3nzhpxhlzjvpjzm2njmirsj")))

(define-public crate-openldap-1.0.1 (c (n "openldap") (v "1.0.1") (d (list (d (n "libc") (r "^0.2.10") (d #t) (k 0)))) (h "0sip0y38g45v34sac8rbh7hynvh5r11d3jr9h5732q1r0d37w3k7") (y #t)))

(define-public crate-openldap-1.0.2 (c (n "openldap") (v "1.0.2") (d (list (d (n "libc") (r "^0.2.10") (d #t) (k 0)))) (h "0bsg1illzraj840lzvf5qcibrn5yga0391zm3fa9z5lvy06ygs1s")))

(define-public crate-openldap-1.1.0 (c (n "openldap") (v "1.1.0") (d (list (d (n "libc") (r "^0.2.10") (d #t) (k 0)))) (h "11ymmb5na8k06fqv6nkazc69mm6cxvi9m428gjbj76j62pl975hr")))

(define-public crate-openldap-1.2.0 (c (n "openldap") (v "1.2.0") (d (list (d (n "libc") (r "^0.2.10") (d #t) (k 0)))) (h "18qhn5330yi3gqhjq6d1pzsq5hv3dhxhapfy86xjifysvdkzmh3s")))

(define-public crate-openldap-1.2.1 (c (n "openldap") (v "1.2.1") (d (list (d (n "libc") (r "^0.2.10") (d #t) (k 0)))) (h "03jaz9fsb5whvyxi63mfhcqr3l2qcqxrf57jzmggglvglwjbgcla")))

(define-public crate-openldap-1.2.2 (c (n "openldap") (v "1.2.2") (d (list (d (n "libc") (r "^0.2.10") (d #t) (k 0)))) (h "05m211xanwsdm5k0nbm5ia4f8b72xa9jps0p57akr17adnjbyzxj")))

