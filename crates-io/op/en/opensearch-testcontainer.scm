(define-module (crates-io op en opensearch-testcontainer) #:use-module (crates-io))

(define-public crate-opensearch-testcontainer-0.1.0 (c (n "opensearch-testcontainer") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11.24") (f (quote ("blocking"))) (d #t) (k 2)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.113") (d #t) (k 2)) (d (n "testcontainers") (r "^0.15.0") (d #t) (k 0)))) (h "1xd1vqnkvqj0v803dr4g8vv4d8gilwmk2wl9jb6i9rxw0wy3ihav")))

