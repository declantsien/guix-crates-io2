(define-module (crates-io op en opentelemetry-tonic) #:use-module (crates-io))

(define-public crate-opentelemetry-tonic-0.1.0 (c (n "opentelemetry-tonic") (v "0.1.0") (d (list (d (n "opentelemetry") (r "^0.17") (d #t) (k 0)) (d (n "tonic") (r "^0.8") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-opentelemetry") (r "^0.17") (d #t) (k 0)))) (h "0l7fx4vq2l825yjywm0z9yr9437vdn3ysr9r8m0czisfxawp4c68")))

