(define-module (crates-io op en openai-api-rs) #:use-module (crates-io))

(define-public crate-openai-api-rs-0.1.0 (c (n "openai-api-rs") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "16nbak7g2n70x281ixv1wk9pf72mx1w19cl8crgj5xi45w3vkn1n")))

(define-public crate-openai-api-rs-0.1.1 (c (n "openai-api-rs") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1lhicdh8m4x43kfxy4kbnbbcq8q70r7g5qvfddrhwss7wcgr1zj4")))

(define-public crate-openai-api-rs-0.1.2 (c (n "openai-api-rs") (v "0.1.2") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0ihym6z98l4rg81yarss54249skbm8m7kacfh3i9clrnblm9bjwm")))

(define-public crate-openai-api-rs-0.1.3 (c (n "openai-api-rs") (v "0.1.3") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1ri47sljj81nr3fvd195rm3x2vikpf8c5cy2npxrm9lh28qik1hd")))

(define-public crate-openai-api-rs-0.1.4 (c (n "openai-api-rs") (v "0.1.4") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1c2cmm4x33az9n1ixv5nf78apf4mql2p9cwg29bkhh1srrwbxf91")))

(define-public crate-openai-api-rs-0.1.5 (c (n "openai-api-rs") (v "0.1.5") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1r22yrlk4p7ywl4ysxxqr8wzkmpi4g0biygd4yclg1wdf0hv4x1r")))

(define-public crate-openai-api-rs-0.1.6 (c (n "openai-api-rs") (v "0.1.6") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "12dsizighrdr4g4mx10cbjhi2xslxsmxrp0d6yr9g6947i2clls3")))

(define-public crate-openai-api-rs-0.1.7 (c (n "openai-api-rs") (v "0.1.7") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1w7qg4q0n39lba2l9i7b3hpv4bkdcpqabfclkp6i1y76sfzyvrnb")))

(define-public crate-openai-api-rs-0.1.8 (c (n "openai-api-rs") (v "0.1.8") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.97") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "01sxylqqyizl658fk5xdd11anvkvaa805jvn4h21z41s22lnr8h2")))

(define-public crate-openai-api-rs-0.1.9 (c (n "openai-api-rs") (v "0.1.9") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.97") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "06k5rcfm3lba1vqdc6rw46kcf0hppf8k1f28y2vr8c53lhc12pxi")))

(define-public crate-openai-api-rs-0.1.10 (c (n "openai-api-rs") (v "0.1.10") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.97") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "03rlbqfp1li68rdf0nwlgsh0lpq2xlrisqbdl2a5gcb0a0bv6dkf")))

(define-public crate-openai-api-rs-0.1.11 (c (n "openai-api-rs") (v "0.1.11") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.97") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "16z6kr9jlhmz9wvpxdfk0d0ak0qwwl2cdp78iv3q663dgdacpg9b")))

(define-public crate-openai-api-rs-0.1.12 (c (n "openai-api-rs") (v "0.1.12") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.97") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0a4i4694hlpw92g3xgs9spwzkhbz4vwm9gmz6zxilvlv8ibhsgm6")))

(define-public crate-openai-api-rs-0.1.13 (c (n "openai-api-rs") (v "0.1.13") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.97") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "131wbf1hwddrqkvhrvkimbq4k271hs5841ydi5q39nm2fig8a2v8")))

(define-public crate-openai-api-rs-0.1.14 (c (n "openai-api-rs") (v "0.1.14") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.97") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "00cpj1zb31vv5zx6fdrddahlaqglrx2fj1wphnq14nr5jbzifpmf")))

(define-public crate-openai-api-rs-0.1.15 (c (n "openai-api-rs") (v "0.1.15") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.97") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0zv33fgs6g6kv9a9rykyw0kb2fd0jwn2fnzgakxb8kfr8w9y7jgc")))

(define-public crate-openai-api-rs-1.0.0 (c (n "openai-api-rs") (v "1.0.0") (d (list (d (n "minreq") (r "^2") (f (quote ("https-rustls" "json-using-serde"))) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1") (k 0)))) (h "1g9a49qjihqrblpzrd28y6fpx1k3h29cfz3myxq8gy8r4knqll5s")))

(define-public crate-openai-api-rs-1.0.1 (c (n "openai-api-rs") (v "1.0.1") (d (list (d (n "minreq") (r "^2") (f (quote ("https-rustls" "json-using-serde"))) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1") (k 0)))) (h "05qkdvblfl2bik4qyhkgg6ncx6xrgda8h7959ky1mzmynabqavgf")))

(define-public crate-openai-api-rs-1.0.2 (c (n "openai-api-rs") (v "1.0.2") (d (list (d (n "minreq") (r "^2") (f (quote ("https-rustls" "json-using-serde"))) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1") (k 0)))) (h "0b6gpmpssgmi4x25l2ryz0ssbfm5i9l5najxacxbfci21n5kks6d")))

(define-public crate-openai-api-rs-1.0.3 (c (n "openai-api-rs") (v "1.0.3") (d (list (d (n "minreq") (r "^2") (f (quote ("https-rustls" "json-using-serde"))) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1") (k 0)))) (h "04yi308hb2kmsll3kxr5gnjd84xcx3lx6dygzifklcr4bcs8fqcz")))

(define-public crate-openai-api-rs-1.0.4 (c (n "openai-api-rs") (v "1.0.4") (d (list (d (n "minreq") (r "^2") (f (quote ("https-rustls" "json-using-serde"))) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1") (k 0)))) (h "0h3sxyrgisc1rxgqbg3pawg8g6d4v14dx9zin6hxnbxdf0h0gazf")))

(define-public crate-openai-api-rs-2.0.0 (c (n "openai-api-rs") (v "2.0.0") (d (list (d (n "minreq") (r "^2") (f (quote ("https-rustls" "json-using-serde"))) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1") (k 0)))) (h "1sl9j8x3k9fy7zrk0kqmnjkqwkj6908bj7n9mw8df38qhmaj55in")))

(define-public crate-openai-api-rs-2.0.1 (c (n "openai-api-rs") (v "2.0.1") (d (list (d (n "minreq") (r "^2") (f (quote ("https-rustls" "json-using-serde"))) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1") (k 0)))) (h "1304w34ac83jhlilab8a2xij80n2z6h9i7zy46l3lgmqmbzy00ai")))

(define-public crate-openai-api-rs-2.0.2 (c (n "openai-api-rs") (v "2.0.2") (d (list (d (n "minreq") (r "^2") (f (quote ("https-rustls" "json-using-serde"))) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1") (k 0)))) (h "0cdkrgifwchrihc1arvdsjdh328v2kg8hlpvmnq49w64qv8fqw0q")))

(define-public crate-openai-api-rs-2.0.3 (c (n "openai-api-rs") (v "2.0.3") (d (list (d (n "minreq") (r "^2") (f (quote ("https-rustls" "json-using-serde"))) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1") (k 0)))) (h "1hwah4702hr7zb2f5sl3spbz27fmaf672mwv6qrpxf5i27q4xsbl")))

(define-public crate-openai-api-rs-2.0.4 (c (n "openai-api-rs") (v "2.0.4") (d (list (d (n "minreq") (r "^2") (f (quote ("https-rustls" "json-using-serde"))) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1") (k 0)))) (h "0szb62r8sd8sp9dyp25p9jid2vdj3q0zbc3ka0rlw53jm39glb71")))

(define-public crate-openai-api-rs-2.0.5 (c (n "openai-api-rs") (v "2.0.5") (d (list (d (n "minreq") (r "^2") (f (quote ("https-rustls" "json-using-serde"))) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1") (k 0)))) (h "1w54ac2by6xwv8ka2wfmrwgikbdhqbcmijja0f01xdwirldr1j9s")))

(define-public crate-openai-api-rs-2.1.0 (c (n "openai-api-rs") (v "2.1.0") (d (list (d (n "minreq") (r "^2") (f (quote ("https-rustls" "json-using-serde"))) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1") (k 0)))) (h "0a4zizb25jfj1v1abmxgw7fchnwxf97cx4jlxdhmdl1s4j19scf5")))

(define-public crate-openai-api-rs-2.1.1 (c (n "openai-api-rs") (v "2.1.1") (d (list (d (n "minreq") (r "^2") (f (quote ("https-rustls" "json-using-serde"))) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1") (k 0)))) (h "0p78b62d8yy7cqylv3ngqp7jvy328qkvfgfifrw35ackb7v9gxbs")))

(define-public crate-openai-api-rs-2.1.2 (c (n "openai-api-rs") (v "2.1.2") (d (list (d (n "minreq") (r "^2") (f (quote ("https-rustls" "json-using-serde"))) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1") (k 0)))) (h "1wz17pam9h03s6ip4zw6f7zfycrs1y7i9a6r8akpqzj7l6mkbgs6")))

(define-public crate-openai-api-rs-2.1.3 (c (n "openai-api-rs") (v "2.1.3") (d (list (d (n "minreq") (r "^2") (f (quote ("https-rustls" "json-using-serde"))) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1") (k 0)))) (h "1hfa2bs6x14xn8970fl7bagr3pqd1bbnn2d0siqajyi1q3qaibs0")))

(define-public crate-openai-api-rs-2.1.4 (c (n "openai-api-rs") (v "2.1.4") (d (list (d (n "minreq") (r "^2") (f (quote ("https-rustls" "json-using-serde"))) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1") (k 0)))) (h "1pm5kv17m9rkx4mfw6kqmymrbmi4mr9dss5b8ns45ws1nl8fkn7a")))

(define-public crate-openai-api-rs-2.1.5 (c (n "openai-api-rs") (v "2.1.5") (d (list (d (n "minreq") (r "^2") (f (quote ("https-rustls" "json-using-serde"))) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1") (k 0)))) (h "1a68h44f42gcz3c8b5zjkldnyb0mlllpqrvqnwbhy8vlw8r86h94")))

(define-public crate-openai-api-rs-2.1.6 (c (n "openai-api-rs") (v "2.1.6") (d (list (d (n "minreq") (r "^2") (f (quote ("https-rustls" "json-using-serde"))) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1") (k 0)))) (h "1p4g78n74vfrkyswi6670fni9ybr9bxsrfdwq01gk650yyzpbzw3")))

(define-public crate-openai-api-rs-2.1.7 (c (n "openai-api-rs") (v "2.1.7") (d (list (d (n "minreq") (r "^2") (f (quote ("https-rustls" "json-using-serde"))) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1") (k 0)))) (h "0jgw0sq35r2p9a77vcsghrbjca95s66is04a3pyq7m6sh1rvi432")))

(define-public crate-openai-api-rs-3.0.0 (c (n "openai-api-rs") (v "3.0.0") (d (list (d (n "minreq") (r "^2") (f (quote ("https-rustls" "json-using-serde"))) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1") (k 0)))) (h "1kjv4g0y9dgr0qg8lvxz8a8q74q1fl8pp83s1dx31kn5ra3lwhg8")))

(define-public crate-openai-api-rs-3.0.1 (c (n "openai-api-rs") (v "3.0.1") (d (list (d (n "minreq") (r "^2") (f (quote ("https-rustls" "json-using-serde"))) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1") (k 0)))) (h "11alvq7rmflnjnx9jamrrd12lr66dac2jhgdxi8j9xk2c9pj55g8")))

(define-public crate-openai-api-rs-4.0.0 (c (n "openai-api-rs") (v "4.0.0") (d (list (d (n "minreq") (r "^2") (f (quote ("https-rustls" "json-using-serde"))) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1") (k 0)))) (h "0pfpnfqw8gcv7wca56csqyrmk981y4jgp1afdxsd359hkn52ar62")))

(define-public crate-openai-api-rs-4.0.1 (c (n "openai-api-rs") (v "4.0.1") (d (list (d (n "minreq") (r "^2") (f (quote ("https-rustls" "json-using-serde" "proxy"))) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1") (k 0)))) (h "161fq8gihbklqa2p685is34m86ik6rbkfgqmyxmys0hddmkg7dfm")))

(define-public crate-openai-api-rs-4.0.2 (c (n "openai-api-rs") (v "4.0.2") (d (list (d (n "minreq") (r "^2") (f (quote ("https-rustls" "json-using-serde" "proxy"))) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1") (k 0)))) (h "0qa7jirkc6x3d9a1s2m5m9kl62iq9q9mklb6lax252lvymml4ihr")))

(define-public crate-openai-api-rs-4.0.3 (c (n "openai-api-rs") (v "4.0.3") (d (list (d (n "minreq") (r "^2") (f (quote ("https-rustls" "json-using-serde" "proxy"))) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1") (k 0)))) (h "1rdv403nw8r52y04r36srpa8yvv1flixky01nnr4zsd2hy4vpcyj")))

(define-public crate-openai-api-rs-4.0.4 (c (n "openai-api-rs") (v "4.0.4") (d (list (d (n "minreq") (r "^2") (f (quote ("https-rustls" "json-using-serde" "proxy"))) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1") (k 0)))) (h "0f26cfqkh4ylkzvv1rkhza94w8z7zqhxmqcda9vla39bl182nida")))

(define-public crate-openai-api-rs-4.0.5 (c (n "openai-api-rs") (v "4.0.5") (d (list (d (n "minreq") (r "^2") (f (quote ("https-rustls" "json-using-serde" "proxy"))) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1") (k 0)))) (h "0vh7gyaxvadvn5rl32dpaxpp3nns85700qi3v9dsmp04fh19cx6a")))

(define-public crate-openai-api-rs-4.0.6 (c (n "openai-api-rs") (v "4.0.6") (d (list (d (n "minreq") (r "^2") (f (quote ("https-rustls" "json-using-serde" "proxy"))) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1") (k 0)))) (h "1xlj455bwv01rs074kb0jq13gn6pw179k5k4qsd893yfiy44l6fb")))

(define-public crate-openai-api-rs-4.0.7 (c (n "openai-api-rs") (v "4.0.7") (d (list (d (n "minreq") (r "^2") (f (quote ("https-rustls" "json-using-serde" "proxy"))) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1") (k 0)))) (h "07wa07rn88g5m87crdba2y0lj25ar0d4s2hsakqydxrj340prl4g")))

(define-public crate-openai-api-rs-4.0.8 (c (n "openai-api-rs") (v "4.0.8") (d (list (d (n "minreq") (r "^2") (f (quote ("https-rustls" "json-using-serde" "proxy"))) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1") (k 0)))) (h "0913l5ab37qvsrhaax90qp1sp05wifkj3awmkff1b2asq8n0jpky")))

(define-public crate-openai-api-rs-4.0.9 (c (n "openai-api-rs") (v "4.0.9") (d (list (d (n "minreq") (r "^2") (f (quote ("https-rustls" "json-using-serde" "proxy"))) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1") (k 0)))) (h "16i2sf7fdacp6i5zav0av9ffkc7c5qnwj77679b1gnsvzrvyrl59")))

