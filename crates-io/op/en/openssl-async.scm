(define-module (crates-io op en openssl-async) #:use-module (crates-io))

(define-public crate-openssl-async-0.1.0 (c (n "openssl-async") (v "0.1.0") (d (list (d (n "futures-preview") (r "^0.3.0-alpha.16") (d #t) (k 0)) (d (n "openssl") (r "^0.10.23") (d #t) (k 0)) (d (n "runtime") (r "^0.3.0-alpha.4") (d #t) (k 2)))) (h "08kza6qgigi1r17a84z4l3ig5l7lbdfdmsfbzbbpcivrkhibf33r") (y #t)))

(define-public crate-openssl-async-0.1.1 (c (n "openssl-async") (v "0.1.1") (d (list (d (n "futures-preview") (r "^0.3.0-alpha.16") (d #t) (k 0)) (d (n "openssl") (r "^0.10.23") (d #t) (k 0)) (d (n "runtime") (r "^0.3.0-alpha.4") (d #t) (k 2)))) (h "1gawc7vjlbdn5bh5p3m1ha3qc7yqpyvjrkbdvl39sldra3aq8vp3") (y #t)))

(define-public crate-openssl-async-0.1.2 (c (n "openssl-async") (v "0.1.2") (d (list (d (n "futures-preview") (r "^0.3.0-alpha.16") (d #t) (k 0)) (d (n "openssl") (r "^0.10.23") (d #t) (k 0)) (d (n "runtime") (r "^0.3.0-alpha.4") (d #t) (k 2)))) (h "0ygqcy1p3a4lmrp7n1nykssf785ga38az7bm2i9hklpq73f1ydxb") (y #t)))

(define-public crate-openssl-async-0.3.0-alpha.0 (c (n "openssl-async") (v "0.3.0-alpha.0") (d (list (d (n "async-stdio") (r "^0.3.0-alpha.2") (d #t) (k 0)) (d (n "futures-preview") (r "^0.3.0-alpha.16") (d #t) (k 0)) (d (n "openssl") (r "^0.10.23") (d #t) (k 0)) (d (n "runtime") (r "^0.3.0-alpha.4") (d #t) (k 2)))) (h "1snbhpz3w3b8gmpc8axpbd4ribx3al211dynhpxsclnqwbx5630i")))

(define-public crate-openssl-async-0.3.0-alpha.1 (c (n "openssl-async") (v "0.3.0-alpha.1") (d (list (d (n "async-stdio") (r "^0.3.0-alpha.2") (d #t) (k 0)) (d (n "futures-preview") (r "^0.3.0-alpha.16") (d #t) (k 0)) (d (n "openssl") (r "^0.10.23") (d #t) (k 0)) (d (n "runtime") (r "^0.3.0-alpha.4") (d #t) (k 2)))) (h "1cpvcny95n71wyh33qix7jmp0a3ycy4vn0291iy5pw0ps3m2h291")))

(define-public crate-openssl-async-0.3.0-alpha.2 (c (n "openssl-async") (v "0.3.0-alpha.2") (d (list (d (n "async-stdio") (r "^0.3.0-alpha.3") (d #t) (k 0)) (d (n "futures-preview") (r "^0.3.0-alpha.16") (d #t) (k 0)) (d (n "openssl") (r "^0.10.23") (d #t) (k 0)) (d (n "runtime") (r "^0.3.0-alpha.5") (d #t) (k 2)))) (h "0hs9i5bw063vf1xwlhih1klzkjqlcmwrpb7kh9xlykpi2nbzwjcx")))

(define-public crate-openssl-async-0.3.0-alpha.3 (c (n "openssl-async") (v "0.3.0-alpha.3") (d (list (d (n "async-stdio") (r "^0.3.0-alpha.3") (d #t) (k 0)) (d (n "futures-preview") (r "^0.3.0-alpha.16") (d #t) (k 0)) (d (n "openssl") (r "^0.10.23") (d #t) (k 0)) (d (n "runtime") (r "^0.3.0-alpha.5") (d #t) (k 2)))) (h "0h7pgrdd1vcs92x3cs4nhq8fxisniygndsaizxj23fym1fafdhjz")))

(define-public crate-openssl-async-0.3.0-alpha.4 (c (n "openssl-async") (v "0.3.0-alpha.4") (d (list (d (n "async-stdio") (r "^0.3.0-alpha.3") (d #t) (k 0)) (d (n "futures-preview") (r "^0.3.0-alpha.17") (d #t) (k 0)) (d (n "openssl") (r "^0.10.23") (d #t) (k 0)) (d (n "runtime") (r "^0.3.0-alpha.6") (d #t) (k 2)))) (h "1rgg2g8qbpjgkpcqgbl07fpkkphw4prj5mi0pykvz0n9svcg76ra")))

(define-public crate-openssl-async-0.3.0-alpha.5 (c (n "openssl-async") (v "0.3.0-alpha.5") (d (list (d (n "async-stdio") (r "^0.3.0-alpha.4") (d #t) (k 0)) (d (n "futures-preview") (r "^0.3.0-alpha.19") (d #t) (k 0)) (d (n "openssl") (r "^0.10.25") (d #t) (k 0)) (d (n "runtime") (r "^0.3.0-alpha.7") (d #t) (k 2)) (d (n "tokio-io") (r "^0.2.0-alpha.6") (o #t) (d #t) (k 0)))) (h "0kp9ij8hhjhigh8wrragqij6n1x6kdrwkjpragwlz0flpa1wyfdh") (f (quote (("tokio" "tokio-io") ("default"))))))

