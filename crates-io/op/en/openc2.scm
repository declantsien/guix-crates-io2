(define-module (crates-io op en openc2) #:use-module (crates-io))

(define-public crate-openc2-0.1.0 (c (n "openc2") (v "0.1.0") (d (list (d (n "from_variants") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)))) (h "1nmyasqp51dbmkb0cdn4da8038dq57zdnvzdkr2qd3imkary5nl0")))

