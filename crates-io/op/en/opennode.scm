(define-module (crates-io op en opennode) #:use-module (crates-io))

(define-public crate-opennode-0.1.0 (c (n "opennode") (v "0.1.0") (d (list (d (n "actix-http") (r "^0.2.5") (d #t) (k 0)) (d (n "actix-rt") (r "^0.2.2") (d #t) (k 2)) (d (n "awc") (r "^0.2.1") (f (quote ("ssl"))) (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 2)) (d (n "futures") (r "^0.1.27") (d #t) (k 0)) (d (n "serde") (r "^1.0.93") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)))) (h "1ywnzqh6n4m314m2c6sz8xzh646jv70986ashb9l6iqxwaqzdjgx")))

(define-public crate-opennode-1.0.0 (c (n "opennode") (v "1.0.0") (d (list (d (n "serde") (r "^1.0.93") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)))) (h "1608xxpans6a0zmqihbk1rxywj6ssi9fqzrb7cm8f4x1q0x6q71g")))

