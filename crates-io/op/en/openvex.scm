(define-module (crates-io op en openvex) #:use-module (crates-io))

(define-public crate-openvex-0.1.0 (c (n "openvex") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.23") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "09fsjhnlbhqkgyblq2mhcvfmi040d0r6jm6zmhjlvaks2gw3w4xj")))

(define-public crate-openvex-0.1.1 (c (n "openvex") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.23") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1ss2ci8si6ywkq96j5gz0ibidfqmkqb96a3dkch5p9p02qzscncm")))

