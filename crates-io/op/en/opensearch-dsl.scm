(define-module (crates-io op en opensearch-dsl) #:use-module (crates-io))

(define-public crate-opensearch-dsl-0.1.0 (c (n "opensearch-dsl") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.34") (f (quote ("std" "serde"))) (k 0)) (d (n "num-traits") (r "^0.2.18") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1.0.113") (f (quote ("raw_value"))) (d #t) (k 0)))) (h "1xv67xwfzp1qivw4v19glf94nn815h8cb89fzw5mjy0b2xls66cx")))

