(define-module (crates-io op en open_read_later) #:use-module (crates-io))

(define-public crate-open_read_later-1.0.0 (c (n "open_read_later") (v "1.0.0") (d (list (d (n "clap") (r "^2.26.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "0z4kv30wwkr1z343x3aclghzmcb6zyzjcs16ac7mpnp86p8qpvxh")))

(define-public crate-open_read_later-1.0.1 (c (n "open_read_later") (v "1.0.1") (d (list (d (n "clap") (r "^2.26.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "1pf0s60f9zdsd80gyiywb450c9hry384lwlxfisr5k6qcvjbd54y")))

(define-public crate-open_read_later-1.1.0 (c (n "open_read_later") (v "1.1.0") (d (list (d (n "clap") (r "^2.26.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1yq8m71yfd76q8273gcphj4aajp5vrpj9p1xw1pvh63ar1gs4p3a")))

(define-public crate-open_read_later-1.1.1 (c (n "open_read_later") (v "1.1.1") (d (list (d (n "clap") (r "^2.26.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "03hindwk031gyl89p1fscxfdcfvygzks7qds9b4mx8284h6ypi62")))

