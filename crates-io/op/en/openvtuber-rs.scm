(define-module (crates-io op en openvtuber-rs) #:use-module (crates-io))

(define-public crate-openvtuber-rs-0.1.3 (c (n "openvtuber-rs") (v "0.1.3") (d (list (d (n "image") (r "^0.23.14") (k 0)) (d (n "ndarray") (r "^0.15.1") (d #t) (k 0)) (d (n "opencv") (r "^0.52.0") (f (quote ("clang-runtime"))) (d #t) (k 0)) (d (n "tflite") (r "^0.9.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "1wvcy663k6gr6yhx61vafskm59jbiwwfp7r5ayw4974ynz37nxvb") (y #t)))

(define-public crate-openvtuber-rs-0.1.4 (c (n "openvtuber-rs") (v "0.1.4") (d (list (d (n "image") (r "^0.23.14") (k 0)) (d (n "ndarray") (r "^0.15.1") (d #t) (k 0)) (d (n "opencv") (r "^0.52.0") (f (quote ("clang-runtime"))) (d #t) (k 0)) (d (n "tflite") (r "^0.9.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "0yfsrr36d1avchpighv999w76vipzixfpkx84z40ppyfvm76cij3") (f (quote (("docs" "opencv/docs-only")))) (y #t)))

