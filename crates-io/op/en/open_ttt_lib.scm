(define-module (crates-io op en open_ttt_lib) #:use-module (crates-io))

(define-public crate-open_ttt_lib-0.1.0 (c (n "open_ttt_lib") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "doc-comment") (r "^0.3.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)))) (h "0xcshqqjcmf9j1qxilh2ligw6bipz0n71jb6bg1d4blapwa991xy")))

(define-public crate-open_ttt_lib-0.1.1 (c (n "open_ttt_lib") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "doc-comment") (r "^0.3.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)))) (h "0qfhzz3i2ly2frsrrn4j7x7x7g1xfrima3fn4x3lp1g7ir9c710c")))

(define-public crate-open_ttt_lib-0.1.2 (c (n "open_ttt_lib") (v "0.1.2") (d (list (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "doc-comment") (r "^0.3.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)))) (h "1cccfslfpp2m8dlcq444800p7w5bklvp2rd89v4jy0dqyhh2bq0g")))

(define-public crate-open_ttt_lib-0.2.0 (c (n "open_ttt_lib") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "doc-comment") (r "^0.3.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "0dwq51j3kwpnavgpi7bmnidcg3mc5lym53hjcbx8dwxil0fxwjdw")))

(define-public crate-open_ttt_lib-0.2.1 (c (n "open_ttt_lib") (v "0.2.1") (d (list (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "doc-comment") (r "^0.3.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "0ipk5fbfc6l9cn4dsv4gaj2bg3k8zq4i9jihddpgz5dxs0c5w8ka")))

(define-public crate-open_ttt_lib-0.2.2 (c (n "open_ttt_lib") (v "0.2.2") (d (list (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "doc-comment") (r "^0.3.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "1kcscpzgl1nnbp54461axan1ppaa82xwqg4j2jw4kbsj91ybhz3i")))

