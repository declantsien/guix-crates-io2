(define-module (crates-io op en opensles) #:use-module (crates-io))

(define-public crate-opensles-0.0.1 (c (n "opensles") (v "0.0.1") (d (list (d (n "bindgen") (r "^0.53.2") (d #t) (k 1)))) (h "0zaqhf8s1aw1sfpi6jmj0rn6kgq4571k1kg0hdzgc2sdf804gy71") (l "OpenSLES")))

