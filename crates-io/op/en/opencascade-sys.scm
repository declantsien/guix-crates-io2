(define-module (crates-io op en opencascade-sys) #:use-module (crates-io))

(define-public crate-opencascade-sys-0.1.0 (c (n "opencascade-sys") (v "0.1.0") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "cxx") (r "^1") (d #t) (k 0)) (d (n "cxx-build") (r "^1") (d #t) (k 1)))) (h "1d5qcm2knps8bi04xxy51xhjc2fn0wp7bpbia0jqihdskjjzr7ij")))

(define-public crate-opencascade-sys-0.2.0 (c (n "opencascade-sys") (v "0.2.0") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "cxx") (r "^1") (d #t) (k 0)) (d (n "cxx-build") (r "^1") (d #t) (k 1)) (d (n "occt-sys") (r "^0.2") (d #t) (k 1)))) (h "177xjszc9rg4y6ajp63rbsd7k9r6dzng0qf0g2qxyh4nfxlkndp5")))

