(define-module (crates-io op en open-oak-game-of-life) #:use-module (crates-io))

(define-public crate-open-oak-game-of-life-0.1.0 (c (n "open-oak-game-of-life") (v "0.1.0") (d (list (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "conlife") (r "^0.1") (d #t) (k 0)) (d (n "open-oak") (r "^0.1") (d #t) (k 0)))) (h "10vl1byvm1nzvswh7yjx17bhv83hkh7q0w64zn7zj9bxnvizqmc7")))

(define-public crate-open-oak-game-of-life-0.1.1 (c (n "open-oak-game-of-life") (v "0.1.1") (d (list (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "conlife") (r "^0.1") (d #t) (k 0)) (d (n "open-oak") (r "^0.1") (d #t) (k 0)))) (h "1w9q8mgn3gpw8v4a1gr2f5lnmc1shxa7lsnf63jiynmqvbqdsbk9")))

(define-public crate-open-oak-game-of-life-0.1.2 (c (n "open-oak-game-of-life") (v "0.1.2") (d (list (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "conlife") (r "^0.1") (d #t) (k 0)) (d (n "open-oak") (r "^0.1") (d #t) (k 0)))) (h "12al0mngrna0kdn378laphgipcj71n73p4hwvhn8dnfxq12ab067")))

