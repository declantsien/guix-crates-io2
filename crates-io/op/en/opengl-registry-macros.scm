(define-module (crates-io op en opengl-registry-macros) #:use-module (crates-io))

(define-public crate-opengl-registry-macros-0.1.0 (c (n "opengl-registry-macros") (v "0.1.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.0") (d #t) (k 0)) (d (n "opengl-registry") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "quote") (r "^1.0.23") (d #t) (k 2)))) (h "154cvzx22xjrm5j8a04rlgxpdfb2nsllzq24m0ahf0bq3i4bhjg8")))

