(define-module (crates-io op en openfx-sys) #:use-module (crates-io))

(define-public crate-openfx-sys-0.1.0 (c (n "openfx-sys") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.139") (d #t) (k 0)) (d (n "bindgen") (r "^0.64.0") (d #t) (k 1)))) (h "0ycng6xli6hnx30n0ldgcsaj9ada9y1rfcv85p721m38qklzjcwc")))

