(define-module (crates-io op en openssl-sys-extras) #:use-module (crates-io))

(define-public crate-openssl-sys-extras-0.7.0 (c (n "openssl-sys-extras") (v "0.7.0") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "openssl-sys") (r "^0.7") (d #t) (k 0)))) (h "0cxqzxdl0fz7ryxlkzy0mqikzry8qa2nf2k2dyn995lc3xcc2lzf") (f (quote (("ecdh_auto"))))))

(define-public crate-openssl-sys-extras-0.7.1 (c (n "openssl-sys-extras") (v "0.7.1") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "openssl-sys") (r "^0.7.1") (d #t) (k 0)))) (h "032c93q8i7pfxqbywnp3yn8wvqkq2ghmbmjkwri91pf5ip9r2x4j") (f (quote (("ecdh_auto"))))))

(define-public crate-openssl-sys-extras-0.7.2 (c (n "openssl-sys-extras") (v "0.7.2") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "openssl-sys") (r "^0.7.1") (d #t) (k 0)))) (h "06sx64d81xhqva4zzbpbdk771z0p9avyxgdaig65nw2by6qmns2h") (f (quote (("ecdh_auto"))))))

(define-public crate-openssl-sys-extras-0.7.3 (c (n "openssl-sys-extras") (v "0.7.3") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "openssl-sys") (r "^0.7.3") (d #t) (k 0)))) (h "03hwayw41kbchrkzxwlskaiyw96dlgswdm3dlj9knzxqnydxqc53") (f (quote (("ecdh_auto"))))))

(define-public crate-openssl-sys-extras-0.7.4 (c (n "openssl-sys-extras") (v "0.7.4") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "openssl-sys") (r "^0.7.4") (d #t) (k 0)))) (h "063y3awr5h6jcxj92pcvykjdavjijk3k6hdfi1l0f31vh90phvpn") (f (quote (("ecdh_auto"))))))

(define-public crate-openssl-sys-extras-0.7.5 (c (n "openssl-sys-extras") (v "0.7.5") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "openssl-sys") (r "^0.7.5") (d #t) (k 0)))) (h "09xj3n1dlllb5v7ym5yn8pf0n7izvrdimj4b2rbhiyb0ykgiz2lk") (f (quote (("ecdh_auto"))))))

(define-public crate-openssl-sys-extras-0.7.6 (c (n "openssl-sys-extras") (v "0.7.6") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "openssl-sys") (r "^0.7.6") (d #t) (k 0)))) (h "1shnrzi00sggfwkjjlgnrdszkanlh526sw1g766ym3vdzawvvjvg") (f (quote (("ecdh_auto"))))))

(define-public crate-openssl-sys-extras-0.7.7 (c (n "openssl-sys-extras") (v "0.7.7") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "openssl-sys") (r "^0.7.7") (d #t) (k 0)))) (h "0sxv8hn2fjgrvjx1my05yrdl496i0xddf5m2xhy9fc3vg1xfa741") (f (quote (("ecdh_auto"))))))

(define-public crate-openssl-sys-extras-0.7.8 (c (n "openssl-sys-extras") (v "0.7.8") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "openssl-sys") (r "^0.7.8") (d #t) (k 0)))) (h "1a6w7i271j3gja5fp4nx7pxy04j72psmf351jnxa4vaihgk5z4dv") (f (quote (("ecdh_auto"))))))

(define-public crate-openssl-sys-extras-0.7.9 (c (n "openssl-sys-extras") (v "0.7.9") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "openssl-sys") (r "^0.7.9") (d #t) (k 0)))) (h "1rsyk8jyr8xzbhid2mjkay2fs2ciihw5c5k9836vp8xp2xs45zz4") (f (quote (("ecdh_auto"))))))

(define-public crate-openssl-sys-extras-0.7.10 (c (n "openssl-sys-extras") (v "0.7.10") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "openssl-sys") (r "^0.7.10") (d #t) (k 0)))) (h "0gy6kjkspzqkb8bc5wf8gnl0920iz0ryis1871ykwlarnj49jpgm") (f (quote (("ecdh_auto"))))))

(define-public crate-openssl-sys-extras-0.7.11 (c (n "openssl-sys-extras") (v "0.7.11") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "openssl-sys") (r "^0.7.11") (d #t) (k 0)))) (h "19jcmpvsczx9j04cqad87qvxdrdg9n1ld3y29599ixyflxbhzv07") (f (quote (("ecdh_auto"))))))

(define-public crate-openssl-sys-extras-0.7.12 (c (n "openssl-sys-extras") (v "0.7.12") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "openssl-sys") (r "^0.7.12") (d #t) (k 0)))) (h "12z2gqvqc0dfwhw8h0fll3c4g21k5lmw4nyv31828s7lk7p6y52g") (f (quote (("ecdh_auto"))))))

(define-public crate-openssl-sys-extras-0.7.13 (c (n "openssl-sys-extras") (v "0.7.13") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "openssl-sys") (r "^0.7.13") (d #t) (k 0)))) (h "0dhgplhb1wk97rip1q0aiv62vndza9kmvz1h6m6snccfv8kq10q1") (f (quote (("ecdh_auto"))))))

(define-public crate-openssl-sys-extras-0.7.14 (c (n "openssl-sys-extras") (v "0.7.14") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "openssl-sys") (r "^0.7.14") (d #t) (k 0)))) (h "1ymrmfnknyjji74fflbnnq9r5ihx25h0vgs5y203vl6klzdy3i8i") (f (quote (("ecdh_auto"))))))

