(define-module (crates-io op en openvpn-plugin) #:use-module (crates-io))

(define-public crate-openvpn-plugin-0.1.0 (c (n "openvpn-plugin") (v "0.1.0") (d (list (d (n "log") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0jhv02r4a94bxlnv3bhh3hsvh82ij6zcrj9zfdv7w552iqbk6bf3") (f (quote (("serialize" "serde" "serde_derive") ("default"))))))

(define-public crate-openvpn-plugin-0.2.0 (c (n "openvpn-plugin") (v "0.2.0") (d (list (d (n "log") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0fzv6xpm68m6jz1fspa0jz56ly990xrj5p974r0rjix254i5mh4r") (f (quote (("serialize" "serde" "serde_derive") ("default"))))))

(define-public crate-openvpn-plugin-0.3.0 (c (n "openvpn-plugin") (v "0.3.0") (d (list (d (n "log") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1s67jzmsv7n1h1fpnnj5wpm0y6hfz6pgcm6643x4i1ky0zx987zh")))

(define-public crate-openvpn-plugin-0.4.0 (c (n "openvpn-plugin") (v "0.4.0") (d (list (d (n "derive-try-from-primitive") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0h0yc10w4hsr543asnxrjif7zwhmy3mrcyvklqk6a5hxqjwy4czl") (f (quote (("auth-failed-event"))))))

(define-public crate-openvpn-plugin-0.4.1 (c (n "openvpn-plugin") (v "0.4.1") (d (list (d (n "derive-try-from-primitive") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1hr6w7s8qls2fvz2c5jsilyhxdsigsnax1hyscqw6kcanjs3pmj5") (f (quote (("auth-failed-event"))))))

(define-public crate-openvpn-plugin-0.4.2 (c (n "openvpn-plugin") (v "0.4.2") (d (list (d (n "derive-try-from-primitive") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "146z5xm2vvh4sqcc4zgjxnfvb6bgjxybhi56c3yqvvs6gvm9ccv9") (f (quote (("auth-failed-event"))))))

