(define-module (crates-io op en openmw-cfg) #:use-module (crates-io))

(define-public crate-openmw-cfg-0.1.0 (c (n "openmw-cfg") (v "0.1.0") (d (list (d (n "cfg-if") (r "^0.1.10") (d #t) (k 0)) (d (n "dirs") (r "^2.0") (d #t) (k 0)) (d (n "rust-ini") (r "^0.15") (d #t) (k 0)))) (h "1kxypbbzsq5ish2fp7mp6jp7hba3jhm6y0mql2y3nyfsqf582g4h")))

(define-public crate-openmw-cfg-0.1.1 (c (n "openmw-cfg") (v "0.1.1") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "dirs") (r "^3.0") (d #t) (k 0)) (d (n "rust-ini") (r "^0.16") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1m2q8a22s0lv3jin4ckmdw4qm6cdhlri2a237mhhrc2dyiln8mmg")))

(define-public crate-openmw-cfg-0.1.2 (c (n "openmw-cfg") (v "0.1.2") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "dirs") (r "^3.0") (d #t) (k 0)) (d (n "rust-ini") (r "^0.17") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "109y9i9shwrmgiw4kq2nzhchi8rnaayjil1wqb40fll7jqdlcf3d")))

(define-public crate-openmw-cfg-0.1.3 (c (n "openmw-cfg") (v "0.1.3") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "dirs") (r "^3.0") (d #t) (k 0)) (d (n "rust-ini") (r "^0.17") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0m2412y2sfks8mhw9xxm4qaiwzcak2zhbpl3mfd2a5s2xqvaskma")))

(define-public crate-openmw-cfg-0.2.0 (c (n "openmw-cfg") (v "0.2.0") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "dirs") (r ">=3.0, <5.0") (d #t) (k 0)) (d (n "rust-ini") (r ">=0.17, <0.19") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0dm70whsk8zxq9bz94znysxdpbxs1mv3833wpzmzhk477wn8k6lr")))

(define-public crate-openmw-cfg-0.3.0 (c (n "openmw-cfg") (v "0.3.0") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "dirs") (r ">=3.0, <5.0") (d #t) (k 0)) (d (n "rust-ini") (r ">=0.17, <0.19") (d #t) (k 0)) (d (n "shellexpand") (r "^2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "07k16grccpk09s1s0w8ypwqmqgn3db030sh85vzj7limcsm4dvhz")))

(define-public crate-openmw-cfg-0.4.0 (c (n "openmw-cfg") (v "0.4.0") (d (list (d (n "bsatoollib") (r "^0.1") (d #t) (k 0)) (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "dirs") (r ">=3.0, <5.0") (d #t) (k 0)) (d (n "rust-ini") (r ">=0.17, <0.19") (d #t) (k 0)) (d (n "shellexpand") (r "^2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0m8x6lryga77v9jmlmxay8vjw070z1nmp791gyldqswzyzl9hjq5")))

(define-public crate-openmw-cfg-0.5.0 (c (n "openmw-cfg") (v "0.5.0") (d (list (d (n "bsatoollib") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "dirs") (r ">=3.0, <6.0") (d #t) (k 0)) (d (n "rayon") (r "^1") (d #t) (k 0)) (d (n "rust-ini") (r ">=0.17, <0.20") (d #t) (k 0)) (d (n "shellexpand") (r ">=2.1, <4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1irldw0wq0xlzri3ka921p4lmhbw4gicswf8samv7sn8ydr5pwxp") (f (quote (("default" "bsa") ("bsa" "bsatoollib"))))))

(define-public crate-openmw-cfg-0.5.1 (c (n "openmw-cfg") (v "0.5.1") (d (list (d (n "bsatoollib") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "dirs") (r ">=3.0, <6.0") (d #t) (k 0)) (d (n "rayon") (r "^1") (d #t) (k 0)) (d (n "rust-ini") (r ">=0.17, <0.21") (d #t) (k 0)) (d (n "shellexpand") (r ">=2.1, <4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1kywvhb834rdgmvxn5ksfdi5djkdn0kf5y6gh62j78r9z94wx6fl") (f (quote (("default" "bsa") ("bsa" "bsatoollib"))))))

