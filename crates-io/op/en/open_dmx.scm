(define-module (crates-io op en open_dmx) #:use-module (crates-io))

(define-public crate-open_dmx-0.1.0 (c (n "open_dmx") (v "0.1.0") (d (list (d (n "serial") (r "^0.4.0") (d #t) (k 0)) (d (n "thread-priority") (r "^0.10.0") (o #t) (d #t) (k 0)))) (h "1cjhwm4j6ddmy3bmjww75dzhrprc5ynpfgqbqf6h2gpm1mriahxj") (f (quote (("default" "thread_priority")))) (s 2) (e (quote (("thread_priority" "dep:thread-priority"))))))

(define-public crate-open_dmx-0.1.1 (c (n "open_dmx") (v "0.1.1") (d (list (d (n "serial") (r "^0.4.0") (d #t) (k 0)) (d (n "thread-priority") (r "^0.10.0") (o #t) (d #t) (k 0)))) (h "00ydyqwaivvr8jx51jgwa9sckqjrgckcfdqf39vpvakmfnk6jz8v") (f (quote (("default" "thread_priority")))) (s 2) (e (quote (("thread_priority" "dep:thread-priority"))))))

(define-public crate-open_dmx-0.1.2 (c (n "open_dmx") (v "0.1.2") (d (list (d (n "serial") (r "^0.4.0") (d #t) (k 0)) (d (n "thread-priority") (r "^0.10.0") (o #t) (d #t) (k 0)))) (h "12gxdgh4864qgnl7787w0s44s2w7y229cy5bn6byzjv92mbcirlc") (f (quote (("default" "thread_priority")))) (s 2) (e (quote (("thread_priority" "dep:thread-priority"))))))

(define-public crate-open_dmx-0.1.3 (c (n "open_dmx") (v "0.1.3") (d (list (d (n "serial") (r "^0.4.0") (d #t) (k 0)) (d (n "thread-priority") (r "^0.13") (o #t) (d #t) (k 0)))) (h "1si3as734wsjjzgi7hix3m4g8hyrmil0aji6br434qmqc5clbchs") (f (quote (("default" "thread_priority")))) (s 2) (e (quote (("thread_priority" "dep:thread-priority"))))))

(define-public crate-open_dmx-1.0.0 (c (n "open_dmx") (v "1.0.0") (d (list (d (n "serial") (r "^0.4.0") (d #t) (k 0)) (d (n "thread-priority") (r "^0.13") (o #t) (d #t) (k 0)))) (h "1imi13c1224kci4syx43px0hyjycz77c96s8ilrnhswq5i4i3wjc") (f (quote (("default" "thread_priority")))) (s 2) (e (quote (("thread_priority" "dep:thread-priority"))))))

(define-public crate-open_dmx-1.0.1 (c (n "open_dmx") (v "1.0.1") (d (list (d (n "serial") (r "^0.4.0") (d #t) (k 0)) (d (n "thread-priority") (r "^0.13") (o #t) (d #t) (k 0)))) (h "1yqm1rp92wvk2df5p45z7mlj2pxjik1c5rv11q5ij9zp5n0803bl") (f (quote (("default" "thread_priority")))) (s 2) (e (quote (("thread_priority" "dep:thread-priority"))))))

(define-public crate-open_dmx-1.1.1 (c (n "open_dmx") (v "1.1.1") (d (list (d (n "serialport") (r "^4.3") (d #t) (k 0)) (d (n "thread-priority") (r "^0.15") (o #t) (d #t) (k 0)))) (h "14ws1dmfnkqg33xxjhnbnh9vplzi9q3afnhppdny9nqza13i79dp") (f (quote (("default" "thread_priority")))) (s 2) (e (quote (("thread_priority" "dep:thread-priority"))))))

