(define-module (crates-io op en openassets) #:use-module (crates-io))

(define-public crate-openassets-0.1.0 (c (n "openassets") (v "0.1.0") (d (list (d (n "bitcoin") (r "^0.18.0") (d #t) (k 0)) (d (n "bitcoin_hashes") (r "^0.3") (d #t) (k 0)) (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "hex") (r "= 0.3.2") (d #t) (k 0)) (d (n "leb128") (r "^0.2.3") (d #t) (k 0)))) (h "1sfwwkdgyfkwp0casw3i43nfajz545mwynbx08di5vmphssjj1kc")))

