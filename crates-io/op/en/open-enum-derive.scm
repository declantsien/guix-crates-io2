(define-module (crates-io op en open-enum-derive) #:use-module (crates-io))

(define-public crate-open-enum-derive-0.1.0 (c (n "open-enum-derive") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1k1vigy8n5chjk7ncrzwbvgp4rz1id4kyvq5zk97yx2q4057pry6")))

(define-public crate-open-enum-derive-0.2.0 (c (n "open-enum-derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1dzzvsbmvydrycp39386bhbkrzyj5qsxjkkimfvbfsidzyh50zaq") (f (quote (("repr_c"))))))

(define-public crate-open-enum-derive-0.3.0 (c (n "open-enum-derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "125vh04g7mav3b35729w3fkky6a47x3n7fdk78bzgv4zwm1y8jl9") (f (quote (("repr_c"))))))

(define-public crate-open-enum-derive-0.4.0 (c (n "open-enum-derive") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0xcs59vvv9byl0nqq7v9gp71f4zsbbxnjkl2vazp407a4qls0bsq") (f (quote (("repr_c"))))))

(define-public crate-open-enum-derive-0.4.1 (c (n "open-enum-derive") (v "0.4.1") (d (list (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0xp71m2ll7g4iz6i7h66kr1m0my1ld1fq7mmnawcln808dhw5lgy") (f (quote (("repr_c"))))))

(define-public crate-open-enum-derive-0.5.0 (c (n "open-enum-derive") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0zzdsbizhr29xrv3lv1qwlfwi4jv03lmg192m1fw6rcc1knxik9v") (f (quote (("repr_c"))))))

(define-public crate-open-enum-derive-0.5.1 (c (n "open-enum-derive") (v "0.5.1") (d (list (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1q72lad17qvk0c26vnrs3xswkcmqnc4h6m0zqfkl6wqww1bs2l9g") (f (quote (("repr_c"))))))

