(define-module (crates-io op en open_secrets) #:use-module (crates-io))

(define-public crate-open_secrets-0.1.0 (c (n "open_secrets") (v "0.1.0") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.6") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.71") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "tokio") (r "^1.8") (f (quote ("full"))) (d #t) (k 0)))) (h "18n5cjdr3wcqb8n80bc7blxxj6xk4nclv2nzg4lmmj4l7d3gh4jg") (y #t)))

(define-public crate-open_secrets-0.1.1 (c (n "open_secrets") (v "0.1.1") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.6") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.71") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "tokio") (r "^1.8") (f (quote ("full"))) (d #t) (k 0)))) (h "0rmfqvx2vksr84gsw1pn9zvakjs4j7k9m9dlwc6j9lhwxqf1m09p") (y #t)))

(define-public crate-open_secrets-0.1.2 (c (n "open_secrets") (v "0.1.2") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.6") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.71") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "tokio") (r "^1.8") (f (quote ("full"))) (d #t) (k 0)))) (h "16dcclalgh60nl9d69yvi9srcqsrbvx9ygbfvd5srmw6l8db5bng")))

(define-public crate-open_secrets-0.1.3 (c (n "open_secrets") (v "0.1.3") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.6") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.71") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "tokio") (r "^1.8") (f (quote ("full"))) (d #t) (k 0)))) (h "1cdxjm4k2rniayj8miywkk7cjqbacqhprajb7ywq5m74nqg7z2zq")))

(define-public crate-open_secrets-0.1.4 (c (n "open_secrets") (v "0.1.4") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.6") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.71") (d #t) (k 0)) (d (n "tokio") (r "^1.21.1") (f (quote ("full"))) (d #t) (k 0)))) (h "03p9vlk88n218hrhflsnmi6hsrfc7hgi76m6v2bd8k9kg15pr10r")))

