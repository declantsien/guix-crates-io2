(define-module (crates-io op en open-enum) #:use-module (crates-io))

(define-public crate-open-enum-0.1.0 (c (n "open-enum") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "open-enum-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "08lv4cqyyw8rwzng8x02khh10cxjlg6jydd8g4pmf41nj7cla54w")))

(define-public crate-open-enum-0.2.0 (c (n "open-enum") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "open-enum-derive") (r "^0.2.0") (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "12xpk60bwwfcs4q4d3vsllhw43aiiw2ankkgrkl68kkzy342yl9q") (f (quote (("std" "open-enum-derive/repr_c") ("libc_" "libc" "open-enum-derive/repr_c"))))))

(define-public crate-open-enum-0.3.0 (c (n "open-enum") (v "0.3.0") (d (list (d (n "libc") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "open-enum-derive") (r "^0.3.0") (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "07lirnywmcrhdh3cgff6c93qw5x5kbpfay8sh36cfkpqkhcz21wq") (f (quote (("std" "open-enum-derive/repr_c") ("libc_" "libc" "open-enum-derive/repr_c"))))))

(define-public crate-open-enum-0.4.0 (c (n "open-enum") (v "0.4.0") (d (list (d (n "libc") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "open-enum-derive") (r "=0.4.0") (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "14664lys4havblg4isdkwsdpk3l0p90a86sqk0swa7yhhz6x813k") (f (quote (("std" "open-enum-derive/repr_c") ("libc_" "libc" "open-enum-derive/repr_c"))))))

(define-public crate-open-enum-0.4.1 (c (n "open-enum") (v "0.4.1") (d (list (d (n "libc") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "open-enum-derive") (r "=0.4.1") (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)) (d (n "zerocopy") (r "^0.7.11") (f (quote ("derive"))) (d #t) (k 2)))) (h "0vah68205ajnabpc2b38d5r56jkscisapz4cv59afgz7nfa5nj5s") (f (quote (("std" "open-enum-derive/repr_c") ("libc_" "libc" "open-enum-derive/repr_c"))))))

(define-public crate-open-enum-0.5.0 (c (n "open-enum") (v "0.5.0") (d (list (d (n "libc") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "open-enum-derive") (r "=0.5.0") (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)) (d (n "zerocopy") (r "^0.7.11") (f (quote ("derive"))) (d #t) (k 2)))) (h "1l9p3c4h62g8sbkmy7cygqbscib9fvhxymbpy6imzrp4gar0yziy") (f (quote (("std" "open-enum-derive/repr_c") ("libc_" "libc" "open-enum-derive/repr_c"))))))

(define-public crate-open-enum-0.5.1 (c (n "open-enum") (v "0.5.1") (d (list (d (n "libc") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "open-enum-derive") (r "=0.5.1") (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)) (d (n "zerocopy") (r "^0.7.11") (f (quote ("derive"))) (d #t) (k 2)))) (h "0vziihb05kkgmq5rqhrqi9ml9ivwmqdjdrpcjqxg4cmkwzjf520f") (f (quote (("std" "open-enum-derive/repr_c") ("libc_" "libc" "open-enum-derive/repr_c"))))))

