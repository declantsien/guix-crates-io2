(define-module (crates-io op en openbuildservice) #:use-module (crates-io))

(define-public crate-openbuildservice-0.1.0 (c (n "openbuildservice") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.6") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "05zpqrm71rzb4nrxg31g3jh489kxnjscwh9hpsj2ah1mrbrbg5qz")))

(define-public crate-openbuildservice-0.1.1 (c (n "openbuildservice") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.6") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "16kkwdbs388qq918cchbzdi2np81vczf59a9k91kbf72ql7nddcz")))

