(define-module (crates-io op en openapi_generator) #:use-module (crates-io))

(define-public crate-openapi_generator-0.1.0 (c (n "openapi_generator") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.25") (d #t) (k 0)) (d (n "handlebars") (r "^2.0.2") (d #t) (k 0)) (d (n "heck") (r "^0.3.1") (d #t) (k 0)) (d (n "json-pointer") (r "^0.3.2") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.3.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.44") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.11") (d #t) (k 0)) (d (n "structopt") (r "^0.3.5") (d #t) (k 0)) (d (n "walkdir") (r "^2.2.9") (d #t) (k 0)))) (h "1a8n8k6g55360y0l0wa9s41pzvmcscf05vakp38v4c2290wv411x")))

