(define-module (crates-io op en openat2) #:use-module (crates-io))

(define-public crate-openat2-0.1.0 (c (n "openat2") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1xxxhyvgd0nnj6ns4j6wlvb47z6y18yfgv42s5f76n1cdp9ya5cc")))

(define-public crate-openat2-0.1.1 (c (n "openat2") (v "0.1.1") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "05xl5slb3i1n2sxg6qmkm1m101dz3kq2gv10742zb4yfb03mgr23")))

(define-public crate-openat2-0.1.2 (c (n "openat2") (v "0.1.2") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1nm4bg7cp3nw1rkjlvr108b61ipg1pgq189k44f6nz73ib21vgvj")))

