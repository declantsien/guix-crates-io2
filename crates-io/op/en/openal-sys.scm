(define-module (crates-io op en openal-sys) #:use-module (crates-io))

(define-public crate-openal-sys-1.16.0-1 (c (n "openal-sys") (v "1.16.0-1") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "1254akrrcbc6b00vk2b4vdhy8j36r0qgd8nvpyg1x6lgggzwap2k")))

(define-public crate-openal-sys-1.16.0-2 (c (n "openal-sys") (v "1.16.0-2") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "1nwx13075vm7j3l38qpfnhmz43vaylq5sm71bbywbkjpbqnlzkw5")))

(define-public crate-openal-sys-1.16.0-3 (c (n "openal-sys") (v "1.16.0-3") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "1567m0dh6wialx635gwk4sbbmizn3lv1p8sql6gppb8dy5vxl7lv")))

(define-public crate-openal-sys-1.16.0-4 (c (n "openal-sys") (v "1.16.0-4") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "0vrnh5k3vxzv9cclisjgbai2m9hr2y38cyml9p43n8d3239wk1ia") (f (quote (("static"))))))

(define-public crate-openal-sys-1.16.0 (c (n "openal-sys") (v "1.16.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "07z08yxpsq1k5pv6lqivzz7936fjc6m3k1zqjw64zmgfq5dc68dm") (f (quote (("static"))))))

