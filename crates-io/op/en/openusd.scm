(define-module (crates-io op en openusd) #:use-module (crates-io))

(define-public crate-openusd-0.1.0 (c (n "openusd") (v "0.1.0") (h "1hc8j46k93a24rr49s9pg93mmv01lpd2fhlhqkwlbs09zdp4wf4a")))

(define-public crate-openusd-0.1.1 (c (n "openusd") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "bytemuck") (r "^1.14.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "glam") (r "^0.25.0") (f (quote ("bytemuck"))) (d #t) (k 0)) (d (n "lz4_flex") (r "^0.11.1") (d #t) (k 0)) (d (n "strum") (r "^0.25.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "02b6c0v9ljn58lancj1fczbnmwrbrlfv6agn930vxiff7swks8l2")))

(define-public crate-openusd-0.1.2 (c (n "openusd") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "bytemuck") (r "^1.14.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "lz4_flex") (r "^0.11.1") (d #t) (k 0)) (d (n "strum") (r "^0.25.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1lfsx97mxznds8yhlsvyvxs4fk86xg89wkl1r5q44xscqjx2879j")))

(define-public crate-openusd-0.1.3 (c (n "openusd") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "bytemuck") (r "^1.14.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "lz4_flex") (r "^0.11.1") (d #t) (k 0)) (d (n "strum") (r "^0.25.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0kzk0h1gyjmzrg0xx07rxb7b81czma3gcszzwvz06nwsmg5zgf7x")))

