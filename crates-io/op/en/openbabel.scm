(define-module (crates-io op en openbabel) #:use-module (crates-io))

(define-public crate-openbabel-0.1.0 (c (n "openbabel") (v "0.1.0") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "openbabel-sys") (r "^0.1.0") (d #t) (k 0)))) (h "0cyw5zda99cb3q6186yi098jw7kgqras5jd9625762hbd4g75d6v")))

(define-public crate-openbabel-0.1.1 (c (n "openbabel") (v "0.1.1") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "openbabel-sys") (r "^0.1.2") (d #t) (k 0)))) (h "1z4mmblklmbvgb6b68lcqj1zy7sw0wkkk78svjzdky2p4zvxalbm")))

(define-public crate-openbabel-0.1.2 (c (n "openbabel") (v "0.1.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "openbabel-sys") (r "^0.1.3") (d #t) (k 0)))) (h "1frmygkmf6j0psf437fq7a5pjqydafa7wbggxjd890b8ykqpd0gv")))

(define-public crate-openbabel-0.2.0 (c (n "openbabel") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "ob_rs") (r "^0.1.4") (d #t) (k 0) (p "openbabel-sys")))) (h "0z5ng2dhcg8wcbxwmjflg3g9wjcs4c20lhr0dhjd0rsg6sdaaivm")))

(define-public crate-openbabel-0.2.1 (c (n "openbabel") (v "0.2.1") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "ob_rs") (r "^0.1.4") (d #t) (k 0) (p "openbabel-sys")))) (h "0rlsijarww9ny5zag1r814nybaqrrz4x6wxbbmakw5zpi5jf73sl")))

(define-public crate-openbabel-0.2.2 (c (n "openbabel") (v "0.2.2") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "ob_rs") (r "^0.1.5") (d #t) (k 0) (p "openbabel-sys")))) (h "12pzkvzihljldifq1dlsii21nxl6rjcra2k2spqyl1l8n42ynwiq")))

(define-public crate-openbabel-0.2.3 (c (n "openbabel") (v "0.2.3") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "ob_rs") (r "^0.1.6") (d #t) (k 0) (p "openbabel-sys")))) (h "0kmx7rkxm1a38h8fgpg19mqr7gq8si7yn98gl1nqksz4pzr9hzs6")))

(define-public crate-openbabel-0.3.0 (c (n "openbabel") (v "0.3.0") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "ob_rs") (r "^0.1.7") (d #t) (k 0) (p "openbabel-sys")))) (h "0fwqjl6drdcqlah4nha2cs8fmcxdsvg2p6vwcqc5pg94q9sw7cap")))

(define-public crate-openbabel-0.3.1 (c (n "openbabel") (v "0.3.1") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "ob_rs") (r "^0.1.7") (d #t) (k 0) (p "openbabel-sys")))) (h "1h68zs9rfgf498hlqqmyccf5788pibbrkf8kn0q1qqpiw3v3zld1")))

(define-public crate-openbabel-0.3.2 (c (n "openbabel") (v "0.3.2") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "ob_rs") (r "^0.1.7") (d #t) (k 0) (p "openbabel-sys")))) (h "1s9abvmmisfhdrh13ghx57gx109jdnjj0w3pl20h751ixayf6r6c")))

(define-public crate-openbabel-0.3.3 (c (n "openbabel") (v "0.3.3") (d (list (d (n "chiral_db_sources") (r "^0.2.0") (d #t) (k 2) (p "chiral-db-sources")) (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "ob_rs") (r "^0.1.8") (d #t) (k 0) (p "openbabel-sys")))) (h "0daf8vwpp01r1l7khs72r3svxxr389m7qsx39ia1y8viv26qx9hs")))

(define-public crate-openbabel-0.3.4 (c (n "openbabel") (v "0.3.4") (d (list (d (n "chiral-db-fp-kind") (r "^0.1.0") (d #t) (k 0)) (d (n "chiral_db_sources") (r "^0.2.0") (d #t) (k 2) (p "chiral-db-sources")) (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "ob_rs") (r "^0.1.8") (d #t) (k 0) (p "openbabel-sys")))) (h "0fz7rh2wff5yk6lncxa6s8lk25cjbbf885qd057x72gzzcffpcra")))

(define-public crate-openbabel-0.3.5 (c (n "openbabel") (v "0.3.5") (d (list (d (n "chiral-db-fp-kind") (r "^0.1.0") (d #t) (k 0)) (d (n "chiral_db_sources") (r "^0.2.0") (d #t) (k 2) (p "chiral-db-sources")) (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "ob_rs") (r "^0.1.8") (d #t) (k 0) (p "openbabel-sys")))) (h "00vfslmwaf8jbdxdylhsywb9xf12b4izmibh7f7xwj3qmrkf3igg")))

(define-public crate-openbabel-0.4.0 (c (n "openbabel") (v "0.4.0") (d (list (d (n "chiral-db-sources") (r "^0.2.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "ob_rs") (r "^0.4.0") (d #t) (k 0) (p "openbabel-sys")))) (h "06p7rdsbp85gscpa7y91vb9l07rz4p8n2fzn1r1hviyj7kcq0hwx")))

(define-public crate-openbabel-0.5.0 (c (n "openbabel") (v "0.5.0") (d (list (d (n "chiral-db-sources") (r "^0.2.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "ob_rs") (r "^0.5.0") (d #t) (k 0) (p "openbabel-sys")) (d (n "strum") (r "^0.24") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum_macros") (r "^0.24") (d #t) (k 0)))) (h "1yfxm17a93qzvyyrzwa7nk93z83r7r1b31764sqpx27p6rrllg8y")))

(define-public crate-openbabel-0.5.1 (c (n "openbabel") (v "0.5.1") (d (list (d (n "chiral-db-sources") (r "^0.2.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "ob_rs") (r "^0.5.0") (d #t) (k 0) (p "openbabel-sys")) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum") (r "^0.24") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum_macros") (r "^0.24") (d #t) (k 0)))) (h "0zhknqvc274c095yk9snqcirzkjdrq90rni7xgac7anzynwqa3mi")))

(define-public crate-openbabel-0.5.2 (c (n "openbabel") (v "0.5.2") (d (list (d (n "chiral-db-sources") (r "^0.2.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "ob_rs") (r "^0.5.0") (d #t) (k 0) (p "openbabel-sys")) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum") (r "^0.24") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum_macros") (r "^0.24") (d #t) (k 0)))) (h "1awafpcisk45h47nfikwhchkhcvkd5cnk1q32bqfggw0b1b8h9qz")))

(define-public crate-openbabel-0.5.3 (c (n "openbabel") (v "0.5.3") (d (list (d (n "chiral-db-sources") (r "^0.2.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "ob_rs") (r "^0.5.3") (d #t) (k 0) (p "openbabel-sys")) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum") (r "^0.24") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum_macros") (r "^0.24") (d #t) (k 0)))) (h "1nmzaxhwks3zh67pi1k39jp4bwyq6z0631pymfkc4vyawh5aq658")))

