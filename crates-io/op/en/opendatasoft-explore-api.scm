(define-module (crates-io op en opendatasoft-explore-api) #:use-module (crates-io))

(define-public crate-opendatasoft-explore-api-0.1.0 (c (n "opendatasoft-explore-api") (v "0.1.0") (d (list (d (n "bytes") (r "~1.3.0") (d #t) (k 0)) (d (n "chrono") (r "~0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "reqwest") (r "~0.11.13") (d #t) (k 0)) (d (n "serde") (r "~1.0.152") (d #t) (k 0)) (d (n "serde_derive") (r "~1.0.152") (d #t) (k 0)) (d (n "serde_json") (r "~1.0.91") (d #t) (k 0)) (d (n "lazy_static") (r "~1.4.0") (d #t) (k 2)) (d (n "mockito") (r "~0.31.1") (d #t) (k 2)) (d (n "tokio") (r "~1.23.0") (f (quote ("macros" "rt"))) (d #t) (k 2)))) (h "1zn93az1l22r3wlf53pnj4zbnm47s0ls3qxd8vymjc26hiqkqda8")))

