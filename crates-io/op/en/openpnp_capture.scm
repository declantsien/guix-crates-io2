(define-module (crates-io op en openpnp_capture) #:use-module (crates-io))

(define-public crate-openpnp_capture-0.1.0 (c (n "openpnp_capture") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.53") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "openpnp_capture_sys") (r "^0.2") (d #t) (k 0)))) (h "1h9srch2cvr7q7xaly12sm69ay9ksfgagzw3m2dn0vnva9w89p9y")))

(define-public crate-openpnp_capture-0.2.0 (c (n "openpnp_capture") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "openpnp_capture_sys") (r "^0.3") (d #t) (k 0)))) (h "0pdci8rzghvcgxij40r8s8mhxggvixic23qpm2rqwif8bw1lf3si")))

(define-public crate-openpnp_capture-0.2.1 (c (n "openpnp_capture") (v "0.2.1") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "openpnp_capture_sys") (r "^0.3") (d #t) (k 0)))) (h "1jac7yn7kjwzvgc5pmcnlqr2h3h6rgjyk3bjh30vmv65rq86i8n6")))

(define-public crate-openpnp_capture-0.2.2 (c (n "openpnp_capture") (v "0.2.2") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "openpnp_capture_sys") (r "^0.3") (d #t) (k 0)))) (h "154yifnkc97nfgl9cib7rikd3jva1j0avi2hh660rb4s4xy2k00g")))

(define-public crate-openpnp_capture-0.2.3 (c (n "openpnp_capture") (v "0.2.3") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "openpnp_capture_sys") (r "^0.3") (d #t) (k 0)))) (h "0p11sh17j2gy61chq1428i1va0sj5rphy8m5ylw74yq52f4zxwh6")))

(define-public crate-openpnp_capture-0.2.4 (c (n "openpnp_capture") (v "0.2.4") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "openpnp_capture_sys") (r "^0.4") (d #t) (k 0)))) (h "146f91lbccj9z5srja4xjbnyc0d6cibpg5q6cc8317s0wg8r00m1")))

