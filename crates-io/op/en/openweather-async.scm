(define-module (crates-io op en openweather-async) #:use-module (crates-io))

(define-public crate-openweather-async-0.1.0 (c (n "openweather-async") (v "0.1.0") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2.22") (f (quote ("macros" "tcp" "dns" "io-util"))) (d #t) (k 0)))) (h "17i3sd53gm3n5f73mgr1hkvmy4f45ghld7pj6563fsrpdynr8shr")))

(define-public crate-openweather-async-0.1.1 (c (n "openweather-async") (v "0.1.1") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2.22") (f (quote ("macros" "tcp" "dns" "io-util"))) (d #t) (k 0)))) (h "13jdbrgg7dnwd7731x9lb1im0c56p5cqj4f0n3zpiqilhaxkv9ca") (y #t)))

(define-public crate-openweather-async-0.1.2 (c (n "openweather-async") (v "0.1.2") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2.22") (f (quote ("macros" "tcp" "dns" "io-util"))) (d #t) (k 0)))) (h "1jvrzslvag74masx4pycwdk0dmmr591jzb080ipy1gbxa0mvb9jn")))

