(define-module (crates-io op en openssh-keys) #:use-module (crates-io))

(define-public crate-openssh-keys-0.1.0 (c (n "openssh-keys") (v "0.1.0") (d (list (d (n "base64") (r "^0.6") (d #t) (k 0)) (d (n "byteorder") (r "^1.1") (d #t) (k 0)) (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)))) (h "1hn7f5pvjmlll3cill9lmagss8ss1z9igvzhadnqbzl5zn1pgia3")))

(define-public crate-openssh-keys-0.1.1 (c (n "openssh-keys") (v "0.1.1") (d (list (d (n "base64") (r "^0.6") (d #t) (k 0)) (d (n "byteorder") (r "^1.1") (d #t) (k 0)) (d (n "error-chain") (r "^0.11") (k 0)) (d (n "sha2") (r "^0.6") (d #t) (k 0)))) (h "10bpav5dkzmnkl2md65cs241h9kca5mgsvnda8q4nfg2jdrcnccg")))

(define-public crate-openssh-keys-0.1.2 (c (n "openssh-keys") (v "0.1.2") (d (list (d (n "base64") (r "^0.6") (d #t) (k 0)) (d (n "byteorder") (r "^1.1") (d #t) (k 0)) (d (n "error-chain") (r "^0.11") (k 0)) (d (n "sha2") (r "^0.6") (d #t) (k 0)))) (h "136ybmv79v8z4q6frxkj845kljzyy1d0xg7dm2sl4xrz7x789bx1")))

(define-public crate-openssh-keys-0.2.0 (c (n "openssh-keys") (v "0.2.0") (d (list (d (n "base64") (r "^0.6") (d #t) (k 0)) (d (n "byteorder") (r "^1.1") (d #t) (k 0)) (d (n "error-chain") (r "^0.11") (k 0)) (d (n "sha2") (r "^0.6") (d #t) (k 0)))) (h "0md24k30w3hqxn02br1bqkkwyy8ci0ca2g1jrh43xh2gd1zhhxpb")))

(define-public crate-openssh-keys-0.2.1 (c (n "openssh-keys") (v "0.2.1") (d (list (d (n "base64") (r "^0.6") (d #t) (k 0)) (d (n "byteorder") (r "^1.1") (d #t) (k 0)) (d (n "error-chain") (r "^0.11") (k 0)) (d (n "md-5") (r "^0.7.0") (d #t) (k 0)) (d (n "sha2") (r "^0.6") (d #t) (k 0)))) (h "0gn4wr9v1m3l7g7bfbvkz4qcrpxlnj2nzk3wp0xs3dg7k9fb3vy9")))

(define-public crate-openssh-keys-0.2.2 (c (n "openssh-keys") (v "0.2.2") (d (list (d (n "base64") (r "^0.8") (d #t) (k 0)) (d (n "byteorder") (r "^1.1") (d #t) (k 0)) (d (n "error-chain") (r "^0.11") (k 0)) (d (n "md-5") (r "^0.7") (d #t) (k 0)) (d (n "sha2") (r "^0.7") (d #t) (k 0)))) (h "0r9spd7v9iwrgqzw18r2hl5blby8glww6ahygchvk557lvfgnj0b")))

(define-public crate-openssh-keys-0.2.3 (c (n "openssh-keys") (v "0.2.3") (d (list (d (n "base64") (r "^0.9") (d #t) (k 0)) (d (n "byteorder") (r "^1.1") (d #t) (k 0)) (d (n "error-chain") (r "^0.12") (k 0)) (d (n "md-5") (r "^0.7") (d #t) (k 0)) (d (n "sha2") (r "^0.7") (d #t) (k 0)))) (h "0dbf8wx3jajb5d5g29szdzqkqb8xjrwqn5mr8gywgkjzjvg86pp5") (y #t)))

(define-public crate-openssh-keys-0.3.0 (c (n "openssh-keys") (v "0.3.0") (d (list (d (n "base64") (r "^0.9") (d #t) (k 0)) (d (n "byteorder") (r "^1.1") (d #t) (k 0)) (d (n "error-chain") (r "^0.12") (k 0)) (d (n "md-5") (r "^0.7") (d #t) (k 0)) (d (n "sha2") (r "^0.7") (d #t) (k 0)))) (h "0s8sw27aih1y5lsphqrwvd67gxfwqi9jr1hqnijfcn6s9n4wdm86")))

(define-public crate-openssh-keys-0.4.0 (c (n "openssh-keys") (v "0.4.0") (d (list (d (n "base64") (r "^0.9") (d #t) (k 0)) (d (n "byteorder") (r "^1.1") (d #t) (k 0)) (d (n "dirs") (r "^1.0") (d #t) (k 2)) (d (n "error-chain") (r "^0.12") (k 0)) (d (n "md-5") (r "^0.8") (d #t) (k 0)) (d (n "sha2") (r "^0.8") (d #t) (k 0)))) (h "06y1a2j682s06ahrq0a8hg84dmjfvfjhirchx1717vx23lv4d2gs")))

(define-public crate-openssh-keys-0.4.1 (c (n "openssh-keys") (v "0.4.1") (d (list (d (n "base64") (r "^0.10") (d #t) (k 0)) (d (n "byteorder") (r "^1.1") (d #t) (k 0)) (d (n "dirs") (r "^1.0") (d #t) (k 2)) (d (n "error-chain") (r "^0.12") (k 0)) (d (n "md-5") (r "^0.8") (d #t) (k 0)) (d (n "sha2") (r "^0.8") (d #t) (k 0)))) (h "0cx1dj5zbv7ymi1mk9249mnwxnsi72j81hmg6547giirr12vpayw")))

(define-public crate-openssh-keys-0.4.2 (c (n "openssh-keys") (v "0.4.2") (d (list (d (n "base64") (r "^0.12") (d #t) (k 0)) (d (n "byteorder") (r "^1.1") (d #t) (k 0)) (d (n "dirs") (r "^3.0") (d #t) (k 2)) (d (n "error-chain") (r "^0.12") (k 0)) (d (n "md-5") (r "^0.9") (d #t) (k 0)) (d (n "sha2") (r "^0.9") (d #t) (k 0)))) (h "0lv47viasmwcm0mmb0xpwdgc5vbns7v0py3wwnw842f9b9s9igyd")))

(define-public crate-openssh-keys-0.5.0 (c (n "openssh-keys") (v "0.5.0") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "byteorder") (r "^1.1") (d #t) (k 0)) (d (n "dirs") (r "^3.0") (d #t) (k 2)) (d (n "md-5") (r "^0.9") (d #t) (k 0)) (d (n "sha2") (r "^0.9") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0w8ppxjp6a2szfm76hzl4irqdjvp0wszj6rzqwd2dsnykilrl977")))

(define-public crate-openssh-keys-0.6.0 (c (n "openssh-keys") (v "0.6.0") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "byteorder") (r "^1.1") (d #t) (k 0)) (d (n "dirs") (r ">=3.0, <5.0") (d #t) (k 2)) (d (n "md-5") (r ">=0.9, <0.11") (d #t) (k 0)) (d (n "sha2") (r ">=0.9, <0.11") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1b60bag5md5sdjn2libbqhavca5b57yicmnsk2rlranqg278md9p")))

(define-public crate-openssh-keys-0.6.1 (c (n "openssh-keys") (v "0.6.1") (d (list (d (n "base64") (r "^0.21") (d #t) (k 0)) (d (n "byteorder") (r "^1.1") (d #t) (k 0)) (d (n "home") (r "~0.5") (d #t) (k 2)) (d (n "md-5") (r ">=0.9, <0.11") (d #t) (k 0)) (d (n "sha2") (r ">=0.9, <0.11") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0lag9i9j5hxawbcvf15mzcrc4m9k208yr4r1ngp7nsx1vqpwzs47") (r "1.58.0")))

(define-public crate-openssh-keys-0.6.2 (c (n "openssh-keys") (v "0.6.2") (d (list (d (n "base64") (r "^0.21") (d #t) (k 0)) (d (n "byteorder") (r "^1.1") (d #t) (k 0)) (d (n "home") (r "~0.5") (d #t) (k 2)) (d (n "md-5") (r "^0.10") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0n7xbh2in8f70i48cxh1c120xk2z6a4l48h3nlpl20mks710wnn7") (r "1.58.0")))

(define-public crate-openssh-keys-0.6.3 (c (n "openssh-keys") (v "0.6.3") (d (list (d (n "base64") (r "^0.21") (d #t) (k 0)) (d (n "byteorder") (r "^1.1") (d #t) (k 0)) (d (n "home") (r "~0.5") (d #t) (k 2)) (d (n "md-5") (r "^0.10") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0gayq9dl0bvk5s9rdwknkikifg3imka10hy7hwp599j1qik9b4z9") (r "1.75.0")))

