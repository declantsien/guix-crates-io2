(define-module (crates-io op en opentype) #:use-module (crates-io))

(define-public crate-opentype-0.0.1 (c (n "opentype") (v "0.0.1") (h "1ji589d2lv95pdngd60qn65bxafg1w1hbalhs2xhjsgqh5pcmfd9")))

(define-public crate-opentype-0.0.2 (c (n "opentype") (v "0.0.2") (d (list (d (n "date") (r "^0.0.1") (d #t) (k 0)))) (h "17pvlvxxf4ywg91ff09kq2yvvf42ay8dsz2mycrn1lq7c74bl5wx")))

(define-public crate-opentype-0.0.3 (c (n "opentype") (v "0.0.3") (d (list (d (n "assert") (r "^0.0.2") (d #t) (k 2)) (d (n "date") (r "^0.0.5") (d #t) (k 0)))) (h "0qgvrw39m93w8wdr81vsx9ggz3lklsgj96sfb118wwkhvq83f5d8")))

(define-public crate-opentype-0.0.4 (c (n "opentype") (v "0.0.4") (d (list (d (n "assert") (r "^0.0.2") (d #t) (k 2)) (d (n "date") (r "^0.0.6") (d #t) (k 0)))) (h "11r77y3bhk9pd6qkl14ywd5i9p72wc2x5lp5kjxxp04myp35dr81")))

(define-public crate-opentype-0.0.5 (c (n "opentype") (v "0.0.5") (d (list (d (n "assert") (r "^0.0.3") (d #t) (k 2)) (d (n "date") (r "^0.0.7") (d #t) (k 0)))) (h "0q42kwdb5w9j5w85hzhw01b4d64wmwjaj7252fgcxqk1qs94272h")))

(define-public crate-opentype-0.0.6 (c (n "opentype") (v "0.0.6") (d (list (d (n "assert") (r "^0.0.4") (d #t) (k 2)) (d (n "date") (r "^0.1") (d #t) (k 0)))) (h "0n6cmqkwbjnw33r7nvrdq4pyjlj8ympwvrlgm2z2apigybivqk8m")))

(define-public crate-opentype-0.0.7 (c (n "opentype") (v "0.0.7") (d (list (d (n "assert") (r "^0.1") (d #t) (k 2)) (d (n "date") (r "^0.1") (d #t) (k 0)))) (h "0sxm2d1aidds5fjll3q7hmshd97xrx86f4gpf7566mqz5wkg4446")))

(define-public crate-opentype-0.0.8 (c (n "opentype") (v "0.0.8") (d (list (d (n "assert") (r "^0.1") (d #t) (k 2)) (d (n "date") (r "^0.1") (d #t) (k 0)))) (h "11pdz55vqmn18iwm8y6i3hdz6w7xvr7qzbvbci61flkqarwszswj")))

(define-public crate-opentype-0.0.9 (c (n "opentype") (v "0.0.9") (d (list (d (n "assert") (r "^0.1") (d #t) (k 2)) (d (n "date") (r "^0.1") (d #t) (k 0)))) (h "0xxma747qdzlj50sf3762gg7240jip53da7dmwrzvwaavh8aj4zh")))

(define-public crate-opentype-0.1.0 (c (n "opentype") (v "0.1.0") (d (list (d (n "assert") (r "*") (d #t) (k 2)) (d (n "date") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)))) (h "13bm3rzhzwxl9lna09bb6ck0y2l271jsxy3k43ifm764qy2clkdn")))

(define-public crate-opentype-0.1.1 (c (n "opentype") (v "0.1.1") (d (list (d (n "date") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)))) (h "12lglhkhbi6hd3744giwqq9c2pi3ly9wyyvfq0a4wb1xsmwy6afz")))

(define-public crate-opentype-0.1.2 (c (n "opentype") (v "0.1.2") (d (list (d (n "date") (r "*") (d #t) (k 0)))) (h "14f1zwjzlfhav32wq5dnlrfvwphp4xkizi5pk1phzl3sgqs9mgba")))

(define-public crate-opentype-0.2.0 (c (n "opentype") (v "0.2.0") (h "0bz541rajl15v01as77px1fyyjwmac967fwq5zqpm2nm75fi6pvm")))

(define-public crate-opentype-0.2.1 (c (n "opentype") (v "0.2.1") (h "055zjxxsvzf2sq5rkiz4k20hl70v882c297w8ks5nmdsdi6x9v3c")))

(define-public crate-opentype-0.3.0 (c (n "opentype") (v "0.3.0") (h "1jfs6x3dw84sh1yzk1iiq70a9mjg9aah802q56rmpr6kyz6j4fqy")))

(define-public crate-opentype-0.3.1 (c (n "opentype") (v "0.3.1") (h "17xj1abw0bl3bb3zjw2x2wzvf8am34alhbv6jlzn57q82vvm6g62")))

(define-public crate-opentype-0.3.2 (c (n "opentype") (v "0.3.2") (h "0w8wi4y1a6rnc7l1aadccb371bi2rx81k80kv7l5zsi827h7hzr0")))

(define-public crate-opentype-0.3.3 (c (n "opentype") (v "0.3.3") (h "0sgcx00aff13xwqbi0zlk7ax8ybjx3axmjjaxlcsi0vfcjbyfrgk")))

(define-public crate-opentype-0.3.4 (c (n "opentype") (v "0.3.4") (d (list (d (n "truetype") (r "*") (d #t) (k 0)))) (h "0442687j8aczlg7k46pnlgbl59b3js3qghhjrvw1qv6pw44h4lqi")))

(define-public crate-opentype-0.3.5 (c (n "opentype") (v "0.3.5") (d (list (d (n "truetype") (r "*") (d #t) (k 0)))) (h "1v5wmk76l4h4z5mmfq44mbk6gwrrr7lwis722bac4as7cyqkmpz3")))

(define-public crate-opentype-0.4.0 (c (n "opentype") (v "0.4.0") (d (list (d (n "postscript") (r "*") (d #t) (k 0)) (d (n "truetype") (r "*") (d #t) (k 0)))) (h "1nzyb76gdfdcgk6rf8ab4q9cd7wjnqhpl55n6f43bsl4zwp9nn71")))

(define-public crate-opentype-0.4.1 (c (n "opentype") (v "0.4.1") (d (list (d (n "postscript") (r "*") (d #t) (k 0)) (d (n "truetype") (r "*") (d #t) (k 0)))) (h "1nzgbyd6q5wb6fc3irbslc739ngj3splc7h12av1wz864k963a7p")))

(define-public crate-opentype-0.4.2 (c (n "opentype") (v "0.4.2") (d (list (d (n "postscript") (r "*") (d #t) (k 0)) (d (n "truetype") (r "*") (d #t) (k 0)))) (h "1a8a3kzk01gx6xax9ix174avrfcn3ixnc4hq1i0ipn7q51fc88yk")))

(define-public crate-opentype-0.4.3 (c (n "opentype") (v "0.4.3") (d (list (d (n "postscript") (r "^0.2") (d #t) (k 0)) (d (n "truetype") (r "^0.3") (d #t) (k 0)))) (h "1vzm42a3zhl0b168yws2d8bpywi687b3sfzdm2z8vkiqiqv1js3d")))

(define-public crate-opentype-0.5.0 (c (n "opentype") (v "0.5.0") (d (list (d (n "postscript") (r "^0.3") (d #t) (k 0)) (d (n "truetype") (r "^0.4") (d #t) (k 0)))) (h "136qsvffq8jclfcfgm2n7smcpjddawapxjjlw8v8a65ickizzpq7")))

(define-public crate-opentype-0.5.1 (c (n "opentype") (v "0.5.1") (d (list (d (n "postscript") (r "^0.3") (d #t) (k 0)) (d (n "truetype") (r "^0.4") (d #t) (k 0)))) (h "10f3xfh84hssx1msyhzapb75sq4xa9hsz8sr9xx1prrl8ix276nh")))

(define-public crate-opentype-0.6.0 (c (n "opentype") (v "0.6.0") (d (list (d (n "postscript") (r "^0.3") (d #t) (k 0)) (d (n "truetype") (r "^0.5") (d #t) (k 0)))) (h "1mgph76m13fhq6mssq5wcljj68cg4jkpimmv8gs07rvi41zhwk61")))

(define-public crate-opentype-0.7.0 (c (n "opentype") (v "0.7.0") (d (list (d (n "postscript") (r "^0.3") (d #t) (k 0)) (d (n "truetype") (r "^0.6") (d #t) (k 0)))) (h "11a3wqbcl8iijpgdbfm3fikhfj79s0kswvs7c1b9ly6ynlqqy46v")))

(define-public crate-opentype-0.8.0 (c (n "opentype") (v "0.8.0") (d (list (d (n "postscript") (r "^0.3") (d #t) (k 0)) (d (n "truetype") (r "^0.6") (d #t) (k 0)))) (h "0xfxzb8r9ln2xq0hyr612mn2xkl5zf5sn9p02wjanfxx3fxd930b")))

(define-public crate-opentype-0.9.0 (c (n "opentype") (v "0.9.0") (d (list (d (n "postscript") (r "^0.3") (d #t) (k 0)) (d (n "truetype") (r "^0.6") (d #t) (k 0)))) (h "18accg970409qkh317mby2x808dzll8dzp0rkf7z6j41awl940dv")))

(define-public crate-opentype-0.9.1 (c (n "opentype") (v "0.9.1") (d (list (d (n "postscript") (r "^0.4") (d #t) (k 0)) (d (n "truetype") (r "^0.6") (d #t) (k 0)))) (h "1pl7nsiwny9y7gjhl5iywxw9f52islj8wk096gwc0bqbkg9jgrr3")))

(define-public crate-opentype-0.10.0 (c (n "opentype") (v "0.10.0") (d (list (d (n "postscript") (r "^0.5") (d #t) (k 0)) (d (n "truetype") (r "^0.7") (d #t) (k 0)))) (h "038qqcs3jgx6sgbpyf8bdjb3yj58w8wxx7ny3ixdq2v337b17bw5")))

(define-public crate-opentype-0.10.1 (c (n "opentype") (v "0.10.1") (d (list (d (n "postscript") (r "^0.5") (d #t) (k 0)) (d (n "truetype") (r "^0.7") (d #t) (k 0)))) (h "1g1xwihmc3pv713dwwm43kxwbrmrj8q665rsx049rph187y1a2r0")))

(define-public crate-opentype-0.11.0 (c (n "opentype") (v "0.11.0") (d (list (d (n "postscript") (r "^0.6") (d #t) (k 0)) (d (n "truetype") (r "^0.9") (d #t) (k 0)))) (h "05w2r0sn9hlgvadkfyp3wvrl6i4m3acrjk3vv7s2bcpaa0ay74wb")))

(define-public crate-opentype-0.12.0 (c (n "opentype") (v "0.12.0") (d (list (d (n "postscript") (r "^0.6") (d #t) (k 0)) (d (n "truetype") (r "^0.10") (d #t) (k 0)))) (h "0bzmh3dd07g519mnwdhsi5p2qr353n9fj34g2is3m7h49j9id77w")))

(define-public crate-opentype-0.13.0 (c (n "opentype") (v "0.13.0") (d (list (d (n "postscript") (r "^0.7") (d #t) (k 0)) (d (n "truetype") (r "^0.10") (d #t) (k 0)))) (h "0z483iaj7dpg62xnwkb4kkfljfcs016kj7mf38f7zicy4pka5bj1")))

(define-public crate-opentype-0.14.0 (c (n "opentype") (v "0.14.0") (d (list (d (n "postscript") (r "^0.7") (d #t) (k 0)) (d (n "truetype") (r "^0.11") (d #t) (k 0)))) (h "127831ig1fqm67qbjhlm9p6cw8wlb35pa9v9jl7qsa7bi22yzw5i")))

(define-public crate-opentype-0.14.1 (c (n "opentype") (v "0.14.1") (d (list (d (n "postscript") (r "^0.7") (d #t) (k 0)) (d (n "truetype") (r "^0.13") (d #t) (k 0)))) (h "1h74vhfpxbda57qd5bx9dpzhzkpjjgsdqq1bq0himxi126z9mprn")))

(define-public crate-opentype-0.15.0 (c (n "opentype") (v "0.15.0") (d (list (d (n "postscript") (r "^0.10") (d #t) (k 0)) (d (n "truetype") (r "^0.20") (d #t) (k 0)))) (h "1qhwp9p6mgzwgac5nf4fjfg748j95ii5ps2i09ag5hfaa836mni1")))

(define-public crate-opentype-0.16.0 (c (n "opentype") (v "0.16.0") (d (list (d (n "postscript") (r "^0.11") (d #t) (k 0)) (d (n "truetype") (r "^0.20") (d #t) (k 0)))) (h "11gdvgiwkbl560vngsbgam3jfih3ygkplcdf8al6cm1zi1pcvlms")))

(define-public crate-opentype-0.17.0 (c (n "opentype") (v "0.17.0") (d (list (d (n "postscript") (r "^0.11") (d #t) (k 0)) (d (n "truetype") (r "^0.22") (d #t) (k 0)))) (h "03zzs6jfcf6y2yifk18z2081qd0y6h6zx207xsxw83mmkv24i6vn")))

(define-public crate-opentype-0.17.1 (c (n "opentype") (v "0.17.1") (d (list (d (n "postscript") (r "^0.11") (d #t) (k 0)) (d (n "truetype") (r "^0.22") (d #t) (k 0)))) (h "1kkyk8pcfyih63myql2vz4r3h3m9h9izhnx7k71pnvsxsx2xmnfx")))

(define-public crate-opentype-0.19.0 (c (n "opentype") (v "0.19.0") (d (list (d (n "postscript") (r "^0.14") (d #t) (k 0)) (d (n "truetype") (r "^0.30") (d #t) (k 0)))) (h "0assc3yqb2ib8c5j9jlf2zfbxysqk413jhh83bdw7aailsd8042g")))

(define-public crate-opentype-0.19.1 (c (n "opentype") (v "0.19.1") (d (list (d (n "postscript") (r "^0.14") (d #t) (k 0)) (d (n "truetype") (r "^0.31") (d #t) (k 0)))) (h "16zrqvi1hwgi1lz4vwf187zc0vgnrqrr2yan3ld8hf7ljfi79xi4") (f (quote (("ignore-invalid-checksums"))))))

(define-public crate-opentype-0.20.0 (c (n "opentype") (v "0.20.0") (d (list (d (n "postscript") (r "^0.16") (d #t) (k 0)) (d (n "truetype") (r "^0.32") (d #t) (k 0)) (d (n "typeface") (r "^0.1") (d #t) (k 0)))) (h "0g52xahf88lpirsr84y1xcgzvw8f13sjspv0gk8v5pls21l4v520") (f (quote (("ignore-invalid-checksums"))))))

(define-public crate-opentype-0.20.1 (c (n "opentype") (v "0.20.1") (d (list (d (n "postscript") (r "^0.16") (d #t) (k 0)) (d (n "truetype") (r "^0.33") (d #t) (k 0)) (d (n "typeface") (r "^0.1") (d #t) (k 0)))) (h "0g47qlk6y6571qsrdgyhzh0mzlp6ys3j961byiwsym7mmgm5rnsm") (f (quote (("ignore-invalid-checksums"))))))

(define-public crate-opentype-0.20.2 (c (n "opentype") (v "0.20.2") (d (list (d (n "postscript") (r "^0.16") (d #t) (k 0)) (d (n "truetype") (r "^0.34") (d #t) (k 0)) (d (n "typeface") (r "^0.1") (d #t) (k 0)))) (h "0b4wvsf05al413by96zi5sj6k88wlbbzb20wnnl1pl3h5zy3gack") (f (quote (("ignore-invalid-checksums"))))))

(define-public crate-opentype-0.21.0 (c (n "opentype") (v "0.21.0") (d (list (d (n "postscript") (r "^0.16") (d #t) (k 0)) (d (n "truetype") (r "^0.35") (d #t) (k 0)) (d (n "typeface") (r "^0.1") (d #t) (k 0)))) (h "1bz33fajya4kpxlyarjm5i71k2p087jch338apzx9l3pjs5qn2aj") (f (quote (("ignore-invalid-checksums"))))))

(define-public crate-opentype-0.22.0 (c (n "opentype") (v "0.22.0") (d (list (d (n "postscript") (r "^0.16") (d #t) (k 0)) (d (n "truetype") (r "^0.36") (d #t) (k 0)) (d (n "typeface") (r "^0.2") (d #t) (k 0)))) (h "17l98ax7lb0sa7nb0ksscp1c53wb2im2hxvzig0x32jbfvzy6jd1") (f (quote (("ignore-invalid-checksums"))))))

(define-public crate-opentype-0.23.0 (c (n "opentype") (v "0.23.0") (d (list (d (n "postscript") (r "^0.16") (d #t) (k 0)) (d (n "truetype") (r "^0.37") (d #t) (k 0)) (d (n "typeface") (r "^0.2") (d #t) (k 0)))) (h "1v12halmdd84kh5v7qnqbdr25rrjq0g9lif6c7ghhhayc4dvgsnx") (f (quote (("ignore-invalid-checksums"))))))

(define-public crate-opentype-0.23.1 (c (n "opentype") (v "0.23.1") (d (list (d (n "postscript") (r "^0.16") (d #t) (k 0)) (d (n "truetype") (r "^0.38") (d #t) (k 0)) (d (n "typeface") (r "^0.2") (d #t) (k 0)))) (h "1sj1x8apcgmpzylw905f3vgia5j8sgl61npms6gbhpn76b4cwzjg") (f (quote (("ignore-invalid-checksums"))))))

(define-public crate-opentype-0.23.2 (c (n "opentype") (v "0.23.2") (d (list (d (n "postscript") (r "^0.16.4") (d #t) (k 0)) (d (n "truetype") (r "^0.39.0") (d #t) (k 0)) (d (n "typeface") (r "^0.2.2") (d #t) (k 0)))) (h "1064110qmzms76j0z424dihvkgy9kch2gvg1c8lv24hkm527s88r") (f (quote (("ignore-invalid-checksums"))))))

(define-public crate-opentype-0.24.0 (c (n "opentype") (v "0.24.0") (d (list (d (n "postscript") (r "^0.16.4") (d #t) (k 0)) (d (n "truetype") (r "^0.39.0") (d #t) (k 0)) (d (n "typeface") (r "^0.2.2") (d #t) (k 0)))) (h "1vdss3gvyy8mlrg202jbfyfij8prpzxyspnsap86k0pb088rgnsn") (f (quote (("ignore-invalid-checksums"))))))

(define-public crate-opentype-0.24.1 (c (n "opentype") (v "0.24.1") (d (list (d (n "postscript") (r "^0.16.4") (d #t) (k 0)) (d (n "truetype") (r "^0.39.0") (d #t) (k 0)) (d (n "typeface") (r "^0.2.2") (d #t) (k 0)))) (h "0axg2y4mizzpxx1rxb4jk2nlx0clxzcqwsdl5v456gqsbhbxfr6v") (f (quote (("ignore-invalid-checksums"))))))

(define-public crate-opentype-0.24.2 (c (n "opentype") (v "0.24.2") (d (list (d (n "postscript") (r "^0.16.4") (d #t) (k 0)) (d (n "truetype") (r "^0.40.0") (d #t) (k 0)) (d (n "typeface") (r "^0.2.3") (d #t) (k 0)))) (h "06wirq87hvwr6fngrgr80c9zh4zdqnnyl08i4by2ia2vypk0rcmj") (f (quote (("ignore-invalid-checksums"))))))

(define-public crate-opentype-0.24.3 (c (n "opentype") (v "0.24.3") (d (list (d (n "postscript") (r "^0.16.5") (d #t) (k 0)) (d (n "truetype") (r "^0.40.0") (d #t) (k 0)) (d (n "typeface") (r "^0.2.3") (d #t) (k 0)))) (h "0q45av5g864ygpcp9skpqc6sx36mqmqysnvydgb2y43b0hbmxiyr") (f (quote (("ignore-invalid-checksums"))))))

(define-public crate-opentype-0.24.4 (c (n "opentype") (v "0.24.4") (d (list (d (n "postscript") (r "^0.16.5") (d #t) (k 0)) (d (n "truetype") (r "^0.40.1") (d #t) (k 0)) (d (n "typeface") (r "^0.2.4") (d #t) (k 0)))) (h "0sfagywlnacfd6x3yn0vf547cws5fv4311g9xss9bhiaj8jv0l5i") (f (quote (("ignore-invalid-checksums"))))))

(define-public crate-opentype-0.24.5 (c (n "opentype") (v "0.24.5") (d (list (d (n "postscript") (r "^0.16.6") (d #t) (k 0)) (d (n "truetype") (r "^0.41.0") (d #t) (k 0)) (d (n "typeface") (r "^0.2.4") (d #t) (k 0)))) (h "13anvgnh2b61c9iyzyd2rqcw118vcw6sx8bkkbnr6iph88hy32a6") (f (quote (("ignore-invalid-checksums"))))))

(define-public crate-opentype-0.24.6 (c (n "opentype") (v "0.24.6") (d (list (d (n "postscript") (r "^0.17.0") (d #t) (k 0)) (d (n "truetype") (r "^0.41.0") (d #t) (k 0)) (d (n "typeface") (r "^0.2.4") (d #t) (k 0)))) (h "0r7idasnrdafkvz7b5ixgg42lzjph8aqddc51snf3kwc5hs6l7w9") (f (quote (("ignore-invalid-checksums"))))))

(define-public crate-opentype-0.24.7 (c (n "opentype") (v "0.24.7") (d (list (d (n "postscript") (r "^0.17.1") (d #t) (k 0)) (d (n "truetype") (r "^0.41.0") (d #t) (k 0)) (d (n "typeface") (r "^0.2.4") (d #t) (k 0)))) (h "1jvrdm5r78v5d1529hix2l0hpmkpiy1jkms8kb2g25s2a62lvy30") (f (quote (("ignore-invalid-checksums"))))))

(define-public crate-opentype-0.24.8 (c (n "opentype") (v "0.24.8") (d (list (d (n "postscript") (r "^0.17.2") (d #t) (k 0)) (d (n "truetype") (r "^0.41.0") (d #t) (k 0)) (d (n "typeface") (r "^0.2.4") (d #t) (k 0)))) (h "0nrfq04a4bqbg72bxmxm7lyjgj05hyq2iwfbisljy5pagayb15yv") (f (quote (("ignore-invalid-checksums"))))))

(define-public crate-opentype-0.24.9 (c (n "opentype") (v "0.24.9") (d (list (d (n "postscript") (r "^0.17.3") (d #t) (k 0)) (d (n "truetype") (r "^0.41.1") (d #t) (k 0)) (d (n "typeface") (r "^0.2.7") (d #t) (k 0)))) (h "0kkyp42sgrnpmaqk3pz5yn4ffx1blsm89br0jrqndj3pr9llxyrn") (f (quote (("ignore-invalid-checksums"))))))

(define-public crate-opentype-0.24.10 (c (n "opentype") (v "0.24.10") (d (list (d (n "postscript") (r "^0.17.3") (d #t) (k 0)) (d (n "truetype") (r "^0.42.0") (d #t) (k 0)) (d (n "typeface") (r "^0.2.7") (d #t) (k 0)))) (h "1mxqzxfr6w1fl5ycjsinqcwdq29whk4jpl3p2j9aqaa6h1xaml7k") (f (quote (("ignore-invalid-checksums"))))))

(define-public crate-opentype-0.24.11 (c (n "opentype") (v "0.24.11") (d (list (d (n "postscript") (r "^0.17.3") (d #t) (k 0)) (d (n "truetype") (r "^0.42.0") (d #t) (k 0)) (d (n "typeface") (r "^0") (d #t) (k 0)))) (h "1zxmsmi6y8cr6d472ij1gcr8z9zf6v986pjkhqrhh8zf2yhq16j5") (f (quote (("ignore-invalid-checksums"))))))

(define-public crate-opentype-0.24.12 (c (n "opentype") (v "0.24.12") (d (list (d (n "postscript") (r "^0.17.4") (d #t) (k 0)) (d (n "truetype") (r "^0.42.1") (d #t) (k 0)) (d (n "typeface") (r "^0") (d #t) (k 0)))) (h "1g0xq9kq5w25wna096dv4ii9l9nqpmbhnv4frg62xs2s39g5j2dq") (f (quote (("ignore-invalid-checksums"))))))

(define-public crate-opentype-0.24.13 (c (n "opentype") (v "0.24.13") (d (list (d (n "postscript") (r "^0.17.4") (d #t) (k 0)) (d (n "truetype") (r "^0.42.1") (d #t) (k 0)) (d (n "typeface") (r "^0") (d #t) (k 0)))) (h "0f207rkdif547awyvg11zacrmps76cspjvj61y2gimhajvib2lny") (f (quote (("ignore-invalid-checksums"))))))

(define-public crate-opentype-0.24.14 (c (n "opentype") (v "0.24.14") (d (list (d (n "postscript") (r "^0.17.4") (d #t) (k 0)) (d (n "truetype") (r "^0.42.6") (d #t) (k 0)) (d (n "typeface") (r "^0") (d #t) (k 0)))) (h "0qc9cb0g9kabxnhqgmcnm3a84x454q2a6nbxl78w54arw9r6sj56") (f (quote (("ignore-invalid-checksums"))))))

(define-public crate-opentype-0.25.0 (c (n "opentype") (v "0.25.0") (d (list (d (n "postscript") (r "^0.17.4") (d #t) (k 0)) (d (n "truetype") (r "^0.42.6") (d #t) (k 0)) (d (n "typeface") (r "^0") (d #t) (k 0)))) (h "19142y6rshsb616lk81lv7aygp5i10hyn056nykxnnqvak80gpkf") (f (quote (("ignore-invalid-checksums"))))))

(define-public crate-opentype-0.26.0 (c (n "opentype") (v "0.26.0") (d (list (d (n "postscript") (r "^0.17.4") (d #t) (k 0)) (d (n "truetype") (r "^0.43.0") (d #t) (k 0)) (d (n "typeface") (r "^0") (d #t) (k 0)))) (h "1i03x0i283cmif91p1mx6qq6c7hqn8x5bwcwkhgmkbc1h6jbmywl") (f (quote (("ignore-invalid-checksums"))))))

(define-public crate-opentype-0.27.0 (c (n "opentype") (v "0.27.0") (d (list (d (n "postscript") (r "^0.17.4") (d #t) (k 0)) (d (n "truetype") (r "^0.43.0") (d #t) (k 0)) (d (n "typeface") (r "^0") (d #t) (k 0)))) (h "1b4kbyw3kbjlq5izmayhw37igqjd1xf8ljv8n3xicb6bd71160n7") (f (quote (("ignore-invalid-checksums"))))))

(define-public crate-opentype-0.28.0 (c (n "opentype") (v "0.28.0") (d (list (d (n "postscript") (r "^0.17.4") (d #t) (k 0)) (d (n "truetype") (r "^0.43.0") (d #t) (k 0)) (d (n "typeface") (r "^0") (d #t) (k 0)))) (h "1bkv21li4ldh9b8zsm7xza6cnn4h5kihrccd9g39c8yqm9kkhybv") (f (quote (("ignore-invalid-checksums"))))))

(define-public crate-opentype-0.28.1 (c (n "opentype") (v "0.28.1") (d (list (d (n "postscript") (r "^0.17.4") (d #t) (k 0)) (d (n "truetype") (r "^0.43.0") (d #t) (k 0)) (d (n "typeface") (r "^0") (d #t) (k 0)))) (h "16qxnaiqqkz9pvlg0lqq98nyvz6h3y5p9sci4rvq3nka55zk7piy") (f (quote (("ignore-invalid-checksums"))))))

(define-public crate-opentype-0.28.2 (c (n "opentype") (v "0.28.2") (d (list (d (n "postscript") (r "^0.17.4") (d #t) (k 0)) (d (n "truetype") (r "^0.43.0") (d #t) (k 0)) (d (n "typeface") (r "^0") (d #t) (k 0)))) (h "0gq3ql7xd7wznbi1p4aa687w4mc1j9zivrw6yv795ys5fjr7figd") (f (quote (("ignore-invalid-checksums"))))))

(define-public crate-opentype-0.28.3 (c (n "opentype") (v "0.28.3") (d (list (d (n "postscript") (r "^0.17.4") (d #t) (k 0)) (d (n "truetype") (r "^0.43.0") (d #t) (k 0)) (d (n "typeface") (r "^0") (d #t) (k 0)))) (h "11vi27l13bqmz3h9xs5nxhx1w72klnl6jbb001lwi8i1q8nwj77d") (f (quote (("ignore-invalid-checksums"))))))

(define-public crate-opentype-0.28.4 (c (n "opentype") (v "0.28.4") (d (list (d (n "postscript") (r "^0.17.4") (d #t) (k 0)) (d (n "truetype") (r "^0.43.0") (d #t) (k 0)) (d (n "typeface") (r "^0") (d #t) (k 0)))) (h "1d4nxz5lzklbx1vg00jj07dc577p65abnvwhw7sgg70w64207ql3") (f (quote (("ignore-invalid-checksums"))))))

(define-public crate-opentype-0.29.0 (c (n "opentype") (v "0.29.0") (d (list (d (n "postscript") (r "^0.17.4") (d #t) (k 0)) (d (n "truetype") (r "^0.43.0") (d #t) (k 0)) (d (n "typeface") (r "^0") (d #t) (k 0)))) (h "1cif6yqhvdq9praqqfskkqbjyfr9myn4bkd5xzaji573m2hidwss") (f (quote (("ignore-invalid-checksums"))))))

(define-public crate-opentype-0.29.1 (c (n "opentype") (v "0.29.1") (d (list (d (n "postscript") (r "^0.17.4") (d #t) (k 0)) (d (n "truetype") (r "^0.43.0") (d #t) (k 0)) (d (n "typeface") (r "^0") (d #t) (k 0)))) (h "050144pqxqspz1i4br8vrava59jb4r8zsjcckhdhps2gd15jy28a") (f (quote (("ignore-invalid-checksums"))))))

(define-public crate-opentype-0.29.2 (c (n "opentype") (v "0.29.2") (d (list (d (n "postscript") (r "^0.17.4") (d #t) (k 0)) (d (n "truetype") (r "^0.43.0") (d #t) (k 0)) (d (n "typeface") (r "^0") (d #t) (k 0)))) (h "0f8kkgm2mkkrh4gh6f8n5hmy432ixidfzmvch03ng0lw15f6ypvb") (f (quote (("ignore-invalid-checksums"))))))

(define-public crate-opentype-0.29.3 (c (n "opentype") (v "0.29.3") (d (list (d (n "postscript") (r "^0.17.4") (d #t) (k 0)) (d (n "truetype") (r "^0.43.0") (d #t) (k 0)) (d (n "typeface") (r "^0") (d #t) (k 0)))) (h "18sqw3k1hyq97jslidj0s33jjfviq8mfmv0jy1jl3wcqbsxvz88f") (f (quote (("ignore-invalid-checksums") ("ignore-incomplete-directories")))) (y #t)))

(define-public crate-opentype-0.29.4 (c (n "opentype") (v "0.29.4") (d (list (d (n "postscript") (r "^0.17.4") (d #t) (k 0)) (d (n "truetype") (r "^0.43.0") (d #t) (k 0)) (d (n "typeface") (r "^0") (d #t) (k 0)))) (h "0glqcwbzd5r7f8s4c8yhfb09w72vs1z0162qpcvh9g94z4s7xcnm") (f (quote (("ignore-invalid-checksums") ("ignore-incomplete-directories"))))))

(define-public crate-opentype-0.29.5 (c (n "opentype") (v "0.29.5") (d (list (d (n "postscript") (r "^0.17.4") (d #t) (k 0)) (d (n "truetype") (r "^0.43.0") (d #t) (k 0)) (d (n "typeface") (r "^0") (d #t) (k 0)))) (h "05bdw163mfl8nyxa5qrdhipz1a3rfqi3vazx0bq7a073hbd5anc5") (f (quote (("ignore-invalid-checksums") ("ignore-incomplete-directories") ("ignore-empty-tables"))))))

(define-public crate-opentype-0.29.6 (c (n "opentype") (v "0.29.6") (d (list (d (n "postscript") (r "^0.17.4") (d #t) (k 0)) (d (n "truetype") (r "^0.43.0") (d #t) (k 0)) (d (n "typeface") (r "^0") (d #t) (k 0)))) (h "1mfq79pxffclfgclnkx0nciq6vn7acdv8c653g5nylm9lwnddv4k") (f (quote (("ignore-invalid-checksums") ("ignore-incomplete-marks") ("ignore-incomplete-directories") ("ignore-empty-tables"))))))

(define-public crate-opentype-0.29.7 (c (n "opentype") (v "0.29.7") (d (list (d (n "postscript") (r "^0.17.4") (d #t) (k 0)) (d (n "truetype") (r "^0.43.0") (d #t) (k 0)) (d (n "typeface") (r "^0") (d #t) (k 0)))) (h "0g5zv66s8xbf88hl09c0skfak9i19yxs28ss0rnyg1k8gkgsvh4r") (f (quote (("ignore-invalid-checksums") ("ignore-incomplete-tables") ("ignore-incomplete-marks") ("ignore-incomplete-directories"))))))

(define-public crate-opentype-0.29.8 (c (n "opentype") (v "0.29.8") (d (list (d (n "postscript") (r "^0.17.4") (d #t) (k 0)) (d (n "truetype") (r "^0.43.0") (d #t) (k 0)) (d (n "typeface") (r "^0") (d #t) (k 0)))) (h "07jmacw6vapq7h59l245951mhxfxqisycxbnhim75w8r8slxkfg9") (f (quote (("ignore-invalid-checksums") ("ignore-incomplete-marks") ("ignore-incomplete-directories"))))))

(define-public crate-opentype-0.30.0 (c (n "opentype") (v "0.30.0") (d (list (d (n "postscript") (r "^0.17.4") (d #t) (k 0)) (d (n "truetype") (r "^0.43.0") (d #t) (k 0)) (d (n "typeface") (r "^0") (d #t) (k 0)))) (h "1rd4z38fnsmjngdpynjkf4pb068g77fs2mi7qhgxjp7ciy3z77b2") (f (quote (("ignore-invalid-checksums") ("ignore-incomplete-marks") ("ignore-incomplete-directories"))))))

(define-public crate-opentype-0.31.0 (c (n "opentype") (v "0.31.0") (d (list (d (n "postscript") (r "^0.17.4") (d #t) (k 0)) (d (n "truetype") (r "^0.44.0") (d #t) (k 0)) (d (n "typeface") (r "^0") (d #t) (k 0)))) (h "1kbd6r6xh6jbvhsl6jf8yrffis6dxy356kl0jfd21mkmaj7yf039") (f (quote (("ignore-invalid-checksums") ("ignore-incomplete-marks") ("ignore-incomplete-directories"))))))

(define-public crate-opentype-0.31.1 (c (n "opentype") (v "0.31.1") (d (list (d (n "postscript") (r "^0.17.4") (d #t) (k 0)) (d (n "truetype") (r "^0.44.0") (d #t) (k 0)) (d (n "typeface") (r "^0") (d #t) (k 0)))) (h "06fl47sv45ga8qx8qgxr5b68cbjxxiicncw86xc8pjzngpi4jlj0") (f (quote (("ignore-invalid-checksums") ("ignore-incomplete-marks") ("ignore-incomplete-directories"))))))

(define-public crate-opentype-0.31.2 (c (n "opentype") (v "0.31.2") (d (list (d (n "postscript") (r "^0.17.4") (d #t) (k 0)) (d (n "truetype") (r "^0.44.0") (d #t) (k 0)) (d (n "typeface") (r "^0") (d #t) (k 0)))) (h "1sxrszp18gj3nqcvrdfr1mw239h85444733binxldjaf2w1bwidj") (f (quote (("ignore-invalid-checksums") ("ignore-incomplete-marks") ("ignore-incomplete-directories"))))))

(define-public crate-opentype-0.32.0 (c (n "opentype") (v "0.32.0") (d (list (d (n "postscript") (r "^0.18.0") (d #t) (k 0)) (d (n "truetype") (r "^0.45.0") (d #t) (k 0)) (d (n "typeface") (r "^0") (d #t) (k 0)))) (h "09h87556i4d887l69rjv4420j06q7cidnsd4rskgas163yxwj94y") (f (quote (("ignore-invalid-checksums") ("ignore-incomplete-marks") ("ignore-incomplete-directories"))))))

(define-public crate-opentype-0.32.1 (c (n "opentype") (v "0.32.1") (d (list (d (n "postscript") (r "^0.18.0") (d #t) (k 0)) (d (n "truetype") (r "^0.45.0") (d #t) (k 0)) (d (n "typeface") (r "^0.4.0") (d #t) (k 0)))) (h "1dh8c2fmrcman0q9x9v56k3ml32yqcn3jz1bxym6k4r3hcdbjq5w") (f (quote (("ignore-invalid-checksums") ("ignore-incomplete-marks") ("ignore-incomplete-directories"))))))

(define-public crate-opentype-0.33.0 (c (n "opentype") (v "0.33.0") (d (list (d (n "postscript") (r "^0.18.0") (d #t) (k 0)) (d (n "truetype") (r "^0.46.0") (d #t) (k 0)) (d (n "typeface") (r "^0.4.1") (d #t) (k 0)))) (h "0iql96jrp3whkzmk7w90jr8vpxvfm9w33kqzv8mfb7lg02v9vd33") (f (quote (("ignore-invalid-checksums") ("ignore-incomplete-marks") ("ignore-incomplete-directories"))))))

(define-public crate-opentype-0.34.0 (c (n "opentype") (v "0.34.0") (d (list (d (n "postscript") (r "^0.18.0") (d #t) (k 0)) (d (n "truetype") (r "^0.46.0") (d #t) (k 0)) (d (n "typeface") (r "^0.4.1") (d #t) (k 0)))) (h "0d58rr92j45n7y8g4ssr067ij5blqn4phh0mk3ig4x78bymp7n88") (f (quote (("ignore-invalid-checksums") ("ignore-incomplete-marks") ("ignore-incomplete-directories"))))))

(define-public crate-opentype-0.34.1 (c (n "opentype") (v "0.34.1") (d (list (d (n "postscript") (r "^0.18.0") (d #t) (k 0)) (d (n "truetype") (r "^0.46.0") (d #t) (k 0)) (d (n "typeface") (r "^0.4.1") (d #t) (k 0)))) (h "0fy1znvjmcymb3db92rrpmy7y3l2lq4ngma0ymddzib7alnnhrqm") (f (quote (("ignore-invalid-checksums") ("ignore-incomplete-marks") ("ignore-incomplete-directories"))))))

(define-public crate-opentype-0.34.2 (c (n "opentype") (v "0.34.2") (d (list (d (n "postscript") (r "^0.18.0") (d #t) (k 0)) (d (n "truetype") (r "^0.46.0") (d #t) (k 0)) (d (n "typeface") (r "^0.4.1") (d #t) (k 0)))) (h "05ji36nx1d6f8ghwm59jyid6jdbdgnpqc9hhax55h6in8z4683v8") (f (quote (("ignore-invalid-checksums") ("ignore-incomplete-marks") ("ignore-incomplete-directories"))))))

(define-public crate-opentype-0.34.3 (c (n "opentype") (v "0.34.3") (d (list (d (n "postscript") (r "^0.18.1") (d #t) (k 0)) (d (n "truetype") (r "^0.46.0") (d #t) (k 0)) (d (n "typeface") (r "^0.4.1") (d #t) (k 0)))) (h "0ig7c1yl9yyhbclkdd7clhdn4mqwl1fjk6byrzmqzjwahli5b2dy") (f (quote (("ignore-invalid-checksums") ("ignore-incomplete-marks") ("ignore-incomplete-directories"))))))

(define-public crate-opentype-0.35.0 (c (n "opentype") (v "0.35.0") (d (list (d (n "postscript") (r "^0.18.1") (d #t) (k 0)) (d (n "truetype") (r "^0.47.0") (d #t) (k 0)) (d (n "typeface") (r "^0.4.1") (d #t) (k 0)))) (h "1nhrhcwz54kmgxxvbmszrpljrjs468gfnyilpizp64br4f5vzzvx") (f (quote (("ignore-invalid-checksums") ("ignore-incomplete-marks") ("ignore-incomplete-directories"))))))

(define-public crate-opentype-0.35.1 (c (n "opentype") (v "0.35.1") (d (list (d (n "postscript") (r "^0.18.1") (d #t) (k 0)) (d (n "truetype") (r "^0.47.3") (d #t) (k 0)) (d (n "typeface") (r "^0.4.1") (d #t) (k 0)))) (h "0kqjz3z2625y37rzp8z780v8jg8s9czs1lagmi7fmr10pwmqdrr4") (f (quote (("ignore-invalid-checksums") ("ignore-incomplete-marks") ("ignore-incomplete-directories"))))))

(define-public crate-opentype-0.35.2 (c (n "opentype") (v "0.35.2") (d (list (d (n "postscript") (r "^0.18.2") (d #t) (k 0)) (d (n "truetype") (r "^0.47.5") (d #t) (k 0)) (d (n "typeface") (r "^0.4.2") (d #t) (k 0)))) (h "0lb00mq66wspmqbdvdnlrdi77m82f8ff4i375jbhgm4mcxz1nyjq") (f (quote (("ignore-invalid-checksums") ("ignore-incomplete-marks") ("ignore-incomplete-directories"))))))

(define-public crate-opentype-0.36.0 (c (n "opentype") (v "0.36.0") (d (list (d (n "postscript") (r "^0.18.2") (d #t) (k 0)) (d (n "truetype") (r "^0.47.5") (d #t) (k 0)) (d (n "typeface") (r "^0.4.2") (d #t) (k 0)))) (h "1530zljvap26cbgplqsnsnwvrn72mbkc9a0skazwlhla5r4nvfc0") (f (quote (("ignore-invalid-checksums") ("ignore-incomplete-marks") ("ignore-incomplete-directories"))))))

(define-public crate-opentype-0.36.1 (c (n "opentype") (v "0.36.1") (d (list (d (n "postscript") (r "^0.18.2") (d #t) (k 0)) (d (n "truetype") (r "^0.47.5") (d #t) (k 0)) (d (n "typeface") (r "^0.4.2") (d #t) (k 0)))) (h "145x1a1z5nkdsvzmwdd19vilf977x9p1z1lkbh57wxcnmdvfqi58") (f (quote (("ignore-invalid-checksums") ("ignore-incomplete-marks") ("ignore-incomplete-directories") ("default-language"))))))

(define-public crate-opentype-0.37.0 (c (n "opentype") (v "0.37.0") (d (list (d (n "postscript") (r "^0.18.2") (d #t) (k 0)) (d (n "truetype") (r "^0.47.5") (d #t) (k 0)) (d (n "typeface") (r "^0.4.2") (d #t) (k 0)))) (h "117l0bh5kpykl1gbpkjbrl4zxbfvcz41fpwjgfj70zs2m9c2g5qh") (f (quote (("ignore-invalid-checksums") ("ignore-incomplete-marks") ("ignore-incomplete-directories") ("default-language")))) (y #t)))

(define-public crate-opentype-0.37.1 (c (n "opentype") (v "0.37.1") (d (list (d (n "postscript") (r "^0.18.2") (d #t) (k 0)) (d (n "truetype") (r "^0.47.5") (d #t) (k 0)) (d (n "typeface") (r "^0.4.2") (d #t) (k 0)))) (h "1ckbd5jw7p24nbfj9aky6w5fs0cb9nc1hgz4n7vjr3x14ypx0ydf") (f (quote (("ignore-invalid-checksums") ("ignore-incomplete-marks") ("ignore-incomplete-directories") ("default-language"))))))

