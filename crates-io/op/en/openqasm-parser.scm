(define-module (crates-io op en openqasm-parser) #:use-module (crates-io))

(define-public crate-openqasm-parser-0.1.0 (c (n "openqasm-parser") (v "0.1.0") (h "1yjplvc504vmvvfxicmrk64p1s2xrfnl7hg8q0i389k4z2s4k8vx")))

(define-public crate-openqasm-parser-0.1.1 (c (n "openqasm-parser") (v "0.1.1") (h "0dcmwdvv0js4ca7mf250vb5bwgk2yjavym262g1a5nhzqas2z60c")))

(define-public crate-openqasm-parser-1.0.0 (c (n "openqasm-parser") (v "1.0.0") (h "18vxlb0bh56z04hdms2al01x0fz7qfcmkq82k7wwm594jfc43vcd")))

