(define-module (crates-io op en openpty) #:use-module (crates-io))

(define-public crate-openpty-0.1.0 (c (n "openpty") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.49") (d #t) (k 0)) (d (n "nix") (r "^0.13.0") (d #t) (k 0)))) (h "0dgbgj004irxv1lndrlaj828v6qw75znl61mjym723gqwjlfz9a2")))

(define-public crate-openpty-0.1.1 (c (n "openpty") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.66") (d #t) (k 0)) (d (n "nix") (r "^0.16.1") (d #t) (k 0)))) (h "02px67drxv90f5z2d4flys3a9zl3xckwwcm77mxk4qbhakk3ixj0")))

(define-public crate-openpty-0.1.2 (c (n "openpty") (v "0.1.2") (d (list (d (n "libc") (r "^0.2.66") (d #t) (k 0)) (d (n "nix") (r "^0.16.1") (d #t) (k 0)))) (h "1724wk8zaq1yjs8xxggzzcgsr15xi6qhpvlpzyxsldk36r2v2iws")))

(define-public crate-openpty-0.2.0 (c (n "openpty") (v "0.2.0") (d (list (d (n "libc") (r "^0.2.66") (d #t) (k 0)) (d (n "nix") (r "^0.23") (d #t) (k 0)))) (h "16ikf3dy7cvqd0g314lp1v4qb7acp9rp2jp40yw6h74gnk35gzyj")))

