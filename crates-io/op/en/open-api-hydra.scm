(define-module (crates-io op en open-api-hydra) #:use-module (crates-io))

(define-public crate-open-api-hydra-1.0.0 (c (n "open-api-hydra") (v "1.0.0") (d (list (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^1.5") (d #t) (k 0)))) (h "1v1qif3awvqgds9bahb3f75gbvby9zwagxwrycfhcinydhrjqgg9")))

