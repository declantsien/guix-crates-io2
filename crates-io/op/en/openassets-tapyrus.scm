(define-module (crates-io op en openassets-tapyrus) #:use-module (crates-io))

(define-public crate-openassets-tapyrus-0.2.2 (c (n "openassets-tapyrus") (v "0.2.2") (d (list (d (n "bitcoin_hashes") (r "^0.7") (d #t) (k 0)) (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "leb128") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tapyrus") (r "^0.4.1") (f (quote ("use-serde"))) (d #t) (k 0)))) (h "1skv2zj5m8bgja7zv8fav8sxi3zc8jpsbxy7367n2cz8jj9wgfca")))

(define-public crate-openassets-tapyrus-0.2.3 (c (n "openassets-tapyrus") (v "0.2.3") (d (list (d (n "bitcoin_hashes") (r "^0.7") (d #t) (k 0)) (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "leb128") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tapyrus") (r "^0.4.2") (f (quote ("use-serde"))) (d #t) (k 0)))) (h "1cv8w565xs1vd6qxwqydlbzd33i6vnz79mgfnldr2xqfii3h3w68")))

(define-public crate-openassets-tapyrus-0.2.4 (c (n "openassets-tapyrus") (v "0.2.4") (d (list (d (n "bitcoin_hashes") (r "^0.7") (d #t) (k 0)) (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "leb128") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tapyrus") (r "^0.4.2") (f (quote ("use-serde"))) (d #t) (k 0)))) (h "0y1q8bzrgw19k4ldlcpi3914rac9p7bvwhvxa3l51wyc3ya4nxqq")))

