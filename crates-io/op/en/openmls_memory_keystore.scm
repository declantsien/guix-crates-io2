(define-module (crates-io op en openmls_memory_keystore) #:use-module (crates-io))

(define-public crate-openmls_memory_keystore-0.1.0-pre.1 (c (n "openmls_memory_keystore") (v "0.1.0-pre.1") (d (list (d (n "openmls_traits") (r "^0.1.0-pre.1") (d #t) (k 0)))) (h "0xwgj8q61g76aqrh4m9hr8hwrw9zdwxpdpgizaclgkfprw8nlflg")))

(define-public crate-openmls_memory_keystore-0.1.0-pre.2 (c (n "openmls_memory_keystore") (v "0.1.0-pre.2") (d (list (d (n "openmls_traits") (r "^0.1.0-pre.2") (d #t) (k 0)))) (h "1chic7v5l3ilz30qk826fjmpwa9xc791y285plgswjxzj0ki2wyb")))

(define-public crate-openmls_memory_keystore-0.1.0-pre.3 (c (n "openmls_memory_keystore") (v "0.1.0-pre.3") (d (list (d (n "openmls_traits") (r "^0.1.0-pre.3") (d #t) (k 0)))) (h "1v4jshl2rlljf7pcd2lawq1237zh4qfjqgl0csx8g950i0icpqfj")))

(define-public crate-openmls_memory_keystore-0.1.0-pre.4 (c (n "openmls_memory_keystore") (v "0.1.0-pre.4") (d (list (d (n "openmls_traits") (r "^0.1.0-pre.4") (d #t) (k 0)))) (h "180vvkxkflg9c20dykr4xxi34mdc004wrvrqbwk0s347534jdvf8")))

(define-public crate-openmls_memory_keystore-0.1.0 (c (n "openmls_memory_keystore") (v "0.1.0") (d (list (d (n "openmls_traits") (r "^0.1.0") (d #t) (k 0)))) (h "1naphfz0g1x6y0zb4gx48crdq1i3sm74g2zk5sy7khv39dv03s0k")))

(define-public crate-openmls_memory_keystore-0.2.0-pre.0 (c (n "openmls_memory_keystore") (v "0.2.0-pre.0") (d (list (d (n "openmls_traits") (r "^0.2.0-pre.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1r10qvyivg2049n5gzybk34xbls81nzqj687ifdchf88mqij4giy")))

(define-public crate-openmls_memory_keystore-0.2.0-pre.1 (c (n "openmls_memory_keystore") (v "0.2.0-pre.1") (d (list (d (n "openmls_traits") (r "^0.2.0-pre.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1bakdpml3f5nvly5m6jd8pq4mc41acz70pqz5g3isl6dsj7ikw3a")))

(define-public crate-openmls_memory_keystore-0.2.0-pre.2 (c (n "openmls_memory_keystore") (v "0.2.0-pre.2") (d (list (d (n "openmls_traits") (r "^0.2.0-pre.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1ysfgr0p7k1gz8mxv437nsgsl54ahz1hjqmg3iflsy4d5wvf71hm")))

(define-public crate-openmls_memory_keystore-0.2.0-pre.3 (c (n "openmls_memory_keystore") (v "0.2.0-pre.3") (d (list (d (n "openmls_traits") (r "^0.2.0-pre.3") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1rpwv6dbvapanp6jkz907hdfwhpx9f26d2riznwbw365ss6884hr")))

(define-public crate-openmls_memory_keystore-0.2.0 (c (n "openmls_memory_keystore") (v "0.2.0") (d (list (d (n "openmls_traits") (r "^0.2.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "17ii12zs9blcl09pki7sx8vial18yqj7dh61cacz4g0x9b9zwchm")))

