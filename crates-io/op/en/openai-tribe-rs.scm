(define-module (crates-io op en openai-tribe-rs) #:use-module (crates-io))

(define-public crate-openai-tribe-rs-1.1.0 (c (n "openai-tribe-rs") (v "1.1.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_with") (r "^2.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)) (d (n "uuid") (r "^1.0") (f (quote ("serde"))) (d #t) (k 0)))) (h "09r7hh1ychas47pdg3fj23n718g59a1dsmh8p3qvcfczx36pff39")))

