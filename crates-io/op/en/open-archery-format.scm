(define-module (crates-io op en open-archery-format) #:use-module (crates-io))

(define-public crate-open-archery-format-0.1.0 (c (n "open-archery-format") (v "0.1.0") (h "1krs5bn2vxmk8yl8a0hy2s75mllmncba6x69z7rasb7qb4ddq1kq")))

(define-public crate-open-archery-format-0.1.1 (c (n "open-archery-format") (v "0.1.1") (h "14miig14g78fv1g2nmx9j0jf021360mgmn0d1qygkl5z7f9y9k6i")))

