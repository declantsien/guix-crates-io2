(define-module (crates-io op en openbabel-sys) #:use-module (crates-io))

(define-public crate-openbabel-sys-0.1.0+openbabel-3.1.1 (c (n "openbabel-sys") (v "0.1.0+openbabel-3.1.1") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 1)))) (h "0c01lbcbayigiqwcm3ncmq4galq1v7byxdb7gwg8zj0jvkn27byw")))

(define-public crate-openbabel-sys-0.1.1+openbabel-3.1.1 (c (n "openbabel-sys") (v "0.1.1+openbabel-3.1.1") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 1)))) (h "0wvwx7pny0fqp60sjwvmfba3b5qdzsx8bjzflmly1g1byhdn2yrl")))

(define-public crate-openbabel-sys-0.1.2+openbabel-3.1.1 (c (n "openbabel-sys") (v "0.1.2+openbabel-3.1.1") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 1)))) (h "1y8q94fl3n2yd06xazggh7k602sp5hidkglmmljpmli6yyhqrlkn")))

(define-public crate-openbabel-sys-0.1.3+openbabel-3.1.1 (c (n "openbabel-sys") (v "0.1.3+openbabel-3.1.1") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 1)))) (h "0hgl7pn9z80yyk3fnhhr6jpf9ky8rvlk383krbn0ilqrqnswyph8")))

(define-public crate-openbabel-sys-0.1.4+openbabel-3.1.1 (c (n "openbabel-sys") (v "0.1.4+openbabel-3.1.1") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 1)))) (h "0idysyfmq7h0qac7lgf17hznf1rrs6bq5h0db2sgfjkihzjvjn3l")))

(define-public crate-openbabel-sys-0.1.5+openbabel-3.1.1 (c (n "openbabel-sys") (v "0.1.5+openbabel-3.1.1") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 1)))) (h "0s4wpsmbmc7yranhsajbv8k1dcaykxszc1znkpadvxs8k6f1bxd6")))

(define-public crate-openbabel-sys-0.1.6+openbabel-3.1.1 (c (n "openbabel-sys") (v "0.1.6+openbabel-3.1.1") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 1)))) (h "12ciaxp5qzyldgy8r832qmr82y4nr4p3a8id1p7x1vlf55wk0f3f")))

(define-public crate-openbabel-sys-0.1.7+openbabel-3.1.1 (c (n "openbabel-sys") (v "0.1.7+openbabel-3.1.1") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 1)))) (h "10awv2ab1pas8mimpy0x2pa3p4y7hw0nfky87b5abmqgz46d8vw9")))

(define-public crate-openbabel-sys-0.1.8+openbabel-3.1.1 (c (n "openbabel-sys") (v "0.1.8+openbabel-3.1.1") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 1)))) (h "1gyxr0jyqkj3k38g8rspxm4gsls15hk6d40r80g5gkvz790jalnx")))

(define-public crate-openbabel-sys-0.4.0+openbabel-3.1.1 (c (n "openbabel-sys") (v "0.4.0+openbabel-3.1.1") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 1)))) (h "0j2g19s6v8shw92d0dcaa205dzmnavxbsj15dgsgbgzcr51isbbx")))

(define-public crate-openbabel-sys-0.5.0+openbabel-3.1.1 (c (n "openbabel-sys") (v "0.5.0+openbabel-3.1.1") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 1)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)))) (h "1jr5qa20g7zpr39lqp3bbrmzh6fir6ggmwapr106lcn366hw0q95")))

(define-public crate-openbabel-sys-0.5.3+openbabel-3.1.1 (c (n "openbabel-sys") (v "0.5.3+openbabel-3.1.1") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 1)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)))) (h "03p6mx40cbkcn64x83siy1cxisj0z08lm6lpmzqmzyd3wayzm9cc")))

