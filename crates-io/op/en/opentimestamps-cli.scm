(define-module (crates-io op en opentimestamps-cli) #:use-module (crates-io))

(define-public crate-opentimestamps-cli-0.1.0 (c (n "opentimestamps-cli") (v "0.1.0") (h "1xhhkglfwmm743rclsiyf58yg2x78bmw7j10yhmkmlh0wni67m4g")))

(define-public crate-opentimestamps-cli-0.2.0 (c (n "opentimestamps-cli") (v "0.2.0") (d (list (d (n "bitcoin_hashes") (r "^0.12.0") (d #t) (k 0)) (d (n "bitcoincore-rpc") (r "^0.18.0") (d #t) (k 0)) (d (n "camino") (r "^1.1.6") (d #t) (k 0)) (d (n "chrono") (r "^0.4.37") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.8.6") (d #t) (k 0)) (d (n "clap") (r "~4.4") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "clap_complete") (r "~4.4") (d #t) (k 0)) (d (n "electrum-client") (r "^0.19.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "ots") (r "^0.1.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.12") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "rs_merkle") (r "^1.4") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "09z48ppjf8r9hyvin2k14ik9wr1hvc1scypnn7cn5k20r1k5zjhy")))

