(define-module (crates-io op en open-oak) #:use-module (crates-io))

(define-public crate-open-oak-0.1.0 (c (n "open-oak") (v "0.1.0") (d (list (d (n "cgmath") (r "^0.18.0") (d #t) (k 0)) (d (n "doc-comment") (r "^0.3.3") (d #t) (k 0)) (d (n "glium") (r "^0.32.1") (d #t) (k 0)) (d (n "image") (r "^0.24.5") (d #t) (k 0)) (d (n "uuid") (r "^1.2.2") (f (quote ("v4" "fast-rng" "macro-diagnostics"))) (d #t) (k 0)))) (h "0c5x9j40x4ar2jmcz0mmpakq5fj5wyrd671sprnkvpl33nc47dcp")))

(define-public crate-open-oak-0.1.1 (c (n "open-oak") (v "0.1.1") (d (list (d (n "cgmath") (r "^0.18.0") (d #t) (k 0)) (d (n "doc-comment") (r "^0.3.3") (d #t) (k 0)) (d (n "glium") (r "^0.32.1") (d #t) (k 0)) (d (n "image") (r "^0.24.5") (d #t) (k 0)) (d (n "uuid") (r "^1.2.2") (f (quote ("v4" "fast-rng" "macro-diagnostics"))) (d #t) (k 0)))) (h "1f6wa2fj8dw4axhg06gxgwzb038cqms6j1lygg1b5rmlpk197vkq")))

(define-public crate-open-oak-0.1.2 (c (n "open-oak") (v "0.1.2") (d (list (d (n "cgmath") (r "^0.18.0") (d #t) (k 0)) (d (n "doc-comment") (r "^0.3.3") (d #t) (k 0)) (d (n "glium") (r "^0.32.1") (d #t) (k 0)) (d (n "image") (r "^0.24.5") (d #t) (k 0)) (d (n "uuid") (r "^1.2.2") (f (quote ("v4" "fast-rng" "macro-diagnostics"))) (d #t) (k 0)))) (h "1cnr6f6lgfi97kfahxa9lmhy10r7rhpi6bbm3j5wsz6a18rbif1p")))

(define-public crate-open-oak-0.1.3 (c (n "open-oak") (v "0.1.3") (d (list (d (n "cgmath") (r "^0.18.0") (d #t) (k 0)) (d (n "doc-comment") (r "^0.3.3") (d #t) (k 0)) (d (n "glium") (r "^0.32.1") (d #t) (k 0)) (d (n "image") (r "^0.24.5") (d #t) (k 0)) (d (n "uuid") (r "^1.2.2") (f (quote ("v4" "fast-rng" "macro-diagnostics"))) (d #t) (k 0)))) (h "18h5b7afa3yzfd93xg4d89dc938fpq01ppp7ak6ylppc4xd4b9f8")))

