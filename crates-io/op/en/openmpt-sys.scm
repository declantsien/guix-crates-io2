(define-module (crates-io op en openmpt-sys) #:use-module (crates-io))

(define-public crate-openmpt-sys-0.1.0 (c (n "openmpt-sys") (v "0.1.0") (h "1yadkhsxg4vpjnjjimdclkl1p1rf82cj99kr6sj93y0kwxkx7dik")))

(define-public crate-openmpt-sys-0.2.0 (c (n "openmpt-sys") (v "0.2.0") (h "0cs98q7qvn3dir183rcqqb65bh68g1fmza24smm46mal71h07llb") (l "openmpt")))

