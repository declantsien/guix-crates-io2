(define-module (crates-io op en opengles) #:use-module (crates-io))

(define-public crate-opengles-0.1.0 (c (n "opengles") (v "0.1.0") (d (list (d (n "khronos") (r "^0.1.1") (d #t) (k 0)) (d (n "libc") (r "^0.1.10") (d #t) (k 0)))) (h "01hf1w50ds0ynqhrfpbmpz0m0d1ffnp8ln8x6yirf79qvkzi67ja")))

(define-public crate-opengles-0.1.1 (c (n "opengles") (v "0.1.1") (d (list (d (n "khronos") (r "^0.1.1") (d #t) (k 0)) (d (n "libc") (r "^0.1.10") (d #t) (k 0)))) (h "07nnlgq2qq7fiagwgi7xs314h5zijciq09nmz4rs92vp962b3aky")))

(define-public crate-opengles-0.1.2 (c (n "opengles") (v "0.1.2") (d (list (d (n "khronos") (r "^0.1.1") (d #t) (k 0)) (d (n "libc") (r "^0.1.10") (d #t) (k 0)))) (h "0g7s6ysv5gd4hgs2p61hnk1ic5qkd2c3zv7gnlsyvh7d18pg7igr")))

