(define-module (crates-io op en openvpn3-rs) #:use-module (crates-io))

(define-public crate-openvpn3-rs-0.0.1 (c (n "openvpn3-rs") (v "0.0.1") (d (list (d (n "async-std") (r "^1.12.0") (d #t) (k 0)) (d (n "enumflags2") (r "^0.7.5") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.25") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "serde_repr") (r "^0.1.10") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)) (d (n "zbus") (r "^3.6.2") (d #t) (k 0)))) (h "0frs78dgi3wx0cxlb4lgxa09qpxj5r2qq5vxhig90h6a88hxbhk2")))

(define-public crate-openvpn3-rs-0.0.2 (c (n "openvpn3-rs") (v "0.0.2") (d (list (d (n "async-std") (r "^1.12.0") (d #t) (k 0)) (d (n "enumflags2") (r "^0.7.5") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.25") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "serde_repr") (r "^0.1.10") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)) (d (n "zbus") (r "^3.6.2") (d #t) (k 0)))) (h "18q2pnnz058x3asp6pcx8hdv043i0i3rf4jp7hnn9hs26f53n8wn")))

