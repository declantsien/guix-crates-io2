(define-module (crates-io op en opencc-sys) #:use-module (crates-io))

(define-public crate-opencc-sys-0.1.0+1.1.5 (c (n "opencc-sys") (v "0.1.0+1.1.5") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1.49") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1jh1kj1ybzwa5jwnxqh2pqcc80bpc2yl7lxyj4p80gagn98i57r5") (l "opencc")))

(define-public crate-opencc-sys-0.1.2+1.1.5 (c (n "opencc-sys") (v "0.1.2+1.1.5") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1.49") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0gbnfyw270m0vby631axaw7k0f520qg3c194fqa0xjgmm6vdkb81") (l "opencc")))

(define-public crate-opencc-sys-0.1.3+1.1.5 (c (n "opencc-sys") (v "0.1.3+1.1.5") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1.49") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "05pzxj7c9rrpvz3rblxs8817dyxh5jj7flhw4rr07p2947cl4brw") (l "opencc")))

(define-public crate-opencc-sys-0.1.4+1.1.5 (c (n "opencc-sys") (v "0.1.4+1.1.5") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1.49") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0crw067w61a3wvnz43lajz8279d08f3aijpvvvyfxsy0cs9w86pr") (l "opencc")))

(define-public crate-opencc-sys-0.1.4+1.1.6 (c (n "opencc-sys") (v "0.1.4+1.1.6") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1.49") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1d73sra0hql74m9lvzh9faddj4y0k02h2p444knkndsi27766xhz") (l "opencc")))

(define-public crate-opencc-sys-0.1.5+1.1.6 (c (n "opencc-sys") (v "0.1.5+1.1.6") (d (list (d (n "bindgen") (r "^0.63.0") (f (quote ("runtime" "which-rustfmt"))) (k 1)) (d (n "cmake") (r "^0.1.49") (d #t) (k 1)) (d (n "link-cplusplus") (r "^1.0.8") (d #t) (k 0)) (d (n "once_cell") (r "^1.16.0") (f (quote ("parking_lot"))) (d #t) (k 0)))) (h "06vcz21r31i6lam1n662h1lg2d4c6j4dkm8837zj8jiwfvdwy784") (l "opencc")))

(define-public crate-opencc-sys-0.1.6+1.1.6 (c (n "opencc-sys") (v "0.1.6+1.1.6") (d (list (d (n "bindgen") (r "^0.63.0") (f (quote ("runtime" "which-rustfmt"))) (k 1)) (d (n "cmake") (r "^0.1.49") (d #t) (k 1)) (d (n "link-cplusplus") (r "^1.0.8") (d #t) (k 0)) (d (n "once_cell") (r "^1.16.0") (f (quote ("parking_lot"))) (d #t) (k 0)))) (h "11z7g5mz0pvh3ijhx788ggc84ah6hy3l7y7liqvydannm7a55790") (l "opencc")))

(define-public crate-opencc-sys-0.1.7+1.1.6 (c (n "opencc-sys") (v "0.1.7+1.1.6") (d (list (d (n "bindgen") (r "^0.64.0") (f (quote ("runtime" "which-rustfmt"))) (k 1)) (d (n "cmake") (r "^0.1.49") (k 1)) (d (n "link-cplusplus") (r "^1.0.8") (k 0)) (d (n "once_cell") (r "^1.17.1") (f (quote ("std" "parking_lot"))) (k 0)))) (h "02jqfm0xwa38x7g1s1bxl6sqdy7fgnjsrfp9lha2ffgna16k8rcc") (l "opencc")))

(define-public crate-opencc-sys-0.1.8+1.1.6 (c (n "opencc-sys") (v "0.1.8+1.1.6") (d (list (d (n "bindgen") (r "^0.66.1") (f (quote ("runtime" "which-rustfmt"))) (k 1)) (d (n "cmake") (r "^0.1.50") (k 1)) (d (n "link-cplusplus") (r "^1.0.9") (k 0)) (d (n "once_cell") (r "^1.18.0") (f (quote ("std" "parking_lot"))) (k 0)))) (h "164n5ljkak4ka41a40cbvnamy7w98vlnhhy4mkpzrnjrnagjrlpi") (l "opencc") (r "1.60")))

(define-public crate-opencc-sys-0.1.9+1.1.6 (c (n "opencc-sys") (v "0.1.9+1.1.6") (d (list (d (n "bindgen") (r "^0.66.1") (f (quote ("runtime" "which-rustfmt"))) (k 1)) (d (n "cmake") (r "^0.1.50") (k 1)) (d (n "link-cplusplus") (r "^1.0.9") (k 0)) (d (n "once_cell") (r "^1.18.0") (f (quote ("std"))) (k 0)))) (h "0zd6d7srs8v85ajz4mkcfbnwyhr9m1s55d27z471gg6zkppm83bw") (l "opencc") (r "1.60")))

(define-public crate-opencc-sys-0.2.0+1.1.7 (c (n "opencc-sys") (v "0.2.0+1.1.7") (d (list (d (n "bindgen") (r "^0.69.4") (f (quote ("logging" "prettyplease" "runtime" "which-rustfmt"))) (k 1)) (d (n "cmake") (r "^0.1.50") (k 1)) (d (n "link-cplusplus") (r "^1.0.9") (k 0)) (d (n "once_cell") (r "^1.19.0") (f (quote ("std"))) (k 0)))) (h "19nhr77hq0ic5qjj0vxiqkkzmzj1xiirza4pa6clnd2h4h2wdnr5") (l "opencc") (r "1.60")))

