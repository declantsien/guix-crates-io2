(define-module (crates-io op en opentracing-rs-jaeger) #:use-module (crates-io))

(define-public crate-opentracing-rs-jaeger-0.1.0 (c (n "opentracing-rs-jaeger") (v "0.1.0") (d (list (d (n "futures") (r "^0.1.25") (d #t) (k 0)) (d (n "opentracing-rs-core") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)))) (h "0h67brf3mcsq6i4i570raxpy91mii1zz5qrxnzbw3zi308c93rir")))

