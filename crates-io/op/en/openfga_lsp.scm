(define-module (crates-io op en openfga_lsp) #:use-module (crates-io))

(define-public crate-openfga_lsp-0.2.0 (c (n "openfga_lsp") (v "0.2.0") (d (list (d (n "dashmap") (r "^5.4.0") (d #t) (k 0)) (d (n "openfga_checker") (r "^0.2.0") (d #t) (k 0)) (d (n "openfga_common") (r "^0.2.0") (d #t) (k 0)) (d (n "openfga_model_dsl_parser") (r "^0.1.1") (d #t) (k 0)) (d (n "ropey") (r "^1.5.0") (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("full"))) (d #t) (k 0)) (d (n "tower-lsp") (r "^0.17.0") (d #t) (k 0)))) (h "0y3x4sa4s923mzig4456vjl4bhyylvqi2ikxm3h08qdjg12q94wr")))

