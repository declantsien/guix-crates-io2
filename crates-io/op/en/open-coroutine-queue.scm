(define-module (crates-io op en open-coroutine-queue) #:use-module (crates-io))

(define-public crate-open-coroutine-queue-0.1.2 (c (n "open-coroutine-queue") (v "0.1.2") (d (list (d (n "crossbeam-deque") (r "^0.8.2") (d #t) (k 0)) (d (n "num_cpus") (r "^1.14.0") (d #t) (k 0)) (d (n "st3") (r "^0.4.1") (d #t) (k 0)))) (h "1fkx0970hvc3fqjpgwpz6cl4wjfnfq2r12n3m2yh4jyhkw45kqfs")))

(define-public crate-open-coroutine-queue-0.4.0 (c (n "open-coroutine-queue") (v "0.4.0") (d (list (d (n "crossbeam-deque") (r "^0.8.2") (d #t) (k 0)) (d (n "num_cpus") (r "^1.14.0") (d #t) (k 0)) (d (n "st3") (r "^0.4.1") (d #t) (k 0)))) (h "1yixjbfrili4w9640xl16wihljhlyi7x6ksba5wg5yg613nwr3ai")))

(define-public crate-open-coroutine-queue-0.5.0 (c (n "open-coroutine-queue") (v "0.5.0") (d (list (d (n "crossbeam-deque") (r "^0.8.3") (d #t) (k 0)) (d (n "num_cpus") (r "^1.16.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "st3") (r "^0.4.1") (d #t) (k 0)))) (h "0g4i890kf1a1kwa5m4jrpjjg9ba830jwa81s35d99hagcw42gfcv")))

