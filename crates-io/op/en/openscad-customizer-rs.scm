(define-module (crates-io op en openscad-customizer-rs) #:use-module (crates-io))

(define-public crate-openscad-customizer-rs-0.1.0 (c (n "openscad-customizer-rs") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "14lsd9bfd50b9fms87h8p4zrysfvff4jy8xsjg2xlr5lq6ap3vrn")))

(define-public crate-openscad-customizer-rs-0.1.1 (c (n "openscad-customizer-rs") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1dxq7h3031rfc7d745sq5xad8iz4f69wfm1mk5mzkfipcfsg0v36")))

(define-public crate-openscad-customizer-rs-0.1.2 (c (n "openscad-customizer-rs") (v "0.1.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1dcghj9cjb89954isfnmhy5lbf371dj8c25argn8pl5n7aiassmv")))

