(define-module (crates-io op en open_ui) #:use-module (crates-io))

(define-public crate-open_ui-1.1.1 (c (n "open_ui") (v "1.1.1") (d (list (d (n "glium") (r "^0.31.0") (d #t) (k 0)))) (h "0zld9f3r55lplxyik7dx6sx24v1l4pjklw5baaqs6110d0mxlkiz")))

(define-public crate-open_ui-1.2.0 (c (n "open_ui") (v "1.2.0") (d (list (d (n "glium") (r "^0.31.0") (d #t) (k 0)))) (h "1l5yi2ai7icf339sm3y9lm0vkk3046jpmzrjmazp7bg3nnz3h9r9")))

(define-public crate-open_ui-1.3.0 (c (n "open_ui") (v "1.3.0") (d (list (d (n "glium") (r "^0.31.0") (d #t) (k 0)))) (h "149nlkm99445nn5057gm19w7vjwqvwvjmb6zp3cnlwfb8ff9sl1n")))

(define-public crate-open_ui-1.3.1 (c (n "open_ui") (v "1.3.1") (d (list (d (n "glium") (r "^0.31.0") (d #t) (k 0)))) (h "16kdcahala84bjjsfhrra836c2hk74d36sj90g908x728zg35md4")))

(define-public crate-open_ui-1.4.0 (c (n "open_ui") (v "1.4.0") (d (list (d (n "glium") (r "^0.31.0") (d #t) (k 0)))) (h "02ql5qzsn1n6svx6yfqwk260gl8mx0sp5s769rs14ibsbhrlkidx")))

