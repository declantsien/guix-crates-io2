(define-module (crates-io op en openvino) #:use-module (crates-io))

(define-public crate-openvino-0.1.0 (c (n "openvino") (v "0.1.0") (d (list (d (n "openvino-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "07c4z3hn5k6zrd5bzbmbjp13p4f9rqf7w4lj2wqzkmwcrnayqdb8")))

(define-public crate-openvino-0.1.1 (c (n "openvino") (v "0.1.1") (d (list (d (n "openvino-sys") (r "^0.1.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "1ck7cyy5rdxgdhnjdz4b9v24sbk5f7yvfkm1i8n2sxd57f1pvm9s")))

(define-public crate-openvino-0.1.2 (c (n "openvino") (v "0.1.2") (d (list (d (n "openvino-sys") (r "^0.1.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "16dmh3vybiradxvax1kd8yp1r30kpb21zy61cxhpr5gpn9zamqb0")))

(define-public crate-openvino-0.1.3 (c (n "openvino") (v "0.1.3") (d (list (d (n "openvino-sys") (r "^0.1.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "18d7q006vp74k58g6a41zd1wpanppjqsw714hbf97nvzi3ls4xz9")))

(define-public crate-openvino-0.1.4 (c (n "openvino") (v "0.1.4") (d (list (d (n "openvino-sys") (r "^0.1.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "0sjbyw12k0ql03ijvhxcydi8d3zn5ijn82f7lvp6lz006p3a217r")))

(define-public crate-openvino-0.1.5 (c (n "openvino") (v "0.1.5") (d (list (d (n "openvino-sys") (r "^0.1.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "0a7hc3wmq66rwxzry2bfvhnakg9y99rglbxdwd9l24vzy284z9w7")))

(define-public crate-openvino-0.1.8 (c (n "openvino") (v "0.1.8") (d (list (d (n "float-cmp") (r "^0.8") (d #t) (k 2)) (d (n "openvino-sys") (r "^0.1.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "044lbqg017paxpk15s2jskdf28l6jqrfqaxr28h8xkmphm1b9vj3")))

(define-public crate-openvino-0.2.0 (c (n "openvino") (v "0.2.0") (d (list (d (n "float-cmp") (r "^0.8") (d #t) (k 2)) (d (n "openvino-sys") (r "^0.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "1klc5skzw3vmj9z70lq2xq2636x68pdsa6zd3591yxys3s1jbl17")))

(define-public crate-openvino-0.3.0 (c (n "openvino") (v "0.3.0") (d (list (d (n "float-cmp") (r "^0.8") (d #t) (k 2)) (d (n "openvino-sys") (r "^0.3.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "0bb3glw9r4021b1jr0y48pj65i3lgxcdvbiakf8kjkfmrfz562zg") (f (quote (("runtime-linking" "openvino-sys/runtime-linking"))))))

(define-public crate-openvino-0.3.1 (c (n "openvino") (v "0.3.1") (d (list (d (n "float-cmp") (r "^0.8") (d #t) (k 2)) (d (n "openvino-sys") (r "^0.3.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "0xnf9avyniavh3js7lr8njh3wdp6579r995x52cplgv5ihylpdqc") (f (quote (("runtime-linking" "openvino-sys/runtime-linking"))))))

(define-public crate-openvino-0.3.2 (c (n "openvino") (v "0.3.2") (d (list (d (n "float-cmp") (r "^0.8") (d #t) (k 2)) (d (n "openvino-sys") (r "^0.3.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "08gqsb8y3yszildfbzdly575qh3ijirqjvl7m2qyvq0kfpdzzggm") (f (quote (("runtime-linking" "openvino-sys/runtime-linking"))))))

(define-public crate-openvino-0.3.3 (c (n "openvino") (v "0.3.3") (d (list (d (n "float-cmp") (r "^0.8") (d #t) (k 2)) (d (n "openvino-sys") (r "^0.3.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "0q9sx7gval1qc32k344shmcxk0bg5x374dvxp64kig8z3x7hqrv1") (f (quote (("runtime-linking" "openvino-sys/runtime-linking"))))))

(define-public crate-openvino-0.4.0 (c (n "openvino") (v "0.4.0") (d (list (d (n "float-cmp") (r "^0.8") (d #t) (k 2)) (d (n "openvino-sys") (r "^0.4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "0nv4861h5c40q5vi2hk90sfihqjl2gwhpg919pr58ka7n9qvl02m") (f (quote (("runtime-linking" "openvino-sys/runtime-linking"))))))

(define-public crate-openvino-0.4.1 (c (n "openvino") (v "0.4.1") (d (list (d (n "float-cmp") (r "^0.8") (d #t) (k 2)) (d (n "openvino-sys") (r "^0.4.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "06l5xaghcl5lk7lzdza51gvzg6j70y31q6da81h7dxaax847jqnr") (f (quote (("runtime-linking" "openvino-sys/runtime-linking"))))))

(define-public crate-openvino-0.4.2 (c (n "openvino") (v "0.4.2") (d (list (d (n "float-cmp") (r "^0.9") (d #t) (k 2)) (d (n "openvino-sys") (r "^0.4.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "0xrw8ilrs4lpz773cf75pyhrfwv97h3vzk9nakv4bsyhr88nqcy7") (f (quote (("runtime-linking" "openvino-sys/runtime-linking"))))))

(define-public crate-openvino-0.5.0 (c (n "openvino") (v "0.5.0") (d (list (d (n "float-cmp") (r "^0.9") (d #t) (k 2)) (d (n "openvino-finder") (r "^0.5.0") (d #t) (k 0)) (d (n "openvino-sys") (r "^0.5.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "1ryx5hpbcrfiiygw38gwm7gs2pid0qryxqwxnqrxapc0lzck3iyb") (f (quote (("runtime-linking" "openvino-sys/runtime-linking"))))))

(define-public crate-openvino-0.6.0 (c (n "openvino") (v "0.6.0") (d (list (d (n "float-cmp") (r "^0.9") (d #t) (k 2)) (d (n "openvino-finder") (r "^0.6.0") (d #t) (k 0)) (d (n "openvino-sys") (r "^0.6.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0yncbnnbnfc5kzbbrk1ryw6db6vghwf6q85iy6jfcs4rydz3mg94") (f (quote (("runtime-linking" "openvino-sys/runtime-linking"))))))

(define-public crate-openvino-0.7.0 (c (n "openvino") (v "0.7.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "float-cmp") (r "^0.9") (d #t) (k 2)) (d (n "openvino-finder") (r "=0.7.0") (d #t) (k 0)) (d (n "openvino-sys") (r "=0.7.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0wk96xjsij0mkm8jdbpdq54sw5b8migj7xqijjf28q80y1qq8h3b") (f (quote (("runtime-linking" "openvino-sys/runtime-linking"))))))

(define-public crate-openvino-0.7.1 (c (n "openvino") (v "0.7.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "float-cmp") (r "^0.9") (d #t) (k 2)) (d (n "openvino-finder") (r "=0.7.1") (d #t) (k 0)) (d (n "openvino-sys") (r "=0.7.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0kp1cz7xn9x4nfl9kqh84dp3gwxlvs6bsf27fkvxmcrrnrcjb483") (f (quote (("runtime-linking" "openvino-sys/runtime-linking"))))))

