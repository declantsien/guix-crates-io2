(define-module (crates-io op en opentelemetry-semantic-conventions) #:use-module (crates-io))

(define-public crate-opentelemetry-semantic-conventions-0.1.0 (c (n "opentelemetry-semantic-conventions") (v "0.1.0") (d (list (d (n "opentelemetry") (r "^0.9.0") (k 0)) (d (n "opentelemetry") (r "^0.9.0") (f (quote ("trace"))) (k 2)))) (h "0l9n6i6ll14bbb42q06bd8byp0wpkrkw5wwsry3xb76zk5gd3axx")))

(define-public crate-opentelemetry-semantic-conventions-0.2.0 (c (n "opentelemetry-semantic-conventions") (v "0.2.0") (d (list (d (n "opentelemetry") (r "^0.10") (k 0)) (d (n "opentelemetry") (r "^0.10") (f (quote ("trace"))) (k 2)))) (h "1647232084i1kfwq3zmgph0qj4ixfl1rvsq26rii7iknczhzhl9j")))

(define-public crate-opentelemetry-semantic-conventions-0.3.0 (c (n "opentelemetry-semantic-conventions") (v "0.3.0") (d (list (d (n "opentelemetry") (r "^0.11") (k 0)) (d (n "opentelemetry") (r "^0.11") (f (quote ("trace"))) (k 2)))) (h "07f241js7bh7w7cp7l4am2rrgykq8kbklqk4z6yamh8xjcndsf9q")))

(define-public crate-opentelemetry-semantic-conventions-0.4.0 (c (n "opentelemetry-semantic-conventions") (v "0.4.0") (d (list (d (n "opentelemetry") (r "^0.12") (k 0)) (d (n "opentelemetry") (r "^0.12") (f (quote ("trace"))) (k 2)))) (h "01pi9nwdlppr6g5dhakjk8x68bzy6fwyhg4f96510hbwk6dngjqf")))

(define-public crate-opentelemetry-semantic-conventions-0.5.0 (c (n "opentelemetry-semantic-conventions") (v "0.5.0") (d (list (d (n "opentelemetry") (r "^0.13") (k 0)) (d (n "opentelemetry") (r "^0.13") (f (quote ("trace"))) (k 2)))) (h "18qkbk2vgsn8ygkxlc64gm8fvwljr76x13i4rzmwwg5pbbdsiqqf")))

(define-public crate-opentelemetry-semantic-conventions-0.6.0 (c (n "opentelemetry-semantic-conventions") (v "0.6.0") (d (list (d (n "opentelemetry") (r "^0.14") (k 0)) (d (n "opentelemetry") (r "^0.14") (f (quote ("trace"))) (k 2)))) (h "078jvrhwdmi8hkvrkmn2rkjsw0x54a87vwqhscg8c2kwjll9clsm")))

(define-public crate-opentelemetry-semantic-conventions-0.7.0 (c (n "opentelemetry-semantic-conventions") (v "0.7.0") (d (list (d (n "opentelemetry") (r "^0.15") (k 0)))) (h "0vbbmid0809y9kj9qaqwiz708fkppiswn9ngx47py7b2np4h51bl")))

(define-public crate-opentelemetry-semantic-conventions-0.8.0 (c (n "opentelemetry-semantic-conventions") (v "0.8.0") (d (list (d (n "opentelemetry") (r "^0.16") (k 0)))) (h "1sbh48ghkmjz06ylbirvhvr9g7xzax83ix31p4khz2wy6ciwispz")))

(define-public crate-opentelemetry-semantic-conventions-0.9.0 (c (n "opentelemetry-semantic-conventions") (v "0.9.0") (d (list (d (n "opentelemetry") (r "^0.17") (k 0)))) (h "1gb21vchkkhd11nzggnhlmm9pf5ijj8jzzngn8j24h9dhdfw6p4q")))

(define-public crate-opentelemetry-semantic-conventions-0.10.0 (c (n "opentelemetry-semantic-conventions") (v "0.10.0") (d (list (d (n "opentelemetry") (r "^0.18") (k 0)))) (h "1swyxkrhm3n1r8639092fxiyl0r9l27vmqhqdmivc2mv18iy00lv") (r "1.56")))

(define-public crate-opentelemetry-semantic-conventions-0.11.0 (c (n "opentelemetry-semantic-conventions") (v "0.11.0") (d (list (d (n "opentelemetry") (r "^0.19") (k 0)))) (h "1r9wcw7y5sy6f1yahxxqibj0zawg6n7dnkgazkvwc25zwql39qr4") (r "1.60")))

(define-public crate-opentelemetry-semantic-conventions-0.12.0 (c (n "opentelemetry-semantic-conventions") (v "0.12.0") (d (list (d (n "opentelemetry") (r "^0.20") (k 0)))) (h "0scjg1lyrlykvqc8bgzm8dqrxv89kr7b5wg70240cdfi18sgkjbk") (r "1.60")))

(define-public crate-opentelemetry-semantic-conventions-0.13.0 (c (n "opentelemetry-semantic-conventions") (v "0.13.0") (d (list (d (n "opentelemetry") (r "^0.21") (k 0)))) (h "115wbgk840dklyhpg3lwp4x1m643qd7f0vkz8hmfz0pry4g4yxzm") (r "1.65")))

(define-public crate-opentelemetry-semantic-conventions-0.14.0 (c (n "opentelemetry-semantic-conventions") (v "0.14.0") (d (list (d (n "opentelemetry") (r "^0.22") (k 2)) (d (n "opentelemetry_sdk") (r "^0.22") (f (quote ("trace"))) (d #t) (k 2)))) (h "04197racbkpj75fh9jnwkdznjzv6l2ljpbr8ryfk9f9gqkb5pazr") (r "1.65")))

(define-public crate-opentelemetry-semantic-conventions-0.15.0 (c (n "opentelemetry-semantic-conventions") (v "0.15.0") (h "1mx45kzf86fs5558c7h90w6h2k3vi899n374l6l5np5kp55zns8q") (r "1.65")))

