(define-module (crates-io op en openfair) #:use-module (crates-io))

(define-public crate-openfair-0.1.1 (c (n "openfair") (v "0.1.1") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)))) (h "1ydyafhnf0r1aly9p3cpsgnihc4aaqwrlscbyjl0w9j8vic64h5q")))

