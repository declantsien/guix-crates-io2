(define-module (crates-io op en opennode-client) #:use-module (crates-io))

(define-public crate-opennode-client-0.1.0 (c (n "opennode-client") (v "0.1.0") (d (list (d (n "actix-http") (r "^0.2.5") (d #t) (k 0)) (d (n "actix-rt") (r "^0.2.2") (d #t) (k 2)) (d (n "awc") (r "^0.2.1") (f (quote ("ssl"))) (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 2)) (d (n "futures") (r "^0.1.27") (d #t) (k 0)) (d (n "opennode") (r "^1.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.93") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)))) (h "18zx89k9cg7s2vxpv086df8fry9y4rx9c41qc8qn79mcrz2zja16")))

(define-public crate-opennode-client-1.0.0 (c (n "opennode-client") (v "1.0.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 2)) (d (n "opennode") (r "^1.0.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.0-alpha.2") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.93") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)) (d (n "tokio") (r "^0.2.0-alpha.6") (d #t) (k 2)))) (h "0vzvlihs4asggxadhyph01cqqqivn86cbz7kdvzg2yag9g4gxgcy")))

(define-public crate-opennode-client-1.0.1 (c (n "opennode-client") (v "1.0.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 2)) (d (n "opennode") (r "^1.0.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.0") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.93") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)) (d (n "tokio") (r "^0.2.0-alpha.6") (d #t) (k 2)))) (h "09dncc1hy8m29zlcasc1m05mm8jcfa7lg0mddw5cm9aaqk574jvb")))

