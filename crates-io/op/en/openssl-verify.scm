(define-module (crates-io op en openssl-verify) #:use-module (crates-io))

(define-public crate-openssl-verify-0.1.0 (c (n "openssl-verify") (v "0.1.0") (d (list (d (n "openssl") (r "^0.7.11") (d #t) (k 0)))) (h "0bgb7y6bqcci09lkivmrkj3ch7bg08sfn89faza0wssgi776rn1y")))

(define-public crate-openssl-verify-0.2.0 (c (n "openssl-verify") (v "0.2.0") (d (list (d (n "openssl") (r "^0.8") (d #t) (k 0)))) (h "1m1kx4yad307mrbvbbgm99ycmz7z9psij2y3qaybll8a994w0n4d")))

