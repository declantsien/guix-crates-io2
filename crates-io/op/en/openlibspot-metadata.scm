(define-module (crates-io op en openlibspot-metadata) #:use-module (crates-io))

(define-public crate-openlibspot-metadata-0.6.0 (c (n "openlibspot-metadata") (v "0.6.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "openlibspot-core") (r "^0.6.0") (d #t) (k 0)) (d (n "openlibspot-protocol") (r "^0.6.0") (d #t) (k 0)) (d (n "protobuf") (r "^3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "uuid") (r "^1") (k 0)))) (h "1lg6nimikwypq5vmci04xq9wgz9q8hwa91rj3pni9mynksvcwr26") (r "1.61")))

