(define-module (crates-io op en opentimestamps) #:use-module (crates-io))

(define-public crate-opentimestamps-0.1.2 (c (n "opentimestamps") (v "0.1.2") (d (list (d (n "env_logger") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)))) (h "1dgwca3csjz01ilvss1j9fk0zpnw94bm3bz4haaamwr9amb002b2")))

(define-public crate-opentimestamps-0.2.0 (c (n "opentimestamps") (v "0.2.0") (d (list (d (n "bitcoin_hashes") (r "^0.12.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1fhjkzchdp0mgchhnchbws8jm5m0rvxb27qa454msyv41kvcavs2")))

