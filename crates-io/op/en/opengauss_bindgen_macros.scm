(define-module (crates-io op en opengauss_bindgen_macros) #:use-module (crates-io))

(define-public crate-opengauss_bindgen_macros-0.1.0 (c (n "opengauss_bindgen_macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0dl82np0darrn4qsm0mp0h8cxy2pg48qbgz02n6360kjmbc9px5v")))

