(define-module (crates-io op en openscad-language-server) #:use-module (crates-io))

(define-public crate-openscad-language-server-0.1.0 (c (n "openscad-language-server") (v "0.1.0") (d (list (d (n "home") (r "^0.5.3") (d #t) (k 0)) (d (n "lsp-server") (r "^0.5.2") (d #t) (k 0)) (d (n "lsp-types") (r "^0.89.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tree-sitter") (r "^0.20") (d #t) (k 0)) (d (n "tree-sitter-openscad") (r "^0.2.0") (d #t) (k 0)))) (h "0dhqnjsl17mdnymq018q2ybnzal7acj9441ay5xl7gpkcxh7a3lk")))

