(define-module (crates-io op en openrpc-types) #:use-module (crates-io))

(define-public crate-openrpc-types-0.1.0 (c (n "openrpc-types") (v "0.1.0") (d (list (d (n "schemars") (r "^0.8.16") (d #t) (k 0)) (d (n "semver") (r "^1.0.23") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.117") (d #t) (k 0)) (d (n "url") (r "^2.5.0") (f (quote ("serde"))) (d #t) (k 0)))) (h "0vn5qrr872dlaqgj74hbmvqax4xyb3ncys25gzf00adlz5fv83bb")))

(define-public crate-openrpc-types-0.2.0 (c (n "openrpc-types") (v "0.2.0") (d (list (d (n "schemars") (r "^0.8.16") (d #t) (k 0)) (d (n "semver") (r "^1.0.23") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.117") (d #t) (k 0)) (d (n "url") (r "^2.5.0") (f (quote ("serde"))) (d #t) (k 0)))) (h "1dnr7vi99fmyqxm77gafb75smydvchhl7bqjk8jg7bb3j0ryc6c0")))

(define-public crate-openrpc-types-0.2.1 (c (n "openrpc-types") (v "0.2.1") (d (list (d (n "schemars") (r "^0.8.16") (d #t) (k 0)) (d (n "semver") (r "^1.0.23") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.117") (d #t) (k 0)) (d (n "serde_path_to_error") (r "^0.1.16") (d #t) (k 2)) (d (n "url") (r "^2.5.0") (f (quote ("serde"))) (d #t) (k 0)))) (h "1rq6pv0chhx1x2i96m7g4qik4vrfakw2d8qdd5976dl18wmvld2p")))

(define-public crate-openrpc-types-0.2.2 (c (n "openrpc-types") (v "0.2.2") (d (list (d (n "schemars") (r "^0.8.16") (d #t) (k 0)) (d (n "semver") (r "^1.0.23") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.117") (d #t) (k 0)) (d (n "serde_path_to_error") (r "^0.1.16") (d #t) (k 2)) (d (n "url") (r "^2.5.0") (f (quote ("serde"))) (d #t) (k 0)))) (h "1yjjmnkgzaxsjshn6xh3rh83jwpyd8l2najh34pwpk73snkq5ipy")))

