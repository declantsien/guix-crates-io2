(define-module (crates-io op en openapi) #:use-module (crates-io))

(define-public crate-openapi-0.1.0 (c (n "openapi") (v "0.1.0") (d (list (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.7") (d #t) (k 0)))) (h "0airyjhqpxj4fnchxw72gsqbbg78nr9rj6r58kx712isighviakd")))

(define-public crate-openapi-0.1.1 (c (n "openapi") (v "0.1.1") (d (list (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.7") (d #t) (k 0)))) (h "0bdn9jiwn683pyn4mnzi5v4inzf441q3hwq19d1966kqvcycyn9x")))

(define-public crate-openapi-0.1.2 (c (n "openapi") (v "0.1.2") (d (list (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.7") (d #t) (k 0)))) (h "1jfygmrf7bbndhz29d4m40kczzpvniad7k848g5ml1mbyvqd8rvq")))

(define-public crate-openapi-0.1.3 (c (n "openapi") (v "0.1.3") (d (list (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.7") (d #t) (k 0)))) (h "03xhxscfl9vhn0ax3qkwfin35yczmnwvki772d2vbaqfy5xp57ag")))

(define-public crate-openapi-0.1.4 (c (n "openapi") (v "0.1.4") (d (list (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.7") (d #t) (k 0)))) (h "0wqd7rfzwwxq8qs6pp5blx2x4gdg6kchfs03wcnswqhqagj9b6c6")))

(define-public crate-openapi-0.1.5 (c (n "openapi") (v "0.1.5") (d (list (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.7") (d #t) (k 0)))) (h "007ahdgndpgm6ry22jldnm8lcpxyl2mp32fzfn0n7x55z3yax73k")))

