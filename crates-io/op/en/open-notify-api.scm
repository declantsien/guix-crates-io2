(define-module (crates-io op en open-notify-api) #:use-module (crates-io))

(define-public crate-open-notify-api-0.2.0 (c (n "open-notify-api") (v "0.2.0") (d (list (d (n "reqwest") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.34") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.34") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1laprq88pf4zz93yb14d0fhicdjlvv04kpdqf4h95ar3jwx7ykd4")))

