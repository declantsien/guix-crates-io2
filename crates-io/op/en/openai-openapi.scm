(define-module (crates-io op en openai-openapi) #:use-module (crates-io))

(define-public crate-openai-openapi-0.1.1 (c (n "openai-openapi") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "httpclient") (r "^0.1.13") (d #t) (k 0)) (d (n "indexmap") (r "^1.8.1") (d #t) (k 0)) (d (n "openapi-client-generator") (r "^0.1.1") (d #t) (k 2)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "tokio") (r "^1.18.2") (f (quote ("full"))) (d #t) (k 2)))) (h "0jxwd4ij2bw89i677g3pi1yqqlwzy086iv2crnc1wkwqk64j411l")))

(define-public crate-openai-openapi-0.2.0 (c (n "openai-openapi") (v "0.2.0") (h "1wcn3v9gjdq34dj71341s62c0d321hp58rajhxlz0fqr89hhhawl")))

