(define-module (crates-io op en openapi-wit) #:use-module (crates-io))

(define-public crate-openapi-wit-1.0.0 (c (n "openapi-wit") (v "1.0.0") (h "02zl3dsxms8lrl2ssp8i5bqhm34fgpk7kngcljc037x67jq0df9p")))

(define-public crate-openapi-wit-0.0.0 (c (n "openapi-wit") (v "0.0.0") (h "0nyh90a11n9zxicmyaz5vnb0md33yxhx1zpd70dvzlw91k9dbzgg")))

