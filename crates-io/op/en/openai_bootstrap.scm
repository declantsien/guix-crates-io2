(define-module (crates-io op en openai_bootstrap) #:use-module (crates-io))

(define-public crate-openai_bootstrap-1.0.0-alpha.3 (c (n "openai_bootstrap") (v "1.0.0-alpha.3") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0chxvhwdjl2nq1k5rxxv0xxy5giiwqyz7plqbm0vfdmdl6cy000g")))

(define-public crate-openai_bootstrap-1.0.0-alpha.4 (c (n "openai_bootstrap") (v "1.0.0-alpha.4") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0x0gihz7725sb9frnw060hr7n1rq6sndnbjbqrd347vlipgw2qb5")))

(define-public crate-openai_bootstrap-1.0.0-alpha.5 (c (n "openai_bootstrap") (v "1.0.0-alpha.5") (d (list (d (n "serde") (r "^1.0.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1gghya562jwa574g1rq6fwcvgj19m7wpnml9f91c3bjvvfvy07q5")))

