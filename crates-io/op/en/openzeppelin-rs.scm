(define-module (crates-io op en openzeppelin-rs) #:use-module (crates-io))

(define-public crate-openzeppelin-rs-0.1.0 (c (n "openzeppelin-rs") (v "0.1.0") (d (list (d (n "ethers") (r "^2.0.6") (d #t) (k 0)) (d (n "ethers") (r "^2.0.6") (d #t) (k 1)) (d (n "ethers-solc") (r "^2.0.6") (d #t) (k 1)) (d (n "flate2") (r "^1.0.26") (d #t) (k 1)) (d (n "git2") (r "^0.17.2") (d #t) (k 1)) (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking"))) (d #t) (k 1)) (d (n "serde") (r "^1.0.163") (d #t) (k 1)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 1)) (d (n "tar") (r "^0.4.38") (d #t) (k 1)) (d (n "tempfile") (r "^3.5.0") (d #t) (k 1)) (d (n "walkdir") (r "^2.3.3") (d #t) (k 1)))) (h "05wgw4h40sr9699id5ns82s6a22kqb659ss7dp9kxj4n6ja6aqzs")))

(define-public crate-openzeppelin-rs-0.1.1 (c (n "openzeppelin-rs") (v "0.1.1") (d (list (d (n "ethers-contract") (r "^2.0.6") (d #t) (k 0)) (d (n "ethers-core") (r "^2.0.6") (d #t) (k 0)) (d (n "ethers-providers") (r "^2.0.6") (d #t) (k 0)))) (h "04xs2fdgvdp09rxa4f88cj9aqnisidfvwrxjmanlk9bcb1kxll5m")))

(define-public crate-openzeppelin-rs-0.1.2 (c (n "openzeppelin-rs") (v "0.1.2") (d (list (d (n "ethers-contract") (r "^2.0.6") (d #t) (k 0)) (d (n "ethers-core") (r "^2.0.6") (d #t) (k 0)) (d (n "ethers-providers") (r "^2.0.6") (d #t) (k 0)))) (h "1032hjxng3y7v5j7s4cwnhrp9325z3jjnbaasp7xqd8rcfpq1aqk")))

