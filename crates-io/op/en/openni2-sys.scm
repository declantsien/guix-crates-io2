(define-module (crates-io op en openni2-sys) #:use-module (crates-io))

(define-public crate-openni2-sys-0.1.0 (c (n "openni2-sys") (v "0.1.0") (h "182i1108y67xyjkygaclinalh36mm1y7inhmn4gggbzn892374x1") (l "openni2")))

(define-public crate-openni2-sys-0.2.0 (c (n "openni2-sys") (v "0.2.0") (h "00dypb5d3gj80nsrp83307v48gqdzz065mnd578wjd397mmwnl7w") (l "openni2")))

(define-public crate-openni2-sys-0.3.0 (c (n "openni2-sys") (v "0.3.0") (h "1qc4wvppfipglr67ycalydlfj6232jfbc14nfwy1s71lqyj9h09n") (l "openni2")))

(define-public crate-openni2-sys-1.0.0 (c (n "openni2-sys") (v "1.0.0") (h "0y08x21yfc0p73msrjq6wbasqk13mw46pr2yjpx3hpjpjdi4zq25") (l "openni2")))

(define-public crate-openni2-sys-1.0.1 (c (n "openni2-sys") (v "1.0.1") (h "1mqglmpmyx4j1hgcw39jb20m6z4512pgs0mzv0jvl5sfgyxgrc1l") (l "openni2")))

(define-public crate-openni2-sys-1.0.2 (c (n "openni2-sys") (v "1.0.2") (h "1pi7mbgiphzfccaj94p4krfk520wykzfidqhkzaiickshrqla2bi") (l "openni2")))

(define-public crate-openni2-sys-1.1.0 (c (n "openni2-sys") (v "1.1.0") (h "04j1ydcpcvd70zzipd7ic7vxxqjg9infibyrlpxrjhvjri3788gq") (l "openni2")))

(define-public crate-openni2-sys-1.1.1 (c (n "openni2-sys") (v "1.1.1") (h "132ixgx2mqvdm7j57sxd937wkfdq0zgjqpiab4642dkycvm4lmj0")))

