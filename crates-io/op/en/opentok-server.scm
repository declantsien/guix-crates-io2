(define-module (crates-io op en opentok-server) #:use-module (crates-io))

(define-public crate-opentok-server-0.1.3 (c (n "opentok-server") (v "0.1.3") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.17") (d #t) (k 0)) (d (n "hmac-sha1") (r "^0.1.3") (d #t) (k 0)) (d (n "jsonwebtoken") (r "^7.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.24") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.68") (d #t) (k 0)) (d (n "surf") (r "^2.3.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)))) (h "0xjpm5vi9x2mq1i558mn36nj6ik393bdqq0iwm9lv3kc4g2s4966")))

