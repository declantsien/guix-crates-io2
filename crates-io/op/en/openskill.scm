(define-module (crates-io op en openskill) #:use-module (crates-io))

(define-public crate-openskill-0.0.1 (c (n "openskill") (v "0.0.1") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 2)) (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 0)) (d (n "statrs") (r "^0.16.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "0aa7q7jkkjs3b6rn8ak9jsp2ml7ndmhpp2y0m8gchas1m2ihv9kn")))

