(define-module (crates-io op en openat) #:use-module (crates-io))

(define-public crate-openat-0.1.0 (c (n "openat") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.18") (d #t) (k 0)))) (h "06dkf4s82qqrhkjqz3g7b7f4wzfgzzk4a118glgdr06m7whkm7xj")))

(define-public crate-openat-0.1.1 (c (n "openat") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.18") (d #t) (k 0)))) (h "0k771pc8fx5y02860lny5l8qjfiwfd0gsv8ag6xhhyxx749qjg1m") (y #t)))

(define-public crate-openat-0.1.2 (c (n "openat") (v "0.1.2") (d (list (d (n "libc") (r "^0.2.18") (d #t) (k 0)))) (h "1kpagzjp62d4rwibnjp1q8mv6551lbb2h75qz5i7alxzlsh38vhl")))

(define-public crate-openat-0.1.3 (c (n "openat") (v "0.1.3") (d (list (d (n "libc") (r "^0.2.18") (d #t) (k 0)))) (h "0xm062piw67jfapnzf43fgmlqwczfq1ybnnicqjw8qgc7dk4dkcn")))

(define-public crate-openat-0.1.4 (c (n "openat") (v "0.1.4") (d (list (d (n "libc") (r "^0.2.18") (d #t) (k 0)))) (h "19bc30zb07cl1ljjlii5iyxfl0qfdwwyqmryhaizkwgkf7fddzlq")))

(define-public crate-openat-0.1.5 (c (n "openat") (v "0.1.5") (d (list (d (n "libc") (r "^0.2.18") (d #t) (k 0)))) (h "0jivav35dj08qi25w4zih9kd3yw9rly7gjv38j19y5yr58vna111")))

(define-public crate-openat-0.1.6 (c (n "openat") (v "0.1.6") (d (list (d (n "libc") (r "^0.2.18") (d #t) (k 0)))) (h "1afh7pfj8mywhhrfj9aj8636gc5r4bjrcc9xxphgff7971x8l5cw")))

(define-public crate-openat-0.1.7 (c (n "openat") (v "0.1.7") (d (list (d (n "libc") (r "^0.2.18") (d #t) (k 0)))) (h "024m3ns4i0w6ramms1a0hp7qg96f072vj8bahhammipyrx4k5vlf")))

(define-public crate-openat-0.1.8 (c (n "openat") (v "0.1.8") (d (list (d (n "libc") (r "^0.2.18") (d #t) (k 0)))) (h "1a3yvhwqknxdswg26v066gdbba07vfwa8cl3npcs01ynrmzzncvj")))

(define-public crate-openat-0.1.9 (c (n "openat") (v "0.1.9") (d (list (d (n "libc") (r "^0.2.18") (d #t) (k 0)))) (h "07zryc12q5hjbmvsam7zxsg8xnj0cd48jaasydjkrijwf4vhnkcp")))

(define-public crate-openat-0.1.10 (c (n "openat") (v "0.1.10") (d (list (d (n "argparse") (r "^0.2.1") (d #t) (k 2)) (d (n "libc") (r "^0.2.28") (d #t) (k 0)))) (h "110qf5lznww6j2r5ilkdsqp22h8dfdkrq0g30jbvllzw484n3j7p")))

(define-public crate-openat-0.1.11 (c (n "openat") (v "0.1.11") (d (list (d (n "argparse") (r "^0.2.1") (d #t) (k 2)) (d (n "libc") (r "^0.2.28") (d #t) (k 0)))) (h "090mn72rm00jf48hafl8919md1d9j1aala2n7zwyc2grxhgnr8nc")))

(define-public crate-openat-0.1.12 (c (n "openat") (v "0.1.12") (d (list (d (n "argparse") (r "^0.2.1") (d #t) (k 2)) (d (n "libc") (r "^0.2.28") (d #t) (k 0)))) (h "1lizcflp05ypvbqvyh3z873s7c2vc5wvg2jhkhvx4r1fb0mmf9s3")))

(define-public crate-openat-0.1.13 (c (n "openat") (v "0.1.13") (d (list (d (n "argparse") (r "^0.2.1") (d #t) (k 2)) (d (n "libc") (r "^0.2.34") (d #t) (k 0)))) (h "0hxziwz8mvjjsf8yfwqyhh2f0hrq3qd17i6jvps9vjzhs4kk94ra")))

(define-public crate-openat-0.1.14 (c (n "openat") (v "0.1.14") (d (list (d (n "argparse") (r "^0.2.1") (d #t) (k 2)) (d (n "libc") (r "^0.2.34") (d #t) (k 0)))) (h "1jqhpx9bs7w0ldzjk9g32s0lsakr62jcj9139riicvqq3w4lmkd9")))

(define-public crate-openat-0.1.15 (c (n "openat") (v "0.1.15") (d (list (d (n "argparse") (r "^0.2.1") (d #t) (k 2)) (d (n "libc") (r "^0.2.34") (d #t) (k 0)))) (h "1d28f6szy7bcl7d2067csi8knna0bhs9v54b7h1a5s3ifw5djqy7")))

(define-public crate-openat-0.1.16 (c (n "openat") (v "0.1.16") (d (list (d (n "argparse") (r "^0.2.1") (d #t) (k 2)) (d (n "libc") (r "^0.2.34") (d #t) (k 0)) (d (n "tempfile") (r "^3.0.3") (d #t) (k 2)))) (h "12mlhk6c07gz8gx6784r9ywa5ay0jsxsj6gxh4z34shwykcll5b5")))

(define-public crate-openat-0.1.17 (c (n "openat") (v "0.1.17") (d (list (d (n "argparse") (r "^0.2.1") (d #t) (k 2)) (d (n "libc") (r "^0.2.34") (d #t) (k 0)) (d (n "tempfile") (r "^3.0.3") (d #t) (k 2)))) (h "1izflv62mmzwi21j95isy48ys96rxlr31s03frgyf69d4vxszxad")))

(define-public crate-openat-0.1.18 (c (n "openat") (v "0.1.18") (d (list (d (n "argparse") (r "^0.2.1") (d #t) (k 2)) (d (n "libc") (r "^0.2.34") (d #t) (k 0)) (d (n "tempfile") (r "^3.0.3") (d #t) (k 2)))) (h "16l18b7jcca5d2ikl7ybscl6sym0jk6vvwqb2xzzvqhvpwk7mrfc")))

(define-public crate-openat-0.1.19 (c (n "openat") (v "0.1.19") (d (list (d (n "argparse") (r "^0.2.1") (d #t) (k 2)) (d (n "libc") (r "^0.2.34") (d #t) (k 0)) (d (n "tempfile") (r "^3.0.3") (d #t) (k 2)))) (h "1kni0hpyha182q7jmkhw3af8f5831q21vkvy0vb1z13475p8gzwf")))

(define-public crate-openat-0.1.20 (c (n "openat") (v "0.1.20") (d (list (d (n "argparse") (r "^0.2.1") (d #t) (k 2)) (d (n "libc") (r "^0.2.34") (d #t) (k 0)) (d (n "tempfile") (r "^3.0.3") (d #t) (k 2)))) (h "1f1qchwzc8xnh8qbn83g5g9yhwwvyrvimix92qm053i5idkc1kcp")))

(define-public crate-openat-0.1.21 (c (n "openat") (v "0.1.21") (d (list (d (n "argparse") (r "^0.2.1") (d #t) (k 2)) (d (n "libc") (r "^0.2.34") (d #t) (k 0)) (d (n "tempfile") (r "^3.0.3") (d #t) (k 2)))) (h "1gism05z75h3qd3kcjc4db64a8crvm6jyq0dc7ibsgkvj02pralm")))

