(define-module (crates-io op en openssl-hkdf) #:use-module (crates-io))

(define-public crate-openssl-hkdf-0.1.0 (c (n "openssl-hkdf") (v "0.1.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "openssl") (r "^0.10") (d #t) (k 0)) (d (n "openssl-sys") (r "^0.9") (d #t) (k 0)))) (h "017a3b34zmdzwaf5b1kz9ayg27mz2j0p3jigx8zxk4m7r4zzkw8x")))

(define-public crate-openssl-hkdf-0.1.1 (c (n "openssl-hkdf") (v "0.1.1") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "openssl") (r "^0.10") (d #t) (k 0)) (d (n "openssl-sys") (r "^0.9") (d #t) (k 0)))) (h "10h4iqi8iy5hcz70jxjjpaqvmirxahxmbg6ssz983c98b353il8w")))

(define-public crate-openssl-hkdf-0.2.0 (c (n "openssl-hkdf") (v "0.2.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "openssl") (r "^0.10") (d #t) (k 0)) (d (n "openssl-sys") (r "^0.9.72") (d #t) (k 0)))) (h "1bsb6q789iaalafyi9z1vl0mgj7r433xmp56dbl88mq9kd3xd7hd")))

