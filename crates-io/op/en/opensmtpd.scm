(define-module (crates-io op en opensmtpd) #:use-module (crates-io))

(define-public crate-opensmtpd-0.1.0 (c (n "opensmtpd") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.6") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^4.1") (d #t) (k 0)))) (h "15vdmifnhy7i78hpbndywhn1nvhrgj83s5hg93z90drkjhq0acvf")))

(define-public crate-opensmtpd-0.2.0 (c (n "opensmtpd") (v "0.2.0") (d (list (d (n "log") (r ">=0.4.0, <0.5.0") (d #t) (k 0)) (d (n "nom") (r ">=6.0.0, <7.0.0") (d #t) (k 0)) (d (n "simplelog") (r ">=0.8.0, <0.9.0") (d #t) (k 2)))) (h "1wfqls0qyxkkzhygv7mxs28bvbbn2z7gxb4yp7flblis5752ll4s")))

(define-public crate-opensmtpd-0.3.0 (c (n "opensmtpd") (v "0.3.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^6.0") (d #t) (k 0)) (d (n "pretty-hex") (r "^0.2") (d #t) (k 0)) (d (n "simplelog") (r "^0.9") (d #t) (k 2)))) (h "113f1qjb0q42gkma5irzb6cy1h1fz4prjy3ijc49yd2sb4gjvxpx")))

(define-public crate-opensmtpd-0.4.0 (c (n "opensmtpd") (v "0.4.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^6.0") (d #t) (k 0)) (d (n "opensmtpd_derive") (r "^0.4") (d #t) (k 0)) (d (n "pretty-hex") (r "^0.2") (d #t) (k 0)) (d (n "simplelog") (r "^0.9") (d #t) (k 2)))) (h "1ccc2g1dk0bkp9l4644qygv3crx4qfwdpc27f2jmmv0v6q7bfl5v")))

(define-public crate-opensmtpd-0.4.1 (c (n "opensmtpd") (v "0.4.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^6.0") (d #t) (k 0)) (d (n "opensmtpd_derive") (r "^0.4") (d #t) (k 0)) (d (n "pretty-hex") (r "^0.2") (d #t) (k 0)) (d (n "simplelog") (r "^0.9") (d #t) (k 2)))) (h "1gsqxqi2f9ca2i37llhaaz60r29hsg1m80lxp4yfffb9sdwgzsaj")))

