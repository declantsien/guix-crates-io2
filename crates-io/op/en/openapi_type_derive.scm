(define-module (crates-io op en openapi_type_derive) #:use-module (crates-io))

(define-public crate-openapi_type_derive-0.1.0 (c (n "openapi_type_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0wy57rawp5axnxcwsx76354kvhb96xcmszr68dfx327466lwh4lq")))

(define-public crate-openapi_type_derive-0.2.0 (c (n "openapi_type_derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0rg89gw27kkrjc4fin92qdmfsijhf9az0c8mqd1ll8czj4glrvv0")))

(define-public crate-openapi_type_derive-0.2.1 (c (n "openapi_type_derive") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0q5sikhfa1znfshckm6dw6xmv36qfhndnpz02iar47p32w7dfgh6")))

(define-public crate-openapi_type_derive-0.2.2 (c (n "openapi_type_derive") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0ia1xsjwla36bcmrqw4hv865kbfpd72z0bip10awng0ikqr09j8a")))

(define-public crate-openapi_type_derive-0.2.3 (c (n "openapi_type_derive") (v "0.2.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "13qgzhhh7riq05bl7dvxglkmvlnk97z93ilmv20r69bs52x9ynac")))

(define-public crate-openapi_type_derive-0.2.4 (c (n "openapi_type_derive") (v "0.2.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive_internals") (r "^0.26") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1s6f4dpdr8vw6cnh26l81ny0bvvvcn12cjk178jzs635k25pib24")))

(define-public crate-openapi_type_derive-0.2.5 (c (n "openapi_type_derive") (v "0.2.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive_internals") (r "^0.26") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "05fyszrc4j0226ry3bll0xyvz68w8785lx45y8adnrlqyvql9qy4")))

(define-public crate-openapi_type_derive-0.3.0 (c (n "openapi_type_derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive_internals") (r "^0.26") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0bj1pfxmv3xkjczypr3in97jmwjv8bfr8jhspy9dk5lk1r7h30pi") (r "1.56")))

(define-public crate-openapi_type_derive-0.4.0 (c (n "openapi_type_derive") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive_internals") (r "^0.26") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1m74qzpwwm35sbpbb4asq4fmwb1l7igfnxd1zkxzin4yr1sxq8sl") (r "1.58")))

(define-public crate-openapi_type_derive-0.4.1 (c (n "openapi_type_derive") (v "0.4.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive_internals") (r "^0.26") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1ivca3bdwqqz54yyf38w3wyx7hdjd1s73gcfyyn28jla8rljg589") (r "1.58")))

(define-public crate-openapi_type_derive-0.4.2 (c (n "openapi_type_derive") (v "0.4.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive_internals") (r "^0.26") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "syn-path") (r "^1.0") (d #t) (k 0)))) (h "19ca2vbn00sn0w6sjm4wqsn0ch4ikb0ikmp3sr0cb7sjd6pwiaag") (r "1.58")))

(define-public crate-openapi_type_derive-0.4.3 (c (n "openapi_type_derive") (v "0.4.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive_internals") (r "^0.27") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)) (d (n "syn-path") (r "^2.0") (d #t) (k 0)))) (h "0rh2a60jyp28i6dybm57z2zhfmd2rx1m571kyl2mnlzqwvqqin7l") (r "1.58")))

(define-public crate-openapi_type_derive-0.4.4 (c (n "openapi_type_derive") (v "0.4.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive_internals") (r "^0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)) (d (n "syn-path") (r "^2.0") (d #t) (k 0)))) (h "09gc8x7h2kznsvjpn5bvrrwar3392pi8qjxmvmwyyj1ia02m9a23") (r "1.58")))

(define-public crate-openapi_type_derive-0.4.5 (c (n "openapi_type_derive") (v "0.4.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive_internals") (r "^0.29") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)) (d (n "syn-path") (r "^2.0") (d #t) (k 0)))) (h "0jll1968zqbl63y82v60q875ca8w9151znnqvqn0fzg6yb32k4j1") (r "1.58")))

