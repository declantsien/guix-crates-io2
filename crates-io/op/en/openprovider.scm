(define-module (crates-io op en openprovider) #:use-module (crates-io))

(define-public crate-openprovider-0.1.0 (c (n "openprovider") (v "0.1.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0621bgmk7mh9zqy0l5vwv7yn3qqazyxsfx6vfj8rnjs3pm8aykrr")))

