(define-module (crates-io op en openpilot) #:use-module (crates-io))

(define-public crate-openpilot-0.0.0 (c (n "openpilot") (v "0.0.0") (h "05bpz1nk9r5i7xvn7wbfp9hkzy9r3bsffzd2gy8i1hv94d7f2p16")))

(define-public crate-openpilot-0.0.1 (c (n "openpilot") (v "0.0.1") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 2)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)))) (h "0bgijwdciz20r1259s0kw9lml3bn4pa67qbn8flyg0b13s83yhmz")))

(define-public crate-openpilot-0.0.2 (c (n "openpilot") (v "0.0.2") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 2)) (d (n "interp") (r "^1.0.3") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)))) (h "1mmmb4613rknb0a803hlx9y15514s5v9ypphk7xl1kd7ms8azfa2")))

(define-public crate-openpilot-0.0.3 (c (n "openpilot") (v "0.0.3") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 2)) (d (n "interp") (r "^1.0.3") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)))) (h "03hiasz84a086p747vhrrb13xbb5w0s8vk03nxkzd7rrjbm0j7bb")))

(define-public crate-openpilot-0.0.4 (c (n "openpilot") (v "0.0.4") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 2)) (d (n "interp") (r "^1.0.3") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)))) (h "1w7idffndv1p3xvgyb22ar57fsixfsg8wqbqvqxp3dxv1lf2picz")))

