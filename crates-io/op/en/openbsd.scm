(define-module (crates-io op en openbsd) #:use-module (crates-io))

(define-public crate-openbsd-0.0.0 (c (n "openbsd") (v "0.0.0") (h "1dz5mq8n2jd4synk1xln98cs12ya9jhj53ihqkc2qbyd6291zc4i")))

(define-public crate-openbsd-0.1.0 (c (n "openbsd") (v "0.1.0") (h "08nrni48yj2qw83yqa2s8gbhxs9a9lmq64w5sp4mvcn0pa296qh6")))

(define-public crate-openbsd-0.1.1 (c (n "openbsd") (v "0.1.1") (h "0l6pb19arlmwl3iwzrj28amynr8fi2vaz2bck2p8y2gmyqqlvx14")))

(define-public crate-openbsd-0.1.2 (c (n "openbsd") (v "0.1.2") (h "0j1705n58a8xz0j14kzx6iqc00yrmvvfpjq22a0i73sz5fqbly7d")))

