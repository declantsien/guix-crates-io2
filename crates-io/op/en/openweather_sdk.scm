(define-module (crates-io op en openweather_sdk) #:use-module (crates-io))

(define-public crate-openweather_sdk-0.1.0 (c (n "openweather_sdk") (v "0.1.0") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 2)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1q27an8ly01as834602l7jvsip9zcmn1i8wib3asbw4r18id6hf6")))

(define-public crate-openweather_sdk-0.1.1 (c (n "openweather_sdk") (v "0.1.1") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 2)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "05vpiqfvrixk3lgbvaczq5lbzk665z8x9wharipz9lj89hd24qb1")))

(define-public crate-openweather_sdk-0.1.2 (c (n "openweather_sdk") (v "0.1.2") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 2)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0vj9fal3pq0j8yyzqyn3dqmzikcw2zacwygmzd8423iw0sbwiciz")))

(define-public crate-openweather_sdk-0.1.3 (c (n "openweather_sdk") (v "0.1.3") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 2)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0n2dplp6z0c8v32wz7bjyvxnkv629a6xsvscbsa37h03c76qm7d8")))

(define-public crate-openweather_sdk-0.1.4 (c (n "openweather_sdk") (v "0.1.4") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 2)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0s5qhsw7dz7srq7dv4ha4ic057b4df2q5hy3gi86fmvcippwv281")))

(define-public crate-openweather_sdk-0.1.5 (c (n "openweather_sdk") (v "0.1.5") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 2)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1v9nqd6z70plng8418rnyaalh2840y94agvl3mx10rk02zk1fhwh")))

