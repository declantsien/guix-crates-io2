(define-module (crates-io op en openh264) #:use-module (crates-io))

(define-public crate-openh264-0.0.0 (c (n "openh264") (v "0.0.0") (h "13sgwggsx3s1d7vx3xi7hfqwplwvidavjkajv6hkr0mrpvw0yn2a")))

(define-public crate-openh264-0.1.9 (c (n "openh264") (v "0.1.9") (d (list (d (n "image") (r "^0.23") (d #t) (k 2)) (d (n "openh264-sys2") (r "^0.1") (d #t) (k 0)))) (h "1v6dk3b9k0ypswi2zicwhmpn2j082ans8jg0wbz3r21g2ipl0a32")))

(define-public crate-openh264-0.1.11 (c (n "openh264") (v "0.1.11") (d (list (d (n "image") (r "^0.23") (d #t) (k 2)) (d (n "openh264-sys2") (r "^0.1") (d #t) (k 0)))) (h "1gmgk96wdc03s22jpwcb5fcrqvkzp015yi8vimjp78xijzbchvmi")))

(define-public crate-openh264-0.1.12 (c (n "openh264") (v "0.1.12") (d (list (d (n "image") (r "^0.23") (d #t) (k 2)) (d (n "openh264-sys2") (r "^0.1") (d #t) (k 0)))) (h "0i7z5z8239ri04vsh21kh913m2lj1vqmp8l15xj4pkgiy2jgk00i")))

(define-public crate-openh264-0.1.13 (c (n "openh264") (v "0.1.13") (d (list (d (n "image") (r "^0.23") (d #t) (k 2)) (d (n "openh264-sys2") (r "^0.1") (d #t) (k 0)))) (h "0xp2hxhwj0gbp3ky8jdsygzwzmb40mnh72z14ifb1nn8mjbn2ayq")))

(define-public crate-openh264-0.1.14 (c (n "openh264") (v "0.1.14") (d (list (d (n "image") (r "^0.23") (d #t) (k 2)) (d (n "openh264-sys2") (r "^0.1") (d #t) (k 0)))) (h "0xvyra02x3s83mcbzrs6q8kk0pwa3rjh8aphbb3cga19d2816xkz") (f (quote (("backtrace"))))))

(define-public crate-openh264-0.1.15 (c (n "openh264") (v "0.1.15") (d (list (d (n "openh264-sys2") (r "^0.1") (d #t) (k 0)))) (h "04q9nx9n9ydvm0cmr2yw2yfaji7c3w8yd9bmlfi1izcif32gq9hk") (f (quote (("backtrace"))))))

(define-public crate-openh264-0.1.17 (c (n "openh264") (v "0.1.17") (d (list (d (n "openh264-sys2") (r "^0.1") (k 0)))) (h "08lm64v5i2w8bx8nlg7wzj9y5rdzib0rbp4dmj4jy0ilbi772ias") (f (quote (("encoder" "openh264-sys2/encoder") ("default" "decoder" "encoder") ("decoder" "openh264-sys2/decoder") ("backtrace"))))))

(define-public crate-openh264-0.2.2 (c (n "openh264") (v "0.2.2") (d (list (d (n "openh264-sys2") (r "^0.1") (k 0)))) (h "1821jjrdibypq8dz6s296sq71m86s3a7872185h8dfqm90z1bdmd") (f (quote (("encoder" "openh264-sys2/encoder") ("default" "decoder" "encoder") ("decoder" "openh264-sys2/decoder") ("backtrace"))))))

(define-public crate-openh264-0.2.3 (c (n "openh264") (v "0.2.3") (d (list (d (n "openh264-sys2") (r "^0.1") (k 0)))) (h "1kiadmfwdvpqdiy07ghpdkspdrk3xpx463ldn1ifwp2hw1lsw2ag") (f (quote (("encoder" "openh264-sys2/encoder") ("default" "decoder" "encoder") ("decoder" "openh264-sys2/decoder") ("backtrace"))))))

(define-public crate-openh264-0.2.4 (c (n "openh264") (v "0.2.4") (d (list (d (n "openh264-sys2") (r "^0.1") (k 0)))) (h "0gnc6mw6byp90cv3zv0rbxxrq55d7qy9bv7pk96ya52nfgx0h6q6") (f (quote (("encoder" "openh264-sys2/encoder") ("default" "decoder" "encoder") ("decoder" "openh264-sys2/decoder") ("backtrace"))))))

(define-public crate-openh264-0.2.5 (c (n "openh264") (v "0.2.5") (d (list (d (n "openh264-sys2") (r "^0.2.5") (k 0)))) (h "1n2fbmyymdlsjz5hs25zwzbpand9bpvzardf48w6aqb540sg5yhg") (f (quote (("encoder" "openh264-sys2/encoder") ("default" "decoder" "encoder") ("decoder" "openh264-sys2/decoder") ("backtrace") ("asm" "openh264-sys2/asm"))))))

(define-public crate-openh264-0.2.7 (c (n "openh264") (v "0.2.7") (d (list (d (n "openh264-sys2") (r "^0.2.5") (k 0)))) (h "17xxa589cx051jy9di3fqnlkrrdhylxbnhq4jswzs4djyvkk3ash") (f (quote (("encoder" "openh264-sys2/encoder") ("default" "decoder" "encoder") ("decoder" "openh264-sys2/decoder") ("backtrace") ("asm" "openh264-sys2/asm"))))))

(define-public crate-openh264-0.2.8 (c (n "openh264") (v "0.2.8") (d (list (d (n "openh264-sys2") (r "^0.2.5") (k 0)))) (h "140yqra3paxc523xbs49w898mwz5fja5jvb2kpadr7vgq10pnvf2") (f (quote (("encoder" "openh264-sys2/encoder") ("default" "decoder" "encoder") ("decoder" "openh264-sys2/decoder") ("backtrace") ("asm" "openh264-sys2/asm"))))))

(define-public crate-openh264-0.2.9 (c (n "openh264") (v "0.2.9") (d (list (d (n "openh264-sys2") (r "^0.2.5") (k 0)))) (h "0sg37fbd4pf264jfiqhdr41v7s618k909nr6kbdm1745fr0y0ip1") (f (quote (("encoder" "openh264-sys2/encoder") ("default" "decoder" "encoder") ("decoder" "openh264-sys2/decoder") ("backtrace") ("asm" "openh264-sys2/asm"))))))

(define-public crate-openh264-0.2.10 (c (n "openh264") (v "0.2.10") (d (list (d (n "openh264-sys2") (r "^0.2.5") (k 0)))) (h "0asbmjn0xpi9b0cwlal19lcjwq42nf893hqnawqr4xcr2494kl0p") (f (quote (("encoder" "openh264-sys2/encoder") ("default" "decoder" "encoder") ("decoder" "openh264-sys2/decoder") ("backtrace") ("asm" "openh264-sys2/asm"))))))

(define-public crate-openh264-0.2.11 (c (n "openh264") (v "0.2.11") (d (list (d (n "openh264-sys2") (r "^0.2.5") (k 0)))) (h "1jxqlk990aayfs6lk213d7x648a7jv60pxsdzxb1h3dzrvw92f9x") (f (quote (("encoder" "openh264-sys2/encoder") ("default" "decoder" "encoder") ("decoder" "openh264-sys2/decoder") ("backtrace") ("asm" "openh264-sys2/asm"))))))

(define-public crate-openh264-0.2.12 (c (n "openh264") (v "0.2.12") (d (list (d (n "openh264-sys2") (r "^0.2.5") (k 0)))) (h "1im1jcgxdj6dm05wjqj9m6bwc8pz720wlk8viam2h9cgmvcxnh6v") (f (quote (("encoder" "openh264-sys2/encoder") ("default" "decoder" "encoder") ("decoder" "openh264-sys2/decoder") ("backtrace") ("asm" "openh264-sys2/asm"))))))

(define-public crate-openh264-0.2.13 (c (n "openh264") (v "0.2.13") (d (list (d (n "openh264-sys2") (r "^0.2.5") (k 0)))) (h "0qygiqshrkcl312hwbn5f2g7jpx2wr88z4ps6l87a83swknblj1b") (f (quote (("encoder" "openh264-sys2/encoder") ("default" "decoder" "encoder") ("decoder" "openh264-sys2/decoder") ("backtrace") ("asm" "openh264-sys2/asm"))))))

(define-public crate-openh264-0.2.15 (c (n "openh264") (v "0.2.15") (d (list (d (n "openh264-sys2") (r "^0.2.5") (k 0)))) (h "1v6vgannja5g7m78ir20068i2qrkrdmkg8ryybf51l5qighmwds7") (f (quote (("encoder" "openh264-sys2/encoder") ("default" "decoder" "encoder") ("decoder" "openh264-sys2/decoder") ("backtrace") ("asm" "openh264-sys2/asm"))))))

(define-public crate-openh264-0.2.16 (c (n "openh264") (v "0.2.16") (d (list (d (n "openh264-sys2") (r "^0.2.5") (k 0)))) (h "07i3fa5mkiylxjpszaxddrwmg0c1a3qxkjnd2rj3s264invcwqj2") (f (quote (("encoder" "openh264-sys2/encoder") ("default" "decoder" "encoder") ("decoder" "openh264-sys2/decoder") ("backtrace") ("asm" "openh264-sys2/asm"))))))

(define-public crate-openh264-0.2.18 (c (n "openh264") (v "0.2.18") (d (list (d (n "openh264-sys2") (r "^0.2.5") (k 0)))) (h "16jhr8ccrnp8qj76w1i7vpp8pi6gp70i7hnz0m82qn7az48xcx9q") (f (quote (("encoder" "openh264-sys2/encoder") ("default" "decoder" "encoder") ("decoder" "openh264-sys2/decoder") ("backtrace") ("asm" "openh264-sys2/asm"))))))

(define-public crate-openh264-0.3.0 (c (n "openh264") (v "0.3.0") (d (list (d (n "image") (r "^0.24.4") (d #t) (k 2)) (d (n "image-compare") (r "^0.2.3") (d #t) (k 2)) (d (n "openh264-sys2") (r "^0.3.0") (k 0)))) (h "0cs1p3z2v7pp31rdwzjyrg7aszw01zibjrrqg34kngv4x1gbfzas") (f (quote (("encoder" "openh264-sys2/encoder") ("default" "decoder" "encoder") ("decoder" "openh264-sys2/decoder") ("backtrace"))))))

(define-public crate-openh264-0.3.1 (c (n "openh264") (v "0.3.1") (d (list (d (n "image") (r "^0.24.4") (d #t) (k 2)) (d (n "image-compare") (r "^0.2.3") (d #t) (k 2)) (d (n "openh264-sys2") (r "^0.3.0") (k 0)))) (h "1s277z3mi8p5diin7pa4902g3g7ly1487k9m694n5ihjkpv5nhsg") (f (quote (("encoder" "openh264-sys2/encoder") ("default" "decoder" "encoder") ("decoder" "openh264-sys2/decoder") ("backtrace"))))))

(define-public crate-openh264-0.3.2 (c (n "openh264") (v "0.3.2") (d (list (d (n "image") (r "^0.24.4") (d #t) (k 2)) (d (n "image-compare") (r "^0.2.3") (d #t) (k 2)) (d (n "openh264-sys2") (r "^0.3.0") (k 0)))) (h "1chkkvvs05zcssamap9lf7d8ac54xy6bha3aw3sfnwkz5d68hvza") (f (quote (("encoder" "openh264-sys2/encoder") ("default" "decoder" "encoder") ("decoder" "openh264-sys2/decoder") ("backtrace")))) (r "1.65")))

(define-public crate-openh264-0.4.0 (c (n "openh264") (v "0.4.0") (d (list (d (n "image") (r "^0.24.4") (d #t) (k 2)) (d (n "image-compare") (r "^0.2.3") (d #t) (k 2)) (d (n "mp4") (r "^0.13.0") (d #t) (k 2)) (d (n "openh264-sys2") (r "^0.4.0") (k 0)))) (h "0xjv3rdwnmff3zv8kza9jfk1jysy8mhzbqki1cxxdl4s8a2cdvbs") (f (quote (("encoder" "openh264-sys2/encoder") ("default" "decoder" "encoder") ("decoder" "openh264-sys2/decoder") ("backtrace")))) (r "1.65")))

(define-public crate-openh264-0.4.1 (c (n "openh264") (v "0.4.1") (d (list (d (n "image") (r "^0.24.4") (d #t) (k 2)) (d (n "image-compare") (r "^0.2.3") (d #t) (k 2)) (d (n "mp4") (r "^0.13.0") (d #t) (k 2)) (d (n "openh264-sys2") (r "^0.4.0") (k 0)))) (h "1p0452yw32yg1ic8sxvd3x3jag8dyika6bdf15l69yn3w38rj2v0") (f (quote (("encoder" "openh264-sys2/encoder") ("default" "decoder" "encoder") ("decoder" "openh264-sys2/decoder") ("backtrace")))) (r "1.65")))

(define-public crate-openh264-0.4.2 (c (n "openh264") (v "0.4.2") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 2)) (d (n "image") (r "^0.24.6") (d #t) (k 2)) (d (n "image-compare") (r "^0.3.0") (d #t) (k 2)) (d (n "mp4") (r "^0.13.0") (d #t) (k 2)) (d (n "openh264-sys2") (r "^0.4.0") (k 0)))) (h "0gbymnrwqrd9ijgjmp0pqgrzs31vpyhsj20532zzwpb0097b7p2p") (f (quote (("encoder" "openh264-sys2/encoder") ("default" "decoder" "encoder") ("decoder" "openh264-sys2/decoder") ("backtrace")))) (r "1.65")))

(define-public crate-openh264-0.4.3 (c (n "openh264") (v "0.4.3") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 2)) (d (n "image") (r "^0.24.6") (d #t) (k 2)) (d (n "image-compare") (r "^0.3.0") (d #t) (k 2)) (d (n "mp4") (r "^0.13.0") (d #t) (k 2)) (d (n "openh264-sys2") (r "^0.4.0") (k 0)))) (h "1fnpg34vfilpy2w5ram1ascfdbsh6hlvh8gyxwmp5r82k2gwx8x6") (f (quote (("encoder" "openh264-sys2/encoder") ("default" "decoder" "encoder") ("decoder" "openh264-sys2/decoder") ("backtrace")))) (r "1.65")))

(define-public crate-openh264-0.4.4 (c (n "openh264") (v "0.4.4") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 2)) (d (n "image") (r "^0.24.6") (d #t) (k 2)) (d (n "image-compare") (r "^0.3.0") (d #t) (k 2)) (d (n "mp4") (r "^0.13.0") (d #t) (k 2)) (d (n "openh264-sys2") (r "^0.4.0") (k 0)))) (h "1ryslc2axs0flq6i9shwjw6w14qbxlj77pjc7rcwadmv94j66bp4") (f (quote (("encoder" "openh264-sys2/encoder") ("default" "decoder" "encoder") ("decoder" "openh264-sys2/decoder") ("backtrace")))) (r "1.65")))

(define-public crate-openh264-0.5.0 (c (n "openh264") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 2)) (d (n "image") (r "^0.24.6") (d #t) (k 2)) (d (n "image-compare") (r "^0.3.0") (d #t) (k 2)) (d (n "mp4") (r "^0.14.0") (d #t) (k 2)) (d (n "openh264-sys2") (r "^0.5.0") (k 0)))) (h "0vy9gvrngjcl0jaqpiqf8825law938w5jwysmhhlhd2cgrs0xx06") (f (quote (("source" "openh264-sys2/source") ("libloading" "openh264-sys2/libloading") ("default" "source") ("backtrace")))) (r "1.65")))

(define-public crate-openh264-0.6.0 (c (n "openh264") (v "0.6.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 2)) (d (n "image") (r "^0.24.6") (d #t) (k 2)) (d (n "image-compare") (r "^0.3.0") (d #t) (k 2)) (d (n "mp4") (r "^0.14.0") (d #t) (k 2)) (d (n "openh264-sys2") (r "^0.6.0") (k 0)))) (h "1isf5jjlm0159zbpghq9447iqagq94zxjkb5mmxgk7084w0id9ff") (f (quote (("source" "openh264-sys2/source") ("libloading" "openh264-sys2/libloading") ("default" "source") ("backtrace")))) (r "1.65")))

