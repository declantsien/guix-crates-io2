(define-module (crates-io op en opentelemetry-resource-detectors) #:use-module (crates-io))

(define-public crate-opentelemetry-resource-detectors-0.1.0 (c (n "opentelemetry-resource-detectors") (v "0.1.0") (d (list (d (n "opentelemetry") (r "^0.22") (d #t) (k 0)) (d (n "opentelemetry-semantic-conventions") (r "^0.14") (d #t) (k 0)) (d (n "opentelemetry_sdk") (r "^0.22") (k 0)))) (h "1071iq5304pmcs2z6vz5p1mx7v80pfy8qsv5f9rdkhb0prnvcbr5") (r "1.65")))

