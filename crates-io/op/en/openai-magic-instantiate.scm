(define-module (crates-io op en openai-magic-instantiate) #:use-module (crates-io))

(define-public crate-openai-magic-instantiate-0.1.0 (c (n "openai-magic-instantiate") (v "0.1.0") (d (list (d (n "async-openai") (r "^0.18") (d #t) (k 0)) (d (n "openai-magic-instantiate-derive") (r "^0.1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "1yk85f2323ngnv6ndbn1clhj1h1s3fpks28lbp98zf6mksr23hk6")))

(define-public crate-openai-magic-instantiate-0.1.1 (c (n "openai-magic-instantiate") (v "0.1.1") (d (list (d (n "async-openai") (r "^0.18") (d #t) (k 0)) (d (n "openai-magic-instantiate-derive") (r "^0.1.1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "1qykm0i3g2j6fm5aya5znqz5jn2f00n3x9j9pl0w2bn4w988x8ys")))

(define-public crate-openai-magic-instantiate-0.1.2 (c (n "openai-magic-instantiate") (v "0.1.2") (d (list (d (n "async-openai") (r "^0.18") (d #t) (k 0)) (d (n "openai-magic-instantiate-derive") (r "^0.1.1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "0j38n4znl7paf7h5xwmn7lsfmn7m4fwhl27mbj22wpi0jpv0m9lr")))

(define-public crate-openai-magic-instantiate-0.1.3 (c (n "openai-magic-instantiate") (v "0.1.3") (d (list (d (n "async-openai") (r "^0.18") (d #t) (k 0)) (d (n "openai-magic-instantiate-derive") (r "^0.1.2") (d #t) (k 0)) (d (n "serde_json") (r "^1") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "0r8236cnwx2yckr8q5dhhkpaqhlk9nk6yvhwyvyhc61v16q49acr")))

(define-public crate-openai-magic-instantiate-0.1.4 (c (n "openai-magic-instantiate") (v "0.1.4") (d (list (d (n "async-openai") (r "^0.18") (d #t) (k 0)) (d (n "openai-magic-instantiate-derive") (r "^0.1.3") (d #t) (k 0)) (d (n "serde_json") (r "^1") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "15k30bh1fx3hap8rpga3yy2i92rfqi442lz1bxy1326hmxnmigga")))

(define-public crate-openai-magic-instantiate-0.1.5 (c (n "openai-magic-instantiate") (v "0.1.5") (d (list (d (n "async-openai") (r "^0.18") (d #t) (k 0)) (d (n "openai-magic-instantiate-derive") (r "^0.1.4") (d #t) (k 0)) (d (n "serde_json") (r "^1") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "1bcs4g7nq319v7jpp0z1qsbc6r6pxkl6p3v7bphiaxnxz59hxgr7")))

(define-public crate-openai-magic-instantiate-0.1.6 (c (n "openai-magic-instantiate") (v "0.1.6") (d (list (d (n "async-openai") (r "^0.18") (d #t) (k 0)) (d (n "openai-magic-instantiate-derive") (r "^0.1.5") (d #t) (k 0)) (d (n "serde_json") (r "^1") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "1wfn6vpq9py53z8six2plv72xdhkzaj823mpf2qcaf3j01jia9cf")))

(define-public crate-openai-magic-instantiate-0.1.7 (c (n "openai-magic-instantiate") (v "0.1.7") (d (list (d (n "async-openai") (r "^0.18") (d #t) (k 0)) (d (n "openai-magic-instantiate-derive") (r "^0.1.6") (d #t) (k 0)) (d (n "serde_json") (r "^1") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "1msz7nzcbr101y86qjy8zf9ajzhmw4xyzmkddq82a8ss55n8ffkn")))

(define-public crate-openai-magic-instantiate-0.2.0 (c (n "openai-magic-instantiate") (v "0.2.0") (d (list (d (n "async-openai") (r "^0.18") (d #t) (k 0)) (d (n "openai-magic-instantiate-derive") (r "^0.2.0") (d #t) (k 0)) (d (n "serde_json") (r "^1") (f (quote ("preserve_order"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1v34pzi28mc0i2vldg17g0pqsrs5m75f7nvgbn6x712p6gh0dplk")))

(define-public crate-openai-magic-instantiate-0.2.1 (c (n "openai-magic-instantiate") (v "0.2.1") (d (list (d (n "async-openai") (r "^0.18") (d #t) (k 0)) (d (n "openai-magic-instantiate-derive") (r "^0.2.0") (d #t) (k 0)) (d (n "serde_json") (r "^1") (f (quote ("preserve_order"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1vgvyv6ybkqdfkbw2dg4diyzj6dcigvfj0c0cxq11vs7csxnsbzw")))

(define-public crate-openai-magic-instantiate-0.2.2 (c (n "openai-magic-instantiate") (v "0.2.2") (d (list (d (n "async-openai") (r "^0.18") (d #t) (k 0)) (d (n "openai-magic-instantiate-derive") (r "^0.2.0") (d #t) (k 0)) (d (n "serde_json") (r "^1") (f (quote ("preserve_order"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "0x93ga0pic95i8gbqhdkx91iczv2kv7bylwcspsapk68vgfdcml9")))

