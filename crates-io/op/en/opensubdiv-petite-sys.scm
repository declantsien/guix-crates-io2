(define-module (crates-io op en opensubdiv-petite-sys) #:use-module (crates-io))

(define-public crate-opensubdiv-petite-sys-0.1.0 (c (n "opensubdiv-petite-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.58.1") (d #t) (k 1)) (d (n "bitfield") (r "^0.13.2") (d #t) (k 0)) (d (n "cc") (r "^1.0.67") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "cmake") (r "^0.1.45") (d #t) (k 1)) (d (n "derive_more") (r "^0.99.13") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.1") (d #t) (k 0)))) (h "14lifyi0sw938aa1haj85n1yxf44sm24wy12vab07nnrjipf75qn") (f (quote (("ptex") ("openmp") ("opencl") ("omp") ("metal") ("cuda") ("clew"))))))

(define-public crate-opensubdiv-petite-sys-0.2.0 (c (n "opensubdiv-petite-sys") (v "0.2.0") (d (list (d (n "bitfield") (r "^0.14.0") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.10") (d #t) (k 0)) (d (n "bindgen") (r "^0.64.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.79") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "cmake") (r "^0.1.49") (d #t) (k 1)))) (h "1kj1iqib1a4j9xpf1nsz7h2qxlq8xwmsb706my39w8yd16012s2y") (f (quote (("ptex") ("openmp") ("opencl") ("omp") ("metal") ("cuda") ("clew"))))))

