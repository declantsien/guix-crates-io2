(define-module (crates-io op en openrazer) #:use-module (crates-io))

(define-public crate-openrazer-0.1.0 (c (n "openrazer") (v "0.1.0") (d (list (d (n "dbus-async") (r "^1") (d #t) (k 0)) (d (n "dbus-message-parser") (r "^2.2") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "palette") (r "^0.5") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("rt-core"))) (d #t) (k 0)) (d (n "xmltree") (r "^0.10") (d #t) (k 0)))) (h "1dvayz007v0mpddysymwkhnyrqw8hpc3hwqgamn7jrkwrl07786d")))

