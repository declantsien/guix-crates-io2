(define-module (crates-io op en open-fastrlp-derive) #:use-module (crates-io))

(define-public crate-open-fastrlp-derive-0.1.1 (c (n "open-fastrlp-derive") (v "0.1.1") (d (list (d (n "bytes") (r "^1") (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "075vckvkh0616rx4b15nzq8d8p4iqfhvhf52n3z1qg65qvjjnfq0")))

