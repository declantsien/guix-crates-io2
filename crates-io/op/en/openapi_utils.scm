(define-module (crates-io op en openapi_utils) #:use-module (crates-io))

(define-public crate-openapi_utils-0.1.1 (c (n "openapi_utils") (v "0.1.1") (d (list (d (n "http") (r "^0.1") (d #t) (k 0)) (d (n "indexmap") (r "^1.3.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "openapiv3") (r "^0.3.0") (d #t) (k 0)))) (h "129699gxgv7mm2xmfdap3njmm1rrc923d7z9xjykcrivkn28q5c1")))

(define-public crate-openapi_utils-0.1.2 (c (n "openapi_utils") (v "0.1.2") (d (list (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "indexmap") (r "^1.3.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "openapiv3") (r "^0.3.0") (d #t) (k 0)))) (h "0fnv792njv21qqmyd4mm7mn933l4q5rqfhz8zd98bk5ii25kjyg0")))

(define-public crate-openapi_utils-0.1.3 (c (n "openapi_utils") (v "0.1.3") (d (list (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "indexmap") (r "^1.3.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "openapiv3") (r "^0.3.0") (d #t) (k 0)))) (h "0x1qc2xrv9q8s9isxylj6hc86nn8rr5hhfd7g75b8p2lg7bn1vyk")))

(define-public crate-openapi_utils-0.1.4 (c (n "openapi_utils") (v "0.1.4") (d (list (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "indexmap") (r "^1.3.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "openapiv3") (r "^0.3.0") (d #t) (k 0)))) (h "1c8rxck6a46wsawkgg740n5iiby50ajz683i0601vbywzl30ly8k")))

(define-public crate-openapi_utils-0.2.0 (c (n "openapi_utils") (v "0.2.0") (d (list (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "indexmap") (r "^1.3.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "openapiv3") (r "^0.3.0") (d #t) (k 0)))) (h "0lqib4xsw29cl4jqv29p4zlmjzx9d273y8vdidc149jaxini8qkv")))

(define-public crate-openapi_utils-0.2.1 (c (n "openapi_utils") (v "0.2.1") (d (list (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "indexmap") (r "^1.3.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "openapiv3") (r "^0.3.0") (d #t) (k 0)))) (h "0bljx0qaaw0kai0xfqcj8fcysqdbwjmrp995y98mlcj3qg4fc38i")))

(define-public crate-openapi_utils-0.2.2 (c (n "openapi_utils") (v "0.2.2") (d (list (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "indexmap") (r "^1.3.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "openapiv3") (r "^0.5.0") (d #t) (k 0)))) (h "1cjinbxw56xzm62xgnpw9waahjy7jhs0r3h10fp0dm6g214cs924")))

(define-public crate-openapi_utils-0.3.0 (c (n "openapi_utils") (v "0.3.0") (d (list (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "indexmap") (r "^1.3.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "openapiv3") (r "^1.0") (d #t) (k 0)))) (h "19jc2kswa6p110jjg1zs7l4ka7w8p462i4pvviybg2mqj3n32ljc")))

(define-public crate-openapi_utils-0.4.0 (c (n "openapi_utils") (v "0.4.0") (d (list (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "indexmap") (r "^1.3.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "openapiv3") (r "^1.0") (d #t) (k 0)))) (h "0skf6xn03272zcdpabz1cmfxkavfmgj0q3hj7db5ly0pqri818v5")))

(define-public crate-openapi_utils-0.5.0 (c (n "openapi_utils") (v "0.5.0") (d (list (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "indexmap") (r "^2.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "openapiv3") (r "^1.0") (d #t) (k 0)))) (h "0mkvhsps0lpq2zq028mw9grpvfj1b5brh0zy3bv2pca6swkn2yam")))

(define-public crate-openapi_utils-0.5.1 (c (n "openapi_utils") (v "0.5.1") (d (list (d (n "http") (r "^1.0") (d #t) (k 0)) (d (n "indexmap") (r "^2.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "openapiv3") (r "^1.0") (d #t) (k 0)))) (h "1jiwj278x1fp1vj2fc8r9as1fqi86x731fic52c8qmpblkm4bz3g")))

(define-public crate-openapi_utils-0.5.2 (c (n "openapi_utils") (v "0.5.2") (d (list (d (n "http") (r "^1.0") (d #t) (k 0)) (d (n "indexmap") (r "^2.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "openapiv3") (r "^1.0") (d #t) (k 0)))) (h "1i6kqh879bzbn1n1wwyagpg28w2d3vfsfn7x3wnk63bp15m8l4a4")))

(define-public crate-openapi_utils-0.6.0 (c (n "openapi_utils") (v "0.6.0") (d (list (d (n "http") (r "^1.0") (d #t) (k 0)) (d (n "indexmap") (r "^2.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "openapiv3") (r "^2.0") (d #t) (k 0)))) (h "1l8s6820h1b4v69aqgj36zic36wp7hjg3m25xyxcksvba7nming2")))

