(define-module (crates-io op en openblas-src) #:use-module (crates-io))

(define-public crate-openblas-src-0.5.0 (c (n "openblas-src") (v "0.5.0") (h "17dlxgx2x0j3cc5n1d6kkghw0nmymrw3vp1ad6gx63g3ba96izfg") (f (quote (("system") ("static") ("default" "cblas") ("cblas"))))))

(define-public crate-openblas-src-0.5.1 (c (n "openblas-src") (v "0.5.1") (h "10d756j83lhw60ph2i65l1i0b14h51h124dy78p5wzkw869h22qv") (f (quote (("system") ("static") ("lapacke") ("default" "cblas" "lapacke") ("cblas"))))))

(define-public crate-openblas-src-0.5.2 (c (n "openblas-src") (v "0.5.2") (h "0wiw0jp7qw75rhrbfwr7qgw4h1fw06yvyzm30bc1pphsbrp7jrwm") (f (quote (("system") ("static") ("lapacke") ("default" "cblas" "lapacke") ("cblas"))))))

(define-public crate-openblas-src-0.5.3 (c (n "openblas-src") (v "0.5.3") (h "128bkqr694ban0fb98prb1nlqyvvd8kxzvh87g7la5r14dbng0va") (f (quote (("system") ("static") ("lapacke") ("default" "cblas" "lapacke") ("cblas"))))))

(define-public crate-openblas-src-0.5.4 (c (n "openblas-src") (v "0.5.4") (h "0gijd3yqjarq7wxxzqf28s00663wybnrp9lz1dm3rdm6419l56s0") (f (quote (("system") ("static") ("lapacke") ("default" "cblas" "lapacke") ("cblas"))))))

(define-public crate-openblas-src-0.5.5 (c (n "openblas-src") (v "0.5.5") (d (list (d (n "libc") (r "^0.2") (d #t) (k 2)))) (h "0yiw2m4r6sbl36shhczjhjh7dc3zvyninwydw98shdkrbzl9d572") (f (quote (("system") ("static") ("lapacke") ("default" "cblas" "lapacke") ("cblas"))))))

(define-public crate-openblas-src-0.5.6 (c (n "openblas-src") (v "0.5.6") (d (list (d (n "libc") (r "^0.2") (d #t) (k 2)))) (h "15yd985bdfc4m3v2nisv39hhm998r0r6s8yjvmvx6fn7lgy97lk8") (f (quote (("system") ("static") ("lapacke") ("default" "cblas" "lapacke") ("cblas"))))))

(define-public crate-openblas-src-0.6.0 (c (n "openblas-src") (v "0.6.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 2)))) (h "0zjspy6zihngljf6fmdiya4wilgws36m1n3c1kvw1qih9dmnbk8n") (f (quote (("system") ("static") ("lapacke") ("default" "cblas" "lapacke") ("cblas")))) (l "openblas")))

(define-public crate-openblas-src-0.6.1 (c (n "openblas-src") (v "0.6.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 2)))) (h "1bbs55s8bz2z0gcj7kh9ykxqn3x79m4cnmip7r6n5w4msyinalmg") (f (quote (("system") ("static") ("lapacke") ("default" "cblas" "lapacke") ("cblas")))) (l "openblas")))

(define-public crate-openblas-src-0.7.0 (c (n "openblas-src") (v "0.7.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 2)))) (h "1dyf7yh6rmkk7k3pgcp5p8248f08hhajkigw42bfwjw1d3jk6d8b") (f (quote (("system") ("static") ("lapacke") ("default" "cblas" "lapacke") ("cblas")))) (l "openblas")))

(define-public crate-openblas-src-0.8.0 (c (n "openblas-src") (v "0.8.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 2)))) (h "19ckf64sx9xja6nfva5a26lj0pbgbx5ly2g5324hx9p9lwbj4d93") (f (quote (("system") ("static") ("lapacke") ("default" "cblas" "lapacke") ("cblas")))) (l "openblas")))

(define-public crate-openblas-src-0.9.0 (c (n "openblas-src") (v "0.9.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 2)))) (h "0syy38a5bgv5mj6mb1n1zk1d6l5gqqrswvbmwkwx6h4z9wfrsql4") (f (quote (("system") ("static") ("lapacke") ("default" "cblas" "lapacke") ("cblas") ("cache")))) (l "openblas")))

(define-public crate-openblas-src-0.10.0 (c (n "openblas-src") (v "0.10.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 2)) (d (n "vcpkg") (r "^0.2") (d #t) (t "cfg(target_os = \"windows\")") (k 1)))) (h "0fjckphxs18af03mg1266icb0p522kzjk6i56gnbb09rp8b0ym5l") (f (quote (("system") ("static") ("lapacke") ("default" "cblas" "lapacke") ("cblas") ("cache")))) (y #t) (l "openblas")))

(define-public crate-openblas-src-0.10.1 (c (n "openblas-src") (v "0.10.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 2)) (d (n "vcpkg") (r "^0.2") (d #t) (t "cfg(target_os = \"windows\")") (k 1)))) (h "0q1shb8mhm16i1g2nizwd3kdaxlc0hyph02r791mkf6ylabb37zc") (f (quote (("system") ("static") ("lapacke") ("default" "cblas" "lapacke") ("cblas") ("cache")))) (y #t) (l "openblas")))

(define-public crate-openblas-src-0.10.2 (c (n "openblas-src") (v "0.10.2") (d (list (d (n "dirs") (r "^3.0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 2)) (d (n "openblas-build") (r "^0.1.1") (d #t) (t "cfg(target_os = \"linux\")") (k 1)) (d (n "vcpkg") (r "^0.2") (d #t) (t "cfg(target_os = \"windows\")") (k 1)))) (h "1rxhhaazx3b08d4nj0x9w3wlii1ivccm1mm6nagkflp7rgax34z9") (f (quote (("system") ("static") ("lapacke") ("default" "cblas" "lapacke") ("cblas") ("cache")))) (l "openblas")))

(define-public crate-openblas-src-0.10.3 (c (n "openblas-src") (v "0.10.3") (d (list (d (n "dirs") (r "^3.0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 2)) (d (n "openblas-build") (r "^0.1.1") (d #t) (t "cfg(target_os = \"linux\")") (k 1)) (d (n "vcpkg") (r "^0.2") (d #t) (t "cfg(target_os = \"windows\")") (k 1)))) (h "14aw6alvy5ba61n2cxi0k7lws29jrgxan1b6kfnqzrjwqqkbdzik") (f (quote (("system") ("static") ("lapacke") ("default" "cblas" "lapacke") ("cblas") ("cache")))) (l "openblas")))

(define-public crate-openblas-src-0.10.4 (c (n "openblas-src") (v "0.10.4") (d (list (d (n "dirs") (r "^3.0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 2)) (d (n "openblas-build") (r "^0.1.1") (d #t) (t "cfg(target_os = \"linux\")") (k 1)) (d (n "vcpkg") (r "^0.2") (d #t) (t "cfg(target_os = \"windows\")") (k 1)))) (h "0s8wskanmlp9l0a3d5dlw7wvw6ap70vlg2whxd15kawc61xrd81m") (f (quote (("system") ("static") ("lapacke") ("default" "cblas" "lapacke") ("cblas") ("cache")))) (l "openblas")))

(define-public crate-openblas-src-0.10.5 (c (n "openblas-src") (v "0.10.5") (d (list (d (n "dirs") (r "^3.0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 2)) (d (n "openblas-build") (r "^0.10.5") (d #t) (t "cfg(target_os = \"linux\")") (k 1)) (d (n "vcpkg") (r "^0.2") (d #t) (t "cfg(target_os = \"windows\")") (k 1)))) (h "0rwdl4qszpbgr6mk6gvl54lhcz3ahqvymmidwpicdlabi5an60gr") (f (quote (("system") ("static") ("lapacke") ("default" "cblas" "lapacke") ("cblas") ("cache")))) (l "openblas")))

(define-public crate-openblas-src-0.10.7 (c (n "openblas-src") (v "0.10.7") (d (list (d (n "dirs") (r "^3.0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 2)) (d (n "openblas-build") (r "^0.10.7") (d #t) (k 1)) (d (n "vcpkg") (r "^0.2") (d #t) (t "cfg(target_os = \"windows\")") (k 1)))) (h "05v3anrjpbx5hc56rgi4hphwcay5q3iqj93sfpjj282xaclg39lv") (f (quote (("system") ("static") ("lapacke") ("default" "cblas" "lapacke") ("cblas") ("cache")))) (l "openblas")))

(define-public crate-openblas-src-0.10.8 (c (n "openblas-src") (v "0.10.8") (d (list (d (n "dirs") (r "^3.0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 2)) (d (n "openblas-build") (r "^0.10.8") (d #t) (k 1)) (d (n "vcpkg") (r "^0.2") (d #t) (t "cfg(target_os = \"windows\")") (k 1)))) (h "00850xjbph72wfgw7irxvsq77njp865sinkl2pzc4ykh1fpxir9q") (f (quote (("system") ("static") ("lapacke") ("default" "cblas" "lapacke") ("cblas") ("cache")))) (l "openblas")))

(define-public crate-openblas-src-0.10.9 (c (n "openblas-src") (v "0.10.9") (d (list (d (n "dirs") (r "^3.0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 2)) (d (n "openblas-build") (r "^0.10.9") (d #t) (k 1)) (d (n "vcpkg") (r "^0.2") (d #t) (t "cfg(target_os=\"windows\")") (k 1)))) (h "0qvkadgr05kk27x58rlvhrj6g0zjrm9ahm22vc9i0sknkxj5hjda") (f (quote (("system") ("static") ("lapacke") ("default" "cblas" "lapacke") ("cblas") ("cache")))) (l "openblas")))

