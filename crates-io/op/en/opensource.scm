(define-module (crates-io op en opensource) #:use-module (crates-io))

(define-public crate-opensource-0.1.0 (c (n "opensource") (v "0.1.0") (d (list (d (n "hyper") (r "0.9.*") (d #t) (k 0)) (d (n "serde") (r "0.7.*") (d #t) (k 0)) (d (n "serde_json") (r "0.7.*") (d #t) (k 0)) (d (n "serde_macros") (r "0.7.*") (d #t) (k 0)) (d (n "url") (r "1.1.*") (d #t) (k 0)))) (h "1b04f5p54wav30v3pzxbsac033ir176wd0xk7nz220w18hq8cv76")))

(define-public crate-opensource-0.1.1 (c (n "opensource") (v "0.1.1") (d (list (d (n "hyper") (r "0.9.*") (d #t) (k 0)) (d (n "serde") (r "0.7.*") (d #t) (k 0)) (d (n "serde_json") (r "0.7.*") (d #t) (k 0)) (d (n "serde_macros") (r "0.7.*") (d #t) (k 0)) (d (n "url") (r "1.1.*") (d #t) (k 0)))) (h "0k64sf5p8zqbi3rrvyg3lpmrfbddcrrkfvaqylw91as1pvhnqzxv")))

(define-public crate-opensource-0.2.0 (c (n "opensource") (v "0.2.0") (d (list (d (n "reqwest") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "1.0.*") (d #t) (k 0)) (d (n "serde_derive") (r "1.0.*") (d #t) (k 0)) (d (n "serde_json") (r "1.0.*") (d #t) (k 0)) (d (n "url") (r "1.6.*") (d #t) (k 0)))) (h "1wvvwmgpipndysv16fv5994m5zdrsd8ipl0j3g3az7awplgmfgz0")))

