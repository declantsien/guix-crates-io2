(define-module (crates-io op en open_configuration) #:use-module (crates-io))

(define-public crate-open_configuration-1.3.0 (c (n "open_configuration") (v "1.3.0") (d (list (d (n "schemars") (r "^0.8.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.78") (d #t) (k 0)) (d (n "serde_repr") (r "^0.1.7") (d #t) (k 0)))) (h "0374p8lh6z4px5q1knjz5ld215lgna19pc82zz90m7qp7a53sgg5") (y #t)))

(define-public crate-open_configuration-1.3.1 (c (n "open_configuration") (v "1.3.1") (d (list (d (n "insta") (r "^1.13.0") (d #t) (k 2)) (d (n "schemars") (r "^0.8.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.78") (d #t) (k 0)) (d (n "serde_repr") (r "^0.1.7") (d #t) (k 0)))) (h "01zs1v4yxxkhqxmsp6xaly07vvl9d62719bkiharznbj896716sz") (y #t)))

(define-public crate-open_configuration-1.3.2 (c (n "open_configuration") (v "1.3.2") (d (list (d (n "insta") (r "^1.13.0") (d #t) (k 2)) (d (n "schemars") (r "^0.8.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.78") (d #t) (k 0)) (d (n "serde_repr") (r "^0.1.7") (d #t) (k 0)))) (h "1pmxck39dpqag0m3qvbszvc3scj9z0na84lra05fwdsvp9gajn6l") (y #t)))

