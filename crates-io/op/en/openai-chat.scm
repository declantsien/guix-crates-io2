(define-module (crates-io op en openai-chat) #:use-module (crates-io))

(define-public crate-openai-chat-0.1.0 (c (n "openai-chat") (v "0.1.0") (d (list (d (n "actix-rt") (r "^2.7.0") (d #t) (k 0)) (d (n "openai_api_client") (r "^0.5.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.14") (f (quote ("socks" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "06j7z2pfi4c5dr8vxsi0ajf8i6c0p2cfjb2dfwrs111j1hvwa1lj")))

(define-public crate-openai-chat-0.1.1 (c (n "openai-chat") (v "0.1.1") (d (list (d (n "actix-rt") (r "^2.7.0") (d #t) (k 0)) (d (n "openai_api_client") (r "^0.5.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.14") (f (quote ("socks" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "1kd43cgyb2fwb1sx0lxr9jvx25wdy3sg08b4qazyvq7jcgjy6scq")))

