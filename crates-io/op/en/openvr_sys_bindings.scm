(define-module (crates-io op en openvr_sys_bindings) #:use-module (crates-io))

(define-public crate-openvr_sys_bindings-2.0.3 (c (n "openvr_sys_bindings") (v "2.0.3") (d (list (d (n "bindgen") (r "^0.69.1") (o #t) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1cli6fbqz0nv3j7dh0b7jb25ijnzzc173pdhmc9z764xpq2sqny9") (f (quote (("default") ("buildtime_bindgen" "bindgen"))))))

