(define-module (crates-io op en openv) #:use-module (crates-io))

(define-public crate-openv-0.1.0 (c (n "openv") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.37") (d #t) (k 0)) (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "mockall") (r "^0.9.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.118") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)))) (h "0d6sjb41925cx7gbdlhjxd3rza3w8qcvx5a7n6j1540ylc5pwb2n")))

