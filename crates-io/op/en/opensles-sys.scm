(define-module (crates-io op en opensles-sys) #:use-module (crates-io))

(define-public crate-opensles-sys-0.0.1 (c (n "opensles-sys") (v "0.0.1") (d (list (d (n "bindgen") (r "^0.26.3") (d #t) (k 1)))) (h "1z111a9k58khs4p6qdg0dgvgq0lcxhamq32arq58crw9zf91w9km")))

(define-public crate-opensles-sys-0.0.2 (c (n "opensles-sys") (v "0.0.2") (d (list (d (n "bindgen") (r "^0.26.3") (d #t) (k 1)))) (h "09zg0xk3l4q9pn7yjd8m7yihjbx3yjpq3z3fjmgz9w76vwa5izd8")))

(define-public crate-opensles-sys-0.0.3 (c (n "opensles-sys") (v "0.0.3") (d (list (d (n "bindgen") (r "^0.26.3") (d #t) (k 1)))) (h "03y5z5valkjp0dl436j1gsas1r5w0md4zh27bbbyb5lrdraw5bj1")))

(define-public crate-opensles-sys-0.0.5 (c (n "opensles-sys") (v "0.0.5") (d (list (d (n "bindgen") (r "^0.37.0") (d #t) (k 1)))) (h "00j4xprhf48qy0gwhc1knscp1v27d1vhhyx5gmj75x7aj1ndg84r") (l "OpenSLES")))

(define-public crate-opensles-sys-0.0.6 (c (n "opensles-sys") (v "0.0.6") (d (list (d (n "bindgen") (r "^0.40.0") (d #t) (k 1)))) (h "1aijj11x0mx3ya0kwpn6hwpsjll7k93b0nz78a0ymwgfbf90gjnz") (l "OpenSLES")))

