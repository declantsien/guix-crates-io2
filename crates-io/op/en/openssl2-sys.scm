(define-module (crates-io op en openssl2-sys) #:use-module (crates-io))

(define-public crate-openssl2-sys-0.0.1 (c (n "openssl2-sys") (v "0.0.1") (d (list (d (n "pkg-config") (r "*") (d #t) (k 0)))) (h "06jija0ydpxpq39wxpqnb7xv7yy5p5sv9gqg1j7cwazh26bw1j0x") (f (quote (("tlsv1_2") ("tlsv1_1") ("sslv2") ("aes_xts"))))))

(define-public crate-openssl2-sys-1.0.0 (c (n "openssl2-sys") (v "1.0.0") (d (list (d (n "pkg-config") (r "*") (d #t) (k 0)))) (h "0fpjyc29kvw0y2giavy9l1bx7gld4hqf3hgfpxynbw0qhrvrfh78") (f (quote (("tlsv1_2") ("tlsv1_1") ("sslv2") ("aes_xts"))))))

