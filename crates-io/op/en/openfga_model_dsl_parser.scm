(define-module (crates-io op en openfga_model_dsl_parser) #:use-module (crates-io))

(define-public crate-openfga_model_dsl_parser-0.1.1 (c (n "openfga_model_dsl_parser") (v "0.1.1") (d (list (d (n "chumsky") (r "^0.8.0") (d #t) (k 0)) (d (n "openfga_common") (r "^0.2.0") (d #t) (k 0)))) (h "17s44nmsgbdggqwkir5387kb0qr1pyz4qlmvydajc7p9lsny8m8p")))

