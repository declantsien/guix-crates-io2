(define-module (crates-io op en openrtb-native1) #:use-module (crates-io))

(define-public crate-openrtb-native1-0.1.0 (c (n "openrtb-native1") (v "0.1.0") (d (list (d (n "default-ext") (r "^0.1") (d #t) (k 0)) (d (n "json-ext") (r "^0.3") (d #t) (k 0)) (d (n "openrtb2") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "162f9nc6n4nybqpmxc6m1plj5q5vgln9kj5g3yg94jzkv9ffyqd4")))

(define-public crate-openrtb-native1-0.2.0 (c (n "openrtb-native1") (v "0.2.0") (d (list (d (n "default-ext") (r "^0.1") (d #t) (k 0)) (d (n "openrtb2") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1ibdbi3q80d35w9zkyvxanm3fslqmc9mf8w4ds87lxcmix2gpgsh")))

