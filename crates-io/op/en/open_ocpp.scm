(define-module (crates-io op en open_ocpp) #:use-module (crates-io))

(define-public crate-open_ocpp-0.1.0 (c (n "open_ocpp") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.83") (d #t) (k 0)))) (h "1gwgpqn19p4l6ihkc5yi771bjm3wq4q7rn0ac57gbjq9wnvlqnx6")))

(define-public crate-open_ocpp-0.1.1 (c (n "open_ocpp") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.83") (d #t) (k 0)) (d (n "serde_test") (r "^1.0.144") (d #t) (k 0)))) (h "0vbsm64cxj2dwar489ljh8in3lb0zcsbh14n1bv2pbkkqj12n78c")))

(define-public crate-open_ocpp-0.1.2 (c (n "open_ocpp") (v "0.1.2") (d (list (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.83") (d #t) (k 0)) (d (n "serde_test") (r "^1.0.144") (d #t) (k 0)))) (h "1gka0bgk2yakpavbyg05gy1f4cyyfp3pswgmklcfng46ll7pc1bf")))

