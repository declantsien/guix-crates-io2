(define-module (crates-io op en openai_client) #:use-module (crates-io))

(define-public crate-openai_client-0.1.0 (c (n "openai_client") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.59") (d #t) (k 0)) (d (n "derive_builder") (r "^0.12.0") (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 2)) (d (n "reqwest") (r "^0.11.13") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)) (d (n "tokio") (r "^1.22.0") (f (quote ("macros"))) (d #t) (k 0)) (d (n "wiremock") (r "^0.5.15") (d #t) (k 2)))) (h "1gdc9cmb807d7jlpi2bpkx1l328lmgw8fm0bhy1s1i99hfyqvi5i")))

