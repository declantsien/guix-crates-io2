(define-module (crates-io op en opentalk-kustos-prefix-impl) #:use-module (crates-io))

(define-public crate-opentalk-kustos-prefix-impl-0.8.0-rc.1 (c (n "opentalk-kustos-prefix-impl") (v "0.8.0-rc.1") (d (list (d (n "proc-macro-crate") (r "^2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "176zd8ry59i6i7r6zg3pxfhy4b1gla4509pshk54h544i4ljic6x")))

(define-public crate-opentalk-kustos-prefix-impl-0.9.0-alpha (c (n "opentalk-kustos-prefix-impl") (v "0.9.0-alpha") (d (list (d (n "proc-macro-crate") (r "^2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1lw08f0lix6vv11i7mcikv4z5748rhfy47kcxv2q5ilh28j5fv4j")))

(define-public crate-opentalk-kustos-prefix-impl-0.9.0 (c (n "opentalk-kustos-prefix-impl") (v "0.9.0") (d (list (d (n "proc-macro-crate") (r "^3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1dzypj8117biig9z3sfa4f5ibyjnyv9brqamnv028ki5k2mp0zd5")))

(define-public crate-opentalk-kustos-prefix-impl-0.10.0-rc.1 (c (n "opentalk-kustos-prefix-impl") (v "0.10.0-rc.1") (d (list (d (n "proc-macro-crate") (r "^3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "03ziy5kqni8agdnc488513rk39vhhdb7s5db235jzz7dh9g7pwr5")))

(define-public crate-opentalk-kustos-prefix-impl-0.11.0-alpha (c (n "opentalk-kustos-prefix-impl") (v "0.11.0-alpha") (d (list (d (n "proc-macro-crate") (r "^3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0sal6r9rb5ymciz5qh0l2vn7d9lhsqjfc28nspsabq8rbs6v23d1")))

(define-public crate-opentalk-kustos-prefix-impl-0.12.0-alpha (c (n "opentalk-kustos-prefix-impl") (v "0.12.0-alpha") (d (list (d (n "proc-macro-crate") (r "^3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1mf0c4chdr035wrws3gqddrz3rbhp2h40kpmr4y1dv70nds7z0yd")))

(define-public crate-opentalk-kustos-prefix-impl-0.12.0-alpha.1 (c (n "opentalk-kustos-prefix-impl") (v "0.12.0-alpha.1") (d (list (d (n "proc-macro-crate") (r "^3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1mc2a68287hvji44kcj7pwcsxx374maralwznjlw643s9b104c4a")))

(define-public crate-opentalk-kustos-prefix-impl-0.11.0-rc.2 (c (n "opentalk-kustos-prefix-impl") (v "0.11.0-rc.2") (d (list (d (n "proc-macro-crate") (r "^3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0nww31g7k7pnsj2b6fl7x9wisqq8jvsva4c2s46d1jp676rii3fl")))

(define-public crate-opentalk-kustos-prefix-impl-0.10.0 (c (n "opentalk-kustos-prefix-impl") (v "0.10.0") (d (list (d (n "proc-macro-crate") (r "^3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0lbd2kwchkyihplzrfkscj36lvpkl4jrvnbbi9krq684jk4fcgy8")))

(define-public crate-opentalk-kustos-prefix-impl-0.13.0-alpha (c (n "opentalk-kustos-prefix-impl") (v "0.13.0-alpha") (d (list (d (n "proc-macro-crate") (r "^3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "071s7b6gan7n2c3k8nvaw8mf96l3rxw5kxzjmz5p2j5y23qza69w")))

(define-public crate-opentalk-kustos-prefix-impl-0.11.0 (c (n "opentalk-kustos-prefix-impl") (v "0.11.0") (d (list (d (n "proc-macro-crate") (r "^3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "095h7aag4pngbp47y0i62j819dvvajcp4f37f889mq75gl59a54s")))

