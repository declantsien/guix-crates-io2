(define-module (crates-io op en openlibspot-protocol) #:use-module (crates-io))

(define-public crate-openlibspot-protocol-1.0.0 (c (n "openlibspot-protocol") (v "1.0.0") (d (list (d (n "protobuf") (r "^3") (d #t) (k 0)) (d (n "protobuf-codegen") (r "^3") (d #t) (k 1)))) (h "0azj6vxhs2f3skywpr8k9d3zkg1b0xlpkdr28krccq8f1c8n0074") (y #t) (r "1.61")))

(define-public crate-openlibspot-protocol-0.6.0 (c (n "openlibspot-protocol") (v "0.6.0") (d (list (d (n "protobuf") (r "^3") (d #t) (k 0)) (d (n "protobuf-codegen") (r "^3") (d #t) (k 1)))) (h "1bq7h5kb9j0w2v7ycm5spklphcj8na2xg6yvasy1fiin7yfwfj9g") (r "1.61")))

