(define-module (crates-io op en openreal) #:use-module (crates-io))

(define-public crate-openreal-0.1.0 (c (n "openreal") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "rusb") (r "^0.9.1") (d #t) (k 0)))) (h "1zhr0j7llqvjjpa5yb28lhib6kn206gs5j6gfpi8azmas9d6cp6l") (y #t)))

(define-public crate-openreal-0.1.1 (c (n "openreal") (v "0.1.1") (h "12h63n48mflfqlkz5bq81h847qkdaz6wn4qjxz9xfgg634zhaw4c")))

