(define-module (crates-io op en open-opus) #:use-module (crates-io))

(define-public crate-open-opus-0.1.0 (c (n "open-opus") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_with") (r "^1.12") (f (quote ("macros"))) (d #t) (k 0)) (d (n "strum") (r "^0.24") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("macros"))) (d #t) (k 2)))) (h "00w4bhy1x7j4ci3wn3cmrncj9knpn6ljf7bd9p8cmvgjirvwf2f1")))

