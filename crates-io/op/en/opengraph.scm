(define-module (crates-io op en opengraph) #:use-module (crates-io))

(define-public crate-opengraph-0.1.0 (c (n "opengraph") (v "0.1.0") (d (list (d (n "html5ever") (r "^0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "110acls2v9xpy0nyvrgw7d5dxbiwl5dplrwza63mpimm37fkcsgq")))

(define-public crate-opengraph-0.1.1 (c (n "opengraph") (v "0.1.1") (d (list (d (n "html5ever") (r "^0") (d #t) (k 0)) (d (n "hyper") (r "^0") (d #t) (k 0)) (d (n "hyper-native-tls") (r "^0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1gmi0a3n8s8zhdyn4yzhaiznipwi3lg8ax3cshxw5q13gqrx7pm4")))

(define-public crate-opengraph-0.1.2 (c (n "opengraph") (v "0.1.2") (d (list (d (n "html5ever") (r "^0") (d #t) (k 0)) (d (n "hyper") (r "^0") (d #t) (k 0)) (d (n "hyper-native-tls") (r "^0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1jfrmj1ng1kn9kgb6r3x492gbafhffwd3w1kp2hj1784vyq1i91k")))

(define-public crate-opengraph-0.1.3 (c (n "opengraph") (v "0.1.3") (d (list (d (n "html5ever") (r "^0.16.0") (d #t) (k 0)) (d (n "hyper") (r "^0") (d #t) (k 0)) (d (n "hyper-native-tls") (r "^0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0kk575s5w3rmmwiz4sihsb03l2p6kfkf3bidh8chb3pq49a80rdf")))

(define-public crate-opengraph-0.1.4 (c (n "opengraph") (v "0.1.4") (d (list (d (n "html5ever") (r "^0.16.0") (d #t) (k 0)) (d (n "hyper") (r "^0.10.12") (d #t) (k 0)) (d (n "hyper-native-tls") (r "^0.2.4") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0i6hlmblxqb8s4847p05p5ph9vf75fpszni4npy4kxvmp3qi95dk")))

(define-public crate-opengraph-0.1.5 (c (n "opengraph") (v "0.1.5") (d (list (d (n "html5ever") (r "^0.18.0") (d #t) (k 0)) (d (n "hyper") (r "^0.10.12") (d #t) (k 0)) (d (n "hyper-native-tls") (r "^0.2.4") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "11x725fyxi8jmfk6fjrhwbdz1h8w3y9nv5rc05x5hi725v1jwz80")))

(define-public crate-opengraph-0.2.0 (c (n "opengraph") (v "0.2.0") (d (list (d (n "html5ever") (r "^0.22") (d #t) (k 0)) (d (n "reqwest") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1vc1vwpz9mjdmldvffppqz7lm3gsjw4qxjcyxal3a1knsss4b7ql") (f (quote (("default" "reqwest"))))))

(define-public crate-opengraph-0.2.1 (c (n "opengraph") (v "0.2.1") (d (list (d (n "html5ever") (r "^0.22") (d #t) (k 0)) (d (n "reqwest") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1m5ps67lggzj5q9v19bzikswhm6sigxjrfc29jj4bafy6zw98n1y") (f (quote (("default" "reqwest"))))))

(define-public crate-opengraph-0.2.2 (c (n "opengraph") (v "0.2.2") (d (list (d (n "html5ever") (r "^0.22") (d #t) (k 0)) (d (n "reqwest") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0mql14qhdic99czsi8i9qlvn5g4nkqfqw44hy3r07al1hsc1nh4h") (f (quote (("default" "reqwest"))))))

(define-public crate-opengraph-0.2.3 (c (n "opengraph") (v "0.2.3") (d (list (d (n "html5ever") (r "^0.22") (d #t) (k 0)) (d (n "reqwest") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1qy7m12mbxg9jsq4xaandr36rmdp02nfw173wzyc5xp9x6d548fd") (f (quote (("default" "reqwest"))))))

(define-public crate-opengraph-0.2.4 (c (n "opengraph") (v "0.2.4") (d (list (d (n "html5ever") (r "^0.22") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1kx9vdxf2fxqv4h9xg7fzw7liammnf4xrcwlknwqr0c84hxfzk93") (f (quote (("default" "reqwest"))))))

