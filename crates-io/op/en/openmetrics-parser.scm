(define-module (crates-io op en openmetrics-parser) #:use-module (crates-io))

(define-public crate-openmetrics-parser-0.1.0 (c (n "openmetrics-parser") (v "0.1.0") (d (list (d (n "auto_ops") (r "^0.3.0") (d #t) (k 0)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0dc4kp0k8jm2n7z6n9dl391f47vif3i04iicz9lq2ignw3qg2akd") (y #t)))

(define-public crate-openmetrics-parser-0.1.1 (c (n "openmetrics-parser") (v "0.1.1") (d (list (d (n "auto_ops") (r "^0.3.0") (d #t) (k 0)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1zdsgy87wfc6qaxljcb643xqkab5rnl30dwdnld04nhbbv4jvvr3") (y #t)))

(define-public crate-openmetrics-parser-0.1.2 (c (n "openmetrics-parser") (v "0.1.2") (d (list (d (n "auto_ops") (r "^0.3.0") (d #t) (k 0)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "090kz7bmqj2s6nb9fcgj2r3qbnx1vff2p92lqbdiaf3mqbyqvigi")))

(define-public crate-openmetrics-parser-0.1.3 (c (n "openmetrics-parser") (v "0.1.3") (d (list (d (n "auto_ops") (r "^0.3.0") (d #t) (k 0)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "12szh1kklhqa8wm9vnnbbiabx1hqxj0mhp1xrnpr7ddl48jgzsyn")))

(define-public crate-openmetrics-parser-0.2.0 (c (n "openmetrics-parser") (v "0.2.0") (d (list (d (n "auto_ops") (r "^0.3.0") (d #t) (k 0)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1wi5z2d18ivw2ghcjdl6mi6iak7miy6jsk6kjvsmcf7rdcq43wz1")))

(define-public crate-openmetrics-parser-0.3.0 (c (n "openmetrics-parser") (v "0.3.0") (d (list (d (n "auto_ops") (r "^0.3.0") (d #t) (k 0)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "092a835a71019vjd5j0krnr4lcfa8yi26m6qa5gr0pqr5hznhxj5")))

(define-public crate-openmetrics-parser-0.3.1 (c (n "openmetrics-parser") (v "0.3.1") (d (list (d (n "auto_ops") (r "^0.3.0") (d #t) (k 0)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0s1m40nq3q5rvm0b2rv4gbn5vyfv8hxwc822m8yvw5nls2kn2fvr")))

(define-public crate-openmetrics-parser-0.4.0 (c (n "openmetrics-parser") (v "0.4.0") (d (list (d (n "auto_ops") (r "^0.3.0") (d #t) (k 0)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0q7864lv7mi3rfm2z3r3n3cdhs9q2wx3axysmg2iqra3mz51rbsw")))

(define-public crate-openmetrics-parser-0.3.2 (c (n "openmetrics-parser") (v "0.3.2") (d (list (d (n "auto_ops") (r "^0.3.0") (d #t) (k 0)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "06fpvia563j4p17lbvpk80lsyzxabv6nfbx8wvb851g9d8qqgax2")))

(define-public crate-openmetrics-parser-0.4.1 (c (n "openmetrics-parser") (v "0.4.1") (d (list (d (n "auto_ops") (r "^0.3.0") (d #t) (k 0)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0a12hhnzliv3kzkvhi56778lm8jczqk8j4r60h8800pdglnk6zm5")))

(define-public crate-openmetrics-parser-0.4.2 (c (n "openmetrics-parser") (v "0.4.2") (d (list (d (n "auto_ops") (r "^0.3.0") (d #t) (k 0)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0vx8mdm66kjv1vk3dh3cw2pc6jkcfbjkkjqxb8vh8kdq81dhg88z")))

(define-public crate-openmetrics-parser-0.4.3 (c (n "openmetrics-parser") (v "0.4.3") (d (list (d (n "auto_ops") (r "^0.3.0") (d #t) (k 0)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "14w086rqfvhm4069d6xwddvjlrv1pjckisaz4lnfv1i926d1xlhq") (y #t)))

(define-public crate-openmetrics-parser-0.4.4 (c (n "openmetrics-parser") (v "0.4.4") (d (list (d (n "auto_ops") (r "^0.3.0") (d #t) (k 0)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1jjfhyxzz51pwbn7hk7izh3gbpg8slxsywk45zndzi895v36h2p4")))

