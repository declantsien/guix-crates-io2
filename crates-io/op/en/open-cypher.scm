(define-module (crates-io op en open-cypher) #:use-module (crates-io))

(define-public crate-open-cypher-0.1.0 (c (n "open-cypher") (v "0.1.0") (d (list (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)))) (h "0aahrv81iv40l082zm74n969sz255p9i02wrc6yx14qk996nkk2j")))

(define-public crate-open-cypher-0.1.1 (c (n "open-cypher") (v "0.1.1") (d (list (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)))) (h "18x49i7v2j884v6n0rkp9mz42310b1qjqv24s2kavqjv70bam0cz")))

