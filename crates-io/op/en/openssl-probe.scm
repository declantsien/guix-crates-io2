(define-module (crates-io op en openssl-probe) #:use-module (crates-io))

(define-public crate-openssl-probe-0.1.0 (c (n "openssl-probe") (v "0.1.0") (h "0689h6rhzy6dypqr90lsxnf108nsnh952wsx7ggs70s48b44jvbm")))

(define-public crate-openssl-probe-0.1.1 (c (n "openssl-probe") (v "0.1.1") (h "1bwd6xghzv043r3v72qnfm9v279dqmwxah8a0lycsk201lkz13fr")))

(define-public crate-openssl-probe-0.1.2 (c (n "openssl-probe") (v "0.1.2") (h "1pijrdifgsdwd45b08c2g0dsmnhz7c3kmagb70839ngrd7d29bvp")))

(define-public crate-openssl-probe-0.1.3 (c (n "openssl-probe") (v "0.1.3") (h "055s3s1iqn7c0dj29m3cll5l0q0nc28lx7hc24n9r2zbnk5y6g0m") (y #t)))

(define-public crate-openssl-probe-0.1.4 (c (n "openssl-probe") (v "0.1.4") (h "0nmrvlrza9ya23kgzmsgfc17f9pxajdxk25cwsk9aq5p5a3qv618")))

(define-public crate-openssl-probe-0.1.5 (c (n "openssl-probe") (v "0.1.5") (h "1kq18qm48rvkwgcggfkqq6pm948190czqc94d6bm2sir5hq1l0gz")))

