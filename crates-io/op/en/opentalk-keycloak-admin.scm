(define-module (crates-io op en opentalk-keycloak-admin) #:use-module (crates-io))

(define-public crate-opentalk-keycloak-admin-0.0.0 (c (n "opentalk-keycloak-admin") (v "0.0.0") (h "1ppq3qvmzwhjng8hmsrp9h1hfq9kd4gmm4kjhpk2i2189vyrzwd9")))

(define-public crate-opentalk-keycloak-admin-0.1.0 (c (n "opentalk-keycloak-admin") (v "0.1.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.12") (f (quote ("json" "rustls-tls-native-roots"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "snafu") (r "^0.8") (d #t) (k 0)) (d (n "tokio") (r "^1") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "1y3mmpl155p2cimcgd0m4q5b3abacwxzds3g5yx6qfl781s4yb9a")))

