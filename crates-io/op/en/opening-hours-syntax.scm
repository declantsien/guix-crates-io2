(define-module (crates-io op en opening-hours-syntax) #:use-module (crates-io))

(define-public crate-opening-hours-syntax-0.4.0 (c (n "opening-hours-syntax") (v "0.4.0") (d (list (d (n "chrono") (r "^0.4") (k 0)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)))) (h "1q29jskhq5np2nizdnvr1rrfb5paqbyrgi6c1j6q9x2kj31syxz2")))

(define-public crate-opening-hours-syntax-0.4.1 (c (n "opening-hours-syntax") (v "0.4.1") (d (list (d (n "chrono") (r "^0.4") (k 0)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)))) (h "0wgnwrw0zxicnwyq6kzwzps0x1hmznprj3s4qkb9rqi9vbkg5shp")))

(define-public crate-opening-hours-syntax-0.5.1 (c (n "opening-hours-syntax") (v "0.5.1") (d (list (d (n "chrono") (r "^0.4") (k 0)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)))) (h "0kxbr3m9zxaq42a5z9gjgchymsl6zw6zwr7hkys9hkd5csx2hj03")))

(define-public crate-opening-hours-syntax-0.5.2 (c (n "opening-hours-syntax") (v "0.5.2") (d (list (d (n "chrono") (r "^0.4") (k 0)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)))) (h "0bp14bi970ias998c8icrgm40qs8vkkc18jyacl2615vrnka0lwy")))

(define-public crate-opening-hours-syntax-0.5.3 (c (n "opening-hours-syntax") (v "0.5.3") (d (list (d (n "chrono") (r "^0.4") (k 0)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)))) (h "1ay99w24w3abzhadivyg9y7c5vipnc8giwqbs43sgsgacb8lgm4b")))

(define-public crate-opening-hours-syntax-0.5.4 (c (n "opening-hours-syntax") (v "0.5.4") (d (list (d (n "chrono") (r "^0.4") (k 0)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)))) (h "0g58322wpyb32yfl876zfi15q4y7cds05zz977x9lvhg02vv5jzm")))

(define-public crate-opening-hours-syntax-0.5.5 (c (n "opening-hours-syntax") (v "0.5.5") (d (list (d (n "chrono") (r "^0.4") (k 0)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)))) (h "1ja6w13d7jciivbasz807cm4g7zd6cwvw781pjn3yv4whxr8461p")))

(define-public crate-opening-hours-syntax-0.5.6 (c (n "opening-hours-syntax") (v "0.5.6") (d (list (d (n "chrono") (r "^0.4") (k 0)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)))) (h "0vhpdnznnhzw5m00p3qmwqb86nllgrisnxm1p00k8hdh072a79yj")))

(define-public crate-opening-hours-syntax-0.6.0 (c (n "opening-hours-syntax") (v "0.6.0") (d (list (d (n "chrono") (r "^0.4") (k 0)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)))) (h "0ghh970z0cr9wkxb8n2v3rsxcx8idvf48k0bz3ybsmkiqwx5kcz2")))

(define-public crate-opening-hours-syntax-0.6.1 (c (n "opening-hours-syntax") (v "0.6.1") (d (list (d (n "chrono") (r "^0.4") (k 0)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)))) (h "1lhfrb00wpgpc8pqqnqngaxb7rx66sbix3xbxcif0xvkz45a78k6")))

(define-public crate-opening-hours-syntax-0.6.2 (c (n "opening-hours-syntax") (v "0.6.2") (d (list (d (n "chrono") (r "^0.4") (k 0)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)))) (h "0b262dndjcxmsf6zv5il3a8vxn9ikm54vpk78b9b5m8flcvdlw5f")))

(define-public crate-opening-hours-syntax-0.6.3 (c (n "opening-hours-syntax") (v "0.6.3") (d (list (d (n "chrono") (r "^0.4") (k 0)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)))) (h "12vywh64gyrfsz68aldi2h8gnd2wcaq0adix8n6p1igb90ja8bcc")))

(define-public crate-opening-hours-syntax-0.6.4 (c (n "opening-hours-syntax") (v "0.6.4") (d (list (d (n "chrono") (r "^0.4") (k 0)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)))) (h "0lbd16zgk60ssad2lmwbwkmvg5rn6x0zcirvx8kzaq5c54lgvaam")))

(define-public crate-opening-hours-syntax-0.6.5 (c (n "opening-hours-syntax") (v "0.6.5") (d (list (d (n "chrono") (r "^0.4") (k 0)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)))) (h "14511hmq479cm97s6n4g2h1h8ph1qhflklh0jmync79xryh5a54j")))

(define-public crate-opening-hours-syntax-0.6.6 (c (n "opening-hours-syntax") (v "0.6.6") (d (list (d (n "chrono") (r "^0.4") (k 0)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)))) (h "0623qbv9r2v0w9dpgr993zcb8xg4j4f05a7h5vd5bm35sisf44zc")))

(define-public crate-opening-hours-syntax-0.6.7 (c (n "opening-hours-syntax") (v "0.6.7") (d (list (d (n "chrono") (r "^0.4") (k 0)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)))) (h "088qyq0qpc9gx9fnprvvffm13p47ps38iddciand4z1z1s6f8cga")))

(define-public crate-opening-hours-syntax-0.6.8 (c (n "opening-hours-syntax") (v "0.6.8") (d (list (d (n "chrono") (r "^0.4") (k 0)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)))) (h "1nwp8ic3m2pb23wl9n1yi51mw2n59mjhjz6m9lwd9ja8ai7sghxx")))

(define-public crate-opening-hours-syntax-0.6.9 (c (n "opening-hours-syntax") (v "0.6.9") (d (list (d (n "chrono") (r "^0.4") (k 0)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)))) (h "0nzi8xld2h7czjalglirksr0ahsh129ma9qvg4y3h4llnmda9fy7")))

(define-public crate-opening-hours-syntax-0.6.10 (c (n "opening-hours-syntax") (v "0.6.10") (d (list (d (n "chrono") (r "^0.4") (k 0)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)))) (h "0n02m3vljddfjnig1b2hy0f6zx7wxc062bh73xmk4wkdhrijqa00")))

(define-public crate-opening-hours-syntax-0.6.11 (c (n "opening-hours-syntax") (v "0.6.11") (d (list (d (n "chrono") (r "^0.4") (k 0)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)))) (h "1hzxj5f445rzp016by5h4l3i72q9smmwnszxn3gpi3vr662l6l55")))

(define-public crate-opening-hours-syntax-0.6.12 (c (n "opening-hours-syntax") (v "0.6.12") (d (list (d (n "chrono") (r "^0.4") (k 0)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)))) (h "1w9g8wjngfjbqn0bxwv8ijp8c5jd6fsvc0m8xqfyxnq10q8sg2hz")))

(define-public crate-opening-hours-syntax-0.6.13 (c (n "opening-hours-syntax") (v "0.6.13") (d (list (d (n "chrono") (r "^0.4") (k 0)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)))) (h "1gydnkamzc9lpcwnw6fwicnndribdadada2qm30b6nc9gjv5dk6g")))

(define-public crate-opening-hours-syntax-0.6.14 (c (n "opening-hours-syntax") (v "0.6.14") (d (list (d (n "chrono") (r "^0.4") (k 0)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)))) (h "0wmi2hgga9x9xsr4j5k7lzi7mfhgw3pzawqwinbgbs01iz7z8asd")))

(define-public crate-opening-hours-syntax-0.6.15 (c (n "opening-hours-syntax") (v "0.6.15") (d (list (d (n "chrono") (r "^0.4") (k 0)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)))) (h "0nhj0kzddiyk7vpinn3imk08kpkbn26333w4i9hvzj6qkz2cwym4")))

(define-public crate-opening-hours-syntax-0.6.18 (c (n "opening-hours-syntax") (v "0.6.18") (d (list (d (n "chrono") (r "^0.4") (k 0)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)))) (h "07gw9fc57vsnwm35rqis05xdvzfjhdj62ma47l5dpv4hn37has92")))

