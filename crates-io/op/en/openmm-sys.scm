(define-module (crates-io op en openmm-sys) #:use-module (crates-io))

(define-public crate-openmm-sys-7.4.0 (c (n "openmm-sys") (v "7.4.0") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0kqbga47k73f3x9h5c1gaak7bwj0dkd32q72qr614akz6g7pd3mq") (f (quote (("static-lib") ("shared-lib") ("rpmd") ("python-wrappers") ("pme") ("opencl") ("no-default") ("minimal") ("generate-api-docs") ("examples") ("drude") ("cuda") ("cpu") ("c-and-fortran-wrappers") ("amoeba")))) (y #t) (l "OpenMM")))

(define-public crate-openmm-sys-7.4.1 (c (n "openmm-sys") (v "7.4.1") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0bqmp0y6h746ibcgp8l6g1yqqq8n9ly241b0civsil1zvnvx0lsk") (f (quote (("static-lib") ("shared-lib") ("rpmd") ("pme") ("opencl") ("no-default") ("minimal") ("generate-api-docs") ("examples") ("drude") ("cuda") ("cpu") ("c-and-fortran-wrappers") ("amoeba")))) (l "OpenMM")))

