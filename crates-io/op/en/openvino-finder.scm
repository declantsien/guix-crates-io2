(define-module (crates-io op en openvino-finder) #:use-module (crates-io))

(define-public crate-openvino-finder-0.3.0 (c (n "openvino-finder") (v "0.3.0") (h "1x242yv4bz77skgpy01h9l21y6jzrcqd2y5f4ri9kgsxbmrb3vlg")))

(define-public crate-openvino-finder-0.3.1 (c (n "openvino-finder") (v "0.3.1") (h "0p2j3iymh00lqjwnylmnszahzzflrfbfl3qv27hv27l466hqfra2")))

(define-public crate-openvino-finder-0.3.2 (c (n "openvino-finder") (v "0.3.2") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 2)))) (h "1qh9g8is05rqvfrkscp1dw4dbp4w4918qvmbxbr49lhqllf4pyj6")))

(define-public crate-openvino-finder-0.3.3 (c (n "openvino-finder") (v "0.3.3") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 2)))) (h "1imm8iblfk5h51igi7r9i8kc9vhm7xic30s5715jz8wk7wp0v9c3")))

(define-public crate-openvino-finder-0.4.0 (c (n "openvino-finder") (v "0.4.0") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 2)))) (h "147hw61n5a3zja3jx82kmkqnb3vlhv2pr48y8ygjlwg5536jg14k")))

(define-public crate-openvino-finder-0.4.1 (c (n "openvino-finder") (v "0.4.1") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 2)))) (h "0y15kivwm3x6rmwhf0p70sfj0k3wbyaqljcxyx5dpwywhkj96f11")))

(define-public crate-openvino-finder-0.4.2 (c (n "openvino-finder") (v "0.4.2") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 2)))) (h "1kvc8w7k66zpsph7gdh8lgvd7gphiy2i7m8aizhznkd5kvrysl66")))

(define-public crate-openvino-finder-0.5.0 (c (n "openvino-finder") (v "0.5.0") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 2)))) (h "01sdxfxnkbq5l0w9nxav200nqndfyc7r11ff7pnbkhh6x87difyq")))

(define-public crate-openvino-finder-0.6.0 (c (n "openvino-finder") (v "0.6.0") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0wqr6d56ah48d0gl82riwrmr9lcsdf04035gmnl3whaa778k9lh5")))

(define-public crate-openvino-finder-0.7.0 (c (n "openvino-finder") (v "0.7.0") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0giq9vsl96zyrfdplw0bnci6v10k4k6s3xh9y4papwknj6ky8k38")))

(define-public crate-openvino-finder-0.7.1 (c (n "openvino-finder") (v "0.7.1") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0lnax4jlz6dzhwvc6gbcyzadx3093gwbb3bgkx878lricvwdb2qs")))

