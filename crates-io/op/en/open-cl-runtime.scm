(define-module (crates-io op en open-cl-runtime) #:use-module (crates-io))

(define-public crate-open-cl-runtime-0.1.0 (c (n "open-cl-runtime") (v "0.1.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.3.1") (d #t) (k 0)) (d (n "open-cl-low-level") (r "^0.1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2.11") (f (quote ("full"))) (d #t) (k 0)))) (h "02m0yfb3dgj2vwq172vvvbnwm26swvn2frz00fbm9fndig60vzq1")))

