(define-module (crates-io op en openpgp-card-pcsc) #:use-module (crates-io))

(define-public crate-openpgp-card-pcsc-0.0.1 (c (n "openpgp-card-pcsc") (v "0.0.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "openpgp-card") (r "^0.0.2") (d #t) (k 0)) (d (n "pcsc") (r "^2") (d #t) (k 0)))) (h "1myklzydkcwdyhqhb05ady22i6dm8af5la9gnpar92mya74629yx")))

(define-public crate-openpgp-card-pcsc-0.0.2 (c (n "openpgp-card-pcsc") (v "0.0.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "openpgp-card") (r "^0.0.3") (d #t) (k 0)) (d (n "pcsc") (r "^2") (d #t) (k 0)))) (h "006ivix5icy7q8vsma5r7vwc9a1950bgci122klfkng5gpxkp08v")))

(define-public crate-openpgp-card-pcsc-0.0.3 (c (n "openpgp-card-pcsc") (v "0.0.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "openpgp-card") (r "^0.0.4") (d #t) (k 0)) (d (n "pcsc") (r "^2") (d #t) (k 0)))) (h "1mm0gz8kgb4jxbq208qdj12zjwi153bmr1x8nyyjbiaxc5fl2qs9")))

(define-public crate-openpgp-card-pcsc-0.0.4 (c (n "openpgp-card-pcsc") (v "0.0.4") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "openpgp-card") (r "^0.0.4") (d #t) (k 0)) (d (n "pcsc") (r "^2") (d #t) (k 0)))) (h "1kf6mrgfkc90nzy5kny5zfpfq4dbhk8gpyajcxb7hic2pzw22crr")))

(define-public crate-openpgp-card-pcsc-0.0.5 (c (n "openpgp-card-pcsc") (v "0.0.5") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "openpgp-card") (r "^0.0.5") (d #t) (k 0)) (d (n "pcsc") (r "^2") (d #t) (k 0)))) (h "0qxs1bhn97s6nqhdrr4r2rfmiv78prmr6jlckdkcacqadzfniqa1")))

(define-public crate-openpgp-card-pcsc-0.1.0 (c (n "openpgp-card-pcsc") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "iso7816-tlv") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "openpgp-card") (r "^0.1") (d #t) (k 0)) (d (n "pcsc") (r "^2") (d #t) (k 0)))) (h "1z3yr0i01jfkdv25391036drdm6gnhkc1krrn7202s54kmli0spv")))

(define-public crate-openpgp-card-pcsc-0.2.0 (c (n "openpgp-card-pcsc") (v "0.2.0") (d (list (d (n "iso7816-tlv") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "openpgp-card") (r "^0.2") (d #t) (k 0)) (d (n "pcsc") (r "^2.7") (d #t) (k 0)))) (h "1cxpbjwwb3xw7rd64ph9x0k657rcqz64x7pawa3d2914nnch3ark")))

(define-public crate-openpgp-card-pcsc-0.2.1 (c (n "openpgp-card-pcsc") (v "0.2.1") (d (list (d (n "iso7816-tlv") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "openpgp-card") (r "^0.2") (d #t) (k 0)) (d (n "pcsc") (r "^2.7") (d #t) (k 0)))) (h "06kjdniiyavyz9697ji33cw6f87yz5ikj13224s5hpv7h6i2rvcc")))

(define-public crate-openpgp-card-pcsc-0.2.2 (c (n "openpgp-card-pcsc") (v "0.2.2") (d (list (d (n "iso7816-tlv") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "openpgp-card") (r "^0.3") (d #t) (k 0)) (d (n "pcsc") (r "^2.7") (d #t) (k 0)))) (h "07hdlam0m2s1q30lz0i35amanh4q09sn9d8h1pa8z8rb8412a8pk") (y #t)))

(define-public crate-openpgp-card-pcsc-0.3.0 (c (n "openpgp-card-pcsc") (v "0.3.0") (d (list (d (n "iso7816-tlv") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "openpgp-card") (r "^0.3") (d #t) (k 0)) (d (n "pcsc") (r "^2.7") (d #t) (k 0)))) (h "0h4p00gyv0hvb95lzfsq4bx2fzr21zwfgn56yzvnibqmnbslk4v2")))

(define-public crate-openpgp-card-pcsc-0.3.1 (c (n "openpgp-card-pcsc") (v "0.3.1") (d (list (d (n "iso7816-tlv") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "openpgp-card") (r "^0.3.5") (d #t) (k 0)) (d (n "pcsc") (r "^2.7") (d #t) (k 0)))) (h "0svava2r2djn7v7lkdkkdmkzzq4fbrr8n5qh6gl3b18b1kzcsv7v")))

