(define-module (crates-io op en openvx-sys) #:use-module (crates-io))

(define-public crate-openvx-sys-0.1.0 (c (n "openvx-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "clang") (r "^2.0.0") (f (quote ("runtime"))) (d #t) (k 1)))) (h "154l3khndw7r9bl65x5dm4ilw4y0rxviphqq5w7ilim52flsvny2") (l "openvx")))

(define-public crate-openvx-sys-0.1.2 (c (n "openvx-sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)) (d (n "clang") (r "^2.0.0") (f (quote ("runtime"))) (d #t) (k 1)))) (h "0dkamjbniic3a7058ih43ck2a3hg394ikchm5rgm67v8dhnsz1qg") (y #t) (l "openvx")))

(define-public crate-openvx-sys-0.1.3 (c (n "openvx-sys") (v "0.1.3") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)) (d (n "clang") (r "^2.0.0") (f (quote ("runtime"))) (d #t) (k 1)))) (h "1bka6yg85hrwc4zj1afpirjiax1hamc60gv3f9r5cg6hh6qqpr97") (l "openvx")))

