(define-module (crates-io op en openh264-sys2) #:use-module (crates-io))

(define-public crate-openh264-sys2-0.1.8 (c (n "openh264-sys2") (v "0.1.8") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "walkdir") (r "^2.3") (d #t) (k 1)))) (h "1yk72nqk77vfwz01zragfbxki3m8f6ml5vh7pngfl6dj4x9kawck")))

(define-public crate-openh264-sys2-0.1.9 (c (n "openh264-sys2") (v "0.1.9") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "walkdir") (r "^2.3") (d #t) (k 1)))) (h "1sfc548ba7r0r19cp219shkn6j239dli9n65crsxm46fgn2s7sha")))

(define-public crate-openh264-sys2-0.1.11 (c (n "openh264-sys2") (v "0.1.11") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "walkdir") (r "^2.3") (d #t) (k 1)))) (h "1skl2s1zcxg2476izd9k2z4dlb3cdn9v951rqzf2nyd1w96hr4bv")))

(define-public crate-openh264-sys2-0.1.12 (c (n "openh264-sys2") (v "0.1.12") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "walkdir") (r "^2.3") (d #t) (k 1)))) (h "19aq7iim9bwld184r5iqhzbxy981rwv88l2dgf6fy7cfljp5kgs8")))

(define-public crate-openh264-sys2-0.1.13 (c (n "openh264-sys2") (v "0.1.13") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "walkdir") (r "^2.3") (d #t) (k 1)))) (h "1wvc0fymz5nb11anjixn41ihk5904vmq6xdi1gmmddkbgfxr3vy9")))

(define-public crate-openh264-sys2-0.1.15 (c (n "openh264-sys2") (v "0.1.15") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "walkdir") (r "^2.3") (d #t) (k 1)))) (h "0mwfvpbidwlx3nkp6wgnvv3wfr77gjpm4qnzb6r3yw4q2vl104q8")))

(define-public crate-openh264-sys2-0.1.16 (c (n "openh264-sys2") (v "0.1.16") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "walkdir") (r "^2.3") (d #t) (k 1)))) (h "0vfgc89j52j8nxx3vnpq68sxw0jgx73qdh69hkdpvbr4sjyhnhqq")))

(define-public crate-openh264-sys2-0.1.17 (c (n "openh264-sys2") (v "0.1.17") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "walkdir") (r "^2.3") (d #t) (k 1)))) (h "1xri9b7gabj3qdsk6f38kssngf2g0pxk0rcsrdg1nl77fpccgxdk") (f (quote (("encoder") ("default" "decoder" "encoder") ("decoder"))))))

(define-public crate-openh264-sys2-0.2.5 (c (n "openh264-sys2") (v "0.2.5") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "nasm-rs") (r "^0.2") (f (quote ("parallel"))) (o #t) (d #t) (k 1)) (d (n "walkdir") (r "^2.3") (d #t) (k 1)))) (h "0dls5kh10kxxqm0zknq6fj16lsp07djw96sc7fihk5k8xm0d30a5") (f (quote (("encoder") ("default" "decoder" "encoder") ("decoder") ("asm" "nasm-rs"))))))

(define-public crate-openh264-sys2-0.2.9 (c (n "openh264-sys2") (v "0.2.9") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "nasm-rs") (r "^0.2") (f (quote ("parallel"))) (o #t) (d #t) (k 1)) (d (n "walkdir") (r "^2.3") (d #t) (k 1)))) (h "1xiym6f5czz8146cr4y9li9kv4vxyxwficcdi26lz4xynvmi5ffv") (f (quote (("encoder") ("default" "decoder" "encoder") ("decoder") ("asm" "nasm-rs"))))))

(define-public crate-openh264-sys2-0.2.11 (c (n "openh264-sys2") (v "0.2.11") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "nasm-rs") (r "^0.2") (f (quote ("parallel"))) (o #t) (d #t) (k 1)) (d (n "walkdir") (r "^2.3") (d #t) (k 1)))) (h "0xfhwpn3swrgnlcc39p40p2b3rvmh6i4py5h826vkx34g0cnx3ig") (f (quote (("encoder") ("default" "decoder" "encoder") ("decoder") ("asm" "nasm-rs"))))))

(define-public crate-openh264-sys2-0.2.12 (c (n "openh264-sys2") (v "0.2.12") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "nasm-rs") (r "^0.2") (f (quote ("parallel"))) (o #t) (d #t) (k 1)) (d (n "walkdir") (r "^2.3") (d #t) (k 1)))) (h "11chlv7l3rniwdnmac0rnabpivp9hp6dla64zrkdza65ah2a3k71") (f (quote (("encoder") ("default" "decoder" "encoder") ("decoder") ("asm" "nasm-rs"))))))

(define-public crate-openh264-sys2-0.2.13 (c (n "openh264-sys2") (v "0.2.13") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "nasm-rs") (r "^0.2") (f (quote ("parallel"))) (o #t) (d #t) (k 1)) (d (n "walkdir") (r "^2.3") (d #t) (k 1)))) (h "17cwzrvg0kiavlkrnlxs666jnp9ixlicngshx4mqk0h6bqhb9cs0") (f (quote (("encoder") ("default" "decoder" "encoder") ("decoder") ("asm" "nasm-rs"))))))

(define-public crate-openh264-sys2-0.2.14 (c (n "openh264-sys2") (v "0.2.14") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "nasm-rs") (r "^0.2") (f (quote ("parallel"))) (o #t) (d #t) (k 1)) (d (n "walkdir") (r "^2.3") (d #t) (k 1)))) (h "06ybxph80albsdm7dma7gczxwgbjlg5gxqsh18pdmb7vjdh3b4x0") (f (quote (("encoder") ("default" "decoder" "encoder") ("decoder") ("asm" "nasm-rs"))))))

(define-public crate-openh264-sys2-0.2.16 (c (n "openh264-sys2") (v "0.2.16") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "nasm-rs") (r "^0.2") (f (quote ("parallel"))) (o #t) (d #t) (k 1)) (d (n "walkdir") (r "^2.3") (d #t) (k 1)))) (h "03h0xmd9b4ygasmwmw07pcxpvy8zxpxm1iylrxjcnn6c820kvszp") (f (quote (("encoder") ("default" "decoder" "encoder") ("decoder") ("asm" "nasm-rs"))))))

(define-public crate-openh264-sys2-0.3.0 (c (n "openh264-sys2") (v "0.3.0") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "nasm-rs") (r "^0.2") (d #t) (k 1)) (d (n "walkdir") (r "^2.3") (d #t) (k 1)))) (h "1jnxipanf7w1va8adr5z7f73nc6r0sckb57lc7df44k8qixqr8wh") (f (quote (("encoder") ("default" "decoder" "encoder") ("decoder"))))))

(define-public crate-openh264-sys2-0.3.1 (c (n "openh264-sys2") (v "0.3.1") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "nasm-rs") (r "^0.2") (d #t) (k 1)) (d (n "walkdir") (r "^2.3") (d #t) (k 1)))) (h "16gxdwsbp1xvnklym33na3qs0smz4jgj9kwd3fmlrpqhr3sdpd00") (f (quote (("encoder") ("default" "decoder" "encoder") ("decoder"))))))

(define-public crate-openh264-sys2-0.3.2 (c (n "openh264-sys2") (v "0.3.2") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "nasm-rs") (r "^0.2") (d #t) (k 1)) (d (n "walkdir") (r "^2.3") (d #t) (k 1)))) (h "0jpca3fc5hwr39i540cjw9inwpznd5gjf75lsl0b84cax0bdn7qp") (f (quote (("encoder") ("default" "decoder" "encoder") ("decoder"))))))

(define-public crate-openh264-sys2-0.4.0 (c (n "openh264-sys2") (v "0.4.0") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "nasm-rs") (r "^0.2") (d #t) (k 1)) (d (n "walkdir") (r "^2.3") (d #t) (k 1)))) (h "179cxd0mvz3zxcbabfw83z4vsgq3wxhgadh0zdmyq1gvk6iqnpkq") (f (quote (("encoder") ("default" "decoder" "encoder") ("decoder"))))))

(define-public crate-openh264-sys2-0.4.1 (c (n "openh264-sys2") (v "0.4.1") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "nasm-rs") (r "^0.2") (d #t) (k 1)) (d (n "walkdir") (r "^2.3") (d #t) (k 1)))) (h "08m3zmh1lflvqx8gig0xfrldyphnwx4c70lj77y9jw4impg9jv62") (f (quote (("encoder") ("default" "decoder" "encoder") ("decoder"))))))

(define-public crate-openh264-sys2-0.4.4 (c (n "openh264-sys2") (v "0.4.4") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "nasm-rs") (r "^0.2") (d #t) (k 1)) (d (n "walkdir") (r "^2.3") (d #t) (k 1)))) (h "0ywcdasa6annq3460h17xsh6axzly8kvqq1n3pm8s9jfzndw89kn") (f (quote (("encoder") ("default" "decoder" "encoder") ("decoder"))))))

(define-public crate-openh264-sys2-0.5.0 (c (n "openh264-sys2") (v "0.5.0") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libloading") (r "^0.8.1") (o #t) (d #t) (k 0)) (d (n "nasm-rs") (r "^0.2") (d #t) (k 1)) (d (n "sha2") (r "^0.10.8") (o #t) (d #t) (k 0)) (d (n "walkdir") (r "^2.3") (d #t) (k 1)))) (h "14349aabx65l43pnszlgzbzzshdbss68g7r3vd1q5mn9hkj76042") (f (quote (("source") ("default" "source")))) (s 2) (e (quote (("libloading" "dep:libloading" "dep:sha2"))))))

(define-public crate-openh264-sys2-0.6.0 (c (n "openh264-sys2") (v "0.6.0") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libloading") (r "^0.8.1") (o #t) (d #t) (k 0)) (d (n "nasm-rs") (r "^0.2") (d #t) (k 1)) (d (n "sha2") (r "^0.10.8") (o #t) (d #t) (k 0)) (d (n "walkdir") (r "^2.3") (d #t) (k 1)))) (h "1q61rccn39zal5darmjwvw9jbqqs6dqmwrcfpcpjydvmkvk6r090") (f (quote (("source") ("default" "source")))) (s 2) (e (quote (("libloading" "dep:libloading" "dep:sha2"))))))

