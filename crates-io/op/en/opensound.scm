(define-module (crates-io op en opensound) #:use-module (crates-io))

(define-public crate-opensound-0.0.1 (c (n "opensound") (v "0.0.1") (h "0iaipr5f6q18i15gjxhsifvp3sygxgxpnj41m2sq82x1k8h507ym")))

(define-public crate-opensound-0.0.2 (c (n "opensound") (v "0.0.2") (d (list (d (n "chrono") (r "^0.4.35") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.18") (f (quote ("chrono"))) (d #t) (k 0)))) (h "1xrafvlk3pdlpp4ks54v847sd96xm97y3hpjkqiaz0kj8gsan4rq")))

(define-public crate-opensound-0.0.3 (c (n "opensound") (v "0.0.3") (d (list (d (n "chrono") (r "^0.4.35") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.18") (f (quote ("chrono"))) (d #t) (k 0)))) (h "0nnvbyldinb5sddiskkq9dy1q183yq67ag46653w4g4r6nvjx50c")))

(define-public crate-opensound-0.0.4 (c (n "opensound") (v "0.0.4") (d (list (d (n "chrono") (r "^0.4.35") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.18") (f (quote ("chrono"))) (d #t) (k 0)))) (h "12nbwag9kkqpcamwki92klskp6h5n8k74bnm9rwb6fn69ndhfx8p")))

(define-public crate-opensound-0.0.5 (c (n "opensound") (v "0.0.5") (d (list (d (n "chrono") (r "^0.4.35") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.18") (f (quote ("chrono"))) (d #t) (k 0)))) (h "1l7v960jj8d86mari6nwfx6ny5sgqx3ms86s8f26zbddmx2i4hh4")))

(define-public crate-opensound-0.0.6 (c (n "opensound") (v "0.0.6") (d (list (d (n "chrono") (r "^0.4.35") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.18") (f (quote ("chrono"))) (d #t) (k 0)))) (h "1z5px73vavg88sx9sdrr4fdifm0fywmgjgill5ryscvz8mphs26y")))

