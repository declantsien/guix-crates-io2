(define-module (crates-io op en opensc-sys) #:use-module (crates-io))

(define-public crate-opensc-sys-0.1.0 (c (n "opensc-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.53.2") (d #t) (k 1)))) (h "0156alvdqvkv6j8gc89qvrndbvcya6w2x7k0978whwycsnfv6186")))

(define-public crate-opensc-sys-0.1.1 (c (n "opensc-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.53.2") (d #t) (k 1)))) (h "037jin8fcyppsi69fy760a33wsacx6bbzw4wx1mbkf333wvnx619") (f (quote (("enable_sm" "enable_openssl") ("enable_openssl") ("default" "enable_openssl" "enable_sm"))))))

