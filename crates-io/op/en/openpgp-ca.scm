(define-module (crates-io op en openpgp-ca) #:use-module (crates-io))

(define-public crate-openpgp-ca-0.10.1 (c (n "openpgp-ca") (v "0.10.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "openpgp-ca-lib") (r "^0.10") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1zshyva63q7kkp6s0gkl7g8z5d84z7gjkg1gf0azphqx6hb1af8g")))

(define-public crate-openpgp-ca-0.11.1 (c (n "openpgp-ca") (v "0.11.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "openpgp-ca-lib") (r "^0.11") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0zjxhvp66kfjk1s5mcp0810a201qb7xrayshxdfarvzk22ygpnz7")))

(define-public crate-openpgp-ca-0.11.2 (c (n "openpgp-ca") (v "0.11.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "openpgp-ca-lib") (r "^0.11.3") (d #t) (k 0)))) (h "1wby5z5s2gcpfrmsxpwaib179qk6yypfz19nfg9m2g31wq6l9vr4")))

(define-public crate-openpgp-ca-0.12.0-alpha.1 (c (n "openpgp-ca") (v "0.12.0-alpha.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "openpgp-ca-lib") (r "^0.12.0-alpha.1") (d #t) (k 0)) (d (n "rpassword") (r "^7") (d #t) (k 0)))) (h "0imw1spmzz9xzjinxy4r0kw62jw04mpwac6hfazs1ld4piqqwy3q")))

(define-public crate-openpgp-ca-0.12.0 (c (n "openpgp-ca") (v "0.12.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "openpgp-ca-lib") (r "^0.12") (d #t) (k 0)) (d (n "rpassword") (r "^7") (d #t) (k 0)))) (h "14z6nvlk3pm5k1lq95r5m1x6mlzawzzp30hlfr3vgyyrkb2585mp")))

(define-public crate-openpgp-ca-0.13.0-alpha.1 (c (n "openpgp-ca") (v "0.13.0-alpha.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "openpgp-ca-lib") (r "^0.13.0-alpha.1") (d #t) (k 0)) (d (n "rpassword") (r "^7") (d #t) (k 0)))) (h "1x29vf4fplczi06n64w12yfli741ja13lvh1anq6aj242m5bgjz2")))

(define-public crate-openpgp-ca-0.13.0 (c (n "openpgp-ca") (v "0.13.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "openpgp-ca-lib") (r "^0.13") (d #t) (k 0)) (d (n "rpassword") (r "^7") (d #t) (k 0)))) (h "16pwwg20gfwqwfgqv1nlfp53f67vw1gd02an8hds51l8kbsy0g0d")))

(define-public crate-openpgp-ca-0.13.1 (c (n "openpgp-ca") (v "0.13.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "openpgp-ca-lib") (r "^0.13") (d #t) (k 0)) (d (n "rpassword") (r "^7") (d #t) (k 0)))) (h "05h2znbmz5y34s9d10by2n1g48kwcr7jv6c8hxfxxrszr1ga3j5w")))

