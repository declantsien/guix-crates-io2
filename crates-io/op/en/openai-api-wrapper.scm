(define-module (crates-io op en openai-api-wrapper) #:use-module (crates-io))

(define-public crate-openai-api-wrapper-0.0.1 (c (n "openai-api-wrapper") (v "0.0.1") (d (list (d (n "derive_builder") (r "^0.12.0") (d #t) (k 0)) (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.16") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 2)) (d (n "mockito") (r "^1.0.2") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("macros"))) (d #t) (k 2)))) (h "0qffcar6c75ybawk8pklk6pcab524n3bm9pp0jlqnsznc2hq3lnl")))

