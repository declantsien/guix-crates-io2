(define-module (crates-io op en openssl-macros) #:use-module (crates-io))

(define-public crate-openssl-macros-0.1.0 (c (n "openssl-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0v3kgnzbadrf9c06q4cqmbjas53av73n5w7wwz3n0nb6257y80dm")))

(define-public crate-openssl-macros-0.1.1 (c (n "openssl-macros") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "173xxvfc63rr5ybwqwylsir0vq6xsj4kxiv4hmg4c3vscdmncj59")))

