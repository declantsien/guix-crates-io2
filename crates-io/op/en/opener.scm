(define-module (crates-io op en opener) #:use-module (crates-io))

(define-public crate-opener-0.1.0 (c (n "opener") (v "0.1.0") (d (list (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.2") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("shellapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0008jpzsc1xxmj1vvi0a46v46qsibimwi9hjpbx15dl9w47cds65")))

(define-public crate-opener-0.2.0 (c (n "opener") (v "0.2.0") (d (list (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.2") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("shellapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "08c5bp32273yjgcwrrjvlsl8yc8acd4pzns332krqzcsm58ibhqc")))

(define-public crate-opener-0.3.0 (c (n "opener") (v "0.3.0") (d (list (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.2") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("shellapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "152cnadp87y07x55jgcvy9mjrh32ar9m556iqsjrzvzmvzmdhv0p")))

(define-public crate-opener-0.3.1 (c (n "opener") (v "0.3.1") (d (list (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.2") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("shellapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0wm8zx8qv3zhnlrb6p9idnrg4h149irk8ihkacnk2f86zl7p6zw7")))

(define-public crate-opener-0.3.2 (c (n "opener") (v "0.3.2") (d (list (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.2") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("shellapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1jcf5h0nyvx0lghvh17l2k7ji55d20dzidpra2ah1cyrhsqddc84")))

(define-public crate-opener-0.4.0 (c (n "opener") (v "0.4.0") (d (list (d (n "winapi") (r "^0.3") (f (quote ("shellapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0j3l3b9wh7fk76wqpgw6s8fhdfq4g8xh4vm94w8w0x4l7pl5k34r")))

(define-public crate-opener-0.4.1 (c (n "opener") (v "0.4.1") (d (list (d (n "winapi") (r "^0.3") (f (quote ("shellapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1bpknqvhqkalhmq8n2m97apc0r3y194ppybl1qxay34xr83p848k")))

(define-public crate-opener-0.5.0 (c (n "opener") (v "0.5.0") (d (list (d (n "bstr") (r "^0.2") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "winapi") (r "^0.3") (f (quote ("shellapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0lkrn4fv1h4m8gmp7ll6x7vjvb6kls2ngwa5cgsh2ix5fb6yp8sf")))

(define-public crate-opener-0.5.1 (c (n "opener") (v "0.5.1") (d (list (d (n "bstr") (r "^1") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "winapi") (r "^0.3") (f (quote ("shellapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "02hjjaqkhfc3pv6zxckr2zb8g49vrmbyz3b3l8a1gk71b8dyqnpb")))

(define-public crate-opener-0.5.2 (c (n "opener") (v "0.5.2") (d (list (d (n "bstr") (r "^1") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "winapi") (r "^0.3") (f (quote ("shellapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "01ghahdn64lw4whj0p70vmzivrdlmca2629gplalq99pirkiag19")))

(define-public crate-opener-0.6.0 (c (n "opener") (v "0.6.0") (d (list (d (n "bstr") (r "^1") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "dbus") (r "^0.9") (f (quote ("vendored"))) (o #t) (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "normpath") (r "^1") (d #t) (t "cfg(windows)") (k 0)) (d (n "url") (r "^2") (o #t) (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "winapi") (r "^0.3") (f (quote ("shellapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "16gs07z39az5cqg8n0x2305wc2panwf6zpwpsh2rdazlnw1vszym") (s 2) (e (quote (("reveal" "dep:url" "dep:dbus" "winapi/shtypes" "winapi/objbase"))))))

(define-public crate-opener-0.6.1 (c (n "opener") (v "0.6.1") (d (list (d (n "bstr") (r "^1") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "dbus") (r "^0.9") (f (quote ("vendored"))) (o #t) (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "normpath") (r "^1") (d #t) (t "cfg(windows)") (k 0)) (d (n "url") (r "^2") (o #t) (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "winapi") (r "^0.3") (f (quote ("shellapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1217aqxl8srr7v0bcc174yf5apaxjmzg0j62x8kb772g2yvdqqkc") (s 2) (e (quote (("reveal" "dep:url" "dep:dbus" "winapi/shtypes" "winapi/objbase"))))))

(define-public crate-opener-0.7.0 (c (n "opener") (v "0.7.0") (d (list (d (n "bstr") (r "^1") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "dbus") (r "^0.9") (o #t) (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "normpath") (r "^1") (d #t) (t "cfg(windows)") (k 0)) (d (n "url") (r "^2") (o #t) (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "windows-sys") (r "^0.52") (f (quote ("Win32_Foundation" "Win32_UI_Shell" "Win32_UI_WindowsAndMessaging"))) (d #t) (t "cfg(windows)") (k 0)))) (h "00dgss95r1yizng4pmq5pwq93mpydzi9wcnvasr27jbzkns1r47r") (f (quote (("default" "dbus-vendored") ("dbus-vendored" "dbus/vendored")))) (s 2) (e (quote (("reveal" "dep:url" "dep:dbus" "windows-sys/Win32_System_Com"))))))

(define-public crate-opener-0.7.1 (c (n "opener") (v "0.7.1") (d (list (d (n "bstr") (r "^1") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "dbus") (r "^0.9") (o #t) (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "normpath") (r "^1") (d #t) (t "cfg(windows)") (k 0)) (d (n "url") (r "^2") (o #t) (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "windows-sys") (r "^0.52") (f (quote ("Win32_Foundation" "Win32_UI_Shell" "Win32_UI_WindowsAndMessaging"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1h4h79ggp6jgd3p5ivnzhb75c73jj8xivx5gzzlzn41jcnz39pzq") (f (quote (("default" "dbus-vendored")))) (s 2) (e (quote (("reveal" "dep:url" "dep:dbus" "windows-sys/Win32_System_Com") ("dbus-vendored" "dbus?/vendored"))))))

