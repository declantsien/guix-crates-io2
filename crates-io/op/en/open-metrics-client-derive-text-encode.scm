(define-module (crates-io op en open-metrics-client-derive-text-encode) #:use-module (crates-io))

(define-public crate-open-metrics-client-derive-text-encode-0.1.0 (c (n "open-metrics-client-derive-text-encode") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1rs714wc9b3ch3zskz68fk6n957lxpdc9mm2sj3filhyyz07xjrv")))

(define-public crate-open-metrics-client-derive-text-encode-0.1.1 (c (n "open-metrics-client-derive-text-encode") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1z4grpaycs1s4wx87nd1kgfmjvqsxi037jy13736h0phhssq6p51")))

