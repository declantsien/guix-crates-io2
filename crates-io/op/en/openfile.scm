(define-module (crates-io op en openfile) #:use-module (crates-io))

(define-public crate-openfile-0.1.0 (c (n "openfile") (v "0.1.0") (h "0aq8f0881hp5q688qz3q5a6j9jxvzb17n9n119pm890jjh96z0v9")))

(define-public crate-openfile-0.1.1 (c (n "openfile") (v "0.1.1") (h "108dnpa2aaaif9nd4zcm8n8cfk85w20gp6cqh3ff3ldqfw724s25")))

(define-public crate-openfile-0.1.2 (c (n "openfile") (v "0.1.2") (h "1553b1llgf5508xndmsblsrnd2wifzd653asjg6vgrw83a57k5jj")))

(define-public crate-openfile-0.2.0 (c (n "openfile") (v "0.2.0") (h "178lmdv5asmr059cdqr5lhwlnyq8ds0g1wlfa3symn2msi12hx3p")))

(define-public crate-openfile-0.2.1 (c (n "openfile") (v "0.2.1") (h "15257dwfjx1v098cp8mxzw9b2y3l9sqpb40s3b04z3wrryqkhg2f")))

(define-public crate-openfile-1.0.0 (c (n "openfile") (v "1.0.0") (h "03jspl8i68rx32gyhm411g7xvczn9bdrfcrcbjlrd994jwywhc5r")))

(define-public crate-openfile-1.0.1 (c (n "openfile") (v "1.0.1") (h "1ramlsxw2sxq1c6ksvnmhjhglgr6h68crpl4kw2vmifc8k8dk9sa")))

(define-public crate-openfile-1.1.0 (c (n "openfile") (v "1.1.0") (h "04r3cqp4871saw1v48fniqka079qqj37y8zg4zi8n6m76xqxbpmr")))

(define-public crate-openfile-2.0.0 (c (n "openfile") (v "2.0.0") (h "19kpvgll0y9ddzv2vgw6wrdbvr3wrd4bvbvlccyh814d1k5l7xvx")))

(define-public crate-openfile-2.1.0 (c (n "openfile") (v "2.1.0") (h "1d30frb5536wxzg6jlcyr70didcyygb718m68c7k2w8l9vvnxr4b")))

