(define-module (crates-io op en open_with) #:use-module (crates-io))

(define-public crate-open_with-0.1.0 (c (n "open_with") (v "0.1.0") (d (list (d (n "dbus") (r "^0.9.7") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "osascript") (r "^0.3.0") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("shellapi" "combaseapi"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "1jaa9bvykhwhwzah3p3vl7adfr2jlcalhkc10c429mnmnjy7992r")))

(define-public crate-open_with-0.1.1 (c (n "open_with") (v "0.1.1") (d (list (d (n "dbus") (r "^0.9.7") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "osascript") (r "^0.3.0") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("shellapi" "combaseapi"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "07lgnlnaar9l71j2m0hr3wp15cl8g04j9cgjj1jx8czq49kb3a1n")))

(define-public crate-open_with-0.1.2 (c (n "open_with") (v "0.1.2") (d (list (d (n "dbus") (r "^0.9.7") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "osascript") (r "^0.3.0") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("shellapi" "combaseapi"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "0l1wlbkzb768bzdjy4l7snp0b9ay150xkg8q8rl4jfmrb7y426h9")))

