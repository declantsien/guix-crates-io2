(define-module (crates-io op en openvr_bindings) #:use-module (crates-io))

(define-public crate-openvr_bindings-2.1.0 (c (n "openvr_bindings") (v "2.1.0") (d (list (d (n "bindgen") (r "^0.69") (o #t) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0ipsmwbxbycmcn2xw4wn6pnp22pf62vpn9w6xkpp1sx5n8fpmxpr") (f (quote (("default") ("buildtime_bindgen" "bindgen"))))))

(define-public crate-openvr_bindings-2.1.1 (c (n "openvr_bindings") (v "2.1.1") (d (list (d (n "bindgen") (r "^0.69") (o #t) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "10y001r1pnnm3xkxgrqa6z813w3plnzv3drv7saiv38vjixv4k1m") (f (quote (("default") ("buildtime_bindgen" "bindgen"))))))

