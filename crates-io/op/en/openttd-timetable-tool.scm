(define-module (crates-io op en openttd-timetable-tool) #:use-module (crates-io))

(define-public crate-openttd-timetable-tool-0.1.0 (c (n "openttd-timetable-tool") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)))) (h "0c3v2ryn68qpkcxqddmvljv6i18pac47mssb5mf97s53cxb00jfh")))

(define-public crate-openttd-timetable-tool-0.2.0 (c (n "openttd-timetable-tool") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)))) (h "1vynxxasv25vs67g1vxp2wh1x354mv0lgi8hkihfs2hwq499w1gj")))

