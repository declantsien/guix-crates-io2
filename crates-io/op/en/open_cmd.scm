(define-module (crates-io op en open_cmd) #:use-module (crates-io))

(define-public crate-open_cmd-0.1.0 (c (n "open_cmd") (v "0.1.0") (d (list (d (n "path-clean") (r "^0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)) (d (n "which") (r "^4.2") (d #t) (k 0)))) (h "08hhnircxq543vn355asqd5p6j5xy1cybh363kcsg1hfrdgjffbv")))

