(define-module (crates-io op en openblas-build) #:use-module (crates-io))

(define-public crate-openblas-build-0.1.0 (c (n "openblas-build") (v "0.1.0") (d (list (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "0ayk4fa5npax76dg9v515q71gvankiq2zw6i9lzni3qwf10lq1y1")))

(define-public crate-openblas-build-0.1.1 (c (n "openblas-build") (v "0.1.1") (d (list (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "13rf9z936f7xw2j3hiblqvr41244ii6lmv3c64blnlsnzh0vdv2r")))

(define-public crate-openblas-build-0.10.5 (c (n "openblas-build") (v "0.10.5") (d (list (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "1fij55606q89xp525wkq1qadsj729agvxa4mc69k8i6hw9hax5i7")))

(define-public crate-openblas-build-0.10.6 (c (n "openblas-build") (v "0.10.6") (d (list (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "1zr9x0g6wnc68q6qz514aayval8dkmcrsnrwad9w659083sgswja") (y #t)))

(define-public crate-openblas-build-0.10.7 (c (n "openblas-build") (v "0.10.7") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "flate2") (r "^1.0.25") (d #t) (k 0)) (d (n "tar") (r "^0.4.38") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)) (d (n "ureq") (r "^2.5.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "0ixgnhrb67p43mdzmwwaw4xiib8byivpb3ifqq204cx74cc0f9g5")))

(define-public crate-openblas-build-0.10.8 (c (n "openblas-build") (v "0.10.8") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "flate2") (r "^1.0.25") (d #t) (k 0)) (d (n "native-tls") (r "^0.2.11") (d #t) (k 0)) (d (n "tar") (r "^0.4.38") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)) (d (n "ureq") (r "^2.5.0") (f (quote ("native-certs" "native-tls" "gzip"))) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "0zqa04v3l3d7bziihmpbwfmq5yvcfl5ywycd1905yq3pahwjr97b")))

(define-public crate-openblas-build-0.10.9 (c (n "openblas-build") (v "0.10.9") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "flate2") (r "^1.0.25") (d #t) (k 0)) (d (n "native-tls") (r "^0.2.11") (d #t) (k 0)) (d (n "tar") (r "^0.4.38") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)) (d (n "ureq") (r "^2.5.0") (f (quote ("native-certs" "native-tls" "gzip"))) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "0sg1s6q1r1x5jiyhm0l5xb96snqk4wsarkqm373sz309jm0b9dnl")))

