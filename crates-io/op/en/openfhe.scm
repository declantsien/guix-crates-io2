(define-module (crates-io op en openfhe) #:use-module (crates-io))

(define-public crate-openfhe-0.0.0 (c (n "openfhe") (v "0.0.0") (h "1zj3p2adqmp1mvw1rn4k8zvapvwhhlkr3dnp6k2f9y5faq12dljr")))

(define-public crate-openfhe-0.1.0 (c (n "openfhe") (v "0.1.0") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "0mgxq20m7sipkrxzky6k9fcynryviaq3rxdsmsmy64d1jhsza0pf")))

(define-public crate-openfhe-0.1.1 (c (n "openfhe") (v "0.1.1") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "09zgjnq2bsm915vydpsvqn8vy4789gmfcxxx1y0b7c6773gzaxmf")))

(define-public crate-openfhe-0.1.2 (c (n "openfhe") (v "0.1.2") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "1ppgnbimx05mrvbk2yphy6aycxbjds0v9460zaswd200xz0c9wyz")))

(define-public crate-openfhe-0.1.3 (c (n "openfhe") (v "0.1.3") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "1d8zdicrk661ajja72znxav507bq91chmgxyxd72mzaj6aambwa5")))

(define-public crate-openfhe-0.1.4 (c (n "openfhe") (v "0.1.4") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "1h37gpak4nk45is44xiksi8dmf4hvalpvr9a0m44574sahv8vrpb")))

