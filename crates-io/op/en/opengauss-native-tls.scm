(define-module (crates-io op en opengauss-native-tls) #:use-module (crates-io))

(define-public crate-opengauss-native-tls-0.1.0 (c (n "opengauss-native-tls") (v "0.1.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "native-tls") (r "^0.2") (d #t) (k 0)) (d (n "opengauss") (r "^0.1.0") (d #t) (k 2)) (d (n "tokio") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 2)) (d (n "tokio-native-tls") (r "^0.3") (d #t) (k 0)) (d (n "tokio-opengauss") (r "^0.1.0") (k 0)))) (h "04kg99hd0y18h31x9wjwmv51ji6mx6cf8khmmd6xf0cdlzsr5sam") (f (quote (("runtime" "tokio-opengauss/runtime") ("default" "runtime"))))))

