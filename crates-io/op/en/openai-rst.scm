(define-module (crates-io op en openai-rst) #:use-module (crates-io))

(define-public crate-openai-rst-0.1.0 (c (n "openai-rst") (v "0.1.0") (d (list (d (n "minreq") (r "^2") (f (quote ("https-rustls" "json-using-serde" "proxy"))) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1.0") (k 0)))) (h "14mlrlmkckzc9a55v4wvc7xcfby7k55d41pprsjm8an936c5i9r2")))

