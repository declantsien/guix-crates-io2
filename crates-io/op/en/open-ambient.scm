(define-module (crates-io op en open-ambient) #:use-module (crates-io))

(define-public crate-open-ambient-0.0.0 (c (n "open-ambient") (v "0.0.0") (h "1n02npyn1yf1gcryy0n5qd0az75nxyvsw6lsl46hv2s5q5hwa996")))

(define-public crate-open-ambient-0.0.1 (c (n "open-ambient") (v "0.0.1") (d (list (d (n "cap-std") (r "^0.16.3") (d #t) (k 0)))) (h "1yszgqar2fz3v4qpdm204lhx3nil736dx2g29zfh408nzmdjc4gs")))

(define-public crate-open-ambient-0.1.0 (c (n "open-ambient") (v "0.1.0") (d (list (d (n "cap-std") (r "^0.22.0") (d #t) (k 0)))) (h "1jqs2brq01ms2hd4i8zjn38qyz0lzad8sqi2hbm5lmisq48x7s0s")))

(define-public crate-open-ambient-0.2.0 (c (n "open-ambient") (v "0.2.0") (d (list (d (n "cap-std") (r "^0.25.0") (d #t) (k 0)))) (h "02sdab52w3zn2zjw6nd3yqrlixxdpldivflridg65fg0w830hazf")))

(define-public crate-open-ambient-0.3.0 (c (n "open-ambient") (v "0.3.0") (d (list (d (n "cap-std") (r "^1.0.0") (d #t) (k 0)))) (h "0kvdyp24fy020jqqbj2azcjqm5aqhbcb5a3fz9rwniaz3y41ss6k")))

(define-public crate-open-ambient-0.4.0 (c (n "open-ambient") (v "0.4.0") (d (list (d (n "cap-std") (r "^2.0.0") (d #t) (k 0)))) (h "1v4vhrlj9n5x0djb7may7k9kwbis8i12sfnin5sc3n56ccckrkdf")))

(define-public crate-open-ambient-0.5.0 (c (n "open-ambient") (v "0.5.0") (d (list (d (n "cap-std") (r "^3.0.0") (d #t) (k 0)))) (h "0dlxhbyygnfjlyl2a4d13l3bn3ywz9ykprw845q07h7flq40vqnh")))

