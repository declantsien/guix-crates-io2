(define-module (crates-io op en opengauss-openssl) #:use-module (crates-io))

(define-public crate-opengauss-openssl-0.1.0 (c (n "opengauss-openssl") (v "0.1.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "opengauss") (r "^0.1.0") (d #t) (k 2)) (d (n "openssl") (r "^0.10") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 2)) (d (n "tokio-opengauss") (r "^0.1.0") (k 0)) (d (n "tokio-openssl") (r "^0.6") (d #t) (k 0)))) (h "16g5crryzbh3zdzhbb0gb0qragmf0fblgdgs5n3q48qzl1w29xyy") (f (quote (("runtime" "tokio-opengauss/runtime") ("default" "runtime"))))))

