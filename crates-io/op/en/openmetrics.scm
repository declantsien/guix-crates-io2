(define-module (crates-io op en openmetrics) #:use-module (crates-io))

(define-public crate-openmetrics-0.1.0 (c (n "openmetrics") (v "0.1.0") (d (list (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)))) (h "0gmy2mrb2pwavdh2f0r9rcq8k2l3yw2gr2210lc69my0qqf1lcqb")))

(define-public crate-openmetrics-0.1.1 (c (n "openmetrics") (v "0.1.1") (d (list (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)))) (h "1hmh4rz6xmz6wpdjxlaw3944fdhjgkj4w2m5jbmphxj3bswsi4j4")))

(define-public crate-openmetrics-0.1.3 (c (n "openmetrics") (v "0.1.3") (d (list (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)))) (h "1c4ql8q48485ig0dwp9a37q1xjba6kvlypp5rxxbi4w8pj02bns1")))

(define-public crate-openmetrics-0.2.0 (c (n "openmetrics") (v "0.2.0") (d (list (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)))) (h "1xbbyk3145g0zpziy5xpkn1r7kisvbayb8wxv3ikk67q60gnwafc")))

(define-public crate-openmetrics-0.2.1 (c (n "openmetrics") (v "0.2.1") (d (list (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)))) (h "1y00nrq0b9jdayc0j0g698gvvr0h56h65g9mgskl9plwdl4ywcp3")))

