(define-module (crates-io op en openapi_rocketapi) #:use-module (crates-io))

(define-public crate-openapi_rocketapi-0.1.5 (c (n "openapi_rocketapi") (v "0.1.5") (d (list (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.5") (d #t) (k 2)) (d (n "semver") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "url") (r "^1.7") (d #t) (k 0)) (d (n "url_serde") (r "^0.2") (d #t) (k 0)))) (h "1lxck3grajxkl84zf34lqscsy1krivixzfdcyyd3x964bwrgb4zj")))

