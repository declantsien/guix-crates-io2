(define-module (crates-io op en openfga-rs) #:use-module (crates-io))

(define-public crate-openfga-rs-0.1.0 (c (n "openfga-rs") (v "0.1.0") (d (list (d (n "prost") (r "~0.12") (f (quote ("std" "prost-derive"))) (d #t) (k 0)) (d (n "prost-build") (r "~0.12") (d #t) (k 1)) (d (n "prost-types") (r "~0.12") (d #t) (k 0)) (d (n "prost-wkt") (r "~0.5") (d #t) (k 0)) (d (n "prost-wkt-build") (r "~0.5") (d #t) (k 1)) (d (n "prost-wkt-types") (r "~0.5") (d #t) (k 0)) (d (n "serde") (r "~1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tonic") (r "~0.11") (f (quote ("tls"))) (d #t) (k 0)) (d (n "tonic-build") (r "~0.11") (d #t) (k 1)))) (h "1h0j8dc6b9hcy85c73f9zhx3w27jkmy74dwif6v3k505h7wxjvri")))

