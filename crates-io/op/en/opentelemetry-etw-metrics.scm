(define-module (crates-io op en opentelemetry-etw-metrics) #:use-module (crates-io))

(define-public crate-opentelemetry-etw-metrics-0.1.0 (c (n "opentelemetry-etw-metrics") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "opentelemetry") (r "^0.22") (f (quote ("metrics"))) (d #t) (k 0)) (d (n "opentelemetry-proto") (r "^0.5") (f (quote ("gen-tonic" "metrics"))) (k 0)) (d (n "opentelemetry_sdk") (r "^0.22") (f (quote ("metrics" "rt-tokio"))) (k 0)) (d (n "prost") (r "^0.12") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 2)) (d (n "tracelogging") (r "^1.2.1") (d #t) (k 0)))) (h "11gvbsv0mdsx34fig81zr6pv4ajkvqk30mxk6rzhh3gh93rxhm75") (r "1.65")))

