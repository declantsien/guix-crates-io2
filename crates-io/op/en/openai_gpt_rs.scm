(define-module (crates-io op en openai_gpt_rs) #:use-module (crates-io))

(define-public crate-openai_gpt_rs-0.1.0 (c (n "openai_gpt_rs") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.65") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("full"))) (d #t) (k 0)))) (h "054r5wsd5b76xgw9xid1vmr0vf80j8mfg146ff8za945q61fpwka")))

(define-public crate-openai_gpt_rs-0.1.1 (c (n "openai_gpt_rs") (v "0.1.1") (d (list (d (n "async-trait") (r "^0.1.65") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0gm7ldw1cy9j7x42czd0dyjlvas9l8xd524qcaxgyyqacx3fdkm7")))

(define-public crate-openai_gpt_rs-0.2.0 (c (n "openai_gpt_rs") (v "0.2.0") (d (list (d (n "async-trait") (r "^0.1.65") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("full"))) (d #t) (k 0)))) (h "00g4fji2yj8mwp3z4xa98dk0xgqwp7nynjii5bfm2ilhccmhwq51")))

(define-public crate-openai_gpt_rs-0.2.1 (c (n "openai_gpt_rs") (v "0.2.1") (d (list (d (n "async-trait") (r "^0.1.65") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1ms4f2fczdqq62hspkz38lp898p31zj836nyhagbi1q1rb18cz32")))

(define-public crate-openai_gpt_rs-0.2.2 (c (n "openai_gpt_rs") (v "0.2.2") (d (list (d (n "async-trait") (r "^0.1.65") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("full"))) (d #t) (k 0)))) (h "09n26xyhpal0dij84c6lpal39ms5c6vdbwvh1gkpah6amslbvbqc")))

(define-public crate-openai_gpt_rs-0.3.0 (c (n "openai_gpt_rs") (v "0.3.0") (d (list (d (n "async-trait") (r "^0.1.65") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0sv7dzsjsixfinq7fxkmwwkgfna0075nriahfizlhsvdf8vmgqhj")))

(define-public crate-openai_gpt_rs-0.3.2 (c (n "openai_gpt_rs") (v "0.3.2") (d (list (d (n "async-trait") (r "^0.1.65") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0bkksf6gn9sw5xc887nfda6l3dwm18b613jjp8k3db308bpdy2hv")))

(define-public crate-openai_gpt_rs-0.4.0 (c (n "openai_gpt_rs") (v "0.4.0") (d (list (d (n "async-trait") (r "^0.1.65") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0gg1cxqpgx1w4xja1przgzqxkgm03jbd5pgn01a0zqvb86jpjwjs")))

(define-public crate-openai_gpt_rs-0.4.1 (c (n "openai_gpt_rs") (v "0.4.1") (d (list (d (n "async-trait") (r "^0.1.65") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0938929mlk7xn4k445ncdjlcpd57j6m509kj17cx9di0iqpw6n35")))

