(define-module (crates-io op en openvr) #:use-module (crates-io))

(define-public crate-openvr-0.1.1 (c (n "openvr") (v "0.1.1") (d (list (d (n "lazy_static") (r "0.2.*") (d #t) (k 0)) (d (n "libloading") (r "0.2.*") (d #t) (k 0)))) (h "08xzwjl3gcmswxfl9v1njn1smdrv32p3byhqszxyzwgbhmqzackj")))

(define-public crate-openvr-0.5.0 (c (n "openvr") (v "0.5.0") (d (list (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)) (d (n "openvr_sys") (r "^2") (d #t) (k 0)))) (h "1w3y4i5dqznd9s4l4i9wr8jx39ax6v225zf39l9jzbyb7pdw2260")))

(define-public crate-openvr-0.5.1 (c (n "openvr") (v "0.5.1") (d (list (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)) (d (n "openvr_sys") (r "^2") (d #t) (k 0)))) (h "1snn4lcblzl8r6ys36h9zca9528sbhp0fn9vqck9bg1wzsab0gp8")))

(define-public crate-openvr-0.5.2 (c (n "openvr") (v "0.5.2") (d (list (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)) (d (n "openvr_sys") (r "^2") (d #t) (k 0)))) (h "0la8647i820frc1jb0g4ac79xykflkrx1ldwml154c9i37sxcbgz")))

(define-public crate-openvr-0.6.0 (c (n "openvr") (v "0.6.0") (d (list (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "openvr_sys") (r "^2.0.3") (d #t) (k 0)))) (h "0q1kw9ls91dckzcrf3m43cwd7bk1mfabfd420rjr0zid4g1jga4k")))

