(define-module (crates-io op en open-vcdiff-sys) #:use-module (crates-io))

(define-public crate-open-vcdiff-sys-0.1.0 (c (n "open-vcdiff-sys") (v "0.1.0") (d (list (d (n "gcc") (r "^0.3.39") (d #t) (k 1)) (d (n "tempdir") (r "^0.3.5") (d #t) (k 1)))) (h "12jbvr5f4zj7i0kq2ix1r0v54jik1zl21nj98r8gns2kqpji6sdm")))

(define-public crate-open-vcdiff-sys-0.1.1 (c (n "open-vcdiff-sys") (v "0.1.1") (d (list (d (n "gcc") (r "^0.3.39") (d #t) (k 1)) (d (n "tempdir") (r "^0.3.5") (d #t) (k 1)))) (h "1v3ka3ni14lh3hmddfl6kji82lzajjkdmfxbmxg10bi2vwwfzjbf")))

