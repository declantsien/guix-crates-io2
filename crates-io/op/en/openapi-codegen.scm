(define-module (crates-io op en openapi-codegen) #:use-module (crates-io))

(define-public crate-openapi-codegen-0.1.0 (c (n "openapi-codegen") (v "0.1.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "handlebars") (r "^1") (d #t) (k 0)) (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "openapiv3") (r "^0.1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "1a6dffzq9igky0s72avxl4w9hrrrwjr87c77hxqx68vi9sccy34y")))

