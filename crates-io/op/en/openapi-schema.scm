(define-module (crates-io op en openapi-schema) #:use-module (crates-io))

(define-public crate-openapi-schema-0.1.0 (c (n "openapi-schema") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "serde_test") (r "^1.0.163") (d #t) (k 0)))) (h "0158b0vmwayain3f0rf1kz5kvyalrzbwg31v6clnbqxy6gllq5l7")))

(define-public crate-openapi-schema-0.1.1 (c (n "openapi-schema") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "serde_test") (r "^1.0.163") (d #t) (k 0)))) (h "17lmd9b73f8h7wbi7ra7xni1cikaay5dl9cq0wh116jdmgjsri2l")))

(define-public crate-openapi-schema-0.1.2 (c (n "openapi-schema") (v "0.1.2") (d (list (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "serde_test") (r "^1.0.163") (d #t) (k 0)))) (h "1zdv6gb3ha9srligmk7s3m6przgk96s8iv1dl9kcv2kaj4kh1yb4")))

(define-public crate-openapi-schema-0.1.3 (c (n "openapi-schema") (v "0.1.3") (d (list (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "serde_test") (r "^1.0.163") (d #t) (k 0)))) (h "1rb41i52d2rw6nfnn852alcvqcjcm4i3ycas1i5rvam93wwjn13i")))

(define-public crate-openapi-schema-0.1.4 (c (n "openapi-schema") (v "0.1.4") (d (list (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "serde_test") (r "^1.0.163") (d #t) (k 0)))) (h "1q26bj478igw6l2kfh72lisxv13sb23f5jbxxpzwawjr4y51qhfj")))

(define-public crate-openapi-schema-0.1.5 (c (n "openapi-schema") (v "0.1.5") (d (list (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "serde_test") (r "^1.0.163") (d #t) (k 0)))) (h "04s2v8axb2d1b8yqv46y42yqlldzgk0zp6vgydkkd9lzhvi43pxq")))

(define-public crate-openapi-schema-0.1.6 (c (n "openapi-schema") (v "0.1.6") (d (list (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "serde_test") (r "^1.0.163") (d #t) (k 0)))) (h "0b1sfj7dq4a0f7576psqdrkcwx1hxkjkn1130ck4lwfd8g4p0rvv")))

(define-public crate-openapi-schema-0.1.7 (c (n "openapi-schema") (v "0.1.7") (d (list (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "serde_test") (r "^1.0.163") (d #t) (k 0)))) (h "1sdh03cl12r4lvbavy7k5hwwa1mxminqlpm1wqnilj961rakrbf2")))

(define-public crate-openapi-schema-0.1.8 (c (n "openapi-schema") (v "0.1.8") (d (list (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "serde_test") (r "^1.0.163") (d #t) (k 0)))) (h "056l4pnrhqr8skb1ab4brr40vlgm6ajqiqhmkrlf03lmgn94cfqs")))

(define-public crate-openapi-schema-0.1.9 (c (n "openapi-schema") (v "0.1.9") (d (list (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "serde_test") (r "^1.0.163") (d #t) (k 0)))) (h "0immsaj5b1klaq9i7x88bhs3gdm33s8idzmg5npl9k6i6jpv6av1")))

(define-public crate-openapi-schema-0.1.10 (c (n "openapi-schema") (v "0.1.10") (d (list (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "serde_test") (r "^1.0.163") (d #t) (k 0)))) (h "1vkpm7swv3ya58w59v5q2xnkx60iff551yjhkdr0zfp5ja4rp5x6")))

