(define-module (crates-io op en opencc) #:use-module (crates-io))

(define-public crate-opencc-0.1.0 (c (n "opencc") (v "0.1.0") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "1s1wlfzv46wsd7kjyrh1jz7gz9iriaa8yppfs5zdvmbvd999053b")))

(define-public crate-opencc-0.1.1 (c (n "opencc") (v "0.1.1") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "189qgfhqcdm24vkqrj2vf4r9g1p61kzw9sh694y56q77cdzhcfd6")))

(define-public crate-opencc-0.2.0 (c (n "opencc") (v "0.2.0") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "12grdmdqdy6lmb2cjjgzl9p3lbzldlz2csa6pcfzh1295zqdj2zj") (f (quote (("unstable"))))))

(define-public crate-opencc-0.2.1 (c (n "opencc") (v "0.2.1") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "1x70p8yqmdr7499fc70635bj77lwj5a83z4mh8r0cj0hj2550did") (f (quote (("unstable"))))))

(define-public crate-opencc-0.2.2 (c (n "opencc") (v "0.2.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "01pi6l7bksjsbhyqfj03pj4q765rb5axaq34z6rkpdrn60scrrlg") (f (quote (("unstable"))))))

(define-public crate-opencc-0.2.3 (c (n "opencc") (v "0.2.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1kpv3hiw2hdlfk91v4zljrikv8lfgf23lr7fbmkpdmncdw5dvha5") (f (quote (("unstable"))))))

(define-public crate-opencc-0.3.0 (c (n "opencc") (v "0.3.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1l9mz8rq7592zf6bj264irrw8c7pvgn3qh9hgawyms7y2a8v2ypf") (f (quote (("unstable"))))))

