(define-module (crates-io op en open62541-sys) #:use-module (crates-io))

(define-public crate-open62541-sys-0.1.3 (c (n "open62541-sys") (v "0.1.3") (d (list (d (n "bindgen") (r "^0.69.1") (f (quote ("experimental"))) (d #t) (k 1)) (d (n "cc") (r "^1.0.83") (d #t) (k 1)) (d (n "cmake") (r "^0.1.50") (d #t) (k 1)))) (h "09vamfkifpqn27k23blkmgza7598kmplknspmd7islqzdk0w2rjs") (r "1.70")))

(define-public crate-open62541-sys-0.2.0 (c (n "open62541-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.69.1") (f (quote ("experimental"))) (d #t) (k 1)) (d (n "cc") (r "^1.0.83") (d #t) (k 1)) (d (n "cmake") (r "^0.1.50") (d #t) (k 1)))) (h "0yrqr8byk4cc8qzzi64j8lr6s4dkzkbkd29wijmx1q67barlabny") (r "1.70")))

(define-public crate-open62541-sys-0.2.1 (c (n "open62541-sys") (v "0.2.1") (d (list (d (n "bindgen") (r "^0.69.1") (f (quote ("experimental"))) (d #t) (k 1)) (d (n "cc") (r "^1.0.83") (d #t) (k 1)) (d (n "cmake") (r "^0.1.50") (d #t) (k 1)))) (h "07zp1lrlz7d1msxzxr61rk9ipkg7qiarq3cmljvcxhq4lnifqcn9") (r "1.70")))

(define-public crate-open62541-sys-0.2.2 (c (n "open62541-sys") (v "0.2.2") (d (list (d (n "bindgen") (r "^0.69.1") (f (quote ("experimental"))) (d #t) (k 1)) (d (n "cc") (r "^1.0.83") (d #t) (k 1)) (d (n "cmake") (r "^0.1.50") (d #t) (k 1)))) (h "1qvr9n5pvk37y0y97ij1f9mxrkvc2p8faraas96fjsqin2qfz3sm") (r "1.70")))

(define-public crate-open62541-sys-0.3.0 (c (n "open62541-sys") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.69.1") (f (quote ("experimental"))) (d #t) (k 1)) (d (n "cc") (r "^1.0.83") (d #t) (k 1)) (d (n "cmake") (r "^0.1.50") (d #t) (k 1)))) (h "1769kv1db8kaivypl3z6c12fgwm9a5l1pmzw70dylai7irv08plw") (r "1.70")))

(define-public crate-open62541-sys-0.3.1 (c (n "open62541-sys") (v "0.3.1") (d (list (d (n "bindgen") (r "^0.69.1") (f (quote ("experimental"))) (d #t) (k 1)) (d (n "cc") (r "^1.0.83") (d #t) (k 1)) (d (n "cmake") (r "^0.1.50") (d #t) (k 1)))) (h "0qzlrwj9mfcc7z24bnzfrdssjgmj1fdp8zfbvhdwwy8cjbvx8dxg") (r "1.70")))

(define-public crate-open62541-sys-0.3.2 (c (n "open62541-sys") (v "0.3.2") (d (list (d (n "bindgen") (r "^0.69.1") (f (quote ("experimental"))) (d #t) (k 1)) (d (n "cc") (r "^1.0.83") (d #t) (k 1)) (d (n "cmake") (r "^0.1.50") (d #t) (k 1)))) (h "1v08vyzmjvs8799j84xclnbgjfdqpgd01mkyc5p0lfkfgr3zfx47") (r "1.70")))

(define-public crate-open62541-sys-0.3.3 (c (n "open62541-sys") (v "0.3.3") (d (list (d (n "bindgen") (r "^0.69.1") (f (quote ("experimental"))) (d #t) (k 1)) (d (n "cc") (r "^1.0.83") (d #t) (k 1)) (d (n "cmake") (r "^0.1.50") (d #t) (k 1)))) (h "0b99zhkhb72692g7al7b8w6qb9mkwqyhkrw7b18qdzv5jgqyiqfw") (r "1.72")))

(define-public crate-open62541-sys-0.4.0-pre.1 (c (n "open62541-sys") (v "0.4.0-pre.1") (d (list (d (n "bindgen") (r "^0.69.4") (f (quote ("experimental"))) (d #t) (k 1)) (d (n "cc") (r "^1.0.83") (d #t) (k 1)) (d (n "cmake") (r "^0.1.50") (d #t) (k 1)))) (h "0pynwymllbcrbf3pd6ax73jd76h3kc33ca4f3pkw6q1b56pnjczr") (r "1.72")))

(define-public crate-open62541-sys-0.4.0-pre.2 (c (n "open62541-sys") (v "0.4.0-pre.2") (d (list (d (n "bindgen") (r "^0.69.4") (f (quote ("experimental"))) (d #t) (k 1)) (d (n "cc") (r "^1.0.83") (d #t) (k 1)) (d (n "cmake") (r "^0.1.50") (d #t) (k 1)))) (h "1l13gbiwpq6kiwz9i6807xzi8w1l9rwfa2bg9xsnr9viyyqpmqmi") (r "1.72")))

(define-public crate-open62541-sys-0.4.0-pre.3 (c (n "open62541-sys") (v "0.4.0-pre.3") (d (list (d (n "bindgen") (r "^0.69.4") (f (quote ("experimental"))) (d #t) (k 1)) (d (n "cc") (r "^1.0.83") (d #t) (k 1)) (d (n "cmake") (r "^0.1.50") (d #t) (k 1)))) (h "1dpi43130bdg94bm5hdnyds721ffv0xhij7056ly6awl5a74ny6x") (r "1.72")))

(define-public crate-open62541-sys-0.4.0-pre.4 (c (n "open62541-sys") (v "0.4.0-pre.4") (d (list (d (n "bindgen") (r "^0.69.4") (f (quote ("experimental"))) (d #t) (k 1)) (d (n "cc") (r "^1.0.83") (d #t) (k 1)) (d (n "cmake") (r "^0.1.50") (d #t) (k 1)))) (h "0p29iw3jmil92fvbd00n1s15a217qncdm6z55n8fc4hq9mxichvb") (r "1.72")))

