(define-module (crates-io op en openmp-sys) #:use-module (crates-io))

(define-public crate-openmp-sys-0.1.0 (c (n "openmp-sys") (v "0.1.0") (d (list (d (n "cc") (r "^1.0.3") (d #t) (k 1)))) (h "0jaxrzji4qyfkhhgsfb8k8ql9irpf3zsgzsh5qz1galff826nbwi") (f (quote (("static")))) (y #t)))

(define-public crate-openmp-sys-0.1.1 (c (n "openmp-sys") (v "0.1.1") (d (list (d (n "cc") (r "^1.0.3") (d #t) (k 1)))) (h "1sz7q0r2han8r5ghp4fkwvj2ldax6przmjgsrsqy6qjrwjlngk00") (f (quote (("static")))) (y #t)))

(define-public crate-openmp-sys-0.1.2 (c (n "openmp-sys") (v "0.1.2") (d (list (d (n "cc") (r "^1.0.3") (d #t) (k 1)))) (h "0j9zkc3f0mb2ahdyqzp9dm05z7cjqmw95wpdisk6b2acwncggnlq") (f (quote (("static")))) (y #t)))

(define-public crate-openmp-sys-0.1.3 (c (n "openmp-sys") (v "0.1.3") (d (list (d (n "cc") (r "^1.0.3") (d #t) (k 1)))) (h "1k8mcrk596zinsyad49ck6han2ww5v4av4yvjc0mfzpjcrsf22ci") (f (quote (("static")))) (y #t)))

(define-public crate-openmp-sys-0.1.5 (c (n "openmp-sys") (v "0.1.5") (d (list (d (n "cc") (r "^1.0.3") (d #t) (k 1)))) (h "0zi500m4348mr6k264hxmq564hcxgd0yy3vbjr7f5rzgd4mynzkv") (f (quote (("static")))) (y #t) (l "openmp")))

(define-public crate-openmp-sys-0.1.6 (c (n "openmp-sys") (v "0.1.6") (d (list (d (n "cc") (r "^1.0.3") (d #t) (k 1)))) (h "1np4rc4f65nvc3hfh98djiab2wx8hwb4c78d3sslsap1xb0c19b0") (f (quote (("static")))) (y #t) (l "openmp")))

(define-public crate-openmp-sys-0.1.7 (c (n "openmp-sys") (v "0.1.7") (d (list (d (n "cc") (r "^1.0.50") (d #t) (k 1)))) (h "04ws58i4h704q9ahs91z63cyrxlizcaqfr61v3mq7nmwv7va0svd") (f (quote (("static")))) (l "openmp")))

(define-public crate-openmp-sys-1.0.0 (c (n "openmp-sys") (v "1.0.0") (d (list (d (n "cc") (r "^1.0.50") (d #t) (k 1)))) (h "13pv9wwjb730377hpipv2h90a2pdv9b0cx0hh879wj3d13nmnfxl") (f (quote (("static")))) (l "openmp")))

(define-public crate-openmp-sys-1.2.0-alpha.1 (c (n "openmp-sys") (v "1.2.0-alpha.1") (d (list (d (n "cc") (r "^1.0.66") (d #t) (k 1)))) (h "15qzc9sfvj8bh52qkfmggfidw2858krxdgawb3xjsqwj0kncj4yr") (f (quote (("static")))) (y #t) (l "openmp")))

(define-public crate-openmp-sys-1.2.0 (c (n "openmp-sys") (v "1.2.0") (d (list (d (n "cc") (r "^1.0.67") (d #t) (k 1)))) (h "0yir88fvsma1y6s4ih8haarbjaw247nkhvs1am8kxgw3g50rsc6n") (f (quote (("static")))) (l "openmp")))

(define-public crate-openmp-sys-1.2.1 (c (n "openmp-sys") (v "1.2.1") (d (list (d (n "cc") (r "^1.0.67") (d #t) (k 1)))) (h "1hz3pga7l0c1z5xdajr8f5vn1w7d5kg9wmc7sjj4hg01177kphh6") (f (quote (("static")))) (l "openmp")))

(define-public crate-openmp-sys-1.2.2 (c (n "openmp-sys") (v "1.2.2") (d (list (d (n "cc") (r "^1.0.67") (d #t) (k 1)))) (h "1l050vavrsyqmking05gl81gvhksjr0l8ccl4p58h4wfqmyv7qnr") (f (quote (("static")))) (l "openmp")))

(define-public crate-openmp-sys-1.2.3 (c (n "openmp-sys") (v "1.2.3") (d (list (d (n "cc") (r "^1.0.67") (d #t) (k 1)))) (h "1yhpwf397rczjlwcw93i5g12c1hrfkl60ghjb6d2nkbrs9vyv853") (f (quote (("static")))) (l "openmp")))

