(define-module (crates-io op en openweather_rs) #:use-module (crates-io))

(define-public crate-openweather_rs-0.1.0 (c (n "openweather_rs") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros"))) (d #t) (k 0)))) (h "0cz2smnnz5g3ffyqs4sgqxlcv9j0wy37v9p2z6wk2i9hkdap79wx")))

(define-public crate-openweather_rs-0.1.1 (c (n "openweather_rs") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros"))) (d #t) (k 0)))) (h "05zmg03zrp9q3489zhy7gd1cfizqihla87drbqpk1jv3ywmx0f4f")))

(define-public crate-openweather_rs-0.1.2 (c (n "openweather_rs") (v "0.1.2") (d (list (d (n "reqwest") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros"))) (d #t) (k 0)))) (h "0jllkx1zn7sl2cg86c2z13d8nahjycvpq0h5gh3lv04iyccsn9nc")))

(define-public crate-openweather_rs-0.1.3 (c (n "openweather_rs") (v "0.1.3") (d (list (d (n "reqwest") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros"))) (d #t) (k 0)))) (h "0smzaj6c07850jxarnnixwpa1ag05pxf0dxmg7rvwrb5vkq2m218")))

