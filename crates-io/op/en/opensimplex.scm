(define-module (crates-io op en opensimplex) #:use-module (crates-io))

(define-public crate-opensimplex-0.1.0 (c (n "opensimplex") (v "0.1.0") (d (list (d (n "gcc") (r "^0.3.13") (d #t) (k 1)) (d (n "libc") (r ">= 0.1") (d #t) (k 0)))) (h "0gbpz1axldhz54p7n0k0rwkl93l3m69vx0q3v0ia3jpn7j4zdmi1")))

(define-public crate-opensimplex-0.2.0 (c (n "opensimplex") (v "0.2.0") (d (list (d (n "gcc") (r "^0.3.13") (d #t) (k 1)) (d (n "libc") (r ">= 0.1") (d #t) (k 0)))) (h "0zpii5s46vw03bna7l13br3k5vlscfm4kvffhq2qhn2sfz0h2arc")))

