(define-module (crates-io op en opensmtpd_derive) #:use-module (crates-io))

(define-public crate-opensmtpd_derive-0.4.0 (c (n "opensmtpd_derive") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "parsing" "printing" "proc-macro"))) (k 0)))) (h "099mf01wa6xvpwbqyg774qb7iyqn4rslf9rgaznqw0ic0m6r3zlb")))

(define-public crate-opensmtpd_derive-0.4.1 (c (n "opensmtpd_derive") (v "0.4.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "parsing" "printing" "proc-macro"))) (k 0)))) (h "05yn0yq5n4hy6gq7zbsca44x4pjzn3120lj0b8y3j08q64iwjca9")))

