(define-module (crates-io op en openai-rs) #:use-module (crates-io))

(define-public crate-openai-rs-0.1.0 (c (n "openai-rs") (v "0.1.0") (d (list (d (n "hyper") (r "^0.14.19") (f (quote ("client" "http2"))) (d #t) (k 0)) (d (n "hyper-openssl") (r "^0.9.2") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0rqwarc05air5crkzrbsykldqlbavk1869gbnwvmq28q6l2zljkr")))

(define-public crate-openai-rs-0.1.1 (c (n "openai-rs") (v "0.1.1") (d (list (d (n "hyper") (r "^0.14.19") (f (quote ("client" "http2"))) (d #t) (k 0)) (d (n "hyper-openssl") (r "^0.9.2") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1vzqfam0npsp324rya1f007z0fl5rrmsik9r14ly4ysxk22i2ksf")))

