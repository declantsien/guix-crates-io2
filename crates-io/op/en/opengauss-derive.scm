(define-module (crates-io op en opengauss-derive) #:use-module (crates-io))

(define-public crate-opengauss-derive-0.1.0 (c (n "opengauss-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1xlqdbly8n72yrw6jvd2f34bfqc1j6kp32vnr9vl3avzxg12yp03")))

