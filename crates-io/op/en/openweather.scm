(define-module (crates-io op en openweather) #:use-module (crates-io))

(define-public crate-openweather-0.0.1 (c (n "openweather") (v "0.0.1") (d (list (d (n "reqwest") (r "^0.8.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.70") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.70") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "time") (r "^0.1.40") (d #t) (k 0)) (d (n "url") (r "^1.5.1") (d #t) (k 0)))) (h "0afmdh6iz6mhppg851gz143wliwjjak3hsk1xfcl8w3qx6f04wb6")))

