(define-module (crates-io op en openat-ext) #:use-module (crates-io))

(define-public crate-openat-ext-0.1.0 (c (n "openat-ext") (v "0.1.0") (d (list (d (n "openat") (r "^0.1.15") (d #t) (k 0)) (d (n "tempfile") (r "^3.0.3") (d #t) (k 2)))) (h "1fr497pdaqkicnkz1gxkwhak0yy5gr6syr9ri9ndwvs4ahmanpqm")))

(define-public crate-openat-ext-0.1.2 (c (n "openat-ext") (v "0.1.2") (d (list (d (n "libc") (r "^0.2.34") (d #t) (k 0)) (d (n "openat") (r "^0.1.15") (d #t) (k 0)) (d (n "tempfile") (r "^3.0.3") (d #t) (k 2)))) (h "0xnfr7wgw339ap0315ra4mrhr3g39yb5kb3k3qfi1ndqvhik46r8")))

(define-public crate-openat-ext-0.1.1 (c (n "openat-ext") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.34") (d #t) (k 0)) (d (n "nix") (r "^0.17") (d #t) (k 0)) (d (n "openat") (r "^0.1.15") (d #t) (k 0)) (d (n "tempfile") (r "^3.0.3") (d #t) (k 2)))) (h "06h3g03x93c7fl0fi3in37n4r0b2xrcw4vz6lamp4vym9lcy7vm4")))

(define-public crate-openat-ext-0.1.3 (c (n "openat-ext") (v "0.1.3") (d (list (d (n "libc") (r "^0.2.34") (d #t) (k 0)) (d (n "nix") (r "^0.17") (d #t) (k 0)) (d (n "openat") (r "^0.1.15") (d #t) (k 0)) (d (n "tempfile") (r "^3.0.3") (d #t) (k 2)))) (h "1vyqdl9b4ksigr8l88kxw1ncp8ff4spzf0my9z7j7h449yvcxdvj")))

(define-public crate-openat-ext-0.1.4 (c (n "openat-ext") (v "0.1.4") (d (list (d (n "libc") (r "^0.2.34") (d #t) (k 0)) (d (n "nix") (r "^0.17") (d #t) (k 0)) (d (n "openat") (r "^0.1.15") (d #t) (k 0)) (d (n "tempfile") (r "^3.0.3") (d #t) (k 2)))) (h "1s77rki4qw6b73p7amhw68j09vwmqlgh8n1kksric48qkl2j5i1d")))

(define-public crate-openat-ext-0.1.5 (c (n "openat-ext") (v "0.1.5") (d (list (d (n "libc") (r "^0.2.34") (d #t) (k 0)) (d (n "nix") (r "^0.17") (d #t) (k 0)) (d (n "openat") (r "^0.1.15") (d #t) (k 0)) (d (n "tempfile") (r "^3.0.3") (d #t) (k 2)))) (h "1mm15nb9m8llh8g4g7r27p87mmz04vqfcz73dm8krb2wyrga2qi8")))

(define-public crate-openat-ext-0.1.6 (c (n "openat-ext") (v "0.1.6") (d (list (d (n "libc") (r "^0.2.34") (d #t) (k 0)) (d (n "nix") (r "^0.17") (d #t) (k 0)) (d (n "openat") (r "^0.1.15") (d #t) (k 0)) (d (n "tempfile") (r "^3.0.3") (d #t) (k 2)))) (h "0n8biw57pns8bprs4zx3izqmgblp1z1xqza9nklblm5x9zjy26xg")))

(define-public crate-openat-ext-0.1.7 (c (n "openat-ext") (v "0.1.7") (d (list (d (n "anyhow") (r "^1.0.33") (d #t) (k 2)) (d (n "drop_bomb") (r "^0.1.5") (d #t) (k 0)) (d (n "libc") (r "^0.2.34") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)) (d (n "openat") (r "^0.1.15") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "tempfile") (r "^3.0.3") (d #t) (k 2)))) (h "0k3wnaxkjh9x41lz089isz1y243z70dpvbrh65l448bl04k6z3za")))

(define-public crate-openat-ext-0.1.8 (c (n "openat-ext") (v "0.1.8") (d (list (d (n "anyhow") (r "^1.0.33") (d #t) (k 2)) (d (n "drop_bomb") (r "^0.1.5") (d #t) (k 0)) (d (n "libc") (r "^0.2.34") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)) (d (n "openat") (r "^0.1.15") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "tempfile") (r "^3.0.3") (d #t) (k 2)))) (h "12rb5n8qgrpzcyxdw3rnjm8a1i6bynwl9a629pshpwfajbkmxs3k")))

(define-public crate-openat-ext-0.1.9 (c (n "openat-ext") (v "0.1.9") (d (list (d (n "anyhow") (r "^1.0.33") (d #t) (k 2)) (d (n "drop_bomb") (r "^0.1.5") (d #t) (k 0)) (d (n "libc") (r "^0.2.34") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)) (d (n "openat") (r "^0.1.15") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "tempfile") (r "^3.0.3") (d #t) (k 2)))) (h "1yahdr1d5w6gxk4c3jn2ccg841j2wihfvlii8abwcgq0z42b5h9x")))

(define-public crate-openat-ext-0.1.10 (c (n "openat-ext") (v "0.1.10") (d (list (d (n "anyhow") (r "^1.0.33") (d #t) (k 2)) (d (n "drop_bomb") (r "^0.1.5") (d #t) (k 0)) (d (n "libc") (r "^0.2.34") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)) (d (n "openat") (r "^0.1.15") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "tempfile") (r "^3.0.3") (d #t) (k 2)))) (h "0lba9rzy3dvna7vpj1lrypp3q1nrv4k3ngnq5pi9q2rl6s1nib67")))

(define-public crate-openat-ext-0.1.11 (c (n "openat-ext") (v "0.1.11") (d (list (d (n "anyhow") (r "^1.0.33") (d #t) (k 2)) (d (n "drop_bomb") (r "^0.1.5") (d #t) (k 0)) (d (n "libc") (r "^0.2.34") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)) (d (n "openat") (r "^0.1.15") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "tempfile") (r "^3.0.3") (d #t) (k 2)))) (h "1giklzfwixrnf9hidi36zgzm212jp1ax6l8d38b8ymnslb3ynmsi")))

(define-public crate-openat-ext-0.1.12 (c (n "openat-ext") (v "0.1.12") (d (list (d (n "anyhow") (r "^1.0.33") (d #t) (k 2)) (d (n "drop_bomb") (r "^0.1.5") (d #t) (k 0)) (d (n "libc") (r "^0.2.34") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)) (d (n "openat") (r "^0.1.15") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "tempfile") (r "^3.0.3") (d #t) (k 2)))) (h "1i45djj8p2qrh6ds2d187pymlkkmil4pnrpdqgrjdljpidfdh318")))

(define-public crate-openat-ext-0.1.13 (c (n "openat-ext") (v "0.1.13") (d (list (d (n "anyhow") (r "^1.0.33") (d #t) (k 2)) (d (n "drop_bomb") (r "^0.1.5") (d #t) (k 0)) (d (n "libc") (r "^0.2.34") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)) (d (n "openat") (r "^0.1.15") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "tempfile") (r "^3.0.3") (d #t) (k 2)))) (h "16a4qr5gw8hdfp9gsvm4zzlxsj75ljlv5j8wymzxz5c9q8bmxidl")))

(define-public crate-openat-ext-0.2.0 (c (n "openat-ext") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.33") (d #t) (k 2)) (d (n "libc") (r "^0.2.34") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)) (d (n "openat") (r "^0.1.15") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "tempfile") (r "^3.0.3") (d #t) (k 2)))) (h "1zl81ahlh3yh23vlm10h8dyplscxs0da378np4ijr7jc8k5da25b")))

(define-public crate-openat-ext-0.2.1 (c (n "openat-ext") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.33") (d #t) (k 2)) (d (n "libc") (r "^0.2.94") (d #t) (k 0)) (d (n "nix") (r ">=0.18, <0.21") (d #t) (k 0)) (d (n "openat") (r "^0.1.15") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "tempfile") (r "^3.0.3") (d #t) (k 2)))) (h "04ccrph3nmwca70s2nn6qrbn2cfsg3jhglr1la9zifyfhygw7n34")))

(define-public crate-openat-ext-0.2.2 (c (n "openat-ext") (v "0.2.2") (d (list (d (n "anyhow") (r "^1.0.33") (d #t) (k 2)) (d (n "libc") (r "^0.2.94") (d #t) (k 0)) (d (n "nix") (r ">=0.18, <0.23") (d #t) (k 0)) (d (n "openat") (r "^0.1.15") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "tempfile") (r "^3.0.3") (d #t) (k 2)))) (h "0nyqnlv1154cdv9s7gj82zwimbhd097z3bnxlwn9dnkaimffbsq8")))

(define-public crate-openat-ext-0.2.3 (c (n "openat-ext") (v "0.2.3") (d (list (d (n "anyhow") (r "^1.0.33") (d #t) (k 2)) (d (n "libc") (r "^0.2.94") (d #t) (k 0)) (d (n "nix") (r ">=0.22, <0.24") (d #t) (k 0)) (d (n "openat") (r "^0.1.15") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "tempfile") (r "^3.0.3") (d #t) (k 2)))) (h "07f62hh0hfri1n5fcl1b5vx5s6p9ysm5hgrpb0gl85pmlyxf9wrc")))

