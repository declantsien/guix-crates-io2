(define-module (crates-io op en openvx) #:use-module (crates-io))

(define-public crate-openvx-0.1.0 (c (n "openvx") (v "0.1.0") (d (list (d (n "openvx-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "1qgsvf606y2k74w170h713lvmm9gdppqr481n402rl3cyxjg8z38") (y #t)))

(define-public crate-openvx-0.1.1 (c (n "openvx") (v "0.1.1") (d (list (d (n "openvx-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "0zf6gp71zrzfr8gn7d2szmb9bkw6n7xlwjv6r0yk9mf96053lmwv")))

(define-public crate-openvx-0.1.2 (c (n "openvx") (v "0.1.2") (d (list (d (n "openvx-sys") (r "^0.1.2") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "0z0j6gfa4qa9d20cpsj5i4jb2gp7xplygk7njl0j7md96winklzq") (y #t)))

(define-public crate-openvx-0.1.3 (c (n "openvx") (v "0.1.3") (d (list (d (n "openvx-sys") (r "^0.1.3") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "1hz5gx2lkhwhr7fjb3yaigqlns8sfqwfc5375k4b8hw7fzplc6yy")))

