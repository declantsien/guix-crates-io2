(define-module (crates-io op en opengl32-sys) #:use-module (crates-io))

(define-public crate-opengl32-sys-0.0.1 (c (n "opengl32-sys") (v "0.0.1") (d (list (d (n "winapi") (r "*") (d #t) (k 0)))) (h "0z21h9rc6p8q8xxmkizdfdi811gbv1m6xr3z17w4s00hh3y5q0br")))

(define-public crate-opengl32-sys-0.0.2 (c (n "opengl32-sys") (v "0.0.2") (d (list (d (n "winapi") (r "*") (d #t) (k 0)))) (h "0xamxfx6qmimbn6bf2ak60kkpb8nhz2svap0vmnxy69nwna069q0")))

(define-public crate-opengl32-sys-0.1.0 (c (n "opengl32-sys") (v "0.1.0") (d (list (d (n "winapi") (r "*") (d #t) (k 0)))) (h "188m9d5b1hrr1x70d1k666icmifkpvaaj00kghn84pvj73fxbgfa")))

(define-public crate-opengl32-sys-0.1.1 (c (n "opengl32-sys") (v "0.1.1") (d (list (d (n "winapi") (r "^0.2") (d #t) (k 0)) (d (n "winapi-build") (r "^0.1.1") (d #t) (k 1)))) (h "1g1rnzn3yk9h9ffifmr6myf48bi207pm21yw6nci5wya6b1q8zgg")))

