(define-module (crates-io op en open-lyrics-cli) #:use-module (crates-io))

(define-public crate-open-lyrics-cli-0.1.0 (c (n "open-lyrics-cli") (v "0.1.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 0)) (d (n "rusqlite") (r "^0.21") (d #t) (k 0)))) (h "19375c4n7a1lzd88691dj3r42c0by454cqnxpsyrc29d0avsysi3")))

