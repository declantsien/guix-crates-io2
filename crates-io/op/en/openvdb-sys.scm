(define-module (crates-io op en openvdb-sys) #:use-module (crates-io))

(define-public crate-openvdb-sys-0.0.1 (c (n "openvdb-sys") (v "0.0.1") (d (list (d (n "bindgen") (r "^0.54.1") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "12cz4qjfkhk64x7y2prpznqw3sr8vmpm4h3hkr7r5brijs539vp6")))

