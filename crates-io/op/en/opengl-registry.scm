(define-module (crates-io op en opengl-registry) #:use-module (crates-io))

(define-public crate-opengl-registry-0.1.0 (c (n "opengl-registry") (v "0.1.0") (d (list (d (n "quick-xml") (r "^0.27.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)))) (h "1ic6hyz9b207axfrnffw21n4qffrmdi4j4pggif6dm0mry9k1sr0") (f (quote (("include-xml") ("default" "include-xml"))))))

(define-public crate-opengl-registry-0.1.1 (c (n "opengl-registry") (v "0.1.1") (d (list (d (n "quick-xml") (r "^0.27.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)))) (h "0wcm6gsis8xwfcyf1y7p8m81c5ydvcs2jink4spc2f72v2x9ngl3") (f (quote (("include-xml") ("default" "include-xml"))))))

(define-public crate-opengl-registry-0.2.0 (c (n "opengl-registry") (v "0.2.0") (d (list (d (n "quick-xml") (r "^0.27.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "ridicule") (r "^0.3.0") (d #t) (k 2)))) (h "1xp230inxiln1nkvack9mnf6756acrd8yp7cjs79dgaxwbl4y9ky") (f (quote (("include-xml") ("default" "include-xml"))))))

