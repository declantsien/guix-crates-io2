(define-module (crates-io op en openlimits-messaging) #:use-module (crates-io))

(define-public crate-openlimits-messaging-0.1.0 (c (n "openlimits-messaging") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.42") (d #t) (k 0)) (d (n "cross-test") (r "^0.1.6") (d #t) (k 2)) (d (n "futures") (r "^0.3.12") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.12") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.20") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 2)))) (h "0vjfb594scq53qw1flwprcvbhmydbcxrkh14r8xqjcvp2grryzzb")))

