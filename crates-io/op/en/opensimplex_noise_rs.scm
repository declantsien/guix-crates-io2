(define-module (crates-io op en opensimplex_noise_rs) #:use-module (crates-io))

(define-public crate-opensimplex_noise_rs-0.1.0 (c (n "opensimplex_noise_rs") (v "0.1.0") (h "0lmqsw8micb0askllw7zjc1r8rjybv4ji2v1s2x0rmsi7qdzbb4j")))

(define-public crate-opensimplex_noise_rs-0.2.0 (c (n "opensimplex_noise_rs") (v "0.2.0") (h "0kihv4lmx4zs6pqd950nnm30vnd2f0np0s1x2v8fyy5dvj4v1xqc")))

(define-public crate-opensimplex_noise_rs-0.2.1 (c (n "opensimplex_noise_rs") (v "0.2.1") (h "19dp0zayhldb2m178mpzavdk62cknjaapaj5q748c11biwg1c6fk")))

(define-public crate-opensimplex_noise_rs-0.3.0 (c (n "opensimplex_noise_rs") (v "0.3.0") (h "1dg84rslvrrjn0nmbjz9z8kw9zf4hf2hrsdgjxlbc6r9y92waxxs")))

