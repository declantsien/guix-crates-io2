(define-module (crates-io op en openblas-build-buildtest) #:use-module (crates-io))

(define-public crate-openblas-build-buildtest-0.10.8 (c (n "openblas-build-buildtest") (v "0.10.8") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "flate2") (r "^1.0.25") (d #t) (k 0)) (d (n "native-tls") (r "^0.2.11") (d #t) (k 0)) (d (n "tar") (r "^0.4.38") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)) (d (n "ureq") (r "^2.5.0") (f (quote ("native-certs" "native-tls" "gzip"))) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "1yw24gka06jc7vnzw75lz9plis8gwnds4ad9hcj933ch2digzib0") (y #t)))

