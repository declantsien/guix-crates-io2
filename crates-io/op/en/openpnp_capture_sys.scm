(define-module (crates-io op en openpnp_capture_sys) #:use-module (crates-io))

(define-public crate-openpnp_capture_sys-0.1.0 (c (n "openpnp_capture_sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.53") (d #t) (k 1)))) (h "0vkg7i4ax3ksjcc6vyww1j9chrjbq43vy0a64pr5bms8l9dsnhar")))

(define-public crate-openpnp_capture_sys-0.2.0 (c (n "openpnp_capture_sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.53") (d #t) (k 1)) (d (n "cc") (r "^1.0") (o #t) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (o #t) (d #t) (k 1)))) (h "1g3ai9vpsp6b8rgd8nglwgnc95yzf910dc3gb6yk56vy5ibgfmq5") (f (quote (("vendor" "cc" "cmake") ("native") ("default" "vendor"))))))

(define-public crate-openpnp_capture_sys-0.2.1 (c (n "openpnp_capture_sys") (v "0.2.1") (d (list (d (n "bindgen") (r "^0.53") (d #t) (k 1)) (d (n "cc") (r "^1.0") (o #t) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (o #t) (d #t) (k 1)))) (h "0lc70ydw56inar37vl8qzmlmxpqxy592p1xbf8zp4h1p6vwjggqz") (f (quote (("vendor" "cc" "cmake") ("native") ("default" "vendor")))) (y #t)))

(define-public crate-openpnp_capture_sys-0.2.2 (c (n "openpnp_capture_sys") (v "0.2.2") (d (list (d (n "bindgen") (r "^0.53") (d #t) (k 1)) (d (n "cc") (r "^1.0") (o #t) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (o #t) (d #t) (k 1)))) (h "1nj4vld0cdlaambj5p3p49ipakv5x77kjkfflib7mf3zl54ynp1h") (f (quote (("vendor" "cc" "cmake") ("native") ("default" "vendor"))))))

(define-public crate-openpnp_capture_sys-0.2.3 (c (n "openpnp_capture_sys") (v "0.2.3") (d (list (d (n "bindgen") (r "^0.53") (d #t) (k 1)) (d (n "cc") (r "^1.0") (o #t) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (o #t) (d #t) (k 1)))) (h "1v7kh4wvpw5cmj6ixasvy0934qi2snmyj0paa6gis6kiwbs3hj8n") (f (quote (("vendor" "cc" "cmake") ("native") ("default" "vendor"))))))

(define-public crate-openpnp_capture_sys-0.2.4 (c (n "openpnp_capture_sys") (v "0.2.4") (d (list (d (n "bindgen") (r "^0.53") (d #t) (k 1)) (d (n "cc") (r "^1.0") (o #t) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (o #t) (d #t) (k 1)))) (h "0w75wimfcpv8i1zf8ymrihg98yvasdrmb5k38iyl3cf30nlnqmqa") (f (quote (("vendor" "cc" "cmake") ("native") ("default" "vendor"))))))

(define-public crate-openpnp_capture_sys-0.2.5 (c (n "openpnp_capture_sys") (v "0.2.5") (d (list (d (n "bindgen") (r "^0.53") (d #t) (k 1)) (d (n "cc") (r "^1.0") (o #t) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (o #t) (d #t) (k 1)))) (h "1mgd0zryn9m2yjdpm7i9m33vr8l347j06djk91vcvicb4hans91g") (f (quote (("vendor" "cc" "cmake") ("native") ("default" "vendor"))))))

(define-public crate-openpnp_capture_sys-0.2.6 (c (n "openpnp_capture_sys") (v "0.2.6") (d (list (d (n "bindgen") (r "^0.53") (d #t) (k 1)) (d (n "cc") (r "^1.0") (o #t) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (o #t) (d #t) (k 1)))) (h "0cdx5x79f1snsqli8nv0pg3x8ynan2hz6q666ffc2cr0vhxlzq8m") (f (quote (("vendor" "cc" "cmake") ("native") ("default" "vendor"))))))

(define-public crate-openpnp_capture_sys-0.3.0 (c (n "openpnp_capture_sys") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.60") (d #t) (k 1)) (d (n "cc") (r "^1.0") (o #t) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (o #t) (d #t) (k 1)))) (h "022a10hf9a280f8bm806b3vlmgkpfr3f7wvzggb1ahy3swdylf9s") (f (quote (("vendor" "cc" "cmake") ("native") ("default" "vendor"))))))

(define-public crate-openpnp_capture_sys-0.4.0 (c (n "openpnp_capture_sys") (v "0.4.0") (d (list (d (n "bindgen") (r "^0.60") (d #t) (k 1)) (d (n "cc") (r "^1.0") (o #t) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (o #t) (d #t) (k 1)))) (h "01wm67blfh2pb6hr43n7ny5z1bm2h0w8l32v7xi3l59505a480r1") (f (quote (("vendor" "cc" "cmake") ("native") ("default" "vendor"))))))

