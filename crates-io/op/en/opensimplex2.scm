(define-module (crates-io op en opensimplex2) #:use-module (crates-io))

(define-public crate-opensimplex2-1.0.0 (c (n "opensimplex2") (v "1.0.0") (h "0a9d30i9vgpd2ks7i4ihggzvh8iajla9s3chprl66p7q2v1gak7j")))

(define-public crate-opensimplex2-1.0.1 (c (n "opensimplex2") (v "1.0.1") (h "0ss62mmv357hlw95283i0d1k6z01ycn5fd5vvi9aa47wpm463jwi")))

(define-public crate-opensimplex2-1.1.0 (c (n "opensimplex2") (v "1.1.0") (h "0wr59a40z6bb1vvx8sdh21w3fy2bcy8ry90sxmgzclnbyjnsndy1")))

