(define-module (crates-io op en opentalk-kustos-prefix) #:use-module (crates-io))

(define-public crate-opentalk-kustos-prefix-0.8.0-rc.1 (c (n "opentalk-kustos-prefix") (v "0.8.0-rc.1") (d (list (d (n "kustos-shared") (r "^0.8.0-rc.1") (d #t) (k 0)) (d (n "opentalk-kustos-prefix-impl") (r "^0.8.0-rc.1") (d #t) (k 0)))) (h "1l9csphxdyhd4zd6yj49x3f5dwa9395hxjkp7vzxzxz3j043m64j")))

(define-public crate-opentalk-kustos-prefix-0.9.0-alpha (c (n "opentalk-kustos-prefix") (v "0.9.0-alpha") (d (list (d (n "kustos-shared") (r "^0.9.0-alpha") (d #t) (k 0)) (d (n "opentalk-kustos-prefix-impl") (r "^0.9.0-alpha") (d #t) (k 0)))) (h "1gr5xh7r162lmcmd01g66rmchhrlya3s7k1yrv8pj3w33a8q2vly")))

(define-public crate-opentalk-kustos-prefix-0.9.0 (c (n "opentalk-kustos-prefix") (v "0.9.0") (d (list (d (n "kustos-shared") (r "^0.9.0") (d #t) (k 0)) (d (n "opentalk-kustos-prefix-impl") (r "^0.9.0") (d #t) (k 0)))) (h "0ymqraahc2cp4ll8l5v2i50a2q5lgdinmsfwv2ara03wvii6rlc4")))

(define-public crate-opentalk-kustos-prefix-0.10.0-rc.1 (c (n "opentalk-kustos-prefix") (v "0.10.0-rc.1") (d (list (d (n "kustos-shared") (r "^0.10.0-rc.1") (d #t) (k 0)) (d (n "opentalk-kustos-prefix-impl") (r "^0.10.0-rc.1") (d #t) (k 0)))) (h "0j7mg2kvaci7ds4l2h98p1pr7aksxqzp53im74qq24i7sr45ymxg")))

(define-public crate-opentalk-kustos-prefix-0.11.0-alpha (c (n "opentalk-kustos-prefix") (v "0.11.0-alpha") (d (list (d (n "kustos-shared") (r "^0.11.0-alpha") (d #t) (k 0)) (d (n "opentalk-kustos-prefix-impl") (r "^0.11.0-alpha") (d #t) (k 0)))) (h "0d3156q8j6v7nq4jrwa2i0564c0s46dsjmj9zpj06zxqrjgmjqlc")))

(define-public crate-opentalk-kustos-prefix-0.12.0-alpha (c (n "opentalk-kustos-prefix") (v "0.12.0-alpha") (d (list (d (n "kustos-shared") (r "^0.12.0-alpha") (d #t) (k 0)) (d (n "opentalk-kustos-prefix-impl") (r "^0.12.0-alpha") (d #t) (k 0)))) (h "10n7h5dirbc08dxkfzhi1pk8rsgkabq2i0bsv1c3fzl81q5hcbyq")))

(define-public crate-opentalk-kustos-prefix-0.12.0-alpha.1 (c (n "opentalk-kustos-prefix") (v "0.12.0-alpha.1") (d (list (d (n "kustos-shared") (r "^0.12.0-alpha.1") (d #t) (k 0)) (d (n "opentalk-kustos-prefix-impl") (r "^0.12.0-alpha.1") (d #t) (k 0)))) (h "0sxxsd0948sqh48vv972z0mijqqcl1iidkrsjhnsmrryj781fsmf")))

(define-public crate-opentalk-kustos-prefix-0.11.0-rc.2 (c (n "opentalk-kustos-prefix") (v "0.11.0-rc.2") (d (list (d (n "kustos-shared") (r "^0.11.0-rc.2") (d #t) (k 0)) (d (n "opentalk-kustos-prefix-impl") (r "^0.11.0-rc.2") (d #t) (k 0)))) (h "03pg9ds1hc6iycsj02qc2bwq20n5qm9n2yw93v3brivqnqqcq68y")))

(define-public crate-opentalk-kustos-prefix-0.10.0 (c (n "opentalk-kustos-prefix") (v "0.10.0") (d (list (d (n "kustos-shared") (r "^0.10.0") (d #t) (k 0)) (d (n "opentalk-kustos-prefix-impl") (r "^0.10.0") (d #t) (k 0)))) (h "1hw6yxkb0qrwp1x5sxb893b2394qamahh0zm45lkhbsv4l5il966")))

(define-public crate-opentalk-kustos-prefix-0.13.0-alpha (c (n "opentalk-kustos-prefix") (v "0.13.0-alpha") (d (list (d (n "kustos-shared") (r "^0.13.0-alpha") (d #t) (k 0)) (d (n "opentalk-kustos-prefix-impl") (r "^0.13.0-alpha") (d #t) (k 0)))) (h "0pcz0kax42rz5zk5lgpqsdc2vx2y1dw1d23b3m8kpwfi18qlm9kj")))

(define-public crate-opentalk-kustos-prefix-0.11.0 (c (n "opentalk-kustos-prefix") (v "0.11.0") (d (list (d (n "kustos-shared") (r "^0.11.0") (d #t) (k 0)) (d (n "opentalk-kustos-prefix-impl") (r "^0.11.0") (d #t) (k 0)))) (h "0jxpibf8gi3br7ychxm9wn8abydq49awqwvrdr60zr67kky8q3gy")))

