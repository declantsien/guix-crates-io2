(define-module (crates-io op en openssh-mux-client-error) #:use-module (crates-io))

(define-public crate-openssh-mux-client-error-0.1.0 (c (n "openssh-mux-client-error") (v "0.1.0") (d (list (d (n "ssh_format_error") (r "^0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.29") (d #t) (k 0)))) (h "0j76ibjlv6a0pjqj914hr1c8x3jq6s0c8viy0dba9lpljbjljp81")))

