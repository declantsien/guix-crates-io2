(define-module (crates-io op en open-cl-sys) #:use-module (crates-io))

(define-public crate-open-cl-sys-0.1.0 (c (n "open-cl-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.51.1") (d #t) (k 1)) (d (n "libc") (r "^0.2.65") (d #t) (k 0)))) (h "1rk3r1d0vwkrw1yks9ccp3958kdk8ss6vxcvbrskgqah3d3pqq04") (l "OpenCL")))

