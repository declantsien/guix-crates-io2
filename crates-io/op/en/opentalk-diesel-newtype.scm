(define-module (crates-io op en opentalk-diesel-newtype) #:use-module (crates-io))

(define-public crate-opentalk-diesel-newtype-0.0.0 (c (n "opentalk-diesel-newtype") (v "0.0.0") (h "114g0gysfnkkiziiyk5g5xcg5dxprkqspr7dichz4wn9kid4db6v")))

(define-public crate-opentalk-diesel-newtype-0.8.0-rc.1 (c (n "opentalk-diesel-newtype") (v "0.8.0-rc.1") (d (list (d (n "diesel") (r "^2") (f (quote ("postgres"))) (d #t) (k 0)) (d (n "opentalk-diesel-newtype-impl") (r "^0.8.0-rc.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "derive"))) (d #t) (k 0)))) (h "03aj3smn67m2krq493liq3w3sm5f61ql0c4br2xa8fbsik919v43")))

(define-public crate-opentalk-diesel-newtype-0.9.0-alpha (c (n "opentalk-diesel-newtype") (v "0.9.0-alpha") (d (list (d (n "diesel") (r "^2") (f (quote ("postgres"))) (d #t) (k 0)) (d (n "opentalk-diesel-newtype-impl") (r "^0.9.0-alpha") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "derive"))) (d #t) (k 0)))) (h "00vn5dsa5i10i7rxk26g22ccbg2ias6slsw6yy71vzrs0c72vllk")))

(define-public crate-opentalk-diesel-newtype-0.9.0 (c (n "opentalk-diesel-newtype") (v "0.9.0") (d (list (d (n "diesel") (r "^2") (f (quote ("postgres"))) (d #t) (k 0)) (d (n "opentalk-diesel-newtype-impl") (r "^0.9.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "derive"))) (d #t) (k 0)))) (h "08v77lxrvfpv0lbkwa3gzf63hih743m68xfm1psz1c7qf0838d60")))

(define-public crate-opentalk-diesel-newtype-0.10.0-rc.1 (c (n "opentalk-diesel-newtype") (v "0.10.0-rc.1") (d (list (d (n "diesel") (r "^2") (f (quote ("postgres"))) (d #t) (k 0)) (d (n "opentalk-diesel-newtype-impl") (r "^0.10.0-rc.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "derive"))) (d #t) (k 0)))) (h "16ww83dgs8wdgvzk6m1gqj33k1h1mh5y8jzf5nknkxgamfhcd8jm")))

(define-public crate-opentalk-diesel-newtype-0.11.0-alpha (c (n "opentalk-diesel-newtype") (v "0.11.0-alpha") (d (list (d (n "diesel") (r "^2") (f (quote ("postgres"))) (d #t) (k 0)) (d (n "opentalk-diesel-newtype-impl") (r "^0.11.0-alpha") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "derive"))) (d #t) (k 0)))) (h "00qj6b5n7nrh7z5glcdjamds9bsrb2zsa56xw8w9b7vi60fk2dxm")))

(define-public crate-opentalk-diesel-newtype-0.12.0-alpha (c (n "opentalk-diesel-newtype") (v "0.12.0-alpha") (d (list (d (n "diesel") (r "^2") (f (quote ("postgres"))) (d #t) (k 0)) (d (n "opentalk-diesel-newtype-impl") (r "^0.12.0-alpha") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "derive"))) (d #t) (k 0)))) (h "1y19g4mf884qg3chr4j1115sakllfkzfnbnr8g9w518ry80369w2")))

(define-public crate-opentalk-diesel-newtype-0.12.0-alpha.1 (c (n "opentalk-diesel-newtype") (v "0.12.0-alpha.1") (d (list (d (n "diesel") (r "^2") (f (quote ("postgres"))) (d #t) (k 0)) (d (n "opentalk-diesel-newtype-impl") (r "^0.12.0-alpha.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "derive"))) (d #t) (k 0)))) (h "0d9v2cr2p9v2csp5kppld1pjckcc08ac6xz3v2h2mmnqjw86hnc1")))

(define-public crate-opentalk-diesel-newtype-0.11.0-rc.2 (c (n "opentalk-diesel-newtype") (v "0.11.0-rc.2") (d (list (d (n "diesel") (r "^2") (f (quote ("postgres"))) (d #t) (k 0)) (d (n "opentalk-diesel-newtype-impl") (r "^0.11.0-rc.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "derive"))) (d #t) (k 0)))) (h "1sc6n2lain2gf8lmwqc3gh9pfjz3ggri95gz924msmg25bjrrjqp")))

(define-public crate-opentalk-diesel-newtype-0.10.0 (c (n "opentalk-diesel-newtype") (v "0.10.0") (d (list (d (n "diesel") (r "^2") (f (quote ("postgres"))) (d #t) (k 0)) (d (n "opentalk-diesel-newtype-impl") (r "^0.10.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "derive"))) (d #t) (k 0)))) (h "0rw4sj57spx64w90dankjn379pzb4zn83xfgk5w5rh42nkwq60wj")))

(define-public crate-opentalk-diesel-newtype-0.12.0 (c (n "opentalk-diesel-newtype") (v "0.12.0") (d (list (d (n "diesel") (r "^2") (f (quote ("postgres"))) (d #t) (k 0)) (d (n "opentalk-diesel-newtype-impl") (r "^0.12.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1jxcspq93ppch6288wv2nahz069fnd0qwnv6hkigqlrkfy3qrimy")))

(define-public crate-opentalk-diesel-newtype-0.11.0 (c (n "opentalk-diesel-newtype") (v "0.11.0") (d (list (d (n "diesel") (r "^2") (f (quote ("postgres"))) (d #t) (k 0)) (d (n "opentalk-diesel-newtype-impl") (r "^0.11.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "derive"))) (d #t) (k 0)))) (h "09wgvsnsxjjdqp7xcwxvlwrginsqn2v9fd0jkyl4kp3jc1m98vzk")))

