(define-module (crates-io op en openal) #:use-module (crates-io))

(define-public crate-openal-0.1.0 (c (n "openal") (v "0.1.0") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "openal-sys") (r "^1.16.0") (d #t) (k 0)))) (h "03zadr2k4fsq7rw7wx9rsgr6i0q8kq24s1mrfwn0q4rs6yw9va7k")))

(define-public crate-openal-0.1.1 (c (n "openal") (v "0.1.1") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "openal-sys") (r "^1.16.0") (d #t) (k 0)))) (h "0nphdcg8g63pr42c74wj44n1n3v0k585ma012xd98mfr9046l7mv")))

(define-public crate-openal-0.2.0 (c (n "openal") (v "0.2.0") (d (list (d (n "ffmpeg") (r "*") (d #t) (k 2)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "openal-sys") (r "^1.16.0") (d #t) (k 0)))) (h "183pgpg8i0alldayz1wr5f9b3v5pj50xqqi7n0zzvy41ark20y77")))

(define-public crate-openal-0.2.1 (c (n "openal") (v "0.2.1") (d (list (d (n "ffmpeg") (r "*") (d #t) (k 2)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "openal-sys") (r "*") (d #t) (k 0)))) (h "0kw7mq139cnlwag5c0wf0czysp20yhxwbbx4n4z5s4rpbb1dgn36") (f (quote (("static" "openal-sys/static"))))))

(define-public crate-openal-0.2.2 (c (n "openal") (v "0.2.2") (d (list (d (n "ffmpeg") (r "^0.2.0-alpha.1") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "openal-sys") (r "^1.16") (d #t) (k 0)))) (h "0j4b58llvr463rzzawkf6aid1s005nk4yn54z21k0cyz36vidf3n") (f (quote (("static" "openal-sys/static"))))))

