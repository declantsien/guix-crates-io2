(define-module (crates-io op en openpgp-card-state) #:use-module (crates-io))

(define-public crate-openpgp-card-state-0.0.1 (c (n "openpgp-card-state") (v "0.0.1") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "secret-service") (r "^3") (f (quote ("rt-tokio-crypto-rust"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full" "rt"))) (d #t) (k 0)))) (h "1aspv9f84ypd1icy9qgwhk8pq1y2fymp0rhcrygq70xpf98sa8f6")))

(define-public crate-openpgp-card-state-0.0.2 (c (n "openpgp-card-state") (v "0.0.2") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "keyring") (r "^2") (d #t) (k 0)))) (h "0whqhns5xpw7941f2dma0id4nw71wvi47rm4ji9blad0qw81mamf")))

(define-public crate-openpgp-card-state-0.0.3 (c (n "openpgp-card-state") (v "0.0.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "keyring") (r "^2") (d #t) (k 0)))) (h "1p5shb9hb1xrih42afamhck8fb6crnz39h3hh65s5s42zl0p9sdr")))

(define-public crate-openpgp-card-state-0.1.0 (c (n "openpgp-card-state") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "confy") (r "^0.6") (d #t) (k 0)) (d (n "keyring") (r "^2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "09rkmdhwy75w2picicq662nz0zmpr2ki8rqxdxvgn9nz1pjh7bz3")))

(define-public crate-openpgp-card-state-0.2.0 (c (n "openpgp-card-state") (v "0.2.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "confy") (r "^0.6") (d #t) (k 0)) (d (n "keyring") (r "^2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1ydda6zdfmc5avliw2jclpc5lcj9qd9rd6r68xd8zs4bgq118k6m")))

(define-public crate-openpgp-card-state-0.2.1 (c (n "openpgp-card-state") (v "0.2.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "confy") (r "^0.6") (d #t) (k 0)) (d (n "keyring") (r "^2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1jnj7mgdccgfk0s7796l0zxirg43igxyj2qf2akdi9mprxhiq5gf")))

