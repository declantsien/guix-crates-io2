(define-module (crates-io op en openzwave-sys) #:use-module (crates-io))

(define-public crate-openzwave-sys-0.1.0 (c (n "openzwave-sys") (v "0.1.0") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1al2hj7kw0m0p7gz9m61xa6xjansk8a21ajmwqs8jmp6mj34cls8")))

(define-public crate-openzwave-sys-0.1.1 (c (n "openzwave-sys") (v "0.1.1") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "12mvdp5nq9x1alh53i7jykqnzskd1jj9dvgxr061lr1gvrrfi1fp")))

