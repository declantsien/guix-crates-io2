(define-module (crates-io op en opentracing-rs-core) #:use-module (crates-io))

(define-public crate-opentracing-rs-core-0.1.0 (c (n "opentracing-rs-core") (v "0.1.0") (d (list (d (n "futures") (r "^0.1.25") (d #t) (k 0)) (d (n "tokio") (r "^0.1.13") (d #t) (k 0)))) (h "12hg3djhia5a0xvhspif8p09yrpkan06xwpdw796sip3kdli49js")))

