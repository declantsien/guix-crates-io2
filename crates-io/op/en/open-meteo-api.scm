(define-module (crates-io op en open-meteo-api) #:use-module (crates-io))

(define-public crate-open-meteo-api-0.1.0 (c (n "open-meteo-api") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11.18") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.104") (d #t) (k 0)) (d (n "tokio") (r "^1.30.0") (f (quote ("full"))) (d #t) (k 0)))) (h "15imh7wr0gxq6lsixlc0b9hyj0jvym6rd04pa2nl5gprxgx2y83l") (y #t)))

(define-public crate-open-meteo-api-0.1.1 (c (n "open-meteo-api") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.11.18") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.104") (d #t) (k 0)) (d (n "tokio") (r "^1.30.0") (f (quote ("full"))) (d #t) (k 0)))) (h "02cs9lwj3ih702d6rr3pizfn6c9slijldx8dg163v85xp01v1a9v") (y #t)))

(define-public crate-open-meteo-api-0.1.2 (c (n "open-meteo-api") (v "0.1.2") (d (list (d (n "reqwest") (r "^0.11.18") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.104") (d #t) (k 0)) (d (n "tokio") (r "^1.30.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0jf6nq0ciyh4nxw6gsv79v5hhx2dc9msizavwwmi533il92pl5rf") (y #t)))

(define-public crate-open-meteo-api-0.1.3 (c (n "open-meteo-api") (v "0.1.3") (d (list (d (n "reqwest") (r "^0.11.18") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.104") (d #t) (k 0)) (d (n "tokio") (r "^1.30.0") (f (quote ("full"))) (d #t) (k 0)))) (h "19ypfrsqr6hy27ga5vhv577khp87280qf0p0y311h9mpzsbbkwz9")))

