(define-module (crates-io op en openjpeg-sys) #:use-module (crates-io))

(define-public crate-openjpeg-sys-1.0.0 (c (n "openjpeg-sys") (v "1.0.0") (d (list (d (n "cc") (r "^1.0.37") (d #t) (k 1)) (d (n "libc") (r "^0.2.54") (d #t) (k 0)))) (h "08pljzdiajdxnqzri4cbhzphvam2ra8yhn2kbdwhv5699ckgcyjk") (f (quote (("parallel" "cc/parallel")))) (y #t) (l "openjpeg")))

(define-public crate-openjpeg-sys-1.0.1 (c (n "openjpeg-sys") (v "1.0.1") (d (list (d (n "cc") (r "^1.0.37") (d #t) (k 1)) (d (n "libc") (r "^0.2.54") (d #t) (k 0)))) (h "1a3xynr9ky2h33zigiiqnciz1gpssif98xr0w1nxy7qhd0rd2rf9") (f (quote (("parallel" "cc/parallel")))) (y #t) (l "openjpeg")))

(define-public crate-openjpeg-sys-1.0.2 (c (n "openjpeg-sys") (v "1.0.2") (d (list (d (n "cc") (r "^1.0.67") (d #t) (k 1)) (d (n "libc") (r "^0.2.94") (d #t) (k 0)))) (h "081kjl4fp2sgryh92yhjihs0cyp08b62kx1smjf4bgmgbqrkciwk") (f (quote (("parallel" "cc/parallel")))) (y #t) (l "openjpeg")))

(define-public crate-openjpeg-sys-1.0.3 (c (n "openjpeg-sys") (v "1.0.3") (d (list (d (n "cc") (r "^1.0.67") (d #t) (k 1)) (d (n "libc") (r "^0.2.94") (d #t) (k 0)))) (h "134swbfd4xskjiyikhg4wh3vgzh8xwizfvxv650kq7dnvpbh347k") (f (quote (("parallel" "cc/parallel")))) (y #t) (l "openjpeg")))

(define-public crate-openjpeg-sys-1.0.4 (c (n "openjpeg-sys") (v "1.0.4") (d (list (d (n "cc") (r "^1.0.67") (d #t) (k 1)) (d (n "libc") (r "^0.2.94") (d #t) (k 0)))) (h "1jkd3xmmqf7hg4y5jh6rhiqcr5ghpyl5wj8idk5hxmk8fgy5n2x8") (f (quote (("parallel" "cc/parallel")))) (y #t) (l "openjpeg")))

(define-public crate-openjpeg-sys-1.0.5 (c (n "openjpeg-sys") (v "1.0.5") (d (list (d (n "cc") (r "^1.0.67") (d #t) (k 1)) (d (n "libc") (r "^0.2.94") (d #t) (k 0)))) (h "13wl42xc9a4mi7p1vpqzi4wf2zhp3jhwc85k74sg2x70yj6jc287") (f (quote (("parallel" "cc/parallel")))) (l "openjpeg")))

(define-public crate-openjpeg-sys-1.0.6 (c (n "openjpeg-sys") (v "1.0.6") (d (list (d (n "cc") (r "^1.0.67") (d #t) (k 1)) (d (n "libc") (r "^0.2.94") (d #t) (k 0)))) (h "0ih6w7sc9yik9ljwy7sfdr8brakmhbik3x7hfqwk1hz5q53xhp98") (f (quote (("threads") ("parallel" "cc/parallel") ("default" "threads")))) (l "openjpeg")))

(define-public crate-openjpeg-sys-1.0.7 (c (n "openjpeg-sys") (v "1.0.7") (d (list (d (n "cc") (r "^1.0.67") (d #t) (k 1)) (d (n "libc") (r "^0.2.94") (d #t) (k 0)))) (h "14gi9i3s9j0bv856amkbpyyr1rspli9makiiq01635p7j5k19sai") (f (quote (("threads") ("parallel" "cc/parallel") ("default" "threads")))) (l "openjpeg")))

(define-public crate-openjpeg-sys-1.0.8 (c (n "openjpeg-sys") (v "1.0.8") (d (list (d (n "cc") (r "^1.0.67") (d #t) (k 1)) (d (n "libc") (r "^0.2.94") (d #t) (k 0)))) (h "0p14q7ngqa2sj21wvjsjqc3yaszsnrw5ws5l5kmay3h7v498vrxm") (f (quote (("threads") ("parallel" "cc/parallel") ("default" "threads")))) (l "openjpeg")))

(define-public crate-openjpeg-sys-1.0.9 (c (n "openjpeg-sys") (v "1.0.9") (d (list (d (n "cc") (r "^1.0.67") (d #t) (k 1)) (d (n "libc") (r "^0.2.94") (d #t) (k 0)))) (h "1vqd0b7kmv2dn7mrrjbfh7pizfbcxqwpw0i0vb2jrjnfbcwva6g6") (f (quote (("threads") ("parallel" "cc/parallel") ("default" "threads")))) (l "openjpeg")))

(define-public crate-openjpeg-sys-1.0.10 (c (n "openjpeg-sys") (v "1.0.10") (d (list (d (n "cc") (r "^1.0.67") (d #t) (k 1)) (d (n "libc") (r "^0.2.94") (d #t) (k 0)))) (h "1yhkna25z7pb6iyikpmd07hl03xzlzxb5x93visahn7g1j2nsdhw") (f (quote (("threads") ("parallel" "cc/parallel") ("default" "threads")))) (l "openjpeg")))

