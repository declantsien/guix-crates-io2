(define-module (crates-io op en openslide-sys) #:use-module (crates-io))

(define-public crate-openslide-sys-0.1.0 (c (n "openslide-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.61") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0g658wfs7zjcql53dj2vv0hr734drripzr6isp1vij531xiwd9b5") (f (quote (("default"))))))

(define-public crate-openslide-sys-0.1.1 (c (n "openslide-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.62") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1k5cs6hqqisdbwfvsakgz2s1ylz9j6l7fznypw0sy2cw7sf6lzw5") (f (quote (("default"))))))

(define-public crate-openslide-sys-0.1.2 (c (n "openslide-sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.63") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0yfhlybnj29hhljxigkbgdz35zbd85vf4zkvlnyw335i0sy1dhnv") (f (quote (("dynamic-link") ("default"))))))

(define-public crate-openslide-sys-0.1.3 (c (n "openslide-sys") (v "0.1.3") (d (list (d (n "bindgen") (r "^0.63") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1b07iabh63yq4niz46wkp17g3d8als35vqagd94215c7f918ch46") (f (quote (("dynamic-link") ("default"))))))

(define-public crate-openslide-sys-0.1.4 (c (n "openslide-sys") (v "0.1.4") (d (list (d (n "bindgen") (r "^0.63") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0c33xbw1h35jsks24ab1a6pn2fg1bc2xhb8m68rxc6cxw2xkhm1r") (f (quote (("dynamic-link") ("default"))))))

(define-public crate-openslide-sys-1.0.0 (c (n "openslide-sys") (v "1.0.0") (d (list (d (n "bindgen") (r "^0.65") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "02p9vnpa5dw0d1kz8a8w2j06sj5iqs62fg1ys5dv57ryqg987idw") (f (quote (("dynamic-link") ("default"))))))

(define-public crate-openslide-sys-1.0.1 (c (n "openslide-sys") (v "1.0.1") (d (list (d (n "bindgen") (r "^0.68") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1hnl7vy1vnahwx9q770xzx3hfqr2qdr8m8s0av6gs0g0m6x9z9hq") (f (quote (("dynamic-link") ("default"))))))

(define-public crate-openslide-sys-1.0.3 (c (n "openslide-sys") (v "1.0.3") (d (list (d (n "bindgen") (r "^0.69") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "17k9aig531j57g7v2j7fi2dlh9yxgvbj1cj1bqf8w7qclq6wjspx") (f (quote (("dynamic-link") ("default"))))))

(define-public crate-openslide-sys-1.0.4 (c (n "openslide-sys") (v "1.0.4") (d (list (d (n "bindgen") (r "^0.69") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1509s517wiskpfqsxsvr2xmi4585blsj2jgyfcf5yvqvzaszy87k") (f (quote (("dynamic-link") ("default"))))))

(define-public crate-openslide-sys-1.0.5 (c (n "openslide-sys") (v "1.0.5") (d (list (d (n "bindgen") (r "^0.69") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1gj50hh42zk628pnxfkzrx7lky01zjx32h1v193mzf3ajqmqvy00") (f (quote (("dynamic-link") ("default"))))))

