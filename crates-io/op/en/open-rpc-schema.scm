(define-module (crates-io op en open-rpc-schema) #:use-module (crates-io))

(define-public crate-open-rpc-schema-0.0.1 (c (n "open-rpc-schema") (v "0.0.1") (d (list (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "schemars_derive") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)))) (h "1v8rhlk49szc9nspdnwrfigcb9l070hchixvq15g1666q0fxwvzl")))

(define-public crate-open-rpc-schema-0.0.2 (c (n "open-rpc-schema") (v "0.0.2") (d (list (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "schemars_derive") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)))) (h "1j6367k10vk3pp6ixdjb3jxc4k4xcryi2kcxsglfajxgc2x9xdyf")))

(define-public crate-open-rpc-schema-0.0.3 (c (n "open-rpc-schema") (v "0.0.3") (d (list (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "schemars_derive") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)))) (h "16vhf5nh84vssg6wmxqpknpb8i7j743n0zqdclc283nq8ahi7r2b")))

(define-public crate-open-rpc-schema-0.0.4 (c (n "open-rpc-schema") (v "0.0.4") (d (list (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "schemars_derive") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)))) (h "087nmc21grf3kkgf2x9f3b0q021wfv705xv6ybcbhrl1lxa9d4g6")))

