(define-module (crates-io op en open-meteo-cli) #:use-module (crates-io))

(define-public crate-open-meteo-cli-0.1.0 (c (n "open-meteo-cli") (v "0.1.0") (d (list (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)) (d (n "ureq") (r "^2.6.2") (f (quote ("json"))) (d #t) (k 0)))) (h "00gkwgjvwyf3rj9b6zihja7n96yxgh2a6jyaaz6pdk1bi05adpdv")))

(define-public crate-open-meteo-cli-0.1.1 (c (n "open-meteo-cli") (v "0.1.1") (d (list (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)) (d (n "ureq") (r "^2.6.2") (f (quote ("json"))) (d #t) (k 0)))) (h "1rxayxqichzmgdnzyyj9n47zg0pbb6959s6nvhhfascz4myqw5zs")))

(define-public crate-open-meteo-cli-0.1.2 (c (n "open-meteo-cli") (v "0.1.2") (d (list (d (n "billboard") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)) (d (n "ureq") (r "^2.6.2") (f (quote ("json"))) (d #t) (k 0)))) (h "0aldd2hla79pyrs3s8ybh9yhw21n57fm3jq0j5gk7a0j1y5fihli")))

(define-public crate-open-meteo-cli-0.1.3 (c (n "open-meteo-cli") (v "0.1.3") (d (list (d (n "billboard") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)) (d (n "ureq") (r "^2.6.2") (f (quote ("json"))) (d #t) (k 0)))) (h "1h0fjrnrq2lg22vwzx70p7dlxlx5w8mp1bznsz9gija7rlz133sk")))

(define-public crate-open-meteo-cli-0.1.4 (c (n "open-meteo-cli") (v "0.1.4") (d (list (d (n "billboard") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)) (d (n "ureq") (r "^2.6.2") (f (quote ("json"))) (d #t) (k 0)))) (h "0s0df08rgsv7znrgfgwh1vh6ikpxz2npz20rdgl2906yxp4kkbd8")))

(define-public crate-open-meteo-cli-0.1.5 (c (n "open-meteo-cli") (v "0.1.5") (d (list (d (n "billboard") (r "^0.2") (d #t) (k 0)) (d (n "directories") (r "^5.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)) (d (n "ureq") (r "^2.6.2") (f (quote ("json"))) (d #t) (k 0)))) (h "1y040r10akhm682m2nndncv9ppl9mxd7bnw0lqrhpwdcd0gwjsql")))

(define-public crate-open-meteo-cli-0.1.6 (c (n "open-meteo-cli") (v "0.1.6") (d (list (d (n "billboard") (r "^0.2") (d #t) (k 0)) (d (n "directories") (r "^5.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)) (d (n "ureq") (r "^2.9.6") (f (quote ("json"))) (d #t) (k 0)))) (h "1s5sj99jk5zryjii5vd6wnkb9c0rkah9vw3xzzd85nvr9c8drfy5")))

