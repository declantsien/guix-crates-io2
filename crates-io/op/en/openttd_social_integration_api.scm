(define-module (crates-io op en openttd_social_integration_api) #:use-module (crates-io))

(define-public crate-openttd_social_integration_api-0.1.0 (c (n "openttd_social_integration_api") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)))) (h "1msls455v4h676vmbqzpqjhw4mkbrcs0h9xigm7ns88swjcvkvql")))

(define-public crate-openttd_social_integration_api-0.1.1 (c (n "openttd_social_integration_api") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)) (d (n "openttd_social_integration_api_macros") (r "^0.1.0") (d #t) (k 0)))) (h "0i7v52was0w7h3yzi5i3binjjww2mkmb6ggrl0g02giqxb975sx8")))

(define-public crate-openttd_social_integration_api-0.1.2 (c (n "openttd_social_integration_api") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)) (d (n "openttd_social_integration_api_macros") (r "^0.1.0") (d #t) (k 0)))) (h "02y6knzk0vivzy662kgi86f5cfa0m6r611ybrpsw05bcr619l6nv") (y #t)))

(define-public crate-openttd_social_integration_api-0.1.3 (c (n "openttd_social_integration_api") (v "0.1.3") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)) (d (n "openttd_social_integration_api_macros") (r "^0.1.0") (d #t) (k 0)))) (h "0k5d0k70v24wyxa11yih0c56sjqmqna3m52jazi4hdl1q3hjblpk")))

