(define-module (crates-io op en open-coroutine-timer) #:use-module (crates-io))

(define-public crate-open-coroutine-timer-0.0.4 (c (n "open-coroutine-timer") (v "0.0.4") (h "02085m2ir97w0maq9q1x5w32hcnv0amq67zw3r8wi5mcq1s1claz")))

(define-public crate-open-coroutine-timer-0.4.0 (c (n "open-coroutine-timer") (v "0.4.0") (h "0ranb2lk45fmf4n4q0r1b97g8dwlsyf3l2b82d23f1hwdg63j3y0")))

(define-public crate-open-coroutine-timer-0.1.0 (c (n "open-coroutine-timer") (v "0.1.0") (h "0bz09is28frpzfjvdx2xvabzjg358l9xxk8bzl3znqnp7rqv4y20")))

(define-public crate-open-coroutine-timer-0.5.0 (c (n "open-coroutine-timer") (v "0.5.0") (h "1mz1jrbrvdj5pvi5a9w1kiszyv4gaf3pmz698missbj7a8pnxb16")))

