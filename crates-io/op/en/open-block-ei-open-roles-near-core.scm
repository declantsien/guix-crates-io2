(define-module (crates-io op en open-block-ei-open-roles-near-core) #:use-module (crates-io))

(define-public crate-open-block-ei-open-roles-near-core-0.1.0 (c (n "open-block-ei-open-roles-near-core") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "near-contract-standards") (r "^3.1.0") (d #t) (k 0)) (d (n "near-sdk") (r "^3.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.57") (d #t) (k 0)))) (h "0z18f04rz1lj07k57sc50xvjwawm828yib2sxdfp4rjykk1vhllj")))

