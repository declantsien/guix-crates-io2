(define-module (crates-io op en openvpn-management) #:use-module (crates-io))

(define-public crate-openvpn-management-0.1.0 (c (n "openvpn-management") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)))) (h "1pdpcx4d56fghb9rbqf09dmcif26wbinnzmiyj4y2h42ypjx3s5h")))

(define-public crate-openvpn-management-0.1.1 (c (n "openvpn-management") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)))) (h "1qhph2q7phirfhkx54a5fnglnjpwqcx991d8h8vkm36ym3rrkafz")))

(define-public crate-openvpn-management-0.2.0 (c (n "openvpn-management") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)))) (h "08kc2nm5f9knj24sdyhp0k9fzixsjav7cdfvz26fj7rm4gfcv28f")))

(define-public crate-openvpn-management-0.2.1 (c (n "openvpn-management") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)))) (h "0937wx9yw0mf3gccr6kvwp7admn2rz8y19prm50v3ak1fqdv4h5x")))

(define-public crate-openvpn-management-0.2.2 (c (n "openvpn-management") (v "0.2.2") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)))) (h "1sspj2zc36n2g3w8wdqg9qdhigkfagcla9ccysgxniwbg6b0mc6i")))

(define-public crate-openvpn-management-0.3.0 (c (n "openvpn-management") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)))) (h "0zyg3wxz3b17s3m26n6mw4c9xgjli7pxlil5hyrpi5w3xv9ag3if")))

