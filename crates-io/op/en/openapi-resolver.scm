(define-module (crates-io op en openapi-resolver) #:use-module (crates-io))

(define-public crate-openapi-resolver-0.1.0 (c (n "openapi-resolver") (v "0.1.0") (d (list (d (n "insta") (r "^1.6") (f (quote ("ron" "redactions"))) (d #t) (k 2)) (d (n "openapiv3") (r "^0.3.2") (d #t) (k 0)))) (h "1zgkfcqrk1vxfqlvwxq2w7vb9k4hbp1kzdigkd7zhncxgxzwiz80")))

