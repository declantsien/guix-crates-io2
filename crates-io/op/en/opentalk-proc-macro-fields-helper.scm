(define-module (crates-io op en opentalk-proc-macro-fields-helper) #:use-module (crates-io))

(define-public crate-opentalk-proc-macro-fields-helper-0.8.0-rc.1 (c (n "opentalk-proc-macro-fields-helper") (v "0.8.0-rc.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0n0rxgvarmdyb7ndhkqn8jswj236n1g6pxdb0814d81fffdhbl5r")))

(define-public crate-opentalk-proc-macro-fields-helper-0.9.0-alpha (c (n "opentalk-proc-macro-fields-helper") (v "0.9.0-alpha") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0wa9cz6s9myc20yij6i1z05k2f51924i0k0j60jx4w8j73kpfyab")))

(define-public crate-opentalk-proc-macro-fields-helper-0.9.0 (c (n "opentalk-proc-macro-fields-helper") (v "0.9.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0jancv89apq31lq7ih8i0dsnb3my3nkagdp0sbhvwfbvkcwg2y0y")))

(define-public crate-opentalk-proc-macro-fields-helper-0.10.0-rc.1 (c (n "opentalk-proc-macro-fields-helper") (v "0.10.0-rc.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "158mxqxma0k0rllbyvh5sbmfh0hs9pc0wqdz5vs70ivm687pyc58")))

(define-public crate-opentalk-proc-macro-fields-helper-0.11.0-alpha (c (n "opentalk-proc-macro-fields-helper") (v "0.11.0-alpha") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0gdslp0qmgsd8w1m5d8lxrnqkprq36zqvwh8r5wzf74zgk0ylpd0")))

(define-public crate-opentalk-proc-macro-fields-helper-0.12.0-alpha (c (n "opentalk-proc-macro-fields-helper") (v "0.12.0-alpha") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "13c0i0azpw426vswmvaz9289l0hv0lwwmf1vxvavf9rk0svrfz21")))

(define-public crate-opentalk-proc-macro-fields-helper-0.12.0-alpha.1 (c (n "opentalk-proc-macro-fields-helper") (v "0.12.0-alpha.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1wpylnnfim0nnjjc2jhgk2yxhhzim779443w9x6mr6kbrna9njh9")))

(define-public crate-opentalk-proc-macro-fields-helper-0.11.0-rc.2 (c (n "opentalk-proc-macro-fields-helper") (v "0.11.0-rc.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "19rmfppih06gkcl9piy4bc01a1aghlvw2pzvr4iir5j9is4hgrck")))

(define-public crate-opentalk-proc-macro-fields-helper-0.10.0 (c (n "opentalk-proc-macro-fields-helper") (v "0.10.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1x7q9bbf3z16rijny9c2xqmxg883rmv6s3qfdk2g6lwy88v2sg71")))

(define-public crate-opentalk-proc-macro-fields-helper-0.13.0-alpha (c (n "opentalk-proc-macro-fields-helper") (v "0.13.0-alpha") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "02glypj362czl8d1b4pqvzcdw8bn1qbsppzjr6ipjc5iqah2hkdg")))

(define-public crate-opentalk-proc-macro-fields-helper-0.11.0 (c (n "opentalk-proc-macro-fields-helper") (v "0.11.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1a9v0i44acdf8p3bznknjfwxrbf5i109irn5xl2gxczdgyibb0l6")))

