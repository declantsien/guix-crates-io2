(define-module (crates-io op en open62541-ffi) #:use-module (crates-io))

(define-public crate-open62541-ffi-1.4.0-rc2 (c (n "open62541-ffi") (v "1.4.0-rc2") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)) (d (n "cmake") (r "^0.1.50") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1wkq6h76ks5g7jyc1gki5yiak85mzska625xjr4wmj3wcn9b6cb7")))

