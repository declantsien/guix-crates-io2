(define-module (crates-io op en openssl-rehash) #:use-module (crates-io))

(define-public crate-openssl-rehash-0.1.0 (c (n "openssl-rehash") (v "0.1.0") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "openssl") (r "^0.10.48") (f (quote ("vendored"))) (d #t) (k 0)) (d (n "regex") (r "^1.7.3") (d #t) (k 0)) (d (n "insta") (r "^1.29.0") (d #t) (k 2)) (d (n "tempfile") (r "^3.4.0") (d #t) (k 2)))) (h "11bx4pslbl0yq4d2l8pbkl6hwz141w8mdhrqzkb8c22lzyydpjy1")))

(define-public crate-openssl-rehash-0.1.1 (c (n "openssl-rehash") (v "0.1.1") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "openssl") (r "^0.10.48") (f (quote ("vendored"))) (d #t) (k 0)) (d (n "regex") (r "^1.7.3") (d #t) (k 0)) (d (n "insta") (r "^1.29.0") (d #t) (k 2)) (d (n "tempfile") (r "^3.4.0") (d #t) (k 2)))) (h "1a2wlvprnd367ssbw3spqw836z0h1ihfdiyy8djk8d6m5714hsg8")))

