(define-module (crates-io op en openaq-client) #:use-module (crates-io))

(define-public crate-openaq-client-0.0.1 (c (n "openaq-client") (v "0.0.1") (d (list (d (n "hyper") (r "^0.10.10") (d #t) (k 0)) (d (n "hyper-native-tls") (r "^0.2.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde-transcode") (r "^1.0.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.4") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.2") (d #t) (k 0)))) (h "0jabr5dxar2h484x6lhs23vmd26ranffy8s3zyci6k3bawkqa35p")))

(define-public crate-openaq-client-0.0.2 (c (n "openaq-client") (v "0.0.2") (d (list (d (n "hyper") (r "^0.10.10") (d #t) (k 0)) (d (n "hyper-native-tls") (r "^0.2.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde-transcode") (r "^1.0.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.4") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.2") (d #t) (k 0)))) (h "1agahn3w6xgljhgb9nfmz28bdcjlikwnyvc0ah0xq0h88dhalg54")))

(define-public crate-openaq-client-0.0.3 (c (n "openaq-client") (v "0.0.3") (d (list (d (n "hyper") (r "^0.10.10") (d #t) (k 0)) (d (n "hyper-native-tls") (r "^0.2.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde-transcode") (r "^1.0.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.4") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.2") (d #t) (k 0)))) (h "1l2a0zf3675yvpfxg7bp74r0d4767dfari9kfj2w34wzcy6fsaqv")))

(define-public crate-openaq-client-0.0.4 (c (n "openaq-client") (v "0.0.4") (d (list (d (n "hyper") (r "^0.10.10") (d #t) (k 0)) (d (n "hyper-native-tls") (r "^0.2.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde-transcode") (r "^1.0.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.4") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.2") (d #t) (k 0)))) (h "0is559wg3h14b207zda5dlk32147l5hm7dxz0j6ah7vb07d3zfkl")))

(define-public crate-openaq-client-0.0.5 (c (n "openaq-client") (v "0.0.5") (d (list (d (n "hyper") (r "^0.10.10") (d #t) (k 0)) (d (n "hyper-native-tls") (r "^0.2.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde-transcode") (r "^1.0.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.4") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.2") (d #t) (k 0)))) (h "07crilq7vi8p559lrkxk1pqh3zyccv8ibs5byahcnkx47dvnsh0q")))

