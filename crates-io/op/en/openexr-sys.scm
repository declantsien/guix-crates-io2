(define-module (crates-io op en openexr-sys) #:use-module (crates-io))

(define-public crate-openexr-sys-0.1.0 (c (n "openexr-sys") (v "0.1.0") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0j7l6zijv4i4s6rz9h64nxsmhxmix3c9s794cd533i6dqy1qj3f7")))

(define-public crate-openexr-sys-0.2.0 (c (n "openexr-sys") (v "0.2.0") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0gzy1i1lrj5f6kjavqgbmbqwlj1zp14q48wh76av8y5ywmvgnlz6")))

(define-public crate-openexr-sys-0.3.0 (c (n "openexr-sys") (v "0.3.0") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0ahfkl056lzvkjkk8knhr5rpxvgpywchacg8kr020qq86wc8vis8")))

(define-public crate-openexr-sys-0.3.1 (c (n "openexr-sys") (v "0.3.1") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0xk6pf9nnf6szmpv8xy1bjv8l0nbvzcz6qppd9m37q2zx9g627qv")))

(define-public crate-openexr-sys-0.4.0 (c (n "openexr-sys") (v "0.4.0") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1qbaqzcljc2ccw768anlrjv21dqnn8nypv4n7s5jplc92w84vhzl")))

(define-public crate-openexr-sys-0.5.0 (c (n "openexr-sys") (v "0.5.0") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1i5ww17j4750fjx7xvawbnjnzyg6nip7rfabws60kna9f06775ym")))

(define-public crate-openexr-sys-0.6.0 (c (n "openexr-sys") (v "0.6.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1gbmvgpcl9i88f6sdba1bccyc7ja7dx581paks8ilhakk528p9ln")))

(define-public crate-openexr-sys-0.7.0 (c (n "openexr-sys") (v "0.7.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0dn8m5v6n19g99j55hbvy9fx3b5pjxqslzi2zyr6grlki5h36v0l")))

(define-public crate-openexr-sys-0.7.1 (c (n "openexr-sys") (v "0.7.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0lar47z3wncyfn4mak4g7bjwcxag3857yn1aqjh1265qxj7jiyr9")))

(define-public crate-openexr-sys-0.10.0 (c (n "openexr-sys") (v "0.10.0") (d (list (d (n "cppmm-build") (r "^0.2") (d #t) (k 1)))) (h "0fjby9mw8hhv7qwz2k97b3nhhx8nrbgld8gdi2pp7laaw828vn09")))

(define-public crate-openexr-sys-0.10.1 (c (n "openexr-sys") (v "0.10.1") (d (list (d (n "cppmm-build") (r "^0.2") (d #t) (k 1)))) (h "00y0f8zpq2nrdl1x7b8absnc6s4b7zm188pbmgdv1607lvq1ic3i")))

