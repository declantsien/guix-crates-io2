(define-module (crates-io op en openai2) #:use-module (crates-io))

(define-public crate-openai2-1.0.0 (c (n "openai2") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "httpclient") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "tokio") (r "^1.18.2") (f (quote ("full"))) (d #t) (k 2)))) (h "14c35xrcypfwzxanynjd7wgqqwm38d7622m1a44zn0wvspr3b24s")))

(define-public crate-openai2-2.0.0 (c (n "openai2") (v "2.0.0") (d (list (d (n "futures") (r "^0.3.25") (d #t) (k 0)) (d (n "httpclient") (r "^0.15.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "tokio") (r "^1.18.2") (f (quote ("full"))) (d #t) (k 2)))) (h "0jpcdlkaywhxhydcbcdqqw36fvkr6kf3k36lb3ksmmgj35r0sirk")))

