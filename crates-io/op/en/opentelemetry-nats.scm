(define-module (crates-io op en opentelemetry-nats) #:use-module (crates-io))

(define-public crate-opentelemetry-nats-0.1.0 (c (n "opentelemetry-nats") (v "0.1.0") (d (list (d (n "async-nats") (r "^0.31") (k 0)) (d (n "opentelemetry") (r "^0.20") (f (quote ("rt-tokio"))) (k 0)) (d (n "tracing") (r "^0.1") (f (quote ("log"))) (k 0)) (d (n "tracing-opentelemetry") (r "^0.20") (k 0)))) (h "05r3sinyf2jdgyxapskq7bw5ac3lg7nn37szbpwc1imk6bxk3qbz") (f (quote (("default"))))))

