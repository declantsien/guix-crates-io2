(define-module (crates-io op en openzwave) #:use-module (crates-io))

(define-public crate-openzwave-0.1.1 (c (n "openzwave") (v "0.1.1") (d (list (d (n "itertools") (r "^0.4.11") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "openzwave-sys") (r "^0.1.1") (d #t) (k 0)))) (h "0ypplr71w0chv3kf00wzfc6s5bbyicpjy2y0gmysih04f7ivalil")))

