(define-module (crates-io op en openai-macros) #:use-module (crates-io))

(define-public crate-openai-macros-0.1.0 (c (n "openai-macros") (v "0.1.0") (d (list (d (n "openai-utils") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.68") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full"))) (d #t) (k 0)))) (h "10gbp3dgvdrwfchzgkqzpcrnx98b687c1p97yaj6cbj8mfz8v30i")))

(define-public crate-openai-macros-0.1.1 (c (n "openai-macros") (v "0.1.1") (d (list (d (n "openai-utils") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.68") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full"))) (d #t) (k 0)))) (h "0fhn2np50m7731irinid6dskxzkmll56mgmjq7fcnrxj0l2i70z2")))

(define-public crate-openai-macros-0.2.0 (c (n "openai-macros") (v "0.2.0") (d (list (d (n "openai-utils") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.68") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full"))) (d #t) (k 0)))) (h "0xa379znyh9jw2474iay5mqppjszhw2c4xib3xa6fbmay0q61jv5")))

