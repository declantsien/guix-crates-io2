(define-module (crates-io op en openthread-sys) #:use-module (crates-io))

(define-public crate-openthread-sys-0.1.0 (c (n "openthread-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.53.1") (d #t) (k 1)))) (h "0lz0h02jn67sgamyfwzc2vypllalqkgb70rfmra93qklcz6sl21b") (l "openthread")))

(define-public crate-openthread-sys-0.1.1 (c (n "openthread-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.53.1") (d #t) (k 1)))) (h "1js83jcnlz88akzhvrnjknpja6nlqbmvmgpq3j6i7i3b508iy234") (f (quote (("mtd") ("go-faster") ("ftd") ("default" "cli" "ftd") ("cli")))) (l "openthread")))

(define-public crate-openthread-sys-0.1.2 (c (n "openthread-sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.53.1") (d #t) (k 1)))) (h "0v5k5zwsq9h658kbwyf4m13va9x6bcriz6fxdj9q2si8p872gx71") (f (quote (("mtd") ("go-faster") ("ftd") ("default" "cli" "ftd") ("cli")))) (l "openthread")))

(define-public crate-openthread-sys-0.1.3 (c (n "openthread-sys") (v "0.1.3") (d (list (d (n "bindgen") (r "^0.53.1") (d #t) (k 1)))) (h "07ydrkm37wf0qkmd0x1drnvlj871iypd970fg56qm7slyy1p467g") (f (quote (("mtd") ("go-faster") ("ftd") ("default" "cli" "ftd") ("cli")))) (l "openthread")))

(define-public crate-openthread-sys-0.1.4 (c (n "openthread-sys") (v "0.1.4") (d (list (d (n "bindgen") (r "^0.53.1") (d #t) (k 1)))) (h "0g5jvzzaz7c40fay4368anp1hvbfslcp3z9j0dql8h15alrl2km4") (f (quote (("mtd") ("go-faster") ("ftd") ("default" "cli" "ftd") ("cli")))) (l "openthread")))

