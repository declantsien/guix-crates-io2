(define-module (crates-io op en openfga-sdk) #:use-module (crates-io))

(define-public crate-openfga-sdk-0.0.1 (c (n "openfga-sdk") (v "0.0.1") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "1y9b55lkix7wci93rh9zf8rvdgc6na8zvdvk7ki1saav826j5qfq")))

