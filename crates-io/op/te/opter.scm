(define-module (crates-io op te opter) #:use-module (crates-io))

(define-public crate-opter-0.1.0 (c (n "opter") (v "0.1.0") (h "1dmfs55achmr1kndkwv32b7iv55sdd931g6bacrc1q2y3kvcbzw3")))

(define-public crate-opter-0.2.0 (c (n "opter") (v "0.2.0") (h "14h43y2rs9x4i6smshxa3jhj3di29d37qa0g49x9jzixwp91afcl")))

