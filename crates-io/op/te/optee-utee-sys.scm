(define-module (crates-io op te optee-utee-sys) #:use-module (crates-io))

(define-public crate-optee-utee-sys-0.0.0 (c (n "optee-utee-sys") (v "0.0.0") (d (list (d (n "libc") (r "^0.2.49") (d #t) (k 0)))) (h "00l0bn409p4jzdp2dz9j7pnkwmdzdf35rybsjpggxk2a6g901v01")))

(define-public crate-optee-utee-sys-0.0.1 (c (n "optee-utee-sys") (v "0.0.1") (d (list (d (n "libc") (r "^0.2.49") (d #t) (k 0)))) (h "0nbwph91x4phsrfqmsvsdk4p9lp2yxcnjlhhq9hl71s0mjiynpjg")))

