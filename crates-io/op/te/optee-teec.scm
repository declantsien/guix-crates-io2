(define-module (crates-io op te optee-teec) #:use-module (crates-io))

(define-public crate-optee-teec-0.0.1 (c (n "optee-teec") (v "0.0.1") (d (list (d (n "hex") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "optee-teec-sys") (r "^0.0.1") (d #t) (k 0)) (d (n "uuid") (r "^0.7") (d #t) (k 0)))) (h "1ichfpc9z3i9sl00k34p83fkcqs0b509i1crva04v3kzxq5d9y71")))

(define-public crate-optee-teec-0.1.0 (c (n "optee-teec") (v "0.1.0") (d (list (d (n "hex") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "optee-teec-sys") (r "^0.0.1") (d #t) (k 0)) (d (n "uuid") (r "^0.7") (d #t) (k 0)))) (h "0rgds8w1py6ml4yka1ridzzyrz8cc5j1l4vkakdfj8hh4fd74s30") (y #t)))

