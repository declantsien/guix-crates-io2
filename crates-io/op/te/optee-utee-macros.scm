(define-module (crates-io op te optee-utee-macros) #:use-module (crates-io))

(define-public crate-optee-utee-macros-0.0.0 (c (n "optee-utee-macros") (v "0.0.0") (d (list (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "1r42bgvr9slh4i4hhm9qllzwjiw3a5q4z5hqcgm8gsp5yc66dk6k")))

(define-public crate-optee-utee-macros-0.0.1 (c (n "optee-utee-macros") (v "0.0.1") (d (list (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "1l2qai67pwwvgm9c5s8c0xi7mbh6sky968vvpppi56qkb5dk0jsv")))

