(define-module (crates-io op te optee-teec-sys) #:use-module (crates-io))

(define-public crate-optee-teec-sys-0.0.0 (c (n "optee-teec-sys") (v "0.0.0") (d (list (d (n "libc") (r "^0.2.48") (d #t) (k 0)))) (h "12y8fbnv9x1sassh2dx35ay0jkqdj2qjip0kpawn2q6mb9ymi5q8")))

(define-public crate-optee-teec-sys-0.0.1 (c (n "optee-teec-sys") (v "0.0.1") (d (list (d (n "libc") (r "^0.2.48") (d #t) (k 0)))) (h "04a5bc5g203g5y9kkr3vaq4d8c0f227ambvwkaqdmbgyqw8lp7c4")))

