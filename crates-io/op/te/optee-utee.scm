(define-module (crates-io op te optee-utee) #:use-module (crates-io))

(define-public crate-optee-utee-0.0.0 (c (n "optee-utee") (v "0.0.0") (d (list (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "libc") (r "^0.2.49") (d #t) (k 0)) (d (n "optee-utee-macros") (r "^0.0.0") (d #t) (k 0)) (d (n "optee-utee-sys") (r "^0.0.0") (d #t) (k 0)))) (h "0x97zlbv0gdd3sl9jv25ggp2vswnxa4pizpv1x3x77si5k2k50hm")))

(define-public crate-optee-utee-0.0.1 (c (n "optee-utee") (v "0.0.1") (d (list (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "libc") (r "^0.2.49") (d #t) (k 0)) (d (n "optee-utee-macros") (r "^0.0.1") (d #t) (k 0)) (d (n "optee-utee-sys") (r "^0.0.1") (d #t) (k 0)))) (h "0gydl5z8ds4nmaaa2m7wr2bxw1awr1i22ygfda7gvixa21l29nm1")))

