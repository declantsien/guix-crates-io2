(define-module (crates-io op te optempty) #:use-module (crates-io))

(define-public crate-optempty-0.1.7 (c (n "optempty") (v "0.1.7") (d (list (d (n "maplit") (r "^1") (d #t) (k 2)))) (h "1v4nxljygysk29713armrim03dbq353vgnxqigipi2544n5x3sd4")))

(define-public crate-optempty-0.1.8 (c (n "optempty") (v "0.1.8") (d (list (d (n "maplit") (r "^1") (d #t) (k 2)))) (h "1i94yl57rzfkxsdzryxr3v087h0d9qbff3ldr7p8jws7ac9yg4zx")))

(define-public crate-optempty-0.1.9 (c (n "optempty") (v "0.1.9") (d (list (d (n "maplit") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)))) (h "0gskfdr5jklk39vzc5dc6qxi38xzhmkszqwkn2jqa4dbgddn2qvl") (f (quote (("serdejson" "serde_json") ("default"))))))

(define-public crate-optempty-0.1.10 (c (n "optempty") (v "0.1.10") (d (list (d (n "maplit") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)))) (h "0wkdb18vzyj4fazwvmr6snkmdyj1mwq47lhvxxy7pk0jlipwzqw0") (f (quote (("std") ("serdejson" "serde_json") ("default" "std"))))))

(define-public crate-optempty-0.1.11 (c (n "optempty") (v "0.1.11") (d (list (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)))) (h "1zdypkkcwhl0l2lx4lac42mzn8inqws2v60z68ncflx00x4q5mpk") (f (quote (("std") ("serdejson" "serde_json") ("default" "std"))))))

(define-public crate-optempty-0.1.12 (c (n "optempty") (v "0.1.12") (d (list (d (n "serde_json") (r "^1") (f (quote ("alloc"))) (o #t) (k 0)))) (h "1a4brck2r1qbmdvp4qxcfszapzd604vrjswif4bmxg86jnff0961") (f (quote (("std") ("serdejson" "serde_json") ("default" "std"))))))

(define-public crate-optempty-0.1.13 (c (n "optempty") (v "0.1.13") (d (list (d (n "query_map") (r "^0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (f (quote ("alloc"))) (o #t) (k 0)))) (h "0cc55n3j046i2bvy1xdc59axxqm1p1kkf799m0iqb9d77mbm7gf7") (f (quote (("std") ("serdejson" "serde_json") ("querymap" "query_map" "std") ("default" "std"))))))

