(define-module (crates-io op pa oppai-rs) #:use-module (crates-io))

(define-public crate-oppai-rs-0.1.0 (c (n "oppai-rs") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "libc") (r "^0.2.65") (d #t) (k 0)))) (h "1l6av7r2qzxp3nshg9fsqclslpx9b3qcx4n6394186j9b4khqgma")))

(define-public crate-oppai-rs-0.2.0 (c (n "oppai-rs") (v "0.2.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "libc") (r "^0.2.65") (d #t) (k 0)))) (h "0nqh4fpkq23s142sz99084jybjqg8kghjd9k014c198dambk652g")))

(define-public crate-oppai-rs-0.2.1 (c (n "oppai-rs") (v "0.2.1") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "libc") (r "^0.2.65") (d #t) (k 0)))) (h "1cqb8yb97hc8fckkh5n2r4rcq3hdygyjz0g36fd6hp1sjan8r1kf") (y #t)))

(define-public crate-oppai-rs-0.2.2 (c (n "oppai-rs") (v "0.2.2") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "libc") (r "^0.2.65") (d #t) (k 0)))) (h "08g6fmi3xirfs0jfvb919wmyvfwy6myqmz8y31wdsn57dqx5h222")))

(define-public crate-oppai-rs-0.2.3 (c (n "oppai-rs") (v "0.2.3") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "libc") (r "^0.2.65") (d #t) (k 0)))) (h "0zl73v628zm8z9pccbjqblc6xh09zshg0id00c0mkmjyxhylhq70")))

