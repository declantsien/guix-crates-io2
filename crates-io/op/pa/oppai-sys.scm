(define-module (crates-io op pa oppai-sys) #:use-module (crates-io))

(define-public crate-oppai-sys-0.1.0 (c (n "oppai-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.51") (d #t) (k 1)) (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)))) (h "1201avxlq07ir9wzfl17gj3v3v9fjn7snaj4cw9fmwmmr82fgx7x")))

