(define-module (crates-io op tf optfield) #:use-module (crates-io))

(define-public crate-optfield-0.1.0 (c (n "optfield") (v "0.1.0") (d (list (d (n "paste") (r "^0.1.10") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.10") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.106") (f (quote ("derive"))) (d #t) (k 2)) (d (n "syn") (r "^1.0.17") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "19711ng33zc7jbpll5nvb5qyw8r4vskspaqqqn9ilqarn7cs8lgh")))

(define-public crate-optfield-0.2.0 (c (n "optfield") (v "0.2.0") (d (list (d (n "paste") (r "^0.1.10") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.10") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.106") (f (quote ("derive"))) (d #t) (k 2)) (d (n "syn") (r "^1.0.17") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1ia0dvmx10kvdwir8irravhbm1yfyg3m3pvlps0bijzag077iq5m")))

(define-public crate-optfield-0.3.0 (c (n "optfield") (v "0.3.0") (d (list (d (n "paste") (r "^1.0.12") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.27") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 2)) (d (n "syn") (r "^2.0.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "079a5n7lhqsj444fkgk580vra5v2nhym7czwnky9iip9rljz0ngs")))

