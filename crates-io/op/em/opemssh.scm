(define-module (crates-io op em opemssh) #:use-module (crates-io))

(define-public crate-opemssh-0.1.0 (c (n "opemssh") (v "0.1.0") (d (list (d (n "base64") (r "^0.6") (d #t) (k 0)) (d (n "byteorder") (r "^1.1") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "yasna") (r "^0.1") (d #t) (k 0)))) (h "10v695l4mynmbb0awcl5csg320x6a0qb3rcm6alfsk4gnni20z3f")))

(define-public crate-opemssh-0.1.1 (c (n "opemssh") (v "0.1.1") (d (list (d (n "base64") (r "^0.6") (d #t) (k 0)) (d (n "byteorder") (r "^1.1") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "yasna") (r "^0.1") (d #t) (k 0)))) (h "0m9pbm5ihsyf0i32r2n5xzpr2n7v6d3d7vsx4d1a3i8858ldhhj4")))

