(define-module (crates-io op -p op-primitives) #:use-module (crates-io))

(define-public crate-op-primitives-0.1.0 (c (n "op-primitives") (v "0.1.0") (d (list (d (n "enum-variants-strings") (r "^0.2.3") (d #t) (k 0)) (d (n "ethers-core") (r "^2.0") (d #t) (k 0)) (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "hex-literal") (r "^0.4.1") (d #t) (k 0)) (d (n "inquire") (r "^0.6.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "project-root") (r "^0.2.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "strum") (r "^0.25") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "1vjhya08c19mc3ffzcscykmrv0wxjz1imlzpax7a5rg2qijk0cgk") (r "1.72")))

