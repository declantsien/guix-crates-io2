(define-module (crates-io op cu opcua-console-logging) #:use-module (crates-io))

(define-public crate-opcua-console-logging-0.6.0 (c (n "opcua-console-logging") (v "0.6.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "env_logger") (r "^0.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1dm73xs3c2qw7bhwzppk32myblmn0g85x5ynwlwdhwxrq57nqrlk")))

(define-public crate-opcua-console-logging-0.7.0 (c (n "opcua-console-logging") (v "0.7.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "env_logger") (r "^0.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "101vjbkrdlbmpgf5969sgiv8vi6py9rfjnpmprxnnhvf6vvhrvb2")))

(define-public crate-opcua-console-logging-0.8.0 (c (n "opcua-console-logging") (v "0.8.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "env_logger") (r "^0.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0aa529v6k7qbb8ykg7grfzhaw98jxckjfv0q7bsg7h5gfxxn7d8f")))

(define-public crate-opcua-console-logging-0.8.1 (c (n "opcua-console-logging") (v "0.8.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "env_logger") (r "^0.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "00g9kvl9i20q7kflbpy4rjlvgnpdnva9fzfbqf38jfm9fjswp6hv")))

(define-public crate-opcua-console-logging-0.9.0 (c (n "opcua-console-logging") (v "0.9.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "env_logger") (r "^0.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1ziv6iz2c3ilx62wrg9nqg2vzb0rcx15sypiyh5xnwdsa2ckxx9f")))

(define-public crate-opcua-console-logging-0.9.1 (c (n "opcua-console-logging") (v "0.9.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "env_logger") (r "^0.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1349bg00siparxxgv26gwzfzxgr0afycddrpv258fsm3wv269iix")))

