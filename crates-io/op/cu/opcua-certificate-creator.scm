(define-module (crates-io op cu opcua-certificate-creator) #:use-module (crates-io))

(define-public crate-opcua-certificate-creator-0.1.0 (c (n "opcua-certificate-creator") (v "0.1.0") (d (list (d (n "clap") (r "~2.19.0") (d #t) (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "opcua-core") (r "^0.1.0") (d #t) (k 0)) (d (n "opcua-types") (r "^0.1.0") (d #t) (k 0)))) (h "0mvcgq2x2id866aaarvw60z35jnj6nksirlx1wrl310805y5lhfl")))

(define-public crate-opcua-certificate-creator-0.2.0 (c (n "opcua-certificate-creator") (v "0.2.0") (d (list (d (n "clap") (r "~2.19.0") (d #t) (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "opcua-core") (r "^0.2.0") (d #t) (k 0)) (d (n "opcua-types") (r "^0.2.0") (d #t) (k 0)))) (h "15kix4c8dw0vk9dwy13za8cbiz5q6jw421icllhhvrw5yfngnyg0")))

(define-public crate-opcua-certificate-creator-0.3.0 (c (n "opcua-certificate-creator") (v "0.3.0") (d (list (d (n "clap") (r "^2.27") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "opcua-core") (r "^0.3.0") (d #t) (k 0)) (d (n "opcua-types") (r "^0.3.0") (d #t) (k 0)))) (h "1365vh6vq992ky1ladfs23f815cqjk5fxxn5i3xq9z1nhsxd3j1c")))

(define-public crate-opcua-certificate-creator-0.4.0 (c (n "opcua-certificate-creator") (v "0.4.0") (d (list (d (n "clap") (r "^2.27") (d #t) (k 0)) (d (n "opcua-core") (r "^0.4.0") (d #t) (k 0)) (d (n "opcua-types") (r "^0.4.0") (d #t) (k 0)))) (h "0knilpz86f2ij9g09c1h7mzgw8nrl7x04cgraghnsfa997j96kgw")))

(define-public crate-opcua-certificate-creator-0.5.0 (c (n "opcua-certificate-creator") (v "0.5.0") (d (list (d (n "clap") (r "^2.27") (d #t) (k 0)) (d (n "opcua-core") (r "^0.5.0") (d #t) (k 0)) (d (n "opcua-types") (r "^0.5.0") (d #t) (k 0)))) (h "0q0wbpp6bsqhjvzclkyvw3g0633r850mppfsqhli467b8rbqa5dm")))

(define-public crate-opcua-certificate-creator-0.6.0 (c (n "opcua-certificate-creator") (v "0.6.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "opcua-core") (r "^0.6.0") (d #t) (k 0)) (d (n "opcua-types") (r "^0.6.0") (d #t) (k 0)))) (h "12rz9l0kpnafxa4q77xy3l7kl79bzjs7jy4zb7yghh96iw09nb8k")))

(define-public crate-opcua-certificate-creator-0.7.0 (c (n "opcua-certificate-creator") (v "0.7.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "opcua-core") (r "^0.7.0") (d #t) (k 0)) (d (n "opcua-types") (r "^0.7.0") (d #t) (k 0)))) (h "05qqfknx69xkf2x2s966zdxx0nnrm4jcw1p0rvlmsxvlm90zq0zm")))

(define-public crate-opcua-certificate-creator-0.8.0 (c (n "opcua-certificate-creator") (v "0.8.0") (d (list (d (n "opcua-crypto") (r "^0.8.0") (d #t) (k 0)) (d (n "pico-args") (r "^0.3") (d #t) (k 0)))) (h "1afp89anc3j84xqq9v2q10zmpdxkxa0bpjjblylwazzbv6k0pyn5")))

(define-public crate-opcua-certificate-creator-0.9.0 (c (n "opcua-certificate-creator") (v "0.9.0") (d (list (d (n "opcua-crypto") (r "^0.9.0") (d #t) (k 0)) (d (n "pico-args") (r "^0.3") (d #t) (k 0)))) (h "0mb8bgxh9picqj655wmy8brfr5d27fa6g4v579mhqnz314d95vns")))

