(define-module (crates-io op -a op-api-sdk) #:use-module (crates-io))

(define-public crate-op-api-sdk-0.1.0 (c (n "op-api-sdk") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 2)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.8") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros"))) (d #t) (k 2)))) (h "0x76c5ai87kj34mxcrjgv2m9axr945ba8d6j110m349jzfhf10jx")))

