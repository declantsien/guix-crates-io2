(define-module (crates-io op ma opmap) #:use-module (crates-io))

(define-public crate-opmap-1.0.0 (c (n "opmap") (v "1.0.0") (d (list (d (n "drain-while") (r "^0.1.0") (d #t) (k 0)) (d (n "hamcrest2") (r "^0.3.0") (d #t) (k 2)))) (h "0wd2lj3aqip7wa8k1cmrj2bfzzc41hchn13fdrn9w27xzb1nlvvc")))

(define-public crate-opmap-1.0.1 (c (n "opmap") (v "1.0.1") (d (list (d (n "drain-while") (r "^0.1.0") (d #t) (k 0)) (d (n "hamcrest2") (r "^0.3.0") (d #t) (k 2)))) (h "1apad6r29zdn8nc560ipm3l2922nbbwhlgdprw52g8q755kir3jr")))

(define-public crate-opmap-1.0.2 (c (n "opmap") (v "1.0.2") (d (list (d (n "drain-while") (r "^0.1.0") (d #t) (k 0)) (d (n "hamcrest2") (r "^0.3.0") (d #t) (k 2)))) (h "0in48ls6rnxcdij4mdprmrpzjav6a3jxfjpccx8c1k7d8hx928w2")))

(define-public crate-opmap-1.0.3 (c (n "opmap") (v "1.0.3") (d (list (d (n "drain-while") (r "^0.1.0") (d #t) (k 0)) (d (n "hamcrest2") (r "^0.3.0") (d #t) (k 2)))) (h "1g3lidyb0liwajhz0ywrshk2d9azszgm5ncd32yr4mz7sikp1nxc")))

(define-public crate-opmap-1.0.31 (c (n "opmap") (v "1.0.31") (d (list (d (n "drain-while") (r "^0.1.0") (d #t) (k 0)) (d (n "hamcrest2") (r "^0.3.0") (d #t) (k 2)))) (h "0pkpq02rgi3cdl1z0zgixchs48gfxi3db32mfrh6cgjz4f7zidii")))

(define-public crate-opmap-1.0.32 (c (n "opmap") (v "1.0.32") (d (list (d (n "drain-while") (r "^0.1.0") (d #t) (k 0)) (d (n "hamcrest2") (r "^0.3.0") (d #t) (k 2)))) (h "06pr9bxwmxsvnkr4ah92na97bwvnkn4d5ydrzvyflhkizmp9r71z")))

(define-public crate-opmap-1.0.33 (c (n "opmap") (v "1.0.33") (d (list (d (n "drain-while") (r "^0.1.0") (d #t) (k 0)) (d (n "hamcrest2") (r "^0.3.0") (d #t) (k 2)))) (h "01xrh379hmn80ckp6q1wxg915a3w5w2mc8qmhxvrlhamkzsgdcy7")))

(define-public crate-opmap-1.0.34 (c (n "opmap") (v "1.0.34") (d (list (d (n "drain-while") (r "^0.1.0") (d #t) (k 0)) (d (n "hamcrest2") (r "^0.3.0") (d #t) (k 2)))) (h "176jx5qcpssk631204q0d389k0dspc38mwhzr2vd8vigl0zrr0mm")))

(define-public crate-opmap-1.0.35 (c (n "opmap") (v "1.0.35") (d (list (d (n "drain-while") (r "^0.1.0") (d #t) (k 0)) (d (n "hamcrest2") (r "^0.3.0") (d #t) (k 2)))) (h "046zr7gnn057cibdngqsws365x8ihk3b83rafhfii4cjgvwnx3s4")))

(define-public crate-opmap-1.0.36 (c (n "opmap") (v "1.0.36") (d (list (d (n "drain-while") (r "^0.1.0") (d #t) (k 0)) (d (n "hamcrest2") (r "^0.3.0") (d #t) (k 2)))) (h "1jdwa86p0j2wjflsqz455d4h0hkzp90mplnqhyg5cm3521p9qcsw")))

(define-public crate-opmap-1.0.37 (c (n "opmap") (v "1.0.37") (d (list (d (n "drain-while") (r "^0.1.0") (d #t) (k 0)) (d (n "hamcrest2") (r "^0.3.0") (d #t) (k 2)))) (h "0bf2vjqab9jia5skkrqzjwhb6dwhp0vmzbmaijpsnrj6s6dzhw64")))

(define-public crate-opmap-1.0.38 (c (n "opmap") (v "1.0.38") (d (list (d (n "drain-while") (r "^0.1.0") (d #t) (k 0)) (d (n "hamcrest2") (r "^0.3.0") (d #t) (k 2)))) (h "1dqwqmrx625jga561z8g0l45k79c30hyfrs97hrwkp2airdznshd")))

(define-public crate-opmap-1.0.39 (c (n "opmap") (v "1.0.39") (d (list (d (n "drain-while") (r "^0.1.0") (d #t) (k 0)) (d (n "hamcrest2") (r "^0.3.0") (d #t) (k 2)))) (h "1xhhqq2qjxybqjm6n9jwxpipz8wyz1czz8n0342jbjihziqj29yk")))

(define-public crate-opmap-1.0.40 (c (n "opmap") (v "1.0.40") (d (list (d (n "drain-while") (r "^0.1.0") (d #t) (k 0)) (d (n "hamcrest2") (r "^0.3.0") (d #t) (k 2)))) (h "1whqq93x6agdd4wrfv9lr4nzwqj1rkn0gdyzr9chm3mlxxbw90bk")))

(define-public crate-opmap-1.0.41 (c (n "opmap") (v "1.0.41") (d (list (d (n "drain-while") (r "^0.1.0") (d #t) (k 0)) (d (n "hamcrest2") (r "^0.3.0") (d #t) (k 2)))) (h "02d29mqcaifgwd9icqw7sz8alnfn4yir9in1j4zg3v0nqy8047g2")))

(define-public crate-opmap-1.0.42 (c (n "opmap") (v "1.0.42") (d (list (d (n "drain-while") (r "^0.1.0") (d #t) (k 0)) (d (n "hamcrest2") (r "^0.3.0") (d #t) (k 2)))) (h "0jjbr29n7k90aa0ihw3n951jg8d0fns7nw78z5jl0dm0jl4adr9x")))

(define-public crate-opmap-1.0.43 (c (n "opmap") (v "1.0.43") (d (list (d (n "drain-while") (r "^0.1.0") (d #t) (k 0)) (d (n "hamcrest2") (r "^0.3.0") (d #t) (k 2)))) (h "0ysyh8icsgknvj3vax2crglkwrpqczj5psv3jnm6k614vskwkysg")))

(define-public crate-opmap-1.0.44 (c (n "opmap") (v "1.0.44") (d (list (d (n "drain-while") (r "^0.1.0") (d #t) (k 0)) (d (n "hamcrest2") (r "^0.3.0") (d #t) (k 2)))) (h "003xiaxgb9478ychxp4fr1wxvbwxph5mzvg6raa7i129yxgcpd5z")))

(define-public crate-opmap-1.0.45 (c (n "opmap") (v "1.0.45") (d (list (d (n "drain-while") (r "^0.1.0") (d #t) (k 0)) (d (n "hamcrest2") (r "^0.3.0") (d #t) (k 2)))) (h "0x98agsyiiqwacx0sfn7hsz0bq63wkwfwiva12jxgn0d8ri1lr7s")))

(define-public crate-opmap-1.0.46 (c (n "opmap") (v "1.0.46") (d (list (d (n "drain-while") (r "^0.1.0") (d #t) (k 0)) (d (n "hamcrest2") (r "^0.3.0") (d #t) (k 2)))) (h "0scp5ajf6lc8mdpq6hddm0c1iizys71wzqc6hp0vxlnk8cqsqbmg")))

(define-public crate-opmap-1.0.47 (c (n "opmap") (v "1.0.47") (d (list (d (n "drain-while") (r "^0.1.0") (d #t) (k 0)) (d (n "hamcrest2") (r "^0.3.0") (d #t) (k 2)))) (h "0wrc7wzhyjvvjhv2fx244070zlgvflf3fibhpf66wrc7kqzs00p0")))

