(define-module (crates-io op ma opmark-egui) #:use-module (crates-io))

(define-public crate-opmark-egui-0.0.1 (c (n "opmark-egui") (v "0.0.1") (d (list (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "eframe") (r "^0.16") (d #t) (k 0)) (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "opmark") (r "^0.0.1") (d #t) (k 0)))) (h "0mhssp1l7jv6zcqfzircxf1dfjjdwvqgvrw66yll7mf9npyfxbiz")))

(define-public crate-opmark-egui-0.0.2 (c (n "opmark-egui") (v "0.0.2") (d (list (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "eframe") (r "^0.16") (d #t) (k 0)) (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "opmark") (r "^0.0.1") (d #t) (k 0)))) (h "10z4y0m60j8m56gd6d2xgywv6xg2l2s2qisav8g8lmd5jq02hfwp")))

(define-public crate-opmark-egui-0.0.3 (c (n "opmark-egui") (v "0.0.3") (d (list (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "eframe") (r "^0.16") (d #t) (k 0)) (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "opmark") (r "^0.0.1") (d #t) (k 0)))) (h "1sfppqx9k9i6c9mcdg4fc1q6msb40hq74sfpah8i2jyv8r50bcyx")))

(define-public crate-opmark-egui-0.0.4 (c (n "opmark-egui") (v "0.0.4") (d (list (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "eframe") (r "^0.16") (d #t) (k 0)) (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "opmark") (r "^0.0.1") (d #t) (k 0)))) (h "0yiwry7y7w5ngw78ga9q61qwqcd11kzl2npplqq9b5glwy8n2r09")))

(define-public crate-opmark-egui-0.0.5 (c (n "opmark-egui") (v "0.0.5") (d (list (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "eframe") (r "^0.16") (d #t) (k 0)) (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "opmark") (r "^0.0.2") (d #t) (k 0)))) (h "0434r21xwb9b2q0r09831sjp4h183xvysapzp1srsxab804qbv7v")))

(define-public crate-opmark-egui-0.0.6 (c (n "opmark-egui") (v "0.0.6") (d (list (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "eframe") (r "^0.16") (d #t) (k 0)) (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "opmark") (r "^0.0.2") (d #t) (k 0)))) (h "1yksnbbgin1pwss0p1b0smhdc1w6zzq67scz206jzvk085l2gm1f")))

(define-public crate-opmark-egui-0.0.7 (c (n "opmark-egui") (v "0.0.7") (d (list (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "eframe") (r "^0.16") (d #t) (k 0)) (d (n "font-kit") (r "^0.11") (d #t) (k 0)) (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "opmark") (r "^0.0.2") (d #t) (k 0)))) (h "19wljyrdsgf9lnx2zss7jbb9d5dv8wnv8lhkar4a4xr1kazqpksp")))

