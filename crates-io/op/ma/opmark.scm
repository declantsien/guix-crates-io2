(define-module (crates-io op ma opmark) #:use-module (crates-io))

(define-public crate-opmark-0.0.1 (c (n "opmark") (v "0.0.1") (h "12k9bm5ksf6vf2vifj6cywjifrqvn33j1qahr1scrvmhg371fj49")))

(define-public crate-opmark-0.0.2 (c (n "opmark") (v "0.0.2") (h "1cplg2zq28gk9y57hlqdzq6mk5hdz58xpbm07vn8f58rh8dwh727")))

(define-public crate-opmark-0.0.3 (c (n "opmark") (v "0.0.3") (h "1371xv4gw12rbqkbdc8vw5mghxpw2xnp6nab5irjfygbw3x67qj8")))

