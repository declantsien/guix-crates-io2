(define-module (crates-io op sg opsgenie-rs) #:use-module (crates-io))

(define-public crate-opsgenie-rs-0.1.0-alpha.1 (c (n "opsgenie-rs") (v "0.1.0-alpha.1") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "1a0wlkv3pi1n4rdj9k7jhaqxxih4wsh8rdd5gr98y3yzdks7k6z4")))

(define-public crate-opsgenie-rs-0.1.0-alpha.2 (c (n "opsgenie-rs") (v "0.1.0-alpha.2") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "1nvjw55gzddlwi6b70i0b6zckl5frif9647swrvsand2n1sx8rwq")))

