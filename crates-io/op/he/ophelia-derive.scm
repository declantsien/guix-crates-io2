(define-module (crates-io op he ophelia-derive) #:use-module (crates-io))

(define-public crate-ophelia-derive-0.1.0 (c (n "ophelia-derive") (v "0.1.0") (d (list (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "15jigvqx708kx40a9yf4dw1nxlv56zqkxq6sk5rsx99vnj506h0v")))

(define-public crate-ophelia-derive-0.2.0 (c (n "ophelia-derive") (v "0.2.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1m7v39g6c85lbz8insacm5703rwad9jdhmlh553abn8kaskcfqc2")))

(define-public crate-ophelia-derive-0.3.0 (c (n "ophelia-derive") (v "0.3.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "056qd5c74irl3m0hkmlaxa3c4b1zvrxr0p8zcafvaj892hshzgwk")))

