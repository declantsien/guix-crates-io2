(define-module (crates-io op he ophelia-blst) #:use-module (crates-io))

(define-public crate-ophelia-blst-0.3.0 (c (n "ophelia-blst") (v "0.3.0") (d (list (d (n "blst") (r "^0.3") (d #t) (k 0)) (d (n "ophelia") (r "^0.3") (d #t) (k 0)) (d (n "ophelia-derive") (r "^0.3") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1.0") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0sddjpd3rqs86dgr323g20d2xkwnc7ycjfhlhkm672pg9y9dg5yf") (f (quote (("default"))))))

(define-public crate-ophelia-blst-0.3.1 (c (n "ophelia-blst") (v "0.3.1") (d (list (d (n "blst") (r "^0.3") (d #t) (k 0)) (d (n "ophelia") (r "^0.3") (d #t) (k 0)) (d (n "ophelia-derive") (r "^0.3") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1.0") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "18w2gmsgpaiz2y9fin2cms1p9qlpsj6ghz3mkvzvyahdjw0nqah7") (f (quote (("fast") ("default"))))))

