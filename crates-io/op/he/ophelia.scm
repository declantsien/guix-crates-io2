(define-module (crates-io op he ophelia) #:use-module (crates-io))

(define-public crate-ophelia-0.1.0 (c (n "ophelia") (v "0.1.0") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "derive_more") (r "^0.15") (d #t) (k 0)) (d (n "ophelia-hasher") (r "^0.1") (d #t) (k 0)) (d (n "ophelia-quickcheck-types") (r "^0.1") (o #t) (k 0)) (d (n "quickcheck") (r "^0.8") (o #t) (k 0)) (d (n "quickcheck") (r "^0.8") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.8") (d #t) (k 2)) (d (n "rand") (r "^0.6") (o #t) (k 0)))) (h "0571y8i7fnhx2nihlpyz68yaxc658ihslg72xsyannk00lqlz5y5") (f (quote (("proptest" "quickcheck" "ophelia-quickcheck-types" "ophelia-hasher/proptest") ("generate" "rand") ("default"))))))

(define-public crate-ophelia-0.2.0 (c (n "ophelia") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "ophelia-hasher") (r "^0.2") (d #t) (k 0)) (d (n "rand_core") (r "^0.5") (d #t) (k 0)))) (h "0wi6z20d0nw3vqs3gqh6jz2mfanpfa4f960rp0cw394fdraflk9x")))

(define-public crate-ophelia-0.3.0 (c (n "ophelia") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "ophelia-hasher") (r "^0.2") (d #t) (k 0)) (d (n "rand_core") (r "^0.5") (d #t) (k 0)))) (h "00ck83svk0c6jbrir0rdly5zy071ahq8aqfgna4zzxfhxbn7b0ka") (y #t)))

(define-public crate-ophelia-0.3.1 (c (n "ophelia") (v "0.3.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "ophelia-hasher") (r "^0.2") (d #t) (k 0)) (d (n "rand_core") (r "^0.5") (d #t) (k 0)))) (h "1s0fqj9vj01gmsd768kar109pn7bprarzz72y1rcdaqa70lcf8iy") (y #t)))

(define-public crate-ophelia-0.3.2 (c (n "ophelia") (v "0.3.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "ophelia-hasher") (r "^0.2") (d #t) (k 0)) (d (n "rand_core") (r "^0.5") (d #t) (k 0)))) (h "0c7zd00c37r99g8g75pxdqws8jwdhrwlqji4xpvcqw8vihrlvajw")))

(define-public crate-ophelia-0.3.3 (c (n "ophelia") (v "0.3.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bytes") (r "^1.1") (d #t) (k 0)) (d (n "ophelia-hasher") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1sx7wkdq1nig9s09viadd5jr1zw4b7ac1ymqjlw07gwhfcd6nycr")))

(define-public crate-ophelia-0.3.4 (c (n "ophelia") (v "0.3.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bytes") (r "^1.1") (d #t) (k 0)) (d (n "ophelia-hasher") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "0pk24xzwbq9jsjz7g8p6g55zglwsrmbpan3gaa2p528jqv0aarwf")))

