(define-module (crates-io op he ophelia-quickcheck) #:use-module (crates-io))

(define-public crate-ophelia-quickcheck-0.2.0 (c (n "ophelia-quickcheck") (v "0.2.0") (d (list (d (n "ophelia-hasher") (r "^0.2") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9") (d #t) (k 0)))) (h "18mzgy5ydhx1kwycqwrrc5dn7f2dyj2nv513s7qd20k3k5xkn6dq")))

(define-public crate-ophelia-quickcheck-0.2.1 (c (n "ophelia-quickcheck") (v "0.2.1") (d (list (d (n "ophelia-hasher") (r "^0.2") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0") (d #t) (k 0)))) (h "0lc3gpybbnpi4kp1rwsphw0igq6gz8lq6krird6dp3dzps7m4zhd")))

