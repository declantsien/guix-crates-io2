(define-module (crates-io op he ophelia-hasher-blake2b) #:use-module (crates-io))

(define-public crate-ophelia-hasher-blake2b-0.1.0 (c (n "ophelia-hasher-blake2b") (v "0.1.0") (d (list (d (n "blake2b_simd") (r "^0.5") (d #t) (k 0)) (d (n "ophelia-hasher") (r "^0.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.8") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.8") (d #t) (k 2)))) (h "0jlmcgba5ii6x5b26jwxc6gwx1k701w9mr4lqdsfp3y2ix53mpas")))

(define-public crate-ophelia-hasher-blake2b-0.2.0 (c (n "ophelia-hasher-blake2b") (v "0.2.0") (d (list (d (n "blake2b_simd") (r "^0.5") (d #t) (k 0)) (d (n "ophelia-hasher") (r "^0.2") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.8") (d #t) (k 2)))) (h "11pnqyqnajlm2insbm5xnwari9y5qqzn26iv3n04g7s5gwswqlx9")))

