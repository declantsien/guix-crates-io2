(define-module (crates-io op he ophelia-hasher) #:use-module (crates-io))

(define-public crate-ophelia-hasher-0.1.0 (c (n "ophelia-hasher") (v "0.1.0") (d (list (d (n "derive_more") (r "^0.15") (d #t) (k 0)) (d (n "quickcheck") (r "^0.8") (o #t) (k 0)) (d (n "quickcheck") (r "^0.8") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.8") (d #t) (k 2)))) (h "1fbnqxajbhsbgk264jrdxcf9idaxa8qnrhnksmihf63w9ibwi3mh") (f (quote (("proptest" "quickcheck"))))))

(define-public crate-ophelia-hasher-0.2.0 (c (n "ophelia-hasher") (v "0.2.0") (d (list (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.8") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "13fqw6mnlqcdxjcl44q52fpjj8svzc662scygs809wxw6sil3nb1")))

