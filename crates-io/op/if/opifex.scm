(define-module (crates-io op if opifex) #:use-module (crates-io))

(define-public crate-opifex-0.1.0 (c (n "opifex") (v "0.1.0") (h "0cgqgmz73jx2qd68r6hrz7c4wp7v0a7jz6q6cy9d5ngp1zmxih1d") (y #t)))

(define-public crate-opifex-0.3.0 (c (n "opifex") (v "0.3.0") (d (list (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-util") (r "^0.7.10") (d #t) (k 0)))) (h "14r7jazv0a6ygcmrpgak2bqxn1cj3pfprjna4f0cqdzj21hkrhnb")))

