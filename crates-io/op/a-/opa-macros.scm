(define-module (crates-io op a- opa-macros) #:use-module (crates-io))

(define-public crate-opa-macros-0.1.0 (c (n "opa-macros") (v "0.1.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "sha1") (r "^0.10.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (d #t) (k 0)) (d (n "which") (r "^4.2.4") (d #t) (k 0)))) (h "04p2czij3siraqybfylg139prr990cg3v42xzrcq6v5zwm4kgqcq") (y #t)))

