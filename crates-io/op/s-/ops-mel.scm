(define-module (crates-io op s- ops-mel) #:use-module (crates-io))

(define-public crate-ops-mel-0.6.0 (c (n "ops-mel") (v "0.6.0") (d (list (d (n "melodium-core") (r "^0.6.0") (d #t) (k 0)) (d (n "melodium-macro") (r "^0.6.0") (d #t) (k 0)))) (h "0nr203m6gbq6dlm1pc0czc33nn6s3ckyfrnsnhlkb8plp0kg94i7") (r "1.60")))

(define-public crate-ops-mel-0.7.0-rc1 (c (n "ops-mel") (v "0.7.0-rc1") (d (list (d (n "melodium-core") (r "=0.7.0-rc1") (d #t) (k 0)) (d (n "melodium-macro") (r "=0.7.0-rc1") (d #t) (k 0)))) (h "0plrdkbm85hakvjsifg9ax143aaacqda5i7894j20n0bpxdpk8w6") (f (quote (("real") ("plugin") ("mock")))) (r "1.60")))

(define-public crate-ops-mel-0.7.0 (c (n "ops-mel") (v "0.7.0") (d (list (d (n "melodium-core") (r "=0.7.0") (d #t) (k 0)) (d (n "melodium-macro") (r "=0.7.0") (d #t) (k 0)))) (h "0vk4frgnvfbmvsw2xl1qjfa2nx9vcm1cx4h03lxwvsda2d06qcsf") (f (quote (("real") ("plugin") ("mock")))) (r "1.60")))

(define-public crate-ops-mel-0.7.1 (c (n "ops-mel") (v "0.7.1") (d (list (d (n "melodium-core") (r "^0.7.1") (d #t) (k 0)) (d (n "melodium-macro") (r "^0.7.1") (d #t) (k 0)))) (h "1fx00v945cyhcvlsnjq8ih1622ybxfir5mfvs0ik07j3r7nrrpps") (f (quote (("real") ("plugin") ("mock")))) (r "1.60")))

