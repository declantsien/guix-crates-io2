(define-module (crates-io op s- ops-core) #:use-module (crates-io))

(define-public crate-ops-core-0.1.0 (c (n "ops-core") (v "0.1.0") (h "0shrx2qk3nxdprhc7bvk2vy39nb2ymw7iciwbw8d61c0b6nr4j3s")))

(define-public crate-ops-core-0.2.0 (c (n "ops-core") (v "0.2.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)))) (h "0mxgqy5wg0d4rw2cp1lf3cbvds99818j3j63512y44wawddlcmqd")))

