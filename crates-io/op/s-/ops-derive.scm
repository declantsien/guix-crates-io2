(define-module (crates-io op s- ops-derive) #:use-module (crates-io))

(define-public crate-ops-derive-0.1.0 (c (n "ops-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0wkai92ywl1mvydvbd6rjjv1f10679yrrqm6l6n8ig43bl9r9qvi")))

(define-public crate-ops-derive-0.1.1 (c (n "ops-derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0h9rjh2c319vcrp5z7qf2cny2fqxn41kmvi1nflhm946baw1zyz1")))

