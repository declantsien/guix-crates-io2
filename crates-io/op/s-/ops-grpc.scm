(define-module (crates-io op s- ops-grpc) #:use-module (crates-io))

(define-public crate-ops-grpc-0.1.0 (c (n "ops-grpc") (v "0.1.0") (d (list (d (n "ops-core") (r "^0.2") (d #t) (k 0)) (d (n "prost") (r "^0.6") (d #t) (k 0)) (d (n "tonic") (r "^0.2") (d #t) (k 0)) (d (n "tonic-build") (r "^0.2") (d #t) (k 1)))) (h "1pfvs26gn2p0j514k9q9wsjyk6nramgx4bfja9b2hrgffiydp1ij")))

