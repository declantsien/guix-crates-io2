(define-module (crates-io op tr optrs) #:use-module (crates-io))

(define-public crate-optrs-0.1.0 (c (n "optrs") (v "0.1.0") (d (list (d (n "clap") (r "^3.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pyo3") (r "^0.16.5") (f (quote ("extension-module"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1cwclydh9r3ij5ddnhk22cdlwrmkd49p7mg8zav2vnlls65pxk0l")))

