(define-module (crates-io op tr optra) #:use-module (crates-io))

(define-public crate-optra-0.1.0 (c (n "optra") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.3") (d #t) (k 2)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rdiff") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)) (d (n "serde_macros") (r "^0.8") (d #t) (k 0)))) (h "1sfcbd4n4d1qq0da5im58552prdg41lck3ab0qwphr2jx5jxwfkb")))

(define-public crate-optra-0.1.1 (c (n "optra") (v "0.1.1") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "env_logger") (r "^0.3") (d #t) (k 2)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rdiff") (r "^0.1") (d #t) (k 0)))) (h "0i80chxrssfsfcsfri1w9zqxcywk85m0z0vrwyg7s6ax9x21pkbj")))

(define-public crate-optra-0.2.1 (c (n "optra") (v "0.2.1") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "env_logger") (r "^0.3") (d #t) (k 2)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rdiff") (r "^0.1") (d #t) (k 0)))) (h "0qyzd9dxjcl57db9svibv3q2v7f2wif93xzh93a4smxl6jbadh0f")))

