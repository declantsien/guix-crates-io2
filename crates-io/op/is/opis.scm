(define-module (crates-io op is opis) #:use-module (crates-io))

(define-public crate-opis-0.2.0 (c (n "opis") (v "0.2.0") (h "1a5pf6v27n0xjlfizsfi8c8vc5ym5p185fmdv2is55h8xajlzb4m")))

(define-public crate-opis-0.3.0 (c (n "opis") (v "0.3.0") (h "112qq0vhhyk6q1sgsz2vslmgj94yqlybl90bn0a671icfhl3qd2k")))

(define-public crate-opis-0.4.0 (c (n "opis") (v "0.4.0") (h "1i896mz2bp84m4mzpnwnqjy1hn3fj86js53n64r917jwyivfsa0k")))

(define-public crate-opis-0.5.0 (c (n "opis") (v "0.5.0") (h "0h9pf464cbvg45fk9yibyl0ai092qwacncm7gwnf5f6pd7sdi5lg")))

(define-public crate-opis-0.6.0 (c (n "opis") (v "0.6.0") (h "1nywa5hvvd0r9zp4c6qzmirf9vjnqcrih218im2wqhs5r45l1khk")))

(define-public crate-opis-0.7.0 (c (n "opis") (v "0.7.0") (h "07p1i38vn4vqvqp3li8kwv54f8qaa2mx5s1hfxi65ivw3pmi707x")))

(define-public crate-opis-0.8.0 (c (n "opis") (v "0.8.0") (h "02smlf3r1rjprabj4cd88hrcgmgqzpdqlbxn7m1vk89pg84xi273")))

(define-public crate-opis-0.9.0 (c (n "opis") (v "0.9.0") (h "0yk2xv01ij4lr7wnr0py30jbdaj3rrjjvklcxj5vb1wbhrzcbhqb")))

(define-public crate-opis-1.0.0 (c (n "opis") (v "1.0.0") (h "1vg5ln99k6zpr6bkkx85wbfizc99rgrqwfrm5kwfl3xw8hhkv2yh")))

(define-public crate-opis-1.1.0 (c (n "opis") (v "1.1.0") (h "03cwwb8yh4b4zcfdakcjidhwhflzykw98qdka4nzxzis5ngaww71")))

(define-public crate-opis-1.2.0 (c (n "opis") (v "1.2.0") (h "0vn94abiz9qazpwz72rpkamfn8l42bn3ahmh6js0vbjlq2iszndw")))

(define-public crate-opis-2.0.0 (c (n "opis") (v "2.0.0") (h "1h6w5mna2j9di941nfg3cyyq1dy9zwbccmm2gj6vf3apq5xigvsc")))

(define-public crate-opis-2.1.0 (c (n "opis") (v "2.1.0") (h "0y6gvplcs9bdf27bsjb50dbysa2xasywp2kw3a5l1rsn5vwl241i")))

(define-public crate-opis-2.2.0 (c (n "opis") (v "2.2.0") (h "0ja6hzrzkj95z1wyr02r47rb92knhrq4jgwdd8cwvw84lx00dyh7")))

(define-public crate-opis-3.0.0 (c (n "opis") (v "3.0.0") (h "11sbd47ly2xzg8knspxmjwr5q13kkliagjmzy6y0rphhclfikrlw")))

(define-public crate-opis-3.0.1 (c (n "opis") (v "3.0.1") (h "05hkrz3i5m824msp8184w794piaim2i20s8ig4fazv713g9df7jg")))

(define-public crate-opis-3.0.2 (c (n "opis") (v "3.0.2") (h "03h384xy0qvl2zxlw9a88x91ckypp4gv64y7vp5fn2d8znqyapxh")))

(define-public crate-opis-3.0.3 (c (n "opis") (v "3.0.3") (h "14xv2yzqwz06xlviww32s6iqws2rsvqh4v746jnxb807v8q004q6")))

(define-public crate-opis-3.0.4 (c (n "opis") (v "3.0.4") (h "0v4c8bjrh4fay4iriqm3vbdw4gns0dnkafj147kiaa1y5wmkfdcg")))

(define-public crate-opis-3.0.5 (c (n "opis") (v "3.0.5") (h "1xrn57n15h5fyknjnbci3j3svwin3krw9x6napz93v65pxh9xfgj")))

(define-public crate-opis-3.0.6 (c (n "opis") (v "3.0.6") (h "01najmdm0c8ynswziwqgf4qfx97r5rp4lpd7fy7930s6r1d4l7rw")))

(define-public crate-opis-3.0.7 (c (n "opis") (v "3.0.7") (h "1ycibq97cycqi0avssw1zh628d3mprqjd609jr2pashnhad79z9m")))

(define-public crate-opis-3.1.0 (c (n "opis") (v "3.1.0") (h "0w4f3jxn4wii2hqjh56q4wpah6anbxmxrhi9j9nxdsq3lxacl0y4")))

(define-public crate-opis-4.0.0 (c (n "opis") (v "4.0.0") (h "00nxidva8i11ky79ibbxb5f1xw990bc5gdbgpxacfapwhc35kcag")))

(define-public crate-opis-4.1.0 (c (n "opis") (v "4.1.0") (h "0ldzkyzrnk80p9qfsd0kpfq3m3qbkhwrcq15pzs65hx2ipv7ffmc")))

(define-public crate-opis-5.0.0 (c (n "opis") (v "5.0.0") (h "1smxhlrrqaqrz9dnrgagyw40q85w912i5wfpzlhgcy82dgg1m1cb")))

(define-public crate-opis-5.0.1 (c (n "opis") (v "5.0.1") (h "00fschqf6a2fch2zcvfv1g2fjcpm6jpq3wkj37crjgwkim5wwasl")))

(define-public crate-opis-5.0.2 (c (n "opis") (v "5.0.2") (h "1yl88d9lfyfsix1xc95vc8xx5fy28scxjkhndl441w74khg8skd0")))

(define-public crate-opis-5.1.0 (c (n "opis") (v "5.1.0") (h "1dv92hxjk1ia3a0xkczk1c3xmnv5vcrdz0pnwnx28qyswhipsxfg")))

(define-public crate-opis-5.2.0 (c (n "opis") (v "5.2.0") (h "0zrkghqxzgdyv6invqdinhanrfrvy7vfbpwa8g5qmh2vii88alkj")))

(define-public crate-opis-5.3.0 (c (n "opis") (v "5.3.0") (h "17ixqcgss119b3x3p1yxf33bgmli21bpfjarwycrgjnh7ym2i5mh")))

(define-public crate-opis-5.3.1 (c (n "opis") (v "5.3.1") (h "188zzp2kph0lfdgrrrjd3q69rlicxlnlsnfdbw8v57nrrsni4kvj")))

(define-public crate-opis-5.4.0 (c (n "opis") (v "5.4.0") (h "1mb60pxaypw9gri9wmlxqbgpa7qi3msijhvs679j6k0sfl7mxvw0")))

(define-public crate-opis-5.5.0 (c (n "opis") (v "5.5.0") (h "1lcx2svspkcw125hw1f9jshdcapx1j5d748mpv6xwdzqzqxg1a5y")))

(define-public crate-opis-5.6.0 (c (n "opis") (v "5.6.0") (h "034x4fm3divd16n4vvx5ghl5a8k1qj51575n94xjjpq1jf5qfwzp")))

(define-public crate-opis-5.7.0 (c (n "opis") (v "5.7.0") (h "16icz1shij7h3d4ph2ljm1bxqrcqsd8h4kzl2y29xbp5jq72wz4w")))

