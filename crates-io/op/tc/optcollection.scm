(define-module (crates-io op tc optcollection) #:use-module (crates-io))

(define-public crate-optcollection-0.1.0 (c (n "optcollection") (v "0.1.0") (d (list (d (n "maplit") (r "^1") (d #t) (k 2)))) (h "10s7g4c35fwq2m1xkfsm185dna6ry4hmi0lb4arlv91digqm2x70")))

(define-public crate-optcollection-0.1.1 (c (n "optcollection") (v "0.1.1") (d (list (d (n "maplit") (r "^1") (d #t) (k 2)))) (h "1v9lxip9kswywvaxvjligzy3bnvmp56cpf3dxhapaslz73p19ph0")))

(define-public crate-optcollection-0.1.2 (c (n "optcollection") (v "0.1.2") (d (list (d (n "maplit") (r "^1") (d #t) (k 2)))) (h "19w7lrfqazjgcdac7rn1z4qld1c0p1crvazfqm324hs6his7gjfa")))

(define-public crate-optcollection-0.1.3 (c (n "optcollection") (v "0.1.3") (d (list (d (n "maplit") (r "^1") (d #t) (k 2)))) (h "1ij23476afkja5hdfswqcqmi3dwcjxs4157ia4y4yqxv2y91n5km")))

(define-public crate-optcollection-0.1.4 (c (n "optcollection") (v "0.1.4") (d (list (d (n "maplit") (r "^1") (d #t) (k 2)))) (h "1wimngirxjvfpg1n0za85vgf6lagpcrkmgi3985qyhvj4hv0lfca")))

(define-public crate-optcollection-0.1.5 (c (n "optcollection") (v "0.1.5") (d (list (d (n "maplit") (r "^1") (d #t) (k 2)))) (h "1splgccaxq34llx3knxmwc7mb7qs4rc8alyx5d93a0ci8xp0pnl1")))

(define-public crate-optcollection-0.1.6 (c (n "optcollection") (v "0.1.6") (d (list (d (n "maplit") (r "^1") (d #t) (k 2)))) (h "18h7hr29wywqvvhkzn5rd04p6pqy9z6phnyv9i3yirgz7g9ainwr")))

