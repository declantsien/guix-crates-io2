(define-module (crates-io op ti optitag) #:use-module (crates-io))

(define-public crate-optitag-0.1.0 (c (n "optitag") (v "0.1.0") (h "1kf6r0jbf8xnf5gh5m9a73z65hglx197f47bn66357fa90l1nq1j")))

(define-public crate-optitag-0.2.0 (c (n "optitag") (v "0.2.0") (d (list (d (n "clap") (r "^4.2.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs") (r "^5.0.0") (d #t) (k 0)) (d (n "sled") (r "^0.34.7") (d #t) (k 0)))) (h "0xq905530g9qs1qpdyh2qfgid24cvs908wdg321wjq4clq0az04r")))

(define-public crate-optitag-0.2.1 (c (n "optitag") (v "0.2.1") (d (list (d (n "clap") (r "^4.2.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs") (r "^5.0.0") (d #t) (k 0)) (d (n "sled") (r "^0.34.7") (d #t) (k 0)))) (h "0l8f2yx7qccn56m03sjk5zs4fg8f7yrj2ja7raxrc4iy33har29h")))

(define-public crate-optitag-0.2.2 (c (n "optitag") (v "0.2.2") (d (list (d (n "clap") (r "^4.2.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs") (r "^5.0.0") (d #t) (k 0)) (d (n "sled") (r "^0.34.7") (d #t) (k 0)))) (h "0pwzk18v02znhzfv3nir065wjrxbhsi5ddp2r7yb0kkn27ajh2ik")))

(define-public crate-optitag-0.3.0 (c (n "optitag") (v "0.3.0") (d (list (d (n "clap") (r "^4.2.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs") (r "^5.0.0") (d #t) (k 0)) (d (n "sled") (r "^0.34.7") (d #t) (k 0)))) (h "0x9cinf73j57h425nkjs7jjyzr4d5pfhpi5h2m1cmr0fikdrb7dx")))

