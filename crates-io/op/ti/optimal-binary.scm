(define-module (crates-io op ti optimal-binary) #:use-module (crates-io))

(define-public crate-optimal-binary-0.0.0 (c (n "optimal-binary") (v "0.0.0") (d (list (d (n "num-traits") (r "^0.2.16") (d #t) (k 0)) (d (n "serde") (r "^1.0.185") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0rlc04kaqsqfiy14krcj4hyzl3mavqdapjab1fmpaz3q7k39k63a") (s 2) (e (quote (("serde" "dep:serde"))))))

