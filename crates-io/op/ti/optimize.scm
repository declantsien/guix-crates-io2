(define-module (crates-io op ti optimize) #:use-module (crates-io))

(define-public crate-optimize-0.1.0 (c (n "optimize") (v "0.1.0") (d (list (d (n "derive_builder") (r "^0.5") (d #t) (k 0)) (d (n "float-cmp") (r "^0.4") (d #t) (k 0)) (d (n "ndarray") (r "^0.11") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1cdcz64dvgh3r8iqjyvc6sbyk58y4zhdk3kbh1v4dn7zzcgqh53k")))

