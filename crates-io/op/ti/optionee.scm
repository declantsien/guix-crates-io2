(define-module (crates-io op ti optionee) #:use-module (crates-io))

(define-public crate-optionee-0.1.0 (c (n "optionee") (v "0.1.0") (h "1mbpbx2hr0d0ffblg4vmgw3awssx6s6rwqfylcjps6y4njygwz7l")))

(define-public crate-optionee-0.2.0 (c (n "optionee") (v "0.2.0") (d (list (d (n "anyhow") (r "^1") (k 0)))) (h "0gwa090cjm7zsw8g1b0y1j4g5b5b2iqgscnh8bbsj9ppmhan8lm0") (f (quote (("std" "anyhow/default") ("default" "std"))))))

