(define-module (crates-io op ti option-pricing) #:use-module (crates-io))

(define-public crate-option-pricing-0.1.0 (c (n "option-pricing") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1i45z94rx1hj4hlh414ky5iz1xx725j5vbp4i72hfinlymxg18px")))

(define-public crate-option-pricing-0.1.1 (c (n "option-pricing") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "10ryr72a914b8l5g1ypzfp5d5rqgq8l2r7sdp4isw7jh82jgvs20")))

(define-public crate-option-pricing-0.1.2 (c (n "option-pricing") (v "0.1.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1j9641sbk4rcjz6r004n2vlwxqz4l5b3bd2mbz3znfz3qcq48sxf")))

(define-public crate-option-pricing-0.1.3 (c (n "option-pricing") (v "0.1.3") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1pxswyf4s09d0bx4scqgjlx44gj4746vdpbrhy6pg83dgsjp32cy")))

