(define-module (crates-io op ti optimization) #:use-module (crates-io))

(define-public crate-optimization-0.0.1 (c (n "optimization") (v "0.0.1") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)) (d (n "env_logger") (r "^0.3") (d #t) (k 2)) (d (n "herbie-lint") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "10hcpaffjji1bn7rdki7xczdp82ahkf9f4mlhs9nxb4caawzran8") (f (quote (("unstable" "clippy" "herbie-lint"))))))

(define-public crate-optimization-0.1.0 (c (n "optimization") (v "0.1.0") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)) (d (n "env_logger") (r "^0.3") (d #t) (k 2)) (d (n "herbie-lint") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0ydbhfwzhk63rzxdw85dkkj1xbwq0hp1n0hyb795c1kzwncjpz30") (f (quote (("unstable" "clippy" "herbie-lint"))))))

(define-public crate-optimization-0.2.0 (c (n "optimization") (v "0.2.0") (d (list (d (n "env_logger") (r "^0.7") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "rand_distr") (r "^0.2") (d #t) (k 0)) (d (n "rand_pcg") (r "^0.2") (d #t) (k 0)))) (h "00csd8dr85vjygnp206drxlbcv2w0ipcpx52sgf9wald773zl8wb")))

