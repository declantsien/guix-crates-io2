(define-module (crates-io op ti optional-fields-serde-macro) #:use-module (crates-io))

(define-public crate-optional-fields-serde-macro-0.1.0 (c (n "optional-fields-serde-macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.3") (f (quote ("full"))) (d #t) (k 0)))) (h "0r57pd9qyri1p3nsm2p5aqlh2fv9hl5gf4hw7rnanbv56ffs7clb")))

(define-public crate-optional-fields-serde-macro-0.1.1 (c (n "optional-fields-serde-macro") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.3") (f (quote ("full"))) (d #t) (k 0)))) (h "13qakxfjwjs5mpzdp87ly60l8qdjs38l8qb98kvzlwi27abz87fk")))

(define-public crate-optional-fields-serde-macro-0.1.2 (c (n "optional-fields-serde-macro") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.3") (f (quote ("full"))) (d #t) (k 0)))) (h "0ifxcll3khq0rdg8bsjmbv4138a0zldcbbx6nz05a1i78h4zdrhw")))

