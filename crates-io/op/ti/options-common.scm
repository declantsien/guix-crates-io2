(define-module (crates-io op ti options-common) #:use-module (crates-io))

(define-public crate-options-common-0.1.0 (c (n "options-common") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "num-rational") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "ordered-float") (r "^2.0") (d #t) (k 0)) (d (n "statrs") (r "^0.13") (d #t) (k 0)))) (h "0bl9ixcq1r2b3wb48s3066idbbcyfxbxwb0gr8njzn8jxn363clf")))

(define-public crate-options-common-0.2.0 (c (n "options-common") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "num-rational") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "ordered-float") (r "^2.0") (d #t) (k 0)) (d (n "statrs") (r "^0.13") (d #t) (k 0)))) (h "090rq6gy144bzfv0q5xx2h4y20j4j0f3ryq97j6pyd2h8cyszrls")))

(define-public crate-options-common-0.3.0 (c (n "options-common") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "num-rational") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "ordered-float") (r "^2.0") (d #t) (k 0)) (d (n "statrs") (r "^0.13") (d #t) (k 0)))) (h "19sphhyk8crqmhhq19n0yb318khb3v19n6n9xsa6p1ylfxi93v93")))

(define-public crate-options-common-0.4.0 (c (n "options-common") (v "0.4.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "num-rational") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "ordered-float") (r "^2.0") (d #t) (k 0)) (d (n "statrs") (r "^0.13") (d #t) (k 0)))) (h "1r1s6hdygnmzcy1akj412rr6nqa7100gx9clg4hnc6x17cdm7gha")))

(define-public crate-options-common-0.4.1 (c (n "options-common") (v "0.4.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "num-rational") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "ordered-float") (r "^2.0") (d #t) (k 0)) (d (n "statrs") (r "^0.13") (d #t) (k 0)))) (h "0d8sgxq4baa1jr1df472m7qjms9shyjiwg43d9h2syyhlhahp3qm")))

(define-public crate-options-common-0.4.2 (c (n "options-common") (v "0.4.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "num-rational") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "ordered-float") (r "^2.0") (d #t) (k 0)) (d (n "statrs") (r "^0.13") (d #t) (k 0)))) (h "1199z06z8rg1gz7qllky3mm9c65hm173ysk9vmwhyjg1zp8rg8zq")))

(define-public crate-options-common-0.4.3 (c (n "options-common") (v "0.4.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "num-rational") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "ordered-float") (r "^2.0") (d #t) (k 0)) (d (n "statrs") (r "^0.13") (d #t) (k 0)))) (h "1656mh6x0gck7sp2vj7pdydqrcfk2w2rzzbxxrb548cd224y4afz")))

(define-public crate-options-common-0.5.0 (c (n "options-common") (v "0.5.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "num-rational") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "ordered-float") (r "^2.0") (d #t) (k 0)) (d (n "statrs") (r "^0.13") (d #t) (k 0)))) (h "1qz9k1nhxyh46hsl3mvncdq6mqyib1k3zmshf81lf3xhvhybw6xy")))

(define-public crate-options-common-0.5.1 (c (n "options-common") (v "0.5.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "num-rational") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "ordered-float") (r "^2.0") (d #t) (k 0)) (d (n "statrs") (r "^0.13") (d #t) (k 0)))) (h "0dsmr5v7g5yrz5qs2j63d4ckjf993rhfyd4lpcp707adn7l7m3y6")))

(define-public crate-options-common-0.6.0 (c (n "options-common") (v "0.6.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "enum_dispatch") (r "^0.3") (d #t) (k 0)) (d (n "num-rational") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "ordered-float") (r "^2.0") (d #t) (k 0)) (d (n "statrs") (r "^0.13") (d #t) (k 0)))) (h "1qipva67bbykf9213afq7d5hkpr8hrqhxqkdkfqi81585d5fisbf")))

(define-public crate-options-common-0.7.0 (c (n "options-common") (v "0.7.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "enum_dispatch") (r "^0.3") (d #t) (k 0)) (d (n "num-rational") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "ordered-float") (r "^2.0") (d #t) (k 0)) (d (n "statrs") (r "^0.13") (d #t) (k 0)))) (h "0xqa8bglc1wg8gyi03bnaa3zgjaypc2pvqlff9c5lf55hwx56c1r")))

(define-public crate-options-common-0.7.1 (c (n "options-common") (v "0.7.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "enum_dispatch") (r "^0.3") (d #t) (k 0)) (d (n "num-rational") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "ordered-float") (r "^2.0") (d #t) (k 0)) (d (n "statrs") (r "^0.13") (d #t) (k 0)))) (h "09a56winj1zvn8vranlqva00vnlydw7hc6s1lchzi7mhha6xrg3l")))

(define-public crate-options-common-0.8.0 (c (n "options-common") (v "0.8.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "enum_dispatch") (r "^0.3") (d #t) (k 0)) (d (n "num-rational") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "ordered-float") (r "^2.0") (d #t) (k 0)) (d (n "statrs") (r "^0.13") (d #t) (k 0)))) (h "0kzrliql3rih18yj51g30zjlsfii6zaphpj6haclgp3wn94dd1nz")))

