(define-module (crates-io op ti options) #:use-module (crates-io))

(define-public crate-options-0.0.1 (c (n "options") (v "0.0.1") (h "0l7r6jh32imc1riphzrlb10v8gqsn7cdw6ahvacs5mbfw139kkyg")))

(define-public crate-options-0.1.0 (c (n "options") (v "0.1.0") (h "1q03c6ykfd8gsx3rd24rmiwrs3yglli6afh0h9vdv05zd4y6appa")))

(define-public crate-options-0.2.0 (c (n "options") (v "0.2.0") (h "070xqxymxj4jgrriv6g86p6gbb7ai5x0l2ajiqpw6zzd0s0pgg01")))

(define-public crate-options-0.3.0 (c (n "options") (v "0.3.0") (h "0f4j2canc74gkq9m4fajz028gj70z6q5x6zq7bicarjd2p74wflh")))

(define-public crate-options-0.3.1 (c (n "options") (v "0.3.1") (h "04kpw71hkkzk9kxcby18vz882pm93nddn4469yzbxwkzbn1837ss")))

(define-public crate-options-0.3.2 (c (n "options") (v "0.3.2") (h "020203js682y2ab9xyfam770y6n8c5js8n83yrh1l39a8sg2cv8b")))

(define-public crate-options-0.4.0 (c (n "options") (v "0.4.0") (h "1wldvqp8la0i01lf5va6nzawc4cqsnbwfnyycbcnhfin6phs8s5f")))

(define-public crate-options-0.5.0 (c (n "options") (v "0.5.0") (h "14706p0kx81dm5wan4qznj2q1y1cpciysqcg7xb1v0kfw4fphq28")))

(define-public crate-options-0.5.1 (c (n "options") (v "0.5.1") (h "1c2cy6gdvlzfqvf6vha5m6sajrifhpc8qm70ir39v3ihg0n6dbkw")))

(define-public crate-options-0.5.2 (c (n "options") (v "0.5.2") (h "05m98xsyi7dm2yczfbfycm123ir4jdlca9ii9xsq4ca55jqwk8b1")))

(define-public crate-options-0.5.3 (c (n "options") (v "0.5.3") (h "0hbycsg3hdxzzjfxzvbpfskk08lzs2bxby8lm24dfik70sd066h1")))

