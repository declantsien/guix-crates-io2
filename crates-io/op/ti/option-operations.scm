(define-module (crates-io op ti option-operations) #:use-module (crates-io))

(define-public crate-option-operations-0.1.0 (c (n "option-operations") (v "0.1.0") (h "01bm4mjpp2q3g04qswzx95ppnbn5612x8jwfjswg3va5g4hrmcms") (f (quote (("std") ("default" "std"))))))

(define-public crate-option-operations-0.2.0 (c (n "option-operations") (v "0.2.0") (h "01606g6blgnb3z2sfz8l592cznqlfb9rd3zv2lx8v2qpl06vly4b") (f (quote (("std") ("default" "std"))))))

(define-public crate-option-operations-0.3.0 (c (n "option-operations") (v "0.3.0") (h "16vwawgvs8v5wgyhlprwccmmzy2fwbl0l0hgbvd4z5l5019l1g1z") (f (quote (("std") ("default" "std"))))))

(define-public crate-option-operations-0.4.0 (c (n "option-operations") (v "0.4.0") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)))) (h "0izl2qn8l2cm6w2vp37jwq7f8scpa4kp944m267jc6zl2ls13mlm") (f (quote (("std") ("default" "std"))))))

(define-public crate-option-operations-0.4.1 (c (n "option-operations") (v "0.4.1") (d (list (d (n "paste") (r "^1.0.6") (d #t) (k 0)))) (h "18jhy5sc56gwdvqc3asr6az685zc9zkgv8p8n69s94bcj6bibc22") (f (quote (("std") ("default" "std")))) (r "1.53")))

(define-public crate-option-operations-0.5.0 (c (n "option-operations") (v "0.5.0") (d (list (d (n "paste") (r "^1.0.6") (d #t) (k 0)))) (h "1l13n9487gk6063zzjdwzqbig78n8mh6dxsbiq9nbaxfn5xx49kw") (f (quote (("std") ("default" "std")))) (r "1.53")))

