(define-module (crates-io op ti option-symbology) #:use-module (crates-io))

(define-public crate-option-symbology-0.1.0 (c (n "option-symbology") (v "0.1.0") (d (list (d (n "fancy-regex") (r "^0.5") (d #t) (k 0)))) (h "0scdsqalkhnkps9ffykf8m89941zx2ja08cdm781j3pf1yq581km") (y #t)))

(define-public crate-option-symbology-0.1.1 (c (n "option-symbology") (v "0.1.1") (d (list (d (n "fancy-regex") (r "^0.5") (d #t) (k 0)))) (h "1p0wzyi8mhyzrzpsr4kdh9msfyx2mi0bqjl90awmrwv22wvii05n")))

(define-public crate-option-symbology-0.1.2 (c (n "option-symbology") (v "0.1.2") (d (list (d (n "fancy-regex") (r "^0.6.0") (d #t) (k 0)) (d (n "strum") (r "^0.21.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.21") (d #t) (k 0)))) (h "02bp66kca8jkml836lwm023npphh9q1zv0iqbzs7fvccdl57kxlf")))

(define-public crate-option-symbology-0.2.0 (c (n "option-symbology") (v "0.2.0") (d (list (d (n "fancy-regex") (r "^0.6.0") (d #t) (k 0)) (d (n "strum") (r "^0.21.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.21") (d #t) (k 0)))) (h "036ac48awzx2d6lfq92pkx6z43likxik1bzn31qh9k7pgaydpbn1")))

