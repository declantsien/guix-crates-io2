(define-module (crates-io op ti optional-log) #:use-module (crates-io))

(define-public crate-optional-log-0.1.0 (c (n "optional-log") (v "0.1.0") (h "0f84krahv670z3lyzk9z1mrlx9x3s8kd4r2sk0lw6qyipbnr2r1q") (y #t)))

(define-public crate-optional-log-0.1.1 (c (n "optional-log") (v "0.1.1") (h "180lx08wv03rslmwpg67cg8ppwl6f37q8h6ivd94f77s211mrhw2") (y #t)))

(define-public crate-optional-log-0.1.2 (c (n "optional-log") (v "0.1.2") (h "0572ay09wjvv3qviwqxv9iwz3a665cnby5l9jpmgazlmjndsn5d1") (f (quote (("log") ("default"))))))

