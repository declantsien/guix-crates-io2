(define-module (crates-io op ti optimax) #:use-module (crates-io))

(define-public crate-optimax-0.1.3 (c (n "optimax") (v "0.1.3") (d (list (d (n "finitediff") (r "^0.1.4") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.3") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "ndarray-linalg") (r "^0.16.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 0)))) (h "07qlya0za1ih3ib6n5igrfbnqydz2a2p7gis9r9dgh7m1wr67m5i")))

