(define-module (crates-io op ti optimigation) #:use-module (crates-io))

(define-public crate-optimigation-0.1.0 (c (n "optimigation") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "text-colorizer") (r "^1") (d #t) (k 0)))) (h "10j1s7vl2r0w5k246w2lzha2s7c29lkj52vp3wk0vzn3mxxp8l0w")))

