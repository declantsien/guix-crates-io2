(define-module (crates-io op ti option-block) #:use-module (crates-io))

(define-public crate-option-block-0.1.0 (c (n "option-block") (v "0.1.0") (h "1g6p294qszbg6w1z33rcnr3qkib7y9n29vrfaw06xi4bl6884b0z") (y #t)))

(define-public crate-option-block-0.2.0 (c (n "option-block") (v "0.2.0") (h "1a60zpavshjxwknvhzr7665x5lhrvvxji32lhp32y6z44wk9468x")))

(define-public crate-option-block-0.2.1 (c (n "option-block") (v "0.2.1") (h "0fw5bz4gzj3qxbh6b85fkv9w0gb5gi3nhg255wwaai9lkqbny7hr")))

(define-public crate-option-block-0.2.2 (c (n "option-block") (v "0.2.2") (h "0ipi79s3g7s2jbj6w61y6hf62hdaxvnvs7d10f5z6i0k55arqimw")))

(define-public crate-option-block-0.3.0 (c (n "option-block") (v "0.3.0") (h "09rjxxz4zj3i4fbsbf9fkw2z0mbr8f7sccmhr3bi8sjr8p9wbwp0")))

