(define-module (crates-io op ti optimal-core) #:use-module (crates-io))

(define-public crate-optimal-core-0.0.0 (c (n "optimal-core") (v "0.0.0") (d (list (d (n "blanket") (r "^0.3.0") (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 2)) (d (n "replace_with") (r "^0.1.7") (d #t) (k 2)) (d (n "serde") (r "^1.0.185") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 2)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 2)) (d (n "streaming-iterator") (r "^0.1.9") (d #t) (k 0)))) (h "0igyy158qwqz41s28y8dsaaf68q5vgsl4jy8xhi19vqxa8dzwz14")))

