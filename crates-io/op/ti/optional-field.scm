(define-module (crates-io op ti optional-field) #:use-module (crates-io))

(define-public crate-optional-field-0.1.1 (c (n "optional-field") (v "0.1.1") (d (list (d (n "optional-fields-serde-macro") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "0kgl8rivvrlfgcq26m1nvnc1fjnr7z4d7k3xm5z15zfqx29vszan") (f (quote (("default" "serde"))))))

(define-public crate-optional-field-0.1.2 (c (n "optional-field") (v "0.1.2") (d (list (d (n "optional-fields-serde-macro") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "0c25x4lz1zmj0x69rl9dmhbs8h35fqwa4qm71347s7xlrygr4iag") (f (quote (("default" "serde"))))))

(define-public crate-optional-field-0.1.3 (c (n "optional-field") (v "0.1.3") (d (list (d (n "optional-fields-serde-macro") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "09vha7dld0xv7p536nvqmgy9fkqghg2rk118khmq688yz2b7yijr") (f (quote (("default" "serde"))))))

(define-public crate-optional-field-0.1.4 (c (n "optional-field") (v "0.1.4") (d (list (d (n "optional-fields-serde-macro") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1vgh0rzlc14zhbz3wa85fy4fnhk8z0g69kmw8g6n0zdcimq77zix") (f (quote (("default" "serde"))))))

(define-public crate-optional-field-0.1.5 (c (n "optional-field") (v "0.1.5") (d (list (d (n "optional-fields-serde-macro") (r "^0.1.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1s9dlvy1qfqygq2mzwrgbpgajplj0i3sf5rgv80ar1vy7lz8325r") (f (quote (("default" "serde"))))))

(define-public crate-optional-field-0.1.6 (c (n "optional-field") (v "0.1.6") (d (list (d (n "optional-fields-serde-macro") (r "^0.1.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1cl1ycv51yk0dyrr1yrlkysr6my1281c3cx14s6y4mmn9xma8jn1") (f (quote (("default" "serde"))))))

