(define-module (crates-io op ti option-constructor-derive) #:use-module (crates-io))

(define-public crate-option-constructor-derive-0.1.0 (c (n "option-constructor-derive") (v "0.1.0") (d (list (d (n "quote") (r "^0.3.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "1yvw7l24hn0l5c74mxw5masw35czkjfap268zjw9a2v1lakhg23n")))

(define-public crate-option-constructor-derive-0.1.1 (c (n "option-constructor-derive") (v "0.1.1") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "1y1h9ykdbi7a0rq7njf5n0g5y3ippvv6igi2wwqfigr7llfar3yq")))

