(define-module (crates-io op ti optimus) #:use-module (crates-io))

(define-public crate-optimus-0.1.0 (c (n "optimus") (v "0.1.0") (d (list (d (n "modinverse") (r "^0.1.1") (d #t) (k 0)) (d (n "primal-check") (r "^0.3.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.39") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1xsan5lk4zwfavx4jq4q9dzi2i5haz63i60xlabbxi930gnx4rsl")))

(define-public crate-optimus-0.2.0 (c (n "optimus") (v "0.2.0") (d (list (d (n "modinverse") (r "^0.1.1") (d #t) (k 0)) (d (n "primal-check") (r "^0.3.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.39") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "13jnxdr7n6dcqfqabiy8kzklznhbr50b9j7fylzbr0l93jy547wm")))

(define-public crate-optimus-0.2.1 (c (n "optimus") (v "0.2.1") (d (list (d (n "modinverse") (r "^0.1.1") (d #t) (k 0)) (d (n "primal-check") (r "^0.3.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.39") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1ww13gz4hpw2bz5rim6h222j6w0cpvw22ng7li0sajc5m9cw6kpk")))

