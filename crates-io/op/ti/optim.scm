(define-module (crates-io op ti optim) #:use-module (crates-io))

(define-public crate-optim-0.1.0 (c (n "optim") (v "0.1.0") (h "14q3pwbf73kj4gvmgz2ci4371l3iqwasdgy5z2zcajig4fmxa4x4") (y #t)))

(define-public crate-optim-0.2.0 (c (n "optim") (v "0.2.0") (h "0hp8a1qb2h8ql8i9aim7lqxyiqzi50fp76flij7fn2knaja9bf1v")))

