(define-module (crates-io op ti optimal-transport) #:use-module (crates-io))

(define-public crate-optimal-transport-0.1.0 (c (n "optimal-transport") (v "0.1.0") (h "0q05amvmw5crhm0d1vq8431y9fghj04b1dgf4ayn0r91y3xs6l9v")))

(define-public crate-optimal-transport-0.2.0 (c (n "optimal-transport") (v "0.2.0") (d (list (d (n "float-ord") (r "^0.2.0") (d #t) (k 0)) (d (n "mcmf") (r "^2.0.0") (d #t) (k 0)) (d (n "ndarray") (r "^0.13.1") (d #t) (k 0)) (d (n "numpy") (r "^0.11.0") (d #t) (k 0)) (d (n "pyo3") (r "^0.11.1") (d #t) (k 0)))) (h "08fwzggqhpwvm4gagj7f55hm8lgvy0alnpvxr57zdx2fzrmfay1m")))

