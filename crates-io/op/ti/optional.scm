(define-module (crates-io op ti optional) #:use-module (crates-io))

(define-public crate-optional-0.0.1 (c (n "optional") (v "0.0.1") (h "0dzqzhlask50zqmnpc9r58asyczr0q90i1gpmj48bj8ksx3bk4dn")))

(define-public crate-optional-0.0.2 (c (n "optional") (v "0.0.2") (h "0jkbvbqh51scb4ddby79vi09dzh3si6hqbmmq3himf1jmhw7277i")))

(define-public crate-optional-0.0.3 (c (n "optional") (v "0.0.3") (h "1cw41s6rvp9wmz7lf5i8sgpgjzwmb189gxc6alyh7waq8aqnps29")))

(define-public crate-optional-0.0.4 (c (n "optional") (v "0.0.4") (h "1csgd56y5shi6lqydfd8l11ym5j7pl5a6pgnzrvm7xzi2i7abxln")))

(define-public crate-optional-0.0.6 (c (n "optional") (v "0.0.6") (h "1y6cyvk7zkddhg5c88wbvlv2a2fb007m5c9hhz293lzcw6ra61rl") (f (quote (("unstable"))))))

(define-public crate-optional-0.0.7 (c (n "optional") (v "0.0.7") (h "16hp6hwlan0wyd8k1plmpymlcdal3ahqpmrv9r8jrxz5dc76m1yz") (f (quote (("unstable"))))))

(define-public crate-optional-0.0.8 (c (n "optional") (v "0.0.8") (h "1dxmd128qsp656k9h5qvl2ai0l7lrkqs5lmq0j9l1iv27b56x63a") (f (quote (("unstable"))))))

(define-public crate-optional-0.0.10 (c (n "optional") (v "0.0.10") (h "1ny92a0lsr1l4cvgch6yw3g4c3r0y4px01iqzii2v86bsafxvc7y") (f (quote (("unstable"))))))

(define-public crate-optional-0.0.11 (c (n "optional") (v "0.0.11") (h "0jw3a6shik7a5pvcq7xqv6clyfzic3fyi36y54gy1hw5wr3wm9d6") (f (quote (("unstable"))))))

(define-public crate-optional-0.0.12 (c (n "optional") (v "0.0.12") (h "14h02wspbqxz72b731c934bb26pc236anw7qk0ig995gjdzzqwmz") (f (quote (("unstable"))))))

(define-public crate-optional-0.0.13 (c (n "optional") (v "0.0.13") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)))) (h "1qwlqijgkkdbdxladigqryb4s3v1sg0pckndmrmhfvxckd2a5cns") (f (quote (("unstable"))))))

(define-public crate-optional-0.1.0 (c (n "optional") (v "0.1.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0.8") (o #t) (d #t) (k 0)))) (h "00n5dbxvjm7w00ldysc0xjjlabk791g8w1xk70ql9z8847n8madh") (f (quote (("unstable"))))))

(define-public crate-optional-0.2.0 (c (n "optional") (v "0.2.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0.8") (o #t) (d #t) (k 0)))) (h "0dlkrx7dd8h5l89ffmg1zqh19i0b721vc2slidvbssg3badz6f50") (f (quote (("unstable"))))))

(define-public crate-optional-0.3.0 (c (n "optional") (v "0.3.0") (d (list (d (n "bencher") (r "^0.1.4") (d #t) (k 2)) (d (n "serde") (r "^1.0.12") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.3") (d #t) (k 2)))) (h "0l5p9y35cv539jjbjj7aivms28samciwlb8zzzyw5807lyd7p4ik") (f (quote (("unstable")))) (y #t)))

(define-public crate-optional-0.4.0 (c (n "optional") (v "0.4.0") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "serde") (r "^1.0.42") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.16") (d #t) (k 2)))) (h "09zcxr625fk9iwzmihfb349f8nic2506p32cbih980x4gy9idj6g") (f (quote (("unstable"))))))

(define-public crate-optional-0.4.1 (c (n "optional") (v "0.4.1") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "serde") (r "^1.0.43") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.16") (d #t) (k 2)))) (h "0jjsc48f3ckvwipkf55w825y4ic3fv4wqpajr8p1gqvz7plgapzg") (f (quote (("unstable"))))))

(define-public crate-optional-0.4.2 (c (n "optional") (v "0.4.2") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "serde") (r "^1.0.43") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.16") (d #t) (k 2)))) (h "0ppyrim714c3j1x1fmc0k8jl9ayjcligi531w3sk3c889rv8qb4y") (f (quote (("unstable"))))))

(define-public crate-optional-0.4.3 (c (n "optional") (v "0.4.3") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "serde") (r "^1.0.43") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.16") (d #t) (k 2)))) (h "091s599dk204087j8fck1x7vnjdawm2vcrf1kc8978s6cvhnysg3") (f (quote (("unstable"))))))

(define-public crate-optional-0.5.0 (c (n "optional") (v "0.5.0") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "serde") (r "^1.0.80") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.33") (d #t) (k 2)))) (h "1z508c38khix5fx7mzl11mwwkjl77q4n764jfjns8g2xb2aa92lp") (f (quote (("unstable"))))))

