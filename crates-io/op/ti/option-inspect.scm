(define-module (crates-io op ti option-inspect) #:use-module (crates-io))

(define-public crate-option-inspect-0.1.0 (c (n "option-inspect") (v "0.1.0") (h "0rr02z5yfprj35yp745kw14bs26fjkqhjcz8dc06chkdn9cqw3bv")))

(define-public crate-option-inspect-0.2.0 (c (n "option-inspect") (v "0.2.0") (h "17aqbf6qg6p55j4rvng6a7c3ia90f3awnj3wc8q12h3gqszvl0gq")))

(define-public crate-option-inspect-0.3.0 (c (n "option-inspect") (v "0.3.0") (h "1xc3aqlaqw11csrl5ad5wqa1m6rsvkpcpmfig8ks28szn8ivsczi")))

