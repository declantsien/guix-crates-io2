(define-module (crates-io op ti optick) #:use-module (crates-io))

(define-public crate-optick-1.3.1 (c (n "optick") (v "1.3.1") (h "1bhy88rxmj9sg4yk4cyl0pycal8pc7n8inh6z67xs6lfczx9000l") (f (quote (("enable") ("default" "enable"))))))

(define-public crate-optick-1.3.2 (c (n "optick") (v "1.3.2") (h "05rmqy73ws6ksshn5m0wb42zpnxcjy2hdhrpika1n6hirf198ng5") (f (quote (("enable") ("default" "enable"))))))

(define-public crate-optick-1.3.3 (c (n "optick") (v "1.3.3") (h "01czn70q17m5vxmdmgc3sjhc81yhg9ifivx5jdvfzhagk682ph7b") (f (quote (("enable") ("default" "enable"))))))

(define-public crate-optick-1.3.4 (c (n "optick") (v "1.3.4") (h "0j35dj8ggfpcc399h1ljm6xfz8kszqc4nrw3vcl9kfndd1hapryp") (f (quote (("enable") ("default" "enable"))))))

