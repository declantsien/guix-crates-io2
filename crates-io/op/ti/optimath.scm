(define-module (crates-io op ti optimath) #:use-module (crates-io))

(define-public crate-optimath-0.0.0 (c (n "optimath") (v "0.0.0") (d (list (d (n "packed_simd") (r "^0.3") (d #t) (k 0)))) (h "109vx4xv2clxpslg7mnc1apm213hz9qd7hinnidgcm5j3g3z0hlv")))

(define-public crate-optimath-0.1.0 (c (n "optimath") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "13wq5g5yy8kfm2ashvd0r2fl47jcivrnqmjm4yp6hc553gqfs9v3")))

(define-public crate-optimath-0.2.0 (c (n "optimath") (v "0.2.0") (d (list (d (n "bincode") (r "^1.2") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.7") (o #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (k 0)))) (h "06m6rxdimbzc3zxs5d72q34z7as5fnypr93121ksm3g26xv3bzdj") (f (quote (("default" "serde" "rand") ("alloc"))))))

(define-public crate-optimath-0.3.0 (c (n "optimath") (v "0.3.0") (d (list (d (n "bincode") (r "^1.2") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.7") (o #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (k 0)))) (h "006bnqhk4q54y3vzlkqfzh8k0byimx807zd076pzjzm2g2wr00fw") (f (quote (("default" "serde" "rand") ("alloc"))))))

(define-public crate-optimath-0.3.1 (c (n "optimath") (v "0.3.1") (d (list (d (n "bincode") (r "^1.2") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.7") (o #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (k 0)))) (h "1bq6nfxz8aciw241i8babh96bvl0zyw18d2f7nwx008vhfzc1icn") (f (quote (("default" "serde" "rand") ("alloc"))))))

