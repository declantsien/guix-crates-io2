(define-module (crates-io op ti optional_struct_internal) #:use-module (crates-io))

(define-public crate-optional_struct_internal-0.3.0 (c (n "optional_struct_internal") (v "0.3.0") (h "1dp7a9fsq1hngkzfbd0lrr4r0cvvqijhhwgr5hmm1gij3jl4jnss")))

(define-public crate-optional_struct_internal-0.3.1 (c (n "optional_struct_internal") (v "0.3.1") (h "0kczkq9bs9550spx2b3rxz4bh1cfga97pp6s6x3r6g6zq378kqv4")))

