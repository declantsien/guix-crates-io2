(define-module (crates-io op ti options_results) #:use-module (crates-io))

(define-public crate-options_results-0.1.0 (c (n "options_results") (v "0.1.0") (h "0rgqzj393ikbjsjd0q2raqcnkv26wiqmsgy6jq0rgljvi31xrm6w") (y #t)))

(define-public crate-options_results-0.1.1 (c (n "options_results") (v "0.1.1") (h "1aijc7i0grf68wic7b2i0b1gv15sf90qp1gsirhgvz2nwd5c62k4")))

