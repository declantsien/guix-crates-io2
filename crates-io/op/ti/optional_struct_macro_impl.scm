(define-module (crates-io op ti optional_struct_macro_impl) #:use-module (crates-io))

(define-public crate-optional_struct_macro_impl-0.3.0 (c (n "optional_struct_macro_impl") (v "0.3.0") (d (list (d (n "optional_struct_internal") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.49") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("parsing" "proc-macro"))) (d #t) (k 0)))) (h "0ahd3x4l5y3dfhk34z95aqqkkjq7g33k50bil19jm225yxz7drh5")))

(define-public crate-optional_struct_macro_impl-0.3.1 (c (n "optional_struct_macro_impl") (v "0.3.1") (d (list (d (n "optional_struct_internal") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.49") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("parsing" "proc-macro"))) (d #t) (k 0)))) (h "17qgr0m5jbllpyg4vn8qdkz0xg7ylb5fx2dyzgahk5q4j8fb2v98")))

(define-public crate-optional_struct_macro_impl-0.3.2 (c (n "optional_struct_macro_impl") (v "0.3.2") (d (list (d (n "proc-macro2") (r "^1.0.70") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (d #t) (k 0)))) (h "14px5w2r1wa7crhxnqgi98d7pb7pzr8wscknbyym0gjhyhklqcgk")))

(define-public crate-optional_struct_macro_impl-0.4.0 (c (n "optional_struct_macro_impl") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0.70") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (d #t) (k 0)))) (h "1imspj6k5abpc4gv4viqq9lkawwwalzbx8zglgzlidhzmilp0wbb")))

(define-public crate-optional_struct_macro_impl-0.4.1 (c (n "optional_struct_macro_impl") (v "0.4.1") (d (list (d (n "proc-macro2") (r "^1.0.70") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (d #t) (k 0)))) (h "1vvmsh8n2a9iw65zy27pi5afz1z8vgni9lnmcsmx0p8r5l1dd3kd")))

