(define-module (crates-io op ti option-lock) #:use-module (crates-io))

(define-public crate-option-lock-0.1.0 (c (n "option-lock") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "07kqsb6mkx52ycsx8xi6x3k5k8si16k35h2jcm5nw28x8ysp7as5") (f (quote (("std") ("default" "std"))))))

(define-public crate-option-lock-0.2.0 (c (n "option-lock") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3") (o #t) (d #t) (k 0)))) (h "0h5276qzspazpmqh6nh87pr6rrjwi06srjkbbxpgfkzxhqqnvirx") (f (quote (("std") ("default" "std") ("bench" "criterion"))))))

(define-public crate-option-lock-0.2.1 (c (n "option-lock") (v "0.2.1") (d (list (d (n "criterion") (r "^0.3") (o #t) (d #t) (k 0)))) (h "1ar7hfyps1azkixic93fqfwkhbq8bhwdzx3769qp3v8052y8iqi7") (f (quote (("std") ("default" "std") ("bench" "criterion"))))))

(define-public crate-option-lock-0.3.0 (c (n "option-lock") (v "0.3.0") (d (list (d (n "criterion") (r "^0.3") (o #t) (d #t) (k 0)))) (h "0lzkfv6k9vy0slfpdv2ab3xj5glb72jrzxm72rxcgkalg8r1kmnz") (f (quote (("std" "alloc") ("default" "std") ("bench" "criterion") ("alloc"))))))

(define-public crate-option-lock-0.3.1 (c (n "option-lock") (v "0.3.1") (d (list (d (n "criterion") (r "^0.3") (o #t) (d #t) (k 0)))) (h "1s1xlphq6h06dmymp7wy962xr07kndaa81kc25pg69wj36c3fg5p") (f (quote (("std" "alloc") ("default" "std") ("bench" "criterion") ("alloc"))))))

