(define-module (crates-io op ti optimistic_lock_coupling) #:use-module (crates-io))

(define-public crate-optimistic_lock_coupling-0.1.0 (c (n "optimistic_lock_coupling") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "05g6fhhvqaf5ivsq8qmzv9gw5v9mxmqapkpzalbn0liwxwcn0skf")))

(define-public crate-optimistic_lock_coupling-0.2.0 (c (n "optimistic_lock_coupling") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0m8645v5kk9v6bdcj95z55n3fyc7nsp89wpq5a97ddyhmi2f798s")))

(define-public crate-optimistic_lock_coupling-0.2.5 (c (n "optimistic_lock_coupling") (v "0.2.5") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1mgshg1kyddjpsnkb2pfhz6q8padricad5qanwkszzzxiq7r6gz8")))

