(define-module (crates-io op ti optimal) #:use-module (crates-io))

(define-public crate-optimal-0.0.0 (c (n "optimal") (v "0.0.0") (d (list (d (n "optimal-core") (r "^0.0.0") (d #t) (k 0)) (d (n "optimal-pbil") (r "^0.0.0") (d #t) (k 0)) (d (n "optimal-steepest") (r "^0.0.0") (d #t) (k 0)) (d (n "rand_xoshiro") (r "^0.6.0") (d #t) (k 0)))) (h "0r409as4yjq9ibic3mfl8fxdsms4rl43iwjzphibd3cqa43h414l")))

