(define-module (crates-io op ti option_trait) #:use-module (crates-io))

(define-public crate-option_trait-0.1.0 (c (n "option_trait") (v "0.1.0") (h "1z77wbg0czhakg920z4ynf9567yykpck3ysn61kf5h3y74j5g8px")))

(define-public crate-option_trait-0.1.1 (c (n "option_trait") (v "0.1.1") (h "0bfl6q0favzimlcf9wl1ckrzpbg871b4s1p5rxqiiqbk7sprdc7f")))

(define-public crate-option_trait-0.1.2 (c (n "option_trait") (v "0.1.2") (h "08ywbpqfvhg2mf43vz5ipk07rx5nc7414nv9d0zr9yd3bw1pqjww")))

(define-public crate-option_trait-0.1.3 (c (n "option_trait") (v "0.1.3") (h "1rjwg0xmsmndc6b8xs3f4a315q7n6d22n5dlm9lc97sp93gr3k8f")))

(define-public crate-option_trait-0.1.4 (c (n "option_trait") (v "0.1.4") (d (list (d (n "moddef") (r "^0.2.2") (d #t) (k 0)))) (h "0wqrqwg57x5s5ifvmzw6x7c7b39xqhy2djyky8vz8mix03q7dzxa")))

(define-public crate-option_trait-0.1.5 (c (n "option_trait") (v "0.1.5") (d (list (d (n "moddef") (r "^0.2.2") (d #t) (k 0)))) (h "0yidv9x992wk9b6y1c9w0pga5mkjc75pimi5vg3464kh547p0kvk")))

(define-public crate-option_trait-0.1.6 (c (n "option_trait") (v "0.1.6") (d (list (d (n "moddef") (r "^0.2.2") (d #t) (k 0)))) (h "0ym4wsz4nyvlgx26q4bzmpx9w9409x9cfwgfckg4kzgcsywggg6z")))

(define-public crate-option_trait-0.1.7 (c (n "option_trait") (v "0.1.7") (d (list (d (n "moddef") (r "^0.2.2") (d #t) (k 0)))) (h "1p03zm7ch7h3vs44cngqf97jlv6qnahpxan18fpbv9s5g3jrpzw9")))

(define-public crate-option_trait-0.1.8 (c (n "option_trait") (v "0.1.8") (d (list (d (n "moddef") (r "^0.2.2") (d #t) (k 0)))) (h "015vwdzia8596km4cny4scy7ng02bwq5h8nbmwdsijxw3lz935mw")))

(define-public crate-option_trait-0.1.9 (c (n "option_trait") (v "0.1.9") (d (list (d (n "moddef") (r "^0.2.2") (d #t) (k 0)))) (h "1rb5mhrf3aq0a5vhi5z0hfxb5yd70xvgm45gs8zzh3j60r1bpfpp")))

(define-public crate-option_trait-0.1.10 (c (n "option_trait") (v "0.1.10") (d (list (d (n "moddef") (r "^0.2.2") (d #t) (k 0)))) (h "09lw3plfjpk1s5msqvaqna6q1bf97q28sj8lbqjx0jzl3yzsb92l")))

(define-public crate-option_trait-0.1.11 (c (n "option_trait") (v "0.1.11") (d (list (d (n "moddef") (r "^0.2.2") (d #t) (k 0)))) (h "11x1mdvq76ssgckzsyvw0rmnslgigdxmw9bv0fp62bdabbr7wga9")))

(define-public crate-option_trait-0.1.12 (c (n "option_trait") (v "0.1.12") (d (list (d (n "moddef") (r "^0.2.2") (d #t) (k 0)))) (h "1alnfgypy8xz91c5p7ixk4s5hv6a0yv82bvy06b3dnqrg651d5l5")))

(define-public crate-option_trait-0.1.13 (c (n "option_trait") (v "0.1.13") (d (list (d (n "moddef") (r "^0.2.2") (d #t) (k 0)))) (h "1da8957arr2h3kqixm3v8v7xbml5hkq6j9qby8avs9lwpqnph3mp")))

(define-public crate-option_trait-0.1.14 (c (n "option_trait") (v "0.1.14") (d (list (d (n "moddef") (r "^0.2.2") (d #t) (k 0)))) (h "1arixdsgva16vay1dhinrclcrykzgvv4rmidlxrq23ghjy9b39w0")))

(define-public crate-option_trait-0.1.15 (c (n "option_trait") (v "0.1.15") (d (list (d (n "moddef") (r "^0.2.2") (d #t) (k 0)))) (h "1h9yd7jp7pp2k9pgmnsx4j8sqnjp2khz167rr1wp0r9yddvf70r1")))

(define-public crate-option_trait-0.1.16 (c (n "option_trait") (v "0.1.16") (d (list (d (n "moddef") (r "^0.2.2") (d #t) (k 0)))) (h "1s86ywnlg1q23s44f4wh57ny06iqwnqyy2s86q21mfbcxrn5i277")))

