(define-module (crates-io op ti optifi-usdc-cpi) #:use-module (crates-io))

(define-public crate-optifi-usdc-cpi-0.1.0 (c (n "optifi-usdc-cpi") (v "0.1.0") (d (list (d (n "anchor-gen") (r "^0.3.0") (d #t) (k 0)) (d (n "anchor-lang") (r ">=0.20") (d #t) (k 0)))) (h "0jd06k5w0h5kv5bq5x2w483s9fffzcdip72f40zk46zppzdvv3yl") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("devnet" "no-entrypoint") ("default" "cpi") ("cpi" "no-entrypoint"))))))

