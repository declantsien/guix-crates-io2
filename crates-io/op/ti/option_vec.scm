(define-module (crates-io op ti option_vec) #:use-module (crates-io))

(define-public crate-option_vec-0.1.0 (c (n "option_vec") (v "0.1.0") (h "1jjgcxlpbv1qyfhifbbjmx0axn50kcc933r2kk8kl7grx7qn3a4n") (y #t)))

(define-public crate-option_vec-0.1.1 (c (n "option_vec") (v "0.1.1") (h "091jbw1h5l8qcikiqg7srpxp3bwdcch2kj780sk0z6jvjfnigwlv")))

(define-public crate-option_vec-0.1.2 (c (n "option_vec") (v "0.1.2") (h "1iipb77j9rg9bc8rycncfsxzqgi6ik3djqv8zg616kgxzqp9ws74")))

(define-public crate-option_vec-0.1.3 (c (n "option_vec") (v "0.1.3") (h "0ps7lnwd049hbikpm83j4iwv6ddafw1nmwsp1jq7g8hvl0fp29pg")))

