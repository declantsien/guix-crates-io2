(define-module (crates-io op ti optimum) #:use-module (crates-io))

(define-public crate-optimum-0.0.0 (c (n "optimum") (v "0.0.0") (h "1gfvbz226g49pfqh65ykyv64fvjj2asd0y3892gplnj8r1q1xgwg") (y #t)))

(define-public crate-optimum-0.0.1 (c (n "optimum") (v "0.0.1") (h "1xv751jbxb3j0647y2cqg8sdyhqlfa64zx2gihdfbnrinn0wqxag") (y #t)))

(define-public crate-optimum-0.0.2 (c (n "optimum") (v "0.0.2") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "ordered-float") (r "^2.8.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "168lmhp7j32yhwpf66qvwsbgnlkph0lpqvg29d34nr7ixmyynagd")))

(define-public crate-optimum-0.0.3 (c (n "optimum") (v "0.0.3") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "ordered-float") (r "^2.8.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "09i0zjzk2dnrh9vnf109w16yh0l3m51ky1dq4nbvjj7hjf2hqlcc")))

(define-public crate-optimum-0.0.4 (c (n "optimum") (v "0.0.4") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "ordered-float") (r "^2.10.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "typed-builder") (r "^0.9.1") (d #t) (k 0)))) (h "07dp2kbhz5i3i8amrsli3l19hd70yxm1q19lip8imlnx3nl8gyan")))

