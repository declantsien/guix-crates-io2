(define-module (crates-io op ti optional_struct_export) #:use-module (crates-io))

(define-public crate-optional_struct_export-0.3.2 (c (n "optional_struct_export") (v "0.3.2") (d (list (d (n "optional_struct_macro_impl") (r "^0.3.2") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)))) (h "02i3f7z2ifaiab55rvma4plhnscf9gix42yvwb59y91jz5fjf16i")))

(define-public crate-optional_struct_export-0.4.0 (c (n "optional_struct_export") (v "0.4.0") (d (list (d (n "optional_struct_macro_impl") (r "^0.4.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)))) (h "0rcpnbrbwfj892r38nf5m73vypy5m3w6is6vjsa5f08yya645yxg")))

(define-public crate-optional_struct_export-0.4.1 (c (n "optional_struct_export") (v "0.4.1") (d (list (d (n "optional_struct_macro_impl") (r "^0.4.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)))) (h "1vc8q04mb4pcg8mbsgxrqwh6jwirfqn8drkrwsjl2pnq08c3bars")))

