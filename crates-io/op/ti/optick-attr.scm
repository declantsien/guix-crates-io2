(define-module (crates-io op ti optick-attr) #:use-module (crates-io))

(define-public crate-optick-attr-0.1.0 (c (n "optick-attr") (v "0.1.0") (d (list (d (n "optick") (r "^1.3.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("full"))) (d #t) (k 0)))) (h "00cbh099y8hv9kz9lnpscnpz64xyfzw49sjj56i7vi6whgimpjlx") (f (quote (("enable") ("default" "enable"))))))

(define-public crate-optick-attr-0.2.0 (c (n "optick-attr") (v "0.2.0") (d (list (d (n "optick") (r "^1.3.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0spr8liz4lrwlfhwldsi0z880amdgd3y5yj9azmr94hm9s8rq32n") (f (quote (("enable") ("default" "enable"))))))

(define-public crate-optick-attr-0.3.0 (c (n "optick-attr") (v "0.3.0") (d (list (d (n "optick") (r "^1.3.2") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("full"))) (d #t) (k 0)))) (h "01fd429hcr6akz641syc58fbacm9ywz9h4z4a69shnmsmhi05ikr") (f (quote (("enable") ("default" "enable"))))))

