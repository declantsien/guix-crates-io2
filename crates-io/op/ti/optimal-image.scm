(define-module (crates-io op ti optimal-image) #:use-module (crates-io))

(define-public crate-optimal-image-0.3.0 (c (n "optimal-image") (v "0.3.0") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "dssim") (r "^2.9.9") (d #t) (k 0)) (d (n "image") (r "^0.19.0") (d #t) (k 0)) (d (n "imgref") (r "^1.3.5") (d #t) (k 0)) (d (n "libc") (r "^0.2.43") (d #t) (k 0)) (d (n "lodepng") (r "^2.1.2") (d #t) (k 0)) (d (n "rgb") (r "^0.8.11") (d #t) (k 0)) (d (n "vips-sys") (r "^0.1.2") (d #t) (k 0)))) (h "0jmf5x52vxhhgqdnl4bfq634l6xmn1676gakh20hj43ccwb34wky")))

(define-public crate-optimal-image-0.3.1 (c (n "optimal-image") (v "0.3.1") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "dssim") (r "^2.9.9") (d #t) (k 0)) (d (n "image") (r "^0.19.0") (d #t) (k 0)) (d (n "imgref") (r "^1.3.5") (d #t) (k 0)) (d (n "libc") (r "^0.2.43") (d #t) (k 0)) (d (n "lodepng") (r "^2.1.2") (d #t) (k 0)) (d (n "rgb") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.82") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.82") (d #t) (k 0)) (d (n "vips-sys") (r "^0.1.2") (d #t) (k 0)))) (h "1558cm9sqfqjql9smyyvckygd03vyx047q0rn9wnl9qh1lvdxan1")))

