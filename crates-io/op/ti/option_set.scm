(define-module (crates-io op ti option_set) #:use-module (crates-io))

(define-public crate-option_set-0.1.0 (c (n "option_set") (v "0.1.0") (d (list (d (n "heck") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.29") (d #t) (k 0)))) (h "00dr1ws9yqafayy7b1hb4gdd6daccrp4mn29nv9w52glp7bgmj93")))

(define-public crate-option_set-0.1.1 (c (n "option_set") (v "0.1.1") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 2)) (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "03f55m59wlv6gb48ivf9hzdjv7d0c5ra18xzaibc35gialv1617k")))

(define-public crate-option_set-0.1.2 (c (n "option_set") (v "0.1.2") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 2)) (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1dn3wc6r1vf7vd7ynkrmfnj70c0q03kcbn4qdq2ls5l5qpspnabh")))

(define-public crate-option_set-0.1.3 (c (n "option_set") (v "0.1.3") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 2)) (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 2)))) (h "1skpj9p24d8a1239amk7z1safpynxgbv6da3vmk2qmdsp96by1cv")))

(define-public crate-option_set-0.1.4 (c (n "option_set") (v "0.1.4") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 2)) (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 2)))) (h "16wsxh2qmjb2bf7mcq7dhlpzwkslzgjpg3nwzx97ia6gpnb2sfy8")))

(define-public crate-option_set-0.2.0 (c (n "option_set") (v "0.2.0") (d (list (d (n "bitflags") (r "^2") (d #t) (k 2)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 2)))) (h "0i6s3bmnrw44nffqbbcaiq7fyhz7j881lcgspb57jxsi752m11k0")))

