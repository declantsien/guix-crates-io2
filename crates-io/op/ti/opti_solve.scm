(define-module (crates-io op ti opti_solve) #:use-module (crates-io))

(define-public crate-opti_solve-0.1.0 (c (n "opti_solve") (v "0.1.0") (d (list (d (n "pyo3") (r "^0.18.3") (d #t) (k 0)))) (h "07faki2rrpm8isxaig91dzf4c59g5qa96x4yl567ddlvc9hz5yj7")))

(define-public crate-opti_solve-0.1.1 (c (n "opti_solve") (v "0.1.1") (d (list (d (n "array-init") (r "^2.0.1") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.3") (f (quote ("rand" "std"))) (d #t) (k 0)) (d (n "num-complex") (r "^0.4.3") (f (quote ("rand" "std"))) (d #t) (k 0)) (d (n "pyo3") (r "^0.19.2") (f (quote ("num-complex" "rust_decimal" "auto-initialize" "num-bigint"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.29.1") (f (quote ("rand" "std"))) (d #t) (k 0)) (d (n "send_wrapper") (r "^0.6.0") (d #t) (k 0)))) (h "0grd1wgr0nxjdjva0qxn9mqaha9x9xz72zxh07aaz874s9sbhqmk")))

