(define-module (crates-io op er operator_advisor) #:use-module (crates-io))

(define-public crate-operator_advisor-0.1.0 (c (n "operator_advisor") (v "0.1.0") (h "1x23pnlgbid1mz72iq6brmymgx89idfk07553ch3r7vnhy22qn45")))

(define-public crate-operator_advisor-0.1.1 (c (n "operator_advisor") (v "0.1.1") (h "1p4v2nhhakyhbx64dfhfwlvf5h3xh3ps3q6axfqm5505gd4dakfa")))

(define-public crate-operator_advisor-0.1.2 (c (n "operator_advisor") (v "0.1.2") (h "094idwvxiq0g9k09y97r5f9w5mw3hs6n2vz1blkc41fq990is5dy")))

(define-public crate-operator_advisor-0.1.7 (c (n "operator_advisor") (v "0.1.7") (d (list (d (n "enum-ordinalize") (r "^3.1.10") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.3") (d #t) (k 0)) (d (n "ndarray-rand") (r "^0.14.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.68") (d #t) (k 0)) (d (n "strum") (r "^0.22.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.22") (d #t) (k 0)))) (h "0q0mxhyb8w1az9p0yisv3f4jsvp3jqkd1rs6i5rfpajnkl3b8hxd")))

(define-public crate-operator_advisor-0.1.8 (c (n "operator_advisor") (v "0.1.8") (d (list (d (n "enum-ordinalize") (r "^3.1.10") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.3") (d #t) (k 0)) (d (n "ndarray-rand") (r "^0.14.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.68") (d #t) (k 0)) (d (n "strum") (r "^0.22.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.22") (d #t) (k 0)))) (h "0awm8nfrkqdvfj6cbcs7bzv4rpywnimmjb1w2gl22aj6qi3q5mn1")))

