(define-module (crates-io op er operational) #:use-module (crates-io))

(define-public crate-operational-0.0.1 (c (n "operational") (v "0.0.1") (h "0vkfr22x2gspr5k2wcdnfznihqqc3dg6r6anzizcjasfkrz4zy0d")))

(define-public crate-operational-0.0.2 (c (n "operational") (v "0.0.2") (h "0lwnriaz47wg6g3b2ps98kh4dkr30jfrqy3pbfxxjsg07f8x6kp2")))

(define-public crate-operational-0.0.3 (c (n "operational") (v "0.0.3") (h "1p8ncl35adpywqppj2pc3ag8iqd8nkzfj54b0k6hp0pvk4drbl3b")))

(define-public crate-operational-0.0.4 (c (n "operational") (v "0.0.4") (h "0hywmi8ncmybmd3axsf23zlikc1w3bybq0vcap609yqhq4x4fk12")))

(define-public crate-operational-0.0.5 (c (n "operational") (v "0.0.5") (h "01h3x2119ybp5wapk8b9mpq8y22jm5xvzkf7yybk9659d2xhf7fg")))

