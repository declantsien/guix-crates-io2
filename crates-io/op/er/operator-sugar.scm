(define-module (crates-io op er operator-sugar) #:use-module (crates-io))

(define-public crate-operator-sugar-0.1.0 (c (n "operator-sugar") (v "0.1.0") (h "13lcnc0pdrdrmizq4y0jss4a2h7lim69f8ph9xx7fy0l1r89swzc")))

(define-public crate-operator-sugar-0.1.1 (c (n "operator-sugar") (v "0.1.1") (h "0x2aks46bb0zdki6jfqanhyca5qjjqd4c78vi4fqp688yxhfjw7l")))

(define-public crate-operator-sugar-0.1.2 (c (n "operator-sugar") (v "0.1.2") (h "1azmx4iyjmqk6n14h2g07cgpd8f545vs491m0p61cv4v5dnpif14")))

