(define-module (crates-io op er operations) #:use-module (crates-io))

(define-public crate-operations-0.1.0 (c (n "operations") (v "0.1.0") (d (list (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "rc-box") (r "^1.2.0") (d #t) (k 0)))) (h "14w6wi7rpbn9bigv4w2hjs778j2kbi2qqhwfv742pm1lrnnipx10")))

(define-public crate-operations-0.1.1 (c (n "operations") (v "0.1.1") (d (list (d (n "nalgebra") (r "^0.32.3") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "1klx5gdzd2b9q1n6pqv9xiyiwabay4s2vjk7vy24zl2h22qwhsss")))

(define-public crate-operations-0.1.2 (c (n "operations") (v "0.1.2") (d (list (d (n "nalgebra") (r "^0.32.3") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "016jlh720n8byj5sqwz3h00ryk9zqaj8yrlml9r6ghrsfqkafjgi")))

