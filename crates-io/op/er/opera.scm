(define-module (crates-io op er opera) #:use-module (crates-io))

(define-public crate-opera-1.0.0 (c (n "opera") (v "1.0.0") (d (list (d (n "trybuild") (r "^1.0.45") (d #t) (k 2)))) (h "1ivlxdlpzs7zswhcphkwx181pp6ij0dddh5kkr0hkxc6f1him3sw")))

(define-public crate-opera-1.0.1 (c (n "opera") (v "1.0.1") (d (list (d (n "trybuild") (r "^1.0.45") (d #t) (k 2)))) (h "1lfyh00nljjzapdxwdg4jagkxgr57h6czayxdazjc85d6xw88i2l")))

