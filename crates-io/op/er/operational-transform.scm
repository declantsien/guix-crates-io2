(define-module (crates-io op er operational-transform) #:use-module (crates-io))

(define-public crate-operational-transform-0.1.0 (c (n "operational-transform") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (k 0)) (d (n "serde_json") (r "^1.0.50") (d #t) (k 2)))) (h "00n2mbczlcvw42h4bhkgy2ka26m4xp9h6p8b4fz5psi171ggndzd")))

(define-public crate-operational-transform-0.1.1 (c (n "operational-transform") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (k 0)) (d (n "serde_json") (r "^1.0.50") (d #t) (k 2)))) (h "0my44s6dvyyyrgdzglznqa557j4x13adxrrw6a6a36idxwypkaxy")))

(define-public crate-operational-transform-0.2.0 (c (n "operational-transform") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (k 0)) (d (n "serde_json") (r "^1.0.50") (d #t) (k 2)))) (h "0swqvymvsg0id8azmx4mlzy1889n1zxd860ff2n3wzbyrlc284hf")))

(define-public crate-operational-transform-0.3.0 (c (n "operational-transform") (v "0.3.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (k 0)) (d (n "serde_json") (r "^1.0.50") (d #t) (k 2)))) (h "1jni7swqfbl5l8ii427kshwaj7vddwgss9i8lww341lnjq8k8d5i")))

(define-public crate-operational-transform-0.5.0 (c (n "operational-transform") (v "0.5.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (k 0)) (d (n "serde_json") (r "^1.0.50") (d #t) (k 2)))) (h "17pr0bnzvzs7fk2jw7c202232ww8airrap4pjnmxzg73vvps6hgz")))

(define-public crate-operational-transform-0.6.0 (c (n "operational-transform") (v "0.6.0") (d (list (d (n "bytecount") (r "^0.6.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (k 0)) (d (n "serde_json") (r "^1.0.50") (d #t) (k 2)))) (h "0439mdiyy3v4smq36lvzyb6k08wfwjhskkv37zjdaywsd741pcsp") (f (quote (("runtime-dispatch-simd" "bytecount/runtime-dispatch-simd") ("generic-simd" "bytecount/generic-simd"))))))

(define-public crate-operational-transform-0.6.1 (c (n "operational-transform") (v "0.6.1") (d (list (d (n "bytecount") (r "^0.6.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (k 0)) (d (n "serde_json") (r "^1.0.50") (d #t) (k 2)))) (h "1pqiam5fgml6yp9j3kics488sg9kvwgypcnc33yqhwnkc4i59p7w") (f (quote (("runtime-dispatch-simd" "bytecount/runtime-dispatch-simd") ("generic-simd" "bytecount/generic-simd"))))))

