(define-module (crates-io op do opdot) #:use-module (crates-io))

(define-public crate-opdot-0.1.0 (c (n "opdot") (v "0.1.0") (d (list (d (n "argh") (r "^0.1.8") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "inquire") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)))) (h "0qf00nf6q680rq7g0bp0b73cwfz3r2cxq1j0jfrks793w2c2nv10") (y #t)))

