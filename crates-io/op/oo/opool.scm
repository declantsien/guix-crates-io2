(define-module (crates-io op oo opool) #:use-module (crates-io))

(define-public crate-opool-0.1.0 (c (n "opool") (v "0.1.0") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "crossbeam-queue") (r "^0.3") (d #t) (k 0)) (d (n "rayon") (r "^1") (d #t) (k 2)))) (h "1z9vm4dfqp2sw9pkrwpcg5zr41k9352yldklndld2w9bqyvlpcr5")))

(define-public crate-opool-0.1.1 (c (n "opool") (v "0.1.1") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "crossbeam-queue") (r "^0.3") (d #t) (k 0)) (d (n "rayon") (r "^1") (d #t) (k 2)))) (h "1gp2qjvjlfgxgjc0qcj9vri76zim9q7i9rdq4dpmkirmhhfvd3f3")))

