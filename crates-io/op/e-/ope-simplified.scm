(define-module (crates-io op e- ope-simplified) #:use-module (crates-io))

(define-public crate-ope-simplified-0.1.0 (c (n "ope-simplified") (v "0.1.0") (d (list (d (n "aes") (r "^0.8") (d #t) (k 0)) (d (n "block-modes") (r "^0.9.1") (d #t) (k 0)) (d (n "cipher") (r "^0.4.4") (d #t) (k 0)) (d (n "ctr") (r "^0.9.2") (d #t) (k 0)) (d (n "hmac") (r "^0.12.1") (d #t) (k 0)) (d (n "openssl") (r "^0.10.55") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "sha2") (r "^0.10.7") (d #t) (k 0)))) (h "0vbnydkrhz1frchz09wnigw3p32srsnlk9qgni854w7lx9xv69zz")))

