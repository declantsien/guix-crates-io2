(define-module (crates-io op e- ope-rust-web-server) #:use-module (crates-io))

(define-public crate-ope-rust-web-server-0.1.0 (c (n "ope-rust-web-server") (v "0.1.0") (h "09jmha5j2b93bvx20rvv4kc1lhv2arbisx85wg9cyh01429znrz5")))

(define-public crate-ope-rust-web-server-0.1.1 (c (n "ope-rust-web-server") (v "0.1.1") (h "1ibl47jqpd678xghrrj6vdlj3mpzmip3bqfr7qdz786lmi905cbh")))

