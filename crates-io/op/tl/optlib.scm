(define-module (crates-io op tl optlib) #:use-module (crates-io))

(define-public crate-optlib-0.1.0 (c (n "optlib") (v "0.1.0") (d (list (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.6.4") (d #t) (k 0)))) (h "1njjfsnqyg5j2yzdxaq32x5csi3kac5xjxnpns7v627ijszfy0s4")))

(define-public crate-optlib-0.2.0 (c (n "optlib") (v "0.2.0") (d (list (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "optlib-testfunc") (r "^0.1.0") (d #t) (k 2)) (d (n "rand") (r "^0.6.4") (d #t) (k 0)))) (h "1m9hrh8faqmp9cgrx5gp688wn0bggkn5dx0d36xiplizhp7038lf")))

(define-public crate-optlib-0.3.0 (c (n "optlib") (v "0.3.0") (d (list (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "optlib-testfunc") (r "^0.1.0") (d #t) (k 2)) (d (n "rand") (r "^0.6.4") (d #t) (k 0)))) (h "1iclbs4nm8bmq5zdzfxs9jzwgcgr9abcjf0bw85ah19lyxi8pqy2")))

(define-public crate-optlib-0.4.0 (c (n "optlib") (v "0.4.0") (d (list (d (n "num") (r "^0.2.1") (d #t) (k 0)) (d (n "num_cpus") (r "^1.12.0") (d #t) (k 2)) (d (n "optlib-testfunc") (r "^0.1.0") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "13jx89yfz9qgy1h7345ik2r9i6ynn4dghlnd5j81x21ky1a68g0k")))

