(define-module (crates-io op tp optparse) #:use-module (crates-io))

(define-public crate-optparse-0.1.0 (c (n "optparse") (v "0.1.0") (h "0apv7nyf958516s4f2fkd6lhkcrq9wr6vn71302a87ji34w4zm8x")))

(define-public crate-optparse-0.1.1 (c (n "optparse") (v "0.1.1") (h "0adkc3n2sgg57hvcz2g515siyr84qrsbk8hvkjw5s92qf1bdm501")))

(define-public crate-optparse-0.1.2 (c (n "optparse") (v "0.1.2") (h "17qmkq7knpcqga21nd81yd7zp511m1iv0z6m5nk2ammyca0hs6ny")))

(define-public crate-optparse-0.1.3 (c (n "optparse") (v "0.1.3") (h "0blf9xs83y84ssvbb07w72drdfl50dsr9w7fjpn3rahnk0l0ccf4")))

(define-public crate-optparse-0.1.4 (c (n "optparse") (v "0.1.4") (h "0fn3ivs227yypmrvzb8dy2rankfv5cx39lfz2bf0svd2dxwqn1yz")))

(define-public crate-optparse-0.1.5 (c (n "optparse") (v "0.1.5") (h "1l48x5waxdgy6f1kv9mcxwzvflmmqh98w4nxpk7r479y8ign5h1n")))

