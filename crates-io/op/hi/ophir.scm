(define-module (crates-io op hi ophir) #:use-module (crates-io))

(define-public crate-ophir-0.1.0 (c (n "ophir") (v "0.1.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "encoding") (r "^0.2") (d #t) (k 0)) (d (n "futures") (r "^0.1.2") (d #t) (k 0)) (d (n "peg") (r "^0.4") (d #t) (k 1)) (d (n "time") (r "^0.1.35") (d #t) (k 0)))) (h "0z0fpbfqy6yaa4c30z2x4qkr9y4lhrnjsrg4vrhb8xwmncg4s32h")))

(define-public crate-ophir-0.1.1 (c (n "ophir") (v "0.1.1") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "encoding") (r "^0.2") (d #t) (k 0)) (d (n "futures") (r "^0.1.2") (d #t) (k 0)) (d (n "peg") (r "^0.4") (d #t) (k 1)) (d (n "time") (r "^0.1.35") (d #t) (k 0)))) (h "04gg0anvmqkzfkgnfpx6ss0vklrk9i5ppkar317cbzlccqk1h37z")))

(define-public crate-ophir-0.1.2 (c (n "ophir") (v "0.1.2") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "encoding") (r "^0.2") (d #t) (k 0)) (d (n "futures") (r "^0.1.2") (d #t) (k 0)) (d (n "peg") (r "^0.4") (d #t) (k 1)) (d (n "time") (r "^0.1.35") (d #t) (k 0)))) (h "09wj1v12a7z20zcgil2qma38326lsljza6idqdhcv5cvr9rbx1a3")))

(define-public crate-ophir-0.1.3 (c (n "ophir") (v "0.1.3") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "encoding") (r "^0.2") (d #t) (k 0)) (d (n "peg") (r "^0.4") (d #t) (k 1)) (d (n "time") (r "^0.1.35") (d #t) (k 0)))) (h "0n1pwwlp3qb1gvbxwddh9fcvs3nnns4353cli8fm8cjfpwz5ybcm")))

(define-public crate-ophir-0.1.4 (c (n "ophir") (v "0.1.4") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "encoding") (r "^0.2") (d #t) (k 0)) (d (n "peg") (r "^0.4") (d #t) (k 1)) (d (n "time") (r "^0.1.35") (d #t) (k 0)))) (h "08zifrrkh3bwjn3m8qn8dds1k7kkswiyyack19018vz0df2sk0g2")))

(define-public crate-ophir-0.2.0 (c (n "ophir") (v "0.2.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "encoding") (r "^0.2") (d #t) (k 0)) (d (n "peg") (r "^0.5") (d #t) (k 1)) (d (n "regex") (r "^0.2.1") (d #t) (k 0)) (d (n "time") (r "^0.1.35") (d #t) (k 0)))) (h "0p42hv42nl7bgkd0yndbc5g6pjss22l3jz1hbdi14wpmw1bimlf0")))

(define-public crate-ophir-0.2.2 (c (n "ophir") (v "0.2.2") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "encoding") (r "^0.2") (d #t) (k 0)) (d (n "peg") (r "^0.5") (d #t) (k 1)) (d (n "regex") (r "^0.2.1") (d #t) (k 0)) (d (n "time") (r "^0.1.35") (d #t) (k 0)))) (h "0837506x73a6rksas87vkw95wd3ryjyxz7ac8xqfj7xgaxkjhdyy")))

(define-public crate-ophir-0.2.3 (c (n "ophir") (v "0.2.3") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "encoding") (r "^0.2") (d #t) (k 0)) (d (n "peg") (r "^0.5") (d #t) (k 1)) (d (n "regex") (r "^0.2.1") (d #t) (k 0)) (d (n "time") (r "^0.1.35") (d #t) (k 0)))) (h "13jfx2knr1qhddmhby2nlsskcfl062wqaizda5kcd543vvrnvzns")))

(define-public crate-ophir-0.2.4 (c (n "ophir") (v "0.2.4") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "encoding") (r "^0.2") (d #t) (k 0)) (d (n "peg") (r "^0.5") (d #t) (k 1)) (d (n "regex") (r "^0.2.1") (d #t) (k 0)) (d (n "time") (r "^0.1.35") (d #t) (k 0)))) (h "0ppn7853xg6qddz8pwpp8p4djxf4ag8g2n4xxk01his2hd1gvkry")))

(define-public crate-ophir-0.2.5 (c (n "ophir") (v "0.2.5") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "dimensioned") (r "^0.6.0") (d #t) (k 0)) (d (n "encoding") (r "^0.2") (d #t) (k 0)) (d (n "peg") (r "^0.5") (d #t) (k 1)) (d (n "regex") (r "^0.2.1") (d #t) (k 0)) (d (n "time") (r "^0.1.35") (d #t) (k 0)))) (h "1n6bdvam3cw03cb7q739xac6il07j62nv6fnz8c9gqfd716a4f24")))

