(define-module (crates-io op im opimus) #:use-module (crates-io))

(define-public crate-opimus-0.1.0 (c (n "opimus") (v "0.1.0") (h "1yn5nvwialnm6r9p13gq3cmnrb1c3fsri5iy3a3q1s66qns3yjnn") (y #t)))

(define-public crate-opimus-0.1.1 (c (n "opimus") (v "0.1.1") (h "1vgg6ci0n2mfl1jz32zyalcqh5003kcwf8y6zg1ns2nwjw8azl36") (y #t)))

