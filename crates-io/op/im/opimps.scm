(define-module (crates-io op im opimps) #:use-module (crates-io))

(define-public crate-opimps-0.1.0 (c (n "opimps") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1lc5hmiapp2z8fq4viwm4w84wd6k7i1zy3df5sz8mibjg5g3p1cb")))

(define-public crate-opimps-0.1.1 (c (n "opimps") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0cka7lgb89jzzc2790irl09v3v49v2v5wlw9cw625iyhay5cqrpm")))

(define-public crate-opimps-0.1.2 (c (n "opimps") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1av886xpmwgdw0ph1ayzziks4k8mp8xf601nr0zz69n3vaid3g7a")))

(define-public crate-opimps-0.1.3 (c (n "opimps") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "07qbw3y8dpbc4rpwsrh7i935s3rqcndd0g0rfq2bm1nrbmdhl0c8")))

(define-public crate-opimps-0.1.4 (c (n "opimps") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0gld4sfg9wgdzb94gs65s1xhwalrxzmk3xhgdmy3icn7nbsgmsb8")))

(define-public crate-opimps-0.2.0 (c (n "opimps") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (f (quote ("full"))) (d #t) (k 0)))) (h "0j74y0iys7jpcpp2xgwhnnfillpr9ydcpi4l7ylw745zgv8dxy3g") (y #t)))

(define-public crate-opimps-0.2.1 (c (n "opimps") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (f (quote ("full"))) (d #t) (k 0)))) (h "0xvrdwyvbw80n0fx2ajqc86jx0ik15x8dcs9xx6p4kr6n1lsg1qg") (y #t)))

(define-public crate-opimps-0.2.2 (c (n "opimps") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (f (quote ("full"))) (d #t) (k 0)))) (h "0a82gak6kz5iwykj2znl1l2swx4f00ydqqlrmi8jxzks9bkanzc5")))

