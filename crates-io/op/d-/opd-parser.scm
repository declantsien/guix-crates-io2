(define-module (crates-io op d- opd-parser) #:use-module (crates-io))

(define-public crate-opd-parser-0.1.0 (c (n "opd-parser") (v "0.1.0") (d (list (d (n "glam") (r "^0.22") (f (quote ("serde"))) (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "125yv4kbas730m83v1wv1pmzyid7hhm3s50mw75z1agn09cypybb")))

(define-public crate-opd-parser-0.1.1 (c (n "opd-parser") (v "0.1.1") (d (list (d (n "glam") (r "^0.22") (f (quote ("serde"))) (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1wva4nf8clkiilv2p2wc79kkpwxlp8g1787wjly5zd5azpcsalqb")))

(define-public crate-opd-parser-0.2.0 (c (n "opd-parser") (v "0.2.0") (d (list (d (n "glam") (r "^0.22") (f (quote ("serde"))) (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "02dii2a1a05cq6a8lk0c0ypiahi1mdyn53a549dvmc40fhqp07f0")))

(define-public crate-opd-parser-0.3.0 (c (n "opd-parser") (v "0.3.0") (d (list (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1slmhgph6lvq8z7385rg11ag5lp9d4vcrhfz81x3sdmxvapmqkh7")))

(define-public crate-opd-parser-0.4.0 (c (n "opd-parser") (v "0.4.0") (d (list (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0w0p3c75wfvg188y6mpdvv1p7qix51vqm2zs6n5fjgxjqjzlg3f2")))

(define-public crate-opd-parser-0.5.0 (c (n "opd-parser") (v "0.5.0") (d (list (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "17pyglcx3iw551216x87jfzc5364c294243x2crbvxa3rr4afkf2")))

