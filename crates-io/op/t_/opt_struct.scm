(define-module (crates-io op t_ opt_struct) #:use-module (crates-io))

(define-public crate-opt_struct-0.1.1 (c (n "opt_struct") (v "0.1.1") (h "1apddjpwnpmjlkb0fc26701d32rmaw2alsxn07pvqmpf8v922a8c")))

(define-public crate-opt_struct-0.1.2 (c (n "opt_struct") (v "0.1.2") (h "14cm9lmxvyzfl96m34hj9s90wd65bvi4jksks9mn15i2z57nygp6")))

