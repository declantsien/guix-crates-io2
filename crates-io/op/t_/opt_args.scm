(define-module (crates-io op t_ opt_args) #:use-module (crates-io))

(define-public crate-opt_args-0.0.1 (c (n "opt_args") (v "0.0.1") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "09jc0j6d99xk8h5x5h40r98xxx6s6az0bad266kz5aprvnjfb8nh")))

(define-public crate-opt_args-0.1.0 (c (n "opt_args") (v "0.1.0") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "17pn54z7x878v6nsn1xip2w52zsmzcbi4bbhlsp8411bh2q554ww")))

(define-public crate-opt_args-1.0.0 (c (n "opt_args") (v "1.0.0") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1bpn5wzwgcxfnwninhfi9mxkxsqyy0834fhdal5rw10cw4l9w4np")))

(define-public crate-opt_args-2.0.0 (c (n "opt_args") (v "2.0.0") (d (list (d (n "deluxe") (r "^0.5.0") (d #t) (k 0)) (d (n "derive-syn-parse") (r "^0.1.5") (d #t) (k 0)) (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.52") (f (quote ("full"))) (d #t) (k 0)))) (h "1gaf85ysrjb02a72wd5xs6bn2s5c842iwpprni1r68wyfbf2rinm")))

