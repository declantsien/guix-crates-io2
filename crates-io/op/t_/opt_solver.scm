(define-module (crates-io op t_ opt_solver) #:use-module (crates-io))

(define-public crate-opt_solver-0.1.1 (c (n "opt_solver") (v "0.1.1") (d (list (d (n "array-init") (r "^2.0.1") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.3") (f (quote ("rand" "std"))) (d #t) (k 0)) (d (n "num-complex") (r "^0.4.3") (f (quote ("rand" "std"))) (d #t) (k 0)) (d (n "pyo3") (r "^0.19.2") (f (quote ("num-complex" "rust_decimal" "auto-initialize" "num-bigint"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.29.1") (f (quote ("rand" "std"))) (d #t) (k 0)))) (h "1j81m0vhdwpfgjqrli0jf9yrl2vmxicq0rk9p2lhv494d3rzy7sz")))

