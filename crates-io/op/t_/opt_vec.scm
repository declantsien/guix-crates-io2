(define-module (crates-io op t_ opt_vec) #:use-module (crates-io))

(define-public crate-opt_vec-0.1.0 (c (n "opt_vec") (v "0.1.0") (h "0ns52175a7gwj2843g39lr0dykn9yp1rd4w66c6fswrmszz9nizk") (f (quote (("std" "alloc") ("default" "std") ("alloc")))) (r "1.56")))

(define-public crate-opt_vec-0.1.1 (c (n "opt_vec") (v "0.1.1") (h "1mym014jz6qkmdf2x5a762vfw5pncnznblxdnr206c2ydi6y1j02") (f (quote (("std" "alloc") ("default" "std") ("alloc")))) (r "1.56")))

(define-public crate-opt_vec-0.1.2 (c (n "opt_vec") (v "0.1.2") (h "1v7zfgnf8y3idqlgigdg55ig6bcf7lwx69nj03kqbycsflh4iawj") (f (quote (("std" "alloc") ("default" "std") ("alloc")))) (r "1.56")))

