(define-module (crates-io op aq opaquebind) #:use-module (crates-io))

(define-public crate-opaquebind-0.2.0 (c (n "opaquebind") (v "0.2.0") (d (list (d (n "argon2") (r "^0.3") (d #t) (k 0)) (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "curve25519-dalek") (r "^3") (k 0)) (d (n "digest") (r "^0.9") (d #t) (k 0)) (d (n "opaque-ke") (r "^1.2.0") (f (quote ("slow-hash"))) (d #t) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("getrandom"))) (k 0)) (d (n "sha2") (r "^0.9") (d #t) (k 0)))) (h "0208h0yjwns9p1q81krqgr1rzadff2paz3xc3i8l0sg6j47pp4cc")))

(define-public crate-opaquebind-0.2.1 (c (n "opaquebind") (v "0.2.1") (d (list (d (n "argon2") (r "^0.3") (d #t) (k 0)) (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "curve25519-dalek") (r "^3") (k 0)) (d (n "digest") (r "^0.9") (d #t) (k 0)) (d (n "opaque-ke") (r "^1.2.0") (f (quote ("slow-hash"))) (d #t) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("getrandom"))) (k 0)) (d (n "sha2") (r "^0.9") (d #t) (k 0)))) (h "013mx5xys3s44kdpqsc6r5fixcyql71ad7pa4h6y0vhkbb3bh0rd")))

