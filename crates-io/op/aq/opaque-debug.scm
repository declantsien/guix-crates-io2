(define-module (crates-io op aq opaque-debug) #:use-module (crates-io))

(define-public crate-opaque-debug-0.1.0 (c (n "opaque-debug") (v "0.1.0") (h "00c88i0vp9y0b9z26aanwpgh7i7fdlmr6r9k7ng7fyzq4w5ks183") (y #t)))

(define-public crate-opaque-debug-0.1.1 (c (n "opaque-debug") (v "0.1.1") (h "1mra8m537df9ch53a67qr9fc2yicn7yhvb49jh1lzcrld31cj86n")))

(define-public crate-opaque-debug-0.2.0 (c (n "opaque-debug") (v "0.2.0") (h "1m23ma2mirf0khcgppvigpkr9aynyfn8g76nig25147jdl8yy5c0") (y #t)))

(define-public crate-opaque-debug-0.2.1 (c (n "opaque-debug") (v "0.2.1") (h "10k6vlrrzgg1xs3fpcpaccwbd07klym5is3g8mnjbgg146wbrv2i")))

(define-public crate-opaque-debug-0.2.2 (c (n "opaque-debug") (v "0.2.2") (h "02942l2gc7w5r4js7i9063x99szic5mzzk1055j83v4diqpbpxck")))

(define-public crate-opaque-debug-0.2.3 (c (n "opaque-debug") (v "0.2.3") (h "172j6bs8ndclqxa2m64qc0y1772rr73g4l9fg2svscgicnbfff98")))

(define-public crate-opaque-debug-0.3.0 (c (n "opaque-debug") (v "0.3.0") (h "1m8kzi4nd6shdqimn0mgb24f0hxslhnqd1whakyq06wcqd086jk2")))

(define-public crate-opaque-debug-0.3.1 (c (n "opaque-debug") (v "0.3.1") (h "10b3w0kydz5jf1ydyli5nv10gdfp97xh79bgz327d273bs46b3f0")))

