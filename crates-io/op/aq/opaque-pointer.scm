(define-module (crates-io op aq opaque-pointer) #:use-module (crates-io))

(define-public crate-opaque-pointer-0.1.0 (c (n "opaque-pointer") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (o #t) (d #t) (k 0)))) (h "16ra8m2sqf7i2mq1z0hnsgr3hzd6xb8sfl7gxzd9vv9l3414wmh2") (y #t)))

(define-public crate-opaque-pointer-0.1.1 (c (n "opaque-pointer") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (o #t) (d #t) (k 0)))) (h "1kxz96szly67npybkj9r4rqlxr1hdy7lqmm7ydghb60gv0gzi5sy") (f (quote (("std") ("default")))) (y #t)))

(define-public crate-opaque-pointer-0.1.2 (c (n "opaque-pointer") (v "0.1.2") (d (list (d (n "libc") (r "^0.2") (o #t) (d #t) (k 0)))) (h "1mssyjmwr4d454z8bz3qyxxf80jqbmgy0y8jrv6dpnjv02d9yrl3") (f (quote (("std") ("default")))) (y #t)))

(define-public crate-opaque-pointer-0.2.0 (c (n "opaque-pointer") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (o #t) (d #t) (k 0)))) (h "0alw24xvfya89yrq8zhihsp6lll51dmizjxbvd19fn79wky5zjry") (f (quote (("std") ("default")))) (y #t)))

(define-public crate-opaque-pointer-0.3.0 (c (n "opaque-pointer") (v "0.3.0") (h "0qqam2p6k70w0gxb72baxl2p2lmnjh34z8brbnx0zadmnxsjc5gj") (f (quote (("std") ("default" "std")))) (y #t)))

(define-public crate-opaque-pointer-0.4.0 (c (n "opaque-pointer") (v "0.4.0") (h "1mypim9p8mz3kriixpxfa94h5w1vfx7adpx6v7q9dkg2nl52f099") (f (quote (("std") ("default" "std")))) (y #t)))

(define-public crate-opaque-pointer-0.5.0 (c (n "opaque-pointer") (v "0.5.0") (h "0mzdnc8l2mdc10jpyy66k70iz270h58z7mn8fv45mmfwy0dalqkm") (f (quote (("std") ("default" "std") ("c-types") ("alloc")))) (y #t)))

(define-public crate-opaque-pointer-0.6.0 (c (n "opaque-pointer") (v "0.6.0") (h "06l77dihq77j2j479ivzqa2klf5a0x02lyy5wpjdi0r2wcw6nwz5") (f (quote (("std") ("panic-if-null") ("default" "std") ("c-types" "std") ("alloc")))) (y #t)))

(define-public crate-opaque-pointer-0.6.1 (c (n "opaque-pointer") (v "0.6.1") (d (list (d (n "log") (r "^0") (d #t) (k 0)))) (h "0kzl6r79iwvbp684703ycps410bkrf1f9walbj20i4v1al42ykb3") (f (quote (("std") ("panic-if-null") ("default" "std") ("c-types" "std") ("alloc")))) (y #t)))

(define-public crate-opaque-pointer-0.7.0 (c (n "opaque-pointer") (v "0.7.0") (d (list (d (n "log") (r "^0") (d #t) (k 0)))) (h "0cmmwx42gfz4mkkfjqx9wpmp5g67f7ndm52jjfm5qsnkp881j57p") (f (quote (("std") ("panic-if-null") ("default" "std") ("c-types" "std") ("alloc")))) (y #t)))

(define-public crate-opaque-pointer-0.7.1 (c (n "opaque-pointer") (v "0.7.1") (d (list (d (n "log") (r "^0") (d #t) (k 0)))) (h "11ba2rsk5mp4567hadfrafq0fb2dbri4h4nnqk9fnspnx4x5y3i1") (f (quote (("std") ("panic-if-null") ("default" "std") ("c-types" "std") ("alloc")))) (y #t)))

(define-public crate-opaque-pointer-0.7.2 (c (n "opaque-pointer") (v "0.7.2") (d (list (d (n "log") (r "^0") (d #t) (k 0)))) (h "1hvryxc9x670canx50zwffrvxyxily79f3g4a2dsyxinxhscagr7") (f (quote (("std") ("panic-if-null") ("default" "std") ("c-types" "std") ("alloc"))))))

(define-public crate-opaque-pointer-0.8.0 (c (n "opaque-pointer") (v "0.8.0") (d (list (d (n "log") (r "^0") (d #t) (k 0)))) (h "1pn5hlkqnqhlddxklcwbslljscvphsnc2c9734hg6zkxk99bzfyy") (f (quote (("std") ("default" "std") ("c-types" "std") ("alloc")))) (y #t)))

(define-public crate-opaque-pointer-0.8.1 (c (n "opaque-pointer") (v "0.8.1") (d (list (d (n "log") (r "^0") (d #t) (k 0)))) (h "14vlhzqydjv6xmzpxjxicrh9dpsfzkax78li94g1iy5858199yfb") (f (quote (("std") ("default" "std") ("c-types" "std") ("alloc")))) (y #t)))

(define-public crate-opaque-pointer-0.8.2 (c (n "opaque-pointer") (v "0.8.2") (d (list (d (n "lazy_static") (r "^1") (o #t) (d #t) (k 0)) (d (n "log") (r "^0") (d #t) (k 0)))) (h "0izvw03d1wj53rq54wx0jifmfqw31wg8b6v3bz1m29x1h87fnns7") (f (quote (("std") ("lender" "lazy_static") ("default" "std") ("c-types" "std") ("alloc")))) (y #t)))

(define-public crate-opaque-pointer-0.8.3 (c (n "opaque-pointer") (v "0.8.3") (d (list (d (n "lazy_static") (r "^1") (o #t) (d #t) (k 0)) (d (n "log") (r "^0") (d #t) (k 0)))) (h "1hgqswz19ip84b12faiqm06xsbf1vvy53sc9kq1f7sfjly85ihya") (f (quote (("std") ("lender" "lazy_static") ("default" "std") ("c-types" "std") ("alloc")))) (y #t)))

(define-public crate-opaque-pointer-0.8.4 (c (n "opaque-pointer") (v "0.8.4") (d (list (d (n "lazy_static") (r "^1") (o #t) (d #t) (k 0)) (d (n "log") (r "^0") (d #t) (k 0)))) (h "1fxp78z43kky0la3hbwp0vnr644fvpfx0qwq42prrg05p1p7qa1m") (f (quote (("std") ("lender" "lazy_static") ("default" "std") ("c-types" "std") ("alloc")))) (y #t)))

(define-public crate-opaque-pointer-0.8.5 (c (n "opaque-pointer") (v "0.8.5") (d (list (d (n "lazy_static") (r "^1") (o #t) (d #t) (k 0)) (d (n "log") (r "^0") (d #t) (k 0)))) (h "1bxzz9fl05wqd6kvkps2sdp82lxv2pjy4adzaxlswl16bzh23b8g") (f (quote (("std") ("lender" "lazy_static") ("default" "std") ("c-types" "std") ("alloc")))) (y #t)))

(define-public crate-opaque-pointer-0.8.6 (c (n "opaque-pointer") (v "0.8.6") (d (list (d (n "lazy_static") (r "^1") (o #t) (d #t) (k 0)) (d (n "log") (r "^0") (d #t) (k 0)))) (h "1m9i925y1zkrds0brskhjzq4b3gf5psfn10727yg54afny53f4vp") (f (quote (("std") ("lender" "lazy_static") ("default" "std") ("c-types" "std") ("alloc")))) (y #t)))

(define-public crate-opaque-pointer-0.8.7 (c (n "opaque-pointer") (v "0.8.7") (d (list (d (n "lazy_static") (r "^1") (o #t) (d #t) (k 0)) (d (n "log") (r "^0") (d #t) (k 0)))) (h "0ib9aqzf9y6ri5jwilbn5vqni1zi9408g8s93s3f69w9xn7l00qa") (f (quote (("std") ("lender" "lazy_static") ("default" "std") ("c-types" "std") ("alloc"))))))

(define-public crate-opaque-pointer-0.8.8 (c (n "opaque-pointer") (v "0.8.8") (d (list (d (n "lazy_static") (r "^1") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "00zh260crmcdm5s3cwzpkdbar4570izsfww91bfgmcq71h9r3jig") (f (quote (("std") ("lender" "lazy_static") ("default" "std") ("c-types" "std") ("alloc"))))))

(define-public crate-opaque-pointer-0.8.9 (c (n "opaque-pointer") (v "0.8.9") (d (list (d (n "lazy_static") (r "^1") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1lklj0hshnvaj9j81lmnzhd7lz9xg1hqd7h9lshai78cp6k8n1d2") (f (quote (("std") ("lender" "lazy_static") ("default" "std") ("c-types" "std") ("alloc")))) (r "1.57.0")))

(define-public crate-opaque-pointer-0.8.10 (c (n "opaque-pointer") (v "0.8.10") (d (list (d (n "lazy_static") (r "^1.4") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "028c36481pyavv7dyf0b13lbhc4aq5dnw9gydl3s9jlfnb5frzi0") (f (quote (("std") ("lender" "lazy_static") ("default" "std") ("c-types" "std") ("alloc")))) (r "1.57.0")))

(define-public crate-opaque-pointer-0.8.11 (c (n "opaque-pointer") (v "0.8.11") (d (list (d (n "lazy_static") (r "^1.4") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "09d2jybr6snsykj9p7micn5srs7qr2nb3apc1l0j4idh96vjlih8") (f (quote (("std") ("lender" "lazy_static") ("default" "std") ("c-types" "std") ("alloc")))) (r "1.57.0")))

(define-public crate-opaque-pointer-0.9.0 (c (n "opaque-pointer") (v "0.9.0") (d (list (d (n "lazy_static") (r "^1.4") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0pv9qlfbwdjkzqmvlb7mr9ammywlhj62b1dhxw3w47y8njk13fsn") (f (quote (("std") ("lender" "lazy_static") ("default" "std" "lender") ("c-types" "std") ("alloc")))) (r "1.57.0")))

