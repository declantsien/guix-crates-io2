(define-module (crates-io op aq opaque_typedef_macros) #:use-module (crates-io))

(define-public crate-opaque_typedef_macros-0.0.1 (c (n "opaque_typedef_macros") (v "0.0.1") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "strum") (r "^0.8") (d #t) (k 0)) (d (n "strum_macros") (r "^0.8") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "0i7f3w2xj9ns0bja938bwln8bkn68i1xikzp2w4z6cwvwazkyy61")))

(define-public crate-opaque_typedef_macros-0.0.2 (c (n "opaque_typedef_macros") (v "0.0.2") (d (list (d (n "quote") (r "^0.5") (d #t) (k 0)) (d (n "strum") (r "^0.9") (d #t) (k 0)) (d (n "strum_macros") (r "^0.9") (d #t) (k 0)) (d (n "syn") (r "^0.13.1") (f (quote ("full"))) (d #t) (k 0)))) (h "0xgcgpfxjs11hqldlhbc33r6fsqvxa6pilxwqzxqnccw213xnqd6")))

(define-public crate-opaque_typedef_macros-0.0.3 (c (n "opaque_typedef_macros") (v "0.0.3") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^0.5") (d #t) (k 0)) (d (n "strum") (r "^0.9") (d #t) (k 0)) (d (n "strum_macros") (r "^0.9") (d #t) (k 0)) (d (n "syn") (r "^0.13.1") (f (quote ("full"))) (d #t) (k 0)))) (h "1356k270bp83qibc73z66qi686j4slix0b71zzv00rvvlqggvcl8")))

(define-public crate-opaque_typedef_macros-0.0.4 (c (n "opaque_typedef_macros") (v "0.0.4") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "strum") (r "^0.9") (d #t) (k 0)) (d (n "strum_macros") (r "^0.9") (d #t) (k 0)) (d (n "syn") (r "^0.14") (f (quote ("full"))) (d #t) (k 0)))) (h "0xzr2ij5gdhhk0pygr20waj4mk36gzsmygpz52q0qlxzhxrmjzfv")))

(define-public crate-opaque_typedef_macros-0.0.5 (c (n "opaque_typedef_macros") (v "0.0.5") (d (list (d (n "lazy_static") (r "^1.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "strum") (r "^0.15") (d #t) (k 0)) (d (n "strum_macros") (r "^0.15") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "169vsmwia40c4wbjz59vhxcbhxqdbjfzm26fkz4j3hisz86sws2m")))

