(define-module (crates-io op tb optbuilder) #:use-module (crates-io))

(define-public crate-optbuilder-0.1.0 (c (n "optbuilder") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4.30") (d #t) (k 0)) (d (n "quote") (r "^0.6.13") (d #t) (k 0)) (d (n "syn") (r "^0.15.39") (d #t) (k 0)))) (h "13ii55mw24l0al13y3npdxhh9f7md23b7masszmapa26xaphd23n")))

(define-public crate-optbuilder-0.1.1 (c (n "optbuilder") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^0.4.30") (d #t) (k 0)) (d (n "quote") (r "^0.6.13") (d #t) (k 0)) (d (n "syn") (r "^0.15.39") (d #t) (k 0)))) (h "05ga6wwybbi5nfwhdxa5b7j2w3891wszfy99pp988m7hfd2wv6wf")))

(define-public crate-optbuilder-0.1.2 (c (n "optbuilder") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.39") (d #t) (k 0)))) (h "1wgwlg8ga7bzjqxfb6b3sy4gs3zskjrr0d2xp1bip96ms2308nvd")))

