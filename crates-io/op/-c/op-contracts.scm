(define-module (crates-io op -c op-contracts) #:use-module (crates-io))

(define-public crate-op-contracts-0.1.0 (c (n "op-contracts") (v "0.1.0") (d (list (d (n "enum-variants-strings") (r "^0.2.3") (d #t) (k 0)) (d (n "ethers-core") (r "^2.0") (d #t) (k 0)) (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "hex-literal") (r "^0.4.1") (d #t) (k 0)) (d (n "inquire") (r "^0.6.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "strum") (r "^0.25") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "0xyzflcmlmp7b7i4qad82xw0c0j2k7hi85zkcy1s1amk01n9p5an") (r "1.72")))

