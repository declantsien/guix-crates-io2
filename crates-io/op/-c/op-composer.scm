(define-module (crates-io op -c op-composer) #:use-module (crates-io))

(define-public crate-op-composer-0.1.0 (c (n "op-composer") (v "0.1.0") (d (list (d (n "bollard") (r "^0.15.0") (d #t) (k 0)) (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "flate2") (r "^1.0.28") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.28") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tar") (r "^0.4.40") (d #t) (k 0)) (d (n "tokio") (r "^1.11.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.17") (d #t) (k 2)))) (h "000kyfxd0hsgjxrszi62db3ijllxbyip1phryfbyh1l4l65xqyjd") (r "1.72")))

