(define-module (crates-io op t2 opt2r) #:use-module (crates-io))

(define-public crate-opt2r-0.1.0 (c (n "opt2r") (v "0.1.0") (h "1lzw692q4paim5swpfghyc731ms460dnpvsj3v2pa8milcpf8nzb") (f (quote (("std") ("default" "std")))) (y #t)))

(define-public crate-opt2r-0.1.1 (c (n "opt2r") (v "0.1.1") (h "0d5rhgjb9csc7yism75kp46cn5d811vqvpymcdjw83nr4zfk297p") (f (quote (("std") ("default" "std")))) (y #t)))

(define-public crate-opt2r-0.1.2 (c (n "opt2r") (v "0.1.2") (h "0cjhnkcjkbcnbsch8m1y1an51308jw9ac3i09rcas53cq1v8siqa") (f (quote (("std") ("default" "std")))) (y #t)))

(define-public crate-opt2r-0.1.3 (c (n "opt2r") (v "0.1.3") (h "1zcga2kwpnnawnkwvmpb4dy2jnd91il4siw3hq9gsijr39d5c90p") (f (quote (("std") ("default" "std")))) (y #t)))

(define-public crate-opt2r-0.1.4 (c (n "opt2r") (v "0.1.4") (h "0hii1b6xirj35xxwym03pvs476dbqnh7mry2kk1sxah6kq1kk2qh") (f (quote (("std") ("default" "std")))) (y #t)))

(define-public crate-opt2r-0.2.0 (c (n "opt2r") (v "0.2.0") (h "1wd23sqrqhl6l2d5a3qhgwlvdg721hrrggj1n4vbrb2m157j2i0k") (f (quote (("std") ("default" "std")))) (y #t)))

(define-public crate-opt2r-0.2.1 (c (n "opt2r") (v "0.2.1") (h "1v9x6c77d1195mi0hgwnh7q3z5h35a5hsl4y0xax599wxvyrbb8g") (f (quote (("std") ("default" "std"))))))

