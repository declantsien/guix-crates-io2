(define-module (crates-io op t2 opt2doc) #:use-module (crates-io))

(define-public crate-opt2doc-0.1.0 (c (n "opt2doc") (v "0.1.0") (d (list (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-jsonlines") (r "^0.5.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.8.12") (d #t) (k 0)))) (h "0dh9q7ry1xjlnfcb9jw0p7yj1z3wv5fsfa30sr59i9h3v4pgv1xy")))

(define-public crate-opt2doc-0.1.1 (c (n "opt2doc") (v "0.1.1") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-jsonlines") (r "^0.5.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.8.12") (d #t) (k 0)))) (h "13hdaqqdhlf9x7ch6pqbigxl42izx9lh2275qdyiqr9aj4qd4yq9")))

(define-public crate-opt2doc-0.1.2 (c (n "opt2doc") (v "0.1.2") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-jsonlines") (r "^0.5.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.8.12") (d #t) (k 0)))) (h "0pralyfny4w2vm92k80lrz9jbpxq6vl79im5cndap2gf75hi3icj")))

