(define-module (crates-io op td optd-core) #:use-module (crates-io))

(define-public crate-optd-core-0.0.0 (c (n "optd-core") (v "0.0.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "itertools") (r "^0.11") (d #t) (k 0)) (d (n "num-derive") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "ordered-float") (r "^4") (d #t) (k 0)) (d (n "pretty-xmlish") (r "^0.1") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 0)))) (h "0m41mj4syy02qcnzb87dvj9wg2dqycdpqbbr4pk5prgpmdw65nk5")))

