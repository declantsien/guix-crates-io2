(define-module (crates-io op in opinionated_metrics) #:use-module (crates-io))

(define-public crate-opinionated_metrics-0.1.0 (c (n "opinionated_metrics") (v "0.1.0") (d (list (d (n "metrics") (r "^0.18.0") (d #t) (k 0)) (d (n "metrics-exporter-newrelic") (r "^0.1.0") (d #t) (k 0)) (d (n "metrics-exporter-prometheus") (r "^0.8.0") (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "tracing") (r "^0.1.29") (d #t) (k 0)))) (h "19lz9j6qacwh2jawzxcq5rzhrkw8fzqxjfcs5g8dfdhg6pj2zpfd")))

(define-public crate-opinionated_metrics-0.2.0 (c (n "opinionated_metrics") (v "0.2.0") (d (list (d (n "metrics") (r "^0.20.1") (d #t) (k 0)) (d (n "metrics-exporter-newrelic") (r "^0.2.0") (d #t) (k 0)) (d (n "metrics-exporter-prometheus") (r "^0.11.0") (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "tracing") (r "^0.1.29") (d #t) (k 0)))) (h "1yn6nivkvk71ffx4mag4q7kjn3758mcxlgr17hxqylqcx7cmm19k")))

