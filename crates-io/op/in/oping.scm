(define-module (crates-io op in oping) #:use-module (crates-io))

(define-public crate-oping-0.1.0 (c (n "oping") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1igw2bmmm45r87qzp55rmbm70pnv647l851c5wzx5za39fy3hym5")))

(define-public crate-oping-0.2.0 (c (n "oping") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1a5jqzx8hx58lsp7cz3cj21qk0f5bxswhzq4vkjsx2y47gy96gil")))

(define-public crate-oping-0.3.0 (c (n "oping") (v "0.3.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "17cgyysz5kg6dxg480zv5ksqliqshli6iy1y3rkd987iim745r0c")))

(define-public crate-oping-0.3.1 (c (n "oping") (v "0.3.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "09wk964bjvvxn9n443fx6nmp4rxz04hdmfi3j8wpfmzp31kyjkm5")))

(define-public crate-oping-0.3.2 (c (n "oping") (v "0.3.2") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "112vmv08k0ks5lvx9jvb41gwsrnn036w7k5mf8hid284sbqwaxjs")))

(define-public crate-oping-0.3.3 (c (n "oping") (v "0.3.3") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0vqnd2syl01x3crfp1h3794qdg7k5pk8548vljfs1bdrv98hv069")))

(define-public crate-oping-0.3.4 (c (n "oping") (v "0.3.4") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0l207dp0g0ph96gzl3xynj9w3qhnpjlamiwhgr5v5wbnkl2kns3l")))

(define-public crate-oping-0.3.5 (c (n "oping") (v "0.3.5") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "19bn0r53qq17wxh3qpd6pm402nrvarxbpcw8a5nw05ajfvcjbfvi")))

(define-public crate-oping-0.4.0 (c (n "oping") (v "0.4.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0mmx2f6p3j1sb00v88njmmmv7a5navr7v6y9nm8bspa78mbzh8j4")))

