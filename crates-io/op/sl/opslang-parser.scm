(define-module (crates-io op sl opslang-parser) #:use-module (crates-io))

(define-public crate-opslang-parser-0.2.0 (c (n "opslang-parser") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "chrono") (r "^0.4.33") (d #t) (k 0)) (d (n "opslang-ast") (r "^0.2") (d #t) (k 0)) (d (n "peg") (r "^0.8.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)))) (h "0cnz0315x9mw193cqx9048vx0p33kd4qf7jppcv3b2xilmxc8qj6")))

(define-public crate-opslang-parser-0.2.1 (c (n "opslang-parser") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "chrono") (r "^0.4.33") (d #t) (k 0)) (d (n "opslang-ast") (r "^0.2.1") (d #t) (k 0)) (d (n "peg") (r "^0.8.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)))) (h "0pflckdk6cp8yllwygy7b3k84zlvv1w3yhn0igyxn0z03abvl7ca")))

(define-public crate-opslang-parser-0.3.0-alpha.1 (c (n "opslang-parser") (v "0.3.0-alpha.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "chrono") (r "^0.4.33") (d #t) (k 0)) (d (n "opslang-ast") (r "^0.3.0-alpha.1") (d #t) (k 0)) (d (n "peg") (r "^0.8.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)))) (h "0v7rdyv0f8k1rhq2252kafpw6q4fn09hawmdabjgrxqip2r9z2gw")))

