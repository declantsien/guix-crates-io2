(define-module (crates-io op us opus) #:use-module (crates-io))

(define-public crate-opus-0.1.0 (c (n "opus") (v "0.1.0") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "opus-sys") (r "^0.1.2") (d #t) (k 0)))) (h "1cmx765mwvrv65a555q5wdggz2j9w3zq6c2lqs10xxgsz7ha1ljr")))

(define-public crate-opus-0.1.1 (c (n "opus") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "opus-sys") (r "^0.2.0") (d #t) (k 0)))) (h "0whw1hxhly6yh30g8plz9zp78qmxgd6qx4j0jcnvx094hhia5pn6")))

(define-public crate-opus-0.2.0 (c (n "opus") (v "0.2.0") (d (list (d (n "compiletest_rs") (r "^0.2.4") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "opus-sys") (r "^0.2.0") (d #t) (k 0)))) (h "1nyhx841v0s2shpc8f7rham32gwfzal9y74r3lm8bl1rnjda47jw")))

(define-public crate-opus-0.2.1 (c (n "opus") (v "0.2.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "opus-sys") (r "^0.2.0") (d #t) (k 0)))) (h "12y1brz0sa4ia7cfjrpxfv9ad4zkkz59ajlcp1g6drpivakmk42y")))

(define-public crate-opus-0.3.0 (c (n "opus") (v "0.3.0") (d (list (d (n "audiopus_sys") (r "^0.2.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "13jyszydy9p15k1vnzbbshscvriqznm9d7gmbzjrhzja4ydl09k5")))

