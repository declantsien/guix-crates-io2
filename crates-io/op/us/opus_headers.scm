(define-module (crates-io op us opus_headers) #:use-module (crates-io))

(define-public crate-opus_headers-0.1.0 (c (n "opus_headers") (v "0.1.0") (h "1yb759ahj4kkga4pw207isggaynf55d81w7ynp9j9217ajbdh9n1")))

(define-public crate-opus_headers-0.1.1 (c (n "opus_headers") (v "0.1.1") (h "025c5w663rncxq42qzsdjdr4jk52gzghp0060k3yzqjmf03qhb37")))

(define-public crate-opus_headers-0.1.2 (c (n "opus_headers") (v "0.1.2") (h "0q2b81b0p6g1xxwd90rkwyj58zxcim7r8dn55dy3j4gi8wwrkfxg")))

