(define-module (crates-io op us opusenc-sys) #:use-module (crates-io))

(define-public crate-opusenc-sys-0.1.0 (c (n "opusenc-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)))) (h "0i4sizqxbmwfzzprcmigm2szjdai47wd85iizm8k20ic3ilz1pi8")))

(define-public crate-opusenc-sys-0.2.0 (c (n "opusenc-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.69.2") (d #t) (k 1)))) (h "06yqz8di6b6qwmkprxc339q1bf3j282c82abc90plakcqmssr493") (y #t) (l "opusenc")))

(define-public crate-opusenc-sys-0.2.1 (c (n "opusenc-sys") (v "0.2.1") (d (list (d (n "bindgen") (r "^0.69.2") (d #t) (k 1)))) (h "022azvv8hxlvbn64am2rann2gpam4mnd3x78nx15in4lgg1g1kai") (l "opusenc")))

