(define-module (crates-io op us opus-rust) #:use-module (crates-io))

(define-public crate-opus-rust-0.1.0 (c (n "opus-rust") (v "0.1.0") (d (list (d (n "opus-cmake-sys") (r "^1.0.0") (d #t) (k 0)))) (h "0015kfvy854yv8817pbjz9dk0wi0pyps0p64vkg0ig9cccyc2p3q")))

(define-public crate-opus-rust-0.1.1 (c (n "opus-rust") (v "0.1.1") (d (list (d (n "opus-cmake-sys") (r "^1.0.2") (d #t) (k 0)))) (h "16m6ylr9wgh5glav26ag87jpyp523qp9kzqycrf2ma0x932c9vrx")))

