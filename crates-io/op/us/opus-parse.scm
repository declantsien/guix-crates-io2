(define-module (crates-io op us opus-parse) #:use-module (crates-io))

(define-public crate-opus-parse-0.0.1 (c (n "opus-parse") (v "0.0.1") (d (list (d (n "derive-error-chain") (r "^0.10.1") (d #t) (k 0)) (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)) (d (n "flate2") (r "^0.2.19") (d #t) (k 0)) (d (n "itertools") (r "^0.6.0") (d #t) (k 0)) (d (n "minidom") (r "^0.3.0") (d #t) (k 0)) (d (n "xml-rs") (r "^0.4.1") (d #t) (k 0)))) (h "1xry6x8k8lz6afs22c27j9msdsgf42sqldiv2q7sjnhzy5v1xcfc")))

(define-public crate-opus-parse-0.0.2 (c (n "opus-parse") (v "0.0.2") (d (list (d (n "derive-error-chain") (r "^0.10.1") (d #t) (k 0)) (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)) (d (n "flate2") (r "^0.2.19") (d #t) (k 0)) (d (n "itertools") (r "^0.6.0") (d #t) (k 0)) (d (n "minidom") (r "^0.3.0") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)) (d (n "xml-rs") (r "^0.4.1") (d #t) (k 0)))) (h "1x6k1hxm3pcvzkbwpzmcgrh441dkx0hgqs8d2fdk7r81ph9rrinq")))

(define-public crate-opus-parse-0.0.3 (c (n "opus-parse") (v "0.0.3") (d (list (d (n "derive-error-chain") (r "^0.10.1") (d #t) (k 0)) (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)) (d (n "flate2") (r "^0.2.19") (d #t) (k 0)) (d (n "itertools") (r "^0.6.0") (d #t) (k 0)) (d (n "minidom") (r "^0.3.0") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)) (d (n "xml-rs") (r "^0.4.1") (d #t) (k 0)))) (h "0hz0haq638zvak3c8aag0aa77wzwrg6xpvx14lmxx2ng97cf4fnk")))

