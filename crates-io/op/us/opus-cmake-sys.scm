(define-module (crates-io op us opus-cmake-sys) #:use-module (crates-io))

(define-public crate-opus-cmake-sys-1.0.0 (c (n "opus-cmake-sys") (v "1.0.0") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.8") (d #t) (k 1)))) (h "0g91ff6pnjwlwnls9g1by8q30i4fgnb63kl6pq3jmdqq98q238cc") (l "opus")))

(define-public crate-opus-cmake-sys-1.0.1 (c (n "opus-cmake-sys") (v "1.0.1") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.8") (d #t) (k 1)))) (h "13a8mxzzi7g3w3wn0yrpiigcgza7szbkxcfsn4q6nj1bn1j5sfzn") (l "opus")))

(define-public crate-opus-cmake-sys-1.0.2 (c (n "opus-cmake-sys") (v "1.0.2") (d (list (d (n "cc") (r "^1.0.59") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.8") (d #t) (k 1)))) (h "1xf4x1avl5nwqracsn45z8f62jwmdzddvc4x83z2rzfpyvycswdg") (l "opus")))

(define-public crate-opus-cmake-sys-1.0.3 (c (n "opus-cmake-sys") (v "1.0.3") (d (list (d (n "cc") (r "^1.0.59") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.8") (d #t) (k 1)))) (h "10s43qqqggrfvr3cf6j92blyhd1qs9caf9xy32zwndw34lmrsxp9") (l "opus")))

(define-public crate-opus-cmake-sys-1.0.4 (c (n "opus-cmake-sys") (v "1.0.4") (d (list (d (n "cc") (r "^1.0.59") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.8") (d #t) (k 1)))) (h "0505rj0am8h86i9vwhbcfq41xwm87ll23zglc47jzdzjgsx5jzp2") (l "opus")))

(define-public crate-opus-cmake-sys-1.0.5 (c (n "opus-cmake-sys") (v "1.0.5") (d (list (d (n "cc") (r "^1.0.59") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.8") (d #t) (k 1)))) (h "1qma6yn7ypw0vsvfvhpbfzhiaf54ls8q8iz971h423yqk0y0p80j") (l "opus")))

(define-public crate-opus-cmake-sys-1.0.6 (c (n "opus-cmake-sys") (v "1.0.6") (d (list (d (n "cc") (r "^1.0.59") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.8") (d #t) (k 1)))) (h "1bxdrlj69iljx2n1pknh1w82mxlhqgca1r4p4i5v41xxzqxc7a13") (l "opus")))

