(define-module (crates-io op us opusenc) #:use-module (crates-io))

(define-public crate-opusenc-0.1.0 (c (n "opusenc") (v "0.1.0") (d (list (d (n "cstr") (r "^0.2.11") (d #t) (k 0)) (d (n "num_enum") (r "^0.6.1") (d #t) (k 0)) (d (n "opusenc-sys") (r "^0.1.0") (d #t) (k 0)))) (h "04qm9y0vds4cg5x370yzszhha8mawr8pgsl485n3jb6mpzbdni4c") (f (quote (("encoder-options") ("default"))))))

(define-public crate-opusenc-0.2.0 (c (n "opusenc") (v "0.2.0") (d (list (d (n "cstr") (r "^0.2.11") (d #t) (k 0)) (d (n "num_enum") (r "^0.7.2") (d #t) (k 0)) (d (n "opusenc-sys") (r "^0.1.0") (d #t) (k 0)))) (h "074yyxjb520fnkpklng9bynvdivkzdl3262l376q9gv9ck29zx79") (f (quote (("encoder-options") ("default")))) (y #t)))

(define-public crate-opusenc-0.2.1 (c (n "opusenc") (v "0.2.1") (d (list (d (n "cstr") (r "^0.2.11") (d #t) (k 0)) (d (n "num_enum") (r "^0.7.2") (d #t) (k 0)) (d (n "opusenc-sys") (r "^0.2.1") (d #t) (k 0)))) (h "0bk9v81bfv3i2kx615anb21kf2cq7592lj8dvciacacbginmyp4l") (f (quote (("encoder-options") ("default"))))))

