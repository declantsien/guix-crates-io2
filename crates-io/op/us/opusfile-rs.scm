(define-module (crates-io op us opusfile-rs) #:use-module (crates-io))

(define-public crate-opusfile-rs-0.1.0 (c (n "opusfile-rs") (v "0.1.0") (d (list (d (n "enum_primitive") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "opusfile-sys") (r "*") (d #t) (k 0)))) (h "10drx1plkxkklhvbgjgqy6vw0vm9rcn6gm2601grycp9h0f8a38c")))

(define-public crate-opusfile-rs-0.1.1 (c (n "opusfile-rs") (v "0.1.1") (d (list (d (n "enum_primitive") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "opusfile-sys") (r "*") (d #t) (k 0)))) (h "1s31yxni58lq018mynqfnb88ijzvsvdiy3ss2y0g6swbj3wg3339")))

(define-public crate-opusfile-rs-0.1.2 (c (n "opusfile-rs") (v "0.1.2") (d (list (d (n "enum_primitive") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "opusfile-sys") (r "*") (d #t) (k 0)))) (h "1z0l7ci6fxpdrr7a4yc3q3g7wwl4xvy1di65nsvg1x120fjswll2")))

(define-public crate-opusfile-rs-0.1.3 (c (n "opusfile-rs") (v "0.1.3") (d (list (d (n "enum_primitive") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "opusfile-sys") (r "^0.1") (d #t) (k 0)))) (h "0jknyy9yfd10mvw5yv0h5q986wmx4avz1qb7y9xqsd78800q32pa")))

(define-public crate-opusfile-rs-0.1.4 (c (n "opusfile-rs") (v "0.1.4") (d (list (d (n "enum_primitive") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "opusfile-sys") (r "^0.2") (d #t) (k 0)))) (h "0wwgymr7mdp3zy33kgh4j7m4sfifqvbb894kv0hi0sm79mhl68qx")))

