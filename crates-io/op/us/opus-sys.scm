(define-module (crates-io op us opus-sys) #:use-module (crates-io))

(define-public crate-opus-sys-0.1.0 (c (n "opus-sys") (v "0.1.0") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "pkg-config") (r "*") (d #t) (k 1)))) (h "1cyh9akm30z29vw2g5clff7f83sjw3qz2qf0hm4nybh6ycvxn2rb")))

(define-public crate-opus-sys-0.1.1 (c (n "opus-sys") (v "0.1.1") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "pkg-config") (r "*") (d #t) (k 1)))) (h "03dihjb0pci6vnal2rq50bpmfqj8dcdancy7mhldpm4mng7q09w7")))

(define-public crate-opus-sys-0.1.2 (c (n "opus-sys") (v "0.1.2") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "pkg-config") (r "*") (d #t) (k 1)))) (h "08z6lnjdb3wzhk8lb3jziinjdryvi34p23cjzw69xs8iyqz6iwsr")))

(define-public crate-opus-sys-0.2.0 (c (n "opus-sys") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.8") (d #t) (k 1)))) (h "0r9fbzlkf04mxcwwywfv5h82xyqyx25qnsjrqfqwprl6682pza82")))

(define-public crate-opus-sys-0.2.1 (c (n "opus-sys") (v "0.2.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.8") (d #t) (k 1)))) (h "1v5n4lzshkzfhc2di6amjzk4wkjnb96wdcb6lhprgxw2yjab5n7s")))

