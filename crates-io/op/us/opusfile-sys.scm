(define-module (crates-io op us opusfile-sys) #:use-module (crates-io))

(define-public crate-opusfile-sys-0.1.0 (c (n "opusfile-sys") (v "0.1.0") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "ogg-sys") (r "*") (d #t) (k 0)) (d (n "opus-sys") (r "*") (d #t) (k 0)) (d (n "pkg-config") (r "*") (d #t) (k 1)))) (h "15aqnyz4k9akc3gh7bfh27rvakz5rfy6979g81llsx1dk6cdc9mg")))

(define-public crate-opusfile-sys-0.1.1 (c (n "opusfile-sys") (v "0.1.1") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "ogg-sys") (r "*") (d #t) (k 0)) (d (n "opus-sys") (r "*") (d #t) (k 0)) (d (n "pkg-config") (r "*") (d #t) (k 1)))) (h "1a9zhdymjn5yyqbdkd73427m9x53d4i2j06kwawjk9nkvwzaqz2h")))

(define-public crate-opusfile-sys-0.2.0 (c (n "opusfile-sys") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "ogg-sys") (r "^0.0.9") (d #t) (k 0)) (d (n "opus-sys") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.8") (d #t) (k 1)))) (h "1dvwv40rp6pgzd3pa8zdjxla07823xqmj7yd38zfy8rd7h7ndy4r")))

