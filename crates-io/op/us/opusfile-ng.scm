(define-module (crates-io op us opusfile-ng) #:use-module (crates-io))

(define-public crate-opusfile-ng-0.1.0 (c (n "opusfile-ng") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "num-derive") (r "^0.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.16") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.27") (d #t) (k 1)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)))) (h "1if0pgv7am04b9dvs8ak6zymgsggahq5s0f8bl2vd1qx6w9c8h3z")))

