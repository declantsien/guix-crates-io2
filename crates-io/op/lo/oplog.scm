(define-module (crates-io op lo oplog) #:use-module (crates-io))

(define-public crate-oplog-0.1.0 (c (n "oplog") (v "0.1.0") (d (list (d (n "bson") (r "^0.3.0") (d #t) (k 0)) (d (n "chrono") (r "^0.2.0") (d #t) (k 0)) (d (n "mongodb") (r "^0.1.8") (d #t) (k 0)))) (h "192hmzrcyn63rr35z86q8fq9d4f2rrm5d0zzyh69sskfn43c9vbf")))

(define-public crate-oplog-0.2.0 (c (n "oplog") (v "0.2.0") (d (list (d (n "bson") (r "^0.3.0") (d #t) (k 0)) (d (n "chrono") (r "^0.2.0") (d #t) (k 0)) (d (n "mongodb") (r "^0.1.8") (d #t) (k 0)))) (h "0ihsfcnpjmivjz92nr7d1kc32qx4qf12r0sz121x5l10pyzz4c50")))

(define-public crate-oplog-0.3.0 (c (n "oplog") (v "0.3.0") (d (list (d (n "bson") (r "^0.10.0") (d #t) (k 0)) (d (n "chrono") (r "^0.2.0") (d #t) (k 0)) (d (n "mongodb") (r "^0.3.0") (d #t) (k 0)))) (h "10g27ffpwhgfnc1yl6dqyj2k2lrpx3kzd0234px7byrvbsnza556")))

