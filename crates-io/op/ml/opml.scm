(define-module (crates-io op ml opml) #:use-module (crates-io))

(define-public crate-opml-0.1.0 (c (n "opml") (v "0.1.0") (d (list (d (n "regex") (r "^1.3.7") (d #t) (k 0)) (d (n "strong-xml") (r "^0.6.0") (d #t) (k 0)))) (h "1icx5nx627v8slq1m3y8c00is4r76an7z62d900vn5ch3p11dvs2")))

(define-public crate-opml-0.1.1 (c (n "opml") (v "0.1.1") (d (list (d (n "regex") (r "^1.3.7") (d #t) (k 0)) (d (n "strong-xml") (r "^0.6.0") (d #t) (k 0)))) (h "1yyj45jglhmgan702icysjqcwqidp6i8a4pmhxcprfrh663x0q8a")))

(define-public crate-opml-0.2.0 (c (n "opml") (v "0.2.0") (d (list (d (n "regex") (r "^1.3.7") (d #t) (k 0)) (d (n "strong-xml") (r "^0.6.0") (d #t) (k 0)))) (h "14xgf2b5fc8a3g0affhglwjhig2qnilbx9hnn83dww23wg98w8vz")))

(define-public crate-opml-0.2.1 (c (n "opml") (v "0.2.1") (d (list (d (n "regex") (r "^1.3.7") (d #t) (k 0)) (d (n "strong-xml") (r "^0.6.0") (d #t) (k 0)))) (h "1qzc2dsq81cg4adlhr9alnddnwyiwjjm3zdpk00mnxh8r1r8rnqq")))

(define-public crate-opml-0.2.2 (c (n "opml") (v "0.2.2") (d (list (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strong-xml") (r "^0.6.0") (d #t) (k 0)))) (h "1il48pnj66jwb13n37sfhikcln0dknh5hyzmqv34gkg3zxj1sa6z")))

(define-public crate-opml-0.2.3 (c (n "opml") (v "0.2.3") (d (list (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strong-xml") (r "^0.6.0") (d #t) (k 0)))) (h "108p390dijcd8ml1pkr2q6zg8k51w1p23j7vq0asvsa3aqslixn4")))

(define-public crate-opml-0.2.4 (c (n "opml") (v "0.2.4") (d (list (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strong-xml") (r "^0.6.0") (d #t) (k 0)))) (h "16x1yw5qmpf6l49shvyd4a8k7f0xnanm07hxr513q4s4acwrgmjw")))

(define-public crate-opml-0.3.0 (c (n "opml") (v "0.3.0") (d (list (d (n "regex") (r "^1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strong-xml") (r "^0.6") (d #t) (k 0)))) (h "07bpjgl0c8gpi93ilrrvf2mrj6w9za6z97dkyfcjvrky110shnh4")))

(define-public crate-opml-1.0.1 (c (n "opml") (v "1.0.1") (d (list (d (n "regex") (r "^1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strong-xml") (r "^0.6.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "1kp4rvhsi13fy70dr3pp3r2b2lr6ybdjl0p2cbspj8brrp0d4hg7")))

(define-public crate-opml-1.1.0 (c (n "opml") (v "1.1.0") (d (list (d (n "regex") (r "^1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strong-xml") (r "^0.6.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "0zr7f101inkz3rknl0r3q0qkklfxhqrji7yxmqixajfn3m5mbjfa")))

(define-public crate-opml-1.1.1 (c (n "opml") (v "1.1.1") (d (list (d (n "regex") (r "^1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strong-xml") (r "^0.6.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "1xr51n6xvw43xxvwnn2yyk331z2b746hbhimd59j23zqk6kzr803")))

(define-public crate-opml-1.1.2 (c (n "opml") (v "1.1.2") (d (list (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strong-xml") (r "^0.6.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.29") (d #t) (k 0)))) (h "1mx2wzhalx20nxs1xc5lj3kcx1bnlsygln97ldqhfil796zrgcsx")))

(define-public crate-opml-1.1.3 (c (n "opml") (v "1.1.3") (d (list (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strong-xml") (r "^0.6.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.29") (d #t) (k 0)))) (h "18bjav05xmh4nq4mna5y4w2fd22fmgpgs7qllf9ww1lcxv5ykscg")))

(define-public crate-opml-1.1.4 (c (n "opml") (v "1.1.4") (d (list (d (n "hard-xml") (r "^1.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.29") (d #t) (k 0)))) (h "16cyzw4z04kmkkyy6kg4xj369q208wmsvnqh34fyb8mz2518q6yl")))

(define-public crate-opml-1.1.5 (c (n "opml") (v "1.1.5") (d (list (d (n "hard-xml") (r "^1.13.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "17bp5kb6xdwv6ldlkrfp8h329i3v11ydfyd5zjycjlkscykiczk5")))

(define-public crate-opml-1.1.6 (c (n "opml") (v "1.1.6") (d (list (d (n "hard-xml") (r "^1.34.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "1wnx34bf1sk93i4r9jbs0hhr7cqy30m9x6n2dmkr4yl5di19cbyz")))

