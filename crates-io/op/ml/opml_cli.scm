(define-module (crates-io op ml opml_cli) #:use-module (crates-io))

(define-public crate-opml_cli-0.3.0 (c (n "opml_cli") (v "0.3.0") (d (list (d (n "assert_cmd") (r "^1.0") (d #t) (k 2)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "opml") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "06rp22m2c5mqa7jv9mgx5bmhnhhd07bdbcrrjzkfbhsvbapjmrl5")))

(define-public crate-opml_cli-1.0.1 (c (n "opml_cli") (v "1.0.1") (d (list (d (n "assert_cmd") (r "^1.0") (d #t) (k 2)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "opml") (r "^1.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1izkaapnfzmhzb5zbsslclh9wf9fy81k8jdnpyi4sc1bzjk9lk0v")))

(define-public crate-opml_cli-1.1.0 (c (n "opml_cli") (v "1.1.0") (d (list (d (n "assert_cmd") (r "^1.0") (d #t) (k 2)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "opml") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0mf5ixz63r7186m8s3cvdqikrbniz8an9cqk265s3whdamd2x190")))

(define-public crate-opml_cli-1.1.1 (c (n "opml_cli") (v "1.1.1") (d (list (d (n "assert_cmd") (r "^1.0") (d #t) (k 2)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "opml") (r "^1.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0355vcmszr1wrd44nfr9ncnfr29igw95g6978b8zrxahhdgdyngx")))

(define-public crate-opml_cli-1.1.3 (c (n "opml_cli") (v "1.1.3") (d (list (d (n "assert_cmd") (r "^2.0.1") (d #t) (k 2)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "opml") (r "^1.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.68") (d #t) (k 0)))) (h "0jifgxq28rm854p7iivml869zkybw3mf1inr6k81xkrp2bakl5mj")))

(define-public crate-opml_cli-1.1.4 (c (n "opml_cli") (v "1.1.4") (d (list (d (n "assert_cmd") (r "^2.0.1") (d #t) (k 2)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "opml") (r "^1.1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.68") (d #t) (k 0)))) (h "0rwghbrxaffc0s382n1wc8q4vpwmcfq3q2dpa3qhfbxghn4zzkwz")))

(define-public crate-opml_cli-1.1.5 (c (n "opml_cli") (v "1.1.5") (d (list (d (n "assert_cmd") (r "^2.0.4") (d #t) (k 2)) (d (n "clap") (r "^4.0.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "insta") (r "^1.21.0") (d #t) (k 2)) (d (n "opml") (r "^1.1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)) (d (n "test-case") (r "^2.2.1") (d #t) (k 2)))) (h "1w73rfqpdrwsdvjjzcmwzkyvyxb08hk2f10g5hs5qamschdzr93d")))

(define-public crate-opml_cli-1.1.6 (c (n "opml_cli") (v "1.1.6") (d (list (d (n "assert_cmd") (r "^2.0.13") (d #t) (k 2)) (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "insta") (r "^1.34.0") (d #t) (k 2)) (d (n "opml") (r "^1.1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)) (d (n "test-case") (r "^3.3.1") (d #t) (k 2)))) (h "0rcffjjmw86nw4pr0nqrlrc9601ld1lby0phpj5hxl8fl0hzrpx7")))

