(define-module (crates-io op al opal) #:use-module (crates-io))

(define-public crate-opal-0.1.0 (c (n "opal") (v "0.1.0") (h "0ldcb4cnh779aac8bx7wy46rh49ix3is9xb0jzk06hxg5z1b2926") (y #t)))

(define-public crate-opal-0.1.1 (c (n "opal") (v "0.1.1") (h "1kp5adixrzzf3c7ggyammr7z5z5s8am7lbg3qd0xi9h5z6hxg52z") (y #t)))

(define-public crate-opal-0.1.2 (c (n "opal") (v "0.1.2") (h "0j949h94jyby5wxvgnrij5qzf5gps1n03apw3168xjh1p9g9fcm5")))

