(define-module (crates-io op al opal-editor) #:use-module (crates-io))

(define-public crate-opal-editor-0.1.0 (c (n "opal-editor") (v "0.1.0") (h "0nd3qngd69jjv4dyhjh5y4rc1isg8ypwa0jxrqjw114a5xby6pvv") (y #t)))

(define-public crate-opal-editor-0.0.0 (c (n "opal-editor") (v "0.0.0") (h "0h93mgzb6a1dnpq08glhml5003y99lp17h34fch5qzggvy494lv8")))

