(define-module (crates-io op lt opltypes) #:use-module (crates-io))

(define-public crate-opltypes-0.1.0 (c (n "opltypes") (v "0.1.0") (d (list (d (n "arrayvec") (r "^0.7") (d #t) (k 0)) (d (n "ascii") (r "^1.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "strum") (r "^0.21") (d #t) (k 0)) (d (n "strum_macros") (r "^0.21") (d #t) (k 0)))) (h "0qgq42b1a4smqpsksqrlfsgm05sdxpqiym16kyprjvyc0s1nfqhn")))

(define-public crate-opltypes-0.2.0 (c (n "opltypes") (v "0.2.0") (d (list (d (n "arrayvec") (r "^0.7") (d #t) (k 0)) (d (n "ascii") (r "^1.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "strum") (r "^0.21") (d #t) (k 0)) (d (n "strum_macros") (r "^0.21") (d #t) (k 0)))) (h "1xk0bsya3nj78938rih9089zlw8hvl866whsplaimdsd7pbbyygl")))

