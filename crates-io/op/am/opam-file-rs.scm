(define-module (crates-io op am opam-file-rs) #:use-module (crates-io))

(define-public crate-opam-file-rs-0.1.1 (c (n "opam-file-rs") (v "0.1.1") (d (list (d (n "lalrpop") (r "^0.19.4") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.19.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "0d4z0x03f6n5yn6yqb97fdbpz6rkx3545n2815n48bp00kpawgyq")))

(define-public crate-opam-file-rs-0.1.2 (c (n "opam-file-rs") (v "0.1.2") (d (list (d (n "lalrpop") (r "^0.19.4") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.19.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "0yqqzwpybk5yvhd2mxms9r90hdlzj60bs8galbpild7bvmai6cpi")))

(define-public crate-opam-file-rs-0.1.4 (c (n "opam-file-rs") (v "0.1.4") (d (list (d (n "lalrpop") (r "^0.19.4") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.19.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "0cxakhmc9bd4bn9gf2gfxg91xdqj298i9wyn0aw0qwmv68brjkg8")))

(define-public crate-opam-file-rs-0.1.5 (c (n "opam-file-rs") (v "0.1.5") (d (list (d (n "lalrpop") (r "^0.19.4") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.19.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "0qs81i727vp12745di3w22iyjx6qh5lxhbbp6031gj86czigvjad")))

