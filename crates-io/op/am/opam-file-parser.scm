(define-module (crates-io op am opam-file-parser) #:use-module (crates-io))

(define-public crate-opam-file-parser-0.1.0 (c (n "opam-file-parser") (v "0.1.0") (d (list (d (n "lalrpop") (r "^0.19.4") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.19.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "0ca1qs1v4b7j6yjfvaxyvs3hgkwn0p0x7va4drina0wcgxagzg47") (y #t)))

